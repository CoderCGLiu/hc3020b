/******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件实现了ReWorks启动初始化程序
 * 修改：
 * 		 1. 2011-09-08，规范BSP
 *
 * 系统启动代码
 * 管理系统启动后的初始化过程。包括识别处理器类型，设置寄存器，设置系统堆栈等。初始化
 * 最后阶段跳入c_main，进入操作系统内核。
 */ 
#define ASM_LANGUAGE
#include <gs464_cpu.h>
#include <mips_asm.h>

/*******************************************************************************
 *
 * 全局函数定义
 */

	.rdata

str_hello_loongson:
	.ascii	"\r\n"
	.asciz  "ECI: hello hr3 boot!\r\n"

str_not_loongson2f:
	.ascii	"\r\n"
	.ascii  "the processor reversion identifier(PRID) is not LOONGSON 3A!\r\n"
	.asciz  "suspended.\r\n"

	/* internals */
	.global _stack_size		/* 栈大小 */
	.globl	start			/* start.s文件的入口 */
	.globl	sys_cpu_init			/* 从核初始化 */
	.globl _text_start

	/* externals */
	.globl  serial_putstr	/* 串口输出 */
	.globl  c_main			/* ReWorks核心启动入口 */	
	.globl  uart_transmit_test

	.global c_main_secondary

	.text


#define PUT_CHAR(c)             \
    li      v1, c              ;\
    dli	    v0, 0x900000001f050030 ;\
    sb      v1, 0(v0)          ;\
    nop                        ;

#define LOOP_CHAR(c) \
1:                   \
	PUT_CHAR(c)      \
	b 1b             ;\
	nop              ;\



/*******************************************************************************
 *
 * 栈和堆的大小
 */

	.ent	start
start: 
	.set noreorder
   
	/*测试串口轮询 Hello,ReWorks!*/
    PUT_CHAR('\n')
    PUT_CHAR('H')PUT_CHAR('e')PUT_CHAR('l')PUT_CHAR('l')PUT_CHAR('o')PUT_CHAR(',')
    PUT_CHAR('R')PUT_CHAR('e')PUT_CHAR('W')PUT_CHAR('o')PUT_CHAR('r')PUT_CHAR('k')PUT_CHAR('s')PUT_CHAR('!')
    PUT_CHAR('\t')PUT_CHAR('7')
    PUT_CHAR('\n')

    	//test
	li      v1, '1'
    dli	    v0, 0x900000001f050030
    sb      v1, 0(v0)
    nop
/*----------------------------------------------------------------------------+
|                         屏蔽所有中断
+----------------------------------------------------------------------------*/
#  modified by FuKai,20210326
#	li		t0, SR_CU1|SR_CU0|SR_UX  /* 禁止所有中断 */
	li		t0, SR_CU1|SR_CU0|SR_UX|SR_KX  /* 禁止所有中断 */
/*******************************************************************************
 *
 * 初始化处理器
 */
init_cpu:
#   del by FuKai,20210326, 如果执行该指令，则mmu会被重置，无法访问0x900000001f000030

//	li		t0,0x748800E0 //test
	mtc0	t0, C0_SR		   /* 打开CPU	*/


    	//test
	li      v1, '2'
    dli	    v0, 0x900000001f050030
    sb      v1, 0(v0)
    nop


/*
	mfc0   t0, $15, 1
    andi   t0, t0, 0x3ff

    beqz   t0, 0f
    nop
    li     t1, 1
    beq    t0, t1, 1f
    nop

1:
	jal	    c_main_secondary
	nop
0:
*/
//	mfc0	t0, C0_PRID        /* 判别处理器类型 */
//	li		t1, PRID_LSN3A
//	bne		t0, t1, 0f
//	nop
    
 cpu_reg:
/*----------------------------------------------------------------------------+
|                         CPU内部寄存器初始化
+----------------------------------------------------------------------------*/
	mtc0	zero, C0_COUNT
	li		v0, 0xffffffff
	mtc0	v0, C0_COMPARE
    
    
/*----------------------------------------------------------------------------+
|                         读取PMON传递的参数
+----------------------------------------------------------------------------*/  
  
	/*sd		a0, fw_arg0
	sd		a1, fw_arg1
	sd		a2, fw_arg2
	sd		a3, fw_arg3*/
	
hard_init:	
/*----------------------------------------------------------------------------+
|                         系统硬件初始化
+----------------------------------------------------------------------------*/	
	

exc_init:	
/*----------------------------------------------------------------------------+
|                         异常空间初始化
+----------------------------------------------------------------------------*/
/*******************************************************************************
 *
 * 初始化栈
 */
init_stack:
	dla		gp, _gp			
	dla		sp, _text_start-1024
	/*jal		printk_debug_hw_init
	nop
	dla      a0, str_hello_loongson 
	jal     serial_putstr 
	nop */

/* test for uart*/
/*CTRL*/
#	li      v1, 0x17
 #   dli     v0, 0xffffffffbf000000
 #   sb      v1, 0(v0)
/*MODE*/
#    li      v1, 0x1022
#    dli     v0, 0xffffffffbf000004
#    sw      v1, 0(v0)
/*BDIVR*/
 #   li      v1, 0x1c
 #   dli     v0, 0xffffffffbf000034
 #   sw      v1, 0(v0)
/*BRGR*/
 #   li      v1, 0x6
 #   dli  v0, 0xffffffffbf000018
  #  sw      v1, 0(v0)
/*FIFO*/
#1:
#	li      v1, 0x55
#    li  v0, 0xbf000030
#    sb      v1, 0(v0)
#	b 1b
#	nop
#	jal		uart_transmit_test

    li      t1,0xbf0c0000
	li		t2,'#'
4:
	sb      t2,0(t1)  
	//b   4b
jump_reworks_kernel:	
/*----------------------------------------------------------------------------+
|                         进入reworks内核
+----------------------------------------------------------------------------*/	
	jal	    c_main	
	nop
	b	1f
	nop
 
0:
	dla      a0, str_not_loongson2f
	jal		serial_putstr
	nop
1:
	b	1b				# but loop back just in-case
	nop

    .set reorder
	.end	start




sys_cpu_init:
	mtc0	zero, C0_CAUSE
	
	/* give as long as possible before a clock interrupt */
	li	    v0, 1
	mtc0	v0, C0_COUNT
	mtc0	zero, C0_COMPARE
	
	/* 
	 * need to clear the BEV bit. Exception vectors have
	 * already been set up by the boot processor, so we are
	 * able to use them.
	 */
	mfc0	t0, C0_SR
    nop
    nop
	
	and	t0, ~SR_BEV
	ori t0, SR_UX
	mtc0	t0, C0_SR
	nop
	nop
	nop
	nop
    jr		ra
	/*lw	a0, taskSrDefault
	and	a0, ~SR_IE
	jal	intSRSet	
	nop*/
