/*
 * mips_n32_regdef.h
 * 
 * GNU的mips寄存器定义没有采用n32的标准，导致vx的汇编移植不过来，这里重新定义寄存器
 *
 *  Created on: 2018-5-25
 *      Author: FuKai
 */

#ifndef MIPS_N32_REGDEF_H_
#define MIPS_N32_REGDEF_H_

#if defined(__mips_abi_n32__) || defined(__mips_abi_n64__)
/*
#undef zero
#undef v0
#undef v1
#undef a0
#undef a1
#undef a2
#undef a3
*/
#undef t0
#undef t1
#undef t2
#undef t3
#undef t4
#undef t5
#undef t6
#undef t7
/*
#undef s0
#undef s1
#undef s2
#undef s3
#undef s4
#undef s5
#undef s6
#undef s7
#undef t8
#undef t9
#undef k0
#undef k1
#undef gp
#undef sp
#undef s8
#undef ra
#undef pc
*/

/*
#define zero	$0
#define at      $1
#define v0	$2
#define v1	$3
#define a0	$4
#define a1	$5
#define a2	$6
#define a3	$7
*/

//ww mod 20180606 现在64位编译器 没有定义__mips_abi_n32__而是__mips_abi_n64__
//#ifdef __mips_abi_n32__
#define a4	$8
#define a5	$9
#define a6	$10
#define a7	$11
#define t0	$12
#define t1	$13
#define t2	$14
#define t3	$15
//#else /*o32*/
//#define t0	$8
//#define t1	$9
//#define t2	$10
//#define t3	$11
//#define t4	$12
//#define t5	$13
//#define t6	$14
//#define t7	$15
//#endif


/*
#define s0	$16
#define s1	$17
#define s2	$18
#define s3	$19
#define s4	$20
#define s5	$21
#define s6	$22
#define s7	$23
#define t8	$24
#define t9	$25
#define k0	$26
#define k1	$27
#define gp	$gp
#define sp	$sp
#define s8	$30
#define ra	$31
#define pc	$pc
*/
#endif


#endif /* MIPS_N32_REGDEF_H_ */
