/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件主要定义龙芯3A mailbox寄存器相关定义 
 * 
 * 修改：
 * 		 2013-08-22，张静，创建。 
 */
#include <gs464_cpu.h>

#if !defined(__HUARUI2__) && !defined(__HUARUI3__)
#define  core_group0_base    0x3ff01000

#define  core0_offset  0x0
#define  core1_offset  0x100
#define  core2_offset  0x200
#define  core3_offset  0x300

#define  STATUS0  0x0
#define  EN0      0x4
#define  SET0     0x8
#define  CLEAR0   0xc
#define  STATUS1  0x10
#define  MASK1    0x14
#define  SET1     0x18
#define  CLEAR1   0x1c
#define  BUF      0x20

#define  core0_STATUS0    core_group0_base+core0_offset+0x0             // R
#define  core0_EN0        core_group0_base+core0_offset+0x4             // R/W
#define  core0_SET0       core_group0_base+core0_offset+0x8             // W
#define  core0_CLEAR0     core_group0_base+core0_offset+0xc             // W
#define  core0_STATUS1    core_group0_base+core0_offset+0x10            // R
#define  core0_MASK1      core_group0_base+core0_offset+0x14            // R/W
#define  core0_SET1       core_group0_base+core0_offset+0x18            // W
#define  core0_CLEAR1     core_group0_base+core0_offset+0x1c            // w
#define  core0_BUF        core_group0_base+core0_offset+0x20            // R/w

#define  core1_STATUS0    core_group0_base+core1_offset+0x0             // R
#define  core1_EN0        core_group0_base+core1_offset+0x4             // R/W
#define  core1_SET0       core_group0_base+core1_offset+0x8             // W
#define  core1_CLEAR0     core_group0_base+core1_offset+0xc             // W
#define  core1_STATUS1    core_group0_base+core1_offset+0x10            // R
#define  core1_MASK1      core_group0_base+core1_offset+0x14            // R/W
#define  core1_SET1       core_group0_base+core1_offset+0x18            // W
#define  core1_CLEAR1     core_group0_base+core1_offset+0x1c            // w
#define  core1_BUF        core_group0_base+core1_offset+0x20            // R/w

#define  core2_STATUS0    core_group0_base+core2_offset+0x0             // R
#define  core2_EN0        core_group0_base+core2_offset+0x4             // R/W
#define  core2_SET0       core_group0_base+core2_offset+0x8             // W
#define  core2_CLEAR0     core_group0_base+core2_offset+0xc             // W
#define  core2_STATUS1    core_group0_base+core2_offset+0x10            // R
#define  core2_MASK1      core_group0_base+core2_offset+0x14            // R/W
#define  core2_SET1       core_group0_base+core2_offset+0x18            // W
#define  core2_CLEAR1     core_group0_base+core2_offset+0x1c            // w
#define  core2_BUF        core_group0_base+core2_offset+0x20            // R/w

#define  core3_STATUS0    core_group0_base+core3_offset+0x0             // R
#define  core3_EN0        core_group0_base+core3_offset+0x4             // R/W
#define  core3_SET0       core_group0_base+core3_offset+0x8             // W
#define  core3_CLEAR0     core_group0_base+core3_offset+0xc             // W
#define  core3_STATUS1    core_group0_base+core3_offset+0x10            // R
#define  core3_MASK1      core_group0_base+core3_offset+0x14            // R/W
#define  core3_SET1       core_group0_base+core3_offset+0x18            // W
#define  core3_CLEAR1     core_group0_base+core3_offset+0x1c            // w
#define  core3_BUF        core_group0_base+core3_offset+0x20            // R/w

#define  core_group1_base    (((get_c0_prid() & 0x0ffff) == PRID_LSN3A) ? 0x3ff01000 : 0x3ff05000)

#define  core4_offset  0x0
#define  core5_offset  0x100
#define  core6_offset  0x200
#define  core7_offset  0x300

#define  core4_STATUS0    core_group1_base+core4_offset+0x0             // R
#define  core4_EN0        core_group1_base+core4_offset+0x4             // R/W
#define  core4_SET0       core_group1_base+core4_offset+0x8             // W
#define  core4_CLEAR0     core_group1_base+core4_offset+0xc             // W
#define  core4_STATUS1    core_group1_base+core4_offset+0x10            // R
#define  core4_MASK1      core_group1_base+core4_offset+0x14            // R/W
#define  core4_SET1       core_group1_base+core4_offset+0x18            // W
#define  core4_CLEAR1     core_group1_base+core4_offset+0x1c            // w
#define  core4_BUF        core_group1_base+core4_offset+0x20            // R/w

#define  core5_STATUS0    core_group1_base+core5_offset+0x0             // R
#define  core5_EN0        core_group1_base+core5_offset+0x4             // R/W
#define  core5_SET0       core_group1_base+core5_offset+0x8             // W
#define  core5_CLEAR0     core_group1_base+core5_offset+0xc             // W
#define  core5_STATUS1    core_group1_base+core5_offset+0x10            // R
#define  core5_MASK1      core_group1_base+core5_offset+0x14            // R/W
#define  core5_SET1       core_group1_base+core5_offset+0x18            // W
#define  core5_CLEAR1     core_group1_base+core5_offset+0x1c            // w
#define  core5_BUF        core_group1_base+core5_offset+0x20            // R/w

#define  core6_STATUS0    core_group1_base+core6_offset+0x0             // R
#define  core6_EN0        core_group1_base+core6_offset+0x4             // R/W
#define  core6_SET0       core_group1_base+core6_offset+0x8             // W
#define  core6_CLEAR0     core_group1_base+core6_offset+0xc             // W
#define  core6_STATUS1    core_group1_base+core6_offset+0x10            // R
#define  core6_MASK1      core_group1_base+core6_offset+0x14            // R/W
#define  core6_SET1       core_group1_base+core6_offset+0x18            // W
#define  core6_CLEAR1     core_group1_base+core6_offset+0x1c            // w
#define  core6_BUF        core_group1_base+core6_offset+0x20            // R/w

#define  core7_STATUS0    core_group1_base+core7_offset+0x0             // R
#define  core7_EN0        core_group1_base+core7_offset+0x4             // R/W
#define  core7_SET0       core_group1_base+core7_offset+0x8             // W
#define  core7_CLEAR0     core_group1_base+core7_offset+0xc             // W
#define  core7_STATUS1    core_group1_base+core7_offset+0x10            // R
#define  core7_MASK1      core_group1_base+core7_offset+0x14            // R/W
#define  core7_SET1       core_group1_base+core7_offset+0x18            // W
#define  core7_CLEAR1     core_group1_base+core7_offset+0x1c            // w
#define  core7_BUF        core_group1_base+core7_offset+0x20            // R/w

#define  core_group1_base_ls3a	0x3ff01000
 void *mailbox_set0_regs[] = {                        	/* addr for core_set0 reg,*/
    (void *)(core_group0_base + core0_offset + SET0), 	/* which is a 32 bit reg*/
    (void *)(core_group0_base + core1_offset + SET0), 	/*When the bit of core_set0 is 1,*/
    (void *)(core_group0_base + core2_offset + SET0), 	/*the bit of core_status0 become 1 immediately*/
    (void *)(core_group0_base + core3_offset + SET0),    
    (void *)(core_group1_base_ls3a + core4_offset + SET0), 	
    (void *)(core_group1_base_ls3a + core5_offset + SET0), 	
    (void *)(core_group1_base_ls3a + core6_offset + SET0), 	
    (void *)(core_group1_base_ls3a + core7_offset + SET0),      
                      
};

 void *mailbox_clear0_regs[] = {                      		/* addr for core_clear0 reg,*/
	(void *)(core_group0_base + core0_offset + CLEAR0), 	/* which is a 32 bit reg*/
	(void *)(core_group0_base + core1_offset + CLEAR0), 	/*When the bit of core_clear0 is 1,*/
	(void *)(core_group0_base + core2_offset + CLEAR0), 	/*the bit of core_status0 become 0 immediately*/
	(void *)(core_group0_base + core3_offset + CLEAR0),
	(void *)(core_group1_base_ls3a + core4_offset + CLEAR0), 	
	(void *)(core_group1_base_ls3a + core5_offset + CLEAR0), 	
	(void *)(core_group1_base_ls3a + core6_offset + CLEAR0), 	
	(void *)(core_group1_base_ls3a + core7_offset + CLEAR0),  
	
 };

 void *mailbox_regs0[] = {                            		/* addr for core_status0 reg*/
	(void *)(core_group0_base + core0_offset + STATUS0),    /* which is a 32 bit reg*/
	(void *)(core_group0_base + core1_offset + STATUS0),    /* the reg is read only*/
	(void *)(core_group0_base + core2_offset + STATUS0),
	(void *)(core_group0_base + core3_offset + STATUS0),
	(void *)(core_group1_base_ls3a + core4_offset + STATUS0),    
	(void *)(core_group1_base_ls3a + core5_offset + STATUS0),    
	(void *)(core_group1_base_ls3a + core6_offset + STATUS0),
	(void *)(core_group1_base_ls3a + core7_offset + STATUS0),
 };

 void *mailbox_en0_regs[] = {                        	/* addr for core_set0 reg,*/
    (void *)(core_group0_base + core0_offset + EN0), 	/*which is a 32 bit reg*/
    (void *)(core_group0_base + core1_offset + EN0), 	/*When the bit of core_set0 is 1,*/
    (void *)(core_group0_base + core2_offset + EN0), 	/*the bit of core_status0 become 1  immediately */
    (void *)(core_group0_base + core3_offset + EN0),       
    (void *)(core_group1_base_ls3a + core4_offset + EN0), 	
    (void *)(core_group1_base_ls3a + core5_offset + EN0), 	
    (void *)(core_group1_base_ls3a + core6_offset + EN0), 	
    (void *)(core_group1_base_ls3a + core7_offset + EN0), 
 };


  void *mailbox_buf[] = {                                /* addr for core_buf regs*/
	(void *)(core_group0_base + core0_offset + BUF),     /* a group of regs with 0x40 byte size*/
	(void *)(core_group0_base + core1_offset + BUF),     /*  which could be used for  */
	(void *)(core_group0_base + core2_offset + BUF),     /*transfer args , r/w , uncached*/
	(void *)(core_group0_base + core3_offset + BUF),
	(void *)(core_group1_base_ls3a + core4_offset + BUF),     
	(void *)(core_group1_base_ls3a + core5_offset + BUF),    
	(void *)(core_group1_base_ls3a + core6_offset + BUF),    
	(void *)(core_group1_base_ls3a + core7_offset + BUF),
  };
#define mailbox_fixup(x, y)		(y < 4 ? x[y] : (((get_c0_prid() & 0x0ffff) == PRID_LSN3A) ? (x[y]) : (x[y] + 0x04000)))
#endif /* 0 */
