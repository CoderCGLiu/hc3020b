/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件实现了BSP模块初始化接口
 * 修改：
 * 		 1. 2011-11-17，规范BSP 
 */
#include <reworks/printk.h> 
#include <string.h>
#include <cpu.h>
#include <gs464_cpu.h>
#include "hr2_cpu.h"
#include <bsp.h>
#include <memory.h>
#include <private/boot_param.h>
#ifdef INCLUDE_UNAME
#include <osinfo.h>
#endif

#ifdef __HUARUI2__
#include "tool/hr3SysTools.h"
#endif

#include <pthread.h>
#include <stdio.h>
#include <sys/types.h>
#include "huarui3/h/drvCommon.h"
#include "vcscpumacro.h"


/* 默认按照HC3080测试板的配置(DDR0 8GB),进行MMU映射,以及初始化PCIE和SRIO.
 * HC3080B的SRIO phy初始化操作和HC3080有区别. 
 * 而HC3080 VPX应用板的PCIE是用来多片DSP进行数据通讯,不需要外接pcie设备,且需要PCIE驱动同时支持RC和EP两种模式的初始化配置.
 * 
 * 若为其他板卡,需打开如下对应的宏开关
 * */
#define _HC3080B_DEMO_MMU_ 		/*HC3080B DEMO板*/
//#define _HC3080_VPX_BOARD_MMU_ 	/*HC3080 VPX应用板*/

#if defined(_HC3080B_DEMO_MMU_)
const int hr3_board_type = 2;/*HC3080B DEMO板*/
#elif defined(_HC3080_VPX_BOARD_MMU_)
const int hr3_board_type = 1;/*HC3080 VPX应用板*/
#else
const int hr3_board_type = 0;/*HC3080 测试板*/
#endif

extern sys_cfg_tbl *sys_cfg_ptr;
/**
 * 函数声明
 */
extern void sys_hw_init();
extern void bsp_int_module_init(void);
extern void printk_debug_hw_init(void);
extern void ls3a_hw_init(void);

#ifdef INCLUDE_UNAME


#if defined(__HUARUI2__)
static MOD_INFO hr2_bsp_info;

static BOARD_INFO hr2_board_info = 
	{
			"HUARUI2",
			"mips HUARUI2",
			"4GB",
			"lltemac",
			"NANDFLASH",
			"NULL",
			"NULL",
			"NS16550",
			"NULL",
			"NULL"
	};
int hr2_bsp_info_init(void)
{
	/* 清理 */
	memset(&hr2_bsp_info, 0, sizeof(MOD_INFO));
	
	hr2_bsp_info.name			= "BSP";
//	hr2_bsp_info.version		= MODULE_VERSION;
//	hr2_bsp_info.subversion		= MODULE_SUBVERSION;
	hr2_bsp_info.builddate		= __DATE__;
	hr2_bsp_info.buildtime		= __TIME__;
	hr2_bsp_info.description	= "HUARUI2 Board Support Package";
	
	/* 注册 */
	register_module_info(&hr2_bsp_info);
	
	return 0;
}
static int hr2_board_init(void)
{
	register_board_info(&hr2_board_info);
	
	return 0;
}
#elif defined(__HUARUI3__)
static MOD_INFO hr3_bsp_info;

static BOARD_INFO hr3_board_info =
	{
			"HUARUI3 HC3020B",
			"HUARUI3 MIPS64 2-CORE SMP",
			"8GB",
			"NULL",
			"NULL",
			"NULL",
			"NULL",
			"NS16550",
			"NULL",
			"NULL"
	};
int hr3_bsp_info_init(void)
{
	/* 清理 */
	memset(&hr3_bsp_info, 0, sizeof(MOD_INFO));

	hr3_bsp_info.name			= "BSP";
//	hr3_bsp_info		= MODULE_VERSION;
//	hr3_bsp_info		= MODULE_SUBVERSION;
	hr3_bsp_info.builddate		= __DATE__;
	hr3_bsp_info.buildtime		= __TIME__;
	hr3_bsp_info.description	= "HUARUI3 Board Support Package";

	/* 注册 */
	register_module_info(&hr3_bsp_info);

	return 0;
}
static int hr3_board_init(void)
{
	register_board_info(&hr3_board_info);

	return 0;
}
#else
#error "Unknown Huarui dsp!!!!"
#endif

#endif



/*---add by ldf at 20230511------------------------------------------------------*/
#if 1
static const char mon_name[12][3] = 
{
	"Jan", "Feb", "Mar", "Apr", "May", "Jun", 
	"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
};

static char *date_to_str(char *date)
{
	char year[32];
	char mon[32];
	char day[32];
	char *buf;
	int i;
	
	if (NULL == (buf = malloc(32)))
	{
		return NULL;
	}
	memset(buf, 0, 32);
	sscanf(date, "%s %s %s", mon, day, year);
	
	for (i = 0; i < 12; i++)
	{		
		if (0 == strncmp(mon_name[i], mon, 3))
		{
			break;
		}	
	}
	
	sprintf(buf, "%s-%d-%s", year, i + 1, day);
	
	return buf;
}

void get_bsp_info(MOD_INFO *bsp_info)
{
	memcpy(bsp_info, &hr3_bsp_info, sizeof(hr3_bsp_info));
	bsp_info->builddate = date_to_str(hr3_bsp_info.builddate);
	
	printf("bsp_info.name : %s\n", bsp_info->name);
	printf("bsp_info.version : %s\n", bsp_info->version);
	printf("bsp_info.subversion : %s\n", bsp_info->subversion);
	printf("bsp_info.builddate : %s\n", bsp_info->builddate);
	printf("bsp_info.buildtime : %s\n", bsp_info->buildtime);
	printf("bsp_info.description : %s\n", bsp_info->description);
	return;
} 

void get_board_info(BOARD_INFO *board_info)  
{
	memcpy(board_info, &hr3_board_info, sizeof(hr3_board_info));
	
	printf("board_info.board : %s\n", board_info->board);
	printf("board_info.cpu : %s\n", board_info->cpu);
	printf("board_info.mem : %s\n", board_info->mem);
	printf("board_info.net : %s\n", board_info->net);
	printf("board_info.storage : %s\n", board_info->storage);
	printf("board_info.vedio : %s\n", board_info->vedio);
	printf("board_info.audio : %s\n", board_info->audio);
	printf("board_info.serial : %s\n", board_info->serial);
	printf("board_info.usb : %s\n", board_info->usb);
	printf("board_info.other : %s\n", board_info->other);
	return;
}

extern OS_INFO reworks_os_infos;
void get_os_info(OS_INFO *os_info)
{
	memcpy(os_info, &reworks_os_infos, sizeof(reworks_os_infos));
	os_info->builddate = date_to_str(reworks_os_infos.builddate);
	
	printf("os_info.osname : %s\n", os_info->osname);
	printf("os_info.version : %s\n", os_info->version);
	printf("os_info.subversion : %s\n", os_info->subversion);
	printf("os_info.releasetime : %s\n", os_info->releasetime);
	printf("os_info.builddate : %s\n", os_info->builddate);
	printf("os_info.buildtime : %s\n", os_info->buildtime);
	return;
}
#endif
/*---add end at 20230511--------------------------------------------------------------------*/



#define ROUND_UP_BSP(x, align)	(( (unsigned long long)(x) + (align - 1)) & ~(align - 1))
extern int diagnose_reworks;
extern  void mmu_add_item(u64 virtual_addr,u64 physical_addr,u64 size,u32 option);
int high_mem_flag = 0; /*是否支持高端内存*/

#if 1
#define CONFIG_MMU_ITEM(idx, vir, phy, sz, attr) {\
		mem_seg_info_array[idx].start_addr= vir; \
		mem_seg_info_array[idx].size = sz; \
		mmu_add_item(mem_seg_info_array[idx].start_addr,phy,mem_seg_info_array[idx].size, attr); \
		idx++; \
}
#else
#define CONFIG_MMU_ITEM(idx, vir, phy, sz, attr) {\
		mmu_add_item(vir,phy,sz, attr); \
		idx++; \
}
#endif
    

extern int gem_dev_num_get();
extern unsigned int gemTxDescMemSize_get();
extern unsigned int gemRxDescMemSize_get();
extern unsigned int gemTxDmaMemSize_get();
extern void* gemTxDescMemAddr_get(int unit);
extern void gemTxDescMemAddr_set(int unit, unsigned long long TxDescMemAddr);
extern void* gemRxDescMemAddr_get(int unit);
extern void gemRxDescMemAddr_set(int unit, unsigned long long RxDescMemAddr);
extern void* gemTxDmaMemAddr_get(int unit);
extern void gemTxDmaMemAddr_set(int unit, unsigned long long TxDmaMemAddr);
extern int sdio_dev_num_get();
extern unsigned int sdio_dma_mem_size_get(); 
extern void sdio_dma_read_mem_set(int controller, unsigned long long dma_read_mem); 
extern void sdio_dma_write_mem_set(int controller, unsigned long long dma_write_mem); 
extern unsigned long long sdio_dma_read_mem_get(int controller);
extern unsigned long long sdio_dma_write_mem_get(int controller);
/*******************************************************************************
 * 
 * 高端内存添加
 * 
 * 		本程序用于设置龙芯平台中高端内存的映射，同时将内存起始地址添加到数组中便于核心管理。
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		无
 */

typedef struct{
	unsigned long long vir_addr; 
	unsigned long long phy_addr;
	unsigned long long size;
	unsigned long long entrylo_opt;//Cache一致性属性
} USER_MMU_CFG;

static USER_MMU_CFG user_mmu_array[] = { /*ldf 20230825 add:: mmu配置*/
		{
				/* 1536MB DDR0, 0x9000_0000 - 0xEFFF_FFFF, cached, 加入内存管理数组*/
				.vir_addr 		= 0x90000000UL,
				.phy_addr 		= 0x90000000UL,
				.size			= 0x60000000UL,
				.entrylo_opt	= HR2_ENTRYLO_OPT(HR2_MMU_ATTR_CACHE_CACHEABLE_COHERENT, HR2_MMU_ATTR_DIRTY, \
													HR2_MMU_ATTR_VALID,HR2_MMU_ATTR_GLOBAL),
		},
		{
				/* 256MB DDR0, 0xF000_0000 - 0xFFFF_FFFF, cached, 预留的内存,用于驱动DMA之类的*/
				.vir_addr 		= 0xf0000000UL,
				.phy_addr 		= 0xf0000000UL,
				.size			= 0x10000000UL,/*256MB*/
				.entrylo_opt	= HR2_ENTRYLO_OPT(HR2_MMU_ATTR_CACHE_CACHEABLE_COHERENT, HR2_MMU_ATTR_DIRTY, \
													HR2_MMU_ATTR_VALID,HR2_MMU_ATTR_GLOBAL),
		},
#if defined(_HC3080B_DEMO_MMU_) /*HC3080B DEMO板*/
		{
				/* 6GB DDR0, 0x10_8000_0000 - 0x11_FFFF_FFFF, cached*/
				.vir_addr 		= 0x1080000000UL,
				.phy_addr 		= 0x1080000000UL,
				.size			= 0x180000000UL,
				.entrylo_opt	= HR2_ENTRYLO_OPT(HR2_MMU_ATTR_CACHE_CACHEABLE_COHERENT, HR2_MMU_ATTR_DIRTY, \
													HR2_MMU_ATTR_VALID,HR2_MMU_ATTR_GLOBAL),
		},
#elif defined(_HC3080_VPX_BOARD_MMU_) /*HC3080 VPX板*/
		{
				/* 2GB DDR0, 0x10_8000_0000 - 0x10_FFFF_FFFF, cached*/
				.vir_addr 		= 0x1080000000UL,
				.phy_addr 		= 0x1080000000UL,
				.size			= 0x80000000UL,
				.entrylo_opt	= HR2_ENTRYLO_OPT(HR2_MMU_ATTR_CACHE_CACHEABLE_COHERENT, HR2_MMU_ATTR_DIRTY, \
													HR2_MMU_ATTR_VALID,HR2_MMU_ATTR_GLOBAL),
		},
		{
				/* 4GB DDR1, 0x18_0000_0000 - 0x18_FFFF_FFFF, cached*/
				.vir_addr 		= 0x1800000000UL,
				.phy_addr 		= 0x1800000000UL,
				.size			= 0x100000000UL,
				.entrylo_opt	= HR2_ENTRYLO_OPT(HR2_MMU_ATTR_CACHE_CACHEABLE_COHERENT, HR2_MMU_ATTR_DIRTY, \
													HR2_MMU_ATTR_VALID,HR2_MMU_ATTR_GLOBAL),
		},
#else /*HC3080 测试板*/
		{
				/* 6GB DDR0, 0x10_8000_0000 - 0x11_FFFF_FFFF, cached*/
				.vir_addr 		= 0x1080000000UL,
				.phy_addr 		= 0x1080000000UL,
				.size			= 0x180000000UL,
				.entrylo_opt	= HR2_ENTRYLO_OPT(HR2_MMU_ATTR_CACHE_CACHEABLE_COHERENT, HR2_MMU_ATTR_DIRTY, \
													HR2_MMU_ATTR_VALID,HR2_MMU_ATTR_GLOBAL),
		},
#endif
		/*END*/
};

#if 1/*ldf 20230825 add*/
void mips_high_mem_add(void)
{
	int i = 0;
	unsigned long long reserve_for_driver_mem_addr = user_mmu_array[1].vir_addr;//0xf0000000
	unsigned int reserve_for_driver_mem_size = user_mmu_array[1].size;/*预留的内存，用于驱动DMA之类的*/
	
	int sdio_dev_num = sdio_dev_num_get();
	unsigned int sdio_dma_mem_size = sdio_dma_mem_size_get();
	int gem_dev_num = gem_dev_num_get();
	unsigned int gemTxDescMemSize = gemTxDescMemSize_get();
	unsigned int gemRxDescMemSize = gemRxDescMemSize_get();
	unsigned int gemTxDmaMemSize = gemTxDmaMemSize_get();
	
	
//	void * heap_area_start = (void*)free_mem_start;
//	u64 sys_mem_size_config = sys_cfg_ptr->sys_mem_size;
//	u64 aligned_mem_size = sys_mem_size_config;

	
	/*初始化数组, MAX_SEGS=6*/
	for (i = 0; i < MAX_SEGS; i++) {
		mem_seg_info_array[i].start_addr = 0;
		mem_seg_info_array[i].size = 0;
	}

	mem_seg_info_array[0].start_addr = free_mem_start;
	mem_seg_info_array[0].size = 0x10000000UL - (free_mem_start - sys_cfg_ptr->sys_mem_start);

	printk("free_mem_start=0x%lx, sys_mem_start=0x%lx, sys_mem_size=0x%x\n", free_mem_start, sys_cfg_ptr->sys_mem_start,sys_cfg_ptr->sys_mem_size);
	printk("mem_seg_info_array[0].start_addr=0x%lx, mem_seg_info_array[0].size=0x%x\n", mem_seg_info_array[0].start_addr, mem_seg_info_array[0].size);

	mem_seg_info_array[1].start_addr = user_mmu_array[0].vir_addr;
	mem_seg_info_array[1].size = user_mmu_array[0].size;
	mem_seg_info_array[2].start_addr = 0;
	mem_seg_info_array[2].size = 0;
	mem_seg_info_array[3].start_addr = 0;
	mem_seg_info_array[3].size = 0;
	mem_seg_info_array[4].start_addr = 0;
	mem_seg_info_array[4].size = 0;
		
	for(i = 0; i < (sizeof(user_mmu_array)/sizeof(user_mmu_array[0])); i++)
	{
		mmu_add_item(user_mmu_array[i].vir_addr, user_mmu_array[i].phy_addr, user_mmu_array[i].size, user_mmu_array[i].entrylo_opt);
		printk("user_mmu_vir_addr=0x%lx, user_mmu_phy_addr=0x%lx, user_mmu_size=0x%lx\n", user_mmu_array[i].vir_addr, user_mmu_array[i].phy_addr, user_mmu_array[i].size);
	}
	
	high_mem_flag = 1;/*支持高端内存*/

		
#if 1 /*ldf 20230824 add:: 用于gmac-txdesc、gmac-rxdesc、gmac-txdma、sdio-dma*/
	
	if((((gemTxDescMemSize+gemRxDescMemSize+gemTxDmaMemSize)*gem_dev_num) + (sdio_dma_mem_size*sdio_dev_num*2)) > reserve_for_driver_mem_size)
	{
		printk("<ERROR> [%s():_%d_]:: reserve_for_driver_mem is not enough!\n", __FUNCTION__, __LINE__);
		return;
	}
	printk("==> Reserve for driver mem addr: \n");
	printk("## reserve_for_driver_mem_addr=0x%lx, reserve_for_driver_mem_size=0x%lx ##\n", reserve_for_driver_mem_addr, reserve_for_driver_mem_size);
	
#if 0/*ldf 20230907 ignore:: gmac描述符以及dma的内存改为申请内存的方式*/
	/*用于gmac-rxdesc*/
	for(i = 0; i < gem_dev_num; i++)
	{
		if(gemRxDescMemSize == 0)
			break;
		reserve_for_driver_mem_addr = ROUND_UP_BSP(reserve_for_driver_mem_addr, gemRxDescMemSize);
		gemRxDescMemAddr_set(i, reserve_for_driver_mem_addr);
		reserve_for_driver_mem_addr += gemRxDescMemSize;
		printk("gemRxDescMemAddr[%d] = 0x%lx, gemRxDescMemSize=0x%x\n", i,gemRxDescMemAddr_get(i),gemRxDescMemSize);
	}
	
	/*用于gmac-txdesc*/
	for(i = 0; i < gem_dev_num; i++)
	{
		if(gemTxDescMemSize == 0)
			break;
		reserve_for_driver_mem_addr = ROUND_UP_BSP(reserve_for_driver_mem_addr, gemTxDescMemSize);
		gemTxDescMemAddr_set(i, reserve_for_driver_mem_addr);
		reserve_for_driver_mem_addr += gemTxDescMemSize;
		printk("gemTxDescMemAddr[%d] = 0x%lx, gemTxDescMemSize=0x%x\n", i,gemTxDescMemAddr_get(i),gemTxDescMemSize);
	}
	
	/*用于gmac-txdma*/
	for(i = 0; i < gem_dev_num; i++)
	{
		if(gemTxDmaMemSize == 0)
			break;
		reserve_for_driver_mem_addr = ROUND_UP_BSP(reserve_for_driver_mem_addr, gemTxDmaMemSize);
		gemTxDmaMemAddr_set(i, reserve_for_driver_mem_addr);
		reserve_for_driver_mem_addr += gemTxDmaMemSize;
		printk("gemTxDmaMemAddr[%d] = 0x%lx, gemTxDmaMemSize=0x%x\n", i,gemTxDmaMemAddr_get(i),gemTxDmaMemSize);
	}
#endif
	
	/*sdio 用于dma read的内存地址*/
	for(i = 0; i < sdio_dev_num; i++)
	{
		if(sdio_dma_mem_size == 0)
			break;
		reserve_for_driver_mem_addr = ROUND_UP_BSP(reserve_for_driver_mem_addr, 4/*sdio_dma_mem_size*/);
		sdio_dma_read_mem_set(i,  reserve_for_driver_mem_addr);
		reserve_for_driver_mem_addr += sdio_dma_mem_size;
		printk("sdio_dma_read_mem[%d] = 0x%lx, sdio_dma_mem_size = 0x%x\n", i,sdio_dma_read_mem_get(i), sdio_dma_mem_size);
	}
	
	/*sdio 用于dma write的内存地址*/
	for(i = 0; i < sdio_dev_num; i++)
	{
		if(sdio_dma_mem_size == 0)
			break;
		reserve_for_driver_mem_addr = ROUND_UP_BSP(reserve_for_driver_mem_addr, 4/*sdio_dma_mem_size*/);
		sdio_dma_write_mem_set(i,  reserve_for_driver_mem_addr);
		reserve_for_driver_mem_addr += sdio_dma_mem_size;
		printk("sdio_dma_write_mem[%d] = 0x%lx, sdio_dma_mem_size = 0x%x\n", i,sdio_dma_write_mem_get(i), sdio_dma_mem_size);
	}
#endif

}
#else
void mips_high_mem_add(void)
{
	int i = 0;
	unsigned long long reserve_for_driver_mem_addr = 0;
	unsigned int reserve_for_driver_mem_size = 0x10000000UL;/*预留256MB，用于驱动DMA之类的*/
	
	int sdio_dev_num = sdio_dev_num_get();
	unsigned int sdio_dma_mem_size = sdio_dma_mem_size_get();
	int gem_dev_num = gem_dev_num_get();
	unsigned int gemTxDescMemSize = gemTxDescMemSize_get();
	unsigned int gemRxDescMemSize = gemRxDescMemSize_get();
	unsigned int gemTxDmaMemSize = gemTxDmaMemSize_get();


	
//	void * heap_area_start = (void*)free_mem_start;
//	u64 sys_mem_size_config = sys_cfg_ptr->sys_mem_size;
//	u64 aligned_mem_size = sys_mem_size_config;

	
	/*初始化数组, MAX_SEGS=6*/
	for (i = 0; i < MAX_SEGS; i++) {
		mem_seg_info_array[i].start_addr = 0;
		mem_seg_info_array[i].size = 0;
	}


//	testc = (char*)0x70000000;
//	*testc = 'x';
//	c = *testc;
//	printk("c=%c\n",c);
//	printk("[%s][%d]\n",__FUNCTION__,__LINE__);

	mem_seg_info_array[0].start_addr = free_mem_start;
	mem_seg_info_array[0].size = 0x10000000UL - (free_mem_start - sys_cfg_ptr->sys_mem_start);

	printk("free_mem_start=0x%lx, sys_mem_start=0x%lx, sys_mem_size=0x%x\n", free_mem_start, sys_cfg_ptr->sys_mem_start,sys_cfg_ptr->sys_mem_size);
	printk("mem_seg_info_array[0].start_addr=0x%lx, mem_seg_info_array[0].size=0x%x\n", mem_seg_info_array[0].start_addr, mem_seg_info_array[0].size);

#if defined(_HC3080B_DEMO_MMU_) /*HC3080B DEMO板*/
	mem_seg_info_array[1].start_addr = 0x90000000UL;/* 1536MB DDR0, 0x9000_0000 - 0xEFFF_FFFF, cached*/
	mem_seg_info_array[1].size = 0x60000000UL;
	mem_seg_info_array[2].start_addr = 0;
	mem_seg_info_array[2].size = 0;
	mem_seg_info_array[3].start_addr = 0;
	mem_seg_info_array[3].size = 0;
	mem_seg_info_array[4].start_addr = 0;
	mem_seg_info_array[4].size = 0;
#elif defined(_HC3080_VPX_BOARD_MMU_) /*HC3080 VPX板*/
	mem_seg_info_array[1].start_addr = 0x90000000UL;/* 1536MB DDR0, 0x9000_0000 - 0xEFFF_FFFF, cached*/
	mem_seg_info_array[1].size = 0x60000000UL;
	mem_seg_info_array[2].start_addr = 0;
	mem_seg_info_array[2].size = 0;
	mem_seg_info_array[3].start_addr = 0;
	mem_seg_info_array[3].size = 0;
	mem_seg_info_array[4].start_addr = 0;
	mem_seg_info_array[4].size = 0;
#else /*HC3080 测试板*/
	mem_seg_info_array[1].start_addr = 0x90000000UL;/* 1536MB DDR0, 0x9000_0000 - 0xEFFF_FFFF, cached*/
	mem_seg_info_array[1].size = 0x60000000UL;
	mem_seg_info_array[2].start_addr = 0;
	mem_seg_info_array[2].size = 0;
	mem_seg_info_array[3].start_addr = 0;
	mem_seg_info_array[3].size = 0;
	mem_seg_info_array[4].start_addr = 0;
	mem_seg_info_array[4].size = 0;
#endif

	reserve_for_driver_mem_addr = mem_seg_info_array[1].start_addr + mem_seg_info_array[1].size;//0xf0000000

#if defined(_HC3080B_DEMO_MMU_) /*HC3080B DEMO板, DDR0 16GB*/
	/* 1536MB DDR0, 0x9000_0000 - 0xEFFF_FFFF, cached*/
	mmu_add_item(
			mem_seg_info_array[1].start_addr,
			mem_seg_info_array[1].start_addr,
			mem_seg_info_array[1].size,
			HR2_ENTRYLO_OPT(HR2_MMU_ATTR_CACHE_CACHEABLE_COHERENT, HR2_MMU_ATTR_DIRTY,HR2_MMU_ATTR_VALID,HR2_MMU_ATTR_GLOBAL));
	/* 256MB DDR0, 0xF000_0000 - 0xFFFF_FFFF, uncached, 用于gmac-txdesc、gmac-rxdesc、gmac-txdma、sdio-dma*/
	mmu_add_item(
			reserve_for_driver_mem_addr,
			reserve_for_driver_mem_addr,
			reserve_for_driver_mem_size,
			HR2_ENTRYLO_OPT(HR2_MMU_ATTR_CACHE_UNCACHED, HR2_MMU_ATTR_DIRTY,HR2_MMU_ATTR_VALID,HR2_MMU_ATTR_GLOBAL));
	
	/* 6GB DDR0, 0x10_8000_0000 - 0x11_FFFF_FFFF*/
	mmu_add_item(
			0x1080000000UL,
			0x1080000000UL,
			0x180000000UL,
			HR2_ENTRYLO_OPT(HR2_MMU_ATTR_CACHE_CACHEABLE_COHERENT, HR2_MMU_ATTR_DIRTY,HR2_MMU_ATTR_VALID,HR2_MMU_ATTR_GLOBAL));
#elif defined(_HC3080_VPX_BOARD_MMU_) /*HC3080 VPX板, DDR0 4GB, DDR1 4GB*/
	/* 1536MB DDR0, 0x9000_0000 - 0xEFFF_FFFF, cached*/
	mmu_add_item(
			mem_seg_info_array[1].start_addr,
			mem_seg_info_array[1].start_addr,
			mem_seg_info_array[1].size,
			HR2_ENTRYLO_OPT(HR2_MMU_ATTR_CACHE_CACHEABLE_COHERENT, HR2_MMU_ATTR_DIRTY,HR2_MMU_ATTR_VALID,HR2_MMU_ATTR_GLOBAL));
	/* 256MB DDR0, 0xF000_0000 - 0xFFFF_FFFF, uncached, 用于gmac-txdesc、gmac-rxdesc、gmac-txdma、sdio-dma*/
	mmu_add_item(
			reserve_for_driver_mem_addr,
			reserve_for_driver_mem_addr,
			reserve_for_driver_mem_size,
			HR2_ENTRYLO_OPT(HR2_MMU_ATTR_CACHE_UNCACHED, HR2_MMU_ATTR_DIRTY,HR2_MMU_ATTR_VALID,HR2_MMU_ATTR_GLOBAL));
	
	/* 2GB DDR0, 0x10_8000_0000 - 0x10_FFFF_FFFF*/
	mmu_add_item(
			0x1080000000UL,
			0x1080000000UL,
			0x80000000UL,
			HR2_ENTRYLO_OPT(HR2_MMU_ATTR_CACHE_CACHEABLE_COHERENT, HR2_MMU_ATTR_DIRTY,HR2_MMU_ATTR_VALID,HR2_MMU_ATTR_GLOBAL));
	/* 4GB DDR1, 0x18_0000_0000 - 0x18_FFFF_FFFF*/
	mmu_add_item(
			0x1800000000UL,
			0x1800000000UL,
			0x100000000UL,
			HR2_ENTRYLO_OPT(HR2_MMU_ATTR_CACHE_CACHEABLE_COHERENT, HR2_MMU_ATTR_DIRTY,HR2_MMU_ATTR_VALID,HR2_MMU_ATTR_GLOBAL));
#else /*HC3080 测试板, DDR0 8GB*/
	/* 1536MB DDR0, 0x9000_0000 - 0xEFFF_FFFF, cached*/
	mmu_add_item(
			mem_seg_info_array[1].start_addr,
			mem_seg_info_array[1].start_addr,
			mem_seg_info_array[1].size,
			HR2_ENTRYLO_OPT(HR2_MMU_ATTR_CACHE_CACHEABLE_COHERENT, HR2_MMU_ATTR_DIRTY,HR2_MMU_ATTR_VALID,HR2_MMU_ATTR_GLOBAL));
	/* 256MB DDR0, 0xF000_0000 - 0xFFFF_FFFF, uncached, 用于gmac-txdesc、gmac-rxdesc、gmac-txdma、sdio-dma*/
	mmu_add_item(
			reserve_for_driver_mem_addr,
			reserve_for_driver_mem_addr,
			reserve_for_driver_mem_size,
			HR2_ENTRYLO_OPT(HR2_MMU_ATTR_CACHE_UNCACHED, HR2_MMU_ATTR_DIRTY,HR2_MMU_ATTR_VALID,HR2_MMU_ATTR_GLOBAL));
	
	/* 6GB DDR0, 0x10_8000_0000 - 0x11_FFFF_FFFF*/
	mmu_add_item(
			0x1080000000UL,
			0x1080000000UL,
			0x180000000UL,
			HR2_ENTRYLO_OPT(HR2_MMU_ATTR_CACHE_CACHEABLE_COHERENT, HR2_MMU_ATTR_DIRTY,HR2_MMU_ATTR_VALID,HR2_MMU_ATTR_GLOBAL));
#endif


	high_mem_flag = 1;

	
	
#if 1 /*ldf 20230824 add:: 用于gmac-txdesc、gmac-rxdesc、gmac-txdma、sdio-dma*/
	
	if((((gemTxDescMemSize+gemRxDescMemSize+gemTxDmaMemSize)*gem_dev_num) + (sdio_dma_mem_size*sdio_dev_num*2)) > reserve_for_driver_mem_size)
	{
		printk("<ERROR> [%s():_%d_]:: reserve_for_driver_mem init error!\n", __FUNCTION__, __LINE__);
		return;
	}
	printk("==> Reserve for driver mem addr: \n");
	printk("## reserve_for_driver_mem_addr=0x%lx, reserve_for_driver_mem_size=0x%lx ##\n", reserve_for_driver_mem_addr, reserve_for_driver_mem_size);
	
	/*用于gmac-rxdesc*/
	for(i = 0; i < gem_dev_num; i++)
	{
		reserve_for_driver_mem_addr = ROUND_UP_BSP(reserve_for_driver_mem_addr, gemRxDescMemSize);
		gemRxDescMemAddr_set(i, reserve_for_driver_mem_addr);
		reserve_for_driver_mem_addr += gemRxDescMemSize;
		printk("gemRxDescMemAddr[%d] = 0x%lx, gemRxDescMemSize=0x%x\n", i,gemRxDescMemAddr_get(i),gemRxDescMemSize);
	}
	
	/*用于gmac-txdesc*/
	for(i = 0; i < gem_dev_num; i++)
	{
		reserve_for_driver_mem_addr = ROUND_UP_BSP(reserve_for_driver_mem_addr, gemTxDescMemSize);
		gemTxDescMemAddr_set(i, reserve_for_driver_mem_addr);
		reserve_for_driver_mem_addr += gemTxDescMemSize;
		printk("gemTxDescMemAddr[%d] = 0x%lx, gemTxDescMemSize=0x%x\n", i,gemTxDescMemAddr_get(i),gemTxDescMemSize);
	}
	
	/*用于gmac-txdma*/
	for(i = 0; i < gem_dev_num; i++)
	{
		reserve_for_driver_mem_addr = ROUND_UP_BSP(reserve_for_driver_mem_addr, gemTxDmaMemSize);
		gemTxDmaMemAddr_set(i, reserve_for_driver_mem_addr);
		reserve_for_driver_mem_addr += gemTxDmaMemSize;
		printk("gemTxDmaMemAddr[%d] = 0x%lx, gemTxDmaMemSize=0x%x\n", i,gemTxDmaMemAddr_get(i),gemTxDmaMemSize);
	}
	
	/*sdio 用于dma read的内存地址*/
	for(i = 0; i < sdio_dev_num; i++)
	{
		reserve_for_driver_mem_addr = ROUND_UP_BSP(reserve_for_driver_mem_addr, 4/*sdio_dma_mem_size*/);
		sdio_dma_read_mem_set(i,  reserve_for_driver_mem_addr);
		reserve_for_driver_mem_addr += sdio_dma_mem_size;
		printk("sdio_dma_read_mem[%d] = 0x%lx, sdio_dma_mem_size = 0x%x\n", i,sdio_dma_read_mem_get(i), sdio_dma_mem_size);
	}
	
	/*sdio 用于dma write的内存地址*/
	for(i = 0; i < sdio_dev_num; i++)
	{
		reserve_for_driver_mem_addr = ROUND_UP_BSP(reserve_for_driver_mem_addr, 4/*sdio_dma_mem_size*/);
		sdio_dma_write_mem_set(i,  reserve_for_driver_mem_addr);
		reserve_for_driver_mem_addr += sdio_dma_mem_size;
		printk("sdio_dma_write_mem[%d] = 0x%lx, sdio_dma_mem_size = 0x%x\n", i,sdio_dma_write_mem_get(i), sdio_dma_mem_size);
	}
#endif
	

	
    return;

}
#endif	

#if 1/*ldf 20230825 add*/
ptrdiff_t sys_phy_to_virtual(ptrdiff_t phy_addr)
{
	int i = 0;
	
//	printk("<DEBUG> [%s:__%d__]:: phy_addr=0x%x\n", __FUNCTION__,__LINE__,phy_addr);
	/*low 512MB(Unmapped UnCached)*/
	if(phy_addr>=0x0 && phy_addr<=0x1FFFFFFFUL)
		return (phy_addr + 0xFFFFFFFFA0000000UL);
	/*(Unmapped UnCached)*/
	else if(phy_addr>=0x20000000UL && phy_addr<=0x7FFFFFFFUL)
		return (phy_addr + 0x9000000000000000UL);
	/*(Unmapped UnCached)*/
	else if(phy_addr>=0x100000000UL && phy_addr<=0x88FFFFFFFUL)
		return (phy_addr + 0x9000000000000000UL);
		
	/*Mapped*/
	for(i = 0; i < (sizeof(user_mmu_array)/sizeof(user_mmu_array[0])); i++)
	{
		if((phy_addr >= user_mmu_array[i].phy_addr) && (phy_addr < (user_mmu_array[i].phy_addr + user_mmu_array[i].size)))
			return (user_mmu_array[i].vir_addr + (phy_addr - user_mmu_array[i].phy_addr));
	}

	if(i == (sizeof(user_mmu_array)/sizeof(user_mmu_array[0])))
	{
		printk("<ERROR> phy_to_virtual error %x\n",phy_addr);
		return -1;
	}
}
#else
ptrdiff_t sys_phy_to_virtual(ptrdiff_t phy_addr)
{
//	printk("<DEBUG> [%s:__%d__]:: phy_addr=0x%x\n", __FUNCTION__,__LINE__,phy_addr);
	/*low 512MB(Unmapped UnCached)*/
	if(phy_addr>=0x0 && phy_addr<=0x1FFFFFFFUL)
		return (phy_addr + 0xFFFFFFFFA0000000UL);
	/*(Unmapped UnCached)*/
	else if(phy_addr>=0x20000000UL && phy_addr<=0x7FFFFFFFUL)
		return (phy_addr + 0x9000000000000000UL);
	/*(Unmapped UnCached)*/
	else if(phy_addr>=0x100000000UL && phy_addr<=0x88FFFFFFFUL)
		return (phy_addr + 0x9000000000000000UL);

#if  defined(_HC3080B_DEMO_MMU_) /*HC3080B DEMO板, DDR0 16GB*/
	/*DDR0 1792MB (Mapped)*/
	else if(phy_addr>=0x90000000UL && phy_addr<(0x90000000UL + 0x70000000UL))
		return (phy_addr);
	/*DDR0 6GB (Mapped)*/
	else if(phy_addr>=0x1080000000UL && phy_addr<(0x1080000000UL + 0x180000000UL))
		return (phy_addr);
#elif defined(_HC3080_VPX_BOARD_MMU_) /*HC3080 VPX板, DDR0 4G, DDR1 4G*/
	/*DDR0 1792MB (Mapped)*/
	else if(phy_addr>=0x90000000UL && phy_addr<(0x90000000UL + 0x70000000UL))
		return (phy_addr);
	/*DDR0 2GB (Mapped)*/
	else if(phy_addr>=0x1080000000UL && phy_addr<(0x1080000000UL + 0x80000000UL))
		return (phy_addr);
	/*DDR1 4GB (Mapped)*/
	else if(phy_addr>=0x1800000000UL && phy_addr<(0x1800000000UL + 0x100000000UL))
		return (phy_addr);
#else /*HC3080 测试板, DDR0 8G*/
	/*DDR0 1792MB (Mapped)*/
	else if(phy_addr>=0x90000000UL && phy_addr<(0x90000000UL + 0x70000000UL))
		return (phy_addr);
	/*DDR0 6GB (Mapped)*/
	else if(phy_addr>=0x1080000000UL && phy_addr<(0x1080000000UL + 0x180000000UL))
		return (phy_addr);
#endif
	else
	{
		printk("phy_to_virtual error %x\n",phy_addr);
		return -1;
	}
}
#endif

#if 1/*ldf 20230825 add*/
ptrdiff_t sys_virtual_to_phy(ptrdiff_t virtual_addr)
{
	int i = 0;
	
//	printk("<DEBUG> [%s:__%d__]:: virtual_addr=0x%x\n", __FUNCTION__,__LINE__,virtual_addr);
	/*512MB (Unmapped Cached)*/
	if(virtual_addr>=0xFFFFFFFF80000000UL && virtual_addr<=0xFFFFFFFF9FFFFFFFUL)
		return (virtual_addr - 0xFFFFFFFF80000000UL);
	/*512MB (Unmapped UnCached)*/
	else if(virtual_addr>=0xFFFFFFFFA0000000UL && virtual_addr<=0xFFFFFFFFBFFFFFFFUL)
		return (virtual_addr - 0xFFFFFFFFA0000000UL);
	/*(Unmapped UnCached)*/
	else if(virtual_addr>=0x9000000020000000UL && virtual_addr<=0x900000007FFFFFFFUL)
		return (virtual_addr - 0x9000000000000000UL);
	/*(Unmapped Cached)*/
//	else if(virtual_addr>=0x9800000020000000UL && virtual_addr<=0x980000007FFFFFFFUL)
//		return (virtual_addr - 0x9800000000000000UL);
	/*(Unmapped UnCached)*/
	else if(virtual_addr>=0x9000000100000000UL && virtual_addr<=0x900000088FFFFFFFUL)
		return (virtual_addr - 0x9000000000000000UL);
	/*(Unmapped Cached)*/
//	else if(virtual_addr>=0x9800000100000000UL && virtual_addr<=0x980000088FFFFFFFUL)
//		return (virtual_addr - 0x9800000000000000UL);

	/*Mapped*/
	for(i = 0; i < (sizeof(user_mmu_array)/sizeof(user_mmu_array[0])); i++)
	{
		if((virtual_addr >= user_mmu_array[i].vir_addr) && (virtual_addr < (user_mmu_array[i].vir_addr + user_mmu_array[i].size)))
		{
			return (user_mmu_array[i].phy_addr + (virtual_addr - user_mmu_array[i].vir_addr));
		}
	}

	if(i == (sizeof(user_mmu_array)/sizeof(user_mmu_array[0])))
	{
		printk("<ERROR> virtual_to_phy error %x\n",virtual_addr);
		return -1;
	}

}
#else
ptrdiff_t sys_virtual_to_phy(ptrdiff_t virtual_addr)
{
//	printk("<DEBUG> [%s:__%d__]:: virtual_addr=0x%x\n", __FUNCTION__,__LINE__,virtual_addr);
	/*512MB (Unmapped Cached)*/
	if(virtual_addr>=0xFFFFFFFF80000000UL && virtual_addr<=0xFFFFFFFF9FFFFFFFUL)
		return (virtual_addr - 0xFFFFFFFF80000000UL);
	/*512MB (Unmapped UnCached)*/
	else if(virtual_addr>=0xFFFFFFFFA0000000UL && virtual_addr<=0xFFFFFFFFBFFFFFFFUL)
		return (virtual_addr - 0xFFFFFFFFA0000000UL);
	/*(Unmapped UnCached)*/
	else if(virtual_addr>=0x9000000020000000UL && virtual_addr<=0x900000007FFFFFFFUL)
		return (virtual_addr - 0x9000000000000000UL);
	/*(Unmapped Cached)*/
//	else if(virtual_addr>=0x9800000020000000UL && virtual_addr<=0x980000007FFFFFFFUL)
//		return (virtual_addr - 0x9800000000000000UL);
	/*(Unmapped UnCached)*/
	else if(virtual_addr>=0x9000000100000000UL && virtual_addr<=0x900000088FFFFFFFUL)
		return (virtual_addr - 0x9000000000000000UL);
	/*(Unmapped Cached)*/
//	else if(virtual_addr>=0x9800000100000000UL && virtual_addr<=0x980000088FFFFFFFUL)
//		return (virtual_addr - 0x9800000000000000UL);

#if  defined(_HC3080B_DEMO_MMU_) /*HC3080B DEMO板, DDR0 16GB*/
	/*DDR0 1792MB (Mapped)*/
	else if(virtual_addr>=0x90000000UL && virtual_addr<(0x90000000UL + 0x70000000UL))
		return (virtual_addr);
	/*DDR0 14GB (Mapped)*/
	else if(virtual_addr>=0x1080000000UL && virtual_addr<(0x1080000000UL + 0x180000000UL))
		return (virtual_addr);
#elif defined(_HC3080_VPX_BOARD_MMU_) /*HC3080 VPX板, DDR0 4G, DDR1 4G*/
	/*DDR0 1792MB (Mapped)*/
	else if(virtual_addr>=0x90000000UL && virtual_addr<(0x90000000UL + 0x70000000UL))
		return (virtual_addr);
	/*DDR0 2GB (Mapped)*/
	else if(virtual_addr>=0x1080000000UL && virtual_addr<(0x1080000000UL + 0x80000000UL))
		return (virtual_addr);
	/*DDR1 4GB (Mapped)*/
	else if(virtual_addr>=0x1800000000UL && virtual_addr<(0x1800000000UL + 0x100000000UL))
		return (virtual_addr);
#else /*HC3080 测试板, DDR0 8G*/
	/*DDR0 1792MB (Mapped)*/
	else if(virtual_addr>=0x90000000UL && virtual_addr<(0x90000000UL + 0x70000000UL))
		return (virtual_addr);
	/*DDR0 6GB (Mapped)*/
	else if(virtual_addr>=0x1080000000UL && virtual_addr<(0x1080000000UL + 0x180000000UL))
		return (virtual_addr);
#endif
	else 
	{
		printk("virtual_to_phy error %x\n",virtual_addr);
		return -1;
	}
}
#endif

#if 0
typedef struct addr_64
{
	u32 low;
	u32 high;
}ADDR_64;
/* 移入BSP */
u64 sys_virtual_to_phy_64(u64 virtual_addr)
{
	ADDR_64 *ptr = (ADDR_64*)&virtual_addr;
	ptr->low = sys_virtual_to_phy(ptr->low);
	return virtual_addr;
}
#endif


/*******************************************************************************
 * 查询是否有效virtual address
 * 
 * */
int is_va_valid(unsigned long address)
{
	if((address > 0xffffffff80000000) && (address < 0xffffffff90000000))
		return 1;
	
	if(high_mem_flag)//是否支持高端内存
	{

		int i = 0;
		for(i = 0; i < MAX_SEGS; i++)
		{
			if(mem_seg_info_array[i].start_addr != 0)
			{
				if((address >= mem_seg_info_array[i].start_addr) && (address <= (mem_seg_info_array[i].start_addr + (mem_seg_info_array[i].size - 1))))
				{
					return 1;	
				}				
			}
		}
	}
	
#ifdef _ONLY_USE_4G_MEM_
	
//do nothing

#else
	//非操作系统管理的内存 ddr0
	if((address >= 0x10000000UL) && (address < 0x50000000UL))
			return 1;
	
	//非操作系统管理的内存 ddr1
	if((address >= 0xff0000000UL) && (address < 0xff0000000UL + 0x100000000UL))
				return 1;
#endif
	return 0;	
}

unsigned int sys_freq_detect(void)
{

	unsigned int regValue  = 0;
	unsigned int ret = 0;
#ifdef __HUARUI2__
	/* DSP(RASP) FREQ */
	regValue = *(unsigned int *)DSP_FREQ_REG;
	switch(regValue)
	{
	case 0x00014032:
		ret = 208.33 * 1000000;
		break;
	case 0x0001203c:
		ret = 500 * 1000000;
		break;
	case 0x00012048:
		ret = 600 * 1000000;
		break;
	case 0x0001204e:
		ret = 650 * 1000000;
		break;
	case 0x00012054:
		ret = 700 * 1000000;
		break;
	case 0x0001205a:
		ret = 750 * 1000000;
		break;
	case 0x00012060:
		ret = 800 * 1000000;
		break;
	case 0x0001904c:
		ret = 844.44 * 1000000;
		break;
	case 0x00011036:
		ret = 900 * 1000000;
		break;
	case 0x00019056:
		ret = 955.56 * 1000000;
		break;
	case 0x0001103c:
		ret = 1000 * 1000000;
		break;
	case 0x00011040:
		ret = 1066.67 * 1000000;
		break;
	case 0x00011042:
		ret = 1100 * 1000000;
		break;
	case 0x00011046:
		ret = 1166.67 * 1000000;
		break;
	case 0x00011048:
		ret = 1199.88 * 1000000;
		break;
	default:
		ret = 800 * 1000000;
		break;
	}
#else
	ret = 1500 * 1000000;
#endif
	return ret;
}



#if 1
void test_loop()
{
	
	while(1)
	{
		*(volatile unsigned char*)0xbf0c0000 = 'F';
	}
}


/**
 * 切换华睿2号串口输出到相应的CPU（注意，cpuIndex不是核的ID）
 * FuKai,2018-7-10
 */
extern int sysUartSwitch(unsigned int cpuIndex);
OS_STATUS hr2_uart_switch(unsigned int cpuIndex)
{
	return sysUartSwitch(cpuIndex);	
}

/**
 * 获取华睿2号当前CPU的ID（注意，不是核的ID）
 * FuKai,2018-7-10
 */
extern unsigned int sysCpuGetID();
u32 hr2_cpu_id_get()
{
	return sysCpuGetID();
}

//ww mod 20181113 add for ClbResBrowserSvc/tool/src/nicflux.c文件中使用
int getSpeed(char name)
{
	return 1000000000;
}
#endif

/* 挂起shell 任务 */
void disableShell()
{
	pthread_t pid ;
	
	int ret = pthread_getid("shell", &pid);
	
	if(ret!= 0)
	{
		printf("Fail to find shell task.\n");
	}else
	{
		pthread_suspend(pid);
		printf("shell suspended.\n");
	}
	
	return;
}

/* 唤醒shell 任务 */
void enableShell()
{
	pthread_t pid ;
	

	int ret = pthread_getid("shell", &pid);
	
	if(ret != 0)
	{
		printf("Fail to find shell task.\n");
	}else
	{
		pthread_resume(pid);
		printf("shell resumed.\n");
	}
}

#if 0
double d_double[100]={0};
float d_float[100]={0};
int d_int[100]={0};
long d_long[100]={0};
double t_double = 1.234;
#endif

void mshow(unsigned long addr,unsigned long size,char format)
{
	unsigned long end;
	unsigned long start;
	double * data_double;
	float *  data_float;
	int * 	 data_int;
	long *   data_long;
	
	int i;
	int cnt = 0;
	
#if 0
	printf("d_double = 0x%lx\n",(unsigned long)d_double);
	printf("d_float = 0x%lx\n",(unsigned long)d_float);
	printf("d_int = 0x%lx\n",(unsigned long)d_int);
	printf("d_long = 0x%lx\n",(unsigned long)d_long);
	
	for (i = 0; i < 100; i++) {
		d_double[i] = 0.050 - 0.001 * i;
	}

	for (i = 0; i < 100; i++) {
		d_float[i] = 0.050 - 0.002 * i;
	}
	for (i = 0; i < 100; i++) {
		d_int[i] = 3000 - i*100;
	}

	for (i = 0; i < 100; i++) {
		d_long[i] = 4000 - i*100;
	}
#endif

	start = addr;
	
	
	switch (format)
	{
	case 'F':

		data_double = (double*) start;
		end = addr + size * sizeof(double);
		
		while ((unsigned long) data_double < end) {
			printf("%lf ", *data_double);
			data_double++;
			cnt++;
			if (cnt % 8 == 0)
				printf("\n");
		}
		break;

	case 'f':

		data_float = (float*) start;
		end = addr + size * sizeof(float);
		
		while ((unsigned long) data_float < end) {
			printf("%f ", *data_float);
			data_float++;
			cnt++;
			if (cnt % 8 == 0)
				printf("\n");
		}
		break;

	case 'd':

		data_int = (int*) start;
		end = addr + size * sizeof(int);
		
		while ((unsigned long) data_int < end) {
			printf("%d ", *data_int);
			data_int++;
			cnt++;
			if (cnt % 8 == 0)
				printf("\n");
		}
		break;

	case 'l':

		data_long = (long*) start;
		end = addr + size * sizeof(long);
		
		while ((unsigned long) data_long < end) {
			printf("%ld ", *data_long);
			data_long++;
			cnt++;
			if (cnt % 8 == 0)
				printf("\n");
		}
		break;

	case 'c':
		break;

	default:
		break;
	}
	
	printf("\r\n");
	
	return;
}






#if 1/*ldf 20230719 add:: 打印DDR/GMAC ErrorLogger寄存器值*/
#define DDR_ERR_LOG_REG_BASE(x)		((unsigned long long)(0x9000000000000000 | (0x1e200000 + (x)*0x80)))
#define GMAC_ERR_LOG_REG_BASE(x)	((unsigned long long)(0x9000000000000000 | (0x1e202000 + (x)*0x80)))

#define Id_CoreId 		0x00
#define Id_RevisionId 	0x04
#define FaultEn 		0x08
#define ErrVld 			0x0c
#define ErrClr 			0x10
#define ErrLog0 		0x14
#define ErrLog1 		0x18
#define ErrLog3 		0x20
#define ErrLog4 		0x24
#define ErrLog5 		0x28
#define StallEn 		0x4c

static unsigned int read_reg_u32(unsigned long long base, unsigned int offset)
{
	unsigned int val = 0;
	val = *(unsigned int*)(base + offset);
	return val;
}
void DumpErrorLoggerReg(void)
{
	int i = 0;
	unsigned long long base;
	
	/*DDR_ERR_LOG_REG*/
	printk("\n\n  ###### DDR_ERR_LOG_REG###### \n");
	for(i=0;i<4;i++)
	{
		base = DDR_ERR_LOG_REG_BASE(i);
//		printk("base = 0x%lx \n",base);
		printk("DDR%d  Id_CoreId: 0x%x\n",i,read_reg_u32(base, Id_CoreId));
		printk("DDR%d  Id_RevisionId: 0x%x\n",i,read_reg_u32(base, Id_RevisionId));
		printk("DDR%d  FaultEn: 0x%x\n",i,read_reg_u32(base, FaultEn));
		printk("DDR%d  ErrVld: 0x%x\n",i,read_reg_u32(base, ErrVld));
		printk("DDR%d  ErrClr: 0x%x\n",i,read_reg_u32(base, ErrClr));
		printk("DDR%d  ErrLog0: 0x%x\n",i,read_reg_u32(base, ErrLog0));
		printk("DDR%d  ErrLog1: 0x%x\n",i,read_reg_u32(base, ErrLog1));
		printk("DDR%d  ErrLog3: 0x%x\n",i,read_reg_u32(base, ErrLog3));
		printk("DDR%d  ErrLog4: 0x%x\n",i,read_reg_u32(base, ErrLog4));
		printk("DDR%d  ErrLog5: 0x%x\n",i,read_reg_u32(base, ErrLog5));
		printk("DDR%d  StallEn: 0x%x\n\n",i,read_reg_u32(base, StallEn));
//		usleep(10000);
	}
	printk("  ################################# \n");
	
	/*GMAC_ERR_LOG_REG*/
	printk("\n\n  ###### GMAC_ERR_LOG_REG ###### \n");
	for(i=0;i<4;i++)
	{
		base = GMAC_ERR_LOG_REG_BASE(i);
//		printk("base = 0x%lx \n",base);
		printk("GMAC%d  Id_CoreId: 0x%x\n",i,read_reg_u32(base, Id_CoreId));
		printk("GMAC%d  Id_RevisionId: 0x%x\n",i,read_reg_u32(base, Id_RevisionId));
		printk("GMAC%d  FaultEn: 0x%x\n",i,read_reg_u32(base, FaultEn));
		printk("GMAC%d  ErrVld: 0x%x\n",i,read_reg_u32(base, ErrVld));
		printk("GMAC%d  ErrClr: 0x%x\n",i,read_reg_u32(base, ErrClr));
		printk("GMAC%d  ErrLog0: 0x%x\n",i,read_reg_u32(base, ErrLog0));
		printk("GMAC%d  ErrLog1: 0x%x\n",i,read_reg_u32(base, ErrLog1));
		printk("GMAC%d  ErrLog3: 0x%x\n",i,read_reg_u32(base, ErrLog3));
		printk("GMAC%d  ErrLog4: 0x%x\n",i,read_reg_u32(base, ErrLog4));
		printk("GMAC%d  ErrLog5: 0x%x\n",i,read_reg_u32(base, ErrLog5));
		printk("GMAC%d  StallEn: 0x%x\n\n",i,read_reg_u32(base, StallEn));
//		usleep(10000);
	}
	printk("  ################################# \n\n");
}
#endif

#if 0 /*ldf 20230731 add:: 华睿3号 设置cache模式,  2-Uncached, 3-Cacheable非一致性, 6-Cacheable一致性*/
extern u32 get_c0_config(void);
extern void set_c0_config(u32 val);
void hr3_set_cache_mode(u32 val)
{
	u32 tmp = 0;
	
	tmp = get_c0_config();
	tmp &= (~0x7);
	tmp |= (val&0x7);
	set_c0_config(tmp);
}
#endif

#if 1
#define write_c0_inhibit_start0_64(val)		__write_64bit_c0_register($22, 0, val)
#define write_c0_inhibit_end0_64(val)		__write_64bit_c0_register($22, 1, val)
#define write_c0_inhibit_start1_64(val)		__write_64bit_c0_register($22, 2, val)
#define write_c0_inhibit_end1_64(val)		__write_64bit_c0_register($22, 3, val)
#define write_c0_inhibit_start2_64(val)		__write_64bit_c0_register($22, 4, val)
#define write_c0_inhibit_end2_64(val)		__write_64bit_c0_register($22, 5, val)
#define write_c0_inhibit_start3_64(val)		__write_64bit_c0_register($22, 6, val)
#define write_c0_inhibit_end3_64(val)		__write_64bit_c0_register($22, 7, val)

#define write_c0_inhibit_start4_64(val)		__write_64bit_c0_register($21, 0, val)
#define write_c0_inhibit_end4_64(val)		__write_64bit_c0_register($21, 1, val)
#define write_c0_inhibit_start5_64(val)		__write_64bit_c0_register($21, 2, val)
#define write_c0_inhibit_end5_64(val)		__write_64bit_c0_register($21, 3, val)
#define write_c0_inhibit_start6_64(val)		__write_64bit_c0_register($21, 4, val)
#define write_c0_inhibit_end6_64(val)		__write_64bit_c0_register($21, 5, val)
#define write_c0_inhibit_start7_64(val)		__write_64bit_c0_register($21, 6, val)
#define write_c0_inhibit_end7_64(val)		__write_64bit_c0_register($21, 7, val)

void hr3_set_c0_inhibit(void)/*ldf 20230801 add:: 华睿3号 加上屏蔽窗, 防止芯片分支预测到非法地址*/
{
	write_c0_inhibit_start0_64(((0x3ul << 40) | 0x00010000000ul));
	write_c0_inhibit_end0_64(0x0008ffffffful);
	
	write_c0_inhibit_start1_64(((0x3ul << 40) | 0x000100000000ul));
	write_c0_inhibit_end1_64(0x000107ffffffful);
	
	write_c0_inhibit_start2_64(((0x3ul << 40) | 0x01200000000ul));
	write_c0_inhibit_end2_64(0x0fffffffffful);
	
	write_c0_inhibit_start3_64(0);
	write_c0_inhibit_end3_64(0);
	
	write_c0_inhibit_start4_64(0);
	write_c0_inhibit_end4_64(0);
	
	write_c0_inhibit_start5_64(0);
	write_c0_inhibit_end5_64(0);
	
	write_c0_inhibit_start6_64(0);
	write_c0_inhibit_end6_64(0);
	
	write_c0_inhibit_start7_64(0);
	write_c0_inhibit_end7_64(0);
}
#endif

/*葛文博 20230818 add:: 调bsp_for_auth里注册读写接口，让授权warning不触发*/
int flash_pageio_read_ptr_null(u32 ulStartPage, u32 pPageBuff, u32 pSpareBuff){
	return 0;
}
/*葛文博 20230818 add:: 调bsp_for_auth里注册读写接口，让授权warning不触发*/
int flash_pageio_write_ptr_null(u32 ulStartPage, u32 pPageBuff, u32 pSpareBuff){
	return 0;
}


/*******************************************************************************
 * 
 * BSP模块初始化接口
 * 
 * 		本程序用于初始化BSP。BSP初始化主要包括：初始化系统硬件（系统硬件、外部中断控制器、
 * 串口设备、显示设备等），初始化printk模块（可选）。
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 */
extern void serial_putchar(char s);
extern u32 cpu_freq_probe(void);
extern u32 bsp_clicks_per_usec;

/* 放到data段 */
u64 __attribute__((section(".data"))) fw_arg0, fw_arg1, fw_arg2, fw_arg3;
int __cpu_number_map[MAX_SMP_CPUS];		/* Map physical to logical */
int __cpu_logical_map[MAX_SMP_CPUS];		/* Map logical to physical */
int nr_online_cpu_loongson;
int __cpu_type;

int bsp_module_init(void)
{
	
	/* printk模块调试模块初始化 */

	printk_init(&serial_putchar);


	printk("in bsp_module_init\n\r");  

//	testc('h');
//	testc('r');
//	testc('\r');
//	testc('\n');

	printk("[%s][%d]ebase[0x%x]\n",__FUNCTION__,__LINE__,get_c0_ebase());
	int status  = get_c0_sr();
	set_c0_sr(status|1<<22);
	set_c0_ebase(0x80000000);
	set_c0_sr(status&(~(1<<22)));
	printk("[%s][%d]c0_ebase[0x%x]\n",__FUNCTION__,__LINE__,get_c0_ebase());
	printk("[%s][%d]c0_sr[0x%x]\n",__FUNCTION__,__LINE__,get_c0_sr());
	
#if 1
//	printk("[%s][%d]\n",__FUNCTION__,__LINE__);
//	/*ldf 20230731 add:: flush l3 cache to ddr */
//	phx_write_u32(0x1e0c0080, 0x4);
//	phx_write_u32(0x1e0c0010, 0x0);
//	phx_write_u32(0x1e0c1080, 0x4);
//	phx_write_u32(0x1e0c1010, 0x0);
//	phx_write_u32(0x1e0c2080, 0x4);
//	phx_write_u32(0x1e0c2010, 0x0);
//	phx_write_u32(0x1e0c3080, 0x4);
//	phx_write_u32(0x1e0c3010, 0x0);
	printk("[%s][%d]\n",__FUNCTION__,__LINE__);
	/*ldf 20230801 add:: 华睿3号 加上屏蔽窗, 防止芯片分支预测到非法地址*/
	hr3_set_c0_inhibit();
	printk("[%s][%d]\n",__FUNCTION__,__LINE__);
#endif
	
#if 1 /*ldf 20230601 modify*/
	printk("[%s][%d]\n",__FUNCTION__,__LINE__);
	sys_cfg_ptr->sys_clock_freq = sysDspGetConfig()*1000000;
	if(sys_cfg_ptr->sys_clock_freq == 0)
	{
		sys_cfg_ptr->sys_clock_freq = sys_freq_detect();//固定返回1.5GHz
	}
	printk("[%s][%d]\n",__FUNCTION__,__LINE__);
#else
	sys_cfg_ptr->sys_clock_freq = sys_freq_detect();//固定返回1.5G
#endif
//	sys_cfg_ptr->sys_clock_freq = 1000000000/2;//我们这块板子是500M
	bsp_clicks_per_usec = sys_timestamp_freq() / 1000000;
	printk("bsp_clicks_per_usec = %u\r\n", bsp_clicks_per_usec);
	ls3a_hw_init();
	printk("[%s][%d]\n",__FUNCTION__,__LINE__);
	/* 可编程中断控制器初始化 */
	bsp_int_module_init();
	printk("[%s][%d]\n",__FUNCTION__,__LINE__);
//	/*使能64位的kernel段访问，保证能使用64位数据*/
	u32 val = get_c0_sr();
//	val |=SR_KX;
	val |=SR_UX;
	set_c0_sr(val);
	printk("[%s][%d]\n",__FUNCTION__,__LINE__);
	extern int mmu_module_init(int type);
	mmu_module_init(0);

	/* 内存MMU配置 */
	mips_high_mem_add();

	/*板卡信息*/
#ifdef INCLUDE_UNAME
#if defined(__HUARUI2__)
	hr2_bsp_info_init();
	hr2_board_init();
#elif defined(__HUARUI3__)
	hr3_bsp_info_init();
	hr3_board_init();
#endif

#endif
	
	/*葛文博 20230818 add:: 调bsp_for_auth里注册读写接口，让授权warning不触发*/
	register_flash_dev(&flash_pageio_read_ptr_null, &flash_pageio_write_ptr_null);
	
	printk(" bsp_module_init end\n\r");
	
	return 0;
}

void reboot(void)
{
	sysReset(0);
}

/*关机接口*/
void poweroff(void)
{
	printf("%s NOT support!!!\n", __FUNCTION__);

	return;
}
