#ifndef _IF_TXGBE_H_
#define _IF_TXGBE_H_


#include <reworks/printk.h>
#include "pcie_msi.h"

typedef int 		(*FUNCPTR) ();	   /* ptr to function returning int */
extern int net_job_add( void *  ,
    		int ,
    	    int ,
    	    int ,
    	    int ,
    	    int  );

#define TXGBE_DEVICE_NAME               "txgbe" 
#define TXGBE_DEVICE_NAME_LENGTH        16
#define TXGBE_MAX_DEV            		2


/* typedefs */
typedef struct end_resource
{
	boolean adr64; /* Indicator for 64-bit support */
	u32 memBaseLow; /* Base Address LOW */
	u32 memBaseHigh; /* Base Address HIGH */
	u32 flashBase; /* Base Address for FLASH */

	u16 eepromSize; /* size in unit of word (16 bit) - 64/256 */
	u16 eeprom_icw1; /* EEPROM initialization control word 1 */
	u16 eeprom_icw2; /* EEPROM initialization control word 2 */
	u8 enetAddr[6]; /* MAC address for this adaptor */

	u32 shMemBase; /* Share memory address if any */
	u32 shMemSize; /* Share memory size if any */

	u32 rxDesNum; /* RX descriptor for this unit */
	u32 txDesNum; /* TX descriptor for this unit */

	boolean useShortCable; /* TRUE if short cable used for 82544 */
	/* by default is FALSE */
	u32 usr_flags; /* user flags for this unit */
	int iniStatus; /* initialization perform status */
	
	u32		   message_addr_lo; /*ldf 20230830 add*/
	u32		   message_addr_hi; /*ldf 20230830 add*/
	u16		   message_data; /*ldf 20230830 add*/

} NET_RESOURCE;

typedef struct pci_board_resource      /* PCI_BOARD_RESOURCE */
{
	u32        pci_bus;            /* PCI Bus number */
	u32        pci_device;         /* PCI Device number */
	u32        pci_func;           /* PCI Function number */

	u32        pci_VID;          /* PCI Vendor ID */
	u32        pci_DID;          /* PCI Device ID */
	u8         revisionID;        /* PCI Revision ID */
	u32        boardType;         /* BSP-specific board type ID */

	int         irq;               /* Interrupt Request Level */
	u32        irqvec;            /* Interrupt Request vector */

	u32        bar [6];           /* PCI Base Address Registers */

	void * const  pExtended;         /* pointer to extended device info */

} PCI_BOARD_RESOURCE;

typedef struct pci_dev {
	void *	   pDrvCtrl;
	u32        pci_bus;            /* PCI Bus number */
	u32        pci_device;         /* PCI Device number */
	u32        pci_func;           /* PCI Function number */

	u32        vendor;          /* PCI Vendor ID */
	u32        device;          /* PCI Device ID */

	u32		   subsystem_vendor;
	u32		   subsystem_device;
	
	int        irq;               /* Interrupt Request Level */
	u8         msi_cap;               /* msi_cap */
	u8         msix_cap;               /* msix_cap */
	u8 		   unitNumber;
	void * 	   pRegBase;

}PCI_DEV;

#define MSB(x)	(((x) >> 8) & 0xff)	  /* most signif byte of 2-byte integer */
#define LSB(x)	((x) & 0xff)		  /* least signif byte of 2-byte integer*/
#define MSW(x) (((x) >> 16) & 0xffff) /* most signif word of 2-word integer */
#define LSW(x) ((x) & 0xffff) 		  /* least signif byte of 2-word integer*/

#define LLSB(x)	((x) & 0xff)		/* 32bit word byte/word swap macros */
#define LNLSB(x) (((x) >> 8) & 0xff)
#define LNMSB(x) (((x) >> 16) & 0xff)
#define LMSB(x)	 (((x) >> 24) & 0xff)
#define LONGSWAP(x) ((LLSB(x) << 24) | \
		     (LNLSB(x) << 16)| \
		     (LNMSB(x) << 8) | \
		     (LMSB(x)))

#define bswap16(x)      ((LSB(x) << 8) | MSB(x))
#define bswap32(x)      LONGSWAP((UINT32)(x))
#define bswap64(x)	/* not yet */

#define htobe16(x)      bswap16((x))
#define htobe32(x)      bswap32((x))
#define htobe64(x)      bswap64((x))
#define htole16(x)      ((UINT16)(x))
#define htole32(x)      ((UINT32)(x))
#define htole64(x)      ((UINT64)(x))

#define be16toh(x)      bswap16((x))
#define be32toh(x)      bswap32((x))
#define be64toh(x)      bswap64((x))
#define le16toh(x)      ((UINT16)(x))
#define le32toh(x)      ((UINT32)(x))
#define le64toh(x)      ((UINT64)(x))

#endif 
