#include <net_types.h>
#include <drv/net_board.h>
#include <udelay.h>
#include <pci/pcibus.h>
#include <drv/pci/pciConfigLib.h>
#include <endLib.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <lx_ioctl.h>
#include <irq.h>
#include "if_txgbe.h"
#include "TxgbeEnd.h"


/* Device IDs */
#define TGB_DEV_ID_SP1000                     0x1001
#define TGB_DEV_ID_WX1820                     0x2001

#define    	net_drv_lib_init 	endLibInit

#define PCI_NET_ETHERNET_CLASS \
		((PCI_CLASS_NETWORK_CTLR << 16) | (PCI_SUBCLASS_NET_ETHERNET << 8))

int txgbe_net_pci_init(u32 pciBus, /* store a PCI bus number */
		u32 pciDevice, /* store a PCI device number */
		u32 pciFunc, /* store a PCI function number */
		void * pArg /* reserved argument */
) ;

extern int  net_dev_start
(
		void *     /* device identifier */
);

extern void* net_dev_load(int  ,  NET_OBJ* (*) (char*, void*),
		char *    ,		 
		int      ,		         
		void *     );

extern NET_OBJ* txgbe_load_init(char *initString);
extern END_OBJ * tgbEndLoad(char * loadStr);

/* Default RX descriptor  */
#ifndef E1000_RXDES_NUM
#define E1000_RXDES_NUM              (0x400)
#endif

/* Default TX descriptor  */

#ifndef E1000_TXDES_NUM
#define E1000_TXDES_NUM              (0x400)
#endif

/* Default User's flags  */

/* ORed the flag of E1000_NET_JUMBO_FRAME_SUPPORT if jumbo frame needed */

#ifndef E1000_USR_FLAG
#define E1000_USR_FLAG        (0x0)
#endif /* E1000_USR_FLAG */

#define NONE (-1)
#define BAR0_64_BIT                     0x04

#ifndef INT_NUM_GET
#define INT_NUM_GET(irq)  (/*OPENPIC_VEC_EXT_BASE + */irq)
#endif 	

/* device resources */
#define E1000_MEMSIZE_CSR            (0x20000)     /* 128Kb CSR memory size */
#define E1000_MEMSIZE_FLASH          (0x80000)     /* 512Kb Flash memory size */
#define E1000_EEPROM_SZ_64           (64)          /* 64 WORD */   
#define E1000_EEPROM_SZ_256          (256)         /* 256 WORD */   

#define EM0_SHMEM_BASE             NONE
#define EM0_SHMEM_SIZE             (0)
#define EM0_RXDES_NUM              E1000_RXDES_NUM
#define EM0_TXDES_NUM              E1000_TXDES_NUM
#define EM0_USR_FLAG               E1000_USR_FLAG

#define EM1_SHMEM_BASE             NONE
#define EM1_SHMEM_SIZE             (0)
#define EM1_RXDES_NUM              E1000_RXDES_NUM
#define EM1_TXDES_NUM              E1000_TXDES_NUM
#define EM1_USR_FLAG               E1000_USR_FLAG

#define EM2_SHMEM_BASE             NONE
#define EM2_SHMEM_SIZE             (0)
#define EM2_RXDES_NUM              E1000_RXDES_NUM
#define EM2_TXDES_NUM              E1000_TXDES_NUM
#define EM2_USR_FLAG               E1000_USR_FLAG

#define EM3_SHMEM_BASE             NONE
#define EM3_SHMEM_SIZE             (0)
#define EM3_RXDES_NUM              E1000_RXDES_NUM
#define EM3_TXDES_NUM              E1000_TXDES_NUM
#define EM3_USR_FLAG               E1000_USR_FLAG


#ifdef __ENABLE_MSI__
extern s32 its_request_irq(void *);
extern void * its_create_pci_msi_device(s32,s32,s32);
extern void * its_irq_compose_msi_msg(void * ,s32);
extern void its_release_msi_msg(void *);
extern void pci_config_msi_data(s32 busno,s32 devno,s32 funcno,void * msi_data);
extern void	lpi_install_handler(s32,void *,void *);
extern s32 lpi_uninstall_handler(s32);
extern void lpi_enable_int(s32);
extern void lpi_disable_int(s32);
#else
extern s32 int_enable_pic(u32);
extern s32 int_disable_pic(u32);
#endif


/* locals */
static u32 txgbe_units = 0; /* number of ems we found */  
/* This table defined board extended resources */
static NET_RESOURCE em_resources[TXGBE_MAX_DEV+2] =
{ 
		{ 
				FALSE, NONE, NONE, NONE, E1000_EEPROM_SZ_64, 0, 0, {NONE},
				EM0_SHMEM_BASE, EM0_SHMEM_SIZE, EM0_RXDES_NUM, EM0_TXDES_NUM, FALSE,
				EM0_USR_FLAG, ERROR
		},

		{
				FALSE, NONE, NONE, NONE, E1000_EEPROM_SZ_64, 0, 0, {NONE},
				EM1_SHMEM_BASE, EM1_SHMEM_SIZE, EM1_RXDES_NUM, EM1_TXDES_NUM, FALSE,
				EM1_USR_FLAG, ERROR
		}
};

/* This table defines board PCI resources */
PCI_BOARD_RESOURCE txgbe_pci_resources[TXGBE_MAX_DEV+2] =
{ 
		{ 
				NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE,
				{ NONE, NONE, NONE, NONE, NONE, NONE },
				(void * const)(&em_resources[0])
		},

		{
				NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE,
				{ NONE, NONE, NONE, NONE, NONE, NONE },
				(void * const)(&em_resources[1])
		}
};

#define SYS_BUFF_LOAN_PRI   1
#define SYS_BUFF_LOAN_SEC   2
#define SYS_BUFF_LOAN_TER   3
#define SYS_BUFF_LOAN_QUA   4
#define SYS_BUFF_LOAN_QUI   5
#define SYS_BUFF_LOAN_SEN   6

#define LOAD_FUNC_SEN       sys_txgbe_load
#define BUFF_LOAN_SEN       SYS_BUFF_LOAN_SEN

extern struct net_drv_object * sys_txgbe_load(char *, void *);

#define LOAD_FUNC_QUI       sys_txgbe_load
#define BUFF_LOAN_QUI       SYS_BUFF_LOAN_QUI

#define LOAD_STRING ""      /* created in sys<device>End.c */
#define TBL_NET NULL
static NET_TBL_ENTRY txgbe_end_dev_tbl[TXGBE_MAX_DEV+2] =
{
		{0, LOAD_FUNC_QUI, LOAD_STRING, BUFF_LOAN_QUI, NULL},
		{1, LOAD_FUNC_QUI, LOAD_STRING, BUFF_LOAN_QUI, NULL},
		{0, TBL_NET, NULL, 0, NULL} 	/* must be last */
};

NET_OBJ * sys_txgbe_load(char * pParamStr, void * unused )
{
	NET_OBJ * pNET = NULL;
	char paramStr[NETDRV_INIT_STR_MAX];

//	printk("<**DEBUG**> [%s():_%d_]:: pParamStr: %s\n", __FUNCTION__, __LINE__,pParamStr);

	if (strlen(pParamStr) == 0) 
	{
		pNET = tgbEndLoad(pParamStr);
	} 
	else 
	{
		char * holder = NULL;
		int unit = atoi(strtok_r(pParamStr, ":", &holder));
//		printk("<**DEBUG**> [%s():_%d_]:: unit: %d, txgbe_units: %d\n", __FUNCTION__, __LINE__,unit,txgbe_units);
		/* is there a PCI resource associated with this END unit ? */
		if (unit >= txgbe_units) 
		{
			return NULL;
		}

		/* finish off the initialization parameter string */
		sprintf(paramStr, "%d",unit);
		if ((pNET = tgbEndLoad(pParamStr)) == (NET_OBJ *) NULL) 
		{
			printf("ERROR: sys_em_load fails to load em %d\n", unit);
		}
	}
	return (pNET);
}

int txgbe_sys_pci_init(u32 pci_bus, /* store a PCI bus number */
		u32 pci_device, /* store a PCI device number */
		u32 pci_func, /* store a PCI function number */
		u32 pci_VID, /* store a PCI vendor ID */
		u32 pci_DID, /* store a PCI device ID */
		u8 revisionId /* store a PCI revision ID */
)
{
	u32 boardType = 0; /* store a BSP-specific board type constant */
	u32 memBaseLo = 0; /* temporary BAR storage */
	u32 memBaseHi = 0;
	int irq = 0; /* store PCI interrupt line (IRQ) number */
	NET_RESOURCE * pReso; /* alias extended resource table */
	
	printk("<**DEBUG**> [%s():_%d_]:: [%d,%d,%d] vid:0x%x, did:0x%x, revisionId:0x%x\n", 
			__FUNCTION__, __LINE__,pci_bus,pci_device,pci_func,pci_VID,pci_DID,revisionId);

	/* number of physical units exceeded the number supported ? */
	if (txgbe_units >= TXGBE_MAX_DEV) 
	{
		return (ERROR);
	}

	if(pci_VID != PCI_VENDOR_ID_WX || 
			(pci_DID != TGB_DEV_ID_WX1820 && 
					pci_DID != TGB_DEV_ID_SP1000))
	{
		return ERROR;
	}

	pReso = (NET_RESOURCE *) (txgbe_pci_resources[txgbe_units].pExtended);

	pci_read_config_dword(pci_bus, pci_device, pci_func, PCI_CFG_BASE_ADDRESS_0,&memBaseLo);

	pReso->adr64 = ((memBaseLo & BAR0_64_BIT) == BAR0_64_BIT) ? TRUE : FALSE;

	if (pReso->adr64)
	{
		pci_read_config_dword(pci_bus, pci_device, pci_func, PCI_CFG_BASE_ADDRESS_1,&memBaseHi);
	} 
	else 
	{
		memBaseHi = 0x0;
	}

	memBaseLo &= PCI_MEMBASE_MASK;

#ifdef __ENABLE_MSI__
	//	msi_device = its_create_pci_msi_device(pci_bus,pci_device,pci_func);
	//	if(NULL!=msi_device)
	//	{
	//		irq = its_request_irq(msi_device);		
	//		msi_msg = its_irq_compose_msi_msg(msi_device,irq);
	//		if(NULL!=msi_msg)
	//		{
	//			pci_config_msi_data(pci_bus,pci_device,pci_func,msi_msg);
	//			its_release_msi_msg(msi_msg);
	//		}
	//	}
	MSI_MSG_T  msiMsg;
	if ((msi_request(&msiMsg,1) == 0))
	{
		pcie_msi_init(pci_bus,pci_device,pci_func, msiMsg.lowAddr, msiMsg.baseData);	
		irq = msiMsg.baseIntVector;
		pReso->message_addr_lo = msiMsg.lowAddr;/*ldf 2030830 add*/
		pReso->message_data = msiMsg.baseData;/*ldf 2030830 add*/
	}
#else
	//	pci_write_config_byte(pci_bus, pci_device, pci_func, PCI_CFG_DEV_INT_LINE, tmp_irq);
	/* get the device's interrupt line (IRQ) number */
	//	pci_read_config_byte(pci_bus, pci_device, pci_func, PCI_CFG_DEV_INT_LINE, &irq);

	pci_read_config_byte(pci_bus, pci_device, pci_func, PCI_CFG_DEV_INT_PIN, &irq);	
	irq = INT_LINK0 + irq - 1;	
	pci_write_config_byte(pci_bus, pci_device, pci_func, PCI_CFG_DEV_INT_LINE, irq);
	irq = INT_LINK0;
#endif

	/* update the board-specific resource tables */
	pReso->memBaseLow = memBaseLo;
	pReso->memBaseHigh = memBaseHi;

	txgbe_pci_resources[txgbe_units].irq = irq;
	txgbe_pci_resources[txgbe_units].irqvec = INT_NUM_GET(irq);

	printk("<**DEBUG**> [%s():_%d_]:: [%d,%d,%d] memBaseLow: 0x%lx, memBaseHigh: 0x%lx, irq: %d, irqvec: %d\n", \
			__FUNCTION__, __LINE__,pci_bus,pci_device,pci_func,pReso->memBaseLow, pReso->memBaseHigh, \
			txgbe_pci_resources[txgbe_units].irq, txgbe_pci_resources[txgbe_units].irqvec);
	
	txgbe_pci_resources[txgbe_units].pci_VID = pci_VID;
	txgbe_pci_resources[txgbe_units].pci_DID = pci_DID;
	txgbe_pci_resources[txgbe_units].revisionID = revisionId;
	txgbe_pci_resources[txgbe_units].boardType = boardType;

	/* the following support legacy interfaces and data structures */
	txgbe_pci_resources[txgbe_units].pci_bus = pci_bus;
	txgbe_pci_resources[txgbe_units].pci_device = pci_device;
	txgbe_pci_resources[txgbe_units].pci_func = pci_func;

	/* enable mapped memory and IO decoders */
	pci_write_config_word(pci_bus, pci_device, pci_func, PCI_CFG_COMMAND,
			PCI_CMD_MEM_ENABLE | PCI_CMD_IO_ENABLE | PCI_CMD_MASTER_ENABLE |
			PCI_CMD_PERR_ENABLE | PCI_CMD_SERR_ENABLE);

	/* disable sleep mode */
	pci_write_config_byte(pci_bus, pci_device, pci_func, PCI_CFG_MODE, SLEEP_MODE_DIS);


	++txgbe_units; /* increment number of units initialized */

	return (OK);
}

int txgbe_net_pci_init(u32 pci_bus, /* store a PCI bus number */
		u32 pci_device, /* store a PCI device number */
		u32 pci_func, /* store a PCI function number */
		void * pArg /* reserved argument */
) 
{
	u32 classCodeReg; /* store a 24-bit PCI class code */

	pci_read_config_dword(pci_bus, pci_device, pci_func, PCI_CFG_REVISION, &classCodeReg);

	if (((classCodeReg >> 8) & 0x00ffffff) == PCI_NET_ETHERNET_CLASS) //判定该设备是否是以太网网卡设备
	{
		u32 pci_VID; /* store a PCI vendor ID */
		u32 pci_DID; /* store a PCI device ID */
		u8 revisionId; /* store a PCI revision ID */    
		/* get the PCI Vendor and Device IDs */
		pci_read_config_dword(pci_bus, pci_device, pci_func, PCI_CFG_VENDOR_ID, &pci_VID);		

		/* get the PCI Revision ID */
		pci_read_config_byte(pci_bus, pci_device, pci_func, PCI_CFG_REVISION, &revisionId);

		/* test for driver support of the specified Ethernet function */
		pci_DID = ((pci_VID >> 16) & 0x0000ffff);
		pci_VID = (pci_VID & 0x0000ffff);
		if (txgbe_sys_pci_init(pci_bus, pci_device, pci_func, pci_VID,
				pci_DID, revisionId) == OK) 
			return OK;
	}
	return (OK);
}

int usr_txgbe_net_lib_init() {

	int count;
	NET_TBL_ENTRY * pDevTbl;
	void * pCookie = NULL;

	for (count = 0, pDevTbl = txgbe_end_dev_tbl; pDevTbl->net_load_func != TBL_NET; pDevTbl++, count++) {

		/* Make sure that WDB has not already installed the device. */
		if (!pDevTbl->processed) {
			pCookie = net_dev_load(pDevTbl->unit, pDevTbl->net_load_func,
					pDevTbl->net_load_string, pDevTbl->net_loan, pDevTbl->pBSP);
			if (pCookie == NULL) {
			} else {
				if (net_dev_start(pCookie) == ERROR) {
				} else {
					pDevTbl->processed = TRUE;
				}
			}
		}
	}
	return (OK);
}


static void pci_netdevice_load(u32 class, void (*load_func)(u32, u32, u32, void *), void * arg)
{
	u32 index;
	int busno,devno,funcno;
	u16 vendor,devid;

	index = 0;
	do{
		if(0 == pci_find_class(class, index, &busno, &devno, &funcno))
		{
			pci_read_config_word (busno, devno, funcno, PCI_VENDOR_ID_REG, &vendor);
			pci_read_config_word (busno, devno, funcno, PCI_DEVICE_ID_REG, &devid);
			if(PCI_VENDOR_ID_WX == vendor && TGB_DEV_ID_WX1820 == devid)
				load_func(busno, devno, funcno, arg);
		}
		index++;
	}while(index < 4);

}

static int txgbe_net_drv_init_flag = 0;

void txgbe_net_drv_init(void)
{
	if(txgbe_net_drv_init_flag == 1)
		return ;

	printk("\r\nProbe WangXun Sapphire 10GbE END Driver\r\n");
	net_drv_lib_init();                      
	pci_netdevice_load(0x020000, (void *)txgbe_net_pci_init, NULL);
	usr_txgbe_net_lib_init();
	txgbe_net_drv_init_flag = 1;
}


static int usr_txgbe_net_lib_init2(u32 unit) 
{
	NET_TBL_ENTRY * pDevTbl;
	void * pCookie = NULL;
	if(unit >= TXGBE_MAX_DEV) return -1;
	pDevTbl = &txgbe_end_dev_tbl[unit];
	if(pDevTbl->net_load_func == TBL_NET) return -1;
	/* Make sure that WDB has not already installed the device. */
	if (!pDevTbl->processed) {
		pCookie = net_dev_load(pDevTbl->unit, pDevTbl->net_load_func,
				pDevTbl->net_load_string, pDevTbl->net_loan, pDevTbl->pBSP);
		if (pCookie == NULL) {

		} else {
			if (net_dev_start(pCookie) == ERROR) {

			} else {
				pDevTbl->processed = TRUE;
				//				++ drv_init[unit];
				return (OK);
			}
		}
	}
	else
	{
		return (OK);
	}
	return (ERROR);
}

void txgbe_net_drv_init2(u32 unit)
{
	if(txgbe_net_drv_init_flag == 0)
	{
		printk("Probe WangXun Sapphire 10GbE END Driver\n");
		net_drv_lib_init();                      
		pci_netdevice_load(0x020000, (void *)txgbe_net_pci_init, NULL);
		txgbe_net_drv_init_flag = 1;
	}

	usr_txgbe_net_lib_init2(unit);

}


void printk_hex_data_send(unsigned char *dat,unsigned int len)
{
	//	if(len==60)
	return;
	int i = 0;
	printk("send len = %d\n",len);	//return;
	for(i = 0;i < len;i++)
	{
		if((i+1)%32==0)
		{
			printk("%02X\n",dat[i]);
		}
		else
		{
			printk("%02X ",dat[i]);
		}
	}
	printk("\n");
}

void printk_hex_data_rev(unsigned char *dat,unsigned int len)
{
	//	if(len==60)
	return;
	int i = 0;
	printk("rev len = %d\n",len);//return;
	for(i = 0;i < len;i++)
	{
		if((i+1)%32==0)
		{
			printk("%02X\n",dat[i]);
		}
		else
		{
			printk("%02X ",dat[i]);
		}
	}
	printk("\n");
}
