/*
 * WangXun 10 Gigabit PCI Express End Driver
 * Copyright (c) 2015 - 2017 Beijing WangXun Technology Co., Ltd.
 */
#include <vxWorks.h>
#include <intLib.h>
#include <logLib.h>
#include <muxLib.h>
#include <drv/netBufLib.h>
#include <semLib.h>
#include <sysLib.h>
#include <wdLib.h>
#include <etherMultiLib.h>
#include <drv/net_board_lib.h>
#include <drv/vxcompat/end.h>
#include <drv/netLib.h>
#include <drv/netBufLib.h>
#include <net/mbuf.h>
//#define END_MACROS
#include <drv/vxcompat/endLib.h>
#include <vxAtomicLib.h>
#include <lstLib.h>
//#include <bsp_head.h>
#include <cacheLib.h>
#include "TxgbeEnd.h"
#include "if_txgbe.h"
#include <pci/pcibus.h>
#include <drv/pci/pciConfigLib.h>
#include <endMedia.h>
#include <udelay.h>
#include <string.h>
#include <stdlib.h>
#include <clock.h>
#include <irq.h>
#include <memory.h>


//#define _TXGBE_DBG_ /*ldf 20230828 add:: debug*/

static void DumpUint8(const unsigned char *pAddr, unsigned int length)
{
    int index = 0;

    printk("pAddr: 0x%016lX, length: %lu = 0x%016lX", (unsigned long)pAddr, length, length);
    for (index = 0; index < length; index++)
    {
        if (index % 0x10 == 0)
        {
        	printk("\n0x%08lX: ", index);
        }
        printk("0x%02X ", *(unsigned char *)(pAddr + index));
    } 

    printk("\n");
}

#define MEM_BARRIER_RW1() __asm volatile ("sync" ::: "memory")/*ldf 20230904 add*/


#ifdef  CACHE_PIPE_FLUSH
#undef  CACHE_PIPE_FLUSH
#endif

#define CACHE_PIPE_FLUSH()

#ifndef  UNUSED
#define  UNUSED   __attribute__((__unused__))
#endif

#ifndef  FOREVER
#define  FOREVER   for(;;)
#endif

#ifndef  LOCAL
#define  LOCAL   static
#endif

#ifndef  IMPORT
#define  IMPORT   extern
#endif



#define vxbUsDelay(x) udelay(x)
#define vxbMsDelay(x) pthread_delay((x)*sys_clk_rate_get()/1000+1)   //vxbUsDelay(1000 * x)

#if 1
#define cacheDmaMallocEnd   kmalloc
#define cacheDmaFree        kfree
#else
//#define cacheDmaMallocEnd   malloc
//#define cacheDmaFree        free
#endif
extern void * arch_dma_malloc(size_t size);
extern void arch_dma_free(void * addr);

#ifdef __ENABLE_MSI__
extern s32 its_request_irq(void *);
extern void * its_create_pci_msi_device(s32,s32,s32);
extern void * its_irq_compose_msi_msg(void * ,s32);
extern void its_release_msi_msg(void *);
extern void pci_config_msi_data(s32 busno,s32 devno,s32 funcno,void * msi_data);
extern void	lpi_install_handler(s32,void *,void *);
extern s32 lpi_uninstall_handler(s32);
extern void lpi_enable_int(s32);
extern void lpi_disable_int(s32);
#else
extern s32 int_enable_pic(u32);
extern s32 int_disable_pic(u32);
#endif
extern void gic_set_edged_trig(unsigned int irq);//设置gic中断边沿触发

IMPORT FUNCPTR _func_m2PollStatsIfPoll;

/* KR-KX-KX4 methods */
LOCAL u32  tgb_rd32_epcs(PCI_DEV * pDev, u32 addr);
LOCAL void    tgb_wr32_ephy(PCI_DEV * pDev, u32 addr, u32 data);
LOCAL void    tgb_wr32_epcs(PCI_DEV * pDev, u32 addr, u32 data);

LOCAL int     tgbSetupMacLink(PCI_DEV * pDev, u32 speed);
LOCAL int     tgb_set_link_to_kr(PCI_DEV * pDev, BOOL autoneg);
LOCAL int     tgb_set_link_to_kx4(PCI_DEV * pDev, BOOL autoneg);
LOCAL int     tgb_set_link_to_kx(PCI_DEV * pDev, u32 speed, BOOL autoneg);

/* VxBus methods */

LOCAL void    tgbInstInit (PCI_DEV *);
LOCAL void    tgbInstInit2 (PCI_DEV *);
LOCAL void    tgbInstConnect (PCI_DEV *);
LOCAL STATUS  tgbInstUnlink (PCI_DEV *, void *);

/* mux methods */

LOCAL void    tgbMuxConnect (PCI_DEV *, void *);

/* Driver utility functions */

LOCAL STATUS       tgbReset (PCI_DEV *);
LOCAL void         tgbIvarWxSet (PCI_DEV *);
int  tgbCheckFlashLoad(PCI_DEV *, u32);

/* END functions */

END_OBJ *    tgbEndLoad (char *);
LOCAL STATUS       tgbEndUnload (END_OBJ *);
LOCAL int          tgbEndIoctl (END_OBJ *, int, caddr_t);
LOCAL STATUS       tgbEndMCastAddrAdd (END_OBJ *, char *);
LOCAL STATUS       tgbEndMCastAddrDel (END_OBJ *, char *);
LOCAL STATUS       tgbEndMCastAddrGet (END_OBJ *, MULTI_TABLE *);
LOCAL void         tgbEndHashTblPopulate (TGB_DRV_CTRL *);
LOCAL STATUS       tgbEndStatsDump (TGB_DRV_CTRL *);
LOCAL void         tgbEndRxConfig (TGB_DRV_CTRL *);
LOCAL STATUS       tgbEndStart (END_OBJ *);
LOCAL STATUS       tgbEndStop (END_OBJ *);
LOCAL int          tgbEndSend (END_OBJ *, M_BLK_ID );
LOCAL void         tgbEndTbdClean (TGB_DRV_CTRL *);
LOCAL int          tgbEndEncap (TGB_DRV_CTRL *, M_BLK_ID );
LOCAL STATUS       tgbEndPollSend (END_OBJ *, M_BLK_ID );
LOCAL int          tgbEndPollReceive (END_OBJ *, M_BLK_ID );
LOCAL void         tgbEndInt (TGB_DRV_CTRL *);
LOCAL int          tgbEndRxHandle (TGB_DRV_CTRL *);
LOCAL void         tgbEndTxHandle (TGB_DRV_CTRL *);
LOCAL void         tgbEndIntHandle (void *);

int tgbGetLinkCapabilities(PCI_DEV * pDev, u32 *speed, BOOL *autoneg);
int tgbCheckMacLink(PCI_DEV * pDev, u32 *speed, BOOL *link_up);

/*temporary*/
PCI_DEV * pDev1;
PCI_DEV * pDev2;

TGB_DRV_CTRL * pgDrvCtrl[4];

static unsigned long long gTxgbeIntCount[2] = {0};/*中断总次数统计*/
static unsigned long long gTxgbeIntHandleCount[2] = {0};/*中断处理次数统计*/
static unsigned long long gTxgbeLinkIntCount[2] = {0};//link中断次数
static unsigned long long gTxgbeRxHandleCount[2] = {0};//实际的接收处理次数
static unsigned long long gTxgbeSendCount[2] = {0};//实际的发送次数
static unsigned long long gTxgbeTxHandleCount[2] = {0};//实际的发送处理的次数

LOCAL NET_FUNCS tgbNetFuncs =
{
		tgbEndStart,                    /* start func. */
		tgbEndStop,                     /* stop func. */
		tgbEndUnload,                   /* unload func. */
		tgbEndIoctl,                    /* ioctl func. */
		tgbEndSend,                     /* send func. */
		tgbEndMCastAddrAdd,             /* multicast add func. */
		tgbEndMCastAddrDel,             /* multicast delete func. */
		tgbEndMCastAddrGet,             /* multicast get fun. */
		tgbEndPollSend,                 /* polling send func. */
		tgbEndPollReceive,              /* polling receive func. */
		endEtherAddressForm,            /* put address info into a NET_BUFFER */
		endEtherPacketDataGet,          /* get pointer to data in NET_BUFFER */
		endEtherPacketAddrGet           /* Get packet addresses */
};

extern PCI_BOARD_RESOURCE txgbe_pci_resources[TXGBE_MAX_DEV];

/*****************************************************************************
 *
 * tgbInstInit - VxBus instInit handler
 *
 * This function implements the VxBus instInit handler for an tgb
 * device instance. The only thing done here is to select a unit
 * number for the device.
 *
 * RETURNS: N/A
 *
 * ERRNO: N/A
 */

UNUSED LOCAL void tgbInstInit
(
		PCI_DEV * pDev
)
{
	return;
}



/*****************************************************************************
 *
 * tgbInstInit2 - VxBus instInit2 handler
 *
 * This function implements the VxBus instInit2 handler for an tgb
 * device instance. Once we reach this stage of initialization, it's
 * safe for us to allocate memory, so we can create our pDrvCtrl
 * structure and do some initial hardware setup. The important
 * steps we do here are to connect our ISR to our assigned interrupt
 * vector, read the station address from the EEPROM, and set up our
 * vxbDma tags and memory regions.
 *
 * RETURNS: N/A
 *
 * ERRNO: N/A
 */

LOCAL void tgbInstInit2
(
		PCI_DEV * pDev
)
{
	TGB_DRV_CTRL * pDrvCtrl;
	UINT16 devId;
	UINT16 vendorId;
	UINT16 subsystemId;
	UINT8 revisionId;
	u32 media;
	u32 rar_high;
	u32 rar_low;
	int i;

	UNUSED UINT8 busNo = 0;
	UNUSED UINT8 deviceNo = 0;
	UNUSED UINT16 _devId;
	UNUSED UINT16 _vendorId;
	UNUSED UINT8 devCapID;
	UNUSED UINT8 setflags = 0;
	UNUSED int busNumber;
	UNUSED int deviceNumber;
	UNUSED int funcNumber;

	pDrvCtrl = kmalloc (sizeof(TGB_DRV_CTRL));
	if(!pDrvCtrl)
	{
		printk(" %s:%d  kmalloc fail\r\n",__func__,__LINE__);
		return;
	}

	bzero ((char *)pDrvCtrl, sizeof(TGB_DRV_CTRL));
	pDev->pDrvCtrl = pDrvCtrl;
	pDrvCtrl->tgbDev = pDev;
	pDrvCtrl->tgbBar = pDev->pRegBase;


	/* create mutex to protect critical sections */
	pDrvCtrl->tgbDevSem = semMCreate (SEM_Q_PRIORITY | SEM_DELETE_SAFE | SEM_INVERSION_SAFE);
	if(!pDrvCtrl->tgbDevSem)
	{
		printk(" %s:%d  semMCreate fail\r\n",__func__,__LINE__);
		return;
	}

//	printk("<**DEBUG**> [%s():_%d_]\n", __FUNCTION__, __LINE__);
	
#ifdef VXB_VER_4_0_0
	VXB_PCI_BUS_CFG_READ (pDev, PCI_CFG_DEVICE_ID, 2, devId);
	VXB_PCI_BUS_CFG_READ (pDev, PCI_CFG_VENDOR_ID, 2, vendorId);
	VXB_PCI_BUS_CFG_READ (pDev, PCI_CFG_SUB_SYSTEM_ID, 2, subsystemId);
	VXB_PCI_BUS_CFG_READ (pDev, PCI_CFG_REVISION, 1, revisionId);
#else
	{
		pci_read_config_word(pDev->pci_bus,pDev->pci_device,pDev->pci_func, PCI_CFG_DEVICE_ID, &devId);
		pci_read_config_word(pDev->pci_bus,pDev->pci_device,pDev->pci_func, PCI_CFG_VENDOR_ID, &vendorId);
		pci_read_config_word(pDev->pci_bus,pDev->pci_device,pDev->pci_func, PCI_CFG_SUB_SYSTEM_ID, &subsystemId);
		pci_read_config_byte(pDev->pci_bus,pDev->pci_device,pDev->pci_func, PCI_CFG_REVISION, &revisionId);
	}
#endif

	pDrvCtrl->subSystemId = subsystemId;
	pDrvCtrl->revisionId = revisionId;


	/* set max request size to 0x2000 on FT */
	/*test passed,can find the device*/
	//    pciFindDevice ((int)vendorId, (int)devId, 0, &busNumber, &deviceNumber, &funcNumber);
	//    logMsg("tgbInstInit2: busNumber is %d,deviceNumber is %d ,funcNumber is %d\n", 
	//           busNumber, deviceNumber, funcNumber,0,0,0);

	/* test if the devID is correct */
	pDrvCtrl->tgbDevType = TGB_DEVTYPE_SP;
	pDrvCtrl->lanId = (TXGBE_CSR_READ_4(pDev, TGB_CFG_PORT_ST) & 0x00000300U) >>8;

	printk("<**DEBUG**> [%s():_%d_]:: [txgbe%d] lanId = %d\n", __FUNCTION__, __LINE__, pDev->unitNumber, pDrvCtrl->lanId);

	if ((TXGBE_CSR_READ_4(pDev, TGB_CFG_PORT_ST) & TGB_CFG_PORT_ST_READ_LAN_ID))
		pDev2 = pDev;
	else
		pDev1 = pDev;

	/* Get the station address */
	TXGBE_CSR_WRITE_4(pDev, TGB_PSR_MAC_SWC_IDX, 0);

	/* Get mac addr */
	rar_high = TXGBE_CSR_READ_4(pDev, TGB_PSR_MAC_SWC_AD_H);
	rar_low = TXGBE_CSR_READ_4(pDev, TGB_PSR_MAC_SWC_AD_L);
	pDrvCtrl->tgbAddrHi = rar_high;
	pDrvCtrl->tgbAddrLo = rar_low;

	for (i = 0; i < 2; i++)
		pDrvCtrl->tgbAddr[i] = (UINT8)(rar_high >> (1 - i) * 8);

	for (i = 0; i < 4; i++)
		pDrvCtrl->tgbAddr[i + 2] = (UINT8)(rar_low >> (3 - i) * 8);
	
#if 1	/*ldf 20230828 add:: debug*/
//	printk("<**DEBUG**> [%s():_%d_]:: [txgbe%d]  pDrvCtrl->tgbAddr[5]=0x%x\n", __func__, __LINE__,pDev->unitNumber,pDrvCtrl->tgbAddr[5]);
//	pDrvCtrl->tgbAddr[5] += pDev->unitNumber;
	/*dump mac addr*/
	printk("<INFO> [%s():_%d_]:: [txgbe%d] MAC  ", __func__, __LINE__,pDev->unitNumber);
	for (i = 0; i < 6; i++)
	{
		printk("%02x", pDrvCtrl->tgbAddr[i]);
		if(i != 5)
			printk(":");
		else
			printk("\n");
	}
#endif

	/* Do lan reset */
	tgbReset (pDev);

	/* fiber support */
	switch (devId)
	{
	case TGB_DEV_ID_SP1000:
		media = IFM_10G_SR;
		break;
	case TGB_DEV_ID_WX1820:
		media = IFM_10G_SR;
		break;
	default:
		media = IFM_10G_SR;
		break;
	}

	/*
	 * We don't bother using miiBus for this adapter, since it
	 * doesn't really have an MII-compatible link management interface.
	 * Also, there's really only one supported mode.
	 */
	pDrvCtrl->tgbCurStatus = IFM_AVALID;
	pDrvCtrl->tgbCurMedia = IFM_ETHER | IFM_FDX | media;
	pDrvCtrl->tgbMediaList = kmalloc(sizeof(END_MEDIALIST) + sizeof(u32));
	pDrvCtrl->tgbMediaList->endMediaList[0] = pDrvCtrl->tgbCurMedia;
	pDrvCtrl->tgbMediaList->endMediaListLen = 1;
	pDrvCtrl->tgbMediaList->endMediaListDefault = pDrvCtrl->tgbCurMedia;

	i = -1;//vxbInstParamByNameGet (pDev, "jumboEnable", VXB_PARAM_INT32, &val);

	if (i != OK)
	{
		i = TGB_CLSIZE;
		pDrvCtrl->tgbMaxMtu = TGB_MTU;
	}
	else
	{
		i = END_JUMBO_CLSIZE;
		pDrvCtrl->tgbMaxMtu = TGB_JUMBO_MTU;
	}

#ifdef TGB_VXB_DMA_BUF
	pDrvCtrl->tgbParentTag = vxbDmaBufTagParentGet (pDev, 0);

	/* Create tag for RX descriptor ring. */
	pDrvCtrl->tgbRxDescTag = vxbDmaBufTagCreate (pDev,
			pDrvCtrl->tgbParentTag,         /* parent */
			256 /*_CACHE_ALIGN_SIZE*/,      /* alignment */
			0,                              /* boundary */
			0xFFFFFFFF,                     /* lowaddr */
			0xFFFFFFFF,                     /* highaddr */
			NULL,                           /* filter */
			NULL,                           /* filterarg */
			sizeof(TGB_ADV_RDESC) * TGB_RX_DESC_CNT,      /* max size */
			1,                              /* nSegments */
			sizeof(TGB_ADV_RDESC) * TGB_RX_DESC_CNT,      /* max seg size */
			VXB_DMABUF_ALLOCNOW|VXB_DMABUF_NOCACHE,            /* flags */
			NULL,                           /* lockfunc */
			NULL,                           /* lockarg */
			NULL);                          /* ppDmaTag */

	pDrvCtrl->tgbRxDescMem = vxbDmaBufMemAlloc (pDev,
			pDrvCtrl->tgbRxDescTag, NULL, 0, &pDrvCtrl->tgbRxDescMap);

	/* Create tag for TX descriptor ring. */
	pDrvCtrl->tgbTxDescTag = vxbDmaBufTagCreate (pDev,
			pDrvCtrl->tgbParentTag,       /* parent */
			256 /*_CACHE_ALIGN_SIZE*/,              /* alignment */
			0,                              /* boundary */
			0xFFFFFFFF,                     /* lowaddr */
			0xFFFFFFFF,                     /* highaddr */
			NULL,                           /* filter */
			NULL,                           /* filterarg */
			sizeof(TGB_ADV_TDESC) * TGB_TX_DESC_CNT,      /* max size */
			1,                              /* nSegments */
			sizeof(TGB_ADV_TDESC) * TGB_TX_DESC_CNT,      /* max seg size */
			VXB_DMABUF_ALLOCNOW|VXB_DMABUF_NOCACHE,            /* flags */
			NULL,                           /* lockfunc */
			NULL,                           /* lockarg */
			NULL);                          /* ppDmaTag */

	pDrvCtrl->tgbTxDescMem = vxbDmaBufMemAlloc (pDev,
			pDrvCtrl->tgbTxDescTag, NULL, 0, &pDrvCtrl->tgbTxDescMap);

	/* setup isb resources */
	pDrvCtrl->tgbIsbTag = vxbDmaBufTagCreate (pDev,
			pDrvCtrl->tgbParentTag,       /* parent */
			256 /*_CACHE_ALIGN_SIZE*/,              /* alignment */
			0,                              /* boundary */
			0xFFFFFFFF,                     /* lowaddr */
			0xFFFFFFFF,                     /* highaddr */
			NULL,                             /* filter */
			NULL,                             /* filterarg */
			sizeof(u32) * TGB_ISB_MAX,       /* max size */
			1,                              /* nSegments */
			sizeof(u32) * TGB_ISB_MAX,       /* max seg size */
			VXB_DMABUF_ALLOCNOW|VXB_DMABUF_NOCACHE,            /* flags */
			NULL,                             /* lockfunc */
			NULL,                             /* lockarg */
			NULL);                          /* ppDmaTag */

	pDrvCtrl->isb_mem = vxbDmaBufMemAlloc (pDev,
			pDrvCtrl->tgbIsbTag, NULL, 0, &pDrvCtrl->tgbIsbMap);

	pDrvCtrl->tgbMblkTag = vxbDmaBufTagCreate (pDev,
			pDrvCtrl->tgbParentTag,       /* parent */
			1,                            /* alignment */
			0,                            /* boundary */
			0xFFFFFFFF,                   /* lowaddr */
			0xFFFFFFFF,                   /* highaddr */
			NULL,                         /* filter */
			NULL,                         /* filterarg */
			i,                            /* max size */
			TGB_MAXFRAG,                  /* nSegments */
			i,                            /* max seg size */
			VXB_DMABUF_ALLOCNOW,          /* flags */
			NULL,                         /* lockfunc */
			NULL,                         /* lockarg */
			NULL);                        /* ppDmaTag */

	for (i = 0; i < TGB_TX_DESC_CNT; i++)
	{
		if (vxbDmaBufMapCreate (pDev, pDrvCtrl->tgbMblkTag, 0,
				&pDrvCtrl->tgbTxMblkMap[i]) == NULL)
			logMsg("create Tx map %d failed\n", i, 0,0,0,0,0);
	}

	for (i = 0; i < TGB_RX_DESC_CNT; i++)
	{
		if (vxbDmaBufMapCreate (pDev, pDrvCtrl->tgbMblkTag, 0,
				&pDrvCtrl->tgbRxMblkMap[i]) == NULL)
			logMsg("create Rx map %d failed\n", i, 0,0,0,0,0);
	}

#else /* not TGB_VXB_DMA_BUF */
	/*
	 * Note, cacheDmaMalloc() is guaranteed to return a cache-aligned
	 * buffer only when the cache is enabled (and it may not be in bootroms).
	 * So we must do manual alignment.  The descriptor base address must
	 * be at least 16-byte aligned, but the memory length must be a multiple
	 * of 128 for both RX and TX; so we might as well make the start of the
	 * descriptor table 128-byte aligned anyway.
	 *
	 * We introduce tgbDescBuf to hold the (possibly) unaligned
	 * start address, for freeing.
	 */
	if ((pDrvCtrl->tgbDescBuf =cacheDmaMallocEnd (sizeof(TGB_ADV_RDESC) * TGB_RX_DESC_CNT + sizeof(TGB_ADV_TDESC) * TGB_TX_DESC_CNT + 128))
			== NULL)
	{
		printk (TGB_NAME "%d: could not allocate descriptor memory\n",
				pDev->unitNumber, 0, 0, 0, 0, 0);
		return;
	}
	printk("<**DEBUG**> [%s():_%d_]:: [txgbe%d] tgbDescBuf: 0x%lx\n", __func__, __LINE__, pDev->unitNumber,pDrvCtrl->tgbDescBuf);

	if ((pDrvCtrl->tgbIsbBuf = cacheDmaMallocEnd(sizeof(u32) * TGB_ISB_MAX + 128)) == NULL)
	{ 
		printk (TGB_NAME "%d: could not allocate isb memory\n",
				0, 0, 0, 0, 0, 0);
		return;
	}
	printk("<**DEBUG**> [%s():_%d_]:: [txgbe%d] tgbIsbBuf: 0x%lx\n", __func__, __LINE__, pDev->unitNumber,pDrvCtrl->tgbIsbBuf);


	pDrvCtrl->tgbRxDescMem = (TGB_ADV_RDESC *)
        		ROUND_UP (pDrvCtrl->tgbDescBuf, 128);
	pDrvCtrl->tgbTxDescMem = (TGB_ADV_TDESC *)
        		(pDrvCtrl->tgbRxDescMem + TGB_RX_DESC_CNT);
	pDrvCtrl->isb_mem = (u32 *)
        		ROUND_UP (pDrvCtrl->tgbIsbBuf, 128);
	
	printk("<**DEBUG**> [%s():_%d_]:: [txgbe%d] tgbRxDescCount: %d, tgbRxDescSize:0x%x, tgbRxDescMem: 0x%lx\n", 
			__func__, __LINE__, pDev->unitNumber,TGB_RX_DESC_CNT,sizeof(TGB_ADV_RDESC),pDrvCtrl->tgbRxDescMem);
	printk("<**DEBUG**> [%s():_%d_]:: [txgbe%d] tgbTxDescCount: %d, tgbTxDescSize:0x%x, tgbTxDescMem: 0x%lx\n", 
			__func__, __LINE__, pDev->unitNumber,TGB_TX_DESC_CNT,sizeof(TGB_ADV_TDESC),pDrvCtrl->tgbTxDescMem);
	printk("<**DEBUG**> [%s():_%d_]:: [txgbe%d] isb_mem: 0x%lx\n", __func__, __LINE__, pDev->unitNumber,pDrvCtrl->isb_mem);

#endif
	pgDrvCtrl[pDrvCtrl->lanId] = pDrvCtrl;

	return;
}

/*****************************************************************************
 *
 * tgbInstConnect -  VxBus instConnect handler
 *
 * This function implements the VxBus instConnect handler for an tgb
 * device instance.
 *
 * RETURNS: N/A
 *
 * ERRNO: N/A
 */

UNUSED LOCAL void tgbInstConnect
(
		PCI_DEV * pDev
)
{
	return;
}

/*****************************************************************************
 *
 * tgbInstUnlink - VxBus unlink handler
 *
 * This function shuts down an tgb device instance in response to an
 * unlink event from VxBus. This may occur if our VxBus instance has
 * been terminated, or if the tgb driver has been unloaded. When an
 * unlink event occurs, we must shut down and unload the END interface
 * associated with this device instance and then release all the
 * resources allocated during instance creation, such as vxbDma
 * memory and maps, and interrupt handles.
 *
 * RETURNS: OK if device was successfully destroyed, otherwise ERROR
 *
 * ERRNO: N/A
 */

UNUSED LOCAL STATUS tgbInstUnlink
(
		PCI_DEV * pDev, void * unused
)
{
	TGB_DRV_CTRL * pDrvCtrl;
#ifdef TGB_VXB_DMA_BUF
	int i;
#endif

	pDrvCtrl = pDev->pDrvCtrl;

	/*
	 * Stop the device and detach from the MUX.
	 * Note: it's possible someone might try to delete
	 * us after our vxBus instantiation has completed,
	 * but before anyone has called our muxConnect method.
	 * In this case, there'll be no MUX connection to
	 * tear down, so we can skip this step.
	 */

	if (pDrvCtrl->tgbMuxDevCookie != NULL)
	{
		if (muxDevStop (pDrvCtrl->tgbMuxDevCookie) != OK)
		{
			DBG_LOG("dev stop failed\n", 0,0,0,0,0,0);
			return (ERROR);
		}

		/* Detach from the MUX. */

		if (muxDevUnload (TGB_NAME, pDev->unitNumber) != OK)
		{
			DBG_LOG("dev unload failed\n", 0,0,0,0,0,0);
			return (ERROR);
		}
	}

	/* Release the memory we allocated for the DMA rings */
#ifdef TGB_VXB_DMA_BUF

	vxbDmaBufMemFree (pDrvCtrl->tgbRxDescTag, pDrvCtrl->tgbRxDescMem,
			pDrvCtrl->tgbRxDescMap);

	vxbDmaBufMemFree (pDrvCtrl->tgbTxDescTag, pDrvCtrl->tgbTxDescMem,
			pDrvCtrl->tgbTxDescMap);

	for (i = 0; i < TGB_RX_DESC_CNT; i++)
		vxbDmaBufMapDestroy (pDrvCtrl->tgbMblkTag,
				pDrvCtrl->tgbRxMblkMap[i]);

	for (i = 0; i < TGB_TX_DESC_CNT; i++)
		vxbDmaBufMapDestroy (pDrvCtrl->tgbMblkTag,
				pDrvCtrl->tgbTxMblkMap[i]);

	/* Destroy the tags. */

	vxbDmaBufTagDestroy (pDrvCtrl->tgbRxDescTag);
	vxbDmaBufTagDestroy (pDrvCtrl->tgbTxDescTag);
	vxbDmaBufTagDestroy (pDrvCtrl->tgbMblkTag);
#else /* not TGB_VXB_DMA_BUF */
	cacheDmaFree (pDrvCtrl->tgbDescBuf);
#endif

	/* Disconnect the ISR. */

	//    vxbIntDisconnect (pDev, 0, tgbEndInt, pDrvCtrl);
	int_uninstall_handler(pDev->irq);

	/* free isb resources */
#ifdef TGB_VXB_DMA_BUF
	vxbDmaBufMemFree (pDrvCtrl->tgbIsbTag, pDrvCtrl->isb_mem,
			pDrvCtrl->tgbIsbMap);
	vxbDmaBufTagDestroy (pDrvCtrl->tgbIsbTag);
#else
	cacheDmaFree (pDrvCtrl->tgbIsbBuf);
#endif

	semDelete (pDrvCtrl->tgbDevSem);

	/* Destroy the adapter context. */
	free (pDrvCtrl);
	pDev->pDrvCtrl = NULL;

	return (OK);
}

/*****************************************************************************
 *
 * tgbLinkUpdate - link change event handler
 *
 * This function processes link change event notifications triggered by
 * the interrupt handler. In most configurations, the link speed will
 * remain constant at 10Gbps full duplex, but we must respond to cable
 * unplug and replug events.
 *
 * Once we determine the new link state, we will announce the change
 * to any bound protocols via muxError(). We also update the ifSpeed
 * fields in the MIB2 structures so that SNMP queries can detect the
 * correct link speed.
 *
 * RETURNS: ERROR if obtaining the new media setting fails, else OK
 *
 * ERRNO: N/A
 */

LOCAL STATUS tgbLinkUpdate
(
		PCI_DEV * pDev
)
{
	TGB_DRV_CTRL * pDrvCtrl;
	u32 oldStatus;
	u32 val = 0;
	
	if (pDev->pDrvCtrl == NULL)
		return (ERROR);

	pDrvCtrl = (TGB_DRV_CTRL *)pDev->pDrvCtrl;

	semTake (pDrvCtrl->tgbDevSem, WAIT_FOREVER);

	/* Check link status */

	oldStatus = pDrvCtrl->tgbCurStatus;
	val = TXGBE_CSR_READ_4(pDev, TGB_CFG_PORT_ST);
//	printk("<**DEBUG**> [%s():_%d_]:: [txgbe%d]  TXGBE_CSR_READ_4(0x14404): 0x%x\n", __FUNCTION__, __LINE__,pDev->unitNumber,val);
	
	if (TXGBE_CSR_READ_4(pDev, TGB_CFG_PORT_ST) &&
			(TXGBE_CSR_READ_4(pDev, TGB_CFG_PORT_ST) & TGB_CFG_PORT_ST_LINK_UP))
		pDrvCtrl->tgbCurStatus = IFM_AVALID|IFM_ACTIVE;
	else
		pDrvCtrl->tgbCurStatus = IFM_AVALID;

//	printk("<**DEBUG**> [%s():_%d_]:: [txgbe%d]  pDrvCtrl->tgbCurStatus=0x%x\n", __FUNCTION__, __LINE__,pDev->unitNumber,pDrvCtrl->tgbCurStatus);
	if (pDrvCtrl->tgbCurStatus & IFM_ACTIVE)
	{
		//enable flow control
		TXGBE_CSR_SETBIT_4(pDev, TGB_MAC_RX_FLOW_CTRL, TGB_MAC_RX_FLOW_CTRL_RFE);
		TXGBE_CSR_SETBIT_4(pDev, TGB_RDB_RFCC, TGB_RDB_RFCC_RFCE_802_3X);

		TXGBE_CSR_WRITE_4(pDev, TGB_RDB_PFCMACDAL, 0xC2000001);
		TXGBE_CSR_WRITE_4(pDev, TGB_RDB_PFCMACDAH, 0x0180);

		int i;

		for (i = 0; i < 8; i++)
		{
			TXGBE_CSR_WRITE_4(pDev, TGB_RDB_RFCL(i) , 0x80002800);
//          TXGBE_CSR_WRITE_4(pDev, TGB_RDB_RFCH(i) , 0x8006C400);
			TXGBE_CSR_WRITE_4(pDev, TGB_RDB_RFCH(i) , 0x80006C00);
		}

		for (i = 0; i < 4; i++)
		{
			TXGBE_CSR_WRITE_4(pDev, TGB_RDB_RFCV(i) , TGB_DEFAULT_FCPAUSE * 0x00010001);
		}

		TXGBE_CSR_WRITE_4(pDev, TGB_RDB_RFCRT , TGB_DEFAULT_FCPAUSE / 2);

		TXGBE_CSR_WRITE_4(pDev, TGB_MAC_TX_CFG, TGB_MAC_TX_CFG_TE | TGB_MAC_TX_CFG_SPEED_10G);
		TXGBE_CSR_WRITE_4(pDev, TGB_MAC_RX_CFG, TXGBE_CSR_READ_4(pDev, TGB_MAC_RX_CFG));
		TXGBE_CSR_WRITE_4(pDev, TGB_MAC_PKT_FLT, TGB_MAC_PKT_FLT_PR);
		TXGBE_CSR_WRITE_4(pDev, TGB_MAC_WDG_TIMEOUT, TXGBE_CSR_READ_4(pDev, TGB_MAC_WDG_TIMEOUT));
//		printk(" txgbe%d link up.\r\n", pDrvCtrl->lanId);
	}
	else
	{
//		printk(" txgbe%d link down.\r\n", pDrvCtrl->lanId);
	}

	/* If status went from down to up, announce link up. */

	if (pDrvCtrl->tgbCurStatus & IFM_ACTIVE && !(oldStatus & IFM_ACTIVE))
		jobQueueStdPost (pDrvCtrl->tgbJobQueue, NET_TASK_QJOB_PRI,
				(void *)muxLinkUpNotify, (void *)&pDrvCtrl->tgbEndObj,
				NULL, NULL, NULL, NULL);

	/* If status went from up to down, announce link down. */

	if (!(pDrvCtrl->tgbCurStatus & IFM_ACTIVE) && oldStatus & IFM_ACTIVE)
		jobQueueStdPost (pDrvCtrl->tgbJobQueue, NET_TASK_QJOB_PRI,
				(void *)muxLinkDownNotify, (void *)&pDrvCtrl->tgbEndObj,
				NULL, NULL, NULL, NULL);

	semGive (pDrvCtrl->tgbDevSem);
	
	return (OK);
}

/*****************************************************************************
 *
 * tgbMuxConnect - muxConnect method handler
 *
 * This function handles muxConnect() events, which may be triggered
 * manually or (more likely) by the bootstrap code. Most VxBus
 * initialization occurs before the MUX has been fully initialized,
 * so the usual muxDevLoad()/muxDevStart() sequence must be defered
 * until the networking subsystem is ready. This routine will ultimately
 * trigger a call to tgbEndLoad() to create the END interface instance.
 *
 * RETURNS: N/A
 *
 * ERRNO: N/A
 */

LOCAL void tgbMuxConnect
(
		PCI_DEV * pDev,
		void * unused
)
{
	TGB_DRV_CTRL *pDrvCtrl;

	/*
	 * Attach our ISR. For PCI, the index value is always
	 * 0, since the PCI bus controller dynamically sets
	 * up interrupts for us.
	 */
#ifdef __ENABLE_MSI__
	pDrvCtrl = pDev->pDrvCtrl;
	//   	lpi_install_handler(pDev->irq, tgbEndInt, pDrvCtrl);
	//  	printk("%s-%d irq %d\n",__func__,__LINE__,pDev->irq);

	msi_int_install(pDev->irq, (void *)tgbEndInt, (void *)pDrvCtrl);
	
	//   	gic_set_edged_trig(pDev->irq);//设置gic中断边沿触发   /*xxx ldf 20230823 note:: 这里先注释*/
#else
	pDrvCtrl = pDev->pDrvCtrl;
//	int_install_handler("txgbe", pDev->irq, 1, tgbEndInt, pDrvCtrl);
	shared_int_install(pDev->irq, tgbEndInt, pDrvCtrl);/*ldf 20230831 add*/
	int_enable_pic(pDev->irq);
#endif

#if 0
	/* Save the cookie. */
	pDrvCtrl->tgbMuxDevCookie = muxDevLoad (pDev->unitNumber,
			tgbEndLoad, "", TRUE, pDev);

	if (pDrvCtrl->tgbMuxDevCookie != NULL)
		muxDevStart (pDrvCtrl->tgbMuxDevCookie);

	if (_func_m2PollStatsIfPoll != NULL)
		endPollStatsInit (pDrvCtrl->tgbMuxDevCookie,
				_func_m2PollStatsIfPoll);
#endif
return;
}

/*****************************************************************************
 *
 * tgbReset - do lan reset for each port
 *
 * This function issues a reset command to the controller and waits
 * for it to complete. This routine is always used to place the
 * controller into a known state prior to configuration.
 *
 * RETURNS: ERROR if the reset bit never clears, otherwise OK
 *
 * ERRNO: N/A
 */

LOCAL STATUS tgbReset
(
		PCI_DEV * pDev
)
{
	TGB_DRV_CTRL * pDrvCtrl;
	pDrvCtrl = pDev->pDrvCtrl;
	UNUSED int ret;
	u32 sr_pcs_ctl, sr_pma_mmd_ctl1, sr_an_mmd_ctl, sr_an_mmd_adv_reg2;
	u32 vr_xs_or_pcs_mmd_digi_ctl1, curr_vr_xs_or_pcs_mmd_digi_ctl1;
	u32 curr_sr_pcs_ctl, curr_sr_pma_mmd_ctl1;
	u32 curr_sr_an_mmd_ctl, curr_sr_an_mmd_adv_reg2;

	/* remember internel phy regs from before we reset */
	curr_sr_pcs_ctl = tgb_rd32_epcs(pDev, TGB_SR_PCS_CTL2);
	curr_sr_pma_mmd_ctl1 = tgb_rd32_epcs(pDev, TGB_SR_PMA_MMD_CTL1);
	curr_sr_an_mmd_ctl = tgb_rd32_epcs(pDev, TGB_SR_AN_MMD_CTL);
	curr_sr_an_mmd_adv_reg2 = tgb_rd32_epcs(pDev, TGB_SR_AN_MMD_ADV_REG2);
	curr_vr_xs_or_pcs_mmd_digi_ctl1 = tgb_rd32_epcs(pDev, TGB_VR_XS_OR_PCS_MMD_DIGI_CTL1);

	if ((TXGBE_CSR_READ_4(pDev, TGB_CFG_PORT_ST) & TGB_CFG_PORT_ST_READ_LAN_ID))
	{
		TXGBE_CSR_SETBIT_4(pDev, TGB_MIS_RST, TGB_MIS_RST_LAN1_RST);
	}
	else
	{
		TXGBE_CSR_SETBIT_4(pDev, TGB_MIS_RST, TGB_MIS_RST_LAN0_RST);
	}
#ifdef _TXGBE_DBG_
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x1000C): 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_MIS_RST));
#endif

	vxbMsDelay(500);

#if 0
/* Check if flash load is done after hw power up */
	ret = tgbCheckFlashLoad(pDev, TGB_SPI_ILDR_STATUS_PERST);
	if(ret < 0)
		logMsg("tgbInstInit2:Loading Flash failed in PCIERST\n", 0, 0,0,0,0,0);

	ret = tgbCheckFlashLoad(pDev, TGB_SPI_ILDR_STATUS_PWRRST);
	if(ret < 0)
		logMsg("tgbInstInit2:Loading Flash failed IN POWERRST\n", 0, 0,0,0,0,0);
#endif

	sr_pcs_ctl = tgb_rd32_epcs(pDev, TGB_SR_PCS_CTL2);
	sr_pma_mmd_ctl1 = tgb_rd32_epcs(pDev, TGB_SR_PMA_MMD_CTL1);
	sr_an_mmd_ctl = tgb_rd32_epcs(pDev, TGB_SR_AN_MMD_CTL);
	sr_an_mmd_adv_reg2 = tgb_rd32_epcs(pDev, TGB_SR_AN_MMD_ADV_REG2);
	vr_xs_or_pcs_mmd_digi_ctl1 = tgb_rd32_epcs(pDev, TGB_VR_XS_OR_PCS_MMD_DIGI_CTL1);
	if (pDrvCtrl->orig_link_settings_stored == FALSE)
	{
		pDrvCtrl->orig_sr_pcs_ctl2 = sr_pcs_ctl;
		pDrvCtrl->orig_sr_pma_mmd_ctl1 = sr_pma_mmd_ctl1;
		pDrvCtrl->orig_sr_an_mmd_ctl = sr_an_mmd_ctl;
		pDrvCtrl->orig_sr_an_mmd_adv_reg2 = sr_an_mmd_adv_reg2;
		pDrvCtrl->orig_vr_xs_or_pcs_mmd_digi_ctl1 =
				vr_xs_or_pcs_mmd_digi_ctl1;
		pDrvCtrl->orig_link_settings_stored = TRUE;
	} else
	{
		/* If MNG FW is running on a multi-speed device that
		 * doesn't autoneg with out driver support we need to
		 * leave LMS in the state it was before we MAC reset.
		 * Likewise if we support WoL we don't want change the
		 * LMS state.
		 */
		pDrvCtrl->orig_sr_pcs_ctl2 = curr_sr_pcs_ctl;
		pDrvCtrl->orig_sr_pma_mmd_ctl1 = curr_sr_pma_mmd_ctl1;
		pDrvCtrl->orig_sr_an_mmd_ctl = curr_sr_an_mmd_ctl;
		pDrvCtrl->orig_sr_an_mmd_adv_reg2 =
				curr_sr_an_mmd_adv_reg2;
		pDrvCtrl->orig_vr_xs_or_pcs_mmd_digi_ctl1 =
				curr_vr_xs_or_pcs_mmd_digi_ctl1;

	}
	return (OK);
}

/*****************************************************************************
 *
 * tgbeIvarWxSet - program interrupt vector allocation registers
 *
 * This routine programs the wangxun tgb's interrupt vector allocation
 * registers in order to allow an event on a given queue to trigger a particular
 * interrupt source. For MSI-X interrupts, this controls which MSI-X vector
 * is tied to a given queue. Note that there are fewer interrupt source
 * bits and vectors than there are queues, so some of them will necessarily
 * overlap.
 *
 * RETURNS: N/A
 *
 * ERRNO: N/A
 */

LOCAL void tgbIvarWxSet
(
		PCI_DEV * pDev
)
{

	/* Allocate interrupt vectors, support 1 queue */

	TXGBE_CSR_WRITE_4(pDev, TGB_PX_IVAR0, (TGB_PX_IVAR_RX0_DEFAULT |
			TGB_PX_IVAR_RX0_VALID |
			TGB_PX_IVAR_TX0_DEFAULT |
			TGB_PX_IVAR_TX0_VALID));

	TXGBE_CSR_WRITE_4(pDev, TGB_PX_MISC_IVAR, (TGB_PX_MISC_IVAR_DEFAULT |
			TGB_PX_MISC_IVAR_VALID));

	TXGBE_CSR_WRITE_4(pDev, TGB_PX_ITR0, (TGB_DEFAULT_ITR | TGB_PX_ITR_CNT_WDIS));

	return;
}

/*****************************************************************************
 *
 * tgbCheckFlashLoad - check flash is load or not
 *
 *  check if flash load is done after hw power up 
 *
 * RETURNS: N/A
 *
 * ERRNO: N/A
 */

int  tgbCheckFlashLoad
(
		PCI_DEV * pDev,
		u32 checkBit
)
{
	UNUSED TGB_DRV_CTRL * pDrvCtrl;
	pDrvCtrl = pDev->pDrvCtrl;
	u32 i = 0;
	u32 reg = 0;

	/* if there's flash existing */
	if (!(TXGBE_CSR_READ_4(pDev, TGB_SPI_STATUS) & TGB_SPI_STATUS_FLASH_BYPASS))
	{
		/* wait hw load flash done */
		for (i = 0; i < TGB_MAX_FLASH_LOAD_POLL_TIME; i++)
		{
			reg = TXGBE_CSR_READ_4(pDev, TGB_SPI_ILDR_STATUS);
			if (!(reg & checkBit))
			{
				/* done */
				break;
			}
			vxbMsDelay(200);
		}
		if (i == TGB_MAX_FLASH_LOAD_POLL_TIME)
		{

			logMsg("Loading Flash failed \n", 0,0,0,0,0,0);
			return ERROR;
		}
	}
	return TRUE;
}
/*****************************************************************************
 *
 * tgbEndLoad - END driver entry point
 *
 * This routine initializes the END interface instance associated
 * with this device. In traditional END drivers, this function is
 * the only public interface, and it's typically invoked by a BSP
 * driver configuration stub. With VxBus, the BSP stub code is no
 * longer needed, and this function is now invoked automatically
 * whenever this driver's muxConnect() method is called.
 *
 * For older END drivers, the load string would contain various
 * configuration parameters, but with VxBus this use is deprecated.
 * The load string should just be an empty string. The second
 * argument should be a pointer to the VxBus device instance
 * associated with this device. Like older END drivers, this routine
 * will still return the device name if the init string is empty,
 * since this behavior is still expected by the MUX. The MUX will
 * invoke this function twice: once to obtain the device name,
 * and then again to create the actual END_OBJ instance.
 *
 * When this function is called the second time, it will initialize
 * the END object, perform MIB2 setup, allocate a buffer pool, and
 * initialize the supported END capabilities.
 *
 * RETURNS: An END object pointer, or NULL on error, or 0 and the name
 * of the device if the <loadStr> was empty.
 *
 * ERRNO: N/A
 */

END_OBJ * tgbEndLoad
(
		char * loadStr
)
{

	TGB_DRV_CTRL *pDrvCtrl;
	PCI_DEV * pDev;
	int r;
	
	/* Make the MUX happy. */
	if (loadStr == NULL)
	{
		printk("<WARNNING> [%s():_%d_]:: input loadStr is NULL!\n", __func__, __LINE__);
		return NULL;
	}

	if (loadStr[0] == 0)
	{
		bcopy (TGB_NAME, loadStr, sizeof(TGB_NAME));
		printk("<WARNNING> [%s():_%d_]:: input loadStr[0] is 0!\n", __func__, __LINE__);
		return NULL;
	}

	char *holder = NULL;
	int unit = atoi(strtok_r(loadStr, ":", &holder));
	NET_RESOURCE *pReso;
	PCI_BOARD_RESOURCE *boad_res = &txgbe_pci_resources[unit];
	pReso = (NET_RESOURCE *) (boad_res->pExtended);
	pDev = (struct pci_dev *)kmalloc(sizeof(struct pci_dev));
	
	pDev->unitNumber = unit;
	pDev->pci_bus = boad_res->pci_bus;
	pDev->pci_device = boad_res->pci_device;
	pDev->pci_func = boad_res->pci_func;
	pDev->vendor = boad_res->pci_VID;
	pDev->device = boad_res->pci_DID;
	pDev->irq = boad_res->irq;
	pDev->pRegBase = (void*)(pReso->memBaseLow|(u64)pReso->memBaseHigh<<32);
	
	printk("<**DEBUG**> [%s():_%d_]:: txgbe%d (%d,%d,%d)  VID: 0x%x, DID: 0x%x, pDev->pRegBase: 0x%lx, irq: %d\n", \
			__FUNCTION__, __LINE__,unit,pDev->pci_bus,pDev->pci_device,pDev->pci_func,pDev->vendor,pDev->device,pDev->pRegBase,pDev->irq);

	tgbInstInit2(pDev);
	
	pDrvCtrl = pDev->pDrvCtrl;

	if (NETDRV_OBJ_INIT (&pDrvCtrl->tgbEndObj, NULL, TGB_NAME,
			pDev->unitNumber, &tgbNetFuncs,
			"WangXun Sapphire 10GbE VxBus END Driver") == ERROR)
	{
		printk("%s%d: END_OBJ_INIT failed\n", (ptrdiff_t)TGB_NAME,
				pDev->unitNumber, 0, 0, 0, 0);
		return (NULL);
	}
	/*
	 * Note: we lie about the interface speed here. While the
	 * WangXun Sapphire is really a 10GbE device, VxWorks currently stores
	 * the interface speed in a 32-bit variable, and 10000000000
	 * is too big to fit in it.
	 */

	UNUSED int ret = net_drv_M2_init (&pDrvCtrl->tgbEndObj, M2_ifType_ethernet_csmacd,
			pDrvCtrl->tgbAddr, ETHER_ADDR_LEN, ETHERMTU, 1000000000,
			IFF_NOTRAILERS | IFF_SIMPLEX | IFF_MULTICAST | IFF_BROADCAST);

	if(ret != OK)
	{
		printk("%s%d: net_drv_M2_init failed\n", (ptrdiff_t)TGB_NAME,
				pDev->unitNumber, 0, 0, 0, 0);
		return (NULL);
	}
	/*
	 * The ifHighSpeed field we can set correctly. And since the
	 * speed is always 10gig, we never need to change it.
	 */

	if (pDrvCtrl->tgbEndObj.pMib2Tbl != NULL)
	{
		pDrvCtrl->tgbEndObj.pMib2Tbl->m2Data.mibXIfTbl.ifHighSpeed = 1000;
		pDrvCtrl->tgbEndObj.pMib2Tbl->m2Data.mibIfTbl.ifSpeed =
				pDrvCtrl->tgbEndObj.mib2Tbl.ifSpeed;
	}

	/* Allocate a buffer pool */
	int endPoolSize = (5 * TGB_RX_DESC_CNT) + TGB_TX_DESC_CNT;
	if (pDrvCtrl->tgbMaxMtu == TGB_JUMBO_MTU){
		r = endPoolJumboCreate (endPoolSize, &pDrvCtrl->tgbEndObj.pNetPool);
	}
	else{
		r = endPoolCreate (endPoolSize, &pDrvCtrl->tgbEndObj.pNetPool);
	}
	printk("<**DEBUG**> [%s():_%d_]:: txgbe%d endPoolCount = %d \n", __FUNCTION__, __LINE__,unit,endPoolSize);
	
	if (r == ERROR){
		printk("%s%d: pool creation failed\n", (ptrdiff_t)TGB_NAME,
				pDev->unitNumber, 0, 0, 0, 0);
		return (NULL);
	}
	pDrvCtrl->tgbPollBuf = endPoolTupleGet (pDrvCtrl->tgbEndObj.pNetPool);

	/* Set up polling stats. */

	pDrvCtrl->tgbEndStatsConf.ifPollInterval = sys_clk_rate_get();
	pDrvCtrl->tgbEndStatsConf.ifEndObj = &pDrvCtrl->tgbEndObj;
	pDrvCtrl->tgbEndStatsConf.ifWatchdog = 0;
	pDrvCtrl->tgbEndStatsConf.ifValidCounters = (END_IFINUCASTPKTS_VALID |
			END_IFINMULTICASTPKTS_VALID | END_IFINBROADCASTPKTS_VALID |
			END_IFINOCTETS_VALID | END_IFINERRORS_VALID | END_IFINDISCARDS_VALID |
			END_IFOUTUCASTPKTS_VALID | END_IFOUTMULTICASTPKTS_VALID |
			END_IFOUTBROADCASTPKTS_VALID | END_IFOUTOCTETS_VALID |
			END_IFOUTERRORS_VALID);

	/* Set up capabilities. */

	pDrvCtrl->tgbCaps.cap_available = IFCAP_VLAN_MTU;
	pDrvCtrl->tgbCaps.cap_enabled = IFCAP_VLAN_MTU;

	pDrvCtrl->tgbCaps.csum_flags_tx = CSUM_IP|CSUM_TCP|CSUM_UDP;
	//    pDrvCtrl->tgbCaps.csum_flags_tx |= CSUM_TCPv6|CSUM_UDPv6;
	pDrvCtrl->tgbCaps.csum_flags_rx = CSUM_IP|CSUM_UDP|CSUM_TCP;
	//    pDrvCtrl->tgbCaps.cap_available |= IFCAP_TXCSUM|IFCAP_RXCSUM;
	//    pDrvCtrl->tgbCaps.cap_enabled |= IFCAP_TXCSUM|IFCAP_RXCSUM;

	if (pDrvCtrl->tgbMaxMtu == TGB_JUMBO_MTU)
	{
		pDrvCtrl->tgbCaps.cap_available |= IFCAP_JUMBO_MTU;
		pDrvCtrl->tgbCaps.cap_enabled |= IFCAP_JUMBO_MTU;
	}

	return (&pDrvCtrl->tgbEndObj);
}

/*****************************************************************************
 *
 * tgbEndUnload - unload END driver instance
 *
 * This routine undoes the effects of tgbEndLoad(). The END object
 * is destroyed, our network pool is released, the endM2 structures
 * are released, and the polling stats watchdog is terminated.
 *
 * Note that the END interface instance can't be unloaded if the
 * device is still running. The device must be stopped with muxDevStop()
 * first.
 *
 * RETURNS: ERROR if device is still in the IFF_UP state, otherwise OK
 *
 * ERRNO: N/A
 */

LOCAL STATUS tgbEndUnload
(
		END_OBJ * pEnd
)
{

	TGB_DRV_CTRL * pDrvCtrl;

	/* We must be stopped before we can be unloaded. */

	if (pEnd->flags & IFF_UP)
		return (ERROR);

	pDrvCtrl = (TGB_DRV_CTRL *)pEnd;

	netMblkClChainFree (pDrvCtrl->tgbPollBuf);

	/* Relase our buffer pool */
	endPoolDestroy (pDrvCtrl->tgbEndObj.pNetPool);

	/* terminate stats polling */
	wdDelete (pDrvCtrl->tgbEndStatsConf.ifWatchdog);

	endM2Free (&pDrvCtrl->tgbEndObj);

	END_OBJECT_UNLOAD (&pDrvCtrl->tgbEndObj);

	return (EALREADY);  /* prevent freeing of pDrvCtrl */

}

/*****************************************************************************
 *
 * tgbEndHashTblPopulate - populate the multicast hash filter
 *
 * This function programs the wangxun tgb's multicast hash
 * filter to receive frames sent to the multicast groups specified
 * in the multicast address list attached to the END object. If
 * the interface is in IFF_ALLMULTI mode, the filter will be
 * programmed to receive all multicast packets by setting all the
 * bits in the hash table to one.
 *
 * RETURNS: N/A
 *
 * ERRNO: N/A
 */

LOCAL void tgbEndHashTblPopulate
(
		TGB_DRV_CTRL * pDrvCtrl
)
{

	PCI_DEV * pDev;
	u32 h;
	u32 fctrl;
	int reg, bit;
	ETHER_MULTI * mCastNode = NULL;
	int i;
	u32 vmolr;

	pDev = pDrvCtrl->tgbDev;

	for (i = 0; i < 128; i++)
		TXGBE_CSR_WRITE_4(pDev, TGB_PSR_UC_TBL(0) + (i * sizeof(u32)), 0);

	for (i = 0; i < TGB_MAR_CNT; i++)
		TXGBE_CSR_WRITE_4(pDev, TGB_PSR_MC_TBL(0) + (i * sizeof(u32)), 0);

	vmolr = TXGBE_CSR_READ_4(pDev, TGB_PSR_VM_L2CTL(0)) &
			~(TGB_PSR_VM_L2CTL_UPE |
					TGB_PSR_VM_L2CTL_MPE |
					TGB_PSR_VM_L2CTL_ROPE |
					TGB_PSR_VM_L2CTL_ROMPE);
	vmolr |= TGB_PSR_VM_L2CTL_BAM |
			TGB_PSR_VM_L2CTL_AUPE |
			TGB_PSR_VM_L2CTL_VACC;

	if (pDrvCtrl->tgbEndObj.flags & (IFF_ALLMULTI|IFF_PROMISC))
	{
		/* set allmulticast mode */
		fctrl = TXGBE_CSR_READ_4(pDev, TGB_PSR_CTL);
		fctrl |= (TGB_PSR_CTL_BAM | TGB_PSR_CTL_UPE | TGB_PSR_CTL_MPE);
		vmolr |= TGB_PSR_VM_L2CTL_MPE;
		TXGBE_CSR_WRITE_4(pDev, TGB_PSR_CTL, fctrl);
		return;
	}

	TXGBE_CSR_SETBIT_4(pDev, TGB_PSR_CTL, TGB_PSR_CTL_MFE);
	TXGBE_CSR_CLRBIT_4(pDev, TGB_PSR_CTL, TGB_PSR_CTL_MO);
	vmolr |= TGB_PSR_VM_L2CTL_ROPE | TGB_PSR_VM_L2CTL_ROMPE;

	/* Now repopulate it. */

	for (mCastNode =
			(ETHER_MULTI *) lstFirst ((void *)&pDrvCtrl->tgbEndObj.multiList);
			mCastNode != NULL;
			mCastNode = (ETHER_MULTI *) lstNext ((void *)&mCastNode->node))
	{

		h = (mCastNode->addr[4] >> 4) | (mCastNode->addr[5] << 4);
		h &= 0xFFF;
		reg = (h >> 5) & 0x7F;
		bit = h & 0x1F;
		TXGBE_CSR_SETBIT_4(pDev, TGB_PSR_MC_TBL(0) + (reg * sizeof(u32)),
				1 << bit);
	}

	TXGBE_CSR_WRITE_4(pDev, TGB_PSR_VM_L2CTL(0), vmolr);

	return;
}

/*****************************************************************************
 *
 * tgbEndMCastAddrAdd - add a multicast address for the device
 *
 * This routine adds a multicast address to whatever the driver
 * is already listening for.  It then resets the address filter.
 *
 * RETURNS: OK or ERROR.
 *
 * ERRNO: N/A
 */

LOCAL STATUS tgbEndMCastAddrAdd
(
		END_OBJ * pEnd,
		char * pAddr
)
{
	int retVal;
	TGB_DRV_CTRL * pDrvCtrl = (TGB_DRV_CTRL *) pEnd;

	semTake (pDrvCtrl->tgbDevSem, WAIT_FOREVER);

	if (!(pDrvCtrl->tgbEndObj.flags & IFF_UP))
	{
		semGive (pDrvCtrl->tgbDevSem);
		return (OK);
	}

	retVal = etherMultiAdd (&pEnd->multiList, pAddr);

	if (retVal == ENETRESET)
	{
		pEnd->nMulti++;
		tgbEndHashTblPopulate ((TGB_DRV_CTRL *)pEnd);
	}

	semGive (pDrvCtrl->tgbDevSem);
	return (OK);
}

/*****************************************************************************
 *
 * tgbEndMCastAddrDel - delete a multicast address for the device
 *
 * This routine removes a multicast address from whatever the driver
 * is listening for.  It then resets the address filter.
 *
 * RETURNS: OK or ERROR.
 *
 * ERRNO: N/A
 */

LOCAL STATUS tgbEndMCastAddrDel
(
		END_OBJ * pEnd,
		char * pAddr
)
{
	int retVal;
	TGB_DRV_CTRL * pDrvCtrl = (TGB_DRV_CTRL *) pEnd;

	semTake (pDrvCtrl->tgbDevSem, WAIT_FOREVER);

	if (!(pDrvCtrl->tgbEndObj.flags & IFF_UP))
	{
		semGive (pDrvCtrl->tgbDevSem);
		return (OK);
	}

	retVal = etherMultiDel (&pEnd->multiList, pAddr);

	if (retVal == ENETRESET)
	{
		pEnd->nMulti--;
		tgbEndHashTblPopulate ((TGB_DRV_CTRL *)pEnd);
	}

	//  	printk("%s-%d %p\n",__func__,__LINE__,pDrvCtrl->tgbDevSem);

	semGive (pDrvCtrl->tgbDevSem);
	return (OK);
}

/*****************************************************************************
 *
 * tgbEndMCastAddrGet - get the multicast address list for the device
 *
 * This routine gets the multicast list of whatever the driver
 * is already listening for.
 *
 * RETURNS: OK or ERROR.
 *
 * ERRNO: N/A
 */

LOCAL STATUS tgbEndMCastAddrGet
(
		END_OBJ * pEnd,
		MULTI_TABLE * pTable
)
{
	STATUS rval;
	TGB_DRV_CTRL * pDrvCtrl = (TGB_DRV_CTRL *) pEnd;

	semTake (pDrvCtrl->tgbDevSem, WAIT_FOREVER);

	if (!(pDrvCtrl->tgbEndObj.flags & IFF_UP))
	{
		semGive (pDrvCtrl->tgbDevSem);
		return (OK);
	}

	rval = etherMultiGet (&pEnd->multiList, pTable);

	semGive (pDrvCtrl->tgbDevSem);
	return (rval);
}

/*****************************************************************************
 *
 * tgbEndStatsDump - return polled statistics counts
 *
 * This routine is automatically invoked periodically by the polled
 * statistics watchdog.
 *
 * RETURNS: always OK
 *
 * ERRNO: N/A
 */

LOCAL STATUS tgbEndStatsDump
(
		TGB_DRV_CTRL * pDrvCtrl
)
{
	END_IFCOUNTERS *    pEndStatsCounters;
	PCI_DEV * pDev;
	u32 tmp;
	int i = 0;

	semTake (pDrvCtrl->tgbDevSem, WAIT_FOREVER);

	if (!(pDrvCtrl->tgbEndObj.flags & IFF_UP))
	{
		semGive (pDrvCtrl->tgbDevSem);
		return (ERROR);
	}

	pDev = pDrvCtrl->tgbDev;

	pEndStatsCounters = &pDrvCtrl->tgbEndStatsCounters;

	/* Get number of RX'ed octets */

	tmp = TXGBE_CSR_READ_4(pDev, TGB_PX_GORC_LSB);
	pEndStatsCounters->ifInOctets = tmp;
	tmp = TXGBE_CSR_READ_4(pDev, TGB_PX_GORC_MSB); /* Read to clear counter */
	pEndStatsCounters->ifInOctets += (uint64_t)tmp << 32;

	/* Get number of TX'ed octets */

	tmp = TXGBE_CSR_READ_4(pDev, TGB_PX_GOTC_LSB);
	pEndStatsCounters->ifOutOctets = tmp;
	tmp = TXGBE_CSR_READ_4(pDev, TGB_PX_GOTC_MSB); /* Read to clear counter */
	pEndStatsCounters->ifOutOctets += (uint64_t)tmp << 32;

	/* Get RX'ed unicasts, broadcasts, multicasts */

	tmp = TXGBE_CSR_READ_4(pDev, TGB_PX_GPRC);
	pEndStatsCounters->ifInUcastPkts = tmp;
	tmp = TXGBE_CSR_READ_4(pDev, TGB_RX_BC_FRAMES_GOOD_LOW);
	pEndStatsCounters->ifInBroadcastPkts = tmp;
	tmp = 0;
	for(i = 0; i < 128; i++)
		tmp += TXGBE_CSR_READ_4(pDev, TGB_PX_MPRC(i));
	pEndStatsCounters->ifInMulticastPkts = tmp;
	pEndStatsCounters->ifInUcastPkts -=
			(pEndStatsCounters->ifInMulticastPkts +
					pEndStatsCounters->ifInBroadcastPkts);

	/* Get TX'ed unicasts, broadcasts, multicasts */

	tmp = TXGBE_CSR_READ_4(pDev, TGB_PX_GPTC);
	pEndStatsCounters->ifOutUcastPkts = tmp;
	tmp = TXGBE_CSR_READ_4(pDev, TGB_TX_BC_FRAMES_GOOD_LOW);
	pEndStatsCounters->ifOutBroadcastPkts = tmp;
	tmp = TXGBE_CSR_READ_4(pDev, TGB_TX_MC_FRAMES_GOOD_LOW);
	pEndStatsCounters->ifOutMulticastPkts = tmp;
	pEndStatsCounters->ifOutUcastPkts -=
			(pEndStatsCounters->ifOutMulticastPkts +
					pEndStatsCounters->ifOutBroadcastPkts);

	semGive (pDrvCtrl->tgbDevSem);

	return (OK);
}

/*****************************************************************************
 *
 * tgbEndIoctl - the driver I/O control routine
 *
 * This function processes ioctl requests supplied via the muxIoctl()
 * routine. In addition to the normal boilerplate END ioctls, this
 * driver supports the IFMEDIA ioctls, END capabilities ioctls, and
 * polled stats ioctls.
 *
 * RETURNS: A command specific response, usually OK or ERROR.
 *
 * ERRNO: N/A
 */

LOCAL int tgbEndIoctl
(
		END_OBJ * pEnd,
		int cmd,
		caddr_t data
)
{
	TGB_DRV_CTRL * pDrvCtrl;
	END_MEDIALIST * mediaList;
	END_CAPABILITIES * hwCaps;
	END_MEDIA * pMedia;
	END_RCVJOBQ_INFO * qinfo;
	u32 nQs;
	PCI_DEV * pDev;
	int value;
	int error = OK;

	pDrvCtrl = (TGB_DRV_CTRL *)pEnd;
	pDev = pDrvCtrl->tgbDev;

	if (cmd != EIOCPOLLSTART && cmd != EIOCPOLLSTOP)
		semTake (pDrvCtrl->tgbDevSem, WAIT_FOREVER);

	switch (cmd)
	{
	case EIOCSADDR:
		if (data == NULL)
			error = EINVAL;
		else
		{
			bcopy ((char *)data, (char *)pDrvCtrl->tgbAddr,
					ETHER_ADDR_LEN);
			bcopy ((char *)data,
					(char *)pEnd->mib2Tbl.ifPhysAddress.phyAddress,
					ETHER_ADDR_LEN);
			if (pEnd->pMib2Tbl != NULL)
				bcopy ((char *)data,
						(char *)pEnd->pMib2Tbl->m2Data.mibIfTbl.ifPhysAddress.phyAddress,
						ETHER_ADDR_LEN);
		}
		tgbEndRxConfig (pDrvCtrl);
		break;

	case EIOCGADDR:
		if (data == NULL)
			error = EINVAL;
		else
			bcopy ((char *)pDrvCtrl->tgbAddr, (char *)data,
					ETHER_ADDR_LEN);
		break;
	case EIOCSFLAGS:
		value = (int) data;
		if (value < 0)
		{
			value = -value;
			value--;
			END_FLAGS_CLR (pEnd, value);
		}
		else
			NETDRV_FLAGS_SET (pEnd, value);

		tgbEndRxConfig (pDrvCtrl);
		break;

	case EIOCGFLAGS:
		if (data == NULL)
			error = EINVAL;
		else
			*(s32 *)data = END_FLAGS_GET(pEnd);

		break;

	case EIOCMULTIADD:
		error = tgbEndMCastAddrAdd (pEnd, (char *) data);
		break;

	case EIOCMULTIDEL:
		error = tgbEndMCastAddrDel (pEnd, (char *) data);
		break;

	case EIOCMULTIGET:
		error = tgbEndMCastAddrGet (pEnd, (MULTI_TABLE *) data);
		break;

	case EIOCPOLLSTART:
		pDrvCtrl->tgbPolling = TRUE;
		pDrvCtrl->tgbIntMask = TXGBE_CSR_READ_4(pDev, TGB_PX_MISC_IC);
		//TXGBE_CSR_WRITE_4(pDev, TGB_PX_MISC_ICS, TGB_PX_MISC_IEN_MASK & 0x00000000);

		/*
		 * We may have been asked to enter polled mode while
		 * there are transmissions pending. This is a problem,
		 * because the polled transmit routine expects that
		 * the TX ring will be empty when it's called. In
		 * order to guarantee this, we have to drain the TX
		 * ring here. We could also just plain reset and
		 * reinitialize the transmitter, but this is faster.
		 */
		 while (TXGBE_CSR_READ_4(pDev, TGB_PX_TR_WP(0)) != TXGBE_CSR_READ_4(pDev, TGB_PX_TR_RP(0)));

		while (pDrvCtrl->tgbTxFree < TGB_TX_DESC_CNT - 1)
		{
#ifdef TGB_VXB_DMA_BUF
			VXB_DMA_MAP_ID pMap;
#endif
			M_BLK_ID pMblk;

			pMblk = pDrvCtrl->tgbTxMblk[pDrvCtrl->tgbTxCons];

			if (pMblk != NULL)
			{
#ifdef TGB_VXB_DMA_BUF
				pMap = pDrvCtrl->tgbTxMblkMap[pDrvCtrl->tgbTxCons];
				vxbDmaBufMapUnload (pDrvCtrl->tgbMblkTag, pMap);
#endif
				endPoolTupleFree (pMblk);
				pDrvCtrl->tgbTxMblk[pDrvCtrl->tgbTxCons] = NULL;
			}

			pDrvCtrl->tgbTxFree++;
			TGB_INC_DESC (pDrvCtrl->tgbTxCons, TGB_TX_DESC_CNT);
		}

		break;

	case EIOCPOLLSTOP:
		pDrvCtrl->tgbPolling = FALSE;
		TXGBE_CSR_WRITE_4(pDev, TGB_PX_MISC_ICS, pDrvCtrl->tgbIntMask);

		break;

	case EIOCGMIB2233:
	case EIOCGMIB2:
		error = endM2Ioctl (&pDrvCtrl->tgbEndObj, cmd, data);
		break;

	case EIOCGPOLLCONF:
		if (data == NULL)
			error = EINVAL;
		else
			*((END_IFDRVCONF **)data) = &pDrvCtrl->tgbEndStatsConf;
		break;

	case EIOCGPOLLSTATS:
		if (data == NULL)
			error = EINVAL;
		else
		{
			error = tgbEndStatsDump(pDrvCtrl);
			if (error == OK)
				*((END_IFCOUNTERS **)data) =
						&pDrvCtrl->tgbEndStatsCounters;
		}
		break;

	case EIOCGMEDIALIST:
		if (data == NULL)
		{
			error = EINVAL;
			break;
		}
		if (pDrvCtrl->tgbMediaList->endMediaListLen == 0)
		{
			error = ENOTSUP;
			break;
		}

		mediaList = (END_MEDIALIST *)data;
		if (mediaList->endMediaListLen <
				pDrvCtrl->tgbMediaList->endMediaListLen)
		{
			mediaList->endMediaListLen =
					pDrvCtrl->tgbMediaList->endMediaListLen;
			error = ENOSPC;
			break;
		}

		bcopy((char *)pDrvCtrl->tgbMediaList, (char *)mediaList,
				sizeof(END_MEDIALIST) + (sizeof(u32) *
						pDrvCtrl->tgbMediaList->endMediaListLen));
		break;

	case EIOCGIFMEDIA:
		if (data == NULL)
			error = EINVAL;
		else
		{
			pMedia = (END_MEDIA *)data;
			pMedia->endMediaActive =
					pDrvCtrl->tgbMediaList->endMediaListDefault;
			pMedia->endMediaStatus = pDrvCtrl->tgbCurStatus;
		}

		break;

	case EIOCSIFMEDIA:
		if (data == NULL)
			error = EINVAL;
		else
		{
			pMedia = (END_MEDIA *)data;
			pDrvCtrl->tgbCurMedia = pMedia->endMediaActive;
			tgbLinkUpdate (pDrvCtrl->tgbDev);
			error = OK;
		}
		break;

	case EIOCGIFCAP:
		hwCaps = (END_CAPABILITIES *)data;
		if (hwCaps == NULL)
		{
			error = EINVAL;
			break;
		}
		hwCaps->csum_flags_tx = pDrvCtrl->tgbCaps.csum_flags_tx;
		hwCaps->csum_flags_rx = pDrvCtrl->tgbCaps.csum_flags_rx;
		hwCaps->cap_available = pDrvCtrl->tgbCaps.cap_available;
		hwCaps->cap_enabled = pDrvCtrl->tgbCaps.cap_enabled;
		break;

	case EIOCSIFCAP:
		hwCaps = (END_CAPABILITIES *)data;
		if (hwCaps == NULL)
		{
			error = EINVAL;
			break;
		}
	case EIOCGIFMTU:
		if (data == NULL)
			error = EINVAL;
		else
			*(int *)data = pEnd->mib2Tbl.ifMtu;
		break;

	case EIOCSIFMTU:
		value = (int)data;
		if (value <= 0 || value > pDrvCtrl->tgbMaxMtu)
		{
			error = EINVAL;
			break;
		}
		pEnd->mib2Tbl.ifMtu = value;
		if (pEnd->pMib2Tbl != NULL)
			pEnd->pMib2Tbl->m2Data.mibIfTbl.ifMtu = value;
		break;

	case EIOCGRCVJOBQ:
		if (data == NULL)
		{
			error = EINVAL;
			break;
		}

		qinfo = (END_RCVJOBQ_INFO *)data;
		nQs = qinfo->numRcvJobQs;
		qinfo->numRcvJobQs = 1;
		if (nQs < 1)
			error = ENOSPC;
		else
			qinfo->qIds[0] = pDrvCtrl->tgbJobQueue;
		break;

	default:
		error = EINVAL;
		break;
	}
	if (cmd != EIOCPOLLSTART && cmd != EIOCPOLLSTOP)
		semGive (pDrvCtrl->tgbDevSem);

	return (error);
}

/*****************************************************************************
 *
 * tgbEndRxConfig - configure the wangxun sapphire RX filter
 *
 * This is a helper routine used by tgbEndIoctl() and tgbEndStart() to
 * configure the controller's RX filter. The unicast address filter is
 * programmed with the currently configured MAC address, and the RX
 * configuration is set to allow unicast and broadcast frames to be
 * received. If the interface is in IFF_PROMISC mode, the FCTL_UPE
 * bit is set, which allows all packets to be received.
 *
 * The tgbEndHashTblPopulate() routine is also called to update the
 * multicast filter.
 *
 * RETURNS: N/A
 *
 * ERRNO: N/A
 */
LOCAL void tgbEndRxConfig
(
		TGB_DRV_CTRL * pDrvCtrl
)
{
	UNUSED u32 addr[2];
	PCI_DEV * pDev;
	u32 parLo;
	u32 parHi;
	int i;
	UNUSED u32 addrLo;
	UNUSED u32 addrHi;

	/* set rar */
	if (pDrvCtrl->tgbDevType == TGB_DEVTYPE_SP)
	{
		parLo = TGB_PSR_MAC_SWC_AD_L;
		parHi = TGB_PSR_MAC_SWC_AD_H;
	}

	pDev = pDrvCtrl->tgbDev;

	/* Copy the address to a buffer we know is 32-bit aligned. */

	//    bcopy ((char *)pDrvCtrl->tgbAddr, (char *)addr, ETHER_ADDR_LEN);

	/*
	 * Init our MAC address.
	 */
#if 0
	TXGBE_CSR_WRITE_4(pDev, TGB_PSR_MAC_SWC_IDX, 0);
	TXGBE_CSR_WRITE_4(pDev, TGB_PSR_MAC_SWC_AD_H, 0x0202);
	TXGBE_CSR_WRITE_4(pDev, TGB_PSR_MAC_SWC_AD_L, 0x03040506);
#endif

	TXGBE_CSR_WRITE_4(pDev, TGB_PSR_MAC_SWC_IDX, 0);
	//	CACHE_PIPE_FLUSH();
	TXGBE_CSR_WRITE_4(pDev, parLo, pDrvCtrl->tgbAddrLo);
	TXGBE_CSR_WRITE_4(pDev, parHi, (pDrvCtrl->tgbAddrHi & 0xFFFF) | TGB_PARH_AV);
	
#ifdef _TXGBE_DBG_
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x16210): 0x%x,  except: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_PSR_MAC_SWC_IDX), (0));
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x%x): 0x%x,  except: 0x%x\n", 
			__FUNCTION__, __LINE__,parLo,TXGBE_CSR_READ_4(pDev, parLo), (pDrvCtrl->tgbAddrLo));
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x%x): 0x%x,  except: 0x%x\n", 
			__FUNCTION__, __LINE__,parHi,TXGBE_CSR_READ_4(pDev, parHi), ((pDrvCtrl->tgbAddrHi & 0xFFFF) | TGB_PARH_AV));
#endif

	for (i = 1; i < 128; i++)
	{
		TXGBE_CSR_WRITE_4(pDev, TGB_PSR_MAC_SWC_IDX, i);
		TXGBE_CSR_WRITE_4(pDev, parLo, 0);
		TXGBE_CSR_WRITE_4(pDev, parHi, 0);
	}

	/* Program the RX filter to receive unicasts and broadcasts */

	TXGBE_CSR_SETBIT_4(pDev, TGB_PSR_CTL, TGB_PSR_CTL_BAM);
#ifdef _TXGBE_DBG_
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x15000): 0x%x, set bit:0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_PSR_CTL), TGB_PSR_CTL_BAM);
#endif

	/* Enable promisc mode, if specified. */

	if (pDrvCtrl->tgbEndObj.flags & IFF_PROMISC)
		TXGBE_CSR_SETBIT_4(pDev, TGB_PSR_CTL, TGB_PSR_CTL_UPE);
	else
		TXGBE_CSR_CLRBIT_4(pDev, TGB_PSR_CTL, TGB_PSR_CTL_UPE);

	/* disable RSC */

	TXGBE_CSR_SETBIT_4(pDev, TGB_PSR_CTL, TGB_PSR_CTL_RSC_DIS |
			TGB_PSR_CTL_RSC_ACK);

	/* Program the multicast filter. */

	tgbEndHashTblPopulate (pDrvCtrl);

	return;
}

/*****************************************************************************
 *
 * tgbEndStart - start the device
 *
 * This function resets the device to put it into a known state and
 * then configures it for RX and TX operation. The RX and TX configuration
 * registers are initialized, and the address of the RX DMA window is
 * loaded into the device. Interrupts are then enabled, and the initial
 * link state is configured.
 *
 * Note that this routine also checks to see if an alternate jobQueue
 * has been specified via the vxbParam subsystem. This allows the driver
 * to divert its work to an alternate processing task, such as may be
 * done with TIPC. This means that the jobQueue can be changed while
 * the system is running, but the device must be stopped and restarted
 * for the change to take effect.
 *
 * RETURNS: ERROR if device initialization failed, otherwise OK
 *
 * ERRNO: N/A
 */

LOCAL STATUS tgbEndStart
(
		END_OBJ * pEnd
)
{
	TGB_DRV_CTRL * pDrvCtrl;
	PCI_DEV * pDev;
	//    VXB_INST_PARAM_VALUE val;
	//    HEND_RX_QUEUE_PARAM * pRxQueue;
	TGB_ADV_RDESC * pDesc;
	M_BLK_ID pMblk;
#ifdef TGB_VXB_DMA_BUF
	VXB_DMA_MAP_ID pMap;
#else
	ptrdiff_t addr;
#endif
	int i;
	u32 dctrl = 0;
	u32 pcap = 0;
	u32 value = 0;

	pDrvCtrl = (TGB_DRV_CTRL *)pEnd;
	pDev = pDrvCtrl->tgbDev;

	printk("<**DEBUG**> [%s():_%d_]:: [txgbe%d] IN\n", __FUNCTION__, __LINE__,pDev->unitNumber);
	
	
#if 0//wgq add to avoid NULL ptr
	printk("%s-%d pDev->pDrvCtrl=%p\n",__func__,__LINE__,pDev->pDrvCtrl);

	//
	//    printk("--pdev ==  %p bar=%p\n",pDev1,((TGB_DRV_CTRL *)(pDev1)->pDrvCtrl)->tgbBar);
	//    printk("pDev1 ==  %p \n",pDev1);
	//
	printk("-bar=%p\n",pDrvCtrl->tgbBar); 
	printk("--pDrvCtrl ==  %p pDrvCtrl_1=%p\n",pDrvCtrl,pDev->pDrvCtrl);
	//    if(pDrvCtrl != pDev->pDrvCtrl){
	//    	pDev->pDrvCtrl = pDrvCtrl;
	//    }

	PCI_DEV * pDev1 = pDrvCtrl->tgbDev;;
	printk("pdev ==  %p pDev1)->pDrvCtrl = %p bar=%p\n",pDev1,pDev1->pDrvCtrl,((TGB_DRV_CTRL *)(pDev1)->pDrvCtrl)->tgbBar);

#endif
	

	semTake (pDrvCtrl->tgbDevSem, WAIT_FOREVER);
	END_TX_SEM_TAKE (pEnd, WAIT_FOREVER);

	if (pEnd->flags & IFF_UP)
	{
		END_TX_SEM_GIVE (pEnd);
		semGive (pDrvCtrl->tgbDevSem);
		return (OK);
	}

	/* Initialize job queues */

	pDrvCtrl->tgbJobQueue = netJobQueueId;

	/* Override the job queue ID if the user supplied an alternate one. */

//    if (vxbInstParamByNameGet (pDev, "rxQueue00",
//        VXB_PARAM_POINTER, &val) == OK)
//        {
//        pRxQueue = (HEND_RX_QUEUE_PARAM *) val.pValue;
//        if (pRxQueue->jobQueId != NULL)
//            pDrvCtrl->tgbJobQueue = pRxQueue->jobQueId;
//        }

	QJOB_SET_PRI(&pDrvCtrl->tgbIntJob, NET_TASK_QJOB_PRI);
	pDrvCtrl->tgbIntJob.func = tgbEndIntHandle;

	atomic_set (&pDrvCtrl->tgbIntPending, FALSE);

	tgbReset (pDev);

	/* descriptor check bit */
#if 0
	for (i = 0; i < 4; i++)
	{
		TXGBE_CSR_WRITE_4(pDev, 0x180B0 + 4 * i, 0xFFFFFFFF);
	}
#endif

/* Backplane*/
	if ((pDrvCtrl->subSystemId & 0xF0) == TGB_ID_KR_KX_KX4)
	{
		tgbSetupMacLink(pDev, TGB_LINK_SPEED_AUTONEG);
	}

	/* Set up the RX ring. */

	bzero ((char *)pDrvCtrl->tgbRxDescMem,
			sizeof(TGB_ADV_RDESC) * TGB_RX_DESC_CNT);

	for (i = 0; i < TGB_RX_DESC_CNT; i++)
	{
		pMblk = endPoolTupleGet (pDrvCtrl->tgbEndObj.pNetPool);
		if (pMblk == NULL)
		{
			/* TODO: cleanup */
			END_TX_SEM_GIVE (pEnd);
			semGive (pDrvCtrl->tgbDevSem);
			printk(" %s:%d  endPoolTupleGet fail\r\n",__func__,__LINE__);
			return (ERROR);
		}

		pMblk->m_next = NULL;

		(void)cache_invalidate (DATA_CACHE, pMblk->m_data, 2048);

		TGB_ADJ (pMblk);
		pDrvCtrl->tgbRxMblk[i] = pMblk;

		pDesc = &pDrvCtrl->tgbRxDescMem[i];

		//        printk("pMblk->m_data = 0x%lx\r\n", pMblk->m_data);

#ifdef TGB_VXB_DMA_BUF

		pMap = pDrvCtrl->tgbRxMblkMap[i];

		vxbDmaBufMapMblkLoad (pDev, pDrvCtrl->tgbMblkTag, pMap, pMblk, 0);

		/*
		 * Currently, pointers in VxWorks are always 32 bits, even on
		 * 64-bit architectures. This means that for the time being,
		 * we can save a few cycles by only setting the addrlo field
		 * in DMA descriptors and leaving the addrhi field set to 0.
		 * When pointers become 64 bits wide, this will need to be
		 * changed.
		 */

#ifdef TGB_64
		pDesc->read.tgb_addrlo =
				htole32(TGB_ADDR_LO(pMap->fragList[0].frag));
		pDesc->read.tgb_addrhi =
				htole32(TGB_ADDR_HI(pMap->fragList[0].frag));
#else
		pDesc->read.tgb_addrlo = htole32((u32)pMap->fragList[0].frag);
		pDesc->read.tgb_addrhi = 0;
#endif

#else  /* not TGB_VXB_DMA_BUF */
		{
			addr = (ptrdiff_t)pMblk->m_data;
			addr = SYS_VIRT_TO_PHY(addr);
			pDesc->read.tgb_addrlo = htole32(addr);
			pDesc->read.tgb_addrhi = 0;
		}
#endif /* TGB_VXB_DMA_BUF */

		pDesc->read.tgb_hdrlo = 0;
		pDesc->read.tgb_hdrhi = 0;
	}

	/* Set up the TX ring. */

	bzero ((char *)pDrvCtrl->tgbTxDescMem,
			sizeof(TGB_ADV_TDESC) * TGB_TX_DESC_CNT);

	bzero ((char *)pDrvCtrl->isb_mem,
			sizeof(u32) * TGB_ISB_MAX);

#ifdef TGB_VXB_DMA_BUF
	/* Load the maps for the RX and TX DMA ring. */

	vxbDmaBufMapLoad (pDev, pDrvCtrl->tgbRxDescTag,
			pDrvCtrl->tgbRxDescMap, pDrvCtrl->tgbRxDescMem,
			sizeof(TGB_ADV_RDESC) * TGB_RX_DESC_CNT, 0);

	vxbDmaBufMapLoad (pDev, pDrvCtrl->tgbTxDescTag,
			pDrvCtrl->tgbTxDescMap, pDrvCtrl->tgbTxDescMem,
			sizeof(TGB_ADV_TDESC) * TGB_TX_DESC_CNT, 0);

	vxbDmaBufMapLoad (pDev, pDrvCtrl->tgbIsbTag,
			pDrvCtrl->tgbIsbMap, pDrvCtrl->isb_mem,
			sizeof(u32) * TGB_ISB_MAX, 0);
#endif

/* Initialize state */
	pDrvCtrl->tgbRxIdx = 0;
	pDrvCtrl->tgbMoreRx = 0;
	pDrvCtrl->tgbTxStall = FALSE;
	pDrvCtrl->tgbTxProd = 0;
	pDrvCtrl->tgbTxCons = 0;
	pDrvCtrl->tgbTxFree = TGB_TX_DESC_CNT - 1;
	pDrvCtrl->tgbLastCtx = 0xFFFF;
#ifdef CSUM_IPHDR_OFFSET
	pDrvCtrl->tgbLastOffsets = -1;
#else
	pDrvCtrl->tgbLastIpLen = -1;
#endif
	pDrvCtrl->tgbLastVlan = 0;

	/* Program the RX filter. */

	tgbEndRxConfig (pDrvCtrl);

	/* set Receive Packet Buffer Size to 512KB */
	TXGBE_CSR_WRITE_4(pDev, TGB_RDB_PB_SZ(0), 512 << 10);
#ifdef _TXGBE_DBG_
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x19020): 0x%x,  except: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_RDB_PB_SZ(0)), (512 << 10));
#endif

	/* set Transmit Packet Buffer Size to 160KB */
	//    TXGBE_CSR_WRITE_4(pDev, TGB_TDB_PB_SZ(0), TGB_TDB_PB_SZ_MAX);
	//    TXGBE_CSR_WRITE_4(pDev, TGB_TDM_PB_THRE(0), TGB_TDB_PB_SZ_MAX/1024 -
	//                                      TGB_TXPKT_SIZE_MAX);

	/* Initialize RX and TX queue zero */
	TXGBE_CSR_CLRBIT_4(pDev, TGB_PX_RR_CFG(0), TGB_PX_RR_CFG_RR_EN);
	TXGBE_CSR_CLRBIT_4(pDev, TGB_PX_TR_CFG(0), TGB_PX_TR_CFG_ENABLE);
#ifdef _TXGBE_DBG_
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x01010): 0x%x, clear bit:0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_PX_RR_CFG(0)), TGB_PX_RR_CFG_RR_EN);
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x03010): 0x%x, clear bit:0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_PX_TR_CFG(0)), TGB_PX_TR_CFG_ENABLE);
#endif

	/* configure tx ring */
#ifdef TGB_VXB_DMA_BUF
	TXGBE_CSR_WRITE_4(pDev, TGB_PX_TR_BAH(0),
			TGB_ADDR_HI(pDrvCtrl->tgbTxDescMap->fragList[0].frag));
	TXGBE_CSR_WRITE_4(pDev, TGB_PX_TR_BAL(0),
			TGB_ADDR_LO(pDrvCtrl->tgbTxDescMap->fragList[0].frag));
	TXGBE_CSR_WRITE_4(pDev, TGB_PX_TR_BAH(0), 0);
#else
	addr = (ptrdiff_t)pDrvCtrl->tgbTxDescMem;
	addr = SYS_VIRT_TO_PHY(addr);
	TXGBE_CSR_WRITE_4(pDev, TGB_PX_TR_BAL(0), (addr & 0xFFFFFFFF));
	TXGBE_CSR_WRITE_4(pDev, TGB_PX_TR_BAH(0), (addr >> 32));
#ifdef _TXGBE_DBG_
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x03000): 0x%x,  except: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_PX_TR_BAL(0)), (addr & 0xFFFFFFFF));
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x03004): 0x%x,  except: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_PX_TR_BAH(0)), (addr >> 32));
#endif /*_TXGBE_DBG_*/
#endif

	CACHE_PIPE_FLUSH();//zhaoyf
	TXGBE_CSR_WRITE_4(pDev, TGB_PX_TR_WP(0), 0);
	TXGBE_CSR_WRITE_4(pDev, TGB_PX_TR_RP(0), 0);
#ifdef _TXGBE_DBG_
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x03008): 0x%x,  except: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_PX_TR_WP(0)), 0);
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x0300C): 0x%x,  except: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_PX_TR_RP(0)), 0);
#endif /*_TXGBE_DBG_*/

	dctrl = TXGBE_CSR_READ_4(pDev, TGB_PX_TR_CFG(0));
	dctrl |= (TGB_TX_DESC_CNT / 128) << TGB_PX_TR_CFG_TR_SIZE_SHIFT |
			0x20 << TGB_PX_TR_CFG_WTHRESH_SHIFT;
	TXGBE_CSR_WRITE_4(pDev, TGB_PX_TR_CFG(0), dctrl);
#ifdef _TXGBE_DBG_
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x03010): 0x%x,  except: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_PX_TR_CFG(0)), dctrl);
#endif

	/* configure rx ring */
#ifdef TGB_VXB_DMA_BUF
	TXGBE_CSR_WRITE_4(pDev, TGB_PX_RR_BAH(0),
			TGB_ADDR_HI(pDrvCtrl->tgbRxDescMap->fragList[0].frag));
	TXGBE_CSR_WRITE_4(pDev, TGB_PX_RR_BAL(0),
			TGB_ADDR_LO(pDrvCtrl->tgbRxDescMap->fragList[0].frag));
#else
	addr = (ptrdiff_t)pDrvCtrl->tgbRxDescMem;
	addr = SYS_VIRT_TO_PHY(addr);
	TXGBE_CSR_WRITE_4(pDev, TGB_PX_RR_BAL(0), (addr & 0xFFFFFFFF));
	TXGBE_CSR_WRITE_4(pDev, TGB_PX_RR_BAH(0), (addr >> 32));
#ifdef _TXGBE_DBG_
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x01000): 0x%x,  except: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_PX_RR_BAL(0)), (addr & 0xFFFFFFFF));
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x01004): 0x%x,  except: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_PX_RR_BAH(0)), (addr >> 32));
#endif /*_TXGBE_DBG_*/
#endif

	TXGBE_CSR_WRITE_4(pDev, TGB_PX_RR_WP(0), TGB_RX_DESC_CNT - 1);
	TXGBE_CSR_WRITE_4(pDev, TGB_PX_RR_RP(0), 0);
#ifdef _TXGBE_DBG_
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x01008): 0x%x,  except: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_PX_RR_WP(0)), (TGB_RX_DESC_CNT - 1));
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x0100C): 0x%x,  except: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_PX_RR_RP(0)), (0));
#endif /*_TXGBE_DBG_*/

	dctrl = TXGBE_CSR_READ_4(pDev, TGB_PX_RR_CFG(0));
	dctrl |= (TGB_RX_DESC_CNT / 128) << TGB_PX_RR_CFG_RR_SIZE_SHIFT |
			0x1 << TGB_PX_RR_CFG_RR_THER_SHIFT;
	dctrl &= ~(TGB_PX_RR_CFG_RR_HDR_SZ |
			TGB_PX_RR_CFG_RR_BUF_SZ |
			TGB_PX_RR_CFG_SPLIT_MODE);
	dctrl |= TGB_PX_RR_CFG_HDR_SIZE << TGB_PX_RR_CFG_BSIZEHDRSIZE_SHIFT;

	/* write 2 to bit[11:8] indicate 2KB RR buf size */
	/* it may lay mine if true MTU > 2000 but < TGB_JUMBO_MTU,
	 * thus a packet is too big for a ring buffer to convoy, it may cost
	 * more than 1 descriptor which caused non-EOP, but we did not process
	 * non-EOP descriptor, it finally leads to recv halt.
	 */
	if (pDrvCtrl->tgbMaxMtu == TGB_JUMBO_MTU)
		dctrl |= TGB_PX_RR_CFG_BSIZE_10K >> TGB_PX_RR_CFG_BSIZEPKT_SHIFT;
	else
		dctrl |= TGB_PX_RR_CFG_BSIZE_DEFAULT >> TGB_PX_RR_CFG_BSIZEPKT_SHIFT;
	TXGBE_CSR_WRITE_4(pDev, TGB_PX_RR_CFG(0), dctrl);
#ifdef _TXGBE_DBG_
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x01010): 0x%x,  except: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_PX_RR_CFG(0)), dctrl);
#endif


	/* Select advanced RX descriptors and enable VLAN stripping */

	if (pDrvCtrl->tgbCaps.cap_enabled & IFCAP_VLAN_HWTAGGING)
		TXGBE_CSR_SETBIT_4(pDev, TGB_PX_RR_CFG(0), TGB_PX_RR_CFG_VLAN);
	else
		TXGBE_CSR_CLRBIT_4(pDev, TGB_PX_RR_CFG(0), TGB_PX_RR_CFG_VLAN);

	/* Set frame sizes */

	if (pDrvCtrl->tgbMaxMtu == TGB_JUMBO_MTU)
	{
		//TXGBE_CSR_WRITE_4(pDev, TGB_PX_RR_CFG(0), 0x00000A00U);
		TXGBE_CSR_WRITE_4(pDev, TGB_PSR_MAX_SZ, 9018 & 0x0000FFFF);
		TXGBE_CSR_SETBIT_4(pDev, TGB_MAC_RX_CFG, TGB_MAC_RX_CFG_JE);
#ifdef _TXGBE_DBG_
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x15020): 0x%x,  except: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_PSR_MAX_SZ), (9018 & 0x0000FFFF));
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x11004): 0x%x, set_bit: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_MAC_RX_CFG), TGB_MAC_RX_CFG_JE);
#endif
	}
	else
	{
		//TXGBE_CSR_WRITE_4(pDev, TGB_PX_RR_CFG(0), 0x00000200U);
		TXGBE_CSR_WRITE_4(pDev, TGB_PSR_MAX_SZ, 1518 & 0x0000FFFF);
		TXGBE_CSR_CLRBIT_4(pDev, TGB_MAC_RX_CFG, TGB_MAC_RX_CFG_JE);
#ifdef _TXGBE_DBG_
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x15020): 0x%x,  except: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_PSR_MAX_SZ), (1518 & 0x0000FFFF));
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x11004): 0x%x, clear_bit: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_MAC_RX_CFG), TGB_MAC_RX_CFG_JE);
#endif
	}

	TXGBE_CSR_SETBIT_4(pDev, TGB_MAC_RX_CFG, TGB_MAC_RX_CFG_RE);
#ifdef _TXGBE_DBG_
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x11004): 0x%x, set_bit: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_MAC_RX_CFG), TGB_MAC_RX_CFG_RE);
#endif

	if (pDrvCtrl->tgbDevType == TGB_DEVTYPE_SP)
	{
		TXGBE_CSR_SETBIT_4(pDev, TGB_RSEC_CTL, TGB_RSEC_CTL_CRC_STRIP);
#ifdef _TXGBE_DBG_
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x17000): 0x%x, set_bit: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_RSEC_CTL), TGB_RSEC_CTL_CRC_STRIP);
#endif
	}
	/* TODO: if autonegotiation is enabled, this bit should be set by software
       to the negotiated flow control value... */
	//    if (pDrvCtrl->tgbDevType == TGB_DEVTYPE_SP)
	/*
	 * Enable auto-negotiation between the MAC & PHY;
	 * the MAC will advertise clause 37 flow control.
	 */
	pcap |= TGB_SR_MII_MMD_AN_ADV_PAUSE_SYM |
			TGB_SR_MII_MMD_AN_ADV_PAUSE_ASM;
	value = tgb_rd32_epcs(pDev, TGB_SR_MII_MMD_AN_ADV);
	value = (value & ~(TGB_SR_MII_MMD_AN_ADV_PAUSE_ASM |
			TGB_SR_MII_MMD_AN_ADV_PAUSE_SYM)) | pcap;
	tgb_wr32_epcs(pDev, TGB_SR_MII_MMD_AN_ADV, value);

	//enable RX flow control
	TXGBE_CSR_SETBIT_4(pDev, TGB_MAC_RX_FLOW_CTRL, TGB_MAC_RX_FLOW_CTRL_RFE);
	TXGBE_CSR_SETBIT_4(pDev, TGB_RDB_RFCC, TGB_RDB_RFCC_RFCE_802_3X);
#ifdef _TXGBE_DBG_
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x11090): 0x%x, set_bit: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_MAC_RX_FLOW_CTRL), TGB_MAC_RX_FLOW_CTRL_RFE);
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x192A4): 0x%x, set_bit: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_RDB_RFCC), TGB_RDB_RFCC_RFCE_802_3X);
#endif

	/*
	 * Enable interrupts
	 * We assign RX queue 0 events to interrupt source 0 and TX
	 * queue 0 events to interrupt source 1.
	 */
#ifdef TGB_VXB_DMA_BUF
	TXGBE_CSR_WRITE_4(pDev, TGB_PX_ISB_ADDR_L,
			TGB_ADDR_LO(pDrvCtrl->tgbIsbMap->fragList[0].frag));

#ifdef TGB_64
	TXGBE_CSR_WRITE_4(pDev, TGB_PX_ISB_ADDR_H,
			TGB_ADDR_HI(pDrvCtrl->tgbIsbMap->fragList[0].frag));
#else
	TXGBE_CSR_WRITE_4(pDev, TGB_PX_ISB_ADDR_H, 0);
#endif /* TGB_64 */

#else /* not TGB_VXB_DMA_BUF */
	addr = (ptrdiff_t)pDrvCtrl->isb_mem;
	addr = SYS_VIRT_TO_PHY(addr);
	TXGBE_CSR_WRITE_4(pDev, TGB_PX_ISB_ADDR_L, (addr & 0xFFFFFFFF));
#ifdef _TXGBE_DBG_
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x160): 0x%x,  except: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_PX_ISB_ADDR_L), (addr & 0xFFFFFFFF));
#endif /*_TXGBE_DBG_*/
	
#ifdef TGB_64
	TXGBE_CSR_WRITE_4(pDev, TGB_PX_ISB_ADDR_H,  (addr >> 32));
#ifdef _TXGBE_DBG_
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x164): 0x%x,  except: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_PX_ISB_ADDR_H), (addr >> 32));
#endif /*_TXGBE_DBG_*/
	
#else
	TXGBE_CSR_WRITE_4(pDev, TGB_PX_ISB_ADDR_H, 0);
#endif /* TGB_64 */
#endif /* TGB_VXB_DMA_BUF */

	/* Legacy interrupt and MSI interrupt mode naturally use single-vector
	 * model. MSIX mode could be configured to use single-vector model.
	 * Single-vector model doesn鈥檛 have 1-on-1 mapping, Bit 0 of px_ims0
	 * is the only active mask bit using this model.
	 * Only MSIX mode could be configured to use multi-vector model.
	 */
	TXGBE_CSR_WRITE_4(pDev, TGB_PX_GPIE, 0);
#ifdef _TXGBE_DBG_
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x118): 0x%x,  except: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_PX_GPIE), (0));
#endif

	/* reset TSEC buf almost empty to 0x10, the default value is 0x19 */
	//    TXGBE_CSR_WRITE_4(pDev, TGB_TSEC_BUF_AE,
	//        (TXGBE_CSR_READ_4(pDev, TGB_TSEC_BUF_AE) & ~0x3FF) | 0x10);
	
	tgbMuxConnect(pDev,0);/*安装中断处理函数*/

	//    vxbIntEnable (pDev, 0, tgbEndInt, pDrvCtrl);
	if (pDrvCtrl->tgbDevType == TGB_DEVTYPE_SP)
	{
		tgbIvarWxSet (pDev);
	}

	/* enable irq */
	TXGBE_CSR_WRITE_4(pDev, TGB_PX_MISC_IEN, TGB_PX_MISC_IC_ETH_LKDN |
			TGB_PX_MISC_IC_ETH_LK);
#ifdef _TXGBE_DBG_
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x108): 0x%x,  except: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_PX_MISC_IEN), (TGB_PX_MISC_IC_ETH_LKDN | TGB_PX_MISC_IC_ETH_LK));
#endif
	//    TXGBE_CSR_WRITE_4(pDev, TGB_PX_IMC(0), 0x3);
	TXGBE_CSR_WRITE_4(pDev, TGB_PX_IMC(0),0xFFFFFFFF);
#ifdef _TXGBE_DBG_
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x150): 0x%x,  except: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_PX_IMC(0)), (0xFFFFFFFF));
#endif

	/* Initialize transmitter. */
	TXGBE_CSR_SETBIT_4(pDev, TGB_TDM_CTL, TGB_TDM_CTL_TE);
	TXGBE_CSR_SETBIT_4(pDev, TGB_MAC_TX_CFG, TGB_MAC_TX_CFG_TE |
			TGB_MAC_TX_CFG_SPEED_10G);
#ifdef _TXGBE_DBG_
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x18000): 0x%x, set_bit: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_TDM_CTL), TGB_TDM_CTL_TE);
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x11000): 0x%x, set_bit: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_MAC_TX_CFG), TGB_MAC_TX_CFG, TGB_MAC_TX_CFG_TE | TGB_MAC_TX_CFG_SPEED_10G);
#endif

	TXGBE_CSR_SETBIT_4(pDev, TGB_PX_RR_CFG(0), TGB_PX_RR_CFG_RR_EN);
	TXGBE_CSR_SETBIT_4(pDev, TGB_PX_TR_CFG(0), TGB_PX_TR_CFG_ENABLE);
#ifdef _TXGBE_DBG_
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x01010): 0x%x, set_bit: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_PX_RR_CFG(0)), TGB_PX_RR_CFG_RR_EN);
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x03010): 0x%x, set_bit: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_PX_TR_CFG(0)), TGB_PX_TR_CFG_ENABLE);
#endif

	/* Enable receiver and transmitter */
	TXGBE_CSR_SETBIT_4(pDev, TGB_RDB_PB_CTL, TGB_RDB_PB_CTL_RXEN);
	TXGBE_CSR_SETBIT_4(pDev, TGB_CFG_PORT_CTL, TGB_CFG_PORT_CTL_DRV_LOAD);
#ifdef _TXGBE_DBG_
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x19000): 0x%x, set_bit: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_RDB_PB_CTL), TGB_RDB_PB_CTL_RXEN);
	printk("<**DEBUG**> [%s():_%d_]:: TXGBE_CSR_READ_4(0x14400): 0x%x, set_bit: 0x%x\n", 
			__FUNCTION__, __LINE__,TXGBE_CSR_READ_4(pDev, TGB_CFG_PORT_CTL), TGB_CFG_PORT_CTL_DRV_LOAD);
#endif

	/* Set initial link state */

	pDrvCtrl->tgbCurMedia = IFM_ETHER|IFM_NONE;
	pDrvCtrl->tgbCurStatus = IFM_AVALID;
	printk("<**DEBUG**> [%s():_%d_]:: (IFF_UP | IFF_RUNNING):0x%lx\n", __FUNCTION__, __LINE__,(IFF_UP | IFF_RUNNING));
	printk("<**DEBUG**> [%s():_%d_]:: NETDRV_FLAGS_GET:0x%lx\n", __FUNCTION__, __LINE__,NETDRV_FLAGS_GET(pEnd));
	NETDRV_FLAGS_SET (pEnd, (IFF_UP | IFF_RUNNING));
	printk("<**DEBUG**> [%s():_%d_]:: NETDRV_FLAGS_GET:0x%lx\n", __FUNCTION__, __LINE__,NETDRV_FLAGS_GET(pEnd));
	tgbLinkUpdate (pDev);
	END_TX_SEM_GIVE (pEnd);
	semGive (pDrvCtrl->tgbDevSem);

	printk("<**DEBUG**> [%s():_%d_]:: [txgbe%d] Return\n", __FUNCTION__, __LINE__,pDev->unitNumber);
	return (OK);
}

/*****************************************************************************
 *
 * tgbEndStop - stop the device
 *
 * This function undoes the effects of tgbEndStart(). The device is shut
 * down and all resources are released. Note that the shutdown process
 * pauses to wait for all pending RX, TX and link event jobs that may have
 * been initiated by the interrupt handler to complete. This is done
 * to prevent tNetTask from accessing any data that might be released by
 * this routine.
 *
 * RETURNS: ERROR if device shutdown failed, otherwise OK
 *
 * ERRNO: N/A
 */

LOCAL STATUS tgbEndStop
(
		END_OBJ * pEnd
)
{
	TGB_DRV_CTRL * pDrvCtrl;
	PCI_DEV * pDev;
	int i;

	pDrvCtrl = (TGB_DRV_CTRL *)pEnd;
	pDev = pDrvCtrl->tgbDev;

	semTake (pDrvCtrl->tgbDevSem, WAIT_FOREVER);

	if (!(pEnd->flags & IFF_UP))
	{
		semGive (pDrvCtrl->tgbDevSem);
		return (OK);
	}

	/* Disable interrupts */
	/*vxbIntDisable (pDev, 0, teiEndInt, pDrvCtrl);*/
	TXGBE_CSR_WRITE_4(pDev, TGB_PX_MISC_IEN, 0);

	/*
	 * Wait for all jobs to drain.
	 * Note: this must be done before we disable the receiver
	 * and transmitter below. If someone tries to reboot us via
	 * WDB, this routine may be invoked while the RX handler is
	 * still running in tNetTask. If we disable the chip while
	 * that function is running, it'll start reading inconsistent
	 * status from the chip. We have to wait for that job to
	 * terminate first, then we can disable the receiver and
	 * transmitter.
	 */

	for (i = 0; i < TGB_TIMEOUT; i++)
	{
		if (atomic_get (&pDrvCtrl->tgbIntPending) == FALSE)
			break;
		pthread_delay(1);
	}

	if (i == TGB_TIMEOUT)
		logMsg("%s%d: timed out waiting for job to complete\n",
				(ptrdiff_t)TGB_NAME, pDev->unitNumber, 0, 0, 0, 0);

	END_TX_SEM_TAKE (pEnd, WAIT_FOREVER);

	END_FLAGS_CLR (pEnd, (IFF_UP | IFF_RUNNING));

	/* Disable RX and TX. */

	tgbReset (pDev);

	/* Release resources */

#ifdef TGB_VXB_DMA_BUF
	vxbDmaBufMapUnload (pDrvCtrl->tgbRxDescTag, pDrvCtrl->tgbRxDescMap);
	vxbDmaBufMapUnload (pDrvCtrl->tgbTxDescTag, pDrvCtrl->tgbTxDescMap);
	vxbDmaBufMapUnload (pDrvCtrl->tgbIsbTag, pDrvCtrl->tgbIsbMap);
#endif

	for (i = 0; i < TGB_RX_DESC_CNT; i++)
	{
		if (pDrvCtrl->tgbRxMblk[i] != NULL)
		{
			netMblkClChainFree (pDrvCtrl->tgbRxMblk[i]);
			pDrvCtrl->tgbRxMblk[i] = NULL;
#ifdef TGB_VXB_DMA_BUF
			vxbDmaBufMapUnload (pDrvCtrl->tgbMblkTag,
					pDrvCtrl->tgbRxMblkMap[i]);
#endif
		}
	}
	endMcacheFlush ();

	for (i = 0; i < TGB_TX_DESC_CNT; i++)
	{
		if (pDrvCtrl->tgbTxMblk[i] != NULL)
		{
			netMblkClChainFree (pDrvCtrl->tgbTxMblk[i]);
			pDrvCtrl->tgbTxMblk[i] = NULL;
#ifdef TGB_VXB_DMA_BUF
			vxbDmaBufMapUnload (pDrvCtrl->tgbMblkTag,
					pDrvCtrl->tgbTxMblkMap[i]);
#endif
		}
	}

	END_TX_SEM_GIVE (pEnd);
	semGive (pDrvCtrl->tgbDevSem);

	return (OK);
}

/***************************************************************************
 *
 * This routine is scheduled to run in tNetTask by the interrupt service
 * routine whenever a chip interrupt occurs. This function will check
 * what interrupt events are pending and schedule additional jobs to
 * service them. Once there are no more events waiting to be serviced
 * (i.e. the chip has gone idle), interrupts will be unmasked so that
 * the ISR can fire again.
 *
 * RETURNS: N/A
 *
 * ERRNO: N/A
 */

LOCAL void tgbEndIntHandle
(
		void * pArg
)
{
	QJOB *pJob;
	TGB_DRV_CTRL *pDrvCtrl;
	PCI_DEV * pDev;
	u32 status;
	UNUSED u32 icr;
	int loopCtrl;

	pJob = pArg;
	pDrvCtrl = member_to_object (pJob, TGB_DRV_CTRL, tgbIntJob);
	pDev = pDrvCtrl->tgbDev;
	icr = TXGBE_CSR_READ_4(pDev, TGB_PX_IMS(0)) & 0x3;
	
	gTxgbeIntHandleCount[pDev->unitNumber]++;/*ldf 20230905 add:: 中断处理次数统计*/
	
//	printk("<**DEBUG**> [%s():_%d_]:: [txgbe%d] 22222222\n", __FUNCTION__, __LINE__,pDev->unitNumber);

	/* Read queue interrupts here. */
	tgbEndTxHandle (pDrvCtrl);
	loopCtrl = tgbEndRxHandle (pDrvCtrl);
	if (loopCtrl == 0)
	{
		if(0 != jobQueuePost (pDrvCtrl->tgbJobQueue, &pDrvCtrl->tgbIntJob))
		{
			printk("<WARNING> [%s():_%d_]:: The netJobRing is full.\n", __FUNCTION__, __LINE__);
			/* we need unmask interrupts here */
			TXGBE_CSR_WRITE_4(pDev, TGB_PX_MISC_IEN, TGB_PX_MISC_IC_ETH_LKDN | TGB_PX_MISC_IC_ETH_LK);
			TXGBE_CSR_WRITE_4(pDev, TGB_PX_IMC(0), 0xFFFFFFFF);
			atomic_set((atomic_t *)&pDrvCtrl->tgbIntPending, FALSE);
		}
		return;
	}

	/* Read MISC interrupts here. */
	status = pDrvCtrl->isb_mem[TGB_ISB_MISC];
	pDrvCtrl->isb_mem[TGB_ISB_MISC] = 0;
	if (status & (TGB_PX_MISC_IC_ETH_LKDN | TGB_PX_MISC_IC_ETH_LK))
	{
		gTxgbeLinkIntCount[pDev->unitNumber]++;/*ldf 20230905 add:: link中断次数*/
		tgbLinkUpdate (pDev);
	}


	//    vxAtomicSet (&pDrvCtrl->tgbIntPending, FALSE);
	atomic_cas((atomic_t *)&pDrvCtrl->tgbIntPending, TRUE, FALSE);

	/* Unmask interrupts here */
	TXGBE_CSR_WRITE_4(pDev, TGB_PX_MISC_IEN, TGB_PX_MISC_IC_ETH_LKDN | TGB_PX_MISC_IC_ETH_LK);
	TXGBE_CSR_WRITE_4(pDev, TGB_PX_IMC(0), 0xFFFFFFFF);
//	TXGBE_CSR_WRITE_4(pDev, TGB_PX_IMC(0), 0x3);

//	printk("<**DEBUG**> [%s():_%d_]:: [txgbe%d] Return\n", __FUNCTION__, __LINE__,pDev->unitNumber);
	
	return;
}

/*****************************************************************************
 *
 * tgbEndInt - handle device interrupts
 *
 * This function is invoked whenever the wangxun tgb's interrupt line
 * is asserted.
 *
 * RETURNS: N/A
 *
 * ERRNO: N/A
 */

LOCAL void tgbEndInt
(
		TGB_DRV_CTRL * pDrvCtrl
)
{
	PCI_DEV * pDev;
	u32 val = 0;
	pDev = pDrvCtrl->tgbDev;
	int ret = 0;
	
//	printk("<**DEBUG**> [%s():_%d_]:: [txgbe%d] 11111111\n", __FUNCTION__, __LINE__,pDev->unitNumber);

	if (!(val = TXGBE_CSR_READ_4(pDev, TGB_PX_IMS(0)) & 0x3))
	{
//		printk("<WARNNING> [%s():_%d_]:: [txgbe%d] int status: 0x%x\n", __FUNCTION__, __LINE__,pDev->unitNumber,val);
		return;
	}
//	printk("<**DEBUG**> [%s():_%d_]:: [txgbe%d] int status: 0x%x\n", __FUNCTION__, __LINE__,pDev->unitNumber,val);

	gTxgbeIntCount[pDev->unitNumber]++;/*ldf 20230905 add:: 中断总次数统计*/
	
#ifdef __ENABLE_MSI__	
//	if(pDev->unitNumber == 1) /*ldf 20230905 add:: debug*/
//		printk("<**DEBUG**> [%s():_%d_]:: [txgbe%d] int status: 0x%x,  clear msi int!\n", __FUNCTION__, __LINE__,pDev->unitNumber,val);
	
	ret = msi_int_ack(pDev);/*xxx ldf 20230823 ignore::  清中断*/
//	if(ret < 0)/*ldf 20230830 add:: 如果不是当前port产生的中断，就直接退出*/
//		return;
#endif
	
	/* hwerrata for LEGACY and MSI only */
	TXGBE_CSR_WRITE_4(pDev, TGB_PX_INTA, 1);

	if (atomic_cas (&pDrvCtrl->tgbIntPending, FALSE, TRUE))
	{
		if(0 != jobQueuePost (pDrvCtrl->tgbJobQueue, &pDrvCtrl->tgbIntJob))
		{
			printk("<WARNING> [%s():_%d_]:: The netJobRing is full.\n", __FUNCTION__, __LINE__);
			/* we need unmask interrupts here */
			TXGBE_CSR_WRITE_4(pDev, TGB_PX_MISC_IEN, TGB_PX_MISC_IC_ETH_LKDN | TGB_PX_MISC_IC_ETH_LK);
			TXGBE_CSR_WRITE_4(pDev, TGB_PX_IMC(0), 0xFFFFFFFF);
			atomic_set((atomic_t *)&pDrvCtrl->tgbIntPending, FALSE);
		}
	}
	else
	{
//		printk(" %s-%d  tgbIntPending!!\r\n",__func__,__LINE__);
	}
	
	
//	printk("<**DEBUG**> [%s():_%d_]:: [txgbe%d] Return\n", __FUNCTION__, __LINE__,pDev->unitNumber);

	return;
}

/******************************************************************************
 *
 * tgbEndRxHandle - process received frames
 *
 * This function is scheduled by the ISR to run in the context of tNetTask
 * whenever an RX interrupt is received. It processes packets from the
 * RX window and encapsulates them into mBlk tuples which are handed up
 * to the MUX.
 *
 * There may be several packets waiting in the window to be processed.
 * We take care not to process too many packets in a single run through
 * this function so as not to monopolize tNetTask and starve out other
 * jobs waiting in the jobQueue. If we detect that there's still more
 * packets waiting to be processed, we queue ourselves up for another
 * round of processing.
 *
 * RETURNS: N/A
 *
 * ERRNO: N/A
 */

LOCAL int tgbEndRxHandle
(
		TGB_DRV_CTRL *pDrvCtrl
)
{
	PCI_DEV * pDev;
	M_BLK_ID pMblk;
	M_BLK_ID pNewMblk;
	TGB_ADV_RDESC * pDesc;
#ifdef TGB_VXB_DMA_BUF
	VXB_DMA_MAP_ID pMap;
#endif
	int loopCounter = TGB_MAX_RX;
	pDev = pDrvCtrl->tgbDev;

	pDesc = (TGB_ADV_RDESC *)&pDrvCtrl->tgbRxDescMem[pDrvCtrl->tgbRxIdx];

	while ((pDesc->write.tgb_sts & htole16(TGB_ADV_RDESC_STS_DD)) && (loopCounter > 0))
	{
		loopCounter--;
		
		/* check desc done. if desc done is set to 1, continue */
//		if (!(pDesc->write.tgb_sts & htole16(TGB_ADV_RDESC_STS_DD)))
//			break;

#ifdef TGB_VXB_DMA_BUF
		pMap = pDrvCtrl->tgbRxMblkMap[pDrvCtrl->tgbRxIdx];
#endif
		pMblk = pDrvCtrl->tgbRxMblk[pDrvCtrl->tgbRxIdx];

		/*
		 * Ignore checksum errors here. They'll be handled by the
		 * checksum offload code below, or by the stack.
		 */

		pNewMblk = endPoolTupleGet (pDrvCtrl->tgbEndObj.pNetPool);
		if (pNewMblk == NULL)
		{
			printk("<WARNING> [%s():_%d_]:: [txgbe%d] out of mBlks at %d\n", __FUNCTION__, __LINE__,pDev->unitNumber,pDrvCtrl->tgbRxIdx);
#if 0 /*ldf 20230905 add:: 如果mBlks不够就break出去，等待下次再接收*/
			break;
#else
			pDrvCtrl->tgbLastError.errCode = END_ERR_NO_BUF;
			muxError (&pDrvCtrl->tgbEndObj, &pDrvCtrl->tgbLastError);

#ifdef TGB_VXB_DMA_BUF
			pDesc->read.tgb_addrlo =
					htole32(TGB_ADDR_LO(pMap->fragList[0].frag));
			pDesc->read.tgb_addrhi =
					htole32(TGB_ADDR_HI(pMap->fragList[0].frag));
#else
			{
				ptrdiff_t addr = (ptrdiff_t)pMblk->m_data;
				addr = SYS_VIRT_TO_PHY(addr);
				pDesc->read.tgb_addrlo = (addr & 0xFFFFFFFF);
				pDesc->read.tgb_addrhi = ((u64)addr >> 32);
			}
#endif
			pDesc->read.tgb_hdrlo = 0;
			pDesc->read.tgb_hdrhi = 0;
//            CACHE_PIPE_FLUSH();
			TXGBE_CSR_WRITE_4(pDev, TGB_PX_RR_WP(0), pDrvCtrl->tgbRxIdx);
			TGB_INC_DESC(pDrvCtrl->tgbRxIdx, TGB_RX_DESC_CNT);
			pDesc = (TGB_ADV_RDESC *)&pDrvCtrl->tgbRxDescMem[pDrvCtrl->tgbRxIdx];
			continue;
#endif/*ldf 20230905 add end:: 如果mBlks不够就break出去，等待下次再接收*/
		}
		
		gTxgbeRxHandleCount[pDev->unitNumber]++;/*ldf 20230905 add:: 实际的接收处理统计*/

		/* Sync the packet buffer and unload the map. */
#ifdef TGB_VXB_DMA_BUF
		vxbDmaBufSync (pDev, pDrvCtrl->tgbMblkTag,
				pMap, VXB_DMABUFSYNC_PREREAD);
		vxbDmaBufMapUnload (pDrvCtrl->tgbMblkTag, pMap);
#else
		/* pre-invalidate the replacement buffer */
//        CACHE_DATA_INVALIDATE (pNewMblk->m_data, pNewMblk->m_len);
//        cache_invalidate(DATA_CACHE, pNewMblk->m_data, 2048);
#endif

		/* Swap the mBlks. */
		pDrvCtrl->tgbRxMblk[pDrvCtrl->tgbRxIdx] = pNewMblk;
		pNewMblk->m_next = NULL;
		/* Note, the data space lengths in pNewMblk were set by
           endPoolTupleGet(). */
		TGB_ADJ (pNewMblk);

#ifdef TGB_VXB_DMA_BUF
		/*
		 * Take advantage of the fact that we know there's only
		 * going to be one buffer, and use the single buffer map
		 * load routine instead of the mBlk load routine.
		 */

		vxbDmaBufMapLoad (pDev, pDrvCtrl->tgbMblkTag, pMap,
				pNewMblk->m_data, pNewMblk->m_len, 0);

		pDesc->read.tgb_addrlo = htole32(TGB_ADDR_LO(pMap->fragList[0].frag));
		pDesc->read.tgb_addrhi = htole32(TGB_ADDR_HI(pMap->fragList[0].frag));
#else
		{
			ptrdiff_t addr = (ptrdiff_t)pNewMblk->m_data;
			addr = SYS_VIRT_TO_PHY(addr);
			pDesc->read.tgb_addrlo = (addr & 0xFFFFFFFF);
			pDesc->read.tgb_addrhi = ((u64)addr >> 32);
		}
#endif

		pMblk->m_len = pMblk->m_pkthdr.len = le16toh(pDesc->write.tgb_pkt_buflen);
		pMblk->m_flags = M_PKTHDR|M_EXT;


		/* Handle checksum offload. */

		if (pDrvCtrl->tgbCaps.cap_enabled & IFCAP_RXCSUM)
		{
			if (pDesc->write.tgb_sts & htole16(TGB_ADV_RDESC_STS_IPCS))
				pMblk->m_pkthdr.csum_flags |= CSUM_IP_CHECKED;
			if (!(pDesc->write.tgb_err & htole16(TGB_ADV_RDESC_ERRSTS_IPE)))
				pMblk->m_pkthdr.csum_flags |= CSUM_IP_VALID;
			if ((pDesc->write.tgb_sts & htole16(TGB_ADV_RDESC_STS_UDPCV)) &&
					!(pDesc->write.tgb_err & htole16(TGB_ADV_RDESC_ERR_TCPE)))
			{
				pMblk->m_pkthdr.csum_flags |= CSUM_DATA_VALID|CSUM_PSEUDO_HDR;
				pMblk->m_pkthdr.csum_data = 0xffff;
			}
		}

		/* Handle VLAN tags */

		if (pDrvCtrl->tgbCaps.cap_enabled & IFCAP_VLAN_HWTAGGING)
		{
			if (pDesc->write.tgb_sts & htole32(TGB_ADV_RDESC_STS_VP))
			{
				pMblk->m_pkthdr.csum_flags |= CSUM_VLAN;
				pMblk->m_pkthdr.vlan = le16toh(pDesc->write.tgb_vlan);
			}
		}

		/* Reset this descriptor's status fields. */

		pDesc->read.tgb_hdrlo = 0;
		pDesc->read.tgb_hdrhi = 0;

		/* Advance to the next descriptor */
		//        CACHE_PIPE_FLUSH();
		cache_invalidate(DATA_CACHE, pMblk->mBlkHdr.mData, pMblk->m_len);

		TXGBE_CSR_WRITE_4(pDev, TGB_PX_RR_WP(0), pDrvCtrl->tgbRxIdx);
		TGB_INC_DESC(pDrvCtrl->tgbRxIdx, TGB_RX_DESC_CNT);

#if 0 /*ldf 20230904 add:: debug*/
		printk("RX: txgbe%d  |  ",	pDev->unitNumber);
		DumpUint8(pMblk->mBlkHdr.mData, pMblk->m_len);
#endif
		
		END_RCV_RTN_CALL (&pDrvCtrl->tgbEndObj, pMblk);
		pDesc = (TGB_ADV_RDESC *)&pDrvCtrl->tgbRxDescMem[pDrvCtrl->tgbRxIdx];
	}

	return loopCounter;
}

/******************************************************************************
 *
 * tgbEndTbdClean - clean the TX ring
 *
 * This function is called from both tgbEndSend() or tgbEndTxHandle() to
 * reclaim TX descriptors, and to free packets and DMA maps associated
 * with completed transmits.  This routine must be called with the END
 * TX semaphore already held.
 *
 * RETURNS: N/A
 *
 * ERRNO: N/A
 */

LOCAL void tgbEndTbdClean
(
		TGB_DRV_CTRL * pDrvCtrl
)
{
	UNUSED PCI_DEV * pDev;
#ifdef TGB_VXB_DMA_BUF
	VXB_DMA_MAP_ID pMap;
#endif
	TGB_ADV_TDESC * pDesc;
	M_BLK_ID pMblk;

	pDev = pDrvCtrl->tgbDev;

	while (pDrvCtrl->tgbTxFree < (TGB_TX_DESC_CNT - 1))
	{
		pDesc = &pDrvCtrl->tgbTxDescMem[pDrvCtrl->tgbTxCons];
		/* if desc done is set, continue;if not, exit the clean process */
		if ((pDesc->tgb_sts & htole32(TGB_ADV_STS_DD)) == 0)
		{
			break;
		}
		
		gTxgbeTxHandleCount[pDev->unitNumber]++; /*ldf 20230905 add:: 实际发送处理的总次数*/

		pMblk = pDrvCtrl->tgbTxMblk[pDrvCtrl->tgbTxCons];
#ifdef TGB_VXB_DMA_BUF
		pMap = pDrvCtrl->tgbTxMblkMap[pDrvCtrl->tgbTxCons];
#endif

		/* free used mblk */
		if (pMblk != NULL)
		{
#ifdef TGB_VXB_DMA_BUF
			vxbDmaBufMapUnload (pDrvCtrl->tgbMblkTag, pMap);
#endif
			endPoolTupleFree (pMblk);
			pDrvCtrl->tgbTxMblk[pDrvCtrl->tgbTxCons] = NULL;
		}
		//skip:
		pDrvCtrl->tgbTxFree++;
		/* move to next desc */
		TGB_INC_DESC (pDrvCtrl->tgbTxCons, TGB_TX_DESC_CNT);

		/*
		 * We forcibly clear out the addrhi field in the descriptor.
		 * All other fields are always initialized each time a
		 * descriptor is consumed, but the addrhi field is only
		 * used for TCP/IP context setup descriptors. After a
		 * context descriptor is completed, we need to reset the
		 * addrhi field in case the descriptor is used again later
		 * for a normal TX descriptor, otherwise we will end up
		 * telling the chip to DMA from an invalid address.
		 */
		pDesc->tgb_addrhi = 0;
	}

	return;
}

/******************************************************************************
 *
 * tgbEndTxHandle - process TX completion events
 *
 * This function is scheduled by the ISR to run in the context of tNetTask
 * whenever an TX interrupt is received. It runs through all of the
 * TX register pairs and checks the TX status to see how many have
 * completed. For each completed transmission, the associated TX mBlk
 * is released, and the outbound packet stats are updated.
 *
 * In the event that a TX underrun error is detected, the TX FIFO
 * threshold is increased. This will continue until the maximum TX
 * FIFO threshold is reached.
 *
 * If the transmitter has stalled, this routine will also call muxTxRestart()
 * to drain any packets that may be waiting in the protocol send queues,
 *
 * RETURNS: N/A
 *
 * ERRNO: N/A
 */

LOCAL void tgbEndTxHandle
(
		TGB_DRV_CTRL *pDrvCtrl
)
{
	u32 origFree;

	END_TX_SEM_TAKE (&pDrvCtrl->tgbEndObj, WAIT_FOREVER);
	origFree = pDrvCtrl->tgbTxFree;
	tgbEndTbdClean (pDrvCtrl);

	/* check real-time stall happened, if not, try to restore */
	if (pDrvCtrl->tgbTxStall && origFree < pDrvCtrl->tgbTxFree)
	{
		pDrvCtrl->tgbTxStall = FALSE;
		origFree = ~0U; /* flag that we should TX restart */
	}
	END_TX_SEM_GIVE (&pDrvCtrl->tgbEndObj);

	if (origFree == ~0U)
		muxTxRestart (pDrvCtrl);

	return;
}

/******************************************************************************
 *
 * tgbEndEncap - encapsulate an outbound packet using advanced descriptors
 *
 * This function sets up a descriptor for a packet transmit operation.
 * With the WangXun ethernet controller, the TX DMA ring consists of
 * descriptors that each describe a single packet fragment. We consume
 * as many descriptors as there are mBlks in the outgoing packet, unless
 * the chain is too long. The length is limited by the number of DMA
 * segments we want to allow in a given DMA map. If there are too many
 * segments, this routine will fail, and the caller must coalesce the
 * data into fewer buffers and try again.
 *
 * The WangXun also has context descriptors that are used to set
 * up what kind of checksum offload will be used for the frames that
 * follow. One context descriptor is required whenever the frame payload
 * changes between UDP and TCP.
 *
 * This routine will also fail if there aren't enough free descriptors
 * available in the ring, in which case the caller must defer the
 * transmission until more descriptors are completed by the chip.
 *
 * RETURNS: ENOSPC if there are too many fragments in the packet, EAGAIN
 * if the DMA ring is full, otherwise OK.
 *
 * ERRNO: N/A
 */

LOCAL int tgbEndEncap
(
		TGB_DRV_CTRL * pDrvCtrl,
		M_BLK_ID pMblk
)
{
	UNUSED PCI_DEV * pDev;
	TGB_ADV_TDESC * pDesc = NULL;
	TGB_ADV_CDESC * pCtx;
	UNUSED u32 firstIdx, lastIdx = 0;
	BOOL ctx_used = FALSE;
	u32 cmd;

#ifdef TGB_VXB_DMA_BUF
	int i;
	VXB_DMA_MAP_ID pMap;
#else
	M_BLK * pTmp;
	u32 nFrags;
	ptrdiff_t addr;
#endif

	pDev = pDrvCtrl->tgbDev;
	firstIdx = pDrvCtrl->tgbTxProd;

#ifdef TGB_VXB_DMA_BUF
	pMap = pDrvCtrl->tgbTxMblkMap[pDrvCtrl->tgbTxProd];
#endif

	/* Set up a TCP/IP offload context descriptor if needed. */
	if (pMblk->m_pkthdr.csum_flags & (CSUM_VLAN|CSUM_IP|CSUM_UDP|CSUM_TCP|CSUM_UDPv6|CSUM_TCPv6))
	{
#ifdef CSUM_IPHDR_OFFSET
		int offsets = CSUM_IPHDR_OFFSET(pMblk) + (CSUM_IP_HDRLEN (pMblk) << 8);
		if (
				(pMblk->m_pkthdr.csum_flags != pDrvCtrl->tgbLastCtx) ||
				(offsets != pDrvCtrl->tgbLastOffsets) ||
#else
		if (
				(pMblk->m_pkthdr.csum_flags != pDrvCtrl->tgbLastCtx) ||
				(CSUM_IP_HDRLEN (pMblk) != pDrvCtrl->tgbLastIpLen) ||
#endif
				((pMblk->m_pkthdr.csum_flags & CSUM_VLAN) && (pMblk->m_pkthdr.vlan != pDrvCtrl->tgbLastVlan))
			)
		{
			pDrvCtrl->tgbLastCtx = pMblk->m_pkthdr.csum_flags;
#ifdef CSUM_IPHDR_OFFSET
			pDrvCtrl->tgbLastOffsets = offsets;
#else
			pDrvCtrl->tgbLastIpLen = CSUM_IP_HDRLEN (pMblk);
#endif
			pDrvCtrl->tgbLastVlan = pMblk->m_pkthdr.vlan;
			pCtx=(TGB_ADV_CDESC *)&pDrvCtrl->tgbTxDescMem[pDrvCtrl->tgbTxProd];
			pCtx->tgb_cmd = htole32(TGB_ADV_TDESC_DTYP_CTX);

//			if (pMblk->m_pkthdr.csum_flags & (CSUM_TCP|CSUM_TCPv6))
//				pCtx->tgb_cmd |= htole32(TGB_ADV_CDESC_L4T_TCP);
//
//			if (pMblk->m_pkthdr.csum_flags & (CSUM_IP|CSUM_TCP|CSUM_UDP))
//				pCtx->tgb_cmd |= htole32(TGB_ADV_CDESC_CMD_IPV4);

			pCtx->tgb_macip = htole16(TGB_ADV_MACLEN(SIZEOF_ETHERHEADER));
			pCtx->tgb_macip |= htole16(TGB_ADV_IPLEN(CSUM_IP_HDRLEN(pMblk)));

			if (pMblk->m_pkthdr.csum_flags & CSUM_VLAN)
				pCtx->tgb_vlan = htole16(pMblk->m_pkthdr.vlan);
			
			pDrvCtrl->tgbTxFree--;

			lastIdx = pDrvCtrl->tgbTxProd;
			TGB_INC_DESC(pDrvCtrl->tgbTxProd, TGB_TX_DESC_CNT);
			ctx_used = TRUE;
		}
	}

#ifdef TGB_VXB_DMA_BUF
	pMap = pDrvCtrl->tgbTxMblkMap[pDrvCtrl->tgbTxProd];

	/*
	 * Load the DMA map to build the segment list.
	 * This will fail if there are too many segments.
	 */

	if (vxbDmaBufMapMblkLoad (pDev, pDrvCtrl->tgbMblkTag,
			pMap, pMblk, 0) != OK || (pMap->nFrags > pDrvCtrl->tgbTxFree))
	{
		vxbDmaBufMapUnload (pDrvCtrl->tgbMblkTag, pMap);
		return (ENOSPC); /* XXX what about the context descriptor? */
	}

	for (i = 0; i < pMap->nFrags; i++)
	{
		pDesc = (TGB_ADV_TDESC *)&pDrvCtrl->tgbTxDescMem[pDrvCtrl->tgbTxProd];
#ifdef TGB_64
		pDesc->tgb_addrlo = htole32(TGB_ADDR_LO(pMap->fragList[i].frag));
		pDesc->tgb_addrhi = htole32(TGB_ADDR_HI(pMap->fragList[i].frag));
#else
		pDesc->tgb_addrlo = htole32((u32)pMap->fragList[i].frag);
#endif

		pDesc->tgb_cmd = htole32(TGB_ADV_TDESC_DTYP_DSC|TGB_ADV_TDESC_CMD_RS|
				TGB_ADV_TDESC_CMD_IFCS);
		pDesc->tgb_cmd |= htole32(pMap->fragList[i].fragLen);
		pDesc->tgb_sts = 0;

		if (i == 0)
		{
			pDesc->tgb_sts |=
					htole32(TGB_DESC_STATUS_PAYLEN(pMblk->m_pkthdr.len));
		}
		pDrvCtrl->tgbTxFree--;
		lastIdx = pDrvCtrl->tgbTxProd;
		TGB_INC_DESC(pDrvCtrl->tgbTxProd, TGB_TX_DESC_CNT);
	}

#else /* not TGB_VXB_DMA_BUF */
//	nFrags = 0;
//	for (pTmp = pMblk; pTmp != NULL; pTmp = pTmp->m_next)
//	{
//		if (pTmp->m_len != 0)
//			++nFrags;
//	}
//	/* not enough free desc */
//	if (nFrags > pDrvCtrl->tgbTxFree)
//	{
//		printk("<WARNNING> [%s():_%d_]:: [txgbe%d]  not enough free TXdesc!\n", __func__, __LINE__, pDev->unitNumber);
//		if (ctx_used)
//		{
//			pDrvCtrl->tgbTxFree++;
//			pDrvCtrl->tgbTxProd = lastIdx; 
//		}
//
//		return ENOSPC;
//	}

	nFrags = 0;
	for (pTmp = pMblk; pTmp != NULL; pTmp = pTmp->m_next)
	{
		if ((cmd = pTmp->m_len) == 0)
			continue;

		gTxgbeSendCount[pDev->unitNumber]++;/*ldf 20230904 add:: 实际的发送统计*/
		
#if 0 /*ldf 20230904 add:: debug*/
		printk("TX: txgbe%d  |  ",	pDev->unitNumber);
		DumpUint8(pTmp->m_data, pTmp->m_len);
#endif

		pDesc = (TGB_ADV_TDESC *)&pDrvCtrl->tgbTxDescMem[pDrvCtrl->tgbTxProd];

		addr = (ptrdiff_t)pTmp->m_data;
//		CACHE_DATA_FLUSH (addr, cmd);
//		cache_flush(DATA_CACHE, (unsigned long)addr, cmd);
		MEM_BARRIER_RW1();/*ldf 20230905 add*/

		addr = SYS_VIRT_TO_PHY(addr);
		pDesc->tgb_addrlo = (addr & 0xFFFFFFFF);
		pDesc->tgb_addrhi= ((u64)addr >> 32);

		cmd |= TGB_ADV_TDESC_DTYP_DSC |
				TGB_ADV_TDESC_CMD_IFCS |
				TGB_ADV_TDESC_CMD_RS;

		pDesc->tgb_cmd = htole32(cmd);
		pDesc->tgb_sts = 0;

		if (nFrags++ == 0)
		{
			pDesc->tgb_sts |=
					htole32(TGB_DESC_STATUS_PAYLEN(pMblk->m_pkthdr.len));
		}

		pDrvCtrl->tgbTxFree--;
		lastIdx = pDrvCtrl->tgbTxProd;
		TGB_INC_DESC(pDrvCtrl->tgbTxProd, TGB_TX_DESC_CNT);
	}

#endif /* TGB_VXB_DMA_BUF */

	if (pMblk->m_pkthdr.csum_flags & CSUM_VLAN)
		pDesc->tgb_cmd |= htole32(TGB_ADV_TDESC_CMD_VLE);

	pDesc->tgb_cmd |= htole32(TGB_ADV_TDESC_CMD_EOP|
			TGB_ADV_TDESC_CMD_RS);

	/* Save the mBlk for later. */
	pDrvCtrl->tgbTxMblk[lastIdx] = pMblk;

#ifdef TGB_VXB_DMA_BUF
	/*
	 * Ensure that the map for this transmission
	 * is placed at the array index of the last descriptor
	 * in this chain.  (Swap last and first dmamaps.)
	 */

	pDrvCtrl->tgbTxMblkMap[firstIdx] = pDrvCtrl->tgbTxMblkMap[lastIdx];
	pDrvCtrl->tgbTxMblkMap[lastIdx] = pMap;

	/* Sync the buffer. */

	vxbDmaBufSync (pDev, pDrvCtrl->tgbMblkTag, pMap,
			VXB_DMABUFSYNC_POSTWRITE);
#endif

	return (OK);
}

/******************************************************************************
 *
 * tgbdSend - transmit packets
 *
 * This function transmit the packets specified in <pkt>.
 *
 * RETURNS: OK, ERROR, or END_ERR_BLOCK.
 *
 * ERRNO: N/A
 */

LOCAL int tgbEndSend
(
		END_OBJ * pEnd,
		M_BLK_ID pMblk
)
{

	TGB_DRV_CTRL * pDrvCtrl;
	PCI_DEV * pDev;
	M_BLK_ID pTmp;
	int rval;

	pDrvCtrl = (TGB_DRV_CTRL *)pEnd;

	pDev = pDrvCtrl->tgbDev;
	
	int nFrags = 0;
	for (pTmp = pMblk; pTmp != NULL; pTmp = pTmp->m_next)
	{
		if (pTmp->m_len != 0)
			++nFrags;
	}

	END_TX_SEM_TAKE (pEnd, WAIT_FOREVER);
	
	/* not enough free desc */
	if (nFrags > pDrvCtrl->tgbTxFree)
	{
		printk("<WARNNING> [%s():_%d_]:: [txgbe%d]  not enough free TXdesc!\n", __func__, __LINE__, pDev->unitNumber);
		goto blocked;
	}

	if (pDrvCtrl->tgbTxFree < 2 || !(pDrvCtrl->tgbCurStatus & IFM_ACTIVE)){
		printk("<WARNNING> [%s():_%d_]:: [txgbe%d]  Ran out of TX descriptors!\n", __func__, __LINE__, pDev->unitNumber);
		goto blocked;
	}

	/* Attempt early cleanup */
	if (pDrvCtrl->tgbTxFree < TGB_TX_DESC_CNT - TGB_TX_CLEAN_THRESH)
		tgbEndTbdClean (pDrvCtrl);

	rval = tgbEndEncap (pDrvCtrl, pMblk);
	/*
	 * If tgbEndEncap() returns ENOSPC, it means it ran out
	 * of TX descriptors and couldn't encapsulate the whole
	 * packet fragment chain. In that case, we need to
	 * coalesce everything into a single buffer and try
	 * again. If any other error is returned, then something
	 * went wrong, and we have to abort the transmission
	 * entirely.
	 */

	if (rval == ENOSPC)
	{
		if ((pTmp = endPoolTupleGet (pDrvCtrl->tgbEndObj.pNetPool)) == NULL){
			printk("<WARNNING> [%s():_%d_]:: [txgbe%d]  Ran out of TX descriptors!\n", __func__, __LINE__, pDev->unitNumber);
			goto blocked;
		}
		pTmp->m_len = pTmp->m_pkthdr.len = netMblkToBufCopy (pMblk, mtod(pTmp, char *), NULL);
		pTmp->m_flags = pMblk->m_flags;
		pTmp->m_pkthdr.csum_flags = pMblk->m_pkthdr.csum_flags;
		pTmp->m_pkthdr.csum_data = pMblk->m_pkthdr.csum_data;
		pTmp->m_pkthdr.vlan = pMblk->m_pkthdr.vlan;
		CSUM_IP_HDRLEN(pTmp) = CSUM_IP_HDRLEN(pMblk);
#ifdef CSUM_IPHDR_OFFSET
		CSUM_IPHDR_OFFSET(pTmp) = CSUM_IPHDR_OFFSET(pMblk);
#endif

		/* Try transmission again, should succeed this time. */
		rval = tgbEndEncap (pDrvCtrl, pTmp);
		if (rval == OK)
			endPoolTupleFree (pMblk);
		else
			endPoolTupleFree (pTmp);
	}


	/* Issue transmit command */
	CACHE_PIPE_FLUSH();
	TXGBE_CSR_WRITE_4(pDev, TGB_PX_TR_WP(0), pDrvCtrl->tgbTxProd);

	if (rval != OK){
		printk("<WARNNING> [%s():_%d_]:: [txgbe%d]  Ran out of TX descriptors!\n", __func__, __LINE__, pDev->unitNumber);
		goto blocked;
	}

	END_TX_SEM_GIVE (pEnd);

	return (0);

blocked:
	DBG_LOG("send blocked\n", 0,0,0,0,0,0);

	pDrvCtrl->tgbTxStall = TRUE;
	END_TX_SEM_GIVE (pEnd);

	return (END_ERR_BLOCK);
}

/******************************************************************************
 *
 * tgbEndPollSend - polled mode transmit routine
 *
 * This function is similar to the tgbEndSend() routine shown above, except
 * it performs transmissions synchronously with interrupts disabled. After
 * the transmission is initiated, the routine will poll the state of the
 * TX status register associated with the current slot until transmission
 * completed. If transmission times out, this routine will return ERROR.
 *
 * RETURNS: OK, EAGAIN, or ERROR
 *
 * ERRNO: N/A
 */

LOCAL STATUS tgbEndPollSend(END_OBJ * pEnd, M_BLK_ID pMblk)
{
	TGB_DRV_CTRL * pDrvCtrl;
	PCI_DEV * pDev;
#ifdef TGB_VXB_DMA_BUF
	VXB_DMA_MAP_ID pMap;
#endif
	TGB_ADV_TDESC * pDesc;
	M_BLK_ID pTmp;
	int len, i;

	pDrvCtrl = (TGB_DRV_CTRL *)pEnd;

	if (pDrvCtrl->tgbPolling == FALSE)
		return (ERROR);

	pDev = pDrvCtrl->tgbDev;
	pTmp = pDrvCtrl->tgbPollBuf;

	len = netMblkToBufCopy (pMblk, mtod(pTmp, char *), NULL);
	pTmp->m_len = pTmp->m_pkthdr.len = len;
	pTmp->m_flags = pMblk->m_flags;
	pTmp->m_pkthdr.csum_flags = pMblk->m_pkthdr.csum_flags;
	pTmp->m_pkthdr.csum_data = pMblk->m_pkthdr.csum_data;
	pTmp->m_pkthdr.vlan = pMblk->m_pkthdr.vlan;
	CSUM_IP_HDRLEN(pTmp) = CSUM_IP_HDRLEN(pMblk);

	/* Encapsulate buffer */

	if (tgbEndEncap (pDrvCtrl, pTmp) != OK)
		return (EAGAIN);

	/* Issue transmit command */

	TXGBE_CSR_WRITE_4(pDev, TGB_PX_TR_RP(0), pDrvCtrl->tgbTxProd);

	/* Wait for transmission to complete */

	for (i = 0; i < TGB_TIMEOUT; i++)
	{
		if (TXGBE_CSR_READ_4(pDev, TGB_PX_TR_WP(0)) ==
				TXGBE_CSR_READ_4(pDev, TGB_PX_TR_RP(0)))
			break;
	}

	TXGBE_CSR_WRITE_4(pDev, TGB_PX_IMC(0), 0x00000001);

	/* Remember to unload the map once transmit completes. */

	FOREVER
	{
		pDesc = &pDrvCtrl->tgbTxDescMem[pDrvCtrl->tgbTxCons];
		pDesc->tgb_addrhi = 0;
		pTmp = pDrvCtrl->tgbTxMblk[pDrvCtrl->tgbTxCons];
		if (pDrvCtrl->tgbTxMblk[pDrvCtrl->tgbTxCons] != NULL)
		{
#ifdef TGB_VXB_DMA_BUF
			pMap = pDrvCtrl->tgbTxMblkMap[pDrvCtrl->tgbTxCons];
			vxbDmaBufMapUnload (pDrvCtrl->tgbMblkTag, pMap);
#endif
			pDrvCtrl->tgbTxMblk[pDrvCtrl->tgbTxCons] = NULL;
		}
		pDrvCtrl->tgbTxFree++;
		TGB_INC_DESC(pDrvCtrl->tgbTxCons, TGB_TX_DESC_CNT);
		if (pDesc->tgb_cmd & htole32(TGB_ADV_TDESC_CMD_EOP))
			break;
	}

	if (i == TGB_TIMEOUT)
		return (ERROR);

	return (OK);
}

/******************************************************************************
 *
 * tgbEndPollReceive - polled mode receive routine
 *
 * This function receive a packet in polled mode, with interrupts disabled.
 * It's similar in operation to the tgbEndRxHandle() routine, except it
 * doesn't process more than one packet at a time and does not load out
 * buffers. Instead, the caller supplied an mBlk tuple into which this
 * function will place the received packet.
 *
 * If no packet is available, this routine will return EAGAIN. If the
 * supplied mBlk is too small to contain the received frame, the routine
 * will return ERROR.
 *
 * RETURNS: OK, EAGAIN, or ERROR
 *
 * ERRNO: N/A
 */

LOCAL int tgbEndPollReceive(END_OBJ * pEnd, M_BLK_ID pMblk)
{
	TGB_DRV_CTRL * pDrvCtrl;
	PCI_DEV * pDev;
#ifdef TGB_VXB_DMA_BUF
	VXB_DMA_MAP_ID pMap;
#endif
	TGB_ADV_RDESC * pDesc;
	M_BLK_ID pPkt;
	int rval = EAGAIN;
	UINT16 rxLen = 0;

	pDrvCtrl = (TGB_DRV_CTRL *)pEnd;
	if (pDrvCtrl->tgbPolling == FALSE)
		return (ERROR);
	if (!(pMblk->m_flags & M_EXT))
		return (ERROR);

	pDev = pDrvCtrl->tgbDev;

	pDesc = (TGB_ADV_RDESC *)&pDrvCtrl->tgbRxDescMem[pDrvCtrl->tgbRxIdx];

	if (!(pDesc->write.tgb_sts & htole16(TGB_ADV_RDESC_STS_DD)))
		return (EAGAIN);

	TXGBE_CSR_WRITE_4(pDev, TGB_PX_IMC(0), 0x00000001);

	pPkt = pDrvCtrl->tgbRxMblk[pDrvCtrl->tgbRxIdx];

#ifdef TGB_VXB_DMA_BUF
pMap = pDrvCtrl->tgbRxMblkMap[pDrvCtrl->tgbRxIdx];
#endif
rxLen = le16toh(pDesc->write.tgb_pkt_buflen);

#ifndef TGB_SECRC
rxLen -= 4;
#endif

#ifdef TGB_VXB_DMA_BUF
vxbDmaBufSync (pDev, pDrvCtrl->tgbMblkTag, pMap, VXB_DMABUFSYNC_PREREAD);
#endif

/*
 * Ignore checksum errors here. They'll be handled by the
 * checksum offload code below, or by the stack.
 */

if (!(pDesc->write.tgb_err & htole16(TGB_RXD_ERR_RXE)))
{
	/* Handle checksum offload. */

	if (pDrvCtrl->tgbCaps.cap_enabled & IFCAP_RXCSUM)
	{
		if ((pDesc->write.tgb_sts & htole16(TGB_ADV_RDESC_STS_UDPCV)) &&
				!(pDesc->write.tgb_err & htole16(TGB_ADV_RDESC_ERR_TCPE)))
		{
			pMblk->m_pkthdr.csum_flags |= CSUM_DATA_VALID|CSUM_PSEUDO_HDR;
			pMblk->m_pkthdr.csum_data = 0xffff;
		}
	}

	/* Handle VLAN tags */

	if (pDrvCtrl->tgbCaps.cap_enabled & IFCAP_VLAN_HWTAGGING)
	{
		if (pDesc->write.tgb_sts & htole16(TGB_ADV_RDESC_STS_VP))
		{
			pMblk->m_pkthdr.csum_flags |= CSUM_VLAN;
			pMblk->m_pkthdr.vlan = le16toh(pDesc->write.tgb_vlan);
		}
	}

	pMblk->m_flags |= M_PKTHDR;
	pMblk->m_len = pMblk->m_pkthdr.len = rxLen;
	TGB_ADJ(pMblk);
	bcopy (mtod(pPkt, char *), mtod(pMblk, char *), rxLen);
	rval = OK;
}

#ifdef TGB_VXB_DMA_BUF
/*
 * Pre-invalidate the re-used buffer. Note that pPkt->m_len
 * was set by endPoolTupleGet(), and hasn't been changed,
 * but pPkt->m_data was adjusted up by 2 bytes.
 */
CACHE_DATA_INVALIDATE (pPkt->m_data - 2, pPkt->m_len);
#endif


/* Reset the descriptor */

#ifdef TGB_VXB_DMA_BUF
pDesc->read.tgb_addrlo = htole32(TGB_ADDR_LO(pMap->fragList[0].frag));
pDesc->read.tgb_addrhi = htole32(TGB_ADDR_HI(pMap->fragList[0].frag));
#else
{
	ptrdiff_t addr = (ptrdiff_t)pPkt->m_data;
	addr = SYS_VIRT_TO_PHY(addr);
	pDesc->read.tgb_addrlo = (addr & 0xFFFFFFFF);
	pDesc->read.tgb_addrhi = ((u64)addr >>32);
}
#endif
pDesc->read.tgb_hdrhi = 0;
pDesc->read.tgb_hdrlo = 0;

TXGBE_CSR_WRITE_4(pDev, TGB_PX_RR_RP(0), pDrvCtrl->tgbRxIdx);
TGB_INC_DESC(pDrvCtrl->tgbRxIdx, TGB_RX_DESC_CNT);

return (rval);

}

/*****************************************************************************
 *
 * tgb_rd32_epcs 
 *
 */
u32 tgb_rd32_epcs(PCI_DEV * pDev, u32 addr)
{
	u32 data;
	/* Set the LAN port indicator to portRegOffset[1] */
	/* 1st, write the regOffset to IDA_ADDR register */
	TXGBE_CSR_WRITE_4(pDev, TGB_XPCS_IDA_ADDR, addr);

	/* 2nd, read the data from IDA_DATA register */
	data = TXGBE_CSR_READ_4(pDev, TGB_XPCS_IDA_DATA);

	return data;
}

/*****************************************************************************
 *
 * tgb_wr32_ephy 
 *
 */
void tgb_wr32_ephy(PCI_DEV * pDev, u32 addr, u32 data)
{
	/* Set the LAN port indicator to portRegOffset[1] */
	/* 1st, write the regOffset to IDA_ADDR register */
	TXGBE_CSR_WRITE_4(pDev, TGB_ETHPHY_IDA_ADDR, addr);

	//2nd, read the data from IDA_DATA register
	TXGBE_CSR_WRITE_4(pDev, TGB_ETHPHY_IDA_DATA, data);
}

/*****************************************************************************
 *
 * tgb_wr32_epcs
 *
 */

void tgb_wr32_epcs(PCI_DEV * pDev, u32 addr, u32 data)
{
	/* Set the LAN port indicator to portRegOffset[1] */
	/* 1st, write the regOffset to IDA_ADDR register */
	TXGBE_CSR_WRITE_4(pDev, TGB_XPCS_IDA_ADDR, addr);

	/* 2nd, read the data from IDA_DATA register */
	TXGBE_CSR_WRITE_4(pDev, TGB_XPCS_IDA_DATA, data);
}

/*****************************************************************************
 *
 * tgbSetupMacLink - Set MAC link speed
 *
 */
int tgbSetupMacLink(PCI_DEV * pDev, u32 speed)
{
	TGB_DRV_CTRL * pDrvCtrl;
	pDrvCtrl = pDev->pDrvCtrl;

	BOOL autoneg = FALSE;
	int status = 0;
	u32 link_capabilities = TGB_LINK_SPEED_UNKNOWN;
	u32 link_speed = TGB_LINK_SPEED_UNKNOWN;
	BOOL link_up = FALSE;

	/* Check to see if speed passed in is supported. */
	status = tgbGetLinkCapabilities(pDev, &link_capabilities, &autoneg);
	if (status < 0)
		goto out;

	speed &= link_capabilities;

	if (speed == TGB_LINK_SPEED_UNKNOWN)
	{
		status = ERROR;
		goto out;
	}

	status = tgbCheckMacLink(pDev, &link_speed, &link_up);
	if (status != 0)
		goto out;
	if ((link_speed == speed) && link_up)
		goto out;

	if ((pDrvCtrl->subSystemId & 0xF0) == TGB_ID_KR_KX_KX4)
	{
		if (!autoneg)
		{
			switch (pDrvCtrl->phyLinkMode)
			{
			case TGB_PHYSICAL_LAYER_10GBASE_KR:
				tgb_set_link_to_kr(pDev, autoneg);
				break;
			case TGB_PHYSICAL_LAYER_10GBASE_KX4:
				tgb_set_link_to_kx4(pDev, autoneg);
				break;
			case TGB_PHYSICAL_LAYER_1000BASE_KX:
				tgb_set_link_to_kx(pDev, speed, autoneg);
				break;
			default:
				status = ERROR;
				goto out;
			}
		} else
		{
			//			tgb_set_link_to_kr_kx4_kx(hw, speed, autoneg);
		}

	}
	out:
	return status;
}

/*****************************************************************************
 *
 * tgbGetLinkCapabilities - Determines link capabilities
 *
 */
int tgbGetLinkCapabilities(PCI_DEV * pDev, u32 *speed, BOOL *autoneg)
{
	TGB_DRV_CTRL * pDrvCtrl;
	pDrvCtrl = pDev->pDrvCtrl;

	int status = 0;
	u32 sr_pcs_ctl;
	u32 sr_pma_mmd_ctl1;
	u32 sr_an_mmd_ctl;
	u32 sr_an_mmd_adv_reg2;

	/* KR KX KX4 */
	/*
	 * Determine link capabilities based on the stored value,
	 * which represents EEPROM defaults.  If value has not
	 * been stored, use the current register values.
	 */
	if (pDrvCtrl->orig_link_settings_stored)
	{
		sr_pcs_ctl = pDrvCtrl->orig_sr_pcs_ctl2;
		sr_pma_mmd_ctl1 = pDrvCtrl->orig_sr_pma_mmd_ctl1;
		sr_an_mmd_ctl = pDrvCtrl->orig_sr_an_mmd_ctl;
		sr_an_mmd_adv_reg2 = pDrvCtrl->orig_sr_an_mmd_adv_reg2;
	} else
	{
		sr_pcs_ctl = tgb_rd32_epcs(pDev, TGB_SR_PCS_CTL2);
		sr_pma_mmd_ctl1 = tgb_rd32_epcs(pDev, TGB_SR_PMA_MMD_CTL1);
		sr_an_mmd_ctl = tgb_rd32_epcs(pDev, TGB_SR_AN_MMD_CTL);
		sr_an_mmd_adv_reg2 = tgb_rd32_epcs(pDev, TGB_SR_AN_MMD_ADV_REG2);
	}

	if ((sr_pcs_ctl & TGB_SR_PCS_CTL2_PCS_TYPE_SEL_MASK) ==
			TGB_SR_PCS_CTL2_PCS_TYPE_SEL_X &&
			(sr_pma_mmd_ctl1 & TGB_SR_PMA_MMD_CTL1_SPEED_SEL_MASK)
			== TGB_SR_PMA_MMD_CTL1_SPEED_SEL_1G &&
			(sr_an_mmd_ctl & TGB_SR_AN_MMD_CTL_ENABLE) == 0)
	{
		//1G or KX - no backplane auto-negotiation
		*speed = TGB_LINK_SPEED_1GB_FULL;
		*autoneg = FALSE;
		pDrvCtrl->phyLinkMode = TGB_PHYSICAL_LAYER_1000BASE_KX;
	} else if ((sr_pcs_ctl & TGB_SR_PCS_CTL2_PCS_TYPE_SEL_MASK) ==
			TGB_SR_PCS_CTL2_PCS_TYPE_SEL_X &&
			(sr_pma_mmd_ctl1 & TGB_SR_PMA_MMD_CTL1_SPEED_SEL_MASK)
			== TGB_SR_PMA_MMD_CTL1_SPEED_SEL_10G &&
			(sr_an_mmd_ctl & TGB_SR_AN_MMD_CTL_ENABLE) == 0)
	{
		*speed = TGB_LINK_SPEED_10GB_FULL;
		*autoneg = FALSE;
		pDrvCtrl->phyLinkMode = TGB_PHYSICAL_LAYER_10GBASE_KX4;
	} else if ((sr_pcs_ctl & TGB_SR_PCS_CTL2_PCS_TYPE_SEL_MASK) ==
			TGB_SR_PCS_CTL2_PCS_TYPE_SEL_R &&
			(sr_an_mmd_ctl & TGB_SR_AN_MMD_CTL_ENABLE) == 0)
	{
		//10 GbE serial link (KR -no backplane auto-negotiation)
		*speed = TGB_LINK_SPEED_10GB_FULL;
		*autoneg = FALSE;
		pDrvCtrl->phyLinkMode = TGB_PHYSICAL_LAYER_10GBASE_KR;
	} else if ((sr_an_mmd_ctl & TGB_SR_AN_MMD_CTL_ENABLE))
	{
		//KX/KX4/KR backplane auto-negotiation enable
		*speed = TGB_LINK_SPEED_UNKNOWN;
		if (sr_an_mmd_adv_reg2 &
				TGB_SR_AN_MMD_ADV_REG2_BP_TYPE_KR)
			*speed |= TGB_LINK_SPEED_10GB_FULL;
		if (sr_an_mmd_adv_reg2 &
				TGB_SR_AN_MMD_ADV_REG2_BP_TYPE_KX4)
			*speed |= TGB_LINK_SPEED_10GB_FULL;
		if (sr_an_mmd_adv_reg2 &
				TGB_SR_AN_MMD_ADV_REG2_BP_TYPE_KX)
			*speed |= TGB_LINK_SPEED_1GB_FULL;
		*autoneg = TRUE;
		pDrvCtrl->phyLinkMode = TGB_PHYSICAL_LAYER_10GBASE_KR |
				TGB_PHYSICAL_LAYER_10GBASE_KX4 |
				TGB_PHYSICAL_LAYER_1000BASE_KX;
	} else
	{
		status = ERROR;
		goto out;
	}
	out:
	return status;
}

/*****************************************************************************
 *
 * tgbCheckMacLink - Determine link and speed status
 *
 * Reads the links register to determine if link is up and the current speed
 */

int tgbCheckMacLink(PCI_DEV * pDev, u32 *speed, BOOL *link_up)
{
	UNUSED TGB_DRV_CTRL * pDrvCtrl;
	pDrvCtrl = pDev->pDrvCtrl;
	u32 links_reg = 0;
	//	u32 i;
	//	UINT16 value;

	printk("<**DEBUG**> [%s():_%d_]:: IN\n", __FUNCTION__, __LINE__);
	
	links_reg = TXGBE_CSR_READ_4(pDev, TGB_CFG_PORT_ST);

	if (links_reg & TGB_CFG_PORT_ST_LINK_UP) 
	{
		printk(" tgb%d link up.\r\n", pDrvCtrl->lanId);
		*link_up = TRUE;
	} else 
	{
		printk(" tgb%d link down!\r\n", pDrvCtrl->lanId);
		*link_up = FALSE;
	}

	if (*link_up)
	{
		if ((links_reg & TGB_CFG_PORT_ST_LINK_10G) == TGB_CFG_PORT_ST_LINK_10G)
		{
			*speed = TGB_LINK_SPEED_10GB_FULL;
		} else if ((links_reg & TGB_CFG_PORT_ST_LINK_1G) == TGB_CFG_PORT_ST_LINK_1G)
		{
			*speed = TGB_LINK_SPEED_1GB_FULL;
		} else if ((links_reg & TGB_CFG_PORT_ST_LINK_100M) == TGB_CFG_PORT_ST_LINK_100M)
		{
			*speed = TGB_LINK_SPEED_100_FULL;
		} else
			*speed = TGB_LINK_SPEED_10_FULL;

	} else
	{
		*speed = TGB_LINK_SPEED_UNKNOWN;
	}
	
	printk("<**DEBUG**> [%s():_%d_]:: Return\n", __FUNCTION__, __LINE__);

	return 0;
}

/*****************************************************************************
 *
 * tgb_set_link_to_kr
 *
 */
int tgb_set_link_to_kr(PCI_DEV * pDev, BOOL autoneg)
{
	TGB_DRV_CTRL * pDrvCtrl;
	pDrvCtrl = pDev->pDrvCtrl;

	u32 i;
	int status = 0;

	/* 1. Wait xpcs power-up good */
	for (i = 0; i < TGB_XPCS_POWER_GOOD_MAX_POLLING_TIME; i++) {
		if ((tgb_rd32_epcs(pDev, TGB_VR_XS_OR_PCS_MMD_DIGI_STATUS) &
				TGB_VR_XS_OR_PCS_MMD_DIGI_STATUS_PSEQ_MASK) ==
						TGB_VR_XS_OR_PCS_MMD_DIGI_STATUS_PSEQ_POWER_GOOD)
			break;
		vxbMsDelay(10);
	}
	if (i == TGB_XPCS_POWER_GOOD_MAX_POLLING_TIME) {
		status = ERROR;
		goto out;
	}

	if (!autoneg) {
		/* 2. Disable xpcs AN-73 */
		tgb_wr32_epcs(pDev, TGB_SR_AN_MMD_CTL, 0x0);
		if (pDrvCtrl->revisionId != TGB_SP_MPW) {
			/* Disable PHY MPLLA for eth mode change(after ECO) */
			tgb_wr32_ephy(pDev, 0x4, 0x243A);
			TXGBE_CSR_READ_4(pDev, TGB_MIS_PWR);
			vxbMsDelay(1);
			/* Set the eth change_mode bit first in mis_rst register
			 * for corresponding LAN port 
			 */
			if (pDrvCtrl->lanId == 0)
				TXGBE_CSR_WRITE_4(pDev, TGB_MIS_RST, TGB_MIS_RST_LAN0_CHG_ETH_MODE);
			else
				TXGBE_CSR_WRITE_4(pDev, TGB_MIS_RST, TGB_MIS_RST_LAN1_CHG_ETH_MODE);
		}

		/* 3. Set VR_XS_PMA_Gen5_12G_MPLLA_CTRL3 Register */
		/* Bit[10:0](MPLLA_BANDWIDTH) = 11'd123 (default: 11'd16) */
		tgb_wr32_epcs(pDev, TGB_PHY_MPLLA_CTL3,
				TGB_PHY_MPLLA_CTL3_MULTIPLIER_BW_10GBASER_KR);

		/* 4. Set VR_XS_PMA_Gen5_12G_MISC_CTRL0 Register */
		/* Bit[12:8](RX_VREF_CTRL) = 5'hF (default: 5'h11) */
		tgb_wr32_epcs(pDev, TGB_PHY_MISC_CTL0, 0xCF00);

		/* 5. Set VR_XS_PMA_Gen5_12G_RX_EQ_CTRL0 Register */
		/* Bit[15:8](VGA1/2_GAIN_0) = 8'h77, Bit[7:5](CTLE_POLE_0) = 3'h2
		 * Bit[4:0](CTLE_BOOST_0) = 4'hA 
		 */
		tgb_wr32_epcs(pDev, TGB_PHY_RX_EQ_CTL0,
				0x774A);

		/* 6. Set VR_MII_Gen5_12G_RX_GENCTRL3 Register */
		/* Bit[2:0](LOS_TRSHLD_0) = 3'h4 (default: 3) */
		tgb_wr32_epcs(pDev, TGB_PHY_RX_GEN_CTL3, 0x0004);
		/* 7. Initialize the mode by setting VR XS or PCS MMD Digital */
		/* Control1 Register Bit[15](VR_RST) */
		tgb_wr32_epcs(pDev, TGB_VR_XS_OR_PCS_MMD_DIGI_CTL1, 0xA000);
		/* wait phy initialization done */
		for (i = 0; i < TGB_PHY_INIT_DONE_POLLING_TIME; i++) {
			if ((tgb_rd32_epcs(pDev,
					TGB_VR_XS_OR_PCS_MMD_DIGI_CTL1) &
					TGB_VR_XS_OR_PCS_MMD_DIGI_CTL1_VR_RST) == 0)
				break;
			vxbMsDelay(100);
		}
		if (i == TGB_PHY_INIT_DONE_POLLING_TIME) {
			status = ERROR;
			goto out;
		}
	} else {
		tgb_wr32_epcs(pDev, TGB_VR_AN_KR_MODE_CL,
				0x1);
	}
	out:
	return status;
}

/*****************************************************************************
 *
 * tgb_set_link_to_kx4
 *
 */
int tgb_set_link_to_kx4(PCI_DEV * pDev, BOOL autoneg)
{
	TGB_DRV_CTRL * pDrvCtrl;
	pDrvCtrl = pDev->pDrvCtrl;

	u32 i;
	int status = 0;
	u32 value;
	u32 temp;

	/* check link status, if already set, skip setting it again */
	//	if (hw->link_status == TGB_LINK_STATUS_KX4) 
	//	{
	//		goto out;
	//	}

	/* 1. Wait xpcs power-up good */
	for (i = 0; i < TGB_XPCS_POWER_GOOD_MAX_POLLING_TIME; i++) {
		if ((tgb_rd32_epcs(pDev, TGB_VR_XS_OR_PCS_MMD_DIGI_STATUS) &
				TGB_VR_XS_OR_PCS_MMD_DIGI_STATUS_PSEQ_MASK) ==
						TGB_VR_XS_OR_PCS_MMD_DIGI_STATUS_PSEQ_POWER_GOOD)
			break;
		vxbMsDelay(10);
	}
	if (i == TGB_XPCS_POWER_GOOD_MAX_POLLING_TIME) {
		status = ERROR;
		goto out;
	}

	temp = TXGBE_CSR_READ_4(pDev, TGB_MAC_TX_CFG);
	temp = ((temp & ~TGB_MAC_TX_CFG_TE) | (~TGB_MAC_TX_CFG_TE & TGB_MAC_TX_CFG_TE));
	TXGBE_CSR_WRITE_4(pDev, TGB_MAC_TX_CFG, temp);

	/* 2. Disable xpcs AN-73 */
	if (!autoneg)
		tgb_wr32_epcs(pDev, TGB_SR_AN_MMD_CTL, 0x0);
	else
		tgb_wr32_epcs(pDev, TGB_SR_AN_MMD_CTL, 0x3000);

	if (pDrvCtrl->revisionId == TGB_SP_MPW) {
		//Disable PHY MPLLA
		tgb_wr32_ephy(pDev, 0x4, 0x2501);
		//Reset rx lane0-3 clock
		tgb_wr32_ephy(pDev, 0x1005, 0x4001);
		tgb_wr32_ephy(pDev, 0x1105, 0x4001);
		tgb_wr32_ephy(pDev, 0x1205, 0x4001);
		tgb_wr32_ephy(pDev, 0x1305, 0x4001);
	} else {
		//Disable PHY MPLLA for eth mode change(after ECO)
		tgb_wr32_ephy(pDev, 0x4, 0x250A);
		TXGBE_CSR_READ_4(pDev, TGB_MIS_PWR);
		vxbMsDelay(1);

		//Set the eth change_mode bit first in mis_rst register
		//for corresponding LAN port
		if (pDrvCtrl->lanId == 0)
			TXGBE_CSR_WRITE_4(pDev, TGB_MIS_RST, TGB_MIS_RST_LAN0_CHG_ETH_MODE);
		else
			TXGBE_CSR_WRITE_4(pDev, TGB_MIS_RST, TGB_MIS_RST_LAN1_CHG_ETH_MODE);
	}

	//Set SR PCS Control2 Register Bits[1:0] = 2'b01  //PCS_TYPE_SEL: non KR
	tgb_wr32_epcs(pDev, TGB_SR_PCS_CTL2,
			TGB_SR_PCS_CTL2_PCS_TYPE_SEL_X);
	//Set SR PMA MMD Control1 Register Bit[13] = 1'b1  //SS13: 10G speed
	tgb_wr32_epcs(pDev, TGB_SR_PMA_MMD_CTL1,
			TGB_SR_PMA_MMD_CTL1_SPEED_SEL_10G);

	//value = tgb_rd32_epcs(hw, TGB_PHY_TX_GENCTRL1);
	value = (0xf5f0 & ~0x7F0) |  (0x5 << 8) | (0x7 << 5) | 0x10;
	tgb_wr32_epcs(pDev, TGB_PHY_TX_GENCTRL1, value);

	tgb_wr32_epcs(pDev, TGB_PHY_MISC_CTL0, 0x4F00);

	//value = tgb_rd32_epcs(hw, TGB_PHY_TX_EQ_CTL0);
	value = (0x1804 & ~0x3F3F);
	tgb_wr32_epcs(pDev, TGB_PHY_TX_EQ_CTL0, value);

	//value = tgb_rd32_epcs(hw, TGB_PHY_TX_EQ_CTL1);
	value = (0x50 & ~0x7F) | 40 | (1 << 6);
	tgb_wr32_epcs(pDev, TGB_PHY_TX_EQ_CTL1, value);

	for (i = 0; i < 4; i++) {
		//value = tgb_rd32_epcs(hw, TGB_PHY_RX_EQ_CTL0 + i);
		if (i == 0)
			value = (0x45 & ~0xFFFF) | (0x7 << 12) | (0x7 << 8) | 0x6;
		else
			value = (0xff06 & ~0xFFFF) | (0x7 << 12) | (0x7 << 8) | 0x6;
		tgb_wr32_epcs(pDev, TGB_PHY_RX_EQ_CTL0 + i, value);
	}

	//value = tgb_rd32_epcs(hw, TGB_PHY_RX_EQ_ATT_LVL0);
	value = 0x0 & ~0x7777;
	tgb_wr32_epcs(pDev, TGB_PHY_RX_EQ_ATT_LVL0, value);

	tgb_wr32_epcs(pDev, TGB_PHY_DFE_TAP_CTL0, 0x0);

	//value = tgb_rd32_epcs(hw, TGB_PHY_RX_GEN_CTL3);
	value = (0x6db & ~0xFFF) | (0x1 << 9) | (0x1 << 6) | (0x1 << 3) | 0x1;
	tgb_wr32_epcs(pDev, TGB_PHY_RX_GEN_CTL3, value);

	//Set VR XS, PMA, or MII Synopsys Enterprise Gen5 12G PHY MPLLA
	//Control 0 Register Bit[7:0] = 8'd40  //MPLLA_MULTIPLIER
	tgb_wr32_epcs(pDev, TGB_PHY_MPLLA_CTL0, TGB_PHY_MPLLA_CTL0_MULTIPLIER_OTHER);
	//Set VR XS, PMA or MII Synopsys Enterprise Gen5 12G PHY MPLLA
	//Control 3 Register Bit[10:0] = 11'd86  //MPLLA_BANDWIDTH
	tgb_wr32_epcs(pDev, TGB_PHY_MPLLA_CTL3, TGB_PHY_MPLLA_CTL3_MULTIPLIER_BW_OTHER);

	//Set VR XS, PMA, or MII Synopsys Enterprise Gen5 12G PHY VCO
	//Calibration Load 0 Register  Bit[12:0] = 13'd1360  //VCO_LD_VAL_0
	tgb_wr32_epcs(pDev, TGB_PHY_VCO_CAL_LD0, TGB_PHY_VCO_CAL_LD0_OTHER);

	//Set VR XS, PMA, or MII Synopsys Enterprise Gen5 12G PHY VCO
	//Calibration Load 1 Register  Bit[12:0] = 13'd1360  //VCO_LD_VAL_1
	tgb_wr32_epcs(pDev, TGB_PHY_VCO_CAL_LD1, TGB_PHY_VCO_CAL_LD0_OTHER);

	//Set VR XS, PMA, or MII Synopsys Enterprise Gen5 12G PHY VCO
	//Calibration Load 2 Register  Bit[12:0] = 13'd1360  //VCO_LD_VAL_2
	tgb_wr32_epcs(pDev, TGB_PHY_VCO_CAL_LD2, TGB_PHY_VCO_CAL_LD0_OTHER);
	//Set VR XS, PMA, or MII Synopsys Enterprise Gen5 12G PHY VCO
	//Calibration Load 3 Register  Bit[12:0] = 13'd1360  //VCO_LD_VAL_3
	tgb_wr32_epcs(pDev, TGB_PHY_VCO_CAL_LD3, TGB_PHY_VCO_CAL_LD0_OTHER);
	//Set VR XS, PMA, or MII Synopsys Enterprise Gen5 12G PHY VCO
	//Calibration Reference 0 Register Bit[5:0] = 6'd34  //VCO_REF_LD_0/1
	tgb_wr32_epcs(pDev, TGB_PHY_VCO_CAL_REF0, 0x2222);

	//Set VR XS, PMA, or MII Synopsys Enterprise Gen5 12G PHY VCO
	//Calibration Reference 1 Register Bit[5:0] = 6'd34  //VCO_REF_LD_2/3
	tgb_wr32_epcs(pDev, TGB_PHY_VCO_CAL_REF1, 0x2222);

	//Set VR XS, PMA, or MII Synopsys Enterprise Gen5 12G PHY AFE-DFE
	//Enable Register Bit[7:0] = 8'd0  //AFE_EN_0/3_1, DFE_EN_0/3_1
	tgb_wr32_epcs(pDev, TGB_PHY_AFE_DFE_ENABLE, 0x0);


	//Set  VR XS, PMA, or MII Synopsys Enterprise Gen5 12G PHY Rx
	//Equalization Control 4 Register Bit[3:0] = 4'd0  //CONT_ADAPT_0/3_1
	tgb_wr32_epcs(pDev, TGB_PHY_RX_EQ_CTL, 0x00F0);

	//Set VR XS, PMA, or MII Synopsys Enterprise Gen5 12G PHY Tx Rate
	//Control Register Bit[14:12], Bit[10:8], Bit[6:4], Bit[2:0],
	//all rates to 3'b010  //TX0/1/2/3_RATE
	tgb_wr32_epcs(pDev, TGB_PHY_TX_RATE_CTL, 0x2222);

	//Set VR XS, PMA, or MII Synopsys Enterprise Gen5 12G PHY Rx Rate
	//Control Register Bit[13:12], Bit[9:8], Bit[5:4], Bit[1:0],
	//all rates to 2'b10  //RX0/1/2/3_RATE
	tgb_wr32_epcs(pDev, TGB_PHY_RX_RATE_CTL, 0x2222);

	//Set VR XS, PMA, or MII Synopsys Enterprise Gen5 12G PHY Tx General
	//Control 2 Register Bit[15:8] = 2'b01  //TX0/1/2/3_WIDTH: 10bits
	tgb_wr32_epcs(pDev, TGB_PHY_TX_GEN_CTL2, 0x5500);
	//Set VR XS, PMA, or MII Synopsys Enterprise Gen5 12G PHY Rx General
	//Control 2 Register Bit[15:8] = 2'b01  //RX0/1/2/3_WIDTH: 10bits
	tgb_wr32_epcs(pDev, TGB_PHY_RX_GEN_CTL2, 0x5500);

	//Set VR XS, PMA, or MII Synopsys Enterprise Gen5 12G PHY MPLLA Control
	//2 Register Bit[10:8] = 3'b010
	//MPLLA_DIV16P5_CLK_EN=0, MPLLA_DIV10_CLK_EN=1, MPLLA_DIV8_CLK_EN=0
	tgb_wr32_epcs(pDev, TGB_PHY_MPLLA_CTL2, TGB_PHY_MPLLA_CTL2_DIV_CLK_EN_10);

	tgb_wr32_epcs(pDev, 0x1f0000, 0x0);
	tgb_wr32_epcs(pDev, 0x1f8001, 0x0);
	tgb_wr32_epcs(pDev, TGB_SR_MII_MMD_DIGI_CTL, 0x0);

	//10. Initialize the mode by setting VR XS or PCS MMD Digital Control1
	//Register Bit[15](VR_RST)
	tgb_wr32_epcs(pDev, TGB_VR_XS_OR_PCS_MMD_DIGI_CTL1, 0xA000);
	/* wait phy initialization done */
	for (i = 0; i < TGB_PHY_INIT_DONE_POLLING_TIME; i++) {
		if ((tgb_rd32_epcs(pDev, TGB_VR_XS_OR_PCS_MMD_DIGI_CTL1) & TGB_VR_XS_OR_PCS_MMD_DIGI_CTL1_VR_RST) == 0)
			break;
		vxbMsDelay(100);
	}

	//if success, set link status
	//	hw->link_status = TGB_LINK_STATUS_KX4;

	if (i == TGB_PHY_INIT_DONE_POLLING_TIME) {
		status = ERROR;
		goto out;
	}

	out:
	return status;

}

/*****************************************************************************
 *
 * tgb_set_link_to_kx
 *
 */
int tgb_set_link_to_kx(PCI_DEV * pDev, u32 speed, BOOL autoneg)
{
	TGB_DRV_CTRL * pDrvCtrl;
	pDrvCtrl = pDev->pDrvCtrl;

	u32 i;
	int status = 0;
	u32 wdata = 0;
	u32 value;
	u32 temp;

	/* check link status, if already set, skip setting it again */
	//	if (hw->link_status == TGB_LINK_STATUS_KX) {
	//		goto out;
	//	}

	/* 1. Wait xpcs power-up good */
	for (i = 0; i < TGB_XPCS_POWER_GOOD_MAX_POLLING_TIME; i++) {
		if ((tgb_rd32_epcs(pDev, TGB_VR_XS_OR_PCS_MMD_DIGI_STATUS) &
				TGB_VR_XS_OR_PCS_MMD_DIGI_STATUS_PSEQ_MASK) ==
						TGB_VR_XS_OR_PCS_MMD_DIGI_STATUS_PSEQ_POWER_GOOD)
			break;
		vxbMsDelay(10);
	}
	if (i == TGB_XPCS_POWER_GOOD_MAX_POLLING_TIME) {
		status = ERROR;
		goto out;
	}

	temp = TXGBE_CSR_READ_4(pDev, TGB_MAC_TX_CFG);
	temp = ((temp & ~TGB_MAC_TX_CFG_TE) | (~TGB_MAC_TX_CFG_TE & TGB_MAC_TX_CFG_TE));
	TXGBE_CSR_WRITE_4(pDev, TGB_MAC_TX_CFG, temp);

	/* 2. Disable xpcs AN-73 */
	if (!autoneg)
		tgb_wr32_epcs(pDev, TGB_SR_AN_MMD_CTL, 0x0);
	else
		tgb_wr32_epcs(pDev, TGB_SR_AN_MMD_CTL, 0x3000);

	if (pDrvCtrl->revisionId == TGB_SP_MPW) {
		/* Disable PHY MPLLA */
		tgb_wr32_ephy(pDev, 0x4, 0x2401);
		/* Reset rx lane0 clock */
		tgb_wr32_ephy(pDev, 0x1005, 0x4001);
	} else {
		/* Disable PHY MPLLA for eth mode change(after ECO) */
		tgb_wr32_ephy(pDev, 0x4, 0x240A);
		TXGBE_CSR_READ_4(pDev, TGB_MIS_PWR);
		vxbMsDelay(1);

		/* Set the eth change_mode bit first in mis_rst register */
		/* for corresponding LAN port */
		if (pDrvCtrl->lanId == 0)
			TXGBE_CSR_WRITE_4(pDev, TGB_MIS_RST, TGB_MIS_RST_LAN0_CHG_ETH_MODE);
		else
			TXGBE_CSR_WRITE_4(pDev, TGB_MIS_RST, TGB_MIS_RST_LAN1_CHG_ETH_MODE);
	}

	/* Set SR PCS Control2 Register Bits[1:0] = 2'b01  PCS_TYPE_SEL: non KR */
	tgb_wr32_epcs(pDev, TGB_SR_PCS_CTL2, TGB_SR_PCS_CTL2_PCS_TYPE_SEL_X);

	/* Set SR PMA MMD Control1 Register Bit[13] = 1'b0 SS13: 1G speed */
	tgb_wr32_epcs(pDev, TGB_SR_PMA_MMD_CTL1, TGB_SR_PMA_MMD_CTL1_SPEED_SEL_1G);

	/* Set SR MII MMD Control Register to corresponding speed: {Bit[6],
	 * Bit[13]}=[2'b00,2'b01,2'b10]->[10M,100M,1G]
	 */
	if (speed == TGB_LINK_SPEED_100_FULL)
		wdata = 0x2100;
	else if (speed == TGB_LINK_SPEED_1GB_FULL)
		wdata = 0x0140;
	else if (speed == TGB_LINK_SPEED_10_FULL)
		wdata = 0x0100;
	tgb_wr32_epcs(pDev, TGB_SR_MII_MMD_CTL, wdata);

	value = (0xf5f0 & ~0x710) |  (0x5 << 8);
	tgb_wr32_epcs(pDev, TGB_PHY_TX_GENCTRL1, value);

	tgb_wr32_epcs(pDev, TGB_PHY_MISC_CTL0, 0x4F00);

	value = (0x1804 & ~0x3F3F) | (24 << 8) | 4;
	tgb_wr32_epcs(pDev, TGB_PHY_TX_EQ_CTL0, value);

	value = (0x50 & ~0x7F) | 16 | (1 << 6);
	tgb_wr32_epcs(pDev, TGB_PHY_TX_EQ_CTL1, value);

	for (i = 0; i < 4; i++) {
		if (i) {
			value = 0xff06;
		} else {
			value = (0x45 & ~0xFFFF) | (0x7 << 12) | (0x7 << 8) | 0x6;
		}
		tgb_wr32_epcs(pDev, TGB_PHY_RX_EQ_CTL0 + i, value);
	}

	value = 0x0 & ~0x7;
	tgb_wr32_epcs(pDev, TGB_PHY_RX_EQ_ATT_LVL0, value);

	tgb_wr32_epcs(pDev, TGB_PHY_DFE_TAP_CTL0, 0x0);

	value = (0x6db & ~0x7) | 0x4;
	tgb_wr32_epcs(pDev, TGB_PHY_RX_GEN_CTL3, value);

	/* Set VR XS, PMA, or MII Synopsys Enterprise Gen5 12G PHY MPLLA Control
	 * 0 Register Bit[7:0] = 8'd32  MPLLA_MULTIPLIER
	 */
	tgb_wr32_epcs(pDev, TGB_PHY_MPLLA_CTL0, TGB_PHY_MPLLA_CTL0_MULTIPLIER_1GBASEX_KX);

	/* Set VR XS, PMA or MII Synopsys Enterprise Gen5 12G PHY MPLLA Control 3
	 * Register Bit[10:0] = 11'd70  MPLLA_BANDWIDTH
	 */
	tgb_wr32_epcs(pDev, TGB_PHY_MPLLA_CTL3, TGB_PHY_MPLLA_CTL3_MULTIPLIER_BW_1GBASEX_KX);

	/* Set VR XS, PMA, or MII Synopsys Enterprise Gen5 12G PHY VCO
	 * Calibration Load 0 Register  Bit[12:0] = 13'd1344  VCO_LD_VAL_0
	 */
	tgb_wr32_epcs(pDev, TGB_PHY_VCO_CAL_LD0,
			TGB_PHY_VCO_CAL_LD0_1GBASEX_KX);

	tgb_wr32_epcs(pDev, TGB_PHY_VCO_CAL_LD1, 0x549);
	tgb_wr32_epcs(pDev, TGB_PHY_VCO_CAL_LD2, 0x549);
	tgb_wr32_epcs(pDev, TGB_PHY_VCO_CAL_LD3, 0x549);

	/* Set VR XS, PMA, or MII Synopsys Enterprise Gen5 12G PHY VCO
	 * Calibration Reference 0 Register Bit[5:0] = 6'd42  VCO_REF_LD_0
	 */
	tgb_wr32_epcs(pDev, TGB_PHY_VCO_CAL_REF0, TGB_PHY_VCO_CAL_REF0_LD0_1GBASEX_KX);

	tgb_wr32_epcs(pDev, TGB_PHY_VCO_CAL_REF1, 0x2929);

	/* Set VR XS, PMA, or MII Synopsys Enterprise Gen5 12G PHY AFE-DFE Enable
	 * Register Bit[4], Bit[0] = 1'b0  AFE_EN_0, DFE_EN_0
	 */
	tgb_wr32_epcs(pDev, TGB_PHY_AFE_DFE_ENABLE, 0x0);
	/* Set	VR XS, PMA, or MII Synopsys Enterprise Gen5 12G PHY Rx
	 * Equalization Control 4 Register Bit[0] = 1'b0  CONT_ADAPT_0
	 */
	tgb_wr32_epcs(pDev, TGB_PHY_RX_EQ_CTL, 0x0010);
	/* Set VR XS, PMA, or MII Synopsys Enterprise Gen5 12G PHY Tx Rate
	 * Control Register Bit[2:0] = 3'b011  TX0_RATE
	 */
	tgb_wr32_epcs(pDev, TGB_PHY_TX_RATE_CTL, TGB_PHY_TX_RATE_CTL_TX0_RATE_1GBASEX_KX);

	/* Set VR XS, PMA, or MII Synopsys Enterprise Gen5 12G PHY Rx Rate
	 * Control Register Bit[2:0] = 3'b011 RX0_RATE
	 */
	tgb_wr32_epcs(pDev, TGB_PHY_RX_RATE_CTL, TGB_PHY_RX_RATE_CTL_RX0_RATE_1GBASEX_KX);

	/* Set VR XS, PMA, or MII Synopsys Enterprise Gen5 12G PHY Tx General
	 * Control 2 Register Bit[9:8] = 2'b01  TX0_WIDTH: 10bits
	 */
	tgb_wr32_epcs(pDev, TGB_PHY_TX_GEN_CTL2, TGB_PHY_TX_GEN_CTL2_TX0_WIDTH_OTHER);
	/* Set VR XS, PMA, or MII Synopsys Enterprise Gen5 12G PHY Rx General
	 * Control 2 Register Bit[9:8] = 2'b01  RX0_WIDTH: 10bits
	 */
	tgb_wr32_epcs(pDev, TGB_PHY_RX_GEN_CTL2, TGB_PHY_RX_GEN_CTL2_RX0_WIDTH_OTHER);
	/* Set VR XS, PMA, or MII Synopsys Enterprise Gen5 12G PHY MPLLA Control
	 * 2 Register Bit[10:8] = 3'b010	MPLLA_DIV16P5_CLK_EN=0,
	 * MPLLA_DIV10_CLK_EN=1, MPLLA_DIV8_CLK_EN=0
	 */
	tgb_wr32_epcs(pDev, TGB_PHY_MPLLA_CTL2, TGB_PHY_MPLLA_CTL2_DIV_CLK_EN_10);
	/* VR MII MMD AN Control Register Bit[8] = 1'b1 MII_CTRL */
	/* Set to 8bit MII (required in 10M/100M SGMII) */
	tgb_wr32_epcs(pDev, TGB_SR_MII_MMD_AN_CTL, 0x0100);

	/* 10. Initialize the mode by setting VR XS or PCS MMD Digital Control1
	 * Register Bit[15](VR_RST)
	 */
	tgb_wr32_epcs(pDev, TGB_VR_XS_OR_PCS_MMD_DIGI_CTL1, 0xA000);
	/* wait phy initialization done */
	for (i = 0; i < TGB_PHY_INIT_DONE_POLLING_TIME; i++) {
		if ((tgb_rd32_epcs(pDev, TGB_VR_XS_OR_PCS_MMD_DIGI_CTL1) &
				TGB_VR_XS_OR_PCS_MMD_DIGI_CTL1_VR_RST) == 0)
			break;
		vxbMsDelay(100);
	}

	/* if success, set link status */
	//	hw->link_status = TGB_LINK_STATUS_KX;

	if (i == TGB_PHY_INIT_DONE_POLLING_TIME) {
		status = ERROR;
		goto out;
	}

	out:
	return status;
}

void tgbmuxconnect()
{
}

#ifdef TGB_DEBUG

LOCAL void tgbEndPrint
(
		PCI_DEV * pDev
)
{
	u32 reg_ret_1;
	u32 *reg_1;

	//	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PSR_MAC_SWC_AD_L);
	//	DBG_LOG(" mac addr low is 0x%lx \n",reg_ret_1,1,2,3,4,5);
	//	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PSR_MAC_SWC_AD_H);
	//	DBG_LOG(" mac addr high is 0x%lx \n",reg_ret_1,1,2,3,4,5);

	DBG_LOG("\r\n ++++++++++++++ TGB_CFG_PORT_ST is 0x%lx +++++++++++++++\r\n",
			TXGBE_CSR_READ_4(pDev, TGB_CFG_PORT_ST),1,2,3,4,5);

	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PX_IMS(0));
	DBG_LOG(" TGB_PX_IMS(0) is 0x%lx \r\n",reg_ret_1,1,2,3,4,5);

	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PX_TR_CFG(0));
	DBG_LOG(" TGB_PX_TR_CFG(0) is 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PX_TR_BAH(0));
	DBG_LOG(" TGB_PX_TR_BAH(0) is 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PX_TR_BAL(0));
	DBG_LOG(" TGB_PX_TR_BAL(0) is 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PX_TR_WP(0));
	DBG_LOG(" TGB_PX_TR_WP is 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PX_TR_RP(0));
	DBG_LOG(" TGB_PX_TR_RP is 0x%lx \r\n",reg_ret_1,1,2,3,4,5);

	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PX_RR_RP(0));
	DBG_LOG(" TGB_PX_RR_RP(0) is 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PX_RR_WP(0));
	DBG_LOG(" TGB_PX_RR_WP(0) is 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PX_RR_BAH(0));
	DBG_LOG(" TGB_PX_RR_BAH(0) is 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PX_RR_BAL(0));
	DBG_LOG(" TGB_PX_RR_BAL(0) is 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PX_RR_CFG(0)); 
	DBG_LOG(" TGB_PX_RR_CFG(0) is 0x%lx \r\n",reg_ret_1,1,2,3,4,5);

	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PSR_CTL);
	DBG_LOG(" TGB_PSR_CTL is 0x%lx \r\n",reg_ret_1,1,2,3,4,5);

	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_MAC_RX_CFG);
	DBG_LOG(" TGB_MAC_RX_CFG is 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_RDB_PB_CTL);
	DBG_LOG(" TGB_RDB_PB_CTL is 0x%lx \r\n",reg_ret_1,1,2,3,4,5);

	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_RSEC_CTL);
	DBG_LOG(" TGB_RSC_CTL is 0x%lx \r\n",reg_ret_1,1,2,3,4,5);

	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_RDB_PB_CTL);
	DBG_LOG(" TGB_RDB_PB_CTL is 0x%lx \r\n",reg_ret_1,1,2,3,4,5);


#if 1
	//    DBG_LOG(" the status is 0x%lx \r\n",g_ret,1,2,3,4,5);

	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PX_MISC_IEN);
	DBG_LOG(" TGB_PX_MISC_IEN is 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PX_IMC(0));
	DBG_LOG(" TGB_PX_IMC(0) is 0x%lx \r\n",reg_ret_1,1,2,3,4,5);

	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PSR_CTL);
	DBG_LOG(" TGB_PSR_CTL TGB_PSR_CTL_BAM is 0x%lx \r\n",reg_ret_1,1,2,3,4,5);

	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PSR_CTL);
	DBG_LOG(" TGB_PSR_CTL TGB_PSR_CTL_UPE is 0x%lx \r\n",reg_ret_1,1,2,3,4,5);

	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_MAC_TX_CFG);
	DBG_LOG(" TGB_MAC_TX_CFG is 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_MAC_RX_CFG);

	DBG_LOG(" TGB_MAC_RX_CFG is 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_RDB_PB_CTL);
	DBG_LOG(" TGB_RDB_PB_CTL is 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_CFG_PORT_CTL);
	DBG_LOG(" TGB_CFG_PORT_CTL is 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PX_IVAR0);
	DBG_LOG(" TGB_PX_IVAR0is 0x%lx \r\n",reg_ret_1,1,2,3,4,5);

	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PX_MISC_IVAR);
	DBG_LOG(" TGB_PX_MISC_IVAR is 0x%lx \r\n",reg_ret_1,1,2,3,4,5);

	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PX_ITR0);
	DBG_LOG(" TGB_PX_ITR0 is 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
#endif

	/**rx path**/
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, 0x11900);
	DBG_LOG("  mac rx is 0x%lx\r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, 0x151B8);
	DBG_LOG("  PSR rx is 0x%lx\r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, 0x19060);
	DBG_LOG("  RDB rx is 0x%lx\r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, 0x12504);
	DBG_LOG("  rdma rx is 0x%lx\r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, 0x12500);
	DBG_LOG("  0X2500 is 0x%lx\r\n",reg_ret_1,1,2,3,4,5);

	/**tx path*/
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, 0x18308);
	DBG_LOG("  tdma tx is 0x%lx\r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, 0x1CF00);
	DBG_LOG("  tdB tx is 0x%lx\r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, 0x1181C);
	DBG_LOG("  MAC tx is 0x%lx\r\n",reg_ret_1,1,2,3,4,5);

	/* mac switcher */
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PSR_MAC_SWC_AD_L);
	DBG_LOG(" TGB_PSR_MAC_SWC_AD_L : \t 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PSR_MAC_SWC_AD_H);
	DBG_LOG(" TGB_PSR_MAC_SWC_AD_H : \t 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PSR_MAC_SWC_IDX);
	DBG_LOG(" TGB_PSR_MAC_SWC_IDX : \t 0x%lx \r\n",reg_ret_1,1,2,3,4,5);

	/* chip control Registers */
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_MIS_RST);
	DBG_LOG(" TGB_MIS_RST : \t 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_MIS_PWR);
	DBG_LOG(" TGB_MIS_PWR : \t 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_MIS_CTL);
	DBG_LOG(" TGB_MIS_CTL : \t 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_MIS_PF_SM);
	DBG_LOG(" TGB_MIS_PF_SM : \t 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_MIS_ST);
	DBG_LOG(" TGB_MIS_ST : \t 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_MIS_SWSM);
	DBG_LOG(" TGB_MIS_SWSM : \t 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_MIS_RST_ST);
	DBG_LOG(" TGB_MIS_RST_ST : \t 0x%lx \r\n",reg_ret_1,1,2,3,4,5);

	/* Interrupt Registers */
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PX_MISC_IC);
	DBG_LOG(" TGB_PX_MISC_IC : \t 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PX_MISC_ICS);
	DBG_LOG(" TGB_PX_MISC_ICS : \t 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PX_MISC_IEN);
	DBG_LOG(" TGB_PX_MISC_IEN : \t 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PX_MISC_IVAR);
	DBG_LOG(" TGB_PX_MISC_IVAR : \t 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PX_IMS(0));
	DBG_LOG(" TGB_PX_IMS(0) : \t 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PX_IMC(0));
	DBG_LOG(" TGB_PX_IMC(0) : \t 0x%lx \r\n",reg_ret_1,1,2,3,4,5);


#if 1
	TGB_ADV_RDESC *pprDesc;
	TGB_ADV_TDESC *pptDesc;
	pprDesc = pgDrvCtrl[pDev->unitNumber]->tgbRxDescMem;
	pptDesc = pgDrvCtrl[pDev->unitNumber]->tgbTxDescMem;

	for (int i = 0; i < 128; i++)
	{
		printk("pprDesc[%d].write:tgb_sts is 0x%0x \n", i, pprDesc->write.tgb_sts,2,3,4,5);
		pprDesc++;
	}

	for (int i = 0; i < 6; i++)
	{
		printk("pptDesc[%d]->addrlo:0x%0x, addrhi:0x%0x, cmd:0x%0x, sts:0x%0x\n", \
				i, pptDesc->tgb_addrlo, pptDesc->tgb_addrhi, pptDesc->tgb_cmd, pptDesc->tgb_sts, 5);
		pptDesc++;
	}
#endif

}

LOCAL void tgbPhyPrint
(
		PCI_DEV * pDev
)
{
	u32 reg_ret_1;

	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_XPCS_IDA_ADDR);
	DBG_LOG(" TGB_XPCS_IDA_ADDR : \t 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_XPCS_IDA_DATA);
	DBG_LOG(" TGB_XPCS_IDA_DATA : \t 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_ETHPHY_IDA_ADDR);
	DBG_LOG(" TGB_ETHPHY_IDA_ADDR : \t 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_ETHPHY_IDA_DATA);
	DBG_LOG(" TGB_ETHPHY_IDA_DATA : \t 0x%lx \r\n",reg_ret_1,1,2,3,4,5);
	reg_ret_1 = TXGBE_CSR_READ_4(pDev, TGB_PHY_MISC_CTL0);
	DBG_LOG(" TGB_PHY_MISC_CTL0 : \t 0x%lx \r\n",reg_ret_1,1,2,3,4,5);

}


void tgbprint(int u)
{
	if(u==0){
		DBG_LOG(" print reg of LAN0 \r\n",0,1,2,3,4,5);

		tgbEndPrint(pDev1);
	}
	if(u==1){
		DBG_LOG(" print reg of LAN1 \r\n",0,1,2,3,4,5);

		tgbEndPrint(pDev2);
	}

}

void tgbprint2(int u)
{
	if(u==0)
		tgbPhyPrint(pDev1);
	if(u==1)
		tgbPhyPrint(pDev2);
}
#endif



/*统计信息*/
void txgbe_info_dump(void)
{
	int i = 0;
	unsigned int txgbeRxFree = 0;
	unsigned int txgbeRxIdx = 0;
	TGB_DRV_CTRL *pDrvCtrl;
	TGB_ADV_RDESC *pRxDesc;
	
	printf("======================================\n");
	for(i = 0; i < 2; i++)
	{
		/* 中断计数统计 */
		printf("txgbe%d 中断总次数统计:        %llu\n",i,gTxgbeIntCount[i]);
		printf("txgbe%d 中断处理次数统计:      %llu\n",i,gTxgbeIntHandleCount[i]);
		printf("txgbe%d link中断:              %llu\n",i,gTxgbeLinkIntCount[i]);
		
		/*收发次数统计*/
		printf("txgbe%d 有效的接收次数:        %llu\n",i,gTxgbeRxHandleCount[i]);
		printf("txgbe%d 有效的发送次数:        %llu\n",i,gTxgbeSendCount[i]);
		printf("txgbe%d 实际发送处理次数:      %llu\n",i,gTxgbeTxHandleCount[i]);	
		
		/*描述符剩余统计*/

		if(pgDrvCtrl[i] != NULL)
			pDrvCtrl = pgDrvCtrl[i];
		else
			continue;
		
		/*发送描述符剩余统计*/
		printf("txgbe%d 发送描述符剩余:        %lu\n",i,pDrvCtrl->tgbTxFree);
		
		/*接收描述符剩余统计*/
		txgbeRxIdx = 0;
		txgbeRxFree = 0;
		pRxDesc = (TGB_ADV_RDESC *)&pDrvCtrl->tgbRxDescMem[txgbeRxIdx];
		while((txgbeRxIdx < TGB_RX_DESC_CNT) && (pRxDesc != NULL))
		{
			if (!(pRxDesc->write.tgb_sts & htole16(TGB_ADV_RDESC_STS_DD)))
				txgbeRxFree++;
			
			txgbeRxIdx++;
			pRxDesc = (TGB_ADV_RDESC *)&pDrvCtrl->tgbRxDescMem[txgbeRxIdx];
		}
		printf("txgbe%d 接收描述符剩余:        %lu\n\n",i,txgbeRxFree);
	}
	printf("======================================\n");
}

