#ifndef __GIC_MSI_H__
#define __GIC_MSI_H__

#ifdef __cplusplus
extern "C" {
#endif

//#include "../irq/bsp_int.h"

typedef struct msi_msg{
	unsigned int lowAddr;
	unsigned int highAddr;
	unsigned int baseData;
	int  baseIntVector;
}MSI_MSG_T;



#ifndef  __ENABLE_MSI__
#define __ENABLE_MSI__
#endif

/* 使用legency中断会导致nvme死机!? */
//#ifdef  __ENABLE_MSI__
//#undef __ENABLE_MSI__
//#endif



int msi_request(MSI_MSG_T *msiMsg, int num);

int msi_int_install( int intVector,
                     void (*handler) (void*),  /* ISR */
                     void *arg);

int pcie_msi_init(int bus,int device,int func, u32 vecaddr, u16 vecdata);


#ifdef __cplusplus
}
#endif

#endif
