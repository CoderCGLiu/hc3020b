/*
 * WangXun 10 Gigabit PCI Express End Driver
 * Copyright (c) 2015 - 2017 Beijing WangXun Technology Co., Ltd.
 */

#ifndef __TxgbeEndh
#define __TxgbeEndh

#include "drvCommon.h"
#include "if_txgbe.h"
#include <semLib.h>

#if 1 /*START*/
#include <reworks/types.h>
#include <reworksio.h>

#ifndef	UINT8
#define UINT8	u8
#endif
#ifndef	UINT16
#define UINT16	u16
#endif
#ifndef	UINT32
#define UINT32	u32
#endif
#ifndef	UINT64
#define UINT64	u64
#endif
#ifndef	U8
#define U8		u8
#endif
#ifndef	U16
#define U16		u16
#endif
#ifndef	U32
#define U32		u32
#endif
#ifndef	U64
#define U64		u64
#endif
#ifndef	VOID
#define VOID	void
#endif
#ifndef	PVOID
#define	PVOID	void*
#endif
#ifndef	INT
#define INT		int
#endif
#ifndef	BOOL
#define	BOOL	int
#endif
#ifndef	UINT
#define	UINT	unsigned int
#endif
#ifndef	ULONG
#define	ULONG	unsigned long
#endif
#ifndef	LONG
#define	LONG	long
#endif
#ifndef	UCHAR
#define	UCHAR	unsigned char
#endif
#ifndef PUCHAR
#define PUCHAR	unsigned char*
#endif
#ifndef	CHAR
#define	CHAR	char
#endif
#ifndef	CPCHAR
#define CPCHAR	char*
#endif
#ifndef	PCHAR
#define PCHAR	char*
#endif
#ifndef	PX_ERROR
#define	PX_ERROR	-1
#endif
#ifndef	ERROR_NONE
#define	ERROR_NONE	0
#endif
#ifndef	ERROR
#define	ERROR	-1
#endif
#ifndef	OK
#define	OK	0
#endif
#ifndef TRUE
#define TRUE		(1)
#endif
#ifndef FALSE
#define FALSE		(0)
#endif
#endif /*END*/


extern ptrdiff_t sys_virtual_to_phy(ptrdiff_t virtual_addr);
#define SYS_VIRT_TO_PHY	 sys_virtual_to_phy


#ifndef ETHER_ADDR_LEN
#define ETHER_ADDR_LEN (6)
#endif

#define TGB_ROUND_UP(x, align)	(((size_t) (x) + (align - 1)) & ~(align - 1))

#ifdef __cplusplus
extern "C" {
#endif

#if 1//#ifdef    _WRS_CONFIG_LP64	/*xxx ldf 20230828 modify:: ft1500下这里是定义TGB_64的*/
#define TGB_64            /* 64-bit physical address support */   
#endif    /* _WRS_CONFIG_LP64 */

/* TGB_VXB_DMA_BUF wait to test, temporary unavailable */

#if (_VX_CPU_FAMILY == _VX_I80X86)
#undef TGB_VXB_DMA_BUF
#else
#define TGB_VXB_DMA_BUF
#endif
//#endif
#undef TGB_VXB_DMA_BUF

extern void tgbRegister (void);

/* debug log output */
#define TGB_DEBUG
#ifdef TGB_DEBUG
#define DBG_LOG(fmt,a1,a2,a3,a4,a5,a6)          \
        printk(fmt,a1,a2,a3,a4,a5,a6)
#else
#define DBG_LOG(fmt,a1,a2,a3,a4,a5,a6)   {}
//#define DBG_LOG(fmt,a1,a2,a3,a4,a5,a6)   do {} while (0)
#endif

#if 1//#ifndef BSP_VERSION  /*ldf 20230825 modify*/
/************ tgb_register.h ************/
/* Vendor ID */
#ifndef PCI_VENDOR_ID_WX
#define PCI_VENDOR_ID_WX              0x8088
#endif

/* Device IDs */
#define TGB_DEV_ID_SP1000                     0x1001
#define TGB_DEV_ID_WX1820                     0x2001

/* Subsystem IDs */
/* SFP */
#define TGB_ID_SP1000_SFP                     0x1000
#define TGB_ID_WX1820_SFP                     0x2000
#define TGB_ID_SFP                            0x00

/* copper */
#define TGB_ID_SP1000_XAUI                    0x1010
#define TGB_ID_WX1820_XAUI                    0x2010
#define TGB_ID_XAUI                           0x10
#define TGB_ID_SP1000_SGMII                   0x1020
#define TGB_ID_WX1820_SGMII                   0x2020
#define TGB_ID_SGMII                          0x20
/* backplane */
#define TGB_ID_SP1000_KR_KX_KX4               0x1030
#define TGB_ID_WX1820_KR_KX_KX4               0x2030
#define TGB_ID_KR_KX_KX4                      0x30

/* Combined interface*/
#define TGB_ID_SFI_XAUI                       0x50

/* Revision ID */
#define TGB_SP_MPW                            1

/********* some driver information define *************/
#define TGB_MTU                               1500
#define TGB_JUMBO_MTU                         9000
#define TGB_CLSIZE                            1536
#define TGB_NAME                             "txgbe"
#define TGB_TIMEOUT                           10000

#define TGB_MAX_RX                            64
#define TGB_MAR_CNT                           128
#define TGB_MAXFRAG                           16

#define TGB_RX_DESC_CNT                       512
#define TGB_TX_DESC_CNT                       512

#define TGB_TX_CLEAN_THRESH                   32

#define TGB_DEVTYPE_SP                        0x1

#define TGB_PCI_MASTER_DISABLE_TIMEOUT        800

#define TGB_PX_INTA                           0x110

#define TGB_PTYPE_PKT_IP              (0x20)
#define TGB_PTYPE_TYP_UDP             (0x03)
enum tgb_tx_flags {
	/* cmd_type flags */
	TGB_TX_FLAGS_HW_VLAN  = 0x01,
	TGB_TX_FLAGS_TSO      = 0x02,
	TGB_TX_FLAGS_TSTAMP   = 0x04,

	/* olinfo flags */
	TGB_TX_FLAGS_CC       = 0x08,
	TGB_TX_FLAGS_IPV4     = 0x10,
	TGB_TX_FLAGS_CSUM     = 0x20,
	TGB_TX_FLAGS_OUTER_IPV4 = 0x100,
	TGB_TX_FLAGS_LINKSEC	= 0x200,
	TGB_TX_FLAGS_IPSEC    = 0x400,

	/* software defined flags */
	TGB_TX_FLAGS_SW_VLAN  = 0x40,
	TGB_TX_FLAGS_FCOE     = 0x80,
};


#define TGB_UDP_HDR_LEN 8

/****************************** GPIO Registers ******************************/
/* GPIO Registers */
#define TGB_GPIO_DR                   0x14800
#define TGB_GPIO_DDR                  0x14804
#define TGB_GPIO_CTL                  0x14808
#define TGB_GPIO_INTEN                0x14830
#define TGB_GPIO_INTMASK              0x14834
#define TGB_GPIO_INTTYPE_LEVEL        0x14838
#define TGB_GPIO_INTSTATUS            0x14844
#define TGB_GPIO_EOI                  0x1484C
/*GPIO bit */
#define TGB_GPIO_DR_0         0x00000001U /* SDP0 Data Value */
#define TGB_GPIO_DR_1         0x00000002U /* SDP1 Data Value */

/******************************* PSR Registers *******************************/
/* psr control */
#define TGB_PSR_CTL                   0x15000
#define TGB_PSR_MAX_SZ                0x15020
#define TGB_PSR_VLAN_CTL              0x15088

#define TGB_PSR_CTL_RSC_ACK           0x00020000U
#define TGB_PSR_CTL_RSC_DIS           0x00010000U
#define TGB_PSR_CTL_BAM               0x00000400U
#define TGB_PSR_CTL_UPE               0x00000200U
#define TGB_PSR_CTL_MPE               0x00000100U
#define TGB_PSR_CTL_MFE               0x00000080U
#define TGB_PSR_CTL_MO                0x00000060U

/* RX address excact match filter registers */
#define TGB_PARH_AV                   0x80000000 /* Address valid */

#define TGB_PSR_UC_TBL(_i)            (0x15400  + ((_i) * 4))

/* mcasst/ucast overflow tbl */
#define TGB_PSR_MC_TBL(_i)            (0x15200  + ((_i) * 4))

#define TGB_PSR_VM_L2CTL(_i)          (0x15600 + ((_i) * 4))

/* mac switcher */
#define TGB_PSR_MAC_SWC_AD_L          0x16200
#define TGB_PSR_MAC_SWC_AD_H          0x16204
#define TGB_PSR_MAC_SWC_IDX           0x16210


/* VMOLR bitmasks */
#define TGB_PSR_VM_L2CTL_UPE          0x00000010U /* unicast promiscuous */
#define TGB_PSR_VM_L2CTL_TPE          0x00000020U /* ETAG promiscuous */
#define TGB_PSR_VM_L2CTL_VACC         0x00000040U /* accept nomatched vlan */
#define TGB_PSR_VM_L2CTL_VPE          0x00000080U /* vlan promiscuous mode */
#define TGB_PSR_VM_L2CTL_AUPE         0x00000100U /* accept untagged packets */
#define TGB_PSR_VM_L2CTL_ROMPE        0x00000200U /*accept packets in MTA tbl*/
#define TGB_PSR_VM_L2CTL_ROPE         0x00000400U /* accept packets in UC tbl*/
#define TGB_PSR_VM_L2CTL_BAM          0x00000800U /* accept broadcast packets*/
#define TGB_PSR_VM_L2CTL_MPE          0x00001000U /* multicast promiscuous */


/********************************* RSEC **************************************/
/* general rsec */
#define TGB_RSEC_CTL                          0x17000
#define TGB_RSEC_CTL_CRC_STRIP                0x00000004U


/**************** Global Registers ****************************/
/* chip control Registers */
#define TGB_MIS_RST                           0x1000C
#define TGB_MIS_PWR                           0x10000
#define TGB_MIS_CTL                           0x10004
#define TGB_MIS_PF_SM                         0x10008
#define TGB_MIS_ST                            0x10028
#define TGB_MIS_SWSM                          0x1002C
#define TGB_MIS_RST_ST                        0x10030

#define TGB_MIS_RST_LAN0_RST                  0x00000002U
#define TGB_MIS_RST_LAN1_RST                  0x00000004U
#define TGB_MIS_RST_LAN0_CHG_ETH_MODE         0x20000000U
#define TGB_MIS_RST_LAN1_CHG_ETH_MODE         0x40000000U
#define TGB_MIS_RST_ST_DEV_RST_ST_DONE        0x00000000U
#define TGB_MIS_RST_ST_DEV_RST_ST_REQ         0x00080000U
#define TGB_MIS_RST_ST_DEV_RST_ST_INPROGRESS  0x00100000U


/********************************* TSEC **************************************/
/* general rsec */
#define TGB_TSEC_CTL                          0x1D000
#define TGB_TSEC_BUF_AE                       0x1D00C


/************************************** ETH PHY ******************************/
#define TGB_XPCS_IDA_ADDR                     0x13000
#define TGB_XPCS_IDA_DATA                     0x13004
#define TGB_ETHPHY_IDA_ADDR                   0x13008
#define TGB_ETHPHY_IDA_DATA                   0x1300C

/* ETH PHY Registers */
#define TGB_SR_XS_PCS_MMD_STATUS1             0x30001
#define TGB_SR_PCS_CTL2                       0x30007
#define TGB_SR_PMA_MMD_CTL1                   0x10000
#define TGB_SR_MII_MMD_CTL                    0x1F0000
#define TGB_SR_MII_MMD_DIGI_CTL               0x1F8000
#define TGB_SR_MII_MMD_AN_CTL                 0x1F8001
#define TGB_SR_MII_MMD_AN_ADV                 0x1F0004
#define TGB_SR_MII_MMD_AN_ADV_PAUSE(_v)       ((0x3 & (_v)) << 7)
#define TGB_SR_MII_MMD_AN_ADV_PAUSE_ASM       0x80
#define TGB_SR_MII_MMD_AN_ADV_PAUSE_SYM       0x100
#define TGB_SR_MII_MMD_LP_BABL                0x1F0005
#define TGB_SR_AN_MMD_CTL                     0x70000
#define TGB_SR_AN_MMD_ADV_REG1                0x70010
#define TGB_SR_AN_MMD_ADV_REG1_PAUSE(_v)      ((0x3 & (_v)) << 10)
#define TGB_SR_AN_MMD_ADV_REG1_PAUSE_SYM      0x400
#define TGB_SR_AN_MMD_ADV_REG1_PAUSE_ASM      0x800
#define TGB_SR_AN_MMD_ADV_REG2                0x70011
#define TGB_SR_AN_MMD_LP_ABL1                 0x70013
#define TGB_VR_AN_KR_MODE_CL                  0x78003
#define TGB_VR_XS_OR_PCS_MMD_DIGI_CTL1        0x38000
#define TGB_VR_XS_OR_PCS_MMD_DIGI_STATUS      0x38010
#define TGB_PHY_MPLLA_CTL0                    0x18071
#define TGB_PHY_MPLLA_CTL3                    0x18077
#define TGB_PHY_MISC_CTL0                     0x18090
#define TGB_PHY_VCO_CAL_LD0                   0x18092
#define TGB_PHY_VCO_CAL_LD1                   0x18093
#define TGB_PHY_VCO_CAL_LD2                   0x18094
#define TGB_PHY_VCO_CAL_LD3                   0x18095
#define TGB_PHY_VCO_CAL_REF0                  0x18096
#define TGB_PHY_VCO_CAL_REF1                  0x18097
#define TGB_PHY_RX_AD_ACK                     0x18098
#define TGB_PHY_AFE_DFE_ENABLE                0x1805D
#define TGB_PHY_DFE_TAP_CTL0                  0x1805E
#define TGB_PHY_RX_EQ_ATT_LVL0                0x18057
#define TGB_PHY_RX_EQ_CTL0                    0x18058
#define TGB_PHY_RX_EQ_CTL                     0x1805C
#define TGB_PHY_TX_EQ_CTL0                    0x18036
#define TGB_PHY_TX_EQ_CTL1                    0x18037
#define TGB_PHY_TX_RATE_CTL                   0x18034
#define TGB_PHY_RX_RATE_CTL                   0x18054
#define TGB_PHY_TX_GEN_CTL2                   0x18032
#define TGB_PHY_RX_GEN_CTL2                   0x18052
#define TGB_PHY_RX_GEN_CTL3                   0x18053
#define TGB_PHY_MPLLA_CTL2                    0x18073
#define TGB_PHY_RX_POWER_ST_CTL               0x18055
#define TGB_PHY_TX_POWER_ST_CTL               0x18035
#define TGB_PHY_TX_GENCTRL1                   0x18031

#define TGB_SR_PCS_CTL2_PCS_TYPE_SEL_R        0x0
#define TGB_SR_PCS_CTL2_PCS_TYPE_SEL_X        0x1
#define TGB_SR_PCS_CTL2_PCS_TYPE_SEL_MASK     0x3
#define TGB_SR_PMA_MMD_CTL1_SPEED_SEL_1G      0x0
#define TGB_SR_PMA_MMD_CTL1_SPEED_SEL_10G     0x2000
#define TGB_SR_PMA_MMD_CTL1_SPEED_SEL_MASK    0x2000
#define TGB_SR_PMA_MMD_CTL1_LB_EN             0x1
#define TGB_SR_MII_MMD_CTL_AN_EN              0x1000
#define TGB_SR_MII_MMD_CTL_RESTART_AN         0x0200
#define TGB_SR_AN_MMD_CTL_RESTART_AN          0x0200
#define TGB_SR_AN_MMD_CTL_ENABLE              0x1000
#define TGB_SR_AN_MMD_ADV_REG2_BP_TYPE_KX4    0x40
#define TGB_SR_AN_MMD_ADV_REG2_BP_TYPE_KX     0x20
#define TGB_SR_AN_MMD_ADV_REG2_BP_TYPE_KR     0x80
#define TGB_SR_AN_MMD_ADV_REG2_BP_TYPE_MASK   0xFFFF
#define TGB_VR_XS_OR_PCS_MMD_DIGI_CTL1_ENABLE 0x1000
#define TGB_VR_XS_OR_PCS_MMD_DIGI_CTL1_VR_RST 0x8000
#define TGB_VR_XS_OR_PCS_MMD_DIGI_STATUS_PSEQ_MASK            0x1C
#define TGB_VR_XS_OR_PCS_MMD_DIGI_STATUS_PSEQ_POWER_GOOD      0x10

#define TGB_PHY_MPLLA_CTL0_MULTIPLIER_1GBASEX_KX              32
#define TGB_PHY_MPLLA_CTL0_MULTIPLIER_10GBASER_KR             33
#define TGB_PHY_MPLLA_CTL0_MULTIPLIER_OTHER                   40
#define TGB_PHY_MPLLA_CTL0_MULTIPLIER_MASK                    0xFF
#define TGB_PHY_MPLLA_CTL3_MULTIPLIER_BW_1GBASEX_KX           0x46
#define TGB_PHY_MPLLA_CTL3_MULTIPLIER_BW_10GBASER_KR          0x7B
#define TGB_PHY_MPLLA_CTL3_MULTIPLIER_BW_OTHER                0x56
#define TGB_PHY_MPLLA_CTL3_MULTIPLIER_BW_MASK                 0x7FF
#define TGB_PHY_MISC_CTL0_TX2RX_LB_EN_0                       0x1
#define TGB_PHY_MISC_CTL0_TX2RX_LB_EN_3_1                     0xE
#define TGB_PHY_MISC_CTL0_RX_VREF_CTRL                        0x1F00
#define TGB_PHY_VCO_CAL_LD0_1GBASEX_KX                        1344
#define TGB_PHY_VCO_CAL_LD0_10GBASER_KR                       1353
#define TGB_PHY_VCO_CAL_LD0_OTHER                             1360
#define TGB_PHY_VCO_CAL_LD0_MASK                              0x1000
#define TGB_PHY_VCO_CAL_REF0_LD0_1GBASEX_KX                   42
#define TGB_PHY_VCO_CAL_REF0_LD0_10GBASER_KR                  41
#define TGB_PHY_VCO_CAL_REF0_LD0_OTHER                        34
#define TGB_PHY_VCO_CAL_REF0_LD0_MASK                         0x3F
#define TGB_PHY_AFE_DFE_ENABLE_DFE_EN0                        0x10
#define TGB_PHY_AFE_DFE_ENABLE_AFE_EN0                        0x1
#define TGB_PHY_AFE_DFE_ENABLE_MASK                           0xFF
#define TGB_PHY_RX_EQ_CTL_CONT_ADAPT0                         0x1
#define TGB_PHY_RX_EQ_CTL_CONT_ADAPT_MASK                     0xF
#define TGB_PHY_TX_RATE_CTL_TX0_RATE_10GBASER_KR              0x0
#define TGB_PHY_TX_RATE_CTL_TX0_RATE_RXAUI                    0x1
#define TGB_PHY_TX_RATE_CTL_TX0_RATE_1GBASEX_KX               0x3
#define TGB_PHY_TX_RATE_CTL_TX0_RATE_OTHER                    0x2
#define TGB_PHY_TX_RATE_CTL_TX1_RATE_OTHER                    0x20
#define TGB_PHY_TX_RATE_CTL_TX2_RATE_OTHER                    0x200
#define TGB_PHY_TX_RATE_CTL_TX3_RATE_OTHER                    0x2000
#define TGB_PHY_TX_RATE_CTL_TX0_RATE_MASK                     0x7
#define TGB_PHY_TX_RATE_CTL_TX1_RATE_MASK                     0x70
#define TGB_PHY_TX_RATE_CTL_TX2_RATE_MASK                     0x700
#define TGB_PHY_TX_RATE_CTL_TX3_RATE_MASK                     0x7000
#define TGB_PHY_RX_RATE_CTL_RX0_RATE_10GBASER_KR              0x0
#define TGB_PHY_RX_RATE_CTL_RX0_RATE_RXAUI                    0x1
#define TGB_PHY_RX_RATE_CTL_RX0_RATE_1GBASEX_KX               0x3
#define TGB_PHY_RX_RATE_CTL_RX0_RATE_OTHER                    0x2
#define TGB_PHY_RX_RATE_CTL_RX1_RATE_OTHER                    0x20
#define TGB_PHY_RX_RATE_CTL_RX2_RATE_OTHER                    0x200
#define TGB_PHY_RX_RATE_CTL_RX3_RATE_OTHER                    0x2000
#define TGB_PHY_RX_RATE_CTL_RX0_RATE_MASK                     0x7
#define TGB_PHY_RX_RATE_CTL_RX1_RATE_MASK                     0x70
#define TGB_PHY_RX_RATE_CTL_RX2_RATE_MASK                     0x700
#define TGB_PHY_RX_RATE_CTL_RX3_RATE_MASK                     0x7000
#define TGB_PHY_TX_GEN_CTL2_TX0_WIDTH_10GBASER_KR             0x200
#define TGB_PHY_TX_GEN_CTL2_TX0_WIDTH_10GBASER_KR_RXAUI       0x300
#define TGB_PHY_TX_GEN_CTL2_TX0_WIDTH_OTHER                   0x100
#define TGB_PHY_TX_GEN_CTL2_TX0_WIDTH_MASK                    0x300
#define TGB_PHY_TX_GEN_CTL2_TX1_WIDTH_OTHER                   0x400
#define TGB_PHY_TX_GEN_CTL2_TX1_WIDTH_MASK                    0xC00
#define TGB_PHY_TX_GEN_CTL2_TX2_WIDTH_OTHER                   0x1000
#define TGB_PHY_TX_GEN_CTL2_TX2_WIDTH_MASK                    0x3000
#define TGB_PHY_TX_GEN_CTL2_TX3_WIDTH_OTHER                   0x4000
#define TGB_PHY_TX_GEN_CTL2_TX3_WIDTH_MASK                    0xC000
#define TGB_PHY_RX_GEN_CTL2_RX0_WIDTH_10GBASER_KR             0x200
#define TGB_PHY_RX_GEN_CTL2_RX0_WIDTH_10GBASER_KR_RXAUI       0x300
#define TGB_PHY_RX_GEN_CTL2_RX0_WIDTH_OTHER                   0x100
#define TGB_PHY_RX_GEN_CTL2_RX0_WIDTH_MASK                    0x300
#define TGB_PHY_RX_GEN_CTL2_RX1_WIDTH_OTHER                   0x400
#define TGB_PHY_RX_GEN_CTL2_RX1_WIDTH_MASK                    0xC00
#define TGB_PHY_RX_GEN_CTL2_RX2_WIDTH_OTHER                   0x1000
#define TGB_PHY_RX_GEN_CTL2_RX2_WIDTH_MASK                    0x3000
#define TGB_PHY_RX_GEN_CTL2_RX3_WIDTH_OTHER                   0x4000
#define TGB_PHY_RX_GEN_CTL2_RX3_WIDTH_MASK                    0xC000

#define TGB_PHY_MPLLA_CTL2_DIV_CLK_EN_8                       0x100
#define TGB_PHY_MPLLA_CTL2_DIV_CLK_EN_10                      0x200
#define TGB_PHY_MPLLA_CTL2_DIV_CLK_EN_16P5                    0x400
#define TGB_PHY_MPLLA_CTL2_DIV_CLK_EN_MASK                    0x700

#define TGB_XPCS_POWER_GOOD_MAX_POLLING_TIME                  100
#define TGB_PHY_INIT_DONE_POLLING_TIME                        100

/***************************** RDB registers *********************************/
/* receive packet buffer */
#define TGB_RDB_PB_SZ(_i)         (0x19020 + ((_i) * 4))
#define TGB_RDB_PB_CTL             0x19000

/* Receive Config masks */
#define TGB_RDB_PB_CTL_RXEN           (0x80000000) /* Enable Receiver */
#define TGB_RDB_PB_CTL_DISABLED        0x1

#define TGB_DEFAULT_FCPAUSE 0x0100

#define TGB_RDB_RFCV(_i)          (0x19200 + ((_i) * 4)) /* 4 of these (0-3)*/
#define TGB_RDB_RFCL(_i)          (0x19220 + ((_i) * 4)) /* 8 of these (0-7)*/
#define TGB_RDB_RFCH(_i)          (0x19260 + ((_i) * 4)) /* 8 of these (0-7)*/
#define TGB_RDB_RFCRT             0x192A0
#define TGB_RDB_RFCC              0x192A4
#define TGB_RDB_PFCMACDAL         0x19210
#define TGB_RDB_PFCMACDAH         0x19214


#define TGB_RDB_RFCC_RFCE_802_3X      0x00000008U /* Tx link FC enable */


/* RX flow control threshold low */


#define TGB_FCRTL_RTL	0x0007FFE0 /* RX threshold low (82599) */
#define TGB_FCRTL_XONE		0x80000000 /* XON enable */

#define TGB_RTL(x)		(((x) << 4) & TGB_FCRTL_RTL)

/* RX flow control threshold high */


#define TGB_FCRTL_RTH	0x0007FFE0 /* RX threshold low (82599) */
#define TGB_FCRTL_FCEN		0x80000000 /* XON enable */

#define TGB_RTH(x)		(((x) << 4) & TGB_FCRTL_RTH)

/* Flow control receive threshold low watermark */

#define TGB_RX_FLOW_THRESH_LOW	0x1000

/* Flow control receive threshold high watermark */

#define TGB_RX_FLOW_THRESH_HIGH	0x2000

#define TGB_SR_MII_MMD_AN_ADV                 0x1F0004
#define TGB_SR_MII_MMD_AN_ADV_PAUSE(_v)       ((0x3 & (_v)) << 7)
#define TGB_SR_MII_MMD_AN_ADV_PAUSE_ASM       0x80
#define TGB_SR_MII_MMD_AN_ADV_PAUSE_SYM       0x100


/***************************** TDB registers *********************************/
/* transmit packet buffer */
#define TGB_TDB_PB_SZ(_i)         (0x1CC00 + ((_i) * 4)) /* 8 of these */

#define TGB_TDB_PB_SZ_20KB         0x00005000U /* 20KB Packet Buffer */
#define TGB_TDB_PB_SZ_40KB         0x0000A000U /* 40KB Packet Buffer */
#define TGB_TDB_PB_SZ_MAX          0x00028000U /* 160KB Packet Buffer */
#define TGB_TXPKT_SIZE_MAX         0xA /* Max Tx Packet size */


/************************************* ETH MAC *****************************/
#define TGB_MAC_TX_CFG                0x11000
#define TGB_MAC_RX_CFG                0x11004
#define TGB_MAC_PKT_FLT               0x11008
#define TGB_MAC_WDG_TIMEOUT           0x1100C
#define TGB_MAC_PKT_FLT_PR            (0x1) /* promiscuous mode */
#define TGB_MAC_PKT_FLT_RA            (0x80000000) /* receive all */
#define TGB_MAC_RX_FLOW_CTRL          0x11090
#define TGB_MAC_TX_FLOW_CTRL          0x11070

#define TGB_MAC_TX_CFG_TE             0x00000001U
#define TGB_MAC_TX_CFG_SPEED_MASK     0x60000000U
#define TGB_MAC_TX_CFG_SPEED_10G      0x00000000U
#define TGB_MAC_TX_CFG_SPEED_1G       0x60000000U
#define TGB_MAC_RX_CFG_RE             0x00000001U
#define TGB_MAC_RX_CFG_JE             0x00000100U
#define TGB_MAC_RX_CFG_LM             0x00000400U
#define TGB_MAC_RX_FLOW_CTRL_RFE      0x00000001U /* receive fc enable */

/* statistic */
#define TGB_RX_BC_FRAMES_GOOD_LOW     0x11918
#define TGB_TX_MC_FRAMES_GOOD_LOW     0x1182C
#define TGB_TX_BC_FRAMES_GOOD_LOW     0x11824

#define TGB_SPI_STATUS                0x1010C
#define TGB_SPI_STATUS_OPDONE         ((0x1))
#define TGB_SPI_STATUS_FLASH_BYPASS   ((0x1) << 31)
#define TGB_MAX_FLASH_LOAD_POLL_TIME  10

#define TGB_SPI_ILDR_STATUS           0x10120
#define TGB_SPI_ILDR_STATUS_PERST     0x00000001U /* PCIE_PERST is done */
#define TGB_SPI_ILDR_STATUS_PWRRST    0x00000002U /* Power on reset is done */
#define TGB_SPI_ILDR_STATUS_SW_RESET  0x00000080U /* software reset is done */
#define TGB_SPI_ILDR_STATUS_LAN0_SW_RST 0x00000200U /* lan0 soft reset done */
#define TGB_SPI_ILDR_STATUS_LAN1_SW_RST 0x00000400U /* lan1 soft reset done */


/********************************* BAR registers ***************************/
/* Interrupt Registers */
#define TGB_PX_MISC_IC                        0x100
#define TGB_PX_MISC_ICS                       0x104
#define TGB_PX_MISC_IEN                       0x108
#define TGB_PX_MISC_IVAR                      0x4FC
#define TGB_PX_MISC_IVAR_DEFAULT              0x1
#define TGB_PX_MISC_IVAR_VALID                0x00000080UL

#define TGB_PX_ISB_ADDR_L                     0x160
#define TGB_PX_ISB_ADDR_H                     0x164
#define TGB_PX_IC(_i)                         (0x120 + (_i) * 4)
#define TGB_PX_ICS(_i)                        (0x130 + (_i) * 4)
#define TGB_PX_IMS(_i)                        (0x140 + (_i) * 4)
#define TGB_PX_IMC(_i)                        (0x150 + (_i) * 4)
#define TGB_PX_IVAR(_i)                       (0x500 + (_i) * 4)
#define TGB_PX_ITR(_i)                        (0x200 + (_i) * 4)
#define TGB_PX_TRANSACTION_PENDING            0x168
#define TGB_PX_INTA                           0x110
#define TGB_PX_GPIE                           0x118

#define TGB_PX_GPIE_MODEL                     0x00000001U

/*  Interrupt vector allocation registers */
#define TGB_PX_IVAR0                          0x500UL
#define TGB_PX_IVAR_RX0_DEFAULT               0x00
#define TGB_PX_IVAR_RX0_VALID           0x00000080UL  /**< RX queue 0 valid */
#define TGB_PX_IVAR_TX0_DEFAULT               0x00
#define TGB_PX_IVAR_TX0_VALID           0x00008000UL  /**< TX queue 0 valid */

#define TGB_PX_ITR0                           0x200UL
#define TGB_DEFAULT_ITR                       200
#define TGB_PX_ITR_CNT_WDIS                   0x80000000U


/******************** Interrupt register bitmasks *****************/
/* Extended Interrupt Cause Read */
#define TGB_PX_MISC_IC_ETH_LKDN       0x00000100U /* eth link down */
#define TGB_PX_MISC_IC_ETH_LK         0x00040000U /* link up */
#define TGB_PX_MISC_IC_ETH_AN         0x00080000U /* link auto-nego done */

/* Extended Interrupt Cause Set */
#define TGB_PX_MISC_ICS_ETH_LKDN      0x00000100U
#define TGB_PX_MISC_ICS_ETH_LK        0x00040000U
#define TGB_PX_MISC_ICS_ETH_AN        0x00080000U

/* Extended Interrupt Enable Set */
#define TGB_PX_MISC_IEN_ETH_LKDN      0x00000100U
#define TGB_PX_MISC_IEN_ETH_LK        0x00040000U
#define TGB_PX_MISC_IEN_ETH_AN        0x00080000U


/**************************** Transmit DMA registers **************************/
/* transmit global control */
#define TGB_TDM_CTL           0x18000
#define TGB_TDM_CTL_TE        0x1 /* Transmit Enable */

/* transmit DMA Registers */
#define TGB_PX_TR_BAL(_i)     (0x03000 + ((_i) * 0x40))
#define TGB_PX_TR_BAH(_i)     (0x03004 + ((_i) * 0x40))
#define TGB_PX_TR_WP(_i)      (0x03008 + ((_i) * 0x40))
#define TGB_PX_TR_RP(_i)      (0x0300C + ((_i) * 0x40))

#define TGB_PX_TR_CFG(_i)             (0x03010 + ((_i) * 0x40))
#define TGB_PX_TR_CFG_ENABLE          (1) /* Ena specific Tx Queue */
#define TGB_PX_TR_CFG_TR_SIZE_SHIFT   1 /* tx desc number per ring */
#define TGB_PX_TR_CFG_SWFLSH          (1 << 26) /* Tx Desc. wr-bk flushing */
#define TGB_PX_TR_CFG_WTHRESH_SHIFT   16 /* shift to WTHRESH bits */
#define TGB_PX_TR_CFG_THRE_SHIFT      8
#define TGB_PX_TR_CFG_TR_SZ           0x0000007EU

#define TGB_TDM_PB_THRE(_i)          (0x18020 + ((_i) * 4))

/* statistic */
#define TGB_TDM_SEC_DRP       0x18304
#define TGB_TDM_PKT_CNT       0x18308
#define TGB_TDM_OS2BMC_CNT    0x18314


/**************************** Receive DMA registers **************************/

#define TGB_PX_RR_CFG(_i)             (0x01010 + ((_i) * 0x40))

/* statistic */
#define TGB_RDM_DRP_PKT           0x12500
#define TGB_RDM_BMC2OS_CNT        0x12510

/* Receive DMA Registers */
#define TGB_PX_RR_BAL(_i)           (0x01000 + ((_i) * 0x40))
#define TGB_PX_RR_BAH(_i)           (0x01004 + ((_i) * 0x40))
#define TGB_PX_RR_RP(_i)            (0x0100C + ((_i) * 0x40))
#define TGB_PX_RR_WP(_i)            (0x01008 + ((_i) * 0x40))

/* PX_RR_CFG bit definitions */
#define TGB_PX_RR_CFG_RR_SIZE_SHIFT           1
#define TGB_PX_RR_CFG_BSIZEPKT_SHIFT          2 /* so many KBs */
#define TGB_PX_RR_CFG_BSIZEHDRSIZE_SHIFT      6 /* 64byte resolution (>> 6)*/
#define TGB_PX_RR_CFG_VLAN            0x80000000U
#define TGB_PX_RR_CFG_DROP            0x40000000U
#define TGB_PX_RR_CFG_SPLIT_MODE      0x04000000U
#define TGB_PX_RR_CFG_RR_THER         0x00070000U
#define TGB_PX_RR_CFG_RR_THER_SHIFT   16
#define TGB_PX_RR_CFG_RR_HDR_SZ       0x0000F000U
#define TGB_PX_RR_CFG_RR_BUF_SZ       0x00000F00U
#define TGB_PX_RR_CFG_RR_SZ           0x0000007EU
#define TGB_PX_RR_CFG_RR_EN           0x00000001U

#define TGB_PX_RR_CFG_HDR_SIZE        256
#define TGB_PX_RR_CFG_BSIZE_DEFAULT   2048
#define TGB_PX_RR_CFG_BSIZE_10K       10240

/* statistic */
#define TGB_PX_MPRC(_i)               (0x1020 + ((_i) * 64))
#define TGB_PX_GPRC                   0x12504
#define TGB_PX_GPTC                   0x18308
#define TGB_PX_GORC_LSB               0x12508
#define TGB_PX_GORC_MSB               0x1250C
#define TGB_PX_GOTC_LSB               0x1830C
#define TGB_PX_GOTC_MSB               0x18310


/************************* Port Registers ************************************/

/* port cfg Registers */
#define TGB_CFG_PORT_CTL                      0x14400
#define TGB_CFG_PORT_ST                       0x14404

/* port cfg bit */
#define TGB_CFG_PORT_CTL_DRV_LOAD             0x00000008U
/* Status Bit */
#define TGB_CFG_PORT_ST_LINK_UP               0x00000001U
#define TGB_CFG_PORT_ST_READ_LAN_ID           0x00000100U
#define TGB_CFG_PORT_ST_LINK_10G              0x00000002U
#define TGB_CFG_PORT_ST_LINK_1G               0x00000004U
#define TGB_CFG_PORT_ST_LINK_100M             0x00000008U
#define TGB_CFG_PORT_ST_LAN_ID(_r)          ((0x00000100U & (_r)) >> 8)
#define TGB_LINK_UP_TIME                      90

/* Physical layer type */
#define TGB_PHYSICAL_LAYER_UNKNOWN            0
#define TGB_PHYSICAL_LAYER_10GBASE_T          0x0001
#define TGB_PHYSICAL_LAYER_1000BASE_T         0x0002
#define TGB_PHYSICAL_LAYER_100BASE_TX         0x0004
#define TGB_PHYSICAL_LAYER_SFP_PLUS_CU        0x0008
#define TGB_PHYSICAL_LAYER_10GBASE_LR         0x0010
#define TGB_PHYSICAL_LAYER_10GBASE_LRM        0x0020
#define TGB_PHYSICAL_LAYER_10GBASE_SR         0x0040
#define TGB_PHYSICAL_LAYER_10GBASE_KX4        0x0080
#define TGB_PHYSICAL_LAYER_1000BASE_KX        0x0200
#define TGB_PHYSICAL_LAYER_1000BASE_BX        0x0400
#define TGB_PHYSICAL_LAYER_10GBASE_KR         0x0800
#define TGB_PHYSICAL_LAYER_10GBASE_XAUI       0x1000
#define TGB_PHYSICAL_LAYER_SFP_ACTIVE_DA      0x2000
#define TGB_PHYSICAL_LAYER_1000BASE_SX        0x4000


/* Advanced RX descriptor format */

typedef union tgb_adv_rdesc
{
    struct
	{
        volatile UINT32        	tgb_addrlo; /* NSE */
        volatile UINT32        	tgb_addrhi;
        volatile UINT32        	tgb_hdrlo; /* DD */
        volatile UINT32        	tgb_hdrhi;
	} read;
    struct
	{
        volatile UINT16        	tgb_ptype_rsstype;
        volatile UINT16       	tgb_sph_hdrlen;
        volatile UINT32     	tgb_rss; /* RSS Hash */
        volatile UINT16        	tgb_sts;
        volatile UINT16        	tgb_err;
        volatile UINT16        	tgb_pkt_buflen;
        volatile UINT16        	tgb_vlan;
	} write;
    struct
	{
        volatile UINT32    		tgb_pktinfo;
        volatile UINT32 		tgb_rss;
        volatile UINT32    		tgb_errsts;
        volatile UINT16    		tgb_len;
        volatile UINT16 		tgb_vlan;
	} write_rss;
} TGB_ADV_RDESC;

#define TGB_ADV_RDESC_READ_DD          0x00000001
#define TGB_ADV_RDESC_RTYPE_NONE       0x00000000    /* No hash */
#define TGB_ADV_RDESC_RTYPE_TCPIPV4    0x00000001
#define TGB_ADV_RDESC_RTYPE_IPV4       0x00000002
#define TGB_ADV_RDESC_RTYPE_TCPIPV6    0x00000003
#define TGB_ADV_RDESC_RTYPE_SCTP4      0x00000004  /* RSVD */
#define TGB_ADV_RDESC_RTYPE_IPV6       0x00000005
#define TGB_ADV_RDESC_RTYPE_SCTP6      0x00000006  /* RSVD */
#define TGB_ADV_RDESC_RTYPE_UDPIPV4    0x00000007
#define TGB_ADV_RDESC_RTYPE_UDPIPV6    0x00000008
#define TGB_ADV_RDESC_RTYPE_FDFS       0x0000000f
#define TGB_ADV_RDESC_PTYPE_RSVD       0x10000
#define TGB_ADV_RDESC_PTYPE_L2PKT      0x8000 /* When set, interpretation of
                                               * other PTYPE bits changes
                                               */

#define TGB_RXD_ERR_RXE               0x2000U /* Any MAC Error */
#define TGB_ADV_RDESC_ERR_TCPE        0x4000U /* TCP/UDP Checksum Error */

#define TGB_ADV_RDESC_STS_VP          0x0020U /* IEEE VLAN Pkt */
#define TGB_ADV_RDESC_STS_UDPCV       0x0040U /* UDP csum calculated */
#define TGB_ADV_RDESC_STS_L4CS        0x0080U /* L4 csum calculated */
#define TGB_ADV_RDESC_STS_IPCS        0x0100U /* IP xsum calculated */
#define TGB_ADV_RDESC_STS_PIF         0x0200U /* passed in-exact filter */
#define TGB_ADV_RDESC_STS_EIPCS       0x0400U /* Cloud IP xsum calculated*/
#define TGB_ADV_RDESC_STS_VEXT        0x0800U /* 1st VLAN found */
#define TGB_ADV_RDESC_STS_LLINT       0x2000U /* Pkt caused Low Latency */
#define TGB_ADV_RDESC_STS_DD          0x0001 /* Descriptor done */
#define TGB_ADV_RDESC_STS_EOP         0x0002 /* End of packet */

/* Extended Error and Extended Status considered as a single 32-bit field */
#define TGB_ADV_RDESC_ERRSTS_IPE     0x80000000 /* IPv4 checksum error */
#define TGB_ADV_RDESC_ERRSTS_TCPE    0x40000000 /* TCP/UDP checksum error */
#define TGB_ADV_RDESC_ERRSTS_USE     0x20000000 /* Undersize (runt) frame */
#define TGB_ADV_RDESC_ERRSTS_OSE     0x10000000 /* Oversize (giant) frame */
#define TGB_ADV_RDESC_ERRSTS_PE      0x08000000 /* Packet symbol error */
#define TGB_ADV_RDESC_ERRSTS_LE      0x02000000 /* Length error */
#define TGB_ADV_RDESC_ERRSTS_CE      0x01000000 /* CRC error */
#define TGB_ADV_RDESC_ERRSTS_HBO     0x00800000 /* Header buffer overflow */
#define TGB_ADV_RDESC_ERRSTS_LB      0x00040000 /* Loopback - VM to VM */
#define TGB_ADV_RDESC_ERRSTS_SECP    0x00020000 /* Security encap. processed */
#define TGB_ADV_RDESC_ERRSTS_TS      0x00010000 /* Time sync packet */
#define TGB_ADV_RDESC_ERRSTS_DYNINT  0x00000800 /* Did low-latency interrupt */
#define TGB_ADV_RDESC_ERRSTS_UDPV    0x00000400 /* UDP csum valid */
#define TGB_ADV_RDESC_ERRSTS_VEXT    0x00000200 /* Outer VLAN found */
#define TGB_ADV_RDESC_ERRSTS_CRCV    0x00000100 /* Speculative CRC OK */
#define TGB_ADV_RDESC_ERRSTS_PIF     0x00000080 /* Passed inexact filter */
#define TGB_ADV_RDESC_ERRSTS_IPCS    0x00000040 /* IP csum calculated */
#define TGB_ADV_RDESC_ERRSTS_L4CS    0x00000020 /* TCP csum calculated */
#define TGB_ADV_RDESC_ERRSTS_UDPCS   0x00000010 /* UDP checksum calculated */
#define TGB_ADV_RDESC_ERRSTS_VLAN    0x00000008 /* 802.1q packet, stripped */
#define TGB_ADV_RDESC_ERRSTS_FLM     0x00000004 /* Flow director match */
#define TGB_ADV_RDESC_ERRSTS_EOP     0x00000002 /* End of packet */
#define TGB_ADV_RDESC_ERRSTS_DD      0x00000001 /* Descriptor done */


/* Advanced TCP/IP context descriptor format */
typedef struct tgb_adv_cdesc
    {
    volatile UINT16     tgb_macip;
    volatile UINT16     tgb_vlan;
    volatile UINT32     tgb_rsvd;
    volatile UINT32     tgb_cmd;
    volatile UINT8      tgb_idx;
    volatile UINT8      tgb_l4len;
    volatile UINT16     tgb_mss;
    } TGB_ADV_CDESC;

#define TGB_ADV_CDESC_MACIP_IP  0x01FF          /* IP header length */
#define TGB_ADV_CDESC_MACIP_MAC 0xFE00          /* Frame header length */
#define TGB_ADV_IPLEN(x)        ((x) & TGB_ADV_CDESC_MACIP_IP)
#define TGB_ADV_MACLEN(x)       (((x) << 9) & TGB_ADV_CDESC_MACIP_MAC)


typedef struct tgb_adv_tdesc
    {
    volatile UINT32     tgb_addrlo;
    volatile UINT32     tgb_addrhi;
    volatile UINT32     tgb_cmd;
    volatile UINT32     tgb_sts;
    } TGB_ADV_TDESC;

#define TGB_ADV_STS_DD                     0x00000001
#define TGB_DESC_STATUS_PAYLEN( len )      ( (len) << 13 )

#define TGB_ADV_TDESC_DTYP_CTX             0x00100000U /* Adv Context Desc */
#define TGB_ADV_TDESC_DTYP_DSC             0x00000000U /* Adv Data Descriptor */
#define TGB_ADV_TDESC_CMD_EOP              0x01000000U /* End of Packet */
#define TGB_ADV_TDESC_CMD_IFCS             0x02000000U /* Insert FCS */
#define TGB_ADV_TDESC_CMD_LINKSEC          0x04000000U /* enable linksec */
#define TGB_ADV_TDESC_CMD_RS               0x08000000U /* Report Status */
#define TGB_ADV_TDESC_CMD_VLE              0x40000000U /* VLAN tag
                                                        * insertion Enable
                                                        */
#define TGB_ADV_TDESC_L4CS                 0x00000200U


#define TGB_INC_DESC(x, y)     (x) = ((x + 1) & (y - 1))
#define TGB_ADDR_LO(y)         ((UINT64)((UINT32) (y)) & 0xFFFFFFFF)
#define TGB_ADDR_HI(y)         (((UINT64)((UINT32) (y)) >> 32) & 0xFFFFFFFF)
//#define TGB_ADJ(m)             (m)->m_data += 2
#define TGB_ADJ(m)             (m)->m_data += 8

enum tgb_isb_idx {
    TGB_ISB_HEADER,
    TGB_ISB_MISC,
    TGB_ISB_VEC0,
    TGB_ISB_VEC1,
    TGB_ISB_MAX
};

/* Link speed */
#define TGB_LINK_SPEED_UNKNOWN        0
#define TGB_LINK_SPEED_100_FULL       1
#define TGB_LINK_SPEED_1GB_FULL       2
#define TGB_LINK_SPEED_10GB_FULL      4
#define TGB_LINK_SPEED_10_FULL        8
#define TGB_LINK_SPEED_AUTONEG  (TGB_LINK_SPEED_100_FULL | \
                                 TGB_LINK_SPEED_1GB_FULL | \
                                 TGB_LINK_SPEED_10GB_FULL | \
                                 TGB_LINK_SPEED_10_FULL)


/*
 * Private adapter context structure.
 */

 typedef struct tgb_drv_ctrl
     {
     END_OBJ     tgbEndObj;
     void *      tgbDev;
     void *      tgbBar;
     void *      tgbHandle;
     void *      tgbMuxDevCookie;

     JOB_QUEUE_ID     tgbJobQueue;
     QJOB         tgbIntJob;
     atomic_t         tgbIntPending;
     UINT32      tgbIntrs;
     UINT32      tgbMoreRx;

     BOOL         tgbPolling;
     M_BLK_ID        tgbPollBuf;
     UINT32      tgbIntMask;

     UINT8         tgbAddr[ETHER_ADDR_LEN];

     END_CAPABILITIES     tgbCaps;

     END_IFDRVCONF     tgbEndStatsConf;
     END_IFCOUNTERS  tgbEndStatsCounters;

     SEM_ID      tgbDevSem;

     /* Begin MII/ifmedia required fields. */
     END_MEDIALIST     *tgbMediaList;
     END_ERR     tgbLastError;
     UINT32      tgbCurMedia;
     UINT32      tgbCurStatus;
     /* End MII/ifmedia required fields */

#ifdef TGB_VXB_DMA_BUF
     /* DMA tags and maps. */
     VXB_DMA_TAG_ID  tgbParentTag;

     VXB_DMA_TAG_ID  tgbRxDescTag;
     VXB_DMA_TAG_ID  tgbTxDescTag;
     VXB_DMA_TAG_ID  tgbIsbTag;

     VXB_DMA_MAP_ID  tgbRxDescMap;
     VXB_DMA_MAP_ID  tgbTxDescMap;
     VXB_DMA_MAP_ID  tgbIsbMap;

     VXB_DMA_TAG_ID  tgbMblkTag;

     VXB_DMA_MAP_ID  tgbRxMblkMap[TGB_RX_DESC_CNT];
     VXB_DMA_MAP_ID  tgbTxMblkMap[TGB_TX_DESC_CNT];
#else
     /* possibly unaligned desc buffer to free */
     char *      tgbDescBuf;
     char *      tgbIsbBuf;
#endif

     TGB_ADV_RDESC * tgbRxDescMem;
     TGB_ADV_TDESC * tgbTxDescMem;
     UINT32 *     isb_mem;

     M_BLK_ID        tgbTxMblk[TGB_TX_DESC_CNT];
     M_BLK_ID        tgbRxMblk[TGB_RX_DESC_CNT];

     UINT32      tgbTxProd;
     UINT32      tgbTxCons;
     UINT32      tgbTxFree;
     BOOL         tgbTxStall;

     UINT32      tgbRxIdx;

     UINT32      tgbLastCtx;
#ifdef CSUM_IPHDR_OFFSET
     int         tgbLastOffsets;
#else
     int         tgbLastIpLen;
#endif
     UINT16      tgbLastVlan;

     int         tgbMaxMtu;
     UINT32      tgbDevType;
	 UINT32      tgbAddrHi;
	 UINT32      tgbAddrLo;
	 UINT16      subSystemId;
	 UINT8       revisionId;
	 UINT32      lanId;
	 UINT32      phyLinkMode;
	 BOOL        orig_link_settings_stored;
	 UINT32      orig_sr_pcs_ctl2;
	 UINT32      orig_sr_pma_mmd_ctl1;
	 UINT32      orig_sr_an_mmd_ctl;
	 UINT32      orig_sr_an_mmd_adv_reg2;
	 UINT32      orig_vr_xs_or_pcs_mmd_digi_ctl1;
     
     } TGB_DRV_CTRL;


#if 1//(CPU_FAMILY == I80X86)  /*ldf 20230825 modify*/
     
#define TXGBE_CSR_READ_4(pDev, addr)                                  \
				*(volatile UINT32 *)(0x9000000000000000 | ((unsigned long)pDev->pRegBase + addr))\
				
     
#define TXGBE_CSR_WRITE_4(pDev, addr, data)                           \
			  do {                                                     \
				  volatile UINT32 *pReg =                             \
					  (UINT32 *)(0x9000000000000000 | ((unsigned long)pDev->pRegBase + addr));     \
				  *(pReg) = (UINT32)(data);                             \
			  } while ((0))
     
#else /* CPU_FAMILY != I80X86 */

#define TGB_BAR(p)   ((TGB_DRV_CTRL *)(p)->pDrvCtrl)->tgbBar
#define TGB_HANDLE(p)   ((TGB_DRV_CTRL *)(p)->pDrvCtrl)->tgbHandle
     
static inline u32 vxbRead32 (unsigned long addr)
{
	return (*(volatile unsigned int *)(addr | 0x9000000000000000));
}

static inline void vxbWrite32 (unsigned long addr, u32 value)
{
	*(volatile unsigned int *)(addr | 0x9000000000000000) = value;
}
     
#define TXGBE_CSR_READ_4(pDev, addr)                                  \
         vxbRead32 (((unsigned long)TGB_BAR(pDev) + addr))
     
#define TXGBE_CSR_WRITE_4(pDev, addr, data)                           \
        do {                                                     \
        	/*printk("%p %p\n",(UINT32 *)((char *)TGB_BAR(pDev) + addr),data);*/\
        	vxbWrite32 (((unsigned long)TGB_BAR(pDev) + addr), data);\
        } while ((0))
     
#endif /* CPU_FAMILY != I80X86 */
     
#define TXGBE_CSR_SETBIT_4(pDev, offset, val)          \
             TXGBE_CSR_WRITE_4(pDev, offset, TXGBE_CSR_READ_4(pDev, offset) | (val))
     
#define TXGBE_CSR_CLRBIT_4(pDev, offset, val)          \
             TXGBE_CSR_WRITE_4(pDev, offset, TXGBE_CSR_READ_4(pDev, offset) & ~(val))


#endif /* BSP_VERSION */

END_OBJ *tgbEndLoad(char * loadStr);

#ifdef __cplusplus
}
#endif

#endif /* __TxgbeEndh */


