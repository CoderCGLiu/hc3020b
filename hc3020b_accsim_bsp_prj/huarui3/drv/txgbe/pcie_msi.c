/* 
 * 作者:张亭
 * 日期:2017-11-27
 * 版权:中国电子科技集体公司第三十二研究所
 * 功能:gic msi中断实现
 */
#include <vxworks.h>
//#include <net_types.h>
#include <irq.h>
#include <stdio.h>
#include <reworks/printk.h>
#include <pci/pcibus.h>
#include <drv/pci/msi.h>
#include "if_txgbe.h"
#include "pcie_msi.h"



//#define GICD_BASE			0x29800000UL
//#define GICD_SETSPI_NSR		0x40
//#define GICD_CLRSPI_NSR		0x48
//#define GICD_SETSPI_SR		0x50
//#define GICD_CLRSPI_SR		0x58
#define MSI_INT_VECTOR_BASE     INT_LINK0//88 /*ldf 20230822 modify*/
#define HC3080_MSI_REG			0x1f4508fcUL /*ldf 20230823 modify:: 3080 MSIR*/

extern int get_gic_irqs();
static unsigned char msiNextIntVector = 0;

//static int msi_int_vector_max()
//{
//	return (get_gic_irqs() -1 + 32);
//}

static int msi_int_vector_base()
{
	return MSI_INT_VECTOR_BASE;
}

static void msi_init()
{
	static int msiInitFlag = 0;

	if (msiInitFlag)
	{
		return;
	}

	msiNextIntVector = msi_int_vector_base();
	msiInitFlag = 1;

	return;
}

int msi_request(MSI_MSG_T *msiMsg, int num)
{	
	//	if ((msiNextIntVector + num) > msi_int_vector_max())/*ldf 20230823 ignore*/
	//		return -1;

	msi_init();

	msiMsg->lowAddr = HC3080_MSI_REG;//GICD_BASE + GICD_SETSPI_NSR;
	msiMsg->highAddr = 0;
	msiMsg->baseData = (0x0 << 15) | (msiNextIntVector & 0xff); /*[15] 0-边沿触发,1-电平触发;  [0:7] 中断向量*/
	msiMsg->baseIntVector = MSI_INT_VECTOR_BASE;//msiNextIntVector; /*ldf 20230828 modify:: pcie中断号要固定*/

	msiNextIntVector += num;

	return 0;
}

int msi_int_install( int irq,
		void (*handler) (void*),  /* ISR */
		void *arg)
{
	int ret = 0;
	
	printk("<**DEBUG**> [%s():_%d_]:: irq = %d\n", __FUNCTION__, __LINE__, irq);
	
//	ret = int_install_handler("msi", irq, 1, handler, arg);
	ret = shared_int_install(irq, handler, arg);/*ldf 20230828 modify*/
	
	int_enable_pic(irq);
	
	return ret;
}

extern void clear_msi_int(int sel,int intr_sel,int is_clr_intr);/*ldf 20230828 add*/
extern PCI_BOARD_RESOURCE txgbe_pci_resources[TXGBE_MAX_DEV+2];
int msi_int_ack(PCI_DEV * pDev)
{
	unsigned int val = 0;
	int intVector = pDev->irq;
	int unit = pDev->unitNumber;
	NET_RESOURCE *pReso = (NET_RESOURCE *)(txgbe_pci_resources[unit].pExtended);
	unsigned short message_data = pReso->message_data;
	
//	val = *(volatile unsigned int*)(HC3080_MSI_REG|0x9000000000000000);
//	if(val != message_data)/*ldf 20230830 add:: 根据msi data来判断是不是当前port产生的中断*/
//	{
//		return -1;
//	}
//	printk("<**DEBUG**> [%s():_%d_]:: unit = %d |  ", __FUNCTION__, __LINE__,unit);
	
//	printk("MSI REG: 0x%x  ([4:0] Vector, [7:5] RegN)  |  ", val);
	
	val = *(volatile unsigned int*)((0x1f450000 + 0x1018)|0x9000000000000000);
//	printk("INTR STATUS: 0x%x  ([28] msi_int, [10] dma_int)\n", val);
	if(val & (1<<28))
		clear_msi_int(1, (1<<28), 1);
	*(volatile unsigned int*)((0x1f450000 + 0x1018)|0x9000000000000000) = val;
	
	//	*(volatile unsigned int*)(GICD_BASE + GICD_CLRSPI_NSR) = intVector;/*ldf 20230823 ignore*/
	return 0;
}


int pcie_msi_init(int bus,int device,int func, u32 vecaddr, u16 vecdata)
{
	int ret = -1;
	u8  msiBase = 0;
	u8 msiCtl;

	printk("<**DEBUG**> [%s():_%d_]:: [%d,%d,%d] vecaddr:0x%x, vecdata:0x%x\n", 
			__FUNCTION__, __LINE__,bus,device,func,vecaddr,vecdata);
	
	pci_config_extcap_find(PCI_EXT_CAP_MSI, bus, device, func, &msiBase);
	if (msiBase)
	{	
		ret = 0;

		printk("<**DEBUG**> [%s():_%d_]:: msiBase = 0x%x\n",__FUNCTION__, __LINE__,msiBase);

		//msiBase -= PCI_MSI_CTL;

		/* disable MSI */
		pci_read_config_byte(bus, device, func, msiBase + PCI_MSI_CTL, &msiCtl);
		printk("<**DEBUG**> [%s():_%d_]:: msiCtl = 0x%x ([bit0]: 1-MSI_EN, [bit7]: 1-64bit_Addr, [bit8]: 1-Per_Vector)\n",__FUNCTION__, __LINE__,msiCtl);
		msiCtl &= ~PCI_MSI_CTL_ENABLE;
		pci_write_config_byte(bus, device, func, msiBase + PCI_MSI_CTL, msiCtl);
		/* clear PCI_MSI_ADDR_LO */
		pci_write_config_dword(bus, device, func, msiBase + PCI_MSI_ADDR_LO, vecaddr);

		if (msiCtl & PCI_MSI_CTL_64BIT)
		{
			/* clear PCI_MSI_ADDR_HI and PCI_MSI_DATA_64 */
			pci_write_config_dword(bus, device, func, msiBase + PCI_MSI_ADDR_HI, 0);
			pci_write_config_word(bus, device, func, msiBase + PCI_MSI_DATA_64, vecdata);
		}
		else
		{
			/* clear PCI_MSI_DATA_32 */
			pci_write_config_word(bus, device, func, msiBase + PCI_MSI_DATA_32, vecdata);
		}

		/* enable MSI */
		pci_read_config_byte(bus, device, func, msiBase + PCI_MSI_CTL, &msiCtl);
		msiCtl |= PCI_MSI_CTL_ENABLE;
		pci_write_config_byte(bus, device, func, msiBase + PCI_MSI_CTL, msiCtl);

#if 1/*ldf 20230830 add:: print info*/
		u32 val = 0;
		if (msiCtl & PCI_MSI_CTL_64BIT)
		{
			pci_read_config_byte(bus, device, func, msiBase + PCI_MSI_CTL, &val);
			printk("==>64-bit Address,  Message Control: 0x%x ([bit0]: 1-MSI_EN)\n", val);
			
			val = 0;
			pci_read_config_dword(bus, device, func, msiBase + PCI_MSI_ADDR_LO, &val);
			printk("<**DEBUG**> [%s():_%d_]:: Message Address[31:0]: 0x%x\n",__FUNCTION__, __LINE__,val);
			
			val = 0;
			pci_read_config_word(bus, device, func, msiBase + PCI_MSI_ADDR_HI, &val);
			printk("<**DEBUG**> [%s():_%d_]:: Message Address[63:32]: 0x%x\n",__FUNCTION__, __LINE__,val);
	
			val = 0;
			pci_read_config_word(bus, device, func, msiBase + PCI_MSI_DATA_64, &val);
			printk("<**DEBUG**> [%s():_%d_]:: Message Data: 0x%x\n",__FUNCTION__, __LINE__,val);
	
			val = 0;
			pci_read_config_word(bus, device, func, msiBase + PCI_MSI_MASK_64, &val);
			printk("<**DEBUG**> [%s():_%d_]:: Mask Bits: 0x%x\n",__FUNCTION__, __LINE__,val);
		}
		else
		{
			pci_read_config_byte(bus, device, func, msiBase + PCI_MSI_CTL, &val);
			printk("==>32-bit Address,  Message Control: 0x%x ([bit0]: 1-MSI_EN)\n", val);
			
			val = 0;
			pci_read_config_dword(bus, device, func, msiBase + PCI_MSI_ADDR_LO, &val);
			printk("<**DEBUG**> [%s():_%d_]:: Message Address[31:0]: 0x%x\n",__FUNCTION__, __LINE__,val);
	
			val = 0;
			pci_read_config_word(bus, device, func, msiBase + PCI_MSI_DATA_32, &val);
			printk("<**DEBUG**> [%s():_%d_]:: Message Data: 0x%x\n",__FUNCTION__, __LINE__,val);
	
			val = 0;
			pci_read_config_word(bus, device, func, msiBase + PCI_MSI_MASK_32, &val);
			printk("<**DEBUG**> [%s():_%d_]:: Mask Bits: 0x%x\n",__FUNCTION__, __LINE__,val);
		}
#endif
	}
	else
	{
		printk("pci-msi is not supported!\r\n");
	}
	return ret;

}
