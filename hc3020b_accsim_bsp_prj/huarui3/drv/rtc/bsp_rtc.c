/******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件实现了MPC8245平台的实时时钟功能
 * 修改：
 * 		 1. 2011-09-14，BSP规范化 
 */
#include <reworks/types.h>
#include <string.h>
#include <time.h>
#include "rtc_lib.h"

/******************************************************************************
 * 
 * 设置实时时钟
 * 
 * 输入：
 * 		the_rm：要设置实时时钟信息
 * 输出：
 * 		无
 * 返回：
 * 		成功返回OS_OK；失败返回OS_ERROR
 */
int rtc_write(struct tm *the_tm)
{

	register u32 year, month, date, wday, hour, min, sec;
//	register int year_reg;
	
#if 1/*ldf 20230808 add:: 因为year寄存器range是0-99 所以能代表的年份是2000~2099 转成tm_year是(2000-1900)~(2099-1900)即100~199*/
	year = the_tm->tm_year + 1900;	//实际年份year是tm_year加上1900后的值
	if(year > 2099 || year < 2000)
	{
		return OS_ERROR;
	}
//	year_reg = the_tm->tm_year - 100;	//写入寄存器的值是tm_year再减去100 假如要设2022年tm_year传的是122 那写入寄存器的是22 因为寄存器year范围是0-99所以才要多减去这100 
#else
	year = the_tm->tm_year/*- 1900*/ ;
	if (year > 9999)
	{
		return OS_ERROR;
	}
#endif

//	month = the_tm->tm_mon ;
	month = the_tm->tm_mon + 1;//实际月份是tm_mon+1
	if (month == 0 || month > 12)
	{
		return OS_ERROR;
	}

	date = the_tm->tm_mday;
	if (date == 0 || date > 31)
	{
		return OS_ERROR;
	}

	switch(month)
	{
		case 2:
			if((year%4 == 0)&&(year%100 != 0)) //判断是否是闰年
			{
				if(date >29)
				{
					return OS_ERROR;
				}

			}else if(year%400 == 0)
			{
				if(date >29)
				{
					return OS_ERROR;
				}
			}else
			{
				if(date >28)
				{
					return OS_ERROR;
				}
			}
			break;						
		case 4:
		case 6:  
		case 9: 
		case 11:
			if(date > 30)
			{
				return OS_ERROR;
			}
			break;
	default:
		break;

	}
	 
//	wday = the_tm->tm_wday;
	wday = the_tm->tm_wday + 1;	/*ldf 20230808 modify:: tm_wday取值范围是0-6 写入寄存器是1-7 所以+1*/
	if (wday > 7)
	{
		return OS_ERROR;	
	}

	hour = the_tm->tm_hour;

	if (hour > 23)
	{
		return OS_ERROR;
	}
	
	min = the_tm->tm_min;
	if (min > 59)
	{
		return OS_ERROR;
	}
	
	sec = the_tm->tm_sec;
	if (sec > 59)
	{
		return OS_ERROR;
	}

	librtc_set_time(RTC_HC_REGS, hour, min, sec, 0);
	librtc_set_date(RTC_HC_REGS, year, month, date);
	
	return OS_OK;
}


/******************************************************************************
 * 
 * 读取实时时钟
 * 
 * 输入：
 * 		无
 * 输出：
 * 		the_rm：实时时钟返回值
 * 返回：
 * 		成功返回OS_OK；失败返回OS_ERROR
 */
int rtc_read(struct tm *the_tm)
{	

	u32 year, month, date, wday, hour, min, sec, second_percent;
	printk("<**DEBUG**> [%s():_%d_]:: librtc_get_time Start\n", __FUNCTION__, __LINE__);
	librtc_get_time(RTC_HC_REGS, &wday, &year, &month, &date, &hour, &min, &sec, &second_percent, 1);
	printk("<**DEBUG**> [%s():_%d_]:: librtc_get_time Return\n", __FUNCTION__, __LINE__);
	
	year = year%16 + year/16*10;
#if 1 /*ldf 20230808 add:: 假如是2022 存到寄存器里的是22  存到tm_year里的是2022-1900=122*/
	year += 100;
#else
    if(year < 50)
	{
		year += 100;
	}
#endif
       
    month = (month%16 + month/16*10);
	//month = (month%16 + month/16*10) - 1;
	wday = wday%16 /*+ wday/16*10*/;/*ldf 20230808 modify:: weekday一共才1-7*/
	date = date%16 + date/16*10;
	hour = hour%16 + hour/16*10;
	min  = min%16 + min/16*10;
	sec  = sec%16 + sec/16*10;
		
	memset(the_tm, 0, sizeof(struct tm));

	the_tm->tm_year = year ;     /* year */
	the_tm->tm_mon 	= month -1 ; /* month   */
	the_tm->tm_mday = date;      /* day     */
	the_tm->tm_wday = wday - 1;//wday;      /* weekday */ /*ldf 20230808 modify:: wday是1-7 tm_wday是0-6*/
	the_tm->tm_hour = hour;      /* hour    */
	the_tm->tm_min 	= min;       /* minutes */
	the_tm->tm_sec 	= sec;       /* seconds */

	return OS_OK;
}

void hrRtcRegionRelease(void)
{
    u32 tmp;

    /*配置其它寄存器的通道解锁*/
    phx_write_u32(0x1f0c0028, 0xa5accede); // unlock
	/*rtc配置release reset*/
	tmp = phx_read_u32(0x1f0c0004);
    phx_write_u32(0x1f0c0004, tmp & (~(0x1<<16)));
}

/******************************************************************************
 * 
 * 初始化实时时钟
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		无 
 */
int rtc_module_init(void)
{


	/* 设置时间 */

	/* 将当前时间转换成秒数 */


	/* 把秒数设置给系统实时时钟 */

    
	register int year, month, date, wday, hour, min, sec;
	struct tm the_tm;
    /* 初始化判断 */
	static int rtc_module_init_flag = 0;
	
	if (1 == rtc_module_init_flag)
	{
		return OS_ERROR;
	}
	
	hrRtcRegionRelease();/*ldf 20230810 add:: rtc release reset*/
	
	printk("<**DEBUG**> [%s():_%d_]:: rtc_read Start\n", __FUNCTION__, __LINE__);
	rtc_read(&the_tm);
	printk("<**DEBUG**> [%s():_%d_]:: rtc_read Return\n", __FUNCTION__, __LINE__);
	if((the_tm.tm_year > 150) || (the_tm.tm_hour > 23) || (the_tm.tm_mday > 31))
	{
		/*ldf 20230808 add:: 如果寄存器里读出来的时间不对 修改为2008-8-8 8:8:8 */
		the_tm.tm_year = 109 ; /* year */
		the_tm.tm_mon 	= 8 -1 ; /* month   */
		the_tm.tm_mday = 8; /* day     */
		//the_tm->tm_wday = wday; /* weekday */
		the_tm.tm_hour = 8; /* hour    */
		the_tm.tm_min 	= 8; /* minutes */
		the_tm.tm_sec 	= 8; /* seconds */
	}
	
	time_t t = mktime(&the_tm);
		
	struct timespec the_tp;
	the_tp.tv_sec = t;
	the_tp.tv_nsec = 0;
	clock_settime(CLOCK_REALTIME,&the_tp);
	
	rtc_module_init_flag = 1;

    return OS_OK;
}



void test_rtc(void)
{
	struct tm the_tm = {0};
	
	rtc_read(&the_tm);
	printk("%d-%d-%d %d:%d:%d\n",
			the_tm.tm_year, the_tm.tm_mon, the_tm.tm_mday,
			the_tm.tm_hour, the_tm.tm_min, the_tm.tm_sec);
	
	/*2023-12-13 14:15:16*/
	the_tm.tm_year = 2023 - 1900; 
	the_tm.tm_mon = 12 - 1; 
	the_tm.tm_mday = 13;
	the_tm.tm_hour = 14;
	the_tm.tm_min = 15;
	the_tm.tm_sec = 16;
	rtc_write(&the_tm);
	rtc_read(&the_tm);
	printk("%d-%d-%d %d:%d:%d\n",
			the_tm.tm_year, the_tm.tm_mon, the_tm.tm_mday,
			the_tm.tm_hour, the_tm.tm_min, the_tm.tm_sec);
}
