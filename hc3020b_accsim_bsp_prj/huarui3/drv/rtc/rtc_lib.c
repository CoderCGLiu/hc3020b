/* ******************************************************************************************
 * FILE NAME   : rtc_lib.c
 * PROGRAMMER  : zhaozz
 * DESCRIPTION : rtc_lib
 * DATE        : 2022-05-07 14:20:20
 * *****************************************************************************************/

#ifndef  __RTC_LIB_C__
#define  __RTC_LIB_C__

#include "./rtc_lib.h"

#define udebug printk
#define uinfo printf

//0:sunday 1-6:monday-saturday
int calc_week(unsigned int year, unsigned int month, unsigned int day)
{
	int c, y, week;
	if (month == 1 || month == 2)
		year--, month += 12;
	c = year / 100;
	y = year - c * 100;
	week = y + y/4 + c/4 - 2*c + 26*(month+1)/10 + day-1;
	while (week<0)
		week+=7;
	week %= 7;
	return week;
}


//24hour mode
int librtc_set_time(struct rtc_hc_regs *host, u32 hour, u32 minute, u32 second, u32 second_percent)
{
	udebug("\r\n");
	host->rtc_hc_ctrl.bits.TIME_COUNT_STOP = 1;
	udebug("\r\n");
	host->rtc_hc_12_24hour.bits.TIME_COUNT_12HOUR = 0;
	udebug("\r\n");

	RTC_HC_TIME timeone = {.v = 0};
	timeone.bits.SECOND_LOW = second%10;
	timeone.bits.SECOND_HIGH = second/10;
	timeone.bits.MINUTE_LOW = minute%10;
	timeone.bits.MINUTE_HIGH = minute/10;
	timeone.bits.HOUR_LOW    = hour%10;
	timeone.bits.HOUR_HIGH   = hour/10;
	timeone.bits.SECOND_PERCENT_LOW   = second_percent%10;
	timeone.bits.SECOND_PERCENT_HIGH  = second_percent/10;
	host->rtc_hc_time = timeone;
	udebug("\r\n");

	RTC_HC_STATUS status = host->rtc_hc_status;
	udebug("\r\n");

	if (status.bits.TIME_VALID)
	{
		uinfo("set time [%d:%d:%d.%d] succeed!\r\n", hour, minute, second, second_percent);
		return 0;
	} else {
		uinfo("set time [%d:%d:%d.%d] failed!\r\n", hour, minute, second, second_percent);
		return 1;
	}
	udebug("\r\n");

	host->rtc_hc_ctrl.bits.TIME_COUNT_STOP = 0;
	return 0;
}

int librtc_set_date(struct rtc_hc_regs* host, u32 year, u32 month, u32 day)
{
	host->rtc_hc_ctrl.bits.CALENDAR_COUNT_STOP = 1;

	RTC_HC_CALENDAR cal_one = {.v = 0};
	int weekday = calc_week(year, month, day);
	if (weekday == 0)
		weekday = 7;
	cal_one.bits.WEEK = weekday;
	cal_one.bits.MONTH_LOW = month%10;
	cal_one.bits.MONTH_HIGH = month/10;
	cal_one.bits.DAY_LOW = day%10;
	cal_one.bits.DAY_HIGH = day/10;
	cal_one.bits.YEAR_LOW = year%10;
	cal_one.bits.YEAR_HIGH = (year%100)/10;
	cal_one.bits.CENT_LOW = (year/100)%10;
	cal_one.bits.CENT_HIGH = (year/100)/10;
	host->rtc_hc_calendar = cal_one;

	RTC_HC_STATUS status = host->rtc_hc_status;

	if (status.bits.CALENDAR_VALID)
	{
		uinfo("set date [%d:%d:%d,week:%d] succeed!\r\n", year, month, day, weekday);
		return 0;
	} else {
		uinfo("set date [%d:%d:%d,week:%d] failed!\r\n", year, month, day, weekday);
		return 1;
	}
	host->rtc_hc_ctrl.bits.CALENDAR_COUNT_STOP = 0;
	return 0;
}

void librtc_get_time(struct rtc_hc_regs* host, u32* weekday, u32* year, u32* month, u32* day, u32* hour, u32* minute, u32* second, u32* second_percent, int do_print)
{
	printk("<**DEBUG**> [%s():_%d_]:: host=0x%lx\n", __FUNCTION__, __LINE__,host);
//	printk("<**DEBUG**> [%s():_%d_]:: host->rtc_hc_time=0x%lx\n", __FUNCTION__, __LINE__,host->rtc_hc_time);
	RTC_HC_TIME timeone = host->rtc_hc_time;
	printk("<**DEBUG**> [%s():_%d_]\n", __FUNCTION__, __LINE__);
	*hour = timeone.bits.HOUR_LOW + timeone.bits.HOUR_HIGH*10;
	printk("<**DEBUG**> [%s():_%d_]\n", __FUNCTION__, __LINE__);
	*minute = timeone.bits.MINUTE_LOW + timeone.bits.MINUTE_HIGH*10;
	printk("<**DEBUG**> [%s():_%d_]\n", __FUNCTION__, __LINE__);
	*second = timeone.bits.SECOND_LOW + timeone.bits.SECOND_HIGH*10;
	printk("<**DEBUG**> [%s():_%d_]\n", __FUNCTION__, __LINE__);
	*second_percent = timeone.bits.SECOND_PERCENT_LOW + timeone.bits.SECOND_PERCENT_HIGH*10;
	printk("<**DEBUG**> [%s():_%d_]\n", __FUNCTION__, __LINE__);

	RTC_HC_CALENDAR cal_one = host->rtc_hc_calendar;
	printk("<**DEBUG**> [%s():_%d_]\n", __FUNCTION__, __LINE__);
	*weekday = cal_one.bits.WEEK;
	printk("<**DEBUG**> [%s():_%d_]\n", __FUNCTION__, __LINE__);
	*year = cal_one.bits.YEAR_LOW + cal_one.bits.YEAR_HIGH*10 +  cal_one.bits.CENT_LOW*100 + cal_one.bits.CENT_HIGH*1000;
	printk("<**DEBUG**> [%s():_%d_]\n", __FUNCTION__, __LINE__);
	*month = cal_one.bits.MONTH_LOW + cal_one.bits.MONTH_HIGH*10;
	printk("<**DEBUG**> [%s():_%d_]\n", __FUNCTION__, __LINE__);
	*day = cal_one.bits.DAY_LOW + cal_one.bits.DAY_HIGH*10;
	printk("<**DEBUG**> [%s():_%d_]\n", __FUNCTION__, __LINE__);

	if (do_print)
		udebug("current time:[%d-%d-%d] [%d:%d:%d.%d] [week:%d]\r\n", year, month, day, hour, minute, second, second_percent, weekday);
}

//mask: month-day-hour-minute-second-percent
//      010100 means:
//      every specific houre-second arise date_time alarm
void librtc_set_alarm_date_time_mask(struct rtc_hc_regs* host, u32 year, u32 month, u32 day, u32 hour, u32 minute, u32 second, u32 second_percent, u32 mask)
{
	host->rtc_hc_interrupt_disable.bits.ALARM = 1;
	host->rtc_hc_12_24hour.bits.TIME_COUNT_12HOUR = 0;

	RTC_HC_TIME_ALARM timeone = {.v = 0};
	timeone.bits.SECOND_LOW = second%10;
	timeone.bits.SECOND_HIGH = second/10;
	timeone.bits.MINUTE_LOW = minute%10;
	timeone.bits.MINUTE_HIGH = minute/10;
	timeone.bits.HOUR_LOW    = hour%10;
	timeone.bits.HOUR_HIGH   = hour/10;
	timeone.bits.SECOND_PERCENT_LOW   = second_percent%10;
	timeone.bits.SECOND_PERCENT_HIGH  = second_percent/10;
	host->rtc_hc_time_alarm = timeone;

	RTC_HC_CALENDAR_ALARM dateone = {.v = 0};
	dateone.bits.MONTH_LOW = month%10;
	dateone.bits.MONTH_HIGH = month/10;
	dateone.bits.DAY_LOW = day%10;
	dateone.bits.DAY_HIGH = day/10;
	host->rtc_hc_calendar_alarm = dateone;

	host->rtc_hc_alarm_enable.v = mask;
	host->rtc_hc_interrupt_enable.bits.ALARM = 1;
}

//static void static_assert_check()
//{
//	ustatic_assert(0x10 == uoffsetof(struct rtc_hc_regs, rtc_hc_time_alarm));
//	ustatic_assert(0x30 == uoffsetof(struct rtc_hc_regs, rtc_hc_keep_rtc));
//}

void librtc_enable_int(struct rtc_hc_regs *host)
{
	host->rtc_hc_interrupt_disable.v = 0;
	host->rtc_hc_interrupt_enable.v = 0xffffffff;
}

void librtc_disable_int(struct rtc_hc_regs *host)
{
	host->rtc_hc_interrupt_enable.v = 0;
	host->rtc_hc_interrupt_disable.v = 0xffffffff;
}

#endif  /* __RTC_LIB_C__ */
