/* ******************************************************************************************
 * FILE NAME   : rtc_lib.h
 * PROGRAMMER  : zhaozz
 * DESCRIPTION : rtc_lib.h
 * DATE        : 2022-05-07 14:32:47
 * *****************************************************************************************/
#ifndef  __RTC_LIB_H__
#define  __RTC_LIB_H__

#include <sys/types.h>
#include "../../h/drvCommon.h"

typedef union {
	volatile u32 v;
	struct {
		volatile u32 TIME_COUNT_STOP             :1;
		volatile u32 CALENDAR_COUNT_STOP         :1;
	} bits;
} RTC_HC_CTRL;

typedef union {
	volatile u32 v;
	struct {
		volatile u32 TIME_COUNT_12HOUR           :1;
	} bits;
} RTC_HC_12_24HOUR;

typedef union {
	volatile u32 v;
	struct {
		volatile u32 SECOND_PERCENT_LOW            :4;
		volatile u32 SECOND_PERCENT_HIGH           :4;
		volatile u32 SECOND_LOW                    :4;
		volatile u32 SECOND_HIGH                   :3;
		volatile u32 __RESVED0                     :1;
		volatile u32 MINUTE_LOW                    :4;
		volatile u32 MINUTE_HIGH                   :3;
		volatile u32 __RESVED1                     :1;
		volatile u32 HOUR_LOW                      :4;
		volatile u32 HOUR_HIGH                     :2;
		volatile u32 PM_12HOUR                     :1;
		volatile u32 CALENDAR_CHANGE               :1;
	} bits;
} RTC_HC_TIME;

typedef union {
	volatile u32 v;
	struct {
		volatile u32 WEEK                          :3;
		volatile u32 MONTH_LOW                     :4;
		volatile u32 MONTH_HIGH                    :1;
		volatile u32 DAY_LOW                       :4;
		volatile u32 DAY_HIGH                      :2;
		volatile u32 __RESVED0                     :2;
		volatile u32 YEAR_LOW                      :4;
		volatile u32 YEAR_HIGH                     :4;
		volatile u32 CENT_LOW                      :4;
		volatile u32 CENT_HIGH                     :2;
		volatile u32 __RESVED1                     :1;
		volatile u32 CALENDAR_CHANGE               :1;
	} bits;
} RTC_HC_CALENDAR;

typedef union {
	volatile u32 v;
	struct {
		volatile u32 SECOND_PERCENT_LOW            :4;
		volatile u32 SECOND_PERCENT_HIGH           :4;
		volatile u32 SECOND_LOW                    :4;
		volatile u32 SECOND_HIGH                   :3;
		volatile u32 __RESVED0                     :1;
		volatile u32 MINUTE_LOW                    :4;
		volatile u32 MINUTE_HIGH                   :3;
		volatile u32 __RESVED1                     :1;
		volatile u32 HOUR_LOW                      :4;
		volatile u32 HOUR_HIGH                     :2;
		volatile u32 PM_12HOUR                     :1;
		volatile u32 __RESVED2                     :1;
	} bits;
} RTC_HC_TIME_ALARM;

typedef union {
	volatile u32 v;
	struct {
		volatile u32 __RESVED0                     :3;
		volatile u32 MONTH_LOW                     :4;
		volatile u32 MONTH_HIGH                    :1;
		volatile u32 DAY_LOW                       :4;
		volatile u32 DAY_HIGH                      :2;
		volatile u32 __RESVED1                     :18;
	} bits;
} RTC_HC_CALENDAR_ALARM;

typedef union {
	volatile u32 v;
	struct {
		volatile u32 SECOND_PERCENT                :1;
		volatile u32 SECOND                        :1;
		volatile u32 MINUTE                        :1;
		volatile u32 HOUR                          :1;
		volatile u32 DAY                           :1;
		volatile u32 MONTH                         :1;
		volatile u32 __RESVED0                     :26;
	} bits;
} RTC_HC_ALARM_ENABLE;

typedef union {
	volatile u32 v;
	struct {
		volatile u32 SECOND_PERCENT                :1;
		volatile u32 SECOND                        :1;
		volatile u32 MINUTE                        :1;
		volatile u32 HOUR                          :1;
		volatile u32 DAY                           :1;
		volatile u32 MONTH                         :1;
		volatile u32 __RESVED0                     :26;
	} bits;
} RTC_HC_EVENT_FLAGS;

typedef union {
	volatile u32 v;
	struct {
		volatile u32 SECOND_PERCENT                :1;
		volatile u32 SECOND                        :1;
		volatile u32 MINUTE                        :1;
		volatile u32 HOUR                          :1;
		volatile u32 DAY                           :1;
		volatile u32 MONTH                         :1;
		volatile u32 ALARM                         :1;
		volatile u32 __RESVED0                     :25;
	} bits;
} RTC_HC_INTERRUPT_ENABLE;

typedef union {
	volatile u32 v;
	struct {
		volatile u32 SECOND_PERCENT                :1;
		volatile u32 SECOND                        :1;
		volatile u32 MINUTE                        :1;
		volatile u32 HOUR                          :1;
		volatile u32 DAY                           :1;
		volatile u32 MONTH                         :1;
		volatile u32 ALARM                         :1;
		volatile u32 __RESVED0                     :25;
	} bits;
} RTC_HC_INTERRUPT_DISABLE;

typedef union {
	volatile u32 v;
	struct {
		volatile u32 SECOND_PERCENT                :1;
		volatile u32 SECOND                        :1;
		volatile u32 MINUTE                        :1;
		volatile u32 HOUR                          :1;
		volatile u32 DAY                           :1;
		volatile u32 MONTH                         :1;
		volatile u32 ALARM                         :1;
		volatile u32 __RESVED0                     :25;
	} bits;
} RTC_HC_INTERRUPT_MASK;

typedef union {
	volatile u32 v;
	struct {
		volatile u32 TIME_VALID                    :1;
		volatile u32 CALENDAR_VALID                :1;
		volatile u32 TIME_ALARM_VALID              :1;
		volatile u32 CALENDAR_ALARM_VALID          :1;
		volatile u32 __RESVED0                     :28;
	} bits;
} RTC_HC_STATUS;

typedef union {
	volatile u32 v;
	struct {
		volatile u32 KEEP_RTC                      :1;
		volatile u32 __RESV0                       :31;
	} bits;
} RTC_HC_KEEP_RTC;


struct rtc_hc_regs {
	volatile  RTC_HC_CTRL                              rtc_hc_ctrl              ;
	volatile  RTC_HC_12_24HOUR                         rtc_hc_12_24hour         ;
	volatile  RTC_HC_TIME                              rtc_hc_time              ;
	volatile  RTC_HC_CALENDAR                          rtc_hc_calendar          ;
	volatile  RTC_HC_TIME_ALARM                        rtc_hc_time_alarm        ;
	volatile  RTC_HC_CALENDAR_ALARM                    rtc_hc_calendar_alarm    ;
	volatile  RTC_HC_ALARM_ENABLE                      rtc_hc_alarm_enable      ;
	volatile  RTC_HC_EVENT_FLAGS                       rtc_hc_event_flags       ;
	volatile  RTC_HC_INTERRUPT_ENABLE                  rtc_hc_interrupt_enable  ;
	volatile  RTC_HC_INTERRUPT_DISABLE                 rtc_hc_interrupt_disable ;
	volatile  RTC_HC_INTERRUPT_MASK                    rtc_hc_interrupt_mask    ;
	volatile  RTC_HC_STATUS                            rtc_hc_status            ;
	volatile  RTC_HC_KEEP_RTC                          rtc_hc_keep_rtc          ;
};

#define RTC_REG_BASE (0x9000000000000000 | 0x1F0C8000)
#define RTC_HC_REGS ((struct rtc_hc_regs*)RTC_REG_BASE)

#endif  /* __RTC_LIB_H__ */
