/******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件实现了GDMA驱动模板
 * 修改：
 * 		2022-04-05，tank，建立代码模板
 */

#include <stdlib.h>
#include "hr3GeneralDma.h"
#include "stdio.h"
#include "string.h"
#include "tool/hr3SysTools.h"
#include "irq.h"
#include "sysLib.h" 
#include "taskLib.h" 
#include "semLib.h"
/*********************************************************************************************************
  调试信息，全部定义为全局可见
*********************************************************************************************************/
HRGDMADBGINFO *              g_pGdmaDbgInfo       = (HRGDMADBGINFO *)DBG_GDMA_BASE_ADDR;
static unsigned long long    s_gdmaTotalTransSize = 0;                  /*  记录 GDMA 传输的总数据量(B) */
static int                   s_gdmaTransState     = 0;                  /*  记录一次 DMA 传输的状态          */
static unsigned int          s_gdmaIntNum         = 0;                  /*  记录通用 DMA 中断个数             */
int                          g_gdmaDbgBscSign     = 0;                  /*  记录基本调试信息的标识           */
                                                                        /*  0 表示不记录，1 表示记录      */
int                          g_gdmaDbgDtlSign     = 0;                  /*  记录详细调试信息的标识，       */
                                                                        /*  0 表示不记录，1 表示记录      */
static unsigned int          s_gdmaConfigNum      = 0;                  /*  记录通用 DMA 配置寄存器次数 */
                                                                        /*  一维、二维和矩阵配置的总和   */
/*********************************************************************************************************
  信号量定义
*********************************************************************************************************/
SEM_ID semID_GDMA;                                                      /*  the sem for gdma            */
SEM_ID semID_GDMA_Lock;                                                 /*  the sem for gdma lock       */
/*********************************************************************************************************
** 函数名称: hrGdmaInt
** 功能描述: GDMA 中断处理函数
** 输　入  : NONE
** 输　出  : LW_IRQ_HANDLED
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static  void  hrGdmaInt (void)
{
    unsigned int  uiVal = 0;

//    UINT dtVal = 0;

    if (g_gdmaDbgBscSign == 1) {
        g_pGdmaDbgInfo->intNumIn = g_pGdmaDbgInfo->intNumIn + 1;
    }
    s_gdmaIntNum = s_gdmaIntNum + 1;
#if 0
    uiVal = HR2_REG_READ32(DMA_CSR_ADDR(0, DMA_IRQ));
//    _DebugFormat(__PRINTMESSAGE_LEVEL, "uiVal = 0x%x.\r\n",uiVal);
    if (uiVal & 0xf0000) {                                              /*  传输完成处理                */
        HR2_REG_WRITE32(DMA_CSR_ADDR(0, DMA_IRQ),
                       uiVal & 0xfff0ffff);                             /*  清中断                      */

//        uiVal = HR2_REG_READ32(DMA_CSR_ADDR(0, DMA_IRQ));
//        _DebugFormat(__PRINTMESSAGE_LEVEL, "uiVal = 0x%x.\r\n",uiVal);
//
//        dtVal = HR2_REG_READ32(DMA_CSR_ADDR(0, DMA_DONE_INT_L));
//        _DebugFormat(__PRINTMESSAGE_LEVEL, "dtVal = 0x%x.\r\n",dtVal);

        HR2_REG_WRITE32(DMA_CSR_ADDR(0, DMA_DONE_INT_L),0);
//
//        uiVal = HR2_REG_READ32(DMA_CSR_ADDR(0, DMA_IRQ));
//        _DebugFormat(__PRINTMESSAGE_LEVEL, "uiVal = 0x%x.\r\n",uiVal);
//
//        dtVal = HR2_REG_READ32(DMA_CSR_ADDR(0, DMA_DONE_INT_L));
//        _DebugFormat(__PRINTMESSAGE_LEVEL, "dtVal = 0x%x.\r\n",dtVal);

        s_gdmaTransState = 0;
        semGive(semID_GDMA);

    } else if (uiVal & 0x0f000) {
        HR2_REG_WRITE32(DMA_CSR_ADDR(0, DMA_IRQ),
                        uiVal & 0xffff0fff);
        HR2_REG_WRITE32(DMA_CSR_ADDR(0, DMA_STOP_INT_L),0);
        s_gdmaTransState = -1;

    } else if (uiVal & 0x00f00) {
        HR2_REG_WRITE32(DMA_CSR_ADDR(0, DMA_IRQ),
                        uiVal & 0xfffff0ff);
        HR2_REG_WRITE32(DMA_CSR_ADDR(0, DMA_AXI_ERR_INT_L),0);
        s_gdmaTransState = -2;
    }
#else
    uiVal = HR2_REG_READ32(DMA_CSR_ADDR(0, DMA_DONE_INT_L));
    if (uiVal & 0x1)
    {
        HR2_REG_WRITE32(DMA_CSR_ADDR(0, DMA_DONE_INT_L),1);
        s_gdmaTransState = 0;
        semGive(semID_GDMA);
    }

    uiVal = HR2_REG_READ32(DMA_CSR_ADDR(0, DMA_STOP_INT_L));
    if (uiVal & 0x1)
    {
        HR2_REG_WRITE32(DMA_CSR_ADDR(0, DMA_STOP_INT_L),0);
        s_gdmaTransState = -1;
    }

    uiVal = HR2_REG_READ32(DMA_CSR_ADDR(0, DMA_AXI_ERR_INT_L));
    if (uiVal & 0x1)
    {
        HR2_REG_WRITE32(DMA_CSR_ADDR(0, DMA_AXI_ERR_INT_L),0);
        s_gdmaTransState = -2;
    }

#endif


    if (g_gdmaDbgBscSign == 1) {
        g_pGdmaDbgInfo->intNumOut = g_pGdmaDbgInfo->intNumOut + 1;
    }

    return;
}
/*********************************************************************************************************
** 函数名称: hrGdmaInstConnect
** 功能描述: GDMA 中断连接函数
** 输　入  : NONE
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
void  hrGdmaInstConnect (void)
{
	int_install_handler("GPDMA", INT_GPDMA, 1, (INT_HANDLER)hrGdmaInt, NULL);
	int_enable_pic(INT_GPDMA);
}
/*********************************************************************************************************
** 函数名称: hrGdmaInstInit
** 功能描述: GDMA 初始化入口
** 输　入  : NONE
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
void  hrGdmaInstInit (void)
{
    semID_GDMA      = semBCreate(SEM_Q_PRIORITY, SEM_EMPTY);
    semID_GDMA_Lock = semBCreate(SEM_Q_PRIORITY, SEM_FULL);

    HR2_REG_WRITE32(DMA_CSR_ADDR(0, DMA_IRQ), 0xfff00000);

    hrGdmaInstConnect();
}
/*********************************************************************************************************
** 函数名称: hrGdmaGetIntNum
** 功能描述: 获取 GDMA 中断个数
** 输　入  : NONE
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
UINT hrGdmaGetIntNum (VOID)
{
    return s_gdmaIntNum;
}
/*********************************************************************************************************
** 函数名称: hrGdmaGetTransState
** 功能描述: 获取 GDMA 转置状态
** 输　入  : NONE
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
int  hrGdmaGetTransState (void)
{
    return s_gdmaTransState;
}
/*********************************************************************************************************
** 函数名称: hrGdma1DTransConfig
** 功能描述: GDMA 一维传输配置
** 输　入  : uiChannel   通道
**           pcSrcAddr   源地址
**           pcDstAddr   目的地址
**           uiSize      传输大小
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
void  hrGdma1DTransConfig (unsigned int  uiChannel, char *  pcSrcAddr, char *  pcDstAddr, unsigned int  uiSize)
{
	unsigned int  uiData[16] = {0};

    semTake(semID_GDMA_Lock, WAIT_FOREVER);
    if (g_gdmaDbgDtlSign == 1) {
        g_pGdmaDbgInfo->hrGdmaParam[s_gdmaConfigNum % DBG_GDMA_INDEX_NUM].srcAddr  = (ULONG)pcSrcAddr;
        g_pGdmaDbgInfo->hrGdmaParam[s_gdmaConfigNum % DBG_GDMA_INDEX_NUM].dstAddr  = (ULONG)pcDstAddr;
        g_pGdmaDbgInfo->hrGdmaParam[s_gdmaConfigNum % DBG_GDMA_INDEX_NUM].uiLength = uiSize;
        g_pGdmaDbgInfo->newRecord = s_gdmaConfigNum % DBG_GDMA_INDEX_NUM;
        s_gdmaConfigNum = s_gdmaConfigNum + 1;
    }

    uiData[0] = ((0x1 << GPDMA_CSR_R_AR_BURST_TYPE) |                   /* [19~20] ar_burst_type only 1:incr */
                 (0x0 << GPDMA_CSR_R_AR_CACHE_TYPE) |                   /* [15~18] ar_cache_type only 0 */
                 (0x5 << GPDMA_CSR_R_AR_BURST_SIZE));                   /* [12~14] ar_burst_size only 5:32 byte */

    uiData[1]  = uiSize;                                                /*  CHUNK                       */
    uiData[2]  = uiSize;                                                /*  TOTAL                       */
    uiData[3]  = uiSize;                                                /*  BLOCK                       */
    uiData[4]  = (UINT)pcSrcAddr;                                       /*  SRC_ADR_L                   */
    uiData[5]  = (UINT)pcDstAddr;                                       /*  DST_ADR_L                   */
    uiData[6]  = ((UINT64)pcSrcAddr >> 32);                             /*  SRC_ADR_H                   */
    uiData[7]  = ((UINT64)pcDstAddr >> 32);                             /*  DST_ADR_H                   */
    uiData[8]  = 0x0;                                                   /*  ROW_NUM                     */
    uiData[9]  = 0x0;                                                   /*  COL_NUM                     */
    uiData[10] = uiSize;                                                /*  DST_BLOCK                   */
    uiData[11] = 0x0;                                                   /*  DESC_L                      */
    uiData[12] = 0x0;                                                   /*  DESC_H                      */
    uiData[13] = 0x0;                                                   /*  CSR_NEXT                    */

    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_CSR_R),     uiData[0]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_CHUNK),     uiData[1]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_TOTAL),     uiData[2]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_BLOCK),     uiData[3]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_SRC_ADR_L), uiData[4]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_DST_ADR_L), uiData[5]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_SRC_ADR_H), uiData[6]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_DST_ADR_H), uiData[7]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_ROW_NUM),   uiData[8]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_COL_NUM),   uiData[9]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_DST_BLOCK), uiData[10]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_DESC_L),    uiData[11]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_DESC_H),    uiData[12]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_CSR_NEXT),  uiData[13]);

    s_gdmaTotalTransSize = s_gdmaTotalTransSize + uiSize;
}
/*********************************************************************************************************
** 函数名称: hrGdma2DTransConfig
** 功能描述: GDMA 二维传输配置
** 输　入  : uiChannel   通道
**           pcSrcAddr   源地址
**           pcDstAddr   目的地址
**           uiSize      传输大小
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
void  hrGdma2DTransConfig (unsigned int  uiChannel, unsigned int   uiTotal,   unsigned int   uiSrcChunk,
		unsigned int  uiSrcBlk,  char *  pcSrcAddr, char *  pcDstAddr, unsigned int  uiDstBlk)
{
	unsigned int uiData[16] = {0};

    semTake(semID_GDMA_Lock, WAIT_FOREVER);

    if (g_gdmaDbgDtlSign == 1) {
        g_pGdmaDbgInfo->hrGdmaParam[s_gdmaConfigNum % DBG_GDMA_INDEX_NUM].srcAddr  = (ULONG)pcSrcAddr;
        g_pGdmaDbgInfo->hrGdmaParam[s_gdmaConfigNum % DBG_GDMA_INDEX_NUM].dstAddr  = (ULONG)pcDstAddr;
        g_pGdmaDbgInfo->hrGdmaParam[s_gdmaConfigNum % DBG_GDMA_INDEX_NUM].uiLength = uiTotal;
        g_pGdmaDbgInfo->newRecord = s_gdmaConfigNum % DBG_GDMA_INDEX_NUM;
        s_gdmaConfigNum = s_gdmaConfigNum + 1;
    }

    uiData[0]  = ((0x1 << GPDMA_CSR_R_AR_BURST_TYPE) |                  /*  [19~20] ar_burst_type only 1:incr */
                  (0x0 << GPDMA_CSR_R_AR_CACHE_TYPE) |                  /*  [15~18] ar_cache_type only 0 */
                  (0x5 << GPDMA_CSR_R_AR_BURST_SIZE));                  /*  [12~14] ar_burst_size only 5:32 byte */
    uiData[1]  = uiSrcChunk;                                            /*  CHUNK                       */
    uiData[2]  = uiTotal;                                               /*  TOTAL                       */
    uiData[3]  = uiSrcBlk;                                              /*  BLOCK                       */
    uiData[4]  = (UINT)pcSrcAddr;                                       /*  SRC_ADR_L                   */
    uiData[5]  = (UINT)pcDstAddr;                                       /*  DST_ADR_L                   */
    uiData[6]  = ((UINT64)pcSrcAddr >> 32);                             /*  SRC_ADR_H                   */
    uiData[7]  = ((UINT64)pcDstAddr >> 32);                             /*  DST_ADR_H                   */
    uiData[8]  = 0;                                                     /*  ROW_NUM                     */
    uiData[9]  = 0;                                                     /*  COL_NUM                     */
    uiData[10] = uiDstBlk;                                              /*  DST_BLOCK                   */
    uiData[11] = 0;                                                     /*  DESC_L                      */
    uiData[12] = 0;                                                     /*  DESC_H                      */
    uiData[13] = 0;                                                     /*  CSR_NEXT                    */

    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_CSR_R),     uiData[0]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_CHUNK),     uiData[1]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_TOTAL),     uiData[2]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_BLOCK),     uiData[3]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_SRC_ADR_L), uiData[4]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_DST_ADR_L), uiData[5]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_SRC_ADR_H), uiData[6]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_DST_ADR_H), uiData[7]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_ROW_NUM),   uiData[8]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_COL_NUM),   uiData[9]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_DST_BLOCK), uiData[10]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_DESC_L),    uiData[11]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_DESC_H),    uiData[12]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_CSR_NEXT),  uiData[13]);

    s_gdmaTotalTransSize = s_gdmaTotalTransSize + uiTotal;
}
/*********************************************************************************************************
** 函数名称: hrGdmaMatrixTransConfig
** 功能描述: GDMA 矩阵转置配置
** 输　入  : uiChannel   通道
**           pcSrcAddr   源地址
**           pcDstAddr   目的地址
**           uiSize      传输大小
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
void  hrGdmaMatrixTransConfig (unsigned int  uiChannel, unsigned int   uiTotal,   unsigned int   uiChunk,
		unsigned int  uiBlock,   char *  pcSrcAddr, char *  pcDstAddr,
		unsigned int  uiRownum,  unsigned int   uiColnum,  unsigned int   uiDstblk)
{
	unsigned int  uiDate[16] = {0};

    semTake(semID_GDMA_Lock, WAIT_FOREVER);

    if (g_gdmaDbgDtlSign == 1) {
        g_pGdmaDbgInfo->hrGdmaParam[s_gdmaConfigNum % DBG_GDMA_INDEX_NUM].srcAddr  = (ULONG)pcSrcAddr;
        g_pGdmaDbgInfo->hrGdmaParam[s_gdmaConfigNum % DBG_GDMA_INDEX_NUM].dstAddr  = (ULONG)pcDstAddr;
        g_pGdmaDbgInfo->hrGdmaParam[s_gdmaConfigNum % DBG_GDMA_INDEX_NUM].uiLength = uiTotal;
        g_pGdmaDbgInfo->newRecord = s_gdmaConfigNum % DBG_GDMA_INDEX_NUM;
        s_gdmaConfigNum = s_gdmaConfigNum + 1;
    }

    uiDate[0]  = ((0x1 << GPDMA_CSR_R_AR_BURST_TYPE) |                  /*  [19~20] ar_burst_type only 1:incr */
                  (0x0 << GPDMA_CSR_R_AR_CACHE_TYPE) |                  /*  [15~18] ar_cache_type only 0 */
                  (0x5 << GPDMA_CSR_R_AR_BURST_SIZE));                  /*  [12~14] ar_burst_size only 5:32 byte */
    uiDate[1]  = uiChunk;                                               /*  CHUNK                       */
    uiDate[2]  = uiTotal;                                               /*  TOTAL                       */
    uiDate[3]  = uiBlock;                                               /*  BLOCK                       */
    uiDate[4]  = (UINT)pcSrcAddr;                                       /*  SRC_ADR_L                   */
    uiDate[5]  = (UINT)pcDstAddr;                                       /*  DST_ADR_L                   */
    uiDate[6]  = ((UINT64)pcSrcAddr >> 32);                             /*  SRC_ADR_H                   */
    uiDate[7]  = ((UINT64)pcDstAddr >> 32);                             /*  DST_ADR_H                   */
    uiDate[8]  = uiRownum;                                              /*  ROW_NUM                     */
    uiDate[9]  = uiColnum;                                              /*  COL_NUM                     */
    uiDate[10] = uiDstblk;                                              /*  DST_BLOCK                   */
    uiDate[11] = 0;                                                     /*  DESC_L                      */
    uiDate[12] = 0;                                                     /*  DESC_H                      */
    uiDate[13] = 0;                                                     /*  CSR_NEXT                    */

    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_CSR_R),     uiDate[0]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_CHUNK),     uiDate[1]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_TOTAL),     uiDate[2]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_BLOCK),     uiDate[3]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_SRC_ADR_L), uiDate[4]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_DST_ADR_L), uiDate[5]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_SRC_ADR_H), uiDate[6]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_DST_ADR_H), uiDate[7]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_ROW_NUM),   uiDate[8]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_COL_NUM),   uiDate[9]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_DST_BLOCK), uiDate[10]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_DESC_L),    uiDate[11]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_DESC_H),    uiDate[12]);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_CSR_NEXT),  uiDate[13]);

    s_gdmaTotalTransSize = s_gdmaTotalTransSize + uiTotal;
}
/*********************************************************************************************************
** 函数名称: hrGdmaStart
** 功能描述: GDMA 启动传输
** 输　入  : uiChannel      通道
**           uiTransMode    为 0 时，一维传输；
**                          为 1 时，二维至二维传输；
**                          为 2 时，二维至一维传输；
**                          为 3 时，一维至二维传输；
**                          为 4 时，为矩阵转置传输；
**           uiElementSize  元素大小
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
void  hrGdmaStart (unsigned int  uiChannel, unsigned int  uiTransMode, unsigned int  uiElementSize)
{
	unsigned int                uiData = 0;
    struct hr_gdma_desc gdmaDesc;

    if (g_gdmaDbgBscSign == 1) {
        g_pGdmaDbgInfo->dmaStartNum = g_pGdmaDbgInfo->dmaStartNum + 1;
    }

    if (uiChannel > 4) {
        printf("hrGdmaStart:error,param channel not support!\r\n");
        return;
    }

    gdmaDesc.CSR_STOP            = 0;
    gdmaDesc.CSR_RESERVED_B29_30 = 0;
    gdmaDesc.CSR_FORCE_DESC      = 0;
    gdmaDesc.CSR_NEXT_DESC       = 0;
    gdmaDesc.CSR_INT_DONE_EN     = 1;
    gdmaDesc.CSR_INT_ERR_EN      = 1;
    gdmaDesc.CSR_RESERVED_B24    = 0;

    if (uiTransMode == 4) {
        if (uiElementSize == 4) {
            gdmaDesc.CSR_MATRIX_POINT = 2;                              /*  矩阵元素大小                */
        } else if(uiElementSize == 8) {
            gdmaDesc.CSR_MATRIX_POINT = 3;                              /*  矩阵元素大小                */
        }

        gdmaDesc.CSR_MATRIX_REQ = 1;

    } else {
        gdmaDesc.CSR_MATRIX_POINT = 0;
        gdmaDesc.CSR_MATRIX_REQ   = 0;
    }

    gdmaDesc.CSR_AW_BURST_TYPE = 1;
    gdmaDesc.CSR_AW_CACHE_TYPE = 0;
    gdmaDesc.CSR_AW_BURST_SIZE = 5;

    if (uiTransMode == 4) {
        gdmaDesc.CSR_READ_REQ_NUM = 8;
    } else {
        gdmaDesc.CSR_READ_REQ_NUM = 8;
    }

    gdmaDesc.CSR_REQ       = 1;
    gdmaDesc.CSR_ADR_1_DST = 0;
    gdmaDesc.CSR_ADR_0_SRC = 0;

    if (uiTransMode == 2) {
        gdmaDesc.CSR_ADR_0_SRC = 1;
    } else if (uiTransMode == 3) {
        gdmaDesc.CSR_ADR_1_DST = 1;
    }

    gdmaDesc.CSR_INC_SRC = 1;
    gdmaDesc.CSR_INC_DST = 1;
    gdmaDesc.CSR_EN      = 0;

    memcpy((void *)&uiData, (void *)&gdmaDesc, 4);
    HR2_REG_WRITE32(DMA_CSR_ADDR(uiChannel, DMA_CSR),uiData);
    semTake(semID_GDMA, WAIT_FOREVER);
    semGive(semID_GDMA_Lock);

    if (g_gdmaDbgBscSign == 1) {
        g_pGdmaDbgInfo->dmaFinishNum = g_pGdmaDbgInfo->dmaFinishNum + 1;
    }
}
/*********************************************************************************************************
** 函数名称: getGDmaChannelReg
** 功能描述: GDMA 获取通道寄存器配置
** 输　入  : uiChannel      通道
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
int  getGDmaChannelReg (unsigned int  uiChannel, unsigned int * puiConfigAddr)
{
    if (puiConfigAddr == NULL) {
        return  (ERROR);
    }

    puiConfigAddr[0]  = HR2_REG_READ32(DMA_CSR_ADDR(uiChannel, DMA_CSR_R));
    puiConfigAddr[1]  = HR2_REG_READ32(DMA_CSR_ADDR(uiChannel, DMA_CHUNK));
    puiConfigAddr[2]  = HR2_REG_READ32(DMA_CSR_ADDR(uiChannel, DMA_TOTAL));
    puiConfigAddr[3]  = HR2_REG_READ32(DMA_CSR_ADDR(uiChannel, DMA_BLOCK));
    puiConfigAddr[4]  = HR2_REG_READ32(DMA_CSR_ADDR(uiChannel, DMA_SRC_ADR_L));
    puiConfigAddr[5]  = HR2_REG_READ32(DMA_CSR_ADDR(uiChannel, DMA_DST_ADR_L));
    puiConfigAddr[6]  = HR2_REG_READ32(DMA_CSR_ADDR(uiChannel, DMA_SRC_ADR_H));
    puiConfigAddr[7]  = HR2_REG_READ32(DMA_CSR_ADDR(uiChannel, DMA_DST_ADR_H));
    puiConfigAddr[8]  = HR2_REG_READ32(DMA_CSR_ADDR(uiChannel, DMA_ROW_NUM));
    puiConfigAddr[9]  = HR2_REG_READ32(DMA_CSR_ADDR(uiChannel, DMA_COL_NUM));
    puiConfigAddr[10] = HR2_REG_READ32(DMA_CSR_ADDR(uiChannel, DMA_DST_BLOCK));
    puiConfigAddr[11] = HR2_REG_READ32(DMA_CSR_ADDR(uiChannel, DMA_DESC_L));
    puiConfigAddr[12] = HR2_REG_READ32(DMA_CSR_ADDR(uiChannel, DMA_DESC_H));
    puiConfigAddr[13] = HR2_REG_READ32(DMA_CSR_ADDR(uiChannel, DMA_CSR_NEXT));

    return  (OK);
}
/*********************************************************************************************************
** 函数名称: hrGdmaChkParams
** 功能描述: GDMA 参数检查
** 输　入  : uiChannel      通道
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
int  hrGdmaChkParams (unsigned long  ulSrcAddr, unsigned long  ulDstAddr, unsigned int  uiSrcTotalSize, unsigned int uiDstTotalSize)
{
    if ((ulSrcAddr == 0) || (ulDstAddr == 0)) {
    	printf("通用DMA源地址或目的地址为空！\r\n");
        return  (ERROR);
    }

    if ((uiSrcTotalSize < 4) || (uiDstTotalSize < 4)) {
        printf("通用DMA传输数据包大小错误！\r\n");
        return  (ERROR);
    }

    if ((ulDstAddr <= ulSrcAddr) &&
        ((ulDstAddr + uiDstTotalSize) > ulSrcAddr)) {
        printf("通用DMA传输目的地址空间与源地址空间重叠！\r\n");
        return  (ERROR);
    }

    if ((ulDstAddr > ulSrcAddr) &&
       ((ulSrcAddr + uiSrcTotalSize) > ulDstAddr)) {
        printf("通用DMA传输目的地址空间与源地址空间重叠！\r\n");
        return  (ERROR);
    }

    return  (OK);
}
/*********************************************************************************************************
** 函数名称: hrGdmaWaitChannelDone
** 功能描述: 等待 channel 至 ready 状态
** 输　入  : uiChannel      通道
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
int  hrGdmaWaitChannelDone (unsigned int  uiChannel)
{
	unsigned int  uiStatus = 0;
	unsigned int  bTrue    = 1;
	unsigned int  t0       = 0;
	unsigned int  t1       = 0;

    t0 = tickGet();
    while (bTrue) {
        t1 = tickGet();
        if (t1 >= t0) {
            if ((t1 - t0) >= LOOP_TIMER_OUT * sysClkRateGet()) {
                printf("读取通用DMA传输完成状态寄存器超时.\r\n");
                break;
            }
        } else {
            if (t1 >= LOOP_TIMER_OUT * sysClkRateGet()) {
                printf("读取通用DMA传输完成状态寄存器超时.\r\n");
                break;
            }
        }

        uiStatus = HR2_REG_READ32(DMA_CSR_ADDR(uiChannel, DMA_IRQ));
        if ((uiStatus & (0x1 << uiChannel)) == 0) {
            break;
        }
    }

    return  (OK);
}
/*********************************************************************************************************
** 函数名称: hrGdmaGetTransSize
** 功能描述: 获取转置大小
** 输　入  : NONE
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
UINT64  hrGdmaGetTransSize (VOID)
{
    return s_gdmaTotalTransSize;
}
/*********************************************************************************************************
** 函数名称: hrGdmaGetDbgInfo
** 功能描述: GDMA 调试信息打印
** 输　入  : NONE
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
int  hrGdmaGetDbgInfo (void)
{
    UINT j = 0;

    printf("GDMA Debug Info:\n");

    printf("进中断次数: %d\n",   g_pGdmaDbgInfo->intNumIn);
    printf("出中断次数: %d\n",   g_pGdmaDbgInfo->intNumOut);
    printf("DMA启动次数: %d\n",  g_pGdmaDbgInfo->dmaStartNum);
    printf("DMA结束次数: %d\n",  g_pGdmaDbgInfo->dmaFinishNum);
    printf("进API函数次数: %d\n",g_pGdmaDbgInfo->apiInNum);
    printf("出API函数次数: %d\n",g_pGdmaDbgInfo->apiOutNum);

    for (j = 0; j < DBG_GDMA_INDEX_NUM; j++) {
        printf("索引%d源地址: 0x%lx\n",j,  g_pGdmaDbgInfo->hrGdmaParam[j].srcAddr);
        printf("索引%d目的地址: 0x%lx\n",j,g_pGdmaDbgInfo->hrGdmaParam[j].dstAddr);
        printf("索引%d长度: 0x%x\n",j,    g_pGdmaDbgInfo->hrGdmaParam[j].uiLength);
    }

    printf("最新索引值: %d\n",g_pGdmaDbgInfo->newRecord);

    return  (OK);
}
/*********************************************************************************************************
  END
*********************************************************************************************************/
