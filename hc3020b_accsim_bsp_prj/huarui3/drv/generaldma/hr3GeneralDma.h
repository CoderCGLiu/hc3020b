/******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件实现了GDMA驱动模板
 * 修改：
 * 		2022-04-05，tank，建立代码模板
 */

#ifndef _VXB_GDMA_H
#define _VXB_GDMA_H
#include <sys/types.h>
#include "huarui3/h/tool/hr3Macro.h"
/*********************************************************************************************************
  索引数目
*********************************************************************************************************/
#define DBG_GDMA_INDEX_NUM          1
/*********************************************************************************************************
  寄存器基地址
*********************************************************************************************************/
#define HRGDMA_REG_BASE                   0xffffffffbf900000
#define DMA_CSR_ADDR(_channel, addr) \
        (HRGDMA_REG_BASE + _channel * 0x40 + addr)
/*********************************************************************************************************
  寄存器
*********************************************************************************************************/
#define DMA_CSR                    0x00
#define DMA_CSR_R                  0x04
#define DMA_CHUNK                  0x08
#define DMA_TOTAL                  0x0c
#define DMA_BLOCK                  0x10
#define DMA_SRC_ADR_L              0x14
#define DMA_DST_ADR_L              0x18
#define DMA_SRC_ADR_H              0x1c
#define DMA_DST_ADR_H              0x20
#define DMA_ROW_NUM                0x24
#define DMA_COL_NUM                0x28
#define DMA_DST_BLOCK              0x2c
#define DMA_DESC_L                 0x30
#define DMA_DESC_H                 0x34
#define DMA_CSR_NEXT               0x38
//#define DMA_START                  0x40
//#define DMA_IRQ                    0xfc
#define DMA_IRQ                    0x2fc

#define DMA_DONE_INT_L             0x3c
#define DMA_DONE_INT_H             0x7c
#define DMA_STOP_INT_L             0xbc
#define DMA_STOP_INT_H             0xfc
#define DMA_AXI_ERR_INT_L          0x13c
#define DMA_AXI_ERR_INT_H          0x17c

#define HR23_GPDMA
#ifdef HR23_GPDMA
#define GPDMA_CSR_EN                0
#define GPDMA_CSR_INC_DST           1
#define GPDMA_CSR_INC_SRC           2
#define GPDMA_CSR_ADR_0_SRC         3
#define GPDMA_CSR_ADR_1_DST         4
#else
#define GPDMA_CSR_RESERVED_B0       0
#define GPDMA_CSR_RESERVED_B1       1
#define GPDMA_CSR_RESERVED_B2       2
#define GPDMA_CSR_2D_TO_1D          3
#define GPDMA_CSR_1D_TO_2D          4
#endif

#define GPDMA_CSR_REQ               5
#define GPDMA_CSR_READ_REQ_NUM      6
#define GPDMA_CSR_RESERVED_B10_11   10
#define GPDMA_CSR_AW_BURST_SIZE     12
#define GPDMA_CSR_AW_CACHE_TYPE     15
#define GPDMA_CSR_AW_BURST_TYPE     19
#define GPDMA_CSR_MATRIX_REQ        21
#define GPDMA_CSR_MATRIX_POINT      22
#define GPDMA_CSR_RESERVED_B24      24
#define GPDMA_CSR_INT_ERR_EN        25
#define GPDMA_CSR_INT_DONE_EN       26
#define GPDMA_CSR_NEXT_DESC         27
#define GPDMA_CSR_FORCE_DESC        28
#define GPDMA_CSR_RESERVED_B29_30   29
#define GPDMA_CSR_STOP              31

#define GPDMA_CSR_R_RESERVED_B0_11   0
#define GPDMA_CSR_R_AR_BURST_SIZE   12
#define GPDMA_CSR_R_AR_CACHE_TYPE   15
#define GPDMA_CSR_R_AR_BURST_TYPE   19
#define GPDMA_CSR_R_RESERVED_B21_31 21
/*********************************************************************************************************
  DMA 描述符
*********************************************************************************************************/
struct hr_gdma_desc {
    u32 CSR_EN                     : 1;
    u32 CSR_INC_DST                : 1;
    u32 CSR_INC_SRC                : 1;
    u32 CSR_ADR_0_SRC              : 1;
    u32 CSR_ADR_1_DST              : 1;
    u32 CSR_REQ                    : 1;
    u32 CSR_READ_REQ_NUM           : 6;
    u32 CSR_AW_BURST_SIZE          : 3;
    u32 CSR_AW_CACHE_TYPE          : 4;
    u32 CSR_AW_BURST_TYPE          : 2;
    u32 CSR_MATRIX_REQ             : 1;                              /*  [21] matrix req             */
    u32 CSR_MATRIX_POINT           : 2;                              /*  [22~23] matrix element size 2:4B  3:8B */
    u32 CSR_RESERVED_B24           : 1;
    u32 CSR_INT_ERR_EN             : 1;
    u32 CSR_INT_DONE_EN            : 1;
    u32 CSR_NEXT_DESC              : 1;
    u32 CSR_FORCE_DESC             : 1;
    u32 CSR_RESERVED_B29_30        : 2;
    u32 CSR_STOP                   : 1;
};

struct hrGdmaTransDesc
{
    unsigned int sign;                                                          /*  标示该项是否被处理过 1：未处理；0：已处理过 */
    unsigned int transMode;                                                     /*  标示传输类型，0：一维传输；1：二维传输；2：矩阵转置传输*/
    unsigned int channel;
    unsigned int srcAddr;
    unsigned int srcChunk;
    unsigned int srcBlk;
    unsigned int dstAddr;
    unsigned int dstBlk;
    unsigned int rowNum;
    unsigned int colNum;
    unsigned int transSize;
};

typedef struct
{
    unsigned long        srcAddr;                                               /*  源地址                      */
    unsigned long        dstAddr;                                               /*  目的地址                    */
    unsigned int         uiLength;                                              /*  传输数据量                  */
} HRGDMAPARAM;

typedef struct
{
	unsigned int         intNumIn;                                              /*  进入中断次数                */
	unsigned int         intNumOut;                                             /*  出中断次数                  */
	unsigned int         dmaStartNum;                                           /*  DMA 启动次数                */
	unsigned int         dmaFinishNum;                                          /*  DMA 结束次数                */
	unsigned int         apiInNum;                                              /*  进 API 函数次数             */
	unsigned int         apiOutNum;                                             /*  出 API 函数次数             */
	unsigned int         newRecord;                                             /*  指示最新的索引记录，        */
                                                                                /*  取值0~ DBG_CDMA_INDEX_NUM - 1 */
    HRGDMAPARAM  hrGdmaParam[DBG_GDMA_INDEX_NUM];
} HRGDMADBGINFO;
/*********************************************************************************************************
  函数声明
*********************************************************************************************************/
void  hrGdma1DTransConfig(unsigned int  uiChannel, char *  pcSrcAddr, char *  pcDstAddr, unsigned int  uiSize);
void  hrGdmaStart(unsigned int  uiChannel, unsigned int  uiTransMode, unsigned int  uiElementSize);
void  hrGdma2DTransConfig(unsigned int  uiChannel, unsigned int  uiTotal,   unsigned int   uiSrcChunk,
		unsigned int   uiSrcBlk,  char * pcSrcAddr, char *  pcDstAddr, unsigned int  uiDstBlk);
void  hrGdmaMatrixTransConfig(unsigned int  uiChannel, unsigned int  uiTotal, unsigned int  uiChunk,
		unsigned int  uiBlock, char *  pcSrcAddr, char *  pcDstAddr,
		unsigned int  uiRownum, unsigned int  uiColnum,  unsigned int   uiDstblk);
void  hrGdmaSetTransMode(unsigned int  uiTransMode);
int   hrGdmaWaitChannelDone(unsigned int  uiChannel);
void  hrGdmaInstInit(void);
#endif
/*********************************************************************************************************
  END
*********************************************************************************************************/
