/*-------------------------------------------------------------------------+
|
|程序名称：龙芯3A核间中断处理
|描述：主要实现核间中断的使能、屏蔽、发送等功能
|   
+--------------------------------------------------------------------------*/
#include "private/mailbox.h"
#include <reworks/types.h>
#include <bitops.h>
#include <bsp.h>
#include <cpu.h>
#include <ipi.h>
#include "hr3_intc_defs.h"

#include "tool/hr3SysTools.h"
  

#if defined(__HUARUI2__) || (__HUARUI3__)
 void *mailbox_set_regs[] = {                        	/* addr for core_set0 reg,*/
    (void *)(DSP0_IPI_SET), 	/* which is a 32 bit reg*/
    (void *)(DSP1_IPI_SET), 	/*When the bit of core_set0 is 1,*/
    (void *)(DSP2_IPI_SET), 	/*the bit of core_status0 become 1 immediately*/
    (void *)(DSP3_IPI_SET),
    (void *)(DSP4_IPI_SET),
    (void *)(DSP5_IPI_SET),
    (void *)(DSP6_IPI_SET),
    (void *)(DSP7_IPI_SET),
                      
};

 void *mailbox_clear_regs[] = {                      		/* addr for core_clear0 reg,*/
	(void *)(DSP0_IPI_CLEAR), 	/* which is a 32 bit reg*/
	(void *)(DSP1_IPI_CLEAR), 	/*When the bit of core_clear0 is 1,*/
	(void *)(DSP2_IPI_CLEAR), 	/*the bit of core_status0 become 0 immediately*/
	(void *)(DSP3_IPI_CLEAR),  
	(void *)(DSP4_IPI_CLEAR),
	(void *)(DSP5_IPI_CLEAR),
	(void *)(DSP6_IPI_CLEAR),
	(void *)(DSP7_IPI_CLEAR),
};

 void *mailbox_status_regs[] = {                            		/* addr for core_status0 reg*/
	(void *)(CORE_IPI_STATUS(0)),    /* which is a 32 bit reg*/
	(void *)(CORE_IPI_STATUS(1)),    /* the reg is read only*/
	(void *)(CORE_IPI_STATUS(2)),
	(void *)(CORE_IPI_STATUS(3)),
	(void *)(CORE_IPI_STATUS(4)),
	(void *)(CORE_IPI_STATUS(5)),
	(void *)(CORE_IPI_STATUS(6)),
	(void *)(CORE_IPI_STATUS(7)),
};

 void *mailbox_enable_regs[] = {                        	/* addr for core_set0 reg,*/
    (void *)(DSP0_IPI_ENABLE), 	/*which is a 32 bit reg*/
    (void *)(DSP1_IPI_ENABLE), 	/*When the bit of core_set0 is 1,*/
    (void *)(DSP2_IPI_ENABLE), 	/*the bit of core_status0 become 1  immediately */
    (void *)(DSP3_IPI_ENABLE),
    (void *)(DSP4_IPI_ENABLE),
    (void *)(DSP5_IPI_ENABLE),
    (void *)(DSP6_IPI_ENABLE),
    (void *)(DSP7_IPI_ENABLE),
};
#endif

extern int cpu_id_get();
extern u32 mips64_readw(u32 cpu,u32 addr);
extern void  mips64_writew(u32 cpu,u32 addr,u32 val);

#define IPI_MAX 32
struct ipi_data ipi_table[IPI_MAX];


//extern void * mailbox_buf[];
/***************************************************************************
 * 使能指定CPU的核间中断
 *    输入：
 *    cpuid：CPU号
 *    输出：
 *       无
 *    返回：
 *       成功返回OK，否则返回ERROR
 */
int ipi_enable(int cpuid)
{
	if((cpuid < 0)||(cpuid >=MAX_SMP_CPUS))
	{
		printk("invalid cpu id\n\r"); 
		return -1;
	}

	sysWrite32(mailbox_enable_regs[cpuid], 0xffffffff);
//	mips64_writew(cpuid, mailbox_buf(mailbox_en0_regs, cpuid), 0xffffffff);
	
	printk("<**DEBUG**> [%s():_%d_]:: [CPU%d] mailbox_enable_regs:0x%x, val:0x%08x, except:0x%08x\n", \
			__func__, __LINE__,cpuid,mailbox_enable_regs[cpuid], \
			sysRead32(mailbox_enable_regs[cpuid]), 0xffffffff);
	
	return 0;
}
/***************************************************************************
 * 屏蔽指定CPU的核间中断
 *    输入：
 *    cpuid：CPU号
 *    输出：
 *       无
 *    返回：
 *       成功返回OK，否则返回ERROR
 */
int ipi_disable(int cpuid)
{
	if((cpuid < 0)||(cpuid >=MAX_SMP_CPUS))
	{
		printk("invalid cpu id\n\r");
		return -1;
	}

	sysWrite32(mailbox_enable_regs[cpuid], 0x0);
//	mips64_writew(cpuid, mailbox_fixup(mailbox_en0_regs, cpuid), 0x0);
	
	printk("<**DEBUG**> [%s():_%d_]:: [CPU%d] mailbox_enable_regs:0x%x, val:0x%08x, except:0x%08x\n", \
			__func__, __LINE__,cpuid,mailbox_enable_regs[cpuid], \
			sysRead32(mailbox_enable_regs[cpuid]), 0x0);
	
	return 0;
}
/***************************************************************************
 * 发送核间中断给其他CPU
 *    输入：
 *    cpuid：CPU号
 *    输出：
 *       无
 *    返回：
 *       成功返回OK，否则返回ERROR
 */
int ipi_set(int cpuid, int value)
{
	if((cpuid < 0)||(cpuid >=MAX_SMP_CPUS))
	{
		printk("invalid cpu id\n\r");
		return -1;
	}	

	sysWrite32(mailbox_set_regs[cpuid],  1<<value);
//	mips64_writew(cpuid, mailbox_fixup(mailbox_set0_regs, cpuid), 1<<value);
	
//	printk("<**DEBUG**> [%s():_%d_]:: [CPU%d] mailbox_set_regs:0x%x, val:0x%08x, except:0x%08x\n", \
//			__func__, __LINE__,cpuid,mailbox_set_regs[cpuid], \
//			sysRead32(mailbox_set_regs[cpuid]), 1<<value);
	
//	extern void dump_hr3_int_reg(void);
//	dump_hr3_int_reg();/*ldf 20230922 add:: debug*/
	
	return 0;
}

static int sysIpiClr_hr()
{
	unsigned int addrh,addrl;
	unsigned int pMboxClear;
	u32 cpuNum = cpu_id_get();

	pMboxClear = A_IMR_REGISTER(cpuNum&3, MAILBOX_CLR);
	addrh = 0x90000000;
	addrl = pMboxClear;

	return 0;
}

//inline int ffsMsbLL
//    (
//    u64 i            /* value in which to find first set bit */
//    )
//    {
//    register u32 msw;    /* most significant word */
//
//    if ((msw = (u32)(i >> 32)) != 0)
//        return (ffsMsb(msw) + 32);
//    else
//        return (ffsMsb(i & 0xffffffff));
//    }



//int ffsMsbLL
//    (
//    long i            /* value in which to find first set bit */
//    )
//    {
//	register int msw;    /* most significant word */
//
//    if (((int)(i >> 32)) != 0)
//        return (ffs16(msw) + 32);
//    else
//        return (ffs16(i & 0xffffffff));
//    }



/***************************************************************************
 * 核间中断响应函数
 *    输入：
 *     无
 *    输出：
 *       无
 *    返回：
 *       成功返回OK，否则返回ERROR
 */
void ipi_ack(void)
{

	
	u32 cpu_num ;
	struct ipi_data * data;
	cpu_num = cpu_id_get();

	
//	printk("ipi_ack[%d]\n", cpu_num);
//	sysLEDTwinkle(3);
	
	
#if 0
	u64 mailboxInts;
	unsigned int ix;
	/* IPI interrupts ! */
	mailboxInts = MIPS_LW64((u32)IMR_REGISTER(cpu_num, MAILBOX_READ));
	/* handle CPC IPI if set first then just return */
	if((mailboxInts & SMP_CPC_MAILBOX_INT_BIT) != 0LL)
	{
		/* clr interrupt & call ISR , CPC IPI */
		MIPS_SW64((u32)IMR_REGISTER(cpu_num, MAILBOX_CLR),SMP_CPC_MAILBOX_INT_BIT);
//		VXB_INTCTLR_ISR_CALL(pEnt, IPI_ISR_INPUT_PIN_SB1A(IPI_INTR_ID_CPC));			
	}
#ifdef INCLUDE_SM_COMMON
	else if ((mailboxInts & (1<<4)) != 0LL)
	{			
		sysSmInt(4); 		
	}
#endif
	else if((mailboxInts & (1<<3)) != 0LL)
	{
		sysIpiClr_hr();
		//sysIpiClr();
	}
	else if (mailboxInts != 0)
	{
		/* clr interrupt & call ISR , other IPIs*/
		MIPS_SW64((u32)IMR_REGISTER(cpu_num, MAILBOX_CLR),(mailboxInts));
		while ((ix = ffsMsbLL(mailboxInts)) != 0)
		{
//			VXB_INTCTLR_ISR_CALL(pEnt, IPI_ISR_INPUT_PIN_SB1A(ix-1));
			mailboxInts ^= (1LL << (ix - 1));                
		}	        
	}
	
#else

    /*读状态寄存器*/
	int ipiId;
	
	//ww mod 20180808 mod 
#if 0
	int action;
#else
	long action;
#endif
	//ww mod 20180808 mod 
	
//	action = mips64_readw(cpu_num, mailbox_fixup(mailbox_regs0, cpu_num));
	action = sysRead32(mailbox_status_regs[cpu_num]);
//	printk("<**DEBUG**> [%s():_%d_]:: [CPU%d] mailbox_status_regs:0x%x, val:0x%08x\n", \
//			__func__, __LINE__,cpu_num,mailbox_status_regs[cpu_num], action);
	
	if ((action & SMP_CPC_MAILBOX_INT_BIT) != 0LL)
	{
		/* clr interrupt & call ISR , CPC IPI */
		sysWrite32(mailbox_clear_regs[cpu_num], SMP_CPC_MAILBOX_INT_BIT);
//		printk("<**DEBUG**> [%s():_%d_]:: [CPU%d] mailbox_clear_regs:0x%x, val:0x%08x, except:0x%08x\n", \
//				__func__, __LINE__,cpu_num,mailbox_clear_regs[cpu_num], \
//				sysRead32(mailbox_clear_regs[cpu_num]), SMP_CPC_MAILBOX_INT_BIT);
		
		data = &ipi_table[IPI_INTR_ID_CPC];
		if(data->valid) {
			data->handler(data->param);
		}
	}
	else if((action &(1<<3))!=0LL)
	{  
		sysIpiClr_hr();
	}
	else if (action != 0)
	{
		/*清核间中断*/
		//	mips64_writew(cpu_num, mailbox_fixup(mailbox_clear0_regs, cpu_num),action);
		sysWrite32(mailbox_clear_regs[cpu_num], action);
//		printk("<**DEBUG**> [%s():_%d_]:: [CPU%d] mailbox_clear_regs:0x%x, val:0x%08x, except:0x%08x\n", \
//				__func__, __LINE__,cpu_num,mailbox_clear_regs[cpu_num], \
//				sysRead32(mailbox_clear_regs[cpu_num]), action);
#if 1
		
		//运行应用OK的处理方式
		while(action != 0) {
			ffs16(action,ipiId);			
			data = &ipi_table[ipiId];
			if(data->valid) {
				data->handler(data->param);
			}
			action &= ~(1<<ipiId);

		}	
		
//		while(action != 0) {
//			ffs16(action,ipiId);			
//			data = &ipi_table[ipiId-1];
//			if(data->valid) {
//				data->handler(data->param);
//			}
//			action &= ~(1<<(ipiId-1));
//
//		}
#else
		while ((ipiId = ffsMsbLL(action)) != 0)
		{
//			VXB_INTCTLR_ISR_CALL(pEnt, IPI_ISR_INPUT_PIN_SB1A(ix-1));
//			mailboxInts ^= (1LL << (ix - 1));         
			data = &ipi_table[ipiId-1];
			if(data->valid) {
				data->handler(data->param);
			}
			action &= ~(1<<(ipiId-1));
		}
		
#endif
		//ww mod 20180808 mod
		
	}
#endif
}

OS_STATUS ipi_install_handler(int ipiId, IPI_HANDLER handler, void* arg)
{
	
//	printk("ipi_install_handler[%d]\n", cpu_id_get());
//	if (cpu_id_get() == 1)
//		sysLEDSetState(1);
	struct ipi_data * data = &ipi_table[ipiId];
	if(ipiId<0 || ipiId> IPI_MAX){
		return OS_ERROR;
	}
	data->handler = handler;
	data->param = arg;
	data->valid = 1;
	
	return OS_OK;
}
