/*******************************************************************************
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了外部中断控制器的操作。
 * 修改：
 * 		 2012-04-10，建立
 */
//#define DEBUG
#include <string.h>
#include <irq.h>
#include <bsp.h>
//#include "hr3_intc_defs.h"
#include <gs464_cpu.h>
//#include "external_int.h"
#include "hr3_intc.h"
#include <irq.h>
#include <bsp_int.h>


#include <spinLocklib.h>


/**
 * 由于现在龙芯3A平台中有些中断引脚CSP中没有使用，为了
 * 方便用户使用这些中断引脚，特设定钩子函数提供给用户定义
 */
 
BSP_INT_HOOK bsp_int_hook_ip4 = NULL;
BSP_INT_HOOK bsp_int_hook_ip5 = NULL;
BSP_INT_HOOK_ENABLE bsp_int_ip3_enable = NULL;
BSP_INT_HOOK_DISABLE bsp_int_ip3_disable = NULL;
INT_INUM_FUNCPTR bsp_int_hook_inum_ip3 = NULL;
INT_INUM_FUNCPTR bsp_int_hook_inum_ip4 = NULL;
INT_INUM_FUNCPTR bsp_int_hook_inum_ip5 = NULL;

/**
 * 函数声明
 */
extern void mips64_writeb(u32 cpu_num, u32 addr,char val);
extern char mips64_readb(u32 cpu_num, u32 addr);
extern void mips64_writew(u32 cpu_num, u32 addr ,u32 val);
extern int mips64_readw(u32 cpu_num, u32 addr);
extern u32 mips64_read_HT_reg(u32 cpu_num, u32 addr);
extern void mips64_write_HT_reg(u32 cpu_num, u32 addr, u32 val);

/******************************************************************************
 * 
 * 外部中断控制器中断使能接口
 * 		
 * 		本接口用于使能指定的中断请求号irq对应的硬件中断。在使能中断成功后，参数irq
 * 对应的中断引脚处于中断可用状态，能够响应连接到该中断引脚上的外部设备发出的中断。
 * 
 * 输入：
 * 		irq：中断请求
 * 输出:
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 * 备注：
 * 		本程序在CSP中断框架中调用，为必须实现的接口。
 */
static int bsp_int_enable(u32 irq)
{
/* dual3a */
//	if( irq > CLOCK_IRQ && irq <= I8259_IRQ_END )
//	if( irq > I8259_IRQ_START && irq <= I8259_IRQ_END )
//	{
////		return extern_int_enable(irq);
////		int_enable_i8259(irq);
//		return 0;
//	}
//	else if (irq == (int)INT_UART0)
//	{
//		mips64_writew(0, INTENSET_REG,(1<<((int)INT_UART0))); /*打开串口*/
//		return 0;
//	}
	
	return hr3_int_enable(irq);;
}


/******************************************************************************
 * 
 * 中断屏蔽函数
 * 		
 * 		本接口用于屏蔽指定的中断请求号irq对应的硬件中断。在屏蔽中断成功后，参数irq
 * 对应的中断引脚处于中断不可用状态，连接到该中断引脚上的外部设备发出的中断不再被相应。
 * 
 * 输入：
 * 		irq：中断请求
 * 输出:
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 * 备注：
 * 		本程序在CSP中断框架中调用，为必须实现的接口。
 */
static int bsp_int_disable(u32 irq)
{
/* dual3a */
//	if( irq > CLOCK_IRQ && irq <= I8259_IRQ_END )
//	if( irq > I8259_IRQ_START && irq <= I8259_IRQ_END )
//	{
////		int_disable_i8259(irq);
//		return 0 ;
//	}
//	else if (irq == (int)INT_UART0)
//	{
//		mips64_writew(0, INTENCLR_REG,(1 << LPC_INT));
//		return 0;
//	}
//	
	
	return hr3_int_disable(irq);
}


/*******************************************************************************
 * 
 * 中断响应操作
 * 
 * 		本接口对外部中断进行响应，如果外部中断控制器需要进行中断响应的话。
 * 输入：
 * 		interrupt_pending：需要响应的中断号。
 * 输出：
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 * 备注：
 * 		本接口需要根据具体的硬件平台和外部中断控制器进行实现
 */
static int bsp_int_ack(int irq)
{
/* dual3a */
//	if( irq > CLOCK_IRQ && irq <= I8259_IRQ_END )
//	if( irq > I8259_IRQ_START && irq <= I8259_IRQ_END )
//	{
//		int_ack_i8259(irq);
//		return 0;
//	}
//	else 
//	if (irq == (int)INT_UART0)
//	{
//		mips64_readw(0, INTISR_REG);
//		return 0;
//	}

	return hr3_int_ack(irq);
}

extern spinlockIsr_t * pintcSpinLock;
/******************************************************************************
 * 
 * 中断控制器硬件初始化
 * 		
 * 		本接口用于负责中断控制器初始化，本接口负责调用内部中断控制器初始化接口（x86
 * 平台不存在内部中断控制器）和外部中断控制器初始化接口。最后返回内部和外部中断
 控制器支持的中断数量。
 * 		例如在x86平台通常使用两片8259级联作为中断控制器，那么本接口对两片8259中断
 控制器进行初始化后，应返回16（）。
 * 
 * 输入：
 * 		无
 * 输出:
 * 		无
 * 返回：
 * 		成功返回硬件平台支持的中断数量；失败返回-1
 */
static int bsp_int_init(void)
{
#ifdef DEBUG
	printk("begin of bsp_int_init\n");
#endif
////	extern_int_init();
////	internal_int_init();
//	;
//#ifdef DEBUG
//	printk("end of bsp_int_init\n");
//#endif
	printk("end of bsp_int_init spinlock\n");
	pintcSpinLock= (spinlockIsr_t *)t_spinlockIsrInit();
	if(pintcSpinLock == NULL)
	{
		printstr("用户空间发送缓存初始化时spinLock创建失败！\n");
		return -1;
	}
	printk("end of bsp_int_init spinlock 222\n");
	return hr3_int_init();
}


/*******************************************************************************
 * 
 * 获取中断号
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		中断号
 */
static int bsp_int_inum(u32 interrupt_pending, u32 irq_array[], u32 irq_num)
{
//	if( interrupt_pending & CAUSE_IP3 ) /*南桥*/
//		return i8259_int_inum();
//	else 
//		if (interrupt_pending & CAUSE_IP2) /*串口*/
//		return ((int)INT_UART0);
//	
	return hr3_int_inum(interrupt_pending,irq_array, irq_num);
}


/*******************************************************************************
 * 
 * BSP中断控制器模块初始化接口
 * 
 * 		本接口用于向CSP注册中断操作接口。
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 */
int bsp_int_module_init(void)
{    
	/* 定义临时变量注册 */
    struct int_operations ops;
    
    /* 外部中断控制器操作注册  */
    memset(&ops, 0, sizeof(struct int_operations));
    ops.enable 		= bsp_int_enable;
    ops.disable 	= bsp_int_disable;
    ops.ack 			= bsp_int_ack;
    ops.inum			= bsp_int_inum;
    ops.init			= bsp_int_init;
    if (0 != int_operations_register(&ops))
    {
    	return -1;
    }
	
    return 0;
}
/*******************************************************************************
 * 
 * 使能CPU核中断
 * 
 * 		本接口用于 使能CPU核中断，设置各核的status寄存器。
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 */
void enable_core()
{
	u32 val = get_c0_sr();
//	printk("cpu1: before status[0x%x]\n\r",val);
	val |=0xff01;
//	val |=SR_KX;
	val |=SR_UX;
	set_c0_sr(val);
//	printk("cpu1:status[0x%x]\n\r",get_c0_sr());
}
/*******************************************************************************
 * 
 * 响应IP5的中断，该接口目前为空
 * 
 * 		。
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		无
 */
/*extern struct irq_data rtos_irqs[];
void int_ack_ip5()
{
	if(NULL != bsp_int_hook_ip5)
	{
		bsp_int_hook_ip5();
	}
	return;
}

void int_ack_ip4()
{

	if(NULL != bsp_int_hook_ip4)
	{
		bsp_int_hook_ip4();
	}
	return;
}*/

/*判断中断号是否有效，有效返回1，否则返回*/
int is_irq_valid(int irq)
{
	if(irq >= 0 && irq <= IRQ_NUM)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
