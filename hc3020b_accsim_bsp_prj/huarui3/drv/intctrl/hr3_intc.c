/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件主要实现华睿2号中断控制器的初始化、使能、屏蔽等操作。 
 * 
 * 修改：
 * 		 2018-05-22，符凯，创建。 
 */
#include "hr3_intc_defs.h"
#include "hr3_intc.h"
#include "tool/hr3Macro.h"
#include <gs464_cpu.h>
//#include "hr2_cpu.h"
#include <reworks/types.h>
#include <assert.h>
#include "tool/hr3SysTools.h"
#include <ipi.h>
#include <spinLocklib.h> 
#include <irq.h>
/* added by syj begin */


#if defined(__HUARUI2__)
//ww mod 20190215 add
#ifndef __BOOTIMAGE__
#define ROUTE_CORE_NUM 	4 /* 有4个核可用来处理中断 */
#else
//bootimage模式下 只需要路由一个核即可
#define ROUTE_CORE_NUM 	1 /* 有4个核可用来处理中断 */
#endif
//ww mod 20190215 add
#elif defined(__HUARUI3__)
//FuKai,2020-9-4：暂时设置为1
#define ROUTE_CORE_NUM  MAX_SMP_CPUS
#endif

#define IO_PIN_NUM 		4 /* 每个核有4个用来处理IO中断的引脚 */
#define MAX_REUSABLE_INTSOURCE_NUM 	16 /* 最多有16个中断共用同一个引脚 */

typedef struct _HR3_INT_CFG
{
	HR3_INT irqNo;
	boolean isCfg;
	boolean isEnable;
#ifdef _HC3020B_ /*ldf 20230921 add:: for hc3020b*/
	int preemption_pri; /*中断抢占优先级*/
	int sub_pri; /*中断响应优先级*/
#endif
}HR3_INT_CFG;

#ifndef _HC3020B_
static const char* const g_hr3_int_name[HR3_INT_CNT] =
{
	 	"UART0 Interrupt",			/*(0)*/
	 	"UART1 Interrupt",			/*(1)*/
	 	"INT_I2C0 Interrupt",			/*(2)*/
	 	"INT_I2C1 Interrupt",	    	/*(3)*/
	 	"INT_SPI0 Interrupt",	    	/*(4)*/
	 	"INT_SPI1 Interrupt",	    	/*(5)*/
	 	"INT_CAN0 Interrupt",	    	/*(6)*/
	 	"INT_CAN1 Interrupt",	    	/*(7)*/
	 	"INT_RTC Interrupt",    		/*(8)*/
	 	"GPIO TMP Interrupt MS0",	/*(9)*/
	 	"GPIO TMP Interrupt MS1",	/*(10)*/
	 	"GPIO TMP Interrupt MS2",	/*(11)*/
	 	"GPIO TMP Interrupt MS3",	/*(12)*/
	 	"GPIO PWM Interrupt MS0",	   	/*(13)*/
	 	"GPIO PWM Interrupt MS1",		/*(14)*/
	 	"GPIO PWM Interrupt MS2",		/*(15)*/
	 	"GPIO PWM Interrupt MS3",	    /*(16)*/
	 	"INT_GPIO_MS0",	    /*(17)*/
	 	"INT_GPIO_MS1",	    /*(18)*/
	 	"INT_GPIO_MS2",	    /*(19)*/
	 	"INT_GPIO_MS3",	    /*(20)*/
	 	"INT_GPIO_MS4",	    /*(21)*/
	 	"INT_GPIO_MS5",	    /*(22)*/
	 	"INT_GPIO_MS6",	    /*(23)*/
	 	"INT_GPIO_MS7",	    	/*(24)*/
	 	"INT_GPIO_MS8",	    	/*(25)*/
	 	"INT_GPIO_MS9",	    /*(26)*/
	 	"INT_GPIO_MS10",		/*(27)*/
	 	"INT_GPIO_MS11",	    	/*(28)*/
	 	"INT_GPIO_MS12",	    	/*(29)*/
	 	"INT_GPIO_MS13",	    	/*(30)*/
	 	"INT_GPIO_MS14",	    	/*(31)*/
	 	"INT_GPIO_MS15",	        /*(32)*/
	 	"INT_GPIO_MS16",	   		/*(33)*/
	 	"INT_GPIO_MS17",	    	/*(34)*/
	 	"INT_GPIO_MS18",	    	/*(35)*/
	 	"INT_GPIO_MS19",	    	/*(36)*/
	 	"INT_GPIO_MS20",	    	/*(37)*/
	 	"INT_GPIO_MS21",	    	/*(38)*/
	 	"INT_GPIO_MS22",	    	/*(39)*/
	 	"INT_GPIO_MS23",	    /*(40)*/
	 	"INT_WDOG",	    /*(41)*/
	 	"INT_TIMER0",	/*(42)*/
	 	"INT_TIMER1",	    	/*(43)*/
		"INT_DSP0",    		/*(44)*/
	 	"INT_DSP1",	    			/*(45)*/
	 	"INT_DSP2",	    		/*(46)*/
	 	"INT_DSP3",	  			/*(47)*/
	 	"INT_DSP4",    				/*(48)*/
	 	"INT_DSP5",   			/*(49)*/
	 	"INT_DSP6",    				/*(50)*/
	 	"INT_DSP7",    				/*(51)*/
	 	"O_PVT0_INT0", 					/*(52)*/
	 	"O_PVT0_INT1", 					/*(53)*/
	 	"O_PVT0_INT2", 					/*(54)*/
	 	"O_PVT0_INT3", 					/*(55)*/
	 	"O_PVT0_INT4",   			/*(56)*/
	 	"O_PVT0_INT5",   			/*(57)*/
	 	"O_PVT0_INT6",   			/*(58)*/
	 	"O_PVT0_INT7",   			/*(59)*/
	 	"INT_IPC",   			/*(60)*/
	 	"INT_SERIRQ",   			/*(61)*/
	 	"INT_QSPI",   			/*(62)*/
	 	"INT_NAND",   			/*(63)*/
		"INT_SD0",   			/*(64)*/
	 	"INT_SD1",   			/*(65)*/
	 	"INT_PRAB0_PHY",	    			/*(66)*/
	 	"INT_RAB0",	    			/*(67)*/
	 	"INT_PRAB0_PCIEX1",	    			/*(68)*/
	 	"INT_PRAB0_PCIEX4",	    			/*(69)*/
	 	"INT_PRAB1_PHY",	    			/*(70)*/
	 	"INT_RAB1",	    			/*(71)*/
	 	"INT_PRAB1_PCIEX1",	    			/*(72)*/
	 	"INT_PRAB1_PCIEX4",	    			/*(73)*/
	 	"INT_LINK0",	    			/*(74)*/
	 	"INT_LINK1",	    			/*(75)*/
	 	"INT_LINK2",	    			/*(76)*/
	 	"INT_LINK3",	    			/*(77)*/
	 	"INT_PCIE_PHY",	    			/*(78)*/
	 	"INT_GMAC0",			/*(79)*/
	 	"INT_GMAC1",		/*(80)*/
	 	"INT_GMAC2", 			/*(81)*/		/*FuKai",2020-9-9 以时钟中断为界，后面的都是片内中断*/
	 	"INT_GMAC3",			/*(82)*/
	 	"INT_MSIO_LINK0",			/*(83)*/
	 	"INT_MSIO_LINK1",			/*(84)*/
	 	"INT_MSIO_PHY",			/*(85)*/
	 	"INT_RASP0_ERR_AI",			/*(86)*/
	 	"INT_RASP0_DONE_AI",			/*(87)*/
	 	"INT_RASP0_ERR_DSP",			/*(88)*/
	 	"INT_RASP0_DONE_DSP",			/*(89)*/
	 	"INT_RASP1_ERR_AI",			/*(90)*/
	 	"INT_RASP1_DONE_AI",			/*(91)*/
	 	"INT_RASP1_ERR_DSP",			/*(92)*/
	 	"INT_RASP1_DONE_DSP",			/*(93)*/
	 	"INT_RASP2_ERR_AI",			/*(94)*/
	 	"INT_RASP2_DONE_AI",			/*(95)*/
	 	"INT_RASP2_ERR_DSP",			/*(96)*/
	 	"INT_RASP2_DONE_DSP",			/*(97)*/
	 	"INT_RASP3_ERR_AI",			/*(98)*/
	 	"INT_RASP3_DONE_AI",			/*(99)*/
	 	"INT_RASP3_ERR_DSP",			/*(100)*/
	 	"INT_RASP3_DONE_DSP",			/*(101)*/
	 	"INT_GPDMA",			/*(102)*/
	 	"INT_CONTROLLER0",			/*(103)*/
	 	"INT_CONTROLLER1",			/*(104)*/
	 	"INT_UNCORRECTABLE_IRQ",			/*(105)*/
	 	"INT_CORRECTABLE_IRQ",			/*(106)*/
	 	"INT_SRAM_ECCERR0_INTSET_I",			/*(107)*/
	 	"INT_SRAM_ECCERR1_INTSET_I",			/*(108)*/
	 	"INT_RESV0",			/*(109)*/
	 	"INT_RESV1",			/*(110)*/
	 	"INT_RESV2",			/*(111)*/
	 	"INT_RESV3",			/*(112)*/
	 	"INT_RESV4",			/*(113)*/
	 	"INT_RESV5",			/*(114)*/
	 	"INT_RESV6",			/*(115)*/
	 	"INT_RESV7",			/*(116)*/
	 	"INT_RESV8",			/*(117)*/
	 	"INT_RESV9",			/*(118)*/
	 	"INT_RESV10",			/*(119)*/
	 	"INT_RESV11",			/*(120)*/
	 	"INT_RESV12",			/*(121)*/
	 	"INT_RESV13",			/*(122)*/
	 	"INT_RESV14",			/*(123)*/
	 	"INT_RESV15",			/*(124)*/
	 	"INT_RESV16",			/*(125)*/
	 	"INT_RESV17",			/*(126)*/
	 	"INT_RESV18"			/*(127)*/
	 	"INT_CLOCK",			/*(128)*/
	 	"INT_IPI"				/*(129)*/
};
#else /*ldf 20230920 modify:: for 3020b*/
static const char* const g_hr3_int_name[HR3_INT_CNT] =
{
		"INT_UART0",					/*(0)*/
	 	"INT_UART1",					/*(1)*/
	 	"INT_UART2",					/*(2)*/
	 	"INT_UART3",					/*(3)*/
	 	"INT_UART4",					/*(4)*/
	 	"INT_UART5",					/*(5)*/
	 	"INT_UART6",					/*(6)*/
	 	"INT_UART7",					/*(7)*/
	 	"INT_I2C0",					/*(8)*/
	 	"INT_I2C1",	    			/*(9)*/
	 	"INT_SPI0",	    			/*(10)*/
	 	"INT_SPI1",	    			/*(11)*/
	 	"INT_CAN0",	    			/*(12)*/
	 	"INT_CAN1",	    			/*(13)*/
	 	"INT_RTC",    				/*(14)*/
	 	"INT_GPIO_TMP_MS0",			/*(15)*/
	 	"INT_GPIO_TMP_MS1",	   		/*(16)*/
	 	"INT_GPIO_TMP_MS2",	   		/*(17)*/
	 	"INT_GPIO_TMP_MS3",	   		/*(18)*/
	 	"INT_GPIO_PWM_MS0",	   		/*(19)*/
	 	"INT_GPIO_PWM_MS1",	    	/*(20)*/
	 	"INT_GPIO_PWM_MS2",			/*(21)*/
	 	"INT_GPIO_PWM_MS3",	    	/*(22)*/
	 	"INT_GPIO_MS0",	    		/*(23)*/
	 	"INT_GPIO_MS1",	    		/*(24)*/
	 	"INT_GPIO_MS2",	    		/*(25)*/
	 	"INT_GPIO_MS3",	    		/*(26)*/
	 	"INT_GPIO_MS4",	    		/*(27)*/
	 	"INT_GPIO_MS5",	    		/*(28)*/
	 	"INT_GPIO_MS6",	    		/*(29)*/
	 	"INT_GPIO_MS7",	    		/*(30)*/
	 	"INT_GPIO_MS8",	    		/*(31)*/
	 	"INT_GPIO_MS9",	    		/*(32)*/
	 	"INT_GPIO_MS10",				/*(33)*/
	 	"INT_GPIO_MS11",	    		/*(34)*/
	 	"INT_GPIO_MS12",	    		/*(35)*/
	 	"INT_GPIO_MS13",	    		/*(36)*/
	 	"INT_GPIO_MS14",	    		/*(37)*/
	 	"INT_GPIO_MS15",	    		/*(38)*/
	 	"INT_GPIO_MS16",	    		/*(39)*/
	 	"INT_GPIO_MS17",	    		/*(40)*/
	 	"INT_GPIO_MS18",	    		/*(41)*/
	 	"INT_GPIO_MS19",	    		/*(42)*/
	 	"INT_GPIO_MS20",	    		/*(43)*/
	 	"INT_GPIO_MS21",	    		/*(44)*/
	 	"INT_GPIO_MS22",	    		/*(45)*/
	 	"INT_GPIO_MS23",	    		/*(46)*/
	 	"INT_WDOG0",	    			/*(47)*/
	 	"INT_WDOG1",	    			/*(48)*/
	 	"INT_WDOG2",	    			/*(49)*/
	 	"INT_WDOG3",	    			/*(50)*/
	 	"INT_TIMER0",	    			/*(51)*/
	 	"INT_TIMER1",	    			/*(52)*/
	 	"INT_TIMER2",	    			/*(53)*/
		"INT_TIMER3",	    			/*(54)*/
		"INT_TIMER4",	    			/*(55)*/
		"INT_TIMER5",	    			/*(56)*/
		"INT_TIMER6",	    			/*(57)*/
		"INT_TIMER7",	    			/*(58)*/
	 	"INT_DSP0",	    			/*(59)*/
	 	"INT_DSP1",	    			/*(60)*/
	 	"O_PVT0_INT0", 				/*(61)*/
	 	"O_PVT0_INT1", 				/*(62)*/
	 	"INT_IPC",   					/*(63)*/
	 	"INT_SERIRQ",   				/*(64)*/
	 	"INT_QSPI",   				/*(65)*/
		"INT_SD0",   					/*(66)*/
	 	"INT_SD1",   					/*(67)*/
	 	"INT_LBC",   					/*(68)*/
	 	"INT_PRAB0_PHY",	    		/*(69)*/
	 	"INT_RAB0",	    			/*(70)*/
	 	"INT_PRAB0_PCIEX1",	    	/*(71)*/
	 	"INT_PRAB0_PCIEX4",	    	/*(72)*/
	 	"INT_PRAB1_PHY",	    		/*(73)*/
	 	"INT_RAB1",	    			/*(74)*/
	 	"INT_PRAB1_PCIEX1",	    	/*(75)*/
	 	"INT_PRAB1_PCIEX4",	    	/*(76)*/
	 	"INT_GMAC0",					/*(77)*/
	 	"INT_GMAC1",					/*(78)*/
	 	"INT_RASP_ERR",				/*(79)*/
	 	"INT_RASP_DON",				/*(80)*/
	 	"INT_GPDMA",					/*(81)*/
	 	"INT_CONTROLLER",				/*(82)*/
	 	"INT_UNCORRECTABLE_IRQ",		/*(83)*/
	 	"INT_CORRECTABLE_IRQ",		/*(84)*/
	 	"INT_AICORE_DONE",			/*(85)*/
	 	"INT_AICORE_ERR",				/*(86)*/
	 	"INT_SRAM_ECCERR_INTSET_I",	/*(87)*/
	 	
	 	"INT_RESV0",			/*(88)*/
	 	"INT_RESV1",			/*(89)*/
	 	"INT_RESV2",			/*(90)*/
	 	"INT_RESV3",			/*(91)*/
	 	"INT_RESV4",			/*(92)*/
	 	"INT_RESV5",			/*(93)*/
	 	"INT_RESV6",			/*(94)*/
	 	"INT_RESV7",			/*(95)*/
	 	"INT_RESV8",			/*(96)*/
	 	"INT_RESV9",			/*(97)*/
	 	"INT_RESV10",			/*(98)*/
	 	"INT_RESV11",			/*(99)*/
	 	"INT_RESV12",			/*(100)*/
	 	"INT_RESV13",			/*(101)*/
	 	"INT_RESV14",			/*(102)*/
	 	"INT_RESV15",			/*(103)*/
	 	"INT_RESV16",			/*(104)*/
	 	"INT_RESV17",			/*(105)*/
	 	"INT_RESV18",			/*(106)*/
	  	"INT_RESV19",			/*(107)*/
	  	"INT_RESV20",			/*(108)*/
	  	"INT_RESV21",			/*(109)*/
	  	"INT_RESV22",			/*(110)*/
	  	"INT_RESV23",			/*(111)*/
	  	"INT_RESV24",			/*(112)*/
	  	"INT_RESV25",			/*(113)*/
	  	"INT_RESV26",			/*(114)*/
	  	"INT_RESV27",			/*(115)*/
	  	"INT_RESV28",			/*(116)*/
	  	"INT_RESV29",			/*(117)*/
		"INT_RESV30",			/*(118)*/
	  	"INT_RESV31",			/*(119)*/
	  	"INT_RESV32",			/*(120)*/
	  	"INT_RESV33",			/*(121)*/
	  	"INT_RESV34",			/*(122)*/
	  	"INT_RESV35",			/*(123)*/
	  	"INT_RESV36",			/*(124)*/
	  	"INT_RESV37",			/*(125)*/	
	  	
	 	"INT_IPI0", 			/*(126)*/
	 	"INT_IPI1",       		/*(127)*/
	 	
	 	"INT_CLOCK" 			/*(128)*/
	 	"INT_IPI"				/*(129)*/
};
#endif

#if 0 /*not use*/
static const char* const g_hr3_int_name_nouse[HR3_INT_CNT] =
{
	 	"UART0 Interrupt",			/*(0)*/
	 	"UART1 Interrupt",			/*(1)*/
	 	"UART1 Interrupt",			/*(2)*/
	 	"CAN0 Interrupt",	    	/*(3)*/
	 	"CAN1 Interrupt",	    	/*(4)*/
	 	"PS2 0 Interrupt",	    	/*(5)*/
	 	"PS2 1 Interrupt",	    	/*(6)*/
	 	"SPI 0 Interrupt",	    	/*(7)*/
	 	"SPI 1 Interrupt",    		/*(8)*/
	 	"GPIO PWM Interrupt MS0",	/*(9)*/
	 	"GPIO PWM Interrupt MS1",	/*(10)*/
	 	"GPIO PWM Interrupt MS2",	/*(11)*/
	 	"GPIO PWM Interrupt MS3",	/*(12)*/
	 	"GPIO Interrupt MS0",	   	/*(13)*/
	 	"GPIO Interrupt MS1",		/*(14)*/
	 	"GPIO Interrupt MS2",		/*(15)*/
	 	"GPIO Interrupt MS3",	    /*(16)*/
	 	"GPIO Interrupt MS4",	    /*(17)*/
	 	"GPIO Interrupt MS5",	    /*(18)*/
	 	"GPIO Interrupt MS6",	    /*(19)*/
	 	"GPIO Interrupt MS7",	    /*(20)*/
	 	"GPIO Interrupt MS8",	    /*(21)*/
	 	"GPIO Interrupt MS9",	    /*(22)*/
	 	"Watchdog Interrupt",	    /*(23)*/
	 	"Timer0 Interrupt",	    	/*(24)*/
	 	"Timer1 Interrupt",	    	/*(25)*/
	 	"GPIO Interrupt MS10",	    /*(26)*/
	 	"GPIO Interrupt MS11",		/*(27)*/
	 	"DSP0 Interrupt",	    	/*(28)*/
	 	"DSP1 Interrupt",	    	/*(29)*/
	 	"DSP2 Interrupt",	    	/*(30)*/
	 	"DSP3 Interrupt",	    	/*(31)*/
	 	"TSSC Interrupt",	        /*(32)*/
	 	"RTC Interrupt",	   		/*(33)*/
	 	"I2C0 M Interrupt",	    	/*(34)*/
	 	"SRIO-0 Interrupt",	    	/*(35)*/
	 	"SRIO-1 Interrupt",	    	/*(36)*/
	 	"NAND Interrupt",	    	/*(37)*/
	 	"IPC Interrupt",	    	/*(38)*/
	 	"SERIRQ Interrupt",	    	/*(39)*/
	 	"GMAC0 Interrupt",	    /*(40)*/
	 	"GMAC1 Interrupt",	    /*(41)*/
	 	"GMAC2 Interrupt",	/*(42)*/
	 	"GMAC3 Interrupt",	    	/*(43)*/
	 	"Reserved Interrupt",	    /*(44)*/
	 	"PCIE0 Interrupt",	    	/*(45)*/
	 	"Reserved Interrupt",	    /*(46)*/
	 	"Reserved Interrupt",	    /*(47)*/
	 	"PHY0 Interrupt",    		/*(48)*/
	 	"Reserved Interrupt",   	/*(49)*/
	 	"PHY2 Interrupt",    		/*(50)*/
	 	"Reserved Interrupt",    		/*(51)*/
	 	"Reserved Interrupt", 			/*(52)*/
	 	"Reserved Interrupt", 			/*(53)*/
	 	"Reserved Interrupt", 			/*(54)*/
	 	"DDR Interrupt", 			/*(55)*/
	 	"Reserved Interrupt",	    /*(56)*/
	 	"Reserved Interrupt",	    /*(57)*/
	 	"Reserved Interrupt",	    /*(58)*/
	 	"Reserved Interrupt",	    /*(59)*/
	 	"Reserved Interrupt",	    /*(60)*/
	 	"Reserved Interrupt",	    /*(61)*/
	 	"Reserved Interrupt",	    /*(62)*/
	 	"Reserved Interrupt",	    /*(63)*/
	 	"Clock Interrupt",			/*(64)*/
 	    "Inter-processor Interrupt"	/*(65)*/
	 	"UART0 Interrupt",			/*(66)*/
		"UART1 Interrupt",			/*(67)*/
		"UART1 Interrupt",			/*(68)*/
		"CAN0 Interrupt",	    	/*(69)*/
		"CAN1 Interrupt",	    	/*(70)*/
		"PS2 0 Interrupt",	    	/*(71)*/
		"PS2 1 Interrupt",	    	/*(72)*/
		"SPI 0 Interrupt",	    	/*(73)*/
		"SPI 1 Interrupt",    		/*(74)*/
		"GPIO PWM Interrupt MS0",	/*(75)*/
		"GPIO PWM Interrupt MS1",	/*(76)*/
		"GPIO PWM Interrupt MS2",	/*(77)*/
		"GPIO PWM Interrupt MS3",	/*(78)*/
		"GPIO Interrupt MS0",	   	/*(79)*/
		"GPIO Interrupt MS1",		/*(80)*/
		"GPIO Interrupt MS2",		/*(81)*/
		"GPIO Interrupt MS3",	    /*(82)*/
		"GPIO Interrupt MS4",	    /*(83)*/
		"GPIO Interrupt MS5",	    /*(84)*/
		"GPIO Interrupt MS6",	    /*(85)*/
		"GPIO Interrupt MS7",	    /*(86)*/
		"GPIO Interrupt MS8",	    /*(87)*/
		"GPIO Interrupt MS9",	    /*(88)*/
		"Watchdog Interrupt",	    /*(89)*/
		"Timer0 Interrupt",	    	/*(90)*/
		"Timer1 Interrupt",	    	/*(91)*/
		"GPIO Interrupt MS10",	    /*(92)*/
		"GPIO Interrupt MS11",		/*(93)*/
		"DSP0 Interrupt",	    	/*(94)*/
		"DSP1 Interrupt",	    	/*(95)*/
		"DSP2 Interrupt",	    	/*(96)*/
		"DSP3 Interrupt",	    	/*(97)*/
		"TSSC Interrupt",	        /*(98)*/
		"RTC Interrupt",	   		/*(99)*/
		"I2C0 M Interrupt",	    	/*(100)*/
		"SRIO-0 Interrupt",	    	/*(101)*/
		"NAND Interrupt",	    	/*(102)*/
		"GPDMA Interrupt",	    	/*(103)*/
		"IPC Interrupt",	    	/*(104)*/
		"SERIRQ Interrupt",	    	/*(105)*/
		"GMAC0 Interrupt",	    /*(106)*/
		"GMAC1 Interrupt",	    /*(107)*/
		"GMAC2 Interrupt",	/*(108)*/
		"GMAC3 Interrupt",	    	/*(43)*/
		"Reserved Interrupt",	    /*(44)*/
		"PCIE0 Interrupt",	    	/*(45)*/
		"Reserved Interrupt",	    /*(46)*/
		"Reserved Interrupt",	    /*(47)*/
		"PHY0 Interrupt",    		/*(48)*/
		"Reserved Interrupt",   	/*(49)*/
		"PHY2 Interrupt",    		/*(50)*/
		"Reserved Interrupt",    		/*(51)*/
		"Reserved Interrupt", 			/*(52)*/
		"Reserved Interrupt", 			/*(53)*/
		"Reserved Interrupt", 			/*(54)*/
		"DDR Interrupt", 			/*(55)*/
		"Reserved Interrupt",	    /*(56)*/
		"Reserved Interrupt",	    /*(57)*/
		"Reserved Interrupt",	    /*(58)*/
		"Reserved Interrupt",	    /*(59)*/
		"Reserved Interrupt",	    /*(60)*/
		"Reserved Interrupt",	    /*(61)*/
		"Reserved Interrupt",	    /*(62)*/
		"Reserved Interrupt",	    /*(63)*/
		"Clock Interrupt",			/*(64)*/
		"Inter-processor Interrupt"	/*(65)*/
};
#endif

HR3_INT_CFG * g_irqCfg[HR3_INT_CNT] = {NULL};
HR3_INT_CFG g_intRouteTbl[ROUTE_CORE_NUM][IO_PIN_NUM][MAX_REUSABLE_INTSOURCE_NUM];

unsigned int g_intRouteTblReuseSign[ROUTE_CORE_NUM][IO_PIN_NUM];/* 记录某个core的某个中断引脚有多少个中断源共享 */

spinlockIsr_t * pintcSpinLock;

static int hr3_int_isEnable(u32 intrSource);

#ifndef _HC3020B_
#define CFG_HR2_ROUTE(in, out, core) \
{\
	u64 _data; \
	_data = MIPS_LW64((unsigned int)INTERRUPT_SEL_REG(in)); \
	_data = _data | ((out<<4)|core) ; \
	MIPS_SW64(INTERRUPT_SEL_REG(in),_data); \
}
#else /*ldf 20230921 add:: for hc3020b*/
#define CFG_HR2_ROUTE(in, out, core, preemption_pri, sub_pri) \
{\
	u64 _data; \
	_data = MIPS_LW64((unsigned int)INTERRUPT_SEL_REG(in)); \
	_data = _data | ((sub_pri<<4)|(core<<3)|preemption_pri) ; \
	MIPS_SW64((unsigned int)INTERRUPT_SEL_REG(in), _data); \
}
#endif

inline const char * const get_hr3_int_name(HR3_INT irq)
{
	return (IS_HR3_INT_VALID(irq)? g_hr3_int_name[(int)irq] : NULL);
}



#ifndef _HC3020B_
#define CFG_HR3_INT_ROUTE(core, pin, index, irq) \
		{ \
	g_intRouteTbl[core][pin][index].irqNo = irq; \
	g_irqCfg[irq] = &g_intRouteTbl[core][pin][index]; \
		}
#else /*ldf 20230921 add:: for hc3020b*/
#define CFG_HR3_INT_ROUTE(core, pin, index, irq, preemption, sub) \
		{ \
	g_intRouteTbl[core][pin][index].irqNo = irq; \
	g_intRouteTbl[core][pin][index].preemption_pri = preemption; \
	g_intRouteTbl[core][pin][index].sub_pri = sub; \
	g_irqCfg[irq] = &g_intRouteTbl[core][pin][index]; \
		}
#endif

/**
 * 初始化中断路由配置
 */
static void ht3_init_intRouteCfg()
{
	int ix, jx, kx;
	for (ix = 0; ix < ROUTE_CORE_NUM; ix++)
	{
		for (jx = 0; jx < IO_PIN_NUM; jx++)
		{
			for (kx = 0; kx < MAX_REUSABLE_INTSOURCE_NUM; kx++)
			{
				g_intRouteTbl[ix][jx][kx].irqNo = HR3_INT_END;
				g_intRouteTbl[ix][jx][kx].isCfg = FALSE;
				g_intRouteTbl[ix][jx][kx].isEnable = FALSE;
#ifdef _HC3020B_ /*ldf 20230921 add:: for hc3020b*/
				g_intRouteTbl[ix][jx][kx].preemption_pri = 0; /*中断抢占优先级*/
				g_intRouteTbl[ix][jx][kx].sub_pri = 1; /*中断响应优先级*/
#endif
			}
		}
	}

//ww mod 20190215 add 
#ifndef __BOOTIMAGE__
	
#if defined(__HUARUI2__)
// core: 0
	// pin:  0
//	CFG_HR3_INT_ROUTE(0, 0, 0, INT_GPDMA);//INT_GMAC0 INT_GPDMA
	//ww mod 20190130 3个网卡中断路由到0核

	CFG_HR3_INT_ROUTE(0, 0, 0, INT_GPDMA);//INT_GMAC0 INT_GPDMA
//	CFG_HR3_INT_ROUTE(0, 0, 0, INT_GMAC1);//INT_GMAC0 INT_GPDMA
//	CFG_HR3_INT_ROUTE(0, 0, 0, INT_GMAC2);//INT_GMAC0 INT_GPDMA
	// pin:  1
	CFG_HR3_INT_ROUTE(0, 1, 0, INT_I2CS1);
	CFG_HR3_INT_ROUTE(0, 1, 1, INT_PCIE);
	CFG_HR3_INT_ROUTE(0, 1, 2, INT_WDOG);
	// pin:  2
	CFG_HR3_INT_ROUTE(0, 2, 0, INT_UART0);
	CFG_HR3_INT_ROUTE(0, 2, 1, INT_I2CM0);
	CFG_HR3_INT_ROUTE(0, 2, 2, INT_TIMER0);//INT_TIMER0
	
//	CFG_HR3_INT_ROUTE(0, 2, 2, INT_CAN0);
	CFG_HR3_INT_ROUTE(0, 2, 3, INT_CAN0);//ww mod 20180611 mod
	
	
	// pin:  3
	CFG_HR3_INT_ROUTE(0, 3, 0, INT_TIMER1);//INT_TIMER1
	

	
#if 1
	
// core: 1
	// pin:  0
	// pin:  1
#if 1//ww mod 20190130 3个网卡中断路由到0核
	CFG_HR3_INT_ROUTE(1, 1, 0, INT_GMAC0);
	// pin:  2
	CFG_HR3_INT_ROUTE(1, 2, 0, INT_GMAC1);
//	 pin:  3
	CFG_HR3_INT_ROUTE(1, 3, 0, INT_GMAC2);
	
#endif
// core: 2
	// pin:  0
	// pin:  1
	// pin:  2
	CFG_HR3_INT_ROUTE(2, 2, 0, INT_RAB0);
	// pin:  3
	CFG_HR3_INT_ROUTE(2, 3, 0, INT_RAB1);
// core: 3
	// pin:  0
	// pin:  1
	CFG_HR3_INT_ROUTE(3, 1, 0, GPIO_INT_MS1);
	// pin:  2
	// pin:  3
	
	
#endif //__HUARUI2__
	
#else //__HUARUI3__
#ifndef _HC3020B_
	/*core(0~7), pin(0~3), index(0~15)*/
	CFG_HR3_INT_ROUTE(0, 0, 0, INT_UART0);
	CFG_HR3_INT_ROUTE(0, 0, 1, INT_UART1);
	
	CFG_HR3_INT_ROUTE(0, 0, 2, INT_RAB0);
	CFG_HR3_INT_ROUTE(0, 0, 3, INT_RAB1);
	
	CFG_HR3_INT_ROUTE(0, 0, 4, INT_GPDMA);
	
	CFG_HR3_INT_ROUTE(0, 0, 5, INT_WDOG);
	
	CFG_HR3_INT_ROUTE(0, 1, 0, INT_GMAC0);
	CFG_HR3_INT_ROUTE(0, 1, 1, INT_GMAC1);
	CFG_HR3_INT_ROUTE(0, 1, 2, INT_GMAC2);
	CFG_HR3_INT_ROUTE(0, 1, 3, INT_GMAC3);

	CFG_HR3_INT_ROUTE(0, 2, 0, INT_LINK0);
	CFG_HR3_INT_ROUTE(0, 2, 1, INT_LINK1);
	CFG_HR3_INT_ROUTE(0, 2, 2, INT_LINK2);
	CFG_HR3_INT_ROUTE(0, 2, 3, INT_LINK3);
	
	CFG_HR3_INT_ROUTE(0, 3, 0, INT_CAN0);
	CFG_HR3_INT_ROUTE(0, 3, 1, INT_CAN1);
	
	CFG_HR3_INT_ROUTE(0, 3, 2, INT_TIMER0);
	CFG_HR3_INT_ROUTE(0, 3, 3, INT_TIMER1);
	
//	CFG_HR3_INT_ROUTE(0, 3, 4, INT_SD0);
//	CFG_HR3_INT_ROUTE(0, 3, 5, INT_SD1);
//	CFG_HR3_INT_ROUTE(0, 3, 6, INT_I2C0);
//	CFG_HR3_INT_ROUTE(0, 3, 7, INT_I2C1);
//	CFG_HR3_INT_ROUTE(0, 3, 8, RTC);

	CFG_HR3_INT_ROUTE(1, 0, 0, INT_GPIO_MS0);
	CFG_HR3_INT_ROUTE(1, 0, 1, INT_GPIO_MS1);
	CFG_HR3_INT_ROUTE(1, 0, 2, INT_GPIO_MS2);
	CFG_HR3_INT_ROUTE(1, 0, 3, INT_GPIO_MS3);
	CFG_HR3_INT_ROUTE(1, 0, 4, INT_GPIO_MS4);
	CFG_HR3_INT_ROUTE(1, 0, 5, INT_GPIO_MS5);
	CFG_HR3_INT_ROUTE(1, 1, 0, INT_GPIO_MS6);
	CFG_HR3_INT_ROUTE(1, 1, 1, INT_GPIO_MS7);
	CFG_HR3_INT_ROUTE(1, 1, 2, INT_GPIO_MS8);
	CFG_HR3_INT_ROUTE(1, 1, 3, INT_GPIO_MS9);
	CFG_HR3_INT_ROUTE(1, 1, 4, INT_GPIO_MS10);
	CFG_HR3_INT_ROUTE(1, 1, 5, INT_GPIO_MS11);
	CFG_HR3_INT_ROUTE(1, 2, 0, INT_GPIO_MS12);
	CFG_HR3_INT_ROUTE(1, 2, 1, INT_GPIO_MS13);
	CFG_HR3_INT_ROUTE(1, 2, 2, INT_GPIO_MS14);
	CFG_HR3_INT_ROUTE(1, 2, 3, INT_GPIO_MS15);
	CFG_HR3_INT_ROUTE(1, 2, 4, INT_GPIO_MS16);
	CFG_HR3_INT_ROUTE(1, 2, 5, INT_GPIO_MS17);
	CFG_HR3_INT_ROUTE(1, 3, 0, INT_GPIO_MS18);
	CFG_HR3_INT_ROUTE(1, 3, 1, INT_GPIO_MS19);
	CFG_HR3_INT_ROUTE(1, 3, 2, INT_GPIO_MS20);
	CFG_HR3_INT_ROUTE(1, 3, 3, INT_GPIO_MS21);
	CFG_HR3_INT_ROUTE(1, 3, 4, INT_GPIO_MS22);
	CFG_HR3_INT_ROUTE(1, 3, 5, INT_GPIO_MS23);
#else /*ldf 20230920 add:: for 3020b*/
	/*core(0~1); pin(0~3)和index(0~15)未使用,不重复即可; 
	 * 抢占优先级preemption_pri(0~4)数字越大优先级越高; 
	 * 响应优先级sub_pri(1~15)数字越大优先级越高*/

	CFG_HR3_INT_ROUTE(0, 0, 0, INT_IPI0, 4, 6);
	CFG_HR3_INT_ROUTE(1, 0, 0, INT_IPI1, 4, 6);
	
	CFG_HR3_INT_ROUTE(0, 0, 1, INT_UART0, 0, 1);
	CFG_HR3_INT_ROUTE(0, 0, 2, INT_UART1, 0, 1);
	CFG_HR3_INT_ROUTE(0, 0, 3, INT_UART2, 0, 1);
	CFG_HR3_INT_ROUTE(0, 0, 4, INT_UART3, 0, 1);
	CFG_HR3_INT_ROUTE(0, 0, 5, INT_UART4, 0, 1);
	CFG_HR3_INT_ROUTE(0, 0, 6, INT_UART5, 0, 1);
	CFG_HR3_INT_ROUTE(0, 0, 7, INT_UART6, 0, 1);
	CFG_HR3_INT_ROUTE(0, 0, 8, INT_UART7, 0, 1);
	
	CFG_HR3_INT_ROUTE(0, 0, 9, INT_GPDMA, 0, 1);
	
	CFG_HR3_INT_ROUTE(0, 1, 0, INT_GMAC0, 0, 1);
	CFG_HR3_INT_ROUTE(0, 1, 1, INT_GMAC1, 0, 1);
	CFG_HR3_INT_ROUTE(0, 1, 2, INT_RAB0, 0, 1);
	CFG_HR3_INT_ROUTE(0, 1, 3, INT_RAB1, 0, 1);

	CFG_HR3_INT_ROUTE(0, 2, 0, INT_WDOG0, 0, 1);
	CFG_HR3_INT_ROUTE(0, 2, 1, INT_WDOG1, 0, 1);
	CFG_HR3_INT_ROUTE(0, 2, 2, INT_WDOG2, 0, 1);
	CFG_HR3_INT_ROUTE(0, 2, 3, INT_WDOG3, 0, 1);
	
	CFG_HR3_INT_ROUTE(0, 3, 0, INT_CAN0, 0, 1);
	CFG_HR3_INT_ROUTE(0, 3, 1, INT_CAN1, 0, 1);
	
	CFG_HR3_INT_ROUTE(0, 3, 2, INT_TIMER0, 0, 1);
	CFG_HR3_INT_ROUTE(0, 3, 3, INT_TIMER1, 0, 1);
	CFG_HR3_INT_ROUTE(0, 3, 4, INT_TIMER2, 0, 1);
	CFG_HR3_INT_ROUTE(0, 3, 5, INT_TIMER3, 0, 1);
	CFG_HR3_INT_ROUTE(0, 3, 6, INT_TIMER4, 0, 1);
	CFG_HR3_INT_ROUTE(0, 3, 7, INT_TIMER5, 0, 1);
	CFG_HR3_INT_ROUTE(0, 3, 8, INT_TIMER6, 0, 1);
	CFG_HR3_INT_ROUTE(0, 3, 9, INT_TIMER7, 0, 1);

	CFG_HR3_INT_ROUTE(1, 0, 1, INT_GPIO_MS0, 0, 1);
	CFG_HR3_INT_ROUTE(1, 0, 2, INT_GPIO_MS1, 0, 1);
	CFG_HR3_INT_ROUTE(1, 0, 3, INT_GPIO_MS2, 0, 1);
	CFG_HR3_INT_ROUTE(1, 0, 4, INT_GPIO_MS3, 0, 1);
	CFG_HR3_INT_ROUTE(1, 0, 5, INT_GPIO_MS4, 0, 1);
	CFG_HR3_INT_ROUTE(1, 0, 6, INT_GPIO_MS5, 0, 1);
	CFG_HR3_INT_ROUTE(1, 1, 0, INT_GPIO_MS6, 0, 1);
	CFG_HR3_INT_ROUTE(1, 1, 1, INT_GPIO_MS7, 0, 1);
	CFG_HR3_INT_ROUTE(1, 1, 2, INT_GPIO_MS8, 0, 1);
	CFG_HR3_INT_ROUTE(1, 1, 3, INT_GPIO_MS9, 0, 1);
	CFG_HR3_INT_ROUTE(1, 1, 4, INT_GPIO_MS10, 0, 1);
	CFG_HR3_INT_ROUTE(1, 1, 5, INT_GPIO_MS11, 0, 1);
	CFG_HR3_INT_ROUTE(1, 2, 0, INT_GPIO_MS12, 0, 1);
	CFG_HR3_INT_ROUTE(1, 2, 1, INT_GPIO_MS13, 0, 1);
	CFG_HR3_INT_ROUTE(1, 2, 2, INT_GPIO_MS14, 0, 1);
	CFG_HR3_INT_ROUTE(1, 2, 3, INT_GPIO_MS15, 0, 1);
	CFG_HR3_INT_ROUTE(1, 2, 4, INT_GPIO_MS16, 0, 1);
	CFG_HR3_INT_ROUTE(1, 2, 5, INT_GPIO_MS17, 0, 1);
	CFG_HR3_INT_ROUTE(1, 3, 0, INT_GPIO_MS18, 0, 1);
	CFG_HR3_INT_ROUTE(1, 3, 1, INT_GPIO_MS19, 0, 1);
	CFG_HR3_INT_ROUTE(1, 3, 2, INT_GPIO_MS20, 0, 1);
	CFG_HR3_INT_ROUTE(1, 3, 3, INT_GPIO_MS21, 0, 1);
	CFG_HR3_INT_ROUTE(1, 3, 4, INT_GPIO_MS22, 0, 1);
	CFG_HR3_INT_ROUTE(1, 3, 5, INT_GPIO_MS23, 0, 1);
#endif/*_HC3020B_*/
//	CFG_HR3_INT_ROUTE()
#endif /*__HUARUI2__*/

	
#else//__BOOTIMAGE__

#if defined(__HUARUI2__)
//	bootimage模式下不用启动多核
// core: 0
		// pin:  0	
	CFG_HR3_INT_ROUTE(0, 1, 0, INT_GMAC0);
	CFG_HR3_INT_ROUTE(0, 2, 4, INT_GMAC1);
	CFG_HR3_INT_ROUTE(0, 3, 1, INT_GMAC2);
	
//	CFG_HR3_INT_ROUTE(0, 1, 0, INT_I2CS1);
	CFG_HR3_INT_ROUTE(0, 1, 1, INT_PCIE);
	CFG_HR3_INT_ROUTE(0, 1, 2, INT_WDOG);
	// pin:  2
	CFG_HR3_INT_ROUTE(0, 2, 0, INT_UART0);
	CFG_HR3_INT_ROUTE(0, 2, 1, INT_I2CM0);
	CFG_HR3_INT_ROUTE(0, 2, 2, INT_TIMER0);//INT_TIMER0
	CFG_HR3_INT_ROUTE(0, 2, 3, INT_CAN0);//ww mod 20180611 mod
	
	
	// pin:  3
	CFG_HR3_INT_ROUTE(0, 3, 0, INT_TIMER1);//INT_TIMER1
	
#endif /*__HURRUI2__*/

#endif//__BOOTIMAGE__
//ww mod 20190215 add
	
	
	
	for (ix = 0; ix < ROUTE_CORE_NUM; ix++)
	{
		for (jx = 0; jx < IO_PIN_NUM; jx++)
		{
			g_intRouteTblReuseSign[ix][jx] = 0;
			for (kx = 0; kx < MAX_REUSABLE_INTSOURCE_NUM; kx++)
			{
				if (g_intRouteTbl[ix][jx][kx].irqNo != HR3_INT_END)
					g_intRouteTblReuseSign[ix][jx]++;
			}
		}
	}
}

int hr3_int_init(void)
{
//#ifdef DEBUG
	printk("enter hr3_int_init()\n");
	ht3_init_intRouteCfg();
//#endif
	union datacast_64 _data64;
	/*zz Mask off all interrupts (they'll be enabled as needed later). */
//	sysWrite64(0x90000000,INTERRUPT_MODE_REG,0,0);
	sysWrite64(0x90000000,INTERRUPT_ENABLE_H_REG,0,0);
	sysWrite64(0x90000000,INTERRUPT_ENABLE_L_REG,0,0);
	
	//////////////////////////////////////////////////
//	_data64.l32.h = 0xffff1234;
//	_data64.l32.l  = 0xffff5678;
//	
//	sysWrite64(0x90000000,INTERRUPT_MODE_REG,_data64.l32.h,_data64.l32.l);
//	_data64.l32.h = 0;
//	_data64.l32.l  = 0;
//	sysRead64(0x90000000,INTERRUPT_MODE_REG,&_data64.l32.h,&_data64.l32.l);
//	printk("%s, %d, _data64.l32.h = 0x%x, _data64.l32.l = 0x%x\n", __FUNCTION__, __LINE__,_data64.l32.h, _data64.l32.l);
//	
//	sysRead64(0x90000000,INTERRUPT_ENABLE_REG,&_data64.l32.h,&_data64.l32.l);
//	printk("%s, %d, _data64.l32.h = 0x%x, _data64.l32.l = 0x%x\n", __FUNCTION__, __LINE__,_data64.l32.h, _data64.l32.l);
//	sysLEDSetState(0);
//	while(1);
	////////////////////////////////////////////////
	
	
	
	
	_data64.l32.h = 0xffffffff;
	_data64.l32.l  = 0xffffffff;
	//sysWrite64(0x90000000,INTERRUPT_MASK_REG,_data64.l32.h,_data64.l32.l);
	
	//////////////////////////////////////////////////
//	sysRead64(0x90000000,INTERRUPT_STAT_REG,&_data64.l32.h,&_data64.l32.l);
//	printk("%s, %d, _data64.l32.h = 0x%x, _data64.l32.l = 0x%x\n", __FUNCTION__, __LINE__,_data64.l32.h, _data64.l32.l);
//	sysLEDSetState(0);
//	while(1);
	////////////////////////////////////////////////
	
	

//	sysRead64(0x90000000,INTERRUPT_STAT_REG,&_data64.l32.h,&_data64.l32.l);
	sysWrite64(0x90000000,INTERRUPT_CLR_H_REG,_data64.l32.h,_data64.l32.l);
	sysWrite64(0x90000000,INTERRUPT_CLR_L_REG,_data64.l32.h,_data64.l32.l);
	
	

//	sysWrite64(INTERRUPT_ENABLE_REG, ~0); /*  默认使能所有中断            */
//	sysWrite64(INTERRUPT_MASK_REG, ~0); /*  默认屏蔽所有中断            */
//	sysWrite64(INTERRUPT_MODE_REG, 0);
//	sysWrite64(INTERRUPT_CLR_REG, ~0);	
//	int i;
//    for (i = 0; i < HR3_INT_CNT; i++) {
//        if (30 == i || 31 == i || 32 == i || 33 == i) {                 /*  核间中断绑定 INT4           */
//        	sysWrite32(INTERRUPT_SEL_REG(i),4 << 4);
//        } else {
//        	sysWrite32(INTERRUPT_SEL_REG(i),0);
//        }
//    }
#if 1
	
	int ix, jx, kx;
	for (ix = 0; ix < ROUTE_CORE_NUM; ix++)
	{
//		printk("++++++++++++++++++++++++++++++++++++++++++++++++\n", ix);
//		printk("core: %d\n", ix);		
		for (jx = 0; jx < IO_PIN_NUM; jx++)
		{
//			printk("  -------------------------------------------\n");
//			printk("  pin:%d\n", jx);			
			for (kx = 0; kx < MAX_REUSABLE_INTSOURCE_NUM; kx++)
			{
				if (g_intRouteTbl[ix][jx][kx].irqNo != HR3_INT_END)
				{
					if (!g_intRouteTbl[ix][jx][kx].isCfg)
					{
#ifndef _HC3020B_
						CFG_HR2_ROUTE(g_intRouteTbl[ix][jx][kx].irqNo, jx, ix);
#else /*ldf 20230921 add:: for hc3020b*/
						CFG_HR2_ROUTE(g_intRouteTbl[ix][jx][kx].irqNo, jx, ix,\
								g_intRouteTbl[ix][jx][kx].preemption_pri, \
								g_intRouteTbl[ix][jx][kx].sub_pri);
#endif
						g_intRouteTbl[ix][jx][kx].isCfg = TRUE;
//						printk("<**DEBUG**> [%s():_%d_]::     intrSource[0x%x, %d] ------ configured!!!\n", 
//								__FUNCTION__, __LINE__,g_intRouteTbl[ix][jx][kx].irqNo, g_intRouteTbl[ix][jx][kx].irqNo);
						hr3_int_isEnable(g_intRouteTbl[ix][jx][kx].irqNo);//同步一下使能状态
//						printk("<**DEBUG**> [%s():_%d_]:: hr3_int_isEnable = %d\n", __FUNCTION__, __LINE__,g_intRouteTbl[ix][jx][kx].irqNo);
					}
				}
			}
		}
//		printk("  -------------------------------------------\n");
	}
//	printk("++++++++++++++++++++++++++++++++++++++++++++++++\n");


#endif
	

//	
//	
//	u32 val = 0;
	int i = 0;
//	spin_lock_init(&bsp_int_lock);
//	
//	int zrID = bslProcGetId();
//	
//#ifdef DEBUG
//	printk("begin of internal_int_init\n");
//#endif
//	/* TODO: 初始化本级中断控制器 */
//	/* 将LPC中断路由到Core0的INT0(即IP2)*/
////	mips64_writeb(LPC_INT_ENTRY,0x11);
//	CPU_WRITE8(LPC_INT_ENTRY,zrID,0x11);
//	
//	/* 将网卡中断路由到Core0的INT2(即IP4)*/
//	CPU_WRITE8(SYS_INT2_ENTRY,zrID,0x41);
//
//	/* 将MATRIX dma Int0中断路由到Core0的INT2(即IP4)*/
//	CPU_WRITE8(MATRIX_INT0_ENTRY,zrID,0x41);
//	
//	/* 将MATRIX dma Int1中断路由到Core0的INT2(即IP4)*/
//	CPU_WRITE8(MATRIX_INT1_ENTRY,zrID,0x41);
//	
//	/* 将rapidio send中断路由到Core0的INT3(即IP5)*/
//	CPU_WRITE8(SYS_INT3_ENTRY,zrID,0x81);
//	
//	/* 将rapidio recv中断路由到Core0的INT3(即IP5)*/
//	CPU_WRITE8(SYS_INT1_ENTRY,zrID,0x81);
//	
//	/* 将gpio中断路由到Core0的INT2(即IP4)*/
//	CPU_WRITE8(SYS_INT2_ENTRY,zrID,0x41);
//	
//	/*通过Intenset寄存器使能LPC和SYS_INT2中断*/
//    val = (1 << LPC_INT) | (1 << SYS_INT2);
//	CPU_WRITE32(INTENSET_REG,zrID,val);
//
	//ww mod 20190216 add num_online_cpus()为啥会返回1？
#if MAX_SMP_CPUS==2
	printk("num_online_cpus == %d, MAX_SMP_CPUS=%d\n", num_online_cpus(), MAX_SMP_CPUS);
#endif
/*使能各核核间中断*/
#ifndef __BOOTIMAGE__
	for(i = 0; i < MAX_SMP_CPUS ;i++)
#else//__BOOTIMAGE__
	for(i = 0; i < 1 ;i++)
#endif//__BOOTIMAGE__ ww mod 20190226 add
	{
		ipi_enable(i);
#ifdef _HC3020B_/*ldf 20230921 add:: for hc3020b*/
//		ipi_install_handler(i, NULL, NULL);
#endif
	}
	
	
	printk("end of hr3_int_init\n");
	return 0;
}



/**
 * 
 * 外部中断控制器中断使能接口
 * 		
 * 		本接口用于使能指定的中断请求号irq对应的硬件中断。在使能中断成功后，参数irq
 * 对应的中断引脚处于中断可用状态，能够响应连接到该中断引脚上的外部设备发出的中断。
 * 
 * 输入：
 * 		irq：中断请求
 * 输出:
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 */
static int hr3_int_isEnable(u32 intrSource)
{
	assert(IS_HR3_INT_VALID(intrSource));
	if (IS_HR3_INT_INTERNAL(intrSource)) return 1;

	if(NULL == g_irqCfg[intrSource])
	{
		printk("%s, %d, intrSource[%d] %s, not cfg!!!\n", __FUNCTION__, __LINE__, intrSource, get_hr3_int_name(intrSource));
		return 0;
	}  
	assert(g_irqCfg[intrSource]->isCfg);
//	printk("%s, %d\n", __FUNCTION__, __LINE__);
	int isMaskSet = 0;
	int isEnableRegSet = 0;
	

	union datacast_64 _data64; 	
 
	/*zz modify*/		
	if(intrSource <= 63)
	{
		sysRead64(0x90000000,INTERRUPT_ENABLE_L_REG,&_data64.l32.h,&_data64.l32.l);
		if ((_data64.l64 & (1UL << intrSource)) != 0)
		{
			isEnableRegSet = 1;
		}
	}
	else
	{
		sysRead64(0x90000000,INTERRUPT_ENABLE_H_REG,&_data64.l32.h,&_data64.l32.l);
		if ((_data64.l64 & (1UL << (intrSource-64))) != 0)
		{
			isEnableRegSet = 1;
		}
	}
	
	
	
	g_irqCfg[intrSource]->isEnable = (isEnableRegSet);
	

	return g_irqCfg[intrSource]->isEnable;
}


/******************************************************************************
 * 
 * 外部中断控制器中断使能接口
 * 		
 * 		本接口用于使能指定的中断请求号irq对应的硬件中断。在使能中断成功后，参数irq
 * 对应的中断引脚处于中断可用状态，能够响应连接到该中断引脚上的外部设备发出的中断。
 * 
 * 输入：
 * 		irq：中断请求
 * 输出:
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 * 备注：
 * 		本程序在CSP中断框架中调用，为必须实现的接口。
 */
int hr3_int_enable(u32 intrSource)
{
	assert(IS_HR3_INT_VALID(intrSource));

	if (IS_HR3_INT_INTERNAL(intrSource)) return 0; //华睿3号内部中断不需要使能

	if(NULL == g_irqCfg[intrSource])
	{
		printk("%s, %d, intrSource[%d] %s not cfg!!!\n", __FUNCTION__, __LINE__, intrSource, get_hr3_int_name(intrSource));
		return -1;
	}
	assert(g_irqCfg[intrSource]->isCfg);	
	//if(intrSource>0)
	//printk("%s, %d,HR3_INT_CNT=0x%x,INTERRUPT_ENABLE_L_REG=0x%lx,0x%lx\n", __FUNCTION__, __LINE__,HR3_INT_CNT,INTERRUPT_ENABLE_L_REG,INTERRUPT_ENABLE_H_REG);
	
	if(g_irqCfg[intrSource]->isEnable)
	{
//		printk("%s, %d, intrSource[%d] %s has been enabled!!!\n",  __FUNCTION__, __LINE__, intrSource, get_hr3_int_name(intrSource));
		//return 0;//gongchao 20190128
	}
	
	//for hr3
//	t_spinlockIsrTake(pintcSpinLock);
	int level = int_lock();

//	printk("%s, %d\n", __FUNCTION__, __LINE__);
	union datacast_64 _data64;
	/*zz modify*/	
//	sysRead64(0x90000000,INTERRUPT_MASK_REG,&_data64.l32.h,&_data64.l32.l);
//
//	if (intrSource <= 31)
//	{
//		_data64.l32.l = (_data64.l32.l & (~(1 << intrSource)));
//	}
//	else
//	{
//		_data64.l32.h = (_data64.l32.h & (~(1 << (intrSource - 32))));
//	}
//
//	sysWrite64(0x90000000,INTERRUPT_MASK_REG,_data64.l32.h,_data64.l32.l);

	u64 data;
	
	if(intrSource <= 63)
	{
		sysRead64(0x90000000,INTERRUPT_ENABLE_L_REG,&_data64.l32.h,&_data64.l32.l);
		_data64.l64 = (_data64.l64 | (1UL << intrSource));
		sysWrite64(0x90000000,INTERRUPT_ENABLE_L_REG,_data64.l32.h,_data64.l32.l);
	}
	else
	{
		sysRead64(0x90000000,INTERRUPT_ENABLE_H_REG,&_data64.l32.h,&_data64.l32.l);
		//if(intrSource>0)
		//printk("_data64--1--[0x%lx]intrSource[%d]\n",_data64.l64,intrSource);

		data = (1UL << (intrSource-64));
		_data64.l64 = (_data64.l64 | data);
		//if(intrSource>0)
		//printk("_data64--1--_data64.l64[0x%lx]data[0x%lx]\n",_data64.l64,data);

		sysWrite64(0x90000000,INTERRUPT_ENABLE_H_REG,_data64.l32.h,_data64.l32.l);

	}


	

	//printstr("_data64.l32.h:0x");printnum(_data64.l32.h);printstr("\r\n");
	

	g_irqCfg[intrSource]->isEnable = 1;

	//for hr3
//	t_spinlockIsrGive(pintcSpinLock);
	int_unlock(level);

	return 0;
}


/******************************************************************************
 * 
 * 中断屏蔽函数
 * 		
 * 		本接口用于屏蔽指定的中断请求号irq对应的硬件中断。在屏蔽中断成功后，参数irq
 * 对应的中断引脚处于中断不可用状态，连接到该中断引脚上的外部设备发出的中断不再被相应。
 * 
 * 输入：
 * 		irq：中断请求
 * 输出:
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 * 备注：
 * 		本程序在CSP中断框架中调用，为必须实现的接口。
 */
int hr3_int_disable(u32 intrSource)
{
	assert(IS_HR3_INT_VALID(intrSource));

	if (IS_HR3_INT_INTERNAL(intrSource))
	{
		printk("internal intrSource %d cannot disable!!!\n", intrSource);
		return -1; //华睿3号内部中断不需要使能
	}

	if(NULL == g_irqCfg[intrSource])
	{
		printk("intrSource %d not cfg!!!\n", intrSource);
		return -1;
	}
	assert(g_irqCfg[intrSource]->isCfg);	
	if(FALSE == g_irqCfg[intrSource]->isEnable)
	{
//		printk("intrSource %d has been disabled!!!\n", intrSource);
		////return 0;//gongchao 20190128
	}
	
	//for hr3
//	t_spinlockIsrTake(pintcSpinLock);
	int level = int_lock();

	union datacast_64 _data64; 	
 
	/*zz modify*/		
//	sysRead64(0x90000000,INTERRUPT_MASK_REG,&_data64.l32.h,&_data64.l32.l);
//
//	if(intrSource <= 31)
//	{
//		_data64.l32.l  = (_data64.l32.l  | (1 << intrSource));
//	}
//	else
//	{
//		_data64.l32.h  = (_data64.l32.h  | (1 << (intrSource - 32)));
//	}

	if(intrSource <= 63)
	{
		sysRead64(0x90000000,INTERRUPT_ENABLE_L_REG,&_data64.l32.h,&_data64.l32.l);
		_data64.l64 = (_data64.l64 & (~(1UL << intrSource)));
		sysWrite64(0x90000000,INTERRUPT_ENABLE_L_REG,_data64.l32.h,_data64.l32.l);
	}
	else
	{
		sysRead64(0x90000000,INTERRUPT_ENABLE_H_REG,&_data64.l32.h,&_data64.l32.l);
		_data64.l64 = (_data64.l64 & (~(1UL << (intrSource-64))));
		sysWrite64(0x90000000,INTERRUPT_ENABLE_H_REG,_data64.l32.h,_data64.l32.l);
	}


//
//
//	sysWrite64(0x90000000,INTERRUPT_MASK_REG,_data64.l32.h,_data64.l32.l);
//
//	sysRead64(0x90000000,INTERRUPT_ENABLE_REG,&_data64.l32.h,&_data64.l32.l);
//	if (intrSource <= 31)
//	{
//		_data64.l32.l = (_data64.l32.l & (~(1 << intrSource)));
//	}
//	else
//	{
//		_data64.l32.h = (_data64.l32.h & (~(1 << (intrSource - 32))));
//	}
//
//	sysWrite64(0x90000000,INTERRUPT_ENABLE_REG,_data64.l32.h,_data64.l32.l);

//    mips64_writew(0, INTENCLR_REG,(1 << LPC_INT));
	g_irqCfg[intrSource]->isEnable = 0;

	//for hr3
	//	t_spinlockIsrGive(pintcSpinLock);
	int_unlock(level);

	return 0;
}


/*******************************************************************************
 * 
 * 中断响应操作
 * 
 * 		本接口对内部中断进行响应，如果外部中断控制器需要进行中断响应的话。
 * 输入：
 * 		interrupt_pending：需要响应的中断号。
 * 输出：
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 * 备注：
 * 		本接口需要根据具体的硬件平台和外部中断控制器进行实现
 */
int hr3_int_ack(int intrSource)
{	
	assert(IS_HR3_INT_VALID(intrSource));
	
#ifndef _HC3020B_
	if (intrSource == INT_IPI)
#else/*ldf 20230920 modify:: for hc3020b*/
	if ((intrSource == INT_IPI0) || (intrSource == INT_IPI1)) 
#endif
	{
		ipi_ack();
		return 0;
	}
	
	if (intrSource == INT_CLOCK)
	{
		return 0;
	}
		
	if(NULL == g_irqCfg[intrSource])
	{
		printk("intrSource %d not cfg!!!\n", intrSource);
		return -1;
	}
	assert(g_irqCfg[intrSource]->isCfg);
//	assert(g_irqCfg[intrSource]->isEnable);
	
	u32 intisr, inten, and;
		
	union datacast_64 _data64;
	/*zz Mask off all interrupts (they'll be enabled as needed later). */
//	sysWrite64(0x90000000,INTERRUPT_MODE_REG,0,0);
//	sysWrite64(0x90000000,INTERRUPT_ENABLE_REG,0,0);
	
//	_data64.l32.h = 0xffffffff;
//	_data64.l32.l  = 0xffffffff;
//	sysWrite64(0x90000000,INTERRUPT_CLR_REG,_data64.l32.h,_data64.l32.l);

	//for hr3
//	t_spinlockIsrTake(pintcSpinLock);
	int level = int_lock();




	if(intrSource <= 63)
	{
		sysRead64(0x90000000,INTERRUPT_STAT_L_REG,&_data64.l32.h,&_data64.l32.l);
		_data64.l64 = (_data64.l64 | (1UL << intrSource));
		sysWrite64(0x90000000,INTERRUPT_CLR_L_REG,_data64.l32.h,_data64.l32.l);
	}
	else
	{
		sysRead64(0x90000000,INTERRUPT_STAT_H_REG,&_data64.l32.h,&_data64.l32.l);
		_data64.l64 = (_data64.l64 | (1UL << (intrSource-64)));
		sysWrite64(0x90000000,INTERRUPT_CLR_H_REG,_data64.l32.h,_data64.l32.l);
	}




	//for hr3
//	t_spinlockIsrGive(pintcSpinLock);
	int_unlock(level);
	

//	mips64_readw(0, INTISR_REG);
//	intisr = mips64_readw(0, INTISR_CORE0_REG);
//	inten = mips64_readw(0, INTEN_REG);
//	
	
//	extern u32 get_c0_sr();
//	extern void set_c0_sr(u32 val);
//	register u32 srValue = get_c0_sr();
//    srValue |= SR_IE;	
//	set_c0_sr(srValue);
	
//	and = intisr & inten;

    return 0;
}

#if 1

#define HR3_INT_FILTER_CORE 1    /*宏控制按中断路由到的核对中断进行过滤*/

//add by gongchao 2022.09.01 增加原因是发现在华睿3080上，会出现一个中断同时在多个核上触发的情况，这是不正常的，违反中断路由约定的
//但是出现这现象的原因不明，待深入排查
/*******************************************************************************
 *
 * 获取中断向量
 *
 * 		本接口特殊中断操作，本接口存在必要性以及接口语法定义由具体平台相关。
 * 输入：
 * 		interrupt_pending：中断屏蔽码，由具体平台确定
 * 输出：
 * 		无
 * 返回：
 * 		成功返回中断向量；失败返回0
 * 备注：
 * 		本接口需要根据具体的硬件平台和外部中断控制器进行实现
 */

inline int hr3_int_is_in_select_core(int irq,int core)
{
	volatile unsigned int val=0;
	val = sysRead32(INTERRUPT_SEL_REG(irq));
#ifndef _HC3020B_
	if((val & 0xf) == core)
#else /*ldf 20230921 add:: for 3020b*/
	if(((val&0x8) >> 3) == core)
#endif
	{
//		if(irqCal+64 == 67 || irqCal+64 == 71)
//			printk("H:_data2= 0x%x, _data1=0x%x,cpunum=0x%x,val=0x%x,irq=%d\n",_data2x,_data1x,cpunum,val,irqCal+64);
		return 1;
	}

	//printk("H:irq=%d,val =0x%x should not in cpunum=0x%x,0x%x\n",irqCal+64,val,cpunum,val& 0xf);
	return 0;
}


#endif
/*******************************************************************************
 * 
 * 获取中断向量
 * 
 * 		本接口特殊中断操作，本接口存在必要性以及接口语法定义由具体平台相关。
 * 输入：
 * 		interrupt_pending：中断屏蔽码，由具体平台确定
 * 输出：
 * 		无
 * 返回：
 * 		成功返回中断向量；失败返回0
 * 备注：
 * 		本接口需要根据具体的硬件平台和外部中断控制器进行实现
 */
int hr3_int_inum(u32 interrupt_pending, u32 irq_array[], u32 irq_num)
{
	int cpu = cpu_id_get();
	assert(irq_array != NULL);
	if (irq_num == 0) return 0;
	if (interrupt_pending == 0) return 0;
	
	int irq_index = 0;
	u32 temp = interrupt_pending;

	if (interrupt_pending & CAUSE_IP7)
	{
		irq_array[irq_index++] = INT_CLOCK;
		interrupt_pending &= ~CAUSE_IP7;
	}
	if (irq_index == irq_num) return irq_index;
	
	if (interrupt_pending & CAUSE_IP6)
	{
#ifndef _HC3020B_
		irq_array[irq_index++] = INT_IPI;
#else /*ldf 20230920 add:: for hc3020b*/
		irq_array[irq_index++] = (cpu == 0) ? INT_IPI0 : INT_IPI1;
#endif
		interrupt_pending &= ~CAUSE_IP6;
	}
	if (irq_index == irq_num) return irq_index;
	
//	if(irq_index >1)
//		printk("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ temp 0x%x \n",temp);
	unsigned int _data1 = 0;
	unsigned int _data2 = 0;
	unsigned int _data1x = 0;
	unsigned int _data2x = 0;
	unsigned int  ix;
	unsigned int temp2 = 0;	
	unsigned int i = 0;
	unsigned int cpunum = cpu_id_get();
	unsigned int val=0;
	interrupt_pending >>= 10;

	
	sysRead64(0x90000000,INTERRUPT_STAT_L_REG,&_data2,&_data1);

//	printk("L:_data2= 0x%x, _data1=0x%x",_data2,_data1);

	/**********Edward.Jiang 20210605 华睿3号测试OK，解决多个外设中断只返回irq_index=1的问题***************/
	int irqCal=0;
	while(_data1!=0){

		if((_data1 & 0x1) == 1){
			//irq_array[irq_index++]=irqCal;
#ifdef HR3_INT_FILTER_CORE  //add by gongchao 2022.09.01  实现根据中断路由核进行中断过滤
			if(hr3_int_is_in_select_core(irqCal,cpunum))
			{
				irq_array[irq_index++]=irqCal;
				//	printk("H:_data2= 0x%x, _data1=0x%x,cpunum=0x%x,val=0x%x,irq=%d\n",_data2x,_data1x,cpunum,val,irqCal);
			}
			else
			{
				//printk("H:irq=%d,val =0x%x should not in cpunum=0x%x,0x%x\n",irqCal,val,cpunum,val& 0xf);
			}
#else
			val = sysRead32(INTERRUPT_SEL_REG(irqCal));
			irq_array[irq_index++]=irqCal;
			//printk("H:_data2= 0x%x, _data1=0x%x,cpunum=0x%x,val=0x%x,irq=%d\n",_data2x,_data1x,cpunum,val,irqCal);
#endif
			//printk("irq_index=%d irq_array[irq_index]=%d \r\n",irq_index,irq_array[irq_index]);
		}
		_data1=_data1 >> 1;
		irqCal++;
	}

	irqCal=0;
	while(_data2!=0){

		if((_data2 & 0x1) == 1){
			//irq_array[irq_index++]=irqCal+32;
#ifdef HR3_INT_FILTER_CORE   //add by gongchao 2022.09.01  实现根据中断路由核进行中断过滤
			if(hr3_int_is_in_select_core(irqCal+32,cpunum))
			{
				irq_array[irq_index++]=irqCal+32;
				//	printk("H:_data2= 0x%x, _data1=0x%x,cpunum=0x%x,val=0x%x,irq=%d\n",_data2x,_data1x,cpunum,val,irqCal+32);

			}
			else
			{
				//printk("H:irq=%d,val =0x%x should not in cpunum=0x%x,0x%x\n",irqCal+32,val,cpunum,val& 0xf);
			}
#else
			val = sysRead32(INTERRUPT_SEL_REG(irqCal+32));
			irq_array[irq_index++]=irqCal+32;
			//printk("H:_data2= 0x%x, _data1=0x%x,cpunum=0x%x,val=0x%x,irq=%d\n",_data2x,_data1x,cpunum,val,irqCal+32);
#endif
			//printk("###################irq_index=%d irq_array[irq_index]=%d \r\n",irq_index,irq_array[irq_index]);
		}
		_data2=_data2 >> 1;
		irqCal++;
	}

	sysRead64(0x90000000,INTERRUPT_STAT_H_REG,&_data2,&_data1);
	_data2x=_data2;
	_data1x=_data1;
		//printk("H:_data2= 0x%x, _data1=0x%x",_data2,_data1);

		/**********Edward.Jiang 20210605 华睿3号测试OK，解决多个外设中断只返回irq_index=1的问题***************/
	irqCal=0;
	while(_data1!=0){

		if((_data1 & 0x1) == 1){
#ifdef HR3_INT_FILTER_CORE   //add by gongchao 2022.09.01  实现根据中断路由核进行中断过滤
			if(hr3_int_is_in_select_core(irqCal+64,cpunum))
			{
				irq_array[irq_index++]=irqCal+64;
				//if(irqCal+64 == 67 || irqCal+64 == 71)
				//	printk("H:_data2= 0x%x, _data1=0x%x,cpunum=0x%x,val=0x%x,irq=%d\n",_data2x,_data1x,cpunum,val,irqCal+64);
			}
			else
			{
				//printk("H:irq=%d,val =0x%x should not in cpunum=0x%x,0x%x\n",irqCal+64,val,cpunum,val& 0xf);
			}
#else
			val = sysRead32(INTERRUPT_SEL_REG(irqCal+64));
			irq_array[irq_index++]=irqCal+64;
			printk("H:_data2= 0x%x, _data1=0x%x,cpunum=0x%x,val=0x%x,irq=%d\n",_data2x,_data1x,cpunum,val,irqCal+64);
#endif
			//printk("irq_index=%d irq_array[irq_index]=%d \r\n",irq_index,irq_array[irq_index]);
		}
		_data1=_data1 >> 1;
		irqCal++;
	}

	irqCal=0;
	while(_data2!=0){

		if((_data2 & 0x1) == 1){

#ifdef HR3_INT_FILTER_CORE   //add by gongchao 2022.09.01  实现根据中断路由核进行中断过滤
			if(hr3_int_is_in_select_core(irqCal+96,cpunum))
			{
				irq_array[irq_index++]=irqCal+96;
				//	printk("H:_data2= 0x%x, _data1=0x%x,cpunum=0x%x,val=0x%x,irq=%d\n",_data2x,_data1x,cpunum,val,irqCal+96);

			}
			else
			{
				//printk("H:irq=%d,val =0x%x should not in cpunum=0x%x,0x%x\n",irqCal+96,val,cpunum,val& 0xf);
			}
#else
			val = sysRead32(INTERRUPT_SEL_REG(irqCal+96));
			irq_array[irq_index++]=irqCal+96;
			//printk("H:_data2= 0x%x, _data1=0x%x,cpunum=0x%x,val=0x%x,irq=%d\n",_data2x,_data1x,cpunum,val,irqCal+96);
#endif
			//printk("###################irq_index=%d irq_array[irq_index]=%d \r\n",irq_index,irq_array[irq_index]);
		}
		_data2=_data2 >> 1;
		irqCal++;
	}


#if 0 //华睿3不适用
	//_data1 = _data64.l32.l;
	//_data2 = _data64.l32.h;
	
	//_intrLine = 0;	
//#if 1 /* edited by syj for muticores int begin*/

	/*CAUSE_IP2 ~ CAUSE_IP5*/
	while (interrupt_pending != 0) 
	{
		if ((interrupt_pending & 0x1) != 0) 
		{
			for(i = 0; i < g_intRouteTblReuseSign[cpunum][temp2]; i++)
			{
				if(g_intRouteTbl[cpunum][temp2][i].irqNo < 32)
				{
					if((1 << g_intRouteTbl[cpunum][temp2][i].irqNo) & _data1)
					{
						irq_array[irq_index++] = g_intRouteTbl[cpunum][temp2][i].irqNo;
						if (irq_index == irq_num) break;
//						VXB_INTCTLR_ISR_CALL(pEnt, HIGH(intSource));
						
//						sysWrite64(0x90000000,INTERRUPT_STAT_REG,0,(1 << g_intRouteTbl[cpunum][temp2][i]));
					}
				}
				if(g_intRouteTbl[cpunum][temp2][i].irqNo >= 32)
				{
					if((1 << (g_intRouteTbl[cpunum][temp2][i].irqNo - 32)) & _data2)
					{
						irq_array[irq_index++] = g_intRouteTbl[cpunum][temp2][i].irqNo;
						if (irq_index == irq_num) break;
//						intSource = g_intRouteTbl[cpunum][temp2][i];
//						VXB_INTCTLR_ISR_CALL(pEnt, HIGH(intSource));
						
//						sysWrite64(0x90000000,INTERRUPT_STAT_REG,(1 << (g_intRouteTbl[cpunum][temp2][i] - 32)),0);
					}
				}				
			}
		}
		interrupt_pending >>= 1;
		temp2++;
	}

#endif
	return irq_index;
//#endif
//    return irq;
}


/*******************************************************************************
 * 
 * 中断响应操作
 * 
 * 		本接口特殊中断操作，本接口存在必要性以及接口语法定义由具体平台相关。
 * 输入：
 * 		arg：特殊中断参数，由具体平台确定
 * 输出：
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 * 备注：
 * 		本接口需要根据具体的硬件平台和外部中断控制器进行实现
 */
int hr3_int_special1(void *arg)
{
	return 0;
}


int hr3FindIntIndex(int coreid)
{
    unsigned long index,mask;
    index = MIPS_LW64(INTERRUPT_PIC_INDEX_REG);
    mask = 0xffUL << (coreid * 8);
    index = index & mask;
    index = index >> (coreid * 8);
    return index;
}


void dump_hr3_int_route()
{
	int ix, jx, kx;
	for (ix = 0; ix < ROUTE_CORE_NUM; ix++)
	{
		printk("++++++++++++++++++++++++++++++++++++++++++++++++\n", ix);
		printk("core: %d\n", ix);		
		for (jx = 0; jx < IO_PIN_NUM; jx++)
		{
			printk("  -------------------------------------------\n");
			printk("  pin:%d\n", jx);			
			for (kx = 0; kx < MAX_REUSABLE_INTSOURCE_NUM; kx++)
			{				
				if (g_intRouteTbl[ix][jx][kx].irqNo != HR3_INT_END)
				{
					printk("    intsrc[%d] : ", kx);
					printk("irq:0x%x ", g_intRouteTbl[ix][jx][kx].irqNo);
#ifdef _HC3020B_ /*ldf 20230921 add:: for hc3020b*/
					printk("preemption_pri:0x%x ", g_intRouteTbl[ix][jx][kx].preemption_pri);
					printk("sub_pri:0x%x ", g_intRouteTbl[ix][jx][kx].sub_pri);
#endif
					printk("%s ", (g_intRouteTbl[ix][jx][kx].isCfg ? "IsCFG" : ""));
					printk("%s\n", (g_intRouteTbl[ix][jx][kx].isEnable ? "ENABLE" : ""));
				}
				/*else
				{
					printk("    intsrc[%d] : NOT USE\n", kx);
				}*/
			}
		}
		printk("  -------------------------------------------\n");
	}
	printk("++++++++++++++++++++++++++++++++++++++++++++++++\n");
	
	for (ix = 0; ix < ROUTE_CORE_NUM; ix++)
	{
		for (jx = 0; jx < IO_PIN_NUM; jx++)
		{
			printk("core[%d]pin[%d] has intsrc: %d \n", ix, jx, g_intRouteTblReuseSign[ix][jx]);
		}
	}
}

void dump_hr3_int_reg(void)
{
	int core = 0;
	int i = 0;
	u64 val; 
	
	printk("++++++++++++++++++++++++++++++++++++++++++++++++\n");
#if 1
	for(i = 0; i < 126; i++)
	{
		val = MIPS_LW64((unsigned int)INTERRUPT_SEL_REG(i));
		printk("pic_int%d_sel_r: 0x%lx\n",i,val);
	}
	printk("  -------------------------------------------\n");
	
	val = MIPS_LW64((unsigned int)INTERRUPT_ENABLE_L_REG);
	printk("pic_int_en_l_r: 0x%lx\n",val);
	val = MIPS_LW64((unsigned int)INTERRUPT_ENABLE_H_REG);
	printk("pic_int_en_h_r: 0x%lx\n",val);
	printk("  -------------------------------------------\n");
	
	val = MIPS_LW64((unsigned int)INTERRUPT_MASK_L_REG);
	printk("pic_int_mask_l_r: 0x%lx\n",val);
	val = MIPS_LW64((unsigned int)INTERRUPT_MASK_H_REG);
	printk("pic_int_mask_h_r: 0x%lx\n",val);
	printk("  -------------------------------------------\n");
	
	val = MIPS_LW64((unsigned int)INTERRUPT_MODE_L_REG);
	printk("pic_int_mode_l_r: 0x%lx\n",val);
	val = MIPS_LW64((unsigned int)INTERRUPT_MODE_H_REG);
	printk("pic_int_mode_h_r: 0x%lx\n",val);
	printk("  -------------------------------------------\n");
	
	val = MIPS_LW64((unsigned int)INTERRUPT_CLR_L_REG);
	printk("pic_int_clr_l_r: 0x%lx\n",val);
	val = MIPS_LW64((unsigned int)INTERRUPT_CLR_H_REG);
	printk("pic_int_clr_h_r: 0x%lx\n",val);
	printk("  -------------------------------------------\n");
	
	val = MIPS_LW64((unsigned int)INTERRUPT_STAT_L_REG);
	printk("pic_int_sta_l_r: 0x%lx\n",val);
	val = MIPS_LW64((unsigned int)INTERRUPT_STAT_H_REG);
	printk("pic_int_sta_h_r: 0x%lx\n",val);
	printk("  -------------------------------------------\n");
	
	val = MIPS_LW64((unsigned int)INTERRUPT_RAWSTA_L_REG);
	printk("pic_int_rawsta_l_r: 0x%lx\n",val);
	val = MIPS_LW64((unsigned int)INTERRUPT_RAWSTA_H_REG);
	printk("pic_int_rawsta_h_r: 0x%lx\n",val);
	printk("  -------------------------------------------\n");
	
	val = MIPS_LW64((unsigned int)INTERRUPT_REQSTA_L_REG);
	printk("pic_int_reqsta_l_r: 0x%lx\n",val);
	val = MIPS_LW64((unsigned int)INTERRUPT_REQSTA_H_REG);
	printk("pic_int_reqsta_h_r: 0x%lx\n",val);
	printk("  -------------------------------------------\n");
	
	val = MIPS_LW64((unsigned int)INTERRUPT_PIC_INDEX_REG);
	printk("pic_int_index_r: 0x%lx\n",val);
	printk("  -------------------------------------------\n");
#endif
	
	for(i = 126; i < 128; i++)
	{
		val = MIPS_LW64((unsigned int)INTERRUPT_SEL_REG(i));
		printk("pic_ipi%d_sel_r: 0x%lx\n",i-126,val);
	}
	printk("  -------------------------------------------\n");
	
	extern void *mailbox_status_regs[];
	extern void *mailbox_enable_regs[];
	extern void *mailbox_set_regs[];
	extern void *mailbox_clear_regs[];
	//CORE_IPI_MAILBOX(core,index)
	
	for(core = 0; core < ROUTE_CORE_NUM; core++)
	{
		val = MIPS_LW64((unsigned int)mailbox_status_regs[core]);
		printk("pic_ipi%d_status_r: 0x%lx\n",core,val);
		printk("  -------------------------------------------\n");
	}
	
	for(core = 0; core < ROUTE_CORE_NUM; core++)
	{
		val = MIPS_LW64((unsigned int)mailbox_enable_regs[core]);
		printk("pic_ipi%d_enable_r: 0x%lx\n",core,val);
		printk("  -------------------------------------------\n");
	}
	
	for(core = 0; core < ROUTE_CORE_NUM; core++)
	{
		val = MIPS_LW64((unsigned int)mailbox_set_regs[core]);
		printk("pic_ipi%d_set_r: 0x%lx\n",core,val);
		printk("  -------------------------------------------\n");
	}
	
	for(core = 0; core < ROUTE_CORE_NUM; core++)
	{
		val = MIPS_LW64((unsigned int)mailbox_clear_regs[core]);
		printk("pic_ipi%d_clear_r: 0x%lx\n",core,val);
		printk("  -------------------------------------------\n");
	}
	
	for(i = 0; i < 4; i++)/*mailbox 0-3*/
	{
		for(core = 0; core < ROUTE_CORE_NUM; core++)
		{
			val = MIPS_LW64((unsigned int)CORE_IPI_MAILBOX(core, i));
			printk("[core%d] pic_mailbox%d_r: 0x%lx\n",core,i,val);
			printk("  -------------------------------------------\n");
		}
	}
	printk("++++++++++++++++++++++++++++++++++++++++++++++++\n");
}
