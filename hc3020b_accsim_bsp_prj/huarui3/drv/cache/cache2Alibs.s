/******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件实现了cache驱动模板
 * 修改：
 * 		2022-04-05，tank，建立代码模板
 */

#define _ASMLANGUAGE
#define ASM_LANGUAGE

/*头文件*/
#include <private/asm.h>
#include <private/arch_mips.h>
#include "cache2Lib.h"
#include "../h/bsp_macro.h"

/*宏定义*/
#define _CACHE_ALIGN_SIZE 32

/* 全局函数定义声明 */
	.globl cache2_Reset		/* low level cache init */
	.globl cache2_enable
	.globl cache2_disable
	.globl cache2_RomTextUpdate	/* cache-text-update */
    .globl cache2_FlushInvalidateAll /* flush entire cache */
	.globl cache2_DCFlushInvalidateAll /* flush entire data cache */
	.globl cache2_DCFlushInvalidate/* flush data cache locations */
	.globl cache2_DCInvalidateAll	/* invalidate entire d cache */
	.globl cache2_DCInvalidate	/* invalidate d cache locations */
	.globl cache2_ICInvalidateAll	/* invalidate i cache locations */
	.globl cache2_ICInvalidate	/* invalidate i cache locations */
	.globl cacheMipsOp_Hit_Invalidate_D
	
	.globl cacheSb1DCFlushInvalidateLines
#if 0
	.globl cache2_DCacheSize	/* data cache size */
	.globl cache2_ICacheSize	/* inst. cache size */
	.globl cache2_SCacheSize	/* secondary cache size */

	.globl cache2_DCacheLineSize    /* data cache line size */
	.globl cache2_ICacheLineSize    /* inst. cache line size */
	.globl cache2_SCacheLineSize    /* secondary cache line size */
#endif
	.globl cache2_VirtPageFlush	/* flush cache on MMU page unmap */
	.globl cache2_Sync		/* cache sync operation */
	.globl sys_clearTlbEntry
	.globl sys_setTlbEntry
    .globl sysWbFlush
#if 0
	.data
	.align	4
cache2_ICacheSize:
	.word	0
cache2_DCacheSize:
	.word	0
cache2_SCacheSize:
	.word	0
cache2_ICacheLineSize:
	.word	0
cache2_DCacheLineSize:
	.word	0
cache2_SCacheLineSize:
	.word	0
#endif
	.text
	.set	reorder

/*******************************************************************************
*
* cache2_Reset - low level initialisation of the hr2 primary caches
*
*/
resetcache:
	mfc0	v0,C0_SR
	HAZARD_CP_READ
	and	v1,v0,SR_BEV
	or	v1,SR_DE
	mtc0	v1,C0_SR


	/* Invalidate icache tags */
	li	a0,K0BASE
	move	a2,t2		# icacheSize
	move	a3,t4		# icacheLineSize
	move	a1,a2
	i10cacheop(a0,a1,a2,a3,Index_Invalidate_I)

	/* Invalidate dcache tags */
	li	a0,K0BASE
	move	a2,t3		# dcacheSize
	move	a3,t5		# dcacheLineSize
	move	a1,a2
	i10cacheop(a0,a1,a2,a3,Index_Writeback_Inv_D)

	/* FIXME assumes unified I & D in scache */
	/* set invalid tag */
	mtc0	zero,C0_TAGLO
	mtc0	zero,C0_TAGHI
	HAZARD_CACHE_TAG


	li	a0,K0BASE
	move	a2,t6
	move	a3,t7
	move	a1,a2
	i10cacheop(a0,a1,a2,a3,Index_Store_Tag_S)

	mtc0	v0,C0_SR
	HAZARD_CP_WRITE

	j	ra
/*-------------------------------------------------------------------------+
|
|   函数：cache2_enable - 使能二级cache
|
|   描述：函数int cache_enable(u32	cache_type	)的汇编实现。
		使能数据cache或指令cache,具体类型由参数cache_type指定
|
| 输入值：cache_type - cache类型
|
| 输出值：无
|
| 返回值：使能成功与否
|
+--------------------------------------------------------------------------*/
	/*.ent cache2_enable*/
	/*添加用户汇编代码*/
cache2_enable:
	mfc0 v0,C0_CONFIG
	HAZARD_CP_READ
	and v0,0xfffffff8
	ori v0,0x3
	mtc0 v0,C0_CONFIG
	HAZARD_CP_WRITE

	j ra
/*	.end cache2_enable*/
/*-------------------------------------------------------------------------+
|
|   函数：cache2_disable - 关闭二级cache
|
|   描述：函数int cache_disable(u32	cache_type	)的汇编实现。
		使能数据cache或指令cache,具体类型由参数cache_type指定
|
| 输入值：cache_type - cache类型
|
| 输出值：无
|
| 返回值：使能成功与否
|
+--------------------------------------------------------------------------*/
	/*.ent cache2_disable*/
	/*添加用户汇编代码*/
cache2_disable:
	mfc0 v0,C0_CONFIG
	HAZARD_CP_READ
	and v0,0xfffffff8
	ori v0,0x2
	mtc0 v0,C0_CONFIG
	HAZARD_CP_WRITE

	j ra
/*	.end cache2_disable*/
/******************************************************************************
* void cache2_RomTextUpdate ()

*/
/*	.ent	cache2_RomTextUpdate*/
cache2_RomTextUpdate:
	/* Save I-cache parameters */
	move	t0,a0
	move	t1,a1

	/* Check for primary data cache */
	blez	a2,99f

	/* Flush-invalidate primary data cache */
	li	a0,K0BASE
	move	a1,a2
	i10cacheop(a0,a1,a2,a3,Index_Writeback_Inv_D)
99:
	/* replace I-cache parameters */
	move	a2,t0
	move	a3,t1

	/* Check for primary instruction cache */
	blez	a0,99f

	/* Invalidate primary instruction cache */
	li	a0,K0BASE
	move	a1,a2
	i10cacheop(a0,a1,a2,a3,Index_Invalidate_I)
99:
	j	ra
/*	.end	cache2_RomTextUpdate*/

/*******************************************************************************
*
* cache2_FlushInvalidateAll - flush entire hr2 cache:icache,dcache,scache
*
* RETURNS: N/A
*

* void cache2_FlushInvalidateAll (void)

*/
/*	.ent	cache2_FlushInvalidateAll*/
cache2_FlushInvalidateAll:

	li	a2,(512<<10)
	li	a3,32
	li	a0,K0BASE
	move	a1,a2
	i10cacheop(a0,a1,a2,a3,Index_Writeback_Inv_S)

	j	ra

/*	.end	cache2_FlushInvalidateAll*/

/*******************************************************************************
*
* cache2_DCFlushInvalidateAll - flush entire hr2 data cache
*
* RETURNS: N/A
*

* void cache2_DCFlushInvalidateAll (void)

*/
/*	.ent	cache2_DCFlushInvalidateAll*/
cache2_DCFlushInvalidateAll:

	li	a2,(4096<<10)
	li	a3,32
	li	a0,K0BASE
	move	a1,a2
	i10cacheop(a0,a1,a2,a3,Index_Writeback_Inv_S)

	j	ra

/*	.end	cache2_DCFlushInvalidateAll*/

/*-------------------------------------------------------------------------+
|
|   函数：cache2_DCFlushInvalidate -- flush指定范围数据cache无效
|
|   描述：该例程flush 数据cache内的指定空间无效。函数原型：
 	void cache2_DCFlushInvalidate
    (
   		baseAddr,		/@ 内存逻辑地址@/
    	byteCount		/@ 指定的内存大小 @/
    )	   
	输入值:baseAddr		内存逻辑地址
|          byteCount	内存大小(单位:字节)
|
| 	输出值：无
|
| 	返回值：无
+--------------------------------------------------------------------------*/
/*	.ent	cache2_DCFlushInvalidate*/
cache2_DCFlushInvalidate:

	li	a2, (4096<<10)
	li	a3, 32
	vcacheop(a0,a1,a2,a3,Hit_Writeback_Inv_S)
	j	ra
/*	.end	cache2_DCFlushInvalidate*/


/*******************************************************************************
*
* cache2_DCInvalidateAll - invalidate entire hr2 data cache
*
* RETURNS: N/A
*

* void cache2_DCInvalidateAll (void)

*/
/*	.ent	cache2_DCInvalidateAll*/
cache2_DCInvalidateAll:

    li	a0,K0BASE
	li	a2,(4096<<10)
	li	a3,32
	move	a1,a2
	i10cacheop(a0,a1,a2,a3,Index_Writeback_Inv_S)

	j	ra

/*	.end	cache2_DCInvalidateAll*/

/*******************************************************************************
*
* cache2_DCInvalidate - invalidate hr2 data cache locations
*
* RETURNS: N/A
*

* void cache2_DCInvalidate
*     (
*     baseAddr,		/@ virtual address @/
*     byteCount		/@ number of bytes to invalidate @/
*     )

*/
	/*.ent	cache2_DCInvalidate*/
cache2_DCInvalidate:

	li	a2,(4096<<10)
	li	a3,32
	vcacheop(a0,a1,a2,a3,Hit_Invalidate_S)

	j	ra
/*	.end	cache2_DCInvalidate*/

/*******************************************************************************
*
* cache2_ICInvalidateAll - invalidate entire hr2 instruction cache
*
* RETURNS: N/A
*

* void cache2_ICInvalidateAll (void)

*/
/*	.ent	cache2_ICInvalidateAll*/
cache2_ICInvalidateAll:
	li	a0,K0BASE
	li	a2,(512<<10)
	li	a3,32
	move	a1,a2
	i10cacheop(a0,a1,a2,a3,Index_Writeback_Inv_S)

	j	ra

/*	.end	cache2_ICInvalidateAll*/

/*-------------------------------------------------------------------------+
|
|   函数：cache2_ICInvalidate -- 使指定范围指令cache无效
|
|   描述：该例程使指令cache内的指定空间为无效。函数原型：
| 	void cache2_ICInvalidate
|   (
|     baseAddr,		/@ virtual address @/
|     byteCount		/@ number of bytes to invalidate @/
|   )  
|	输入值: baseAddr	内存逻辑地址
|           byteCount	内存大小(单位:字节)
|
| 	输出值：无
|
| 	返回值：无
+--------------------------------------------------------------------------*/
/*	.ent	cache2_ICInvalidate*/
cache2_ICInvalidate:
	li	a2,(512<<10)
	li	a3,32
	vcacheop(a0,a1,a2,a3,Hit_Invalidate_S)

	j	ra
/*	.end	cache2_ICInvalidate*/

/**************************************************************************/
/*       .ent    cacheSb1DCFlushInvalidateLines*/
cacheSb1DCFlushInvalidateLines:

	.set noreorder
1:
	subu	a1, a1, 1
	cache	Hit_Writeback_Inv_D, 0(a0)
	bnez	a1, 1b
	addu	a0, a0, _CACHE_ALIGN_SIZE
	.set reorder

	sync
	sync
	j	ra
	nop
/*	.end	cacheSb1DCFlushInvalidateLines*/


	
/*.ent    cacheMipsOp_Hit_Invalidate_D*/
cacheMipsOp_Hit_Invalidate_D:		
	cache	Hit_Invalidate_D, 0(a0)		
	jr	ra			
	nop
	/*.end	cacheMipsOp_Hit_Invalidate_D*/
/**************************************************************************/


/******************************************************************************
*
* cache2_VirtPageFlush - flush one page of virtual addresses from caches
*
* Change ASID, flush the appropriate cache lines from the D- and I-cache,
* and restore the original ASID.
*
* CAVEAT: This routine and the routines it calls MAY be running to clear
* cache for an ASID which is only partially mapped by the MMU. For that
* reason, the caller may want to lock interrupts.
*
* RETURNS: N/A
*
* void cache2_VirtPageFlush (UINT asid, void *vAddr, UINT pageSize);
*/
/*	.ent	cache2_VirtPageFlush*/
cache2_VirtPageFlush:
	/* Save parameters */
	move	t4,a0			/* ASID to flush */
	move	t0,a1			/* beginning VA */
	move	t1,a2			/* length */

	/*
	 * When we change ASIDs, our stack might get unmapped,
	 * so use the stack now to free up some registers for use:
	 *	t0 - virtual base address of page to flush
	 *	t1 - page size
	 *	t2 - original SR
	 *	t3 - original ASID
	 *	t4 - ASID to flush
	 */

	/* lock interrupts */

	mfc0	t2, C0_SR
	HAZARD_CP_READ
	li	t3, ~SR_INT_ENABLE
	and	t3, t2
	mtc0	t3, C0_SR
	HAZARD_INTERRUPT

	/* change the current ASID to context where page is mapped */

	mfc0	t3, C0_TLBHI		/* read current TLBHI */
	HAZARD_CP_READ
	and	t3, 0xff		/* extract ASID field */
	beq	t3, t4, 0f		/* branch if no need to change */
	mtc0	t4, C0_TLBHI		/* Store new EntryHi  */
	HAZARD_TLB
0:
	/* clear the virtual addresses from D- and I-caches */

	li	a2,(512<<10)

	/* Flush-invalidate primary data cache */
	move	a0, t0
	move	a1, t1
	li	a3,32
	vcacheop(a0,a1,a2,a3,Hit_Writeback_Inv_S)


	/* restore the original ASID */
	mtc0	t3, C0_TLBHI		/* Restore old EntryHi  */
	HAZARD_TLB

	mtc0	t2, C0_SR		/* restore interrupts */

	j	ra
/*	.end	cache2_VirtPageFlush*/

/******************************************************************************
*
* cache2_Sync - sync region of memory through all caches
*
* RETURNS: N/A
*
* void cache2_Sync (void *vAddr, UINT pageSize);
*/
/*	.ent	cache2_Sync*/
cache2_Sync:
	/* Save parameters */
	move	t0,a0			/* beginning VA */
	move	t1,a1			/* length */

	/* lock interrupts */

	mfc0	t2, C0_SR
	HAZARD_CP_READ
	li	t3, ~SR_INT_ENABLE
	and	t3, t2
	mtc0	t3, C0_SR
	HAZARD_INTERRUPT

	/*
	 * starting with primary caches, push the memory
	 * block out completely
	 */
	sync

	li	 a2,(512<<10)
	move a0,t0
	move a1,t1
	li	 a3,32
	vcacheop(a0,a1,a2,a3,Hit_Writeback_Inv_S)
1:
	mtc0	t2, C0_SR		/* restore interrupts */

	j	ra
/*	.end	cache2_Sync*/



/*-------------------------------------------------------------------------+
|
|   函数：sys_clearTlbEntry -- 清除TLB（translation lookaside buffer）入口
|
|   描述：该例程通过将虚页号清零来清除指定的TLB入口。函数原型：
| 	void sysClearTlbEntry
|   (
|     int entry
|   )
|	输入值: entry		指定的TLB入口
|
| 	输出值：无
|
| 	返回值：无
+--------------------------------------------------------------------------*/

/*	.ent	sys_clearTlbEntry*/
sys_clearTlbEntry:
	subu	t0, a0, TLB_ENTRIES - 1 /* how many tlb entries are there */
	bgtz	t0, invalidEntry	/* is my index bad ? */
	li	t2,DRAM_CACHE_VIRT_BASE&TLBHI_VPN2MASK
	mtc0	t2,C0_TLBHI		/* zero tlbhi entry */
	mtc0	zero,C0_TLBLO0		/* set valid bit to zero */
	mtc0	zero,C0_TLBLO1		/* set valid bit to zero */
	mtc0	a0,C0_INX		/* set up index for write     */
	tlbwi				/* write indexed tlb entry */
invalidEntry:
	j	ra
/*	.end	sys_clearTlbEntry*/

/*-------------------------------------------------------------------------+
|
|   函数：sys_setTlbEntry -- 设置TLB（translation lookaside buffer）入口
|
|   描述：该例程设置指定的TLB入口。函数原型：
| 	STATUS sysSetTlbEntry
|     (
|     UINT entry,
|     ULONG virtual,
|     ULONG physical,
|     UINT mode		 Bits 31:12 size, bits 5:0 is cache mode/enable
|     )
|	输入值: entry		指定的TLB入口
|
| 	输出值：无
|
| 	返回值：无
+--------------------------------------------------------------------------*/

/*	.ent	sys_setTlbEntry*/
sys_setTlbEntry:
	subu	t0, a0, TLB_ENTRIES - 1 /* how many tlb entries are there */
	bgtz	t0, badEntry		/* is my index bad ? */

	and	t1, a3, ~TLBLO_PAGEMASK_SIZE/* Check for invalid size */
	bgtz	t1, badEntry		/* Invalid size parameter ? */
	and	t0, a3, (TLBLO_PAGEMASK_SIZE & ~TLBLO_MODE)
	sll	t0, 1
	sub	t0, 1

	/* Set the page size mask */
	and	t0, TLBLO_PAGEMASK
	mtc0	t0, C0_PAGEMASK

	/* Set the virtual address */
	and	t0, a1, TLBHI_VPN2MASK	/* ASID not used   */
	dmtc0	t0, C0_TLBHI		/* zero tlbhi entry */

	/* Set physical address, LO0 register */
	srl	t0, a2, 6			/* Physical address */
	and	t0, TLBLO_PFNMASK
	move	t1, a3			/* Caching characteristic */
	and	t1, TLBLO_MODE
	or	t0, t0, t1
	dmtc0	t0, C0_TLBLO0		/* set valid bit to zero */

	/* Set physical address, LO1 register */
	add	t0, a2, a3		/* Next physical address */
	srl	t0, 6
	and	t0, TLBLO_PFNMASK
	or	t0, t0, t1
	dmtc0	t0, C0_TLBLO1		/* set valid bit to zero */

	/* Set the index into the TLB */
	dmtc0	a0,C0_INX

 	/* Write the TLB entries */
	tlbwi				/* write indexed tlb entry */
	li	v0, 0    /* OK */
	b	goodEntry

badEntry:
	li	v0, -1    /* ERROR */
goodEntry:
	j	ra
/*	.end	sys_setTlbEntry*/
/*******************************************************************************
*
* sysWbFlush - flush the write buffer
*
* This routine flushes the write buffers, making certain all subsequent
* memory writes have occurred.  It is used during critical periods only, e.g.,
* after memory-mapped I/O register access.
*
* RETURNS: N/A
*
* sysWbFlush (void)
*
* NOMANUAL
*/
/*	.ent	sysWbFlush*/
sysWbFlush:
	.set noreorder
	nop
	#if 0 
        la      v0,sysWbFlush
        or      v0,DRAM_NONCACHE_VIRT_BASE
        sw      zero,0(v0)
        lw      v0,0(v0)
    #endif
    	sync 
    	nop
        j       ra
	nop
	.set reorder
/*	.end	sysWbFlush*/
