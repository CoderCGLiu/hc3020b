/******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件实现了cache驱动模板
 * 修改：
 * 		2022-04-05，tank，建立代码模板
 */

#include <reworks/types.h>
#include <reworks/cache.h>
#include <private/arch_mips.h>
#include "stdlib.h"
#include "errno.h"

/*
 * 宏定义
 * */
#define ROUND_UP(x, align)	(((int) (x) + (align - 1)) & ~(align - 1))
#define ROUND_DOWN(x, align)	((int)(x) & ~(align - 1))

#define	ENTIRE_CACHE	(__LONG_MAX__ * 2UL + 1UL)
#define S_cacheLib_INVALID_CACHE	((69 << 16)|1)
#define	CACHE_DISABLED		0x00	/* No cache or disabled */

#ifndef _CACHE_ALIGN_SIZE
#define _CACHE_ALIGN_SIZE	16
#endif	/* _CACHE_ALIGN_SIZE */

/*
 * 全局变量
 * */
typedef enum				/* CACHE_TYPE */
{
     instCACHE = INST_CACHE,
    dataCACHE  = DATA_CACHE
} CACHE_TYPE;

typedef	u32	CACHE_MODE;		/* CACHE_MODE */
typedef int 		(*FUNCPTR) ();
typedef void 		(*VOIDFUNCPTR) ();

typedef	struct				/* Cache Routine Pointers */
{
    FUNCPTR	enableRtn;		/* cacheEnable() */
    FUNCPTR	disableRtn;		/* cacheDisable() */
    FUNCPTR	lockRtn;		/* cacheLock() */
    FUNCPTR	unlockRtn;		/* cacheUnlock() */
    FUNCPTR	flushRtn;		/* cacheFlush() */
    FUNCPTR	invalidateRtn;		/* cacheInvalidate() */
    FUNCPTR	clearRtn;		/* cacheClear() */

    FUNCPTR	textUpdateRtn;		/* cacheTextUpdate() */
    FUNCPTR	pipeFlushRtn;		/* cachePipeFlush() */
    FUNCPTR	dmaMallocRtn;		/* cacheDmaMalloc() */
    FUNCPTR	dmaFreeRtn;		/* cacheDmaFree() */
    FUNCPTR	dmaVirtToPhysRtn;	/* virtual-to-Physical Translation */
    FUNCPTR	dmaPhysToVirtRtn;	/* physical-to-Virtual Translation */
} CACHE_LIB;

int reCacheDataEnabled = FALSE;		/* data cache disabled */
int reCacheMmuAvailable = FALSE;		/* MMU support available */
CACHE_MODE reCacheDataMode = CACHE_DISABLED;	/* data cache mode */
FUNCPTR reCacheDmaMallocRtn = NULL;	/* malloc dma memory if MMU */
FUNCPTR reCacheDmaFreeRtn = NULL;		/* free dma memory if MMU */


/* 本模块函数定义声明*/
static void * cache2_malloc(size_t bytes);
static OS_STATUS cache2_free(void * pBuf);
 OS_STATUS cache2_flush(CACHE_TYPE cache, void * pVirtAdrs, size_t bytes);
 OS_STATUS cache2_invalidate(CACHE_TYPE cache, void * pVirtAdrs, size_t bytes);
static void * cache2_physToVirt(void * address);
static void * cache2_virtToPhys(void * address);
static OS_STATUS cache2_textUpdate(void * address, size_t bytes);
static OS_STATUS cache2_pipeFlush(void);

/* 外部函数声明 */
extern int cache2_enable(void);
extern int cache2_disable(void);

extern void sysWbFlush(void);

extern void cache2_DCFlushInvalidate(void * address, size_t byteCount);
extern void cache2_DCFlushInvalidateAll(void);
extern void cache2_ICInvalidate(void * address, size_t byteCount);
extern void cache2_ICInvalidateAll(void);

extern void cache2_FlushInvalidateAll(void);
extern void cache2_DCInvalidate(void * baseAdr, size_t byteCount);
extern void cache2_DCInvalidateAll(void);

extern void	cache2_Sync (void * vAddr, u32 len);
extern void	cache2_VirtPageFlush (u32 asid, void * vAddr, u32 len);

/* 外部声明2级cache管理的对象cacheLib,其在cache.c中定义*/
extern CACHE_LIB cacheLib;

/*-------------------------------------------------------------------------+
|
|   函数：cache2_libInit - 二级cache函数库初始化
|
|   描述：挂接管理二级cache需要的函数指针。
|
| 输入值：inst_cache_mode - 指令cache模式
|         data_cache_mode - 数据cache模式
| 输出值：无
|
| 返回值：OS_OK 	初始化成功。
+--------------------------------------------------------------------------*/
OS_STATUS cache2_libInit
    (
    CACHE_MODE	instMode,	/* instruction cache mode */
    CACHE_MODE	dataMode	/* data cache mode */


    )
    {


    cacheLib.enableRtn = cache2_enable;
    cacheLib.disableRtn = cache2_disable;

    cacheLib.lockRtn = NULL;			/* cacheLock */
    cacheLib.unlockRtn = NULL;			/* cacheUnlock */

    cacheLib.flushRtn = cache2_flush;		/* cacheFlush() */
    cacheLib.pipeFlushRtn = cache2_pipeFlush  ;	/* cachePipeFlush() */
    cacheLib.textUpdateRtn = cache2_textUpdate ;/* cacheTextUpdate() */

    cacheLib.invalidateRtn = cache2_invalidate;/* cacheInvalidate() */
    cacheLib.clearRtn = cache2_invalidate;	/* cacheClear() */

    cacheLib.dmaMallocRtn = (FUNCPTR) cache2_malloc;	/* cacheDmaMalloc() */
    cacheLib.dmaFreeRtn = cache2_free;			/* cacheDmaFree() */


    cacheLib.dmaVirtToPhysRtn = (FUNCPTR) cache2_virtToPhys;
    cacheLib.dmaPhysToVirtRtn = (FUNCPTR) cache2_physToVirt;


    reCacheDataMode	= dataMode;		/* save dataMode for enable */
    reCacheDataEnabled	= TRUE;			/* d-cache is currently on */
    reCacheMmuAvailable	= TRUE;			/* mmu support is provided */


    return (OS_OK);
    }


/*-------------------------------------------------------------------------+
|
|   函数：cache2_malloc - 分配非缓存的缓冲区
|
|   描述：该例程尝试分配一块cache-safe内存，这块内存不会发生缓存一致性问题。
|		在使用该例程时，要记住分配的内存指针，在必要的时候释放内存。
| 输入值：bytes - 内存大小

| 输出值：无
|
| 返回值：指向分配的内存的指针。如果分配失败返回NULL
+--------------------------------------------------------------------------*/

static void * cache2_malloc
    (
    size_t bytes
    )
    {
    void * pDmaBuffer;

	int	allocBytes;
	void  * pBuffer;

        /* Round up the allocation size so that we can store a "back pointer"
         * to the allocated buffer, align the buffer on a cache line boundary
         * and pad the buffer to a cache line boundary.
         * sizeof(void *) 		for "back pointer"
         * _CACHE_ALIGN_SIZE-1	for cache line alignment
         * _CACHE_ALIGN_SIZE-1	for cache line padding
         */
        allocBytes = sizeof (void *) +
	  	    (_CACHE_ALIGN_SIZE - 1) +
		    bytes +
		    (_CACHE_ALIGN_SIZE - 1);

        if ((pBuffer = (void *)malloc (allocBytes)) == NULL)
	    return (NULL);

        /* Flush any data that may be still sitting in the cache */
        cache2_DCFlushInvalidate (pBuffer, allocBytes);

	pDmaBuffer = pBuffer;

	/* allocate space for the back pointer */
	pDmaBuffer = (void *)((int)pDmaBuffer + sizeof (void *));

	/* Now align to a cache line boundary */
	pDmaBuffer = (void *)ROUND_UP (pDmaBuffer,_CACHE_ALIGN_SIZE);

	/* Store "back pointer" in previous cache line using CACHED location */
	*(((void **)pDmaBuffer)-1) = pBuffer;

	return ((void *)K0_TO_K1(pDmaBuffer));
    }


/*-------------------------------------------------------------------------+
|
|   函数：cache2_free - 释放缓冲区
|
|   描述：该例程释放cacheMalloc ()分配的内存。
| 输入值：指向需要释放的内存的指针。

| 输出值：无
|
| 返回值：OS_OK 	成功
		OS_ERROR 	失败
+--------------------------------------------------------------------------*/
static OS_STATUS cache2_free
    (
    void * pBuf
    )
    {
    void      * pCacheBuffer;

	pCacheBuffer = (void *)K1_TO_K0(pBuf);
	pCacheBuffer = (void *)((int)pCacheBuffer - sizeof (void *));
	free (*(void **)pCacheBuffer);
    return (OS_OK);
    }

/*-------------------------------------------------------------------------+
|
|   函数：cache2_flush - 刷新缓存
|
|   描述：该例程刷新指定的的cache。
| 输入值： CACHE_TYPE	 cache	cache类型
		  pVirtAdrs			逻辑内存地址
		  bytes 			内存大小

| 输出值：无
|
| 返回值：OS_OK 	成功
		OS_ERROR 	如果cache类型无效或者不支持cache控制
+--------------------------------------------------------------------------*/
 OS_STATUS	cache2_flush
    (
    CACHE_TYPE	cache,			/* Cache to Invalidate */
    void *	pVirtAdrs,		/* Virtual Address */
    size_t	bytes 			/* Number of Bytes to Invalidate */
    )
    {
    if (IS_KSEG1(pVirtAdrs))
	return(OS_OK);
    switch (cache)
	{
	case dataCACHE:
	    if (bytes == ENTIRE_CACHE)
		cache2_DCFlushInvalidateAll ();
	    else
	    	cache2_DCFlushInvalidate (pVirtAdrs, bytes);
	    break;
	default:
	    errno = S_cacheLib_INVALID_CACHE;
	    return (OS_ERROR);
	    break;
        }

    return (OS_OK);
    }

/*-------------------------------------------------------------------------+
|
|   函数：cache2_invalidate -- 使指定范围cache无效
|
|   描述：该例程使数据cache或指令cache内的指定空间为无效。
| 输入值:cache_type      - cache类型
|          pVirtAdrs            - 内存逻辑地址
|          bytes            - 内存大小(单位:字节)
|
| 输出值：无
|
| 返回值：OS_OK 	成功
		OS_ERROR 	如果cache类型无效或者不支持cache控制
+--------------------------------------------------------------------------*/
 OS_STATUS	cache2_invalidate
    (
    CACHE_TYPE	cache,			/* Cache to Invalidate */
    void *	pVirtAdrs,		/* Virtual Address */
    size_t	bytes 			/* Number of Bytes to Invalidate */
    )
    {
    if (IS_KSEG1(pVirtAdrs))
	return(OS_OK);
    switch (cache)
	{
	case dataCACHE:
	    if (bytes == ENTIRE_CACHE)
		cache2_DCInvalidateAll ();
	    else
		cache2_DCInvalidate (pVirtAdrs, bytes);
	    break;
	case instCACHE:
	    if (bytes == ENTIRE_CACHE)
		cache2_ICInvalidateAll ();
	    else
	    	cache2_ICInvalidate (pVirtAdrs, bytes);
	    break;
	default:
	    errno = S_cacheLib_INVALID_CACHE;
	    return (OS_ERROR);
	    break;
        }

    return (OS_OK);
    }

/*-------------------------------------------------------------------------+
|
|   函数：cache2_virtToPhys -- 将逻辑地址转换为物理地址
|
|   描述：该例程将逻辑地址转换为物理地址。
| 输入值:
|          address            - 内存逻辑地址
|
| 输出值：无
|
| 返回值：物理地址
+--------------------------------------------------------------------------*/

static void * cache2_virtToPhys
    (
    void * address                      /* Virtual address */
    )
    {
    return ((void *) K0_TO_PHYS(address));
    }

/*-------------------------------------------------------------------------+
|
|   函数：cache2_physToVirt -- 将物理地址转换为逻辑地址
|
|   描述：该例程将物理地址转换为逻辑地址。
| 输入值:
|          address            - 物理地址
|
| 输出值：无
|
| 返回值：内存逻辑地址
+--------------------------------------------------------------------------*/

static void * cache2_physToVirt
    (
    void * address                      /* Physical address */
    )
    {
    return ((void *) PHYS_TO_K0(address));
    }

/*-------------------------------------------------------------------------+
|
|   函数：cache2_textUpdate -- 无效指定的text段
|
|   描述：该例程无效指定的text段，以便更新正确的text段。
| 输入值:
|          address          物理地址
|			bytes			内存大小
| 输出值：无
|
| 返回值：OS_OK 	成功
		OS_ERROR 	失败
+--------------------------------------------------------------------------*/
static OS_STATUS cache2_textUpdate 
    (
    void * address,                     /* Physical address */
    size_t bytes 			/* bytes to invalidate */
    )
    {
    if ((bytes != ENTIRE_CACHE) &&
	((address == NULL) || (bytes == 0) || IS_KSEG1(address)))
	return (OS_OK);

    if (cache2_flush (dataCACHE, address, bytes) != OS_OK)
	return (OS_ERROR);
    return (cache2_invalidate (instCACHE, address, bytes));
    }


/*-------------------------------------------------------------------------+
|
|   函数：cache2_pipeFlush -- 刷新hr2写缓存区
|
|   描述：该例程强制处理器将缓存区内容写入RAM，保持一致性。
| 输入值: 无

| 输出值：无
|
| 返回值：OS_OK 	成功
		OS_ERROR 	失败
+--------------------------------------------------------------------------*/

static OS_STATUS cache2_pipeFlush   (void)
    {
    sysWbFlush ();
    return (OS_OK);
    }



int cacheMipsDCInvalidate
    (
    void	*baseAddr,
    int size
    )
    {
    char	*addr;

    for (addr = (char *)baseAddr; addr < (char *)baseAddr + size; addr += 32)
	cacheMipsOp_Hit_Invalidate_D(addr);
    asm("sync");

    return (0);
    }


void cache_flush_invalidate(u32 addr,u32 size)
{
	if(size < 32)
		size = 32;
	cacheSb1DCFlushInvalidateLines(addr,size/32);
	return;
}
