/******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件实现了cache驱动模板
 * 修改：
 * 		2022-04-05，tank，建立代码模板
 */

#ifndef _CACHE_HR2_LIB_H_
#define _CACHE_HR2_LIB_H_

#ifdef __cplusplus
extern "C" {
#endif
	
#ifndef	_ASMLANGUAGE

#define	_INSTRUCTION_CACHE 	0	/* Instruction Cache(s) */
#define	_DATA_CACHE		1		/* Data Cache(s) */

#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#undef Index_Invalidate_I               /*#0x0          0       0 */
#undef Index_Writeback_Inv_D            /*#0x1          0       1 */
#undef Index_Invalidate_SI              /*#0x2          0       2 */
#undef Index_Writeback_Inv_SD           /*#0x3          0       3 */
#undef Index_Load_Tag_I                 /*#0x4          1       0 */
#undef Index_Load_Tag_D                 /*#0x5          1       1 */
#undef Index_Load_Tag_SI                /*#0x6          1       2 */
#undef Index_Load_Tag_SD                /*#0x7          1       3 */
#undef Index_Store_Tag_I                /*#0x8          2       0 */
#undef Index_Store_Tag_D                /*#0x9          2       1 */
#undef Index_Store_Tag_SI               /*#0xA          2       2 */
#undef Index_Store_Tag_SD               /*#0xB          2       3 */
#undef Create_Dirty_Exc_D               /*#0xD          3       1 */
#undef Create_Dirty_Exc_SD              /*#0xF          3       3 */
#undef Hit_Invalidate_I                 /*#0x10         4       0 */
#undef Hit_Invalidate_D                 /*#0x11         4       1 */
#undef Hit_Invalidate_SI                /*#0x12         4       2 */
#undef Hit_Invalidate_SD                /*#0x13         4       3 */
#undef Hit_Writeback_Inv_D              /*#0x15         5       1 */
#undef Hit_Writeback_Inv_SD             /*#0x17         5       3 */
#undef Fill_I                           /*#0x14         5       0 */
#undef Hit_Writeback_D                  /* #0x19        6       1 */
#undef Hit_Writeback_SD                 /*#0x1B         6       3 */
#undef Hit_Writeback_I                  /*#0x18         6       0 */
#undef Lock_I				            /*#0x1C	        7	    0 */
#undef Lock_D				            /*#0x1D	        7	    1 */
#undef Hit_Set_Virtual_SI               /*#0x1E         7       2 */
#undef Hit_Set_Virtual_SD               /*#0x1F         7       3 */

/*
 * HR2 defined Cache operations
 */
#define	Index_Invalidate_I              0x0         /* 0       0 */
#define	Index_Writeback_Inv_D           0x1         /* 0       1 */
#define	Hit_Invalidate_I                0x10        /* 4       0 */
#define	Hit_Invalidate_D                0x11        /* 4       1 */
#define	Hit_Writeback_Inv_D             0x15        /* 5       1 */

#define	Index_Writeback_Inv_S           0x3         /* 0       3 */
#define	Hit_Invalidate_S                0x13        /* 4       3 */
#define	Hit_Writeback_Inv_S             0x17        /* 5       3 */

#define Index_Store_Tag_S               0xB         /* 2       3 */


#define _mincache(size, maxsize) \
	bltu	size,maxsize,9f     ;\
	move	size,maxsize        ;\
9:

#define _align(minaddr, maxaddr, linesize) \
	.set noat             ; \
	subu	AT,linesize,1 ;	\
	not	AT                ; \
	and	minaddr,AT        ; \
	addu	maxaddr,-1    ; \
	and	maxaddr,AT        ; \
	.set at

/* general operations */
#define doop1(op1) \
	cache	op1,0(a0)	; \
	HAZARD_CACHE
#define doop2(op1, op2) \
	cache	op1,0(a0)	; \
	HAZARD_CACHE		; \
	cache	op2,0(a0)	; \
	HAZARD_CACHE

/* Loop operation for 4-way set */
/* associative index operations   */
#define doop10(op1) \
	cache	op1,0(a0)	;\
	HAZARD_CACHE		;\
	cache	op1,1(a0)	;\
	HAZARD_CACHE        ;\
	cache	op1,2(a0)	;\
	HAZARD_CACHE		;\
	cache	op1,3(a0)	;\
	HAZARD_CACHE

/* specials for cache initialisation            */
/* All cache initialization is done by index ops*/
/* 4-way set associativity is considered here */
#define doop1lw(op1) \
	lw	zero,0(a0)
#define doop1lw1(op1) \
	cache	op1,0(a0)  ; \
	HAZARD_CACHE	   ; \
	cache	op1,1(a0)  ; \
	HAZARD_CACHE	   ; \
	cache	op1,2(a0)  ; \
	HAZARD_CACHE	   ; \
	cache   op1,3(a0)  ; \
	lw	zero,0(a0)     ; \
	cache	op1,0(a0)  ; \
	HAZARD_CACHE	   ; \
	cache	op1,1(a0)  ; \
	HAZARD_CACHE       ; \
	cache	op1,2(a0)  ; \
	HAZARD_CACHE	   ; \
	cache	op1,3(a0)  ; \
	HAZARD_CACHE
#define doop121(op1,op2) \
	cache	op1,0(a0)  ; \
	HAZARD_CACHE	   ; \
	cache	op1,1(a0)  ; \
	HAZARD_CACHE	   ; \
	cache	op1,2(a0)  ; \
	HAZARD_CACHE	   ; \
	cache	op1,3(a0)  ; \
	HAZARD_CACHE	   ; \
	cache	op2,0(a0)  ; \
	HAZARD_CACHE	   ; \
	cache	op2,1(a0)  ; \
	HAZARD_CACHE	   ; \
	cache	op2,2(a0)  ; \
	HAZARD_CACHE	   ; \
	cache	op2,3(a0)  ; \
	HAZARD_CACHE	   ; \
	cache	op1,0(a0)  ; \
	HAZARD_CACHE	   ; \
	cache	op1,1(a0)  ; \
	HAZARD_CACHE       ; \
	cache	op1,2(a0)  ; \
	HAZARD_CACHE	   ; \
	cache	op1,3(a0)  ; \
	HAZARD_CACHE


#define _oploopn(minaddr, maxaddr, linesize, tag, ops) \
	.set	noreorder ;		\
10: 	doop##tag##ops ;	\
	bne     minaddr,maxaddr,10b ;	\
	add   	minaddr,linesize ;	\
	.set	reorder

/* finally the cache operation macros */
#define vcacheopn(kva, n, cacheSize, cacheLineSize, tag, ops) \
 	blez	n,11f ;			\
	addu	n,kva ;			\
	_align(kva, n, cacheLineSize) ; \
	_oploopn(kva, n, cacheLineSize, tag, ops) ; \
11:

#define icacheopn(kva, n, cacheSize, cacheLineSize, tag, ops) \
	_mincache(n, cacheSize);	\
 	blez	n,11f ;			\
	addu	n,kva ;			\
	_align(kva, n, cacheLineSize) ; \
	_oploopn(kva, n, cacheLineSize, tag, ops) ; \
11:

/* Cache macro for 4-way set associative cache index operations */
#define i10cacheopn(kva, n, cacheSize, cacheLineSize, tag, ops) \
	srl	cacheSize,2	; \
	_mincache(n, cacheSize)	; \
	blez	n,11f		; \
	addu	n,kva		; \
	_align(kva, n, cacheLineSize); \
	_oploopn(kva, n, cacheLineSize, tag, ops); \
11:

#define vcacheop(kva, n, cacheSize, cacheLineSize, op) \
	vcacheopn(kva, n, cacheSize, cacheLineSize, 1, (op))

#define icacheop(kva, n, cacheSize, cacheLineSize, op) \
	icacheopn(kva, n, cacheSize, cacheLineSize, 1, (op))

#define i10cacheop(kva, n, cacheSize, cacheLineSize, op) \
	i10cacheopn(kva, n, cacheSize, cacheLineSize, 10, (op))

#endif
