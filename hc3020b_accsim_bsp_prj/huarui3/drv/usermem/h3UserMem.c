#include <stdlib.h>
#include "tickLib.h" 
#include "stdio.h"
#include "string.h"
#include "tool/hr3SysTools.h"
#include "hr3UserMem.h"
#include "spinLockLib.h"

int hrUserMemSendBufInit(unsigned int initType,
		void * sendBuf0, unsigned int sendBuf0Size,
		void * sendBuf1, unsigned int sendBuf1Size,
		void * sendBuf2, unsigned int sendBuf2Size,
		void * sendBuf3, unsigned int sendBuf3Size);
int hrUserMemRecvBufInit(unsigned int initType,
		void * recvBuf0, unsigned int recvBuf0Size,
		void * recvBuf1, unsigned int recvBuf1Size,
		void * recvBuf2, unsigned int recvBuf2Size,
		void * recvBuf3, unsigned int recvBuf3Size);

static unsigned int s_userMemSendBufCount = 0; /* 记录初始化发送缓存的个数*/
static unsigned int s_userMemRecvBufCount = 0; /* 记录初始化发送缓存的个数*/

/* 在DDR用户空间定义USER_MEM_SEND_BUF_MAX_COUNT个发送缓存 */
static void * s_userMemSendBuf[USER_MEM_SEND_BUF_MAX_COUNT] = {NULL};

/* 在DDR用户空间定义USER_MEM_SEND_BUF_MAX_COUNT个发送缓存的容量 */
static unsigned int s_userMemSendBufSize[USER_MEM_SEND_BUF_MAX_COUNT] = {0};

/* 
 * 为DDR用户空间的USER_MEM_SEND_BUF_MAX_COUNT个发送缓存定义USER_MEM_SEND_BUF_MAX_COUNT个写指针，
 * 用以标示USER_MEM_SEND_BUF_MAX_COUNT个缓存的当前可写地址 
 */
static unsigned int s_userMemSendBufWrPtr[USER_MEM_SEND_BUF_MAX_COUNT] = {0};

/* 
 * 为DDR用户空间的USER_MEM_SEND_BUF_MAX_COUNT个发送缓存定义USER_MEM_SEND_BUF_MAX_COUNT个spinLock指针，
 * 用以实现USER_MEM_SEND_BUF_MAX_COUNT块缓存申请空间时的互斥 
 */
static void * s_pSendSpinLock[USER_MEM_SEND_BUF_MAX_COUNT] = {NULL};

/* 在DDR用户空间定义USER_MEM_RECV_BUF_MAX_COUNT个接收缓存 */
static void * s_userMemRecvBuf[USER_MEM_RECV_BUF_MAX_COUNT] = {NULL};

/* 在DDR用户空间定义USER_MEM_RECV_BUF_MAX_COUNT个接收缓存的容量 */
static unsigned int s_userMemRecvBufSize[USER_MEM_RECV_BUF_MAX_COUNT] = {0};

/* 
 * 为DDR用户空间的USER_MEM_RECV_BUF_MAX_COUNT个发送缓存定义USER_MEM_RECV_BUF_MAX_COUNT个写指针，
 * 用以标示USER_MEM_RECV_BUF_MAX_COUNT个缓存的当前可写地址 
 */
static unsigned int s_userMemRecvBufWrPtr[USER_MEM_RECV_BUF_MAX_COUNT] = {0};

/* 
 * 为DDR用户空间的USER_MEM_RECV_BUF_MAX_COUNT个发送缓存定义USER_MEM_RECV_BUF_MAX_COUNT个spinLock指针，
 * 用以实现USER_MEM_RECV_BUF_MAX_COUNT块缓存申请空间时的互斥 
 */
static void * s_pRecvSpinLock[USER_MEM_RECV_BUF_MAX_COUNT] = {NULL};

int  hrUserMemInit (VOID)
{	
#if 1
	unsigned int i = 0;
//	HRUSERMEM *pDrvCtrl = NULL;
	
//	pDrvCtrl = (HRUSERMEM*)malloc(sizeof(HRUSERMEM));
//	if (pDrvCtrl == NULL)
//		return;
//
//	bzero ((char *)pDrvCtrl, sizeof(HRUSERMEM));
//	pDev->pDrvCtrl = pDrvCtrl;
//	pDrvCtrl->pDev = pDev;
	
#endif	
	/* 初始化发送缓存 */
	hrUserMemSendBufInit(0,(void *)USER_MEM_SEND_BUF0_BASE_ADDR,USER_MEM_SEND_BUF0_SIZE,
			(void *)USER_MEM_SEND_BUF1_BASE_ADDR,USER_MEM_SEND_BUF1_SIZE,
			(void *)USER_MEM_SEND_BUF2_BASE_ADDR,USER_MEM_SEND_BUF2_SIZE,
			(void *)USER_MEM_SEND_BUF3_BASE_ADDR,USER_MEM_SEND_BUF3_SIZE);
	/* 初始化接收缓存 */
	hrUserMemRecvBufInit(0,(void *)USER_MEM_RECV_BUF0_BASE_ADDR,USER_MEM_RECV_BUF0_SIZE,
			(void *)USER_MEM_RECV_BUF1_BASE_ADDR,USER_MEM_RECV_BUF1_SIZE,
			(void *)USER_MEM_RECV_BUF2_BASE_ADDR,USER_MEM_RECV_BUF2_SIZE,
			(void *)USER_MEM_RECV_BUF3_BASE_ADDR,USER_MEM_RECV_BUF3_SIZE);
	
	for(i = 0; i < USER_MEM_SEND_BUF_MAX_COUNT; i ++)
	{
		spinLockTaskInit(s_pSendSpinLock[i], 0);
		if(s_pSendSpinLock[i] == NULL)
		{
			printk("用户空间发送缓存初始化时spinLock创建失败！\r\n");
			return -1;
		}
	}
	for(i = 0; i < USER_MEM_RECV_BUF_MAX_COUNT; i ++)
	{
		spinLockTaskInit(s_pRecvSpinLock[i], 0);
		if(s_pRecvSpinLock[i] == NULL)
		{
			printk("用户空间接收缓存初始化时spinLock创建失败！\r\n");
			return -1;
		}
	}
	
	return 0;
}

int hrUserMemSendBufInit(unsigned int initType,		
								void * sendBuf0, unsigned int sendBuf0Size,
								void * sendBuf1, unsigned int sendBuf1Size,
								void * sendBuf2, unsigned int sendBuf2Size,
								void * sendBuf3, unsigned int sendBuf3Size)
{
	
	if(sendBuf1 != 0)
		if((unsigned int)sendBuf0 + sendBuf0Size > (unsigned int)sendBuf1)
		{
			printf("[DSP%d]发送缓存边界设置错误！\n",sysCpuGetID());
			return ERROR;
		}
	if(sendBuf2 != 0)
		if((unsigned int)sendBuf1 + sendBuf1Size > (unsigned int)sendBuf2)
		{
			printf("[DSP%d]发送缓存边界设置错误！\n",sysCpuGetID());
			return ERROR;
		}
	if(sendBuf3 != 0)
		if((unsigned int)sendBuf2 + sendBuf2Size > (unsigned int)sendBuf3)
		{
			printf("[DSP%d]发送缓存边界设置错误！\n",sysCpuGetID());
			return ERROR;
		}
	/* 重新建立发送缓存 */
	if(initType == 0)
		s_userMemSendBufCount = 0; 

	if(sendBuf0 == 0)
		s_userMemSendBuf[s_userMemSendBufCount] = 0;
	else
	{
		if(s_userMemSendBufCount >= USER_MEM_SEND_BUF_MAX_COUNT - 1)
		{
			printf("[DSP%d]初始化的缓存个数错误！\n",sysCpuGetID());
			return ERROR;
		}
		s_userMemSendBuf[s_userMemSendBufCount] = sendBuf0;
		s_userMemSendBufSize[s_userMemSendBufCount] = sendBuf0Size;
		s_userMemSendBufWrPtr[s_userMemSendBufCount] = 0;/* 将写指针重新置零 */
		s_userMemSendBufCount = s_userMemSendBufCount + 1;
		
	}
	
	if(sendBuf1 == 0)
		s_userMemSendBuf[s_userMemSendBufCount] = 0;
	else
	{
		s_userMemSendBuf[s_userMemSendBufCount] = sendBuf1;
		s_userMemSendBufSize[s_userMemSendBufCount] = sendBuf1Size;
		s_userMemSendBufWrPtr[s_userMemSendBufCount] = 0;/* 将写指针重新置零 */
		s_userMemSendBufCount = s_userMemSendBufCount + 1;
		
	}

	if(sendBuf2 == 0)
		s_userMemSendBuf[s_userMemSendBufCount] = 0;
	else
	{
		s_userMemSendBuf[s_userMemSendBufCount] = sendBuf2;
		s_userMemSendBufSize[s_userMemSendBufCount] = sendBuf2Size;
		s_userMemSendBufWrPtr[s_userMemSendBufCount] = 0;/* 将写指针重新置零 */
		s_userMemSendBufCount = s_userMemSendBufCount + 1;
		
	}
	
	if(sendBuf3 == 0)
		s_userMemSendBuf[s_userMemSendBufCount] = 0;
	else
	{
		s_userMemSendBuf[s_userMemSendBufCount] = sendBuf3;
		s_userMemSendBufSize[s_userMemSendBufCount] = sendBuf3Size;
		s_userMemSendBufWrPtr[s_userMemSendBufCount] = 0;/* 将写指针重新置零 */
		s_userMemSendBufCount = s_userMemSendBufCount + 1;
		
	}
	
	return OK;
}

int hrUserMemRecvBufInit(unsigned int initType,	
								void * recvBuf0, unsigned int recvBuf0Size,
								void * recvBuf1, unsigned int recvBuf1Size,
								void * recvBuf2, unsigned int recvBuf2Size,
								void * recvBuf3, unsigned int recvBuf3Size)
{
	if(recvBuf1 != 0)
		if((unsigned int)recvBuf0 + recvBuf0Size > (unsigned int)recvBuf1)
		{
			printf("[DSP%d]接收缓存边界设置错误！\n",sysCpuGetID());
			return ERROR;
		}
	if(recvBuf2 != 0)
		if((unsigned int)recvBuf1 + recvBuf1Size > (unsigned int)recvBuf2)
		{
			printf("[DSP%d]接收缓存边界设置错误！\n",sysCpuGetID());
			return ERROR;
		}
	if(recvBuf3 != 0)
		if((unsigned int)recvBuf2 + recvBuf2Size > (unsigned int)recvBuf3)
		{
			printf("[DSP%d]接收缓存边界设置错误！\n",sysCpuGetID());
			return ERROR;
		}
	/* 重新建立接收缓存 */
	if(initType == 0)
		s_userMemRecvBufCount = 0;
	
	if(recvBuf0 == 0)
		s_userMemRecvBuf[s_userMemRecvBufCount] = 0;
	else
	{
		s_userMemRecvBuf[s_userMemRecvBufCount] = recvBuf0;
		s_userMemRecvBufSize[s_userMemRecvBufCount] = recvBuf0Size;
		s_userMemSendBufWrPtr[s_userMemRecvBufCount] = 0;/* 将读指针重新置零 */
		s_userMemRecvBufCount = s_userMemRecvBufCount + 1;	
	}
	
	if(recvBuf1 == 0)
		s_userMemRecvBuf[s_userMemRecvBufCount] = 0;
	else
	{
		s_userMemRecvBuf[s_userMemRecvBufCount] = recvBuf1;
		s_userMemRecvBufSize[s_userMemRecvBufCount] = recvBuf1Size;
		s_userMemSendBufWrPtr[s_userMemRecvBufCount] = 0;/* 将读指针重新置零 */
		s_userMemRecvBufCount = s_userMemRecvBufCount + 1;
	}

	if(recvBuf2 == 0)
		s_userMemRecvBuf[s_userMemRecvBufCount] = 0;
	else
	{
		s_userMemRecvBuf[s_userMemRecvBufCount] = recvBuf2;
		s_userMemRecvBufSize[s_userMemRecvBufCount] = recvBuf2Size;
		s_userMemSendBufWrPtr[s_userMemRecvBufCount] = 0;/* 将读指针重新置零 */
		s_userMemRecvBufCount = s_userMemRecvBufCount + 1;
	}
	
	if(recvBuf3 == 0)
		s_userMemRecvBuf[s_userMemRecvBufCount] = 0;
	else
	{
		s_userMemRecvBuf[s_userMemRecvBufCount] = recvBuf3;
		s_userMemRecvBufSize[s_userMemRecvBufCount] = recvBuf3Size;
		s_userMemSendBufWrPtr[s_userMemRecvBufCount] = 0;/* 将读指针重新置零 */
		s_userMemRecvBufCount = s_userMemRecvBufCount + 1;
	}
	
	return OK;
}

int hrUserMemGetSendBufInfo()
{
	unsigned int i = 0;
	
	printf("发送缓存共有%u块！\n",s_userMemSendBufCount);
	
	for(i = 0; i < s_userMemSendBufCount; i ++)
	{
		if(s_userMemSendBuf[i] == 0)
			printf("发送缓存%u不可用！\n",i);
		else
		{
			printf("发送缓存%u基地址：0x%lx\n",i,(ULONG)s_userMemSendBuf[i]);
			printf("发送缓存%u容量：0x%lx\n",i,(ULONG)s_userMemSendBufSize[i]);
		}
	}
	
	return OK;
}
int hrUserMemGetRecvBufInfo()
{
	unsigned int i = 0;
	
	printf("接收缓存共有%u块！\n",s_userMemRecvBufCount);
	
	for(i = 0; i < s_userMemRecvBufCount; i ++)
	{
		if(s_userMemRecvBuf[i] == 0)
			printf("接收缓存%u不可用！\n",i);
		else
		{
			printf("接收缓存%u基地址：0x%lx\n",i,(ULONG)s_userMemRecvBuf[i]);
			printf("接收缓存%u容量：0x%lx\n",i,(ULONG)s_userMemRecvBufSize[i]);
		}
	}
	
	return OK;	
}

/* 根据要对齐的字节数生成新的长度len */
unsigned int userMemAlignLen(unsigned int alignment,unsigned int nBytes)
{
	unsigned int mask = 0;
	unsigned int lenAlign = 0;
	
	lenAlign = nBytes;
	mask = alignment -1;
	lenAlign += mask;
	lenAlign &= (~mask);
	
	return lenAlign;
}

/*
 * 检查用户DDR空间申请API函数的参数，该函数为API函数服务
 */
int hrMemChkParams(void * buf, unsigned int nBytes)
{	
	unsigned long tmp = 0;
	
//	if((unsigned long)buf < 0xffffffff80000000)
	if((((unsigned long)buf + nBytes) > 0xffffffff80000000) ||  ((unsigned long)buf + nBytes < (unsigned long)buf))
	{
		printf("[DSP%d]申请用户DDR空间时，参数buf与空间大小配置错误！\n",sysCpuGetID());
		return ERROR;		
	}
	
//	if((unsigned long)buf >= (unsigned long)0xffffffffd0000000)
	if((((unsigned long)buf + nBytes) > (unsigned long)0xffffffffffff8000) ||  ((unsigned long)buf + nBytes < (unsigned long)buf))
	{
		printf("[DSP%d]申请用户DDR空间时，参数buf与空间大小配置错误！\n",sysCpuGetID());
		return ERROR;		
	}
	
	tmp = hrKmToPhys((void *)buf);
	
	if(tmp < 0x10000000/*0x02200000*/)
	{
		printf("[DSP%d]申请用户DDR空间时，参数buf错误(不能在操作系统空间)！\n",sysCpuGetID());
		return ERROR;
	}
	
	return OK;
}
void * hrUserMemAllocSendBuf(unsigned int sendBufIndex, unsigned int align, unsigned int nBytes)
{
	unsigned int len = 0;
	unsigned int wrPtr = 0;
	void * userMemSendBuf = NULL;
	
	if(nBytes == 0) 
	{
		printf("[DSP%d]申请用户空间内存时参数nBytes错误！\n",sysCpuGetID());
		return NULL;
	}

	if(sendBufIndex > s_userMemSendBufCount - 1)
	{
		printf("[DSP%d]申请用户空间内存时参数sendBufIndex错误！\n",sysCpuGetID());
		return NULL;
	}
	
	if(nBytes > s_userMemSendBufSize[sendBufIndex])
	{
		printf("[DSP%d]申请用户空间超过Buf容量！\n",sysCpuGetID());
		return NULL;
	}
	
	if(align%4 != 0)
	{
		printf("[DSP%d]申请用户空间内存时参数align必须是4的倍数！\n",sysCpuGetID());
		return NULL;
	}
	
	len = userMemAlignLen(/*USER_MEM_ALLOC_ALIGN*/align,nBytes);	
	if(len >= USER_MEM_ALLOC_MAX_LEN) 
	{
		printf("[DSP%d]申请长度大于0x%x，实际申请长度为0x%x\n",sysCpuGetID(),USER_MEM_ALLOC_MAX_LEN,USER_MEM_ALLOC_MAX_LEN);
		len = USER_MEM_ALLOC_MAX_LEN;
	}
	
	spinLockTaskTake(s_pSendSpinLock[sendBufIndex]);
	wrPtr = s_userMemSendBufWrPtr[sendBufIndex];

	if ((wrPtr + len) > s_userMemSendBufSize[sendBufIndex]) 
	{
		wrPtr = 0;
		s_userMemSendBufWrPtr[sendBufIndex] = len;
	} 
	else 
	{
		s_userMemSendBufWrPtr[sendBufIndex] = wrPtr + len;
	}

	spinLockTaskGive(s_pSendSpinLock[sendBufIndex]);

	return (void *) ((unsigned int) s_userMemSendBuf[sendBufIndex] + wrPtr);

}
void * hrUserMemAllocRecvBuf(unsigned int recvBufIndex, unsigned int align, unsigned int nBytes)
{
	unsigned int len = 0;
	unsigned int wrPtr = 0;
	void * userMemRecvBuf = NULL;
	
	if(nBytes == 0) 
	{
		printf("[DSP%d]申请用户空间内存时参数nBytes错误！\n",sysCpuGetID());
		return NULL;
	}

	if(recvBufIndex > s_userMemRecvBufCount - 1)
	{
		printf("[DSP%d]申请用户空间内存时参数recvBufIndex错误！\n",sysCpuGetID());
		return NULL;
	}
	
	if(nBytes > s_userMemRecvBufSize[recvBufIndex])
	{
		printf("[DSP%d]申请用户空间超过Buf容量！\n",sysCpuGetID());
		return NULL;
	}
	
	if(align%4 != 0)
	{
		printf("[DSP%d]申请用户空间内存时参数align必须是4的倍数！\n",sysCpuGetID());
		return NULL;
	}	
	
	len = userMemAlignLen(/*USER_MEM_ALLOC_ALIGN*/align, nBytes);	
	if(len >= USER_MEM_ALLOC_MAX_LEN) 
	{
		printf("[DSP%d]申请长度大于0x%x，实际申请长度为0x%x\n",sysCpuGetID(),USER_MEM_ALLOC_MAX_LEN,USER_MEM_ALLOC_MAX_LEN);
		len = USER_MEM_ALLOC_MAX_LEN;
	}
	
	spinLockTaskTake(s_pRecvSpinLock[recvBufIndex]);
	wrPtr = s_userMemRecvBufWrPtr[recvBufIndex];

	if ((wrPtr + len) > s_userMemRecvBufSize[recvBufIndex]) 
	{
		wrPtr = 0;
		s_userMemRecvBufWrPtr[recvBufIndex] = len;
	} 
	else 
	{
		s_userMemRecvBufWrPtr[recvBufIndex] = wrPtr + len;
	}

	spinLockTaskGive(s_pRecvSpinLock[recvBufIndex]);

	return (void *) ((unsigned int) s_userMemRecvBuf[recvBufIndex] + wrPtr);

}

void UserMemModuleInit(void)
{
	return;
}
