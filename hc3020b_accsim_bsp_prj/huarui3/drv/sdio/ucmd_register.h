/*
 * ucmd_register.h
 *
 *  Created on: Oct 20, 2022
 *      Author: lenovo
 */

#ifndef _DRIVER_SDIO_UCMD_REGISTER_H_
#define _DRIVER_SDIO_UCMD_REGISTER_H_

extern int cmd_init_sd(u8 controller, int mode);
extern int cmd_write_sd(u8 controller, u64 sd_waddr, u64 src_addr, u64 wlen);
extern int cmd_read_sd(u8 controller, u64 sd_raddr, u64 dst_addr, u64 rlen);
extern int cmd_init_mmc(u8 controller);
extern int cmd_write_mmc(u8 controller, u64 sd_waddr, u64 src_addr, u64 wlen);
extern int cmd_read_mmc(u8 controller, u64 sd_raddr, u64 dst_addr, u64 rlen);
extern int cmd_sdhc_init(int argc, u64 *argv);
extern int cmd_sdcard_get_card_id(int argc, u64 *argv);
extern int cmd_print_card_status(int argc, u64 *argv);
extern int cmd_print_reg_resp(int argc, u64 *argv);
extern int cmd_print_srs_cap(int argc, u64 *argv);
extern int cmd_mmc_ext_csd(int argc, u64 *argv);

int cmd_mmc_get_partition_info(int argc, u64 *argv);

#endif /* _DRIVER_SDIO_UCMD_REGISTER_H_ */
