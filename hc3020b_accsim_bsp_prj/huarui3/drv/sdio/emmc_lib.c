/* ******************************************************************************************
 * FILE NAME   : emmc_lib.c
 * PROGRAMMER  : zhaozz
 * DESCRIPTION : emmc library
 * DATE        : 2022-05-13 09:33:13
 * *****************************************************************************************/

#ifndef  __EMMC_LIB_C__
#define  __EMMC_LIB_C__
#include "./sdhc_common.h"
#include "./emmc_lib.h"

/*
 * Mode Name                                     |       Data Rate  | I/O Voltage          |   Bus Width  |   Frequency |   Max Data Transfer (implies x8 bus width)
 * Backwards Compatibility with legacy MMC card  |  Single          | 3 V/1.8 V/1.2 V      | 1, 4, 8      | 0-26 MHz    |   26 MB/s
 * High Speed SDR                                |  Single          | 3 V/1.8 V/1.2 V      | 1,4, 8       | 0-52 MHz    |   52 MB/s
 * High Speed DDR                                |  Dual            | 3 V/1.8 V/ 1.2V,     | 4, 8         | 0-52 MHz    |   104 MB/s
 * HS200                                         |  Single          | 1.8 V/1.2 V          | 4, 8         | 0-200 MHz   |   200 MB/s
 * HS400                                         |  Dual            | 1.8 V/1.2 V          | 8            | 0-200 MHz   |   400 MB/s
 * */

static struct MMC_MODE_MAP global_mmc_mode_map[] = {
	//{
	//	.desc = "backwords compatibility, Single, 0-26MHz, 26MB/s, 1-bit",
	//	.mode = E_COMP,
	//	.voltage = E_1o8,      //1.2 1.8 3
	//	.bit_width = E_1BITS  //1,4,8
	//},
	//{
	//	.desc = "backwords compatibility, Single, 0-26MHz, 26MB/s, 4-bit",
	//	.mode = E_COMP,
	//	.voltage = E_1o8,      //1.2 1.8 3
	//	.bit_width = E_4BITS,  //1,4,8
	//},
	//{
	//	.desc = "backwords compatibility, Single, 0-26MHz, 26MB/s, 8-bit",
	//	.mode = E_COMP,
	//	.voltage = E_1o8,      //1.2 1.8 3
	//	.bit_width = E_8BITS,  //1,4,8
	//	.sdhc_clk = 20000,
	//},
	{
		.desc = "high speed SDR, Single, 0-52MHz, 52MB/s, 1-bit",
		.mode = E_SDR,
		.voltage = E_1o8,      //1.2 1.8 3
		.bit_width = E_1BITS,  //1,4,8
		.sdhc_clk = 50000,
	},
	{
		.desc = "high speed SDR, Single, 0-52MHz, 52MB/s, 4-bit",
		.mode = E_SDR,
		.voltage = E_1o8,      //1.2 1.8 3
		.bit_width = E_4BITS,  //1,4,8
		.sdhc_clk = 50000,
	},
	{
		.desc = "high speed SDR, Single, 0-52MHz, 52MB/s, 8-bit",
		.mode = E_SDR,
		.voltage = E_1o8,      //1.2 1.8 3
		.bit_width = E_8BITS,  //1,4,8
		.sdhc_clk = 50000,
	},
	//{
	//	.desc = "high speed DDR, Dual,  0-52MHz, 104MB/s, 4-bit",
	//	.mode = E_DDR,
	//	.voltage = E_1o8,      //1.2 1.8 3
	//	.bit_width = E_4BITS,  //4,8
	//	.sdhc_clk = 50000,
	//},
	//{
	//	.desc = "high speed DDR, Dual,  0-52MHz, 104MB/s, 8-bit",
	//	.mode = E_DDR,
	//	.voltage = E_1o8,      //1.2 1.8 3
	//	.bit_width = E_8BITS,  //4,8
	//	.sdhc_clk = 50000,
	//},
	{
		.desc = "HS, Single,  0-200MHz, 200MB/s, 8-bit",
		.mode = E_HS,
		.voltage = E_1o8,      //1.2 1.8
		.bit_width = E_8BITS,  //4,8
		.sdhc_clk = 50000,
	},
	{
		.desc = "HS, Single,  0-200MHz, 200MB/s, 4-bit",
		.mode = E_HS,
		.voltage = E_1o8,      //1.2 1.8
		.bit_width = E_4BITS,  //4,8
		.sdhc_clk = 50000,
	},
	{
		.desc = "HS200, Single,  0-200MHz, 200MB/s, 4-bit",
		.mode = E_HS200,
		.voltage = E_1o8,      //1.2 1.8
		.bit_width = E_4BITS,  //4,8
		.sdhc_clk = 50000,
	},
	{
		.desc = "HS200, Single,  0-200MHz, 200MB/s, 8-bit",
		.mode = E_HS200,
		.voltage = E_1o8,      //1.2 1.8
		.bit_width = E_8BITS,  //4,8
		.sdhc_clk = 50000,
	},
	{
		.desc = "HS400, Dual,  0-200MHz, 400MB/s, 8-bit",
		.mode = E_HS400,
		.voltage = E_1o8,      //1.2 1.8
		.bit_width = E_8BITS,  //8
		.sdhc_clk = 50000,
	},
};

const static u32 mmc_pattern_4b[64/4] = {
	0x00ff0fff,
	0xccc3ccff,
	0xffcc3cc3,
	0xeffefffe,
	0xddffdfff,
	0xfbfffbff,
	0xff7fffbf,
	0xefbdf777,
	0xf0fff0ff,
	0x3cccfc0f,
	0xcfcc33cc,
	0xeeffefff,
	0xfdfffdff,
	0xffbfffdf,
	0xfff7ffbb,
	0xde7b7ff7
};

const static u32 mmc_pattern_8b[128/4] = {
	0xff00ffff,
	0x0000ffff,
	0xccccffff,
	0xcccc33cc,
	0xcc3333cc,
	0xffffcccc,
	0xffffeeff,
	0xffeeeeff,
	0xffddffff,
	0xddddffff,
	0xbbffffff,
	0xbbffffff,
	0xffffffbb,
	0xffffff77,
	0x77ff7777,
	0xffeeddbb,
	0x00ffffff,
	0x00ffffff,
	0xccffff00,
	0xcc33cccc,
	0x3333cccc,
	0xffcccccc,
	0xffeeffff,
	0xeeeeffff,
	0xddffffff,
	0xddffffff,
	0xffffffdd,
	0xffffffbb,
	0xffffbbbb,
	0xffff77ff,
	0xff7777ff,
	0xeeddbb77,
};

static int mmc_send_abort_cmd(struct sdhc_regs *host)
{
	SRS03_CTM srs03_val = {.bits.CIDX = 12, .bits.CRCCE = 1, .bits.CICE=1, .bits.RTS=2};
	MMC_ARG_CMD12 arg12_val = {.bits.RCA = EMMC_CARD_ADDRESS};
	sdhc_send(host, srs03_val, arg12_val.v, 200, "send abort command");
}

int mmc_tuning_error_recover(struct sdhc_regs *host)
{
	int i = 0;
	SRS12_ENIS srs12_val;
	int err_cnt = 0;
	// disable interrupt
	host->srs_regs.srs14_ense.v = 0;
	srs12_val = host->srs_regs.srs12_enis;
	if(srs12_val.bits.CC && (srs12_val.bits.ECT || srs12_val.bits.ECCRC || srs12_val.bits.ECEB || srs12_val.bits.ECI ))
	{
		err_cnt++;
		host->srs_regs.srs11_hc2.bits.SRCMD = 1;
		wait_if( host->srs_regs.srs11_hc2.bits.SRCMD == 1, DEFAULT_WAIT_CNT, "srs11 SRCMD");
		host->srs_regs.srs12_enis.bits.ECT = 1;
		host->srs_regs.srs12_enis.bits.ECCRC = 1;
		host->srs_regs.srs12_enis.bits.ECEB = 1;
		host->srs_regs.srs12_enis.bits.ECI = 1;
	}
	if(srs12_val.bits.TC && (srs12_val.bits.EDT || srs12_val.bits.EDCRC || srs12_val.bits.EDEB))
	{
		err_cnt++;
		host->srs_regs.srs11_hc2.bits.SRCMD = 1;
		wait_if( host->srs_regs.srs11_hc2.bits.SRDAT == 1, DEFAULT_WAIT_CNT, "srs11 SRDAT");
		host->srs_regs.srs12_enis.bits.EDT = 1;
		host->srs_regs.srs12_enis.bits.EDCRC = 1;
		host->srs_regs.srs12_enis.bits.EDEB = 1;
	}
	if (err_cnt)
	{
		// issue abort command
		uerror("error, sts:0x%x\r\n", srs12_val.v);
		mmc_send_abort_cmd(host);
		// check command cmd or data error
		srs12_val = host->srs_regs.srs12_enis;
		wait_if(host->srs_regs.srs11_hc2.bits.SRCMD==1 || host->srs_regs.srs11_hc2.bits.SRDAT == 1, DEFAULT_WAIT_CNT, "check if srs11 SRCMD or SRDAT error");

		srs12_val = host->srs_regs.srs12_enis;
		if (srs12_val.bits.ECT || srs12_val.bits.ECCRC || srs12_val.bits.ECEB || srs12_val.bits.ECI )
		{
			host->srs_regs.srs11_hc2.bits.SRCMD = 1;
			print_card_status(host, EMMC_CARD_ADDRESS);
			if (host->srs_regs.srs12_enis.bits.EINT)
			{
				print_reg_srs12(host->srs_regs.srs12_enis);
				uerror("cmd any EINT\r\n");
				goto none_recoverable_error;
			}
		}
		srs12_val = host->srs_regs.srs12_enis;
		if (srs12_val.bits.EDT)
		{
			uerror("EDT\r\n");
			goto none_recoverable_error;
		}
		wait_if(1, HZ/10000, "spin"); //100us
		if (host->srs_regs.srs11_hc2.bits.SRDAT == 0)
		{
			uerror("DAT low error\r\n");
			goto none_recoverable_error;
		}
		goto recoverable_error;
	} else {
		uinfo("no error, sts:0x%x\r\n", srs12_val.v);
	}

recoverable_error:
	uerror("recoverable_error!\r\n");
	return 0;

none_recoverable_error:
	uerror("none_recoverable_error!\r\n");
	return 1;
}

static int mmc_card_tunning(struct sdhc_regs *host, u8 data_width)
{
	u32 pattern_buf[128/4];
	u8 pattern_ok[40];
	u32 block_size;
	if (data_width == 4)
		block_size = 64;
	else
		block_size = 128;
	SRS11_HC2 srs11_hc2_prev = host->srs_regs.srs11_hc2;
	host->srs_regs.srs11_hc2.bits.DTCV = 0; // data timeout counter, value: 0-t_sdmclk*2(13+2)
	// restart start tunning procedure
	//host->srs_regs.srs15_hcaces.bits.SCS = 0;
	//host->srs_regs.srs15_hcaces.bits.EXTNG = 1;
	int i = 0;
	volatile SRS12_ENIS srs12_val = {.v = 0};
	int patter_cnt = 40;
	for (i=0; i<patter_cnt; i++)
	{
		uinfo("tunning index:%d\r\n", i);
		sdhc_set_tune_val(host, i);
		host->srs_regs.srs01_bsr.v = block_size;
		SRS03_CTM srs03_val = {.bits.CIDX = 21, .bits.DPS = 1, .bits.CRCCE = 1, .bits.CICE=1, .bits.RTS=2, .bits.DTDS = 1};
		sdhc_send(host, srs03_val, 0, DEFAULT_WAIT_CNT, "send tuning pattern");
		wait_if(1, 500, "spin");

		// check SRS12_ENIS SRS11_HC2
		int srs12_err = 0;
		SRS11_HC2 srs11_val = {.v =0 };
		wait_if( ({
					srs12_val = host->srs_regs.srs12_enis;
					if ((srs12_val.bits.EDT || srs12_val.bits.EDCRC || srs12_val.bits.EDEB) ||
							(srs12_val.bits.BRR && (srs12_val.bits.ECT || srs12_val.bits.ECCRC || srs12_val.bits.ECEB || srs12_val.bits.ECI)))
					{
					uerror("error,0x%x\r\n", srs12_val.v);
					// reset dat line and cmd line
					srs11_val = host->srs_regs.srs11_hc2;
					srs11_val.bits.SRDAT = 1;
					srs11_val.bits.SRCMD = 1;
					host->srs_regs.srs11_hc2 = srs11_val;
					wait_if( host->srs_regs.srs11_hc2.bits.SRDAT == 1, DEFAULT_WAIT_CNT, "srs11 SRDAT == 1");
					wait_if( host->srs_regs.srs11_hc2.bits.SRCMD == 1, DEFAULT_WAIT_CNT, "srs11 SRCMD == 1");
					srs12_err = 1;
					break;
					} else {
					udebug("no error,0x%x\r\n", srs12_val.v);
					srs12_err = 0;
					}
					srs12_val.bits.BRR == 0;
					}), 200, "recovery SRDAT SRCMD");


		if (srs12_err == 0)
		{
			int j=0;
			for (j=0; j<block_size/4; j++)
			{
				pattern_buf[j] = host->srs_regs.srs08_db.v;
				udebug("pattern_buf:%d-0x%x\r\n", j, pattern_buf[j]);
			}
			wait_if(host->srs_regs.srs12_enis.bits.TC==0, DEFAULT_WAIT_CNT, "srs12 TC");
			pattern_ok[i] = 1;
			for (j=0; j<block_size/4; j++)
			{
				uart_printf_wait(".");
				if((data_width == 4 && (pattern_buf[j] != mmc_pattern_4b[j])) ||
						(data_width == 8 && (pattern_buf[j] != mmc_pattern_8b[j])))
				{
					pattern_ok[i] = 0;
					uerror("read pattern not correct: data_width=%d, pattern_buf=0x%x, mmc_pattern_4b=0x%x, mmc_pattern_8b=0x%x\r\n",
							data_width, pattern_buf[j], mmc_pattern_4b[j], mmc_pattern_8b[j]);
					break;
				}
			}
		} else {
			pattern_buf[i] = 0;
		}

	}
	// select the best tuning value
	int pos = 0, len = 0, cur_len = 0;
	for (i = 0; i<patter_cnt; i++)
	{
		if (pattern_ok[i] == 1)
		{
			cur_len ++;
			if (cur_len > len)
			{
				pos = i - len++;
			}
			uart_printf_wait("calc tunning, i=%d, pos=%d, len=%d\r\n", i, pos, len);
		} else {
			cur_len = 0;
		}
	}
	pos = pos + len/2;
	uinfo("sdhc_set_tune_val set to %d\r\n", pos);
	sdhc_set_tune_val(host, pos);

	// set previous timeout counter value
	host->srs_regs.srs11_hc2 = srs11_hc2_prev;
	return pos;
}

int mmc_card_set_mode(struct sdhc_regs *host, enum MMC_MODE mode, enum MMC_BIT_WIDTH bit_width)
{
	// 0. search mode  desc voltage bit_width
	int len_global_mmc_mode_map = sizeof(global_mmc_mode_map)/sizeof(struct MMC_MODE_MAP);
	enum MMC_VOL  this_vol;
	u32 this_sdhc_clk;
	int i=0;
	int match = 0;
	for (i=0; i< len_global_mmc_mode_map; i++)
	{
		if (global_mmc_mode_map[i].mode == mode && global_mmc_mode_map[i].bit_width == bit_width)
		{
			this_vol = global_mmc_mode_map[i].voltage;
			this_sdhc_clk = global_mmc_mode_map[i].sdhc_clk;
			match++;
			uinfo("match: %s\r\n", global_mmc_mode_map[i].desc);
			break;
		}
	}
	if (match == 0)
	{
		uerror("mode not match:%d, %d\r\n", mode, bit_width);
		return -1;
	}
	//DEBUG_CONSOLE;
	// 1. set bit_width
	SRS10_HC1 srs10_val = host->srs_regs.srs10_hc1;

	switch (bit_width) {
		case E_1BITS:
			srs10_val.bits.EDTW = 0;
			srs10_val.bits.DTW = 0;
			host->srs_regs.srs10_hc1 = srs10_val;
			break;
		case E_4BITS:
			srs10_val.bits.EDTW = 0;
			srs10_val.bits.DTW = 1;
			host->srs_regs.srs10_hc1 = srs10_val;
			break;
		case E_8BITS:
			srs10_val.bits.EDTW = 1;
			srs10_val.bits.DTW = 1;
			host->srs_regs.srs10_hc1 = srs10_val;
			break;
	}
	MMC_ARG_CMD6 arg_cmd6 = {.bits.VALUE = bit_width, .bits.INDEX=183, .bits.ACCESS=3};
	SRS03_CTM srs03_val = {.bits.CIDX = 6, .bits.CRCCE = 1, .bits.CICE = 1, .bits.RTS=3};
	sdhc_send(host, srs03_val, arg_cmd6.v, DEFAULT_WAIT_CNT, "set bit width");
	wait_if(host->srs_regs.srs12_enis.bits.TC==0, DEFAULT_WAIT_CNT, "wait srs12 TC");

	// 2. switch to HS200 first and perform calibration, if switching to HS400
	if (mode == E_HS400)
	{
		mmc_card_set_mode(host, E_HS200, E_8BITS);
		// try to HS400 perform calibration
		HRS10_HCSSPA hrs10_val = {.bits.HCSDCLKADJ = 3};
		host->hrs_regs.hrs10_hcsspa = hrs10_val; // disable sdclk earlier adjustment
		host->hrs_regs.hrs16_hsge.v = 0;
		mmc_card_tunning(host, 8);
		uinfo("switch to  HS200 and perform calibration done, now switching to  HS400...\r\n");

		arg_cmd6.v = 0;
		arg_cmd6.bits.VALUE=E_HS400;
		arg_cmd6.bits.INDEX=185;
		arg_cmd6.bits.ACCESS=3;
		srs03_val.v = 0;
		srs03_val.bits.CIDX=6;
		srs03_val.bits.CRCCE=1;
		srs03_val.bits.CICE=1;
		srs03_val.bits.RTS=3;
		sdhc_send(host, srs03_val, arg_cmd6.v, DEFAULT_WAIT_CNT, "switch to HS400 done");
		wait_if(host->srs_regs.srs12_enis.bits.TC==0, DEFAULT_WAIT_CNT, "srs12 TC");

		// addtional DDR setting need for HS400
		arg_cmd6.v = 0;
		arg_cmd6.bits.VALUE=6; // ??
		arg_cmd6.bits.INDEX=183;
		arg_cmd6.bits.ACCESS=3;
		srs03_val.v = 0;
		srs03_val.bits.CIDX=6;
		srs03_val.bits.CRCCE=1;
		srs03_val.bits.CICE=1;
		srs03_val.bits.RTS=3;
		sdhc_send(host, srs03_val, arg_cmd6.v, DEFAULT_WAIT_CNT, "additional DDR setting for HS400");
		wait_if(host->srs_regs.srs12_enis.bits.TC, DEFAULT_WAIT_CNT, "srs12 TC");
		/* set emmc mode select
		 * 000 - 0 SD card in use
		 * 010 - 2 SDR
		 * 011 - 3 DDR
		 * 100 - 4 HS200
		 * 101 - 5 HS400
		 * 110 - 6 HS400 enhanced strobe
		 * */
		// disable SDCE, it will be re-enabled when sdhc_set_clock called
		host->srs_regs.srs11_hc2.v = 0;
		wait_if(1, 500, "spin");
		host->hrs_regs.hrs06_ec.bits.EMM = 5;

		sdhc_set_clock(host, this_sdhc_clk); //200MHz

		// set 64-bit address
		SRS15_HCACES srs15_val = host->srs_regs.srs15_hcaces;
		srs15_val.bits.HV4E = 1;
		srs15_val.bits.A64B = 1;
		host->srs_regs.srs15_hcaces = srs15_val;
	} else if (mode == E_HS200) {
		// switch work mode
		arg_cmd6.v = 0;
		arg_cmd6.bits.VALUE=E_HS200;
		arg_cmd6.bits.INDEX=185;
		arg_cmd6.bits.ACCESS=3;
		srs03_val.v = 0;
		srs03_val.bits.CIDX=6;
		srs03_val.bits.CRCCE=1;
		srs03_val.bits.CICE=1;
		srs03_val.bits.RTS=3;
		sdhc_send(host, srs03_val, arg_cmd6.v, DEFAULT_WAIT_CNT, "switch to HS200");
		wait_if(host->srs_regs.srs12_enis.bits.TC==0, DEFAULT_WAIT_CNT, "srs12 TC");
		// disable SDCE, it will be re-enabled when sdhc_set_clock called
		host->srs_regs.srs11_hc2.v = 0;
		wait_if(1, 500, "spin");
		// set emmc mode select
		/*
		 * 000 - 0 SD card in use
		 * 010 - 2 SDR
		 * 011 - 3 DDR
		 * 100 - 4 HS200
		 * 101 - 5 HS400
		 * 110 - 6 HS400 enhanced strobe
		 * */
		host->hrs_regs.hrs06_ec.bits.EMM = 4;
		sdhc_set_clock(host, this_sdhc_clk); // 200MHz

		// set 64-bit address
		SRS15_HCACES srs15_val = host->srs_regs.srs15_hcaces;
		srs15_val.bits.HV4E = 1;
		srs15_val.bits.A64B = 1;
		host->srs_regs.srs15_hcaces = srs15_val;
	} else if (mode == E_SDR) { // E_SDR
		// switch work mode
		arg_cmd6.v = 0;
		arg_cmd6.bits.VALUE=mode;
		arg_cmd6.bits.INDEX=185;
		arg_cmd6.bits.ACCESS=3;
		srs03_val.v = 0;
		srs03_val.bits.CIDX=6;
		srs03_val.bits.CRCCE=1;
		srs03_val.bits.CICE=1;
		srs03_val.bits.RTS=3;
		sdhc_send(host, srs03_val, arg_cmd6.v, DEFAULT_WAIT_CNT, "switch to SDR");

		// disable SDCE, it will be re-enabled when sdhc_set_clock called
		host->srs_regs.srs11_hc2.v = 0;
//		wait_if(1, 500, "spin");
		delay_cnt(500);

		// set emmc mode select
		/*
		 * 000 - 0 SD card in use
		 * 010 - 2 SDR
		 * 011 - 3 DDR
		 * 100 - 4 HS200
		 * 101 - 5 HS400
		 * 110 - 6 HS400 enhanced strobe
		 * */
		host->hrs_regs.hrs06_ec.bits.EMM = 2;
		sdhc_set_clock(host, this_sdhc_clk); // 20MHz

		// set 64-bit address
		SRS15_HCACES srs15_val = host->srs_regs.srs15_hcaces;
		srs15_val.bits.HV4E = 1;
		srs15_val.bits.A64B = 1;
		host->srs_regs.srs15_hcaces = srs15_val;
	} else {
		uerror("no mode configuration:%d\r\n", mode);
	}

	return 0;
}

/*
 * voltage        :   EMMC_OCR.VOL_17_195 VOL_20_26 VOL_27_36(0x1ff)
 * access_mode    :   EMMC_OCR.ACCESS_MODE(0: byte, 2:sector)
 * rca_addr       :   emmc card address (not 0x00)
 * trans_width    :   1 1-bit, 4 4-bit, 8 8-bits
 * mode           :   0-standard mode(0-26MHz), 1-high speed mode(0-52MHz), 2-HS200(0-200MHz), 3-HS400(0-200MHz, DDR)
 * */
int mmc_card_init(struct sdhc_regs *host, u32 rca_addr)
{
	// 74 clks before first command
//	wait_if(1, 1000, "spin");
	delay_cnt(1000);
	// cmd0, no crc, no response
	volatile SRS03_CTM srs03_val = {.v = 0};
	srs03_val.v = 0;
	srs03_val.bits.CIDX = 0;
	srs03_val.bits.RTS = 0;
	sdhc_send(host, srs03_val, 0x0, DEFAULT_WAIT_CNT/*0*/, "reset to idle state"); // reset to idle state
	// cmd1  access_mode config
	volatile MMC_ARG_CMD1 mmc_arg_val1 = {.v = 0};
	srs03_val.v = 0;
	srs03_val.bits.CIDX=1;
	srs03_val.bits.RTS=2;
	mmc_arg_val1.bits.VOL_27_36 = 0x1FF;  // 2.7-3.6v
	mmc_arg_val1.bits.VOL_17_195 = 1;     // 1.8v
	mmc_arg_val1.bits.ACCESS_MODE = 2;    // sector mode
	wait_if( ({
				sdhc_send(host, srs03_val, mmc_arg_val1.v, DEFAULT_WAIT_CNT, "set mode and voltage");
				mmc_arg_val1.v = sdhc_get_response(host, 0);
				mmc_arg_val1.bits.POWER_STS_OR_BUSY == 0;
				}),
			DEFAULT_WAIT_CNT, "mmc response BUSY");

	// cmd2 get card id
	srs03_val.v = 0;
	srs03_val.bits.CIDX=2;
	srs03_val.bits.CRCCE=0;
	srs03_val.bits.RTS=1;
	sdhc_send(host, srs03_val, 0x0, DEFAULT_WAIT_CNT, "get card id");
	udebug("card id resp:0x%x,%x,%x,%x\r\n", sdhc_get_response(host, 0), sdhc_get_response(host, 1), sdhc_get_response(host, 2), sdhc_get_response(host, 3));
	// cmd3 send RCA and goto STBY
	MMC_ARG_CMD3 cmd3_arg = {.bits.RCA = rca_addr};
	srs03_val.v = 0;
	srs03_val.bits.CIDX=3;
	srs03_val.bits.CRCCE=1;
	srs03_val.bits.CICE=1;
	srs03_val.bits.RTS=2;
	sdhc_send(host, srs03_val, cmd3_arg.v, DEFAULT_WAIT_CNT, "config rca_addr");
	udebug("emmc init with rca:0x%x, 0x%x\r\n", cmd3_arg.bits.RCA, cmd3_arg.v);
	print_card_status(host, rca_addr);

	srs03_val.v = 0;
	srs03_val.bits.CIDX=7;
	srs03_val.bits.CRCCE=1;
	srs03_val.bits.CICE=1;
	srs03_val.bits.RTS=2;
	cmd3_arg.v = 0;
	cmd3_arg.bits.RCA = rca_addr;
	sdhc_send(host, srs03_val, cmd3_arg.v, DEFAULT_WAIT_CNT, "select card with rca");
	// cmd7 select card
	print_card_status(host, rca_addr);
}

// using sdma trans
int mmc_write(struct sdhc_regs *host, u64 mmc_dst_addr, u64 src_addr, u64 block_cnt, u32 block_size)
{
	if (block_cnt == 0)
		return -1;
	udebug("src_addr:0x%lx, mmc_dst_addr:0x%lx, block_cnt:%d\r\n", src_addr, mmc_dst_addr, block_cnt);

	// dest memory address
	host->srs_regs.srs22_asa1.v = src_addr & 0xFFFFFFFF;
	host->srs_regs.srs23_asa2.v = (src_addr >> 32) & 0xFFFFFFFF;

	// block count
	//SDMABB = 7;       // system address boundary size: 2^(x+2) KB
	//TBS = 512;        // transfor block size Byte
	SRS01_BSR srs01_val = {.bits.SDMABB=7, .bits.TBS=block_size, .bits.BCCT=block_cnt};
	host->srs_regs.srs01_bsr = srs01_val;

	// sdma config
	host->srs_regs.srs10_hc1.bits.DMASEL = 0; // 0-sdma, 3-adma2(64-bit), 2-adma2(32-bit)

	// clear cpu timestamp for speed test
//	write_c0_count(0);
	// record cmd start timestamp
//	global_pre_cpu_cnt = read_c0_count();

	// send cmd
	SRS03_CTM srs03_val = {.v = 0};
	if (block_cnt > 1)
	{
		srs03_val.v = 0;
		srs03_val.bits.CIDX=25;
		srs03_val.bits.MSBS=1;
		srs03_val.bits.ACE=1;
		srs03_val.bits.CRCCE=1;
		srs03_val.bits.CICE=1;
		srs03_val.bits.RTS=2;
		srs03_val.bits.DPS=1;
		srs03_val.bits.BCE=1;
		srs03_val.bits.DMAE=1;
		sdhc_send(host, srs03_val, mmc_dst_addr, DEFAULT_WAIT_CNT, "write multi-block dma start");
	} else {
		srs03_val.v = 0;
		srs03_val.bits.CIDX=24;
		srs03_val.bits.CRCCE=1;
		srs03_val.bits.CICE=1;
		srs03_val.bits.RTS=2;
		srs03_val.bits.DPS=1;
		srs03_val.bits.BCE=1;
		srs03_val.bits.DMAE=1;
		sdhc_send(host, srs03_val, mmc_dst_addr, DEFAULT_WAIT_CNT, "write single-block dma start");
	}

	// check dma intterupt
	volatile SRS12_ENIS srs12_val = {.v = 0};
	volatile SRS00_SSAR srs00_val;
	wait_if(({
				delay_cnt(1000);/*ldf 20230821 add*/
				srs12_val = host->srs_regs.srs12_enis;
				if(srs12_val.bits.DMAINT) // when meeting SDMABB interrupt
				{
#if 1/*ldf 20230817 add:: 参考裸测驱动修改*/
					u64 up_addr_next = (u64)(((((u64)host->srs_regs.srs23_asa2.v) & 0xffffffff) << 32) | host->srs_regs.srs22_asa1.v);
					udebug("DMAINT meet, update sdma address: next:0x%lx\r\n", up_addr_next);
					host->srs_regs.srs12_enis.bits.DMAINT = 1; // clear dma intterupt flag
					host->srs_regs.srs22_asa1.v = (up_addr_next) & 0xFFFFFFFF;
					host->srs_regs.srs23_asa2.v = ((up_addr_next)>> 32) & 0xFFFFFFFF;
#else
					udebug("DMAINT meet, update sdma address: 0x%lx\r\n", src_addr+512*1024);
					host->srs_regs.srs12_enis.bits.DMAINT = 1; // clear dma intterupt flag
					host->srs_regs.srs22_asa1.v = (src_addr + 512*1024) & 0xFFFFFFFF;
					host->srs_regs.srs23_asa2.v = ((src_addr + 512*1024)>> 32) & 0xFFFFFFFF;
#endif				
				}
				srs12_val.bits.TC == 0;
				}), DEFAULT_WAIT_CNT*1000, "update sdma address TC");/*ldf 20230821 modify:: 配置成1bit模式，速率太慢，这里要加长延时*/

//	global_cur_cpu_cnt = read_c0_count();
//	uinfo("pre_cpu_cnt:0x%x, cur_cpu_cnt:0x%x, diff:%d, ms:%d\r\n", global_pre_cpu_cnt, global_cur_cpu_cnt, global_cur_cpu_cnt - global_pre_cpu_cnt, ((global_cur_cpu_cnt-global_pre_cpu_cnt)/(HZ/1000)));
}

int mmc_write_interrupt(struct sdhc_regs *host, u64 mmc_dst_addr, u64 src_addr, u64 block_cnt, u32 block_size)
{
	if (block_cnt == 0)
		return -1;
	uinfo("src_addr:0x%x, mmc_dst_addr:0x%x, block_cnt:%d\r\n", src_addr, mmc_dst_addr, block_cnt);

	// dest memory address
	host->srs_regs.srs22_asa1.v = src_addr & 0xFFFFFFFF;
	host->srs_regs.srs23_asa2.v = (src_addr >> 32) & 0xFFFFFFFF;

	// block count
	//SDMABB = 7;       // system address boundary size: 2^(x+2) KB
	//TBS = 512;        // transfor block size Byte
	SRS01_BSR srs01_val = {.bits.SDMABB=7, .bits.TBS=block_size, .bits.BCCT=block_cnt};
	host->srs_regs.srs01_bsr = srs01_val;

	// sdma config
	host->srs_regs.srs10_hc1.bits.DMASEL = 0; // 0-sdma, 3-adma2(64-bit), 2-adma2(32-bit)

	// clear cpu timestamp for speed test
//	write_c0_count(0);
	// record cmd start timestamp
//	global_pre_cpu_cnt = read_c0_count();

	// send cmd
	SRS03_CTM srs03_val = {.v = 0};
	if (block_cnt > 1)
	{
		srs03_val.v = 0;
		srs03_val.bits.CIDX=25;
		srs03_val.bits.MSBS=1;
		srs03_val.bits.ACE=1;
		srs03_val.bits.CRCCE=1;
		srs03_val.bits.CICE=1;
		srs03_val.bits.RTS=2;
		srs03_val.bits.DPS=1;
		srs03_val.bits.BCE=1;
		srs03_val.bits.DMAE=1;
		sdhc_send_cmd_interrupt(host, srs03_val, mmc_dst_addr, DEFAULT_WAIT_CNT, "write multi-block dma start");
	} else {
		srs03_val.v = 0;
		srs03_val.bits.CIDX=24;
		srs03_val.bits.CRCCE=1;
		srs03_val.bits.CICE=1;
		srs03_val.bits.RTS=2;
		srs03_val.bits.DPS=1;
		srs03_val.bits.BCE=1;
		srs03_val.bits.DMAE=1;
		sdhc_send_cmd_interrupt(host, srs03_val, mmc_dst_addr, DEFAULT_WAIT_CNT, "write single-block dma start");
	}

	// check dma intterupt
	volatile SRS00_SSAR srs00_val;
	wait_if(({
				delay_cnt(1000);/*ldf 20230821 add*/
				if(global_srs12_sts0.bits.DMAINT) // when meeting SDMABB interrupt
				{
#if 1/*ldf 20230817 add:: 参考裸测驱动修改*/
					u64 up_addr_next = (u64)(((((u64)host->srs_regs.srs23_asa2.v) & 0xffffffff) << 32) | host->srs_regs.srs22_asa1.v);
					udebug("DMAINT meet, update sdma address: next:0x%lx\r\n", up_addr_next);
					host->srs_regs.srs12_enis.bits.DMAINT = 1; // clear dma intterupt flag
					host->srs_regs.srs22_asa1.v = (up_addr_next) & 0xFFFFFFFF;
					host->srs_regs.srs23_asa2.v = ((up_addr_next)>> 32) & 0xFFFFFFFF;
#else
					udebug("DMAINT meet, update sdma address: 0x%lx\r\n", src_addr+512*1024);
					global_srs12_sts0.bits.DMAINT = 0; // clear dma intterupt flag
					host->srs_regs.srs22_asa1.v = (src_addr + 512*1024) & 0xFFFFFFFF;
					host->srs_regs.srs23_asa2.v = ((src_addr + 512*1024)>> 32) & 0xFFFFFFFF;
#endif				
				}
				global_srs12_sts0.bits.TC == 0;
				}), DEFAULT_WAIT_CNT, "update sdma address TC");

//	uinfo("pre_cpu_cnt:0x%x, cur_cpu_cnt:0x%x, diff:%d, ms:%d\r\n", global_pre_cpu_cnt, global_cur_cpu_cnt, global_cur_cpu_cnt - global_pre_cpu_cnt, ((global_cur_cpu_cnt-global_pre_cpu_cnt)/(HZ/1000)));
}


int mmc_read(struct sdhc_regs *host, u64 mmc_src_addr, u64 dst_addr, u64 block_cnt, u32 block_size)
{
	if (block_cnt == 0)
		return -1;
	udebug("mmc_src_addr:0x%lx, dst_addr:0x%lx, block_cnt:%d\r\n", mmc_src_addr, dst_addr, block_cnt);
	// dest memory address
	host->srs_regs.srs22_asa1.v = dst_addr & 0xFFFFFFFF;
	host->srs_regs.srs23_asa2.v = (dst_addr >> 32) & 0xFFFFFFFF;

	// block count
	//SDMABB = 7;       // system address boundary size: 2^(x+2) KB
	//TBS = 512;        // transfer block size Byte
	SRS01_BSR srs01_val = {.bits.SDMABB=7, .bits.TBS=block_size, .bits.BCCT=block_cnt};
	host->srs_regs.srs01_bsr = srs01_val;

	// sdma config
	host->srs_regs.srs10_hc1.bits.DMASEL = 0; // 0-sdma, 3-adma2(64-bit), 2-adma2(32-bit)

	// clear cpu timestamp for speed test
//	write_c0_count(0);
	// record cmd start timestamp
//	global_pre_cpu_cnt = read_c0_count();

	// send cmd
	SRS03_CTM srs03_val = {.v = 0};
	if (block_cnt > 1)
	{
		srs03_val.v = 0;
		srs03_val.bits.CIDX=18;
		srs03_val.bits.MSBS=1;
		srs03_val.bits.ACE=1;
		srs03_val.bits.DPS=1;
		srs03_val.bits.CRCCE=1;
		srs03_val.bits.CICE=1;
		srs03_val.bits.RTS=2;
		srs03_val.bits.DTDS=1;
		srs03_val.bits.BCE=1;
		srs03_val.bits.DMAE=1;
		sdhc_send(host, srs03_val, mmc_src_addr, DEFAULT_WAIT_CNT, "read dma multi-block start");
	} else {
		srs03_val.v = 0;
		srs03_val.bits.CIDX=17;
		srs03_val.bits.DPS=1;
		srs03_val.bits.CRCCE=1;
		srs03_val.bits.CICE=1;
		srs03_val.bits.RTS=2;
		srs03_val.bits.DTDS=1;
		srs03_val.bits.BCE=1;
		srs03_val.bits.DMAE=1;
		sdhc_send(host, srs03_val, mmc_src_addr, DEFAULT_WAIT_CNT, "read dma start");
	}

	// resume dma if DMAINT occur
	SRS12_ENIS srs12_val = {.v = 0};
	wait_if(({
				delay_cnt(1000);/*ldf 20230821 add*/
				srs12_val = host->srs_regs.srs12_enis;
				if (srs12_val.bits.DMAINT)
				{
#if 1/*ldf 20230817 add:: 参考裸测驱动修改*/
					u64 up_addr_next = (u64)(((((u64)host->srs_regs.srs23_asa2.v) & 0xffffffff) << 32) | host->srs_regs.srs22_asa1.v);
					udebug("DMAINT meet, update sdma address: next:0x%lx\r\n", up_addr_next);
					host->srs_regs.srs12_enis.bits.DMAINT = 1; // clear dma intterupt flag
					host->srs_regs.srs22_asa1.v = (up_addr_next) & 0xFFFFFFFF;
					host->srs_regs.srs23_asa2.v = ((up_addr_next)>> 32) & 0xFFFFFFFF;
#else
					udebug("DMAINT meet, update sdma address: 0x%lx\r\n", dst_addr+512*1024);
					host->srs_regs.srs12_enis.bits.DMAINT = 1;
					host->srs_regs.srs22_asa1.v = (dst_addr + 512*1024) & 0xFFFFFFFF;
					host->srs_regs.srs23_asa2.v = ((dst_addr + 512*1024)>> 32) & 0xFFFFFFFF;
#endif
				}
				srs12_val.bits.TC == 0; }), DEFAULT_WAIT_CNT*1000, "update sdma address TC");/*ldf 20230821 modify:: 配置成1bit模式，速率太慢，这里要加长延时*/
//	global_cur_cpu_cnt = read_c0_count();
//	uinfo("pre_cpu_cnt:0x%x, cur_cpu_cnt:0x%x, diff:%d, ms:%d\r\n", global_pre_cpu_cnt, global_cur_cpu_cnt, global_cur_cpu_cnt - global_pre_cpu_cnt, ((global_cur_cpu_cnt-global_pre_cpu_cnt)/(HZ/1000)));

	return 0;
}

int mmc_read_interrupt(struct sdhc_regs *host, u64 dst_addr, u64 mmc_src_addr, u64 block_cnt, u32 block_size)
{
	if (block_cnt == 0)
		return -1;
	udebug("mmc_src_addr:0x%x, dst_addr:0x%x, block_cnt:%d\r\n", mmc_src_addr, dst_addr, block_cnt);
	// dest memory address
	host->srs_regs.srs22_asa1.v = dst_addr & 0xFFFFFFFF;
	host->srs_regs.srs23_asa2.v = (dst_addr >> 32) & 0xFFFFFFFF;

	// block count
	//SDMABB = 7;       // system address boundary size: 2^(x+2) KB
	//TBS = 512;        // transfor block size Byte
	SRS01_BSR srs01_val = {.bits.SDMABB=7, .bits.TBS=block_size, .bits.BCCT=block_cnt};
	host->srs_regs.srs01_bsr = srs01_val;

	// sdma config
	host->srs_regs.srs10_hc1.bits.DMASEL = 0; // 0-sdma, 3-adma2(64-bit), 2-adma2(32-bit)

	// clear cpu timestamp for speed test
//	write_c0_count(0);
	// record cmd start timestamp
//	global_pre_cpu_cnt = read_c0_count();

	// send cmd
	SRS03_CTM srs03_val = {.v = 0};
	if (block_cnt > 1)
	{
		srs03_val.v = 0;
		srs03_val.bits.CIDX=18;
		srs03_val.bits.MSBS=1;
		srs03_val.bits.ACE=1;
		srs03_val.bits.DPS=1;
		srs03_val.bits.CRCCE=1;
		srs03_val.bits.CICE=1;
		srs03_val.bits.RTS=2;
		srs03_val.bits.DTDS=1;
		srs03_val.bits.BCE=1;
		srs03_val.bits.DMAE=1;
		sdhc_send_cmd_interrupt(host, srs03_val, mmc_src_addr, DEFAULT_WAIT_CNT, "read dma start");
	} else {
		srs03_val.v = 0;
		srs03_val.bits.CIDX=17;
		srs03_val.bits.DPS=1;
		srs03_val.bits.CRCCE=1;
		srs03_val.bits.CICE=1;
		srs03_val.bits.RTS=2;
		srs03_val.bits.DTDS=1;
		srs03_val.bits.BCE=1;
		srs03_val.bits.DMAE=1;
		sdhc_send_cmd_interrupt(host, srs03_val, mmc_src_addr, DEFAULT_WAIT_CNT, "read dma start");
	}

	wait_if(({
				delay_cnt(1000);/*ldf 20230821 add*/
				if(global_srs12_sts0.bits.DMAINT) // when meeting SDMABB interrupt
				{
#if 1/*ldf 20230817 add:: 参考裸测驱动修改*/
					u64 up_addr_next = (u64)(((((u64)host->srs_regs.srs23_asa2.v) & 0xffffffff) << 32) | host->srs_regs.srs22_asa1.v);
					udebug("DMAINT meet, update sdma address: next:0x%lx\r\n", up_addr_next);
					host->srs_regs.srs12_enis.bits.DMAINT = 1; // clear dma intterupt flag
					host->srs_regs.srs22_asa1.v = (up_addr_next) & 0xFFFFFFFF;
					host->srs_regs.srs23_asa2.v = ((up_addr_next)>> 32) & 0xFFFFFFFF;
#else
					udebug("DMAINT meet, update sdma address: 0x%lx\r\n", dst_addr+512*1024);
					global_srs12_sts0.bits.DMAINT = 0; // clear dma intterupt flag
					host->srs_regs.srs22_asa1.v = (dst_addr + 512*1024) & 0xFFFFFFFF;
					host->srs_regs.srs23_asa2.v = ((dst_addr + 512*1024)>> 32) & 0xFFFFFFFF;
#endif
				}
				global_srs12_sts0.bits.TC == 0;
				}), DEFAULT_WAIT_CNT, "update sdma address TC");

//	uinfo("pre_cpu_cnt:0x%x, cur_cpu_cnt:0x%x, diff:%d, ms:%d\r\n", global_pre_cpu_cnt, global_cur_cpu_cnt, global_cur_cpu_cnt - global_pre_cpu_cnt, ((global_cur_cpu_cnt-global_pre_cpu_cnt)/(HZ/1000)));
	return 0;
}

void format_print(u8 *buf, u32 len, u32 break_len)
{
    int i=0;
    int line = 0;
    for (i=0; i<len; i++)
    {
        if (i%break_len == 0)
            uclean("0x%x: ", i);
        uclean("%x ", buf[i]);
        if ((i+1)%break_len == 0)
            uclean("\r\n");
    }
    uclean("\r\n");
}

u32 g_csd_buf[128] = {0};
void mmc_read_ext_csd(struct sdhc_regs* host, int need_print)
{
	SRS01_BSR srs01_val = {.bits.SDMABB=7, .bits.TBS=512, .bits.BCCT=1};
	host->srs_regs.srs01_bsr = srs01_val;
	SRS03_CTM srs03_val = {.bits.CIDX = 8, .bits.CRCCE = 1, .bits.CICE=1, .bits.RTS=2, .bits.DPS=1, .bits.DTDS=1};
	sdhc_send(host, srs03_val, 0, DEFAULT_WAIT_CNT, "cmd8 read ext_csd");

	wait_if(host->srs_regs.srs09_psr.bits.BRE==0, 1000, "SRS09_PSR.BRE==0");
	int j = 0;
	for (j=0; j<512/4; j++)
	{
		g_csd_buf[j] = host->srs_regs.srs08_db.v;
	}
	if (need_print)
		format_print(g_csd_buf, 512, 16);
	host->srs_regs.srs12_enis.v = 0xffffffff;
}

void mmc_write_ext_csd(struct sdhc_regs* host, u32 addr, u32 wdata)
{
	SRS03_CTM srs03_val;
	srs03_val.v = 0;
	srs03_val.bits.CIDX=6;
	srs03_val.bits.CRCCE=1;
	srs03_val.bits.CICE=1;
	srs03_val.bits.RTS=3;
	MMC_ARG_CMD6 arg_cmd6 = {.bits.VALUE = wdata, .bits.INDEX=addr, .bits.ACCESS=3};
	sdhc_send(host, srs03_val, arg_cmd6.v, DEFAULT_WAIT_CNT, "cmd6 write ext_csd");
	wait_if(host->srs_regs.srs12_enis.bits.TC==0, DEFAULT_WAIT_CNT, "srs12 TC");
	host->srs_regs.srs12_enis.v = 0xffffffff;
}

void mmc_get_partition_info(struct sdhc_regs* host, int need_print)
{
	mmc_read_ext_csd(host, need_print);
	u8 *tmp_csd_buf = g_csd_buf;
	/*******************************************************************************************/
	//Boot Partition size = 128Kbytes 脳 BOOT_SIZE_MULT 
	u64 boot_partition_size = tmp_csd_buf[226]*128/1024;
	udebug("BOOT_PARTITION_SIZE:%dMB\r\n", boot_partition_size); //4MB
	/*******************************************************************************************/
	u64 rpmb_partition_size = tmp_csd_buf[168]*128/1024;
	udebug("RPMB_PARTITION_SIZE:%dMB\r\n", rpmb_partition_size); //4MB

	/*******************************************************************************************/
	//GP_SIZE_MULT_GP0 - GP_SIZE_MULT_GP3 [154:143]
	u8 GP_SIZE_MULT_1_0 = tmp_csd_buf[143];
	u8 GP_SIZE_MULT_1_1 = tmp_csd_buf[144];
	u8 GP_SIZE_MULT_1_2 = tmp_csd_buf[145];

	u8 GP_SIZE_MULT_2_0 = tmp_csd_buf[146];
	u8 GP_SIZE_MULT_2_1 = tmp_csd_buf[147];
	u8 GP_SIZE_MULT_2_2 = tmp_csd_buf[148];

	u8 GP_SIZE_MULT_3_0 = tmp_csd_buf[149];
	u8 GP_SIZE_MULT_3_1 = tmp_csd_buf[150];
	u8 GP_SIZE_MULT_3_2 = tmp_csd_buf[151];

	u8 GP_SIZE_MULT_4_0 = tmp_csd_buf[152];
	u8 GP_SIZE_MULT_4_1 = tmp_csd_buf[153];
	u8 GP_SIZE_MULT_4_2 = tmp_csd_buf[154];

	u8 HC_WP_GRP_SIZE = tmp_csd_buf[221];    //0x10
	u8 HC_ERASE_GRP_SIZE = tmp_csd_buf[224]; //1

	u64 GPP_PARTITION_1 =  (GP_SIZE_MULT_1_0 + GP_SIZE_MULT_1_1*(2^8) + GP_SIZE_MULT_1_2*(2^16)) * HC_WP_GRP_SIZE * HC_ERASE_GRP_SIZE / 1024; //
	u64 GPP_PARTITION_2 =  (GP_SIZE_MULT_2_0 + GP_SIZE_MULT_2_1*(2^8) + GP_SIZE_MULT_2_2*(2^16)) * HC_WP_GRP_SIZE * HC_ERASE_GRP_SIZE / 1024;
	u64 GPP_PARTITION_3 =  (GP_SIZE_MULT_3_0 + GP_SIZE_MULT_3_1*(2^8) + GP_SIZE_MULT_3_2*(2^16)) * HC_WP_GRP_SIZE * HC_ERASE_GRP_SIZE / 1024;
	u64 GPP_PARTITION_4 =  (GP_SIZE_MULT_4_0 + GP_SIZE_MULT_4_1*(2^8) + GP_SIZE_MULT_4_2*(2^16)) * HC_WP_GRP_SIZE * HC_ERASE_GRP_SIZE / 1024;

	udebug(
			"GPP_PARTITION_1:%dMB\r\n"
			"GPP_PARTITION_2:%dMB\r\n"
			"GPP_PARTITION_3:%dMB\r\n"
			"GPP_PARTITION_4:%dMB\r\n",
			GPP_PARTITION_1,
			GPP_PARTITION_2,
			GPP_PARTITION_3,
			GPP_PARTITION_4
	      );
}




#endif /* __EMMC_LIB_C__ */
