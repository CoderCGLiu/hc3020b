#ifndef  __SDCARD_LIB_H__
#define  __SDCARD_LIB_H__
#include "./sdhc_lib.h"

enum SD_MODE {
	E_SDCARD_DS     = 0,
	E_SDCARD_HS     = 1,
	E_UHS_I_SDR50   = 2,
	E_UHS_I_SDR104  = 3,
	E_UHS_I_DDR50   = 4,
};

enum SD_BIT_WIDTH {
	E_SD_1BITS = 0,
	E_SD_4BITS = 2,
};


//declare functions
extern int sdcard_get_card_id(struct sdhc_regs *host, u32 id_buf[4]);
extern u32 sdcard_get_rca(struct sdhc_regs *host);
extern int sdcard_init(struct sdhc_regs *host, u32 uhs_support);
extern int sdcard_set_mode(struct sdhc_regs *host, enum SD_BIT_WIDTH bit_width, u8 mode, u32 rca);
extern void sdcard_select_card(struct sdhc_regs* host, u32 rca_addr);
extern int sdcard_write(struct sdhc_regs *host, u64 dst_addr, u64 src_addr, u64 block_cnt, u32 block_size);
extern int sdcard_read(struct sdhc_regs *host, u64 src_addr, u64 dst_addr, u64 block_cnt, u32 block_size);
extern void sdcard_reset_disable_clock_tuning(struct sdhc_regs *host);
extern void sdcard_reset_restart_tuning(struct sdhc_regs *host);
extern void sdcard_stop_tuning(struct sdhc_regs *host);
extern void sdcard_start_retuning_without_clock_tuning_reset(struct sdhc_regs *host);
extern void sdcard_tuning(struct sdhc_regs *host);

#endif  /* __SDCARD_LIB_H__ */
