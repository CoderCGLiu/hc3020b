/* ********************************************
 * FILE NAME  : udebug.h
 * PROGRAMMER : zhaozz
 * START DATE : 2021-09-01 17:39:59
 * DESCIPTION : standalone udebug
 * *******************************************/

#ifndef  __UDEBUG_H__
#define  __UDEBUG_H__

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
//typedef unsigned long u64;

static union { char c[4]; unsigned long l;} endian_test = { {'l', '?', '?', 'b'}};
#define ENDIANNESS ((char)endian_test.l)

#define L2B32(little) (((little&0xff)<<24) | ((little&0xff00)<<8) | ((little&0xff0000)>>8) | ((little&0xff000000)>>24))

// ============================================================using which uart
#define USING_UART0

// ============================================================wait if
//#define WAIT_LOG
#ifdef WAIT_LOG
#define wait_udebug udebug
#define wait_uclean uclean
#define wait_always uclean
#define wait_error  uerror
#else
#define wait_udebug(fmt, args...) do{;}while(0)
#define wait_uclean(fmt, args...) do{;}while(0)
#define wait_always uclean
#define wait_error  uerror
#endif

#define wait_if(expr, cnt, desc) ({\
		wait_udebug("wait_if[%s][%d]", desc, cnt); \
		int _i = 0; \
		for (;_i < cnt; _i++) { if (!(expr)) break; if (99 == _i%100) /*wait_always(".")*/;} \
		if (_i == cnt) wait_error("->[%d/%d]wait timeout!\r\n", _i, cnt); \
		else wait_uclean("->[%d/%d]\r\n", _i, cnt); \
		_i;\
		})

// ============================================================wait if

// ============================================================log
#define LEVEL_NONE    0
#define LEVEL_DEBUG   1
#define LEVEL_INFO    2
#define LEVEL_ERROR   3
#define LEVEL_SIMPLE  4

#define ULOG_LEVEL    LEVEL_NONE//LEVEL_SIMPLE
#define uart_printf   printk//_PrintFormat
#define uart_printf_wait uart_printf

#if ULOG_LEVEL ==  LEVEL_DEBUG
#define udebug(fmt, args...) uart_printf("[DEBUG][%s:%d][%s] " fmt, __FILE__,__LINE__,__FUNCTION__,##args)
#define uinfo(fmt, args...) uart_printf("[INFO][%s:%d][%s] " fmt, __FILE__,__LINE__,__FUNCTION__,##args)
#define uerror(fmt, args...) uart_printf("[ERROR][%s:%d][%s] " fmt, __FILE__,__LINE__,__FUNCTION__,##args)
#define uclean uart_printf
#define ulog uinfo
#elif ULOG_LEVEL == LEVEL_INFO
#define udebug(fmt, args...) do {;} while(0)
#define uinfo(fmt, args...) uart_printf("[INFO][%d][%s] " fmt, __LINE__, __FUNCTION__, ##args)
#define uerror(fmt, args...) uart_printf("[ERROR][%d][%s] " fmt, __LINE__, __FUNCTION__, ##args)
#define uclean uart_printf
#define ulog uinfo
#elif ULOG_LEVEL == LEVEL_ERROR
#define udebug(fmt, args...) do {;} while(0)
#define uinfo(fmt, args...) do {;} while(0)
#define uerror(fmt, args...) uart_printf("[ERROR][%d][%s] " fmt, __LINE__, __FUNCTION__, ##args)
#define uclean uart_printf
#define ulog uinfo
#elif ULOG_LEVEL == LEVEL_SIMPLE
#define udebug(fmt, args...) uart_printf("[DEBUG][%d][%s] " fmt, __LINE__, __FUNCTION__, ##args)
#define uinfo(fmt, args...)  uart_printf("[INFO][%d][%s] " fmt, __LINE__, __FUNCTION__, ##args)
#define uerror(fmt, args...) uart_printf("[ERROR][%d][%s] " fmt, __LINE__, __FUNCTION__, ##args)
#define uclean uart_printf
#define ulog uinfo
#else
#define udebug 	//uart_printf
#define uinfo 	//uart_printf
#define uerror 	//uart_printf
#define uclean 	//uart_printf
#define ulog 	//uart_printf
#endif
// ============================================================log


// ============================================================read write Transfer
#define _P2V(addr)	(0x9000000000000000 | ((u64)(addr)))	/* physical address to virtual address */
#define _P2V_CACHE(addr)	(0xb000000000000000 | ((u64)(addr)))	/* physical address to virtual address */

#define READ8_REGISTER(x, y) do {			\
	y= *((volatile u8 *)(_P2V(x))) ;	\
} while(0)
#define READ_REGISTER(x, y) do {			\
	y = *((volatile u32*)(_P2V(x)));	\
} while(0)
#define READ16_REGISTER(x, y) do {			\
	y= *((volatile u16 *)(_P2V(x))) ;	\
} while(0)
#define READ64_REGISTER(x, y) do {			\
	y = *((volatile u64 *)(_P2V(x)));	\
} while(0)
#define WRITE8_REGISTER(x, y) do {			\
	*((volatile u8 *)(_P2V(x)))= (u8)y ;	\
} while(0)
#define WRITE16_REGISTER(x, y) do {			\
	*((volatile u16 *)(_P2V(x)))= y ;	\
} while(0)
#define WRITE_REGISTER(x, y) do {			\
	*((volatile u32 *)(_P2V(x))) = y;	\
} while(0)
#define WRITE64_REGISTER(x, y) do {			\
	*((volatile u64 *)(_P2V(x))) = y;	\
} while(0)

#define phx_read_u8(where)						\
	({                                                              \
	 u8 val;                                        \
	 READ8_REGISTER(where, val);				\
	 val;                                                    \
	 })
#define phx_read_u16(where)						\
	({                                                              \
	 volatile u16 val;                                       \
	 READ16_REGISTER(where, val);				\
	 val;                                                    \
	 })
#define phx_read_u32(where)						\
	({                                                              \
	 volatile u32 val;                                       \
	 READ_REGISTER(where, val);				\
	 val;                                                    \
	 })
#define phx_read_u64(where)						\
	({                                                              \
	 volatile u64 val;                                       \
	 READ64_REGISTER(where, val);				\
	 val;                                                    \
	 })

#define phx_write_u8(where, val)   WRITE8_REGISTER(where, val)
#define phx_write_u16(where, val)  WRITE16_REGISTER(where, val)
#define phx_write_u32(where, val)  WRITE_REGISTER(where, val)
#define phx_write_u64(where, val)  WRITE64_REGISTER(where, val)
#define phx_write_mask_bit(where, mask, val)				\
	({								\
	 volatile u32 tmp;					\
	 tmp = ((phx_read_u32(where)) & (~(mask))) | ((val) & (mask));\
	 phx_write_u32(where, tmp);				\
	 })
// ============================================================read write Transfer
/***********************************cache read write********************************************************/
#define READ_CACHE_REGISTER(x, y) do {				\
	y = *((volatile u32 *)(_P2V_CACHE(x)));	\
} while(0)

#define WRITE_CACHE_REGISTER(x, y) do {				\
	*((volatile u32 *)(_P2V_CACHE(x))) = y;	\
} while(0)

#define phx_read_cache_u32(where)						\
	({                                                              \
	 volatile u32 val;                                       \
	 READ_CACHE_REGISTER(where, val);				\
	 val;                                                    \
	 })

#define phx_write_cache_u32(where, val)  WRITE_CACHE_REGISTER(where, val)

// ============================================================printf uart
typedef __builtin_va_list __gnuc_va_list;
#define va_start(v,l)   __builtin_va_start(v,l)
#define va_end(v)       __builtin_va_end(v)
#define va_arg(v,l)     __builtin_va_arg(v,l)
typedef __gnuc_va_list va_list;

// ============================================================printf uart

/***************************************exports begin****************************************************/
extern void delay_cnt(int cnt);
/***************************************exports end****************************************************/

#endif  /* __UDEBUG_H__ */
