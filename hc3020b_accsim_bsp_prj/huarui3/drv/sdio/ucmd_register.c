/* ******************************************************************************************
 * FILE NAME   : ucmd_register.c
 * PROGRAMMER  : zhaozz
 * DESCRIPTION : add sdhc ucmd command
 * DATE        : 2022-07-14 13:41:12
 * *****************************************************************************************/
#ifndef  __UCMD_REGISTER_C__
#define  __UCMD_REGISTER_C__

#include "./sdhc_lib.c"
#include "./emmc_lib.c"
#include "./sdcard_lib.c"
#include <stdio.h>
#include <stdlib.h>
#include <reworks/types.h>
#include <driver.h>
#include <devnum.h>


static int sdio_init_flag[SDIO_DEV_NUM] = {0};/*ldf 20230817 add*/

extern unsigned long long hrKmToPhys(void *addr_mapped);
static unsigned int sdio_dma_mem_size = 0x1000000UL; /*ldf 20230817 add:: sdio 用于dma传输的内存大小  16MB*/
static unsigned long long sdio_dma_read_mem[SDIO_DEV_NUM] = {0}; /*ldf 20230817 add:: sdio 用于dma读的内存地址,在bsp mmu时进行赋值*/
static unsigned long long sdio_dma_write_mem[SDIO_DEV_NUM] = {0}; /*ldf 20230817 add:: sdio 用于dma写的内存地址,在bsp mmu时进行赋值*/

int sdio_dev_num_get()
{
	return SDIO_DEV_NUM;
}
unsigned int sdio_dma_mem_size_get()
{
	return sdio_dma_mem_size;
}

unsigned long long sdio_dma_read_mem_get(int controller)
{
	return sdio_dma_read_mem[controller];
}
void sdio_dma_read_mem_set(int controller, unsigned long long dma_read_mem)
{
	sdio_dma_read_mem[controller] = dma_read_mem;
}
unsigned long long sdio_dma_write_mem_get(int controller)
{
	return sdio_dma_write_mem[controller];
}
void sdio_dma_write_mem_set(int controller, unsigned long long dma_write_mem)
{
	sdio_dma_write_mem[controller] = dma_write_mem;
}


typedef struct{
	char					dev_name[16];
	char					dev_partition[16];
	char					mount_point[8];
	char					fs_type[8];
	struct block_device*	pBlkDev;
	int 					controller;
} SDIO_BLKDEV_INFO;
static SDIO_BLKDEV_INFO g_BlkDev[2] = {/*ldf 20230821 add*/
		[0] = {
			.controller 	= 0,
			.dev_name 		= "/dev/emmc0",
			.dev_partition 	= "/dev/emmc0p1",
			.mount_point 	= "/emmc0",
			.fs_type 		= "dosfs",
			.pBlkDev 		= NULL
		},
		[1] = {
			.controller 	= 1,
			.dev_name 		= "/dev/emmc1",
			.dev_partition 	= "/dev/emmc1p1",
			.mount_point 	= "/emmc1",
			.fs_type 		= "dosfs",
			.pBlkDev 		= NULL
		},
		/*END*/
};

int cmd_sdhc_init(u8 controller, u8 voltage, u32 kfreq)
{
	struct sdhc_regs *host = controller ? SDHC1_REGS : SDHC0_REGS;
	sdhc_init(host/*SDHC0_REGS*/, voltage, kfreq);
	return 0;
}

int cmd_sdhc_send(u8 controller, SRS03_CTM srs03_val, u32 cmd_args, u64 wait_cnt, const char* desc)
{
	struct sdhc_regs *host = controller ? SDHC1_REGS : SDHC0_REGS;
	sdhc_send(host/*SDHC0_REGS*/, srs03_val, cmd_args, wait_cnt, desc/*"debug send"*/);
	return 0;
}

int cmd_print_card_status(u8 controller, u32 rca_addr)
{
	struct sdhc_regs *host = controller ? SDHC1_REGS : SDHC0_REGS;
	print_card_status(host/*SDHC0_REGS*/, rca_addr);
	return 0;
}
int cmd_print_host_reg_addr(u8 controller)
{
	struct sdhc_regs *host = controller ? SDHC1_REGS : SDHC0_REGS;
	print_host_reg_addr(host/*SDHC0_REGS*/);
	return 0;
}
int cmd_print_reg_resp(u8 controller)
{
	struct sdhc_regs *host = controller ? SDHC1_REGS : SDHC0_REGS;
	print_reg_resp(host/*SDHC0_REGS*/);
	return 0;
}

int cmd_print_srs_cap(u8 controller)
{
	struct sdhc_regs *host = controller ? SDHC1_REGS : SDHC0_REGS;
	print_cap_srs16(host/*SDHC0_REGS*/);
	print_cap_srs17(host/*SDHC0_REGS*/);
	print_cap_srs18(host/*SDHC0_REGS*/);
	print_cap_srs19(host/*SDHC0_REGS*/);
	return 0;
}


/*************************sdcard commands******************************************************************/
int sdio_sdcard_init(u8 controller, int mode)
{
	if(sdio_init_flag[controller])
		return 0;
	
	struct sdhc_regs *host = controller ? SDHC1_REGS : SDHC0_REGS;
	if (controller == 0)
	{
		switch (mode) {
			case E_SDCARD_DS:
				sdhc_user_reset(host);
				sdhc_init(host, 2, 400);
				sdcard_init(host, 0);
				sdcard_select_card(host, g_sd_card_rca);
				sdcard_set_mode(host, E_SD_1BITS, E_SDCARD_DS, g_sd_card_rca);//bit_width, mode, rca
				sdhc_set_clock(host, 20000);
				break;
			case E_SDCARD_HS:
				break;
			case E_UHS_I_SDR50:
				sdhc_user_reset(host);
				sdhc_init(host, 2, 20000);
				sdcard_init(host, 1);
				sdcard_select_card(host, g_sd_card_rca);
				sdcard_set_mode(host, E_SD_4BITS, E_UHS_I_SDR50, g_sd_card_rca);//bit_width, mode, rca
				dfi_sdr50(host);
				sdhc_set_clock(host, 50000);
				break;
			case E_UHS_I_SDR104:
				sdhc_user_reset(host);
				sdhc_init(host, 2, 20000);
				sdcard_init(host, 1);
				sdcard_select_card(host, g_sd_card_rca);
				sdcard_set_mode(host, E_SD_4BITS, E_UHS_I_SDR104, g_sd_card_rca);//bit_width, mode, rca
				sdhc_set_clock(host, 100000);
				sdcard_tuning(host);
				break;
		}
	} else {
		sdhc_user_reset(host);
		sdhc_init(host, 2, 400);
		sdcard_init(host, 0);
		sdcard_select_card(host, g_sd_card_rca);
		sdcard_set_mode(host, E_SD_1BITS, E_SDCARD_DS, g_sd_card_rca);//bit_width, mode, rca
		sdhc_set_clock(host, 20000);
	}
	
	sdio_init_flag[controller] = 1;
	return 0;
}

int cmd_write_sd(u8 controller, u64 sd_waddr, u64 src_addr, u64 wlen)
{
	if (controller == 0)
	{
		sdcard_write(SDHC0_REGS, sd_waddr, src_addr, (wlen + 512-1)/512, 512);
	} else {
		sdcard_write(SDHC1_REGS, sd_waddr, src_addr, (wlen + 512-1)/512, 512);
	}
	return 0;
}

int cmd_read_sd(u8 controller, u64 sd_raddr, u64 dst_addr, u64 rlen)
{
	if (controller == 0)
	{
		sdcard_read(SDHC0_REGS, dst_addr, sd_raddr, (rlen + 512-1)/512,  512);
	} else {
		sdcard_read(SDHC1_REGS, dst_addr, sd_raddr, (rlen + 512-1)/512, 512);
	}
	return 0;
}

int cmd_sdcard_get_rca(u8 controller)
{
	struct sdhc_regs *host = controller ? SDHC1_REGS : SDHC0_REGS;
	sdcard_get_rca(host/*SDHC0_REGS*/);
	return 0;
}

int cmd_sdcard_get_card_id(u8 controller, u32 id_buf[4])
{
	struct sdhc_regs *host = controller ? SDHC1_REGS : SDHC0_REGS;
	sdcard_get_card_id(host/*SDHC0_REGS*/, id_buf);
	return 0;

}


/*************************mmc commands******************************************************************/
int cmd_write_mmc(int controller, int block_addr, int blocks, char *buffer)
{
	struct sdhc_regs *host = controller ? SDHC1_REGS : SDHC0_REGS;
	u64 dma_addr_loop = sdio_dma_write_mem_get(controller);
	u32 block_size = EMMC_BLOCK_SIZE;
	int blocks_loop = 0;
	int block_addr_loop = block_addr;
	int loop = 0;
	int i = 0;
	u64 len = blocks * block_size;//byte
	u64 len_once = sdio_dma_mem_size_get();
	u64 len_last = 0;
	u64 len_loop = 0;
	char *pbuffer = buffer;
	
	if((block_addr + blocks) >= EMMC_BLOCK_COUNT)
	{
		uerror("block_addr:0x%lx, blocks:0x%lx\n", i, block_addr, blocks);
		return -1;
	}
	
	udebug("block_addr:0x%lx, blocks:0x%lx, buffer:0x%lx\r\n", block_addr, blocks, buffer);
	
	if(len < len_once)
	{
		memcpy(dma_addr_loop, pbuffer, len);
		if(mmc_write(host, block_addr, dma_addr_loop, blocks, block_size) < 0)
		{
			uerror("block_addr:0x%lx, dma_addr:0x%lx\n", i, block_addr_loop, dma_addr_loop);
			return -1;
		}
		return 0;
	}
	
	loop = len / len_once;
	len_last = len % len_once;
	if(len_last != 0)
		loop++;
	udebug("len:0x%lx, len_once:0x%lx, len_last:0x%lx, loop:%d\r\n", len, len_once, len_last, loop);
	
	for(i = 0; i < loop; i++)
	{
		len_loop = ((i == (loop - 1)) && (len_last != 0)) ? len_last : len_once;//写入的数据长度
		blocks_loop = len_loop / block_size;
		if((len_loop%block_size) != 0)
			blocks_loop++;
		
		udebug("i=%d, len_loop:0x%lx, blocks_loop:0x%lx, block_size:0x%lx\r\n", i, len_loop, blocks_loop, block_size);
		udebug("i=%d, dma_addr_loop:0x%lx, pbuffer:0x%lx, block_addr_loop:0x%lx\r\n", i, dma_addr_loop, pbuffer, block_addr_loop);
		
		memcpy(dma_addr_loop, pbuffer, len_loop);
		if(mmc_write(host, block_addr_loop, dma_addr_loop, blocks_loop, block_size) < 0)
		{
			uerror("i=%d, block_addr_loop:0x%lx, dma_addr_loop:0x%lx\n", i, block_addr_loop, dma_addr_loop);
			return -1;
		}
		
		pbuffer = pbuffer + len_loop;
		block_addr_loop = block_addr_loop + blocks_loop;
	}
	
	return 0;
}


int cmd_read_mmc(int controller, int block_addr, int blocks, char *buffer)
{
	struct sdhc_regs *host = controller ? SDHC1_REGS : SDHC0_REGS;
	u64 dma_addr_loop = sdio_dma_read_mem_get(controller);
	u32 block_size = EMMC_BLOCK_SIZE;
	int blocks_loop = 0;
	int block_addr_loop = block_addr;
	int loop = 0;
	int i = 0;
	u64 len = blocks * block_size;//byte
	u64 len_once = sdio_dma_mem_size_get();
	u64 len_last = 0;
	u64 len_loop = 0;
	char *pbuffer = buffer;
	
	if((block_addr + blocks) >= EMMC_BLOCK_COUNT)
	{
		uerror("block_addr:0x%lx, blocks:0x%lx\n", i, block_addr, blocks);
		return -1;
	}
	
	udebug("block_addr:0x%lx, blocks:0x%lx, buffer:0x%lx\r\n", block_addr, blocks, buffer);
	
	if(len < len_once)
	{
		if(mmc_read(host, block_addr, dma_addr_loop, blocks, block_size) < 0)
		{
			uerror("block_addr:0x%lx, dma_addr:0x%lx\n", i, block_addr_loop, dma_addr_loop);
			return -1;
		}
		memcpy(pbuffer, dma_addr_loop, len);
		return 0;
	}
	
	loop = len / len_once;
	len_last = len % len_once;
	if(len_last != 0)
		loop++;
	udebug("len:0x%lx, len_once:0x%lx, len_last:0x%lx, loop:%d\r\n", len, len_once, len_last, loop);
	
	for(i = 0; i < loop; i++)
	{
		len_loop = ((i == (loop - 1)) && (len_last != 0)) ? len_last : len_once;//写入的数据长度
		blocks_loop = len_loop / block_size;
		if((len_loop%block_size) != 0)
			blocks_loop++;
		
		udebug("i=%d, len_loop:0x%lx, blocks_loop:0x%lx, block_size:0x%lx\r\n", i, len_loop, blocks_loop, block_size);
		udebug("i=%d, dma_addr_loop:0x%lx, pbuffer:0x%lx, block_addr_loop:0x%lx\r\n", i, dma_addr_loop, pbuffer, block_addr_loop);
		
		if(mmc_read(host, block_addr_loop, dma_addr_loop, blocks_loop, block_size) < 0)
		{
			uerror("i=%d, block_addr_loop:0x%lx, dma_addr_loop:0x%lx\n", i, block_addr_loop, dma_addr_loop);
			return -1;
		}
		memcpy(pbuffer, dma_addr_loop, len_loop);
		
		pbuffer = pbuffer + len_loop;
		block_addr_loop = block_addr_loop + blocks_loop;
	}
	
	return 0;
}

int cmd_mmc_write_ext_csd(u8 controller, u32 addr, u32 wdata)
{
	struct sdhc_regs *host = controller ? SDHC1_REGS : SDHC0_REGS;
	mmc_write_ext_csd(host/*SDHC0_REGS*/, addr, wdata);
	return 0;

}

int cmd_mmc_read_ext_csd(u8 controller, int need_print)
{
	struct sdhc_regs *host = controller ? SDHC1_REGS : SDHC0_REGS;
	mmc_read_ext_csd(host/*SDHC0_REGS*/, need_print);
	return 0;

}

int cmd_mmc_get_partition_info(u8 controller, int need_print)
{
	struct sdhc_regs *host = controller ? SDHC1_REGS : SDHC0_REGS;
	mmc_get_partition_info(host/*SDHC0_REGS*/, need_print);
	return 0;
}

#if 1/*ldf 20230821 add:: emmc创建块设备*/
int emmc_blkdev_read(
		struct block_device *blkdev, 
		int block_addr, 
		int blocks, 
		char *buffer)
{
	int ret = 0;
	int controller = (((u64)blkdev) == ((u64)g_BlkDev[0].pBlkDev)) ? 0 : 1;

	ret = cmd_read_mmc(controller, block_addr, blocks, buffer);
	
	return ret;
}

int emmc_blkdev_write(
		struct block_device *blkdev, 
		int block_addr, 
		int blocks, 
		char *buffer)
{

	int ret = 0;
	int controller = (((u64)blkdev) == ((u64)g_BlkDev[0].pBlkDev)) ? 0 : 1;

	ret = cmd_write_mmc(controller, block_addr, blocks, buffer);
	
	return ret;
}

int emmc_dev_create(int controller)
{
    int     bytesPerBlk 	= EMMC_BLOCK_SIZE;  
    int     blksPerTrack	= 0;
    int     nBlocks			= EMMC_BLOCK_COUNT;   
    int 	ret = 0;
    
	/* ptr to struct block_device struct in SDCARD_DEV */
    struct block_device *pBlkDev;  

    printk("%s: %d,%d,%d\n", __func__, bytesPerBlk, blksPerTrack, nBlocks);

    /* Set up defaults for any values not specified */
    if (bytesPerBlk <= 0 || nBlocks <= 0)
    {
        printk("create emmc device failed, invalid parameter.");
        return -1;
    }

    /* Initialize struct block_device structure (in SDCARD_DEV) */
    pBlkDev = (struct block_device *)malloc(sizeof(struct block_device));
    if (pBlkDev == NULL)
    {
        printk("malloc block_device error\n");
        return -1;
    }
    else
    {
        memset(pBlkDev, 0x0, sizeof(struct block_device));
    }
    g_BlkDev[controller].pBlkDev = pBlkDev;/*ldf 20230821 add*/
    
//    printk("<**DEBUG**> [%s():_%d_]\n", __FUNCTION__, __LINE__);

    pBlkDev->bd_nBlocks      = (block_t)nBlocks;/* number of blocks */
    pBlkDev->bd_bytesPerBlk  = bytesPerBlk;     /* bytes per block */
    pBlkDev->bd_blksPerTrack = blksPerTrack;    /* blocks per track */
    pBlkDev->bd_nOffset      = 0;              /* block offset */

    pBlkDev->bd_nHeads       = 1;               /* one "head" */
    pBlkDev->bd_removable    = FALSE;           /* not removable */
    pBlkDev->bd_retry        = 1;               /* retry count */
    pBlkDev->bd_mode         = O_RDWR;          /* initial mode for device */
    pBlkDev->bd_readyChanged = TRUE;            /* new ready status */

    pBlkDev->bd_blkRd        = emmc_blkdev_read;     /* read block function */
    pBlkDev->bd_blkWrt       = emmc_blkdev_write;    /* write block function */
    pBlkDev->bd_ioctl        = NULL;    		/* ioctl function */
    pBlkDev->bd_reset        = NULL;            /* no reset function */
    pBlkDev->bd_statusChk    = NULL;            /* no check-status function */
    pBlkDev->bd_devShow      = NULL;			/*设备信息显示接口*/		

    printk("[%s:%d] start register\n",__FUNCTION__,__LINE__);

    ret =register_blkdev(g_BlkDev[controller].dev_name, SDCARD_MAJOR, pBlkDev);  
    if(ret != 0)
    	printk("register_blkdev failed.\n");
    printk("[%s:%d] register_blkdev success!\n",__FUNCTION__,__LINE__);
    return 0;
}
#endif


int sdio_emmc_init(int controller, int fmt_state)
{
	struct sdhc_regs *host = controller ? SDHC1_REGS : SDHC0_REGS;
	int ret = 0;
	
	if(sdio_init_flag[controller])
		return 0;
	
	sdhc_user_reset(host);
	sdhc_init(host, 2, 398);
	mmc_card_init(host, EMMC_CARD_ADDRESS);
	mmc_card_set_mode(host, E_SDR, E_1BITS);
	dfi_sdr(host);
	sdhc_set_clock(host, 20000);
	
	ret = emmc_dev_create(controller);
	if(ret == 0)
	{
		switch(fmt_state)
		{
		case 0://NO
			break;
		case 1://AUTO
			ret = mount(g_BlkDev[controller].fs_type, g_BlkDev[controller].dev_partition, g_BlkDev[controller].mount_point);
			if(ret != 0)
			{
				printf("%s mount %s failed\n",g_BlkDev[controller].dev_partition,g_BlkDev[controller].fs_type);
				return -1;
			}
			break;
		case 2://YES
			ret = fdisk(g_BlkDev[controller].dev_name,100,0,0,0);
			if(ret != 0)
			{
				printf("%s fdisk failed\n",g_BlkDev[controller].dev_name);
				return -1;
			}
			ret = format(g_BlkDev[controller].fs_type,g_BlkDev[controller].dev_partition);
			if(ret != 0)
			{
				printf("%s format %s failed\n",g_BlkDev[controller].dev_partition,g_BlkDev[controller].fs_type);
				return -1;
			}
			ret = mount(g_BlkDev[controller].fs_type, g_BlkDev[controller].dev_partition, g_BlkDev[controller].mount_point);
			if(ret != 0)
			{
				printf("%s mount %s failed\n",g_BlkDev[controller].dev_partition,g_BlkDev[controller].fs_type);
				return -1;
			}
			break;
		default:;
		}
	}
	
	sdio_init_flag[controller] = 1;
	return 0;
}

/*************************************** test func********************************************************************/
void test_sdcard(u8 controller)
{
	sdio_sdcard_init(controller, 0);
    cmd_sdcard_get_card_id(controller, 0);
}

void test_mmc(u8 controller)
{
	sdio_emmc_init(controller, 1);
    cmd_mmc_get_partition_info(controller, 1);
}


static int CheckUint8(const unsigned char *pSrc, const unsigned char *pDst, unsigned int length)
{
    int index = 0;
    unsigned char valSrc = 0;
    unsigned char valDst = 0;
    int err_num = 0;
    
    printf(" ##CheckData## pSrc: 0x%016lX, pDst: 0x%016lX, length: %lu = 0x%016lX\n", (unsigned long)pSrc, (unsigned long)pDst, length, length);
    for(index = 0; index < length; index ++)
    {
    	valSrc = *(pSrc + index);
    	valDst = *(pDst + index);
		if(valSrc != valDst)
		{
			err_num++;
			if(err_num <= 0x10)
			{
				printf("<Check Error> index=0x%x, val=0x%x, except_val=0x%x\n",\
						index,valDst,valSrc);
			}
//			return -1;
		}
    }
    
    if(err_num == 0)
    	printf("   Check OK!\n");
    else
    	printf("   Check Failed! err_num = 0x%x\n",err_num);
    
	return err_num;
}
static void DumpUint8(const unsigned char *pAddr, unsigned int length)
{
    int index = 0;

    printf("pAddr: 0x%016lX, length: %lu = 0x%016lX", (unsigned long)pAddr, length, length);
    for (index = 0; index < length; index++)
    {
        if (index % 0x10 == 0)
        {
            printf("\n0x%08lX: ", index);
        }
        printf("0x%02X ", *(unsigned char *)(pAddr + index));
    }

    printf("\n");
}

void Dump_sdio_emmc(int controller, unsigned long block_addr, unsigned int blocks)
{
	u32 block_size = EMMC_BLOCK_SIZE;
	u32 test_len = blocks * block_size;
	unsigned char *recv_buf = (unsigned char *)malloc(test_len);
	
	if(recv_buf != 0)
		memset(recv_buf, 0, test_len);
	else
		return;
	
	cmd_read_mmc(controller, block_addr, blocks, recv_buf);
	
	DumpUint8(recv_buf, test_len);
	
	free(recv_buf);
}

void test_sdio_emmc_write(int controller, unsigned long block_addr, unsigned int blocks, unsigned int first_data, int is_DumpData)
{
	int i = 0;
	u32 block_size = EMMC_BLOCK_SIZE;
	u32 test_len = blocks * block_size;
    unsigned char *send_buf = (unsigned char *)malloc(test_len);
    
    if(send_buf != 0)
    	memset(send_buf, 0, test_len);
	else
		return;
    
    for (i=0; i<test_len; i++)
    {
    	send_buf[i] = first_data + i;
    }
	
    cmd_write_mmc(controller, block_addr, blocks, send_buf);
	printf("    sdio emmc write finished.\n");
    
    if(is_DumpData)
    	DumpUint8(send_buf, test_len);

	free(send_buf);
}

void test_sdio_emmc_rw(int controller, unsigned long block_addr, unsigned int blocks, unsigned int first_data, int is_DumpData)
{
	int i = 0;
	u32 block_size = EMMC_BLOCK_SIZE;
	u32 test_len = blocks * block_size;
	int err_num;
    unsigned char *send_buf = (unsigned char *)malloc(test_len);
    unsigned char *recv_buf = (unsigned char *)malloc(test_len);
    
    if(send_buf != 0)
    	memset(send_buf, 0, test_len);
	else
		return;
    
    if(recv_buf != 0)
    {
    	memset(recv_buf, 0, test_len);
    }
	else
	{
		free(send_buf);
		return;
	}
    
    for (i=0; i<test_len; i++)
    {
    	send_buf[i] = first_data + i;
    }
	
    cmd_write_mmc(controller, block_addr, blocks, send_buf);
	printf("    sdio emmc write finished.\n");
	sleep(1);
	cmd_read_mmc(controller, block_addr, blocks, recv_buf);
	
    err_num = CheckUint8(send_buf, recv_buf, test_len);
    if(err_num > 0)
    	printf("  SDIO EMMC Test Failed!, err_num = 0x%x\n",err_num); 
    else
    	printf("  SDIO EMMC Test OK!\n"); 
    
    if(is_DumpData)
    	DumpUint8(recv_buf, test_len);
    
#if 0
    printf("======>SDIO EMMC Check DMA_R_MEM - DMA_W_MEM: \n");
    err_num = CheckUint8(sdio_dma_write_mem[0], sdio_dma_read_mem[0], test_len);
    if(err_num > 0)
    	printf("  SDIO EMMC Test Failed!, err_num = 0x%x\n",err_num); 
    else
    	printf("  SDIO EMMC Test OK!\n");
    printf("======>SDIO EMMC Check DMA_W_MEM - send_buf: \n");
    err_num = CheckUint8(send_buf, sdio_dma_write_mem[0], test_len);
    if(err_num > 0)
    	printf("  SDIO EMMC Test Failed!, err_num = 0x%x\n",err_num); 
    else
    	printf("  SDIO EMMC Test OK!\n");
    printf("======>SDIO EMMC Check DMA_R_MEM - recv_buf: \n");
    err_num = CheckUint8(sdio_dma_read_mem[0], recv_buf, test_len);
    if(err_num > 0)
    	printf("  SDIO EMMC Test Failed!, err_num = 0x%x\n",err_num); 
    else
    	printf("  SDIO EMMC Test OK!\n");
#endif
	
	free(send_buf);
	free(recv_buf);
}

#endif  /* __UCMD_REGISTER_C__ */
