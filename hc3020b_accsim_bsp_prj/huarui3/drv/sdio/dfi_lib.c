#ifndef __DFI_LIB_C__
#define __DFI_LIB_C__
#include "./sdhc_lib.h"

enum DFI_MODE {
	DS = 1,
	HS,
	SDR,
	SDR50,
	SDR104,
	DDR,
	DDR50,
	HS200,
	HS400,
	HS400ES,
};

static void dfi_config(struct sdhc_regs *host)
{
	host->hrs_regs.hrs09_pcs.bits.PHY_SW_RESET = 0; //dfi reset
}

static void dfi_release(struct sdhc_regs *host)
{
	host->hrs_regs.hrs09_pcs.bits.PHY_SW_RESET = 1;
	wait_if(!host->hrs_regs.hrs09_pcs.bits.PHY_INIT_COMPLETE, HZ/20000, "hrs09 PHY_INIT_COMPLETE");
}

void dfi_init(struct sdhc_regs *host)
{
	dfi_config(host);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DQS_TIMING_REG; // select phy_dqs_timing_reg
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = ( 1<<EXT_LPBK_DQS | 1<<LPBK_DQS | 1<<PHONY_DQS | 1<<PHONY_DQS_CMD);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_GATE_LPBK_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (1<< SYNC_METHOD | 0<<SW_HALF_CYCLE_SHIFT | 52<<RD_DEL_SEL | 1<<UNDERRUN_SUPPRESS | 1<<GATE_CFG_ALWAYS_ON);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DLL_MASTER_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = ((host->hrs_regs.hrs05_prdp.v & ~(1<<23 | 7<<20 | 0xff<0))
			| 1<<DLL_BYPASS_MODE | 2<<20 | 4<<DLL_START_POINT);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DLL_SLAVE_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (0<< READ_DQS_CMD_DELAY | 0<<CLK_WRDQS_DELAY | 0<<CLK_WRDQS_DELAY | 0<<READ_DQS_DELAY);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = 0x2080;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = host->hrs_regs.hrs05_prdp.v & ~(0x3F<<4); //phony_dqs_timing=0

	dfi_release(host);

	u32 read_val = sdhc_read_phy(host, 0x2000);
	sdhc_write_phy(host, 0x2000, (read_val & 0x7FFFF8) | 0<<31 | 0<<27 | 0<<24 | 1<<0);

	//0XFFFE7EE3
	HRS09_PCS hrs09_val = host->hrs_regs.hrs09_pcs;
	hrs09_val.bits.RDDATA_EN = 1;
	hrs09_val.bits.RDCMD_EN = 1;
	hrs09_val.bits.EXTENDED_WR_MODE = 1;
	hrs09_val.bits.EXTENDED_RD_MODE = 1;
	host->hrs_regs.hrs09_pcs = hrs09_val;

	//0xFFF0FFFF
	host->hrs_regs.hrs10_hcsspa.bits.HCSDCLKADJ = 1;

	HRS16_HSGE hrs16_val = {.v=0};
	hrs16_val.bits.WRDATA0_DLY = 1;
	hrs16_val.bits.WRCMD0_DLY = 1;
	host->hrs_regs.hrs16_hsge = hrs16_val;

	HRS07_IDI hrs07_val = {.v = 0};
	hrs07_val.bits.RW_COMPENSATE = 9;
	host->hrs_regs.hrs07_idi = hrs07_val;
}

void dfi_ds(struct sdhc_regs *host)
{
	dfi_config(host);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DQS_TIMING_REG; // select phy_dqs_timing_reg
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = ( 1<<EXT_LPBK_DQS | 1<<LPBK_DQS | 1<<PHONY_DQS | 1<<PHONY_DQS_CMD);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_GATE_LPBK_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (1<< SYNC_METHOD | 0<<SW_HALF_CYCLE_SHIFT | 52<<RD_DEL_SEL | 1<<UNDERRUN_SUPPRESS | 1<<GATE_CFG_ALWAYS_ON);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DLL_MASTER_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (host->hrs_regs.hrs05_prdp.v & ~(1<<23 | 7<<20 | 0xff<0)
			| 1<<DLL_BYPASS_MODE | 2<<20 | 4<<DLL_START_POINT);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DLL_SLAVE_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (0<< READ_DQS_CMD_DELAY | 0<<CLK_WRDQS_DELAY | 0<<CLK_WRDQS_DELAY | 0<<READ_DQS_DELAY);

	//host->hrs_regs.hrs04_pra.bits.PHYREGADDR = 0x2080;
	//host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = host->hrs_regs.hrs05_prdp.v & ~(0x3F<<4);

	dfi_release(host);

	u32 read_val = sdhc_read_phy(host, 0x2000);
	sdhc_write_phy(host, 0x2000, (read_val & 0x7FFFF8) | 0<<31 | 0<<27 | 0<<24 | 1<<0);

	//0XFFFE7FF3
	HRS09_PCS hrs09_val = host->hrs_regs.hrs09_pcs;
	hrs09_val.bits.EXTENDED_WR_MODE = 1;
	hrs09_val.bits.EXTENDED_RD_MODE = 1;
	hrs09_val.bits.RDCMD_EN = 1;
	hrs09_val.bits.RDDATA_EN = 1;
	host->hrs_regs.hrs09_pcs = hrs09_val;

	//0xFFF0FFFF
	host->hrs_regs.hrs10_hcsspa.bits.HCSDCLKADJ = 2;

	//0 8
	HRS16_HSGE hrs16_val = {.v=0};
	hrs16_val.bits.WRCMD0_DLY = 1;
	hrs16_val.bits.WRDATA0_DLY = 1;
	host->hrs_regs.hrs16_hsge = hrs16_val;

	HRS07_IDI hrs07_val = {.v = 0};
	hrs07_val.bits.RW_COMPENSATE = 9;
	host->hrs_regs.hrs07_idi = hrs07_val;
}

void dfi_hs(struct sdhc_regs *host)
{
	dfi_config(host);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DQS_TIMING_REG; // select phy_dqs_timing_reg
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = ( 1<<EXT_LPBK_DQS | 1<<LPBK_DQS | 1<<PHONY_DQS | 1<<PHONY_DQS_CMD);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_GATE_LPBK_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (1<< SYNC_METHOD | 0<<SW_HALF_CYCLE_SHIFT | 52<<RD_DEL_SEL | 1<<UNDERRUN_SUPPRESS | 1<<GATE_CFG_ALWAYS_ON);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DLL_MASTER_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (host->hrs_regs.hrs05_prdp.v & ~(1<<23 | 7<<20 | 0xff<0)
			| 1<<DLL_BYPASS_MODE | 2<<20 | 4<<DLL_START_POINT);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DLL_SLAVE_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (0<< READ_DQS_CMD_DELAY | 0<<CLK_WRDQS_DELAY | 0<<CLK_WRDQS_DELAY | 0<<READ_DQS_DELAY);

	//host->hrs_regs.hrs04_pra.bits.PHYREGADDR = 0x2080;
	//host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = host->hrs_regs.hrs05_prdp.v & ~(0x3F<<4);

	dfi_release(host);

	u32 read_val = sdhc_read_phy(host, 0x2000);
	sdhc_write_phy(host, 0x2000, (read_val & 0x7FFFF8) | 0<<31 | 0<<27 | 0<<24 | 1<<0);

	//0XFFFE7FF3
	HRS09_PCS hrs09_val = host->hrs_regs.hrs09_pcs;
	hrs09_val.bits.EXTENDED_WR_MODE = 1;
	hrs09_val.bits.EXTENDED_RD_MODE = 1;
	hrs09_val.bits.RDCMD_EN = 1;
	hrs09_val.bits.RDDATA_EN = 1;
	host->hrs_regs.hrs09_pcs = hrs09_val;

	//0xFFF0FFFF
	host->hrs_regs.hrs10_hcsspa.bits.HCSDCLKADJ = 3;

	//0 8
	HRS16_HSGE hrs16_val = {.v = 0};
	hrs16_val.bits.WRCMD0_DLY = 1;
	hrs16_val.bits.WRDATA0_DLY = 1;
	host->hrs_regs.hrs16_hsge = hrs16_val;

	HRS07_IDI hrs07_val = {.v = 0};
	hrs07_val.bits.RW_COMPENSATE = 8;
	host->hrs_regs.hrs07_idi = hrs07_val;
}

void dfi_sdr50(struct sdhc_regs *host)
{
	dfi_config(host);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DQS_TIMING_REG; // select phy_dqs_timing_reg
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = ( 1<<EXT_LPBK_DQS | 1<<LPBK_DQS | 1<<PHONY_DQS | 1<<PHONY_DQS_CMD);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_GATE_LPBK_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (1<< SYNC_METHOD | 0<<SW_HALF_CYCLE_SHIFT | 52<<RD_DEL_SEL | 1<<UNDERRUN_SUPPRESS | 1<<GATE_CFG_ALWAYS_ON);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DLL_MASTER_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (host->hrs_regs.hrs05_prdp.v & ~(1<<23 | 7<<20 | 0xff<0)
			| 1<<DLL_BYPASS_MODE | 2<<20 | 4<<DLL_START_POINT);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DLL_SLAVE_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (0<< READ_DQS_CMD_DELAY | 0<<CLK_WRDQS_DELAY | 0<<CLK_WRDQS_DELAY | 0<<READ_DQS_DELAY);

	//host->hrs_regs.hrs04_pra.bits.PHYREGADDR = 0x2080;
	//host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = host->hrs_regs.hrs05_prdp.v & ~(0x3F<<4);

	dfi_release(host);

	sdhc_write_phy(host, 0x2000, sdhc_read_phy(host, 0x2000) & 0x7FFFF8 | 0<<31 | 0<<27 | 0<<24 | 1<<0);

	//0XFFFE7FF3
	HRS09_PCS hrs09_val = host->hrs_regs.hrs09_pcs;
	hrs09_val.bits.EXTENDED_WR_MODE = 1;
	hrs09_val.bits.EXTENDED_RD_MODE = 1;
	hrs09_val.bits.RDCMD_EN = 1;
	hrs09_val.bits.RDDATA_EN = 1;
	host->hrs_regs.hrs09_pcs = hrs09_val;

	//0xFFF0FFFF
	host->hrs_regs.hrs10_hcsspa.bits.HCSDCLKADJ = 5;

	//0 8
	//HRS16_HSGE hrs16_val = host->hrs_regs.hrs16_hsge;
	//hrs16_val.bits.WRCMD0_DLY = 1;
	//hrs16_val.bits.WRDATA0_DLY = 1;
	//host->hrs_regs.hrs16_hsge = hrs16_val;

	HRS07_IDI hrs07_val = {.v = 0};
	hrs07_val.bits.RW_COMPENSATE = 8;
	host->hrs_regs.hrs07_idi = hrs07_val;
}

void dfi_sdr104(struct sdhc_regs *host)
{
	dfi_config(host);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DQS_TIMING_REG; // select phy_dqs_timing_reg
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = ( 1<<EXT_LPBK_DQS | 1<<LPBK_DQS | 1<<PHONY_DQS | 1<<PHONY_DQS_CMD);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_GATE_LPBK_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (1<< SYNC_METHOD | 0<<SW_HALF_CYCLE_SHIFT | 52<<RD_DEL_SEL | 1<<UNDERRUN_SUPPRESS | 1<<GATE_CFG_ALWAYS_ON);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DLL_MASTER_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (host->hrs_regs.hrs05_prdp.v & ~(1<<23 | 7<<20 | 0xff<0)
			| 0<<DLL_BYPASS_MODE | 2<<20 | 4<<DLL_START_POINT);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DLL_SLAVE_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (0<< READ_DQS_CMD_DELAY | 77<<CLK_WRDQS_DELAY | 77<<CLK_WR_DELAY | 0<<READ_DQS_DELAY);

	dfi_release(host);

	sdhc_write_phy(host, 0x2000, sdhc_read_phy(host, 0x2000) & 0x7FFFF8 | 0<<31 | 0<<27 | 1<<24 | 1<<0);

	//0XFFFE7FF3
	HRS09_PCS hrs09_val = host->hrs_regs.hrs09_pcs;
	hrs09_val.bits.RDCMD_EN = 1;
	hrs09_val.bits.RDDATA_EN = 1;
	hrs09_val.bits.EXTENDED_RD_MODE = 0;
	hrs09_val.bits.EXTENDED_WR_MODE = 0;
	host->hrs_regs.hrs09_pcs = hrs09_val;

	//0xFFF0FFFF
	host->hrs_regs.hrs10_hcsspa.bits.HCSDCLKADJ = 8;

	//0 8
	HRS16_HSGE hrs16_val = {.v = 0};
	hrs16_val.bits.WRCMD0_DLY = 1;
	hrs16_val.bits.WRDATA0_DLY = 1;
	host->hrs_regs.hrs16_hsge = hrs16_val;

	HRS07_IDI hrs07_val = {.v = 0};
	hrs07_val.bits.RW_COMPENSATE = 9;
	host->hrs_regs.hrs07_idi = hrs07_val;
}

void dfi_ddr50(struct sdhc_regs *host)
{
	dfi_config(host);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DQS_TIMING_REG; // select phy_dqs_timing_reg
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = ( 1<<EXT_LPBK_DQS | 1<<LPBK_DQS | 1<<PHONY_DQS | 1<<PHONY_DQS_CMD);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_GATE_LPBK_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (1<< SYNC_METHOD | 0<<SW_HALF_CYCLE_SHIFT | 52<<RD_DEL_SEL | 1<<UNDERRUN_SUPPRESS | 1<<GATE_CFG_ALWAYS_ON);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DLL_MASTER_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (host->hrs_regs.hrs05_prdp.v & ~(1<<23 | 7<<20 | 0xff<0)
			| 1<<DLL_BYPASS_MODE | 2<<20 | 4<<DLL_START_POINT);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DLL_SLAVE_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (0<< READ_DQS_CMD_DELAY | 0<<CLK_WRDQS_DELAY | 0<<CLK_WRDQS_DELAY | 0<<READ_DQS_DELAY);

	dfi_release(host);

	sdhc_write_phy(host, 0x2000, sdhc_read_phy(host, 0x2000) & 0x7FFFF8 | 0<<31 | 0<<27 | 0<<24 | 1<<0);

	//0XFFFE7FF3
	HRS09_PCS hrs09_val = host->hrs_regs.hrs09_pcs;
	hrs09_val.bits.RDCMD_EN = 1;
	hrs09_val.bits.RDDATA_EN = 1;
	hrs09_val.bits.EXTENDED_WR_MODE = 1;
	hrs09_val.bits.EXTENDED_RD_MODE = 1;
	host->hrs_regs.hrs09_pcs = hrs09_val;

	//0xFFF0FFFF
	host->hrs_regs.hrs10_hcsspa.bits.HCSDCLKADJ = 2;

	//0 8
	HRS16_HSGE hrs16_val = {.v = 0};
	//hrs16_val.bits.WRCMD0_DLY = 1;
	//hrs16_val.bits.WRDATA0_DLY = 1;
	hrs16_val.bits.WRDATA0_SDCLK_DLY = 1;
	hrs16_val.bits.WRDATA1_SDCLK_DLY = 1;
	host->hrs_regs.hrs16_hsge = hrs16_val;

	HRS07_IDI hrs07_val = {.v = 0};
	hrs07_val.bits.RW_COMPENSATE = 8;
	host->hrs_regs.hrs07_idi = hrs07_val;
}

void dfi_sdr(struct sdhc_regs *host)
{
	dfi_config(host);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DQS_TIMING_REG; // select phy_dqs_timing_reg
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = ( 1<<EXT_LPBK_DQS | 1<<LPBK_DQS | 1<<PHONY_DQS | 1<<PHONY_DQS_CMD);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_GATE_LPBK_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (1<< SYNC_METHOD | 0<<SW_HALF_CYCLE_SHIFT | 52<<RD_DEL_SEL | 1<<UNDERRUN_SUPPRESS | 1<<GATE_CFG_ALWAYS_ON);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DLL_MASTER_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (host->hrs_regs.hrs05_prdp.v & ~(1<<23 | 7<<20 | 0xff<0)
			| 1<<DLL_BYPASS_MODE | 2<<20 | 4<<DLL_START_POINT);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DLL_SLAVE_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (0<< READ_DQS_CMD_DELAY | 0<<CLK_WRDQS_DELAY | 0<<CLK_WRDQS_DELAY | 0<<READ_DQS_DELAY);

	//host->hrs_regs.hrs04_pra.bits.PHYREGADDR = 0x2080;
	//host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = host->hrs_regs.hrs05_prdp.v & ~(0x3F<<4);

	dfi_release(host);

	sdhc_write_phy(host, 0x2000, sdhc_read_phy(host, 0x2000) & 0x7FFFF8 | 0<<31 | 0<<27 | 0<<24 | 1<<0);

	//0XFFFE7FF3
	HRS09_PCS hrs09_val = host->hrs_regs.hrs09_pcs;
	hrs09_val.bits.RDCMD_EN = 1;
	hrs09_val.bits.RDDATA_EN = 1;
	hrs09_val.bits.EXTENDED_WR_MODE = 1;
	hrs09_val.bits.EXTENDED_RD_MODE = 1;
	host->hrs_regs.hrs09_pcs = hrs09_val;

	//0xFFF0FFFF
	host->hrs_regs.hrs10_hcsspa.bits.HCSDCLKADJ = 2;

	//0 8
	HRS16_HSGE hrs16_val = {.v = 0};
	hrs16_val.bits.WRCMD0_DLY = 1;
	hrs16_val.bits.WRDATA0_DLY = 1;
	//hrs16_val.bits.WRDATA0_SDCLK_DLY = 1;
	//hrs16_val.bits.WRDATA1_SDCLK_DLY = 1;
	host->hrs_regs.hrs16_hsge = hrs16_val;

	HRS07_IDI hrs07_val = {.v = 0};
	hrs07_val.bits.RW_COMPENSATE = 9;
	host->hrs_regs.hrs07_idi = hrs07_val;
}

void dfi_ddr(struct sdhc_regs *host)
{
	dfi_config(host);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DQS_TIMING_REG; // select phy_dqs_timing_reg
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = ( 1<<EXT_LPBK_DQS | 1<<LPBK_DQS | 1<<PHONY_DQS | 1<<PHONY_DQS_CMD);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_GATE_LPBK_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (1<< SYNC_METHOD | 0<<SW_HALF_CYCLE_SHIFT | 52<<RD_DEL_SEL | 1<<UNDERRUN_SUPPRESS | 1<<GATE_CFG_ALWAYS_ON);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DLL_MASTER_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (host->hrs_regs.hrs05_prdp.v & ~(1<<23 | 7<<20 | 0xff<0)
			| 1<<DLL_BYPASS_MODE | 2<<20 | 4<<DLL_START_POINT);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DLL_SLAVE_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (0<< READ_DQS_CMD_DELAY | 0<<CLK_WRDQS_DELAY | 0<<CLK_WRDQS_DELAY | 0<<READ_DQS_DELAY);

	dfi_release(host);

	sdhc_write_phy(host, 0x2000, sdhc_read_phy(host, 0x2000) & 0x7FFFF8 | 0<<31 | 0<<27 | 0<<24 | 1<<0);

	//0XFFFE7FF3
	HRS09_PCS hrs09_val = host->hrs_regs.hrs09_pcs;
	hrs09_val.bits.RDCMD_EN = 1;
	hrs09_val.bits.RDDATA_EN = 1;
	hrs09_val.bits.EXTENDED_WR_MODE = 1;
	hrs09_val.bits.EXTENDED_RD_MODE = 1;
	host->hrs_regs.hrs09_pcs = hrs09_val;

	//0xFFF0FFFF
	host->hrs_regs.hrs10_hcsspa.bits.HCSDCLKADJ = 2;

	//0 8
	HRS16_HSGE hrs16_val = {.v = 0};
	//hrs16_val.bits.WRCMD0_DLY = 1;
	//hrs16_val.bits.WRDATA0_DLY = 1;
	hrs16_val.bits.WRDATA0_SDCLK_DLY = 1;
	hrs16_val.bits.WRDATA1_SDCLK_DLY = 1;
	host->hrs_regs.hrs16_hsge = hrs16_val;

	HRS07_IDI hrs07_val = {.v = 0};
	hrs07_val.bits.RW_COMPENSATE = 8;
	host->hrs_regs.hrs07_idi = hrs07_val;
}

void dfi_hs200(struct sdhc_regs *host, u32 tune_val)
{
	volatile u32 tune_val_tmp = 0xff & ((tune_val<<8)/40); // N*256/40
	dfi_config(host);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DQS_TIMING_REG; // select phy_dqs_timing_reg
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = ( 1<<EXT_LPBK_DQS | 1<<LPBK_DQS | 1<<PHONY_DQS | 1<<PHONY_DQS_CMD);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_GATE_LPBK_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (1<< SYNC_METHOD | 0<<SW_HALF_CYCLE_SHIFT | 52<<RD_DEL_SEL | 1<<UNDERRUN_SUPPRESS | 1<<GATE_CFG_ALWAYS_ON);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DLL_MASTER_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = ((host->hrs_regs.hrs05_prdp.v & ~(1<<23 | 7<<20 | 0xff<0)) |
			1<<DLL_BYPASS_MODE | 2<<20 | 4<<DLL_START_POINT);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DLL_SLAVE_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (tune_val_tmp<< READ_DQS_CMD_DELAY | 0<<CLK_WRDQS_DELAY | 0<<CLK_WR_DELAY | tune_val_tmp<<READ_DQS_DELAY);

	dfi_release(host);

	sdhc_write_phy(host, PHY_DQ_TIMING_REG, (sdhc_read_phy(host, PHY_DQ_TIMING_REG) & 0x7FFFF8) | 0<<31 | 0<<27 | 0<<24 | 1<<0);

	//0XFFFE7FF3
	HRS09_PCS hrs09_val = host->hrs_regs.hrs09_pcs;
	hrs09_val.bits.RDDATA_EN = 1;
	hrs09_val.bits.RDCMD_EN = 1;
	hrs09_val.bits.EXTENDED_WR_MODE = 0;
	hrs09_val.bits.EXTENDED_RD_MODE = 0;
	host->hrs_regs.hrs09_pcs = hrs09_val;

	//0xFFF0FFFF
	host->hrs_regs.hrs10_hcsspa.bits.HCSDCLKADJ = 2;

	//0 8
	HRS16_HSGE hrs16_val = {.v = 0};
	//hrs16_val.bits.WRDATA0_SDCLK_DLY = 1;
	//hrs16_val.bits.WRDATA1_SDCLK_DLY = 1;
	hrs16_val.bits.WRCMD0_DLY = 1;
	hrs16_val.bits.WRDATA0_DLY = 1;
	host->hrs_regs.hrs16_hsge = hrs16_val;

	HRS07_IDI hrs07_val = {.v = 0};
	hrs07_val.bits.RW_COMPENSATE = 9;
	host->hrs_regs.hrs07_idi = hrs07_val;
}

void dfi_hs400(struct sdhc_regs *host)
{
	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DLL_SLAVE_CTRL_REG;
	HRS05_PRDP hrs05_val = host->hrs_regs.hrs05_prdp;
	hrs05_val.v &= 0xFF000000;
	dfi_config(host);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DQS_TIMING_REG; // select phy_dqs_timing_reg
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = ( 1<<EXT_LPBK_DQS | 1<<LPBK_DQS | 0<<PHONY_DQS | 1<<PHONY_DQS_CMD);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_GATE_LPBK_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (1<< SYNC_METHOD | 0<<SW_HALF_CYCLE_SHIFT | 52<<RD_DEL_SEL | 1<<UNDERRUN_SUPPRESS | 1<<GATE_CFG_ALWAYS_ON);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DLL_MASTER_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (host->hrs_regs.hrs05_prdp.v & ~(1<<23 | 7<<20 | 0xff<0)
			| 0<<DLL_BYPASS_MODE | 2<<20 | 4<<DLL_START_POINT);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DLL_SLAVE_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (hrs05_val.v | 77<<CLK_WRDQS_DELAY | 75<<CLK_WR_DELAY | 64<<READ_DQS_DELAY);

	dfi_release(host);

	sdhc_write_phy(host, 0x2000, (sdhc_read_phy(host, 0x2000) & 0x7FFFF8) | 0<<31 | 0<<27 | 0<<24 | 1<<0);

	//0XFFFE7FF3
	HRS09_PCS hrs09_val = host->hrs_regs.hrs09_pcs;
	hrs09_val.bits.RDCMD_EN = 1;
	hrs09_val.bits.RDDATA_EN = 1;
	hrs09_val.bits.EXTENDED_WR_MODE = 0;
	hrs09_val.bits.EXTENDED_RD_MODE = 0;
	host->hrs_regs.hrs09_pcs = hrs09_val;

	//0xFFF0FFFF
	host->hrs_regs.hrs10_hcsspa.bits.HCSDCLKADJ = 8;

	//0 8
	HRS16_HSGE hrs16_val = {.v = 0};
	hrs16_val.bits.WRDATA1_SDCLK_DLY = 1;
	hrs16_val.bits.WRDATA0_SDCLK_DLY = 1;
	//hrs16_val.bits.WRDATA0_DLY = 1;
	hrs16_val.bits.WRCMD0_DLY = 1;
	host->hrs_regs.hrs16_hsge = hrs16_val;

	HRS07_IDI hrs07_val = {.v = 0};
	hrs07_val.bits.RW_COMPENSATE = 8;
	host->hrs_regs.hrs07_idi = hrs07_val;
}

void dfi_hs400es(struct sdhc_regs *host)
{
	dfi_config(host);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DQS_TIMING_REG; // select phy_dqs_timing_reg
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = ( 1<<EXT_LPBK_DQS | 1<<LPBK_DQS | 1<<PHONY_DQS | 1<<PHONY_DQS_CMD);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_GATE_LPBK_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (1<< SYNC_METHOD | 0<<SW_HALF_CYCLE_SHIFT | 52<<RD_DEL_SEL | 1<<UNDERRUN_SUPPRESS | 1<<GATE_CFG_ALWAYS_ON);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DLL_MASTER_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (host->hrs_regs.hrs05_prdp.v & ~(1<<23 | 7<<20 | 0xff<0)
			| 0<<DLL_BYPASS_MODE | 2<<20 | 4<<DLL_START_POINT);

	host->hrs_regs.hrs04_pra.bits.PHYREGADDR = PHY_DLL_SLAVE_CTRL_REG;
	host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = (64<<READ_DQS_CMD_DELAY | 77<<CLK_WRDQS_DELAY | 75<<CLK_WRDQS_DELAY | 64<<READ_DQS_DELAY);

	//host->hrs_regs.hrs04_pra.bits.PHYREGADDR = 0x2080;
	//host->hrs_regs.hrs05_prdp.bits.PHYREGDATA = host->hrs_regs.hrs05_prdp.v & ~(0x3F<<4);

	dfi_release(host);

	sdhc_write_phy(host, 0x2000, (sdhc_read_phy(host, 0x2000) & 0x7FFFF8) | 0<<31 | 0<<27 | 0<<24 | 1<<0);

	//0XFFFE7FF3
	HRS09_PCS hrs09_val = host->hrs_regs.hrs09_pcs;
	hrs09_val.bits.RDDATA_EN = 1;
	hrs09_val.bits.RDCMD_EN = 1;
	hrs09_val.bits.EXTENDED_WR_MODE = 0;
	hrs09_val.bits.EXTENDED_RD_MODE = 0;
	host->hrs_regs.hrs09_pcs = hrs09_val;

	//0xFFF0FFFF
	host->hrs_regs.hrs10_hcsspa.bits.HCSDCLKADJ = 8;

	//0 8
	HRS16_HSGE hrs16_val = {.v = 0};
	hrs16_val.bits.WRDATA1_SDCLK_DLY = 1;
	hrs16_val.bits.WRDATA0_SDCLK_DLY = 1;
	//hrs16_val.bits.WRDATA0_DLY = 1;
	hrs16_val.bits.WRCMD0_DLY = 1;
	host->hrs_regs.hrs16_hsge = hrs16_val;

	HRS07_IDI hrs07_val = {.v = 0};
	hrs07_val.bits.RW_COMPENSATE = 8;
	host->hrs_regs.hrs07_idi = hrs07_val;
}




#endif /* __DFI_LIB_C__ */
