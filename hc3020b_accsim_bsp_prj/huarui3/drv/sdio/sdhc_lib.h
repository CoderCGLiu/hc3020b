#ifndef __SDHC_LIB_H__
#define __SDHC_LIB_H__
#include "./sdhc_common.h"

#define SD_HOST_CLK 100000000

#define SDIO_DEV_NUM	2


// ==================== reg bits =====
// hrs ===============================
typedef union {
	volatile u32 v;
	struct {
		volatile u32 SWR     :1;
		volatile u32 _PAD0   :15;
		volatile u32 SAV     :16;
	} bits;
} HRS00_GI; // general information

typedef union {
	volatile u32 v;
	struct {
		volatile u32 DP      :24;
	} bits;
} HRS01_DS; //debounce setting

typedef union {
	volatile u32 v;
	struct {
		volatile u32 PBL      :4;
		volatile u32 _PAD0    :12;
		volatile u32 OTN      :2;
	} bits;
} HRS02_BS; //bus setting

typedef union {
	volatile u32 v;
	struct {
		volatile u32 AER_RD       :1;
		volatile u32 AER_RS       :1;
		volatile u32 AER_BD       :1;
		volatile u32 AER_BS       :1;
		volatile u32 _PAD0        :4; //-7
		volatile u32 AER_SENRD    :1;
		volatile u32 AER_SENRS    :1;
		volatile u32 AER_SENBD    :1;
		volatile u32 AER_SENBS    :1;
		volatile u32 _PAD1        :4; //-15
		volatile u32 AER_IERD     :1;
		volatile u32 AER_IERS     :1;
		volatile u32 AER_IEBD     :1;
		volatile u32 AER_IEBS     :1; //19
	} bits;
} HRS03_AE; //axi error responses

typedef union {
	volatile u32 v;
	struct {
		volatile u32 PHYREGADDR   :16;
	} bits;
} HRS04_PRA; // phy register address

typedef union {
	volatile u32 v;
	struct {
		volatile u32 PHYREGDATA   :32;
	} bits;
} HRS05_PRDP; // phy register data port

typedef union {
	volatile u32 v;
	struct {
		volatile u32 EMM          :3;
	} bits;
} HRS06_EC; // emmc control register

typedef union {
	volatile u32 v;
	struct {
		volatile u32 IDELAY_VAL     :5;
		volatile u32 _PAD0          :11;
		volatile u32 RW_COMPENSATE  :5;
	} bits;
} HRS07_IDI;  // io delay information

typedef union {
	volatile u32 v;
	struct {
		volatile u32 PHY_DLL_UPDACK :1;
		volatile u32 PHY_DLL_UPDREQ :1;
	} bits;
} HRS08_PDUCS;  // phy dll update control and status register

typedef union {
	volatile u32 v;
	struct {
		volatile u32 PHY_SW_RESET        :1;
		volatile u32 PHY_INIT_COMPLETE   :1;
		volatile u32 EXTENDED_RD_MODE    :1;
		volatile u32 EXTENDED_WR_MODE    :1;
		volatile u32 _PAD0               :11;
		volatile u32 RDCMD_EN            :1; //15
		volatile u32 RDDATA_EN           :1;
		volatile u32 LVSI_TCKSEL         :6;
		volatile u32 LVSI_CNT            :4;
	} bits;
} HRS09_PCS;  // phy contrl and status register

typedef union {
	volatile u32 v;
	struct {
		volatile u32 _PAD0               :16;
		volatile u32 HCSDCLKADJ          :4;
		volatile u32 _PAD1               :2;
		volatile u32 RDDATA_SWAP         :1; //22
	} bits;
} HRS10_HCSSPA;  // host controller SDCLK start point adjustment

typedef union {
	volatile u32 v;
	struct {
		volatile u32 EMMC_RST            :1;
	} bits;
} HRS11_EC;      // emmc control

typedef union {
	volatile u32 v;
	struct {
		volatile u32 PCU                 :1;
		volatile u32 PCO                 :1;
		volatile u32 PDU                 :1;
		volatile u32 PDO                 :1;
	} bits;
} HRS12_HIS;     // host interrupt status

typedef union {
	volatile u32 v;
	struct {
		volatile u32 MFPCURS             :1;
		volatile u32 MFPCOFS             :1;
		volatile u32 MSPDURS             :1;
		volatile u32 MFPDOFS             :1;
	} bits;
} HRS13_HSTE;     // host status enable

typedef union {
	volatile u32 v;
	struct {
		volatile u32 IMFPCURS             :1;
		volatile u32 IMFPCOFS             :1;
		volatile u32 IMSPDURS             :1;
		volatile u32 IMFPDOFS             :1;
	} bits;
} HRS14_HSGE;     // host signal enable

typedef union {
	volatile u32 v;
	struct {
		volatile u32 WRCMD0_DLY           :4;
		volatile u32 WRCMD1_DLY           :4;
		volatile u32 WRDATA0_DLY          :4;
		volatile u32 WRDATA1_DLY          :4;
		volatile u32 WRCMD0_SDCLK_DLY     :4;
		volatile u32 WRCMD1_SDCLK_DLY     :4;
		volatile u32 WRDATA0_SDCLK_DLY    :4;
		volatile u32 WRDATA1_SDCLK_DLY    :4;
	} bits;
} HRS16_HSGE;     // host signal enable

typedef union {
	volatile u32 v;
	struct {
		volatile u32 PN_IP6061            :32;
	} bits;
} HRS29_SDN;      // sd magic number

typedef union {
	volatile u32 v;
	struct {
		volatile u32 CQS                  :1;
		volatile u32 HS4ESS               :1;
	} bits;
} HRS30_HCR;      // host capability register

typedef union {
	volatile u32 v;
	struct {
		volatile u32 FVN                  :8;
		volatile u32 _PAD0                :8;
		volatile u32 HCV                  :12;
	} bits;
} HRS31_HCV;      // host controller version

typedef union {
	volatile u32 v;
	struct {
		volatile u32 DFS                  :16;
		volatile u32 AFA                  :15;
		volatile u32 LFUR                 :1;
	} bits;
} HRS32_FMR;      // fsm monitor register

typedef union {
	volatile u32 v;
	struct {
		volatile u32 S0TS0                :32;
	} bits;
} HRS33_TS0R;     // tune status 0 register

typedef union {
	volatile u32 v;
	struct {
		volatile u32 S1TS1                :32;
	} bits;
} HRS34_TS1R;     // tune status 1 register

typedef union {
	volatile u32 v;
	struct {
		volatile u32 BA                   :1;
		volatile u32 BE_ATE               :1;
		volatile u32 BE_IAE               :1;
		volatile u32 BE_DTE               :1;
		volatile u32 BE_DCE               :1;
		volatile u32 BE_EBE               :1;
		volatile u32 BE_DME               :1;
	} bits;
} HRS36_BSR;      // boot status register

typedef union {
	volatile u32 v;
	struct {
		volatile u32 BA0                  :32;
	} bits;
} HRS40_BA0;      // base address 0 for auto-configuration descriptor mechanism

typedef union {
	volatile u32 v;
	struct {
		volatile u32 BA1                  :32;
	} bits;
} HRS41_BA1;      // base address 1 for auto-configuration descriptor mechanism

typedef union {
	volatile u32 v;
	struct {
		volatile u32 DESCMECH_EN          :1;
		volatile u32 DESCMECH_TM          :4;
	} bits;
} HRS42_ACDMED;   // auto-configuration descriptor mechanism enable/disable

typedef union {
	volatile u32 v;
	struct {
		volatile u32 EODACDMP             :1;
		volatile u32 ERROR_VAL            :3;
	} bits;
} HRS43_ESFACDM;  // error status for auto-configuration descriptor mechanism

// SRS =================================================================

typedef union {
	volatile u32 v;
	struct {
		volatile u32 SAAR                 :32;
	} bits;
} SRS00_SSAR;      // sdma system address / argument2 /32-bit block count

typedef union {
	volatile u32 v;
	struct {
		volatile u32 TBS                  :12;
		volatile u32 SDMABB               :3;
		volatile u32 _PAD0                :1;
		volatile u32 BCCT                 :16;
	} bits;
} SRS01_BSR;       // block size/ block count

typedef union {
	volatile u32 v;
	struct {
		volatile u32 ARG1                 :32;
	} bits;
} SRS02_CA1;       // arg1 - command argument 1

typedef union {
	volatile u32 v;
	struct {
		volatile u32 DMAE                 :1;
		volatile u32 BCE                  :1;
		volatile u32 ACE                  :2;
		volatile u32 DTDS                 :1;
		volatile u32 MSBS                 :1;
		volatile u32 RECT                 :1;
		volatile u32 RECE                 :1;
		volatile u32 RID                  :1; //8
		volatile u32 _PAD0                :7;
		volatile u32 RTS                  :2; //16-17
		volatile u32 SCF                  :1;
		volatile u32 CRCCE                :1;
		volatile u32 CICE                 :1;
		volatile u32 DPS                  :1;
		volatile u32 CT                   :2;
		volatile u32 CIDX                 :6; //24-29
	} bits;
} SRS03_CTM;       // command/transfer mode

typedef union {
	volatile u32 v;
	struct {
		volatile u32 RESP0                :32;
	} bits;
} SRS04_RESP0;      // response register

typedef union {
	volatile u32 v;
	struct {
		volatile u32 RESP1                :32;
	} bits;
} SRS05_RESP1;      // response register

typedef union {
	volatile u32 v;
	struct {
		volatile u32 RESP2                :32;
	} bits;
} SRS06_RESP2;      // response register

typedef union {
	volatile u32 v;
	struct {
		volatile u32 RESP3                :32;
	} bits;
} SRS07_RESP3;      // response register

typedef union {
	volatile u32 v;
	struct {
		volatile u32 BDP                  :32;
	} bits;
} SRS08_DB;      // data buffer

typedef union {
	volatile u32 v;
	struct {
		volatile u32 CICMD                :1;
		volatile u32 CIDAT                :1;
		volatile u32 DLA                  :1;
		volatile u32 _PAD0                :1;
		volatile u32 DATSL2               :4;
		volatile u32 WTA                  :1; //8
		volatile u32 RTA                  :1;
		volatile u32 BWE                  :1;
		volatile u32 BRE                  :1;
		volatile u32 _PAD1                :4;
		volatile u32 CI                   :1;
		volatile u32 CSS                  :1;
		volatile u32 CDSL                 :1;
		volatile u32 WPSL                 :1;
		volatile u32 DATSL1               :4;
		volatile u32 CMDSL1               :1;
		volatile u32 _PAD2                :1;
		volatile u32 LVSIRSLT             :1; //26
		volatile u32 CNIBE                :1;
		volatile u32 CSMDS                :1;
	} bits;
} SRS09_PSR;     // present state register

typedef union {
	volatile u32 v;
	struct {
		volatile u32 LEDC                 :1;
		volatile u32 DTW                  :1;
		volatile u32 HSE                  :1;
		volatile u32 DMASEL               :2; //4
		volatile u32 EDTW                 :1;
		volatile u32 CDTL                 :1;
		volatile u32 CDSS                 :1;
		volatile u32 BP                   :1;
		volatile u32 BVS                  :3; //11
		volatile u32 _PAD0                :4;
		volatile u32 SBGR                 :1; //16
		volatile u32 CR                   :1;
		volatile u32 RWC                  :1;
		volatile u32 IBG                  :1;
		volatile u32 _PAD1                :4;
		volatile u32 WOIQ                 :1; //24
		volatile u32 WOIS                 :1;
		volatile u32 WORM                 :1;
	} bits;
} SRS10_HC1;     // host control 1 (general power block-gap wake-up)

typedef union {
	volatile u32 v;
	struct {
		volatile u32 ICE                  :1;
		volatile u32 ICS                  :1;
		volatile u32 SDCE                 :1;
		volatile u32 _PAD0                :3;
		volatile u32 SDCFSH               :2;
		volatile u32 SDCFSL               :8;
		volatile u32 DTCV                 :4;
		volatile u32 _PAD1                :4;
		volatile u32 SRFA                 :1;
		volatile u32 SRCMD                :1;
		volatile u32 SRDAT                :1; //26
	} bits;
} SRS11_HC2;     // host control 2 (clock, timeout, reset)

typedef volatile union {
	volatile u32 v;
	struct {
		volatile u32 CC                   :1;
		volatile u32 TC                   :1;
		volatile u32 BGE                  :1;
		volatile u32 DMAINT               :1;
		volatile u32 BWR                  :1;
		volatile u32 BRR                  :1; //5
		volatile u32 CIN                  :1;
		volatile u32 CR                   :1;
		volatile u32 CINT                 :1;
		volatile u32 _PAD0                :4;
		volatile u32 FXE                  :1; //13
		volatile u32 CQINT                :1; //14
		volatile u32 EINT                 :1; //15
		volatile u32 ECT                  :1; //16
		volatile u32 ECCRC                :1;
		volatile u32 ECEB                 :1;
		volatile u32 ECI                  :1;
		volatile u32 EDT                  :1;
		volatile u32 EDCRC                :1;
		volatile u32 EDEB                 :1;
		volatile u32 ECL                  :1;
		volatile u32 EAC                  :1;
		volatile u32 EADMA                :1;
		volatile u32 _PAD1                :1;
		volatile u32 ERSP                 :1; //27
	} bits;
} SRS12_ENIS;    // error/normal interrupt status


typedef union {
	volatile u32 v;
	struct {
		volatile u32 CC_SE                :1;
		volatile u32 TC_SE                :1;
		volatile u32 BGE_SE               :1;
		volatile u32 DMAINT_SE            :1;
		volatile u32 BWR_SE               :1; //4
		volatile u32 BRR_SE               :1;
		volatile u32 CIN_SE               :1;
		volatile u32 CR_SE                :1;
		volatile u32 CINT_SE              :1; //8
		volatile u32 _PAD0                :4;
		volatile u32 FXE_SE               :1;
		volatile u32 CQINT_SE             :1; //14
		volatile u32 _PAD1                :1;
		volatile u32 ECT_SE               :1;
		volatile u32 ECCRC_SE             :1;
		volatile u32 ECEB_SE              :1;
		volatile u32 ECI_SE               :1;
		volatile u32 EDT_SE               :1;
		volatile u32 EDCTC_SE             :1;
		volatile u32 EDEB_SE              :1;
		volatile u32 ECL_SE               :1;
		volatile u32 EAC_SE               :1;
		volatile u32 EAMDA_SE             :1;
		volatile u32 _PAD2                :1;
		volatile u32 ERSP_SE              :1; //27
	} bits;
} SRS13_ENSE;    // error/normal status enable

typedef union {
	volatile u32 v;
	struct {
		volatile u32 CC_IE                :1;
		volatile u32 TC_IE                :1;
		volatile u32 BGE_IE               :1;
		volatile u32 DMAINT_IE            :1;
		volatile u32 BWR_IE               :1; //4
		volatile u32 BRR_IE               :1;
		volatile u32 CIN_IE               :1;
		volatile u32 CR_IE                :1;
		volatile u32 CINT_IE              :1; //8
		volatile u32 _PAD0                :4;
		volatile u32 FXE_IE               :1;
		volatile u32 CQINT_IE             :1; //14
		volatile u32 _PAD1                :1;
		volatile u32 ECT_IE               :1;
		volatile u32 ECCRC_IE             :1;
		volatile u32 ECEB_IE              :1;
		volatile u32 ECI_IE               :1;
		volatile u32 EDT_IE               :1;
		volatile u32 EDCTC_IE             :1;
		volatile u32 EDEB_IE              :1;
		volatile u32 ECL_IE               :1;
		volatile u32 EAC_IE               :1;
		volatile u32 EAMDA_IE             :1;
		volatile u32 _PAD2                :1;
		volatile u32 ERSP_IE              :1; //27
	} bits;
} SRS14_ENSE;    // error/normal signal enable

typedef union {
	volatile u32 v;
	struct {
		volatile u32 ACNE                 :1;
		volatile u32 ACTE                 :1;
		volatile u32 ACCE                 :1;
		volatile u32 ACEBE                :1;
		volatile u32 ACIE                 :1;
		volatile u32 ACRE                 :1;
		volatile u32 _PAD0                :1;
		volatile u32 CNIACE               :1;
		volatile u32 _PAD1                :8;
		volatile u32 UMS                  :3; //16
		volatile u32 V18SE                :1;
		volatile u32 DSS                  :2; //20
		volatile u32 EXTNG                :1;
		volatile u32 SCS                  :1;
		volatile u32 LVSIEXEC             :1;
		volatile u32 _PAD2                :1;
		volatile u32 ADMA2LM              :1; //26
		volatile u32 CMD23E               :1;
		volatile u32 HV4E                 :1;
		volatile u32 A64B                 :1;
		volatile u32 _PAD3                :1;
		volatile u32 PVE                  :1; //31
	} bits;
} SRS15_HCACES;  // host control auto cmd error status

typedef union {
	volatile u32 v;
	struct {
		volatile u32 TCF                  :6;
		volatile u32 _PAD0                :1;
		volatile u32 TCU                  :1; //7
		volatile u32 BCSDCLK              :8;
		volatile u32 SRS16MBL             :2;
		volatile u32 EDS8                 :1; //18
		volatile u32 ADAM2S               :1;
		volatile u32 ADMA1S               :1;
		volatile u32 HSS                  :1;
		volatile u32 DMAS                 :1;
		volatile u32 SRS                  :1;
		volatile u32 VS33                 :1;
		volatile u32 VS30                 :1;
		volatile u32 VS18                 :1;
		volatile u32 A64SV4               :1; //27
		volatile u32 A64SV3               :1;
		volatile u32 AIS                  :1;
		volatile u32 SLT                  :2;
	} bits;
} SRS16_CAP1;     // capability

typedef union {
	volatile u32 v;
	struct {
		volatile u32 SDR50                :1;
		volatile u32 SDR104               :1;
		volatile u32 DDR50                :1;
		volatile u32 UHSII                :1;
		volatile u32 DRVA                 :1;
		volatile u32 DRVC                 :1;
		volatile u32 DRVD                 :1;
		volatile u32 _PAD0                :1;
		volatile u32 RTNGCNT              :4;
		volatile u32 _PAD1                :1;
		volatile u32 UTSM50               :1;
		volatile u32 RTNGM                :2;
		volatile u32 CLKMPR               :8;
		volatile u32 _PAD2                :3;
		volatile u32 ADMA3SUP             :1; //27
		volatile u32 VDD2S                :1;
		volatile u32 _PAD3                :2;
		volatile u32 LVSH                 :1;
	} bits;
} SRS17_CAP2;     // capability

typedef union {
	volatile u32 v;
	struct {
		volatile u32 MC33                 :8;
		volatile u32 MC30                 :8;
		volatile u32 MC18                 :8; //16
	} bits;
} SRS18_CAP3;     // capability

typedef union {
	volatile u32 v;
	struct {
		volatile u32 MC18V2               :8;
	} bits;
} SRS19_CAP4;     // capability

typedef union {
	volatile u32 v;
	struct {
		volatile u32 ACNE_FE              :1;
		volatile u32 ACTE_FE              :1;
		volatile u32 ACCE_FE              :1;
		volatile u32 ACEBE_FE             :1;
		volatile u32 ACIE_FE              :1;
		volatile u32 _PAD0                :2;
		volatile u32 CNIACE_FE            :1; //7
		volatile u32 _PAD1                :8;
		volatile u32 ECT_FE               :1; //16
		volatile u32 ECCRC_FE             :1;
		volatile u32 ECEB_FE              :1;
		volatile u32 ECI_FE               :1;
		volatile u32 EDT_FE               :1;
		volatile u32 EDCRC_FE             :1;
		volatile u32 EDEB_FE              :1;
		volatile u32 ECL_FE               :1;
		volatile u32 EAC_FE               :1; //24
		volatile u32 EADMA_FE             :1;
		volatile u32 ETUNE_FE             :1;
		volatile u32 ERESP_FE             :1;
	} bits;
} SRS20_FE;       // force event

typedef union {
	volatile u32 v;
	struct {
		volatile u32 EAMDAS               :2;
		volatile u32 EAMDAL               :1;
	} bits;
} SRS21_AES;      // adma error status

typedef union {
	volatile u32 v;
	struct {
		volatile u32 DMASA1               :32;
	} bits;
} SRS22_ASA1;      // adma/sdma system address 1

typedef union {
	volatile u32 v;
	struct {
		volatile u32 DMASA2               :32;
	} bits;
} SRS23_ASA2;      // adma/sdma system address 2

typedef union {
	volatile u32 v;
	struct {
		volatile u32 _PAD0                :16;
		volatile u32 SDCFSPV              :10;
		volatile u32 _PAD1                :4;
		volatile u32 DSSPV                :2;
	} bits;
} SRS24_PV;        // preset value(default speed)

typedef union {
	volatile u32 v;
	struct {
		volatile u32 SDCFSPV0             :10;
		volatile u32 _PAD0                :4;
		volatile u32 DSSPV0               :2;
		volatile u32 SDCFSPV1             :10;
		volatile u32 _PAD1                :4;
		volatile u32 DSSPV1               :2;
	} bits;
} SRS25_PV;        // preset value(high speed and sdr12)

typedef union {
	volatile u32 v;
	struct {
		volatile u32 SDCFSPV0             :10;
		volatile u32 CGSPV                :1;
		volatile u32 _PAD0                :3;
		volatile u32 DSSPV0               :2;
		volatile u32 SDCFSPV              :10;
		volatile u32 _PAD1                :4;
		volatile u32 DSSPV1               :2;
	} bits;
} SRS26_PV;        // preset value(sdr25 and sdr50)

typedef union {
	volatile u32 v;
	struct {
		volatile u32 SDCFSPV0             :10;
		volatile u32 _PAD0                :4;
		volatile u32 DSSPV0               :2;
		volatile u32 SDCFSPV1             :10;
		volatile u32 _PAD1                :4;
		volatile u32 DSSPV1               :2;
	} bits;
} SRS27_PV;       // present value (sdr104 and ddr50)

typedef union {
	volatile u32 v;
	struct {
		volatile u32 ADMA3IDA1            :32;
	} bits;
} SRS30_ADMA3IDA1;  // srs30 adma3 id address 1

typedef union {
	volatile u32 v;
	struct {
		volatile u32 ADMA3IDA2            :32;
	} bits;
} SRS31_ADMA3IDA2;  // srs30 adma3 id address 2

// CRS =================================================================
typedef union {
	volatile u32 v;
	struct {
		volatile u32 ISES                 :1;
		volatile u32 _PAD0                :15;
		volatile u32 SVN                  :8;
	} bits;
} CRS63_HCVIS;     // srs30 adma3 id address 2

// CQRS =================================================================
typedef union {
	volatile u32 v;
	struct {
		volatile u32 CQVN3                :4;
		volatile u32 CQVN2                :4;
		volatile u32 CQVN1                :4;
	} bits;
} CQRS00_CQV;      // command queuing version

typedef union {
	volatile u32 v;
	struct {
		volatile u32 ITCFVAL              :10;
		volatile u32 _PAD0                :2;
		volatile u32 ITCFMUL              :4;
	} bits;
} CQRS01_CQC;      // command queuing capabilities

typedef union {
	volatile u32 v;
	struct {
		volatile u32 CQE                  :1;
		volatile u32 _PAD0                :7;
		volatile u32 CQTDS                :1;
		volatile u32 _PAD1                :3;
		volatile u32 CQDCE                :1;
	} bits;
} CQRS02_CQC;      // command queuing configuration

typedef union {
	volatile u32 v;
	struct {
		volatile u32 CQHLT                :1;
		volatile u32 _PAD0                :7;
		volatile u32 CQCAT                :1;
	} bits;
} CQRS03_CQC;      // command queuing control

typedef union {
	volatile u32 v;
	struct {
		volatile u32 CQHAC                :1;
		volatile u32 CQTCC                :1;
		volatile u32 CQREDI               :1;
		volatile u32 CQTCL                :1;
	} bits;
} CQRS04_CQIS;     // command queuing interrupt status

typedef union {
	volatile u32 v;
	struct {
		volatile u32 CQHACST              :1;
		volatile u32 CQTCCST              :1;
		volatile u32 CQREDST              :1;
		volatile u32 CQTCLST              :1;
	} bits;
} CQRS05_CQISE;     // command queuing interrupt status enable

typedef union {
	volatile u32 v;
	struct {
		volatile u32 CQHACSI              :1;
		volatile u32 CQTCCSI              :1;
		volatile u32 CQREDSI              :1;
		volatile u32 CQTCLSI              :1;
	} bits;
} CQRS06_CQISE;     // command queuing interrupt signal enable

typedef union {
	volatile u32 v;
	struct {
		volatile u32 CQICTOVAL            :7;
		volatile u32 CQICTOVALEN          :1;
		volatile u32 CQICCTH              :5;
		volatile u32 _PAD0                :2;
		volatile u32 CQICCTHWEN           :1;
		volatile u32 CQICCTR              :1; //16
		volatile u32 _PAD1                :3;
		volatile u32 CQICSB               :1;
		volatile u32 _PAD2                :10;
		volatile u32 CQICED               :1;
	} bits;
} CQRS07_IC;        // interrupt coalescing

typedef union {
	volatile u32 v;
	struct {
		volatile u32 CQTDLBA              :32;
	} bits;
} CQRS08_CQTDLBA;   // command queuing task descriptor list base address

typedef union {
	volatile u32 v;
	struct {
		volatile u32 CQTDLBAU             :32;
	} bits;
} CQRS09_CQTDLBAU32B;   // command queuing task descriptor list base address upper 32 bits

typedef union {
	volatile u32 v;
	struct {
		volatile u32 CQTD00               :1;
		volatile u32 CQTD01               :1;
		volatile u32 CQTD02               :1;
		volatile u32 CQTD03               :1;
		volatile u32 CQTD04               :1;
		volatile u32 CQTD05               :1;
		volatile u32 CQTD06               :1;
		volatile u32 CQTD07               :1;
		volatile u32 CQTD08               :1;
		volatile u32 CQTD09               :1;
		volatile u32 CQTD10               :1;
		volatile u32 CQTD11               :1;
		volatile u32 CQTD12               :1;
		volatile u32 CQTD13               :1;
		volatile u32 CQTD14               :1;
		volatile u32 CQTD15               :1;
		volatile u32 CQTD16               :1;
		volatile u32 CQTD17               :1;
		volatile u32 CQTD18               :1;
		volatile u32 CQTD19               :1;
		volatile u32 CQTD20               :1;
		volatile u32 CQTD21               :1;
		volatile u32 CQTD22               :1;
		volatile u32 CQTD23               :1;
		volatile u32 CQTD24               :1;
		volatile u32 CQTD25               :1;
		volatile u32 CQTD26               :1;
		volatile u32 CQTD27               :1;
		volatile u32 CQTD28               :1;
		volatile u32 CQTD29               :1;
		volatile u32 CQTD30               :1;
		volatile u32 CQTD31               :1;
	} bits;
} CQRS10_CQTD;          // command queuing task doorbell

typedef union {
	volatile u32 v;
	struct {
		volatile u32 CQTCN00               :1;
		volatile u32 CQTCN01               :1;
		volatile u32 CQTCN02               :1;
		volatile u32 CQTCN03               :1;
		volatile u32 CQTCN04               :1;
		volatile u32 CQTCN05               :1;
		volatile u32 CQTCN06               :1;
		volatile u32 CQTCN07               :1;
		volatile u32 CQTCN08               :1;
		volatile u32 CQTCN09               :1;
		volatile u32 CQTCN10               :1;
		volatile u32 CQTCN11               :1;
		volatile u32 CQTCN12               :1;
		volatile u32 CQTCN13               :1;
		volatile u32 CQTCN14               :1;
		volatile u32 CQTCN15               :1;
		volatile u32 CQTCN16               :1;
		volatile u32 CQTCN17               :1;
		volatile u32 CQTCN18               :1;
		volatile u32 CQTCN19               :1;
		volatile u32 CQTCN20               :1;
		volatile u32 CQTCN21               :1;
		volatile u32 CQTCN22               :1;
		volatile u32 CQTCN23               :1;
		volatile u32 CQTCN24               :1;
		volatile u32 CQTCN25               :1;
		volatile u32 CQTCN26               :1;
		volatile u32 CQTCN27               :1;
		volatile u32 CQTCN28               :1;
		volatile u32 CQTCN29               :1;
		volatile u32 CQTCN30               :1;
		volatile u32 CQTCN31               :1;
	} bits;
} CQRS11_TCN;           // task compete notification

typedef union {
	volatile u32 v;
	struct {
		volatile u32 CQDQS                :32;
	} bits;
} CQRS12_DQS;       // device queue status

typedef union {
	volatile u32 v;
	struct {
		volatile u32 CQDPT00               :1;
		volatile u32 CQDPT01               :1;
		volatile u32 CQDPT02               :1;
		volatile u32 CQDPT03               :1;
		volatile u32 CQDPT04               :1;
		volatile u32 CQDPT05               :1;
		volatile u32 CQDPT06               :1;
		volatile u32 CQDPT07               :1;
		volatile u32 CQDPT08               :1;
		volatile u32 CQDPT09               :1;
		volatile u32 CQDPT10               :1;
		volatile u32 CQDPT11               :1;
		volatile u32 CQDPT12               :1;
		volatile u32 CQDPT13               :1;
		volatile u32 CQDPT14               :1;
		volatile u32 CQDPT15               :1;
		volatile u32 CQDPT16               :1;
		volatile u32 CQDPT17               :1;
		volatile u32 CQDPT18               :1;
		volatile u32 CQDPT19               :1;
		volatile u32 CQDPT20               :1;
		volatile u32 CQDPT21               :1;
		volatile u32 CQDPT22               :1;
		volatile u32 CQDPT23               :1;
		volatile u32 CQDPT24               :1;
		volatile u32 CQDPT25               :1;
		volatile u32 CQDPT26               :1;
		volatile u32 CQDPT27               :1;
		volatile u32 CQDPT28               :1;
		volatile u32 CQDPT29               :1;
		volatile u32 CQDPT30               :1;
		volatile u32 CQDPT31               :1;
	} bits;
} CQRS13_DPT;           // task compete notification

typedef union {
	volatile u32 v;
	struct {
		volatile u32 CQTC00               :1;
		volatile u32 CQTC01               :1;
		volatile u32 CQTC02               :1;
		volatile u32 CQTC03               :1;
		volatile u32 CQTC04               :1;
		volatile u32 CQTC05               :1;
		volatile u32 CQTC06               :1;
		volatile u32 CQTC07               :1;
		volatile u32 CQTC08               :1;
		volatile u32 CQTC09               :1;
		volatile u32 CQTC10               :1;
		volatile u32 CQTC11               :1;
		volatile u32 CQTC12               :1;
		volatile u32 CQTC13               :1;
		volatile u32 CQTC14               :1;
		volatile u32 CQTC15               :1;
		volatile u32 CQTC16               :1;
		volatile u32 CQTC17               :1;
		volatile u32 CQTC18               :1;
		volatile u32 CQTC19               :1;
		volatile u32 CQTC20               :1;
		volatile u32 CQTC21               :1;
		volatile u32 CQTC22               :1;
		volatile u32 CQTC23               :1;
		volatile u32 CQTC24               :1;
		volatile u32 CQTC25               :1;
		volatile u32 CQTC26               :1;
		volatile u32 CQTC27               :1;
		volatile u32 CQTC28               :1;
		volatile u32 CQTC29               :1;
		volatile u32 CQTC30               :1;
		volatile u32 CQTC31               :1;
	} bits;
} CQRS14_TC;            // task clear

typedef union {
	volatile u32 v;
	struct {
		volatile u32 CQSSCIT              :16;
		volatile u32 CQSSCBC              :16;
	} bits;
} CQRS16_SSC1;      // send status configuration 1

typedef union {
	volatile u32 v;
	struct {
		volatile u32 CQSQSR               :16;
	} bits;
} CQRS17_SSC2;      // send status configuration 2

typedef union {
	volatile u32 v;
	struct {
		volatile u32 CQDCLR               :32;
	} bits;
} CQRS18_CRDCT;     // command responses for direct-command task

typedef union {
	volatile u32 v;
	struct {
		volatile u32 CQRMEM               :32;
	} bits;
} CQRS20_RMEM;      // response mode error mask

typedef union {
	volatile u32 v;
	struct {
		volatile u32 CQRMECI              :6;
		volatile u32 _PAD0                :2;
		volatile u32 CQRMETID             :5;
		volatile u32 _PAD1                :2;
		volatile u32 CQDTECI              :6;
		volatile u32 _PAD2                :2;
		volatile u32 CQDTETID             :5;
		volatile u32 _PAD3                :2;
		volatile u32 CQDTEFV              :1;
	} bits;
} CQRS21_TEI;       // task error information

typedef union {
	volatile u32 v;
	struct {
		volatile u32 CQLCRI               :6;
	} bits;
} CQRS22_CRI;       // command response index

typedef union {
	volatile u32 v;
	struct {
		volatile u32 CQLCRA               :32;
	} bits;
} CQRS23_CRI;       // command response argument


// ==================== reg addr =====
struct sdhc_regs_hrs {
	// hrs =======
	volatile HRS00_GI      hrs00_gi;
	volatile HRS01_DS      hrs01_ds;
	volatile HRS02_BS      hrs02_bs;
	volatile HRS03_AE      hrs03_ae;
	volatile HRS04_PRA     hrs04_pra;
	volatile HRS05_PRDP    hrs05_prdp;
	volatile HRS06_EC      hrs06_ec;
	volatile HRS07_IDI     hrs07_idi;
	volatile HRS08_PDUCS   hrs08_pducs;
	volatile HRS09_PCS     hrs09_pcs;
	volatile HRS10_HCSSPA  hrs10_hcsspa;
	volatile HRS11_EC      hrs11_ec;
	volatile HRS12_HIS     hrs12_his;
	volatile HRS13_HSTE    hrs13_hste;
	volatile HRS14_HSGE    hrs14_hsge;
	volatile u32           _pad0;
	volatile HRS16_HSGE    hrs16_hsge; //0x0040
	volatile u32           _pad1[12];
	volatile HRS29_SDN     hrs29_sdn;
	volatile HRS30_HCR     hrs30_hcr;
	volatile HRS31_HCV     hrs31_hcv;
	volatile HRS32_FMR     hrs32_fmr;
	volatile HRS33_TS0R    hrs33_ts0r;
	volatile HRS34_TS1R    hrs34_ts1r;
	volatile u32           _pad2;
	volatile HRS36_BSR     hrs36_bsr;
	volatile u32           _pad3[3];
	volatile HRS40_BA0     hrs40_ba0;
	volatile HRS41_BA1     hrs41_ba1;
	volatile HRS42_ACDMED  hrs42_acdmed;
	volatile HRS43_ESFACDM hrs43_esfacdm;
};

struct sdhc_regs_srs {
	volatile SRS00_SSAR              srs00_ssar;
	volatile SRS01_BSR               srs01_bsr;
	volatile SRS02_CA1               srs02_ca1;
	volatile SRS03_CTM               srs03_ctm;
	volatile SRS04_RESP0             srs04_resp0;
	volatile SRS05_RESP1             srs05_resp1;
	volatile SRS06_RESP2             srs06_resp2;
	volatile SRS07_RESP3             srs07_resp3;
	volatile SRS08_DB                srs08_db;
	volatile SRS09_PSR               srs09_psr;
	volatile SRS10_HC1               srs10_hc1;
	volatile SRS11_HC2               srs11_hc2;
	volatile SRS12_ENIS              srs12_enis;
	volatile SRS13_ENSE              srs13_ense;
	volatile SRS14_ENSE              srs14_ense;
	volatile SRS15_HCACES            srs15_hcaces;
	volatile SRS16_CAP1              srs16_cap1;
	volatile SRS17_CAP2              srs17_cap2;
	volatile SRS18_CAP3              srs18_cap3;
	volatile SRS19_CAP4              srs19_cap4;
	volatile SRS20_FE                srs20_fe;
	volatile SRS21_AES               srs21_aes;
	volatile SRS22_ASA1              srs22_asa1;
	volatile SRS23_ASA2              srs23_asa2;
	volatile SRS24_PV                srs24_pv;
	volatile SRS25_PV                srs25_pv;
	volatile SRS26_PV                srs26_pv;
	volatile SRS27_PV                srs27_pv;
	volatile u32                     _pad0[2];
	volatile SRS30_ADMA3IDA1         srs30_adma3ida1;
	volatile SRS31_ADMA3IDA2         srs31_adma3ida2;
};

struct sdhc_regs_crs {
	volatile CRS63_HCVIS    crs63_hcvis;
};

struct sdhc_regs_cqrs {
	volatile CQRS00_CQV              cqrs00_cqv;
	volatile CQRS01_CQC              cqrs01_cqc;
	volatile CQRS02_CQC              cqrs02_cqc;
	volatile CQRS03_CQC              cqrs03_cqc;
	volatile CQRS04_CQIS             cqrs04_cqis;
	volatile CQRS05_CQISE            cqrs05_cqise;
	volatile CQRS06_CQISE            cqrs06_cqise;
	volatile CQRS07_IC               cqrs07_ic;
	volatile CQRS08_CQTDLBA          cqrs08_cqtdlba;
	volatile CQRS09_CQTDLBAU32B      cqrs09_cqtdlbau32b;
	volatile CQRS10_CQTD             cqrs10_cqtd;
	volatile CQRS11_TCN              cqrs11_tcn;
	volatile CQRS12_DQS              cqrs12_dqs;
	volatile CQRS13_DPT              cqrs13_dpt;
	volatile CQRS14_TC               cqrs14_tc;
	volatile u32                     _pad0;
	volatile CQRS16_SSC1             cqrs16_ssc1;
	volatile CQRS17_SSC2             cqrs17_ssc2;
	volatile CQRS18_CRDCT            cqrs18_crdct;
	volatile u32                     _pad1;
	volatile CQRS20_RMEM             cqrs20_rmem;
	volatile CQRS21_TEI              cqrs21_tei;
	volatile CQRS22_CRI              cqrs22_cri;
	volatile CQRS23_CRI              cqrs23_cri;   // 0x45C
};

// from SD_TOP user defined registers start from 0x500
typedef union {
	volatile u32 v;
	struct {
		volatile u32 ITCFMUL                  :4;   // internal timer clock frequency multiplier
		volatile u32 ITCFVAL                  :10;  // internal timer clock frequency value
		volatile u32 CQITCFS                  :5;   // command queuing internal timer clock frequency select
	} bits;
} HWINIT_ITCF;

typedef union {
	volatile u32 v;
	struct {
		volatile u32 ITCFMUL                  :4;   // internal timer clock frequency multiplier
		volatile u32 ITCFVAL                  :10;  // internal timer clock frequency value
		volatile u32 CQITCFS                  :5;   // command queuing internal timer clock frequency select
	} bits;
} CTRL_CFG;

typedef union {
	volatile u32 v;
	struct {
		volatile u32 LOCAL_RSTN               :32;
	} bits;
} LOCAL_RSTN;

typedef union {
	volatile u32 v;
	struct {
		volatile u32 BOOT_ACK_EN              :1;
		volatile u32 BOOT_METHOD              :1;
		volatile u32 BOOT_BUSWIDTH            :2;
		volatile u32 BOOT_SDCLKDIV            :10;
		volatile u32 BOOT_ADDRWIDTH           :1;
		volatile u32 BOOT_BVS                 :3;
		volatile u32 BOOT_SPEEDMODE           :2;
		volatile u32 BOOT_TIMEOUT_ACK         :4;
		volatile u32 BOOT_DATA_TRANS_TIMEOUT  :4;
	} bits;
} BOOT_CFG_0;

typedef union {
	volatile u32 v;
	struct {
		volatile u32 BOOT_ADDR_L              :32;
	} bits;
} BOOT_ADDR_L;

typedef union {
	volatile u32 v;
	struct {
		volatile u32 BOOT_ADDR_H              :32;
	} bits;
} BOOT_ADDR_H;

typedef union {
	volatile u32 v;
	struct {
		volatile u32 BOOT_DESC_ADDR_L         :32;
	} bits;
} BOOT_DESC_ADDR_L;

typedef union {
	volatile u32 v;
	struct {
		volatile u32 BOOT_DESC_ADDR_H         :32;
	} bits;
} BOOT_DESC_ADDR_H;

typedef union {
	volatile u32 v;
	struct {
		volatile u32 BOOT_IO_DLY              :10;
		volatile u32 BOOT_BLOCK_SIZE          :12;
	} bits;
} BOOT_CFG_1;

typedef union {
	volatile u32 v;
	struct {
		volatile u32 BOOT_POW_CLK_DLY         :32;
	} bits;
} BOOT_POW_CLK_DLY;

typedef union {
	volatile u32 v;
	struct {
		volatile u32 BOOT_CLK_CMD_DLY         :32;
	} bits;
} BOOT_CLK_CMD_DLY;

typedef union {
	volatile u32 v;
	struct {
		volatile u32 BOOT_BLOCK_CNT           :32;
	} bits;
} BOOT_BLOCK_CNT;

typedef union {
	volatile u32 v;
	struct {
		volatile u32 BOOT_WR_DLY              :32;
	} bits;
} BOOT_WR_DLY;

typedef union {
	volatile u32 v;
	struct {
		volatile u32 BOOT_EXTERN              :4;
		volatile u32 BOOT_CLKADJ              :4;
		volatile u32 BOOT_PHY_DQSTM           :4;
		volatile u32 BOOT_PHY_DQTM            :10;
		volatile u32 BOOT_PHY_GLPBK           :9;
	} bits;
} BOOT_PHY_CFG_0;

typedef union {
	volatile u32 v;
	struct {
		volatile u32 BOOT_PHY_DLLMST          :1;
		volatile u32 BOOT_PHY_DLLSLV          :16;
		volatile u32 BOOT_SETUP_MODE          :1;
	} bits;
} BOOT_PHY_CFG_1;

typedef union {
	volatile u32 v;
	struct {
		volatile u32 BOOT_ACK                 :1;
		volatile u32 BOOT_DONE                :1;
		volatile u32 BOOT_ERR                 :1;
	} bits;
} BOOT_STATE;

struct user_defined_regs {
	volatile SRS17_CAP2              hwinit_srs17; //0x88000077
	volatile SRS18_CAP3              hwinit_srs18; //0x202020
	volatile SRS19_CAP4              hwinit_srs19; //0x20
	volatile SRS24_PV                hwinit_srs24; //0x40000
	volatile SRS25_PV                hwinit_srs25; //0x40002
	volatile SRS26_PV                hwinit_srs26; //0x10002
	volatile SRS27_PV                hwinit_srs27; //0x20000
	volatile HWINIT_ITCF             hwinit_itcf;
	volatile CTRL_CFG                ctrl_cfg;
	volatile LOCAL_RSTN              local_rstn;
	volatile BOOT_CFG_0              boot_cfg_0;
	volatile BOOT_ADDR_L             boot_addr_l;
	volatile BOOT_ADDR_H             boot_addr_H;
	volatile BOOT_DESC_ADDR_L        boot_desc_addr_l;
	volatile BOOT_DESC_ADDR_H        boot_desc_addr_h;
	volatile BOOT_CFG_1              boot_cfg_1;
	volatile BOOT_POW_CLK_DLY        boot_pow_clk_dly;
	volatile BOOT_CLK_CMD_DLY        boot_clk_cmd_dly;
	volatile BOOT_BLOCK_CNT          boot_block_cnt;
	volatile BOOT_WR_DLY             boot_wr_dlY;
	volatile BOOT_PHY_CFG_0          boot_phy_cfg_0;
	volatile BOOT_PHY_CFG_1          boot_phy_cfg_1;
	volatile BOOT_STATE              boot_state;
};

// dfi macro 
#define  PHY_DQ_TIMING_REG           0x2000
#define DATA_SELECT_OE_END           0
#define IO_MASK_END                  27
#define IO_MASK_ALWAYS_ON            31 0
#define PHY_DQS_TIMING_REG           0x2004
#define PHONY_DQS_CMD                19
#define PHONY_DQS                    20
#define LPBK_DQS                     21
#define EXT_LPBK_DQS                 22
#define PHY_GATE_LPBK_CTRL_REG       0x2008
#define GATE_CFG_ALWAYS_ON           6
#define UNDERRUN_SUPPRESS            18
#define RD_DEL_SEL                   19
#define SW_HALF_CYCLE_SHIFT          28
#define SYNC_METHOD                  31
#define PHY_DLL_MASTER_CTRL_REG      0x200C
#define DLL_BYPASS_MODE              23
#define DLL_START_POINT              0
#define PHY_DLL_SLAVE_CTRL_REG       0x2010
#define READ_DQS_CMD_DELAY           24
#define CLK_WRDQS_DELAY              16
#define CLK_WR_DELAY                 8
#define READ_DQS_DELAY               0
#define PHY_DLL_OBS_REG_0            0x201C
#define DLL_LOCKED_MODE              1


// ==================== data structure ======
struct sdhc_regs {
	volatile struct sdhc_regs_hrs hrs_regs;                   // 0x0 - 0xAC
	volatile u32    _PAD0[0x54];                              //(0x200 - 0xAC - 0x4)/4
	volatile struct sdhc_regs_srs srs_regs;                   // 0x200 - 0x27C
	volatile u32    _PAD1[0x1F];                              // (0x2FC - 0x27C - 0x4)/4
	volatile struct sdhc_regs_crs crs_regs;                   // 0x2FC - 0x2FC
	volatile u32    _PAD2[0x40];                              // (0x400- 0x2FC - 0x4)/4
	volatile struct sdhc_regs_cqrs cqrs_regs;                 // 0x400 - 0x45C
	volatile u32    _PAD3[0x28];                              // (0x500 - 0x45C - 0x4)/4
	volatile struct user_defined_regs user_regs;              // 0x500 - 0x558
};

// ==================== define ======
#define SDHC0_BASE (0x9000000000000000 | (u64)0x1F920000)
#define SDHC1_BASE (0x9000000000000000 | (u64)0x1F928000)

#define SDHC0_REGS  ((struct sdhc_regs*)(SDHC0_BASE))
#define SDHC1_REGS  ((struct sdhc_regs*)(SDHC1_BASE))




// function declare
extern u32 sdhc_read_phy(struct sdhc_regs *host, u32 addr);
extern void sdhc_write_phy(struct sdhc_regs *host, u32 addr, u32 data);
extern void sdhc_set_clock(struct sdhc_regs *host, u32 freq_khz);
extern void sdhc_user_reset(struct sdhc_regs *host);
extern int sdhc_init(struct sdhc_regs *host, u8 voltage, u32 kfreq);
extern int sdhc_send(struct sdhc_regs *host, SRS03_CTM srs03_val, u32 cmd_args, u64 wait_cnt, const char* desc);
extern int sdhc_send_cmd_interrupt(struct sdhc_regs *host, SRS03_CTM srs03_val, u32 cmd_args, u64 wait_cnt, const char* desc);
extern void sdhc_isr(void);

extern u32 sdhc_get_response(struct sdhc_regs *host, int reg_index);
extern int sdhc_set_tune_val(struct sdhc_regs *host, u32 val);
extern void print_host_reg_addr(struct sdhc_regs *host);
extern void print_reg_srs21(SRS21_AES srs21_val);
extern void print_reg_srs12(SRS12_ENIS srs12_val);
extern void print_reg_resp(struct sdhc_regs *host);
extern int print_card_status(struct sdhc_regs *host, u32 rca_addr);
extern volatile SRS12_ENIS global_srs12_sts0;

#endif /* __SDHC_LIB_H__ */
