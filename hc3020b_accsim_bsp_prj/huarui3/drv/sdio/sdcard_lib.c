#ifndef  __SDCARD_LIB_C__
#define  __SDCARD_LIB_C__
#include "./sdcard_lib.h"
#include "./udebug.h"

//#define DEFAULT_CMD_WAIT 20000
#define DEFAULT_CMD_WAIT 1000   /*ldf 20230817 modify:: �ο���������޸�*/

static u32 g_sd_card_rca = 0;
extern volatile u64 global_pre_cpu_cnt;
extern volatile u64 global_cur_cpu_cnt;

u32 g_sdcard_tuning_pattern[4][4] = {
	{0xFF0FFF00, 0xFFCCC3CC, 0xC33CCCFF, 0xFEFFFEEF},
	{0xFFDFFFDD, 0xFFFBFFFB, 0xBFFF7FFF, 0x77F7BDEF},
	{0xFFF0FFF0, 0x0FFCCC3C, 0xCC33CCCF, 0xFFEFFFEE},
	{0xFFFDFFFD, 0xDFFFBFFF, 0xBBFFF7FF, 0xF77F7BDE},
};

int sdcard_get_card_id(struct sdhc_regs *host, u32 id_buf[4])
{
	if(id_buf == 0) return -1;
	// CMD2 - get card ID
	SRS03_CTM srs03_val = {.v=0};
	srs03_val.bits.CIDX=2;
	srs03_val.bits.CRCCE=0;
	srs03_val.bits.RTS=1;
	sdhc_send(host, srs03_val, 0x0, DEFAULT_CMD_WAIT, "get card id");
	id_buf[0] = sdhc_get_response(host, 0);
	id_buf[1] = sdhc_get_response(host, 1);
	id_buf[2] = sdhc_get_response(host, 2);
	id_buf[3] = sdhc_get_response(host, 3);
	udebug("id:0x%x-0x%x-0x%x-0x%x\r\n", id_buf[0], id_buf[1], id_buf[2], id_buf[3]);
	return 0;
}

u32 sdcard_get_rca(struct sdhc_regs *host)
{
	SRS03_CTM srs03_val = {.v = 0};
	srs03_val.bits.CIDX=3;
	srs03_val.bits.CRCCE=1;
	srs03_val.bits.CICE=1;
	srs03_val.bits.RTS=2;
	sdhc_send(host, srs03_val, 0, DEFAULT_CMD_WAIT, "get rca_addr");
	u32 ret = sdhc_get_response(host, 0);
	udebug("rca:0x%x--0x%x\r\n", ret, ret>>16);
	return ret>>16;
}

int sdcard_init(struct sdhc_regs *host, u32 uhs_support)
{
	u32 _status, _uhs_I_supported=0;
	udebug("sd card init\r\n");

	// CMD0 - reset all cards to idle state
	SRS03_CTM srs03_val = {.bits.CIDX = 0};
	sdhc_send(host, srs03_val, 0, DEFAULT_CMD_WAIT, "issue reset");
//	delay_cnt(100000);
	delay_cnt(100);/*ldf 20230817 modify:: �ο���������޸�*/

	// CMD8 - send interface condition
	srs03_val.bits.CIDX = 8;
	srs03_val.bits.CRCCE = 1;
	srs03_val.bits.CICE = 1;
	srs03_val.bits.RTS = 2;
	sdhc_send(host, srs03_val, 0x1aa, DEFAULT_CMD_WAIT, "interface condition");
//	delay_cnt(DEFAULT_CMD_WAIT);
	delay_cnt(100);/*ldf 20230817 modify:: �ο���������޸�*/
	_status = sdhc_get_response(host, 0);
	if (_status != 0x000001aa) {
		uerror("sdcard 0 error: srs4 = 0x%x (should be 0x000001aa)\r\n", _status);
		return 1;
	}

	// ACMD41 - CMD55 - R1 - CMD41 - R3 (until R3->ready)
	udebug("sdcard 0 info: send acmd41\r\n");
	u32 resp0 = 0;
	wait_if(({
				delay_cnt(100);
				SRS03_CTM srs03_val = {.bits.CIDX = 55, .bits.CRCCE=1, .bits.CICE=1, .bits.RTS=2};
				sdhc_send(host, srs03_val, 0, DEFAULT_CMD_WAIT, "next command is application specific");
				srs03_val.v = 0;
				srs03_val.bits.CIDX=41;
				srs03_val.bits.CRCCE=0;
				srs03_val.bits.RTS=2;
				sdhc_send(host, srs03_val, 0x101f0000 | (1<<30) | (1<<24), DEFAULT_CMD_WAIT, "application specific");
				resp0 = sdhc_get_response(host, 0);
				((resp0 & (1<<31)) == 0);
				}), DEFAULT_CMD_WAIT, "SRS04.bit31==0");

	// set mode to UHS-I
	_uhs_I_supported = uhs_support & ((resp0 >> 24) & 0x01);
	if (_uhs_I_supported) {
		udebug("uhsi supported\r\n");

		// CMD11 - voltage switch
		SRS03_CTM srs03_val = {.bits.CIDX = 11, .bits.CRCCE=1, .bits.RTS=2};
		sdhc_send(host, srs03_val, 0, DEFAULT_CMD_WAIT, "voltage switch command");
		uinfo("now enter SDR12\r\n");

		// disable SDCLK
		udebug("disable sdclk\r\n");
		host->srs_regs.srs11_hc2.bits.SDCE = 0;

		// check state of DAT[3:0] lines
		udebug("check dat lines\r\n");
		wait_if(({
					0 != host->srs_regs.srs09_psr.bits.DATSL1;
					}), DEFAULT_CMD_WAIT, "0 != srs09_psr.DATSL1");

		udebug("set host voltage to 1.8v\r\n");
		host->srs_regs.srs15_hcaces.bits.V18SE = 1;
		// check if the SRS15.19/1V8SE is enabled
		wait_if(({
					0 == host->srs_regs.srs15_hcaces.bits.V18SE;
					}), DEFAULT_CMD_WAIT, "srs15_hcaces.V18SE == 0");

		// enable SDCLK
		udebug("enable sdclk\r\n");
		host->srs_regs.srs11_hc2.bits.SDCE = 1;
//		delay_cnt(12000);
		delay_cnt(100);/*ldf 20230817 modify:: �ο���������޸�*/

		// check if the DAT[3:0] lines are 1111b.
		udebug("check dat lines\r\n");
		wait_if(({
					u32 datsl1_one = host->srs_regs.srs09_psr.bits.DATSL1;
					//uclean("srs09_psr.DATSL1=0x%x\r\n", datsl1_one);
					0xf != datsl1_one;
					}), DEFAULT_CMD_WAIT, "0xf != srs09_psr.DATSL1");

		udebug("uhs mode enabled\r\n");
	} else if (uhs_support) {
		uerror("uhs mode requested but not enabled!\r\n");
		return 1;
	}

	// CMD2 - get card ID
	u32 id_buf[4];
	sdcard_get_card_id(host, id_buf);
	uinfo("card id resp:0x%x-0x%x-0x%x-0x%x\r\n", id_buf[0], id_buf[1], id_buf[2], id_buf[3]);

	// CMD3 - get card RCA
	g_sd_card_rca = sdcard_get_rca(host);
	udebug("sdcard 0 info: card rca number: 0x%x\r\n", g_sd_card_rca);
	return 0;
}

int sdcard_set_mode(struct sdhc_regs *host, enum SD_BIT_WIDTH bit_width, u8 mode, u32 rca)
{
	/*******************************************************************************************/
	//
	//  bit_width:  1 or 4 (UHS-I mode support only 4 bit data transmission)
	//  mode:
	//      0 - Default mode / UHS-I SDR12
	//      1 - High Speed mode / UHS-I SDR25
	//      2 - UHS-I SDR50
	//      3 - UHS-I SDR104
	//      4 - UHS-I DDR50
	//  For supported modes check:
	//    SD Specifications, Part 1 PLS, V4.00
	//      chapter 3.9.2 UHS-I Card Types,
	//      Fig. 3-14 "UHS-I Card Type Modes of Operation"
	/*******************************************************************************************/

	u32 _status;
	int i;

	// CMD55 with response R1
	SRS03_CTM srs03_val = {.bits.CIDX = 55, .bits.CRCCE=1, .bits.CICE=1, .bits.RTS=2};
	sdhc_send(host, srs03_val, rca<<16, DEFAULT_CMD_WAIT, "application-specific commands");

	// CMD6 with response R1
	srs03_val.v=0;
	srs03_val.bits.CIDX=6;
	srs03_val.bits.CRCCE=1;
	srs03_val.bits.CICE=1;
	srs03_val.bits.RTS=2;
	sdhc_send(host, srs03_val, bit_width, DEFAULT_CMD_WAIT, "acmd6: data bus width:0-1bit, 2-4bit");

	if (bit_width==E_SD_1BITS)
	{
		host->srs_regs.srs10_hc1.bits.DTW=0;

	} else if(bit_width == E_SD_4BITS) {
		host->srs_regs.srs10_hc1.bits.DTW=1;
	}

	udebug("set work mode: %d\r\n", mode);

	// CMD6 - enable high speed
	SRS01_BSR srs01_val = {.bits.BCCT=1, .bits.TBS=0x40, .bits.SDMABB=0x7}; //block size=0x40=64
	host->srs_regs.srs01_bsr = srs01_val;

	srs03_val.v = 0;
	srs03_val.bits.CIDX=6;
	srs03_val.bits.DPS=1;
	srs03_val.bits.CRCCE=1;
	srs03_val.bits.CICE=1;
	srs03_val.bits.RTS=2;
	srs03_val.bits.DTDS=1;
	srs03_val.bits.BCE=1;
	sdhc_send(host, srs03_val, 0x80000000|mode, DEFAULT_CMD_WAIT, "send set mode command and read 64 bytes");

	udebug("set mode and start 64 byte read:\r\n");
	u32 recv_buf[16];
	for (i=0; i<(512/32); i++) {
		wait_if(host->srs_regs.srs09_psr.bits.BRE==0, DEFAULT_CMD_WAIT, "SRS09_PSR.BRE==0");
		recv_buf[i] = host->srs_regs.srs08_db.v;
	}
	for (i=0; i<(512/32); i++)
	{
		uclean("%d=0x%x\r\n", i, recv_buf[i]);
	}

	delay_cnt (100); // MR edit (to supresss Denali errors): wait 8 cycles
	wait_if(host->srs_regs.srs12_enis.bits.TC==0, DEFAULT_CMD_WAIT, "SRS12_ENIS.TC");

	if (mode != 0) {
		host->srs_regs.srs10_hc1.bits.HSE = 1; //High Speed operating mode
	} else {
		host->srs_regs.srs10_hc1.bits.HSE = 0; //Default Speed operating mode
	}
	host->srs_regs.srs10_hc1.bits.LEDC = 1;

	SRS15_HCACES srs15_val = host->srs_regs.srs15_hcaces;
	srs15_val.bits.HV4E = 1;
	srs15_val.bits.A64B = 1;
	srs15_val.bits.UMS = mode;
	host->srs_regs.srs15_hcaces = srs15_val;
	return 0;
}

void sdcard_select_card(struct sdhc_regs* host, u32 rca_addr)
{
	// CMD7 - select card, go to TRANS state
	if (rca_addr != 0x0000) {
		udebug("select card:0x%x\r\n", rca_addr);
		SRS03_CTM srs03_val = {.bits.CIDX = 7, .bits.CRCCE=1, .bits.CICE=1, .bits.RTS=3};
		sdhc_send(host, srs03_val, rca_addr<<16, DEFAULT_CMD_WAIT, "select card");
		wait_if(host->srs_regs.srs12_enis.bits.TC==0, DEFAULT_CMD_WAIT, "SRS12_ENIS.TC==0");
	} else {
		udebug("deselect card: 0x%x\r\n", rca_addr);
		SRS03_CTM srs03_val = {.bits.CIDX = 7};
		sdhc_send(host, srs03_val, 0, DEFAULT_CMD_WAIT, "select card");
	}
}

int sdcard_write(struct sdhc_regs *host, u64 dst_addr, u64 src_addr, u64 block_cnt, u32 block_size)
{
	if (block_cnt == 0)
		return -1;
	udebug("src_addr:0x%lx, dst_addr:0x%lx, block_cnt:%d\r\n", src_addr, dst_addr, block_cnt);

	// dest memory address
	host->srs_regs.srs22_asa1.v = src_addr & 0xFFFFFFFF;
	host->srs_regs.srs23_asa2.v = (src_addr >> 32) & 0xFFFFFFFF;

	// block count
	//SDMABB = 7;       // system address boundary size: 2^(x+2) KB
	//TBS = 512;        // transfor block size Byte
	SRS01_BSR srs01_val = {.bits.SDMABB=7, .bits.TBS=block_size, .bits.BCCT=block_cnt};
	host->srs_regs.srs01_bsr = srs01_val;

	// sdma config
	host->srs_regs.srs10_hc1.bits.DMASEL = 0; // 0-sdma, 3-adma2(64-bit), 2-adma2(32-bit)

	// clear cpu timestamp for speed test
//	write_c0_count(0);
	// record cmd start timestamp
//	global_pre_cpu_cnt = read_c0_count();

	// send cmd
	SRS03_CTM srs03_val = {.v = 0};
	if (block_cnt > 1)
	{
		srs03_val.v = 0;
		srs03_val.bits.CIDX=25;
		srs03_val.bits.MSBS=1;
		srs03_val.bits.ACE=1;
		srs03_val.bits.CRCCE=1;
		srs03_val.bits.CICE=1;
		srs03_val.bits.RTS=2;
		srs03_val.bits.DPS=1;
		srs03_val.bits.BCE=1;
		srs03_val.bits.DMAE=1;
		sdhc_send(host, srs03_val, dst_addr, DEFAULT_WAIT_CNT, "write multi-block dma start");
	} else {
		srs03_val.v = 0;
		srs03_val.bits.CIDX=24;
		srs03_val.bits.CRCCE=1;
		srs03_val.bits.CICE=1;
		srs03_val.bits.RTS=2;
		srs03_val.bits.DPS=1;
		srs03_val.bits.BCE=1;
		srs03_val.bits.DMAE=1;
		sdhc_send(host, srs03_val, dst_addr, DEFAULT_WAIT_CNT, "write single-block dma start");
	}

	// check dma intterupt
	volatile SRS12_ENIS srs12_val = {.v = 0};
	volatile SRS00_SSAR srs00_val;
	wait_if(({
				delay_cnt(100);/*ldf 20230817 add*/
				srs12_val = host->srs_regs.srs12_enis;
				if(srs12_val.bits.DMAINT) // when meeting SDMABB interrupt
				{
#if 1 /*ldf 20230817 add:: �ο���������޸�*/
					u64 up_addr_next = (u64)(((((u64)host->srs_regs.srs23_asa2.v) & 0xffffffff) << 32) | host->srs_regs.srs22_asa1.v);
					udebug("DMAINT meet, update sdma address: next:0x%lx\r\n", up_addr_next);
					host->srs_regs.srs12_enis.bits.DMAINT = 1; // clear dma intterupt flag
					host->srs_regs.srs22_asa1.v = (up_addr_next) & 0xFFFFFFFF;
					host->srs_regs.srs23_asa2.v = ((up_addr_next)>> 32) & 0xFFFFFFFF;
#else
					udebug("DMAINT meet, update sdma address: 0x%lx\r\n", src_addr+512*1024);
					host->srs_regs.srs12_enis.bits.DMAINT = 1; // clear dma intterupt flag
					host->srs_regs.srs22_asa1.v = (src_addr + 512*1024) & 0xFFFFFFFF;
					host->srs_regs.srs23_asa2.v = ((src_addr + 512*1024)>> 32) & 0xFFFFFFFF;
#endif
				}
				srs12_val.bits.TC == 0;
				}), DEFAULT_WAIT_CNT, "update sdma address TC");

//	global_cur_cpu_cnt = read_c0_count();
//	uinfo("pre_cpu_cnt:0x%x, cur_cpu_cnt:0x%x, diff:%d, ms:%d\r\n", global_pre_cpu_cnt, global_cur_cpu_cnt, global_cur_cpu_cnt - global_pre_cpu_cnt, ((global_cur_cpu_cnt-global_pre_cpu_cnt)/(HZ/1000)));
}

int sdcard_read(struct sdhc_regs *host, u64 src_addr, u64 dst_addr, u64 block_cnt, u32 block_size)
{
	if (block_cnt == 0)
		return -1;
	udebug("src_addr:0x%lx, dst_addr:0x%lx, block_cnt:%d\r\n", src_addr, dst_addr, block_cnt);
	// dest memory address
	host->srs_regs.srs22_asa1.v = dst_addr & 0xFFFFFFFF;
	host->srs_regs.srs23_asa2.v = (dst_addr >> 32) & 0xFFFFFFFF;

	// block count
	//SDMABB = 7;       // system address boundary size: 2^(x+2) KB
	//TBS = 512;        // transfor block size Byte
	SRS01_BSR srs01_val = {.bits.SDMABB=7, .bits.TBS=block_size, .bits.BCCT=block_cnt};
	host->srs_regs.srs01_bsr = srs01_val;

	// sdma config
	host->srs_regs.srs10_hc1.bits.DMASEL = 0; // 0-sdma, 3-adma2(64-bit), 2-adma2(32-bit)

	// clear cpu timestamp for speed test
//	write_c0_count(0);
	// record cmd start timestamp
//	global_pre_cpu_cnt = read_c0_count();

	// send cmd
	SRS03_CTM srs03_val = {.v = 0};
	if (block_cnt > 0)
	{
		srs03_val.v = 0;
		srs03_val.bits.CIDX=18;
		srs03_val.bits.MSBS=1;
		srs03_val.bits.ACE=1;
		srs03_val.bits.DPS=1;
		srs03_val.bits.CRCCE=1;
		srs03_val.bits.CICE=1;
		srs03_val.bits.RTS=2;
		srs03_val.bits.DTDS=1;
		srs03_val.bits.BCE=1;
		srs03_val.bits.DMAE=1;
		sdhc_send(host, srs03_val, src_addr, DEFAULT_WAIT_CNT, "read dma start");
	} else {
		srs03_val.v = 0;
		srs03_val.bits.CIDX=17;
		srs03_val.bits.DPS=1;
		srs03_val.bits.CRCCE=1;
		srs03_val.bits.CICE=1;
		srs03_val.bits.RTS=2;
		srs03_val.bits.DTDS=1;
		srs03_val.bits.BCE=1;
		srs03_val.bits.DMAE=1;
		sdhc_send(host, srs03_val, src_addr, DEFAULT_WAIT_CNT, "read dma start");
	}

	// resume dma if DMAINT occur
	SRS12_ENIS srs12_val = {.v = 0};
	wait_if(({
				delay_cnt(100);/*ldf 20230817 add*/
				srs12_val = host->srs_regs.srs12_enis;
				if (srs12_val.bits.DMAINT)
				{
#if 1 /*ldf 20230817 add:: �ο���������޸�*/
					u64 up_addr_next = (u64)(((((u64)host->srs_regs.srs23_asa2.v) & 0xffffffff) << 32) | host->srs_regs.srs22_asa1.v);
					udebug("DMAINT meet, update sdma address: next:0x%lx\r\n", up_addr_next);
					host->srs_regs.srs12_enis.bits.DMAINT = 1; // clear dma intterupt flag
					host->srs_regs.srs22_asa1.v = (up_addr_next) & 0xFFFFFFFF;
					host->srs_regs.srs23_asa2.v = ((up_addr_next)>> 32) & 0xFFFFFFFF;
#else
					udebug("DMAINT meet, update sdma address: 0x%lx\r\n", dst_addr+512*1024);
					host->srs_regs.srs12_enis.bits.DMAINT = 1;
					host->srs_regs.srs22_asa1.v = (dst_addr + 512*1024) & 0xFFFFFFFF;
					host->srs_regs.srs23_asa2.v = ((dst_addr + 512*1024)>> 32) & 0xFFFFFFFF;
#endif
				}
				srs12_val.bits.TC == 0; }), 10000, "update sdma address TC");
//	global_cur_cpu_cnt = read_c0_count();
//	uinfo("pre_cpu_cnt:0x%x, cur_cpu_cnt:0x%x, diff:%d, ms:%d\r\n", global_pre_cpu_cnt, global_cur_cpu_cnt, global_cur_cpu_cnt - global_pre_cpu_cnt, ((global_cur_cpu_cnt-global_pre_cpu_cnt)/(HZ/1000)));

	return 0;
}


void sdcard_reset_disable_clock_tuning(struct sdhc_regs *host)
{
	SRS15_HCACES val = host->srs_regs.srs15_hcaces;
	val.bits.SCS = 0;
	val.bits.EXTNG = 0;
	host->srs_regs.srs15_hcaces = val;
}

void sdcard_reset_restart_tuning(struct sdhc_regs *host)
{
	SRS15_HCACES val = host->srs_regs.srs15_hcaces;
	val.bits.SCS = 0;
	val.bits.EXTNG = 1;
	host->srs_regs.srs15_hcaces = val;
}

void sdcard_stop_tuning(struct sdhc_regs *host)
{
	SRS15_HCACES val = host->srs_regs.srs15_hcaces;
	val.bits.SCS = 1;
	val.bits.EXTNG = 0;
	host->srs_regs.srs15_hcaces = val;
}

void sdcard_start_retuning_without_clock_tuning_reset(struct sdhc_regs *host)
{
	SRS15_HCACES val = host->srs_regs.srs15_hcaces;
	val.bits.SCS = 1;
	val.bits.EXTNG = 1;
	host->srs_regs.srs15_hcaces = val;
}

void sdcard_tuning(struct sdhc_regs *host)
{
	dfi_sdr104(host);
	host->srs_regs.srs01_bsr.bits.TBS = 64;
	host->srs_regs.srs15_hcaces.bits.EXTNG = 1;
	int cnt = 0;
	u32 recv_buf[64];
	do {
		udebug("tuning:%d========\r\n", cnt++);
		SRS03_CTM srs03_val = {.v=0};
		srs03_val.bits.CIDX=19;
		srs03_val.bits.CRCCE=1;
		srs03_val.bits.CICE=1;
		srs03_val.bits.DPS=1;
		srs03_val.bits.RTS=2;
		srs03_val.bits.DTDS=1;
		sdhc_send(host, srs03_val, 0x0, DEFAULT_CMD_WAIT, "send sdr104 tuning command 19");
		wait_if(host->srs_regs.srs09_psr.bits.BRE==0, DEFAULT_CMD_WAIT, "SRS09_PSR.BRE==0");
		int j = 0;
		for (j=0; j<64; j++)
		{
			recv_buf[j] = host->srs_regs.srs08_db.v;
		}
		format_print(recv_buf, 64, 16);
		host->srs_regs.srs12_enis.v = 0xffffffff;

	} while(host->srs_regs.srs15_hcaces.bits.EXTNG != 0);

	if (host->srs_regs.srs15_hcaces.bits.SCS == 1)
	{
		uinfo("tuning succeed! cnt=%d\r\n", cnt);
	} else {
		uerror("tuning failed: SCS==0\r\n");
	}
}

#endif  /* __SDCARD_LIB_C__ */
