#ifndef __SDHC_COMMON_H__
#define __SDHC_COMMON_H__
#include "./udebug.h"

#define HZ 1500000000 //1.5GHz
#define DEFAULT_WAIT_CNT (HZ/10000)

#ifndef u8
typedef unsigned char u8;
#endif
#ifndef u16
typedef unsigned short u16;
#endif
#ifndef u32
typedef unsigned int u32;
#endif
#ifndef u64
typedef unsigned long u64;
#endif


#endif /* __SDHC_COMMON_H__ */
