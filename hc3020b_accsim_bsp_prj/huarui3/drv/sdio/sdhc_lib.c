#ifndef __SDHC_LIB_C__
#define __SDHC_LIB_C__
#include "./sdhc_lib.h"
#include "./dfi_lib.c"
#include "./emmc_lib.c"
#include <pthread.h>

static pthread_t muteSem;/*ldf 20230817 add*/

volatile SRS12_ENIS global_srs12_sts0;
volatile u64 global_pre_cpu_cnt = 0;
volatile u64 global_cur_cpu_cnt = 0;

//emmc ===>  0-standard mode(0-26MHz), 1-high speed mode(0-52MHz), 2-HS200(0-200MHz), 3-HS400(0-200MHz, DDR)
//sdcard ===> default speed(0-25MHz,3.3v), high speed(0-25MHz,3.3v), SDR(12M,25M,50M,104M, 1.8v)
void sdhc_set_clock(struct sdhc_regs *host, u32 freq_khz)
{
	u32 data_timeout = 0xe;
	u32 clk_freq = (SD_HOST_CLK / 2000) / freq_khz;

	// disable sd clock
	host->srs_regs.srs11_hc2.v = 0;

	// config internal clock frequency
	SRS11_HC2 srs11_val  = {.v = 0};
	srs11_val.bits.DTCV = data_timeout;
	srs11_val.bits.SDCFSL = clk_freq;
	srs11_val.bits.ICE = 1;
	host->srs_regs.srs11_hc2 = srs11_val;
	wait_if(host->srs_regs.srs11_hc2.bits.ICS == 0, DEFAULT_WAIT_CNT, "srs11 ICS");

	// enable DLL reset
	host->hrs_regs.hrs09_pcs.bits.PHY_SW_RESET = 0;
	// set extended_wr_mode
	host->hrs_regs.hrs09_pcs.bits.EXTENDED_WR_MODE = (clk_freq > 0) ? 1 : 0;
	// release DLL reset
	host->hrs_regs.hrs09_pcs.bits.PHY_SW_RESET = 1;
	// wait PHY_INIT_COMPLETE
	wait_if(host->hrs_regs.hrs09_pcs.bits.PHY_INIT_COMPLETE == 0, DEFAULT_WAIT_CNT, "hrs09 PHY_INIT_COMPLETE");

	//enable sd clock
	srs11_val.v = 0;
	srs11_val.bits.DTCV = data_timeout;
	srs11_val.bits.SDCFSL = clk_freq;
	srs11_val.bits.ICE = 1;
	srs11_val.bits.SDCE = 1;
	host->srs_regs.srs11_hc2 = srs11_val;
	uinfo("sd clock enabled\r\n");
}

void sdhc_user_reset(struct sdhc_regs *host)
{
	// user defined register: reset
	host->user_regs.local_rstn.v = 1;
	delay_cnt(10000);
}

//voltage: 1-1.8v  2-3.3v
int sdhc_init(struct sdhc_regs *host, u8 voltage, u32 kfreq)
{
	pthread_mutex_init2(&muteSem,PTHREAD_MUTEX_RECURSIVE,
			RE_MUTEX_PRIO_NONE |RE_MUTEX_WAIT_FIFO ,PTHREAD_MUTEX_CEILING);/*ldf 20230817 add*/
	
	host->srs_regs.srs11_hc2.v = 0;
	// software reset
	host->hrs_regs.hrs00_gi.bits.SWR = 1;
	wait_if(host->hrs_regs.hrs00_gi.bits.SWR, HZ/20000, "hrs00 SWR");
	// check insert card
	dfi_init(host);
	wait_if(!host->srs_regs.srs09_psr.bits.CI, HZ/20000, "srs09 CI");

	uinfo("power off-on card ...\r\n");
	SRS10_HC1 srs10_val = host->srs_regs.srs10_hc1;
	u8 bvs = voltage == 1 ? 5 : 7;
	srs10_val.bits.BVS = bvs; // 3.3v
	//srs10_val.bits.BVS = 5; // 1.8v
	srs10_val.bits.BP = 1;
	host->srs_regs.srs10_hc1 = srs10_val;

	srs10_val.v = 0;
	srs10_val.bits.BVS = bvs;
	host->srs_regs.srs10_hc1 = srs10_val;

	delay_cnt(10000);
	srs10_val.v = 0;
	srs10_val.bits.BVS = bvs;
	srs10_val.bits.BP = 1;
	host->srs_regs.srs10_hc1 = srs10_val;

	uinfo("sd clock starting ...\r\n");
	//sdhc_set_clock(host, 20000);
	sdhc_set_clock(host, kfreq);

	// enable interrupts
	host->srs_regs.srs13_ense.v = 0xffffffff;
	uinfo("init complete.\r\n");
}

/* emmc functions
 * cmd_index     :   0-63
 * crc_enable    :   0, 1
 * resp_type     :   0:no resp, 1:136-bit, 2:48-bit, 3:48-bit with BUSY
 * */
int sdhc_send(struct sdhc_regs *host, SRS03_CTM srs03_val, u32 cmd_args, u64 wait_cnt, const char* desc)
{
	pthread_mutex_lock2(&muteSem,WAIT, NO_TIMEOUT);/*ldf 20230817 add*/
	
	uinfo("start: cmd:%d, cmd_full:0x%x, args:0x%x, wait_cnt:%d, desc:%s\r\n", srs03_val.bits.CIDX, srs03_val.v, cmd_args, wait_cnt, desc);
	// clear interrupts flags
	host->srs_regs.srs12_enis.v = 0xFFFFFFFF;
	// set command args
	host->srs_regs.srs02_ca1.v = cmd_args;
	// disable interrupts
	host->srs_regs.srs14_ense.v = 0;
	// start command
	host->srs_regs.srs03_ctm = srs03_val;
	// error or complete not occur, just wait about HZ/xxx cycles

	volatile SRS12_ENIS srs12_val = {.v = 0};
	wait_if( ({
				srs12_val = host->srs_regs.srs12_enis;
				(srs12_val.bits.EINT == 0 && srs12_val.bits.CC == 0);
				}), wait_cnt, "srs12 EINT && CC");
	uinfo("end: cmd=%d, EINT=0x%d, CC=0x%d, TC=0x%d\r\n", srs03_val.bits.CIDX, srs12_val.bits.EINT, srs12_val.bits.CC, srs12_val.bits.TC);

	if (srs12_val.bits.EINT)
	{
		print_reg_srs12(srs12_val);
	}
	
	pthread_mutex_unlock(&muteSem);/*ldf 20230817 add*/
	
	return srs12_val.bits.EINT;
}

int sdhc_send_cmd_interrupt(struct sdhc_regs *host, SRS03_CTM srs03_val, u32 cmd_args,  u64 wait_cnt, const char* desc)
{
	uinfo("start: cmd:%d, cmd_full:0x%x, args:0x%x, wait_cnt:%d, desc:%s\r\n", srs03_val.bits.CIDX, srs03_val.v, cmd_args, wait_cnt, desc);

	// clear interrupts flags
	host->srs_regs.srs12_enis.v = 0xFFFFFFFF;
	// set command args
	host->srs_regs.srs02_ca1.v = cmd_args;
	// disable interrupts
	host->srs_regs.srs14_ense.v = 0xFFFFFFFF;
	// start command
	host->srs_regs.srs03_ctm = srs03_val;
	// error or complete not occur, just wait about HZ/xxx cycles
	wait_if( ({
				(global_srs12_sts0.bits.EINT == 0 && global_srs12_sts0.bits.CC == 0);
				}), wait_cnt, "srs12 EINT && CC");
	uinfo("end: cmd=%d, EINT=0x%d, CC=0x%d\r\n", srs03_val.bits.CIDX, global_srs12_sts0.bits.EINT, global_srs12_sts0.bits.CC, global_srs12_sts0.bits.TC);

	if (global_srs12_sts0.bits.EINT)
	{
		print_reg_srs12(global_srs12_sts0);
		if (global_srs12_sts0.bits.EADMA)
		{
			print_reg_srs21(host->srs_regs.srs21_aes);
		}
	}
	return global_srs12_sts0.bits.EINT;
}

void sdhc_isr(void)
{
	static int cnt_isr = 0;
	uinfo("isr in: %d\r\n", ++cnt_isr);

	// record cpu clk elapsed
//	global_cur_cpu_cnt = read_c0_count();

	// update srs12 sts
	global_srs12_sts0 = SDHC0_REGS->srs_regs.srs12_enis;

	// clear interrupt flags
	SDHC0_REGS->srs_regs.srs12_enis = global_srs12_sts0;
	print_reg_srs12(global_srs12_sts0);

	// clear pic irq
//	clear_pic_irq(PIC_IRQ_SDIO0);
	uinfo("isr out!\r\n");
}

/*
 * resp[0-3]  : 4 response registers
 * */
u32 sdhc_get_response(struct sdhc_regs *host, int reg_index)
{
	int ret = 0;
	if (reg_index == 0)
	{
		return host->srs_regs.srs04_resp0.v;
	} else if (reg_index == 1) {
		return host->srs_regs.srs05_resp1.v;
	} else if (reg_index == 2) {
		return host->srs_regs.srs06_resp2.v;
	} else if (reg_index == 3) {
		return host->srs_regs.srs07_resp3.v;
	} else {
		return 0;
	}
}

int sdhc_set_tune_val(struct sdhc_regs *host, u32 val)
{
	dfi_hs200(host, val);
}

u32 sdhc_read_phy(struct sdhc_regs *host, u32 addr)
{
	u8 ret = 0;
	host->hrs_regs.hrs04_pra.v = addr;
	ret = host->hrs_regs.hrs05_prdp.v;
	uinfo("phy :0x%x\r\n", ret);
	return ret;
}

void sdhc_write_phy(struct sdhc_regs *host, u32 addr, u32 data)
{
	host->hrs_regs.hrs04_pra.v = addr;
	host->hrs_regs.hrs05_prdp.v = data;
	uinfo("phy :0x%x\r\n", data);
}

int print_card_status(struct sdhc_regs *host, u32 rca_addr)
{
	const char* map_sts[] = {
		"idle",
		"ready",
		"ident",
		"stby",
		"tran",
		"data",
		"rcv",
		"prg",
		"dis",
		"btst",
		"slp",
		"11recv",
		"12recv",
		"13recv",
		"14recv",
		"15recv",
	};
	SRS03_CTM srs03_val = {.bits.CIDX = 13, .bits.CRCCE = 1, .bits.CICE = 1,  .bits.RTS=2};
	MMC_ARG_CMD3 cmd3_arg = {.bits.RCA = rca_addr};
	sdhc_send(host, srs03_val, cmd3_arg.v, DEFAULT_WAIT_CNT, "print_card_status");
	MMC_RESP_R1_DEVICE_STS mmc_resp_r1_dev_sts ={.v = sdhc_get_response(host, 0)};
	udebug("sts:0x%x\r\n"
			"TEST_MODE:0x%x,        APP_COMMAND:0x%x,      APP_CMD:0x%x,           EXCEPTION_EVENT:0x%x, SWITCH_ERROR:0x%x\r\n"
			"READY_FOR_DATA:0x%x,   CURRENT_STATE:0x%x=%s, ERASE_RESET:0x%x,       WP_ERASE_SKIP:0x%x,   CID_CSD_OVERWRITE:0x%x\r\n"
			"ERROR:0x%x,            CC_ERROR:0x%x,         DEVICE_ECC_FAILED:0x%x, ILLEGAL_CMD:0x%x,     COM_CRC_ERROR:0x%x, LOCK_UNLOCK_FAILED:0x%x\r\n"
			"DEVICE_IS_LOCKED:0x%x, WP_VIOLATION:0x%x,     ERASE_PARAM:0x%x,       ERASE_SEQ_ERROR:0x%x, BLOCK_LEN_ERROR:0x%x\r\n"
			"ADDRESS_MISALIGN:0x%x, ADDRESS_OUT_OF_RANGE:0x%x\r\n",
			mmc_resp_r1_dev_sts.v,
			mmc_resp_r1_dev_sts.bits.TEST_MODE, mmc_resp_r1_dev_sts.bits.APP_COMMAND, mmc_resp_r1_dev_sts.bits.APP_CMD, mmc_resp_r1_dev_sts.bits.EXCEPTION_EVENT, mmc_resp_r1_dev_sts.bits.SWITCH_ERROR,
			mmc_resp_r1_dev_sts.bits.READY_FOR_DATA, mmc_resp_r1_dev_sts.bits.CURRENT_STATE, map_sts[mmc_resp_r1_dev_sts.bits.CURRENT_STATE], mmc_resp_r1_dev_sts.bits.ERASE_RESET,
			mmc_resp_r1_dev_sts.bits.WP_ERASE_SKIP, mmc_resp_r1_dev_sts.bits.CID_CSD_OVERWRITE, mmc_resp_r1_dev_sts.bits.ERROR,
			mmc_resp_r1_dev_sts.bits.CC_ERROR, mmc_resp_r1_dev_sts.bits.DEVICE_ECC_FAILED, mmc_resp_r1_dev_sts.bits.ILLEGAL_CMD,
			mmc_resp_r1_dev_sts.bits.COM_CRC_ERROR, mmc_resp_r1_dev_sts.bits.LOCK_UNLOCK_FAILED, mmc_resp_r1_dev_sts.bits.DEVICE_IS_LOCKED,
			mmc_resp_r1_dev_sts.bits.WP_VIOLATION, mmc_resp_r1_dev_sts.bits.ERASE_PARAM, mmc_resp_r1_dev_sts.bits.ERASE_SEQ_ERROR,
			mmc_resp_r1_dev_sts.bits.BLOCK_LEN_ERROR, mmc_resp_r1_dev_sts.bits.ADDRESS_MISALIGN, mmc_resp_r1_dev_sts.bits.ADDRESS_OUT_OF_RANGE);
	return mmc_resp_r1_dev_sts.bits.CURRENT_STATE;
}

void print_host_reg_addr(struct sdhc_regs *host)
{
	uart_printf("hrs_regs  addr:0x%x size:0x%x len:0x%x\r\n", &host->hrs_regs,  sizeof(host->hrs_regs)  ,sizeof(host->hrs_regs)/4   );
	uart_printf("srs_regs  addr:0x%x size:0x%x len:0x%x\r\n", &host->srs_regs,  sizeof(host->srs_regs)  ,sizeof(host->srs_regs)/4   );
	uart_printf("crs_regs  addr:0x%x size:0x%x len:0x%x\r\n", &host->crs_regs,  sizeof(host->crs_regs)  ,sizeof(host->crs_regs)/4   );
	uart_printf("cqrs_regs addr:0x%x size:0x%x len:0x%x\r\n", &host->cqrs_regs, sizeof(host->cqrs_regs) ,sizeof(host->cqrs_regs)/4   );
	uart_printf("user_regs addr:0x%x size:0x%x len:0x%x\r\n", &host->user_regs, sizeof(host->user_regs) ,sizeof(host->user_regs)/4   );
}

void print_reg_srs21(SRS21_AES srs21_val)
{
	uart_printf("SRS21_AES:0x%x\r\n", srs21_val.v);
	uart_printf("|-EAMDAS:0x%x\r\n", srs21_val.bits.EAMDAS);
	uart_printf("`-EAMDAL:0x%x\r\n", srs21_val.bits.EAMDAL);
}

void print_reg_srs12(SRS12_ENIS srs12_val)
{
	// SRS12_ENIS
	uart_printf("SRS12_ENIS:0x%x, \r\n"
			"CC:0x%x,   TC:0x%x,    BGE:0x%x,  DMAINT:0x%x, BWR:0x%x,   BRR:0x%x\r\n"
			"CIN:0x%x,  CR:0x%x,    CINT:0x%x, FXE:0x%x,    CQINT:0x%x, EINT:0x%x\r\n"
			"ECT:0x%x,  ECCRC:0x%x, ECEB:0x%x, ECI:0x%x,    EDT:0x%x,   EDCRC:0x%x\r\n"
			"EDEB:0x%x, ECL:0x%x,   EAC:0x%x,  EADMA:0x%x,  ERSP:0x%x\r\n",
			srs12_val.v,
			srs12_val.bits.CC,
			srs12_val.bits.TC,
			srs12_val.bits.BGE,
			srs12_val.bits.DMAINT,
			srs12_val.bits.BWR,
			srs12_val.bits.BRR,
			srs12_val.bits.CIN,
			srs12_val.bits.CR,
			srs12_val.bits.CINT,
			srs12_val.bits.FXE,
			srs12_val.bits.CQINT,
			srs12_val.bits.EINT,
			srs12_val.bits.ECT,
			srs12_val.bits.ECCRC,
			srs12_val.bits.ECEB,
			srs12_val.bits.ECI,
			srs12_val.bits.EDT,
			srs12_val.bits.EDCRC,
			srs12_val.bits.EDEB,
			srs12_val.bits.ECL,
			srs12_val.bits.EAC,
			srs12_val.bits.EADMA,
			srs12_val.bits.ERSP);
}

void print_reg_srs15(SRS15_HCACES srs15_val)
{
	// SRS12_ENIS
	uart_printf("SRS15_HCACES:0x%x\r\n"
			"ACNE:0x%x,       ACTE:0x%x,       ACCE:0x%x,    ACEBE:0x%x, ACIE:0x%x,   ACRE:0x%x\r\n"
			"CNIACE:0x%x,     UMS:0x%x,        V18SE:0x%x,   DSS:0x%x,   EXTNG:0x%x,  SCS:0x%x\r\n"
			"LVSIEXEC:0x%x,   ADMA2LM:0x%x,    CMD23E:0x%x,  HV4E:0x%x,  A64B:0x%x,   PVE:0x%x\r\n",
			srs15_val.v,
			srs15_val.bits.ACNE,
			srs15_val.bits.ACTE,
			srs15_val.bits.ACCE,
			srs15_val.bits.ACEBE,
			srs15_val.bits.ACIE,
			srs15_val.bits.ACRE,
			srs15_val.bits.CNIACE,
			srs15_val.bits.UMS,
			srs15_val.bits.V18SE,
			srs15_val.bits.DSS,
			srs15_val.bits.EXTNG,
			srs15_val.bits.SCS,
			srs15_val.bits.LVSIEXEC,
			srs15_val.bits.ADMA2LM,
			srs15_val.bits.CMD23E,
			srs15_val.bits.HV4E,
			srs15_val.bits.A64B,
			srs15_val.bits.PVE);
}

void print_reg_srs08(struct sdhc_regs* host)
{
	uart_printf("SRS08_DB:0x%x\r\n", host->srs_regs.srs08_db.v);
}

void print_reg_resp(struct sdhc_regs* host)
{
	// srs04_resp0...
	uart_printf("SRS04_RESP0:0x%x\r\n", host->srs_regs.srs04_resp0.v);
	uart_printf("SRS05_RESP1:0x%x\r\n", host->srs_regs.srs05_resp1.v);
	uart_printf("SRS06_RESP2:0x%x\r\n", host->srs_regs.srs06_resp2.v);
	uart_printf("SRS07_RESP3:0x%x\r\n", host->srs_regs.srs07_resp3.v);
}

void print_cap_srs16(struct sdhc_regs *host)
{
	//srs16
	SRS16_CAP1 val16 = host->srs_regs.srs16_cap1;
	uclean("SRS16_CAP1:v=0x%x\r\n"
			"TCF      0x%x\r\n"
			"_PAD0    0x%x\r\n"
			"TCU      0x%x\r\n"
			"BCSDCLK  0x%x\r\n"
			"SRS16MBL 0x%x\r\n"
			"EDS8     0x%x\r\n"
			"ADAM2S   0x%x\r\n"
			"ADMA1S   0x%x\r\n"
			"HSS      0x%x\r\n"
			"DMAS     0x%x\r\n"
			"SRS      0x%x\r\n"
			"VS33     0x%x\r\n"
			"VS30     0x%x\r\n"
			"VS18     0x%x\r\n"
			"A64SV4   0x%x\r\n"
			"A64SV3   0x%x\r\n"
			"AIS      0x%x\r\n"
			"SLT      0x%x\r\n",
			val16.v,
			val16.bits.TCF     ,
			val16.bits._PAD0   ,
			val16.bits.TCU     ,
			val16.bits.BCSDCLK ,
			val16.bits.SRS16MBL,
			val16.bits.EDS8    ,
			val16.bits.ADAM2S  ,
			val16.bits.ADMA1S  ,
			val16.bits.HSS     ,
			val16.bits.DMAS    ,
			val16.bits.SRS     ,
			val16.bits.VS33    ,
			val16.bits.VS30    ,
			val16.bits.VS18    ,
			val16.bits.A64SV4  ,
			val16.bits.A64SV3  ,
			val16.bits.AIS     ,
			val16.bits.SLT);
}

void print_cap_srs17(struct sdhc_regs *host)
{
	SRS17_CAP2 srs17_val = host->srs_regs.srs17_cap2;
	uclean("SRS17_CAP2:value=0x%x\r\n"
			"SDR50        0x%x\r\n"
			"SDR104       0x%x\r\n"
			"DDR50        0x%x\r\n"
			"UHSII        0x%x\r\n"
			"DRVA         0x%x\r\n"
			"DRVC         0x%x\r\n"
			"DRVD         0x%x\r\n"
			"_PAD0        0x%x\r\n"
			"RTNGCNT      0x%x\r\n"
			"_PAD1        0x%x\r\n"
			"UTSM50       0x%x\r\n"
			"RTNGM        0x%x\r\n"
			"CLKMPR       0x%x\r\n"
			"_PAD2        0x%x\r\n"
			"ADMA3SUP     0x%x\r\n"
			"VDD2S        0x%x\r\n"
			"_PAD3        0x%x\r\n"
			"LVSH         0x%x\r\n"      ,
			srs17_val.v,
			srs17_val.bits.SDR50       ,
			srs17_val.bits.SDR104      ,
			srs17_val.bits.DDR50       ,
			srs17_val.bits.UHSII       ,
			srs17_val.bits.DRVA        ,
			srs17_val.bits.DRVC        ,
			srs17_val.bits.DRVD        ,
			srs17_val.bits._PAD0       ,
			srs17_val.bits.RTNGCNT     ,
			srs17_val.bits._PAD1       ,
			srs17_val.bits.UTSM50      ,
			srs17_val.bits.RTNGM       ,
			srs17_val.bits.CLKMPR      ,
			srs17_val.bits._PAD2       ,
			srs17_val.bits.ADMA3SUP    ,
			srs17_val.bits.VDD2S       ,
			srs17_val.bits._PAD3       ,
			srs17_val.bits.LVSH         );
}

void print_cap_srs18(struct sdhc_regs *host)
{
	SRS18_CAP3 srs18_val = host->srs_regs.srs18_cap3;
	uclean("SRS18_CAP3:value=0x%x\r\n"
			"MC33       0x%x\r\n"
			"MC30       0x%x\r\n"
			"MC18       0x%x\r\n"          ,
			srs18_val.v,
			srs18_val.bits.MC33      ,
			srs18_val.bits.MC30      ,
			srs18_val.bits.MC18);
}

void print_cap_srs19(struct sdhc_regs *host)
{
	SRS19_CAP4 srs19_val  = host->srs_regs.srs19_cap4;
	uclean("SRS19_CAP4:value=0x%x\r\n"
			"MC18V2 0x%x\r\n",
			srs19_val.v,
			srs19_val.bits.MC18V2);
}



#endif /* __SDHC_LIB_C__ */
