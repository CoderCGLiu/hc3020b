#ifndef  __EMMC_LIB_H__
#define  __EMMC_LIB_H__

#include "./sdhc_common.h"
#include "./sdhc_lib.h"

#define EMMC_SIZE			(0x200000000UL)//8GB
#define EMMC_BLOCK_SIZE		512
#define EMMC_BLOCK_COUNT	(EMMC_SIZE / EMMC_BLOCK_SIZE)

#define EMMC_CARD_ADDRESS 0x1234

/* ========================== from emmc5.1 pdf */
typedef u32 MMC_ARG_CMD0   ;
typedef union {
	u32 v;
	struct {
		u32 RESERVED0            :7;
		u32 VOL_17_195           :1;
		u32 VOL_20_26            :7;
		u32 VOL_27_36            :9;
		u32 RESERVED1            :5;
		u32 ACCESS_MODE          :2;   // 0-byte mode, 2-sector mode
		u32 POWER_STS_OR_BUSY    :1;
	} bits; // reg OCR
} MMC_ARG_CMD1;
typedef u32 MMC_ARG_CMD2   ;
typedef union {
	u32 v;
	struct {
		u32 _PAD0          :16;
		u32 RCA            :16;
	} bits; // reg RCA
} MMC_ARG_CMD3;
typedef union {
	u32 v;
	struct {
		u32 _PAD0          :16;
		u32 DSR            :16;
	} bits; // reg DSR
} MMC_ARG_CMD4;
typedef union {
	u32 v;
	struct {
		u32 _PAD0          :15;
		u32 SLEEP_AWAKE    :1;
		u32 RCA            :16;
	} bits; // reg RCA
} MMC_ARG_CMD5;
typedef union {
	u32 v;
	struct {
		u32 CMD_SET        :3;
		u32 SET00          :5;
		u32 VALUE          :8;
		u32 INDEX          :8;
		u32 ACCESS         :2;
		u32 SET01          :2;
	} bits; // reg RCA
} MMC_ARG_CMD6;
typedef union {
	u32 v;
	struct {
		u32 _PAD0          :16;
		u32 RCA            :16;
	} bits; // reg RCA
} MMC_ARG_CMD7;
typedef u32 MMC_ARG_CMD8   ;
typedef union {
	u32 v;
	struct {
		u32 _PAD0          :16;
		u32 RCA            :16;
	} bits; // reg RCA
} MMC_ARG_CMD9;
typedef union {
	u32 v;
	struct {
		u32 _PAD0          :16;
		u32 RCA            :16;
	} bits; // reg RCA
} MMC_ARG_CMD10;
typedef u32 MMC_ARG_CMD11  ;
typedef union {
	u32 v;
	struct {
		u32 HPI            :1;
		u32 _PAD0          :15;
		u32 RCA            :16;
	} bits; // reg RCA
} MMC_ARG_CMD12;
typedef union {
	u32 v;
	struct {
		u32 HPI            :1;
		u32 _PAD0          :14;
		u32 SQS            :1;
		u32 RCA            :16;
	} bits; // reg RCA
} MMC_ARG_CMD13;
typedef u32 MMC_ARG_CMD14  ;
typedef union {
	u32 v;
	struct {
		u32 _PAD0          :16;
		u32 RCA            :16;
	} bits; // reg RCA
} MMC_ARG_CMD15;
typedef u32 MMC_ARG_CMD16  ;
typedef u32 MMC_ARG_CMD17  ;
typedef u32 MMC_ARG_CMD18  ;
typedef u32 MMC_ARG_CMD19  ;
typedef u32 MMC_ARG_CMD20  ;
typedef u32 MMC_ARG_CMD21  ;
typedef u32 MMC_ARG_CMD22  ;
typedef u32 MMC_ARG_CMD23UP;
typedef u32 MMC_ARG_CMD23P ;
typedef u32 MMC_ARG_CMD24  ;
typedef u32 MMC_ARG_CMD25  ;
typedef u32 MMC_ARG_CMD26  ;
typedef u32 MMC_ARG_CMD27  ;
typedef u32 MMC_ARG_CMD28  ;
typedef u32 MMC_ARG_CMD29  ;
typedef u32 MMC_ARG_CMD30  ;
typedef u32 MMC_ARG_CMD31  ;
typedef u32 MMC_ARG_CMD32  ;
typedef u32 MMC_ARG_CMD33  ;
typedef u32 MMC_ARG_CMD34  ;
typedef u32 MMC_ARG_CMD35  ;
typedef u32 MMC_ARG_CMD36  ;
typedef u32 MMC_ARG_CMD37  ;
typedef u32 MMC_ARG_CMD38  ;
typedef u32 MMC_ARG_CMD39  ;
typedef u32 MMC_ARG_CMD40  ;
typedef u32 MMC_ARG_CMD41  ;
typedef u32 MMC_ARG_CMD42  ;
typedef u32 MMC_ARG_CMD43  ;
typedef u32 MMC_ARG_CMD44  ;
typedef u32 MMC_ARG_CMD45  ;
typedef u32 MMC_ARG_CMD46  ;
typedef u32 MMC_ARG_CMD47  ;
typedef u32 MMC_ARG_CMD48  ;
typedef u32 MMC_ARG_CMD49  ;
typedef u32 MMC_ARG_CMD50  ;
typedef u32 MMC_ARG_CMD51  ;
typedef u32 MMC_ARG_CMD52  ;
typedef u32 MMC_ARG_CMD53  ;
typedef u32 MMC_ARG_CMD54  ;
typedef union {
	u32 v;
	struct {
		u32 _PAD0          :16;
		u32 RCA            :16;
	} bits; // reg RCA
} MMC_ARG_CMD55;
typedef u32 MMC_ARG_CMD56  ;
typedef u32 MMC_ARG_CMD57  ;
typedef u32 MMC_ARG_CMD58  ;
typedef u32 MMC_ARG_CMD59  ;
typedef u32 MMC_ARG_CMD60  ;
typedef u32 MMC_ARG_CMD61  ;
typedef u32 MMC_ARG_CMD62  ;
typedef u32 MMC_ARG_CMD63  ;


typedef union {
	u8  v[6];  //48 bits
	struct {
		u32 END_BIT    :1;  // 1
		u32 CRC_7      :7;
		u32 DEV_STS    :32;
		u32 CMD_IDX    :6;
		u32 TRANS_B    :1;  // 0
		u32 START_B    :1;  // 0
	} bits;
} MMC_RESP_R1; // normal response

typedef union {
	u32  v;
	struct {
		u32 TEST_MODE               :2;
		u32 APP_COMMAND             :2;
		u32 _PAD0                   :1;
		u32 APP_CMD                 :1;
		u32 EXCEPTION_EVENT         :1;
		u32 SWITCH_ERROR            :1;
		u32 READY_FOR_DATA          :1;  // 8
		u32 CURRENT_STATE           :4;  //9-12
		u32 ERASE_RESET             :1;
		u32 _PAD1                   :1;
		u32 WP_ERASE_SKIP           :1;
		u32 CID_CSD_OVERWRITE       :1; //16
		u32 _PAD2                   :1;
		u32 _PAD3                   :1;
		u32 ERROR                   :1;
		u32 CC_ERROR                :1;
		u32 DEVICE_ECC_FAILED       :1;
		u32 ILLEGAL_CMD             :1;
		u32 COM_CRC_ERROR           :1;
		u32 LOCK_UNLOCK_FAILED      :1; //24
		u32 DEVICE_IS_LOCKED        :1;
		u32 WP_VIOLATION            :1;
		u32 ERASE_PARAM             :1;
		u32 ERASE_SEQ_ERROR         :1;
		u32 BLOCK_LEN_ERROR         :1;
		u32 ADDRESS_MISALIGN        :1;
		u32 ADDRESS_OUT_OF_RANGE    :1;
	} bits;
} MMC_RESP_R1_DEVICE_STS; // normal response

typedef union {
	u8  v[17]; //136 bits
	struct {
		u32 END_BIT     :1;   // 1
		u32 CID_CSD0    :32;
		u32 CID_CSD1    :32;
		u32 CID_CSD2    :32;
		u32 CID_CSD3    :31;
		u32 CHECK_B     :6;
		u32 TRANS_B     :1;   // 0
		u32 START_B     :1;   // 0
	} bits;
} MMC_RESP_R2; // CRD CSD register

typedef union {
	u8  v[6]; //48 bits
	struct {
		u32 END_BIT    :1;  // 1
		u32 CHECK_B0   :7;
		u32 OCR        :32;
		u32 CHECK_B1   :6; // 111111
		u32 TRANS_B    :1; // 0
		u32 START_B    :1; // 0
	} bits;
} MMC_RESP_R3; // OCR response

typedef union {
	u8  v[6]; //48 bits
	struct {
		u32 END_BIT    :1;  // 1
		u32 CRC        :7;
		u32 READ_CTX   :8;
		u32 REG_ADDR   :7;
		u32 STS        :1;
		u32 RCA        :16;
		u32 CMD39      :6;  // 100111
		u32 TRANS_B    :1;  // 0
		u32 START_B    :1;  // 0
	} bits;
} MMC_RESP_R4; // fast io

typedef union {
	u8  v[6]; //48 bits
	struct {
		u32 END_BIT    :1;  // 1
		u32 CRC        :7;
		u32 ND_IRQ     :16;
		u32 RCA        :16;
		u32 CMD40      :6;  // 101000
		u32 TRANS_B    :1;  // 0
		u32 START_B    :1;  // 0
	} bits;
} MMC_RESP_R5; // interrupt request

enum MMC_MODE {
	E_SDR = 0,
	E_HS,
	E_HS200,
	E_HS400,
};

enum MMC_VOL {
	E_1o2 = 1,
	E_1o8,
	E_3,
};

enum MMC_BIT_WIDTH {
	E_1BITS = 0,
	E_4BITS = 1,
	E_8BITS = 2,
};

struct MMC_MODE_MAP {
	char*                         desc;
	enum MMC_MODE                 mode;
	enum MMC_VOL                  voltage;
	enum MMC_BIT_WIDTH            bit_width;
	u32  sdhc_clk; //khz
};



#define EMMC_CMD0_GO_IDLE_STATE 0
#define EMMC_CMD0_GO_PRE_IDLE_STATE 0xF0F0F0F0
#define EMMC_CMD0_BOOT_INITIATION   0xFFFFFFFA

extern void format_print(u8 *buf, u32 len, u32 break_len);

#endif  /* __EMMC_LIB_H__ */
