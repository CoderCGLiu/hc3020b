/*!************************************************************
 * @file: Wdog.h
 * @brief: 本文件为华睿3号用户看门狗驱动实现头文件
 * @author:
 * @date:
 * @version: 1.0.0
 * @note:
 * ------------------------------------------------------------  
 * Change History:
 * <Date> | <Author> | <Description>
 * 
 *
 **************************************************************/
#ifndef WDOG_H_
#define WDOG_H_

/*
 * 宏定义
 * */

#ifndef OK
#define OK    (0)
#endif

#ifndef ERR
#define ERR    (1)
#endif

#define WDOG_FREQ 33000000

/* BASE */
#define WDOG_REGISTER_BASE    0xffffffffbf0c0000

/* watchdog load regiter */
#define WDOG_LOAD_REG_LOW     0xffffffffbf0c0400
#define WDOG_LOAD_REG_HIGH    0xffffffffbf0c0404

/* watchdog control register */
#define WDOG_CTRL_REG         0xffffffffbf0c0408
#define WDOG_CTRL_INTEN       (0x1 << 0)
#define WDOG_CTRL_RSTEN_SS    (0x1 << 1)
#define WDOG_CTRL_RSTEN_SH    (0x1 << 2)

/* watchdog int status register */
#define WDOG_INT_STATE_REG    0xffffffffbf0c040c
#define WDOG_STA_RIS          (0x1 << 0)
#define WDOG_STA_CIS          (0x1 << 1)

/* watchdog interrupt clear register */
#define WDOG_INT_CLEAR_REG    0xffffffffbf0c0410
#define WDOG_INT_CLRAR_CLR    0xffffffffffffffff

/* watchdog lock register */
#define WDOG_LOCK_REG         0xffffffffbf0c0414
#define WDOG_LOCK_CIS         (0x1 << 1)
#define WDOG_LOCK_RIS         (0x1 << 0)

/* watchdog value register */
#define WDOG_VAL_REG_LOW      0xffffffffbf0c0418
#define WDOG_VAL_REG_HIGH     0xffffffffbf0c041c

#define HRWDOG_INT_PIN        41
#define HRWDOG_CORE_PIN       1
#define HRWDOG_CORE_NUM       0

/* misc */
#define SYS_CTRL_REG          0xffffffffbf0c0010
//#define SYS_STA_R           0xffffffffbf0c0014
#define SYS_LOCK_REG          0xffffffffbf0c0028 /*xxx ldf 20230914 note:: 3020b下解锁寄存器*/
#define SYS_RSTN_TIME_REG     0xffffffffbf0c0028 /*xxx ldf 20230914 note:: 0x1f0c0028是3080下的地址，需确认3020下对应哪个地址*/


typedef void 		(*VOIDFUNCPTR_WDOG) (int);


#define WDOG_REG_GET(addr)          \
		*(volatile unsigned int *)(addr)
#define WDOG_REG_SET(addr, data)    \
		*(volatile unsigned int *)(addr) = data

#define HRWDOG_VXBNAME "hrWdog"


#endif /* HR2WATCHDOG_H_ */
