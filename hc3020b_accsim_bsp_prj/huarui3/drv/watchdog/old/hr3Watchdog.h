#ifndef __H_HR3_WDOG__
#define __H_HR3_WDOG__

#if 1
/* watchdog register base */
#define WDOG_REGISTER_BASE 0x900000001f038400UL

/* watchdog load regiter */
#define WDOG_LOAD_LOW_R       (WDOG_REGISTER_BASE + 0x0)
#define WDOG_LOAD_HIGH_R      (WDOG_REGISTER_BASE + 0x4)

/* watchdog control register */
#define WDOG_CTRL_R           (WDOG_REGISTER_BASE + 0x8)
#define WDOG_CTRL_INTEN       (0x1 << 0)
#define WDOG_CTRL_RSTEN_SS    (0x1 << 1)
#define WDOG_CTRL_RSTEN_SH    (0x1 << 2)

/* watchdog int status register */
#define WDOG_STA_R            (WDOG_REGISTER_BASE + 0xc)
#define WDOG_STA_RIS          (0x1 << 0)
#define WDOG_STA_CIS          (0x1 << 1)

/* watchdog interrupt clear register */
#define WDOG_INT_CLR_R        (WDOG_REGISTER_BASE + 0x10)
#define WDOG_INT_CLR_CLR      0xFFFFFFFF

/* watchdog lock register */
#define WDOG_LOCK_R           (WDOG_REGISTER_BASE + 0x14)
#define WDOG_LOCK_CIS         (0x1 << 1)
#define WDOG_LOCK_RIS         (0x1 << 0)

/* watchdog value register */
#define WDOG_VALUE_LOW_R      (WDOG_REGISTER_BASE + 0x18)
#define WDOG_VALUE_HIGH_R     (WDOG_REGISTER_BASE + 0x1c)


/* misc */
#define SYS_CTRL_R       0x900000001f038010UL
//#define SYS_STA_R        0x0x900000001f038014
#define SYS_LOCK_R       0x900000001f038018UL

#else

/* watchdog load regiter */
#define WDOG_LOAD_LOW_R       0x1f038400
#define WDOG_LOAD_HIGH_R      0x1f038404

/* watchdog control register */
#define WDOG_CTRL_R           0x1f038408
#define WDOG_CTRL_INTEN       (0x1 << 0)
#define WDOG_CTRL_RSTEN_SS    (0x1 << 1)
#define WDOG_CTRL_RSTEN_SH    (0x1 << 2)

/* watchdog int status register */
#define WDOG_STA_R            0x1f03840c
#define WDOG_STA_RIS          (0x1 << 0)
#define WDOG_STA_CIS          (0x1 << 1)

/* watchdog interrupt clear register */
#define WDOG_INT_CLR_R        0x1f038410
#define WDOG_INT_CLR_CLR      0xFFFFFFFF

/* watchdog lock register */
#define WDOG_LOCK_R           0x1f038414
#define WDOG_LOCK_CIS         (0x1 << 1)
#define WDOG_LOCK_RIS         (0x1 << 0)

/* watchdog value register */
#define WDOG_VALUE_LOW_R      0x1f038418
#define WDOG_VALUE_HIGH_R     0x1f03841c


/* misc */
#define SYS_CTRL_R            0x1f038010
//#define SYS_STA_R           0x1f038014
#define SYS_LOCK_R            0x1f038018
#define SYS_RSTN_TIME_R       0x1f038028

#endif

#endif
