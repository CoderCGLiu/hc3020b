//=====================================================================
//  Filename           : sw_wdog_base.c
//  Author             : fudy
//  Create on          : 2020-11-10
//  Description        : wdog basic test
//  History:
//              1.2020-11-10, fudy, create                      
//=====================================================================

//#include "watchdog.h"
//#include "hw_io.h"
//#include "io.h"
//
//#include "my_types.h"
//#include "mydef.h"
//#include "hw_addr.h"
//#include "debug.h"
//
//#include "hrdsp3020_pic.h"
#include <reworks/types.h>
#include <reworks/printk.h>
#include "drvCommon.h"

#define PIC_IRQ_WDOG 23

#define H32(x)	(((x) & 0xffffffff00000000) >> 32)
#define L32(x)	((x) & 0xffffffff)


static void print_null(const char *fmt, ...)
{
}

//#define debug_print  uart_printf
#define debug_print  printk
#define uart_printf  printk

#if 0
#define debug_print1  uart_printf
#else
#define debug_print1 print_null
#endif



#ifndef OK
#define OK	(0)
#endif

#ifndef ERR
#define ERR	(1)
#endif

#if 1
/* watchdog register base */
#define WDOG_REGISTER_BASE 0x900000001f038400

/* watchdog load regiter */
#define WDOG_LOAD_LOW_R       (WDOG_REGISTER_BASE + 0x0)
#define WDOG_LOAD_HIGH_R      (WDOG_REGISTER_BASE + 0x4)

/* watchdog control register */
#define WDOG_CTRL_R           (WDOG_REGISTER_BASE + 0x8)
#define WDOG_CTRL_INTEN       (0x1 << 0)
#define WDOG_CTRL_RSTEN_SS    (0x1 << 1)
#define WDOG_CTRL_RSTEN_SH    (0x1 << 2)

/* watchdog int status register */
#define WDOG_STA_R            (WDOG_REGISTER_BASE + 0xc)
#define WDOG_STA_RIS          (0x1 << 0)
#define WDOG_STA_CIS          (0x1 << 1)

/* watchdog interrupt clear register */
#define WDOG_INT_CLR_R        (WDOG_REGISTER_BASE + 0x10)
#define WDOG_INT_CLR_CLR      0xFFFFFFFF

/* watchdog lock register */
#define WDOG_LOCK_R           (WDOG_REGISTER_BASE + 0x14)
#define WDOG_LOCK_CIS         (0x1 << 1)
#define WDOG_LOCK_RIS         (0x1 << 0)

/* watchdog value register */
#define WDOG_VALUE_LOW_R      (WDOG_REGISTER_BASE + 0x18)
#define WDOG_VALUE_HIGH_R     (WDOG_REGISTER_BASE + 0x1c)


/* misc */
#define SYS_CTRL_R       0x900000001f038010
//#define SYS_STA_R        0x0x900000001f038014
#define SYS_LOCK_R       0x900000001f038018

#else

/* watchdog load regiter */
#define WDOG_LOAD_LOW_R       0x1f038400
#define WDOG_LOAD_HIGH_R      0x1f038404

/* watchdog control register */
#define WDOG_CTRL_R           0x1f038408
#define WDOG_CTRL_INTEN       (0x1 << 0)
#define WDOG_CTRL_RSTEN_SS    (0x1 << 1)
#define WDOG_CTRL_RSTEN_SH    (0x1 << 2)

/* watchdog int status register */
#define WDOG_STA_R            0x1f03840c
#define WDOG_STA_RIS          (0x1 << 0)
#define WDOG_STA_CIS          (0x1 << 1)

/* watchdog interrupt clear register */
#define WDOG_INT_CLR_R        0x1f038410
#define WDOG_INT_CLR_CLR      0xFFFFFFFF

/* watchdog lock register */
#define WDOG_LOCK_R           0x1f038414
#define WDOG_LOCK_CIS         (0x1 << 1)
#define WDOG_LOCK_RIS         (0x1 << 0)

/* watchdog value register */
#define WDOG_VALUE_LOW_R      0x1f038418
#define WDOG_VALUE_HIGH_R     0x1f03841c


/* misc */
#define SYS_CTRL_R            0x1f038010
//#define SYS_STA_R           0x1f038014
#define SYS_LOCK_R            0x1f038018
#define SYS_RSTN_TIME_R       0x1f038028

#endif

/*
 * base:register r/w
 *
 * WDOG_LOAD_LOW_R
 * WDOG_LOAD_HIGH_R
 * WDOG_CTRL_R
 * 	   watchdog int status register
 * WDOG_STA_R 
 * 	   watchdog interrupt clear register
 * WDOG_INT_CLR_R
 *     watchdog lock register 
 * WDOG_LOCK_R
 *     watchdog value register:
 * WDOG_VALUE_LOW_R
 * WDOG_VALUE_HIGH_R
 *
 */

/***********************************************************************
 *
 * 基本项
 *
 **********************************************************************/

/* 
 * 看门狗时钟使能
 */
static void wdog_clk_en()
{
	//tmp
	uart_printf("CTRL:%x  \n",phx_read_u32(SYS_CTRL_R));
	

	phx_write_u32(SYS_LOCK_R,0xa5accede);//misc lock free
	phx_write_u32(SYS_CTRL_R, (phx_read_u32(SYS_CTRL_R)) | 1);//misc wdog_clk_en

	/*tmp*/uart_printf("CTRL:%x  ,WD CLK EN!\n",phx_read_u32(SYS_CTRL_R));


	return;
}	




static void reg_r()
{
	printk("printk:  \n");
	printf("printf:  \n");
	uart_printf("uart_printf:  \n");	

	unsigned long addr = WDOG_LOAD_LOW_R;
	
    uart_printf("==WatchDog==BASE==register read:  \n");
	
	for(addr = WDOG_LOAD_LOW_R; addr <= WDOG_VALUE_HIGH_R; addr+=4){
		uart_printf("0x%x	0x%x  \n",addr,readw(addr));
	}


    return;
}

/*
 * 寄存器复位默认值检测
 *
 * 复位默认值：
 * WDOG_CTRL_R		0
 * WDOG_STA_R		bit1:0 = 0
 * WDOG_LOCK_R		bit0 = 1, 配置锁 ( 复位初始状态是锁住的 )
 */
static void reg_default()
{
 	unsigned long addr = WDOG_LOAD_LOW_R;
	unsigned int val = 0xffff;
	unsigned int ret = 0;
	
    uart_printf("==WatchDog==BASE==register default:  \n");

	val = phx_read_u32(WDOG_CTRL_R);
	if(0 != val){
		uart_printf("ERROR: WDOG_CTRL_R default: %x  \n",val);
		ret = 0xffff;
		}

	val = 0xffff;		
	val = phx_read_u32(WDOG_STA_R);
	val = ( (WDOG_STA_RIS & val) || (WDOG_STA_CIS & val) );
	if(0 != val){
		uart_printf("ERROR: WDOG_STA_R default: %x  \n",val);
		ret = 0xffff;
		}

	val = 0;
	val = phx_read_u32(WDOG_LOCK_R);
	val = val & (WDOG_LOCK_RIS);
	if(0 == val){
		uart_printf("ERROR: WDOG_LOCK_R default: %x  \n",val);
		ret = 0xffff;		
		}

	if(0 == ret)
		uart_printf("register default: OK!  \n");
	
    return;
}

/*
 * 判断锁状态
 * bit0=1:锁住，0:解锁
 */
static unsigned int is_lock()
{

	/* RIS=1 表示锁住 */
	if(0 != ( phx_read_u32(WDOG_LOCK_R) & WDOG_LOCK_RIS ) ){
		return TRUE;
		}
	
	return FALSE;
}


/*
 * 配置锁
 * 写入非0xA5ACCEDE的任意值，锁住
 */
static void lock()
{
	phx_write_u32(WDOG_LOCK_R,0x55555555);

	return;
}

/*
 * 解锁
 * 写入 0xA5ACCEDE，解锁
 */
static unsigned int unlock()
{
	unsigned int ret = OK;
	
	phx_write_u32(WDOG_LOCK_R,0xA5ACCEDE);
	if(0 != ( phx_read_u32(WDOG_LOCK_R) & WDOG_LOCK_RIS ) ){
		ret = ERR;
		}

	return ret;
}

static void lock_test()
{

    uart_printf("==WatchDog==BASE==lock test:  \n");

	/* unlock test */
	if(OK != unlock() ){
		uart_printf("ERROR: unlock failure! \n");
		}else{
		phx_write_u32(WDOG_LOAD_LOW_R,0x5555);		
		phx_write_u32(WDOG_LOAD_HIGH_R,0xaaaa); 
		if(0x5555 != phx_read_u32(WDOG_LOAD_LOW_R)){
			uart_printf("ERROR: unlock failure! \n");
			return;
			}
		phx_write_u32(WDOG_LOAD_LOW_R,0x55aa);
		phx_write_u32(WDOG_LOAD_HIGH_R,0xaaaa);	
		if(0x55aa != phx_read_u32(WDOG_LOAD_LOW_R)){
			uart_printf("ERROR: unlock failure! \n");
			return;
			}
		
		uart_printf("unlock success! \n");
		}


	/* lock test *///锁的状态正确，而且写不进去，则锁功能测试成功
	lock();
	if(TRUE == is_lock()){
		phx_write_u32(WDOG_LOAD_LOW_R,0x5555);
		phx_write_u32(WDOG_LOAD_HIGH_R,0xaaaa);	
		if(0x5555 == phx_read_u32(WDOG_LOAD_LOW_R)){
			uart_printf("ERROR: lock failure! \n");
			}else{
			uart_printf("lock success! \n");
			}
		}
	else
		uart_printf("ERROR: lock failure! \n");
		
	
	return;
}

/*
 * 测试寄存器写操作
 *
 * 低32位计数器配置： WDOG_LOAD_LOW_R	可读可写，且不变
 */
static void reg_w()
{

    uart_printf("==WatchDog==BASE==register write:  \n");


    unsigned int tmp1,tmp2,val1,val2;
	val1 = 0x55555555;
	val2 = 0xaaaaaaaa;

	/* 先确保解锁 */
	if(TRUE == is_lock())
		unlock();

	phx_write_u32(WDOG_LOAD_HIGH_R,val2);
	phx_write_u32(WDOG_LOAD_LOW_R,val1);

	tmp1 = phx_read_u32(WDOG_LOAD_LOW_R);
	tmp2 = phx_read_u32(WDOG_LOAD_HIGH_R);
	if( (val1 != tmp1)  || (val2 != tmp2) ){
		uart_printf("ERROR: 0x%x  0x%x  ,Read: 0x%x  0x%x  \n",val1,val2,tmp1,tmp2);
		}

    uart_printf("register write OK!  \n");

	return;
	
}

/***********************************************************************
 *
 * 功能项
 *
 **********************************************************************/

/*
 * 计数器初值设置测试
 *
 * 对应的寄存器：Watchdog load register，值不会递减
 */
static void count_load()
{

    uart_printf("------count load------  \n");


    unsigned int tmp1,tmp2,val1,val2;
	val1 = 0x55555555;
	val2 = 0xaaaaaaaa;

	/* 先确保解锁 */
	if(TRUE == is_lock())
		unlock();

	phx_write_u32(WDOG_LOAD_HIGH_R,val2);
	phx_write_u32(WDOG_LOAD_LOW_R,val1);

	tmp2 = phx_read_u32(WDOG_LOAD_HIGH_R);
	tmp1 = phx_read_u32(WDOG_LOAD_LOW_R);
	if( (val1 != tmp1)  || (val2 != tmp2) ){
		uart_printf("ERROR: 0x%x  0x%x  ,Read: 0x%x  0x%x  \n",val1,val2,tmp1,tmp2);
		}
	else
		uart_printf("count load success!  \n");

	return;
	
}


/*
 * 获取并解析看门狗各状态
 */
//static
void wdog_stat()
{
    uart_printf("------wdog stat------  \n");

    uart_printf("Load  L: 0x%x      Load  H: 0x%x  \n",phx_read_u32(WDOG_LOAD_LOW_R),phx_read_u32(WDOG_LOAD_HIGH_R));
    uart_printf("Value L: 0x%x      Value H: 0x%x  \n",phx_read_u32(WDOG_VALUE_LOW_R),phx_read_u32(WDOG_VALUE_HIGH_R));	


	uint32_t val;
	val = phx_read_u32(WDOG_CTRL_R);
    uart_printf("CTRL: 0x%x  \n",val);	
    uart_printf("    INTEN:    %x  \n",val & WDOG_CTRL_INTEN);	
	uart_printf("    RSTEN_SS: %x  \n",val & WDOG_CTRL_RSTEN_SS);	
	uart_printf("    RSTEN_SH: %x  \n",val & WDOG_CTRL_RSTEN_SH);	

	val = phx_read_u32(WDOG_STA_R);
    uart_printf("INT STAT: 0x%x  \n",val);	
    uart_printf("    CIS:  %x  \n",val & WDOG_STA_CIS);	
	uart_printf("    RIS:  %x  \n",val & WDOG_STA_RIS);	

	val = phx_read_u32(WDOG_LOCK_R);
//    uart_printf("LOCK: %x  \n",(val & WDOG_LOCK_RIS));	
    uart_printf("LOCK: %x  \n",(val & WDOG_LOCK_RIS)?1:0);	

	return;
}

/*
 * 中断使能
 *
 * 使能中断，则当计数器减到0时，会触发中断
 *
 * 相关寄存器：
 * #define WDOG_CTRL_R           (WDOG_REGISTER_BASE + 0x8)
 * #define WDOG_CTRL_INTEN       (0x1 << 0)
 * 置高使能中断
 */
static void int_en()
{
	uint32_t val;

	val = phx_read_u32(WDOG_CTRL_R);
	val = val | WDOG_CTRL_INTEN;
	phx_write_u32(WDOG_CTRL_R,val);
		
	return;
}

/*
 * 中断屏蔽
 */
static void int_dis()
{
	uint32_t val;

	val = phx_read_u32(WDOG_CTRL_R);
	val = val & ~(WDOG_CTRL_INTEN);
	phx_write_u32(WDOG_CTRL_R,val);
		
	return;
}

/*
 * current 中断状态获取
 * 返回0：没有中断，返回大于0：产生了中断，且未清除
 */
static uint32_t int_cis_get()
{
	return ( phx_read_u32(WDOG_STA_R) & (WDOG_STA_CIS) );
}

/*
 * raw 中断状态获取
 * 判断：
 * 1：已经发出至少一次interrupt，为计数器数到0时发出。
 * 0：未发出interrupt。
 */
static uint32_t int_ris_get()
{
	return ( phx_read_u32(WDOG_STA_R) & (WDOG_STA_RIS) );
}

/*
 * 计数测试
 *
 * 1、获取计数器的当前值
 * 2、判断计数器的当前值是否不断递减
 */


static void test_cur_count(uint32_t val_l, uint32_t val_h)
{
    uart_printf("------count current------  \n");


    unsigned int cur11,cur12,cur21,cur22,val1,val2;
	val1 = val_l;//0xffffffff;
	val2 = val_h;//0xffffffff;

	/* 先确保解锁 */
	if(TRUE == is_lock())
		unlock();

	/* load */
	phx_write_u32(WDOG_LOAD_HIGH_R,val2);
	phx_write_u32(WDOG_LOAD_LOW_R,val1);

	/* 开中断 */
	int_en();

	/*tmp*/uart_printf("D-CTRL:%x  \n",phx_read_u32(WDOG_CTRL_R));

	/* current 1 */
	cur12 = phx_read_u32(WDOG_VALUE_HIGH_R);
	cur11 = phx_read_u32(WDOG_VALUE_LOW_R);

	//tmp
	uart_printf("LOAD:%x  %x  ,CUR1: %x  %x  \n",val1,val2,cur11,cur12);

	/* 如果现值不小于初值，报错 */
	if( \
		(cur12 > val2) || \
	    ((cur12 == val2) && (cur11 >= val1 )) \
	){
		uart_printf("ERROR: LOAD:%x  %x  ,CUR1: %x  %x  \n",val1,val2,cur11,cur12);
		return;
		}

	/* current 2 */
	cur22 = phx_read_u32(WDOG_VALUE_HIGH_R);
	cur21 = phx_read_u32(WDOG_VALUE_LOW_R);

	/* 如果现值不小于上一次的现值，报错 */
	if( \
		(cur22 > cur12 ) || \
	    ((cur22 == cur12) && (cur21 >= cur11 )) \
	){
		uart_printf("ERROR: CUR1:%x  %x  ,CUR2: %x  %x  \n",cur11,cur12,cur21,cur22);
		return;
		}

	uart_printf("count current success!  \n");

	return;
	
}

/*
 * 重新计数测试
 *
 * 1、多次获取计数器的当前值
 * 2、判断是否递减后重载
 */


static void recount()
{
    unsigned int cur_l,cur_h,val1,val2;
	val1 = 0x350;//LOW < 500，便于测试
	val2 = 0;//HIGH = 0

	/* 先确保解锁 */
	if(TRUE == is_lock())
		unlock();

	/* load */
	phx_write_u32(WDOG_LOAD_HIGH_R,val2);
	phx_write_u32(WDOG_LOAD_LOW_R,val1);
	uart_printf("LOAD:%x  %x  \n",val2,val1);

	/* 开中断 */
	int_en();

	/* 打印不同时间的现值，观察如果出现先递减后出现重新加载的情况，则判断为正确 */
	uart_printf("RULE: first decreasing, then increasing, OK;otherwise ERR :	\n");
	int i=0;
	for(i=0; i<100; i++){
		if(i%20==0){
			uart_printf("CUR%d: %x  %x  \n",i,\
				phx_read_u32(WDOG_VALUE_HIGH_R),phx_read_u32(WDOG_VALUE_LOW_R) );
			}
		}

	return;
	
}

u32 g_wdog_int_count = 0;

static void wdog_isr(void)
{
	//int dis
	int_dis();

	//清中断
	phx_write_u32(WDOG_INT_CLR_R,WDOG_INT_CLR_CLR);

//	phx_write_u64(DSP48_PIC_CLR_R,1<<(PIC_IRQ_WDOG));	//pic int clr
	
	g_wdog_int_count++;
	debug_print("ISR OK:%x \n",g_wdog_int_count);//for test ,del

}

static void intr_pic()
{

	uart_printf("INTR-PIC: irq=%d  \n",PIC_IRQ_WDOG);	
//	uart_printf("PIC_EN Addr:%x %x \n",H32(DSP48_PIC_EN_R),L32(DSP48_PIC_EN_R));

	g_wdog_int_count = 0;

 	/* 解锁 */
	unlock();

	/* 关中断 */
	int_dis();

	/* 设置计数器 */
	phx_write_u32(WDOG_LOAD_HIGH_R,0);
	phx_write_u32(WDOG_LOAD_LOW_R,0x500);

	//tmp
//	debug_print("PIC_EN1:%x \n",phx_read_u64(DSP48_PIC_EN_R));

	//pic
//	register_pic_irq(PIC_IRQ_WDOG, wdog_isr);
//	pic_init(PIC_IRQ_WDOG, 0);
	int_install_handler("wdog",PIC_IRQ_WDOG, 1,(void (*)(void *))wdog_isr,(void *)0);
	int_enable_pic(PIC_IRQ_WDOG);

	//tmp
//	debug_print("PIC_EN2:%x \n",L32(phx_read_u64(DSP48_PIC_EN_R)));
//	phx_write_u64(DSP48_PIC_EN_R,1UL<<PIC_IRQ_WDOG);
//	debug_print("PIC_EN3:%x \n",L32(phx_read_u64(DSP48_PIC_EN_R)));


	/* 开中断 */
	int_en();

	//phx_write_u32(WDOG_CTRL_R,0xf);//test:最低3位都写1试试

	/* 判断中断是否产生 */
	int i;
	int num=40000; //实际测试LOW=0x500时，i=80左右
//	for(i = 0; i < num; i++){  
	for(i = 0; ; i++){  
		if(g_wdog_int_count){
			uart_printf("i=%d \n",i);			
			break;
			}
		}

	if(g_wdog_int_count)
		uart_printf("INT PIC OK! \n");
	else
		uart_printf("INT PIC ERROR! \n");
	

	/* 锁住：可选 */

	return;

}

/* 
 * 中断功能测试--poll
 *
 * 测试步骤：
 * 验收条件：
 */
static void intr()
{

	uart_printf("INTR:  ");

 	/* 解锁 */
	unlock();

	/* 设置计数器 */
	phx_write_u32(WDOG_LOAD_HIGH_R,0);
	phx_write_u32(WDOG_LOAD_LOW_R,0x500);

	/* 开中断 */
	int_en();

	//phx_write_u32(WDOG_CTRL_R,0xf);//test:最低3位都写1试试

	/* 判断中断是否产生 */
	int i;
	int num=200; //实际测试LOW=0x500时，i=80左右
	for(i = 0; i < num; i++){  
		if(int_cis_get()>0){
			//清中断
			phx_write_u32(WDOG_INT_CLR_R,WDOG_INT_CLR_CLR);

			uart_printf("i=%d,intr occur OK!  \n",i);
			break;
			}
		}

	uart_printf("[%x] \n",phx_read_u32(WDOG_VALUE_LOW_R));//tmp
	
	/* 判断中断是否再次产生 */
	for(i = 0; i < num; i++){
		if(int_cis_get()>0){
			//关中断
			int_dis();
			//清中断
			phx_write_u32(WDOG_INT_CLR_R,WDOG_INT_CLR_CLR);

			uart_printf("i=%d,clr intr,occur again OK!  \n",i);
			break;
			}
		}

	/* 判断中断是否再次产生 */
	for(i = 0; i < num; i++){
		if(int_cis_get()>0){
			//关中断
			int_dis();
			//清中断
			phx_write_u32(WDOG_INT_CLR_R,WDOG_INT_CLR_CLR);

			uart_printf("i=%d,close int,but intr occur,ERROR!  \n",i);
			break;
			}
		}

	if(i>=num)
		uart_printf("after close int,No intr occur,OK!  \n");


	/* 锁住：可选 */

	return;

}

/* 
 * 测试中断开关对计数的影响
 */
static void int_tmp()
{

 	/* 解锁 */
	unlock();

	int_dis();

	/* 计数器先设大值 */
	phx_write_u32(WDOG_LOAD_HIGH_R,0);
	phx_write_u32(WDOG_LOAD_LOW_R,0xffffffff);

	//测试中断开关对计数的影响
	uart_printf("int_dis:L:%x V:%x \n",phx_read_u32(WDOG_LOAD_LOW_R),phx_read_u32(WDOG_VALUE_LOW_R));//tmp
	int_en();//tmp
	uart_printf("int_en: L:%x V:%x \n",phx_read_u32(WDOG_LOAD_LOW_R),phx_read_u32(WDOG_VALUE_LOW_R));//tmp

	return;
}

/* 
 * 特殊值测试：load 0
 *
 * 测试步骤：load 0，立即产生中断
 * 验收条件：
 */
static void load0()
{

 	/* 解锁 */
	unlock();

	/* 关中断、清中断 */
	int_dis();
	phx_write_u32(WDOG_INT_CLR_R,WDOG_INT_CLR_CLR);

	/* 特殊值0 设置 */
	phx_write_u32(WDOG_LOAD_LOW_R,0);	

	/* 开中断 */
	int_en();


	//检测是否立即产生中断
	if(int_cis_get()>0){
		//关中断
		int_dis();
		//清中断
		phx_write_u32(WDOG_INT_CLR_R,WDOG_INT_CLR_CLR);
	
		uart_printf("Load 0, intr occur immediately,OK!  \n");
		}else{
		uart_printf("Load 0, no intr occur immediately,ERROR!  \n");
		}
	
	return;

}

/* 
 * 复位功能测试
 *
 * 测试步骤：
 * 验收条件：需要硬件配合检测复位功能
 */
static void rst()
{

 	/* 解锁 */
	unlock();

	/* 复位拉低的时长变短一些，便于观察复位情况 */
#if 0 //SYS_RSTN_TIME_R undefined
	uart_printf("RSTN val old:%d  \n",phx_read_u32(SYS_RSTN_TIME_R));
	phx_write_u32(SYS_RSTN_TIME_R,500);//500cycle:500*50ns	
	uart_printf("RSTN val new:%d  \n",phx_read_u32(SYS_RSTN_TIME_R));	
#endif

	uart_printf("RST: need hw to detect... \n");


	/* 关中断、清中断 */
	int_dis();
	phx_write_u32(WDOG_INT_CLR_R,WDOG_INT_CLR_CLR);

	/* 设置计数器 */
	phx_write_u32(WDOG_LOAD_HIGH_R,0);
	phx_write_u32(WDOG_LOAD_LOW_R,0x500);

	/* 使能复位 */
	phx_write_u32(WDOG_CTRL_R, phx_read_u32(WDOG_CTRL_R) | WDOG_CTRL_RSTEN_SH);

	/* 开中断 */
	int_en();

	//后面需要通过硬件手段判断是否复位

}


/* 定义WDOG测试模块代码的版本号 */
#define WDOG_TEST_VER	3

int wdog_main()
{

	wdog_clk_en();

#if 1 //回归测试时放开
//打印比较多的前期验证
    uart_printf("====== Test WatchDog base begin: ver=%d ======  \n",(WDOG_TEST_VER));
	reg_default();
	reg_w();
	lock_test();
	count_load();
	wdog_stat();
	test_cur_count(0xffffffff,0xffffffff);/* 设最大初值 */
	test_cur_count(0x55555555,0x33333333);/* 设普通初值 */
//x	test_cur_count(0x355,0x0);/* 设极小值 *///太小，会导致重新load，可能出现cur2>cur1

			    uart_printf("------recount------  \n");//重新计数测试
	recount();

			    uart_printf("------intr------  \n");//中断测试
	intr();
#endif
				uart_printf("------intr - pic------  \n");//中断测试

	intr_pic();

#if 0 //回归测试时放开
				uart_printf("------load0------  \n");//加载特殊值0测试
	load0();

				uart_printf("------rst------  \n");//复位功能测试：需要通过硬件等外围手段去判断是否复位了
	rst();

	u32 i=0x10000;
	while(i)i--;

//	wdog_stat();

#endif

	return 0;
}

