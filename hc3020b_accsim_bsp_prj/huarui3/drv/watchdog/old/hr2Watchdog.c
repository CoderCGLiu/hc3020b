/*
 * hr2Watchdog.c
 *
 *  Created on: 2019-7-24
 *      Author: Administrator
 */
#include <vxBusLib.h>
#include <stdlib.h>
#include <hwif/vxbus/vxBus.h>
#include <hwif/vxbus/vxbPlbLib.h>
#include <hwif/vxbus/hwConf.h>

#include <vxWorks.h>
#include <stdio.h>
#include <taskLib.h>
#include <vmLib.h>
#include <cacheLib.h>
#include <intLib.h>
#include <stdio.h>
#include <string.h>
#include "tickLib.h" 
#include "semLib.h"
#include "sysLib.h" 
//#include "tool/hr3SysTools.h"
#include "hr2Watchdog.h"
#include <math.h>

#ifdef __REWORKS__
#include <irq.h>
#include <hr3_intc_defs.h>
#include "tool/hr3SysTools.h"
#endif

#include "hr3Watchdog.h"
#include "drvCommon.h"

extern TASK_ID taskCreateCore(char * name,int priority,int options,size_t stackSize,FUNCPTR entryPt,unsigned int core,_Vx_usr_arg_t arg0,_Vx_usr_arg_t arg1,_Vx_usr_arg_t arg2,_Vx_usr_arg_t arg3,_Vx_usr_arg_t arg4,_Vx_usr_arg_t arg5,_Vx_usr_arg_t arg6,_Vx_usr_arg_t arg7,_Vx_usr_arg_t arg8,_Vx_usr_arg_t arg9);


/*设备控制 data struct*/
typedef struct
{
	VXB_DEVICE_ID pDev;			/* vxbus 要求的项目*/
	unsigned int devUnit;
	unsigned int intSource;			/* intSource */
	unsigned int intLine;			/* intline of core*/ 
	struct hcfDevice * pHcf;
} HRWDOG;

void hrWdogInstInit ();
void hrWdogInstInit2();
void hrWdogInstConnect();
void hrWdogInt();
void hrWdogClearInt();
void hrWdogClkEnable();

SEM_ID semID_Wdog = 0;   /* the sem for wdog */

VOIDFUNCPTR hr_bsl_wdog_callback_func_ptr = NULL;
int hr_bsl_wdog_callback_func_param0 = 0;

/* wdogUsedSign用来标示wdog是否正在使用中
 * wdogUsedSign = 1，标示wdog正在使用
 */
static unsigned int wdogUsedSign = 0;
/* 记录看门狗中断数量 */
static unsigned int s_wdogIntNum = 0;
/* 工作模式：0-作为看门狗，1-作为定时器 */
static char m_wdogMode = 0;

/*vxBus 注册总线加载函数*/
LOCAL struct drvBusFuncs HRWdogFuncs =
{
    hrWdogInstInit ,     /* devInstanceInit  */
    hrWdogInstInit2,     /* devInstanceInit2 */
    hrWdogInstConnect    /* devConnect       */
};
/*vxBus 设备注册入口*/
LOCAL struct vxbPlbRegister hrWdogRegistration =
{
    {
        NULL,                 /* pNext,一般都用NULL */
        VXB_DEVID_DEVICE,     /* devID,固定,表示这是一个设备 */
        VXB_BUSID_PLB,        /* busID = PLB ,表示这个设备挂接在PLB上*/
        VXBUS_VERSION_4,      /* 版本号,6.7为版本4 */
        HRWDOG_VXBNAME,       	  /*设备名称,必须和hwconfig.c中统一*/
        &HRWdogFuncs ,       /* pDrvBusFuncs,设备总线函数组在后面定义 */
        NULL,     	          /* pMethods ,设备方法组,在后面定义*/
        NULL		          /* devProbe ,设备探测*/
    }
};

		
/**************************************总入口***********************************/
void hrWdogRegister(void)
{
	printf("\r\nhrWdogRegister\r\n");
	vxbDevRegister((struct vxbDevRegInfo *)&(hrWdogRegistration));
	return;
}

void hrWdogInstInit (struct vxbDev * pDev)
{
	printf("hrWdogInstInit\r\n");
	vxbNextUnitGet(pDev);
	return;
}
void hrWdogIntTask()
{
	unsigned int bTrue = 1;
	
	semID_Wdog = semBCreate(SEM_Q_PRIORITY, SEM_EMPTY);
	while (bTrue) 
	{
		semTake(semID_Wdog, WAIT_FOREVER);
		if (hr_bsl_wdog_callback_func_ptr != NULL)
			hr_bsl_wdog_callback_func_ptr(hr_bsl_wdog_callback_func_param0);
	}
}

void hrWdogInstInit2(struct vxbDev * pDev)
{	 
	printf("hrWdogInstInit2\r\n");
	HRWDOG *pDrvCtrl = NULL;
	struct hcfDevice * pHcf = NULL;
	
	pDrvCtrl = (HRWDOG*)malloc(sizeof(HRWDOG));
	if (pDrvCtrl == NULL) 
		return;

	bzero ((char *)pDrvCtrl, sizeof(HRWDOG));
	pDev->pDrvCtrl = pDrvCtrl;
	pDrvCtrl->pDev = pDev;
	
	pHcf = (struct hcfDevice *) hcfDeviceGet(pDev);
	pDrvCtrl->pHcf = pHcf;
	
	if(ERROR == devResourceGet(pHcf, "deviceId", HCF_RES_INT,(void *) &pDrvCtrl->devUnit))
	{
		printf("wdog get deviceId in hwconf.c error.\r\n");
		return ;
	} 
	if(pDrvCtrl->devUnit == 0)
#ifdef BOOTROM
		taskSpawn("tWdog",51,VX_FP_TASK,0x20000,(FUNCPTR)hrWdogIntTask,0,1,2,3,4,5,6,7,8,9);
#else
		taskCreateCore("tWdog",51,VX_FP_TASK,0x20000,(FUNCPTR)hrWdogIntTask,0,0,1,2,3,4,5,6,7,8,9);
#endif
	
	hrWdogClkEnable();
	
	return;
}

void hr3WdogInstInit2()
{

#ifdef BOOTROM
		taskSpawn("tWdog",51,VX_FP_TASK,0x20000,(FUNCPTR)hrWdogIntTask,0,1,2,3,4,5,6,7,8,9);
#else
		taskCreateCore("tWdog",51,VX_FP_TASK,0x20000,(FUNCPTR)hrWdogIntTask,0,0,1,2,3,4,5,6,7,8,9);
#endif

	hrWdogClkEnable();

	return;
}

void hrWdogInstConnect()
{  
	hrWdogClearInt();

//    int_install_handler(get_hr3_int_name(INT_WDOG), INT_WDOG, 1, hrWdogInt, NULL);
//    int_enable_pic(INT_WDOG);
	int_install_handler("wdog",INT_WDOG, 1,(void (*)(void *))hrWdogInt,(void *)0);
	int_enable_pic(INT_WDOG);
    
}


void hrWdogClearInt()
{
//	WDOG_REG_SET(WDOG_INT_CLEAR_REG,0x1);
	phx_write_u32(WDOG_INT_CLR_R,WDOG_INT_CLR_CLR);
}
void hrWdogClkEnable()
{
	unsigned int val = 0;
	
//	val = WDOG_REG_GET(SYS_CTRL_REG);
//	val = val | 0x1;
//	WDOG_REG_SET(SYS_CTRL_REG,val);

	phx_write_u32(SYS_LOCK_R,0xa5accede);//misc lock free
	phx_write_u32(SYS_CTRL_R, (phx_read_u32(SYS_CTRL_R)) | 1);//misc wdog_clk_en

	printk("wd en CTRL:0x%x\n",phx_read_u32(SYS_CTRL_R));

}
void hrWdogLockReg(int lock)
{
	unsigned int ret = OK;

	if(lock)
	{
//		WDOG_REG_SET(WDOG_LOCK_REG,0x0);
		phx_write_u32(WDOG_LOCK_R,0x55555555);
	}
	else
	{
//		WDOG_REG_SET(WDOG_LOCK_REG,0x1ACC5EE1);
		phx_write_u32(WDOG_LOCK_R,0xA5ACCEDE);
		if(0 != ( phx_read_u32(WDOG_LOCK_R) & WDOG_LOCK_RIS ) ){
			ret = ERROR;
			}
	}
}
void hrWdogInt()
{
	printk("%s:%d\r\n", __FUNCTION__, __LINE__);

    if( m_wdogMode == 0 )
        return;

	s_wdogIntNum = s_wdogIntNum + 1;
	
//	if(WDOG_REG_GET(WDOG_INT_STATE_REG) & 0x2)
	{
		hrWdogClearInt(0);
		semGive(semID_Wdog);
		//printstr("wdog int.\r\n");
	}
}

void hrWdogInit(VOIDFUNCPTR routine,int param)
{
	hr_bsl_wdog_callback_func_ptr = routine;
	hr_bsl_wdog_callback_func_param0 = param;
}

static unsigned int is_lock()
{

	/* RIS=1 表示锁住 */
	if(0 != ( phx_read_u32(WDOG_LOCK_R) & WDOG_LOCK_RIS ) ){
		return 1;
		}

	return 0;
}


/*
 * 配置锁
 * 写入非0xA5ACCEDE的任意值，锁住
 */
static unsigned int unlock()
{
	unsigned int ret = OK;

	phx_write_u32(WDOG_LOCK_R,0xA5ACCEDE);
	if(0 != ( phx_read_u32(WDOG_LOCK_R) & WDOG_LOCK_RIS ) ){
		ret = -1;
		}

	return ret;
}

void bslWdogSetValue(unsigned long long us)
{
	unsigned int tmpHighVal = 0;
	unsigned int tmpLowVal = 0;
	   unsigned int tmp1,tmp2;
	
	tmpLowVal = (us * 33) & 0xFFFFFFFF;
	tmpHighVal = ((us * 33) >> 32) & 0xFFFFFFFF;

	if(TRUE == is_lock())
		unlock();

//	WDOG_REG_SET(WDOG_LOAD_REG_HIGH, tmpHighVal);
//	WDOG_REG_SET(WDOG_LOAD_REG_LOW, tmpLowVal);
	phx_write_u32(WDOG_LOAD_HIGH_R,tmpHighVal);
	phx_write_u32(WDOG_LOAD_LOW_R,tmpLowVal);

	tmp1 = phx_read_u32(WDOG_LOAD_LOW_R);
	tmp2 = phx_read_u32(WDOG_LOAD_HIGH_R);
	if( (tmpHighVal != tmp1)  || (tmpLowVal != tmp2) ){
		printk("ERROR: 0x%x  0x%x  ,Read: 0x%x  0x%x  \n",tmpHighVal,tmpLowVal,tmp1,tmp2);
		}

	printk("register write OK!  \n");

}

unsigned long long hrWdogGetValue()
{
	unsigned long long tmp = 0;
//
//	tmp = WDOG_REG_GET(WDOG_VAL_REG_HIGH);
//	tmp = (tmp << 32) | WDOG_REG_GET(WDOG_VAL_REG_LOW);

	tmp =  phx_read_u32(WDOG_VALUE_HIGH_R);
	tmp = (tmp << 32) | phx_read_u32(WDOG_VALUE_LOW_R);

	return tmp;
}

int bslWdogStart(char mode)
{
	unsigned int val = 0;
	
	if((wdogUsedSign & 0x1) == 0x1)
	{
		printf("[DSP%d]wdog0正在使用.\n",sysCpuGetID());
		return ERROR;
	}
	else
	{
		wdogUsedSign = 1;
	}
		
	m_wdogMode = mode;

//	val = WDOG_REG_GET(WDOG_CTRL_REG);
//	val = val | 0x5;
//	WDOG_REG_SET(WDOG_CTRL_REG,val);

	val = phx_read_u32(WDOG_CTRL_R);
	val = val | WDOG_CTRL_INTEN;
	phx_write_u32(WDOG_CTRL_R,val);

//	phx_write_u32(SYS_LOCK_R,0xa5accede);//misc lock free
//	phx_write_u32(SYS_CTRL_R, (phx_read_u32(SYS_CTRL_R)) | 1);//misc wdog_clk_en


	return OK;	
}
void hrWdogStop()
{
	unsigned int val = 0;

	wdogUsedSign = 0;
		
//	val = WDOG_REG_GET(WDOG_CTRL_REG);
//	val = val & 0xFFFFFFFE;
//	WDOG_REG_SET(WDOG_CTRL_REG,val);


	val = phx_read_u32(WDOG_CTRL_R);
	val = val & ~(WDOG_CTRL_INTEN);
	phx_write_u32(WDOG_CTRL_R,val);
}
unsigned int hrWdogGetIntNum()
{
	return s_wdogIntNum;
}

void wdog_show()
{
    printk("------wdog stat------  \n");

    printk("Load  L: 0x%x      Load  H: 0x%x  \n",phx_read_u32(WDOG_LOAD_LOW_R),phx_read_u32(WDOG_LOAD_HIGH_R));
    printk("Value L: 0x%x      Value H: 0x%x  \n",phx_read_u32(WDOG_VALUE_LOW_R),phx_read_u32(WDOG_VALUE_HIGH_R));


	uint32_t val;
	val = phx_read_u32(WDOG_CTRL_R);
    printk("CTRL: 0x%x  \n",val);
    printk("    INTEN:    %x  \n",val & WDOG_CTRL_INTEN);
	printk("    RSTEN_SS: %x  \n",val & WDOG_CTRL_RSTEN_SS);
	printk("    RSTEN_SH: %x  \n",val & WDOG_CTRL_RSTEN_SH);

	val = phx_read_u32(WDOG_STA_R);
    printk("INT STAT: 0x%x  \n",val);
    printk("    CIS:  %x  \n",val & WDOG_STA_CIS);
	printk("    RIS:  %x  \n",val & WDOG_STA_RIS);

	val = phx_read_u32(WDOG_LOCK_R);
//    printk("LOCK: %x  \n",(val & WDOG_LOCK_RIS));
    printk("LOCK: %x  \n",(val & WDOG_LOCK_RIS)?1:0);

	return;
}

void wdog_cb()
{
	printk("wdog cb\n");
}
void wdog_test()
{
	hrWdogInit(wdog_cb,0);
	hr3WdogInstInit2();
	hrWdogInstConnect();

	bslWdogSetValue(10);

	wdog_show();

	bslWdogStart(1);
	sleep(5);

	hrWdogStop();
}
