/*
 * hr2Watchdog.h
 *
 *  Created on: 2019-7-24
 *      Author: Administrator
 */

#ifndef HR2WATCHDOG_H_
#define HR2WATCHDOG_H_

#define WDOG_FREQ 33000000

#define SYS_CTRL_REG 			0xffffffffbf088010

#define WDOG_LOAD_REG_LOW 		0xffffffffbf088400
#define WDOG_LOAD_REG_HIGH 		0xffffffffbf088404
#define WDOG_CTRL_REG  			0xffffffffbf088408
#define WDOG_INT_STATE_REG  	0xffffffffbf08840C
#define WDOG_INT_CLEAR_REG  	0xffffffffbf088410
#define WDOG_LOCK_REG  			0xffffffffbf088414
#define WDOG_VAL_REG_LOW  		0xffffffffbf088418
#define WDOG_VAL_REG_HIGH  		0xffffffffbf08841c

#define WDOG_REG_GET(addr)          \
		*(volatile unsigned int *)(addr)
#define WDOG_REG_SET(addr, data)    \
		*(volatile unsigned int *)(addr) = data

#define HRWDOG_VXBNAME "hrWdog"


#endif /* HR2WATCHDOG_H_ */
