#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <vxworks.h>
#include <taskLib.h>
#include <semLib.h>
#include <irq.h>
#include "hr3_intc_defs.h"
#include "hr3Watchdog.h"
#include "../../h/tool/hr3SysTools.h"
#include "../../h/tool/hr3Macro.h"

/*
 * 外部函数声明
 * */
extern TASK_ID taskCreateCore(char * name,int priority,int options,size_t stackSize,FUNCPTR entryPt,unsigned int core,_Vx_usr_arg_t arg0,_Vx_usr_arg_t arg1,_Vx_usr_arg_t arg2,_Vx_usr_arg_t arg3,_Vx_usr_arg_t arg4,_Vx_usr_arg_t arg5,_Vx_usr_arg_t arg6,_Vx_usr_arg_t arg7,_Vx_usr_arg_t arg8,_Vx_usr_arg_t arg9);


/*设备控制 data struct*/
typedef struct
{
	//VXB_DEVICE_ID pDev;			/* vxbus 要求的项目*/
	unsigned int devUnit;
	unsigned int intSource;			/* intSource */
	unsigned int intLine;			/* intline of core*/
	//struct hcfDevice * pHcf;
} HRWDOG;

/*
 * 本地变量声明
 * */
SEM_ID semID_Wdog = NULL;   /* the sem for wdog */
VOIDFUNCPTR_WDOG hr_bsl_wdog_callback_func_ptr = NULL;
int hr_bsl_wdog_callback_func_param0 = 0;
static int wdog_init_flag = 0;

/* wdogUsedSign用来标示wdog是否正在使用中
 * wdogUsedSign = 1，标示wdog正在使用
 */
static unsigned int wdogUsedSign = 0;
/* 记录看门狗中断数量 */
static unsigned int s_wdogIntNum = 0;
/* 工作模式：0-作为看门狗，1-作为定时器 */
static char m_wdogMode = 0;


static void hrWdogIntTask()
{
	unsigned int bTrue = 1;

	semID_Wdog = semBCreate(SEM_Q_PRIORITY, SEM_EMPTY);
	while (bTrue)
	{
		semTake(semID_Wdog, WAIT_FOREVER);
		if (hr_bsl_wdog_callback_func_ptr != NULL)
			hr_bsl_wdog_callback_func_ptr(hr_bsl_wdog_callback_func_param0);
		else
			printf("<**DEBUG**> [%s():_%d_]:: --- hrWdogIntTask ---\n", __FUNCTION__, __LINE__);
	}
}

/*
 * 看门狗时钟使能
 */
static void hrWdogClkEnable()
{
	unsigned int val = 0;
	WDOG_REG_SET(SYS_LOCK_REG,0xa5accede);//misc lock free
	val = WDOG_REG_GET(SYS_CTRL_REG);
	val = val | 0x1;
	WDOG_REG_SET(SYS_CTRL_REG,val);	//misc wdog_clk_en
    //tmp
    printf("CTRL:%x  ,WD CLK EN!\n",WDOG_REG_GET(SYS_CTRL_REG));
}

static void hrWdogInstInit()
{
	printf("hrWdogInstInit\r\n");
	HRWDOG *pDrvCtrl = NULL;
	
	pDrvCtrl = (HRWDOG*)malloc(sizeof(HRWDOG));
	if (pDrvCtrl == NULL) 
		return;

	bzero ((char *)pDrvCtrl, sizeof(HRWDOG));

	if(pDrvCtrl->devUnit == 0)
#ifdef BOOTROM
		taskSpawn("tWdog",51,VX_FP_TASK,0x20000,(FUNCPTR)hrWdogIntTask,0,1,2,3,4,5,6,7,8,9);
#else
		taskCreateCore("tWdog",120,VX_FP_TASK,0x10000,(FUNCPTR)hrWdogIntTask,0,0,1,2,3,4,5,6,7,8,9);//core 0
#endif
	
	hrWdogClkEnable();
	
	return;
}

static void hrWdogInt()
{
    if( m_wdogMode == 0 )
        return;

	s_wdogIntNum++;
	
	if(WDOG_REG_GET(WDOG_INT_STATE_REG) & 0x2)
	{
		hrWdogClearInt();
		semGive(semID_Wdog);
		printf("wdog int.\r\n");
	}
}
static void hrWdogInstConnect()
{  
	hrWdogClearInt();
#ifndef _HC3020B_
	int_install_handler("hrWdog_isr", INT_WDOG, 1, (INT_HANDLER)hrWdogInt, NULL);
	int_enable_pic(INT_WDOG);
#else
	int_install_handler("hrWdog_isr", INT_WDOG0, 1, (INT_HANDLER)hrWdogInt, NULL);
	int_enable_pic(INT_WDOG0);
#endif
}

void hrWdogClearInt()
{
	hrWdogLockCfg(0);
	WDOG_REG_SET(WDOG_INT_CLEAR_REG,WDOG_INT_CLRAR_CLR);
	hrWdogLockCfg(1);
}



/*
 * 中断使能
 *
 * 使能中断，则当计数器减到0时，会触发中断
 *
 * 相关寄存器：
 * #define WDOG_CTRL_REG         (WDOG_REGISTER_BASE + 0x8)
 * #define WDOG_CTRL_INTEN       (0x1 << 0)
 * 置高使能中断
 */
static void int_en()
{
    uint32_t val;

    val = WDOG_REG_GET(WDOG_CTRL_REG);
    val = val | WDOG_CTRL_INTEN;
    WDOG_REG_SET(WDOG_CTRL_REG,val);

    return;
}

/*
 * 中断屏蔽
 */
static void int_dis()
{
    uint32_t val;

    val = WDOG_REG_GET(WDOG_CTRL_REG);
    val = val & ~(WDOG_CTRL_INTEN);
    WDOG_REG_SET(WDOG_CTRL_REG,val);

    return;
}



void hrWdogInit(VOIDFUNCPTR_WDOG routine,int param)
{
	hr_bsl_wdog_callback_func_ptr = routine;
	hr_bsl_wdog_callback_func_param0 = param;
}

void hrWdogSetValue(unsigned long long us)
{
	unsigned int tmpHighVal = 0;
	unsigned int tmpLowVal = 0;
	
	tmpLowVal = (us * 33) & 0xFFFFFFFF;
	tmpHighVal = ((us * 33) >> 32) & 0xFFFFFFFF;

	hrWdogLockCfg(0);
	
	WDOG_REG_SET(WDOG_LOAD_REG_HIGH, tmpHighVal);
	WDOG_REG_SET(WDOG_LOAD_REG_LOW, tmpLowVal);
	
	hrWdogLockCfg(1);

	printf("<**DEBUG**> [%s():_%d_]:: val = 0x%lx\n", __FUNCTION__, __LINE__, (us * 33));
}

unsigned long long hrWdogGetValue()
{
	unsigned long long tmp = 0;

	tmp = WDOG_REG_GET(WDOG_VAL_REG_HIGH);
	tmp = (tmp << 32) | WDOG_REG_GET(WDOG_VAL_REG_LOW);	

	return tmp;
}

int hrWdogLockCfg(int is_lock)
{
	int tmp = 0;
	
	tmp = is_lock ? 0x1 : 0xA5ACCEDE;
	
	WDOG_REG_SET(WDOG_LOCK_REG, tmp);
	tmp = WDOG_REG_GET(WDOG_LOCK_REG);
	return (tmp&0x1);
}

int hrWdogStart(void)
{
	unsigned int val = 0;
	
	if((wdogUsedSign & 0x1) == 0x1)
	{
		//printf("[DSP%d]wdog0正在使用.\n",sysCpuGetID());
	    printf("wdog0正在使用.\n");
		return ERROR;
	}
	else
	{
		wdogUsedSign = 1;
	}

	hrWdogLockCfg(0);
	
	val = WDOG_REG_GET(WDOG_CTRL_REG);
	val |= WDOG_CTRL_INTEN | WDOG_CTRL_RSTEN_SH;
	WDOG_REG_SET(WDOG_CTRL_REG,val);
	
	hrWdogLockCfg(1);
	
	printf("WDOG START\n");

	return OK;	
}
void hrWdogStop()
{
	unsigned int val = 0;

	wdogUsedSign = 0;
		
	hrWdogLockCfg(0);
	
	val = WDOG_REG_GET(WDOG_CTRL_REG);
	val = val & 0xFFFFFFFE;
	WDOG_REG_SET(WDOG_CTRL_REG,val);
	
	hrWdogLockCfg(1);
}
unsigned int hrWdogGetIntNum()
{
	return s_wdogIntNum;
}

void hr3_wdog_module_init(void)
{
	if(wdog_init_flag)
		return;
    hrWdogInstInit();
    hrWdogInstConnect();
    wdog_init_flag = 1;
}


/*
 * 复位功能测试
 *
 * 测试步骤：
 * 验收条件：需要硬件配合检测复位功能
 */
void rst(void)
{
#if 1
    /* 复位拉低的时长变短一些，便于观察复位情况 */
    printf("RSTN val old:%d  \n",WDOG_REG_GET(SYS_RSTN_TIME_REG));
    WDOG_REG_SET(SYS_RSTN_TIME_REG,500);//500cycle:500*50ns
    printf("RSTN val new:%d  \n",WDOG_REG_GET(SYS_RSTN_TIME_REG));
    
    printf("begin to RST... \n");
    
    /* 关中断、清中断 */
    int_dis();
    hrWdogClearInt();
    
    /*设置计数值*/
    hrWdogSetValue(1000000);
    
    /*开始计时*/
    hrWdogStart();
    
    /* 开中断 */
    int_en();
    
    //后面需要通过硬件手段判断是否复位
    printf("1111111111111111\n");
    
#else
    /* 解锁 */
    WDOG_REG_SET(WDOG_LOCK_REG,0xA5ACCEDE);
    if(0 != ( WDOG_REG_GET(WDOG_LOCK_REG) & WDOG_LOCK_RIS ) )
    {
        printf("lock error... \n");
    }

    /* 复位拉低的时长变短一些，便于观察复位情况 */
    printf("RSTN val old:%d  \n",WDOG_REG_GET(SYS_RSTN_TIME_REG));
    WDOG_REG_SET(SYS_RSTN_TIME_REG,500);//500cycle:500*50ns
    printf("RSTN val new:%d  \n",WDOG_REG_GET(SYS_RSTN_TIME_REG));

    printf("begin to RST... \n");


    /* 关中断、清中断 */
    int_dis();
    WDOG_REG_SET(WDOG_INT_CLEAR_REG,WDOG_INT_CLRAR_CLR);

    /* 设置计数器 */
    WDOG_REG_SET(WDOG_LOAD_REG_HIGH,0);//0/* wait finishing print */
    WDOG_REG_SET(WDOG_LOAD_REG_LOW,5*0x1312d00);//20M * 5s//0x500

    /* 使能复位 */
    WDOG_REG_SET(WDOG_CTRL_REG, WDOG_REG_GET(WDOG_CTRL_REG) | WDOG_CTRL_RSTEN_SH);

    /* 开中断 */
    int_en();

    //后面需要通过硬件手段判断是否复位
    printf("1111111111111111\n");
#endif
}


static int test_wdog_count = 0;
static unsigned int wdogValue = 60000000;//us
static void test_wdog_callback_func(int param)
{
	printk("<**DEBUG**> [%s():_%d_]:: test_wdog_count = %d\n", __FUNCTION__, __LINE__,test_wdog_count);
	test_wdog_count++;
	
	hrWdogSetValue(wdogValue);
}

void test_wdog(void)
{
    unsigned int tick = 0;
    unsigned int a = 0;
    unsigned long long tmp = 0;

    hr3_wdog_module_init();
    hrWdogInit(test_wdog_callback_func, test_wdog_count);

    printf("wdog功能和性能测试\n");
    hrWdogSetValue(wdogValue);
    printf("设置TimerValue = %lluus\n",wdogValue);
    tick = tickGet();
    printf("TIME0 =%d\n",tick);
//    while(tickGet() - tick <= sysClkRateGet()*5);
    hrWdogStart();
    printf("TESTING...\n");
    sleep(30);
    hrWdogStop();
    tmp = hrWdogGetValue();
    printf("tmp: %lld\n",tmp);
    a = abs(tmp - 33000000 * 30)*100/(33000000 * 30);
    printf("wdog计时误差:%d %%\n",a);
}
