#include "Timer.h"

int test_timer_callback(u64 param)
{
//	static int i = 0;
//	
//	if(i)
//	{
		printk("<INFO> [%s:__%d__]::  --- Timer%d ---\n",__FUNCTION__,__LINE__,param);
//		i = 0;
//	}
//	else
//		i = 1;
	return 0;
}
/*
 * 计数器重配测试
 *
 * 验收条件：设置一个小值，能检测到计数器递减；
 *           然后重新设置一个大值，判断计数器是否重新从大值开始计数
 */
void test_timer_reload(int n)
{
	u32 mode = TIMER_MODE_CYCLE;
	
	hrTimerModuleInit(n);
	hrTimerInstConnect(n, (FUNCPTR)test_timer_callback, n);
	hrTimerSetValue(n,0x12345678);
	hrTimerSetMode(n, mode);
	hrTimerStart(n);
	hrTimerSetValue(n,0x1234567800000000);

	if((hrTimerGetValue(n)) > 0x12345678) {
		printk("Timer%d reload test OK!\n\r",n);
	}else{
		printk("Timer%d reload test ERROR!\n\r",n);
	}
	return;

}

void test_timer_init()
{
	return;
}
