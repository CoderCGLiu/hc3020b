#include "Timer.h"
#include <irq.h>
#include <pthread.h>
#include <cpuset.h>
#include <taskLib.h>
#include <semLib.h>

static int flag_timer_init[TIMER_COUNT] = {0,0};

/* timerUsedSign用来标示timer是否正在使用中*/
static unsigned int timerUsedSign[TIMER_COUNT] = {0,0};
/* 用来记录Timer的倒计时中断数目 */
static unsigned int timerLoopCount[TIMER_COUNT] = {0,0};
static unsigned int s_timerLoopCount[TIMER_COUNT] = {0,0};
/* 记录Timer的中断数量 */
static unsigned int s_timerIntNum[TIMER_COUNT] = {0,0};

static SEM_ID semID_Timer[TIMER_COUNT] = {NULL, NULL};


static TIMER_INT_FUN hr_bsl_timer_callback_func_ptr[TIMER_COUNT] = {NULL, NULL};
static u64 hr_bsl_timer_callback_func_param[TIMER_COUNT] = {0,0};

static char* timer_irq_name[TIMER_COUNT] = {"timer0", "timer1"};
static u32 timer_irq_num[TIMER_COUNT] = {INT_TIMER0, INT_TIMER1};


static void inner_hrTimerSetValue_H(u32 index, u32 value)
{
	TIMER_REG_STRUCT *timer_reg_struct = (TIMER_REG_STRUCT*)TIMER_BASE(index);
	timer_reg_struct->timer_load_r.bits.Timer_load_high_r = (u32)value;
}

static void inner_hrTimerSetValue_L(u32 index, u32 value)
{
	TIMER_REG_STRUCT *timer_reg_struct = (TIMER_REG_STRUCT*)TIMER_BASE(index);
	timer_reg_struct->timer_load_r.bits.Timer_load_low_r = (u32)value;
}
void hrTimerSetValue(u32 index, u64 value)
{
	inner_hrTimerSetValue_H(index, (u32)(value>>32));
	inner_hrTimerSetValue_L(index, (u32)(value&0xffffffff));
}

u64 hrTimerGetValue(u32 index)
{
	TIMER_REG_STRUCT *timer_reg_struct = (TIMER_REG_STRUCT*)TIMER_BASE(index);
	u64 value = timer_reg_struct->timer_value_r.v;
    printk("timer_value_r = 0x%016x\r\n", value);
    return value;
}

void hrTimerSetMode(u32 index, u32 mode)
{
	TIMER_REG_STRUCT *timer_reg_struct = (TIMER_REG_STRUCT*)TIMER_BASE(index);
	// 开始循环计数，从load寄存器加载初值
	timer_reg_struct->timer_ctrl_r.v &= ~(u32)(0x6);
	
	if(mode == TIMER_MODE_ONCE)
		// 设置一次计数模式，直到load寄存器被设置新值
		timer_reg_struct->timer_ctrl_r.v |= (u32)(0x2);
}

u32 hrTimerGetMode(u32 index)
{
	TIMER_REG_STRUCT *timer_reg_struct = (TIMER_REG_STRUCT*)TIMER_BASE(index);
	u32 mode = (timer_reg_struct->timer_ctrl_r.v & (u32)0x6) >> 1;
	
	return mode;
}

void hrTimerStart(u32 index)
{
	TIMER_REG_STRUCT *timer_reg_struct = (TIMER_REG_STRUCT*)TIMER_BASE(index);
	
	timer_reg_struct->timer_ctrl_r.v |= (u32)0x1;
	
	timerUsedSign[index] = 1; 
}

void hrTimerStop(u32 index)
{
	TIMER_REG_STRUCT *timer_reg_struct = (TIMER_REG_STRUCT*)TIMER_BASE(index);
	
	timer_reg_struct->timer_ctrl_r.v &= ~(u32)(0x1);
	
	timerUsedSign[index] = 0; 
}

static void hrTimerIntTask(u32 index)
{
	unsigned int bTrue = 1;
	
	
	while (bTrue) 
	{
		// 获取信号量，永远等待
		semTake(semID_Timer[index], WAIT_FOREVER);
		if (hr_bsl_timer_callback_func_ptr[index] != NULL)
		{
			hr_bsl_timer_callback_func_ptr[index](hr_bsl_timer_callback_func_param[index]);
		}
	}
}

static int hrTimerSemInit(u32 index)
{
	pthread_t new_th;
	char task_name[30];
	
	sprintf(task_name, "tTimer%d", index);
	
	if (pthread_create2(&new_th, task_name, 120, RE_FP_TASK|RE_UNBREAKABLE, 0x10000,
			 (void* (*)(void *)) hrTimerIntTask, index) != 0) 
	{
		printk("Error creating thread, index = %u\n",index);
		return -1;
	}
	
	// 创建二值信号量，按优先级，初值0
	semID_Timer[index] = semBCreate(SEM_Q_PRIORITY, SEM_EMPTY);
	return 0;
}

static void hrTimerClearInt(u32 index)
{
	TIMER_REG_STRUCT *timer_reg_struct = (TIMER_REG_STRUCT*)TIMER_BASE(index);
	// 清除中断
	timer_reg_struct->timer_int_clr_r.v = 0x1;
}

static void hrTimerIsrHandle(u32 index)
{
	s_timerLoopCount[index]++;

	hrTimerClearInt(index);
	s_timerIntNum[index]++;

	semGive(semID_Timer[index]);
//	printk("<DEBUG> [%s:__%d__]:: Timer%d hrTimerIsrHandle\n",__FUNCTION__,__LINE__,index);

	if(timerLoopCount[index] > 0)
	{
		if(s_timerLoopCount[index] >= timerLoopCount[index])
		{
			hrTimerStop(index);
			s_timerLoopCount[index] = 0;
			timerLoopCount[index] = 0;
		}
	}
}

void DumpTimerIntNum()
{
	printk("<INFO> [%s:__%d__]:: Timer0  s_timerIntNum: %d\n",__FUNCTION__,__LINE__,s_timerIntNum[0]);
	printk("<INFO> [%s:__%d__]:: Timer1  s_timerIntNum: %d\n",__FUNCTION__,__LINE__,s_timerIntNum[1]);
	return;
}

void hrTimerInstConnect(u32 index, FUNCPTR routine, u64 param)
{
	hr_bsl_timer_callback_func_ptr[index] = routine;
	hr_bsl_timer_callback_func_param[index] = param;
}

static void hrTimerInstInit(u32 index)
{
	// 清除已产生的中断
	hrTimerClearInt(index);
	// 挂载中断
	int_install_handler(timer_irq_name[index], timer_irq_num[index], 1, hrTimerIsrHandle, index);
	int_enable_pic(timer_irq_num[index]);

	return;
}

void  hrTimerModuleInit (u32 index)
{
	if(flag_timer_init[index] == 0)
	{
		// 设置load寄存器值
		hrTimerSetValue(index, 0);
		// 设置定时器模式（一次模式）
		hrTimerSetMode(index, TIMER_MODE_ONCE);
		if(hrTimerSemInit(index) < 0)
		{
			printk("<ERROR> [%s:__%d__]:: Timer%d hrTimerSemInit error.\n",index);
			return;
		}
		// 定时器中断初始化
		hrTimerInstInit(index);
		flag_timer_init[index] = 1;
	}
}
