#ifndef _R4KTIMER_H_
#define _R4KTIMER_H_

#include <reworks/types.h>


/*TIMER REG==========================================================*/
#define TIMER_COUNT			2
#define TIMER_BASE(index)	(0x9000000000000000 | (u64)(0x1f0c0500 + 0x100 * (index)))
#define TIMER_MODE_ONCE		1
#define TIMER_MODE_CYCLE	0

typedef union _timer_load_r
{
	volatile unsigned long long v;
	struct{
		volatile unsigned int Timer_load_low_r; //低32bit 计数器配置值
		volatile unsigned int Timer_load_high_r; //高32bit 计数器配置值
	} bits;
} TIMER_LOAD_R;/*在中断被配置为使能且计数器已开始的过程，若软件配置 load寄存器则立即重新配置的数值开始重新计数。*/

typedef union _timer_ctrl_r
{
	volatile unsigned int v;
	struct{
		volatile unsigned int Inten                 :1; /* 1-开始计数,当达到0时发出interrupt.   
															0-停止计数,不会产生中断.*/
		volatile unsigned int Timer_mode            :2; /* 00: 周期的,counter会循环计数.
															01: 一次的,counter数到零后停止计数,直到load寄存器重新被配置*/
		volatile unsigned int Reserved              :29;
	} bits;
} TIMER_CTRL_R;

typedef union _timer_sta_r
{
	volatile unsigned int v;
	struct{
		volatile unsigned int RIS                 	:1; /* Raw interrupt status
															1-至少产生了一次中断，为计数器到0时发出.   
															0-未产生中断.*/
		volatile unsigned int CIS            		:1; /* current interrupt status
															1-正在产生中断，且未被清除.   
															0-未产生中断，或者已被清除.*/
		volatile unsigned int Reserved              :30;
	} bits;
} TIMER_STA_R;

typedef union _timer_int_clr_r
{
	volatile unsigned int v;
	struct{
		volatile unsigned int Clr;/*任意值清除已产生的中断*/
	} bits;
} TIMER_INR_CLR_R;

typedef union _timer_value_r
{
	volatile unsigned long long v;
	struct{
		volatile unsigned int Timer_value_low_r; //低32bit 计数当前值
		volatile unsigned int Timer_value_high_r; //高32bit 计数当前值
	} bits;
} TIMER_VALUE_R;

typedef struct _timer_reg_struct
{
	TIMER_LOAD_R		timer_load_r;
	TIMER_CTRL_R		timer_ctrl_r;
	TIMER_STA_R			timer_sta_r;
	TIMER_INR_CLR_R		timer_int_clr_r;
	TIMER_VALUE_R		timer_value_r;
} __attribute__((packed)) TIMER_REG_STRUCT;

/*TIMER REG==========================================================*/



#if 0// ============================================================read write Transfer
#define _P2V(addr)		(0x9000000000000000 | ((u64)(addr)))	/* physical address to virtual address */
#define _P2V_CACHE(addr)	(0xb000000000000000 | ((u64)(addr)))	/* physical address to virtual address */

#define READ8_REGISTER(x, y) do {			\
	y= *((volatile u8 *)(_P2V(x))) ;	\
} while(0)
#define READ16_REGISTER(x, y) do {			\
	y= *((volatile u16 *)(_P2V(x))) ;	\
} while(0)
#define READ32_REGISTER(x, y) do {			\
	y = *((volatile u32*)(_P2V(x)));	\
} while(0)
#define READ64_REGISTER(x, y) do {			\
	y = *((volatile u64 *)(_P2V(x)));	\
} while(0)
#define WRITE8_REGISTER(x, y) do {			\
	*((volatile u8 *)(_P2V(x)))= (u8)y ;	\
} while(0)
#define WRITE16_REGISTER(x, y) do {			\
	*((volatile u16 *)(_P2V(x)))= (u16)y ;	\
} while(0)
#define WRITE32_REGISTER(x, y) do {			\
	*((volatile u32 *)(_P2V(x))) = y;	\
} while(0)
#define WRITE64_REGISTER(x, y) do {			\
	*((volatile u64 *)(_P2V(x))) = y;	\
} while(0)

#define READ8_CACHE_REGISTER(x, y) do {			\
	y= *((volatile u8 *)(_P2V_CACHE(x))) ;	\
} while(0)
#define READ16_CACHE_REGISTER(x, y) do {			\
	y= *((volatile u16 *)(_P2V_CACHE(x))) ;	\
} while(0)
#define READ32_CACHE_REGISTER(x, y) do {			\
	y = *((volatile u32*)(_P2V_CACHE(x)));	\
} while(0)
#define READ64_CACHE_REGISTER(x, y) do {			\
	y = *((volatile u64 *)(_P2V_CACHE(x)));	\
} while(0)
#define WRITE8_CACHE_REGISTER(x, y) do {			\
	*((volatile u8 *)(_P2V_CACHE(x)))= (u8)y ;	\
} while(0)
#define WRITE16_CACHE_REGISTER(x, y) do {			\
	*((volatile u16 *)(_P2V_CACHE(x)))= (u16)y ;	\
} while(0)
#define WRITE32_CACHE_REGISTER(x, y) do {			\
	*((volatile u32 *)(_P2V_CACHE(x))) = y;	\
} while(0)
#define WRITE64_CACHE_REGISTER(x, y) do {			\
	*((volatile u64 *)(_P2V_CACHE(x))) = y;	\
} while(0)

#define phx_read_u8(where)						\
	({                                                              \
	 u8 val;                                        \
	 READ8_REGISTER(where, val);				\
	 val;                                                    \
	 })
#define phx_read_u16(where)						\
	({                                                              \
	 u16 val;                                      \
	 READ16_REGISTER(where, val);				\
	 val;                                                    \
	 })
#define phx_read_u32(where)						\
	({                                                              \
	 volatile u32 val;                                       \
	 READ32_REGISTER(where, val);				\
	 val;                                                    \
	 })
#define phx_read_u64(where)						\
	({                                                              \
	 volatile u64 val;                                       \
	 READ64_REGISTER(where, val);				\
	 val;                                                    \
	 })

#define phx_write_u8(where, val)   WRITE8_REGISTER(where, val)
#define phx_write_u16(where, val)  WRITE16_REGISTER(where, val)
#define phx_write_u32(where, val)  WRITE32_REGISTER(where, val)
#define phx_write_u64(where, val)  WRITE64_REGISTER(where, val)
#define phx_write_mask_bit(where, mask, val)				\
	({								\
	 volatile u32 tmp;					\
	 tmp = ((phx_read_u32(where)) & (~(mask))) | ((val) & (mask));\
	 phx_write_u32(where, tmp);				\
	 })

#define phx_read_cache_u8(where)						\
	({                                                              \
	 u8 val;                                        \
	 READ8_CACHE_REGISTER(where, val);				\
	 val;                                                    \
	 })
#define phx_read_cache_u16(where)						\
	({                                                              \
	 u16 val;                                      \
	 READ16_CACHE_REGISTER(where, val);				\
	 val;                                                    \
	 })
#define phx_read_cache_u32(where)						\
	({                                                              \
	 volatile u32 val;                                       \
	 READ32_CACHE_REGISTER(where, val);				\
	 val;                                                    \
	 })
#define phx_read_cache_u64(where)						\
	({                                                              \
	 volatile u64 val;                                       \
	 READ64_CACHE_REGISTER(where, val);				\
	 val;                                                    \
	 })

#define phx_write_cache_u8(where, val)   WRITE8_CACHE_REGISTER(where, val)
#define phx_write_cache_u16(where, val)  WRITE16_CACHE_REGISTER(where, val)
#define phx_write_cache_u32(where, val)  WRITE32_CACHE_REGISTER(where, val)
#define phx_write_cache_u64(where, val)  WRITE64_CACHE_REGISTER(where, val)

#endif// ============================================================read write Transfer

typedef int (*TIMER_INT_FUN) (u64);

void  hrTimerModuleInit (u32 index);
void hrTimerInstConnect(u32 index, TIMER_INT_FUN routine, u64 param);
void hrTimerSetValue(u32 index, u64 value);
u64 hrTimerGetValue(u32 index);
void hrTimerSetMode(u32 index, u32 mode);
u32 hrTimerGetMode(u32 index);
void hrTimerStop(u32 index);
void hrTimerStart(u32 index);
void DumpTimerIntNum();

#endif
