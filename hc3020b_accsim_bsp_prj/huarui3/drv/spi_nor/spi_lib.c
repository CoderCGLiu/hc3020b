/* ******************************************************************************************
 * FILE NAME   : spi_lib.c
 * PROGRAMMER  : zhaozz
 * DESCRIPTION : spi library
 * DATE        : 2022-06-07 13:59:46
 * *****************************************************************************************/
#ifndef  __SPI_LIB_C__
#define  __SPI_LIB_C__
#include <stdio.h>
#include "./spi_lib.h"
#include "./spi_flash.h"
#include "../../h/drvCommon.h"
#include <pthread.h>

//#define _NOR_4B_ADDR_MODE_ /*ldf 20230628 add:: 使用4B addr mode*/
#define HC3080_SPI_CONTROLLER	2 /*ldf 20230612 add*/
static pthread_t muteSem[HC3080_SPI_CONTROLLER];/*ldf 20230612 add*/
static int spi_init_flag[HC3080_SPI_CONTROLLER] = {0, 0};/*ldf 20230612 add*/


#define wait_if(expr, cnt, desc) ({\
        int _i = 0; \
        for (;_i < cnt; _i++) { if (!(expr)) break; if (0 == _i%100) /*printf(".")*/;} \
        _i;\
        })

#if 0/*ldf 20230612 modify*/
#define uassert(x)              \
    do {                   \
        if (!(x)) {    \
            printf("[PANIC][%s:%d][%s] assertion failed! %s", __FILE__, __LINE__, __FUNCTION__, #x); \
        } \
    }while(0)

#define ustatic_assert(x)   \
    switch(x) {        \
        case 0:    \
        case (x):; \
    }

#define uoffsetof(type, member) \
    ((unsigned long)(&((type*)0)->member))

#define ROUNDDOWN(a, n) \
    ({ \
     unsigned long __a = (unsigned long)(a);\
     (typeof(a))(__a - __a%(n));\
     })
#define ROUNDUP(a, n) \
    ({ \
     unsigned long __n = (unsigned long)n; \
     (typeof(a))(ROUNDDOWN((unsigned long)a + __n -1, __n)); \
     })

static unsigned short int phx_read_u32(unsigned long addr)
{
   unsigned int val = 0;
   val =*((volatile unsigned int *)(addr));
   return val;
}
static void phx_write_u32(unsigned long addr, unsigned int val)
{
    *(volatile unsigned int *)addr =val;
}


void static_check()
{
	ustatic_assert(uoffsetof(struct spi_hc_regs, spi_hc_interrupt_status) == 0x4);
	ustatic_assert(uoffsetof(struct spi_hc_regs, spi_hc_transmit_data) == 0x1c);
	ustatic_assert(uoffsetof(struct spi_hc_regs, spi_hc_rx_threshold) == 0x2c);
	ustatic_assert(uoffsetof(struct spi_hc_regs, spi_hc_module_id) == 0xfc);
}
#endif

extern void delay_cnt(unsigned int count);
void libspi_release_reset(int spi_id)
{
	unsigned int rst_ctl;

	phx_write_u32(0x1F0C0000 + 0x28, 0xA5ACCEDE); 
	rst_ctl = phx_read_u32(0x1F0C0000 + 0x08);
	rst_ctl = rst_ctl | (1<<(spi_id+5));
	phx_write_u32(0x1F0C0000 + 0x08, rst_ctl);
	delay_cnt(1000);
	rst_ctl = rst_ctl &(~(1<<(spi_id+5)));
	phx_write_u32(0x1F0C0000 + 0x08, rst_ctl);

	phx_write_u32(0x1F0C0000 + 0x18, 0x0); 
}

void libspi_select_chip(struct spi_hc_regs* host, unsigned char cs)
{
	SPI_HC_CONFIGURATION val =  host->spi_hc_configuration;
	val.bits.MCSE = 1;
	val.bits.PSD = 0;
	val.bits.PCSL = (1<<cs)-1;
	host->spi_hc_configuration = val;
	host->spi_hc_interrupt_disable.v = 0;
	host->spi_hc_interrupt_enable.v = 0x7F;
	host->spi_hc_spi_enable.bits.SPIE = 1;
}

void libspi_release_chip(struct spi_hc_regs* host)
{
	host->spi_hc_spi_enable.bits.SPIE = 0;
	host->spi_hc_interrupt_enable.v = 0;
	host->spi_hc_interrupt_disable.v = 0x7f;
	host->spi_hc_configuration.bits.PCSL = 0xf;
	host->spi_hc_interrupt_status.v = 0xff;
}

/*is_master:0 - SLAVE
 * 			1 - MASTER
 * 
 * CPHA:0 – SCLK 上升沿采样
		1 – SCLK 下降沿采样
 * CPOL:0 – 空闲时 SCLK 为低电平
		1 – 空闲时 SCLK 为高电平
 * clk_div: 0 - 2分频
 * 			1 - 4分频
 * 			2 - 8分频
 * 			3 - 16分频
 * 			4 - 32分频
 * 			5 - 64分频
 * 			6 - 128分频
 * 			7 - 256分频
 * 低速总线参考时钟200MHz
*/
void libspi_init(struct spi_hc_regs* host, int is_master, int cpha, int cpol, int clk_div)
{
//	printf("module id: 0x%x\r\n", host->spi_hc_module_id.bits.MID);
//	libspi_reset(0);
	host->spi_hc_spi_enable.bits.SPIE = 0;
	host->spi_hc_interrupt_enable.v = 0;
	host->spi_hc_interrupt_disable.v = 0x7F;

	//config
	SPI_HC_CONFIGURATION cfg = host->spi_hc_configuration;
	cfg.bits.CPHA = cpha;
	cfg.bits.CPOL = cpol;
	cfg.bits.MSEL = is_master;
	cfg.bits.MSC = 0;
	cfg.bits.MSE = 0;
	cfg.bits.MBRD = clk_div;
	cfg.bits.PCSL = 0xf;
	host->spi_hc_configuration = cfg;

	libspi_select_chip(host, 0);
	libspi_release_chip(host);
}

void libspi_send(struct spi_hc_regs* host, unsigned char* wbuf, int len, unsigned char cs, int is_msg_end)
{
	unsigned int controller = (host == SPI0_HC_REGS) ? 0 : 1;/*ldf 20230612 add*/
	
	//lock();
	pthread_mutex_lock2(&muteSem[controller],WAIT, NO_TIMEOUT);/*ldf 20230612 add*/
	
	libspi_select_chip(host, cs);
	int i = 0;
	for (i=0; i<len; i++)
	{
		wait_if(host->spi_hc_interrupt_status.bits.TF, 1000, "spi_hc_interrupt_status.TF");
		host->spi_hc_transmit_data.bits.TDATA =  wbuf[i];
	}
	if (is_msg_end)
	{
		libspi_release_chip(host);
		//unlock();
		pthread_mutex_unlock(&muteSem[controller]);/*ldf 20230612 add*/
	}
}

void libspi_recv(struct spi_hc_regs* host, unsigned char *rbuf, unsigned int len, int is_msg_end)
{
	unsigned int controller = (host == SPI0_HC_REGS) ? 0 : 1;/*ldf 20230612 add*/
	
	host->spi_hc_configuration.bits.RXCLR = 1;
	int i = 0;
	for (i=0; i<len; i++)
	{
		host->spi_hc_transmit_data.bits.TDATA = 0;
		wait_if(!host->spi_hc_interrupt_status.bits.RNE, 1000, "!spi_hc_interrupt_status.RNE");
		rbuf[i] = host->spi_hc_receive_data.bits.RDATA;
	}
	if (is_msg_end)
	{
		libspi_release_chip(host);
		//	unlock();
		pthread_mutex_unlock(&muteSem[controller]);/*ldf 20230612 add*/
	}
}

unsigned int libspi_flash_read_id(struct spi_hc_regs* host)
{
	unsigned char cmd[8] = {CMD_READ_ID};
	unsigned int retid = 0;
	libspi_send(host, cmd, 1, 0, 0);
	libspi_recv(host, (unsigned char*)&retid, 3, 1);
	return retid;
}

void libspi_flash_write_enable(struct spi_hc_regs* host)
{
	unsigned char cmd[8] ={CMD_WRITE_ENABLE};
	libspi_send(host, cmd, 1, 0, 1);
}

void libspi_flash_write_disable(struct spi_hc_regs* host)
{
	unsigned char cmd[8] ={CMD_WRITE_DISABLE};
	libspi_send(host, cmd, 1, 0, 1);
}

SPI_FLASH_STATUS libspi_flash_read_status(struct spi_hc_regs* host)
{
	unsigned char cmd[8] = {CMD_READ_STATUS_REGISTER};
	SPI_FLASH_STATUS ret = {.v = 0};
	unsigned char rbuf[8]={0};
	libspi_send(host, cmd, 1, 0, 0);
	libspi_recv(host, rbuf, 1, 1);
	ret.v = rbuf[0];
	return ret;
}

SPI_FLASH_FLAG_STATUS libspi_flash_read_flag_status(struct spi_hc_regs* host)
{
	unsigned char cmd[8] = {CMD_READ_FLAG_STATUS_REGISTER};
	SPI_FLASH_FLAG_STATUS ret = {.v = 0};
	unsigned char rbuf[8]={0};
	libspi_send(host, cmd, 1, 0, 0);
	libspi_recv(host, rbuf, 1, 1);
	ret.v = rbuf[0];
	return ret;
}

SPI_FLASH_NONVOLATILE_CONFIGURATION libspi_flash_read_nonvolatile_config(struct spi_hc_regs* host)/*ldf 20230613 add*/
{
	unsigned char cmd[8] = {CMD_READ_NONVOLATILE_CONFIGURATION_REGISTER};
	SPI_FLASH_NONVOLATILE_CONFIGURATION ret = {.v = 0};
	unsigned char rbuf[8]={0};
	libspi_send(host, cmd, 1, 0, 0);
	libspi_recv(host, rbuf, 2, 1);
	ret.v = (((unsigned short)(rbuf[0])) << 8) | rbuf[1];
	return ret;
}

void libspi_flash_wait_write_enable(struct spi_hc_regs* host)
{
	wait_if(libspi_flash_read_status(host).bits.WRITE_ENABLE_LATCH != 1, 1000, "SPI_FLASH_STATUS.WRITE_ENABLE_LATCH != 1");
}

void libspi_flash_wait_write_idle(struct spi_hc_regs* host)
{
	wait_if(libspi_flash_read_status(host).bits.WRITE_IN_PROGRESS == 1, 1000, "SPI_FLASH_STATUS.WRITE_IN_PROGRESS==1");
}

void libspi_flash_wait_erase_idle(struct spi_hc_regs* host)/*ldf 20230614 add*/
{
	wait_if(libspi_flash_read_flag_status(host).bits.PROGRAM_OR_ERASE_CONTROLLER != 1, 1000000, "SPI_FLASH_FLAG_STATUS.PROGRAM_OR_ERASE_CONTROLLER != 1");
}

//16M
void libspi_flash_bulk_erase(struct spi_hc_regs* host)
{
	unsigned char cmd[8] = {CMD_BULK_ERASE};
	
	libspi_send(host, cmd, 1, 0, 1);
}

//64K
void libspi_flash_sector_erase(struct spi_hc_regs* host, unsigned int addr, int is_4B_mode)
{
	unsigned char cmd[8] = {0};
	
	if(is_4B_mode)
	{
		cmd[0] = CMD_SECTOR_ERASE_4B;
		cmd[1] = (addr>>24)&0xff;
		cmd[2] = (addr>>16)&0xff;
		cmd[3] = (addr>>8)&0xff;
		cmd[4] = addr&0xff;
		libspi_send(host, cmd, 5, 0, 1);
	}
	else
	{
		cmd[0] = CMD_SECTOR_ERASE;
		cmd[1] = (addr>>16)&0xff;
		cmd[2] = (addr>>8)&0xff;
		cmd[3] = addr&0xff;
		libspi_send(host, cmd, 4, 0, 1);
	}
}

//4K
void libspi_flash_subsector_erase(struct spi_hc_regs *host, unsigned int addr, int is_4B_mode)
{
	unsigned char cmd[8] = {0};
	
	if(is_4B_mode)
	{
		cmd[0] = CMD_SUBSECTOR_ERASE_4B;
		cmd[1] = (addr>>24)&0xff;
		cmd[2] = (addr>>16)&0xff;
		cmd[3] = (addr>>8)&0xff;
		cmd[4] = addr&0xff;
		libspi_send(host, cmd, 5, 0, 1);
	}
	else
	{
		cmd[0] = CMD_SUBSECTOR_ERASE;
		cmd[1] = (addr>>16)&0xff;
		cmd[2] = (addr>>8)&0xff;
		cmd[3] = addr&0xff;
		libspi_send(host, cmd, 4, 0, 1);
	}
}

void libspi_flash_page_program(struct spi_hc_regs* host, unsigned int addr, unsigned char *wbuf, unsigned int len, int is_4B_mode)
{
	unsigned char cmd[FLASH_PAGE_SIZE+5] = {0};
	int i = 0;
	
	if (len > FLASH_PAGE_SIZE)
	{
		printf("max page size is 0x%x\r\n", FLASH_PAGE_SIZE);
		return;
	}
	
	if(is_4B_mode)
	{
		cmd[0] = CMD_PAGE_PROGRAM_4B;
		cmd[1] = (addr>>24)&0xff;
		cmd[2] = (addr>>16)&0xff;
		cmd[3] = (addr>>8)&0xff;
		cmd[4] = addr&0xff;
		for(i=0; i<len; i++)
		{
			cmd[i+5] = wbuf[i];
		}
		libspi_send(host, cmd, len+5, 0, 1);
	}
	else
	{
		cmd[0] = CMD_PAGE_PROGRAM;
		cmd[1] = (addr>>16)&0xff;
		cmd[2] = (addr>>8)&0xff;
		cmd[3] = addr&0xff;
		for(i=0; i<len; i++)
		{
			cmd[i+4] = wbuf[i];
		}
		libspi_send(host, cmd, len+4, 0, 1);
	}
}

void libspi_flash_read(struct spi_hc_regs* host, unsigned int addr, unsigned char* rbuf, unsigned int len, int is_4B_mode)
{
	unsigned char cmd[8] = {0};
	
	if(is_4B_mode)
	{
		cmd[0] = CMD_READ_4B;
		cmd[1] = (addr>>24)&0xff;
		cmd[2] = (addr>>16)&0xff;
		cmd[3] = (addr>>8)&0xff;
		cmd[4] = addr&0xff;
		libspi_send(host, cmd, 5, 0, 0);
	}
	else
	{
		cmd[0] = CMD_READ;
		cmd[1] = (addr>>16)&0xff;
		cmd[2] = (addr>>8)&0xff;
		cmd[3] = addr&0xff;
		libspi_send(host, cmd, 4, 0, 0);
	}
	libspi_recv(host, rbuf, len, 1);
}

void libspi_flash_fast_read(struct spi_hc_regs* host, unsigned int addr, unsigned char* rbuf, unsigned int len, int is_4B_mode)
{
	unsigned char cmd[8] = {0};
	
	if(is_4B_mode)
	{
		cmd[0] = CMD_FAST_READ_4B;
		cmd[1] = (addr>>24)&0xff;
		cmd[2] = (addr>>16)&0xff;
		cmd[3] = (addr>>8)&0xff;
		cmd[4] = addr&0xff;
		libspi_send(host, cmd, 5, 0, 0);
	}
	else
	{
		cmd[0] = CMD_FAST_READ;
		cmd[1] = (addr>>16)&0xff;
		cmd[2] = (addr>>8)&0xff;
		cmd[3] = addr&0xff;
		libspi_send(host, cmd, 4, 0, 0);
	}
	libspi_recv(host, rbuf, 1, 0);/*Dummy*/
	libspi_recv(host, rbuf, len, 1);
}


void libspi_reset_device(struct spi_hc_regs* host)/*ldf 20230613 add*/
{
	unsigned char cmd[8] = {CMD_RESET_ENABLE, CMD_RESET_MEMORY};
	unsigned int controller = (host == SPI0_HC_REGS) ? 0 : 1;/*ldf 20230612 add*/
	
	//lock();
	pthread_mutex_lock2(&muteSem[controller],WAIT, NO_TIMEOUT);
	
	libspi_select_chip(host, 0);
	wait_if(host->spi_hc_interrupt_status.bits.TF, 1000, "spi_hc_interrupt_status.TF");
	host->spi_hc_transmit_data.bits.TDATA =  cmd[0];
	
	libspi_release_chip(host);
	
	libspi_select_chip(host, 0);
	wait_if(host->spi_hc_interrupt_status.bits.TF, 1000, "spi_hc_interrupt_status.TF");
	host->spi_hc_transmit_data.bits.TDATA =  cmd[1];
	
	libspi_release_chip(host);
	//unlock();
	pthread_mutex_unlock(&muteSem[controller]);
}

void libspi_flash_set_addr_mode(struct spi_hc_regs* host, unsigned int is_3byte_addr)/*ldf 20230613 add*/
{
	SPI_FLASH_NONVOLATILE_CONFIGURATION ret = {.v = 0};
	unsigned char cmd[8] = {CMD_WRITE_NONVOLATILE_CONFIGURATION_REGISTER};
	SPI_FLASH_FLAG_STATUS status = {.v = 0};
	
	ret = libspi_flash_read_nonvolatile_config(host);
//	printk("<**DEBUG**> [%s():_%d_]:: read nonvolatile config: 0x%x\n", __FUNCTION__, __LINE__,ret);
	ret.bits.ADDRESS_BYTES = is_3byte_addr;/*0 = Enable 4B address, 1 = Enable 3B address (Default)*/
	cmd[0] = CMD_WRITE_NONVOLATILE_CONFIGURATION_REGISTER;
	cmd[1] = ret.v >> 8;
	cmd[2] = ret.v & 0xff;
	libspi_send(host, cmd, 3, 0, 1);
	
	usleep(100000);
	
	ret = libspi_flash_read_nonvolatile_config(host);
	if(ret.bits.ADDRESS_BYTES != is_3byte_addr)
	{
		printk("<ERROR> [%s():_%d_]:: set addr mode failed! ret=0x%x\n", __FUNCTION__, __LINE__,ret);
	}
	
//	status = libspi_flash_read_flag_status(host);
//	printk("<**DEBUG**> [%s():_%d_]:: read flag status: 0x%x\n", __FUNCTION__, __LINE__,status);
//	if(status.bits.ADDRESSING == is_3byte_addr)
//	{
//		printk("<ERROR> [%s():_%d_]:: set addr mode failed! status=0x%x\n", __FUNCTION__, __LINE__,status);
//	}
}

/*****************************************cmd**************************************************/
void spi_module_init(u32 controller)
{
	if(spi_init_flag[controller])/*ldf 20230612 add*/
		return;
//	printk(">>>[%d]%s\n", __LINE__, __func__); /* lcg 20230921 add */
	struct spi_hc_regs* host = (controller == 0) ? SPI0_HC_REGS : SPI1_HC_REGS;

	/*spi0/1配置release reset*/
    libspi_release_reset(controller); /*ldf 20230612 add*/
	
	/*MASTER, SCLK 下降沿采样, 空闲时 SCLK 为高电平, SCLK为2分频*/ /*xxx ldf 20230628:: spi/qspi的参考时钟都是50MHz*/
    libspi_init(host,1,1,1,0);/*xxx ldf 20230614:: 换用其他频率后，读出来的id不对*/
    
#ifdef _NOR_4B_ADDR_MODE_
    libspi_flash_set_addr_mode(host, 0);/*ldf 20230613 add:: 设置为4byte addr*/
#endif
	
	pthread_mutex_init2(&muteSem[controller],PTHREAD_MUTEX_RECURSIVE,
			RE_MUTEX_PRIO_NONE |RE_MUTEX_WAIT_FIFO ,PTHREAD_MUTEX_CEILING); /*ldf 20230612 add*/
	
	spi_init_flag[controller] = 1;/*ldf 20230612 add*/
	
	return;
}

void switchToNor(void)
{
	spi_module_init(0);
	spi_module_init(1);
	return;
}

int NorFlashEraseAll(int controller)
{
	struct spi_hc_regs* host = (controller == 0) ? SPI0_HC_REGS : SPI1_HC_REGS;

	libspi_flash_write_enable(host);
	libspi_flash_wait_write_enable(host);
	libspi_flash_bulk_erase(host);

	sleep(3);/*ldf 20230612 add*/
	libspi_flash_wait_write_idle(host);/*ldf 20230612 add*/
	libspi_flash_wait_erase_idle(host);/*ldf 20230613 add*/
	
	return 0;
}

int NorFlashErase(int controller, unsigned int addr, unsigned int len)
{
	unsigned int erase_size = FLASH_SECTOR_SIZE;//FLASH_SUBSECTOR_SIZE;
	unsigned int erase_addr = 0;
	int erase_loop = 0;
	int i = 0;
	struct spi_hc_regs* host = (controller == 0) ? SPI0_HC_REGS : SPI1_HC_REGS;
	
	if(len < erase_size)
	{
		printf("<ERROR> len(0x%x) is too small! must >= 0x%x\n",len,erase_size);
		return;
	}
	
	erase_loop = len / erase_size;
//	if((len % erase_size) != 0)
//		erase_loop++;

	for(i=0;i<erase_loop;i++)
	{
		erase_addr = addr + i * erase_size;
		libspi_flash_write_enable(host);
		libspi_flash_wait_write_enable(host);

#ifndef _NOR_4B_ADDR_MODE_
		//3B addr mode
//		libspi_flash_subsector_erase(host, erase_addr, 0);//4k
		libspi_flash_sector_erase(host, erase_addr, 0);//64k
#else
		//4B addr mode
//		libspi_flash_subsector_erase(host, erase_addr, 1);//4k
		libspi_flash_sector_erase(host, erase_addr, 1);//64k
#endif

		usleep(10000);/*ldf 20230612 add*/
		libspi_flash_wait_write_idle(host);/*ldf 20230612 add*/
		libspi_flash_wait_erase_idle(host);/*ldf 20230613 add*/
	}

	return 0;
}

int NorFlashWrite(int controller, unsigned int addr, unsigned char* wbuf, unsigned int len)
{
	struct spi_hc_regs* host = (controller == 0) ? SPI0_HC_REGS : SPI1_HC_REGS;
	int i = 0;
	
	/*************************************************erase******************************************/
//	printf("erase start!\r\n");
//	int erase_loop = ROUNDUP(len, FLASH_SUBSECTOR_SIZE)/FLASH_SUBSECTOR_SIZE;
//	for (i=0; i<erase_loop; i++)
//	{
//		libspi_flash_write_enable(host);
//		libspi_flash_wait_write_enable(host);
//		printf("0x%x\r\n", i);
//		libspi_flash_subsector_erase(host, addr+i*FLASH_SUBSECTOR_SIZE);
//	}
//	printf("erase complete!\r\n");

//	delay_cnt(1000);
	/*************************************************write******************************************/
//	printf("write start!\r\n");
	int write_size = FLASH_PAGE_SIZE;
	int write_loop = 0;
	int write_remain_len = 0;
	
	if(len >= write_size)
		write_loop = len / write_size;
	write_remain_len = len % write_size;
	if(write_remain_len != 0)
		write_loop++;
	
	for (i=0; i<write_loop; i++)
	{
		libspi_flash_write_enable(host);
		libspi_flash_wait_write_enable(host);
		if ((i == (write_loop - 1)) && (write_remain_len != 0)) 
		{
#ifndef _NOR_4B_ADDR_MODE_
			libspi_flash_page_program(host, addr+i*write_size, wbuf+i*write_size, write_remain_len, 0);//3B addr mode
#else
			libspi_flash_page_program(host, addr+i*write_size, wbuf+i*write_size, write_remain_len, 1);//4B addr mode
#endif
		} 
		else 
		{
#ifndef _NOR_4B_ADDR_MODE_
			libspi_flash_page_program(host, addr+i*write_size, wbuf+i*write_size, write_size, 0);//3B addr mode
#else
			libspi_flash_page_program(host, addr+i*write_size, wbuf+i*write_size, write_size, 1);//4B addr mode
#endif
		}
		libspi_flash_wait_write_idle(host);
	}
	
//	printf("write complete!\r\n");
	return 0;

}

/*0x19bb20*/
unsigned int NorFlashReadid(int controller)
{
	u32 id;
	struct spi_hc_regs* host = (controller == 0) ? SPI0_HC_REGS : SPI1_HC_REGS;

	id = libspi_flash_read_id(host);
	
	return id;
}


int NorFlashRead(int controller, unsigned int addr, unsigned char* rbuf, unsigned int len)
{
	struct spi_hc_regs* host = (controller == 0) ? SPI0_HC_REGS : SPI1_HC_REGS;
	
	libspi_flash_write_enable(host);/*ldf 20230612 add*/
	libspi_flash_wait_write_enable(host);/*ldf 20230612 add*/
	
#ifndef _NOR_4B_ADDR_MODE_
//	libspi_flash_read(host, addr, rbuf, len, 0);
	libspi_flash_fast_read(host, addr, rbuf, len, 0);/*ldf 20230612 add*/
#else
//	libspi_flash_read(host, addr, rbuf, len, 1);
	libspi_flash_fast_read(host, addr, rbuf, len, 1);/*ldf 20230612 add*/
#endif
	
	return 0;
}	


static int CheckUint8(const unsigned char *pSrc, const unsigned char *pDst, unsigned int length)
{
    int index = 0;
    unsigned char valSrc = 0;
    unsigned char valDst = 0;
    int err_num = 0;
    
    printf(" ##CheckData## pSrc: 0x%016lX, pDst: 0x%016lX, length: %lu = 0x%016lX\n", (unsigned long)pSrc, (unsigned long)pDst, length, length);
    for(index = 0; index < length; index ++)
    {
    	valSrc = *(pSrc + index);
    	valDst = *(pDst + index);
		if(valSrc != valDst)
		{
			err_num++;
			if(err_num <= 0x10)
			{
				printf("<Check Error> index=0x%x, val=0x%x, except_val=0x%x\n",\
						index,valDst,valSrc);
			}
//			return -1;
		}
    }
    
    if(err_num == 0)
    	printf("   Check OK!\n");
    else
    	printf("   Check Failed! err_num = 0x%x\n",err_num);
    
	return err_num;
}
static void DumpUint8(const unsigned char *pAddr, unsigned int length)
{
    int index = 0;

    printf("pAddr: 0x%016lX, length: %lu = 0x%016lX", (unsigned long)pAddr, length, length);
    for (index = 0; index < length; index++)
    {
        if (index % 0x10 == 0)
        {
            printf("\n0x%08lX: ", index);
        }
        printf("0x%02X ", *(unsigned char *)(pAddr + index));
    } 

    printf("\n");
}
void Dump_nor_flash(int controller, unsigned int addr, unsigned int len)
{
	int i;
	
	u8* rbuf = malloc(len);
	
	NorFlashRead(controller, addr, rbuf, len);
//	printf("<DEBUG> [%s:__%d__]\n",__FUNCTION__,__LINE__);
	DumpUint8(rbuf, len);
//	printf("<DEBUG> [%s:__%d__]\n",__FUNCTION__,__LINE__);
//	printf("val = ");
//	for(i=0;i<len; i++)
//	{
//		printf("0x%x ", rbuf[i]);
//	}
//	printf("\n");
	
	free(rbuf);
	return;
}



void test_norflash_write(int controller, unsigned int addr, unsigned int len, unsigned char first_data)
{
    unsigned char *data = (unsigned char *)malloc(len);
    int i = 0;
    
    memset(data, 0, len);
    
    for(i = 0; i < len; i ++)
    {
        data[i] = i + first_data;
//        printf("0x%x ", data[i]);
    }
//    printf("\n");
    
    printf("\nafter write =====================\n");
    NorFlashWrite(controller, addr, data, len);
    
    free(data);
    
	return;
}

void test_norflash_rw(int controller, unsigned int addr, unsigned int len, unsigned char first_data, int is_DumpData)
{
//	if(len > FLASH_PAGE_SIZE)
//	{
//		printf("\n<ERROR> len is too large!\n");
//		return;
//	}
	
//    volatile unsigned int val = 0;
    unsigned char *buf = (unsigned char *)malloc(len);
    unsigned char *data = (unsigned char *)malloc(len);
    int i = 0;
    unsigned int retid;
    int err_num = 0;
    
    memset(buf, 0, len);
    memset(data, 0, len);

    printf("\n=======NOR FLASH TEST START=========\n");
    
    spi_module_init(controller);

    retid = NorFlashReadid(controller);
    printf("nor flash id= 0x%x\n", retid);

    printf("\nfirst read =====================\n");
//    Dump_nor_flash(controller, addr, len);
	NorFlashRead(controller, addr, buf, len);
	if(is_DumpData)
		DumpUint8(buf, len);
    
    printf("\nafter erase =====================\n"); 
    NorFlashErase(controller, addr, len);
    memset(buf, 0, len);
//    Dump_nor_flash(controller, addr, len);
	NorFlashRead(controller, addr, buf, len);
	if(is_DumpData)
		DumpUint8(buf, len);
    memset(data, 0xff, len);
    err_num = CheckUint8(data, buf, len);
    if(err_num > 0)
    	printf("  Nor Flash Erase Test Failed!, err_num = 0x%x\n",err_num); 
    else
    	printf("  Nor Flash Erase Test OK!\n"); 
    
    for(i = 0; i < len; i ++)
    {
        data[i] = i + first_data;
//        printf("0x%x ", data[i]);
    }
//    printf("\n");
    
    printf("\nafter write =====================\n");
    NorFlashWrite(controller, addr, data, len);
    memset(buf, 0, len);
//    Dump_nor_flash(controller, addr, len);
	NorFlashRead(controller, addr, buf, len);
	if(is_DumpData)
		DumpUint8(buf, len);
    err_num = CheckUint8(data, buf, len);
    if(err_num > 0)
    	printf("  Nor Flash Write Test Failed!, err_num = 0x%x\n",err_num); 
    else
    	printf("  Nor Flash Write Test OK!\n");
	
    printf("\n=======NOR FLASH TEST END=========\n");
    
    free(buf);
    free(data);
}


#endif  /* __SPI_LIB_C__ */

