/* ******************************************************************************************
 * FILE NAME   : spi_flash.h
 * PROGRAMMER  : zhaozz
 * DESCRIPTION : N25Q128A flash define
 * DATE        : 2022-06-08 13:53:00
 * *****************************************************************************************/
#ifndef  __SPI_FLASH_H__
#define  __SPI_FLASH_H__

//reset
#define CMD_RESET_ENABLE                                        0x66
#define CMD_RESET_MEMORY                                        0x99
//identification
#define CMD_READ_ID                                             0x9F
#define CMD_MULTIPLE_IO_READ_ID                                 0xAF
#define CMD_READ_SERIAL_FLASH_DISCOVERY_PARAMETER               0x5A
//read
#define CMD_READ                                                0x03
#define CMD_FAST_READ                                           0x0B
#define CMD_DUAL_OUTPUT_FAST_READ                               0x3B
#define CMD_QUAD_OUTPUT_FAST_READ                               0x6B
#define CMD_READ_4B                                             0x13/*ldf 20230613 add*/
#define CMD_FAST_READ_4B                                        0x0C/*ldf 20230613 add*/
#define CMD_DUAL_OUTPUT_FAST_READ_4B                            0x3C/*ldf 20230613 add*/
#define CMD_DUAL_INPUT_OUTPUT_FAST_READ_4B                      0xBC/*ldf 20230613 add*/
//write
#define CMD_WRITE_ENABLE                                        0x06
#define CMD_WRITE_DISABLE                                       0x04
//register
#define CMD_READ_STATUS_REGISTER                                0x05
#define CMD_WRITE_STATUS_REGISTER                               0x01
#define CMD_READ_LOCK_REGISTER                                  0xE8
#define CMD_WRITE_LOCK_REGISTER                                 0xE5
#define CMD_READ_FLAG_STATUS_REGISTER                           0x70
#define CMD_CLEAR_FLAG_STATUS_REGISTER                          0x50
#define CMD_READ_NONVOLATILE_CONFIGURATION_REGISTER             0xB5
#define CMD_WRITE_NONVOLATILE_CONFIGURATION_REGISTER            0xB1
#define CMD_READ_VOLATILE_CONFIGURATION_REGISTER                0x85
#define CMD_WRITE_VOLATILE_CONFIGURATION_REGISTER               0x81
#define CMD_READ_ENHANCED_VOLATILE_CONFIGURATION_REGISTER       0x65
#define CMD_WRITE_ENHANCED_VOLATILE_CONFIGURATION_REGISTER      0x61
//program
#define CMD_PAGE_PROGRAM                                        0x02
#define CMD_PAGE_PROGRAM_4B                                     0x12/*ldf 20230613 add*/
#define CMD_DUAL_INPUT_FAST_PROGRAM                             0xA2
#define CMD_EXTEND_DUAL_INPUT_FAST_PROGRAM                      0xD2
#define CMD_QUAD_INPUT_FAST_PROGRAM                             0x32
#define CMD_EXTEND_QUAD_INPUT_FAST_PROGRAM                      0x12
//erase
#define CMD_SUBSECTOR_ERASE                                     0x20
#define CMD_SUBSECTOR_ERASE_4B                                  0x21/*ldf 20230613 add*/
#define CMD_SECTOR_ERASE                                        0xD8
#define CMD_SECTOR_ERASE_4B                                     0xDC/*ldf 20230613 add*/
#define CMD_BULK_ERASE                                          0xC7
#define CMD_PROGRAM_ERASE_RESUME                                0x7A
#define CMD_PROGRAM_ERASE_SUSPEND                               0x75
//one-time programmable
#define CMD_READ_OTP_ARRAY                                      0x4B
#define CMD_PROGRAM_OTP_ARRAY                                   0x42
//4-BYTE ADDRESS MODE Operations
#define CMD_ENTER_4B_ADDR_MODE                                  0xB7/*ldf 20230613 add*/
#define CMD_EXIT_4B_ADDR_MODE                                   0xE9/*ldf 20230613 add*/


#define FLASH_PAGE_SIZE      256
#define FLASH_SUBSECTOR_SIZE (0x1000UL)//4KB
#define FLASH_SECTOR_SIZE    (0x10000UL)//64KB
#define FLASH_CHIP_SIZE    (0x2000000UL)//32MB


typedef union {
	volatile unsigned char v;
	struct {
		volatile unsigned int WRITE_IN_PROGRESS   :1; //1-busy 0-ready
		volatile unsigned int WRITE_ENABLE_LATCH  :1; //1-set  0-clear
		volatile unsigned int BP0                 :3; //protected area tables
		volatile unsigned int TOP_BOTTOM          :1; //0-top  1-bottom
		volatile unsigned int BP1                 :1; //protected area tables
		volatile unsigned int WRITE_DISABLED      :1; //0-enabled   1-disabled
	} bits;
} SPI_FLASH_STATUS;

#if 0
typedef union {
	volatile unsigned short int v;
	struct {
		volatile unsigned int __RESVED0           :2;
		volatile unsigned int DUAL_IO_PROTOCOL    :1;
		volatile unsigned int QUAD_IO_PROTOCOL    :1;
		volatile unsigned int RESET_HOLD          :1;
		volatile unsigned int __RESVED1           :1;
		volatile unsigned int OUTPUT_DRIVER       :3;
		volatile unsigned int XIP_MODE_AT_RESET   :3;
		volatile unsigned int DUMMY_CYCLES        :4;
	} bits;
} SPI_FLASH_NONVOLATILE_CONFIGURATION;
#else /*ldf 20230613 add*/
typedef union {
	volatile unsigned short int v;
	struct {
		volatile unsigned int ADDRESS_BYTES       :1;/*0 = Enable 4B address, 1 = Enable 3B address (Default)*/
		volatile unsigned int SEGMENT_SELETC      :1;/*0 = Upper 128Mb segment, 1 = Lower 128Mb segment (Default)*/
		volatile unsigned int DUAL_IO_PROTOCOL    :1;/*0 = Enabled, 1 = Disabled (Default, Extended SPI protocol)*/
		volatile unsigned int QUAD_IO_PROTOCOL    :1;/*0 = Enabled, 1 = Disabled (Default, Extended SPI protocol)*/
		volatile unsigned int RESET_HOLD          :1;/*0 = Disabled, 1 = Enabled (Default)*/
		volatile unsigned int __RESVED1           :1;
		volatile unsigned int OUTPUT_DRIVER       :3;
		volatile unsigned int XIP_MODE_AT_RESET   :3;
		volatile unsigned int DUMMY_CYCLES        :4;
	} bits;
} SPI_FLASH_NONVOLATILE_CONFIGURATION;
#endif

typedef union {
	volatile unsigned char v;
	struct {
		volatile unsigned int OUTPUT_DRIVER       :3;
		volatile unsigned int VPP_ACCELERATOR     :1; /*0 = Enabled, 1 = Disabled (Default)*/
		volatile unsigned int REST_HOLD           :1; //0-disable, 1-enable(Default)
		volatile unsigned int DOUBLE_TRANS_RATE   :1; //0-enable, 1-disable
		volatile unsigned int DUAL_PROTOCAL       :1; //0-enable, 1-disable(Default,extended SPI protocol)
		volatile unsigned int QUAD_PROTOCAL       :1; //0-enable, 1-disable(Default,extended SPI protocol)
	} bits;
} SPI_FLASH_ENHANCED_VOLATILE_CONF_REG;


typedef union {
	volatile unsigned char v;
	struct {
		volatile unsigned int ADDRESSING                    :1;/*0-3 byte addr, 1-4 byte addr*/
		volatile unsigned int PROTECTION                    :1;/*0-clear, 1-Failure or protection error*/
		volatile unsigned int PROGRAM_SUSPEND               :1;/*0 = Not in effect, 1 = In effect*/
		volatile unsigned int VPP                           :1;/*0 = Enabled, 1 = Disabled (Default)*/
		volatile unsigned int PROGRAM                       :1;/*0 = Clear, 1 = Failure or protection error*/
		volatile unsigned int ERASE                         :1;/*0 = Clear, 1 = Failure or protection error*/
		volatile unsigned int ERASE_SUSPEND                 :1;/*0 = Not in effect, 1 = In effect*/
		volatile unsigned int PROGRAM_OR_ERASE_CONTROLLER   :1;/*0 = Busy, 1 = Ready*/
	} bits;
} SPI_FLASH_FLAG_STATUS;



#endif  /* __SPI_FLASH_H__ */
