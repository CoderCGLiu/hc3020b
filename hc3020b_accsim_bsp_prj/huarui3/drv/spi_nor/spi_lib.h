/* ******************************************************************************************
 * FILE NAME   : spi_lib.h
 * PROGRAMMER  : zhaozz
 * DESCRIPTION : spi header
 * DATE        : 2022-06-07 14:00:00
 * *****************************************************************************************/
#ifndef  __SPI_LIB_H__
#define  __SPI_LIB_H__

typedef union {
	volatile unsigned int v;
	struct {
		volatile unsigned int MSEL                             :1;
		volatile unsigned int CPOL                             :1;
		volatile unsigned int CPHA                             :1;
		volatile unsigned int MBRD                             :3;
		volatile unsigned int TWS                              :2;
		volatile unsigned int MRCS                             :1;
		volatile unsigned int PSD                              :1;
		volatile unsigned int PCSL                             :4;
		volatile unsigned int MCSE                             :1;
		volatile unsigned int MSE                              :1;
		volatile unsigned int MSC                              :1;
		volatile unsigned int MFGE                             :1;
		volatile unsigned int SPSE                             :1;
		volatile unsigned int RXCLR                            :1;
		volatile unsigned int TXCLR                            :1;
		volatile unsigned int __RESVERD                        :11;
	} bits;
} SPI_HC_CONFIGURATION;

typedef union {
	volatile unsigned int v;
	struct {
		volatile unsigned int ROF                              :1;
		volatile unsigned int MF                               :1;
		volatile unsigned int TNF                              :1;
		volatile unsigned int TF                               :1;
		volatile unsigned int RNE                              :1;
		volatile unsigned int RF                               :1;
		volatile unsigned int TUF                              :1;
		volatile unsigned int __RESVERD                        :25;
	} bits;
} SPI_HC_INTERRUPT_STATUS;

typedef union {
	volatile unsigned int v;
	struct {
		volatile unsigned int ROFE                             :1;
		volatile unsigned int MFE                              :1;
		volatile unsigned int TNFE                             :1;
		volatile unsigned int TFE                              :1;
		volatile unsigned int RNEE                             :1;
		volatile unsigned int RFE                              :1;
		volatile unsigned int TUFE                             :1;
		volatile unsigned int __RESVERD                        :25;
	} bits;
} SPI_HC_INTERRUPT_ENABLE;

typedef union {
	volatile unsigned int v;
	struct {
		volatile unsigned int ROFD                             :1;
		volatile unsigned int MFD                              :1;
		volatile unsigned int TNFD                             :1;
		volatile unsigned int TFD                              :1;
		volatile unsigned int RNED                             :1;
		volatile unsigned int RFD                              :1;
		volatile unsigned int TUFD                             :1;
		volatile unsigned int __RESVERD                        :25;
	} bits;
} SPI_HC_INTERRUPT_DISABLE;

typedef union {
	volatile unsigned int v;
	struct {
		volatile unsigned int ROFM                             :1;
		volatile unsigned int MFM                              :1;
		volatile unsigned int TNFM                             :1;
		volatile unsigned int TFM                              :1;
		volatile unsigned int RNEM                             :1;
		volatile unsigned int RFM                              :1;
		volatile unsigned int TUFM                             :1;
		volatile unsigned int __RESVERD                        :25;
	} bits;
} SPI_HC_INTERRUPT_MASK;

typedef union {
	volatile unsigned int v;
	struct {
		volatile unsigned int SPIE                             :1;
		volatile unsigned int __RESVERD                        :30;
	} bits;
} SPI_HC_SPI_ENABLE;

typedef union {
	volatile unsigned int v;
	struct {
		volatile unsigned int D_INIT                           :8;
		volatile unsigned int D_AFTER                          :8;
		volatile unsigned int D_BTWN                           :8;
		volatile unsigned int D_NSS                            :8;
	} bits;
} SPI_HC_DELAY;

typedef union {
	volatile unsigned int v;
	struct {
		volatile unsigned int TDATA                            :8;
		volatile unsigned int __RESVERD                        :24;
	} bits;
} SPI_HC_TRANSMIT_DATA;

typedef union {
	volatile unsigned int v;
	struct {
		volatile unsigned int RDATA                            :8;
		volatile unsigned int __RESVERD                        :24;
	} bits;
} SPI_HC_RECEIVE_DATA;

typedef union {
	volatile unsigned int v;
	struct {
		volatile unsigned int SICNT                            :8;
		volatile unsigned int __RESVERD                        :24;
	} bits;
} SPI_HC_SLAVE_IDLE_COUNT;

typedef union {
	volatile unsigned int v;
	struct {
		volatile unsigned int TTRSH                            :8;
		volatile unsigned int __RESVERD                        :24;
	} bits;
} SPI_HC_TX_THRESHOLD;

typedef union {
	volatile unsigned int v;
	struct {
		volatile unsigned int RTRSH                            :8;
		volatile unsigned int __RESVERD                        :24;
	} bits;
} SPI_HC_RX_THRESHOLD;

typedef union {
	volatile unsigned int v;
	struct {
		volatile unsigned int MID                              :32;
	} bits;
} SPI_HC_MODULE_ID; //default: 0x90108

struct spi_hc_regs {
	volatile SPI_HC_CONFIGURATION                 spi_hc_configuration    ;
	volatile SPI_HC_INTERRUPT_STATUS              spi_hc_interrupt_status ;
	volatile SPI_HC_INTERRUPT_ENABLE              spi_hc_interrupt_enable ;
	volatile SPI_HC_INTERRUPT_DISABLE             spi_hc_interrupt_disable;
	volatile SPI_HC_INTERRUPT_MASK                spi_hc_interrupt_mask   ;
	volatile SPI_HC_SPI_ENABLE                    spi_hc_spi_enable       ;
	volatile SPI_HC_DELAY                         spi_hc_delay            ;
	volatile SPI_HC_TRANSMIT_DATA                 spi_hc_transmit_data    ;
	volatile SPI_HC_RECEIVE_DATA                  spi_hc_receive_data     ;
	volatile SPI_HC_SLAVE_IDLE_COUNT              spi_hc_slave_idle_count ;
	volatile SPI_HC_TX_THRESHOLD                  spi_hc_tx_threshold     ;
	volatile SPI_HC_RX_THRESHOLD                  spi_hc_rx_threshold     ;
	volatile unsigned int                                  __pad[51]               ;
	volatile SPI_HC_MODULE_ID                     spi_hc_module_id        ;
};


#define SPI_REG_BASE (0x9000000000000000 | 0x1F0A0000)
#define SPI0_HC_REGS ((struct spi_hc_regs*)SPI_REG_BASE)
#define SPI1_HC_REGS ((struct spi_hc_regs*)(SPI_REG_BASE+0x8000))

#endif  /* __SPI_LIB_H__ */

