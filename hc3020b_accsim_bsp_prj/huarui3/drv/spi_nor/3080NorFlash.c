/*!************************************************************
 * @brief: nor flash底层操作接口
 * @author: cetc
 * @date: 
 * @version: 1.0.0
 * @note:
 * ------------------------------------------------------------
 * Change History:
 * <Date> | <Author> | <Description>
 * 
 **************************************************************/ 
#if 1

#include <stdio.h>
#include "../../h/tool/hr3Macro.h"
#include "../../h/tool/hr3SysTools.h"
#include "./spi_lib.h"
//#include "tffs/flflash.h"
//#include "tffs/backgrnd.h"

#define 	MAXSECTORS	1024	/* should work with any value *//*edited by syj 20140811*/
#define     ERROR_PARA_INPUT    -1  /* 参数输入错误 */
#define     ERROR_FUNC_EXEC     -2  /* 函数执行错误 */
#define     DRIVES      5
typedef enum
    {
    BOOTBLOCK_NONE,
    BOOTBLOCK_TOP,
    BOOTBLOCK_BOTTOM
    }BOOTBLOCK;

typedef union
    {
    u8 uchar[4];
    u32 uint32;
    } CFI_DWORD;

typedef union
    {
    u8 uchar[2];
    ushort ushort;
    } CFI_WORD;
    
/* Instance specific CFI data so that a system may have more than one CFI
 * device. */
typedef struct {

  unsigned         commandSetId;            /* id of a specific command set. */
  unsigned         altCommandSetId;         /* id of alternate command set.  */
  BOOL             wordMode;                /* TRUE - word mode.             */
					    /* FALSE - byte mode.            */
  int              multiplier;              /* the number of bytes between   */
					    /* 1st Q and 1st R in query	     */
  int		       interleaveWidth;	    /* 1 - byte 2 - word interleaved */
  unsigned         maxBytesWrite;           /* maximum number of bytes       */
					    /* in multi-byte write.          */
  BOOL             vpp;                     /* if = TRUE, need vpp.          */
  long             optionalCommands;        /* optional commands supported   */
					    /* (1 = yes, 0 = no):            */
					    /* bit 0 - chip erase.           */
					    /* bit 1 - suspend erase.        */
					    /* bit 2 - suspend write         */
					    /* bit 3 - lock/unlock.          */
					    /* bit 4 - queued erase.         */
  unsigned         afterSuspend;            /* functions supported after     */
					    /* suspend (1 = yes, 0 = no):    */
					    /* bit 0 - write after erase     */
					    /*         suspend.              */
  int 		   sectorDefs;		    /* Number of sector definitions  */
  struct {				    /* sector descriptions as found  */
      long 	   secSize;		    /* in CFI query. 		     */
      int 	   numSecs;		    /*				     */
  }secDesc[8];				    /* assume 8 is enough	     */
  struct {				    /*				     */
      long	   sectorSize;		    /* sector size 		     */
      u32	   sectorBaseAdrs;	    /* base address of sector	     */
  } secInfo[MAXSECTORS];		    /* per sector info		     */
  u32 unlockAddr1;			    /* offset for first unlock	     */
  u32 unlockAddr2;			    /* offset for 2nd unlock	     */
  int sectorsInCFI;			    /* sector count for device	     */
  int bootBlockSectors;			    /* sectors that makeup boot block*/
  BOOTBLOCK bootBlockType;		    /* Top, Bottom or None	     */
      
} CFI;

CFI mtdVars_spi[DRIVES];

/* defines */

/* Save the last erase block on each device discovered in the array 
 * for NVRAM */
#define SAVE_NVRAM_REGION

//#ifndef _BYTE_ORDER
//#error "Error: _BYTE_ORDER needs to be #define'd.  Try #include \"vxWorks.h\" "
//#endif

/* supported functions after suspend */
#define WRITE_AFTER_SUSPEND_SUPPORT  0x0001

#define thisCFI   ((CFI *)vol.mtdVars)

/*added by syj 20151103 begin*/
#define	SPI_FLASH_BASE_ADRS		0x9000000015000000
#define SEC_SIZE_IN_BYTE 		0x20000 /* 128KB */
#define SECTORNUM 		1016 /* 1MB for bootrom */ 

#define READID_CMD1 	0x0090

#define READCFI_CMD1 	0x0098

#define UNLOCK_CMD1 	0x00AA
#define UNLOCK_CMD2 	0x0055

#define ERASE_CMD1 		0x0020
#define ERASE_CMD2 		0x00D0

#define WRITE_CMD1 		0x00A0
#define READ_CMD1 		0x00FF

#define WRITEBUFFER_CMD1 0x0025
#define WRITEBUFFER_CMD2 0x0029

#define ADDR_LP64(addr)   ((addr & 0x80000000UL)?(0xffffffff00000000 | addr):(addr))

/*added by syj 20151103 end*/
unsigned int bPrintInfo = 0;
void setPrintInfo(unsigned int val)
{
	bPrintInfo = val;
}
/*
static void timeDelay(unsigned int loopNum)
{
	unsigned int i = 0;
	unsigned int j = 0;
	for(i = 0; i < loopNum; i ++)
	{
		j = j + 1;
	}
}
*/
static unsigned int xxFlashRead(unsigned long addr)
{
   unsigned short int val = 0;
   val =*((volatile unsigned int *)(addr));
   return val;
}
static void xxflashWrite(unsigned long addr, unsigned int val)
{
	*(volatile unsigned int *)addr =val;
}

int flashWrite(unsigned long addr, unsigned int val)
{
	volatile unsigned int flag = 0;
	
	xxflashWrite(addr, val);

    return 0;
}

int flashBufferWrite(unsigned long addr, unsigned int * buf,unsigned int wordNum)
{
    /*unsigned short int tmpBuf[128];*/
    unsigned int i = 0;
    unsigned int val ;

    //sectorAddr = SPI_FLASH_BASE_ADRS + ((addr - SPI_FLASH_BASE_ADRS) / 0x20000) * 0x20000;
    printf("test spi1\r\n");
    for(i = 0;i < wordNum; i++)
    {
        val = i;
        //xxflashWrite((unsigned int *)(addr + i * 4), val);
        *(volatile unsigned int *)(addr + i * 4) = val;
       printf("flashBufferWrite 0x%x\n", *(volatile unsigned int *)(addr+ i*4));
    }

    return 0;
}

int flashSectorErase(unsigned short sectorNum)
{
	unsigned int i = 0;

	for(i = 0; i < SEC_SIZE_IN_BYTE; i = i + 4)
	{
	    xxflashWrite(SPI_FLASH_BASE_ADRS+ sectorNum * SEC_SIZE_IN_BYTE + i,0xffffffff);
	}

  	return 0;
}


int test_norflash_write(unsigned long addr, void * p_buffer,unsigned int len)
{
    unsigned int i = 0;
    unsigned int bTrue = 1;
    unsigned int waitTime = 0;

    for(i = 0; i < len; i = i + 4)
    {
        xxflashWrite(addr + i, *((unsigned int *)((unsigned char*)p_buffer + i)));
    }
    return 0;
}

int test_norflash_sectorerase(unsigned int beginSector,unsigned int endSector)
{
    unsigned int bTrue = 1;
    unsigned int i = 0;
    unsigned int waitTime = 0;
    unsigned char * buffercmp;

    for (i = beginSector; i < endSector; i++)
    {
        flashSectorErase(i);
    }

    return 0;
}

void test_norflash_read(unsigned long offset, unsigned int intnum)
{
	volatile unsigned int val = 0;	
	int i = 0;
	
	unsigned int t1 = 0;
	unsigned int t2 = 0;
	t1 = tickGet();
	for(i=0;i<intnum; i++)
	{
	   val =*(volatile unsigned int *)(offset+ i*4);
	}
    t2 = tickGet();

    printf("t1 = %d,t2 = %d,t2 - t1 = %d\n",t1,t2,t2 - t1);
}


void test_norflash_bufferwrite(unsigned long addr,unsigned int length,void *pbuf)
{
//	unsigned short int buf[256];
	unsigned int i = 0;
//	for(i = 0; i < 256; i ++)
//	{
//		buf[i] = i;
//	}

	for(i = 0; i < length; i = i + 64)
	{
		flashBufferWrite(addr + i,pbuf,32);
	}
}




//兼容测试集程序包增加的接口
int hr3_norflash_erase_block(unsigned int start_addr, unsigned int data_length)
{
    unsigned int beginSector = 0;
    unsigned int dataSectorNum = 0;
    unsigned int endSector = 0;
    int i = 0;
    unsigned char * buffercmp;

    buffercmp = (unsigned char * )malloc(128*1024);
    memset(buffercmp, 0xff, 128*1024);
    beginSector = (start_addr / SEC_SIZE_IN_BYTE);
    dataSectorNum = (data_length / SEC_SIZE_IN_BYTE);
    endSector = beginSector + dataSectorNum;
    test_norflash_sectorerase(beginSector, endSector);

    return 0;
}

int hr3_write_nor(unsigned int start_addr, unsigned int data_length, void *p_buffer)
{
    unsigned long cAddr = 0 ;
    cAddr = start_addr| SPI_FLASH_BASE_ADRS;
    test_norflash_write(cAddr,p_buffer, data_length);
    return 0;
}

int	hr3_norflash_write(unsigned int start_addr, unsigned int data_length, void *p_buffer)
{
	hr3_write_nor(start_addr, data_length, p_buffer);
	return 0;
}


#endif

