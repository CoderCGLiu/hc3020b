/* ********************************************
 * FILE NAME  : srio_debug.h
 * PROGRAMMER : wangjf
 * START DATE : 2022-09-13 17:39:59
 * DESCIPTION : standalone
 * *******************************************/

#ifndef  __SRIO_DEBUG_H__
#define  __SRIO_DEBUG_H__

#ifndef u8
typedef unsigned char u8;
#endif
#ifndef u16
typedef unsigned short u16;
#endif
#ifndef u32
typedef unsigned int u32;
#endif
#ifndef u64
typedef unsigned long u64;
#endif

static union { char c[4]; unsigned long l;} endian_test = { {'l', '?', '?', 'b'}};
#define ENDIANNESS ((char)endian_test.l)

#define L2B32(little) (((little&0xff)<<24) | ((little&0xff00)<<8) | ((little&0xff0000)>>8) | ((little&0xff000000)>>24))

#define NULL	((void*)0)

// ============================================================using which uart
#define USING_UART0

// ============================================================wait if
#define WAIT_LOG
#ifdef WAIT_LOG
#define wait_udebug udebug
#define wait_uclean uclean
#define wait_always uclean
#else
#define wait_udebug(fmt, args...) do{;}while(0)
#define wait_uclean(fmt, args...) do{;}while(0)
#define wait_always uclean
#endif

#define wait_if(expr, cnt, desc) ({\
		wait_udebug("wait_if[%s][%d]", desc, cnt); \
		int _i = 0; \
		for (;_i < cnt; _i++) { if (!(expr)) break; if (0 == _i%100) wait_always(".");} \
		wait_uclean("->[%d]\r\n", _i); \
		_i;\
		})


// ============================================================wait if

// ============================================================log
#define LEVEL_DEBUG   0
#define LEVEL_INFO    1
#define LEVEL_ERROR   2
#define LEVEL_SIMPLE  3
#define ULOG_LEVEL    LEVEL_SIMPLE

// ============================================================log


#define SWAP_U32(x)                    \
            ((u32)((((u32)(x) & 0x000000FFU) << 24) |    \
            (((u32)(x) & 0x0000FF00U) <<  8) |           \
            (((u32)(x) & 0x00FF0000U) >>  8) |           \
            (((u32)(x) & 0xFF000000U) >> 24)))

// ============================================================read write Transfer
#define _P2V(addr)		(0x9000000000000000 | ((u64)(addr)))	/* physical address to virtual address */
#define _P2V_CACHE(addr)	(0xb000000000000000 | ((u64)(addr)))	/* physical address to virtual address */

#define READ8_REGISTER(x, y) do {			\
	y= *((volatile u8 *)(_P2V(x))) ;	\
} while(0)
#define READ16_REGISTER(x, y) do {			\
	y= *((volatile u16 *)(_P2V(x))) ;	\
} while(0)
#define READ32_REGISTER(x, y) do {			\
	y = *((volatile u32*)(_P2V(x)));	\
} while(0)
#define READ64_REGISTER(x, y) do {			\
	y = *((volatile u64 *)(_P2V(x)));	\
} while(0)
#define WRITE8_REGISTER(x, y) do {			\
	*((volatile u8 *)(_P2V(x)))= (u8)y ;	\
} while(0)
#define WRITE16_REGISTER(x, y) do {			\
	*((volatile u16 *)(_P2V(x)))= (u16)y ;	\
} while(0)
#define WRITE32_REGISTER(x, y) do {			\
	*((volatile u32 *)(_P2V(x))) = y;	\
} while(0)
#define WRITE64_REGISTER(x, y) do {			\
	*((volatile u64 *)(_P2V(x))) = y;	\
} while(0)

#define READ8_CACHE_REGISTER(x, y) do {			\
	y= *((volatile u8 *)(_P2V_CACHE(x))) ;	\
} while(0)
#define READ16_CACHE_REGISTER(x, y) do {			\
	y= *((volatile u16 *)(_P2V_CACHE(x))) ;	\
} while(0)
#define READ32_CACHE_REGISTER(x, y) do {			\
	y = *((volatile u32*)(_P2V_CACHE(x)));	\
} while(0)
#define READ64_CACHE_REGISTER(x, y) do {			\
	y = *((volatile u64 *)(_P2V_CACHE(x)));	\
} while(0)
#define WRITE8_CACHE_REGISTER(x, y) do {			\
	*((volatile u8 *)(_P2V_CACHE(x)))= (u8)y ;	\
} while(0)
#define WRITE16_CACHE_REGISTER(x, y) do {			\
	*((volatile u16 *)(_P2V_CACHE(x)))= (u16)y ;	\
} while(0)
#define WRITE32_CACHE_REGISTER(x, y) do {			\
	*((volatile u32 *)(_P2V_CACHE(x))) = y;	\
} while(0)
#define WRITE64_CACHE_REGISTER(x, y) do {			\
	*((volatile u64 *)(_P2V_CACHE(x))) = y;	\
} while(0)

#define phx_read_u8(where)						\
	({                                                              \
	 u8 val;                                        \
	 READ8_REGISTER(where, val);				\
	 val;                                                    \
	 })
#define phx_read_u16(where)						\
	({                                                              \
	 u16 val;                                      \
	 READ16_REGISTER(where, val);				\
	 val;                                                    \
	 })
#define phx_read_u32(where)						\
	({                                                              \
	 volatile u32 val;                                       \
	 READ32_REGISTER(where, val);				\
	 val;                                                    \
	 })
#define phx_read_u64(where)						\
	({                                                              \
	 volatile u64 val;                                       \
	 READ64_REGISTER(where, val);				\
	 val;                                                    \
	 })

#define phx_write_u8(where, val)   WRITE8_REGISTER(where, val)
#define phx_write_u16(where, val)  WRITE16_REGISTER(where, val)
#define phx_write_u32(where, val)  WRITE32_REGISTER(where, val)
#define phx_write_u64(where, val)  WRITE64_REGISTER(where, val)
#define phx_write_mask_bit(where, mask, val)				\
	({								\
	 volatile u32 tmp;					\
	 tmp = ((phx_read_u32(where)) & (~(mask))) | ((val) & (mask));\
	 phx_write_u32(where, tmp);				\
	 })

#define phx_read_cache_u8(where)						\
	({                                                              \
	 u8 val;                                        \
	 READ8_CACHE_REGISTER(where, val);				\
	 val;                                                    \
	 })
#define phx_read_cache_u16(where)						\
	({                                                              \
	 u16 val;                                      \
	 READ16_CACHE_REGISTER(where, val);				\
	 val;                                                    \
	 })
#define phx_read_cache_u32(where)						\
	({                                                              \
	 volatile u32 val;                                       \
	 READ32_CACHE_REGISTER(where, val);				\
	 val;                                                    \
	 })
#define phx_read_cache_u64(where)						\
	({                                                              \
	 volatile u64 val;                                       \
	 READ64_CACHE_REGISTER(where, val);				\
	 val;                                                    \
	 })

#define phx_write_cache_u8(where, val)   WRITE8_CACHE_REGISTER(where, val)
#define phx_write_cache_u16(where, val)  WRITE16_CACHE_REGISTER(where, val)
#define phx_write_cache_u32(where, val)  WRITE32_CACHE_REGISTER(where, val)
#define phx_write_cache_u64(where, val)  WRITE64_CACHE_REGISTER(where, val)

#define uart_printf printk
#endif  /* __SRIO_DEBUG_H__ */
