
#include <stdio.h>
#include <semLib.h>
#include <semaphore.h>

#include "../srio.h"


extern unsigned int bslProcGetId();
extern int dbgWriteLog(int bPrint,const char *format,...);
extern void bslRioMaintRead(UINT8 controller,UINT16 dstId,UINT8 hopcount, UINT32 offset, void  *rddata);
extern void bslRioSetMaintOW(u8 controller);
extern unsigned int sysCpuGetID();
extern int getSrioSwAccFlag(void);
extern unsigned int getDebugStatus(void);
extern int getSrioMaAccFlag(void);

extern int bslI2CMTransData(unsigned int devIndex,unsigned int transType,unsigned char* ddrAddr,unsigned int length,unsigned int slaveId,unsigned int memAddr);
void spy1848Lane();

#define SWAP_32(x)					\
        ((u32)((((u32)(x) & 0x000000FFU) << 24) |	\
	       (((u32)(x) & 0x0000FF00U) <<  8) |	\
	       (((u32)(x) & 0x00FF0000U) >>  8) |	\
	       (((u32)(x) & 0xFF000000U) >> 24)))

extern void delay_cnt(unsigned int count);
extern unsigned int get_1848_reg(unsigned int offset);
extern void set_1848_reg(unsigned int offset,unsigned int data);

#if _RIO_SEM_REWORKS_
sem_t accSrioSwSem;
#else
SEM_ID accSrioSwSem;
#endif

#define DEV1848ADDR (0x3)

int m_RioInit = 0;
int m_RioStatus = 0;

/*
 * A-rab0对应1848port-1				B-rab0对应1848port-5				C-rab0对应1848port-7				D-rab0对应1848port-8			
 * A-rab1对应1848port-9				B-rab1对应1848port-2				C-rab1对应1848port-0				D-rab1对应1848port-4			
 * 
 */

int getSrioMaAccFlag(void)
{
    return 0;
}

void accSrioSwInit(void)
{
#if _RIO_SEM_REWORKS_
    sem_init2(&accSrioSwSem, 0, SEM_BINARY, PTHREAD_WAITQ_FIFO, 1);
#else
    accSrioSwSem = semBCreate(SEM_Q_FIFO, SEM_FULL);
#endif /*_RIO_SEM_REWORKS_*/
    if(accSrioSwSem ==0)
    {
        printf("accSrioSwSem init failed!\n");
        return;
    }
}

unsigned int get_1848_reg(unsigned int offset)
{
    u32 _data;
    u16 _slave1848 = DEV1848ADDR;
    u32 len = 0;

    if(getSrioMaAccFlag() == 1)
    {
        bslRioMaintRead(0,0xff,0,offset,&_data);
        return _data;
    }
    else
    {
#if _RIO_SEM_REWORKS_
		sem_wait2(&accSrioSwSem, WAIT, NO_TIMEOUT);
#else
        semTake(accSrioSwSem, WAIT_FOREVER);
#endif /*_RIO_SEM_REWORKS_*/
        bslI2CMTransData(0, 0, (u8*)&_data, 4, DEV1848ADDR, (offset >> 2));
//        printf("1848:offset = 0x%x, data = 0x%x\n",offset,SWAP_32(_data));
#if _RIO_SEM_REWORKS_
        sem_post(&accSrioSwSem);
#else
        semGive(accSrioSwSem);
#endif /*_RIO_SEM_REWORKS_*/
        return SWAP_32(_data);
    }
}

void set_1848_reg(unsigned int offset,unsigned int data)
{
    u32 _data;
    u16 _slave1848 = DEV1848ADDR;

    if(getSrioMaAccFlag() == 1)
    {
        bslRioMaintWrite(0,0xff,0,offset,data);
    }
    else
    {
        _data = SWAP_32(data);
#if _RIO_SEM_REWORKS_
		sem_wait2(&accSrioSwSem, WAIT, NO_TIMEOUT);
#else
        semTake(accSrioSwSem, WAIT_FOREVER);
#endif /*_RIO_SEM_REWORKS_*/
        
        bslI2CMTransData(0, 1, (u8*)&_data, 4, DEV1848ADDR, (offset >> 2));
        
#if _RIO_SEM_REWORKS_
        sem_post(&accSrioSwSem);
#else
        semGive(accSrioSwSem);
#endif /*_RIO_SEM_REWORKS_*/
    }
}

void reset_1848_device(void)
{
	set_1848_reg(0XF20040, SWAP_32(0x30097));
}

void reset_1848_port(int port_num)
{
	u32 reset_ctl,_data;

	_data = get_1848_reg(0xf2000c);
	_data = _data | 0x1;
	set_1848_reg(0xf2000c,_data);
	
	reset_ctl = (1 << 31)|(1 << port_num);
	set_1848_reg(0xF20300, reset_ctl);
}

u32 read_1848_status(u8 port_num)
{
	u32 tmp = get_1848_reg(0x158+0x20*port_num);	
	return tmp;
}

void set_1848_route(u8 port_num, u16 dest_id)
{
	set_1848_reg(0xE00000 + 0x4 * port_num, dest_id);
}


void disable_1848_lane(u8 lane_num)
{
	u32 tmp;
	tmp = get_1848_reg(0xFF8000ul + (0x100 * lane_num));
	set_1848_reg(0xFF8000ul + (0x100 * lane_num), tmp | 1);
}

void enable_1848_lane(u8 lane_num)
{
	u32 tmp;
	tmp = get_1848_reg(0xFF8000ul + (0x100 * lane_num));
	//DBPRINT("FF8000 %x\r\n", tmp);
	set_1848_reg(0xFF8000ul + (0x100 * lane_num), (tmp & 0xfffffffeul) /*| (32 << 5)*/);
}

void enable_1848_port(u8 port_num)
{
	u32 tmp;
	tmp = get_1848_reg(0x15c + (0x20 * port_num));
	set_1848_reg(0x15c + (0x20 * port_num), tmp | (0x3 << 21));
}

void repair_1848_port(u8 port_num)
{
	u32 offset,val;	
	
	//recover failed link
	offset = 0x148+port_num*0x20;
	val = get_1848_reg(offset);
	val |= 0x80000000;
	set_1848_reg(offset,val);
	delay_cnt(0xfffff);
	set_1848_reg(offset,0);
	delay_cnt(0xfffff);
	offset = 0x140+port_num*0x20;
	set_1848_reg(offset,0x4);
	delay_cnt(0xfffff);
	offset = 0x158+port_num*0x20;
	val = get_1848_reg(offset);
	set_1848_reg(offset,val);
	delay_cnt(0xfffff);
	val = get_1848_reg(offset);
	//pprintf(5,"1848:read port %d again, value=0x%x\n\r",port_num,val);
}

void iic_1848PortCountEnable(void)
{
	int i;
	unsigned int offset;
	u8 portCnt = 12;	//只有12路X4

	for( i=0; i<portCnt; i++)
	{
		offset = 0xf40004+i*0x100;
		set_1848_reg(offset,0x6400000);
	}
}

void iic_1848_init(void)
{
	int i;
	unsigned int val;
	unsigned int offset = 0;
	static short _errorInit = 0;
	
	if( _errorInit == 0 )
	{
		_errorInit = 1;
		
		//dbgWriteLog(0,"[DSP%d]i2c read 0x%x.\r\n",bslProcGetId(),get_1848_reg(0));
		
		//将port_rst_ctl置上1
		//这样1848端口在收到control symbol之后，
		//就仅会对端口进行复位，而不是整个1848芯片进行复位
		//dbgWriteLog(0,"[DSP%d]i2c init 1848 reset reg.\r\n",bslProcGetId());
		val = get_1848_reg(0xf2000c);
		val |= 0x1;
		set_1848_reg(0xf2000c,val);
			
		//dbgWriteLog(0,"[DSP%d]i2c init 1848 counter reg.\r\n",bslProcGetId());
		iic_1848PortCountEnable();		
		
		
		for( i=0; i<12; i++)
		{
			offset = 0x1044+i*0x40;
			set_1848_reg(offset,0x807e8037);
		}
		
		//dbgWriteLog(0,"[DSP%d]i2c clear 1848 reg.\r\n",bslProcGetId());
		for( i=0; i<12; i++)
		{
			offset = 0x1040+i*0x40;
			set_1848_reg(offset,0);
			val = get_1848_reg(offset);	//just for clear
			
			offset = 0xf40008+i*0x100;
			set_1848_reg(offset,0);
			val = get_1848_reg(offset);	//just for clear
		}
		
		for( i=0; i<48; i++)
		{
			offset = 0xff8010+i*0x100;
			set_1848_reg(offset,0x1fff);
			
			offset = 0xff800c+i*0x100;
			set_1848_reg(offset,0);
			val = get_1848_reg(offset);	//just for clear
			
			offset = 0x2010+i*0x20;
			set_1848_reg(offset,0);
			val = get_1848_reg(offset);	//just for clear
		}
		
		//dbgWriteLog(0,"[DSP%d]i2c init 1848 OK.\r\n",bslProcGetId());
	}
}
void spy1848Port(void)
{

}

#ifdef QUAD_VPX
void spy1848Port()
{	
	unsigned int pdata[8];
	unsigned int offset = 0;
	int i;			
	
	//iic_1848_init();
	printf("-------------------------------------------------------------------------------------------------------------------------\n");
	printf("A-rab0对应port-9		B-rab0对应port-2		C-rab0对应port-0		D-rab0对应port-4\n");
	printf("\r\n");
	printf("A-rab1对应port-1		B-rab1对应port-5		C-rab1对应port-7		D-rab1对应port-8\n");
	printf("\r\n");
	
	printf("--------------------------------------------------1848 - TX---------------------------------------------------------------\n");
	printf("port      pstatus         pa               pna            retrysymbol        alltx            droptx            pwidth\n");
												
	for( i=0; i<12; i++)
	{				
		offset = 0x158+i*0x20;
		pdata[0] = get_1848_reg(offset);
		
		offset = 0xf40010+i*0x100;
		pdata[1] = get_1848_reg(offset);				

		offset = 0xf40014+i*0x100;
		pdata[2] = get_1848_reg(offset);

		offset = 0xf40018+i*0x100;
		pdata[3] = get_1848_reg(offset);

		offset = 0xf4001c+i*0x100;
		pdata[4] = get_1848_reg(offset);

		offset = 0xf40068+i*0x100;
		pdata[5] = get_1848_reg(offset);
		
		offset = 0x15c+i*0x20;
		pdata[6] = get_1848_reg(offset);

		printf("0x%.2x     0x%.8x      0x%.8x      0x%.8x       0x%.8x         0x%.8x       0x%.8x       0x%.8x\n",i,pdata[0],pdata[1],pdata[2],pdata[3],pdata[4],pdata[5],pdata[6]);
	}
	
	printf("\n");
	

	printf("--------------------------------------------------1848 - RX---------------------------------------------------------------\n");
	
	printf("port      pstatus         pa               pna            retrysymbol        allrx            droprx            pwidth\n");
				
	for( i=0; i<12; i++)
	{				
		offset = 0x158+i*0x20;
		pdata[0] = get_1848_reg(offset);
		
		offset = 0xf40040+i*0x100;
		pdata[1] = get_1848_reg(offset);				

		offset = 0xf40044+i*0x100;
		pdata[2] = get_1848_reg(offset);

		offset = 0xf40048+i*0x100;
		pdata[3] = get_1848_reg(offset);

		offset = 0xf40050+i*0x100;
		pdata[4] = get_1848_reg(offset);

		offset = 0xf40064+i*0x100;
		pdata[5] = get_1848_reg(offset);
		
		offset = 0x15c+i*0x20;
		pdata[6] = get_1848_reg(offset);

		printf("0x%.2x     0x%.8x      0x%.8x      0x%.8x       0x%.8x         0x%.8x       0x%.8x       0x%.8x\n",i,pdata[0],pdata[1],pdata[2],pdata[3],pdata[4],pdata[5],pdata[6]);
	}
	
	printf("\n");
}
#elif defined QUAD_XYG

void spy1848Port()
{	
	unsigned int pdata[8];
	unsigned int offset = 0;
	int i;			
	
//	iic_1848_init();
//	if(getDebugStatus() == 0 || getSrioSwAccFlag() == 0)
//	{
//	    if(sysCpuGetID() != 0)
//	    {
//	        printf("pls debug_on() at DSP A and current DSP , wait 5s!!!!\n");
//	    }
//	    else
//	    {
//	        printf("pls debug_on() at DSP A and wait 5s!\n");
//	    }
//	    return;
//	}

	printf("-------------------------------------------------------------------------------------------------------------------------\n");
	printf("A-rab0对应port-2		B-rab0对应port-9		C-rab0对应port-4		D-rab0对应port-a\n");
	printf("\r\n");
	printf("A-rab1对应port-5		B-rab1对应port-1		C-rab1对应port-8		D-rab1对应port-6\n");
	printf("\r\n");
	
	printf("--------------------------------------------------1848 - TX---------------------------------------------------------------\n");
	printf("port      pstatus         pa               pna            retrysymbol        alltx            droptx            pwidth\n");
												
	for( i=0; i<12; i++)
	{				
		offset = 0x158+i*0x20;
		pdata[0] = get_1848_reg(offset);
		
		offset = 0xf40010+i*0x100;
		pdata[1] = get_1848_reg(offset);				

		offset = 0xf40014+i*0x100;
		pdata[2] = get_1848_reg(offset);

		offset = 0xf40018+i*0x100;
		pdata[3] = get_1848_reg(offset);

		offset = 0xf4001c+i*0x100;
		pdata[4] = get_1848_reg(offset);

		offset = 0xf40068+i*0x100;
		pdata[5] = get_1848_reg(offset);
		
		offset = 0x15c+i*0x20;
		pdata[6] = get_1848_reg(offset);

		printf("0x%.2x     0x%.8x      0x%.8x      0x%.8x       0x%.8x         0x%.8x       0x%.8x       0x%.8x\n",i,pdata[0],pdata[1],pdata[2],pdata[3],pdata[4],pdata[5],pdata[6]);
	}
	
	printf("\n");
	

	printf("--------------------------------------------------1848 - RX---------------------------------------------------------------\n");
	
	printf("port      pstatus         pa               pna            retrysymbol        allrx            droprx            pwidth\n");
				
	for( i=0; i<12; i++)
	{				
		offset = 0x158+i*0x20;
		pdata[0] = get_1848_reg(offset);
		
		offset = 0xf40040+i*0x100;
		pdata[1] = get_1848_reg(offset);				

		offset = 0xf40044+i*0x100;
		pdata[2] = get_1848_reg(offset);

		offset = 0xf40048+i*0x100;
		pdata[3] = get_1848_reg(offset);

		offset = 0xf40050+i*0x100;
		pdata[4] = get_1848_reg(offset);

		offset = 0xf40064+i*0x100;
		pdata[5] = get_1848_reg(offset);
		
		offset = 0x15c+i*0x20;
		pdata[6] = get_1848_reg(offset);

		printf("0x%.2x     0x%.8x      0x%.8x      0x%.8x       0x%.8x         0x%.8x       0x%.8x       0x%.8x\n",i,pdata[0],pdata[1],pdata[2],pdata[3],pdata[4],pdata[5],pdata[6]);
	}
	
	printf("\n");
}

#elif defined QUAD_IPC

void spy1848Port()
{
    unsigned int pdata[8];
    unsigned int offset = 0;
    int i;

    //iic_1848_init();
    printf("-------------------------------------------------------------------------------------------------------------------------\n");
    printf("A-rab0对应port-2      B-rab0对应port-9      C-rab0对应port-4      D-rab0对应port-a\n");
    printf("\r\n");
    printf("A-rab1对应port-5      B-rab1对应port-1      C-rab1对应port-8      D-rab1对应port-6\n");
    printf("\r\n");

    printf("--------------------------------------------------1848 - TX---------------------------------------------------------------\n");
    printf("port      pstatus         pa               pna            retrysymbol        alltx            droptx            pwidth\n");

    for( i=0; i<12; i++)
    {
        offset = 0x158+i*0x20;
        pdata[0] = get_1848_reg(offset);

        offset = 0xf40010+i*0x100;
        pdata[1] = get_1848_reg(offset);

        offset = 0xf40014+i*0x100;
        pdata[2] = get_1848_reg(offset);

        offset = 0xf40018+i*0x100;
        pdata[3] = get_1848_reg(offset);

        offset = 0xf4001c+i*0x100;
        pdata[4] = get_1848_reg(offset);

        offset = 0xf40068+i*0x100;
        pdata[5] = get_1848_reg(offset);

        offset = 0x15c+i*0x20;
        pdata[6] = get_1848_reg(offset);

        printf("0x%.2x     0x%.8x      0x%.8x      0x%.8x       0x%.8x         0x%.8x       0x%.8x       0x%.8x\n",i,pdata[0],pdata[1],pdata[2],pdata[3],pdata[4],pdata[5],pdata[6]);
    }

    printf("\n");


    printf("--------------------------------------------------1848 - RX---------------------------------------------------------------\n");

    printf("port      pstatus         pa               pna            retrysymbol        allrx            droprx            pwidth\n");

    for( i=0; i<12; i++)
    {
        offset = 0x158+i*0x20;
        pdata[0] = get_1848_reg(offset);

        offset = 0xf40040+i*0x100;
        pdata[1] = get_1848_reg(offset);

        offset = 0xf40044+i*0x100;
        pdata[2] = get_1848_reg(offset);

        offset = 0xf40048+i*0x100;
        pdata[3] = get_1848_reg(offset);

        offset = 0xf40050+i*0x100;
        pdata[4] = get_1848_reg(offset);

        offset = 0xf40064+i*0x100;
        pdata[5] = get_1848_reg(offset);

        offset = 0x15c+i*0x20;
        pdata[6] = get_1848_reg(offset);

        printf("0x%.2x     0x%.8x      0x%.8x      0x%.8x       0x%.8x         0x%.8x       0x%.8x       0x%.8x\n",i,pdata[0],pdata[1],pdata[2],pdata[3],pdata[4],pdata[5],pdata[6]);
    }

    printf("\n");
}

#endif


void spy1848Lane(void)
{
	int i;
	unsigned int pdata[10];
	unsigned int offset = 0;	
		
	printf("--------------------------------------------------1848 - LANE---------------------------------------------------------------\n");
	printf("          lane0           lane1           lane2           lane3           lane4           lane5           lane6           lane7\n");

	for( i=0; i<8; i++)
	{
		offset = 0x2010+i*0x20;
		pdata[i] = get_1848_reg(offset);
	}
	printf("0x2010    0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x\n",pdata[0],pdata[1],pdata[2],pdata[3],pdata[4],pdata[5],pdata[6],pdata[7]);

	for( i=0; i<8; i++)
	{
		offset = 0xff800c+i*0x100;
		pdata[i] = get_1848_reg(offset);
	}
	printf("0xff800c  0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x\n\n",pdata[0],pdata[1],pdata[2],pdata[3],pdata[4],pdata[5],pdata[6],pdata[7]);

	printf("          lane8           lane9           lane10          lane11          lane12          lane13           lane14          lane15\n");

	for( i=0; i<8; i++)
	{
		offset = 0x2010+(i+8)*0x20;
		pdata[i] = get_1848_reg(offset);
	}
	printf("0x2010    0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x\n",pdata[0],pdata[1],pdata[2],pdata[3],pdata[4],pdata[5],pdata[6],pdata[7]);

	for( i=0; i<8; i++)
	{
		offset = 0xff800c+(i+8)*0x100;
		pdata[i] = get_1848_reg(offset);
	}
	printf("0xff800c  0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x\n\n",pdata[0],pdata[1],pdata[2],pdata[3],pdata[4],pdata[5],pdata[6],pdata[7]);

	printf("          lane16           lane17          lane18          lane19          lane20          lane21           lane22          lane23\n");
	for( i=0; i<8; i++)
	{
		offset = 0x2010+(i+16)*0x20;
		pdata[i] = get_1848_reg(offset);
	}
	printf("0x2010    0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x\n",pdata[0],pdata[1],pdata[2],pdata[3],pdata[4],pdata[5],pdata[6],pdata[7]);

	for( i=0; i<8; i++)
	{
		offset = 0xff800c+(i+16)*0x100;
		pdata[i] = get_1848_reg(offset);
	}
	printf("0xff800c  0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x\n\n",pdata[0],pdata[1],pdata[2],pdata[3],pdata[4],pdata[5],pdata[6],pdata[7]);

	printf("          lane24           lane25          lane26          lane27          lane28          lane29           lane30          lane31\n");
	for( i=0; i<8; i++)
	{
		offset = 0x2010+(i+24)*0x20;
		pdata[i] = get_1848_reg(offset);
	}
	printf("0x2010    0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x\n",pdata[0],pdata[1],pdata[2],pdata[3],pdata[4],pdata[5],pdata[6],pdata[7]);

	for( i=0; i<8; i++)
	{
		offset = 0xff800c+(i+24)*0x100;
		pdata[i] = get_1848_reg(offset);
	}
	printf("0xff800c  0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x\n\n",pdata[0],pdata[1],pdata[2],pdata[3],pdata[4],pdata[5],pdata[6],pdata[7]);


	printf("          lane32           lane33          lane34          lane35          lane36          lane37           lane38          lane39\n");
	for( i=0; i<8; i++)
	{
		offset = 0x2010+(i+32)*0x20;
		pdata[i] = get_1848_reg(offset);
	}
	printf("0x2010    0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x\n",pdata[0],pdata[1],pdata[2],pdata[3],pdata[4],pdata[5],pdata[6],pdata[7]);

	for( i=0; i<8; i++)
	{
		offset = 0xff800c+(i+32)*0x100;
		pdata[i] = get_1848_reg(offset);
	}
	printf("0xff800c  0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x\n\n",pdata[0],pdata[1],pdata[2],pdata[3],pdata[4],pdata[5],pdata[6],pdata[7]);


	printf("          lane40           lane41          lane42          lane43          lane44          lane45           lane46          lane47\n");
	for( i=0; i<8; i++)
	{
		offset = 0x2010+(i+40)*0x20;
		pdata[i] = get_1848_reg(offset);
	}
	printf("0x2010    0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x\n",pdata[0],pdata[1],pdata[2],pdata[3],pdata[4],pdata[5],pdata[6],pdata[7]);

	for( i=0; i<8; i++)
	{
		offset = 0xff800c+(i+40)*0x100;
		pdata[i] = get_1848_reg(offset);
	}
	printf("0xff800c  0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x      0x%.8x\n\n",pdata[0],pdata[1],pdata[2],pdata[3],pdata[4],pdata[5],pdata[6],pdata[7]);


	printf("\n");

	printf("--------------------------------------------------1848 - PORT---------------------------------------------------------------\n");

	printf("port      ERRCSR(0x1040)         SPECERR(0xF40008) \n");

	for( i=0; i<12; i++)
	{
		offset = 0x1040+i*0x40;
		pdata[0] = get_1848_reg(offset);

		offset = 0xF40008+i*0x100;
		pdata[1] = get_1848_reg(offset);

		printf("0x%.2x     0x%.8x              0x%.8x\n",i,pdata[0],pdata[1]);
	}

}

int spyRIOInfo()
{
	int i;
	u16 _port;	
	u32 _port1848[8],_data[11];
	float _speed;	
	
	//printf("tstRioInit Results: PASS\n");
	/*
	printf("A-rab0对应port-1		B-rab0对应port-5		C-rab0对应port-7		D-rab0对应port-8\n");
	printf("A-rab0对应ID - 1		B-rab0对应ID - 3		C-rab0对应ID - 5		D-rab0对应ID - 7\n");
	printf("\r\n");
	printf("A-rab1对应port-9		B-rab1对应port-2		C-rab1对应port-0		D-rab1对应port-4\n");
	printf("A-rab1对应ID - 2		B-rab1对应ID - 4		C-rab1对应ID - 6		D-rab1对应ID - 8\n");
	printf("\r\n");
	printf("TOTAL: 8\r\n");*/		
	
	
	//get rab0 - 1848 port number
	bslRioSetMaintOW(0);
	bslRioMaintRead(0,0xff,0,0x14,&_port);
	_port = _port&0xff;	
	_port1848[0] = _port&0xff;
	//get rab1 - 1848 port number
	bslRioSetMaintOW(1);
	bslRioMaintRead(1,0xff,0,0x14,&_port);
	_port = _port&0xff;
	_port1848[1] = _port&0xff;	
		
	//printf("--------------------------------------------------------------------------\r\n");
	printf("Port    L_ES       L_C       L_S \r\n");
	for(i=0;i<2;i++)
	{				
		_data[0] = SWAP_32(rab_page_read(i,0x158));
		_data[1] = SWAP_32(rab_page_read(i,0x15c));
		
		_data[2] = rab_page_read(i,0x20980);
		_data[3] = rab_page_read(i,0x20988);
		
		if( _data[3] == 0xf)
		{
			if( (_data[2] & 0x2800) == 0x2800 )
			{
				_speed = 3.125;				
			}
			else if( (_data[2] & 0x4000) == 0x4000 )
			{
				_speed = 5.0;
			}
			else if( (_data[2] & 0x2000) == 0x2000 )
			{
				_speed = 2.5;
			}
			else
			{
				_speed = 0.00;
			}
		}
		else if( _data[3] == 0 )
		{
			_speed = 6.25;
		}
		else
		{
			_speed = 0.00;
		}
		
		printf(" %02d    %.8x  %.8x   %.3f \r\n",_port1848[i],_data[0],_data[1],_speed);
	}
	
	
	printf("\r\n");
		
	printf("Port    P_ES       P_C      P_S      P_S0_Lane0 | Lane1 |  Lane2 |  Lane3    P_S1_Lane0 | Lane1 |  Lane2 |  Lane3 \r\n");
	
	_port1848[0] = 1;	
	_port1848[1] = 9;
	_port1848[2] = 5;
	_port1848[3] = 2;
	_port1848[4] = 7;
	_port1848[5] = 0;
	_port1848[6] = 8;
	_port1848[7] = 4;
	
	for(i=0;i<8;i++)
	{
		_data[0] = get_1848_reg(0x158+_port1848[i]*0x20);
		_data[1] = get_1848_reg(0x15c+_port1848[i]*0x20);
		_data[2] = get_1848_reg(0xff0000+_port1848[i]*0x10);
		
		_data[3] = get_1848_reg(0x2010+(_port1848[i]*4)*0x20);
		_data[4] = get_1848_reg(0x2010+(_port1848[i]*4+1)*0x20);
		_data[5] = get_1848_reg(0x2010+(_port1848[i]*4+2)*0x20);
		_data[6] = get_1848_reg(0x2010+(_port1848[i]*4+3)*0x20);
		
		_data[7] = get_1848_reg(0x2014+(_port1848[i]*4)*0x20);
		_data[8] = get_1848_reg(0x2014+(_port1848[i]*4+1)*0x20);
		_data[9] = get_1848_reg(0x2014+(_port1848[i]*4+2)*0x20);
		_data[10] = get_1848_reg(0x2014+(_port1848[i]*4+3)*0x20);
		
		printf(" %02d    %.8x  %.8x %.8x   %.8x  %.8x  %.8x  %.8x  %.8x  %.8x  %.8x  %.8x\r\n",_port1848[i],_data[0],_data[1],_data[2],_data[3],_data[4],_data[5],
              _data[6],_data[7],_data[8],_data[9],_data[10]);
		
		if( (i+1)%2 == 0 )
			printf("\r\n");
	}
	
	printf("Port   P_C_Lane0 | Lane1 |  Lane2 |  Lane3      P_ED_Lane0 | Lane1 |  Lane2 |  Lane3 \r\n");
	for(i=0;i<8;i++)
	{				
		_data[0] = get_1848_reg(0xff8000+(_port1848[i]*4)*0x100);
		_data[1] = get_1848_reg(0xff8000+(_port1848[i]*4+1)*0x100);
		_data[2] = get_1848_reg(0xff8000+(_port1848[i]*4+2)*0x100);
		_data[3] = get_1848_reg(0xff8000+(_port1848[i]*4+3)*0x100);
		
		_data[4] = get_1848_reg(0xff800c+(_port1848[i]*4)*0x100);
		_data[5] = get_1848_reg(0xff800c+(_port1848[i]*4+1)*0x100);
		_data[6] = get_1848_reg(0xff800c+(_port1848[i]*4+2)*0x100);
		_data[7] = get_1848_reg(0xff800c+(_port1848[i]*4+3)*0x100);
		
		printf(" %02d    %.8x  %.8x  %.8x  %.8x   %.8x  %.8x  %.8x  %.8x \r\n",_port1848[i],_data[0],_data[1],_data[2],_data[3],_data[4],_data[5],_data[6],_data[7]);			  
		
		if( (i+1)%2 == 0 )
			printf("\r\n");
	}	
	
	return 1;
}

void spyRIOPktCount()
{	
	unsigned int pdata[15];
	unsigned int offset = 0;
	int i;				
	
	printf("port   Pkt_Rec  RPkt_Drop  PA_Tran    PNA_Tran   Retry_Tran  Pkt_Tran  PA_Rec     PNA_Rec    Retry_Rec   OPkt_Tran  TPkt_Drop  TTL_Drop  CRC_Drop \n");
	printf("\r\n");
	for( i=0; i<12; i++)
	{				
		offset = 0xf40050+i*0x100;
		pdata[0] = get_1848_reg(offset);
		
		offset = 0xf40064+i*0x100;
		pdata[1] = get_1848_reg(offset);
		
		offset = 0xf40010+i*0x100;
		pdata[2] = get_1848_reg(offset);
		
		offset = 0xf40014+i*0x100;
		pdata[3] = get_1848_reg(offset);
		
		offset = 0xf40018+i*0x100;
		pdata[4] = get_1848_reg(offset);
		
		offset = 0xf4001c+i*0x100;
		pdata[5] = get_1848_reg(offset);
		
		offset = 0xf40040+i*0x100;
		pdata[6] = get_1848_reg(offset);
		
		offset = 0xf40044+i*0x100;
		pdata[7] = get_1848_reg(offset);
		
		offset = 0xf40048+i*0x100;
		pdata[8] = get_1848_reg(offset);
		
		offset = 0xf4004c+i*0x100;
		pdata[9] = get_1848_reg(offset);
		
		offset = 0xf40068+i*0x100;
		pdata[10] = get_1848_reg(offset);
		
		offset = 0xf4006c+i*0x100;
		pdata[11] = get_1848_reg(offset);
		
		offset = 0xf40070+i*0x100;
		pdata[12] = get_1848_reg(offset);
		
		
		
		printf("0x%.2x 0x%.8x 0x%.8x 0x%.8x 0x%.8x 0x%.8x 0x%.8x 0x%.8x 0x%.8x 0x%.8x 0x%.8x 0x%.8x 0x%.8x 0x%.8x\n",i,pdata[0],pdata[1],pdata[2],
				pdata[3],pdata[4],pdata[5],pdata[6],pdata[7],pdata[8],pdata[9],pdata[10],pdata[11],pdata[12]);
		
		printf("\r\n");
	}		
}

void bsl1848PortClear(void)
{
	int i;
	unsigned int offset;
	u8 portCnt = 12;

	printf("Switch --------Clear port !\n");

	for( i=0; i<portCnt; i++)
	{
		offset = 0x1040+i*0x40;
		set_1848_reg(offset,0);
		offset = 0xff800c+i*0x100;
		set_1848_reg(offset,0);
		offset = 0xf40008+i*0x100;
		set_1848_reg(offset,0);
	}
}

//speedIndex-- 0--3.125G;	1--5.0G;		2--6.25G
void bslChange1848Speed(unsigned int speedIndex)
{
	unsigned int _data;
	unsigned short _portNum;
	unsigned short _laneNum;	
	float _speed;
	int i,j;

	if( speedIndex > 2 )
	{
		printf("----iic_ChangeSpeed:: speed index error----\r\n");
		return ;
	}

	if( speedIndex == 0 )
		_speed = 3.125;
	else if( speedIndex == 1 )
		_speed = 5.0;
	else
		_speed = 6.25;	

	
	printf( "stage 1 disable ports.\n\r" );
	for(i=0;i<12;i++)
	{		
		if( (i!=0) && (i!=1) )
			continue;
		
		_portNum = i;
		_data = get_1848_reg(0x15c+_portNum*0x20);
		//xil_printf( "stage1:set before port-%d status = 0x%x.\n\r",_portNum,_data);
		set_1848_reg(0x15c+_portNum*0x20,_data|0x800000);
		//_data = get_1848_reg(0x15c+_portNum*0x20);
		//xil_printf( "stage1:now port-%d status = 0x%x.\n\r",_portNum,_data);
	}

	printf( "stage 2 change pll selection.\n\r" );
	for(i=0;i<12;i++)
	{		
		if( (i!=0) && (i!=1) )
			continue;
		
		_portNum = i;
		_data = get_1848_reg(0xff0000+_portNum*0x10);

		if( speedIndex == 1 )		//5.0G
		{
			_data = _data&0xfe;
		}
		else 		//3.125G/6.25G
		{
			_data = _data|0x1;
		}
		set_1848_reg(0xff0000+_portNum*0x10,_data);
	}

	printf( "stage 3 set %.3fGbaud lane rate.\n\r",_speed);
	for(i=0;i<12;i++)
	{
		if( (i!=0) && (i!=1) )
			continue;
		
		_portNum = i;
		for(j=0;j<4;j++)
		{
			_laneNum = _portNum*4+j;
			_data = get_1848_reg(0xff8000+_laneNum*0x100);
			_data = _data&0xffffffe1;

			if( speedIndex == 1 || speedIndex == 2 )	//5.0G/6.25G
			{
				_data = _data|0x14;
			}
			else			//3.125G
			{
				_data = _data|0xa;
			}
			set_1848_reg(0xff8000+_laneNum*0x100,_data);
		}
	}

	printf( "stage 4 reset port.\n\r");
	_data = 0x80000000;
	for(i=0;i<12;i++)
	{
		if( (i!=0) && (i!=1) )
			continue;
		
		_portNum = i;
		_data = _data|(1<<_portNum);
		//pll_num
		_data = _data|(1<<(18+_portNum));
	}
	set_1848_reg(0xf20300,_data);
	delay_cnt(0x1ffffff);

	//_data = get_1848_reg(0xf20300);
	//xil_printf( "stage4:set 0xf20300 data = 0x%x.\n\r",_data);
	//set_1848_reg(0xf20300,_data);

	printf( "stage 5 enable ports \n\r" );

	for(i=0;i<12;i++)
	{
		if( (i!=0) && (i!=1) )
			continue;
		
		_portNum = i;
		_data = get_1848_reg(0x15c+_portNum*0x20);
		//xil_printf( "stage5:set before port-%d status = 0x%x.\n\r",_portNum,_data);
		set_1848_reg(0x15c+_portNum*0x20,_data&0xff7fffff);
		//_data = get_1848_reg(0x15c+_portNum*0x20);
		//xil_printf( "stage5:now port-%d status = 0x%x.\n\r",_portNum,_data);
	}
}

void spy1848Speed()
{
	unsigned int pdata[8];	
	unsigned short _portNum,_laneNum;
	int i,j;	

	printf("-------------------------------------------------- 1848 ----------------------------------------------------------------\n");
	printf("port      pll(0xff0000)         lane0(0xff8000)	     lane1		lane2		lane3\r\n");

	for( i=0; i<12; i++)
	{		
		_portNum = i;
		pdata[0] = get_1848_reg(0xff0000+_portNum*0x10);

		for(j=0;j<4;j++)
		{
			_laneNum = _portNum*4+j;
			pdata[j+1] = get_1848_reg(0xff8000+_laneNum*0x100);
		}

		printf("%.2d         0x%.8x     	 0x%.8x  	    0x%.8x        0x%.8x        0x%.8x\r\n",_portNum,pdata[0],pdata[1],pdata[2],pdata[3],pdata[4]);
	}
}

int bslUsrRioInit(void)
{
	unsigned int _val = 0;
    unsigned int cabinetNum,caseNum,slotNum;

    if( m_RioInit )
    {
    	printf("[DSP%d]bslUsrRioInit already intialized.\r\n",sysCpuGetID());
        return m_RioStatus;
    }
    m_RioInit = 1;
    //i2c_init();
    accSrioSwInit();
    if(sysCpuGetID() == 0)
    {
        get_1848_reg(0);
        printf("[DSP%d]i2c read 0x%x.\r\n",sysCpuGetID(),get_1848_reg(0));
        _val = sys_clk_rate_get();
        printf("[DSP%d]get sys clk_rate %d.\r\n",sysCpuGetID(),_val);
        sys_clk_rate_set(1000);
        iic_1848_init();
        sys_clk_rate_set(_val);
        set_1848_reg(0x78, 0xdf);
    }
    //_val= rioRepair_new();
    m_RioStatus = _val;

    return _val;
}

int bslNULL(void)
{
	return 0;
}



