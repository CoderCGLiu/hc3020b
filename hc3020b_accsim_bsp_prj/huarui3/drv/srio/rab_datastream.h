#ifndef _RAB_DATASTREAM_H_
#define _RAB_DATASTREAM_H_

#include "rab.h"
#include "srio.h"
/* Data Stream REgisters */

struct rab_obds_hdr_desc {
	/* W0_CS */
	u32 valid		: 1;
    u32 next_descriptor_pointer_valid : 1;
    u32 descriptor_type	: 1;
    u32 single_descriptor	: 1;
    u32 end_of_chain	: 1;
    u32 interrupt_enable	: 1;
    u32 resv_1		: 2;
    u32 done		: 1;
    u32 resv_2		: 1;
    u32 axi_error		: 1;
    u32 resv_3		: 5;
    u32 dest_id     : 16;

	/* WISTSTRM */
	u32 stream_id		: 16;
    u32 pdu_length		: 16;

	/* W2 ADDR */
    u32 priority		: 2;
    u32 class_of_service	: 8;
    u32 crf			: 1;
    u32 resv_4		: 11;
    u32 data_address_37_32	: 6;
    u32 next_descriptor_address_upper_37	: 1;
	u32 resv_5		: 3;
	
	/* W3 ADDR_EXT */
	u32 dest_id_high	: 16;
	u32 resv_6	: 16;

	/* W4 NXTD */
	u32 next_descriptor_address	: 32;

	/* W5 STD ADDR */
	u32 data_address_31_0	: 32;
};

struct rab_obds_data_desc {
	/* W0_CS */
    u32 valid       : 1;
    u32 next_data_descriptor_pointer_valid  : 1;
    u32 descriptor_type : 1;
    u32 resv_1      : 1;
    u32 sop         : 1;
    u32 eop         : 1;
    u32 resv_2      : 2;
    u32 done        : 1;
	u32 resv_3		: 7;
    u32 data_len		: 16;
	/* W1 DADDR */
	u32 data_address_31_0	: 32;

	/* W2 ADDR */
	u32 data_address_37_32	: 6;
	u32 resv_4		: 22;
    u32 next_data_descriptor_addr_upper_37	: 1;
    u32 resv_5		: 3;

	/* W3 ADDR_EXT */
	u32 resv_6	: 32;
	
	/* W4 NXTDD */
	u32 next_data_descriptor_addr	: 32;
};

struct rab_ibds_desc {
	/* W0 CS */
    u32 valid       : 1;
    u32 next_descriptor_pointer_valid   : 1;
    u32 end_of_chain    : 1;
    u32 interrupt_enable    : 1;
    u32 descriptor_size : 3;
    u32 priority        : 2;
    u32 done        : 1;
    u32 data_stream_error   : 1;
    u32 axi_error       : 1;
    u32 crf         : 1;
    u32 resv_1      : 3;
	u32 source_id		: 16;
	/* W1STREAM */
	u32 dest_id		: 16;
    u32 pdu_length		: 16;

	/* W2 ADDR */
    u32 stream_id		: 16;
    u32 cos			: 8;
    u32 next_descriptor_address_upper_37	: 1;
    u32 resv_2		: 1;
	u32 destination_address_37_32	: 6;

	/* W3 ADDR_EXT */
	u32 dest_id_high	: 16;
	u32 src_id_high		: 16;

	/* W4DADDR */
	u32 destination_address_31_0	: 32;

	/* W5NXTD */
	u32 next_descriptor_address	: 32;
};

#define OBDS_MULTI_DESC_DATA_LEN	4096

#define RIO_DS_MTU_OFFSET 26

#define OB_DSE_STAT_DESC_CHAIN_TRANS_COMPLETED	0x1
#define OB_DSE_STAT_DESC_TRANS_COMPLETE		0x2
#define OB_DSE_STAT_DESC_FETCH_ERROR		0X4
#define OB_DSE_STAT_DESC_ERROR			0x8
#define OB_DSE_STAT_DESC_UPDATE_ERROR		0x10
#define OB_DSE_STAT_DATA_TRANS_ERROR		0x20
#define OB_DSE_STAT_TRANS_PENDING		0x80
#define OB_DSE_STAT_SLEEPING			0x100


#define IB_DSE_STAT_TRANS_PENDING	0x40
#define IB_DSE_STAT_TIMEOUT		0x80


#define IB_DSE_VSID_STAT_DESC_CHAIN_TRANS_COMPLETE	0x1
#define IB_DSE_VSID_STAT_DESC_TRANS_COMPLETE		0x2
#define IB_DSE_VSID_STAT_DESC_FETCH_ERROR		0x4
#define IB_DSE_VSID_STAT_TIMEOUT_ERROR			0x8
#define IB_DSE_VSID_STAT_DESC_UPDATE_ERROR		0x10
#define IB_DSE_VSID_STAT_DATA_TRANS_ERROR		0x20
#define IB_DSE_VSID_STAT_DATA_STREAM_PDU_LENGTH_MISMATCH_ERROR 0x40
#define IB_DSE_VSID_STAT_DATA_STREAM_MTU_LENGTH_mismatch_error 0X80
#define IB_DSE_VSID_STAT_LOSS_OF_SEGMENT_ERROR		0x100
#define IB_DSE_VSID_STAT_DROPPED_PDU_ON_ENGINE_BUSY	0x200
#define IB_DSE_VSID_STAT_TRANS_PENDING			0x400
#define IB_DSE_VSID_STAT_SLEEPING			0x800

#endif /* _RAB_DATASTREAM_H_ */
