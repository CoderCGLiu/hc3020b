#ifndef _SRIO_H_
#define _SRIO_H_

#include "srio_debug.h"

#define SRIO_RAB_INNER_ADDR(_controller)	(0x1f600000 + (_controller) * 0x100000)
#define SRIO_RAB_WRAPPER_ADDR(_controller)	(0x1f640000 + (_controller) * 0x100000)
#define SRIO_PHY_INNER_ADDR(_controller)	(0x1f680000 + (_controller) * 0x100000)
#define SRIO_PHY_WRAPPER_ADDR(_controller)	(0x1f6c0000 + (_controller) * 0x100000)

#define ENDIAN_REVERT(a)	((((a) & 0x000000ff) << 24) | (((a) & 0x0000ff00) << 8) | (((a) & 0x00ff0000) >> 8) | (((a) & 0xff000000) >> 24))

typedef enum {SRIO0 = 0, SRIO1 =1} sriox;
enum {LPC_READ = 0, LPC_WRITE};
enum {ONE_BYTE = 1, TWO_BYTES = 2, FOUR_BYTES = 0, FW_128_BYTES = 15};
enum {SIRQ_CONTINUE_MODE = 0, SIRQ_QUIET_MODE};

#define PHY_SERIAL_LOOPBACK 0
#define PHY_BIST_CONFIG 0
#define TX_FFE_PARA_TRAVERSE 0
#define CACHE_EN    1

#if TX_FFE_PARA_TRAVERSE
int g_total_errbits = 0;
int g_loop_times = 0;
int g_min_errbits = 0xffff * 4 * 3; 
int g_best_index = 0;
#endif

#define HR_RAB_DB_SEM 			1 /*ldf 20230523 add:: 门铃信号量*/
#define _TEST_SRIO_DBSndTime_ 	0 /*ldf 20230523 add:: 发送门铃耗时打印*/
#define _TEST_SRIO_DMASndTime_ 	0 /*ldf 20230523 add:: DMA发送耗时打印*/
#define _RIO_SEM_REWORKS_  		0 /*ldf 20230523 add:: 信号量由vx改为reworks接口*/

#if _TEST_SRIO_DMASndTime_
#define _TEST_DMA_PRINT_CYCLE_	400 /*ldf 20230523 add:: DMA发送计时间隔次数*/
extern unsigned long long bslGetTimeUsec(void);
#endif /*_TEST_SRIO_DMASndTime_*/

void rioLockInit(u8 controller);
void rab_page_write(sriox base_addr, u32 addr, u32 data);
u32 rab_page_read(sriox base_addr, u32 addr);
void rab_link_and_mode_status(sriox base_addr);
void rab_wait_for_linkup(sriox base_addr);

void rab_phy_link_ini_config_2G5(sriox base_addr);
void rab_phy_link_ini_config_3G125(sriox base_addr);
void rab_phy_link_ini_config_5G(sriox base_addr);
void rab_phy_link_ini_config_6G25(sriox base_addr);
void rab_phy_link_ini_config_10G3125(sriox base_addr);

#endif /* _SRIO_H_ */


