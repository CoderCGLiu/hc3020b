#ifndef _rab_H_
#define _rab_H_

//#include <linux/compat.h> 
#include "srio_debug.h"
//#include "srio.h"

//��������4��DB����
#define RAB_DB_ENGINE_MAX       (15)

#define BFM_BDID    1

typedef enum
{
	OB_DB_MSG_DONE = 0,
	OB_DB_MSG_RETRY = 1,
	OB_DB_MSG_ERROR = 2,
	OB_DB_MSG_TIMEOUT = 3,
	OB_DB_MSG_PENDING = 4
} e_trans_msg_status;


/* RIO Register */
#define RIO_SPICAR	0x14	/* Switch Port Information CAR */
#define RIO_SRTDIDLCSR	0x34	/* Switch Route Table Destination ID Limit CAR */
#define RIO_SRCDIDSCSR	0x70	/* Standard Route Configuration Destination ID Select CSR */
#define RIO_SRCPSCSR	0x74	/* Standard Route Configuration Port Select CSR */
#define RIO_SRDPCSR	0x78	/* Standard Route Default Port CSR */

/* GRIO Register Detail */
#define GRIO_DIDCAR		(0x0000)
#define GRIO_DICAR		(0x0004)
#define GRIO_AIDCAR		(0x0008)
#define GRIO_AICAR		(0x000c)
#define GRIO_PEFCAR		(0x0010)
#define GRIO_SOCAR		(0x0018)
#define GRIO_DOCAR		(0x001c)
#define GRIO_DSLLCCSR		(0x0048)
#define GRIO_PELLCSR		(0x004c)
#define GRIO_LCSBA1CSR		(0x005c)
#define GRIO_BDIDCSR		(0x0060)
#define GRIO_HBDIDLCSR		(0x0068)
#define GRIO_CTCSR		(0x006c)
#define GRIO_PELLCCSR		(0x004c)

/* Extented Feature Space(Generic Port Registers)) */
#define GRIO_PMBH0		(0x0100)	//r
#define GRIO_PLTOCCSR		(0x0120)
#define GRIO_PRTOCCSR		(0x0124)
#define GRIO_PGCCSR		(0x013c)
#define GRIO_PLMREQCSR(n)	(0x0140 + 0x40 * (n))
#define GRIO_PLMRESPCSR(n)	(0x0144 + 0x40 * (n))	//r
#define GRIO_PLASCSR(n)		(0x0148 + 0x40 * (n))
#define GRIO_PESCSR(n)		(0x0158 + 0x40 * (n))
#define GRIO_PCCSR(n)		(0x015c + 0x40 * (n))

/* LP-Serial Lane Extended Features Block */
#define GRIO_SLEFBHCSR		(0x0200)	//r

/* Lane N Status Registers */
#define GRIO_LS0CSR(n)		(0x0210 + 0x20 * (n))	//r
#define GRIO_LS1CSR(n)		(0x0214 + 0x20 * (n))	//r

/* Error Reporting, Logical */
#define GRIO_ERBH		(0x0600)	//r
#define GRIO_LTLEDCSR		(0x0608)
#define GRIO_LTLEECSR		(0x060c)
#define GRIO_LTLACCSR		(0x0614)
#define GRIO_LTLDIDCCSR		(0x0618)
#define GRIO_LTLCCCSR		(0x061c)

/* Port N Specific Physical Error Reporting Registers */
#define GRIO_PEDCSR(n)		(0x0640 + 0x40 * (n))
#define GRIO_PERECSR(n)		(0x0644 + 0x40 * (n))
#define GRIO_PECACSR(n)		(0x0648 + 0x40 * (n))
#define GRIO_PPCSECCSR0(n)	(0x064c + 0x40 * (n))
#define GRIO_PPECCSR1(n)	(0x0650 + 0x40 * (n))
#define GRIO_PPECCSR2(n)	(0x0654 + 0x40 * (n))
#define GRIO_PPECCSR3(n)	(0x0658 + 0x40 * (n))
#define GRIO_PERCSR(n)		(0x0668 + 0x40 * (n))
#define GRIO_PERTCSR(n)		(0x066c + 0x40 * (n))

#define GRIO_PNR		(0x10000)	//rwc Port Number Register
#define GRIO_LLCR		(0x10004)
#define GRIO_EPWISR		(0x10010)	//r
#define GRIO_PRETCR		(0x10080)

/* Port N specific General Registers */
#define GRIO_PADIDmCSR(m, n)	(0x10100 + (m) + 0x80 * (n))	//rw
#define GRIO_PPTAACR(n)		(0x10120 + 0x80 * (n))	//rw
#define GRIO_PIECSR(n)		(0x10130 + 0x80 * (n))	//rwc
#define GRIO_PPCR(n)		(0x10140 + 0x80 * (n))	//rw
#define GRIO_PSLCSR(n)		(0x10158 + 0x80 * (n))	//rwc
#define GRIO_PSLEICR(n)		(0x10160 + 0x80 * (n))	//rw

/* Port N specific Debug Registers */
#define GRIO_PLLDMR(n)		(0x10900 + 0x20 * (n))	//rw
#define GRIO_PPLDMR(n)		(0x10904 + 0x20 * (n))	//rw
#define GRIO_PPLCTMR(n)		(0x10908 + 0x20 * (n))	//rw
#define GRIO_PLLCTMR(n)		(0x1090c + 0x20 * (n))	//rw
#define GRIO_PDBGCNTR(n)	(0x10910 + 0x20 * (n))	//r

/* Revision Control Register - Port Common */
#define GRIO_IPBRR1		(0x10bf8)	//r
#define GRIO_IPBRR2		(0x10bfc)	//r

/* Port Initialization Related Registers */
#define GRIO_PORT_TIMEOUT_CSR		(0x10c00)	//rw
#define GRIO_PORT_CHARACTER_COUNT_CSR	(0x10c04)	//rw


/* --------------------------------------------------------------------------------*/
/* RAB GLobal CSR*/
#define RAB_VER			(0x20000)
#define RAB_CAPA		(0x20004)
#define RAB_CTRL		(0x20008)
#define	RAB_CTRL_AMBA_PIO_ENABLE	0x1
#define RAB_CTRL_RIO_PIO_ENABLE		0x2
#define	RAB_CTRL_WR_DMA_ENABLE		0x4
#define	RAB_CTRL_RD_DMA_ENABLE		0x8
#define RAB_STAT		(0x2000c)
#define RAB_AXI_TIMEOUT		(0x20010)
#define RAB_DME_TIMEOUT		(0x20014)
#define RAB_RST_CTRL		(0x20018)
#define RAB_COOP_LOCK		(0x2001c)
#define RAB_STAT_ESEL		(0x20020)
#define RAB_STAT_STAT		(0x20024)
#define RAB_IB_PW_CSR		(0x20028)
#define RAB_IB_PW_DATA		(0x2002c)
#define RAB_APB_CSR		(0x20030)
#define RAB_APB_TIMEOUT		(0x20034)
#define RAB_DESC_RDY_TIMEOUT	(0x20038)

/* Interrupt CSR */
#define RAB_INTR_ENAB_GNRL	(0x20040)
#define RAB_INTR_ENAB_APIO	(0x20044)
#define RAB_INTR_ENAB_RPIO	(0x20048)
#define RAB_INTR_ENAB_WDMA	(0x2004c)
#define RAB_INTR_ENAB_RDMA	(0x20050)
#define RAB_INTR_ENAB_IDME	(0x20054)
#define RAB_INTR_ENAB_ODME	(0x20058)
#define RAB_INTR_ENAB_MISC	(0x2005c)
#define RAB_INTR_STAT_GNRL	(0x20060)
#define RAB_INTR_STAT_APIO	(0x20064)
#define RAB_INTR_STAT_RPIO	(0x20068)
#define RAB_INTR_STAT_WDMA	(0x2006c)
#define RAB_INTR_STAT_RDMA	(0x20070)
#define RAB_INTR_STAT_IDME	(0x20074)
#define RAB_INTR_STAT_ODME	(0x20078)
#define RAB_INTR_STAT_MISC	(0x2007c)

/* RIO PIO Engine CSR */
#define RAB_RPIO_CTRL(n)	(0x20080 + 0x8 * (n))	//n = 0 ~ 7
#define RAB_RPIO_CTRL_PIO_ENABLE	1
#define RAB_RPIO_STAT(n)	(0x20084 + 0x8 * (n))	//n = 0 ~ 7

/* RIO Address Mapping CSR */
#define RAB_RIO_AMAP_LUT(n)	(0x20100 + 0x4 * (n))	//n = 0 ~ 15
#define RAB_RIO_AMAP_IDSL	(0x20140)
#define RAB_RIO_AMAP_BYPS	(0x20144)

/* AXI PIO Engine CSR */
#define RAB_APIO_CTRL(n)	(0x20180 + 0x8 * (n))	//n = 0 ~ 7
#define		RAB_APIO_CTRL_PIO_ENABLE	0x1
#define		RAB_APIO_CTRL_MEM_MAP_ENABLE	0x2
#define		RAB_APIO_CTRL_MAINTENANCE_MAP_ENABLE 0x4
#define RAB_APIO_STAT(n)	(0x20184 + 0x8 * (n))	//n = 0 ~ 7

/* AXI Slave CSR */
#define RAB_ASLV_STAT_CMD(n)	(0x201c0 + 0x8 * (n))	//n = 0 ~ 3
#define RAB_ASLV_STAT_ADDR(n)	(0x201c4 + 0x8 * (n))	//n = 0 ~ 3

/* AXI Master CSR */
#define RAB_AMST_STAT(n)	(0x201e0 + 0x8 * (n))	//n = 0 ~ 7

/*AXI Address Mapping CSR*/
#define RAB_APIO_AMAP_CTRL(n)	(0x20200 + 0x10 * (n))	//n = 0 ~ 31
#define RAB_APIO_AMAP_SIZE(n)	(0x20204 + 0x10 * (n))	//n = 0 ~ 31
#define RAB_APIO_AMAP_ABAR(n)	(0x20208 + 0x10 * (n))	//n = 0 ~ 31
#define RAB_APIO_AMAP_RBAR(n)	(0x2020c + 0x10 * (n))	//n = 0 ~ 31

/* Doorbell Message CSR */
#define RAB_OB_DB_CSR(n)	(0x20400 + 0x8 * (n))	//n = 0 ~ 15
#define RAB_OB_DB_INFO(n)	(0x20404 + 0x8 * (n))	//n = 0 ~ 15
#define RAB_OB_IDB_CSR		(0x20478)
#define RAB_OB_IDB_INFO		(0x2047c)
#define RAB_IB_DB_CSR		(0x20480)
#define RAB_IB_DB_INFO		(0x20484)
#define RAB_IB_DB_CHK(n)	(0x20488 + 0x4 * (n))	//n = 0 ~ 29

/* Outbound DME CSR */
#define RAB_OB_DME_CTRL(n)	(0x20500 + 0x10 * (n))	//n = 0 ~ 15
#define RAB_OB_DME_ADDR(n)	(0x20504 + 0x10 * (n))	//n = 0 ~ 15
#define RAB_OB_DME_STAT(n)	(0x20508 + 0x10 * (n))	//n = 0 ~ 15
#define RAB_OB_DME_DESC(n)	(0x2050c + 0x10 * (n))	//n = 0 ~ 15
#define RAB_OB_DME_TIDMSK	(0x205f0)

/* Inbound DME CSR */
#define RAB_IB_DME_CTRL(n)	(0x20600 + 0x10 * (n))	//n = 0 ~ 32
#define RAB_IB_DME_ADDR(n)	(0x20604 + 0x10 * (n))	//n = 0 ~ 32
#define RAB_IB_DME_STAT(n)	(0x20608 + 0x10 * (n))	//n = 0 ~ 32
#define RAB_IB_DME_DESC(n)	(0x2060c + 0x10 * (n))	//n = 0 ~ 32

/* Write DMA CSR */
#define RAB_WDMA_CTRL(n)	(0x20800 + 0x10 * (n))	//n = 0 ~ 7
#define RAB_WDMA_ADDR(n)	(0x20804 + 0x10 * (n))	//n = 0 ~ 7
#define RAB_WDMA_STAT(n)	(0x20808 + 0x10 * (n))	//n = 0 ~ 7
#define RAB_WDMA_ADDR_EXT(n)	(0x2080C + 0x10 * (n))	//n = 0 ~ 7

/* Read DMA CSR */
#define RAB_RDMA_CTRL(n)	(0x20880 + 0x10 * (n))	//n = 0 ~ 7
#define RAB_RDMA_ADDR(n)	(0x20884 + 0x10 * (n))	//n = 0 ~ 7
#define RAB_RDMA_STAT(n)	(0x20888 + 0x10 * (n))	//n = 0 ~ 7
#define RAB_RDMA_ADDR_EXT(n)	(0x2088C + 0x10 * (n))	//n = 0 ~ 7

/* DMA Descriptor CSR */
#define RAB_DMA_IADDR_DESC_SEL		(0x20900)
#define RAB_DMA_IADDR_DESC_CTRL		(0x20904)
#define RAB_DMA_IADDR_DESC_SRC_ADDR	(0x20908)
#define RAB_DMA_IADDR_DESC_DEST_ADDR	(0x2090c)
#define RAB_DMA_IADDR_DESC_NEXT_ADDR	(0x20910)

/* Logical Flow Control */
#if RAB_NUM_LFC <= 16
#define RAB_LFC_CSR(n)			(0x20920 + 0x4 * (n))
#define RAB_LFC_TIMEOUT			(0x20960)
#define RAB_LFC_BLOCKED			(0x20964)
#endif

/* SERDES Control & Status */
#define RAB_SRDS_CTRL(n)		(0x20980 + 0x4 * (n))
#define RAB_SRDS_STAT(n)		(0x20990 + 0x4 * (n))

/* USR CSR Register */
#define RAB_USR_CSR(n)			(0x209a0 + 0x4 * (n))

/* AXI-WR Registers for Interrupt */
#define RAB_INTR_RPIO_AXI_ADDDR(n)	(0x22000 + 4 * (n))
#define RAB_INTR_RPIO_AXI_ADDR_EXT(n)	(0x22020 + 4 * (n))
#define RAB_INTR_APIO_AXI_ADDR		(0x22040)
#define RAB_INTR_APIO_AXI_ADDR_EXT	(0x22044)
#define RAB_INTR_WDMA_AXI_ADDR(n)	(0x22048 + 4 * (n))
#define RAB_INTR_WDMA_AXI_ADDR_EXT(n)	(0x22068 + 4 * (n))
#define RAB_INTR_RDMA_AXI_ADDR(n)	(0x22088 + 4 * (n))
#define RAB_INTR_RDMA_AXI_ADDR_EXT(n)	(0x220a8 + 4 * (n))
#define RAB_INTR_IDME_AXI_ADDR(n)	(0x220c8 + 4 * (n))
#define RAB_INTR_IDME_AXI_ADDR_EXT(n)	(0x22148 + 4 * (n))
#define RAB_INTR_ODME_AXI_ADDR(n)	(0x221c8 + 4 * (n))
#define RAB_INTR_ODME_AXI_ADDR_EXT(n)	(0x22248 + 4 * (n))
#define RAB_INTR_MISC_AXI_ADDR		(0x222c8)
#define RAB_INTR_MISC_AXI_ADDR_EXT	(0x222cc)
#define RAB_INTR_IBDB_AXI_ADDR(n)	(0x222d0 + 4 * (n))
#define RAB_INTR_IBDB_AXI_ADDR_EXT(n)	(0x22348 + 4 * (n))
#define RAB_INTR_RPIO_N_AXI_DATA	(0X22400)
#define RAB_INTR_APIO_AXI_DATA		(0x22420)
#define RAB_INTR_WDMA_AXI_DATA(n)	(0x22424 + 4 * (n))
#define RAB_INTR_RDMA_AXI_DATA(n)	(0x22444 + 4 * (n))
#define RAB_INTR_IDME_AXI_DATA(n)	(0x22464 + 4 * (n))
#define RAB_INTR_ODME_AXI_DATA(n)	(0x224e4 + 4 * (n))
#define RAB_INTR_MISC_AXI_DATA		(0x22564)
#define RAB_INTR__IBDB_AXI_DATA(n)	(0x22568 + 4 * (n))

/* Low Power Management */
#define RAB_PIO_ENGINE_STAT		(0x22800)
#define RAB_DMA_ENGINE_STAT		(0x22804)
#define RAB_IDME_ENGINE_STAT		(0x22808)
#define RAB_ODME_ENGINE_STAT		(0x2280c)
#define RAB_AMBA_MISC_STAT		(0x22810)
#define RAB_IDSE_ENGINE_STAT		(0x22814)
#define RAB_ODSE_ENGINE_STAT		(0x22818)

/* 50-bi Addressing */
#define RAB_APIO_RIO_UPPER16_ADDR(n)	(0x22900 + 4 * (n))
#define RAB_WDMA_RIO_UPPER16_ADDR(n)	(0x22920 + 4 * (n))
#define RAB_RDMA_RIO_UPPER16_ADDR(n)	(0x22940 + 4 * (n))

/* Data Stream REgisters */
#define RAB_DS_CFG			(0x22a00)
#define RAB_INTR_ENAB_IBDS_LO		(0x22a04)
#define RAB_INTR_ENAB_IBDS_HI		(0x22a08)
#define RAB_INTR_ENAB_OBDSE		(0x22a0c)
#define RAB_INTR_STAT_IBDSE_LO		(0x22a10)
#define RAB_INTR_STAT_IBDSE_HI		(0x22a14)
#define RAB_INTR_STAT_OBDSE		(0x22a18)
#define RAB_IBDS_VSID_ALIAS		(0x22a1c)
#define RAB_IBDSE_CTRL(n)		(0x22a20 + 0x8 * (n))
#define RAB_IBDSE_STAT(n)		(0x22a24 + 0x8 * (n))
#define RAB_IBDS_DESC_INDEX		(0X22b20)
#define RAB_IBDS_DESC_DATA_STAT		(0x22b24)
#define RAB_IBDS_VSID_LOW_ADDR(n)	(0x22b28 + 0x8 * (n))
#define RAB_IBDS_VSID_HIGH_ADDR(n)	(0x22b2c + 0x8 * (n))
#define RAB_ODSE_CTRL(n)		(0x22d28 + 0xc * (n))
#define RAB_OBDSE_STAT(n)		(0x22d2c + 0xc * (n))
#define RAB_OBDS_DESC_ADDR(n)		(0x22d30 + 0xc * (n))
#define RAB_OBDS_DESC_INDEX		(0x22ea8)
#define RAB_OBDS_DESC_DATA		(0x22eac)

/* Inbound DS Interrupt Route REgister */
#define RAB_IBDS_INTR_ROUTE(n)		(0x22eb0 + 4 * (n))

/* Outbound DS Interrupt Route Register */
#define RAB_OBDS_INTR_ROUTE(n)		(0x22ed0 + 4 * (n))
#define RAB_IBDSE_VSID_STAT(n)		(0x22ef0 + 4 * (n))
#define RAB_IBDS_TIMEOUT		(0x22ff0)
#define RAB_IBDS_TIMEOUT_CTRL		(0x22ff4)
#define RAB_INTR_IDSE_AXI_ADDR(n)	(0x23000 + 0xc * (n))
#define RAB_INTR_IDSE_AXI_ADDR_EXT(n)	(0x23004 + 0xc * (n))
#define RAB_INTR_IDSE_AXI_DATA(n)	(0x23008 + 0xc * (n))
#define RAB_INTR_ODSE_AXI_ADDR(n)	(0x23300 + 0xc * (n))
#define RAB_INTR_ODSE_AXI_ADDR_EXT(n)	(0x23304 + 0xc * (n))
#define RAB_INTR_ODSE_AXI_DATA(n)	(0x23308 + 0xc * (n))

/* AXI Master Cache Register */
#define RAB_AMST_CACHE(n)		(0x23480 + 0x4 * (n))

/* Inbound DME Interrupt Route Register */
#define RAB_IBMSG_INTR_ROUTE(n)		(0x234a0 + 0x4 * (n))

/* Outbound DME N CSR */
#define RAB_OBMSG_INTR_ROUTE(n)		(0x234b0 + 0x4 * (n))

/* Logical Flow Control */
#if RAB_NUM_LFC > 16
#define RAB_LFC_CSR(n)			(0x23500 + 0x4 * (n))
#define RAB_LFC_TIMEOUT			(0X23600)
#define RAB_LFC_BLOCKED			(0X23604)
#define RAB_LFC_BLOCKED_1		(0x23608)
#endif

/* DME Descriptor Memory(16KB) */
#define RAB_DME_DESC_MEM_BASE		(0x30000)
#define RAB_DME_DESC_MEM_SIZE		(0x4000)

#define APB_PAGE_SIZE			0x2000 /* 8K */
#define APB_PAGE_WINDOW		0x800  /* 2K */

#define RAB_SPR_BASE	0x20000

extern volatile u16 _csr_page_0;
extern volatile u16 _csr_page_1;

#define AXI_PIO_INTR_ENAB	1
#define RIO_PIO_INTR_ENAB	(1 << 1)
#define WDMA_INTR_ENAB		(1 << 2)
#define RDMA_INTR_ENAB		(1 << 3)
#define IB_DME_INTR_ENAB	(1 << 4)
#define OB_DME_INTR_ENAB	(1 << 5)
#define MISC_INTR_ENAB		(1 << 6)
#define IB_DS_INTR_ENAB		(1 << 7)
#define OB_DS_INTR_ENAB		(1 << 8)
#define AXI_WRITE_INTR_ENAB	(1 << 15)
#define OB_DB_INTR_ENAB		(1 << 31)

struct resource {
	u64 base;
	u64 len;
};

struct rab {
	int rab_no;
	u32 reg_base_addr;
	u32 coop_lock_id;
	int is_host; 				/* the host should do the enumeration */
	u16 id;					/* lbdid */
	int port_num;				/* port num on switch */

	u64 rio_base;				/* rapidio space address */

	struct resource ob_low_res;		/* outbound axi base */
	struct resource ob_high_res;

	struct resource ibds_desc_res;		/* inbound data stream */
	struct resource ibds_data_res;

	struct resource ibdmsg_desc_res;	/* inbound data message */
	struct resource ibdmsg_data_res;

	struct resource ibdata_res;		/* inbound data nread/nwrite */

	struct resource default_res;		/* default ddr memory for test */

};

#endif /* _rab_H_ */
