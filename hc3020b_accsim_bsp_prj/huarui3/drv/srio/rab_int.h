#ifndef _RAB_INT_H_
#define _RAB_INT_H_

#include "rab.h"
#include "srio.h"

#define DSP48_PIC_BASE_ADDR        0x1f930000UL
#define DSP48_PIC_CLR_L_R          (DSP48_PIC_BASE_ADDR + 0x430UL)
#define DSP48_PIC_CLR_H_R          (DSP48_PIC_BASE_ADDR + 0x438UL)
#define DSP48_PIC_STA_L_R          (DSP48_PIC_BASE_ADDR + 0x440UL)
#define DSP48_PIC_STA_H_R          (DSP48_PIC_BASE_ADDR + 0x448UL)

#define AXI_PIO_INTR_ENAB	1
#define RIO_PIO_INTR_ENAB	(1 << 1)
#define WDMA_INTR_ENAB		(1 << 2)
#define RDMA_INTR_ENAB		(1 << 3)
#define IB_DME_INTR_ENAB	(1 << 4)
#define OB_DME_INTR_ENAB	(1 << 5)
#define MISC_INTR_ENAB		(1 << 6)
#define IB_DS_INTR_ENAB		(1 << 7)
#define OB_DS_INTR_ENAB		(1 << 8)
#define AXI_WRITE_INTR_ENAB	(1 << 15)
#define OB_DB_INTR_ENAB		(1 << 31)

#define RAB0_INTR           (0x0000000000000001ull << (67 - 64))
#define RAB1_INTR           (0x0000000000000001ull << (71 - 64))



void rab0_int_do();
void rab1_int_do();
void rab_int_Init(u8 controller);

#endif /* _RAB_INT_H_ */
