#include "srio_debug.h"
#include "rab_amap.h"

void gpdma_trans(u64 src, u64 dst, u32 size)
{
	u32 ch_no = 1;

	if (0 != ((0x1 << 5) & phx_read_u32(0x900000001f800000UL + 0x40 * ch_no + 0 * 4))) {
		uart_printf("ERROR：channel unusable, req = 1\n");
		return;
	}

	phx_write_u32(0x900000001f800000UL + 0x1fcUL, 0xfff00000);//DMA_TEST_DMA_CR[31:20]允许中断，其他位RO

	phx_write_u32(0x900000001f800000UL + 0x40 * ch_no + 2 * 4, size);//chunk:传输大小(字节)
	phx_write_u32(0x900000001f800000UL + 0x40 * ch_no + 3 * 4, size);//total:总传输大小
	phx_write_u32(0x900000001f800000UL + 0x40 * ch_no + 4 * 4, size);//block:block大小
	//?地址范围？
	phx_write_u32(0x900000001f800000UL + 0x40 * ch_no + 5 * 4, src & 0xffffffff);//src_addr_l
	phx_write_u32(0x900000001f800000UL + 0x40 * ch_no + 6 * 4, dst & 0xffffffff);//dst_addr_l
	phx_write_u32(0x900000001f800000UL + 0x40 * ch_no + 7 * 4, (src & 0xffffffff00000000UL)>>32);//src_addr_h
	phx_write_u32(0x900000001f800000UL + 0x40 * ch_no + 8 * 4, (dst & 0xffffffff00000000UL)>>32);//dst_addr_h
	//矩阵、描述符
	phx_write_u32(0x900000001f800000UL + 0x40 * ch_no + 9 * 4, 0x000000UL);//row_num:原矩阵行数,低24有效
	phx_write_u32(0x900000001f800000UL + 0x40 * ch_no + 10 * 4, 0x000000UL);//col_num:原矩阵列数,低24有效
	phx_write_u32(0x900000001f800000UL + 0x40 * ch_no + 11 * 4, 0x06400000UL);//dst_block:目的矩阵行跨度
	phx_write_u32(0x900000001f800000UL + 0x40 * ch_no + 12 * 4, 0x00000000UL);//desc_l:DMA描述符起始地址低32
	phx_write_u32(0x900000001f800000UL + 0x40 * ch_no + 13 * 4, 0x00UL);//desc_h:DMA描述符起始地址高8

	//最后配置CSR
	unsigned long long csr_val;
	csr_val = (
		(0x0UL << 31) //非停止操作，固定为0
		| (0x0UL << 29) //*选定物理通道0-3
		| (0x0UL << 28) //[28]
		| (0x0UL << 27) //[27]
		| (0x1UL << 26) //[26]
		| (0x1UL << 25) //[25]
		| (0x3UL << 22) //[22~23] 2:4B  3:8B
		| (0x0UL << 21) //[21]
		| (0x0UL << 6) //[6~11] 0：独占
		| (0X1UL << 5) //*[5]
		| (0x0UL << 4) //读一维数据，写二维连续数据
 		| (0X0UL << 3)//读二维数据，写一维连续数据
		| 0x00850000UL); //fix value axisize 5, awburst_type 1

	phx_write_u32(0x900000001f800000UL + 0x40 * ch_no + 0 * 4, csr_val);

	//等待传输完成
	int i = 0;
	//uart_printf("wait dma ...  ");
	while(0 != ((0x1 << 5) & phx_read_u32(0x900000001f800000UL + 0x40 * ch_no + 0 * 4))){
		//uart_printf("%d	", i);
		i++;
	}
	uart_printf("  finish.\n");

	if (ch_no < 32)
		phx_write_u32(0x900000001f80003cUL, ~(0x1<<ch_no) & phx_read_u32(0x900000001f80003cUL));
	else
		phx_write_u32(0x900000001f80007cUL, ~(0x1<<(ch_no-32)) & phx_read_u32(0x900000001f80007cUL));
}


#define ENABLE_RPIO_AMAP_BYPASS     1
#define DISABLE_RRIO_AMAP_BYPASS    0

/* set the size of the address mapping window, as multiple of 1KB
 * index              : 0 ~ RAB_NUM_A2P_AMAP_WIN (max 32) - 1
 * win_size(22 bits)  : KBs size of the address mapping window
 */
void set_rab_apio_amap_size(u8 controller, u8 index, u32 win_size)
{
	rab_page_write(controller, RAB_APIO_AMAP_SIZE(index), win_size);
}

/* set the base address of AXI window for mapping to RIO address
 * index                   : 0 ~ RAB_NUM_A2P_AMAP_WIN (max 32) - 1
 * axi_addr >> 10(22 bits)       : AXI based address for the windows, at 1KB boundary
 * axi_addr >> 10(upper 6 bits)  : upper 6 bits of AXI Base Address for 38 bit AXI Addressing
 */
void set_rab_apio_amap_abase(u8 controller, u8 index, u64 axi_addr)
{
	rab_page_write(controller, RAB_APIO_AMAP_ABAR(index), (axi_addr >> 10) & 0xfffffff);
}


/* set the base address of RIO corresponding to window
 * index                   : 0 ~ RAB_NUM_A2P_AMAP_WIN (max 32) - 1
 * rio_base(22 bits)       : RIO based address for the windows, at 1KB boundary
 */
void set_rab_apio_amap_rbase(u8 controller, u8 index, u32 rio_base)
{
	rab_page_write(controller, RAB_APIO_AMAP_RBAR(index), (rio_base & 0x3fffff));
}

/* *************************************************************************
 *                        AXI Address Mapping(PIO -> RIO)
 * ************************************************************************* */
/* set AXI Address Mapping Control Register
 * index              : 0 ~ RAB_NUM_A2P_AMAP_WIN (max 32) - 1
 * enable(1 bit)      : enable the AMBA trasactions to the windows address
 * type(2 bits)       : indicates the type that AXI transactions to the window address is mapped to
 * 			00 - Maintenance Read and Maintenance Write
 * 			01 - NREAD and NWRITE
 * 			10 - NREAD and NWRITE_R
 * 			11 - NREAD and SWRITE
 * priority(2 bits)   : priority for transaction generation(2'b11 is illegal for Request)
 * x_addr(2 btis)     : bit33 and bit32 to be used for transaction
 * crf(1 bit)         : crf for transaction generation
 * dest_id(16 bits)   : destination ID of the RIO transactions
 */
void set_rab_apio_amap_ctrl(u8 controller,
	u8 index,
	u8 enable,
	e_apio_type type,
	u8 priority,
	u8 x_addr,
	u8 crf,
	u16 dest_id)
{
	u32 amap_ctrl_val;

	amap_ctrl_val = ((enable & 0x1) << 0) |
			((type & 0x3) << 1) |
			((priority & 0x3) << 3) |
			((x_addr & 0x3) << 5) |
			((crf & 0x1) << 7) |
			((dest_id & 0xffff) << 8);
	rab_page_write(controller, RAB_APIO_AMAP_CTRL(index), amap_ctrl_val);
}

/*AXI-->SRIO*/
void setup_apio_amap(u8 controller, u8 index, e_apio_type type, u64 axi_addr,
		u32 rio_addr, u32 size, u16 dest_id)
{
	set_rab_apio_amap_size(controller, index, size);
	set_rab_apio_amap_abase(controller, index, axi_addr);
	set_rab_apio_amap_rbase(controller, index, rio_addr >> 10);
	set_rab_apio_amap_ctrl(controller, index, 1, type, 0x0, 0x0, 0x0, dest_id);

	rab_page_read(controller, RAB_APIO_AMAP_CTRL(index));
}

void setup_rpio_amap(u8 controller, u8 index, u32 win_size, u64 axi_base_addr)
{
	u32 lut;
	u64 _data64;

	_data64 = 0x3fffffc000;

	lut = 1 | ((win_size & 0xf) << 1)
		| ((axi_base_addr & _data64) >> 6);
		//| ((axi_base_addr >> 6) & _data64);

	rab_page_write(controller, RAB_RIO_AMAP_LUT(index), lut);
}

void set_rab_apio_ctrl(u8 controller, u8 index, u32 val)
{
	rab_page_write(controller, RAB_APIO_CTRL(index), val);
}

/* *************************************************************************
 *                      *RIO Address Mapping(RIO -> PIO)
 * ************************************************************************* */
/* set RIO-PIO Address Mapping Look Up Table Entry
 * entry                    : RIO Address Mapping Lookup Table Entry N(0~15)
 * win_size                 : used to form the AXI address
 * upper_axi_base_addr      : 18 bits address offset
 */
void set_rab_rpio_amap_lut(u8 controller,
	u8 entry,
	u8 enable,
	u8 win_size,
	u32 upper_axi_base_addr)
{
	u32 amap_lut_val;

	amap_lut_val = ((enable & 0x1) << 0) |
		       ((win_size & 0xf) << 1) |
		       ((upper_axi_base_addr & 0x3ffff) << 14);
	rab_page_write(controller, RAB_RIO_AMAP_LUT(entry), amap_lut_val);
}

/* set RIO-PIO Address Mapping LUT Index Select
 * bit_sel   (6 bits)      : defines the bits used to index RIO-PIO Offset Look UP Table
 */
void set_rab_rpio_amap_idsl(u8 controller, u8 bit_sel)
{
	rab_page_write(controller, RAB_RIO_AMAP_IDSL, ((u32)(bit_sel & 0x1f)));
}

/* enbale or disable RIO-PIO Address Mapping Bypass
 * mode         : RIO->AXI address mapping use 4 bit from
 * 		1 - bypass enable, from RIO packet address
 * 		0 - bypass disable, from RIO packet source ID
 */
void set_rab_rpio_amap_bypass(u8 controller, u8 mode)
{
	rab_page_write(controller, RAB_RIO_AMAP_BYPS, ((u32)mode));
}

void set_rab_rpio_ctrl(u8 controller, u8 index, u32 val)
{
	rab_page_write(controller, RAB_RPIO_CTRL(index), val);
}

u32 win_size_table[] = {
	0x100000,		/* 1 M */
	0x200000,		/* 2 M */
	0x400000,		/* 4 M */
	0x800000,		/* 8 M */
	0x1000000,		/* 16 M */
	0x2000000,		/* 32 M */
	0x4000000,		/* 64 M */
	0x8000000,		/* 128 M */
	0x10000000,		/* 256 M */
	0x20000000,		/* 512 M */
	0x40000000,		/* 1 G */
	0x80000000,		/* 2 G */
};

/* _memAddr必须大于 windowSize
*比如windowSize = 0x10000000(256M), _memAddr need >= 0x10000000
*/
int rio_set_inbound_win(u8 controller, u32 _rioAddr, u64 _memAddr, u32 windowSize)
{
	u32 win_size_code;
	int i;
	u8 _index;

	for (i = 0; i < 12; i++) {
		if (win_size_table[i] == windowSize)
			break;
	}

	if (i == 12) {
		uart_printf("Rio set inbound windowSize 0x%x invalid!\r\n", windowSize);
		return -1;
	}
	if (i >= 9) {
		uart_printf("win_size_code = %d, only valid when config 50 or 66 bits address!\r\n", i);
		return -1;
	}
    if ((_memAddr & ((1 << (20 + i)) - 1)) != 0)/*检查物理地址是否对齐*/
    {
        uart_printf("localMemAddr not aligned with the byte 0x%x.\r\n", 1 << (20 + i));
        return -1;
    }

	win_size_code = i;

	/*使能RIO-PIO 地址 Bypass查找LUT*/
	set_rab_rpio_amap_bypass(controller, 1); // use addr not ID for LUT lookup
	set_rab_rpio_amap_idsl(controller, 4);   // use addr [31:28] for LUT lookup
	_index = (_rioAddr & 0xf0000000) >> 28;  //  set LUT with addr [31:28]

	setup_rpio_amap(controller, _index, win_size_code, _memAddr);

	return 0;
}


int rio_set_outbound_win(u8 controller, u8 index, u16 targetId, u64 axiSendAddr,
	u32 rioRecvAddr, u32 windowSize, u8 type)
{
	if ((type != 0) &&(type != 1) && (type != 2) && (type != 3))
		return -1;

	if (index >= 16)
		return -1;

	if (windowSize % 1024 != 0)
		return -1;

	if ((axiSendAddr >= RAB_OW_LOW_BASE_ADDR(controller) && (axiSendAddr+windowSize) < (RAB_OW_LOW_BASE_ADDR(controller)+RAB_OW_LOW_SIZE)) ||
		(axiSendAddr >= RAB_OW_HIGH_BASE_ADDR(controller) && (axiSendAddr+windowSize) < (RAB_OW_HIGH_BASE_ADDR(controller)+RAB_OW_HIGH_SIZE))) {
		setup_apio_amap(controller, index, type, axiSendAddr, rioRecvAddr, windowSize, targetId);
	} else {
		printk("<ERROR> [%s():_%d_]:: set ob error\n", __FUNCTION__, __LINE__);
		return -1;
	}
}

int rio_set_dma_win_p2p(u8 controller)
{
	u64 _memAddr;
	u32 _rioAddr;

	_rioAddr = 0x40000000;
	_memAddr = 0x90000000;
	// _memAddr must larger than windowSize
	// now windowSize = 0x10000000(256M), _memAddr need >= 0x10000000
	rio_set_inbound_win(controller, _rioAddr, _memAddr, 0x10000000); // windowSize 256M
}


void rio_set_dma_win(u16 rio0Id, u16 rio1Id)
{
	u64 _memAddr0;
	u64 _memAddr1;
	u32 _rioAddr0;
	u32 _rioAddr1;

	_rioAddr0 = 0x40000000;
	_rioAddr1 = 0x60000000;

	_memAddr0 = 0x1080000000 + 0x10000000;
	// _memAddr must larger than windowSize
	// now windowSize = 0x10000000(256M), _memAddr need >= 0x10000000
	rio_set_inbound_win(0, _rioAddr0, (u64)hrKmToPhys((void *)_memAddr0), 0x10000000); // windowSize 256M

	_memAddr1 = 0x1080000000 + 0x10000000 * 2;
	rio_set_inbound_win(1, _rioAddr1, (u64)hrKmToPhys((void *)_memAddr1), 0x10000000); // windowSize 256M

	//rio_set_outbound_win(0, 0, rio1Id, RAB_OW_LOW_BASE_ADDR(0), _rioAddr1, 16 * 0x100000, 1);
	//rio_set_outbound_win(1, 0, rio0Id, RAB_OW_LOW_BASE_ADDR(1), _rioAddr0, 16 * 0x100000, 1);
}


//const int TEST_COUNT = 128 * 1024;
//const int U16_TEST_COUNT = 1024; // watch out too big to overflow
//const int U8_TEST_COUNT = 128; // watch out too big to overflow
const int TEST_COUNT = 4;
const int U16_TEST_COUNT = 4; // watch out too big to overflow
const int U8_TEST_COUNT = 4; // watch out too big to overflow
#define MEM_ADDR0    0x1098000000
#define MEM_ADDR1    0x10a8000000
int pio_test(u16 rio0Id, u16 rio1Id)
{
	u64 _memAddr0;
	u64 _memAddr1;
	u32 _rioAddr0;
	u32 _rioAddr1;
	u32 i;
	u32 tmp1;
	u32 tmp2;
	u64 tmpx1;
	u64 tmpx2;
	u32 err_cnt=0;

	_rioAddr0 = 0x40000000;
	_rioAddr1 = 0x60000000;

	_memAddr0 = MEM_ADDR0;
	_memAddr1 = MEM_ADDR1;
	// _memAddr must larger than windowSize
	// now windowSize = 0x100000(1M), _memAddr need >= 0x100000
	rio_set_inbound_win(0, _rioAddr0, _memAddr0, 0x100000); // windowSize 1M
	rio_set_inbound_win(1, _rioAddr1, _memAddr1, 0x100000); // windowSize 1M

	rio_set_outbound_win(0, 0, rio1Id, RAB_OW_LOW_BASE_ADDR(0), _rioAddr1, 0x100000, 1);//write window
	rio_set_outbound_win(1, 0, rio0Id, RAB_OW_LOW_BASE_ADDR(1), _rioAddr0, 0x100000, 1);

	err_cnt = 0;
	for (i = 0; i < TEST_COUNT; i++) {
		phx_write_u32(RAB_OW_LOW_BASE_ADDR(0) + i * 4, i + 0x87654321);
		phx_write_u32(RAB_OW_LOW_BASE_ADDR(1) + i * 4, i + 0x12345678);
	}

	for (i = 0; i < TEST_COUNT; i++) {
#if CACHE_EN
		tmp1 = phx_read_cache_u32(MEM_ADDR0 + i * 4);
		tmp2 = phx_read_cache_u32(MEM_ADDR1 + i * 4);
#else
		tmp1 = phx_read_u32(MEM_ADDR0 + i * 4);
		tmp2 = phx_read_u32(MEM_ADDR1 + i * 4);
#endif
		if (tmp1 != (i + 0x12345678)) {
			uart_printf("type = 1, nwrite 32bit:exp 0x%x, read 0x%x \r\n", i + 0x12345678, tmp1);
			err_cnt++;
		}
		if (tmp2 != (i + 0x87654321)) {
			uart_printf("type = 1, nwrite 32bit:exp 0x%x, read 0x%x \r\n", i + 0x87654321, tmp2);
			err_cnt++;
		}
	}
	if (err_cnt)
		uart_printf("========================= 32bit apio nwrite err! err_cnt = 0x%x\r\n", err_cnt);
	else
		uart_printf("========================= 32bit apio nwrite pass!\r\n");

	err_cnt = 0;
	for (i = 0; i < TEST_COUNT; i++) {
		tmp1 = phx_read_u32(RAB_OW_LOW_BASE_ADDR(1)  + i * 4);
		tmp2 = phx_read_u32(RAB_OW_LOW_BASE_ADDR(0)  + i * 4);
		if (tmp1 != (i + 0x12345678)) {
			uart_printf("type = 1, nread 32bit:exp 0x%x, read 0x%x \r\n", i + 0x12345678, tmp1);
			err_cnt++;
		}
		if (tmp2 != (i + 0x87654321)) {
			uart_printf("type = 1, nread 32bit:exp 0x%x, read 0x%x \r\n", i + 0x87654321, tmp2);
			err_cnt++;
		}
	}
	if (err_cnt)
		uart_printf("========================= 32bit apio nread err! err_cnt = 0x%x\r\n", err_cnt);
	else
		uart_printf("========================= 32bit apio nread pass!\r\n");

	err_cnt = 0;
	for (i = 0; i < U16_TEST_COUNT; i++)
		phx_write_u16(RAB_OW_LOW_BASE_ADDR(0) + 0x0500 + i * 2, i + 0x4321);

	for (i = 0; i < U16_TEST_COUNT; i++) {
#if CACHE_EN
		tmp1 = phx_read_cache_u16(MEM_ADDR1 + 0x500 + i * 2);
#else
		tmp1 = phx_read_u16(MEM_ADDR1 + 0x500 + i * 2);
#endif
		if (tmp1 != (i + 0x4321)) {
			uart_printf("type = 1, nwrite 16bit:exp 0x%x, read 0x%x \r\n", i + 0x4321, tmp1);
			err_cnt++;
		}
	}
	if (err_cnt)
		uart_printf("========================= 16bit apio nwrite err! err_cnt = 0x%x\r\n", err_cnt);
	else
		uart_printf("========================= 16bit apio nwrite pass!\r\n");

	err_cnt = 0;
	for (i = 0; i < U16_TEST_COUNT; i++) {
		tmp1 = phx_read_u16(RAB_OW_LOW_BASE_ADDR(0) + 0x500 + i * 2);
		if (tmp1 != (i + 0x4321)) {
			uart_printf("type = 1, nread 16bit:exp 0x%x, read 0x%x \r\n", i + 0x4321, tmp1);
			err_cnt++;
		}
	}
	if (err_cnt)
		uart_printf("========================= 16bit apio nread err! err_cnt = 0x%x\r\n", err_cnt);
	else
		uart_printf("========================= 16bit apio nread pass!\r\n");

	err_cnt = 0;
	for (i = 0; i < U8_TEST_COUNT; i++)
		phx_write_u8(RAB_OW_LOW_BASE_ADDR(0) + 0x1000 + i , i + 0x54);

	for (i = 0; i < U8_TEST_COUNT; i++) {
#if CACHE_EN
		tmp2 = phx_read_cache_u8(MEM_ADDR1 + 0x1000 + i);
#else
		tmp2 = phx_read_u8(MEM_ADDR1 + 0x1000 + i);
#endif
		if (tmp2 != (i + 0x54)) {
			uart_printf("type = 1, nwrite 8bit:exp 0x%x, read 0x%x \r\n", i + 0x54, tmp2);
			err_cnt++;
		}
	}
	if (err_cnt)
		uart_printf("========================= 8bit apio nwrite err! err_cnt = 0x%x\r\n", err_cnt);
	else
		uart_printf("========================= 8bit apio nwrite pass!\r\n");

	err_cnt = 0;
	for (i = 0; i < U8_TEST_COUNT; i++) {
		tmp2 = phx_read_u8(RAB_OW_LOW_BASE_ADDR(0) + 0x1000 + i);
		if (tmp2 != (i + 0x54)) {
			uart_printf("type = 1, nread 8bit:exp 0x%x, read 0x%x \r\n", i + 0x54, tmp2);
			err_cnt++;
		}
	}
	if (err_cnt)
		uart_printf("========================= 8bit apio nread err! err_cnt = 0x%x\r\n", err_cnt);
	else
		uart_printf("========================= 8bit apio nread pass!\r\n");

	rio_set_outbound_win(0, 0, rio1Id, RAB_OW_LOW_BASE_ADDR(0), _rioAddr1, 16 * 0x100000, 2);
	rio_set_outbound_win(1, 0, rio0Id, RAB_OW_LOW_BASE_ADDR(1), _rioAddr0, 16 * 0x100000, 2);

	for (i = 0; i < TEST_COUNT; i++) {
		phx_write_u32(RAB_OW_LOW_BASE_ADDR(0) + i * 4, i + 0x43218765);
		phx_write_u32(RAB_OW_LOW_BASE_ADDR(1) + i * 4, i + 0x87654321);
	}

	err_cnt = 0;
	for (i = 0; i < TEST_COUNT; i++) {
#if CACHE_EN
		tmp1 = phx_read_cache_u32(MEM_ADDR0 + i * 4);
		tmp2 = phx_read_cache_u32(MEM_ADDR1 + i * 4);
#else
		tmp1 = phx_read_u32(MEM_ADDR0 + i * 4);
		tmp2 = phx_read_u32(MEM_ADDR1 + i * 4);
#endif
		if (tmp1 != (i + 0x87654321)) {
			uart_printf("type = 2, nwrite_r 32bit:exp 0x%x, read 0x%x \r\n", i + 0x87654321, tmp1);
			err_cnt++;
		}
		if (tmp2 != (i + 0x43218765)) {
			uart_printf("type = 2, nwrite_r 32bit:exp 0x%x, read 0x%x \r\n", i + 0x43218765, tmp2);
			err_cnt++;
		}
	}
	if (err_cnt)
		uart_printf("========================= 32bit apio nwrite_r err! err_cnt = 0x%x\r\n", err_cnt);
	else
		uart_printf("========================= 32bit apio nwrite_r pass!\r\n");

	err_cnt = 0;
	for (i = 0; i < TEST_COUNT; i++) {
		tmp1 = phx_read_u32(RAB_OW_LOW_BASE_ADDR(1) + i * 4);
		tmp2 = phx_read_u32(RAB_OW_LOW_BASE_ADDR(0) + i * 4);
		if (tmp1 != (i + 0x87654321)) {
			uart_printf("type = 2, nread 32bit:exp 0x%x, read 0x%x \r\n", i + 0x87654321, tmp1);
			err_cnt++;
		}
		if (tmp2 != (i + 0x43218765)) {
			uart_printf("type = 2, nread 32bit:exp 0x%x, read 0x%x \r\n", i + 0x43218765, tmp2);
			err_cnt++;
		}
	}
	if (err_cnt)
		uart_printf("========================= 32bit apio nread err! err_cnt = 0x%x\r\n", err_cnt);
	else
		uart_printf("========================= 32bit apio nread pass!\r\n");

	rio_set_outbound_win(0, 0, rio1Id, RAB_OW_LOW_BASE_ADDR(0), _rioAddr1, 16 * 0x100000, 3);
	rio_set_outbound_win(1, 0, rio0Id, RAB_OW_LOW_BASE_ADDR(1), _rioAddr0, 16 * 0x100000, 3);

	for (i = 0; i < TEST_COUNT; i++) {
		phx_write_u64(RAB_OW_LOW_BASE_ADDR(0) + i * 8, i + 0x123456789abcdeffull);
		phx_write_u64(RAB_OW_LOW_BASE_ADDR(1) + i * 8, i + 0x87654321);
	}

	err_cnt = 0;
	for (i = 0; i < TEST_COUNT; i++) {
#if CACHE_EN
		tmpx1 = phx_read_cache_u64(MEM_ADDR0 + i * 8);
		tmpx2 = phx_read_cache_u64(MEM_ADDR1 + i * 8);
#else
		tmpx1 = phx_read_u64(MEM_ADDR0 + i * 8);
		tmpx2 = phx_read_u64(MEM_ADDR1 + i * 8);
#endif
		if (tmpx1 != (i + 0x87654321)) {
			uart_printf("type = 3, swrite 64bit:exp 0x%x, read 0x%x\r\n", i + 0x87654321, tmpx1);
			err_cnt++;
		}
		if (tmpx2 != (i + 0x123456789abcdeffull)) {
			uart_printf("type = 3, swrite 64bit:exp 0x%x, read 0x%x\r\n", i + 0x123456789abcdeffull, tmpx2);
			err_cnt++;
		}
	}
	if (err_cnt)
		uart_printf("========================= 64bit apio swirte err! err_cnt = 0x%x\r\n", err_cnt);
	else
		uart_printf("========================= 64bit apio swrite pass!\r\n");

	err_cnt = 0;
	for (i = 0; i < TEST_COUNT; i++) {
		tmpx1 = phx_read_u64(RAB_OW_LOW_BASE_ADDR(1) + i * 8);
		tmpx2 = phx_read_u64(RAB_OW_LOW_BASE_ADDR(0) + i * 8);
		if (tmpx1 != (i + 0x87654321)) {
			uart_printf("type = 3, nread 64bit:exp 0x%x, read 0x%x\r\n", i + 0x87654321, tmpx1);
			err_cnt++;
		}
		if (tmpx2 != (i + 0x123456789abcdeffull)) {
			uart_printf("type = 3, nread 64bit:exp 0x%x, read 0x%x\r\n", i + 0x123456789abcdeffull, tmpx2);
			err_cnt++;
		}
	}
	if (err_cnt)
		uart_printf("========================= 64bit apio nread err! err_cnt = 0x%x\r\n", err_cnt);
	else
	    uart_printf("========================= 64bit apio nread pass!\r\n");
}

int pio_maint_test(u16 rio0Id, u16 rio1Id)
{
	u64 _memAddr;
	u32 _rioAddr0;
	u32 _rioAddr1;
	u32 tmp1;
	u32 tmp2;

	_rioAddr0 = 0x20000000;
	_rioAddr1 = 0x28000000;

	rio_set_outbound_win(0, 0, rio1Id, RAB_OW_LOW_BASE_ADDR(0), 0x00000000, 0x100000, 0);
	rio_set_outbound_win(1, 0, rio0Id, RAB_OW_LOW_BASE_ADDR(1), 0x00000000, 0x100000, 0);

	//maintance read write
	udelay(100);
	tmp1 = phx_read_u32(RAB_OW_LOW_BASE_ADDR(0) + 0x124);
	uart_printf("rab0 maint read RIO_SP_RT_CTL(offset 0x124):0x%x\r\n", tmp1);
	phx_write_u32(RAB_OW_LOW_BASE_ADDR(0) + 0x124, 0xabcdef88); //rab data MSB 8bit first
	tmp1 = phx_read_u32(RAB_OW_LOW_BASE_ADDR(0) + 0x124);
	tmp2 = rab_page_read(1, 0x124);
	if (tmp1 != 0x80cdef88) //rab data MSB 8bit first
		uart_printf("========================= rab0 maint err! 0x%x 0x%x\r\n", tmp1, tmp2);
	else
		uart_printf("========================= rab0 maint pass! 0x%x 0x%x\r\n", tmp1, tmp2);

	tmp1 = phx_read_u32(RAB_OW_LOW_BASE_ADDR(1) + 0x124);
	uart_printf("rab1 maint read RIO_SP_RT_CTL(offset 0x124):0x%x\r\n", tmp1);
	phx_write_u32(RAB_OW_LOW_BASE_ADDR(1) + 0x124, 0xabcdef88); //rab data MSB 8bit first
	tmp1 = phx_read_u32(RAB_OW_LOW_BASE_ADDR(1) + 0x124);
	tmp2 = rab_page_read(0, 0x124);
	if (tmp1 != 0x80cdef88) //rab data MSB 8bit first
		uart_printf("========================= rab1 maint err! 0x%x 0x%x\r\n", tmp1, tmp2);
	else
		uart_printf("========================= rab1 maint pass! 0x%x 0x%x\r\n", tmp1, tmp2);

	phx_write_u32(RAB_OW_LOW_BASE_ADDR(0) + 0x0060, 0x12345678); //rab ID change test
	tmp1 = rab_page_read(1, 0x0060);
	if (tmp1 != 0x563412) //rab data MSB 8bit first
		uart_printf("========================= rab0 maint change rabid err! 0x%x\r\n", tmp1);
	else
		uart_printf("========================= rab0 maint change rabid pass! 0x%x\r\n", tmp1);

	phx_write_u32(RAB_OW_LOW_BASE_ADDR(1) + 0x0060, 0x87654321); //rab ID change test
	tmp1 = rab_page_read(0, 0x0060);
	if (tmp1 != 0x436587) //rab data MSB 8bit first
		uart_printf("========================= rab1 maint change rabid err! 0x%x\r\n", tmp1);
	else
		uart_printf("========================= rab1 maint change rabid pass! 0x%x\r\n", tmp1);
}


const int TEST_SPEED_COUNT = 1024 * 64;
#define TEST_DATA_MEM_ADDR	0x1800000000
int pio_speed_test(u16 rio0Id, u16 rio1Id)
{
	u64 _memAddr0;
	u64 _memAddr1;
	u32 _rioAddr0;
	u32 _rioAddr1;
	u32 i;
	u32 tmp1;
	u32 tmp2;
	u64 tmpx1;
	u64 tmpx2;
	u32 err_cnt = 0;
	u32 elapsed_time1 = 0, elapsed_time2 = 0;
	_rioAddr0 = 0x40000000;
	_rioAddr1 = 0x60000000;

	_memAddr0 = 0x9000000000000000+MEM_ADDR0;
	_memAddr1 = 0x9000000000000000+MEM_ADDR1;
	// _memAddr must larger than windowSize
	// now windowSize = 0x1000000(16M), _memAddr need >= 0x1000000
	rio_set_inbound_win(0, _rioAddr0, _memAddr0, 0x1000000); // windowSize 16M
	rio_set_inbound_win(1, _rioAddr1, _memAddr1, 0x1000000); // windowSize 16M

	rio_set_outbound_win(0, 0, rio1Id, RAB_OW_LOW_BASE_ADDR(0), _rioAddr1, 16 * 0x100000, 1);//write window
	rio_set_outbound_win(1, 0, rio0Id, RAB_OW_LOW_BASE_ADDR(1), _rioAddr0, 16 * 0x100000, 1);

	for (i = 0; i < TEST_SPEED_COUNT / 4;) {
		phx_write_u32(TEST_DATA_MEM_ADDR + i * 4, i);
		uart_printf("addr 0x%x, write 0x%x \n", TEST_DATA_MEM_ADDR + i * 4, i);
		i += (TEST_SPEED_COUNT / 16);
	}

	gpdma_trans(TEST_DATA_MEM_ADDR, RAB_OW_LOW_BASE_ADDR(0), TEST_SPEED_COUNT);

	for (i = 0; i < TEST_SPEED_COUNT/4;) {
		tmp2 = phx_read_u32(TEST_DATA_MEM_ADDR + i * 4);
		uart_printf("addr 0x%x, read 0x%x \n", TEST_DATA_MEM_ADDR + i * 4, tmp2);
		i += (TEST_SPEED_COUNT / 16);
	}
}

