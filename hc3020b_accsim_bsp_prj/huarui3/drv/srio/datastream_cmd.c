//#include "srio_debug.h"
#include "srio.h"
#include "rab_amap.h"
#include "rab_datamessage.h"
/*
#include "rab_amap.c"
#include "rab_dma.c"
#include "rab_datastream.c"
#include "rab_int.c"
*/

#define SPEEDUP_ENABLE  1
#define SPEEDUP_DISABLE 0
#define TEST_SRIO	0
#define RAB0_ID 0x1
#define RAB1_ID 0x2

void datasream_main(void)
{
	u64 tmpx;
	u32 tmp;
	u32 err_cnt = 0;
	sriox SRIO_0, SRIO_1;
	int i;


	uart_printf("Test Datastream \r\n");

	SRIO_0 = SRIO0;
	SRIO_1 = SRIO1;
	
	rioLockInit(SRIO_0);/*ldf 20230404*/
	rioLockInit(SRIO_1);/*ldf 20230404*/

//	// sysctl release reset : pcie0 pcie1 rab0 rab1
//	phx_write_u32(0x1f0c0028, 0xa5accede); // unlock
//	tmp = phx_read_u32(0x1f0c0008);
//	phx_write_u32(0x1f0c0008, tmp & (~0x0c300000));

	rab_phy_link_ini_config_10G3125(SRIO_0/*, SPEEDUP_ENABLE*/);
	rab_page_write(SRIO_0, GRIO_BDIDCSR, RAB0_ID);// rab0 16bit id

	rab_phy_link_ini_config_10G3125(SRIO_1/*, SPEEDUP_ENABLE*/);
	rab_page_write(SRIO_1, GRIO_BDIDCSR, RAB1_ID);// rab1 16bit id

	rab_wait_for_linkup(SRIO_0);
	rab_wait_for_linkup(SRIO_1);

#if 1
	rab_int_Init(0);
	rab_int_Init(1);
#else
	rab_page_write(SRIO_0, RAB_CTRL, RAB_CTRL_AMBA_PIO_ENABLE | RAB_CTRL_RIO_PIO_ENABLE |
			RAB_CTRL_WR_DMA_ENABLE | RAB_CTRL_RD_DMA_ENABLE);
	rab_page_write(SRIO_1, RAB_CTRL, RAB_CTRL_AMBA_PIO_ENABLE | RAB_CTRL_RIO_PIO_ENABLE |
			RAB_CTRL_WR_DMA_ENABLE | RAB_CTRL_RD_DMA_ENABLE);
#endif

	set_rab_rpio_ctrl(0, 0, 0x3);
	set_rab_rpio_ctrl(1, 0, 0x3);
	set_rab_apio_ctrl(0, 0, 0x3);//nread nwrite test
	set_rab_apio_ctrl(1, 0, 0x3);

	test_ib_data_stream(0, 0x0002);
	test_ib_data_stream(1, 0x0001);

	// test_ob_single_obds have 2 desc, each desc sends 0x800 bytes data(0x200 u64)
	for (i = 0; i < 0x200; i++)
#if CACHE_EN
		phx_write_cache_u64(RAB_OB_DSE_DATA_START(1) + i * 8, i + 0x98765432abcd8888ull);
#else
		phx_write_u64(RAB_OB_DSE_DATA_START(1) + i * 8, i + 0x98765432abcd8888ull);
#endif

	test_ob_single_obds(1, 0, RAB_OB_DSE_DESC_START(1), RAB_OB_DSE_DATA_START(1), 0x800, 0x0001);
	udelay(1000);
	err_cnt = 0;
	// 2 ob desc sends data to 2 ib desc, each ib desc buffer's size is 0x1000 bytes
	for (i = 0; i < 0x100; i++) {
#if CACHE_EN
		tmpx = phx_read_cache_u64(RAB_IB_DSE_DATA_START(0) + i * 8);
#else
		tmpx = phx_read_u64(RAB_IB_DSE_DATA_START(0) + i * 8);
#endif
		if (tmpx != (i + 0x98765432abcd8888ull)) {
			//uart_printf("single_obds0 %d : 0x%x:ds err!\r\n", i, tmpx);
			err_cnt++;
		}
	}
	for (i = 0; i < 0x100; i++) {
#if CACHE_EN
		tmpx = phx_read_cache_u64(RAB_IB_DSE_DATA_START(0) + 0x1000 + i * 8);
#else
		tmpx = phx_read_u64(RAB_IB_DSE_DATA_START(0) + 0x1000 + i * 8);
#endif
		if (tmpx != (i + 0x100 + 0x98765432abcd8888ull)) {
			//uart_printf("single_obds1 %d : 0x%x:ds err!\r\n", i, tmpx);
			err_cnt++;
		}
	}
	if (err_cnt == 0)
		uart_printf("========================= rab0 recv single ds pass!\r\n");
	else
		uart_printf("========================= rab0 recv single ds err! err_cnt = 0x%x\r\n", err_cnt);

	for (i = 0; i < 0x200; i++)
#if CACHE_EN
		phx_write_cache_u64(RAB_OB_DSE_DATA_START(0) + i * 8, i + 0x98765432abcd8888ull);
#else
		phx_write_u64(RAB_OB_DSE_DATA_START(0) + i * 8, i + 0x98765432abcd8888ull);
#endif

	test_ob_single_obds(0, 0, RAB_OB_DSE_DESC_START(0), RAB_OB_DSE_DATA_START(0), 0x800, 0x0002);
	udelay(1000);
	err_cnt = 0;
	for (i = 0; i < 0x100; i++) {
#if CACHE_EN
		tmpx = phx_read_cache_u64(RAB_IB_DSE_DATA_START(1) + i * 8);
#else
		tmpx = phx_read_u64(RAB_IB_DSE_DATA_START(1) + i * 8);
#endif
		if (tmpx != (i + 0x98765432abcd8888ull)) {
			//uart_printf("single_obds0 %d : 0x%x:ds err!\r\n", i, tmpx);
			err_cnt++;
		}
	}
	for (i = 0; i < 0x100; i++) {
#if CACHE_EN
		tmpx = phx_read_cache_u64(RAB_IB_DSE_DATA_START(1) + 0x1000 + i * 8);
#else
		tmpx = phx_read_u64(RAB_IB_DSE_DATA_START(1) + 0x1000 + i * 8);
#endif
		if (tmpx != (i + 0x100 +  0x98765432abcd8888ull)) {
			//uart_printf("single_obds1 %d : 0x%x:ds err!\r\n", i, tmpx);
			err_cnt++;
		}
	}
	if (err_cnt == 0)
		uart_printf("========================= rab1 recv single ds pass!\r\n");
	else
		uart_printf("========================= rab1 recv single ds err! err_cnt = 0x%x\r\n", err_cnt);

	// test_ob_multi_obds sends 0x1000 * 2 bytes data(0x400 u64)
	for (i = 0; i < 0x400; i++)
#if CACHE_EN
		phx_write_cache_u64(RAB_OB_DSE_DATA_START(1) + i * 8, i + 0x134567af1abcdeffull);
#else
		phx_write_u64(RAB_OB_DSE_DATA_START(1) + i * 8, i + 0x134567af1abcdeffull);
#endif

	test_ob_multi_obds(1, 0, RAB_OB_DSE_DESC_START(1), RAB_OB_DSE_DATA_START(1), 0x1000, 0x0001);
	udelay(1000);
	err_cnt = 0;
	for (i = 0; i < 0x400; i++) {
		// single_obds takes 2 desc
#if CACHE_EN
		tmpx = phx_read_cache_u64(RAB_IB_DSE_DATA_START(0) + 0x1000 * 2 + i * 8);
#else
		tmpx = phx_read_u64(RAB_IB_DSE_DATA_START(0) + 0x1000 * 2 + i * 8);
#endif
		if (tmpx != (i + 0x134567af1abcdeffull)) {
			//uart_printf("%d : 0x%x:ds err! \r\n", i, tmpx);
			err_cnt++;
		}
	}
	if (err_cnt == 0)
		uart_printf("========================= rab0 recv multi ds pass!\r\n");
	else
		uart_printf("========================= rab0 recv multi ds err!\r\n");

	for (i = 0; i < 0x400; i++)
#if CACHE_EN
		phx_write_cache_u64(RAB_OB_DSE_DATA_START(0) + i * 8, i + 0x134567af1abcdeffull);
#else
		phx_write_u64(RAB_OB_DSE_DATA_START(0) + i * 8, i + 0x134567af1abcdeffull);
#endif

	test_ob_multi_obds(0, 0, RAB_OB_DSE_DESC_START(0), RAB_OB_DSE_DATA_START(0), 0x1000, 0x0002);
	udelay(1000);
	err_cnt = 0;
	for (i = 0; i < 0x400; i++) {
#if CACHE_EN
		tmpx = phx_read_cache_u64(RAB_IB_DSE_DATA_START(1) + 0x1000 * 2 + i * 8);
#else
		tmpx = phx_read_u64(RAB_IB_DSE_DATA_START(1) + 0x1000 * 2 + i * 8);
#endif
		if (tmpx != (i + 0x134567af1abcdeffull)) {
			//uart_printf("%d : 0x%x:ds err! \r\n", i, tmpx);
			err_cnt++;
		}
	}
	if (err_cnt == 0)
		uart_printf("========================= rab1 recv multi ds pass!\r\n");
	else
		uart_printf("========================= rab1 recv multi ds err! err_cnt = 0x%x\r\n", err_cnt);

	uart_printf("Test end \r\n");

}
