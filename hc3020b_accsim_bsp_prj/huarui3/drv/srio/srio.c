#include "srio.h"
#include "rab.h"
#include "spinlockLib.h"

extern void * t_spinlockIsrInit(void);
extern void t_spinlockIsrTake(void *lock_id);
extern void t_spinlockIsrGive(void *lock_id);

spinlockIsr_t *m_rabSpinIsrLock[2] = {NULL, NULL};;
//spinlock_rt_dtm_t *m_rabSpinIsrLock[2] = {NULL, NULL};/*ldf 20230403*/

extern const int hr3_board_type;


void rioLockInit(u8 controller)
{
	if(m_rabSpinIsrLock[controller] != NULL)
		return;

    m_rabSpinIsrLock[controller] = (spinlockIsr_t *)t_spinlockIsrInit();
//	m_rabSpinIsrLock[controller] = (spinlock_rt_dtm_t *)t_spinlockIsrInit();/*ldf 20230403*/
//	uart_printf("<DEBUG> rioLockInit controller %u done\n",controller);
}

void rab_page_write(sriox base_addr, u32 addr, u32 data)
{
    u32 rab_innr;
    u32 page;

    rab_innr = SRIO_RAB_INNER_ADDR(base_addr);

    t_spinlockIsrTake(m_rabSpinIsrLock[base_addr]);
    page = 0x1fff & (addr >> 11);
    // for safety, you should get coop_lock before read or write register
    phx_write_u32(rab_innr + 0x0030, page << 16);
    phx_write_u32(rab_innr + 0x0800 + (addr & 0x07ff), data);
    t_spinlockIsrGive(m_rabSpinIsrLock[base_addr]);
}

u32 rab_page_read(sriox base_addr, u32 addr)
{
    u32 rab_innr;
    u32 page;
    u32 val;

    rab_innr = SRIO_RAB_INNER_ADDR(base_addr);

    t_spinlockIsrTake(m_rabSpinIsrLock[base_addr]);
    page = 0x1fff & (addr >> 11);
    // for safety, you should get coop_lock before read or write register
    phx_write_u32(rab_innr + 0x0030, page << 16);
    val = phx_read_u32(rab_innr + 0x0800 + (addr & 0x07ff));
    t_spinlockIsrGive(m_rabSpinIsrLock[base_addr]);

    return val;
}

#if PHY_BIST_CONFIG
void rab_phy_bist_config(sriox base_addr)
{
    u32 phy_innr;

    phy_innr = SRIO_PHY_INNER_ADDR(base_addr);

    // TX_BIST_CONTROLS_PREG
    phx_write_u32(phy_innr+(0x00006078 << 2), 0x16); // prbs31
    phx_write_u32(phy_innr+(0x00006078 << 2), 0x17); // enable tx bist
    // RX_BIST_SYNCCNT_PREG
    phx_write_u32(phy_innr+(0x000061a9 << 2), 0x800); // Programming RX bist sync count
    // RX_BIST_CONTROLS_PREG
    phx_write_u32(phy_innr+(0x000061a8 << 2), 0xb00); // prbs31
    phx_write_u32(phy_innr+(0x000061a8 << 2), 0xb01); // enable rx bist
}

void rab_phy_bist_check_status(sriox base_addr)
{
    u32 phy_wrap;
    u32 phy_innr;
    u32 val1, val2, val3, val4;

    phy_wrap = SRIO_PHY_WRAPPER_ADDR(base_addr);
    phy_innr = SRIO_PHY_INNER_ADDR(base_addr);

	uart_printf("rab%d rx prbs results\r\n", base_addr);
    // checking for phy_status link0
    do {
        udelay(100);
        val1 = phx_read_u32(phy_wrap + 0x1c);
        val2 = phx_read_u32(phy_wrap + 0x28);
        val3 = phx_read_u32(phy_wrap + 0x34);
        val4 = phx_read_u32(phy_wrap + 0x40);
        val1 = (val1 >> 12) & 0x3f; // bit[12:17]
        val2 = (val2 >> 12) & 0x3f;
        val3 = (val3 >> 12) & 0x3f;
        val4 = (val4 >> 12) & 0x3f;
		uart_printf("phy status 0x%x 0x%x 0x%x 0x%x\r\n", val1, val2, val3, val4);
    //} while((val1 != 1) || (val2 != 1) || (val3 != 1) || (val4 != 1));
    } while((val1 != 1));
    //uart_printf("phy status check OK: 0x%x 0x%x 0x%x 0x%x\r\n", val1, val2, val3, val4);

    // reset rx bist error count
    // RX_BIST_CONTROLS_PREG  Reset errors for prbs31
    phx_write_u32(phy_innr + (0x000061a8 << 2), 0xb11);
    // RX_BIST_CONTROLS_PREG  Clear RX BIST reset bit
    phx_write_u32(phy_innr + (0x000061a8 << 2), 0xb01);

	udelay(1000000);
#if TX_FFE_PARA_TRAVERSE
	int err0, err1, err2, err3;
	err0 = phx_read_u32(phy_innr + (0x000041ab << 2));
	err1 = phx_read_u32(phy_innr + (0x000043ab << 2));
	err2 = phx_read_u32(phy_innr + (0x000045ab << 2));
	err3 = phx_read_u32(phy_innr + (0x000047ab << 2));
	g_total_errbits += (err0 + err1 + err2 + err3);
	uart_printf("index%d prbs status 0x41ab 0x43ab 0x45ab 0x47ab = 0x%x 0x%x 0x%x 0x%x\r\n",
			g_loop_times / 3, err0, err1, err2, err3);

	if ((g_loop_times % 3) == 2) {
		if (g_total_errbits < g_min_errbits) {
			g_min_errbits = g_total_errbits;
			g_best_index = g_loop_times / 3;
		}
		uart_printf("---------------------------------- index%d err_cnt = 0x%x\r\n",
				g_loop_times / 3, g_total_errbits);
		g_total_errbits = 0;
	}
	g_loop_times++;
	if (g_loop_times == 3 * 64)
		uart_printf("---------------------------------- best index = %d err_cnt = 0x%x\r\n",
				g_best_index, g_min_errbits);
#else
	do {
		udelay(100);
		// PHY_PMA_XCVR_CTRL (verify bist is synced and no errors)
		val1 = phx_read_u32(phy_innr + (0x0000f000 << 2));
		val2 = phx_read_u32(phy_innr + (0x0000f100 << 2));
		val3 = phx_read_u32(phy_innr + (0x0000f200 << 2));
		val4 = phx_read_u32(phy_innr + (0x0000f300 << 2));
		uart_printf("phy status 0x%x 0x%x 0x%x 0x%x\r\n", val1, val2, val3, val4);
        val1 &= 2;
        val2 &= 2;
        val3 &= 2;
        val4 &= 2;
    } while((val1 == 0) || (val2 == 0) || (val3 == 0) || (val4 == 0));

    u32 check_times = 50;
    do {
		udelay(800000);
        // RX_BIST_ERRCNT_PREG (check the actual error count)
		uart_printf("rab%d prbs status 0x41ab + 0x200 * n: 0x%x 0x%x 0x%x 0x%x expect 0x0\r\n", base_addr,
                phx_read_u32(phy_innr + (0x000041ab << 2)),
                phx_read_u32(phy_innr + (0x000043ab << 2)),
                phx_read_u32(phy_innr + (0x000045ab << 2)),
                phx_read_u32(phy_innr + (0x000047ab << 2)));
		uart_printf("-----------------------------------------------------------------\r\n");
    } while(check_times--);
#endif
}

void rab_phy_bist_tx_inject_error(sriox base_addr)
{
    u32 phy_innr;

    phy_innr = SRIO_PHY_INNER_ADDR(base_addr);

    // inject error
	uart_printf("inject error bits\r\n");
    // TX_BIST_CONTROLS_PREG force_error
    phx_write_u32(phy_innr + (0x00006078 << 2), 0x115);

    udelay(2000000);
    // if loopback, check self's rx prbs status, otherwise check another rab's rx prbs status
#if !PHY_SERIAL_LOOPBACK
    phy_innr = SRIO_PHY_INNER_ADDR(!base_addr);
#endif
	uart_printf("prbs status 0xf000 + 0x100 * n: 0x%x 0x%x 0x%x 0x%x expect 0xa02a\r\n",
            phx_read_u32(phy_innr + (0x0000f000 << 2)),
            phx_read_u32(phy_innr + (0x0000f100 << 2)),
            phx_read_u32(phy_innr + (0x0000f200 << 2)),
            phx_read_u32(phy_innr + (0x0000f300 << 2)));
	uart_printf("prbs status 0x41ab + 0x200 * n: 0x%x 0x%x 0x%x 0x%x expect 0x1 ~ 0x5\r\n",
            phx_read_u32(phy_innr + (0x000041ab << 2)),
            phx_read_u32(phy_innr + (0x000043ab << 2)),
            phx_read_u32(phy_innr + (0x000045ab << 2)),
            phx_read_u32(phy_innr + (0x000047ab << 2)));
	uart_printf("-----------------------------------------------------------------\r\n");
}
#endif

void rab_phy_tx_ffe_para_search_result(sriox base_addr)
{
    u32 phy_innr;

    phy_innr = SRIO_PHY_INNER_ADDR(base_addr);

    udelay(100);
    // tx ffe para search result
	uart_printf("tx ffe para search result PHY_PMA_ISO_TX_FSLF[8:13]: 0x%x 0x%x 0x%x 0x%x\r\n",
            phx_read_u32(phy_innr + (0xf008 << 2)),
            phx_read_u32(phy_innr + (0xf108 << 2)),
            phx_read_u32(phy_innr + (0xf208 << 2)),
            phx_read_u32(phy_innr + (0xf308 << 2)));//0x2D0F 0x2D0F 0x2D0F 0x2D0F
}

void rab_link_and_mode_status(sriox base_addr)
{
    // check rab link status
	uart_printf("rab%d 0x158 + 0x40 * n: 0x%x 0x%x 0x%x 0x%x\r\n", base_addr,
            rab_page_read(base_addr, 0x158 + 0x40 * 0),
            rab_page_read(base_addr, 0x158 + 0x40 * 1),
            rab_page_read(base_addr, 0x158 + 0x40 * 2),
            rab_page_read(base_addr, 0x158 + 0x40 * 3));
    // check rab mode, x4 or x2 or x1
	uart_printf("rab%d 0x168 + 0x40 * n: 0x%x 0x%x 0x%x 0x%x\r\n", base_addr,
            rab_page_read(base_addr, 0x168 + 0x40 * 0),
            rab_page_read(base_addr, 0x168 + 0x40 * 1),
            rab_page_read(base_addr, 0x168 + 0x40 * 2),
            rab_page_read(base_addr, 0x168 + 0x40 * 3));
	uart_printf("-----------------------------------------------------\r\n");
}

void rab_wait_for_linkup(sriox base_addr)
{
	u32 tmp, tmp1, tmp2, tmp3;
	u32 count = 0;
	u32 rab_phy_wrap = SRIO_PHY_WRAPPER_ADDR(base_addr);

    do {
        tmp = rab_page_read(base_addr, 0x158);
        if ((count % 20000) == 0) {
            udelay(100);
			uart_printf("0x158: 0x%x\r\n",tmp);
        }

#if 1
        if (((count % 200000) == 0) && (count != 0)) {
            if (tmp & 0x70700)
                rab_page_write(base_addr, 0x158, tmp); // clear input and ouput error

            uart_printf("reset phy\r\n");
			phx_write_u32(rab_phy_wrap + 0x00000000, 0x00000000);
            udelay(10);
			phx_write_u32(rab_phy_wrap + 0x00000000, 0x00000011);
        }
#endif
        count++;
    } while ((tmp & 0x2) == 0 || (tmp & 0x70700));
    uart_printf("rab%d linked!\r\n", base_addr);
    rab_link_and_mode_status(base_addr);
}

void rab_phy_rx_termination_set(sriox base_addr, u32 enable)
{
    u32 phy_wrap = SRIO_PHY_WRAPPER_ADDR(base_addr);
    
    phx_write_u32(phy_wrap+0x48, enable);
    phx_write_u32(phy_wrap+0x60, enable);
    phx_write_u32(phy_wrap+0x78, enable);
    phx_write_u32(phy_wrap+0x90, enable);
}

void rab_dlb_loopback(sriox SRIO)
{
    u32 tmp;

    tmp = rab_page_read(SRIO, 0x10100 + 0x100 * 0);
    rab_page_write(SRIO, 0x10100 + 0x100 * 0, tmp | (1 << 28)); //controllor  loopback
    tmp = rab_page_read(SRIO, 0x10100 + 0x100 * 1);
    rab_page_write(SRIO, 0x10100 + 0x100 * 1, tmp | (1 << 28)); //controllor  loopback
    tmp = rab_page_read(SRIO, 0x10100 + 0x100 * 2);
    rab_page_write(SRIO, 0x10100 + 0x100 * 2, tmp | (1 << 28)); //controllor  loopback
    tmp = rab_page_read(SRIO, 0x10100 + 0x100 * 3);
    rab_page_write(SRIO, 0x10100 + 0x100 * 3, tmp | (1 << 28)); //controllor  loopback

    uart_printf("----------- rab dlb loopback -----------------\r\n");
}

void rab_llb_loopback(sriox SRIO)
{
    u32 tmp;

    tmp = rab_page_read(SRIO, 0x10100 + 0x100 * 0);
    rab_page_write(SRIO, 0x10100 + 0x100 * 0, tmp | (1 << 23)); //controllor  loopback
    tmp = rab_page_read(SRIO, 0x10100 + 0x100 * 1);
    rab_page_write(SRIO, 0x10100 + 0x100 * 1, tmp | (1 << 23)); //controllor  loopback
    tmp = rab_page_read(SRIO, 0x10100 + 0x100 * 2);
    rab_page_write(SRIO, 0x10100 + 0x100 * 2, tmp | (1 << 23)); //controllor  loopback
    tmp = rab_page_read(SRIO, 0x10100 + 0x100 * 3);
    rab_page_write(SRIO, 0x10100 + 0x100 * 3, tmp | (1 << 23)); //controllor  loopback

    uart_printf("----------- rab llb loopback -----------------\r\n");
}

void rab_config(sriox base_addr, u32 data)
{
    rab_page_write(base_addr, 0x10108 + 0x100 * 0, data);
    rab_page_write(base_addr, 0x10108 + 0x100 * 1, data);
    rab_page_write(base_addr, 0x10108 + 0x100 * 2, data);
    rab_page_write(base_addr, 0x10108 + 0x100 * 3, data);
}


void rab_nwrite_config(sriox base_addr)
{
	u32 rab_innr;

	rab_innr = SRIO_RAB_INNER_ADDR(base_addr);

	phx_write_u32(rab_innr + 0x0008, 0x00000003); //addr the default page
	phx_write_u32(rab_innr + 0x0180, 0x00000003); //end pio /memory map
	phx_write_u32(rab_innr + 0x0200, 0x00010003); //amap ctrl
	phx_write_u32(rab_innr + 0x0204, 0x00000400); //amap size 1k
	phx_write_u32(rab_innr + 0x0208, 0x00000001); //amap abar 1k
	phx_write_u32(rab_innr + 0x020c, 0x00000001); //amap rbar 1k
	uart_printf("rab%d \n", base_addr);
}

void rab_maintance_config(sriox base_addr)
{
	u32 rab_innr;

	rab_innr = SRIO_RAB_INNER_ADDR(base_addr);

	phx_write_u32(rab_innr + 0x0008, 0x00000003); //addr the default page
	phx_write_u32(rab_innr + 0x0028, 0x00000001); //end pio /memory map
	uart_printf("rab%d \n", base_addr);
}


void rab_phy_link_ini_config_1G25(sriox base_addr)
{
    u32 rab_innr;
    u32 rab_wrap;
    u32 phy_wrap;
    u32 phy_innr;

    rab_innr = SRIO_RAB_INNER_ADDR(base_addr);
    rab_wrap = SRIO_RAB_WRAPPER_ADDR(base_addr);
    phy_wrap = SRIO_PHY_WRAPPER_ADDR(base_addr);
    phy_innr = SRIO_PHY_INNER_ADDR(base_addr);

    uart_printf("rab%d 1G25 x4 mode config\r\n", base_addr);

    // reset rab and phy
//    if(hr3_board_type == 2)
//    {
		phx_write_u32(rab_wrap+0x00000024, 0x00000010);/*ldf 20230627 add:: hc3080b*/
		phx_write_u32(phy_wrap+0x00000000, 0x00000000);/*ldf 20230627 add:: hc3080b*/
//    }
    
    phx_write_u32(rab_wrap+0x00000010, 0x00014021); // x4 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00010021); // x1x1x1x1 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00011021); // x2x1x1 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00012021); // x1x1x2 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00013021); // x2x2 mode

    // port_mode:1.25G idle1 20bits
    phx_write_u32(rab_wrap+0x00000014, 0x00000112);
    phx_write_u32(rab_wrap+0x00000018, 0x00000112);
    phx_write_u32(rab_wrap+0x0000001c, 0x00000112);
    phx_write_u32(rab_wrap+0x00000020, 0x00000112);
    // cdr lock time
    phx_write_u32(rab_wrap+0x00000028, 30000);
    // rx_ready_time
    phx_write_u32(rab_wrap+0x0000002c, 32000);
    // Config reg name: srio_reset
//    if(hr3_board_type == 2)
//    {
    	phx_write_u32(rab_wrap+0x00000024, 0x00000101/*0x00000001*/);/*ldf 20230627 modify:: hc3080b*/
//    }
//    else
//    {
//    	phx_write_u32(rab_wrap+0x00000024, 0x00000001);
//    }
    udelay(100);

    // Config reg name: phy apb reset release
//    if(hr3_board_type == 2)
//    {
    	phx_write_u32(phy_wrap+0x00000000, 0x00100000);/*ldf 20230627 add:: hc3080b*/
//    }
    
    phx_write_u32(phy_wrap+0x50, 0x300c40); // tx deemphasis
    phx_write_u32(phy_wrap+0x68, 0x300c40);
    phx_write_u32(phy_wrap+0x80, 0x300c40);
    phx_write_u32(phy_wrap+0x98, 0x300c40);
    
//    if(hr3_board_type == 2)
//    {
    	rab_phy_rx_termination_set(base_addr, 0);/*ldf 20230627 add:: hc3080b*/
//    }

    // pma_rx_eq_training_data_valid_ln
    phx_write_u32(phy_wrap+0x58, 0x1);
    phx_write_u32(phy_wrap+0x70, 0x1);
    phx_write_u32(phy_wrap+0x88, 0x1);
    phx_write_u32(phy_wrap+0xa0, 0x1);

    // Config reg name: serdes_width_p0
    phx_write_u32(phy_wrap+0x00000018, 0x20101005);
    // Config reg name: serdes_width_p1
    phx_write_u32(phy_wrap+0x00000024, 0x20101005);
    // Config reg name: serdes_width_p2
    phx_write_u32(phy_wrap+0x00000030, 0x20101005);
    // Config reg name: serdes_width_p3
    phx_write_u32(phy_wrap+0x0000003c, 0x20101005);
    // Config reg name: pma_link_mode
    phx_write_u32(phy_wrap+0x00000004, 0x00000000); // x4, 1 link
    //phx_write_u32(phy_wrap+0x00000004, 0x00003210); // x1x1x1x1, 4 link
    //phx_write_u32(phy_wrap+0x00000004, 0x00001100); // x2x2, 2 link
    // Config reg name: pma_refclk_sel
    phx_write_u32(phy_wrap+0x00000008, 0x00003200);

    // Config reg name: PHY_PLL_CFG
    phx_write_u32(phy_innr+(0x0000C00E << 2), 0x00000000);
    // Config reg name: sdosccal_clk_cnt_preg
    phx_write_u32(phy_innr+(0x0000006E << 2), 0x00000180);
    // Config reg name: cmn_rescal_ctrla_preg
    phx_write_u32(phy_innr+(0x000000A0 << 2), 0x00000031);
    // Config reg name: cmn_refrcv1_preg
    phx_write_u32(phy_innr+(0x000000B8 << 2), 0x00006100);
    // Config reg name: cmn_plllc_gen_preg
    phx_write_u32(phy_innr+(0x00000042 << 2), 0x00000002);
    // Config reg name: cmn_plllc_fbdiv_int_mode0_preg
    phx_write_u32(phy_innr+(0x00000043 << 2), 0x00000020);
    // Config reg name: cmn_plllc_lf_coeff_mode0_preg
    phx_write_u32(phy_innr+(0x0000004A << 2), 0x00002106);
    // Config reg name: cmn_plllc_locksearch_preg
    phx_write_u32(phy_innr+(0x0000004C << 2), 0x00000000);
    // Config reg name: cmn_plllc_bwcal_mode0_preg
    phx_write_u32(phy_innr+(0x00000050 << 2), 0x00000000);
    // Config reg name: cmn_plllc_avdd_preg
    phx_write_u32(phy_innr+(0x0000005A << 2), 0x00000CDB);
    // Config reg name: cmn_plllcsm_pllen_tmr_preg
    phx_write_u32(phy_innr+(0x0000005D << 2), 0x00000027);
    // Config reg name: cmn_plllcsm_pllpre_tmr_preg
    phx_write_u32(phy_innr+(0x0000005E << 2), 0x00000062);
    // Config reg name: cmn_plllc_ss_time_stepsize_mode_preg
    phx_write_u32(phy_innr+(0x00000062 << 2), 0x00000800);
    // Config reg name: cmn_cdbpwriso_ovrd_preg
    phx_write_u32(phy_innr+(0x00000023 << 2), 0x0000413B);
#if 1
    // Config reg name: det_standec_a_preg
    phx_write_u32(phy_innr+(0x00006000 << 2), 0x0000ED08);
    // Config reg name: det_standec_d_preg
    phx_write_u32(phy_innr+(0x00006003 << 2), 0x0000691E);
    // Config reg name: pllctrl_subrate_preg
    phx_write_u32(phy_innr+(0x0000603A << 2), 0x00000013);
    // Config reg name: pllctrl_gen_d_preg
    phx_write_u32(phy_innr+(0x0000603E << 2), 0x00000106);
    // Config reg name: dfe_biastrim_preg
    phx_write_u32(phy_innr+(0x0000604C << 2), 0x00000D33);
    // Config reg name: drvctrl_atten_preg
    phx_write_u32(phy_innr+(0x0000606A << 2), 0x00000100);
    // Config reg name: drvctrl_boost_preg
    phx_write_u32(phy_innr+(0x0000606F << 2), 0x00009502);
    // Config reg name: rx_creq_fltr_a_mode0_preg
    phx_write_u32(phy_innr+(0x00006088 << 2), 0x00008843);
    // Config reg name: creq_fsmclk_sel_preg
    phx_write_u32(phy_innr+(0x00006092 << 2), 0x00003220);

    // Config reg name: creq_eq_ctrl_preg
    phx_write_u32(phy_innr+(0x00006093 << 2), 0x00000102);

    // Config reg name: dfe_ecmp_ratesel_preg
    phx_write_u32(phy_innr+(0x000060C0 << 2), 0x00004110);
    // Config reg name: dfe_smp_ratesel_preg
    phx_write_u32(phy_innr+(0x000060C1 << 2), 0x00004110);
    // Config reg name: deq_errcmp_ctrl_preg
    phx_write_u32(phy_innr+(0x000060D0 << 2), 0x00003E3E);
    // Config reg name: deq_tau_ctrl2_preg
    phx_write_u32(phy_innr+(0x00006151 << 2), 0x00002008);
    // Config reg name: deq_tau_ctrl6_preg
    phx_write_u32(phy_innr+(0x00006155 << 2), 0x0000003F);
    // Config reg name: deq_tau_epioffset_mode_preg
    phx_write_u32(phy_innr+(0x00006158 << 2), 0x00006422);
    // Config reg name: deq_concur_epioffset_mode_preg
    phx_write_u32(phy_innr+(0x00006159 << 2), 0x00006622);
    // Config reg name: deq_pictrl_preg
    phx_write_u32(phy_innr+(0x00006161 << 2), 0x00000023);
    // Config reg name: datpeak_thresh_hi_ceph_0_preg
    phx_write_u32(phy_innr+(0x000061D8 << 2), 0x00000019);
    // Config reg name: datpeak_thresh_hi_ceph_1_preg
    phx_write_u32(phy_innr+(0x000061D9 << 2), 0x00007FF1);
    // Config reg name: datpeak_thresh_hi_ceph_2_preg
    phx_write_u32(phy_innr+(0x000061DA << 2), 0x00000028);
    // Config reg name: datpeak_thresh_hi_ceph_3_preg
    phx_write_u32(phy_innr+(0x000061DB << 2), 0x00000028);
    // Config reg name: datpeak_thresh_hi_ceph_4_preg
    phx_write_u32(phy_innr+(0x000061DC << 2), 0x00007FFF);
    // Config reg name: datpeak_thresh_lo_ceph_0_preg
    phx_write_u32(phy_innr+(0x000061DE << 2), 0x00007FC3);
    // Config reg name: datpeak_thresh_lo_ceph_1_preg
    phx_write_u32(phy_innr+(0x000061DF << 2), 0x00007F81);
    // Config reg name: datpeak_thresh_lo_ceph_2_preg
    phx_write_u32(phy_innr+(0x000061E0 << 2), 0x00007FC3);
    // Config reg name: datpeak_thresh_lo_ceph_3_preg
    phx_write_u32(phy_innr+(0x000061E1 << 2), 0x00007FC3);
    // Config reg name: datpeak_thresh_lo_ceph_4_preg
    phx_write_u32(phy_innr+(0x000061E2 << 2), 0x00007F81);
    // Config reg name: datpeak_scaling_tap_ceph_0_preg
    phx_write_u32(phy_innr+(0x000061E4 << 2), 0x00000000);
    // Config reg name: datpeak_scaling_tap_ceph_1_preg
    phx_write_u32(phy_innr+(0x000061E5 << 2), 0x00001000);
    // Config reg name: datpeak_scaling_tap_ceph_2_preg
    phx_write_u32(phy_innr+(0x000061E6 << 2), 0x00001092);
    // Config reg name: datpeak_scaling_tap_ceph_3_preg
    phx_write_u32(phy_innr+(0x000061E7 << 2), 0x00001092);
    // Config reg name: datpeak_scaling_tap_ceph_4_preg
    phx_write_u32(phy_innr+(0x000061E8 << 2), 0x00001000);
#else
    // lane 0
    phx_write_u32(phy_innr+(0x00004000 << 2), 0x0000ED08);
    phx_write_u32(phy_innr+(0x00004003 << 2), 0x0000691E);
    phx_write_u32(phy_innr+(0x0000403A << 2), 0x00000013);
    phx_write_u32(phy_innr+(0x0000403E << 2), 0x00000106);
    phx_write_u32(phy_innr+(0x0000404C << 2), 0x00000D33);
    phx_write_u32(phy_innr+(0x0000406A << 2), 0x00000100);
    phx_write_u32(phy_innr+(0x0000406F << 2), 0x00009502);
    phx_write_u32(phy_innr+(0x00004088 << 2), 0x00008843);
    phx_write_u32(phy_innr+(0x00004092 << 2), 0x00003220);
    // Config reg name: creq_eq_ctrl_preg
    phx_write_u32(phy_innr+(0x00004093 << 2), 0x00000102);
    phx_write_u32(phy_innr+(0x000040C0 << 2), 0x00004110);
    phx_write_u32(phy_innr+(0x000040C1 << 2), 0x00004110);
    phx_write_u32(phy_innr+(0x000040D0 << 2), 0x00003E3E);
    phx_write_u32(phy_innr+(0x00004151 << 2), 0x00002008);
    phx_write_u32(phy_innr+(0x00004155 << 2), 0x0000003F);
    phx_write_u32(phy_innr+(0x00004158 << 2), 0x00006422);
    phx_write_u32(phy_innr+(0x00004159 << 2), 0x00006622);
    phx_write_u32(phy_innr+(0x00004161 << 2), 0x00000023);
    phx_write_u32(phy_innr+(0x000041D8 << 2), 0x00000019);
    phx_write_u32(phy_innr+(0x000041D9 << 2), 0x00007FF1);
    phx_write_u32(phy_innr+(0x000041DA << 2), 0x00000028);
    phx_write_u32(phy_innr+(0x000041DB << 2), 0x00000028);
    phx_write_u32(phy_innr+(0x000041DC << 2), 0x00007FFF);
    phx_write_u32(phy_innr+(0x000041DE << 2), 0x00007FC3);
    phx_write_u32(phy_innr+(0x000041DF << 2), 0x00007F81);
    phx_write_u32(phy_innr+(0x000041E0 << 2), 0x00007FC3);
    phx_write_u32(phy_innr+(0x000041E1 << 2), 0x00007FC3);
    phx_write_u32(phy_innr+(0x000041E2 << 2), 0x00007F81);
    phx_write_u32(phy_innr+(0x000041E4 << 2), 0x00000000);
    phx_write_u32(phy_innr+(0x000041E5 << 2), 0x00001000);
    phx_write_u32(phy_innr+(0x000041E6 << 2), 0x00001092);
    phx_write_u32(phy_innr+(0x000041E7 << 2), 0x00001092);
    phx_write_u32(phy_innr+(0x000041E8 << 2), 0x00001000);
    // lane 1
    phx_write_u32(phy_innr+(0x00004200 << 2), 0x0000ED08);
    phx_write_u32(phy_innr+(0x00004203 << 2), 0x0000691E);
    phx_write_u32(phy_innr+(0x0000423A << 2), 0x00000013);
    phx_write_u32(phy_innr+(0x0000423E << 2), 0x00000106);
    phx_write_u32(phy_innr+(0x0000424C << 2), 0x00000D33);
    phx_write_u32(phy_innr+(0x0000426A << 2), 0x00000100);
    phx_write_u32(phy_innr+(0x0000426F << 2), 0x00009502);
    phx_write_u32(phy_innr+(0x00004288 << 2), 0x00008843);
    phx_write_u32(phy_innr+(0x00004292 << 2), 0x00003220);
    // Config reg name: creq_eq_ctrl_preg
    phx_write_u32(phy_innr+(0x00004293 << 2), 0x00000102);
    phx_write_u32(phy_innr+(0x000042C0 << 2), 0x00004110);
    phx_write_u32(phy_innr+(0x000042C1 << 2), 0x00004110);
    phx_write_u32(phy_innr+(0x000042D0 << 2), 0x00003E3E);
    phx_write_u32(phy_innr+(0x00004351 << 2), 0x00002008);
    phx_write_u32(phy_innr+(0x00004355 << 2), 0x0000003F);
    phx_write_u32(phy_innr+(0x00004358 << 2), 0x00006422);
    phx_write_u32(phy_innr+(0x00004359 << 2), 0x00006622);
    phx_write_u32(phy_innr+(0x00004361 << 2), 0x00000023);
    phx_write_u32(phy_innr+(0x000043D8 << 2), 0x00000019);
    phx_write_u32(phy_innr+(0x000043D9 << 2), 0x00007FF1);
    phx_write_u32(phy_innr+(0x000043DA << 2), 0x00000028);
    phx_write_u32(phy_innr+(0x000043DB << 2), 0x00000028);
    phx_write_u32(phy_innr+(0x000043DC << 2), 0x00007FFF);
    phx_write_u32(phy_innr+(0x000043DE << 2), 0x00007FC3);
    phx_write_u32(phy_innr+(0x000043DF << 2), 0x00007F81);
    phx_write_u32(phy_innr+(0x000043E0 << 2), 0x00007FC3);
    phx_write_u32(phy_innr+(0x000043E1 << 2), 0x00007FC3);
    phx_write_u32(phy_innr+(0x000043E2 << 2), 0x00007F81);
    phx_write_u32(phy_innr+(0x000043E4 << 2), 0x00000000);
    phx_write_u32(phy_innr+(0x000043E5 << 2), 0x00001000);
    phx_write_u32(phy_innr+(0x000043E6 << 2), 0x00001092);
    phx_write_u32(phy_innr+(0x000043E7 << 2), 0x00001092);
    phx_write_u32(phy_innr+(0x000043E8 << 2), 0x00001000);
    // lane 2
    phx_write_u32(phy_innr+(0x00004400 << 2), 0x0000ED08);
    phx_write_u32(phy_innr+(0x00004403 << 2), 0x0000691E);
    phx_write_u32(phy_innr+(0x0000443A << 2), 0x00000013);
    phx_write_u32(phy_innr+(0x0000443E << 2), 0x00000106);
    phx_write_u32(phy_innr+(0x0000444C << 2), 0x00000D33);
    phx_write_u32(phy_innr+(0x0000446A << 2), 0x00000100);
    phx_write_u32(phy_innr+(0x0000446F << 2), 0x00009502);
    phx_write_u32(phy_innr+(0x00004488 << 2), 0x00008843);
    phx_write_u32(phy_innr+(0x00004492 << 2), 0x00003220);
    // Config reg name: creq_eq_ctrl_preg
    phx_write_u32(phy_innr+(0x00004493 << 2), 0x00000102);
    phx_write_u32(phy_innr+(0x000044C0 << 2), 0x00004110);
    phx_write_u32(phy_innr+(0x000044C1 << 2), 0x00004110);
    phx_write_u32(phy_innr+(0x000044D0 << 2), 0x00003E3E);
    phx_write_u32(phy_innr+(0x00004551 << 2), 0x00002008);
    phx_write_u32(phy_innr+(0x00004555 << 2), 0x0000003F);
    phx_write_u32(phy_innr+(0x00004558 << 2), 0x00006422);
    phx_write_u32(phy_innr+(0x00004559 << 2), 0x00006622);
    phx_write_u32(phy_innr+(0x00004561 << 2), 0x00000023);
    phx_write_u32(phy_innr+(0x000045D8 << 2), 0x00000019);
    phx_write_u32(phy_innr+(0x000045D9 << 2), 0x00007FF1);
    phx_write_u32(phy_innr+(0x000045DA << 2), 0x00000028);
    phx_write_u32(phy_innr+(0x000045DB << 2), 0x00000028);
    phx_write_u32(phy_innr+(0x000045DC << 2), 0x00007FFF);
    phx_write_u32(phy_innr+(0x000045DE << 2), 0x00007FC3);
    phx_write_u32(phy_innr+(0x000045DF << 2), 0x00007F81);
    phx_write_u32(phy_innr+(0x000045E0 << 2), 0x00007FC3);
    phx_write_u32(phy_innr+(0x000045E1 << 2), 0x00007FC3);
    phx_write_u32(phy_innr+(0x000045E2 << 2), 0x00007F81);
    phx_write_u32(phy_innr+(0x000045E4 << 2), 0x00000000);
    phx_write_u32(phy_innr+(0x000045E5 << 2), 0x00001000);
    phx_write_u32(phy_innr+(0x000045E6 << 2), 0x00001092);
    phx_write_u32(phy_innr+(0x000045E7 << 2), 0x00001092);
    phx_write_u32(phy_innr+(0x000045E8 << 2), 0x00001000);
    // lane 3
    phx_write_u32(phy_innr+(0x00004600 << 2), 0x0000ED08);
    phx_write_u32(phy_innr+(0x00004603 << 2), 0x0000691E);
    phx_write_u32(phy_innr+(0x0000463A << 2), 0x00000013);
    phx_write_u32(phy_innr+(0x0000463E << 2), 0x00000106);
    phx_write_u32(phy_innr+(0x0000464C << 2), 0x00000D33);
    phx_write_u32(phy_innr+(0x0000466A << 2), 0x00000100);
    phx_write_u32(phy_innr+(0x0000466F << 2), 0x00009502);
    phx_write_u32(phy_innr+(0x00004688 << 2), 0x00008843);
    phx_write_u32(phy_innr+(0x00004692 << 2), 0x00003220);
    // Config reg name: creq_eq_ctrl_preg
    phx_write_u32(phy_innr+(0x00004693 << 2), 0x00000102);
    phx_write_u32(phy_innr+(0x000046C0 << 2), 0x00004110);
    phx_write_u32(phy_innr+(0x000046C1 << 2), 0x00004110);
    phx_write_u32(phy_innr+(0x000046D0 << 2), 0x00003E3E);
    phx_write_u32(phy_innr+(0x00004751 << 2), 0x00002008);
    phx_write_u32(phy_innr+(0x00004755 << 2), 0x0000003F);
    phx_write_u32(phy_innr+(0x00004758 << 2), 0x00006422);
    phx_write_u32(phy_innr+(0x00004759 << 2), 0x00006622);
    phx_write_u32(phy_innr+(0x00004761 << 2), 0x00000023);
    phx_write_u32(phy_innr+(0x000047D8 << 2), 0x00000019);
    phx_write_u32(phy_innr+(0x000047D9 << 2), 0x00007FF1);
    phx_write_u32(phy_innr+(0x000047DA << 2), 0x00000028);
    phx_write_u32(phy_innr+(0x000047DB << 2), 0x00000028);
    phx_write_u32(phy_innr+(0x000047DC << 2), 0x00007FFF);
    phx_write_u32(phy_innr+(0x000047DE << 2), 0x00007FC3);
    phx_write_u32(phy_innr+(0x000047DF << 2), 0x00007F81);
    phx_write_u32(phy_innr+(0x000047E0 << 2), 0x00007FC3);
    phx_write_u32(phy_innr+(0x000047E1 << 2), 0x00007FC3);
    phx_write_u32(phy_innr+(0x000047E2 << 2), 0x00007F81);
    phx_write_u32(phy_innr+(0x000047E4 << 2), 0x00000000);
    phx_write_u32(phy_innr+(0x000047E5 << 2), 0x00001000);
    phx_write_u32(phy_innr+(0x000047E6 << 2), 0x00001092);
    phx_write_u32(phy_innr+(0x000047E7 << 2), 0x00001092);
    phx_write_u32(phy_innr+(0x000047E8 << 2), 0x00001000);
#endif

#if PHY_BIST_CONFIG
    // tx rx prbs
    rab_phy_bist_config(base_addr);
#endif

#if PHY_SERIAL_LOOPBACK
    phx_write_u32(phy_innr+(0x00006050 << 2), 0xe04); // LANE_LOOPBACK_CTRL_PREG
    phx_write_u32(phy_innr+(0x00006048 << 2), 0x0); // TX_BIASTRIM_PREG
#endif

    // boot complete
    rab_page_write(base_addr, 0x404, 0x00000001);

    // en port rx tx
    rab_page_write(base_addr, 0x15c + 0x40 * 0, 0x00600000);
    rab_page_write(base_addr, 0x15c + 0x40 * 1, 0x00600000);
    rab_page_write(base_addr, 0x15c + 0x40 * 2, 0x00600000);
    rab_page_write(base_addr, 0x15c + 0x40 * 3, 0x00600000);
    rab_config(base_addr, 0x1104000c); // 1.25G x4 idle1
    //rab_config(base_addr, 0x1100000c); // 1.25G x1x1x1x1 idle1
    //rab_config(base_addr, 0x1103000c); // 1.25G x2x2 idle1
    //rab_dlb_loopback(base_addr);

    // PHY Reset Free
    // Config reg name: phy_reset
//    if(hr3_board_type == 2)
//    {
    	phx_write_u32(phy_wrap+0x00000000, 0x00111111/*0x00011111*/);/*ldf 20230627 modify:: hc3080b*/
//    }
//    else
//    {
//    	phx_write_u32(phy_wrap+0x00000000, 0x00011111);
//    }

//	uart_printf("pma_cmn_ready[8:8]  phy_wrap 0xC = 0x%x\r\n", phx_read_u32(phy_wrap + 0xc));
//	uart_printf("tx deemphasis[0:17] phy_wrap 0x50 0x68 0x80 0x98 = 0x%x 0x%x 0x%x 0x%x\r\n",
//			phx_read_u32(phy_wrap + 0x50),
//			phx_read_u32(phy_wrap + 0x68),
//			phx_read_u32(phy_wrap + 0x80),
//			phx_read_u32(phy_wrap + 0x98));
//	rab_phy_tx_ffe_para_search_result(base_addr);
}

/* phy config_2G5 */
void rab_phy_link_ini_config_2G5(sriox base_addr)
{
    u32 rab_innr;
    u32 rab_wrap;
    u32 phy_wrap;
    u32 phy_innr;

    rab_innr = SRIO_RAB_INNER_ADDR(base_addr);
    rab_wrap = SRIO_RAB_WRAPPER_ADDR(base_addr);
    phy_wrap = SRIO_PHY_WRAPPER_ADDR(base_addr);
    phy_innr = SRIO_PHY_INNER_ADDR(base_addr);

    uart_printf("rab%d 2G5 x4 mode config\r\n", base_addr);

    // reset rab and phy
//    if(hr3_board_type == 2)
//    {
		phx_write_u32(rab_wrap+0x00000024, 0x00000010);/*ldf 20230627 add:: hc3080b*/
		phx_write_u32(phy_wrap+0x00000000, 0x00000000);/*ldf 20230627 add:: hc3080b*/
//    }
    
    phx_write_u32(rab_wrap+0x00000010, 0x00014021); // x4 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00010021); // x1x1x1x1 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00011021); // x2x1x1 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00012021); // x1x1x2 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00013021); // x2x2 mode
    
    // Config reg name: phy apb reset release
//    if(hr3_board_type == 2)
//    {
    	phx_write_u32(phy_wrap+0x00000000, 0x00100000);/*ldf 20230627 add:: hc3080b*/
//    }
    
    // port_mode: 2.5G idle1 20bits
    phx_write_u32(rab_wrap+0x00000014, 0x00000212);
    phx_write_u32(rab_wrap+0x00000018, 0x00000212);
    phx_write_u32(rab_wrap+0x0000001C, 0x00000212);
    phx_write_u32(rab_wrap+0x00000020, 0x00000212);
    // srio cdr lock time, rx_ready_time
    phx_write_u32(rab_wrap+0x00000028, 30000);
    phx_write_u32(rab_wrap+0x0000002c, 32000);
    // Config reg name: srio_reset
//    if(hr3_board_type == 2)
//    {
    	phx_write_u32(rab_wrap+0x00000024, 0x00000101/*0x00000001*/);/*ldf 20230627 modify:: hc3080b*/
//    }
//    else
//    {
//    	phx_write_u32(rab_wrap+0x00000024, 0x00000001);
//    }
    udelay(100);

#if TX_FFE_PARA_TRAVERSE
	phx_write_u32(phy_wrap+0x50, 0x300000 + ((g_loop_times / 3) << 6)); // tx deemphasis
	phx_write_u32(phy_wrap+0x68, 0x300000 + ((g_loop_times / 3) << 6));
	phx_write_u32(phy_wrap+0x80, 0x300000 + ((g_loop_times / 3) << 6));
	phx_write_u32(phy_wrap+0x98, 0x300000 + ((g_loop_times / 3) << 6));
#else
	//phx_write_u32(phy_wrap+0x50, 0x300c40);
	//phx_write_u32(phy_wrap+0x68, 0x300c40);
	//phx_write_u32(phy_wrap+0x80, 0x300c40);
	//phx_write_u32(phy_wrap+0x98, 0x300c40);
	phx_write_u32(phy_wrap+0x50, 0x880);
	phx_write_u32(phy_wrap+0x68, 0x880);
	phx_write_u32(phy_wrap+0x80, 0x880);
	phx_write_u32(phy_wrap+0x98, 0x880);
#endif

    // pma_rx_eq_training_data_valid_ln
    phx_write_u32(phy_wrap+0x58, 0x1);
    phx_write_u32(phy_wrap+0x70, 0x1);
    phx_write_u32(phy_wrap+0x88, 0x1);
    phx_write_u32(phy_wrap+0xa0, 0x1);
    
//    if(hr3_board_type == 2)
//    {
    	rab_phy_rx_termination_set(base_addr, 0);/*ldf 20230627 add:: hc3080b*/
//    }

    // serdes_width
    // port0~3 20bit
    phx_write_u32(phy_wrap+0x00000018, 0x20101005);
    phx_write_u32(phy_wrap+0x00000024, 0x20101005);
    phx_write_u32(phy_wrap+0x00000030, 0x20101005);
    phx_write_u32(phy_wrap+0x0000003c, 0x20101005);
    // Config reg name: pma_link_mode
    phx_write_u32(phy_wrap+0x00000004, 0x00000000); // x4, 1 link
    //phx_write_u32(phy_wrap+0x00000004, 0x00003210); // x1x1x1x1, 4 link
    // Config reg name: pma_refclk_sel
    phx_write_u32(phy_wrap+0x00000008, 0x00003200);

    //////////////////////////////////////
    //  PHY config for SRIO 2G NS      //
    //////////////////////////////////////
    // Config reg name: PHY_PLL_CFG
    phx_write_u32(phy_innr+(0x0000C00E << 2), 0x00000000);
    // Config reg name: sdosccal_clk_cnt_preg
    phx_write_u32(phy_innr+(0x0000006E << 2), 0x00000180);
    // Config reg name: cmn_rescal_ctrla_preg
    phx_write_u32(phy_innr+(0x000000A0 << 2), 0x00000031);
    // Config reg name: cmn_refrcv1_preg
    phx_write_u32(phy_innr+(0x000000B8 << 2), 0x00006100);
    // Config reg name: cmn_plllc_gen_preg
    phx_write_u32(phy_innr+(0x00000042 << 2), 0x00000002);
    // Config reg name: cmn_plllc_fbdiv_int_mode0_preg
    phx_write_u32(phy_innr+(0x00000043 << 2), 0x00000020);
    // Config reg name: cmn_plllc_lf_coeff_mode0_preg
    phx_write_u32(phy_innr+(0x0000004A << 2), 0x00002106);
    // Config reg name: cmn_plllc_locksearch_preg
    phx_write_u32(phy_innr+(0x0000004C << 2), 0x00000000);
    // Config reg name: cmn_plllc_bwcal_mode0_preg
    phx_write_u32(phy_innr+(0x00000050 << 2), 0x00000000);
    // Config reg name: cmn_plllc_avdd_preg
    phx_write_u32(phy_innr+(0x0000005A << 2), 0x00000CDB);
    // Config reg name: cmn_plllcsm_pllen_tmr_preg
    phx_write_u32(phy_innr+(0x0000005D << 2), 0x00000027);
    // Config reg name: cmn_plllcsm_pllpre_tmr_preg
    phx_write_u32(phy_innr+(0x0000005E << 2), 0x00000062);
    // Config reg name: cmn_plllc_ss_time_stepsize_mode_preg
    phx_write_u32(phy_innr+(0x00000062 << 2), 0x00000800);
    // Config reg name: cmn_cdbpwriso_ovrd_preg
    phx_write_u32(phy_innr+(0x00000023 << 2), 0x0000413B);

    // port 0 cfg
    // Config reg name: det_standec_a_preg
    phx_write_u32(phy_innr+(0x00006000 << 2), 0x0000ED08);
    // Config reg name: pllctrl_gen_d_preg
    phx_write_u32(phy_innr+(0x0000603E << 2), 0x00000101);
    // Config reg name: dfe_biastrim_preg
    phx_write_u32(phy_innr+(0x0000604C << 2), 0x00000D33);
    // Config reg name: drvctrl_atten_preg
    phx_write_u32(phy_innr+(0x0000606A << 2), 0x00000100);
    // Config reg name: drvctrl_boost_preg
    phx_write_u32(phy_innr+(0x0000606F << 2), 0x00009502);
    // Config reg name: rx_creq_fltr_a_mode0_preg
    phx_write_u32(phy_innr+(0x00006088 << 2), 0x00008843);
    // Config reg name: creq_fsmclk_sel_preg
    phx_write_u32(phy_innr+(0x00006092 << 2), 0x00003220);

    // Config reg name: creq_eq_ctrl_preg
    phx_write_u32(phy_innr+(0x00006093 << 2), 0x00000102);

    // Config reg name: dfe_ecmp_ratesel_preg
    phx_write_u32(phy_innr+(0x000060C0 << 2), 0x00004110);
    // Config reg name: dfe_smp_ratesel_preg
    phx_write_u32(phy_innr+(0x000060C1 << 2), 0x00004110);
    // Config reg name: deq_errcmp_ctrl_preg
    phx_write_u32(phy_innr+(0x000060D0 << 2), 0x00003E3E);
    // Config reg name: deq_tau_ctrl2_preg
    phx_write_u32(phy_innr+(0x00006151 << 2), 0x00002008);
    // Config reg name: deq_tau_ctrl6_preg
    phx_write_u32(phy_innr+(0x00006155 << 2), 0x0000003F);
    // Config reg name: deq_tau_epioffset_mode_preg
    phx_write_u32(phy_innr+(0x00006158 << 2), 0x00006422);
    // Config reg name: deq_concur_epioffset_mode_preg
    phx_write_u32(phy_innr+(0x00006159 << 2), 0x00006622);
    // Config reg name: deq_pictrl_preg
    phx_write_u32(phy_innr+(0x00006161 << 2), 0x00000023);
    // Config reg name: datpeak_thresh_hi_ceph_0_preg
    phx_write_u32(phy_innr+(0x000061D8 << 2), 0x00000019);
    // Config reg name: datpeak_thresh_hi_ceph_1_preg
    phx_write_u32(phy_innr+(0x000061D9 << 2), 0x00007FF1);
    // Config reg name: datpeak_thresh_hi_ceph_2_preg
    phx_write_u32(phy_innr+(0x000061DA << 2), 0x00000028);
    // Config reg name: datpeak_thresh_hi_ceph_3_preg
    phx_write_u32(phy_innr+(0x000061DB << 2), 0x00000028);
    // Config reg name: datpeak_thresh_hi_ceph_4_preg
    phx_write_u32(phy_innr+(0x000061DC << 2), 0x00007FFF);
    // Config reg name: datpeak_thresh_lo_ceph_0_preg
    phx_write_u32(phy_innr+(0x000061DE << 2), 0x00007FC3);
    // Config reg name: datpeak_thresh_lo_ceph_1_preg
    phx_write_u32(phy_innr+(0x000061DF << 2), 0x00007F81);
    // Config reg name: datpeak_thresh_lo_ceph_2_preg
    phx_write_u32(phy_innr+(0x000061E0 << 2), 0x00007FC3);
    // Config reg name: datpeak_thresh_lo_ceph_3_preg
    phx_write_u32(phy_innr+(0x000061E1 << 2), 0x00007FC3);
    // Config reg name: datpeak_thresh_lo_ceph_4_preg
    phx_write_u32(phy_innr+(0x000061E2 << 2), 0x00007F81);
    // Config reg name: datpeak_scaling_tap_ceph_0_preg
    phx_write_u32(phy_innr+(0x000061E4 << 2), 0x00000000);
    // Config reg name: datpeak_scaling_tap_ceph_1_preg
    phx_write_u32(phy_innr+(0x000061E5 << 2), 0x00001000);
    // Config reg name: datpeak_scaling_tap_ceph_2_preg
    phx_write_u32(phy_innr+(0x000061E6 << 2), 0x00001092);
    // Config reg name: datpeak_scaling_tap_ceph_3_preg
    phx_write_u32(phy_innr+(0x000061E7 << 2), 0x00001092);
    // Config reg name: datpeak_scaling_tap_ceph_4_preg
    phx_write_u32(phy_innr+(0x000061E8 << 2), 0x00001000);

#if PHY_BIST_CONFIG
    // tx rx prbs
    rab_phy_bist_config(base_addr);
#endif

#if PHY_SERIAL_LOOPBACK
    phx_write_u32(phy_innr+(0x00006050 << 2), 0xe04); // LANE_LOOPBACK_CTRL_PREG
    phx_write_u32(phy_innr+(0x00006048 << 2), 0x0); // TX_BIASTRIM_PREG
#endif

    // boot complete
    rab_page_write(base_addr, 0x404, 0x00000001);
    // en port rx tx
    rab_page_write(base_addr, 0x15c + 0x40 * 0, 0x00600000);
    rab_page_write(base_addr, 0x15c + 0x40 * 1, 0x00600000);
    rab_page_write(base_addr, 0x15c + 0x40 * 2, 0x00600000);
    rab_page_write(base_addr, 0x15c + 0x40 * 3, 0x00600000);
    rab_config(base_addr, 0x2104000c); // 2G5 x4 idle1
    //rab_config(base_addr, 0x2100000c); // 2G5 x1x1x1x1 idle1
    //rab_dlb_loopback(base_addr);

    // PHY Reset Free
    // Config reg name: phy_reset
//    if(hr3_board_type == 2)
//    {
    	phx_write_u32(phy_wrap+0x00000000, 0x00111111/*0x00011111*/);/*ldf 20230627 modify:: hc3080b*/
//    }
//    else
//    {
//    	phx_write_u32(phy_wrap+0x00000000, 0x00011111);
//    }

//	uart_printf("pma_cmn_ready[8:8]  phy_wrap 0xC = 0x%x\r\n", phx_read_u32(phy_wrap + 0xc));
//	uart_printf("tx deemphasis[0:17] phy_wrap 0x50 0x68 0x80 0x98 = 0x%x 0x%x 0x%x 0x%x\r\n",
//			phx_read_u32(phy_wrap + 0x50),
//			phx_read_u32(phy_wrap + 0x68),
//			phx_read_u32(phy_wrap + 0x80),
//			phx_read_u32(phy_wrap + 0x98));
//	rab_phy_tx_ffe_para_search_result(base_addr);
}

/* phy config_3G125 */
void rab_phy_link_ini_config_3G125(sriox base_addr)
{
    u32 rab_innr;
    u32 rab_wrap;
    u32 phy_wrap;
    u32 phy_innr;

    rab_innr = SRIO_RAB_INNER_ADDR(base_addr);
    rab_wrap = SRIO_RAB_WRAPPER_ADDR(base_addr);
    phy_wrap = SRIO_PHY_WRAPPER_ADDR(base_addr);
    phy_innr = SRIO_PHY_INNER_ADDR(base_addr);

    uart_printf("rab%d 3G125 x4 mode config\r\n", base_addr);

    // reset rab and phy
//    if(hr3_board_type == 2)
//    {
		phx_write_u32(rab_wrap+0x00000024, 0x00000010);/*ldf 20230627 add:: hc3080b*/
		phx_write_u32(phy_wrap+0x00000000, 0x00000000);/*ldf 20230627 add:: hc3080b*/
//    }
    
    phx_write_u32(rab_wrap+0x00000010, 0x00014021); // x4 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00010021); // x1x1x1x1 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00011021); // x2x1x1 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00012021); // x1x1x2 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00013021); // x2x2 mode
    
    // port_mode: 3.125G idle1 20bits
    phx_write_u32(rab_wrap+0x00000014, 0x00000312);
    phx_write_u32(rab_wrap+0x0000001C, 0x00000312);
    phx_write_u32(rab_wrap+0x00000018, 0x00000312);
    phx_write_u32(rab_wrap+0x00000020, 0x00000312);

    // Config reg name: srio_cdr_ready
    phx_write_u32(rab_wrap+0x00000028, 30000);
    // Config reg name: srio_rx_ready
    phx_write_u32(rab_wrap+0x0000002c, 32000);
    // Config reg name: srio_reset
//    if(hr3_board_type == 2)
//    {
    	phx_write_u32(rab_wrap+0x00000024, 0x00000101/*0x00000001*/);/*ldf 20230627 modify:: hc3080b*/
//    }
//    else
//    {
//    	phx_write_u32(rab_wrap+0x00000024, 0x00000001);
//    }
    udelay(100);

    // Config reg name: phy apb reset release
//    if(hr3_board_type == 2)
//    {
    	phx_write_u32(phy_wrap+0x00000000, 0x00100000);/*ldf 20230627 add:: hc3080b*/
//    }
    
    phx_write_u32(phy_wrap+0x50, 0x880); // tx deemphasis
    phx_write_u32(phy_wrap+0x68, 0x880);
    phx_write_u32(phy_wrap+0x80, 0x880);
    phx_write_u32(phy_wrap+0x98, 0x880);
    //phx_write_u32(phy_wrap+0x50, 0x300c40); // tx deemphasis
    //phx_write_u32(phy_wrap+0x68, 0x300c40);
    //phx_write_u32(phy_wrap+0x80, 0x300c40);
    //phx_write_u32(phy_wrap+0x98, 0x300c40);
    
//    if(hr3_board_type == 2)
//    {
    	rab_phy_rx_termination_set(base_addr, 0);/*ldf 20230627 add:: hc3080b*/
//    }

    // pma_rx_eq_training_data_valid_ln
    phx_write_u32(phy_wrap+0x58, 0x1);
    phx_write_u32(phy_wrap+0x70, 0x1);
    phx_write_u32(phy_wrap+0x88, 0x1);
    phx_write_u32(phy_wrap+0xa0, 0x1);
    

    // Config reg name: serdes_width_p0
    phx_write_u32(phy_wrap+0x00000018, 0x20101005);
    // Config reg name: serdes_width_p1
    phx_write_u32(phy_wrap+0x00000024, 0x20101005);
    // Config reg name: serdes_width_p2
    phx_write_u32(phy_wrap+0x00000030, 0x20101005);
    // Config reg name: serdes_width_p3
    phx_write_u32(phy_wrap+0x0000003c, 0x20101005);
    // Config reg name: pma_link_mode
    phx_write_u32(phy_wrap+0x00000004, 0x00000000);
    // Config reg name: pma_refclk_sel
    phx_write_u32(phy_wrap+0x00000008, 0x00003200);

    // Config reg name: PHY_PLL_CFG
    phx_write_u32(phy_innr+(0x0000C00E << 2), 0x00000000);
    // Config reg name: sdosccal_clk_cnt_preg
    phx_write_u32(phy_innr+(0x0000006E << 2), 0x00000180);
    // Config reg name: cmn_rescal_ctrla_preg
    phx_write_u32(phy_innr+(0x000000A0 << 2), 0x00000031);
    // Config reg name: cmn_refrcv1_preg
    phx_write_u32(phy_innr+(0x000000B8 << 2), 0x00006100);
    // Config reg name: cmn_plllc_gen_preg
    phx_write_u32(phy_innr+(0x00000042 << 2), 0x00000002);
    // Config reg name: cmn_plllc_fbdiv_int_mode0_preg
    phx_write_u32(phy_innr+(0x00000043 << 2), 0x0000001E);
    // Config reg name: cmn_plllc_lf_coeff_mode0_preg
    phx_write_u32(phy_innr+(0x0000004A << 2), 0x00002106);
    // Config reg name: cmn_plllc_locksearch_preg
    phx_write_u32(phy_innr+(0x0000004C << 2), 0x00000000);
    // Config reg name: cmn_plllc_clk0_preg
    phx_write_u32(phy_innr+(0x0000004E << 2), 0x00008102);
    // Config reg name: cmn_plllc_bwcal_mode0_preg
    phx_write_u32(phy_innr+(0x00000050 << 2), 0x00000000);
    // Config reg name: cmn_plllc_avdd_preg
    phx_write_u32(phy_innr+(0x0000005A << 2), 0x00000CDB);
    // Config reg name: cmn_plllcsm_pllen_tmr_preg
    phx_write_u32(phy_innr+(0x0000005D << 2), 0x00000027);
    // Config reg name: cmn_plllcsm_pllpre_tmr_preg
    phx_write_u32(phy_innr+(0x0000005E << 2), 0x00000062);
    // Config reg name: cmn_plllc_ss_time_stepsize_mode_preg
    phx_write_u32(phy_innr+(0x00000062 << 2), 0x00000800);
    // Config reg name: cmn_cdbpwriso_ovrd_preg
    phx_write_u32(phy_innr+(0x00000023 << 2), 0x0000413B);
    // Config reg name: det_standec_a_preg
    phx_write_u32(phy_innr+(0x00006000 << 2), 0x0000ED08);
    // Config reg name: det_standec_d_preg
    phx_write_u32(phy_innr+(0x00006003 << 2), 0x0000691E);
    // Config reg name: det_standec_e_preg
    phx_write_u32(phy_innr+(0x00006004 << 2), 0x00000292);
    // Config reg name: pllctrl_fbdiv_mode01_preg
    phx_write_u32(phy_innr+(0x00006039 << 2), 0x00000104);
    // Config reg name: pllctrl_gen_d_preg
    phx_write_u32(phy_innr+(0x0000603E << 2), 0x00000106);
    // Config reg name: dfe_biastrim_preg
    phx_write_u32(phy_innr+(0x0000604C << 2), 0x00000D33);
    // Config reg name: drvctrl_atten_preg
    phx_write_u32(phy_innr+(0x0000606A << 2), 0x00000100);
    // Config reg name: drvctrl_boost_preg
    phx_write_u32(phy_innr+(0x0000606F << 2), 0x00009502);
    // Config reg name: rx_creq_fltr_a_mode0_preg
    phx_write_u32(phy_innr+(0x00006088 << 2), 0x00008C43);
    // Config reg name: creq_fsmclk_sel_preg
    phx_write_u32(phy_innr+(0x00006092 << 2), 0x00003221);

    // Config reg name: creq_eq_ctrl_preg
    phx_write_u32(phy_innr+(0x00006093 << 2), 0x00000102);

    // Config reg name: dfe_ecmp_ratesel_preg
    phx_write_u32(phy_innr+(0x000060C0 << 2), 0x00004110);
    // Config reg name: dfe_smp_ratesel_preg
    phx_write_u32(phy_innr+(0x000060C1 << 2), 0x00004110);
    // Config reg name: deq_errcmp_ctrl_preg
    phx_write_u32(phy_innr+(0x000060D0 << 2), 0x00003E3E);
    // Config reg name: deq_tau_ctrl2_preg
    phx_write_u32(phy_innr+(0x00006151 << 2), 0x00002008);
    // Config reg name: deq_tau_ctrl6_preg
    phx_write_u32(phy_innr+(0x00006155 << 2), 0x0000003F);
    // Config reg name: deq_tau_epioffset_mode_preg
    phx_write_u32(phy_innr+(0x00006158 << 2), 0x00006422);
    // Config reg name: deq_concur_epioffset_mode_preg
    phx_write_u32(phy_innr+(0x00006159 << 2), 0x00006622);
    // Config reg name: deq_pictrl_preg
    phx_write_u32(phy_innr+(0x00006161 << 2), 0x00000023);
    // Config reg name: cpical_tmrval_mode0_preg
    phx_write_u32(phy_innr+(0x00006171 << 2), 0x00000660);
    // Config reg name: datpeak_thresh_hi_ceph_0_preg
    phx_write_u32(phy_innr+(0x000061D8 << 2), 0x00000019);
    // Config reg name: datpeak_thresh_hi_ceph_1_preg
    phx_write_u32(phy_innr+(0x000061D9 << 2), 0x00007FF1);
    // Config reg name: datpeak_thresh_hi_ceph_2_preg
    phx_write_u32(phy_innr+(0x000061DA << 2), 0x00000028);
    // Config reg name: datpeak_thresh_hi_ceph_3_preg
    phx_write_u32(phy_innr+(0x000061DB << 2), 0x00000028);
    // Config reg name: datpeak_thresh_hi_ceph_4_preg
    phx_write_u32(phy_innr+(0x000061DC << 2), 0x00007FFF);
    // Config reg name: datpeak_thresh_lo_ceph_0_preg
    phx_write_u32(phy_innr+(0x000061DE << 2), 0x00007FC3);
    // Config reg name: datpeak_thresh_lo_ceph_1_preg
    phx_write_u32(phy_innr+(0x000061DF << 2), 0x00007F81);
    // Config reg name: datpeak_thresh_lo_ceph_2_preg
    phx_write_u32(phy_innr+(0x000061E0 << 2), 0x00007FC3);
    // Config reg name: datpeak_thresh_lo_ceph_3_preg
    phx_write_u32(phy_innr+(0x000061E1 << 2), 0x00007FC3);
    // Config reg name: datpeak_thresh_lo_ceph_4_preg
    phx_write_u32(phy_innr+(0x000061E2 << 2), 0x00007F81);
    // Config reg name: datpeak_scaling_tap_ceph_0_preg
    phx_write_u32(phy_innr+(0x000061E4 << 2), 0x00000000);
    // Config reg name: datpeak_scaling_tap_ceph_1_preg
    phx_write_u32(phy_innr+(0x000061E5 << 2), 0x00001000);
    // Config reg name: datpeak_scaling_tap_ceph_2_preg
    phx_write_u32(phy_innr+(0x000061E6 << 2), 0x00001092);
    // Config reg name: datpeak_scaling_tap_ceph_3_preg
    phx_write_u32(phy_innr+(0x000061E7 << 2), 0x00001092);
    // Config reg name: datpeak_scaling_tap_ceph_4_preg
    phx_write_u32(phy_innr+(0x000061E8 << 2), 0x00001000);

#if PHY_BIST_CONFIG
    // tx rx prbs
    rab_phy_bist_config(base_addr);
#endif

#if PHY_SERIAL_LOOPBACK
    phx_write_u32(phy_innr+(0x00006050 << 2), 0xe04); // LANE_LOOPBACK_CTRL_PREG
    phx_write_u32(phy_innr+(0x00006048 << 2), 0x0); // TX_BIASTRIM_PREG
#endif

    // boot complete
    rab_page_write(base_addr, 0x404, 0x00000001);

    rab_page_write(base_addr, 0x15c + 0x40 * 0, 0x00600000);
    rab_page_write(base_addr, 0x15c + 0x40 * 1, 0x00600000);
    rab_page_write(base_addr, 0x15c + 0x40 * 2, 0x00600000);
    rab_page_write(base_addr, 0x15c + 0x40 * 3, 0x00600000);
    rab_config(base_addr, 0x3104000c); // 3.125G x4 idle1
    //rab_dlb_loopback(base_addr);

    // PHY Reset Free
    // Config reg name: phy_reset
//    if(hr3_board_type == 2)
//    {
    	phx_write_u32(phy_wrap+0x00000000, 0x00111111/*0x00011111*/);/*ldf 20230627 modify:: hc3080b*/
//    }else
//    {
//    	phx_write_u32(phy_wrap+0x00000000, 0x00011111);
//    }

//	uart_printf("pma_cmn_ready[8:8]  phy_wrap 0xC = 0x%x\r\n", phx_read_u32(phy_wrap + 0xc));
//	uart_printf("tx deemphasis[0:17] phy_wrap 0x50 0x68 0x80 0x98 = 0x%x 0x%x 0x%x 0x%x\r\n",
//			phx_read_u32(phy_wrap + 0x50),
//			phx_read_u32(phy_wrap + 0x68),
//			phx_read_u32(phy_wrap + 0x80),
//			phx_read_u32(phy_wrap + 0x98));
//	rab_phy_tx_ffe_para_search_result(base_addr);
}

/* phy config_5G */
void rab_phy_link_ini_config_5G(sriox base_addr)
{
    u32 rab_innr;
    u32 rab_wrap;
    u32 phy_wrap;
    u32 phy_innr;

    rab_innr = SRIO_RAB_INNER_ADDR(base_addr);
    rab_wrap = SRIO_RAB_WRAPPER_ADDR(base_addr);
    phy_wrap = SRIO_PHY_WRAPPER_ADDR(base_addr);
    phy_innr = SRIO_PHY_INNER_ADDR(base_addr);

    uart_printf("rab%d 5G x4 mode config\r\n", base_addr);

    // reset rab and phy
//    if(hr3_board_type == 2)
//    {
		phx_write_u32(rab_wrap+0x00000024, 0x00000010);/*ldf 20230627 add:: hc3080b*/
		phx_write_u32(phy_wrap+0x00000000, 0x00000000);/*ldf 20230627 add:: hc3080b*/
//    }
    
    phx_write_u32(rab_wrap+0x00000010, 0x00014021); // x4 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00010021); // x1x1x1x1 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00011021); // x2x1x1 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00012021); // x1x1x2 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00013021); // x2x2 mode
    
    // port_mode: 5G idle2 20bits
    phx_write_u32(rab_wrap+0x00000014, 0x00000422);
    phx_write_u32(rab_wrap+0x0000001C, 0x00000422);
    phx_write_u32(rab_wrap+0x00000018, 0x00000422);
    phx_write_u32(rab_wrap+0x00000020, 0x00000422);

    // Config reg name: cdr lock time
    phx_write_u32(rab_wrap+0x00000028, 30000);
    // Config reg name: rx_ready_time
    phx_write_u32(rab_wrap+0x0000002c, 32000);
    // Config reg name: srio_reset
//    if(hr3_board_type == 2)
//    {
    	phx_write_u32(rab_wrap+0x00000024, 0x00000101/*0x00000001*/);/*ldf 20230627 modify:: hc3080b*/
//    }
//    else
//    {
//    	phx_write_u32(rab_wrap+0x00000024, 0x00000001);
//    }
    udelay(100);

    // Config reg name: phy apb reset release
//    if(hr3_board_type == 2)
//    {
    	phx_write_u32(phy_wrap+0x00000000, 0x00100000);/*ldf 20230627 add:: hc3080b*/
//    }
    
#if TX_FFE_PARA_TRAVERSE
	phx_write_u32(phy_wrap+0x50, (g_loop_times / 3) << 6); // tx deemphasis
	phx_write_u32(phy_wrap+0x68, (g_loop_times / 3) << 6);
	phx_write_u32(phy_wrap+0x80, (g_loop_times / 3) << 6);
	phx_write_u32(phy_wrap+0x98, (g_loop_times / 3) << 6);
#else
	phx_write_u32(phy_wrap+0x50, 0x7c0); // tx deemphasis
	phx_write_u32(phy_wrap+0x68, 0x7c0);
	phx_write_u32(phy_wrap+0x80, 0x7c0);
	phx_write_u32(phy_wrap+0x98, 0x7c0);
	//phx_write_u32(phy_wrap+0x50, 0x6540); // tx deemphasis
	//phx_write_u32(phy_wrap+0x68, 0x6540);
	//phx_write_u32(phy_wrap+0x80, 0x6540);
	//phx_write_u32(phy_wrap+0x98, 0x6540);
#endif

//    if(hr3_board_type == 2)
//    {
    	rab_phy_rx_termination_set(base_addr, 0);/*ldf 20230627 add:: hc3080b*/
//    }

    // pma_rx_eq_training_data_valid_ln
    phx_write_u32(phy_wrap+0x58, 0x1);
    phx_write_u32(phy_wrap+0x70, 0x1);
    phx_write_u32(phy_wrap+0x88, 0x1);
    phx_write_u32(phy_wrap+0xa0, 0x1);

    // Config reg name: serdes_width_p0
    phx_write_u32(phy_wrap+0x00000018, 0x20101005);
    // Config reg name: serdes_width_p1
    phx_write_u32(phy_wrap+0x00000024, 0x20101005);
    // Config reg name: serdes_width_p2
    phx_write_u32(phy_wrap+0x00000030, 0x20101005);
    // Config reg name: serdes_width_p3
    phx_write_u32(phy_wrap+0x0000003c, 0x20101005);
    // Config reg name: pma_link_mode
    phx_write_u32(phy_wrap+0x00000004, 0x00000000);
    // Config reg name: pma_refclk_sel
    //phx_write_u32(phy_wrap+0x00000008, 0x00122000);
    phx_write_u32(phy_wrap+0x00000008, 0x00003200);

    // Config reg name: PHY_PLL_CFG
    phx_write_u32(phy_innr+(0x0000C00E << 2), 0x00000000);
    // Config reg name: sdosccal_clk_cnt_preg
    phx_write_u32(phy_innr+(0x0000006E << 2), 0x00000180);
    // Config reg name: cmn_rescal_ctrla_preg
    phx_write_u32(phy_innr+(0x000000A0 << 2), 0x00000031);
    // Config reg name: cmn_refrcv1_preg
    phx_write_u32(phy_innr+(0x000000B8 << 2), 0x00006100);
    // Config reg name: cmn_plllc_gen_preg
    phx_write_u32(phy_innr+(0x00000042 << 2), 0x00000002);
    // Config reg name: cmn_plllc_fbdiv_int_mode0_preg
    phx_write_u32(phy_innr+(0x00000043 << 2), 0x00000020);
    // Config reg name: cmn_plllc_lf_coeff_mode0_preg
    phx_write_u32(phy_innr+(0x0000004A << 2), 0x00002106);
    // Config reg name: cmn_plllc_locksearch_preg
    phx_write_u32(phy_innr+(0x0000004C << 2), 0x00000000);
    // Config reg name: cmn_plllc_bwcal_mode0_preg
    phx_write_u32(phy_innr+(0x00000050 << 2), 0x00000000);
    // Config reg name: cmn_plllc_avdd_preg
    phx_write_u32(phy_innr+(0x0000005A << 2), 0x00000CDB);
    // Config reg name: cmn_plllcsm_pllen_tmr_preg
    phx_write_u32(phy_innr+(0x0000005D << 2), 0x00000027);
    // Config reg name: cmn_plllcsm_pllpre_tmr_preg
    phx_write_u32(phy_innr+(0x0000005E << 2), 0x00000062);
    // Config reg name: cmn_plllc_ss_time_stepsize_mode_preg
    phx_write_u32(phy_innr+(0x00000062 << 2), 0x00000800);
    // Config reg name: cmn_cdbpwriso_ovrd_preg
    phx_write_u32(phy_innr+(0x00000023 << 2), 0x0000413B);
    // Config reg name: det_standec_a_preg
    phx_write_u32(phy_innr+(0x00006000 << 2), 0x0000ED08);
    // Config reg name: det_standec_e_preg
    phx_write_u32(phy_innr+(0x00006004 << 2), 0x00000292);
    // Config reg name: pllctrl_subrate_preg
    phx_write_u32(phy_innr+(0x0000603A << 2), 0x00000011);
    // Config reg name: dfe_biastrim_preg
    phx_write_u32(phy_innr+(0x0000604C << 2), 0x00000D33);
    // Config reg name: drvctrl_atten_preg
    phx_write_u32(phy_innr+(0x0000606A << 2), 0x00000000);
    // Config reg name: drvctrl_boost_preg
    phx_write_u32(phy_innr+(0x0000606F << 2), 0x00001402);
    // Config reg name: rx_creq_fltr_a_mode0_preg
    phx_write_u32(phy_innr+(0x00006088 << 2), 0x00008C43);
    // Config reg name: creq_fsmclk_sel_preg
    phx_write_u32(phy_innr+(0x00006092 << 2), 0x00003221);

    // Config reg name: creq_eq_ctrl_preg
    phx_write_u32(phy_innr+(0x00006093 << 2), 0x00000102);

    // Config reg name: dfe_ecmp_ratesel_preg
    phx_write_u32(phy_innr+(0x000060C0 << 2), 0x00004111);
    // Config reg name: dfe_smp_ratesel_preg
    phx_write_u32(phy_innr+(0x000060C1 << 2), 0x00004111);
    // Config reg name: deq_errcmp_ctrl_preg
    phx_write_u32(phy_innr+(0x000060D0 << 2), 0x00003E3E);
    // Config reg name: deq_tau_ctrl2_preg
    phx_write_u32(phy_innr+(0x00006151 << 2), 0x00002008);
    // Config reg name: deq_tau_ctrl6_preg
    phx_write_u32(phy_innr+(0x00006155 << 2), 0x0000003F);
    // Config reg name: deq_tau_epioffset_mode_preg
    phx_write_u32(phy_innr+(0x00006158 << 2), 0x00006422);
    // Config reg name: deq_concur_epioffset_mode_preg
    phx_write_u32(phy_innr+(0x00006159 << 2), 0x00006622);
    // Config reg name: deq_pictrl_preg
    phx_write_u32(phy_innr+(0x00006161 << 2), 0x00000023);
    // Config reg name: cpical_tmrval_mode0_preg
    phx_write_u32(phy_innr+(0x00006171 << 2), 0x00000660);
    // Config reg name: cpi_outbuf_ratesel_preg
    phx_write_u32(phy_innr+(0x0000617C << 2), 0x000000D5);
    // Config reg name: datpeak_thresh_hi_ceph_0_preg
    phx_write_u32(phy_innr+(0x000061D8 << 2), 0x00000019);
    // Config reg name: datpeak_thresh_hi_ceph_1_preg
    phx_write_u32(phy_innr+(0x000061D9 << 2), 0x00007FF1);
    // Config reg name: datpeak_thresh_hi_ceph_2_preg
    phx_write_u32(phy_innr+(0x000061DA << 2), 0x00000028);
    // Config reg name: datpeak_thresh_hi_ceph_3_preg
    phx_write_u32(phy_innr+(0x000061DB << 2), 0x00000028);
    // Config reg name: datpeak_thresh_hi_ceph_4_preg
    phx_write_u32(phy_innr+(0x000061DC << 2), 0x00007FFF);
    // Config reg name: datpeak_thresh_lo_ceph_0_preg
    phx_write_u32(phy_innr+(0x000061DE << 2), 0x00007FC3);
    // Config reg name: datpeak_thresh_lo_ceph_1_preg
    phx_write_u32(phy_innr+(0x000061DF << 2), 0x00007F81);
    // Config reg name: datpeak_thresh_lo_ceph_2_preg
    phx_write_u32(phy_innr+(0x000061E0 << 2), 0x00007FC3);
    // Config reg name: datpeak_thresh_lo_ceph_3_preg
    phx_write_u32(phy_innr+(0x000061E1 << 2), 0x00007FC3);
    // Config reg name: datpeak_thresh_lo_ceph_4_preg
    phx_write_u32(phy_innr+(0x000061E2 << 2), 0x00007F81);
    // Config reg name: datpeak_scaling_tap_ceph_0_preg
    phx_write_u32(phy_innr+(0x000061E4 << 2), 0x00000000);
    // Config reg name: datpeak_scaling_tap_ceph_1_preg
    phx_write_u32(phy_innr+(0x000061E5 << 2), 0x00001000);
    // Config reg name: datpeak_scaling_tap_ceph_2_preg
    phx_write_u32(phy_innr+(0x000061E6 << 2), 0x00001092);
    // Config reg name: datpeak_scaling_tap_ceph_3_preg
    phx_write_u32(phy_innr+(0x000061E7 << 2), 0x00001092);
    // Config reg name: datpeak_scaling_tap_ceph_4_preg
    phx_write_u32(phy_innr+(0x000061E8 << 2), 0x00001000);

    // boot complete
    rab_page_write(base_addr, 0x404, 0x00000001);
    // enable port tx rx
    rab_page_write(base_addr, 0x15c + 0x40 * 0, 0x00600000);
    rab_page_write(base_addr, 0x15c + 0x40 * 1, 0x00600000);
    rab_page_write(base_addr, 0x15c + 0x40 * 2, 0x00600000);
    rab_page_write(base_addr, 0x15c + 0x40 * 3, 0x00600000);
    rab_config(base_addr, 0x4204000c); // 5G x4 idle2
    //rab_config(base_addr, 0x4200000c); // 5G x1x1x1x1 idle2
    //rab_dlb_loopback(base_addr);

#if PHY_BIST_CONFIG
    // tx rx prbs
    rab_phy_bist_config(base_addr);
#endif

#if PHY_SERIAL_LOOPBACK
    phx_write_u32(phy_innr+(0x00006050 << 2), 0xe04); // LANE_LOOPBACK_CTRL_PREG
    phx_write_u32(phy_innr+(0x00006048 << 2), 0x0); // TX_BIASTRIM_PREG
#endif

    // PHY Reset Free
    // Config reg name: phy_reset
//    if(hr3_board_type == 2)
//    {
    	phx_write_u32(phy_wrap+0x00000000, 0x00111111/*0x00011111*/);/*ldf 20230627 modify:: hc3080b*/
//    }
//    else
//    {
//    	phx_write_u32(phy_wrap+0x00000000, 0x00011111);
//    }

//	uart_printf("pma_cmn_ready[8:8]  phy_wrap 0xC = 0x%x\r\n", phx_read_u32(phy_wrap + 0xc));
//	uart_printf("tx deemphasis[0:17] phy_wrap 0x50 0x68 0x80 0x98 = 0x%x 0x%x 0x%x 0x%x\r\n",
//			phx_read_u32(phy_wrap + 0x50),
//			phx_read_u32(phy_wrap + 0x68),
//			phx_read_u32(phy_wrap + 0x80),
//			phx_read_u32(phy_wrap + 0x98));
//	rab_phy_tx_ffe_para_search_result(base_addr);
}

void rab_phy_link_ini_config_6G25_100M(sriox base_addr)
{
    u32 rab_innr;
    u32 rab_wrap;
    u32 phy_wrap;
    u32 phy_innr;

    rab_innr = SRIO_RAB_INNER_ADDR(base_addr);
    rab_wrap = SRIO_RAB_WRAPPER_ADDR(base_addr);
    phy_wrap = SRIO_PHY_WRAPPER_ADDR(base_addr);
    phy_innr = SRIO_PHY_INNER_ADDR(base_addr);

    uart_printf("rab%d 6G25 x4 mode 100M config\r\n", base_addr);

    // reset rab and phy
//    if(hr3_board_type == 2)
//    {
		phx_write_u32(rab_wrap+0x00000024, 0x00000010);/*ldf 20230627 add:: hc3080b*/
		phx_write_u32(phy_wrap+0x00000000, 0x00000000);/*ldf 20230627 add:: hc3080b*/
//    }
		
    
    phx_write_u32(rab_wrap+0x00000010, 0x00014021); // x4 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00010021); // x1x1x1x1 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00011021); // x2x1x1 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00012021); // x1x1x2 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00013021); // x2x2 mode
    
    // 6.25G idle2 20bits
    phx_write_u32(rab_wrap+0x00000014, 0x00000522);
    phx_write_u32(rab_wrap+0x0000001C, 0x00000522);
    phx_write_u32(rab_wrap+0x00000018, 0x00000522);
    phx_write_u32(rab_wrap+0x00000020, 0x00000522);

    // cdr lock time
    phx_write_u32(rab_wrap+0x00000028, 30000);
    // rx_ready_time
    phx_write_u32(rab_wrap+0x0000002c, 32000);
    // Config reg name: srio_reset
//    if(hr3_board_type == 2)
//    {
    	phx_write_u32(rab_wrap+0x00000024, 0x00000101/*0x00000001*/);/*ldf 20230627 modify:: hc3080b*/
//    }
//    else
//    {
//    	phx_write_u32(rab_wrap+0x00000024, 0x00000001);
//    }
    udelay(100);

    // Config reg name: phy apb reset release
//    if(hr3_board_type == 2)
//    {
    	phx_write_u32(phy_wrap+0x00000000, 0x00100000);/*ldf 20230627 add:: hc3080b*/
//    }
    
    phx_write_u32(phy_wrap+0x50, 0x7c0); // tx deemphasis
    phx_write_u32(phy_wrap+0x68, 0x7c0);
    phx_write_u32(phy_wrap+0x80, 0x7c0);
    phx_write_u32(phy_wrap+0x98, 0x7c0);
    //phx_write_u32(phy_wrap+0x50, 0x6540); // tx deemphasis
    //phx_write_u32(phy_wrap+0x68, 0x6540);
    //phx_write_u32(phy_wrap+0x80, 0x6540);
    //phx_write_u32(phy_wrap+0x98, 0x6540);

//    if(hr3_board_type == 2)
//    {
    	rab_phy_rx_termination_set(base_addr, 0);/*ldf 20230627 add:: hc3080b*/
//    }
    
    // pma_rx_eq_training_data_valid_ln
    phx_write_u32(phy_wrap+0x58, 0x1);
    phx_write_u32(phy_wrap+0x70, 0x1);
    phx_write_u32(phy_wrap+0x88, 0x1);
    phx_write_u32(phy_wrap+0xa0, 0x1);

    // Config reg name: serdes_width_p0
    phx_write_u32(phy_wrap+0x00000018, 0x20101005);
    // Config reg name: serdes_width_p1
    phx_write_u32(phy_wrap+0x00000024, 0x20101005);
    // Config reg name: serdes_width_p2
    phx_write_u32(phy_wrap+0x00000030, 0x20101005);
    // Config reg name: serdes_width_p3
    phx_write_u32(phy_wrap+0x0000003c, 0x20101005);
    // Config reg name: pma_link_mode
    phx_write_u32(phy_wrap+0x00000004, 0x00000000);
    // Config reg name: pma_refclk_sel
    phx_write_u32(phy_wrap+0x00000008, 0x00002000);

    // Config reg name: PHY_PLL_CFG
    phx_write_u32(phy_innr+(0x0000C00E << 2), 0x00000000);
    // Config reg name: cmn_rescal_ctrla_preg
    phx_write_u32(phy_innr+(0x000000A0 << 2), 0x00000031);
    // Config reg name: cmn_plllc_fbdiv_int_mode0_preg
    phx_write_u32(phy_innr+(0x00000043 << 2), 0x0000002E);
    // Config reg name: cmn_plllc_fbdiv_frac_preg
    phx_write_u32(phy_innr+(0x00000044 << 2), 0x0000E000);
    // Config reg name: cmn_plllc_mode_preg
    phx_write_u32(phy_innr+(0x00000048 << 2), 0x0000000E);
    // Config reg name: cmn_plllc_lf_coeff_mode0_preg
    phx_write_u32(phy_innr+(0x0000004A << 2), 0x00002106);
    // Config reg name: cmn_plllc_locksearch_preg
    phx_write_u32(phy_innr+(0x0000004C << 2), 0x00000000);
    // Config reg name: cmn_plllc_clk0_preg
    phx_write_u32(phy_innr+(0x0000004E << 2), 0x00008102);
    // Config reg name: cmn_plllc_bwcal_mode0_preg
    phx_write_u32(phy_innr+(0x00000050 << 2), 0x00000000);
    // Config reg name: cmn_plllc_dsmcorr_preg
    phx_write_u32(phy_innr+(0x00000051 << 2), 0x00000581);
    // Config reg name: cmn_plllc_avdd_preg
    phx_write_u32(phy_innr+(0x0000005A << 2), 0x00000ADB);
    // Config reg name: cmn_plllc_ss_time_stepsize_mode_preg
    phx_write_u32(phy_innr+(0x00000062 << 2), 0x00000800);
    // Config reg name: cmn_plllc_lock_delay_ctrl_preg
    phx_write_u32(phy_innr+(0x00000063 << 2), 0x00000060);

    // Config reg name: cmn_cdbpwriso_ovrd_preg
    phx_write_u32(phy_innr+(0x00000023 << 2), 0x0000413B);
    // Config reg name: det_standec_a_preg
    phx_write_u32(phy_innr+(0x00006000 << 2), 0x0000ED08);
    // Config reg name: det_standec_d_preg
    phx_write_u32(phy_innr+(0x00006003 << 2), 0x0000691E);
    // Config reg name: det_standec_e_preg
    phx_write_u32(phy_innr+(0x00006004 << 2), 0x00000291);
    // Config reg name: pllctrl_fbdiv_mode01_preg
    phx_write_u32(phy_innr+(0x00006039 << 2), 0x00000104);
    // Config reg name: pllctrl_subrate_preg
    phx_write_u32(phy_innr+(0x0000603A << 2), 0x00000011);
    // Config reg name: pllctrl_gen_d_preg
    phx_write_u32(phy_innr+(0x0000603E << 2), 0x00000106);
    // Config reg name: dfe_biastrim_preg
    phx_write_u32(phy_innr+(0x0000604C << 2), 0x00000D33);
    // Config reg name: drvctrl_atten_preg
    phx_write_u32(phy_innr+(0x0000606A << 2), 0x00000000);
    // Config reg name: drvctrl_boost_preg
    phx_write_u32(phy_innr+(0x0000606F << 2), 0x00001402);
    // Config reg name: rx_creq_fltr_a_mode0_preg
    phx_write_u32(phy_innr+(0x00006088 << 2), 0x00008C43);
    // Config reg name: creq_fsmclk_sel_preg
    phx_write_u32(phy_innr+(0x00006092 << 2), 0x00003222);

    // Config reg name: creq_eq_ctrl_preg
    phx_write_u32(phy_innr+(0x00006093 << 2), 0x00000102);

    // Config reg name: dfe_ecmp_ratesel_preg
    phx_write_u32(phy_innr+(0x000060C0 << 2), 0x00004111);
    // Config reg name: dfe_smp_ratesel_preg
    phx_write_u32(phy_innr+(0x000060C1 << 2), 0x00004111);
    // Config reg name: deq_errcmp_ctrl_preg
    phx_write_u32(phy_innr+(0x000060D0 << 2), 0x00003E3E);
    // Config reg name: deq_tau_ctrl2_preg
    phx_write_u32(phy_innr+(0x00006151 << 2), 0x00002008);
    // Config reg name: deq_tau_ctrl6_preg
    phx_write_u32(phy_innr+(0x00006155 << 2), 0x0000003F);
    // Config reg name: deq_tau_epioffset_mode_preg
    phx_write_u32(phy_innr+(0x00006158 << 2), 0x00006422);
    // Config reg name: deq_concur_epioffset_mode_preg
    phx_write_u32(phy_innr+(0x00006159 << 2), 0x00006622);
    // Config reg name: deq_pictrl_preg
    phx_write_u32(phy_innr+(0x00006161 << 2), 0x00000023);
    // Config reg name: cpical_tmrval_mode0_preg
    phx_write_u32(phy_innr+(0x00006171 << 2), 0x00000330);
    // Config reg name: cpi_outbuf_ratesel_preg
    phx_write_u32(phy_innr+(0x0000617C << 2), 0x000000D5);
    // Config reg name: datpeak_thresh_hi_ceph_0_preg
    phx_write_u32(phy_innr+(0x000061D8 << 2), 0x00000019);
    // Config reg name: datpeak_thresh_hi_ceph_1_preg
    phx_write_u32(phy_innr+(0x000061D9 << 2), 0x00007FFF);
    // Config reg name: datpeak_thresh_hi_ceph_2_preg
    phx_write_u32(phy_innr+(0x000061DA << 2), 0x00000028);
    // Config reg name: datpeak_thresh_hi_ceph_3_preg
    phx_write_u32(phy_innr+(0x000061DB << 2), 0x00000028);
    // Config reg name: datpeak_thresh_hi_ceph_4_preg
    phx_write_u32(phy_innr+(0x000061DC << 2), 0x00007FFF);
    // Config reg name: datpeak_thresh_lo_ceph_0_preg
    phx_write_u32(phy_innr+(0x000061DE << 2), 0x00007FC3);
    // Config reg name: datpeak_thresh_lo_ceph_1_preg
    phx_write_u32(phy_innr+(0x000061DF << 2), 0x00007F81);
    // Config reg name: datpeak_thresh_lo_ceph_2_preg
    phx_write_u32(phy_innr+(0x000061E0 << 2), 0x00007FC3);
    // Config reg name: datpeak_thresh_lo_ceph_3_preg
    phx_write_u32(phy_innr+(0x000061E1 << 2), 0x00007FC3);
    // Config reg name: datpeak_thresh_lo_ceph_4_preg
    phx_write_u32(phy_innr+(0x000061E2 << 2), 0x00007F81);
    // Config reg name: datpeak_scaling_tap_ceph_0_preg
    phx_write_u32(phy_innr+(0x000061E4 << 2), 0x00000000);
    // Config reg name: datpeak_scaling_tap_ceph_1_preg
    phx_write_u32(phy_innr+(0x000061E5 << 2), 0x00001000);
    // Config reg name: datpeak_scaling_tap_ceph_2_preg
    phx_write_u32(phy_innr+(0x000061E6 << 2), 0x00001092);
    // Config reg name: datpeak_scaling_tap_ceph_3_preg
    phx_write_u32(phy_innr+(0x000061E7 << 2), 0x00001092);
    // Config reg name: datpeak_scaling_tap_ceph_4_preg
    phx_write_u32(phy_innr+(0x000061E8 << 2), 0x00001000);

    // enable port tx rx
    rab_page_write(base_addr, 0x15c + 0x40 * 0, 0x00600000);
    rab_page_write(base_addr, 0x15c + 0x40 * 1, 0x00600000);
    rab_page_write(base_addr, 0x15c + 0x40 * 2, 0x00600000);
    rab_page_write(base_addr, 0x15c + 0x40 * 3, 0x00600000);
    rab_config(base_addr, 0x5204000c); // 6.25G x4 idle2
    //rab_config(base_addr, 0x5200000c); // 6.25G x1x1x1x1 idle2
    //rab_dlb_loopback(base_addr);

    // boot complete
    rab_page_write(base_addr, 0x404, 0x00000001);

#if PHY_BIST_CONFIG
    // tx rx prbs
    rab_phy_bist_config(base_addr);
#endif

#if PHY_SERIAL_LOOPBACK
    phx_write_u32(phy_innr+(0x00006050 << 2), 0xe04); // LANE_LOOPBACK_CTRL_PREG
    phx_write_u32(phy_innr+(0x00006048 << 2), 0x0); // TX_BIASTRIM_PREG
#endif

    // PHY Reset Free
    // Config reg name: phy_reset
//    if(hr3_board_type == 2)
//    {
    	phx_write_u32(phy_wrap+0x00000000, 0x00111111/*0x00011111*/);/*ldf 20230627 modify:: hc3080b*/
//    }
//    else
//    {
//    	phx_write_u32(phy_wrap+0x00000000, 0x00011111);
//    }

//	uart_printf("pma_cmn_ready[8:8]  phy_wrap 0xC = 0x%x\r\n", phx_read_u32(phy_wrap + 0xc));
//	uart_printf("tx deemphasis[0:17] phy_wrap 0x50 0x68 0x80 0x98 = 0x%x 0x%x 0x%x 0x%x\r\n",
//			phx_read_u32(phy_wrap + 0x50),
//			phx_read_u32(phy_wrap + 0x68),
//			phx_read_u32(phy_wrap + 0x80),
//			phx_read_u32(phy_wrap + 0x98));
//	rab_phy_tx_ffe_para_search_result(base_addr);
}

/* phy config_6G25 */
void rab_phy_link_ini_config_6G25(sriox base_addr)
{
    u32 rab_innr;
    u32 rab_wrap;
    u32 phy_wrap;
    u32 phy_innr;

    rab_innr = SRIO_RAB_INNER_ADDR(base_addr);
    rab_wrap = SRIO_RAB_WRAPPER_ADDR(base_addr);
    phy_wrap = SRIO_PHY_WRAPPER_ADDR(base_addr);
    phy_innr = SRIO_PHY_INNER_ADDR(base_addr);

    uart_printf("rab%d 6G25 x4 mode config\r\n", base_addr);

    // reset rab and phy
//    if(hr3_board_type == 2)
//    {
		phx_write_u32(rab_wrap+0x00000024, 0x00000010);/*ldf 20230627 add:: hc3080b*/
		phx_write_u32(phy_wrap+0x00000000, 0x00000000);/*ldf 20230627 add:: hc3080b*/
//    }
    
    phx_write_u32(rab_wrap+0x00000010, 0x00014021); // x4 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00010021); // x1x1x1x1 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00011021); // x2x1x1 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00012021); // x1x1x2 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00013021); // x2x2 mode
    
    // 6.25G idle2 20bits
    phx_write_u32(rab_wrap+0x00000014, 0x00000522);
    phx_write_u32(rab_wrap+0x0000001C, 0x00000522);
    phx_write_u32(rab_wrap+0x00000018, 0x00000522);
    phx_write_u32(rab_wrap+0x00000020, 0x00000522);

    // cdr lock time
    phx_write_u32(rab_wrap+0x00000028, 30000);
    // rx_ready_time
    phx_write_u32(rab_wrap+0x0000002c, 32000);
    // Config reg name: srio_reset
//    if(hr3_board_type == 2)
//    {
    	phx_write_u32(rab_wrap+0x00000024, 0x00000101/*0x00000001*/);/*ldf 20230627 modify:: hc3080b*/
//    }
//    else
//    {
//    	phx_write_u32(rab_wrap+0x00000024, 0x00000001);
//    }
    udelay(100);

    // Config reg name: phy apb reset release
//    if(hr3_board_type == 2)
//    {
    	phx_write_u32(phy_wrap+0x00000000, 0x00100000);/*ldf 20230627 add:: hc3080b*/
//    }
    
    phx_write_u32(phy_wrap+0x50, 0x7c0); // tx deemphasis
    phx_write_u32(phy_wrap+0x68, 0x7c0);
    phx_write_u32(phy_wrap+0x80, 0x7c0);
    phx_write_u32(phy_wrap+0x98, 0x7c0);
    //phx_write_u32(phy_wrap+0x50, 0x6540); // tx deemphasis
    //phx_write_u32(phy_wrap+0x68, 0x6540);
    //phx_write_u32(phy_wrap+0x80, 0x6540);
    //phx_write_u32(phy_wrap+0x98, 0x6540);

//    if(hr3_board_type == 2)
//    {
    	rab_phy_rx_termination_set(base_addr, 0);/*ldf 20230627 add:: hc3080b*/
//    }
    
    // pma_rx_eq_training_data_valid_ln
    phx_write_u32(phy_wrap+0x58, 0x1);
    phx_write_u32(phy_wrap+0x70, 0x1);
    phx_write_u32(phy_wrap+0x88, 0x1);
    phx_write_u32(phy_wrap+0xa0, 0x1);

    // Config reg name: serdes_width_p0
    phx_write_u32(phy_wrap+0x00000018, 0x20101005);
    // Config reg name: serdes_width_p1
    phx_write_u32(phy_wrap+0x00000024, 0x20101005);
    // Config reg name: serdes_width_p2
    phx_write_u32(phy_wrap+0x00000030, 0x20101005);
    // Config reg name: serdes_width_p3
    phx_write_u32(phy_wrap+0x0000003c, 0x20101005);
    // Config reg name: pma_link_mode
    phx_write_u32(phy_wrap+0x00000004, 0x00000000);
    // Config reg name: pma_refclk_sel
    phx_write_u32(phy_wrap+0x00000008, 0x00003200);

    // Config reg name: PHY_PLL_CFG
    phx_write_u32(phy_innr+(0x0000C00E << 2), 0x00000000);
    // Config reg name: sdosccal_clk_cnt_preg
    phx_write_u32(phy_innr+(0x0000006E << 2), 0x00000180);
    // Config reg name: cmn_rescal_ctrla_preg
    phx_write_u32(phy_innr+(0x000000A0 << 2), 0x00000031);
    // Config reg name: cmn_refrcv1_preg
    phx_write_u32(phy_innr+(0x000000B8 << 2), 0x00006100);
    // Config reg name: cmn_plllc_gen_preg
    phx_write_u32(phy_innr+(0x00000042 << 2), 0x00000002);
    // Config reg name: cmn_plllc_fbdiv_int_mode0_preg
    phx_write_u32(phy_innr+(0x00000043 << 2), 0x0000001E);
    // Config reg name: cmn_plllc_lf_coeff_mode0_preg
    phx_write_u32(phy_innr+(0x0000004A << 2), 0x00002106);
    // Config reg name: cmn_plllc_clk0_preg
    phx_write_u32(phy_innr+(0x0000004E << 2), 0x00008102);
    // Config reg name: cmn_plllc_bwcal_mode0_preg
    phx_write_u32(phy_innr+(0x00000050 << 2), 0x00000000);
    // Config reg name: cmn_plllc_avdd_preg
    phx_write_u32(phy_innr+(0x0000005A << 2), 0x00000CDB);
    // Config reg name: cmn_plllcsm_pllen_tmr_preg
    phx_write_u32(phy_innr+(0x0000005D << 2), 0x00000027);
    // Config reg name: cmn_plllcsm_pllpre_tmr_preg
    phx_write_u32(phy_innr+(0x0000005E << 2), 0x00000062);
    // Config reg name: cmn_plllc_ss_time_stepsize_mode_preg
    phx_write_u32(phy_innr+(0x00000062 << 2), 0x00000800);
    // Config reg name: cmn_cdbpwriso_ovrd_preg
    phx_write_u32(phy_innr+(0x00000023 << 2), 0x0000413B);
    // Config reg name: det_standec_a_preg
    phx_write_u32(phy_innr+(0x00006000 << 2), 0x0000ED08);
    // Config reg name: det_standec_d_preg
    phx_write_u32(phy_innr+(0x00006003 << 2), 0x0000691E);
    // Config reg name: det_standec_e_preg
    phx_write_u32(phy_innr+(0x00006004 << 2), 0x00000291);
    // Config reg name: pllctrl_fbdiv_mode01_preg
    phx_write_u32(phy_innr+(0x00006039 << 2), 0x00000104);
    // Config reg name: pllctrl_subrate_preg
    phx_write_u32(phy_innr+(0x0000603A << 2), 0x00000011);
    // Config reg name: pllctrl_gen_d_preg
    phx_write_u32(phy_innr+(0x0000603E << 2), 0x00000106);
    // Config reg name: dfe_biastrim_preg
    phx_write_u32(phy_innr+(0x0000604C << 2), 0x00000D33);
    // Config reg name: drvctrl_atten_preg
    phx_write_u32(phy_innr+(0x0000606A << 2), 0x00000000);
    // Config reg name: drvctrl_boost_preg
    phx_write_u32(phy_innr+(0x0000606F << 2), 0x00001402);
    // Config reg name: rx_creq_fltr_a_mode0_preg
    phx_write_u32(phy_innr+(0x00006088 << 2), 0x00008C43);
    // Config reg name: creq_fsmclk_sel_preg
    phx_write_u32(phy_innr+(0x00006092 << 2), 0x00003222);

    // Config reg name: creq_eq_ctrl_preg
    phx_write_u32(phy_innr+(0x00006093 << 2), 0x00000102);

    // Config reg name: dfe_ecmp_ratesel_preg
    phx_write_u32(phy_innr+(0x000060C0 << 2), 0x00004111);
    // Config reg name: dfe_smp_ratesel_preg
    phx_write_u32(phy_innr+(0x000060C1 << 2), 0x00004111);
    // Config reg name: deq_errcmp_ctrl_preg
    phx_write_u32(phy_innr+(0x000060D0 << 2), 0x00003E3E);
    // Config reg name: deq_tau_ctrl2_preg
    phx_write_u32(phy_innr+(0x00006151 << 2), 0x00002008);
    // Config reg name: deq_tau_ctrl6_preg
    phx_write_u32(phy_innr+(0x00006155 << 2), 0x0000003F);
    // Config reg name: deq_tau_epioffset_mode_preg
    phx_write_u32(phy_innr+(0x00006158 << 2), 0x00006422);
    // Config reg name: deq_concur_epioffset_mode_preg
    phx_write_u32(phy_innr+(0x00006159 << 2), 0x00006622);
    // Config reg name: deq_pictrl_preg
    phx_write_u32(phy_innr+(0x00006161 << 2), 0x00000023);
    // Config reg name: cpical_tmrval_mode0_preg
    phx_write_u32(phy_innr+(0x00006171 << 2), 0x00000330);
    // Config reg name: cpi_outbuf_ratesel_preg
    phx_write_u32(phy_innr+(0x0000617C << 2), 0x000000D5);
    // Config reg name: datpeak_thresh_hi_ceph_0_preg
    phx_write_u32(phy_innr+(0x000061D8 << 2), 0x00000019);
    // Config reg name: datpeak_thresh_hi_ceph_1_preg
    phx_write_u32(phy_innr+(0x000061D9 << 2), 0x00007FF1);
    // Config reg name: datpeak_thresh_hi_ceph_2_preg
    phx_write_u32(phy_innr+(0x000061DA << 2), 0x00000028);
    // Config reg name: datpeak_thresh_hi_ceph_3_preg
    phx_write_u32(phy_innr+(0x000061DB << 2), 0x00000028);
    // Config reg name: datpeak_thresh_hi_ceph_4_preg
    phx_write_u32(phy_innr+(0x000061DC << 2), 0x00007FFF);
    // Config reg name: datpeak_thresh_lo_ceph_0_preg
    phx_write_u32(phy_innr+(0x000061DE << 2), 0x00007FC3);
    // Config reg name: datpeak_thresh_lo_ceph_1_preg
    phx_write_u32(phy_innr+(0x000061DF << 2), 0x00007F81);
    // Config reg name: datpeak_thresh_lo_ceph_2_preg
    phx_write_u32(phy_innr+(0x000061E0 << 2), 0x00007FC3);
    // Config reg name: datpeak_thresh_lo_ceph_3_preg
    phx_write_u32(phy_innr+(0x000061E1 << 2), 0x00007FC3);
    // Config reg name: datpeak_thresh_lo_ceph_4_preg
    phx_write_u32(phy_innr+(0x000061E2 << 2), 0x00007F81);
    // Config reg name: datpeak_scaling_tap_ceph_0_preg
    phx_write_u32(phy_innr+(0x000061E4 << 2), 0x00000000);
    // Config reg name: datpeak_scaling_tap_ceph_1_preg
    phx_write_u32(phy_innr+(0x000061E5 << 2), 0x00001000);
    // Config reg name: datpeak_scaling_tap_ceph_2_preg
    phx_write_u32(phy_innr+(0x000061E6 << 2), 0x00001092);
    // Config reg name: datpeak_scaling_tap_ceph_3_preg
    phx_write_u32(phy_innr+(0x000061E7 << 2), 0x00001092);
    // Config reg name: datpeak_scaling_tap_ceph_4_preg
    phx_write_u32(phy_innr+(0x000061E8 << 2), 0x00001000);

    // enable port tx rx
    rab_page_write(base_addr, 0x15c + 0x40 * 0, 0x00600000);
    rab_page_write(base_addr, 0x15c + 0x40 * 1, 0x00600000);
    rab_page_write(base_addr, 0x15c + 0x40 * 2, 0x00600000);
    rab_page_write(base_addr, 0x15c + 0x40 * 3, 0x00600000);
    rab_config(base_addr, 0x5204000c); // 6.25G x4 idle2
    //rab_config(base_addr, 0x5200000c); // 6.25G x1x1x1x1 idle2
    //rab_dlb_loopback(base_addr);

    // boot complete
    rab_page_write(base_addr, 0x404, 0x00000001);

#if PHY_BIST_CONFIG
    // tx rx prbs
    rab_phy_bist_config(base_addr);
#endif

#if PHY_SERIAL_LOOPBACK
    phx_write_u32(phy_innr+(0x00006050 << 2), 0xe04); // LANE_LOOPBACK_CTRL_PREG
    phx_write_u32(phy_innr+(0x00006048 << 2), 0x0); // TX_BIASTRIM_PREG
#endif

    // PHY Reset Free
    // Config reg name: phy_reset
//    if(hr3_board_type == 2)
//    {
    	phx_write_u32(phy_wrap+0x00000000, 0x00111111/*0x00011111*/);/*ldf 20230627 modify:: hc3080b*/
//    }
//    else
//    {
//    	phx_write_u32(phy_wrap+0x00000000, 0x00011111);
//    }

//	uart_printf("pma_cmn_ready[8:8]  phy_wrap 0xC = 0x%x\r\n", phx_read_u32(phy_wrap + 0xc));
//	uart_printf("tx deemphasis[0:17] phy_wrap 0x50 0x68 0x80 0x98 = 0x%x 0x%x 0x%x 0x%x\r\n",
//			phx_read_u32(phy_wrap + 0x50),
//			phx_read_u32(phy_wrap + 0x68),
//			phx_read_u32(phy_wrap + 0x80),
//			phx_read_u32(phy_wrap + 0x98));
//	rab_phy_tx_ffe_para_search_result(base_addr);
}

/* phy config_10G3125 */
void rab_phy_link_ini_config_10G3125(sriox base_addr)
{
    u32 rab_innr;
    u32 rab_wrap;
    u32 phy_wrap;
    u32 phy_innr;

    rab_innr = SRIO_RAB_INNER_ADDR(base_addr);
    rab_wrap = SRIO_RAB_WRAPPER_ADDR(base_addr);
    phy_wrap = SRIO_PHY_WRAPPER_ADDR(base_addr);
    phy_innr = SRIO_PHY_INNER_ADDR(base_addr);

    uart_printf("rab%d 10G3125 x4 mode config\r\n", base_addr);

    // reset rab and phy
//    if(hr3_board_type == 2)
//    {
		phx_write_u32(rab_wrap+0x00000024, 0x00000010);/*ldf 20230627 add:: hc3080b*/
		phx_write_u32(phy_wrap+0x00000000, 0x00000000);/*ldf 20230627 add:: hc3080b*/
//    }
    
    phx_write_u32(rab_wrap+0x00000010, 0x00014021); // x4 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00010021); // x1x1x1x1 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00011021); // x2x1x1 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00012021); // x1x1x2 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00013021); // x2x2 mode

    // srio_port_mode  10.3125G idle3 32bit
    phx_write_u32(rab_wrap+0x00000014, 0x00000833);
    phx_write_u32(rab_wrap+0x00000018, 0x00000833);
    phx_write_u32(rab_wrap+0x0000001C, 0x00000833);
    phx_write_u32(rab_wrap+0x00000020, 0x00000833);

    // Config reg name: cdr_lock_time
    phx_write_u32(rab_wrap+0x00000028, 30000);
    // Config reg name: rx_ready_time
    phx_write_u32(rab_wrap+0x0000002c, 32000);
    // Config reg name: srio_reset
//    if(hr3_board_type == 2)
//    {
    	phx_write_u32(rab_wrap+0x00000024, 0x00000101/*0x00000001*/);/*ldf 20230627 modify:: hc3080b*/
//    }
//    else
//    {
//    	phx_write_u32(rab_wrap+0x00000024, 0x00000001);
//    }
    udelay(100);

    
    // Config reg name: phy apb reset release
//    if(hr3_board_type == 2)
//    {
    	phx_write_u32(phy_wrap+0x00000000, 0x00100000);/*ldf 20230627 add:: hc3080b*/
//    }
    
    // Config reg name: serdes_width_p0
    phx_write_u32(phy_wrap+0x00000018, 0x20101002);
    // Config reg name: serdes_width_p1
    phx_write_u32(phy_wrap+0x00000024, 0x20101002);
    // Config reg name: serdes_width_p2
    phx_write_u32(phy_wrap+0x00000030, 0x20101002);
    // Config reg name: serdes_width_p3
    phx_write_u32(phy_wrap+0x0000003c, 0x20101002);
    // Config reg name: pma_link_mode
    phx_write_u32(phy_wrap+0x00000004, 0x00000000); // x4, 1 link
    //phx_write_u32(phy_wrap+0x00000004, 0x00003210); // x1x1x1x1, 4 link
    //phx_write_u32(phy_wrap+0x00000004, 0x00001100); // x2x2, 2 link
    // Config reg name: pma_refclk_sel
    phx_write_u32(phy_wrap+0x00000008, 0x00003200);

    // Config reg name: PHY_PLL_CFG
    phx_write_u32(phy_innr+(0x0000C00E << 2), 0x00000000);
    // Config reg name: sdosccal_clk_cnt_preg
    phx_write_u32(phy_innr+(0x0000006E << 2), 0x00000180);
    // Config reg name: cmn_rescal_ctrla_preg
    phx_write_u32(phy_innr+(0x000000A0 << 2), 0x00000031);
    // Config reg name: cmn_refrcv1_preg
    phx_write_u32(phy_innr+(0x000000B8 << 2), 0x00006100);
    // Config reg name: cmn_plllc_gen_preg
    phx_write_u32(phy_innr+(0x00000042 << 2), 0x00000002);
    // Config reg name: cmn_plllc_fbdiv_int_mode0_preg
    phx_write_u32(phy_innr+(0x00000043 << 2), 0x00000021);
    // Config reg name: cmn_plllc_lf_coeff_mode0_preg
    phx_write_u32(phy_innr+(0x0000004A << 2), 0x00002106);
    // Config reg name: cmn_plllc_locksearch_preg
    phx_write_u32(phy_innr+(0x0000004C << 2), 0x00000000);
    // Config reg name: cmn_plllc_clk0_preg
    phx_write_u32(phy_innr+(0x0000004E << 2), 0x00008102);
    // Config reg name: cmn_plllc_bwcal_mode0_preg
    phx_write_u32(phy_innr+(0x00000050 << 2), 0x00000000);
    // Config reg name: cmn_plllc_avdd_preg
    phx_write_u32(phy_innr+(0x0000005A << 2), 0x00000CDB);
    // Config reg name: cmn_plllcsm_pllen_tmr_preg
    phx_write_u32(phy_innr+(0x0000005D << 2), 0x00000027);
    // Config reg name: cmn_plllcsm_pllpre_tmr_preg
    phx_write_u32(phy_innr+(0x0000005E << 2), 0x00000062);
    // Config reg name: cmn_plllc_ss_time_stepsize_mode_preg
    phx_write_u32(phy_innr+(0x00000062 << 2), 0x00000800);
    // Config reg name: cmn_cdbpwriso_ovrd_preg
    phx_write_u32(phy_innr+(0x00000023 << 2), 0x0000413B);

#if 0
    // port 0 cfg
    // Config reg name: det_standec_a_preg
    phx_write_u32(phy_innr+(0x00006000 << 2), 0x0000ED09);
    // Config reg name: det_standec_b_preg
    phx_write_u32(phy_innr+(0x00006001 << 2), 0x0000000D);
    // Config reg name: det_standec_c_preg
    phx_write_u32(phy_innr+(0x00006002 << 2), 0x000000A2);
    // Config reg name: det_standec_d_preg
    phx_write_u32(phy_innr+(0x00006003 << 2), 0x0000691E);
    // Config reg name: det_standec_e_preg
    phx_write_u32(phy_innr+(0x00006004 << 2), 0x00000291);
    // Config reg name: pllctrl_fast_lock_opt2a_ref_cnt
    phx_write_u32(phy_innr+(0x00006006 << 2), 0x00008880);
    // Config reg name: psc_rx_a0_def_preg
    phx_write_u32(phy_innr+(0x00006030 << 2), 0x00000FFE);
    // Config reg name: pllctrl_fbdiv_mode01_preg
    phx_write_u32(phy_innr+(0x00006039 << 2), 0x00000102);
    // Config reg name: pllctrl_subrate_preg
    phx_write_u32(phy_innr+(0x0000603A << 2), 0x00000010);
    // Config reg name: pllctrl_gen_d_preg
    phx_write_u32(phy_innr+(0x0000603E << 2), 0x00000104);
    // Config reg name: dfe_biastrim_preg
    phx_write_u32(phy_innr+(0x0000604C << 2), 0x00000D33);
    // Config reg name: drvctrl_atten_preg
    phx_write_u32(phy_innr+(0x0000606A << 2), 0x00000000);
    // Config reg name: drvctrl_boost_preg
    phx_write_u32(phy_innr+(0x0000606F << 2), 0x00001402);
    // Config reg name: rx_creq_fltr_a_mode0_preg
    phx_write_u32(phy_innr+(0x00006088 << 2), 0x0000888B);
    // Config reg name: creq_cclkdet_mode01_preg
    phx_write_u32(phy_innr+(0x0000608E << 2), 0x00003C7F);
    // Config reg name: creq_fsmclk_sel_preg
    phx_write_u32(phy_innr+(0x00006092 << 2), 0x00003233);
    // Config reg name: creq_eq_ctrl_preg
    phx_write_u32(phy_innr+(0x00006093 << 2), 0x00000102);
    // Config reg name: deq_blk_tau_ctrl1_preg
    phx_write_u32(phy_innr+(0x000060AC << 2), 0x00006A6A);
    // Config reg name: deq_blk_tau_ctrl2_preg
    phx_write_u32(phy_innr+(0x000060AD << 2), 0x00000080);
    // Config reg name: deq_blk_tau_ctrl4_preg
    phx_write_u32(phy_innr+(0x000060AF << 2), 0x00001C08);
    // Config reg name: dfe_ecmp_ratesel_preg
    phx_write_u32(phy_innr+(0x000060C0 << 2), 0x00004112);
    // Config reg name: dfe_smp_ratesel_preg
    phx_write_u32(phy_innr+(0x000060C1 << 2), 0x00004112);
    // Config reg name: deq_concur_ctrl1_preg
    phx_write_u32(phy_innr+(0x000060C8 << 2), 0x00000028);
    // Config reg name: deq_errcmp_ctrl_preg
    phx_write_u32(phy_innr+(0x000060D0 << 2), 0x00003E3E);
    // Config reg name: deq_vga_ratesel_preg
    phx_write_u32(phy_innr+(0x000060E1 << 2), 0x00003103);
    // Config reg name: datgain_init_mode10_preg
    phx_write_u32(phy_innr+(0x000060E3 << 2), 0x00002B1F);
    // Config reg name: seq_blanking_ctrl_preg
    phx_write_u32(phy_innr+(0x000060EA << 2), 0x00000450);
    // Config reg name: eoffcal_en_phctrl_preg
    phx_write_u32(phy_innr+(0x000060F5 << 2), 0x00001561);
    // Config reg name: phalign_en_ctrl_preg
    phx_write_u32(phy_innr+(0x000060F8 << 2), 0x00001561);
    // Config reg name: rxtrain_cmplte_en_tau_delta_acqsel_ctrl_preg
    phx_write_u32(phy_innr+(0x000060FA << 2), 0x000000B0);
    // Config reg name: tau_en_ceph2to0_preg
    phx_write_u32(phy_innr+(0x000060FB << 2), 0x000000A6);
    // Config reg name: tau_en_ceph5to3_preg
    phx_write_u32(phy_innr+(0x000060FC << 2), 0x000001B6);
    // Config reg name: concur_time_ceph2to0_preg
    phx_write_u32(phy_innr+(0x000060FF << 2), 0x00000333);
    // Config reg name: concur_time_ceph5to3_preg
    phx_write_u32(phy_innr+(0x00006100 << 2), 0x00000738);
    // Config reg name: datdfe_tap0_thresh_ceph2to0_preg
    phx_write_u32(phy_innr+(0x00006101 << 2), 0x00000777);
    // Config reg name: datdfe_tap0_thresh_ceph5to3_preg
    phx_write_u32(phy_innr+(0x00006102 << 2), 0x00000BCC);
    // Config reg name: datdfe_tap1_thresh_ceph2to0_preg
    phx_write_u32(phy_innr+(0x00006103 << 2), 0x00000888);
    // Config reg name: datdfe_tap1_thresh_ceph5to3_preg
    phx_write_u32(phy_innr+(0x00006104 << 2), 0x00000CDD);
    // Config reg name: datdfe_tap2_thresh_ceph2to0_preg
    phx_write_u32(phy_innr+(0x00006105 << 2), 0x00000888);
    // Config reg name: datdfe_tap2_thresh_ceph5to3_preg
    phx_write_u32(phy_innr+(0x00006106 << 2), 0x00000CDD);
    // Config reg name: datdfe_tap3_thresh_ceph2to0_preg
    phx_write_u32(phy_innr+(0x00006107 << 2), 0x00000888);
    // Config reg name: datdfe_tap3_thresh_ceph5to3_preg
    phx_write_u32(phy_innr+(0x00006108 << 2), 0x00000CDD);
    // Config reg name: datdfe_tap4_thresh_ceph2to0_preg
    phx_write_u32(phy_innr+(0x00006109 << 2), 0x00000888);
    // Config reg name: datdfe_tap4_thresh_ceph5to3_preg
    phx_write_u32(phy_innr+(0x0000610A << 2), 0x00000CDD);
    // Config reg name: datgain_thresh_ceph0to2_preg
    phx_write_u32(phy_innr+(0x0000610B << 2), 0x00000599);
    // Config reg name: datgain_thresh_ceph3to5_preg
    phx_write_u32(phy_innr+(0x0000610C << 2), 0x00000DEE);
    // Config reg name: datoff_thresh_ceph0to2_preg
    phx_write_u32(phy_innr+(0x0000610E << 2), 0x00000599);
    // Config reg name: datoff_thresh_ceph3to5_preg
    phx_write_u32(phy_innr+(0x0000610F << 2), 0x00000DEE);
    // Config reg name: phase_iter_ceph_23_preg
    phx_write_u32(phy_innr+(0x00006111 << 2), 0x000028FF);
    // Config reg name: tau_ecmp_vth_ceph_45_preg
    phx_write_u32(phy_innr+(0x00006118 << 2), 0x00000000);
    // Config reg name: tau_error_thresh_ceph_0_preg
    phx_write_u32(phy_innr+(0x0000611A << 2), 0x00000100);
    // Config reg name: tau_error_thresh_ceph_1_preg
    phx_write_u32(phy_innr+(0x0000611B << 2), 0x0000000F);
    // Config reg name: tau_error_thresh_ceph_2_preg
    phx_write_u32(phy_innr+(0x0000611C << 2), 0x00000024);
    // Config reg name: tau_error_thresh_ceph_3_preg
    phx_write_u32(phy_innr+(0x0000611D << 2), 0x00000024);
    // Config reg name: tau_error_thresh_ceph_4_preg
    phx_write_u32(phy_innr+(0x0000611E << 2), 0x000001FF);
    // Config reg name: tau_max_error_hyst_sel_preg
    phx_write_u32(phy_innr+(0x00006121 << 2), 0x0000001F);
    // Config reg name: tau_time_ceph_012_preg
    phx_write_u32(phy_innr+(0x00006122 << 2), 0x00001CE7);
    // Config reg name: tau_time_ceph_345_preg
    phx_write_u32(phy_innr+(0x00006123 << 2), 0x00001D67);
    // Config reg name: datpeak_en_ceph_preg
    phx_write_u32(phy_innr+(0x00006125 << 2), 0x0000001F);
    // Config reg name: vga_lutmode_sel_ceph_preg
    phx_write_u32(phy_innr+(0x00006126 << 2), 0x00004000);
    // Config reg name: deq_tau_ctrl2_preg
    phx_write_u32(phy_innr+(0x00006151 << 2), 0x00002008);
    // Config reg name: deq_tau_ctrl6_preg
    phx_write_u32(phy_innr+(0x00006155 << 2), 0x0000003F);
    // Config reg name: deq_tau_epioffset_mode_preg
    phx_write_u32(phy_innr+(0x00006158 << 2), 0x00006422);
    // Config reg name: deq_concur_epioffset_mode_preg
    phx_write_u32(phy_innr+(0x00006159 << 2), 0x00006622);
    // Config reg name: deq_pictrl_preg
    phx_write_u32(phy_innr+(0x00006161 << 2), 0x00000023);
    // Config reg name: cpical_tmrval_mode0_preg
    phx_write_u32(phy_innr+(0x00006171 << 2), 0x00000400);
    // Config reg name: cpical_pictrl_mode0_preg
    phx_write_u32(phy_innr+(0x00006175 << 2), 0x000001FF);
    // Config reg name: cpi_outbuf_ratesel_preg
    phx_write_u32(phy_innr+(0x0000617C << 2), 0x000000D6);
    // Config reg name: datpeak_thresh_hi_ceph_0_preg
    phx_write_u32(phy_innr+(0x000061D8 << 2), 0x00000019);
    // Config reg name: datpeak_thresh_hi_ceph_1_preg
    phx_write_u32(phy_innr+(0x000061D9 << 2), 0x00007FF1);
    // Config reg name: datpeak_thresh_hi_ceph_2_preg
    phx_write_u32(phy_innr+(0x000061DA << 2), 0x00000028);
    // Config reg name: datpeak_thresh_hi_ceph_3_preg
    phx_write_u32(phy_innr+(0x000061DB << 2), 0x00000028);
    // Config reg name: datpeak_thresh_hi_ceph_4_preg
    phx_write_u32(phy_innr+(0x000061DC << 2), 0x00007FFF);
    // Config reg name: datpeak_thresh_lo_ceph_0_preg
    phx_write_u32(phy_innr+(0x000061DE << 2), 0x00007FC3);
    // Config reg name: datpeak_thresh_lo_ceph_1_preg
    phx_write_u32(phy_innr+(0x000061DF << 2), 0x00007F81);
    // Config reg name: datpeak_thresh_lo_ceph_2_preg
    phx_write_u32(phy_innr+(0x000061E0 << 2), 0x00007FC3);
    // Config reg name: datpeak_thresh_lo_ceph_3_preg
    phx_write_u32(phy_innr+(0x000061E1 << 2), 0x00007FC3);
    // Config reg name: datpeak_thresh_lo_ceph_4_preg
    phx_write_u32(phy_innr+(0x000061E2 << 2), 0x00007F81);
    // Config reg name: datpeak_scaling_tap_ceph_0_preg
    phx_write_u32(phy_innr+(0x000061E4 << 2), 0x00000000);
    // Config reg name: datpeak_scaling_tap_ceph_1_preg
    phx_write_u32(phy_innr+(0x000061E5 << 2), 0x00001000);
    // Config reg name: datpeak_scaling_tap_ceph_2_preg
    phx_write_u32(phy_innr+(0x000061E6 << 2), 0x00001092);
    // Config reg name: datpeak_scaling_tap_ceph_3_preg
    phx_write_u32(phy_innr+(0x000061E7 << 2), 0x00001092);
    // Config reg name: datpeak_scaling_tap_ceph_4_preg
    phx_write_u32(phy_innr+(0x000061E8 << 2), 0x00001000);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000000);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000E01);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000001);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000C02);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000002);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000A03);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000003);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000804);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000004);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000805);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000005);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000806);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000006);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000807);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000007);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000808);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000008);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000809);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000009);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x0000080A);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000000A);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x0000080B);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000000B);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x0000080C);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000000C);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x0000080D);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000000D);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x0000090D);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000000E);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x0000090E);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000000F);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000A0F);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000010);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000A10);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000011);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000B10);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000012);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000B11);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000013);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000C12);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000014);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000C13);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000015);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000D13);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000016);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000D14);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000017);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000E15);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000018);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000E16);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000019);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000F16);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000001A);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000F17);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000001B);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001018);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000001C);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001019);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000001D);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001119);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000001E);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x0000111A);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000001F);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x0000121B);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000020);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x0000121C);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000021);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x0000131C);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000022);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x0000131D);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000023);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x0000141E);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000024);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x0000141F);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000025);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x0000151F);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000026);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001520);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000027);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001621);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000028);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001721);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000029);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001822);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000002A);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001922);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000002B);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001A23);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000002C);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B23);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000002D);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B24);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000002E);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B25);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000002F);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B26);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000030);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B27);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000031);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B28);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000032);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B29);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000033);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B29);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000034);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B2A);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000035);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B2C);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000036);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B2D);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000037);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B2E);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000038);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B2F);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000039);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B30);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000003A);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B31);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000003B);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B32);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000003C);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B33);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000003D);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B34);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000003E);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B35);
    // Config reg name: deq_vgalut_addr
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000003F);
    // Config reg name: deq_vgalut_data
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B36);
#else
    // port 0 cfg
    phx_write_u32(phy_innr+(0x00006000 << 2), 0x0000ED09);
    phx_write_u32(phy_innr+(0x00006001 << 2), 0x0000000D);
    phx_write_u32(phy_innr+(0x00006002 << 2), 0x000000A2);
    phx_write_u32(phy_innr+(0x00006003 << 2), 0x0000691E);
    phx_write_u32(phy_innr+(0x00006004 << 2), 0x00000291);
    phx_write_u32(phy_innr+(0x00006006 << 2), 0x00008880);
    phx_write_u32(phy_innr+(0x00006030 << 2), 0x00000FFE);
    phx_write_u32(phy_innr+(0x00006039 << 2), 0x00000102);
    phx_write_u32(phy_innr+(0x0000603A << 2), 0x00000010);
    phx_write_u32(phy_innr+(0x0000603E << 2), 0x00000104);
    phx_write_u32(phy_innr+(0x0000604C << 2), 0x00000D33);
    phx_write_u32(phy_innr+(0x0000606A << 2), 0x00000000);
    phx_write_u32(phy_innr+(0x0000606F << 2), 0x00001402);
    phx_write_u32(phy_innr+(0x00006088 << 2), 0x0000888B);
    phx_write_u32(phy_innr+(0x0000608E << 2), 0x00003C7F);
    phx_write_u32(phy_innr+(0x00006092 << 2), 0x00003233);
    phx_write_u32(phy_innr+(0x00006093 << 2), 0x00000102);
    phx_write_u32(phy_innr+(0x000060AC << 2), 0x00006A6A);
    phx_write_u32(phy_innr+(0x000060AD << 2), 0x00000080);
    phx_write_u32(phy_innr+(0x000060AF << 2), 0x00001C08);
    phx_write_u32(phy_innr+(0x000060C0 << 2), 0x00004112);
    phx_write_u32(phy_innr+(0x000060C1 << 2), 0x00004112);
    phx_write_u32(phy_innr+(0x000060C8 << 2), 0x00000028);
    phx_write_u32(phy_innr+(0x000060D0 << 2), 0x00003E3E);
    phx_write_u32(phy_innr+(0x000060E1 << 2), 0x00003103);
    phx_write_u32(phy_innr+(0x000060E3 << 2), 0x00002B1F);
    phx_write_u32(phy_innr+(0x000060EA << 2), 0x00000450);
    phx_write_u32(phy_innr+(0x000060F5 << 2), 0x00001561);
    phx_write_u32(phy_innr+(0x000060F8 << 2), 0x00001561);
    phx_write_u32(phy_innr+(0x000060FA << 2), 0x000000B0);
    phx_write_u32(phy_innr+(0x000060FB << 2), 0x000000A6);
    phx_write_u32(phy_innr+(0x000060FC << 2), 0x000001B6);
    phx_write_u32(phy_innr+(0x000060FF << 2), 0x00000333);
    phx_write_u32(phy_innr+(0x00006100 << 2), 0x00000738);
    phx_write_u32(phy_innr+(0x00006101 << 2), 0x00000777);
    phx_write_u32(phy_innr+(0x00006102 << 2), 0x00000BCC);
    phx_write_u32(phy_innr+(0x00006103 << 2), 0x00000888);
    phx_write_u32(phy_innr+(0x00006104 << 2), 0x00000CDD);
    phx_write_u32(phy_innr+(0x00006105 << 2), 0x00000888);
    phx_write_u32(phy_innr+(0x00006106 << 2), 0x00000CDD);
    phx_write_u32(phy_innr+(0x00006107 << 2), 0x00000888);
    phx_write_u32(phy_innr+(0x00006108 << 2), 0x00000CDD);
    phx_write_u32(phy_innr+(0x00006109 << 2), 0x00000888);
    phx_write_u32(phy_innr+(0x0000610A << 2), 0x00000CDD);
    phx_write_u32(phy_innr+(0x0000610B << 2), 0x00000599);
    phx_write_u32(phy_innr+(0x0000610C << 2), 0x00000DEE);
    phx_write_u32(phy_innr+(0x0000610E << 2), 0x00000599);
    phx_write_u32(phy_innr+(0x0000610F << 2), 0x00000DEE);
    phx_write_u32(phy_innr+(0x00006111 << 2), 0x000028FF);
    phx_write_u32(phy_innr+(0x00006118 << 2), 0x00000000);
    phx_write_u32(phy_innr+(0x0000611A << 2), 0x00000100);
    phx_write_u32(phy_innr+(0x0000611B << 2), 0x0000000F);
    phx_write_u32(phy_innr+(0x0000611C << 2), 0x00000024);
    phx_write_u32(phy_innr+(0x0000611D << 2), 0x00000024);
    phx_write_u32(phy_innr+(0x0000611E << 2), 0x000001FF);
    phx_write_u32(phy_innr+(0x00006121 << 2), 0x0000001F);
    phx_write_u32(phy_innr+(0x00006122 << 2), 0x00001CE7);
    phx_write_u32(phy_innr+(0x00006123 << 2), 0x00001D67);
    phx_write_u32(phy_innr+(0x00006125 << 2), 0x0000001F);
    phx_write_u32(phy_innr+(0x00006126 << 2), 0x00004000);
    phx_write_u32(phy_innr+(0x00006151 << 2), 0x00002008);
    phx_write_u32(phy_innr+(0x00006155 << 2), 0x0000003F);
    phx_write_u32(phy_innr+(0x00006158 << 2), 0x00006422);
    phx_write_u32(phy_innr+(0x00006159 << 2), 0x00006622);
    phx_write_u32(phy_innr+(0x00006161 << 2), 0x00000023);
    phx_write_u32(phy_innr+(0x00006171 << 2), 0x00000400);
    phx_write_u32(phy_innr+(0x00006175 << 2), 0x000001FF);
    phx_write_u32(phy_innr+(0x0000617C << 2), 0x000000D6);
    phx_write_u32(phy_innr+(0x000061D8 << 2), 0x00000019);
    phx_write_u32(phy_innr+(0x000061D9 << 2), 0x00007FF1);
    phx_write_u32(phy_innr+(0x000061DA << 2), 0x00000028);
    phx_write_u32(phy_innr+(0x000061DB << 2), 0x00000028);
    phx_write_u32(phy_innr+(0x000061DC << 2), 0x00007FFF);
    phx_write_u32(phy_innr+(0x000061DE << 2), 0x00007FC3);
    phx_write_u32(phy_innr+(0x000061DF << 2), 0x00007F81);
    phx_write_u32(phy_innr+(0x000061E0 << 2), 0x00007FC3);
    phx_write_u32(phy_innr+(0x000061E1 << 2), 0x00007FC3);
    phx_write_u32(phy_innr+(0x000061E2 << 2), 0x00007F81);
    phx_write_u32(phy_innr+(0x000061E4 << 2), 0x00000000);
    phx_write_u32(phy_innr+(0x000061E5 << 2), 0x00001000);
    phx_write_u32(phy_innr+(0x000061E6 << 2), 0x00001092);
    phx_write_u32(phy_innr+(0x000061E7 << 2), 0x00001092);
    phx_write_u32(phy_innr+(0x000061E8 << 2), 0x00001000);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000000);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000E01);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000001);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000C02);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000002);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000A03);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000003);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000804);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000004);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000805);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000005);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000806);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000006);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000807);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000007);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000808);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000008);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000809);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000009);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x0000080A);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000000A);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x0000080B);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000000B);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x0000080C);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000000C);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x0000080D);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000000D);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x0000090D);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000000E);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x0000090E);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000000F);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000A0F);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000010);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000A10);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000011);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000B10);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000012);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000B11);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000013);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000C12);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000014);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000C13);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000015);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000D13);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000016);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000D14);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000017);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000E15);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000018);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000E16);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000019);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000F16);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000001A);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00000F17);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000001B);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001018);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000001C);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001019);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000001D);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001119);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000001E);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x0000111A);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000001F);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x0000121B);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000020);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x0000121C);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000021);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x0000131C);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000022);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x0000131D);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000023);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x0000141E);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000024);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x0000141F);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000025);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x0000151F);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000026);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001520);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000027);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001621);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000028);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001721);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000029);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001822);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000002A);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001922);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000002B);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001A23);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000002C);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B23);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000002D);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B24);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000002E);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B25);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000002F);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B26);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000030);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B27);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000031);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B28);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000032);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B29);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000033);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B29);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000034);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B2A);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000035);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B2C);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000036);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B2D);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000037);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B2E);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000038);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B2F);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x00000039);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B30);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000003A);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B31);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000003B);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B32);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000003C);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B33);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000003D);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B34);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000003E);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B35);
    phx_write_u32(phy_innr+(0x000060E8 << 2), 0x0000003F);
    phx_write_u32(phy_innr+(0x000060E9 << 2), 0x00001B36);

    // port 1 cfg
    phx_write_u32(phy_innr+(0x00006200 << 2), 0x0000ED09);
    phx_write_u32(phy_innr+(0x00006201 << 2), 0x0000000D);
    phx_write_u32(phy_innr+(0x00006202 << 2), 0x000000A2);
    phx_write_u32(phy_innr+(0x00006203 << 2), 0x0000691E);
    phx_write_u32(phy_innr+(0x00006204 << 2), 0x00000291);
    phx_write_u32(phy_innr+(0x00006206 << 2), 0x00008880);
    phx_write_u32(phy_innr+(0x00006230 << 2), 0x00000FFE);
    phx_write_u32(phy_innr+(0x00006239 << 2), 0x00000102);
    phx_write_u32(phy_innr+(0x0000623A << 2), 0x00000010);
    phx_write_u32(phy_innr+(0x0000623E << 2), 0x00000104);
    phx_write_u32(phy_innr+(0x0000624C << 2), 0x00000D33);
    phx_write_u32(phy_innr+(0x0000626A << 2), 0x00000000);
    phx_write_u32(phy_innr+(0x0000626F << 2), 0x00001402);
    phx_write_u32(phy_innr+(0x00006288 << 2), 0x0000888B);
    phx_write_u32(phy_innr+(0x0000628E << 2), 0x00003C7F);
    phx_write_u32(phy_innr+(0x00006292 << 2), 0x00003233);
    phx_write_u32(phy_innr+(0x00006293 << 2), 0x00000102);
    phx_write_u32(phy_innr+(0x000062AC << 2), 0x00006A6A);
    phx_write_u32(phy_innr+(0x000062AD << 2), 0x00000080);
    phx_write_u32(phy_innr+(0x000062AF << 2), 0x00001C08);
    phx_write_u32(phy_innr+(0x000062C0 << 2), 0x00004112);
    phx_write_u32(phy_innr+(0x000062C1 << 2), 0x00004112);
    phx_write_u32(phy_innr+(0x000062C8 << 2), 0x00000028);
    phx_write_u32(phy_innr+(0x000062D0 << 2), 0x00003E3E);
    phx_write_u32(phy_innr+(0x000062E1 << 2), 0x00003103);
    phx_write_u32(phy_innr+(0x000062E3 << 2), 0x00002B1F);
    phx_write_u32(phy_innr+(0x000062EA << 2), 0x00000450);
    phx_write_u32(phy_innr+(0x000062F5 << 2), 0x00001561);
    phx_write_u32(phy_innr+(0x000062F8 << 2), 0x00001561);
    phx_write_u32(phy_innr+(0x000062FA << 2), 0x000000B0);
    phx_write_u32(phy_innr+(0x000062FB << 2), 0x000000A6);
    phx_write_u32(phy_innr+(0x000062FC << 2), 0x000001B6);
    phx_write_u32(phy_innr+(0x000062FF << 2), 0x00000333);
    phx_write_u32(phy_innr+(0x00006300 << 2), 0x00000738);
    phx_write_u32(phy_innr+(0x00006301 << 2), 0x00000777);
    phx_write_u32(phy_innr+(0x00006302 << 2), 0x00000BCC);
    phx_write_u32(phy_innr+(0x00006303 << 2), 0x00000888);
    phx_write_u32(phy_innr+(0x00006304 << 2), 0x00000CDD);
    phx_write_u32(phy_innr+(0x00006305 << 2), 0x00000888);
    phx_write_u32(phy_innr+(0x00006306 << 2), 0x00000CDD);
    phx_write_u32(phy_innr+(0x00006307 << 2), 0x00000888);
    phx_write_u32(phy_innr+(0x00006308 << 2), 0x00000CDD);
    phx_write_u32(phy_innr+(0x00006309 << 2), 0x00000888);
    phx_write_u32(phy_innr+(0x0000630A << 2), 0x00000CDD);
    phx_write_u32(phy_innr+(0x0000630B << 2), 0x00000599);
    phx_write_u32(phy_innr+(0x0000630C << 2), 0x00000DEE);
    phx_write_u32(phy_innr+(0x0000630E << 2), 0x00000599);
    phx_write_u32(phy_innr+(0x0000630F << 2), 0x00000DEE);
    phx_write_u32(phy_innr+(0x00006311 << 2), 0x000028FF);
    phx_write_u32(phy_innr+(0x00006318 << 2), 0x00000000);
    phx_write_u32(phy_innr+(0x0000631A << 2), 0x00000100);
    phx_write_u32(phy_innr+(0x0000631B << 2), 0x0000000F);
    phx_write_u32(phy_innr+(0x0000631C << 2), 0x00000024);
    phx_write_u32(phy_innr+(0x0000631D << 2), 0x00000024);
    phx_write_u32(phy_innr+(0x0000631E << 2), 0x000001FF);
    phx_write_u32(phy_innr+(0x00006321 << 2), 0x0000001F);
    phx_write_u32(phy_innr+(0x00006322 << 2), 0x00001CE7);
    phx_write_u32(phy_innr+(0x00006323 << 2), 0x00001D67);
    phx_write_u32(phy_innr+(0x00006325 << 2), 0x0000001F);
    phx_write_u32(phy_innr+(0x00006326 << 2), 0x00004000);
    phx_write_u32(phy_innr+(0x00006351 << 2), 0x00002008);
    phx_write_u32(phy_innr+(0x00006355 << 2), 0x0000003F);
    phx_write_u32(phy_innr+(0x00006358 << 2), 0x00006422);
    phx_write_u32(phy_innr+(0x00006359 << 2), 0x00006622);
    phx_write_u32(phy_innr+(0x00006361 << 2), 0x00000023);
    phx_write_u32(phy_innr+(0x00006371 << 2), 0x00000400);
    phx_write_u32(phy_innr+(0x00006375 << 2), 0x000001FF);
    phx_write_u32(phy_innr+(0x0000637C << 2), 0x000000D6);
    phx_write_u32(phy_innr+(0x000063D8 << 2), 0x00000019);
    phx_write_u32(phy_innr+(0x000063D9 << 2), 0x00007FF1);
    phx_write_u32(phy_innr+(0x000063DA << 2), 0x00000028);
    phx_write_u32(phy_innr+(0x000063DB << 2), 0x00000028);
    phx_write_u32(phy_innr+(0x000063DC << 2), 0x00007FFF);
    phx_write_u32(phy_innr+(0x000063DE << 2), 0x00007FC3);
    phx_write_u32(phy_innr+(0x000063DF << 2), 0x00007F81);
    phx_write_u32(phy_innr+(0x000063E0 << 2), 0x00007FC3);
    phx_write_u32(phy_innr+(0x000063E1 << 2), 0x00007FC3);
    phx_write_u32(phy_innr+(0x000063E2 << 2), 0x00007F81);
    phx_write_u32(phy_innr+(0x000063E4 << 2), 0x00000000);
    phx_write_u32(phy_innr+(0x000063E5 << 2), 0x00001000);
    phx_write_u32(phy_innr+(0x000063E6 << 2), 0x00001092);
    phx_write_u32(phy_innr+(0x000063E7 << 2), 0x00001092);
    phx_write_u32(phy_innr+(0x000063E8 << 2), 0x00001000);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000000);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00000E01);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000001);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00000C02);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000002);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00000A03);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000003);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00000804);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000004);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00000805);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000005);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00000806);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000006);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00000807);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000007);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00000808);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000008);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00000809);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000009);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x0000080A);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x0000000A);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x0000080B);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x0000000B);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x0000080C);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x0000000C);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x0000080D);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x0000000D);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x0000090D);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x0000000E);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x0000090E);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x0000000F);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00000A0F);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000010);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00000A10);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000011);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00000B10);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000012);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00000B11);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000013);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00000C12);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000014);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00000C13);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000015);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00000D13);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000016);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00000D14);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000017);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00000E15);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000018);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00000E16);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000019);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00000F16);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x0000001A);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00000F17);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x0000001B);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00001018);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x0000001C);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00001019);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x0000001D);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00001119);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x0000001E);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x0000111A);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x0000001F);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x0000121B);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000020);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x0000121C);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000021);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x0000131C);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000022);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x0000131D);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000023);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x0000141E);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000024);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x0000141F);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000025);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x0000151F);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000026);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00001520);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000027);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00001621);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000028);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00001721);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000029);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00001822);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x0000002A);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00001922);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x0000002B);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00001A23);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x0000002C);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00001B23);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x0000002D);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00001B24);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x0000002E);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00001B25);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x0000002F);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00001B26);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000030);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00001B27);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000031);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00001B28);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000032);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00001B29);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000033);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00001B29);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000034);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00001B2A);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000035);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00001B2C);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000036);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00001B2D);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000037);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00001B2E);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000038);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00001B2F);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x00000039);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00001B30);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x0000003A);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00001B31);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x0000003B);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00001B32);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x0000003C);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00001B33);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x0000003D);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00001B34);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x0000003E);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00001B35);
    phx_write_u32(phy_innr+(0x000062E8 << 2), 0x0000003F);
    phx_write_u32(phy_innr+(0x000062E9 << 2), 0x00001B36);

    // port 2 cfg
    phx_write_u32(phy_innr+(0x00006400 << 2), 0x0000ED09);
    phx_write_u32(phy_innr+(0x00006401 << 2), 0x0000000D);
    phx_write_u32(phy_innr+(0x00006402 << 2), 0x000000A2);
    phx_write_u32(phy_innr+(0x00006403 << 2), 0x0000691E);
    phx_write_u32(phy_innr+(0x00006404 << 2), 0x00000291);
    phx_write_u32(phy_innr+(0x00006406 << 2), 0x00008880);
    phx_write_u32(phy_innr+(0x00006430 << 2), 0x00000FFE);
    phx_write_u32(phy_innr+(0x00006439 << 2), 0x00000102);
    phx_write_u32(phy_innr+(0x0000643A << 2), 0x00000010);
    phx_write_u32(phy_innr+(0x0000643E << 2), 0x00000104);
    phx_write_u32(phy_innr+(0x0000644C << 2), 0x00000D33);
    phx_write_u32(phy_innr+(0x0000646A << 2), 0x00000000);
    phx_write_u32(phy_innr+(0x0000646F << 2), 0x00001402);
    phx_write_u32(phy_innr+(0x00006488 << 2), 0x0000888B);
    phx_write_u32(phy_innr+(0x0000648E << 2), 0x00003C7F);
    phx_write_u32(phy_innr+(0x00006492 << 2), 0x00003233);
    phx_write_u32(phy_innr+(0x00006493 << 2), 0x00000102);
    phx_write_u32(phy_innr+(0x000064AC << 2), 0x00006A6A);
    phx_write_u32(phy_innr+(0x000064AD << 2), 0x00000080);
    phx_write_u32(phy_innr+(0x000064AF << 2), 0x00001C08);
    phx_write_u32(phy_innr+(0x000064C0 << 2), 0x00004112);
    phx_write_u32(phy_innr+(0x000064C1 << 2), 0x00004112);
    phx_write_u32(phy_innr+(0x000064C8 << 2), 0x00000028);
    phx_write_u32(phy_innr+(0x000064D0 << 2), 0x00003E3E);
    phx_write_u32(phy_innr+(0x000064E1 << 2), 0x00003103);
    phx_write_u32(phy_innr+(0x000064E3 << 2), 0x00002B1F);
    phx_write_u32(phy_innr+(0x000064EA << 2), 0x00000450);
    phx_write_u32(phy_innr+(0x000064F5 << 2), 0x00001561);
    phx_write_u32(phy_innr+(0x000064F8 << 2), 0x00001561);
    phx_write_u32(phy_innr+(0x000064FA << 2), 0x000000B0);
    phx_write_u32(phy_innr+(0x000064FB << 2), 0x000000A6);
    phx_write_u32(phy_innr+(0x000064FC << 2), 0x000001B6);
    phx_write_u32(phy_innr+(0x000064FF << 2), 0x00000333);
    phx_write_u32(phy_innr+(0x00006500 << 2), 0x00000738);
    phx_write_u32(phy_innr+(0x00006501 << 2), 0x00000777);
    phx_write_u32(phy_innr+(0x00006502 << 2), 0x00000BCC);
    phx_write_u32(phy_innr+(0x00006503 << 2), 0x00000888);
    phx_write_u32(phy_innr+(0x00006504 << 2), 0x00000CDD);
    phx_write_u32(phy_innr+(0x00006505 << 2), 0x00000888);
    phx_write_u32(phy_innr+(0x00006506 << 2), 0x00000CDD);
    phx_write_u32(phy_innr+(0x00006507 << 2), 0x00000888);
    phx_write_u32(phy_innr+(0x00006508 << 2), 0x00000CDD);
    phx_write_u32(phy_innr+(0x00006509 << 2), 0x00000888);
    phx_write_u32(phy_innr+(0x0000650A << 2), 0x00000CDD);
    phx_write_u32(phy_innr+(0x0000650B << 2), 0x00000599);
    phx_write_u32(phy_innr+(0x0000650C << 2), 0x00000DEE);
    phx_write_u32(phy_innr+(0x0000650E << 2), 0x00000599);
    phx_write_u32(phy_innr+(0x0000650F << 2), 0x00000DEE);
    phx_write_u32(phy_innr+(0x00006511 << 2), 0x000028FF);
    phx_write_u32(phy_innr+(0x00006518 << 2), 0x00000000);
    phx_write_u32(phy_innr+(0x0000651A << 2), 0x00000100);
    phx_write_u32(phy_innr+(0x0000651B << 2), 0x0000000F);
    phx_write_u32(phy_innr+(0x0000651C << 2), 0x00000024);
    phx_write_u32(phy_innr+(0x0000651D << 2), 0x00000024);
    phx_write_u32(phy_innr+(0x0000651E << 2), 0x000001FF);
    phx_write_u32(phy_innr+(0x00006521 << 2), 0x0000001F);
    phx_write_u32(phy_innr+(0x00006522 << 2), 0x00001CE7);
    phx_write_u32(phy_innr+(0x00006523 << 2), 0x00001D67);
    phx_write_u32(phy_innr+(0x00006525 << 2), 0x0000001F);
    phx_write_u32(phy_innr+(0x00006526 << 2), 0x00004000);
    phx_write_u32(phy_innr+(0x00006551 << 2), 0x00002008);
    phx_write_u32(phy_innr+(0x00006555 << 2), 0x0000003F);
    phx_write_u32(phy_innr+(0x00006558 << 2), 0x00006422);
    phx_write_u32(phy_innr+(0x00006559 << 2), 0x00006622);
    phx_write_u32(phy_innr+(0x00006561 << 2), 0x00000023);
    phx_write_u32(phy_innr+(0x00006571 << 2), 0x00000400);
    phx_write_u32(phy_innr+(0x00006575 << 2), 0x000001FF);
    phx_write_u32(phy_innr+(0x0000657C << 2), 0x000000D6);
    phx_write_u32(phy_innr+(0x000065D8 << 2), 0x00000019);
    phx_write_u32(phy_innr+(0x000065D9 << 2), 0x00007FF1);
    phx_write_u32(phy_innr+(0x000065DA << 2), 0x00000028);
    phx_write_u32(phy_innr+(0x000065DB << 2), 0x00000028);
    phx_write_u32(phy_innr+(0x000065DC << 2), 0x00007FFF);
    phx_write_u32(phy_innr+(0x000065DE << 2), 0x00007FC3);
    phx_write_u32(phy_innr+(0x000065DF << 2), 0x00007F81);
    phx_write_u32(phy_innr+(0x000065E0 << 2), 0x00007FC3);
    phx_write_u32(phy_innr+(0x000065E1 << 2), 0x00007FC3);
    phx_write_u32(phy_innr+(0x000065E2 << 2), 0x00007F81);
    phx_write_u32(phy_innr+(0x000065E4 << 2), 0x00000000);
    phx_write_u32(phy_innr+(0x000065E5 << 2), 0x00001000);
    phx_write_u32(phy_innr+(0x000065E6 << 2), 0x00001092);
    phx_write_u32(phy_innr+(0x000065E7 << 2), 0x00001092);
    phx_write_u32(phy_innr+(0x000065E8 << 2), 0x00001000);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000000);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00000E01);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000001);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00000C02);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000002);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00000A03);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000003);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00000804);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000004);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00000805);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000005);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00000806);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000006);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00000807);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000007);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00000808);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000008);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00000809);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000009);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x0000080A);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x0000000A);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x0000080B);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x0000000B);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x0000080C);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x0000000C);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x0000080D);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x0000000D);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x0000090D);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x0000000E);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x0000090E);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x0000000F);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00000A0F);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000010);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00000A10);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000011);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00000B10);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000012);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00000B11);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000013);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00000C12);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000014);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00000C13);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000015);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00000D13);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000016);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00000D14);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000017);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00000E15);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000018);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00000E16);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000019);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00000F16);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x0000001A);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00000F17);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x0000001B);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00001018);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x0000001C);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00001019);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x0000001D);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00001119);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x0000001E);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x0000111A);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x0000001F);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x0000121B);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000020);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x0000121C);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000021);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x0000131C);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000022);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x0000131D);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000023);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x0000141E);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000024);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x0000141F);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000025);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x0000151F);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000026);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00001520);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000027);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00001621);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000028);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00001721);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000029);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00001822);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x0000002A);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00001922);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x0000002B);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00001A23);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x0000002C);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00001B23);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x0000002D);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00001B24);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x0000002E);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00001B25);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x0000002F);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00001B26);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000030);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00001B27);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000031);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00001B28);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000032);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00001B29);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000033);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00001B29);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000034);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00001B2A);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000035);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00001B2C);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000036);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00001B2D);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000037);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00001B2E);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000038);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00001B2F);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x00000039);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00001B30);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x0000003A);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00001B31);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x0000003B);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00001B32);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x0000003C);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00001B33);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x0000003D);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00001B34);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x0000003E);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00001B35);
    phx_write_u32(phy_innr+(0x000064E8 << 2), 0x0000003F);
    phx_write_u32(phy_innr+(0x000064E9 << 2), 0x00001B36);
    
    // port 3 cfg
    phx_write_u32(phy_innr+(0x00006600 << 2), 0x0000ED09);
    phx_write_u32(phy_innr+(0x00006601 << 2), 0x0000000D);
    phx_write_u32(phy_innr+(0x00006602 << 2), 0x000000A2);
    phx_write_u32(phy_innr+(0x00006603 << 2), 0x0000691E);
    phx_write_u32(phy_innr+(0x00006604 << 2), 0x00000291);
    phx_write_u32(phy_innr+(0x00006606 << 2), 0x00008880);
    phx_write_u32(phy_innr+(0x00006630 << 2), 0x00000FFE);
    phx_write_u32(phy_innr+(0x00006639 << 2), 0x00000102);
    phx_write_u32(phy_innr+(0x0000663A << 2), 0x00000010);
    phx_write_u32(phy_innr+(0x0000663E << 2), 0x00000104);
    phx_write_u32(phy_innr+(0x0000664C << 2), 0x00000D33);
    phx_write_u32(phy_innr+(0x0000666A << 2), 0x00000000);
    phx_write_u32(phy_innr+(0x0000666F << 2), 0x00001402);
    phx_write_u32(phy_innr+(0x00006688 << 2), 0x0000888B);
    phx_write_u32(phy_innr+(0x0000668E << 2), 0x00003C7F);
    phx_write_u32(phy_innr+(0x00006692 << 2), 0x00003233);
    phx_write_u32(phy_innr+(0x00006693 << 2), 0x00000102);
    phx_write_u32(phy_innr+(0x000066AC << 2), 0x00006A6A);
    phx_write_u32(phy_innr+(0x000066AD << 2), 0x00000080);
    phx_write_u32(phy_innr+(0x000066AF << 2), 0x00001C08);
    phx_write_u32(phy_innr+(0x000066C0 << 2), 0x00004112);
    phx_write_u32(phy_innr+(0x000066C1 << 2), 0x00004112);
    phx_write_u32(phy_innr+(0x000066C8 << 2), 0x00000028);
    phx_write_u32(phy_innr+(0x000066D0 << 2), 0x00003E3E);
    phx_write_u32(phy_innr+(0x000066E1 << 2), 0x00003103);
    phx_write_u32(phy_innr+(0x000066E3 << 2), 0x00002B1F);
    phx_write_u32(phy_innr+(0x000066EA << 2), 0x00000450);
    phx_write_u32(phy_innr+(0x000066F5 << 2), 0x00001561);
    phx_write_u32(phy_innr+(0x000066F8 << 2), 0x00001561);
    phx_write_u32(phy_innr+(0x000066FA << 2), 0x000000B0);
    phx_write_u32(phy_innr+(0x000066FB << 2), 0x000000A6);
    phx_write_u32(phy_innr+(0x000066FC << 2), 0x000001B6);
    phx_write_u32(phy_innr+(0x000066FF << 2), 0x00000333);
    phx_write_u32(phy_innr+(0x00006700 << 2), 0x00000738);
    phx_write_u32(phy_innr+(0x00006701 << 2), 0x00000777);
    phx_write_u32(phy_innr+(0x00006702 << 2), 0x00000BCC);
    phx_write_u32(phy_innr+(0x00006703 << 2), 0x00000888);
    phx_write_u32(phy_innr+(0x00006704 << 2), 0x00000CDD);
    phx_write_u32(phy_innr+(0x00006705 << 2), 0x00000888);
    phx_write_u32(phy_innr+(0x00006706 << 2), 0x00000CDD);
    phx_write_u32(phy_innr+(0x00006707 << 2), 0x00000888);
    phx_write_u32(phy_innr+(0x00006708 << 2), 0x00000CDD);
    phx_write_u32(phy_innr+(0x00006709 << 2), 0x00000888);
    phx_write_u32(phy_innr+(0x0000670A << 2), 0x00000CDD);
    phx_write_u32(phy_innr+(0x0000670B << 2), 0x00000599);
    phx_write_u32(phy_innr+(0x0000670C << 2), 0x00000DEE);
    phx_write_u32(phy_innr+(0x0000670E << 2), 0x00000599);
    phx_write_u32(phy_innr+(0x0000670F << 2), 0x00000DEE);
    phx_write_u32(phy_innr+(0x00006711 << 2), 0x000028FF);
    phx_write_u32(phy_innr+(0x00006718 << 2), 0x00000000);
    phx_write_u32(phy_innr+(0x0000671A << 2), 0x00000100);
    phx_write_u32(phy_innr+(0x0000671B << 2), 0x0000000F);
    phx_write_u32(phy_innr+(0x0000671C << 2), 0x00000024);
    phx_write_u32(phy_innr+(0x0000671D << 2), 0x00000024);
    phx_write_u32(phy_innr+(0x0000671E << 2), 0x000001FF);
    phx_write_u32(phy_innr+(0x00006721 << 2), 0x0000001F);
    phx_write_u32(phy_innr+(0x00006722 << 2), 0x00001CE7);
    phx_write_u32(phy_innr+(0x00006723 << 2), 0x00001D67);
    phx_write_u32(phy_innr+(0x00006725 << 2), 0x0000001F);
    phx_write_u32(phy_innr+(0x00006726 << 2), 0x00004000);
    phx_write_u32(phy_innr+(0x00006751 << 2), 0x00002008);
    phx_write_u32(phy_innr+(0x00006755 << 2), 0x0000003F);
    phx_write_u32(phy_innr+(0x00006758 << 2), 0x00006422);
    phx_write_u32(phy_innr+(0x00006759 << 2), 0x00006622);
    phx_write_u32(phy_innr+(0x00006761 << 2), 0x00000023);
    phx_write_u32(phy_innr+(0x00006771 << 2), 0x00000400);
    phx_write_u32(phy_innr+(0x00006775 << 2), 0x000001FF);
    phx_write_u32(phy_innr+(0x0000677C << 2), 0x000000D6);
    phx_write_u32(phy_innr+(0x000067D8 << 2), 0x00000019);
    phx_write_u32(phy_innr+(0x000067D9 << 2), 0x00007FF1);
    phx_write_u32(phy_innr+(0x000067DA << 2), 0x00000028);
    phx_write_u32(phy_innr+(0x000067DB << 2), 0x00000028);
    phx_write_u32(phy_innr+(0x000067DC << 2), 0x00007FFF);
    phx_write_u32(phy_innr+(0x000067DE << 2), 0x00007FC3);
    phx_write_u32(phy_innr+(0x000067DF << 2), 0x00007F81);
    phx_write_u32(phy_innr+(0x000067E0 << 2), 0x00007FC3);
    phx_write_u32(phy_innr+(0x000067E1 << 2), 0x00007FC3);
    phx_write_u32(phy_innr+(0x000067E2 << 2), 0x00007F81);
    phx_write_u32(phy_innr+(0x000067E4 << 2), 0x00000000);
    phx_write_u32(phy_innr+(0x000067E5 << 2), 0x00001000);
    phx_write_u32(phy_innr+(0x000067E6 << 2), 0x00001092);
    phx_write_u32(phy_innr+(0x000067E7 << 2), 0x00001092);
    phx_write_u32(phy_innr+(0x000067E8 << 2), 0x00001000);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000000);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00000E01);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000001);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00000C02);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000002);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00000A03);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000003);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00000804);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000004);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00000805);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000005);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00000806);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000006);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00000807);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000007);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00000808);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000008);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00000809);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000009);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x0000080A);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x0000000A);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x0000080B);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x0000000B);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x0000080C);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x0000000C);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x0000080D);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x0000000D);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x0000090D);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x0000000E);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x0000090E);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x0000000F);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00000A0F);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000010);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00000A10);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000011);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00000B10);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000012);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00000B11);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000013);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00000C12);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000014);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00000C13);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000015);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00000D13);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000016);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00000D14);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000017);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00000E15);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000018);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00000E16);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000019);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00000F16);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x0000001A);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00000F17);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x0000001B);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00001018);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x0000001C);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00001019);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x0000001D);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00001119);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x0000001E);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x0000111A);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x0000001F);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x0000121B);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000020);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x0000121C);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000021);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x0000131C);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000022);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x0000131D);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000023);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x0000141E);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000024);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x0000141F);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000025);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x0000151F);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000026);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00001520);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000027);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00001621);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000028);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00001721);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000029);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00001822);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x0000002A);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00001922);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x0000002B);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00001A23);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x0000002C);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00001B23);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x0000002D);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00001B24);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x0000002E);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00001B25);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x0000002F);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00001B26);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000030);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00001B27);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000031);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00001B28);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000032);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00001B29);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000033);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00001B29);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000034);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00001B2A);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000035);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00001B2C);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000036);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00001B2D);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000037);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00001B2E);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000038);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00001B2F);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x00000039);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00001B30);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x0000003A);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00001B31);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x0000003B);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00001B32);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x0000003C);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00001B33);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x0000003D);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00001B34);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x0000003E);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00001B35);
    phx_write_u32(phy_innr+(0x000066E8 << 2), 0x0000003F);
    phx_write_u32(phy_innr+(0x000066E9 << 2), 0x00001B36);
#endif

#if PHY_BIST_CONFIG
    // tx rx prbs
    rab_phy_bist_config(base_addr);
#endif

#if PHY_SERIAL_LOOPBACK
    phx_write_u32(phy_innr+(0x00006050 << 2), 0xe04); // LANE_LOOPBACK_CTRL_PREG
    phx_write_u32(phy_innr+(0x00006048 << 2), 0x0); // TX_BIASTRIM_PREG
#endif
    
    phx_write_u32(phy_wrap+0x50, 0x7640); // tx deemphasis
    phx_write_u32(phy_wrap+0x68, 0x7640);
    phx_write_u32(phy_wrap+0x80, 0x7640);
    phx_write_u32(phy_wrap+0x98, 0x7640);
    
//    if(hr3_board_type == 2)
//    {
    	rab_phy_rx_termination_set(base_addr, 0);/*ldf 20230627 add:: hc3080b*/
//    }
    
    // pma_rx_eq_training_data_valid_ln
    phx_write_u32(phy_wrap+0x58, 0x1);
    phx_write_u32(phy_wrap+0x70, 0x1);
    phx_write_u32(phy_wrap+0x88, 0x1);
    phx_write_u32(phy_wrap+0xa0, 0x1);
    
    // enable port tx rx
    rab_page_write(base_addr, 0x15c + 0x40 * 0, 0x00600000);
    rab_page_write(base_addr, 0x15c + 0x40 * 1, 0x00600000);
    rab_page_write(base_addr, 0x15c + 0x40 * 2, 0x00600000);
    rab_page_write(base_addr, 0x15c + 0x40 * 3, 0x00600000);
    rab_config(base_addr, 0x8304000c); // 10.3125G x4 idle3
    //rab_config(base_addr, 0x8300000c); // 10.3125G x1x1x1x1 idle3
    //rab_config(base_addr, 0x8301000c); // 10.3125G x2x1x1 idle3
    //rab_config(base_addr, 0x8302000c); // 10.3125G x1x1x2 idle3
    //rab_config(base_addr, 0x8303000c); // 10.3125G x2x2 idle3
    //rab_dlb_loopback(base_addr);
    
    // boot complete
    rab_page_write(base_addr, 0x404, 0x00000001);

    // PHY Reset Free
    // Config reg name: phy_reset
//    if(hr3_board_type == 2)
//    {
    	phx_write_u32(phy_wrap+0x00000000, 0x00111111/*0x00011111*/);/*ldf 20230627 modify:: hc3080b*/
//    }
//    else
//    {
//    	phx_write_u32(phy_wrap+0x00000000, 0x00011111);
//    }

//	uart_printf("pma_cmn_ready[8:8]  phy_wrap 0xC = 0x%x\r\n", phx_read_u32(phy_wrap + 0xc));//0x100
//	uart_printf("tx deemphasis[0:17] phy_wrap 0x50 0x68 0x80 0x98 = 0x%x 0x%x 0x%x 0x%x\r\n",
//			phx_read_u32(phy_wrap + 0x50),
//			phx_read_u32(phy_wrap + 0x68),
//			phx_read_u32(phy_wrap + 0x80),
//			phx_read_u32(phy_wrap + 0x98));//0x7640 0x7640 0x7640 0x7640
//	rab_phy_tx_ffe_para_search_result(base_addr);
    
}

/* phy config_12G5 */
void rab_phy_link_ini_config_12G5(sriox base_addr)
{
    u32 rab_innr;
    u32 rab_wrap;
    u32 phy_wrap;
    u32 phy_innr;

    rab_innr = SRIO_RAB_INNER_ADDR(base_addr);
    rab_wrap = SRIO_RAB_WRAPPER_ADDR(base_addr);
    phy_wrap = SRIO_PHY_WRAPPER_ADDR(base_addr);
    phy_innr = SRIO_PHY_INNER_ADDR(base_addr);

    uart_printf("rab%d 12G5 x4 mode config\r\n", base_addr);

    // reset rab and phy
//    if(hr3_board_type == 2)
//    {
		phx_write_u32(rab_wrap+0x00000024, 0x00000010);/*ldf 20230627 add:: hc3080b*/
		phx_write_u32(phy_wrap+0x00000000, 0x00000000);/*ldf 20230627 add:: hc3080b*/
//    }
    
    phx_write_u32(rab_wrap+0x00000010, 0x00014021); // x4 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00010021); // x1x1x1x1 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00011021); // x2x1x1 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00012021); // x1x1x2 mode
//    phx_write_u32(rab_wrap+0x00000010, 0x00013021); // x2x2 mode
    
    // srio_port0_mode
    phx_write_u32(rab_wrap+0x00000014, 0x00000933);
    phx_write_u32(rab_wrap+0x0000001C, 0x00000933);
    phx_write_u32(rab_wrap+0x00000018, 0x00000933);
    phx_write_u32(rab_wrap+0x00000020, 0x00000933);
    // srio rx_ready_timer
    phx_write_u32(rab_wrap+0x00000028, 30000);
    phx_write_u32(rab_wrap+0x0000002c, 32000);
    // Config reg name: srio_reset
//    if(hr3_board_type == 2)
//    {
    	phx_write_u32(rab_wrap+0x00000024, 0x00000101/*0x00000001*/);/*ldf 20230627 modify:: hc3080b*/
//    }
//    else
//    {
//    	phx_write_u32(rab_wrap+0x00000024, 0x00000001);
//    }
    udelay(100);

    // Config reg name: phy apb reset release
//    if(hr3_board_type == 2)
//    {
    	phx_write_u32(phy_wrap+0x00000000, 0x00100000);/*ldf 20230627 add:: hc3080b*/
//    }
    
    phx_write_u32(phy_wrap+0x50, 0x7640); // tx deemphasis
    phx_write_u32(phy_wrap+0x68, 0x7640);
    phx_write_u32(phy_wrap+0x80, 0x7640);
    phx_write_u32(phy_wrap+0x98, 0x7640);
    
//    if(hr3_board_type == 2)
//    {
    	rab_phy_rx_termination_set(base_addr, 0);/*ldf 20230627 add:: hc3080b*/
//    }

    // pma_rx_eq_training_data_valid_ln
    phx_write_u32(phy_wrap+0x58, 0x1);
    phx_write_u32(phy_wrap+0x70, 0x1);
    phx_write_u32(phy_wrap+0x88, 0x1);
    phx_write_u32(phy_wrap+0xa0, 0x1);

    // serdes_width
    // port0~3 32bit
    phx_write_u32(phy_wrap+0x00000018, 0x20101002);
    phx_write_u32(phy_wrap+0x00000024, 0x20101002);
    phx_write_u32(phy_wrap+0x00000030, 0x20101002);
    phx_write_u32(phy_wrap+0x0000003c, 0x20101002);
    // Config reg name: pma_link_mode
    phx_write_u32(phy_wrap+0x00000004, 0x00000000); // x4, 1 link
    //phx_write_u32(phy_wrap+0x00000004, 0x00003210); // x1x1x1x1, 4 link
    //phx_write_u32(phy_wrap+0x00000004, 0x00001100); // x2x2, 2 link
    // Config reg name: pma_refclk_sel
    phx_write_u32(phy_wrap+0x00000008, 0x00003200);

    //////////////////////////////////////
    //   PHY config for SRIO 12.5G NS   //
    //////////////////////////////////////
    // PHY Register CDB
    phx_write_u32(phy_innr + (0x0000C00E << 2), 0x00000000);
    // PMA Common CDB
    phx_write_u32(phy_innr + (0x0000006E << 2), 0x00000180);
    phx_write_u32(phy_innr + (0x00000098 << 2), 0x00006000);
    phx_write_u32(phy_innr + (0x000000A0 << 2), 0x00000033);
    phx_write_u32(phy_innr + (0x00000043 << 2), 0x0000001E);
    phx_write_u32(phy_innr + (0x0000004A << 2), 0x00002106);
    phx_write_u32(phy_innr + (0x0000004C << 2), 0x00000000);
    phx_write_u32(phy_innr + (0x0000004E << 2), 0x00008002);
    phx_write_u32(phy_innr + (0x00000050 << 2), 0x00000000);
    phx_write_u32(phy_innr + (0x0000005D << 2), 0x00000027);
    phx_write_u32(phy_innr + (0x0000005E << 2), 0x00000062);
    phx_write_u32(phy_innr + (0x00000062 << 2), 0x00000800);
    // PMA Lane CDB
    phx_write_u32(phy_innr + (0x00004000 << 2), 0x0000ED09);
    phx_write_u32(phy_innr + (0x00004200 << 2), 0x0000ED09);
    phx_write_u32(phy_innr + (0x00004400 << 2), 0x0000ED09);
    phx_write_u32(phy_innr + (0x00004600 << 2), 0x0000ED09);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004001 << 2), 0x0000000D);
    phx_write_u32(phy_innr + (0x00004201 << 2), 0x0000000D);
    phx_write_u32(phy_innr + (0x00004401 << 2), 0x0000000D);
    phx_write_u32(phy_innr + (0x00004601 << 2), 0x0000000D);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004002 << 2), 0x000000A2);
    phx_write_u32(phy_innr + (0x00004202 << 2), 0x000000A2);
    phx_write_u32(phy_innr + (0x00004402 << 2), 0x000000A2);
    phx_write_u32(phy_innr + (0x00004602 << 2), 0x000000A2);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004003 << 2), 0x0000691E);
    phx_write_u32(phy_innr + (0x00004203 << 2), 0x0000691E);
    phx_write_u32(phy_innr + (0x00004403 << 2), 0x0000691E);
    phx_write_u32(phy_innr + (0x00004603 << 2), 0x0000691E);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004004 << 2), 0x00000291);
    phx_write_u32(phy_innr + (0x00004204 << 2), 0x00000291);
    phx_write_u32(phy_innr + (0x00004404 << 2), 0x00000291);
    phx_write_u32(phy_innr + (0x00004604 << 2), 0x00000291);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004030 << 2), 0x00000FFE);
    phx_write_u32(phy_innr + (0x00004230 << 2), 0x00000FFE);
    phx_write_u32(phy_innr + (0x00004430 << 2), 0x00000FFE);
    phx_write_u32(phy_innr + (0x00004630 << 2), 0x00000FFE);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004039 << 2), 0x00000104);
    phx_write_u32(phy_innr + (0x00004239 << 2), 0x00000104);
    phx_write_u32(phy_innr + (0x00004439 << 2), 0x00000104);
    phx_write_u32(phy_innr + (0x00004639 << 2), 0x00000104);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x0000403A << 2), 0x00000010);
    phx_write_u32(phy_innr + (0x0000423A << 2), 0x00000010);
    phx_write_u32(phy_innr + (0x0000443A << 2), 0x00000010);
    phx_write_u32(phy_innr + (0x0000463A << 2), 0x00000010);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x0000403E << 2), 0x00000104);
    phx_write_u32(phy_innr + (0x0000423E << 2), 0x00000104);
    phx_write_u32(phy_innr + (0x0000443E << 2), 0x00000104);
    phx_write_u32(phy_innr + (0x0000463E << 2), 0x00000104);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x0000404C << 2), 0x00000D33);
    phx_write_u32(phy_innr + (0x0000424C << 2), 0x00000D33);
    phx_write_u32(phy_innr + (0x0000444C << 2), 0x00000D33);
    phx_write_u32(phy_innr + (0x0000464C << 2), 0x00000D33);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x0000406A << 2), 0x00000000);
    phx_write_u32(phy_innr + (0x0000426A << 2), 0x00000000);
    phx_write_u32(phy_innr + (0x0000446A << 2), 0x00000000);
    phx_write_u32(phy_innr + (0x0000466A << 2), 0x00000000);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x0000406F << 2), 0x00001402);
    phx_write_u32(phy_innr + (0x0000426F << 2), 0x00001402);
    phx_write_u32(phy_innr + (0x0000446F << 2), 0x00001402);
    phx_write_u32(phy_innr + (0x0000466F << 2), 0x00001402);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004088 << 2), 0x0000808B);
    phx_write_u32(phy_innr + (0x00004288 << 2), 0x0000808B);
    phx_write_u32(phy_innr + (0x00004488 << 2), 0x0000808B);
    phx_write_u32(phy_innr + (0x00004688 << 2), 0x0000808B);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x0000408E << 2), 0x00003C7F);
    phx_write_u32(phy_innr + (0x0000428E << 2), 0x00003C7F);
    phx_write_u32(phy_innr + (0x0000448E << 2), 0x00003C7F);
    phx_write_u32(phy_innr + (0x0000468E << 2), 0x00003C7F);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004092 << 2), 0x00003233);
    phx_write_u32(phy_innr + (0x00004292 << 2), 0x00003233);
    phx_write_u32(phy_innr + (0x00004492 << 2), 0x00003233);
    phx_write_u32(phy_innr + (0x00004692 << 2), 0x00003233);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004093 << 2), 0x00000102);
    phx_write_u32(phy_innr + (0x00004293 << 2), 0x00000102);
    phx_write_u32(phy_innr + (0x00004493 << 2), 0x00000102);
    phx_write_u32(phy_innr + (0x00004693 << 2), 0x00000102);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000040AC << 2), 0x00006A6A);
    phx_write_u32(phy_innr + (0x000042AC << 2), 0x00006A6A);
    phx_write_u32(phy_innr + (0x000044AC << 2), 0x00006A6A);
    phx_write_u32(phy_innr + (0x000046AC << 2), 0x00006A6A);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000040AD << 2), 0x00000080);
    phx_write_u32(phy_innr + (0x000042AD << 2), 0x00000080);
    phx_write_u32(phy_innr + (0x000044AD << 2), 0x00000080);
    phx_write_u32(phy_innr + (0x000046AD << 2), 0x00000080);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000040AF << 2), 0x00001C08);
    phx_write_u32(phy_innr + (0x000042AF << 2), 0x00001C08);
    phx_write_u32(phy_innr + (0x000044AF << 2), 0x00001C08);
    phx_write_u32(phy_innr + (0x000046AF << 2), 0x00001C08);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000040C0 << 2), 0x00004112);
    phx_write_u32(phy_innr + (0x000042C0 << 2), 0x00004112);
    phx_write_u32(phy_innr + (0x000044C0 << 2), 0x00004112);
    phx_write_u32(phy_innr + (0x000046C0 << 2), 0x00004112);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000040C1 << 2), 0x00004112);
    phx_write_u32(phy_innr + (0x000042C1 << 2), 0x00004112);
    phx_write_u32(phy_innr + (0x000044C1 << 2), 0x00004112);
    phx_write_u32(phy_innr + (0x000046C1 << 2), 0x00004112);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000040C8 << 2), 0x00000028);
    phx_write_u32(phy_innr + (0x000042C8 << 2), 0x00000028);
    phx_write_u32(phy_innr + (0x000044C8 << 2), 0x00000028);
    phx_write_u32(phy_innr + (0x000046C8 << 2), 0x00000028);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000040D0 << 2), 0x00003E3E);
    phx_write_u32(phy_innr + (0x000042D0 << 2), 0x00003E3E);
    phx_write_u32(phy_innr + (0x000044D0 << 2), 0x00003E3E);
    phx_write_u32(phy_innr + (0x000046D0 << 2), 0x00003E3E);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000040E1 << 2), 0x00003102);
    phx_write_u32(phy_innr + (0x000042E1 << 2), 0x00003102);
    phx_write_u32(phy_innr + (0x000044E1 << 2), 0x00003102);
    phx_write_u32(phy_innr + (0x000046E1 << 2), 0x00003102);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000040E3 << 2), 0x00002B1F);
    phx_write_u32(phy_innr + (0x000042E3 << 2), 0x00002B1F);
    phx_write_u32(phy_innr + (0x000044E3 << 2), 0x00002B1F);
    phx_write_u32(phy_innr + (0x000046E3 << 2), 0x00002B1F);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000040EA << 2), 0x00000450);
    phx_write_u32(phy_innr + (0x000042EA << 2), 0x00000450);
    phx_write_u32(phy_innr + (0x000044EA << 2), 0x00000450);
    phx_write_u32(phy_innr + (0x000046EA << 2), 0x00000450);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000040F5 << 2), 0x00001561);
    phx_write_u32(phy_innr + (0x000042F5 << 2), 0x00001561);
    phx_write_u32(phy_innr + (0x000044F5 << 2), 0x00001561);
    phx_write_u32(phy_innr + (0x000046F5 << 2), 0x00001561);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000040F8 << 2), 0x00001561);
    phx_write_u32(phy_innr + (0x000042F8 << 2), 0x00001561);
    phx_write_u32(phy_innr + (0x000044F8 << 2), 0x00001561);
    phx_write_u32(phy_innr + (0x000046F8 << 2), 0x00001561);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000040FA << 2), 0x000000B0);
    phx_write_u32(phy_innr + (0x000042FA << 2), 0x000000B0);
    phx_write_u32(phy_innr + (0x000044FA << 2), 0x000000B0);
    phx_write_u32(phy_innr + (0x000046FA << 2), 0x000000B0);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000040FB << 2), 0x00000126);
    phx_write_u32(phy_innr + (0x000042FB << 2), 0x00000126);
    phx_write_u32(phy_innr + (0x000044FB << 2), 0x00000126);
    phx_write_u32(phy_innr + (0x000046FB << 2), 0x00000126);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000040FC << 2), 0x000001B6);
    phx_write_u32(phy_innr + (0x000042FC << 2), 0x000001B6);
    phx_write_u32(phy_innr + (0x000044FC << 2), 0x000001B6);
    phx_write_u32(phy_innr + (0x000046FC << 2), 0x000001B6);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000040FF << 2), 0x00000333);
    phx_write_u32(phy_innr + (0x000042FF << 2), 0x00000333);
    phx_write_u32(phy_innr + (0x000044FF << 2), 0x00000333);
    phx_write_u32(phy_innr + (0x000046FF << 2), 0x00000333);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004100 << 2), 0x00000738);
    phx_write_u32(phy_innr + (0x00004300 << 2), 0x00000738);
    phx_write_u32(phy_innr + (0x00004500 << 2), 0x00000738);
    phx_write_u32(phy_innr + (0x00004700 << 2), 0x00000738);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004101 << 2), 0x00000777);
    phx_write_u32(phy_innr + (0x00004301 << 2), 0x00000777);
    phx_write_u32(phy_innr + (0x00004501 << 2), 0x00000777);
    phx_write_u32(phy_innr + (0x00004701 << 2), 0x00000777);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004102 << 2), 0x00000BCC);
    phx_write_u32(phy_innr + (0x00004302 << 2), 0x00000BCC);
    phx_write_u32(phy_innr + (0x00004502 << 2), 0x00000BCC);
    phx_write_u32(phy_innr + (0x00004702 << 2), 0x00000BCC);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004103 << 2), 0x00000888);
    phx_write_u32(phy_innr + (0x00004303 << 2), 0x00000888);
    phx_write_u32(phy_innr + (0x00004503 << 2), 0x00000888);
    phx_write_u32(phy_innr + (0x00004703 << 2), 0x00000888);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004104 << 2), 0x00000CDD);
    phx_write_u32(phy_innr + (0x00004304 << 2), 0x00000CDD);
    phx_write_u32(phy_innr + (0x00004504 << 2), 0x00000CDD);
    phx_write_u32(phy_innr + (0x00004704 << 2), 0x00000CDD);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004105 << 2), 0x00000888);
    phx_write_u32(phy_innr + (0x00004305 << 2), 0x00000888);
    phx_write_u32(phy_innr + (0x00004505 << 2), 0x00000888);
    phx_write_u32(phy_innr + (0x00004705 << 2), 0x00000888);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004106 << 2), 0x00000CDD);
    phx_write_u32(phy_innr + (0x00004306 << 2), 0x00000CDD);
    phx_write_u32(phy_innr + (0x00004506 << 2), 0x00000CDD);
    phx_write_u32(phy_innr + (0x00004706 << 2), 0x00000CDD);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004107 << 2), 0x00000888);
    phx_write_u32(phy_innr + (0x00004307 << 2), 0x00000888);
    phx_write_u32(phy_innr + (0x00004507 << 2), 0x00000888);
    phx_write_u32(phy_innr + (0x00004707 << 2), 0x00000888);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004108 << 2), 0x00000CDD);
    phx_write_u32(phy_innr + (0x00004308 << 2), 0x00000CDD);
    phx_write_u32(phy_innr + (0x00004508 << 2), 0x00000CDD);
    phx_write_u32(phy_innr + (0x00004708 << 2), 0x00000CDD);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004109 << 2), 0x00000888);
    phx_write_u32(phy_innr + (0x00004309 << 2), 0x00000888);
    phx_write_u32(phy_innr + (0x00004509 << 2), 0x00000888);
    phx_write_u32(phy_innr + (0x00004709 << 2), 0x00000888);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x0000410A << 2), 0x00000CDD);
    phx_write_u32(phy_innr + (0x0000430A << 2), 0x00000CDD);
    phx_write_u32(phy_innr + (0x0000450A << 2), 0x00000CDD);
    phx_write_u32(phy_innr + (0x0000470A << 2), 0x00000CDD);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x0000410B << 2), 0x00000599);
    phx_write_u32(phy_innr + (0x0000430B << 2), 0x00000599);
    phx_write_u32(phy_innr + (0x0000450B << 2), 0x00000599);
    phx_write_u32(phy_innr + (0x0000470B << 2), 0x00000599);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x0000410C << 2), 0x00000DEE);
    phx_write_u32(phy_innr + (0x0000430C << 2), 0x00000DEE);
    phx_write_u32(phy_innr + (0x0000450C << 2), 0x00000DEE);
    phx_write_u32(phy_innr + (0x0000470C << 2), 0x00000DEE);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x0000410E << 2), 0x00000599);
    phx_write_u32(phy_innr + (0x0000430E << 2), 0x00000599);
    phx_write_u32(phy_innr + (0x0000450E << 2), 0x00000599);
    phx_write_u32(phy_innr + (0x0000470E << 2), 0x00000599);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x0000410F << 2), 0x00000DEE);
    phx_write_u32(phy_innr + (0x0000430F << 2), 0x00000DEE);
    phx_write_u32(phy_innr + (0x0000450F << 2), 0x00000DEE);
    phx_write_u32(phy_innr + (0x0000470F << 2), 0x00000DEE);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004111 << 2), 0x000028FF);
    phx_write_u32(phy_innr + (0x00004311 << 2), 0x000028FF);
    phx_write_u32(phy_innr + (0x00004511 << 2), 0x000028FF);
    phx_write_u32(phy_innr + (0x00004711 << 2), 0x000028FF);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004118 << 2), 0x00000000);
    phx_write_u32(phy_innr + (0x00004318 << 2), 0x00000000);
    phx_write_u32(phy_innr + (0x00004518 << 2), 0x00000000);
    phx_write_u32(phy_innr + (0x00004718 << 2), 0x00000000);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x0000411B << 2), 0x0000000F);
    phx_write_u32(phy_innr + (0x0000431B << 2), 0x0000000F);
    phx_write_u32(phy_innr + (0x0000451B << 2), 0x0000000F);
    phx_write_u32(phy_innr + (0x0000471B << 2), 0x0000000F);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x0000411C << 2), 0x00000024);
    phx_write_u32(phy_innr + (0x0000431C << 2), 0x00000024);
    phx_write_u32(phy_innr + (0x0000451C << 2), 0x00000024);
    phx_write_u32(phy_innr + (0x0000471C << 2), 0x00000024);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x0000411D << 2), 0x00000024);
    phx_write_u32(phy_innr + (0x0000431D << 2), 0x00000024);
    phx_write_u32(phy_innr + (0x0000451D << 2), 0x00000024);
    phx_write_u32(phy_innr + (0x0000471D << 2), 0x00000024);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x0000411E << 2), 0x000001FF);
    phx_write_u32(phy_innr + (0x0000431E << 2), 0x000001FF);
    phx_write_u32(phy_innr + (0x0000451E << 2), 0x000001FF);
    phx_write_u32(phy_innr + (0x0000471E << 2), 0x000001FF);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004121 << 2), 0x0000001F);
    phx_write_u32(phy_innr + (0x00004321 << 2), 0x0000001F);
    phx_write_u32(phy_innr + (0x00004521 << 2), 0x0000001F);
    phx_write_u32(phy_innr + (0x00004721 << 2), 0x0000001F);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004122 << 2), 0x00001CE7);
    phx_write_u32(phy_innr + (0x00004322 << 2), 0x00001CE7);
    phx_write_u32(phy_innr + (0x00004522 << 2), 0x00001CE7);
    phx_write_u32(phy_innr + (0x00004722 << 2), 0x00001CE7);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004123 << 2), 0x00001D67);
    phx_write_u32(phy_innr + (0x00004323 << 2), 0x00001D67);
    phx_write_u32(phy_innr + (0x00004523 << 2), 0x00001D67);
    phx_write_u32(phy_innr + (0x00004723 << 2), 0x00001D67);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004125 << 2), 0x0000001F);
    phx_write_u32(phy_innr + (0x00004325 << 2), 0x0000001F);
    phx_write_u32(phy_innr + (0x00004525 << 2), 0x0000001F);
    phx_write_u32(phy_innr + (0x00004725 << 2), 0x0000001F);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004126 << 2), 0x00004000);
    phx_write_u32(phy_innr + (0x00004326 << 2), 0x00004000);
    phx_write_u32(phy_innr + (0x00004526 << 2), 0x00004000);
    phx_write_u32(phy_innr + (0x00004726 << 2), 0x00004000);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004151 << 2), 0x00002008);
    phx_write_u32(phy_innr + (0x00004351 << 2), 0x00002008);
    phx_write_u32(phy_innr + (0x00004551 << 2), 0x00002008);
    phx_write_u32(phy_innr + (0x00004751 << 2), 0x00002008);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004155 << 2), 0x0000003F);
    phx_write_u32(phy_innr + (0x00004355 << 2), 0x0000003F);
    phx_write_u32(phy_innr + (0x00004555 << 2), 0x0000003F);
    phx_write_u32(phy_innr + (0x00004755 << 2), 0x0000003F);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004158 << 2), 0x0000642F);
    phx_write_u32(phy_innr + (0x00004358 << 2), 0x0000642F);
    phx_write_u32(phy_innr + (0x00004558 << 2), 0x0000642F);
    phx_write_u32(phy_innr + (0x00004758 << 2), 0x0000642F);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004159 << 2), 0x00006629);
    phx_write_u32(phy_innr + (0x00004359 << 2), 0x00006629);
    phx_write_u32(phy_innr + (0x00004559 << 2), 0x00006629);
    phx_write_u32(phy_innr + (0x00004759 << 2), 0x00006629);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004161 << 2), 0x00000023);
    phx_write_u32(phy_innr + (0x00004361 << 2), 0x00000023);
    phx_write_u32(phy_innr + (0x00004561 << 2), 0x00000023);
    phx_write_u32(phy_innr + (0x00004761 << 2), 0x00000023);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004171 << 2), 0x00000400);
    phx_write_u32(phy_innr + (0x00004371 << 2), 0x00000400);
    phx_write_u32(phy_innr + (0x00004571 << 2), 0x00000400);
    phx_write_u32(phy_innr + (0x00004771 << 2), 0x00000400);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004175 << 2), 0x000001FF);
    phx_write_u32(phy_innr + (0x00004375 << 2), 0x000001FF);
    phx_write_u32(phy_innr + (0x00004575 << 2), 0x000001FF);
    phx_write_u32(phy_innr + (0x00004775 << 2), 0x000001FF);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x0000417C << 2), 0x000000D6);
    phx_write_u32(phy_innr + (0x0000437C << 2), 0x000000D6);
    phx_write_u32(phy_innr + (0x0000457C << 2), 0x000000D6);
    phx_write_u32(phy_innr + (0x0000477C << 2), 0x000000D6);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000041B0 << 2), 0x0000911E);
    phx_write_u32(phy_innr + (0x000043B0 << 2), 0x0000911E);
    phx_write_u32(phy_innr + (0x000045B0 << 2), 0x0000911E);
    phx_write_u32(phy_innr + (0x000047B0 << 2), 0x0000911E);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000041D8 << 2), 0x00007FFF);
    phx_write_u32(phy_innr + (0x000043D8 << 2), 0x00007FFF);
    phx_write_u32(phy_innr + (0x000045D8 << 2), 0x00007FFF);
    phx_write_u32(phy_innr + (0x000047D8 << 2), 0x00007FFF);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000041D9 << 2), 0x00000028);
    phx_write_u32(phy_innr + (0x000043D9 << 2), 0x00000028);
    phx_write_u32(phy_innr + (0x000045D9 << 2), 0x00000028);
    phx_write_u32(phy_innr + (0x000047D9 << 2), 0x00000028);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000041DA << 2), 0x00000028);
    phx_write_u32(phy_innr + (0x000043DA << 2), 0x00000028);
    phx_write_u32(phy_innr + (0x000045DA << 2), 0x00000028);
    phx_write_u32(phy_innr + (0x000047DA << 2), 0x00000028);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000041DB << 2), 0x00000028);
    phx_write_u32(phy_innr + (0x000043DB << 2), 0x00000028);
    phx_write_u32(phy_innr + (0x000045DB << 2), 0x00000028);
    phx_write_u32(phy_innr + (0x000047DB << 2), 0x00000028);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000041DC << 2), 0x00007FFF);
    phx_write_u32(phy_innr + (0x000043DC << 2), 0x00007FFF);
    phx_write_u32(phy_innr + (0x000045DC << 2), 0x00007FFF);
    phx_write_u32(phy_innr + (0x000047DC << 2), 0x00007FFF);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000041DD << 2), 0x0000000F);
    phx_write_u32(phy_innr + (0x000043DD << 2), 0x0000000F);
    phx_write_u32(phy_innr + (0x000045DD << 2), 0x0000000F);
    phx_write_u32(phy_innr + (0x000047DD << 2), 0x0000000F);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000041DE << 2), 0x00007FC3);
    phx_write_u32(phy_innr + (0x000043DE << 2), 0x00007FC3);
    phx_write_u32(phy_innr + (0x000045DE << 2), 0x00007FC3);
    phx_write_u32(phy_innr + (0x000047DE << 2), 0x00007FC3);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000041DF << 2), 0x00007FC3);
    phx_write_u32(phy_innr + (0x000043DF << 2), 0x00007FC3);
    phx_write_u32(phy_innr + (0x000045DF << 2), 0x00007FC3);
    phx_write_u32(phy_innr + (0x000047DF << 2), 0x00007FC3);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000041E0 << 2), 0x00007FC3);
    phx_write_u32(phy_innr + (0x000043E0 << 2), 0x00007FC3);
    phx_write_u32(phy_innr + (0x000045E0 << 2), 0x00007FC3);
    phx_write_u32(phy_innr + (0x000047E0 << 2), 0x00007FC3);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000041E1 << 2), 0x00007FC3);
    phx_write_u32(phy_innr + (0x000043E1 << 2), 0x00007FC3);
    phx_write_u32(phy_innr + (0x000045E1 << 2), 0x00007FC3);
    phx_write_u32(phy_innr + (0x000047E1 << 2), 0x00007FC3);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000041E2 << 2), 0x00007F81);
    phx_write_u32(phy_innr + (0x000043E2 << 2), 0x00007F81);
    phx_write_u32(phy_innr + (0x000045E2 << 2), 0x00007F81);
    phx_write_u32(phy_innr + (0x000047E2 << 2), 0x00007F81);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000041E3 << 2), 0x00007FF1);
    phx_write_u32(phy_innr + (0x000043E3 << 2), 0x00007FF1);
    phx_write_u32(phy_innr + (0x000045E3 << 2), 0x00007FF1);
    phx_write_u32(phy_innr + (0x000047E3 << 2), 0x00007FF1);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000041E4 << 2), 0x00000000);
    phx_write_u32(phy_innr + (0x000043E4 << 2), 0x00000000);
    phx_write_u32(phy_innr + (0x000045E4 << 2), 0x00000000);
    phx_write_u32(phy_innr + (0x000047E4 << 2), 0x00000000);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000041E5 << 2), 0x00001092);
    phx_write_u32(phy_innr + (0x000043E5 << 2), 0x00001092);
    phx_write_u32(phy_innr + (0x000045E5 << 2), 0x00001092);
    phx_write_u32(phy_innr + (0x000047E5 << 2), 0x00001092);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000041E6 << 2), 0x00001092);
    phx_write_u32(phy_innr + (0x000043E6 << 2), 0x00001092);
    phx_write_u32(phy_innr + (0x000045E6 << 2), 0x00001092);
    phx_write_u32(phy_innr + (0x000047E6 << 2), 0x00001092);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000041E7 << 2), 0x00001092);
    phx_write_u32(phy_innr + (0x000043E7 << 2), 0x00001092);
    phx_write_u32(phy_innr + (0x000045E7 << 2), 0x00001092);
    phx_write_u32(phy_innr + (0x000047E7 << 2), 0x00001092);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x000041E8 << 2), 0x00001000);
    phx_write_u32(phy_innr + (0x000043E8 << 2), 0x00001000);
    phx_write_u32(phy_innr + (0x000045E8 << 2), 0x00001000);
    phx_write_u32(phy_innr + (0x000047E8 << 2), 0x00001000);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004098 << 2), 0x00000000);
    phx_write_u32(phy_innr + (0x00004298 << 2), 0x00000000);
    phx_write_u32(phy_innr + (0x00004498 << 2), 0x00000000);
    phx_write_u32(phy_innr + (0x00004698 << 2), 0x00000000);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004099 << 2), 0x0000050B);
    phx_write_u32(phy_innr + (0x00004299 << 2), 0x0000050B);
    phx_write_u32(phy_innr + (0x00004499 << 2), 0x0000050B);
    phx_write_u32(phy_innr + (0x00004699 << 2), 0x0000050B);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004098 << 2), 0x00000001);
    phx_write_u32(phy_innr + (0x00004298 << 2), 0x00000001);
    phx_write_u32(phy_innr + (0x00004498 << 2), 0x00000001);
    phx_write_u32(phy_innr + (0x00004698 << 2), 0x00000001);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004099 << 2), 0x0000068B);
    phx_write_u32(phy_innr + (0x00004299 << 2), 0x0000068B);
    phx_write_u32(phy_innr + (0x00004499 << 2), 0x0000068B);
    phx_write_u32(phy_innr + (0x00004699 << 2), 0x0000068B);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004098 << 2), 0x00000002);
    phx_write_u32(phy_innr + (0x00004298 << 2), 0x00000002);
    phx_write_u32(phy_innr + (0x00004498 << 2), 0x00000002);
    phx_write_u32(phy_innr + (0x00004698 << 2), 0x00000002);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004099 << 2), 0x0000080B);
    phx_write_u32(phy_innr + (0x00004299 << 2), 0x0000080B);
    phx_write_u32(phy_innr + (0x00004499 << 2), 0x0000080B);
    phx_write_u32(phy_innr + (0x00004699 << 2), 0x0000080B);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004098 << 2), 0x00000003);
    phx_write_u32(phy_innr + (0x00004298 << 2), 0x00000003);
    phx_write_u32(phy_innr + (0x00004498 << 2), 0x00000003);
    phx_write_u32(phy_innr + (0x00004698 << 2), 0x00000003);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004099 << 2), 0x0000098B);
    phx_write_u32(phy_innr + (0x00004299 << 2), 0x0000098B);
    phx_write_u32(phy_innr + (0x00004499 << 2), 0x0000098B);
    phx_write_u32(phy_innr + (0x00004699 << 2), 0x0000098B);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004098 << 2), 0x00000004);
    phx_write_u32(phy_innr + (0x00004298 << 2), 0x00000004);
    phx_write_u32(phy_innr + (0x00004498 << 2), 0x00000004);
    phx_write_u32(phy_innr + (0x00004698 << 2), 0x00000004);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004099 << 2), 0x00000B0B);
    phx_write_u32(phy_innr + (0x00004299 << 2), 0x00000B0B);
    phx_write_u32(phy_innr + (0x00004499 << 2), 0x00000B0B);
    phx_write_u32(phy_innr + (0x00004699 << 2), 0x00000B0B);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004098 << 2), 0x00000005);
    phx_write_u32(phy_innr + (0x00004298 << 2), 0x00000005);
    phx_write_u32(phy_innr + (0x00004498 << 2), 0x00000005);
    phx_write_u32(phy_innr + (0x00004698 << 2), 0x00000005);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004099 << 2), 0x00000C8B);
    phx_write_u32(phy_innr + (0x00004299 << 2), 0x00000C8B);
    phx_write_u32(phy_innr + (0x00004499 << 2), 0x00000C8B);
    phx_write_u32(phy_innr + (0x00004699 << 2), 0x00000C8B);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004098 << 2), 0x00000006);
    phx_write_u32(phy_innr + (0x00004298 << 2), 0x00000006);
    phx_write_u32(phy_innr + (0x00004498 << 2), 0x00000006);
    phx_write_u32(phy_innr + (0x00004698 << 2), 0x00000006);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004099 << 2), 0x00000D8B);
    phx_write_u32(phy_innr + (0x00004299 << 2), 0x00000D8B);
    phx_write_u32(phy_innr + (0x00004499 << 2), 0x00000D8B);
    phx_write_u32(phy_innr + (0x00004699 << 2), 0x00000D8B);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004098 << 2), 0x00000007);
    phx_write_u32(phy_innr + (0x00004298 << 2), 0x00000007);
    phx_write_u32(phy_innr + (0x00004498 << 2), 0x00000007);
    phx_write_u32(phy_innr + (0x00004698 << 2), 0x00000007);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004099 << 2), 0x00000F0B);
    phx_write_u32(phy_innr + (0x00004299 << 2), 0x00000F0B);
    phx_write_u32(phy_innr + (0x00004499 << 2), 0x00000F0B);
    phx_write_u32(phy_innr + (0x00004699 << 2), 0x00000F0B);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004098 << 2), 0x00000008);
    phx_write_u32(phy_innr + (0x00004298 << 2), 0x00000008);
    phx_write_u32(phy_innr + (0x00004498 << 2), 0x00000008);
    phx_write_u32(phy_innr + (0x00004698 << 2), 0x00000008);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004099 << 2), 0x0000108B);
    phx_write_u32(phy_innr + (0x00004299 << 2), 0x0000108B);
    phx_write_u32(phy_innr + (0x00004499 << 2), 0x0000108B);
    phx_write_u32(phy_innr + (0x00004699 << 2), 0x0000108B);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004098 << 2), 0x00000009);
    phx_write_u32(phy_innr + (0x00004298 << 2), 0x00000009);
    phx_write_u32(phy_innr + (0x00004498 << 2), 0x00000009);
    phx_write_u32(phy_innr + (0x00004698 << 2), 0x00000009);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004099 << 2), 0x0000118B);
    phx_write_u32(phy_innr + (0x00004299 << 2), 0x0000118B);
    phx_write_u32(phy_innr + (0x00004499 << 2), 0x0000118B);
    phx_write_u32(phy_innr + (0x00004699 << 2), 0x0000118B);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004098 << 2), 0x0000000A);
    phx_write_u32(phy_innr + (0x00004298 << 2), 0x0000000A);
    phx_write_u32(phy_innr + (0x00004498 << 2), 0x0000000A);
    phx_write_u32(phy_innr + (0x00004698 << 2), 0x0000000A);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004099 << 2), 0x0000128B);
    phx_write_u32(phy_innr + (0x00004299 << 2), 0x0000128B);
    phx_write_u32(phy_innr + (0x00004499 << 2), 0x0000128B);
    phx_write_u32(phy_innr + (0x00004699 << 2), 0x0000128B);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004098 << 2), 0x0000000B);
    phx_write_u32(phy_innr + (0x00004298 << 2), 0x0000000B);
    phx_write_u32(phy_innr + (0x00004498 << 2), 0x0000000B);
    phx_write_u32(phy_innr + (0x00004698 << 2), 0x0000000B);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004099 << 2), 0x0000140B);
    phx_write_u32(phy_innr + (0x00004299 << 2), 0x0000140B);
    phx_write_u32(phy_innr + (0x00004499 << 2), 0x0000140B);
    phx_write_u32(phy_innr + (0x00004699 << 2), 0x0000140B);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004098 << 2), 0x0000000C);
    phx_write_u32(phy_innr + (0x00004298 << 2), 0x0000000C);
    phx_write_u32(phy_innr + (0x00004498 << 2), 0x0000000C);
    phx_write_u32(phy_innr + (0x00004698 << 2), 0x0000000C);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004099 << 2), 0x0000150B);
    phx_write_u32(phy_innr + (0x00004299 << 2), 0x0000150B);
    phx_write_u32(phy_innr + (0x00004499 << 2), 0x0000150B);
    phx_write_u32(phy_innr + (0x00004699 << 2), 0x0000150B);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004098 << 2), 0x0000000D);
    phx_write_u32(phy_innr + (0x00004298 << 2), 0x0000000D);
    phx_write_u32(phy_innr + (0x00004498 << 2), 0x0000000D);
    phx_write_u32(phy_innr + (0x00004698 << 2), 0x0000000D);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004099 << 2), 0x0000160B);
    phx_write_u32(phy_innr + (0x00004299 << 2), 0x0000160B);
    phx_write_u32(phy_innr + (0x00004499 << 2), 0x0000160B);
    phx_write_u32(phy_innr + (0x00004699 << 2), 0x0000160B);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004098 << 2), 0x0000000E);
    phx_write_u32(phy_innr + (0x00004298 << 2), 0x0000000E);
    phx_write_u32(phy_innr + (0x00004498 << 2), 0x0000000E);
    phx_write_u32(phy_innr + (0x00004698 << 2), 0x0000000E);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004099 << 2), 0x0000170B);
    phx_write_u32(phy_innr + (0x00004299 << 2), 0x0000170B);
    phx_write_u32(phy_innr + (0x00004499 << 2), 0x0000170B);
    phx_write_u32(phy_innr + (0x00004699 << 2), 0x0000170B);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004098 << 2), 0x0000000F);
    phx_write_u32(phy_innr + (0x00004298 << 2), 0x0000000F);
    phx_write_u32(phy_innr + (0x00004498 << 2), 0x0000000F);
    phx_write_u32(phy_innr + (0x00004698 << 2), 0x0000000F);
    //------------------------------------------------------------------------------------------------
    phx_write_u32(phy_innr + (0x00004099 << 2), 0x0000180B);
    phx_write_u32(phy_innr + (0x00004299 << 2), 0x0000180B);
    phx_write_u32(phy_innr + (0x00004499 << 2), 0x0000180B);
    phx_write_u32(phy_innr + (0x00004699 << 2), 0x0000180B);
    //------------------------------------------------------------------------------------------------

    // enable port tx rx
    rab_page_write(base_addr, 0x15c + 0x40 * 0, 0x00600000);
    rab_page_write(base_addr, 0x15c + 0x40 * 1, 0x00600000);
    rab_page_write(base_addr, 0x15c + 0x40 * 2, 0x00600000);
    rab_page_write(base_addr, 0x15c + 0x40 * 3, 0x00600000);
    rab_config(base_addr, 0x9304000c); // 12.5G x4 idle3
    //rab_config(base_addr, 0x9300000c); // 12.5G x1x1x1x1 idle3
    //rab_config(base_addr, 0x9303000c); // 12.5G x2x2 idle3
    //rab_dlb_loopback(base_addr);

    // boot complete
    rab_page_write(base_addr, 0x404, 0x00000001);

#if PHY_BIST_CONFIG
    // tx rx prbs
    rab_phy_bist_config(base_addr);
#endif

#if PHY_SERIAL_LOOPBACK
    phx_write_u32(phy_innr+(0x00006050 << 2), 0xe04); // LANE_LOOPBACK_CTRL_PREG
    phx_write_u32(phy_innr+(0x00006048 << 2), 0x0); // TX_BIASTRIM_PREG
#endif

    // PHY Reset Free
    // Config reg name: phy_reset
//    if(hr3_board_type == 2)
//    {
    	phx_write_u32(phy_wrap+0x00000000, 0x00111111/*0x00011111*/);/*ldf 20230627 modify:: hc3080b*/
//    }
//    else
//    {
//    	phx_write_u32(phy_wrap+0x00000000, 0x00011111);
//    }

//	uart_printf("pma_cmn_ready[8:8]  phy_wrap 0xC = 0x%x\r\n", phx_read_u32(phy_wrap + 0xc));
//	uart_printf("tx deemphasis[0:17] phy_wrap 0x50 0x68 0x80 0x98 = 0x%x 0x%x 0x%x 0x%x\r\n",
//			phx_read_u32(phy_wrap + 0x50),
//			phx_read_u32(phy_wrap + 0x68),
//			phx_read_u32(phy_wrap + 0x80),
//			phx_read_u32(phy_wrap + 0x98));
//	rab_phy_tx_ffe_para_search_result(base_addr);
}
