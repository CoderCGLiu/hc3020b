#include "rab_dma.h"
#include <taskLib.h>
#include <semLib.h>
#include <semaphore.h>
#include "rab_dma.h"
#include "vcscpumacro.h"
#include <memLib.h>

struct rab_dma_desc *rab_wdma_malloc_desc[2] = {NULL, NULL};
struct rab_dma_desc *rab_rdma_malloc_desc[2] = {NULL, NULL};


#define DMA_DEBUG_INFO 0


#if _RIO_SEM_REWORKS_
sem_t HR_WDMA_SendAccessSem[2];
sem_t HR_RDMA_SendAccessSem[2];
sem_t HR_RAB_DMA_WSem[2][RAB_DMA_MAX_ENGINE];
sem_t HR_RAB_DMA_RSem[2][RAB_DMA_MAX_ENGINE];
#else
SEM_ID HR_WDMA_SendAccessSem[2];
SEM_ID HR_RDMA_SendAccessSem[2];
SEM_ID HR_RAB_DMA_WSem[2][RAB_DMA_MAX_ENGINE];
SEM_ID HR_RAB_DMA_RSem[2][RAB_DMA_MAX_ENGINE];
#endif /*_RIO_SEM_REWORKS_*/
u16 HR_WDMA_STATUS[2][RAB_DMA_MAX_ENGINE]; /* 0:表示空闲；1:表示使用中 */
u16 HR_RDMA_STATUS[2][RAB_DMA_MAX_ENGINE]; /* 0:表示空闲；1:表示使用中 */

#if 0
//static u64 malloc_base = 0x8000000;
u64 malloc_base=0;
void *user_malloc(u64 size)
{
	void *ret_p = (void *)malloc_base;

	malloc_base = malloc_base + size;
	malloc_base = malloc_base + 8;
	malloc_base = malloc_base & ~7UL;

	return ret_p;
}

u64 user_memalign(u64 alignment, u64 size)
{
	void *ret_p = 0;
	if(!malloc_base)
	{
		malloc_base = memalign(128,1*1024*1024);
	}
	malloc_base = malloc_base + alignment;
	malloc_base = malloc_base & ~(alignment - 1);

	ret_p = (void *)malloc_base;
	malloc_base = malloc_base + size;
	malloc_base = malloc_base + 8;
	malloc_base = malloc_base & ~7UL;
	return (u64)ret_p;
}
#endif

void rab_dma_sem_init(u8 controller)
{
    int i;

#if _RIO_SEM_REWORKS_
    sem_init2(&HR_WDMA_SendAccessSem[controller], 0, SEM_BINARY, PTHREAD_WAITQ_FIFO, 1);
#else
    HR_WDMA_SendAccessSem[controller] = semBCreate(SEM_Q_FIFO, SEM_FULL);
#endif
    if ((void *)HR_WDMA_SendAccessSem[controller] == NULL)
    {
    	uart_printf("!!!Error, rab_dma_malloc_desc:create HR_WDMA_SendAccessSem error!\n");
        return;
    }

#if _RIO_SEM_REWORKS_
    sem_init2(&HR_RDMA_SendAccessSem[controller], 0, SEM_BINARY, PTHREAD_WAITQ_FIFO, 1);
#else
    HR_RDMA_SendAccessSem[controller] = semBCreate(SEM_Q_FIFO, SEM_FULL);
#endif
    if ((void *)HR_RDMA_SendAccessSem[controller] == NULL)
    {
    	uart_printf("!!!Error, rab_dma_malloc_desc:create HR_RDMA_SendAccessSem error!\n");
        return;
    }

    for (i = 0; i < RAB_DMA_MAX_ENGINE; i++)
    {
#if _RIO_SEM_REWORKS_
    	sem_init2(&HR_RAB_DMA_WSem[controller][i], 0, SEM_BINARY, PTHREAD_WAITQ_FIFO, 0);
#else
        HR_RAB_DMA_WSem[controller][i] = semBCreate(SEM_Q_FIFO, SEM_EMPTY);
#endif
        if ((void *)HR_RAB_DMA_WSem[controller][i] == NULL)
        {
        	uart_printf("!!!Error, rab_dma_malloc_desc:create HR_RAB_DMA_WSem error!\n");
            return;
        }
#if _RIO_SEM_REWORKS_
        sem_init2(&HR_RAB_DMA_RSem[controller][i], 0, SEM_BINARY, PTHREAD_WAITQ_FIFO, 0);
#else
        HR_RAB_DMA_RSem[controller][i] = semBCreate(SEM_Q_FIFO, SEM_EMPTY);
#endif
        if ((void *)HR_RAB_DMA_RSem[controller][i] == NULL)
        {
        	uart_printf("!!!Error, rab_dma_malloc_desc:create HR_RAB_DMA_RSem error!\n");
            return;
        }

        HR_WDMA_STATUS[controller][i] = 0;
        HR_RDMA_STATUS[controller][i] = 0;
    }
}

void rab_dma_malloc_desc(u8 controller)
{
    u32 _num;
    u64 rdma_memalign_addr,wdma_memalign_addr;

    if(controller >= 2)
    {
    	uart_printf("Error!!!rab_dma_malloc_desc:: controller must be <= 1.\n");
        return;
    }

    if( (rab_rdma_malloc_desc[controller] != NULL) || (rab_wdma_malloc_desc[controller] != NULL))
    	return;
    
    _num = RAB_DMA_MAX_DESC_NUM * RAB_DMA_MAX_ENGINE;
//    printk("<DEBUG> [%s:__%d__]:: _num = 0x%x, RAB_DMA_DESC_LEN = 0x%x\n",__FUNCTION__,__LINE__,_num, RAB_DMA_DESC_LEN);//0x1008

    rdma_memalign_addr = (u64)memalign(128, _num * RAB_DMA_DESC_LEN);
//    printk("<DEBUG> [%s:__%d__]:: rdma_memalign_addr = 0x%lx\n",__FUNCTION__,__LINE__,rdma_memalign_addr);
    if(rdma_memalign_addr == 0)
    {
    	uart_printf("Error!!!rab_dma_malloc_desc::can not malloc enough r-dma describe table.\n");
        return;
    }
    else
    {
#if DMA_DEBUG_INFO
    	uart_printf("r-dma descAddr = 0x%x.\r\n", rdma_memalign_addr);
#endif
    }
    rab_rdma_malloc_desc[controller] = (struct rab_dma_desc *)hrKmToPhys((void *)rdma_memalign_addr);
    

    wdma_memalign_addr = (u64)memalign(128, _num * RAB_DMA_DESC_LEN);
//    printk("<DEBUG> [%s:__%d__]:: wdma_memalign_addr = 0x%lx\n",__FUNCTION__,__LINE__,wdma_memalign_addr);
    if(wdma_memalign_addr == 0)
    {
    	uart_printf("Error!!!rab_dma_malloc_desc::can not malloc enough w-dma describe table.\n");
        return;
    }
    else
    {
#if DMA_DEBUG_INFO
    	uart_printf("w-dma descAddr = 0x%x.\r\n", wdma_memalign_addr);
#endif
    }
    rab_wdma_malloc_desc[controller] = (struct rab_dma_desc *)hrKmToPhys((void *)wdma_memalign_addr);
    

    rab_dma_sem_init(controller);

    return;
}

/*寻找空闲DMA描述符index*/
u8 FindFreeDMAIndex(u8 controller, u8 type)
{
    int i;
    u8 _dmaIndex = 0xff;

    if (type == RAB_DMA_WDMA)
    {
#if _RIO_SEM_REWORKS_
    	sem_wait2(&HR_WDMA_SendAccessSem[controller], WAIT, NO_TIMEOUT);
#else
        semTake(HR_WDMA_SendAccessSem[controller], WAIT_FOREVER);
#endif
    }else if (type == RAB_DMA_RDMA)
    {
#if _RIO_SEM_REWORKS_
		sem_wait2(&HR_RDMA_SendAccessSem[controller], WAIT, NO_TIMEOUT);
#else
        semTake(HR_RDMA_SendAccessSem[controller], WAIT_FOREVER);
#endif
    }

    for (i = 0; i < RAB_DMA_MAX_ENGINE; i++)
    {
        if (type == RAB_DMA_WDMA)
        {
            if (HR_WDMA_STATUS[controller][i] == 0)
            {
                _dmaIndex = i;
                HR_WDMA_STATUS[controller][i] = 1;
                break;
            }
        }
        else if (type == RAB_DMA_RDMA)
        {
            if (HR_RDMA_STATUS[controller][i] == 0)
            {
                _dmaIndex = i;
                HR_RDMA_STATUS[controller][i] = 1;
                break;
            }
        }
    }

    if (type == RAB_DMA_WDMA)
    {
#if _RIO_SEM_REWORKS_
    	sem_post(&HR_WDMA_SendAccessSem[controller]);
#else
        semGive(HR_WDMA_SendAccessSem[controller]);
#endif
    }else if (type == RAB_DMA_RDMA)
    {
#if _RIO_SEM_REWORKS_
    	sem_post(&HR_RDMA_SendAccessSem[controller]);
#else
        semGive(HR_RDMA_SendAccessSem[controller]);
#endif
    }

    return _dmaIndex;
}

void rab_dma_desc_pack(u32 desc_buf[4], struct rab_dma_desc *desc)
{
	desc_buf[0] = desc->valid
		| (desc->next_desc_pointer_valid << 1)
		| (desc->desc_axi_ext_addr_msb << 2)
		| (desc->trans_len << 5)
		| (desc->axi_ext_addr_msb << 23)
		| (desc->axi_ext_addr << 29);
	desc_buf[1] = desc->src_word_addr;
	desc_buf[2] = desc->dst_word_addr;
	desc_buf[3] = desc->next_desc_word_addr
		| (desc->desc_axi_ext_addr << 30);
}

void rab_dma_desc_unpack(u32 desc_buf[4], struct rab_dma_desc *desc)
{
	u32 data = desc_buf[0];

	desc->valid = data & 0x1;
	desc->next_desc_pointer_valid = (data >> 1) & 0x1;
	desc->desc_axi_ext_addr_msb = (data >> 2) & 0x7;
	desc->trans_len = (data >> 5) & 0x3ffff;
	desc->axi_ext_addr_msb = (data >> 23) & 0x1;
	desc->done = (data >> 24 ) & 0x1;
	desc->axi_error = (data >> 25) & 0x1;
	desc->rio_error = (data >> 26) & 0x1;
	desc->desc_fetch_error = (data >> 27) & 0x1;
	desc->desc_update_error = (data >> 28) & 0x1;
	desc->axi_ext_addr = (data >> 29) & 0x3;

	data = desc_buf[1];
	desc->src_word_addr = data;

	data = desc_buf[2];
	desc->dst_word_addr = data;

	data = desc_buf[3];
	desc->next_desc_word_addr = data & 0x3fffffff;
	desc->desc_axi_ext_addr = (data >> 30) & 0x3;
}

void create_one_dma_desc(struct rab_dma_desc *p_desc,
			 u32 trans_len,
			 u64 source_addr,
			 u64 dest_addr,
			 u8 next_desc_pointer_valid,
			 u64 next_desc_addr, u8 mode)
{
	struct rab_dma_desc desc = {0};

	desc.valid = 1;
	desc.next_desc_pointer_valid = next_desc_pointer_valid;
	desc.trans_len = trans_len / 4;

	desc.src_word_addr = source_addr >> 2;
	desc.dst_word_addr = dest_addr >> 2;
	if (mode == RAB_DMA_WDMA) { /*WDMA*/
		desc.axi_ext_addr = (source_addr >> 34) & 0x7;
		desc.axi_ext_addr_msb = (source_addr >> 37) & 1;
	} else if (mode == RAB_DMA_RDMA) { /*RDMA*/
		desc.axi_ext_addr = (dest_addr >> 34) & 0x7;
		desc.axi_ext_addr_msb = (dest_addr >> 37) & 1;
	}

	desc.next_desc_word_addr = next_desc_addr >> 3;
	desc.desc_axi_ext_addr = (next_desc_addr >> 33) & 0x3;
	desc.desc_axi_ext_addr_msb = (next_desc_addr >> 35) & 0x7;

	*p_desc = desc;
}

void _set_rab_dma_ctrl(u8 controller,
		u32 reg,
		u8 local_desc_avail,
		u8 priority, u8 crf,
		u8 db_enable,
		u16 dest_id)
{
	u32 ctrl;

	ctrl = ((local_desc_avail & 0x1) << 2)
		| ((priority & 0x3) << 3)
		| ((crf & 0x1) << 5)
		| ((db_enable & 0x1) << 6)
		| ((dest_id & 0xffff) << 16);

	rab_page_write(controller, reg, ctrl);
}

void set_rab_wdma_ctrl(u8 controller,
		u8 index,
		u8 local_desc_avail,
		u8 priority, u8 crf,
		u8 db_enable,
		u16 dest_id)
{
	_set_rab_dma_ctrl(controller, RAB_WDMA_CTRL(index),
		local_desc_avail, priority, crf,
		db_enable, dest_id);
}

void set_rab_dma_iaddr_desc_sel(u8 controller, u8 rw, u8 engine, u8 desc_num)
{
	u32 sel;

	sel = (rw & 0x1)
		| ((engine & 0x7) << 1)
		| ((desc_num & 0xf) << 4);

	rab_page_write(controller, RAB_DMA_IADDR_DESC_SEL, sel);
}

void set_rab_dma_iaddr_desc_ctrl(u8 controller, u32 ctrl)
{
	rab_page_write(controller, RAB_DMA_IADDR_DESC_CTRL, ctrl);
}

void set_rab_dma_iaddr_desc_src_addr(u8 controller, u32 src_addr)
{
	rab_page_write(controller, RAB_DMA_IADDR_DESC_SRC_ADDR, src_addr);
}

void set_rab_dma_iaddr_desc_dest_addr(u8 controller, u32 dest_addr)
{
	rab_page_write(controller, RAB_DMA_IADDR_DESC_DEST_ADDR, dest_addr);
}

void set_rab_dma_iaddr_desc_next_addr(u8 controller, u32 next_addr)
{
	rab_page_write(controller, RAB_DMA_IADDR_DESC_NEXT_ADDR, next_addr);
}

void _start_rab_dma(u8 controller, u32 reg)
{
	u32 ctrl;

	ctrl = rab_page_read(controller, reg);
	rab_page_write(controller, reg, ctrl | 1);
}

void start_rab_wdma(u8 controller, u8 index)
{
	_start_rab_dma(controller, RAB_WDMA_CTRL(index));
}

void rab_wdma_port_set(u8 controller, u8 index, u32 port)
{
	u32 ctrl;
	u32 reg = RAB_WDMA_CTRL(index);

	ctrl = rab_page_read(controller, reg);
	rab_page_write(controller, reg, ctrl | port << 8);
}

u32 get_rab_dma_iaddr_desc_ctrl(u8 controller)
{
	return rab_page_read(controller, RAB_DMA_IADDR_DESC_CTRL);
}

void register_rab_dma_wdma(u8 controller,
			u8 dma_index,
			u64 mem_addr, u32 rio_addr,
			int len, u16 dst_id)
{
	u8 local_desc_avail = 1;
	u8 priority = 0;
	u8 crf = 0;
	u8 db_enable = 1;
	struct rab_dma_desc desc;
	u32 buf[4];

	set_rab_wdma_ctrl(controller, dma_index,
		local_desc_avail, priority, crf,
		db_enable, dst_id);

	create_one_dma_desc(&desc, len, mem_addr, rio_addr, 0, 0, RAB_DMA_WDMA);
	rab_dma_desc_pack(buf, &desc);

	uart_printf("reg wdma desc %x %x %x %x\n", buf[0], buf[1], buf[2], buf[3]);

	set_rab_dma_iaddr_desc_sel(controller, RAB_DMA_WDMA, dma_index, 0);
	set_rab_dma_iaddr_desc_ctrl(controller, buf[0]);
	set_rab_dma_iaddr_desc_src_addr(controller, buf[1]);
	set_rab_dma_iaddr_desc_dest_addr(controller, buf[2]);
	set_rab_dma_iaddr_desc_next_addr(controller, buf[3]);

	start_rab_wdma(controller, dma_index);

	do {
		buf[0] = get_rab_dma_iaddr_desc_ctrl(controller);
		rab_dma_desc_unpack(buf, &desc);
	} while (desc.done == 0);

	uart_printf("DMA done!\n");
}

void set_rab_rdma_ctrl(u8 controller, u8 index, u8 local_desc_avail,
		u8 priority, u8 crf, u8 db_enable, u16 dest_id)
{
	_set_rab_dma_ctrl(controller, RAB_RDMA_CTRL(index),
		local_desc_avail, priority, crf,
		db_enable, dest_id);
}

void start_rab_rdma(u8 controller, u8 index)
{
	_start_rab_dma(controller, RAB_RDMA_CTRL(index));
}

void rab_rdma_port_set(u8 controller, u8 index, u32 port)
{
	u32 ctrl;
	u32 reg = RAB_RDMA_CTRL(index);

	ctrl = rab_page_read(controller, reg);
	rab_page_write(controller, reg, ctrl | port << 8);
}

void register_rab_dma_rdma(u8 controller,
		u8 dma_index, u64 src_addr,
		u64 dst_addr, int len, u16 dst_id)
{
	u8 local_desc_avail = 1;
	u8 priority = 0;
	u8 crf = 0;
	u8 db_enable = 1;
	struct rab_dma_desc desc;
	u32 buf[4];

	set_rab_rdma_ctrl(controller, dma_index,
		local_desc_avail, priority, crf,
		db_enable, dst_id);

	create_one_dma_desc(&desc, len, src_addr, dst_addr, 0, 0, RAB_DMA_RDMA);
	rab_dma_desc_pack(buf, &desc);

	uart_printf("reg rdma desc %x %x %x %x  \n", buf[0], buf[1], buf[2], buf[3]);

	set_rab_dma_iaddr_desc_sel(controller, RAB_DMA_RDMA, dma_index, 0);
	set_rab_dma_iaddr_desc_ctrl(controller, buf[0]);
	set_rab_dma_iaddr_desc_src_addr(controller, buf[1]);
	set_rab_dma_iaddr_desc_dest_addr(controller, buf[2]);
	set_rab_dma_iaddr_desc_next_addr(controller, buf[3]);

	start_rab_rdma(controller, dma_index);

	do {
		buf[0] = get_rab_dma_iaddr_desc_ctrl(controller);
		rab_dma_desc_unpack(buf, &desc);
	} while (desc.done == 0);

	uart_printf("RDMA done! \n");
}

void _set_rab_dma_addr(u8 controller, u32 reg, u64 first_desc_addr)
{
	rab_page_write(controller, reg, (first_desc_addr >> 2) & 0xfffffffe);
}

void set_rab_wdma_addr(u8 controller, u8 index, u64 first_desc_addr)
{
	_set_rab_dma_addr(controller, RAB_WDMA_ADDR(index), first_desc_addr);
}

void set_rab_wdma_addr_ext(u8 controller, u8 index, u64 first_desc_addr)
{
	rab_page_write(controller, RAB_WDMA_ADDR_EXT(index), (first_desc_addr >> 34) & 0xf);
}

void set_rab_rdma_addr(u8 controller, u8 index, u64 first_desc_addr)
{
	_set_rab_dma_addr(controller, RAB_RDMA_ADDR(index), first_desc_addr);
}

 void set_rab_rdma_addr_ext(u8 controller, u8 index, u64 first_desc_addr)
{
	 rab_page_write(controller, RAB_RDMA_ADDR_EXT(index), (first_desc_addr >> 34) & 0xf);
}

u32 get_rab_wdma_stat(u8 controller, u8 index)
{
	return rab_page_read(controller, RAB_WDMA_STAT(index));
}

/*向DMA描述符写入数据*/
void rab_desc_wdma(u8 controller, u8 dmaIndex,
		u64 memAddr, u32 rioAddr,
		u32 transferLen, u16 destID)
{
	u8 _localDescAvail = 0;
	u8 _priority = 0;
	u8 _crf = 0;
	u8 _dbEnable = 0;
	struct rab_dma_desc desc;
	u32 _dmaTransCount, _lastTransferLen;
	u64 _descAddr;
	u32 _tmpBuf[4];
	int i;
	u32 src0 = 0, src1 = 0, src2 = 0, src3 = 0;

	_descAddr = (u64)((char*)rab_wdma_malloc_desc[controller] +
			dmaIndex * RAB_DMA_MAX_DESC_NUM * RAB_DMA_DESC_LEN);

	_dmaTransCount = transferLen / RAB_DMA_MAX_TRANSFER_LEN;
	if (transferLen % RAB_DMA_MAX_TRANSFER_LEN != 0)
		_dmaTransCount++;

	set_rab_wdma_ctrl(controller, dmaIndex, _localDescAvail,
			_priority, _crf, _dbEnable, destID);
	set_rab_wdma_addr(controller, dmaIndex, _descAddr);
	set_rab_wdma_addr_ext(controller, dmaIndex, _descAddr);

	for (i = 0; i < _dmaTransCount - 1; i++)
	{
		/*创建一个DMA描述符*/
		create_one_dma_desc(&desc, RAB_DMA_MAX_TRANSFER_LEN, memAddr,
			rioAddr, 1, _descAddr + RAB_DMA_DESC_LEN, RAB_DMA_WDMA);
		/*将数据封包*/
		rab_dma_desc_pack(_tmpBuf, &desc);
#if CACHE_EN
		phx_write_cache_u32(_descAddr, _tmpBuf[0]);
		phx_write_cache_u32(_descAddr + 4, _tmpBuf[1]);
		phx_write_cache_u32(_descAddr + 8, _tmpBuf[2]);
		phx_write_cache_u32(_descAddr + 12, _tmpBuf[3]);
//		asm volatile("sync");
#else
		phx_write_u32(_descAddr, _tmpBuf[0]);
		phx_write_u32(_descAddr + 4, _tmpBuf[1]);
		phx_write_u32(_descAddr + 8, _tmpBuf[2]);
		phx_write_u32(_descAddr + 12, _tmpBuf[3]);
//		asm volatile("sync");
#endif
#if DMA_DEBUG_INFO
	#if CACHE_EN
		src0 = phx_read_cache_u32(_descAddr);
		src1 = phx_read_cache_u32(_descAddr + 4);
		src2 = phx_read_cache_u32(_descAddr + 8);
		src3 = phx_read_cache_u32(_descAddr + 12);
	#else
		src0 = phx_read_u32(_descAddr);
		src1 = phx_read_u32(_descAddr + 4);
		src2 = phx_read_u32(_descAddr + 8);
		src3 = phx_read_u32(_descAddr + 12);
	#endif
		uart_printf("wdma%d engine%d desc%d addr 0x%x :[0]0x%x [1]0x%x [2]0x%x [3]0x%x\r\n",
				controller, dmaIndex, i, _descAddr, src0, src1 , src2, src3);
#endif

		memAddr += RAB_DMA_MAX_TRANSFER_LEN;
		rioAddr += RAB_DMA_MAX_TRANSFER_LEN;
		_descAddr += RAB_DMA_DESC_LEN;
	}
	_lastTransferLen = transferLen - (_dmaTransCount - 1) * RAB_DMA_MAX_TRANSFER_LEN;
	create_one_dma_desc(&desc, _lastTransferLen, memAddr, rioAddr, 1, 0, RAB_DMA_WDMA);
	rab_dma_desc_pack(_tmpBuf, &desc);
#if CACHE_EN
	phx_write_cache_u32(_descAddr, _tmpBuf[0]);
	phx_write_cache_u32(_descAddr + 4, _tmpBuf[1]);
	phx_write_cache_u32(_descAddr + 8, _tmpBuf[2]);
	phx_write_cache_u32(_descAddr + 12, _tmpBuf[3]);
//	asm volatile("sync");
#else
	phx_write_u32(_descAddr, _tmpBuf[0]);
	phx_write_u32(_descAddr + 4, _tmpBuf[1]);
	phx_write_u32(_descAddr + 8, _tmpBuf[2]);
	phx_write_u32(_descAddr + 12, _tmpBuf[3]);
//	asm volatile("sync");
#endif

#if DMA_DEBUG_INFO
#if CACHE_EN
	src0 = phx_read_cache_u32(_descAddr);
	src1 = phx_read_cache_u32(_descAddr + 4);
	src2 = phx_read_cache_u32(_descAddr + 8);
	src3 = phx_read_cache_u32(_descAddr + 12);
#else
	src0 = phx_read_u32(_descAddr);
	src1 = phx_read_u32(_descAddr + 4);
	src2 = phx_read_u32(_descAddr + 8);
	src3 = phx_read_u32(_descAddr + 12);
#endif
	uart_printf("wdma%d engine%d desc%d addr 0x%x :[0]0x%x [1]0x%x [2]0x%x [3]0x%x\r\n",
			controller, dmaIndex, i,  _descAddr, src0, src1, src2, src3);
#endif

	/*TODO: ldf 20230526 开始DMA传输后,为什么一直不断有中断产生？？*/
	start_rab_wdma(controller, dmaIndex);
}

void rab_desc_rdma(u8 controller, u8 dmaIndex,
	u32 rioAddr, u64 memAddr, u32 transferLen, u16 destID)
{
	u8 _localDescAvail = 0;
	u8 _priority = 0;
	u8 _crf = 0;
	u8 _dbEnable = 0;
	struct rab_dma_desc desc;
	u32 _dmaTransCount, _lastTransferLen;
	u64 _descAddr;
	u32 _tmpBuf[4];
	int i;
	u32 src0 = 0, src1 = 0, src2 = 0, src3 = 0;
#if 0
	if (controller >= 2 || dmaIndex >= 4) {
		uart_printf("!!!Error, rab_desc_wdma::controller or dmaIndex param error.\n");
		return;
	}

	if (transferLen < 4) {
		uart_printf("!!!Error, DMA transfer length must be >=1 word(4 bytes).\n");
		return;
	}

	if (transferLen > RAB_DMA_MAX_TRANSLEN) {
		uart_printf("!!!Error, DMA transfer length must be <= 256M bytes.\n");
		return;
	}
#endif

	_descAddr = (u64)((char*)rab_rdma_malloc_desc[controller] +
			dmaIndex * RAB_DMA_MAX_DESC_NUM * RAB_DMA_DESC_LEN);

	_dmaTransCount = transferLen / RAB_DMA_MAX_TRANSFER_LEN;
	if (transferLen % RAB_DMA_MAX_TRANSFER_LEN != 0)
		_dmaTransCount++;

	set_rab_rdma_ctrl(controller, dmaIndex, _localDescAvail,
			_priority, _crf, _dbEnable, destID);
	set_rab_rdma_addr(controller, dmaIndex, _descAddr);
	set_rab_rdma_addr_ext(controller, dmaIndex, _descAddr);

	for (i = 0; i < _dmaTransCount - 1; i++)
	{
		create_one_dma_desc(&desc, RAB_DMA_MAX_TRANSFER_LEN, rioAddr,
			memAddr, 1, _descAddr + RAB_DMA_DESC_LEN, RAB_DMA_RDMA);

		rab_dma_desc_pack(_tmpBuf, &desc);

#if CACHE_EN
		phx_write_cache_u32(_descAddr, _tmpBuf[0]);
		phx_write_cache_u32(_descAddr + 4, _tmpBuf[1]);
		phx_write_cache_u32(_descAddr + 8, _tmpBuf[2]);
		phx_write_cache_u32(_descAddr + 12, _tmpBuf[3]);
//		asm volatile("sync");
#else
		phx_write_u32(_descAddr, _tmpBuf[0]);
		phx_write_u32(_descAddr + 4, _tmpBuf[1]);
		phx_write_u32(_descAddr + 8, _tmpBuf[2]);
		phx_write_u32(_descAddr + 12, _tmpBuf[3]);
		//asm volatile("sync");
#endif

#if DMA_DEBUG_INFO
#if CACHE_EN
		src0 = phx_read_cache_u32(_descAddr);
		src1 = phx_read_cache_u32(_descAddr+4);
		src2 = phx_read_cache_u32(_descAddr+8);
		src3 = phx_read_cache_u32(_descAddr+12);
#else
		src0 = phx_read_u32(_descAddr);
		src1 = phx_read_u32(_descAddr+4);
		src2 = phx_read_u32(_descAddr+8);
		src3 = phx_read_u32(_descAddr+12);
#endif
		uart_printf("rdma%d engine%d desc%d addr 0x%x :[0]0x%x [1]0x%x [2]0x%x [3]0x%x\r\n",
				controller, dmaIndex, i, _descAddr, src0, src1, src2, src3);
#endif
		memAddr += RAB_DMA_MAX_TRANSFER_LEN;
		rioAddr += RAB_DMA_MAX_TRANSFER_LEN;
		_descAddr += RAB_DMA_DESC_LEN;
	}
	_lastTransferLen = transferLen - (_dmaTransCount - 1) * RAB_DMA_MAX_TRANSFER_LEN;
	create_one_dma_desc(&desc, _lastTransferLen, rioAddr, memAddr, 1, 0, RAB_DMA_RDMA);
	rab_dma_desc_pack(_tmpBuf, &desc);

#if CACHE_EN
	phx_write_cache_u32(_descAddr, _tmpBuf[0]);
	phx_write_cache_u32(_descAddr + 4, _tmpBuf[1]);
	phx_write_cache_u32(_descAddr + 8, _tmpBuf[2]);
	phx_write_cache_u32(_descAddr + 12, _tmpBuf[3]);
//	asm volatile("sync");
#else
	phx_write_u32(_descAddr, _tmpBuf[0]);
	phx_write_u32(_descAddr + 4, _tmpBuf[1]);
	phx_write_u32(_descAddr + 8, _tmpBuf[2]);
	phx_write_u32(_descAddr + 12, _tmpBuf[3]);
	//asm volatile("sync");
#endif

#if DMA_DEBUG_INFO
#if CACHE_EN
	src0 = phx_read_cache_u32(_descAddr);
	src1 = phx_read_cache_u32(_descAddr+4);
	src2 = phx_read_cache_u32(_descAddr+8);
	src3 = phx_read_cache_u32(_descAddr+12);
#else
	src0 = phx_read_u32(_descAddr);
	src1 = phx_read_u32(_descAddr+4);
	src2 = phx_read_u32(_descAddr+8);
	src3 = phx_read_u32(_descAddr+12);
#endif
	uart_printf("rdma%d engine%d desc%d addr 0x%x :[0]0x%x [1]0x%x [2]0x%x [3]0x%x\r\n",
			controller, dmaIndex, i, _descAddr, src0, src1, src2, src3);
#endif

	start_rab_rdma(controller, dmaIndex);
}

u32 get_rab_rdma_stat(u8 controller, u8 index)
{
	return rab_page_read(controller, RAB_RDMA_STAT(index));
}

u32 get_rab_dma_iaddr_desc_src_addr(u8 controller)
{
	return rab_page_read(controller, RAB_DMA_IADDR_DESC_SRC_ADDR);
}

u32 get_rab_dma_iaddr_desc_dest_addr(u8 controller)
{
	return rab_page_read(controller, RAB_DMA_IADDR_DESC_DEST_ADDR);
}

u32 get_rab_dma_iaddr_desc_next_addr(u8 controller)
{
	return rab_page_read(controller, RAB_DMA_IADDR_DESC_NEXT_ADDR);
}

void set_rab_intr_enab_wdma(u8 controller,
			u8 chain_desc_trans_completed,
			u8 desc_trans_completed,
			u8 trans_aborted)
{
	u32 ctrl;

	ctrl = chain_desc_trans_completed
		| (desc_trans_completed << 8)
		| (trans_aborted << 16);

	rab_page_write(controller, RAB_INTR_ENAB_WDMA, ctrl);
}

void set_rab_intr_enab_rdma(u8 controller,
			u8 chain_desc_trans_completed,
			u8 desc_trans_completed,
			u8 trans_aborted)
{
	u32 ctrl;

	ctrl = chain_desc_trans_completed
		| (desc_trans_completed << 8)
		| (trans_aborted << 16);

	rab_page_write(controller, RAB_INTR_ENAB_RDMA, ctrl);
}


#define RIO0_MEM_ADDR 0x90000000
#define RIO1_MEM_ADDR 0xa0000000

void rab_dma_test(u16 rio0ID, u16 rio1ID)
{
	u32 _memLen;
	u32 error_cnt = 0;
	u64 _pDmaStressBuf = NULL;
	int i, j;
	u32 _rioAddr0 = 0x40000000;
	u32 _rioAddr1 = 0x60000000;
	u32 src0 = 0;

	uart_printf("rab dma test!\r\n");

	_memLen = 0x100000;
	_pDmaStressBuf = 0xb0000000;

	rab_dma_malloc_desc(0);
	rab_dma_malloc_desc(1);

	for (i = 0; i < _memLen; i += 4)
#if CACHE_EN
		phx_write_cache_u32(_pDmaStressBuf + i, 0x12345678 + i);
#else
		phx_write_u32(_pDmaStressBuf + i, 0x12345678 + i);
#endif

	for (i = 0; i < 8; i++) {
		rab_desc_wdma(1, i, _pDmaStressBuf, _rioAddr0 + i * _memLen, _memLen, rio0ID);
		start_rab_wdma(1, i);
		rab_desc_wdma(0, i, _pDmaStressBuf, _rioAddr1 + i * _memLen, _memLen, rio1ID);
		start_rab_wdma(0, i);
		udelay(20000);

		for (j = 0; j < _memLen; j += 4) {
#if CACHE_EN
			src0 = phx_read_cache_u32(RIO0_MEM_ADDR + i * _memLen + j);
#else
			src0 = phx_read_u32(RIO0_MEM_ADDR + i * _memLen + j);
#endif
			if (src0 != (0x12345678 + j)) {
				error_cnt++;
				uart_printf("rab1 wdma error:i = %d, j = 0x%x, value = 0x%x, expect = 0x%x\r\n", i, j, src0, 0x12345678 + j);
			}
		}
		uart_printf("rab1 engine%d wdma completed! error_cnt = 0x%x\r\n", i, error_cnt);
		for (j = 0; j < _memLen; j += 4) {
#if CACHE_EN
			src0 = phx_read_cache_u32(RIO1_MEM_ADDR + i * _memLen + j);
#else
			src0 = phx_read_u32(RIO1_MEM_ADDR + i * _memLen + j);
#endif
			if (src0 != (0x12345678 + j)) {
				error_cnt++;
				uart_printf("rab0 wdma error:i = %d, j = 0x%x, value = 0x%x, expect = 0x%x\r\n", i, j, src0, 0x12345678 + j);
			}
		}
		uart_printf("rab0 engine%d wdma completed! error_cnt = 0x%x\r\n", i, error_cnt);

		rab_desc_rdma(1, i, _rioAddr0 + i * _memLen, _pDmaStressBuf + 0x10000000 + i * _memLen, _memLen, rio0ID);
		start_rab_rdma(1, i);
		rab_desc_rdma(0, i, _rioAddr1 + i * _memLen, _pDmaStressBuf + 0x20000000 + i * _memLen, _memLen, rio1ID);
		start_rab_rdma(0, i);
		udelay(20000);

		for (j = 0; j < _memLen; j += 4) {
#if CACHE_EN
			src0 = phx_read_cache_u32(_pDmaStressBuf + 0x10000000 + i * _memLen + j);
#else
			src0 = phx_read_u32(_pDmaStressBuf + 0x10000000 + i * _memLen + j);
#endif
			if (src0 != (0x12345678 + j)) {
				error_cnt++;
				uart_printf("rab1 rdma error:i = %d, j = 0x%x, value = 0x%x, expect = 0x%x\r\n", i, j, src0, 0x12345678 + j);
			}
		}
		uart_printf("rab1 engine%d rdma completed! error_cnt = 0x%x\r\n", i, error_cnt);
		for (j = 0; j < _memLen; j += 4) {
#if CACHE_EN
			src0 = phx_read_cache_u32(_pDmaStressBuf + 0x20000000 + i * _memLen + j);
#else
			src0 = phx_read_u32(_pDmaStressBuf + 0x20000000 + i * _memLen + j);
#endif
			if (src0 != (0x12345678 + j)) {
				error_cnt++;
				uart_printf("rab0 rdma error:i = %d, j = 0x%x, value = 0x%x, expect = 0x%x\r\n", i, j, src0, 0x12345678 + j);
			}
		}
		uart_printf("rab0 engine%d rdma completed! error_cnt = 0x%x\r\n", i, error_cnt);
	}
	if (error_cnt)
		uart_printf("========================= desc dma err! error_cnt = 0x%x\r\n", error_cnt);
	else
		uart_printf("========================= desc dma pass!\r\n");
}

void rab_dma_loopback_test(u16 rio0ID, u16 rio1ID)
{
	u32 _memLen;
	u32 error_cnt = 0;
	u64 _pDmaStressBuf = NULL;
	int i, j;
	u32 _rioAddr0 = 0x40000000;
	u32 _rioAddr1 = 0x60000000;
	u32 src0 = 0;

	uart_printf("rab dma loopback test!\r\n");

	_memLen = 0x100000;
	_pDmaStressBuf = 0x1080000000;

	rab_dma_malloc_desc(0);
	rab_dma_malloc_desc(1);

	for (i = 0; i < _memLen; i += 4)
		*((u32 *)(_pDmaStressBuf + i)) = 0x12345678 + i;
//#if CACHE_EN
//		phx_write_cache_u32(_pDmaStressBuf + i, 0x12345678 + i);
//#else
//		phx_write_u32(_pDmaStressBuf + i, 0x12345678 + i);
//#endif

	for (i = 0; i < 8; i++) {
		rab_desc_wdma(0, i, (u64)hrKmToPhys((void *)_pDmaStressBuf), _rioAddr0 + i * _memLen, _memLen, rio0ID);
		start_rab_wdma(0, i);
		rab_desc_wdma(1, i, (u64)hrKmToPhys((void *)_pDmaStressBuf), _rioAddr1 + i * _memLen, _memLen, rio1ID);
		start_rab_wdma(1, i);
		udelay(20000);

		for (j = 0; j < _memLen; j += 4) {
#if CACHE_EN
			src0 = phx_read_cache_u32(_pDmaStressBuf+0x10000000 + i * _memLen + j);
#else
			src0 = phx_read_u32(_pDmaStressBuf+0x10000000 + i * _memLen + j);
#endif
			if (src0 != (0x12345678 + j)) {
				error_cnt++;
				uart_printf("rab0 wdma error:i = %d, j = 0x%x, value = 0x%x, expect = 0x%x\r\n", i, j, src0, 0x12345678 + j);
			}
		}
		uart_printf("rab0 engine%d wdma completed! error_cnt = 0x%x\r\n", i, error_cnt);
		for (j = 0; j < _memLen; j += 4) {
#if CACHE_EN
			src0 = phx_read_cache_u32(_pDmaStressBuf+2*0x10000000 + i * _memLen + j);
#else
			src0 = phx_read_u32(_pDmaStressBuf+2*0x10000000 + i * _memLen + j);
#endif
			if (src0 != (0x12345678 + j)) {
				error_cnt++;
				uart_printf("rab1 wdma error:i = %d, j = 0x%x, value = 0x%x, expect = 0x%x\r\n", i, j, src0, 0x12345678 + j);
			}
		}
		uart_printf("rab1 engine%d wdma completed! error_cnt = 0x%x\r\n", i, error_cnt);

		rab_desc_rdma(0, i, _rioAddr0 + i * _memLen, _pDmaStressBuf+3*0x10000000 + i * _memLen, _memLen, rio0ID);
		start_rab_rdma(0, i);
		rab_desc_rdma(1, i, _rioAddr1 + i * _memLen, _pDmaStressBuf+4*0x10000000 + i * _memLen, _memLen, rio1ID);
		start_rab_rdma(1, i);
		udelay(20000);

		for (j = 0; j < _memLen; j += 4) {
#if CACHE_EN
			src0 = phx_read_cache_u32(_pDmaStressBuf+3*0x10000000 + i * _memLen + j);
#else
			src0 = phx_read_u32(_pDmaStressBuf+3*0x10000000 + i * _memLen + j);
#endif
			if (src0 != (0x12345678 + j)) {
				error_cnt++;
				uart_printf("rab0 rdma error:i = %d, j = 0x%x, value = 0x%x, expect = 0x%x\r\n", i, j, src0, 0x12345678 + j);
			}
		}
		uart_printf("rab0 engine%d rdma completed! error_cnt = 0x%x\r\n", i, error_cnt);
		for (j = 0; j < _memLen; j += 4) {
#if CACHE_EN
			src0 = phx_read_cache_u32(_pDmaStressBuf+4*0x10000000 + i * _memLen + j);
#else
			src0 = phx_read_u32(_pDmaStressBuf+4*0x10000000 + i * _memLen + j);
#endif
			if (src0 != (0x12345678 + j)) {
				error_cnt++;
				uart_printf("rab1 rdma error:i = %d, j = 0x%x, value = 0x%x, expect = 0x%x\r\n", i, j, src0, 0x12345678 + j);
			}
		}
		uart_printf("rab1 engine%d rdma completed! error_cnt = 0x%x\r\n", i, error_cnt);
	}
	if (error_cnt)
		uart_printf("========================= desc dma err! error_cnt = 0x%x\r\n", error_cnt);
	else
		uart_printf("========================= desc dma pass!\r\n");
}

//#include "common.h"
// HC3080 1.5G freq
#define CHIP_FREQ  ((u64)(1500000000))
u32 g_speed_test = 0;
u32 g_start_times;
u32 g_end_times;
u32 g_delta_times;
void rab_dma_speed_test(u16 rio0ID, u16 rio1ID)
{
	u32 _memLen;
	u32 error_cnt = 0;
	u64 _pDmaStressBuf = NULL;
	u64 transfer_speed;
	int i, j;
	u32 _rioAddr0 = 0x40000000;
	u32 _rioAddr1 = 0x60000000;
	u32 src0 = 0;

	uart_printf("rab dma speed test!\r\n");

	_memLen = 0x10000000;
	_pDmaStressBuf = 0xb0000000;
	g_speed_test = 1;

	rab_dma_malloc_desc(0);
	rab_dma_malloc_desc(1);

	write_c0_count(0); // init time count
	rab_desc_wdma(0, 0, _pDmaStressBuf, _rioAddr1, _memLen, rio1ID);
	g_start_times = read_c0_count();
	start_rab_wdma(0, 0);
	udelay(100000);
	g_delta_times = g_end_times - g_start_times;
	transfer_speed = ((CHIP_FREQ * (u64)_memLen / (u64)g_delta_times) >> 20); // MB/s
	uart_printf("rab0 wdma v = %d(MB/s), transfer_len:0x%x(bytes), start_times:0x%x, end_times:0x%x, delta_times:0x%x [for CHIP_FREQ = %d]\r\n",
			transfer_speed, _memLen, g_start_times, g_end_times, g_delta_times, CHIP_FREQ);

	write_c0_count(0); // init time count
	rab_desc_wdma(1, 0, _pDmaStressBuf, _rioAddr0, _memLen, rio0ID);
	g_start_times = read_c0_count();
	start_rab_wdma(1, 0);
	udelay(100000);
	g_delta_times = g_end_times - g_start_times;
	transfer_speed = ((CHIP_FREQ * (u64)_memLen / (u64)g_delta_times) >> 20); // MB/s
	uart_printf("rab1 wdma v = %d(MB/s), transfer_len:0x%x(bytes), start_times:0x%x, end_times:0x%x, delta_times:0x%x [for CHIP_FREQ = %d]\r\n",
			transfer_speed, _memLen, g_start_times, g_end_times, g_delta_times, CHIP_FREQ);

	write_c0_count(0); // init time count
	rab_desc_rdma(0, 0, _rioAddr1, _pDmaStressBuf + 0x20000000, _memLen, rio1ID);
	g_start_times = read_c0_count();
	start_rab_rdma(0, 0);
	udelay(100000);
	g_delta_times = g_end_times - g_start_times;
	transfer_speed = ((CHIP_FREQ * (u64)_memLen / (u64)g_delta_times) >> 20); // MB/s
	uart_printf("rab0 rdma v = %d(MB/s), transfer_len:0x%x(bytes), start_times:0x%x, end_times:0x%x, delta_times:0x%x [for CHIP_FREQ = %d]\r\n",
			transfer_speed, _memLen, g_start_times, g_end_times, g_delta_times, CHIP_FREQ);

	write_c0_count(0); // init time count
	rab_desc_rdma(1, 0, _rioAddr0, _pDmaStressBuf + 0x10000000, _memLen, rio0ID);
	g_start_times = read_c0_count();
	start_rab_rdma(1, 0);
	udelay(100000);
	g_delta_times = g_end_times - g_start_times;
	transfer_speed = ((CHIP_FREQ * (u64)_memLen / (u64)g_delta_times) >> 20); // MB/s
	uart_printf("rab1 rdma v = %d(MB/s), transfer_len:0x%x(bytes), start_times:0x%x, end_times:0x%x, delta_times:0x%x [for CHIP_FREQ = %d]\r\n",
			transfer_speed, _memLen, g_start_times, g_end_times, g_delta_times, CHIP_FREQ);

	uart_printf("rab desc dma speed end!\r\n");
}

#define RIO_MEM_ADDR 0x90000000
void rab_dma_p2p_test(u8 controller, u16 sender_ID, u16 receiver_ID)
{
	u32 _memLen;
	u32 error_cnt = 0;
	u64 _pDmaStressBuf;
	u64 transfer_speed;
	int i;
	u32 _rioAddr = 0x40000000;
	u32 dst_val = 0;

	_memLen = 0x10000000;
	_pDmaStressBuf = 0xb0000000;

#if RAB_SENDER_ENDIAN

	uart_printf("p2p dma sender!\r\n");
	rab_dma_malloc_desc(0);
	rab_dma_malloc_desc(1);

	for (i = 0; i < _memLen; i += 4)
#if CACHE_EN
		phx_write_cache_u32(_pDmaStressBuf + i, 0x12345678 + i);
#else
		phx_write_u32(_pDmaStressBuf + i, 0x12345678 + i);
#endif
	asm volatile("sync");

	write_c0_count(0); // init time count
	rab_desc_wdma(controller, 0, _pDmaStressBuf, _rioAddr, _memLen, receiver_ID);
	g_start_times = read_c0_count();
	start_rab_wdma(controller, 0);
	udelay(1000000); // wait for interrupt print
	if (g_speed_test) {
		g_delta_times = g_end_times - g_start_times;
		transfer_speed = ((CHIP_FREQ * (u64)_memLen / (u64)g_delta_times) >> 20); // MB/s
		uart_printf("rab%d wdma v = %d(MB/s), data_len:%d(bytes), start_times:0x%x, end_times:0x%x, delta_times:0x%x [for CHIP_FREQ = %d]\r\n",
			controller, transfer_speed, _memLen, g_start_times, g_end_times, g_delta_times, CHIP_FREQ);
	}

	write_c0_count(0); // init time count
	rab_desc_rdma(controller, 0, _rioAddr, _pDmaStressBuf + 0x20000000, _memLen, receiver_ID);
	g_start_times = read_c0_count();
	start_rab_rdma(controller, 0);
	udelay(1000000); // wait for interrupt print
	if (g_speed_test) {
		g_delta_times = g_end_times - g_start_times;
		transfer_speed = ((CHIP_FREQ * (u64)_memLen / (u64)g_delta_times) >> 20); // MB/s
		uart_printf("rab%d rdma v = %d(MB/s), data_len:%d(bytes), start_times:0x%x, end_times:0x%x, delta_times:0x%x [for CHIP_FREQ = %d]\r\n",
			controller, transfer_speed, _memLen, g_start_times, g_end_times, g_delta_times, CHIP_FREQ);
	}

	error_cnt = 0;
	for (i = 0; i < _memLen; i += 4) {
#if CACHE_EN
		dst_val = phx_read_cache_u32(_pDmaStressBuf + 0x20000000 + i);
#else
		dst_val = phx_read_u32(_pDmaStressBuf + 0x20000000 + i);
#endif
		if (dst_val != (0x12345678 + i)) {
			error_cnt++;
			//uart_printf("rdma error:index = 0x%x, value = 0x%x, expect = 0x%x\r\n", i, dst_val, 0x12345678 + i);
		}
	}
	if (error_cnt)
		uart_printf("========================= desc rdma err! error_cnt = 0x%x\r\n", error_cnt);
	else
		uart_printf("========================= desc rdma pass!\r\n");

#else // RAB_SENDER_ENDIAN

	udelay(2000000); // wait sender inits src data and completes wdma
	uart_printf("p2p dma receiver\r\n");

	for (i = 0; i < _memLen; i += 4) {
#if CACHE_EN
		dst_val = phx_read_cache_u32(RIO_MEM_ADDR + i);
#else
		dst_val = phx_read_u32(RIO_MEM_ADDR + i);
#endif
		if (dst_val != (0x12345678 + i)) {
			error_cnt++;
			//uart_printf("wdma error:index = 0x%x, value = 0x%x, expect = 0x%x\r\n", i, dst_val, 0x12345678 + i);
		}
	}
	if (error_cnt)
		uart_printf("========================= desc wdma err! error_cnt = 0x%x\r\n", error_cnt);
	else
		uart_printf("========================= desc wdma pass!\r\n");

#endif // RAB_SENDER_ENDIAN
}

void rab_dma_register_speed_test(u16 rio0ID, u16 rio1ID)
{
	int i;
	u64 memAddr;
	u32 rioAddr0 = 0x40000000;
	u32 rioAddr1 = 0x60000000;
	u32 memlen = 64 * 1024;

	uart_printf("rab dma register speed test!\n");

	memAddr = 0x1812300000ULL;
	phx_write_u32(memAddr, 0x12345678);
	phx_write_u32(memAddr + 4, 0xfedcba98);
	phx_write_u32(memAddr + 8, 0xaabbccdd);
	phx_write_u32(memAddr + 0xc, 0x87654321);

	register_rab_dma_wdma(1, 0, memAddr, rioAddr0, memlen, rio0ID);

	uart_printf("desc dma reg speed end!\n");
}


void rab_dma_register_test(u16 rio0ID, u16 rio1ID)
{
	int i;
	u64 memAddr;
	u32 rioAddr0 = 0x40000000;
	u32 rioAddr1 = 0x60000000;
	u32 memlen = 16;

	u32 src0 = 0, src1 = 0, src2 = 0, src3 = 0;
	u32 tmp1, tmp2;
	u32 error_cnt;

	uart_printf("rab dma register test!\n");

	memAddr = 0x801000000;
	phx_write_u32(memAddr, 0x12345678);
	phx_write_u32(memAddr + 4, 0xfedcba98);
	phx_write_u32(memAddr + 8, 0xaabbccdd);
	phx_write_u32(memAddr + 0xc, 0x87654321);

	src0 = phx_read_u32(memAddr);
	src1 = phx_read_u32(memAddr+4);
	src2 = phx_read_u32(memAddr+8);
	src3 = phx_read_u32(memAddr+12);
	uart_printf("src:[0]%x [1]%x [2]%x [3]%x  \n", src0, src1, src2, src3);

	for (i = 0; i < 8; i++)
	{
		register_rab_dma_wdma(1, i, memAddr, rioAddr0 + i * memlen, memlen, rio0ID);
		register_rab_dma_wdma(0, i, memAddr, rioAddr1 + i * memlen, memlen, rio1ID);
		register_rab_dma_rdma(1, i, rioAddr0 + i * memlen, memAddr + 0x4000000 + i * memlen, memlen, rio0ID);
		register_rab_dma_rdma(0, i, rioAddr1 + i * memlen, memAddr + 0x5000000 + i * memlen, memlen, rio1ID);

		src0 = phx_read_u32(memAddr + 0x4000000 + i * memlen);
		src1 = phx_read_u32(memAddr + 0x4000000 + i * memlen + 4);
		src2 = phx_read_u32(memAddr + 0x4000000 + i * memlen + 8);
		src3 = phx_read_u32(memAddr + 0x4000000 + i * memlen + 12);
		uart_printf("src#4000000:[0]%x [1]%x [2]%x [3]%x\n", src0, src1, src2, src3);

		src0 = phx_read_u32(memAddr + 0x5000000 + i * memlen);
		src1 = phx_read_u32(memAddr + 0x5000000 + i * memlen + 4);
		src2 = phx_read_u32(memAddr + 0x5000000 + i * memlen + 8);
		src3 = phx_read_u32(memAddr + 0x5000000 + i * memlen + 12);
		uart_printf("src#5000000:[0]%x [1]%x [2]%x [3]%x\n", src0, src1, src2, src3);

#if 0
		tmp1= phx_read_u32(memAddr+0x4000000 + 0x0);
		tmp2= phx_read_u32(rioAddr1 + 0x0);
		uart_printf("tmp1:%x, tmp2:%x  \n", tmp1, tmp2);
		if (tmp1 != tmp2)
			uart_printf("test err!\n");
		tmp1= phx_read_u32(memAddr+0x4000000 + 0x4);
		tmp2= phx_read_u32(rioAddr1 + 0x4);
		uart_printf("tmp1:%x, tmp2:%x  \n", tmp1, tmp2);
		if (tmp1 != tmp2)
			uart_printf("test err!\n");
		tmp1= phx_read_u32(memAddr+0x4000000 + 0x8);
		tmp2= phx_read_u32(rioAddr1 + 0x8);
		uart_printf("tmp1:%x, tmp2:%x  \n", tmp1, tmp2);
		if (tmp1 != tmp2)
			uart_printf("test err!\n");
		tmp1= phx_read_u32(memAddr+0x4000000 + 0xc);
		tmp2= phx_read_u32(rioAddr1 + 0xc);
		uart_printf("tmp1:%x, tmp2:%x  \n", tmp1, tmp2);
		if (tmp1 != tmp2)
			uart_printf("test err!\n");

		tmp1= phx_read_u32(memAddr+0x5000000+i*memlen+ 0x0);
		tmp2= phx_read_u32(rioAddr0+i*memlen + 0x0);
		uart_printf("tmp1:%x, tmp2:%x  \n", tmp1, tmp2);
		if (tmp1 != tmp2) {
			uart_printf("r%d 0 err!\n", i);
			error_cnt++;
		}
		tmp1= phx_read_u32(memAddr+0x5000000+i*memlen+ 0x4);
		tmp2= phx_read_u32(rioAddr0+i*memlen + 0x4);
		uart_printf("tmp1:%x, tmp2:%x  \n", tmp1, tmp2);
		if (tmp1 != tmp2) {
			uart_printf("r%d 4 err!\n", i);
			error_cnt++;
		}
		tmp1= phx_read_u32(memAddr+0x5000000+i*memlen+ 0x8);
		tmp2= phx_read_u32(rioAddr0+i*memlen + 0x8);
		uart_printf("tmp1:%x, tmp2:%x  \n", tmp1, tmp2);
		if (tmp1 != tmp2) {
			uart_printf("r%d 8 err!\n", i);
			error_cnt++;
		}
		tmp1= phx_read_u32(memAddr+0x5000000+i*memlen+ 0xc);
		tmp2= phx_read_u32(rioAddr0+i*memlen + 0xc);
		uart_printf("tmp1:%x, tmp2:%x  \n", tmp1, tmp2);
		if (tmp1 != tmp2) {
			uart_printf("r%d c err!\n", i);
			error_cnt++;
		}
#endif
    }

	if (error_cnt)
		uart_printf("desc dma err!\n");
	else
		uart_printf("desc dma pass!\n");
}

