#include "rab.h"
#include "srio.h"


/* Set Outbound Doorbell Control & Status Register
 * index                   : 0 ~ RAB_NUM _OB_DB - 1
 * priority      (2 bits)  : Priority of the Outbound Doorbell Messages
 *                           b11 is illegal for Request)
 * crf           (1 bit)   : CRF of the Outbound Doorbell Messages.
 * wdma_assigned (1 bit)   : When this bit is set, an Outbound Doorbell
 *                           Messages is sent to after the last NWrite
 *                           of the WDMA descriptor.
 * wdma_number   (3 bits)  : The WDMA that this OB DB Engine is assgined to.
 * dest_id       (16 bits) : Destination ID of the Outbound Doorbell Message
 */
void set_ob_db_csr(u8 controller, u8 index, u8 priority,
		u8 crf, u8 wdma_assigned, u8 wdma_number, u16 dest_id)
{
	u32 ob_db_csr;

	ob_db_csr = ((priority & 0x3) << 4) |
		((crf & 0x1) << 6) |
		((wdma_assigned & 0x1) << 7) |
		((wdma_number & 0x7) << 8) |
		(dest_id << 16);

	rab_page_write(controller, RAB_OB_DB_CSR(index), ob_db_csr);
}

/* Set Outbound Doorbell Control & Status Register
 * index                   : 0 ~ RAB_NUM _OB_DB - 1
 * send          (1 bit)   : Software sets this bit to trigger the trans-
 *                           mission of an OB Doorbell Message, using the
 *                           programmed DestID and Info. This bit is self-
 *                           clearing after being set.
 *                           Software can send a second message
 *                           by setting this bit once again, without wait-
 *                           ing for the previous message to finish. Res-
 *                           ponses of the previous messages will be
 *                           silently dropped.
 * status        (3 bits)  :
 *                            000(Done)   : Received a DONE Response (success-
 *                                          ful completion);
 *                            001(Retry)  : Received a RETRY Response (triggers
 *                                          a retransmission);
 *                            010(Error)  : Received an ERROR Response;
 *                            011(Timeout): Timeout of waiting for the Response;
 *                            100(Pending): Indicating pending status upon
 *                                          SW setting the Send bit.
 *                            Others      : reserved.
 * priority      (2 bits)  : Priority of the Outbound Doorbell Messages
 *                           (2’b11 is illegal for Request)
 * crf           (1 bit)   : CRF of the Outbound Doorbell Messages.
 * wdma_assigned (1 bit)   : When this bit is set, an Outbound Doorbell
 *                           Messages is sent to after the last NWrite
 *                           of the WDMA descriptor.
 * wdma_number   (3 bits)  : The WDMA that this OB DB Engine is assgined to.
 * dest_id       (16 bits) : Destination ID of the Outbound Doorbell Message
 */
void get_ob_db_csr(u8 controller, u8 index, u8 *send,
		u8 *status, u8 *priority, u8 *crf, u8 *wdma_assigned,
		u8 *wdma_number, u16 *dest_id)
{
	u32 ob_db_csr;

	ob_db_csr = rab_page_read(controller, RAB_OB_DB_CSR(index));
	*send = ob_db_csr & 0x1;
	*status = (ob_db_csr >> 1) & 0x7;
	*priority = (ob_db_csr >> 4) & 0x3;
	*crf = (ob_db_csr >> 6) & 0x1;
	*wdma_assigned = (ob_db_csr >> 7) & 0x1;
	*wdma_number = (ob_db_csr >> 8) * 0x7;
	*dest_id = (ob_db_csr >> 16) & 0xffff;
}

void start_trans_ob_db_msg(u8 controller, u8 index)
{
	u32 ob_db_csr;

	ob_db_csr = rab_page_read(controller, RAB_OB_DB_CSR(index));
	rab_page_write(controller, RAB_OB_DB_CSR(index), ob_db_csr | 0x1);
}

e_trans_msg_status get_trams_ob_db_msg_status(u8 controller, u8 index)
{
	u32 ob_db_csr;
	e_trans_msg_status status;

	ob_db_csr = rab_page_read(controller, RAB_OB_DB_CSR(index));
	status = (e_trans_msg_status)((ob_db_csr >> 1) & 0x7);

	return status;
}

/* Set Outbound Doorbell Information Register
 * index      : 0 ~ RAB_NUM _OB_DB - 1
 * info_field : Info Field of the Outbound Doorbell Message
 */
void set_ob_db_info(u8 controller, u8 index, u16 info_field)
{
	rab_page_write(controller, RAB_OB_DB_INFO(index), info_field);
}

/* Get Outbound Doorbell Information Register
 * index      : 0 ~ RAB_NUM _OB_DB - 1
 * info_field : Info Field of the Outbound Doorbell Message
 */
u16 get_ob_db_info(u8 controller, u8 index)
{
	u32 info;

	info = rab_page_read(controller, RAB_OB_DB_INFO(index));
	return info & 0xffff;
}


/* Set Outbound Doorbell Control & Status Register
 * priority      (2 bits)  : Priority of the Outbound Doorbell Messages
 *                           is illegal for Request)
 * crf           (1 bit)   : CRF of the Outbound Doorbell Messages.
 * info_select   (1 bit)   : Info Select
 *                           0b: from RAB_OB_IDB_INFO[Info Field]
 *                           1b: from masked version of {rab_intr, ext_intr}
 * dest_id       (16 bits) : Destination ID of the Outbound Doorbell Message
 */
void set_ob_idb_csr(u8 controller, u8 priority,
		u8 crf, u8 info_select, u16 dest_id)
{
	u32 ob_idb_csr;

	ob_idb_csr = ((priority & 0x3) << 4) |
		((crf & 0x1) << 6) |
		((info_select & 0x1) << 7) |
		(dest_id << 16);

	rab_page_write(controller, RAB_OB_IDB_CSR, ob_idb_csr);
}

/* Get Outbound Doorbell Control & Status Register
 * status        (3 bits)  : 000(Done)   : Received a DONE Response (successful
 *                                         completion);
 *                           001(Retry)  : Received a RETRY Response (triggers
 *                                         a retransmission);
 *                           010(Error)  : Received an ERROR Response (triggers
 *                                         a retransmission);
 *                           011(Timeout): Timeout of waiting for the Response;
 *                           100(Pending): Indicating pending status upon SW
 *                                         setting the Send bit.
 *                           Others       : reserved.
 * priority      (2 bits)  : Priority of the Outbound Doorbell Messages
 *                           is illegal for Request)
 * crf           (1 bit)   : CRF of the Outbound Doorbell Messages.
 * info_select   (1 bit)   : Info Select
 *                           0b: from RAB_OB_IDB_INFO[Info Field]
 *                           1b: from masked version of {rab_intr, ext_intr}
 * dest_id       (16 bits) : Destination ID of the Outbound Doorbell Message
 */
void get_ob_idb_csr(u8 controller, u8 *status, u8 *priority,
		u8 *crf, u8 *info_select, u16 *dest_id)
{
	u32 ob_idb_csr;

	ob_idb_csr = rab_page_read(controller, RAB_OB_IDB_CSR);

	*status = (ob_idb_csr >> 1) & 7;
	*priority = (ob_idb_csr >> 4) & 0x3;
	*crf = (ob_idb_csr >> 6) >> 0x1;
	*info_select = (ob_idb_csr >> 7) & 0x1;
	*dest_id = (ob_idb_csr >> 16) & 0xffff;
}

/* Set Outbound Interrupt Doorbell Information Register
 * info_field (16 bits) :Info Field of the Outbound Interrupt Doorbell Message
 */
void set_ob_idb_info(u8 controller, u16 info_field)
{
	rab_page_write(controller, RAB_OB_IDB_INFO, info_field);
}

/* Set Outbound Interrupt Doorbell Information Register */
u16 get_ob_idb_info(u8 controller)
{
	u32 ob_idb_info;

	ob_idb_info = rab_page_read(controller, RAB_OB_IDB_INFO);
	return ob_idb_info & 0xffff;
}

/* Set Inbound Doorbell Control & Status Register */
void enable_ib_db(u8 controller)
{
	rab_page_write(controller, RAB_IB_DB_CSR, 1);
}

void disable_ib_db(u8 controller)
{
	rab_page_write(controller, RAB_IB_DB_CSR, 0);
}

u8 get_ib_db_msg_number(u8 controller)
{
	u32 ib_db_csr = rab_page_read(controller, RAB_IB_DB_CSR);
	return (ib_db_csr >> 16) & 0x3f;
}

void get_ib_db_info(u8 controller, u16 *info_field, u16 *source_id)
{
	u32 ib_db_info;

	ib_db_info = rab_page_read(controller, RAB_IB_DB_INFO);
	*info_field = ib_db_info & 0xffff;
	*source_id = (ib_db_info  >> 16) & 0xffff;
}

/* Set Inbound Doorbell Check Register
 * status: It indicates having received an IB DB with matched Info
 * info : The Info to be compared with the IB DB.
 */
void set_and_enable_ib_db_chk(u8 controller, u8 index, u8 status, u16 info)
{
	u32 ib_db_chk;

	ib_db_chk = 1 |
		((status & 0x1) << 8) |
		((info & 0xffff) << 16);

	rab_page_write(controller, RAB_IB_DB_CHK(index), ib_db_chk);
}

void get_ib_db_chk(u8 controller, u8 index,
		u8 *enable, u8 *status, u16 *info)
{
	u32 ib_db_chk;

	ib_db_chk = rab_page_read(controller, RAB_IB_DB_CHK(index));

	*enable = ib_db_chk & 0x1;
	*status = (ib_db_chk >> 8) & 0x1;
	*info = (ib_db_chk >> 16) & 0xffff;
}

void assign_doorbell(u8 controller, u8 db_index,
		u8 dma_number, u16 info, u16 destID)
{
	set_ob_db_csr(controller, db_index, 0, 0, 1,
			dma_number, destID);
	set_ob_db_info(controller, db_index, info);
}

/*RAB DorrBell���Ͳ���*/
void rab_db_test(u8 controller, u16 targetID, u16 dbInfo)
{
	/*ldf 20230405, ��������*/
	return bslRioSendDB(controller, targetID, dbInfo);
}
