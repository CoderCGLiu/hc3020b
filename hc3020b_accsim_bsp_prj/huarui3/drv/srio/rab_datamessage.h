#ifndef _RAB_DATAMESSAGE_H_
#define _RAB_DATAMESSAGE_H_

#include "rab.h"
#include "srio.h"

#define DME_SOURCE_ADDR_256_ALIGNED

#define DME_BUFFER_SIZE		0x1000 /* 512B, 1KB, 2KB, 4KB */

#define RIO_DMSG_SSIZE_8BYTES		1
#define RIO_DMSG_SSIZE_16BYTES		2
#define RIO_DMSG_SSIZE_32BYTES		3
#define RIO_DMSG_SSIZE_64BYTES		4
#define RIO_DMSG_SSIZE_128BYTES		5
#define RIO_DMSG_SSIZE_256BYTES		6

#ifdef DME_SOURCE_ADDR_256_ALIGNED
#define RIO_DMSG_SSIZE_DEFAULT	RIO_DMSG_SSIZE_256BYTES
#endif

#pragma pack (1)
struct rab_ib_dme_desc {
	/* W0 */
	u8 valid             : 1;
	u8 next_desc_p_valid : 1;
	u8 end_of_chain      : 1;
	u8 intr_enable       : 1;
	u8 buffer_size       : 2; /* 00 - 512B; 01 - 1KB; 10 - 2KB; 11 - 4KB */
	u8 _resv_1           : 2;
	u8 done              : 1;
	u8 timeout_error     : 1;
	u8 axi_error         : 1;
	u8 rio_error         : 1;
	u8 _resv_2           : 4;
	u16 sourceID         : 16;

	/* W1 */
	u8 letter            : 2;
	u8 mbox              : 2;
	u8 xmbox             : 4;
	u16 msg_len          : 10;
	u8 _resv_3           : 5;
	u8 crf               : 1;
	u8 dest_addr_37_32   : 6;
	u8 priority          : 2;

	/* W2 */
	u16 sourceID_h       : 16;
	u16 _resv_4          : 16;

	/* W3 */
	u32 _resv_5          : 32;

	/* W4 */
	u32 dest_addr_37_8   : 30;
	u8 next_desc_addr_37 : 1;
	u8 _resv_6          : 1;

	/* W5 */
	u32 next_desc_addr_36_5 : 32;
};

struct rab_ob_dme_desc {
	/* W0 */
	u8 valid             : 1;
	u8 next_desc_p_valid : 1;
	u8 end_of_chain      : 1;
	u8 intr_enable       : 1;
	u8 _resv_1           : 4;
	u8 done              : 1;
	u8 timeout_error     : 1;
	u8 axi_error         : 1;
	u8 rio_error         : 1;
	u8 port_num          : 2;
	u8 _resv_2           : 2;
	u16 destID           : 16;

	/* W1 */
	u8 letter            : 2;
	u8 mbox              : 2;
	u8 xmbox             : 4;
	u16 msg_len          : 10;
	u8 standard_size     : 3;
	u8 source_addr_37_30 : 8;
	u8 crf               : 1;
	u8 priority          : 2;

	/* w2 */
	u16 destID_h         : 16;
	u16 _resv_3          : 16;

	/* w3*/
	u32 _resv_4          : 32;

	/* w4 */
	u32 source_addr_37_8 : 30;
	u8 next_desc_addr_37 : 1;
	u8 _resv_5           : 1;

	/* W5 */
	u32 next_desc_addr_36_5 : 32;
};
#pragma pack ()

#define DME_DESC_CHAIN_TRANS_COMPLETED	1
#define DME_DESC_TRANS_COMPLETED	(1 << 1)
#define DME_DESC_FETCH_ERROR		(1 << 2)
#define DME_DESC_ERROR			(1 << 3)
#define DME_DESC_UPDATE_ERROR		(1 << 4)
#define DME_DESC_TRANS_ERROR		(1 << 5)
#define DME_MSG_ERROR			(1 << 6)
#define DME_MSG_TIMEOUT			(1 << 7)
#define DME_TRANS_PENDING		(1 << 8)
#define DME_SLEEPING			(1 << 9)

// shareable addr to cache mode, unshareable addr to uncache mode
//#define RAB_MEM_BASE(_controller)		(0x11400000 + (_controller)*0x200000) // sram unshareable addr
#define RAB_MEM_BASE(_controller)		(0x1090000000 + (_controller)*0x1000000)
#define RAB_IB_DME_DESC_START(_controller)	(RAB_MEM_BASE(_controller))
#define RAB_IB_DME_DESC_END(_controller)	(RAB_IB_DME_DESC_START(_controller) + 0x400)
#define RAB_IB_DME_BUF_START(_controller)	(RAB_IB_DME_DESC_END(_controller))
#define RAB_IB_DME_BUF_END(_controller)		(RAB_IB_DME_BUF_START(_controller) + 0x4000*0x20) /* 16 KB */
#define RAB_OB_DME_DESC_START(_controller)	(RAB_IB_DME_BUF_END(_controller))
#define RAB_OB_DME_DESC_END(_controller)	(RAB_OB_DME_DESC_START(_controller) + 0x400)
#define RAB_OB_DME_BUF_START(_controller)	(RAB_OB_DME_DESC_END(_controller))
#define RAB_OB_DME_BUF_END(_controller)		(RAB_OB_DME_BUF_START(_controller) + 0x5000) /* 20 KB */
#define RAB_OB_DSE_DESC_START(_controller)	(RAB_OB_DME_BUF_END(_controller))
#define RAB_OB_DSE_DESC_END(_controller)	(RAB_OB_DSE_DESC_START(_controller) + 0X200)
#define RAB_OB_DSE_DATA_START(_controller)	(RAB_OB_DSE_DESC_END(_controller))
#define RAB_OB_DSE_DATA_END(_controller) 	(RAB_OB_DSE_DATA_START(_controller) + 0x1000 * 0xa)
#define RAB_IB_DSE_DESC_START(_controller)	(RAB_OB_DSE_DATA_END(_controller))
#define RAB_IB_DSE_DESC_END(_controller)	(RAB_IB_DSE_DESC_START(_controller) + 0x200)
#define RAB_IB_DSE_DATA_START(_controller)	(RAB_IB_DSE_DESC_END(_controller))
#define RAB_IB_DSE_DATA_END(_controller)	(RAB_IB_DSE_DATA_START(_controller) + (0x1000 * 0xa))

#define BFM_DEST_ID	0x0000

#endif /* _RAB_DATAMESSAGE_H_ */
