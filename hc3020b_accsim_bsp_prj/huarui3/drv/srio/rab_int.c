#include "rab.h"
#include "srio.h"
#include "rab_int.h"
#include "rab_dma.h"
#include <taskLib.h>
#include <semLib.h>
#include <semaphore.h>
#include <hr3_intc_defs.h>
#include <irq.h>
#include <bsp_int.h>
#include "vcscpumacro.h"

extern u32 g_speed_test;
extern u32 g_end_times;
int test_debug = 0;

#if HR_RAB_DB_SEM
#if _RIO_SEM_REWORKS_
extern sem_t HR_RAB_DB_SendSem[2];
extern sem_t HR_RAB_DB_RecvSem[2];
#else
extern SEM_ID HR_RAB_DB_SendSem[2];
extern SEM_ID HR_RAB_DB_RecvSem[2];
#endif /*_RIO_SEM_REWORKS_*/
#endif /*HR_RAB_DB_SEM*/

#if _RIO_SEM_REWORKS_
extern sem_t HR_RAB_DMA_WSem[2][RAB_DMA_MAX_ENGINE];
extern sem_t HR_RAB_DMA_RSem[2][RAB_DMA_MAX_ENGINE];
#else
extern SEM_ID HR_RAB_DMA_WSem[2][RAB_DMA_MAX_ENGINE];
extern SEM_ID HR_RAB_DMA_RSem[2][RAB_DMA_MAX_ENGINE];
#endif /*_RIO_SEM_REWORKS_*/

#if _TEST_SRIO_DMASndTime_
extern unsigned long long tt0, tt1, tt2, tt3, tt4, tt5, tt6, tt7;
unsigned int wdma_int_count = 0;/*ldf 20230526 add:: 进WDMA中断次数*/
#endif

void rab_int_stat(u8 controller)
{
	u32 stat_gnrl;
	u32 stat;
//	u32 saved_page;
	int i;
//	saved_page = phx_read_u32(SRIO_RAB_INNER_ADDR(controller) + 0x0030);/*ldf 20230404, 合并所里的*/

	stat_gnrl = rab_page_read(controller, RAB_INTR_STAT_GNRL) & 0xffff;
	//uart_printf("RAB_INTR_STAT_GNRL: 0x%x \r\n", stat_gnrl);

	if (stat_gnrl & MISC_INTR_ENAB) 
	{
		stat = rab_page_read(controller, RAB_INTR_STAT_MISC);
		rab_page_write(controller, RAB_INTR_STAT_MISC, stat);
		if(test_debug)uart_printf("RAB_INTR_STAT_MISC: 0x%x \r\n", stat);

		if ((stat & 0x1) == 0x1) 
		{
#if HR_RAB_DB_SEM 
#if _RIO_SEM_REWORKS_
			sem_post(&HR_RAB_DB_RecvSem[controller]);
#else
		    semGive(HR_RAB_DB_RecvSem[controller]);
#endif /*_RIO_SEM_REWORKS_*/
#endif /*HR_RAB_DB_SEM*/	
			if(test_debug)uart_printf("IB Doorbell recv! \r\n");
		}

		if ((stat & 0x2) == 0x2)
		{
#if _TEST_SRIO_DBSndTime_ /*ldf 20230523 add:: 发送门铃耗时计算*/
			extern unsigned long long t2;
			t2 = bslGetTimeUsec();
#endif /*_TEST_SRIO_DBSndTime_*/
			
#if HR_RAB_DB_SEM 
#if _RIO_SEM_REWORKS_
			sem_post(&HR_RAB_DB_SendSem[controller]);
#else
		    semGive(HR_RAB_DB_SendSem[controller]);
#endif /*_RIO_SEM_REWORKS_*/
		    
#if _TEST_SRIO_DBSndTime_ /*ldf 20230523 add:: 发送门铃耗时计算*/
			extern unsigned long long t3;
			t3 = bslGetTimeUsec();
#endif /*_TEST_SRIO_DBSndTime_*/	
#endif /*HR_RAB_DB_SEM*/	
			if(test_debug)uart_printf("OB Doorbell done! \r\n");
		}

		if ((stat & 0x4) == 0x4)
			if(test_debug)uart_printf("Unexpected IB Message! \r\n");

		if ((stat & 0x8) == 0x8)
			if(test_debug)uart_printf("Unsupport RIO Request! \r\n");

		if ((stat & 0x10) == 0x10)
			if(test_debug)uart_printf("GRIO port-write Recv! \r\n");

		if ((stat & 0x20) == 0x20)
			if(test_debug)uart_printf("GRIO Err! \r\n");
		
	}

	if (stat_gnrl & WDMA_INTR_ENAB) 
	{
#if _TEST_SRIO_DMASndTime_
		wdma_int_count++;
		if((wdma_int_count % _TEST_DMA_PRINT_CYCLE_) == 0)
			tt2 = bslGetTimeUsec();
//		printk("<***DEBUG***> [%s:_%d_]:: [CPU%d] [wdma中断: %u] IN\n",__FUNCTION__,__LINE__,cpu_id_get(),wdma_int_count);
#endif /*_TEST_SRIO_DMASndTime_*/
//		if (g_speed_test)
//			g_end_times = read_c0_count();
		stat = rab_page_read(controller, RAB_INTR_STAT_WDMA);
		rab_page_write(controller, RAB_INTR_STAT_WDMA, stat);

//		printk("<DEBUG> [%s:__%d__]\n",__FUNCTION__,__LINE__);
		
		//if (!g_speed_test) {
			if (stat & 0xff)
				if(test_debug)uart_printf("WDMA chain Desc Transfer done 0x%x \r\n", stat & 0xff);

			if ((stat >> 8) & 0xff)
				if(test_debug)uart_printf("WDMA Desc done Transfer 0x%x \r\n", (stat >> 8) & 0xff);
//				printk("WDMA Desc done Transfer 0x%x \r\n", (stat >> 8) & 0xff);
		//}

		if ((stat >> 16) & 0xff)
			if(test_debug)uart_printf("WDMA Transfer Aborted 0x%x \r\n", (stat >> 16) & 0xff);
    	
//		printk("<***DEBUG***> [%s:_%d_]:: [CPU%d] [wdma中断: %u] stat = 0x%x\n",__FUNCTION__,__LINE__,cpu_id_get(),wdma_int_count,stat);
		
		if (stat & 0xff)/*ldf 20230526 add:: DMA传输完成再释放信号量*/
		{
//			printk("<***DEBUG***> [%s:_%d_]:: [CPU%d] [wdma中断: %u] stat = 0x%x\n",__FUNCTION__,__LINE__,cpu_id_get(),wdma_int_count,stat);
			for (i = 0; i < RAB_DMA_MAX_ENGINE; i++)
			{
				if (stat & (1 << i))
				{
#if _RIO_SEM_REWORKS_
					sem_post(&HR_RAB_DMA_WSem[controller][i]);
#else
					semGive(HR_RAB_DMA_WSem[controller][i]);
#endif
				}
			}
		}
#if _TEST_SRIO_DMASndTime_
		if((wdma_int_count % _TEST_DMA_PRINT_CYCLE_) == 0)
		{
			tt3 = bslGetTimeUsec();
//			printk("<***DEBUG***> [%s:_%d_]:: [CPU%d] [wdma中断: %u] OUT\n",__FUNCTION__,__LINE__,cpu_id_get(),wdma_int_count);
		}
#endif /*_TEST_SRIO_DMASndTime_*/
	}

	if (stat_gnrl & RDMA_INTR_ENAB) {
//		if (g_speed_test)
//			g_end_times = read_c0_count();
		stat = rab_page_read(controller, RAB_INTR_STAT_RDMA);
		rab_page_write(controller, RAB_INTR_STAT_RDMA, stat);

		//if (!g_speed_test) {
			if (stat & 0xff)
				if(test_debug)uart_printf("RDMA chain Desc Transfer done 0x%x \r\n", stat & 0xff);
//				printk("RDMA chain Desc Transfer done 0x%x \r\n", stat & 0xff);

			if ((stat >> 8) & 0xff)
				if(test_debug)uart_printf("RDMA Desc done Transfer 0x%x \r\n", (stat >> 8) & 0xff);
//				printk("RDMA Desc done Transfer 0x%x \r\n", (stat >> 8) & 0xff);
		//}

		if ((stat >> 16) & 0xff)
			if(test_debug)uart_printf("RDMA Transfer Aborted 0x%x \r\n", (stat >> 16) & 0xff);
//			printk("RDMA Transfer Aborted 0x%x \r\n", (stat >> 16) & 0xff);

        for (i = 0; i < RAB_DMA_MAX_ENGINE; i++)
        {
            if (stat & (1 << i))
            {
#if _RIO_SEM_REWORKS_
            	sem_post(&HR_RAB_DMA_RSem[controller][i]);
#else
                semGive(HR_RAB_DMA_RSem[controller][i]);
#endif
            }
        }
	}

	if (stat_gnrl & RIO_PIO_INTR_ENAB) {
		stat = rab_page_read(controller, RAB_INTR_STAT_RPIO);
		rab_page_write(controller, RAB_INTR_STAT_RPIO, stat);

		//if (!g_speed_test) {
			if (stat & 0xff)
				if(test_debug)uart_printf("RIO PIO Trans done 0x%x \r\n", stat);
		//}

		if ((stat >> 8) & 0xff)
			if(test_debug)uart_printf("RIO PIO Trans Failed 0x%x \r\n", stat);
	}

	if (stat_gnrl & AXI_PIO_INTR_ENAB) {
		stat = rab_page_read(controller, RAB_INTR_STAT_APIO);
		rab_page_write(controller, RAB_INTR_STAT_APIO, stat);
		//uart_printf("RAB_INTR_STAT_APIO: 0x%x \r\n", stat);

		if (stat & 0xff)
			if(test_debug)uart_printf("AXI PIO Trans done 0x%x \r\n", stat);

		if ((stat >> 8) & 0xff)
			if(test_debug)uart_printf("AXI PIO Trans Failed 0x%x \r\n", stat);
	}

	if (stat_gnrl & OB_DME_INTR_ENAB) {
		stat = rab_page_read(controller, RAB_INTR_STAT_ODME);
		rab_page_write(controller, RAB_INTR_STAT_ODME, stat);
		//uart_printf("RAB_INTR_STAT_ODME: 0x%x \r\n", stat);

		if (stat > 0)
			if(test_debug)uart_printf("OB dme intr = 0x%x \r\n", stat);
		else
			if(test_debug)uart_printf("OB dme intr err intr = 0x%x\r\n", stat);
	}

	if (stat_gnrl & IB_DME_INTR_ENAB) {
		stat = rab_page_read(controller, RAB_INTR_STAT_IDME);
		rab_page_write(controller, RAB_INTR_STAT_IDME, stat);
		//uart_printf("RAB_INTR_STAT_IDME: 0x%x \r\n", stat);

		if (stat > 0)
			if(test_debug)uart_printf("IB dme intr = 0x%x \r\n", stat);
		else
			if(test_debug)uart_printf("IB dme intr err intr = 0x%x\r\n", stat);
	}

	if (stat_gnrl & OB_DS_INTR_ENAB) {
		stat = rab_page_read(controller, RAB_INTR_STAT_OBDSE);
		rab_page_write(controller, RAB_INTR_STAT_OBDSE, stat);
		if(test_debug)uart_printf("OBDSE stat = 0x%x \r\n", stat);
	}

	if (stat_gnrl & IB_DS_INTR_ENAB) {
		stat = rab_page_read(controller, RAB_INTR_STAT_IBDSE_LO);
		rab_page_write(controller, RAB_INTR_STAT_IBDSE_LO, stat);
		if(test_debug)uart_printf("IBDSE stat = 0x%x \r\n", stat);
	}

	// restore page
//	phx_write_u32(SRIO_RAB_INNER_ADDR(controller) + 0x0030, saved_page);/*ldf 20230404, 合并所里的*/
}

static void set_rab_intr_enab_gnrl(u8 controller, u32 intr_flags, u16 ext_ob_db_intr_enab)
{
	intr_flags |= (ext_ob_db_intr_enab & 0x7fff) << 16;
	rab_page_write(controller, RAB_INTR_ENAB_GNRL, intr_flags);
}

void rab0_int_do()
{
	u64 intr_val;
	intr_val = phx_read_u64(DSP48_PIC_STA_H_R);
	if (intr_val & RAB0_INTR) {
		//uart_printf("rab0 intr! \r\n");
		//uart_printf("rab0 intr,cpunum=%d! \r\n",cpunum);
		rab_int_stat(0);
	}
//	else {
//		uart_printf("err! rab0 not intr!0x%x\r\n", intr_val);
//	}
	phx_write_u64(DSP48_PIC_CLR_H_R, RAB0_INTR);
}

void rab1_int_do()
{
	u64 intr_val;
	intr_val = phx_read_u64(DSP48_PIC_STA_H_R);
	if (intr_val & RAB1_INTR) {
		//uart_printf("rab1 intr! \r\n");
		rab_int_stat(1);
	}
//	else {
//		uart_printf("err! rab1 not intr!0x%x \r\n", intr_val);
//	}
	phx_write_u64(DSP48_PIC_CLR_H_R, RAB1_INTR);
}

void rab_int_Init(u8 controller)
{
	hrSrioIntConnect(controller);/*ldf 20230405,挂接中断处理函数*/
	
	rab_page_write(controller, RAB_CTRL, RAB_CTRL_AMBA_PIO_ENABLE |
		RAB_CTRL_RIO_PIO_ENABLE | RAB_CTRL_WR_DMA_ENABLE | RAB_CTRL_RD_DMA_ENABLE);

	rab_page_write(controller, RAB_INTR_ENAB_APIO, 0xffff);
	rab_page_write(controller, RAB_INTR_ENAB_RPIO, 0xffff);

	rab_page_write(controller, RAB_INTR_ENAB_MISC, 0xffffffff);

	set_rab_intr_enab_gnrl(controller, OB_DME_INTR_ENAB | IB_DME_INTR_ENAB | WDMA_INTR_ENAB |
			RDMA_INTR_ENAB | RIO_PIO_INTR_ENAB | AXI_PIO_INTR_ENAB | IB_DS_INTR_ENAB |
			OB_DS_INTR_ENAB | MISC_INTR_ENAB, 0);

	set_rab_intr_enab_wdma(controller, 0xff, 0xff, 0);
	set_rab_intr_enab_rdma(controller, 0xff, 0xff, 0);

	return;
}
