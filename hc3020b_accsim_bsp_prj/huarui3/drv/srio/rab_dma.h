#ifndef _RAB_DMA_H_
#define _RAB_DMA_H_

#include "rab.h"
#include "srio.h"

//#define CPHYSADDR(a)    ((a) & 0x1fffffff)

#define RAB_DMA_WDMA 1
#define RAB_DMA_RDMA 0

#define RAB_DMA_MAX_ENGINE		8
#define RAB_DMA_MAX_TRANSLEN		(256 * 0x100000)
//#define RAB_DMA_MAX_TRANSFER_LEN	0xffffc
#define RAB_DMA_MAX_TRANSFER_LEN	0x80000 //512K

#define RAB_DMA_MAX_DESC_NUM	(RAB_DMA_MAX_TRANSLEN / RAB_DMA_MAX_TRANSFER_LEN + 1)

struct rab_dma_desc {
	u32 valid			: 1;
	u32 next_desc_pointer_valid	: 1;
	u32 desc_axi_ext_addr_msb	: 3;
	u32 trans_len			: 18;
	u32 axi_ext_addr_msb		: 1;
	u32 done			: 1;
	u32 axi_error			: 1;
	u32 rio_error			: 1;
	u32 desc_fetch_error		: 1;
	u32 desc_update_error		: 1;
	u32 axi_ext_addr		: 3;

	u32 src_word_addr		: 32;
	u32 dst_word_addr		: 32;

	u32 next_desc_word_addr		: 30;
	u32 desc_axi_ext_addr 		: 2;
};

#define RAB_DMA_DESC_LEN		(sizeof(struct rab_dma_desc)) //16B


void set_rab_intr_enab_wdma(u8 controller, u8 chain_desc_trans_completed,
        u8 desc_trans_completed, u8 trans_aborted);
void set_rab_intr_enab_rdma(u8 controller, u8 chain_desc_trans_completed,
        u8 desc_trans_completed, u8 trans_aborted);
void rab_dma_test(u16 rio0ID, u16 rio1ID);

#endif /* _RAB_DMA_H_ */
