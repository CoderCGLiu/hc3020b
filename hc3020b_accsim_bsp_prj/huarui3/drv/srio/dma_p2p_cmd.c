#include "srio.h"
#include "rab.h"

#define RAB_SENDER_ENDIAN 1

/*
#include "rab_amap.c"
#include "rab_dma.c"
#include "rab_int.c"
*/

#define SENDER_ID 0x3333
#define RECEIVER_ID 0x9999
#define SPEEDUP_ENABLE 1
#define SPEEDUP_DISABLE 0

void p2p_main(void)
{
	sriox SEND_SRIO;
	sriox RECEIVE_SRIO;
	sriox SRIO;
	u32 tmp;


	uart_printf("Test DMA\r\n");

	SEND_SRIO = SRIO1;
	RECEIVE_SRIO = SRIO1;
	
	rioLockInit(0);/*ldf 20230404*/
	rioLockInit(1);/*ldf 20230404*/

	// sysctl release reset : pcie0 pcie1 rab0 rab1
//	phx_write_u32(0x1f0c0028, 0xa5accede); // unlock
//	tmp = phx_read_u32(0x1f0c0008);
//	phx_write_u32(0x1f0c0008, tmp & (~0x0c300000));
//#if 1
//	// flush l3 to ddr -------------------------
//	phx_write_u32(0x1e0c0080, 0x4);
//	phx_write_u32(0x1e0c0010, 0x0);
//	phx_write_u32(0x1e0c1080, 0x4);
//	phx_write_u32(0x1e0c1010, 0x0);
//	phx_write_u32(0x1e0c2080, 0x4);
//	phx_write_u32(0x1e0c2010, 0x0);
//	phx_write_u32(0x1e0c3080, 0x4);
//	phx_write_u32(0x1e0c3010, 0x0);
//#endif

#if RAB_SENDER_ENDIAN
	SRIO = SEND_SRIO;
#else
	SRIO = RECEIVE_SRIO;
#endif

	rab_phy_link_ini_config_12G5(SRIO/*, SPEEDUP_ENABLE*/);
#if RAB_SENDER_ENDIAN
	rab_page_write(SRIO, GRIO_BDIDCSR, SENDER_ID);
#else
	rab_page_write(SRIO, GRIO_BDIDCSR, RECEIVER_ID);
#endif

	rab_wait_for_linkup(SRIO);

	rab_page_write(SRIO, RAB_CTRL, RAB_CTRL_AMBA_PIO_ENABLE | RAB_CTRL_RIO_PIO_ENABLE |
			RAB_CTRL_WR_DMA_ENABLE | RAB_CTRL_RD_DMA_ENABLE);
	rab_int_Init(SRIO); // enable interrupt

	set_rab_rpio_ctrl(SRIO, 0, 0x3);
	set_rab_apio_ctrl(SRIO, 0, 0x3);

	rio_set_dma_win_p2p(SRIO);
	udelay(10000); // wait receiver setwin

	rab_dma_p2p_test(SRIO, SENDER_ID, RECEIVER_ID);

	rab_link_and_mode_status(SRIO);

	uart_printf("Test end\r\n");

}
