//#include "srio_debug.h"
#include "srio.h"
#include "rab.h"
/*
#include "rab_amap.c"
#include "rab_dma.c"
#include "rab_datamessage.c"
#include "rab_int.c"
*/

#define SPEEDUP_ENABLE 1
#define SPEEDUP_DISABLE 0
#define RAB0_ID 0x1
#define RAB1_ID 0x2

void dme_main(void)
{
	u32 tmp;
	sriox SRIO_0, SRIO_1;


	uart_printf("Test DME\r\n");

	SRIO_0 = SRIO0;
	SRIO_1 = SRIO1;
	
	rioLockInit(SRIO_0);/*ldf 20230404*/
	rioLockInit(SRIO_1);/*ldf 20230404*/

	// sysctl release reset : pcie0 pcie1 rab0 rab1
//	phx_write_u32(0x1f0c0028, 0xa5accede); // unlock
//	tmp = phx_read_u32(0x1f0c0008);
//	phx_write_u32(0x1f0c0008, tmp & (~0x0c300000));

	rab_phy_link_ini_config_10G3125(SRIO_0/*, SPEEDUP_ENABLE*/);
	rab_page_write(SRIO_0, GRIO_BDIDCSR, RAB0_ID);// rab0 16bit id

	rab_phy_link_ini_config_10G3125(SRIO_1/*, SPEEDUP_ENABLE*/);
	rab_page_write(SRIO_1, GRIO_BDIDCSR, RAB1_ID);// rab1 16bit id

	rab_wait_for_linkup(SRIO_0);
	rab_wait_for_linkup(SRIO_1);

#if 1
	rab_int_Init(0);
	rab_int_Init(1);
#else
	rab_page_write(SRIO_0, RAB_CTRL, RAB_CTRL_AMBA_PIO_ENABLE | RAB_CTRL_RIO_PIO_ENABLE |
			RAB_CTRL_WR_DMA_ENABLE | RAB_CTRL_RD_DMA_ENABLE);
	rab_page_write(SRIO_1, RAB_CTRL, RAB_CTRL_AMBA_PIO_ENABLE | RAB_CTRL_RIO_PIO_ENABLE |
			RAB_CTRL_WR_DMA_ENABLE | RAB_CTRL_RD_DMA_ENABLE);
#endif

	set_rab_rpio_ctrl(0, 0, 0x3);
	set_rab_rpio_ctrl(1, 0, 0x3);
	set_rab_apio_ctrl(0, 0, 0x3);//nread nwrite test
	set_rab_apio_ctrl(1, 0, 0x3);
	rab_page_write(SRIO_0, RAB_IB_PW_CSR, 0x1);
	rab_page_write(SRIO_1, RAB_IB_PW_CSR, 0x1);

	test_i_msg(0);
	test_i_msg(1);
	test_o_msg(0, 0x9999);
	test_o_msg(1, 0x5555);

	udelay(100);
	check_dme_test_result();

	uart_printf("Test end \r\n");

}
