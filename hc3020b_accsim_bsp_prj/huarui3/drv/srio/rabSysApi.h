#ifndef  __RAB_SYS_API_H__
#define  __RAB_SYS_API_H__

#include "rab.h"
#include "srio.h"

/*
 * rapidio接收参数
 */
typedef struct 
{
	unsigned int controller;	
	unsigned short srcID;
	unsigned short dbInfo;
} RapidIO_Recv;

void hrSrioRegionRelease();
/*
功能：获取当前RapidIO设备的ID号
参数：
u8 controller:控制器号，(0-1)
返回值：返回当前RapidIO的ID号
若为0xff,则当前RapidIO设备没有初始化成功
*/
u16 bslRioGetID(u8 controller);

/*
功能：设置当前RapidIO设备的ID号
参数：
u8 controller:控制器号，(0-1)
u16 id:设置的rapidio  id号
返回值：
*/
u16 bslRioSetID(u8 controller, u16 id);

#endif /* __RAB_SYS_API_H__ */
