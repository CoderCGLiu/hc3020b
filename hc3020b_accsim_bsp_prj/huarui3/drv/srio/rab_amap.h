#ifndef _RAB_AMAP_H_
#define _RAB_AMAP_H_

#include "rab.h"
#include "srio.h"


#define RAB_OW_LOW_BASE_ADDR(_controller)	(0x50000000 + ((_controller) * 0x08000000))
#define RAB_OW_LOW_SIZE				0x2000000

#define RAB_OW_HIGH_BASE_ADDR(_controller)	(0x500000000 + ((_controller) * 0x080000000))
#define RAB_OW_HIGH_SIZE			0x20000000
#define RAB_OW_MAINT_BASE_ADDR(_controller)  (0x570000000 + ((_controller) * 0x080000000))

typedef enum 
{
	RAB_APIO_MAINTENANCE = 0,
	RAB_APIO_NREAD_NWRITE = 1,
	RAB_APIO_NREAD_NWRITE_R = 2,
	RAB_APIO_NREAD_SWRITE = 3
} e_apio_type;

void set_rab_rpio_ctrl(u8 controller, u8 index, u32 val);
void set_rab_apio_ctrl(u8 controller, u8 index, u32 val);
void rio_set_dma_win(u16 rio0Id, u16 rio1Id);

#endif /* _RAB_AMAP_H_ */
