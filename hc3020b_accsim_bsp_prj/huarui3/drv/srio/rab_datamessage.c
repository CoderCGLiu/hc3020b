#include "rab_datamessage.h"

#define DM_DEBUG_INFO 0

#if DME_BUFFER_SIZE == 512
	#define IB_DME_BUFFER_SIZE_CODE		0
#elif DME_BUFFER_SIZE == 1024
	#define IB_DME_BUFFER_SIZE_CODE		1
#elif DME_BUFFER_SIZE == 2048
	#define IB_DME_BUFFER_SIZE_CODE		2
#elif DME_BUFFER_SIZE == 4096
	#define IB_DME_BUFFER_SIZE_CODE		3
#else
	#error "wrong dme buffer size"
#endif

void clear_dme_desc_node(void *desc)
{
	u32 *p = desc;

	p[0] = p[1] = p[2] = p[3] = p[4] = p[5] = 0;
}

struct rab_ib_dme_desc* make_a_dmsg_receive_buf(struct rab_ib_dme_desc *desc,
		u64 data_addr)
{
	//if (data_addr & 0xff)
		//return NULL;

	clear_dme_desc_node(desc);

	desc->valid = 1;
	desc->intr_enable = 1;
	desc->buffer_size = IB_DME_BUFFER_SIZE_CODE;
	desc->msg_len = 512;
	desc->dest_addr_37_8 = (data_addr >> 8);

	return desc;
}

int fill_a_data_message(struct rab_ob_dme_desc *desc, u16 dest_id,
		u8 letter, u8 mbox, u8 xmbox,
		u64 data_addr, int data_len)
{
	if (data_addr & 0xff)
		return -1;

	if ((data_len % 8) != 0)
		return -1;

	clear_dme_desc_node(desc);
	desc->valid = 1;
	desc->intr_enable = 1;
	desc->destID = dest_id;
	desc->letter = letter;
	desc->mbox = mbox;
	desc->xmbox = xmbox;
	desc->msg_len = data_len / 8; /* DW 64bits */
	desc->standard_size = RIO_DMSG_SSIZE_DEFAULT;
	desc->source_addr_37_8 = (data_addr >> 8);

	return 0;
}

void copy_dme_desc_to_physical_address(u64 addr, void *desc)
{
	u32 *p = desc;

#if CACHE_EN
	phx_write_cache_u32(addr + 0, p[0]);
	phx_write_cache_u32(addr + 4, p[1]);
	phx_write_cache_u32(addr + 8, p[2]);
	phx_write_cache_u32(addr + 12, p[3]);
	phx_write_cache_u32(addr + 16, p[4]);
	phx_write_cache_u32(addr + 20, p[5]);
	//asm volatile("sync");
#else
	phx_write_u32(addr + 0, p[0]);
	phx_write_u32(addr + 4, p[1]);
	phx_write_u32(addr + 8, p[2]);
	phx_write_u32(addr + 12, p[3]);
	phx_write_u32(addr + 16, p[4]);
	phx_write_u32(addr + 20, p[5]);
	//asm volatile("sync");
#endif
}

void show_dme_desc(struct rab_ob_dme_desc *desc)
{
	u32 *p = (u32*)desc;
	
	uart_printf("desc addr = %x \r\n", p);
	uart_printf("%x \r\n", p[0]);
	uart_printf("%x \r\n", p[1]);
	uart_printf("%x \r\n", p[2]);
	uart_printf("%x \r\n", p[3]);
	uart_printf("%x \r\n", p[4]);
	uart_printf("%x \r\n", p[5]);
}

/*
 * @data_len: should be DW aligned (64 bits)
 *
 * Note: when data_len > 256B, it will use multi segments data message.
 */
int fill_ob_data_message(u8 controller, u64 *p_desc_head,
		u16 dest_id, u8 letter, u8 mbox, u8 xmbox,
		u64 *p_data_head, int data_len)
{
	struct rab_ob_dme_desc ob_desc;
	u64 desc_head = *p_desc_head;
	u64 data_head = *p_data_head;
	u64 data_end = data_head + data_len;
	int len_per_msg = 8 << (RIO_DMSG_SSIZE_DEFAULT - 1);
	int len_per_desc = sizeof(struct rab_ob_dme_desc) + 0x08;

	if (data_len <= 0)
		return -1;

	//while(data_head < data_end - len_per_msg) {
	while(data_head + len_per_msg < data_end) {
		fill_a_data_message(&ob_desc, dest_id, letter, mbox, xmbox, data_head, len_per_msg);
		copy_dme_desc_to_physical_address(desc_head, &ob_desc);
		data_head += len_per_msg;
		desc_head += len_per_desc;
	}

	fill_a_data_message(&ob_desc, dest_id, letter, mbox, xmbox, data_head, data_end - data_head);
	ob_desc.end_of_chain = 1;
	copy_dme_desc_to_physical_address(desc_head, &ob_desc);
	//show_dme_desc(&ob_desc);
	data_head += len_per_msg;
	desc_head += len_per_desc;

	*p_data_head = data_head;
	*p_desc_head = desc_head;

	return 0;
}


/* Set Inbound DME N Control Register
 * letter (2 bits) : Software sets the DME letter value.
 * mbox (2 bits)   : Software sets the DME mbox value
 * xmbox (4 bits)  : Software sets the DME xmbox value.
 */
void set_rab_ib_dme_ctrl(u8 controller, u8 index, u8 letter, u8 mbox, u8 xmbox)
{
	u32 ctrl;

	ctrl = ((letter & 0x3) << 4)
		| ((mbox & 0x3) << 6)
		| ((xmbox & 0xf) << 8);

	rab_page_write(controller, RAB_IB_DME_CTRL(index), ctrl);
}

/* Set Inbound DME N Address Register
 * desc_chain_addr_low:
 * 	Indicates the start address of the descriptor chain, aligned to 4 Words.
 *	In 32 bit AXI Addressing, or local descriptor memory:
 *	Start Address = {RAB_IB_DME_ADDRN[27:0], 4'h0}
 *	In 38 bit AXI Addressing:
 *	Start Address = {RAB_IB_DME_CTRLN[31:30],
 *	RAB_IB_DME_ADDRN[31:0], 4'h0}
 */
void set_rab_ib_dme_addr(u8 controller, u8 index, u64 desc_chain_addr_low)
{
	u32 tmp;

	rab_page_write(controller, RAB_IB_DME_ADDR(index), desc_chain_addr_low >> 5);
	tmp = rab_page_read(controller, RAB_IB_DME_CTRL(index));
	rab_page_write(controller, RAB_IB_DME_CTRL(index), tmp | ((desc_chain_addr_low >> 6) & 0x80000000));
}

void rab_init_ib_dme(u8 controller, u8 engine_index, u64 dme_addr,
		u8 letter, u8 mbox, u8 xmbox)
{
	set_rab_ib_dme_ctrl(controller, engine_index, letter, mbox, xmbox);
	set_rab_ib_dme_addr(controller, engine_index, dme_addr);
}

/* Set Outbound DME TID Mask Register
 *
 * dest_id_mask (16 bits) :
 * 	This field provide mask of Dest-ID[15:0] for TID checking.
 * letter_mask (2 bits):
 *	This field provide mask of Letter[1:0] for TID checking.
 * mbox_mask (2 bits):
 *	This field provide mask of Mbox[1:0] for TID checking.
 * xmbox_masK (4 bits):
 * 	This field provide mask of Xmbox[3:0] for TID checking. It is
 *      ignored in the case of multi-segment DME.
 */
void set_rab_ob_dme_tidmask(u8 controller, u16 dest_id_mask,
		u8 letter_mask, u8 mbox_mask, u8 xmbox_mask)
{
	u32 tidmask;

	tidmask = (dest_id_mask & 0xffff)
		| ((letter_mask & 0x3) << 16)
		| ((mbox_mask & 0x3) << 18)
		| ((xmbox_mask & 0xf) << 20);
	rab_page_write(controller, RAB_OB_DME_TIDMSK, tidmask);
}

/* Set Outbound DME N Address Register
 * desc_chain_addr_low:
 *   Descriptor Chain Address - Lower
 *   Indicates the start address of the descriptor chain, aligned to 6 Words.
 *   In 32 bit AXI addressing, or local descriptor memory:
 *   Start Address = {RAB_OB_DMEN_DESC_ADDR[27:0], 4'h0}
 *   In 38 bit AXI addressing, or local descriptor memory:
 *   Start Address = {RAB_OB_DMEN_CTRL[31:30],
 *   RAB_OB_DMEN_DESC_ADDR[31:0], 4'h0}
 */
int set_rab_ob_dme_addr(u8 controller, u8 index, u64 desc_chain_addr_low)
{
	u32 tmp;

	if (desc_chain_addr_low & 0x1f)
		return -1;

	rab_page_write(controller, RAB_OB_DME_ADDR(index), desc_chain_addr_low >> 5);

	tmp = rab_page_read(controller, RAB_OB_DME_CTRL(index));
	rab_page_write(controller, RAB_OB_DME_CTRL(index), tmp | ((desc_chain_addr_low >> 6) & 0x80000000));

	return 0;
}

void rab_init_ob_dme(u8 controller, u8 engine_index, u64 dme_addr)
{
	set_rab_ob_dme_tidmask(controller, 0xffff, 0x3, 0x3, 0xf);
	set_rab_ob_dme_addr(controller, engine_index, dme_addr);
}

/*
 * Set Outbound DME N Control Register
 * buffer_full_threshold (2 bits)         : Buffer Full Threshold
 *                                          00: threshold is 15
 *                                          01: threshold is 15-2=13
 *                                          10: threshold is 15-4=11
 *                                          11: threshold is 15-6=9
 * desc_indirect_access_desc_sel (4 bits) : Descriptor Indirect Access Descript-
 *                                          or Select
 *                                          Selects Descriptor#0~#15 of the
 *                                          engine to be indirectly accessed in
 *                                          RAB_OB_DMEN_DESC register.
 * desc_indirect_access_word_sel (2 bits) : Descriptor Indirect Access Word
 *                                          Select
 *                                          Selects W0~W3 of the current
 *                                          descriptor to be indirectly accessed
 *                                          in RAB_OB_DMEN_DESC register.
 * desc_chain_addr_up (2 bits)            : Descriptor Chain Address - Upper
 *                                          Upper 2 bits of Descriptor Chain
 *                                          Address in 38 bit AXI Addressing.
 * Note: it will clear Engine Enable bit.
 */
void set_rab_ob_dme_ctrl(u8 controller, u8 index,
		u8 buffer_full_threshold,
		u8 desc_indirect_access_desc_sel,
		u8 desc_indirect_access_word_sel,
		u64 desc_chain_addr_up)
{
	u32 ctrl;

	ctrl = ((buffer_full_threshold & 0x3) << 4)
		| ((desc_indirect_access_desc_sel & 0xf) << 24)
		| ((desc_indirect_access_word_sel & 0x3) << 28)
		| ((desc_chain_addr_up & 0x3) << 30);

	rab_page_write(controller, RAB_OB_DME_CTRL(index), ctrl);
}

void enable_rab_ob_dme_engine(u8 controller, u8 index)
{
	u32 ctrl;

	ctrl = rab_page_read(controller, RAB_OB_DME_CTRL(index));
	rab_page_write(controller, RAB_OB_DME_CTRL(index), ctrl | 1);
}


void wakeup_rab_ob_dme_engine(u8 controller, u8 index)
{
	u32 ctrl;

	ctrl = rab_page_read(controller, RAB_OB_DME_CTRL(index));
	rab_page_write(controller, RAB_OB_DME_CTRL(index), ctrl | 0x2);
}

u32 get_rab_ob_dme_stat(u8 controller, u8 index)
{
	return rab_page_read(controller, RAB_OB_DME_STAT(index));
}

u32 get_rab_ib_dme_stat(u8 controller, u8 index)
{
	return rab_page_read(controller, RAB_IB_DME_STAT(index));
}

/* Get Outbound DME N Descriptor Register
 * This register is mainly for debug purpose
 */
u32 get_rab_ob_dme_desc(u8 controller, u8 index)
{
	return rab_page_read(controller, RAB_OB_DME_DESC(index));
}

/* Set Outbound DME N Descriptor Register
 * This register is mainly for debug purpose.
 */
void set_rab_ob_dme_desc(u8 controller, u8 index, u32 desc)
{
	rab_page_write(controller, RAB_OB_DME_DESC(index), desc);
}

void get_rab_ob_dme_tidmask(u8 controller,
		u32 *dest_id_mask, u8 *letter_mask,
		u8 *mbox_mask, u8 *xmbox_mask)
{
	u32 tidmask;

	tidmask = rab_page_read(controller, RAB_OB_DME_TIDMSK);
	*dest_id_mask = tidmask & 0xffff;
	*letter_mask = (tidmask >> 16) & 0x3;
	*mbox_mask = (tidmask >> 18) & 0x3;
	*xmbox_mask = (tidmask >> 20) & 0xf;
}

void enable_rab_ib_dme_engine(u8 controller, u8 index)
{
	u32 ctrl;

	ctrl = rab_page_read(controller, RAB_IB_DME_CTRL(index));
	rab_page_write(controller, RAB_IB_DME_CTRL(index), ctrl | 1);
}

void wakeup_rab_ib_dme_engine(u8 controller, u8 index)
{
	u32 ctrl;

	ctrl = rab_page_read(controller, RAB_IB_DME_CTRL(index));
	rab_page_write(controller, RAB_IB_DME_CTRL(index), ctrl | 0x2);
}



u32 get_rab_ib_dme_addr(u8 controller, u8 index)
{
	u32 addr;

	addr = rab_page_read(controller, RAB_IB_DME_ADDR(index));

	return addr << 5;
}

void set_rab_ib_dme_desc(u8 controller, u8 index, u32 desc)
{
	rab_page_write(controller, RAB_IB_DME_DESC(index), desc);
}

u32 get_rab_ib_dme_desc(u8 controller, u8 index)
{
	return rab_page_read(controller, RAB_IB_DME_DESC(index));
}



void set_rab_ibmsg_intr_router_i(u8 controller, u8 index, u32 route)
{
	rab_page_write(controller, RAB_IBMSG_INTR_ROUTE(index), route);
}


void set_rab_obmsg_intr_route_i(u8 controller, u8 index, u32 route)
{
	rab_page_write(controller, RAB_OBMSG_INTR_ROUTE(index), route);
}

void get_dme_desc_from_physical_address(u64 addr, void *desc)
{
	u32 *p = desc;

#if CACHE_EN
	p[0] = phx_read_cache_u32(addr + 0);
	p[1] = phx_read_cache_u32(addr + 4);
	p[2] = phx_read_cache_u32(addr + 8);
	p[3] = phx_read_cache_u32(addr + 12);
	p[4] = phx_read_cache_u32(addr + 16);
	p[5] = phx_read_cache_u32(addr + 20);
#else
	p[0] = phx_read_u32(addr + 0);
	p[1] = phx_read_u32(addr + 4);
	p[2] = phx_read_u32(addr + 8);
	p[3] = phx_read_u32(addr + 12);
	p[4] = phx_read_u32(addr + 16);
	p[5] = phx_read_u32(addr + 20);
#endif
}

void set_rab_intr_enab_odme(u8 controller, u32 odme_enab)
{
	rab_page_write(controller, RAB_INTR_ENAB_ODME, odme_enab);
}

void set_rab_intr_enab_idme(u8 controller, u32 idme_enab)
{
	rab_page_write(controller, RAB_INTR_ENAB_IDME, idme_enab);
}

void test_ib_data_msg(u8 controller, u8 dmsg_index,
		u64 desc_addr, u64 data_addr,
		u8 letter, u8 mbox, u8 xmbox)
{
	struct rab_ib_dme_desc ib_desc;

	rab_init_ib_dme(controller, dmsg_index, desc_addr,
			letter, mbox, xmbox);

	make_a_dmsg_receive_buf(&ib_desc, data_addr);
	ib_desc.end_of_chain = 1;
	copy_dme_desc_to_physical_address(desc_addr, &ib_desc);

	enable_rab_ib_dme_engine(controller, dmsg_index);
}

static void clear_data_buffer(u64 buf_addr, int size)
{
	int i, j;
	static int test_data = 0x0;

	for (i = 0; i < (size / 4); i++)
#if CACHE_EN
		phx_write_cache_u32(buf_addr + i * 4, i * 0x10 + 0x1100 + test_data);
#else
		phx_write_u32(buf_addr + i * 4, i * 0x10 + 0x1100 + test_data);
#endif

	for (j = 0; j < (size % 4); j++)
#if CACHE_EN
		phx_write_cache_u32(buf_addr + i * 4 + j * 4, i * 0x10 + j * 0x1 + 0x2200);
#else
		phx_write_u32(buf_addr + i * 4 + j * 4, i * 0x10 + j * 0x1 + 0x2200);
#endif
	//asm volatile("sync");

	test_data++;
}

void test_ob_data_msg(u8 controller, int data_len, u16 dest_id)
{
	u64 ob_desc_head = RAB_OB_DME_DESC_START(controller);
	u64 ob_data_head = RAB_OB_DME_BUF_START(controller);
	int i, j, ob_index;

#if DM_DEBUG_INFO
	uart_printf("OB data Message desc_addr:0x%x  data_addr:0x%x \r\n", ob_desc_head, ob_data_head);
#endif
	for (ob_index = 0 ; ob_index < 32;) {
		for (i = 0; i < 4; i++) {
			for (j = 0; j < 4; j++) {
				if (ob_index % 3 == 0) {
					ob_desc_head = RAB_OB_DME_DESC_START(controller);
					ob_data_head = RAB_OB_DME_BUF_START(controller);
				}
				rab_init_ob_dme(controller, ob_index % 3, ob_desc_head);
				clear_data_buffer(ob_data_head, data_len);
				fill_ob_data_message(controller, &ob_desc_head, dest_id,
					i, /* letter */
					j, /* mbox */
					0, /* xmbox */
					&ob_data_head,
					data_len /* 1 DW */);
				enable_rab_ob_dme_engine(controller, ob_index % 3);
				ob_index++;
			}
		}
	}
}

void test_i_msg(u8 controller)
{
	int i, j;
	int engine_index = 0;

	u64 desc_addr = RAB_IB_DME_DESC_START(controller);
	u64 data_addr = RAB_IB_DME_BUF_START(controller);
	int len_per_desc = 32;

	set_rab_intr_enab_idme(controller, 0xffffffff);

	for (engine_index = 0; engine_index < 32;) {
		for (i = 0; i < 4; i++) {
			for (j = 0; j < 4; j++) {
				test_ib_data_msg(controller, engine_index, desc_addr, data_addr, i, j, 0);
				desc_addr += len_per_desc;
				data_addr += DME_BUFFER_SIZE;
				engine_index++;
			}
		}
	}
}

void test_o_msg(u8 controller, u16 destID)
{
	set_rab_intr_enab_odme(controller, 0xf);

	test_ob_data_msg(controller, 16, destID);

#if DM_DEBUG_INFO
	u32 stat, ctrl;
	int i;

	for (i = 0; i < 3; i++) {
		stat = rab_page_read(controller, RAB_OB_DME_STAT(i));
		uart_printf("rab%d ob dme%d stat = 0x%x \r\n", controller, i, stat);
		ctrl = rab_page_read(controller, RAB_OB_DME_CTRL(i));
		uart_printf("rab%d ob dme%d ctrl = 0x%x \r\n", controller, i, ctrl);
	}
#endif
}

void check_dme_test_result()
{
	u32 stat;
	u32 err_cnt = 0;
	u32 val0, val1, val2, val3, val4;
	u32 test_data = 0;
	int i;

	for (i = 0; i < 32; i++) {
#if CACHE_EN
		val0 =  phx_read_cache_u32(RAB_IB_DME_BUF_START(1) + i * DME_BUFFER_SIZE);
		val1 =  phx_read_cache_u32(RAB_IB_DME_BUF_START(1) + i * DME_BUFFER_SIZE + 4);
		val2 =  phx_read_cache_u32(RAB_IB_DME_BUF_START(1) + i * DME_BUFFER_SIZE + 8);
		val3 =  phx_read_cache_u32(RAB_IB_DME_BUF_START(1) + i * DME_BUFFER_SIZE + 12);
		val4 =  phx_read_cache_u32(RAB_IB_DME_BUF_START(1) + i * DME_BUFFER_SIZE + 16);
#else
		val0 =  phx_read_u32(RAB_IB_DME_BUF_START(1) + i * DME_BUFFER_SIZE);
		val1 =  phx_read_u32(RAB_IB_DME_BUF_START(1) + i * DME_BUFFER_SIZE + 4);
		val2 =  phx_read_u32(RAB_IB_DME_BUF_START(1) + i * DME_BUFFER_SIZE + 8);
		val3 =  phx_read_u32(RAB_IB_DME_BUF_START(1) + i * DME_BUFFER_SIZE + 12);
		val4 =  phx_read_u32(RAB_IB_DME_BUF_START(1) + i * DME_BUFFER_SIZE + 16);
#endif
#if DM_DEBUG_INFO
		stat = rab_page_read(1, RAB_IB_DME_STAT(i));
		uart_printf("rab1 dme%d IB_BUF val = 0x%x 0x%x 0x%x 0x%x 0x%x stat = 0x%x\r\n", i, val0, val1, val2, val3, val4, stat);
#endif
		// each ob dme sends 16 bytes
		if (val0 != (0x1100 + 0 * 0x10 + test_data))
			err_cnt++;
		if (val1 != (0x1100 + 1 * 0x10 + test_data))
			err_cnt++;
		if (val2 != (0x1100 + 2 * 0x10 + test_data))
			err_cnt++;
		if (val3 != (0x1100 + 3 * 0x10 + test_data++))
			err_cnt++;
	}

	for (i = 0; i < 32; i++) {
#if CACHE_EN
		val0 =  phx_read_cache_u32(RAB_IB_DME_BUF_START(0) + i * DME_BUFFER_SIZE);
		val1 =  phx_read_cache_u32(RAB_IB_DME_BUF_START(0) + i * DME_BUFFER_SIZE + 4);
		val2 =  phx_read_cache_u32(RAB_IB_DME_BUF_START(0) + i * DME_BUFFER_SIZE + 8);
		val3 =  phx_read_cache_u32(RAB_IB_DME_BUF_START(0) + i * DME_BUFFER_SIZE + 12);
		val4 =  phx_read_cache_u32(RAB_IB_DME_BUF_START(0) + i * DME_BUFFER_SIZE + 16);
#else
		val0 =  phx_read_u32(RAB_IB_DME_BUF_START(0) + i * DME_BUFFER_SIZE);
		val1 =  phx_read_u32(RAB_IB_DME_BUF_START(0) + i * DME_BUFFER_SIZE + 4);
		val2 =  phx_read_u32(RAB_IB_DME_BUF_START(0) + i * DME_BUFFER_SIZE + 8);
		val3 =  phx_read_u32(RAB_IB_DME_BUF_START(0) + i * DME_BUFFER_SIZE + 12);
		val4 =  phx_read_u32(RAB_IB_DME_BUF_START(0) + i * DME_BUFFER_SIZE + 16);
#endif
#if DM_DEBUG_INFO
		stat = rab_page_read(0, RAB_IB_DME_STAT(i));
		uart_printf("rab0 dme%d IB_BUF val = 0x%x 0x%x 0x%x 0x%x 0x%x stat = 0x%x\r\n", i, val0, val1, val2, val3, val4, stat);
#endif
		// each ob dme sends 16 bytes
		if (val0 != (0x1100 + 0 * 0x10 + test_data))
			err_cnt++;
		if (val1 != (0x1100 + 1 * 0x10 + test_data))
			err_cnt++;
		if (val2 != (0x1100 + 2 * 0x10 + test_data))
			err_cnt++;
		if (val3 != (0x1100 + 3 * 0x10 + test_data++))
			err_cnt++;
	}

	if (err_cnt)
		uart_printf("========================= dme err! err_cnt = 0x%x\r\n", err_cnt);
	else
		uart_printf("========================= dme pass \r\n");
}
