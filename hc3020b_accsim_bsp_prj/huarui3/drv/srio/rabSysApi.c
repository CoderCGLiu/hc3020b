#include "rab.h"
#include "srio.h"
#include "rab_amap.h"
#include "rab_dma.h"
#include "rab_int.h"
#include "rabSysApi.h"
//#include "semLibCommon.h"
//#include "taskLibCommon.h"
#include <vxworks.h>
#include <sioLib.h>
#include <taskLib.h>
#include <msgQLib.h>
#include <mqueue.h>
#include <semLib.h>
#include <irq.h>
#include <semaphore.h>
#include <pthread.h>
#include <fcntl.h>

extern const int hr3_board_type;

static u16 HR_DB_INIT[2] = {0, 0};
static u16 HR_DB_STATUS[2][RAB_DB_ENGINE_MAX];      /*0:表示空闲；1:表示使用中*/

#if _RIO_SEM_REWORKS_
sem_t HR_DB_SendAccessSem[2];
#else
SEM_ID HR_DB_SendAccessSem[2];
#endif

#if HR_RAB_DB_SEM
#if _RIO_SEM_REWORKS_
sem_t HR_RAB_DB_SendSem[2];
sem_t HR_RAB_DB_RecvSem[2];
#else
SEM_ID HR_RAB_DB_SendSem[2];
SEM_ID HR_RAB_DB_RecvSem[2];
#endif /*_RIO_SEM_REWORKS_*/
#endif /*HR_RAB_DB_SEM*/



MSG_Q_ID   g_RioDBellRxMsgQ[2];
//mqd_t  g_RioDBellRxMsgQ[2];

int (*hrRioDBRecv[2])(u8 controller) = {NULL, NULL};/*门铃接收处理函数*/

#if _RIO_SEM_REWORKS_
extern sem_t HR_RAB_DMA_WSem[2][RAB_DMA_MAX_ENGINE];
extern sem_t HR_RAB_DMA_RSem[2][RAB_DMA_MAX_ENGINE];
extern sem_t HR_WDMA_SendAccessSem[2];
extern sem_t HR_RDMA_SendAccessSem[2];
#else
extern SEM_ID HR_RAB_DMA_WSem[2][RAB_DMA_MAX_ENGINE];
extern SEM_ID HR_RAB_DMA_RSem[2][RAB_DMA_MAX_ENGINE];
extern SEM_ID HR_WDMA_SendAccessSem[2];
extern SEM_ID HR_RDMA_SendAccessSem[2];
#endif /*_RIO_SEM_REWORKS_*/

extern u16 HR_WDMA_STATUS[2][RAB_DMA_MAX_ENGINE];      /* 0:表示空闲；1:表示使用中 */
extern u16 HR_RDMA_STATUS[2][RAB_DMA_MAX_ENGINE];      /* 0:表示空闲；1:表示使用中 */

/*配置其它寄存器的通道解锁,并release reset: pcie 0/1, RAB 0/1*/
void hrSrioRegionRelease(void)
{/*TODO: release_rst()*/
    u32 tmp;

    /*配置其它寄存器的通道解锁*/
    phx_write_u32(0x1f0c0028, 0xa5accede); // unlock
	/*rapidio 0/1 和 pcie_phy 0/1配置release reset*/
	tmp = phx_read_u32(0x1f0c0004);
    phx_write_u32(0x1f0c0004, tmp & 0xfe7fffff);
	tmp = phx_read_u32(0x1f0c0008);
    phx_write_u32(0x1f0c0008, tmp & 0xfffff9ff);
	return;
}


/*static*/ void hrSrioIntConnect (u8 controller)
{
    if (controller == SRIO0) {
//        API_InterVectorConnect(67, (PINT_SVR_ROUTINE)rab0_int_do, (PVOID)NULL, "hr3srio0_isr");
//        API_InterVectorEnable(67);
    	int_install_handler("hr3srio0_isr", 67,  1, (INT_HANDLER)rab0_int_do, (void*)NULL);
    	int_enable_pic(67);
    } else if (controller == SRIO1) {
//        API_InterVectorConnect(71, (PINT_SVR_ROUTINE)rab1_int_do, (PVOID)NULL, "hr3srio1_isr");
//        API_InterVectorEnable(71);
    	int_install_handler("hr3srio1_isr", 71,  1, (INT_HANDLER)rab1_int_do, (void*)NULL);
    	int_enable_pic(71);
    }
}

#if 0
void hrSrioInit(u8 controller)
{
	u32 tmp = 0;
			
    rioLockInit(controller);
    
	//向0x1f0c0028寄存器写0xA5ACCEDE时，配置其它寄存器的通道解锁，写其他值会锁住其它寄存器配置通道。
	phx_write_u32(0x1f0c0028, 0xa5accede); 
	tmp = phx_read_u32(0x1f0c0008);
	phx_write_u32(0x1f0c0008, tmp & (~0x0c300000));

    rab_phy_link_ini_config_5G(controller);
//    rab_phy_link_ini_config_10G3125(controller);/*ldf 20230404*/
    bslRioSetID(controller, 0xff); // default RAB ID
//    rab_wait_for_linkup(controller);

//    hrSrioIntConnect(controller);
    rab_int_Init(controller);

    set_rab_rpio_ctrl(controller, 0, 0x1);
    set_rab_apio_ctrl(controller, 0, 0x7);

    // 防止重复初始化导致内存泄漏
    rab_dma_malloc_desc(controller);
}
#else /*ldf 20230405, HR3 SRIO初始化*/
static u8 flag_hrSrioInit[2] = {0,0};
void hrSrioInit(u8 controller)
{
	if(controller >= 2)
    {
    	uart_printf("<ERROR> [%s:__%d__]:: controller must be <= 1.\n",__FUNCTION__,__LINE__);
        return;
    }
	
    if(flag_hrSrioInit[controller] != 0)
    {
    	uart_printf("<WARNNIG> [%s:__%d__]:: The controller %u has been initialized.\n",__FUNCTION__,__LINE__,controller);
        return;
    }
    /*初始化实时确定性自旋锁 */
	rioLockInit(controller);

	/*配置其它寄存器的通道解锁,并release reset: pcie 0/1, RAB 0/1*/
	hrSrioRegionRelease();
#if 0
	// flush l3 to ddr -------------------------
	phx_write_u32(0x1e0c0080, 0x4);
	phx_write_u32(0x1e0c0010, 0x0);
	phx_write_u32(0x1e0c1080, 0x4);
	phx_write_u32(0x1e0c1010, 0x0);
	phx_write_u32(0x1e0c2080, 0x4);
	phx_write_u32(0x1e0c2010, 0x0);
	phx_write_u32(0x1e0c3080, 0x4);
	phx_write_u32(0x1e0c3010, 0x0);
#endif

	/*初始化物理层*/
#if 0
    switch(controller)
    {
        case 0:
            rab_phy_link_ini_config_6G25(controller);
            break;
        case 1:
        	rab_phy_link_ini_config_10G3125(controller);
            break;
        default :
            rab_phy_link_ini_config_6G25(controller);
            break;    	
    }
#else
//    rab_phy_link_ini_config_10G3125(controller);
    rab_phy_link_ini_config_12G5(controller);
//    rab_phy_link_ini_config_5G(controller);
#endif
	
	/*设置默认的RAB ID为0xff*/
	bslRioSetID(controller, 0xff); 

	/*等待RAB连接*/
//	rab_wait_for_linkup(controller);

	/*RAB中断初始化*/
	rab_int_Init(controller);
	
    set_rab_rpio_ctrl(controller, 0, 0x1);
    set_rab_apio_ctrl(controller, 0, 0x7);

    // 防止重复初始化导致内存泄漏
    rab_dma_malloc_desc(controller);
	
	flag_hrSrioInit[controller] = 1;
	                
	uart_printf("srio init done\r\n");

	return;
}
#endif



/*
功能：获取当前RapidIO设备的ID号
参数：
u8 controller:控制器号，(0-1)
返回值：返回当前RapidIO的ID号
若为0xff,则当前RapidIO设备没有初始化成功
*/
u16 bslRioGetID(u8 controller)
{
    u32 rio_id;

    if (controller >= 2)
    {
        uart_printf("!!!Error, bslRioGetID:controller index error! %d\r\n", controller);
        return 0xff;
    }

    rio_id = rab_page_read(controller, GRIO_BDIDCSR);

    return (u16)(rio_id & 0xffff);
}

/*
功能：设置当前RapidIO设备的ID号
参数：
u8 controller:控制器号，(0-1)
u16 id:设置的rapidio  id号
返回值：
*/
u16 bslRioSetID(u8 controller, u16 id)
{
    u32 rio_id;

    if (controller >= 2)
    {
        uart_printf("!!!Error, bslRioSetID:controller index error! %d\r\n", controller);
        return 0xff;
    }

    rio_id = (u32)id;

    rab_page_write(controller, GRIO_BDIDCSR, rio_id);

    return 0;
}


u8 bslRioGetDBNum(u8 controller)
{
    return get_ib_db_msg_number(controller);
}

void bslRioGetDBInfo(u8 controller,u16 *dbInfo, u16 *sourceID)
{
    get_ib_db_info(controller, dbInfo, sourceID);
}

static int dbRecvThread(u8 controller)
{
    u16 _bRun;

    _bRun = 1;
    while (_bRun)
    {
#if HR_RAB_DB_SEM 
#if _RIO_SEM_REWORKS_
    	sem_wait2(&HR_RAB_DB_RecvSem[controller], WAIT, NO_TIMEOUT);
#else
        semTake(HR_RAB_DB_RecvSem[controller], WAIT_FOREVER);
#endif /*_RIO_SEM_REWORKS_*/
#endif /*HR_RAB_DB_SEM*/  
    	if(hrRioDBRecv[controller] != NULL)
        {
//            LW_SOFUNC_PREPARE(hrRioDBRecv[controller]);
           (hrRioDBRecv[controller])(controller);
        }
    }

    return 0;
}

void bslUsrRioDBRecvQuick(u8 controller, RapidIO_Recv *_param)
{
	msgQReceive(g_RioDBellRxMsgQ[controller], (char*)_param, 8, WAIT_FOREVER);
//	mq_receive(g_RioDBellRxMsgQ[controller],(char*)_param, sizeof(RapidIO_Recv), 0);
}

int RabDBFun(u8 controller)
{
    int i;
    u8 _dbNum;
    u16 _dbInfo,_sourceID;
    RapidIO_Recv _param;

    _dbNum = bslRioGetDBNum(controller);

    //printk("RabDBFun dbNum = %d\n", _dbNum);

    for( i=0; i<_dbNum; i++)
    {
        bslRioGetDBInfo(controller,&_dbInfo,&_sourceID);
        _param.controller = controller;
        _param.srcID = _sourceID;
        _param.dbInfo = _dbInfo;

        //g_rabDBRecvNumCounts[controller]++;
        msgQSend(g_RioDBellRxMsgQ[controller],(char*)&_param,8,NO_WAIT,MSG_PRI_URGENT);
//        mq_send(g_RioDBellRxMsgQ[controller],(char*)&_param,sizeof(RapidIO_Recv),0);
    }
    return 0;
}

void bslRioDBInit(u8 controller, int (*dbFuc)(u8))
{
    char _dbTaskName[100];
    pthread_t  _recvDBThreadID;

    if (HR_DB_INIT[controller])
        return;

    /*使能入站门铃*/
    enable_ib_db(controller);
#if HR_RAB_DB_SEM 
    /*初始化接收门铃的二进制信号量，先进先出， 初始值为0*/
#if _RIO_SEM_REWORKS_
    sem_init2(&HR_RAB_DB_RecvSem[controller], 0, SEM_BINARY, PTHREAD_WAITQ_FIFO, 0);
#else
    HR_RAB_DB_RecvSem[controller] = semBCreate(SEM_Q_FIFO, SEM_EMPTY);
#endif /*_RIO_SEM_REWORKS_*/
    if ((void*)HR_RAB_DB_RecvSem[controller] == NULL)
    {
        uart_printf("!!!Error, bslRioDBInit:create db_RecvSem error!\n");
        return;
    }
    /*初始化发送门铃的二进制信号量，先进先出， 初始值为0*/
#if _RIO_SEM_REWORKS_
    sem_init2(&HR_RAB_DB_SendSem[controller], 0, SEM_BINARY, PTHREAD_WAITQ_FIFO, 0);
#else
    HR_RAB_DB_SendSem[controller] = semBCreate(SEM_Q_FIFO, SEM_EMPTY);
#endif /*_RIO_SEM_REWORKS_*/
    if ((void*)HR_RAB_DB_SendSem[controller] == NULL)
    {
        uart_printf("!!!Error, bslRioDBInit:create db_SendSem error!\n");
        return;
    }
#endif /*HR_RAB_DB_SEM*/
    
#if _RIO_SEM_REWORKS_
    sem_init2(&HR_DB_SendAccessSem[controller], 0, SEM_BINARY, PTHREAD_WAITQ_FIFO, 1);
#else
    HR_DB_SendAccessSem[controller] = semBCreate(SEM_Q_FIFO, SEM_FULL);
#endif /*_RIO_SEM_REWORKS_*/
    if(HR_DB_SendAccessSem[controller] == NULL )
    {
    	uart_printf("!!!Error, bslRioDBInit:create HR_DB_SendAccessSem error!\n");
        return;
    }
    
    g_RioDBellRxMsgQ[controller] = msgQCreate(1024,8,MSG_Q_FIFO);
//    g_RioDBellRxMsgQ[controller] = mq_create(0, 4096, sizeof(RapidIO_Recv), PTHREAD_WAITQ_FIFO);
    if(!(g_RioDBellRxMsgQ[controller]))
    {
        uart_printf("!!!Error, bslRioDBInit:create g_RioDBellRxMsgQ error!\n");
        return;
    }
    
    /*创建门铃接收处理任务*/
    if (dbFuc != NULL)
        hrRioDBRecv[controller] = dbFuc;
    else
        hrRioDBRecv[controller] = RabDBFun;

	sprintf(_dbTaskName, "TaskDBRecv%d", controller);
	/*craete recv db thread*/
//        _recvDBThreadID = taskSpawn(_dbTaskName, 51, VX_FP_TASK, 200000,
//            (FUNCPTR)dbRecvThread, controller, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        if(pthread_create2(&_recvDBThreadID, _dbTaskName, 51, RE_FP_TASK,\
        		200000, dbRecvThread, (void*)controller) != 0) /*ldf 20230405*/
//        if (_recvDBThreadID == ERROR)
        {
            hrRioDBRecv[controller] = NULL;
            uart_printf("!!!Error, bslRioDBInit: fail to create recv doorbell thread!\n");
            return;
        }
    HR_DB_INIT[controller] = 1;
    }

u8 FindFreeDBIndex(u8 controller)
{
    u8 _dbIndex = 0xff;
    int i;

    for(i=0;i<RAB_DB_ENGINE_MAX;i++)
    {
        if( HR_DB_STATUS[controller][i] == 0 )
        {
            _dbIndex = i;
#if _RIO_SEM_REWORKS_
            sem_wait2(&HR_DB_SendAccessSem[controller], WAIT, NO_TIMEOUT);
#else
            semTake(HR_DB_SendAccessSem[controller],WAIT_FOREVER);
#endif /*_RIO_SEM_REWORKS_*/
            
            HR_DB_STATUS[controller][i] = 1;
            
#if _RIO_SEM_REWORKS_
            sem_post(&HR_DB_SendAccessSem[controller]);
#else
            semGive(HR_DB_SendAccessSem[controller]);
#endif /*_RIO_SEM_REWORKS_*/
            break;
        }
    }

    return _dbIndex;
}

u8 freeDBIndex(u8 controller, u16 dbEngeindex)
{
    if(controller > 2 || dbEngeindex >= RAB_DB_ENGINE_MAX)
    {
        printk("param error controller %d, dbEngeindex %d\n", controller, dbEngeindex);
        return -1;
    }
#if _RIO_SEM_REWORKS_
    sem_wait2(&HR_DB_SendAccessSem[controller], WAIT, NO_TIMEOUT);
#else
    semTake(HR_DB_SendAccessSem[controller],WAIT_FOREVER);
#endif /*_RIO_SEM_REWORKS_*/
    
    HR_DB_STATUS[controller][dbEngeindex] = 0;
    
#if _RIO_SEM_REWORKS_
    sem_post(&HR_DB_SendAccessSem[controller]);
#else
    semGive(HR_DB_SendAccessSem[controller]);
#endif /*_RIO_SEM_REWORKS_*/

    return 0;
}


/*
功能：发送门铃
参数：
u8 controller:控制器号，(0-1)
u16 targetID：门铃发送目的设备的ID号
u16 dbInfo：门铃的信息内容
返回值:0表示成功;-1表示失败
*/
#if _TEST_SRIO_DBSndTime_
unsigned long long t0, t1, t2, t3, t4, t5;/*ldf 20230523 add:: 发送门铃耗时打印*/
#endif

int bslRioSendDB(u8 controller, u16 targetID, u16 dbInfo)
{
    u8 _dbIndex = 0x0; // 0 ~ 14
    u32 _timeOut = 6;
    e_trans_msg_status _status;

    _dbIndex = FindFreeDBIndex(controller);
    if( _dbIndex >= RAB_DB_ENGINE_MAX )
    {
    	uart_printf("!!!error!can not find free db index!\n");
        //addRioCntErr(controller, targetID);
        return -1;
    }
#if _TEST_SRIO_DBSndTime_
    t0 = bslGetTimeUsec();
#endif
    /*设置出站门铃控制寄存器*/
    set_ob_db_csr(controller, _dbIndex, 0, 0, 0, 0, targetID);
    /*设置出站门铃消息寄存器*/
    set_ob_db_info(controller, _dbIndex, dbInfo);
    /*传输门铃消息*/
    start_trans_ob_db_msg(controller, _dbIndex);
#if _TEST_SRIO_DBSndTime_
    t1 = bslGetTimeUsec();
#endif /*_TEST_SRIO_DBSndTime_*/
#if HR_RAB_DB_SEM
#if _RIO_SEM_REWORKS_
    if (sem_wait2(&HR_RAB_DB_SendSem[controller], WAIT, _timeOut) == ERROR)
#else
    if (semTake(HR_RAB_DB_SendSem[controller], _timeOut) == ERROR)
#endif /*_RIO_SEM_REWORKS_*/
    {
        uart_printf("!!!Error! bslRioSendDB::门铃发送失败, controller = %d, dbIndex = %d dstID = 0x%x\n", controller, _dbIndex, targetID);
#if 0
#if _RIO_SEM_REWORKS_
        sem_wait2(&HR_DB_SendAccessSem[controller], WAIT, NO_TIMEOUT);
#else
        semTake(HR_DB_SendAccessSem[controller], WAIT_FOREVER);
#endif /*_RIO_SEM_REWORKS_*/
        
        HR_DB_STATUS[controller][_dbIndex] = 0;
        
#if _RIO_SEM_REWORKS_
        sem_post(&HR_DB_SendAccessSem[controller]);
#else
        semGive(HR_DB_SendAccessSem[controller]);
#endif /*_RIO_SEM_REWORKS_*/
#endif
        
#if _RIO_SEM_REWORKS_
        if (sem_wait2(&HR_RAB_DB_SendSem[controller], WAIT, taskDelay(sysClkRateGet() * 6)) == ERROR)//延迟6秒 
#else
        if (semTake(HR_RAB_DB_SendSem[controller], taskDelay(sysClkRateGet() * 6)) == ERROR)//延迟6秒  
#endif /*_RIO_SEM_REWORKS_*/
        {
			freeDBIndex(controller, _dbIndex);
			return -1;
        }
    }
    else
#endif /*HR_RAB_DB_SEM*/
    {
#if _TEST_SRIO_DBSndTime_
    	t4 = bslGetTimeUsec();
#endif /*_TEST_SRIO_DBSndTime_*/
        semTake(HR_DB_SendAccessSem[controller], WAIT_FOREVER);
        HR_DB_STATUS[controller][_dbIndex] = 0;
        semGive(HR_DB_SendAccessSem[controller]);
    	freeDBIndex(controller, _dbIndex);
    }  	 
#if _TEST_SRIO_DBSndTime_
    	t5 = bslGetTimeUsec();
    	printk("<***DEBUG***> [%s:_%d_]:: t0 = %u, t1 = %u, t2 = %u, t3 = %u, t4 = %u, t5 = %u\n", 
    			__FUNCTION__,__LINE__,t0, t1, t2, t3, t4, t5);
#endif /*_TEST_SRIO_DBSndTime_*/
	/*TODO: ldf 20230405, 检查门铃消息发送状态*/
//	_status = get_trams_ob_db_msg_status(controller, _dbIndex);
//	uart_printf("<INFO> get_trams_ob_db_msg_status:: controller %u, _dbIndex=0x%X, _status=0x%X\n",controller,_dbIndex,_status);

    return 0;
}



/*
功能：映射Maintenance OUTBOUND WINDOWS，
即：把本地发送的AXI地址映射成维护包发送窗口
参数：
u8 controller:控制器号，(0-1)
修改RAB_APIO_MAINTENANCE_BASE：发送方本地的AXI地址
默认将0窗口配置成维护包窗口
*/

/* 映射的窗口大小,最小1k;默认1M */
void bslRioSetMaintOW(u8 controller)
{
    u8 _hopcount = 0xff;
    u16 _destID = 0xff;
    u32 _maintSize = 0x100000;
    u8 index = 0;

    rio_set_outbound_win(controller, index, _destID, /*RAB_OW_MAINT_BASE_ADDR*/RAB_OW_HIGH_BASE_ADDR(controller), _hopcount << 24, _maintSize, RAB_APIO_MAINTENANCE);
}

/*
  * 维护写操作
  * 对于AXI寄存器，是小端模式不需要进行高低字节倒序>=0x20000
*/
void bslRioMaintWrite(u8 controller, u16 dstId, u8 hopcount, u32 offset, u32 writedata)
{
    u32 _data;
    u8 enable = 1;
    u8 index = 0;

    set_rab_apio_amap_rbase(controller, index, hopcount << 14);
    set_rab_apio_amap_ctrl(controller, index, enable, RAB_APIO_MAINTENANCE, 0x0, 0x0, 0x0, dstId);


    if (offset == 0x60)
    {
        _data = writedata | (writedata << 16);
        _data = SWAP_U32(_data);
    }
    else
    {
        _data = SWAP_U32(writedata);
    }

    phx_write_u32(/*RAB_OW_MAINT_BASE_ADDR*/RAB_OW_HIGH_BASE_ADDR(controller) + offset, _data);
}

/*
  * 维护读操作
  * 对于AXI寄存器，是小端模式不需要进行高低字节倒序>=0x20000
*/
void bslRioMaintRead(u8 controller, u16 dstId, u8 hopcount, u32 offset, void  *rddata)
{
    u32 _data;
    u8 enable = 1;
    u8 index = 0;

    set_rab_apio_amap_rbase(controller, index, hopcount << 14);
    set_rab_apio_amap_ctrl(controller, index, enable, RAB_APIO_MAINTENANCE, 0x0, 0x0, 0x0, dstId);

    _data = phx_read_u32(/*RAB_OW_MAINT_BASE_ADDR*/RAB_OW_HIGH_BASE_ADDR(controller) + offset);

    *(u32 *)rddata = SWAP_U32(_data);
}


/*
功能：映射OUTBOUND WINDOWS，
即：把本地发送的AXI地址映射到接收的RapidIO地址上。
参数：
u8 controller:控制器号，(0-1)
u8 index：OUTBOUND 窗口号，（1---15）
u8 targetId：需要发送的目标id。
u64 axiSendAddr：发送方本地的RapidIO发送地址（AXI本地地址）

u32 rioRecvAddr：对应的RapidIO地址,必须1M字节对齐
即接收方的映射好的RapidIO INBOUND窗口地址.
u16 windowSize：映射的窗口大小1K字节对齐
u8 type:窗口模式NREAD_NWRITE = 1,   NREAD_NWRITE_R = 2,  NREAD_SWRITE = 3
*/
void bslRioSetOW(u8 controller, u8 index, u16 targetId, u64 axiSendAddr,
    u32 rioRecvAddr, u32 windowSize, u8 type)
{
    if ((type != 1) && (type != 2) && (type != 3))
    {
        uart_printf("!!!Error, bslRioSetOW::OW type error! type = %d\n", type);
        return;
    }
    if ((index == 0) || (index >= 16))
    {
        uart_printf("!!!Error, bslRioSetOW::OW index error! index = %d\n", index);
        return;
    }
    if (windowSize % 1024 != 0)
    {
        uart_printf("!!!Error! windowSize not aligned with the bytes 1024. windowSize = 0x%x\n", windowSize);
        return;
    }
    if ((axiSendAddr >= RAB_OW_LOW_BASE_ADDR(controller) && (axiSendAddr+windowSize) < (RAB_OW_LOW_BASE_ADDR(controller)+RAB_OW_LOW_SIZE)) ||
            (axiSendAddr >= RAB_OW_HIGH_BASE_ADDR(controller) && (axiSendAddr+windowSize) < (RAB_OW_HIGH_BASE_ADDR(controller)+RAB_OW_HIGH_SIZE)))
    {
        rio_set_outbound_win(controller, index, targetId, axiSendAddr, rioRecvAddr, windowSize, type);
    }
    else
    {
        uart_printf("!!!Error!axiSendAddr = 0x%x param error.\n", axiSendAddr);
        return;
    }
}

/*
功能：映射INBOUND WINDOWS，
即：将Rapidio 地址映射到LOCAL MEMORY。
参数：
u8 controller:控制器号，(0-1)
u32 rioAddr：需1M字节对齐
u64 memPhyAddr：映射目的LOCAL MEMORY 地址
                要求映射大小是多少就多少字节对齐
                要求是物理地址
u8 windowSize：映射的窗口大小(0-11)
对于34位rapidio地址只能用到8(256M)
后面三个在50位地址时可用
    0- 1 M  (0x100000)
    1- 2 M  (0x200000)
    2- 4 M  (0x400000)
    3- 8 M  (0x800000)
    4- 16M  (0x1000000)
    5- 32M  (0x2000000)
    6- 64M  (0x4000000)
    7- 128M (0x8000000)
    8- 256M (0x10000000)
    9- 512M (0x20000000)
    10 -1G  (0x40000000)
    11 -2G  (0x80000000)
*/
int bslRioSetIW(u8 controller, u32 rioAddr, u64 memPhyAddr, u32 windowSize)
{
//    if (windowSize > win_size_table[12])
//    {
//        uart_printf("!!!Error, bslRioSetIW::param windowSize %d set error!\r\n", windowSize);
//        return -1;
//    }
//    else
//    {
//        if (windowSize >= win_size_table[9])
//        {
//            uart_printf("!!!Error,bslRioSetIW::rio addr is not 50bit, windowSize set error!\r\n");
//            return -1;
//        }
//    }
//
//    if ((rioAddr & 0xfffff) != 0)
//    {
//        uart_printf("!!!Error,bslRioSetIW::rioAddr not aligned with the 1M bytes!\r\n");
//        return -1;
//    }
//
//    if ((memPhyAddr & ((1 << (20 + windowSize)) - 1)) != 0)
//    {
//        uart_printf("!!!Error,bslRioSetIW::localMemAddr not aligned with the byte 0x%x.\r\n", 1 << (20 + windowSize));
//        return -1;
//    }

    rio_set_inbound_win(controller, rioAddr, memPhyAddr, windowSize);

    return 0;
}

/*
 * rapidio发送接口,该接口为阻塞接口
 * controller:控制器号，(0-1)
 * dmaIndex:dma的索引号(0-7)
 * memAddr: 虚拟地址，dma需要转换成物理地址
 * 对齐要求至少4字节对齐
 * rioAddr:Rio地址:对齐要求至少4字节对齐
 * transferLen:传输长度:长度要求至少4字节整数倍
 * destID:目的id
 * 成功返回0，失败返回-1
*/
#if _TEST_SRIO_DMASndTime_
unsigned long long tt0, tt1, tt2, tt3, tt4, tt5, tt6, tt7;/*ldf 20230523 add:: DMA发送耗时打印*/
unsigned int wdma_count = 0;/*ldf 20230526 add:: DmaWrite进入次数*/
extern unsigned int wdma_int_count;/*ldf 20230526 add:: 进WDMA中断次数*/
#endif /*_TEST_SRIO_DMASndTime_*/

int bslRioDmaWrite(u8 controller, u64 memAddr, u32 rioAddr, u32 transferLen, u16 destID)
{
#if _TEST_SRIO_DMASndTime_
	wdma_count++;
	if((wdma_count % _TEST_DMA_PRINT_CYCLE_) == 0)
	{
		tt1=tt2=tt3=tt4=tt5=tt6=tt7=wdma_count;
    	tt0 = bslGetTimeUsec();
	}
//	printk("<***DEBUG***> [%s:_%d_]:: [CPU%d] [第%u次wdma]  Start\n",__FUNCTION__,__LINE__,cpu_id_get(),wdma_count);
#endif /*_TEST_SRIO_DMASndTime_*/
    u64 _phyAddr;
    u8 _wdmaIndex;
    u32 _timeout = sysClkRateGet() * 5;
    
//    printk("<DEBUG> [%s:__%d__]:; _timeout=%u\n",__FUNCTION__,__LINE__,_timeout);
   
    if (transferLen > RAB_DMA_MAX_TRANSLEN)
    {
        printk("<ERROR> [%s:__%d__]:: DMA transfer length must be <= 256M bytes.\n",__FUNCTION__,__LINE__);
        return -1;
    }
    	
//    printk("<DEBUG> [%s:__%d__]\n",__FUNCTION__,__LINE__);
    /*寻找空闲DMA描述符index*/
    _wdmaIndex = FindFreeDMAIndex(controller, RAB_DMA_WDMA);
//    printk("<DEBUG> [%s:__%d__]:: srio%d  _wdmaIndex = 0x%x\n",__FUNCTION__,__LINE__,controller,_wdmaIndex);
    if (_wdmaIndex >= RAB_DMA_MAX_ENGINE)
    {
        printk("<ERROR> [%s:__%d__]:: srio%d  can not find free dma index!\n",__FUNCTION__,__LINE__,controller);
        return -1;
    }
    
    /*向DMA描述符写入数据*/
    _phyAddr = (u64)hrKmToPhys((void *)memAddr);
    rab_desc_wdma(controller, _wdmaIndex, _phyAddr, rioAddr, transferLen, destID);
//    printk("<DEBUG> [%s:__%d__]\n",__FUNCTION__,__LINE__);
 
#if _TEST_SRIO_DMASndTime_
    if((wdma_count % _TEST_DMA_PRINT_CYCLE_) == 0)
    	tt1 = bslGetTimeUsec();
#endif /*_TEST_SRIO_DMASndTime_*/
  	
#if _RIO_SEM_REWORKS_
    if (sem_wait2(&HR_RAB_DMA_WSem[controller][_wdmaIndex], WAIT, _timeout) == -1)
#else
    if (semTake(HR_RAB_DMA_WSem[controller][_wdmaIndex], _timeout) == -1)
#endif /*_RIO_SEM_REWORKS_*/
    {
        printk("<ERROR> [%s:__%d__]:: srio%d  dma is busy,write failed! dmaIndex = %d destID = 0x%x\n",\
        		__FUNCTION__,__LINE__,controller, _wdmaIndex, destID);
        return -1;
    }
    else
    {
    	
#if _RIO_SEM_REWORKS_
    	sem_wait2(&HR_WDMA_SendAccessSem[controller], WAIT, NO_TIMEOUT);
#else
        semTake(HR_WDMA_SendAccessSem[controller], WAIT_FOREVER);
#endif /*_RIO_SEM_REWORKS_*/
        	
//    	printk("<DEBUG> [%s:__%d__]\n",__FUNCTION__,__LINE__);
    	HR_WDMA_STATUS[controller][_wdmaIndex] = 0;
#if _RIO_SEM_REWORKS_
    	sem_post(&HR_WDMA_SendAccessSem[controller]);
#else
        semGive(HR_WDMA_SendAccessSem[controller]);
#endif /*_RIO_SEM_REWORKS_*/
    }
//    printk("<DEBUG> [%s:__%d__]\n",__FUNCTION__,__LINE__);
    
#if _TEST_SRIO_DMASndTime_
    if((wdma_count % _TEST_DMA_PRINT_CYCLE_) == 0)
	{
    	tt4 = bslGetTimeUsec();
//		printk("<***DEBUG***> [%s:_%d_]:: [CPU%d] [第%u次wdma] [wdma中断: %u] DMA开始传输 %u us, 进WDMA中断 %u us, 出WDMA中断 %u us, WDMA解除阻塞 %u us\n", 
//				__FUNCTION__,__LINE__,cpu_id_get(),wdma_count, wdma_int_count, tt1-tt0, tt2-tt1, tt3-tt2, tt4-tt3);
	}
//    printk("<***DEBUG***> [%s:_%d_]:: [CPU%d] [第%u次wdma]  Return\n",__FUNCTION__,__LINE__,cpu_id_get(),wdma_count);
#endif /*_TEST_SRIO_DMASndTime_*/
    return 0;
}

/*
功能：启动rio内部的DMA，发送nread数据包
参数：
u8 controller:控制器号，(0-1)
u32 rioAddr:Rio地址
u64 memPhyAddr：虚拟地址，dma需要转换成物理地址
u32 transferLen:传输长度
u16 destID:目的id
返回值:0表示成功，-1表示失败
*/
int bslRioDmaRead(u8 controller, u32 rioAddr, u64 memAddr, u32 transferLen, u16 destID)
{
    u64 _phyAddr;
    u8 _rdmaIndex;
    u32 _timeout = sysClkRateGet() * 5;

    if(transferLen > RAB_DMA_MAX_TRANSLEN)
    {
        printk("<ERROR> [%s:__%d__]:: DMA transfer length must be <= 256M bytes.\n",__FUNCTION__,__LINE__);
        return -1;
    }

    _rdmaIndex = FindFreeDMAIndex(controller, RAB_DMA_RDMA);
//    printk("<DEBUG> [%s:__%d__]:: srio%d  _rdmaIndex = 0x%x\n",__FUNCTION__,__LINE__,controller,_rdmaIndex);
    if (_rdmaIndex >= RAB_DMA_MAX_ENGINE)
    {
        printk("<ERROR> [%s:__%d__]:: srio%d  can not find free dma index!\n",__FUNCTION__,__LINE__,controller);
        return -1;
    }

    _phyAddr = hrKmToPhys((void *)memAddr);
    rab_desc_rdma(controller, _rdmaIndex, rioAddr, _phyAddr, transferLen, destID);

#if _RIO_SEM_REWORKS_
    if (sem_wait2(&HR_RAB_DMA_RSem[controller][_rdmaIndex], WAIT, _timeout) == -1)
#else
    if (semTake(HR_RAB_DMA_RSem[controller][_rdmaIndex], _timeout) == -1)
#endif
    {
    	printk("<ERROR> [%s:__%d__]:: srio%d  dma is busy,write failed! dmaIndex = %d destID = 0x%x\n",\
    	        		__FUNCTION__,__LINE__,controller, _rdmaIndex, destID);
        return -1;
    }
    else
    {
#if _RIO_SEM_REWORKS_
    	sem_wait2(&HR_RDMA_SendAccessSem[controller], WAIT, NO_TIMEOUT);
#else
        semTake(HR_RDMA_SendAccessSem[controller],WAIT_FOREVER);
#endif
        HR_RDMA_STATUS[controller][_rdmaIndex] = 0;
#if _RIO_SEM_REWORKS_
        sem_post(&HR_RDMA_SendAccessSem[controller]);
#else
        semGive(HR_RDMA_SendAccessSem[controller]);
#endif
    }

    return 0;
}


void srio_module_init(void)
{
	hrSrioInit(0);
	hrSrioInit(1);
	
//	if(hr3_board_type == 2)
//	{
		rab_phy_rx_termination_set(0, 1);/*ldf 20230627 add:: hc3080b*/
		rab_phy_rx_termination_set(1, 1);/*ldf 20230627 add:: hc3080b*/
//	}
}


/************************************** demo ***********************************/

#define RAB0_ID 0x9999
#define RAB1_ID 0x3333
#define SPEEDUP_ENABLE 1
#define SPEEDUP_DISABLE 0
#define RAB0_INFO 0xab14
#define RAB1_INFO 0x89ac

int dbCallbackFuc(u8 controller)
{
	uart_printf(">>>>>>>rab%d recv Doorbell!\r\n", controller);
	return 0;
}

#if 0
void doorbellCmdMain()
{
    sriox SRIO_0, SRIO_1;
    u16 id, info;

    uart_printf("Test Doorbell\r\n");

    SRIO_0 = SRIO0;
    SRIO_1 = SRIO1;

    bslRioSetID(SRIO_0, RAB0_ID);
    bslRioSetID(SRIO_1, RAB1_ID);

    rab_wait_for_linkup(SRIO_0);
    rab_wait_for_linkup(SRIO_1);

    bslRioDBInit(SRIO_0, dbCallbackFuc);
    bslRioDBInit(SRIO_1, dbCallbackFuc);

    id = bslRioGetID(SRIO_1);
    info = 0xab14;
    bslRioSendDB(SRIO_0, id, info);

    udelay(10000);

    id = bslRioGetID(SRIO_0);
    info = 0x89ac;
    bslRioSendDB(SRIO_1, id, info);

    udelay(10000);
    uart_printf("rab0 doorbell num = 0x%x, rab1 doorbell num = 0x%x\r\n", bslRioGetDBNum(SRIO_0), bslRioGetDBNum(SRIO_1));

    bslRioGetDBInfo(SRIO_0, &info, &id);
    if ((id != RAB1_ID) || (info != 0x89ac))
        uart_printf("========================= rab0 recv doorbell err! source_id:0x%x, info:0x%x\r\n", id, info);
    else
        uart_printf("========================= rab0 recv doorbell OK!  source_id:0x%x, info:0x%x\r\n", id, info);

    uart_printf("rab0 doorbell num = 0x%x, rab1 doorbell num = 0x%x\r\n", bslRioGetDBNum(SRIO_0), bslRioGetDBNum(SRIO_1));

    bslRioGetDBInfo(SRIO_1, &info, &id);
    if ((id != RAB0_ID) || (info != 0xab14))
        uart_printf("========================= rab1 recv doorbell err! source_id:0x%x, info:0x%x\r\n", id, info);
    else
        uart_printf("========================= rab1 recv doorbell OK!  source_id:0x%x, info:0x%x\r\n", id, info);

    uart_printf("rab0 doorbell num = 0x%x, rab1 doorbell num = 0x%x\r\n", bslRioGetDBNum(SRIO_0), bslRioGetDBNum(SRIO_1));

    rab_link_and_mode_status(SRIO_0);
    rab_link_and_mode_status(SRIO_1);

    uart_printf("Doorbell test end\r\n");
}
#else /*ldf 20230405*/
void doorbellCmdMain(void)
{
	u32 tmp;
	sriox SRIO_0, SRIO_1;
	u16 id, info;

	uart_printf("Test doorbell\r\n");

	SRIO_0 = SRIO0;
	SRIO_1 = SRIO1;
	
	/*HR3 SRIO初始化*/
	hrSrioInit(SRIO_0);
	hrSrioInit(SRIO_1);

	/*设置RAB ID*/
	bslRioSetID(SRIO_0, RAB0_ID); 
	bslRioSetID(SRIO_1, RAB1_ID); 
	
	/*等待RAB连接*/
//	rab_wait_for_linkup(SRIO_0);
//	rab_wait_for_linkup(SRIO_1);
	
	/*SRIO门铃初始化*/
	bslRioDBInit(SRIO_0, dbCallbackFuc);
	bslRioDBInit(SRIO_1, dbCallbackFuc);

	/*SRIO_0发送DoorBell测试*/
	id = bslRioGetID(SRIO_1);
	rab_db_test(SRIO_0, id, RAB0_INFO);
	udelay(10000);
	/*SRIO_1发送DoorBell测试*/
	id = bslRioGetID(SRIO_0);
	rab_db_test(SRIO_1, id, RAB1_INFO);
	
    udelay(10000);
    uart_printf("rab0 doorbell num = 0x%x, rab1 doorbell num = 0x%x\r\n", bslRioGetDBNum(SRIO_0), bslRioGetDBNum(SRIO_1));

    /*SRIO_0收取门铃消息*/
    bslRioGetDBInfo(SRIO_0, &info, &id);
    if ((id != RAB1_ID) || (info != RAB1_INFO))
        uart_printf("========================= rab0 recv doorbell err! source_id:0x%x, info:0x%x\r\n", id, info);
    else
        uart_printf("========================= rab0 recv doorbell OK!  source_id:0x%x, info:0x%x\r\n", id, info);

    uart_printf("rab0 doorbell num = 0x%x, rab1 doorbell num = 0x%x\r\n", bslRioGetDBNum(SRIO_0), bslRioGetDBNum(SRIO_1));

    /*SRIO_1收取门铃消息*/
    bslRioGetDBInfo(SRIO_1, &info, &id);
    if ((id != RAB0_ID) || (info != RAB0_INFO))
        uart_printf("========================= rab1 recv doorbell err! source_id:0x%x, info:0x%x\r\n", id, info);
    else
        uart_printf("========================= rab1 recv doorbell OK!  source_id:0x%x, info:0x%x\r\n", id, info);

    uart_printf("rab0 doorbell num = 0x%x, rab1 doorbell num = 0x%x\r\n", bslRioGetDBNum(SRIO_0), bslRioGetDBNum(SRIO_1));

    /*关闭SRIO入站门铃*/
    disable_ib_db(SRIO_0);
    disable_ib_db(SRIO_1);
    
    /*读取RAB连接状态和模式*/
    rab_link_and_mode_status(SRIO_0);
    rab_link_and_mode_status(SRIO_1);

    uart_printf("Doorbell test end\r\n");

    return;
}
#endif

void maintCmdMain()
{
    sriox SRIO_0, SRIO_1;
    u32 offset;
    u32 writedata;
    u32 rddata;

    uart_printf("Test maint\r\n");

    SRIO_0 = SRIO0;
    SRIO_1 = SRIO1;

	/*HR3 SRIO初始化*/
	hrSrioInit(SRIO_0);
	hrSrioInit(SRIO_1);

	/*设置RAB ID*/
//    bslRioSetID(SRIO_0, RAB0_ID);
//    bslRioSetID(SRIO_1, RAB1_ID);
	if(sysCpuGetID() == 0)
	{
		bslRioSetID(SRIO_0, 0x10); 
		bslRioSetID(SRIO_1, 0x11); 
	}
	else
	{
		bslRioSetID(SRIO_0, 0x12); 
		bslRioSetID(SRIO_1, 0x13); 
	}
	
	/*等待RAB连接*/
	rab_wait_for_linkup(SRIO_0);
	rab_wait_for_linkup(SRIO_1);

    bslRioSetMaintOW(SRIO0);
    bslRioSetMaintOW(SRIO1);

    offset = 0x124;
    writedata = 0xabcdefff;
    bslRioMaintRead(SRIO0, RAB1_ID, 0, offset, &rddata);
    udelay(10000);
    uart_printf("offset = 0x%x  maintRead = 0x%x  localRead = 0x%x\r\n", offset, rddata, rab_page_read(SRIO_1, offset));
    bslRioMaintWrite(SRIO0, RAB1_ID, 0, offset, writedata);
    bslRioMaintRead(SRIO0, RAB1_ID, 0, offset, &rddata);
    udelay(10000);
    uart_printf("offset = 0x%x  maintRead = 0x%x  localRead = 0x%x\r\n", offset, rddata, rab_page_read(SRIO_1, offset));

    offset = 0x2004c;
    writedata = 0x12345678;
    bslRioMaintRead(SRIO0, RAB1_ID, 0, offset, &rddata);
    udelay(10000);
    uart_printf("offset = 0x%x  maintRead = 0x%x  localRead = 0x%x\r\n", offset, rddata, rab_page_read(SRIO_1, offset));
    bslRioMaintWrite(SRIO0, RAB1_ID, 0, offset, writedata);
    bslRioMaintRead(SRIO0, RAB1_ID, 0, offset, &rddata);
    udelay(10000);
    uart_printf("offset = 0x%x  maintRead = 0x%x  localRead = 0x%x\r\n", offset, rddata, rab_page_read(SRIO_1, offset));

    /*读取RAB连接状态和模式*/
    rab_link_and_mode_status(SRIO_0);
    rab_link_and_mode_status(SRIO_1);

    uart_printf("maint test end\r\n");
}

const int U32_TESTS_COUNT = 4;
const int U16_TESTS_COUNT = 4; // watch out too big to overflow
const int U8_TESTS_COUNT = 4; // watch out too big to overflow
void pio_tests(u32 _rioAddr0, u32 _rioAddr1, u64 _memAddr0, u64 _memAddr1)
{
    u32 i;
    u32 tmp1;
    u32 tmp2;
    u32 err_cnt=0;

    err_cnt = 0;
    for (i = 0; i < U32_TESTS_COUNT; i++) {
        phx_write_u32(RAB_OW_LOW_BASE_ADDR(0) + i * 4, i + 0x87654321);
        phx_write_u32(RAB_OW_LOW_BASE_ADDR(1) + i * 4, i + 0x12345678);
    }

    udelay(10000);
    for (i = 0; i < U32_TESTS_COUNT; i++) {
#if CACHE_EN
        tmp1 = phx_read_cache_u32(_memAddr0 + i * 4);
        tmp2 = phx_read_cache_u32(_memAddr1 + i * 4);
#else
        tmp1 = phx_read_u32(_memAddr0 + i * 4);
        tmp2 = phx_read_u32(_memAddr1 + i * 4);
#endif
        if (tmp1 != (i + 0x12345678)) {
            uart_printf("type = 1, nwrite 32bit:exp 0x%x, read 0x%x \r\n", i + 0x12345678, tmp1);
            err_cnt++;
        }
        if (tmp2 != (i + 0x87654321)) {
            uart_printf("type = 1, nwrite 32bit:exp 0x%x, read 0x%x \r\n", i + 0x87654321, tmp2);
            err_cnt++;
        }
    }
    if (err_cnt)
        uart_printf("========================= 32bit apio nwrite err! err_cnt = 0x%x\r\n", err_cnt);
    else
        uart_printf("========================= 32bit apio nwrite pass!\r\n");

    err_cnt = 0;
    for (i = 0; i < U32_TESTS_COUNT; i++) {
        tmp1 = phx_read_u32(RAB_OW_LOW_BASE_ADDR(1)  + i * 4);
        tmp2 = phx_read_u32(RAB_OW_LOW_BASE_ADDR(0)  + i * 4);
        udelay(10000);
        if (tmp1 != (i + 0x12345678)) {
            uart_printf("type = 1, nread 32bit:exp 0x%x, read 0x%x \r\n", i + 0x12345678, tmp1);
            err_cnt++;
        }
        if (tmp2 != (i + 0x87654321)) {
            uart_printf("type = 1, nread 32bit:exp 0x%x, read 0x%x \r\n", i + 0x87654321, tmp2);
            err_cnt++;
        }
    }
    if (err_cnt)
        uart_printf("========================= 32bit apio nread err! err_cnt = 0x%x\r\n", err_cnt);
    else
        uart_printf("========================= 32bit apio nread pass!\r\n");

    err_cnt = 0;
    for (i = 0; i < U16_TESTS_COUNT; i++)
        phx_write_u16(RAB_OW_LOW_BASE_ADDR(0) + 0x0500 + i * 2, i + 0x4321);

    udelay(10000);
    for (i = 0; i < U16_TESTS_COUNT; i++) {
#if CACHE_EN
        tmp1 = phx_read_cache_u16(_memAddr1 + 0x500 + i * 2);
#else
        tmp1 = phx_read_u16(_memAddr1 + 0x500 + i * 2);
#endif
        if (tmp1 != (i + 0x4321)) {
            uart_printf("type = 1, nwrite 16bit:exp 0x%x, read 0x%x \r\n", i + 0x4321, tmp1);
            err_cnt++;
        }
    }
    if (err_cnt)
        uart_printf("========================= 16bit apio nwrite err! err_cnt = 0x%x\r\n", err_cnt);
    else
        uart_printf("========================= 16bit apio nwrite pass!\r\n");

    err_cnt = 0;
    for (i = 0; i < U16_TESTS_COUNT; i++) {
        tmp1 = phx_read_u16(RAB_OW_LOW_BASE_ADDR(0) + 0x500 + i * 2);
        udelay(10000);
        if (tmp1 != (i + 0x4321)) {
            uart_printf("type = 1, nread 16bit:exp 0x%x, read 0x%x \r\n", i + 0x4321, tmp1);
            err_cnt++;
        }
    }
    if (err_cnt)
        uart_printf("========================= 16bit apio nread err! err_cnt = 0x%x\r\n", err_cnt);
    else
        uart_printf("========================= 16bit apio nread pass!\r\n");

    err_cnt = 0;
    for (i = 0; i < U8_TESTS_COUNT; i++)
        phx_write_u8(RAB_OW_LOW_BASE_ADDR(0) + 0x1000 + i , i + 0x54);

    udelay(10000);
    for (i = 0; i < U8_TESTS_COUNT; i++) {
#if CACHE_EN
        tmp2 = phx_read_cache_u8(_memAddr1 + 0x1000 + i);
#else
        tmp2 = phx_read_u8(_memAddr1 + 0x1000 + i);
#endif
        if (tmp2 != (i + 0x54)) {
            uart_printf("type = 1, nwrite 8bit:exp 0x%x, read 0x%x \r\n", i + 0x54, tmp2);
            err_cnt++;
        }
    }
    if (err_cnt)
        uart_printf("========================= 8bit apio nwrite err! err_cnt = 0x%x\r\n", err_cnt);
    else
        uart_printf("========================= 8bit apio nwrite pass!\r\n");

    err_cnt = 0;
    for (i = 0; i < U8_TESTS_COUNT; i++) {
        tmp2 = phx_read_u8(RAB_OW_LOW_BASE_ADDR(0) + 0x1000 + i);
        udelay(10000);
        if (tmp2 != (i + 0x54)) {
            uart_printf("type = 1, nread 8bit:exp 0x%x, read 0x%x \r\n", i + 0x54, tmp2);
            err_cnt++;
        }
    }
    if (err_cnt)
        uart_printf("========================= 8bit apio nread err! err_cnt = 0x%x\r\n", err_cnt);
    else
        uart_printf("========================= 8bit apio nread pass!\r\n");
}

void pioCmdMain()
{
    sriox SRIO_0, SRIO_1;
    u8 index = 1;
    u32 _rioAddr0 = 0x40000000;
    u32 _rioAddr1 = 0x60000000;
    u64 _memPhyAddr0 = 0x1080000000;
    u64 _memPhyAddr1 = 0x1090000000;

    uart_printf("Test PIO\r\n");

    SRIO_0 = SRIO0;
    SRIO_1 = SRIO1;

    bslRioSetID(SRIO_0, RAB0_ID);
    bslRioSetID(SRIO_1, RAB1_ID);

    rab_wait_for_linkup(SRIO_0);
    rab_wait_for_linkup(SRIO_1);

    bslRioSetIW(SRIO0, _rioAddr0, _memPhyAddr0, 0x10000000);/*ldf 20230405*/
    bslRioSetIW(SRIO1, _rioAddr1, _memPhyAddr1, 0x10000000);/*ldf 20230405*/

    //初始化制器0的RapidIO设备OW发送窗口，大小16M
    bslRioSetOW(SRIO0, index, RAB1_ID, RAB_OW_LOW_BASE_ADDR(SRIO0), _rioAddr1, 16 * 0x100000, RAB_APIO_NREAD_NWRITE);
    //初始化制器1的RapidIO设备OW发送窗口，大小16M
    bslRioSetOW(SRIO1, index, RAB0_ID, RAB_OW_LOW_BASE_ADDR(SRIO1), _rioAddr0, 16 * 0x100000, RAB_APIO_NREAD_NWRITE);

    pio_tests(_rioAddr0, _rioAddr1, _memPhyAddr0, _memPhyAddr1);

    rab_link_and_mode_status(SRIO_0);
    rab_link_and_mode_status(SRIO_1);

    uart_printf("PIO test end\r\n");
}

void bslDmaCmdMain(void)
{
	sriox SRIO_0, SRIO_1;
    u32 _rioAddr0;
    u32 _rioAddr1;
    u64 _rioMemAddr0;
    u64 _rioMemAddr1;
    u64 _srcMemAddr;
    u32 _memLen;
    u32 windowSize;
    u32 data0, data1;
    int i;
    u32 error_cnt = 0;

	printk("<DEBUG> [%s:__%d__]:: Test WDMA and RDMA\n",__FUNCTION__,__LINE__);

	SRIO_0 = SRIO0;
	SRIO_1 = SRIO1;

	/*HR3 SRIO初始化*/
	hrSrioInit(SRIO_0);
	hrSrioInit(SRIO_1);

#if PHY_BIST_CONFIG 
	rab_phy_bist_check_status(SRIO_0);
	rab_phy_bist_check_status(SRIO_1);

	//rab_phy_bist_tx_inject_error(SRIO_0);
	//rab_phy_bist_tx_inject_error(SRIO_1);

#endif
	
	/*设置RAB ID*/
	bslRioSetID(SRIO_0, RAB0_ID); 
	bslRioSetID(SRIO_1, RAB1_ID); 
	
	/*等待RAB连接*/
	rab_wait_for_linkup(SRIO_0);
	rab_wait_for_linkup(SRIO_1);
	 
#if 0
	set_rab_rpio_ctrl(0, 0, 0x3);
	set_rab_rpio_ctrl(1, 0, 0x3);
	set_rab_apio_ctrl(0, 0, 0x3);
	set_rab_apio_ctrl(1, 0, 0x3);
#endif
	
//	/*初始化DMA信号量*/
//	rab_dma_sem_init(SRIO_0);
//	rab_dma_sem_init(SRIO_1);
	
	
	/*将_rioAddr映射到_rioMemAddr*/
    _rioAddr0 = 0x40000000;
    _rioAddr1 = 0x60000000;
	_memLen = 0x2000000;//测试数据  32M
	windowSize = 0x10000000;// windowSize 256M
	_srcMemAddr = (u64)malloc(3 * _memLen);
    printk("<DEBUG> [%s:__%d__]:: _srcMemAddr = 0x%lx\n",__FUNCTION__,__LINE__,_srcMemAddr);
    if(_srcMemAddr == 0)
    {
    	printk("<ERROR> [%s:__%d__]:: can not malloc enough _srcMemAddr.\n",__FUNCTION__,__LINE__);
        return;
    }
	memset((void*)_srcMemAddr, 0, 3 * _memLen);
    /*向_srcMemAddr写入_memLen测试数据*/
    for (i = 0; i < _memLen; i += 4)
        *((u32 *)(_srcMemAddr + i)) = 0x12345678 + i;
    

    _rioMemAddr0 = 0x90000000;
    _rioMemAddr1 = 0xA0000000;
    //输入窗口  
    bslRioSetIW(SRIO_0, _rioAddr0, (u64)hrKmToPhys((void *)_rioMemAddr0), windowSize);
    bslRioSetIW(SRIO_1, _rioAddr1, (u64)hrKmToPhys((void *)_rioMemAddr1), windowSize);
    
    
//    /*给DMA描述符分配空间*/
//	rab_dma_malloc_desc(SRIO_0);
//	rab_dma_malloc_desc(SRIO_1);

    

/***DmaWrite*******************************************************************************************/
    /*使用RAB1将_srcMemAddr中的数据通过DMA写入到_rioAddr0*/
    bslRioDmaWrite(SRIO_1, _srcMemAddr, _rioAddr0, _memLen, RAB0_ID);
    delay_cnt(50000);
    /*使用RAB0将_srcMemAddr中的数据通过DMA写入到_rioAddr1*/
    bslRioDmaWrite(SRIO_0, _srcMemAddr, _rioAddr1, _memLen, RAB1_ID);
    delay_cnt(50000);


    // 比较WDMA的数据
//    if(memcmp((void*)(_rioMemAddr1), (void*)_srcMemAddr, _memLen))
//    {
        // data compare error
    	for (i = 0; i < _memLen; i += 4)
        {
    		data0 = *((u32 *)(_srcMemAddr + i));
    		data1 = *((u32 *)(_rioMemAddr1 + i));
//#if CACHE_EN
//            data1 = phx_read_cache_u32(_rioMemAddr1 + i);
//#else
//            data1 = phx_read_u32(_rioMemAddr1 + i);
//#endif
            if (data0 != data1)
            {
            	error_cnt += 4;
//                printk("<ERROR> [%s:__%d__]:: rab0 wdma Error! i = 0x%x expect = 0x%x value = 0x%x\n",\
//                		__FUNCTION__,__LINE__,i, data0, data1);
//                break;
            }
        }
//    }
//    else
//    {
        printk("<DEBUG> [%s:__%d__]:: rab0 wdma completed! error_cnt = 0x%x\n",__FUNCTION__,__LINE__,error_cnt);
//    }

        error_cnt = 0;
        
//    if(memcmp((void*)(_rioMemAddr0), (void*)_srcMemAddr, _memLen))
//    {
        // data compare error
        for (i = 0; i < _memLen; i += 4)
        {
            data0 = *((u32 *)(_srcMemAddr + i));
            data1 = *((u32 *)(_rioMemAddr0 + i));
//#if CACHE_EN
//            data1 = phx_read_cache_u32(_rioMemAddr0 + i);
//#else
//            data1 = phx_read_u32(_rioMemAddr0 + i);
//#endif
            if (data0 != data1)
            {
            	error_cnt += 4;
//                printk("<ERROR> [%s:__%d__]:: rab1 wdma Error! i = 0x%x expect = 0x%x value = 0x%x\n",\
//                                		__FUNCTION__,__LINE__,i, data0, data1);
//                break;
            }
        }
//    }
//    else
//    {
        printk("<DEBUG> [%s:__%d__]:: rab1 wdma completed! error_cnt = 0x%x\n",__FUNCTION__,__LINE__,error_cnt);
//    }
   
        
#if 1
/***DmaRead*******************************************************************************************/      
	memset((void*)(_srcMemAddr + _memLen), 0, _memLen);
	memset((void*)(_srcMemAddr + 2*_memLen), 0, _memLen);
    /*使用RAB0将_rioAddr1中的数据通过DMA读出到_rioMemAddr0*/
    bslRioDmaRead(SRIO_0, _rioAddr1, (_srcMemAddr + _memLen), _memLen, RAB1_ID);
    /*使用RAB1将_rioAddr0中的数据通过DMA读出到_rioMemAddr1*/
    bslRioDmaRead(SRIO_1, _rioAddr0, (_srcMemAddr + 2*_memLen), _memLen, RAB0_ID);

    // 比较RDMA的数据
    error_cnt = 0;
//    if(memcmp((void*)(_rioMemAddr0), (void*)_srcMemAddr, _memLen))
//    {
        // data compare error
    for (i = 0; i < _memLen; i += 4)
        {
            data0 = *((u32 *)(_srcMemAddr + i));
            data1 = *((u32 *)(_srcMemAddr + _memLen + i));
            if (data0 != data1)
            {
            	error_cnt += 4;
//                printk("<ERROR> [%s:__%d__]:: rab0 rdma Error! i = 0x%x expect = 0x%x value = 0x%x\n",\
//                                                		__FUNCTION__,__LINE__,i, data0, data1);
//                break;
            }
        }
//    }
//    else
//    {
        printk("<DEBUG> [%s:__%d__]:: rab0 rdma completed! error_cnt = 0x%x\n",__FUNCTION__,__LINE__,error_cnt);
//    }

      error_cnt = 0;
//    if(memcmp((void*)(_rioMemAddr1), (void*)_srcMemAddr, _memLen))
//    {
        // data compare error
        for (i = 0; i < _memLen; i += 4)
        {
            data0 = *((u32 *)(_srcMemAddr + i));
            data1 = *((u32 *)(_srcMemAddr + 2*_memLen + i));
            if (data0 != data1)
            {
            	error_cnt += 4;
//                printk("<ERROR> [%s:__%d__]:: rab1 rdma Error! i = 0x%x expect = 0x%x value = 0x%x\n",\
//                                                		__FUNCTION__,__LINE__,i, data0, data1);
//                break;
            }
        }
//    }
//    else
//    {
        printk("<DEBUG> [%s:__%d__]:: rab1 rdma completed! error_cnt = 0x%x\n",__FUNCTION__,__LINE__,error_cnt);
//    }
#endif
    /*读取RAB连接状态和模式*/
    rab_link_and_mode_status(SRIO_0);
    rab_link_and_mode_status(SRIO_1);

    free(_srcMemAddr);
    printk("<DEBUG> [%s:__%d__]:: WDMA and RDMA test end\n",__FUNCTION__,__LINE__);
    
    return;

}

#define CHIP_FREQ (u64)1500000000
#define USEC_PER_SEC (1000000)
void bslDmaSpeedCmdMain(void)
{
    sriox SRIO_0, SRIO_1;
    u32 _rioAddr0 = 0x60000000;
    u32 _rioAddr1 = 0x70000000;
    u64 _rioMemAddr0;
    u64 _rioMemAddr1;
    u64 _srcMemAddr;
    u64 _memLen[] = {
            0x4000,   // 16KB
            0x8000,   // 32KB
            0x10000,  // 64KB
            0x20000,  // 128KB
            0x40000,  // 256KB
            0x80000,  // 512KB
            0x100000, // 1MB
            0x200000,  // 2MB
            0x400000,  // 4MB
            0x800000,  // 8MB
            0x1000000,  // 16MB
            0x2000000,  // 32MB
            0x4000000,  // 64MB
            0x8000000,  // 128MB
            0x10000000  // 256MB
    };
    int _test_times;
    u32 _step = 0x800000;
    u32 _IWSize = 0x10000000; // 256MB
    int i, j;
    u64 t0, t1, t2, total, usec;

    printk("<DEBUG> [%s:__%d__]:: Test DMA Speed\n",__FUNCTION__,__LINE__);

    SRIO_0 = SRIO0;
    SRIO_1 = SRIO1;

	/*HR3 SRIO初始化*/
	hrSrioInit(SRIO_0);
	hrSrioInit(SRIO_1);
	
    bslRioSetID(SRIO_0, RAB0_ID);
    bslRioSetID(SRIO_1, RAB1_ID);

    rab_wait_for_linkup(SRIO_0);
    rab_wait_for_linkup(SRIO_1);

    _srcMemAddr = (u64)malloc(2*_IWSize);
	printk("<DEBUG> [%s:__%d__]:: _srcMemAddr = 0x%lx\n",__FUNCTION__,__LINE__,_srcMemAddr);
	if(_srcMemAddr == 0)
	{
		printk("<ERROR> [%s:__%d__]:: can not malloc enough _srcMemAddr.\n",__FUNCTION__,__LINE__);
		return;
	}
	memset((void*)_srcMemAddr, 0, 2*_IWSize);
	
    _rioMemAddr0 = 0x90000000;
    _rioMemAddr1 = 0xA0000000;
    //输入窗口  
    bslRioSetIW(SRIO_0, _rioAddr0, (u64)hrKmToPhys((void *)_rioMemAddr0), _IWSize);
    bslRioSetIW(SRIO_1, _rioAddr1, (u64)hrKmToPhys((void *)_rioMemAddr1), _IWSize);
    
    /*给DMA描述符分配空间*/
//	rab_dma_malloc_desc(SRIO_0);
//	rab_dma_malloc_desc(SRIO_1);

    /*向_srcMemAddr写入_memLen测试数据*/
    for (i = 0; i < _IWSize; i += 4)
        *((u32 *)(_srcMemAddr + i)) = 0x12345678 + i;

    for (i = 0; i < sizeof(_memLen) / sizeof(_memLen[0]); i++)
    {
#if 0
        _test_times = 0;
        total = 0;
        while (_test_times < 2000)
        {
            t1 = (u64)sysCountGet();
            for (j = 0; j < 20; j++) // 20 < _IWSize / _step
                bslRioDmaWrite(SRIO_0, _srcMemAddr + j * _step, _rioAddr1 + j * _step, _memLen[i], RAB1_ID);
            _test_times += 20;
            t2 = (u64)sysCountGet();
            if (t2 > t1)
                total += (t2 - t1);
            else
                total += (t2 + (1ull << 32) - t1);
        };
        printk("<DEBUG> [%s:__%d__]:: WDMA i = %d, _memLen = 0x%x  _test_times = %d, total = 0x%llx  speed = %d MB/s\n",\
        		__FUNCTION__,__LINE__,i, _memLen[i], _test_times, total, (_memLen[i] * CHIP_FREQ  * _test_times / total) >> 20);
#else
        /* 先执行一遍会用到的函数，把它们的代码和数据装入cache */
        sys_timestamp();
        bslRioDmaWrite(SRIO_0, _srcMemAddr, _rioAddr1, _memLen[i], RAB1_ID);
        delay_cnt(20000);
        
        t0 = sys_timestamp();
        t1 = sys_timestamp();
        bslRioDmaWrite(SRIO_0, _srcMemAddr, _rioAddr1, _memLen[i], RAB1_ID);
        t2 = sys_timestamp();
        total = t2-t1-(t1-t0);
        usec = total * USEC_PER_SEC / CHIP_FREQ; 
        if(usec > 0)
			printk("<DEBUG> [%s:__%d__]:: WDMA(0>>1)  i = %d, _memLen = 0x%x B  total_time = %u us  speed = %u MB/s\n",\
					__FUNCTION__,__LINE__,i, _memLen[i], usec, (_memLen[i]*1000000/1024/1024)/usec);
        delay_cnt(20000);
#endif      
    }

    for (i = 0; i < sizeof(_memLen) / sizeof(_memLen[0]); i++)
    {
#if 0
        _test_times = 0;
        total = 0;
        while (_test_times < 2000)
        {
            t1 = (u64)sysCountGet();
            for (j = 0; j < 20; j++) // 20 < _IWSize / _step
                bslRioDmaRead(SRIO_0, _rioAddr1 + j * _step, _srcMemAddr+_IWSize + j * _step, _memLen[i], RAB1_ID);
            _test_times += 20;
            t2 = (u64)sysCountGet();
            if (t2 > t1)
                total += (t2 - t1);
            else
                total += (t2 + (1ull << 32) - t1);
        };
        printk("<DEBUG> [%s:__%d__]:: RDMA i = %d, _memLen = 0x%x  _test_times = %d, total = 0x%llx  speed = %d MB/s\n",\
        		__FUNCTION__,__LINE__,i, _memLen[i], _test_times, total, (_memLen[i] * CHIP_FREQ  * _test_times / total) >> 20);
#else
        /* 先执行一遍会用到的函数，把它们的代码和数据装入cache */
        sys_timestamp();
        bslRioDmaRead(SRIO_0, _rioAddr1, _srcMemAddr+_IWSize, _memLen[i], RAB1_ID);
        delay_cnt(20000);
        
        t0 = sys_timestamp();
        t1 = sys_timestamp();
        bslRioDmaRead(SRIO_0, _rioAddr1, _srcMemAddr+_IWSize, _memLen[i], RAB1_ID);
        t2 = sys_timestamp();
        total = t2-t1-(t1-t0);
        usec = total * USEC_PER_SEC / CHIP_FREQ; 
        if(usec > 0)
			printk("<DEBUG> [%s:__%d__]:: RDMA(0<<1)  i = %d, _memLen = 0x%x B  total_time = %u us  speed = %u MB/s\n",\
					__FUNCTION__,__LINE__,i, _memLen[i], usec, (_memLen[i]*1000000/1024/1024)/usec);
        delay_cnt(20000);
#endif 
    }

    rab_link_and_mode_status(SRIO_0);
    rab_link_and_mode_status(SRIO_1);

    free(_srcMemAddr);
    printk("<DEBUG> [%s:__%d__]:: DMA speed test end\n",__FUNCTION__,__LINE__);
}
