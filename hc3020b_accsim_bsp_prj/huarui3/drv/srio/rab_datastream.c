#include "rab_datamessage.h"
#include "rab_datastream.h"

#define DS_DEBUG_INFO 0

void rab_obds_hdr_desc_pack(u32 desc_buf[8],
		struct rab_obds_hdr_desc *hdr)
{
	desc_buf[0] = hdr->valid
		| (hdr->next_descriptor_pointer_valid << 1)
		| (hdr->descriptor_type << 2)
		| (hdr->single_descriptor << 3)
		| (hdr->end_of_chain << 4)
		| (hdr->interrupt_enable << 5)
		| (hdr->dest_id << 16);
	desc_buf[1] = hdr->stream_id
		| (hdr->pdu_length << 16);
	desc_buf[2] = hdr->priority
		| (hdr->class_of_service << 2)
		| (hdr->crf << 10)
		| (hdr->data_address_37_32 << 22)
		| (hdr->next_descriptor_address_upper_37 << 28);
	desc_buf[3] = 0x00000000;
	desc_buf[4] = hdr->next_descriptor_address;
	desc_buf[5] = hdr->data_address_31_0;
}

void rab_obds_desc_hdr_unpack(u32 desc_buf[8],
		struct rab_obds_hdr_desc *hdr)
{
	hdr->valid			= desc_buf[0] & 0x1;
	hdr->next_descriptor_pointer_valid = (desc_buf[0] >> 1) & 0x1;
	hdr->descriptor_type		= (desc_buf[0] >> 2) & 0x1;
	hdr->single_descriptor		= (desc_buf[0] >> 3) & 0x1;
	hdr->end_of_chain		= (desc_buf[0] >> 4) & 0x1;
	hdr->interrupt_enable		= (desc_buf[0] >> 5) & 0x1;
	hdr->done			= (desc_buf[0] >> 8) & 0x1;
	hdr->axi_error			= (desc_buf[0] >> 10) & 0x1;
	hdr->dest_id			= (desc_buf[0] >> 16) & 0xffff;
	hdr->stream_id			= desc_buf[1] & 0xffff;
	hdr->pdu_length			= (desc_buf[1] >> 16) & 0xffff;
	hdr->priority			= (desc_buf[2]) & 0x3;
	hdr->class_of_service		= (desc_buf[2] >> 2) & 0xff;
	hdr->crf			= (desc_buf[2] >> 10) & 0x1;
	hdr->data_address_37_32		= (desc_buf[2] >> 22) & 0x3f;
	hdr->next_descriptor_address_upper_37 = (desc_buf[2] >> 28) & 0x1;
	hdr->dest_id_high = desc_buf[3] & 0xffff;
	hdr->next_descriptor_address	= desc_buf[4];
	hdr->data_address_31_0		= desc_buf[5];
}

void rab_obds_data_desc_pack(u32 desc_buf[8],
		struct rab_obds_data_desc *data)
{
	desc_buf[0] = data->valid
		| (data->next_data_descriptor_pointer_valid << 1)
		| (data->descriptor_type << 2)
		| (data->sop << 4)
		| (data->eop << 5)
		| (data->data_len << 16);
	desc_buf[1] = data->data_address_31_0;
	desc_buf[2] = data->data_address_37_32
		| (data->next_data_descriptor_addr_upper_37 << 28);
	desc_buf[3] = 0x00000000;
	desc_buf[4] = data->next_data_descriptor_addr;
}

void rab_obds_data_desc_unpack(u32 desc_buf[8],
		struct rab_obds_data_desc *data)
{
	data->valid = desc_buf[0] & 0x1;
	data->next_data_descriptor_pointer_valid =
		(desc_buf[0] >> 1) & 0x1;
	data->descriptor_type = (desc_buf[0] >> 2) & 0x1;
	data->sop = (desc_buf[0] >> 4) & 0x1;
	data->eop = (desc_buf[0] >> 5) & 0x1;
	data->done = (desc_buf[0] >> 8) & 0x1;
	data->data_len = (desc_buf[0] >> 16) & 0xffff;
	data->data_address_31_0 = desc_buf[1];
	data->data_address_37_32 = desc_buf[2] & 0x3f;
	data->next_data_descriptor_addr_upper_37 = (desc_buf[2] >> 28) & 0x1;
	data->next_data_descriptor_addr = (desc_buf[4]);
}

typedef enum {IBDS_DESC = 0, OBDS_HDR_DESC = 1, OBDS_DATA_DESC} desc_type;
void clear_desc(void *desc, int size)
{
	u32 *p = desc;
	int i;

	for (i = 0; i < size; i++)
		p[i] = 0;
}

void copy_desc_to_physical_address(u64 phy_addr, void *desc, int size, desc_type type, int desc_index)
{
	u32 *p = desc;
	u32 src0, src1, src2, src3, src4, src5;
	int i;

	for (i = 0; i < size; i++)
#if CACHE_EN
		phx_write_cache_u32(phy_addr + 4 * i, p[i]);
#else
		phx_write_u32(phy_addr + 4 * i, p[i]);
#endif
	//asm volatile("sync");

#if DS_DEBUG_INFO
#if CACHE_EN
	src0 = phx_read_cache_u32(phy_addr);
	src1 = phx_read_cache_u32(phy_addr + 4);
	src2 = phx_read_cache_u32(phy_addr + 8);
	src3 = phx_read_cache_u32(phy_addr + 12);
	src4 = phx_read_cache_u32(phy_addr + 16);
	src5 = phx_read_cache_u32(phy_addr + 20);
#else
	src0 = phx_read_u32(phy_addr);
	src1 = phx_read_u32(phy_addr + 4);
	src2 = phx_read_u32(phy_addr + 8);
	src3 = phx_read_u32(phy_addr + 12);
	src4 = phx_read_u32(phy_addr + 16);
	src5 = phx_read_u32(phy_addr + 20);
#endif
	if (type == IBDS_DESC)
		uart_printf("ibds desc%d:[0]0x%x [1]0x%x [2]0x%x [3]0x%x [4]0x%x [5]0x%x\r\n", desc_index, src0, src1, src2, src3, src4, src5);
	else if (type == OBDS_HDR_DESC)
		uart_printf("obds hdr desc%d:[0]0x%x [1]0x%x [2]0x%x [3]0x%x [4]0x%x [5]0x%x\r\n", desc_index, src0, src1, src2, src3, src4, src5);
	else if (type == OBDS_DATA_DESC)
		uart_printf("obds data desc%d:[0]0x%x [1]0x%x [2]0x%x [3]0x%x [4]0x%x [5]0x%x\r\n", desc_index, src0, src1, src2, src3, src4, src5);
	else
		uart_printf("Error desc type!\r\n");
#endif
}

enum {DataDescriptor = 0, HeaderDescriptor = 1};
enum {MultiDescriptor = 0, SingleDescriptor = 1};

void create_one_obds_hdr_desc(struct rab_obds_hdr_desc *p_hdr,
		u32 dest_id, u16 stream_id, u8 priority,
		u8 class_of_service, u8 crf,
		u8 end_of_chain,
		u8 single_descriptor,
		u64 data_addr, int data_len,
		u8 next_desc_pointer_valid,
		u64 next_desc_addr)
{
	struct rab_obds_hdr_desc hdr = {0};

	hdr.valid				= 1;
	hdr.next_descriptor_pointer_valid	= next_desc_pointer_valid;
	hdr.descriptor_type			= HeaderDescriptor;
	hdr.interrupt_enable			= 1;
	hdr.single_descriptor 			= single_descriptor;
	hdr.end_of_chain			= end_of_chain;
	hdr.dest_id				= dest_id & 0xffff;
	hdr.dest_id_high			= (dest_id >> 16) & 0xffff;
	hdr.stream_id				= stream_id;
	hdr.priority				= priority;
	hdr.class_of_service			= class_of_service;
	hdr.crf					= crf;
	hdr.pdu_length				= data_len;
	hdr.data_address_37_32			= ((data_addr >> 32) & 0x3f);
	hdr.data_address_31_0			= (data_addr & 0xffffffff);
	hdr.next_descriptor_address		= (next_desc_addr >> 5) & 0xffffffff;
	hdr.next_descriptor_address_upper_37	= (next_desc_addr >> 37) & 0x01;

	*p_hdr = hdr;
}

void create_one_obds_data_desc(struct rab_obds_data_desc *p_data,
		u8 sop, u8 eop,
		u64 data_addr, int data_len,
		u8 next_desc_pointer_valid,
		u64 next_desc_addr)
{
	struct rab_obds_data_desc data = {0};

	data.valid				= 1;
	data.next_data_descriptor_pointer_valid	= next_desc_pointer_valid;
	data.descriptor_type			= DataDescriptor;
	data.sop				= sop;
	data.eop				= eop;
	data.data_len				= data_len;
	data.data_address_31_0			= data_addr;
	data.data_address_37_32			= ((data_addr >> 32) & 0x3f);
	data.next_data_descriptor_addr		= (next_desc_addr >> 5) & 0xffffffff;
	data.next_data_descriptor_addr_upper_37 = (next_desc_addr >> 37) & 0x01;

	*p_data = data;
}

int set_rab_ob_dse_desc_addr(u8 controller, u8 index, u64 desc_chain_addr_low)
{
	if (desc_chain_addr_low & 0x1f)
	{
		uart_printf("should be 8dw aligned\r\n");
		return -1;
	}

	rab_page_write(controller, RAB_OBDS_DESC_ADDR(index), desc_chain_addr_low >> 5);
	return 0;
}

void rab_init_ob_dse(u8 controller, u8 index, u64 desc_addr)
{
	set_rab_ob_dse_desc_addr(controller, index, desc_addr);
}

/****************************** inbound data message ******************/

static int ibds_desc_size[] = {
	64 * 1024,
	1 * 1024,
	2 * 1024,
	4 * 1024,
	8 * 1024,
	16 * 1024,
	32 * 1024,
	64 * 1024
};

static int size_2_code(int size)
{
	int i;

	for (i = 0; i < 8; i++)
	{
		if (ibds_desc_size[i] == size)
			return i;
	}

	return -1;
}

void rab_ibds_desc_pack(u32 desc_buf[5], struct rab_ibds_desc *desc)
{
	desc_buf[0] = desc->valid
		| (desc->next_descriptor_pointer_valid << 1)
		| (desc->end_of_chain << 2)
		| (desc->interrupt_enable << 3)
		| (desc->descriptor_size << 4);
	desc_buf[2] = (desc->next_descriptor_address_upper_37 << 24)
		| (desc->destination_address_37_32 << 26);
	desc_buf[3] = 0x00000000;
	desc_buf[4] = desc->destination_address_31_0;
	desc_buf[5] = desc->next_descriptor_address;
}

void rab_ibds_desc_unpack(u32 desc_buf[5], struct rab_ibds_desc *desc)
{
	desc->valid                         = desc_buf[0] & 0x1;
	desc->next_descriptor_pointer_valid = (desc_buf[0] >> 1) & 0x1;
	desc->end_of_chain                  = (desc_buf[0] >> 2) & 0x1;
	desc->interrupt_enable              = (desc_buf[0] >> 3) & 0x1;
	desc->descriptor_size               = (desc_buf[0] >> 4) & 0x7;
	desc->priority                      = (desc_buf[0] >> 7) & 0x3;
	desc->done                          = (desc_buf[0] >> 9) & 0x1;
	desc->data_stream_error             = (desc_buf[0] >> 10) & 0x1;
	desc->axi_error                     = (desc_buf[0] >> 11) & 0x1;
	desc->crf                           = (desc_buf[0] >> 12) & 0x1;
	desc->source_id                     = (desc_buf[0] >> 16) & 0xffff;

	desc->dest_id                       = desc_buf[1] & 0xffff;
	desc->pdu_length                    = (desc_buf[1] >> 16) & 0xffff;

	desc->stream_id                     = desc_buf[2] & 0xffff;
	desc->cos                           = (desc_buf[2] >> 16) & 0xff;
	desc->next_descriptor_address_upper_37 = (desc_buf[2] >> 24) & 0x1;
	desc->destination_address_37_32     = (desc_buf[2] >> 26) & 0x3f;

	desc->dest_id_high                  = desc_buf[3] & 0xffff;
	desc->src_id_high                   = (desc_buf[3] >> 16) & 0xffff;

	desc->destination_address_31_0      = desc_buf[4];

	desc->next_descriptor_address       = desc_buf[5];
}

void create_one_ibds_desc(struct rab_ibds_desc *p_desc,
		int descriptor_size, u64 dest_addr,
		u8 next_desc_pointer_valid,
		u64 next_desc_addr,
		u8 end_of_chain)
{
	struct rab_ibds_desc desc = {0};
	int code = size_2_code(descriptor_size);

	if (code < 0)
	{
		uart_printf("ibds desc size is not correct %d\r\n", descriptor_size);
		*p_desc = desc;
		return;
	}

	desc.valid = 1;
	desc.next_descriptor_pointer_valid = next_desc_pointer_valid;
	desc.end_of_chain = end_of_chain;
	desc.interrupt_enable = 1;
	desc.descriptor_size = code;
	desc.destination_address_31_0 = dest_addr&0xffffffff;
	desc.destination_address_37_32 = (dest_addr >> 32) & 0x3f;
	desc.next_descriptor_address = (next_desc_addr >> 5) & 0xffffffff;
	desc.next_descriptor_address_upper_37 = (next_desc_addr >> 37) & 1;

	*p_desc = desc;
}

u32 get_rab_ob_dse_stat(u8 controller, u8 index)
{
	return rab_page_read(controller, RAB_OBDSE_STAT(index));
}

void enable_rab_ob_dse(u8 controller, u8 index, u8 desc_chain_addr_upper)
{
	rab_page_write(controller, RAB_ODSE_CTRL(index), 0x7 | (desc_chain_addr_upper << 31));
}

/***************************** inbound **************************/

void enable_rab_ib_dse(u8 controller, u8 index)
{
	rab_page_write(controller, RAB_IBDSE_CTRL(index), 0x5); /* prefetch, enable */
}

u32 get_rab_ib_dse_stat(u8 controller, u8 index, u8 *vsid_in_use)
{
	u32 stat;

	stat = rab_page_read(controller, RAB_IBDSE_STAT(index));
	*vsid_in_use = stat & 0x3f;

	return stat;
}

u32 get_rab_ib_dse_vsid_stat(u8 controller, u8 index,
		u8 *engine_number_in_use)
{
	u32 stat;

	stat = rab_page_read(controller, RAB_IBDSE_VSID_STAT(index));
	*engine_number_in_use = (stat >> 16) & 0x1f;

	return stat;
}

void set_rab_ib_dse_vsid_alias(u8 controller, u32 avsid)
{
	rab_page_write(controller, RAB_IBDS_VSID_ALIAS, avsid);
}

int set_rab_ib_dse_vsid_low_addr(u8 controller, u8 index,
		u64 data_stream_desc_chain_addr_low)
{
	if (data_stream_desc_chain_addr_low & 0x1f)
	{
		uart_printf("should be 8dw aligned\r\n");
		return -1;
	}

	rab_page_write(controller, RAB_IBDS_VSID_LOW_ADDR(index),
		(data_stream_desc_chain_addr_low >> 5) & 0xffffffff);
	return 0;
}

void set_rab_ib_dse_vsid_high_addr(
	u8 controller, u8 index,
	u8 data_stream_desc_chain_addr_upper,
	u8 vsid_desc_chain_prefetch_enable,
	u8 vsid_desc_chain_prefetch_wakeup)
{
	u32 addr;

	addr = (data_stream_desc_chain_addr_upper & 0x1) |
		((vsid_desc_chain_prefetch_enable & 0x1) << 1) |
		((vsid_desc_chain_prefetch_wakeup & 0x1) << 2);

	rab_page_write(controller, RAB_IBDS_VSID_HIGH_ADDR(index), addr);
}


void set_rio_ds_mtu(u8 controller, u16 mtu)
{
	rab_page_write(controller, GRIO_DSLLCCSR, (mtu / 4) << RIO_DS_MTU_OFFSET);
}

int get_rio_ds_mtu(u8 controller)
{
	u32 dsllccsr;

	dsllccsr = rab_page_read(controller, GRIO_DSLLCCSR);

	return (dsllccsr >> RIO_DS_MTU_OFFSET) * 4;
}

void set_rab_intr_enab_obdse(u8 controller, u32 obdse_intr_enab)
{
	rab_page_write(controller, RAB_INTR_ENAB_OBDSE, obdse_intr_enab);
}

void set_rab_intr_enab_ibds(u8 controller, u32 ibds_intr_enab)
{
	rab_page_write(controller, RAB_INTR_ENAB_IBDS_LO, ibds_intr_enab);
}

void test_ob_single_obds(u8 controller, u8 obdse_index,
		u64 desc_addr, u64 data_addr, int data_len, u16 dest_id)
{
	struct rab_obds_hdr_desc desc;
	u16 stream_id;
	u8 prio, cos, crf;
	u8 single_desc = 1;
	u8 next_desc_v = 1;
	u8 end_of_chain;
	int len_per_desc = 32;
	u32 desc_buf[8];
	int i;
	u32 src0, src1, src2, src3, src4, src5;

	prio = cos = crf = 0;

	set_rab_intr_enab_obdse(controller, 0xffff);
	rab_init_ob_dse(controller, obdse_index, desc_addr);

#if DS_DEBUG_INFO
	uart_printf("single obds desc_addr:0x%x   data_addr:0x%x  \r\n", desc_addr, data_addr);
#endif

	stream_id = 0x5555;
	end_of_chain = 0;
	create_one_obds_hdr_desc(&desc, dest_id, stream_id, prio, cos,
			crf, end_of_chain, single_desc,
			data_addr, data_len,
			next_desc_v, desc_addr + len_per_desc);
	rab_obds_hdr_desc_pack(desc_buf, &desc);
	copy_desc_to_physical_address(desc_addr, desc_buf, 6, OBDS_HDR_DESC, 0);

	desc_addr += len_per_desc;
	data_addr += data_len;
	stream_id = 0xaaaa;
	next_desc_v = 1;
	end_of_chain = 1;
	create_one_obds_hdr_desc(&desc, dest_id, stream_id, prio, cos,
			crf, end_of_chain, single_desc,
			data_addr, data_len,
			next_desc_v, desc_addr + len_per_desc);
	rab_obds_hdr_desc_pack(desc_buf, &desc);
	copy_desc_to_physical_address(desc_addr, desc_buf, 6, OBDS_HDR_DESC, 1);

	enable_rab_ob_dse(controller, obdse_index, (desc_addr >> 37) & 1);
}

void test_ob_multi_obds(u8 controller, u8 obdse_index,
		u64 desc_addr, u64 data_addr, int data_len, u16 dest_id)
{
	struct rab_obds_hdr_desc hdr;
	struct rab_obds_data_desc data;
	u16 stream_id;
	u8 prio, cos, crf;
	u8 single_desc = 0;
	u8 next_desc_v = 1;
	u8 end_of_chain;
	int len_per_desc = 32;
	u32 desc_buf[8];
	int i;
	u8 sop, eop;
	u32 src0, src1, src2, src3, src4, src5;

	prio = cos = crf = 0;

	set_rab_intr_enab_obdse(controller, 0xffff);
	rab_init_ob_dse(controller, obdse_index, desc_addr);

#if DS_DEBUG_INFO
	uart_printf("multi obds desc_addr:0x%x   data_addr:0x%x  \r\n", desc_addr, data_addr);
#endif
	stream_id = 2222;
	end_of_chain = 1;
	create_one_obds_hdr_desc(&hdr, dest_id, stream_id, prio, cos,
			crf, end_of_chain, single_desc, data_addr, data_len,
			next_desc_v, desc_addr + len_per_desc);
	rab_obds_hdr_desc_pack(desc_buf, &hdr);
	copy_desc_to_physical_address(desc_addr, desc_buf, 6, OBDS_HDR_DESC, 0);

	desc_addr += len_per_desc;
	sop = 1;
	eop = 0;
	create_one_obds_data_desc(&data, sop, eop, data_addr,
			data_len, next_desc_v,
			desc_addr + len_per_desc );
	rab_obds_data_desc_pack(desc_buf, &data);
	copy_desc_to_physical_address(desc_addr, desc_buf, 5, OBDS_DATA_DESC, 0);

	desc_addr += len_per_desc;
	data_addr += data_len;

	sop = 0;
	eop = 1;
	next_desc_v =1;
	create_one_obds_data_desc(&data, sop, eop, data_addr,
			data_len, next_desc_v,
			desc_addr + len_per_desc);
	rab_obds_data_desc_pack(desc_buf, &data);
	copy_desc_to_physical_address(desc_addr, desc_buf, 5, OBDS_DATA_DESC, 1);

	enable_rab_ob_dse(controller, obdse_index, (desc_addr >> 37) & 1);
}


void test_ib_data_stream(u8 controller, u16 source_id)
{
	struct rab_ibds_desc desc;
	u8 m_index;
	u64 data_addr;
	int data_size = 0x1000;
	u64 desc_addr;
	u8 end_of_chain;
	u8 next_point_v = 1;
	u32 size_per_desc = 32;
	u32 desc_buf[6];
	int i;
	u32 src0, src1, src2, src3, src4, src5;

	set_rab_intr_enab_ibds(controller, 0xffff);

	//set_rab_ib_dse_vsid_alias(controller, source_id<<8);

	m_index = 0;
	desc_addr = RAB_IB_DSE_DESC_START(controller);
	data_addr = RAB_IB_DSE_DATA_START(controller);
#if DS_DEBUG_INFO
	uart_printf("ibds desc_addr:0x%x   data_addr:0x%x  \r\n", desc_addr, data_addr);
#endif
	end_of_chain = 0;
	set_rab_ib_dse_vsid_low_addr(controller, m_index, desc_addr);

	create_one_ibds_desc(&desc, data_size, data_addr, next_point_v,
			desc_addr+size_per_desc, end_of_chain);
	rab_ibds_desc_pack(desc_buf, &desc);
	copy_desc_to_physical_address(desc_addr, desc_buf, 6, IBDS_DESC, 0);

	desc_addr += size_per_desc;
	data_addr += data_size;
	create_one_ibds_desc(&desc, data_size, data_addr, next_point_v,
			desc_addr+size_per_desc, end_of_chain);
	rab_ibds_desc_pack(desc_buf, &desc);
	copy_desc_to_physical_address(desc_addr, desc_buf, 6, IBDS_DESC, 1);

	desc_addr += size_per_desc;
	data_addr += data_size;
	end_of_chain = 1;
	// this ib desc correspond to multi obds that will send 0x1000 * 2 bytes data , so it needs data_size * 2
	create_one_ibds_desc(&desc, data_size * 2, data_addr, next_point_v,
			desc_addr+size_per_desc, end_of_chain);
	rab_ibds_desc_pack(desc_buf, &desc);
	copy_desc_to_physical_address(desc_addr, desc_buf, 6, IBDS_DESC, 2);

	set_rab_ib_dse_vsid_high_addr(controller, m_index, (desc_addr >> 37) & 1, 1, 1);

	m_index = 1;
	desc_addr += size_per_desc;
	data_addr += (2 * data_size);
	end_of_chain = 0;
	set_rab_ib_dse_vsid_low_addr(controller, m_index, desc_addr);

	create_one_ibds_desc(&desc, data_size, data_addr, next_point_v,
			desc_addr+size_per_desc , end_of_chain);
	rab_ibds_desc_pack(desc_buf, &desc);
	copy_desc_to_physical_address(desc_addr, desc_buf, 6, IBDS_DESC, 3);

	desc_addr += size_per_desc;
	data_addr += data_size;
	end_of_chain = 1;
	create_one_ibds_desc(&desc, data_size, data_addr, next_point_v,
			desc_addr+size_per_desc, end_of_chain);
	rab_ibds_desc_pack(desc_buf, &desc);
	copy_desc_to_physical_address(desc_addr, desc_buf, 6, IBDS_DESC, 4);

	set_rab_ib_dse_vsid_high_addr(controller, m_index, (desc_addr >> 37) & 1, 1, 1);

	enable_rab_ib_dse(controller, 0);
}
