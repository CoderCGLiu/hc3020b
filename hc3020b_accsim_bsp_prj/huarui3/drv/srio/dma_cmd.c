#include "rab.h"
#include "srio.h"

/*
#include "rab_amap.c"
#include "rab_dma.c"
#include "rab_int.c"
*/

#define RAB0_ID 0x3333
#define RAB1_ID 0x9999
#define SPEEDUP_ENABLE 1
#define SPEEDUP_DISABLE 0

void dma_main()
{
	sriox SRIO_0, SRIO_1;
	u32 tmp;

	uart_printf("Test DMA\r\n");

	SRIO_0 = SRIO0;
	SRIO_1 = SRIO1;

	// sysctl release reset : pcie0 pcie1 rab0 rab1
//	phx_write_u32(0x1f0c0028, 0xa5accede); // unlock
//	tmp = phx_read_u32(0x1f0c0008);
//	phx_write_u32(0x1f0c0008, tmp & (~0x0c300000));
//#if 1
//	// flush l3 to ddr -------------------------
//	phx_write_u32(0x1e0c0080, 0x4);
//	phx_write_u32(0x1e0c0010, 0x0);
//	phx_write_u32(0x1e0c1080, 0x4);
//	phx_write_u32(0x1e0c1010, 0x0);
//	phx_write_u32(0x1e0c2080, 0x4);
//	phx_write_u32(0x1e0c2010, 0x0);
//	phx_write_u32(0x1e0c3080, 0x4);
//	phx_write_u32(0x1e0c3010, 0x0);
//#endif

	rab_phy_link_ini_config_10G3125(SRIO_0);
	rab_page_write(SRIO_0, GRIO_BDIDCSR, RAB0_ID);

	rab_phy_link_ini_config_10G3125(SRIO_1);
	rab_page_write(SRIO_1, GRIO_BDIDCSR, RAB1_ID);

#if PHY_BIST_CONFIG 
	rab_phy_bist_check_status(SRIO_0);
	rab_phy_bist_check_status(SRIO_1);

	//rab_phy_bist_tx_inject_error(SRIO_0);
	//rab_phy_bist_tx_inject_error(SRIO_1);

#endif

	rab_wait_for_linkup(SRIO_0);
	rab_wait_for_linkup(SRIO_1);

#if 1
	rab_int_Init(0);
	rab_int_Init(1);
#else
	rab_page_write(SRIO_0, RAB_CTRL, RAB_CTRL_AMBA_PIO_ENABLE | RAB_CTRL_RIO_PIO_ENABLE |
			RAB_CTRL_WR_DMA_ENABLE | RAB_CTRL_RD_DMA_ENABLE);
	rab_page_write(SRIO_1, RAB_CTRL, RAB_CTRL_AMBA_PIO_ENABLE | RAB_CTRL_RIO_PIO_ENABLE |
			RAB_CTRL_WR_DMA_ENABLE | RAB_CTRL_RD_DMA_ENABLE);
#endif
	set_rab_rpio_ctrl(0, 0, 0x3);
	set_rab_rpio_ctrl(1, 0, 0x3);
	set_rab_apio_ctrl(0, 0, 0x3);
	set_rab_apio_ctrl(1, 0, 0x3);

	rio_set_dma_win(RAB0_ID, RAB1_ID);

	//rab_dma_test(RAB0_ID, RAB1_ID);
	rab_dma_speed_test(RAB0_ID, RAB1_ID);

	rab_link_and_mode_status(SRIO_0);
	rab_link_and_mode_status(SRIO_1);

	uart_printf("DMA test end\r\n");

}
