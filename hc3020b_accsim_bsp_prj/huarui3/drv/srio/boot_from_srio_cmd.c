//#include "srio_debug.h"
#include "srio.h"
#include "rab_amap.h"
/*
#include "rab_amap.c"
#include "rab_dma.c"
#include "rab_int.c"
*/

#define SPEEDUP_ENABLE 1
#define SPEEDUP_DISABLE 0
#define RAB_ID 0x1

// other side's switch: 1. boot from rapidio 2. speed 5G 3. ep mode 
// boot.bin's src code  needs release reset pcie0 pcie1 rab0 rab1
void bootsrio_main(void)
{
	sriox SRIO;
	u32 tmp;
	u32 _memAddr = 0x90000000;
	u16 dest_id = 0xffff;


	uart_printf("Test BOOT FROM SRIO\r\n");
	
	rioLockInit(0);/*ldf 20230404*/
	rioLockInit(1);/*ldf 20230404*/

	// srio_region can't read lpc flash addr(crossbus is not supportted)
	// so copy boot.bin to ddr, read boot.bin from ddr instead
	// boot.bin max size is 0x100000
	for (tmp = 0; tmp < 0x100000; tmp += 4)
		phx_write_cache_u32(_memAddr + tmp, phx_read_u32(0x1fc00000 + tmp));

	SRIO = SRIO0;

	// sysctl release reset : pcie0 pcie1 rab0 rab1
//	phx_write_u32(0x1f0c0028, 0xa5accede); // unlock
//	tmp = phx_read_u32(0x1f0c0008);
//	phx_write_u32(0x1f0c0008, tmp & (~0x0c300000));
//#if 1
//	// flush l3 to ddr -------------------------
//	phx_write_u32(0x1e0c0080, 0x4);
//	phx_write_u32(0x1e0c0010, 0x0);
//	phx_write_u32(0x1e0c1080, 0x4);
//	phx_write_u32(0x1e0c1010, 0x0);
//	phx_write_u32(0x1e0c2080, 0x4);
//	phx_write_u32(0x1e0c2010, 0x0);
//	phx_write_u32(0x1e0c3080, 0x4);
//	phx_write_u32(0x1e0c3010, 0x0);
//#endif

	rab_phy_link_ini_config_5G(SRIO/*, SPEEDUP_ENABLE*/);
	rab_page_write(SRIO, GRIO_BDIDCSR, RAB_ID);

	rab_wait_for_linkup(SRIO);

	rab_page_write(SRIO, RAB_CTRL, RAB_CTRL_AMBA_PIO_ENABLE | RAB_CTRL_RIO_PIO_ENABLE |
			RAB_CTRL_WR_DMA_ENABLE | RAB_CTRL_RD_DMA_ENABLE);
	//rab_int_Init(SRIO); // enable interrupt

	set_rab_rpio_ctrl(SRIO, 0, 0x3);
	set_rab_apio_ctrl(SRIO, 0, 0x5); // maintance
	rab_page_write(SRIO, RAB_IB_PW_CSR, 0x1);

	rio_set_outbound_win(SRIO, 0, dest_id, RAB_OW_LOW_BASE_ADDR(SRIO), 0x00000000, 16 * 0x100000, 0);
	rio_set_inbound_win(SRIO, 0x60000000, _memAddr, 0x10000000); // windowSize 256M

	// maint config dest RAB
	phx_write_u32(RAB_OW_LOW_BASE_ADDR(SRIO) + 0x15C, ENDIAN_REVERT(0x00600000)); // enable tx rx
	phx_write_u32(RAB_OW_LOW_BASE_ADDR(SRIO) + RAB_CTRL, ENDIAN_REVERT(RAB_CTRL_AMBA_PIO_ENABLE | RAB_CTRL_RIO_PIO_ENABLE));
	phx_write_u32(RAB_OW_LOW_BASE_ADDR(SRIO) + RAB_APIO_CTRL(0), ENDIAN_REVERT(0x3));
	phx_write_u32(RAB_OW_LOW_BASE_ADDR(SRIO) + RAB_IB_PW_CSR, ENDIAN_REVERT(0x1));
	// enable apio outbound
	phx_write_u32(RAB_OW_LOW_BASE_ADDR(SRIO) + RAB_APIO_AMAP_SIZE(0), ENDIAN_REVERT(0x1000000));
	phx_write_u32(RAB_OW_LOW_BASE_ADDR(SRIO) + RAB_APIO_AMAP_ABAR(0), ENDIAN_REVERT(0x14000000 >> 10)); // srio boot adr: 0x14000000
	phx_write_u32(RAB_OW_LOW_BASE_ADDR(SRIO) + RAB_APIO_AMAP_RBAR(0), ENDIAN_REVERT(0x60000000 >> 10)); // rio addr: 0x60000000
	phx_write_u32(RAB_OW_LOW_BASE_ADDR(SRIO) + RAB_APIO_AMAP_CTRL(0), ENDIAN_REVERT(0x103)); // dest_id is 0x0001
	// enable srio_boot_ready
	phx_write_u32(RAB_OW_LOW_BASE_ADDR(SRIO) + 0x124, 0xffffffff);

	//set_rab_apio_ctrl(0, 0, 0x3);

	//rab_link_and_mode_status(SRIO);

	// wait dest RAB read boot.bin

}
