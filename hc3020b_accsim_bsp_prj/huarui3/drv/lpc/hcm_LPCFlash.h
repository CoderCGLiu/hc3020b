#ifndef _LPC_FLASH_H_
#define _LPC_FLASH_H_

#if 1

#include "../../h/drvCommon.h"
#define SYS_CTRL		        0x1f0c0000
#define LPC_BASE_ADDR			0x1f908000

#else

#define SYS_CTRL		        0xffffffffbf0c0000//0x1f0c0000
#define LPC_BASE_ADDR			0xffffffffbf908000//0x1f908000

static unsigned int phx_read_u32(unsigned long addr)
{
	return *((volatile unsigned long *)addr);
}
static void phx_write_u32(unsigned long addr,unsigned int data)
{
	*((volatile unsigned long *)addr) = data;
}

#endif


#define REG_SYS_CONFIG1			(SYS_CTRL + 0x08)
#define SYSCFG1_LPC_RST				(0x1 << 9)


#define REG_LPC_ADDR			(LPC_BASE_ADDR + 0x00)
#define REG_LPC_RDATA			(LPC_BASE_ADDR + 0x04)
#define REG_LPC_WDATA			(LPC_BASE_ADDR + 0x08)
#define REG_LPC_CLEAN			(LPC_BASE_ADDR + 0x0c)
#define BIT_INT_OP_END_CLEAN		(0x1 << 0)
#define BIT_INT_ERR_CLEAN		(0x1 << 1)
#define BIT_INT_SIRQ_CLEAN		(0x1 << 2)
#define BIT_INT_DPRAM_ADDR_CLEAN	(0x1 << 4)

#define REG_LPC_STATE			(LPC_BASE_ADDR + 0x10)
#define BIT_INT_OP_END_STATUS		(0x1 << 0)
#define BIT_INT_ERR_STATUS		(0x1 << 1)
#define BIT_INT_SIRQ_STATUS		(0x1 << 2)
#define BIT_INT_OP_END			(0x1 << 4)
#define BIT_INT_ERR			(0x1 << 5)
#define BIT_INT_SIRQ			(0x1 << 6)
#define BIT_LPC_ERR_TYPE		(0xf << 8)
#define BIT_LPC_IDLE			(0x1 << 12)
#define BIT_DPRAM_DATA_NUM		(0x3f << 16)

#define REG_LPC_CTRL			(LPC_BASE_ADDR + 0x14)
#define BIT_CYCLE_SEL			(0x3 << 0)
#define BIT_RW_CTRL			(0x1 << 2)
#define BIT_OP_START			(0x1 << 3)
#define BIT_MASK_OP_END_INT		(0x1 << 4)
#define BIT_MASK_ERR_INT		(0x1 << 5)
#define BIT_MASK_SIRQ			(0x1 << 6)
#define BIT_OP_DATA_LEN			(0xf << 8)
#define BIT_SIRQ_EN			(0x1 << 13)
#define BIT_SIRQ_MODE			(0x1 << 14)
#define BIT_SIRQ_NUM			(0x1f << 16)

#define REG_LPC_TIME			(LPC_BASE_ADDR + 0x18)
#define REG_LPC_SIRQ_SRC		(LPC_BASE_ADDR + 0x1c)

//#define REG_SYS_CONFIG1			(SYS_CTRL + 0x08)

enum {IO_CYCLE = 1, MEMORY_CYCLE, FW_MEM_CYCLE};
enum {LPC_READ = 0, LPC_WRITE};
enum {ONE_BYTE = 1,
	TWO_BYTES = 2,
	FOUR_BYTES = 0,
	FW_128_BYTES = 15};
enum {SIRQ_CONTINUE_MODE = 0, SIRQ_QUIET_MODE};
enum {FULL_ACCESS = 0,
	WRITE_LOCKED,
	LOCKED_OPEN,
	WRITE_LOCKED_DOWN,
	BLOCK_READ_LOCKED,
	BLOCK_RW_LOCK,
	BLOCK_READ_LOCKED_DOWN,
	BLOCK_RW_LOCK_DOWN
};


//#define writec(v,a)	{ \
//	(*(volatile u8 *)((a)|0xffffffffa0000000) = (v));}
//
//#define writes(v,a)	{ \
//	(*(volatile u16 *)((a)|0xffffffffa0000000) = (v));}
//
//#define writel(v,a)	{ \
//	(*(volatile u32 *)((a)|0xffffffffa0000000) = (v));}
//
//#define readc(c)	(*(volatile u8 *)((c)|0xffffffffa0000000))
//
//#define reads(c)	(*(volatile u16 *)((c)|0xffffffffa0000000))
//
//#define readl(c)	(*(volatile u32 *)((c)|0xffffffffa0000000))
//
//#define phx_write_u8(a,v)  writec(v,a)
//#define phx_read_u8(v)	readc(v)
//#define phx_write_u16(a,v)  writes(v,a)
//#define phx_read_u16(v)	reads(v)
//#define phx_write_u32(a,v)  writel(v,a)
//#define phx_read_u32(v)	readl(v)


#endif /*_LPC_FLASH_H_*/
