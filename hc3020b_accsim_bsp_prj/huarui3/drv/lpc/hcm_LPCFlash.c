#include <stdio.h>
#include <sysLib.h>
#//#include "simpleprintf.h" 
#include "tool/hr3SysTools.h"
#include "hcm_LPCFlash.h"
#include <pthread.h>


static pthread_t muteSem;


static void lpc_usleep(unsigned int loopNum)
{
	unsigned int i = 0;
	unsigned int j = 0;
	
	for(i = 0; i < loopNum; i++)
	{
		j = j + 1;
	}
}

static void lpc_phx_write_mask_bit(unsigned long where, char mask, char val)
{								
	volatile unsigned int tmp = 0;	
	tmp = ((phx_read_u32(where)) & (~(mask))) | ((val) & (mask));
	phx_write_u32(where, tmp);				
}

/*wait for lpc idle*/
static void wait_for_lpc_idle(void)
{
	unsigned int val = 0;
	do {
		val = phx_read_u32(REG_LPC_STATE);
	} while (!(val & BIT_LPC_IDLE));
}
/* write 1 to clear lpc intr and dpram addr */
static void clear_lpc_intr(char intr_bits)
{
	lpc_phx_write_mask_bit(REG_LPC_CLEAN, intr_bits, intr_bits);
	lpc_usleep(5);
	lpc_phx_write_mask_bit(REG_LPC_CLEAN, intr_bits, 0x0);
}

/* mask and disable lpc intr */
static void mask_lpc_intr(char mask_bits)
{
	lpc_phx_write_mask_bit(REG_LPC_CTRL, mask_bits, mask_bits);
}
/* config LPC_addr_reg */
static void config_lpc_addr_reg(unsigned int lpc_addr)
{
	phx_write_u32(REG_LPC_ADDR, lpc_addr);
}
static void config_lpc_op_data_len(char len)
{
	unsigned int tmp = 0;
	tmp = phx_read_u32(REG_LPC_CTRL);
	tmp = tmp | (len << 8);
	phx_write_u32(REG_LPC_CTRL, tmp);
}
static void set_lpc_ctrl_reg(char cycle_sel, char rw, char len)
{
	lpc_phx_write_mask_bit(REG_LPC_CTRL, (BIT_CYCLE_SEL | BIT_RW_CTRL | BIT_OP_DATA_LEN), (cycle_sel) | (rw << 2) | ((unsigned int)len << 8));
}
static void config_and_start_lpc(char cycle_sel,char rw)
{
	lpc_phx_write_mask_bit(REG_LPC_CTRL, (BIT_CYCLE_SEL | BIT_RW_CTRL | BIT_OP_START), 
			(cycle_sel) | (rw << 2) | BIT_OP_START);
}

#if 0
static void unmask_lpc_intr(char unmask_bits)
{
	lpc_phx_write_mask_bit(REG_LPC_CTRL,unmask_bits,0x0);
}
#endif

static void write_lpc_wdata_u8(unsigned char data, char offset)
{
//	printk("write_lpc_wdata_u8:: data=0x%x, offset=0x%x\n",data,offset);
	unsigned int val = (((unsigned int)data) & 0xff) << (offset << 3);
	phx_write_u32(REG_LPC_WDATA, val);
}

#if 1
static void write_lpc_wdata_u32(unsigned int data)
{
	phx_write_u32(REG_LPC_WDATA, data);
}
#endif

static void start_lpc_operation(void)
{
	lpc_phx_write_mask_bit(REG_LPC_CTRL, BIT_OP_START, BIT_OP_START);
}
static unsigned char read_lpc_rdata_u8(char offset)
{
	unsigned int val = phx_read_u32(REG_LPC_RDATA);
	unsigned char data = (unsigned char)((val >> (offset << 3)) & 0xff);
	
//	printk("read_lpc_rdata_u8:: data=0x%x, offset=0x%x\n",data,offset);
	return data;
}

static unsigned short read_lpc_rdata_u16(void)/*ldf 20230523 add:: TODO: 待测试*/
{
	return phx_read_u16(REG_LPC_RDATA);
}

static unsigned int read_lpc_rdata_u32(void)
{
	return phx_read_u32(REG_LPC_RDATA);
}



#if 0
static void sys_reset_lpcmodule(unsigned int reg, unsigned int module)
{
	lpc_phx_write_mask_bit(reg, module, module);
	lpc_usleep(10);
	lpc_phx_write_mask_bit(reg, module, 0x0);
}
#endif

/* lpc flash reset */
void read_array_lpc_flash(unsigned int addr)
{
	config_lpc_addr_reg(addr);
	set_lpc_ctrl_reg(MEMORY_CYCLE, LPC_WRITE, ONE_BYTE);
	write_lpc_wdata_u8((char)0xff, (char)(addr % 4));
	start_lpc_operation();
	wait_for_lpc_idle();
}

void lpc_release_reset(void)
{
	unsigned int rst_ctl;

	phx_write_u32(0x1F0C0000 + 0x28, 0xA5ACCEDE); 
	rst_ctl = phx_read_u32(0x1F0C0000 + 0x08);
	rst_ctl = rst_ctl | (1<<9);
	phx_write_u32(0x1F0C0000 + 0x08, rst_ctl);
	delay_cnt(1000);
	rst_ctl = rst_ctl &(~(1<<9));
	phx_write_u32(0x1F0C0000 + 0x08, rst_ctl);

	phx_write_u32(0x1F0C0000 + 0x18, 0x0); 
}

unsigned char lpcflash_read_id(unsigned int id_addr)
{
	unsigned int addr = id_addr;
	unsigned int addr_id = id_addr;
	unsigned int cycle_addr = 0;
	unsigned char id = 0;
	printf("lpc flash addr: 0x%x\n",addr);
	wait_for_lpc_idle();
	clear_lpc_intr(BIT_INT_OP_END_CLEAN | BIT_INT_ERR_CLEAN | BIT_INT_SIRQ_CLEAN | BIT_INT_DPRAM_ADDR_CLEAN);	
	config_lpc_op_data_len(ONE_BYTE);
	/* first bus cycle */
	cycle_addr = ((addr & 0xffff0000) | 0x5555);
	config_lpc_addr_reg(cycle_addr);
	write_lpc_wdata_u8((char)0xaa,(char)(cycle_addr % 4));
	config_and_start_lpc(MEMORY_CYCLE, LPC_WRITE);
	lpc_usleep(10);
	wait_for_lpc_idle();
	/* second bus cycle */
	cycle_addr = ((addr & 0xffff0000) | 0x2aaa);
	clear_lpc_intr(BIT_INT_OP_END_CLEAN | BIT_INT_ERR_CLEAN | BIT_INT_SIRQ_CLEAN | BIT_INT_DPRAM_ADDR_CLEAN);
	config_lpc_addr_reg(cycle_addr);
	write_lpc_wdata_u8((char)0x55, (char)(cycle_addr % 4));
	config_and_start_lpc(MEMORY_CYCLE, LPC_WRITE);
	lpc_usleep(10);
	wait_for_lpc_idle();
	/* third bus cycle */
	cycle_addr = ((addr & 0xffff0000) | 0x5555);
	clear_lpc_intr(BIT_INT_OP_END_CLEAN | BIT_INT_ERR_CLEAN | BIT_INT_SIRQ_CLEAN | BIT_INT_DPRAM_ADDR_CLEAN);
	config_lpc_addr_reg(cycle_addr);
	write_lpc_wdata_u8((char)0x90, (char)(cycle_addr % 4));
	config_and_start_lpc(MEMORY_CYCLE, LPC_WRITE);
	lpc_usleep(10);
	wait_for_lpc_idle();
	/* forth bus cycle */
	clear_lpc_intr(BIT_INT_OP_END_CLEAN | BIT_INT_ERR_CLEAN | BIT_INT_SIRQ_CLEAN | BIT_INT_DPRAM_ADDR_CLEAN);
	config_lpc_addr_reg(addr_id);
	config_and_start_lpc(MEMORY_CYCLE, LPC_READ);
	lpc_usleep(10);
	wait_for_lpc_idle();
	
	id = read_lpc_rdata_u8(addr % 4);

	config_lpc_addr_reg(addr_id + 1);
	config_and_start_lpc(MEMORY_CYCLE, LPC_READ);
	lpc_usleep(10);
	wait_for_lpc_idle();
	
	id = id | phx_read_u32(REG_LPC_RDATA);

	/* exit read flash id mode */
	config_lpc_addr_reg(addr);
	write_lpc_wdata_u8((char)0xf0, (char)(addr % 4));
	config_and_start_lpc(MEMORY_CYCLE, LPC_WRITE);
	lpc_usleep(10);
	wait_for_lpc_idle();
	
	return id;
}

static unsigned char lpcflash_read_status(unsigned int addr)
{
	/* first bus cycle */
	config_lpc_addr_reg(addr);
	set_lpc_ctrl_reg(MEMORY_CYCLE, LPC_WRITE, ONE_BYTE);
	write_lpc_wdata_u8(0x70, addr % 4);
	start_lpc_operation();
	wait_for_lpc_idle();

	/* second bus cycle */
	config_lpc_addr_reg(addr);
	set_lpc_ctrl_reg(MEMORY_CYCLE, LPC_READ, ONE_BYTE);
	start_lpc_operation();
	wait_for_lpc_idle();
	return read_lpc_rdata_u8(addr % 4);
}

unsigned char lpcflash_read_byte(unsigned int addr)
{
	pthread_mutex_lock2(&muteSem,WAIT, NO_TIMEOUT);
	
    config_lpc_addr_reg(addr);
	//clear_lpc_intr(BIT_INT_OP_END_CLEAN | BIT_INT_ERR_CLEAN | BIT_INT_SIRQ_CLEAN | BIT_INT_DPRAM_ADDR_CLEAN);
	config_lpc_op_data_len(ONE_BYTE);
	
	config_and_start_lpc(MEMORY_CYCLE, LPC_READ);
	lpc_usleep(10);
	wait_for_lpc_idle();
	
	pthread_mutex_unlock(&muteSem);
	
	return read_lpc_rdata_u8(addr % 4);
}

int lpcflash_poll_check_status(unsigned int addr,char ept)
{
	char d0 = 0;
	char d1 = 0;
	unsigned int count = 0x1000;
	
	do{
		d0 = lpcflash_read_byte(addr);
		d1 = lpcflash_read_byte(addr);
		if((d0 ^ d1) & 0x40)
		{
			count --;
			if(count == 0)
			{
			    printf("err\n\r");
				return -1;
			}
			continue;
		}
		d0 = lpcflash_read_byte(addr);
		if(d0 == ept)
		{
			return 0;
		}
	}while(1);
}

int lpcflash_program_byte(unsigned int addr,char data)
{
	unsigned int cycle_addr = 0;
	
	pthread_mutex_lock2(&muteSem,WAIT, NO_TIMEOUT);
	
	wait_for_lpc_idle();
	clear_lpc_intr(BIT_INT_OP_END_CLEAN | BIT_INT_ERR_CLEAN | BIT_INT_SIRQ_CLEAN | BIT_INT_DPRAM_ADDR_CLEAN);		
	config_lpc_op_data_len(ONE_BYTE);
	/* first bus cycle */
	cycle_addr = ((addr & 0xffff0000) | 0x5555);
	config_lpc_addr_reg(cycle_addr);
	write_lpc_wdata_u8((char)0xaa,(char)(cycle_addr % 4));
	config_and_start_lpc(MEMORY_CYCLE, LPC_WRITE);
	lpc_usleep(10);
	wait_for_lpc_idle();
	/* second bus cycle */
	clear_lpc_intr(BIT_INT_OP_END_CLEAN | BIT_INT_ERR_CLEAN | BIT_INT_SIRQ_CLEAN | BIT_INT_DPRAM_ADDR_CLEAN);
	cycle_addr = ((addr & 0xffff0000) | 0x2aaa);
	config_lpc_addr_reg(cycle_addr);
	write_lpc_wdata_u8((char)0x55, (char)(cycle_addr % 4));
	config_and_start_lpc(MEMORY_CYCLE, LPC_WRITE);
	lpc_usleep(10);
	wait_for_lpc_idle();
	/* third bus cycle */
	clear_lpc_intr(BIT_INT_OP_END_CLEAN | BIT_INT_ERR_CLEAN | BIT_INT_SIRQ_CLEAN | BIT_INT_DPRAM_ADDR_CLEAN);
	cycle_addr = ((addr & 0xffff0000) | 0x5555);
	config_lpc_addr_reg(cycle_addr);
	write_lpc_wdata_u8((char)0xa0, (char)(cycle_addr % 4));
	config_and_start_lpc(MEMORY_CYCLE, LPC_WRITE);
	lpc_usleep(10);
	wait_for_lpc_idle();
	/* forth bus cycle */
	clear_lpc_intr(BIT_INT_OP_END_CLEAN | BIT_INT_ERR_CLEAN | BIT_INT_SIRQ_CLEAN | BIT_INT_DPRAM_ADDR_CLEAN);
	config_lpc_addr_reg(addr);
	write_lpc_wdata_u8((char)data, (char)(addr % 4));
	config_and_start_lpc(MEMORY_CYCLE, LPC_WRITE);
	lpc_usleep(10);
	wait_for_lpc_idle();
	
	pthread_mutex_unlock(&muteSem);
	
	return lpcflash_poll_check_status(addr,(char)data);	
}

int lpcflash_erase_block(unsigned int addr)
{
#if 0
	/* first bus cycle */
	config_lpc_addr_reg(blk_addr);
	set_lpc_ctrl_reg(MEMORY_CYCLE, LPC_WRITE, ONE_BYTE);
	write_lpc_wdata_u8(0x20, blk_addr % 4);
	start_lpc_operation();
	wait_for_lpc_idle();
	
	/* second bus cycle */
	config_lpc_addr_reg(blk_addr);
	set_lpc_ctrl_reg(MEMORY_CYCLE, LPC_WRITE, ONE_BYTE);
	write_lpc_wdata_u8((char)0xd0, (char)(blk_addr % 4));
	start_lpc_operation();
	wait_for_lpc_idle();

	do {
	} while (!(read_lpc_flash_status(blk_addr) & 0x80));

	read_array_lpc_flash(blk_addr);
#else

	unsigned int cycle_addr = 0;
	
	wait_for_lpc_idle();
	clear_lpc_intr(BIT_INT_OP_END_CLEAN | BIT_INT_ERR_CLEAN | BIT_INT_SIRQ_CLEAN | BIT_INT_DPRAM_ADDR_CLEAN);	
	config_lpc_op_data_len(ONE_BYTE);
	/* first bus cycle */
	cycle_addr = ((addr & 0xffff0000) | 0x5555);
	config_lpc_addr_reg(cycle_addr);
	write_lpc_wdata_u8((char)0xaa,(char)(cycle_addr % 4));
	config_and_start_lpc(MEMORY_CYCLE, LPC_WRITE);
	lpc_usleep(10);
	wait_for_lpc_idle();
	/* second bus cycle */
	clear_lpc_intr(BIT_INT_OP_END_CLEAN | BIT_INT_ERR_CLEAN | BIT_INT_SIRQ_CLEAN | BIT_INT_DPRAM_ADDR_CLEAN);	
	cycle_addr = ((addr & 0xffff0000) | 0x2aaa);
	config_lpc_addr_reg(cycle_addr);
	write_lpc_wdata_u8((char) 0x55, (char) (cycle_addr % 4));
	config_and_start_lpc(MEMORY_CYCLE, LPC_WRITE);
	lpc_usleep(10);
	wait_for_lpc_idle();
	/* third bus cycle */
	clear_lpc_intr(BIT_INT_OP_END_CLEAN | BIT_INT_ERR_CLEAN | BIT_INT_SIRQ_CLEAN | BIT_INT_DPRAM_ADDR_CLEAN);	
	cycle_addr = ((addr & 0xffff0000) | 0x5555);
	config_lpc_addr_reg(cycle_addr);
	write_lpc_wdata_u8((char) 0x80, (char) (cycle_addr % 4));
	config_and_start_lpc(MEMORY_CYCLE, LPC_WRITE);
	lpc_usleep(10);
	wait_for_lpc_idle();
	/* forth bus cycle */
	clear_lpc_intr(BIT_INT_OP_END_CLEAN | BIT_INT_ERR_CLEAN | BIT_INT_SIRQ_CLEAN | BIT_INT_DPRAM_ADDR_CLEAN);	
	cycle_addr = ((addr & 0xffff0000) | 0x5555);
	config_lpc_addr_reg(cycle_addr);
	write_lpc_wdata_u8((char) 0xaa, (char) (cycle_addr % 4));
	config_and_start_lpc(MEMORY_CYCLE, LPC_WRITE);
	lpc_usleep(10);
	wait_for_lpc_idle();
	/* fifth bus cycle */
	clear_lpc_intr(BIT_INT_OP_END_CLEAN | BIT_INT_ERR_CLEAN | BIT_INT_SIRQ_CLEAN | BIT_INT_DPRAM_ADDR_CLEAN);	
	cycle_addr = ((addr & 0xffff0000) | 0x2aaa);
	config_lpc_addr_reg(cycle_addr);
	write_lpc_wdata_u8((char) 0x55, (char) (cycle_addr % 4));
	config_and_start_lpc(MEMORY_CYCLE, LPC_WRITE);
	lpc_usleep(10);
	wait_for_lpc_idle();
	/* sixth bus cycle */
	clear_lpc_intr(BIT_INT_OP_END_CLEAN | BIT_INT_ERR_CLEAN | BIT_INT_SIRQ_CLEAN | BIT_INT_DPRAM_ADDR_CLEAN);	
	config_lpc_addr_reg(addr);
	write_lpc_wdata_u8((char) 0x50, (char) (addr % 4));
	config_and_start_lpc(MEMORY_CYCLE, LPC_WRITE);
	lpc_usleep(10);
	wait_for_lpc_idle();
	
	return lpcflash_poll_check_status(addr,(char)0xff);
#endif
}

int lpcflash_erase_sector(unsigned int addr)
{
	unsigned int cycle_addr = 0;
	
	wait_for_lpc_idle();
	clear_lpc_intr(BIT_INT_OP_END_CLEAN | BIT_INT_ERR_CLEAN | BIT_INT_SIRQ_CLEAN | BIT_INT_DPRAM_ADDR_CLEAN);	
	config_lpc_op_data_len(ONE_BYTE);
	/* first bus cycle */
	cycle_addr = ((addr & 0xffff0000) | 0x5555);
	config_lpc_addr_reg(cycle_addr);
	write_lpc_wdata_u8((char)0xaa,(char)(cycle_addr % 4));
	config_and_start_lpc(MEMORY_CYCLE, LPC_WRITE);
	lpc_usleep(10);
	wait_for_lpc_idle();
	/* second bus cycle */
	clear_lpc_intr(BIT_INT_OP_END_CLEAN | BIT_INT_ERR_CLEAN | BIT_INT_SIRQ_CLEAN | BIT_INT_DPRAM_ADDR_CLEAN);	
	cycle_addr = ((addr & 0xffff0000) | 0x2aaa);
	config_lpc_addr_reg(cycle_addr);
	write_lpc_wdata_u8((char) 0x55, (char) (cycle_addr % 4));
	config_and_start_lpc(MEMORY_CYCLE, LPC_WRITE);
	lpc_usleep(10);
	wait_for_lpc_idle();
	/* third bus cycle */
	clear_lpc_intr(BIT_INT_OP_END_CLEAN | BIT_INT_ERR_CLEAN | BIT_INT_SIRQ_CLEAN | BIT_INT_DPRAM_ADDR_CLEAN);	
	cycle_addr = ((addr & 0xffff0000) | 0x5555);
	config_lpc_addr_reg(cycle_addr);
	write_lpc_wdata_u8((char) 0x80, (char) (cycle_addr % 4));
	config_and_start_lpc(MEMORY_CYCLE, LPC_WRITE);
	lpc_usleep(10);
	wait_for_lpc_idle();
	/* forth bus cycle */
	clear_lpc_intr(BIT_INT_OP_END_CLEAN | BIT_INT_ERR_CLEAN | BIT_INT_SIRQ_CLEAN | BIT_INT_DPRAM_ADDR_CLEAN);	
	cycle_addr = ((addr & 0xffff0000) | 0x5555);
	config_lpc_addr_reg(cycle_addr);
	write_lpc_wdata_u8((char) 0xaa, (char) (cycle_addr % 4));
	config_and_start_lpc(MEMORY_CYCLE, LPC_WRITE);
	lpc_usleep(10);
	wait_for_lpc_idle();
	/* fifth bus cycle */
	clear_lpc_intr(BIT_INT_OP_END_CLEAN | BIT_INT_ERR_CLEAN | BIT_INT_SIRQ_CLEAN | BIT_INT_DPRAM_ADDR_CLEAN);	
	cycle_addr = ((addr & 0xffff0000) | 0x2aaa);
	config_lpc_addr_reg(cycle_addr);
	write_lpc_wdata_u8((char) 0x55, (char) (cycle_addr % 4));
	config_and_start_lpc(MEMORY_CYCLE, LPC_WRITE);
	lpc_usleep(10);
	wait_for_lpc_idle();
	/* sixth bus cycle */
	clear_lpc_intr(BIT_INT_OP_END_CLEAN | BIT_INT_ERR_CLEAN | BIT_INT_SIRQ_CLEAN | BIT_INT_DPRAM_ADDR_CLEAN);	
	config_lpc_addr_reg(addr);
	write_lpc_wdata_u8((char) 0x30, (char) (addr % 4));
	config_and_start_lpc(MEMORY_CYCLE, LPC_WRITE);
	lpc_usleep(10);
	wait_for_lpc_idle();
	
	return lpcflash_poll_check_status(addr,(char)0xff);
}


/*  lpc_read_data
 * 功能：lpc总线读接口
 * 输入：
 * 		addr   	lpc起始地址
 * 		byte_mode   FOUR_BYTES = 0 , ONE_BYTE = 1
 * 		cycle	连续读取的次数
 * 输出：
 * 		buf
 * 返回值：
 * 	 	函数执行成功，返回0，失败返回-1。
 */
int lpc_read_data(unsigned int lpc_addr, unsigned int byte_mode, void* buf, int cycle)
{
	pthread_mutex_lock2(&muteSem,WAIT, NO_TIMEOUT);
	
	int i = 0;
//	printk("<***DEBUG***> [%s:_%d_]:: lpc_addr: 0x%lx\n",__FUNCTION__,__LINE__,lpc_addr);
	
	if ((byte_mode == TWO_BYTES) && (lpc_addr % 2)) 
	{
		printf("lpc addr must aligned with 2 bytes!\n");
		return -1;
	}
	else if ((byte_mode == FOUR_BYTES) && (lpc_addr % 4)) 
	{
		printf("lpc addr must aligned with 4 bytes!\n");
		return -1;
	}
	
	wait_for_lpc_idle();
	clear_lpc_intr(BIT_INT_OP_END_CLEAN | BIT_INT_ERR_CLEAN | BIT_INT_SIRQ_CLEAN | BIT_INT_DPRAM_ADDR_CLEAN);
//	mask_lpc_intr(BIT_MASK_OP_END_INT | BIT_MASK_ERR_INT | BIT_MASK_SIRQ);/*ldf 20230608 ignored*/

	
	if(byte_mode == ONE_BYTE)
	{
		for(i=0;i<cycle;i++)
		{
			config_lpc_addr_reg(lpc_addr+i);
#if 0
			set_lpc_ctrl_reg(MEMORY_CYCLE, LPC_READ, byte_mode);
			start_lpc_operation();
#else /*ldf 20230607 add*/
			config_lpc_op_data_len(byte_mode);
			config_and_start_lpc(MEMORY_CYCLE, LPC_READ);
#endif
			lpc_usleep(10);/*ldf 20230607 add*/
			wait_for_lpc_idle();
			*(unsigned char*)(buf+i) = read_lpc_rdata_u8((char)((lpc_addr+i) % 4));
//			printk("0x%lx read lpc: 0x%x\n",(lpc_addr+i), *(unsigned char*)(buf+i));
		}
	}
#if 0 /*ldf 20230608:: 暂时只用到 1byte 模式*/
	else if(byte_mode == TWO_BYTES)/*ldf 20230523 add:: TODO: 待测试*/
	{
//		*(unsigned short*)buf = read_lpc_rdata_u16();
//		printk("read lpc: 0x%x\n", *(unsigned short*)buf);
	}	
	else if(byte_mode == FOUR_BYTES)
	{
		for(i=0;i<cycle;i++)
		{
			lpc_addr  = lpc_addr + i*4;
			*(unsigned int*)buf = read_lpc_rdata_u32();
//			printk("read lpc: 0x%x\n", *(unsigned int*)buf);
		}
	}
#endif
	pthread_mutex_unlock(&muteSem);
	return 0;
}

/*  lpc_write_data
 * 功能：lpc总线写接口
 * 输入：
 * 		addr   	lpc起始地址
 * 		byte_mode   FOUR_BYTES = 0 , ONE_BYTE = 1
 * 		buf: 要写入的数据
 * 		cycle	连续写入次数
 * 输出：
 * 				
 * 返回值：
 * 	 	函数执行成功，返回0，失败返回-1。
 */
int lpc_write_data(unsigned int lpc_addr, unsigned int byte_mode, void* buf, int cycle)
{
	pthread_mutex_lock2(&muteSem,WAIT, NO_TIMEOUT);
	
	int i = 0;
//	printk("<***DEBUG***> [%s:_%d_]:: lpc_addr: 0x%lx\n",__FUNCTION__,__LINE__,lpc_addr);
	
	if ((byte_mode == TWO_BYTES) && (lpc_addr % 2)) 
	{
		printf("lpc addr must aligned with 2 bytes!\n");
		return -1;
	}
	else if ((byte_mode == FOUR_BYTES) && (lpc_addr % 4)) 
	{
		printf("lpc addr must aligned with 4 bytes!\n");
		return -1;
	}
	
	wait_for_lpc_idle();
	clear_lpc_intr(BIT_INT_OP_END_CLEAN | BIT_INT_ERR_CLEAN | BIT_INT_SIRQ_CLEAN | BIT_INT_DPRAM_ADDR_CLEAN);
//	mask_lpc_intr(BIT_MASK_OP_END_INT | BIT_MASK_ERR_INT | BIT_MASK_SIRQ);/*ldf 20230608 ignored*/

	/* lpc write operation*/
	if(byte_mode == ONE_BYTE)
	{
		for(i=0;i<cycle;i++)
		{
			config_lpc_addr_reg(lpc_addr+i);
			write_lpc_wdata_u8(*(unsigned char*)(buf+i), (char)((lpc_addr+i)%4));
//			printk("0x%lx write lpc: 0x%x\n", (lpc_addr+i), *(unsigned char*)(buf+i));
#if 0
			set_lpc_ctrl_reg(MEMORY_CYCLE, LPC_WRITE, byte_mode);
			start_lpc_operation();
#else/*ldf 20230607 add*/
			config_lpc_op_data_len(byte_mode);
			config_and_start_lpc(MEMORY_CYCLE, LPC_WRITE);
#endif
			lpc_usleep(10);/*ldf 20230607 add*/
			wait_for_lpc_idle();
		}
	}
#if 0 /*ldf 20230608:: 暂时只用到 1byte 模式*/
	else if(byte_mode == TWO_BYTES)/*ldf 20230523 add:: TODO: 待测试*/
	{
	}	
	else if(byte_mode == FOUR_BYTES)
	{
		config_lpc_addr_reg(lpc_addr);
		set_lpc_ctrl_reg(MEMORY_CYCLE, LPC_WRITE, FOUR_BYTES);
		write_lpc_wdata_u32(*(unsigned int*)buf);
		start_lpc_operation();
//		config_and_start_lpc(MEMORY_CYCLE, LPC_WRITE);/*ldf 20230607 add*/
//		lpc_usleep(10);/*ldf 20230607 add*/
		wait_for_lpc_idle();
	}
#endif
	
//	pollLPCFlashStatus(addr,(char)data);
	
	pthread_mutex_unlock(&muteSem);
	return 0;
}


/* memory cycle or firmware memory cycle 4 bytes r/w test */
void test_command_4bytes_mode(unsigned int lpc_addr, char cycle_sel)
{
	char i = 0;
	unsigned int data[4] = {0x12345678, 0x5678abcd, 0xabcd1234, 0x5aa5a55a};
	unsigned int tmp_addr = lpc_addr;
	unsigned int read_val = 0;
	
	if (lpc_addr % 4) 
	{
		printf("lpc addr must aligned with 4 bytes!\n");
		return;
	}
	
	wait_for_lpc_idle();
	clear_lpc_intr(BIT_INT_OP_END_CLEAN | BIT_INT_ERR_CLEAN | BIT_INT_SIRQ_CLEAN | BIT_INT_DPRAM_ADDR_CLEAN);
	mask_lpc_intr(BIT_MASK_OP_END_INT | BIT_MASK_ERR_INT | BIT_MASK_SIRQ);
	
	
	/* lpc write first */
	for (i = 0; i < 4; i ++) 
	{
		config_lpc_addr_reg(tmp_addr);
		set_lpc_ctrl_reg(cycle_sel, LPC_WRITE, FOUR_BYTES);
		write_lpc_wdata_u32(data[i]);
		start_lpc_operation();
		wait_for_lpc_idle();
		tmp_addr += 4;
	}

	clear_lpc_intr(BIT_INT_OP_END_CLEAN | BIT_INT_ERR_CLEAN | BIT_INT_SIRQ_CLEAN | BIT_INT_DPRAM_ADDR_CLEAN);
	mask_lpc_intr(BIT_MASK_OP_END_INT | BIT_MASK_ERR_INT | BIT_MASK_SIRQ);
	tmp_addr = lpc_addr;
	/* lpc read operation*/
	for (i = 0; i < 4; i++) 
	{
		config_lpc_addr_reg(tmp_addr);
		set_lpc_ctrl_reg(cycle_sel, LPC_READ, FOUR_BYTES);
		start_lpc_operation();
		wait_for_lpc_idle();
		read_val = read_lpc_rdata_u32();
		printf("read lpc_flash: %x\n", read_val);
		tmp_addr += 4;
	}
}
/* memory cycle or firmware memory cycle 1 bytes r/w test */
void test_command_1bytes_mode(unsigned int lpc_addr, char cycle_sel)
{
    char i = 0;
    unsigned char data[4] = {0x12, 0x56, 0xab, 0x5a};
    unsigned int tmp_addr = lpc_addr;
    unsigned int read_val = 0;


    wait_for_lpc_idle();

    clear_lpc_intr(BIT_INT_OP_END_CLEAN | BIT_INT_ERR_CLEAN | BIT_INT_SIRQ_CLEAN | BIT_INT_DPRAM_ADDR_CLEAN);
    mask_lpc_intr(BIT_MASK_OP_END_INT | BIT_MASK_ERR_INT | BIT_MASK_SIRQ);

    /* lpc read operation*/
    for (i = 0; i < 0x10000; i++)
    {
        config_lpc_addr_reg(tmp_addr);
        set_lpc_ctrl_reg(cycle_sel, LPC_READ, ONE_BYTE);
        start_lpc_operation();
        wait_for_lpc_idle();
        read_val = read_lpc_rdata_u8(tmp_addr % 4);
        if(8 != read_val)
            printf("0x%x read lpc_flash: %x\n",tmp_addr, read_val);
        tmp_addr += 4;
    }
}



void lpc_module_init(void)
{
	/*lpc配置release reset*/
    lpc_release_reset();
    
	wait_for_lpc_idle();
	clear_lpc_intr(BIT_INT_OP_END_CLEAN | BIT_INT_ERR_CLEAN | BIT_INT_SIRQ_CLEAN | BIT_INT_DPRAM_ADDR_CLEAN);	
	config_lpc_op_data_len(ONE_BYTE);
	
	pthread_mutex_init2(&muteSem,PTHREAD_MUTEX_RECURSIVE,
			RE_MUTEX_PRIO_NONE |RE_MUTEX_WAIT_FIFO ,PTHREAD_MUTEX_CEILING);

	return;
}

static int CheckUint8(const unsigned char *pSrc, const unsigned char *pDst, unsigned int length)
{
    int index = 0;
    unsigned char valSrc = 0;
    unsigned char valDst = 0;
    int err_num = 0;
    
    printf(" ##CheckData## pSrc: 0x%016lX, pDst: 0x%016lX, length: %lu = 0x%016lX\n", (unsigned long)pSrc, (unsigned long)pDst, length, length);
    for(index = 0; index < length; index ++)
    {
    	valSrc = *(pSrc + index);
    	valDst = *(pDst + index);
		if(valSrc != valDst)
		{
			err_num++;
			if(err_num <= 0x10)
			{
				printf("<Check Error> index=0x%x, val=0x%x, except_val=0x%x\n",\
						index,valDst,valSrc);
			}
//			return -1;
		}
    }
    
    if(err_num == 0)
    	printf("   Check OK!\n");
    else
    	printf("   Check Failed! err_num = 0x%x\n",err_num);
    
	return err_num;
}
static void DumpUint8(const unsigned char *pAddr, unsigned int length)
{
    int index = 0;

    printf("pAddr: 0x%016lX, length: %lu = 0x%016lX", (unsigned long)pAddr, length, length);
    for (index = 0; index < length; index++)
    {
        if (index % 0x10 == 0)
        {
            printf("\n0x%08lX: ", index);
        }
        printf("0x%02X ", *(unsigned char *)(pAddr + index));
    } 

    printf("\n");
}

void test_lpcflash_rw(unsigned int testAddr, unsigned int test_len, unsigned char first_data, int is_DumpData)
{
	int err_num;
	unsigned char val;
	unsigned char id = 0;
	unsigned int i = 0;
//	unsigned int testAddr = 0xffe52000;
//	unsigned char testData[64] = {0}; 
//	unsigned char rbuf[512] = {0};
//	int test_len = sizeof(testData)/sizeof(testData[0]);
    unsigned char *testData = (unsigned char *)malloc(test_len);
    unsigned char *rbuf = (unsigned char *)malloc(test_len);
    
    memset(testData, 0, test_len);
    memset(rbuf, 0, test_len);

	printf("\n=======LPC FLASH TEST START=========\n");
	
	printf("lpc REG_LPC_TIME: 0x%x\n",phx_read_u32(REG_LPC_TIME));
	/* sys_reset_lpcmodule(REG_SYS_CONFIG1, SYSCFG1_LPC_RST); */ //reset lpc module
	id = lpcflash_read_id(0xffe00000);
	printf("lpc flash manuf_id: 0x%x\n", id);
	
	id = lpcflash_read_id(0xffe00001);
	printf("lpc flash devc_id: 0x%x\n", id);

	printf("\nfirst read =====================\n");
    for(i = 0; i < test_len; i ++)
    {
        rbuf[i] = lpcflash_read_byte(testAddr + i);
    }
    if(is_DumpData)
    	DumpUint8(rbuf, test_len);
    
    lpcflash_erase_sector(testAddr);
	printf("    lpc flash sector erase finished.\n");
	memset(testData, 0xff, test_len);
	printf("\nafter erase =====================\n");
    for(i = 0; i < test_len; i ++)
    {
        rbuf[i] = lpcflash_read_byte(testAddr + i);
    }

    err_num = CheckUint8(testData, rbuf, test_len);
    if(err_num > 0)
    	printf("  LPC Flash Erase Test Failed!, err_num = 0x%x\n",err_num); 
    else
    	printf("  LPC Flash Erase Test OK!\n"); 
    
    if(is_DumpData)
    	DumpUint8(rbuf, test_len);

    
	for(i=0;i<test_len;i++)
	{
		testData[i] = first_data + i;
	}
	for(i = 0; i < test_len; i ++)
	{
		lpcflash_program_byte(testAddr + i,testData[i]);
	}
	printf("    lpc flash byte write finished.\n");
	
	printf("\nafter write =====================\n");
    for(i = 0; i < test_len; i ++)
    {
        rbuf[i] = lpcflash_read_byte(testAddr + i);
    }
    err_num = CheckUint8(testData, rbuf, test_len);
    if(err_num > 0)
    	printf("  LPC Flash Write Test Failed!, err_num = 0x%x\n",err_num); 
    else
    	printf("  LPC Flash Write Test OK!\n"); 
    
    if(is_DumpData)
    	DumpUint8(rbuf, test_len);
    
	printf("\n=======LPC FLASH TEST END=========\n");
	
	free(testData);
	free(rbuf);
}

#if 0
int LpcFlashEraseAll(void)
{
	struct qspi_hc_regs* host = QSPI0_HC_REGS;
	
	libq_write_enable(host);
	libq_flash_wait_write_enable(host);/*ldf 20230614 add*/
	
	libq_bulk_erase(host);
	
	sleep(3);/*ldf 20230612 add*/
	libq_flash_wait_write_idle(host);/*ldf 20230612 add*/
	libq_flash_wait_erase_idle(host);/*ldf 20230613 add*/
	
	return 0;
}
int LpcFlashErase(unsigned int addr, unsigned int len)
{
	unsigned int erase_size = SECTOR_SIZE;
	struct qspi_hc_regs* host = QSPI0_HC_REGS;
	unsigned int erase_addr = 0;
	int erase_loop = 0;
	int i = 0;
	
	if(len < erase_size)
	{
		printf("<ERROR> len(0x%x) is too small! must >= 0x%x\n",len,erase_size);
		return;
	}
	
	erase_loop = len / erase_size;
	
	for(i=0;i<erase_loop;i++)
	{
		erase_addr = addr + i * erase_size;
		
		libq_write_enable(host);
		libq_flash_wait_write_enable(host);/*ldf 20230614 add*/
		
		libq_sector_erase(host, erase_addr, 0);
		
		usleep(10000);/*ldf 20230612 add*/
		libq_flash_wait_write_idle(host);/*ldf 20230612 add*/
		libq_flash_wait_erase_idle(host);/*ldf 20230613 add*/
	}
	
	return 0;
}
unsigned int LpcFlashReadid(void)
{
	struct qspi_hc_regs* host = QSPI0_HC_REGS;
	unsigned int id;;
	
	libq_read_device_id(host, (unsigned char*)&id);
	
	return id;
}
int LpcFlashRead(unsigned int addr, unsigned char* rbuf, unsigned int len)
{
	struct qspi_hc_regs* host = QSPI0_HC_REGS;
	
	libq_read(host, addr, rbuf, len, 0);
}
int LpcFlashWrite(unsigned int addr, unsigned char* wbuf, unsigned int len)
{
	struct qspi_hc_regs* host = QSPI0_HC_REGS;
	
	libq_program(host, addr, wbuf, len, 0);
}



void test_lpcflash_write(unsigned int addr, unsigned int len, unsigned char first_data)
{
    unsigned char *data = (unsigned char *)malloc(len);
    int i = 0;
    
    memset(data, 0, len);
    
    for(i = 0; i < len; i ++)
    {
        data[i] = i + first_data;
//        printf("0x%x ", data[i]);
    }
//    printf("\n");
    
    printf("\nafter write =====================\n");
    QspiFlashWrite(addr, data, len);
    
    free(data);
    
	return;
}

void test_lpcflash_rw(unsigned int addr, unsigned int len, unsigned char first_data, int is_DumpData)
{
    unsigned char *buf = (unsigned char *)malloc(len);
    unsigned char *data = (unsigned char *)malloc(len);
    int i = 0;
    unsigned int retid;
    int err_num = 0;
    
    memset(buf, 0, len);
    memset(data, 0, len);

    printf("\n=======QSPI FLASH TEST START=========\n");
    
    qspi_module_init();

    retid = QspiFlashReadid();
    printf("Qspi flash id= 0x%x\n", retid);

    printf("\nfirst read =====================\n");
    QspiFlashRead(addr, buf, len);
	if(is_DumpData)
		DumpUint8(buf, len);
    
    printf("\nafter erase =====================\n"); 
    QspiFlashErase(addr, len);
    memset(buf, 0, len);
    QspiFlashRead(addr, buf, len);
	if(is_DumpData)
		DumpUint8(buf, len);
    memset(data, 0xff, len);
    err_num = CheckUint8(data, buf, len);
    if(err_num > 0)
    	printf("  Qspi Flash Erase Test Failed!, err_num = 0x%x\n",err_num); 
    else
    	printf("  Qspi Flash Erase Test OK!\n"); 
    
    for(i = 0; i < len; i ++)
    {
        data[i] = i + first_data;
//        printf("0x%x ", data[i]);
    }
//    printf("\n");
    
    printf("\nafter write =====================\n");
    QspiFlashWrite(addr, data, len);
    memset(buf, 0, len);
    QspiFlashRead(addr, buf, len);
	if(is_DumpData)
		DumpUint8(buf, len);
    err_num = CheckUint8(data, buf, len);
    if(err_num > 0)
    	printf("  Qspi Flash Write Test Failed!, err_num = 0x%x\n",err_num); 
    else
    	printf("  Qspi Flash Write Test OK!\n");
	
    printf("\n=======QSPI FLASH TEST END=========\n");
    
    free(buf);
    free(data);
}
#endif


