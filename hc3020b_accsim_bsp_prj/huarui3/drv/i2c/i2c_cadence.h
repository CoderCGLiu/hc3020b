#ifndef _I2C_CDNSI2C_H_
#define _I2C_CDNSI2C_H_

#include "../../h/drvCommon.h"
#include <semLib.h>
#include <semaphore.h>
#include <pthread.h>
#include <fcntl.h>
#include <irq.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#if 1
#include <reworks/types.h>

#ifndef	PUCHAR
#define PUCHAR	u8*
#endif
#ifndef	UINT8
#define UINT8	u8
#endif
#ifndef	UINT16
#define UINT16	u16
#endif
#ifndef	UINT32
#define UINT32	u32
#endif
//#ifndef	UINT64
//#define UINT64	u64
//#endif
#ifndef	U8
#define U8		u8
#endif
#ifndef	U16
#define U16		u16
#endif
#ifndef	U32
#define U32		u32
#endif
#ifndef	U64
#define U64		u64
#endif
//#ifndef	VOID
//#define VOID	void
//#endif
#ifndef	PVOID
#define	PVOID	void*
#endif
#ifndef	INT
#define INT		int
#endif
//#ifndef	BOOL
//#define	BOOL	int
//#endif
//#ifndef	UINT
//#define	UINT	unsigned int
//#endif
//#ifndef	ULONG
//#define	ULONG	unsigned long
//#endif
#ifndef	LONG
#define	LONG	long
#endif
//#ifndef	UCHAR
//#define	UCHAR	unsigned char
//#endif
#ifndef	CHAR
#define	CHAR	char
#endif
#ifndef	CPCHAR
#define CPCHAR	char*
#endif
#ifndef	PCHAR
#define PCHAR	char*
#endif
#ifndef	PX_ERROR
#define	PX_ERROR	-1
#endif
#ifndef	ERROR_NONE
#define	ERROR_NONE	0
#endif
#ifndef	ERROR
#define	ERROR	-1
#endif
#ifndef	OK
#define	OK	0
#endif
#endif

/*********************************************************************************************************
  i2c 控制器相关宏定义
*********************************************************************************************************/
#define I2C_CHAN_NUM                     2
#define I2C_BUS_FREQ_MAX                 (400000)
#define I2C_BUS_FREQ_MIN                 (100000)
/*
 * Register offsets for the I2C device.
 */
#define CDNS_I2C_CR                      (0x00)                         /* Control Register, RW         */
#define CDNS_I2C_SR                      (0x04)                         /* Status Register, RO          */
#define CDNS_I2C_ADDR                    (0x08)                         /* I2C Address Register, RW     */
#define CDNS_I2C_DATA                    (0x0C)                         /* I2C Data Register, RW        */
#define CDNS_I2C_ISR                     (0x10)                         /* IRQ Status Register, RW      */
#define CDNS_I2C_XFER_SIZE               (0x14)                         /* Transfer Size Register, RW   */
#define CDNS_I2C_TIME_OUT                (0x1C)                         /* Time Out Register, RW        */
#define CDNS_I2C_IER                     (0x24)                         /* IRQ Enable Register, WO      */
#define CDNS_I2C_IDR                     (0x28)                         /* IRQ Disable Register, WO     */
/*
 * Control register fields
 */
#define CDNS_I2C_CONTROL_RW              0x00000001
#define CDNS_I2C_CONTROL_MS              0x00000002
#define CDNS_I2C_CONTROL_NEA             0x00000004
#define CDNS_I2C_CONTROL_ACKEN           0x00000008
#define CDNS_I2C_CONTROL_HOLD            0x00000010
#define CDNS_I2C_CONTROL_SLVMON          0x00000020
#define CDNS_I2C_CONTROL_CLR_FIFO        0x00000040
#define CDNS_I2C_CONTROL_DIV_B_SHIFT     8
#define CDNS_I2C_CONTROL_DIV_B_MASK      0x00003F00
#define CDNS_I2C_CONTROL_DIV_A_SHIFT     14
#define CDNS_I2C_CONTROL_DIV_A_MASK      0x0000C000
/*
 * Status register values
 */
#define CDNS_I2C_STATUS_RXDV             0x00000020
#define CDNS_I2C_STATUS_TXDV             0x00000040
#define CDNS_I2C_STATUS_RXOVF            0x00000080
#define CDNS_I2C_STATUS_BA               0x00000100
/*
 * Interrupt register fields
 */
#define CDNS_I2C_INTERRUPT_COMP          0x00000001
#define CDNS_I2C_INTERRUPT_DATA          0x00000002
#define CDNS_I2C_INTERRUPT_NACK          0x00000004
#define CDNS_I2C_INTERRUPT_TO            0x00000008
#define CDNS_I2C_INTERRUPT_SLVRDY        0x00000010
#define CDNS_I2C_INTERRUPT_RXOVF         0x00000020
#define CDNS_I2C_INTERRUPT_TXOVF         0x00000040
#define CDNS_I2C_INTERRUPT_RXUNF         0x00000080
#define CDNS_I2C_INTERRUPT_ARBLOST       0x00000200

#define CDNS_I2C_FIFO_DEPTH              16
#define CDNS_I2C_TRANSFERT_SIZE_MAX      255                            /* Controller transfer limit    */

/*
 * debug
 */
#define CDNS_I2C_INF(fmt, arg...)        //printf("%s() " fmt, __func__, ##arg)

#if BSP_CFG_I2C0_MUX_EN > 0 || BSP_CFG_I2C1_MUX_EN > 0
#define BSP_CFG_I2C_MUX_EN  1
extern INT  boardI2cMuxAddr2Chan(INT iHwCh, UINT16 usAddr);
#else
#define BSP_CFG_I2C0_MUX_EN 0
#endif
/*********************************************************************************************************
  i2c 控制器是否使用中断方式
*********************************************************************************************************/
#define CDNS_I2C_INTER_EN                0/*xxx ldf 20230816  note:: 暂不使用中断方式*/
/*********************************************************************************************************
  i2c 通道类型定义
*********************************************************************************************************/
typedef struct {
//    LW_I2C_FUNCS        I2C_busfuncs;
	UINT32              I2C_ulPhyAddrBase;
    unsigned int              I2C_uiIrqNum;
    unsigned int                I2C_uiBusFreq;
    unsigned int              I2C_uiBusHoldFlag;
    int                 I2C_iHwChan;

    UINT32              I2C_ulVirtAddrBase;
    SEM_ID           	I2C_hSignal;
    int                I2C_bIsInit;
    PUCHAR              I2C_pucBuffer;
    UINT                I2C_uiBufferLen;
    void               *pvSw;

} CDNS_I2C_CHANNEL, *PCDNS_I2C_CHANNEL;

typedef struct{
	u16 I2CMSG_usAddr;
	u8 *I2CMSG_pucBuffer;
	int I2CMSG_usLen;
	int I2CMSG_usFlag;
} LW_I2C_MESSAGE, *PLW_I2C_MESSAGE;

#define LW_I2C_M_RD 1
#define LW_I2C_M_WR 1

#endif                                                                  /*  _I2C_CDNSI2C_H_                  */
/*********************************************************************************************************
  END
*********************************************************************************************************/
