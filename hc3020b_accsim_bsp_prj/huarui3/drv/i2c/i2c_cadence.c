#include "i2c_cadence.h"
#include "hr3_intc_defs.h"

//#define _I2C_DBG_ /*ldf 20320815 add:: debug*/

/*********************************************************************************************************
  全局变量
*********************************************************************************************************/
static pthread_t muteSem;/*ldf 20230816 add*/

static CDNS_I2C_CHANNEL  _G_cdnsI2cChannels[I2C_CHAN_NUM] = {
        {
                .I2C_ulPhyAddrBase = 0x1F0D0000,
                .I2C_uiIrqNum      = INT_I2C0,
                .I2C_uiBusFreq     = I2C_BUS_FREQ_MIN,
                .I2C_bIsInit       = FALSE,
                .I2C_iHwChan       = 0,
        },
#if 1
        {
                .I2C_ulPhyAddrBase = 0x1F0D8000,
                .I2C_uiIrqNum      = INT_I2C1,
                .I2C_uiBusFreq     = I2C_BUS_FREQ_MIN,
                .I2C_bIsInit       = FALSE,
                .I2C_iHwChan       = 0,
        }
#endif
};
/*********************************************************************************************************
** 函数名称: cdnsI2cIsr
** 功能描述: I2C中断服务程序
** 输　入  : pvArg:        参数
**           ulVector:     中断向量
** 输　出  :
** 全局变量:
** 调用模块:
*********************************************************************************************************/
#if CDNS_I2C_INTER_EN > 0
static void  cdnsI2cIsr (VOID *pvArg, ULONG ulVector)
{
    PCDNS_I2C_CHANNEL   pI2cChannel = (PCDNS_I2C_CHANNEL)pvArg;

//    API_SemaphoreBPost(pI2cChannel->I2C_hSignal);
    semGive(pI2cChannel->I2C_hSignal);

    return;
}
#endif
/*********************************************************************************************************
** 函数名称: cdnsI2cWait
** 功能描述: Wait for an interrupt. zynq I2c控制器在非中断模式下工作时等待数据响应函数, 参考zynq_i2c_wait
** 输　入  : atBaseAddr:  I2C 控制器寄存器基地址
**           uiMaskCode:  等待的事件
** 输　出  :
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static UINT32  cdnsI2cWait (UINT32  atBaseAddr, UINT32  uiMaskCode)
{
    INT  iTimeOut, iStatus;

#if CDNS_I2C_INTER_EN > 0
    if (uiMaskCode & CDNS_I2C_INTERRUPT_NACK) {
        for (iTimeOut = 0; iTimeOut < 100; iTimeOut++) {
            udelay(100);
            iStatus = readl(atBaseAddr + CDNS_I2C_ISR);
            if (iStatus & uiMaskCode) {
                break;
            }
        }
    } else {
        if (atBaseAddr == _G_cdnsI2cChannels[0].I2C_ulVirtAddrBase) {
//            API_SemaphoreBPend(_G_cdnsI2cChannels[0].I2C_hSignal, LW_MSECOND_TO_TICK_1(20));
        	semTake(_G_cdnsI2cChannels[0].I2C_hSignal, sysClkRateGet()*1);
        } else {
//            API_SemaphoreBPend(_G_cdnsI2cChannels[1].I2C_hSignal, LW_MSECOND_TO_TICK_1(20));
        	semTake(_G_cdnsI2cChannels[1].I2C_hSignal, sysClkRateGet()*1);
        }

        iStatus = readl(atBaseAddr + CDNS_I2C_ISR);
    }

#elif 1
    for (iTimeOut = 0; iTimeOut < 1000; iTimeOut++) {
        iStatus = readl(atBaseAddr + CDNS_I2C_ISR);
        if (iStatus & uiMaskCode) {
            break;
        }
        usleep(100);
    }
#else /*ldf 20230816 add:: 非中断模式下等待*/
    for (iTimeOut = 0; iTimeOut < 1000; iTimeOut++) {
        iStatus = readl(atBaseAddr + CDNS_I2C_SR);
        if (!(iStatus & uiMaskCode)) {
            break;
        }
        usleep(100);
    }
#endif

    writel(iStatus, (atBaseAddr + CDNS_I2C_ISR));

    return (iStatus & uiMaskCode);
}

/*********************************************************************************************************
** 函数名称: cdnsI2cHwInit
** 功能描述: zynq i2c 控制器初始化函数, 参考 zynq uboot 驱动文件 zynq_i2c.c 中函数 zynq_i2c_init 编写
** 输　入  : atBaseAddr:  I2C 控制器寄存器基地址
**           uiI2cFre:    I2C 控制器需要设置的通信波特率
** 输　出  :
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  cdnsI2cHwInit (UINT32  atBaseAddr, UINT8  ucDevAddr, UINT32  uiI2cFre)
{
    UINT32  uiRegVal;

    /*
     * 111MHz / ( (3 * 17) * 22 ) = ~100KHz
     */
    writel((16 << CDNS_I2C_CONTROL_DIV_B_SHIFT) |
            (2 << CDNS_I2C_CONTROL_DIV_A_SHIFT), (atBaseAddr + CDNS_I2C_CR));

    /*
     * Enable master mode, ack, and 7-bit addressing
     */
    uiRegVal = readl(atBaseAddr + CDNS_I2C_CR) | CDNS_I2C_CONTROL_MS |
                        CDNS_I2C_CONTROL_ACKEN | CDNS_I2C_CONTROL_NEA;
    writel(uiRegVal, (atBaseAddr + CDNS_I2C_CR));

    /*
     * Attempt to read a byte
     */
    uiRegVal = readl(atBaseAddr + CDNS_I2C_CR) | CDNS_I2C_CONTROL_CLR_FIFO | CDNS_I2C_CONTROL_RW;
    writel(uiRegVal, (atBaseAddr + CDNS_I2C_CR));

    uiRegVal = readl(atBaseAddr + CDNS_I2C_CR) & (~CDNS_I2C_CONTROL_HOLD);
    writel(uiRegVal, (atBaseAddr + CDNS_I2C_CR));

    writel(0xFF, (atBaseAddr + CDNS_I2C_ISR));
    writel(ucDevAddr, (atBaseAddr + CDNS_I2C_ADDR));
    writel(1, (atBaseAddr + CDNS_I2C_XFER_SIZE));

#if CDNS_I2C_INTER_EN > 0
    writel(0xff, atBaseAddr + CDNS_I2C_IER);
#endif

//#if CDNS_I2C_INTER_EN > 0
    return (cdnsI2cWait(atBaseAddr, CDNS_I2C_INTERRUPT_COMP | CDNS_I2C_INTERRUPT_NACK) &
                                    CDNS_I2C_INTERRUPT_COMP) ? 0 : -1;
//#else/*ldf 20230816 add*/
//    return 0;
//#endif

}


/*********************************************************************************************************
** 函数名称: cdnsI2cWrite
** 功能描述: I2C 控制器向设备写入数据函数
** 输　入  : atBaseAddr  控制器基地址
**           ucDevAddr   I2C 设备的设备地址
**           ucWriteBuf  向I2C 设备写入的数据缓冲区
**           iBufLen    需要向 I2C 设备写入的数据长度
** 输　出  : 成功返回 ERROR_NONE, 错误返回错误信息
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  cdnsI2cWrite (UINT32  atBaseAddr, UINT8  ucDevAddr, UINT8  *ucWriteBuf, UINT32  uiBufLen)
{
    UINT32  uiRegVal;
    UINT8  *ucCurData = ucWriteBuf;
    
#ifdef _I2C_DBG_ /*ldf 20320815 add:: debug*/
    	int ii = 0;
		printk("\n=====[%s():_%d_]:: write============Start\n", __FUNCTION__, __LINE__);
		printk("dev_addr:0x%x\n", ucDevAddr);
		printk("buflen:0x%x\n", uiBufLen);
		printk("buf: ");
		for (ii=0; ii< uiBufLen; ii++)
		{
			printk("0x%02x ", ucWriteBuf[ii]);
		}
		printk("\n========================================End\n");
#endif

    CDNS_I2C_INF("dev = 0x%02x, req size = %d\r\n", ucDevAddr, uiBufLen);

    uiRegVal = readl(atBaseAddr + CDNS_I2C_CR) | CDNS_I2C_CONTROL_CLR_FIFO;
    writel(uiRegVal, (atBaseAddr + CDNS_I2C_CR));

    uiRegVal = readl(atBaseAddr + CDNS_I2C_CR) & (~(CDNS_I2C_CONTROL_RW));
    writel(uiRegVal, (atBaseAddr + CDNS_I2C_CR));

    writel(0xFF, (atBaseAddr + CDNS_I2C_ISR));

    /*xxx ldf 20230816 note:: 不清楚这里为什么在起始信号之前要先发一字节数据，先注释掉*/
//    writel(*ucCurData, (atBaseAddr + CDNS_I2C_DATA));
//    ucCurData++;
//    uiBufLen--;

    writel(ucDevAddr, (atBaseAddr + CDNS_I2C_ADDR));                    /* Start the tranfer            */

    while (uiBufLen--) {
        writel(*(ucCurData++), (atBaseAddr + CDNS_I2C_DATA));

        if (readl(atBaseAddr + CDNS_I2C_XFER_SIZE) == CDNS_I2C_FIFO_DEPTH) {
            if (!cdnsI2cWait(atBaseAddr, CDNS_I2C_INTERRUPT_COMP)) {    /* if error Release the bus     */
                uiRegVal = readl(atBaseAddr + CDNS_I2C_CR) & (~(CDNS_I2C_CONTROL_HOLD));
                writel(uiRegVal, (atBaseAddr + CDNS_I2C_CR));
                //printf("cdnsI2cWrite() timeout, addr = 0x%02x, size = %d.\r\n", ucDevAddr, uiBufLen);
                return  (-1);
            }
        }
    }
    
    /* Wait for  data to be sent    */
//#if CDNS_I2C_INTER_EN > 0
    if (!cdnsI2cWait(atBaseAddr, CDNS_I2C_INTERRUPT_COMP)) 
//#else/*ldf 20230816 add*/
//	if (cdnsI2cWait(atBaseAddr, CDNS_I2C_STATUS_TXDV))
//#endif
    {            
        //printf("cdnsI2cWrite() wait timeout, addr = 0x%02x, size = %d.\r\n", ucDevAddr, uiBufLen);
        return  (-1);
    }

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: cdnsI2cRead
** 功能描述: I2C 控制器从 I2C 设备中读取数据
** 输　入  : atBaseAddr  控制器基地址
**           ucDevAddr   I2C 设备的设备地址
**           ucReadBuf   从I2C 设备读取的数据存放缓冲区
**           uiBufLen    需要向 I2C 设备写入的数据长度
** 输　出  : 成功返回 ERROR_NONE, 错误返回错误信息
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  cdnsI2cRead (UINT32  atBaseAddr, UINT8  ucDevAddr, UINT8  *ucReadBuf, UINT32  uiBufLen)
{
    UINT32   iStatus;
    UINT8   *ucCurData = ucReadBuf;
    UINT32   uiRegVal;
    INT      iRecvCurr;
    INT      iLeftCurr;
    UINT32   i;

    CDNS_I2C_INF("dev = 0x%02x, req size = %d\r\n", ucDevAddr, uiBufLen);

    if ((uiBufLen < 0) || (uiBufLen > CDNS_I2C_TRANSFERT_SIZE_MAX)) {
        return -1;
    }

    if (uiBufLen <= CDNS_I2C_FIFO_DEPTH) {
        uiRegVal = readl(atBaseAddr + CDNS_I2C_CR) & (~(CDNS_I2C_CONTROL_RW | CDNS_I2C_CONTROL_HOLD));
        writel(uiRegVal, (atBaseAddr + CDNS_I2C_CR));
    } else {
        uiRegVal  = readl(atBaseAddr + CDNS_I2C_CR) & (~(CDNS_I2C_CONTROL_RW));
        uiRegVal |= CDNS_I2C_CONTROL_HOLD;
        writel(uiRegVal, (atBaseAddr + CDNS_I2C_CR));
    }

    uiRegVal = readl(atBaseAddr + CDNS_I2C_CR) | CDNS_I2C_CONTROL_CLR_FIFO | CDNS_I2C_CONTROL_RW;
    writel(uiRegVal, (atBaseAddr + CDNS_I2C_CR));

    writel(ucDevAddr, (atBaseAddr + CDNS_I2C_ADDR));                    /* Start reading data           */
    writel(uiBufLen, (atBaseAddr + CDNS_I2C_XFER_SIZE));//本次传输期望接收的总字节数

    do {                                                                /* Wait for data read           */
//#if CDNS_I2C_INTER_EN > 0
        iStatus = cdnsI2cWait(atBaseAddr, CDNS_I2C_INTERRUPT_COMP | CDNS_I2C_INTERRUPT_DATA);
        if (!iStatus) 
//#else/*ldf 20230816 add*/
//        iStatus = cdnsI2cWait(atBaseAddr, CDNS_I2C_STATUS_RXDV);
//        if (iStatus) 
//#endif  
        {                                                 /* if error Release the bus     */
            uiRegVal = readl(atBaseAddr + CDNS_I2C_CR) & (~CDNS_I2C_CONTROL_HOLD);
            writel(uiRegVal, (atBaseAddr + CDNS_I2C_CR));

//            printf("cdnsI2cRead() timeout, addr = 0x%02x, size = %d.\r\n", ucDevAddr, uiBufLen);

            return  (-1);
        }

        iLeftCurr = readl(atBaseAddr + CDNS_I2C_XFER_SIZE);//当前期望接收的字节数
        iRecvCurr = uiBufLen - iLeftCurr;//当前已经接收了的字节数
        for (i = 0; i < iRecvCurr; i++) {
            *(ucCurData++) = readl(atBaseAddr + CDNS_I2C_DATA);
        }
        uiBufLen = iLeftCurr;
        
#ifdef _I2C_DBG_ /*ldf 20320815 add:: debug*/
    	int ii = 0;
		printk("\n=====[%s():_%d_]:: read============Start\n", __FUNCTION__, __LINE__);
		printk("dev_addr:0x%x\n", ucDevAddr);
		printk("buflen:0x%x, iLeftCurr:0x%x, iRecvCurr:0x%x\n", uiBufLen,iLeftCurr,iRecvCurr);
		printk("buf: ");
		for (ii=0; ii< iRecvCurr; ii++)
		{
			printk("0x%02x ", ucReadBuf[ii]);
		}
		printk("\n========================================End\n");
#endif

        if (iLeftCurr <= CDNS_I2C_FIFO_DEPTH) {
            uiRegVal = readl(atBaseAddr + CDNS_I2C_CR) & (~(CDNS_I2C_CONTROL_HOLD));
            writel(uiRegVal, (atBaseAddr + CDNS_I2C_CR));
        }

    } while (iLeftCurr > 0);

    return  (ERROR_NONE);
}
/*********************************************************************************************************
** 函数名称: cdnsI2cTransferMsg
** 功能描述: i2c 传输消息
** 输　入  : pI2cChannel     i2c 通道
**           pI2cMsg         i2c 传输消息组
**           iNum            需要传输的  消息数量
** 输　出  : 完成传输的消息数量
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  cdnsI2cTransferMsg (PCDNS_I2C_CHANNEL  pI2cChannel, PLW_I2C_MESSAGE  pI2cMsg, INT  iNUM)
{
	UINT32   atBaseAddr  = pI2cChannel->I2C_ulVirtAddrBase;
    UINT8   *ucDataBuf, devAddr;
    UINT16   usBufNum;
    INT      iRet;

    devAddr   =  pI2cMsg[0].I2CMSG_usAddr;
    ucDataBuf =  pI2cMsg[0].I2CMSG_pucBuffer;
    usBufNum  =  pI2cMsg[0].I2CMSG_usLen;

    if (pI2cMsg->I2CMSG_usFlag & LW_I2C_M_RD) {                         /*  读取操作                    */
        iRet = cdnsI2cRead(atBaseAddr, devAddr, ucDataBuf, usBufNum);
    } else {                                                            /*  发送操作                    */
        iRet = cdnsI2cWrite(atBaseAddr, devAddr, ucDataBuf, usBufNum);
    }

    return  (iRet);
}
/*********************************************************************************************************
** 函数名称: cdnsI2cTryTransfer
** 功能描述: i2c 尝试传输函数
** 输　入  : pI2cChannel     i2c 通道
**           pI2cAdapter     i2c 适配器
**           pI2cMsg         i2c 传输消息组
**           iNum            消息数量
** 输　出  : 完成传输的消息数量
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  cdnsI2cTryTransfer (PCDNS_I2C_CHANNEL  pI2cChannel, /*PLW_I2C_ADAPTER  pI2cAdapter,*/
                                PLW_I2C_MESSAGE    pI2cMsg,     INT              iNum)
{
    INT     i;
    UINT32  uiRegVal;
    UINT32  atBaseAddr;

    atBaseAddr = pI2cChannel->I2C_ulVirtAddrBase;
    
    pthread_mutex_lock2(&muteSem,WAIT, NO_TIMEOUT);
    
    /*
     *  zynq7000 I2C 与 IMX6Q 的 I2C 有所不同，因此驱动的实现方法上也有所不同。zynq7000中只需要设置
     *  成hold模式，i2c控制器会自动的进行restart, 在发送和接收函数中根据是否是最后一个，取消hold即可
     *  由于第一个I2C操作肯定是写入操作，因此这部分设置在 传输开始之前完成
     */
    if (iNum > 1) {
        pI2cChannel->I2C_uiBusHoldFlag = 1;
        uiRegVal = readl(atBaseAddr + CDNS_I2C_CR) | CDNS_I2C_CONTROL_HOLD;
        writel(uiRegVal, (atBaseAddr + CDNS_I2C_CR));
    } else {
        pI2cChannel->I2C_uiBusHoldFlag = 0;
    }

    for (i = 0; i < iNum; i++, pI2cMsg++) {
        if (cdnsI2cTransferMsg(pI2cChannel, pI2cMsg, iNum) != ERROR_NONE) {
            break;
        }
    }

    /*
     * 清除 hold 标志位, 让出 I2C 总线
     */
    if (pI2cChannel->I2C_uiBusHoldFlag) {
        pI2cChannel->I2C_uiBusHoldFlag = 0;
        uiRegVal = readl(atBaseAddr + CDNS_I2C_CR) & (~CDNS_I2C_CONTROL_HOLD);
        writel(uiRegVal, (atBaseAddr + CDNS_I2C_CR));
    }
    //printf("han_test 0x%x\n\r",readl(atBaseAddr + CDNS_I2C_TIME_OUT));
    //printf("han_test2 0x%x\n\r",readl(atBaseAddr + 0x20));
    
    pthread_mutex_unlock(&muteSem);
    
    return  (i);
}
/*********************************************************************************************************
** 函数名称: cdnsI2cDoTransfer
** 功能描述: i2c 传输函数
** 输　入  : pI2cChannel     i2c 通道
**           pI2cAdapter     i2c 适配器
**           pI2cMsg         i2c 传输消息组
**           iNum            消息数量
** 输　出  : 完成传输的消息数量
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  cdnsI2cDoTransfer (PCDNS_I2C_CHANNEL  pI2cChannel, /*PLW_I2C_ADAPTER pI2cAdapter,*/
                               PLW_I2C_MESSAGE    pI2cMsg,     INT             iNum)
{
#if 1
	int ret = 0;
	ret = cdnsI2cTryTransfer(pI2cChannel, /*pI2cAdapter,*/ pI2cMsg, iNum);
	return ret;
#else
    INT    i;

    /*
     * 这里使用了错误重传的功能，若传输失败则多次传输，由于实际应用中传输失败是小概率事件，
     * 建议此功能放在用户层实现，在驱动方便仅仅完成数据传输和接收更合适。
     */
    for (i = 0; i < pI2cAdapter->I2CADAPTER_iRetry; i++) {
        if (cdnsI2cTryTransfer(pI2cChannel, pI2cAdapter, pI2cMsg, iNum) == iNum) {
            return  (iNum);
        } else {
            API_TimeSleep(LW_OPTION_WAIT_A_TICK);                       /*  等待一个机器周期重试        */
        }
    }

    return  (PX_ERROR);
#endif
}
/*********************************************************************************************************
** 函数名称: cdnsI2cTransfer
** 功能描述: i2c0 传输函数
** 输　入  : pI2cAdapter     i2c 适配器
**           pI2cMsg         i2c 传输消息组
**           iNum            消息数量
** 输　出  : 完成传输的消息数量
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  cdnsI2cTransfer (PCDNS_I2C_CHANNEL  pI2cChannel,
                             PLW_I2C_MESSAGE     pI2cMsg,
                             INT                 iNum)
{
    return  (cdnsI2cDoTransfer(pI2cChannel, pI2cMsg, iNum));
}
/*********************************************************************************************************
** 函数名称: cdnsI2cMasterCtl
** 功能描述: i2c0 控制函数
** 输　入  : pI2cAdapter     i2c 适配器
**           iCmd            命令
**           lArg            参数
** 输　出  : ERROR_CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
//static INT  cdnsI2cMasterCtl (PLW_I2C_ADAPTER   pI2cAdapter, INT  iCmd, LONG  lArg)
//{
//    return  (0);
//}
/*********************************************************************************************************
** 函数名称: cdnsI2cInit
** 功能描述: 初始化 i2c 通道
** 输　入  : pI2cChannel     i2c 通道
** 输　出  : ERROR_CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static INT  cdnsI2cInit (PCDNS_I2C_CHANNEL  pI2cChannel)
{
	UINT32  atVirtBase;

    if (!pI2cChannel->I2C_bIsInit) {

        /*
         * 使用 ioremap 功能实现虚拟地址和物理地址转换，
         */
//        atVirtBase = (UINT32)API_VmmIoRemapNocache((PVOID)pI2cChannel->I2C_ulPhyAddrBase,
//                                                   LW_CFG_VMM_PAGE_SIZE);
    	
    	atVirtBase = pI2cChannel->I2C_ulPhyAddrBase;
        if (!atVirtBase) {
            printf("I2C VirtBase failed to remap!\n");
            return  (PX_ERROR);
        }
//        printf("atVirtBase:0x%lx\r\n", atVirtBase);
        pI2cChannel->I2C_ulVirtAddrBase = atVirtBase;

        /*
         * 初始化 I2C 控制器
         */
        cdnsI2cHwInit(atVirtBase, 0x68, I2C_BUS_FREQ_MIN);              /*  默认通信波特率 100K         */


#if CDNS_I2C_INTER_EN > 0
//        API_SemaphoreBCreate("i2c_signal", LW_TRUE, LW_OPTION_OBJECT_GLOBAL, &pI2cChannel->I2C_hSignal);
//        API_InterVectorConnect(pI2cChannel->I2C_uiIrqNum, cdnsI2cIsr, pI2cChannel, "i2c_isr");
//        API_InterVectorEnable(pI2cChannel->I2C_uiIrqNum);
        pI2cChannel->I2C_hSignal = semBCreate(SEM_Q_FIFO, 0);
    	int_install_handler("i2c_isr", pI2cChannel->I2C_uiIrqNum,  1, (INT_HANDLER)cdnsI2cIsr, (void*)NULL);
    	int_enable_pic(pI2cChannel->I2C_uiIrqNum);
#endif

        pI2cChannel->I2C_bIsInit = 1;
    }

    return  (ERROR_NONE);
}


void i2c_module_init(int channel)
{
	PCDNS_I2C_CHANNEL  pI2CChannel = &_G_cdnsI2cChannels[channel];
	cdnsI2cInit(pI2CChannel);
	
	pthread_mutex_init2(&muteSem,PTHREAD_MUTEX_RECURSIVE,
			RE_MUTEX_PRIO_NONE |RE_MUTEX_WAIT_FIFO ,PTHREAD_MUTEX_CEILING);
}
/*********************************************************************************************************
** 函数名称: i2cChanSwSet
** 功能描述: 设置pca地址
** 输　入  : uiChannel      i2c通道号
**           pvSw           pca数据结构
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
VOID i2cChanSwSet (UINT  uiChannel, VOID  *pvSw)
{
    _G_cdnsI2cChannels[uiChannel].pvSw = pvSw;
}
/*********************************************************************************************************
** 函数名称: i2cLowLevelXfer
** 功能描述: 设置pca地址
** 输　入  : pi2cadapter     i2c 适配器
**           pi2cmsg         i2c 传输消息组
**           iNum            消息数量
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
INT i2cLowLevelXfer (PCDNS_I2C_CHANNEL  pI2cChannel,
                     PLW_I2C_MESSAGE     pi2cmsg,
                     INT                 iNum)
{
    return (cdnsI2cTransfer(pI2cChannel, pi2cmsg, iNum));
}

int i2cBusTransferByte(int channel, u16 dev_addr, u32 start_addr, u8 *buf, int rd1_wr0)
{
	PCDNS_I2C_CHANNEL  pI2CChannel = &_G_cdnsI2cChannels[channel];
	u8 *msgbuf0 = NULL;
	u32 i = 0;
	int status = 0;
	int msg_len = 0;
	u16 len = 1;//1 byte
	
    LW_I2C_MESSAGE msg[2] = {
        {
            .I2CMSG_usAddr = dev_addr,
            .I2CMSG_usFlag = 0x4000, 
            .I2CMSG_usLen = 0,
            .I2CMSG_pucBuffer = NULL,
        }, {
            .I2CMSG_usAddr = dev_addr,
            .I2CMSG_usFlag = 0x4000, 
            .I2CMSG_usLen = 0,
            .I2CMSG_pucBuffer = NULL,
        },
    };
    
    msgbuf0 = (u8*)malloc(len + 2);
    if(msgbuf0 == NULL)
    {
    	printk("<ERROR> [%s():_%d_]:: msgbuf0 malloc failed!\n", __FUNCTION__, __LINE__);
    	return -1;
    }
    memset(msgbuf0, 0, sizeof(msgbuf0));
    
    if(rd1_wr0)//read
    {
    	msg[0].I2CMSG_usFlag = 0x4000;//write 2byte: eeprom addr
    	msg[0].I2CMSG_usLen = 2, //2byte
		msg[0].I2CMSG_pucBuffer = msgbuf0,
		
    	msg[1].I2CMSG_usFlag = 0x4000 | 0x0001;//read data
    	msg[1].I2CMSG_usLen = len,
		msg[1].I2CMSG_pucBuffer = buf,
		
	    msgbuf0[0] = (u8)((start_addr >> 8) & 0xff);//MSB first-word
	    msgbuf0[1] = (u8)(start_addr & 0xff);//LSB second-word
	    
    	msg_len = sizeof(msg)/sizeof(msg[0]);
    }
    else//write
    {
    	msg[0].I2CMSG_usFlag = 0x4000;//write 2byte: eeprom addr
    	msg[0].I2CMSG_usLen = 2 + len, 
		msg[0].I2CMSG_pucBuffer = msgbuf0,
		
	    msgbuf0[0] = (u8)((start_addr >> 8) & 0xff);//MSB first-word
	    msgbuf0[1] = (u8)(start_addr & 0xff);//LSB second-word
	    memcpy(&msgbuf0[2], buf, len);
	    
    	msg_len = 1;
    }
   
    status = cdnsI2cTransfer(pI2CChannel, msg, msg_len);
    
    free(msgbuf0);
 
    return status;
}


/*********************************************************************************************************
** 函数名称: i2cBusTransfer
** 功能描述: i2c从设备传输数据
** 输　入  :   channel         i2c 通道号0/1
**           dev_addr        i2c 从设备地址
**           start_addr      i2c 读取/写入数据起始地址
**           buf             i2c 读/写数据缓冲区
**           len             i2c 每次读/写数据的长度
**           rd1_wr0		 i2c读操作 1 /写操作 0 
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
int i2cBusTransfer(int channel, u16 dev_addr, u32 start_addr, u8 *buf, u16 len, int rd1_wr0)
{
	int status = 0;
	PCDNS_I2C_CHANNEL  pI2CChannel = &_G_cdnsI2cChannels[channel];
	u8 *msgbuf0 = NULL;
	int msg_len = 0;
	u32 i = 0;
    LW_I2C_MESSAGE msg[2] = {
        {
            .I2CMSG_usAddr = dev_addr,
            .I2CMSG_usFlag = 0x4000, 
            .I2CMSG_usLen = 0,
            .I2CMSG_pucBuffer = NULL,
        }, 
        {
            .I2CMSG_usAddr = dev_addr,
            .I2CMSG_usFlag = 0x4000, 
            .I2CMSG_usLen = 0,
            .I2CMSG_pucBuffer = NULL,
        },
        /*END*/
    };
	
#ifdef _I2C_DBG_ /*ldf 20320815 add:: debug*/
	int ii = 0;
	printk("\n=====[%s():_%d_]:: rd1_wr0=%d============Start\n", __FUNCTION__, __LINE__,rd1_wr0);
	printk("dev_addr:0x%x\n", dev_addr);
	printk("start_addr:0x%x\n", start_addr);
	printk("buflen:0x%x\n", len);
	printk("buf: ");
	for (ii=0; ii< len; ii++)
	{
		printk("0x%02x ", buf[ii]);
	}
	printk("\n========================================End\n");
#endif
		
//	if(rd1_wr0)
//		goto byte_rdwr;
//	else
//		goto nbyte_rdwr;
		

byte_rdwr:
	for(i = 0; i< len; i++)
	{
		status = i2cBusTransferByte(channel, dev_addr, start_addr+i, buf+i, rd1_wr0);
		if(status < 0)
		{
			printk("<ERROR> [%s():_%d_]:: i2cBusTransferByte failed! i=%d,channel=%d,dev_addr=0x%x,start_addr=0x%x,rd1_wr0=%d\n", 
					__FUNCTION__, __LINE__,i,channel,dev_addr,start_addr+i,rd1_wr0);
			break;
		}
		usleep(2000);/*xxx ldf 20230816 add:: 这个eeprom连续byte write时，需要加点延时，否则硬件操作没完成就开始了下一次*/
	}
	return status;

nbyte_rdwr:
    msgbuf0 = (u8*)malloc(len + 2);
    if(msgbuf0 == NULL)
    {
    	printk("<ERROR> [%s():_%d_]:: msgbuf0 malloc failed!\n", __FUNCTION__, __LINE__);
    	return -1;
    }
    memset(msgbuf0, 0, sizeof(msgbuf0));
    
    if(rd1_wr0)//read
    {
    	msg[0].I2CMSG_usFlag = 0x4000;//write 2byte: eeprom addr
    	msg[0].I2CMSG_usLen = 2, //2byte
		msg[0].I2CMSG_pucBuffer = msgbuf0,
		
    	msg[1].I2CMSG_usFlag = 0x4000 | 0x0001;//read data
    	msg[1].I2CMSG_usLen = len,
		msg[1].I2CMSG_pucBuffer = buf,
		
	    msgbuf0[0] = (u8)((start_addr >> 8) & 0xff);//MSB first-word
	    msgbuf0[1] = (u8)(start_addr & 0xff);//LSB second-word
	    
    	msg_len = sizeof(msg)/sizeof(msg[0]);
    }
    else//write
    {
    	msg[0].I2CMSG_usFlag = 0x4000;//write 2byte: eeprom addr
    	msg[0].I2CMSG_usLen = 2 + len, 
		msg[0].I2CMSG_pucBuffer = msgbuf0,
		
	    msgbuf0[0] = (u8)((start_addr >> 8) & 0xff);//MSB first-word
	    msgbuf0[1] = (u8)(start_addr & 0xff);//LSB second-word
	    memcpy(&msgbuf0[2], buf, len);
	    
    	msg_len = 1;
    }
   
    status = cdnsI2cTransfer(pI2CChannel, msg, msg_len);
    
    free(msgbuf0);
 
    return status;
}
int i2cEepromRead(int channel, u16 dev_addr, u32 start_addr, u8 *rbuf, u16 len)
{
	int i = 0;
	int loop = 0;
	int ret = 0;
	u32 r_addr = 0;
	u16 r_len_last = 0;
	u16 r_len = 0;
	u8	*p_rbuf = NULL;
	u16 len_once = len;//0x10;
	
	loop = len / len_once;
	r_len_last = len % len_once;
	if(r_len_last != 0)
		loop++;
	
	for(i = 0; i< loop; i++)
	{
		r_addr 	= start_addr + i * len_once;
		p_rbuf 	= rbuf + i * len_once;
		r_len = ((i == (loop-1)) && (r_len_last != 0)) ? r_len_last : len_once;
		ret = i2cBusTransfer(channel, dev_addr, r_addr, p_rbuf, r_len, 1);
		if(ret < 0)
			break;
	}
	
	return ret;
}


int i2cEepromWrite(int channel, u16 dev_addr, u32 start_addr, /*u16 addr_width, u16 addr_num,*/ u8 *wbuf, u16 len)
{
	int i = 0;
	int loop = 0;
	int ret = 0;
	u32 w_addr = 0;
	u16 w_len_last = 0;
	u16 w_len = 0;
	u8	*p_wbuf = NULL;
	u16 len_page = 0x40;
	
	loop = len / len_page;
	w_len_last = len % len_page;
	if(w_len_last != 0)
		loop++;
	
	for(i = 0; i< loop; i++)
	{
		w_addr 	= start_addr + i * len_page;
		p_wbuf 	= wbuf + i * len_page;
		w_len = ((i == (loop-1)) && (w_len_last != 0)) ? w_len_last : len_page;
		ret = i2cBusTransfer(channel, dev_addr, w_addr, p_wbuf, w_len, 0);
		if(ret < 0)
			break;
//		usleep(100);
	}
	
	return ret;
}


static int CheckUint8(const unsigned char *pSrc, const unsigned char *pDst, unsigned int length)
{
    int index = 0;
    unsigned char valSrc = 0;
    unsigned char valDst = 0;
    int err_num = 0;
    
    printf(" ##CheckData## pSrc: 0x%016lX, pDst: 0x%016lX, length: %lu = 0x%016lX\n", (unsigned long)pSrc, (unsigned long)pDst, length, length);
    for(index = 0; index < length; index ++)
    {
    	valSrc = *(pSrc + index);
    	valDst = *(pDst + index);
		if(valSrc != valDst)
		{
			err_num++;
			if(err_num <= 0x10)
			{
				printf("<Check Error> index=0x%x, val=0x%x, except_val=0x%x\n",\
						index,valDst,valSrc);
			}
//			return -1;
		}
    }
    
    if(err_num == 0)
    	printf("   Check OK!\n");
    else
    	printf("   Check Failed! err_num = 0x%x\n",err_num);
    
	return err_num;
}
static void DumpUint8(const unsigned char *pAddr, unsigned int length)
{
    int index = 0;

    printf("pAddr: 0x%016lX, length: %lu = 0x%016lX", (unsigned long)pAddr, length, length);
    for (index = 0; index < length; index++)
    {
        if (index % 0x10 == 0)
        {
            printf("\n0x%08lX: ", index);
        }
        printf("0x%02X ", *(unsigned char *)(pAddr + index));
    } 

    printf("\n");
}


#define EEPROM_DEV_ADDR 0x50
void Dump_i2c_eeprom(int channel, unsigned char test_addr, unsigned int test_len)
{
	unsigned char *recv_buf = (unsigned char *)malloc(test_len);
	unsigned short dev_addr = EEPROM_DEV_ADDR;
	
	memset(recv_buf, 0, test_len);
	
	i2cEepromRead(channel, dev_addr, test_addr, recv_buf, test_len);
	
	DumpUint8(recv_buf, test_len);
	
	free(recv_buf);
}

void test_i2c_eeprom_write(int channel, unsigned char test_addr, unsigned int test_len, unsigned int first_data, int is_DumpData)
{
	int i = 0;
    unsigned char *send_buf = (unsigned char *)malloc(test_len);
    unsigned short dev_addr = EEPROM_DEV_ADDR;
    
    memset(send_buf, 0, test_len);
    
    for (i=0; i<test_len; i++)
    {
    	send_buf[i] = first_data + i;
    }
	
    i2cEepromWrite(channel, dev_addr, test_addr, send_buf, test_len);
	printf("    i2c eeprom write finished.\n");
    
    if(is_DumpData)
    	DumpUint8(send_buf, test_len);

	free(send_buf);
}

void test_i2c_eeprom_rw(int channel, unsigned char test_addr, unsigned int test_len, unsigned int first_data, int is_DumpData)
{
	int i = 0;
	int err_num;
    unsigned char *send_buf = (unsigned char *)malloc(test_len);
    unsigned char *recv_buf = (unsigned char *)malloc(test_len);
    unsigned short dev_addr = EEPROM_DEV_ADDR;
    
    memset(send_buf, 0, test_len);
    memset(recv_buf, 0, test_len);
    
    for (i=0; i<test_len; i++)
    {
    	send_buf[i] = first_data + i;
    }
	
    i2cEepromWrite(channel, dev_addr, test_addr, /*2, 1,*/ send_buf, test_len);
	printf("    i2c eeprom write finished.\n");
	sleep(1);
	i2cEepromRead(channel, dev_addr, test_addr, /*2, 1,*/ recv_buf, test_len);
	
    err_num = CheckUint8(send_buf, recv_buf, test_len);
    if(err_num > 0)
    	printf("  I2C EEPROM Test Failed!, err_num = 0x%x\n",err_num); 
    else
    	printf("  I2C EEPROM Test OK!\n"); 
    
    if(is_DumpData)
    	DumpUint8(recv_buf, test_len);

	
	free(send_buf);
	free(recv_buf);
}

#if 1
VOID i2ctest(int i)
{


    UINT32        readbuf       = 0;
    int           status        = 0;
    int  j = 0;

    unsigned char msgbuf0[32+3] = {0};
    unsigned char msgbuf1[32+2] = {0};
    PCDNS_I2C_CHANNEL  pI2CChannel = &_G_cdnsI2cChannels[1];

//    LW_I2C_ADAPTER I2cAdapter;
//    I2cAdapter.I2CADAPTER_pi2cfunc = &pI2CChannel->I2C_busfuncs;
//    I2cAdapter.I2CADAPTER_iRetry   = 1;

    LW_I2C_MESSAGE msg[2] = {
        {
            .I2CMSG_usAddr = i,//dev_addr
            .I2CMSG_usFlag = 0x4000,//w
            .I2CMSG_usLen = 1,
            .I2CMSG_pucBuffer = msgbuf0,
        }, {
            .I2CMSG_usAddr = i,//dev_addr
            .I2CMSG_usFlag = 0x4000 | 0x0001,//r
            .I2CMSG_usLen = 0,
            .I2CMSG_pucBuffer = msgbuf1,
        },
    };

    if(0x49 ==i)
    {
        /*7414*/
        msgbuf0[0] = 0;
        msg[1].I2CMSG_usLen = 2;
        status = cdnsI2cTransfer(pI2CChannel, msg, 2);
        usleep(5000);
        if (status != 2) {
           // _PrintFormat("no device\r\n");
        } else {
            readbuf = msgbuf1[0] | (msgbuf1[1] << 8);
            printf("readbuf:%x\r\n", readbuf);
//            readbuf = bswap16(readbuf);

            /* REG: (0.25C/bit, two's complement) << 6 */
            readbuf = (readbuf /64 )* 250;

            printf("addr 0x%x readbuf:%d\r\n", i, readbuf);
        }
    }
    else
    {
        for(j = 0x100; j<0x110; j++)//eeprom byte read
        {
            msgbuf0[0] = j >> 8;//MSB first-word
            msgbuf0[1] = j & 0xff;//LSB second-word
            msg[0].I2CMSG_usLen = 2;	//2byte: eeprom addr
            msg[1].I2CMSG_usLen = 1;	//read 1 byte back
            status = cdnsI2cTransfer(pI2CChannel, msg, 2);
            usleep(50);
            if (status != 2) {
               // _PrintFormat("no device\r\n");
            } else {
                readbuf = msgbuf1[0];
                printf("readbuf:0x%x\r\n", readbuf);
            }
        }
        
//        for(j = 0x100; j<0x110; j++)//eeprom byte write
//        {
//        	msgbuf0[0] = j >> 8;//MSB first-word
//			msgbuf0[1] = j & 0xff;//LSB second-word
//            msgbuf0[2] = j + 5;	//1 byte: write data
//            msg[0].I2CMSG_usLen = 3;
//            status = cdnsI2cTransfer(pI2CChannel, msg, 1);
//            usleep(5000);
//            if (status != 1) {
//               // _PrintFormat("no device\r\n");
//            } else {
//                readbuf = msgbuf1[0];
//                printf("readbuf:0x%x\r\n", readbuf);
//            }
//        }
    }

}

VOID i2c_test_scan(VOID)
{
    //int i = 0;
    /*
     * 0 eeprom bl24c128      addr 0x50
     * 1 ad7414   温度传感器  addr 0x49
     * */
    i2ctest(0x50);
    i2ctest(0x49);
#if 0
    for (i = 0x45; i < 0x60; i++) {
        //_PrintFormat("%x    ", i);
        i2ctest(i);
    }
#endif
}

#endif
