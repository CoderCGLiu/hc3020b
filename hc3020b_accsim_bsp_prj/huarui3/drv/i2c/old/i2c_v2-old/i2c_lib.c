#ifndef __I2C_LIB__
#define __I2C_LIB__
#include "./i2c_v2.h"
#include "./_umips.h"
#include "./_udebug.h"

//#define _I2C_DBG_ /*ldf 20230815 add:: debug*/

//static u8 global_send_buf[512] = {0};
//static u8 global_recv_buf[512] = {0};

static pthread_t muteSem;

// (div_a + 1) * (div_b +1) = Fpclk / (22 * Fscl)
struct i2c_plk_scl_config i2c_plk_scl_config_map[] = {
	{14, 0, 33, 100},   //33.3MHz - 100KHz - 100.91KHz
	{3,  0, 33, 400},   //33.3MHz - 400KHz - 378.41KHz
	{1,  0, 33, 1000},  //33.3MHz - 1MHz - 756.82KHz
};


void i2c_print_msg(struct i2c_msg *msg)
{
	printk("addr:0x%x, flags:0x%x, len:0x%x, buf:", msg->addr, msg->flags, msg->len);
	int i=0;
	for (i=0; i<msg->len; i++)
		printk("%d ", msg->buf[i]);
	printk("\r\n");
}

void print_sr(struct i2c_regs* host)
{
	SR sr_cur = host->sr;
	printk("SR=0x%x, RXRW:0x%x, RXDV:0x%x, TXDV:0x%x, RXOVF:0x%x, BA:0x%x\r\n", \
			sr_cur.v, \
			sr_cur.bits.RXRW, \
			sr_cur.bits.RXDV, \
			sr_cur.bits.TXDV, \
			sr_cur.bits.RXOVF, \
			sr_cur.bits.BA \
		   );
}

void print_isr(struct i2c_regs *host)
{
	//IMR imr_cur = host->imr;
	ISR isr_cur = host->isr;
	printk("ISR=0x%x, COMP:0x%x, DATA:0x%x, NACK:0x%x, TO:0x%x, SLVRDY:0x%x, RXOVF:0x%x, TXOVF:0x%x, RXUNF:0x%x, ARBLOST:0x%x\r\n", \
			isr_cur.v, \
			isr_cur.bits.COMP, \
			isr_cur.bits.DATA, \
			isr_cur.bits.NACK, \
			isr_cur.bits.TO, \
			isr_cur.bits.SLVRDY, \
			isr_cur.bits.RXOVF, \
			isr_cur.bits.TXOVF, \
			isr_cur.bits.RXUNF, \
			isr_cur.bits.ARBLOST \
		   );
}

void cntdelay(u32 cnt)
{
	while(cnt--);
}

void print_regs(struct i2c_regs *host)
{
	printk("+++ print_regs +++\r\n");
	print_sr(host);
	print_isr(host);
	// clear interupts
	//host->isr.v = 0xffff;
}


void wait_isr_comp(struct i2c_regs *host, int timeout)
{
	if (-1 == timeout)
	{
		while(!host->isr.bits.COMP);
		u16 isr_sts = host->isr.bits.COMP;
		host->isr.bits.COMP = 1;
	} else {
		wait_if(!host->isr.bits.COMP, timeout, "comp");
		host->isr.bits.COMP = 1;
	}
}

void i2c_lib_set_fscl_speed(struct i2c_regs *host, u32 fscl)
{
	CR cur_cr = host->cr;
	int i = 0;
	int len = sizeof(i2c_plk_scl_config_map)/sizeof(struct i2c_plk_scl_config);
	int found_cnt = 0;
	for (; i< len; i++)
	{
		if ( i2c_plk_scl_config_map[i].fscl == fscl )
		{
			cur_cr.bits.DIV_A = i2c_plk_scl_config_map[i].div_a;
			cur_cr.bits.DIV_B = i2c_plk_scl_config_map[i].div_b;
			found_cnt ++;
			break;
		}
	}
	if (!found_cnt)
	{
		cur_cr.bits.DIV_A = i2c_plk_scl_config_map[0].div_a;
		cur_cr.bits.DIV_B = i2c_plk_scl_config_map[0].div_b;
	}
	host->cr = cur_cr;
}

void i2c_lib_set_master(struct i2c_regs *host, u32 is_master)
{
	host->cr.bits.MS = is_master;
}

void i2c_lib_set_addrmode(struct i2c_regs* host, u8 addrmode)
{
	//0-10bits,  1-7bits
	host->cr.bits.NEA = addrmode;
}

void i2c_lib_set_slave_addr(struct i2c_regs* host, u16 addr)
{
	host->ar.v = addr;
}


int i2c_lib_enable_interrupts(struct i2c_regs* host)
{
	host->ier.v = 0xffff;
}

int i2c_lib_disable_interrupts(struct i2c_regs* host)
{
	host->idr.v = 0xffff;
}

// notice: random read at24c32, which means write/read ONE byte once!
int i2c_lib_msg_trans(struct i2c_regs *host, struct i2c_msg *msgs, int i2c_msg_len)
{
	struct i2c_msg *msg_cur;
	int msg_i = 0;
	int i =0;
	int ii=0;/*ldf 20230815 add:: debug*/
	
	pthread_mutex_lock2(&muteSem,WAIT, NO_TIMEOUT);
	
	for (msg_i=0; msg_i<i2c_msg_len; msg_i++)
	{
//		printk("msg start:%d/%d\r\n", msg_i, i2c_msg_len);

		msg_cur = msgs + msg_i;
		
		if (msg_cur->flags == 0 && msg_cur->len > 0)   // if send
		{
//			printk("send msg:%d/%d  | ", msg_i, i2c_msg_len);
//			i2c_print_msg(msg_cur);
			CR cur_cr = host->cr;
			cur_cr.bits.RW = 0; // 0-send, 1-recv
			//cur_cr.bits.CLRFIFO = 1; // 1- auto clear fifo
			cur_cr.bits.ACKEN = 1; // 1- set when fifo enabled
			host->cr = cur_cr;

			// important AR must first pass! (otherwise may send miss)
			host->ar.v = msg_cur->addr;

			host->cr.bits.HOLD = 1;
			for (i=0; i< msg_cur->len; i++)
			{
				wait_if((host->sr.bits.TXDV), HZ/100000, "txdv");
//				wait_if((!host->isr.bits.COMP), HZ/100000, "COMP");/*ldf 20230815 add*/
				host->dr.v = (u8)(msg_cur->buf[i]);
			}
			host->cr.bits.HOLD = 0;
			// for at24c32 write delay
			cntdelay(msg_cur->wdelay); // 100us
			
		} else if (msg_cur->flags == 1)  // if recv
		{
			// clear interupts
			host->isr.v = 0xffff;

			CR cur_cr = host->cr;
			cur_cr.bits.RW = 1; // 0-send, 1-recv
			//cur_cr.bits.CLRFIFO = 1; // 1- auto clear fifo
			cur_cr.bits.ACKEN = 1; // 1- set when fifo enabled
			host->cr = cur_cr;
			//host->tor.v = 0xff;

			// start recv
			host->tsr.v = msg_cur->len;
			host->ar.v = msg_cur->addr;
			host->cr.bits.HOLD = 1;

			for (i=0; i< msg_cur->len; i++)
			{
				wait_if((!host->sr.bits.RXDV), HZ/100000, "rxdv");
//				wait_if((!host->isr.bits.COMP), HZ/10000000, "COMP");/*ldf 20230815 add*/
				msg_cur->buf[i] = (u8)host->dr.v;
			}
			host->cr.bits.HOLD = 0;
			cntdelay(msg_cur->rdelay);
			
//			printk("recv msg:%d/%d  | ", msg_i, i2c_msg_len);
//			i2c_print_msg(msg_cur);
			
		} else {  // nothing
			printk("msg illegal!\r\n");
		}

#ifdef _I2C_DBG_ /*ldf 20320815 add:: debug*/
		printk("\n=====[%s():_%d_]:: msg_i=%d============Start\n", __FUNCTION__, __LINE__,msg_i);
		printk("dev_addr:0x%x\n", msg_cur->addr);
		printk("flags:0x%x   (0-send, 1-recv)\n", msg_cur->flags);
		printk("rdelay:0x%x\n", msg_cur->rdelay);
		printk("wdelay:0x%x\n", msg_cur->wdelay);
		printk("buflen:0x%x\n", msg_cur->len);
		printk("buf: ");
		for (ii=0; ii< msg_cur->len; ii++)
		{
			printk("0x%x ", msg_cur->buf[ii]);
		}
		printk("\n========================================End\n");
#endif
		
	}
//	printk("msg end!\r\n");
	pthread_mutex_unlock(&muteSem);
	
	return 0;
}

int init_msg(struct i2c_msg* i2c_msg, u16 addr, u16 flags, u8* buf, u16 len, u32 wdelay, u32 rdelay)
{
	i2c_msg->addr = addr;
	i2c_msg->flags = flags;
	i2c_msg->buf = buf;
	i2c_msg->len = len;
	i2c_msg->wdelay = wdelay;
	i2c_msg->rdelay = rdelay;
}

void i2c_lib_send_msg(struct i2c_regs* host, u16 dev_addr, u8* ubuf, u32 cnt)
{
	u32 wdelay = 0;
	u32 rdelay = 0;
	struct i2c_msg tmp_i2c_msg_list;
	init_msg(&tmp_i2c_msg_list, dev_addr, 0, ubuf, cnt, wdelay, rdelay);
	i2c_lib_msg_trans(host, &tmp_i2c_msg_list, 1);
}

void i2c_lib_recv_msg(struct i2c_regs* host, u16 dev_addr, u8* ubuf, u32 cnt)
{
	u32 wdelay = 0;
	u32 rdelay = 0;
	struct i2c_msg tmp_i2c_msg_list;
	init_msg(&tmp_i2c_msg_list, dev_addr, 1, ubuf, cnt, wdelay, rdelay);
	i2c_lib_msg_trans(host, &tmp_i2c_msg_list, 1);
}

// max batch send count = MAX_MSGS
int i2c_lib_master_send_data(struct i2c_regs *host, u16 dev_addr, u16 data_addr, u8 *buf, u32 cnt, u32 wdelay, u32 rdelay)
{
//	write_c0_count(0);
//	u64 global_pre_cpu_cnt = read_c0_count();

	int i=0;
	struct i2c_msg *tmp_i2c_msg_list = (struct i2c_msg *)malloc(cnt* sizeof(struct i2c_msg));
	u8* tmp_msg_buf = (u8*)malloc(cnt * 3);
	
	for (i=0; i<cnt; i++)
	{
		tmp_msg_buf[i*3] = ((data_addr+i)>>8)&0xff;
		tmp_msg_buf[i*3+1] = (data_addr+i)&0xff;
		tmp_msg_buf[i*3+2] = buf[i];
		init_msg(tmp_i2c_msg_list+i, dev_addr, 0, tmp_msg_buf+i*3, 3, wdelay, rdelay);
	}
	i2c_lib_msg_trans(host, tmp_i2c_msg_list, cnt);

	free(tmp_i2c_msg_list);
	free(tmp_msg_buf);

//	u64 global_cur_cpu_cnt = read_c0_count();
//	printk("cp0_count diff:%d, ms:%d\r\n", global_cur_cpu_cnt - global_pre_cpu_cnt, ((global_cur_cpu_cnt-global_pre_cpu_cnt)/(CPU_HZ/1000)));
	return 0;
}

// max batch send count = MAX_MSGS
int i2c_lib_master_recv_data(struct i2c_regs *host, u16 dev_addr, u16 data_addr, u8 *buf, u32 cnt, u32 wdelay, u32 rdelay)
{
//	printk("<DEBUG> [%s:__%d__]:: host = 0x%16x\n",__FUNCTION__,__LINE__,host);
//	write_c0_count(0);
//	u64 global_pre_cpu_cnt = read_c0_count();

	struct i2c_msg *tmp_i2c_msg_list = (struct i2c_msg *)malloc(cnt* sizeof(struct i2c_msg) *2);
	u8* tmp_msg_buf = (u8*)malloc(cnt * 2);
	int i=0;
#if 1
	for (i=0; i<cnt; i++)
	{
		// send data address
		tmp_msg_buf[i*2] = ((data_addr+i)>>8)&0xff;
		tmp_msg_buf[i*2+1] = (data_addr+i)&0xff;
		init_msg(tmp_i2c_msg_list+i*2, dev_addr, 0, tmp_msg_buf+i*2, 2, wdelay, rdelay);

		//read
		init_msg(tmp_i2c_msg_list+i*2+1, dev_addr, 1, buf+i, 1, wdelay, rdelay);
	}
	i2c_lib_msg_trans(host, tmp_i2c_msg_list, cnt*2);
#else
	// send data address
	tmp_msg_buf[0] = ((data_addr)>>8)&0xff;
	tmp_msg_buf[1] = (data_addr)&0xff;
	init_msg(tmp_i2c_msg_list, dev_addr, 0, tmp_msg_buf, 2, wdelay, rdelay);
	//read
	init_msg(tmp_i2c_msg_list+1, dev_addr, 1, buf, 4, wdelay, rdelay);
	i2c_lib_msg_trans(host, tmp_i2c_msg_list, 2);
#endif
	
	free(tmp_i2c_msg_list);
	free(tmp_msg_buf);
	
//	u64 global_cur_cpu_cnt = read_c0_count();
//	printk("cp0_count diff:%d, ms:%d\r\n", global_cur_cpu_cnt - global_pre_cpu_cnt, ((global_cur_cpu_cnt-global_pre_cpu_cnt)/(CPU_HZ/1000)));
	return 0;
}

/*********************************************************************************************************
** 函数名称: i2cBusRead
** 功能描述: 读i2c从设备数据
** 输　入  : ichannl         i2c 通道号0/1
**           dev_addr        i2c 从设备地址
**           start_addr      i2c 读取数据起始地址
**           rbuf            i2c 读取数据缓冲区
**           len             i2c 每次读取数据的长度
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
int i2cBusRead(int channel, u16 dev_addr, u32 start_addr, /*u16 addr_width, u16 addr_num,*/ u8 *rbuf, u16 len)
{
//	struct i2c_regs* 	host = (channel == 0) ? I2C0_REGS : I2C1_REGS;
    int                 status;
    struct i2c_msg      *msg;
    u8               *addrbuf;
    int                 i, j;
    int ii = 0;/*ldf 20320815 add:: debug*/
    u16 addr_width = 2;/*ldf 20320815 add*/
    u16 addr_num = 1;/*ldf 20320815 add*/

    msg = (struct i2c_msg *)malloc(sizeof(struct i2c_msg)*addr_num*2);
    if (msg == NULL) {
        return  -1;
    }

    addrbuf = (u8 *)malloc(addr_num * addr_width);
    if (addrbuf == NULL) {
        return  -1;
    }
	
    for(i = 0; i < addr_num; i++)
    {
        for(j = 0; j < addr_width; j++)
        {
            addrbuf[addr_width * i + j] = (u8)((start_addr + i*addr_width) >> (8 * (addr_width - j - 1)));
//            printf("<***DEBUG***> [%s:_%d_]:: addrbuf[%d] = 0x%x\n",__FUNCTION__,__LINE__,addr_width * i + j,addrbuf[addr_width * i + j]);
        }

        //I2C写地址操作
        msg[2 * i].addr = dev_addr;
        msg[2 * i].flags = 0;//w
        msg[2 * i].len = addr_width;
        msg[2 * i].buf = &addrbuf[addr_width * i];
        msg[2 * i].wdelay = 0;
        msg[2 * i].rdelay = 0;
        
#ifdef _I2C_DBG_ /*ldf 20320815 add:: debug*/
		printk("\n===== [%s():_%d_]:: msg_i=%d, write ============Start\n", __FUNCTION__, __LINE__,2 * i);
		printk("dev_addr:0x%x\n", msg[2 * i].addr);
		printk("flags:0x%x   (0-send, 1-recv)\n", msg[2 * i].flags);
		printk("rdelay:0x%x\n", msg[2 * i].rdelay);
		printk("wdelay:0x%x\n", msg[2 * i].wdelay);
		printk("buflen:0x%x\n", msg[2 * i].len);
		printk("buf: ");
		for (ii=0; ii< msg[2 * i].len; ii++)
		{
			printk("0x%x ", msg[2 * i].buf[ii]);
		}
		printk("\n================================================End\n");
#endif
        
        //I2C读数据操作
        msg[2 * i + 1].addr = dev_addr;
        msg[2 * i + 1].flags = 1;//r
        msg[2 * i + 1].len = len;
        msg[2 * i + 1].buf = rbuf + len * i;
        msg[2 * i + 1].wdelay = 0;
        msg[2 * i + 1].rdelay = 0;
        
#ifdef _I2C_DBG_ /*ldf 20320815 add:: debug*/
		printk("\n===== [%s():_%d_]:: msg_i=%d, read =============Start\n", __FUNCTION__, __LINE__,2 * i + 1);
		printk("dev_addr:0x%x\n", msg[2 * i + 1].addr);
		printk("flags:0x%x   (0-send, 1-recv)\n", msg[2 * i + 1].flags);
		printk("rdelay:0x%x\n", msg[2 * i + 1].rdelay);
		printk("wdelay:0x%x\n", msg[2 * i + 1].wdelay);
		printk("buflen:0x%x\n", msg[2 * i + 1].len);
		printk("buf: ");
		for (ii=0; ii< msg[2 * i + 1].len; ii++)
		{
			printk("0x%x ", msg[2 * i + 1].buf[ii]);
		}
		printk("\n================================================End\n");
#endif
		
    }

    status = i2c_lib_msg_trans(I2CS_BASE_ADDR(channel), msg, (2 * addr_num));

//    if (status != 2) {
//       // _PrintFormat("no device\r\n");
//    } else {
//        readbuf = rbuf[0];
//        //_PrintFormat("readbuf:0x%x\r\n", readbuf);
//    }

    free(addrbuf);
    free(msg);

    return status;
}

/*********************************************************************************************************
** 函数名称: i2cBusWrite
** 功能描述: 读i2c从设备数据
** 输　入  : ichannl         i2c 通道号0/1
**           dev_addr        i2c 从设备地址
**           start_addr      i2c 读取数据起始地址
**           wbuf            i2c 写数据缓冲区
**           len             i2c 每次写数据的长度
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
int i2cBusWrite(int channel, u16 dev_addr, u32 start_addr, /*u16 addr_width, u16 addr_num,*/ u8 *wbuf, u16 len)
{
//	struct i2c_regs* 	host = (channel == 0) ? I2C0_REGS : I2C1_REGS;
    int                 status;
    struct i2c_msg      *msg;
    u8               	*tbuf;
    int                 i, j;
    int ii=0;/*ldf 20230815 add:: debug*/
    u16 addr_width = 2;/*ldf 20320815 add*/
    u16 addr_num = 1;/*ldf 20320815 add*/
    
    msg = (struct i2c_msg *)malloc(sizeof(struct i2c_msg)*addr_num);
    if (msg == NULL) {
        return  -1;
    }
    
    tbuf = (u8 *)malloc(addr_num * (addr_width + len));
    if (tbuf == NULL) {
        return  -1;
    }

    for(i = 0; i < addr_num; i++)
    {
        //地址
        for(j = 0; j < addr_width; j++)
        {
            tbuf[(addr_width + len) * i + j] = (u8)((start_addr + i*addr_width) >> (8 * (addr_width - j - 1)));
        }

        //数据
        for(; j < (addr_width + len); j++)
        {
            tbuf[(addr_width + len) * i + j] = wbuf[len * i + (j - addr_width)];
        }

        //I2C写地址操作，写地址和写数据必须放一个消息里面，不能分开存两个消息！
        msg[i].addr = dev_addr;
        msg[i].flags = 0;
        msg[i].len = addr_width + len;
        msg[i].buf = &tbuf[(addr_width + len) * i];
        msg[i].wdelay = 0;
        msg[i].rdelay = 0;
        
#ifdef _I2C_DBG_ /*ldf 20320815 add:: debug*/
		printk("\n=====[%s():_%d_]:: msg_i=%d, write============Start\n", __FUNCTION__, __LINE__,i);
		printk("dev_addr:0x%x\n", msg[i].addr);
		printk("flags:0x%x   (0-send, 1-recv)\n", msg[i].flags);
		printk("rdelay:0x%x\n", msg[i].rdelay);
		printk("wdelay:0x%x\n", msg[i].wdelay);
		printk("buflen:0x%x\n", msg[i].len);
		printk("buf: ");
		for (ii=0; ii< msg[i].len; ii++)
		{
			printk("0x%x ", msg[i].buf[ii]);
		}
		printk("\n==============================================End\n");
#endif

    }
		
    status = i2c_lib_msg_trans(I2CS_BASE_ADDR(channel), msg, addr_num);
    
//    if (status != 2) {
//       // _PrintFormat("no device\r\n");
//    } else {
//        readbuf = wbuf[0];
//        _PrintFormat("readbuf:0x%x\r\n", readbuf);
//    }

    free(tbuf);
    free(msg);

    return status;
}

//int i2cEepromRead(int channel, u16 dev_addr, u32 start_addr, /*u16 addr_width, u16 addr_num,*/ u8 *rbuf, u16 len)
//{
//#if 0
//	return (i2cBusRead(channel, dev_addr, start_addr, /*addr_width, addr_num,*/ rbuf, len));
//#else/*ldf 20230815 add*/
//	int i = 0;
//	int loop = 0;
//	int ret = 0;
//	u32 r_addr = 0;
//	u16 r_len_last = 0;
//	u16 r_len = 0;
//	u8	*p_rbuf = NULL;
//	u16 len_once = 0x10;
//	
//	loop = len / len_once;
//	r_len_last = len % len_once;
//	if(r_len_last != 0)
//		loop++;
//	
//	for(i = 0; i< loop; i++)
//	{
//		r_addr 	= start_addr + i * len_once;
//		p_rbuf 	= rbuf + i * len_once;
//		r_len = ((i == (loop-1)) && (r_len_last != 0)) ? r_len_last : len_once;
//		ret = i2cBusRead(channel, dev_addr, r_addr, /*addr_width, addr_num,*/ p_rbuf, r_len);
//		if(ret < 0)
//			break;
//	}
//	
//	return ret;
//#endif
//}
//
//
//int i2cEepromWrite(int channel, u16 dev_addr, u32 start_addr, /*u16 addr_width, u16 addr_num,*/ u8 *wbuf, u16 len)
//{
//#if 0
//	return (i2cBusWrite(channel, dev_addr, start_addr, /*addr_width, addr_num,*/ wbuf, len));
//#else/*ldf 20230815 add*/
//	int i = 0;
//	int loop = 0;
//	int ret = 0;
//	u32 w_addr = 0;
//	u16 w_len_last = 0;
//	u16 w_len = 0;
//	u8	*p_wbuf = NULL;
//	u16 len_page = 0x40;
//	
//	loop = len / len_page;
//	w_len_last = len % len_page;
//	if(w_len_last != 0)
//		loop++;
//	
//	for(i = 0; i< loop; i++)
//	{
//		w_addr 	= start_addr + i * len_page;
//		p_wbuf 	= wbuf + i * len_page;
//		w_len = ((i == (loop-1)) && (w_len_last != 0)) ? w_len_last : len_page;
//		ret = i2cBusWrite(channel, dev_addr, w_addr, /*addr_width, addr_num,*/ p_wbuf, w_len);
//		if(ret < 0)
//			break;
//	}
//	
//	return ret;
//#endif
//}

//void i2c_module_init(int channel)
//{
//	i2c_lib_enable_interrupts(I2CS_BASE_ADDR(channel));
//	//master, 7bits, 400KHz
//	i2c_lib_set_addrmode(I2CS_BASE_ADDR(channel), 1);
//	i2c_lib_set_fscl_speed(I2CS_BASE_ADDR(channel), 400);
//	i2c_lib_set_master(I2CS_BASE_ADDR(channel), 1);
//	
//	pthread_mutex_init2(&muteSem,PTHREAD_MUTEX_RECURSIVE,
//			RE_MUTEX_PRIO_NONE |RE_MUTEX_WAIT_FIFO ,PTHREAD_MUTEX_CEILING);
//	return;
//}

//static int CheckUint8(const unsigned char *pSrc, const unsigned char *pDst, unsigned int length)
//{
//    int index = 0;
//    unsigned char valSrc = 0;
//    unsigned char valDst = 0;
//    int err_num = 0;
//    
//    printf(" ##CheckData## pSrc: 0x%016lX, pDst: 0x%016lX, length: %lu = 0x%016lX\n", (unsigned long)pSrc, (unsigned long)pDst, length, length);
//    for(index = 0; index < length; index ++)
//    {
//    	valSrc = *(pSrc + index);
//    	valDst = *(pDst + index);
//		if(valSrc != valDst)
//		{
//			err_num++;
//			if(err_num <= 0x10)
//			{
//				printf("<Check Error> index=0x%x, val=0x%x, except_val=0x%x\n",\
//						index,valDst,valSrc);
//			}
////			return -1;
//		}
//    }
//    
//    if(err_num == 0)
//    	printf("   Check OK!\n");
//    else
//    	printf("   Check Failed! err_num = 0x%x\n",err_num);
//    
//	return err_num;
//}
//static void DumpUint8(u8 *pAddr, u32 length)
//{
//    int index = 0;
//
//    printf("pAddr: 0x%08lX, length: %lu = 0x%08lX", (u32)pAddr, length, length);
//    for (index = 0; index < length; index++)
//    {
//        if (index % 0x10 == 0)
//        {
//            printf("\n0x%08lX: ", index);
//        }
//        printf("0x%02X ", *(u8 *)(pAddr + index));
//    }
//
//    printf("\n");
//}
//
//void Dump_i2c_eeprom(int channel, unsigned char test_addr, unsigned int test_len)
//{
//	unsigned char *recv_buf = (unsigned char *)malloc(test_len);
//	unsigned short dev_addr = EEPROM_DEV_ADDR;
//	
//	memset(recv_buf, 0, test_len);
//	
//	i2cEepromRead(channel, dev_addr, test_addr, /*2, 1,*/ recv_buf, test_len);
//	
//	DumpUint8(recv_buf, test_len);
//	
//	free(recv_buf);
//}
//
//void test_i2c_eeprom_write(int channel, unsigned char test_addr, unsigned int test_len, unsigned int first_data, int is_DumpData)
//{
//	int i = 0;
//    unsigned char *send_buf = (unsigned char *)malloc(test_len);
//    unsigned short dev_addr = EEPROM_DEV_ADDR;
//    
//    memset(send_buf, 0, test_len);
//    
//    for (i=0; i<test_len; i++)
//    {
//    	send_buf[i] = first_data + i;
//    }
//	
//	i2cEepromWrite(channel, dev_addr, test_addr, /*2, 1,*/ send_buf, test_len);
////	i2c_lib_master_send_data(I2CS_BASE_ADDR(channel), dev_addr, test_addr, send_buf, test_len, HZ/100000, 0);
//	printf("    i2c eeprom write finished.\n");
//    
//    if(is_DumpData)
//    	DumpUint8(send_buf, test_len);
//
//	free(send_buf);
//}
//
//void test_i2c_eeprom_rw(int channel, unsigned char test_addr, unsigned int test_len, unsigned int first_data, int is_DumpData)
//{
//	int i = 0;
//	int err_num;
//    unsigned char *send_buf = (unsigned char *)malloc(test_len);
//    unsigned char *recv_buf = (unsigned char *)malloc(test_len);
//    unsigned short dev_addr = EEPROM_DEV_ADDR;
//    
//    memset(send_buf, 0, test_len);
//    memset(recv_buf, 0, test_len);
//    
//    for (i=0; i<test_len; i++)
//    {
//    	send_buf[i] = first_data + i;
//    }
//	
//	i2cEepromWrite(channel, dev_addr, test_addr, /*2, 1,*/ send_buf, test_len);
////	i2c_lib_master_send_data(I2CS_BASE_ADDR(channel), dev_addr, test_addr, send_buf, test_len, HZ/100000, 0);
//	printf("    i2c eeprom write finished.\n");
//	sleep(1);
//	i2cEepromRead(channel, dev_addr, test_addr, /*2, 1,*/ recv_buf, test_len);
////	i2c_lib_master_recv_data(I2CS_BASE_ADDR(channel), dev_addr, test_addr, recv_buf, test_len, HZ/100000, 0);
//	
//    err_num = CheckUint8(send_buf, recv_buf, test_len);
//    if(err_num > 0)
//    	printf("  I2C EEPROM Test Failed!, err_num = 0x%x\n",err_num); 
//    else
//    	printf("  I2C EEPROM Test OK!\n"); 
//    
//    if(is_DumpData)
//    	DumpUint8(recv_buf, test_len);
//
//	
//	free(send_buf);
//	free(recv_buf);
//}



#endif /* __I2C_LIB__ */
