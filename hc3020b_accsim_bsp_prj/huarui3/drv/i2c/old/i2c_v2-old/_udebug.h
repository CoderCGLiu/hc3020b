/* ********************************************
 * FILE NAME  : udebug.h
 * PROGRAMMER : zhaozz
 * START DATE : 2021-09-01 17:39:59
 * DESCIPTION : standalone udebug
 * *******************************************/

#ifndef  ___UDEBUG_H__
#define  ___UDEBUG_H__

//#ifndef u8
//typedef unsigned char u8;
//#endif
//#ifndef u16
//typedef unsigned short u16;
//#endif
//#ifndef u32
//typedef unsigned int u32;
//#endif
//#ifndef u64
//typedef unsigned long u64;
//#endif

//static union { char c[4]; unsigned long l;} endian_test = { {'l', '?', '?', 'b'}};
//#define ENDIANNESS ((char)endian_test.l)

//#define L2B32(little) (((little&0xff)<<24) | ((little&0xff00)<<8) | ((little&0xff0000)>>8) | ((little&0xff000000)>>24))

//#define NULL	((void*)0)

// ============================================================using which uart
#define USING_UART0

//// ============================================================wait if
//#define WAIT_LOG
#ifdef WAIT_LOG
#define wait_udebug udebug
#define wait_uclean uclean
#define wait_always uclean
#else
#define wait_udebug(fmt, args...) do{;}while(0)
#define wait_uclean(fmt, args...) do{;}while(0)
//#define wait_always uclean
#endif

#define wait_if(expr, cnt, desc) ({\
		wait_udebug("wait_if[%s][%d]", desc, cnt); \
		int _i = 0; \
		for (;_i < cnt; _i++) { if (!(expr)) break; if (0 == _i%100) /*wait_always(".")*/;} \
		wait_uclean("->[%d]\r\n", _i); \
		_i;\
		})

////void delay_cnt(int cnt)
////{
////	while(cnt--);
////}
//// ============================================================wait if

//// ============================================================log
//#define LEVEL_DEBUG   0
//#define LEVEL_INFO    1
//#define LEVEL_ERROR   2
//#define LEVEL_SIMPLE  3
//#define ULOG_LEVEL    LEVEL_SIMPLE
//
//#if ULOG_LEVEL ==  LEVEL_DEBUG
//#define udebug(fmt, args...) uart_printf("[DEBUG][%s:%d][%s] "fmt, __FILE__,__LINE__,__FUNCTION__,##args)
//#define uinfo(fmt, args...) uart_printf("[INFO][%s:%d][%s] "fmt, __FILE__,__LINE__,__FUNCTION__,##args)
//#define uerror(fmt, args...) uart_printf("[ERROR][%s:%d][%s] "fmt, __FILE__,__LINE__,__FUNCTION__,##args)
//#define uclean uart_printf
//#define ulog uinfo
//#elif ULOG_LEVEL == LEVEL_INFO
//#define udebug(fmt, args...) do {;} while(0)
//#define uinfo(fmt, args...) uart_printf("[INFO][%d][%s] "fmt, __LINE__, __FUNCTION__, ##args)
//#define uerror(fmt, args...) uart_printf("[ERROR][%d][%s] "fmt, __LINE__, __FUNCTION__, ##args)
//#define uclean uart_printf
//#define ulog uinfo
//#elif ULOG_LEVEL == LEVEL_ERROR
//#define udebug(fmt, args...) do {;} while(0)
//#define uinfo(fmt, args...) do {;} while(0)
//#define uerror(fmt, args...) uart_printf("[ERROR][%d][%s] "fmt, __LINE__, __FUNCTION__, ##args)
//#define uclean uart_printf
//#define ulog uinfo
//#elif ULOG_LEVEL == LEVEL_SIMPLE
//#define udebug(fmt, args...) uart_printf("[D][%d][%s] "fmt, __LINE__, __FUNCTION__, ##args)
//#define uinfo(fmt, args...)  uart_printf("[I][%d][%s] "fmt, __LINE__, __FUNCTION__, ##args)
//#define uerror(fmt, args...) uart_printf("[E][%d][%s] "fmt, __LINE__, __FUNCTION__, ##args)
//#define uclean uart_printf
//#define ulog uinfo
//#else
//#define udebug uart_printf
//#define uinfo uart_printf
//#define uerror uart_printf
//#define uclean uart_printf
//#define ulog uart_printf
//#endif
//// ============================================================log


// ============================================================read write Transfer
#define _P2V(addr)		(0x9000000000000000 | ((u64)(addr)))	/* physical address to virtual address */
#define _P2V_CACHE(addr)	(0xb000000000000000 | ((u64)(addr)))	/* physical address to virtual address */

#define READ8_REGISTER(x, y) do {			\
	y= *((volatile u8 *)(_P2V(x))) ;	\
} while(0)
#define READ16_REGISTER(x, y) do {			\
	y= *((volatile u16 *)(_P2V(x))) ;	\
} while(0)
#define READ32_REGISTER(x, y) do {			\
	y = *((volatile u32*)(_P2V(x)));	\
} while(0)
#define READ64_REGISTER(x, y) do {			\
	y = *((volatile u64 *)(_P2V(x)));	\
} while(0)
#define WRITE8_REGISTER(x, y) do {			\
	*((volatile u8 *)(_P2V(x)))= (u8)y ;	\
} while(0)
#define WRITE16_REGISTER(x, y) do {			\
	*((volatile u16 *)(_P2V(x)))= (u16)y ;	\
} while(0)
#define WRITE32_REGISTER(x, y) do {			\
	*((volatile u32 *)(_P2V(x))) = y;	\
} while(0)
#define WRITE64_REGISTER(x, y) do {			\
	*((volatile u64 *)(_P2V(x))) = y;	\
} while(0)

#define READ8_CACHE_REGISTER(x, y) do {			\
	y= *((volatile u8 *)(_P2V_CACHE(x))) ;	\
} while(0)
#define READ16_CACHE_REGISTER(x, y) do {			\
	y= *((volatile u16 *)(_P2V_CACHE(x))) ;	\
} while(0)
#define READ32_CACHE_REGISTER(x, y) do {			\
	y = *((volatile u32*)(_P2V_CACHE(x)));	\
} while(0)
#define READ64_CACHE_REGISTER(x, y) do {			\
	y = *((volatile u64 *)(_P2V_CACHE(x)));	\
} while(0)
#define WRITE8_CACHE_REGISTER(x, y) do {			\
	*((volatile u8 *)(_P2V_CACHE(x)))= (u8)y ;	\
} while(0)
#define WRITE16_CACHE_REGISTER(x, y) do {			\
	*((volatile u16 *)(_P2V_CACHE(x)))= (u16)y ;	\
} while(0)
#define WRITE32_CACHE_REGISTER(x, y) do {			\
	*((volatile u32 *)(_P2V_CACHE(x))) = y;	\
} while(0)
#define WRITE64_CACHE_REGISTER(x, y) do {			\
	*((volatile u64 *)(_P2V_CACHE(x))) = y;	\
} while(0)

#define phx_read_u8(where)						\
	({                                                              \
	 u8 val;                                        \
	 READ8_REGISTER(where, val);				\
	 val;                                                    \
	 })
#define phx_read_u16(where)						\
	({                                                              \
	 u16 val;                                      \
	 READ16_REGISTER(where, val);				\
	 val;                                                    \
	 })
#define phx_read_u32(where)						\
	({                                                              \
	 volatile u32 val;                                       \
	 READ32_REGISTER(where, val);				\
	 val;                                                    \
	 })
#define phx_read_u64(where)						\
	({                                                              \
	 volatile u64 val;                                       \
	 READ64_REGISTER(where, val);				\
	 val;                                                    \
	 })

#define phx_write_u8(where, val)   WRITE8_REGISTER(where, val)
#define phx_write_u16(where, val)  WRITE16_REGISTER(where, val)
#define phx_write_u32(where, val)  WRITE32_REGISTER(where, val)
#define phx_write_u64(where, val)  WRITE64_REGISTER(where, val)
#define phx_write_mask_bit(where, mask, val)				\
	({								\
	 volatile u32 tmp;					\
	 tmp = ((phx_read_u32(where)) & (~(mask))) | ((val) & (mask));\
	 phx_write_u32(where, tmp);				\
	 })

#define phx_read_cache_u8(where)						\
	({                                                              \
	 u8 val;                                        \
	 READ8_CACHE_REGISTER(where, val);				\
	 val;                                                    \
	 })
#define phx_read_cache_u16(where)						\
	({                                                              \
	 u16 val;                                      \
	 READ16_CACHE_REGISTER(where, val);				\
	 val;                                                    \
	 })
#define phx_read_cache_u32(where)						\
	({                                                              \
	 volatile u32 val;                                       \
	 READ32_CACHE_REGISTER(where, val);				\
	 val;                                                    \
	 })
#define phx_read_cache_u64(where)						\
	({                                                              \
	 volatile u64 val;                                       \
	 READ64_CACHE_REGISTER(where, val);				\
	 val;                                                    \
	 })

#define phx_write_cache_u8(where, val)   WRITE8_CACHE_REGISTER(where, val)
#define phx_write_cache_u16(where, val)  WRITE16_CACHE_REGISTER(where, val)
#define phx_write_cache_u32(where, val)  WRITE32_CACHE_REGISTER(where, val)
#define phx_write_cache_u64(where, val)  WRITE64_CACHE_REGISTER(where, val)

// ============================================================read write Transfer


// ============================================================printf uart
#define uart_printf_wait uart_printf
typedef __builtin_va_list __gnuc_va_list;
#define va_start(v,l)   __builtin_va_start(v,l)
#define va_end(v)       __builtin_va_end(v)
#define va_arg(v,l)     __builtin_va_arg(v,l)
typedef __gnuc_va_list va_list;

static const char cmap[16] = "0123456789abcdef";

#define UART_BASE_ADDR		0x1f050000 //0x1f0c8000


static u32 inline uart0_getc_wait()
{
	while ((phx_read_u32(UART_BASE_ADDR + 0x2C) & 0x2)); //until NOT empty
	return phx_read_u8(UART_BASE_ADDR + 0x30);
}

static u32 inline uart1_getc_wait()
{
	while ((phx_read_u32(UART_BASE_ADDR + 0x8000 + 0x2C) & 0x2)); //until NOT empty
	return phx_read_u8(UART_BASE_ADDR + 0x8000 + 0x30);
}

static void inline uart0_putc_wait(u32 data)
{
	while ((phx_read_u32(UART_BASE_ADDR + 0x2C) & 0x10)); //until NOT FULL
	phx_write_u8(UART_BASE_ADDR + 0x30, data);
}

static void inline uart1_putc_wait(u32 data)
{
	while ((phx_read_u32(UART_BASE_ADDR + 0x8000 + 0x2C) & 0x10)); //until NOT FULL
	phx_write_u8(UART_BASE_ADDR + 0x8000 + 0x30, data);
}


static void inline uart_putc_wait(u32 data)
{
#ifdef USING_UART0
	uart0_putc_wait(data);
#else
	uart1_putc_wait(data);
#endif
}

static u32 inline uart_getc_wait()
{
#ifdef USING_UART0
	return uart0_getc_wait();
#else
	return uart1_getc_wait();
#endif
}

static void inline printn(unsigned n, unsigned bas)
{
	unsigned r;
	if ((r = n / bas) != 0)
		printn(r, bas);
	uart_putc_wait(cmap[n % bas]);
}

void inline uart_printf(const char *fmt, ...)
{
	char c, *s;
	unsigned long n;

	va_list args;
	unsigned long t;

	va_start(args, fmt);

	while ((c = *fmt++) != '\0') {
		if (c != '%') {
			uart_putc_wait(c);
			continue;
		}

		switch (c = *fmt++) {
			case 'o':
				t = va_arg(args, int);
				printn(t, 8);
				break;

			case 'd':
			case 'i':
			case 'l':
				t = va_arg(args, int);
				printn(t, 10);
				break;
				//        case 'h':
			case 'x':
				n = va_arg(args, unsigned long);
				// 64-bit
				if(n>>60)
					uart_putc_wait(cmap[(n >> 60) & 0x0f]);
				if(n>>56)
					uart_putc_wait(cmap[(n >> 56) & 0x0f]);
				if(n>>52)
					uart_putc_wait(cmap[(n >> 52) & 0x0f]);
				if(n>>48)
					uart_putc_wait(cmap[(n >> 48) & 0x0f]);
				if(n>>44)
					uart_putc_wait(cmap[(n >> 44) & 0x0f]);
				if(n>>40)
					uart_putc_wait(cmap[(n >> 40) & 0x0f]);
				if(n>>36)
					uart_putc_wait(cmap[(n >> 36) & 0x0f]);
				if(n>>32)
					uart_putc_wait(cmap[(n >> 32) & 0x0f]);

				//32-bit

				if(n>>28)
					uart_putc_wait(cmap[(n >> 28) & 0x0f]);
				if(n>>24)
					uart_putc_wait(cmap[(n >> 24) & 0x0f]);
				if(n>>20)
					uart_putc_wait(cmap[(n >> 20) & 0x0f]);
				if(n>>16)
					uart_putc_wait(cmap[(n >> 16) & 0x0f]);
				if(n>>12)
					uart_putc_wait(cmap[(n >> 12) & 0x0f]);
				if(n>>8)
					uart_putc_wait(cmap[(n >> 8) & 0x0f]);
				if(n>>4)
					uart_putc_wait(cmap[(n >> 4) & 0x0f]);
				uart_putc_wait(cmap[n & 0x0f]);
				break;

			case 's':
				s = va_arg(args, char *);
				for (; *s != '\0'; ++s)
				{
					uart_putc_wait(*s);
				}
				break;

			default:
				c = va_arg(args, int);
				uart_putc_wait(c);
				continue;
		}
	}
	va_end(args);
}

//void format_print(u8 *buf, u32 len, u32 break_len)
//{
//	int i=0;
//	int line = 0;
//	for (i=0; i<len; i++)
//	{
//		if (i%break_len == 0)
//			uclean("0x%x: ", i);
//		uclean("%x ", buf[i]);
//		if ((i+1)%break_len == 0)
//			uclean("\r\n");
//	}
//	uclean("\r\n");
//}

// ============================================================printf uart

#endif  /* ___UDEBUG_H__ */
