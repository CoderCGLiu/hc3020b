#ifndef __I2C_V2_H__
#define __I2C_V2_H__

#include <reworks/types.h>
#include <irq.h>
#include <pthread.h>

//typedef unsigned short u16;
//typedef unsigned int u32;
//typedef unsigned long u64;

#define HZ 1500000000/*3080 1.5G*/

#define AT24C32  0
#define BL24C128 1
#define AD7414   2

#define EEPROM_TYPE BL24C128
//#define EEPROM_TYPE AT24C32
//#define EEPROM_TYPE AD7414

#define MAX_FIFO_LEN 8

#if(EEPROM_TYPE == BL24C128)
#define EEPROM_LEN 16384 // 128kb / 16kB
#define EEPROM_PAGE_LEN 64
#define EEPROM_DEV_ADDR 0x50
#define SEND_CNT_ONCE 64
#elif(EEPROM_TYPE == AT24C32)
#define EEPROM_LEN 4096 // 32kbits
#define EEPROM_PAGE_LEN 32
#define EEPROM_DEV_ADDR 0x50
#define SEND_CNT_ONCE 32
#elif(EEPROM_TYPE == AD7414)
#define EEPROM_LEN 4096 // 32kbits
#define EEPROM_PAGE_LEN 32
#define EEPROM_DEV_ADDR 0x49
#define SEND_CNT_ONCE 32
#endif

//i2c message
#define MAX_MSGS   1024
#define I2C_MSG_BUF_LEN 4

#define CPU_HZ 1500000000

struct i2c_msg {
	u16 addr;
	u16 flags;    // 0-send, 1-recv
	u8* buf;
	u16 len;
	u32 wdelay;
	u32 rdelay;
};

struct i2c_plk_scl_config {
	u32 div_a;
	u32 div_b;
	u32 fpclk;   // mhz
	u32 fscl;    // khz
};

struct s_data {
	int (*init)(struct s_data *data);
	int (*start)(struct s_data *data);
	int (*check)(struct s_data *data);
	char* desc;
};

struct s_task {
	struct s_data *data_list;
	u64 data_list_len;
	u64 cur_task;
	int (*task_step_one)(struct s_task *task);
	int (*auto_run)(struct s_task * task);
};


// ==================== reg bits =====
typedef union {
	volatile u16 v;
	struct {
		volatile u16 RW      :1;
		volatile u16 MS      :1;
		volatile u16 NEA     :1;
		volatile u16 ACKEN   :1;
		volatile u16 HOLD    :1;
		volatile u16 SLVMON  :1;
		volatile u16 CLRFIFO :1;
		volatile u16 RSV     :1;
		volatile u16 DIV_A   :6;
		volatile u16 DIV_B   :2;
	} bits;
} CR;


typedef union {
	volatile u16 v;
	struct {
		volatile u16 RSV0       :3;
		volatile u16 RXRW       :1;
		volatile u16 RSV1       :1;
		volatile u16 RXDV       :1;
		volatile u16 TXDV       :1;
		volatile u16 RXOVF      :1;
		volatile u16 BA         :1;
		volatile u16 RSV2       :7;
	} bits;
} SR;

typedef union  {
	volatile u16 v;
	struct {
		volatile u16 ADDR       :10;
		volatile u16 RSV        :6;
	} bits;
} AR;

typedef union  {
	volatile u16 v;
	struct {
		volatile u16 DAT0       :1;
		volatile u16 DAT1       :1;
		volatile u16 DAT2       :1;
		volatile u16 DAT3       :1;
		volatile u16 DAT4       :1;
		volatile u16 DAT5       :1;
		volatile u16 DAT6       :1;
		volatile u16 DAT7       :1;
		volatile u16 RSV        :8;
	} bits;
} DR;

typedef union {
	volatile u16 v;
	struct {
		volatile u16 COMP       :1;
		volatile u16 DATA       :1;
		volatile u16 NACK       :1;
		volatile u16 TO         :1;
		volatile u16 SLVRDY     :1;
		volatile u16 RXOVF      :1;
		volatile u16 TXOVF      :1;
		volatile u16 RXUNF      :1;
		volatile u16 RSV0       :1;
		volatile u16 ARBLOST    :1;
		volatile u16 RSV1       :6;
	} bits;
} ISR;

typedef union {
	volatile u16 v;
	struct {
		volatile u16 TS0        :1;
		volatile u16 TS1        :1;
		volatile u16 TS2        :1;
		volatile u16 TS3        :1;
		volatile u16 TS4        :1;
		volatile u16 TS5        :1;
		volatile u16 TS6        :1;
		volatile u16 TS7        :1;
		volatile u16 TS8        :1;
		volatile u16 TS9        :1;
		volatile u16 TS10       :1;
		volatile u16 TS11       :1;
		volatile u16 TS12       :1;
		volatile u16 TS13       :1;
		volatile u16 TS14       :1;
		volatile u16 TS15       :1;
	} bits;
} TSR;

typedef union {
	volatile u16 v;
	struct {
		volatile u16 PI0        :1;
		volatile u16 PI1        :1;
		volatile u16 PI2        :1;
		volatile u16 PI3        :1;
		volatile u16 RSV        :12;
	} bits;
} SMPR;

typedef union {
	volatile u16 v;
	struct {
		volatile u16 TO0        :1;
		volatile u16 TO1        :1;
		volatile u16 TO2        :1;
		volatile u16 TO3        :1;
		volatile u16 TO4        :1;
		volatile u16 TO5        :1;
		volatile u16 TO6        :1;
		volatile u16 TO7        :1;
		volatile u16 RSV        :8;
	} bits;
} TOR;

typedef union {
	volatile u16 v;
	struct {
		volatile u16 COMP       :1;
		volatile u16 DATA       :1;
		volatile u16 NACK       :1;
		volatile u16 TO         :1;
		volatile u16 SLVRDY     :1;
		volatile u16 RXOVF      :1;
		volatile u16 TXOVF      :1;
		volatile u16 RXUNF      :1;
		volatile u16 RSV0       :1;
		volatile u16 ARBLOST    :1;
		volatile u16 RSV1       :6;
	} bits;
} IMR;

typedef union {
	volatile u16 v;
	struct {
		volatile u16 COMP       :1;
		volatile u16 DATA       :1;
		volatile u16 NACK       :1;
		volatile u16 TO         :1;
		volatile u16 SLVRDY     :1;
		volatile u16 RXOVF      :1;
		volatile u16 TXOVF      :1;
		volatile u16 RXUNF      :1;
		volatile u16 RSV0       :1;
		volatile u16 ARBLOST    :1;
		volatile u16 RSV1       :6;
	} bits;
} IER;

typedef union {
	volatile u16 v;
	struct {
		volatile u16 COMP       :1;
		volatile u16 DATA       :1;
		volatile u16 NACK       :1;
		volatile u16 TO         :1;
		volatile u16 SLVRDY     :1;
		volatile u16 RXOVF      :1;
		volatile u16 TXOVF      :1;
		volatile u16 RXUNF      :1;
		volatile u16 RSV0       :1;
		volatile u16 ARBLOST    :1;
		volatile u16 RSV1       :6;
	} bits;
} IDR;

typedef union {
	volatile u16 v;
	struct {
		volatile u16 GF0        :1;
		volatile u16 GF1        :1;
		volatile u16 GF2        :1;
		volatile u16 GF3        :1;
		volatile u16 GF4        :1;
		volatile u16 GF5        :1;
		volatile u16 GF6        :1;
		volatile u16 GF7        :1;
		volatile u16 GF8        :1;
		volatile u16 GF9        :1;
		volatile u16 GF10       :1;
		volatile u16 GF11       :1;
		volatile u16 GF12       :1;
		volatile u16 GF13       :1;
		volatile u16 GF14       :1;
		volatile u16 GF15       :1;
	} bits;
} GFCR;



// ==================== reg addr =====
struct i2c_regs {
	volatile CR    cr;          //rw
	u16        __pad0;
	volatile SR    sr;          //ro
	u16        __pad1;
	volatile AR    ar;          //rw
	u16        __pad2;
	volatile DR    dr;          //rw
	u16        __pad3;
	volatile ISR   isr;         //ro
	u16        __pad4;
	volatile TSR   tsr;         //rw
	u16        __pad5;
	volatile SMPR  smpr;        //rw
	u16        __pad6;
	volatile TOR   tor;         //rw
	u16        __pad7;
	volatile IMR   imr;         //ro
	u16        __pad8;
	volatile IER   ier;         //wo
	u16        __pad9;
	volatile IDR   idr;         //wo
	u16        __pad10;
	volatile GFCR  gfcr;        //rw
	u16        __pad11;
};

// ==================== define ======
#define I2C_BASE 0x1F0D0000
#define I2C0_BASE (0x9000000000000000 | ((u64)I2C_BASE))
#define I2C1_BASE (0x9000000000000000 | ((u64)I2C_BASE + 0x8000))
#define I2C0_REGS  ((struct i2c_regs*) I2C0_BASE)
#define I2C1_REGS  ((struct i2c_regs*) I2C1_BASE)
#define I2CS_BASE_ADDR(channel)	(channel ? I2C1_REGS : I2C0_REGS)

int i2c_lib_master_recv_data(struct i2c_regs *host, u16 dev_addr, u16 data_addr, u8 *buf, u32 cnt, u32 wdelay, u32 rdelay);
int i2c_lib_master_send_data(struct i2c_regs *host, u16 dev_addr, u16 data_addr, u8 *buf, u32 cnt, u32 wdelay, u32 rdelay);
void i2c_lib_set_addrmode(struct i2c_regs* host, u8 addrmode);
void i2c_lib_set_fscl_speed(struct i2c_regs *host, u32 fscl);
void i2c_lib_set_master(struct i2c_regs *host, u32 is_master);
void i2c_lib_set_slave_addr(struct i2c_regs* host, u16 addr);

#endif  /* __I2C_V2_H__ */
