#include "./i2c_v2.h"
//#include <libs/ucommand.h>
//#include <libs/umips.h>


static int task_step_one(struct s_task * task)
{
	u64 cur_task = task->cur_task;

	if (cur_task >= task->data_list_len || task->data_list_len == 0)
	{
		printk("cur_task:%d/%d, finished!\r\n", cur_task, task->data_list_len);
		return -1;
	}

	printk("id:[%d/%d], desc:%s\r\n", \
			cur_task, task->data_list_len, task->data_list[cur_task].desc);

	printk("1. init\r\n");
	if (task->data_list[cur_task].init)
	{
		printk("init ret:%d \r\n", task->data_list[cur_task].init(&(task->data_list[cur_task])));
	} else {
		printk("no init function\r\n");
	}

	printk("2. start\r\n");
	if (task->data_list[cur_task].start)
	{
		printk( "start ret:%d\r\n", task->data_list[cur_task].start(&(task->data_list[cur_task])));
	} else {
		printk("no start function\r\n");
	}

	printk("3. check\r\n");
	if (task->data_list[cur_task].check)
	{
		printk("check ret:%d\r\n", task->data_list[cur_task].check(&(task->data_list[cur_task])));
	} else {
		printk("no check function\r\n");
	}

succeed:
	task->cur_task++;
	return 0;
}


static int auto_run(struct s_task * task)
{
	while (0 == task->task_step_one(task));
	printk("+++ all complete! +++\r\n");
}

extern int task7_init(struct s_data *data);
extern int task7_start(struct s_data *data);
extern int task7_check(struct s_data *data);

static struct s_data task_data_list[] = {
	//init,              start,          check,           desc
	//{task0_init,         task0_start,    task0_check,     "task0_i2c0_master"},
	//{task1_init,         task1_start,    task1_check,     "task1_i2c01_memspace"},
	//{task2_init,         task2_start,    task2_check,     "task2_i2c0_slave"},
	//{task3_init,         task3_start,    task3_check,     "task3_i2c1_master"},
	//{task4_init,         task4_start,    task4_check,     "task4_i2c1_slave"},
	//{task5_init,         task5_start,    task5_check,     "task5_i2c0_interrupt"},
	//{task6_init,         0,              task6_check,     "task6_i2c1_interrupt"},
	{task7_init,         task7_start,              task7_check,     "task7_i2c01_connect"},
};

void cmd_i2c_send_msg(int argc, u64 *argv)
{
	int controller = argv[1];
	u32 addr = argv[2];
	u32 data = argv[3];
	if (controller == 0)
	{
		i2c_lib_send_msg(I2C0_REGS, addr, &data, 1);
	} else {
		i2c_lib_send_msg(I2C1_REGS, addr, &data, 1);
	}
}

void cmd_i2c_recv_msg(int argc, u64 *argv)
{
	int controller = argv[1];
	u32 addr = argv[2];
	u32 data = 0;
	if (controller == 0)
	{
		i2c_lib_recv_msg(I2C0_REGS, addr, &data, 1);
	} else {
		i2c_lib_recv_msg(I2C1_REGS, addr, &data, 1);
	}
	printk("recv:0x%x\r\n", data);
}

#if 0
u8 gbuf[1024*16] = {0};
int main()
{
	ubuf_init(gbuf);
	struct cmd_ex ucmd_1 = {
		.name = "i2c_send_msg",
		.help_msg = "i2c_send_msg controller addr data",
		.argc = 4,
		.pfunc = cmd_i2c_send_msg,
	};
	struct cmd_ex ucmd_2 = {
		.name = "i2c_recv_msg",
		.help_msg = "i2c_recv_msg controller addr",
		.argc = 3,
		.pfunc = cmd_i2c_recv_msg,
	};
	ucmd_append(&ucmd_1);
	ucmd_append(&ucmd_2);
	printk("====\r\n");

	//1
	struct s_task task;
	task.data_list = task_data_list;
	task.data_list_len = sizeof(task_data_list)/sizeof(struct s_data);
	task.cur_task = 0;
	task.task_step_one = task_step_one;
	task.auto_run  = auto_run;
	task.auto_run(&task);
	while(1)
	{
		DEBUG_CONSOLE;
	}
}
#endif
