/*!************************************************************
 * @file: hr2NandFlash.h
 * @brief: nand flash驱动接口头文件
 * @author: cetc
 * @date: 06,01,2020 
 * @version: 1.0.0
 * @note:
 * ------------------------------------------------------------
 * Change History:
 * <Date> | <Author> | <Description> 
 * 2020/01/06 | xupeng | 代码整理与规范
 * 
 **************************************************************/
#ifndef _HR2DWC_NANDFLASH_ 
#define _HR2DWC_NANDFLASH_
/*
 * 头文件
 * */
#include "../h/porting.h"

/*x8 : (2K + 64) bytes x 64 pages x 4096 blocks*/
/*
 * A0 – A11 : 	byte (column) address in the page
 * A12 – A17 : 	page address in the block
 * A18 : 		plane address (for multi-plane operations) / block address (for normal operations)
 * A19 – A30 : 	block address
 * */
#define CHIP_SIZE			(512*1024*1024) /*(Byte)不包含spare区*/
#define BLOCK_SIZE			(128*1024) /*(Byte)不包含spare区*/
#define MAIN_SIZE			2048 /*(Byte)  page size of main*/
#define SPARE_SIZE 			64	 /*(Byte)  page size of spare,  
											ECC_EN ON :  64, --- spare区的后一半被用来存储硬件ecc的校验和
											ECC_EN OFF : 128*/
#define PAGE_COUNT			(CHIP_SIZE/MAIN_SIZE) /*总页数量*/
#define PAGES_PER_BLK 		(BLOCK_SIZE/MAIN_SIZE) /*每块的页数量*/
#define BLOCK_COUNT			(CHIP_SIZE/BLOCK_SIZE) /*total number of block in the chip*/


/*********************************************************************************************************
  NAND 控制器
*********************************************************************************************************/
#define NAND_BASE_ADDR                      (0xffffffffbf918000)
#define REG_NAND_CTRL0                      (NAND_BASE_ADDR + 0x00)
#define BIT_NAND_FLASH_WIDTH                (0x1 << 24)
#define REG_NAND_CTRL1                      (NAND_BASE_ADDR + 0x04)
#define REG_NAND_CTRL2                      (NAND_BASE_ADDR + 0x08)
#define BIT_TRANS_MODE                      (0x1 << 0)
#define BIT_SG_EFFECTIVE                    (0x1 << 1)
#define BIT_PAGE_TYPE                       (0xf << 4)
#define REG_NAND_NUM                        (NAND_BASE_ADDR + 0x0c)
#define BIT_NAND_CTRL_WRITE                 (0x1 << 30)
#define BIT_NAND_CTRL_READ                  (0x1 << 31)
#define REG_NAND_TIMEOUT                    (NAND_BASE_ADDR + 0x10)

#define REG_NAND_INT                        (NAND_BASE_ADDR + 0x14)
#define BIT_DMA_COMPLETE_SEND_SG_INT        (0x1 << 0)
#define BIT_DMA_COMPLETE_RECV_SG_INT        (0x1 << 1)
#define BIT_NAND_CTRL_COMPLETE_SEND_CMD_INT (0x1 << 2)
#define BIT_NAND_CTRL_COMPLETE_RECV_CMD_INT (0x1 << 3)
#define BIT_WDDR_DATA_OVER_ERR_INT          (0x1 << 4)
#define BIT_WDDR_NO_DATA_ERR_INT            (0x1 << 5)
#define BIT_RDDR_ERR_INT                    (0x1 << 6)

#define REG_NAND_INT_EN                     (NAND_BASE_ADDR + 0x18)
#define BIT_DMA_SEND_SG_END_INT_EN          (0x1 << 0)
#define BIT_DMA_RECV_SG_END_INT_EN          (0x1 << 1)
#define BIT_NAND_SEND_CMD_END_INT_EN        (0x1 << 2)
#define BIT_NAND_RECV_CMD_END_INT_EN        (0x1 << 3)
#define BIT_WDDR_DATA_OVER_ERR_INT_EN       (0x1 << 4)
#define BIT_WDDR_NO_DATA_ERR_INT_EN         (0x1 << 5)
#define BIT_RDDR_ERR_INT_EN                 (0x1 << 6)

#define REG_NAND_INT_CLEAR                  (NAND_BASE_ADDR + 0x1c)
#define BIT_NAND_CLEAR_SEND_FIFO            (0x1 << 8)
#define BIT_NAND_CLEAR_RECV_FIFO1           (0x1 << 9)
#define BIT_NAND_CLEAR_RECV_FIFO0           (0x1 << 10)
#define BIT_NAND_RESET                      (0x1 << 11)
#define BIT_NAND_START_DMA                  (0x1 << 12)

#define REG_NAND_CTRL_STA1                  (NAND_BASE_ADDR + 0x20)
#define BIT_DMA_IDLE                        (0xf << 12)
#define BIT_DMA_SELF_IDLE                   (0xf << 16)
#define BIT_NAND_CTRL_IDLE                  (0xf << 20)
#define BIT_NAND_RB                         (0x1 << 24) //1: ready  0:busy
#define REG_NAND_CTRL_STA2                  (NAND_BASE_ADDR + 0x24)
#define REG_NAND_CMD                        (NAND_BASE_ADDR + 0x28)
#define BIT_CMD_WAIT                        (0x1 << 31)
#define REG_NAND_ADDR1                      (NAND_BASE_ADDR + 0x2c)
#define REG_NAND_ADDR2                      (NAND_BASE_ADDR + 0x30)
#define REG_NAND_DATA                       (NAND_BASE_ADDR + 0x34)
#define REG_NAND_FIFO_DATA(n)               (NAND_BASE_ADDR + 0x38 + (n) * 4)
#define REG_NAND_SG                         (NAND_BASE_ADDR + 0x80)
#define NAND_SG_ADDR0                       (REG_NAND_SG + 0x00)
#define NAND_SG_ADDR1                       (REG_NAND_SG + 0x04)
#define NAND_SG_ADDR2                       (REG_NAND_SG + 0x08)
#define NAND_SG_ADDR3                       (REG_NAND_SG + 0x0c)
#define NAND_SG_ADDR4                       (REG_NAND_SG + 0x10)
#define NAND_SG_ADDR5                       (REG_NAND_SG + 0x14)
#define NAND_SG_ADDR6                       (REG_NAND_SG + 0x18)
#define NAND_SG_ADDR7                       (REG_NAND_SG + 0x1c)
#define REG_NAND_BCH_CONFIG                 (NAND_BASE_ADDR + 0x40)
#define BIT_BCH_BIT_NUM                     (0x3f << 0)
#define BIT_BCH_DECODE_EN                   (0x1 << 8)
#define BIT_BCH_ENCODE_EN                   (0x1 << 9)

/*********************************************************************************************************
  宏定义
*********************************************************************************************************/
#define MAX_WAIT_TIME                       0x1FFFF                     /* 最大等待时长                 */
#define CHIP_DELAY                          50                          /* 延时时长                     */
#define NAND_N0_START_BLOCK                 10
#define NAND_N0_BLOCK_NUM                   100

/* nand command */
#define RESET           0xff
#define READ_ID         0x90
#define READ_STATUS     0x70/*READ STATUS REGISTER*/
#define ERASE_1ST       0x60
#define ERASE_2ND       0xd0
#define PROGRAM_1ST     0x80
#define PROGRAM_2ND     0x10
#define READ_1ST        0x00
#define READ_2ND        0x30

#define WIDTH_16BIT	0x1
#define WIDTH_8BIT	0x0

#define CPU_TRANS_MODE	0x0
#define DMA_SELF_TRANS_MODE	0x1

#define PAGE_ECC_SIZE 52 /* 32B/page */

#define BIT_BCH_ERR			(0x1 << 25) //1:bch err, read clear
#define BIT_FF_FLAG			(0x1 << 26) //1:all 0xff

/*
 * 函数声明
 * */
void usrFfxConfig();

#endif
