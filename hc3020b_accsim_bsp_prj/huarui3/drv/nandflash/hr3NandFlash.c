/*!************************************************************
 * @file: hr2NandFlash.c
 * @brief: nand flash驱动实现接口
 * @author: cetc
 * @date: 06,01,2020 
 * @version: 1.0.0
 * @note:
 * ------------------------------------------------------------
 * Change History:
 * <Date> | <Author> | <Description>
 * 2019/03/26 | ww     | 删除了不合理的延时 
 * 2020/01/06 | xupeng | 代码整理与规范
 * 
 **************************************************************/
/*
 * 头文件
 * */
#include <stdio.h>
#include <taskLib.h> 
#include <fcntl.h>
#include "tool/gnu/toolMacros.h"
#include "./huarui3/h/tool/hr3SysTools.h"
#include "hr3NandFlash.h"
#include "sys/stat.h"


#include <pthread.h>
static pthread_mutex_t muteSem;

/*
 * 宏定义
 * */
#define PAGES_PER_BLOCK     64          /* 64 pages per block on a single chip*/
   
typedef unsigned long CardAddress;	/* Physical offset on card */

#define READ32_REGISTER(x, y)  				\
		y = *((volatile unsigned int *)x)		

#define WRITE32_REGISTER(x, y) 				\
		*((volatile unsigned int  *)x) = y		


#define READ8_REGISTER(x, y) 				\
		y= *((volatile unsigned char *)x) 


#define WRITE8_REGISTER(x, y) 			\
		*((volatile unsigned char *)x)= y 		


#define READ16_REGISTER(x, y) 				\
		y = *((volatile unsigned short *)x)

#define WRITE16_REGISTER(x, y) 			\
		*((volatile unsigned short *)x)  = y


#define WRITE_MASK_REGISTR(where, mask, val) 	\
	{											\
		unsigned int tmp;						\
		READ32_REGISTER(where,tmp);				\
		tmp = ((tmp) & (~(mask))) | ((val) & (mask));\
		WRITE32_REGISTER(where, tmp);			\
	} 

/*
 * 外部函数声明
 * */
extern void timeDelay(unsigned int loopNum);

typedef struct
{
	unsigned char MakerCode;
	unsigned char DeviceCode;
	unsigned short int iChipNum ;
	unsigned short int iBlockNum ;
		
	unsigned short int iCellType ;
	unsigned short int iSimuProgPageNum ; /*Number of Simultaneously Programmed Pages*/
		
	unsigned short int iPageType ;
	unsigned int iPageSize ;
	unsigned int iBlockSize ;
	unsigned short int iPageNumPerBlock ;
	unsigned int iReduAreaSize ;
	unsigned short int iWidth ;
	unsigned short int iPlaneNum ;
	unsigned int iPlaneSize ;
	
}NANDINFO;
NANDINFO nandInfo;

#ifdef RELFS_NAND
IMPORT STATUS usrRelFsInit(unsigned nConfigOptions, unsigned nMaxHandles);
IMPORT STATUS relFsFmtLibInit(void);
IMPORT STATUS RelRegInit(void);
IMPORT STATUS relFsPartLibInit(void);
IMPORT STATUS RelFmtInit(void);
IMPORT STATUS relFsChkLibInit(void);
IMPORT STATUS relFsToolLibInit(void);
IMPORT void   relFsPortTestInit(void);
IMPORT STATUS RelDclTestInit(void);
IMPORT STATUS RelDclTestFsioInit(void);
IMPORT STATUS RelDclShellInit(void);
IMPORT STATUS relFsShellInit(void);

IMPORT STATUS   usrFlashFXInit62(unsigned nFlags);
IMPORT STATUS   usrFlashFXDeviceInit62(char *pszDevName, unsigned nDiskNum, unsigned nDeviceNum, unsigned nOffsetKB, unsigned nLengthKB, unsigned nFormatState, unsigned nFileSystemID, unsigned nDiskFlags);
#endif

/*
 * 内部全局变量
 * */
static unsigned int nandEccFlag = 0;
/* 向flashfx库提供的配置参数 */
unsigned int nandPageSize 	= 0x800; 		/* 2048B */
unsigned int nandSpareSize 	= 0x40; 		/* 64B */
unsigned int nandBlockSize 	= 0x20000; 		/* 128KB */
unsigned int nandTotalSize 	= 0x80000000; 	/* 512MB */
BOOL nandEccEnable 			= TRUE;			/* 标示是否需要使能ECC */
BOOL nandPrintDbgInfo 		= FALSE;  		/* 标示是否需要打印调试信息 */

static void waitForIdleStatus()
{
	unsigned int iTmpVal = 0;
	unsigned int t0 = 0;
	unsigned int t1 = 0;
	
	t0 = tickGet();
	do {
		t1 = tickGet();
   		if(t1 >= t0)
   		{
   			if((t1 - t0) >= LOOP_TIMER_OUT * sysClkRateGet())
   			{
   				printf("[DSP%d]读取nand Idle状态寄存器超时.\n",sysCpuGetID());
   				break;
   			}
   		}
   		else
   		{
   			if(t1 >= LOOP_TIMER_OUT * sysClkRateGet())
   			{
   			   	printf("[DSP%d]读取nand Idle状态寄存器超时.\n",sysCpuGetID());
   			   	break;
   			}
   		}
		
	    READ32_REGISTER(REG_NAND_CTRL_STA1,iTmpVal);
	    nandEccFlag |= ((iTmpVal & BIT_FF_FLAG) ? 0x0 : ((iTmpVal & BIT_BCH_ERR) >> 25));
	    
//	    printf("nandEccFlag = %d\n", nandEccFlag);//ww mod 20180909 add
	    
	} while (iTmpVal & BIT_NAND_CTRL_IDLE);
}

static void waitForIdleStatus_dma()
{
	unsigned int iTmpVal = 0;
	unsigned int t0 = 0;
	unsigned int t1 = 0;
	
	t0 = tickGet();
	do {
		timeDelay(100);
		if(t1 >= t0)
   		{
   			if((t1 - t0) >= LOOP_TIMER_OUT * sysClkRateGet())
   			{
   				printf("[DSP%d]读取nand Idle状态寄存器超时.\n",sysCpuGetID());
   				break;
   			}
   		}
   		else
   		{
   			if(t1 >= LOOP_TIMER_OUT * sysClkRateGet())
   			{
   			   	printf("[DSP%d]读取nand Idle状态寄存器超时.\n",sysCpuGetID());
   			   	break;
   			}
   		}
		
	    READ32_REGISTER(REG_NAND_CTRL_STA1,iTmpVal);
	    
	} while (iTmpVal & BIT_DMA_IDLE);
} 

static unsigned int get_nand_status()
{
	u32 val = 0;
	WRITE32_REGISTER(REG_NAND_CMD, (0x1 << 31) | READ_STATUS);
	waitForIdleStatus();
	READ32_REGISTER(REG_NAND_DATA,val);
	return val;

}
static void waitForNandReady()
{
#if 0
	volatile unsigned int val;
	do {
		timeDelay(100);
		READ32_REGISTER(REG_NAND_CTRL_STA1,val);
	} while (!(val & BIT_NAND_RB));
#else
	unsigned int val = 0;
	unsigned int t0 = 0;
	unsigned int t1 = 0;
	
	t0 = tickGet();
	do {
		t1 = tickGet();
   		if(t1 >= t0)
   		{
   			if((t1 - t0) >= LOOP_TIMER_OUT * sysClkRateGet())
   			{
   				printf("[DSP%d]读取nand Idle状态寄存器超时.\n",sysCpuGetID());
   				break;
   			}
   		}
   		else
   		{
   			if(t1 >= LOOP_TIMER_OUT * sysClkRateGet())
   			{
   			   	printf("[DSP%d]读取nand Idle状态寄存器超时.\n",sysCpuGetID());
   			   	break;
   			}
   		}
		val = get_nand_status();
	} while (!(val & 0x40));

	get_nand_status();
	waitForIdleStatus();
	WRITE32_REGISTER(REG_NAND_CMD,(0x1 << 31) | 0x0);
	waitForIdleStatus();
#endif
}
static unsigned char readNandStatus()
{
	unsigned int val = 0;
	WRITE32_REGISTER(REG_NAND_CMD, READ_STATUS);
	waitForIdleStatus();
	READ32_REGISTER(REG_NAND_DATA,val);
	return (val & 0x1);
}

static BOOL nandEccResult()
{
	unsigned int val = 0;
	
	READ32_REGISTER(REG_NAND_CTRL_STA1,val);
	nandEccFlag |= ((val & BIT_FF_FLAG) ? 0x0 : ((val & BIT_BCH_ERR) >> 25));
	if (nandEccFlag)
		return TRUE;
	else
		return FALSE;	
}
 BOOL readNandInfo()
{
	unsigned int iTmpVal = 0;
	unsigned char id[5] = {0};
	unsigned short int i = 0;
	
	/* reset the nand flash */
	WRITE32_REGISTER(REG_NAND_CMD, RESET);
	waitForIdleStatus();
		
	/* read nandflash info-Maker Code,*/
	WRITE32_REGISTER(REG_NAND_CMD, READ_ID);
	waitForIdleStatus();

	WRITE32_REGISTER(REG_NAND_ADDR2, ((0x1 << 31) | 0x0));

	for (i = 0; i < 5; i++) 
	{
		READ32_REGISTER(REG_NAND_DATA,iTmpVal);
		id[i] = (unsigned char) (iTmpVal);
	}

	printf("0x%x\n",id[0]);
	printf("0x%x\n",id[1]);
	printf("0x%x\n",id[2]);
	printf("0x%x\n",id[3]);
	printf("0x%x\n",id[4]);
	
	nandInfo.MakerCode = id[0];
	nandInfo.DeviceCode = id[1];

	switch (id[2] & 0x03) 
	{
	case 0:
		nandInfo.iChipNum = 1;
		break;
	case 1:
		nandInfo.iChipNum = 2;
		break;
	case 2:
		nandInfo.iChipNum = 4;
		break;
	case 3:
		nandInfo.iChipNum = 8;
		break;
	default:
		break;
	}

	switch ((id[2] & 0x0c) >> 2) 
	{
	case 0:
		nandInfo.iCellType = 2;
		break;
	case 1:
		nandInfo.iCellType = 4;
		break;
	case 2:
		nandInfo.iCellType = 8;
		break;
	case 3:
		nandInfo.iCellType = 16;
		break;
	default:
		break;
	}

	switch ((id[2] & 0x30) >> 4) 
	{
	case 0:
		nandInfo.iSimuProgPageNum = 1;
		break;
	case 1:
		nandInfo.iSimuProgPageNum = 2;
		break;
	case 2:
		nandInfo.iSimuProgPageNum = 4;
		break;
	case 3:
		nandInfo.iSimuProgPageNum = 8;
		break;
	default:
		break;
	}

	switch (id[3] & 0x03) 
	{
	case 0:
		nandInfo.iPageType = 0;
		nandInfo.iPageSize = 1024;
		break;
	case 1:
		nandInfo.iPageType = 1;
		nandInfo.iPageSize = 2 * 1024;
		break;
	case 2:
		nandInfo.iPageType = 2;
		nandInfo.iPageSize = 4 * 1024;
		break;
	case 3:
		nandInfo.iPageType = 3;
		nandInfo.iPageSize = 8 * 1024;
		break;
	default:
		break;
	}

	switch ((id[3] & 0x30) >> 4) 
	{
	case 0:
		nandInfo.iBlockSize = 64 * 1024;
		break;
	case 1:
		nandInfo.iBlockSize = 128 * 1024;
		break;
	case 2:
		nandInfo.iBlockSize = 256 * 1024;
		break;
	case 3:
		nandInfo.iBlockSize = 512 * 1024;
		break;
	default:
		break;
	}

	switch ((id[3] & 0x04) >> 2) 
	{
	case 0:
		nandInfo.iReduAreaSize = 8;
		break;
	case 1:
		nandInfo.iReduAreaSize = 16;
		break;
	default:
		break;
	}

	switch ((id[3] & 0x40) >> 6) 
	{
	case 0:
		nandInfo.iWidth = 0;
		break;
	case 1:
		nandInfo.iWidth = 1;
		break;
	default:
		break;
	}
	
	switch ((id[4] & 0x0c) >> 2) 
	{
	case 0:
		nandInfo.iPlaneNum = 1;
		break;
	case 1:
		nandInfo.iPlaneNum = 2;
		break;
	case 2:
		nandInfo.iPlaneNum = 4;
		break;
	case 3:
		nandInfo.iPlaneNum = 8;
		break;
	default:
		break;
	}
	
	switch ((id[4] & 0x0c) >> 2) 
	{
	case 0:
		nandInfo.iPlaneNum = 1;
		break;
	case 1:
		nandInfo.iPlaneNum = 2;
		break;
	case 2:
		nandInfo.iPlaneNum = 4;
		break;
	case 3:
		nandInfo.iPlaneNum = 8;
		break;
	default:
		break;
	}

	switch ((id[4] & 0x70) >> 4) 
	{
	case 0:
		nandInfo.iPlaneSize = 64; /* Mb */
		break;
	case 1:
		nandInfo.iPlaneSize = 128; /* Mb */
		break;
	case 2:
		nandInfo.iPlaneSize = 256; /* Mb */
		break;
	case 3:
		nandInfo.iPlaneSize = 512; /* Mb */
		break;
	case 4:
		nandInfo.iPlaneSize = 1024; /* Mb */
		break;
	case 5:
		nandInfo.iPlaneSize = 2048; /* Mb */
		break;
	case 6:
		nandInfo.iPlaneSize = 4096; /* Mb */
		break;
	case 7:
		nandInfo.iPlaneSize = 8192; /* Mb */
		break;
	default:
		break;
	}

	nandInfo.iBlockNum = nandInfo.iPlaneSize / 8 * 1024 * 1024
			/ nandInfo.iBlockSize * nandInfo.iPlaneNum * nandInfo.iChipNum;
	nandInfo.iPageNumPerBlock = nandInfo.iBlockSize / nandInfo.iPageSize;
#if 1
	printf("nandInfo.MakerCode = 0x%x \n", nandInfo.MakerCode);
	printf("nandInfo.DeviceCode = 0x%x \n", nandInfo.DeviceCode);
	printf("nandInfo.iBlockNum = %d \n", nandInfo.iBlockNum);
	printf("nandInfo.iBlockSize = 0x%x \n", nandInfo.iBlockSize);
	printf("nandInfo.iCellType = 0x%x \n", nandInfo.iCellType);
	printf("nandInfo.iChipNum = %d \n", nandInfo.iChipNum);
	printf("nandInfo.iPageNumPerBlock = %d \n", nandInfo.iPageNumPerBlock);
	printf("nandInfo.iPageSize = 0x%x \n", nandInfo.iPageSize);
	printf("nandInfo.iPageType = 0x%x \n", nandInfo.iPageType);
	printf("nandInfo.iPlaneNum = %d \n", nandInfo.iPlaneNum);
	printf("nandInfo.iPlaneSize = 0x%x Mb\n", nandInfo.iPlaneSize);
	printf("nandInfo.iReduAreaSize = 0x%x \n", nandInfo.iReduAreaSize);
	printf("nandInfo.iSimuProgPageNum = 0x%x \n", nandInfo.iSimuProgPageNum);
	printf("nandInfo.iWidth = 0x%x \n", nandInfo.iWidth);
#endif	
#if 0
	printstr("nandInfo.MakerCode = 0x");printnum( nandInfo.MakerCode);printstr("\r\n");
	printstr("nandInfo.DeviceCode = 0x"); printnum(nandInfo.DeviceCode);printstr("\r\n");
	printstr("nandInfo.iBlockNum = 0x"); printnum(nandInfo.iBlockNum);printstr("\r\n");
	printstr("nandInfo.iBlockSize = 0x"); printnum(nandInfo.iBlockSize);printstr("\r\n");
	printstr("nandInfo.iCellType = 0x"); printnum(nandInfo.iCellType);printstr("\r\n");
	printstr("nandInfo.iChipNum = 0x"); printnum(nandInfo.iChipNum);printstr("\r\n");
	printstr("nandInfo.iPageNumPerBlock = 0x"); printnum(nandInfo.iPageNumPerBlock);printstr("\r\n");
	printstr("nandInfo.iPageSize = 0x"); printnum(nandInfo.iPageSize);printstr("\r\n");
	printstr("nandInfo.iPageType = 0x"); printnum(nandInfo.iPageType);printstr("\r\n");
	printstr("nandInfo.iPlaneNum = 0x"); printnum(nandInfo.iPlaneNum);printstr("\r\n");
	printstr("nandInfo.iPlaneSize = 0x"); printnum(nandInfo.iPlaneSize);printstr("Mb\r\n");
	printstr("nandInfo.iReduAreaSize = 0x"); printnum(nandInfo.iReduAreaSize);printstr("\r\n");
	printstr("nandInfo.iSimuProgPageNum = 0x"); printnum(nandInfo.iSimuProgPageNum);printstr("\r\n");
	printstr("nandInfo.iWidth = 0x"); printnum(nandInfo.iWidth);printstr("\r\n");
#endif	
	return TRUE;
}

static void configEraseAddr(unsigned short int block)
{
	unsigned int row_addr = 0;
	//printf("block = %d,nandInfo.iPageNumPerBlock = %d\n",block,nandInfo.iPageNumPerBlock);
	row_addr = ((unsigned int)block) * 64;
	//printf("EraseAddr = 0x%x\n",row_addr);
	WRITE32_REGISTER(REG_NAND_ADDR2, (row_addr & 0xffffff) | (0x2 << 24) | (0x1 << 31));
}

/*page_size = main + spare = 2048 + 64 = 2112B, 寻址需要12bit, 地址占A11~A0
 * page_count = chip_size / page_size = 512MB/2KB = 0x40000, 寻址需要18bit, 地址占A29~A12
 * CardAddress = row_addr | column_addr = A29~A12 | A11~A0  , 4字节寻址
 * 
 * 写到寄存器：
 * addr0 = A7~A0 = column_addr & 0xff ----- REG_NAND_ADDR2[7:0]
 * addr1 = A11~A8 = (column_addr>>8) & 0xff ----- REG_NAND_ADDR2[15:8]
 * addr2 = A19~A12 = row_addr & 0xff ----- REG_NAND_ADDR2[23:16]
 * addr3 = A27~A20 = (row_addr>>8) & 0xff ----- REG_NAND_ADDR1[7:0]
 * addr4 = A29~A28 = (row_addr>>16) & 0xff ----- REG_NAND_ADDR1[15:8]
 * */
static void configRWAddr(CardAddress addr)//unsigned long addr
{
	unsigned int row_addr = 0;
	unsigned int column_addr = 0;
	row_addr = addr/2112;/*页号*/
	column_addr = addr % 2112;/*页内偏移*/
	
	WRITE32_REGISTER(REG_NAND_ADDR1, ((row_addr >> 8) & 0xffff));
	WRITE32_REGISTER(REG_NAND_ADDR2, ((column_addr & 0xffff) | ((row_addr & 0xff) << 16) | (0x5 << 24) | (0x1 << 31)));
}

static void readDataFromFIFO(void *rdata, unsigned int length)
{
	
	unsigned int bTrue = 1;
	volatile unsigned int val = 0;
	unsigned int fifo_id = 0;
	unsigned int i = 0;
	unsigned int word_num = 0;
	int lockVal = 0;
	word_num = length/4;
	do {
	
		READ32_REGISTER(REG_NAND_CTRL_STA2,val);
		if (((val & 0x0fff0000) >> 16) >= word_num) 
		{
			fifo_id = 0;
			break;
		} 
		else if ((val & 0x00000fff) >= word_num) 
		{
			fifo_id = 1;
			break;
		}
		
	}while (bTrue);
#if 0
	do{
		READ32_REGISTER(REG_NAND_FIFO_DATA(fifo_id),*(((unsigned int *)rdata) + i));
		i ++;
	}while(i < word_num);
#endif
#if 1  
	for (i = 0; i < word_num; i++) 
	{
		READ32_REGISTER(REG_NAND_FIFO_DATA(fifo_id),*(((unsigned int *)rdata) + i));
		timeDelay(30);//modify by ww 20200726 from 30 ->80
//		printk("*(((unsigned int *)rdata) + %d) = %d \n",i,*(((unsigned int *)rdata) + i));
		
	}
	
#else
	
	for (i = 0; i < word_num *4 ; i++) 
	{
		READ8_REGISTER(REG_NAND_FIFO_DATA(fifo_id),*(((unsigned char *)rdata) + i));
//		printk("*(((unsigned int *)rdata) + %d) = %d \n",i,*(((unsigned int *)rdata) + i));
	}
	
#endif
	
	READ32_REGISTER(REG_NAND_FIFO_DATA(fifo_id),val);//ww mod 20180908 del
	
#if 0
	if(length%4 == 1)
	{
		*((unsigned char *)rdata + i*4 /*+ 1*/) = val & 0xff;
	}
	else if(length%4 == 2)
	{
		*((unsigned short int *)rdata + i*2 /*+ 1*/) = val & 0xffff;
	}
	else if(length%4 == 3)
	{
		*((unsigned char *)rdata + i*4 + 0) = val & 0x0000ff;
		*((unsigned char *)rdata + i*4 + 1) = (val & 0x00ff00) >> 8;
		*((unsigned char *)rdata + i*4 + 2) = (val & 0xff0000) >> 16;
	}
#endif
}


void resetNand()
{
#if 1
	WRITE32_REGISTER(REG_NAND_CTRL0, 0x02000c01);
	WRITE32_REGISTER(REG_NAND_CTRL1, 0x01000100);
	 
	WRITE_MASK_REGISTR(REG_NAND_CTRL2, BIT_PAGE_TYPE, 0x1 << 4);
	WRITE32_REGISTER(REG_NAND_CTRL2, 0xfff010); 
#else
//	WRITE32_REGISTER(REG_NAND_CTRL0, 0x02005032);
//	WRITE32_REGISTER(REG_NAND_CTRL1, 0x14001000);
	 
//	WRITE_MASK_REGISTR(REG_NAND_CTRL2, BIT_PAGE_TYPE, 0x1 << 4);
//	WRITE32_REGISTER(REG_NAND_CTRL2, 0x800010); 
#endif
//	WRITE_MASK_REGISTR(REG_NAND_BCH_CONFIG, BIT_BCH_BIT_NUM, 8 << 0); /* 8 bits ecc*/
	
	/*disable BCH decode*/
	WRITE_MASK_REGISTR(REG_NAND_BCH_CONFIG, BIT_BCH_DECODE_EN, 0x0);
	/*disable BCH encode*/
	WRITE_MASK_REGISTR(REG_NAND_BCH_CONFIG, BIT_BCH_ENCODE_EN, 0x0);
	
	/* reset the nand flash */
	WRITE32_REGISTER(REG_NAND_CMD, RESET);
	waitForIdleStatus();	
}
 
void switchToNand()
{
//	*(unsigned int *)SYSTEM_CFG2_REG = 0x1;/*ldf 20230511*/
	resetNand();    
	/*  此处添加挂载relfs 的代码 */
#ifdef RELFS_NAND
	//usrFlashFXDeviceInit62("/ffx0",0,0,0,0x200000,1,2,0);
#endif
}

int nandflashBlockErase(int firstErasableBlock,int numOfErasableBlocks)
{
#if 0
	int thisSector = firstErasableBlock;
	int sectorsToErase = numOfErasableBlocks;

	for (; thisSector < firstErasableBlock + sectorsToErase; thisSector++) 
	{
		//t1 = tickGet();
		WRITE32_REGISTER(REG_NAND_CMD, ERASE_1ST);
		waitForIdleStatus();
		configEraseAddr(thisSector);
		waitForIdleStatus();
		WRITE32_REGISTER(REG_NAND_CMD, ERASE_2ND);
		waitForIdleStatus();
		waitForNandReady();
		if (readNandStatus())
		{
			return ERROR;
		}
	}
#else/*ldf 20230522 add*/
	int i = 0;
	for(i = firstErasableBlock; i < numOfErasableBlocks; i++)
		flash_pageio_erase(i);
#endif
	return OK;		
}
int nandflashPageRead(CardAddress address, int intnum,unsigned int * buffer,BOOL eccEnable)
{
#if 0
	int i = 0;
	unsigned int val = 0;
	unsigned short int  eccSize = 0;
	nandEccFlag = 0;
	#if 1	
	if(eccEnable)
	{
		WRITE_MASK_REGISTR(REG_NAND_BCH_CONFIG, BIT_BCH_DECODE_EN, BIT_BCH_DECODE_EN);
		eccSize = 13*4;	
	}
	else
	{
		WRITE_MASK_REGISTR(REG_NAND_BCH_CONFIG, BIT_BCH_DECODE_EN, 0x0);
		eccSize = 0;	
	}
	#endif
	WRITE_MASK_REGISTR(REG_NAND_CTRL2, BIT_TRANS_MODE | BIT_SG_EFFECTIVE /*| BIT_PAGE_TYPE*/, CPU_TRANS_MODE | (0x0 << 1) /*| (0x3 << 4)*/);
	WRITE32_REGISTER(REG_NAND_CMD, READ_1ST);
	waitForIdleStatus();

	configRWAddr(address);
	waitForIdleStatus();

	WRITE32_REGISTER(REG_NAND_CMD,READ_2ND| (0x1 << 31));
	waitForIdleStatus();
	waitForNandReady();

	WRITE32_REGISTER(REG_NAND_NUM, BIT_NAND_CTRL_READ | (intnum*4 - 1) | (eccSize << 16));
	readDataFromFIFO(buffer,intnum*4);

	if(eccEnable)
	{
		if(nandEccResult())
		{
			printf("nand rd ecc error!\n");
			return ERROR;
		}
	}
	#if 0
	 for(i = 0; i < intnum; i++)
	{
		if(*(buffer + i) != 0x12345000 + i)
		{
			printf("0x%x, ",*(buffer + i));
		}
	}
	#endif	
#else/*ldf 20230522*/
	 //nandflashPageRead(CardAddress address, int intnum,unsigned int * buffer,BOOL eccEnable)
	 u32 ulStartPage = (u32)(address / (2048 + 64));
	 u32 pages = intnum*4 / 2048;
	 u32 i = 0;
	 for(i = ulStartPage; i < (ulStartPage + pages); i++)
	 {
		 flash_pageio_read(ulStartPage, buffer, 0);
		 buffer += 2048;
	 }
#endif
	 return OK;
}
int nandflashPageWrite(CardAddress address, int intnum,unsigned int *buffer,BOOL eccEnable)
{
#if 0
	unsigned int i = 0;
	unsigned short int  eccSize = 0;
	nandEccFlag = 0;
	#if 1	
	if(eccEnable)
	{
		WRITE_MASK_REGISTR(REG_NAND_BCH_CONFIG, BIT_BCH_ENCODE_EN, BIT_BCH_ENCODE_EN);
		eccSize = 13 * 4;	
	}
	else
	{
		WRITE_MASK_REGISTR(REG_NAND_BCH_CONFIG, BIT_BCH_ENCODE_EN, 0x0);
		eccSize = 0;	
	}
	#endif
	WRITE_MASK_REGISTR(REG_NAND_CTRL2, BIT_TRANS_MODE | BIT_SG_EFFECTIVE /*| BIT_PAGE_TYPE*/, CPU_TRANS_MODE | (0x0 << 1) /*| (0x3 << 4)*/);
	WRITE32_REGISTER(REG_NAND_CMD, PROGRAM_1ST);
	waitForIdleStatus();
	configRWAddr(address);
	waitForIdleStatus();
	
	WRITE32_REGISTER(REG_NAND_NUM, BIT_NAND_CTRL_WRITE | (intnum*4 - 1) | (eccSize << 16));
		
	for (i = 0; i < intnum; i++) 
	{	
		WRITE32_REGISTER(REG_NAND_DATA, buffer[i]);
	}
	
	waitForIdleStatus();
	WRITE32_REGISTER(REG_NAND_CMD,PROGRAM_2ND | (0x1 << 31));
	waitForIdleStatus();
	waitForNandReady();

	if(readNandStatus())
	{
		printf("readNandStatus error.\n");
//		return ERROR;
	}
	
	if(eccEnable)
	{
		if(nandEccResult())
		{
			printf("nand wr ecc error!\n");
			return ERROR;
		}
	}
#else/*ldf 20230522*/
	 u32 ulStartPage = (u32)(address / (2048 + 64));
	 u32 pages = intnum*4 / 2048;
	 u32 i = 0;
	 for(i = ulStartPage; i < (ulStartPage + pages); i++)
	 {
		 flash_pageio_write(ulStartPage, buffer, 0);
		 buffer += 2048;
	 }
#endif	
	return OK;
}
void nandflashLock(void)
{
	WRITE32_REGISTER(REG_NAND_CMD, 0x2A);
	waitForIdleStatus();
}

void nandflashUnlock(void)
{
	WRITE32_REGISTER(REG_NAND_CMD, 0x23);
	waitForIdleStatus();
	WRITE32_REGISTER(REG_NAND_ADDR2, ((0x1 << 31) | 0x0));
	WRITE32_REGISTER(REG_NAND_CMD, 0x24);
	waitForIdleStatus();
	WRITE32_REGISTER(REG_NAND_ADDR2, ((0x1 << 31) | 64));
}



/* 向flashfx库提供的函数接口 */
BOOL readNandId(void)
{
	unsigned int iTmpVal = 0;
	unsigned char id[5] = {0};
	unsigned short int i = 0;

	/* read nandflash info-Maker Code,*/
	WRITE32_REGISTER(REG_NAND_CMD, READ_ID);
	waitForIdleStatus();

	WRITE32_REGISTER(REG_NAND_ADDR2, ((0x1 << 31) | 0x0));

	for (i = 0; i < 5; i++) 
	{
		READ32_REGISTER(REG_NAND_DATA,iTmpVal);
		id[i] = (unsigned char) (iTmpVal);
		printf("0x%x ",id[i]);
	}
	printf("\n");
	
	return TRUE;
}

int readOnePage 
(
unsigned int   ulStartPage,
unsigned char *pPageBuff
/*D_BUFFER       *pSpareBuff*/
/*BOOL eccEnable*/)
{
	CardAddress address = 0;
	
	unsigned int uCount_main = 2048;
	unsigned int uCount_spare = 64;
	
	unsigned short int iPageType = 1; /* 2048B */
	unsigned short int  eccSize = 0;
//	int lockVal = 0;
	address = (CardAddress)(ulStartPage * (2048 + 64));
//	lockVal = intCpuLock();	
	pthread_mutex_lock2(&muteSem,WAIT, NO_TIMEOUT);
	
//	/*disable BCH decode*/
//	WRITE_MASK_REGISTR(REG_NAND_BCH_CONFIG, BIT_BCH_DECODE_EN, 0x0);
//	/*disable BCH encode*/
//	WRITE_MASK_REGISTR(REG_NAND_BCH_CONFIG, BIT_BCH_ENCODE_EN, 0x0);
//	eccSize = 0;
	
	WRITE_MASK_REGISTR(REG_NAND_CTRL2, BIT_TRANS_MODE | BIT_SG_EFFECTIVE /*| BIT_PAGE_TYPE*/, CPU_TRANS_MODE | (0x0 << 1) /*| (0x3 << 4)*/);
	/* read main memeory */
	WRITE32_REGISTER(REG_NAND_CMD, READ_1ST);
	waitForIdleStatus();

	configRWAddr(address);
	waitForIdleStatus();
		
	WRITE32_REGISTER(REG_NAND_CMD,READ_2ND | (0x1 << 31));
	waitForIdleStatus();

	waitForNandReady();
	
	
	WRITE32_REGISTER(REG_NAND_NUM, BIT_NAND_CTRL_READ | (uCount_main - 1) | (eccSize << 16));
	readDataFromFIFO(pPageBuff, uCount_main);
	
	if(readNandStatus())
	{
		printf("readNandStatus error.\n");
	}
	waitForNandReady();//modify by ww 20200723 add
	
//	intCpuUnlock(lockVal);
	pthread_mutex_unlock(&muteSem);
	return OK;
}
int readOneSpare 
(
unsigned int        ulPage,
unsigned char       *pSpareBuff
)
{
	CardAddress address = 0;
	unsigned int uCount_spare = 64;
	unsigned short int  eccSize = 0;
	int lockVal = 0;
	
//	/*disable BCH decode*/
//	WRITE_MASK_REGISTR(REG_NAND_BCH_CONFIG, BIT_BCH_DECODE_EN, 0x0);
//	/*disable BCH encode*/
//	WRITE_MASK_REGISTR(REG_NAND_BCH_CONFIG, BIT_BCH_ENCODE_EN, 0x0);
//	eccSize = 0;
	
	address = (CardAddress)(ulPage * (2048 + 64) + 2048);
 
	WRITE_MASK_REGISTR(REG_NAND_CTRL2, BIT_TRANS_MODE | BIT_SG_EFFECTIVE /*| BIT_PAGE_TYPE*/, CPU_TRANS_MODE | (0x0 << 1) /*| (0x3 << 4)*/);
	/* read spare memeory */
	WRITE32_REGISTER(REG_NAND_CMD, READ_1ST);
	waitForIdleStatus();
	configRWAddr(address );
	waitForIdleStatus();
	  
	WRITE32_REGISTER(REG_NAND_CMD,READ_2ND | (0x1 << 31));
	waitForIdleStatus();
	waitForNandReady();
	lockVal = intCpuLock();	
	WRITE32_REGISTER(REG_NAND_NUM, BIT_NAND_CTRL_READ | (uCount_spare - 1));
	readDataFromFIFO(pSpareBuff, uCount_spare);
	intCpuUnlock(lockVal);	
	return OK;
} 
int writeOnePage(
unsigned int        ulPageNum,
const unsigned char *pPageBuff
/*const D_BUFFER *pSpareBuff*/
/*BOOL eccEnable*/)
{

	unsigned short int i = 0;
	CardAddress address = 0;
	
	int lockVal = 0;
	
	unsigned int uCount_main = 2048;
	unsigned int uCount_spare = 64;
	
	unsigned short int iPageType = 1; /* 2048B */
	unsigned short int  eccSize = 0;
	
	address = (CardAddress)(ulPageNum * (2048 + 64));
	
//	/*disable BCH decode*/
//	WRITE_MASK_REGISTR(REG_NAND_BCH_CONFIG, BIT_BCH_DECODE_EN, 0x0);
//	/*disable BCH encode*/
//	WRITE_MASK_REGISTR(REG_NAND_BCH_CONFIG, BIT_BCH_ENCODE_EN, 0x0);
//	eccSize = 0;	

	WRITE_MASK_REGISTR(REG_NAND_CTRL2, BIT_TRANS_MODE | BIT_SG_EFFECTIVE /*| BIT_PAGE_TYPE*/, CPU_TRANS_MODE | (0x0 << 1) /*| (0x3 << 4)*/);
	WRITE32_REGISTER(REG_NAND_CMD, PROGRAM_1ST);
	waitForIdleStatus();

	configRWAddr(address);
	waitForIdleStatus();

	lockVal = intCpuLock();	//modify by ww 20200723 add 
	
	WRITE32_REGISTER(REG_NAND_NUM, BIT_NAND_CTRL_WRITE | (uCount_main - 1) | (eccSize << 16));

#if 1
	for (i = 0; i < uCount_main / 4; i++)
	{
		WRITE32_REGISTER(REG_NAND_DATA, *(((unsigned int *)pPageBuff) + i));
	}
#else	
	
	for (i = 0; i < uCount_main; i++)
	{
		WRITE8_REGISTER(REG_NAND_DATA, *(((unsigned char *)pPageBuff) + i));
	}
#endif	
	waitForIdleStatus();
	intCpuUnlock(lockVal);	//modify by ww 20200723 add 
	
	WRITE32_REGISTER(REG_NAND_CMD,PROGRAM_2ND/*| (0x1 << 31)*/);
	waitForIdleStatus();/*ldf 20230417*/
	waitForNandReady();
	if(readNandStatus())
	{
		printf("readNandStatus error.\n");
		return ERROR;
	}	
	
	return OK;
}

int writeOneSpare(
unsigned int        ulPage,
const char  *pSpareBuff)
{

	unsigned short int i = 0;
	CardAddress address = 0;
	int lockVal = 0;
	unsigned int uCount_spare = 64;
	
	unsigned short int iPageType = 1; /* 2048B */
	
//	/*disable BCH decode*/
//	WRITE_MASK_REGISTR(REG_NAND_BCH_CONFIG, BIT_BCH_DECODE_EN, 0x0);
//	/*disable BCH encode*/
//	WRITE_MASK_REGISTR(REG_NAND_BCH_CONFIG, BIT_BCH_ENCODE_EN, 0x0);

	address = (CardAddress)(ulPage * (2048 + 64) + 2048);
	WRITE_MASK_REGISTR(REG_NAND_CTRL2, BIT_TRANS_MODE | BIT_SG_EFFECTIVE /*| BIT_PAGE_TYPE*/, CPU_TRANS_MODE | (0x0 << 1) /*| (0x3 << 4)*/);
	WRITE32_REGISTER(REG_NAND_CMD, PROGRAM_1ST);
	waitForIdleStatus();

	configRWAddr(address);
	waitForIdleStatus();
	lockVal = intCpuLock();	
	WRITE32_REGISTER(REG_NAND_NUM, BIT_NAND_CTRL_WRITE | (uCount_spare - 1));

#if 1
	for (i = 0; i < uCount_spare / 4; i++)
	{
		WRITE32_REGISTER(REG_NAND_DATA, *(((unsigned int *)pSpareBuff) + i));
	}
#else
	for (i = 0; i < uCount_spare; i++)
	{
		/*timeDelay(200);*/
		WRITE8_REGISTER(REG_NAND_DATA, *(((unsigned char *)pSpareBuff) + i));
//		timeDelay(10);
//		timeDelay(200);//ww mod 20180909 add
	}
#endif
	
	waitForIdleStatus();
	intCpuUnlock(lockVal);	
	WRITE32_REGISTER(REG_NAND_CMD,PROGRAM_2ND);
	waitForIdleStatus();
	waitForNandReady();
	if(readNandStatus())
	{
		printf("readNandStatus error.\n");
		return ERROR;
	}
	
	return OK;
}
int eraseOneBlock(unsigned int ulBlock)
{
	 WRITE32_REGISTER(REG_NAND_CMD, ERASE_1ST);
	 waitForIdleStatus();
	 configEraseAddr(ulBlock);
	 waitForIdleStatus();
	 /* clear interrupts ?*/
	 WRITE32_REGISTER(REG_NAND_INT_CLEAR,0xFF);
	 waitForIdleStatus();
	 	
	 WRITE32_REGISTER(REG_NAND_CMD, ERASE_2ND);
	 waitForIdleStatus();
	 waitForNandReady();
	 if(readNandStatus())
	 {
		 return ERROR;
	 }
	 
	 return OK;
}   


#ifdef RELFS_NAND

void usrFfxConfig()
{
	if(sysCpuGetID() != 0)
	{
		taskDelay(sysClkRateGet() * 9);
		return;
	}
	
	switchToNand();

	usrRelFsInit (0,50); 
	RelRegInit (); /* Reliance Nitro transactional file system device driver */
	relFsPartLibInit ();                /* Provides disk partitioning functionality for Reliance Nitro volumes */
	relFsFmtLibInit (); 
	RelFmtInit ();  /* Provides high level formatting of Reliance Nitro volumes */
	relFsChkLibInit ();                 /* Provides consistency checking for Reliance Nitro volumes */
	relFsPortTestInit(); 
	RelDclTestInit(); 
	RelDclTestFsioInit(); 
	relFsShellInit(); /* Provides Reliance Nitro file system unit tests */
	relFsToolLibInit ();
	RelDclShellInit(); /* Provides miscellaneous developer tool functionality */
	    
	usrFlashFXInit62(0);
	usrFlashFXDeviceInit62("/ffx0",0,0,0,0x200000,1,2,0);
}
#endif



/* 任何地址均可以查看是否该扇区有效 */
int nandIsBadBlock(u32 /*uAddr*/ block_num)
{		
	u8 badBlk[64] = {0x15};
	u32 addr;
	
	/* 取第1页 */
	addr = (block_num * 64);
	readOneSpare(addr, badBlk);

	if (badBlk[0] != 0xff)
	{
		return (TRUE);
	}
		

#if 1  
	/* 取第2页 */
	addr = (block_num * 64) + 1;
	readOneSpare(addr, badBlk);
	
	if (badBlk[0] != 0xff)
	{
		return (TRUE);
	}
#endif
	
	return (FALSE);
}

#if 0
/*设置错误标志位,设置该块为废块*/
void nandBadBlkMark(u32 /*uAddr*/block_num)
{
	u8 bBadBlk = 0x66;
	u32 uAddr;
	uAddr = block_num * 131072;
	uAddr = (uAddr / 2048);
	nandMTDWrite(uAddr + 0, &bBadBlk, 1, EXTRA);			/*第一页*/
	nandMTDWrite(uAddr + 0x800 + 0, &bBadBlk, 1, EXTRA);		/*第二页*/
}
#endif


int flash_pageio_erase(u32 block_num)
{
//	printk("<DEBUG> [%s:__%d__]:: block_num=0x%x\n", __FUNCTION__,__LINE__,block_num);
	
	eraseOneBlock(block_num);
	return 0;
}

int flash_pageio_init(void)
{
//	printk("<DEBUG> [%s:__%d__]\n", __FUNCTION__,__LINE__);
	pthread_mutex_init2(&muteSem,PTHREAD_MUTEX_RECURSIVE,
			RE_MUTEX_PRIO_NONE |RE_MUTEX_WAIT_FIFO ,PTHREAD_MUTEX_CEILING);
	
	switchToNand();
	sleep(3);
	return 0;
}
unsigned int flash_pageio_w_cnt = 0;
unsigned int flash_pageio_r_cnt = 0;
unsigned long debugprint = 0;
int flash_pageio_write(u32 ulStartPage, void * pPageBuff, void * pSpareBuff)
{
//	printk("<DEBUG> [%s:__%d__]:: ulStartPage=0x%x\n", __FUNCTION__,__LINE__,ulStartPage);
	
	flash_pageio_w_cnt++;

	if(pPageBuff)
	{
		if(debugprint != 0)
		printf("flash_pageio_write MAIN, pPage.ulStartPage=%d.\n",ulStartPage);
		writeOnePage(ulStartPage, pPageBuff);
	}
	
	if(pSpareBuff)
	{
		if(debugprint != 0)
		printf("flash_pageio_write SPARE, pPage.ulStartPage=%d.\n",ulStartPage);
		writeOneSpare(ulStartPage, pSpareBuff);
	}
	
	return 0;
}

/*----------add by gongchao 2022.07.15 ----------- backup flash sector*/
//flag 0 bak ,1 resume  请和flashfx_config的配置保持一致,针对flash进行修改，请充分测试！！！！！
#define HRFS_FLASH_SIZE   512
#define HRFS_FLASH_LOW_RESV   32
#define HRFS_FLASH_HIGH_RESV   0  
#define HRFS_PAGE_SIZE   2048
#define HRFS_SPARE_SIZE   64
#define HRFS_PAGES_PER_BLOCK   64  


#define HRFS_BAK_FLAG_BLOCK_OFFSET   -3     //备份成功标志区域，用于判断是否有恢复点
#define HRFS_BAK_START_BLOCK_OFFSET   -2    //除去保留块以后的基于开始位置的偏移,低端必须有保留
#define HRFS_BAK_END_BLOCK_OFFSET   -1       //除去保留块以后的基于开始位置的偏移 ,低端必须有保留
#define HRFS_FLASH_ENDQMETASTART_OFFSET      14    //14MB  总是尾部减去高端保留减去14
#define HRFS_START_PAGES_NUM   5   //元数据格式头部记录页数 dont modify 
#define HRFS_END_PAGES_NUM   8     //元数据格式尾部记录页数 dont modify
#define BACKUP_IDENTIFY "Reworks Identify"
unsigned char data[6553500] ={0};   //记住计算，不要溢出
unsigned char dapare[81920] ={0};  //记住计算，不要溢出

void hrfs_flash_bakup()//block
{
	u32 orihead,oriend;//page
	u32 extrahead,extraend;//page
	u32 extraflag;//page

	orihead = HRFS_FLASH_LOW_RESV*1024*1024/HRFS_PAGE_SIZE; //page
	oriend = (HRFS_FLASH_SIZE-HRFS_FLASH_HIGH_RESV-HRFS_FLASH_ENDQMETASTART_OFFSET)*1024*1024/HRFS_PAGE_SIZE;
	extrahead = HRFS_FLASH_LOW_RESV*1024*1024/HRFS_PAGE_SIZE+((HRFS_BAK_START_BLOCK_OFFSET)*HRFS_PAGE_SIZE);
	extraend = HRFS_FLASH_LOW_RESV*1024*1024/HRFS_PAGE_SIZE+((HRFS_BAK_END_BLOCK_OFFSET)*HRFS_PAGE_SIZE);
	extraflag = HRFS_FLASH_LOW_RESV*1024*1024/HRFS_PAGE_SIZE+((HRFS_BAK_FLAG_BLOCK_OFFSET)*HRFS_PAGE_SIZE);
	int i=0;
	int j=0;
 
	memset(data,0,sizeof(data));
	memset(dapare,0,sizeof(dapare));
	printk("1\n");
	i=0;
	j=0;
	for(i=orihead;i<orihead+HRFS_PAGES_PER_BLOCK;i++,j++)
	{
		flash_pageio_read(i, data+j*HRFS_PAGE_SIZE, dapare+j*HRFS_SPARE_SIZE);
	}
	printk("2\n");
	flash_pageio_erase(extrahead/HRFS_PAGES_PER_BLOCK);
	
	i=0;
	j=0;
	for(i=extrahead;i<extrahead+HRFS_PAGES_PER_BLOCK;i++,j++)
	{
		flash_pageio_write(i, data+j*HRFS_PAGE_SIZE, dapare+j*HRFS_SPARE_SIZE);
	}
	
	
	memset(data,0,sizeof(data));
	memset(dapare,0,sizeof(dapare));
	printk("1\n");
	i=0;
	j=0;
	for(i=oriend;i<oriend+HRFS_PAGES_PER_BLOCK;i++,j++)
	{
		flash_pageio_read(i, data+j*HRFS_PAGE_SIZE, dapare+j*HRFS_SPARE_SIZE);
	}
	printk("2\n");
	flash_pageio_erase(extraend/HRFS_PAGES_PER_BLOCK);
	
	i=0;
	j=0;
	for(i=extraend;i<extraend+HRFS_PAGES_PER_BLOCK;i++,j++)
	{
		flash_pageio_write(i, data+j*HRFS_PAGE_SIZE, dapare+j*HRFS_SPARE_SIZE);
	}
	printk("write flag\n");
	memset(data,0,sizeof(data));
	strcpy(data,BACKUP_IDENTIFY);
	flash_pageio_erase(extraflag/HRFS_PAGES_PER_BLOCK);
	flash_pageio_write(extraflag,data ,NULL);
	
	printk("bak end\n");
	
	return;
}

void hrfs_flash_resume()//block
{
	u32 orihead,oriend;//page
	u32 extrahead,extraend;//page
	u32 extraflag;//page
	char flagdata[HRFS_PAGE_SIZE+1]={0};
	orihead = HRFS_FLASH_LOW_RESV*1024*1024/HRFS_PAGE_SIZE; //page
	oriend = (HRFS_FLASH_SIZE-HRFS_FLASH_HIGH_RESV-HRFS_FLASH_ENDQMETASTART_OFFSET)*1024*1024/HRFS_PAGE_SIZE;
	extrahead = HRFS_FLASH_LOW_RESV*1024*1024/HRFS_PAGE_SIZE+((HRFS_BAK_START_BLOCK_OFFSET)*HRFS_PAGE_SIZE);
	extraend = HRFS_FLASH_LOW_RESV*1024*1024/HRFS_PAGE_SIZE+((HRFS_BAK_END_BLOCK_OFFSET)*HRFS_PAGE_SIZE);
	extraflag = HRFS_FLASH_LOW_RESV*1024*1024/HRFS_PAGE_SIZE+((HRFS_BAK_FLAG_BLOCK_OFFSET)*HRFS_PAGE_SIZE);
	int i=0;
	int j=0;
 
	/*flash_pageio_read(extrahead+4, flagdata,NULL);
	if(strncmp(flagdata+3,"ReWorks",7) ==0)*/
	printk("checking flag point\n");
	memset(data,0,sizeof(data));
	flash_pageio_read(extraflag, data,NULL);
	if(strncmp(data,BACKUP_IDENTIFY,strlen(BACKUP_IDENTIFY)) ==0)
	{
		printk("find fs flag\n");
	}
	else
	{
		printk("not find fs flag,please baackup first\n");
		return;
	}
	memset(data,0,sizeof(data));
	memset(dapare,0,sizeof(dapare));
	printk("1\n");
	i=0;
	j=0;
	for(i=orihead;i<orihead+HRFS_PAGES_PER_BLOCK;i++,j++)
	{
		flash_pageio_read(i, data+j*HRFS_PAGE_SIZE, dapare+j*HRFS_SPARE_SIZE);
	}
	
	i=0;
	j=0;
	for(i=extrahead;i<extrahead+HRFS_START_PAGES_NUM;i++,j++)
	{
		flash_pageio_read(i, data+j*HRFS_PAGE_SIZE, dapare+j*HRFS_SPARE_SIZE);
	}
	printk("2\n");
	flash_pageio_erase(orihead/HRFS_PAGES_PER_BLOCK);
	
	i=0;
	j=0;
	for(i=orihead;i<orihead+HRFS_PAGES_PER_BLOCK;i++,j++)
	{
		flash_pageio_write(i, data+j*HRFS_PAGE_SIZE, dapare+j*HRFS_SPARE_SIZE);
	}
	
	//end block
	memset(data,0,sizeof(data));
	memset(dapare,0,sizeof(dapare));
	printk("1\n");
	i=0;
	j=0;
	for(i=oriend;i<oriend+HRFS_PAGES_PER_BLOCK;i++,j++)
	{
		flash_pageio_read(i, data+j*HRFS_PAGE_SIZE, dapare+j*HRFS_SPARE_SIZE);
	}
	
	i=0;
	j=0;
	for(i=extraend;i<extraend+HRFS_END_PAGES_NUM;i++,j++)
	{
		flash_pageio_read(i, data+j*HRFS_PAGE_SIZE, dapare+j*HRFS_SPARE_SIZE);
	}
	printk("2\n");
	flash_pageio_erase(oriend/HRFS_PAGES_PER_BLOCK);
	
	i=0;
	j=0;
	for(i=oriend;i<oriend+HRFS_PAGES_PER_BLOCK;i++,j++)
	{
		flash_pageio_write(i, data+j*HRFS_PAGE_SIZE, dapare+j*HRFS_SPARE_SIZE);
	}
	
	
		
	printk("resume end\n");
	
	return;
}
 
/*----------end add by gongchao 2022.07.15 ----------- backup flash sector*/

int flash_pageio_read(u32 ulStartPage, void * pPageBuff, void * pSpareBuff)
{
//	printk("<DEBUG> [%s:__%d__]:: ulStartPage=0x%x\n", __FUNCTION__,__LINE__,ulStartPage);
	
	flash_pageio_r_cnt++;

	if(pPageBuff)
	{
		if(debugprint != 0)
		printf("flash_pageio_read MAIN, pPage and pSpare.ulStartPage = %d.\n",ulStartPage);
		readOnePage(ulStartPage, pPageBuff);
	}

	if(pSpareBuff)
	{
		if(debugprint != 0)
		printf("flash_pageio_read SPARE, pPage and pSpare.ulStartPage = %d.\n",ulStartPage);
		readOneSpare(ulStartPage,pSpareBuff);
	}
		
	return 0;
}

void erasenandall(void)
{
	int i = 0;
	for(i = 0; i < 4096; i++)
		flash_pageio_erase(i);
	
	printf("erase nand all ok!\n");
	return;
}

#if 0
void update_reworks()
{	
	int fd = 0;
	int fd1 = 0;
	int len = 0;
	int read_len = 0;
	struct stat  st;
	int i = 0;
	int j = 0;
	int k = 0;
	int k0 = 0;
	int m = 0;
	int block_num = 0;
	int block_num1 = 0;
	unsigned char *pVal= NULL;
	unsigned char *pVal1= NULL;
	unsigned char *pVal2= NULL;	
	struct stat statbuf;
	pVal2 = (unsigned char *)malloc(2048);
	memset(pVal2, 0x55, 2048);

	const char *strcom = "HRworks";
#if 0	
	fd = open("/ffx0/reworks.elf", 0x202);
	
	fd1 = open("/ffx0/reworks_bk.elf", 0x202);
	
	fstat(fd, &st);
	len = st.st_size;
#else
	
	FILE *ffd = fopen("/ffx0/reworks.bin","rb");
	FILE *ffd1 = fopen("/ffx0/reworks_bk.bin","w+b");
	stat("/ffx0/reworks.bin", &statbuf);
	len = statbuf.st_size;
	pVal = (unsigned char *)malloc(((len + 2048)/2048) * 2048);
	memset(pVal,0xff,((len + 2048)/2048) * 2048);
	pVal1 = (unsigned char *)malloc(((len + 2048)/2048) * 2048);
	memset(pVal1,0xff,((len + 2048)/2048) * 2048);
	fread(pVal, sizeof(unsigned char), len, ffd);
#endif
	len += sizeof("HRworks");//增加个比较字符 HRworks

	printf("%s, %d, reworks.bin and comparestr len = 0x%x\n", __FUNCTION__, __LINE__, len);
	
	read_len = len;

	memcpy((pVal+len-sizeof("HRworks")), strcom, sizeof("HRworks"));
			
	printf("%s, %d, reworks.bin and comparestr read_len = 0x%x\n", __FUNCTION__, __LINE__, read_len);
	
	j = (read_len / 2048);
	
	k = read_len % 2048;
	
	if(k > 0)
		j++;
			
	
	j++;//多写一个page 的数据，作为读取的结束的判断
	
	printf("Tatol block is %d\n", (j/64));
	
	block_num = (read_len / (128 * 1024));
	
	block_num1 = (read_len % (128 * 1024));
	
	if(block_num1 > 0)
		block_num++;
	
	/* 目前前端保留32M，全部擦除
	 * 10M = 80个block
	 * 14M = 112个block*/
#define TATOL_BLOCK_NUM 256
	for(i = 0; i < TATOL_BLOCK_NUM; i++)
	{
		flash_pageio_erase(i);
			
	}		
	printf("Start...\n");
	
	for(i = 0; i < j; i++)
	{
CHECK_NEXT:
		if(nandIsBadBlock(i/64))
			{
			
				printf("Bad page is %d\n", (i));			
				printf("Bad block is %d\n", (i/64));
				i += 64;
				j += 64;
				goto CHECK_NEXT;
				
			}
		
		//最后一次写上最后一个page的0x55 比较数据即可退出
		if(i == (j -1)){
			printf("%d page is compare page!\n",i);
			flash_pageio_write(i, pVal2, NULL);
			break;
		}
			
		
		
		
//		if(i % 64 == 0)
			printf("*");
		flash_pageio_write(i, (pVal + m * 2048), NULL);
		
		flash_pageio_read(i, (pVal1 + m * 2048), NULL);
		
		for(k0 = 0; k0 < 2048; k0++)
		{
			if(*(pVal + m * 2048 + k0) != *(pVal1 + m * 2048 + k0)){
				printf("update_reworks error......\n");
			}
				
		}		
		m++;
				

	}
	
			
	

	printf("Tatol block is %d\n", (j/64));
		
	
//	write(fd1, pVal1, len);
	fwrite(pVal1,sizeof(unsigned char), len-sizeof("HRworks"), ffd1);
	fflush(ffd1);
	
	fclose(ffd);
	fclose(ffd1);
	printf("\n");	
	printf("Update ReWorks Complete!\n");
//	free(pVal1);
//	free(pVal2);
//	free(pVal);
//	pVal = NULL;
//	pVal1 = NULL;
//	pVal2 = NULL;
	return;
	
}
#else


/*
 * 0:reworks.bin格式
 * 1:reworks.elf格式
 */
void update_reworks(int binorelf)
{	
	int fd = 0;
	int fd1 = 0;
	FILE *ffd;
	FILE *ffd1;
	int len = 0;
	int read_len = 0;
	struct stat  st;
	int i = 0;
	int j = 0;
	int k = 0;
	int k0 = 0;
	int m = 0;
	int block_num = 0;
	int block_num1 = 0;
	unsigned char *pVal= NULL;
	unsigned char *pVal1= NULL;
	unsigned char *pVal2= NULL;	
	struct stat statbuf;
	pVal2 = (unsigned char *)malloc(2048);
	memset(pVal2, 0x55, 2048);
	const char *strcom = "HRworks";
	
	if(!binorelf){
		
		ffd = fopen("/ffx0/reworks.bin","rb");
		ffd1 = fopen("/ffx0/reworks_bk.bin","w+b");
		stat("/ffx0/reworks.bin", &statbuf);
		len = statbuf.st_size;
	}
	else{
		fd = open("/ffx0/reworks.elf", 0x202);
		
		fd1 = open("/ffx0/reworks_bk.elf", 0x202);
		
		fstat(fd, &st);
		len = st.st_size;
	}
	
	pVal = (unsigned char *)malloc(((len + 2048)/2048) * 2048);
	memset(pVal,0xff,((len + 2048)/2048) * 2048);
	pVal1 = (unsigned char *)malloc(((len + 2048)/2048) * 2048);
	memset(pVal1,0xff,((len + 2048)/2048) * 2048);
	
	
	if(!binorelf){
		fread(pVal, sizeof(unsigned char), len, ffd);
	}
	else
	{
		read(fd, pVal, len);
	}

	len += sizeof("HRworks");//增加个比较字符 HRworks

	if(!binorelf){
		printf("%s, %d, reworks.bin and comparestr len = 0x%x\n", __FUNCTION__, __LINE__, len);
	}
	else{
		printf("%s, %d, reworks.elf and comparestr len = 0x%x\n", __FUNCTION__, __LINE__, len);
	}
	
	read_len = len;

	memcpy((pVal+len-sizeof("HRworks")), strcom, sizeof("HRworks"));
			
//	printf("%s, %d, reworks.bin and comparestr read_len = 0x%x\n", __FUNCTION__, __LINE__, read_len);
	
	j = (read_len / 2048);
	
	k = read_len % 2048;
	
	if(k > 0)
		j++;
			
	
	j++;//多写一个page 的数据，作为读取的结束的判断
	
	printf("Tatol block is %d\n", (j/64));
	
	block_num = (read_len / (128 * 1024));
	
	block_num1 = (read_len % (128 * 1024));
	
	if(block_num1 > 0)
		block_num++;
	
	/* 目前前端保留32M，全部擦除
	 * 10M = 80个block
	 * 14M = 112个block*/
#define TATOL_BLOCK_NUM 512
	for(i = 0; i < TATOL_BLOCK_NUM; i++)
	{
		flash_pageio_erase(i);
			
	}		
	printf("Start...\n");
	
	for(i = 0; i < j; i++)
	{
CHECK_NEXT:
		if(nandIsBadBlock(i/64))
			{
			
				printf("Bad page is %d\n", (i));			
				printf("Bad block is %d\n", (i/64));
				i += 64;
				j += 64;
				goto CHECK_NEXT;
				
			}
		
		//最后一次写上最后一个page的0x55 比较数据即可退出
		if(i == (j -1)){
			printf("%d page is compare page!\n",i);
			flash_pageio_write(i, pVal2, NULL);
			break;
		}
		
//		if(i % 64 == 0)
			printf("*");
		flash_pageio_write(i, (pVal + m * 2048), NULL);
		
		flash_pageio_read(i, (pVal1 + m * 2048), NULL);
		
		for(k0 = 0; k0 < 2048; k0++)
		{
			if(*(pVal + m * 2048 + k0) != *(pVal1 + m * 2048 + k0)){
				printf("update_reworks error......\n");
			}
				
		}		
		m++;
	}

	printf("Tatol block is %d\n", (j/64));
		
	
	if(!binorelf){
//		fwrite(pVal1,sizeof(unsigned char), len-sizeof("HRworks"), ffd1);
		fflush(ffd1);	
		fclose(ffd);
		fclose(ffd1);
	}else
	{
//		write(fd1, pVal1, len-sizeof("HRworks"));
		close(fd);
		close(fd1);
	}
	
	printf("\n");	
	printf("Update ReWorks Complete!\n");
	free(pVal1);
	free(pVal2);
	free(pVal);
	pVal = NULL;
	pVal1 = NULL;
	pVal2 = NULL;
	return;
}



static void DumpUint8(u8 *pAddr, u32 length)
{
    int index = 0;

    printf("pAddr: 0x%08lX, length: %lu = 0x%08lX", (u32)pAddr, length, length);
    for (index = 0; index < length; index++)
    {
        if (index % 0x10 == 0)
        {
            printf("\n0x%08lX: ", index);
        }
        printf("0x%02X ", *(u8 *)(pAddr + index));
    }

    printf("\n");
}

void FlashDumpPage(u32 pageAddr)
{
	unsigned char *pMain = NULL;
	unsigned int page_main_size = MAIN_SIZE;
	unsigned char *pSpare = NULL;
	unsigned int page_spare_size = SPARE_SIZE;
	
	pMain = (unsigned char *)malloc(page_main_size);
	memset(pMain, 0, page_main_size);
	
	pSpare = (unsigned char *)malloc(page_spare_size);
	memset(pSpare, 0, page_spare_size);
	
	flash_pageio_read(pageAddr, (void*)pMain, (void*)pSpare);
	
	printf("\n\n============================[Page:%u] Main Data=================================\n",pageAddr);
	DumpUint8(pMain, page_main_size);
	printf("\n\n============================[Page:%u] Spare Data=================================\n",pageAddr);
	DumpUint8(pSpare, page_spare_size);
	
	free(pMain);
	free(pSpare);
	return;
}

/**********************************************************************
 * @brief: test_spi_nand_flash_rw
 * @describe: 裸读写测试用例
 * @parameter: 	blockoffset 要测试的起始块号,从0开始
 * 				blocks 要连续测试的块数量
 * @return: ok(0), error(-1)
 * @author: ldf
 * @date: 20221115
 **********************************************************************/
#define TEST_ALL_PAGE 1 // 1: 每个块测全部页, 0: 每个块只测首页
#define BUF_LEN	(MAIN_SIZE + SPARE_SIZE)
int test_spi_nand_flash_rw(unsigned int blockoffset, unsigned int blocks)
{
	unsigned char *buffer1;
	unsigned char *buffer2;
	unsigned char *buffer3;
	unsigned char *buffer5;
	unsigned char *buffer6;
	unsigned int pagenum = 0;
	unsigned int blocknum = 0;
	int i,j;
	unsigned char w_data = 0x01;
	u32 page_main_size = MAIN_SIZE;
	u32 page_spare_size = SPARE_SIZE;
	u32 pages_per_block = PAGES_PER_BLK;
	
	buffer1 = (unsigned char *)malloc(BUF_LEN);
	buffer2 = (unsigned char *)malloc(BUF_LEN);
	buffer3 = (unsigned char *)malloc(BUF_LEN);
	buffer5 = (unsigned char *)malloc(BUF_LEN);
	buffer6 = (unsigned char *)malloc(BUF_LEN);
	
	for(i = 0;i < BUF_LEN; i++)
	{
		if(0xFF == w_data)
			w_data = 0x01;
		buffer1[i] = w_data++;
	}
		
	memset(buffer2, 0, BUF_LEN);
	memset(buffer3, 0, BUF_LEN);
	memset(buffer5, 0, BUF_LEN);
	memset(buffer6, 0, BUF_LEN);
	  
	flash_pageio_init();
	 
	for(blocknum = blockoffset; blocknum < (blockoffset+blocks); blocknum++)
	{
		printk("\n ***************** flash_pageio_erase() blocknum = 0x%x ***************** \n", blocknum);
		flash_pageio_erase(blocknum);
	
		for(pagenum = blocknum * pages_per_block; 
#if TEST_ALL_PAGE //每个块测全部页
			pagenum < (blocknum+1) * pages_per_block;
#else //每个块只测首页
			pagenum < blocknum * pages_per_block + 1;
#endif
			pagenum++ )
		{
			memset(buffer2, 0, BUF_LEN);//main 擦除后的数据
			memset(buffer3, 0, BUF_LEN);//main 先写buffer1,后读的数据
			memset(buffer5, 0, BUF_LEN);//spare 擦除后的数据
			memset(buffer6, 0, BUF_LEN);//spare 先写buffer1,后读的数据
			
			flash_pageio_read(pagenum, (void*)buffer2, (void*)buffer5); //擦除后的数据
			flash_pageio_write(pagenum, (void*)buffer1, (void*)(buffer1+page_main_size));
			flash_pageio_read(pagenum, (void*)buffer3, (void*)buffer6); //先写后读的数据
	
#if 1			/*Main start*/
			printk("\n ***************** MAIN, pagenum = 0x%x, blocknum = 0x%x ***************** \n", pagenum, blocknum);
			printk("After Erase ----- 1 :\n");
//			DumpUint8(buffer2, page_main_size);//擦除后的数据
			for(i = 0; i < page_main_size; i++)
			{
				if(0xff != buffer2[i])
				{
					printk("<***ERROR***> byte[0x%x]: read_data(0x%x) != 0xff\n", \
							i, buffer2[i]);
					goto ERR1;
				}
			}
			
			printk("After Write ----- 2 :\n");
//			DumpUint8(buffer3, page_main_size);//先写后读的数据
			
			for(i = 0; i < page_main_size; i++)
			{
				if(buffer3[i] != buffer1[i])
				{
					printk("<***ERROR***> byte[0x%x]: write_data = 0x%x, read_data = 0x%x\n", i,buffer1[i],i,buffer3[i]);
					goto ERR1;
				}
			}
			printk("<OK> MAIN, pagenum = 0x%x\n", pagenum);	
#endif			/*Main end*/
			
#if 1			/*Spare start*/
			printk("\n ***************** SPARE, pagenum = 0x%x, blocknum = 0x%x ***************** \n", pagenum, blocknum);
			printk("After Erase ----- 1 :\n");
//			DumpUint8(buffer5, page_spare_size);//擦除后的数据
			for(i = 0; i < page_spare_size; i++)
			{
				if(0xff != buffer5[i])
				{
					printk("<***ERROR***> byte[0x%x]: read_data(0x%x) != 0xff\n", \
							i, buffer5[i]);
					goto ERR1;
				}
			}
			
			printk("After Write ----- 2 :\n");
//			DumpUint8(buffer6, page_spare_size);//先写后读的数据
			
			for(i = 0; i < page_spare_size; i++)
			{
				if(buffer6[i] != buffer1[page_main_size + i])
				{
					printk("<***ERROR***> byte[0x%x]: write_data = 0x%x, read_data = 0x%x\n", \
							i, buffer1[page_main_size+i], buffer6[i]);
					goto ERR1;
				}
			}
			printk("<OK> SPARE, pagenum = 0x%x\n", pagenum);
#endif			/*Spare end*/
		}
		printk("<**OK**> blocknum = 0x%x\n", blocknum);
	}
	
	printk("<****TEST OK****> Blocknum:0x%x ~ 0x%x, Total: 0x%x blocks\n", blockoffset, blockoffset+blocks-1, blocks);

	return 0;
	
ERR1:
	free(buffer1);
	free(buffer2);
	free(buffer3);
	free(buffer5);
	free(buffer6);
	return -1;
}

#endif

