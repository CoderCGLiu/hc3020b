#include <reworks/types.h>
#include "irq.h"
#include "tickLib.h"
#include <memory.h>
#include "ffx_config.h"
#include "hr3NandFlash.h"

#define INCLUDE_NANDFLASH

//typedef int (*_func_flash_pageio_init)();
//extern _func_flash_pageio_init ptr_func_flash_pageio_init;
//typedef int (*_func_flash_pageio_read)(u32 ulStartPage, u32 pPageBuff, u32 pSpareBuff);
//extern _func_flash_pageio_read ptr_func_flash_pageio_read;
//typedef int (*_func_flash_pageio_write)(u32 sector_num, u32 pPageBuff, u32 pSpareBuff);
//extern _func_flash_pageio_write ptr_func_flash_pageio_write;
//typedef int (*_func_flash_pageio_erase)(u32 block_num);
//extern _func_flash_pageio_erase ptr_func_flash_pageio_erase;

extern int flash_pageio_init(void);
extern int flash_pageio_read(u32 ulStartPage, u32 pPageBuff, u32 pSpareBuff);
extern int flash_pageio_write(u32 ulStartPage, u32 pPageBuff, u32 pSpareBuff);
extern int flash_pageio_erase(u32 block_num);

extern struct USERNANDCHIPCLASS *UserNandFlashAttribute;



void SetUserFlashAttribute_NAND(void)
{
	UserNandFlashAttribute = (USERNANDCHIPCLASS *)malloc(sizeof(*UserNandFlashAttribute));

	/* uPageSize：除去spare区之后的页大小，以字节为单位*/
	UserNandFlashAttribute->uPageSize = MAIN_SIZE;//2KB
	
	/* uSpareSize：spare区的大小，以字节为单位*/
	UserNandFlashAttribute->uSpareSize = SPARE_SIZE;//64B
	 
	/* uLinearPageAddrMSB：为获取页(page)索引而需要将Flash地址(Column Address)向右移的位数，比如页大小2KB，这里填11*/
	UserNandFlashAttribute->uLinearPageAddrMSB = 11; //页大小2KB 
	
	/* uChipPageIndexLSB：这里可能是代表级联需要偏移*/
	UserNandFlashAttribute->uChipPageIndexLSB = 0;/* how much to << chip page index before sending to chip */
	 
	/* uLinearBlockAddrLSB：为获取块(block)索引而需要将Flash地址(Row Address)向右移的位数，比如块大小128KB，这里填17*/
	UserNandFlashAttribute->uLinearBlockAddrLSB = 17;	//block大小128KB

	/* uLinearChipAddrMSB：为确定片选而需要将flash地址向右移的位数，比如flash总大小512MB，这里填29*/
	UserNandFlashAttribute->uLinearChipAddrMSB = 29;	//chip总共大小512MB
	 
	/* ulChipBlocks：每块flash芯片的块数(erase block).*/
	UserNandFlashAttribute->ulChipBlocks = BLOCK_COUNT;
	 
	/* ulBlockSize：块大小(erase block)，不包括spare区.*/
	UserNandFlashAttribute->ulBlockSize = BLOCK_SIZE;//128KB
	
	/* uEdcRequirement：纠错位数.用户根据flash芯片手册确定合适的纠错位数，不支持软件ECC的FlashFX版本这里填0*/
	UserNandFlashAttribute->uEdcRequirement = 4;
	 
	/* uEdcCapability：纠错能力.该字段表示flash芯片支持的最大纠错位数.不支持软件ECC的FlashFX版本这里填0
	 * NAND Flash中常用的纠错方式：Hanming,RS,BCH.
	 * 因为闪存中会有出错的可能，如果没有使用ECC模块，读出的数据和写入的数据会有不匹配的可能，也许一个文件中只有一两个bit不匹配，这也是
	 * 不能容忍的。相对来说SLC中出错概率比较低，所以使用一个纠错能力不强的Hanming码就可以了，在MLC中Hanming码就显得力部从心了，需要纠错
	 * 能力更强的RS或者BCH纠错方式了。RS是按多位的组合编码，而BCH按位编码。
	 * 例如在NAND Flash中某个字节应该是0x00，但是读出的却是0xf0,因此RS只认为出现了一个错误，而BCH则认为出现了4个错误。
	 * RS方式有着不错的突发随机错误和随机错误的能力。BCH擅长处理随机错误，因此在MLC中应用比较多的是BCH.纠错需要额外的存储空间，这个存储
	 * 空间从flash的spare区分配。
	 * 目前的FLASH库暂时不支持ECC模块，因此uEdcCapability应设置为0.
	 */
	UserNandFlashAttribute->uEdcCapability = 4;
	
	/* ulEraseCycleRating：Flash的最大擦除周期数.(用户可根据Flash芯片手册中的FEATURES章节可以找到该参数)*/
	UserNandFlashAttribute->ulEraseCycleRating = 100000;
	
	/*ulBBMReservedRating：用于坏块管理的块数.该字段的确定需要参考FLASH库支持的FLASH对应的ulBBMReservedRating字段.
	 * 可参考flashfx中的nandid.c中的型号来配置*/
	UserNandFlashAttribute->ulBBMReservedRating = 40;
	
	
	/* 以下8个数据成员表示的是某些芯片的特殊属性*/
		
	/* ResetBeforeProgram:是否支持STMicro Flash的写前重置操作.
	 * 如果支持STMicro Flash的写前重置操作，则设置ResetBeforeProgram = 1;否则设置ResetBeforeProgram = 0.
	 * (如果用户新添加的芯片是意法半导体公司的STMicro Flash，而且芯片是由两片512Mbit Flash组成，则设置ResetBeforeProgram = 1.)
	 */
	UserNandFlashAttribute->ResetBeforeProgram = 0;
	
	/* ReadConfirm:读确认命令.
	 * 如果支持读确认命令触发读操作,则设置ReadConfirm = 1;否则设置ReadConfirm = 0.
	 * (现在很多比较新的，比较大的flash芯片的读操作命令一般包括两个字节的命令，需要先后发送两个字节的命令以执行读操作，
	 * 发送的第二字节的命令称作读确认命令。)
	 */
	UserNandFlashAttribute->ReadConfirm = 0/*1*/;
	
	/* Samsung2kOps:是否支持三星Flash的扩展操作.
	 * 如果支持三星Flash的扩展操作，则设置Samsung2kOps = 1;否则设置Samsung2kOps = 0.
	 * (如果Flash支持类似三星Flash的Random Data Input、Random Data Output等操作,则应设置Samsung2kOps = 1.)
	 */
	UserNandFlashAttribute->Samsung2kOps = 0/*1*/;
	
	/* ORNANDPartialPageReads:是否支持快速读取小型数据块.
	 * 如果Flash支持快速读取小型数据块,则设置ORNANDPartialPageReads = 1;否则设置ORNANDPartialPageReads = 0.
	 * (传统的NAND Flash必须以页为单位读取数据，但是现在比较新的Spansion ORNAND支持局部页面读命令，
	 * 该命令支持以四分之一页面为增量的更快的数据访问速度.)
	 */
	UserNandFlashAttribute->ORNANDPartialPageReads = 0;
	
	/* fMicronDualPlaneOps:是否支持dual-plane操作.
	 * 如果支持dual-plane操作,则设置fMicronDualPlaneOps = 1;否则设置fMicronDualPlaneOps = 0.
	 * (现在部分比较新的Micron Flash支持dual-plane操作)
	 */
	UserNandFlashAttribute->fMicronDualPlaneOps = 0;
	
	/* BlockLockSupport:是否支持块锁机制.
	 * 如果支持块锁机制，则设置BlockLockSupport = 1;否则设置BlockLockSupport = 0.
	 * (现在部分比较新的STMicro flash芯片支持块锁机制)
	 */
	UserNandFlashAttribute->BlockLockSupport = 1;
	
	/* fProgramOnce:写操作的类型.
	 * 如果Flash芯片仅支持单页写操作,则设置fProgramOnce = 1;否则设置fProgramOnce = 0.
	 * (一般情况下,MLC NAND仅支持单页写操作;而其他一些flash还支持cache program)
	 */
	UserNandFlashAttribute->fProgramOnce = 0;
	
	/* fLastPageFBB:坏块标记是否定义在坏块的最后一页.
	 * 如果坏块标记定义在坏块的最后一页，则设置fLastPageFBB = 1;否则设置fLastPageFBB = 0.
	 */
	UserNandFlashAttribute->fLastPageFBB = 0;
	
	
	/* 如果用户的Flash是Flash库本身就支持的芯片，则以上的字段可全部设置成NULL；否则就要按照芯片手册以及Flash库给定的参考属性进行设置.
	 * 对于以下的所有字段，无论用户的Flash是否为Flash库支持的芯片，均需要根据芯片手册进行设置.
	 */
	
	/* Flash ID的长度.例如型号为K9F2G08U0M的NAND Flash,其ID长度为2.*/
	UserNandFlashAttribute->DevIDLength = 2;/*DeviceID的长度*/
	 
	/* DevID[6]:Flash ID数组,包括厂商ID和设备ID.例如DevID[0]设置为0xEC,0xEC是三星的厂商id,flashfx会走SamsungFindChip()进行设置参数,
	 * 若后边 UnsupportedChip 被设置为0，则flashfx会继续通过DevID[1]往后的设备id来寻找匹配的型号来设置参数，
	 * 若后边 UnsupportedChip 被设置为1，表示芯片是FlashFX库不支持的，则flashfx会从本函数获取型号参数，
	 * 从DevID[1]开始为设备id,数组的其他的元素设置为0xFF.
	 */
 
	/*add by ldf at 20221116*/
	UserNandFlashAttribute->DevID[0] = 0xEC;/*DevID[0]固定为0xEC，0xEC是三星的厂商id，flashfx会走SamsungFindChip()进行设置参数*/
	UserNandFlashAttribute->DevID[1] = 0xAC;/*实际设备id 1*/
	UserNandFlashAttribute->DevID[2] = 0xFF;/*实际设备id 2,没有就填0xFF*/
	UserNandFlashAttribute->DevID[3] = 0xFF;/*0xFF*/
	UserNandFlashAttribute->DevID[4] = 0xFF;/*0xFF*/
	UserNandFlashAttribute->DevID[5] = 0xFF;/*0xFF*/
	/*add end*/
	
	/* FbbType：出厂坏块类型(Factory Bad Block或者Initial Invalid Block).
	 * 如果坏块标记被定义在spare区的第一个字节(X8 device)或者第一个字(X16 device),则设置FbbType = 0.
	 * 如果坏块标记被定义在spare区的第六个字节(X8 device)或者第六个字(X16 device),则设置FbbType = 1.
	 * 如果坏块标记被定义在一个block内的任意字节处,则设置FbbType = 2.(目前这个设置仅出现在比较旧的Toshiba Flash中)(NTpageio模式不支持)
	 * 如果没有出厂坏块，且不需要进行坏块管理，则设置FbbType = 3.(目前这个设置仅出现在Spansion Flash中)(NTpageio模式不支持)
	 */	
	UserNandFlashAttribute->FbbType = 0;
	
	/* Chip_Interface_Width：Flash芯片数据接口宽度*/
	UserNandFlashAttribute->Chip_Interface_Width = 8;
	
	/* UnsupportedChip：如果用户的Flash是FlashFX库支持的芯片类型，则设置UnsupportedChip = 0;否则设置UnsupportedChip = 1.*/
	UserNandFlashAttribute->UnsupportedChip = 1; /*此处固定设置UnsupportedChip = 1，表示芯片是FlashFX库不支持的，则会从此函数获取参数*/
	 
	/* 测试字段*/
	/* DriverAutoTest: 测试程序开启字段.
	 * 如果需要进行测试，则将设置DriverAutoTest = 1；否则设置DriverAutoTest = 0.
	 */
	UserNandFlashAttribute->DriverAutoTest = 0;
	 
	/* DclTestParams：Flash协议单元测试.
	 * 如果需要进行测试，则将设置DclTestParams = 1；否则设置DclTestParams = 0.
	 */
	UserNandFlashAttribute->DclTestParams = 0;
	
	/* FmslTestParams: Flash驱动单元测试.
	 * 如果需要进行测试，则将设置DclTestParams = 1；否则设置DclTestParams = 0.
	 */
	UserNandFlashAttribute->FmslTestParams = 0;

//	printf("uPageSize = %d \n",UserNandFlashAttribute->uPageSize);
//	printf("uSpareSize = %d \n",UserNandFlashAttribute->uSpareSize);
//	printf("uLinearPageAddrMSB = %d \n",UserNandFlashAttribute->uLinearPageAddrMSB);
//	printf("uChipPageIndexLSB = %d \n",UserNandFlashAttribute->uChipPageIndexLSB);
//	printf("uLinearBlockAddrLSB = %d \n",UserNandFlashAttribute->uLinearBlockAddrLSB);
//	printf("uLinearChipAddrMSB = %d \n",UserNandFlashAttribute->uLinearChipAddrMSB);
//	printf("ulChipBlocks = %d \n",UserNandFlashAttribute->ulChipBlocks);
//	printf("ulBlockSize = %d \n",UserNandFlashAttribute->ulBlockSize);
//	printf("BlockLockSupport = %d \n",UserNandFlashAttribute->BlockLockSupport);
//	printf("DevIDLength = %d \n",UserNandFlashAttribute->DevIDLength);
//	printf("DevID[0] = 0x%x \n",UserNandFlashAttribute->DevID[0]);
//	printf("DevID[1] = 0x%x \n",UserNandFlashAttribute->DevID[1]);
//	printf("DevID[2] = 0x%x \n",UserNandFlashAttribute->DevID[2]);
//	printf(" DevID[3] = 0x%x \n",UserNandFlashAttribute->DevID[3]);
//	printf("DevID[4] = 0x%x \n",UserNandFlashAttribute->DevID[4]);
//	printf("DevID[5] = 0x%x \n",UserNandFlashAttribute->DevID[5]);
//	printf("UnsupportedChip = %d \n",UserNandFlashAttribute->UnsupportedChip);
}



void nandflash_config_init(void)
{
	FLASH_CHIP_INFO *flash;
	
#ifdef INCLUDE_NORFLASH	
	
	flash = (FLASH_CHIP_INFO *)malloc(sizeof(FLASH_CHIP_INFO));
	
	flash->fl_fim = FIM_ambx16_be; 		
	
	flash->fl_ntm = 0x0; 						
	
	flash->fl_base_addr = (void *)0xF8000000;  		
	
	flash->fl_reserved_lo_kb = 0 * 1024;  			
	
	flash->fl_reserved_hi_kb = 32 * 1024;
	
	flash->fl_dev_size_kb = 96 * 1024;		
	
	flash_add_dev(flash);	
#endif
	
#ifdef INCLUDE_NANDFLASH	
	
	flash = (FLASH_CHIP_INFO *)malloc(sizeof(FLASH_CHIP_INFO));
	
	flash->fl_fim = FIM_nand; 				
	
	flash->fl_ntm = NTM_pageio; 						
	 
	/*question ww 20141215*/
	flash->fl_base_addr = (void *) /*0xffffffffbf058000*/0;//0xfc000000;/*0xE0005000:cp2010 nand flash		

	flash->fl_reserved_lo_kb = 32*1024;  	 			
	
	flash->fl_reserved_hi_kb = 0*1024;	//130	
	
	flash->fl_dev_size_kb = (512-32)* 1024;
	
	flash->attrAndfuncSet = nandflash_config_init;//nandflash_config_init;
	
	ptr_func_flash_pageio_init = flash_pageio_init;
	ptr_func_flash_pageio_read = flash_pageio_read;
	ptr_func_flash_pageio_write = flash_pageio_write;
	ptr_func_flash_pageio_erase = flash_pageio_erase; 
	  
	SetUserFlashAttribute_NAND();
			
	flash_add_dev(flash);	
#endif
	
#ifdef INCLUDE_ONENANDFLASH	
	
	flash = (FLASH_CHIP_INFO *)malloc(sizeof(FLASH_CHIP_INFO));
	
	flash->fl_fim = FIM_nand; 				
	
	flash->fl_ntm = NTM_onenand; 						
	
	flash->fl_base_addr = 0xE0005000;	
	flash->fl_reserved_lo_kb = 0;  				
	
	flash->fl_reserved_hi_kb = 0;		
	
	flash->fl_dev_size_kb = 256 * 1024;		
		
	flash_add_dev(flash);	
#endif
	
#if 0
	flash = (FLASH_CHIP_INFO *)malloc(sizeof(FLASH_CHIP_INFO));
	
	flash->fl_fim = FIM_nand; 		
	
	flash->fl_ntm = NTM_cad; 						
	
	flash->fl_base_addr = (void *)0xFFFFFFFF;  		
	
	flash->fl_reserved_lo_kb = 32 * 1024;  			
	
	flash->fl_reserved_hi_kb = 32 * 1024;
	
	flash->fl_dev_size_kb = 448 * 1024;		
	
	flash_add_dev(flash);	
#endif
}



