#include "debug.h"
#include "uart_driver.h"


/*test case function*/


/*end*/
u8 uart_baudrate_test(void)
{
	//uart_printf("uart baudrate test...	\n");
	u8 err_cnt=0;

	unsigned char dat=0;
	uart_init_def uart_init_cfg = {
			.baudrate = UART_BAUD_115200,
			.word = UART_WORD_8BIT,
			.stop = UART_STOP_1BIT,
			.parity = UART_PARITY_NONE,
			.transmit_enable = UART_TRANSMIT_ENABLE,
			.receive_enable = UART_RECEIVE_ENABLE,
			.fifo_access = UART_FIFO_1BYTE_MODE,
			.irda_mode = UART_DEFAULT_MODE,
			.channel_mode = UART_NORMAL_CHANNEL,
			.clock_select = UART_SEL_APB_CLOCK,
			.clock_prescalar8 = UART_CLOCK_PRESCALAR_DIS
	};
	uart_enable_pics(MIAN_OBJ,UART_INT_FRAME_MASK);
	

	uart_init_cfg.baudrate = UART_BAUD_4800;
	uart_init(MIAN_OBJ, &uart_init_cfg);
	uart_init(TEST_OBJ, &uart_init_cfg);
	uart_poll_put_char(TEST_OBJ,0x55);
	if( (0x55 != uart_block_get_char(MIAN_OBJ)) || (0 != uart_get_picstate(MIAN_OBJ,UART_CISR_MASK))){
		err_cnt++;
		//uart_printf("4800 baud error!   \n");
	}
	uart_init_cfg.baudrate = UART_BAUD_9600;
	uart_init(MIAN_OBJ, &uart_init_cfg);
	uart_init(TEST_OBJ, &uart_init_cfg);
	uart_poll_put_char(TEST_OBJ,0x55);
	if( (0x55 != uart_block_get_char(MIAN_OBJ)) || (0 != uart_get_picstate(MIAN_OBJ,UART_CISR_MASK))){
		err_cnt++;
		//uart_printf("9600 baud error!   \n");
	}

	uart_init_cfg.baudrate = UART_BAUD_14400;
	uart_init(MIAN_OBJ, &uart_init_cfg);
	uart_init(TEST_OBJ, &uart_init_cfg);
	uart_poll_put_char(TEST_OBJ,0x55);
	if( (0x55 != uart_block_get_char(MIAN_OBJ)) || (0 != uart_get_picstate(MIAN_OBJ,UART_INT_FRAME_MASK))){
		err_cnt++;
		//uart_printf("4800 baud error!   \n");
	}
	uart_init_cfg.baudrate = UART_BAUD_28800;
	uart_init(MIAN_OBJ, &uart_init_cfg);
	uart_init(TEST_OBJ, &uart_init_cfg);
	uart_poll_put_char(TEST_OBJ,0x55);
	if( (0x55 != uart_block_get_char(MIAN_OBJ)) || (0 != uart_get_picstate(MIAN_OBJ,UART_INT_FRAME_MASK))){
		err_cnt++;
		//uart_printf("4800 baud error!   \n");
	}
	uart_init_cfg.baudrate = UART_BAUD_19200;
	uart_init(MIAN_OBJ, &uart_init_cfg);
	uart_init(TEST_OBJ, &uart_init_cfg);
	uart_poll_put_char(TEST_OBJ,0x55);
	if( (0x55 != uart_block_get_char(MIAN_OBJ)) || (0 != uart_get_picstate(MIAN_OBJ,UART_INT_FRAME_MASK))){
		err_cnt++;
		//uart_printf("4800 baud error!   \n");
	}
	uart_init_cfg.baudrate = UART_BAUD_38400;
	uart_init(MIAN_OBJ, &uart_init_cfg);
	uart_init(TEST_OBJ, &uart_init_cfg);
	uart_poll_put_char(TEST_OBJ,0x55);
	if( (0x55 != uart_block_get_char(MIAN_OBJ)) || (0 != uart_get_picstate(MIAN_OBJ,UART_INT_FRAME_MASK))){
		err_cnt++;
		//uart_printf("4800 baud error!   \n");
	}
	uart_init_cfg.baudrate = UART_BAUD_57600;
	uart_init(MIAN_OBJ, &uart_init_cfg);
	uart_init(TEST_OBJ, &uart_init_cfg);
	uart_poll_put_char(TEST_OBJ,0x55);
	if( (0x55 != uart_block_get_char(MIAN_OBJ)) || (0 != uart_get_picstate(MIAN_OBJ,UART_INT_FRAME_MASK))){
		err_cnt++;
		//uart_printf("4800 baud error!   \n");
	}
	uart_init_cfg.baudrate = UART_BAUD_115200;
	uart_init(MIAN_OBJ, &uart_init_cfg);
	uart_init(TEST_OBJ, &uart_init_cfg);
	uart_poll_put_char(TEST_OBJ,0x55);
	if( (0x55 != uart_block_get_char(MIAN_OBJ)) || (0 != uart_get_picstate(MIAN_OBJ,UART_INT_FRAME_MASK))){
		err_cnt++;
		//uart_printf("115200 baud error!   \n");
	}
	uart_init_cfg.baudrate = UART_BAUD_128000;
	uart_init(MIAN_OBJ, &uart_init_cfg);
	uart_init(TEST_OBJ, &uart_init_cfg);
	uart_poll_put_char(TEST_OBJ,0x55);
	if( (0x55 != uart_block_get_char(MIAN_OBJ)) || (0 != uart_get_picstate(MIAN_OBJ,UART_INT_FRAME_MASK))){
		err_cnt++;
		//uart_printf("128000 baud error!   \n");
	}
	uart_init_cfg.baudrate = UART_BAUD_230400;
	uart_init(MIAN_OBJ, &uart_init_cfg);
	uart_init(TEST_OBJ, &uart_init_cfg);
	uart_poll_put_char(TEST_OBJ,0x55);
	if( (0x55 != uart_block_get_char(MIAN_OBJ)) || (0 != uart_get_picstate(MIAN_OBJ,UART_INT_FRAME_MASK))){
		err_cnt++;
		//uart_printf("230400 baud error!   \n");
	}
	uart_init_cfg.baudrate = UART_BAUD_256000;
	uart_init(MIAN_OBJ, &uart_init_cfg);
	uart_init(TEST_OBJ, &uart_init_cfg);
	uart_poll_put_char(TEST_OBJ,0x55);
	if( (0x55 != uart_block_get_char(MIAN_OBJ)) || (0 != uart_get_picstate(MIAN_OBJ,UART_INT_FRAME_MASK))){
		err_cnt++;
		//uart_printf("256000 baud error!   \n");
	}
	uart_init_cfg.baudrate = UART_BAUD_460800;
	uart_init(MIAN_OBJ, &uart_init_cfg);
	uart_init(TEST_OBJ, &uart_init_cfg);
	uart_poll_put_char(TEST_OBJ,0x55);
	if( (0x55 != uart_block_get_char(MIAN_OBJ)) || (0 != uart_get_picstate(MIAN_OBJ,UART_INT_FRAME_MASK))){
		err_cnt++;
		//uart_printf("460800 baud error!   \n");
	}
	uart_init_cfg.baudrate = UART_BAUD_115200;
	//uart_init(MIAN_OBJ, &uart_init_cfg);
	uart_init(TEST_OBJ, &uart_init_cfg);
	uart_poll_put_char(TEST_OBJ,0x55);
	dat = uart_block_get_char(MIAN_OBJ);
	if( 1 != uart_get_picstate(MIAN_OBJ,UART_INT_FRAME_MASK)){
		err_cnt++;
		//uart_printf("460800_115200 baud error!   \n");
	}
	return err_cnt;
/*
	if(err_cnt>0)
		uart_printf("uart baudrate test failed!   \n");
	else
		uart_printf("uart baudrate test pass!   \n");
*/

}

