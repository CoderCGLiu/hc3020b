#include "debug.h"
#include "uart_driver.h"

/*test case function*/


/*end*/
u8 uart_data_test(void)
{
	//uart_printf("uart data test...	\n");
	u8 err_cnt =0;
	unsigned char dat=0;
	uart_init_def uart_init_cfg = {
			.baudrate = UART_BAUD_115200,
			.word = UART_WORD_6BIT,
			.stop = UART_STOP_1BIT,
			.parity = UART_PARITY_NONE,
			.transmit_enable = UART_TRANSMIT_ENABLE,
			.receive_enable = UART_RECEIVE_ENABLE,
			.fifo_access = UART_FIFO_1BYTE_MODE,
			.irda_mode = UART_DEFAULT_MODE,
			.channel_mode = UART_NORMAL_CHANNEL,
			.clock_select = UART_SEL_APB_CLOCK,
			.clock_prescalar8 = UART_CLOCK_PRESCALAR_DIS
	};
	uart_enable_pics(MIAN_OBJ,UART_INT_FRAME_MASK);

	uart_init(MIAN_OBJ, &uart_init_cfg);
	uart_init(TEST_OBJ, &uart_init_cfg);
	uart_poll_put_char(TEST_OBJ,0xEA);
	if( (0x2A != uart_block_get_char(MIAN_OBJ)) || (0 != uart_get_picstate(MIAN_OBJ,UART_INT_FRAME_MASK))){
		err_cnt++;
		//uart_printf("6bit data error!   \n");
	}
	uart_init_cfg.word = UART_WORD_7BIT;	
	uart_init(MIAN_OBJ, &uart_init_cfg);
	uart_init(TEST_OBJ, &uart_init_cfg);
	uart_poll_put_char(TEST_OBJ,0xD5);
	if( (0x55 != uart_block_get_char(MIAN_OBJ)) || (0 != uart_get_picstate(MIAN_OBJ,UART_INT_FRAME_MASK))){
		err_cnt++;
		//uart_printf("7bit data error!   \n");
	}
	uart_init_cfg.word = UART_WORD_8BIT;
	uart_init(MIAN_OBJ, &uart_init_cfg);
	uart_init(TEST_OBJ, &uart_init_cfg);
	uart_poll_put_char(TEST_OBJ,0xAA);
	if( (0xAA != uart_block_get_char(MIAN_OBJ)) || (0 != uart_get_picstate(MIAN_OBJ,UART_INT_FRAME_MASK))){
		err_cnt++;
		//uart_printf("8bit data error!   \n");
	}
	uart_init_cfg.word = UART_WORD_6BIT;
	//uart_init(MIAN_OBJ, &uart_init_cfg);
	uart_init(TEST_OBJ, &uart_init_cfg);
	uart_poll_put_char(TEST_OBJ,0x00);
	dat = uart_block_get_char(MIAN_OBJ);
	if(1 != uart_get_picstate(MIAN_OBJ,UART_INT_FRAME_MASK)){
		err_cnt++;
		//uart_printf("8bit_6bit data error!   \n");
	}

	return err_cnt;
/*
	if(err_cnt>0)
		uart_printf("uart data test failed!   \n");
	else
		uart_printf("uart data test pass!   \n");
*/

}

