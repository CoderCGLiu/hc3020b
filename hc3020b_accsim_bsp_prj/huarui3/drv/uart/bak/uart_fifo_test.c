#include "debug.h"
#include "uart_driver.cx"

/*test case function*/


/*end*/
void uart_fifo_test(void)
{
	uart_printf("uart fifo test...	\n");

	u8 err_cnt =0;
	uart_init_def uart_init_cfg = {
			.baudrate = UART_BAUD_115200,
			.word = UART_WORD_8BIT,
			.stop = UART_STOP_1BIT,
			.parity = UART_PARITY_NONE,
			.transmit_enable = UART_TRANSMIT_ENABLE,
			.receive_enable = UART_RECEIVE_ENABLE,
			.fifo_access = UART_FIFO_1BYTE_MODE,
			.irda_mode = UART_DEFAULT_MODE,
			.channel_mode = UART_NORMAL_CHANNEL,
			.clock_select = UART_SEL_APB_CLOCK,
			.clock_prescalar8 = UART_CLOCK_PRESCALAR_DIS
	};
	uart_enable_pics(MIAN_OBJ,UART_INT_PARE_MASK);
	
	uart_init_cfg.fifo_access = UART_FIFO_1BYTE_MODE;	
	uart_init(MIAN_OBJ, &uart_init_cfg);
	uart_init(TEST_OBJ, &uart_init_cfg);


	uart_poll_put_char(TEST_OBJ,0x55);
	if( 0x55 != uart_block_get_char(MIAN_OBJ)){
		err_cnt++;
		uart_printf("1BYTE MODE error!   \n");
	}

	uart_init_cfg.fifo_access = UART_FIFO_2BYTE_MODE;	
	uart_init(MIAN_OBJ, &uart_init_cfg);
	uart_init(TEST_OBJ, &uart_init_cfg);

	uart_poll_put_char(TEST_OBJ,0x12);
	if( 0x12 != uart_block_get_char(MIAN_OBJ)){
		err_cnt++;
		uart_printf("2BYTE MODE error!   \n");
	}

	uart_init_cfg.fifo_access = UART_FIFO_4BYTE_MODE;	
	uart_init(MIAN_OBJ, &uart_init_cfg);
	uart_init(TEST_OBJ, &uart_init_cfg);

	uart_poll_put_char(TEST_OBJ,0x34);
	if( 0x34 != uart_block_get_char(MIAN_OBJ)){
		err_cnt++;
		uart_printf("4BYTE MODE error!   \n");
	}
	
	if(err_cnt>0)
		uart_printf("uart MODE test failed!   \n");
	else
		uart_printf("uart MODE test pass!   \n");
}
void main(void)
{
	uart_fifo_test();
}


