#include "debug.h"
#include "uart_driver.cx"

/*test case function*/


/*end*/
void uart_mode_test(void)
{
	uart_printf("uart mode test...	\n");

	u8 err_cnt=0;
	uart_init_def uart_init_cfg = {
			.baudrate = UART_BAUD_115200,
			.word = UART_WORD_8BIT,
			.stop = UART_STOP_1BIT,
			.parity = UART_PARITY_NONE,
			.transmit_enable = UART_TRANSMIT_ENABLE,
			.receive_enable = UART_RECEIVE_ENABLE,
			.fifo_access = UART_FIFO_1BYTE_MODE,
			.irda_mode = UART_DEFAULT_MODE,
			.channel_mode = UART_LOCAL_LOOPBACK_CHANNEL,
			.clock_select = UART_SEL_APB_CLOCK,
			.clock_prescalar8 = UART_CLOCK_PRESCALAR_DIS
	};

	uart_init(UART0, &uart_init_cfg);

	uart_poll_put_char(UART0,0x24);
	unsigned char ret = uart_block_get_char(UART0);
	if(ret != 0x24)
		err_cnt++;

	if(ret != 0x24)
		uart_printf("uart0 test pass!: [ %x ] \n",ret);
	else
		uart_printf("[error] uart0 test failed!");


	uart_init(UART1, &uart_init_cfg);

	uart_poll_put_char(UART1,0x24);
	ret = uart_block_get_char(UART1);

	if(ret != 0x24)
		err_cnt++;

	if(ret == 0x24)
		uart_printf("uart1 test pass!: [ %x ] \n",ret);
	else
		uart_printf("[error] uart1 test failed!");


}
void main(void)
{
	uart_mode_test();
}


