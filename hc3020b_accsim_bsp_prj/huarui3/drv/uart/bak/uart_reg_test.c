#include "debug.h"
#include "uart_driver.h"

/*test case function*/
void uart_register_reset_test()
{
	uart_printf("uart register reset test...	\n");

	int i=0;
	uart_def *uart_list[] = {UART0,UART1};
	uart_def *uart;
	u32 err_cnt =0;

	for(i=0;i<2;i++){
		uart = uart_list[i];
	/*reset value test*/
		if(uart->CTRL != 0x00000128){
			err_cnt++;
			uart_printf("CTRL:0x[%x]   0x[%x]   \n",0x00000128,uart->CTRL);
		}
		if(uart->MODE != 0x00000000){
			err_cnt++;
			uart_printf("MODE:0x[%x]   0x[%x]   \n",0x00000000,uart->MODE);
		}
		if(uart->IMR != 0x00000000){
			err_cnt++;
			uart_printf("IMR:0x[%x]   0x[%x]   \n",0x00000000,uart->IMR);
		}
		if(uart->CISR != 0x00000000){
			err_cnt++;
			uart_printf("CISR:0x[%x]   0x[%x]   \n",0x00000000,uart->CISR);
		}
		if(uart->RTOR != 0x00000000){
			err_cnt++;
			uart_printf("RTOR:0x[%x]   0x[%x]   \n",0x00000000,uart->RTOR);
		}
		if(uart->MCR != 0x00000000){
			err_cnt++;
			uart_printf("MCR:0x[%x]   0x[%x]   \n",0x00000000,uart->MCR);
		}
		if(uart->CSR != 0x00000000){
			err_cnt++;
			uart_printf("CSR:0x[%x]   0x[%x]   \n",0x00000000,uart->CSR);
		}
	//	if(uart->FIFO != 0x00000000)
	//		assert_result(8);	
		if(uart->BDIVR != 0x0000000F){
			err_cnt++;
			uart_printf("BDIVR:0x[%x]   0x[%x]   \n",0x0000000F,uart->BDIVR);
		}
		if(uart->FCDR != 0x00000000){
			err_cnt++;
			uart_printf("FCDR:0x[%x]   0x[%x]   \n",0x00000000,uart->FCDR);
		}
		if(uart->RBSR != 0x00000000){
			err_cnt++;
			uart_printf("RBSR:0x[%x]   0x[%x]   \n",0x00000000,uart->RBSR);
		}

		
		if(err_cnt>0)
			uart_printf("uart%d register test failed!   \n",i);
		else
			uart_printf("uart%d register test pass!   \n",i);


	}

}
const unsigned int reg_test_val[]={0x00000014,0x00001aa3,0x00001234,0x00000012,0x00000012,0x00000012,0x00000012,0x00000123,0x00001234,0x00000012,0x00000012
};
void uart_register_wr_test()
{
	uart_printf("uart register wr test...	\n");

	int i,j=0;
	int reg_value=-1;
	uart_def *uart_list[] = {UART0,UART1};
	volatile u64 *reg_addr;
	uart_def *uart;
	u32 err_cnt =0;
	
	for(i=0;i<2;i++){
		uart = uart_list[i];
			err_cnt =0;
			uart->CTRL = reg_test_val[0];
			if(uart->CTRL != reg_test_val[0]){
				err_cnt++;
				uart_printf("CTRL:0x[%x]   0x[%x]   \n",reg_test_val[0],uart->CTRL);
			}
			uart->MODE = reg_test_val[1];
			if(uart->MODE != reg_test_val[1]){
				err_cnt++;
				uart_printf("MODE:0x[%x]   0x[%x]   \n",reg_test_val[1],uart->MODE);
			}
			uart->BRGR = reg_test_val[2];
			if(uart->BRGR != reg_test_val[2]){
				err_cnt++;
				uart_printf("BRGR:0x[%x]   0x[%x]   \n",reg_test_val[2],uart->BRGR);
			}
			uart->RTOR = reg_test_val[3];
			if(uart->RTOR != reg_test_val[3]){
				err_cnt++;
				uart_printf("RTOR:0x[%x]   0x[%x]   \n",reg_test_val[3],uart->RTOR);
			}
			uart->RTR = reg_test_val[4];
			if(uart->RTR != reg_test_val[4]){
				err_cnt++;
				uart_printf("RTR:0x[%x]   0x[%x]   \n",reg_test_val[4],uart->RTR);
			}
			/*
			uart->MCR = reg_test_val[5];
			if(uart->MCR != reg_test_val[5]){
				err_cnt++;
				uart_printf("MCR:0x[%x]   0x[%x]   \n",reg_test_val[5],uart->MCR );
			}*/
			uart->BDIVR = reg_test_val[6];
			if(uart->BDIVR != reg_test_val[6]){
				err_cnt++;
				uart_printf("BDIVR:0x[%x]   0x[%x]   \n",reg_test_val[6],uart->BDIVR);
			}
			uart->FCDR = reg_test_val[7];
			if(uart->FCDR != reg_test_val[7]){
				err_cnt++;
				uart_printf("FCDR:0x[%x]   0x[%x]   \n",reg_test_val[7],uart->FCDR);
			}
			uart->RPWR = reg_test_val[8];
			if(uart->RPWR != reg_test_val[8]){
				err_cnt++;
				uart_printf("RPWR:0x[%x]   0x[%x]   \n",reg_test_val[8],uart->RPWR);
			}
			uart->TPWR = reg_test_val[9];
			if(uart->TPWR != reg_test_val[9]){
				err_cnt++;
				uart_printf("TPWR:0x[%x]   0x[%x]   \n",reg_test_val[9],uart->TPWR);
			}
			uart->TTR = reg_test_val[10];
			if(uart->TTR != reg_test_val[10]){
				err_cnt++;
				uart_printf("TTR:0x[%x]   0x[%x]   \n",reg_test_val[10],uart->TTR);
			}


			if(err_cnt>0){
				uart_printf("uart%d regiter wr test failed!    \n",i);
			}else{
				uart_printf("uart%d regiter wr test pass!    \n",i);
			}


	}

}
/*end*/
