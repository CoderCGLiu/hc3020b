#include "debug.h"
#include "uart_driver.h"

/*test case function*/


/*end*/
void uart_parity_test(void)
{
	uart_printf("uart parity test...	\n");

	u8 err_cnt =0;
	unsigned char dat=0;
	uart_init_def uart_init_cfg = {
			.baudrate = UART_BAUD_115200,
			.word = UART_WORD_8BIT,
			.stop = UART_STOP_1BIT,
			.parity = UART_PARITY_NONE,
			.transmit_enable = UART_TRANSMIT_ENABLE,
			.receive_enable = UART_RECEIVE_ENABLE,
			.fifo_access = UART_FIFO_1BYTE_MODE,
			.irda_mode = UART_DEFAULT_MODE,
			.channel_mode = UART_NORMAL_CHANNEL,
			.clock_select = UART_SEL_APB_CLOCK,
			.clock_prescalar8 = UART_CLOCK_PRESCALAR_DIS
	};
	uart_enable_pics(MIAN_OBJ,UART_INT_PARE_MASK);

	
	uart_init(MIAN_OBJ, &uart_init_cfg);
	uart_init(TEST_OBJ, &uart_init_cfg);
	uart_poll_put_char(TEST_OBJ,0x55);
	if( (0x55 != uart_block_get_char(MIAN_OBJ)) || (0 != uart_get_picstate(MIAN_OBJ,UART_INT_PARE_MASK))){
		err_cnt++;
		uart_printf("8bit parity error!   \n");
	}
	
	uart_init_cfg.parity = UART_PARITY_EVEN;	
	uart_init(MIAN_OBJ, &uart_init_cfg);
	uart_init(TEST_OBJ, &uart_init_cfg);
	uart_poll_put_char(TEST_OBJ,0x55);
	if( (0x55 != uart_block_get_char(MIAN_OBJ)) || (0 != uart_get_picstate(MIAN_OBJ,UART_INT_PARE_MASK))){
		err_cnt++;
		uart_printf("8bit parity error!   \n");
	}
	
	uart_init_cfg.parity = UART_PARITY_MARK;
	uart_init(MIAN_OBJ, &uart_init_cfg);
	uart_init(TEST_OBJ, &uart_init_cfg);
	uart_poll_put_char(TEST_OBJ,0x55);
	if( (0x55 != uart_block_get_char(MIAN_OBJ)) || (0 != uart_get_picstate(MIAN_OBJ,UART_INT_PARE_MASK))){
		err_cnt++;
		uart_printf("8bit parity error!   \n");
	}
	
	uart_init_cfg.parity = UART_PARITY_ODD;
	uart_init(MIAN_OBJ, &uart_init_cfg);
	uart_init(TEST_OBJ, &uart_init_cfg);
	uart_poll_put_char(TEST_OBJ,0x55);
	if( (0x55 != uart_block_get_char(MIAN_OBJ)) || (0 != uart_get_picstate(MIAN_OBJ,UART_INT_PARE_MASK))){
		err_cnt++;
		uart_printf("8bit parity error!   \n");
	}
	
	uart_init_cfg.parity = UART_PARITY_SPACE;
	uart_init(MIAN_OBJ, &uart_init_cfg);
	uart_init(TEST_OBJ, &uart_init_cfg);
	uart_poll_put_char(TEST_OBJ,0x55);
	if( (0x55 != uart_block_get_char(MIAN_OBJ)) || (0 != uart_get_picstate(MIAN_OBJ,UART_INT_PARE_MASK))){
		err_cnt++;
		uart_printf("8bit parity error!   \n");
	}
	
	uart_init_cfg.parity = UART_PARITY_NONE;
	//uart_init(MIAN_OBJ, &uart_init_cfg);
	uart_init(TEST_OBJ, &uart_init_cfg);
	uart_poll_put_char(TEST_OBJ,0x55);
	dat = uart_block_get_char(MIAN_OBJ);
	if(1 != uart_get_picstate(MIAN_OBJ,UART_INT_PARE_MASK)){
		err_cnt++;
		uart_printf("8bit parity error!   \n");
	}

	if(err_cnt>0)
		uart_printf("uart parity test failed!   \n");
	else
		uart_printf("uart parity test pass!   \n");


}


void main(void)
{
	uart_parity_test();
}


