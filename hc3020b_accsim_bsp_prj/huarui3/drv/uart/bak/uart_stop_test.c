#include "debug.h"
#include "uart_driver.h"


/*test case function*/


/*end*/
void uart_stop_test(void)
{
	uart_printf("uart stop test...	\n");
	u8 err_cnt =0;

	unsigned char dat=0;
	uart_init_def uart_init_cfg = {
			.baudrate = UART_BAUD_115200,
			.word = UART_WORD_8BIT,
			.stop = UART_STOP_1BIT,
			.parity = UART_PARITY_NONE,
			.transmit_enable = UART_TRANSMIT_ENABLE,
			.receive_enable = UART_RECEIVE_ENABLE,
			.fifo_access = UART_FIFO_1BYTE_MODE,
			.irda_mode = UART_DEFAULT_MODE,
			.channel_mode = UART_NORMAL_CHANNEL,
			.clock_select = UART_SEL_APB_CLOCK,
			.clock_prescalar8 = UART_CLOCK_PRESCALAR_DIS
	};
	uart_enable_pics(MIAN_OBJ,UART_INT_FRAME_MASK);
	
	uart_init(MIAN_OBJ, &uart_init_cfg);
	uart_init(TEST_OBJ, &uart_init_cfg);
	uart_poll_put_char(TEST_OBJ,0x55);
	if( (0x55 != uart_block_get_char(MIAN_OBJ)) || (0 != uart_get_picstate(MIAN_OBJ,UART_INT_FRAME_MASK))){
		err_cnt++;
		uart_printf("1bit stop error!   \n");
	}
	uart_init_cfg.stop = UART_STOP_1_5BIT;	
	uart_init(MIAN_OBJ, &uart_init_cfg);
	uart_init(TEST_OBJ, &uart_init_cfg);
	uart_poll_put_char(TEST_OBJ,0x55);
	if( (0x55 != uart_block_get_char(MIAN_OBJ)) || (0 != uart_get_picstate(MIAN_OBJ,UART_INT_FRAME_MASK))){
		err_cnt++;
		uart_printf("1_5bit stop error!   \n");
	}
	uart_init_cfg.stop = UART_STOP_2BIT;
	uart_init(MIAN_OBJ, &uart_init_cfg);
	uart_init(TEST_OBJ, &uart_init_cfg);
	uart_poll_put_char(TEST_OBJ,0x55);
	if( (0x55 != uart_block_get_char(MIAN_OBJ)) || (0 != uart_get_picstate(MIAN_OBJ,UART_INT_FRAME_MASK))){
		err_cnt++;
		uart_printf("2bit stop error!   \n");
	}
	uart_init_cfg.stop = UART_STOP_1BIT;
	//uart_init(MIAN_OBJ, &uart_init_cfg);
	uart_init(TEST_OBJ, &uart_init_cfg);
	uart_poll_put_char(TEST_OBJ,0x55);
	dat = uart_block_get_char(MIAN_OBJ);
	if( 1 != uart_get_picstate(MIAN_OBJ,UART_INT_FRAME_MASK)){
		err_cnt++;
		uart_printf("2bit_1bit stop error!   \n");
	}


	if(err_cnt>0)
		uart_printf("uart stop test failed!   \n");
	else
		uart_printf("uart stop test pass!   \n");


}

void main(void)
{
	uart_stop_test();
}


