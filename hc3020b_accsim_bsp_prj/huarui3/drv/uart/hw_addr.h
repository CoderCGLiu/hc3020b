#ifndef HW_ADDR_H
#define HW_ADDR_H

/* uart */
#define UART_BASE_ADDR		0x1f000000 //0x1f008000
//#define UART_BASE_ADDR		0xbf000000 //0x1f008000

/* gpio */
#define GPIO_BASE_ADDR		0x1f805000 //sysctrl:0x1f088000
#define INTR_MASK_ADDR		(GPIO_BASE_ADDR + 0x224)
#define INTR_INT_4		(GPIO_BASE_ADDR + 0x210)
#define SYS_CTRL		0x1f038000
#define DSP_INT_CTRL		(SYS_CTRL + 0x200)
#define DSP_INT_ST		(SYS_CTRL + 0x240 * 4)

/* pcie  */
#define PCI_APP_BUS_ADDR(_controller)	(0x1f028000 + (_controller << 12))
#define PCI_INTR_CTRL			(PCI_APP_BUS_ADDR(controller) + 0X64)
#define PCI_INTR_ST			(PCI_APP_BUS_ADDR(controller) + 0x60)
#define PCI_INT_TIMEOUT_ST		(PCI_APP_BUS_ADDR(controller) + 0x6c)
#define PCI_VENDOR_MSG_CFG_0		(PCI_APP_BUS_ADDR(controller) + 0x10)
#define PCI_VENDOR_MSG_CFG_1		(PCI_APP_BUS_ADDR(controller) + 0x14)
#define PCI_VENDOR_MSG_DATA_0		(PCI_APP_BUS_ADDR(controller) + 0x18)
#define PCI_VENDOR_MSG_DATA_1		(PCI_APP_BUS_ADDR(controller) + 0x1c)
#define LTSSM				(PCI_APP_BUS_ADDR(controller)) /* write (orig_data | 4) to enable ltssm */
#define LTSSM_STATUS_H	 		(PCI_APP_BUS_ADDR(controller) + 0x28) /* cxpl_debug_info[63:0] */
#define LTSSM_STATUS_L	 		(PCI_APP_BUS_ADDR(controller) + 0x24)
#define PCIE_DEVICE_TYPE_SEL		(PCI_APP_BUS_ADDR(controller) + 0x04) /* 0 -- ep; 4 -- rc */

/* PCIe0: 0x18000000
 * PCIe1: 0x18800000
 */
#define PCIE_CONTROLLER(_controller)	(0x18000000 + ((_controller) << 24))
#define CDM_BASE_ADDR			PCIE_CONTROLLER(controller)
#define OUTBOUND_CFG_BASE_ADDR		(PCIE_CONTROLLER(controller) + 0x100000) /* cfg must be 0x100000 */
#define OUTBOUND_CFG_LIMIT_ADDR		((PCIE_CONTROLLER(controller) + 0x1fffff) & 0xffffff)
#define OUTBOUND_MEM_BASE_ADDR		(PCIE_CONTROLLER(controller) + 0x200000)
#define OUTBOUND_MEM_LIMIT_ADDR    	(PCIE_CONTROLLER(controller) + 0x2fffff)
#define	OUTBOUND_MEM_TARGET_ADDR	0x20000000
#define OUTBOUND_IO_BASE_ADDR		(PCIE_CONTROLLER(controller) + 0x300000)
#define OUTBOUND_IO_LIMIT_ADDR		(PCIE_CONTROLLER(controller) + 0X3fffff)
#define OUTBOUND_IO_TARGET_ADDR		0xd0800000
#define RC_MSG_BASE_ADDR		(PCIE_CONTROLLER(controller) + 0x400000)
#define RC_MSG_LIMIT_ADDR		(PCIE_CONTROLLER(controller) + 0x4fffff)
#define MSI_BASE_ADDR			(PCIE_CONTROLLER(controller) + 0x500000)
#define DMA_IMWR_ADDR			(PCIE_CONTROLLER(controller) + 0x600000)

#if __MIPS64__
#define MEMORY_BASE(_controller)	(0x10000000 + (((_controller) * 4) << 20))
#else
#define MEMORY_BASE(_controller)	(0x10000000 + (((_controller) * 4) << 20))
#endif
#define INBOUND_MEM_TARGET_ADDR		(MEMORY_BASE(controller)) /* in DDR memory */
#define INBOUND_MEM_BASE_ADDR		0x30000000
#define INBOUND_MEM_LIMIT_ADDR		0x300fffff
#define INBOUND_IO_TARGET_ADDR		(MEMORY_BASE(controller) + 0x100000) /* in DDR memory */
#define INBOUND_IO_BASE_ADDR		0x30100000
#define INBOUND_IO_LIMIT_ADDR		0x301fffff
#define DMA_TEST_MEM			(MEMORY_BASE(controller) + 0x200000)  /* in DDR memory */
#define DMA_TEST_MEM_2			(MEMORY_BASE(controller) + 0x300000) /* in DDR memory */
#define LINKED_LIST_DESC_MEM		(MEMORY_BASE(controller) + 0x400000)

#define RC_BAR0_MEM_ADDR		OUTBOUND_MEM_TARGET_ADDR
#define RC_BAR1_IO_ADDR			OUTBOUND_IO_TARGET_ADDR
#define EP_BAR0_MEM_ADDR		OUTBOUND_MEM_TARGET_ADDR
#define EP_BAR1_IO_ADDR			0x20800000

/* lpc */
//#define LPC_BASE_ADDR		0x1ca00000
//#define LPC_BASE_ADDR		0x1f0d_0000

/* GRIO */
//#define GRIO_APB_CSR_BASE_ADDR(_controller)	(0x1f600000 + (_controller) * 0x100000)
//#define GRIO_APB_BASE_ADDR(_controller)	    (0x10800000 + (_controller) * 0x2800000)

#endif
