#include "debug.h"
#include "uart_driver.cx"

/*test case function*/


/*end*/
void uart_transmit_test(void)
{
	uart_init_def uart_init_cfg = {
			.baudrate = UART_BAUD_115200,
			.word = UART_WORD_8BIT,
			.stop = UART_STOP_1BIT,
			.parity = UART_PARITY_NONE,
			.transmit_enable = UART_TRANSMIT_ENABLE,
			.receive_enable = UART_RECEIVE_ENABLE,
			.fifo_access = UART_FIFO_1BYTE_MODE,
			.irda_mode = UART_DEFAULT_MODE,
			.channel_mode = UART_NORMAL_CHANNEL,
			.clock_select = UART_SEL_APB_CLOCK,
			.clock_prescalar8 = UART_CLOCK_PRESCALAR_DIS
	};

	uart_def * uart0 = (uart_def *)(0x900000001f000000);
//	uart_init(UART0, &uart_init_cfg);
	uart_init(uart0, &uart_init_cfg);
	
	int i=0;
	while(1){
//		uart_poll_fifo_put_char(UART0,0x30+i%10);
		uart_poll_fifo_put_char(uart0,0x30+i%10);
		i++;
	}
	
}

