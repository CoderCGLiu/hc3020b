#ifndef UART_DRIVER_H
#define UART_DRIVER_H
#include "uart_reg_def.h"
/*define*/

/*define*/
#define	TEST_UART	1
#if TEST_UART==0
#define TEST_OBJ	UART0
#define MIAN_OBJ	UART1
#elif TEST_UART==1
#define TEST_OBJ	UART1
#define MIAN_OBJ	UART0
#endif

#define UART_TRANSMIT_ENABLE			1
#define UART_TRANSMIT_DISABLE			0

#define UART_RECEIVE_ENABLE				1
#define UART_RECEIVE_DISABLE			0

#define UART_BAUD_4800					(4800)
#define UART_BAUD_9600					(9600)
#define UART_BAUD_14400					(14400)
#define UART_BAUD_28800					(28800)
#define UART_BAUD_19200					(19200)
#define UART_BAUD_38400					(38400)
#define UART_BAUD_57600					(57600)
#define UART_BAUD_115200				(115200)
#define UART_BAUD_128000				(128000)
#define UART_BAUD_230400				(230400)
#define UART_BAUD_256000				(256000)
#define UART_BAUD_460800				(460800)

#define UART_WORD_6BIT					(3)
#define UART_WORD_7BIT					(2)
#define UART_WORD_8BIT					(1)//or (0)

#define UART_STOP_1BIT					(0)
#define UART_STOP_1_5BIT				(1)
#define UART_STOP_2BIT					(2)
#define UART_STOP_RES_BIT				(3)

#define UART_PARITY_EVEN				(0)
#define UART_PARITY_ODD					(1)
#define UART_PARITY_SPACE				(2)
#define UART_PARITY_MARK				(3)
#define UART_PARITY_NONE				(4)//or(5)(6)(7)

#define UART_FIFO_LEGACY_MODE			(0)
#define UART_FIFO_1BYTE_MODE			(1)
#define UART_FIFO_2BYTE_MODE			(2)
#define UART_FIFO_4BYTE_MODE			(3)

#define UART_DEFAULT_MODE				(0)
#define UART_IRDA_MODE					(1)

#define UART_NORMAL_CHANNEL				(0)
#define UART_ECHO_CHANNEL				(1)
#define UART_LOCAL_LOOPBACK_CHANNEL		(2)
#define UART_REMOTE_LOOPBACK_CHANNEL	(3)

#define UART_SEL_APB_CLOCK				(0)
#define UART_SEL_USER_CLOCK				(1)

#define UART_CLOCK_PRESCALAR_DIS		(0)
#define UART_CLOCK_PRESCALAR_EN			(1)


/*data struct*/
typedef struct {
	unsigned int baudrate;
	unsigned char word;
	unsigned char stop;
	unsigned char parity;
	unsigned char transmit_enable;
	unsigned char receive_enable;
	unsigned char fifo_access;
	unsigned char irda_mode;
	unsigned char channel_mode;
	unsigned char clock_select;
	unsigned char clock_prescalar8;
}uart_init_def;


/*function api*/

void uart_init(uart_def *uartx ,uart_init_def *uart_init);
void uart_poll_put_char(uart_def *uartx,int c);
void uart_poll_fifo_put_char(uart_def *uartx,int c);
int uart_poll_get_char(uart_def *uartx);
int uart_block_get_char(uart_def *uartx);
void uart_write_byte(uart_def *uartx,unsigned char data);
void uart_write(uart_def *uartx,unsigned char *data,unsigned int size);

//pic
void uart_enable_pics(uart_def *uartx, unsigned int pics);
void uart_disenable_pics(uart_def *uartx, unsigned int pics);
unsigned char uart_get_picstate(uart_def *uartx, unsigned int pic);
unsigned char uart_get_picmask(uart_def *uartx, unsigned int pic);
void uart_rfifo_trigger(uart_def *uartx,int num);
void uart_tfifo_trigger(uart_def *uartx,int num);


#endif

