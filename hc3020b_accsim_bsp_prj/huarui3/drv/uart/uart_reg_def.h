#ifndef UART_REG_H
#define UART_REG_H
#include "hw_addr.h"

/*register define*/
typedef struct 
{
	volatile unsigned int CTRL;
	volatile unsigned int MODE;
	volatile unsigned int IER;
	volatile unsigned int IDR;
	volatile unsigned int IMR;
	volatile unsigned int CISR;
	volatile unsigned int BRGR;
	volatile unsigned int RTOR;
	volatile unsigned int RTR;
	volatile unsigned int MCR;
	volatile unsigned int MSR;
	volatile unsigned int CSR;
	volatile unsigned int FIFO;
	volatile unsigned int BDIVR;
	volatile unsigned int FCDR;
	volatile unsigned int RPWR;
	volatile unsigned int TPWR;
	volatile unsigned int TTR;
	volatile unsigned int RBSR;
}uart_def;

#ifdef __MIPS64__
#define UART0_BASE_ADDR (0x9000000000000000 | ((unsigned long)UART_BASE_ADDR))
#define UART1_BASE_ADDR (0x9000000000000000 | ((unsigned long)(UART_BASE_ADDR + 0x8000)))

#else
#define UART0_BASE_ADDR (UART_BASE_ADDR)
#define UART1_BASE_ADDR (UART_BASE_ADDR + 0x8000)

#endif


#define UART_CTRL_MASK					(0x000001ff)
#define UART_MODE_MASK					(0x00003fff)
#define UART_IER_MASK					(0x00003fff)
#define UART_IDR_MASK					(0x000001ff)
#define UART_IMR_MASK					(0x000001ff)
#define UART_CISR_MASK					(0x000001ff)
#define UART_BRGR_MASK					(0x0000ffff)
#define UART_RTOR_MASK					(0x000000ff)
#define UART_RTR_MASK					(0x0000001f)
#define UART_MCR_MASK					(0x00000023)
#define UART_MSR_MASK					(0x000000ff)

#define UART_CSR_MASK					(0x0000ffff)
#define UART_CSR_TNFUL_MASK				(1<14)
#define UART_CSR_REMPTY_MASK			(0x00000002)
#define UART_CSR_TEMPTY_MASK			(0x00000008)
#define UART_CSR_TFUL_MASK				(1<<4)

#define UART_FIFO_MASK					(0xffffffff)
#define UART_BDIVR_MASK					(0x000000ff)
#define UART_FCDR_MASK					(0x0000001f)
#define UART_RPWR_MASK					(0x0000ffff)
#define UART_TPWR_MASK					(0x000000ff)
#define UART_TTR_MASK					(0x0000001f)
#define UART_RBSR_MASK					(0x000007ff)

#define UART_INT_RBRK_MASK				(1<<13)
#define UART_INT_TOVR_MASK				(1<<12)
#define UART_INT_TNFUL_MASK				(1<<11)
#define UART_INT_TTRIG_MASK				(1<<10)
#define UART_INT_DMSI_MASK				(1<<9)
#define UART_INT_TIMEOUT_MASK			(1<<8)
#define UART_INT_PARE_MASK				(1<<7)
#define UART_INT_FRAME_MASK				(1<<6)
#define UART_INT_ROVR_MASK				(1<<5)
#define UART_INT_TFUL_MASK				(1<<4)
#define UART_INT_TEMPTY_MASK			(1<<3)
#define UART_INT_RFUL_MASK				(1<<2)
#define UART_INT_REMPTY_MASK			(1<<1)
#define UART_INT_RTRIG_MASK				(1<<0)


#define UART0	((uart_def *) UART0_BASE_ADDR)
#define UART1	((uart_def *) UART1_BASE_ADDR)



#endif

