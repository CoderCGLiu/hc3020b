
#ifndef __CDNS_GEM_H__
#define __CDNS_GEM_H__

#include <netLib.h>
#include <endLib.h>
#include <netBufLib.h>
#include <end.h>
#include <net/ethernet.h>
#include <cacheLib.h>
#include <semLib.h>
#include <pthread.h>
#include <lstlib.h>
#include <sys/types.h>

#define GEM_MAX_DEV             4


/*
 * Cadence GEM 千兆以太网控制器寄存器定义
 * */
#define GEM_NCR                  (0x000)	/* RW: network_control */
#define GEM_NCFGR                (0x004)	/* RW: network_config */
#define GEM_NSR                  (0x008)	/* RO: network_status */
#define GEM_UR                   (0x00C)	/* RW: user_io_register */
#define GEM_DCFGR                (0x010)	/* RW: dma_config */
#define GEM_TSR                  (0x014)	/* RW: transmit_status */
#define GEM_RBQB                 (0x018)	/* RW: receive_q_ptr */
#define GEM_TBQB                 (0x01C)	/* RW: transmit_q_ptr */
#define GEM_RSR                  (0x020)	/* RW: receive_status */
#define GEM_ISR                  (0x024)	/* RW: int_status */
#define GEM_IER                  (0x028)	/* RW: int_enable */
#define GEM_IDR                  (0x02C)	/* RW: int_disable */
#define GEM_IMR                  (0x030)	/* RO: int_mask */
#define GEM_MAN                  (0x034)	/* RW: phy_management */
#define GEM_RPQ                  (0x038)	/* RO: pause_time */
#define GEM_TPQ                  (0x03C)	/* RW: tx_pause_quantum */
#define GEM_TPSF                 (0x040)	/* RW: pbuf_txcutthru */
#define GEM_RPSF                 (0x044)	/* RW: pbuf_rxcutthru */

#define GEM_HRB                  (0x080)	/* RW: hash_bottom */
#define GEM_HRT                  (0x084)	/* RW: hash_top */
#define GEM_SAB1                 (0x088)	/* RW: spec_add1_bottom */
#define GEM_SAT1                 (0x08C)	/* RW: spec_add1_top */
#define GEM_SAB2                 (0x090)	/* RW: spec_add2_bottom */
#define GEM_SAT2                 (0x094)	/* RW: spec_add2_top */
#define GEM_SAB3                 (0x098)	/* RW: spec_add3_bottom */
#define GEM_SAT3                 (0x09C)	/* RW: spec_add3_top */
#define GEM_SAB4                 (0x0A0)	/* RW: spec_add4_bottom */
#define GEM_SAT4                 (0x0A4)	/* RW: spec_add4_top */
#define GEM_TIDM1                (0x0A8)	/* RW: spec_type1 */
#define GEM_TIDM2                (0x0AC)	/* RW: spec_type2 */
#define GEM_TIDM3                (0x0B0)	/* RW: spec_type3 */
#define GEM_TIDM4                (0x0B4)	/* RW: spec_type4 */
#define GEM_WOL                  (0x0B8)	/* RW: wol_register */
#define GEM_IPGS                 (0x0BC)	/* RW: stretch_ratio */
#define GEM_SVLAN                (0x0C0)	/* RW: stacked_vlan */
#define GEM_TPFCP                (0x0C4)	/* RW: tx_pfc_pause */
#define GEM_SAMB1                (0x0C8)	/* RW: mask_add1_bottom */
#define GEM_SAMT1                (0x0CC)	/* RW: mask_add1_top */

#define GEM_OTLO                 (0x100)	/* RO: octets_txed_bottom */
#define GEM_OTHI                 (0x104)	/* RO: octets_txed_top */
#define GEM_FT                   (0x108)	/* RO: frames_txed_ok */
#define GEM_BCFT                 (0x10C)	/* RO: broadcast_txed */
#define GEM_MFT                  (0x110)	/* RO: multicast_txed */
#define GEM_PFT                  (0x114)	/* RO: pause_frames_txed */
#define GEM_BFT64                (0x118)	/* RO: frames_txed_64 */
#define GEM_TBFT127              (0x11C)	/* RO: frames_txed_65 */
#define GEM_TBFT255              (0x120)	/* RO: frames_txed_128 */
#define GEM_TBFT511              (0x124)	/* RO: frames_txed_256 */
#define GEM_TBFT1023             (0x128)	/* RO: frames_txed_512 */
#define GEM_TBFT1518             (0x12C)	/* RO: frames_txed_1024 */
#define GEM_GTBFT1518            (0x130)	/* RO: frames_txed_1519 */
#define GEM_TUR                  (0x134)	/* RO: tx_underruns */
#define GEM_SCF                  (0x138)	/* RO: single_collisions */
#define GEM_MCF                  (0x13C)	/* RO: multiple_collisions */
#define GEM_EC                   (0x140)	/* RO: excessive_collisions */
#define GEM_LC                   (0x144)	/* RO: late_collisions */
#define GEM_DTF                  (0x148)	/* RO: deferred_frames */
#define GEM_CSE                  (0x14C)	/* RO: crs_errors */
#define GEM_ORLO                 (0x150)	/* RO: octets_rxed_bottom */
#define GEM_ORHI                 (0x154)	/* RO: octets_rxed_top */
#define GEM_FR                   (0x158)	/* RO: frames_rxed_ok */
#define GEM_BCFR                 (0x15C)	/* RO: broadcast_rxed */
#define GEM_MFR                  (0x160)	/* RO: multicast_rxed */
#define GEM_PFR                  (0x164)	/* RO: pause_frames_rxed */
#define GEM_BFR64                (0x168)	/* RO: frames_rxed_64 */
#define GEM_TBFR127              (0x16C)	/* RO: frames_rxed_65 */
#define GEM_TBFR255              (0x170)	/* RO: frames_rxed_128 */
#define GEM_TBFR511              (0x174)	/* RO: frames_rxed_256 */
#define GEM_TBFR1023             (0x178)	/* RO: frames_rxed_512 */
#define GEM_TBFR1518             (0x17C)	/* RO: frames_rxed_1024 */
#define GEM_TMXBFR               (0x180)	/* RO: frames_rxed_1519 */
#define GEM_UFR                  (0x184)	/* RO: undersize_frames */
#define GEM_OFR                  (0x188)	/* RO: excessive_rx_length */
#define GEM_JR                   (0x18C)	/* RO: rx_jabbers */
#define GEM_FCSE                 (0x190)	/* RO: fcs_errors */
#define GEM_LFFE                 (0x194)	/* RO: rx_length_errors */
#define GEM_RSE                  (0x198)	/* RO: rx_symbol_errors */
#define GEM_AE                   (0x19C)	/* RO: alignment_errors */
#define GEM_RRE                  (0x1A0)	/* RO: rx_resource_errors */
#define GEM_ROE                  (0x1A4)	/* RO: rx_overruns */
#define GEM_IHCE                 (0x1A8)	/* RO: rx_ip_ck_errors */
#define GEM_TCE                  (0x1AC)	/* RO: rx_tcp_ck_errors */
#define GEM_UCE                  (0x1B0)	/* RO: rx_udp_ck_errors */

#define GEM_TSSS                 (0x1C8)	/* RO: tsu_strobe_sec */
#define GEM_TSSN                 (0x1CC)	/* RO: tsu_strobe_nsec */
#define GEM_TS                   (0x1D0)	/* RW: tsu_timer_sec */
#define GEM_TN                   (0x1D4)	/* RW: tsu_timer_nsec */
#define GEM_TA                   (0x1D8)	/* RW: tsu_timer_adjust */
#define GEM_TI                   (0x1DC)	/* RW: tsu_timer_incr */
#define GEM_EFTS                 (0x1E0)	/* RO: tsu_ptp_tx_sec */
#define GEM_EFTN                 (0x1E4)	/* RO: tsu_ptp_tx_nsec */
#define GEM_EFRS                 (0x1E8)	/* RO: tsu_ptp_rx_sec */
#define GEM_EFRN                 (0x1EC)	/* RO: tsu_ptp_rx_nsec */
#define GEM_PEFTS                (0x1F0)	/* RO: tsu_peer_tx_sec */
#define GEM_PEFTN                (0x1F4)	/* RO: tsu_peer_tx_nsec */
#define GEM_PEFRS                (0x1F8)	/* RO: tsu_peer_rx_sec */
#define GEM_PEFRN                (0x1FC)	/* RO: tsu_peer_rx_nsec */
#define GEM_PCR                	 (0x200)	/* RW: pcs_control */
#define GEM_PSR                	 (0x204)	/* RO: pcs_status */
#define GEM_PPTI                 (0x208)	/* RO: pcs_phy_top_id */
#define GEM_PPBI                 (0x20C)	/* RO: pcs_phy_bot_id */
#define GEM_PAA                	 (0x210)	/* RW: pcs_an_adv */
#define GEM_PALB                 (0x214)	/* RO: pcs_an_lp_base */
#define GEM_PAE                  (0x218)	/* RO: pcs_an_exp */
#define GEM_PANT                 (0x21C)	/* RW: pcs_an_np_tx */
#define GEM_PALN                 (0x220)	/* RO: pcs_an_lp_np */

#define GEM_PANES                (0x23C)	/* RO: pcs_an_ext_status */

#define GEM_DCFG1                (0x280)	/* RO: designcfg_debug1 */
#define GEM_DCFG2                (0x284)	/* RO: designcfg_debug2 */
#define GEM_DCFG3                (0x288)	/* RO: designcfg_debug3 */
#define GEM_DCFG4                (0x28C)	/* RO: designcfg_debug4 */
#define GEM_DCFG5                (0x290)	/* RO: designcfg_debug5 */
#define GEM_DCFG6                (0x294)	/* RO: designcfg_debug6 */

#define GEM_TBQB_UPPER           (0x4C8)	/* RW: upper_tx_q_base_addr */
#define GEM_TBDC            	 (0x4CC)	/* RW: tx_bd_control */
#define GEM_RBDC          		 (0x4D0)	/* RW: rx_bd_control */
#define GEM_RBQB_UPPER           (0x4D4)	/* RW: upper_rx_q_base_addr */
/*********************************************************************************************************
   以太网控制器寄存器位定义
*********************************************************************************************************/
#define GEM_NCR_LB               (0x1u << 0)
#define GEM_NCR_LBL              (0x1u << 1)
#define GEM_NCR_RXEN             (0x1u << 2)
#define GEM_NCR_TXEN             (0x1u << 3)
#define GEM_NCR_MPE              (0x1u << 4)
#define GEM_NCR_CLRSTAT          (0x1u << 5)
#define GEM_NCR_INCSTAT          (0x1u << 6)
#define GEM_NCR_WESTAT           (0x1u << 7)
#define GEM_NCR_BP               (0x1u << 8)
#define GEM_NCR_TSTART           (0x1u << 9)
#define GEM_NCR_THALT            (0x1u << 10)
#define GEM_NCR_TXPF             (0x1u << 11)
#define GEM_NCR_TXZQPF           (0x1u << 12)
#define GEM_NCR_RDS              (0x1u << 14)
#define GEM_NCR_SRTSM            (0x1u << 15)
#define GEM_NCR_ENPBPR           (0x1u << 16)
#define GEM_NCR_TXPBPF           (0x1u << 17)
#define GEM_NCR_FNP              (0x1u << 18)

#define GEM_NCFGR_SPD            (0x1u << 0)//100M
#define GEM_NCFGR_FD             (0x1u << 1)//FULL DEPLEX
#define GEM_NCFGR_DNVLAN         (0x1u << 2)
#define GEM_NCFGR_JFRAME         (0x1u << 3)
#define GEM_NCFGR_CAF            (0x1u << 4)    /* copy all frame (promisc)     */
#define GEM_NCFGR_NBC            (0x1u << 5)    /* no broadcast frame           */
#define GEM_NCFGR_MTIHEN         (0x1u << 6)    /* multicast hash en            */
#define GEM_NCFGR_UNIHEN         (0x1u << 7)
#define GEM_NCFGR_MAXFS          (0x1u << 8)
#define GEM_NCFGR_GBE            (0x1u << 10)//1000M
#define GEM_NCFGR_PIS            (0x1u << 11)
#define GEM_NCFGR_RTY            (0x1u << 12)
#define GEM_NCFGR_PEN            (0x1u << 13)
#define GEM_NCFGR_LFERD          (0x1u << 16)
#define GEM_NCFGR_RFCS           (0x1u << 17)
#define GEM_NCFGR_CLK_MCK_8      (0x0u << 18)
#define GEM_NCFGR_CLK_MCK_16     (0x1u << 18)
#define GEM_NCFGR_CLK_MCK_32     (0x2u << 18)
#define GEM_NCFGR_CLK_MCK_48     (0x3u << 18)
#define GEM_NCFGR_CLK_MCK_64     (0x4u << 18)
#define GEM_NCFGR_CLK_MCK_96     (0x5u << 18)
#define GEM_NCFGR_CLK_MCK_128    (0x6u << 18)
#define GEM_NCFGR_CLK_MCK_224    (0x7u << 18)
#define GEM_NCFGR_DBW_DBW32      (0x0u << 21)
#define GEM_NCFGR_DBW_DBW64      (0x1u << 21)
#define GEM_NCFGR_DBW_DBW128     (0x2u << 21)
#define GEM_NCFGR_DCPF           (0x1u << 23)
#define GEM_NCFGR_RXCOEN         (0x1u << 24)
#define GEM_NCFGR_EFRHD          (0x1u << 25)
#define GEM_NCFGR_IRXFCS         (0x1u << 26)
#define GEM_NCFGR_SGMIIEN        (0x1u << 27)//SGMII
#define GEM_NCFGR_IPGSEN         (0x1u << 28)
#define GEM_NCFGR_RXBP           (0x1u << 29)
#define GEM_NCFGR_IRXER          (0x1u << 30)

#define GEM_NSR_MDIO             (0x1u << 1)
#define GEM_NSR_IDLE             (0x1u << 2)

#define GEM_UR_RGMII             (0x1u << 0)
#define GEM_UR_HDFC              (0x1u << 6)
#define GEM_UR_BPDG              (0x1u << 7)

#define GEM_DCFGR_FBLDO_SINGLE   (0x1u << 0)
#define GEM_DCFGR_FBLDO_INCR4    (0x4u << 0)
#define GEM_DCFGR_FBLDO_INCR8    (0x8u << 0)
#define GEM_DCFGR_FBLDO_INCR16   (0x10u << 0)
#define GEM_DCFGR_ESMA           (0x1u << 6)
#define GEM_DCFGR_ESPA           (0x1u << 7)
#define GEM_DCFGR_RXBMS_EIGHTH   (0x0u << 8)
#define GEM_DCFGR_RXBMS_QUARTER  (0x1u << 8)
#define GEM_DCFGR_RXBMS_HALF     (0x2u << 8)
#define GEM_DCFGR_RXBMS_FULL     (0x3u << 8)
#define GEM_DCFGR_TXPBMS         (0x1u << 10)
#define GEM_DCFGR_TXCOEN         (0x1u << 11)
#define GEM_DCFGR_DDRP           (0x1u << 24)

#define GEM_TSR_UBR              (0x1u << 0)
#define GEM_TSR_COL              (0x1u << 1)
#define GEM_TSR_RLE              (0x1u << 2)
#define GEM_TSR_TXGO             (0x1u << 3)
#define GEM_TSR_TFC              (0x1u << 4)
#define GEM_TSR_TXCOMP           (0x1u << 5)
#define GEM_TSR_UND              (0x1u << 6)
#define GEM_TSR_LCO              (0x1u << 7)
#define GEM_TSR_HRESP            (0x1u << 8)

#define GEM_RSR_BNA              (0x1u << 0)
#define GEM_RSR_REC              (0x1u << 1)
#define GEM_RSR_RXOVR            (0x1u << 2)
#define GEM_RSR_HNO              (0x1u << 3)

#define GEM_ISR_MFS              (0x1u << 0)
#define GEM_ISR_RCOMP            (0x1u << 1)
#define GEM_ISR_RXUBR            (0x1u << 2)
#define GEM_ISR_TXUBR            (0x1u << 3)
#define GEM_ISR_TUR              (0x1u << 4)
#define GEM_ISR_RLEX             (0x1u << 5)
#define GEM_ISR_TFC              (0x1u << 6)
#define GEM_ISR_TCOMP            (0x1u << 7)
#define GEM_ISR_LINK             (0x1u << 9)
#define GEM_ISR_ROVR             (0x1u << 10)
#define GEM_ISR_HRESP            (0x1u << 11)
#define GEM_ISR_PFNZ             (0x1u << 12)
#define GEM_ISR_PTZ              (0x1u << 13)
#define GEM_ISR_PFTR             (0x1u << 14)
#define GEM_ISR_EXINT            (0x1u << 15)
#define GEM_ISR_DRQFR            (0x1u << 18)
#define GEM_ISR_SFR              (0x1u << 19)
#define GEM_ISR_DRQFT            (0x1u << 20)
#define GEM_ISR_SFT              (0x1u << 21)
#define GEM_ISR_PDRQFR           (0x1u << 22)
#define GEM_ISR_PDRSFR           (0x1u << 23)
#define GEM_ISR_PDRQFT           (0x1u << 24)
#define GEM_ISR_PDRSFT           (0x1u << 25)
#define GEM_ISR_SRI              (0x1u << 26)
#define GEM_ISR_WOL              (0x1u << 28)

#define GEM_IER_MFS              (0x1u << 0)
#define GEM_IER_RCOMP            (0x1u << 1)
#define GEM_IER_RXUBR            (0x1u << 2)
#define GEM_IER_TXUBR            (0x1u << 3)
#define GEM_IER_TUR              (0x1u << 4)
#define GEM_IER_RLEX             (0x1u << 5)
#define GEM_IER_TFC              (0x1u << 6)
#define GEM_IER_TCOMP            (0x1u << 7)
#define GEM_IER_LINK             (0x1u << 9)
#define GEM_IER_ROVR             (0x1u << 10)
#define GEM_IER_HRESP            (0x1u << 11)
#define GEM_IER_PFNZ             (0x1u << 12)
#define GEM_IER_PTZ              (0x1u << 13)
#define GEM_IER_PFTR             (0x1u << 14)
#define GEM_IER_EXINT            (0x1u << 15)
#define GEM_IER_DRQFR            (0x1u << 18)
#define GEM_IER_SFR              (0x1u << 19)
#define GEM_IER_DRQFT            (0x1u << 20)
#define GEM_IER_SFT              (0x1u << 21)
#define GEM_IER_PDRQFR           (0x1u << 22)
#define GEM_IER_PDRSFR           (0x1u << 23)
#define GEM_IER_PDRQFT           (0x1u << 24)
#define GEM_IER_PDRSFT           (0x1u << 25)
#define GEM_IER_SRI              (0x1u << 26)
#define GEM_IER_LPI              (0x1u << 27)
#define GEM_IER_WOL              (0x1u << 28)

#define GEM_IDR_MFS              (0x1u << 0)
#define GEM_IDR_RCOMP            (0x1u << 1)
#define GEM_IDR_RXUBR            (0x1u << 2)
#define GEM_IDR_TXUBR            (0x1u << 3)
#define GEM_IDR_TUR              (0x1u << 4)
#define GEM_IDR_RLEX             (0x1u << 5)
#define GEM_IDR_TFC              (0x1u << 6)
#define GEM_IDR_TCOMP            (0x1u << 7)
#define GEM_IDR_LINK             (0x1u << 9)
#define GEM_IDR_ROVR             (0x1u << 10)
#define GEM_IDR_ROVR             (0x1u << 10)
#define GEM_IDR_HRESP            (0x1u << 11)
#define GEM_IDR_PFNZ             (0x1u << 12)
#define GEM_IDR_PTZ              (0x1u << 13)
#define GEM_IDR_PFTR             (0x1u << 14)
#define GEM_IDR_EXINT            (0x1u << 15)
#define GEM_IDR_DRQFR            (0x1u << 18)
#define GEM_IDR_SFR              (0x1u << 19)
#define GEM_IDR_DRQFT            (0x1u << 20)
#define GEM_IDR_SFT              (0x1u << 21)
#define GEM_IDR_PDRQFR           (0x1u << 22)
#define GEM_IDR_PDRSFR           (0x1u << 23)
#define GEM_IDR_PDRQFT           (0x1u << 24)
#define GEM_IDR_PDRSFT           (0x1u << 25)
#define GEM_IDR_SRI              (0x1u << 26)
#define GEM_IDR_LPI              (0x1u << 27)
#define GEM_IDR_WOL              (0x1u << 28)

#define GEM_IMR_MFS              (0x1u << 0)
#define GEM_IMR_RCOMP            (0x1u << 1)
#define GEM_IMR_RXUBR            (0x1u << 2)
#define GEM_IMR_TXUBR            (0x1u << 3)
#define GEM_IMR_TUR              (0x1u << 4)
#define GEM_IMR_RLEX             (0x1u << 5)
#define GEM_IMR_TFC              (0x1u << 6)
#define GEM_IMR_TCOMP            (0x1u << 7)
#define GEM_IMR_ROVR             (0x1u << 10)
#define GEM_IMR_HRESP            (0x1u << 11)
#define GEM_IMR_PFNZ             (0x1u << 12)
#define GEM_IMR_PTZ              (0x1u << 13)
#define GEM_IMR_PFTR             (0x1u << 14)
#define GEM_IMR_EXINT            (0x1u << 15)
#define GEM_IMR_DRQFR            (0x1u << 18)
#define GEM_IMR_SFR              (0x1u << 19)
#define GEM_IMR_DRQFT            (0x1u << 20)
#define GEM_IMR_SFT              (0x1u << 21)
#define GEM_IMR_PDRQFR           (0x1u << 22)
#define GEM_IMR_PDRSFR           (0x1u << 23)
#define GEM_IMR_PDRQFT           (0x1u << 24)
#define GEM_IMR_PDRSFT           (0x1u << 25)

#define GEM_MAN_DATA_Pos         0
#define GEM_MAN_DATA_Msk         (0xffffu << GEM_MAN_DATA_Pos)
#define GEM_MAN_DATA(value)      ((GEM_MAN_DATA_Msk & ((value) << GEM_MAN_DATA_Pos)))
#define GEM_MAN_WTN_Pos          16
#define GEM_MAN_WTN_Msk          (0x3u << GEM_MAN_WTN_Pos)
#define GEM_MAN_WTN(value)       ((GEM_MAN_WTN_Msk & ((value) << GEM_MAN_WTN_Pos)))
#define GEM_MAN_REGA_Pos         18
#define GEM_MAN_REGA_Msk         (0x1fu << GEM_MAN_REGA_Pos)
#define GEM_MAN_REGA(value)      ((GEM_MAN_REGA_Msk & ((value) << GEM_MAN_REGA_Pos)))
#define GEM_MAN_PHYA_Pos         23
#define GEM_MAN_PHYA_Msk         (0x1fu << GEM_MAN_PHYA_Pos)
#define GEM_MAN_PHYA(value)      ((GEM_MAN_PHYA_Msk & ((value) << GEM_MAN_PHYA_Pos)))
#define GEM_MAN_OP_Pos           28
#define GEM_MAN_OP_Msk           (0x3u << GEM_MAN_OP_Pos)
#define GEM_MAN_OP(value)        ((GEM_MAN_OP_Msk & ((value) << GEM_MAN_OP_Pos)))
#define GEM_MAN_CLTTO            (0x1u << 30)
#define GEM_MAN_WZO              (0x1u << 31)

#define GEM_MAX_HASH_BITS        64

/*********************************************************************************************************
  GEM 控制器硬件特性配置
*********************************************************************************************************/
#define GEM_DMA_64BIT
#define GEM_CPU_64BIT            1
#define GEM_DESC_DUMMY           1

#define GEM_DEVICE_NAME 		"gem"

#define GEM_TX_DESC_CNT			2048//1024//512
#define GEM_RX_DESC_CNT			2048//1024//512
#define GEM_TX_DMA_SIZE			2048/*ldf 20230824 add:: 单个tx描述符dma传输内存的大小，要大于1514*/

#define CONFIG_ETH_BUFSIZE		2048
#define TX_TOTAL_BUFSIZE		(CONFIG_ETH_BUFSIZE * GEM_TX_DESC_CNT)
#define RX_TOTAL_BUFSIZE		(CONFIG_ETH_BUFSIZE * GEM_RX_DESC_CNT)

//#define ETH_MAC_ADDR_SIZE   6	    /* MAC地址长度,固定为6 */
#define GEM_MTU                	1500
#define GEM_JUMBO_MTU         	9000
#define ETH_MTU             	1500	/* 普通帧最大长度 */
#define ETH_JUMBO_MTU       	8982	/* 巨帧最大长度 */
#define ETH_HDR_SIZE        	14		/* 标准以太网头长度 */
#define ETH_TRL_SIZE        	4		/* 标准以太网尾长度 (FCS) */
#define ETH_HDR_VLAN_SIZE   	18		/* 标准以太网虚网长度 */
#define ETH_MAX_FRAME_SIZE       (ETH_MTU + ETH_HDR_SIZE + ETH_TRL_SIZE)
#define ETH_MAX_VLAN_FRAME_SIZE  (ETH_MTU + ETH_HDR_VLAN_SIZE + ETH_TRL_SIZE)
#define ETH_MAX_JUMBO_FRAME_SIZE (ETH_JUMBO_MTU + ETH_HDR_SIZE + ETH_TRL_SIZE)


#define GEM_MULTICAST_ENTRIES		31
#define GEM_DFT_BUFS              4096

#define GEM_ADDR_LO(addr)	((unsigned int)((unsigned long)(addr) & 0xFFFFFFFF))
#define GEM_ADDR_HI(addr)	((unsigned int)(((unsigned long)(addr) >> 32) & 0xFFFFFFFF))

#define GEM_INC_DESC(x, y)     (x) = ((x + 1) & (y - 1))


#define GEM_PROMISC_OPTION               0x00000001
#define GEM_JUMBO_OPTION                 0x00000002
#define GEM_VLAN_OPTION                  0x00000004
#define GEM_FLOW_CONTROL_OPTION          0x00000008
#define GEM_FCS_STRIP_OPTION             0x00000010
#define GEM_FCS_INSERT_OPTION            0x00000020
#define GEM_LENTYPE_ERR_OPTION           0x00000040
#define GEM_TRANSMITTER_ENABLE_OPTION    0x00000080
#define GEM_RECEIVER_ENABLE_OPTION       0x00000100
#define GEM_BROADCAST_OPTION             0x00000200
#define GEM_MULTICAST_OPTION        	 0x00000400
#define GEM_DEFAULT_OPTIONS              \
			   (GEM_FLOW_CONTROL_OPTION |   \
				GEM_BROADCAST_OPTION |  	 \
				GEM_FCS_INSERT_OPTION |  	 \
				GEM_FCS_STRIP_OPTION |    	 \
				GEM_LENTYPE_ERR_OPTION |     \
				GEM_TRANSMITTER_ENABLE_OPTION |   \
				GEM_RECEIVER_ENABLE_OPTION )

#define GEM_EIOCGPKTTW  130
#define GEM_EIOCSMPSPD  133

typedef enum {
	PHY_INTERFACE_MODE_MII,
	PHY_INTERFACE_MODE_GMII,
	PHY_INTERFACE_MODE_SGMII,
	PHY_INTERFACE_MODE_TBI,
	PHY_INTERFACE_MODE_RMII,
	PHY_INTERFACE_MODE_RGMII,
	PHY_INTERFACE_MODE_RGMII_ID,
	PHY_INTERFACE_MODE_RGMII_RXID,
	PHY_INTERFACE_MODE_RGMII_TXID,
	PHY_INTERFACE_MODE_RTBI,
	PHY_INTERFACE_MODE_XGMII,
	PHY_INTERFACE_MODE_NONE	/* Must be last */
} phy_interface_t;

/*********************************************************************************************************
  GEM 控制器 FIFO/DMA 工作描述符结构体
*********************************************************************************************************/
typedef struct gem_rx_desc
{
	volatile u32 rxBufAddr;
	volatile u32 rxStatus;
#ifdef GEM_DMA_64BIT
	volatile u32 rxBufAddrHi;
	volatile u32 rxReserved;
#endif
} GEM_RX_DESC;

typedef struct gem_tx_desc
{
	volatile u32 txBufAddr;
	volatile u32 txStatus;
#ifdef GEM_DMA_64BIT
	volatile u32 txBufAddrHi;
	volatile u32 txReserved;
#endif
} GEM_TX_DESC;

/* GEM 描述符bit定义 */
#define GEM_RX_ADDRE_MASK        (0xFFFFFFFC)
#define GEM_RX_BUF_LENTH_MASK    (0x1FFF)//(0x7FF << 0) /*ldf 20230802 modified*/
#define GEM_RX_OWNERSHIP_BIT     (1 <<  0)
#define GEM_RX_WRAP_BIT          (1 <<  1)
#define GEM_RX_SOF_BIT           (1 << 14)
#define GEM_RX_EOF_BIT           (1 << 15)
#define GEM_RX_CSUM_BIT          (3 << 22)

#define GEM_TX_USED_BIT          (1 << 31)
#define GEM_TX_WRAP_BIT          (1 << 30)
#define GEM_TX_RLE_BIT           (1 << 29)
#define GEM_TX_UND_BIT           (1 << 28)
#define GEM_TX_ERR_BIT           (1 << 27)
#define GEM_TX_ERR_BITS          (GEM_TX_RLE_BIT | GEM_TX_UND_BIT | GEM_TX_ERR_BIT)
#define GEM_TX_LAST_BIT          (1 << 15)
#define GEM_TX_BUF_LENTH_MASK	 (0x3FFF << 0)

#define CDNS_FRAME_SIZE             1536
#define CDNS_BUF_ALIGN              8
#define CDNS_RX_PAD                 0
#define CDNS_TX_PAD                 0
#define CDNS_RX_OFT                 0
#define CDNS_RX_PKT_SIZE            (CDNS_FRAME_SIZE + CDNS_RX_PAD)
#define CDNS_TX_PKT_SIZE            (CDNS_FRAME_SIZE + CDNS_TX_PAD)


/*END设备*/
typedef struct
{
	END_OBJ     		gemEndObj;       /*vxWorks指定其为第一项*/

	/*下面是本地驱动需要用的数据结构,一般需要的定义如下,如果程序需要,可以添加*/
	u8					gemMacAddr[ETHER_ADDR_LEN];

//	SEM_ID			semConfig;      /*配置锁,在start,stop,ioctl,restart和unload函数保证不冲突*/
	pthread_mutex_t		gemDevSem;
	SEM_ID			 	semSend;		/*发送锁,一定要用*/
	CACHE_FUNCS 	 	cacheFuncs;		/*cache 的函数组,如果CPU不能保证cache一致性,需要通过这个函数组来调用cache相关的操作*/

	atomic_t		 	gemRxPending;
	atomic_t		 	gemTxPending;
	atomic_t		 	gemIntPending;

	/*下面添加和硬件直接相关的部分*/
	void 		       *gemBaseAddr;
	int 			 	unit;
	int 			 	irq;
	char 		     	gemName[16];
	BOOL 			 	is_init;
	int 			 	gemMaxMtu;
	u32 			 	physpeed;
	phy_interface_t  	interface;
	u32 			 	max_speed;
	BOOL			 	int_pcs;
	BOOL 			 	dma_64bit;
	int 	         	bStarted;		//网卡是否开始工作

	unsigned int 	 	gemRxIdx;
	unsigned int 	 	gemTxFree;
	unsigned int 	 	gemTxProd;
	unsigned int 	 	gemTxCons;
	unsigned int        gemTxStall;
//	pthread_mutex_t	 gemDevSem;
	GEM_TX_DESC        *gemTxDescMem;
	GEM_RX_DESC        *gemRxDescMem;
    M_BLK_ID		 	gemTxMblk[GEM_TX_DESC_CNT];
    M_BLK_ID		 	gemRxMblk[GEM_RX_DESC_CNT];
//    void 			   *gemTxDmaAddr;//发送DMA地址
    void 			   *gemTxDmaAddr2[GEM_TX_DESC_CNT];/*ldf 20230728 add*/
} GEM_DRV_CTRL;



#define _WRS_VXWORKS_MAJOR 6

#if (_WRS_VXWORKS_MAJOR >= 6)
#  define COUNT_IN_PKT(MblkPtr)      END_ERR_ADD(&pDrvCtrl->gemEndObj, MIB2_IN_UCAST, 1)
#  define COUNT_OUT_PKT(MblkPtr)     END_ERR_ADD(&pDrvCtrl->gemEndObj, MIB2_OUT_UCAST, 1)
#  define COUNT_IN_DISCARD(MblkPtr)  endM2Packet(&pDrvCtrl->gemEndObj, (MblkPtr), M2_PACKET_IN_DISCARD)
#  define COUNT_OUT_DISCARD(MblkPtr) endM2Packet(&pDrvCtrl->gemEndObj, (MblkPtr), M2_PACKET_OUT_DISCARD)
#  define COUNT_IN_ERROR()           endM2Packet(&pDrvCtrl->gemEndObj, NULL,      M2_PACKET_IN_ERROR)
#  define COUNT_OUT_ERROR(MblkPtr)   endM2Packet(&pDrvCtrl->gemEndObj, (MblkPtr), M2_PACKET_OUT_ERROR)
#else
#  define COUNT_IN_PKT(MblkPtr)      END_ERR_ADD(&DriverPtr->VxEnd, MIB2_IN_UCAST, 1)
#  define COUNT_OUT_PKT(MblkPtr)     END_ERR_ADD(&DriverPtr->VxEnd, MIB2_OUT_UCAST, 1)
#  define COUNT_IN_DISCARD(MblkPtr)
#  define COUNT_OUT_DISCARD(MblkPtr)
#  define COUNT_IN_ERROR()           END_ERR_ADD(&DriverPtr->VxEnd, MIB2_IN_ERRS, 1)
#  define COUNT_OUT_ERROR(MblkPtr)   END_ERR_ADD(&DriverPtr->VxEnd, MIB2_OUT_ERRS, 1)
#endif

#define END_HADDR(EndPtr) \
                ((EndPtr)->mib2Tbl.ifPhysAddress.phyAddress)

#define END_HADDR_LEN(EndPtr) \
                ((EndPtr)->mib2Tbl.ifPhysAddress.addrLength)

#define DEFAULT_LOAN_RXBUF_FACTOR       4
#define GEM_TUPLE_CNT          16384//8192//1536//576
#define GEM_TUPLE_GET(pNetPool) (mBlkGet((pNetPool), M_DONTWAIT, MT_DATA))

#define		MIPS_ADDR		0x9000000000000000


#define CONFIG_PHYS_64BIT
//#define CONFIG_MIPS64

#define STAT_SIZE	44

#define SPEED_10    10
#define SPEED_100   100
#define SPEED_1000  1000

/* Duplex, half or full. */
#define DUPLEX_HALF		0x00
#define DUPLEX_FULL		0x01


#define ARCH_DMA_MINALIGN	128
#define PKTSIZE_ALIGN		1536

/* 2MB granularity */
#define MMU_SECTION_SHIFT	21
#define MMU_SECTION_SIZE	(1 << MMU_SECTION_SHIFT)

/* Indicates what features are supported by the interface. */
#define SUPPORTED_10baseT_Half		(1 << 0)
#define SUPPORTED_10baseT_Full		(1 << 1)
#define SUPPORTED_100baseT_Half		(1 << 2)
#define SUPPORTED_100baseT_Full		(1 << 3)
#define SUPPORTED_1000baseT_Half	(1 << 4)
#define SUPPORTED_1000baseT_Full	(1 << 5)
#define SUPPORTED_Autoneg			(1 << 6)
#define SUPPORTED_TP				(1 << 7)
#define SUPPORTED_AUI				(1 << 8)
#define SUPPORTED_MII				(1 << 9)
#define SUPPORTED_FIBRE				(1 << 10)
#define SUPPORTED_BNC				(1 << 11)
#define SUPPORTED_10000baseT_Full	(1 << 12)

/* Bit/mask specification */
#define HR3_GEM_PHYMNTNC_OP_MASK	0x40020000 /* operation mask bits */
#define HR3_GEM_PHYMNTNC_OP_R_MASK	0x20000000 /* read operation */
#define HR3_GEM_PHYMNTNC_OP_W_MASK	0x10000000 /* write operation */
#define HR3_GEM_PHYMNTNC_PHYAD_SHIFT_MASK	23 /* Shift bits for PHYAD */
#define HR3_GEM_PHYMNTNC_PHREG_SHIFT_MASK	18 /* Shift bits for PHREG */

#define HR3_GEM_RXBUF_EOF_MASK		0x00008000 /* End of frame. */
#define HR3_GEM_RXBUF_SOF_MASK		0x00004000 /* Start of frame. */
#define HR3_GEM_RXBUF_LEN_MASK		0x00003FFF /* Mask for length field */

#define HR3_GEM_RXBUF_WRAP_MASK	0x00000002 /* Wrap bit, last BD */
#define HR3_GEM_RXBUF_NEW_MASK		0x00000001 /* Used bit.. */
#define HR3_GEM_RXBUF_ADD_MASK		0xFFFFFFFC /* Mask for address */

/* Wrap bit, last descriptor */
#define HR3_GEM_TXBUF_WRAP_MASK	0x40000000
#define HR3_GEM_TXBUF_LAST_MASK	0x00008000 /* Last buffer */
#define HR3_GEM_TXBUF_USED_MASK	0x80000000 /* Used by Hw */

#define HR3_GEM_NWCTRL_TXEN_MASK	0x00000008 /* Enable transmit */
#define HR3_GEM_NWCTRL_RXEN_MASK	0x00000004 /* Enable receive */
#define HR3_GEM_NWCTRL_MDEN_MASK	0x00000010 /* Enable MDIO port */
#define HR3_GEM_NWCTRL_STARTTX_MASK	0x00000200 /* Start tx (tx_go) */

#define HR3_GEM_NWCFG_SPEED100		0x00000001 /* 100 Mbps operation */
#define HR3_GEM_NWCFG_SPEED1000		0x00000400 /* 1Gbps operation */
#define HR3_GEM_NWCFG_FDEN			0x00000002 /* Full Duplex mode */
#define HR3_GEM_NWCFG_FSREM			0x00020000 /* FCS removal */
#define HR3_GEM_NWCFG_SGMII_ENBL	0x08000000 /* SGMII Enable */
#define HR3_GEM_NWCFG_PCS_SEL		0x00000800 /* PCS select */
#define HR3_GEM_NWCFG_COPY_ALL		0x00000010 /*COPY ALL FRAMES*/
#define HR3_GEM_NWCFG_BROADCAST     0x00000020
#define HR3_GEM_NWCFG_UNICAST       0x00000080
#define HR3_GEM_NWCFG_MULTICAST     0x00000040

#ifdef CONFIG_MIPS64
#define HR3_GEM_NWCFG_MDCCLKDIV	0x00100000 /* Div pclk by 64, max 160MHz */
#else
#define HR3_GEM_NWCFG_MDCCLKDIV	0x000c0000 /* Div pclk by 48, max 120MHz */
#endif

#ifdef CONFIG_MIPS64
# define HR3_GEM_DBUS_WIDTH		(1 << 21) /* 64  bit bus */
#else
# define HR3_GEM_DBUS_WIDTH		(1 << 22) /* 128 bit bus */
#endif

/*
#define HR3_GEM_NWCFG_INIT	   (HR3_GEM_DBUS_WIDTH | \
								HR3_GEM_NWCFG_FDEN | \
								HR3_GEM_NWCFG_FSREM | \
								HR3_GEM_NWCFG_MDCCLKDIV )
*/

#define HR3_GEM_NWSR_MDIOIDLE_MASK	0x00000004 /* PHY management idle */

#define HR3_GEM_DMACR_BLENGTH		0x00000004 /* INCR4 AHB bursts,  Attempt to use bursts of up to 4. */
/* Use full configured addressable space (8 Kb) */
#define HR3_GEM_DMACR_RXSIZE		0x00000300
/* Use full configured addressable space (4 Kb) */
#define HR3_GEM_DMACR_TXSIZE		0x00000400
/* Set with binary 00011000 to use 1536 byte(1*max length frame/buffer) */
#define HR3_GEM_DMACR_RXBUF		0x00180000  //rx_buf_size


#if defined(CONFIG_PHYS_64BIT)
# define HR3_GEM_DMA_BUS_WIDTH		(0x1 << 30) /* 64 bit bus */
#else
# define HR3_GEM_DMA_BUS_WIDTH		(0 << 30) /* 32 bit bus */
#endif

#define HR3_GEM_DMACR_INIT	   (HR3_GEM_DMACR_BLENGTH | \
								HR3_GEM_DMACR_RXSIZE | \
								HR3_GEM_DMACR_TXSIZE | \
								HR3_GEM_DMACR_RXBUF | \
								HR3_GEM_DMA_BUS_WIDTH )

#define HR3_GEM_TSR_DONE		0x00000020 /* Tx done mask */

#define HR3_GEM_PCS_CTL_ANEG_ENBL	0x1000//开启自协商

#define HR3_GEM_DCFG_DBG6_DMA_64B	(0x1 << 23)

/* Use MII register 1 (MII status register) to detect PHY */
#define PHY_DETECT_REG  1

/* Mask used to verify certain PHY features (or register contents)
 * in the register above:
 *  0x1000: 10Mbps full duplex support
 *  0x0800: 10Mbps half duplex support
 *  0x0008: Auto-negotiation support
 */
#define PHY_DETECT_MASK 0x1808

/* TX BD status masks */
#define HR3_GEM_TXBUF_FRMLEN_MASK	0x000007ff
#define HR3_GEM_TXBUF_EXHAUSTED	0x08000000
#define HR3_GEM_TXBUF_UNDERRUN		0x10000000

/* Clock frequencies for different speeds */
#define HR3_GEM_FREQUENCY_10	2500000UL
#define HR3_GEM_FREQUENCY_100	25000000UL
#define HR3_GEM_FREQUENCY_1000	125000000UL

#define RX_BUF 1024//32
/* Page table entries are set to 1MB, or multiples of 1MB
 * (not < 1MB). driver uses less bd's so use 1MB bdspace.
 */
#define BD_SPACE	0x100000
/* BD separation space */
#define BD_SEPRN_SPACE	(RX_BUF * sizeof(GEM_BD))

/* Setup the first free TX descriptor */
#define TX_FREE_DESC	2


typedef struct gem_device_info
{
	void* 		gemBaseAddr;
	int			irq;
	void*		gemTxDescMemAddr;/*ldf 20230824 add:: 预留的TX描述符内存地址,uncached*/
	void*		gemRxDescMemAddr;/*ldf 20230824 add:: 预留的RX描述符内存地址,uncached*/
	void*		gemTxDmaMemAddr;/*ldf 20230824 add:: 预留的TX DMA内存地址,uncached*/
}GEM_DEV_INFO;
//extern GEM_DEV_INFO gem_resources[GEM_MAX_DEV];

#if 1/**********************cache add**************************************/

/* copy from CacheLib.h TODO: refactor later */
#define	_INSTRUCTION_CACHE 	0	/* Instruction Cache(s) */
#define	_DATA_CACHE		1	/* Data Cache(s) */

//typedef enum				/* CACHE_TYPE */
//{
//    INSTRUCTION_CACHE = _INSTRUCTION_CACHE,
//    DATA_CACHE        = _DATA_CACHE
//} CACHE_TYPE;
/* Cache flush and invalidate support for general use and drivers */

//typedef	struct					/* Cache程序指针 */
//{
//    FUNCPTR	enableRtn;			/* 使能cacheEnable() */
//    FUNCPTR	disableRtn;			/* 非能cacheDisable() */
//    FUNCPTR	lockRtn;			/* 锁cacheLock() */
//    FUNCPTR	unlockRtn;			/* 开cacheUnlock() */
//    FUNCPTR	flushRtn;			/* 刷cacheFlush_intel82574() */
//    FUNCPTR	invalidateRtn;		/* 无效cacheInvalidate_intel82574() */
//    FUNCPTR	clearRtn;			/* 清除cacheClear() */
//    FUNCPTR	textUpdateRtn;		/* 更新cacheTextUpdate() */
//    FUNCPTR	pipeFlushRtn;		/* cachePipeFlush() */
//    FUNCPTR	dmaMallocRtn;		/* cacheDmaMalloc_intel82574() */
//    FUNCPTR	dmaFreeRtn;			/* cacheDmaFree() */
//    FUNCPTR	dmaVirtToPhysRtn;	/* 虚拟到物理virtual-to-Physical Translation */
//    FUNCPTR	dmaPhysToVirtRtn;	/* 物理到虚拟physical-to-Virtual Translation */
//} CACHE_LIB;

//typedef	struct				/* Driver Cache Routine Pointers */
//{
//    FUNCPTR	flushRtn;		/* cacheFlush_intel82574() */
//    FUNCPTR	invalidateRtn;		/* cacheInvalidate_intel82574() */
//    FUNCPTR	virtToPhysRtn;		/* Virtual-to-Physical Translation */
//    FUNCPTR	physToVirtRtn;		/* Physical-to-Virtual Translation */
//} CACHE_FUNCS;

extern CACHE_LIB	cacheLib;
extern CACHE_FUNCS	cacheNullFuncs;	/* functions for non-cached memory */
extern CACHE_FUNCS	cacheDmaFuncs;	/* functions for dma memory */
extern CACHE_FUNCS	cacheUserFuncs;	/* functions for user memory */

/* Cache macros */

#define	CACHE_TEXT_UPDATE(adrs, bytes)	\
        ((cacheLib.textUpdateRtn == NULL) ? OK :	\
	 (cacheLib.textUpdateRtn) ((adrs), (bytes)))

#define	CACHE_PIPE_FLUSH()	\
        ((cacheLib.pipeFlushRtn == NULL) ? OK :	\
	 (cacheLib.pipeFlushRtn) ())


#define	CACHE_DRV_FLUSH(pFuncs, adrs, bytes)	\
        (((pFuncs)->flushRtn == NULL) ? OK :	\
         ((pFuncs)->flushRtn) (DATA_CACHE, (adrs), (bytes)))

#define	CACHE_DRV_INVALIDATE(pFuncs, adrs, bytes)	\
        (((pFuncs)->invalidateRtn == NULL) ? OK :	\
         ((pFuncs)->invalidateRtn) (DATA_CACHE, (adrs), (bytes)))

#define	CACHE_DRV_VIRT_TO_PHYS(pFuncs, adrs)	\
        (((pFuncs)->virtToPhysRtn == NULL) ? (void *) (adrs) :	\
	 (void *) (((pFuncs)->virtToPhysRtn) (adrs)))

#define	CACHE_DRV_PHYS_TO_VIRT(pFuncs, adrs)	\
        (((pFuncs)->physToVirtRtn == NULL) ? (void *) (adrs) :	\
	 ((void *) ((pFuncs)->physToVirtRtn) (adrs)))

#define	CACHE_DRV_IS_WRITE_COHERENT(pFuncs)	\
	((pFuncs)->flushRtn == NULL)

#define	CACHE_DRV_IS_READ_COHERENT(pFuncs)	\
	((pFuncs)->invalidateRtn == NULL)


#define	CACHE_DMA_FLUSH(adrs, bytes)		\
	CACHE_DRV_FLUSH (&cacheDmaFuncs, (adrs), (bytes))

#define	CACHE_DMA_INVALIDATE(adrs, bytes)	\
	CACHE_DRV_INVALIDATE (&cacheDmaFuncs, (adrs), (bytes))

#define	CACHE_DMA_VIRT_TO_PHYS(adrs)	\
	CACHE_DRV_VIRT_TO_PHYS (&cacheDmaFuncs, (adrs))

#define	CACHE_DMA_PHYS_TO_VIRT(adrs)	\
	CACHE_DRV_PHYS_TO_VIRT (&cacheDmaFuncs, (adrs))

#define	CACHE_DMA_IS_WRITE_COHERENT()	\
	CACHE_DRV_IS_WRITE_COHERENT (&cacheDmaFuncs)

#define	CACHE_DMA_IS_READ_COHERENT()	\
	CACHE_DRV_IS_READ_COHERENT (&cacheDmaFuncs)


#define	CACHE_USER_FLUSH(adrs, bytes)	\
	CACHE_DRV_FLUSH (&cacheUserFuncs, (adrs), (bytes))

#define	CACHE_USER_INVALIDATE(adrs, bytes)	\
	CACHE_DRV_INVALIDATE (&cacheUserFuncs, (adrs), (bytes))

#define	CACHE_USER_IS_WRITE_COHERENT()	\
	CACHE_DRV_IS_WRITE_COHERENT (&cacheUserFuncs)

#define	CACHE_USER_IS_READ_COHERENT()	\
	CACHE_DRV_IS_READ_COHERENT (&cacheUserFuncs)


extern FUNCPTR		cacheDmaMallocRtn;
extern FUNCPTR		cacheDmaFreeRtn;
extern BOOL		cacheDataEnabled;
extern BOOL		cacheMmuAvailable;

#endif/******************************************************************/



#endif /* __CDNS_GEM_H__ */
/*********************************************************************************************************/

