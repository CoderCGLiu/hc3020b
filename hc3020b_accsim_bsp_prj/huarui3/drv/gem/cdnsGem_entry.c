#include <vxWorks.h>
#include <semLib.h>
#include <sys/types.h>
#include <net_types.h>
#include <end.h>
#include <drv/net_board.h>
#include <reworks/printk.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "cdnsGem.h"

int usr_net_lib_init_gem(int unit);
extern void net_drv_init_gem(int unit);

#define GEM_ENET0                0x00
#define GEM_ENET1                0xA0
#define GEM_ENET2                0x1E

#define GEM_CUST_ENET3_0            0xA0  /* Customer portion of MAC address */
#define GEM_CUST_ENET3_1            0xA1
#define GEM_CUST_ENET3_2            0xA2
#define GEM_CUST_ENET3_3            0xA3
#define GEM_CUST_ENET3_4            0xA4
#define GEM_CUST_ENET3_5            0xA5
#define GEM_CUST_ENET3_6            0xA6
#define GEM_CUST_ENET3_7            0xA7
#define GEM_CUST_ENET4              0xAA
#define GEM_CUST_ENET5              0xA0
#define GEM_OFFSET_VALUE			 0
#define GEM_JUMBO_MTU_VALUE         (0)

//static u8 gem_enet_addr [GEM_MAX_DEV][ETHER_ADDR_LEN] = {
//		{ GEM_CUST_ENET5, GEM_CUST_ENET4, GEM_CUST_ENET3_4, GEM_ENET2, GEM_ENET1, GEM_ENET0 },
//		{ GEM_CUST_ENET5, GEM_CUST_ENET4, GEM_CUST_ENET3_5, GEM_ENET2, GEM_ENET1, GEM_ENET0 }
//};


/* Default User's flags  */
#ifndef GEM_USR_FLAG
#define GEM_USR_FLAG        0
#endif /* GEM_USR_FLAG */

#define NONE (-1)

extern END_OBJ *gemEndLoad(char *loadStr);

/******************************************************************************
 *
 * create load string and load a gem driver.
 *
 */
END_OBJ * sysGemEndLoad(char * pParamStr, void * unused )
{
	END_OBJ * pEnd;
	char paramStr[NETDRV_INIT_STR_MAX];
 
	if (strlen(pParamStr) == 0)
	{
		/* PASS (1)
		 * The driver load routine returns the driver name in <pParamStr>.
		 */
//		printk("%s,%d \n",__FUNCTION__,__LINE__);
		pEnd = gemEndLoad(pParamStr);
	}
	else
	{
		/* PASS (2)
		 * The NET <unit> number is prepended to <pParamStr>.  Construct
		 * the rest of the driver load string based on physical devices
		 * discovered in sys_pci_init().  When this routine is called
		 * to process a particular NET <unit> number, use the NET <unit> as
		 * an index into the PCI "resources" table to build the driver
		 * parameter string.
		 */

//		printk("%s,%d pParamStr[%s]\n",__FUNCTION__,__LINE__,pParamStr);
		char *holder = NULL;
		int unit = atoi(strtok_r(pParamStr, ":", &holder));
//		printk("%s,%d unit = %d\n",__FUNCTION__,__LINE__,unit);
		/* is there a PCI resource associated with this END unit ? */
		if (unit >= GEM_MAX_DEV)
		{
			printk("[WARNING]Over the limits of net unit!\n");
			return NULL;
		}

		/* finish off the initialization parameter string */
		sprintf(paramStr, "%d:%d",
				unit, /* NET unit number */
				GEM_MTU);

		if ((pEnd = gemEndLoad(paramStr)) == (END_OBJ *) NULL)
		{
			printk("[ERROR]gemEndLoad fails to load gem%d\n", unit);
		}

	}
	return (pEnd);
}

#define SYS_BUFF_LOAN_PRI   1
#define SYS_BUFF_LOAN_SEC   2
#define SYS_BUFF_LOAN_TER   3
#define SYS_BUFF_LOAN_QUA   4
#define SYS_BUFF_LOAN_QUI   5
#define SYS_BUFF_LOAN_SEN   6

#define LOAD_FUNC_SEN      	sysGemEndLoad
#define BUFF_LOAN_SEN       SYS_BUFF_LOAN_SEN

#define LOAD_FUNC_QUI       sysGemEndLoad
#define BUFF_LOAN_QUI       SYS_BUFF_LOAN_QUI

/* The END_LOAD_STRING is defined empty and created dynamicaly */
#define LOAD_STRING 		""      /* created in sys<device>End.c */

/* define IP_MAX_UNITS to the actual number in the table. */
#ifndef IP_MAX_UNITS
#define IP_MAX_UNITS            (NELEMENTS(end_dev_tbl_gem) - 1)
#endif  /* ifndef IP_MAX_UNITS */

#define TBL_NET NULL

static NET_TBL_ENTRY end_dev_tbl_gem[] =
{
		{0, LOAD_FUNC_QUI, LOAD_STRING, BUFF_LOAN_QUI, NULL, FALSE},
		{1, LOAD_FUNC_QUI, LOAD_STRING, BUFF_LOAN_QUI, NULL, FALSE},
		{2, LOAD_FUNC_QUI, LOAD_STRING, BUFF_LOAN_QUI, NULL, FALSE},
		{3, LOAD_FUNC_QUI, LOAD_STRING, BUFF_LOAN_QUI, NULL, FALSE},
		{0, TBL_NET, NULL, 0, NULL, FALSE} 	/* must be last */
};

int usr_net_lib_init_gem(int unit)
{
	NET_TBL_ENTRY *pDevTbl;
	void *pCookie = NULL;

	if(unit>= IP_MAX_UNITS)
	{
		printk("[ERROR]unit is %d, max unit is %d.\n",unit,IP_MAX_UNITS-1);
		return -1;
	}
	pDevTbl = &end_dev_tbl_gem[unit];

	if ((!pDevTbl->processed) && (pDevTbl->net_load_func != TBL_NET))
	{
		printk("start to process gem %d\n",pDevTbl->unit);
		pCookie = net_dev_load(pDevTbl->unit, pDevTbl->net_load_func,
				pDevTbl->net_load_string, pDevTbl->net_loan, pDevTbl->pBSP);
		if (pCookie == NULL)
		{
			printk("[ERROR]net_dev_load failed!\n");
			return -1;
		}
		else
		{
			if (net_dev_start(pCookie) == ERROR)
			{
				printk("[ERROR]net_dev_start failed!\n");
				return -1;
			}
			else
			{
				pDevTbl->processed = TRUE;
			}
		}
		printk("[OK]net_dev_load %d is successful!\n",pDevTbl->unit);
	}

	return (OK);
}


#ifdef USE_REWORKS_IPNET_69
#define    	net_drv_lib_init 				endLibInit
#endif


void net_drv_init_gem(int unit)
{
	static int net_drv_init_flag = 0;
	if(net_drv_init_flag == 0)
	{
		net_drv_lib_init();
		net_drv_init_flag = 1;
	}

	usr_net_lib_init_gem(unit);

	return;
}


#if 0 /*ldf 20230510 add*/
/* ɨ��PHY����ӡPHY��ַ */
int scan_phyaddr(int unit)
{
	u16 data2, data3;
	u32 baseaddr;
	int phyaddr;
	if(0 == unit)
		baseaddr = GMAC0_BASE;
	else if(1 == unit)
		baseaddr = GMAC1_BASE;
	for(phyaddr = 0; phyaddr < 32; phyaddr++)
	{
		synopGMAC_read_phy_reg((u32*)baseaddr, phyaddr, 2, &data2);
		synopGMAC_read_phy_reg((u32*)baseaddr, phyaddr, 3, &data3);
		if(data2 != 0xffff && data3 != 0xffff)
		{
			printk("gmac%d phyaddr:%d\n",(0==unit ? 0 : 1), phyaddr);
			printk("gmac%d phyid:0x%x-0x%x\n",(0==unit ? 0 : 1), data2, data3);
			break;
		}
	}
	return phyaddr;
}
/* ��MII�Ĵ��� */
u16 read_rpc101t_MIIreg(int unit, int mdio_page, u32 reg)
{
	u16 data;
	int phyAddr;
	u32 baseaddr;
	
	if(unit == 0)
		baseaddr = GMAC0_BASE;
	else if(unit == 1)
		baseaddr = GMAC1_BASE;
	
	phyAddr = scan_phyaddr(unit);
	
	if(0 == mdio_page)
	{
		synopGMAC_read_phy_reg((u32 *)baseaddr, phyAddr, reg, &data);
	}else
	{
		synopGMAC_write_phy_reg((u32 *)baseaddr, phyAddr, 0x1e, 0x1);
		synopGMAC_read_phy_reg((u32 *)baseaddr, phyAddr, reg, &data);
		synopGMAC_write_phy_reg((u32 *)baseaddr, phyAddr, 0x1e, 0x0);
	}
	
	printk("[Page%d MIIreg 0x%02x]: data = 0x%x\n", mdio_page, reg, data);
	
	return data;
}
/* дMII�Ĵ��� */
void write_rpc101t_MIIreg(int unit, int mdio_page, u32 reg, u16 data)
{
	int phyAddr;
	u32 baseaddr;
	u16 temp;
	
	if(unit == 0)
		baseaddr = GMAC0_BASE;
	else if(unit == 1)
		baseaddr = GMAC1_BASE;
	
	phyAddr = scan_phyaddr(unit);
	
	if(0 == mdio_page)/*Page0 MIIreg, ����zgphy�Ĵ���,0x00-0x1f*/
	{
		synopGMAC_write_phy_reg((u32 *)baseaddr, phyAddr, reg, data);
	}
	else/*Page1 MIIreg, ����rgmii��led���á�gephy�ӿ����üĴ���,0x10-0x1d*/
	{
		synopGMAC_write_phy_reg((u32 *)baseaddr, phyAddr, 0x1e, 0x1);
		synopGMAC_write_phy_reg((u32 *)baseaddr, phyAddr, reg, data);
		synopGMAC_write_phy_reg((u32 *)baseaddr, phyAddr, 0x1e, 0x0);
	}
	
	synopGMAC_read_phy_reg((u32 *)baseaddr, phyAddr, MII_BMCR, &temp);
	synopGMAC_write_phy_reg((u32 *)baseaddr, phyAddr, MII_BMCR, temp|0x8000);
		
	return;
}

/* ��ӡȫ��MII�Ĵ��� */
void show_rpc101t_MIIregs(int unit)
{
	u16 data;
	u32 reg;
	u32 baseaddr;
	int phyAddr;
	phyAddr = scan_phyaddr(unit);
	
	if(unit == 0)
		baseaddr = GMAC0_BASE;
	else if(unit == 1)
		baseaddr = GMAC1_BASE; 

	/*Page0 MIIreg, ����zgphy�Ĵ���,0x00-0x1f*/
	for(reg = 0x0; reg <= 0x1f; reg++)
	{
		synopGMAC_read_phy_reg((u32 *)baseaddr, phyAddr, reg, &data);			
		printk("[Page0 MIIreg 0x%02x]: data = 0x%x\n", reg, data);
	}

	printk("\n");
	/*Page1 MIIreg, ����rgmii��led���á�gephy�ӿ����üĴ���,0x10-0x1d*/
	synopGMAC_write_phy_reg((u32 *)baseaddr, phyAddr, 0x1e, 0x1);
	for(reg = 0x10; reg <= 0x1d; reg++)
	{
		synopGMAC_read_phy_reg((u32 *)baseaddr, phyAddr, reg, &data);			
		printk("[Page1 MIIreg 0x%02x]: data = 0x%x\n", reg, data);
	}
	synopGMAC_write_phy_reg((u32 *)baseaddr, phyAddr, 0x1e, 0x0);

	return;
}
#endif

