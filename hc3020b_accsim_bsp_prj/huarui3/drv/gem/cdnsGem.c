/**********************************************
 * cdnsGem.c
 * create by ymk  2022.01.18
 */

#include <vxWorks.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <netLib.h>
#include <taskLib.h>
#include <etherMultiLib.h>
#include <reworks/printk.h>
#include <endLib.h>
#include <netBufLib.h>
#include <cacheLib.h>
#include <tickLib.h>
#include <irq.h>
#include <reworks/cache.h>
#include <drv/net_board.h>
#include <endCommon.h>
#include <hr3_intc_defs.h>
#include "cdnsGem.h"
//#include "cdnsPhy.h"

#include "../../h/tool/hr3Macro.h"
#include "../../h/tool/hr3SysTools.h"
#include "../../h/drvCommon.h"

#define MEM_BARRIER_RW1() __asm volatile ("sync" ::: "memory")/*ldf 20230814 add*/

#define GEM_BASE_ADDR0    		0x1f300000
#define GEM_BASE_ADDR1    		0x1f310000
//#define GEM_BASE_ADDR2    		0x1f320000
//#define GEM_BASE_ADDR3    		0x1f330000
#define GEM_BASE_PHYSELF_ADDR   0x1f340000
#define GEM_BASE_PHY_ADDR    	0x1f3c0000

#define GEM_USER_ADDR0    		0x1f301000
#define GEM_USER_ADDR1    		0x1f311000
#define GEM_USER_CTRL0			0x0
#define GEM_USER_CTRL1			0x4

GEM_DEV_INFO gem_resources[] =
{
	{ 
		.gemBaseAddr = (void *)(0x9000000000000000|GEM_BASE_ADDR0),
		.irq = INT_GMAC0,
		.gemTxDescMemAddr = NULL,
		.gemRxDescMemAddr = NULL,
		.gemTxDmaMemAddr = NULL,
	},
	{ 
		.gemBaseAddr = (void *)(0x9000000000000000|GEM_BASE_ADDR1),
		.irq = INT_GMAC1,
		.gemTxDescMemAddr = NULL,
		.gemRxDescMemAddr = NULL,
		.gemTxDmaMemAddr = NULL,
	},
//	{ 
//		.gemBaseAddr = (void *)(0x9000000000000000|GEM_BASE_ADDR2),
//		.irq = INT_GMAC2,
//		.gemTxDescMemAddr = NULL,
//		.gemRxDescMemAddr = NULL,
//		.gemTxDmaMemAddr = NULL,
//	},
//	{ 
//		.gemBaseAddr = (void *)(0x9000000000000000|GEM_BASE_ADDR3),
//		.irq = INT_GMAC3,
//		.gemTxDescMemAddr = NULL,
//		.gemRxDescMemAddr = NULL,
//		.gemTxDmaMemAddr = NULL,
//	},
	/*END*/
};




static unsigned long long gGemIntHandleCount = 0;/*中断处理次数统计*/
static unsigned long long gGemIntCount = 0;//有效中断状态位统计
static unsigned long long gRecvIntCount = 0;//接收完成中断次数
static unsigned long long gRecvOverRunIntCount = 0;//接收溢出中断次数
static unsigned long long gSendIntCount = 0;//发送完成中断次数
static unsigned long long gOtherIntCount = 0;//其他中断次数
static unsigned long long gRecvCount = 0;//有效的接收次数
static unsigned long long gSendCount = 0;//有效的发送次数
static unsigned long long gSendCountTotal = 0;//实际发送处理的总次数
static GEM_DRV_CTRL *g_pDrvCtrl[GEM_MAX_DEV] = {NULL};

static unsigned int g_gemTxDescMemSize = 0;
static unsigned int g_gemRxDescMemSize = 0;
static unsigned int g_gemTxDmaMemSize = 0;

int gem_dev_num_get()
{
	return GEM_MAX_DEV;
}
unsigned int gemTxDescMemSize_get()
{
	unsigned int TxDescMemSize = sizeof(GEM_TX_DESC)*(GEM_TX_DESC_CNT);
	g_gemTxDescMemSize = TxDescMemSize;
	return g_gemTxDescMemSize;
}

unsigned int gemRxDescMemSize_get()
{
	unsigned int RxDescMemSize = sizeof(GEM_RX_DESC)*(GEM_RX_DESC_CNT);
	g_gemRxDescMemSize = RxDescMemSize;
	return g_gemRxDescMemSize;
}

unsigned int gemTxDmaMemSize_get()
{
	unsigned int TxDmaMemSize = (GEM_TX_DMA_SIZE)*(GEM_TX_DESC_CNT);
	g_gemTxDmaMemSize = TxDmaMemSize;
	return g_gemTxDmaMemSize;
}

void* gemTxDescMemAddr_get(int unit)
{
	return gem_resources[unit].gemTxDescMemAddr;
}
void gemTxDescMemAddr_set(int unit, unsigned long long TxDescMemAddr)
{
	gem_resources[unit].gemTxDescMemAddr = (void*)TxDescMemAddr;
}

void* gemRxDescMemAddr_get(int unit)
{
	return gem_resources[unit].gemRxDescMemAddr;
}
void gemRxDescMemAddr_set(int unit, unsigned long long RxDescMemAddr)
{
	gem_resources[unit].gemRxDescMemAddr = (void*)RxDescMemAddr;
}

void* gemTxDmaMemAddr_get(int unit)
{
	return gem_resources[unit].gemTxDmaMemAddr;
}
void gemTxDmaMemAddr_set(int unit, unsigned long long TxDmaMemAddr)
{
	gem_resources[unit].gemTxDmaMemAddr = (void*)TxDmaMemAddr;
}





extern unsigned long long hrKmToPhys(void *addr_mapped);
#define GEM_VIRT_TO_PHYS(x) hrKmToPhys(x)

unsigned long hrCacheToUncache(void * addr_mapped)
{
	return ((unsigned long)addr_mapped | 0xFFFFFFFFA0000000);
}



extern unsigned int sysCpuGetID(void);
extern unsigned int sysGetCabnNum();
extern unsigned int sysGetCaseNum();
extern unsigned int sysGetSlotNum();

static void gemIntHandle(GEM_DRV_CTRL *pDrvCtrl);

static void sysGemStart(GEM_DRV_CTRL *pDrvCtrl);
static void sysGemStop(GEM_DRV_CTRL *pDrvCtrl);
static unsigned char *gemGetMacAddr(GEM_DRV_CTRL *pDrvCtrl);
static void gemSetMacAddr(GEM_DRV_CTRL *pDrvCtrl,unsigned char *MacAddr);
static int gemGetMTU(GEM_DRV_CTRL *pDrvCtrl);
static int IoctlSetDevControl(GEM_DRV_CTRL *pDrvCtrl, int Cmd, caddr_t Data);

static int InitGemDev(GEM_DRV_CTRL *pDrvCtrl);
static void gemTxDescsInit(GEM_DRV_CTRL *pDrvCtrl);
static void gemRxDescsInit(GEM_DRV_CTRL *pDrvCtrl);
static void gemIntEnable(GEM_DRV_CTRL *pDrvCtrl);
static void gemIntDisable(GEM_DRV_CTRL *pDrvCtrl);
static void gemIntClear(GEM_DRV_CTRL *pDrvCtrl);
static void gemEndRxHandle(GEM_DRV_CTRL *pDrvCtrl);
static int gemTbdClean(GEM_DRV_CTRL *pDrvCtrl);
static int gemMemoryInit(GEM_DRV_CTRL *pDrvCtrl);
static void gemEndHashTblPopulate(GEM_DRV_CTRL *pDrvCtrl);
static void gemEndRxConfig(GEM_DRV_CTRL *pDrvCtrl);

/*NET_FUNCS函数组*/
static STATUS gemEndStart(END_OBJ *pEnd);
static STATUS gemEndStop(END_OBJ *pEnd);
static STATUS gemEndUnload(END_OBJ *pEnd);
static STATUS gemEndIoctl(END_OBJ *pEnd, int Cmd, caddr_t Data);
static STATUS gemEndSend(END_OBJ *pEnd, M_BLK_ID MblkPtr);
static STATUS gemEndMCastAddrAdd(END_OBJ *pEnd, char *pAddr);
static STATUS gemEndMCastAddrDel(END_OBJ *pEnd, char *pAddr);
static STATUS gemEndMCastAddrGet(END_OBJ *pEnd, MULTI_TABLE *pTable);
static STATUS gemEndPollSend(END_OBJ *pEnd, M_BLK_ID MblkPtr);
static STATUS gemEndPollRecv(END_OBJ *pEnd, M_BLK_ID MblkPtr);


/*NET_FUNCS 函数组*/
static NET_FUNCS gemEndFuncs =
{
    (FUNCPTR)gemEndStart,
    (FUNCPTR)gemEndStop,
    (FUNCPTR)gemEndUnload,
    (FUNCPTR)gemEndIoctl,
    (FUNCPTR)gemEndSend,
    (FUNCPTR)gemEndMCastAddrAdd,
    (FUNCPTR)gemEndMCastAddrDel,
    (FUNCPTR)gemEndMCastAddrGet,
    (FUNCPTR)gemEndPollSend,
    (FUNCPTR)gemEndPollRecv,
    endEtherAddressForm,
    endEtherPacketDataGet,
    endEtherPacketAddrGet
};

static unsigned char hr3gem_default_macaddr[6] = {0x00, 0xd8, 0x61, 0x1b, 0x64, 0x00};




/*ldf 20230727 add:: debug*/
static void DumpUint8(const unsigned char *pAddr, unsigned int length)
{
    int index = 0;

    printf("pAddr: 0x%016lX, length: %lu = 0x%016lX", (unsigned long)pAddr, length, length);
    for (index = 0; index < length; index++)
    {
        if (index % 0x10 == 0)
        {
            printf("\n0x%08lX: ", index);
        }
        printf("0x%02X ", *(unsigned char *)(pAddr + index));
    } 

    printf("\n");
}





/* 读寄存器接口 */
u32 gemRead32(void *RegAddr)
{
	u32 uiRegVal = *(volatile u32 *)RegAddr;

	return uiRegVal;
}
/* 写寄存器接口 */
void gemWrite32( void *RegAddr, u32 uiRegVal)
{
	*(volatile u32 *)RegAddr = uiRegVal;
}
/* 寄存器bit位置1接口 */
void gemSetBit(void *RegAddr, u32 BitVal)
{
	u32 uiRegVal = gemRead32(RegAddr);
	uiRegVal |= BitVal;
	gemWrite32(RegAddr,uiRegVal);
}
/* 寄存器bit位置0接口 */
void gemClearBit(void *RegAddr, u32 BitVal)
{
	u32 uiRegVal = gemRead32(RegAddr);
	uiRegVal &= (~BitVal);
	gemWrite32(RegAddr,uiRegVal);
}

/* GEM网卡读寄存器接口 */
static inline u32 gemRegRead32(GEM_DRV_CTRL *pDrvCtrl, u32 RegOffset)
{
	if (pDrvCtrl == NULL || pDrvCtrl->gemBaseAddr == NULL)
	{
		return (-1);
	}
	void *RegAddr = pDrvCtrl->gemBaseAddr + RegOffset;
	u32 uiRegVal = 0;
	uiRegVal = *(volatile u32 *)RegAddr;

	return uiRegVal;
}
/* GEM网卡写寄存器接口 */
static inline void gemRegWrite32(GEM_DRV_CTRL *pDrvCtrl, u32 RegOffset, u32 uiRegVal)
{
	if (pDrvCtrl == NULL)
	{
		return;
	}
	void *RegAddr = pDrvCtrl->gemBaseAddr + RegOffset;
	*(volatile u32 *)RegAddr = uiRegVal;

	return;
}
/* GEM网卡寄存器bit位置1接口 */
static void gemRegSetBit(GEM_DRV_CTRL *pDrvCtrl, u32 RegOffset, u32 BitVal)
{
	if (pDrvCtrl == NULL)
	{
		return;
	}
	u32 uiRegVal = gemRegRead32(pDrvCtrl,RegOffset);
	uiRegVal |= BitVal;
	gemRegWrite32(pDrvCtrl,RegOffset,uiRegVal);
}
/* GEM网卡寄存器bit位置0接口 */
static void gemRegClearBit(GEM_DRV_CTRL *pDrvCtrl, u32 RegOffset, u32 BitVal)
{
	if (pDrvCtrl == NULL)
	{
		return;
	}
	u32 uiRegVal = gemRegRead32(pDrvCtrl,RegOffset);
	uiRegVal &= (~BitVal);
	gemRegWrite32(pDrvCtrl,RegOffset,uiRegVal);
}


/* 此接口是按照华创微的要求加的，目的是进行serdes的初始化 */
int init_serdes(int unit)
{
	static int serdes_init_flag = 0;
	unsigned int base_short_addr=GEM_BASE_PHY_ADDR;
	unsigned int base_phyself_addr=GEM_BASE_PHYSELF_ADDR;
	printk("init serdes!unit=%d\n",unit);
	if (serdes_init_flag)
	{
		return 0;
	}
	printk("init serdes2!unit=%d\n",unit);
	switch(unit)
	{
		case 0:
		{
			base_short_addr=GEM_BASE_PHY_ADDR;
			break;
		}
		case 1:
		{
			base_short_addr=GEM_BASE_PHY_ADDR;
			break;
		}
		case 2:
		{
			base_short_addr=GEM_BASE_PHY_ADDR;
			break;
		}
		case 3:
		{
			base_short_addr=GEM_BASE_PHY_ADDR;
			break;
		}
		default:
			return -1;
	}

    //int base_addr;
    //base_addr = base_short_addr;
	printk("init serdes3!unit=%d\n",unit);
	phx_write_u32(base_short_addr+0xc00e*4,0x0000);
	phx_read_u32(base_short_addr+0xc00e*4);
	//printk("--------------------WR-0049:0x2105\n");
	phx_write_u32(base_short_addr+0x0049*4,0x2105);
	phx_read_u32(base_short_addr+0x0049*4);
	//printk("--------------------WR-004a:0x2105\n");
	phx_write_u32(base_short_addr+0x004a*4,0x2105);
	phx_read_u32(base_short_addr+0x004a*4);
	//printk("--------------------WR-004a:0x2085\n");
	phx_write_u32(base_short_addr+0x004a*4,0x2085);
	phx_read_u32(base_short_addr+0x004a*4);
	//printk("--------------------WR-004f:0x8a06\n");
	phx_write_u32(base_short_addr+0x004f*4,0x8a06);
	phx_read_u32(base_short_addr+0x004f*4);
	//printk("--------------------WR-0050:0x8a06\n");
	phx_write_u32(base_short_addr+0x0050*4,0x8a06);
	phx_read_u32(base_short_addr+0x0050*4);
	//printk("--------------------WR-0050:0x0000\n");
	phx_write_u32(base_short_addr+0x0050*4,0x0000);
	phx_read_u32(base_short_addr+0x0050*4);
	//printk("--------------------WR-005a:0x0cdb\n");
	phx_write_u32(base_short_addr+0x005a*4,0x0cdb);
	phx_read_u32(base_short_addr+0x005a*4);
	//printk("--------------------WR-0062:0x0800\n");
	phx_write_u32(base_short_addr+0x0062*4,0x0800);
	phx_read_u32(base_short_addr+0x0062*4);
	//printk("--------------------WR-00a0:0x0031\n");
	phx_write_u32(base_short_addr+0x00a0*4,0x0031);
	phx_read_u32(base_short_addr+0x00a0*4);
	//printk("--------------------WR-4000:0xfc08\n");
	phx_write_u32(base_short_addr+0x4000*4,0xfc08);
	phx_read_u32(base_short_addr+0x4000*4);
	//printf("--------------------WR-4200:0xfc08\n");
	phx_write_u32(base_short_addr+0x4200*4,0xfc08);
	phx_read_u32(base_short_addr+0x4200*4);
	//printk("--------------------WR-4400:0xfc08\n");
	phx_write_u32(base_short_addr+0x4400*4,0xfc08);
	phx_read_u32(base_short_addr+0x4400*4);
	//printf("--------------------WR-4600:0xfc08\n");
	phx_write_u32(base_short_addr+0x4600*4,0xfc08);
	phx_read_u32(base_short_addr+0x4600*4);

	//printk("--------------------WR-406f:0x9703\n");
	phx_write_u32(base_short_addr+0x406f*4,0x9703);
	phx_read_u32(base_short_addr+0x406f*4);
	//printf("--------------------WR-426f:0x9703\n");
	phx_write_u32(base_short_addr+0x426f*4,0x9703);
	phx_read_u32(base_short_addr+0x426f*4);
	//printk("--------------------WR-446f:0x9703\n");
	phx_write_u32(base_short_addr+0x446f*4,0x9703);
	phx_read_u32(base_short_addr+0x446f*4);
	//printf("--------------------WR-466f:0x9703\n");
	phx_write_u32(base_short_addr+0x466f*4,0x9703);
	phx_read_u32(base_short_addr+0x466f*4);

	//printk("--------------------WR-4003:0x691e\n");
	phx_write_u32(base_short_addr+0x4003*4,0x691e);
	//printk("--------------------XXXWR-4203:0x691e\n");
	phx_read_u32(base_short_addr+0x4003*4);
	//printk("--------------------WR-4203:0x691e\n");
	phx_write_u32(base_short_addr+0x4203*4,0x691e);
	phx_read_u32(base_short_addr+0x4203*4);
	//printk("--------------------WR-4403:0x691e\n");
	phx_write_u32(base_short_addr+0x4403*4,0x691e);
	phx_read_u32(base_short_addr+0x4403*4);
	//printk("--------------------WR-4603:0x691e\n");
	phx_write_u32(base_short_addr+0x4603*4,0x691e);
	phx_read_u32(base_short_addr+0x4603*4);
	//printk("--------------------WR-4030:0x0ffe\n");
	phx_write_u32(base_short_addr+0x4030*4,0x0ffe);
	phx_read_u32(base_short_addr+0x4030*4);
	//printk("--------------------WR-4230:0x0ffe\n");
	phx_write_u32(base_short_addr+0x4230*4,0x0ffe);
	phx_read_u32(base_short_addr+0x4230*4);
	//printk("--------------------WR-4430:0x0ffe\n");
	phx_write_u32(base_short_addr+0x4430*4,0x0ffe);
	phx_read_u32(base_short_addr+0x4430*4);
	//printk("--------------------WR-4630:0x0ffe\n");
	phx_write_u32(base_short_addr+0x4630*4,0x0ffe);
	phx_read_u32(base_short_addr+0x4630*4);
	//printk("--------------------WR-403a:0x0013\n");
	phx_write_u32(base_short_addr+0x403a*4,0x0013);
	phx_read_u32(base_short_addr+0x403a*4);
	//printk("--------------------WR-423a:0x0013\n");
	phx_write_u32(base_short_addr+0x423a*4,0x0013);
	phx_read_u32(base_short_addr+0x423a*4);
	//printk("--------------------WR-443a:0x0013\n");
	phx_write_u32(base_short_addr+0x443a*4,0x0013);
	phx_read_u32(base_short_addr+0x443a*4);
	//printk("--------------------WR-463a:0x0013\n");
	phx_write_u32(base_short_addr+0x463a*4,0x0013);
	phx_read_u32(base_short_addr+0x463a*4);
	//printk("--------------------WR-403e:0x0106\n");
	phx_write_u32(base_short_addr+0x403e*4,0x0106);
	phx_read_u32(base_short_addr+0x403e*4);
	//printk("--------------------WR-423e:0x0106\n");
	phx_write_u32(base_short_addr+0x423e*4,0x0106);
	phx_read_u32(base_short_addr+0x423e*4);
	//printk("--------------------WR-443e:0x0106\n");
	phx_write_u32(base_short_addr+0x443e*4,0x0106);
	phx_read_u32(base_short_addr+0x443e*4);
	//printk("--------------------WR-463e:0x0106\n");
	phx_write_u32(base_short_addr+0x463e*4,0x0106);
	phx_read_u32(base_short_addr+0x463e*4);
	//printk("--------------------WR-406a:0x0000\n");
	phx_write_u32(base_short_addr+0x406a*4,0x0000);
	phx_read_u32(base_short_addr+0x406a*4);
	//printk("--------------------WR-426a:0x0000\n");
	phx_write_u32(base_short_addr+0x426a*4,0x0000);
	phx_read_u32(base_short_addr+0x426a*4);
	//printk("--------------------WR-446a:0x0000\n");
	phx_write_u32(base_short_addr+0x446a*4,0x0000);
	phx_read_u32(base_short_addr+0x446a*4);
	//printk("--------------------WR-466a:0x0000\n");
	phx_write_u32(base_short_addr+0x466a*4,0x0000);
	phx_read_u32(base_short_addr+0x466a*4);
	//printk("--------------------WR-4088:0x00a3\n");
	phx_write_u32(base_short_addr+0x4088*4,0x00a3);
	phx_read_u32(base_short_addr+0x4088*4);
	//printk("--------------------WR-4288:0x00a3\n");
	phx_write_u32(base_short_addr+0x4288*4,0x00a3);
	phx_read_u32(base_short_addr+0x4288*4);
	//printk("--------------------WR-4488:0x00a3\n");
	phx_write_u32(base_short_addr+0x4488*4,0x00a3);
	phx_read_u32(base_short_addr+0x4488*4);
	//printk("--------------------WR-4688:0x00a3\n");
	phx_write_u32(base_short_addr+0x4688*4,0x00a3);
	phx_read_u32(base_short_addr+0x4688*4);
	//printk("--------------------WR-408e:0x3c0e\n");
	phx_write_u32(base_short_addr+0x408e*4,0x3c0e);
	phx_read_u32(base_short_addr+0x408e*4);
	//printk("--------------------WR-428e:0x3c0e\n");
	phx_write_u32(base_short_addr+0x428e*4,0x3c0e);
	phx_read_u32(base_short_addr+0x428e*4);
	//printk("--------------------WR-448e:0x3c0e\n");
	phx_write_u32(base_short_addr+0x448e*4,0x3c0e);
	phx_read_u32(base_short_addr+0x448e*4);
	//printk("--------------------WR-468e:0x3c0e\n");
	phx_write_u32(base_short_addr+0x468e*4,0x3c0e);
	phx_read_u32(base_short_addr+0x468e*4);
	//printk("--------------------WR-4092:0x3220\n");
	phx_write_u32(base_short_addr+0x4092*4,0x3220);
	phx_read_u32(base_short_addr+0x4092*4);
	//printk("--------------------WR-4292:0x3220\n");
	phx_write_u32(base_short_addr+0x4292*4,0x3220);
	phx_read_u32(base_short_addr+0x4292*4);
	//printk("--------------------WR-4492:0x3220\n");
	phx_write_u32(base_short_addr+0x4492*4,0x3220);
	phx_read_u32(base_short_addr+0x4492*4);
	//printk("--------------------WR-4692:0x3220\n");
	phx_write_u32(base_short_addr+0x4692*4,0x3220);
	phx_read_u32(base_short_addr+0x4692*4);
	//printk("--------------------WR-4093:0x0000\n");
	phx_write_u32(base_short_addr+0x4093*4,0x0000);
	phx_read_u32(base_short_addr+0x4093*4);
	//printk("--------------------WR-4293:0x0000\n");
	phx_write_u32(base_short_addr+0x4293*4,0x0000);
	phx_read_u32(base_short_addr+0x4293*4);
	//printk("--------------------WR-4493:0x0000\n");
	phx_write_u32(base_short_addr+0x4493*4,0x0000);
	phx_read_u32(base_short_addr+0x4493*4);
	//printk("--------------------WR-4693:0x0000\n");
	phx_write_u32(base_short_addr+0x4693*4,0x0000);
	phx_read_u32(base_short_addr+0x4693*4);
	//printk("--------------------WR-4158:0x6320\n");
	phx_write_u32(base_short_addr+0x4158*4,0x6320);
	phx_read_u32(base_short_addr+0x4158*4);
	//printk("--------------------WR-4358:0x6320\n");
	phx_write_u32(base_short_addr+0x4358*4,0x6320);
	phx_read_u32(base_short_addr+0x4358*4);
	//printk("--------------------WR-4558:0x6320\n");
	phx_write_u32(base_short_addr+0x4558*4,0x6320);
	phx_read_u32(base_short_addr+0x4558*4);
	//printk("--------------------WR-4758:0x6320\n");
	phx_write_u32(base_short_addr+0x4758*4,0x6320);
	phx_read_u32(base_short_addr+0x4758*4);
	//printk("--------------------WR-4159:0x6320\n");
	phx_write_u32(base_short_addr+0x4159*4,0x6320);
	phx_read_u32(base_short_addr+0x4159*4);
	//printk("--------------------WR-4359:0x6320\n");
	phx_write_u32(base_short_addr+0x4359*4,0x6320);
	phx_read_u32(base_short_addr+0x4359*4);
	//printk("--------------------WR-4559:0x6320\n");
	phx_write_u32(base_short_addr+0x4559*4,0x6320);
	phx_read_u32(base_short_addr+0x4559*4);
	//printk("--------------------WR-4759:0x6320\n");
	phx_write_u32(base_short_addr+0x4759*4,0x6320);
	phx_read_u32(base_short_addr+0x4759*4);
	//printk("--------------------WR-415c:0x55fd\n");
	phx_write_u32(base_short_addr+0x415c*4,0x55fd);
	phx_read_u32(base_short_addr+0x415c*4);
	//printk("--------------------WR-435c:0x55fd\n");
	phx_write_u32(base_short_addr+0x435c*4,0x55fd);
	phx_read_u32(base_short_addr+0x435c*4);
	//printk("--------------------WR-455c:0x55fd\n");
	phx_write_u32(base_short_addr+0x455c*4,0x55fd);
	phx_read_u32(base_short_addr+0x455c*4);
	//printk("--------------------WR-475c:0x55fd\n");
	phx_write_u32(base_short_addr+0x475c*4,0x55fd);
	phx_read_u32(base_short_addr+0x475c*4);
	//printk("--------------------WR-417c:0x0000\n");
	phx_write_u32(base_short_addr+0x417c*4,0x0000);
	phx_read_u32(base_short_addr+0x417c*4);
	//printk("--------------------WR-437c:0x0000\n");
	phx_write_u32(base_short_addr+0x437c*4,0x0000);
	phx_read_u32(base_short_addr+0x437c*4);
	//printk("--------------------WR-457c:0x0000\n");
	phx_write_u32(base_short_addr+0x457c*4,0x0000);
	phx_read_u32(base_short_addr+0x457c*4);
	//printk("--------------------WR-477c:0x0000\n");
	phx_write_u32(base_short_addr+0x477c*4,0x0000);
	phx_read_u32(base_short_addr+0x477c*4);
	//printk("%s:%d\n",__FUNCTION__,__LINE__);
	//phx_write_u32(base_short_addr+0x4050*4,0x0604);
	//phx_read_u32(base_short_addr+0x4050*4);
	//phx_write_u32(base_short_addr+0x4048*4,0x0);
	//phx_read_u32(base_short_addr+0x4048*4);
#if 0
	phx_write_u32(base_short_addr+0x08,0x00001403);
	phx_read_u32(base_short_addr+0x08);
	phx_write_u32(base_short_addr+0x0C,0x00000006);
	phx_read_u32(base_short_addr+0x0C);
	phx_write_u32(base_short_addr+0x18,0x00003403);
	phx_read_u32(base_short_addr+0x18);
	phx_write_u32(base_short_addr+0x1C,0x00000006);
	phx_read_u32(base_short_addr+0x1C);
	phx_write_u32(base_short_addr+0x28,0x00005403);
	phx_read_u32(base_short_addr+0x28);
	phx_write_u32(base_short_addr+0x2C,0x00000006);
	phx_read_u32(base_short_addr+0x2C);
	phx_write_u32(base_short_addr+0x38,0x00007403);
	phx_read_u32(base_short_addr+0x38);
	phx_write_u32(base_short_addr+0x3C,0x00000006);
	phx_read_u32(base_short_addr+0x3C);
#endif
	phx_write_u32(base_short_addr+0xe008*4,0x20);//0x20
	//printk("%s:%d\n",__FUNCTION__,__LINE__);
	phx_write_u32(base_phyself_addr+0x40,0x800);
	phx_write_u32(base_phyself_addr+0x08,0x1407);
	phx_write_u32(base_phyself_addr+0x18,0x3407);
	//printk("%s:%d\n",__FUNCTION__,__LINE__);
	phx_write_u32(base_phyself_addr+0x28,0x5407);
	phx_write_u32(base_phyself_addr+0x38,0x7407);
	phx_write_u32(base_phyself_addr+0x48,0xffffffff);
	//printk("%s:%d\n",__FUNCTION__,__LINE__);
	phx_write_u32(base_phyself_addr+0x4c,0xffffffff);
	phx_write_u32(base_phyself_addr+0x50,0xffffffff);
	phx_write_u32(base_phyself_addr+0x54,0xffffffff);
	//printk("%s:%d\n",__FUNCTION__,__LINE__);
	//phx_write_u32(base_short_addr+0x10,0x801);
	//printk("base_short_addr+0x10 = 0x%x.\n", phx_read_u32(0x157c0010));
	//phx_write_u32(base_short_addr+0x40,0x801);
	//printf("base_short_addr+0x40 = 0x%x.\n", phx_read_u32(0x157c0040));

	//base_addr = 0x0015750000;
	phx_write_u32(base_phyself_addr + 0x0000,0x1f);   // 0x100003

	serdes_init_flag = 1;

    return 0;
}

/*************************************************************************
 *
 * parse the init string
 *
 * Parse the input string.  Fill in values in the driver control structure.
 *
 * RETURNS: OK or ERROR for invalid arguments.
 */
static int gem_parse(GEM_DRV_CTRL *pDrvCtrl, char *initString )
{
	char * tok;
	char * pHolder = NULL;
    if (pDrvCtrl == NULL)
    {
    	printk("[ERROR]pDrvCtrl is NULL!");
    	return ERROR;
    }
	/* parse the initString */
	tok = strtok_r(initString, ":", &pHolder);
	if (tok == NULL)
		return ERROR;
	pDrvCtrl->unit = atoi(tok);

	/* address of shared memory */
	tok = strtok_r(NULL, ":", &pHolder);
	if (tok == NULL)
		return ERROR;

//	/* size of shared memory */
//	tok = strtok_r(NULL, ":", &pHolder);
//	if (tok == NULL)
//		return ERROR;
//
//	/* number of rx descriptors */
//	tok = strtok_r(NULL, ":", &pHolder);
//	if (tok == NULL)
//		return ERROR;
//
//	/* number of tx descriptors */
//	tok = strtok_r(NULL, ":", &pHolder);
//	if (tok == NULL)
//		return ERROR;
//
//	/* get the usr_flags */
//	tok = strtok_r(NULL, ":", &pHolder);
//	if (tok == NULL)
//		return ERROR;

//	/* get the offset value */
//	tok = strtok_r(NULL, ":", &pHolder);
//	pDrvCtrl->offset = 0;

	return OK;
}

/* GEM网卡硬件初始化接口 */
int gemHwInit(GEM_DRV_CTRL **ppDrvCtrl, char *loadStr)
{
	GEM_DRV_CTRL *pDrvCtrl;
	u32 uRegVal = 0;
	u32 nwconfig = 0;
	int i;
	unsigned long clk_rate = 0;
	unsigned int val = 0;

	sysWrite32(0xbf0c0028, 0xa5accede);/*解锁其他寄存器*/
	
	/*release reset*/
	val = sysRead32(0x1f0c0004);
	printk("_cpu_up val == 0x%x  \r\n", val);
	val &= 0xf9ffffff;
	sysWrite32(0x1f0c0004, val);
//	gemRead32((void *)(0x900000001f300000+0x4));

    pDrvCtrl = (GEM_DRV_CTRL *)malloc (sizeof(GEM_DRV_CTRL));
    if (pDrvCtrl == NULL)
    {
    	printk("[ERROR]malloc pDrvCtrl failed!");
    	return (-1);
    }
//    printk("%s,%d pDrvCtrl = 0x%lx\n",__FUNCTION__,__LINE__,pDrvCtrl);
    memset ((char *)pDrvCtrl, 0x0, sizeof(GEM_DRV_CTRL));
    *ppDrvCtrl = pDrvCtrl;

	/* parse the init string, filling in the device structure */
	if (gem_parse(pDrvCtrl, loadStr) == ERROR)
	{
		free(pDrvCtrl);
		return (-1);
	}

	//printf("%s,%d pDrvCtrl->unit = %d\n",__FUNCTION__,__LINE__,pDrvCtrl->unit);
    sprintf(pDrvCtrl->gemName,GEM_DEVICE_NAME"%d",pDrvCtrl->unit);
    printk("%s [%s]\n",__FUNCTION__,pDrvCtrl->gemName);
	pDrvCtrl->gemBaseAddr = (void *)gem_resources[pDrvCtrl->unit].gemBaseAddr;
	pDrvCtrl->irq = gem_resources[pDrvCtrl->unit].irq;
	printk("%s,%d gemBaseAddr:0x%lx irq:%d\n",__FUNCTION__,__LINE__,pDrvCtrl->gemBaseAddr,pDrvCtrl->irq);

	pDrvCtrl->gemMaxMtu = ETH_MTU;
	
	

	
//	pDrvCtrl->semConfig = semBCreate(SEM_Q_FIFO, SEM_FULL);//改成posix接口
//	if (pDrvCtrl->semConfig == NULL)
//	{
//		free(pDrvCtrl);
//		return (-1);
//	}
	if (pthread_mutex_init2 (&pDrvCtrl->gemDevSem, PTHREAD_MUTEX_RECURSIVE,
			RE_MUTEX_PRIO_INHERIT | RE_MUTEX_CANCEL_SAFE ,
			 PTHREAD_MUTEX_CEILING) == -1)
	{
    	printk("[ERROR]gemDevSem pthread_mutex_init2 error!\n");
    	return (-1);
	}

	pDrvCtrl->semSend = semBCreate(SEM_Q_FIFO,SEM_FULL);
	if (pDrvCtrl->semSend == NULL)
	{
		free(pDrvCtrl);
		return (-1);
	}

    pDrvCtrl->gemRxIdx = 0;
    pDrvCtrl->gemTxFree = GEM_TX_DESC_CNT;
    pDrvCtrl->gemTxStall = FALSE;
    pDrvCtrl->interface = PHY_INTERFACE_MODE_SGMII;
    pDrvCtrl->physpeed = SPEED_1000;
    pDrvCtrl->int_pcs = 1;

    /*ldf 20230921 ignore:: hc3020b不用初始化serdes*/
//	init_serdes(pDrvCtrl->unit);//serdes初始化

	/* Disable all interrupts */
	gemRegWrite32(pDrvCtrl, GEM_IDR, 0xFFFFFFFF);	/* 禁止并清除所有中断 */

	uRegVal = gemRegRead32(pDrvCtrl, GEM_ISR);
	gemRegWrite32(pDrvCtrl, GEM_ISR, uRegVal);

	/* Disable the receiver & transmitter */
	gemRegWrite32(pDrvCtrl, GEM_NCR, 0x0);		/*  Disable TX & RX and more    */
	gemRegWrite32(pDrvCtrl, GEM_NCR, GEM_NCR_CLRSTAT);

	gemRegWrite32(pDrvCtrl, GEM_TSR, 0xFFFFFFFF); /*  Clear all bits in TSR       */
	gemRegWrite32(pDrvCtrl, GEM_RSR, 0xFFFFFFFF); /*  Clear all bits in RSR       */
	gemRegWrite32(pDrvCtrl, GEM_MAN, 0x0);

	/* Clear the Hash registers for the mac address
	 * pointed by AddressPtr
	 */
	gemRegWrite32(pDrvCtrl, GEM_HRB, 0x0);
	/* Write bits [63:32] in TOP */
	gemRegWrite32(pDrvCtrl, GEM_HRT, 0x0);

	/* Clear all counters */
	for (i = 0; i < STAT_SIZE; i++)
	{
		gemRegRead32(pDrvCtrl, GEM_OTLO + i*4);//0x100~0x1AC
	}

	/* 配置MAC地址 */
    gemGetMacAddr(pDrvCtrl);

	printk("gemSetMacAddr:[%s][%02x:%02x:%02x:%02x:%02x:%02x]\n",
			pDrvCtrl->gemName,
			pDrvCtrl->gemMacAddr[0],
			pDrvCtrl->gemMacAddr[1],
			pDrvCtrl->gemMacAddr[2],
			pDrvCtrl->gemMacAddr[3],
			pDrvCtrl->gemMacAddr[4],
			pDrvCtrl->gemMacAddr[5]);
	gemSetMacAddr(pDrvCtrl,pDrvCtrl->gemMacAddr);

	if (gemMemoryInit(pDrvCtrl) != 0)
	{
		return (-1);
	}
	/* 发送接收描述符初始化 */
	gemTxDescsInit(pDrvCtrl);
	gemRxDescsInit(pDrvCtrl);

	/* Setup for DMA Configuration register */
	gemRegWrite32(pDrvCtrl, GEM_DCFGR, HR3_GEM_DMACR_INIT);
	/* Setup for Network Control register, MDIO, Rx and Tx enable */
	gemRegSetBit(pDrvCtrl, GEM_NCR, GEM_NCR_MPE);

//	nwconfig = HR3_GEM_NWCFG_INIT /*| HR3_GEM_NWCFG_COPY_ALL*/;
	nwconfig = GEM_NCFGR_DBW_DBW128 |
			GEM_NCFGR_CLK_MCK_64 |
			GEM_NCFGR_RFCS |
			GEM_NCFGR_MAXFS |
			GEM_NCFGR_FD;

	//printf("%s,%d nwconfig=0x%x\n",__FUNCTION__,__LINE__,nwconfig);
	if (pDrvCtrl->interface == PHY_INTERFACE_MODE_SGMII && pDrvCtrl->int_pcs)
	{
		nwconfig |= GEM_NCFGR_SGMIIEN | GEM_NCFGR_PIS;
#ifdef CONFIG_MIPS64
		gemRegSetBit(pDrvCtrl, GEM_PCR, HR3_GEM_PCS_CTL_ANEG_ENBL);
#endif
	}

//	if (pDrvCtrl->interface == PHY_INTERFACE_MODE_SGMII)
//	{
//		gemRegSetBit(pDrvCtrl, GEM_NCFGR, GEM_NCFGR_SGMIIEN);
//	}
	
	/*gmac0 PhyAddr: 0x7, gmac1 PhyAddr: 0x6*/
	extern u32 gemPhyRead (int unit, u8 ucPhyAddr, u8 ucRegAddr);
	u8 PhyAddr = pDrvCtrl->unit ? 0x6 : 0x7;
	u32 PhyVal = 0, RegVal = 0, RegAddr = 0;
	PhyVal = gemPhyRead (0, PhyAddr, 17);/*只能用gmac0的地址来读phy*/
	PhyVal = (PhyVal & 0xc000) >> 14;
	printk("<**DEBUG**> [%s():_%d_]:: [GEM%d] PhyVal=0x%x (10M: 0x0, 100M: 0x1, 1000M: 0x2)\n", __func__, __LINE__, pDrvCtrl->unit, PhyVal);
	if(PhyVal == 0x2){
		pDrvCtrl->physpeed = SPEED_1000;
	}else if(PhyVal == 0x1){
		pDrvCtrl->physpeed = SPEED_100;
	}else{
		pDrvCtrl->physpeed = SPEED_10;
	}
	
	switch (pDrvCtrl->physpeed)
	{
		case SPEED_1000:
			nwconfig |= GEM_NCFGR_GBE;
			gemRegWrite32(pDrvCtrl, GEM_NCFGR, nwconfig);

			clk_rate = HR3_GEM_FREQUENCY_1000;
			break;
		case SPEED_100:
			nwconfig &= ~GEM_NCFGR_GBE;
			nwconfig |= GEM_NCFGR_SPD;
			gemRegWrite32(pDrvCtrl, GEM_NCFGR, nwconfig);

			clk_rate = HR3_GEM_FREQUENCY_100;
			break;
		case SPEED_10:
			nwconfig &= ~GEM_NCFGR_GBE;
			nwconfig &= ~GEM_NCFGR_SPD;
			gemRegWrite32(pDrvCtrl, GEM_NCFGR, nwconfig);
			clk_rate = HR3_GEM_FREQUENCY_10;
			break;
	}

	gemRegSetBit(pDrvCtrl, GEM_NCR, GEM_NCR_RXEN | GEM_NCR_TXEN);
	
#ifdef _HC3020B_
	/*gmac0 PhyAddr: 0x7, gmac1 PhyAddr: 0x6*/
	extern u32 gemPhyRead (int unit, u8 ucPhyAddr, u8 ucRegAddr);
	PhyVal = gemPhyRead (0, PhyAddr, 27/*GMII_MODE*/);/*只能用gmac0的地址来读phy*/
	printk("<**DEBUG**> [%s():_%d_]:: [GEM%d] PhyVal=0x%x (mii: 0xf, rgmii: 0xb)\n", __func__, __LINE__, pDrvCtrl->unit, PhyVal);
	if((PhyVal & 0xf) == 0xf){/*mii接口配置*/
		RegAddr = pDrvCtrl->unit ? (GEM_USER_ADDR1|GEM_USER_CTRL1) : (GEM_USER_ADDR0|GEM_USER_CTRL1);
		RegVal = gemRead32(0x9000000000000000 | RegAddr);
//		printk("<**DEBUG**> [%s():_%d_]\n", __func__, __LINE__);
		gemWrite32(0x9000000000000000 | RegAddr, RegVal | 0x1000000);
//		printk("<**DEBUG**> [%s():_%d_]\n", __func__, __LINE__);
		
		RegVal = gemRegRead32(pDrvCtrl, GEM_NCR);
//		printk("<**DEBUG**> [%s():_%d_]\n", __func__, __LINE__);
		gemRegWrite32(pDrvCtrl, GEM_NCR, RegVal | 0x10000000);
//		printk("<**DEBUG**> [%s():_%d_]\n", __func__, __LINE__);
	}else if((PhyVal & 0xf) == 0xb){/*rgmii接口配置*/
		/*do nothing*/
	}
//	printk("<**DEBUG**> [%s():_%d_]\n", __func__, __LINE__);
#endif
	
	
	uRegVal = gemRegRead32(pDrvCtrl, GEM_NCR);
	//printf("%s,%d nwctrl=0x%x\n",__FUNCTION__,__LINE__,uRegVal);
	
	g_pDrvCtrl[pDrvCtrl->unit] = pDrvCtrl;/*ldf 20230801 add*/

	printk("gem init ok!\n");

	return 0;
}

//为vxbUsrCmdLine.c编译通过暂时保留，后续去掉
void hrGMACRegister(void)
{
	printk("%s\n", __FUNCTION__);
//	vxbDevRegister((struct vxbDevRegInfo *)&(hrGMACRegistration));
	return;
}

END_OBJ *gemEndLoad(char *loadStr)
{
	GEM_DRV_CTRL *pDrvCtrl = NULL;
	//printf("%s,%d loadStr[%s]\n",__FUNCTION__,__LINE__,loadStr);
	if (loadStr == NULL)
	{
		printk("[ERROR]loadStr is NULL.\n");
		return ((END_OBJ *)NULL);
	}

	if (loadStr[0] == 0)
	{
//		strcpy(loadStr, "gem");
		bcopy (GEM_DEVICE_NAME, loadStr, 5);
		//printf("First,loadStr[0] is NULL.\n");
		return ((END_OBJ *)NULL);
	}

    if (gemHwInit(&pDrvCtrl, loadStr) != 0)
    {
    	printk("[ERROR]gemHwInit error.\n",__FUNCTION__,__LINE__);
    	return ((END_OBJ *)NULL);
    }

	if (InitGemDev(pDrvCtrl) != 0 )
	{
		printk("[ERROR]InitGemDev error.\n");
		return ((END_OBJ *)NULL);
	}
	
	//printf("%s complete.\n",__FUNCTION__,__LINE__);
	return (&pDrvCtrl->gemEndObj);
}

static int InitGemDev(GEM_DRV_CTRL *pDrvCtrl)
{
	//printf("%s,%d entry\n",__FUNCTION__,__LINE__);

	if (END_OBJ_INIT(&pDrvCtrl->gemEndObj,(DEV_OBJ *)pDrvCtrl,GEM_DEVICE_NAME,
			pDrvCtrl->unit,&gemEndFuncs,"Gem Driver") == ERROR)
	{
		return (ERROR);
	}
	
	/*初始化MUX的MIB */
	if (END_MIB_INIT(&pDrvCtrl->gemEndObj, M2_ifType_ethernet_csmacd,
			(unsigned char *)pDrvCtrl->gemMacAddr,
			ETHER_ADDR_LEN, ETH_MTU, /*1000000000*/SPEED_1000) == ERROR)
	{
		return (ERROR);
	}

	/* 告诉MUX层设备的状态 */
	if (END_OBJ_READY(&pDrvCtrl->gemEndObj,
			IFF_MULTICAST | IFF_NOTRAILERS | IFF_BROADCAST | IFF_SIMPLEX) == ERROR)
	{
		return (ERROR);
	}

	/* Success! */
	pDrvCtrl->bStarted = FALSE;
	//printf("%s,%d end.\n",__FUNCTION__,__LINE__);
	return OK;
}

static void gemTxDescsInit(GEM_DRV_CTRL *pDrvCtrl)
{	
	unsigned int i;
	GEM_TX_DESC *pTxDesc = NULL;
	unsigned long physAddr;
	M_BLK_ID  pRxMblk;

	for (i = 0; i < GEM_TX_DESC_CNT; i++)
	{

		pTxDesc = &pDrvCtrl->gemTxDescMem[i];
//		printf("%s,%d,pTxDesc[%d]=0x%lx\n",__FUNCTION__,__LINE__,i,pTxDesc);
		
		pTxDesc->txStatus = GEM_TX_USED_BIT /*| GEM_TX_WRAP_BIT | GEM_TX_LAST_BIT*/;/*ldf 20230809 note:: 这里初始值若为0，系统启动ping不通*/		
        if (i == (GEM_TX_DESC_CNT-1))
        {
    		/* WRAP bit to last BD */
        	/* Wrap - marks last descriptor in receive buffer descriptor list. */
        	pTxDesc->txStatus |= GEM_TX_WRAP_BIT;
        }
        
        pTxDesc->txBufAddr = 0;
#ifdef GEM_DMA_64BIT
        pTxDesc->txBufAddrHi = 0;
#endif
  
#if 1/*ldf 20230728 add*/
//        pDrvCtrl->gemTxDmaAddr2[i] = (void*)gemTxDmaMemAddr_get(pDrvCtrl->unit) + GEM_TX_DMA_SIZE*i;/*ldf 20230824 add*/
    	pDrvCtrl->gemTxDmaAddr2[i] = (void*)memalign(128, GEM_TX_DMA_SIZE);
//        pDrvCtrl->gemTxDmaAddr2[i] = (void*)kmalloc(GEM_TX_DMA_SIZE);
    	if (pDrvCtrl->gemTxDmaAddr2[i] == NULL)
    	{
    		printk("[ERROR]malloc gemTxDmaAddr2[%d] error!\n",i);
    		return;
    	}
//    	printk("<**DEBUG**> [%s():_%d_]:: gemTxDmaAddr2[%d] = 0x%lx\n", __FUNCTION__, __LINE__,i,pDrvCtrl->gemTxDmaAddr2[i]);
    	memset((void*)pDrvCtrl->gemTxDmaAddr2[i], 0x0, GEM_TX_DMA_SIZE);
#endif
	}
	//printf("%s,%d,pTxDesc[%d]=0x%lx\n",__FUNCTION__,__LINE__,0,&pDrvCtrl->gemTxDescMem[0]);
	//printf("%s,%d,pTxDesc[%d]=0x%lx\n",__FUNCTION__,__LINE__,GEM_TX_DESC_CNT-1,&pDrvCtrl->gemTxDescMem[GEM_TX_DESC_CNT-1]);
	physAddr = GEM_VIRT_TO_PHYS((void*)&pDrvCtrl->gemTxDescMem[0]);
	gemRegWrite32(pDrvCtrl, GEM_TBQB, GEM_ADDR_LO(physAddr));
#ifdef GEM_DMA_64BIT
	gemRegWrite32(pDrvCtrl, GEM_TBQB_UPPER, GEM_ADDR_HI(physAddr));
#endif
	//printf("%s,%d end.\n",__FUNCTION__,__LINE__);
	return;
}

static void gemRxDescsInit(GEM_DRV_CTRL *pDrvCtrl)
{
	GEM_RX_DESC *pRxDesc = NULL;
	M_BLK_ID pRxMblk = NULL;
	unsigned long physAddr;
	unsigned int i;	

//记得添加互斥量
	for (i = 0; i < GEM_RX_DESC_CNT; i++)
	{
		pRxMblk = endPoolTupleGet(pDrvCtrl->gemEndObj.pNetPool);
		if (pRxMblk == NULL)
		{
			printk("<WARNING> [%s():_%d_]:: Memory pool is not enough.\n", __FUNCTION__, __LINE__);
			break;
		}
		pRxMblk->m_next = NULL;
		pRxMblk->m_len = GEM_RX_BUF_LENTH_MASK;
        pDrvCtrl->gemRxMblk[i] = pRxMblk;
        pRxDesc = &pDrvCtrl->gemRxDescMem[i];
        //刷cache
//        cache2_invalidate(DATA_CACHE,pRxMblk->m_data, pRxMblk->m_len);

        physAddr = GEM_VIRT_TO_PHYS((void*)pRxMblk->m_data);
//        printf("%s,%d,pRxMblk->m_data=0x%lx,pRxDesc[%d]=0x%lx\n",__FUNCTION__,__LINE__,physAddr,i,pRxDesc);
        if (i == (GEM_RX_DESC_CNT-1))
        {
    		/* WRAP bit to last BD */
        	/* Wrap - marks last descriptor in receive buffer descriptor list. */
        	pRxDesc->rxBufAddr = (GEM_ADDR_LO(physAddr)&GEM_RX_ADDRE_MASK) | GEM_RX_WRAP_BIT;
//        	printf("%s,%d,pRxDesc->addr=0x%x,pRxDesc[%d]=0x%lx\n",__FUNCTION__,__LINE__,pRxDesc->rxBufAddr,i,pRxDesc);
        }
        else
        {
        	pRxDesc->rxBufAddr = GEM_ADDR_LO(physAddr)&GEM_RX_ADDRE_MASK;
        }
#ifdef GEM_DMA_64BIT
        pRxDesc->rxBufAddrHi  = GEM_ADDR_HI((physAddr));
#endif
        pRxDesc->rxStatus = 0;
	}

	physAddr = GEM_VIRT_TO_PHYS((void*)&pDrvCtrl->gemRxDescMem[0]);
	gemRegWrite32(pDrvCtrl, GEM_RBQB, GEM_ADDR_LO(physAddr));
#ifdef GEM_DMA_64BIT
	gemRegWrite32(pDrvCtrl, GEM_RBQB_UPPER, GEM_ADDR_HI(physAddr));
#endif

	//printf("%s,%d end.\n",__FUNCTION__,__LINE__);
	return;
}

static int gemMemoryInit(GEM_DRV_CTRL *pDrvCtrl)
{

    if (endPoolCreate(GEM_TUPLE_CNT, &pDrvCtrl->gemEndObj.pNetPool) == ERROR)
    {
        printk("[ERROR]%s%d: pool creation failed!\n", GEM_DEVICE_NAME,pDrvCtrl->unit);
        return (ERROR);
    }

//    pDrvCtrl->gemTxDescMem = (GEM_TX_DESC*)gemTxDescMemAddr_get(pDrvCtrl->unit);/*ldf 20230824 add*/
	pDrvCtrl->gemTxDescMem = (GEM_TX_DESC *)memalign(sizeof(GEM_TX_DESC), sizeof(GEM_TX_DESC)*(GEM_TX_DESC_CNT));
//	pDrvCtrl->gemTxDescMem = (GEM_TX_DESC *)kmalloc(sizeof(GEM_TX_DESC)*(GEM_TX_DESC_CNT));
	if (pDrvCtrl->gemTxDescMem == NULL)
	{
		printk("[ERROR]malloc tx_mac_descrtable buffer error!\n");
		return (ERROR);
	}
	printk("<**DEBUG**> [%s():_%d_]:: gemTxDescMem = 0x%lx, GEM_TX_DESC_CNT = %d, sizeof(GEM_TX_DESC) = 0x%x\n", __FUNCTION__, __LINE__,pDrvCtrl->gemTxDescMem,GEM_TX_DESC_CNT,sizeof(GEM_TX_DESC));
	memset((void*)pDrvCtrl->gemTxDescMem, 0x0, gemTxDescMemSize_get());

//	pDrvCtrl->gemRxDescMem = (GEM_RX_DESC*)gemRxDescMemAddr_get(pDrvCtrl->unit);/*ldf 20230824 add*/
	pDrvCtrl->gemRxDescMem = (GEM_RX_DESC *)memalign(sizeof(GEM_RX_DESC), sizeof(GEM_RX_DESC)*(GEM_RX_DESC_CNT));
//	pDrvCtrl->gemRxDescMem = (GEM_RX_DESC *)kmalloc(sizeof(GEM_RX_DESC)*(GEM_RX_DESC_CNT));
	if (pDrvCtrl->gemRxDescMem == NULL)
	{
		printk("[ERROR]malloc rx_mac_descrtable buffer error!\n");
		return (ERROR);
	}
	printk("<**DEBUG**> [%s():_%d_]:: gemRxDescMem = 0x%lx, GEM_RX_DESC_CNT = %d, sizeof(GEM_RX_DESC) = 0x%x\n", __FUNCTION__, __LINE__,pDrvCtrl->gemRxDescMem,GEM_RX_DESC_CNT,sizeof(GEM_RX_DESC));
	memset((void*)pDrvCtrl->gemRxDescMem, 0x0, gemRxDescMemSize_get());
	
	//printf("gemMemoryInit end. \r\n");

	return (0);
}

static void gemEndRxHandle(GEM_DRV_CTRL *pDrvCtrl)
{
	M_BLK_ID pNewMblk = NULL;
	M_BLK_ID pMblk = NULL;
	int 	loopCounter = 16;
	unsigned int length;
	GEM_RX_DESC *pRxDesc;
	unsigned long  physAddr;
	pRxDesc = &pDrvCtrl->gemRxDescMem[pDrvCtrl->gemRxIdx];
	unsigned int uRegVal = 0;
	
	/* Check  if the owner is the CPU */
	while ((pRxDesc->rxBufAddr & GEM_RX_OWNERSHIP_BIT) && (loopCounter > 0)) /* 该缓冲区描述符有数据  */
	{
		loopCounter--;
		length = pRxDesc->rxStatus & GEM_RX_BUF_LENTH_MASK;
//		printf("<**DEBUG**> [%s():_%d_]:: pRxDesc->rxBufAddr=0x%x, pRxDesc->rxStatus=0x%x\n", __FUNCTION__, __LINE__,pRxDesc->rxBufAddr,pRxDesc->rxStatus);

		pNewMblk = endPoolTupleGet(pDrvCtrl->gemEndObj.pNetPool);
		if (pNewMblk == NULL)
		{
//			printf("<WARNING> [%s():_%d_]:: Memory pool is not enough.\n", __FUNCTION__, __LINE__);
#if 1
			break;
#else/*ldf 20230804 add:: 获取不到mBlk时，就舍弃当前描述符的数据，继续处理下一个描述符*/
			pRxDesc->rxBufAddr &= ~(GEM_RX_OWNERSHIP_BIT);
			pRxDesc->rxStatus = 0;
			GEM_INC_DESC(pDrvCtrl->gemRxIdx, GEM_RX_DESC_CNT);
			pRxDesc = &pDrvCtrl->gemRxDescMem[pDrvCtrl->gemRxIdx];
			continue;
#endif
		}

		gRecvCount++;//有效的接收统计

		/* sync the buffer with the cache */
//		cache2_invalidate(DATA_CACHE, pNewMblk->m_data, length);/*ldf 20230706 add: 加了没什么作用*/

		/* Swap the mBlks. */
		pMblk = pDrvCtrl->gemRxMblk[pDrvCtrl->gemRxIdx];
		pDrvCtrl->gemRxMblk[pDrvCtrl->gemRxIdx] = pNewMblk;
		pNewMblk->m_next = NULL;
		pNewMblk->m_len = pNewMblk->m_pkthdr.len = GEM_RX_BUF_LENTH_MASK;
//		printf("%s,%d,pNewMblk->mBlkHdr.mLen=%d\n",__FUNCTION__,__LINE__,pNewMblk->mBlkHdr.mLen);

		physAddr = GEM_VIRT_TO_PHYS((void*)pNewMblk->m_data);
		if (pDrvCtrl->gemRxIdx == (GEM_RX_DESC_CNT-1))
		{
//			if((pRxDesc->rxBufAddr & GEM_RX_WRAP_BIT) == 0)/*ldf 20230808 add: debug*/
//				printf("<**DEBUG**> [%s():_%d_]:: pRxDesc->rxBufAddr=0x%x\n", __FUNCTION__, __LINE__,pRxDesc->rxBufAddr);

			pRxDesc->rxBufAddr = (GEM_ADDR_LO(physAddr)&GEM_RX_ADDRE_MASK) | GEM_RX_WRAP_BIT;
		}
		else
		{
			pRxDesc->rxBufAddr = GEM_ADDR_LO(physAddr)&GEM_RX_ADDRE_MASK;
		}

#ifdef GEM_DMA_64BIT
		pRxDesc->rxBufAddrHi = GEM_ADDR_HI((physAddr));
#endif
		
		pRxDesc->rxStatus = 0;/*xxx ldf 20230808 add:: 这一行如果放到上报数据到协议栈代码的后边，就会容易死机*/

		/* process device packet into net buffer */
		pMblk->mBlkHdr.mFlags  |= M_PKTHDR | M_EXT; /* set the packet header */
		pMblk->mBlkHdr.mLen    = length; /* set the data len */
		pMblk->mBlkPktHdr.len  = length; /* set the total len */

		
//		DumpUint8(pMblk->mBlkHdr.mData, pMblk->mBlkHdr.mLen);/*ldf 20230824 add:: debug*/
		
		/* sync the buffer with the cache */
//		cache2_invalidate(DATA_CACHE, pMblk->mBlkHdr.mData, length);/*ldf 20230706 add:: 加了没什么作用*/  

		/* Send the received frame to the stack */
		END_RCV_RTN_CALL (&pDrvCtrl->gemEndObj, pMblk);/*上报数据到协议栈*/

		GEM_INC_DESC(pDrvCtrl->gemRxIdx, GEM_RX_DESC_CNT);
		pRxDesc = &pDrvCtrl->gemRxDescMem[pDrvCtrl->gemRxIdx];
	}
	
	if (loopCounter == 0)
	{
//		printf("%s  __%d__\n",__func__,__LINE__);
		if ((netJobAdd ((FUNCPTR)gemEndRxHandle, (unsigned long)pDrvCtrl,0,0,0,0)) == ERROR)
		{
			/* Very bad!! The stack is now probably corrupt. */
			printf("<WARNING> [%s():_%d_]:: The netJobRing is full.\n", __FUNCTION__, __LINE__);
		}
		return;
	}

	if(atomic_set(&pDrvCtrl->gemRxPending, FALSE) != TRUE)
	{
//		printf("<WARNNING> [%s():_%d_]:: gemRxPending atomic_set FALSE failed.\n", __FUNCTION__, __LINE__);
	}
	
//	printf("%s, %d end.\n", __FUNCTION__, __LINE__);
	
	uRegVal = GEM_IER_RCOMP /*| GEM_IER_RXUBR*/ | GEM_IER_ROVR;     /*  enable rx int */
	gemRegSetBit(pDrvCtrl, GEM_IER, uRegVal);
	
	return;
}

static void gemEndTxHandle(GEM_DRV_CTRL *pDrvCtrl)
{
	GEM_TX_DESC *pDesc;
    M_BLK_ID pMblk;
    BOOL restart = FALSE;
    unsigned int uRegVal = 0;
    
    END_TX_SEM_TAKE(&pDrvCtrl->gemEndObj, WAIT_FOREVER);

	pDesc = &pDrvCtrl->gemTxDescMem[pDrvCtrl->gemTxCons];
//	CACHE_USER_INVALIDATE(pDesc,sizeof(GEM_TX_DESC));
    
	int ldf_flag = 0;/*ldf 20230803 add:: debug*/
	extern unsigned long long ldf_udelay;/*ldf 20230803 add:: debug*/
	while ((pDrvCtrl->gemTxFree < GEM_TX_DESC_CNT))
	{
		if((pDesc->txStatus & GEM_TX_USED_BIT) == 0)
		{
//				/*ldf 20230803 add:: debug*/
//				printf("<**DEBUG111**> [%s():_%d_]:: pDesc=0x%lx,pDesc->txStatus=0x%x,pDrvCtrl->gemTxCons=%d,gSendCountTotal=%llu\n",
//					__FUNCTION__,__LINE__,pDesc,pDesc->txStatus,pDrvCtrl->gemTxCons,gSendCountTotal);
//				ldf_flag = 1;/*ldf 20230803 add:: debug*/
			
			/*ldf 20230814 add:: 产生发送完成中断后，网卡会将GEM_TX_USED_BIT置为1，这中间有一小段时间差。
			 * 当数据发送很快的时候，中断产生的很快，但是GEM_TX_USED_BIT没有来得及置为1，导致描述符释放就不及时，
			 * 所以这里加个延时，不然描述符释放不及时*/
			usleep(3/*ldf_udelay*/);/*xxx ldf 20230814 note:: 这样会牺牲一些性能，需要继续查下是否有其他原因*/
//			MEM_BARRIER_RW1();/*ldf 20230912 add*/
			
			if((pDesc->txStatus & GEM_TX_USED_BIT) == 0){
				break;
			}else{
//				if(/*((pDesc->txStatus & GEM_TX_BUF_LENTH_MASK) < ETH_MTU) &&*/ (ldf_flag == 1))/*ldf 20230803 add:: debug*/
//				{
//					printf("<**DEBUG222**> [%s():_%d_]:: pDesc=0x%lx,pDesc->txStatus=0x%x,pDrvCtrl->gemTxCons=%d,gSendCountTotal=%llu\n",
//						__FUNCTION__,__LINE__,pDesc,pDesc->txStatus,pDrvCtrl->gemTxCons,gSendCountTotal);
//				}
			}
		}

//		if(/*((pDesc->txStatus & GEM_TX_BUF_LENTH_MASK) < ETH_MTU) &&*/ (ldf_flag == 1))/*ldf 20230803 add:: debug*/
//		{
//			printf("<**DEBUG333**> [%s():_%d_]:: pDesc=0x%lx,pDesc->txStatus=0x%x,pDrvCtrl->gemTxCons=%d,gSendCountTotal=%llu\n",
//				__FUNCTION__,__LINE__,pDesc,pDesc->txStatus,pDrvCtrl->gemTxCons,gSendCountTotal);
//			ldf_flag = 0;
//		}
		
		gSendCountTotal++;//实际发送处理的总次数	
		
#if 0/*ldf 20230728 ignored:: 避免Mblk资源释放不及时造成缓冲区满,将数据拷贝出来后立即释放资源,避免重复释放,注释掉这里*/
		pMblk = pDrvCtrl->gemTxMblk[pDrvCtrl->gemTxCons];
//		printf("%s,%d pDesc=0x%lx,pMblk=0x%lx,pDrvCtrl->gemTxCons=%d\n",__FUNCTION__,__LINE__,pDesc,pMblk,pDrvCtrl->gemTxCons);
		if (pMblk != NULL)
		{
//			printf("%s,%d pMblk=0x%lx,pMblk->mBlkHdr.mLen=0x%lx\n",__FUNCTION__,__LINE__,pMblk,pMblk->mBlkHdr.mLen);
			endPoolTupleFree(pMblk);
			pDrvCtrl->gemTxMblk[pDrvCtrl->gemTxCons] = NULL; 
		}
#endif
		pDesc->txBufAddr = 0;
#ifdef GEM_DMA_64BIT
		pDesc->txBufAddrHi = 0;
#endif
		
		pDesc->txStatus &= GEM_TX_WRAP_BIT;//发送描述符环最后一个描述符标志保留
		pDesc->txStatus |= GEM_TX_USED_BIT;/*ldf 20230809 note:: 这里如果注释了，发送描述符释放不了*/

        pDrvCtrl->gemTxFree++;
 
        GEM_INC_DESC (pDrvCtrl->gemTxCons, GEM_TX_DESC_CNT);
		pDesc = &pDrvCtrl->gemTxDescMem[pDrvCtrl->gemTxCons];
//	    CACHE_USER_INVALIDATE(pDesc,sizeof(GEM_TX_DESC));

		if (pDrvCtrl->gemTxStall == TRUE)
		{
			pDrvCtrl->gemTxStall = FALSE;
			restart = TRUE;
		}
    }
    END_TX_SEM_GIVE(&pDrvCtrl->gemEndObj);
	if(atomic_set(&pDrvCtrl->gemTxPending, FALSE) != TRUE)
	{
//		printf("<WARNNING> [%s():_%d_]:: gemTxPending atomic_set FALSE failed.\n", __FUNCTION__, __LINE__);
	}
    
	uRegVal = GEM_IER_TCOMP /*| GEM_IER_TXUBR*/;     /*  enable tx int */
	gemRegSetBit(pDrvCtrl, GEM_IER, uRegVal);
	
	if (restart == TRUE)
	{
		printf("%s,%d muxTxRestart.\n",__FUNCTION__,__LINE__);
		muxTxRestart(pDrvCtrl);
	}
//	printf("<**DEBUG**> [%s():_%d_]:: Return! \n", __FUNCTION__, __LINE__);
    return;
}

#if 0
static int gemSend(GEM_DRV_CTRL *pDrvCtrl, M_BLK_ID MblkPtr)
{
	GEM_TX_DESC *pTxDesc;
//	GEM_TX_DESC *pFirstTxDesc;
	char *pData;
	int length = 0, temp_len = 0;
	unsigned int uiState = 0;
	unsigned long physAddr;
	unsigned int lastIdx = 0;
	void *gemTxDmaAddr = 0x1080000000UL;//pDrvCtrl->gemTxDmaAddr;

	M_BLK_ID pMblk = MblkPtr;
	pTxDesc = &pDrvCtrl->gemTxDescMem[pDrvCtrl->gemTxProd];
	
//	pFirstTxDesc = &pDrvCtrl->gemTxDescMem[pDrvCtrl->gemTxProd];
	
	if (!(pTxDesc->txStatus & GEM_TX_USED_BIT))//如果GEM_TX_USED_BIT为0 ，说明dma传输还未结束
	{
		//printk("gemSend::CPU not owner of tx frame, tx_status = 0x%x._txDescNum=0x%x\r\n",_txStatus,_txDescNum);
		//printk("curDesc = %d.\r\n",_txDescNum);
		return -1;
	}
			
	while (pMblk != NULL )
	{
//		gSendTaskCount++;
		pData = pMblk->mBlkHdr.mData;
		temp_len = pMblk->mBlkHdr.mLen;
		
		if( temp_len == 0 )
		{
			pMblk = pMblk->mBlkHdr.mNext;
//			printf("!!!Error, gemSend::send length = 0.\r\n");
			return -1;		
		}
		
//		printk("<**DEBUG**> [%s():_%d_]:: pMblk->mBlkHdr.mLen=0x%x, pMblk->m_len=0x%x\n", __FUNCTION__, __LINE__,pMblk->mBlkHdr.mLen,pMblk->m_len);
//		printk("<**DEBUG**> [%s():_%d_]:: pMblk->mBlkHdr.mData=0x%lx, pMblk->m_data=0x%lx\n", __FUNCTION__, __LINE__,pMblk->mBlkHdr.mData,pMblk->m_data);
//		printk("<**DEBUG**> [%s():_%d_]:: pMblk->mBlkHdr.mNext=0x%lx, pMblk->m_next=0x%lx\n", __FUNCTION__, __LINE__,pMblk->mBlkHdr.mNext,pMblk->m_next);
	
		memcpy(gemTxDmaAddr + length, pData, temp_len);
//		bcopy(pData, gemTxDmaAddr + length, temp_len);

	
		length += temp_len;

//		if (pMblk->mBlkHdr.mNext != NULL)
//		{
			pMblk = pMblk->mBlkHdr.mNext;
//			continue;
//		}
	}

//		printk("%s,%d,pDrvCtrl->gemTxProd=0x%lx,\n",__FUNCTION__,__LINE__,pDrvCtrl->gemTxProd);
		
		//printk("%s,%d,pTxDesc=0x%lx,pTxDesc->txStatus=0x%x,length=%d\n",__FUNCTION__,__LINE__,
		//		pTxDesc,pTxDesc->txStatus,length);

//		__asm__ volatile("sync");
		uiState = length & GEM_TX_BUF_LENTH_MASK;
		if (pMblk->m_next == NULL)
		{
			uiState |= GEM_TX_LAST_BIT ;
//			printk("%s,%d\n",__FUNCTION__,__LINE__);
		}

		uiState &= (~GEM_TX_USED_BIT);
		/* WRAP: marks last descriptor in transmit buffer descriptor list */
		if (pDrvCtrl->gemTxProd == (GEM_TX_DESC_CNT-1))
		{
			uiState |= GEM_TX_WRAP_BIT;
		}
		pTxDesc->txStatus = uiState ;

		//printk("%s,%d,pTxDesc->txStatus=0x%x,pDrvCtrl->gemTxProd=%d\n",__FUNCTION__,__LINE__, pTxDesc->txStatus,pDrvCtrl->gemTxProd);
		physAddr = GEM_VIRT_TO_PHYS((void *)gemTxDmaAddr);
//		printk("pData=0x%lx:PhysAddr=0x%lx\n",pData,physAddr);
		pTxDesc->txBufAddr 	 = GEM_ADDR_LO(physAddr);
#ifdef GEM_DMA_64BIT
		pTxDesc->txBufAddrHi = GEM_ADDR_HI(physAddr);
#endif
		__asm__ volatile("sync");//必须要

		//printk("%s,%d,pTxDesc=0x%lx,pTxDesc->txStatus=0x%x,length=%d\n",__FUNCTION__,__LINE__,
		//				pTxDesc,pTxDesc->txStatus,length);
//		pMblk = pMblk->mBlkHdr.mNext;
		pDrvCtrl->gemTxFree--;
		lastIdx = pDrvCtrl->gemTxProd;
		GEM_INC_DESC(pDrvCtrl->gemTxProd, GEM_TX_DESC_CNT);
//		endPoolTupleFree(pMblk);
//	}
	//printk("%s,%d\n",__FUNCTION__,__LINE__);

	pDrvCtrl->gemTxMblk[lastIdx] = MblkPtr;
//	endPoolTupleFree(MblkPtr);
	
	/* Start the transmission */
	gemRegSetBit(pDrvCtrl, GEM_NCR, GEM_NCR_TSTART);

	//printk("%s,%d nwctrl=0x%x\n",__FUNCTION__,__LINE__, gemRegRead32(pDrvCtrl, GEM_NCFGR));

	return 0;
}
#else /*ldf 20230714 add*/
static int gemSend(GEM_DRV_CTRL *pDrvCtrl, M_BLK_ID MblkPtr)
{
	GEM_TX_DESC *pTxDesc, *pFirstTxDesc;
	char *pData;
	int length = 0;
	unsigned int uiState = 0;
	unsigned long physAddr;
	M_BLK_ID pMblk;
	int i = 0, used = 0;
	unsigned int firstIdx = 0, lastIdx = 0;
	void *gemTxDmaAddr;
	
	firstIdx = pDrvCtrl->gemTxProd;
	pFirstTxDesc = &pDrvCtrl->gemTxDescMem[firstIdx];

//	printf("%s %d  IN\n",__func__,__LINE__);
	for(pMblk = MblkPtr, i = 0, used = 0; pMblk != NULL; pMblk = pMblk->mBlkHdr.mNext)
	{
		pTxDesc = &pDrvCtrl->gemTxDescMem[pDrvCtrl->gemTxProd];
		pData = pMblk->mBlkHdr.mData;
		length = pMblk->mBlkHdr.mLen;
		
//		DumpUint8(pData, length);/*ldf 20230824 add:: debug*/
		
//		if(length < ETH_MTU)/*ldf 20230803 add:: debug*/
//		{
//			printf("<**DEBUG000**> [%s():_%d_]:: pTxDesc=0x%lx, pData=0x%lx, length=%d, gemTxProd=%d\n",
//					__FUNCTION__, __LINE__,pTxDesc,pData,length,pDrvCtrl->gemTxProd);
//		}
		
//		if((length != 0) /*&& (pTxDesc->txStatus & GEM_TX_USED_BIT)*/)
//		{
			gSendCount++;/*有效的发送统计*/
//			printf("<**DEBUG**> [%s():_%d_]:: length=%d, pDrvCtrl->gemTxProd=%d\n", __FUNCTION__, __LINE__,length,pDrvCtrl->gemTxProd);
			
#if 1/*ldf 20230728 add:: 避免Mblk资源释放不及时造成缓冲区满，这里改用拷贝方式，将数据拷贝出来后立即释放资源*/
			gemTxDmaAddr = pDrvCtrl->gemTxDmaAddr2[pDrvCtrl->gemTxProd];
			memcpy((char*)gemTxDmaAddr, pData, length);
#endif
		
			uiState = length & GEM_TX_BUF_LENTH_MASK;
			/* WRAP: marks last descriptor in transmit buffer descriptor list */
			if (pDrvCtrl->gemTxProd == (GEM_TX_DESC_CNT-1))
			{
				uiState |= GEM_TX_WRAP_BIT;
			}
			if(pMblk->mBlkHdr.mNext == NULL)
			{
//				printf("%s %d \n",__func__,__LINE__);
				uiState |= GEM_TX_LAST_BIT;
			}
			pTxDesc->txStatus = uiState;
			
//			printf("<**DEBUG**> [%s():_%d_]:: pDrvCtrl->gemTxProd=%d, pTxDesc->txStatus=0x%x, uiState=0x%x\n", __FUNCTION__, __LINE__,pDrvCtrl->gemTxProd,pTxDesc->txStatus,uiState);
			
#if 1/*ldf 20230728 add:: 避免Mblk资源释放不及时造成缓冲区满，这里改用拷贝方式，将数据拷贝出来后立即释放资源*/
			physAddr = GEM_VIRT_TO_PHYS((void*)gemTxDmaAddr);
#else
			physAddr = GEM_VIRT_TO_PHYS((void*)pData);
#endif
			pTxDesc->txBufAddr = GEM_ADDR_LO(physAddr);
#ifdef GEM_DMA_64BIT
			pTxDesc->txBufAddrHi = GEM_ADDR_HI(physAddr);
#endif
	
			pDrvCtrl->gemTxFree--;
			lastIdx = pDrvCtrl->gemTxProd;
			GEM_INC_DESC(pDrvCtrl->gemTxProd, GEM_TX_DESC_CNT);
			i++;
			used++;
			__asm__ volatile("sync");//必须要
//			MEM_BARRIER_RW1();
//		}
	}
	
//	printf("%s %d\n", __FUNCTION__, __LINE__);
#if 1/*ldf 20230728 add:: 避免Mblk资源释放不及时造成缓冲区满，这里改用拷贝方式，将数据拷贝出来后立即释放资源*/
	if (MblkPtr != NULL)/*zhaoyf add 20230728*/
	{
		netMblkClChainFree(MblkPtr);
	}
#endif
	
//	if(used > 1)/*ldf 20230728 add:: debug*/
//		printf("<**DEBUG**> [%s():_%d_]:: used=%d\n", __FUNCTION__, __LINE__,used);
	
	if(pMblk != NULL)
	{
		printf("<WARNNING> [%s():_%d_]:: No TXdescriptor!! \n", __FUNCTION__, __LINE__);
		goto noDescs;
	}
	
	/* Save the mBlk for later. */
	pDrvCtrl->gemTxMblk[lastIdx] = MblkPtr;
//	pDrvCtrl->gemTxMblk[lastIdx] = NULL;/*zhaoyf add 20230728*/
	
	/* Start the transmission */
	gemRegSetBit(pDrvCtrl, GEM_NCR, GEM_NCR_TSTART);
	
//	printf("<**DEBUG**> [%s():_%d_]:: Return 0! gSendTaskCount = %d\n", __FUNCTION__, __LINE__,gSendTaskCount);
	return 0;

noDescs:
	/*
	 * Ran out of descriptors: undo all relevant changes
	 * and fall back to copying.
	 */
	
	pDrvCtrl->gemTxProd = firstIdx;
	
	for (i = 0; i < used; i++)
	{
		pTxDesc = &pDrvCtrl->gemTxDescMem[pDrvCtrl->gemTxProd];
		pTxDesc->txBufAddr = 0;
#ifdef GEM_DMA_64BIT
		pTxDesc->txBufAddrHi = 0;
#endif
		if(pDrvCtrl->gemTxProd == (GEM_TX_DESC_CNT-1))
		{
			pTxDesc->txStatus = GEM_TX_WRAP_BIT;
		}
		else
		{
			pTxDesc->txStatus = 0;
		}
		pTxDesc->txStatus |= GEM_TX_USED_BIT;
		pDrvCtrl->gemTxFree++;
		GEM_INC_DESC(pDrvCtrl->gemTxProd, GEM_TX_DESC_CNT);
		__asm__ volatile("sync");
//		MEM_BARRIER_RW1();
	}
	
	pDrvCtrl->gemTxProd = firstIdx;
	
//	printf("<**DEBUG**> [%s():_%d_]:: Return -1! gSendTaskCount = %d\n", __FUNCTION__, __LINE__,gSendTaskCount);
	return -1;
}
#endif

#if 0//此方式ftp传输会断开
static int gemSendnew(GEM_DRV_CTRL *pDrvCtrl, M_BLK_ID MblkPtr)
{
	GEM_TX_DESC *pTxDesc;
	GEM_TX_DESC *pFirstTxDesc;
	char *pData;
	int length = 0 ;
	unsigned int uiState = 0;
	unsigned long physAddr;
	unsigned int lastIdx = 0;

	M_BLK_ID pMblk = MblkPtr;

//	printk("%s,%d\n",__FUNCTION__,__LINE__);
	pFirstTxDesc = &pDrvCtrl->gemTxDescMem[pDrvCtrl->gemTxProd];

	if (pMblk->m_next == NULL)//短包不copy,直接发送
	{
    	pTxDesc = &pDrvCtrl->gemTxDescMem[pDrvCtrl->gemTxProd];
//        printk("%s,%d pDesc->txBufAddr=0x%x,pTxDesc->txStatus=0x%x,pDrvCtrl->gemTxProd=%d\n",__FUNCTION__,__LINE__,
//        		pDesc->txBufAddr,pDesc->txStatus,pDrvCtrl->gemTxProd);
    	pTxDesc->txStatus &= GEM_TX_WRAP_BIT;

        pTxDesc->txStatus = pMblk->m_len & GEM_TX_BUF_LENTH_MASK;

        pTxDesc->txStatus |= GEM_TX_LAST_BIT ;//置1

        pTxDesc->txStatus &= (~GEM_TX_USED_BIT);//置0

//			printk("pTxDesc->txStatus=0x%x,pDrvCtrl->gemTxProd=%d\n",pTxDesc->txStatus,pDrvCtrl->gemTxProd);
		physAddr = GEM_VIRT_TO_PHYS((void*)pMblk->m_data);
//		printk("0x%x,0x%x,physAddr 0x%x\n",pMblk->m_data,pMblk,physAddr);
		pTxDesc->txBufAddr   = (unsigned int)GEM_ADDR_LO(physAddr);
#ifdef GEM_DMA_64BIT
		pTxDesc->txBufAddrHi = (unsigned int)GEM_ADDR_HI(physAddr);
#endif
		__asm__ volatile("sync");
//		cache2_flush(DATA_CACHE,(void *)pMblk->m_data,pMblk->m_len);//要刷cache
        pDrvCtrl->gemTxMblk[pDrvCtrl->gemTxProd] = pMblk;
        GEM_INC_DESC(pDrvCtrl->gemTxProd, GEM_TX_DESC_CNT);
        pDrvCtrl->gemTxFree--;
        gemRegSetBit(pDrvCtrl, GEM_NCR, GEM_NCR_TSTART);
        return 0;
	}

	while (pMblk != NULL )
	{

		pData = pMblk->m_data;
//		printk("%s,%d\n",__FUNCTION__,__LINE__);
//		memcpy(0xFFFFFFFFAF000000UL+length, pMblk->mBlkHdr.mData, pMblk->mBlkHdr.mLen);//固定dma地址测试
		memcpy(pDrvCtrl->gemTxDmaAddr + length, pMblk->mBlkHdr.mData, pMblk->mBlkHdr.mLen);//组包方式发送
//		bcopy(pMblk->mBlkHdr.mData, pDrvCtrl->gemTxDmaAddr + length, pMblk->mBlkHdr.mLen);
		length += pMblk->m_len;

		if (pMblk->m_next != NULL)
		{
			pMblk = pMblk->m_next;
			continue;
//			printk("%s,%d\n",__FUNCTION__,__LINE__);
		}

//		printk("%s,%d:length=%d,pData=0x%lx\n",__FUNCTION__,__LINE__,length,pData);

//		printk("%s,%d,pDrvCtrl->gemTxProd=0x%lx,\n",__FUNCTION__,__LINE__,pDrvCtrl->gemTxProd);
		pTxDesc = &pDrvCtrl->gemTxDescMem[pDrvCtrl->gemTxProd];
//		printk("%s,%d,pTxDesc=0x%lx,pTxDesc->txStatus=0x%x,length=%d\n",__FUNCTION__,__LINE__,
//				pTxDesc,pTxDesc->txStatus,length);

//		__asm__ volatile("sync");
		uiState = length & GEM_TX_BUF_LENTH_MASK;
//		if (pMblk->mBlkHdr.mNext == NULL)
//		{
			uiState |= GEM_TX_LAST_BIT ;
//			printk("%s,%d\n",__FUNCTION__,__LINE__);
//		}

		uiState &= (~GEM_TX_USED_BIT);
		/* WRAP: marks last descriptor in transmit buffer descriptor list */
		if (pDrvCtrl->gemTxProd == (GEM_TX_DESC_CNT-1))
		{
			uiState |= GEM_TX_WRAP_BIT;
		}
		pTxDesc->txStatus = uiState ;

//		printk("%s,%d,pTxDesc->txStatus=0x%x,pDrvCtrl->gemTxProd=%d\n",__FUNCTION__,__LINE__, pTxDesc->txStatus,pDrvCtrl->gemTxProd);
//		physAddr = GEM_VIRT_TO_PHYS((void*)pData);
//		physAddr = GEM_VIRT_TO_PHYS(0xFFFFFFFFAF000000UL);//固定dma地址测试
		physAddr = GEM_VIRT_TO_PHYS((void*)pDrvCtrl->gemTxDmaAddr);
//		printk("pData=0x%lx:PhysAddr=0x%lx\n",pData,physAddr);
		pTxDesc->txBufAddr 	 = GEM_ADDR_LO(physAddr);
#ifdef GEM_DMA_64BIT
		pTxDesc->txBufAddrHi = GEM_ADDR_HI(physAddr);
#endif
		__asm__ volatile("sync");//必须要 

//		printk("%s,%d,pTxDesc=0x%lx,pTxDesc->txStatus=0x%x,length=%d\n",__FUNCTION__,__LINE__,
//						pTxDesc,pTxDesc->txStatus,length);
		pMblk = pMblk->m_next;
		pDrvCtrl->gemTxFree--;
		lastIdx = pDrvCtrl->gemTxProd;
		GEM_INC_DESC(pDrvCtrl->gemTxProd, GEM_TX_DESC_CNT);

	}
//	printk("%s,%d\n",__FUNCTION__,__LINE__);

	pDrvCtrl->gemTxMblk[lastIdx] = MblkPtr;

	/* Start the transmission */
	gemRegSetBit(pDrvCtrl, GEM_NCR, GEM_NCR_TSTART);

//	printf("%s,%d nwctrl=0x%x\n",__FUNCTION__,__LINE__, gemRegRead32(pDrvCtrl, GEM_NCFGR));

	return 0;
}
#endif

M_BLK_ID gem_defrag(GEM_DRV_CTRL *pDrvCtrl, M_BLK_ID pMbuf)/*zhaoyf add 20230728*/
{
	M_BLK_ID    pMbufNew    = NULL;
	M_BLK_ID    pMbufItr;
	unsigned int FragCount   = 0;
	unsigned int len = 0;

	/* Verify if the packet needs fragmentation */
	for (pMbufItr = pMbuf; pMbufItr != NULL; pMbufItr = pMbufItr->m_next)
	{
		FragCount++;
//		printk_hex_data_send(pMbufItr->m_data, pMbufItr->m_len);
	}

	if (FragCount == 1)
	{
		return pMbuf;
	}

	/* An outgoing packet fits into one extended memory buffer */
	pMbufNew = endPoolTupleGet(pDrvCtrl->gemEndObj.pNetPool);
	if (pMbufNew == NULL)
	{
		printf("<WARNING> [%s():_%d_]:: Memory pool is not enough.\n", __FUNCTION__, __LINE__);
		netMblkClChainFree(pMbuf);
		return NULL;
	}

	len = netMblkToBufCopy (pMbuf, mtod(pMbufNew, char *), NULL);
    /*
     * Manually pad short frames, and zero the pad space to
     * avoid leaking data.
     */
//	if (len < ETHERSMALL)
//	{
//		bzero (mtod(pMbufNew, char *) + len, ETHERSMALL - len);
//		len += ETHERSMALL - len;
//	}
    
	pMbufNew->m_len = pMbufNew->m_pkthdr.len = len;
	pMbufNew->m_flags = pMbuf->m_flags;
	pMbufNew->m_pkthdr.csum_flags = pMbuf->m_pkthdr.csum_flags;
	pMbufNew->m_pkthdr.csum_data = pMbuf->m_pkthdr.csum_data;
	pMbufNew->m_pkthdr.vlan = pMbuf->m_pkthdr.vlan;
//	CSUM_IP_HDRLEN(pMbufNew) = CSUM_IP_HDRLEN (pMbuf);
	
	netMblkClChainFree(pMbuf);
	
//	printk("mtip_defrag after\n");
//	printk_hex_data_send(pMbufNew->m_data, pMbufNew->m_len);
	
	return pMbufNew;
}

//unsigned long long flag_gemEndSend = 0;/*zhaoyf add 20230728:: debug, send不过驱动，协议栈空发*/
static STATUS gemEndSend(END_OBJ *pEnd, M_BLK_ID MblkPtr)
{
	GEM_DRV_CTRL *pDrvCtrl;
	M_BLK_ID pTmp;
	int mBlkNum = 0;
	M_BLK_ID Mblkbuf;

	pDrvCtrl = (GEM_DRV_CTRL *)pEnd;
	
//	if(flag_gemEndSend)/*zhaoyf add 20230728:: debug, send不过驱动，协议栈空发*/
//	{
//		END_TX_SEM_TAKE(&pDrvCtrl->gemEndObj, WAIT_FOREVER);
//		endPoolTupleFree(MblkPtr);
////		printf("%s %d \n",__func__,__LINE__);
//		END_TX_SEM_GIVE(&pDrvCtrl->gemEndObj);
//
//		return 0;
//	}
	
	/* get the mBlk number in this packet */
	mBlkNum = 0;
	pTmp = MblkPtr;

	while (pTmp != NULL) 
	{
		mBlkNum++;
		pTmp = pTmp->mBlkHdr.mNext;
	}
	
	END_TX_SEM_TAKE(&pDrvCtrl->gemEndObj, WAIT_FOREVER);
	
    if(pDrvCtrl->gemTxFree < mBlkNum)
    {
    	printf("<WARNNING> [%s():_%d_]:: TxDescriptor is not enough!! pDrvCtrl->gemTxFree=%d, mBlkNum=%d\n", __FUNCTION__, __LINE__,pDrvCtrl->gemTxFree,mBlkNum);
    	goto blocked;
    }

#if 1 /*ldf 20230728 add:: 此处改为不分包发送，解决udp发送大包不通*/
    	Mblkbuf = gem_defrag(pDrvCtrl,MblkPtr);
    	if(Mblkbuf == NULL)
    	{
        	printf("<WARNNING> [%s():_%d_]:: Mblkbuf == NULL\n", __FUNCTION__, __LINE__);
        	goto blocked;
    	}
    	gemSend(pDrvCtrl,Mblkbuf);
#elif 0 /*ldf 20230809 add:: 参考ls2k  15000 就ping不通了*/
        if ( ((unsigned long)(MblkPtr->mBlkHdr.mData) & 0x10) || (MblkPtr->m_pkthdr.csum_flags & (CSUM_VLAN|CSUM_IP|CSUM_UDP) &&
            !(MblkPtr->m_pkthdr.csum_flags & CSUM_TCP) &&
            MblkPtr->m_pkthdr.len < ETHERSMALL) )   /*fixup unaillianed bug*/
        {
//        	printf("<**DEBUG**> [%s():_%d_]:: fixup unaillianed bug\n", __FUNCTION__, __LINE__);
        	Mblkbuf = gem_defrag(pDrvCtrl,MblkPtr);
        	if(Mblkbuf == NULL)
        	{
            	printf("<WARNNING> [%s():_%d_]:: Mblkbuf == NULL\n", __FUNCTION__, __LINE__);
            	goto blocked;
        	}
        	gemSend(pDrvCtrl,Mblkbuf);
        }
        else
        {
//        	printf("<**DEBUG**> [%s():_%d_]:: no unaillianed bug\n", __FUNCTION__, __LINE__);
        	gemSend(pDrvCtrl,MblkPtr);
        }
#else
    	gemSend(pDrvCtrl,MblkPtr);
#endif
	
//	COUNT_OUT_PKT(MblkPtr);
	END_TX_SEM_GIVE(&pDrvCtrl->gemEndObj);
//	printf("<**DEBUG**> [%s():_%d_]:: Return! \n", __FUNCTION__, __LINE__);
	return (0);
	
blocked:
	pDrvCtrl->gemTxStall = TRUE;
//	printf("gemEndSend(): blocked\n"); 
	END_TX_SEM_GIVE(&pDrvCtrl->gemEndObj);
	return (-1);
}

#if 0/* 资源回收函数 */
static int gemTbdClean(GEM_DRV_CTRL *pDrvCtrl)
{
	GEM_TX_DESC *pDesc;
    M_BLK_ID pMblk;
//    printk("%s,%d entry\n",__FUNCTION__,__LINE__);
    while (pDrvCtrl->gemTxFree < GEM_TX_DESC_CNT)
    {
		pDesc = &pDrvCtrl->gemTxDescMem[pDrvCtrl->gemTxCons];
		if (!(pDesc->txStatus & GEM_TX_USED_BIT))
		{
			break;
		}
		pMblk = pDrvCtrl->gemTxMblk[pDrvCtrl->gemTxCons];
//		printk("%s,%d pDesc=0x%lx,pMblk=0x%lx,pDrvCtrl->gemTxCons=%d\n",__FUNCTION__,__LINE__,pDesc,pMblk,pDrvCtrl->gemTxCons);
		if (pMblk != NULL)
		{
//			printk("%s,%d pMblk=0x%lx\n",__FUNCTION__,__LINE__,pMblk);
			endPoolTupleFree(pMblk);
			pDrvCtrl->gemTxMblk[pDrvCtrl->gemTxCons] = NULL;
		}
		pDesc->txBufAddr = 0;
//		if (pDesc->txStatus & GEM_TX_WRAP_BIT)
//		{
//			printk("%s,%d pDesc=0x%lx,pDrvCtrl->gemTxCons=%d\n",__FUNCTION__,__LINE__,pDesc,pDrvCtrl->gemTxCons);
//		}
		
		if(pDrvCtrl->gemTxCons == (GEM_TX_DESC_CNT-1))/*zhaoyf add 20230728*/
		{
			pDesc->txStatus &= GEM_TX_WRAP_BIT;//发送描述符环最后一个描述符标志保留
		}
		else
		{
			pDesc->txStatus = 0;//发送描述符环最后一个描述符标志保留
		}
//		pDesc->txStatus |= GEM_TX_USED_BIT;
#ifdef GEM_DMA_64BIT
		pDesc->txBufAddrHi = 0;
#endif
        pDrvCtrl->gemTxFree++;
        GEM_INC_DESC (pDrvCtrl->gemTxCons, GEM_TX_DESC_CNT);

    }
//    printk("%s,%d end.\n",__FUNCTION__,__LINE__);
    return 0;
}
#endif

static int gemIsLinkUp(int unit)
{
#ifdef _HC3020B_
	return 1;
#endif
	
	unsigned int uRegVal = 0;
	void *addr = NULL;
	
	printk("<**DEBUG**> [%s():_%d_]\n", __func__, __LINE__);
	addr = gem_resources[unit].gemBaseAddr;
	uRegVal = gemRead32((void *)(addr + GEM_PSR));
	printk("<**DEBUG**> [%s():_%d_]:: uRegVal=0x%x\n", __func__, __LINE__,uRegVal);
	if(uRegVal & 0x4)
		return 1;
	else
		return 0;
}

static int gemLinkUpate(GEM_DRV_CTRL *pDrvCtrl)
{
	unsigned int old_flag = 0;
	
	if(pDrvCtrl == NULL)
		return -1;
	
//	printk("<**DEBUG**> [%s():_%d_]\n", __func__, __LINE__);
	
	old_flag = END_FLAGS_GET (&pDrvCtrl->gemEndObj);
	
//	printk("<**DEBUG**> [%s():_%d_]\n", __func__, __LINE__);
	
	if(gemIsLinkUp(pDrvCtrl->unit)){
//		printk("<**DEBUG**> [%s():_%d_]\n", __func__, __LINE__);
		END_FLAGS_SET (&pDrvCtrl->gemEndObj, IFF_RUNNING);
//		printk("[GEM%d] Link is up.\n",pDrvCtrl->unit);
	}else{
//		printk("<**DEBUG**> [%s():_%d_]\n", __func__, __LINE__);
		END_FLAGS_CLR (&pDrvCtrl->gemEndObj, IFF_RUNNING);
//		printk("[GEM%d] Link is down.\n",pDrvCtrl->unit);
	}
//	printk("<**DEBUG**> [%s():_%d_]\n", __func__, __LINE__);
	return 0;
}

static void gemIntHandle(GEM_DRV_CTRL *pDrvCtrl)
{
	unsigned int int_status;
	unsigned int uRegVal;
	unsigned int tmp;

	gGemIntHandleCount++;/*中断处理次数统计*/
	
	int_status = gemRegRead32(pDrvCtrl, GEM_ISR);
	gemRegWrite32(pDrvCtrl, GEM_ISR, int_status);/*清中断*/
	
	tmp = int_status;
	while(tmp)
	{
		if(tmp & 0x1)
			gGemIntCount++;/*有效中断状态位统计*/
		
		tmp = tmp >> 1;
	}
	
	tmp = GEM_ISR_RCOMP | GEM_ISR_ROVR | GEM_ISR_TCOMP /*| GEM_ISR_RXUBR | GEM_ISR_TXUBR | GEM_ISR_LINK*/;
	if((int_status & (~tmp)) != 0)//产生了除接收和发送之外的其他中断
	{
		gOtherIntCount++;
//		printk("<**DEBUG**> [%s():_%d_]:: GEM%d int_status = 0x%x\n", __FUNCTION__, __LINE__,pDrvCtrl->unit,int_status);
	}
	
	if ((int_status & GEM_ISR_RCOMP) || (int_status & GEM_ISR_ROVR)) //receive_complete / reveive overrun
	{
//		if ((int_status & GEM_ISR_RCOMP) && (int_status & GEM_ISR_ROVR))/*ldf 20230804 add:: debug*/
//			printk("<**DEBUG**> [%s():_%d_]\n", __FUNCTION__, __LINE__);
		
		if(int_status & GEM_ISR_RCOMP)
		{
			gRecvIntCount++;/*接收完成中断统计*/
			int_status &= (~GEM_ISR_RCOMP);
		}
		if(int_status & GEM_ISR_ROVR)
		{
			gRecvOverRunIntCount++;/*接收溢出中断统计*/
			int_status &= (~GEM_ISR_ROVR);
		}
		
		
//		if (int_status != 0)/*ldf 20230804 add:: debug*/
//			printk("<**DEBUG**> [%s():_%d_]:: int_status = 0x%x\n", __FUNCTION__, __LINE__,int_status);

		uRegVal = gemRegRead32(pDrvCtrl, GEM_RSR);
		gemRegWrite32(pDrvCtrl, GEM_RSR, uRegVal);

		if((atomic_set(&pDrvCtrl->gemRxPending, TRUE) == FALSE))
		{
			uRegVal = GEM_IDR_RCOMP /*| GEM_IDR_RXUBR*/ | GEM_IDR_ROVR;     /* disable rx int */
			gemRegSetBit(pDrvCtrl, GEM_IDR, uRegVal);

			if ((netJobAdd((FUNCPTR)gemEndRxHandle, (unsigned long)pDrvCtrl, 0, 0, 0, 0)) == ERROR)
			{
				printk("<WARNING> [%s():_%d_]:: The netJobRing is full.\n", __FUNCTION__, __LINE__);
				goto rx_err;
			}
		}
		else
		{
//			printk("<WARNNING> [%s():_%d_]:: gemRxPending atomic_set TRUE failed.\n", __FUNCTION__, __LINE__);
		}
	}
//	if(int_status & GEM_ISR_RCOMP)
//	{
//		int_status &= (~GEM_ISR_RCOMP);
//	}
//	if(int_status & GEM_ISR_ROVR)
//	{
//		int_status &= (~GEM_ISR_ROVR);
//	}
	
	
	if (int_status & GEM_ISR_TCOMP)/* transmit complete */
	{
		gSendIntCount++;/*发送完成中断统计*/
		int_status &= (~GEM_ISR_TCOMP);
		
//		if (int_status != 0)/*ldf 20230804 add:: debug*/
//			printk("<**DEBUG**> [%s():_%d_]:: int_status = 0x%x\n", __FUNCTION__, __LINE__,int_status);
		
		uRegVal = gemRegRead32(pDrvCtrl, GEM_TSR);
		gemRegWrite32(pDrvCtrl, GEM_TSR, uRegVal);

		if((atomic_set(&pDrvCtrl->gemTxPending, TRUE) == FALSE))
		{
			uRegVal = GEM_IDR_TCOMP /*| GEM_IDR_TXUBR*/;     /* disable tx int */
			gemRegSetBit(pDrvCtrl, GEM_IDR, uRegVal);
			
			if ((netJobAdd((FUNCPTR)gemEndTxHandle, (unsigned long)pDrvCtrl, 0, 0, 0, 0)) == ERROR)
			{
				printk("<WARNING> [%s():_%d_]:: The netJobRing is full.\n", __FUNCTION__, __LINE__);
				goto tx_err;
			}
		}
		else
		{
//			printk("<WARNING> [%s():_%d_]:: gemEndTxHandle atomic_set TRUE failed.\n", __FUNCTION__, __LINE__);
		}
	}
//	if(int_status & GEM_ISR_TCOMP)
//	{
//		int_status &= (~GEM_ISR_TCOMP);
//	}
	
	
	if (int_status & GEM_ISR_LINK)/* link change */
	{
		gemLinkUpate(pDrvCtrl);
		int_status &= (~GEM_ISR_LINK);
		
//		if (int_status != 0)/*ldf 20230804 add:: debug*/
//			printk("<**DEBUG**> [%s():_%d_]:: int_status = 0x%x\n", __FUNCTION__, __LINE__,int_status);
	}
//	if(int_status & GEM_ISR_LINK)
//	{
//		int_status &= (~GEM_ISR_LINK);
//	}
	
//	printk("<**DEBUG**> [%s():_%d_]:: Return!\n", __FUNCTION__, __LINE__);
	
	return;
	
tx_err:
	uRegVal = GEM_IER_TCOMP /*| GEM_IER_TXUBR*/;     /*  enable tx int */
	gemRegSetBit(pDrvCtrl, GEM_IER, uRegVal);
	return;
rx_err:
	uRegVal = GEM_IER_RCOMP /*| GEM_IER_RXUBR*/ | GEM_IER_ROVR;     /*  enable rx int */
	gemRegSetBit(pDrvCtrl, GEM_IER, uRegVal);
	return;
}

static STATUS gemEndStart(END_OBJ *pEnd)
{
	GEM_DRV_CTRL *pDrvCtrl;
	pDrvCtrl = (GEM_DRV_CTRL *)pEnd;
	char gem_int_name[10];
	
	sprintf(gem_int_name,GEM_DEVICE_NAME"%d_int",pDrvCtrl->unit);
	//printf("%s,%d: %s\n",__FUNCTION__,__LINE__,gem_int_name);
	/* 确认mux有接收的函数*/
	if (pDrvCtrl->gemEndObj.receiveRtn == NULL)
	{
		printk("[ERROR]Start: error, no receiveRtn.\n");
		return (ERROR);
	}
	if (pDrvCtrl->bStarted)
	{
		printk("[WARNING]Start: device is started already.\n");
		return (OK);
	}

	pthread_mutex_lock2(&pDrvCtrl->gemDevSem, WAIT, NO_TIMEOUT);
	END_TX_SEM_TAKE (pEnd, WAIT_FOREVER);
    if (pEnd->flags & IFF_UP)
    {
        END_TX_SEM_GIVE (pEnd);
		pthread_mutex_unlock(&pDrvCtrl->gemDevSem);
        return (OK);
     }
    
	if(atomic_set(&pDrvCtrl->gemRxPending, FALSE) != TRUE)
	{
//		printk("<WARNNING> [%s():_%d_]:: gemRxPending atomic_set FALSE failed.\n", __FUNCTION__, __LINE__);
	}
	if(atomic_set(&pDrvCtrl->gemTxPending, FALSE) != TRUE)
	{
//		printk("<WARNNING> [%s():_%d_]:: gemTxPending atomic_set FALSE failed.\n", __FUNCTION__, __LINE__);
	}
	if(atomic_set(&pDrvCtrl->gemIntPending, FALSE) != TRUE)
	{
//		printk("<WARNNING> [%s():_%d_]:: gemIntPending atomic_set FALSE failed.\n", __FUNCTION__, __LINE__);
	}

//	sysGemStop(pDrvCtrl);
	pDrvCtrl->bStarted = TRUE;

	
//	END_FLAGS_SET (&pDrvCtrl->gemEndObj, IFF_ALLMULTI);/*ldf 20230731 add:: 接收所有多播帧*/
//	END_FLAGS_SET (&pDrvCtrl->gemEndObj, IFF_PROMISC);/*ldf 20230731 add:: 混杂模式*/
	gemEndRxConfig(pDrvCtrl);

	gemIntDisable(pDrvCtrl);
	gemIntClear(pDrvCtrl);
	/*设置END的标记*/
	END_FLAGS_SET (&pDrvCtrl->gemEndObj, IFF_UP | IFF_RUNNING);

	gemIntEnable(pDrvCtrl);
	printk("%s:%d,pDrvCtrl->irq=%d\n",__FUNCTION__,__LINE__,pDrvCtrl->irq);
//	int i = 0;
//	for(i = 0; i < 129; i++)
//	{
//		int_install_handler(gem_int_name, i, 1, (INT_HANDLER)gemIntHandle, (void *)pDrvCtrl);//加判断
//		int_enable_pic(i);
//	}
	int_install_handler(gem_int_name, pDrvCtrl->irq, 1, (INT_HANDLER)gemIntHandle, (void *)pDrvCtrl);//加判断
	int_enable_pic(pDrvCtrl->irq);
	
#ifndef _HC3020B_
	gem_reset_pcs();
#endif

	
	gemLinkUpate(pDrvCtrl);
//	sysGemStart(pDrvCtrl);
   	END_TX_SEM_GIVE (pEnd);
	pthread_mutex_unlock(&pDrvCtrl->gemDevSem);
	//printf("Start Complete.\r\n");

	return(OK);
}

static STATUS gemEndStop(END_OBJ *pEnd)
{
	GEM_DRV_CTRL *pDrvCtrl;
	pDrvCtrl = (GEM_DRV_CTRL *)pEnd;
	if (pDrvCtrl == NULL)
	{
		return ERROR;
	}
	if (!pDrvCtrl->bStarted)
	{
		printk("[Warning]: Stop:stry to stop a device not start.\n");
	}
	/* Take the config semaphore for mutex between Stop/Start/SetOptions */

	pthread_mutex_lock2(&pDrvCtrl->gemDevSem, WAIT, NO_TIMEOUT);
	sysGemStop(pDrvCtrl);

	pthread_mutex_unlock(&pDrvCtrl->gemDevSem);
	/* 应该还有停止中断的工作 */
	pDrvCtrl->bStarted = FALSE;
	END_FLAGS_CLR(&pDrvCtrl->gemEndObj, IFF_UP | IFF_RUNNING);

	return (OK);
}

static STATUS gemEndUnload(END_OBJ *pEnd)
{
	GEM_DRV_CTRL *pDrvCtrl;
	pDrvCtrl = (GEM_DRV_CTRL *)pEnd;

	if (pDrvCtrl == NULL)
	{
		return ERROR;
	}

	pthread_mutex_lock2(&pDrvCtrl->gemDevSem, WAIT, NO_TIMEOUT);
	/*停止硬件*/
	sysGemStop(pDrvCtrl);
	pDrvCtrl->bStarted = FALSE;

	/* 通知Mux停止设备 */
	END_OBJECT_UNLOAD(&pDrvCtrl->gemEndObj);
	/* 清理内存*/

	pthread_mutex_unlock(&pDrvCtrl->gemDevSem);//delete
	return (OK);
}

static int gemEndIoctl(END_OBJ *pEnd, int Cmd, caddr_t Data)
{
	GEM_DRV_CTRL *pDrvCtrl;
	pDrvCtrl = (GEM_DRV_CTRL *)pEnd;
	int Rc = OK;
	END_RCVJOBQ_INFO * qinfo;
	UINT32 nQs;
	
	switch ((unsigned)Cmd)
	{
		case EIOCSFLAGS: /*设置设备的选项,过程为停止/设置/启动*/
		case EIOCPOLLSTART:/*启动轮讯*/
		case EIOCPOLLSTOP:/*停止轮讯*/
		case EIOCSADDR: /*设置设备地址*/
		case GEM_EIOCSMPSPD: /*设置通讯速度*/
			Rc =  IoctlSetDevControl(pDrvCtrl, Cmd, Data);
			break;

    	case EIOCGFLAGS: /*获取设备选项*/
			if (Data == NULL)
			{
				Rc = 23;
			} else
			{
				*(long *)Data = END_FLAGS_GET(&pDrvCtrl->gemEndObj);
			}
			break;

    	case EIOCGADDR: /* 获取MAC地址 */
			if (Data == NULL)
			{
				Rc = 24;
			} else
			{
				bcopy((char *)END_HADDR(&pDrvCtrl->gemEndObj), (char *)Data,END_HADDR_LEN(&pDrvCtrl->gemEndObj));
			}
			break;

//    	case EIOCSADDR: /*设置设备地址*/ //待添加 ymk
//            if (Data == NULL){
//            	Rc = EINVAL;//待设置
//            }else {
//                bcopy((char *)Data,
//                      (char *)pDrvCtrl->gemMacAddr,
//                      ETHER_ADDR_LEN);
//
//                bcopy((char *)Data,
//                      (char *)pEnd->mib2Tbl.ifPhysAddress.phyAddress,
//                      ETHER_ADDR_LEN);
//
//                if (pEnd->pMib2Tbl != NULL)
//                    bcopy((char *)Data,
//                          (char *)pEnd->pMib2Tbl->m2Data.mibIfTbl.ifPhysAddress.phyAddress,
//                          ETHER_ADDR_LEN);
//                gemSetMacAddr(pDrvCtrl, (unsigned char *)Data);
//                gemEndRxConfig(pDrvCtrl);
//            }
//            break;

   		case EIOCGNAME: /* 获取设备名称 */
			if (Data == NULL)
			{
				Rc = 25; 
				break;
			}
			strcpy((char *)Data, GEM_DEVICE_NAME);
			break;
    	/* 获取以太网头的大小,vxWorks通过这个大小判断需要保留多少字节 */
    	case EIOCGHDRLEN:
        	if (Data == NULL)
        	{
        		Rc = 26; 
				break; 
			}
        	*(int *)Data = ETH_HDR_SIZE;
        	break;

	    /* 获取MIBII表的数量,vxWorks要用的*/
    	case EIOCGMIB2:
        	if (Data == NULL)
        	{
				Rc = 27;
				break;
			}
        	bcopy((char *)&pDrvCtrl->gemEndObj.mib2Tbl, (char *)Data,sizeof(pDrvCtrl->gemEndObj.mib2Tbl));
        	break;

	    /* 获取内存宽度,这个会影响内存分配,怎样工作值得一查 */
    	case EIOCGMWIDTH:
        	if (Data == NULL)
        	{
				Rc = 28;
				break;
			}
	        *(int *)Data = NONE;   /* no access restrictions */
        	break;

    	/* Are we a NPT toolkit device */
	    case EIOCGNPT:  /* NPT支持,怎样支持也不知道 */
    	    Rc = 29;
        	break;

	    /* Set receive callback */
    	case EIOCSRCVCB: /* 设置协议栈接收回调,不支持 */
        	Rc = 30;
        	break;

	    case EIOCMULTIADD: /* 增加组播地址 */
    	    if (Data == NULL)
    	    {
				Rc = 31;
				break;
			}
	        Rc = gemEndMCastAddrAdd(&pDrvCtrl->gemEndObj, (char*)Data);
        	break;

    	case EIOCMULTIGET: /* 获取组播地址 */
        	if (Data == NULL)
        	{
				Rc = 32;
				break;
			}
        	Rc = gemEndMCastAddrGet(&pDrvCtrl->gemEndObj, (MULTI_TABLE*)Data);
        	break;

	    case EIOCMULTIDEL:/* 删除组播地址 */
    	    if (Data == NULL)
    	    {
				Rc = 33;
				break;
			}
        	Rc = gemEndMCastAddrDel(&pDrvCtrl->gemEndObj, (char*)Data);
        	break;
	    case EIOCGFBUF: /* Get minimum first buffer for chaining (?)不支持 */
    	    Rc = 34;
        	break;

	    /* Get packet threshold and waitbound settings */
    	case GEM_EIOCGPKTTW:/*获取DMA的门限和边界设置,与硬件相关,可以不要*/
			Rc = 37;
        	break;

	    case EIOCGRCVJOBQ:
    	    if (Data == NULL)
    	    {
            	Rc = 38;
	    	    break;
    	    }            
		    qinfo = (END_RCVJOBQ_INFO *)Data;
      		nQs = qinfo->numRcvJobQs;
			qinfo->numRcvJobQs = 1;
			if (nQs < 1)
		    	Rc = ENOSPC;
			else
				qinfo->qIds[0] =  netJobQueueId;
			
	  		break;

    	default:
    		//printf("Ioctl: Bad request %X, Data=%X\n",Cmd,Data);
	    	Rc = 39;
    		break;
	}
	return(Rc);
}

static int IoctlSetDevControl(GEM_DRV_CTRL *pDrvCtrl, int Cmd, caddr_t Data)
{
    int Rc = OK;
    unsigned int OptionsToSet, OptionsToClear;
    long Value, OldFlags, NewFlags;
    int DeviceRunning = pDrvCtrl->bStarted;    
	
    /* Get the configuration semaphore */

    pthread_mutex_lock2(&pDrvCtrl->gemDevSem, WAIT, NO_TIMEOUT);
    /* Stop device if it is running */
	if (DeviceRunning)
	{
		sysGemStop(pDrvCtrl);
	}

    /* Initialize Options flags first */
    OptionsToSet = 0;
    OptionsToClear = 0;

    /* Perform configuration operation */
    switch(Cmd)
    {
        /* Set device flags.  These flags are those defined by VxWorks,
         * so we need to translate them to the appropriate device options.
         */
        case EIOCSFLAGS:
            OldFlags = END_FLAGS_GET(&pDrvCtrl->gemEndObj);
            Value = (long)Data;

            /* Negative argument clears flags */
            if (Value < 0)
            {
                Value = -Value;
                Value--;
                END_FLAGS_CLR(&pDrvCtrl->gemEndObj, Value);
            } else
            {
                END_FLAGS_SET(&pDrvCtrl->gemEndObj, Value);
            }

            /* Handle flags set or cleared by the command */
            NewFlags = END_FLAGS_GET(&pDrvCtrl->gemEndObj);

            /* IFF_BROADCAST */
            if ((NewFlags & IFF_BROADCAST) && !(OldFlags & IFF_BROADCAST))
            {
                OptionsToSet |= GEM_BROADCAST_OPTION;
            }
            else if ((OldFlags & IFF_BROADCAST) && !(NewFlags & IFF_BROADCAST))
            {
                OptionsToClear |= GEM_BROADCAST_OPTION;
            }

            /* IFF_PROMISC */
            if ((NewFlags & IFF_PROMISC) && !(OldFlags & IFF_PROMISC))
            {
                OptionsToSet |= GEM_PROMISC_OPTION;
            }
            else if ((OldFlags & IFF_PROMISC) && !(NewFlags & IFF_PROMISC))
            {
                OptionsToClear |= GEM_PROMISC_OPTION;
            }

            /* IFF_MULTICAST */
            if ((NewFlags & IFF_MULTICAST) && !(OldFlags & IFF_MULTICAST))
            {
                OptionsToSet |= GEM_MULTICAST_OPTION;
            }
            else if ((OldFlags & IFF_MULTICAST) && !(NewFlags & IFF_MULTICAST))
            {
                OptionsToClear |= GEM_MULTICAST_OPTION;
            }
            break;

        /* Set MAC address.  Store the input address in the MIB structure,
         * and give it to the driver.
         */
        case EIOCSADDR:
            if (Data == NULL)
            {
                Rc = EINVAL;
                break;
            }
//            printk("%s,%d\n",__FUNCTION__,__LINE__);
            /* Copy it to the MIB structure for VxWorks, then set it in the
             * device
             */
            bcopy((char *)Data,
                   (char *)pDrvCtrl->gemMacAddr,
                   ETHER_ADDR_LEN);
            bcopy((char *)Data, (char *)END_HADDR(&pDrvCtrl->gemEndObj),
                  END_HADDR_LEN(&pDrvCtrl->gemEndObj));
            gemSetMacAddr(pDrvCtrl, (unsigned char *)Data);

            break;
        /* Place the device into POLLED mode */
        case EIOCPOLLSTART:
            break;
        /* Take the device out of POLLED mode */
        case EIOCPOLLSTOP:
            break;

        /* Set the MAC-PHY MII data clock */
        case GEM_EIOCSMPSPD:
            Value = (long)Data;  /* prevents compiler warning */
            /* Validate speed argument. Support 10/100/1000 only */
            if ((Value != 10) && (Value != 100) && (Value != 1000))
            {
                Rc = ERROR;
                break;
            }
            Rc = 0;
            /*sysSetSpeed(pDrvCtrl,(unsigned int)Value);*/
            break;
        default:
            Rc = EINVAL;
            break;
    }

    /* Restore device state */
    if (DeviceRunning)
    {
        sysGemStart(pDrvCtrl);
    }
    /* Return the configuration semaphore and exit */

    pthread_mutex_unlock(&pDrvCtrl->gemDevSem);
    return(Rc);
}

static unsigned short cdnsBitGet (UINT8 *pucMac, unsigned short puiBit)
{
	unsigned short  uiByte;

    uiByte   = pucMac[puiBit >> 3];
    uiByte >>= (puiBit & 0x7);
    uiByte  &= 1;

    return  (uiByte);
}

static unsigned short cdnsMacHashCalc (UINT8 *pucMac)
{
    int   iIdxBit;
    int   iMacBit;
    unsigned short  uiHashIdx;

    uiHashIdx = 0;
    iMacBit   = 5;

    for (iIdxBit = 5; iIdxBit >= 0; iIdxBit--)
    {
        uiHashIdx |= (cdnsBitGet(pucMac, iMacBit)
                  ^   cdnsBitGet(pucMac, iMacBit + 6)
                  ^   cdnsBitGet(pucMac, iMacBit + 12)
                  ^   cdnsBitGet(pucMac, iMacBit + 18)
                  ^   cdnsBitGet(pucMac, iMacBit + 24)
                  ^   cdnsBitGet(pucMac, iMacBit + 30)
                  ^   cdnsBitGet(pucMac, iMacBit + 36)
                  ^   cdnsBitGet(pucMac, iMacBit + 42))
                  <<  iIdxBit;
        iMacBit--;
    }

    return  (uiHashIdx);
}

static void gemEndHashTblPopulate(GEM_DRV_CTRL *pDrvCtrl)
{

    UINT32             uiHashH = 0;
    UINT32             uiHashL = 0;
    UINT32             uiHashIdx;

	ETHER_MULTI *mCastNode = NULL;

	if (pDrvCtrl->gemEndObj.flags & IFF_ALLMULTI) /*接收所有多播帧*/
	{
		/* set all multicast mode */
		gemRegWrite32(pDrvCtrl,GEM_HRB, 0xffffffff);
		gemRegWrite32(pDrvCtrl,GEM_HRT, 0xffffffff);
		return;
	}

	/* first, clear out the original filter */
	gemRegWrite32(pDrvCtrl,GEM_HRB, 0x0);
	gemRegWrite32(pDrvCtrl,GEM_HRT, 0x0);
//lstFirst -> iplstFirst
	//lstNext ->iplstNext
	/* now repopulate it */
	for (mCastNode = (ETHER_MULTI *)lstFirst((LIST*)&pDrvCtrl->gemEndObj.multiList);
		 mCastNode != NULL;
		 mCastNode = (ETHER_MULTI *)lstNext((NODE*)&mCastNode->node))
	{

		/*
		 * The upper 6 bits of the calculated CRC are used to
		 * index the contens of the hash table.
		 */

        uiHashIdx = cdnsMacHashCalc((UINT8 *)mCastNode->addr);

        if (uiHashIdx >= 64)
        {
            printk("Gem hash calculation out of range %d.\n", uiHashIdx);
            break;
        }
        if (uiHashIdx < 32)
        {
            uiHashL |= (1 << uiHashIdx);
        } else
        {
            uiHashH |= (1 << (uiHashIdx - 32));
        }
	}
//	printk("%s,%d uiHashL=0x%x,uiHashH=0x%x\n",__FUNCTION__,__LINE__,uiHashL,uiHashH);

	/* reload filter */
	gemRegWrite32(pDrvCtrl,GEM_HRB, uiHashL);
	gemRegWrite32(pDrvCtrl,GEM_HRT, uiHashH);

	return;
}


static void gemEndRxConfig(GEM_DRV_CTRL *pDrvCtrl)
{
	UINT32 uiRegVal;

	uiRegVal = gemRegRead32(pDrvCtrl,GEM_NCFGR);
	uiRegVal &= ~(GEM_NCFGR_CAF | GEM_NCFGR_MTIHEN);
	
	/* setup the MAC source address */
	

	/* enable promisc mode, if specified */
	if (pDrvCtrl->gemEndObj.flags & IFF_PROMISC)		/* 混杂模式 */
	{
		printk("%s() __%d__   IFF_PROMISC\n",__func__,__LINE__);
		/* copy all frame (promisc) */
		uiRegVal |= GEM_NCFGR_CAF;
	} else if (pDrvCtrl->gemEndObj.flags & IFF_ALLMULTI)/* 接收所有多播帧 */
	{
		printk("%s() __%d__   IFF_ALLMULTI\n",__func__,__LINE__);
		uiRegVal &= ~GEM_NCFGR_CAF;
		uiRegVal |= GEM_NCFGR_UNIHEN;
		uiRegVal |= GEM_NCFGR_MTIHEN;
		gemRegWrite32(pDrvCtrl,GEM_HRB, 0xffffffff);
		gemRegWrite32(pDrvCtrl,GEM_HRT, 0xffffffff);
	} else if (pDrvCtrl->gemEndObj.flags & IFF_MULTICAST)/* 接收指定多播帧 */
	{
		printk("%s() __%d__   IFF_MULTICAST\n",__func__,__LINE__);
		uiRegVal &= ~GEM_NCFGR_CAF;
		uiRegVal |= GEM_NCFGR_UNIHEN;
		uiRegVal |= GEM_NCFGR_MTIHEN;
		gemEndHashTblPopulate(pDrvCtrl);

	} else 												/* 禁止接收多播帧 */
	{
		printk("%s() __%d__   other\n",__func__,__LINE__);
		uiRegVal &= ~(GEM_NCFGR_CAF | GEM_NCFGR_MTIHEN);
		gemRegWrite32(pDrvCtrl,GEM_HRB, 0x0);
		gemRegWrite32(pDrvCtrl,GEM_HRT, 0x0);
	}

	/* jumbo frame support */
	if (pDrvCtrl->gemMaxMtu == GEM_JUMBO_MTU)
	{
		uiRegVal |= GEM_NCFGR_JFRAME;
	}
	else
	{
		uiRegVal &= ~GEM_NCFGR_JFRAME;
	}
	
	gemRegWrite32(pDrvCtrl,GEM_NCFGR, uiRegVal);
}

static STATUS gemEndMCastAddrAdd(END_OBJ *pEnd, char *pAddr)
{	
	int retVal;
	GEM_DRV_CTRL *pDrvCtrl;

	pDrvCtrl = (GEM_DRV_CTRL *)pEnd;

	pthread_mutex_lock2(&pDrvCtrl->gemDevSem, WAIT, NO_TIMEOUT);

	if (!(pDrvCtrl->gemEndObj.flags & IFF_UP))
	{
		pthread_mutex_unlock(&pDrvCtrl->gemDevSem);
		return (OK);
	}

	retVal = etherMultiAdd (&pEnd->multiList, pAddr);
	if (retVal == ENETRESET)
	{
		pEnd->nMulti++;
		gemEndHashTblPopulate((GEM_DRV_CTRL *)pEnd);
	}

	pthread_mutex_unlock(&pDrvCtrl->gemDevSem);
	return (OK);
}

static STATUS gemEndMCastAddrDel(END_OBJ *pEnd, char *pAddr)
{
	int retVal;
	GEM_DRV_CTRL *pDrvCtrl;

	pDrvCtrl = (GEM_DRV_CTRL*)pEnd;

	pthread_mutex_lock2(&pDrvCtrl->gemDevSem, WAIT, NO_TIMEOUT);

    if (!(pDrvCtrl->gemEndObj.flags & IFF_UP))
    {
		pthread_mutex_unlock(&pDrvCtrl->gemDevSem);
        return (OK);
    }

    retVal = etherMultiDel (&pEnd->multiList, pAddr);
    if (retVal == ENETRESET)
    {
        pEnd->nMulti--;
        gemEndHashTblPopulate((GEM_DRV_CTRL *)pEnd);
    }

	pthread_mutex_unlock(&pDrvCtrl->gemDevSem);
    return (OK);
}

static STATUS gemEndMCastAddrGet(END_OBJ *pEnd, MULTI_TABLE *pTable)
{
    int retVal;
    GEM_DRV_CTRL * pDrvCtrl;

    pDrvCtrl = (GEM_DRV_CTRL *)pEnd;
	pthread_mutex_lock2(&pDrvCtrl->gemDevSem, WAIT, NO_TIMEOUT);

    if (!(pDrvCtrl->gemEndObj.flags & IFF_UP))
    {
		pthread_mutex_unlock(&pDrvCtrl->gemDevSem);
        return (OK);
    }

    retVal = etherMultiGet (&pEnd->multiList, pTable);

	pthread_mutex_unlock(&pDrvCtrl->gemDevSem);

    return (retVal);
}

/*PollSend not support*/
static STATUS gemEndPollSend(END_OBJ *pEnd, M_BLK_ID MblkPtr)
{
	return OK;
}
/*PollRecv not support*/
static STATUS gemEndPollRecv(END_OBJ *pEnd, M_BLK_ID MblkPtr)
{
	return OK;
}


static void sysGemStart(GEM_DRV_CTRL *pDrvCtrl)
{

}

static void sysGemStop(GEM_DRV_CTRL *pDrvCtrl)
{

}


static unsigned char *gemGetMacAddr(GEM_DRV_CTRL *pDrvCtrl)
{
	unsigned char macaddr[ETHER_ADDR_LEN] = {0};

	memcpy(macaddr, hr3gem_default_macaddr, ETHER_ADDR_LEN);

	macaddr[3] = hr3gem_default_macaddr[3] + ((sysGetCabnNum() << 4) | sysGetCaseNum());
	macaddr[4] = hr3gem_default_macaddr[4] + sysGetSlotNum() + sysCpuGetID();/*ldf 20230522*/
	macaddr[5] = hr3gem_default_macaddr[5] + sysCpuGetID() + pDrvCtrl->unit;

    memcpy(pDrvCtrl->gemMacAddr, macaddr, ETHER_ADDR_LEN);

	return pDrvCtrl->gemMacAddr;
}

static void gemSetMacAddr(GEM_DRV_CTRL *pDrvCtrl,unsigned char *MacAddr)
{	
	unsigned int MacAddrLow = 0;
	unsigned int MacAddrHigh = 0;

	MacAddrLow = (MacAddr[3] << 24)
		       | (MacAddr[2] << 16)
               | (MacAddr[1] << 8)
        	   | (MacAddr[0] << 0);
	MacAddrHigh = (MacAddr[5] << 8) | (MacAddr[4]);

	gemRegWrite32(pDrvCtrl,GEM_SAB1, MacAddrLow);
	gemRegWrite32(pDrvCtrl,GEM_SAT1, MacAddrHigh & 0xFFFF);

	/* Clear unused address reg     */
	gemRegWrite32(pDrvCtrl, GEM_SAB2, 0x0);
	gemRegWrite32(pDrvCtrl, GEM_SAT2, 0x0);
	gemRegWrite32(pDrvCtrl, GEM_SAB3, 0x0);
	gemRegWrite32(pDrvCtrl, GEM_SAT3, 0x0);
	gemRegWrite32(pDrvCtrl, GEM_SAB4, 0x0);
	gemRegWrite32(pDrvCtrl, GEM_SAT4, 0x0);

	gemRegWrite32(pDrvCtrl, GEM_TIDM1, 0x0);
	gemRegWrite32(pDrvCtrl, GEM_TIDM2, 0x0);
	gemRegWrite32(pDrvCtrl, GEM_TIDM3, 0x0);
	gemRegWrite32(pDrvCtrl, GEM_TIDM4, 0x0);

	return;

}
static int gemGetMTU(GEM_DRV_CTRL *pDrvCtrl)
{
	return GEM_MTU;
}

static void gemIntEnable(GEM_DRV_CTRL *pDrvCtrl)
{
	u32 val = 0;
	
//	val = 0xffffffff;/*ldf 20230804 add:: debug*/
	val = GEM_IER_RCOMP | GEM_IER_TCOMP /*| GEM_IER_LPI | GEM_IER_PFNZ*/ | GEM_IER_ROVR | GEM_IER_LINK /*| GEM_IER_TFC | GEM_IER_RLEX*/;
	gemRegWrite32(pDrvCtrl,GEM_IER, val);
	return;
}

static void gemIntDisable(GEM_DRV_CTRL *pDrvCtrl)
{	
	u32 val = 0;
	
//	val = 0xffffffff;/*ldf 20230804 add:: debug*/
	val = GEM_IDR_RCOMP | GEM_IDR_TCOMP /*| GEM_IDR_LPI | GEM_IDR_PFNZ*/ | GEM_IDR_ROVR | GEM_IDR_LINK /*| GEM_IDR_TFC | GEM_IDR_RLEX*/;
	gemRegWrite32(pDrvCtrl,GEM_IDR, val);
	return;
}

static void gemIntClear(GEM_DRV_CTRL *pDrvCtrl)
{
	gemRegRead32(pDrvCtrl,GEM_ISR);
	return;
}

/*gmac0 PhyAddr: 0x7, gmac1 PhyAddr: 0x6*/
u32 gemPhyRead (int unit, u8 ucPhyAddr, u8 ucRegAddr)
{
	void *atBaseAddr = gem_resources[unit].gemBaseAddr;
    u32 uiRetryCnt = 0;
    u32 uiRetry = 300000;
    u32 uiRegVal;
    u32 pusValue;

//    while ((gemRegRead32(pDrvCtrl, GEM_NSR) & GEM_NSR_IDLE) == 0); /* Wait until bus idle          */
    while ((gemRead32((void *)(atBaseAddr + GEM_NSR)) & GEM_NSR_IDLE) == 0);


    uiRegVal = (~GEM_MAN_WZO & GEM_MAN_CLTTO)                     /* Write maintain register      */
             | (GEM_MAN_OP(1 ? 0x2 : 0x1))
             | GEM_MAN_WTN(0x02)
             | GEM_MAN_PHYA(ucPhyAddr)
             | GEM_MAN_REGA(ucRegAddr)
             | GEM_MAN_DATA(0) ;

//    gemRegWrite32(pDrvCtrl, GEM_MAN, uiRegVal);
    gemWrite32((void *)(atBaseAddr + GEM_MAN), uiRegVal);

//    while (!((gemRegRead32(pDrvCtrl, GEM_NSR) & GEM_NSR_IDLE) > 0))
    while (!((gemRead32((void *)(atBaseAddr + GEM_NSR)) & GEM_NSR_IDLE) > 0))
    {
        uiRetryCnt++;
        if (uiRetryCnt >= uiRetry)
		{
            printk("Phy read timeout\n");
            return  (0);
        }
    }

//    while ((gemRegRead32(pDrvCtrl, GEM_NSR) & GEM_NSR_IDLE) == 0); /* Wait until bus idle          */
    while ((gemRead32((void *)(atBaseAddr + GEM_NSR)) & GEM_NSR_IDLE) == 0);

//    pusValue = gemRegRead32(pDrvCtrl, GEM_MAN) & GEM_MAN_DATA_Msk;
    pusValue = gemRead32((void *)(atBaseAddr + GEM_MAN)) & GEM_MAN_DATA_Msk;

    return  pusValue;
}

void gem_reset_pcs(int unit)
{
	void *addr = gem_resources[unit].gemBaseAddr;
	unsigned int reg_offset = GEM_PCR;
	unsigned int val;
	
	val = gemRead32((void *)(addr + reg_offset));
	val |= (1<<9) | (1<<15);/*restart auto-neg*/
	gemWrite32((void *)(addr + reg_offset), val);
	
	val = gemRead32((void *)(addr + reg_offset));
	val |= (1<<12);/*restart pcs*/
	gemWrite32((void *)(addr + reg_offset), val);
}

void gem_reg_dump(int unit, unsigned int reg_offset)
{
	void *addr;
	u32 val;
	if(unit >= 4)
	{
		printk("total gem number is 4\n");
		return;
	}
	addr = gem_resources[unit].gemBaseAddr;
//	printk("addr=0x%lx\n",addr);
//	printk("gem%d baseaddr = 0x%lx \n",unit,(u64)addr);
	
	val = gemRead32((void *)(addr+reg_offset));
	printk("offset = 0x%04x val = 0x%08x \n",reg_offset,val);
}

void gem_reg_dump_all(int unit)/*有些寄存器访问会报data bus error*/
{
	int i;
	
	for (i=0;i<=0x34;i=i+4)
	{
		if(i == 0xc)
			continue;
		gem_reg_dump(unit, i);
	}
	
	//tx_underruns 0x134
	gem_reg_dump(unit, 0x134);
	
	//rx error 0x190 - 0x1b0
	for (i=0x190;i<=0x1b0;i=i+4)
	{
		gem_reg_dump(unit, i);
	}
	
	//pcs 0x200 - 0x220
	for (i=0x200;i<=0x220;i=i+4)
	{
		gem_reg_dump(unit, i);
	}
}

/*调试用,释放所有rx描述符*/
void gemFreeRxDesc(int unit)
{
	GEM_DRV_CTRL *pDrvCtrl;
	GEM_RX_DESC *pRxDesc;
	int gemRxIdx = 0;
	
	if(g_pDrvCtrl[unit] != NULL)
		pDrvCtrl = g_pDrvCtrl[unit];

	pRxDesc = &pDrvCtrl->gemRxDescMem[gemRxIdx];
	
	while((gemRxIdx < GEM_RX_DESC_CNT) && (pRxDesc != NULL))
	{
		pRxDesc->rxBufAddr &= ~GEM_RX_OWNERSHIP_BIT;
		pRxDesc->rxStatus = 0;
		
		gemRxIdx++;
		pRxDesc = &pDrvCtrl->gemRxDescMem[gemRxIdx];
	}
}

/*统计信息*/
void gem_info_dump(void)
{
	int i = 0;
	unsigned int gemRxFree = 0;
	unsigned int gemRxIdx = 0;
	GEM_DRV_CTRL *pDrvCtrl;
	GEM_RX_DESC *pRxDesc;
	
	printf("======================================\n");
	/* 中断计数统计 */
	printf("中断处理次数统计:      %llu\n",gGemIntHandleCount);
	printf("有效中断状态位统计:    %llu\n",gGemIntCount);
	printf("接收完成中断:          %llu\n",gRecvIntCount);
	printf("接收溢出中断:          %llu\n",gRecvOverRunIntCount);
	printf("发送完成中断:          %llu\n",gSendIntCount);
	printf("其他中断:              %llu\n\n",gOtherIntCount);
	
	/*收发次数统计*/
	printf("有效的接收次数:        %llu\n",gRecvCount);
	printf("有效的发送次数:        %llu\n",gSendCount);
	printf("实际发送处理次数:      %llu\n\n",gSendCountTotal);	
	
	/*描述符剩余统计*/
	for(i = 0; i < GEM_MAX_DEV; i++)
	{
		if(g_pDrvCtrl[i] != NULL)
			pDrvCtrl = g_pDrvCtrl[i];
		else
			continue;
		
		/*发送描述符剩余统计*/
		printf("gem%d 发送描述符剩余:   %lu\n",i,pDrvCtrl->gemTxFree);
		
		/*接收描述符剩余统计*/
		gemRxIdx = 0;
		gemRxFree = 0;
		pRxDesc = &pDrvCtrl->gemRxDescMem[gemRxIdx];
		
		while((gemRxIdx < GEM_RX_DESC_CNT) && (pRxDesc != NULL))
		{
			if((pRxDesc->rxBufAddr & GEM_RX_OWNERSHIP_BIT) == 0)//空闲
				gemRxFree++;
			
			gemRxIdx++;
			pRxDesc = &pDrvCtrl->gemRxDescMem[gemRxIdx];
		}
		printf("gem%d 接收描述符剩余:   %lu\n",i,gemRxFree);
	}
	printf("======================================\n");
}

