/* pci_autoconfig_lib.h - PCI bus automatic resource allocation facility */

#ifndef __INCpciAutoConfigLibh
#define __INCpciAutoConfigLibh

#ifdef __cplusplus
extern "C" {
#endif

#include <reworks/types.h>
#include <string.h>
#include <stdio.h>
#include <sys/errno.h>
#include "hr3cdnsPci.h"


#ifdef USE_PCI_SIMULATOR
#include "stdio.h"
#include "stdlib.h"
#endif /* USE_PCI_SIMULATOR */


    
#if 0
typedef unsigned char  UINT8 ;
typedef unsigned short UINT16 ;
typedef unsigned int   UINT32 ;
typedef int STATUS;
typedef int BOOL;

#define  LOCAL static
#define OK 0
#define ERROR (-1)

typedef int (*FUNCPTR) ();

#endif
#ifdef __cplusplus
typedef int 		(*FUNCPTR) (...);     /* ptr to function returning int */
typedef void 		(*VOIDFUNCPTR) (...); /* ptr to function returning void */
typedef double 		(*DBLFUNCPTR) (...);  /* ptr to function returning double*/
typedef float 		(*FLTFUNCPTR) (...);  /* ptr to function returning float */
#else
typedef int 		(*FUNCPTR) ();	   /* ptr to function returning int */
typedef void 		(*VOIDFUNCPTR) (); /* ptr to function returning void */
typedef double 		(*DBLFUNCPTR) ();  /* ptr to function returning double*/
typedef float 		(*FLTFUNCPTR) ();  /* ptr to function returning float */
#endif			/* _cplusplus */

#define OK (0)
#define ERROR (-1)


#define	PCI_CFG_DEV_INT_LINE	0x3c
#define	PCI_CFG_BASE_ADDRESS_0	0x10
#define	PCI_CFG_BASE_ADDRESS_1	0x14
#define	PCI_CFG_BASE_ADDRESS_2	0x18
#define	PCI_CFG_BASE_ADDRESS_3	0x1c
#define	PCI_CFG_BASE_ADDRESS_4	0x20
#define	PCI_CFG_BASE_ADDRESS_5	0x24

#define M107_VEN_ID		    0x1057	     /* Motorola Vendor ID */
#define M107_DEV_ID		    0x0006	     /* MPC107 for 8245 Device ID */
#define M107_DEV_VEN_ID   	0x00021057   /* (DevId << 16) + VenId */
#define PPMC8245_ID	        0x00061057   /* Vendor & Dev Id for Kahlua */

#define PAL_BASE 0x78800000
#define M107_PCICMD_ENCPP1  	0x00000006	/* PCI COMMAND Default value */
#define M107_PICR1_ENCPP1    	0x00141b18	/* PICR1 setting sp 0xff041b18 encpp1 0x00141010*/
#define M107_PICR2_ENCPP1    	0x04040000	/* PICR2 setting sp 0x04040004 encpp1 0x04040000*/
#define M107_FLASH_WRITE_BIT	(0x1 << 12)

#define PCI_AUTO_ATTR_DEV_EXCLUDE	(unsigned char)(0x01) /* Exclude this device */
#define PCI_AUTO_ATTR_DEV_DISPLAY	(unsigned char)(0x02) /* (S)VGA disp device  */
#define PCI_AUTO_ATTR_DEV_PREFETCH	(unsigned char)(0x04) /* Device requests PF  */

#define PCI_AUTO_ATTR_BUS_PREFETCH	(unsigned char)(0x08) /* Bridge implements   */
#define PCI_AUTO_ATTR_BUS_PCI		(unsigned char)(0x10) /* PCI-PCI Bridge      */
#define PCI_AUTO_ATTR_BUS_HOST		(unsigned char)(0x20) /* PCI Host Bridge     */
#define PCI_AUTO_ATTR_BUS_ISA		(unsigned char)(0x40) /* PCI-ISA Bridge      */
#define PCI_AUTO_ATTR_BUS_4GB_IO	(unsigned char)(0x80) /* 4G/64K IO Adresses  */

/*added by kun begin*/
//#define FIX_UP_LOONGSON_2K
/*added by kun end*/

/* OPTION COMMANDS for use with pciAutoCfgCtl() */

/* 0 used for pSystem structure copy */
#define PCI_PSYSTEM_STRUCT_COPY		0x0000

/* 1-7 reserved for Fast Back To Back functions */
#define PCI_FBB_ENABLE			0x0001
#define PCI_FBB_DISABLE			0x0002
#define PCI_FBB_UPDATE			0x0003
#define PCI_FBB_STATUS_GET		0x0004

/* 8-11 reserved for MAX_LAT */
#define PCI_MAX_LATENCY_FUNC_SET	0x0008
#define PCI_MAX_LATENCY_ARG_SET		0x0009
#define PCI_MAX_LAT_ALL_SET		0x000a
#define PCI_MAX_LAT_ALL_GET		0x000b

/* 12-15 reserved for message output (logMsg) */
#define PCI_MSG_LOG_SET			0x000c

/* 16-47 reserved for pSystem functionality */
#define PCI_MAX_BUS_SET			0x0010
#define PCI_MAX_BUS_GET			0x0011
#define PCI_CACHE_SIZE_SET		0x0012
#define PCI_CACHE_SIZE_GET		0x0013
#define PCI_AUTO_INT_ROUTE_SET		0x0014
#define PCI_AUTO_INT_ROUTE_GET		0x0015
#define PCI_MEM32_LOC_SET		0x0016
#define PCI_MEM32_SIZE_SET		0x0017
#define PCI_MEMIO32_LOC_SET		0x0018
#define PCI_MEMIO32_SIZE_SET		0x0019
#define PCI_IO32_LOC_SET		0x001a
#define PCI_IO32_SIZE_SET		0x001b
#define PCI_IO16_LOC_SET		0x001c
#define PCI_IO16_SIZE_SET		0x001d
#define PCI_INCLUDE_FUNC_SET		0x001e
#define PCI_INT_ASSIGN_FUNC_SET		0x001f
#define PCI_BRIDGE_PRE_CONFIG_FUNC_SET	0x0020
#define PCI_BRIDGE_POST_CONFIG_FUNC_SET	0x0021
#define PCI_ROLLCALL_FUNC_SET		0x0022

/* 48-511 reserved for other memory configuration */
#define PCI_MEM32_SIZE_GET		0x0030
#define PCI_MEMIO32_SIZE_GET		0x0031
#define PCI_IO32_SIZE_GET		0x0032
#define PCI_IO16_SIZE_GET		0x0033

/* 512-0xffff available for misc items */
#define PCI_TEMP_SPACE_SET		0x0200
#define PCI_MINIMIZE_RESOURCES		0x0201

#ifndef _ASMLANGUAGE

typedef struct pci_mem_ptr
    {
    void *	pMem;
    int		memSize;
    } PCI_MEM_PTR;

typedef struct /* PCI_LOC, a standard bus location */
    {
	unsigned char bus;
    unsigned char device;
    unsigned char function;
    unsigned char attribute;
    unsigned char offset;       /* interrupt routing for this device */
    } PCI_LOC;

/* PCI identification structure */

typedef struct
    {
    PCI_LOC loc;
    unsigned int devVend;
    } PCI_ID;


/* obsolete structure */
typedef struct pci_system /* PCI_SYSTEM, auto configuration info */
    {
    unsigned long pciMem64;		/* 64 bit prefetchable memory location */
    unsigned int pciMem64Size;		/* 64 bit prefetchable memory size */
    unsigned long pciMemIo64;		/* 64 bit non-prefetchable memory location */
    unsigned int pciMemIo64Size;	/* 64 bit non-prefetchable memory size */
    unsigned int pciIo32;		/* 32 bit io location */
    unsigned int pciIo32Size;		/* 32 bit io size */
    unsigned int pciIo16;		/* 16 bit io location */
    unsigned int pciIo16Size;		/* 16 bit io size */
    int maxBus;			/* Highest subbus number */
    int cacheSize;		/* cache line size */
    unsigned int maxLatency;		/* max latency */
    int autoIntRouting;        /* automatic routing strategy */
    int (* includeRtn)	/* returns OK to include */
	   (
	   struct pci_system * pSystem,
	   PCI_LOC * pLoc,
	   unsigned int devVend
	   );
    unsigned char(* intAssignRtn)	/* returns int line, given int pin */
	   (
	   struct pci_system * pSystem,
	   PCI_LOC * pLoc,
	   unsigned char pin
	   );
    void (* bridgePreConfigInit) /* bridge pre-enumeration initialization */
	   (
	   struct pci_system * pSystem,
	   PCI_LOC * pLoc,
	   unsigned int devVend
	   );
    void (* bridgePostConfigInit)/* bridge post-enumeration initialization */
	   (
	   struct pci_system * pSystem,
	   PCI_LOC * pLoc,
	   unsigned int devVend
	   );
    int (* pciRollcallRtn) ();  /* Roll call check */
    } PCI_SYSTEM;

typedef int (*PCI_LOGMSG_FUNC)(char *fmt, int i1, int i2, int i3, int i4, int i5, int i6);
typedef unsigned char (*PCI_MAX_LAT_FUNC)(int bus, int device, int func, void *pArg);
typedef unsigned int (*PCI_MEM_BUS_EXTRA_FUNC)(int bus, int device, int func, void *pArg);
typedef int (*PCI_INCLUDE_FUNC)(PCI_SYSTEM *pSystem, PCI_LOC *pLoc, unsigned int devVend);
typedef unsigned char (*PCI_INT_ASSIGN_FUNC)(PCI_SYSTEM *pSystem, PCI_LOC *pLoc, unsigned int devVend);
typedef void (*PCI_BRIDGE_PRE_CONFIG_FUNC)(PCI_SYSTEM *pSystem, PCI_LOC *pLoc, unsigned int devVend);
typedef void (*PCI_BRIDGE_POST_CONFIG_FUNC)(PCI_SYSTEM *pSystem, PCI_LOC *pLoc, unsigned int devVend);
typedef int (*PCI_ROLL_CALL_FUNC)();

void pci_auto_config (PCI_SYSTEM *);
int pci_auto_get_next_class ( PCI_SYSTEM *pSystem, PCI_LOC *pciFunc,
                             unsigned int *index, unsigned int pciClass, unsigned int mask);
int pci_auto_bus_number_set ( PCI_LOC * pPciLoc, unsigned int primary, unsigned int secondary,
			    unsigned int subordinate);
int pci_auto_dev_reset ( PCI_LOC * pPciLoc);
int pci_auto_addr_align ( unsigned long base, unsigned long limit, unsigned int reqSize,
			  unsigned long *pAlignedBase );
void pci_auto_func_enable ( PCI_SYSTEM * pSystem, PCI_LOC * pFunc);
void pci_auto_func_disable ( PCI_LOC *pPciFunc);
unsigned int pci_auto_reg_config ( PCI_SYSTEM * pSystem, PCI_LOC *pPciFunc,
    		unsigned int baseAddr, unsigned int nSize, unsigned int addrInfo);

void * pci_auto_config_lib_init(void * pArg);
int pci_auto_cfg_ctl ( void *pCookie, int cmd, void *pArg );
int pci_auto_cfg( void *pCookie );
PCI_LOC* ls2k_filter_pci_list(int controller,int port,PCI_LOC* pPciList,int* pListSize);


#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif


#ifndef PCI_AUTO_MAX_FUNCTIONS
# define PCI_AUTO_MAX_FUNCTIONS 64/*32*/
#endif /* PCI_AUTO_MAX_FUNCTIONS */

typedef struct pci_auto_config_opts
    {
    /* copy of pSystem from pciAutoConfig() interface */
    unsigned long pciMem64;		/* 64 bit prefetchable memory location */
    unsigned int pciMem64Size;		/* 64 bit prefetchable memory size */
    unsigned long pciMemIo64;		/* 64 bit non-prefetchable memory location */
    unsigned int pciMemIo64Size;	/* 64 bit non-prefetchable memory size */
    unsigned int pciIo32;		/* 32 bit io location */
    unsigned int pciIo32Size;		/* 32 bit io size */
    unsigned int pciIo16;		/* 16 bit io location */
    unsigned int pciIo16Size;		/* 16 bit io size */
    int maxBus;			/* Highest subbus number */
    int cacheSize;		/* cache line size */
    unsigned int maxLatency;		/* max latency */
    int autoIntRouting;        /* automatic routing strategy */
    PCI_INCLUDE_FUNC		includeRtn;
    PCI_INT_ASSIGN_FUNC		intAssignRtn;
    PCI_BRIDGE_PRE_CONFIG_FUNC	bridgePreConfigInit;
    PCI_BRIDGE_POST_CONFIG_FUNC	bridgePostConfigInit;
    PCI_ROLL_CALL_FUNC		pciRollcallRtn;

    /* new stuff not available in the obsolete PCI_SYSTEM structure */

    int		pciConfigInit;		/* internal use only */
    /* Fast Back TO Back Enable */
    int		pciFBBEnable;		/* Enabled for system */
    int		pciFBBActive;		/* implemented all cards & turned on */
    /* memory allocation */
    unsigned int		pciMemBusMinRes;	/* minimum to reserve per bus */
    unsigned int		pciMemBusExtraRes;	/* extra to reserve per bus */
    unsigned int		pciMemMax;		/* maximum total to reserve */
//    unsigned int		pciMem32Used;		/* total 32-bit mem actually used */
//    unsigned int		pciMemIo32Used;		/* total 32-bit IOmem used */
//    unsigned int		pciIo32Used;		/* total 32-bit IO space used */
//    unsigned int		pciIo16Used;		/* total 16-bit IO space used */
    PCI_MEM_BUS_EXTRA_FUNC pciMemBusExtraFunc;	/* per bus, function to calculate */
    /* misc functions */
    PCI_LOGMSG_FUNC	pciLogMsgFunc;		/* safe logMsg() func */
    PCI_MAX_LAT_FUNC	pciMaxLatFunc;		/* MAX_LAT calc each device */
    void *		pciMaxLatPArg;		/* user-supplied arg */
    /* PCI_AUTO_MAX_FUNCTIONS */
    PCI_LOC *		pFuncList;		/* user-supplied space */
    int			numFuncListEntries;	/* number of entries available */
    int		minimizeResources;	/* sort resource requirements */
    } PCI_AUTO_CONFIG_OPTS;

#endif /* __INCpciAutoConfigLibh */
