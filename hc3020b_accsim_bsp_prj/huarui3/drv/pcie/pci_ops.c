//#include "../../h/io_asm.h"
//#include <reworks/types.h>
#include "hr3cdnsPci.h"

#if 0

#define	outb(a,v)	PCIE_CFG_REG_SET_1B((a), (v))
//#define	out8(a,v)	(*(volatile unsigned char*)(a) = (v))
#define	outw(a,v)	PCIE_CFG_REG_SET_2B((a), (v))
//#define	out16(a,v)	outw(a,v)
#define	outl(a,v)	PCIE_CFG_REG_SET_4B((a), (v))
//#define	out32(a,v)	outl(a,v)
#define	inb(a)		PCIE_CFG_REG_GET_1B(a)
//#define	in8(a)		(*(volatile unsigned char*)(a))
#define	inw(a)		PCIE_CFG_REG_GET_2B(a)
//#define	in16(a)		inw(a)
#define	inl(a)		PCIE_CFG_REG_GET_4B(a)
//#define	in32(a)		inl(a)

#else

#define	outb(a,v)	(*(volatile unsigned char*)(a) = (v))
#define	out8(a,v)	(*(volatile unsigned char*)(a) = (v))
#define	outw(a,v)	(*(volatile unsigned short*)(a) = (v))
#define	out16(a,v)	outw(a,v)
#define	outl(a,v)	(*(volatile unsigned int*)(a) = (v))
#define	out32(a,v)	outl(a,v)
#define	inb(a)		(*(volatile unsigned char*)(a))
#define	in8(a)		(*(volatile unsigned char*)(a))
#define	inw(a)		(*(volatile unsigned short*)(a))
#define	in16(a)		inw(a)
#define	inl(a)		(*(volatile unsigned int*)(a))
#define	in32(a)		inl(a)

#endif

u8 pci_in_byte(unsigned long port)
{
	return inb(port);
}

u16 pci_in_word(unsigned long port)
{
	return inw(port);
}

u32	pci_in_long(unsigned long port)
{
	return inl(port);
}

void pci_out_byte(unsigned long port, u8 val)
{
	outb(val, port);
}

void pci_out_word(unsigned long port, u16 val)
{
	outw(val, port);
}

void pci_out_long(unsigned long port, u32 val)
{
	outl(val, port);
}
