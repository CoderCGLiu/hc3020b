/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件实现了PCI自动配置所需的参数及接口
 * 修改：
 * 		
 */

/* includes */

#include <reworks/types.h>
#include <pci/pcibus.h>
#include "pci_autoconfig_lib.h"


/* forward declarations */
static u8 sys_pci_autoconfig_intasgn ( PCI_SYSTEM * pSys, PCI_LOC * pFunc,
    u8 intPin );
static int sys_pci_autoconfig_include ( PCI_SYSTEM *pSys, PCI_LOC *pciLoc,
    u32 devVend );



/*******************************************************************************
 * 
 * 确定PCI设备是否要自动配置
 * 
 * 		本程序用于确定PCI设备是否要自动配置。
 * 
 * 输入：
 * 		pSys：自动配置系统信息
 *      pciLoc：设备的PCI地址
 *      devVend：设备/厂商号
 * 输出：
 * 		无
 * 返回：
 * 		若设备要自动配置则返回0，否则返回-1
 */
static int sys_pci_autoconfig_include
    (
    PCI_SYSTEM *pSys,		
    PCI_LOC *pciLoc,		
    u32     devVend		
    )
    {
    int retVal = OS_OK;

    /* If it's the host bridge then exclude it */

//    if ((pciLoc->bus == 0) && (pciLoc->device == 0) && (pciLoc->function == 0))
//	return ERROR;


    switch(devVend)
	{

	/* TODO - add any excluded devices by device/vendor ID here */

	default:
	    retVal = OS_OK;
	    break;
	}

    return retVal;
    }
/*******************************************************************************
 * 
 * PCI设备中断号自动分配
 * 
 * 		本程序用于自动分配PCI设备中断号。
 * 
 * 输入：
 * 		pSys：自动配置系统信息
 *      pFunc：设备的PCI地址
 *      intPin：中断引脚
 * 输出：
 * 		无
 * 返回：
 * 		PCI设备的中断号
 */
extern int  __hr3PciIrqGet (int iBus, int iDev, int iFun, void *  pvIrq);
static u8 sys_pci_autoconfig_intasgn
(
	PCI_SYSTEM * pSys,		/* input: AutoConfig system information */
	PCI_LOC * pFunc,
	u8 intPin 		/* input: interrupt pin number */
)
{
	u8 irqValue = 0xff;    /* Calculated value */

	/*ldf 20230911 add:: 固定返回中断号 PCIE_X16_INT_LINK0(74)*/
	__hr3PciIrqGet(pFunc->bus, pFunc->device, pFunc->function, (void*)&irqValue);
	
	if (intPin == 0)
		return irqValue;

	return (irqValue);
}


/*******************************************************************************
 * 
 * PCI设备自动配置程序
 * 
 * 		本程序初始化PCI配置所需的PCI_SYSTEM结构，包括预取、非预取内存地址、16位/32位I/O地址等。
 * 
 * 输入：
 * 		pSys：自动配置系统信息
 *      pFunc：设备的PCI地址
 *      intPin：中断引脚
 * 输出：
 * 		无
 * 返回：
 * 		PCI设备的中断号
 */
void sys_pci_auto_config()
{
    static PCI_SYSTEM sysParams;
    
    extern int pciex16_device_type_probe(void);
    if(pciex16_device_type_probe() == 0) //ep mode
    	return;

    /*ldf 20230912 ignore:: 不需要使用PCIE IO空间*/
//    sysParams.pciIo16 = PCIE_IO_BASE_ADDR;//IO空间基地址
//    sysParams.pciIo16Size = 1 << PCIE_IO_SPACE_SIZE;//IO空间大小

	/*warning:the mem start and limit will be modified later*/
    sysParams.pciMemIo64 = PCIE_MEM_BASE_ADDR;//PCIE MEM空间基地址
    sysParams.pciMemIo64Size =  1 << PCIE_MEM_SPACE_SIZE;//PCIE MEM 空间大小 
    
    sysParams.cacheSize = PCI_CACHE_LINE_SIZE;
    sysParams.maxLatency = PCI_LATENCY_TIMER;

	sysParams.maxBus = 10;//总线最大值
    sysParams.autoIntRouting = FALSE;

    sysParams.includeRtn = sys_pci_autoconfig_include ;
    sysParams.intAssignRtn = sys_pci_autoconfig_intasgn;

    sysParams.pciRollcallRtn = NULL;

    printk("pci_auto_config\n");
    pci_auto_config(&sysParams);//pci header auto config

    return;
}

