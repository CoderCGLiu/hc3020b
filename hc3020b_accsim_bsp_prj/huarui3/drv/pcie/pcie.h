#ifndef PCIE_H
#define PCIE_H

#include <stdio.h>
#include <string.h>
#include <reworks/types.h>
#include "../../h/drvCommon.h"
#include <pci/pcibus.h>

//#include "../../../mylog/mylog.h"/*ldf 20230528 add:: log*/

#define PCIEX16_DBG_PRINT //dbg printf on

#define uart_printf printk

#define debug_print  printk

#ifdef PCIEX16_DBG_PRINT
#define debug_print1  printk
#else
#define debug_print1 0
#endif


typedef enum {
    TLP_CFG0 = 4,
    TLP_CFG1 = 5
} E_CFG_TYPE;

typedef enum {
    OB_TYPE_CFG0 = 0b1010,
    OB_TYPE_CFG1 = 0b1011,
    OB_TYPE_MEM = 0b0010,
    OB_TYPE_IO = 0b0110,
    OB_TYPE_MSGVD = 0b1101,
    OB_TYPE_MSGNM = 0b1100
} E_OB_TYPE;


    //----------------------------------------------- sysctrl
    #define PCIE_X16_LINK0_EN     0b0000000001
    #define PCIE_X16_LINK1_EN     0b0000000010
    #define PCIE_X16_LINK2_EN     0b0000000100
    #define PCIE_X16_LINK3_EN     0b0000001000
    #define PCIE_SRIO_0_LINK0_EN  0b0000010000
    #define PCIE_SRIO_0_LINK1_EN  0b0000100000
    #define PCIE_SRIO_1_LINK0_EN  0b0001000000
    #define PCIE_SRIO_1_LINK1_EN  0b0010000000
    #define PCIE_HSIO_LINK0_EN    0b0100000000
    #define PCIE_HSIO_LINK1_EN    0b1000000000
    
    #define ADDR_SYS_CFG0_R          0x001f0c0004
    #define ADDR_SYS_CFG1_R          0x001f0c0008
    #define ADDR_SYS_CTRL_R          0x001f0c0010
    #define ADDR_CLOCK_GATING_EN_CFG 0x001f0c00f4

    #define SRAM0_S                  0x0010000000
    #define SRAM0_S_CK               0x0010100000
    #define SRAM0_UNS                0x0010400000
    #define SRAM0_UNS_CK             0x0010500000


    #define SRAM1_S                  0x0011000000
    #define SRAM1_UNS                0x0011400000
    #define DDR0_S                   0x0001000000
    #define DDR0_S_CK                0x0003000000

    #define DDR0_UNS                 0x2000000000ull
    #define DDR0_UNS_CK              0x2003000000ull

    #define RC                   0b1
    #define EP                   0b0

    #define AD_4M                0b0
    #define AD_128M              0b1
    #define AD_4G                0b10

    #define PIC_IRQ_X16_PCIE0     74
    #define PIC_IRQ_X16_PCIE1     75
    #define PIC_IRQ_X16_PCIE2     76
    #define PIC_IRQ_X16_PCIE3     77

    #define BIT(n)  (1 << n)
    
    #define PCIE_X16_MSI_INTR_ENABLE   0x10000000
    #define pcie_x16_DMA_INTR_ENABLE   0x00000400
    #define PCIE_X16_MSG_INTR_ENABLE   0x00000040
    #define PCIE_X4_MSI_INTR_ENABLE    0x00010000
    #define pcie_x4_DMA_INTR_ENABLE    0x00000400
    #define PCIE_X4_MSG_INTR_ENABLE    0x00000040

    #define PCIE_X4         0
    #define PCIE_X2         1
    #define PCIE_X1X1       2
    #define PCIE_X1         3


//{-----------------------------------------------  reg_base_addr
    //X16
    #define PCIE_X16_LINK0_IP_BASE_ADDR                 0x1a000000
    #define PCIE_X16_LINK1_IP_BASE_ADDR                 0x1a800000
    #define PCIE_X16_LINK2_IP_BASE_ADDR                 0x1b000000
    #define PCIE_X16_LINK3_IP_BASE_ADDR                 0x1b800000
    #define PCIE_X16_LINK0_MISC_BASE_ADDR               0x1f450000
    #define PCIE_X16_LINK1_MISC_BASE_ADDR               0x1f460000
    #define PCIE_X16_LINK2_MISC_BASE_ADDR               0x1f470000
    #define PCIE_X16_LINK3_MISC_BASE_ADDR               0x1f480000

    #define PCIE_X16_PHY_IP_BASE_ADDR                   0x1f400000
    #define PCIE_X16_PHY_MISC_BASE_ADDR                 0x1f440000
    
    //SRIO
    #define PCIE_SRIO_REGION0_LINK0_IP_BASE_ADDR        0x1c000000
    #define PCIE_SRIO_REGION0_LINK0_MISC_BASE_ADDR      0x1c700000
    #define PCIE_SRIO_REGION0_LINK1_IP_BASE_ADDR        0x1c800000
    #define PCIE_SRIO_REGION0_LINK1_MISC_BASE_ADDR      0x1cf00000
    #define PCIE_SRIO_REGION0_PHY_IP_BASE_ADDR          0x1f680000
    #define PCIE_SRIO_REGION0_PHY_MISC_BASE_ADDR        0x1f6c0000

    #define PCIE_SRIO_REGION1_LINK0_IP_BASE_ADDR        0x1d000000
    #define PCIE_SRIO_REGION1_LINK0_MISC_BASE_ADDR      0x1d700000
    #define PCIE_SRIO_REGION1_LINK1_IP_BASE_ADDR        0x1d800000
    #define PCIE_SRIO_REGION1_LINK1_MISC_BASE_ADDR      0x1df00000
    #define PCIE_SRIO_REGION1_PHY_IP_BASE_ADDR          0x1f780000
    #define PCIE_SRIO_REGION1_PHY_MISC_BASE_ADDR        0x1f7c0000


    //HSIO
    #define PCIE_HSIO_LINK0_IP_BASE_ADDR                0x19000000
    #define PCIE_HSIO_LINK1_IP_BASE_ADDR                0x19800000

    #define PCIE_HSIO_LINK0_MISC_BASE_ADDR              0x19700000
    #define PCIE_HSIO_LINK1_MISC_BASE_ADDR              0x19f00000

    #define PCIE_HSIO_PHY_IP_BASE_ADDR                  0x1f3c0000
    #define PCIE_HSIO_PHY_MISC_BASE_ADDR                0x1f340000

//}-----------------------------------------------


//{----------------------------------------------- X16 reg

    //X16 LINK MISC reg
    #define PCIE_X16_CONFIG0                     0x0
    #define PCIE_X16_LINK_EQUALIZATION           0x4
    #define PCIE_X16_PIPE_MUX_CFG_LANE_0_1       0x8
    #define PCIE_X16_PIPE_MUX_CFG_LANE_2_3       0xc
    #define PCIE_X16_L1_PM_SUBSTATES             0x10
    #define PCIE_X16_L1_PM_SUBSTATES_OUT         0x14
    #define PCIE_X16_SOFT_RESET                  0x1c
    #define PCIE_X16_DEBUG_DATA_OUT_TRIG_MASK    0x20
    #define PCIE_X16_DEBUG_CONTROL               0x24
    #define PCIE_X16_DEBUG_WR_ADDR               0x28
    #define PCIE_X16_DEBUG_RD_ADDR               0x2c
    #define PCIE_X16_DEBUG_DATA                  0x30
    #define PCIE_X16_PERFORMACE_ENABLE           0x38
    #define PCIE_X16_FORMACE_CLEAR               0x3c
    #define PCIE_X16_PERFORMACE_CNT0             0x40
    #define PCIE_X16_PERFORMACE_CNT1             0x44
    #define PCIE_X16_PERFORMACE_CNT2             0x48
    #define PCIE_X16_PERFORMACE_CNT3             0x4c
    #define PCIE_X16_PERFORMACE_CNT4             0x50
    #define PCIE_X16_PERFORMACE_CNT5             0x54
    #define PCIE_X16_PERFORMACE_CNT6             0x58
    #define PCIE_X16_PERFORMACE_CNT7             0x5c
    #define PCIE_X16_PERFORMACE_CNT8             0x60
    #define PCIE_X16_PERFORMACE_CNT9             0x64
    #define PCIE_X16_PERFORMACE_CNT10            0x68
    #define PCIE_X16_PERFORMACE_CNT11            0x6c
    #define PCIE_X16_PERFORMACE_CNT12            0x70
    #define PCIE_X16_PERFORMACE_CNT13            0x74
    #define PCIE_X16_PERFORMACE_CNT14            0x78
    #define PCIE_X16_PERFORMACE_CNT15            0x7c
    #define PCIE_X16_PERFORMACE_CNT16            0x80
    #define PCIE_X16_PERFORMACE_CNT17            0x84
    #define PCIE_X16_CONFIG_INPUT_OUTPUT_0       0x800
    #define PCIE_X16_CONFIG_INPUT_OUTPUT_1       0x804
    #define PCIE_X16_INTR_0                      0x808
    #define PCIE_X16_INTR_1                      0x80c
    #define PCIE_X16_MSI_MASK0                   0x814
    #define PCIE_X16_MSI_MASK1                   0x818
    #define PCIE_X16_MSI_PENDING_ST0             0x834
    #define PCIE_X16_MSI_PENDING_ST1             0x838
    #define PCIE_X16_ERROR_INDICATION            0x854
    #define PCIE_X16_F0_VSEC_CONTROL             0x858
    #define PCIE_X16_F1_VSEC_CONTROL             0x85c
    #define PCIE_X16_POWER_STATE                 0x888
    #define PCIE_X16_AXI_SIDEBAND_SIGNALS        0x88c
    #define PCIE_X16_MSG_RECV_CTRL               0x890
    #define PCIE_X16_MSG_RECV_FREE_CNT           0x894
    #define PCIE_X16_MSG_RECV_WORD_CNT           0x898
    #define PCIE_X16_MSG_RECV_THRESHOLD          0x89c
    #define PCIE_X16_MSG_RECV_INTR_MASK          0x8A0
    #define PCIE_X16_MSG_RECV_INTR_STATUS        0x8A4
    #define PCIE_X16_MSG_RECV_FIFO_DATA          0x8A8
    #define PCIE_X16_MSG_RECV_FIFO_DATA_OFF      0x8AC
    #define PCIE_X16_MSG_RECV_STORE              0x8B0
    #define PCIE_X16_LTSSM_STATE_0_3             0x8d8
    #define PCIE_X16_LTSSM_STATE_4_7             0x8DC
    #define PCIE_X16_LTSSM_STATE_8_11            0x8E0
    #define PCIE_X16_LTSSM_STATE_12_15           0x8E4
    #define PCIE_X16_RESET_OUT                   0x8E8
    #define PCIE_X16_FLR                         0x8F8
    #define PCIE_X16_MSIR                        0x8fc
    #define PCIE_X16_MSISR                       0x900
    #define PCIE_X16_MSIIR0                      0x904
    #define PCIE_X16_MSIIR1                      0x908
    #define PCIE_X16_MSIIR2                      0x90c
    #define PCIE_X16_MSIIR3                      0x910
    #define PCIE_X16_MSIIR4                      0x914
    #define PCIE_X16_MSIIR5                      0x918
    #define PCIE_X16_MSIIR6                      0x91c
    #define PCIE_X16_MSIIR7                      0x920
    #define PCIE_X16_MSIVR0                      0x924
    #define PCIE_X16_MSIVR1                      0x928
    #define PCIE_X16_MSIVR2                      0x92c
    #define PCIE_X16_MSIVR3                      0x930
    #define PCIE_X16_MSIVR4                      0x934
    #define PCIE_X16_MSIVR5                      0x938
    #define PCIE_X16_MSIVR6                      0x93c
    #define PCIE_X16_MSIVR7                      0x940
    #define PCIE_X16_FUNC_POWER_STATE            0x950
    #define PCIE_X16_FUNC_STATE                  0x954
    #define PCIE_X16_TPH_ST_MODE                 0x958
    #define PCIE_X16_PIPE_MUX_CFG_LANE_4_5       0x960
    #define PCIE_X16_PIPE_MUX_CFG_LANE_6_7       0x964
    #define PCIE_X16_PIPE_MUX_CFG_LANE_8_9       0x968
    #define PCIE_X16_PIPE_MUX_CFG_LANE_10_11     0x96c
    #define PCIE_X16_PIPE_MUX_CFG_LANE_12_13     0x970
    #define PCIE_X16_PIPE_MUX_CFG_LANE_14_15     0x974
    #define PCIE_X16_PIPE_M2P_MESSAGEBUS_31_0    0x978
    #define PCIE_X16_PIPE_M2P_MESSAGEBUS_63_32   0x97c
    #define PCIE_X16_PIPE_M2P_MESSAGEBUS_95_64   0x980
    #define PCIE_X16_PIPE_M2P_MESSAGEBUS_127_96  0x984
    #define PCIE_X16_INTERRUPT_SIDEBAND_MASK0    0x1000
    #define PCIE_X16_INTERRUPT_SIDEBAND_MASK1    0x1004
    #define PCIE_X16_INTERRUPT_SIDEBAND_STATUS0  0x1008
    #define PCIE_X16_INTERRUPT_SIDEBAND_STATUS1  0x100c
    #define PCIE_X16_INTX_IN                     0x1010
    #define PCIE_X16_INT_PENDING_STATUS          0x1014
    #define PCIE_X16_INTR_STATUS                 0x1018
    #define PCIE_X16_INTR_ENABLE                 0x101c
    #define PCIE_X16_DPA_INTR                    0x1020
    #define PCIE_X16_VSEC_INTR                   0x1024
    #define PCIE_X16_INTR_FORCE                  0x1028

    //X16 PHY MISC reg
    #define PCIE_X16_PIPE_CFG0        0x0
    #define PCIE_X16_PIPE_CFG1        0x4
    #define PCIE_X16_PIPE_CFG2        0x8
    #define PCIE_X16_PIPE_CFG3        0xc
    #define PCIE_X16_PIPE_CFG4        0x10
    #define PCIE_X16_PIPE_CFG5        0x14
    #define PCIE_X16_PIPE_CFG6        0x18
    #define PCIE_X16_PIPE_CFG7        0x1c
    #define PCIE_X16_PIPE_CFG8        0x20
    #define PCIE_X16_PIPE_CFG9        0x24
    #define PCIE_X16_PIPE_CFG10       0x28
    #define PCIE_X16_PIPE_CFG11       0x2c
    #define PCIE_X16_PIPE_CFG12       0x30
    #define PCIE_X16_PIPE_CFG13       0x34
    #define PCIE_X16_PIPE_CFG14       0x38
    #define PCIE_X16_PIPE_CFG15       0x3c
    #define PCIE_X16_PIPE_RATE        0x40
    #define PCIE_X16_PHY_LINE_CFG0    0x44
    #define PCIE_X16_PHY_LINE_CFG1    0x48
    #define PCIE_X16_PHY_MODE         0x4c
    #define PCIE_X16_PHY_INTR         0x50
    #define PCIE_X16_PHY_RESET        0x54
    #define PCIE_X16_PHY_CFG0         0x58
    #define PCIE_X16_PHY_CFG1         0x5c
    #define PCIE_X16_DTB_DATA         0x60
//}-----------------------------------------------
   
//{----------------------------------------------- X4 reg
    //SRIO LINK MISC reg
    #define PCIE_X4_CONFIG0                     0x0
    #define PCIE_X4_LINK_EQUALIZATION           0x4
    #define PCIE_X4_PIPE_MUX_0                  0x8         
    #define PCIE_X4_PIPE_MUX_1                  0xc
    #define PCIE_X4_L1_PM_SUBSTATES             0x10
    #define PCIE_X4_L1_PM_SUBSTATES_OUT         0x14
    #define PCIE_X4_SOFT_RESET                  0x1c
    #define PCIE_X4_DEBUG_DATA_OUT_TRIG_MASK    0x20
    #define PCIE_X4_DEBUG_CONTROL               0x24
    #define PCIE_X4_DEBUG_WR_ADDR               0x28
    #define PCIE_X4_DEBUG_RD_ADDR               0x2c
    #define PCIE_X4_DEBUG_DATA                  0x30
    #define PCIE_X4_PERFORMACE_ENABLE           0x38
    #define PCIE_X4_FORMACE_CLEAR               0x3c
    #define PCIE_X4_PERFORMACE_CNT0             0x40
    #define PCIE_X4_PERFORMACE_CNT1             0x44
    #define PCIE_X4_PERFORMACE_CNT2             0x48
    #define PCIE_X4_PERFORMACE_CNT3             0x4C
    #define PCIE_X4_PERFORMACE_CNT4             0x50
    #define PCIE_X4_PERFORMACE_CNT5             0x54
    #define PCIE_X4_PERFORMACE_CNT6             0x58
    #define PCIE_X4_PERFORMACE_CNT7             0x5C
    #define PCIE_X4_PERFORMACE_CNT8             0x60
    #define PCIE_X4_PERFORMACE_CNT9             0x64
    #define PCIE_X4_PERFORMACE_CNT10            0x68
    #define PCIE_X4_PERFORMACE_CNT11            0x6C
    #define PCIE_X4_PERFORMACE_CNT12            0x70
    #define PCIE_X4_PERFORMACE_CNT13            0x74
    #define PCIE_X4_PERFORMACE_CNT14            0x78
    #define PCIE_X4_PERFORMACE_CNT15            0x7C
    #define PCIE_X4_PERFORMACE_CNT16            0x80
    #define PCIE_X4_PERFORMACE_CNT17            0x84
    #define PCIE_X4_PIPE_FS                     0x88
    #define PCIE_X4_PIPE_LF                     0x8c
    #define PCIE_X4_PIPE_EQ_RATE                0x90
    #define PCIE_X4_PIPE_M2P_MSGBUS             0x94
    #define PCIE_X4_PIPE_LN_M2P_MSGBUS          0x98
    #define PCIE_X4_PIPE_LN_P2M_MSGBUS          0x9c
    #define PCIE_X4_PIPE_LN_IORECAL             0xa0
    #define PCIE_X4_CONFIG_INPUT_OUTPUT_0       0x800
    #define PCIE_X4_CONFIG_INPUT_OUTPUT_1       0x804
    #define PCIE_X4_INTR_0                      0x808
    #define PCIE_X4_INTR_1                      0x80c
    #define PCIE_X4_INTR_2                      0x810
    #define PCIE_X4_MSI_MASK0                   0x814
    #define PCIE_X4_MSI_MASK1                   0x818
    #define PCIE_X4_MSI_MASK2                   0x81C
    #define PCIE_X4_MSI_MASK3                   0x820
    #define PCIE_X4_MSI_MASK4                   0x824
    #define PCIE_X4_MSI_MASK5                   0x828
    #define PCIE_X4_MSI_MASK6                   0x82C
    #define PCIE_X4_MSI_MASK7                   0x830
    #define PCIE_X4_MSI_PENDING_ST0             0x834
    #define PCIE_X4_MSI_PENDING_ST1             0x838
    #define PCIE_X4_MSI_PENDING_ST2             0x83C
    #define PCIE_X4_MSI_PENDING_ST3             0x840
    #define PCIE_X4_MSI_PENDING_ST4             0x844
    #define PCIE_X4_MSI_PENDING_ST5             0x848
    #define PCIE_X4_MSI_PENDING_ST6             0x84C
    #define PCIE_X4_MSI_PENDING_ST7             0x850
    #define PCIE_X4_ERROR_INDICATION            0x854
    #define PCIE_X4_F0_VSEC_CONTROL             0x858
    #define PCIE_X4_F1_VSEC_CONTROL             0x85c
    #define PCIE_X4_VSEC2_REG                   0x860
    #define PCIE_X4_VSEC3_REG                   0x864
    #define PCIE_X4_VSEC4_REG                   0x868
    #define PCIE_X4_VSEC5_REG                   0x86C
    #define PCIE_X4_VSEC6_REG                   0x870
    #define PCIE_X4_VSEC7_REG                   0x874
    #define PCIE_X4_VSEC_INTR                   0x878
    #define PCIE_X4_PTM_OFFSET_OV               0x87C
    #define PCIE_X4_PTM_OFFSET0                 0x880
    #define PCIE_X4_PTM_OFFSET1                 0x884
    #define PCIE_X4_POWER_STATE                 0x888
    #define PCIE_X4_AXI_SIDEBAND_SIGNALS        0x88c
    #define PCIE_X4_MSG_RECV_CTRL               0x890
    #define PCIE_X4_MSG_RECV_FREE_CNT           0x894
    #define PCIE_X4_MSG_RECV_WORD_CNT           0x898
    #define PCIE_X4_MSG_RECV_THRESHOLD          0x89c
    #define PCIE_X4_MSG_RECV_INTR_MASK          0x8a0
    #define PCIE_X4_MSG_RECV_INTR_STATUS        0x8a4
    #define PCIE_X4_MSG_RECV_FIFO_DATA          0x8a8
    #define PCIE_X4_MSG_RECV_FIFO_DATA_OFF      0x8ac
    #define PCIE_X4_MSG_RECV_STORE              0x8b0
    #define PCIE_X4_INTERRUPT_SIDEBAND_MASK0    0x8b4
    #define PCIE_X4_INTERRUPT_SIDEBAND_MASK1    0x8b8
    #define PCIE_X4_INTERRUPT_SIDEBAND_STATUS0  0x8bc
    #define PCIE_X4_INTERRUPT_SIDEBAND_STATUS1  0x8c0
    #define PCIE_X4_INTx_OUT                    0x8c4
    #define PCIE_X4_INTx_OUT_EN                 0x8c8
    #define PCIE_X4_INTx_OUT_STATUS             0x8cc
    #define PCIE_X4_INTX_IN                     0x8d0
    #define PCIE_X4_INT_PENDING_STATUS          0x8d4
    #define PCIE_X4_LTSSM_STATE_0_3             0x8d8
    #define PCIE_X4_LTSSM_STATE_4_7             0x8dc
    #define PCIE_X4_LTSSM_STATE_8_11            0x8e0
    #define PCIE_X4_LTSSM_STATE_12_15           0x8e4
    #define PCIE_X4_RESET_OUT                   0x8e8
    #define PCIE_X4_INTR_STATUS                 0x8ec
    #define PCIE_X4_INTR_ENABLE                 0x8f0
    #define PCIE_X4_DPA_INTR                    0x8f4
    #define PCIE_X4_FLR                         0x8f8
    #define PCIE_X4_MSIR                        0x8fc
    #define PCIE_X4_MSISR                       0x900
    #define PCIE_X4_MSIIR0                      0x904
    #define PCIE_X4_MSIIR1                      0x908
    #define PCIE_X4_MSIIR2                      0x90c
    #define PCIE_X4_MSIIR3                      0x910
    #define PCIE_X4_MSIIR4                      0x914
    #define PCIE_X4_MSIIR5                      0x918
    #define PCIE_X4_MSIIR6                      0x91c
    #define PCIE_X4_MSIIR7                      0x920
    #define PCIE_X4_MSIVR0                      0x924
    #define PCIE_X4_MSIVR1                      0x928
    #define PCIE_X4_MSIVR2                      0x92c
    #define PCIE_X4_MSIVR3                      0x930
    #define PCIE_X4_MSIVR4                      0x934
    #define PCIE_X4_MSIVR5                      0x938
    #define PCIE_X4_MSIVR6                      0x93c
    #define PCIE_X4_MSIVR7                      0x940
    #define PCIE_X4_AXI_ARADDR                  0x944
    #define PCIE_X4_AXI_AWADDR                  0x948
    #define PCIE_X4_INTR_FORCE                  0x94c
    #define PCIE_X4_FUNC_POWER_STATE            0x950
    #define PCIE_X4_FUNC_STATE                  0x954
    #define PCIE_X4_TPH_ST_MODE                 0x958


    //SRIO PHY MISC reg
    #define PCIE_SRIO_PHY_RESET_CTRL            0x0000
    #define PCIE_SRIO_PHY_LINK_MODE_CTRL        0x0004
    #define PCIE_SRIO_PMA_MODE_CTRL0            0x0008
    #define PCIE_SRIO_PMA_MODE_CTRL1            0x000C
    #define PCIE_SRIO_PMA_RES_CTRL              0x0010
    #define PCIE_SRIO_PHY_PORT0_MODE_CTRL0      0x0014
    #define PCIE_SRIO_PHY_PORT0_MODE_CTRL1      0x0018
    #define PCIE_SRIO_PHY_PORT0_INI_CTRL        0x001C
    #define PCIE_SRIO_PHY_PORT1_MODE_CTRL0      0x0020
    #define PCIE_SRIO_PHY_PORT1_MODE_CTRL1      0x0024
    #define PCIE_SRIO_PHY_PORT1_INI_CTRL        0x0028
    #define PCIE_SRIO_PHY_PORT2_MODE_CTRL0      0x002C
    #define PCIE_SRIO_PHY_PORT2_MODE_CTRL1      0x0030
    #define PCIE_SRIO_PHY_PORT2_INI_CTRL        0x0034
    #define PCIE_SRIO_PHY_PORT3_MODE_CTRL0      0x0038
    #define PCIE_SRIO_PHY_PORT3_MODE_CTRL1      0x003C
    #define PCIE_SRIO_PHY_PORT3_INI_CTRL        0x0040
    #define PCIE_SRIO_LANE0_10GKR_TRAINING      0x0044
    #define PCIE_SRIO_LANE0_MODE_CTRL           0x0048
    #define PCIE_SRIO_LANE0_TX_AET_CTRL0        0x004C
    #define PCIE_SRIO_LANE0_TX_AET_CTRL1        0x0050
    #define PCIE_SRIO_LANE0_TX_AET_CTRL2        0x0054
    #define PCIE_SRIO_LANE0_RX_AET_CTRL         0x0058
    #define PCIE_SRIO_LANE1_10GKR_TRAINING      0x005C
    #define PCIE_SRIO_LANE1_MODE_CTRL           0x0060
    #define PCIE_SRIO_LANE1_TX_AET_CTRL0        0x0064
    #define PCIE_SRIO_LANE1_TX_AET_CTRL1        0x0068
    #define PCIE_SRIO_LANE1_TX_AET_CTRL2        0x006C
    #define PCIE_SRIO_LANE1_RX_AET_CTRL         0x0070
    #define PCIE_SRIO_LANE2_10GKR_TRAINING      0x0074
    #define PCIE_SRIO_LANE2_MODE_CTRL           0x0078
    #define PCIE_SRIO_LANE2_TX_AET_CTRL0        0x007C
    #define PCIE_SRIO_LANE2_TX_AET_CTRL1        0x0080
    #define PCIE_SRIO_LANE2_TX_AET_CTRL2        0x0084
    #define PCIE_SRIO_LANE2_RX_AET_CTRL         0x0088
    #define PCIE_SRIO_LANE3_10GKR_TRAINING      0x008C
    #define PCIE_SRIO_LANE3_MODE_CTRL           0x0090
    #define PCIE_SRIO_LANE3_TX_AET_CTRL0        0x0094
    #define PCIE_SRIO_LANE3_TX_AET_CTRL1        0x0098
    #define PCIE_SRIO_LANE3_TX_AET_CTRL2        0x009C
    #define PCIE_SRIO_LANE3_RX_AET_CTRL         0x00A0
    #define PCIE_SRIO_DTB_BUS                   0x00A4      

    //HSIO PHY MISC reg
    #define PCIE_HSIO_phy_reg0                  0x00000000
    #define PCIE_HSIO_phy_reg1                  0x00000004
    #define PCIE_HSIO_phy_reg2                  0x00000008
    #define PCIE_HSIO_phy_reg3                  0x0000000C
    #define PCIE_HSIO_phy_reg4                  0x00000010
    #define PCIE_HSIO_phy_reg5                  0x00000014
    #define PCIE_HSIO_phy_reg6                  0x00000018
    #define PCIE_HSIO_phy_reg7                  0x0000001C
    #define PCIE_HSIO_phy_reg8                  0x00000020
    #define PCIE_HSIO_phy_reg9                  0x00000024
    #define PCIE_HSIO_phy_reg10                 0x00000028
    #define PCIE_HSIO_phy_reg11                 0x0000002C
    #define PCIE_HSIO_phy_reg12                 0x00000030
    #define PCIE_HSIO_phy_reg13                 0x00000034
    #define PCIE_HSIO_phy_reg14                 0x00000038
    #define PCIE_HSIO_phy_reg15                 0x0000003C
    #define PCIE_HSIO_phy_reg16                 0x00000040
    #define PCIE_HSIO_phy_reg17                 0x00000044
    #define PCIE_HSIO_phy_reg18                 0x00000048
    #define PCIE_HSIO_phy_reg19                 0x0000004c
    #define PCIE_HSIO_phy_reg20                 0x00000050
    #define PCIE_HSIO_phy_reg21                 0x00000054
    #define PCIE_HSIO_phy_reg22                 0x00000058
    #define PCIE_HSIO_phy_reg23                 0x0000005c
    #define PCIE_HSIO_phy_reg24                 0x00000060
    #define PCIE_HSIO_phy_reg25                 0x00000064
//}-----------------------------------------------



//    PIC_IRQ_RAB0_PHY,
//    PIC_IRQ_RAB0_PCIEX1,
//    PIC_IRQ_RAB0_PCIEX4,
//    PIC_IRQ_RAB1_PHY,
//    PIC_IRQ_RAB1_PCIEX1,
//    PIC_IRQ_RAB1_PCIEX4,
//    PIC_IRQ_LINK0,
//    PIC_IRQ_LINK1,
//    PIC_IRQ_LINK2,
//    PIC_IRQ_LINK3,
//    PIC_IRQ_PCIE_PHY,
//    PIC_IRQ_MSIO_LINK0,
//    PIC_IRQ_MSIO_LINK1,
//    PIC_IRQ_MSIO_PHY,


#if 1 /*START*/
#include <reworks/types.h>
#include <reworksio.h>

#ifndef	UINT8
#define UINT8	u8
#endif
#ifndef	UINT16
#define UINT16	u16
#endif
#ifndef	UINT32
#define UINT32	u32
#endif
#ifndef	UINT64
#define UINT64	u64
#endif
#ifndef	U8
#define U8		u8
#endif
#ifndef	U16
#define U16		u16
#endif
#ifndef	U32
#define U32		u32
#endif
#ifndef	U64
#define U64		u64
#endif
#ifndef	VOID
#define VOID	void
#endif
#ifndef	PVOID
#define	PVOID	void*
#endif
#ifndef	INT
#define INT		int
#endif
#ifndef	BOOL
#define	BOOL	int
#endif
#ifndef	UINT
#define	UINT	unsigned int
#endif
#ifndef	ULONG
#define	ULONG	unsigned long
#endif
#ifndef	LONG
#define	LONG	long
#endif
#ifndef	UCHAR
#define	UCHAR	unsigned char
#endif
#ifndef PUCHAR
#define PUCHAR	unsigned char*
#endif
#ifndef	CHAR
#define	CHAR	char
#endif
#ifndef	CPCHAR
#define CPCHAR	char*
#endif
#ifndef	PCHAR
#define PCHAR	char*
#endif
#ifndef	PX_ERROR
#define	PX_ERROR	-1
#endif
#ifndef	ERROR_NONE
#define	ERROR_NONE	0
#endif
#ifndef	ERROR
#define	ERROR	-1
#endif
#ifndef	OK
#define	OK	0
#endif
#ifndef	irqreturn_t
#define	irqreturn_t	int
#endif
#ifndef TRUE
#define TRUE		(1)
#endif
#ifndef FALSE
#define FALSE		(0)
#endif
#ifndef addr_t
#define addr_t	unsigned long
#endif
#ifndef LW_NULL
#define LW_NULL NULL
#endif
#endif /*END*/



#endif

