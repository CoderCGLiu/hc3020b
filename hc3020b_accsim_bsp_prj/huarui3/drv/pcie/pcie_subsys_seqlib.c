#include "pcie.h"

//get link_num
u64 get_pcie_ip_link_base_addr(int sel)
{  
   u64 base_addr;
   if(sel == PCIE_X16_LINK0_EN)
   {
      base_addr = PCIE_X16_LINK0_IP_BASE_ADDR;
      return(base_addr);
   }

   if(sel == PCIE_X16_LINK1_EN)
   {
      base_addr = PCIE_X16_LINK1_IP_BASE_ADDR;
      return(base_addr);
   }

   if(sel == PCIE_X16_LINK2_EN)
   {
      base_addr = PCIE_X16_LINK2_IP_BASE_ADDR;
      return(base_addr);
   }

   if(sel == PCIE_X16_LINK3_EN)
   {
      base_addr = PCIE_X16_LINK3_IP_BASE_ADDR;
      return(base_addr);
   }

   if(sel == PCIE_SRIO_0_LINK0_EN)
   {
      base_addr = PCIE_SRIO_REGION0_LINK0_IP_BASE_ADDR;
      return(base_addr);
   }

   if(sel == PCIE_SRIO_0_LINK1_EN)
   {
      base_addr = PCIE_SRIO_REGION0_LINK1_IP_BASE_ADDR;
      return(base_addr);
   }

   if(sel == PCIE_SRIO_1_LINK0_EN)
   {
      base_addr = PCIE_SRIO_REGION1_LINK0_IP_BASE_ADDR;
      return(base_addr);
   }

   if(sel == PCIE_SRIO_1_LINK1_EN)
   {
      base_addr = PCIE_SRIO_REGION1_LINK1_IP_BASE_ADDR;
      return(base_addr);
   }

   if(sel == PCIE_HSIO_LINK0_EN)
   {
      base_addr = PCIE_HSIO_LINK0_IP_BASE_ADDR;
      return(base_addr);
   }

   if(sel == PCIE_HSIO_LINK1_EN)
   {
      base_addr = PCIE_HSIO_LINK1_IP_BASE_ADDR;
      return(base_addr);
   }
}

//get misc reg 
u64 get_pcie_misc_link_base_addr(int sel)
{
   u64 base_addr;
   if(sel == PCIE_X16_LINK0_EN)
   {
      base_addr = PCIE_X16_LINK0_MISC_BASE_ADDR;
      return(base_addr);
   }

   if(sel == PCIE_X16_LINK1_EN)
   {
      base_addr = PCIE_X16_LINK1_MISC_BASE_ADDR;
      return(base_addr);
   }

   if(sel == PCIE_X16_LINK2_EN)
   {
      base_addr = PCIE_X16_LINK2_MISC_BASE_ADDR;
      return(base_addr);
   }

   if(sel == PCIE_X16_LINK3_EN)
   {
      base_addr = PCIE_X16_LINK3_MISC_BASE_ADDR;
      return(base_addr);
   }

   if(sel == PCIE_SRIO_0_LINK0_EN)
   {
      base_addr = PCIE_SRIO_REGION0_LINK0_MISC_BASE_ADDR;
      return(base_addr);
   }

   if(sel == PCIE_SRIO_0_LINK1_EN)
   {
      base_addr = PCIE_SRIO_REGION0_LINK1_MISC_BASE_ADDR;
      return(base_addr);
   }

   if(sel == PCIE_SRIO_1_LINK0_EN)
   {
      base_addr = PCIE_SRIO_REGION1_LINK0_MISC_BASE_ADDR;
      return(base_addr);
   }

   if(sel == PCIE_SRIO_1_LINK1_EN)
   {
      base_addr = PCIE_SRIO_REGION1_LINK1_MISC_BASE_ADDR;
      return(base_addr);
   }

   if(sel == PCIE_HSIO_LINK0_EN)
   {
      base_addr = PCIE_HSIO_LINK0_MISC_BASE_ADDR;
      return(base_addr);
   }

   if(sel == PCIE_HSIO_LINK1_EN)
   {
      base_addr = PCIE_HSIO_LINK1_MISC_BASE_ADDR;
      return(base_addr);
   }
}


u64 get_pcie_axi_baseaddr(int sel,int Adtype) 
{
   u64 base_addr;
   /*  AD_4M pcie Axi addr  */
   if(Adtype == AD_4M)
   {
   switch(sel)
   {
       case PCIE_X16_LINK0_EN:{base_addr =    0x0012000000;};break;  
       case PCIE_X16_LINK1_EN:{base_addr =    0x0012400000;};break;
       case PCIE_X16_LINK2_EN:{base_addr =    0x0012800000;};break;
       case PCIE_X16_LINK3_EN:{base_addr =    0x0012c00000;};break;
       case PCIE_SRIO_0_LINK0_EN:{base_addr = 0x0013800000;};break;
       case PCIE_SRIO_0_LINK1_EN:{base_addr = 0x0013c00000;};break;
       case PCIE_SRIO_1_LINK0_EN:{base_addr = 0x0014400000;};break;
       case PCIE_SRIO_1_LINK1_EN:{base_addr = 0x0014800000;};break;
       case PCIE_HSIO_LINK0_EN:{base_addr =   0x0013000000;};break;
       case PCIE_HSIO_LINK1_EN:{base_addr =   0x0013400000;};break;
   }
   }
   /*  AD_128M pcie Axi addr  */
   if(Adtype == AD_128M)
   {
   switch(sel)
   {
       case PCIE_X16_LINK0_EN:{base_addr =    0x0030000000;};break; 
       case PCIE_X16_LINK1_EN:{base_addr =    0x0038000000;};break;
       case PCIE_X16_LINK2_EN:{base_addr =    0x0040000000;};break;
       case PCIE_X16_LINK3_EN:{base_addr =    0x0048000000;};break;
       case PCIE_SRIO_0_LINK0_EN:{base_addr = 0x0060000000;};break;
       case PCIE_SRIO_0_LINK1_EN:{base_addr = 0x0068000000;};break;
       case PCIE_SRIO_1_LINK0_EN:{base_addr = 0x0070000000;};break;
       case PCIE_SRIO_1_LINK1_EN:{base_addr = 0x0078000000;};break;
       case PCIE_HSIO_LINK0_EN:{base_addr =   0x0020000000;};break;
       case PCIE_HSIO_LINK1_EN:{base_addr =   0x0028000000;};break;
   }
   }
   /*  AD_4G pcie Axi addr  */
   if(Adtype == AD_4G)
   {
   switch(sel)
   {
       case PCIE_X16_LINK0_EN:{base_addr =    0x0100000000ull;};break; 
       case PCIE_X16_LINK1_EN:{base_addr =    0x0200000000ull;};break;
       case PCIE_X16_LINK2_EN:{base_addr =    0x0300000000ull;};break;
       case PCIE_X16_LINK3_EN:{base_addr =    0x0400000000ull;};break;
       case PCIE_SRIO_0_LINK0_EN:{base_addr = 0x0600000000ull;};break;
       case PCIE_SRIO_0_LINK1_EN:{base_addr = 0x0680000000ull;};break;
       case PCIE_SRIO_1_LINK0_EN:{base_addr = 0x0700000000ull;};break;
       case PCIE_SRIO_1_LINK1_EN:{base_addr = 0x0780000000ull;};break;
       case PCIE_HSIO_LINK0_EN:{base_addr =   0x0800000000ull;};break;
       case PCIE_HSIO_LINK1_EN:{base_addr =   0x0880000000ull;};break;
   }
   }

   return (base_addr);
}

/******************************************************************************
 * 获取LTSSM状态机的状态
 * PCIe使用LTSSM(Link Training and Status State Machine)状态机来管理Link的建立、维护和断开等过程。
 * LTSSM_STATE_0_3 - 0x8d8, 
 * LTSSM_STATE_4_7 - 0x8dc, 
 * LTSSM_STATE_8_11 - 0x8e0, 
 * LTSSM_STATE_12_15 - 0x8e4, 
 * 
 * x16: link0: 0x1f45+  : PCIE_X16_LINK0_MISC_BASE_ADDR
 */
void get_ltssm_switch(u64 link_addr)
{
	uart_printf("ltssm:%x-%x-%x-%x\n\r\n\r",\
			phx_read_u32(link_addr+0x8d8),phx_read_u32(link_addr+0x8dc),\
			phx_read_u32(link_addr+0x8e0),phx_read_u32(link_addr+0x8e4));

}


void pcie_x16_pcie_x16_config_sequence(int pcie_x16_pcie_x16_ep_rc)
{
	uart_printf("pcie_x16_pcie_x16_config_sequence.\n\r");

    delay_cnt(25);//x*40ns;------25-----1us;
    
	/*配置PHY模式(X16)*/
	phx_write_u32(PCIE_X16_PHY_MISC_BASE_ADDR | PCIE_X16_PHY_LINE_CFG0, 0x0);
	phx_write_u32(PCIE_X16_PHY_MISC_BASE_ADDR | PCIE_X16_PHY_LINE_CFG1, 0x0);
	/* PHY release reset(X16)*/
	phx_write_u32(PCIE_X16_PHY_MISC_BASE_ADDR | PCIE_X16_PHY_RESET, 0x1ffff);
    delay_cnt(500);

    //LINK0 CFG
    if(pcie_x16_pcie_x16_ep_rc == 0) 
    {
        phx_write_u32(PCIE_X16_LINK0_MISC_BASE_ADDR,0x2430);//EP
    } else {
        phx_write_u32(PCIE_X16_LINK0_MISC_BASE_ADDR,0x2434);//RC
    }

    phx_write_u32(0x1f450008,0x00010002);
    phx_write_u32(0x1f45000c,0x00040008);
    phx_write_u32(0x1f450960,0x00100020);
    phx_write_u32(0x1f450964,0x00400080);
    phx_write_u32(0x1f450968,0x01000200);
    phx_write_u32(0x1f45096c,0x04000800);
    phx_write_u32(0x1f450970,0x10002000);
    phx_write_u32(0x1f450974,0x40008000);


#if 0//fudy
    //LINK1 CFG
    phx_write_u32(0x1f460008,0x00);
    phx_write_u32(0x1f46000c,0x00);
    phx_write_u32(0x1f460960,0x00);
    phx_write_u32(0x1f460964,0x00);
    phx_write_u32(0x1f460968,0x00);
    phx_write_u32(0x1f46096c,0x00);
    phx_write_u32(0x1f460970,0x00);
    phx_write_u32(0x1f460974,0x00);

    //LINK2 CFG
    phx_write_u32(0x1f470008,0x00);
    phx_write_u32(0x1f47000c,0x00);
    phx_write_u32(0x1f470960,0x00);
    phx_write_u32(0x1f470964,0x00);
    phx_write_u32(0x1f470968,0x00);
    phx_write_u32(0x1f47096c,0x00);
    phx_write_u32(0x1f470970,0x00);
    phx_write_u32(0x1f470974,0x00);

    //LINK3 CFG
    phx_write_u32(0x1f480008,0x00);
    phx_write_u32(0x1f48000c,0x00);
    phx_write_u32(0x1f480960,0x00);
    phx_write_u32(0x1f480964,0x00);
    phx_write_u32(0x1f480968,0x00);
    phx_write_u32(0x1f48096c,0x00);
    phx_write_u32(0x1f480970,0x00);
    phx_write_u32(0x1f480974,0x00);
#endif

		
#if 0//tmp:force 2.5G
		u32 cfg0=0x1f450000+0;
		u32 val = phx_read_u32(cfg0);
		val &= 0xffffffe7;//[4:3]=0, 2.5G
		phx_write_u32(cfg0,val);
#endif


#if 0
	//close link1,2,3 config
	phx_write_u32(PCIE_X16_LINK1_MISC_BASE_ADDR | PCIE_X16_CONFIG_INPUT_OUTPUT_0, 0x0);
	phx_write_u32(PCIE_X16_LINK2_MISC_BASE_ADDR | PCIE_X16_CONFIG_INPUT_OUTPUT_0, 0x0);
	phx_write_u32(PCIE_X16_LINK3_MISC_BASE_ADDR | PCIE_X16_CONFIG_INPUT_OUTPUT_0, 0x0);
#endif

	/*Step5:启动 training*/
	phx_write_u32(PCIE_X16_LINK0_MISC_BASE_ADDR | PCIE_X16_CONFIG_INPUT_OUTPUT_0, 0x3);
	
#if 0
	get_ltssm_switch(PCIE_X16_LINK0_MISC_BASE_ADDR);//fudy,tmp
    delay_cnt(25);
	get_ltssm_switch(PCIE_X16_LINK0_MISC_BASE_ADDR);//fudy,tmp
#endif
	
    return;
}


void pcie_x16_pcie_x8_2_config_sequence(int pcie_x16_pcie_x8_2_ep_rc)
{
        uart_printf("pcie_x16_pcie_x8_2_config_sequence.\n\r");
        //PHY_LINK_CFG
#if 1//fudy        
        phx_write_u32(0x1f440044,0x0);
        phx_write_u32(0x1f440048,0x11111111);
#else//for link0-lane8-15
		phx_write_u32(0x1f440044,0x11111111);
		phx_write_u32(0x1f440048,0x0);
#endif

        //release PHY reset
        phx_write_u32(0x1f440054,0x1ffff);              
        
        //LINK0 CFG
        if(pcie_x16_pcie_x8_2_ep_rc == 0) {
            phx_write_u32(0x1f450000,0x1c30);//EP
        } else {
            phx_write_u32(0x1f450000,0x1c34);//RC
        }
        phx_write_u32(0x1f450008,0x00010002);
        phx_write_u32(0x1f45000c,0x00040008);
        phx_write_u32(0x1f450960,0x00100020);
        phx_write_u32(0x1f450964,0x00400080);
        phx_write_u32(0x1f450968,0x0);
        phx_write_u32(0x1f45096c,0x0);
        phx_write_u32(0x1f450970,0x0);
        phx_write_u32(0x1f450974,0x0);
        
        //LINK1 CFG
        if(pcie_x16_pcie_x8_2_ep_rc == 0) {
            phx_write_u32(0x1f460000,0x1c30);//EP
        } else {
            phx_write_u32(0x1f460000,0x1c34);//RC
        }
        phx_write_u32(0x1f460008,0x01000200);
        phx_write_u32(0x1f46000c,0x04000800);
        phx_write_u32(0x1f460960,0x10002000);
        phx_write_u32(0x1f460964,0x40008000);
        phx_write_u32(0x1f460968,0x0);
        phx_write_u32(0x1f46096c,0x0);
        phx_write_u32(0x1f460970,0x0);
        phx_write_u32(0x1f460974,0x0);        

        //LINK2 CFG
        phx_write_u32(0x1f470008,0x0);
        phx_write_u32(0x1f47000c,0x0);
        phx_write_u32(0x1f470960,0x0);
        phx_write_u32(0x1f470964,0x0);
        phx_write_u32(0x1f470968,0x0);
        phx_write_u32(0x1f47096c,0x0);
        phx_write_u32(0x1f470970,0x0);
        phx_write_u32(0x1f470974,0x0);

        ////LINK3 CFG
        phx_write_u32(0x1f480008,0x0);
        phx_write_u32(0x1f48000c,0x0);
        phx_write_u32(0x1f480960,0x0);
        phx_write_u32(0x1f480964,0x0);
        phx_write_u32(0x1f480968,0x0);
        phx_write_u32(0x1f48096c,0x0);
        phx_write_u32(0x1f480970,0x0);
        phx_write_u32(0x1f480974,0x0);

        //START
        phx_write_u32(0x1f450800,0x3);//link0
        phx_write_u32(0x1f460800,0x3);//link1
        
        delay_cnt(25);
}


void pcie_x16_pcie_x8_x4_x4_config_sequence(int pcie_x16_pcie_x8_x4_x4_ep_rc)
{
        //PHY_LINK_CFG
        phx_write_u32(0x1f440044,0x00000000);
        phx_write_u32(0x1f440048,0x22221111);

        //release PHY reset
        phx_write_u32(0x1f440054,0x1ffff);      

        //LINK0 CFG
        if(pcie_x16_pcie_x8_x4_x4_ep_rc == 0) {
            phx_write_u32(0x1f450000,0x1c30);//EP
        } else {
            phx_write_u32(0x1f450000,0x1c34);//RC
        }
        phx_write_u32(0x1f450008,0x00010002);
        phx_write_u32(0x1f45000c,0x00040008);
        phx_write_u32(0x1f450960,0x00100020);
        phx_write_u32(0x1f450964,0x00400080);
        phx_write_u32(0x1f450968,0x0);
        phx_write_u32(0x1f45096c,0x0);
        phx_write_u32(0x1f450970,0x0);
        phx_write_u32(0x1f450974,0x0);

        //LINK1 CFG
        phx_write_u32(0x1f460000,0x1430);//EP
        phx_write_u32(0x1f460008,0x01000200);
        phx_write_u32(0x1f46000c,0x04000800);        
        phx_write_u32(0x1f460960,0x0);
        phx_write_u32(0x1f460964,0x0);
        phx_write_u32(0x1f460968,0x0);
        phx_write_u32(0x1f46096c,0x0);
        phx_write_u32(0x1f460970,0x0);
        phx_write_u32(0x1f460974,0x0);

        //LINK2 CFG
        phx_write_u32(0x1f470000,0x1430);//EP
        phx_write_u32(0x1f470008,0x10002000);
        phx_write_u32(0x1f47000c,0x40008000); 
        phx_write_u32(0x1f470960,0x0);
        phx_write_u32(0x1f470964,0x0);
        phx_write_u32(0x1f470968,0x0);
        phx_write_u32(0x1f47096c,0x0);
        phx_write_u32(0x1f470970,0x0);
        phx_write_u32(0x1f470974,0x0);       

        //LINK3 CFG
        //phx_write_u32(0x1f480008,0x0);
        //phx_write_u32(0x1f48000c,0x0); 
        //phx_write_u32(0x1f480960,0x0);
        //phx_write_u32(0x1f480964,0x0);
        //phx_write_u32(0x1f480968,0x0);
        //phx_write_u32(0x1f48096c,0x0);
        //phx_write_u32(0x1f480970,0x0);
        //phx_write_u32(0x1f480974,0x0);

        //START
        phx_write_u32(0x1f450800,0x3);
        phx_write_u32(0x1f460800,0x3);
        phx_write_u32(0x1f470800,0x3);

        delay_cnt(25);
}


//fudy.
void pcie_x16_pcie_x4_4_config_sequence(int pcie_x16_pcie_x4_4_ep_rc)
{
        //PHY_LINK_CFG
        phx_write_u32(0x1f440044,0x11110000);
        phx_write_u32(0x1f440048,0x33332222);

        //release PHY reset
        phx_write_u32(0x1f440054,0x1ffff);     
        //LINK0 CFG
        if(pcie_x16_pcie_x4_4_ep_rc == 0) {
            phx_write_u32(0x1f450000,0x1430);//EP
        } else {
            phx_write_u32(0x1f450000,0x1434);//RC
        }
        phx_write_u32(0x1f450008,0x00010002);
        phx_write_u32(0x1f45000c,0x00040008);
        phx_write_u32(0x1f450960,0x0);
        phx_write_u32(0x1f450964,0x0);
        phx_write_u32(0x1f450968,0x0);
        phx_write_u32(0x1f45096c,0x0);
        phx_write_u32(0x1f450970,0x0);
        phx_write_u32(0x1f450974,0x0);

        //LINK1 CFG
        if(pcie_x16_pcie_x4_4_ep_rc == 0) {
            phx_write_u32(0x1f460000,0x1430);//EP
        } else {
            phx_write_u32(0x1f460000,0x1434);//RC
        }

        phx_write_u32(0x1f460008,0x00100020);
        phx_write_u32(0x1f46000c,0x00400080);        
        phx_write_u32(0x1f460960,0x0);
        phx_write_u32(0x1f460964,0x0);
        phx_write_u32(0x1f460968,0x0);
        phx_write_u32(0x1f46096c,0x0);
        phx_write_u32(0x1f460970,0x0);
        phx_write_u32(0x1f460974,0x0);

        //LINK2 CFG
        if(pcie_x16_pcie_x4_4_ep_rc == 0) {
            phx_write_u32(0x1f470000,0x1430);//EP
        } else {
            phx_write_u32(0x1f470000,0x1434);//RC
        }

        phx_write_u32(0x1f470008,0x01000200);
        phx_write_u32(0x1f47000c,0x04000800); 
        phx_write_u32(0x1f470960,0x0);
        phx_write_u32(0x1f470964,0x0);
        phx_write_u32(0x1f470968,0x0);
        phx_write_u32(0x1f47096c,0x0);
        phx_write_u32(0x1f470970,0x0);
        phx_write_u32(0x1f470974,0x0);       

        //LINK3 CFG
        if(pcie_x16_pcie_x4_4_ep_rc == 0) {
            phx_write_u32(0x1f480000,0x1430);//EP
        } else {
            phx_write_u32(0x1f480000,0x1434);//RC
        }

        phx_write_u32(0x1f480008,0x10002000);
        phx_write_u32(0x1f48000c,0x40008000); 
        phx_write_u32(0x1f480960,0x0);
        phx_write_u32(0x1f480964,0x0);
        phx_write_u32(0x1f480968,0x0);
        phx_write_u32(0x1f48096c,0x0);
        phx_write_u32(0x1f480970,0x0);
        phx_write_u32(0x1f480974,0x0);

        //START
        phx_write_u32(0x1f450800,0x3);
        phx_write_u32(0x1f460800,0x3);
        phx_write_u32(0x1f470800,0x3);
        phx_write_u32(0x1f480800,0x3);

        delay_cnt(25);
}



//X16 speedup function
void pcie_x16_phx_write_u32_speedup(u64 waddr,int wdata)
{   
    phx_write_u32((PCIE_X16_PHY_IP_BASE_ADDR + (waddr<<2)),wdata);
}

void pcie_x16_phx_write_u32_speedup_lane(u64 waddr,int wdata,int lanum)
{   
    phx_write_u32((PCIE_X16_PHY_IP_BASE_ADDR + 0x10000 + (lanum*0x800) + (waddr<<2)),wdata);
}

//SRIO region0 speedup function
void pcie_srio_0_phx_write_u32_speedup(u64 waddr,int wdata)
{   
    phx_write_u32((PCIE_SRIO_REGION0_PHY_IP_BASE_ADDR+(waddr<<2)),wdata);
}

//SRIO region1 speedup function
void pcie_srio_1_phx_write_u32_speedup(u64 waddr,int wdata)
{   
    phx_write_u32((PCIE_SRIO_REGION1_PHY_IP_BASE_ADDR+(waddr<<2)),wdata);
}

//HSIO speedup function
void pcie_hsio_phx_write_u32_speedup(u64 waddr,int wdata)
{   
    phx_write_u32((PCIE_HSIO_PHY_IP_BASE_ADDR+(waddr<<2)),wdata);
}


//X16 pcie_pma_speedup_sequence
void pcie_x16_pcie_pma_speedup_sequence(void)
{
    
     int num;
       
        pcie_x16_phx_write_u32_speedup(0x046 , 0x0000);// cmn_plllc_init_preg
        pcie_x16_phx_write_u32_speedup(0x047 , 0x0000);// cmn_plllc_itertmr_preg
        pcie_x16_phx_write_u32_speedup(0x04B , 0x0020);// cmn_plllc_lock_cntstart_preg
        pcie_x16_phx_write_u32_speedup(0x045 , 0x0019);// cmn_plllc_dcocal_ctrl_preg

        pcie_x16_phx_write_u32_speedup(0x04F , 0x0000);// cmn_plllc_bwcal_mode1_preg
        pcie_x16_phx_write_u32_speedup(0x050 , 0x0000);// cmn_plllc_bwcal_mode0_preg
        pcie_x16_phx_write_u32_speedup(0x063 , 0x0000);// cmn_plllc_lock_delay_ctrl_preg
        pcie_x16_phx_write_u32_speedup(0x0C5 , 0x0019);// cmn_plllc1_dcocal_ctrl_preg

        pcie_x16_phx_write_u32_speedup(0x0C6 , 0x0000);// cmn_plllc1_init_preg
        pcie_x16_phx_write_u32_speedup(0x0C7 , 0x0000);// cmn_plllc1_itertmr_preg
        pcie_x16_phx_write_u32_speedup(0x0CB , 0x0020);// cmn_plllc1_lock_cntstart_preg
        pcie_x16_phx_write_u32_speedup(0x0CF , 0x0000);// cmn_plllc1_bwcal_mode1_preg
        pcie_x16_phx_write_u32_speedup(0x0D0 , 0x0000);// cmn_plllc1_bwcal_mode0_preg
        pcie_x16_phx_write_u32_speedup(0x0E3 , 0x0000);// cmn_plllc1_lock_delay_ctrl_preg  
        
        //Table 13: PHY/PCS APB Register Writes for Simulation Speed-up
        pcie_x16_phx_write_u32_speedup(0xC002 ,0x4010);// phy_pipe_com_lock_cfg1
        pcie_x16_phx_write_u32_speedup(0xC003 ,0x0810);// phy_pipe_com_lock_cfg2
        pcie_x16_phx_write_u32_speedup(0xC004 ,0x0101);// phy_pipe_eie_lock_cfg
        pcie_x16_phx_write_u32_speedup(0xC006 ,0x000A);// phy_pipe_rcv_det_inh

	for(num =0x0000;num<0x0010;num++)
    {
        pcie_x16_phx_write_u32_speedup_lane(0x020 , 0x0041, num);// fpwriso_ctrl_preg (should not be used
        pcie_x16_phx_write_u32_speedup_lane(0x041 , 0x0001, num);// pllctrl_phase1en_preg
        pcie_x16_phx_write_u32_speedup_lane(0x042 , 0x0001, num);// pllctrl_phase2en_preg
        pcie_x16_phx_write_u32_speedup_lane(0x071 , 0x0005, num);// tx_rcvdet_wait_start_preg
        pcie_x16_phx_write_u32_speedup_lane(0x096 , 0x8001, num);// creq_spare_preg
        pcie_x16_phx_write_u32_speedup_lane(0x0B3 , 0x000F, num);// smpcal_accum_preg
        pcie_x16_phx_write_u32_speedup_lane(0x0B4 , 0x0003, num);// smpcal_start_code_preg
        pcie_x16_phx_write_u32_speedup_lane(0x0C8 , 0x0000, num);// deq_concur_ctrl1_preg
        pcie_x16_phx_write_u32_speedup_lane(0x0C9 , 0xD004, num);// deq_concur_ctrl2_preg
        pcie_x16_phx_write_u32_speedup_lane(0x0CA , 0x0000, num);// deq_epipwr_ctrl_preg
        pcie_x16_phx_write_u32_speedup_lane(0x0D3 , 0x000A, num);// cmp_avr_timer_preg
        pcie_x16_phx_write_u32_speedup_lane(0x0F1 , 0x0101, num);// datdfe01_en_ceph_ctrl_preg
        pcie_x16_phx_write_u32_speedup_lane(0x0F2 , 0x0101, num);// datdfe23_en_ceph_ctrl_preg
        pcie_x16_phx_write_u32_speedup_lane(0x0F3 , 0x0101, num);// datdfe4_en_ceph_ctrl_preg
        pcie_x16_phx_write_u32_speedup_lane(0x0F4 , 0x0101, num);// datgainoffset_en_ceph_ctrl_preg
        pcie_x16_phx_write_u32_speedup_lane(0x0F5 , 0x0000, num);// eoffcal_en_phctrl_preg
        pcie_x16_phx_write_u32_speedup_lane(0x0F8 , 0x0000, num);// phalign_en_ctrl_preg
        pcie_x16_phx_write_u32_speedup_lane(0x0F9 , 0x0000, num);// postprecur_en_ceph_ctrl_preg
        pcie_x16_phx_write_u32_speedup_lane(0x0FB , 0x0000, num);// tau_en_ceph2to0_preg
        pcie_x16_phx_write_u32_speedup_lane(0x0FC , 0x0003, num);// tau_en_ceph5to3_preg
        pcie_x16_phx_write_u32_speedup_lane(0x110 , 0x0101, num);// phase_iter_ceph_01_preg
        pcie_x16_phx_write_u32_speedup_lane(0x111 , 0x0101, num);// phase_iter_ceph_23_preg
        pcie_x16_phx_write_u32_speedup_lane(0x112 , 0x0100, num);// phase_iter_ceph_45_preg
        pcie_x16_phx_write_u32_speedup_lane(0x124 , 0x0000, num);// oeph_en_ctrl_preg
        pcie_x16_phx_write_u32_speedup_lane(0x15C , 0x5425, num);// deq_openeye_ctrl_preg
        pcie_x16_phx_write_u32_speedup_lane(0x183 , 0x7458, num);// cpical_res_startcode_mode23_preg
        pcie_x16_phx_write_u32_speedup_lane(0x184 , 0x4B3B, num);// cpical_res_startcode_mode01_preg
        }
}

//X16 pcie_pma_nospeedup_sequence ,fudy.
void pcie_x16_pcie_pma_nospeedup_sequence_x8x8(void)
{

    u32 PHY_ADDR       = PCIE_X16_PHY_IP_BASE_ADDR;
    u32 PHY_REG_ADDR   = PCIE_X16_PHY_MISC_BASE_ADDR;

    u32 j;

    uart_printf("pcie_x16_pcie_pma_nospeedup_sequence_x8x8.\n\r");

    //x16(x8x8),nospeedup:889 Registers
	phx_write_u32(((0xC003* 4 ) | PHY_ADDR),0x0144);
	phx_write_u32(((0xC00E* 4 ) | PHY_ADDR),0x0002);
	phx_write_u32(((0xE000* 4 ) | PHY_ADDR),0x3300);
	phx_write_u32(((0x0098* 4 ) | PHY_ADDR),0x2100);
	phx_write_u32(((0x00A0* 4 ) | PHY_ADDR),0x002F);
	phx_write_u32(((0x0043* 4 ) | PHY_ADDR),0x002D);
	phx_write_u32(((0x0048* 4 ) | PHY_ADDR),0x0105);
	phx_write_u32(((0x00C8* 4 ) | PHY_ADDR),0x0105);
	phx_write_u32(((0x004A* 4 ) | PHY_ADDR),0x2187);
	phx_write_u32(((0x00CA* 4 ) | PHY_ADDR),0x2105);
	phx_write_u32(((0x004E* 4 ) | PHY_ADDR),0x8205);
	phx_write_u32(((0x0050* 4 ) | PHY_ADDR),0x8804);
	phx_write_u32(((0x005A* 4 ) | PHY_ADDR),0x0CDB);
	phx_write_u32(((0x00DA* 4 ) | PHY_ADDR),0x0CDB);
	phx_write_u32(((0x0062* 4 ) | PHY_ADDR),0x1219);
	phx_write_u32(((0x00E2* 4 ) | PHY_ADDR),0x1912);
	phx_write_u32(((0x0023* 4 ) | PHY_ADDR),0x413B);
	phx_write_u32(((0x6006* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x6039* 4 ) | PHY_ADDR),0x0606);
	phx_write_u32(((0x603B* 4 ) | PHY_ADDR),0x000C);
	phx_write_u32(((0x604C* 4 ) | PHY_ADDR),0x0D33);
	phx_write_u32(((0x606F* 4 ) | PHY_ADDR),0x9602);
	phx_write_u32(((0x6071* 4 ) | PHY_ADDR),0x0271);
	phx_write_u32(((0x6081* 4 ) | PHY_ADDR),0x813E);
	phx_write_u32(((0x6085* 4 ) | PHY_ADDR),0x8E82);
	phx_write_u32(((0x6086* 4 ) | PHY_ADDR),0x933A);
	phx_write_u32(((0x6087* 4 ) | PHY_ADDR),0x8F5B);
	phx_write_u32(((0x6088* 4 ) | PHY_ADDR),0x8B4B);
	phx_write_u32(((0x6091* 4 ) | PHY_ADDR),0x033C);
	phx_write_u32(((0x6096* 4 ) | PHY_ADDR),0x0003);
	phx_write_u32(((0x6097* 4 ) | PHY_ADDR),0x44CC);
	phx_write_u32(((0x609B* 4 ) | PHY_ADDR),0x088C);
	phx_write_u32(((0x609C* 4 ) | PHY_ADDR),0x8000);
	phx_write_u32(((0x60AC* 4 ) | PHY_ADDR),0x1F1F);
	phx_write_u32(((0x60AD* 4 ) | PHY_ADDR),0x0080);
	phx_write_u32(((0x60AF* 4 ) | PHY_ADDR),0x1C08);
	phx_write_u32(((0x60C4* 4 ) | PHY_ADDR),0x0003);
	phx_write_u32(((0x60D0* 4 ) | PHY_ADDR),0x3C3C);
	phx_write_u32(((0x60E1* 4 ) | PHY_ADDR),0x3100);
	phx_write_u32(((0x60EA* 4 ) | PHY_ADDR),0x0551);
	phx_write_u32(((0x60EB* 4 ) | PHY_ADDR),0x0022);
	phx_write_u32(((0x60EC* 4 ) | PHY_ADDR),0x0010);
	phx_write_u32(((0x60ED* 4 ) | PHY_ADDR),0x0010);
	phx_write_u32(((0x60EE* 4 ) | PHY_ADDR),0x0008);
	phx_write_u32(((0x60F5* 4 ) | PHY_ADDR),0x1561);
	phx_write_u32(((0x60F8* 4 ) | PHY_ADDR),0x1561);
	phx_write_u32(((0x60FA* 4 ) | PHY_ADDR),0x00B0);
	phx_write_u32(((0x60FB* 4 ) | PHY_ADDR),0x00A6);
	phx_write_u32(((0x60FC* 4 ) | PHY_ADDR),0x01B6);
	phx_write_u32(((0x60FF* 4 ) | PHY_ADDR),0x0336);
	phx_write_u32(((0x6100* 4 ) | PHY_ADDR),0x0788);
	phx_write_u32(((0x6101* 4 ) | PHY_ADDR),0x0777);
	phx_write_u32(((0x6102* 4 ) | PHY_ADDR),0x0BCC);
	phx_write_u32(((0x6103* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x6104* 4 ) | PHY_ADDR),0x0DFF);
	phx_write_u32(((0x6105* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x6106* 4 ) | PHY_ADDR),0x0CDD);
	phx_write_u32(((0x6107* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x6108* 4 ) | PHY_ADDR),0x0CDD);
	phx_write_u32(((0x6109* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x610A* 4 ) | PHY_ADDR),0x0CDD);
	phx_write_u32(((0x610B* 4 ) | PHY_ADDR),0x0599);
	phx_write_u32(((0x610C* 4 ) | PHY_ADDR),0x0DEE);
	phx_write_u32(((0x610D* 4 ) | PHY_ADDR),0x0084);
	phx_write_u32(((0x610E* 4 ) | PHY_ADDR),0x0399);
	phx_write_u32(((0x610F* 4 ) | PHY_ADDR),0x0DEE);
	phx_write_u32(((0x6111* 4 ) | PHY_ADDR),0x28FF);
	phx_write_u32(((0x6115* 4 ) | PHY_ADDR),0x0160);
	phx_write_u32(((0x6116* 4 ) | PHY_ADDR),0x0008);
	phx_write_u32(((0x6118* 4 ) | PHY_ADDR),0x1000);
	phx_write_u32(((0x611A* 4 ) | PHY_ADDR),0x0100);
	phx_write_u32(((0x611B* 4 ) | PHY_ADDR),0x0080);
	phx_write_u32(((0x611C* 4 ) | PHY_ADDR),0x0024);
	phx_write_u32(((0x611D* 4 ) | PHY_ADDR),0x0024);
	phx_write_u32(((0x611E* 4 ) | PHY_ADDR),0x01FF);
	phx_write_u32(((0x6121* 4 ) | PHY_ADDR),0x001F);
	phx_write_u32(((0x6122* 4 ) | PHY_ADDR),0x1CE7);
	phx_write_u32(((0x6123* 4 ) | PHY_ADDR),0x1D67);
	phx_write_u32(((0x6125* 4 ) | PHY_ADDR),0x001F);
	phx_write_u32(((0x6126* 4 ) | PHY_ADDR),0x4000);
	phx_write_u32(((0x6148* 4 ) | PHY_ADDR),0x2011);
	phx_write_u32(((0x6151* 4 ) | PHY_ADDR),0x0606);
	phx_write_u32(((0x6152* 4 ) | PHY_ADDR),0x0100);
	phx_write_u32(((0x6155* 4 ) | PHY_ADDR),0x0220);
	phx_write_u32(((0x6158* 4 ) | PHY_ADDR),0xFE21);
	phx_write_u32(((0x6159* 4 ) | PHY_ADDR),0x2121);
	phx_write_u32(((0x615C* 4 ) | PHY_ADDR),0x5882);
	phx_write_u32(((0x6161* 4 ) | PHY_ADDR),0x0023);
	phx_write_u32(((0x61B0* 4 ) | PHY_ADDR),0x559E);
	phx_write_u32(((0x61D8* 4 ) | PHY_ADDR),0x0019);
	phx_write_u32(((0x61D9* 4 ) | PHY_ADDR),0x7FFF);
	phx_write_u32(((0x61DA* 4 ) | PHY_ADDR),0x0028);
	phx_write_u32(((0x61DB* 4 ) | PHY_ADDR),0x0028);
	phx_write_u32(((0x61DC* 4 ) | PHY_ADDR),0x0005);
	phx_write_u32(((0x61DD* 4 ) | PHY_ADDR),0x000F);
	phx_write_u32(((0x61DE* 4 ) | PHY_ADDR),0x7FC3);
	phx_write_u32(((0x61DF* 4 ) | PHY_ADDR),0x7F81);
	phx_write_u32(((0x61E0* 4 ) | PHY_ADDR),0x7FC3);
	phx_write_u32(((0x61E1* 4 ) | PHY_ADDR),0x7FC3);
	phx_write_u32(((0x61E2* 4 ) | PHY_ADDR),0x7F81);
	phx_write_u32(((0x61E3* 4 ) | PHY_ADDR),0x7FF1);
	phx_write_u32(((0x61E4* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x61E5* 4 ) | PHY_ADDR),0x1000);
	phx_write_u32(((0x61E6* 4 ) | PHY_ADDR),0x5336);
	phx_write_u32(((0x61E7* 4 ) | PHY_ADDR),0x5336);
	phx_write_u32(((0x61E8* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x61F3* 4 ) | PHY_ADDR),0x392B);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0401);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0001);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0402);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0002);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0403);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0003);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0404);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0004);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0405);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0005);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0406);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0006);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0407);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0007);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0408);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0008);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0409);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0009);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x040A);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x000A);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x040B);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x000B);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x040C);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x000C);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x040D);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x000D);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x050D);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x000E);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x050E);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x000F);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x060F);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0010);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0610);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0011);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0710);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0012);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0711);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0013);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0812);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0014);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0813);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0015);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0913);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0016);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0914);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0017);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0A15);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0018);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0A16);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0019);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0B16);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x001A);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0B17);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x001B);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0C18);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x001C);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0C19);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x001D);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0D19);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x001E);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0D1A);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x001F);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0E1B);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0020);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0E1C);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0021);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0F1C);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0022);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0F1D);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0023);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x101E);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0024);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x101F);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0025);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x111F);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0026);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1120);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0027);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1221);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0028);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1221);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0029);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1322);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x002A);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1322);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x002B);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1423);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x002C);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1423);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x002D);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1524);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x002E);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1525);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x002F);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1626);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0030);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1627);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0031);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1728);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0032);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1729);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0033);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1829);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0034);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x182A);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0035);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x192C);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0036);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x192D);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0037);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1A2E);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0038);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1A2F);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0039);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1B30);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x003A);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1B31);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x003B);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1B32);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x003C);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1B33);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x003D);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1B34);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x003E);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1B35);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x003F);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1B36);
	phx_write_u32(((0x6206* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x6239* 4 ) | PHY_ADDR),0x0606);
	phx_write_u32(((0x623B* 4 ) | PHY_ADDR),0x000C);
	phx_write_u32(((0x624C* 4 ) | PHY_ADDR),0x0D33);
	phx_write_u32(((0x626F* 4 ) | PHY_ADDR),0x9602);
	phx_write_u32(((0x6271* 4 ) | PHY_ADDR),0x0271);
	phx_write_u32(((0x6281* 4 ) | PHY_ADDR),0x813E);
	phx_write_u32(((0x6285* 4 ) | PHY_ADDR),0x8E82);
	phx_write_u32(((0x6286* 4 ) | PHY_ADDR),0x933A);
	phx_write_u32(((0x6287* 4 ) | PHY_ADDR),0x8F5B);
	phx_write_u32(((0x6288* 4 ) | PHY_ADDR),0x8B4B);
	phx_write_u32(((0x6291* 4 ) | PHY_ADDR),0x033C);
	phx_write_u32(((0x6296* 4 ) | PHY_ADDR),0x0003);
	phx_write_u32(((0x6297* 4 ) | PHY_ADDR),0x44CC);
	phx_write_u32(((0x629B* 4 ) | PHY_ADDR),0x088C);
	phx_write_u32(((0x629C* 4 ) | PHY_ADDR),0x8000);
	phx_write_u32(((0x62AC* 4 ) | PHY_ADDR),0x1F1F);
	phx_write_u32(((0x62AD* 4 ) | PHY_ADDR),0x0080);
	phx_write_u32(((0x62AF* 4 ) | PHY_ADDR),0x1C08);
	phx_write_u32(((0x62C4* 4 ) | PHY_ADDR),0x0003);
	phx_write_u32(((0x62D0* 4 ) | PHY_ADDR),0x3C3C);
	phx_write_u32(((0x62E1* 4 ) | PHY_ADDR),0x3100);
	phx_write_u32(((0x62EA* 4 ) | PHY_ADDR),0x0551);
	phx_write_u32(((0x62EB* 4 ) | PHY_ADDR),0x0022);
	phx_write_u32(((0x62EC* 4 ) | PHY_ADDR),0x0010);
	phx_write_u32(((0x62ED* 4 ) | PHY_ADDR),0x0010);
	phx_write_u32(((0x62EE* 4 ) | PHY_ADDR),0x0008);
	phx_write_u32(((0x62F5* 4 ) | PHY_ADDR),0x1561);
	phx_write_u32(((0x62F8* 4 ) | PHY_ADDR),0x1561);
	phx_write_u32(((0x62FA* 4 ) | PHY_ADDR),0x00B0);
	phx_write_u32(((0x62FB* 4 ) | PHY_ADDR),0x00A6);
	phx_write_u32(((0x62FC* 4 ) | PHY_ADDR),0x01B6);
	phx_write_u32(((0x62FF* 4 ) | PHY_ADDR),0x0336);
	phx_write_u32(((0x6300* 4 ) | PHY_ADDR),0x0788);
	phx_write_u32(((0x6301* 4 ) | PHY_ADDR),0x0777);
	phx_write_u32(((0x6302* 4 ) | PHY_ADDR),0x0BCC);
	phx_write_u32(((0x6303* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x6304* 4 ) | PHY_ADDR),0x0DFF);
	phx_write_u32(((0x6305* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x6306* 4 ) | PHY_ADDR),0x0CDD);
	phx_write_u32(((0x6307* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x6308* 4 ) | PHY_ADDR),0x0CDD);
	phx_write_u32(((0x6309* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x630A* 4 ) | PHY_ADDR),0x0CDD);
	phx_write_u32(((0x630B* 4 ) | PHY_ADDR),0x0599);
	phx_write_u32(((0x630C* 4 ) | PHY_ADDR),0x0DEE);
	phx_write_u32(((0x630D* 4 ) | PHY_ADDR),0x0084);
	phx_write_u32(((0x630E* 4 ) | PHY_ADDR),0x0399);
	phx_write_u32(((0x630F* 4 ) | PHY_ADDR),0x0DEE);
	phx_write_u32(((0x6311* 4 ) | PHY_ADDR),0x28FF);
	phx_write_u32(((0x6315* 4 ) | PHY_ADDR),0x0160);
	phx_write_u32(((0x6316* 4 ) | PHY_ADDR),0x0008);
	phx_write_u32(((0x6318* 4 ) | PHY_ADDR),0x1000);
	phx_write_u32(((0x631A* 4 ) | PHY_ADDR),0x0100);
	phx_write_u32(((0x631B* 4 ) | PHY_ADDR),0x0080);
	phx_write_u32(((0x631C* 4 ) | PHY_ADDR),0x0024);
	phx_write_u32(((0x631D* 4 ) | PHY_ADDR),0x0024);
	phx_write_u32(((0x631E* 4 ) | PHY_ADDR),0x01FF);
	phx_write_u32(((0x6321* 4 ) | PHY_ADDR),0x001F);
	phx_write_u32(((0x6322* 4 ) | PHY_ADDR),0x1CE7);
	phx_write_u32(((0x6323* 4 ) | PHY_ADDR),0x1D67);
	phx_write_u32(((0x6325* 4 ) | PHY_ADDR),0x001F);
	phx_write_u32(((0x6326* 4 ) | PHY_ADDR),0x4000);
	phx_write_u32(((0x6348* 4 ) | PHY_ADDR),0x2011);
	phx_write_u32(((0x6351* 4 ) | PHY_ADDR),0x0606);
	phx_write_u32(((0x6352* 4 ) | PHY_ADDR),0x0100);
	phx_write_u32(((0x6355* 4 ) | PHY_ADDR),0x0220);
	phx_write_u32(((0x6358* 4 ) | PHY_ADDR),0xFE21);
	phx_write_u32(((0x6359* 4 ) | PHY_ADDR),0x2121);
	phx_write_u32(((0x635C* 4 ) | PHY_ADDR),0x5882);
	phx_write_u32(((0x6361* 4 ) | PHY_ADDR),0x0023);
	phx_write_u32(((0x63B0* 4 ) | PHY_ADDR),0x559E);
	phx_write_u32(((0x63D8* 4 ) | PHY_ADDR),0x0019);
	phx_write_u32(((0x63D9* 4 ) | PHY_ADDR),0x7FFF);
	phx_write_u32(((0x63DA* 4 ) | PHY_ADDR),0x0028);
	phx_write_u32(((0x63DB* 4 ) | PHY_ADDR),0x0028);
	phx_write_u32(((0x63DC* 4 ) | PHY_ADDR),0x0005);
	phx_write_u32(((0x63DD* 4 ) | PHY_ADDR),0x000F);
	phx_write_u32(((0x63DE* 4 ) | PHY_ADDR),0x7FC3);
	phx_write_u32(((0x63DF* 4 ) | PHY_ADDR),0x7F81);
	phx_write_u32(((0x63E0* 4 ) | PHY_ADDR),0x7FC3);
	phx_write_u32(((0x63E1* 4 ) | PHY_ADDR),0x7FC3);
	phx_write_u32(((0x63E2* 4 ) | PHY_ADDR),0x7F81);
	phx_write_u32(((0x63E3* 4 ) | PHY_ADDR),0x7FF1);
	phx_write_u32(((0x63E4* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x63E5* 4 ) | PHY_ADDR),0x1000);
	phx_write_u32(((0x63E6* 4 ) | PHY_ADDR),0x5336);
	phx_write_u32(((0x63E7* 4 ) | PHY_ADDR),0x5336);
	phx_write_u32(((0x63E8* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x63F3* 4 ) | PHY_ADDR),0x392B);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0401);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0001);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0402);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0002);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0403);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0003);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0404);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0004);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0405);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0005);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0406);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0006);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0407);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0007);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0408);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0008);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0409);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0009);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x040A);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x000A);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x040B);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x000B);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x040C);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x000C);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x040D);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x000D);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x050D);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x000E);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x050E);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x000F);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x060F);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0010);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0610);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0011);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0710);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0012);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0711);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0013);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0812);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0014);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0813);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0015);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0913);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0016);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0914);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0017);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0A15);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0018);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0A16);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0019);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0B16);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x001A);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0B17);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x001B);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0C18);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x001C);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0C19);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x001D);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0D19);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x001E);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0D1A);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x001F);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0E1B);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0020);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0E1C);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0021);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0F1C);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0022);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0F1D);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0023);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x101E);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0024);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x101F);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0025);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x111F);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0026);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1120);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0027);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1221);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0028);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1221);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0029);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1322);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x002A);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1322);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x002B);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1423);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x002C);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1423);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x002D);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1524);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x002E);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1525);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x002F);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1626);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0030);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1627);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0031);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1728);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0032);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1729);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0033);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1829);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0034);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x182A);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0035);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x192C);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0036);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x192D);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0037);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1A2E);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0038);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1A2F);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0039);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1B30);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x003A);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1B31);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x003B);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1B32);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x003C);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1B33);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x003D);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1B34);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x003E);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1B35);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x003F);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1B36);
	#if 1
	phx_write_u32(((0x6406* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x6439* 4 ) | PHY_ADDR),0x0606);
	phx_write_u32(((0x643B* 4 ) | PHY_ADDR),0x000C);
	phx_write_u32(((0x644C* 4 ) | PHY_ADDR),0x0D33);
	phx_write_u32(((0x646F* 4 ) | PHY_ADDR),0x9602);
	phx_write_u32(((0x6471* 4 ) | PHY_ADDR),0x0271);
	phx_write_u32(((0x6481* 4 ) | PHY_ADDR),0x813E);
	phx_write_u32(((0x6485* 4 ) | PHY_ADDR),0x8E82);
	phx_write_u32(((0x6486* 4 ) | PHY_ADDR),0x933A);
	phx_write_u32(((0x6487* 4 ) | PHY_ADDR),0x8F5B);
	phx_write_u32(((0x6488* 4 ) | PHY_ADDR),0x8B4B);
	phx_write_u32(((0x6491* 4 ) | PHY_ADDR),0x033C);
	phx_write_u32(((0x6496* 4 ) | PHY_ADDR),0x0003);
	phx_write_u32(((0x6497* 4 ) | PHY_ADDR),0x44CC);
	phx_write_u32(((0x649B* 4 ) | PHY_ADDR),0x088C);
	phx_write_u32(((0x649C* 4 ) | PHY_ADDR),0x8000);
	phx_write_u32(((0x64AC* 4 ) | PHY_ADDR),0x1F1F);
	phx_write_u32(((0x64AD* 4 ) | PHY_ADDR),0x0080);
	phx_write_u32(((0x64AF* 4 ) | PHY_ADDR),0x1C08);
	phx_write_u32(((0x64C4* 4 ) | PHY_ADDR),0x0003);
	phx_write_u32(((0x64D0* 4 ) | PHY_ADDR),0x3C3C);
	phx_write_u32(((0x64E1* 4 ) | PHY_ADDR),0x3100);
	phx_write_u32(((0x64EA* 4 ) | PHY_ADDR),0x0551);
	phx_write_u32(((0x64EB* 4 ) | PHY_ADDR),0x0022);
	phx_write_u32(((0x64EC* 4 ) | PHY_ADDR),0x0010);
	phx_write_u32(((0x64ED* 4 ) | PHY_ADDR),0x0010);
	phx_write_u32(((0x64EE* 4 ) | PHY_ADDR),0x0008);
	phx_write_u32(((0x64F5* 4 ) | PHY_ADDR),0x1561);
	phx_write_u32(((0x64F8* 4 ) | PHY_ADDR),0x1561);
	phx_write_u32(((0x64FA* 4 ) | PHY_ADDR),0x00B0);
	phx_write_u32(((0x64FB* 4 ) | PHY_ADDR),0x00A6);
	phx_write_u32(((0x64FC* 4 ) | PHY_ADDR),0x01B6);
	phx_write_u32(((0x64FF* 4 ) | PHY_ADDR),0x0336);
	phx_write_u32(((0x6500* 4 ) | PHY_ADDR),0x0788);
	phx_write_u32(((0x6501* 4 ) | PHY_ADDR),0x0777);
	phx_write_u32(((0x6502* 4 ) | PHY_ADDR),0x0BCC);
	phx_write_u32(((0x6503* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x6504* 4 ) | PHY_ADDR),0x0DFF);
	phx_write_u32(((0x6505* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x6506* 4 ) | PHY_ADDR),0x0CDD);
	phx_write_u32(((0x6507* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x6508* 4 ) | PHY_ADDR),0x0CDD);
	phx_write_u32(((0x6509* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x650A* 4 ) | PHY_ADDR),0x0CDD);
	phx_write_u32(((0x650B* 4 ) | PHY_ADDR),0x0599);
	phx_write_u32(((0x650C* 4 ) | PHY_ADDR),0x0DEE);
	phx_write_u32(((0x650D* 4 ) | PHY_ADDR),0x0084);
	phx_write_u32(((0x650E* 4 ) | PHY_ADDR),0x0399);
	phx_write_u32(((0x650F* 4 ) | PHY_ADDR),0x0DEE);
	phx_write_u32(((0x6511* 4 ) | PHY_ADDR),0x28FF);
	phx_write_u32(((0x6515* 4 ) | PHY_ADDR),0x0160);
	phx_write_u32(((0x6516* 4 ) | PHY_ADDR),0x0008);
	phx_write_u32(((0x6518* 4 ) | PHY_ADDR),0x1000);
	phx_write_u32(((0x651A* 4 ) | PHY_ADDR),0x0100);
	phx_write_u32(((0x651B* 4 ) | PHY_ADDR),0x0080);
	phx_write_u32(((0x651C* 4 ) | PHY_ADDR),0x0024);
	phx_write_u32(((0x651D* 4 ) | PHY_ADDR),0x0024);
	phx_write_u32(((0x651E* 4 ) | PHY_ADDR),0x01FF);
	phx_write_u32(((0x6521* 4 ) | PHY_ADDR),0x001F);
	phx_write_u32(((0x6522* 4 ) | PHY_ADDR),0x1CE7);
	phx_write_u32(((0x6523* 4 ) | PHY_ADDR),0x1D67);
	phx_write_u32(((0x6525* 4 ) | PHY_ADDR),0x001F);
	phx_write_u32(((0x6526* 4 ) | PHY_ADDR),0x4000);
	phx_write_u32(((0x6548* 4 ) | PHY_ADDR),0x2011);
	phx_write_u32(((0x6551* 4 ) | PHY_ADDR),0x0606);
	phx_write_u32(((0x6552* 4 ) | PHY_ADDR),0x0100);
	phx_write_u32(((0x6555* 4 ) | PHY_ADDR),0x0220);
	phx_write_u32(((0x6558* 4 ) | PHY_ADDR),0xFE21);
	phx_write_u32(((0x6559* 4 ) | PHY_ADDR),0x2121);
	phx_write_u32(((0x655C* 4 ) | PHY_ADDR),0x5882);
	phx_write_u32(((0x6561* 4 ) | PHY_ADDR),0x0023);
	phx_write_u32(((0x65B0* 4 ) | PHY_ADDR),0x559E);
	phx_write_u32(((0x65D8* 4 ) | PHY_ADDR),0x0019);
	phx_write_u32(((0x65D9* 4 ) | PHY_ADDR),0x7FFF);
	phx_write_u32(((0x65DA* 4 ) | PHY_ADDR),0x0028);
	phx_write_u32(((0x65DB* 4 ) | PHY_ADDR),0x0028);
	phx_write_u32(((0x65DC* 4 ) | PHY_ADDR),0x0005);
	phx_write_u32(((0x65DD* 4 ) | PHY_ADDR),0x000F);
	phx_write_u32(((0x65DE* 4 ) | PHY_ADDR),0x7FC3);
	phx_write_u32(((0x65DF* 4 ) | PHY_ADDR),0x7F81);
	phx_write_u32(((0x65E0* 4 ) | PHY_ADDR),0x7FC3);
	phx_write_u32(((0x65E1* 4 ) | PHY_ADDR),0x7FC3);
	phx_write_u32(((0x65E2* 4 ) | PHY_ADDR),0x7F81);
	phx_write_u32(((0x65E3* 4 ) | PHY_ADDR),0x7FF1);
	phx_write_u32(((0x65E4* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x65E5* 4 ) | PHY_ADDR),0x1000);
	phx_write_u32(((0x65E6* 4 ) | PHY_ADDR),0x5336);
	phx_write_u32(((0x65E7* 4 ) | PHY_ADDR),0x5336);
	phx_write_u32(((0x65E8* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x65F3* 4 ) | PHY_ADDR),0x392B);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0401);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0001);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0402);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0002);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0403);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0003);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0404);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0004);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0405);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0005);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0406);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0006);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0407);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0007);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0408);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0008);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0409);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0009);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x040A);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x000A);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x040B);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x000B);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x040C);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x000C);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x040D);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x000D);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x050D);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x000E);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x050E);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x000F);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x060F);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0010);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0610);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0011);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0710);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0012);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0711);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0013);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0812);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0014);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0813);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0015);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0913);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0016);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0914);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0017);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0A15);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0018);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0A16);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0019);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0B16);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x001A);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0B17);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x001B);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0C18);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x001C);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0C19);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x001D);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0D19);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x001E);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0D1A);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x001F);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0E1B);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0020);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0E1C);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0021);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0F1C);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0022);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0F1D);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0023);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x101E);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0024);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x101F);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0025);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x111F);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0026);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1120);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0027);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1221);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0028);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1221);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0029);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1322);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x002A);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1322);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x002B);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1423);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x002C);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1423);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x002D);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1524);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x002E);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1525);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x002F);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1626);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0030);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1627);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0031);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1728);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0032);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1729);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0033);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1829);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0034);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x182A);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0035);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x192C);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0036);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x192D);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0037);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1A2E);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0038);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1A2F);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0039);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1B30);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x003A);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1B31);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x003B);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1B32);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x003C);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1B33);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x003D);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1B34);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x003E);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1B35);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x003F);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1B36);
	phx_write_u32(((0x6606* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x6639* 4 ) | PHY_ADDR),0x0606);
	phx_write_u32(((0x663B* 4 ) | PHY_ADDR),0x000C);
	phx_write_u32(((0x664C* 4 ) | PHY_ADDR),0x0D33);
	phx_write_u32(((0x666F* 4 ) | PHY_ADDR),0x9602);
	phx_write_u32(((0x6671* 4 ) | PHY_ADDR),0x0271);
	phx_write_u32(((0x6681* 4 ) | PHY_ADDR),0x813E);
	phx_write_u32(((0x6685* 4 ) | PHY_ADDR),0x8E82);
	phx_write_u32(((0x6686* 4 ) | PHY_ADDR),0x933A);
	phx_write_u32(((0x6687* 4 ) | PHY_ADDR),0x8F5B);
	phx_write_u32(((0x6688* 4 ) | PHY_ADDR),0x8B4B);
	phx_write_u32(((0x6691* 4 ) | PHY_ADDR),0x033C);
	phx_write_u32(((0x6696* 4 ) | PHY_ADDR),0x0003);
	phx_write_u32(((0x6697* 4 ) | PHY_ADDR),0x44CC);
	phx_write_u32(((0x669B* 4 ) | PHY_ADDR),0x088C);
	phx_write_u32(((0x669C* 4 ) | PHY_ADDR),0x8000);
	phx_write_u32(((0x66AC* 4 ) | PHY_ADDR),0x1F1F);
	phx_write_u32(((0x66AD* 4 ) | PHY_ADDR),0x0080);
	phx_write_u32(((0x66AF* 4 ) | PHY_ADDR),0x1C08);
	phx_write_u32(((0x66C4* 4 ) | PHY_ADDR),0x0003);
	phx_write_u32(((0x66D0* 4 ) | PHY_ADDR),0x3C3C);
	phx_write_u32(((0x66E1* 4 ) | PHY_ADDR),0x3100);
	phx_write_u32(((0x66EA* 4 ) | PHY_ADDR),0x0551);
	phx_write_u32(((0x66EB* 4 ) | PHY_ADDR),0x0022);
	phx_write_u32(((0x66EC* 4 ) | PHY_ADDR),0x0010);
	phx_write_u32(((0x66ED* 4 ) | PHY_ADDR),0x0010);
	phx_write_u32(((0x66EE* 4 ) | PHY_ADDR),0x0008);
	phx_write_u32(((0x66F5* 4 ) | PHY_ADDR),0x1561);
	phx_write_u32(((0x66F8* 4 ) | PHY_ADDR),0x1561);
	phx_write_u32(((0x66FA* 4 ) | PHY_ADDR),0x00B0);
	phx_write_u32(((0x66FB* 4 ) | PHY_ADDR),0x00A6);
	phx_write_u32(((0x66FC* 4 ) | PHY_ADDR),0x01B6);
	phx_write_u32(((0x66FF* 4 ) | PHY_ADDR),0x0336);
	phx_write_u32(((0x6700* 4 ) | PHY_ADDR),0x0788);
	phx_write_u32(((0x6701* 4 ) | PHY_ADDR),0x0777);
	phx_write_u32(((0x6702* 4 ) | PHY_ADDR),0x0BCC);
	phx_write_u32(((0x6703* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x6704* 4 ) | PHY_ADDR),0x0DFF);
	phx_write_u32(((0x6705* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x6706* 4 ) | PHY_ADDR),0x0CDD);
	phx_write_u32(((0x6707* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x6708* 4 ) | PHY_ADDR),0x0CDD);
	phx_write_u32(((0x6709* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x670A* 4 ) | PHY_ADDR),0x0CDD);
	phx_write_u32(((0x670B* 4 ) | PHY_ADDR),0x0599);
	phx_write_u32(((0x670C* 4 ) | PHY_ADDR),0x0DEE);
	phx_write_u32(((0x670D* 4 ) | PHY_ADDR),0x0084);
	phx_write_u32(((0x670E* 4 ) | PHY_ADDR),0x0399);
	phx_write_u32(((0x670F* 4 ) | PHY_ADDR),0x0DEE);
	phx_write_u32(((0x6711* 4 ) | PHY_ADDR),0x28FF);
	phx_write_u32(((0x6715* 4 ) | PHY_ADDR),0x0160);
	phx_write_u32(((0x6716* 4 ) | PHY_ADDR),0x0008);
	phx_write_u32(((0x6718* 4 ) | PHY_ADDR),0x1000);
	phx_write_u32(((0x671A* 4 ) | PHY_ADDR),0x0100);
	phx_write_u32(((0x671B* 4 ) | PHY_ADDR),0x0080);
	phx_write_u32(((0x671C* 4 ) | PHY_ADDR),0x0024);
	phx_write_u32(((0x671D* 4 ) | PHY_ADDR),0x0024);
	phx_write_u32(((0x671E* 4 ) | PHY_ADDR),0x01FF);
	phx_write_u32(((0x6721* 4 ) | PHY_ADDR),0x001F);
	phx_write_u32(((0x6722* 4 ) | PHY_ADDR),0x1CE7);
	phx_write_u32(((0x6723* 4 ) | PHY_ADDR),0x1D67);
	phx_write_u32(((0x6725* 4 ) | PHY_ADDR),0x001F);
	phx_write_u32(((0x6726* 4 ) | PHY_ADDR),0x4000);
	phx_write_u32(((0x6748* 4 ) | PHY_ADDR),0x2011);
	phx_write_u32(((0x6751* 4 ) | PHY_ADDR),0x0606);
	phx_write_u32(((0x6752* 4 ) | PHY_ADDR),0x0100);
	phx_write_u32(((0x6755* 4 ) | PHY_ADDR),0x0220);
	phx_write_u32(((0x6758* 4 ) | PHY_ADDR),0xFE21);
	phx_write_u32(((0x6759* 4 ) | PHY_ADDR),0x2121);
	phx_write_u32(((0x675C* 4 ) | PHY_ADDR),0x5882);
	phx_write_u32(((0x6761* 4 ) | PHY_ADDR),0x0023);
	phx_write_u32(((0x67B0* 4 ) | PHY_ADDR),0x559E);
	phx_write_u32(((0x67D8* 4 ) | PHY_ADDR),0x0019);
	phx_write_u32(((0x67D9* 4 ) | PHY_ADDR),0x7FFF);
	phx_write_u32(((0x67DA* 4 ) | PHY_ADDR),0x0028);
	phx_write_u32(((0x67DB* 4 ) | PHY_ADDR),0x0028);
	phx_write_u32(((0x67DC* 4 ) | PHY_ADDR),0x0005);
	phx_write_u32(((0x67DD* 4 ) | PHY_ADDR),0x000F);
	phx_write_u32(((0x67DE* 4 ) | PHY_ADDR),0x7FC3);
	phx_write_u32(((0x67DF* 4 ) | PHY_ADDR),0x7F81);
	phx_write_u32(((0x67E0* 4 ) | PHY_ADDR),0x7FC3);
	phx_write_u32(((0x67E1* 4 ) | PHY_ADDR),0x7FC3);
	phx_write_u32(((0x67E2* 4 ) | PHY_ADDR),0x7F81);
	phx_write_u32(((0x67E3* 4 ) | PHY_ADDR),0x7FF1);
	phx_write_u32(((0x67E4* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x67E5* 4 ) | PHY_ADDR),0x1000);
	phx_write_u32(((0x67E6* 4 ) | PHY_ADDR),0x5336);
	phx_write_u32(((0x67E7* 4 ) | PHY_ADDR),0x5336);
	phx_write_u32(((0x67E8* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x67F3* 4 ) | PHY_ADDR),0x392B);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0401);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0001);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0402);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0002);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0403);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0003);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0404);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0004);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0405);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0005);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0406);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0006);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0407);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0007);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0408);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0008);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0409);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0009);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x040A);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x000A);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x040B);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x000B);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x040C);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x000C);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x040D);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x000D);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x050D);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x000E);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x050E);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x000F);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x060F);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0010);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0610);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0011);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0710);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0012);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0711);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0013);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0812);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0014);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0813);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0015);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0913);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0016);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0914);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0017);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0A15);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0018);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0A16);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0019);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0B16);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x001A);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0B17);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x001B);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0C18);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x001C);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0C19);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x001D);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0D19);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x001E);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0D1A);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x001F);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0E1B);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0020);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0E1C);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0021);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0F1C);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0022);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0F1D);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0023);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x101E);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0024);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x101F);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0025);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x111F);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0026);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1120);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0027);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1221);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0028);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1221);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0029);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1322);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x002A);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1322);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x002B);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1423);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x002C);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1423);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x002D);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1524);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x002E);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1525);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x002F);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1626);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0030);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1627);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0031);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1728);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0032);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1729);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0033);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1829);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0034);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x182A);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0035);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x192C);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0036);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x192D);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0037);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1A2E);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0038);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1A2F);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0039);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1B30);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x003A);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1B31);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x003B);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1B32);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x003C);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1B33);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x003D);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1B34);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x003E);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1B35);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x003F);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1B36);
	#endif
	    
	/*
	for(j = 0; j < 1; j++)
	            {                
	                phx_write_u32(((0xD013 * 4 ) | PHY_ADDR  |  (j << 0x10),0x1111);
	            }
	*/

        
}


//PHY配置(X16): 正常使用(慢仿真) 模式配置, 偏移地址需要乘4后使用
void pcie_x16_pcie_pma_nospeedup_sequence_x16(void)
{
    u32 PHY_ADDR       = PCIE_X16_PHY_IP_BASE_ADDR;
    u32 PHY_REG_ADDR   = PCIE_X16_PHY_MISC_BASE_ADDR;

    uart_printf("pcie_x16_pcie_pma_nospeedup_sequence_x16.\n\r");

/*PHY配置(X16): 正常使用(慢仿真) 模式配置:226 Registers   start------------------------------------------------*/
	phx_write_u32(((0xC003* 4 ) | PHY_ADDR),0x0144);
	phx_write_u32(((0xE000* 4 ) | PHY_ADDR),0x3300);
	phx_write_u32(((0x00A0* 4 ) | PHY_ADDR),0x002F);
	phx_write_u32(((0x0048* 4 ) | PHY_ADDR),0x0105);
	phx_write_u32(((0x0049* 4 ) | PHY_ADDR),0x2105);
	phx_write_u32(((0x004A* 4 ) | PHY_ADDR),0x3106);
	phx_write_u32(((0x0050* 4 ) | PHY_ADDR),0x8804);
	phx_write_u32(((0x005A* 4 ) | PHY_ADDR),0x0CDB);
	phx_write_u32(((0x0062* 4 ) | PHY_ADDR),0x1219);
	phx_write_u32(((0x0023* 4 ) | PHY_ADDR),0x413B);
	phx_write_u32(((0x6006* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x604C* 4 ) | PHY_ADDR),0x0D33);
	phx_write_u32(((0x606F* 4 ) | PHY_ADDR),0x9602);//for x16 link->x8 //0x9703,0x9501

	phx_write_u32(((0x6071* 4 ) | PHY_ADDR),0x0271);
	phx_write_u32(((0x6081* 4 ) | PHY_ADDR),0x813E);
	phx_write_u32(((0x6085* 4 ) | PHY_ADDR),0x8E82);
	phx_write_u32(((0x6086* 4 ) | PHY_ADDR),0x933A);
	phx_write_u32(((0x6087* 4 ) | PHY_ADDR),0x8F5B);
	phx_write_u32(((0x6088* 4 ) | PHY_ADDR),0x8B4B);
	phx_write_u32(((0x6091* 4 ) | PHY_ADDR),0x033C);
	phx_write_u32(((0x6096* 4 ) | PHY_ADDR),0x0003);
	phx_write_u32(((0x6097* 4 ) | PHY_ADDR),0x44CC);
	phx_write_u32(((0x609B* 4 ) | PHY_ADDR),0x088C);
	phx_write_u32(((0x609C* 4 ) | PHY_ADDR),0x8000);
	phx_write_u32(((0x60AC* 4 ) | PHY_ADDR),0x1F1F);
	phx_write_u32(((0x60AD* 4 ) | PHY_ADDR),0x0080);
	phx_write_u32(((0x60AF* 4 ) | PHY_ADDR),0x1C08);
	phx_write_u32(((0x60C4* 4 ) | PHY_ADDR),0x0003);
	phx_write_u32(((0x60D0* 4 ) | PHY_ADDR),0x3C3C);
	phx_write_u32(((0x60E1* 4 ) | PHY_ADDR),0x3100);
	phx_write_u32(((0x60EA* 4 ) | PHY_ADDR),0x0551);
	phx_write_u32(((0x60EB* 4 ) | PHY_ADDR),0x0022);
	phx_write_u32(((0x60EC* 4 ) | PHY_ADDR),0x0010);
	phx_write_u32(((0x60ED* 4 ) | PHY_ADDR),0x0010);
	phx_write_u32(((0x60EE* 4 ) | PHY_ADDR),0x0008);
	phx_write_u32(((0x60F5* 4 ) | PHY_ADDR),0x1561);
	phx_write_u32(((0x60F8* 4 ) | PHY_ADDR),0x1561);
	phx_write_u32(((0x60FA* 4 ) | PHY_ADDR),0x00B0);
	phx_write_u32(((0x60FB* 4 ) | PHY_ADDR),0x00A6);
	phx_write_u32(((0x60FC* 4 ) | PHY_ADDR),0x01B6);
	phx_write_u32(((0x60FF* 4 ) | PHY_ADDR),0x0336);
	phx_write_u32(((0x6100* 4 ) | PHY_ADDR),0x0788);
	phx_write_u32(((0x6101* 4 ) | PHY_ADDR),0x0777);
	phx_write_u32(((0x6102* 4 ) | PHY_ADDR),0x0BCC);
	phx_write_u32(((0x6103* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x6104* 4 ) | PHY_ADDR),0x0DFF);
	phx_write_u32(((0x6105* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x6106* 4 ) | PHY_ADDR),0x0CDD);
	phx_write_u32(((0x6107* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x6108* 4 ) | PHY_ADDR),0x0CDD);
	phx_write_u32(((0x6109* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x610A* 4 ) | PHY_ADDR),0x0CDD);
	phx_write_u32(((0x610B* 4 ) | PHY_ADDR),0x0599);
	phx_write_u32(((0x610C* 4 ) | PHY_ADDR),0x0DEE);
	phx_write_u32(((0x610D* 4 ) | PHY_ADDR),0x0084);
	phx_write_u32(((0x610E* 4 ) | PHY_ADDR),0x0399);
	phx_write_u32(((0x610F* 4 ) | PHY_ADDR),0x0DEE);
	phx_write_u32(((0x6111* 4 ) | PHY_ADDR),0x28FF);
	phx_write_u32(((0x6115* 4 ) | PHY_ADDR),0x0160);
	phx_write_u32(((0x6116* 4 ) | PHY_ADDR),0x0008);
	phx_write_u32(((0x6118* 4 ) | PHY_ADDR),0x1000);
	phx_write_u32(((0x611A* 4 ) | PHY_ADDR),0x0100);
	phx_write_u32(((0x611B* 4 ) | PHY_ADDR),0x0080);
	phx_write_u32(((0x611C* 4 ) | PHY_ADDR),0x0024);
	phx_write_u32(((0x611D* 4 ) | PHY_ADDR),0x0024);
	phx_write_u32(((0x611E* 4 ) | PHY_ADDR),0x01FF);
	phx_write_u32(((0x6121* 4 ) | PHY_ADDR),0x001F);
	phx_write_u32(((0x6122* 4 ) | PHY_ADDR),0x1CE7);
	phx_write_u32(((0x6123* 4 ) | PHY_ADDR),0x1D67);
	phx_write_u32(((0x6125* 4 ) | PHY_ADDR),0x001F);
	phx_write_u32(((0x6126* 4 ) | PHY_ADDR),0x4000);
	phx_write_u32(((0x6148* 4 ) | PHY_ADDR),0x2011);
	phx_write_u32(((0x6151* 4 ) | PHY_ADDR),0x0606);
	phx_write_u32(((0x6152* 4 ) | PHY_ADDR),0x0100);
	phx_write_u32(((0x6155* 4 ) | PHY_ADDR),0x0220);
	phx_write_u32(((0x6158* 4 ) | PHY_ADDR),0xFE21);
	phx_write_u32(((0x6159* 4 ) | PHY_ADDR),0x2121);
	phx_write_u32(((0x615C* 4 ) | PHY_ADDR),0x5882);
	phx_write_u32(((0x6161* 4 ) | PHY_ADDR),0x0023);
	phx_write_u32(((0x61B0* 4 ) | PHY_ADDR),0x559E);
	phx_write_u32(((0x61D8* 4 ) | PHY_ADDR),0x0019);
	phx_write_u32(((0x61D9* 4 ) | PHY_ADDR),0x7FFF);
	phx_write_u32(((0x61DA* 4 ) | PHY_ADDR),0x0028);
	phx_write_u32(((0x61DB* 4 ) | PHY_ADDR),0x0028);
	phx_write_u32(((0x61DC* 4 ) | PHY_ADDR),0x0005);
	phx_write_u32(((0x61DD* 4 ) | PHY_ADDR),0x000F);
	phx_write_u32(((0x61DE* 4 ) | PHY_ADDR),0x7FC3);
	phx_write_u32(((0x61DF* 4 ) | PHY_ADDR),0x7F81);
	phx_write_u32(((0x61E0* 4 ) | PHY_ADDR),0x7FC3);
	phx_write_u32(((0x61E1* 4 ) | PHY_ADDR),0x7FC3);
	phx_write_u32(((0x61E2* 4 ) | PHY_ADDR),0x7F81);
	phx_write_u32(((0x61E3* 4 ) | PHY_ADDR),0x7FF1);
	phx_write_u32(((0x61E4* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x61E5* 4 ) | PHY_ADDR),0x1000);
	phx_write_u32(((0x61E6* 4 ) | PHY_ADDR),0x5336);
	phx_write_u32(((0x61E7* 4 ) | PHY_ADDR),0x5336);
	phx_write_u32(((0x61E8* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x61F3* 4 ) | PHY_ADDR),0x392B);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0401);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0001);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0402);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0002);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0403);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0003);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0404);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0004);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0405);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0005);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0406);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0006);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0407);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0007);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0408);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0008);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0409);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0009);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x040A);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x000A);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x040B);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x000B);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x040C);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x000C);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x040D);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x000D);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x050D);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x000E);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x050E);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x000F);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x060F);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0010);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0610);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0011);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0710);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0012);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0711);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0013);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0812);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0014);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0813);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0015);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0913);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0016);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0914);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0017);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0A15);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0018);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0A16);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0019);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0B16);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x001A);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0B17);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x001B);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0C18);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x001C);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0C19);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x001D);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0D19);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x001E);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0D1A);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x001F);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0E1B);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0020);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0E1C);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0021);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0F1C);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0022);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0F1D);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0023);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x101E);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0024);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x101F);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0025);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x111F);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0026);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1120);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0027);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1221);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0028);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1221);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0029);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1322);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x002A);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1322);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x002B);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1423);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x002C);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1423);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x002D);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1524);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x002E);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1525);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x002F);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1626);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0030);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1627);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0031);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1728);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0032);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1729);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0033);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1829);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0034);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x182A);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0035);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x192C);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0036);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x192D);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0037);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1A2E);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0038);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1A2F);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0039);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1B30);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x003A);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1B31);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x003B);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1B32);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x003C);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1B33);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x003D);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1B34);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x003E);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1B35);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x003F);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1B36);
/*PHY配置(X16): 正常使用(慢仿真) 模式配置:226 Registers   end------------------------------------------------*/
	
	
	//fudy.for x16 not stable:
#if 1 
	phx_write_u32(((0x08d* 4 ) | PHY_ADDR),0x10081840);
	phx_write_u32(((0x0da* 4 ) | PHY_ADDR),0x00000002);
	phx_write_u32(((0x0df* 4 ) | PHY_ADDR),0x00000001);
	phx_write_u32(((0x398* 4 ) | PHY_ADDR),0x00006c09);
#endif
	return;
}

//X16:x4_4 pcie_pma_nospeedup_sequence ,fudy.
void pcie_x16_pcie_pma_nospeedup_sequence_x4_4(void)
{

    u32 PHY_ADDR       = PCIE_X16_PHY_IP_BASE_ADDR;
    u32 PHY_REG_ADDR   = PCIE_X16_PHY_MISC_BASE_ADDR;

    uart_printf("pcie_x16_pcie_pma_nospeedup_sequence_x4_4.\n\r");

    //x16(x4_4),nospeedup:226 Registers
	phx_write_u32(((0xC003* 4 ) | PHY_ADDR),0x0144);
	phx_write_u32(((0xC00E* 4 ) | PHY_ADDR),0x0002);
	phx_write_u32(((0xE000* 4 ) | PHY_ADDR),0x3300);
	phx_write_u32(((0x0098* 4 ) | PHY_ADDR),0x2100);
	phx_write_u32(((0x00A0* 4 ) | PHY_ADDR),0x002F);
	phx_write_u32(((0x0043* 4 ) | PHY_ADDR),0x002D);
	phx_write_u32(((0x0048* 4 ) | PHY_ADDR),0x0105);
	phx_write_u32(((0x00C8* 4 ) | PHY_ADDR),0x0105);
	phx_write_u32(((0x004A* 4 ) | PHY_ADDR),0x2187);
	phx_write_u32(((0x00CA* 4 ) | PHY_ADDR),0x2105);
	phx_write_u32(((0x004E* 4 ) | PHY_ADDR),0x8205);
	phx_write_u32(((0x0050* 4 ) | PHY_ADDR),0x8804);
	phx_write_u32(((0x005A* 4 ) | PHY_ADDR),0x0CDB);
	phx_write_u32(((0x00DA* 4 ) | PHY_ADDR),0x0CDB);
	phx_write_u32(((0x0062* 4 ) | PHY_ADDR),0x1219);
	phx_write_u32(((0x00E2* 4 ) | PHY_ADDR),0x1912);
	phx_write_u32(((0x0023* 4 ) | PHY_ADDR),0x413B);	
	phx_write_u32(((0x6006* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x6039* 4 ) | PHY_ADDR),0x0606);
	phx_write_u32(((0x603B* 4 ) | PHY_ADDR),0x000C);
	phx_write_u32(((0x604C* 4 ) | PHY_ADDR),0x0D33);
	phx_write_u32(((0x606F* 4 ) | PHY_ADDR),0x9602);
	phx_write_u32(((0x6071* 4 ) | PHY_ADDR),0x0271);
	phx_write_u32(((0x6081* 4 ) | PHY_ADDR),0x813E);
	phx_write_u32(((0x6085* 4 ) | PHY_ADDR),0x8E82);
	phx_write_u32(((0x6086* 4 ) | PHY_ADDR),0x933A);
	phx_write_u32(((0x6087* 4 ) | PHY_ADDR),0x8F5B);
	phx_write_u32(((0x6088* 4 ) | PHY_ADDR),0x8B4B);
	phx_write_u32(((0x6091* 4 ) | PHY_ADDR),0x033C);
	phx_write_u32(((0x6096* 4 ) | PHY_ADDR),0x0003);
	phx_write_u32(((0x6097* 4 ) | PHY_ADDR),0x44CC);
	phx_write_u32(((0x609B* 4 ) | PHY_ADDR),0x088C);
	phx_write_u32(((0x609C* 4 ) | PHY_ADDR),0x8000);
	phx_write_u32(((0x60AC* 4 ) | PHY_ADDR),0x1F1F);
	phx_write_u32(((0x60AD* 4 ) | PHY_ADDR),0x0080);
	phx_write_u32(((0x60AF* 4 ) | PHY_ADDR),0x1C08);
	phx_write_u32(((0x60C4* 4 ) | PHY_ADDR),0x0003);
	phx_write_u32(((0x60D0* 4 ) | PHY_ADDR),0x3C3C);
	phx_write_u32(((0x60E1* 4 ) | PHY_ADDR),0x3100);
	phx_write_u32(((0x60EA* 4 ) | PHY_ADDR),0x0551);
	phx_write_u32(((0x60EB* 4 ) | PHY_ADDR),0x0022);
	phx_write_u32(((0x60EC* 4 ) | PHY_ADDR),0x0010);
	phx_write_u32(((0x60ED* 4 ) | PHY_ADDR),0x0010);
	phx_write_u32(((0x60EE* 4 ) | PHY_ADDR),0x0008);
	phx_write_u32(((0x60F5* 4 ) | PHY_ADDR),0x1561);
	phx_write_u32(((0x60F8* 4 ) | PHY_ADDR),0x1561);
	phx_write_u32(((0x60FA* 4 ) | PHY_ADDR),0x00B0);
	phx_write_u32(((0x60FB* 4 ) | PHY_ADDR),0x00A6);
	phx_write_u32(((0x60FC* 4 ) | PHY_ADDR),0x01B6);
	phx_write_u32(((0x60FF* 4 ) | PHY_ADDR),0x0336);
	phx_write_u32(((0x6100* 4 ) | PHY_ADDR),0x0788);
	phx_write_u32(((0x6101* 4 ) | PHY_ADDR),0x0777);
	phx_write_u32(((0x6102* 4 ) | PHY_ADDR),0x0BCC);
	phx_write_u32(((0x6103* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x6104* 4 ) | PHY_ADDR),0x0DFF);
	phx_write_u32(((0x6105* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x6106* 4 ) | PHY_ADDR),0x0CDD);
	phx_write_u32(((0x6107* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x6108* 4 ) | PHY_ADDR),0x0CDD);
	phx_write_u32(((0x6109* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x610A* 4 ) | PHY_ADDR),0x0CDD);
	phx_write_u32(((0x610B* 4 ) | PHY_ADDR),0x0599);
	phx_write_u32(((0x610C* 4 ) | PHY_ADDR),0x0DEE);
	phx_write_u32(((0x610D* 4 ) | PHY_ADDR),0x0084);
	phx_write_u32(((0x610E* 4 ) | PHY_ADDR),0x0399);
	phx_write_u32(((0x610F* 4 ) | PHY_ADDR),0x0DEE);
	phx_write_u32(((0x6111* 4 ) | PHY_ADDR),0x28FF);
	phx_write_u32(((0x6115* 4 ) | PHY_ADDR),0x0160);
	phx_write_u32(((0x6116* 4 ) | PHY_ADDR),0x0008);
	phx_write_u32(((0x6118* 4 ) | PHY_ADDR),0x1000);
	phx_write_u32(((0x611A* 4 ) | PHY_ADDR),0x0100);
	phx_write_u32(((0x611B* 4 ) | PHY_ADDR),0x0080);
	phx_write_u32(((0x611C* 4 ) | PHY_ADDR),0x0024);
	phx_write_u32(((0x611D* 4 ) | PHY_ADDR),0x0024);
	phx_write_u32(((0x611E* 4 ) | PHY_ADDR),0x01FF);
	phx_write_u32(((0x6121* 4 ) | PHY_ADDR),0x001F);
	phx_write_u32(((0x6122* 4 ) | PHY_ADDR),0x1CE7);
	phx_write_u32(((0x6123* 4 ) | PHY_ADDR),0x1D67);
	phx_write_u32(((0x6125* 4 ) | PHY_ADDR),0x001F);
	phx_write_u32(((0x6126* 4 ) | PHY_ADDR),0x4000);
	phx_write_u32(((0x6148* 4 ) | PHY_ADDR),0x2011);
	phx_write_u32(((0x6151* 4 ) | PHY_ADDR),0x0606);
	phx_write_u32(((0x6152* 4 ) | PHY_ADDR),0x0100);
	phx_write_u32(((0x6155* 4 ) | PHY_ADDR),0x0220);
	phx_write_u32(((0x6158* 4 ) | PHY_ADDR),0xFE21);
	phx_write_u32(((0x6159* 4 ) | PHY_ADDR),0x2121);
	phx_write_u32(((0x615C* 4 ) | PHY_ADDR),0x5882);
	phx_write_u32(((0x6161* 4 ) | PHY_ADDR),0x0023);	
	phx_write_u32(((0x61B0* 4 ) | PHY_ADDR),0x559E);
	phx_write_u32(((0x61D8* 4 ) | PHY_ADDR),0x0019);
	phx_write_u32(((0x61D9* 4 ) | PHY_ADDR),0x7FFF);
	phx_write_u32(((0x61DA* 4 ) | PHY_ADDR),0x0028);
	phx_write_u32(((0x61DB* 4 ) | PHY_ADDR),0x0028);
	phx_write_u32(((0x61DC* 4 ) | PHY_ADDR),0x0005);
	phx_write_u32(((0x61DD* 4 ) | PHY_ADDR),0x000F);
	phx_write_u32(((0x61DE* 4 ) | PHY_ADDR),0x7FC3);
	phx_write_u32(((0x61DF* 4 ) | PHY_ADDR),0x7F81);
	phx_write_u32(((0x61E0* 4 ) | PHY_ADDR),0x7FC3);
	phx_write_u32(((0x61E1* 4 ) | PHY_ADDR),0x7FC3);
	phx_write_u32(((0x61E2* 4 ) | PHY_ADDR),0x7F81);
	phx_write_u32(((0x61E3* 4 ) | PHY_ADDR),0x7FF1);
	phx_write_u32(((0x61E4* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x61E5* 4 ) | PHY_ADDR),0x1000);
	phx_write_u32(((0x61E6* 4 ) | PHY_ADDR),0x5336);
	phx_write_u32(((0x61E7* 4 ) | PHY_ADDR),0x5336);
	phx_write_u32(((0x61E8* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x61F3* 4 ) | PHY_ADDR),0x392B);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0401);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0001);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0402);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0002);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0403);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0003);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0404);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0004);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0405);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0005);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0406);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0006);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0407);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0007);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0408);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0008);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0409);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0009);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x040A);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x000A);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x040B);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x000B);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x040C);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x000C);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x040D);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x000D);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x050D);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x000E);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x050E);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x000F);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x060F);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0010);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0610);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0011);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0710);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0012);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0711);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0013);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0812);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0014);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0813);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0015);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0913);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0016);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0914);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0017);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0A15);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0018);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0A16);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0019);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0B16);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x001A);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0B17);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x001B);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0C18);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x001C);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0C19);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x001D);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0D19);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x001E);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0D1A);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x001F);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0E1B);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0020);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0E1C);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0021);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0F1C);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0022);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x0F1D);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0023);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x101E);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0024);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x101F);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0025);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x111F);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0026);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1120);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0027);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1221);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0028);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1221);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0029);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1322);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x002A);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1322);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x002B);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1423);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x002C);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1423);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x002D);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1524);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x002E);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1525);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x002F);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1626);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0030);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1627);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0031);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1728);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0032);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1729);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0033);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1829);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0034);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x182A);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0035);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x192C);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0036);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x192D);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0037);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1A2E);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0038);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1A2F);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x0039);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1B30);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x003A);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1B31);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x003B);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1B32);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x003C);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1B33);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x003D);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1B34);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x003E);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1B35);
	phx_write_u32(((0x60E8* 4 ) | PHY_ADDR),0x003F);
	phx_write_u32(((0x60E9* 4 ) | PHY_ADDR),0x1B36);	
	phx_write_u32(((0x6206* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x6239* 4 ) | PHY_ADDR),0x0606);
	phx_write_u32(((0x623B* 4 ) | PHY_ADDR),0x000C);
	phx_write_u32(((0x624C* 4 ) | PHY_ADDR),0x0D33);
	phx_write_u32(((0x626F* 4 ) | PHY_ADDR),0x9602);
	phx_write_u32(((0x6271* 4 ) | PHY_ADDR),0x0271);
	phx_write_u32(((0x6281* 4 ) | PHY_ADDR),0x813E);
	phx_write_u32(((0x6285* 4 ) | PHY_ADDR),0x8E82);
	phx_write_u32(((0x6286* 4 ) | PHY_ADDR),0x933A);
	phx_write_u32(((0x6287* 4 ) | PHY_ADDR),0x8F5B);
	phx_write_u32(((0x6288* 4 ) | PHY_ADDR),0x8B4B);
	phx_write_u32(((0x6291* 4 ) | PHY_ADDR),0x033C);
	phx_write_u32(((0x6296* 4 ) | PHY_ADDR),0x0003);
	phx_write_u32(((0x6297* 4 ) | PHY_ADDR),0x44CC);
	phx_write_u32(((0x629B* 4 ) | PHY_ADDR),0x088C);
	phx_write_u32(((0x629C* 4 ) | PHY_ADDR),0x8000);
	phx_write_u32(((0x62AC* 4 ) | PHY_ADDR),0x1F1F);
	phx_write_u32(((0x62AD* 4 ) | PHY_ADDR),0x0080);
	phx_write_u32(((0x62AF* 4 ) | PHY_ADDR),0x1C08);
	phx_write_u32(((0x62C4* 4 ) | PHY_ADDR),0x0003);
	phx_write_u32(((0x62D0* 4 ) | PHY_ADDR),0x3C3C);
	phx_write_u32(((0x62E1* 4 ) | PHY_ADDR),0x3100);
	phx_write_u32(((0x62EA* 4 ) | PHY_ADDR),0x0551);
	phx_write_u32(((0x62EB* 4 ) | PHY_ADDR),0x0022);
	phx_write_u32(((0x62EC* 4 ) | PHY_ADDR),0x0010);
	phx_write_u32(((0x62ED* 4 ) | PHY_ADDR),0x0010);
	phx_write_u32(((0x62EE* 4 ) | PHY_ADDR),0x0008);
	phx_write_u32(((0x62F5* 4 ) | PHY_ADDR),0x1561);
	phx_write_u32(((0x62F8* 4 ) | PHY_ADDR),0x1561);
	phx_write_u32(((0x62FA* 4 ) | PHY_ADDR),0x00B0);
	phx_write_u32(((0x62FB* 4 ) | PHY_ADDR),0x00A6);
	phx_write_u32(((0x62FC* 4 ) | PHY_ADDR),0x01B6);
	phx_write_u32(((0x62FF* 4 ) | PHY_ADDR),0x0336);
	phx_write_u32(((0x6300* 4 ) | PHY_ADDR),0x0788);
	phx_write_u32(((0x6301* 4 ) | PHY_ADDR),0x0777);
	phx_write_u32(((0x6302* 4 ) | PHY_ADDR),0x0BCC);
	phx_write_u32(((0x6303* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x6304* 4 ) | PHY_ADDR),0x0DFF);
	phx_write_u32(((0x6305* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x6306* 4 ) | PHY_ADDR),0x0CDD);
	phx_write_u32(((0x6307* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x6308* 4 ) | PHY_ADDR),0x0CDD);
	phx_write_u32(((0x6309* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x630A* 4 ) | PHY_ADDR),0x0CDD);
	phx_write_u32(((0x630B* 4 ) | PHY_ADDR),0x0599);
	phx_write_u32(((0x630C* 4 ) | PHY_ADDR),0x0DEE);
	phx_write_u32(((0x630D* 4 ) | PHY_ADDR),0x0084);
	phx_write_u32(((0x630E* 4 ) | PHY_ADDR),0x0399);
	phx_write_u32(((0x630F* 4 ) | PHY_ADDR),0x0DEE);
	phx_write_u32(((0x6311* 4 ) | PHY_ADDR),0x28FF);
	phx_write_u32(((0x6315* 4 ) | PHY_ADDR),0x0160);
	phx_write_u32(((0x6316* 4 ) | PHY_ADDR),0x0008);
	phx_write_u32(((0x6318* 4 ) | PHY_ADDR),0x1000);
	phx_write_u32(((0x631A* 4 ) | PHY_ADDR),0x0100);
	phx_write_u32(((0x631B* 4 ) | PHY_ADDR),0x0080);
	phx_write_u32(((0x631C* 4 ) | PHY_ADDR),0x0024);
	phx_write_u32(((0x631D* 4 ) | PHY_ADDR),0x0024);
	phx_write_u32(((0x631E* 4 ) | PHY_ADDR),0x01FF);
	phx_write_u32(((0x6321* 4 ) | PHY_ADDR),0x001F);
	phx_write_u32(((0x6322* 4 ) | PHY_ADDR),0x1CE7);
	phx_write_u32(((0x6323* 4 ) | PHY_ADDR),0x1D67);
	phx_write_u32(((0x6325* 4 ) | PHY_ADDR),0x001F);
	phx_write_u32(((0x6326* 4 ) | PHY_ADDR),0x4000);
	phx_write_u32(((0x6348* 4 ) | PHY_ADDR),0x2011);
	phx_write_u32(((0x6351* 4 ) | PHY_ADDR),0x0606);
	phx_write_u32(((0x6352* 4 ) | PHY_ADDR),0x0100);
	phx_write_u32(((0x6355* 4 ) | PHY_ADDR),0x0220);
	phx_write_u32(((0x6358* 4 ) | PHY_ADDR),0xFE21);
	phx_write_u32(((0x6359* 4 ) | PHY_ADDR),0x2121);
	phx_write_u32(((0x635C* 4 ) | PHY_ADDR),0x5882);
	phx_write_u32(((0x6361* 4 ) | PHY_ADDR),0x0023);
	phx_write_u32(((0x63B0* 4 ) | PHY_ADDR),0x559E);
	phx_write_u32(((0x63D8* 4 ) | PHY_ADDR),0x0019);
	phx_write_u32(((0x63D9* 4 ) | PHY_ADDR),0x7FFF);
	phx_write_u32(((0x63DA* 4 ) | PHY_ADDR),0x0028);
	phx_write_u32(((0x63DB* 4 ) | PHY_ADDR),0x0028);
	phx_write_u32(((0x63DC* 4 ) | PHY_ADDR),0x0005);
	phx_write_u32(((0x63DD* 4 ) | PHY_ADDR),0x000F);
	phx_write_u32(((0x63DE* 4 ) | PHY_ADDR),0x7FC3);
	phx_write_u32(((0x63DF* 4 ) | PHY_ADDR),0x7F81);
	phx_write_u32(((0x63E0* 4 ) | PHY_ADDR),0x7FC3);
	phx_write_u32(((0x63E1* 4 ) | PHY_ADDR),0x7FC3);
	phx_write_u32(((0x63E2* 4 ) | PHY_ADDR),0x7F81);
	phx_write_u32(((0x63E3* 4 ) | PHY_ADDR),0x7FF1);
	phx_write_u32(((0x63E4* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x63E5* 4 ) | PHY_ADDR),0x1000);
	phx_write_u32(((0x63E6* 4 ) | PHY_ADDR),0x5336);
	phx_write_u32(((0x63E7* 4 ) | PHY_ADDR),0x5336);
	phx_write_u32(((0x63E8* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x63F3* 4 ) | PHY_ADDR),0x392B);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0401);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0001);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0402);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0002);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0403);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0003);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0404);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0004);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0405);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0005);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0406);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0006);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0407);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0007);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0408);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0008);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0409);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0009);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x040A);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x000A);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x040B);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x000B);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x040C);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x000C);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x040D);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x000D);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x050D);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x000E);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x050E);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x000F);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x060F);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0010);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0610);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0011);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0710);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0012);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0711);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0013);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0812);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0014);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0813);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0015);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0913);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0016);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0914);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0017);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0A15);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0018);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0A16);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0019);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0B16);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x001A);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0B17);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x001B);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0C18);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x001C);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0C19);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x001D);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0D19);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x001E);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0D1A);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x001F);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0E1B);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0020);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0E1C);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0021);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0F1C);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0022);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x0F1D);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0023);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x101E);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0024);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x101F);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0025);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x111F);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0026);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1120);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0027);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1221);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0028);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1221);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0029);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1322);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x002A);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1322);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x002B);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1423);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x002C);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1423);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x002D);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1524);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x002E);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1525);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x002F);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1626);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0030);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1627);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0031);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1728);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0032);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1729);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0033);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1829);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0034);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x182A);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0035);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x192C);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0036);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x192D);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0037);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1A2E);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0038);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1A2F);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x0039);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1B30);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x003A);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1B31);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x003B);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1B32);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x003C);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1B33);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x003D);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1B34);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x003E);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1B35);
	phx_write_u32(((0x62E8* 4 ) | PHY_ADDR),0x003F);
	phx_write_u32(((0x62E9* 4 ) | PHY_ADDR),0x1B36);
	phx_write_u32(((0x6406* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x6439* 4 ) | PHY_ADDR),0x0606);
	phx_write_u32(((0x643B* 4 ) | PHY_ADDR),0x000C);
	phx_write_u32(((0x644C* 4 ) | PHY_ADDR),0x0D33);
	phx_write_u32(((0x646F* 4 ) | PHY_ADDR),0x9602);
	phx_write_u32(((0x6471* 4 ) | PHY_ADDR),0x0271);
	phx_write_u32(((0x6481* 4 ) | PHY_ADDR),0x813E);
	phx_write_u32(((0x6485* 4 ) | PHY_ADDR),0x8E82);
	phx_write_u32(((0x6486* 4 ) | PHY_ADDR),0x933A);
	phx_write_u32(((0x6487* 4 ) | PHY_ADDR),0x8F5B);
	phx_write_u32(((0x6488* 4 ) | PHY_ADDR),0x8B4B);
	phx_write_u32(((0x6491* 4 ) | PHY_ADDR),0x033C);
	phx_write_u32(((0x6496* 4 ) | PHY_ADDR),0x0003);
	phx_write_u32(((0x6497* 4 ) | PHY_ADDR),0x44CC);
	phx_write_u32(((0x649B* 4 ) | PHY_ADDR),0x088C);
	phx_write_u32(((0x649C* 4 ) | PHY_ADDR),0x8000);
	phx_write_u32(((0x64AC* 4 ) | PHY_ADDR),0x1F1F);
	phx_write_u32(((0x64AD* 4 ) | PHY_ADDR),0x0080);
	phx_write_u32(((0x64AF* 4 ) | PHY_ADDR),0x1C08);
	phx_write_u32(((0x64C4* 4 ) | PHY_ADDR),0x0003);
	phx_write_u32(((0x64D0* 4 ) | PHY_ADDR),0x3C3C);
	phx_write_u32(((0x64E1* 4 ) | PHY_ADDR),0x3100);
	phx_write_u32(((0x64EA* 4 ) | PHY_ADDR),0x0551);
	phx_write_u32(((0x64EB* 4 ) | PHY_ADDR),0x0022);
	phx_write_u32(((0x64EC* 4 ) | PHY_ADDR),0x0010);
	phx_write_u32(((0x64ED* 4 ) | PHY_ADDR),0x0010);
	phx_write_u32(((0x64EE* 4 ) | PHY_ADDR),0x0008);
	phx_write_u32(((0x64F5* 4 ) | PHY_ADDR),0x1561);
	phx_write_u32(((0x64F8* 4 ) | PHY_ADDR),0x1561);
	phx_write_u32(((0x64FA* 4 ) | PHY_ADDR),0x00B0);
	phx_write_u32(((0x64FB* 4 ) | PHY_ADDR),0x00A6);
	phx_write_u32(((0x64FC* 4 ) | PHY_ADDR),0x01B6);
	phx_write_u32(((0x64FF* 4 ) | PHY_ADDR),0x0336);
	phx_write_u32(((0x6500* 4 ) | PHY_ADDR),0x0788);
	phx_write_u32(((0x6501* 4 ) | PHY_ADDR),0x0777);
	phx_write_u32(((0x6502* 4 ) | PHY_ADDR),0x0BCC);
	phx_write_u32(((0x6503* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x6504* 4 ) | PHY_ADDR),0x0DFF);
	phx_write_u32(((0x6505* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x6506* 4 ) | PHY_ADDR),0x0CDD);
	phx_write_u32(((0x6507* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x6508* 4 ) | PHY_ADDR),0x0CDD);
	phx_write_u32(((0x6509* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x650A* 4 ) | PHY_ADDR),0x0CDD);
	phx_write_u32(((0x650B* 4 ) | PHY_ADDR),0x0599);
	phx_write_u32(((0x650C* 4 ) | PHY_ADDR),0x0DEE);
	phx_write_u32(((0x650D* 4 ) | PHY_ADDR),0x0084);
	phx_write_u32(((0x650E* 4 ) | PHY_ADDR),0x0399);
	phx_write_u32(((0x650F* 4 ) | PHY_ADDR),0x0DEE);
	phx_write_u32(((0x6511* 4 ) | PHY_ADDR),0x28FF);
	phx_write_u32(((0x6515* 4 ) | PHY_ADDR),0x0160);
	phx_write_u32(((0x6516* 4 ) | PHY_ADDR),0x0008);
	phx_write_u32(((0x6518* 4 ) | PHY_ADDR),0x1000);
	phx_write_u32(((0x651A* 4 ) | PHY_ADDR),0x0100);
	phx_write_u32(((0x651B* 4 ) | PHY_ADDR),0x0080);
	phx_write_u32(((0x651C* 4 ) | PHY_ADDR),0x0024);
	phx_write_u32(((0x651D* 4 ) | PHY_ADDR),0x0024);
	phx_write_u32(((0x651E* 4 ) | PHY_ADDR),0x01FF);
	phx_write_u32(((0x6521* 4 ) | PHY_ADDR),0x001F);
	phx_write_u32(((0x6522* 4 ) | PHY_ADDR),0x1CE7);
	phx_write_u32(((0x6523* 4 ) | PHY_ADDR),0x1D67);
	phx_write_u32(((0x6525* 4 ) | PHY_ADDR),0x001F);
	phx_write_u32(((0x6526* 4 ) | PHY_ADDR),0x4000);
	phx_write_u32(((0x6548* 4 ) | PHY_ADDR),0x2011);
	phx_write_u32(((0x6551* 4 ) | PHY_ADDR),0x0606);
	phx_write_u32(((0x6552* 4 ) | PHY_ADDR),0x0100);
	phx_write_u32(((0x6555* 4 ) | PHY_ADDR),0x0220);
	phx_write_u32(((0x6558* 4 ) | PHY_ADDR),0xFE21);
	phx_write_u32(((0x6559* 4 ) | PHY_ADDR),0x2121);
	phx_write_u32(((0x655C* 4 ) | PHY_ADDR),0x5882);
	phx_write_u32(((0x6561* 4 ) | PHY_ADDR),0x0023);
	phx_write_u32(((0x65B0* 4 ) | PHY_ADDR),0x559E);
	phx_write_u32(((0x65D8* 4 ) | PHY_ADDR),0x0019);
	phx_write_u32(((0x65D9* 4 ) | PHY_ADDR),0x7FFF);
	phx_write_u32(((0x65DA* 4 ) | PHY_ADDR),0x0028);
	phx_write_u32(((0x65DB* 4 ) | PHY_ADDR),0x0028);
	phx_write_u32(((0x65DC* 4 ) | PHY_ADDR),0x0005);
	phx_write_u32(((0x65DD* 4 ) | PHY_ADDR),0x000F);
	phx_write_u32(((0x65DE* 4 ) | PHY_ADDR),0x7FC3);
	phx_write_u32(((0x65DF* 4 ) | PHY_ADDR),0x7F81);
	phx_write_u32(((0x65E0* 4 ) | PHY_ADDR),0x7FC3);
	phx_write_u32(((0x65E1* 4 ) | PHY_ADDR),0x7FC3);
	phx_write_u32(((0x65E2* 4 ) | PHY_ADDR),0x7F81);
	phx_write_u32(((0x65E3* 4 ) | PHY_ADDR),0x7FF1);
	phx_write_u32(((0x65E4* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x65E5* 4 ) | PHY_ADDR),0x1000);
	phx_write_u32(((0x65E6* 4 ) | PHY_ADDR),0x5336);
	phx_write_u32(((0x65E7* 4 ) | PHY_ADDR),0x5336);
	phx_write_u32(((0x65E8* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x65F3* 4 ) | PHY_ADDR),0x392B);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0401);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0001);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0402);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0002);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0403);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0003);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0404);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0004);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0405);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0005);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0406);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0006);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0407);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0007);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0408);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0008);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0409);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0009);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x040A);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x000A);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x040B);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x000B);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x040C);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x000C);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x040D);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x000D);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x050D);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x000E);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x050E);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x000F);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x060F);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0010);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0610);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0011);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0710);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0012);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0711);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0013);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0812);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0014);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0813);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0015);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0913);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0016);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0914);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0017);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0A15);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0018);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0A16);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0019);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0B16);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x001A);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0B17);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x001B);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0C18);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x001C);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0C19);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x001D);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0D19);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x001E);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0D1A);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x001F);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0E1B);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0020);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0E1C);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0021);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0F1C);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0022);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x0F1D);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0023);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x101E);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0024);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x101F);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0025);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x111F);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0026);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1120);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0027);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1221);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0028);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1221);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0029);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1322);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x002A);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1322);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x002B);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1423);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x002C);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1423);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x002D);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1524);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x002E);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1525);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x002F);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1626);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0030);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1627);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0031);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1728);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0032);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1729);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0033);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1829);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0034);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x182A);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0035);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x192C);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0036);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x192D);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0037);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1A2E);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0038);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1A2F);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x0039);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1B30);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x003A);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1B31);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x003B);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1B32);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x003C);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1B33);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x003D);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1B34);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x003E);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1B35);
	phx_write_u32(((0x64E8* 4 ) | PHY_ADDR),0x003F);
	phx_write_u32(((0x64E9* 4 ) | PHY_ADDR),0x1B36);
	phx_write_u32(((0x6606* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x6639* 4 ) | PHY_ADDR),0x0606);
	phx_write_u32(((0x663B* 4 ) | PHY_ADDR),0x000C);
	phx_write_u32(((0x664C* 4 ) | PHY_ADDR),0x0D33);
	phx_write_u32(((0x666F* 4 ) | PHY_ADDR),0x9602);
	phx_write_u32(((0x6671* 4 ) | PHY_ADDR),0x0271);
	phx_write_u32(((0x6681* 4 ) | PHY_ADDR),0x813E);
	phx_write_u32(((0x6685* 4 ) | PHY_ADDR),0x8E82);
	phx_write_u32(((0x6686* 4 ) | PHY_ADDR),0x933A);
	phx_write_u32(((0x6687* 4 ) | PHY_ADDR),0x8F5B);
	phx_write_u32(((0x6688* 4 ) | PHY_ADDR),0x8B4B);
	phx_write_u32(((0x6691* 4 ) | PHY_ADDR),0x033C);
	phx_write_u32(((0x6696* 4 ) | PHY_ADDR),0x0003);
	phx_write_u32(((0x6697* 4 ) | PHY_ADDR),0x44CC);
	phx_write_u32(((0x669B* 4 ) | PHY_ADDR),0x088C);
	phx_write_u32(((0x669C* 4 ) | PHY_ADDR),0x8000);
	phx_write_u32(((0x66AC* 4 ) | PHY_ADDR),0x1F1F);
	phx_write_u32(((0x66AD* 4 ) | PHY_ADDR),0x0080);
	phx_write_u32(((0x66AF* 4 ) | PHY_ADDR),0x1C08);
	phx_write_u32(((0x66C4* 4 ) | PHY_ADDR),0x0003);
	phx_write_u32(((0x66D0* 4 ) | PHY_ADDR),0x3C3C);
	phx_write_u32(((0x66E1* 4 ) | PHY_ADDR),0x3100);
	phx_write_u32(((0x66EA* 4 ) | PHY_ADDR),0x0551);
	phx_write_u32(((0x66EB* 4 ) | PHY_ADDR),0x0022);
	phx_write_u32(((0x66EC* 4 ) | PHY_ADDR),0x0010);
	phx_write_u32(((0x66ED* 4 ) | PHY_ADDR),0x0010);
	phx_write_u32(((0x66EE* 4 ) | PHY_ADDR),0x0008);
	phx_write_u32(((0x66F5* 4 ) | PHY_ADDR),0x1561);
	phx_write_u32(((0x66F8* 4 ) | PHY_ADDR),0x1561);
	phx_write_u32(((0x66FA* 4 ) | PHY_ADDR),0x00B0);
	phx_write_u32(((0x66FB* 4 ) | PHY_ADDR),0x00A6);
	phx_write_u32(((0x66FC* 4 ) | PHY_ADDR),0x01B6);
	phx_write_u32(((0x66FF* 4 ) | PHY_ADDR),0x0336);
	phx_write_u32(((0x6700* 4 ) | PHY_ADDR),0x0788);
	phx_write_u32(((0x6701* 4 ) | PHY_ADDR),0x0777);
	phx_write_u32(((0x6702* 4 ) | PHY_ADDR),0x0BCC);
	phx_write_u32(((0x6703* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x6704* 4 ) | PHY_ADDR),0x0DFF);
	phx_write_u32(((0x6705* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x6706* 4 ) | PHY_ADDR),0x0CDD);
	phx_write_u32(((0x6707* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x6708* 4 ) | PHY_ADDR),0x0CDD);
	phx_write_u32(((0x6709* 4 ) | PHY_ADDR),0x0888);
	phx_write_u32(((0x670A* 4 ) | PHY_ADDR),0x0CDD);
	phx_write_u32(((0x670B* 4 ) | PHY_ADDR),0x0599);
	phx_write_u32(((0x670C* 4 ) | PHY_ADDR),0x0DEE);
	phx_write_u32(((0x670D* 4 ) | PHY_ADDR),0x0084);
	phx_write_u32(((0x670E* 4 ) | PHY_ADDR),0x0399);
	phx_write_u32(((0x670F* 4 ) | PHY_ADDR),0x0DEE);
	phx_write_u32(((0x6711* 4 ) | PHY_ADDR),0x28FF);
	phx_write_u32(((0x6715* 4 ) | PHY_ADDR),0x0160);
	phx_write_u32(((0x6716* 4 ) | PHY_ADDR),0x0008);
	phx_write_u32(((0x6718* 4 ) | PHY_ADDR),0x1000);
	phx_write_u32(((0x671A* 4 ) | PHY_ADDR),0x0100);
	phx_write_u32(((0x671B* 4 ) | PHY_ADDR),0x0080);
	phx_write_u32(((0x671C* 4 ) | PHY_ADDR),0x0024);
	phx_write_u32(((0x671D* 4 ) | PHY_ADDR),0x0024);
	phx_write_u32(((0x671E* 4 ) | PHY_ADDR),0x01FF);
	phx_write_u32(((0x6721* 4 ) | PHY_ADDR),0x001F);
	phx_write_u32(((0x6722* 4 ) | PHY_ADDR),0x1CE7);
	phx_write_u32(((0x6723* 4 ) | PHY_ADDR),0x1D67);
	phx_write_u32(((0x6725* 4 ) | PHY_ADDR),0x001F);
	phx_write_u32(((0x6726* 4 ) | PHY_ADDR),0x4000);
	phx_write_u32(((0x6748* 4 ) | PHY_ADDR),0x2011);
	phx_write_u32(((0x6751* 4 ) | PHY_ADDR),0x0606);
	phx_write_u32(((0x6752* 4 ) | PHY_ADDR),0x0100);
	phx_write_u32(((0x6755* 4 ) | PHY_ADDR),0x0220);
	phx_write_u32(((0x6758* 4 ) | PHY_ADDR),0xFE21);
	phx_write_u32(((0x6759* 4 ) | PHY_ADDR),0x2121);
	phx_write_u32(((0x675C* 4 ) | PHY_ADDR),0x5882);
	phx_write_u32(((0x6761* 4 ) | PHY_ADDR),0x0023);
	phx_write_u32(((0x67B0* 4 ) | PHY_ADDR),0x559E);
	phx_write_u32(((0x67D8* 4 ) | PHY_ADDR),0x0019);
	phx_write_u32(((0x67D9* 4 ) | PHY_ADDR),0x7FFF);
	phx_write_u32(((0x67DA* 4 ) | PHY_ADDR),0x0028);
	phx_write_u32(((0x67DB* 4 ) | PHY_ADDR),0x0028);
	phx_write_u32(((0x67DC* 4 ) | PHY_ADDR),0x0005);
	phx_write_u32(((0x67DD* 4 ) | PHY_ADDR),0x000F);
	phx_write_u32(((0x67DE* 4 ) | PHY_ADDR),0x7FC3);
	phx_write_u32(((0x67DF* 4 ) | PHY_ADDR),0x7F81);
	phx_write_u32(((0x67E0* 4 ) | PHY_ADDR),0x7FC3);
	phx_write_u32(((0x67E1* 4 ) | PHY_ADDR),0x7FC3);
	phx_write_u32(((0x67E2* 4 ) | PHY_ADDR),0x7F81);
	phx_write_u32(((0x67E3* 4 ) | PHY_ADDR),0x7FF1);
	phx_write_u32(((0x67E4* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x67E5* 4 ) | PHY_ADDR),0x1000);
	phx_write_u32(((0x67E6* 4 ) | PHY_ADDR),0x5336);
	phx_write_u32(((0x67E7* 4 ) | PHY_ADDR),0x5336);
	phx_write_u32(((0x67E8* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x67F3* 4 ) | PHY_ADDR),0x392B);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0000);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0401);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0001);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0402);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0002);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0403);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0003);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0404);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0004);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0405);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0005);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0406);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0006);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0407);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0007);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0408);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0008);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0409);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0009);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x040A);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x000A);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x040B);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x000B);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x040C);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x000C);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x040D);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x000D);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x050D);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x000E);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x050E);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x000F);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x060F);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0010);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0610);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0011);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0710);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0012);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0711);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0013);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0812);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0014);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0813);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0015);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0913);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0016);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0914);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0017);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0A15);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0018);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0A16);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0019);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0B16);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x001A);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0B17);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x001B);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0C18);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x001C);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0C19);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x001D);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0D19);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x001E);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0D1A);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x001F);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0E1B);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0020);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0E1C);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0021);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0F1C);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0022);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x0F1D);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0023);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x101E);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0024);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x101F);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0025);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x111F);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0026);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1120);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0027);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1221);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0028);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1221);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0029);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1322);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x002A);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1322);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x002B);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1423);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x002C);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1423);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x002D);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1524);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x002E);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1525);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x002F);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1626);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0030);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1627);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0031);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1728);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0032);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1729);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0033);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1829);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0034);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x182A);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0035);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x192C);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0036);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x192D);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0037);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1A2E);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0038);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1A2F);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x0039);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1B30);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x003A);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1B31);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x003B);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1B32);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x003C);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1B33);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x003D);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1B34);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x003E);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1B35);
	phx_write_u32(((0x66E8* 4 ) | PHY_ADDR),0x003F);
	phx_write_u32(((0x66E9* 4 ) | PHY_ADDR),0x1B36);



}






#if 0/*ldf 20230914 ignore:: 以下代码是从裸测移植过来的，暂时没用途*/
void pcie_rc_enum_sequence_loop(int sel,int type)
{
     u64 cdm_baseaddr;
     u64 rcbarcfg_baseaddr;
     u64 axiaddr = 0x0;
     int rdata;
     int wdata;
     cdm_baseaddr = get_pcie_ip_link_base_addr(sel);
     rcbarcfg_baseaddr  = (get_pcie_ip_link_base_addr(sel) | 0x100000 | 0x300);
//lmz     rdata = phx_read_u32(cdm_baseaddr + 0x0 );
//lmz     rdata = phx_read_u32(cdm_baseaddr + 0x4 );
//lmz     rdata = phx_read_u32(cdm_baseaddr + 0x10);
//lmz     rdata = phx_read_u32(cdm_baseaddr + 0x14);
  
  rdata = phx_read_u32(rcbarcfg_baseaddr);
  wdata = (rdata & 0xffffffc0 | 30);
     phx_write_u32(rcbarcfg_baseaddr,wdata);
  
     phx_write_u32((cdm_baseaddr + 0x4) ,0x100006);
     phx_write_u32((cdm_baseaddr + 0x14),0x2);

     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000 + 0x10),0x00000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000 + 0x14),0x00000002);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000 + 0x18),0x00000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000 + 0x1c),0x00000003);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000 + 0x20),0x00000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000 + 0x24),0x00000404);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000 + 0x04),0x00100007);  
     
}


/*PCIe RC（Root Complex）的枚举序列*/
void pcie_rc_enum_sequence(int sel,int type)
{
    u32 val;
     u64 cdm_baseaddr;
     u64 rcbarcfg_baseaddr;
     u64 axiaddr = 0x0;
     int rdata;
     int wdata;
     cdm_baseaddr = get_pcie_ip_link_base_addr(sel);
     rcbarcfg_baseaddr  = (get_pcie_ip_link_base_addr(sel) | 0x100000 | 0x300);
//lmz     rdata = phx_read_u32(cdm_baseaddr + 0x0 );
//lmz     rdata = phx_read_u32(cdm_baseaddr + 0x4 );
//lmz     rdata = phx_read_u32(cdm_baseaddr + 0x10);
//lmz     rdata = phx_read_u32(cdm_baseaddr + 0x14);
  
	  rdata = phx_read_u32(rcbarcfg_baseaddr);
	  wdata = (rdata & 0xffffffc0 | 32);
     phx_write_u32(rcbarcfg_baseaddr,wdata);
  
     phx_write_u32((cdm_baseaddr + 0x4) ,0x100006);
     phx_write_u32((cdm_baseaddr + 0x14),0xe);
     
   //  if(((sel && 0x0f0) != 0) && ((is_rc && 0x02) == 0))
   //  {
   //       cdm_baseaddr = get_pcie_ip_link_base_addr(sel);
   //       rdata = phx_read_u32(cdm_baseaddr + 0x0 );
   //       rdata = phx_read_u32(cdm_baseaddr + 0x4 );
   //       rdata = phx_read_u32(cdm_baseaddr + 0x10);
   //       rdata = phx_read_u32(cdm_baseaddr + 0x14);
   //       phx_write_u32(cdm_baseaddr + 0x4 ,0x100006);
   //       phx_write_u32(cdm_baseaddr + 0x14,0xe);
   //  }
      
     val = pcie_axi_read_ad(sel,type,axiaddr + 0x0 );
     printk("===== read val[0x0] = 0x%x\n",val);
     val = pcie_axi_read_ad(sel,type,axiaddr + 0x4 );
     printk("===== read val[0x4] = 0x%x\n",val);
     val = pcie_axi_read_ad(sel,type,axiaddr + 0x10);
     printk("===== read val[0x10] = 0x%x\n",val);
     val = pcie_axi_read_ad(sel,type,axiaddr + 0x14);
     printk("===== read val[0x14] = 0x%x\n",val);
     val = pcie_axi_read_ad(sel,type,axiaddr + 0x18);
     printk("===== read val[0x18] = 0x%x\n",val);
     val = pcie_axi_read_ad(sel,type,axiaddr + 0x1c);
     printk("===== read val[0x1c] = 0x%x\n",val);
     val = pcie_axi_read_ad(sel,type,axiaddr + 0x20);
     printk("===== read val[0x20] = 0x%x\n",val);
     val = pcie_axi_read_ad(sel,type,axiaddr + 0x24);
     printk("===== read val[0x24] = 0x%x\n",val);

//     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000 + 0x10),0x10000000);  
//     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000 + 0x14),0x20000000);  
//     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000 + 0x18),0x00000000);  
//     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000 + 0x1c),0x0000000F);  
//     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000 + 0x20),0x00000000);  
//     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000 + 0x24),0x00000400); 
#if 1
     printk("%s:__%d__\n",__FUNCTION__,__LINE__);
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000 + 0x10),0x30a00000);  
     printk("%s:__%d__\n",__FUNCTION__,__LINE__);
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000 + 0x14),0xffffffff); 
     printk("%s:__%d__\n",__FUNCTION__,__LINE__);
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000 + 0x18),0xffffffff);
     printk("%s:__%d__\n",__FUNCTION__,__LINE__);
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000 + 0x1c),0x30b00000);  
     printk("%s:__%d__\n",__FUNCTION__,__LINE__);
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000 + 0x20),0xffffffff);  
     printk("%s:__%d__\n",__FUNCTION__,__LINE__);
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000 + 0x24),0xffffffff); 
     printk("%s:__%d__\n",__FUNCTION__,__LINE__);
     

     val = pcie_axi_read_ad(sel,type,(axiaddr + 0x100000 + 0x10));  
     printk("===== val[0x10] = 0x%x\n",val);
     val = pcie_axi_read_ad(sel,type,(axiaddr + 0x100000 + 0x14));  
     printk("===== val[0x14] = 0x%x\n",val);
     val = pcie_axi_read_ad(sel,type,(axiaddr + 0x100000 + 0x18));
     printk("===== val[0x18] = 0x%x\n",val);
     val = pcie_axi_read_ad(sel,type,(axiaddr + 0x100000 + 0x1c));
     printk("===== val[0x1c] = 0x%x\n",val);
     val = pcie_axi_read_ad(sel,type,(axiaddr + 0x100000 + 0x20)); 
     printk("===== val[0x20] = 0x%x\n",val);
     val = pcie_axi_read_ad(sel,type,(axiaddr + 0x100000 + 0x24));
     printk("===== val[0x24] = 0x%x\n",val);
#endif
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000 + 0x04),0x00100007);  
     
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x1000 + 0x10),0x30000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x1000 + 0x14),0x40000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x1000 + 0x18),0x00000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x1000 + 0x1c),0x0000001F);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x1000 + 0x20),0x00001000);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x1000 + 0x24),0x00001400);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x1000 + 0x4 ),0x00100007); 

     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x2000 + 0x10),0x50000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x2000 + 0x14),0x60000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x2000 + 0x18),0x00000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x2000 + 0x1c),0x0000002F);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x2000 + 0x20),0x00002000);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x2000 + 0x24),0x00002400);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x2000 + 0x4 ),0x00100007); 

     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x3000 + 0x10),0x70000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x3000 + 0x14),0x80000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x3000 + 0x18),0x00000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x3000 + 0x1c),0x0000003F);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x3000 + 0x20),0x00003000);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x3000 + 0x24),0x00003400);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x3000 + 0x4 ),0x00100007); 

}

//fudy.for ddr
void pcie_rc_enum_sequence_ddr(int sel,int type)
{
     u64 cdm_baseaddr;
     u64 rcbarcfg_baseaddr;
     u64 axiaddr = 0x0;
     int rdata;
     int wdata;
     cdm_baseaddr = get_pcie_ip_link_base_addr(sel);
     rcbarcfg_baseaddr  = (get_pcie_ip_link_base_addr(sel) | 0x100000 | 0x300);
//lmz     rdata = phx_read_u32(cdm_baseaddr + 0x0 );
//lmz     rdata = phx_read_u32(cdm_baseaddr + 0x4 );
//lmz     rdata = phx_read_u32(cdm_baseaddr + 0x10);
//lmz     rdata = phx_read_u32(cdm_baseaddr + 0x14);
  
  rdata = phx_read_u32(rcbarcfg_baseaddr);
  wdata = (rdata & 0xffffffc0 | 32);
     phx_write_u32(rcbarcfg_baseaddr,wdata);
  
     phx_write_u32((cdm_baseaddr + 0x4) ,0x100006);
     phx_write_u32((cdm_baseaddr + 0x14),0xe);
     
   //  if(((sel && 0x0f0) != 0) && ((is_rc && 0x02) == 0))
   //  {
   //       cdm_baseaddr = get_pcie_ip_link_base_addr(sel);
   //       rdata = phx_read_u32(cdm_baseaddr + 0x0 );
   //       rdata = phx_read_u32(cdm_baseaddr + 0x4 );
   //       rdata = phx_read_u32(cdm_baseaddr + 0x10);
   //       rdata = phx_read_u32(cdm_baseaddr + 0x14);
   //       phx_write_u32(cdm_baseaddr + 0x4 ,0x100006);
   //       phx_write_u32(cdm_baseaddr + 0x14,0xe);
   //  }
      
     pcie_axi_read_ad(sel,type,axiaddr + 0x0 );
     pcie_axi_read_ad(sel,type,axiaddr + 0x4 );
     pcie_axi_read_ad(sel,type,axiaddr + 0x10);
     pcie_axi_read_ad(sel,type,axiaddr + 0x14);
     pcie_axi_read_ad(sel,type,axiaddr + 0x18);
     pcie_axi_read_ad(sel,type,axiaddr + 0x1c);
     pcie_axi_read_ad(sel,type,axiaddr + 0x20);
     pcie_axi_read_ad(sel,type,axiaddr + 0x24);

	 //fudy.
	 u32 bus_offset = 0;//0x100000;

     pcie_axi_write_ad(sel,type,(axiaddr + bus_offset + 0x10),0x10000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + bus_offset + 0x14),0x20000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + bus_offset + 0x18),0x00000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + bus_offset + 0x1c),0x0000000F);  
     pcie_axi_write_ad(sel,type,(axiaddr + bus_offset + 0x20),0x00000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + bus_offset + 0x24),0x00000400);  
     pcie_axi_write_ad(sel,type,(axiaddr + bus_offset + 0x04),0x00100007);  
     
     pcie_axi_write_ad(sel,type,(axiaddr + bus_offset+ 0x1000 + 0x10),0x30000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + bus_offset+ 0x1000 + 0x14),0x40000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + bus_offset+ 0x1000 + 0x18),0x00000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + bus_offset+ 0x1000 + 0x1c),0x0000001F);  
     pcie_axi_write_ad(sel,type,(axiaddr + bus_offset+ 0x1000 + 0x20),0x00001000);  
     pcie_axi_write_ad(sel,type,(axiaddr + bus_offset+ 0x1000 + 0x24),0x00001400);  
     pcie_axi_write_ad(sel,type,(axiaddr + bus_offset+ 0x1000 + 0x4 ),0x00100007); 

     pcie_axi_write_ad(sel,type,(axiaddr + bus_offset+ 0x2000 + 0x10),0x50000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + bus_offset+ 0x2000 + 0x14),0x60000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + bus_offset+ 0x2000 + 0x18),0x00000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + bus_offset+ 0x2000 + 0x1c),0x0000002F);  
     pcie_axi_write_ad(sel,type,(axiaddr + bus_offset+ 0x2000 + 0x20),0x00002000);  
     pcie_axi_write_ad(sel,type,(axiaddr + bus_offset+ 0x2000 + 0x24),0x00002400);  
     pcie_axi_write_ad(sel,type,(axiaddr + bus_offset+ 0x2000 + 0x4 ),0x00100007); 

     pcie_axi_write_ad(sel,type,(axiaddr + bus_offset+ 0x3000 + 0x10),0x70000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + bus_offset+ 0x3000 + 0x14),0x80000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + bus_offset+ 0x3000 + 0x18),0x00000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + bus_offset+ 0x3000 + 0x1c),0x0000003F);  
     pcie_axi_write_ad(sel,type,(axiaddr + bus_offset+ 0x3000 + 0x20),0x00003000);  
     pcie_axi_write_ad(sel,type,(axiaddr + bus_offset+ 0x3000 + 0x24),0x00003400);  
     pcie_axi_write_ad(sel,type,(axiaddr + bus_offset+ 0x3000 + 0x4 ),0x00100007); 

}


//fudy.for sram
void pcie_rc_enum_sequence_sram(int sel,int type)
{
     u64 cdm_baseaddr;
     u64 rcbarcfg_baseaddr;
     u64 axiaddr = 0x0;
     int rdata;
     int wdata;
     cdm_baseaddr = get_pcie_ip_link_base_addr(sel);
     rcbarcfg_baseaddr  = (get_pcie_ip_link_base_addr(sel) | 0x100000 | 0x300);
  
  rdata = phx_read_u32(rcbarcfg_baseaddr);
  wdata = (rdata & 0xffffffc0 | 32);
     phx_write_u32(rcbarcfg_baseaddr,wdata);
  
     phx_write_u32((cdm_baseaddr + 0x4) ,0x100006);
     phx_write_u32((cdm_baseaddr + 0x14),0xe);
     
   //  if(((sel && 0x0f0) != 0) && ((is_rc && 0x02) == 0))
   //  {
   //       cdm_baseaddr = get_pcie_ip_link_base_addr(sel);
   //       rdata = phx_read_u32(cdm_baseaddr + 0x0 );
   //       rdata = phx_read_u32(cdm_baseaddr + 0x4 );
   //       rdata = phx_read_u32(cdm_baseaddr + 0x10);
   //       rdata = phx_read_u32(cdm_baseaddr + 0x14);
   //       phx_write_u32(cdm_baseaddr + 0x4 ,0x100006);
   //       phx_write_u32(cdm_baseaddr + 0x14,0xe);
   //  }
      
     pcie_axi_read_ad(sel,type,axiaddr + 0x0 );
     pcie_axi_read_ad(sel,type,axiaddr + 0x4 );
     pcie_axi_read_ad(sel,type,axiaddr + 0x10);
     pcie_axi_read_ad(sel,type,axiaddr + 0x14);
     pcie_axi_read_ad(sel,type,axiaddr + 0x18);
     pcie_axi_read_ad(sel,type,axiaddr + 0x1c);
     pcie_axi_read_ad(sel,type,axiaddr + 0x20);
     pcie_axi_read_ad(sel,type,axiaddr + 0x24);

	//fudy
#if 1	
     u64 pcie_axi_baseaddr = get_pcie_axi_baseaddr(sel, type);
	uart_printf("pcie_rc_enum_sequence_sram:sel=%x,type=%x,cdm_baseaddr=%x,rcbarcfg_baseaddr=%x,ep_reg10=%llx\n\r",
		sel,type,cdm_baseaddr,rcbarcfg_baseaddr,pcie_axi_baseaddr+0x100000+0x10);

		pcie_axi_write_ad(sel,type,(axiaddr + 0x10),0x10600000);//0x10000000); //sram addr 
		pcie_axi_write_ad(sel,type,(axiaddr + 0x14),0x20000000);  
		pcie_axi_write_ad(sel,type,(axiaddr + 0x18),0x00000000);  
		pcie_axi_write_ad(sel,type,(axiaddr + 0x1c),0x0000000F);  
		pcie_axi_write_ad(sel,type,(axiaddr + 0x20),0x00000000);  
		pcie_axi_write_ad(sel,type,(axiaddr + 0x24),0x00000400);  
		pcie_axi_write_ad(sel,type,(axiaddr + 0x04),0x00100007); 

		return;

#endif

     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000 + 0x10),0x10600000);//0x10000000); //sram addr 
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000 + 0x14),0x20000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000 + 0x18),0x00000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000 + 0x1c),0x0000000F);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000 + 0x20),0x00000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000 + 0x24),0x00000400);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000 + 0x04),0x00100007);  
     
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x1000 + 0x10),0x30000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x1000 + 0x14),0x40000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x1000 + 0x18),0x00000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x1000 + 0x1c),0x0000001F);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x1000 + 0x20),0x00001000);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x1000 + 0x24),0x00001400);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x1000 + 0x4 ),0x00100007); 

     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x2000 + 0x10),0x50000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x2000 + 0x14),0x60000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x2000 + 0x18),0x00000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x2000 + 0x1c),0x0000002F);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x2000 + 0x20),0x00002000);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x2000 + 0x24),0x00002400);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x2000 + 0x4 ),0x00100007); 

     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x3000 + 0x10),0x70000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x3000 + 0x14),0x80000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x3000 + 0x18),0x00000000);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x3000 + 0x1c),0x0000003F);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x3000 + 0x20),0x00003000);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x3000 + 0x24),0x00003400);  
     pcie_axi_write_ad(sel,type,(axiaddr + 0x100000+ 0x3000 + 0x4 ),0x00100007); 

}


void pcie_oatu_cfg_sequence(int sel,u64 axiaddr,u64 pcieaddr,int size,int rg_num,\
		int is_mem,int is_msgVd,int is_msgNm,int is_io,int is_cfg0,int is_cfg1,int rid)
{
   int atu_baseaddr;
   int rdata;
   //int rg_num;
   //int size; 
   //int is_mem;
   //int is_msgVd;
   //int is_msgNm;
   //int is_io;
   //int is_cfg0;
   //int is_cfg1;   
   //int rid;
   int type_value;


   //srand((unsigned)time(NULL));
   //rg_num = rand()%32;
   //is_mem = rand()%32767;
   //is_msgVd = rand()%32767;
   //is_msgNm = rand()%32767;
   //is_io = rand()%32767;
   //is_cfg0 = rand()%32767;
   //is_cfg1 = rand()%32767;
   //size = rand()%57+7;
   rid = 0;

   if((is_mem + is_io + is_cfg0 + is_cfg1 + is_msgVd + is_msgNm) == 0x1)
   {
        if(is_mem == 1)   {type_value = 0b0010;}   
        if(is_msgVd == 1) {type_value = 0b1101;}   
        if(is_msgNm == 1) {type_value = 0b1100;}   
        if(is_io == 1)    {type_value = 0b0110;}   
        if(is_cfg0 == 1)  {type_value = 0b1010;}   
        if(is_cfg1 == 1)  {type_value = 0b1011;}   
   
   }

   atu_baseaddr = get_pcie_ip_link_base_addr(sel) + (rg_num*0x20) + (0x1<<22);

   //fudy.
   //uart_printf("axiaddr=%x,pcieaddr=%x,size=%x,rg_num=%x,get_pcie_ip_link_base_addr(sel)=%x,atu_baseaddr=%x\n\r",axiaddr,pcieaddr,size,rg_num,get_pcie_ip_link_base_addr(sel),atu_baseaddr);

//lmz   rdata = phx_read_u32(atu_baseaddr+0x0 );
//lmz   rdata = phx_read_u32(atu_baseaddr+0x4 );
//lmz   rdata = phx_read_u32(atu_baseaddr+0x8 );
//lmz   rdata = phx_read_u32(atu_baseaddr+0xc );
//lmz   rdata = phx_read_u32(atu_baseaddr+0x10);
//lmz   rdata = phx_read_u32(atu_baseaddr+0x18);
//lmz   rdata = phx_read_u32(atu_baseaddr+0x1c);

   phx_write_u32((atu_baseaddr+0x0) ,((pcieaddr & 0xffffffff)+size));
   phx_write_u32((atu_baseaddr+0x4) , (pcieaddr >> 32));
   phx_write_u32((atu_baseaddr+0x8) ,(type_value + (rid<<23)));
   phx_write_u32((atu_baseaddr+0xc) ,0x0);
   phx_write_u32((atu_baseaddr+0x10),0x0);
   phx_write_u32((atu_baseaddr+0x18),((axiaddr & 0xffffffff)+size));
   phx_write_u32((atu_baseaddr+0x1c),(axiaddr >> 32));

//lmz   rdata = phx_read_u32(atu_baseaddr+0x0);  //pcie base0
//lmz   rdata = phx_read_u32(atu_baseaddr+0x4);  //pcie base1
//lmz   rdata = phx_read_u32(atu_baseaddr+0x8);  //dec0
//lmz   rdata = phx_read_u32(atu_baseaddr+0xc);  //dec1
//lmz   rdata = phx_read_u32(atu_baseaddr+0x10); //dec2
//lmz   rdata = phx_read_u32(atu_baseaddr+0x18); //axi base0
//lmz   rdata = phx_read_u32(atu_baseaddr+0x1c); //axi base1

#if 1
	//fudy.region x cfg:
	uart_printf("region %x cfg show:\n\r",rg_num);
	uart_printf("PCIE=%x_%x \n\r", phx_read_u32((atu_baseaddr+4)),phx_read_u32((atu_baseaddr)));
	uart_printf("DESC=%x_%x_%x \n\r", phx_read_u32((atu_baseaddr+0x10)),phx_read_u32((atu_baseaddr+0xc)),phx_read_u32((atu_baseaddr+8)));
	uart_printf("AXI =%x_%x \n\r", phx_read_u32((atu_baseaddr+0x1c)),phx_read_u32((atu_baseaddr+0x18)));
	uart_printf("\n\r");
#endif	
}	


void pcie_iatu_cfg_sequence(int sel,u64 axiaddr,int is_rc,int bar_num,int func_num)
{
    //int bar_num;
    //int func_num;
    int num_bits;
    int atu_baseaddr;
    int rdata;
    int flag = 0;

    //srand((unsigned)time(NULL));
    //bar_num = rand()%6;
    //func_num = rand()%2;

    if(((sel == PCIE_X16_LINK0_EN   ) && (is_rc & 0x1) ) != 0) {flag = 1;}
    if(((sel == PCIE_X16_LINK1_EN   ) && (is_rc & 0x2) ) != 0) {flag = 1;}
    if(((sel == PCIE_X16_LINK2_EN   ) && (is_rc & 0x4) ) != 0) {flag = 1;}
    if(((sel == PCIE_X16_LINK3_EN   ) && (is_rc & 0x8) ) != 0) {flag = 1;}
    if(((sel == PCIE_SRIO_0_LINK0_EN) && (is_rc & 0x1) ) != 0) {flag = 1;}
    if(((sel == PCIE_SRIO_0_LINK1_EN) && (is_rc & 0x2) ) != 0) {flag = 1;}
    if(((sel == PCIE_SRIO_1_LINK0_EN) && (is_rc & 0x1) ) != 0) {flag = 1;}
    if(((sel == PCIE_SRIO_1_LINK1_EN) && (is_rc & 0x2) ) != 0) {flag = 1;}
    if(((sel == PCIE_HSIO_LINK0_EN  ) && (is_rc & 0x1) ) != 0) {flag = 1;}
    if(((sel == PCIE_HSIO_LINK1_EN  ) && (is_rc & 0x2) ) != 0) {flag = 1;}
    if(flag==1)
    {
        //ep mode
       atu_baseaddr = get_pcie_ip_link_base_addr(sel) + (0x1<<22)+ 0x840 + (bar_num*0x8) + (func_num*0x40);      
       num_bits = 0x0;    
    }
    else
    {
        //rc mode
       atu_baseaddr = get_pcie_ip_link_base_addr(sel) + (0x1<<22)+ 0x800 + (bar_num*0x8);
       num_bits = 31;    
    }

	//fudy.
	uart_printf("pcie_iatu_cfg_sequence: axiaddr=%x,is_rc=%x,flag=%x,atu_baseaddr=%x.\n\r",
		axiaddr,is_rc,flag,atu_baseaddr);	


//lmz   rdata = phx_read_u32(atu_baseaddr+0x0); // low addr 
//lmz   rdata = phx_read_u32(atu_baseaddr+0x4); // hi  addr 

   phx_write_u32((atu_baseaddr+0x0) ,((axiaddr & 0xffffffff) |num_bits));
   phx_write_u32((atu_baseaddr+0x4) ,(axiaddr >> 32));

//lmz   rdata = phx_read_u32(atu_baseaddr+0x0); // low addr 
//lmz   rdata = phx_read_u32(atu_baseaddr+0x4); // hi  addr 

}

//fudy.for boot from pcie, rc ib set
/*
sel == PCIE_SRIO_0_LINK0_EN,PCIE_SRIO_0_LINK1_EN ...
bar_num=0,1,7
*/
void pcie_rc_ib_cfg(int sel,u64 axiaddr,int bar_num)
{
    int num_bits;
    int atu_baseaddr;
    int flag = 0;

	//rc mode
	if(bar_num>1)//bar_num=7
		atu_baseaddr = get_pcie_ip_link_base_addr(sel) + (1<<22)+ 0x810;
	else
    	atu_baseaddr = get_pcie_ip_link_base_addr(sel) + (1<<22)+ 0x800 + (bar_num*0x8);
    num_bits = 20-1;//29;//23,31	 

	//tmp
	uart_printf("pcie_ib_cfg: axiaddr=%x,atu_baseaddr=%x.\n\r",axiaddr,atu_baseaddr);	

    phx_write_u32((atu_baseaddr+0x0) ,((axiaddr & 0xffffffff) |num_bits));
    phx_write_u32((atu_baseaddr+0x4) ,(axiaddr >> 32));

}

/*枚举*/
void Enum(int sel)
{
    u64 enum_addr;
    uart_printf("Enum begin\n\r");
    enum_addr = get_pcie_axi_baseaddr(sel,AD_128M);
	uart_printf("enum_addr=%x\n\r",enum_addr);
    pcie_oatu_cfg_sequence(sel,enum_addr,0x0,20,1,0,0,0,0,1,0,1);
    pcie_rc_enum_sequence(sel,AD_128M);
    uart_printf("Enum end\n\r");
}

//fudy.for ddr
void Enum_ddr(int sel)
{
    u64 enum_addr;
    uart_printf("Enum begin\n\r");
    enum_addr = get_pcie_axi_baseaddr(sel,AD_128M);
	uart_printf("enum_addr=%x\n\r",enum_addr);
    pcie_oatu_cfg_sequence(sel,enum_addr,0x0,20,1,0,0,0,0,1,0,1);
    pcie_rc_enum_sequence_ddr(sel,AD_128M);//fudy.
    uart_printf("Enum end\n\r");

}


//fudy.for sram
void Enum_sram(int sel)
{
    u64 enum_addr;
    //uart_printf("Enum begin\n\r");
    enum_addr = get_pcie_axi_baseaddr(sel,AD_128M);
	//uart_printf("enum_addr=%x\n\r",enum_addr);
    pcie_oatu_cfg_sequence(sel,enum_addr,0x0,11,0,0,0,0,0,1,0,1);//fudy.size=4k,region=0
    //pcie_rc_enum_sequence_sram(sel,AD_128M);
    //uart_printf("Enum end\n\r");

}


void Enum_LOOP(int sel)
{
    u64 enum_addr;
    uart_printf("Enum begin\n\r");
    enum_addr = get_pcie_axi_baseaddr(sel,AD_128M);
    pcie_oatu_cfg_sequence(sel,enum_addr,0x0,20,1,0,0,0,0,1,0,1);
    pcie_rc_enum_sequence_loop(sel,AD_128M);
    uart_printf("Enum end\n\r");

}


//dma int en
void dma_inten(int sel)
{
     int dma_baseaddr;
     dma_baseaddr = get_pcie_ip_link_base_addr(sel) + (0x1<<22) + (0x1<<21);

        phx_write_u32((dma_baseaddr+0xa4), 0x08);

        phx_write_u32((dma_baseaddr+0xa8), 0xf7);

}
//link int en
void link_inten(int sel, u32 val)
{
   int addr;

   addr = (get_pcie_misc_link_base_addr(sel) + PCIE_X16_INTR_ENABLE);

        phx_write_u32(addr,val);

}

//wait L0
void wait_L0(int sel)
{
   u64 cmd_addr;
   int flag;
   int rdata;
   cmd_addr = get_pcie_ip_link_base_addr(sel);
   flag = 1;
   while (flag)
   {
   rdata = phx_read_u32((cmd_addr + 0xd0));
 //uart_printf("Rdata:0x%x!\n\r",rdata);
 //uart_printf("mask:0x%x!\n\r",(rdata & (3<<16)));
   //if (((rdata & (3<<16)) ==(3<<16))&&(rdata != 0xffffffff)){
    if ((rdata & (0xf<<16))==(3<<16)){
        flag = 0;
        uart_printf("flag=0");
    };
   delay_cnt(10);
   }
 //uart_printf("Wait L0 Done!\n\r");
}


void pcie_dma_cfg_sequence(int sel,int ch_num,int in1out0,u64 dspad)
{
    int dma_baseaddr;
    dma_baseaddr = get_pcie_ip_link_base_addr(sel) + (ch_num*0x14) + (0x1<<22) + (0x1<<21);
	//mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "dma_baseaddr=%x,ch_num=%x,in1out0=%x,dspad=%x.",dma_baseaddr,ch_num,in1out0,dspad);//fudy.tmp

    phx_write_u32((dma_baseaddr+0x4), dspad & 0xffffffff);
    phx_write_u32((dma_baseaddr+0x8), (dspad>>32));
    phx_write_u32((dma_baseaddr+0xc), 0x0);
    phx_write_u32((dma_baseaddr+0x10), 0x0);
    phx_write_u32((dma_baseaddr+0x0), ( (in1out0<<1) | 0x1));/*xxx ldf 20230531:: 猜测这里就是开始传输*/

}

/*
 * pcie dma interrupt
 * fudy
 */
void pcie_dma_int(int sel,int ch_num)
{
	 int dma_baseaddr;
	 u32 int_sta,int_en,int_dis;
	 u32 done_bit,err_bit;
	 
	 dma_baseaddr = get_pcie_ip_link_base_addr(sel) + (ch_num*0x14) + (0x1<<22) + (0x1<<21);

	int_sta = phx_read_u32((dma_baseaddr+0xa0));
	int_en  = phx_read_u32((dma_baseaddr+0xa4));
	int_dis = phx_read_u32((dma_baseaddr+0xa8));

	done_bit = ch_num;
	err_bit  = ch_num + 8;

	uart_printf("dma_int:ch_num=%d,sta_d=%x,sta_e=%x,en_d=%x,en_e=%x,dis_d=%x,dis_e=%x\n\r",
		ch_num,	(int_sta>>done_bit)&0x1,(int_sta>>err_bit)&0x1,
				(int_en>>done_bit)&0x1,(int_en>>err_bit)&0x1,
				(int_dis>>done_bit)&0x1,(int_dis>>err_bit)&0x1 );
}

 
void pcie_srio_config_x4(int sel,u64 base_addr,int dut_is_rc)//0:ep,1:rc;
{
        int rdata;
        phx_write_u32((base_addr +0x14),0x0);
        phx_write_u32((base_addr +0x20),0x0);
        phx_write_u32((base_addr +0x2c),0x0);
        phx_write_u32((base_addr +0x38),0x0);
        
        phx_write_u32((base_addr + 0x0),0x11);


  if(sel == PCIE_SRIO_0_LINK0_EN){
        rdata = phx_read_u32((PCIE_SRIO_REGION0_LINK0_MISC_BASE_ADDR+PCIE_X4_CONFIG0));
        phx_write_u32((PCIE_SRIO_REGION0_LINK0_MISC_BASE_ADDR + PCIE_X4_CONFIG0),((rdata & (~(0x1<<2))) + ((dut_is_rc & 0x1)<<2)));
 
        phx_write_u32((PCIE_SRIO_REGION0_LINK0_MISC_BASE_ADDR + PCIE_X4_CONFIG_INPUT_OUTPUT_0),0x3);  
  }else{
        rdata = phx_read_u32((PCIE_SRIO_REGION1_LINK0_MISC_BASE_ADDR+PCIE_X4_CONFIG0));
        phx_write_u32((PCIE_SRIO_REGION1_LINK0_MISC_BASE_ADDR + PCIE_X4_CONFIG0),((rdata & (~(0x1<<2))) + ((dut_is_rc & 0x1)<<2)));
 
        phx_write_u32((PCIE_SRIO_REGION1_LINK0_MISC_BASE_ADDR + PCIE_X4_CONFIG_INPUT_OUTPUT_0),0x3);  
  }
}


void pcie_srio_config_x2(int sel,u64 base_addr,int dut_is_rc)//0:ep,1:rc;
{
        phx_write_u32((base_addr +0x14),0x0);
        phx_write_u32((base_addr +0x20),0x0);
        phx_write_u32((base_addr +0x2c),0x3);
        phx_write_u32((base_addr +0x38),0x3);
        
        phx_write_u32((base_addr + 0x4), 0x0);
        
        phx_write_u32((base_addr + 0x0),0x11);

  if(sel == PCIE_SRIO_0_LINK0_EN){
        phx_write_u32((PCIE_SRIO_REGION0_LINK0_MISC_BASE_ADDR + PCIE_X4_CONFIG0), (0xc30 + ((dut_is_rc & 0x1) << 2) ));

        phx_write_u32((PCIE_SRIO_REGION0_LINK0_MISC_BASE_ADDR + PCIE_X4_PIPE_MUX_0), 0x0021);
        
        phx_write_u32((PCIE_SRIO_REGION0_LINK0_MISC_BASE_ADDR + PCIE_X4_CONFIG_INPUT_OUTPUT_0),0x3); 
  }else{
        phx_write_u32(PCIE_SRIO_REGION1_LINK0_MISC_BASE_ADDR + PCIE_X4_CONFIG0, (0xc30 + ((dut_is_rc & 0x1) << 2) ));

        phx_write_u32(PCIE_SRIO_REGION1_LINK0_MISC_BASE_ADDR + PCIE_X4_PIPE_MUX_0, 0x0021);
        
        phx_write_u32(PCIE_SRIO_REGION1_LINK0_MISC_BASE_ADDR + PCIE_X4_CONFIG_INPUT_OUTPUT_0,0x3); 
  }

}
        

void pcie_srio_config_x1(int sel,u64 base_addr,int dut_is_rc)//0:ep,1:rc;
{
        phx_write_u32(base_addr +0x14,0x0);
        phx_write_u32(base_addr +0x20,0x3);
        phx_write_u32(base_addr +0x2c,0x3);
        phx_write_u32(base_addr +0x38,0x3);
        
        phx_write_u32(base_addr + 0x4, 0x0);
        
        phx_write_u32(base_addr + 0x0,0x11);

  if(sel == PCIE_SRIO_0_LINK0_EN){
        phx_write_u32(PCIE_SRIO_REGION0_LINK0_MISC_BASE_ADDR + PCIE_X4_CONFIG0, (0x430 + ((dut_is_rc & 0x1) << 2) ));

        phx_write_u32(PCIE_SRIO_REGION0_LINK0_MISC_BASE_ADDR + PCIE_X4_PIPE_MUX_0, 0x0001);
        
        phx_write_u32(PCIE_SRIO_REGION0_LINK0_MISC_BASE_ADDR + PCIE_X4_CONFIG_INPUT_OUTPUT_0,0x3); 
  }else{
        phx_write_u32(PCIE_SRIO_REGION1_LINK0_MISC_BASE_ADDR + PCIE_X4_CONFIG0, (0x430 + ((dut_is_rc & 0x1) << 2) ));

        phx_write_u32(PCIE_SRIO_REGION1_LINK0_MISC_BASE_ADDR + PCIE_X4_PIPE_MUX_0, 0x0001);
        
        phx_write_u32(PCIE_SRIO_REGION1_LINK0_MISC_BASE_ADDR + PCIE_X4_CONFIG_INPUT_OUTPUT_0,0x3); 
  }

}

void pcie_srio_config_x1x1(int sel,u64 base_addr,int dut_is_rc)//0:ep,1:rc;
{
        phx_write_u32(base_addr +0x14,0x0);
        phx_write_u32(base_addr +0x20,0x0);
        phx_write_u32(base_addr +0x2c,0x3);
        phx_write_u32(base_addr +0x38,0x3);
        
        phx_write_u32(base_addr + 0x4, 0x10);
        
        phx_write_u32(base_addr + 0x0,0x111);

  if(sel == PCIE_SRIO_0_LINK0_EN){
        phx_write_u32(PCIE_SRIO_REGION0_LINK0_MISC_BASE_ADDR + PCIE_X4_CONFIG0, (0x430 + ((dut_is_rc & 0x1) << 2) ));
        phx_write_u32(PCIE_SRIO_REGION0_LINK1_MISC_BASE_ADDR + PCIE_X4_CONFIG0, (0x430 + ((dut_is_rc & 0x2) << 1) ));

        phx_write_u32(PCIE_SRIO_REGION0_LINK0_MISC_BASE_ADDR + PCIE_X4_PIPE_MUX_0, 0x0001);
        
        phx_write_u32(PCIE_SRIO_REGION0_LINK1_MISC_BASE_ADDR + PCIE_X4_PIPE_MUX_0, 0x0002);
        
        phx_write_u32(PCIE_SRIO_REGION0_LINK1_MISC_BASE_ADDR + PCIE_X4_PIPE_MUX_1, 0x0001);

        phx_write_u32(PCIE_SRIO_REGION0_LINK0_MISC_BASE_ADDR + PCIE_X4_CONFIG_INPUT_OUTPUT_0,0x3); 
        phx_write_u32(PCIE_SRIO_REGION0_LINK1_MISC_BASE_ADDR + PCIE_X4_CONFIG_INPUT_OUTPUT_0,0x3);
  }else{
        phx_write_u32(PCIE_SRIO_REGION1_LINK0_MISC_BASE_ADDR + PCIE_X4_CONFIG0, (0x430 + ((dut_is_rc & 0x1) << 2) ));
        phx_write_u32(PCIE_SRIO_REGION1_LINK1_MISC_BASE_ADDR + PCIE_X4_CONFIG0, (0x430 + ((dut_is_rc & 0x2) << 1) ));

        phx_write_u32(PCIE_SRIO_REGION1_LINK0_MISC_BASE_ADDR + PCIE_X4_PIPE_MUX_0, 0x0001);
        
        phx_write_u32(PCIE_SRIO_REGION1_LINK1_MISC_BASE_ADDR + PCIE_X4_PIPE_MUX_0, 0x0002);
        
        phx_write_u32(PCIE_SRIO_REGION1_LINK1_MISC_BASE_ADDR + PCIE_X4_PIPE_MUX_1, 0x0001);

        phx_write_u32(PCIE_SRIO_REGION1_LINK0_MISC_BASE_ADDR + PCIE_X4_CONFIG_INPUT_OUTPUT_0,0x3); 
        phx_write_u32(PCIE_SRIO_REGION1_LINK1_MISC_BASE_ADDR + PCIE_X4_CONFIG_INPUT_OUTPUT_0,0x3);
  }
}

void pcie_srio_init_vsequence(int sel,int mode_sel,int pcie_dut_i_is_rc)
{
  int PHY_ADDR;
  int REG_ADDR;
  int PHY_REG_ADDR;
  int PCIE_MGMT_BASE;
  int jj;

  if(sel == PCIE_SRIO_0_LINK0_EN){
    PHY_ADDR       = PCIE_SRIO_REGION0_PHY_IP_BASE_ADDR;
    REG_ADDR       = PCIE_SRIO_REGION0_LINK0_MISC_BASE_ADDR;
    PHY_REG_ADDR   = PCIE_SRIO_REGION0_PHY_MISC_BASE_ADDR;
    PCIE_MGMT_BASE = PCIE_SRIO_REGION0_LINK0_IP_BASE_ADDR | 0x100000;
  }else{
    PHY_ADDR       = PCIE_SRIO_REGION1_PHY_IP_BASE_ADDR;
    REG_ADDR       = PCIE_SRIO_REGION1_LINK0_MISC_BASE_ADDR;
    PHY_REG_ADDR   = PCIE_SRIO_REGION1_PHY_MISC_BASE_ADDR;
    PCIE_MGMT_BASE = PCIE_SRIO_REGION1_LINK0_IP_BASE_ADDR | 0x100000;
  }

//#ifdef CDN_SIM_SPEEDUP
        //{ behave like booting from pcie
        phx_write_u32(((0xD013 * 4 ) | PHY_ADDR), 0x1111);
        phx_write_u32(((0xD113 * 4 ) | PHY_ADDR), 0x1111);  
        phx_write_u32(((0x0046 * 4 ) | PHY_ADDR), 0x0000); // cmn_plllc_init_preg
        phx_write_u32(((0x0047 * 4 ) | PHY_ADDR), 0x0000); // cmn_plllc_init_preg
        phx_write_u32(((0x004B * 4 ) | PHY_ADDR), 0x0020); // cmn_plllc_lock_cntstart_preg
        phx_write_u32(((0x0045 * 4 ) | PHY_ADDR), 0x0019); // cmn_plllc_dcocal_ctrl_preg
        phx_write_u32(((0x004F * 4 ) | PHY_ADDR), 0x0000); // cmn_plllc_bwcal_mode1_preg
        phx_write_u32(((0x0050 * 4 ) | PHY_ADDR), 0x0000); // cmn_plllc_bwcal_mode0_preg
        phx_write_u32(((0x00C6 * 4 ) | PHY_ADDR), 0x0000); // cmn_plllc1_init_preg
        phx_write_u32(((0x00C7 * 4 ) | PHY_ADDR), 0x0000); // cmn_plllc1_itertmr_preg
        phx_write_u32(((0x00CB * 4 ) | PHY_ADDR), 0x0000); // cmn_plllc1_lock_cntstart_preg
        phx_write_u32(((0x00C5 * 4 ) | PHY_ADDR), 0x0019); // cmn_plllc1_dcocal_ctrl_preg
        phx_write_u32(((0x00CF * 4 ) | PHY_ADDR), 0x0000); // cmn_plllc1_bwcal_mode1_preg
        phx_write_u32(((0x00D0 * 4 ) | PHY_ADDR), 0x0000); // cmn_plllc1_bwcal_mode0_preg
        phx_write_u32(((0xC002 * 4 ) | PHY_ADDR), 0x4010);
        phx_write_u32(((0xC003 * 4 ) | PHY_ADDR), 0x0810);
        phx_write_u32(((0xC004 * 4 ) | PHY_ADDR), 0x0101);
        phx_write_u32(((0xC006 * 4 ) | PHY_ADDR), 0x000A);  

        for(jj = 0; jj < 4; jj++) 
        {
            phx_write_u32(((0x4020 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0041); // fpwriso_ctrl_preg
            phx_write_u32(((0x4041 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0001); // pllctrl_phase1en_preg
            phx_write_u32(((0x4042 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0001); // pllctrl_phase2en_preg
            phx_write_u32(((0x4096 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x8001); // creq_spare_preg
            phx_write_u32(((0x40C9 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0xD004); // deq_concur_ctrl2_preg
            phx_write_u32(((0x4071 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0005); // tx_rcvdet_wait_start_preg
            phx_write_u32(((0x40B3 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x000F); //smpcal_accum_preg
            phx_write_u32(((0x40B4 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0003); //smpcal_start_code_preg
            phx_write_u32(((0x40C8 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0000); // deq_concur_ctrl1_preg
            phx_write_u32(((0x40CA * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0000); // deq_epipwr_ctrl_preg
            phx_write_u32(((0x40D3 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x000A); // cmp_avr_timer_preg
            phx_write_u32(((0x40F1 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0101); // datdfe01_en_ceph_ctrl_preg
            phx_write_u32(((0x40F2 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0101); // datdfe23_en_ceph_ctrl_preg
            phx_write_u32(((0x40F3 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0101); // datdfe4_en_ceph_ctrl_preg
            phx_write_u32(((0x40F4 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0101); // datgainoffset_en_ceph_ctrl_preg
            phx_write_u32(((0x40F5 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0000); // eoffcal_en_phctrl_preg
            phx_write_u32(((0x40F8 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0000); // phalign_en_ctrl_preg
            phx_write_u32(((0x40F9 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0000); // postprecur_en_ceph_ctrl_preg
            phx_write_u32(((0x40FB * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0000); // tau_en_ceph2to0_preg
            phx_write_u32(((0x40FC * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0003); // tau_en_ceph5to3_preg
            phx_write_u32(((0x4110 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0101); // phase_iter_ceph_01_preg
            phx_write_u32(((0x4111 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0101); // phase_iter_ceph_23_preg 
            phx_write_u32(((0x4112 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0100); // phase_iter_ceph_45_preg  
            phx_write_u32(((0x4124 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0000); // oeph_en_ctrl_preg
            phx_write_u32(((0x415C * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x5425); // deq_openeye_ctrl_preg  
            phx_write_u32(((0x4183 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x7458); //cpical_res_startcode_mode23_preg
            phx_write_u32(((0x4184 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x4B3B); //cpical_res_startcode_mode01_preg
        }
//#endif

        if(mode_sel == 0)
        {
            pcie_srio_config_x4(sel,PHY_REG_ADDR,pcie_dut_i_is_rc);
        }
        else if(mode_sel == 1)
        {
            pcie_srio_config_x2(sel,PHY_REG_ADDR,pcie_dut_i_is_rc);
        }
        else if(mode_sel == 2)
        {
            pcie_srio_config_x1x1(sel,PHY_REG_ADDR,pcie_dut_i_is_rc);
        }
        else
        {
            pcie_srio_config_x1(sel,PHY_REG_ADDR,pcie_dut_i_is_rc);
        }
        delay_cnt(250);

        phx_write_u32(0x240 | PCIE_MGMT_BASE, 0x05058597);    // 1GB BAR0, Mem
        delay_cnt(2500);
}



void pcie_hsio_config_x4(u64 base_addr,int dut_is_rc)//0:ep,1:rc;
{
        int rdata;
        phx_write_u32(base_addr +0x08,0x0);
        phx_write_u32(base_addr +0x18,0x0);
        phx_write_u32(base_addr +0x28,0x0);
        phx_write_u32(base_addr +0x38,0x0);
        
        phx_write_u32(base_addr + 0x0,0x3);


        rdata = phx_read_u32(PCIE_HSIO_LINK0_MISC_BASE_ADDR+PCIE_X4_CONFIG0);
        phx_write_u32(PCIE_HSIO_LINK0_MISC_BASE_ADDR + PCIE_X4_CONFIG0,((rdata & (~(0x1<<2))) + ((dut_is_rc & 0x1)<<2)));
 
        phx_write_u32(PCIE_HSIO_LINK0_MISC_BASE_ADDR + PCIE_X4_CONFIG_INPUT_OUTPUT_0,0x3);        
}


void pcie_hsio_config_x2(u64 base_addr,int dut_is_rc)//0:ep,1:rc;
{
        phx_write_u32(base_addr +0x08,0x0);
        phx_write_u32(base_addr +0x18,0x0);
        phx_write_u32(base_addr +0x28,0x3);
        phx_write_u32(base_addr +0x38,0x3);
        
        phx_write_u32(base_addr + 0x4, 0x0);
        
        phx_write_u32(base_addr + 0x0,0x3);

        phx_write_u32(PCIE_HSIO_LINK0_MISC_BASE_ADDR + PCIE_X4_CONFIG0, (0xc30 + ((dut_is_rc & 0x1) << 2) ));

        phx_write_u32(PCIE_HSIO_LINK0_MISC_BASE_ADDR + PCIE_X4_PIPE_MUX_0, 0x0021);
        
        phx_write_u32(PCIE_HSIO_LINK0_MISC_BASE_ADDR + PCIE_X4_CONFIG_INPUT_OUTPUT_0,0x3); 

}
   

void pcie_hsio_config_x1(u64 base_addr,int dut_is_rc)//0:ep,1:rc;
{
        phx_write_u32(base_addr +0x08,0x0);
        phx_write_u32(base_addr +0x18,0x3);
        phx_write_u32(base_addr +0x28,0x3);
        phx_write_u32(base_addr +0x38,0x3);
        
        phx_write_u32(base_addr + 0x4, 0x0);
        
        phx_write_u32(base_addr + 0x0,0x3);

        phx_write_u32(PCIE_HSIO_LINK0_MISC_BASE_ADDR + PCIE_X4_CONFIG0, (0x430 + ((dut_is_rc & 0x1) << 2) ));

        phx_write_u32(PCIE_HSIO_LINK0_MISC_BASE_ADDR + PCIE_X4_PIPE_MUX_0, 0x0001);
        
        phx_write_u32(PCIE_HSIO_LINK0_MISC_BASE_ADDR + PCIE_X4_CONFIG_INPUT_OUTPUT_0,0x3); 

}

void pcie_hsio_config_x1x1(u64 base_addr,int dut_is_rc)//0:ep,1:rc;
{
        int tmp;
#if 1   //TEST_PHY_MULTI_FUNC

        tmp = phx_read_u32(base_addr);
        phx_write_u32(base_addr + 0x0, 0x1f|tmp);
#else
        phx_write_u32(base_addr +0x08,0x0);
        phx_write_u32(base_addr +0x18,0x1 << 13);
        phx_write_u32(base_addr +0x28,0x3);
        phx_write_u32(base_addr +0x38,0x3);
        
        //phx_write_u32(base_addr + 0x4, 0x10);
        phx_write_u32(base_addr + 0x0,0x7);
#endif
        phx_write_u32(PCIE_HSIO_LINK0_MISC_BASE_ADDR + PCIE_X4_CONFIG0, (0x430 + ((dut_is_rc & 0x1) << 2) ));
        phx_write_u32(PCIE_HSIO_LINK1_MISC_BASE_ADDR + PCIE_X4_CONFIG0, (0x430 + ((dut_is_rc & 0x2) << 1) ));

        phx_write_u32(PCIE_HSIO_LINK0_MISC_BASE_ADDR + PCIE_X4_PIPE_MUX_0, 0x0001);
        
        phx_write_u32(PCIE_HSIO_LINK1_MISC_BASE_ADDR + PCIE_X4_PIPE_MUX_0, 0x0002);
        
        phx_write_u32(PCIE_HSIO_LINK1_MISC_BASE_ADDR + PCIE_X4_PIPE_MUX_1, 0x0001);

        phx_write_u32(PCIE_HSIO_LINK0_MISC_BASE_ADDR + PCIE_X4_CONFIG_INPUT_OUTPUT_0,0x3); 
        phx_write_u32(PCIE_HSIO_LINK1_MISC_BASE_ADDR + PCIE_X4_CONFIG_INPUT_OUTPUT_0,0x3);
}





void pcie_hsio_init_vsequence(int sel,int mode_sel,int pcie_dut_i_is_rc)
{
  int PHY_ADDR;
  int REG_ADDR;
  int PHY_REG_ADDR;
  int PCIE_MGMT_BASE;

  int j;
  int jj;

  PHY_ADDR       = PCIE_HSIO_PHY_IP_BASE_ADDR;
  REG_ADDR       = PCIE_HSIO_LINK0_MISC_BASE_ADDR;
  PHY_REG_ADDR   = PCIE_HSIO_PHY_MISC_BASE_ADDR;
  PCIE_MGMT_BASE = PCIE_HSIO_LINK0_IP_BASE_ADDR | 0x100000;

//#ifdef CDN_SIM_SPEEDUP
        //{ behave like booting from pcie
        phx_write_u32(((0xD013 * 4 ) | PHY_ADDR), 0x1111);
        phx_write_u32(((0xD113 * 4 ) | PHY_ADDR), 0x1111);  
        phx_write_u32(((0x0046 * 4 ) | PHY_ADDR), 0x0000); // cmn_plllc_init_preg
        phx_write_u32(((0x0047 * 4 ) | PHY_ADDR), 0x0000); // cmn_plllc_init_preg
        phx_write_u32(((0x004B * 4 ) | PHY_ADDR), 0x0020); // cmn_plllc_lock_cntstart_preg
        phx_write_u32(((0x0045 * 4 ) | PHY_ADDR), 0x0019); // cmn_plllc_dcocal_ctrl_preg
        phx_write_u32(((0x004F * 4 ) | PHY_ADDR), 0x0000); // cmn_plllc_bwcal_mode1_preg
        phx_write_u32(((0x0050 * 4 ) | PHY_ADDR), 0x0000); // cmn_plllc_bwcal_mode0_preg
        phx_write_u32(((0x00C6 * 4 ) | PHY_ADDR), 0x0000); // cmn_plllc1_init_preg
        phx_write_u32(((0x00C7 * 4 ) | PHY_ADDR), 0x0000); // cmn_plllc1_itertmr_preg
        phx_write_u32(((0x00CB * 4 ) | PHY_ADDR), 0x0000); // cmn_plllc1_lock_cntstart_preg
        phx_write_u32(((0x00C5 * 4 ) | PHY_ADDR), 0x0019); // cmn_plllc1_dcocal_ctrl_preg
        phx_write_u32(((0x00CF * 4 ) | PHY_ADDR), 0x0000); // cmn_plllc1_bwcal_mode1_preg
        phx_write_u32(((0x00D0 * 4 ) | PHY_ADDR), 0x0000); // cmn_plllc1_bwcal_mode0_preg
        phx_write_u32(((0xC002 * 4 ) | PHY_ADDR), 0x4010);
        phx_write_u32(((0xC003 * 4 ) | PHY_ADDR), 0x0810);
        phx_write_u32(((0xC004 * 4 ) | PHY_ADDR), 0x0101);
        phx_write_u32(((0xC006 * 4 ) | PHY_ADDR), 0x000A);  

        for(jj = 0; jj < 4; jj++) 
        {
            phx_write_u32(((0x4020 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0041); // fpwriso_ctrl_preg
            phx_write_u32(((0x4041 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0001); // pllctrl_phase1en_preg
            phx_write_u32(((0x4042 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0001); // pllctrl_phase2en_preg
            phx_write_u32(((0x4096 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x8001); // creq_spare_preg
            phx_write_u32(((0x40C9 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0xD004); // deq_concur_ctrl2_preg
            phx_write_u32(((0x4071 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0005); // tx_rcvdet_wait_start_preg
            phx_write_u32(((0x40B3 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x000F); //smpcal_accum_preg
            phx_write_u32(((0x40B4 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0003); //smpcal_start_code_preg
            phx_write_u32(((0x40C8 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0000); // deq_concur_ctrl1_preg
            phx_write_u32(((0x40CA * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0000); // deq_epipwr_ctrl_preg
            phx_write_u32(((0x40D3 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x000A); // cmp_avr_timer_preg
            phx_write_u32(((0x40F1 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0101); // datdfe01_en_ceph_ctrl_preg
            phx_write_u32(((0x40F2 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0101); // datdfe23_en_ceph_ctrl_preg
            phx_write_u32(((0x40F3 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0101); // datdfe4_en_ceph_ctrl_preg
            phx_write_u32(((0x40F4 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0101); // datgainoffset_en_ceph_ctrl_preg
            phx_write_u32(((0x40F5 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0000); // eoffcal_en_phctrl_preg
            phx_write_u32(((0x40F8 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0000); // phalign_en_ctrl_preg
            phx_write_u32(((0x40F9 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0000); // postprecur_en_ceph_ctrl_preg
            phx_write_u32(((0x40FB * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0000); // tau_en_ceph2to0_preg
            phx_write_u32(((0x40FC * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0003); // tau_en_ceph5to3_preg
            phx_write_u32(((0x4110 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0101); // phase_iter_ceph_01_preg
            phx_write_u32(((0x4111 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0101); // phase_iter_ceph_23_preg 
            phx_write_u32(((0x4112 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0100); // phase_iter_ceph_45_preg  
            phx_write_u32(((0x4124 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x0000); // oeph_en_ctrl_preg
            phx_write_u32(((0x415C * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x5425); // deq_openeye_ctrl_preg  
            phx_write_u32(((0x4183 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x7458); //cpical_res_startcode_mode23_preg
            phx_write_u32(((0x4184 * 4 )  |  PHY_ADDR  | (jj << 11)) , 0x4B3B); //cpical_res_startcode_mode01_preg
        }
//#else
//        {
//            phx_write_u32(0x0 | PHY_REG_ADDR,0x108000);
//            phx_write_u32(((0xC003 * 4 ) | PHY_ADDR),0x0144);
//            phx_write_u32(((0xE000 * 4 ) | PHY_ADDR),0x3300);
//            phx_write_u32(((0x00A0 * 4 ) | PHY_ADDR),0x0031);
//            phx_write_u32(((0x0048 * 4 ) | PHY_ADDR),0x0105);
//            phx_write_u32(((0x0049 * 4 ) | PHY_ADDR),0x2105);
//            phx_write_u32(((0x004A * 4 ) | PHY_ADDR),0x3106);
//            phx_write_u32(((0x0050 * 4 ) | PHY_ADDR),0x8804);
//            phx_write_u32(((0x005A * 4 ) | PHY_ADDR),0x0ADB);
//            phx_write_u32(((0x0062 * 4 ) | PHY_ADDR),0x1219);
//            phx_write_u32(((0x0023 * 4 ) | PHY_ADDR),0x413B);
//            phx_write_u32(((0x6006 * 4 ) | PHY_ADDR),0x0000);
//            phx_write_u32(((0x604C * 4 ) | PHY_ADDR),0x0D33);
//            phx_write_u32(((0x606F * 4 ) | PHY_ADDR),0x9602);
//            phx_write_u32(((0x6071 * 4 ) | PHY_ADDR),0x0271);
//            phx_write_u32(((0x6081 * 4 ) | PHY_ADDR),0x813E);
//            phx_write_u32(((0x6085 * 4 ) | PHY_ADDR),0x8B7A);
//            phx_write_u32(((0x6086 * 4 ) | PHY_ADDR),0x8B7B);
//            phx_write_u32(((0x6087 * 4 ) | PHY_ADDR),0x8F5B);
//            phx_write_u32(((0x6088 * 4 ) | PHY_ADDR),0x8B4B);
//            phx_write_u32(((0x6091 * 4 ) | PHY_ADDR),0x033C);
//            phx_write_u32(((0x6096 * 4 ) | PHY_ADDR),0x0003);
//            phx_write_u32(((0x6097 * 4 ) | PHY_ADDR),0x44CC);
//            phx_write_u32(((0x609B * 4 ) | PHY_ADDR),0x088C);
//            phx_write_u32(((0x609C * 4 ) | PHY_ADDR),0x8000);
//            phx_write_u32(((0x60AC * 4 ) | PHY_ADDR),0x6A6A);
//            phx_write_u32(((0x60AD * 4 ) | PHY_ADDR),0x0080);
//            phx_write_u32(((0x60AF * 4 ) | PHY_ADDR),0x1C08);
//            phx_write_u32(((0x60D0 * 4 ) | PHY_ADDR),0x3E3E);
//            phx_write_u32(((0x60E1 * 4 ) | PHY_ADDR),0x3300);
//            phx_write_u32(((0x60EA * 4 ) | PHY_ADDR),0x0551);
//            phx_write_u32(((0x60EB * 4 ) | PHY_ADDR),0x0022);
//            phx_write_u32(((0x60EC * 4 ) | PHY_ADDR),0x0010);
//            phx_write_u32(((0x60ED * 4 ) | PHY_ADDR),0x0010);
//            phx_write_u32(((0x60EE * 4 ) | PHY_ADDR),0x0008);
//            phx_write_u32(((0x60F5 * 4 ) | PHY_ADDR),0x1561);
//            phx_write_u32(((0x60F8 * 4 ) | PHY_ADDR),0x1561);
//            phx_write_u32(((0x60FA * 4 ) | PHY_ADDR),0x00B0);
//            phx_write_u32(((0x60FB * 4 ) | PHY_ADDR),0x00A6);
//            phx_write_u32(((0x60FC * 4 ) | PHY_ADDR),0x01B6);
//            phx_write_u32(((0x60FF * 4 ) | PHY_ADDR),0x0333);
//            phx_write_u32(((0x6100 * 4 ) | PHY_ADDR),0x0788);
//            phx_write_u32(((0x6101 * 4 ) | PHY_ADDR),0x0777);
//            phx_write_u32(((0x6102 * 4 ) | PHY_ADDR),0x0BCC);
//            phx_write_u32(((0x6103 * 4 ) | PHY_ADDR),0x0888);
//            phx_write_u32(((0x6104 * 4 ) | PHY_ADDR),0x0DFF);
//            phx_write_u32(((0x6105 * 4 ) | PHY_ADDR),0x0888);
//            phx_write_u32(((0x6106 * 4 ) | PHY_ADDR),0x0CDD);
//            phx_write_u32(((0x6107 * 4 ) | PHY_ADDR),0x0888);
//            phx_write_u32(((0x6108 * 4 ) | PHY_ADDR),0x0CDD);
//            phx_write_u32(((0x6109 * 4 ) | PHY_ADDR),0x0888);
//            phx_write_u32(((0x610A * 4 ) | PHY_ADDR),0x0CDD);
//            phx_write_u32(((0x610B * 4 ) | PHY_ADDR),0x0599);
//            phx_write_u32(((0x610C * 4 ) | PHY_ADDR),0x0DEE);
//            phx_write_u32(((0x610D * 4 ) | PHY_ADDR),0x0084);
//            phx_write_u32(((0x610E * 4 ) | PHY_ADDR),0x0399);
//            phx_write_u32(((0x610F * 4 ) | PHY_ADDR),0x0DEE);
//            phx_write_u32(((0x6111 * 4 ) | PHY_ADDR),0x28FF);
//            phx_write_u32(((0x6115 * 4 ) | PHY_ADDR),0x0000);
//            phx_write_u32(((0x6118 * 4 ) | PHY_ADDR),0x0000);
//            phx_write_u32(((0x611A * 4 ) | PHY_ADDR),0x0100);
//            phx_write_u32(((0x611B * 4 ) | PHY_ADDR),0x0080);
//            phx_write_u32(((0x611C * 4 ) | PHY_ADDR),0x0024);
//            phx_write_u32(((0x611D * 4 ) | PHY_ADDR),0x0024);
//            phx_write_u32(((0x611E * 4 ) | PHY_ADDR),0x01FF);
//            phx_write_u32(((0x6121 * 4 ) | PHY_ADDR),0x001F);
//            phx_write_u32(((0x6122 * 4 ) | PHY_ADDR),0x1CE7);
//            phx_write_u32(((0x6123 * 4 ) | PHY_ADDR),0x1D67);
//            phx_write_u32(((0x6125 * 4 ) | PHY_ADDR),0x001F);
//            phx_write_u32(((0x6126 * 4 ) | PHY_ADDR),0x4000);
//            phx_write_u32(((0x6148 * 4 ) | PHY_ADDR),0x2011);
//            phx_write_u32(((0x6151 * 4 ) | PHY_ADDR),0x2008);
//            phx_write_u32(((0x6155 * 4 ) | PHY_ADDR),0x003F);
//            phx_write_u32(((0x6158 * 4 ) | PHY_ADDR),0x2E21);
//            phx_write_u32(((0x6159 * 4 ) | PHY_ADDR),0x2121);
//            phx_write_u32(((0x615C * 4 ) | PHY_ADDR),0x5882);
//            phx_write_u32(((0x6161 * 4 ) | PHY_ADDR),0x0023);
//            phx_write_u32(((0x61B0 * 4 ) | PHY_ADDR),0x559E);
//            phx_write_u32(((0x61D8 * 4 ) | PHY_ADDR),0x0019);
//            phx_write_u32(((0x61D9 * 4 ) | PHY_ADDR),0x7FFF);
//            phx_write_u32(((0x61DA * 4 ) | PHY_ADDR),0x0028);
//            phx_write_u32(((0x61DB * 4 ) | PHY_ADDR),0x0028);
//            phx_write_u32(((0x61DC * 4 ) | PHY_ADDR),0x0005);
//            phx_write_u32(((0x61DD * 4 ) | PHY_ADDR),0x000F);
//            phx_write_u32(((0x61DE * 4 ) | PHY_ADDR),0x7FC3);
//            phx_write_u32(((0x61DF * 4 ) | PHY_ADDR),0x7F81);
//            phx_write_u32(((0x61E0 * 4 ) | PHY_ADDR),0x7FC3);
//            phx_write_u32(((0x61E1 * 4 ) | PHY_ADDR),0x7FC3);
//            phx_write_u32(((0x61E2 * 4 ) | PHY_ADDR),0x7F81);
//            phx_write_u32(((0x61E3 * 4 ) | PHY_ADDR),0x7FF1);
//            phx_write_u32(((0x61E4 * 4 ) | PHY_ADDR),0x0000);
//            phx_write_u32(((0x61E5 * 4 ) | PHY_ADDR),0x1000);
//            phx_write_u32(((0x61E6 * 4 ) | PHY_ADDR),0x1092);
//            phx_write_u32(((0x61E7 * 4 ) | PHY_ADDR),0x1092);
//            phx_write_u32(((0x61E8 * 4 ) | PHY_ADDR),0x0000);
//            phx_write_u32(((0x61F3 * 4 ) | PHY_ADDR),0x392B);
//            phx_write_u32(((0x60E8 * 4 ) | PHY_ADDR),0x002D);
//            phx_write_u32(((0x60E9 * 4 ) | PHY_ADDR),0x1B24);
//            phx_write_u32(((0x60E8 * 4 ) | PHY_ADDR),0x002E);
//            phx_write_u32(((0x60E9 * 4 ) | PHY_ADDR),0x1B25);
//            phx_write_u32(((0x60E8 * 4 ) | PHY_ADDR),0x002F);
//            phx_write_u32(((0x60E9 * 4 ) | PHY_ADDR),0x1B26);
//            phx_write_u32(((0x60E8 * 4 ) | PHY_ADDR),0x0030);
//            phx_write_u32(((0x60E9 * 4 ) | PHY_ADDR),0x1B27);
//            phx_write_u32(((0x60E8 * 4 ) | PHY_ADDR),0x0031);
//            phx_write_u32(((0x60E9 * 4 ) | PHY_ADDR),0x1B28);
//            phx_write_u32(((0x60E8 * 4 ) | PHY_ADDR),0x0032);
//            phx_write_u32(((0x60E9 * 4 ) | PHY_ADDR),0x1B29);
//            phx_write_u32(((0x60E8 * 4 ) | PHY_ADDR),0x0033);
//            phx_write_u32(((0x60E9 * 4 ) | PHY_ADDR),0x1B29);
//            phx_write_u32(((0x60E8 * 4 ) | PHY_ADDR),0x0034);
//            phx_write_u32(((0x60E9 * 4 ) | PHY_ADDR),0x1B2A);
//            phx_write_u32(((0x60E8 * 4 ) | PHY_ADDR),0x0035);
//            phx_write_u32(((0x60E9 * 4 ) | PHY_ADDR),0x1B2C);
//            phx_write_u32(((0x60E8 * 4 ) | PHY_ADDR),0x0036);
//            phx_write_u32(((0x60E9 * 4 ) | PHY_ADDR),0x1B2D);
//            phx_write_u32(((0x60E8 * 4 ) | PHY_ADDR),0x0037);
//            phx_write_u32(((0x60E9 * 4 ) | PHY_ADDR),0x1B2E);
//            phx_write_u32(((0x60E8 * 4 ) | PHY_ADDR),0x0038);
//            phx_write_u32(((0x60E9 * 4 ) | PHY_ADDR),0x1B2F);
//            phx_write_u32(((0x60E8 * 4 ) | PHY_ADDR),0x0039);
//            phx_write_u32(((0x60E9 * 4 ) | PHY_ADDR),0x1B30);
//            phx_write_u32(((0x60E8 * 4 ) | PHY_ADDR),0x003A);
//            phx_write_u32(((0x60E9 * 4 ) | PHY_ADDR),0x1B31);
//            phx_write_u32(((0x60E8 * 4 ) | PHY_ADDR),0x003B);
//            phx_write_u32(((0x60E9 * 4 ) | PHY_ADDR),0x1B32);
//            phx_write_u32(((0x60E8 * 4 ) | PHY_ADDR),0x003C);
//            phx_write_u32(((0x60E9 * 4 ) | PHY_ADDR),0x1B33);
//            phx_write_u32(((0x60E8 * 4 ) | PHY_ADDR),0x003D);
//            phx_write_u32(((0x60E9 * 4 ) | PHY_ADDR),0x1B34);
//            phx_write_u32(((0x60E8 * 4 ) | PHY_ADDR),0x003E);
//            phx_write_u32(((0x60E9 * 4 ) | PHY_ADDR),0x1B35);
//            phx_write_u32(((0x60E8 * 4 ) | PHY_ADDR),0x003F);
//            phx_write_u32(((0x60E9 * 4 ) | PHY_ADDR),0x1B36);
//
//	        for(j = 0; j < 2; j++)
//            {                
//                phx_write_u32(((0xD013 * 4 ) | PHY_ADDR  |  (j << 0x10),0x1111);
//            }
//        }
//#endif

        if(mode_sel == 0)
        {
            pcie_hsio_config_x4(PHY_REG_ADDR,pcie_dut_i_is_rc);
        }
        else if(mode_sel == 1)
        {
            pcie_hsio_config_x2(PHY_REG_ADDR,pcie_dut_i_is_rc);
        }
        else if(mode_sel == 2)
        {
            pcie_hsio_config_x1x1(PHY_REG_ADDR,pcie_dut_i_is_rc);
        }
        else
        {
            pcie_hsio_config_x1(PHY_REG_ADDR,pcie_dut_i_is_rc);
        }
        delay_cnt(250);

        phx_write_u32(0x240 | PCIE_MGMT_BASE, 0x05058597);    // 1GB BAR0, Mem
        delay_cnt(2500);
}

void dma_dsp_add_cache(u64 pcie_base_addr,u64 axi_base_addr,int length,u64 pointer,int last)
{
    u64 next_pp;
    u64 w_addr;
    int wdata[44];
    int int_en;
    int i;
     int_en = 1;
     next_pp = (last == 1)?0x0:(pointer+0x2c);
     w_addr = pointer;

           wdata[0] = axi_base_addr & 0xff;
           wdata[1] = (axi_base_addr>>8 ) & 0xff;
           wdata[2] = (axi_base_addr>>16) & 0xff;
           wdata[3] = (axi_base_addr>>24) & 0xff;
           wdata[4] = (axi_base_addr>>32) & 0xff;
           wdata[5] = (axi_base_addr>>40) & 0xff;
           wdata[6] = (axi_base_addr>>48) & 0xff;
           wdata[7] = (axi_base_addr>>56) & 0xff;

           wdata[8] = 0x00;
           wdata[9] = 0x00;
           wdata[10] = 0x00;
           wdata[11] = 0x00;

           wdata[12] = pcie_base_addr & 0xff;
           wdata[13] = (pcie_base_addr>>8 ) & 0xff;
           wdata[14] = (pcie_base_addr>>16) & 0xff;
           wdata[15] = (pcie_base_addr>>24) & 0xff;
           wdata[16] = (pcie_base_addr>>32) & 0xff;
           wdata[17] = (pcie_base_addr>>40) & 0xff;
           wdata[18] = (pcie_base_addr>>48) & 0xff;
           wdata[19] = (pcie_base_addr>>56) & 0xff;

           wdata[20] = 0x00;
           wdata[21] = 0x00;
           wdata[22] = 0x00;
           wdata[23] = 0x00;
           wdata[24] = 0x00;
           wdata[25] = 0x00;
           wdata[26] = 0x00;
           wdata[27] = 0x00;

           wdata[28] = length & 0xff;
           wdata[29] = (length>>8 ) & 0xff;
           wdata[30] = (length>>16) & 0xff;

           //wdata[31] = (~last<<5)|(int_en);
           wdata[31] = (int_en);

           wdata[32] = 0x00;
           wdata[33] = 0x00;
           wdata[34] = 0x00;
           wdata[35] = 0x00;

           wdata[36] = next_pp & 0xff;
           wdata[37] = (next_pp>>8 ) & 0xff;
           wdata[38] = (next_pp>>16) & 0xff;
           wdata[39] = (next_pp>>24) & 0xff;
           wdata[40] = (next_pp>>32) & 0xff;
           wdata[41] = (next_pp>>40) & 0xff;
           wdata[42] = (next_pp>>48) & 0xff;
           wdata[43] = (next_pp>>56) & 0xff;

         //void'(inst[link_num].memoryWrite4Bytes(w_addr,data)); 

    //uart_printf("bd ad:0x%x\n\r",pointer);

    //uart_printf("Sad\n\r");
           //for( i=0;i<44;i++)
           for( i=0;i<11;i++)
           {
   //phx_write_cache_u8((pointer+i),wdata[i]);
   uart_printf("bdat:%x\n\r",((wdata[i*4+3]<<24)|(wdata[i*4+2]<<16)|(wdata[i*4+1]<<8)|wdata[i*4]));
   phx_write_cache_u32((pointer+i*4),((wdata[i*4+3]<<24)|(wdata[i*4+2]<<16)|(wdata[i*4+1]<<8)|wdata[i*4]));
           }

    //uart_printf("Ead\n\r");

        }


void dma_dsp_add(u64 pcie_base_addr,u64 axi_base_addr,int length,u64 pointer,int last)
{
    u64 next_pp;
    u64 w_addr;
    int wdata[44];
    int int_en;
    int i;

	//fudy.tmp
//	uart_printf("pcie_base_addr=%x, axi_base_addr=%x, length=%x, pointer=%x, last=%x\n\r",pcie_base_addr, axi_base_addr, length, pointer, last);
	
     int_en = 1;
     next_pp = (last == 1)?0x0:(pointer+0x2c);
     w_addr = pointer;

           wdata[0] = axi_base_addr & 0xff;
           wdata[1] = (axi_base_addr>>8 ) & 0xff;
           wdata[2] = (axi_base_addr>>16) & 0xff;
           wdata[3] = (axi_base_addr>>24) & 0xff;
           wdata[4] = (axi_base_addr>>32) & 0xff;
           wdata[5] = (axi_base_addr>>40) & 0xff;
           wdata[6] = (axi_base_addr>>48) & 0xff;
           wdata[7] = (axi_base_addr>>56) & 0xff;

           wdata[8] = 0x00;
           wdata[9] = 0x00;
           wdata[10] = 0x00;
           wdata[11] = 0x00;

           wdata[12] = pcie_base_addr & 0xff;
           wdata[13] = (pcie_base_addr>>8 ) & 0xff;
           wdata[14] = (pcie_base_addr>>16) & 0xff;
           wdata[15] = (pcie_base_addr>>24) & 0xff;
           wdata[16] = (pcie_base_addr>>32) & 0xff;
           wdata[17] = (pcie_base_addr>>40) & 0xff;
           wdata[18] = (pcie_base_addr>>48) & 0xff;
           wdata[19] = (pcie_base_addr>>56) & 0xff;

           wdata[20] = 0x00;
           wdata[21] = 0x00;
           wdata[22] = 0x00;
           wdata[23] = 0x00;
           wdata[24] = 0x00;
           wdata[25] = 0x00;
           wdata[26] = 0x00;
           wdata[27] = 0x00;

           wdata[28] = length & 0xff;
           wdata[29] = (length>>8 ) & 0xff;
           wdata[30] = (length>>16) & 0xff;

           //wdata[31] = (~last<<5)|(int_en);
           wdata[31] = (int_en);

           wdata[32] = 0x00;
           wdata[33] = 0x00;
           wdata[34] = 0x00;
           wdata[35] = 0x00;

           wdata[36] = next_pp & 0xff;
           wdata[37] = (next_pp>>8 ) & 0xff;
           wdata[38] = (next_pp>>16) & 0xff;
           wdata[39] = (next_pp>>24) & 0xff;
           wdata[40] = (next_pp>>32) & 0xff;
           wdata[41] = (next_pp>>40) & 0xff;
           wdata[42] = (next_pp>>48) & 0xff;
           wdata[43] = (next_pp>>56) & 0xff;

         //void'(inst[link_num].memoryWrite4Bytes(w_addr,data)); 

    //uart_printf("bd ad:0x%x\n\r",pointer);

           for( i=0;i<44;i++)
           {
           
		   //fudy.tmp
		   //uart_printf("pointer+i=%x, wdata[%d]=%x\n\r",(pointer+i),i,wdata[i]);
		   
   //phx_write_u32((pointer+i*4),wdata[i]);
   phx_write_u8((pointer+i),wdata[i]);
           }

   // uart_printf("Ed\n\r");

#ifdef PCIEX16_DBG_PRINT
	//fudy.tmp.
       //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "pointer=0x%x:",(pointer));
	   for( i=0;i<44;i+=4)
	   {
		   //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "[0x%x] = 0x%x ",i,phx_read_u32(pointer+i));
	   }
	   uart_printf("\n\r");
#endif
}

/*
 * get desc static
 */
void dma_dsp_status(u64 pointer)
{
    int i;

#ifdef PCIEX16_DBG_PRINT
    //fudy.tmp.
    //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "pointer=0x%x:",(pointer));
       for( i=0;i<44;i+=4)
       {
    	   //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "0x%x ",phx_read_u32(pointer+i));
       }
       uart_printf("\n\r");

//       for( i=0;i<44;i++)
//       {
//           uart_printf("0x%x ",phx_read_u8(pointer+i));
//       }
//       uart_printf("\n\r");

#endif

}

void set_intr_enable (int sel,int intr_sel)
{  
   u64 base_addr;
   base_addr = get_pcie_misc_link_base_addr(sel);
   if((sel & 0xf) != 0)
   {     
        phx_write_u32(base_addr + PCIE_X16_INTR_ENABLE, intr_sel);
   }
   else
   {
        phx_write_u32(base_addr + PCIE_X4_INTR_ENABLE , intr_sel);
   }   
}



void handle_msi_int_new(int sel,int intr_sel)
{
   int i;
   u64 base_addr;
   int msivr_addr;
   int msir_addr;
   int msiir_addr; 
   int msisr_addr;
   int wdata;
   int rdata;
   int ck_data=0x0;
   //Enum
   //do pcie_Iatucfg_seq
   base_addr = get_pcie_misc_link_base_addr(sel);//0x1f450000
   //set_intr_enable(sel,intr_sel);

//   if((sel & 0xf) != 0)
//   {  
       msivr_addr = PCIE_X16_MSIVR0;
       msir_addr  = PCIE_X16_MSIR;
       msiir_addr = PCIE_X16_MSIIR0;
       msisr_addr = PCIE_X16_MSISR;
//   }
//   else
//   {
//       msivr_addr = PCIE_X4_MSIVR0;
//       msir_addr  = PCIE_X4_MSIR;
//       msiir_addr = PCIE_X4_MSIIR0;//0x904
//       msisr_addr = PCIE_X4_MSISR;//0x900
//   }

   /*清MSI中断*/
   clear_msi_int(sel, intr_sel, 1);

   //clr_int_reg
   for(i = 0;i < 8; i++)
   {
       rdata = phx_read_u32(base_addr + msiir_addr + i*4);
       if(rdata != 0x0)
       {
            phx_write_u32(base_addr + msiir_addr + i*4, rdata);
       }
   }
   phx_write_u32(base_addr + msisr_addr , 0x0);

}
        
void handle_msi_int(int sel,int intr_sel)
{
   int i;
   u64 base_addr;
   int msivr_addr;
   int msir_addr;
   int msiir_addr; 
   int msisr_addr;
   int wdata;
   int rdata;
   int ck_data=0x0;
   //Enum
   //do pcie_Iatucfg_seq
   base_addr = get_pcie_misc_link_base_addr(sel);
   set_intr_enable(sel,intr_sel);

   if((sel & 0xf) != 0)
   {  
       msivr_addr = PCIE_X16_MSIVR0;
       msir_addr  = PCIE_X16_MSIR;
       msiir_addr = PCIE_X16_MSIIR0;
       msisr_addr = PCIE_X16_MSISR;
   }
   else
   {
       msivr_addr = PCIE_X4_MSIVR0;
       msir_addr  = PCIE_X4_MSIR;
       msiir_addr = PCIE_X4_MSIIR0;
       msisr_addr = PCIE_X4_MSISR;   
   }

   clear_msi_int(sel,intr_sel,0);
   
   //rd_chk_reg 
   test_reg_reset_value(base_addr + msisr_addr,0x2);

   test_reg_reset_value(base_addr + msiir_addr + 0*4,0x00000000);
   test_reg_reset_value(base_addr + msiir_addr + 1*4,0x00000004);
   test_reg_reset_value(base_addr + msiir_addr + 2*4,0x00000000);
   test_reg_reset_value(base_addr + msiir_addr + 3*4,0x00000000);
   test_reg_reset_value(base_addr + msiir_addr + 4*4,0x00000000);
   test_reg_reset_value(base_addr + msiir_addr + 5*4,0x00000000);
   test_reg_reset_value(base_addr + msiir_addr + 6*4,0x00000000);
   test_reg_reset_value(base_addr + msiir_addr + 7*4,0x00000000);

   test_reg_reset_value(base_addr + msivr_addr + 0*4,0x00000001);
   test_reg_reset_value(base_addr + msivr_addr + 1*4,0x56220001);
   test_reg_reset_value(base_addr + msivr_addr + 2*4,0x00000001);
   test_reg_reset_value(base_addr + msivr_addr + 3*4,0x00000001);
   test_reg_reset_value(base_addr + msivr_addr + 4*4,0x00000001);
   test_reg_reset_value(base_addr + msivr_addr + 5*4,0x00000001);
   test_reg_reset_value(base_addr + msivr_addr + 6*4,0x00000001);
   test_reg_reset_value(base_addr + msivr_addr + 7*4,0x00000001);

   //clr_int_reg
   for(i = 0;i < 8; i++)
   {
       rdata = phx_read_u32(base_addr + msiir_addr + i*4);
       if(rdata != 0x0)
       {
            phx_write_u32(base_addr + msiir_addr + i*4, rdata);
       }
   }
   phx_write_u32(base_addr + msisr_addr , 0x0);

}


void vseq_msi_int(int sel,int intr_sel)
{
   int i;
   u64 base_addr;
   int msivr_addr;
   int msir_addr;
   int msiir_addr; 
   int msisr_addr;
   int wdata;
   int rdata;
   int ck_data=0x0;
   //Enum
   //do pcie_Iatucfg_seq
   base_addr = get_pcie_misc_link_base_addr(sel);
   set_intr_enable(sel,intr_sel);

   if((sel & 0xf) != 0)
   {  
       msivr_addr = PCIE_X16_MSIVR0;
       msir_addr  = PCIE_X16_MSIR;
       msiir_addr = PCIE_X16_MSIIR0;
       msisr_addr = PCIE_X16_MSISR;
   }
   else
   {
       msivr_addr = PCIE_X4_MSIVR0;
       msir_addr  = PCIE_X4_MSIR;
       msiir_addr = PCIE_X4_MSIIR0;
       msisr_addr = PCIE_X4_MSISR;   
   }

   for(i = 0; i < 8; i++)
   {
       phx_write_u32(base_addr + msivr_addr + i*4 , 0x1);
   }
 
//   wdata = 0x12345622;
//
//   //step1 
//   //Axi Slv addr e00000000 data:0x12345622_00000000_00000000_00000000_00000000_00000000_00000000_00000000
//
//   delay_cnt(25);
//   //step2
//   //phx_write_u32(base_addr + msir_addr , wdata);
//
//   delay_cnt(1);
//   clear_msi_int(sel,intr_sel,0);
//   
//   //rd_chk_reg 
//   test_reg_reset_value(base_addr + msisr_addr,0x2);
//
//   test_reg_reset_value(base_addr + msiir_addr + 0*4,0x00000000);
//   test_reg_reset_value(base_addr + msiir_addr + 1*4,0x00000004);
//   test_reg_reset_value(base_addr + msiir_addr + 2*4,0x00000000);
//   test_reg_reset_value(base_addr + msiir_addr + 3*4,0x00000000);
//   test_reg_reset_value(base_addr + msiir_addr + 4*4,0x00000000);
//   test_reg_reset_value(base_addr + msiir_addr + 5*4,0x00000000);
//   test_reg_reset_value(base_addr + msiir_addr + 6*4,0x00000000);
//   test_reg_reset_value(base_addr + msiir_addr + 7*4,0x00000000);
//
//   test_reg_reset_value(base_addr + msivr_addr + 0*4,0x00000001);
//   test_reg_reset_value(base_addr + msivr_addr + 1*4,0x56220001);
//   test_reg_reset_value(base_addr + msivr_addr + 2*4,0x00000001);
//   test_reg_reset_value(base_addr + msivr_addr + 3*4,0x00000001);
//   test_reg_reset_value(base_addr + msivr_addr + 4*4,0x00000001);
//   test_reg_reset_value(base_addr + msivr_addr + 5*4,0x00000001);
//   test_reg_reset_value(base_addr + msivr_addr + 6*4,0x00000001);
//   test_reg_reset_value(base_addr + msivr_addr + 7*4,0x00000001);
//
//   //clr_int_reg
//   for(i = 0;i < 8; i++)
//   {
//       rdata = phx_read_u32(base_addr + msiir_addr + i*4);
//       if(rdata != 0x0)
//       {
//            phx_write_u32(base_addr + msiir_addr + i*4, 0);
//       }
//   }
//   phx_write_u32(base_addr + msisr_addr , 0x0);
//
//   delay_cnt(1);       
}


void cfg_mps(int sel)
{
    int rdata;

    //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "cfg_mps:get_pcie_ip_link_base_addr(sel)+0xc8=0x%x.",get_pcie_ip_link_base_addr(sel)+0xc8);//fudy.tmp


	//f0
    phx_write_u32((get_pcie_ip_link_base_addr(sel)+0xc8),0x00002970);
    rdata = phx_read_u32((get_pcie_ip_link_base_addr(sel)+0xc8));

    //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "cfg_mps:get_pcie_ip_link_base_addr(sel)+0xc8+BIT(12)=0x%x.",get_pcie_ip_link_base_addr(sel)+0xc8+BIT(12));//fudy.tmp

    //f1
    phx_write_u32((get_pcie_ip_link_base_addr(sel)+0xc8+BIT(12)),0x00002970);
    rdata = phx_read_u32((get_pcie_ip_link_base_addr(sel)+0xc8+BIT(12)));

}

void set_check_data(u64 baseaddr)
{
   u64 addr;
   u32 data;
   //data 1
   addr = baseaddr + 0x0;
   data = 0xdeadbeef;
   phx_write_u32(addr,data);
   uart_printf("addr=%x,data=%x,rddata=%x.\n\r",addr,data,phx_read_u32(addr));//fudy.tmp
   //data 2 
   addr = baseaddr + 0x1c;
   data = 0x55aaaa55;
   phx_write_u32(addr,data);
   uart_printf("addr=%x,data=%x,rddata=%x.\n\r",addr,data,phx_read_u32(addr));//fudy.tmp
   //data 3 
   addr = baseaddr + 0xff8;
   data = 0xff0000ff;
   phx_write_u32(addr,data);
   uart_printf("addr=%x,data=%x,rddata=%x.\n\r",addr,data,phx_read_u32(addr));//fudy.tmp   //data 4 
   addr = baseaddr + 0xffc;
   data = 0x0f0ff0f0;
   phx_write_u32(addr,data);
   uart_printf("addr=%x,data=%x,rddata=%x.\n\r",addr,data,phx_read_u32(addr));//fudy.tmp
}

//fudy.
void set_check_data_100(u64 baseaddr)
{
   u64 addr;
   u32 data;
   //data 1
   addr = baseaddr + 0x0;
   data = 0xdeadbeef;
   phx_write_u32(addr,data);
//   uart_printf("addr=%x,data=%x,rddata=%x.\n\r",addr,data,phx_read_u32(addr));//fudy.tmp
   //data 2 
   addr = baseaddr + 0x1c;
   data = 0x55aaaa55;
   phx_write_u32(addr,data);
//   uart_printf("addr=%x,data=%x,rddata=%x.\n\r",addr,data,phx_read_u32(addr));//fudy.tmp
   //data 3 
   addr = baseaddr + 0xf8;
   data = 0xff0000ff;
   phx_write_u32(addr,data);
//   uart_printf("addr=%x,data=%x,rddata=%x.\n\r",addr,data,phx_read_u32(addr));//fudy.tmp   //data 4 
   addr = baseaddr + 0xfc;
   data = 0x0f0ff0f0;
   phx_write_u32(addr,data);
//   uart_printf("addr=%x,data=%x,rddata=%x.\n\r",addr,data,phx_read_u32(addr));//fudy.tmp

}


void set_check_data_cache(u64 baseaddr)
{
   u64 addr;
   u32 data;
   //data 1
   addr = baseaddr + 0x0;
   data = 0xdeadbeef;
   phx_write_cache_u32(addr,data);
   //data 2 
   addr = baseaddr + 0x1c;
   data = 0x55aaaa55;
   phx_write_cache_u32(addr,data);
   //data 3 
   addr = baseaddr + 0xf8;
   data = 0xff0000ff;
   phx_write_cache_u32(addr,data);
   //data 4 
   addr = baseaddr + 0xfc;
   data = 0x0f0ff0f0;
   phx_write_cache_u32(addr,data);  
   
   //tmp
   u32 i = 0;
   for(i = 0x20; i< 0xf8; i+=4){
	   addr = baseaddr + i;
	   data = 0xaaaa0000+i; 	      
	   phx_write_cache_u32(addr,data);	
	   }

//   asm volatile("sync"::);

#if 0
	//read.tmp.
	for(i = 0; i< 0xf8; i+=4){
		addr = baseaddr + i;
		uart_printf("%x:%x\n\r ",baseaddr+i,phx_read_cache_u32(baseaddr+i));
		}
#endif
}

#if 0
void check_data(u64 baseaddr)
{
   u64 addr;
   u32 data;
   if ((baseaddr >= SRAM0_S)&&(baseaddr < SRAM0_UNS)) {
      //data 1
    uart_printf("SRAM SHARE!\n\r");
   addr = baseaddr + 0x0;
   data = 0xdeadbeef;
   test_cache_reg_reset_value(addr,data);
   //data 2 
   addr = baseaddr + 0x1c;
   data = 0x55aaaa55;
   test_cache_reg_reset_value(addr,data);
   //data 3 
   addr = baseaddr + 0xff8;
   data = 0xff0000ff;
   test_cache_reg_reset_value(addr,data);
   //data 4 
   addr = baseaddr + 0xffc;
   data = 0x0f0ff0f0;
   test_cache_reg_reset_value(addr,data);
   }else{
   //data 1
   addr = baseaddr + 0x0;
   data = 0xdeadbeef;
   test_reg_reset_value(addr,data);
   //data 2 
   addr = baseaddr + 0x1c;
   data = 0x55aaaa55;
   test_reg_reset_value(addr,data);
   //data 3 
   addr = baseaddr + 0xff8;
   data = 0xff0000ff;
   test_reg_reset_value(addr,data);
   //data 4 
   addr = baseaddr + 0xffc;
   data = 0x0f0ff0f0;
   test_reg_reset_value(addr,data);
   }
}
#endif

#if 0
check_data_100(u64 baseaddr)
{
   u64 addr;
   u32 data;
   if ((baseaddr >= SRAM0_S)&&(baseaddr < SRAM0_UNS)) {
      //data 1
    uart_printf("SRAM SHARE!\n\r");
   addr = baseaddr + 0x0;
   data = 0xdeadbeef;
   test_cache_reg_reset_value(addr,data);
   //data 2 
   addr = baseaddr + 0x1c;
   data = 0x55aaaa55;
   test_cache_reg_reset_value(addr,data);
   //data 3 
   addr = baseaddr + 0xf8;
   data = 0xff0000ff;
   test_cache_reg_reset_value(addr,data);
   //data 4 
   addr = baseaddr + 0xfc;
   data = 0x0f0ff0f0;
   test_cache_reg_reset_value(addr,data);
   }else{
   //data 1
   addr = baseaddr + 0x0;
   data = 0xdeadbeef;
   test_reg_reset_value(addr,data);
   //data 2 
   addr = baseaddr + 0x1c;
   data = 0x55aaaa55;
   test_reg_reset_value(addr,data);
   //data 3 
   addr = baseaddr + 0xf8;
   data = 0xff0000ff;
   test_reg_reset_value(addr,data);
   //data 4 
   addr = baseaddr + 0xfc;
   data = 0x0f0ff0f0;
   test_reg_reset_value(addr,data);
   }
}
#else //fudy , for un
void check_data_100(u64 baseaddr)
{
   u64 addr;
   u32 data;
   //data 1
   addr = baseaddr + 0x0;
   data = 0xdeadbeef;
   test_reg_reset_value(addr,data);
   //data 2 
   addr = baseaddr + 0x1c;
   data = 0x55aaaa55;
   test_reg_reset_value(addr,data);
   //data 3 
   addr = baseaddr + 0xf8;
   data = 0xff0000ff;
   test_reg_reset_value(addr,data);
   //data 4 
   addr = baseaddr + 0xfc;
   data = 0x0f0ff0f0;
   test_reg_reset_value(addr,data);
}

#endif

#if 0
void check_data_cache(u64 baseaddr)
{
   u64 addr;
   u32 data;
      //data 1
    uart_printf("SHAREABLE!\n\r");
   addr = baseaddr + 0x0;
   data = 0xdeadbeef;
   test_cache_reg_reset_value(addr,data);
   //data 2 
   addr = baseaddr + 0x1c;
   data = 0x55aaaa55;
   test_cache_reg_reset_value(addr,data);
   //data 3 
   addr = baseaddr + 0xf8;
   data = 0xff0000ff;
   test_cache_reg_reset_value(addr,data);
   //data 4 
   addr = baseaddr + 0xfc;
   data = 0x0f0ff0f0;
   test_cache_reg_reset_value(addr,data);


}
#endif


void pcie_link_ip_reg(unsigned long base_addr){
    test_reg_reset_value(base_addr + 0x0    ,  0x010017cd);
    test_reg_reset_value(base_addr + 0xa28  ,  0x00000000);
    delay_cnt(25);
    test_reg_access(base_addr +  0x00400000 + 0x1c , 0x1234ffff & (~(0xf << (4*3+16))),0x1234ffff & (~(0xf << (4*3+16))));
    test_reg_access(base_addr +  0x00400000 + 0x1c , 0x1234ffff & (~(0xf << (4*2+16))),0x1234ffff & (~(0xf << (4*2+16))));
    test_reg_access(base_addr +  0x00400000 + 0x1c , 0x1234ffff & (~(0xf << (4*1+16))),0x1234ffff & (~(0xf << (4*1+16))));
    test_reg_access(base_addr +  0x00400000 + 0x1c , 0x1234ffff & (~(0xf << (4*0+16))),0x1234ffff & (~(0xf << (4*0+16))));
    delay_cnt(25);
    test_reg_access(base_addr +  0x00100000 + 0xfb8 , 0x1234ffff & (~(0xf << (4*3+16))),0x1234ffff & (~(0xf << (4*3+16))));
    test_reg_access(base_addr +  0x00100000 + 0xfb8 , 0x1234ffff & (~(0xf << (4*2+16))),0x1234ffff & (~(0xf << (4*2+16))));
    test_reg_access(base_addr +  0x00100000 + 0xfb8 , 0x1234ffff & (~(0xf << (4*1+16))),0x1234ffff & (~(0xf << (4*1+16))));
    test_reg_access(base_addr +  0x00100000 + 0xfb8 , 0x1234ffff & (~(0xf << (4*0+16))),0x1234ffff & (~(0xf << (4*0+16))));

}



void pcie_phy_ip_reg(unsigned long base_addr){
    test_reg_reset_value(base_addr + (0xc000<<2),0x420);
    test_reg_access(base_addr +  (0xc000<<2) , 0x3d0,0x3d0);
    test_reg_reset_value(base_addr + 0x0,0x7364);
}




void pcie_x16_ip_reg(void){
    //PHY IP
    pcie_phy_ip_reg(PCIE_X16_PHY_IP_BASE_ADDR);
    //LINK IP
    pcie_link_ip_reg(PCIE_X16_LINK0_IP_BASE_ADDR);
    pcie_link_ip_reg(PCIE_X16_LINK1_IP_BASE_ADDR);
    pcie_link_ip_reg(PCIE_X16_LINK2_IP_BASE_ADDR);
    pcie_link_ip_reg(PCIE_X16_LINK3_IP_BASE_ADDR);

}

void pcie_srio_region0_ip_reg(void){
    //PHY IP
    pcie_phy_ip_reg(PCIE_SRIO_REGION0_PHY_IP_BASE_ADDR);
    //LINK IP
    pcie_link_ip_reg(PCIE_SRIO_REGION0_LINK0_IP_BASE_ADDR);
    pcie_link_ip_reg(PCIE_SRIO_REGION0_LINK1_IP_BASE_ADDR);

}

void pcie_srio_region1_ip_reg(void){
    //PHY IP
    pcie_phy_ip_reg(PCIE_SRIO_REGION1_PHY_IP_BASE_ADDR);
    //LINK IP
    pcie_link_ip_reg(PCIE_SRIO_REGION1_LINK0_IP_BASE_ADDR);
    pcie_link_ip_reg(PCIE_SRIO_REGION1_LINK1_IP_BASE_ADDR);

}

void pcie_hsio_ip_reg(void)
{
    //PHY IP
    pcie_phy_ip_reg(PCIE_HSIO_PHY_IP_BASE_ADDR);
    //LINK IP
    pcie_link_ip_reg(PCIE_HSIO_LINK0_IP_BASE_ADDR);
    pcie_link_ip_reg(PCIE_HSIO_LINK1_IP_BASE_ADDR);

}



void test_reg_reset_value(u64 reg_addr,int ck_data)
{   
    //uart_printf("====================================\n\r");
    int val = 0;
    val = phx_read_u32(reg_addr);
    if(val != ck_data)
        uart_printf("Error, addr=%x , ck_data=%x, rd_data=%x \n\r",reg_addr,ck_data,val);
	else{//fudy.add.tmp
	    uart_printf("OK. addr=%x , ck_data=%x, rd_data=%x \n\r",reg_addr,ck_data,val);
		}
}


void test_reg_access(u64 reg_addr,int wr_data,int ck_data)
{   
    //uart_printf("====================================\n\r");
    int val = 0;
    phx_write_u32(reg_addr,wr_data);
    val = phx_read_u32(reg_addr);
    if(val != ck_data)
        uart_printf("Error, addr=%x , ck_data=%x, rd_data=%x \n\r",reg_addr,ck_data,val);
}





u32 pcie_axi_read(int sel,u64 reg_addr)
{
   u64 base_addr;
   u32 val = 0;
   base_addr = get_pcie_axi_baseaddr(sel,AD_4M);
      val = phx_read_u32(base_addr + reg_addr);
      return(val);
}

void pcie_axi_write(int sel,u64 reg_addr,u32 wdata)
{
   u64 base_addr;
   base_addr = get_pcie_axi_baseaddr(sel,AD_4M); // AD_4G
// uart_printf("Awad:0x%x\n\r",base_addr);
   phx_write_u32(base_addr + reg_addr,wdata);

   return;
}


u32 pcie_axi_read_ad(int sel,int type,u64 reg_addr)
{
	u64 base_addr;
	u32 val = 0;
	base_addr = get_pcie_axi_baseaddr(sel,type);
	uart_printf("Arad:0x%x\n\r",base_addr);
	val = phx_read_u32(base_addr + reg_addr);
	return (val);
}

void pcie_axi_write_ad(int sel,int type,u64 reg_addr,u32 wdata)
{
   u64 base_addr;
   base_addr = get_pcie_axi_baseaddr(sel,type);
//   printk("[%s]: base_addr = 0x%x, reg_addr = 0x%x, wdata = 0x%x\n",\
//		   __FUNCTION__,base_addr,reg_addr,wdata);
   phx_write_u32(base_addr + reg_addr,wdata);

}
#endif/*ldf 20230914 ignore end:: 以下代码是从裸测移植过来的，暂时没用途*/

