// ====================================================================================================================================================
// Created by           : hcm 
// Filename             : sw_pciex16_3080.c
// Author               : fudy
// Created On           : 2022-08-17
// Last Modified        :
// Update Count         :
// Description          : pcie x16 functions
// ====================================================================================================================================================
#include "tool/hr3Macro.h"
#include "drvCommon.h"
#include "vcscpumacro.h"
#include "pcie.h"


/*****************************************************************************/
extern const int hr3_board_type;

static int pciex16_3080_link_mode = 0;
/*4=x4_4, 8=x8_2, 16=x16*/
void hr3_pciex16_link_mode_set(int link_mode)
{
	if(hr3_board_type == 1)/*HC3080 VPX应用板*/
	{
		pciex16_3080_link_mode = 4;
		return;
	}
	
	switch(link_mode)
	{
		case 4://x4_4
			pciex16_3080_link_mode = link_mode;
			break;
		case 8://x8_2
			pciex16_3080_link_mode = link_mode;
			break;
		case 16://x16
		default:
			pciex16_3080_link_mode = 16;
			break;
	}
}

/*****************************************************************************/
//special test items:
#define PCIE_SPECIAL_TEST_DMA_PERFORMANCE

#define PCIE_EP_ID_2FIBRE_NET_CARD_DMA_LOOP


//#define PCIE_SPECIAL_TEST_X16_LINK_LOOP

#define delay_cnt(n) { \
	volatile unsigned int i = (n); \
	while(i--); \
}


typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
//typedef unsigned long long u64;


u32 int_num_pcie=0;
u32 int_oi=1;
u64 in_dsp_axiaddr[4][4];
u64 out_dsp_axiaddr[4][4];    

//current link up:
u32 pciex16_cur_link_en = PCIE_X16_LINK0_EN;
u32 pciex16_cur_link_addr = PCIE_X16_LINK0_IP_BASE_ADDR;
u32 pciex16_cur_link_misc_addr = PCIE_X16_LINK0_MISC_BASE_ADDR;

//current dma ep id:
u32 cur_ep_id = 0;

#define EP_ID_2FIBRE_NET_CARD 	(0x100315b3)

/******************************************************************************
 * common definition
 */

#define H32(x)	(((x) >> 32) & 0xfffffffful)
#define L32(x)	((x) & 0xfffffffful)


/******************************************************************************
 * extern
 */
extern void get_ltssm_switch(u64 link_addr);
extern void pcie_x16_pcie_pma_nospeedup_sequence_x4_4(void);


/******************************************************************************
 * fudy. add
 */

#define SRAM0_UNS_CK_1058 0x10580000 //SRAM for CFG RD

//cfg map addr:

#define CFG_ADDR_AXI			((u64)0x30000000)//only for link0

//mem map addr:

#define MEM_ADDR_AXI			((u64)0x31000000)//only for link0
#define MEM_ADDR_PCIE			((u64)0x00000000)//0x10000000
#define MEM_ADDR_AXI_EP			((u64)0x10600000)

//mem2 map addr:

#define MEM2_ADDR_AXI			((u64)0x32000000)//only for link0
#define MEM2_ADDR_PCIE			((u64)0x10000000)//0x10000000
#define MEM2_ADDR_AXI_EP		((u64)0x10600000)

//addr for current link up:
u64 cur_cfg_addr_axi = CFG_ADDR_AXI;
u64 cur_mem_addr_axi = MEM_ADDR_AXI;//cur_cfg_addr_axi + 0x1000000;
u64 cur_mem2_addr_axi = MEM2_ADDR_AXI;//cur_cfg_addr_axi + 0x2000000;


static void wait(u32 count)
{
	u32 i;
	for(i=0;i<count;i++);
}


//#ifdef HC3080_APP_BOARD_TEST
//
//#define DSP_NUM_REG_TEST    0xffffffffbf0c0030
//
///*
// * get cpu id
// */
//unsigned int sys_cpuid_get(void)
//{
//	unsigned int uiCpuId = 0;
//
//    uiCpuId = (*(unsigned int *)DSP_NUM_REG_TEST & 0x3);
//
//    return uiCpuId;
//}
//
//#endif


/******************************************************************************
 * fudy.add.
 */

/******************************************************************************
 * link reg show, include pcie_link_misc(config_input_output_0) & pcie_link(pl_config_0_reg)
 * pcie_addr:PCIE_0_BASE/PCIE_1_BASE
 * pcie_wrapper_addr:PCIE_WRAP_0_BASE/PCIE_WRAP_1_BASE
 */

static void link_show(u32 pcie_link_addr,u32 pcie_link_misc_addr)
{
	u64 pl_config_reg,config_input_output;
	pl_config_reg = pcie_link_addr + 0x100000;
	config_input_output = pcie_link_misc_addr + 0x800;

	debug_print("link_show:0x%X\n\r",pcie_link_addr);
	u32 val = 0;
	val = phx_read_u32(pl_config_reg);
	debug_print("pl_config_reg(pcie):0x%X addr=0x%X\n\r",val,pl_config_reg);
	debug_print("[0]Link status      = 0x%X(1=link training complete)\n\r",val & 1);
	debug_print("[3:1]Neg Lane Count = 0x%X(00=x1,01=x2,10=x4,11=x8,100=x16)\n\r",(val>>1)&0x7);
	debug_print("[5:4]Neg Speed      = 0x%X(00=2.5G,01=5,10=8,11=16)\n\r",(val>>4)&0x3);
	debug_print("[29:24]LTSSM State  = 0x%X\n\r",(val>>24)&0x3f);
	debug_print("[31]Master Loop EN  = 0x%X\n\r",(val>>31)&0x1);

	val = 0;
	val = phx_read_u32(config_input_output);
	debug_print("config_input_output(pcie_link):0x%X addr=%x\n\r",val,config_input_output);
	debug_print("[0]Link training enable = 0x%X\n\r",val & 1);
	debug_print("[1]config enable        = 0x%X\n\r",(val>>1) & 1);
	debug_print("[4:3]Neg Speed          = 0x%X(00=2.5G,01=5,10=8,11=16)\n\r",(val>>3)&0x3);
	debug_print("[7:5]Neg link width     = 0x%X\n\r",(val>>5)&0x7);
	debug_print("[9:8]link status        = 0x%X\n\r",(val>>8)&0x3);

	debug_print("\n\r");

}


/******************************************************************************
 * link reg show, pcie_link(pl_config_0_reg),for acceptance check
 * pcie_addr:PCIE_X16_LINK0_IP_BASE_ADDR ...
 */

static void link_show_core(u32 pcie_link_addr)
{
	u64 pl_config_reg = pcie_link_addr + 0x100000;
	u32 val = 0;

	debug_print("Cadence IP link register show [P420 in *.pdf]:\n\r");

	val = phx_read_u32(pl_config_reg);
	debug_print("pl_config_reg(pcie):%x addr=%x\n\r",val,pl_config_reg);
	debug_print("[0]Link status      = %x(1=link training complete)\n\r",val & 1);
	debug_print("[3:1]Neg Lane Count = %x(0=x1,1=x2,2=x4,3=x8,4=x16)\n\r",(val>>1)&0x7);
	debug_print("[5:4]Neg Speed      = %x(0=2.5G,1=5G,2=8G,3=16G)\n\r",(val>>4)&0x3);
	debug_print("[29:24]LTSSM State  = %x\n\r",(val>>24)&0x3f);
//	debug_print("[31]Master Loop EN  = %x\n\r",(val>>31)&0x1);

	debug_print("\n\r");

}

/******************************************************************************
 * link state show, enhanced
 * for testing x16 link weak
 * link_addr:PCIE_X16_LINK0_IP_BASE_ADDR,...
 * misc_addr:PCIE_X16_LINK0_MISC_BASE_ADDR,...
 * phy_misc_addr:PCIE_X16_PHY_MISC_BASE_ADDR
 */

static void link_enhance_show(u32 pcie_link_addr,u32 pcie_link_misc_addr,u32 phy_misc_addr)
{
	u64 addr = 0;
	u32 val = 0;

	//Lane Error Status Register
	val = phx_read_u32(pcie_link_addr+0x308);//[15:0]
	debug_print("lane err st [15:0]=%x\n\r",val);

	/*************************************************************************/
	//MISC: CONFIG0
	/*
	CONFIG0 0x0 [31:16] reserved	RO	0x0
			[15:15] reg_access_clk_shutoff	RO	0x0
			[14:14] core_clk_shutoff	RO	0x0
			[13:11] lane_count_in	WR	0x2
			[10:10] core_clk_shutoff_detect_en	WR	0x1
			[9:5]	apb_core_clk_ratio	WR	0x1
			[4:3]	pcie_generation_sel WR	0x2
			[2:2]	mode_select WR	0x0
			[1:1]	disable_gen3_dc_balance WR	0x0
			[0:0]	sris_enable WR	0x0			
	*/
	val = phx_read_u32(pcie_link_misc_addr+0x0);
	debug_print("config0=%x addr=%x\n\r",val,pcie_link_misc_addr+0x0);
	debug_print("  [13:11]  lane_count_in       = %x(4=x16)\n\r",(val>>11)&0x7);
	debug_print("  [4:3]	pcie_generation_sel = %x\n\r",(val>>3)&0x3);
	debug_print("  [2:2]	mode_select         = %x\n\r",(val>>2)&0x1);	
	debug_print("  [1:1]	disable_gen3_dc_balance = %x\n\r",(val>>1)&0x1);
		
	/*************************************************************************/
	//LINK_EQUALIZATION
	/*											doc_default	real_default	
	0x4 [31:20] reserved						RO	0x0
		[19:19] bypass_phase23					WR	0x0
		[18:18] bypass_remote_tx_equalization	WR	0x0
		[17:7]	supported_preset				WR	0x7ff
		[6:0]	max_eval_iteration				WR	0x10	0	
		*/
	debug_print("link equalization=%x\n\r",phx_read_u32(pcie_link_misc_addr+0x4));//3ff80

#if 0//bypass:1,1 no improvement
	phx_write_u32(pcie_link_misc_addr+0x4,0xfff80);//[19:18]=0b11
	debug_print("link equalization=%x\n\r",phx_read_u32(pcie_link_misc_addr+0x4));
#endif

#if 1//max_eval_iteration:recommend:8-16
//	phx_write_u32(pcie_link_misc_addr+0x4,0x3ff88);//default 0, set 8,   no act
//	phx_write_u32(pcie_link_misc_addr+0x4,0x3ff90);//default 0, set 0x10, cause speed 8G -> 2.5G, xxx
//	phx_write_u32(pcie_link_misc_addr+0x4,0x3ffff);//default 0, set 0x7f(max), no act
	phx_write_u32(pcie_link_misc_addr+0x4,0x3ff80);//default 0, default
	debug_print("link equalization=%x\n\r",phx_read_u32(pcie_link_misc_addr+0x4));
#endif

	/*************************************************************************/
	//PIPE_MUX_CFG_LANE_0_1, PIPE_MUX_CFG_LANE_2_3
	debug_print("pipe mux cfg lane 0_1, 2_3=%x,%x\n\r",
		phx_read_u32(pcie_link_misc_addr+0x8),phx_read_u32(pcie_link_misc_addr+0xc));//10002,40004



	//phy_misc_addr:PCIE_X16_PHY_MISC_BASE_ADDR
	/*************************************************************************/
	/*	
	PIPE_RATE	0x40	[31:6]	reserved		
			[5:5]	enable_full_pipe_mux	WR	0x1
			[4:4]	pipe_mux_cfg_error		0x1
			[3:2]	pipe_p01_rate		
			[1:0]	pipe_p00_rate		
	*/
	debug_print("pipe rate=%x\n\r",phx_read_u32(phy_misc_addr+0x40));

	/*************************************************************************/
	/*
	PHY_LINE_CFG0	0x44	[31:28] phy_link_cfg_ln_7	WR	0x1
			[27:24] phy_link_cfg_ln_6	WR	0x1
			[23:20] phy_link_cfg_ln_5	WR	0x1
			[19:16] phy_link_cfg_ln_4	WR	0x1
			[15:12] phy_link_cfg_ln_3	WR	0x0
			[11:8]	phy_link_cfg_ln_2	WR	0x0
			[7:4]	phy_link_cfg_ln_1	WR	0x0
			[3:0]	phy_link_cfg_ln_0	WR	0x0
	PHY_LINE_CFG1	0x48	[31:28] phy_link_cfg_ln_15	WR	0x3
			[27:24] phy_link_cfg_ln_14	WR	0x3
			[23:20] phy_link_cfg_ln_13	WR	0x3
			[19:16] phy_link_cfg_ln_12	WR	0x3
			[15:12] phy_link_cfg_ln_11	WR	0x2
			[11:8]	phy_link_cfg_ln_10	WR	0x2
			[7:4]	phy_link_cfg_ln_9	WR	0x2
			[3:0]	phy_link_cfg_ln_8	WR	0x2
	*/
	debug_print("pipe line cfg0,1=%x,%x\n\r",phx_read_u32(phy_misc_addr+0x44),phx_read_u32(phy_misc_addr+0x48));

	/*************************************************************************/
	//PHY_RESET	0x54
	debug_print("phy reset=%x\n\r",phx_read_u32(phy_misc_addr+0x54));
	
		
	debug_print("\n\r");

}


/******************************************************************************
 * x16: ip wrap: config0
 * pcie_wrap_addr:
 Link0閿涳拷32'h1F45_0000閿涳拷
 Link1閿涳拷32'h1F46_0000閿涳拷
 Link2閿涳拷32'h1F47_0000閿涳拷
 Link3閿涳拷32'h1F48_0000
 pcie_wrap_cfg0(PCIE_X16_LINK0_MISC_BASE_ADDR);//0x1F450000
 */

static void pcie_wrap_cfg0(u32 pcie_wrap_addr)
{

	u32 addr = pcie_wrap_addr + 0;//config0
	u32 val = phx_read_u32(addr);

	debug_print("ip wrap config0:\n\r");	
	//[13:11]	lane_count_in
	debug_print("lane_count_in=%x\n\r",(val>>11)&0x7);
	debug_print("pcie_general_sel=%x\n\r",(val>>3)&0x3);

	return;

}

/******************************************************************************
 * wait L0:
 * sel:PCIE_X16_LINK0_EN,...
 */

static u32 pcie_link_width[100];
static u32 pcie_link_width_count = 0;

void wait_L0_x16_for_loop(u32 sel,u32 pcie_link_addr,u32 pcie_link_misc_addr)
{
	int flag = 1;
	int i = 0;
	u32 val = 0;
	u32 count = 0;
	u32 config_input_output = pcie_link_misc_addr + 0x800;
	debug_print1("wait_L0_x16:\n\r");
	debug_print1("sel=%x,link=%x,misc=%x\n\r",sel,pcie_link_addr,pcie_link_misc_addr);

	wait(0x10000000);//tmp

	while (flag)
	{
		if((0==count)||(count==0x10000)){//only print 2 times
//			link_show(pcie_link_addr,pcie_link_misc_addr);
		}
		count++;

		val = 0;
		val = phx_read_u32(config_input_output);

		if (((val>>8)&0x3) ==0x3){

			//print link status
			link_show(pcie_link_addr,pcie_link_misc_addr);
		//	link_show_core(pcie_link_addr);

			debug_print("lane=%x(0=x1,1=x2,2=x4,3=x8,4=x16) \n\rspeed=%x(0=2.5G,1=5G,2=8G,3=16G)\n\r\n\r",(val>>5)&0x7,(val>>3)&0x3);

			debug_print("Link up OK! \n\r");
			debug_print("\n\r============\n\r\n\r");

			//record retrain result:
			if(pcie_link_width_count<100){
				pcie_link_width[pcie_link_width_count] = (val>>5)&0x7;
				pcie_link_width_count++;
			}
			debug_print("lane count:\n\r");
			for(i=0;i<pcie_link_width_count;i++){
				if(i%10==0)	debug_print("\n\r");
				debug_print("%x ",pcie_link_width[i]);
			}
			debug_print("\n\r\n\r");
			

	        flag = 0;
			
			pciex16_cur_link_en = sel;
			pciex16_cur_link_addr = pcie_link_addr;
			pciex16_cur_link_misc_addr = pcie_link_misc_addr;

		}

		//overtime, break:
		if(count > 0x10000000){
			link_show(pcie_link_addr,pcie_link_misc_addr);
			//link_show_core(pcie_link_addr);			
			debug_print("Link ERROR! \n\r");
			debug_print("\n\r============\n\r\n\r");
			break;
			}
		
		delay_cnt(10);
	}


}

//return 1:linkup, 0:linkdown
int get_link_status_x16link0(void)
{
    u32 val = 0;
    u32 config_input_output = PCIE_X16_LINK0_MISC_BASE_ADDR + 0x800;

    val = phx_read_u32(config_input_output);

    if (((val>>8)&0x3) ==0x3){ //link up
        return 1;
    }else{
        return 0;
    }
}

//sel=1,link=1A000000,misc=1F450000
void wait_L0_x16(u32 sel,u32 pcie_link_addr,u32 pcie_link_misc_addr)
{
	int flag = 1;
	int i = 0;
	u32 val = 0;
	u32 count = 0;
	u32 config_input_output = pcie_link_misc_addr + 0x800;
	debug_print1("wait_L0_x16:\n\r");
	debug_print1("sel=0x%x,link=0x%x,misc=0x%x\n\r",sel,pcie_link_addr,pcie_link_misc_addr);

	//wait(0x10000000);//tmp

	while (flag)
	{
		if((0==count)||(count==0x10000)){//only print 2 times
//			link_show(pcie_link_addr,pcie_link_misc_addr);
		}
		count++;

		val = 0;
		val = phx_read_u32(config_input_output);//0x1F450800

		if (((val>>8)&0x3) ==0x3)/*启动 training成功*/
		{
			//print link status
			link_show(pcie_link_addr,pcie_link_misc_addr);
		//	link_show_core(pcie_link_addr);

			debug_print("lane=0x%x(0x0=x1,0x1=x2,0x2=x4,0x3=x8,0x4=x16) \n\rspeed=0x%x(0x0=2.5G,0x1=5G,0x2=8G,0x3=16G)\n\r\n\r",(val>>5)&0x7,(val>>3)&0x3);

			debug_print("Link up OK! \n\r");
			debug_print("\n\r============\n\r\n\r");

	        flag = 0;
			
			pciex16_cur_link_en = sel;
			pciex16_cur_link_addr = pcie_link_addr;
			pciex16_cur_link_misc_addr = pcie_link_misc_addr;
			
			break;

		}
//		else{/*ldf 20230407*/
//		    /*重新启动 training*/
//		    phx_write_u32(PCIE_X16_LINK0_MISC_BASE_ADDR | PCIE_X16_CONFIG_INPUT_OUTPUT_0, 0x3);
//		    wait_delay(50);
//		}

        //overtime, break:
#ifdef HC3080_APP_BOARD_TEST
//		wait_delay(50);
		//wait(0x80000000);
        if(count > 0x10000000){
#else
        if(count > 0x80000/*0x80000*/){/*ldf 20230822 modify:: for hc3080b*/
#endif
			link_show(pcie_link_addr,pcie_link_misc_addr);
			//link_show_core(pcie_link_addr);			
			debug_print("Link ERROR! \n\r");
			debug_print("\n\r============\n\r\n\r");
			break;
			}
		
		delay_cnt(10);
	}


}



#if 0
/******************************************************************************
 * dma
 */
//dma_test(0x10000000,0x114a1000,0x114c1000,0x100);
void dma_test(u64 pcie_addr,u64 axi_addr_in,u64 axi_addr_out,u32 len)
{
	int i;
	//u32 len;
	u64 inpointer,outpointer;
	//u64 pcie_addr,axi_addr_in,axi_addr_out;

	inpointer  = 0x11400000;//SRAM1_UNS
	outpointer = 0x11404000;
/*
	pcie_addr  = 0x10000000;

	axi_addr_in  = 0x114a1000;
	axi_addr_out = 0x114c1000;

    len = 0x100;
	*/

	debug_print1("\n\rfirst rd:[%lx]=%x,[%lx]=%x,[%lx]=%x\n\r",axi_addr_in,phx_read_u32(axi_addr_in),
	        pcie_addr,phx_read_u32(pcie_addr),axi_addr_out,phx_read_u32(axi_addr_out));

	//list:   
	debug_print("dma list add.\n\r");
	dma_dsp_add(pcie_addr,axi_addr_in,len,inpointer,0x1);
	dma_dsp_add(pcie_addr,axi_addr_out,len,outpointer,0x1);
	set_check_data_100(axi_addr_in);

/*
	//tmp.for debug:
	debug_print("\n\rinitial data.\n\r");
	for(i=0;i<len;i+=(len-1)){//0x40
		debug_print1("%x,%x=%x,%x\n\r",
			axi_addr_in+i,axi_addr_out+i,
			phx_read_u32(axi_addr_in+i),phx_read_u32(axi_addr_out+i));
		}
	debug_print1("\n\r");
*/
    cfg_mps(PCIE_X16_LINK0_EN);//??
    //cfg_mps(PCIE_SRIO_0_LINK1_EN); // x1x1
    //cfg_mps(PCIE_X16_LINK1_EN); // x4.fudy.

	//cfg:
	debug_print("pcie_dma_cfg_sequence.\n\r");
	pcie_dma_cfg_sequence(PCIE_X16_LINK0_EN,0,1,inpointer);
    debug_print("Doing dma ob!\n\r");
    wait_delay(1500);// x*1us

	pcie_dma_cfg_sequence(PCIE_X16_LINK0_EN,0,0,outpointer);
    debug_print("Doing dma ib!\n\r");
    wait_delay(1500);// x*1us

	//check:
	//check_data_100(axi_addr_out);

	//tmp.for debug:
	debug_print1("\n\rfinal data:\n\r");
	for(i=0;i<len;i+=(len-1)){//0x40
		debug_print1("%x,%x=%x,%x\n\r",
			axi_addr_in+i,axi_addr_out+i,
			phx_read_u32(axi_addr_in+i),phx_read_u32(axi_addr_out+i));
		}
	debug_print("\n\r");

    debug_print1("\n\rfinal rd:[%lx]=%x,[%lx]=%x,[%lx]=%x\n\r",axi_addr_in,phx_read_u32(axi_addr_in),
            pcie_addr,phx_read_u32(pcie_addr),axi_addr_out,phx_read_u32(axi_addr_out));

    wait_delay(4);// x*1us
    debug_print("DMA test end!\n\r\n\r");

}




/******************************************************************************
 * dma:only pcie->axi
 */
//dma_test(0x0,0x114c1000,0x100);
void dma_test_out(u64 pcie_addr,u64 axi_addr_out,u32 len)
{
	int i;
	u64 outpointer;
	u32 ch_num = 0;
	u64 dma_baseaddr = get_pcie_ip_link_base_addr(pciex16_cur_link_en) + (ch_num*0x14) + (0x1<<22) + (0x1<<21);//0x1a600000

	//dma ch0 int en:
	dma_int_en(dma_baseaddr,ch_num);

	//print para
	debug_print1("%x->%x(%x)\n\r",pcie_addr,axi_addr_out,len);

	outpointer = 0x11404000;//SRAM1_UNS

	//list:   
	debug_print1("dma list add.\n\r");
	dma_dsp_add(pcie_addr,axi_addr_out,len,outpointer,0x1);

    cfg_mps(pciex16_cur_link_en);//??

	//cfg:
	debug_print1("pcie_dma_cfg_sequence.\n\r");
	pcie_dma_cfg_sequence(pciex16_cur_link_en,0,0,outpointer);

#if 1//fudy,calc speed

	//add time
	u32 start,end;
	start=read_c0_count();

	//fudy.check done int
	u32 int_sta;
	u64 v;
	for(i=0;i<100000;i++){
		int_sta = phx_read_u32(dma_baseaddr+0xa0);
		if(int_sta&0x1){//for ch0 done_int
			end = read_c0_count();
			debug_print1("i=%d,sta=%x,start=%d,end=%d,time=%d\n\r",i,int_sta,start,end,end-start);

			v = 1500000000UL/(end-start);
			v = v * len;
			v = v / 0x100000;
			debug_print1("DMA time:%d,len=%d,v=%dMB/s\n\r",end-start,len,v);

			break;
			}
		}

#endif

	pcie_dma_int(pciex16_cur_link_en,0);//for int status show
	
    debug_print("Doing dma ib!\n\r");
    wait_delay(1500);// x*1us

    debug_print1("DMA test end!\n\r");

}

/******************************************************************************
 * dma test, with check
 *
ddst=0x10400000;//0x10400000;//sram0//0x2090000000;//ddr0 //0x114c1000;//sram1
dpcie=0;
dlen=0x10000;//0x100
dma_axi  = 0x31000000;
 */
void dma_out_check(u64 dpcie,u64 ddst,u32 dlen,u64 dma_axi)
{
	u32 i,dma_ok;

	dma_test_out(dpcie,ddst,dlen);

	//dma check:
	//[axi:0x31000000] 0(pcie)->0x114c1000
	dma_ok = 1;
	for(i = 0; i<dlen; i+=4){
		if((i==0)||(i==dlen-4)){
			debug_print1("addr[%x:%x]=val[%x:%x]\n\r",dma_axi+i,ddst+i,phx_read_u32(dma_axi+i),phx_read_u32(ddst+i));
			}
		
		if(phx_read_u32(dma_axi+i)!=phx_read_u32(ddst+i)){
			debug_print("==DMA ERROR: addr[%x->%x],val[%x!=%x]\n\r\n\r",dma_axi+i,ddst+i,phx_read_u32(dma_axi+i),phx_read_u32(ddst+i));
			dma_ok = 0; //err
			break;
			}		
		}
	if(dma_ok)
		debug_print("==DMA OK!\n\r\n\r");

}


//dma check:
//[axi:0x31000000] 0(pcie)->0x114c1000
#if 0 //call example

	debug_print("\n\r------ dma test sram------\n\r\n\r");
	

	dma_out_check(0,0x10400000,0x1000,cur_mem_addr_axi);				


	debug_print("\n\r------ dma test ddr0------\n\r\n\r");

					//ddr0
	dma_out_check(0,0x2090000000,0x1000,cur_mem_addr_axi);

#endif


	
/******************************************************************************
 * dma api:
 * pcie_dma(0x31000000,0x2090000000/0x114c1000,0x100,0x11404000,  1);
 *         pcie_mem_map_addr   dst:ddr/sram    len   linklist_addr in1out0
 * int1out0: 1(axi->pcie), 0(pcie->axi)
 */

void pcie_dma(u64 pcie_addr,u64 axi_addr,u32 len,u64 llpointer, u32 in1out0)
{
    int i;
    u32 ch_num = 0;
    u64 dma_baseaddr = get_pcie_ip_link_base_addr(pciex16_cur_link_en) + (ch_num*0x14) + (0x1<<22) + (0x1<<21);//0x1a600000
    u32 wait_time = 0x1000000;

    //dma ch0 int en:
    dma_int_en(dma_baseaddr,ch_num);

    //print para
    //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "pcie[0x%lx],axi[0x%lx](0x%x)",pcie_addr,axi_addr,len);

    //llpointer = 0x11404000;//SRAM1_UNS

    //list:
    debug_print1("dma list add.\n\r");
    dma_dsp_add(pcie_addr,axi_addr,len,llpointer,0x1);/*xxx ldf 20230531:: 填写DMA描述符*/

    cfg_mps(pciex16_cur_link_en);/*xxx ldf 20230531:: 不确定什么意思*/

    //cfg:
    debug_print1("pcie_dma_cfg_sequence.\n\r");
    pcie_dma_cfg_sequence(pciex16_cur_link_en,0,in1out0,llpointer);/*xxx ldf 20230531:: 将DMA描述符填写到寄存器，开始DMA传输*/

#if 0//fudy,calc speed

    //add time
    u64 t0,start,end;
    
    t0 = bslGetTimeUsec();
    //mylog(1, __FUNCTION__, __LINE__, "calc dma speed.");
//    start = read_c0_count();
    start = bslGetTimeUsec();
    

    //fudy.check done int
    u32 int_sta;
    u64 speed;
    for(i=0;i<wait_time;i++)
    {
        int_sta = phx_read_u32(dma_baseaddr+0xa0);
        
        if(int_sta & 0x0000ff00){
             debug_print("DMA ERR INT: int_sta=%x\n\r",int_sta);
             return ;
         }
        
        if(int_sta&0x1)//for ch0 done_int
        {
//            end = read_c0_count();
        	end = bslGetTimeUsec();
            //mylog(1, __FUNCTION__, __LINE__, "i=%d,status=0x%x,start=%lluus,end=%lluus,time=%lluus",i,int_sta,start,end,end-start);

            //w0clr
//            debug_print1("int_sta&(~0x1)=%x\n\r",int_sta&(~0x1));
//            phx_write_u32(dma_baseaddr+0xa0,int_sta&(~0x1));
//            debug_print1("int_sta=%x\n\r",phx_read_u32(dma_baseaddr+0xa0));

            //w1clr
            phx_write_u32(dma_baseaddr+0xa0,int_sta | 0x1);
            //mylog(1, __FUNCTION__, __LINE__, "clr status=0x%x",phx_read_u32(dma_baseaddr+0xa0));

            speed = len * 1000000 / (end-start);
            speed = speed / 1024 / 1024;
            //mylog(1, __FUNCTION__, __LINE__, "DMA time:%lluus,len=0x%x,speed=%lluMB/s",end-start,len,speed);

            break;
            }
        }

#endif

    //pcie_dma_int(pciex16_cur_link_en,0);//for int status show

    if(i<wait_time)
        debug_print("DMA done int OK!\n\r");
    else
        debug_print("NO DMA DONE INT!\n\r");
    //wait_delay(1500);// x*1us

#ifdef PCIEX16_DBG_PRINT

    dma_dsp_status(llpointer);//tmp

    //status:
    //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "DMA status:");
    dma_baseaddr = get_pcie_ip_link_base_addr(pciex16_cur_link_en) + (0x1<<22) + (0x1<<21) + 0x00;
    for(i=0;i<20;i+=4)//channel0
    {
        debug_print1("0x%lx=0x%x\n\r",dma_baseaddr+i,phx_read_u32(dma_baseaddr+i));
    }
    //common
    dma_baseaddr = get_pcie_ip_link_base_addr(pciex16_cur_link_en) + (0x1<<22) + (0x1<<21) + 0x00;
    i=0xa0; debug_print1("0x%lx=0x%x\n\r",dma_baseaddr+i,phx_read_u32(dma_baseaddr+i));
    i=0xa4; debug_print1("0x%lx=0x%x\n\r",dma_baseaddr+i,phx_read_u32(dma_baseaddr+i));
    i=0xa8; debug_print1("0x%lx=0x%x\n\r",dma_baseaddr+i,phx_read_u32(dma_baseaddr+i));
    i=0xac; debug_print1("0x%lx=0x%x\n\r",dma_baseaddr+i,phx_read_u32(dma_baseaddr+i));
    i=0xb0; debug_print1("0x%lx=0x%x\n\r",dma_baseaddr+i,phx_read_u32(dma_baseaddr+i));
    i=0xb4; debug_print1("0x%lx=0x%x\n\r",dma_baseaddr+i,phx_read_u32(dma_baseaddr+i));
    i=0xb8; debug_print1("0x%lx=0x%x\n\r",dma_baseaddr+i,phx_read_u32(dma_baseaddr+i));
    i=0xf8; debug_print1("0x%lx=0x%x\n\r",dma_baseaddr+i,phx_read_u32(dma_baseaddr+i));
    i=0xfc; debug_print1("0x%lx=0x%x\n\r",dma_baseaddr+i,phx_read_u32(dma_baseaddr+i));

#endif

    debug_print1("DMA test end!\n\r");

}

/******************************************************************************
 * dma test, with check
 *         pcie_mem_map_addr   cpu:ddr/sram    len         pcie_map_addr linklist_addr in1out0
 * int1out0: 1(axi->pcie), 0(pcie->axi)
 * 
 * ddst=0x10400000;//0x10400000;//sram0//0x2090000000;//ddr0 //0x114c1000;//sram1
 * llpointer = 0x11404000;//SRAM1_UNS
 */
void pcie_dma_check(u64 dpcie,u64 daxi,u32 dlen,u64 dma_axi,u64 llpointer, u32 in1out0, u32 test_data_temp)
{
    u32 i,dma_ok;

    if(in1out0){
        debug_print("DMA:dpcie[0x%lx] <- daxi[0x%lx],len=0x%x\n\r",dpcie,daxi,dlen);
//        phx_write_u32(daxi,0x11223344);//in
    }
    else{
        debug_print("DMA:dpcie[0x%lx] -> daxi[0x%lx],len=0x%x\n\r",dpcie,daxi,dlen);
//        phx_write_u32(dma_axi,0x55667788);//out
    }

    
#if 0 /*准备测试数据*/
    u32 random1,random2;
    random1 = read_c0_count() & 0xffff0000;
    random2 = (~random1) & 0xffff0000;

    debug_print("MEM rw first: daxi[0x%lx] = 0x%x, dma_axi[0x%lx] = 0x%x\n\r",daxi,phx_read_u32(daxi),dma_axi,phx_read_u32(dma_axi));
    for(i=0;i<dlen;i+=4)
    {
    	if(in1out0)
    	{
    		phx_write_u32(daxi+i,random1 | i); 
    	}
    	else
    	{
//    		debug_print("i=0x%x, dma_axi+i=0x%lx\n",i,dma_axi+i);
        	phx_write_u32(dma_axi+i,random2 | i);
    	}
    }
    debug_print("after wr:     daxi[0x%lx] = 0x%x, dma_axi[0x%lx] = 0x%x\n\r",daxi,phx_read_u32(daxi),dma_axi,phx_read_u32(dma_axi));
//    for(i=0;i<dlen;i+=4)
//    {
//        debug_print("daxi[%lx] = %x, dma_axi[%lx] = %x\n\r",daxi+i,phx_read_u32(daxi+i),dma_axi+i,phx_read_u32(dma_axi+i));
//    }
#else
    for(i=0;i<dlen;i+=4)
    {
    	if(in1out0)
    	{
    		phx_write_u32(daxi+i, test_data_temp);
    	}
    	else
    	{
    		phx_write_u32(dma_axi+i, test_data_temp);
    	}
    }
#endif
    
    /*dma 测试*/
    pcie_dma(dpcie,daxi,dlen,llpointer,in1out0);

    //dma check:
    //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "dma check.");
    dma_ok = 1;
    for(i = 0; i<dlen; i+=4)
    {
        if(phx_read_u32(dma_axi+i)!=phx_read_u32(daxi+i))
        {
//        	mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "==DMA ERROR: addr[0x%lx-0x%lx],val[0x%x!=0x%x]\n",
//        			dma_axi+i,daxi+i,phx_read_u32(dma_axi+i),phx_read_u32(daxi+i));
            dma_ok = 0; //err
            break;
		}
	}

    if(dma_ok)
    {
        debug_print("==DMA OK!\n\r\n\r");
    }
}



/******************************************************************************
 * dma test, with check
 *         pcie_mem_map_addr   cpu:ddr/sram    len         pcie_map_addr linklist_addr in1out0
 * int1out0: 1(axi->pcie), 0(pcie->axi)
 */
void pcie_dma_check_ddr(void)
{
    u64 dpcie = 0x31000000;
    u64 daxi = 0x2088000000;//0x114c2000;
    u32 dlen = 0x1000;
    u64 dma_axi = 0x31000000;
    u64 llpointer = 0x11404000;
    u32 in1out0 = 0;
    u32 i,dma_ok;

#ifdef PCIEX16_DBG_PRINT //mem rw first

    u32 random;
    random = read_c0_count() & 0xffff0000;

    debug_print("MEM rw first: daxi[%lx] = %x, dma_axi[%lx] = %x\n\r",daxi,phx_read_u32(daxi),dma_axi,phx_read_u32(dma_axi));
    phx_write_u32(daxi,random | 0x1111); phx_write_u32(dma_axi,random | 0x2222);
    debug_print("after wr:     daxi[%lx] = %x, dma_axi[%lx] = %x\n\r",daxi,phx_read_u32(daxi),dma_axi,phx_read_u32(dma_axi));

#endif

    if(in1out0){
        debug_print("DMA:%lx->%lx,len=%x\n\r",daxi,dpcie,dlen);
        phx_write_u32(dma_axi,0x11223344);
    }
    else{
        debug_print("DMA:%lx->%lx,len=%x\n\r",dpcie,daxi,dlen);
        phx_write_u32(daxi,0x55667788);
    }

    pcie_dma(dpcie,daxi,dlen,llpointer,in1out0);

    //dma check:
    //[axi:0x31000000] 0(pcie)->0x114c1000
    dma_ok = 1;
    for(i = 0; i<dlen; i+=4){
        if((i==0)||(i==dlen-4)){
            debug_print1("addr[%x:%x]=val[%x:%x]\n\r",dma_axi+i,daxi+i,phx_read_u32(dma_axi+i),phx_read_u32(daxi+i));
            }

        if(phx_read_u32(dma_axi+i)!=phx_read_u32(daxi+i)){
            debug_print("==DMA ERROR: addr[%lx-%lx],val[%x!=%x]\n\r\n\r",dma_axi+i,daxi+i,phx_read_u32(dma_axi+i),phx_read_u32(daxi+i));
            dma_ok = 0; //err
            break;
            }
        }


#ifdef PCIEX16_DBG_PRINT //
    u32 val = phx_read_u32(daxi);
    debug_print("[%lx]=%x\n\r",daxi,val);
    for(i=0;i<0x10000;i+=4){
        if(val == phx_read_u32(dma_axi+i))
            debug_print("[%lx]=%x\n\r",dma_axi+i,val);
    }

#endif

    if(dma_ok)
        debug_print("==DMA OK!\n\r\n\r");



}
#endif


void wait_delay(int num)
{
    int pi;
    for( pi=0;pi<num;pi++){ 
    delay_cnt(200);// x*5ns   -- 1 000 ns
    }
}


/*sysctrl: release reset*/
void release_rst(int pcie_en)
{
    int data_sys_cfg0_r = 0;
    int data_sys_cfg1_r = 0;
    int data_sys_ctrl_r = 0;
    int data_clock_gating_en_cfg = 0;
    
    //int data_crossbus_cfg_r
    uart_printf("release_rst\n\r");
	
    /*配置其它寄存器的通道解锁*/
	phx_write_u32(0x1f0c0028, 0xA5ACCEDE);
	phx_write_u32(0x1f0c0024,0); // release sram rst
    
	/*release reset*/
    data_sys_cfg0_r = phx_read_u32(ADDR_SYS_CFG0_R);
    data_sys_cfg1_r = phx_read_u32(ADDR_SYS_CFG1_R);
    data_sys_ctrl_r = phx_read_u32(ADDR_SYS_CTRL_R);
    data_clock_gating_en_cfg = phx_read_u32(ADDR_CLOCK_GATING_EN_CFG);
   
    //X16 link0
    if(pcie_en & PCIE_X16_LINK0_EN){
        data_sys_cfg0_r &= (~(1<<26)); 
        data_sys_cfg1_r &= (~(1<<28));
    }
    //X16 link1    
    if(pcie_en & PCIE_X16_LINK1_EN){
        data_sys_cfg0_r &= (~(1<<28)); 
        data_sys_cfg1_r &= (~(1<<28));
    }
    //X16 link2    
    if(pcie_en & PCIE_X16_LINK2_EN){
        data_sys_cfg0_r &= (~(1<<27)); 
        data_sys_cfg1_r &= (~(1<<28));
    }
    //X16 link3 
    if(pcie_en & PCIE_X16_LINK3_EN){
        data_sys_cfg0_r &= (~(1<<29)); 
        data_sys_cfg1_r &= (~(1<<28));
    }
    //SRIO REGION0 X4
    if(pcie_en & PCIE_SRIO_0_LINK0_EN){
        data_sys_cfg0_r &= (~(1<<22)); 
        data_sys_cfg1_r &= (~(1<<26));
    }
    //SRIO REGION0 X1
    if(pcie_en & PCIE_SRIO_0_LINK1_EN){
        data_sys_cfg0_r &= (~(1<<30)); 
        data_sys_cfg1_r &= (~(1<<26));
    }   
    //SRIO REGION1 X4
    if(pcie_en & PCIE_SRIO_1_LINK0_EN){
        data_sys_cfg0_r &= (~(1<<23)); 
        data_sys_cfg1_r &= (~(1<<27));
    }
    //SRIO REGION1 X1
    if(pcie_en & PCIE_SRIO_1_LINK1_EN){
        data_sys_cfg0_r &= (~(1<<31)); 
        data_sys_cfg1_r &= (~(1<<27));
    }
    //HSIO X4
    if(pcie_en & PCIE_HSIO_LINK0_EN){
        data_sys_cfg0_r &= (~(1<<25)); 
        data_sys_cfg1_r &= (~(1<<29));
    }
    //HSIO X1
    if(pcie_en & PCIE_HSIO_LINK1_EN){
        data_sys_cfg0_r &= (~(1<<24)); 
        data_sys_cfg1_r &= (~(1<<29));
    }

    phx_write_u32(ADDR_SYS_CFG0_R,data_sys_cfg0_r);
    phx_write_u32(ADDR_SYS_CFG1_R,data_sys_cfg1_r);
    
    //add yz 
    phx_write_u32(ADDR_SYS_CTRL_R,(data_sys_ctrl_r|(3<<30)));
}


/******************************************************************************
 * pcie x16:x8x8 init
 */

static void pciex8x8_init(u32 mode)
{
	release_rst(PCIE_X16_LINK0_EN | PCIE_X16_LINK1_EN );

	//register_pic_irq(PIC_IRQ_LINK0, pcie_inthandle);
	//pic_init(PIC_IRQ_LINK0, 0);

	wait_delay(4);//	-- 4 000 ns

	//pcie_x16_pcie_pma_speedup_sequence();//simulation
	//nospeedup:for chiptest

	pcie_x16_pcie_pma_nospeedup_sequence_x8x8();
	pcie_x16_pcie_x8_2_config_sequence(mode);//link0:rc,link1:rc

}


/******************************************************************************
 * pcie x16:x16 init
 */

static void pciex16_init(u32 mode)
{
	/*sysctrl: release reset*/
	release_rst(PCIE_X16_LINK0_EN );

//	register_pic_irq(PIC_IRQ_LINK0, pcie_inthandle);
	//pic_init(PIC_IRQ_LINK0, 0);

	wait_delay(4);//	-- 4 000 ns
	wait_delay(100);//220812,for x16 link test

	//pcie_x16_pcie_pma_speedup_sequence();//simulation
	//nospeedup:for chiptest

	
#if 0//tmp:force 2.5G
	u32 cfg0=0x1f450000+0;
	u32 val = phx_read_u32(cfg0);
	val &= 0xffffffe7;//[4:3]=0, 2.5G
	phx_write_u32(cfg0,val);
#endif

	/*PHY配置(X16)  正常使用(慢仿真) 模式配置*/
	pcie_x16_pcie_pma_nospeedup_sequence_x16();
	/*配置LINK模式: LINK0 (X16) RC/EP模式*/
    pcie_x16_pcie_x16_config_sequence(mode);
    
    return;
}

/******************************************************************************
 * pcie x16:x4_4 init
 */

static void pciex4_4_init(u32 mode)
{
	release_rst(PCIE_X16_LINK0_EN | PCIE_X16_LINK1_EN | PCIE_X16_LINK2_EN | PCIE_X16_LINK3_EN);

	//register_pic_irq(PIC_IRQ_LINK0, pcie_inthandle);
	//pic_init(PIC_IRQ_LINK0, 0);

	wait_delay(4);//	-- 4 000 ns

	//pcie_x16_pcie_pma_speedup_sequence();//simulation
	//nospeedup:for chiptest

	pcie_x16_pcie_pma_nospeedup_sequence_x4_4();
	pcie_x16_pcie_x4_4_config_sequence(mode);//link0

}


/******************************************************************************
 * 控制 PCIe 外发传输，配置ATU （Address Translation Unit），
 * 将 PCIe 地址映射到 AXI 地址，并将相关信息写入 ATU 寄存器 
 * 
 * pcie :outbound
 * 映射类型 type:
		0b0010	MEM
		0b0110	IO
		0b1010	CFG0
	映射大小byte = 2^(size+1)
 */
void pcie_ctrl_ob(u32 sel, u64 axiaddr, u64 pcieaddr, int size, int rg_num, int is_rc, int type)
{
   u32 atu_baseaddr;
   int tc = 0;

   atu_baseaddr = get_pcie_ip_link_base_addr(sel) + (rg_num*0x20) + (0x1<<22);/*xxx ldf 20230808 note:: (0x1<<22)不确定是什么含义，需要查下pcie ip核的手册*/

   phx_write_u32((atu_baseaddr+0x0) ,((pcieaddr & 0xffffffff)+size));
   phx_write_u32((atu_baseaddr+0x4) , (pcieaddr >> 32));

   phx_write_u32(atu_baseaddr+0x8 , ((is_rc << 23) | (tc << 17) | type));

   phx_write_u32((atu_baseaddr+0xc) ,0x0);
   phx_write_u32((atu_baseaddr+0x10),0x0);
   phx_write_u32((atu_baseaddr+0x18),((axiaddr & 0xffffffff)+size));
   phx_write_u32((atu_baseaddr+0x1c),(axiaddr >> 32));
   
#ifdef PCIEX16_DBG_PRINT
   //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "addr0 [0x%lx]=0x%x", atu_baseaddr+0x0,phx_read_u32(atu_baseaddr+0x0));
   //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "addr1 [0x%lx]=0x%x", atu_baseaddr+0x4,phx_read_u32(atu_baseaddr+0x4));
   //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "desc0 [0x%lx]=0x%x,[23]=0x%x", atu_baseaddr+0x8,phx_read_u32(atu_baseaddr+0x8),(phx_read_u32(atu_baseaddr+0x8)>>23)&0x1);
   //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "desc1 [0x%lx]=0x%x", atu_baseaddr+0xc,phx_read_u32(atu_baseaddr+0xc));
   //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "desc2 [0x%lx]=0x%x", atu_baseaddr+0x10,phx_read_u32(atu_baseaddr+0x10));
   //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "axi_addr0 [0x%lx]=0x%x", atu_baseaddr+0x18,phx_read_u32(atu_baseaddr+0x18));
   //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "axi_addr1 [0x%lx]=0x%x", atu_baseaddr+0x1c,phx_read_u32(atu_baseaddr+0x1c));
#endif
}

void pcie_ctrl_ep_ob(u32 sel, u64 axiaddr,u64 pcieaddr,int size,int rg_num, int type)
{
   u32 atu_baseaddr;
   int is_rc = 0; //ep

   atu_baseaddr = get_pcie_ip_link_base_addr(sel) + (rg_num*0x20) + (0x1<<22);

   //addr0:
   phx_write_u32((atu_baseaddr+0x0) ,((pcieaddr & 0xffffffff)+size));
   //addr1:
   phx_write_u32((atu_baseaddr+0x4) , (pcieaddr >> 32));

   //desc0:
   u32 tc = 0;
   phx_write_u32(atu_baseaddr+0x8 ,(type + (is_rc<<23) + (tc<<17)));
   //desc1:
   phx_write_u32((atu_baseaddr+0xc) ,0x00000000);

   //axi0:
   phx_write_u32((atu_baseaddr+0x18),((axiaddr & 0xffffffff)+size));
   //axi1:
   phx_write_u32((atu_baseaddr+0x1c),(axiaddr >> 32));
   
#ifdef PCIEX16_DBG_PRINT
   //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "addr0 [0x%lx]=0x%x", atu_baseaddr+0x0,phx_read_u32(atu_baseaddr+0x0));
   //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "addr1 [0x%lx]=0x%x", atu_baseaddr+0x4,phx_read_u32(atu_baseaddr+0x4));
   //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "desc0 [0x%lx]=0x%x,[23]=0x%x", atu_baseaddr+0x8,phx_read_u32(atu_baseaddr+0x8),(phx_read_u32(atu_baseaddr+0x8)>>23)&0x1);
   //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "desc1 [0x%lx]=0x%x", atu_baseaddr+0xc,phx_read_u32(atu_baseaddr+0xc));
   //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "desc2 [0x%lx]=0x%x", atu_baseaddr+0x10,phx_read_u32(atu_baseaddr+0x10));
   //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "axi_addr0 [0x%lx]=0x%x", atu_baseaddr+0x18,phx_read_u32(atu_baseaddr+0x18));
   //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "axi_addr1 [0x%lx]=0x%x", atu_baseaddr+0x1c,phx_read_u32(atu_baseaddr+0x1c));
#endif
}


void pcie_ctrl_ib(u32 sel, u64 axiaddr, int bar_num, int num_bits)
{
    u32 mgmt_baseaddr = get_pcie_ip_link_base_addr(sel);//0x1a000000
//    int num_bits;
    u32 atu_baseaddr;
//    int flag = 0;
    
	
	if(bar_num>1)//bar_num=7
	    atu_baseaddr = mgmt_baseaddr + (1<<22)+ 0x810;
	else
    	atu_baseaddr = mgmt_baseaddr + (1<<22)+ 0x800 + (bar_num*0x8);//走的这里

    // Inbound address translation, BAR0
    //---------------------------------------------------
    // addr0
    phx_write_u32(atu_baseaddr + 0x0 ,((axiaddr & 0xffffffff) |num_bits));
    //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "[0x%lx]=0x%x", atu_baseaddr + 0x0,phx_read_u32(atu_baseaddr + 0x0));
    // addr1
    phx_write_u32(atu_baseaddr + 0x4 ,(axiaddr >> 32)& 0xffffffff);
    //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "[0x%lx]=0x%x", atu_baseaddr + 0x4,phx_read_u32(atu_baseaddr + 0x4));
}


/******************************************************************************
* pcie x16:get ep mem bar size
* *p_bar_bits = 32/64
*/

int pcie_get_bar_mem_size(u64 ep_cfg_addr, u32* p_bar_no, u32* p_num_bits, u32* p_bar_bits)
{
    u32 val = 0;
    u32 bar_no = 0;
    u32 num_bits = 0;
    u32 bar_bits = 32;

    for(bar_no=0;bar_no<7;bar_no++){
        phx_write_u32(ep_cfg_addr+0x10+bar_no*4,0xffffffff);
        val = phx_read_u32(ep_cfg_addr+0x10+bar_no*4);

        debug_print1("pcie_get_bar_mem_size:bar_val=%x\n\r",val);

        if(0==(val&0x3))  //mem bar
        {
            bar_bits = ( ( val>>2 ) & 0x1 ) ? 64 : 32;

            for(num_bits=5; num_bits<31; num_bits++){
                if( (val>>num_bits) & 0x1 ){
                    break;
                }
            }

            *p_bar_no = bar_no;
            *p_num_bits = num_bits;
            *p_bar_bits = bar_bits;

            debug_print1("pcie_get_bar_mem_size:bar_no=%d,num_bits=%d,bar_bits=%d\n\r",bar_no,num_bits,bar_bits);

            return 0;
        }
    }

    return -1;
}

void pcie_rc_bar_cfg(u32 sel)
{
    u32 mgmt_baseaddr = get_pcie_ip_link_base_addr(sel);
    u64 bar_cfg_addr = 0;
    int is_rc = 1;
    
	//commmand: I/O space、MEM space、BUS Master
    phx_write_u32(mgmt_baseaddr+0x4, 0x00000007);
    
    if(is_rc){
    	bar_cfg_addr = mgmt_baseaddr + (1<<20) + 0x300;
    	phx_write_u32(bar_cfg_addr ,0x5058597);//0x5058597,0x50585cd
    }else{
    	bar_cfg_addr = mgmt_baseaddr + (1<<20) + 0x240;
    	phx_write_u32(bar_cfg_addr ,0x5058597);
    }
}


void pciex16_0_ep_ib(u32 sel, u64 axiaddr, int bar_num, int num_bits)//ctrl_base,bar_num, num_bits: reserved
{
    u32 mgmt_baseaddr = get_pcie_ip_link_base_addr(sel);//0x1a000000
    u32 atu_baseaddr;
    
    phx_write_u32(mgmt_baseaddr + 0x4, 0x7);
    //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "[0x%lx]=0x%x", mgmt_baseaddr + 0x4,phx_read_u32(mgmt_baseaddr + 0x4));

    phx_write_u32(mgmt_baseaddr + (1<<20) + 0x240, 0x50517c7);//0x50585c0//0x5058597//0x50517cf//0x50517c7
    //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "[0x%lx]=0x%x", mgmt_baseaddr + (1<<20) + 0x240,phx_read_u32(mgmt_baseaddr + (1<<20) + 0x240));

    atu_baseaddr = mgmt_baseaddr + (1<<22)+ 0x840;//0x1a400840
	
    if(bar_num == 0)
    {
		phx_write_u32(atu_baseaddr + 0x0, (axiaddr & 0xffffffff)|num_bits);
		//mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "[0x%lx]=0x%x", atu_baseaddr + 0x0,phx_read_u32(atu_baseaddr + 0x0));
		phx_write_u32(atu_baseaddr + 0x4, (axiaddr>>32)&0xffffffff);
		//mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "[0x%lx]=0x%x", atu_baseaddr + 0x4,phx_read_u32(atu_baseaddr + 0x4));
    }
    else if(bar_num == 1)
    {
		phx_write_u32(atu_baseaddr + 0x8, (axiaddr & 0xffffffff)|num_bits);
		//mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "[0x%lx]=0x%x", atu_baseaddr + 0x0,phx_read_u32(atu_baseaddr + 0x0));
		phx_write_u32(atu_baseaddr + 0xc, (axiaddr>>32)&0xffffffff);
		//mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "[0x%lx]=0x%x", atu_baseaddr + 0x4,phx_read_u32(atu_baseaddr + 0x4));
    }
}

/******************************************************************************
 * pcie x16:get ep ib status, just for PCIE_X16_LINK0_EN (EP mode)
*/
void pciex16_0_ep_ib_get(void)
{
    u64 addr = 0;

    addr = PCIE_X16_LINK0_IP_BASE_ADDR+0x4;
    debug_print("pciex16_0_ep_ib_get:[0x%lx]=0x%x\n\r",addr,phx_read_u32(addr));

    addr = PCIE_X16_LINK0_IP_BASE_ADDR+(1<<20)+0x240;
    debug_print("pciex16_0_ep_ib_get:[0x%lx]=0x%x\n\r",addr,phx_read_u32(addr));

    addr = PCIE_X16_LINK0_IP_BASE_ADDR+(1<<22)+0x840;
    debug_print("pciex16_0_ep_ib_get:[0x%lx]=0x%x\n\r",addr,phx_read_u32(addr));

    addr = PCIE_X16_LINK0_IP_BASE_ADDR+(1<<22)+0x844;
    debug_print("pciex16_0_ep_ib_get:[0x%lx]=0x%x\n\r",addr,phx_read_u32(addr));
}

void pciex16_3080_init(u32 mode)
{
    int num;
    int rdata;
    int ch;
    int pi;
    int pcie_ctrl_num = 2;
    u32 i,len;
	u64 addr;
	u32 val;

	hr3_pciex16_link_mode_set(16); //x16
	switch(pciex16_3080_link_mode)
	{
		case 4://x4_4
			pciex4_4_init(mode);
			break;
		case 8://x8_2
			pciex8x8_init(mode);
			break;
		case 16://x16
		default:
			pciex16_init(mode);
			break;
	}

    //fudy.
    //space_show();

//	axi_addr = SRAM0_UNS_CK_1058;
//	pcie_iatu_cfg_sequence(PCIE_X16_LINK0_EN,axi_addr,0x1,0,0);

	//link up:

#if 0	/*link测试(LINK0) */
	debug_print("\n\r------ link selftest (LINK0) ------\n\r\n\r");
	wait_L0_x16(PCIE_X16_LINK0_EN,PCIE_X16_LINK0_IP_BASE_ADDR,PCIE_X16_LINK0_MISC_BASE_ADDR);
	
//	if(pciex16_3080_link_mode == 8)
//	{
//		debug_print("\n\r------ link test (LINK1) ------\n\r\n\r");
//		wait_L0_x16(PCIE_X16_LINK1_EN,PCIE_X16_LINK1_IP_BASE_ADDR,PCIE_X16_LINK1_MISC_BASE_ADDR);
//	}
//
//	
//	if(pciex16_3080_link_mode == 16)
//	{
//		debug_print("\n\r------ link test (LINK1) ------\n\r\n\r");
//		wait_L0_x16(PCIE_X16_LINK1_EN,PCIE_X16_LINK1_IP_BASE_ADDR,PCIE_X16_LINK1_MISC_BASE_ADDR);
//		debug_print("\n\r------ link test (LINK2) ------\n\r\n\r");
//		wait_L0_x16(PCIE_X16_LINK2_EN,PCIE_X16_LINK2_IP_BASE_ADDR,PCIE_X16_LINK2_MISC_BASE_ADDR);
//		debug_print("\n\r------ link test (LINK3) ------\n\r\n\r");
//		wait_L0_x16(PCIE_X16_LINK3_EN,PCIE_X16_LINK3_IP_BASE_ADDR,PCIE_X16_LINK3_MISC_BASE_ADDR);
//	}
	
	/*获取LTSSM状态机的状态*/
	get_ltssm_switch(PCIE_X16_LINK0_MISC_BASE_ADDR);

    wait_delay(4);//    -- 4 000 ns

    debug_print("test end!\r\n");
#endif
    return;
}


