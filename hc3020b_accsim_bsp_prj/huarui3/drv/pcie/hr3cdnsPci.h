#ifndef _HC3CDNS_PCI_
#define _HR3CDNS_PCI_

#include <stdio.h>
#include <string.h>
#include <drvCommon.h>
//#include <spinlock.h>

typedef int (*PFUNC_PCIE_CBACK) (void*,void*,void*);

//#define PCIE_APP_BASE_ADDR 0xffffffffbf050000
//#define PCIE_CFG_BASE_ADDR 0xffffffffb8000000

#define PCIE_APP_BASE_ADDR 0xffffffffbf450000


#define PCIEX16_CFG0_BASE_ADDR 	0x30000000 /*type0配置空间*/
#define PCIEX16_CFG0_SPACE_SIZE	25 /*2^25 = 32MB*/

#define PCIEX16_CFG1_BASE_ADDR 	0x32000000 /*type1配置空间*/
#define PCIEX16_CFG1_SPACE_SIZE	25 /*2^25 = 32MB*/

/*xxx ldf 20230912 note:: 有些pci设备的BAR不支持64bit地址,此处MEM空间基地址最好是使用32位的地址进行分配,
 * 但是需要注意的是 这一段空间只有64MB可供MEM空间分配,对某些pci设备可能不够用,
 * 或者可以考虑将配置空间放到0x100000000段,这样0x30000000段的128MB都可以用来给MEM空间分配*/
#define PCIE_MEM_BASE_ADDR		0x34000000//0x100000000 /*MEM空间*/   
#define PCIE_MEM_SPACE_SIZE		26//30

/*xxx ldf 20230912 note:: IO空间暂时先不使用，等遇到需要使用的情况再配置*/
#define PCIE_IO_BASE_ADDR		0 /*IO空间*/
#define PCIE_IO_SPACE_SIZE		0 


#define PCIE_EP_CFG_BASE_ADDR 0xffffffffb8100000/*not use*/

#define PCIE_BOOT_CMD 0x80000000
#define PCIE_DEVICE_ID 0xabcd16c3
#define PCIE_FPGA_DEVICE_ID 0x702410ee

#define PCIE_PL_IATU_INDEX  0x900   /* iATU Index Register */
#define PCIE_PL_IATU_BASE_L 0x90c   /* iATU Region Lower Base Address Register */
#define PCIE_PL_IATU_BASE_H 0x910   /* iATU Region Upper Base Address Register */
#define PCIE_PL_IATU_LIMIT  0x914   /* iATU Region Limit Address Register */
#define PCIE_PL_IATU_TGT_L  0x918   /* iATU Region Lower Target Address Register */
#define PCIE_PL_IATU_TGT_H  0x91c   /* iATU Region Upper Target Address Register */
#define PCIE_PL_IATU_CTRL_1 0x904   /* iATU Region Control 1 Register */
#define         TLP_TYPE_MEM    0
#define         TLP_TYPE_IO     2
#define         TLP_TYPE_CFG0   4
#define         TLP_TYPE_CFG1   5
#define         TLP_TYPE_MSG_EP_TO_RC   0x10    /* from end point to root complex */
#define         TLP_TYPE_MSG_RC_BC      0x13    /* root complex broadcast */    
#define PCIE_PL_IATU_CTRL_2 0x908   /* iATU Region Control 2 Register */
#define         CFG_SHIFT_MODE  0x10000000
#define         REGION_ENABLE   0X80000000
#define         BAR_MATCH_MODE  0x40000000
#define PCIE_PL_DMA_MAX_MTU         4096
#define PCIE_PL_DMA_WR_ENGINE_ENABLE    	(0x700 + 0x27c)
#define PCIE_PL_DMA_RD_ENGINE_ENABLE    	(0x700 + 0x29c)
#define PCIE_PL_DMA_WR_INT_MASK         	(0x700 + 0x2c4)
#define PCIE_PL_DMA_RD_INT_MASK         	(0x700 + 0x318)
#define PCIE_PL_DMA_CHANNEL_CONTEXT_INDEX 	(0x700 + 0x36c)
#define PCIE_PL_DMA_CHANNEL_CTRL_1      	(0x700 + 0x370)
#define         CHANNEL_CTRL_1_CS 0x60
#define         CS_DMA_RUNNING  0x20
#define         DMA_LIE         0X8
#define         DMA_RIE         0x10
#define			TD              0X04000000
#define PCIE_PL_DMA_CHANNEL_CTRL_2      	(0x700 + 0x374)
#define PCIE_PL_DMA_TX_SIZE         		(0x700 + 0x378)
#define PCIE_PL_DMA_SAR_L               	(0x700 + 0x37c)
#define PCIE_PL_DMA_SAR_H               	(0x700 + 0x380)
#define PCIE_PL_DMA_DAR_L               	(0x700 + 0x384)
#define PCIE_PL_DMA_DAR_H               	(0x700 + 0x388)
#define PCIE_PL_DMA_WR_WEI_L        		(0x700 + 0x288)
#define PCIE_PL_DMA_WR_WEI_H        		(0x700 + 0x28c)
#define PCIE_PL_DMA_WR_DOORBELL         	(0x700 + 0x280)
#define PCIE_PL_DMA_RD_DOORBELL         	(0x700 + 0x2a0)
#define PCIE_PL_DMA_WR_INT_ST               (0x700 + 0x2bc)
#define PCIE_PL_DMA_WR_INT_CLR              (0x700 + 0x2c8)
#define PCIE_PL_DMA_RD_INT_ST               (0x700 + 0x310)
#define PCIE_PL_DMA_RD_INT_CLR              (0x700 + 0x31c)
#define PCIE_PL_DMA_WR_DONE_IMWR_ADDR_L     (0x700 + 0x2d0)
#define PCIE_PL_DMA_WR_DONE_IMWR_ADDR_H     (0x700 + 0x2d4)
#define PCIE_PL_DMA_WR_ABORT_IMWR_ADDR_L    (0x700 + 0x2d8)
#define PCIE_PL_DMA_WR_ABORT_IMWR_ADDR_H    (0x700 + 0x2dc)
#define PCIE_PL_DMA_WR_IMWR_DATA_CHANNEL_0  (0x700 + 0x2e0)
#define PCIE_PL_DMA_RD_DONE_IMWR_ADDR_L     (0x700 + 0x33c)
#define PCIE_PL_DMA_RD_DONE_IMWR_ADDR_H     (0x700 + 0x340)
#define PCIE_PL_DMA_RD_ABORT_IMWR_ADDR_L    (0x700 + 0x344)
#define PCIE_PL_DMA_RD_ABORT_IMWR_ADDR_H    (0x700 + 0x348)
#define PCIE_PL_DMA_RD_IMWR_DATA_CHANNEL_0  (0x700 + 0X34c)
#define PCIE_PL_DMA_READ_INTR_STATUS        (0x700 + 0x310)
#define PCIE_PL_DMA_READ_ERROR_STATUS_L     (0x700 + 0x324)
#define PCIE_PL_DMA_READ_ERROR_STATUS_H     (0x700 + 0x328)
#define PCIE_PL_DMA_WRITE_INTR_STATUS       (0x700 + 0x2bc)
#define PCIE_PL_DMA_WRITE_ERROR_STATUS      (0x700 + 0x2CC)
#define PCIE_PL_DMA_WRITE_LL_ERROR_ENABLE   (0x700 + 0x300)
#define PCIE_PL_DMA_READ_LL_ERROR_ENABLE    (0x700 + 0x334)
#define PCIE_PL_DMA_LL_POINTER_L            (0x700 + 0x38c)
#define PCIE_PL_DMA_LL_POINTER_H            (0x700 + 0x390)
#define PCIE_PL_MSI_CTRL_ADDR_L             (0x700 + 0x120)
#define PCIE_PL_MSI_CTRL_ADDR_H             (0x700 + 0x124)
#define PCIE_PL_MSI_CTRL_INT_0_EN           (0x700 + 0x128)
#define PCIE_PL_MSI_CTRL_INT_0_MASK         (0x700 + 0x12c)
#define PCIE_PL_MSI_CTRL_INT_0_ST           (0x700 + 0x130)
#define PCIE_PL_FILTER_MASK_1               (0x700 + 0x1c)
#define PCIE_PL_FILTER_MASK_2               (0x700 + 0x20)
#define PCIE_PLPORT_LINK_CTRL_REG			(0x700 + 0x10)
#define PCIE_PL_LWSCCR						(0X700 + 0x10e) /* Link Width and Speed Change Control Register */
#define PCIE_PL_TPFCCS						(0x700 + 0x30) /* Transmit Posted FC Credit Status */
#define PCIE_PL_TNPFCCS                  	(0x700 + 0x34) /* Transmit Non-Posted FC Credit Status */
#define PCIE_PL_TCFCCS                   	(0x700 + 0x38) /* Transmit Completion FC Credit Status */
#define PCIE_PL_QS                       	(0x700 + 0x3c) /* Queue Status */

#define	PCIE_LINK_STATUS					 0x12
#define PCIE_INT_PIN						 0x3d

#define APP_LOGIC_PCI_INTR_CTRL                   0x64
#define APP_LOGIC_PCI_INTR_ST                     0x60
#define APP_LOGIC_PCI_INT_TIMEOUT_ST              0x6c
#define APP_LOGIC_PCI_VENDOR_MSG_CFG_0            0x10
#define APP_LOGIC_PCI_VENDOR_MSG_CFG_1            0x14
#define APP_LOGIC_PCI_VENDOR_MSG_DATA_0           0x18
#define APP_LOGIC_PCI_VENDOR_MSG_DATA_1           0x1c
#define APP_LOGIC_LTSSM                           0		/* write (orig_data | 4) to enable ltssm */
#define APP_LOGIC_LTSSM_STATUS_H                  0x28 	/* cxpl_debug_info[63:0] */
#define APP_LOGIC_LTSSM_STATUS_L                  0x24
#define APP_LOGIC_PCI_DEVICE_TYPE_SEL             0x04
#define LTSSM_STATE_MASK                          0x3f
#define LTSSM_L0                                  0x11
#define APP_LOGIC_MIN_CFG                    	  0x8


#define PCIE_BDF(b,d,f)  ( (b << 8) | (d << 3) | (f)  ) /* start from bit 0 */
//#define PCIE_BDF(b,d,f)  ( ((b & 0xff) << 8) | ((d & 0x1f) << 3) | (f & 0x7)  )

#define IATU_IB_REGION_COUNT    3
#define IATU_OB_REGION_COUNT    3
#define IATU_REGION_INBOUND     1
#define IATU_REGION_OUTBOUND    0
#define IATU_BAR_MATCH_MODE     1
#define IATU_ADDRESS_MATCH_MODE 0

#define IATU_OB_CFG_INDEX	0
#define IATU_OB_MEM_INDEX	1
#define IATU_OB_IO_INDEX	2


typedef struct hr2dwcPciDrvCtrl 
{
	//VXB_DEVICE_ID pInst;
	void * appBase;		/* application register base */
	void * cfgBase;		/* config and port logic register space base */
	void * appHandle;	/* application register handle */
	void * cfgHandle;	/* config and port logic register space handle */
	int pciMaxBus; /* Max number of sub-busses */

	void * mem32Addr;
	unsigned int mem32Size;
	void * memIo32Addr;
	unsigned int memIo32Size;
	void * io32Addr;
	unsigned int io32Size;

	void *ibMemAxi32Addr;
	void *ibMemPci32Addr;
	unsigned int ibMemSize;
	void *ibIoAxi32Addr;
	void *ibIoPci32Addr;
	unsigned int ibIoSize;
	void *ibBar0AxiAddr;
	void *ibBar4AxiAddr;

	unsigned int pciExpressHost;
	unsigned int msiEnable;
	unsigned int autoConfig;

	int initDone;
	int bridgeInitFailed;
	struct vxbPciConfig *pPciConfig;
	struct vxbPciInt *pIntInfo;
	struct hcfDevice * pHcf;
//	spinlockIsr_t spinlockIsr; /* ISR-callable spinlock */
	
	unsigned char curr_cfg_type;
} HR2DWCPCI_DRV_CTRL;

struct hrPcieTransDesc
{
	unsigned int sign; 		/* 标示该项是否被处理过 1：未处理；0：已处理过 */
	unsigned int transMode; /* 标示传输类型，0：DMA读；1：DMA写*/
	unsigned int localAddr;
	unsigned int remoteAddr;
	unsigned int transSize;
	PFUNC_PCIE_CBACK callback;
};

#define APP_BAR(p)          ((HR2DWCPCI_DRV_CTRL *)(p)->pDrvCtrl)->appBase
#define APP_HANDLE(p)		((HR2DWCPCI_DRV_CTRL *)(p)->pDrvCtrl)->appHandle

#define PCIE_APP_REG_GET(offset)          \
		*(volatile unsigned int *)(PCIE_APP_BASE_ADDR + offset)
#define PCIE_APP_REG_SET(offset, data)   \
		*(volatile unsigned int *)(PCIE_APP_BASE_ADDR + offset) = data

//#define PCIE_APP_REG_GET(offset) \
//		phx_read_u32(PCIE_X16_LINK0_MISC_BASE_ADDR + offset) /*ldf 20230407*/
//#define PCIE_APP_REG_SET(offset, data)   \
//		phx_write_u32((PCIE_X16_LINK0_MISC_BASE_ADDR + offset), data) /*ldf 20230407*/


		
#define PCIE_CFG_REG_GET_1B(offset)          \
		*(volatile char *)(PCIEX16_CFG0_BASE_ADDR + offset)

#define PCIE_CFG_REG_SET_1B(offset, data)   \
		*(volatile char *)(PCIEX16_CFG0_BASE_ADDR + offset) = data

#define PCIE_CFG_REG_GET_2B(offset)          \
		*(volatile unsigned short int *)(PCIEX16_CFG0_BASE_ADDR + offset)

#define PCIE_CFG_REG_SET_2B(offset, data)   \
		*(volatile unsigned short int *)(PCIEX16_CFG0_BASE_ADDR + offset) = data

#define PCIE_CFG_REG_GET_4B(offset)          \
		*(volatile unsigned int *)(PCIEX16_CFG0_BASE_ADDR + offset)

#define PCIE_CFG_REG_SET_4B(offset, data)   \
		*(volatile unsigned int *)(PCIEX16_CFG0_BASE_ADDR + offset) = data

/*added by syj begin 20140730*/
#define  PCI_COMMAND		0x04	/* 16 bits register*/
#define  PCI_COMMAND_IO		0x1	/* Enable response in I/O space */
#define  PCI_COMMAND_MEMORY	0x2	/* Enable response in Memory space */
#define  PCI_COMMAND_MASTER	0x4	/* Enable bus mastering */
/*added by syj end 20140730*/

#define HRPCIE_VXBNAME "hr3cdnsPci"




#endif
