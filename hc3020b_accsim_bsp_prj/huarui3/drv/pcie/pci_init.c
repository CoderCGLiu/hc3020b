#include <reworks/types.h>
#include <pci/pcibus.h>
#include <reworks/printk.h>
#include "hr3cdnsPci.h"


#define PCI_ACCESS_READ  0
#define PCI_ACCESS_WRITE 1

#define PCIBIOS_SUCCESSFUL		0
#define PCIBIOS_DEVICE_NOT_FOUND	-1

#define HT1LO_PCICFG_BASE_lyj_flag      0
#define HT1LO_PCICFG_BASE_TP1_lyj_flag  1
#define BCM1250_PCI_MAKE_TAG(b,d,f) (((b) << 16) | ((d) << 11) | ((f) << 8))

typedef struct
{
	u8 dev_no;
	u8 func_no;
	u8 intline;
} pci_res;


#define BONITO_PCICFG0_BASE_VA          (0x9000000000000000|PCIEX16_CFG0_BASE_ADDR)//0xba000000 
#define BONITO_PCICFG1_BASE_VA          (0x9000000000000000|PCIEX16_CFG1_BASE_ADDR)//0xbb000000 
 
#define HT_MAP_TYPE0_CONF_ADDR  BONITO_PCICFG0_BASE_VA
#define HT_MAP_TYPE1_CONF_ADDR  BONITO_PCICFG1_BASE_VA


#if 0
u8 _pci_conf_read8(u32 tag,int reg);
u16 _pci_conf_read16(u32 tag,int reg);

/*添加的宏定义*/
#define LS2H_PCIE_PORT0 0
#define ls2h_readl(addr)		(*(volatile unsigned int *)CKSEG1ADDR(addr))
#define ls2h_writel(val, addr)		*(volatile unsigned int *)CKSEG1ADDR(addr) = (val)
#if 1
#define CKSEG1ADDR(a)		(CPHYSADDR(a) | CKSEG1)
#define CKSEG1				0xa0000000
#define CPHYSADDR(a)		((a) & 0x1fffffff)
#else
#define CKSEG1ADDR(a)		(CPHYSADDR(a) | KSEG1)
#define KSEG1				0xa0000000
#define CPHYSADDR(a)		((a) & 0x1fffffff)
#endif

//#define LS2H_PCIE_PORT_HEAD_BASE_PORT(portnum)  (0x1A004800 + (portnum << 22))
//#define LS2H_PCIE_IO_BASE_PORT(portnum)			(0x18100000 + (portnum << 22))
//#define LS2H_PCIE_DEV_HEAD_BASE_PORT(portnum)   (0x18116000 + (portnum << 22))
//#define LS2H_PCIE_REG_BASE_PORT(portnum)        (0x102D1000 + (portnum << 22))
//#define LS2H_PCIE_PORT_REG_STAT1				(0xC)
//#define LS2H_PCIE_REG_STAT1_BIT_LINKUP			(1 << 6)

/*
 * Base addresses specify locations in memory or I/O space.
 * Decoded size can be determined by writing a value of
 * 0xffffffff to the register, and reading it back.  Only
 * 1 bits are decoded.
 */
#define PCI_BASE_ADDRESS_0	0x10	/* 32 bits */
#define PCI_BASE_ADDRESS_1	0x14	/* 32 bits [htype 0,1 only] */
#define PCI_BASE_ADDRESS_2	0x18	/* 32 bits [htype 0 only] */
#define PCI_BASE_ADDRESS_3	0x1c	/* 32 bits */
#define PCI_BASE_ADDRESS_4	0x20	/* 32 bits */
#define PCI_BASE_ADDRESS_5	0x24	/* 32 bits */

#define LIE_IN_WINDOW(addr,base,mask)   ((addr & mask) == base)
#define MAP_2_WINDOW(addr,mmap,mask)    ((addr & (~(mask))) | (mmap & mask))
#define LS2H_PCIE_MEM0_DOWN_BASE		0x10000000
#define LS2H_PCIE_MEM0_DOWN_MASK		0xf8000000
#define LS2H_PCIE_MEM0_DOWN_MASK		0xf8000000
#define LS2H_PCIE_MEM0_UP_BASE			0x10000000
#define LS2H_PCIE_MEM0_UP_MASK			0xfe000000
#define LS2H_PCIE_IO_DOWN_BASE			0x18100000
#define LS2H_PCIE_IO_DOWN_MASK			0xff3f0000
#define LS2H_PCIE_IO_UP_BASE			0x0
#define LS2H_PCIE_IO_UP_MASK			0xffff0000
#define LS2H_PCIE_MEM0_BASE_PORT(portnum)	(0x10000000 + (portnum << 25))
#define PCI_BASE_ADDRESS_SPACE_IO	0x01

#ifdef CONFIG_LS2H_SB
#define LS2H_IO_REG_BASE		0x1b000000
#else
#define LS2H_IO_REG_BASE		0x1f000000
#endif

#define LS2H_CHIP_CFG_REG_BASE		(LS2H_IO_REG_BASE + 0x00d00000)
#define LS2H_CLK_CTRL3_REG		(LS2H_CHIP_CFG_REG_BASE + 0x022c)
#define LS2H_CLK_CTRL3_BIT_PEREF_EN(portnum) (1 << (24 + portnum))

#define LS2H_PCIE_PORT_REG_CTR0			0x0
#define PCI_CLASS_REVISION	0x08	/* High 24 bits are class, low 8 revision */
#define PCI_CLASS_BRIDGE_PCI		0x0604

#define LS2H_PCIE_PORT_REG_CTR_STAT		0x28
#define LS2H_PCIE_REG_CTR_STAT_BIT_ISRC			(1 << 27)
#define LS2H_PCIE_REG_CTR_STAT_BIT_ISX4			(1 << 26)

/*函数声明*/
int ls2h_pci_pcibios_read(int bus,int dev,int func,int where,int size,u32 *pResult);
int ls2h_pci_pcibios_write(int bus,int dev,int func,int where,int size,u32 data);
int ls2h_pcibios_read(int bus, int dev,int func,int where,int size,u32 *pResult);
int ls2h_pcibios_write(int bus, int dev,int func,int where,int size,u32 pResult);
int ls2h_pci_config_access(unsigned char access_type,int bus, int dev, int func,int where, u32 * data);
int is_link_up(unsigned char port);
u32 ls2h_pcie_bar_translate(unsigned char access_type, u32 bar_in,unsigned char portnum);
void en_ref_clock(void);
void ls2h_pcie_port_init(int port);
int is_rc_mode(void);
int is_x4_mode(void);
#endif



#if 0
unsigned int _pci_conf_readn(int tag, int reg, int width)
{
    int bus, device, function;

	if(width==1) return _pci_conf_read8(tag,reg); 
	else if(width==2) return _pci_conf_read16(tag,reg); 

    if ((width != 4) || (reg & 3) || reg < 0 || reg >= 0x100) 
    {
    	printk("_pci_conf_readn: bad reg 0x%x, tag 0x%x, width 0x%x\n", reg, tag, width);
    	return ~0;
    }

    _pci_break_tag (tag, &bus, &device, &function); 
    if (bus == 0) 
    {
    	/* Type 0 configuration on onboard PCI bus */
    	if (device > 31 || function > 7)
    	{	
    		printk("_pci_conf_readn: bad device 0x%x, function 0x%x\n", device, function);
    	    return ~0;		/* device out of range */
    	}
    	return pci_read_type0_config32(device, function, reg);
    }
    else 
    {
    	/* Type 1 configuration on offboard PCI bus */
    	if (bus > 255 || device > 31 || function > 7)
    	{	
    		printk("_pci_conf_readn: bad bus 0x%x, device 0x%x, function 0x%x\n", bus, device, function);
    	    return ~0;		/* device out of range */
    	}
    	return pci_read_type1_config32(bus, device, function, reg);
    }
}

u32 _pci_conf_read(u32 tag,int reg)
{
	return _pci_conf_readn(tag,reg,4);
}

void _pci_conf_writen(u32 tag, int reg, u32 data,int width)
{
    int bus, device, function;
    u32 ori;
    u32 mask = 0x0;

    if ((reg & (width -1)) || reg < 0 || reg >= 0x100) 
    {
    	printk("_pci_conf_writen: bad reg 0x%x, tag 0x%x, width 0x%x\n", reg, tag, width);
    	return;
    }

    _pci_break_tag (tag, &bus, &device, &function);

    if (bus == 0) 
    {
    	/* Type 0 configuration on onboard PCI bus */
    	if (device > 31 || function > 7)
    	{	
		        printk("_pci_conf_writen: bad device 0x%x, function 0x%x\n", device, function);
    	    	return;		/* device out of range */
	    }
    }
    else 
    {
    	/* Type 1 configuration on offboard PCI bus */
    	if (bus > 255 || device > 31 || function > 7)
    	{	
    		printk("_pci_conf_writen: bad bus 0x%x, device 0x%x, function 0x%x\n", bus, device, function);
    	    return;		/* device out of range */
    	}
    }
    ori = _pci_conf_read(tag, reg & 0xfc);
    if(width == 2)
    {
        if(reg & 2)
        {
            mask = 0xffff;
        }
        else
        {
            mask = 0xffff0000;
        }
    }
    else if(width == 1)
    {
        if ((reg & 3) == 1) 
        {
    	  mask = 0xffff00ff;
    	}
        else if ((reg & 3) == 2) 
    	{
    	  mask = 0xff00ffff;
    	}
    	else if ((reg & 3) == 3) 
    	{
    	  mask = 0x00ffffff;
    	}
    	else
    	{
    	  mask = 0xffffff00;
    	}
    }
    data = data << ((reg & 3) * 8);
    data = (ori & mask) | data;
    
    if (bus == 0) 
    {
    	return pci_write_type0_config32(device, function, reg & 0xfc, data);
    }
    else 
    {
    	return pci_write_type1_config32(bus, device, function, reg & 0xfc, data);
    }
}

u32 _pci_conf_read32(u32 tag,int reg)
{
	return _pci_conf_readn(tag,reg,4);
}

u8 _pci_conf_read8(u32 tag,int reg)
{
	u32 data;
	u32 offset;
	u32 new_reg;
	
	new_reg = reg & 0xfc;
	data = _pci_conf_readn(tag,new_reg,4);
	offset = reg & 3;
	data = data >> (offset * 8);
	data &= 0xff;
	
	return (u8)data;
}

u16 _pci_conf_read16(u32 tag,int reg)
{
	u32 data;
	u32 offset;
	u32 new_reg;
	
	new_reg = reg & 0xfc;
	data = _pci_conf_readn(tag,new_reg,4);
	offset = reg & 2;
	data = data >> (offset << 3);
	data &= 0xffff;
	
	return (u16)data;
}

void _pci_conf_write(u32 tag, int reg, u32 data)
{
	return _pci_conf_writen(tag,reg,data,4);
}

void _pci_conf_write32(u32 tag, int reg, u32 data)
{
	return _pci_conf_writen(tag,reg,data,4);
}

void _pci_conf_write8(u32 tag, int reg, u8 data)
{
	return _pci_conf_writen(tag,reg,data,1);
}

void _pci_conf_write16(u32 tag, int reg, u16 data)
{
	return _pci_conf_writen(tag,reg,data,2);
}
#endif


void _pci_break_tag(u32 tag, u32 *busp, u32 *devicep, u32 *functionp)
{
	if (busp) 
	{
		*busp = (tag >> 16) & 255;
	}
	if (devicep)
	{
		*devicep = (tag >> 11) & 31;
	}
	if (functionp) 
	{
		*functionp = (tag >> 8) & 7;
	}
}

u32 _pci_make_tag(u32 bus, u32 device, u32 function)
{
	u32 tag;

	tag = (bus << 16) | (device << 11) | (function << 8);
	return(tag);
}

/*
 * read pci config header type0
 */
u32 pci_read_type0_config32(u32 dev, u32 func, u32 reg)
{
    u64 addr = HT_MAP_TYPE0_CONF_ADDR;
    u32 tag = _pci_make_tag(0, dev, func);
 
    addr |= (tag | reg);
    
    return *((u64 *) addr);
}

/*
 * write pci config header type0
 */
void pci_write_type0_config32(u32 dev, u32 func, u32 reg, u32 val)
{
	u64 addr = HT_MAP_TYPE0_CONF_ADDR;
	u32 tag = _pci_make_tag(0, dev, func);
    
    addr |= (tag | reg);
    
    *((u64 *) addr) = val; 
}

/*
 * read pci config header type1
 */
u32 pci_read_type1_config32(u32 bus, u32 dev, u32 func, u32 reg)
{
	u64 addr = HT_MAP_TYPE1_CONF_ADDR;
	u32 tag = _pci_make_tag(bus, dev, func);
    
    addr |= (tag | reg);
    
    return *((u64 *) addr);
}

/*
 * write pci config header type1
 */
void pci_write_type1_config32(u32 bus, u32 dev, u32 func, u32 reg, u32 val)
{
	u64 addr = HT_MAP_TYPE1_CONF_ADDR;
	u32 tag = _pci_make_tag(bus, dev, func);
    
    addr |= (tag | reg);
    
    *((u64 *) addr) = val;
}



int hr3_pci_config_access(unsigned char access_type,int bus, int dev, int func,int where, u32 * data)
{
	unsigned int addr, type;
	unsigned int addr_i, cfg_addr;
	u32 datarp;
	int device = dev;
	int function = func;
	int reg = where & ~3;
//	unsigned char need_bar_translate = 0;
	
	if (access_type == 1)	//write
	{
		if(bus != 0)
		{
			pci_write_type1_config32(bus, dev, func, reg, *data);
		}
		else
		{
			pci_write_type0_config32(dev, func, reg, *data);
		}
	}
	else		//read
	{
		if(bus != 0)
		{
			*data = pci_read_type1_config32(bus, dev, func, reg);
		}
		else
		{
			*data = pci_read_type0_config32(dev, func, reg);
		}
	}
	
	return 0;
}

int hr3_pcibios_read(	int bus,
						int dev,
						int func,
						int where,
						int size,
						u32 *pResult)
{
	u32 data = 0;

	if (hr3_pci_config_access(0, bus, dev, func, where,&data))
		return -1;
	if (size == 1)
		*pResult = (data >> ((where & 3) << 3)) & 0xff;
	else if (size == 2)
		*pResult = (data >> ((where & 3) << 3)) & 0xffff;
	else
		*pResult = data;
	return 0;
}

int hr3_pcibios_write(int bus,
					   int dev,
					   int func,
					   int where,
					   int size,
					   u32 pResult)
{
	u32 data = 0;
	
	if (size == 4)
	{
		data = pResult;
	}
	else 
	{
		if (hr3_pci_config_access(0, bus, dev, func, where,&data))
		{
			return -1;
		}
		if (size == 1)
		{
			data = (data & ~(0xff << ((where & 3) << 3))) |(pResult << ((where & 3) << 3));
		}
		else if (size == 2)
		{
			data = (data & ~(0xffff << ((where & 3) << 3))) |(pResult << ((where & 3) << 3));
		}
	}
	
	if (hr3_pci_config_access(1, bus, dev, func, where, &data))
	{
		return -1;
	}
	return 0;
}


/*
 * pci 配置读 注册函数
 */
int sys_pci_config_read
(
     int bus,
     int dev,
     int func,
     int reg,
     int size,
     void *pResult
)
{
#if 0
    u32 tag = BCM1250_PCI_MAKE_TAG (bus, dev, func);

    switch (size)
        {
        case 1:
            *(u8  *) pResult = _pci_conf_read8(tag,reg);
            break;
        case 2:
            *(u16 *) pResult =  _pci_conf_read16(tag,reg);
            break;
        case 4:
        default:
            *(u32 *) pResult =  _pci_conf_read32(tag,reg);
            break;
        }
    return 0;
#endif
    	
    return hr3_pcibios_read(bus,dev,func,reg,size,pResult);	
}

/*
 * pci 配置写 注册函数
 */
int sys_pci_config_write(int bus,
    int dev,
    int func,
    int reg,
    int size,
    u32 data)
{
#if 0
	u32 tag = BCM1250_PCI_MAKE_TAG (bus, dev, func);

    switch (size)
    {
        case 1:
            _pci_conf_write8 (tag, reg,(u8) data);
            break;
        case 2:
            _pci_conf_write16 (tag,  reg,(u16) data);
            break;
        case 4:
        default:
            _pci_conf_write32 (tag, reg, data);
            break;
    }
    return 0;
#endif
    
    return hr3_pcibios_write(bus,dev,func,reg,size,data);
}


#if 0
void pci_module_init1()
{
#if 0
	int i;
	pci_config_init(0, (int)sys_pci_config_read, (int)sys_pci_config_write, 0);
    extern void sys_pci_auto_config(); 
    sys_pci_auto_config();
#endif
}

static int loongson3a_pci_config_access_lyj(unsigned char access_type,
					  unsigned int busnum, unsigned int device,unsigned int function,
					  int where, unsigned int *data)
{
	unsigned long long addr, type;
	unsigned int addr32;
	unsigned char lyj_flag1;

	int reg = where & ~3;

	if (busnum == 0) 
	{
	  /* Type 0 configuration on onboard PCI bus */
		if (device > 20 || function > 7) 
		{
	 			*data = -1;	/* device out of range */
				return PCIBIOS_DEVICE_NOT_FOUND;
		}
		addr32 = (device << 11) | (function << 8) | reg;
		type = 0;

		addr32=addr32 & 0xffff;

		lyj_flag1=HT1LO_PCICFG_BASE_lyj_flag;

	} 
	else 
	{
	   /* Type 1 configuration on offboard PCI bus */
	    if (busnum > 255 || device > 31 || function > 7) 
	    {
				*data = -1;	/* device out of range */
		        return PCIBIOS_DEVICE_NOT_FOUND;
		}
		addr32 = (busnum << 16) | (device << 11) | (function << 8) | reg;
		type = 0x10000;

		lyj_flag1=HT1LO_PCICFG_BASE_TP1_lyj_flag;
	}

	/* clear aborts */

	if (access_type == PCI_ACCESS_WRITE)
	{
		if(lyj_flag1==HT1LO_PCICFG_BASE_lyj_flag)
		{
			MIPS_SW64_lyj_HT1LO_PCICFG_BASE(addr32, *data);
		}
		else if(lyj_flag1==HT1LO_PCICFG_BASE_TP1_lyj_flag)
		{
			MIPS_SW64_lyj_HT1LO_PCICFG_BASE_TP1(addr32, *data);
		}
	}
	else 
	{
		if(lyj_flag1==HT1LO_PCICFG_BASE_lyj_flag)
		{
			*data = MIPS_LW64_lyj_HT1LO_PCICFG_BASE(addr32);
		}
		else if(lyj_flag1==HT1LO_PCICFG_BASE_TP1_lyj_flag)
		{
			*data = MIPS_LW64_lyj_HT1LO_PCICFG_BASE_TP1(addr32);
		}
        if (*data == 0xffffffff)
        {
            *data = -1;
	        return PCIBIOS_DEVICE_NOT_FOUND;
        }
	}

	return PCIBIOS_SUCCESSFUL;

}

static int loongson3a_pci_pcibios_write_lyj(unsigned int busnum, unsigned int device,unsigned int function,
				  int where, int size, unsigned int val)
{
	unsigned int data = 0;
	int ret;

	if (size == 4)
		data = val;
	else {
		ret = loongson3a_pci_config_access_lyj(PCI_ACCESS_READ,
				busnum, device, function, where, &data);
		if (ret != PCIBIOS_SUCCESSFUL)
			return ret;

		if (size == 1)
			data = (data & ~(0xff << ((where & 3) << 3))) |
			    (val << ((where & 3) << 3));
		else if (size == 2)
			data = (data & ~(0xffff << ((where & 3) << 3))) |
			    (val << ((where & 3) << 3));
	}

	ret = loongson3a_pci_config_access_lyj(PCI_ACCESS_WRITE,
			busnum, device, function, where, &data);
	if (ret != PCIBIOS_SUCCESSFUL)
		return ret;

	return PCIBIOS_SUCCESSFUL;
}

static int loongson3a_pci_pcibios_read_lyj(unsigned int busnum, unsigned int device,unsigned int function,
				 int where, int size, unsigned int * val)
{
	unsigned int data = 0;

	int ret = loongson3a_pci_config_access_lyj(PCI_ACCESS_READ,
			busnum, device, function, where, &data);

	if (ret != PCIBIOS_SUCCESSFUL)
		return ret;

	if (size == 1)
		*val = (data >> ((where & 3) << 3)) & 0xff;
	else if (size == 2)
		*val = (data >> ((where & 3) << 3)) & 0xffff;
	else
		*val = data;

	return PCIBIOS_SUCCESSFUL;
}

int Test_pcicfg_write_lyj(unsigned int busnum, unsigned int device,unsigned int function,
				  int where, int size, unsigned int val)
{
	unsigned int data = 0;
	int ret=0;

	data=val;

	ret=loongson3a_pci_pcibios_write_lyj(busnum,device,function,where,size,data);

	return ret;
}

int Test_pcicfg_read_lyj(unsigned int busnum, unsigned int device,unsigned int function,
				 int where, int size)
{
	unsigned int data = 0;

	loongson3a_pci_pcibios_read_lyj(busnum,device,function,where,size,&data);

	return data;
}


/* PCI extension registers */
u32 _pci_ext_conf_read(int bus,int dev,int func, u32 reg)
{
	u32  addr;
	u32 bar3_tag = _pci_make_tag(0, 0, 0);

	addr = (_pci_conf_read(bar3_tag, 0x1c)  & ~(0x0f))| 0xb0000000;
	addr |= bus << 20 | dev << 15 | func << 12 | reg;
	//printk("addr=0x%x,bus=%x,dev=%x, fn=%x\n", addr, bus, dev, func);
	return *((volatile u32 *) addr);
}


int ls2h_pci_pcibios_read(int bus,
						  int dev,
						  int func,
						  int where,
						  int size,
						  u32 *pResult)
{
	return ls2h_pcibios_read(bus, dev, func, where, size, pResult);
}
#endif



#if 0
int ls2h_pci_pcibios_write(int bus,
						   int dev,
						   int func,
						   int where,
						   int size,
						   u32 data)
{
	return ls2h_pcibios_write(bus, dev, func, where, size, data);
}
#endif




#if 0
int is_link_up(unsigned char port)
{
	u32 reg, data;

	reg = LS2H_PCIE_REG_BASE_PORT(port) | LS2H_PCIE_PORT_REG_STAT1;
	data = ls2h_readl(reg);

	return data & LS2H_PCIE_REG_STAT1_BIT_LINKUP;
}

u32 ls2h_pcie_bar_translate(unsigned char access_type, u32 bar_in,unsigned char portnum)
{

	static unsigned char tag_mem = 0, tag_io = 0;

	if (portnum > 3)
		return bar_in;

	if ((access_type == 1) && LIE_IN_WINDOW(bar_in,
				LS2H_PCIE_MEM0_DOWN_BASE,
				LS2H_PCIE_MEM0_DOWN_MASK))
		return MAP_2_WINDOW(bar_in, LS2H_PCIE_MEM0_UP_BASE,
				    LS2H_PCIE_MEM0_UP_MASK);

	if ((access_type == 0) && LIE_IN_WINDOW(bar_in,
				LS2H_PCIE_MEM0_UP_BASE,
				LS2H_PCIE_MEM0_UP_MASK)) {
		if (tag_mem)
			return MAP_2_WINDOW(bar_in,
					    LS2H_PCIE_MEM0_BASE_PORT(portnum),
					    LS2H_PCIE_MEM0_UP_MASK);
		else {
			tag_mem = 1;
			return bar_in;
		}
	}

	if ((access_type == 1) && LIE_IN_WINDOW(bar_in,
				LS2H_PCIE_IO_DOWN_BASE,
				LS2H_PCIE_IO_DOWN_MASK))
		return MAP_2_WINDOW(bar_in, LS2H_PCIE_IO_UP_BASE,
				    LS2H_PCIE_IO_UP_MASK);

	if ((access_type == 0)
		&& LIE_IN_WINDOW(bar_in, LS2H_PCIE_IO_UP_BASE,
			LS2H_PCIE_IO_UP_MASK)
		&& (bar_in & PCI_BASE_ADDRESS_SPACE_IO)) {
		if (tag_io)
			return MAP_2_WINDOW(bar_in,
					    LS2H_PCIE_IO_BASE_PORT(portnum),
					    LS2H_PCIE_IO_UP_MASK);
		else 
		{
			tag_io = 1;
			return bar_in;
		}
	}

	return bar_in;
}

void en_ref_clock(void)
{
	unsigned int data;

	data = ls2h_readl(LS2H_CLK_CTRL3_REG);
	data |= (LS2H_CLK_CTRL3_BIT_PEREF_EN(0)
			|LS2H_CLK_CTRL3_BIT_PEREF_EN(1)
		    |LS2H_CLK_CTRL3_BIT_PEREF_EN(2)
		    |LS2H_CLK_CTRL3_BIT_PEREF_EN(3));
	ls2h_writel(data, LS2H_CLK_CTRL3_REG);
}

void ls2h_pcie_port_init(int port)
{
//	unsigned reg, data;

//	reg = LS2H_PCIE_PORT_HEAD_BASE_PORT(port)|0x01<<8| 0x7c;
//	data = ls2h_readl(reg) & (~0xf);
//	data |=1;
//	ls2h_writel(data, reg);

//	printk("\nls2h_pcie_port_init \n");
//	
//	reg = 0xB0801000| LS2H_PCIE_PORT_REG_CTR0;
//	ls2h_writel(0xff204c, reg);
//
//	reg = LS2H_PCIE_PORT_HEAD_BASE_PORT(port)| PCI_CLASS_REVISION;
//	printk("classcode = 0x%x \n",ls2h_readl(reg));
//	
//	data =  ls2h_readl(reg) & 0xffff;
//	printk("pre data = 0x%x \n",data);
//	
//	data |= (PCI_CLASS_BRIDGE_PCI << 16);//初始化为PCI桥
//	printk("now data = 0x%x \n",data);
//	
//	reg = LS2H_PCIE_PORT_HEAD_BASE_PORT(port)|0x01<<8 | PCI_CLASS_REVISION;
//	printk("reg = 0x%x \n",reg);
//	
//	pci_write_type0_config32(9, 1, PCI_CLASS_REVISION, data);
	
	//pci_write_type0_config32(9, 1, 0x08, 0x06040001);
}

/*以下为调试用接口
 * int is_rc_mode(void)		检查pcie是否工作为RC模式
 * int is_x4_mode(void)		检查pcie是否工作为X4模式
 * int read_pcie_control_reg(unsigned int offset)	读取pcie控制寄存器指定数据(0xb8118000 + offset的4字节数据)
 * void print_pcie_control_reg()					读取pcie控制寄存器全部数据(0xb8118000开始的全部0x80字节数据)
 * */

int is_rc_mode(void)
{
	unsigned data;

	data = ls2h_readl(LS2H_PCIE_REG_BASE_PORT(0)
			| LS2H_PCIE_PORT_REG_CTR_STAT);

	return data & LS2H_PCIE_REG_CTR_STAT_BIT_ISRC;
}

int is_x4_mode(void)
{
	unsigned data;

	data = ls2h_readl(LS2H_PCIE_REG_BASE_PORT(0)
			| LS2H_PCIE_PORT_REG_CTR_STAT);

	return data & LS2H_PCIE_REG_CTR_STAT_BIT_ISX4;
}

int read_pcie_control_reg(unsigned int offset)//读从0xb8118000 + offset开始的4字节数据并返回
{
	unsigned int data;

	data = ls2h_readl(LS2H_PCIE_REG_BASE_PORT(0)
			| offset);

	return data;
}

void print_pcie_control_reg()
{
	int offset = 0;
	int flag = 0;
	int data = 0;
	for(; offset < 0x80; offset += 4)
	{
		data = 0;
		data = read_pcie_control_reg(offset);
		//printf("%4x ",data);
		printk("%8x:%8x\n",LS2H_PCIE_REG_BASE_PORT(0)| offset,data);
		flag = (flag + 1) % 4;
		if(flag == 0)
		{
			//printf("\n");	
			printk("\n");
		}
	}
}
#endif
