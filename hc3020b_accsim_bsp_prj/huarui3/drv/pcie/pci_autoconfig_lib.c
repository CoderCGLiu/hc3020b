
/* includes */


#include <reworks/types.h>
#include <clock.h>
#include <pci/pcibus.h>
#include <drv/pci/pciConfigLib.h>
#include "pci_autoconfig_lib.h"

/* 局部变量定义 */

#define NO_ALLOCATION  0xffffffff

#define PCI_CONFIG_ABSENT_F 0xffff
#define PCI_CONFIG_ABSENT_0 0x0000


#define PCI_AUTO_STATIC_LIST

#undef PCI_AUTO_RECLAIM_LIST


//FUNCPTR _func_logMsg;

//#define PCI_AUTO_DEBUG

#ifdef PCI_AUTO_DEBUG
int pci_auto_debug = TRUE;
#else
int pci_auto_debug = FALSE;
#endif

/* 
 * Iying 2021-11-22 : add a log message funtion
 *
 * It saves PCI-enumerate log to memory, and can be printed after system is ready.
 */
#ifdef PCI_AUTO_DEBUG

static int pciLog_i = 0;
static int looptime = 0;
static unsigned char ringBuf[500][128];
void pciLog(char *fmt, int arg1, int arg2,int arg3, int arg4, int arg5, int arg6)
{

	snprintf(&ringBuf[pciLog_i],128, fmt, arg1,arg2,arg3,arg4,arg5,arg6);

	if(pciLog_i == 500)
	{
		pciLog_i = 0;
		looptime ++ ;
	}
	else
		pciLog_i++;

	printf_pciLog();
}

void printf_pciLog()
{
	int i = 0;
	for(i = 0; i<500;i++)
		printf("%s",&ringBuf[i]);

	if (looptime > 0)
		printf("log has overflown %d times\n",looptime);
}

#define PCI_AUTO_DEBUG_MSG(s, a, b, c, d, e, f)  pciLog(s,a,b,c,d,e,f)  
#else
#define PCI_AUTO_DEBUG_MSG(s, a, b, c, d, e, f)
#endif

//#define PCI_AUTO_DEBUG_MSG(s, a, b, c, d, e, f) \
//    { \
//    if ((pci_auto_debug == TRUE) && (_func_logMsg != NULL)) \
//        { \
//	(*_func_logMsg)(s, a, b, c, d, e, f); \
//        pthread_delay(10); \
//        } \
//    }

int pciMaxBus   = PCI_MAX_BUS;

/* locals */

#ifdef PCI_AUTO_STATIC_LIST
static PCI_LOC pciAutoLocalFuncList[PCI_AUTO_MAX_FUNCTIONS];
#endif

static int lastPciListSize;
static PCI_LOC *pLastPciList;

static u8 pci_auto_int_routing_table[4] = { (u8) 0xff,
		(u8) 0xff,
		(u8) 0xff,
		(u8) 0xff
};
/* 前置声明 */

static PCI_LOC * pci_auto_list_create (PCI_SYSTEM * pSystem, int *pListSize);
static u32 pci_auto_bus_probe ( PCI_SYSTEM * pSystem, u32 priBus,
		u32 secBus, PCI_LOC*  pPciLoc, PCI_LOC** ppPciList,
		int * pListSize);
static u32 pci_auto_dev_probe ( PCI_SYSTEM * pSystem, u32 bus,
		u8 offset, u8 inheritAttrib, PCI_LOC **ppPciList, int * pListSize,u32 preBus);
static void pci_auto_func_config_all ( PCI_SYSTEM * pSystem,
		PCI_LOC *pPciList, u32 nSize);
static u8 pci_auto_int_assign ( PCI_SYSTEM * pSystem, PCI_LOC * pFunc);
static void pci_auto_dev_config ( PCI_SYSTEM * pSystem, u32 bus,
		PCI_LOC **ppPciList, u32 *nSize);
static void pci_auto_func_config ( PCI_SYSTEM * pSystem, PCI_LOC * pPciFunc);
static u32 pci_auto_io_alloc ( PCI_SYSTEM * pPciSys, PCI_LOC *pPciFunc,
		u32 *pAlloc, u32 nSize);
static u32 pci_auto_mem_alloc ( PCI_SYSTEM * pPciSys, PCI_LOC * pPciFunc,
		u64 * pAlloc, u32 size, u32 addrInfo);
static void pci_auto_bus_config ( PCI_SYSTEM * pSystem, PCI_LOC * pPciLoc,
		PCI_LOC **ppPciList, u32 *nSize);
int ls2h_pcie_bios_init(int bus, int dev, int func);
void ls2h_pcie_to_pci_bridge_delay();
/******************************************************************************
 *
 * pci_auto_config - 配置pci设备头函数.
 *
 * 此函数在pci配置程序中的优先级最高:
 *
 * 输入：
 *	pSystem 要配置的pci设备结构体
 * 输出：
 *      无
 * 返回值：
 * 	    无
 */
struct pci_config_data {
	int bus;
	int dev;
	int func;
	int interrupt;
	int primary;
	int secondary;
	int subordinate;
	unsigned int mem_start;
	unsigned int mem_end;
	unsigned int io_start;
	unsigned int io_end;
#define PCI_DEV		0x1
#define PCI_BRIDGE	0x2
	int type;
}__attribute__((aligned(4)));

extern struct pci_config_data pci_config_array[];
void pci_auto_config
(	
		PCI_SYSTEM * pSystem	/* 要配置的pci设备结构体 */
)
{
	PCI_LOC* pPciList;		/* pci链表指针	*/
	PCI_LOC* pNewPciList;		/* pci链表指针	*/
	int listSize;		/* pci设备链表大小	*/   

	/* Input parameter sanity checking */

	if (pSystem == NULL)
	{
		return;
	}

	if (pSystem->pciRollcallRtn != NULL)
	{
		printk("pciRollcallRtn enter\n");
		while (TRUE)
		{
			pPciList = pci_auto_list_create(pSystem, &listSize);
			if (pSystem->pciRollcallRtn () == TRUE)
			{
				break;
			}
		}
	}

	pPciList = pci_auto_list_create(pSystem, &listSize);//扫描PCI设备,建立功能链表

	int controller = 0;
	int port = 0;
	int tmpListSize = listSize;

	for(controller = 0; controller < 1; controller++)
	{
		for(port = 0; port < 1; port++)
		{
			listSize = tmpListSize;
			pNewPciList = pPciList;//ls2k_filter_pci_list(controller,port,pPciList,&listSize);
			int i;

			if (NULL != pNewPciList)
			{
				PCI_AUTO_DEBUG_MSG("<==\n",0,0,0,0,0,0);
				for(i = 0; i < listSize; i++)
				{
					PCI_AUTO_DEBUG_MSG("pNewPciList [%d] [%d] [%d]\n",pNewPciList[i].bus,pNewPciList[i].device,pNewPciList[i].function,0,0,0);
				}
				PCI_AUTO_DEBUG_MSG("<==\n",0,0,0,0,0,0);

				//set start and size to right value.
#if 0 /*ldf 20230908 ignore*/
				int start =  pci_config_array[0x9+controller*4 + port].mem_start;
//				int size  =  pci_config_array[0x9+controller*4 + port].mem_end-pci_config_array[0x9+controller*4+port].mem_start; 
				int size  =  pci_config_array[0x9+controller*4 + port].mem_end-pci_config_array[0x9+controller*4+port].mem_start +1;  
				pSystem->pciMemIo32 = start;
				pSystem->pciMemIo32Size =  size;
//				pSystem->pciIo16 = pci_config_array[0x9+controller*4 + port].io_start+1; 
				pSystem->pciIo16 = pci_config_array[0x9+controller*4 + port].io_start; 
				pSystem->pciIo16Size = pci_config_array[0x9+controller*4 + port].io_end-pci_config_array[0x9+controller*4+port].io_start+1;
#endif
				pci_auto_func_config_all(pSystem, pNewPciList, listSize);	
			}
		}
	}

	//pci_auto_func_config_all(pSystem, pPciList, listSize);//自动分配地址空间

	lastPciListSize = listSize;
	pLastPciList = pPciList;

#if defined(PCI_AUTO_RECLAIM_LIST)
#   if defined(PCI_AUTO_STATIC_LIST)
#       error "Can't do PCI_AUTO_RECLAIM_LIST with PCI_AUTO_STATIC_LIST"
#   endif
	free(pPciList);

	lastPciListSize = 0;
	pLastPciList = NULL;
#endif   

}

/******************************************************************************
 *
 * pci_auto_list_create - 本函数用于检查所有的功能并生成一个pci链表.
 *
 * 输入：
 *	pSystem 要配置的pci设备结构体
 *	pListSize 表的大小的指针 
 * 输出：
 *      无
 * 返回值：
 * 	    指向新的pci设备链表的指针
 */
static PCI_LOC * pci_auto_list_create
(
		PCI_SYSTEM * pSystem,
		int *pListSize
)
{
	PCI_LOC  pciLoc;		/* PCI bus/device/function 结构体 */
	PCI_LOC *pPciList;
	PCI_LOC *pRetPciList;

	/* 初始化链表指针 */

#if defined(PCI_AUTO_STATIC_LIST)
	pPciList = pciAutoLocalFuncList;
	pRetPciList = pPciList;
#else
	pPciList = malloc(sizeof(PCI_LOC) *  PCI_AUTO_MAX_FUNCTIONS);
	if (pPciList == NULL)
	{
		return NULL;
	}
	pRetPciList = pPciList;
#endif

	lastPciListSize = 0;
	*pListSize = 0;

	pciLoc.bus = (u8)0;
	pciLoc.device = (u8)0;
	pciLoc.function = (u8)0;

	printk("pci_auto_dev_probe enter \n");
	pciMaxBus = pci_auto_dev_probe (pSystem, pciLoc.bus, (u8)0,
			(u8)(PCI_AUTO_ATTR_BUS_4GB_IO |
					PCI_AUTO_ATTR_BUS_PREFETCH),
					&pPciList, pListSize,-1);
	printk("pci_auto_dev_probe leave \n");

	pSystem->maxBus = pciMaxBus;

	return (pRetPciList);
}

/******************************************************************************
 *
 * pci_auto_dev_reset - 本函数用于重启pci设备并重置可写入状态位.
 *
 * 输入：
 *	pPciLoc 要重启的设备
 * 输出：
 *      无
 * 返回值：
 * 	    ok
 */
int pci_auto_dev_reset(PCI_LOC * pPciLoc)
{
	pci_write_config_dword (pPciLoc->bus, pPciLoc->device,
			pPciLoc->function,
			PCI_CFG_COMMAND, 0xffff0000);
	return OK;
}

/******************************************************************************
 *
 * pci_auto_bus_number_set - 本函数用于为一个设备设置总线号.包括primary，
 * secondary，subordinate。
 *
 * 输入：
 *	pPciLoc 要设置的设备
 *	primary    
    secondary  
    subordinate 
 * 输出：
 *      无
 * 返回值：
 * 	    ok
 */
int pci_auto_bus_number_set
(
		PCI_LOC * pPciLoc,
		u32 primary,
		u32 secondary,
		u32 subordinate
)
{
	u32 workvar = 0;	/* Working variable		*/

	workvar = (subordinate << 16) + (secondary << 8) + primary;

	/* longword write */

	pci_modify_config_dword (pPciLoc->bus, 
			pPciLoc->device, 
			pPciLoc->function,
			PCI_CFG_PRIMARY_BUS, 
			0x00ffffff, 
			workvar);

	return OK;
}

/******************************************************************************
 *
 * pci_auto_bus_probe - 本函数用于配置桥设备并检测其上的设备
 *
 * 输入：
 *	pSystem,	PCI 设备信息	
 *    priBus,		Primary PCI 总线	
 *   secBus,		Secondary PCI 总线	
 *   pPciLoc,	 PCI 设备地址
 *   ppPciList,	下个pci设备位置的指针	
 * 输出：
 *      无
 * 返回值：
 * 	    次级总线号
 */
static u32 pci_auto_bus_probe
(
		PCI_SYSTEM * pSystem,	
		u32 priBus,		
		u32 secBus,		
		PCI_LOC*  pPciLoc,		
		PCI_LOC** ppPciList,	
		int * pListSize		
)
{
	u32 subBus = 0xff;	
	u8 offset = 0;   

	pci_auto_dev_reset (pPciLoc);

	/* 设置总线号, 次级总线号为 0xff */

	pci_auto_bus_number_set (pPciLoc, priBus, secBus, 0xff);

	PCI_AUTO_DEBUG_MSG("pciAutoBusProbe: using bridge [%d,%d,%d,0x%02x]\n",
			(pPciLoc->bus),
			(pPciLoc->device),
			(pPciLoc->function),
			(pPciLoc->attribute),
			0,
			0
	);

	/* 检测总线上的设备 */

	PCI_AUTO_DEBUG_MSG("pciAutoBusProbe: calling pciAutoDevProbe on bus [%d]\n",
			secBus, 0, 0, 0, 0, 0);

	pPciLoc->offset += (priBus > 0) ? (pPciLoc->device % 4) : 0;
	offset = pPciLoc->offset;

	PCI_AUTO_DEBUG_MSG("pciAutoBusProbe: int route offset for bridge is [%d]\n",
			offset, 0, 0, 0, 0, 0);

	//zhangyan 20150317
	ls2h_pcie_to_pci_bridge_delay();

	subBus = pci_auto_dev_probe (pSystem, secBus, offset, (pPciLoc->attribute),ppPciList, pListSize,priBus);

	PCI_AUTO_DEBUG_MSG("pciAutoBusProbe: post-config subordinate bus as [%d]\n",
			subBus, 0, 0, 0, 0, 0);

	pci_auto_bus_number_set (pPciLoc, priBus, secBus, subBus);

	/* 返回最高的次级总线号 */

	return subBus;

}

/******************************************************************************
 *
 * pci_auto_dev_probe - 本函数用于检测单pci总线上的设备
 *
 * 输入：
 *	 pSystem,	PCI 设备信息	
 *    bus,			 当前要检测的总线号		
 *    offset,		 偏移
 *    inheritAttrib,	 桥设备继承属性 	
 *    ppPciList,	 下一个pci接口位置的指针	
 *    pListSize		  当前链表下PCI_LOC的个数
 * 输出：
 *      无
 * 返回值：
 * 	    次级总线号
 */
static u32 pci_auto_dev_probe
(
		PCI_SYSTEM * pSystem,	
		u32 bus,			
		u8 offset,		
		u8 inheritAttrib,	
		PCI_LOC **ppPciList,
		int * pListSize,
		u32 preBus
)
{
	PCI_LOC pciLoc;		
	short pciclass;		
	u32 dev_vend;	
	int device;			
	int function;		
	int subBus;			
	u8 btemp;	
	u32 temp;

	/* 变量初始化 */

	bzero ((char *)&pciLoc, sizeof (PCI_LOC));
	pciLoc.bus = bus;

	subBus = bus;

	PCI_AUTO_DEBUG_MSG("pciAutoDevProbe: Start bus %x , preBus %x offset %x, inherA %x \n",
			bus,preBus, offset, inheritAttrib, 0, 0); 

	for (device = 0; device < PCI_MAX_DEV; device++)//遍历dev 0-31
	{
		pciLoc.device = device;

		/* 检查每个功能直到一个未用到的为止 */

		for (function = 0; function < PCI_MAX_FUNC; function++)//遍历func 0-7
		{
			pciLoc.function = function;

			/* 检测一个可用的设备号/厂商号*/

			pci_read_config_dword(pciLoc.bus, pciLoc.device, pciLoc.function,
					PCI_CFG_VENDOR_ID, &dev_vend);//读取Vendor ID & Device ID

			if ( ((dev_vend & 0x0000ffff) == PCI_CONFIG_ABSENT_F) || 
					((dev_vend & 0x0000ffff) == PCI_CONFIG_ABSENT_0) ||
					((dev_vend & 0x0000ffff) == 0xeeee)) /*ldf 20230908 add*/
			{
				if (function == 0)
				{
					break;	/* 没有存在的设备，跳到下一个 */
				}
				else
				{
					continue;  /* 功能为空，继续下一个 */
				}
			}

			pciLoc.offset = offset;
			pciLoc.attribute = 0;

			/* 检测此功能是否属于 PCI-PCI 桥设备 */

			pci_read_config_word (pciLoc.bus, pciLoc.device, pciLoc.function,
					PCI_CFG_SUBCLASS, &pciclass);//读取设备classcode

			PCI_AUTO_DEBUG_MSG("pciAutoDevProbe: classCode %x , dev_vend %x @ B%x D%x F%x\n",
					pciclass,dev_vend, bus, device, function, 0);

			/*warning:it only use for loongson2k-1000*/
			/*fix up pcie-controller classtype equal 0b30 not equal 0x0604(pci-pci bridge)*/
			/*it leads to the pci scan func will ignore the dev below pcie-controller*/
#ifdef FIX_UP_LOONGSON_2K
			if (dev_vend == 0x7A190014 || dev_vend == 0x7A090014)
			{
				//pciclass = 0x0604;
				pciclass = ((PCI_CLASS_BRIDGE_CTLR << 8) + PCI_SUBCLASS_P2P_BRIDGE);
			}
#endif

			switch(pciclass) 
			{

			/* PCI Host Bridge */

			case ((PCI_CLASS_BRIDGE_CTLR << 8) + PCI_SUBCLASS_HOST_PCI_BRIDGE):
		    				  pciLoc.attribute |= ( PCI_AUTO_ATTR_DEV_EXCLUDE |
		    						  PCI_AUTO_ATTR_BUS_HOST );
			break;

			/* ISA Bridge */

			case ((PCI_CLASS_BRIDGE_CTLR << 8) + PCI_SUBCLASS_ISA_BRIDGE):
		    				pciLoc.attribute |= PCI_AUTO_ATTR_BUS_ISA;

			break;

			/* Display Device */

			case (PCI_CLASS_DISPLAY_CTLR << 8):
		    				pciLoc.attribute |= PCI_AUTO_ATTR_DEV_DISPLAY;

			break;

			/* PCI-to-PCI Bridge */

			case ((PCI_CLASS_BRIDGE_CTLR << 8) + PCI_SUBCLASS_P2P_BRIDGE):

		    				/* Setup and probe this bridge device */

		    				pciLoc.attribute |= PCI_AUTO_ATTR_BUS_PCI;//继承属性

			/*
			 * Check for 32 bit I/O addressability,
			 * but only if the parent bridge supports it
			 */

			if (inheritAttrib & PCI_AUTO_ATTR_BUS_4GB_IO)
			{
				pci_read_config_byte (pciLoc.bus,
						pciLoc.device,
						pciLoc.function,
						PCI_CFG_IO_BASE, &btemp);

				if ((btemp & 0x0F) == 0x01)
				{
					pci_read_config_byte (pciLoc.bus, 
							pciLoc.device, pciLoc.function,
							PCI_CFG_IO_LIMIT, &btemp);
					if ((btemp & 0x0F) == 0x01)
					{
						pciLoc.attribute |= PCI_AUTO_ATTR_BUS_4GB_IO;
						PCI_AUTO_DEBUG_MSG("pciAutoDevProbe: 4G I/O \n",
								0, 0, 0, 0, 0, 0);
					}
				}
			}

			/* Disable prefetch */

			pci_modify_config_dword (pciLoc.bus, 
					pciLoc.device, 
					pciLoc.function,
					PCI_CFG_PRE_MEM_BASE, 
					0xfff0fff0, 0x0000fff0);

			pci_write_config_dword (pciLoc.bus, 
					pciLoc.device,
					pciLoc.function,
					PCI_CFG_PRE_MEM_LIMIT_U, 0);

			pci_write_config_dword (pciLoc.bus,
					pciLoc.device,
					pciLoc.function,
					PCI_CFG_PRE_MEM_BASE_U, 0xffffffff);

			/* Check for Prefetch memory support */

			if (inheritAttrib & PCI_AUTO_ATTR_BUS_PREFETCH)
			{
				pci_read_config_dword  (pciLoc.bus,
						pciLoc.device,
						pciLoc.function,
						PCI_CFG_PRE_MEM_BASE, &temp);

				/* PF Registers return 0 if PF is not implemented */

				if (temp != 0)
				{ 
					pciLoc.attribute |= PCI_AUTO_ATTR_BUS_PREFETCH;
					PCI_AUTO_DEBUG_MSG("pciAutoDevProbe: PF present\n",
							0, 0, 0, 0, 0, 0);
				}
			}
			break;

			default:

				/* Mask off all but bus attribute bits to inherit */

				inheritAttrib &=   ( PCI_AUTO_ATTR_BUS_4GB_IO |
						PCI_AUTO_ATTR_BUS_PREFETCH );

				/* devices inherit bus attributes from their bridge */

				pciLoc.attribute |= inheritAttrib;

				break;
			}

			/* Add this function to the PCI function list */

			if (*pListSize < PCI_AUTO_MAX_FUNCTIONS)
			{
				memcpy (*ppPciList, &pciLoc, sizeof (PCI_LOC));
				(*ppPciList)++;
				(*pListSize)++;
			}

			/* If the device is a PCI-to-PCI bridge, then scan behind it */

			if (pciLoc.attribute & PCI_AUTO_ATTR_BUS_PCI)
			{
				PCI_AUTO_DEBUG_MSG("pciAutoDevProbe: scanning bus[%d]\n",
						(subBus+1), 0, 0, 0, 0, 0 );
				subBus = pci_auto_bus_probe(pSystem, bus, subBus+1, &pciLoc, ppPciList, pListSize);
			}

			/* Proceed to next device if this is a single function device */

			if (function == 0)
			{
				pci_read_config_byte (pciLoc.bus, pciLoc.device, pciLoc.function,
						PCI_CFG_HEADER_TYPE, &btemp);
				if ((btemp & PCI_HEADER_MULTI_FUNC) == 0)
				{
					break; /* No more functions - proceed to next PCI device */
				}
			}
		}

#ifdef FIX_UP_LOONGSON_2K
		/*fix up 2k-1000 pcie port only scan one dev*/
		if (0 == preBus )
		{
			break;
		}
#endif

	}
	return (subBus);
}


/******************************************************************************
 *
 * pci_auto_func_config_all - 本函数用于配置链表中的功能
 *
 * 输入：
 *	 pSystem,	PCI 设备信息	
 *	 pPciList   指向第一个功能的指针		
 *    nSize,	要初始化的 功能数	
 * 输出：
 *      无
 * 返回值：
 * 	    无
 */
static void pci_auto_func_config_all
(
		PCI_SYSTEM * pSystem,	
		PCI_LOC *pPciList,		
		u32 nSize			
)
{
	PCI_LOC *pPciFunc;		/* 指向下个功能的指针 */
	u32 nLoop;			/* 循环控制变量   */
	u32 nEnd;			

	pPciFunc = pPciList;
	nEnd = nSize;
	pci_auto_dev_config(pSystem, pPciList->bus, &pPciFunc, &nEnd);

	/* 使能设备链表上的每个设备 */

	pPciFunc = pPciList;
	for (nLoop = 0; nLoop < nSize; nLoop++)
	{
		pci_auto_func_enable (pSystem, pPciFunc);
		pPciFunc++;
	}
}

/******************************************************************************
 *
 * pci_auto_func_disable - 本函数用于屏蔽一个指定pci设备
 *
 * 输入：
 *	 pPciFunc,	PCI 功能结构体指针	

 * 输出：
 *      无
 * 返回值：
 * 	    无
 */
void pci_auto_func_disable
(
		PCI_LOC *pPciFunc		
)
{
	u8 cTemp;			/* Temporary storage */
	u16 wTemp;

	if ((pPciFunc->attribute) & PCI_AUTO_ATTR_DEV_EXCLUDE)
	{
		return;
	}

	PCI_AUTO_DEBUG_MSG("pciAutoFuncDisable: disable device [%d,%d,%d,0x%02x]\n",
			(pPciFunc->bus),
			(pPciFunc->device),
			(pPciFunc->function),
			(pPciFunc->attribute),
			0,
			0
	);

	/* 非能内存，io，以及总线功能，保存状态位 */
	wTemp = (PCI_CMD_IO_ENABLE | PCI_CMD_MEM_ENABLE | PCI_CMD_MASTER_ENABLE );

	pci_modify_config_dword (pPciFunc->bus, pPciFunc->device, pPciFunc->function,
			PCI_CFG_COMMAND, (0xffff0000 | wTemp), 0x0);

	pci_read_config_byte (pPciFunc->bus, pPciFunc->device, pPciFunc->function,
			PCI_CFG_HEADER_TYPE, &cTemp);

	cTemp &= PCI_HEADER_TYPE_MASK;

	switch (cTemp)
	{
	case PCI_HEADER_TYPE0:	/* non PCI-PCI bridge */

		pci_modify_config_dword (pPciFunc->bus, pPciFunc->device,
				pPciFunc->function, PCI_CFG_EXPANSION_ROM,
				0x1, 0);
		break;

	case PCI_HEADER_PCI_PCI:	/* PCI-PCI bridge */

		pci_modify_config_dword (pPciFunc->bus, pPciFunc->device,
				pPciFunc->function, PCI_CFG_ROM_BASE,
				0x1, 0);
		break;

	default:
		break;
	}
	return;
}

/******************************************************************************
 *
 * pci_auto_func_enable - 本函数用于使能一个指定pci设备
 *
 * 输入：
 *	 pSystem,	PCI 设备信息	
     pFunc      指向pci功能结构体指针
 * 输出：
 *      无
 * 返回值：
 * 	    无
 */
void pci_auto_func_enable
(
		PCI_SYSTEM * pSystem, /* PCI system info */
		PCI_LOC * pFunc       /* input: Pointer to PCI function structure */
)
{
	short pciClass;	  /* PCI class/subclass contents */
	u8 intLine = 0xff; /* Interrupt "Line" value           */

	if ((pFunc->attribute) & PCI_AUTO_ATTR_DEV_EXCLUDE)
	{
		return;
	}

	PCI_AUTO_DEBUG_MSG("pciAutoFuncConfig: enable device [%d,%d,%d,0x%02x]\n",
			(pFunc->bus),
			(pFunc->device),
			(pFunc->function),
			(pFunc->attribute),
			0,
			0
	);

#if 0 /*ldf 20230911 ignore:: 这里重新配置之后会将bar中的地址清0，原因未知*/
	/* 初始化cache行大小寄存器 */

	pci_write_config_byte (pFunc->bus, pFunc->device, pFunc->function,
			PCI_CFG_CACHE_LINE_SIZE, pSystem->cacheSize);

	
	/* Initialize the latency timer */

	pci_write_config_byte (pFunc->bus, pFunc->device, pFunc->function,
			PCI_CFG_LATENCY_TIMER, pSystem->maxLatency);
#endif
	
	/* Get the PCI class code */

	pci_read_config_word (pFunc->bus, pFunc->device, pFunc->function,
			PCI_CFG_SUBCLASS, &pciClass);

	/* Enable Bus Mastering (preserve status bits) */

	pci_modify_config_dword (pFunc->bus, pFunc->device, pFunc->function,
			PCI_CFG_COMMAND,
			(0xffff0000 | PCI_CMD_MASTER_ENABLE),
			PCI_CMD_MASTER_ENABLE);

	
	intLine = pci_auto_int_assign (pSystem, pFunc);
	//printk("intLine = %x\n",intLine);
	pci_write_config_byte (pFunc->bus, pFunc->device, pFunc->function,
			PCI_CFG_DEV_INT_LINE, intLine);

	
	/* Reset all writeable status bits */

	pci_write_config_word (pFunc->bus, pFunc->device, pFunc->function,
			PCI_CFG_STATUS, (u16)0xFFFF);

	
	return;
}


/******************************************************************************
 *
 * pci_auto_func_enable - 本函数用于计算出给定pci设备的中断号
 *
 * 输入：
 *	 pSystem,	PCI 设备信息	
     pFunc      指向pci功能结构体指针
 * 输出：
 *      无
 * 返回值：
 * 	    中断号
 */
static u8 pci_auto_int_assign
(
		PCI_SYSTEM * pSystem, 
		PCI_LOC * pFunc       
)
{
	u8 retVal = 0xFF;
	u8        intPin;        /* Interrupt "Pin" value            */

	pci_read_config_byte (pFunc->bus, pFunc->device, pFunc->function,
			PCI_CFG_DEV_INT_PIN, &intPin);

	if ((!(pSystem->autoIntRouting)) && (intPin != 0))
	{
		if ((pSystem->intAssignRtn) != NULL )
		{
			retVal = (pSystem->intAssignRtn) (pSystem, pFunc, intPin);

			return (retVal);
		}

	}

	/* default interrupt routing: let's find out the IRQ for this device */

	switch (pFunc->bus)
	{
	case 0:

		if (((pSystem->intAssignRtn) != NULL) && (intPin != 0))
		{
			retVal = (pSystem->intAssignRtn) (pSystem, pFunc, intPin);
		}


		if (((pFunc->attribute) & PCI_AUTO_ATTR_BUS_PCI) > 0)
		{
			int i = 0;

			for (i = 0; i < 4; i++)
			{
				if ((pSystem->intAssignRtn) != NULL )
				{
					pci_auto_int_routing_table [i]  = (pSystem->intAssignRtn) 
                                                    		  (pSystem, pFunc, (i+1));
				}
			}

		}

		break;

	default:

		retVal = pci_auto_int_routing_table [(((pFunc->device) + (intPin - 1) 
				+ (pFunc->offset)) % 4)];
		break;
	}

	PCI_AUTO_DEBUG_MSG("pciAutoIntAssign: int for [%d,%d,%d] pin %d is [%d]\n",
			pFunc->bus, pFunc->device, pFunc->function, intPin, retVal, 0);

	return retVal;
}


/******************************************************************************
 *
 * pci_auto_get_next_class - 本函数用于从表中找出下一个特殊类型的设备
 *
 * 输入：
 *	 pPciSystem,		
     pPciFunc  
     index     索引
     pciClass
     mask   
 * 输出：
 *      无
 * 返回值：
 * 	    成功返回true，否则false
 */
int pci_auto_get_next_class
(
		PCI_SYSTEM *pPciSystem,
		PCI_LOC *pPciFunc,  /* output: Contains the BDF of the device found */
		u32 *index,        /* Zero-based device instance number */
		u32 pciClass,      /* class code field from the PCI header */
		u32 mask           /* mask is ANDed with the class field */
)
{
	u32 i;
	u32 idx = *index;
	u32 classCode;
	u32 nSize;
	PCI_LOC *pciList;

	nSize = (u32)lastPciListSize;
	pciList = pLastPciList;

	PCI_AUTO_DEBUG_MSG("\npciAutoGetNextClass: index[%d] listSiz[%d]\n",
			*index, nSize, 0, 0, 0, 0);
	PCI_AUTO_DEBUG_MSG("                     pciClass[0x%08x], mask[0x%08x]\n",
			pciClass, mask, 0, 0, 0, 0);

	if ((nSize <= idx) || (pciList == NULL))
		return (FALSE);             /* No more devices */

	for (i = idx; i < nSize; i++)
	{

		/* Grab the class code 24-bit field */

		pci_read_config_dword ((u32)pciList[i].bus, (u32)pciList[i].device,
				(u32)pciList[i].function, (PCI_CFG_CLASS & 0xfc),
				&classCode);

		classCode >>= 8;        /* Isolate class code in low order bits */

		if ((classCode & mask) == (pciClass & mask))
		{
			*index = i;
			*pPciFunc = pciList[i];
			return (TRUE);
		}

	}
	return (FALSE);
}


/******************************************************************************
 *
 * pci_auto_dev_config - 本函数用于为一个pci功能分配内存和io空间
 * 
 *
 * 输入：
 *	 pSystem,		设备信息
     bus        总线号  
     ppPciList     功能表的指针
     nSize       个数
 * 输出：
 *      无
 * 返回值：
 * 	    无
 */
static void pci_auto_dev_config
(
		PCI_SYSTEM * pSystem,  /* PCI system info */
		u32 bus,		/* Current bus number	   */
		PCI_LOC **ppPciList,	/* Pointer to function list */
		u32 *nSize		/* Number of remaining funcs */
)
{
	PCI_LOC *pPciFunc;	/* Pointer to PCI function	   */
	u32 nextBus;		/* Bus where function is located   */
	short pciClass;		/* Class field of function	   */

	/* Process each function within the list */

	while (*nSize > 0)//剩余需要配置的功能数量大于0
	{

		/* Setup local variables */

		pPciFunc = *ppPciList;
		nextBus = pPciFunc->bus;

		/* Decrease recursion depth if this function is on a parent bus */

		if (nextBus < bus)
		{
			return;
		}
	
		/* Allocate and assign space to functions on this bus */
		pci_auto_func_config (pSystem, pPciFunc);

		(*nSize)--;
		(*ppPciList)++;

		u32 dev_vend;
		pci_read_config_dword(pPciFunc->bus, pPciFunc->device, pPciFunc->function,
				PCI_CFG_VENDOR_ID, &dev_vend);//读取Vendor ID & Device ID

		pci_read_config_word (pPciFunc->bus, pPciFunc->device, pPciFunc->function,
				PCI_CFG_SUBCLASS, &pciClass);

#ifdef FIX_UP_LOONGSON_2K
		if (dev_vend == 0x7A190014 || dev_vend == 0x7A090014)
		{
			pciClass = 0x0604;
		}
#endif

		switch (pciClass)
		{
		case (( PCI_CLASS_BRIDGE_CTLR << 8) + PCI_SUBCLASS_P2P_BRIDGE ):
			/* PCI-PCI bridge functions increase recursion depth */
			pci_auto_bus_config (pSystem, pPciFunc, ppPciList, nSize);
			break;
		default:
			/* Maintain current recursion depth */
			break;
		}
	}
}

/******************************************************************************
 *
 * pci_auto_func_config - 本函数用于为一个pci功能分配内存和io空间
 * 
 *
 * 输入：
 *	 pSystem,		设备信息
      pPciFunc      功能指针
 * 输出：
 *      无
 * 返回值：
 * 	    无
 */
static void pci_auto_func_config
(
		PCI_SYSTEM * pSystem,
		PCI_LOC * pPciFunc	/* input: "Include list" pointer to function */
)
{
	u32 baMax;		/* Total number of base addresses    */
	u32 baI;		/* Base address register index	     */
	u32 baseAddr;	/* PCI Offset of base address	     */
	u32 readVar;	/* Contents of base address register */
	u32 addrInfo;	/* PCI address type information	     */
	u32 sizeMask;	/* LSbit for size calculation	     */
	u8 headerType;	/* Read from PCI config header	     */
	u32 dev_vend;
	u32 register64Bit = 0;		/* 64 bit register flag */

	/* If there is a function, then consult the exclusion routine */

	if ( (pSystem->includeRtn) != NULL )
	{
		pci_read_config_dword (pPciFunc->bus, pPciFunc->device, pPciFunc->function,
				PCI_CFG_VENDOR_ID, &dev_vend);
		if ( ((pSystem->includeRtn) (pSystem, pPciFunc, dev_vend)) == ERROR )
		{
			if ((pPciFunc->attribute & PCI_AUTO_ATTR_BUS_PCI) == 0) 
			{
				pPciFunc->attribute |= PCI_AUTO_ATTR_DEV_EXCLUDE;
				PCI_AUTO_DEBUG_MSG("pciAutoFuncConfig: exc [%d,%d,%d,0x%02x]\n",
						pPciFunc->bus, pPciFunc->device, pPciFunc->function,
						pPciFunc->attribute,0,0);
				return;
			}
		}
	}

	/* Disable the function */

	pci_auto_func_disable (pPciFunc);

	/* Determine the number of base address registers present */

	pci_read_config_byte (pPciFunc->bus, pPciFunc->device, pPciFunc->function,
			PCI_CFG_HEADER_TYPE, &headerType);
	headerType &= 0x7f;

	switch (headerType)
	{
	case PCI_HEADER_TYPE0:
		baMax = 6;
		break;

	case PCI_HEADER_PCI_PCI:
		/* Iying 2021-11-22 : 
		 * Some bridges require memory resource, such as loongson PCIe controller.
		 * As SPEC, every bridge has two BAR registers, so set it to 2.
		 */
		//          baMax = 0;  
		baMax = 2;

		break;

	default:
		baMax = 0;
		break;
	}

	/* Allocate Memory or I/O space for each implemented base addr register */

	for (baI = 0; baI < baMax; baI++)
	{
		/* Get the base address register contents */

		baseAddr = PCI_CFG_BASE_ADDRESS_0 + (baI * 4);//baseaddr 地址初始化

		pci_write_config_dword (pPciFunc->bus, pPciFunc->device, pPciFunc->function,
				baseAddr, 0xFFFFFFFF);//往baseaddr写0xFFFFFFFF

		pci_read_config_dword (pPciFunc->bus, pPciFunc->device, pPciFunc->function,
				baseAddr, &readVar);//回读baseaddr
		
		printk("<**DEBUG**> [%s():_%d_]:: [%d,%d,%d] BAR%d: 0x%08x (after write 0xffffffff)\n", 
				__func__, __LINE__,pPciFunc->bus,pPciFunc->device,pPciFunc->function,baI,readVar);

//		PCI_AUTO_DEBUG_MSG("pciAutoFuncConfig: BAR [%d,%d,%d,0x%02x]\n",
//				pPciFunc->bus, pPciFunc->device, pPciFunc->function,
//				readVar,0,0); 

		/* Go to the next BAR when an unimplemented one (BAR==0) is found */

		if (readVar == 0)
		{
			continue;//地址无属性,跳到下一个地址
		}

		/* Mask off all but space, memory type, and prefetchable bits */

		addrInfo = readVar & PCI_BAR_ALL_MASK;//地址属性掩码提取,0x0f;

		/* Check for type, setup mask variables (based on type) */

		if ((addrInfo & PCI_BAR_SPACE_MASK) == PCI_BAR_SPACE_IO)//此地址空间需要分配IO空间
		{
			printk("<INFO> [%s():_%d_]:: [%d,%d,%d] IO Space found at BAR[%d]\n", 
					__func__, __LINE__,pPciFunc->bus,pPciFunc->device,pPciFunc->function,baI);
//			PCI_AUTO_DEBUG_MSG("pciAutoFuncConfig: IO Space found at BAR[%d]\n",
//					baI, 0, 0, 0, 0, 0);
			sizeMask = (1 << 2);
		}
		else//此地址空间需要分配MEM空间
		{
			printk("<INFO> [%s():_%d_]:: [%d,%d,%d] MemSpace found at BAR[%d]\n", 
					__func__, __LINE__,pPciFunc->bus,pPciFunc->device,pPciFunc->function,baI);
//			PCI_AUTO_DEBUG_MSG("pciAutoFuncConfig: MemSpace found at BAR[%d]\n",
//					baI, 0, 0, 0, 0, 0);
			sizeMask = (1 << 4);
		}

		/* Loop until we find a bit set or until we run out of bits */
		for (; sizeMask; sizeMask <<= 1)
		{
			/* is this bit set? if not, keep looking */
			if (readVar & sizeMask)
			{	
				printk("<**DEBUG**> [%s():_%d_]:: [%d,%d,%d] sizeMask = 0x%x\n", 
						__func__, __LINE__,pPciFunc->bus,pPciFunc->device,pPciFunc->function,sizeMask);
				register64Bit = pci_auto_reg_config (pSystem, pPciFunc, baseAddr, sizeMask, addrInfo);
				baI += register64Bit;
				break;
			}
		}
		
#if 1 /*ldf 20230908 add:: 每个功能只分配一个mem空间*/
	unsigned int memAddrLo, memAddrHi;
    if(baseAddr == 0x10){ /*ldf 20230911 add:: debug*/
    	pci_read_config_dword(pPciFunc->bus,pPciFunc->device,pPciFunc->function, baseAddr, &memAddrLo);
    	printk("<**DEBUG**> [%s():_%d_]:: [%d,%d,%d]  offset:[0x%x] memAddrLo:[0x%08x] ", 
    			__func__,__LINE__,pPciFunc->bus,pPciFunc->device,pPciFunc->function,baseAddr,memAddrLo);
    	if(register64Bit){
    		pci_read_config_dword(pPciFunc->bus,pPciFunc->device,pPciFunc->function, baseAddr+4, &memAddrHi);
    		printk("| offset:[0x%x] memAddrHi:[0x%08x]\n",baseAddr+4,memAddrHi);
    	}else{
    		printk("\n");
    	}
    }
	memAddrLo &= ~0xf;
	memAddrHi &= ~0xf;
	if((memAddrLo != 0xFFFFFFF0) || (memAddrHi != 0xFFFFFFF0))
		break;
#endif/*ldf 20230908 add end:: 每个功能只分配一个mem空间*/
	}
}

/******************************************************************************
 *
 * pci_auto_func_config - 本函数用于为一个pci基地址寄存器分配pci空间
 * 
 *
输入
 * 输出：
 *      无
 * 返回值：
 * 	    1 如果bar支持路由到任意的64位地址空间
 *       否则返回0
 */
u32 pci_auto_reg_config
(
		PCI_SYSTEM * pSystem,	/* Pointer to PCI System structure */
		PCI_LOC *pPciFunc,		/* Pointer to function in device list */
		u32 baseAddr,		/* Offset of base PCI address */
		u32 nSize,			/* Size and alignment requirements */
		u32 addrInfo		/* PCI address type information	*/
)
{
	u64 addr;			/* Working address variable */
	u32 spaceEnable = 0;	/* PCI space enable bit */
	u32 baseaddr_mask;		/* Mask for base address register */
	u32 register64Bit;		/* 64 bit register flag */

	/* Select the appropriate PCI address space for this register */

	if ((addrInfo & PCI_BAR_SPACE_MASK) == PCI_BAR_SPACE_IO) /*PCI I/O space*/
	{
		/* Configure this register for PCI I/O space */
		printk("<**DEBUG**> [%s():_%d_]:: [%d,%d,%d]  pci auto alloc addr for io space\n", 
				__func__, __LINE__,pPciFunc->bus,pPciFunc->device,pPciFunc->function);
		spaceEnable = PCI_CMD_IO_ENABLE;
		baseaddr_mask = 0xFFFFFFFC;
		register64Bit = pci_auto_io_alloc (pSystem, pPciFunc, &addr, nSize);
	}
	else /*PCI memory space*/
	{
		/* Configure this register for PCI memory space */
		printk("<**DEBUG**> [%s():_%d_]:: [%d,%d,%d]  pci auto alloc addr for memory space\n", 
				__func__, __LINE__,pPciFunc->bus,pPciFunc->device,pPciFunc->function);
		spaceEnable = PCI_CMD_MEM_ENABLE;
		baseaddr_mask = 0xFFFFFFF0;
		register64Bit = pci_auto_mem_alloc (pSystem, pPciFunc, &addr, nSize, addrInfo);
	}
	
	printk("<**DEBUG**> [%s():_%d_]:: [%d,%d,%d]  alloc addr: 0x%lx, size: 0x%x, register64Bit: %d\n", 
			__func__,__LINE__,pPciFunc->bus,pPciFunc->device,pPciFunc->function,addr,nSize,register64Bit);

	if (addr != NO_ALLOCATION)
	{
		/* Program the base address register */
#if 1 /*ldf 20230912 add:: MEM Space使用的64位地址*/
		pci_modify_config_dword (pPciFunc->bus, pPciFunc->device, 
				pPciFunc->function, baseAddr, 
				baseaddr_mask, (unsigned int)(addr & 0xffffffff));

		if (register64Bit)
		{
			pci_write_config_dword (pPciFunc->bus, pPciFunc->device, 
					pPciFunc->function,
					baseAddr + 4, (unsigned int)(addr >> 32));
		}
#else
		pci_modify_config_dword (pPciFunc->bus, pPciFunc->device, 
				pPciFunc->function, baseAddr, 
				baseaddr_mask, addr);

		if (register64Bit)
		{
			pci_write_config_dword (pPciFunc->bus, pPciFunc->device, 
					pPciFunc->function,
					baseAddr + 4, 0);
		}
#endif

		/* Set the appropriate enable bit, preserve status bits */

		pci_modify_config_dword (pPciFunc->bus, pPciFunc->device,
				pPciFunc->function, PCI_CFG_COMMAND,
				(0xffff0000 | spaceEnable), spaceEnable);
	}
    
	return (register64Bit);
}


/******************************************************************************
 *
 * pci_auto_io_alloc - 本函数用于为一个设备选择合适的io空间

 * 返回值：
 * 	    0
 */
static u32 pci_auto_io_alloc
(
		PCI_SYSTEM * pPciSys,	/* PCI system structure	  */
		PCI_LOC *pPciFunc,	/* input: Pointer to PCI function element     */
		u32 *pAlloc,		/* output: Pointer to PCI space alloc pointer */
		u32 nSize		/* requested size (power of 2) */
)
{
	u32 * pBase;
	u32 alignedBase;
	u32 sizeAdj;
	u32 * pAvail = NULL;
	int retStat = ERROR;

	printk("<**DEBUG**> [%s():_%d_]:: [%d,%d,%d]  nSize: 0x%x\n", 
			__func__, __LINE__,pPciFunc->bus,pPciFunc->device,pPciFunc->function,nSize);
	
	/* Assign this register to PCI I/O space */

	if ((pPciFunc->bus == 0) && 
			((pPciFunc->attribute & PCI_AUTO_ATTR_BUS_4GB_IO) != 0))
	{
		PCI_AUTO_DEBUG_MSG("pciAutoIoAlloc: 32-bit I/O\n", 0, 0, 0, 0, 0, 0);
		pBase = &(pPciSys->pciIo32);
		pAvail = &(pPciSys->pciIo32Size);
	}
	else
	{
		PCI_AUTO_DEBUG_MSG("pciAutoIoAlloc: 16-bit I/O\n", 0, 0, 0, 0, 0, 0);
		pBase = &(pPciSys->pciIo16);
		pAvail = &(pPciSys->pciIo16Size);
	}

	/* Adjust for alignment */

	if (*pAvail > 0) 
	{
		retStat = pci_auto_addr_align (*pBase,
				(*pBase + *pAvail),
				nSize,
				&alignedBase);
	}

	/* If the space is exhausted, then return an invalid pointer */

	if (retStat == ERROR)
	{
		PCI_AUTO_DEBUG_MSG("Warning: PCI I/O allocation failed\n", 0, 0, 0, 0, 0, 0);
		*pAlloc = NO_ALLOCATION;
		return 0;
	}

	PCI_AUTO_DEBUG_MSG("pciAutoIoAlloc: Pre/Post alloc: \n", 0, 0, 0, 0, 0, 0);
	PCI_AUTO_DEBUG_MSG("  Pre: pBase[0x%08x], pAvail[0x%08x]\n",
			(int)(*pBase), (int)(*pAvail), 0, 0, 0, 0);

	*pAlloc  = alignedBase;
	sizeAdj = (alignedBase - *pBase) + nSize;
	*pBase  += sizeAdj;
	*pAvail -= sizeAdj;

	PCI_AUTO_DEBUG_MSG("  Post: pBase[0x%08x], pAvail[0x%08x]\n",
			(int)(*pBase), (int)(*pAvail), 0, 0, 0, 0);

	return 0; /* can't have 64 bit i/o addresses */
}

/******************************************************************************
 *
 * pci_auto_mem_alloc - 本函数用于为一个设备选择合适的内存空间

 * 返回值：
 *      如果是64位可寻址内存空间 返回1，否则返回0
 * 	
 */
static u32 pci_auto_mem_alloc
(
		PCI_SYSTEM * pPciSys,	/* PCI system structure */
		PCI_LOC * pPciFunc, 	/* Pointer to PCI function element     */
		u64 * pAlloc,   	/* Pointer to PCI space alloc pointer */
		u32 size,		/* space requested (power of 2)  */
		u32 addrInfo		/* PCI address type information	      */
)
{
	u32 register64Bit = 0;	/* 64 bit register flag */
	u64 * pBase;
	u32 * pAvail;
	u64 alignedBase;
	u32 sizeAdj;
	int retStat = ERROR;

	printk("<**DEBUG**> [%s():_%d_]:: [%d,%d,%d]  size: 0x%x, addrInfo: 0x%x\n", 
			__func__, __LINE__,pPciFunc->bus,pPciFunc->device,pPciFunc->function,size,addrInfo);
	
	/* Process address attribute info */

	switch (addrInfo & (u32)PCI_BAR_MEM_TYPE_MASK )
	{
	case PCI_BAR_MEM_BELOW_1MB:
		break;

	case PCI_BAR_MEM_ADDR64:
		register64Bit = 1;
		break;

	case PCI_BAR_MEM_ADDR32:
		break;

	case PCI_BAR_MEM_RESERVED:
		/* fall through */

	default:
		*pAlloc = NO_ALLOCATION;
		return 0;
	}

	if ( (addrInfo & PCI_BAR_MEM_PREFETCH) &&
			((pPciFunc->attribute) & PCI_AUTO_ATTR_BUS_PREFETCH) )
	{
		printk("<**DEBUG**> [%s():_%d_]:: [%d,%d,%d]  Prefetch Memory requested\n", 
				__func__, __LINE__,pPciFunc->bus,pPciFunc->device,pPciFunc->function);

		pBase = &(pPciSys->pciMem64);
		pAvail = &(pPciSys->pciMem64Size);
		printk("<**DEBUG**> [%s():_%d_]:: [%d,%d,%d]  pBase: 0x%lx, pAvail: 0x%x\n", 
			__func__, __LINE__, pPciFunc->bus,pPciFunc->device,pPciFunc->function,*pBase,*pAvail);
		
		if (*pAvail > 0)
		{
			retStat = pci_auto_addr_align (*pBase,
					(*pBase + *pAvail),
					size,
					&alignedBase);
		}

		if (retStat == ERROR)
		{
			/* If no PF memory available, then try conventional */

			PCI_AUTO_DEBUG_MSG("pciAutoMemAlloc: No PF Mem available"
					"Trying MemIO\n", 0, 0, 0, 0, 0, 0);

			pBase = &(pPciSys->pciMemIo64);
			pAvail = &(pPciSys->pciMemIo64Size);
			printk("<**DEBUG**> [%s():_%d_]:: [%d,%d,%d]  pBase: 0x%lx, pAvail: 0x%x\n", 
					__func__, __LINE__, pPciFunc->bus,pPciFunc->device,pPciFunc->function,*pBase,*pAvail);
			
			if (*pAvail > 0)
			{
				retStat = pci_auto_addr_align (*pBase,
						(*pBase + *pAvail),
						size,
						&alignedBase);
			}
			printk("<**DEBUG**> [%s():_%d_]:: [%d,%d,%d]  alignedBase: 0x%lx\n", 
					__func__, __LINE__, pPciFunc->bus,pPciFunc->device,pPciFunc->function,alignedBase);

			if (retStat == ERROR)
			{
				PCI_AUTO_DEBUG_MSG("Warning: PCI PF Mem alloc failed\n", 0, 0, 0, 0, 0, 0);
				*pAlloc = NO_ALLOCATION;
				return 0;
			}
		}
	}
	else
	{
		printk("<**DEBUG**> [%s():_%d_]:: [%d,%d,%d]  Non-Prefetch Memory requested\n", 
				__func__, __LINE__,pPciFunc->bus,pPciFunc->device,pPciFunc->function);
		/* Use 64-bit Non-Prefetch Memory */
		pBase = &(pPciSys->pciMemIo64);
		pAvail = &(pPciSys->pciMemIo64Size);
		printk("<**DEBUG**> [%s():_%d_]:: [%d,%d,%d]  pBase: 0x%lx, pAvail: 0x%x\n", 
				__func__, __LINE__, pPciFunc->bus,pPciFunc->device,pPciFunc->function,*pBase,*pAvail);

		if (*pAvail > 0)
		{
			retStat = pci_auto_addr_align (*pBase,
					(*pBase + *pAvail),
					size,
					&alignedBase);
		}
		printk("<**DEBUG**> [%s():_%d_]:: [%d,%d,%d]  alignedBase: 0x%lx\n", 
				__func__, __LINE__, pPciFunc->bus,pPciFunc->device,pPciFunc->function,alignedBase);

		if (retStat == ERROR)
		{
			printk("<WARNNING> [%s():_%d_]:: [%d,%d,%d]  PCI Memory allocation failed\n", 
					__func__, __LINE__, pPciFunc->bus,pPciFunc->device,pPciFunc->function);
			*pAlloc = NO_ALLOCATION;
			return 0;
		}
	}

	*pAlloc  = alignedBase;
	sizeAdj = (alignedBase - *pBase) + size;
	*pBase  += sizeAdj;
	*pAvail -= sizeAdj;

	printk("<**DEBUG**> [%s():_%d_]:: [%d,%d,%d]  update pBase: 0x%lx, update pAvail: 0x%lx\n", 
			__func__, __LINE__, pPciFunc->bus,pPciFunc->device,pPciFunc->function,(*pBase),(*pAvail));

	return register64Bit;
}

/******************************************************************************
 *
 * pci_auto_bus_config - 本函数用于设置内存和io配置寄存器

 * 返回值：
 * 	    无
 */
static void pci_auto_bus_config
(
		PCI_SYSTEM * pSystem,	/* PCI system info */
		PCI_LOC * pPciLoc,		/* PCI address of this bridge */
		PCI_LOC **ppPciList,	/* Pointer to function list pointer */
		u32 *nSize			/* Number of remaining functions */
)
{
	u8 bus;			/* Bus number for current bus */
	u32 dev_vend;
	u64 debugTmp;
	u32 debugTmp2;
	u32 debugTmp3;
	u64 alignedBase;

	/* If it exists, call the user-defined pre-config pass bridge init */

	if ((pSystem->bridgePreConfigInit) != NULL )
	{
		pci_read_config_dword (pPciLoc->bus, pPciLoc->device, pPciLoc->function,
				PCI_CFG_VENDOR_ID, &dev_vend);

		(pSystem->bridgePreConfigInit) (pSystem, pPciLoc, dev_vend);
	}

	/* Clear the secondary status bits */

	pci_modify_config_dword (pPciLoc->bus, pPciLoc->device, pPciLoc->function,
			PCI_CFG_IO_BASE, 0xffff0000, 0xffff0000);

	/* If Prefetch supported, then pre-configure 32-bit PF Memory Base Addr */

	if ( (pPciLoc->attribute & PCI_AUTO_ATTR_BUS_PREFETCH) 
			&& (pSystem->pciMem64Size > 0) )
	{ 

		PCI_AUTO_DEBUG_MSG("pciAutoBusConfig: Configuring prefetch aperture\n",
				0, 0, 0, 0, 0, 0);

		pci_auto_addr_align(pSystem->pciMem64,
				(pSystem->pciMem64 + pSystem->pciMem64Size),
				0x100000,
				&alignedBase);

		PCI_AUTO_DEBUG_MSG("PF Mem Base orig[0x%08x] new[0x%lx] adj[0x%lx]\n",
				(pSystem->pciMem64),
				alignedBase,
				(alignedBase - (pSystem->pciMem64)),
				0, 
				0,
				0 );

		(pSystem->pciMem64Size) -= (alignedBase - (pSystem->pciMem64));
		(pSystem->pciMem64) = alignedBase;
#if 1/*xxx ldf 20230912 add*/
		pci_write_config_dword (pPciLoc->bus, pPciLoc->device, pPciLoc->function,
				PCI_CFG_PRE_MEM_BASE_U, (u32)(pSystem->pciMem64 >> 32) );

		pci_modify_config_dword (pPciLoc->bus,pPciLoc->device,pPciLoc->function,
				PCI_CFG_PRE_MEM_BASE, 0x0000fff0,
				(u32)((pSystem->pciMem64 & 0xffffffff) >> (20-4)) );
#else
		/* 64-bit Prefetch memory not supported at this time */

		pci_write_config_dword (pPciLoc->bus, pPciLoc->device, pPciLoc->function,
				PCI_CFG_PRE_MEM_BASE_U, 0);

		pci_modify_config_dword (pPciLoc->bus,pPciLoc->device,pPciLoc->function,
				PCI_CFG_PRE_MEM_BASE, 0x0000fff0,
				(pSystem->pciMem32 >> (20-4)));
#endif
		
		pci_read_config_dword (pPciLoc->bus, pPciLoc->device, pPciLoc->function,
				PCI_CFG_PRE_MEM_BASE,
				&debugTmp2);
		debugTmp = ((debugTmp2 & 0x0000fff0) << 16);
		
		pci_read_config_dword (pPciLoc->bus, pPciLoc->device, pPciLoc->function,
				PCI_CFG_PRE_MEM_BASE_U,
				&debugTmp2);
		debugTmp = (debugTmp2 << 32) | debugTmp;

		PCI_AUTO_DEBUG_MSG("pciAutoBusConfig: PF Mem Base [0x%lx]\n",
				debugTmp, 0, 0, 0, 0, 0);
	}

	/* Pre-configure 16-bit I/O Base Address */

	if ((pSystem->pciIo16Size) > 0)
	{
		pci_auto_addr_align(pSystem->pciIo16,
				(pSystem->pciIo16 + pSystem->pciIo16Size),
				0x1000,
				&alignedBase);

		PCI_AUTO_DEBUG_MSG("I/O 16 Base orig[0x%08x] new[0x%lx] adj[0x%lx]\n",
				(pSystem->pciIo16),
				alignedBase,
				(alignedBase - (pSystem->pciIo16)),
				0, 
				0,
				0 );

		(pSystem->pciIo16Size) -= (alignedBase - (pSystem->pciIo16));
		(pSystem->pciIo16) = alignedBase;

		pci_modify_config_dword (pPciLoc->bus, pPciLoc->device, pPciLoc->function,
				PCI_CFG_IO_BASE, 0x000000f0, 
				(pSystem->pciIo16 >> (12-4)) );

		pci_modify_config_dword (pPciLoc->bus, pPciLoc->device, pPciLoc->function,
				PCI_CFG_IO_BASE_U, 0x0000ffff, 
				(pSystem->pciIo16 >> 16) );

		pci_read_config_dword (pPciLoc->bus, pPciLoc->device, pPciLoc->function,
				PCI_CFG_IO_BASE,
				&debugTmp);

		pci_read_config_dword (pPciLoc->bus, pPciLoc->device, pPciLoc->function,
				PCI_CFG_IO_BASE_U,
				&debugTmp2);

		debugTmp3 = (((debugTmp & (u32)0xf0) << (12-4)) & 0x0000ffff);
		debugTmp = debugTmp3 | ((debugTmp2 << 16) & 0xffff0000);

		PCI_AUTO_DEBUG_MSG("pciAutoBusConfig: IO16 Base Address [0x%08x]\n",
				debugTmp, 0, 0, 0, 0, 0);

	}

	/* Pre-configure 32-bit Non-prefetchable Memory Base Address */

	if ((pSystem->pciMemIo64Size) > 0)
	{

		pci_auto_addr_align(pSystem->pciMemIo64,
				(pSystem->pciMemIo64 + pSystem->pciMemIo64Size),
				0x100000,
				&alignedBase);

		PCI_AUTO_DEBUG_MSG("Memory Base orig[0x%08x] new[0x%lx] adj[0x%lx]\n",
				(pSystem->pciMemIo32),
				alignedBase,
				(alignedBase - (pSystem->pciMemIo32)),
				0, 
				0,
				0
		);

		(pSystem->pciMemIo64Size) -= (alignedBase - (pSystem->pciMemIo64));
		(pSystem->pciMemIo64) = alignedBase;

		pci_modify_config_dword (pPciLoc->bus, pPciLoc->device, pPciLoc->function,
				PCI_CFG_MEM_BASE, 0x0000fff0,
				(pSystem->pciMemIo64 >> (20-4))
		);

		pci_read_config_dword (pPciLoc->bus, pPciLoc->device, pPciLoc->function,
				PCI_CFG_MEM_BASE,
				&debugTmp2);

		debugTmp = ((debugTmp2 & 0x0000fff0) << 16);

		PCI_AUTO_DEBUG_MSG("pciAutoBusConfig: Mem Base Address [0x%08x]\n",
				debugTmp, 0, 0, 0, 0, 0);
	}

	/* Configure devices on the bus implemented by this bridge */

	pci_read_config_byte (pPciLoc->bus, pPciLoc->device, pPciLoc->function,
			PCI_CFG_SECONDARY_BUS, &bus);

	pci_auto_dev_config (pSystem, bus, ppPciList, nSize);

	/* Post-configure 32-bit I/O Limit Address */

	if ((pSystem->pciIo16Size) > 0)
	{

		pci_auto_addr_align(pSystem->pciIo16,
				(pSystem->pciIo16 + pSystem->pciIo16Size),
				0x1000,
				&alignedBase);

		PCI_AUTO_DEBUG_MSG("I/O 16 Lim orig[0x%08x] new[0x%lx] adj[0x%lx]\n",
				(pSystem->pciIo16),
				alignedBase,
				(alignedBase - (pSystem->pciIo16)),
				0, 
				0,
				0
		);

		(pSystem->pciIo16Size) -= (alignedBase - (pSystem->pciIo16));
		(pSystem->pciIo16) = alignedBase;

		pci_modify_config_dword (pPciLoc->bus, pPciLoc->device, pPciLoc->function,
				PCI_CFG_IO_BASE, 0x0000f000, 
				(pSystem->pciIo16 - 1)
		);

		pci_modify_config_dword (pPciLoc->bus, pPciLoc->device, pPciLoc->function,
				PCI_CFG_IO_BASE_U, 0xffff0000, 
				(pSystem->pciIo16 - 1)
		);

		pci_read_config_dword (pPciLoc->bus, pPciLoc->device, pPciLoc->function,
				PCI_CFG_IO_BASE,
				&debugTmp);

		pci_read_config_dword (pPciLoc->bus, pPciLoc->device, pPciLoc->function,
				PCI_CFG_IO_BASE_U,
				&debugTmp2);

		debugTmp3 = ((debugTmp & (u32)0xf000) & 0x0000ffff);
		debugTmp = debugTmp3 | (debugTmp2 & 0xffff0000);
		debugTmp |= 0x00000FFF;

		PCI_AUTO_DEBUG_MSG("pciAutoBusConfig: IO Limit [0x%08x]\n",
				debugTmp, 0, 0, 0, 0, 0);

	}

	/* Post-configure 32-bit Non-prefetchable Memory Limit Address */

	if ((pSystem->pciMemIo64Size) > 0)
	{

		pci_auto_addr_align(pSystem->pciMemIo64,
				(pSystem->pciMemIo64 + pSystem->pciMemIo64Size),
				0x100000,
				&alignedBase);

		PCI_AUTO_DEBUG_MSG("MemIo Lim orig[0x%08x] new[0x%lx] adj[0x%lx]\n",
				(pSystem->pciMemIo32),
				alignedBase,
				(alignedBase - (pSystem->pciMemIo32)),
				0, 
				0,
				0
		);

		(pSystem->pciMemIo64Size) -= (alignedBase - (pSystem->pciMemIo64));
		(pSystem->pciMemIo64) = alignedBase;

		pci_modify_config_dword (pPciLoc->bus, pPciLoc->device, pPciLoc->function,
				PCI_CFG_MEM_BASE, 0xfff00000,
				(pSystem->pciMemIo64 - 1)
		);

		pci_read_config_dword (pPciLoc->bus, pPciLoc->device, pPciLoc->function,
				PCI_CFG_MEM_BASE,
				&debugTmp2);

		debugTmp = (debugTmp2 & 0xfff00000);
		debugTmp |= 0x000FFFFF;

		PCI_AUTO_DEBUG_MSG("pciAutoBusConfig: MemIo Limit [0x%08x]\n",

				debugTmp, 0, 0, 0, 0, 0);
	}

	/* Post-configure 32-bit Prefetchable Memory Address */

	if ( (pPciLoc->attribute & PCI_AUTO_ATTR_BUS_PREFETCH) && 
			((pSystem->pciMem64Size) > 0) )
	{

		pci_auto_addr_align(pSystem->pciMem64,
				(pSystem->pciMem64 + pSystem->pciMem64Size),
				0x100000,
				&alignedBase);

		PCI_AUTO_DEBUG_MSG("PF Lim orig[0x%08x] new[0x%lx] adj[0x%lx]\n",
				(pSystem->pciMem64),
				alignedBase,
				(alignedBase - (pSystem->pciMem64)),
				0, 
				0,
				0
		);

		(pSystem->pciMem64Size) -= (alignedBase - (pSystem->pciMem64));
		(pSystem->pciMem64) = alignedBase;

		/* 64-bit Prefetchable memory not supported at this time */

		pci_write_config_dword (pPciLoc->bus, pPciLoc->device, pPciLoc->function,
				PCI_CFG_PRE_MEM_LIMIT_U, 0);

		pci_modify_config_dword (pPciLoc->bus, pPciLoc->device, pPciLoc->function,
				PCI_CFG_PRE_MEM_BASE, 0xfff00000,
				(pSystem->pciMem64 - 1)
		);

		pci_read_config_dword (pPciLoc->bus, pPciLoc->device, pPciLoc->function,
				PCI_CFG_PRE_MEM_BASE,
				&debugTmp2);

		debugTmp = (debugTmp2 & 0xfff00000);
		debugTmp |= 0x000FFFFF;

		PCI_AUTO_DEBUG_MSG("pciAutoBusConfig: PF Mem Limit [0x%08x]\n",
				debugTmp, 0, 0, 0, 0, 0);
	}

	if ((pSystem->bridgePostConfigInit) != NULL )
	{
		pci_read_config_dword (pPciLoc->bus, pPciLoc->device, pPciLoc->function,
				PCI_CFG_VENDOR_ID, &dev_vend);

		(pSystem->bridgePostConfigInit) (pSystem, pPciLoc, dev_vend);
	}

	/* Initialize primary and secondary PCI-PCI bridge latency timers */

	pci_write_config_byte (pPciLoc->bus, pPciLoc->device, pPciLoc->function, 
			PCI_CFG_SEC_LATENCY, pSystem->maxLatency);

	/* Clear status bits turn on downstream and upstream (master) mem,IO */

	/* 
	 * Iying 2021-11-22 : Setting PCI_CFG_MIN_GRANT register (actually it is bridge control register)
	 * will cause failure when to access IO space of PCI devices. So keep this it as its default value 
	 * (usually zero) after PCI bus reset. 
	 *
	 * From PCI bridge SPEC 1.2: If the VGA Enable bit is set, forwarding of these accesses is independent
	 * of the I/O address range and memory address ranges defined by the I/O Base and Limit registers
	 * of the bridge.
	 */

	//    pci_write_config_word (pPciLoc->bus, pPciLoc->device, pPciLoc->function,
	//                      PCI_CFG_MIN_GRANT,0x4);

	pci_write_config_dword (pPciLoc->bus, pPciLoc->device, pPciLoc->function,
			PCI_CFG_COMMAND,
			(u32)(0xffff0000 | PCI_CMD_IO_ENABLE |
					PCI_CMD_MEM_ENABLE | PCI_CMD_MASTER_ENABLE)
	);
}


/******************************************************************************
 *
 * pci_auto_bus_config - 本函数用于pci地址空间，并检查是否越界

 * 返回值：
 * 	    无
 */
int pci_auto_addr_align
(
		u64 base,
		u64 limit,
		u32 reqSize,
		u64 *pAlignedBase
)
{
	u32 sizeMask;
	u32 alignAdjust;

	/* make the size mask */

	sizeMask = reqSize - 1;

	PCI_AUTO_DEBUG_MSG("pciAutoAddrAlign: sizemask[0x%lx]\n",sizeMask,0,0,0,0,0);

	/* see if the address needs to be adjusted */

	if ((base & sizeMask) > 0)
	{
		alignAdjust = reqSize - (base & sizeMask);
		PCI_AUTO_DEBUG_MSG ("pciAutoAddrAlign: adjustment [0x%lx]\n",alignAdjust,
				0,0,0,0,0);
	}
	else
	{
		PCI_AUTO_DEBUG_MSG("pciAutoAddrAlign: already aligned\n",0,0,0,0,0,0);
		alignAdjust = 0;
	}

	/* see if the aligned base exceeds the resource boundary */

	if (((base + alignAdjust) < base) ||
			((base + alignAdjust) > limit) )
	{
		PCI_AUTO_DEBUG_MSG ("pciAutoAddrAlign: base + adjustment [0x%lx]"
				" exceeds limit [0x%lx]\n", (base + alignAdjust),
				limit,
				0,0,0,0);
		return ERROR;
	}

	*pAlignedBase = base + alignAdjust;

	/* see if the aligned base+size exceeds the resource boundary */

	if ( ((base + alignAdjust + reqSize) < base) || 
			((base + alignAdjust + reqSize) > limit) )
	{
		PCI_AUTO_DEBUG_MSG ("pciAutoAddrAlign: base + adjustment + req [0x%lx]"
				" exceeds limit [0x%lx]\n",
				(base + alignAdjust + reqSize),limit,0,0,0,0);
		return ERROR;
	}

	PCI_AUTO_DEBUG_MSG ("pciAutoAddrAlign: new aligned base [0x%lx]\n",
			(base + alignAdjust),0,0,0,0,0);

	return OK;
}
/******************************************************************************
 *
 * ls2h_pcie_bios_init - 本函数用于设置PCI Express专用配置寄存器.
 *
 * 输入：
 *	bus 		总线号
 *	dev 		设备号
 *	func 		功能号
 * 输出：
 *      无
 * 返回值：
 * 	    		0		成功
 * 	    		-1		失败
 */

int ls2h_pcie_bios_init(int bus, int dev, int func)
{
	if((bus < 0 || bus > 255) || (dev < 0 || dev > 31) || (func < 0 || func > 7)){
		printk("%s:Illegal Parameter!\n");
		return -1;
	}
	u8 pos;
	u16 max_payload_spt,control;

	//liano
	if(pci_config_extcap_find(0x10, bus,dev,func,(unsigned char *)&pos))
	{
		printk(":pci_config_extcap_find(%d,%d,%d) return -1!\n", bus,dev,func);
		return -1;
	}
	pos -= 2;
	//    pci_read_config_word(bus,dev,func, pos + 4, &max_payload_spt);
	//    max_payload_spt &= 0x07;
	//    printk("(%d,%d,%d) pos:0x%x|max_payload_spt:0x%x\n",bus,dev,func,pos,max_payload_spt);

	max_payload_spt = 0;

	pci_read_config_word(bus, dev, func, pos + 8, &control);
	//    printk("(%d,%d,%d) ori:control[0x%x]\n",bus,dev,func,control);

	control &= (~0x00e0 & ~0x7000);
	control |= (1<<5)|(1 << 12);
	pci_write_config_word(bus, dev, func, pos + 8, control);

	u16 test;
	pci_read_config_word(bus, dev, func, pos+8, &test);
	printk("(%d,%d,%d) conrol:0x%x|test:0x%x\n",bus,dev,func,control,test);


	return 0;
}

/******************************************************************************
 *
 * ls2h_pcie_to_pci_bridge_delay - 配置PCIE-PCI桥下游的PCI总线上的设备之前，执行一段延时（40ms）.
 *
 * 此函数在pci配置程序中的优先级最高:
 *
 * 输入：
 *		无
 * 输出：
 *      	无
 * 返回值：
 * 	   	 无
 */
void ls2h_pcie_to_pci_bridge_delay()
{
	usleep(40000);
}


/*从pci先序遍历产生的pci树中，获取每个pcie控制器下的子树，并保持pcie子树的先序遍历性*/
PCI_LOC* ls2k_filter_pci_list(int controller,int port,PCI_LOC* pPciList,int* pListSize)
{
	int i;
	int start = -1;
	int end = -1;
	PCI_LOC* pRetPciList;
	PCI_LOC* pNewPciList;
	int pRetSize;

	pNewPciList = (PCI_LOC*)malloc(sizeof(PCI_LOC) *  PCI_AUTO_MAX_FUNCTIONS);
	pRetPciList = pNewPciList;
	if (pRetPciList == NULL)
	{
		return NULL;
	}

	for(i=0;i<*pListSize;i++)
	{
		if (0 == pPciList[i].bus /*&& (0x9+controller*4+port) == pPciList[i].device*/)/*ldf 20230908 ignore*/
		{	
			start = i;
			continue;
		}

		if (-1 != start)
		{
			if (0 == pPciList[i].bus || i == *pListSize-1)
			{
				end = i;
				break;
			}
		}
	}

	if (end-start <= 1 || -1 == start || -1 == end)
	{	
		free(pNewPciList);
		pNewPciList = NULL;
		*pListSize = 0;
		return NULL;
	}

	memcpy (pNewPciList, &pPciList[start], sizeof (PCI_LOC)*(end-start));
	*pListSize = end-start;

	return pRetPciList;
}

