/*********************************************************************************************************
**描        述: hr3 处理器 PCIEx16 驱动.
*********************************************************************************************************/
#include "taskLib.h" 
#include "semLib.h"
#include "vxCpuLib.h"
#include "hr3cdnsPci.h"
#include <irq.h>
#include "tickLib.h" 
#include <sys/types.h>
#include "tool/hr3SysTools.h"
#include "bsp_macro.h"
#include "tool/hr3Macro.h"
#include "vcscpumacro.h"
#include "pcie.h"
#include "../txgbe/pcie_msi.h"

extern const int hr3_board_type;
extern int sys_pci_config_read(int bus,int dev,int func,int reg,int size,void *pResult);
extern int sys_pci_config_write(int bus,int dev,int func,int reg,int size,u32 data);
extern int get_link_status_x16link0(void);

#if 0/*xxx ldf 20230911 note:: 这是之前调试VPX板时，两片DSP的pcie互连的开窗，这部分暂时先保留*/
static u32 ep_cfg_size = 0;//size = 2^ep_cfg_size
static u32 ep_mem_size = 0;//size = 2^ep_mem_size, EP BAR0, 这三者等价: rc的outbound大小  = ep的inbound大小 = ep的mem空间大小
static u64 ep_cfg_axi_addr = 0;//0x30000000
static u64 ep_mem_axi_addr = 0;//0x100000000;//RC OB
static u64 ep_cfg_pcie_addr = 0;//ep_cfg_axi_addr;
static u64 ep_mem_pcie_addr = 0;//ep_mem_axi_addr;
static u64 ep_ib_axi_addr = 0;

static u32 ep_ob_size2 = 0;//要包含0x1f4508fc寄存器在内
static u64 ep_ob_axi_addr2 = 0;//ep_cfg_axi_addr + (1<<ep_cfg_size);//0x32000000;//ep的outbound空间axi地址 等价 rc的mem空间axi地址
static u64 rc_ib_axi_addr2 = 0;
static u64 ep_ob_pcie_addr2 = 0;//ep_ob_axi_addr2;//RC BAR7, 必须为32位地址

static u32 ep_ob_size = 0;//这三者等价: ep的outbound大小 = rc的inbound大小 = rc的mem空间大小
static u64 ep_ob_axi_addr = 0;//0x180000000;//ep的outbound空间axi地址 等价 rc的mem空间axi地址
static u64 rc_ib_axi_addr = 0;//0x28c0000000;
static u64 ep_ob_pcie_addr = 0;//ep_ob_axi_addr;//RC BAR0+BAR1, 可以为64位地址
#endif

/*********************************************************************************************************
  PCI 调试信息相关
*********************************************************************************************************/
unsigned int s_pcieMsiSign      = 0;
static float s_pcieTotalSndSize = 0.0;                                  /*  用于记录 PCIE DMA 传输      */
                                                                        /*  (DMA 读和写)的总数据量 (B)  */
static float s_pcieTotalRecSize = 0.0;                                  /*  用于记录 PCIE DMA 传输      */
                                                                        /*  (DMA 接收)的总数据量 (B)    */
static int   s_pcieDmaWrState   = 0;                                    /*  用于记录 PCIE DMA 写传输状态*/
static int   s_pcieDmaRdState   = 0;                                    /*  用于记录 PCIE DMA 读传输状态*/
/*********************************************************************************************************
  自旋锁操作
*********************************************************************************************************/
//#define VXB_HR2DWCPCI_SPIN_LOCK_ISR_TAKE(pDrvCtrl)            \
//    SPIN_LOCK_ISR_TAKE(&pDrvCtrl->spinlockIsr)
//#define VXB_HR2DWCPCI_SPIN_LOCK_ISR_GIVE(pDrvCtrl)            \
//    SPIN_LOCK_ISR_GIVE(&pDrvCtrl->spinlockIsr)
/*********************************************************************************************************
  PCI 驱动控制块
*********************************************************************************************************/


//#define PCIEX16_0_CFG_BASE			0x30000000
//#define PCIEX16_0_MEM_BASE          0x32000000

//#if 1
//#define PCIEX16_0_PCIE_ADDR_CFG     0x30000000
//#define PCIEX16_0_PCIE_ADDR_MEM     0x32000000
//#define PCIEX16_0_MEM_BASE_EP  		0
//#define PCIEX16_0_PCIE_ADDR_MEM_EP  0
//#else
//#define PCIEX16_0_PCIE_ADDR_CFG     	0
//#define PCIEX16_0_PCIE_ADDR_MEM    	 	(1<<24)
//#define PCIEX16_0_PCIE_ADDR_MEM_EP     	0
//#endif


#define PCIEX16_MSI_CAP_CTRL		0x90
#define PCIEX16_MSI_CAP_LOWADDR		0x94
#define PCIEX16_MSI_CAP_HIGHADDR	0x98
#define PCIEX16_MSI_CAP_DATA		0x9c


/*
"INT_LINK0",	    			(74)
"INT_LINK1",	    			(75)
"INT_LINK2",	    			(76)
"INT_LINK3",	    			(77)
"INT_PCIE_PHY",	    			(78)
*/
#ifndef _HC3020B_
#define PCIE_X16_INT_LINK0		INT_LINK0 /*xxx ldf 20230920 note:: 3020b没有pciex16*/
#define PCIE_X16_INT_LINK1		INT_LINK1
#define PCIE_X16_INT_LINK2		INT_LINK2
#define PCIE_X16_INT_LINK3		INT_LINK3
#else/*xxx ldf 20230920 note:: 3020b没有pciex16*/
#define PCIE_X16_INT_LINK0		0 
#define PCIE_X16_INT_LINK1		0
#define PCIE_X16_INT_LINK2		0
#define PCIE_X16_INT_LINK3		0
#endif

//current link up:
extern u32 pciex16_cur_link_en;
extern u32 pciex16_cur_link_addr;
extern u32 pciex16_cur_link_misc_addr;




//typedef int (*pfunc_pciCfgRead)(int iBus, int iDev, int iFun, int iOft, int iLen, void * pvRet);
//typedef int (*pfunc_pciCfgWrite)(int iBus, int iDev, int iFun, int iOft, int iLen, UINT32 uiData);
//typedef int (*pfunc_pciirqGet)(int iBus, int iDev, int iFun, int iMsiEn, int iLine, int iPin, void *  pvIrq);
//typedef struct{
//	pfunc_pciCfgRead 	cfgRead;
//	pfunc_pciCfgWrite 	cfgWrite;
//	pfunc_pciirqGet 	irqGet;
//} PCI_DRV_FUNCS0;
//typedef struct {
//    PCI_DRV_FUNCS0      HR3PCI_drvFuncs0;
//    PCI_CTRL_CB         HR3PCI_tCtrl;
//} __HR3_PCI_DRV_CB;
//typedef __HR3_PCI_DRV_CB   *__HR3_PCI_DRV_HANDLE;

struct rc_iatu_config
{
    UINT32              ob_mem_lower_base_addr;
    UINT32              ob_mem_upper_base_addr;
    UINT32              ob_mem_size;
    UINT32              ob_mem_pcie_addr;
    UINT32              ib_mem_pcie_addr;
    UINT32              ib_mem_size;
    UINT32              ib_mem_base_addr;
};

struct ep_iatu_config
{
    UINT32              bar0_mem_base_addr;                             /*  bar0 对应的 memory 地址空间 */
    UINT32              bar1_mem_base_addr;                             /*  bar1 对应的 memory 地址空间 */
    UINT32              bar2_mem_base_addr;                             /*  bar2 对应的 memory 地址空间 */
    UINT32              bar3_mem_base_addr;                             /*  bar3 对应的 memory 地址空间 */
    UINT32              ob_mem_base_addr;
    UINT32              ob_mem_size;
    UINT32              ob_mem_pcie_addr;
};

typedef struct dwc_iatu
{
    UINT8               index;
    UINT8               region_dir;

    UINT8               type;
    UINT8               msg_code;
    UINT8               bar_num;
    UINT8               cfg_shift_mode;
    UINT8               match_mode;
    UINT8               region_enable;

    UINT32              lower_base_addr;
    UINT32              upper_base_addr;
    UINT32              limit_addr;
    UINT32              lower_target_addr;
    UINT32              upper_target_addr;
} DWC_IATU;
/*********************************************************************************************************
  全局变量
*********************************************************************************************************/
//static __HR3_PCI_DRV_CB       _G_tHr3PciDrv;
//static HR2DWCPCI_DRV_CTRL     *_G_pDrvCtrl;
static unsigned int           pcieDevType = 0; //0:EP, 1:RC

struct rc_iatu_config          rc_iatu_config_val;
struct ep_iatu_config          ep_iatu_config_val;
/*********************************************************************************************************
  锁与回调函数
*********************************************************************************************************/
SEM_ID semID_PCIE_WR;                                                   /*  同步 DMA 写与回调函数       */
SEM_ID semID_PCIE_RD;                                                   /*  同步 DMA 读与回调函数       */
SEM_ID semID_PCIE_REC;                                                  /*  同步 DMA 接收与回调函数    */
SEM_ID semID_PCIE_WR_Lock;
SEM_ID semID_PCIE_RD_Lock;
SEM_ID semID_PCIE_MEM_WR; /*ldf 20230601 add:: 同步 MEM space写与回调函数*/
SEM_ID semID_PCIE_MEM_RD; /*ldf 20230601 add:: 同步 MEM space读与回调函数*/


PFUNC_PCIE_CBACK hr_bsl_pcie_dma_recv_callback_func_ptr    = NULL;
int         hr_bsl_pcie_dma_recv_callback_func_param0 = 0;                       /*  发送方 CPU ID 号          */
int         hr_bsl_pcie_dma_recv_callback_func_param1 = 0;                       /*  接收方偏移地址              */
int         hr_bsl_pcie_dma_recv_callback_func_param2 = 0;                       /*  接收数据包大小              */

PFUNC_PCIE_CBACK hr_bsl_pcie_msi_recv_callback_func_ptr    = NULL;
void*         hr_bsl_pcie_msi_recv_callback_func_param0 = 0;                       
void*         hr_bsl_pcie_msi_recv_callback_func_param1 = 0;                       
void*         hr_bsl_pcie_msi_recv_callback_func_param2 = 0;                       


#if 0
/*********************************************************************************************************
** 函数名称: set_rc_iatu
** 功能描述:
** 输  入  : NONE
** 输  出  : NONE
** 返  回  : NONE
*********************************************************************************************************/
void  set_rc_iatu (DWC_IATU  *iatu)
{
    PCIE_CFG_REG_SET_4B(PCIE_PL_IATU_INDEX,
                        iatu->index | (iatu->region_dir << 31));
    PCIE_CFG_REG_SET_4B(PCIE_PL_IATU_CTRL_1, iatu->type);
    PCIE_CFG_REG_SET_4B(PCIE_PL_IATU_CTRL_2,
                        (iatu->msg_code        & 0xff)       |
                        ((iatu->bar_num        & 0x7) << 8)  |
                        ((iatu->cfg_shift_mode & 0x1) << 28) |
                        ((iatu->match_mode     & 0x1) << 30) |
                        ((iatu->region_enable)        << 31));
    PCIE_CFG_REG_SET_4B(PCIE_PL_IATU_BASE_L, iatu->lower_base_addr);
    PCIE_CFG_REG_SET_4B(PCIE_PL_IATU_BASE_H, iatu->upper_base_addr);
    PCIE_CFG_REG_SET_4B(PCIE_PL_IATU_LIMIT,  iatu->limit_addr);
    PCIE_CFG_REG_SET_4B(PCIE_PL_IATU_TGT_L,  iatu->lower_target_addr);
    PCIE_CFG_REG_SET_4B(PCIE_PL_IATU_TGT_H,  iatu->upper_target_addr);
}
/*********************************************************************************************************
** 函数名称: set_rc_iatu
** 功能描述: cfg iatu address should be 0x1810000000, since we should use
**           MIN_CFG register application logic to create config0/config1
**           pci request, which will use less memory space.
**           Otherwise we will use at least 28 bits address to map memory
**           address to pci region address, because BDF and register off-
**           set which should be in cfg0/cfg1 request will take 28 bits
**           space.
** 输  入  : NONE
** 输  出  : NONE
** 返  回  : NONE
*********************************************************************************************************/
void  set_rc_ob_cfg_iatu (E_CFG_TYPE type)
{
    DWC_IATU   iatu;
    memset(&iatu, 0, sizeof(iatu));

    iatu.index           = IATU_OB_CFG_INDEX;
    iatu.region_dir      = IATU_REGION_OUTBOUND;
    iatu.type            = type;
    iatu.lower_base_addr = 0x10000000;
    iatu.upper_base_addr = 0x18;
    iatu.limit_addr      = 0x1fffffff;                                  /*  corresponding to the        */
                                                                        /*  lower_base_addr of iatu     */
    iatu.cfg_shift_mode  = 1;
    iatu.region_enable   = 1;

    set_rc_iatu(&iatu);
}


void set_ep_iatu (DWC_IATU  *iatu)
{
    PCIE_CFG_REG_SET_4B(PCIE_PL_IATU_INDEX + 0x100000,
                        iatu->index | (iatu->region_dir << 31));
    PCIE_CFG_REG_SET_4B(PCIE_PL_IATU_CTRL_1 + 0x100000, iatu->type);
    PCIE_CFG_REG_SET_4B(PCIE_PL_IATU_CTRL_2 + 0x100000,
                        (iatu->msg_code        & 0xff)       |
                        ((iatu->bar_num        & 0x7) << 8)  |
                        ((iatu->cfg_shift_mode & 0x1) << 28) |
                        ((iatu->match_mode * 0x1)     << 30) |
                        ((iatu->region_enable)        << 31));
    PCIE_CFG_REG_SET_4B( PCIE_PL_IATU_BASE_L + 0x100000, iatu->lower_base_addr);
    PCIE_CFG_REG_SET_4B( PCIE_PL_IATU_BASE_H + 0x100000, iatu->upper_base_addr);
    PCIE_CFG_REG_SET_4B( PCIE_PL_IATU_LIMIT  + 0x100000, iatu->limit_addr);
    PCIE_CFG_REG_SET_4B( PCIE_PL_IATU_TGT_L  + 0x100000, iatu->lower_target_addr);
    PCIE_CFG_REG_SET_4B( PCIE_PL_IATU_TGT_H  + 0x100000, iatu->upper_target_addr);
}

void  set_ep_ob_iatu (UINT8   ucIndex,      UINT8   ucTlpType,
                      UINT32  uiAxiAddress, UINT32  uiPciAddress, UINT32  uiSize)
{
    DWC_IATU  iatu;

    memset(&iatu, 0, sizeof(iatu));

    iatu.index             = ucIndex;
    iatu.region_dir        = IATU_REGION_OUTBOUND;
    iatu.type              = ucTlpType;
    iatu.lower_base_addr   = (UINT32)uiAxiAddress;
    iatu.limit_addr        = ((UINT32)(uiAxiAddress + uiSize - 1));
    iatu.lower_target_addr = (UINT32)uiPciAddress;
    iatu.region_enable     = 1;

    set_ep_iatu(&iatu);
}

void  set_ep_ib_iatu_with_bar (UINT8  ucIndex, UINT32  uiAxiAddress, UINT8  ucBarNum)
{
    struct dwc_iatu  iatu;
    memset(&iatu, 0, sizeof(iatu));

    iatu.index             = ucIndex;
    iatu.region_dir        = IATU_REGION_INBOUND;
    iatu.lower_target_addr = uiAxiAddress;
    iatu.bar_num           = ucBarNum;
    iatu.match_mode        = IATU_BAR_MATCH_MODE;
    iatu.region_enable     = 1;

    set_ep_iatu(&iatu);
}

static void  set_rc_ib_iatu (UINT8   ucIndex,      UINT8   ucTlpType,
                             UINT32  uiAxiAddress, UINT32  uiPciAddress, UINT32  uiSize)
{
    struct dwc_iatu iatu;

    memset(&iatu, 0, sizeof(iatu));

    iatu.index             = ucIndex;
    iatu.region_dir        = IATU_REGION_INBOUND;
    iatu.type              = ucTlpType;
    iatu.lower_base_addr   = uiPciAddress;
    iatu.limit_addr        = uiPciAddress + (uiSize - 1);
    iatu.lower_target_addr = uiAxiAddress;
    iatu.region_enable     = 1;

    set_rc_iatu(&iatu);
}

void set_rc_ob_iatu (UINT8   ucIndex,           UINT8   ucTlpType,
                     UINT32  uiAxiLowerAddress, UINT32  uiAxiUpperAddress,
                     UINT32  uiPciAddress,      UINT32  uiSize)
{
    DWC_IATU iatu;

    memset(&iatu, 0, sizeof(iatu));

    iatu.index             = ucIndex;
    iatu.region_dir        = IATU_REGION_OUTBOUND;
    iatu.type              = ucTlpType;
    iatu.lower_base_addr   = (UINT32)uiAxiLowerAddress;
    iatu.upper_base_addr   = (UINT32)uiAxiUpperAddress;
    iatu.limit_addr        = ((UINT32)(uiAxiLowerAddress + uiSize  - 1));
    iatu.lower_target_addr = (UINT32)uiPciAddress;
    iatu.region_enable     = 1;

    set_rc_iatu(&iatu);
}

int  init_rc_iatu_default_value (void)
{
    rc_iatu_config_val.ob_mem_lower_base_addr = 0x18200000;
    rc_iatu_config_val.ob_mem_size            = 0x1400000;
    rc_iatu_config_val.ob_mem_pcie_addr       = 0x18200000;
    rc_iatu_config_val.ib_mem_pcie_addr       = 0x30000000;
    rc_iatu_config_val.ib_mem_size            = 0x200000;
    rc_iatu_config_val.ib_mem_base_addr       = 0x10000000;             /*  norflash base addr          */

    return  (OK);
}

int  init_ep_iatu_default_value (void)
{
    ep_iatu_config_val.ob_mem_base_addr   = 0x18800000;
    ep_iatu_config_val.ob_mem_size        = 0x100000;
    ep_iatu_config_val.ob_mem_pcie_addr   = 0x30000000;

    ep_iatu_config_val.bar0_mem_base_addr = 0x200000;                   /*  vxWorks load addr           */
    ep_iatu_config_val.bar1_mem_base_addr = 0x300000;
    ep_iatu_config_val.bar2_mem_base_addr = 0x400000;
    ep_iatu_config_val.bar3_mem_base_addr = 0x500000;

    return  (OK);
}

static int  changePcieBusNum (unsigned int  uiBusNum)
{
    *(volatile unsigned int *)(PCIE_APP_BASE_ADDR + APP_LOGIC_MIN_CFG) = (uiBusNum << 8);

    return  (OK);
}
/*********************************************************************************************************
** 函数名称: __epBarInboundSet
** 功能描述: epBar Inbound 设置
** 输　入  : NONE
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
static int  __epBarInboundSet (unsigned int  uiBusNum, unsigned int  uiAxiAddr)
{
    int  iStatus = OK;

    /*
     *  此处必须设置 conifg outbound 窗口为 TYPE1 模式，
     *  否则应用通过设置 BDF 不能切换总线号
     */
    set_rc_ob_cfg_iatu(TLP_TYPE_CFG1);

    changePcieBusNum(uiBusNum);                                         /*  切换至 bus5(芯片 B)         */

    if (*(unsigned int *)PCIE_EP_CFG_BASE_ADDR == PCIE_DEVICE_ID) {

        /*
         *  set ep inbound(bar match) for application
         */
        set_ep_ib_iatu_with_bar(0, uiAxiAddr, 0);

    } else {

        //mylog(MYLOG_PCIE, 3, __FUNCTION__, __LINE__, "can't find the PCIE ctrl of Chip B.");

        iStatus = ERROR;
    }

    set_rc_ob_cfg_iatu(TLP_TYPE_CFG0);

    return  iStatus;
}



static int __allEPsIATUSet (void)
{
    int  iStatus = 0;

    /*
     *  此处必须设置 conifg outbound 窗口为 TYPE1 模式，
     *  否则应用通过设置 BDF 不能切换总线号
     */
    set_rc_ob_cfg_iatu(TLP_TYPE_CFG1);

#if BSP_CFG_HAS_OTHER_PROC
    changePcieBusNum(5);                                                /*  切换至bus5(芯片 B)          */

    if (*(unsigned int *)PCIE_EP_CFG_BASE_ADDR == PCIE_DEVICE_ID) {             /*  对 B 片处理                 */
        *(unsigned int *)PUBIT_DSPB_DETECTED_ADDR = 0;                          /*  for bit info                */
        /*
         *  set ep outbound 0 for transfer data with chip A
         */
        set_ep_ob_iatu(0, TLP_TYPE_MEM,
                       0x18700000, 0x18700000, 0x100000);               /*  set ep outbound 0 for IPI   */
        set_ep_ob_iatu(1, TLP_TYPE_MEM,
                       0x18300000, 0x18300000, 0x100000);               /*  set ep outbound 1 for       */
                                                                        /*  uart changing               */
        set_ep_ob_iatu(2, TLP_TYPE_MEM,
                       0x18600000, 0x18600000, 0x100000);               /*  set ep outbound 2 for       */
                                                                        /*  transfer data with chip C   */

        /*
         *  set ep inbound(bar match) for application
         */
        set_ep_ib_iatu_with_bar(0, 0x20000000, 0);
        /*
         *  set ep inbound(bar match) for IPI
         */
        set_ep_ib_iatu_with_bar(1, 0x1f070000, 1);

    } else {                                                            /*  没有找到 B 片               */
        _DebugFormat(__PRINTMESSAGE_LEVEL,
                     "warning: can't find the PCIE ctrl of Chip B.\r\n");

        *(unsigned int *)PUBIT_DSPB_DETECTED_ADDR = 1;                          /*  for bit info                */

        iStatus = PX_ERROR;
    }

    changePcieBusNum(2);                                                /*  切换至bus2(芯片 C)          */
    if (*(unsigned int *)PCIE_EP_CFG_BASE_ADDR == PCIE_DEVICE_ID) {             /*  对 C 片处理                 */
        *(unsigned int *)PUBIT_DSPC_DETECTED_ADDR = 0; /* for bit info */
        /*
         *  set ep outbound 0 for transfer data with chip B
         */
        set_ep_ob_iatu(0, TLP_TYPE_MEM,
                       0x18b00000, 0x18b00000, 0x100000);               /*  set ep outbound 0 for IPI   */
        set_ep_ob_iatu(1, TLP_TYPE_MEM,
                       0x18300000, 0x18300000, 0x100000);               /*  set ep outbound 1 for       */
                                                                        /*  uart changing               */
        set_ep_ob_iatu(2, TLP_TYPE_MEM,
                       0x18a00000, 0x18a00000, 0x100000);               /*  set ep outbound 2 for       */
                                                                        /*  transfer data with chip D   */

        /*
         *  set ep inbound(bar match) for application
         */
        set_ep_ib_iatu_with_bar(0, 0x20000000, 0);
        /*
         *  set ep inbound(bar match) for IPI
         */
        set_ep_ib_iatu_with_bar(2, 0x1f000000, 2);

    } else {                                                            /*  没有找到 C 片               */
        _DebugFormat(__PRINTMESSAGE_LEVEL,
                     "warning: can't find the PCIE ctrl of Chip C.\r\n");

        *(unsigned int *)PUBIT_DSPC_DETECTED_ADDR = 1;                          /*  for bit info                */

        iStatus = PX_ERROR;
    }

    changePcieBusNum(3);                                                /*  切换至bus3(芯片 D)          */
    if (*(unsigned int *)PCIE_EP_CFG_BASE_ADDR == PCIE_DEVICE_ID) {             /*  对 D 片处理                 */
        *(unsigned int *)PUBIT_DSPD_DETECTED_ADDR = 0;                          /*  for bit info                */
        /*
         *  set ep outbound 0 for transfer data with chip C
         */
        set_ep_ob_iatu(0, TLP_TYPE_MEM,
                       0x19f00000, 0x31000000, 0x100000);               /*  set ep outbound 0 for IPI   */
        set_ep_ob_iatu(1, TLP_TYPE_MEM,
                       0x18300000, 0x18300000, 0x100000);               /*  set ep outbound 1 for       */
                                                                        /*  uart changing               */
        set_ep_ob_iatu(2, TLP_TYPE_MEM,
                       0x19200000, 0x30000000, 0x100000);               /*  set ep outbound 2 for       */
                                                                        /*  transfer data with chip A   */

        /*
         *  set ep inbound(bar match) for application
         */
        set_ep_ib_iatu_with_bar(0, 0x20000000, 0);
        /*
         *  set ep inbound(bar match) for IPI
         */
        set_ep_ib_iatu_with_bar(1, 0x1f070000, 1);

    } else {                                                            /*  没有找到 D 片               */
        _DebugFormat(__PRINTMESSAGE_LEVEL,
                     "warning: can't find the PCIE ctrl of Chip D.\r\n");

        *(unsigned int *)PUBIT_DSPD_DETECTED_ADDR = 1;                          /*  for bit info                */

        iStatus = PX_ERROR;
    }
#endif                                                                  /*  BSP_CFG_HAS_OTHER_PROC      */

#if BSP_CFG_HAS_FPGA
    changePcieBusNum(6);                                                /*  切换至bus6(FPGA)            */
    udelay(1);
    if (*(unsigned int *)PCIE_EP_CFG_BASE_ADDR == PCIE_FPGA_DEVICE_ID) {        /*  对 FPGA 片处理              */
        *(unsigned int *)PUBIT_ZYNQ_DETECTED_ADDR = 0;                          /*  for bit info                */

    } else {
        _DebugFormat(__PRINTMESSAGE_LEVEL,
                     "warning: can't find the PCIE ctrl of FPGA.\r\n"); /*  没有找到 FPGA 片            */

        *(unsigned int *)PUBIT_ZYNQ_DETECTED_ADDR = 1;                          /*  for bit info                */
        iStatus = ERROR;
    }
#endif                                                                  /*  BSP_CFG_HAS_FPGA            */

    set_rc_ob_cfg_iatu(TLP_TYPE_CFG0);

    return  iStatus;
}

/* Initiated by Local CPU (Non LL mode) begin */
static void  initDMARead (void)
{
    UINT32  uiStatus = 0;

    PCIE_CFG_REG_SET_4B(PCIE_PL_DMA_RD_ENGINE_ENABLE, 0x1);
    PCIE_CFG_REG_SET_4B(PCIE_PL_DMA_RD_INT_MASK,      0x0);

    /*
     *  set the initial read channel status 00
     */
    PCIE_CFG_REG_SET_4B(PCIE_PL_DMA_CHANNEL_CONTEXT_INDEX, 0x80000000);
    uiStatus = PCIE_CFG_REG_GET_4B(PCIE_PL_DMA_CHANNEL_CTRL_1);
    PCIE_CFG_REG_SET_4B(PCIE_PL_DMA_CHANNEL_CTRL_1, uiStatus & 0xffffff9f);
}

static void  initDMAWrite (void)
{
    UINT32  uiStatus = 0;

    PCIE_CFG_REG_SET_4B(PCIE_PL_DMA_WR_ENGINE_ENABLE, 0x1);
    PCIE_CFG_REG_SET_4B(PCIE_PL_DMA_WR_INT_MASK,      0x0);

    /*
     *  set the initial write channel status 00
     */
    PCIE_CFG_REG_SET_4B(PCIE_PL_DMA_CHANNEL_CONTEXT_INDEX, 0x0);
    uiStatus = PCIE_CFG_REG_GET_4B(PCIE_PL_DMA_CHANNEL_CTRL_1);
    PCIE_CFG_REG_SET_4B(PCIE_PL_DMA_CHANNEL_CTRL_1, uiStatus & 0xffffff9f);
}


static int  enableLinkTraining (void)
{
    UINT32     uiStatus  = 0;
    UINT32     uiLtssm   = 0;
    UINT8      ucTmpAddr = 0;
    unsigned int       t0        = 0;
    unsigned int       t1        = 0;

    uiStatus = PCIE_APP_REG_GET(APP_LOGIC_LTSSM_STATUS_H);
    if ((uiStatus & 0x10) == 0x10) {
        return  (OK);
    }

    /*
     *  start link training
     */
    uiLtssm = PCIE_APP_REG_GET(APP_LOGIC_LTSSM);

    /*
     *  enable link training
     */
    if ((uiLtssm & 0x3000) != 0) {
        //mylog(MYLOG_PCIE, 4, __FUNCTION__, __LINE__, "wrong ltssm ctrl setting by hardware!");

        PCIE_APP_REG_SET(APP_LOGIC_LTSSM, 0x6);

    } else {
        PCIE_APP_REG_SET(APP_LOGIC_LTSSM, uiLtssm | 0x4);
    }

    /*
     *  wait untile PHY link up
     */
    t0 = tickGet();
    do {
        t1 = tickGet();

        if (t1 >= t0) {
            if ((t1 - t0) >= LOOP_TIMER_OUT * sysClkRateGet()) {
            	//mylog(MYLOG_PCIE, 4, __FUNCTION__, __LINE__, "PCIe PHY link 超时.");
                break;
            }
        } else {
            if (t1 >= LOOP_TIMER_OUT * sysClkRateGet()) {
            	//mylog(MYLOG_PCIE, 4, __FUNCTION__, __LINE__, "PCIe PHY link 超时.");
                break;
            }
        }

        uiStatus = PCIE_APP_REG_GET( APP_LOGIC_LTSSM_STATUS_H);
    } while ((uiStatus & 0x10) == 0);

    /*
     *  read the pcie Capabilities registers to get pcie ctrl link status with the pcie switch
     */
    ucTmpAddr = PCIE_CFG_REG_GET_1B(0x34);                              /* point to PCI Power Management */
    ucTmpAddr = PCIE_CFG_REG_GET_1B(ucTmpAddr + 1);                     /* point to Message Signaled Interrupt */
    ucTmpAddr = PCIE_CFG_REG_GET_1B(ucTmpAddr + 1);                     /* point to PCI Express Capabilities */
    uiStatus  = PCIE_CFG_REG_GET_2B(ucTmpAddr + PCIE_LINK_STATUS);

    if (uiStatus & 0x1) {
        //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "Gen1:max link speed - 2.5Gb/s");
        return  1;

    } else if (uiStatus & 0x2) {
        //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "Gen2:max link speed - 5.0Gb/s");
        return  2;
    }

    return  (OK);
}

static int  changeSpeedToGen2 (void)
{
    UINT32  uiStatus = 0;
    UINT8   ucLwsccr = 0;
    unsigned int    t0       = 0;
    unsigned int    t1       = 0;

    /*
     *  change speed to GEN 2
     */
    ucLwsccr = PCIE_CFG_REG_GET_1B(PCIE_PL_LWSCCR);
    PCIE_CFG_REG_SET_1B(PCIE_PL_LWSCCR, ucLwsccr | 0x02);

    t0 = tickGet();
    do {
        t1 = tickGet();
        if (t1 >= t0) {
            if ((t1 - t0) >= LOOP_TIMER_OUT * sysClkRateGet()) {
                //mylog(MYLOG_PCIE, 4, __FUNCTION__, __LINE__, "读取PCIe LTSSM状态寄存器超时.");
                break;
            }
        } else {
            if (t1 >= LOOP_TIMER_OUT * sysClkRateGet()) {
            	//mylog(MYLOG_PCIE, 4, __FUNCTION__, __LINE__, "读取PCIe LTSSM状态寄存器超时.");
                break;
            }
        }

        uiStatus = PCIE_APP_REG_GET(APP_LOGIC_LTSSM_STATUS_L);
    } while ((uiStatus & LTSSM_STATE_MASK) != LTSSM_L0);

    uiStatus = PCIE_CFG_REG_GET_2B(0x70 + PCIE_LINK_STATUS);

    if (uiStatus & 0x1) {
    	//mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "Gen1:max link speed - 2.5Gb/s");
    } else if (uiStatus & 0x2) {
    	//mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "Gen2:max link speed - 5.0Gb/s");
    }

    return  (OK);
}

void  hrPcieSetInbound (unsigned int  uiDevType, unsigned int  uiAxiAddr)
{
    UINT8            ucBarNum = 0;
    struct dwc_iatu  iatu;

    memset(&iatu, 0, sizeof(iatu));

    iatu.index             = ucBarNum;
    iatu.region_dir        = IATU_REGION_INBOUND;
    iatu.lower_target_addr = uiAxiAddr;
    iatu.region_enable     = 1;

    if (uiDevType == 0) {                                               /*  ep                          */
        iatu.bar_num    = ucBarNum;
        iatu.match_mode = IATU_BAR_MATCH_MODE;
    } else {                                                            /*  rc                          */
        iatu.type              = TLP_TYPE_MEM;
        iatu.lower_base_addr   = 0x30000000;
        iatu.limit_addr        = 0x30000000 + (0x1000000 - 1);
        iatu.lower_target_addr = uiAxiAddr;
    }

    set_rc_iatu(&iatu);
}

STATUS hr2dwcPciBridgeInit(/*VXB_DEVICE_ID pInst*/)
{

    int linkStatus = 0;
    unsigned int deviceID = 0;

//    HR2DWCPCI_DRV_CTRL * pDrvCtrl = NULL;
//    pDrvCtrl = pInst->pDrvCtrl;

    deviceID = PCIE_CFG_REG_GET_4B(0x0);
    uart_printf("pcie device id: 0x");uart_printf("%x",deviceID);uart_printf("\r\n");

    PCIE_CFG_REG_SET_4B(PCI_COMMAND,
            PCI_COMMAND_MASTER | PCI_COMMAND_MEMORY | PCI_COMMAND_IO); /* added by syj 20140730 */
    //PCIE_CFG_REG_SET_4B(0x10,0x10000000);
#if 0
    PCIE_CFG_REG_SET_4B(0x50, PCIE_CFG_REG_GET_4B(0x50) | 0x10000);
    PCIE_CFG_REG_SET_4B(PCIE_PL_MSI_CTRL_ADDR_L, (0x20000000 & 0xffffffff));
    PCIE_CFG_REG_SET_4B(PCIE_PL_MSI_CTRL_INT_0_EN, 0xffffffff);
    PCIE_CFG_REG_SET_4B( 0x834, 0xffffffff);
    PCIE_CFG_REG_SET_4B( 0x840, 0xffffffff);
    PCIE_CFG_REG_SET_4B( 0x84c, 0xffffffff);
    PCIE_CFG_REG_SET_4B( 0x858, 0xffffffff);
    PCIE_CFG_REG_SET_4B( 0x864, 0xffffffff);
    PCIE_CFG_REG_SET_4B( 0x870, 0xffffffff);
    PCIE_CFG_REG_SET_4B( 0x87c, 0xffffffff);
#endif
    /* init pcie dma begin */

//    printk("<DEBUG> [%s:__%d__]\n",__FUNCTION__,__LINE__);
    initDMARead();
//    printk("<DEBUG> [%s:__%d__]\n",__FUNCTION__,__LINE__);
    initDMAWrite();
//    printk("<DEBUG> [%s:__%d__]\n",__FUNCTION__,__LINE__);
    /* init pcie dma end */

    //PCIE_APP_REG_SET(APP_LOGIC_PCI_INTR_CTRL,0xffffffff); /* enable the pcie ctrl interrupt */
    PCIE_APP_REG_SET(APP_LOGIC_PCI_INTR_CTRL,0x00080001);

    linkStatus = enableLinkTraining();

    if (linkStatus == 1)
        changeSpeedToGen2();

    return OK;
}

void hr2dwcPciBridgeHwInit
    (
    /*VXB_DEVICE_ID    pInst,*/
    int            from
    )
{

//    HR2DWCPCI_DRV_CTRL * pDrvCtrl = NULL;
//
//    pDrvCtrl = _G_pDrvCtrl;
//
//
//    if (pDrvCtrl->initDone != TRUE)
//    {
//        /* book-keeping */
//        pDrvCtrl->initDone = TRUE;

        if ( hr2dwcPciBridgeInit(/*pInst*/) != OK )
        {
                uart_printf("hr2dwcPciBridgeInit error.\r\n");
                return;
        }
        //vxbBusAnnounce(pInst, VXB_BUSID_PCI);
//    }

}
#endif

static int  getChannelStatus (void)
{
    UINT32  uiStatus = 0;

    uiStatus = PCIE_CFG_REG_GET_4B( PCIE_PL_DMA_CHANNEL_CTRL_1);
    uiStatus = (uiStatus & 0x60) >> 5 ;

    if (uiStatus == 0x1) {
        //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "DMA channel status = 0x1(running)");
    } else if (uiStatus == 0x2) {
        //mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "DMA channel status = 0x2(Halted)");
    }

    return  uiStatus;
}

void  hrPcieDMAWrite (unsigned long uiSrcAddr, unsigned long uiDstAddr, unsigned int  uiSize)
{
    unsigned short  usStat = 0;

    semTake(semID_PCIE_WR_Lock, WAIT_FOREVER);

    /*
     *  set the channel index
     */
    PCIE_CFG_REG_SET_4B(PCIE_PL_DMA_CHANNEL_CONTEXT_INDEX, 0x0);

    /*
     *  wait dma write channle untile it's leisure
     */
    usStat = getChannelStatus();
    while ((usStat == 0x1) || (usStat == 0x2)) {
        timeDelay(10);
        usStat = getChannelStatus();
    }

    /*
     *  config the dma write channel
     */
    PCIE_CFG_REG_SET_4B(PCIE_PL_DMA_CHANNEL_CTRL_1, 0x04000008);
    PCIE_CFG_REG_SET_4B(PCIE_PL_DMA_CHANNEL_CTRL_2, 0);
    PCIE_CFG_REG_SET_4B(PCIE_PL_DMA_TX_SIZE,        uiSize);
    PCIE_CFG_REG_SET_4B(PCIE_PL_DMA_SAR_L,          uiSrcAddr);
    PCIE_CFG_REG_SET_4B(PCIE_PL_DMA_SAR_H,          0x00000000);
    PCIE_CFG_REG_SET_4B(PCIE_PL_DMA_DAR_L,          uiDstAddr);
    PCIE_CFG_REG_SET_4B(PCIE_PL_DMA_DAR_H,          0x00000000);
    /*
     *  start the dma write channel
     */
    PCIE_CFG_REG_SET_4B(PCIE_PL_DMA_WR_DOORBELL,    0x0);

    s_pcieTotalSndSize = s_pcieTotalSndSize + uiSize;

    semTake(semID_PCIE_WR, WAIT_FOREVER);
}


void hrPcieDMARead( unsigned long src_addr, unsigned long dst_addr,unsigned int size)
{
    unsigned short int stat = 0;

    uart_printf("semID_PCIE_RD_Lock\r\n");
    semTake(semID_PCIE_RD_Lock,WAIT_FOREVER);
    uart_printf("semID_PCIE_RD_Lock 1\r\n");

    stat = getChannelStatus();
    /* set the channel index */
    PCIE_CFG_REG_SET_4B(PCIE_PL_DMA_CHANNEL_CONTEXT_INDEX, 0x80000000);
    /* wait dma read channle untile it's leisure */
    stat = getChannelStatus();
    while ((stat == 0x1) || (stat == 0x2))
    {
        timeDelay(10);
        stat = getChannelStatus();
    }
    /* config the dma read channel */
    PCIE_CFG_REG_SET_4B(PCIE_PL_DMA_CHANNEL_CTRL_1, 0x04000008);
    PCIE_CFG_REG_SET_4B(PCIE_PL_DMA_CHANNEL_CTRL_2, 0);
    PCIE_CFG_REG_SET_4B(PCIE_PL_DMA_TX_SIZE, size);
    PCIE_CFG_REG_SET_4B(PCIE_PL_DMA_SAR_L, src_addr);
    PCIE_CFG_REG_SET_4B(PCIE_PL_DMA_SAR_H, 0x00000000);
    PCIE_CFG_REG_SET_4B(PCIE_PL_DMA_DAR_L, dst_addr);
    PCIE_CFG_REG_SET_4B(PCIE_PL_DMA_DAR_H, 0x00000000);
    /* start the dma read channel */
    PCIE_CFG_REG_SET_4B(PCIE_PL_DMA_RD_DOORBELL, 0x0);
    s_pcieTotalSndSize = s_pcieTotalSndSize + size;
    semTake(semID_PCIE_RD, WAIT_FOREVER);

}


/*
 * 功能：默认的PCIE的DMA接收中断处理函数。
 * 输入：
 * 		cpu_id，		发送方 CPU ID 号,
 * 		offset，		接收方偏移地址
 * 		buf_len，		接收数据包大小
 * 输出：
 * 		无
 * 返回值：
 * 	 	无。
 */
int pcie_dma_recv_handler(int cpu_id,int offset,int buf_len)
{
	printk("<***DEBUG***> [%s:_%d_]:: cpu_id = 0x%x, offset = 0x%x, buf_len = 0x%x\n",__FUNCTION__,__LINE__,cpu_id,offset,buf_len);
	return 0;
}

int pcie_mem_read_recv_handler(int dspID,int offset,int buf_len)
{
	printk("<***DEBUG***> [%s:_%d_]:: dspID = 0x%x, offset = 0x%x, buf_len = 0x%x\n",__FUNCTION__,__LINE__,dspID,offset,buf_len);
	
	/*从MEM读取数据出来*/
	
	return 0;
}

/*PCIE的DMA接收中断处理函数*/
void hrPcieDmaIntTask(void)
{
    unsigned int bTrue = 1;

    while (bTrue)
    {
        semTake(semID_PCIE_REC, WAIT_FOREVER);
        s_pcieTotalRecSize = s_pcieTotalRecSize + hr_bsl_pcie_dma_recv_callback_func_param2 * 1.0;
        if (hr_bsl_pcie_dma_recv_callback_func_ptr != NULL)
        	hr_bsl_pcie_dma_recv_callback_func_ptr(hr_bsl_pcie_dma_recv_callback_func_param0,
        			hr_bsl_pcie_dma_recv_callback_func_param1,hr_bsl_pcie_dma_recv_callback_func_param2);
    }
    return;
}

/*PCIE的MSI中断处理任务,读mem空间数据*/
void hrPcieMsiIntTask(void)
{
    unsigned int bTrue = 1;

    while (bTrue)
    {
        semTake(semID_PCIE_MEM_RD, WAIT_FOREVER);
        if (hr_bsl_pcie_msi_recv_callback_func_ptr != NULL)
            hr_bsl_pcie_msi_recv_callback_func_ptr(hr_bsl_pcie_msi_recv_callback_func_param0,
                    hr_bsl_pcie_msi_recv_callback_func_param1,hr_bsl_pcie_msi_recv_callback_func_param2);
    }
    return;
}

/*
 * 功能：初始化PCIE的DMA接收中断处理函数。
 * 输入：
 * 		func，		用户函数，即PCIE DMA接收中断产生时用户要做的工作；
 * 		param0，		发送方 CPU ID 号,
 * 		param1，		接收方偏移地址
 * 		param2，		接收数据包大小
 * 输出：
 * 		无
 * 返回值：
 * 	 	无。
 */
void hrPcieDMARecInit(PFUNC_PCIE_CBACK func,int param0,int param1,int param2)
{
	hr_bsl_pcie_dma_recv_callback_func_ptr = func;
	hr_bsl_pcie_dma_recv_callback_func_param0 = param0;
	hr_bsl_pcie_dma_recv_callback_func_param1 = param1;
    hr_bsl_pcie_dma_recv_callback_func_param2 = param2;
}

void hrPcieMSIInit(PFUNC_PCIE_CBACK func,void *param0,void *param1,void *param2)
{
	hr_bsl_pcie_msi_recv_callback_func_ptr = func;
	hr_bsl_pcie_msi_recv_callback_func_param0 = param0;
	hr_bsl_pcie_msi_recv_callback_func_param1 = param1;
    hr_bsl_pcie_msi_recv_callback_func_param2 = param2;
}

/*等待DMA操作完成*/
void hrPcieDMADone(void)
{
    unsigned int size = 0;
    unsigned int t0 = 0;
    unsigned int t1 = 0;

    size = PCIE_CFG_REG_GET_4B(PCIE_PL_DMA_TX_SIZE);

    t0 = tickGet();
    while (size > 0)
    {
        t1 = tickGet();
           if(t1 >= t0)
           {
               if((t1 - t0) >= LOOP_TIMER_OUT * sysClkRateGet())
               {
                   uart_printf("[DSP]读取PCIe传输完成状态寄存器超时.\n");
                   break;
               }
           }
           else
           {
               if(t1 >= LOOP_TIMER_OUT * sysClkRateGet())
               {
                      uart_printf("[DSP]读取PCIe传输完成状态寄存器超时.\n");
                      break;
               }
           }
        size = PCIE_CFG_REG_GET_4B(PCIE_PL_DMA_TX_SIZE);
    }
}
int hrPcieDMAGetState(unsigned int transType)
{
    if(transType == 0)
        return s_pcieDmaWrState;
    else
        return s_pcieDmaRdState;

}
float hrPcieGetTransSize(unsigned int transType)
{
    float transSize = 0.0;
    if(transType == 0)
        return s_pcieTotalRecSize;
    else if(transType == 1)
        return s_pcieTotalSndSize;
    else
    {
        uart_printf("aaaaa\n");
        return transSize;
    }
}



int pciex16_device_type_probe(void)
{	
	u32 val, DevType;
	u64 msic_base_addr = get_pcie_misc_link_base_addr(pciex16_cur_link_en/*PCIE_X16_LINK0_EN*/);

	val = phx_read_u32(msic_base_addr);
	printk("<***DEBUG***> [%s:__%d__]:: val = 0x%x  (RC:0x2434, EP:0x2430)\n",__FUNCTION__,__LINE__,val);/*RC:0x2434, EP:0x2430*/
	
	if ((val & 0x4) == 0){
		DevType = 0; /*end point mode  */
	}else{
		DevType = 1; /* root complex mode */
	}
	
	return DevType;
}

void hr3dwcPciRegister(void)
{
	pcieDevType = pciex16_device_type_probe();
    
    uart_printf("pcieDevType:");
    if(pcieDevType == 1)
           uart_printf("RC.\r\n");
    else
          uart_printf("EP.\r\n");
}

void  hr3dwcPciInstInit(u32 mode)
{
    pciex16_3080_init(mode);
    hr3dwcPciRegister();
   
#if 0/*xxx ldf 20230911 note:: 这是之前调试VPX板时，两片DSP的pcie互连的开窗，这部分暂时先保留*/
/*映射关系：
 * <RC端>访问outbound地址, 即  ep_mem_axi_addr <-> ep_mem_pcie_addr <-> ep_ib_axi_addr, 即EP端的inbound地址
 * <EP端>访问outbound地址, 即  ep_ob_axi_addr <-> ep_ob_pcie_addr <-> rc_ib_axi_addr, 即RC端的inbound地址
 * 
 *                    AXI ADDR          <==>       PCIE ADDR           <==>        AXI ADDR
 * <RC-OB> 0x3000_0000-0x31ff_ffff   <==>   0x3000_0000-0x31ff_ffff   <==>  0x1a00_0000 <EP-IB> (2^25) type0 CFG
 * <RC-OB2> 0x3200_0000-0x3200_ffff <==> 0x1f45_0000-0x1f45_ffff <==> 0x1f45_0000-0x1f45_ffff <EP-IB2> (2^16) MSI
 * <RC-IB> 0x20_9000_0000-0x20_91ff_ffff <==> 0x1_8000_0000-0x1_81ff_ffff <==> 0x1_8000_0000-0x1_81ff_ffff <EP-OB> (2^25)DMA
 * <RC-IB2> 0x1f45_0000-0x1f45_ffff <==> 0x3300_0000-0x3300_ffff  <==>  0x3300_0000-0x3300_ffff <EP-OB2>  (2^16)  MSI
 * 
 * 
 * 设置outbound时，pcie地址要和axi保持一致，否则映射不成功，原因不清楚
 * */
    
//    /*EP CFG*/
//    ep_cfg_size = 25;//2^25
//    ep_cfg_axi_addr = 0x30000000;
//    ep_cfg_pcie_addr = ep_cfg_axi_addr;
//    /*RC-OB <-> EP-IB, for MSI(RC触发EP的MSI)*/
//    ep_mem_size = 16;//2^16, EP BAR0 BAR1, rc的outbound大小  = ep的inbound大小 = ep的mem空间大小
//    ep_mem_axi_addr = 0x32000000;//0x100000000;//ep的mem空间axi地址 等价 rc的outbound空间axi地址
//    ep_ib_axi_addr = PCIE_X16_LINK0_MISC_BASE_ADDR;
//    ep_mem_pcie_addr = ep_ib_axi_addr;
//    /*RC-IB <-> EP-OB, for DMA*/
//    ep_ob_size = 25;//RC BAR0最大2^25, ep的outbound大小 = rc的inbound大小 = rc的mem空间大小
//    ep_ob_axi_addr = 0x180000000;//ep的outbound空间axi地址 等价 rc的mem空间axi地址
//    ep_ob_pcie_addr = ep_ob_axi_addr;//RC BAR0+BAR1, 可以为64位地址
//    rc_ib_axi_addr = 0x2090000000;
//    /*EP-OB2 <-> RC-IB2, for MSI(EP触发RC的MSI)*/
//    ep_ob_size2 = 16;//2^16
//    ep_ob_axi_addr2 = 0x33000000;//ep的outbound空间axi地址 等价 rc的mem空间axi地址
//    ep_ob_pcie_addr2 = ep_ob_axi_addr2;//必须为32位地址
//    rc_ib_axi_addr2 = PCIE_X16_LINK0_MISC_BASE_ADDR;//0x1f450000;
#endif
    
    if(pcieDevType)//RC
	{
        printk("RC ob/ib cfg.\r\n");

#if 0/*xxx ldf 20230911 note:: 这是之前调试VPX板时，两片DSP的pcie互连的开窗，这部分暂时先保留*/
        if(hr3_board_type == 1){ /*片间pci互连的情况*/
        	//RC IB, for DMA  
        	/*RC  BAR0 + BAR1*/
        	ep_ob_size = 25;//RC BAR0最大2^25, ep的outbound大小 = rc的inbound大小 = rc的mem空间大小
        	rc_ib_axi_addr = 0x2090000000;
        	ep_ob_pcie_addr = ep_ob_axi_addr;//RC BAR0+BAR1, 可以为64位地址
        	pcie_ctrl_ib(/*0, */rc_ib_axi_addr, 0, ep_ob_size-1);
        	pcie_rc_bar_cfg(0, ep_ob_pcie_addr & 0xffffffff);
        	pcie_rc_bar_cfg(1, (ep_ob_pcie_addr>>32) & 0xffffffff);

        	//RC IB-2, for MSI(EP触发RC的MSI)
        	ep_ob_size2 = 16;
        	rc_ib_axi_addr2 = PCIE_X16_LINK0_MISC_BASE_ADDR;//0x1f450000;
        	/*xxx ldf 20230530 note:: bar_num=1 似乎无法填其他地址*/
        	pcie_ctrl_ib(/*0, */rc_ib_axi_addr2, 7, ep_ob_size2-1);

        	//RC OB, for MSI(RC触发EP的MSI)
        	//分配EP的配置空间
        	ep_cfg_size = 25;//2^25
        	ep_cfg_axi_addr = 0x30000000;
        	ep_cfg_pcie_addr = ep_cfg_axi_addr;
        	pcie_ctrl_ob(/*0, */ep_cfg_axi_addr, ep_cfg_pcie_addr, ep_cfg_size-1, 0, OB_TYPE_CFG0);
        	//分配EP的MEM空间
        	ep_mem_size = 16;
        	ep_mem_axi_addr = 0x32000000;//0x100000000;//ep的mem空间axi地址 等价 rc的outbound空间axi地址
        	ep_mem_pcie_addr = ep_ib_axi_addr;
        	pcie_ctrl_ob(/*0, */ep_mem_axi_addr, ep_mem_pcie_addr, ep_mem_size-1, 1, OB_TYPE_MEM);
        	/*通过RC来配置EP的BAR0,BAR1*/
        	phx_write_u32(ep_cfg_axi_addr + 0x10, ep_mem_axi_addr & 0xffffffff);
        	phx_write_u32(ep_cfg_axi_addr + 0x14, (ep_mem_axi_addr>>32) & 0xffffffff);

        	//配置EP CFG commmand: MEM,I/O Enable
        	phx_write_u32(ep_cfg_axi_addr + 0x04, 0x146);	
        	
			printk("hr3dwcPciRegister end.\r\n");
			return;
        }
#endif
        
        
        /*以下为只作RC的情况*/
        
		//分配type0的配置空间
//            ep_cfg_size = PCIEX16_CFG0_SPACE_SIZE;
//            ep_cfg_axi_addr = PCIEX16_CFG0_BASE_ADDR;
//            ep_cfg_pcie_addr = ep_cfg_axi_addr;
        	printk("<**DEBUG**> [%s():_%d_]:: TYPE0 CFG SPACE: ep_cfg_axi_addr=0x%lx, ep_cfg_pcie_addr=0x%lx, ep_cfg_size=0x%x\n", 
        			__FUNCTION__, __LINE__,PCIEX16_CFG0_BASE_ADDR,PCIEX16_CFG0_BASE_ADDR,(1<<PCIEX16_CFG0_SPACE_SIZE));
			pcie_ctrl_ob(pciex16_cur_link_en, PCIEX16_CFG0_BASE_ADDR, PCIEX16_CFG0_BASE_ADDR, PCIEX16_CFG0_SPACE_SIZE-1, 0, pcieDevType, OB_TYPE_CFG0);
			
		//分配type1的配置空间  /*xxx ldf 20230912 note:: 还没测试过接pci桥设备的情况，建链可能会有问题*/
//			ep_cfg_size = PCIEX16_CFG1_SPACE_SIZE;
//			ep_cfg_axi_addr = PCIEX16_CFG1_BASE_ADDR;
//			ep_cfg_pcie_addr = ep_cfg_axi_addr;
			printk("<**DEBUG**> [%s():_%d_]:: TYPE1 CFG SPACE: ep_cfg_axi_addr=0x%lx, ep_cfg_pcie_addr=0x%lx, ep_cfg_size=0x%x\n", 
					__FUNCTION__, __LINE__,PCIEX16_CFG1_BASE_ADDR,PCIEX16_CFG1_BASE_ADDR,(1<<PCIEX16_CFG1_SPACE_SIZE));
			pcie_ctrl_ob(pciex16_cur_link_en, PCIEX16_CFG1_BASE_ADDR, PCIEX16_CFG1_BASE_ADDR, PCIEX16_CFG1_SPACE_SIZE-1, 1, pcieDevType, OB_TYPE_CFG1);

		//分配EP的MEM空间
//			ep_mem_size = PCIE_MEM_SPACE_SIZE;
//			ep_mem_axi_addr = PCIE_MEM_BASE_ADDR;//ep_cfg_axi_addr + (1<<ep_cfg_size);
//			ep_mem_pcie_addr = ep_mem_axi_addr;
        	printk("<**DEBUG**> [%s():_%d_]:: MEM SPACE: ep_mem_axi_addr=0x%lx, ep_mem_pcie_addr=0x%lx, ep_mem_size=0x%x\n", 
        			__FUNCTION__, __LINE__,PCIE_MEM_BASE_ADDR,PCIE_MEM_BASE_ADDR,(1<<PCIE_MEM_SPACE_SIZE));
			pcie_ctrl_ob(pciex16_cur_link_en, PCIE_MEM_BASE_ADDR, PCIE_MEM_BASE_ADDR, PCIE_MEM_SPACE_SIZE-1, 2, pcieDevType, OB_TYPE_MEM);
		
		//配置RC的BAR
			pcie_rc_bar_cfg(pciex16_cur_link_en, pcieDevType);
			
		//RC InBound, EP访问RC
			pcie_ctrl_ib(pciex16_cur_link_en, 0, 7, 31);/*ldf 20230828 add:: 这里必须使用bar7*/
			
    }else//EP
    {
    	printk("EP ob/ib cfg.\r\n");
#if 0/*xxx ldf 20230911 note:: 这是之前调试VPX板时，两片DSP的pcie互连的开窗，这部分暂时先保留*/
	//EP IB, for MSI(RC触发EP的MSI)
    	ep_mem_size = 16;
        ep_ib_axi_addr = PCIE_X16_LINK0_MISC_BASE_ADDR;
        ep_mem_pcie_addr = ep_ib_axi_addr;
    	pciex16_0_ep_ib(/*0, */ep_ib_axi_addr, 0, ep_mem_size-1);
	//EP OB, for DMA  
        ep_ob_size = 25;//RC BAR0最大2^25, ep的outbound大小 = rc的inbound大小 = rc的mem空间大小
        ep_ob_axi_addr = 0x180000000;//ep的outbound空间axi地址 等价 rc的mem空间axi地址
        ep_ob_pcie_addr = ep_ob_axi_addr;//RC BAR0+BAR1, 可以为64位地址
    	pcie_ctrl_ep_ob(/*0, */ep_ob_axi_addr, ep_ob_pcie_addr, ep_ob_size-1, 0, OB_TYPE_MEM);
	//EP OB-2, for MSI(EP触发RC的MSI)
        ep_ob_size2 = 16;//2^16
        ep_ob_axi_addr2 = 0x33000000;//ep的outbound空间axi地址 等价 rc的mem空间axi地址
        ep_ob_pcie_addr2 = ep_ob_axi_addr2;//必须为32位地址
    	pcie_ctrl_ep_ob(/*0, */ep_ob_axi_addr2, ep_ob_pcie_addr2, ep_ob_size2-1, 1, OB_TYPE_MEM);
#endif
    }

    printk("hr3dwcPciRegister end.\r\n");
}

void dma_int_en(u64 dma_baseaddr, u32 ch_num)
{
	//for int
	u32 int_sta,int_en,int_dis;
	u32 done_bit,err_bit;
	u64 misc_base_addr = get_pcie_misc_link_base_addr(pciex16_cur_link_en/*PCIE_X16_LINK0_EN*/);
	
	done_bit = ch_num;
	err_bit  = ch_num + 8;

	//INTR_EN: dma int en  [bit28]msi_en  [bit10]dma_en
	phx_write_u32(misc_base_addr + PCIE_X16_INTR_ENABLE, 0x1<<10);/*ldf 20230601 add*/
	
	//fudy,int default:
	int_sta = phx_read_u32(dma_baseaddr+0xa0);
	int_en	= phx_read_u32(dma_baseaddr+0xa4);
	int_dis = phx_read_u32(dma_baseaddr+0xa8);
	//mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "dma_int default:status=0x%x,en=0x%x,dis=0x%x",int_sta,int_en,int_dis);
/*	uart_printf("dma_int init:ch_num=%d,sta_d=%x,sta_e=%x,en_d=%x,en_e=%x,dis_d=%x,dis_e=%x\n\r",
		ch_num,	(int_sta>>done_bit)&0x1,(int_sta>>err_bit)&0x1,
				(int_en>>done_bit)&0x1,(int_en>>err_bit)&0x1,
				(int_dis>>done_bit)&0x1,(int_dis>>err_bit)&0x1 );	
*/
	//fudy,int en:
	int_en = int_en | (1<<done_bit) | (1<<err_bit);
	phx_write_u32(dma_baseaddr+0xa4,int_en);
	int_dis = int_dis & ~(1<<done_bit) & ~(1<<err_bit);
	phx_write_u32(dma_baseaddr+0xa8,int_dis);	

	int_sta = phx_read_u32(dma_baseaddr+0xa0);
	int_en	= phx_read_u32(dma_baseaddr+0xa4);
	int_dis = phx_read_u32(dma_baseaddr+0xa8);
	//mylog(MYLOG_PCIE, 1, __FUNCTION__, __LINE__, "status=0x%x,en=%x,dis=0x%x",int_sta,int_en,int_dis);


/*	uart_printf("dma_int init:ch_num=%d,sta_d=%x,sta_e=%x,en_d=%x,en_e=%x,dis_d=%x,dis_e=%x\n\r",
		ch_num,	(int_sta>>done_bit)&0x1,(int_sta>>err_bit)&0x1,
				(int_en>>done_bit)&0x1,(int_en>>err_bit)&0x1,
				(int_dis>>done_bit)&0x1,(int_dis>>err_bit)&0x1 );	
*/

}
/*初始化DMA操作相关的信号量以及接收任务*/
void  hr3PciDmaInit (void)
{
    semID_PCIE_WR      = semBCreate(SEM_Q_PRIORITY, SEM_EMPTY);
    semID_PCIE_RD      = semBCreate(SEM_Q_PRIORITY, SEM_EMPTY);
    semID_PCIE_REC     = semBCreate(SEM_Q_PRIORITY, SEM_EMPTY);
    semID_PCIE_WR_Lock = semBCreate(SEM_Q_PRIORITY, SEM_FULL);
    semID_PCIE_RD_Lock = semBCreate(SEM_Q_PRIORITY, SEM_FULL);


    /*PCIE的DMA接收中断处理任务*/
    taskCreateCore("t_PcieX16Dma", 110, VX_FP_TASK, 0x20000,
                   (FUNCPTR)hrPcieDmaIntTask, 0,
                   0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
    
    /*xxx ldf 20230601 note:: 使用DMA向pcie地址写入数据还存在问题，目前只能从PCIE读数据*/
    //dma ch0 int en
	u32 ch_num = 0;
	u64 dma_baseaddr = get_pcie_ip_link_base_addr(pciex16_cur_link_en) + (ch_num*0x14) + (0x1<<22) + (0x1<<21);//0x1a600000
    dma_int_en(dma_baseaddr,ch_num);
    /*初始化PCIE的DMA接收任务*/
	bslPcieRecInit(NULL,sysCpuGetID(),0,0);
}

/*PCIE的MSI中断处理函数*/
void pci_msi_int_handler(void)
{
//	printk("<**DEBUG**> [%s():_%d_]:: MSI int ......\n", __FUNCTION__, __LINE__);
	semGive(semID_PCIE_MEM_RD);/*通知hrPcieMsiIntTask读取MEM数据*/
	return;
}


void pciex16_isr_dma(void)
{
	u32 ch_num = 0;

	u64 dma_baseaddr = get_pcie_ip_link_base_addr(pciex16_cur_link_en) + (ch_num*0x14) + (0x1<<22) + (0x1<<21);
	u32 int_sta = 0;

	int_sta = phx_read_u32(dma_baseaddr+0xa0);
	//debug_print("DMA int sta =%x\n\r",int_sta);

	if(int_sta & 0x0000ff00){
		debug_print("DMA ERR INT: int_sta=%x\n\r",int_sta);
		//dma_err_flag = 1;
	}

	if(int_sta&0x1){//for ch0 done_int

		debug_print("ch0 dma done int.\n\r");

		//w1clr
		phx_write_u32(dma_baseaddr+0xa0,int_sta | 0x1);

	}
}

/*PCIE的DMA接收中断处理函数*/
void pci_dma_int_handler(void)
{
	pciex16_isr_dma();/*ldf 20230822 add*/
	
	
//	printk("<**DEBUG**> [%s():_%d_]:: DMA int ......\n", __FUNCTION__, __LINE__);
	semGive(semID_PCIE_REC);/*通知hrPcieIntTask执行dma接收处理*/

	return;
}


/*sel = PCIE_X16_LINK0_EN,
 * intr_sel = 1<<28  (x16)   or  1<<16  (x4),
 * is_clr_intr 是否清MSI中断
 * */
void clear_msi_int(int sel,int intr_sel,int is_clr_intr)
{
   u64 base_addr;
   int rdata,i;
   u64 intr_status_addr;
   u64 msiir_addr;
   u64 msisr_addr;
   //clr_intr = 1;
   base_addr = get_pcie_misc_link_base_addr(sel);//0x1f450000
   
//   if((sel & 0xf) != 0)
//   {     
      intr_status_addr = PCIE_X16_INTR_STATUS;
      msiir_addr =  PCIE_X16_MSIIR0;
      msisr_addr =  PCIE_X16_MSISR;
//   }
//   else
//   {
//      intr_status_addr = PCIE_X4_INTR_STATUS;
//      msiir_addr =  PCIE_X4_MSIIR0;
//      msisr_addr =  PCIE_X4_MSISR;
//   }
      
   rdata = phx_read_u32(base_addr + intr_status_addr);
   if((rdata & intr_sel) == 0x0)
   {
      //mylog(MYLOG_PCIE, 4, __FUNCTION__, __LINE__, "intr error,intr_sel = 0x%x",intr_sel); 
   }
   if(is_clr_intr == 1)
   {
       if((intr_sel & 0x10010000) != 0)  //x16 or x4 msi intr
       {
          for(i = 0;i < 8; i++)
          {
              rdata = phx_read_u32(base_addr + msiir_addr + i*4);
              if(rdata != 0x0)
              {
                   phx_write_u32(base_addr + msiir_addr + i*4, rdata);
              }
          }
          phx_write_u32(base_addr + msisr_addr, 0x0);
       }
       else
       {
           phx_write_u32(base_addr + intr_status_addr , rdata);
       }
        rdata = phx_read_u32(base_addr + intr_status_addr);
        if((rdata & intr_sel) != 0x0)
        {
           //mylog(MYLOG_PCIE, 4, __FUNCTION__, __LINE__, "intr error,intr_sel = 0x%x",intr_sel);
        }
   }
}

/* the ISR for pcie ctrl */
void hr3cdnsPciInt(void)
{
#if 0
    UINT32 reason = 0;
    UINT32 status = 0;
    //uart_printf("hr2dwcPcivxbInt >>>>>>>>>>>>>>>>>>>>>>>\r\n");

    reason = PCIE_APP_REG_GET(APP_LOGIC_PCI_INTR_ST);

    if (reason & 0x1) /* dma interrupt */
    {
        /* clear int of write channel 0 */
        status = PCIE_CFG_REG_GET_4B(PCIE_PL_DMA_WR_INT_ST);

        //uart_printf("status =0x");
        //printnum(status);
        //uart_printf("\r\n");
        if ((status & 0x1) != 0)
        {
            //dma_t2 = tickGet();
            //uart_printf("write channel0 Done Interrupt \r\n");
            semGive(semID_PCIE_WR);
            semGive(semID_PCIE_WR_Lock);
            s_pcieDmaWrState = 0;
            PCIE_CFG_REG_SET_4B(PCIE_PL_DMA_WR_INT_CLR,0x1);
        }
        else if ((status & 0x10000) != 0)
        {
            uart_printf("write channel0 Abort Interrupt \r\n");
            s_pcieDmaWrState = -1;
            PCIE_CFG_REG_SET_4B(PCIE_PL_DMA_WR_INT_CLR,0x10000);
        }
        /* clear int of read channel 0 */
        status = PCIE_CFG_REG_GET_4B(PCIE_PL_DMA_RD_INT_ST);
        if ((status & 0x1) != 0)
        {
            //dma_t5 = tickGet();
            //uart_printf("read channel0 Done Interrupt \r\n");
            semGive(semID_PCIE_RD);
            semGive(semID_PCIE_RD_Lock);
            s_pcieDmaRdState = 0;
            PCIE_CFG_REG_SET_4B(PCIE_PL_DMA_RD_INT_CLR,0x1);

        }
        else if ((status & 0x10000) != 0)
        {
            uart_printf("read channel0 Abort Interrupt \r\n");
            s_pcieDmaRdState = -1;
            PCIE_CFG_REG_SET_4B(PCIE_PL_DMA_RD_INT_CLR,0x10000);
        }
    }
    else if(reason & 0x80000) /* MSI interrupt */
    {
        status = PCIE_CFG_REG_GET_4B(PCIE_PL_MSI_CTRL_INT_0_ST);
        s_pcieMsiSign = *(unsigned int *)0x20000000;
        PCIE_CFG_REG_SET_4B(PCIE_PL_MSI_CTRL_INT_0_ST, status);
        //s_pcieMsiSign = 1;
    }
    PCIE_APP_REG_SET(APP_LOGIC_PCI_INTR_ST,~reason);
#endif

    //added by fudy.
    //MSI int
    u32 reason = 0;
    u64 msic_base_addr = get_pcie_misc_link_base_addr(pciex16_cur_link_en/*PCIE_X16_LINK0_EN*/);
    reason = phx_read_u32(msic_base_addr + PCIE_X16_INTR_STATUS);

    printk("<**DEBUG**> [%s():_%d_]:: reason = 0x%x\n", __FUNCTION__, __LINE__, reason);

    if ((reason & BIT(28))) //for MSI int
    {
//    	pci_msi_int_handler();
    	clear_msi_int(PCIE_X16_LINK0_EN, 1<<28, 1);/*清MSI中断*/
    }
    else if((reason & BIT(10)))//for DMA int...   
    {   
//    	pci_dma_int_handler();
    	
    	/*clear dma int*/
    }
    
    //pcie clr all int:
    phx_write_u32(msic_base_addr + PCIE_X16_INTR_STATUS, reason);/*ldf 20230822 add*/

    return;
}

void  hr3dwcPciInstConnect(void)
{
	int_install_handler("hr3Pciex16", PCIE_X16_INT_LINK0, 1, (INT_HANDLER)hr3cdnsPciInt, NULL);
	int_enable_pic(PCIE_X16_INT_LINK0);
//	int_disable_pic(PCIE_X16_INT_LINK0);
}

/*触发对端的msi中断*/
void pciex16_trig_msi_int(void)
{
//	//MSIR  0x1f4508fc
//	u64 msi_addr;
//	u32 msi_data = 0x2a82;//0xaa82; /*ldf 20230529 note:: 将msi_data写入到msi_addr会产生MSI中断*/
//	
//	if (pcieDevType == 0) //EP, <RC-IB2>0x900000001f4508fc <==> 0x90000000330008fc<EP-OB2>
//	{
//		msi_addr = ep_ob_axi_addr2 + PCIE_X16_MSIR;
//	}
//	else //RC, <RC-OB>0x90000000320008fc <==> 0x900000001f4508fc<EP-IB>
//	{
//		msi_addr = ep_mem_axi_addr + PCIE_X16_MSIR;
//	}
//	
//	phx_write_u32(msi_addr, msi_data);
//	return;
}

//配置MSI中断,由EP端产生MSI中断，需要通过OB向RC的MSIR写入数据
void hr3MsiIntConfig(void)
{
	u32 bus_num = 0;
	u32 val = 0;
	//MSIR  0x1f4508fc, <RC-IB>0x900000001f4508fc <==> 0x90000000324508fc<EP-OB>
	u64 misc_base_addr = get_pcie_misc_link_base_addr(pciex16_cur_link_en/*PCIE_X16_LINK0_EN*/);
	u64 ip_base_addr = get_pcie_ip_link_base_addr(pciex16_cur_link_en/*PCIE_X16_LINK0_EN*/);
	u64 msi_addr = misc_base_addr + PCIE_X16_MSIR;
	u32 msi_data = 0x2a82;//0xaa02;//0xaa82; /*ldf 20230529 note:: 将msi_data写入到msi_addr会产生MSI中断*/
	/*Message Data:
	 * [7:0] vector  中断向量 
	 * [10:8] 传送模式
	 * 			Fixed: 000b  此时中断请求将被Destination ID指定的CPU处理
	 * 			Lowest Priority: 001b
	 * 			SMI: 010b
	 * 			NMI: 100b
	 * 			INIT: 101b
	 * 			ExtINT: 111b
	 * [13:11] 保留
	 * [14] 当触发模式为0时，忽略该位
	 * 			0: Deassert
	 * 			1: Assert
	 * [15] 触发模式
	 * 			0: 边沿触发
	 * 			1: 电平触发
	 */


	//INTR_EN: msi int en  [bit28]msi_en  [bit10]dma_en
	phx_write_u32(misc_base_addr + PCIE_X16_INTR_ENABLE, 0x1<<28);

	phx_write_u32(misc_base_addr + PCIE_X16_MSIVR0, 0x1);
	phx_write_u32(misc_base_addr + PCIE_X16_MSIVR1, 0x1);
	phx_write_u32(misc_base_addr + PCIE_X16_MSIVR2, 0x1);
	phx_write_u32(misc_base_addr + PCIE_X16_MSIVR3, 0x1);
	phx_write_u32(misc_base_addr + PCIE_X16_MSIVR4, 0x1);
	phx_write_u32(misc_base_addr + PCIE_X16_MSIVR5, 0x1);
	phx_write_u32(misc_base_addr + PCIE_X16_MSIVR6, 0x1);
	phx_write_u32(misc_base_addr + PCIE_X16_MSIVR7, 0x1);

	//EP: msi cap(addr,data)
	phx_write_u32(ip_base_addr + (bus_num << 20) + PCIEX16_MSI_CAP_LOWADDR, (msi_addr & 0xffffffff));
	phx_write_u32(ip_base_addr + (bus_num << 20) + PCIEX16_MSI_CAP_HIGHADDR, ((msi_addr >> 32) & 0xffffffff));
	phx_write_u32(ip_base_addr + (bus_num << 20) + PCIEX16_MSI_CAP_DATA, msi_data);
	//EP: msi en
	val = phx_read_u32(ip_base_addr + (bus_num << 20) + PCIEX16_MSI_CAP_CTRL);
	val |= (0x1) << 16; //Enable MSI
	phx_write_u32(ip_base_addr + (bus_num << 20) + PCIEX16_MSI_CAP_CTRL, val);
	
	return;
}

void hr3PciMsiInit(void)
{
	hr3MsiIntConfig();/*ldf 20230601 add:: 配置MSI中断*/

	if(hr3_board_type == 1)/*HC3080 VPX应用板*/
	{
		semID_PCIE_MEM_RD = semBCreate(SEM_Q_PRIORITY, SEM_EMPTY); /*ldf 20230601 add:: 同步 MEM space读与回调函数*/

		/*PCIE的MSI中断处理任务,读mem空间数据*/
		taskCreateCore("t_PcieX16MemRD", 110, VX_FP_TASK, 0x20000,
				(FUNCPTR)hrPcieMsiIntTask, 0,
				0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
		/*PCIE的读mem空间数据任务*/
		bslPcieRecInit(NULL,sysCpuGetID(),0,0);
	}
		
	return;
}



/*********************************************************************************************************
** 函数名称: __hr3PciIrqGet
** 功能描述: 获取中断向量
** 输    入: iBus       总线号
**           iDev       设备号
**           iFun       功能号
**           iMsiEn     是否使能 MSI
** 输    出: pvIrq      中断向量
** 返    回: ERROR CODE
*********************************************************************************************************/
int  __hr3PciIrqGet (int iBus, int iDev, int iFun, void *  pvIrq)
{
    unsigned long    *pulVector = (unsigned long *)pvIrq;
    unsigned int     ucLineNew = ERROR;


    ucLineNew = PCIE_X16_INT_LINK0;
    if (pulVector) {
        *pulVector = ucLineNew;
    }

    return  (OK);
}


/*********************************************************************************************************
** 函数名称: hr3_pciex16_module_init
** 功能描述: hr3 PCIx16 控制器驱动初始化
** 输  入  : NONE
** 输  出  : ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
int  hr3_pciex16_module_init(void)
{
    if(hr3_board_type == 1)/*HC3080 VPX应用板*/
    	hr3dwcPciInstInit((sysCpuGetID() == 1) ? RC : EP);
    else
    	hr3dwcPciInstInit(RC);
    
    if(hr3_board_type == 1)/*HC3080 VPX应用板*/
    	hr3PciDmaInit();/*xxx ldf 20230601 add:: 使用DMA向pcie地址写入数据还存在问题，目前只能从PCIE读数据*/
    
#ifdef __ENABLE_MSI__
    hr3PciMsiInit();
#endif
    
    if(hr3_board_type == 1)/*HC3080 VPX应用板*/
    	hr3dwcPciInstConnect();

    if(get_link_status_x16link0() && pcieDevType){
    	if(pci_config_init(PCI_MECHANISM_0, (unsigned long)sys_pci_config_read,(unsigned long)sys_pci_config_write,0)) 
    	{
           	debug_print("hr3_pciex16_module_init(): failed to config pci!\n");
            return  -1;
    	}

    }
    debug_print("PciCtrlCreate end.\r\n");
    return  0;
}


#if 0
/*********************************************************************************************************
** 函数名称: hr3AppPciex16_x4_0_Test
** 功能描述: x16配成x4_4模式，lane0对接测试
** 输    入:
** 输    出: NONE
** 返    回: NONE
*********************************************************************************************************/
void  hr3AppPciex16_x4_0_MemRW(void)
{
#if 0
    unsigned int i;
    unsigned int len = 0x100;
    unsigned int val = 0;

    uart_printf("MEM RW TEST: \r\n");

    //uart_printf(__PRINTMESSAGE_LEVEL, "%x\n\r",*(unsigned int *)(0x9000000000000000+rc_ib_axi_addr));
    uart_printf("RD [%x]=%x\n\r",rc_ib_axi_addr,*(unsigned int *)(0x9000000000000000+rc_ib_axi_addr));
    *(unsigned int *)(0x9000000000000000+rc_ib_axi_addr) = 0x55aa55aa;
    uart_printf("WR [%x]=%x\n\r",rc_ib_axi_addr,0x55aa55aa);
    uart_printf("RD [%x]=%x\n\r",rc_ib_axi_addr,*(unsigned int *)(0x9000000000000000+rc_ib_axi_addr));
    if(0x55aa55aa == *(unsigned int *)(0x9000000000000000+rc_ib_axi_addr))
    	uart_printf("==MEM RW TEST OK! \r\n");
    else
    	uart_printf("==MEM RW TEST ERROR! \r\n");

    //mem rw for len
    for(i = 0; i<len; i+=4 )
        *(unsigned int *)(0x9000000000000000+rc_ib_axi_addr+i) = 0x5a5a0000+i;
    for(i = 0; i<len; i+=4 ){
        if((0x5a5a0000+i) != *(unsigned int *)(0x9000000000000000+rc_ib_axi_addr+i)){
        	uart_printf("MEM RW LEN ERROR:[%x]WR:%x,RD:%x\n\r",
        			rc_ib_axi_addr+i,0x5a5a0000+i,*(unsigned int *)(0x9000000000000000+rc_ib_axi_addr+i));
            return;
        }
    }

    uart_printf("==MEM RW LEN [%x] OK!\n\r",len);
#endif

}

/*********************************************************************************************************
** 函数名称: hr3AppPciex16_x4_0_Test
** 功能描述: x16配成x4_4模式，lane0对接测试
** 输    入:
** 输    出: NONE
** 返    回: NONE
*********************************************************************************************************/
void  hr3AppPciex16_x4_0_Test (u32 test_data_temp)
{
    u64 test_dma_axi;/*测试数据区*/
    u32 test_dma_data_len;
    u64 test_dma_desc0;
    u64 test_dma_desc1;
    
    uart_printf("pciex16 x4 lane0 test:\r\n");

    if(pcieDevType==0)//EP
    {
    	uart_printf("EP HEADER[0]=0x%x\n\r",*(unsigned int*)(0x9000000000000000+0x1a000000+0x0));
    	uart_printf("EP HEADER[4]=0x%x\n\r",*(unsigned int*)(0x9000000000000000+0x1a000000+0x4));
    	uart_printf("EP BAR0=0x%x\n\r",*(unsigned int*)(0x9000000000000000+0x1a000000+0x10));
    	uart_printf("EP BAR1=0x%x\n\r",*(unsigned int*)(0x9000000000000000+0x1a000000+0x14));
       pciex16_0_ep_ib_get();


       printk("EP DMA TEST:\n\r");
//    	pcie_dma_check(0x114c8000,0x114c2000,0x100,ep_mem_axi_addr,   0x11404000,  0);
//    	pcie_dma_check(0x114c8000,0x114c4000,0x1000,ep_mem_axi_addr,   0x11404100,  1);

       test_dma_axi = 0x2090000000;//0x10400000;/*测试数据区*/
       test_dma_data_len = 1<<24;//1<<24;/*xxx ldf 20230531:: 超出1<<24的部分DMA传输不正确,原因还不清楚*/
       test_dma_desc0 = 0x11404000;//SRAM1_UNS
       test_dma_desc1 = (u64)memalign (128, 0x2c);
       printk("test_dma_desc0=0x%lx, test_dma_desc1=0x%lx \r\n",test_dma_desc0,test_dma_desc1);
       
//0(pcie->axi)
        /*xxx ldf 20230531:: DMA没有传输 【FAIL】*/ /*<RC-OB> 0x9000000100000000 <==>  0x9000002880000000 <EP-IB> (2^28)*/
	    printk("0(pcie->axi), ep_ib -> test_dma_axi:: \r\n");
		pcie_dma_check(ep_mem_pcie_addr, test_dma_axi, test_dma_data_len, ep_ib_axi_addr, test_dma_desc0, 0, test_data_temp);
	    
        /*xxx ldf 20230531:: 【PASS】*/ /*<RC-IB> 0x90000028c0000000 <==> 0x9000000180000000 <EP-OB> (2^28)*/
	    printk("0(pcie->axi), ep_ob -> test_dma_axi:: \r\n");
//		pcie_dma_check(ep_ob_pcie_addr, test_dma_axi, test_dma_data_len, ep_ob_axi_addr, test_dma_desc0, 0, test_data_temp);

		
//1(axi->pcie)
		/*xxx ldf 20230531:: DMA没有传输 【FAIL】*/ /*<RC-IB> 0x90000028c0000000 <==> 0x9000000180000000 <EP-OB> (2^28)*/
	    printk("1(pcie<-axi), ep_ob <- test_dma_axi:: \r\n");
//		pcie_dma_check(ep_ob_pcie_addr, test_dma_axi, test_dma_data_len, ep_ob_axi_addr, test_dma_desc0, 1, test_data_temp);
	    
        /*xxx ldf 20230531:: DMA没有传输 【FAIL】*/ /*<RC-OB> 0x9000000100000000 <==>  0x9000002880000000 <EP-IB> (2^28)*/
	    printk("1(pcie<-axi), ep_ib <- test_dma_axi:: \r\n");
//		pcie_dma_check(ep_mem_pcie_addr, test_dma_axi, test_dma_data_len, ep_ib_axi_addr, test_dma_desc0, 1, test_data_temp);;
		
		printk("EP device, test end!\r\n");
        return;
    }
    else//RC
    {
    	printk("RC.\r\n");
#if 1
        test_dma_axi = 0x2090000000;//0x10400000;/*测试数据区*/
        test_dma_data_len = 1<<24;//1<<24;/*xxx ldf 20230531:: 超出1<<24报总线错误*/
        test_dma_desc0 = 0x11404000;//SRAM1_UNS
        test_dma_desc1 = (u64)memalign (128, 0x2c);
        printk("test_dma_desc0=0x%lx, test_dma_desc1=0x%lx \r\n",test_dma_desc0,test_dma_desc1);
        
//0(pcie->axi)
        /*xxx ldf 20230531:: DMA没有传输 【FAIL】*/ /*<RC-IB> 0x90000028c0000000 <==> 0x9000000180000000 <EP-OB> (2^28)*/
        printk("0(pcie->axi), rc_ib -> test_dma_axi:: \r\n");
 		pcie_dma_check(ep_ob_pcie_addr, test_dma_axi, test_dma_data_len, rc_ib_axi_addr, test_dma_desc0, 0, test_data_temp);
        
        /*xxx ldf 20230531:: 【PASS】*/ /*<RC-OB> 0x9000000100000000 <==>  0x9000002880000000 <EP-IB> (2^28)*/
        printk("0(pcie->axi), rc_ob -> test_dma_axi:: \r\n");
//        pcie_dma_check(ep_mem_pcie_addr, test_dma_axi, test_dma_data_len, ep_mem_axi_addr, test_dma_desc0, 0, test_data_temp);
 		
//1(axi->pcie)
 		/*xxx ldf 20230531:: DMA没有传输 【FAIL】*/ /*<RC-OB> 0x9000000100000000 <==>  0x9000002880000000 <EP-IB> (2^28)*/
        printk("1(pcie<-axi), rc_ob <- test_dma_axi:: \r\n");
// 		pcie_dma_check(ep_mem_pcie_addr, test_dma_axi, test_dma_data_len, ep_mem_axi_addr, test_dma_desc0, 1, test_data_temp);
        
        /*xxx ldf 20230531:: DMA没有传输 【FAIL】*/ /*<RC-IB> 0x90000028c0000000 <==> 0x9000000180000000 <EP-OB> (2^28)*/
        printk("1(pcie<-axi), rc_ib <- test_dma_axi:: \r\n");
// 		pcie_dma_check(ep_ob_pcie_addr, test_dma_axi, test_dma_data_len, rc_ib_axi_addr, test_dma_desc0, 1, test_data_temp);

 		
		printk("RC device, test end!\r\n");
        return;
#else
//    unsigned long id = 0;
//    id = PCIE_CFG_REG_GET_4B(0);
//
//    uart_printf("==CFG RD TEST: EP DEVICE ID = %x\r\n",id);
//
//    unsigned int val1,val2,tmp,bar_addr;
//    val1 = PCIE_CFG_REG_GET_2B(0x4);
//    tmp = val1 & 0x7;
//    tmp = ~tmp;
//    val2 = ((val1>>3)<<3) | (tmp & 0x7);
//    PCIE_CFG_REG_SET_2B(0x4,val2);
//    val2 = PCIE_CFG_REG_GET_2B(0x4);
//    PCIE_CFG_REG_SET_2B(0x4,val1);
//    uart_printf("==CFG WR TEST: 0x4:%x/%x/%x,",val1,val2,PCIE_CFG_REG_GET_2B(0x4));
//    if(val1!=val2)
//    	uart_printf("test OK!\n\r");
//    else
//    	uart_printf("test ERROR!\n\r");
//
//    //set ep bar(from rc):
//#if 0//tmp
//    uart_printf("EP BAR0=%x\n\r",PCIE_CFG_REG_GET_4B(0x10));
//    PCIE_CFG_REG_SET_4B(0x10, 0xffffffff);
//    uart_printf("EP BAR0=%x(after wr 0xffffffff)\n\r",PCIE_CFG_REG_GET_4B(0x10));
//
//    bar_addr = 0x14;//bar1
//    uart_printf("EP BAR1=%x\n\r",PCIE_CFG_REG_GET_4B(bar_addr));
//    PCIE_CFG_REG_SET_4B(bar_addr, 0xffffffff);
//    uart_printf("EP BAR1=%x(after wr 0xffffffff)\n\r",PCIE_CFG_REG_GET_4B(bar_addr));
//    PCIE_CFG_REG_SET_4B(bar_addr, 0x0);
//    uart_printf("EP BAR1=%x(after wr 0x0)\n\r",PCIE_CFG_REG_GET_4B(bar_addr));
//
//#endif
//    PCIE_CFG_REG_SET_4B(0x10, PCIEX16_0_PCIE_ADDR_MEM);
//    uart_printf("EP BAR0=%x\n\r",PCIE_CFG_REG_GET_4B(0x10));
//
//    hr3AppPciex16_x4_0_MemRW();
//
//    uart_printf("DMA TEST:\n\r");
//    u8 *axiAddr1 = 0x114c2000/*(u8 *)malloc(0x100)*/;
//    u8 *llpoint1 = 0x11404000/*(u8 *)malloc(0x100)*/;
//    uart_printf("axiAddr1 = 0x%016x, llpoint1 = 0x%016x\n\r",axiAddr1,llpoint1);
//    pcie_dma_check(PCIEX16_0_PCIE_ADDR_MEM,axiAddr1/*0x114c2000*/,0x100,ep_mem_axi_addr,llpoint1/*0x11404000*/,0);//pcie -> axi
//
////    pcie_dma_check_ddr();  //ok
//
//    u8 *axiAddr2 = 0x114c4000/*(u8 *)malloc(0x1000)*/;
//    u8 *llpoint2 = 0x11404100/*(u8 *)malloc(0x1000)*/;
//    uart_printf("axiAddr2 = 0x%016x, llpoint2 = 0x%016x\n\r",axiAddr2,llpoint2);
//    pcie_dma_check(PCIEX16_0_PCIE_ADDR_MEM,axiAddr2/*0x114c4000*/,0x1000,ep_mem_axi_addr, llpoint2/*0x11404100*/,1);//pcie <- axi
//
////    pcie_dma_check(ep_mem_axi_addr,0x114c4000,0x100,ep_mem_axi_addr,   0x11404100,  0); //ok
//
////    pcie_dma_check(ep_mem_axi_addr,0x2088000000,0x1000,ep_mem_axi_addr,   0x11404000,  1); //err
//
////    dma_test(ep_mem_axi_addr,0x114a1000,0x114c1000,0x100); //err
#endif
    }
}
#endif


