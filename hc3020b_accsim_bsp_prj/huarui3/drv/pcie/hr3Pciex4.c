#include "taskLib.h" 
#include "semLib.h"
#include "vxCpuLib.h"
#include <irq.h>
#include "tickLib.h" 
#include <sys/types.h>
#include "tool/hr3SysTools.h"
#include "bsp_macro.h"
#include "tool/hr3Macro.h"
#include "vcscpumacro.h"
#include "pcie.h"


/*********************************************************************************************************
** 函数名称: hr3_pciex4_module_init
** 功能描述: hr3 PCIx4 控制器驱动初始化
** 输  入  : NONE
** 输  出  : ERROR CODE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
int  hr3_pciex4_module_init(void)
{
	printk("<**DEBUG**> [%s():_%d_]:: PCIE X4 Init Start.\n", __func__, __LINE__);
	
	pciex4_3080_init(RC);
	
	printk("<**DEBUG**> [%s():_%d_]\n", __func__, __LINE__);
	/*TYPE0 CFG Space*/
	pcie_config_ob(PCIE_SRIO_REGION1_LINK0_IP_BASE_ADDR,0x70000000,0x70000000,0,(25-1),RC, OB_TYPE_CFG0);
	
	printk("<**DEBUG**> [%s():_%d_]\n", __func__, __LINE__);
	/*TYPE1 CFG Space*/
	pcie_config_ob(PCIE_SRIO_REGION1_LINK0_IP_BASE_ADDR,0x72000000,0x72000000,1,(25-1),RC, OB_TYPE_CFG1);

	printk("<**DEBUG**> [%s():_%d_]\n", __func__, __LINE__);
	/*MEM Space*/
	pcie_config_ob(PCIE_SRIO_REGION1_LINK0_IP_BASE_ADDR,0x74000000,0x74000000,2,(26-1),RC, OB_TYPE_MEM);
	
	printk("<**DEBUG**> [%s():_%d_]:: PCIE X4 Init End.\n", __func__, __LINE__);
    return  0;
}
