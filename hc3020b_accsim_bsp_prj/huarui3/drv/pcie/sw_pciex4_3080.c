#include "./pcie.h"

#define H32(x)	(((x) >> 32) & 0xfffffffful)
#define L32(x)	((x) & 0xfffffffful)

#if 1
#define debug_print  printk
#else
#define debug_print 
#endif


#define PCIE_DEBUG_COMPLETE
//#define PCIE_DEBUG_UNCOMPLETE
#define REGION_USE_SRAM //using sram for pcie mem map, if using ddr, don't define this macro

/******************************************************************************/

/////////////////////////region0//////////////////////
//link0-pciex4
#define PCIE_0_BASE         0x1C000000 // hc3080,pcie core apb addr
#define PCIE_WRAP_0_BASE    0x1C700000 // hc3080,pcie user_defined regs apb addr
#define PHY_WRAP_0_BASE     0x1F6C0000 // hc3080,phy user_defined regs apb addr
#define PHY_0_BASE          0x1F680000 // hc3080,phy core apb addr

#define PCIE0_CTRL_CONFIG_BASE   PCIE_0_BASE
#define PCIE0_HAL_MGMT_BASE     (PCIE_0_BASE |  0x40000)
#define PCIE0_CTRL_MGMT_BASE    (PCIE_0_BASE | 0x100000)
#define PCIE0_AXI_CFG_BASE      (PCIE_0_BASE | 0x400000)
#define PCIE0_DMA_CONFIG_BASE   (PCIE_0_BASE | 0x600000)
#define PCIE0_MGMT_BASE         (PCIE_0_BASE | 0x100000)

/////////////////////////region1//////////////////////
//link0-pciex4
#define PCIE_1_BASE         (PCIE_0_BASE      + 0x1000000)  //hc3080
#define PCIE_WRAP_1_BASE    (PCIE_WRAP_0_BASE + 0x1000000)  //hc3080
#define PHY_WRAP_1_BASE     (PHY_WRAP_0_BASE  + 0x100000)   //hc3080
#define PHY_1_BASE          (PHY_0_BASE       + 0x100000)   //hc3080

#define PCIE1_CTRL_CONFIG_BASE   PCIE_1_BASE
#define PCIE1_HAL_MGMT_BASE     (PCIE_1_BASE |  0x40000)
#define PCIE1_CTRL_MGMT_BASE    (PCIE_1_BASE | 0x100000)
#define PCIE1_AXI_CFG_BASE      (PCIE_1_BASE | 0x400000)
#define PCIE1_DMA_CONFIG_BASE   (PCIE_1_BASE | 0x600000)
#define PCIE1_MGMT_BASE         (PCIE_1_BASE | 0x100000)

#ifdef REGION_USE_SRAM
#define OB_REGION_SIZE 17 //for sram,fudy.
#else//using ddr
#define OB_REGION_SIZE 28
#endif

#define EP_IB_AXI_ADDR_SRAM	0x105c0000


/******************************************************************************
 * fuction declaration
 */

static void link_show(u32 pcie_addr,u32 pcie_wrapper_addr);
static void link_show_core(u32 pcie_addr);
static void cap_link_show(u64 pcie_addr);
static void link_cap(u32 pcie_addr);
static void lp_phy_ctrl1(u64 phy);

static void wait(u32 count)
{

	u32 i;
	for(i=0;i<count;i++);

}


/******************************************************************************/

void wait_link_down(u32 pcie_wrapper_addr)
{
	u32 apb_paddr;
	u32 apb_prdata;
	u8 link_state;
	u32 wait_count = 0;//fudy.
	u32 config_input_output = pcie_wrapper_addr + 0x800;
	u32 val = 0;

	do {
//		link_show(PCIE_0_BASE,PCIE_WRAP_0_BASE);//tmp
//		link_show(PCIE_1_BASE,PCIE_WRAP_1_BASE);//tmp
		apb_paddr = 0x200 * 4;
		apb_prdata = phx_read_u32(pcie_wrapper_addr+apb_paddr);
		link_state = ((apb_prdata>>7) & 0x3);
		//debug_print("addr: 0x%x, data: 0x%x, 0x%x \n\r", apb_paddr, apb_prdata, link_state);

		wait(10000);//fudy.
		wait_count++;
		if(wait_count > 2000) break;//fudy.
	} while (link_state != 0x3);

	debug_print("i=%d \n\r",wait_count);//vcs:i=618

	val = phx_read_u32(config_input_output);//get neg link status

	if(link_state != 0x3){
		if(pcie_wrapper_addr == PCIE_WRAP_0_BASE){debug_print("link0 ERROR! \n\r");}
		if(pcie_wrapper_addr == PCIE_WRAP_1_BASE){debug_print("link1 ERROR! \n\r");}
	}
	else{
		if(pcie_wrapper_addr == PCIE_WRAP_0_BASE){debug_print("link0 up!  \n\rwidth=%x(0=x1,1=x2,2=x4,3=x8) \n\rspeed=%x(0=2.5G,1=5G,2=8G)\n\r",(val>>5)&0x3,(val>>3)&0x3);}
		if(pcie_wrapper_addr == PCIE_WRAP_1_BASE){debug_print("link1 up!  \n\rwidth=%x(0=x1,1=x2,2=x4,3=x8) \n\rspeed=%x(0=2.5G,1=5G,2=8G)\n\r",(val>>5)&0x3,(val>>3)&0x3);}
	}

	//print pcie core reg value:
	link_show_core((pcie_wrapper_addr == PCIE_WRAP_0_BASE)?(PCIE_0_BASE):(PCIE_1_BASE));

	//link_show(PCIE_0_BASE,PCIE_WRAP_0_BASE);//tmp
	//link_show(PCIE_1_BASE,PCIE_WRAP_1_BASE);//tmp


}


void pcie_sysctrl_en(void)
{
	u32 temp_data;

	phx_write_u32(0x1f0c0018,0xA5ACCEDE);

	temp_data=phx_read_u32(0x1f0c0004);

	phx_write_u32(0x1f0c0004,temp_data & 0x3f3fffff );  //release rst

	temp_data=phx_read_u32(0x1f0c0008);
	phx_write_u32(0x1f0c0008,temp_data & 0xf3ffffff ); //0xf3ffffff //release rst

	temp_data=phx_read_u32(0x1f0c0010);
	phx_write_u32(0x1f0c0010,temp_data | 0x7f80000 );   //enable clk_en

	debug_print("en dn\n\r");
}

void pcie_sysctrl_en1(void)
{
	u32 temp_data;

	phx_write_u32(0x1f0c0018,0xA5ACCEDE);

	temp_data=phx_read_u32(0x1f0c0004);
	phx_write_u32(0x1f0c0004,temp_data & 0x3f3fffff );  //release rst

	//    temp_data=phx_read_u32(0x1f0c0004);	//3f0000f0
	//	debug_print("0x1f0c0004=%x  \n\r",temp_data); //fu,tmp

	temp_data=phx_read_u32(0x1f0c0008);	//fff30000
	phx_write_u32(0x1f0c0008,temp_data & 0xe3ffffff ); //0xf3ffffff //release rst
	//bit28:pciex4

	//	debug_print("8=%x	\n\r",phx_read_u32(0x1f0c0008)); //fu,tmp//e3f30000

	temp_data=phx_read_u32(0x1f0c0010);	//ffff001e
	phx_write_u32(0x1f0c0010,temp_data | 0x7f90018 ); //0x7f80000  //enable clk_en
	//pcie:[25:19][16][4:3]

	//	debug_print("10=%x	\n\r",phx_read_u32(0x1f0c0010)); //fu,tmp//ffff001e


	debug_print("release rst.\n\r");
}

void pcie_sysctrl_en_3020b(void)
{
	u32 temp_data;

	phx_write_u32(0x1f0c0028,0xA5ACCEDE);

	temp_data=phx_read_u32(0x1f0c0008);
	phx_write_u32(0x1f0c0008,temp_data & ~(0x3<<2) );  //release rst

	temp_data=phx_read_u32(0x1f0c0008);
	phx_write_u32(0x1f0c0008,temp_data & ~(0x3<<9) );  //release rst

	temp_data=phx_read_u32(0x1f0c000c);
	phx_write_u32(0x1f0c000c,temp_data  | 0x78 );  //release rst

	debug_print("hc3020b, release rst.\n\r");
}

void lib_pcie_sysctrl_en_rc1_3020b(void)
{
	u32 temp_data;

	phx_write_u32(0x1f0c0028,0xA5ACCEDE);//3080b is :0x1f0c0018

	temp_data=phx_read_u32(0x1f0c0008);
	phx_write_u32(0x1f0c0008,temp_data & ~(0x1<<3));  //release rst

	temp_data=phx_read_u32(0x1f0c0008);
	phx_write_u32(0x1f0c0008,temp_data & ~(0x1<<10));  //release rst

	temp_data=phx_read_u32(0x1f0c000c);
	phx_write_u32(0x1f0c000c,temp_data | (1<<6) | (1<<4));  //release rst


	debug_print("sysctrl:0x1f0c0008=%x,0c=%x \n\r",phx_read_u32(0x1f0c0008),phx_read_u32(0x1f0c000c)); 

	debug_print("release rst.rc1.3020b\n\r");
}

void set_phy(u32 phy_addr)
{
	u32 apb_paddr;
	u32 apb_pwdata;
	int j;

	apb_paddr = (0xD013 * 4 ) ; //TO MAKE port0's CORE_CLK/PMA_FULLRT_CLK 250MHz.
	apb_pwdata = 0x1111;
	phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

	apb_paddr = (0xD113 * 4 ) ; //TO MAKE port1's CORE_CLK/PMA_FULLRT_CLK 250MHz.
	apb_pwdata = 0x1111;
	phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

	apb_paddr = (0x0046 * 4 ) ; // cmn_plllc_init_preg
	apb_pwdata = 0x0000; // cmn_plllc_init_preg
	phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

	apb_paddr = (0x0047 * 4 ) ; // cmn_plllc_init_preg
	apb_pwdata = 0x0000; // cmn_plllc_init_preg
	phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

	apb_paddr = (0x004B * 4 ) ; // cmn_plllc_lock_cntstart_preg
	apb_pwdata = 0x0020; // cmn_plllc_lock_cntstart_preg
	phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

	apb_paddr = (0x0045 * 4 ) ; // cmn_plllc_dcocal_ctrl_preg
	apb_pwdata = 0x0019; // cmn_plllc_dcocal_ctrl_preg
	phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

	apb_paddr = (0x004F * 4 ) ; // cmn_plllc_bwcal_mode1_preg
	apb_pwdata = 0x0000; // cmn_plllc_bwcal_mode1_preg
	phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

	apb_paddr = (0x0050 * 4 ) ; // cmn_plllc_bwcal_mode0_preg
	apb_pwdata = 0x0000; // cmn_plllc_bwcal_mode0_preg
	phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

	apb_paddr = (0x00C6 * 4 ) ; // cmn_plllc1_init_preg
	apb_pwdata = 0x0000; // cmn_plllc1_init_preg
	phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

	apb_paddr = (0x00C7 * 4 ) ; // cmn_plllc1_itertmr_preg
	apb_pwdata = 0x0000; // cmn_plllc1_itertmr_preg
	phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

	apb_paddr = (0x00CB * 4 ) ; // cmn_plllc1_lock_cntstart_preg
	apb_pwdata = 0x0000; // cmn_plllc1_lock_cntstart_preg
	phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

	apb_paddr = (0x00C5 * 4 ) ; // cmn_plllc1_dcocal_ctrl_preg
	apb_pwdata = 0x0019; // cmn_plllc1_dcocal_ctrl_preg
	phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

	apb_paddr = (0x00CF * 4 ) ; // cmn_plllc1_bwcal_mode1_preg
	apb_pwdata = 0x0000; // cmn_plllc1_bwcal_mode1_preg
	phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

	apb_paddr = (0x00D0 * 4 ) ; // cmn_plllc1_bwcal_mode0_preg
	apb_pwdata = 0x0000; // cmn_plllc1_bwcal_mode0_preg
	phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

	apb_paddr = (0xC002 * 4 ) ;
	apb_pwdata = 0x4010;
	phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

	apb_paddr = (0xC003 * 4 ) ;
	apb_pwdata = 0x0810;
	phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

	apb_paddr = (0xC004 * 4 ) ;
	apb_pwdata = 0x0101;
	phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

	apb_paddr = (0xC006 * 4 ) ;
	apb_pwdata = 0x000A;
	phx_write_u32(phy_addr+apb_paddr, apb_pwdata);


	for (j = 0; j < 4; j=j+1) {
		apb_paddr = (0x4020 * 4 )  |  (j<<11); // fpwriso_ctrl_preg
		apb_pwdata = 0x0041; // fpwriso_ctrl_preg
		phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

		apb_paddr = (0x4041 * 4 )  |  (j << 11); // pllctrl_phase1en_preg
		apb_pwdata = 0x0001; // pllctrl_phase1en_preg
		phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

		apb_paddr = (0x4042 * 4 )  |  (j << 11); // pllctrl_phase2en_preg
		apb_pwdata = 0x0001; // pllctrl_phase2en_preg
		phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

		apb_paddr = (0x4096 * 4 )  |  (j << 11); // creq_spare_preg
		apb_pwdata = 0x8001; // creq_spare_preg
		phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

		apb_paddr = (0x40C9 * 4 )  |  (j << 11); // deq_concur_ctrl2_preg
		apb_pwdata = 0xD004; // deq_concur_ctrl2_preg
		phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

		apb_paddr = (0x4071 * 4 )  |  (j << 11); // tx_rcvdet_wait_start_preg
		apb_pwdata = 0x0005; // tx_rcvdet_wait_start_preg
		phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

		apb_paddr = (0x40B3 * 4 )  |  (j << 11); //smpcal_accum_preg
		apb_pwdata = 0x000F; //smpcal_accum_preg
		phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

		apb_paddr = (0x40B4 * 4 )  |  (j << 11); //smpcal_start_code_preg
		apb_pwdata = 0x0003; //smpcal_start_code_preg
		phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

		apb_paddr = (0x40C8 * 4 )  |  (j << 11); // deq_concur_ctrl1_preg
		apb_pwdata = 0x0000; // deq_concur_ctrl1_preg
		phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

		apb_paddr = (0x40CA * 4 )  |  (j << 11); // deq_epipwr_ctrl_preg
		apb_pwdata = 0x0000; // deq_epipwr_ctrl_preg
		phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

		apb_paddr = (0x40D3 * 4 )  |  (j << 11); // cmp_avr_timer_preg
		apb_pwdata = 0x000A; // cmp_avr_timer_preg
		phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

		apb_paddr = (0x40F1 * 4 )  |  (j << 11); // datdfe01_en_ceph_ctrl_preg
		apb_pwdata = 0x0101; // datdfe01_en_ceph_ctrl_preg
		phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

		apb_paddr = (0x40F2 * 4 )  |  (j << 11); // datdfe23_en_ceph_ctrl_preg
		apb_pwdata = 0x0101; // datdfe23_en_ceph_ctrl_preg
		phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

		apb_paddr = (0x40F3 * 4 )  |  (j << 11); // datdfe4_en_ceph_ctrl_preg
		apb_pwdata = 0x0101; // datdfe4_en_ceph_ctrl_preg
		phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

		apb_paddr = (0x40F4 * 4 )  |  (j << 11); // datgainoffset_en_ceph_ctrl_preg
		apb_pwdata = 0x0101; // datgainoffset_en_ceph_ctrl_preg
		phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

		apb_paddr = (0x40F5 * 4 )  |  (j << 11); // eoffcal_en_phctrl_preg
		apb_pwdata = 0x0000; // eoffcal_en_phctrl_preg
		phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

		apb_paddr = (0x40F8 * 4 )  |  (j << 11); // phalign_en_ctrl_preg
		apb_pwdata = 0x0000; // phalign_en_ctrl_preg
		phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

		apb_paddr = (0x40F9 * 4 )  |  (j << 11); // postprecur_en_ceph_ctrl_preg
		apb_pwdata = 0x0000; // postprecur_en_ceph_ctrl_preg
		phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

		apb_paddr = (0x40FB * 4 )  |  (j << 11); // tau_en_ceph2to0_preg
		apb_pwdata = 0x0000; // tau_en_ceph2to0_preg
		phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

		apb_paddr = (0x40FC * 4 )  |  (j << 11); // tau_en_ceph5to3_preg
		apb_pwdata = 0x0003; // tau_en_ceph5to3_preg
		phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

		apb_paddr = (0x4110 * 4 )  |  (j << 11); // phase_iter_ceph_01_preg
		apb_pwdata = 0x0101; // phase_iter_ceph_01_preg
		phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

		apb_paddr = (0x4111 * 4 )  |  (j << 11); // phase_iter_ceph_23_preg
		apb_pwdata = 0x0101; // phase_iter_ceph_23_preg
		phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

		apb_paddr = (0x4112 * 4 )  |  (j << 11); // phase_iter_ceph_45_preg
		apb_pwdata = 0x0100; // phase_iter_ceph_45_preg
		phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

		apb_paddr = (0x4124 * 4 )  |  (j << 11); // oeph_en_ctrl_preg
		apb_pwdata = 0x0000; // oeph_en_ctrl_preg
		phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

		apb_paddr = (0x415C * 4 )  |  (j << 11); // deq_openeye_ctrl_preg
		apb_pwdata = 0x5425; // deq_openeye_ctrl_preg
		phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

		apb_paddr = (0x4183 * 4 )  |  (j << 11); //cpical_res_startcode_mode23_preg
		apb_pwdata = 0x7458; //cpical_res_startcode_mode23_preg
		phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

		apb_paddr = (0x4184 * 4 )  |  (j << 11); //cpical_res_startcode_mode01_preg
		apb_pwdata = 0x4B3B; //cpical_res_startcode_mode01_preg
		phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

		//                apb_paddr = (0xD013 * 4 )  |  (j << 11);
		//                apb_pwdata = 0x1111;
		//                phx_write_u32(phy_addr+apb_paddr, apb_pwdata);

		// TO MAKE THE CORE_CLK/PMA_FULLRT_CLK 1000 MHz.
	}
}

void set_phy_chip(u32 phy_addr)
{
	u32 apb_paddr;
	u32 apb_pwdata;
	int j;

	for (j = 0; j < 4; j=j+1) {
		apb_paddr = (0xD013 * 4 )  |  (j << 11);
		apb_pwdata = 0x1111;
		phx_write_u32(phy_addr+apb_paddr, apb_pwdata);
	}
}

/******************************************************************************
 * serdes initialization, nospeedup
 */
 
//phy_addr:PHY_0_BASE,1
//phy_wrapper_addr:PHY_WRAP_0_BASE,1
void set_phy_chip_all(u32 phy_addr,u32 phy_wrapper_addr)
{

	debug_print("set_phy_chip_all.%x,%x\n\r",phy_addr,phy_wrapper_addr);

	u32 PHY_REG_ADDR = phy_wrapper_addr;
	u32 PHY_ADDR = phy_addr;

	phx_write_u32(0x0 | PHY_REG_ADDR,0x102000);//0x108000,0x102000

#if 1
	phx_write_u32((0xC003 * 4 ) | PHY_ADDR,0x0144);
	phx_write_u32((0xE000 * 4 ) | PHY_ADDR,0x3300);
	phx_write_u32((0x00A0 * 4 ) | PHY_ADDR,0x0031);
	phx_write_u32((0x0048 * 4 ) | PHY_ADDR,0x0105);
	phx_write_u32((0x0049 * 4 ) | PHY_ADDR,0x2105);
	phx_write_u32((0x004A * 4 ) | PHY_ADDR,0x3106);
	phx_write_u32((0x0050 * 4 ) | PHY_ADDR,0x8804);
	phx_write_u32((0x005A * 4 ) | PHY_ADDR,0x0ADB);
	phx_write_u32((0x0062 * 4 ) | PHY_ADDR,0x1219);
	phx_write_u32((0x0023 * 4 ) | PHY_ADDR,0x413B);
	phx_write_u32((0x6006 * 4 ) | PHY_ADDR,0x0000);
	phx_write_u32((0x604C * 4 ) | PHY_ADDR,0x0D33);
	phx_write_u32((0x606F * 4 ) | PHY_ADDR,0x9602);
	phx_write_u32((0x6071 * 4 ) | PHY_ADDR,0x0271);
	phx_write_u32((0x6081 * 4 ) | PHY_ADDR,0x813E);
	phx_write_u32((0x6085 * 4 ) | PHY_ADDR,0x8B7A);
	phx_write_u32((0x6086 * 4 ) | PHY_ADDR,0x8B7B);
	phx_write_u32((0x6087 * 4 ) | PHY_ADDR,0x8F5B);
	phx_write_u32((0x6088 * 4 ) | PHY_ADDR,0x8B4B);
	phx_write_u32((0x6091 * 4 ) | PHY_ADDR,0x033C);
	phx_write_u32((0x6096 * 4 ) | PHY_ADDR,0x0003);
	phx_write_u32((0x6097 * 4 ) | PHY_ADDR,0x44CC);
	phx_write_u32((0x609B * 4 ) | PHY_ADDR,0x088C);
	phx_write_u32((0x609C * 4 ) | PHY_ADDR,0x8000);
	phx_write_u32((0x60AC * 4 ) | PHY_ADDR,0x6A6A);
	phx_write_u32((0x60AD * 4 ) | PHY_ADDR,0x0080);
	phx_write_u32((0x60AF * 4 ) | PHY_ADDR,0x1C08);
	phx_write_u32((0x60D0 * 4 ) | PHY_ADDR,0x3E3E);
	phx_write_u32((0x60E1 * 4 ) | PHY_ADDR,0x3300);
	phx_write_u32((0x60EA * 4 ) | PHY_ADDR,0x0551);
	phx_write_u32((0x60EB * 4 ) | PHY_ADDR,0x0022);
	phx_write_u32((0x60EC * 4 ) | PHY_ADDR,0x0010);
	phx_write_u32((0x60ED * 4 ) | PHY_ADDR,0x0010);
	phx_write_u32((0x60EE * 4 ) | PHY_ADDR,0x0008);
	phx_write_u32((0x60F5 * 4 ) | PHY_ADDR,0x1561);
	phx_write_u32((0x60F8 * 4 ) | PHY_ADDR,0x1561);
	phx_write_u32((0x60FA * 4 ) | PHY_ADDR,0x00B0);
	phx_write_u32((0x60FB * 4 ) | PHY_ADDR,0x00A6);
	phx_write_u32((0x60FC * 4 ) | PHY_ADDR,0x01B6);
	phx_write_u32((0x60FF * 4 ) | PHY_ADDR,0x0333);
	phx_write_u32((0x6100 * 4 ) | PHY_ADDR,0x0788);
	phx_write_u32((0x6101 * 4 ) | PHY_ADDR,0x0777);
	phx_write_u32((0x6102 * 4 ) | PHY_ADDR,0x0BCC);
	phx_write_u32((0x6103 * 4 ) | PHY_ADDR,0x0888);
	phx_write_u32((0x6104 * 4 ) | PHY_ADDR,0x0DFF);
	phx_write_u32((0x6105 * 4 ) | PHY_ADDR,0x0888);
	phx_write_u32((0x6106 * 4 ) | PHY_ADDR,0x0CDD);
	phx_write_u32((0x6107 * 4 ) | PHY_ADDR,0x0888);
	phx_write_u32((0x6108 * 4 ) | PHY_ADDR,0x0CDD);
	phx_write_u32((0x6109 * 4 ) | PHY_ADDR,0x0888);
	phx_write_u32((0x610A * 4 ) | PHY_ADDR,0x0CDD);
	phx_write_u32((0x610B * 4 ) | PHY_ADDR,0x0599);
	phx_write_u32((0x610C * 4 ) | PHY_ADDR,0x0DEE);
	phx_write_u32((0x610D * 4 ) | PHY_ADDR,0x0084);
	phx_write_u32((0x610E * 4 ) | PHY_ADDR,0x0399);
	phx_write_u32((0x610F * 4 ) | PHY_ADDR,0x0DEE);
	phx_write_u32((0x6111 * 4 ) | PHY_ADDR,0x28FF);
	phx_write_u32((0x6115 * 4 ) | PHY_ADDR,0x0000);
	phx_write_u32((0x6118 * 4 ) | PHY_ADDR,0x0000);
	phx_write_u32((0x611A * 4 ) | PHY_ADDR,0x0100);
	phx_write_u32((0x611B * 4 ) | PHY_ADDR,0x0080);
	phx_write_u32((0x611C * 4 ) | PHY_ADDR,0x0024);
	phx_write_u32((0x611D * 4 ) | PHY_ADDR,0x0024);
	phx_write_u32((0x611E * 4 ) | PHY_ADDR,0x01FF);
	phx_write_u32((0x6121 * 4 ) | PHY_ADDR,0x001F);
	phx_write_u32((0x6122 * 4 ) | PHY_ADDR,0x1CE7);
	phx_write_u32((0x6123 * 4 ) | PHY_ADDR,0x1D67);
	phx_write_u32((0x6125 * 4 ) | PHY_ADDR,0x001F);
	phx_write_u32((0x6126 * 4 ) | PHY_ADDR,0x4000);
	phx_write_u32((0x6148 * 4 ) | PHY_ADDR,0x2011);
	phx_write_u32((0x6151 * 4 ) | PHY_ADDR,0x2008);
	phx_write_u32((0x6155 * 4 ) | PHY_ADDR,0x003F);
	phx_write_u32((0x6158 * 4 ) | PHY_ADDR,0x2E21);
	phx_write_u32((0x6159 * 4 ) | PHY_ADDR,0x2121);
	phx_write_u32((0x615C * 4 ) | PHY_ADDR,0x5882);
	phx_write_u32((0x6161 * 4 ) | PHY_ADDR,0x0023);
	phx_write_u32((0x61B0 * 4 ) | PHY_ADDR,0x559E);
	phx_write_u32((0x61D8 * 4 ) | PHY_ADDR,0x0019);
	phx_write_u32((0x61D9 * 4 ) | PHY_ADDR,0x7FFF);
	phx_write_u32((0x61DA * 4 ) | PHY_ADDR,0x0028);
	phx_write_u32((0x61DB * 4 ) | PHY_ADDR,0x0028);
	phx_write_u32((0x61DC * 4 ) | PHY_ADDR,0x0005);
	phx_write_u32((0x61DD * 4 ) | PHY_ADDR,0x000F);
	phx_write_u32((0x61DE * 4 ) | PHY_ADDR,0x7FC3);
	phx_write_u32((0x61DF * 4 ) | PHY_ADDR,0x7F81);
	phx_write_u32((0x61E0 * 4 ) | PHY_ADDR,0x7FC3);
	phx_write_u32((0x61E1 * 4 ) | PHY_ADDR,0x7FC3);
	phx_write_u32((0x61E2 * 4 ) | PHY_ADDR,0x7F81);
	phx_write_u32((0x61E3 * 4 ) | PHY_ADDR,0x7FF1);
	phx_write_u32((0x61E4 * 4 ) | PHY_ADDR,0x0000);
	phx_write_u32((0x61E5 * 4 ) | PHY_ADDR,0x1000);
	phx_write_u32((0x61E6 * 4 ) | PHY_ADDR,0x1092);
	phx_write_u32((0x61E7 * 4 ) | PHY_ADDR,0x1092);
	phx_write_u32((0x61E8 * 4 ) | PHY_ADDR,0x0000);
	phx_write_u32((0x61F3 * 4 ) | PHY_ADDR,0x392B);
	phx_write_u32((0x60E8 * 4 ) | PHY_ADDR,0x002D);
	phx_write_u32((0x60E9 * 4 ) | PHY_ADDR,0x1B24);
	phx_write_u32((0x60E8 * 4 ) | PHY_ADDR,0x002E);
	phx_write_u32((0x60E9 * 4 ) | PHY_ADDR,0x1B25);
	phx_write_u32((0x60E8 * 4 ) | PHY_ADDR,0x002F);
	phx_write_u32((0x60E9 * 4 ) | PHY_ADDR,0x1B26);
	phx_write_u32((0x60E8 * 4 ) | PHY_ADDR,0x0030);
	phx_write_u32((0x60E9 * 4 ) | PHY_ADDR,0x1B27);
	phx_write_u32((0x60E8 * 4 ) | PHY_ADDR,0x0031);
	phx_write_u32((0x60E9 * 4 ) | PHY_ADDR,0x1B28);
	phx_write_u32((0x60E8 * 4 ) | PHY_ADDR,0x0032);
	phx_write_u32((0x60E9 * 4 ) | PHY_ADDR,0x1B29);
	phx_write_u32((0x60E8 * 4 ) | PHY_ADDR,0x0033);
	phx_write_u32((0x60E9 * 4 ) | PHY_ADDR,0x1B29);
	phx_write_u32((0x60E8 * 4 ) | PHY_ADDR,0x0034);
	phx_write_u32((0x60E9 * 4 ) | PHY_ADDR,0x1B2A);
	phx_write_u32((0x60E8 * 4 ) | PHY_ADDR,0x0035);
	phx_write_u32((0x60E9 * 4 ) | PHY_ADDR,0x1B2C);
	phx_write_u32((0x60E8 * 4 ) | PHY_ADDR,0x0036);
	phx_write_u32((0x60E9 * 4 ) | PHY_ADDR,0x1B2D);
	phx_write_u32((0x60E8 * 4 ) | PHY_ADDR,0x0037);
	phx_write_u32((0x60E9 * 4 ) | PHY_ADDR,0x1B2E);
	phx_write_u32((0x60E8 * 4 ) | PHY_ADDR,0x0038);
	phx_write_u32((0x60E9 * 4 ) | PHY_ADDR,0x1B2F);
	phx_write_u32((0x60E8 * 4 ) | PHY_ADDR,0x0039);
	phx_write_u32((0x60E9 * 4 ) | PHY_ADDR,0x1B30);
	phx_write_u32((0x60E8 * 4 ) | PHY_ADDR,0x003A);
	phx_write_u32((0x60E9 * 4 ) | PHY_ADDR,0x1B31);
	phx_write_u32((0x60E8 * 4 ) | PHY_ADDR,0x003B);
	phx_write_u32((0x60E9 * 4 ) | PHY_ADDR,0x1B32);
	phx_write_u32((0x60E8 * 4 ) | PHY_ADDR,0x003C);
	phx_write_u32((0x60E9 * 4 ) | PHY_ADDR,0x1B33);
	phx_write_u32((0x60E8 * 4 ) | PHY_ADDR,0x003D);
	phx_write_u32((0x60E9 * 4 ) | PHY_ADDR,0x1B34);
	phx_write_u32((0x60E8 * 4 ) | PHY_ADDR,0x003E);
	phx_write_u32((0x60E9 * 4 ) | PHY_ADDR,0x1B35);
	phx_write_u32((0x60E8 * 4 ) | PHY_ADDR,0x003F);
	phx_write_u32((0x60E9 * 4 ) | PHY_ADDR,0x1B36);

#endif

	int j;
	for(j = 0; j < 2; j++)
	{
		phx_write_u32((0xD013 * 4 ) | PHY_ADDR  |  (j << 0x10),0x1111);
	}

	debug_print("set_phy_chip_all,end.\n\r");


}

void enable_phy(u32 phy_wrapper_addr)
{
	u32 apb_paddr;
	u32 apb_pwdata;

	debug_print("enable_phy:%x\n\r",phy_wrapper_addr);

	//phy_p00_mode set to 0
	apb_paddr = 0x14;
	apb_pwdata =  0x0;
	phx_write_u32(phy_wrapper_addr+apb_paddr, apb_pwdata);

	//phy_p01_mode set to 0
	apb_paddr = 0x20;
	apb_pwdata =  0x0;
	phx_write_u32(phy_wrapper_addr+apb_paddr, apb_pwdata);
	//phy_p02_mode set to 0
	apb_paddr = 0x2c;
	apb_pwdata =  0x0;
	phx_write_u32(phy_wrapper_addr+apb_paddr, apb_pwdata);
	//phy_p03_mode set to 0
	apb_paddr = 0x38;
	apb_pwdata =  0x0;
	phx_write_u32(phy_wrapper_addr+apb_paddr, apb_pwdata);

	//fudy.tmp.for serdes loopback
#if 0 //serdes loopback
	lp_phy_ctrl1(PHY_0_BASE);
	lp_phy_ctrl1(PHY_1_BASE);
#endif
	
	//release phy reset
	apb_paddr = 0x0;
	apb_pwdata = 0x11;
	phx_write_u32(phy_wrapper_addr+apb_paddr, apb_pwdata);
}

void pcie_training_enable(u32 pcie_wrapper_addr)
{
	u32 apb_paddr;
	u32 apb_pwdata;

	apb_paddr = 0x200 * 4;
	apb_pwdata = 0x3;
	phx_write_u32(pcie_wrapper_addr+apb_paddr, apb_pwdata);
}

void wait_clock_stable(u32 pcie_wrapper_addr)
{
	u32 apb_paddr;
	u32 apb_prdata;
	u8 clock_state;
	u32 wait_count = 0;//fudy.
	int j;
	do {
		apb_paddr = 0x0;
		apb_prdata = phx_read_u32(pcie_wrapper_addr+apb_paddr);
		clock_state = ((apb_prdata>>16) & 0x1);
		//debug_print("addr: 0x%x, data: 0x%x, 0x%x", apb_paddr, apb_prdata, link_state);

		//fudy.wait a moment:
		wait(10000);
		wait_count++;
		if(wait_count > 20) break;
		
	} while (clock_state != 0x0);

	apb_paddr = 0x0;

	for (j = 0; j < 50; j=j+1) {
		apb_prdata = phx_read_u32(pcie_wrapper_addr+apb_paddr);
	}
}

void pcie_set_ep_bar(u32 pcie_mgmt_addr)
{
	u32 apb_paddr = 0x240;
	u32 apb_pwdata = 0x50585cd; //Physical Function BAR Configuration Register 0, 1GB BAR0, Mem
	//[4:0] 01101:1 Gb,BAR0
	//[7:5] 110: 64bit memory BAR0
	phx_write_u32(pcie_mgmt_addr+apb_paddr, apb_pwdata);
}

/******************************************************************************
 * ep bar set, 128KB, 64bit mem
 */
void pcie_set_ep_bar0(u32 pcie_mgmt_addr)
{
	u32 apb_paddr = 0x240;
	u32 apb_pwdata = 0x50585c0; //Physical Function BAR Configuration Register 0, 128 KB BAR0, Mem
	//[4:0] 00000 : 128 KB,BAR0
	//[7:5] 110: 64bit memory BAR0
	phx_write_u32(pcie_mgmt_addr+apb_paddr, apb_pwdata);
}

void pcie_set_ep_ctrl_status(u32 pcie_ctrl_cfg_addr)
{
	u32 apb_paddr = 0x4;
	u32 apb_pwdata = 0x7; //
	phx_write_u32(pcie_ctrl_cfg_addr+apb_paddr, apb_pwdata);
}


void pcie_set_rc(u32 pcie_wrapper_addr)
{
	u32 apb_paddr;
	u32 apb_pwdata;
	//u32 apb_prdata;

	apb_paddr  = 0x0 * 4;
	//apb_prdata = phx_read_u32(pcie_wrapper_addr+apb_paddr);
	//debug_print("addr: 0x%x, data: 0x%x", apb_paddr, apb_prdata);

	// set ref to RC
	//		  apb_pwdata = 0x1414;
	apb_pwdata = 0x4 | phx_read_u32(pcie_wrapper_addr+apb_paddr);//fudy.

	phx_write_u32(pcie_wrapper_addr+apb_paddr, apb_pwdata);
}

void pcie_set_msg_reg(u32 pcie_wrapper_addr)
{
	u32 apb_paddr;
	u32 apb_pwdata;

	apb_paddr  = 0x890;        //MSG_RECV_CTRL
	apb_pwdata = 0x3;
	phx_write_u32(pcie_wrapper_addr+apb_paddr, apb_pwdata);

	apb_paddr  = 0x89c;        //MSG_RECV_THRESHOLD
	apb_pwdata = 0xff;
	phx_write_u32(pcie_wrapper_addr+apb_paddr, apb_pwdata);

	apb_paddr  = 0x8F0;        //INTR_EN
	apb_pwdata = 0x40;
	phx_write_u32(pcie_wrapper_addr+apb_paddr, apb_pwdata);

	apb_paddr  = 0x8A4;        //MSG_RECV_INTR_STATUS,clear all status
	apb_pwdata = 0xffffffff;
	phx_write_u32(pcie_wrapper_addr+apb_paddr, apb_pwdata);

	//apb_paddr  = 0x8B0;        //MSG_RECV_STORE,defaul all message store in fifo
	//apb_pwdata = 0x40000000;
	//phx_write_u32(pcie_wrapper_addr+apb_paddr, apb_pwdata);

}

void pcie_set_msi_reg(u32 pcie_wrapper_addr)
{
	u32 apb_paddr;
	u32 apb_pwdata;

	apb_paddr  = 0x8F0;        //INTR_EN
	apb_pwdata = 0x10000;
	phx_write_u32(pcie_wrapper_addr+apb_paddr, apb_pwdata);

	apb_paddr  = 0x0928;        //MSIVR_1
	apb_pwdata = 0x1;
	phx_write_u32(pcie_wrapper_addr+apb_paddr, apb_pwdata);

}


void pcie_read_msg_reg(u32 pcie_wrapper_addr)
{
	u32 apb_paddr;
	u32 apb_prdata;

	apb_paddr = 0x8A4;         //MSG_RECV_INTR_STATUS
	apb_prdata = phx_read_u32(pcie_wrapper_addr+apb_paddr);
	debug_print("1111addr: 0x%x, data: 0x%x", apb_paddr, apb_prdata);

	apb_paddr = 0x8A8;         //MSG_RECV_FIFO_DATA
	apb_prdata = phx_read_u32(pcie_wrapper_addr+apb_paddr);
	debug_print("2222addr: 0x%x, data: 0x%x", apb_paddr, apb_prdata);

	apb_paddr = 0x8A8;         //MSG_RECV_FIFO_DATA
	apb_prdata = phx_read_u32(pcie_wrapper_addr+apb_paddr);
	debug_print("3333addr: 0x%x, data: 0x%x", apb_paddr, apb_prdata);


}


void pcie_read_msi_reg(u32 pcie_wrapper_addr)
{
	u32 apb_paddr;
	u32 apb_prdata;
	u32 msisr;

	do{
		apb_paddr = 0x900;         //MSISR
		apb_prdata = phx_read_u32(pcie_wrapper_addr+apb_paddr);
		msisr  = ((apb_prdata>>1) & 0x1);
	}while(msisr!= 0x1);
	debug_print("1111addr: 0x%x, data: 0x%x", apb_paddr, apb_prdata);

}

void wait_msg_intr(u32 pcie_wrapper_addr)
{
	u32 apb_paddr;
	u32 apb_prdata;
	u32 intr_status;

	do{
		apb_paddr    = 0x8EC;
		apb_prdata   = phx_read_u32(pcie_wrapper_addr+apb_paddr);
		intr_status  = ((apb_prdata>>6) & 0x1);
	}while(intr_status != 0x1);
}


#if 0 //3020
static u64 pcie0_region_axi_base[] = {
	0x100000000,
	0x100000000 + 1 * (1 << OB_REGION_SIZE),
	0x100000000 + 2 * (1 << OB_REGION_SIZE),
};
#else //3080
static u64 pcie0_region_axi_base[] = {
	0x600000000,
	0x600000000 + 1 * (1 << OB_REGION_SIZE),
	0x600000000 + 2 * (1 << OB_REGION_SIZE),
};
#endif


static u64 pcie0_region_pass_bits[] = {
	OB_REGION_SIZE - 1,
	OB_REGION_SIZE - 1,
	OB_REGION_SIZE - 1,
};
#ifndef REGION_USE_SRAM
static u64 pcie0_region_addr_trans[] = {
	0x0,
	0x0 + 1 * (1 << OB_REGION_SIZE),
	0x0 + 2 * (1 << OB_REGION_SIZE),
};
#define TEST_MEM_REGION_SIZE (1<<OB_REGION_SIZE)
#else //for sram,fudy.

#define SRAM_OB_REGION_SIZE 0x20000
#define TEST_MEM_REGION_SIZE SRAM_OB_REGION_SIZE
static u64 pcie0_region_addr_trans[] = {
	0x0,
	0x10000000 + 1 * (SRAM_OB_REGION_SIZE),
	0x10000000 + 2 * (SRAM_OB_REGION_SIZE),
};


#endif


static u64 pcie0_region_addr_trans_msg[] = {
	0x0, // Zero for message
	0x0, // Zero for message
	0x0, // Zero for message
};
static u64 pcie0_region_addr_trans_vdm[] = {
	0x1234567887654321ull, // VDM header[63:8]
	0xABCDEF0110FEDCBAull, // VDM header[63:8]
	0xF007BA11F007BA11ull, // VDM header[63:8]
};


#define TRANS_TYPE_MEM  0
#define TRANS_TYPE_IO   1
#define TRANS_TYPE_CFG  2
#define TRANS_TYPE_MSG  3
#define TRANS_TYPE_VDM  4

void pcie0_prog_axi_region (int region_num, int trans_type)
{
	u32 region_base;
	u32 region_data;
	u32 tc;
	u32 rc_mode;

	region_base = 0;
	region_base = region_base + region_num * 0x20;

	// addr0
	switch (trans_type) {
		case TRANS_TYPE_MEM:
		case TRANS_TYPE_IO:
		case TRANS_TYPE_CFG :
			region_data = (pcie0_region_addr_trans[region_num] & 0xffffffff) + pcie0_region_pass_bits[region_num];
			break;
		case TRANS_TYPE_MSG :
			region_data = (pcie0_region_addr_trans_msg[region_num] & 0xffffffff);
			break;
		case TRANS_TYPE_VDM :
			region_data = pcie0_region_addr_trans_vdm[region_num] & 0xffffff00;
			break;
		default :
			debug_print("ERROR! Unsupported pcie0_prog_axi_region trans_type.");
			break;
	}
	phx_write_u32(PCIE0_AXI_CFG_BASE+region_base + 0x00, region_data);

	// addr1
	switch(trans_type) {
		case TRANS_TYPE_MEM:
		case TRANS_TYPE_IO:
		case TRANS_TYPE_CFG :
			region_data = pcie0_region_addr_trans[region_num] >> 32;
			break;
		case TRANS_TYPE_MSG :
			region_data = pcie0_region_addr_trans_msg[region_num] >> 32;
			break;
		case TRANS_TYPE_VDM :
			region_data = pcie0_region_addr_trans_vdm[region_num] >> 32;
			break;
		default :
			debug_print("ERROR! Unsupported pcie0_prog_axi_region trans_type.");
			break;
	}
	phx_write_u32(PCIE0_AXI_CFG_BASE+region_base + 0x04, region_data);

	// desc0
	tc = 0;
	rc_mode = 1;
	switch (trans_type) {
		case TRANS_TYPE_MEM :
			region_data = (rc_mode << 21) | (tc << 17) | 0x02;
			break;
		case TRANS_TYPE_IO  :
			region_data = (rc_mode << 21) | (tc << 17) | 0x06;
			break;
		case TRANS_TYPE_CFG :
			region_data = (rc_mode << 21) | (tc << 17) | 0x0A;
			break;
		case TRANS_TYPE_MSG :
			region_data = (rc_mode << 21) | (tc << 17) | 0x0C;
			break;
		case TRANS_TYPE_VDM :
			region_data = (rc_mode << 21) | (tc << 17) | ((pcie0_region_addr_trans_vdm[region_num] & 0xff) << 8) | 0x0D;
			break;
		default :
			debug_print("ERROR! Unsupported REF_PROG_AXI_REGION trans_type.");
			break;
	}
	phx_write_u32(PCIE0_AXI_CFG_BASE+region_base + 0x08, region_data);

	// desc1
	phx_write_u32(PCIE0_AXI_CFG_BASE+region_base + 0x0C, 0x00000000);

	// Address0 register (ADDR_BASED_REGION_DECODE)
	region_data = pcie0_region_axi_base[region_num];
	//region_data[6] = 1'b1;
	region_data = (region_data & 0xffffffc0) | (pcie0_region_pass_bits[region_num] & 0x3f);
	phx_write_u32(PCIE0_AXI_CFG_BASE+region_base + 0x18, region_data);

	// Address1 register (ADDR_BASED_REGION_DECODE)
	region_data = pcie0_region_axi_base[region_num] >> 32;
	phx_write_u32(PCIE0_AXI_CFG_BASE+region_base + 0x1c, region_data);
}

void pcie0_configure_axi_regions(int region0_type, int region1_type, int region2_type)
{
	if(region0_type != -1)
		pcie0_prog_axi_region(0,region0_type);
	if(region1_type != -1)
		pcie0_prog_axi_region(1,region1_type);
	if(region2_type != -1)
		pcie0_prog_axi_region(2,region2_type);
}

/******************************************************************************
 * pcie x4 ob cfg
 * size: bit numbers
 * trans_type:2(MEM),0x6(IO),0xa(CFG),0xc(MSG)
 * pcie_config_ob(PCIE0_AXI_CFG_BASE,0,0x600000000,0,28-1,1,0xa);
	 PCIE0_AXI_CFG_BASE,	0,			0x600000000,	0,				28-1,			1,		0xa
	 pcie0					pcie_addr	axi_addr		region_number	trans_bits		is_rc	CFG
 * pcie_config_ob(PCIE1_AXI_CFG_BASE,0,0x700000000,0,28-1,0,0x2);
	 PCIE1_AXI_CFG_BASE,	0,			0x700000000,	0,				28-1,			0,		0x2
	 pcie1					pcie_addr	axi_addr		region_number	trans_bits		is_rc	MEM

 */
void pcie_config_ob(u64 axi_cfg_base,u64 pcie_addr,u64 axi_addr,u32 region_num,u32 size,u32 is_rc, u32 trans_type)
{
	u64 atu_baseaddr = axi_cfg_base + (region_num*0x20) + (0x1<<22);
	
	printk("<**DEBUG**> [%s():_%d_]:: atu_baseaddr:0x%lx, axi_addr:0x%lx, pcie_addr:0x%lx\n", \
			__func__, __LINE__,atu_baseaddr,axi_addr,pcie_addr);
	
	//addr0:
	printk("<**DEBUG**> [%s():_%d_]\n", __func__, __LINE__);
	phx_write_u32(atu_baseaddr+region_num*0x20+0x00,L32(pcie_addr)+size);
	//addr1:
	printk("<**DEBUG**> [%s():_%d_]\n", __func__, __LINE__);
	phx_write_u32(atu_baseaddr+region_num*0x20+0x04,H32(pcie_addr));

	//desc0:
	u32 tc = 0;
	printk("<**DEBUG**> [%s():_%d_]\n", __func__, __LINE__);
	phx_write_u32(atu_baseaddr+region_num*0x20+0x08,((is_rc << 23) | (tc << 17) | trans_type));//0x2:for MEM
	//desc1:
	printk("<**DEBUG**> [%s():_%d_]\n", __func__, __LINE__);
	phx_write_u32(atu_baseaddr+region_num*0x20+0x0c,0x00000000);

	//axi0:
	printk("<**DEBUG**> [%s():_%d_]\n", __func__, __LINE__);
	phx_write_u32(atu_baseaddr+region_num*0x20+0x18,L32(axi_addr)+size);
	//axi1:
	printk("<**DEBUG**> [%s():_%d_]\n", __func__, __LINE__);
	phx_write_u32(atu_baseaddr+region_num*0x20+0x1c,H32(axi_addr));
}



/******************************************************************************
 * pcie1 ep ib set, 128KB, pcie addr:0x10020000, axi addr: 0x105c0000
 */
 
void pcie1_ep_config_ib(void)
{
	u64 addr = 0;
	u32 val = 0;
	
    // Controller Config
    // DUT_ Core command and status register - reg addr 1
    //  - Enable memory and IO 
    //---------------------------------------------------
	phx_write_u32(PCIE1_CTRL_CONFIG_BASE+0x4, 0x00000007);

    // EP BAR Config Register
	//Physical Function BAR Configuration Register 0, 128 KB BAR0, Mem
	//[4:0] 00000 : 128 KB,BAR0
	//[7:5] 110: 64bit memory BAR0
    //---------------------------------------------------
	phx_write_u32(PCIE1_CTRL_MGMT_BASE+0x240, 0x50585c0);

	//set EP BAR0, RC pcie config write:
	addr = pcie0_region_axi_base[0]+0x10;
	phx_write_u32(addr,0xffffffff);
    debug_print("EP BAR0=%x\n\r",phx_read_u32(addr));
	phx_write_u32(addr,0x10020000);
    debug_print("EP BAR0=%x\n\r",phx_read_u32(addr));

    // Inbound address translation, BAR0, set AXI address
    //---------------------------------------------------
    // addr0
    phx_write_u32(PCIE1_AXI_CFG_BASE+0x840, EP_IB_AXI_ADDR_SRAM);//EP BAR0 addr0,no bits.
    //in this environment, size 0x10:17bits:size=0x20000,128K
    
    // addr1
    phx_write_u32(PCIE1_AXI_CFG_BASE+0x844, 0x00000000);
	
}


static void space_show(void)
{
	u32 len = 64;
	u32 i = 0;
	u64 addr = 0;

	addr = PCIE_0_BASE;
	debug_print("\n\rPCIE0-%x: \n\r",addr);
	for(i = 0; i<len; i+=4)
	{
		debug_print("%x ",phx_read_u32(addr+i));
	}

	addr = PCIE_1_BASE;
	debug_print("\n\rPCIE1-%x: \n\r",addr);
	for(i = 0; i<len; i+=4)
	{
		debug_print("%x ",phx_read_u32(addr+i));
	}

	addr = PCIE_WRAP_0_BASE;
	debug_print("\n\rPCIE0WRAP-%x: \n\r",addr);
	for(i = 0; i<len; i+=4)
	{
		debug_print("%x ",phx_read_u32(addr+i));
	}

	addr = PCIE_WRAP_1_BASE;
	debug_print("\n\rPCIE1WRAP-%x: \n\r",addr);
	for(i = 0; i<len; i+=4)
	{
		debug_print("%x ",phx_read_u32(addr+i));
	}

	addr = PHY_0_BASE;
	debug_print("\n\rPHY0-%x: \n\r",addr);
	for(i = 0; i<len; i+=4)
	{
		debug_print("%x ",phx_read_u32(addr+i));
	}
	addr = PHY_1_BASE;
	debug_print("\n\rPHY1-%x: \n\r",addr);
	for(i = 0; i<len; i+=4)
	{
		debug_print("%x ",phx_read_u32(addr+i));
	}
	addr = PHY_WRAP_0_BASE;
	debug_print("\n\rPHY0WRAP-%x: \n\r",addr);
	for(i = 0; i<len; i+=4)
	{
		debug_print("%x ",phx_read_u32(addr+i));
	}
	addr = PHY_WRAP_1_BASE;
	debug_print("\n\rPHY1WRAP-%x: \n\r",addr);
	for(i = 0; i<len; i+=4)
	{
		debug_print("%x ",phx_read_u32(addr+i));
	}

	debug_print("\n\r");


}

static void cfg_show(void)
{
	u32 i = 0;

#ifdef PCIE_DEBUG_COMPLETE
	debug_print("----base: \n\r");

	debug_print("PCIE0-%x:%x \n\r",PCIE_0_BASE,phx_read_u32(PCIE_0_BASE));
	debug_print("PCIE1-%x:%x \n\r",PCIE_1_BASE,phx_read_u32(PCIE_1_BASE));

	debug_print("WRAP0-%x:%x \n\r",PCIE_WRAP_0_BASE,phx_read_u32(PCIE_WRAP_0_BASE));
	debug_print("WRAP1-%x:%x \n\r",PCIE_WRAP_1_BASE,phx_read_u32(PCIE_WRAP_1_BASE));

	debug_print("PHY0-%x:%x \n\r",PHY_0_BASE,phx_read_u32(PHY_0_BASE));
	debug_print("PHY1-%x:%x \n\r",PHY_1_BASE,phx_read_u32(PHY_1_BASE));

	debug_print("WRAP0-%x:%x \n\r",PHY_WRAP_0_BASE,phx_read_u32(PHY_WRAP_0_BASE));
	debug_print("WRAP1-%x:%x \n\r",PHY_WRAP_1_BASE,phx_read_u32(PHY_WRAP_1_BASE));

	//	debug_print("wrap:%x,%x,%x \n\r",phx_read_u32(PCIE_WRAP_0_BASE),
	//		phx_read_u32(PCIE_WRAP_0_BASE+4),
	//		phx_read_u32(PCIE_WRAP_0_BASE+8));

	space_show();

#endif

	debug_print("----sta: \n\r");

	//mode sel
	debug_print("mode0:%x[2]=%x \n\r",PCIE_WRAP_0_BASE,(phx_read_u32(PCIE_WRAP_0_BASE)>>2)&0x1);
	debug_print("mode1:%x[2]=%x \n\r",PCIE_WRAP_1_BASE,(phx_read_u32(PCIE_WRAP_1_BASE)>>2)&0x1);

	//link sta
	debug_print("link0:%x[8:7]=%x \n\r",PCIE_WRAP_0_BASE+0x800,(phx_read_u32(PCIE_WRAP_0_BASE+0x800)>>7)&0x3);
	debug_print("link1:%x[8:7]=%x \n\r",PCIE_WRAP_1_BASE+0x800,(phx_read_u32(PCIE_WRAP_1_BASE+0x800)>>7)&0x3);


}


/******************************************************************************
 * link reg show, include pcie_wrap(config_input_output_0) & pcie(pl_config_0_reg)
 * pcie_wrapper_addr:PCIE_0_BASE/PCIE_1_BASE
 * pcie_wrapper_addr:PCIE_WRAP_0_BASE/PCIE_WRAP_1_BASE
 */

static void link_show(u32 pcie_addr,u32 pcie_wrapper_addr)
{
	u64 pl_config_reg,config_input_output;
	pl_config_reg = pcie_addr + 0x100000;
	config_input_output = pcie_wrapper_addr + 0x800;

	debug_print("link_show:%x\n\r",pcie_addr);
	u32 val = 0;
	val = phx_read_u32(pl_config_reg);
	debug_print("pl_config_reg(pcie):[%x]=%x \n\r",pl_config_reg,val);
	debug_print("[0]Link status      = %x(1=link training complete)\n\r",val & 1);
	debug_print("[2:1]Neg Lane Count = %x(00=x1,01=x2,10=x4,11=x8)\n\r",(val>>1)&0x3);
	debug_print("[4:3]Neg Speed      = %x(00=2.5G,01=5,10=8,11=16)\n\r",(val>>3)&0x3);
	debug_print("[29:24]LTSSM State  = %x\n\r",(val>>24)&0x3f);
	debug_print("[31]Master Loop EN  = %x\n\r",(val>>31)&0x1);

	val = 0;
	val = phx_read_u32(config_input_output);
	debug_print("config_input_output(pcie_warp):%x \n\r",val);
	debug_print("[0]Link training enable = %x\n\r",val & 1);
	debug_print("[1]config enable        = %x\n\r",(val>>1) & 1);
	debug_print("[4:3]Neg Speed          = %x(00=2.5G,01=5,10=8,11=16)\n\r",(val>>3)&0x3);
	debug_print("[6:5]Neg link width     = %x\n\r",(val>>5)&0x3);
	debug_print("[8:7]link status        = %x\n\r",(val>>7)&0x3);

	debug_print("\n\r");

}

/******************************************************************************
 * link reg show,  use pcie core reg: pcie(pl_config_0_reg)
 * pcie_addr:PCIE_0_BASE/PCIE_1_BASE
 */

static void link_show_core(u32 pcie_addr)
{
	u64 pl_config_reg = pcie_addr + 0x100000;
	u32 val = 0;

	debug_print("link_show:\n\r");

	val = phx_read_u32(pl_config_reg);
	
	debug_print("pl_config_reg(pcie):[%x]=%x \n\r",pl_config_reg,val);
	debug_print("[0]Link status      = %x(1=link training complete)\n\r",val & 1);
	debug_print("[2:1]Neg Lane Count = %x(00=x1,01=x2,10=x4,11=x8)\n\r",(val>>1)&0x3);
	debug_print("[4:3]Neg Speed      = %x(00=2.5G,01=5,10=8,11=16)\n\r",(val>>3)&0x3);

	debug_print("\n\r");
}


/******************************************************************************
 * pcie x4 serdes loopback
 * phy: PHY_0_BASE,PHY_1_BASE
 */

static void lp_phy_ctrl(u64 phy)
{
	u64 addr = phy + (0x6050<<2);//lpbkneparen_preg
	u32 val = phx_read_u32(addr);//get
	debug_print("lp_ctrl:%x = %x \n\r",addr,val);

	//	val |= (u32)0x2;//bit1:lpbk en, near
	val |= (u32)0x4;//bit1:lpbk en, near, serial

	debug_print("val=%x\n\r",val);
	phx_write_u32(addr,val);

	val = phx_read_u32(addr);
	debug_print("lp_ctrl:%x = %x \n\r\n\r",addr,val);

}

/******************************************************************************
 * for 4 lane config
 */
static void lp_phy_ctrl1(u64 phy)
{
	u32 lane = 0;//0-3

	for(lane = 0; lane < 4; lane++){

		u64 addr = phy + ((0x4050+0x200*lane)<<2);//lpbkneparen_preg
		u32 val = phx_read_u32(addr);//get
		debug_print("lp_ctrl:%x = %x \n\r",addr,val);

		//	val |= (u32)0x2;//bit1:lpbk en, near
		val |= (u32)0x4;//bit1:lpbk en, near, serial

		debug_print("val=%x\n\r",val);
		phx_write_u32(addr,val);

		val = phx_read_u32(addr);
		debug_print("lp_ctrl:%x = %x \n\r\n\r",addr,val);
	}


}


void pcie_link(u32 pcieaddr, u32 pcie_wrapper_addr,
					 u32 phyaddr, u32 phy_wrapper_addr)
{
	u32 ctrl_num = 0;
	if(PCIE_0_BASE != pcieaddr)
		ctrl_num = 1;
	

	pcie_set_rc(pcie_wrapper_addr);//set rc

	set_phy_chip_all(phyaddr,phy_wrapper_addr);

	enable_phy(phy_wrapper_addr);

//	cap_link_show(pcieaddr);//for debug
//	link_cap(pcieaddr);//for debug

	pcie_training_enable(pcie_wrapper_addr);
	
	wait_clock_stable(pcie_wrapper_addr);

	debug_print("\n\r------ link test: pciex4 ctrl%d ------\n\r\n\r",ctrl_num);

	wait_link_down(pcie_wrapper_addr);

	debug_print("================\n\r\n\r");

}


/******************************************************************************
 * serdes initialization, nospeedup
 */
 
//phy_addr:PHY_0_BASE,1
//phy_wrapper_addr:PHY_WRAP_0_BASE,1
void lib_phy_init_nospeedup(u32 phy_addr,u32 phy_wrapper_addr)
{
	printk("set_phy_chip_all.phy_wrapper_addr=%x,PHY_IP_ADDR=%x\n",phy_wrapper_addr,phy_addr);

#ifdef PCIEX4_DEBUG_PRINT
	printk("phy_wrapper_addr=%x=%x\n",phy_wrapper_addr,phx_read_u32(phy_wrapper_addr));
	printk("0x8 | phy_wrapper_addr=%x=%x\n",(0x8 | phy_wrapper_addr),phx_read_u32(0x8 | phy_wrapper_addr));
    u32 addr;
    addr = 0x1f6c0000;  printk("%x=%x\n",addr,phx_read_u32(addr));
    addr = 0x1f6c0008;  printk("%x=%x\n",addr,phx_read_u32(addr));
#endif

//	phx_write_u32(0x0 | phy_wrapper_addr,0x102000);//0x108000,0x102000
    phx_write_u32(0x0 | phy_wrapper_addr,0x100000);//phy_apb_reset_n

#if 1
	phx_write_u32((0xC003 * 4 ) | phy_addr,0x0144);
	phx_write_u32((0xE000 * 4 ) | phy_addr,0x3300);
	phx_write_u32((0x00A0 * 4 ) | phy_addr,0x0031);
	phx_write_u32((0x0048 * 4 ) | phy_addr,0x0105);
	phx_write_u32((0x0049 * 4 ) | phy_addr,0x2105);
	phx_write_u32((0x004A * 4 ) | phy_addr,0x3106);
	phx_write_u32((0x0050 * 4 ) | phy_addr,0x8804);
	phx_write_u32((0x005A * 4 ) | phy_addr,0x0ADB);
	phx_write_u32((0x0062 * 4 ) | phy_addr,0x1219);
	phx_write_u32((0x0023 * 4 ) | phy_addr,0x413B);
	phx_write_u32((0x6006 * 4 ) | phy_addr,0x0000);
	phx_write_u32((0x604C * 4 ) | phy_addr,0x0D33);
	phx_write_u32((0x606F * 4 ) | phy_addr,0x9602);
	phx_write_u32((0x6071 * 4 ) | phy_addr,0x0271);
	phx_write_u32((0x6081 * 4 ) | phy_addr,0x813E);
	phx_write_u32((0x6085 * 4 ) | phy_addr,0x8B7A);
	phx_write_u32((0x6086 * 4 ) | phy_addr,0x8B7B);
	phx_write_u32((0x6087 * 4 ) | phy_addr,0x8F5B);
	phx_write_u32((0x6088 * 4 ) | phy_addr,0x8B4B);
	phx_write_u32((0x6091 * 4 ) | phy_addr,0x033C);
	phx_write_u32((0x6096 * 4 ) | phy_addr,0x0003);
	phx_write_u32((0x6097 * 4 ) | phy_addr,0x44CC);
	phx_write_u32((0x609B * 4 ) | phy_addr,0x088C);
	phx_write_u32((0x609C * 4 ) | phy_addr,0x8000);
	phx_write_u32((0x60AC * 4 ) | phy_addr,0x6A6A);
	phx_write_u32((0x60AD * 4 ) | phy_addr,0x0080);
	phx_write_u32((0x60AF * 4 ) | phy_addr,0x1C08);
	phx_write_u32((0x60D0 * 4 ) | phy_addr,0x3E3E);
	phx_write_u32((0x60E1 * 4 ) | phy_addr,0x3300);
	phx_write_u32((0x60EA * 4 ) | phy_addr,0x0551);
	phx_write_u32((0x60EB * 4 ) | phy_addr,0x0022);
	phx_write_u32((0x60EC * 4 ) | phy_addr,0x0010);
	phx_write_u32((0x60ED * 4 ) | phy_addr,0x0010);
	phx_write_u32((0x60EE * 4 ) | phy_addr,0x0008);
	phx_write_u32((0x60F5 * 4 ) | phy_addr,0x1561);
	phx_write_u32((0x60F8 * 4 ) | phy_addr,0x1561);
	phx_write_u32((0x60FA * 4 ) | phy_addr,0x00B0);
	phx_write_u32((0x60FB * 4 ) | phy_addr,0x00A6);
	phx_write_u32((0x60FC * 4 ) | phy_addr,0x01B6);
	phx_write_u32((0x60FF * 4 ) | phy_addr,0x0333);
	phx_write_u32((0x6100 * 4 ) | phy_addr,0x0788);
	phx_write_u32((0x6101 * 4 ) | phy_addr,0x0777);
	phx_write_u32((0x6102 * 4 ) | phy_addr,0x0BCC);
	phx_write_u32((0x6103 * 4 ) | phy_addr,0x0888);
	phx_write_u32((0x6104 * 4 ) | phy_addr,0x0DFF);
	phx_write_u32((0x6105 * 4 ) | phy_addr,0x0888);
	phx_write_u32((0x6106 * 4 ) | phy_addr,0x0CDD);
	phx_write_u32((0x6107 * 4 ) | phy_addr,0x0888);
	phx_write_u32((0x6108 * 4 ) | phy_addr,0x0CDD);
	phx_write_u32((0x6109 * 4 ) | phy_addr,0x0888);
	phx_write_u32((0x610A * 4 ) | phy_addr,0x0CDD);
	phx_write_u32((0x610B * 4 ) | phy_addr,0x0599);
	phx_write_u32((0x610C * 4 ) | phy_addr,0x0DEE);
	phx_write_u32((0x610D * 4 ) | phy_addr,0x0084);
	phx_write_u32((0x610E * 4 ) | phy_addr,0x0399);
	phx_write_u32((0x610F * 4 ) | phy_addr,0x0DEE);
	phx_write_u32((0x6111 * 4 ) | phy_addr,0x28FF);
	phx_write_u32((0x6115 * 4 ) | phy_addr,0x0000);
	phx_write_u32((0x6118 * 4 ) | phy_addr,0x0000);
	phx_write_u32((0x611A * 4 ) | phy_addr,0x0100);
	phx_write_u32((0x611B * 4 ) | phy_addr,0x0080);
	phx_write_u32((0x611C * 4 ) | phy_addr,0x0024);
	phx_write_u32((0x611D * 4 ) | phy_addr,0x0024);
	phx_write_u32((0x611E * 4 ) | phy_addr,0x01FF);
	phx_write_u32((0x6121 * 4 ) | phy_addr,0x001F);
	phx_write_u32((0x6122 * 4 ) | phy_addr,0x1CE7);
	phx_write_u32((0x6123 * 4 ) | phy_addr,0x1D67);
	phx_write_u32((0x6125 * 4 ) | phy_addr,0x001F);
	phx_write_u32((0x6126 * 4 ) | phy_addr,0x4000);
	phx_write_u32((0x6148 * 4 ) | phy_addr,0x2011);
	phx_write_u32((0x6151 * 4 ) | phy_addr,0x2008);
	phx_write_u32((0x6155 * 4 ) | phy_addr,0x003F);
	phx_write_u32((0x6158 * 4 ) | phy_addr,0x2E21);
	phx_write_u32((0x6159 * 4 ) | phy_addr,0x2121);
	phx_write_u32((0x615C * 4 ) | phy_addr,0x5882);
	phx_write_u32((0x6161 * 4 ) | phy_addr,0x0023);
	phx_write_u32((0x61B0 * 4 ) | phy_addr,0x559E);
	phx_write_u32((0x61D8 * 4 ) | phy_addr,0x0019);
	phx_write_u32((0x61D9 * 4 ) | phy_addr,0x7FFF);
	phx_write_u32((0x61DA * 4 ) | phy_addr,0x0028);
	phx_write_u32((0x61DB * 4 ) | phy_addr,0x0028);
	phx_write_u32((0x61DC * 4 ) | phy_addr,0x0005);
	phx_write_u32((0x61DD * 4 ) | phy_addr,0x000F);
	phx_write_u32((0x61DE * 4 ) | phy_addr,0x7FC3);
	phx_write_u32((0x61DF * 4 ) | phy_addr,0x7F81);
	phx_write_u32((0x61E0 * 4 ) | phy_addr,0x7FC3);
	phx_write_u32((0x61E1 * 4 ) | phy_addr,0x7FC3);
	phx_write_u32((0x61E2 * 4 ) | phy_addr,0x7F81);
	phx_write_u32((0x61E3 * 4 ) | phy_addr,0x7FF1);
	phx_write_u32((0x61E4 * 4 ) | phy_addr,0x0000);
	phx_write_u32((0x61E5 * 4 ) | phy_addr,0x1000);
	phx_write_u32((0x61E6 * 4 ) | phy_addr,0x1092);
	phx_write_u32((0x61E7 * 4 ) | phy_addr,0x1092);
	phx_write_u32((0x61E8 * 4 ) | phy_addr,0x0000);
	phx_write_u32((0x61F3 * 4 ) | phy_addr,0x392B);
	phx_write_u32((0x60E8 * 4 ) | phy_addr,0x002D);
	phx_write_u32((0x60E9 * 4 ) | phy_addr,0x1B24);
	phx_write_u32((0x60E8 * 4 ) | phy_addr,0x002E);
	phx_write_u32((0x60E9 * 4 ) | phy_addr,0x1B25);
	phx_write_u32((0x60E8 * 4 ) | phy_addr,0x002F);
	phx_write_u32((0x60E9 * 4 ) | phy_addr,0x1B26);
	phx_write_u32((0x60E8 * 4 ) | phy_addr,0x0030);
	phx_write_u32((0x60E9 * 4 ) | phy_addr,0x1B27);
	phx_write_u32((0x60E8 * 4 ) | phy_addr,0x0031);
	phx_write_u32((0x60E9 * 4 ) | phy_addr,0x1B28);
	phx_write_u32((0x60E8 * 4 ) | phy_addr,0x0032);
	phx_write_u32((0x60E9 * 4 ) | phy_addr,0x1B29);
	phx_write_u32((0x60E8 * 4 ) | phy_addr,0x0033);
	phx_write_u32((0x60E9 * 4 ) | phy_addr,0x1B29);
	phx_write_u32((0x60E8 * 4 ) | phy_addr,0x0034);
	phx_write_u32((0x60E9 * 4 ) | phy_addr,0x1B2A);
	phx_write_u32((0x60E8 * 4 ) | phy_addr,0x0035);
	phx_write_u32((0x60E9 * 4 ) | phy_addr,0x1B2C);
	phx_write_u32((0x60E8 * 4 ) | phy_addr,0x0036);
	phx_write_u32((0x60E9 * 4 ) | phy_addr,0x1B2D);
	phx_write_u32((0x60E8 * 4 ) | phy_addr,0x0037);
	phx_write_u32((0x60E9 * 4 ) | phy_addr,0x1B2E);
	phx_write_u32((0x60E8 * 4 ) | phy_addr,0x0038);
	phx_write_u32((0x60E9 * 4 ) | phy_addr,0x1B2F);
	phx_write_u32((0x60E8 * 4 ) | phy_addr,0x0039);
	phx_write_u32((0x60E9 * 4 ) | phy_addr,0x1B30);
	phx_write_u32((0x60E8 * 4 ) | phy_addr,0x003A);
	phx_write_u32((0x60E9 * 4 ) | phy_addr,0x1B31);
	phx_write_u32((0x60E8 * 4 ) | phy_addr,0x003B);
	phx_write_u32((0x60E9 * 4 ) | phy_addr,0x1B32);
	phx_write_u32((0x60E8 * 4 ) | phy_addr,0x003C);
	phx_write_u32((0x60E9 * 4 ) | phy_addr,0x1B33);
	phx_write_u32((0x60E8 * 4 ) | phy_addr,0x003D);
	phx_write_u32((0x60E9 * 4 ) | phy_addr,0x1B34);
	phx_write_u32((0x60E8 * 4 ) | phy_addr,0x003E);
	phx_write_u32((0x60E9 * 4 ) | phy_addr,0x1B35);
	phx_write_u32((0x60E8 * 4 ) | phy_addr,0x003F);
	phx_write_u32((0x60E9 * 4 ) | phy_addr,0x1B36);

#endif

	phx_write_u32((0xD013 * 4 ) | phy_addr,0x1111);
	phx_write_u32((0xD113 * 4 ) | phy_addr,0x1111);

/*
	int j;
	for(j = 0; j < 2; j++)
	{
		phx_write_u32((0xD013 * 4 ) | phy_addr  |  (j << 0x10),0x1111);
	}
*/

//    phx_write_u32(0x0 | phy_wrapper_addr,0x111111);//apb,link0-3,phy


	debug_print("set_phy_chip_all,end.\n\r");


}

void lib_pciex4_cfg_x4(int sel,u64 base_addr,int dut_is_rc)//0:ep,1:rc;
{  
	printk("pciex4 link cfg x4.base_addr=%x\n",base_addr);

    //PCIE link CFG x4:
    phx_write_u32(0x8 | base_addr,0x102000);//0x108000,0x102000
   
    int rdata;
    phx_write_u32(base_addr +0x14,0x0);
    phx_write_u32(base_addr +0x20,0x0);
    phx_write_u32(base_addr +0x2c,0x0);
    phx_write_u32(base_addr +0x38,0x0);

#if 0//3080 old
    phx_write_u32(base_addr + 0x0,0x11);
#else//3080b
    phx_write_u32(base_addr + 0x0,0x111111);//apb,link0-3,phy
//    phx_write_u32(base_addr + 0x0,0x100011);//apb,link0,phy
#endif            

	if(sel == PCIE_SRIO_0_LINK0_EN){
		rdata = phx_read_u32(PCIE_SRIO_REGION0_LINK0_MISC_BASE_ADDR+PCIE_X4_CONFIG0);
		phx_write_u32(PCIE_SRIO_REGION0_LINK0_MISC_BASE_ADDR + PCIE_X4_CONFIG0,((rdata & (~(0x1<<2))) + ((dut_is_rc & 0x1)<<2)));

		phx_write_u32(PCIE_SRIO_REGION0_LINK0_MISC_BASE_ADDR + PCIE_X4_CONFIG_INPUT_OUTPUT_0,0x3);  
	}else{
		rdata = phx_read_u32(PCIE_SRIO_REGION1_LINK0_MISC_BASE_ADDR+PCIE_X4_CONFIG0);
		phx_write_u32(PCIE_SRIO_REGION1_LINK0_MISC_BASE_ADDR + PCIE_X4_CONFIG0,((rdata & (~(0x1<<2))) + ((dut_is_rc & 0x1)<<2)));

		phx_write_u32(PCIE_SRIO_REGION1_LINK0_MISC_BASE_ADDR + PCIE_X4_CONFIG_INPUT_OUTPUT_0,0x3);  
	}
}


void lib_pcie_training_enable(u32 pcie_wrapper_addr)
{
	u32 apb_paddr;
	u32 apb_pwdata;

	apb_paddr = 0x200 * 4;
	apb_pwdata = 0x3;
	phx_write_u32(pcie_wrapper_addr+apb_paddr, apb_pwdata);
}


void lib_wait_clock_stable(u32 pcie_wrapper_addr)
{
	u32 apb_paddr;
	u32 apb_prdata;
	u8 clock_state;
	u32 wait_count = 0;//fudy.
	int j;

  	debug_print("wait_clock_stable...\n\r");
    
	do {
		apb_paddr = 0x0;
		apb_prdata = phx_read_u32(pcie_wrapper_addr+apb_paddr);
		clock_state = ((apb_prdata>>16) & 0x1);
		//debug_print("addr: 0x%x, data: 0x%x, 0x%x", apb_paddr, apb_prdata, link_state);

		//fudy.wait a moment:
		wait(10000);
		wait_count++;
//		if(wait_count > 20) break;
		
	} while (clock_state != 0x0);

  	debug_print("clock stable.wait_count=%d.\n\r",wait_count);

	apb_paddr = 0x0;

	for (j = 0; j < 50; j=j+1) {
		apb_prdata = phx_read_u32(pcie_wrapper_addr+apb_paddr);
	}
}

/******************************************************************************/

#define PCIE_WAIT_LINK_COUNT	0x200000

void lib_wait_link(u32 pcie_wrapper_addr)
{
	u32 apb_paddr;
	u32 apb_prdata;
	u8 link_state;
	u32 wait_count = 0;//fudy.
	u32 config_input_output = pcie_wrapper_addr + 0x800;
	u32 val = 0;
	u32 speed,width;

	do {
//		link_show(PCIE_0_BASE,PCIE_WRAP_0_BASE);//tmp
//		link_show(PCIE_1_BASE,PCIE_WRAP_1_BASE);//tmp
		apb_paddr = 0x200 * 4;
		apb_prdata = phx_read_u32(pcie_wrapper_addr+apb_paddr);
		link_state = ((apb_prdata>>7) & 0x3);
		//debug_print("addr: 0x%x, data: 0x%x, 0x%x \n\r", apb_paddr, apb_prdata, link_state);
		width = ((apb_prdata>>5) & 0x3);
		speed = ((apb_prdata>>3) & 0x3);

		wait(10000);//fudy.
		wait_count++;
		if(wait_count > PCIE_WAIT_LINK_COUNT) break;//fudy.
	} while ( (link_state != 0x3) || (width != 0x2) || (speed != 0x2) ) ;

	debug_print("i=%d \n\r",wait_count);//vcs:i=618

	val = phx_read_u32(config_input_output);//get neg link status

	if(link_state != 0x3){
		if(pcie_wrapper_addr == PCIE_WRAP_0_BASE){debug_print("link0 ERROR! \n\r\n\r");}
		if(pcie_wrapper_addr == PCIE_WRAP_1_BASE){debug_print("link1 ERROR! \n\r\n\r");}
		if(pcie_wrapper_addr == PCIE_SRIO_REGION0_LINK1_MISC_BASE_ADDR){debug_print("link0-1 ERROR! \n\r\n\r");}
		if(pcie_wrapper_addr == PCIE_SRIO_REGION1_LINK1_MISC_BASE_ADDR){debug_print("link1-1 ERROR! \n\r\n\r");}		
	}
	else{
		if(pcie_wrapper_addr == PCIE_WRAP_0_BASE){debug_print("link0 up!  \n\rwidth=%x(0=x1,1=x2,2=x4,3=x8) \n\rspeed=%x(0=2.5G,1=5G,2=8G)\n\r\n\r",(val>>5)&0x3,(val>>3)&0x3);}
		if(pcie_wrapper_addr == PCIE_WRAP_1_BASE){debug_print("link1 up!  \n\rwidth=%x(0=x1,1=x2,2=x4,3=x8) \n\rspeed=%x(0=2.5G,1=5G,2=8G)\n\r\n\r",(val>>5)&0x3,(val>>3)&0x3);}
		if(pcie_wrapper_addr == PCIE_SRIO_REGION0_LINK1_MISC_BASE_ADDR){debug_print("link0-1 up!  \n\rwidth=%x(0=x1,1=x2,2=x4,3=x8) \n\rspeed=%x(0=2.5G,1=5G,2=8G)\n\r\n\r",(val>>5)&0x3,(val>>3)&0x3);}
		if(pcie_wrapper_addr == PCIE_SRIO_REGION1_LINK1_MISC_BASE_ADDR){debug_print("link1-1 up!  \n\rwidth=%x(0=x1,1=x2,2=x4,3=x8) \n\rspeed=%x(0=2.5G,1=5G,2=8G)\n\r\n\r",(val>>5)&0x3,(val>>3)&0x3);}
	}
}



void pciex4_3080_init(u32 mode)
{
	printk("\n== pciex4_1 rc == \n");

	/*unlock, release reset*/
	lib_pcie_sysctrl_en_rc1_3020b();  

    wait(0x100000);

    printk("<**DEBUG**> [%s():_%d_]\n", __func__, __LINE__);
	//pciex4_0 --connect to--- pciex4_1

//    lib_phy_init_nospeedup(PHY_0_BASE,PHY_WRAP_0_BASE);
    lib_phy_init_nospeedup(PHY_1_BASE,PHY_WRAP_1_BASE);
    
    
    printk("<**DEBUG**> [%s():_%d_]\n", __func__, __LINE__);
//    lib_pciex4_cfg_x4(PCIE_SRIO_0_LINK0_EN,PCIE_SRIO_REGION0_PHY_MISC_BASE_ADDR,mode);
    lib_pciex4_cfg_x4(PCIE_SRIO_1_LINK0_EN,PCIE_SRIO_REGION1_PHY_MISC_BASE_ADDR,mode);

    
    printk("<**DEBUG**> [%s():_%d_]\n", __func__, __LINE__);
//    lib_pcie_training_enable(PCIE_WRAP_0_BASE);
	lib_pcie_training_enable(PCIE_WRAP_1_BASE);

	
	printk("<**DEBUG**> [%s():_%d_]\n", __func__, __LINE__);
//	lib_wait_clock_stable(PCIE_WRAP_0_BASE);
	lib_wait_clock_stable(PCIE_WRAP_1_BASE);

#if 0
    wait(0x100000);
	printk("\n------ link test. ------\n");

//	lib_wait_link(PCIE_WRAP_0_BASE);
	lib_wait_link(PCIE_WRAP_1_BASE);
#endif
}

