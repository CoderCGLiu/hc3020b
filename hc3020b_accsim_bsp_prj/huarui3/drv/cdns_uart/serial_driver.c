/******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件实现了串口模板
 * 修改：
 * 		2012-04-17，唐立三，建立代码模板
 * 		2011-11-18，唐立三，修改ISR方式，将ISR分为两个阶段实现
 * 		2011-10-22，唐立三，修改串口驱动层接口名称
 * 		2011-09-28，王志宏，修改poll读写接口名称
 * 		2011-09-26，唐立三，增加poll方式访问串口，重写ISR
 * 		2011-09-16，唐立三，根据自测结果，修改ioctl()接口、tx_char()接口和rx_char()
 * 							接口ioctl_define.h和regs.h文件，增加set_lcr()接口
 * 		2011-08-30，唐立三，统一寄存器定义
 * 		2011-08-27，唐立三，建立
 */
#include <pthread.h>
#include <irq.h>
#include <string.h>
#include<stdio.h>
#include <reworks/printk.h>
#include <devnum.h>
#include "serial_driver.h"		/* 串口驱动头文件 */
#include <taskLib.h>
#include <spinLocklib.h>
#include <clock.h>

#include "uart.h"
#include "uart_driver.h"

/*
 * 本地变量声明
 * */
spinlockIsr_t * puartSpinLock;/*葛文博 20230818 add:: 修复串口波特率设置功能*/

extern char *std_input_device_name;
extern char *std_output_device_name;

extern int data_ldisc_init(void);
extern int line_ldisc_init(void);
/* uart0 配置参数声明*/
uart_init_def uart0_cfg = {
		.baudrate = UART_BAUD_115200,
		.word = UART_WORD_8BIT,
		.stop = UART_STOP_1BIT,
		.parity = UART_PARITY_NONE,
		.transmit_enable = UART_TRANSMIT_ENABLE,
		.receive_enable = UART_RECEIVE_ENABLE,
		.fifo_access = UART_FIFO_1BYTE_MODE,
		.irda_mode = UART_DEFAULT_MODE,
		.channel_mode = UART_NORMAL_CHANNEL,
		.clock_select = UART_SEL_APB_CLOCK,
		.clock_prescalar8 = UART_CLOCK_PRESCALAR_DIS
};

/* Rx Trigger level */
static int rx_trigger_level = 56;
/* Rx Timeout */
static int rx_timeout = 10;


/* 串口参数定义 
 * l2F一个串口可用*/
struct serial_params serial_param_define[SERIAL_NUMBER] =
{
		/*规则库名称  	中断名称    	 注册的设备路径    	寄存器基       		寄存器宽度	中断向量   	优先级 	基准波特率（时钟）	波特率	IER初始值 */// 寄存器宽度1->4
		{"line_ldisc", 	"serial0", 	"/dev/serial0", UART0_BASE_ADDR,4, 			INT_UART0, 	3, 		33333333, 			115200,	UART_IDR_MASK	} /*CPU串口0*/
	   ,{"line_ldisc", 	"serial1", 	"/dev/serial1", UART1_BASE_ADDR,4, 			INT_UART1, 	3, 		33333333, 			115200, UART_IDR_MASK	} /*CPU串口1*/
#ifdef _HC3020B_	   
		,{"line_ldisc", 	"serial2", 	"/dev/serial2", UART2_BASE_ADDR,4, 		INT_UART2, 	3, 		33333333, 			115200, UART_IDR_MASK	} /*CPU串口2*/
		,{"line_ldisc", 	"serial3", 	"/dev/serial3", UART3_BASE_ADDR,4, 		INT_UART3, 	3, 		33333333, 			115200, UART_IDR_MASK	} /*CPU串口3*/
		,{"line_ldisc", 	"serial4", 	"/dev/serial4", UART4_BASE_ADDR,4, 		INT_UART4, 	3, 		33333333, 			115200, UART_IDR_MASK	} /*CPU串口4*/
		,{"line_ldisc", 	"serial5", 	"/dev/serial5", UART5_BASE_ADDR,4, 		INT_UART5, 	3, 		33333333, 			115200, UART_IDR_MASK	} /*CPU串口5*/
		,{"line_ldisc", 	"serial6", 	"/dev/serial6", UART6_BASE_ADDR,4, 		INT_UART6, 	3, 		33333333, 			115200, UART_IDR_MASK	} /*CPU串口6*/
		,{"line_ldisc", 	"serial7", 	"/dev/serial7", UART7_BASE_ADDR,4, 		INT_UART7, 	3, 		33333333, 			115200, UART_IDR_MASK	} /*CPU串口7*/
#endif
};


/* 串口描述结构全局变量 */
struct serial_device_struct_ex global_serial_struct[SERIAL_NUMBER];


#if defined(__HUARUI2__)
#include "tool/hr2SysTools.h"
#include <vxWorks.h>
//#define INCLUDE_SIO_SHARED
#ifdef INCLUDE_SIO_SHARED
static STATUS sysSioTxReq(unsigned int procInd)
{
	switch(procInd)
	{
	case 0:
		*(unsigned int *)UART_SWITCH_TX_REG = 1;
		break;
	case 1:
		*(unsigned int *)UART_SWITCH_TX_REG = 2;
		break;
	case 2:
		*(unsigned int *)UART_SWITCH_TX_REG = 4;
		break;
	case 3:
		*(unsigned int *)UART_SWITCH_TX_REG = 8;
		break;
	default:
		*(unsigned int *)UART_SWITCH_TX_REG = 1;
		break;
	}
	return OK;
}

static STATUS sysSioTxGnt(unsigned int procInd)
{
//	if(*(unsigned int *)SIO_TX_ADDR == 0)
	if((*(unsigned int *)UART_SWITCH_TX_REG /*& 0xf*/)== 0)
	{
		return OK;
	}
	else
	{
		return ERROR;
	}
}
static STATUS sysSioTxRel(/*unsigned int procInd*/)
{
	*(unsigned int *)UART_SWITCH_TX_REG = 0;
	return OK;
}
#if 0
static STATUS sysSioRxReq(unsigned int procInd)
{

	switch(procInd)
	{
	case 0:
		*(unsigned int *)UART_SWITCH_RX_REG = 1;
		break;
	case 1:
		*(unsigned int *)UART_SWITCH_RX_REG = 2;
		break;
	case 2:
		*(unsigned int *)UART_SWITCH_RX_REG = 4;
		break;
	case 3:
		*(unsigned int *)UART_SWITCH_RX_REG = 8;
		break;
	default:
		*(unsigned int *)UART_SWITCH_RX_REG = 1;
		break;
	}
	return OK;
}

static STATUS sysSioRxGnt(unsigned int procInd)
{
	if(*(unsigned int *)ADDR_LP64(0xb8300108) == 0)
	{
		return OK;
	}
	else
	{
		return ERROR;
	}
}

static STATUS sysSioRxRel()
{
	*(unsigned int *)UART_SWITCH_RX_REG = 0;
	return OK;
}
#endif /*#if 0*/
#endif /*INCLUDE_SIO_SHARED*/
#endif /*__HUARUI2__*/

/******************************************************************************
 * 
 * 向串口设备寄存器写入数据
 * 
 * 输入：
 * 		driver：串口设备描述结构指针
 * 		offset：偏移
 * 		value：写入寄存器的值
 * 输出：
 * 		无
 * 返回：
 * 		寄存器值
 */
 /*葛文博 20230818 add:: 修复串口波特率设置功能*/
static inline void to_serial(struct serial_device_struct_ex *driver, 
							 int offset, 
							 int value)
{
	/* 内存基址加偏移，偏移为：寄存器偏移 + 寄存器字节宽度 */

	t_spinlockIsrTake(puartSpinLock);
	writeb(value, (unsigned long) driver->regs_base
				+(offset * driver->delta));
	rdGmacReg();
	t_spinlockIsrGive(puartSpinLock);
	return;
}

/******************************************************************************
 * 
 * 设置波特率接口；本程序为内部接口
 * 
 * 输入：
 * 		driver：
 * 输出：
 * 		无
 * 返回：
 * 		寄存器值
 */
#if 1	/*葛文博 20230818 add:: 修复串口波特率设置功能*/
static int set_baudrate(struct serial_device_struct_ex *driver,
						unsigned int baudrate)
{
	
//	driver->cfgs.baudrate = baudrate;
	driver->cfgs = uart0_cfg;
	driver->cfgs.baudrate = baudrate;
	
	printk("in set_baudrate baudrate = %d\n", driver->cfgs.baudrate);
	uart_init_set_test(driver->regs_base, &(driver->cfgs));
	driver->baudrate = driver->cfgs.baudrate;
	return 0;
}

#else
static int set_baudrate(struct serial_device_struct_ex *driver,
						unsigned int baudrate)
{
	int level;
	
	level = int_cpu_lock();	/* 锁中断 */
	
	/* 设置LCR，使得当前能够设置波特率 */
	to_serial(driver, IOS_UART_LCR, driver->regs.LCR | IOS_UART_LCR_DLAB);
	
	/* 设置波特率低、高分频系数 */
	
	to_serial(driver, IOS_UART_DLL, (driver->base_baudrate / (16 * baudrate)));
	to_serial(driver, IOS_UART_DLM, (driver->base_baudrate / (16 * baudrate)) >> 8);
	
	driver->baudrate = baudrate;	/* 保存当前的波特率 */
	
	/* 还原LCR */
	to_serial(driver, IOS_UART_LCR, driver->regs.LCR);	/* 设置波特率非能 */
	
	int_cpu_unlock(level);	/* 开中断 */
	
	return 0;
}
#endif
/******************************************************************************
 * 
 * 串口中断服务程序ISR
 * 
 * 		本程序在串口初始化代码中调用，通过int_handle_install接口实现ISR与中断的挂接。
 * 本程序实现了Rx和Tx两类中断的处理，尚未实现Modem中断和错误中断的处理。
 * 
 * 输入：
 * 		driver：串口描述结构指针
 * 输出：
 * 		无
 * 返回：
 * 		无
 * 修改：
 * 		2011-11-18，将本程序作为ISR2实现，由ISR1触发
 * 		2011-09-26，重写ISR，原ISR效率低且shell重定向后异常
 * 		2011-08-30，规范代码，统计寄存器定义
 * 		2011-08-25，建立
 */

extern int rx_count;
extern int tx_count;
//extern char test_buffer[256];
//extern int  g_test_buffer_idx;
void serial_int_isr1(struct serial_device_struct_ex *driver)
{
    unsigned char c;
    unsigned int isrstatus, numbytes, imrstatus, ttysize;
//    int_disable_pic(driver->intvec);

//    printk("[%s][%d]\n",__FUNCTION__,__LINE__);

	isrstatus = readl(driver->regs_base + CDNS_UART_ISR);
//    isrstatus = readl(driver->regs_base + CDNS_UART_SR);
	//writel(isrstatus, driver->regs_base + CDNS_UART_ISR);
//    writel(isrstatus, driver->regs_base + CDNS_UART_SR);

//	imrstatus = readl(driver->regs_base + CDNS_UART_IMR);

    if ((isrstatus & CDNS_UART_IXR_TXEMPTY)/*&& (imrstatus & CDNS_UART_IXR_TXEMPTY)*/)
    {

        ttysize = get_tty_wbuf_cnt(driver);
//        serial_putstr(__FUNCTION__);
//        serial_putdec(ttysize);
//        serial_putchar('\n');
        if( !ttysize )
        {
        	writel(CDNS_UART_IXR_TXEMPTY, driver->regs_base + CDNS_UART_IDR);
        	driver->statistic.idr++;
        }
        else
        {
        	numbytes = CDNS_UART_FIFO_SIZE;
        	 while (numbytes && (!(readl(driver->regs_base + CDNS_UART_SR)
					& CDNS_UART_SR_TXFULL))) {
				/* 读取字符 */
				if (-1 != read_char_from_tty((struct tty_driver *) driver, &c)) {
					writel(c, driver->regs_base + CDNS_UART_FIFO);/* 写至发送寄存器 */
					driver->statistic.tx++;
					//tx_count++;
				}
				numbytes--;
			}
        }
        isrstatus &= ~CDNS_UART_IXR_TXEMPTY;
    }


    u32 crstatus = readl(driver->regs_base + CDNS_UART_CR);
//    if ((isrstatus & CDNS_UART_IXR_RXTOUT) || (isrstatus & CDNS_UART_IXR_RXTRIG))
	if (isrstatus & CDNS_UART_IXR_RXMASK &&
	    ((crstatus & CDNS_UART_CR_RX_DIS) == 0x0))
    {

		/* Receive Timeout Interrupt */
        while ((readl(driver->regs_base + CDNS_UART_SR) &CDNS_UART_SR_RXEMPTY) != CDNS_UART_SR_RXEMPTY)
        {
        	u32 srstatus = readl(driver->regs_base + CDNS_UART_SR);
        	if (!(srstatus & CDNS_UART_SR_RXEMPTY))
        	{
        		c =  readl(driver->regs_base + CDNS_UART_FIFO);
//        		test_buffer[g_test_buffer_idx++] = c;
//        		writel(c, driver->regs_base + CDNS_UART_FIFO);
//        		serial_putchar('\r');
        		write_char_to_tty((struct tty_driver *)driver, c);
        		driver->statistic.rx++;
        		//rx_count++;
        	}

        }

//        u32 status = readl(driver->regs_base + CDNS_UART_IER);
//        writel((status|CDNS_UART_IXR_TXEMPTY), driver->regs_base + CDNS_UART_IER);
    }

//    writel(isrstatus, driver->regs_base + CDNS_UART_ISR);

//    int_enable_pic(driver->intvec);

    return;


}

/******************************************************************************
 * 
 * 打开串口设备
 *     该接口在用户调用I/O模块的open接口时调用，主要负责使能中断，即如果设备第一次打开则
 * 使能该设备对应的中断。
 * 
 * 输入：
 * 		driver：串口设备描述结构指针
 * 输出：
 * 
 * 返回：
 * 		成功返回0；失败返回-1
 * 修改：
 * 
 */
int serial_open(struct tty_driver *driver)
{
	struct serial_device_struct_ex *serial_driver;

//	printk("[%s][%d]\n",__FUNCTION__,__LINE__);

	if (NULL == driver)
	{
		return -1;
	}
	serial_driver = (struct serial_device_struct_ex *)driver;

	/* 如果计数为0，则说明第一次打开串口设备，因而需使能中断 */
	if (0 == serial_driver->count)
	{
		int_enable_pic(serial_driver->intvec);
	}
	//printk("XXXXXXX[%s][%d],0x%lx\n",__FUNCTION__,__LINE__,serial_driver->intvec);
	serial_driver->count++;

	return 0;
}

/* 等待发送完成 */
static void serial_wait_for_tx(struct serial_device_struct_ex *serial_driver)
{
	do {
		if (readl(serial_driver->regs_base + CDNS_UART_SR) &CDNS_UART_SR_TXEMPTY)
			break;
		else
			pthread_delay(1);
	}while(1);

}


/******************************************************************************
 * 
 * 关闭串口设备
 *     该接口在用户调用I/O模块的close接口时调用，要检查设备open计数，根据计数情况释
 * 放资源（尤其是非能中断）。
 * 
 * 输入：
 * 		driver：串口设备描述结构指针
 * 输出：
 * 
 * 返回：
 * 		成功返回0；失败返回-1
 * 修改：
 * 
 */
int serial_close(struct tty_driver *driver)
{
	u32 level;
	 unsigned int status;

	struct serial_device_struct_ex *serial_driver =
		(struct serial_device_struct_ex *)driver;

//	printk("[%s][%d]\n",__FUNCTION__,__LINE__);

	level = int_lock();

	serial_driver->count--;
	if (0 > serial_driver->count)
	{
		serial_driver->count = 0;
		int_unlock(level);
		return 0;
	}

	if (0 == serial_driver->count)
	{
		serial_wait_for_tx(serial_driver);
		int_disable_pic(serial_driver->intvec);

        /* Disable interrupts */
		status = readl(serial_driver->regs_base + CDNS_UART_IMR);
		writel(status, serial_driver->regs_base + CDNS_UART_IDR);
		writel(0xffffffff, serial_driver->regs_base + CDNS_UART_ISR);

        /* Disable the TX and RX */
//    	writel(CDNS_UART_CR_TX_DIS | CDNS_UART_CR_RX_DIS,
//    			serial_driver->regs_base + CDNS_UART_CR);
    	writel(CDNS_UART_CR_TX_DIS ,
    			serial_driver->regs_base + CDNS_UART_CR);
	}

	int_unlock(level);

	return 0;
}



/******************************************************************************
 * 
 * 启动一次发送接口
 * 
 * 		本程序负责启动一次发送，本程序在上层TTY规则库中调用。当应用通过IO的write接口
 * 经由规则库向串口写数据时，规则库调用本接口启动一次发送。本程序负责通过Tx中断使能。
 * 实现ISR的调用。
 * 
 * 输入：
 * 		driver：串口设备描述结构指针
 * 输出：
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 * 修改：
 * 		2011-08-30，规范代码，统计寄存器定义
 * 		2011-08-25，建立
 */
int serial_start_tx(struct tty_driver *driver)
{

	struct serial_device_struct_ex *serial_driver =
		(struct serial_device_struct_ex *)driver;

//	printk("[%s][%d]\n",__FUNCTION__,__LINE__);

    unsigned int status, numbytes = CDNS_UART_FIFO_SIZE;

    /* Set the TX enable bit and clear the TX disable bit to enable the
     * transmitter.
     */
	status = readl(serial_driver->regs_base + CDNS_UART_CR);
	status &= ~CDNS_UART_CR_TX_DIS;
	status |= CDNS_UART_CR_TX_EN;
	writel(status, serial_driver->regs_base + CDNS_UART_CR);

    char c;


#if 0
    while (numbytes-- &&
    		(!(readl(serial_driver->regs_base + CDNS_UART_SR) & CDNS_UART_SR_TXFULL)))
    {

		/* 读取字符 */
		if (-1 != read_char_from_tty((struct tty_driver *) driver, &c)) {
			writel( c, serial_driver->regs_base + CDNS_UART_FIFO);/* 写至发送寄存器 */
			//			*lsr_status = from_serial(driver, IOS_UART_LSR);	/* 再次读LSR */
			serial_driver->statistic.tx++;
		} else {
//			driver->regs.IER &= ~IOS_UART_IER_THEN; /* 屏蔽Tx中断位，保存至备份 */
//			to_serial(driver, IOS_UART_IER, driver->regs.IER); /* 设置IER寄存器 */
			writel(CDNS_UART_IXR_TXEMPTY, serial_driver->regs_base + CDNS_UART_IDR);
			break; /* 退出while循环 */
		}

    }
#endif

    status = readl(serial_driver->regs_base + CDNS_UART_ISR);
    writel((status|CDNS_UART_IXR_TXEMPTY), serial_driver->regs_base + CDNS_UART_ISR);

    /* Enable the TX Empty interrupt */
    writel(0, serial_driver->regs_base + CDNS_UART_IDR);
    status = readl(serial_driver->regs_base + CDNS_UART_IER);
    writel((status|CDNS_UART_IXR_TXEMPTY), serial_driver->regs_base + CDNS_UART_IER);
    serial_driver->statistic.ier++;
    writel('\0', serial_driver->regs_base + CDNS_UART_FIFO);
	return 0;
}


/******************************************************************************
 * 
 * 停止发送接口
 * 
 * 		本程序负责停止发送，本程序在上一层规则库中调用。本程序调用的时机是上层规则库
 * 中无等待传输的字符。本程序负责通过Tx非能停止字符发送。
 * 		如果应用程序配置了自动发送控制选项IOS_LDISC_TX_AUTO，那么规则库在无待传输的
 * 字符时自动调用本接口，当然，如果没有配置该选项，那么可以在ISR中停止发送。如果没有
 * 定义或实现本程序，那么自动发送控制选项不起作用，无论是否配置该选项。
 * 
 * 输入：
 * 		driver：串口设备描述结构指针
 * 输出：
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 * 修改：
 * 		2011-08-30，规范代码，统计寄存器定义
 * 		2011-08-25，建立
 */
int serial_stop_tx(struct tty_driver *driver)
{
    unsigned int regval;

	struct serial_device_struct_ex *serial_driver =
		(struct serial_device_struct_ex *)driver;

//	printk("[%s][%d]\n",__FUNCTION__,__LINE__);

	regval = readl(serial_driver->regs_base + CDNS_UART_CR);
	regval |= CDNS_UART_CR_TX_DIS;
	/* Disable the transmitter */
	writel(regval, serial_driver->regs_base + CDNS_UART_CR);

	return 0;
}


/******************************************************************************
 * 
 * 启动接收接口
 * 
 * 		本程序负责启动接收，本程序在上一层规则库中调用。
 * 
 * 输入：
 * 		driver：串口设备描述结构指针
 * 输出：
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 * 修改：
 * 		2011-08-30，规范代码，统计寄存器定义
 * 		2011-08-25，建立
 * 备注：
 * 		本程序尚未实现调用。
 */
int serial_start_rx(struct tty_driver *driver)
{
	int status;

	struct serial_device_struct_ex *serial_driver =
		(struct serial_device_struct_ex *)driver;

	printk("%s:%d\n", __FUNCTION__, __LINE__);

	status = readl(serial_driver->regs_base + CDNS_UART_CR);
	status &= ~CDNS_UART_CR_RX_DIS;
	status |= CDNS_UART_CR_RX_EN;
	writel(status, serial_driver->regs_base + CDNS_UART_CR);

	return 0;
}


/******************************************************************************
 * 
 * 停止接收接口
 * 
 * 		本程序负责停止接收，本程序在上一层规则库中调用。
 * 
 * 输入：
 * 		driver：串口设备描述结构指针
 * 输出：
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 * 修改：
 * 		2011-08-30，规范代码，统计寄存器定义
 * 		2011-08-25，建立
 * 备注：
 * 		本程序尚未实现调用。
 */
int serial_stop_rx(struct tty_driver *driver)
{
	unsigned int regval;

	printk("[%s][%d]\n",__FUNCTION__,__LINE__);

	struct serial_device_struct_ex *serial_driver =
		(struct serial_device_struct_ex *)driver;

	/* Disable the receiver */
	regval = readl(serial_driver->regs_base + CDNS_UART_CR);
	regval |= CDNS_UART_CR_RX_DIS;
	writel(regval, serial_driver->regs_base + CDNS_UART_CR);

	return 0;
}

/******************************************************************************
 * 
 * 发送字符接口
 * 
 * 		本程序通过直接写TRH寄存器向串口输出一个字符。
 * 
 * 输入：
 * 		driver：串口设备描述结构指针
 * 		c：待发送的字符
 * 输出：
 * 返回：
 * 		成功返回0；失败返回-1
 * 修改：
 * 		2011-08-30，规范代码，统计寄存器定义
 * 		2011-08-25，建立
 */
int serial_tx_char(struct tty_driver *driver, char c)
{
	struct serial_device_struct_ex *serial_driver =
		(struct serial_device_struct_ex *)driver;
//	printk("[%s][%d]\n",__FUNCTION__,__LINE__);

	uart_write_byte(serial_driver->regs_base, (unsigned char)c);

	return 0;
}

void uart_write_test(char c)
{
	UART0->FIFO = c;
}


/******************************************************************************
 * 
 * 接收字符接口
 * 
 * 		本程序通过直接读RDH寄存器向调用者输出一个字符。
 * 
 * 输入：
 * 		driver：串口设备描述结构指针
 * 输出：
 * 		c：存放接收到字符的字节类型指针
 * 返回：
 * 		成功返回0；失败返回-1
 * 修改：
 * 		2011-08-30，规范代码，统计寄存器定义
 * 		2011-08-25，建立
 */
int serial_rx_char(struct tty_driver *driver, char *c)
{
	struct serial_device_struct_ex *serial_driver =
		(struct serial_device_struct_ex *)driver;

//	printk("%s:%d\n", __FUNCTION__, __LINE__);

	if(uart_read_byte(serial_driver->regs_base, (u8 *)c)==0)
		return -1;

	return 0;
}


/******************************************************************************
 * 
 * 串口设备控制接口
 * 
 * 输入：
 * 		driver：串口设备描述结构指针
 * 		u32：控制命令
 * 		arg：命令参数
 * 输出：
 * 返回：
 * 		成功返回0；失败返回-1
 * 修改：
 * 		2011-08-30，规范代码，统计寄存器定义
 * 		2011-08-25，建立
 */
int serial_ioctl(struct tty_driver *driver, u32 cmd, void *arg)
{
	struct serial_device_struct_ex *serial_driver =
		(struct serial_device_struct_ex *)driver;

	int status = OS_OK;
	int tmp;	/* 临时变量 */

//	printk("[%s][%d]cmd[0x%x]\n",__FUNCTION__,__LINE__,cmd);

	switch (cmd)
	{
	case TTY_SET_BAUDRATE :	/* 设置波特率 */
	{
//		printk("[%s][%d]\n",__FUNCTION__,__LINE__);
		tmp = (int)arg;
		/* 对波特率进行检查 */
		if ((50 > tmp) || (115200 < tmp))
		{
			return OS_ERROR;
		}
		status = set_baudrate(serial_driver, tmp);
		break;
	}
	case TTY_GET_BAUDRATE :	/* 获取波特率 */
	{
		if(!arg)
		{
			return -1;
		}
		*(int *)arg = serial_driver->baudrate;
		break;
	}

	case TTY_GET_MODE :		/* 获取访问模式 */
	{
//		printk("[%s][%d]\n",__FUNCTION__,__LINE__);
		if (((uart_def*)serial_driver->regs_base)->IMR & UART_CISR_MASK)
		{
			*(int *)arg = INT_MODE;
			return 0;
		}
		else
		{
			*(int *)arg = POLL_MODE;
			return 0;
		}

		break;
	}

	case TTY_SET_MODE :		/* 设置访问模式 */
	{
//		printk("[%s][%d]\n",__FUNCTION__,__LINE__);
		if (INT_MODE == (int)arg)
		{
			uart_enable_pics(serial_driver->regs_base, UART_CISR_MASK);
			return 0;
		}
		else if (POLL_MODE == (int)arg)
		{
			uart_disable_pics(serial_driver->regs_base, UART_CISR_MASK);
			return 0;
		}
		else
		{
			return -1;
		}
		break;
	}

	default :
	{
//		printk("[%s][%d]\n",__FUNCTION__,__LINE__);
		status = -1;
		break;
	}
}

return status;
}

/*******************************************************************************
 * 
 * 以轮询的方式从串口设备发送数据
 * 
 * 输入：
 * 		serial_num：访问的串口号
 * 		count：待读取的数据大小
 * 输出：
 * 		buffer：存放待读取数据的缓冲区
 * 返回：
 * 		成功返回读取的字符个数；失败返回-1
 */
int tx_in_poll(int serial_num, const char *buffer, int count)
{
	int writed = 0;		/* 已写入的字符计数 */

	/* 参数检查 */
	if ((0 > serial_num) || (SERIAL_NUMBER <= serial_num))
	{
		return -1;
	}

	/* Tx数据 */
	while (count)
	{
		while (-1 == serial_tx_char(
				(struct tty_driver *)&global_serial_struct[serial_num],
				buffer[writed]))
		{
			;
		}
		count--;
		writed++;
	}

	(0 == writed) ? ({return -1;}) : ({return writed;});
}


/*******************************************************************************
 * 
 * 以轮询的方式从串口设备读取数据
 * 
 * 输入：
 * 		serial_num：访问的串口号
 * 		count：待读取的数据大小
 * 输出：
 * 		buffer：存放待读取数据的缓冲区
 * 返回：
 * 		成功返回读取的字符个数；失败返回-1
 */
int rx_in_poll(int serial_num, char *buffer, int count)
{
	int readed = 0;		/* 已读取的字符个数 */

	/* 参数检查 */
	if ((0 > serial_num) || (SERIAL_NUMBER <= serial_num) ||
		(NULL == buffer))
	{
		return -1;
	}

	/* Rx数据 */
	while (count)
	{
		if (-1 == serial_rx_char(
				(struct tty_driver *)&global_serial_struct[serial_num],
				&buffer[readed]))
		{
			if (readed)
			{
				goto result;
			}
			else
			{
				continue;
			}
		}
		count--;
		readed++;
	}

result:
	(0 == readed) ? ({return -1;}) : ({return readed;});
}



/******************************************************************************
 * 
 * 初始化串口硬件设备
 * 
 * 输入：
 * 		driver：串口设备描述结构指针
 * 输出：
 * 返回：
 * 		成功返回0；失败返回-1
 * 修改：
 * 		2011-08-30，规范代码，统计寄存器定义
 * 		2011-08-25，建立
 */
int serial_hw_init(struct serial_device_struct_ex *driver)
{
	u32 level;
	int status;

	level = int_lock();

	/* Disable the TX and RX */
	writel(CDNS_UART_CR_TX_DIS | CDNS_UART_CR_RX_DIS,
			driver->regs_base + CDNS_UART_CR);


	/* Set the Control Register with TX/RX Enable, TX/RX Reset,
	 * no break chars.
	 */
	writel(CDNS_UART_CR_TXRST | CDNS_UART_CR_RXRST,
			driver->regs_base + CDNS_UART_CR);

	while (readl(driver->regs_base + CDNS_UART_CR) &
		(CDNS_UART_CR_TXRST | CDNS_UART_CR_RXRST))
		pthread_delay(1);

	/* Clear the RX disable and TX disable bits and then set the TX enable
	 * bit and RX enable bit to enable the transmitter and receiver.
	 */
	status = readl(driver->regs_base + CDNS_UART_CR);
	status &= ~CDNS_UART_CR_RX_DIS;
	status &= ~CDNS_UART_CR_TX_DIS;
	status |= CDNS_UART_CR_RX_EN|CDNS_UART_CR_TX_EN;
	writel(status, driver->regs_base + CDNS_UART_CR);

	status = readl(driver->regs_base + CDNS_UART_CR);


	/* Set the Mode Register with normal mode,8 data bits,1 stop bit,
	 * no parity.
	 */
	writel(CDNS_UART_MR_CHMODE_NORM | CDNS_UART_MR_STOPMODE_1_BIT
		| CDNS_UART_MR_PARITY_NONE | CDNS_UART_MR_CHARLEN_8_BIT,
		driver->regs_base + CDNS_UART_MR);

	/* Set the RX FIFO Trigger level to use most of the FIFO, but it
	 * can be tuned with a module parameter
	 */
	writel(rx_trigger_level, driver->regs_base + CDNS_UART_RXWM);
	//	writel(1, driver->regs_base + CDNS_UART_RXWM);

	/* Receive Timeout register is enabled but it
	 * can be tuned with a module parameter
	 */
	writel(rx_timeout, driver->regs_base + CDNS_UART_RXTOUT);

	/* Clear out any pending interrupts before enabling them */
	writel(readl(driver->regs_base + CDNS_UART_ISR),
			driver->regs_base + CDNS_UART_ISR);

	/* Set the Interrupt Registers with desired interrupts */

	writel(CDNS_UART_RX_IRQS |
			CDNS_UART_IXR_PARITY |
			CDNS_UART_IXR_TXEMPTY,
			driver->regs_base + CDNS_UART_IER);
//	writel(UART_CISR_MASK,
//			driver->regs_base + CDNS_UART_IER);


//	writel(~(CDNS_UART_RX_IRQS |
//					CDNS_UART_IXR_PARITY |
//					CDNS_UART_IXR_TXEMPTY),
//			driver->regs_base + CDNS_UART_IDR);


	int_unlock(level);

	serial_putstr("CDNS_UART_CR:");
    serial_puthex(readl(driver->regs_base + CDNS_UART_CR));
    serial_putchar('\n');

	serial_putstr("CDNS_UART_IMR:");
    serial_puthex(readl(driver->regs_base + CDNS_UART_IMR));
    serial_putchar('\n');

	return 0;
}



/******************************************************************************
 * 
 * 初始化串口接口
 * 
 * 		本程序负责初始化串口设备。
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 */


int serial_device_init(void)
{
	int i;	/* 循环变量 */

	/* 初始化标识 */
	static int serial_device_init_flag = 0;
	if (1 == serial_device_init_flag)
	{
		return 0;
	}

	for (i = 0; i < SERIAL_NUMBER; i++)
	{
		struct serial_device_struct_ex *driver = &global_serial_struct[i];
		memset(driver, 0, sizeof(struct serial_device_struct_ex));

		driver->count					= 0;
		driver->intvec					= serial_param_define[i].int_vector;

		struct tty_driver *base_driver =
			(struct tty_driver *)&(driver->driver);

		/* 操作赋值 */
		base_driver->open				= (TTY_OPEN)serial_open;
		base_driver->close				= (TTY_CLOSE)serial_close;

		base_driver->start_tx 			= (TTY_START_TX)serial_start_tx;
		base_driver->stop_tx 			= (TTY_STOP_TX)serial_stop_tx;
		base_driver->start_rx			= (TTY_START_RX)serial_start_rx;
		base_driver->stop_rx			= (TTY_STOP_RX)serial_stop_rx;
		base_driver->tx_char 			= (TTY_TX_CHAR)serial_tx_char;
		base_driver->rx_char 			= (TTY_RX_CHAR)serial_rx_char;
		base_driver->ioctl 				= (TTY_IOCTL)serial_ioctl;

		/* 属性配置 */
		base_driver->dev	 			= MKDEV(TTY_MAJOR, i);		/* 设备号 */
		driver->base_baudrate		= serial_param_define[i].base_baudrate;
		driver->baudrate			= serial_param_define[i].baudrate;
		driver->regs_base			= (u64)serial_param_define[i].regs_base;
		driver->delta				= serial_param_define[i].delta;
		driver->ierInit             = serial_param_define[i].ier_initvalue;

		memset(&(driver->statistic), 0, sizeof(struct serial_statistic));

		uart_init((uart_def *)driver->regs_base, &uart0_cfg);

		/* 初始化串口硬件 */
		serial_hw_init(driver);

	}

	/* 初始化标识 */
	serial_device_init_flag = 1;

	return 0;
}


/*******************************************************************************
 * 
 * 安装中断
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		成功返回0；失败返回-1
 * 
 */
int serial_device_register(void)
{
	int i;	/* 循环变量 */
//	printk(">>>[%d]%s\n", __LINE__, __func__); /* lcg 20230920 add */
	/* 初始化标识 */
	static int serial_device_register_flag = 0;
	if (1 == serial_device_register_flag)
	{
		return 0;
	}

	for (i = 0; i < SERIAL_NUMBER; i++)
	{
		struct serial_device_struct_ex *driver = &global_serial_struct[i];

		struct tty_driver *base_driver =
			(struct tty_driver *)&(driver->driver);

		/* 注册设备 */
		register_tty_device(serial_param_define[i].ldisc_name,
							serial_param_define[i].registered_path,
							base_driver);

		/* 挂接中断 */
//		int_install_handler("serial",serial_param_define[i].int_vector, 1,
//							(void (*)(void *))serial_int_isr1,
//							(void *)driver);
		shared_int_install(serial_param_define[i].int_vector,(void (*)(void *))serial_int_isr1, (void *)driver);

	}
//	printk(">>>[%d]%s\n", __LINE__, __func__); /* lcg 20230920 add */
	/* 初始化标识 */
	serial_device_register_flag = 1;

	return 0;
}


/*******************************************************************************
 * 
 * 串口模块初始化接口
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		无
 * 修改：
 * 		2011-11-11，删掉单独初始化串口模块的文件，将该函数合并至此
 */


int serial_module_init(/*uart_init_def *uart_init_cfg_ptr*/)
{
	int input_fd = -1;
	int output_fd = -1;
	int opt;

	data_ldisc_init();
	line_ldisc_init();//注册规则库 

	serial_device_init();

	/* 注册串口设备 */
	serial_device_register();


	/* 打开输入设备 */
	if (std_input_device_name && strstr(std_input_device_name, "/dev/serial"))
	{
		if (-1 == (input_fd = open(std_input_device_name, O_RDWR, 0777)))
		{
			printk("[ERROR] cannot open std input device %s\n", std_input_device_name);
			return -1;
		}
	}
	/* 打开输出设备 */
	if (std_output_device_name && strstr(std_output_device_name, "/dev/serial"))
	{
		if (-1 == (output_fd = open(std_output_device_name, O_RDWR, 0777)))
		{
			printk("[ERROR] cannot open std output device: %s\n", std_output_device_name);
			close(input_fd);
			return -1;
		}
	}

	ioctl(input_fd, TTY_GET_OPTIONS, &opt);

	opt |= TTY_OUT_CR2NL;
	ioctl(input_fd, TTY_SET_OPTIONS, opt);

	opt |= TTY_IN_ECHO;
	ioctl(input_fd, TTY_SET_OPTIONS, opt);

	ioctl(output_fd, TTY_GET_OPTIONS, &opt);

	opt |= TTY_OUT_CR2NL;
	ioctl(output_fd, TTY_SET_OPTIONS, opt);

	opt |= TTY_IN_ECHO;
	ioctl(output_fd, TTY_SET_OPTIONS, opt);

	global_std_set(0, input_fd);
	global_std_set(1, output_fd);
	global_std_set(2, output_fd);


	return 0;
}


void print_statistic(int no)
{
	int i;
	for (i = 0; i < SERIAL_NUMBER; i++)
	{
		struct serial_device_struct_ex *driver = &global_serial_struct[i];
		printk("serial[%d]: rx=%lu,  tx=%lu, ier=%lu, idr=%lu\n", i, driver->statistic.rx,
				driver->statistic.tx, driver->statistic.ier, driver->statistic.idr);
	}

}
