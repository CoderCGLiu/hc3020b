/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述： printk底层实现。在适配时要注意当前使用的是哪个串口，其工作
 * 		 频率为多少，其寄存器基址为多少。相关宏在bsp.h、xxx_uart.h中定义。
 * 修改：
 * 		 2013-07-29， 宋伟， 建立。
 */

#include "uart.h"


//<<<----------
#include "uart_driver.h"
#include "porting.h"
void printk_debug_hw_init(uart_init_def *uart_init_cfg_ptr);
//-------->>>add by FuKai,2021-3-10



//<<<----------
#include "uart_driver.h"
#include "porting.h"

uart_init_def default_uart_cfg = {
		.baudrate = UART_BAUD_115200,
		.word = UART_WORD_8BIT,
		.stop = UART_STOP_1BIT,
		.parity = UART_PARITY_NONE,
		.transmit_enable = UART_TRANSMIT_ENABLE,
		.receive_enable = UART_RECEIVE_ENABLE,
		.fifo_access = UART_FIFO_1BYTE_MODE,
		.irda_mode = UART_DEFAULT_MODE,
		.channel_mode = UART_NORMAL_CHANNEL,
		.clock_select = UART_SEL_APB_CLOCK,
		.clock_prescalar8 = UART_CLOCK_PRESCALAR_DIS
};
//-------->>>add by FuKai,2021-3-10

//extern char *std_output_device_name;
//__attribute__((section(".data"))) static u32 printk_dev_idx; //不能放在bss段。。。c_main会将其清零
/*******************************************************************************
 * 
 *  串口寄存器读
 * 
 *       实现读串口寄存器
 *       
 * 输入：
 *       addr		寄存器地址
 * 输出：
 *       无
 * 返回：
 *       寄存器值
 */
//static unsigned char sys_serial_read(unsigned long addr)
//{
//	return *(volatile unsigned char *) addr;
//}


/*******************************************************************************
 * 
 *  串口寄存器写
 * 
 *       实现写串口寄存器
 *       
 * 输入：
 *       addr		寄存器地址
 *       c          写入的值
 * 输出：
 *       无
 * 返回：
 *       寄存器值
 */
//static void sys_serial_write(unsigned long addr, unsigned char c)
//{
//	*(volatile unsigned char *) addr = c;
//	return;
//}


/*******************************************************************************
 * 
 *  输出一个字符
 * 
 *       往串口的发送寄存器写入一个字符。
 *       
 * 输入：
 *       c		待输出的字符
 * 输出：
 *       无
 * 返回：
 *       无 
 */

void testc(char c)
{
	char * addr = 0x900000001f050030;
	*addr = c;
}
void printk_debug_outchar(char c) 
{
//	printk_debug_hw_init(NULL);
	/* 循环等待发送寄存器为空 */
	while(!(UART0->CSR & UART_CSR_TEMPTY_MASK));
	UART0->FIFO = c;

	if (c == '\n')
	{
		/* 循环等待发送寄存器为空 */
		while(!(UART0->CSR & UART_CSR_TEMPTY_MASK));
		UART0->FIFO = '\r';
	}
	return ; 
}

void serial_putchar(char c) 
{
	printk_debug_outchar(c);
	
	return;
}

/*******************************************************************************
 * 
 *  轮询输出字符串
 * 
 *       轮询输出字符串。
 *       
 * 输入：
 *       s		待输出的字符串
 * 输出：
 *       无
 * 返回：
 *       无 
 */
void serial_putstr(char *s) 
{
	while (*s)
	{
		printk_debug_outchar(*s++);
	}
}


/*******************************************************************************
 * 
 *  轮询输出10进制数
 * 
 *       轮询输出10进制数。
 *       
 * 输入：
 *       n		待输出的数值
 * 输出：
 *       无
 * 返回：
 *       无 
 */
void serial_putdec(u32 n) 
{
	register u32 temp = n;
	register int i = 0;
	unsigned char a[10];

	do {
		a[i] = temp % 10;
		temp /= 10;
		i++;
	} while (temp);

	for (i = i - 1; i >= 0; i--)
	{
		printk_debug_outchar('0' + a[i]);
	}
}


/*******************************************************************************
 * 
 *  轮询输出16进制数
 * 
 *       轮询输出16进制数。
 *       
 * 输入：
 *       n		待输出的数值
 * 输出：
 *       无
 * 返回：
 *       无 
 */
void serial_puthex(u32 n) 
{
	register u32 temp = n;
	register int i = 0;
	unsigned char a[8];

	do {
		a[i] = temp % 16;
		temp /= 16;
		i++;
	   } while (temp);

	for (i = i - 1; i >= 0; i--) 
	{
		if (a[i] >= 10)
		{
			printk_debug_outchar('a' + a[i] - 10);
		}
		else
		{
			printk_debug_outchar('0' + a[i]);
		}
	}
}


/*******************************************************************************
 * 
 * 串口硬件初始化
 * 
 *       初始化串口硬件至正常工作，配置波特率、数据位、校验位、停止位等。 
 *       有时候由于引导程序已经做了这部分工作了，此时本函数留空也可以。
 *       
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		无 
 */

int printk_debug_hw_inited = 0;
void printk_debug_hw_init(uart_init_def *uart_init_cfg_ptr)
{	
	/* 利用串口作为printk输出，初始化相关硬件、波特率等 */	
	if ((!uart_init_cfg_ptr) && (printk_debug_hw_inited != 1))
	{
		uart_init(UART0, &default_uart_cfg);
		printk_debug_hw_inited = 1;
	}
	else if ((uart_init_cfg_ptr) && (printk_debug_hw_inited != 2))
	{
		uart_init(UART0, uart_init_cfg_ptr);
		printk_debug_hw_inited = 2;
	}

    return;
}
