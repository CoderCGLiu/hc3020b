/******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义串口寄存器、位信息及通用的串口驱动结构。
 * 修改：
 * 		 2013-07-29，宋伟，创建。
 */

#ifndef __UART_H__
#define __UART_H__

#include <reworks/types.h>
#include <tty_driver.h>
#include <bsp.h>



/* 串口寄存器定义 */
//#include "serial_regs.h"
#include "porting.h"
#include "drvCommon.h"
/**
 * printk使用的串口波特率
 * 一般设置为和引导程序的波特率一致
 */
#define BSP_UART_DEBUG_BAUD  	115200	

/**
 * 串口基地址
 */
//#if defined(__HUARUI2__)
//#include "tool/hr2SysTools.h"
//#define UART0_BASE_ADDR HR2_UART_PORT0
//#elif defined(__HUARUI3__)
////mod by FuKai,2021-3-10
////由于串口的IP更换，基地址发生改变
////#define UART0_BASE_ADDR ADDR_LP64(0xbf080000)
//#define UART0_BASE_ADDR ADDR_LP64(0x1f000000)
//#else
////LS3A
//#define UART_EACH_OFFSET	(8)
//#define UART0_BASE_ADDR	(LS3A_UART_PORT_BASE + 0x1e0 + (UART_EACH_OFFSET) * printk_dev_idx)
//#endif

///**
// * 串口寄存器访问
// */
//#if defined(__HUARUI2__) || defined(__HUARUI3__)
//#define UART_REG(reg)		(UART0_BASE_ADDR + 4*reg)
//#else
////LS3A
//#define UART_REG(reg)		(UART0_BASE_ADDR + reg)
//#endif
/**
 * 串口时钟分频
 */	
#define UART_CLK_FREQ (33333333)

/**
 * 串口驱动结构
 */
typedef struct 
{
	struct tty_driver driver;	/* 基本结构体 */
	u32 refcount;				/* 设备引用计数 */	
	int inum; 
	u32 baudrate;				/* 波特率 */
	int mode;					/* 串口工作模式 */
	struct 
	{
		u32	rx;					/* 接收 */
		u32 tx;					/* 发送 */
		u32 oe;					/* 溢出 */
		u32 pe;					/* 奇偶校验错 */
		u32 fe;					/* 帧错误 */
		u32 bi;					/* break */
	};
}UART_DRIVER;

/**
 * 用户配置参数
 */
typedef struct 
{
	u8	ldisc_name[64];		/* 串口使用的规则库名称 */
	u8	dev_name[128];		/* 待注册的设备路径 */	

	/* 其他信息如寄存器基地址、串口号或者中断号 */
	int port;
	int inum; 
	
	u32 baudrate;			/* 波特率 */	    	
}UART_PARAMS;


/*
 * Generic virtual read/write.  Note that we don't support half-word
 * read/writes.  We define __arch_*[bl] here, and leave __arch_*w
 * to the architecture specific code.
 */
#define __arch_getb(a)			(*(volatile unsigned char *)(a))
#define __arch_getw(a)			(*(volatile unsigned short *)(a))
#define __arch_getl(a)			(*(volatile unsigned int *)(a))

#define __arch_putb(v,a)		(*(volatile unsigned char *)(a) = (v))
#define __arch_putw(v,a)		(*(volatile unsigned short *)(a) = (v))
#define __arch_putl(v,a)		(*(volatile unsigned int *)(a) = (v))

/*
 * TODO: The kernel offers some more advanced versions of barriers, it might
 * have some advantages to use them instead of the simple one here.
 */
//#define dmb()		__asm__ __volatile__ ("" : : : "memory")
//#define dmb() __asm__ __volatile__ ("dmb" : : : "memory")
#define dmb()
#define __iormb()	dmb()
#define __iowmb()	dmb()

#define writeb(v,c)	({ u8  __v = v; __iowmb(); __arch_putb(__v,c); __v; })
#define writew(v,c)	({ u16 __v = v; __iowmb(); __arch_putw(__v,c); __v; })
//#define writel(v,c)	({ u32 __v = v; __iowmb(); __arch_putl(__v,c); __v; })


#define readb(c)	({ u8  __v = __arch_getb(c); __iormb(); __v; })
#define readw(c)	({ u16 __v = __arch_getw(c); __iormb(); __v; })
//#define readl(c)	({ u32 __v = __arch_getl(c); __iormb(); __v; })


#endif
