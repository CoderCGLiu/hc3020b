/******************************************************************************
 * 
 * 版权：
 * 		中国电子科技集团公司第三十二研究所
 * 描述：
 * 		16550A串口驱动程序头文件
 * 修改：
 * 		2011-08-24，唐立三，建立 
 */
#ifndef _REWORKS_IOS_SERIAL_16550A_H_
#define _REWORKS_IOS_SERIAL_16550A_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <reworks/types.h>		/* ReWorks类型头文件 */
#include <sys/errno.h>			/* ReWorks错误号头文件 */
#include <reworksio.h>
#include <tty_driver.h>		/* tty头文件 */
#include <tty_ioctl.h>
#include <semaphore.h>
//#include "serial_regs.h"		/* 串口驱动寄存器定义 */

#include "uart_reg_def.h"
#include "uart_driver.h"

/* 规则库名称最大长度 */
#define MAX_LDISC_NAME_LENGTH				64	
	
/* 串口设备名称最大程度 */
#define MAX_SERIAL_DEVICE_NAME_LENGTH		64
	
/* 串口设备路径最大长度 */
#define MAX_SERIAL_DEVICE_PATH_LENGTH		128
	
/* 串口数量 */
#ifndef _HC3020B_
#define SERIAL_NUMBER						2
#else
#define SERIAL_NUMBER						8 //2 /* lcg 20230920 */
#endif

/* 主要串口参数 */
struct serial_params
{
	char			ldisc_name[MAX_LDISC_NAME_LENGTH];
	char			device_name[MAX_SERIAL_DEVICE_NAME_LENGTH];
	char			registered_path[MAX_SERIAL_DEVICE_PATH_LENGTH];
	uart_def */*u8 */			regs_base;		/* 寄存器基址 */
	int				delta;			/* 寄存器宽度 */
	int				int_vector;		/* 中断向量 */
	int				priority;		/* ISR优先级 */
	unsigned int	base_baudrate;	/*  */
	unsigned int 	baudrate;		/* 波特率 */
	u8              ier_initvalue   /* IER初始值*/
};

/* 主要串口寄存器 */
//struct serial_regs
//{
//    int 							IER;
//    int 							LCR;
//    int								FCR;
//    int								MCR;
//};

/* 统计数据 */
struct serial_statistic
{
	unsigned int					rx;
	unsigned int					tx;
	unsigned int					oe; /* overflow error */
	unsigned int					pe; /* 校验错误parity error */
	unsigned int					fe; /* 帧错误framing error */
	unsigned int					bi; /* receive break */
	unsigned int                    te; /* timeout error */
	unsigned int                    ier;
	unsigned int                    idr;
};

/* 16550A串口设备描述结构定义 */
//struct serial_device_struct_ex
//{
//	struct tty_driver 				driver;
//	struct serial_regs				regs;
//	struct serial_statistic			statistic;
//	sem_t							isr_sem;
//	pthread_t						isr_thread;
//	int 							count;
//	u32								intvec;
//};

struct serial_device_struct_ex
{
	struct tty_driver 				driver;			/* 基本结构体 */
	int 							count;			/* 设备使用计数 */
	u32								intvec;			/* 中断向量 */
	unsigned int					base_baudrate;	/* 基准波特率 */
	unsigned int 					baudrate;		/* 波特率 */
	/*uart_def**/u64    					regs_base;		/* 寄存器基址 */
	u8								delta;			/* 寄存器宽度 */
	uart_def				        regs;			/* 寄存器内容备份 */
	struct serial_statistic			statistic;		/* 统计信息 */
	sem_t							isr_sem;		/* 中断服务同步 */
	pthread_t						isr_thread;		/* 中断服务线程 */	
	u8                              ierInit;        /*关中断值*/
	uart_init_def                   cfgs;
};

/**
 * 打开接口
 */
int serial_open(struct tty_driver *driver);

/**
 * 关闭接口
 */
int serial_close(struct tty_driver *driver);

/**
 * 启动发送
 */
int serial_start_tx(struct tty_driver *driver);

/**
 * 停止发送 
 */
int serial_stop_tx(struct tty_driver *driver);

/**
 * 启动接收
 */
int serial_start_rx(struct tty_driver *driver);

/**
 * 停止接收
 */
int serial_stop_rx(struct tty_driver *driver);

/**
 * 发送一个字符
 */
int serial_tx_char(struct tty_driver *driver, char c);

/**
 * 接收一个字符
 */
int serial_rx_char(struct tty_driver *driver, char *c);

/**
 * 控制
 */
int serial_ioctl(struct tty_driver *driver, u32 cmd, void *arg);

/**
 * 中断服务程序
 */
void serial_int_isr(struct serial_device_struct_ex *driver);

/**
 * 初始化函数
 */
int init_data_serial_hw(struct serial_device_struct_ex *driver);

#ifdef __cplusplus
}
#endif
	
#endif
