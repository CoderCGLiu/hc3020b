#include "uart_driver.h"
#include <irq.h>
						/*baudrate,bdiv,cd*/
//static const u32 uart0_cfg_20M[][3] = {
//						{4800,84, 49},
//                        {9600,207, 18},
//                        {14400,138, 10},
//                        {28800,98, 7},
//                        {19200,129, 8},
//                        {38400,103, 5},
//                        {57600,86, 4},
//                        {115200,28, 6},
//                        {128000,25, 6},
//                        {230400,28, 3},
//                        {256000,12, 6},
//						{460800,21, 2},
//						{0,0, 0}};

static const u32 uart0_cfg_33M[][3] = {
				{2400, 31, 434},
		        {4800, 31, 217},
		        {9600, 15, 217},
		        {14400, 12, 178},
		        {19200, 13, 124},
		        {38400, 13, 62},
		        {56000, 16, 35},
		        {115200, 16, 17},
		        {128000, 12, 20},
		        {230400, 11, 12},
		        {256000, 12, 10},
		        {460800, 5, 12},
		        {1849600, 5, 3},
};

void uart_init(uart_def *uartx ,uart_init_def *uart_cfg)
{
	u8 i=0x00;
	int bdivr=0x00,cd=0x00;
	int temp=0x00;

	int int_level;
	int_level = int_lock();

	/*configuer mode*/
	temp = uartx->MODE;
	
	temp &=~UART_MODE_MASK;
	temp |= (uart_cfg->word<<1);
	temp |= (uart_cfg->stop<<6);
	temp |= (uart_cfg->fifo_access<<12);
	temp |= (uart_cfg->channel_mode<<8);
	temp |= (uart_cfg->irda_mode<<11);
	temp |= (uart_cfg->clock_select<<10);
	temp |= (uart_cfg->clock_prescalar8<<0);
	temp |= (uart_cfg->parity<<3);

 	uartx->MODE = temp;


	for (i = 0; i < 13; i++) {
		if (uart0_cfg_33M[i][0] == uart_cfg->baudrate) {
			bdivr = uart0_cfg_33M[i][1];
			cd = uart0_cfg_33M[i][2];
			//bdivr = 5;		//zz modify for jiasuqi
			//cd = 3;			//zz modify for jiasuqi
			break;
		}
	}
	if(i==12)
	{										/*9600,207, 18*/
		temp = uartx->BDIVR;
		temp &=~UART_BDIVR_MASK;
		temp |= 190;
		uartx->BDIVR = temp;
		
		temp = uartx->BRGR;
		temp &=~UART_BRGR_MASK;
		temp |= 18;
		uartx->BRGR = temp;		
	}else{
		temp = uartx->BDIVR;
		temp &=~UART_BDIVR_MASK;
		temp |= bdivr;
		uartx->BDIVR = temp;
		
		temp = uartx->BRGR;
		temp &=~UART_BRGR_MASK;
		temp |= cd;
		uartx->BRGR = temp;	
	}

	temp = uartx->CTRL;
	temp &= ~(1<<8);
	temp &= ~(1<<7);
	if(uart_cfg->transmit_enable == UART_TRANSMIT_ENABLE){
		temp |= (1<<4);
		temp &= ~(1<<5);
	}else{
		temp |= (1<<5);
	}

	if(uart_cfg->receive_enable == UART_RECEIVE_ENABLE){
		temp |= (1<<2);
		temp &= ~(1<<3);
	}else{
		temp |= (1<<3);
	}
	temp |= (1<<1);
	temp |= (1<<0);
	uartx->CTRL = temp;
	int_unlock(int_level);
}

/*葛文博 20230818 add:: 修复串口波特率设置功能*/
void uart_init_set_test(uart_def *uartx ,uart_init_def *uart_cfg)
{
	u8 i=0x00;
	int bdivr=0x00,cd=0x00;
	int temp=0x00;

	int int_level;
	printk("before\n");
	int_level = int_lock();
	printk("after\n");
	/*configuer mode*/
	temp = uartx->MODE;
	
	temp &=~UART_MODE_MASK;
	temp |= (uart_cfg->word<<1);
	temp |= (uart_cfg->stop<<6);
	temp |= (uart_cfg->fifo_access<<12);
	temp |= (uart_cfg->channel_mode<<8);
	temp |= (uart_cfg->irda_mode<<11);
	temp |= (uart_cfg->clock_select<<10);
	temp |= (uart_cfg->clock_prescalar8<<0);
	temp |= (uart_cfg->parity<<3);
	printk("input baudrate = %d, word = %d,  stop = %d, fifo_access = %d, channel_mode = %d, "
				"irda_mode = %d, clock_select = %d, clock_prescalar8 = %d, parity = %d\n", 
				uart_cfg->baudrate, uart_cfg->word, uart_cfg->stop, uart_cfg->fifo_access, uart_cfg->channel_mode, 
				uart_cfg->irda_mode, uart_cfg->clock_select, uart_cfg->clock_prescalar8, uart_cfg->parity);
	pthread_delay(50);
 	uartx->MODE = temp;

 	
	for (i = 0; i < 13; i++) {
		if (uart0_cfg_33M[i][0] == uart_cfg->baudrate) {
			bdivr = uart0_cfg_33M[i][1];
			cd = uart0_cfg_33M[i][2];
			//bdivr = 5;		//zz modify for jiasuqi
			//cd = 3;			//zz modify for jiasuqi
			break;
		}
	}
	printk("i = %d, bdivr = %d, cd = %d\n", i, bdivr, cd);
	if(i==12)
	{										/*9600,207, 18*/
		// printk("i=12\n");
		temp = uartx->BDIVR;
		temp &=~UART_BDIVR_MASK;
		temp |= 190;
		uartx->BDIVR = temp;
		
		temp = uartx->BRGR;
		temp &=~UART_BRGR_MASK;
		temp |= 18;
		uartx->BRGR = temp;		
	}else{
		temp = uartx->BDIVR;
		temp &=~UART_BDIVR_MASK;
		temp |= bdivr;
		uartx->BDIVR = temp;
		
		temp = uartx->BRGR;
		temp &=~UART_BRGR_MASK;
		temp |= cd;
		uartx->BRGR = temp;	
	}

//	temp = uartx->CTRL;
//	temp &= ~(1<<8);
//	temp &= ~(1<<7);
//	if(uart_cfg->transmit_enable == UART_TRANSMIT_ENABLE){
//		temp |= (1<<4);
//		temp &= ~(1<<5);
//	}else{
//		temp |= (1<<5);
//	}
//
//	if(uart_cfg->receive_enable == UART_RECEIVE_ENABLE){
//		temp |= (1<<2);
//		temp &= ~(1<<3);
//	}else{
//		temp |= (1<<3);
//	}
//	temp |= (1<<1);
//	temp |= (1<<0);
//	uartx->CTRL = temp;
	int_unlock(int_level);
	printk("set baud end\n", i, bdivr, cd);
}

void uart_poll_put_char(uart_def *uartx,int c)
{
	while(!(uartx->CSR & UART_CSR_TEMPTY_MASK));
	uartx->FIFO = c;
	while(!(uartx->CSR & UART_CSR_TEMPTY_MASK));
}
void uart_poll_fifo_put_char(uart_def *uartx,int c)
{
	//while((uartx->CSR & UART_CSR_TFUL_MASK));
	uartx->FIFO = c;
}

int uart_poll_get_char(uart_def *uartx)
{
	int ret=-1;
	if(uartx->CSR & UART_CSR_REMPTY_MASK){
		ret = uartx->FIFO;
	}
	return ret;
}
int uart_block_get_char(uart_def *uartx)
{
	int ret=-1;
	int overtime=10000;
	while((uartx->CSR & UART_CSR_REMPTY_MASK) && overtime--);
	if(overtime >= 0)
		ret = uartx->FIFO;
	
	return ret;
}
void uart_write_byte(uart_def *uartx, u8 data){
	while(!(uartx->CSR & UART_CSR_TEMPTY_MASK)); //发送队列满了则等待
	u32 level;

	uartx->FIFO = data;

}


void uart_write_bytes(uart_def *uartx,u8 *data,u32 size){
	u32 i=0;
	for(i=0;i<size;i++){
		uart_write_byte(uartx,data[i]);
	}
}

int uart_read_byte(uart_def *uartx, u8 * data)
{
	if(uartx->CSR & UART_CSR_REMPTY_MASK) return 0; //接收队列为空，则返回
	u32 level;

	*data = (u8)(uartx->FIFO & 0xFF);

	return 1;
}

int uart_read_bytes(uart_def *uartx,u8 *data,u32 size)
{
	u32 i=0;
	for(i=0;i<size;i++){
		if (uart_read_byte(uartx, &data[i]) == 0) break;
	}
	return i;
}


void uart_enable_pics(uart_def *uartx, u32 pics){
	u32 level;
	level = int_lock();
	uartx->IER = pics;
	int_unlock(level);
}
void uart_disable_pics(uart_def *uartx, u32 pics){
	u32 level;
	level = int_lock();
	uartx->IDR = pics;
	int_unlock(level);
}
u8 uart_get_picstate(uart_def *uartx, u32 pic){
	if(uartx->CISR & pic)
		return 1;
	return 0;
}
u8 uart_get_picmask(uart_def *uartx, u32 pic){
	if(uartx->IMR & pic)
		return 1;
	return 0;
}

void uart_rfifo_trigger(uart_def *uartx,int num){
	uartx->RTR = num;
}
void uart_tfifo_trigger(uart_def *uartx,int num){
	uartx->TTR = num;
}


void uart_print_ctrl()
{
//	printk("CTRL: 0x%x\r\n", UART0->CTRL);
	printk("ISR: 0x%x\r\n", UART0->CISR);
	printk("IMR: 0x%x\r\n", UART0->IMR);

}

void uart_enable_ctrl()
{
//	printk("CTRL: 0x%x\r\n", UART0->CTRL);
//	printk("CTRL: 0x%x\r\n", UART0->CISR);
//	printk("CTRL: 0x%x\r\n", UART0->IMR);
//	uart_enable_pics(UART0, 1);
//	uart_rfifo_trigger(UART0, 1);
    /* Enable the TX Empty interrupt */
	u32 status;
    status = UART0->IER;
    UART0->IER = status|CDNS_UART_IXR_TXEMPTY;

}
