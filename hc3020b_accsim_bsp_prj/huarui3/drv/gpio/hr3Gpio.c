#include "config.h"
#include <stdlib.h>
#include <stdio.h>
#include "hr3Gpio.h"
#include "../../h/tool/hr3SysTools.h"
#include <math.h>
#include <semLib.h>
#include "../../h/tool/hr3Macro.h"
#include <irq.h>
#include <semaphore.h>
/*********************************************************************************************************
  调试开关
*********************************************************************************************************/
#define  BSP_CFG_GPIO_DBG                      1
#define  _GPIO_SEM_REWORKS_
/*********************************************************************************************************
  调试信息，全部定义为全局可见
*********************************************************************************************************/
#if BSP_CFG_GPIO_DBG
#if 1 /*ldf 20230609 add*/
HRGPIODBGINFO  _G_gpioDbgInfo[GPIO_INT_NUM] = {0};
#else
HRGPIODBGINFO * _G_gpioDbgInfo[GPIO_INT_NUM] = {
    (HRGPIODBGINFO *)DBG_GPIO1_BASE_ADDR,
    (HRGPIODBGINFO *)DBG_GPIO2_BASE_ADDR,
    (HRGPIODBGINFO *)DBG_GPIO3_BASE_ADDR,
    (HRGPIODBGINFO *)DBG_GPIO4_BASE_ADDR,
    (HRGPIODBGINFO *)DBG_GPIO5_BASE_ADDR,
    (HRGPIODBGINFO *)DBG_GPIO6_BASE_ADDR,
    (HRGPIODBGINFO *)DBG_GPIO7_BASE_ADDR
};
#endif
int  _G_gpioDbgBscSign = 0;                                             /*  记录基本调试信息的标识      */
                                                                        /*  0 表示不记录，1 表示记录    */
int  _G_gpioDbgDtlSign = 0;                                             /*  记录详细调试信息的标识，    */
                                                                        /*  0 表示不记录，1 表示记录    */
#endif
/*********************************************************************************************************
  函数声明
*********************************************************************************************************/
int  hrGpioClearInt(u32  uiGpioIndex);
int  hrGpioSetTransDirection(u32  uiGpioIndex, u32  uiDirection);
int  hrGpioSetConfigMode(u32  uiGpioIndex, u32  uiMode);
int  hrGpioSetIntEnable(u32  uiGpioIndex, u32  uiEnable);
int  hrGpioSetIntControl(u32  uiGpioIndex, u32  uiMode);
int  hrGpioSetIntEdge(u32  uiGpioIndex, u32  uiMode);
int  hrGpioGetIntStatus(u32  uiGpioIndex);
int  hrGpioSetIntMode(u32  uiGpioIndex, u32  uiMode);
/*********************************************************************************************************
  外部变量
*********************************************************************************************************/
#if 0
#if BSP_CFG_HAS_PCIE
extern SEM_ID  semID_PCIE_REC;
#endif
extern int     hr_bsl_pcie_callback_func_param0;
extern int     hr_bsl_pcie_callback_func_param1;
extern int     hr_bsl_pcie_callback_func_param2;
#endif
/*********************************************************************************************************
  全局变量
*********************************************************************************************************/
#ifndef _GPIO_SEM_REWORKS_
SEM_ID  _G_semIDGpioInt[GPIO_INT_NUM] = {0};                            /*  GPIO 中断信号量             */
#else
sem_t	_G_semIDGpioInt[GPIO_INT_NUM] = {0};                            /*  GPIO 中断信号量             */
#endif
/*********************************************************************************************************
  局部变量
*********************************************************************************************************/
static u32 s_gpioIndex = 0;                                            /*  GPIO Index                  */
static u32 s_gpioIntNum[GPIO_INT_NUM] = {0};                           /*  GPIO 中断数量               */

//VOIDFUNCPTR hrBslGpio0CallbackFuncptr    = NULL;
//int         hrBslGpio0CallbackFuncParam0 = 0;
//int         hrBslGpio0CallbackFuncParam1 = 0;
//
//VOIDFUNCPTR hrBslGpio1CallbackFuncptr    = NULL;
//int         hrBslGpio1CallbackFuncParam0 = 0;
//int         hrBslGpio1CallbackFuncParam1 = 0;



GPIO_INT_HANDLER GpioCallbackFuncptr[GPIO_INT_NUM] = {NULL};/*ldf 20230417*/
void *GpioCallbackFuncParam[GPIO_INT_NUM] = {NULL};/*ldf 20230417*/

void hrGpioInt(void *arg)
{
	u32 gpioIndex = (u32)arg;
	
#if 0 /*ldf 20230608 add*/
    u32 val = *(volatile u32 *)GPIO_INT_MASK_INTSTATUS_REG;/*中断状态寄存器*/
    u32 i;
//    printk("<**DEBUG**> [%s():_%d_]:: gpioIndex = %d, val = %d\n", __FUNCTION__, __LINE__,gpioIndex,val);
    for(i = 0; i < GPIO_INT_NUM; i++)
    {
    	if((val & (0x1<<i)) != 0)
    	{
//    		printk("<**DEBUG**> [%s():_%d_]:: GPIO INT ...,  i = %d\n", __FUNCTION__, __LINE__,i);
    		
            if(_G_gpioDbgBscSign == 1)
            {
                _G_gpioDbgInfo[i].intNumIn = _G_gpioDbgInfo[i].intNumIn + 1;
            }
    	#if 1
                semGive(_G_semIDGpioInt[i]);
    	#else /*ldf 20230608 add*/
    			/*执行对应的中断处理函数*/
    			if((NULL != GpioCallbackFuncptr[i]))
    				(*GpioCallbackFuncptr[i])(GpioCallbackFuncParam[i]);/*ldf 20230608 add*/
    	#endif
                
                s_gpioIntNum[i] = s_gpioIntNum[i] + 1;
                
                hrGpioClearInt(i /*+ 1*/);

                if(_G_gpioDbgBscSign == 1)
                {
                    _G_gpioDbgInfo[i].intNumOut = _G_gpioDbgInfo[i].intNumOut + 1;
                }
        }
    }
#elif 0 /*ldf 20230608 add*/
	if(hrGpioGetIntStatus(gpioIndex /*+ 1*/) == 1)
	{
		if(_G_gpioDbgBscSign == 1)
		{
			_G_gpioDbgInfo[gpioIndex].intNumIn = _G_gpioDbgInfo[gpioIndex].intNumIn + 1;
		}
	#if 1
		semGive(_G_semIDGpioInt[gpioIndex]);
	#else /*ldf 20230608 add*/
		/*执行对应的中断处理函数*/
		if((NULL != GpioCallbackFuncptr[gpioIndex]))
			(*GpioCallbackFuncptr[gpioIndex])(GpioCallbackFuncParam[gpioIndex]);/*ldf 20230608 add*/
	#endif
		
		s_gpioIntNum[gpioIndex] = s_gpioIntNum[gpioIndex] + 1;
		
		hrGpioClearInt(gpioIndex /*+ 1*/);

		if(_G_gpioDbgBscSign == 1)
		{
			_G_gpioDbgInfo[gpioIndex].intNumOut = _G_gpioDbgInfo[gpioIndex].intNumOut + 1;
		}
	}
#else
    unsigned int i = 0;

    for(i = 0; i < GPIO_INT_NUM; i++)
    {
        if(hrGpioGetIntStatus(i /*+ 1*/) == 1)
        {
            if(_G_gpioDbgBscSign == 1)
            {
                _G_gpioDbgInfo[i].intNumIn = _G_gpioDbgInfo[i].intNumIn + 1;
            }
	#if 1
	#ifndef _GPIO_SEM_REWORKS_
            semGive(_G_semIDGpioInt[i]);
	#else
            sem_post(&_G_semIDGpioInt[i]);
	#endif
	#else /*ldf 20230608 add*/
			/*执行对应的中断处理函数*/
			if((NULL != GpioCallbackFuncptr[i]))
				(*GpioCallbackFuncptr[i])(GpioCallbackFuncParam[i]);/*ldf 20230608 add*/
	#endif
            
            s_gpioIntNum[i] = s_gpioIntNum[i] + 1;
            
            hrGpioClearInt(i /*+ 1*/);

            if(_G_gpioDbgBscSign == 1)
            {
                _G_gpioDbgInfo[i].intNumOut = _G_gpioDbgInfo[i].intNumOut + 1;
            }
        }
    }
#endif
    return;
}



/*********************************************************************************************************
** 函数名称: hrGpioInstInit
** 功能描述: GPIO 初始化
** 输　入  : NONE
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
void  hrGpioInstInit (void)
{
	int  i;
    char gpio_isr[20];
    
	u32 val;
	
	/*配置其它寄存器的通道解锁*/
	*(volatile u32 *)(0x900000001f0c0028) = 0xa5accede;// unlock
	
	/*GPIO[15:20]配置为int模式*/
	val = *(volatile u32 *)(0x900000001f0c000c);
	val &= ~(0x1f<<17);
	val |= 0xf<<17;
	*(volatile u32 *)(0x900000001f0c000c) = val;
	

    for (i = 0; i < GPIO_INT_NUM; i++) 
    {
#ifndef _GPIO_SEM_REWORKS_
        _G_semIDGpioInt[s_gpioIndex + i] = semBCreate(SEM_Q_PRIORITY, SEM_EMPTY);
#else
        sem_init2(&_G_semIDGpioInt[s_gpioIndex + i], 0, SEM_BINARY, PTHREAD_WAITQ_FIFO, 0);
#endif
        
//        hrGpioSetConfigMode(i, 0);/*ldf 20230608 add:: PWM需要设置，分为软件计数还是硬件计数*/

        hrGpioClearInt(s_gpioIndex + i);                                /*  清除中断                    */
        hrGpioSetTransDirection(s_gpioIndex + i, 0);                    /*  设置为 input                */

        hrGpioSetIntMode(s_gpioIndex + i, 0);                           /*  设置为边沿触发中断          */
        hrGpioSetIntEdge(s_gpioIndex + i, 1);                           /*  设置上升沿触发              */
    }

    for (i = 0; i < GPIO_INT_NUM; i++) {
         memset(gpio_isr, 0, sizeof(gpio_isr));
         sprintf(gpio_isr, "gpio_isr%d", i);
         
         int_install_handler(gpio_isr, HRGPIO_INT_VECTOR(i),  1, (INT_HANDLER)hrGpioInt, (void*)i);
         int_enable_pic(HRGPIO_INT_VECTOR(i));
    }

    return;
}

void  hrGpioIntEnable (int  iIndex)
{
    hrGpioSetIntEnable(s_gpioIndex + iIndex, 1);                        /*  使能 GPIO 中断              */
}

void  hrGpioIntDisable (int  iIndex)
{
    hrGpioSetIntEnable(s_gpioIndex + iIndex, 0);                        /*  禁能 GPIO 中断              */
}


u32 hrGpioGetIntNum(u32 gpioIndex)
{
    return s_gpioIntNum[gpioIndex];
}


void hrGpioIntConnect(u32 gpioIndex,GPIO_INT_HANDLER routine,void *param)
{
    if(gpioIndex > 23)
    {
        printk("gpioIndex must be 0~23!\n");
        return;
    }
    
    GpioCallbackFuncptr[gpioIndex] = routine;/*ldf 20230417*/
    GpioCallbackFuncParam[gpioIndex] = param;/*ldf 20230417*/
//    if(gpioIndex == 0)
//    {
//        hrBslGpio0CallbackFuncptr = routine;
//        hrBslGpio0CallbackFuncParam1 = param;
//    }
//    else if(gpioIndex == 1)
//    {
//        hrBslGpio1CallbackFuncptr = routine;
//        hrBslGpio1CallbackFuncParam0 = param;
//    }
	return;
}
/* 获取外部中断信号量  */
int hrGpioGetIntSem(u32 gpioIndex)
{
#if 0
    if((gpioIndex < 1) || (gpioIndex > 6)) // 0为片间中断，1~6为外部中断
    {
        printf("[DSP%d]获取外部中断信号量时gpioIndex取值范围为1~6.\n",sysCpuGetID());
        return ERROR;
    }
#endif

#ifndef _GPIO_SEM_REWORKS_
    semTake(_G_semIDGpioInt[gpioIndex], WAIT_FOREVER);
#else
    sem_wait2(&_G_semIDGpioInt[gpioIndex], WAIT, NO_TIMEOUT);
#endif

    return 0;
}
/*
 * 设置GPIO的传输方向，1为output，0为input
 */
int hrGpioSetTransDirection(u32 gpioIndex, u32 direction)
{
    u32 tmp = 0;

    if(gpioIndex > 23)
    {
        printf("设置GPIO的传输方向时参数gpioIndex错误，只能取值0~23！\n");
        return ERROR;
    }
    if(direction > 1)
    {
        printf("设置GPIO的传输方向时参数direction错误，只能取值0~1！\n");
        return ERROR;
    }
#if defined(_GPIO_INT_REG_)
    tmp = *(volatile u32 *)GPIO_INT_OUTPUT_ENABLE_REG;

    if(direction == 0)
    {
        tmp = tmp & (~(1 << gpioIndex));
        *(volatile u32 *)GPIO_INT_OUTPUT_ENABLE_REG = tmp;
    }
    else if(direction == 1)
    {
        tmp = tmp | (1 << gpioIndex);
        *(volatile u32 *)GPIO_INT_OUTPUT_ENABLE_REG = tmp;
    }
#elif defined(_GPIO_PWM_REG_)
    tmp = *(volatile u32 *)GPIO_PWM_OUTPUT_ENABLE_REG;

    if(direction == 0)
    {
        tmp = tmp & (~(1 << gpioIndex));
        *(volatile u32 *)GPIO_PWM_OUTPUT_ENABLE_REG = tmp;
    }
    else if(direction == 1)
    {
        tmp = tmp | (1 << gpioIndex);
        *(volatile u32 *)GPIO_PWM_OUTPUT_ENABLE_REG = tmp;
    }
#elif defined(_GPIO_NM_REG_)
    int i = 0;
    if(gpioIndex >= 16)
    {
    	i = gpioIndex % 16;
		tmp = *(volatile u32 *)GPIO_NM1_OUTPUT_ENABLE_REG;
	
		if(direction == 0)
		{
			tmp = tmp & (~(1 << i));
			*(volatile u32 *)GPIO_NM1_OUTPUT_ENABLE_REG = tmp;
		}
		else if(direction == 1)
		{
			tmp = tmp | (1 << i);
			*(volatile u32 *)GPIO_NM1_OUTPUT_ENABLE_REG = tmp;
		}
    }
    else
    {
		tmp = *(volatile u32 *)GPIO_NM0_OUTPUT_ENABLE_REG;
	
		if(direction == 0)
		{
			tmp = tmp & (~(1 << gpioIndex));
			*(volatile u32 *)GPIO_NM0_OUTPUT_ENABLE_REG = tmp;
		}
		else if(direction == 1)
		{
			tmp = tmp | (1 << gpioIndex);
			*(volatile u32 *)GPIO_NM0_OUTPUT_ENABLE_REG = tmp;
		}
    }
#endif

    return OK;
}
/*
 * outputVal = 1, 输出高电平
 */
u32 hrGpioSetOutputVal(u32 gpioIndex, u32 outputVal)
{
	u32 val = 0;
#if defined(_GPIO_INT_REG_)
	val = *(volatile u32 *)GPIO_INT_OUTPUT_DATA_REG;
	val &= ~(1 << gpioIndex);
    *(volatile u32 *)GPIO_INT_OUTPUT_DATA_REG = val | ((outputVal&0x1)<<gpioIndex);
    
    do{
    	val = *(volatile u32 *)GPIO_INT_OUTPUT_DATA_REG;
    	val &= 1<<gpioIndex;
    }while(val != ((outputVal&0x1)<<gpioIndex));
#elif defined(_GPIO_NM_REG_)
    int i = 0;
    if(gpioIndex >= 16)
    {
    	i = gpioIndex % 16;
		val = *(volatile u32 *)GPIO_NM1_OUTPUT_ENABLE_REG;
		val |= (1 << i);
		*(volatile u32 *)GPIO_NM1_OUTPUT_ENABLE_REG = val;   //16位有效寄存器，写1使能0-15 output
	
		*(volatile u32 *)GPIO_NM1_MODE_REG = 0;   //16位有效寄存器，使能0-15 soft 配置 :0             hardware:1
	
		val = *(volatile u32 *)GPIO_NM1_OUTPUT_DATA_REG;
		val &= ~(1 << i);
		*(volatile u32 *)GPIO_NM1_OUTPUT_DATA_REG = val | ((outputVal&0x1)<<i);   //16位有效寄存器，0-15  写1-H 0-L
    }
    else
    {
		val = *(volatile u32 *)GPIO_NM0_OUTPUT_ENABLE_REG;
		val |= (1 << i);
		*(volatile u32 *)GPIO_NM0_OUTPUT_ENABLE_REG = val;   //16位有效寄存器，写1使能0-15 output
	
		*(volatile u32 *)GPIO_NM0_MODE_REG = 0;   //16位有效寄存器，使能0-15 soft 配置 :0             hardware:1
	
		val = *(volatile u32 *)GPIO_NM0_OUTPUT_DATA_REG;
		val &= ~(1 << i);
		*(volatile u32 *)GPIO_NM0_OUTPUT_DATA_REG = val | ((outputVal&0x1)<<i);   //16位有效寄存器，0-15  写1-H 0-L
    }
#elif defined(_GPIO_PWM_REG_)
	val = *(volatile u32 *)GPIO_PWM_OUTPUT_DATA_REG;
	val &= ~(1 << gpioIndex);
    *(volatile u32 *)GPIO_PWM_OUTPUT_DATA_REG = val | ((outputVal&0x1)<<gpioIndex);
    
    do{
    	val = *(volatile u32 *)GPIO_PWM_OUTPUT_DATA_REG;
    	val &= 1<<gpioIndex;
    }while(val != ((outputVal&0x1)<<gpioIndex));
#endif 
    return (outputVal&0x1);
}
/*
 * 值仅低24位有效
 */
u32 hrGpioGetOutputVal(u32 gpioIndex)
{
    u32 inputVal = 0;
#if defined(_GPIO_INT_REG_)
    inputVal = *(volatile u32 *)GPIO_INT_INPUT_DATA_REG;
#elif defined(_GPIO_PWM_REG_)
    inputVal = *(volatile u32 *)GPIO_PWM_INPUT_DATA_REG;
#elif defined(_GPIO_NM_REG_)
    inputVal = *(volatile u32 *)GPIO_NM1_INPUT_DATA_REG;
#endif
    
//    printk("<**DEBUG**> [%s():_%d_]:: inputVal = 0x%x\n", __FUNCTION__, __LINE__,inputVal);
    
    inputVal &= 1<<gpioIndex;

    return (inputVal>>gpioIndex);
}
/*
 * 设置GPIO的配置模式，0为软件配置，1为硬件配置
 */
int hrGpioSetConfigMode(u32 gpioIndex, u32 mode)
{
    u32 tmp = 0;

    if(gpioIndex > 23)
    {
        printf("设置GPIO的配置模式时参数gpioIndex错误，只能取值0~23！\n");
        return ERROR;
    }
    if(mode > 1)
    {
        printf("设置GPIO的配置模式时参数mode错误，只能取值0~1！\n");
        return ERROR;
    }
#if defined(_GPIO_INT_REG_)
    tmp = *(volatile u32 *)GPIO_INT_MODE_REG;

    if(mode == 0)
    {
        tmp = tmp & (~(1 << gpioIndex));
        *(volatile u32 *)GPIO_INT_MODE_REG = tmp;
    }
    else if(mode == 1)
    {
        tmp = tmp | (1 << gpioIndex);
        *(volatile u32 *)GPIO_INT_MODE_REG = tmp;
    }
#elif defined(_GPIO_PWM_REG_)
    tmp = *(volatile u32 *)GPIO_PWM_MODE_REG;

    if(mode == 0)
    {
        tmp = tmp & (~(1 << gpioIndex));
        *(volatile u32 *)GPIO_PWM_MODE_REG = tmp;
    }
    else if(mode == 1)
    {
        tmp = tmp | (1 << gpioIndex);
        *(volatile u32 *)GPIO_PWM_MODE_REG = tmp;
    }
#elif defined(_GPIO_NM_REG_)
    int i = 0;
    if(gpioIndex >= 16)
    {
    	i = gpioIndex % 16;
        tmp = *(volatile u32 *)GPIO_NM1_MODE_REG;

        if(mode == 0)
        {
            tmp = tmp & (~(1 << i));
            *(volatile u32 *)GPIO_NM1_MODE_REG = tmp;
        }
        else if(mode == 1)
        {
            tmp = tmp | (1 << i);
            *(volatile u32 *)GPIO_NM1_MODE_REG = tmp;
        }
    	
    }
    else
    {
        tmp = *(volatile u32 *)GPIO_NM0_MODE_REG;

        if(mode == 0)
        {
            tmp = tmp & (~(1 << gpioIndex));
            *(volatile u32 *)GPIO_NM0_MODE_REG = tmp;
        }
        else if(mode == 1)
        {
            tmp = tmp | (1 << gpioIndex);
            *(volatile u32 *)GPIO_NM0_MODE_REG = tmp;
        }
    }
#endif

    return OK;
}
/*
 * 设置GPIO的中断使能，0为mask，1为enable
 */
int hrGpioSetIntEnable(u32 gpioIndex, u32 enable)
{
    u32 tmp = 0;

    if(gpioIndex > 23)
    {
        printf("设置GPIO的中断使能时参数gpioIndex错误，只能取值0~23！\n");
        return ERROR;
    }
    if(enable > 1)
    {
        printf("设置GPIO的中断使能时参数enable错误，只能取值0~1！\n");
        return ERROR;
    }

    tmp = *(volatile u32 *)GPIO_INT_INTENABLE_REG;

    if(enable == 0)
    {
        tmp = tmp & (~(1 << gpioIndex));
        *(volatile u32 *)GPIO_INT_INTENABLE_REG = tmp;
    }
    else if(enable == 1)
    {
        tmp = tmp | (1 << gpioIndex);
        *(volatile u32 *)GPIO_INT_INTENABLE_REG = tmp;
    }

    return OK;
}
/*
 * 设置GPIO的中断模式，0为边沿触发产生中断，1为电平产生中断
 */
int hrGpioSetIntMode(u32 gpioIndex, u32 mode)
{
    u32 tmp = 0;

    if(gpioIndex > 23)
    {
        printf("设置GPIO的配置模式时参数gpioIndex错误，只能取值0~23！\n");
        return ERROR;
    }
    if(mode > 1)
    {
        printf("设置GPIO的配置模式时参数mode错误，只能取值0~1！\n");
        return ERROR;
    }

    tmp = *(volatile u32 *)GPIO_INT_INTMODE_REG;

    if(mode == 0)
    {
        tmp = tmp & (~(1 << gpioIndex));
        *(volatile u32 *)GPIO_INT_INTMODE_REG = tmp;
    }
    else if(mode == 1)
    {
        tmp = tmp | (1 << gpioIndex);
        *(volatile u32 *)GPIO_INT_INTMODE_REG = tmp;
    }

    return OK;
}
/*
 * 设置GPIO的中断控制，控制每条gpio通道是否同时由上升沿和下降沿产生中断，
 * 0为single，1为both。
 */
int hrGpioSetIntControl(u32 gpioIndex, u32 mode)
{
    u32 tmp = 0;

    if(gpioIndex > 23)
    {
        printf("设置GPIO的配置模式时参数gpioIndex错误，只能取值0~23！\n");
        return ERROR;
    }
    if(mode > 1)
    {
        printf("设置GPIO的配置模式时参数mode错误，只能取值0~1！\n");
        return ERROR;
    }

    tmp = *(volatile u32 *)GPIO_INT_INTBOTHEDGE_REG;

    if(mode == 0)
    {
        tmp = tmp & (~(1 << gpioIndex));
        *(volatile u32 *)GPIO_INT_INTBOTHEDGE_REG = tmp;
    }
    else if(mode == 1)
    {
        tmp = tmp | (1 << gpioIndex);
        *(volatile u32 *)GPIO_INT_INTBOTHEDGE_REG = tmp;
    }

    return OK;
}
/*
 * 设置GPIO的中断控制
 * 0为falling edge or low level，1为rising edge or high level。
 */
int hrGpioSetIntEdge(u32 gpioIndex, u32 mode)
{
    u32 tmp = 0;

    if(gpioIndex > 23 )
    {
        printf("设置GPIO的配置模式时参数gpioIndex错误，只能取值0~23！\n");
        return ERROR;
    }
    if(mode > 1)
    {
        printf("设置GPIO的配置模式时参数mode错误，只能取值0~1！\n");
        return ERROR;
    }

    tmp = *(volatile u32 *)GPIO_INT_INTEDGE_REG;

    if(mode == 0)
    {
        tmp = tmp & (~(1 << gpioIndex));
        *(volatile u32 *)GPIO_INT_INTEDGE_REG = tmp;
    }
    else if(mode == 1)
    {
        tmp = tmp | (1 << gpioIndex);
        *(volatile u32 *)GPIO_INT_INTEDGE_REG = tmp;
    }

    return OK;
}
/*
 * 读取每条gpio通道是否产生过中断。
 * 产生过中断，返回1；没有产生过中断，返回0。
 */
int hrGpioGetRawIntStatus(u32 gpioIndex)
{
    u32 tmp = 0;

    if(gpioIndex > 23)
    {
        printf("设置GPIO的配置模式时参数gpioIndex错误，只能取值0~23 ！\n");
        return ERROR;
    }

    tmp = *(volatile u32 *)GPIO_INT_INTSTATUS_REG;
    tmp = (tmp >> gpioIndex) & 0x1;

    return tmp;
}
/*
 * 读取每条gpio通道产生的中断状态。
 * 返回值0：当前对应的bit位为对应的gpio没有中断；
 * 返回值1：当前对应的bit位为对应的gpio正在中断状态。
 */
int hrGpioGetIntStatus(u32 gpioIndex)
{
    u32 tmp = 0;

    if(gpioIndex > 23)
    {
        printf("设置GPIO的配置模式时参数gpioIndex错误，只能取值0~23！\n");
        return ERROR;
    }

    tmp = *(volatile u32 *)GPIO_INT_MASK_INTSTATUS_REG;
    tmp = (tmp >> gpioIndex) & 0x1;

    return tmp;
}
/*
 * 清除已产生的中断。
 */
int hrGpioClearInt(u32 gpioIndex)
{
    u32 tmp = 0;

    if(gpioIndex > 23)
    {
        printf("设置GPIO的配置模式时参数gpioIndex错误，只能取值0~23！\n");
        return ERROR;
    }

    tmp = (1 << gpioIndex);
    *(volatile u32 *)GPIO_INT_INTCLEAR_REG = tmp;

    return OK;
}

/*
 * 功能：获取GPIO的调试信息
 * 输入：
 *         无。
 * 输出：
 *         无。
 * 返回值：
 *          无。
 */
int hrGpioGetDbgInfo(u32 gpioindex)
{
	printf("GPIO%d Debug Info:\n",gpioindex);

	printf("进中断次数: %d\n",_G_gpioDbgInfo[gpioindex].intNumIn);
	printf("出中断次数: %d\n",_G_gpioDbgInfo[gpioindex].intNumOut);

    return OK;
}

void gpio_setH(void)
{
#if defined(_GPIO_NM_REG_)
    *(volatile u32 *)GPIO_NM0_OUTPUT_ENABLE_REG = 0xffff;   //16位有效寄存器，使能0-15 output

    *(volatile u32 *)GPIO_NM0_MODE_REG = 0;   //16位有效寄存器，使能0-15 soft 配置 :0             hardware:1

    *(volatile u32 *)GPIO_NM0_OUTPUT_DATA_REG = 0xffff;   //16位有效寄存器，使能0-15 拉高
    
    *(volatile u32 *)GPIO_NM1_OUTPUT_ENABLE_REG = 0xffff;   //16位有效寄存器，使能0-15 output

    *(volatile u32 *)GPIO_NM1_MODE_REG = 0;   //16位有效寄存器，使能0-15 soft 配置 :0             hardware:1

    *(volatile u32 *)GPIO_NM1_OUTPUT_DATA_REG = 0xffff;   //16位有效寄存器，使能0-15 拉高
#elif defined(_GPIO_INT_REG_)
    *(volatile u32 *)GPIO_INT_OUTPUT_ENABLE_REG = 0xffff;   //16位有效寄存器，使能0-15 output

    *(volatile u32 *)GPIO_INT_MODE_REG = 0;   //16位有效寄存器，使能0-15 soft 配置 :0             hardware:1

    *(volatile u32 *)GPIO_INT_OUTPUT_DATA_REG = 0xffff;   //16位有效寄存器，使能0-15 拉高
#elif defined(_GPIO_PWM_REG_)
    *(volatile u32 *)GPIO_PWM_OUTPUT_ENABLE_REG = 0xffff;   //16位有效寄存器，使能0-15 output

    *(volatile u32 *)GPIO_PWM_MODE_REG = 0;   //16位有效寄存器，使能0-15 soft 配置 :0             hardware:1

    *(volatile u32 *)GPIO_PWM_OUTPUT_DATA_REG = 0xffff;   //16位有效寄存器，使能0-15 拉高
#endif

    printf("\r\n================high===================\r\n");
    printf("\r\n");
}
void gpio_setL(void)
{
#if defined(_GPIO_NM_REG_)
    *(volatile u32 *)GPIO_NM0_OUTPUT_ENABLE_REG = 0xffff;   //16位有效寄存器，使能0-15 output

    *(volatile u32 *)GPIO_NM0_MODE_REG = 0;   //16位有效寄存器，使能0-15 soft 配置 :0             hardware:1

    *(volatile u32 *)GPIO_NM0_OUTPUT_DATA_REG = 0;   //16位有效寄存器，使能0-15 
    
    *(volatile u32 *)GPIO_NM1_OUTPUT_ENABLE_REG = 0xffff;   //16位有效寄存器，使能0-15 output

    *(volatile u32 *)GPIO_NM1_MODE_REG = 0;   //16位有效寄存器，使能0-15 soft 配置 :0             hardware:1

    *(volatile u32 *)GPIO_NM1_OUTPUT_DATA_REG = 0;   //16位有效寄存器，使能0-15 
#elif defined(_GPIO_INT_REG_)
    *(volatile u32 *)GPIO_INT_OUTPUT_ENABLE_REG = 0xffff;   //16位有效寄存器，使能0-15 output

    *(volatile u32 *)GPIO_INT_MODE_REG = 0;   //16位有效寄存器，使能0-15 soft 配置 :0             hardware:1

    *(volatile u32 *)GPIO_INT_OUTPUT_DATA_REG = 0;   //16位有效寄存器，使能0-15 
#elif defined(_GPIO_PWM_REG_)
    *(volatile u32 *)GPIO_PWM_OUTPUT_ENABLE_REG = 0xffff;   //16位有效寄存器，使能0-15 output

    *(volatile u32 *)GPIO_PWM_MODE_REG = 0;   //16位有效寄存器，使能0-15 soft 配置 :0             hardware:1

    *(volatile u32 *)GPIO_PWM_OUTPUT_DATA_REG = 0;   //16位有效寄存器，使能0-15 
#endif

    printf("\r\n================low===================\r\n");
    printf("\r\n");
}





