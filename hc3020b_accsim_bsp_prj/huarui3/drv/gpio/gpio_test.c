#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <reworksio.h>
#include <irq.h>
#include <reworks/printk.h>
#include "hr3Gpio.h"


#define LEVEL_H			1 /*高电平*/
#define LEVEL_L			0 /*低电平*/
#define DIR_OUT			1 /*输出*/
#define DIR_IN			0 /*输入*/
//#define POLARITY_HIGH	1 /*中断极性*/
//#define POLARITY_LOW	0 
//#define LVL_TRIG		1 /*电平触发*/
//#define	PULSE_TRIG		0 /*脉冲触发*/

//extern void gpio_init(int gpio_num);
//extern void gpio_set_dir(int dir ,int gpio_num);
//extern void gpio_set(int val, int gpio_num);
//extern int gpio_get(int gpio_num);
//extern void gpio_int_enable(int gpio_num, int polarity, int trig_mode);
//extern void gpio_int_disable(int gpio_num);

typedef struct test_info_struct{
	char string[10];
	int number;
} TEST_INFO;

static int test_int_count = 0;
void test_int_handler(void *arg)
{
	TEST_INFO *test_info = (TEST_INFO*)arg;
	test_int_count++;
	
	if(test_int_count > 10)
	{
		test_int_count = 0;
		gpio_int_test_end(test_info->number);
		return;
	}
	
	printk("[%d] This is test_int_handler, %s-%d\n", test_int_count, test_info->string, test_info->number);
	
	return;
}

int gpio_input_test(unsigned int pin_num)
{
	int pin_value = -1;
	int i = 3;

	hrGpioSetTransDirection(pin_num,DIR_IN);// 设置pin为输入方向
	while(i > 0)
	{
		pin_value = hrGpioGetOutputVal(pin_num);
		printf("GPIO[%u] get value is %d\n",pin_num,pin_value);
		sleep(1);
		i--;
	}
	printf("GPIO[%u] input test END\n",pin_num);
	return 0;
}

int gpio_output_test(unsigned int pin_num,int pin_value)
{
	unsigned int value = 0;
	int i = 0;
	
	hrGpioSetTransDirection(pin_num, DIR_OUT);
	value = hrGpioSetOutputVal(pin_num, pin_value);
	printf("GPIO[%u] set value is %d\n",pin_num,pin_value);
	
	printf("GPIO[%u] output test END\n",pin_num);
	return 0;
}

int gpio_int_test(unsigned int pin_num)
{
	static TEST_INFO test_info;
	char string[12] = "gpio";
	memcpy(test_info.string, string, 12);
	test_info.number = pin_num;
	
	hrGpioIntConnect(pin_num, test_int_handler, (void*)&test_info);
	
	//GPIO中断使能
	hrGpioIntEnable(pin_num);
	
	return 0;
}

int gpio_int_test_end(unsigned int pin_num)
{
	//GPIO中断失能
	hrGpioIntDisable(pin_num);
	printk("GPIO[%u] int test END\n",pin_num);
	return 0;
}

/*需要将pin_num1和pin_num2连接在一起*/
void gpio_test(unsigned int pin_num1, unsigned int pin_num2)
{
//	hrGpioInstInit();
	hrGpioIntEnable(19);
	hrGpioIntEnable(22);
	
	printf("  ##### GPIO%u ==> GPIO%u ##### \n",pin_num1, pin_num2);//GPIO19 ==> GPIO18
	gpio_output_test(pin_num1, LEVEL_H);
	gpio_input_test(pin_num2);
	
	gpio_output_test(pin_num1, LEVEL_L);
	gpio_input_test(pin_num2);
	
	gpio_output_test(pin_num1, LEVEL_H);
	gpio_input_test(pin_num2);
	
	gpio_output_test(pin_num1, LEVEL_L);
	gpio_input_test(pin_num2);
	
//	printf("  ##### GPIO%u <== GPIO%u ##### \n",pin_num1, pin_num2);
//	gpio_output_test(pin_num2, LEVEL_H);
//	gpio_input_test(pin_num1);
//	
//	gpio_output_test(pin_num2, LEVEL_L);
//	gpio_input_test(pin_num1);
	
//	printk("  ##### GPIO%u int test ##### \n",pin_num1);
//	hrGpioSetTransDirection(pin_num1,DIR_IN);
//	hrGpioSetTransDirection(pin_num2,DIR_OUT);
//	gpio_int_test(pin_num1);
//	hrGpioSetOutputVal(pin_num2, LEVEL_H);
//	hrGpioSetOutputVal(pin_num2, LEVEL_L);
//	gpio_int_test_end(pin_num1);
	
	return;
}

void gpio_test_init(void)
{
}
