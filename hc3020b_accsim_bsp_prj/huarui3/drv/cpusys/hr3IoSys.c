/******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：华睿 3 号IO相关系统实现接口
 * 修改：
 * 		2022-04-12，建立代码模板
 */

#include "tool/hr3SysTools.h"
#include <stdio.h>

extern unsigned int MIPS_LW64(unsigned int addr);
extern void MIPS_SW64(unsigned int addr, unsigned int data);
extern void MIPS_RD64(unsigned int addrh, unsigned int addrl, unsigned int *datah, unsigned int *datal);
extern void MIPS_WR64(unsigned int addrh, unsigned int addrl, unsigned int *datah, unsigned int *datal);

/*********************************************************************************************************
** 函数名称: sysRead32
** 功能描述: 读 32 位物理地址
** 输　入  : uiAddr 为 32 位物理地址
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
unsigned int sysRead32 (unsigned int  uiAddr)
{
    return MIPS_LW64(uiAddr);
}
/*********************************************************************************************************
** 函数名称: sysWrite32
** 功能描述: 写 32 位物理地址
** 输　入  : uiAddr 为 32 位物理地址
**           uiData 为数据
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
void  sysWrite32 (unsigned int  uiAddr, unsigned int  uiData)
{
    MIPS_SW64(uiAddr, uiData);
}
/*********************************************************************************************************
** 函数名称: sysRead64
** 功能描述: 读 64 位物理地址
** 输　入  : uiAddrh  为高 32 位物理地址
**           uiAddrl  为低 32 位物理地址
**           puiDatah 为获取的数据高 32 位
**           puiDatal 为获取的数据低 32 位
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
void  sysRead64 (unsigned int  uiAddrh, unsigned int  uiAddrl, unsigned int  *puiDatah, unsigned int  *puiDatal)
{
    MIPS_RD64(uiAddrh, uiAddrl, puiDatah, puiDatal);
}
/*********************************************************************************************************
** 函数名称: sysWrite64
** 功能描述: 写 64 位物理地址
** 输　入  : uiAddrh  为高 32 位物理地址
**           uiAddrl  为低 32 位物理地址
**           uiDatah  为设置的数据高 32 位
**           uiDatal  为设置的数据低 32 位
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
void  sysWrite64 (unsigned int  uiAddrh, unsigned int  uiAddrl, unsigned int  uiDatah, unsigned int  uiDatal)
{
    MIPS_WR64(uiAddrh, uiAddrl, uiDatah, uiDatal);
}


/*********************************************************************************************************
** 函数名称: dumpMem
** 功能描述: dump 内存
** 输　入  : int  iArgC, char** ppcArgV  命令行参数
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
#if 0
void  dumpMem (int  iArgC, char** ppcArgV)
{
    addr_t  addr;
    size_t  len;
    int     i;

    if (1 == iArgC) {
        printk("less args, need addr!!\r\n");
        return;
    }

    addr = strtoul(ppcArgV[1], NULL, 16);
    len = 8;

    if (NULL != ppcArgV[2]) {
        len  = atol(ppcArgV[2]);
    }

    if (len > 1000) {
        printk("length too long!!\r\n");
        return;
    }

    if (len < 0) {
        len = 10;
    }

    for (i = 0; i < len; i += 8) {
        printf("addr: %p %02x %02x %02x %02x  %02x %02x %02x %02x\r\n",
                    (void *)addr,  *(PUCHAR)addr, *(PUCHAR)(addr + 1),
                    *(PUCHAR)(addr + 2), *(PUCHAR)(addr + 3),
                    *(PUCHAR)(addr + 4), *(PUCHAR)(addr + 5),
                    *(PUCHAR)(addr + 6), *(PUCHAR)(addr + 7));
        addr = addr + 8;
        }
}
#else

void  dumpMem (int  iArgC, char** ppcArgV)
{
    static unsigned long long dumpMem_addr = 0;
    unsigned long long addr,new_addr,addr_phy;
    size_t  len;
    int     i;
    int ret;

    if (1 == iArgC) {
        if(dumpMem_addr == 0)
        {
            printf("less args, need addr!\ndumpMem usage:]dumpMem baseaddr len\r\n");
            return;
        }
        else
        {
            addr = dumpMem_addr;
            len = 256;
        }
    }
    else
    {
        addr = strtoul(ppcArgV[1], NULL, 16);
        len = 256;

        if (NULL != ppcArgV[2]) {
            len  = atol(ppcArgV[2]);
        }

        if (len > 1024) {
            printf("length too long:%d, use default 256 as len!!\r\n",len);
            len = 256;
        }

        if (len < 0) {
            len = 256;
        }
    }

#if 0
    ret = API_VmmVirtualToPhysical(addr,&addr_phy);
    if(ret != 0)
#else
    addr_phy = hrKmToPhys((void *)addr); // To Fix
#endif
    {
        if((addr <= 0x9000000000000000) || ((addr>= 0xa000000000000000) && (addr <= 0xffffffff00000000)))
        {
            printf("Addr 0x%lx VmmVirtual Page is Invalid!\r\n",addr);
            return;
        }
    }
    if(((addr&0xf000000000000000)>>60)==0x9)
    {
        printf("@@@dumpMem[PhyAddr:%p],len %d\r\n",(addr&0xfffffffffffffff),len);
    }else
    {
        printf("@@@dumpMem[LogAddr:%p-PhyAddr:%p],len %d\r\n",addr,addr_phy,len);
    }
    //add end

    int offset = addr&0xF;
    if(offset != 0)
    {
        printf("%p]",(void *)(addr-offset));

        for(i=0;i<offset;i++)
        {
            printf("xx ");
            if( (i&0x3) == 0x3) printf("| ");
        }
    }
    for(i=0;i<len;i++)
    {
        new_addr = addr+i;

        if( (new_addr&0xF) == 0) printf("[%p]",(void *)(new_addr));
        printf("%02x ",*(unsigned char *)(addr + i));
        if( (new_addr&0x3) == 0x3)
            {
            printf("| ");
            }
        if( (new_addr&0xF) == 0xF)
            {
            printf("\r\n");
            }
    }

    offset = (new_addr+1)&0xF;
    if(offset != 0)
    {
        for(i=offset;i<16;i++)
        {
            printf("xx ");
            if( (i&0x3) == 0x3) printf("| ");
        }
        printf("\r\n");
    }

    dumpMem_addr = addr + len;
}

void  dumpReg (int  iArgC, char** ppcArgV)
{
    unsigned long  addr,new_addr,addr_phy;
    size_t  len;
    int     i;
    int ret;

    if (1 == iArgC) {
        printf("less args, need addr!dumpReg usage:]dumpReg baseaddr len\r\n");
        return;
    }
    else
    {
        addr = strtoul(ppcArgV[1], NULL, 16);
        len = 256;

        if (NULL != ppcArgV[2]) {
            len  = atol(ppcArgV[2]);
        }

        if (len > 1024) {
            printf("length too long:%d,use default 256 as len!!\r\n",len);
            len = 256;
        }

        if (len < 0) {
            len = 256;
        }
    }

    //add by xu 20190709
#if 0
    ret = API_VmmVirtualToPhysical(addr,&addr_phy);
    if(ret != 0)
#else
    addr_phy = hrKmToPhys((void *)addr); // To Fix
#endif
    {
        if((addr <= 0x9000000000000000) || ((addr>= 0xa000000000000000) && (addr <= 0xffffffff00000000)))
        {
            printf("Addr 0x%lx VmmVirtual Page is Invalid!\r\n",addr);
            return;
        }
    }
    if(((addr&0xf000000000000000)>>60)==0x9)
    {
        printf("@@@dumpMem[PhyAddr:%p],len %d\r\n",(addr&0xfffffffffffffff),len);
    }else
    {
        printf("@@@dumpMem[LogAddr:%p-PhyAddr:%p],len %d\r\n",addr,addr_phy,len);
    }
    //add end

    int offset = addr&0xF;
    if(offset != 0)
    {
        printf("%p]",(void *)(addr-offset));

        for(i=0;i<offset;i++)
        {
            printf("xx ");
            if( (i&0x3) == 0x3) printf("| ");
        }
    }

    for(i=0;i<len;i++)
    {
        new_addr = addr+i*4;

        if( (i%4) == 0) printf("[offset:%3d]",i);
        printf("%08x ",*(unsigned int *)(addr + i*4));

        if( (new_addr&0xF) == 0xC)
        {
            printf("| ");
            printf("\r\n");
        }
        else
        {
            printf("| ");
        }
    }
}
#endif

/*********************************************************************************************************
  END
*********************************************************************************************************/
