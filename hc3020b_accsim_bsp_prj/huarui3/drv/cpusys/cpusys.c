/******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：华睿 3 号系统相关实现接口
 * 修改：
 * 		2022-04-12，建立代码模板
 */

#include "cpusys.h"
/*********************************************************************************************************
** 函数名称: sysDspGetConfig
** 功能描述: 读取芯片 DSP 的配置情况。
** 输　入  : NONE
** 输　出  : NONE
** 全局变量:
** 调用模块:
*********************************************************************************************************/
unsigned int  sysDspGetConfig (void)
{
    unsigned int  uiRegValueH = 0;
    unsigned int  uiRegValueL = 0;
    unsigned int  uiDspFreq  = 0;

    uiRegValueH = *(volatile unsigned int *)DSP_FREQ_REG_H;
    uiRegValueL = *(volatile unsigned int *)DSP_FREQ_REG_L;
//    printk("uiRegValueH=0x%x, uiRegValueL=0x%x\n",uiRegValueH,uiRegValueL);

    switch (uiRegValueH) {
//    case 0x8125f:
//        uiDspFreq = 33;
//        break;

    case 0x8185f:
        uiDspFreq = 200;
        break;

    case 0x8165f:
        uiDspFreq = 400;
        break;

    case 0x81677:
        uiDspFreq = 500;
        break;

    case 0x8168f:
        uiDspFreq = 600;
        break;

    case 0x816a7:
        uiDspFreq = 700;
        break;

    case 0x8145f:
        uiDspFreq = 800;
        break;

    case 0x8146b:
        uiDspFreq = 900;
        break;

    case 0x81477:
        uiDspFreq = 1000;
        break;

    case 0x81483:
        uiDspFreq = 1100;
        break;

    case 0x8148f:
        uiDspFreq = 1200;
        break;

    case 0x8149b:
        uiDspFreq = 1300;
        break;
        
    case 0x814a1:
        uiDspFreq = 1350;
        break;

    case 0x810a7:
        uiDspFreq = 1400;
        break;

    case 0x81259:
        uiDspFreq = 1500;
        break;

    case 0x8125f:
        uiDspFreq = 1600;
        break;

    case 0x8126b:
        uiDspFreq = 1800;
        break;
        
    default:
        uiDspFreq = 0;
        break;
    }

//    printk("uiDspFreq = %d,uiRegValue =0x%x\r\n",uiDspFreq,uiRegValueH);
    return  uiDspFreq;
}


unsigned int  sysRaspGetConfig (void)
{
    unsigned int  uiRegValue = 0;
    unsigned int  uiRaspFreq  = 0;

    uiRegValue = *(volatile unsigned int *)RASP_FREQ_REG;
    //printf("uiRegValue = 0x%x\n",uiRegValue);

    switch (uiRegValue) {

    case 0x8168f:
        uiRaspFreq = 600;
        break;

    case 0x8145f:
        uiRaspFreq = 800;
        break;

    case 0x8146b:
        uiRaspFreq = 900;
        break;

    case 0x81477:
        uiRaspFreq = 1000;
        break;

    case 0x8148f:
        uiRaspFreq = 1200;
        break;

    default:
        uiRaspFreq = 0;
        break;
    }
    return  uiRaspFreq;
}
/*********************************************************************************************************
  END
*********************************************************************************************************/
