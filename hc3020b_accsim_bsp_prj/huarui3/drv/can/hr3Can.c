#include <stdlib.h>
#include "hr3Can.h"
#include "tickLib.h" 
#include <stdio.h>
#include "string.h"
#include "../h/tool/hr3SysTools.h"
#include "sysLib.h" 
#include "taskLib.h" 


static u8 g_mode[MAX_NUM_BORAD] = {0,0};
//static BASIC_FRAME g_basic_frame[MAX_NUM_BORAD] = {{0},{0}};

static CAN_BOARD *pBrd[MAX_NUM_BORAD] = {NULL,NULL};
static CAN_PACKET canRecvPacket[MAX_NUM_BORAD];
static int g_CanRecvStatus[MAX_NUM_BORAD] = {0,0};   
static int g_CanRecvLens[MAX_NUM_BORAD] = {0,0};

static SEM_ID canRxSem[MAX_NUM_BORAD];
static pthread_mutex_t canTxSem[MAX_NUM_BORAD];
static pthread_mutex_t canPortSem[MAX_NUM_BORAD];

ssize_t CanSendMsg(void *pBoard, const void *buffer, size_t count);
unsigned int read_can_reg(unsigned long addr)
{
    return *(unsigned int *)addr;
}
void write_can_reg(unsigned long addr,unsigned int val)
{
    *(volatile unsigned int *)addr = val;
}

void Dump_CAN_BOARD(void)
{
	int i;
	for(i=0;i<MAX_NUM_BORAD;i++)
	{
		printk("[%d] controller=0x%x\n",i,pBrd[i]->controller);
		printk("[%d] can_mode=0x%x\n",i,pBrd[i]->port.can_mode);
		printk("[%d] channelAddr=0x%x\n",i,pBrd[i]->channelAddr);
		printk("[%d] irq=0x%x\n",i,pBrd[i]->irq);
		printk("[%d] dev=0x%x\n",i,pBrd[i]->dev);
		printk("[%d] can_echo_flag=0x%x\n",i,pBrd[i]->can_echo_flag);
		printk("[%d] loop_back_mode_flag=0x%x\n",i,pBrd[i]->loop_back_mode_flag);
		printk("[%d] baudRate=0x%x\n",i,pBrd[i]->port.baudRate);
		printk("[%d] canClk=0x%x\n\n",i,pBrd[i]->port.canClk);
	}
	return;
}

/*TODO: 调试结束后记得关闭ISR里的打印*/
/*TODO: 是否需要屏蔽中断？*/
/*TODO: 其他类型的中断目前没做对应处理*/
#if 1
void hr3CanIntHandler(void *pBoard)
{
	CAN_BOARD *can_info = (CAN_BOARD *)pBoard;
    unsigned int reg = 0;
    u8 canId = can_info->controller;
    u8 can_mode = can_info->port.can_mode;
    CAN_PACKET RcvPacket = {0};

    reg = read_can_reg(REG_CAN_INTR(canId));
    
    if(reg & 0x1)/*接收中断*/
    {
        printk("\ncan%d recv int.\r\n",canId);
    	/*收到接收中断，将数据从CAN口读出来后，发送到消息队列，上层read从消息队列取数据*/
        /*can_mode只有当上层调用ioctl设置mode时才会更新*/
        sja_receive_test(canId, (void *)&RcvPacket, can_mode);
        mq_send(can_info->recMsgID,(char *)&RcvPacket,sizeof(RcvPacket),0);
    }
    else if(reg & 0x2)/*发送中断*/
    {
//      printnum(read_can_reg(REG_CAN_EXT_TXERC(canId)));printstr("\r\n");
        printk("\ncan%d send int.\r\n",canId);
    }
    else if(reg & 0x4)/*错误中断*/
    {
        printk("\ncan%d error int.\r\n",canId);
        printk("\ncan%d status: 0x%x\r\n",canId, read_can_reg(REG_CAN_STATUS(canId)));
//        printnum(read_can_reg(REG_CAN_STATUS(canId)));printstr("\r\n");
//      printnum(read_can_reg(REG_CAN_EXT_TXERC(canId)));printstr("\r\n");
//      timeDelay(0x10000);
#if 0
        //取消发送
        write_can_reg(REG_CAN_CMD(canId), 0x2);
        //等到状态寄存器的值变成默认状态
        while(read_can_reg(REG_CAN_STATUS(canId)) & 0xff != 0x0c)
        {
            timeDelay(0x100);
        }
        timeDelay(0x100);

//      printstr("sta:");printnum(read_can_reg(REG_CAN_STATUS(0)));printstr("\r\n");
//      timeDelay(0x100000);
//      printstr("sta:");printnum(read_can_reg(REG_CAN_STATUS(0)));printstr("\r\n");
        set_can_into_reset(canId);
        write_can_reg(REG_CAN_EXT_TXERC(canId), 0x0);
        write_can_reg(REG_CAN_EXT_RXERC(canId), 0x0);
        set_can_into_operating(canId);
#endif
//      printnum(read_can_reg(REG_CAN_EXT_TXERC(canId)));printstr("\r\n");
//      printnum(read_can_reg(REG_CAN_STATUS(canId)));printstr("\r\n");
    }
    else if(reg & 0x8)/*数据溢出中断*/
    {
        printk("\ncan%d data ovreflow int.\r\n",canId);
    }
    else if(reg & 0x20)/*被动错误中断*/
    {
        printk("\ncan%d passive error int.\r\n",canId);
    }
    else if(reg & 0x40)/*丢失仲裁中断*/
    {
        printk("\ncan%d miss arbitrate int.\r\n",canId);
    }
    else if(reg & 0x80)/*总线错误中断*/
    {
        printk("\ncan%d bus error int.\r\n",canId);
//      printstr("sta:");printnum(read_can_reg(REG_CAN_STATUS(canId)));printstr("\r\n");
        if ((read_can_reg(REG_CAN_STATUS(canId)) & 0x20) == 0x20)
        {
            //取消发送
            write_can_reg(REG_CAN_CMD(canId), 0x2);
            //等到状态寄存器的值变成默认状态
//      while(read_can_reg(REG_CAN_STATUS(canId)) & 0xff != 0x0c)
//      {
//          timeDelay(0x100);
//      }
            timeDelay(0x1000);

            set_can_into_reset(canId);
            write_can_reg(REG_CAN_EXT_TXERC(canId), 0x0);
            write_can_reg(REG_CAN_EXT_RXERC(canId), 0x0);
            set_can_into_operating(canId);
        }
        else
        {
#if 1
            write_can_reg(REG_CAN_CMD(canId), 0x4);
//          timeDelay(0x1000);
            set_can_into_reset(canId);
            write_can_reg(REG_CAN_EXT_TXERC(canId), 0x0);
            write_can_reg(REG_CAN_EXT_RXERC(canId), 0x0);
            set_can_into_operating(canId);
#endif
        }
    }
    else
    	printk("\ncan%d Unknow int, reg = 0x%x\r\n",canId,reg);
    
    return;
}
#else
void hrCanInt0()
{
    unsigned int reg = 0;
    unsigned int canId = 0;

    reg = read_can_reg(REG_CAN_INTR(canId));
    //
    if(reg & 0x1)/*接收中断*/
    {
        sja_receive_test(0, (void *)&g_basic_frame[0], g_mode[0]);
        printk("rec0 int.\r\n");
    }
    else if(reg & 0x2)/*发送中断*/
    {
//      printnum(read_can_reg(REG_CAN_EXT_TXERC(0)));printstr("\r\n");
        printk("send int.\r\n");
    }
    else if(reg & 0x4)
    {

        printk("111 error int.\r\n");
//      printnum(read_can_reg(REG_CAN_STATUS(0)));printstr("\r\n");
//      printnum(read_can_reg(REG_CAN_EXT_TXERC(0)));printstr("\r\n");
//      timeDelay(0x10000);
#if 0
        //取消发送
        write_can_reg(REG_CAN_CMD(canId), 0x2);
        //等到状态寄存器的值变成默认状态
        while(read_can_reg(REG_CAN_STATUS(canId)) & 0xff != 0x0c)
        {
            timeDelay(0x100);
        }
        timeDelay(0x100);

//      printstr("sta:");printnum(read_can_reg(REG_CAN_STATUS(0)));printstr("\r\n");
//      timeDelay(0x100000);
//      printstr("sta:");printnum(read_can_reg(REG_CAN_STATUS(0)));printstr("\r\n");
        set_can_into_reset(canId);
        write_can_reg(REG_CAN_EXT_TXERC(canId), 0x0);
        write_can_reg(REG_CAN_EXT_RXERC(canId), 0x0);
        set_can_into_operating(canId);
#endif
//      printnum(read_can_reg(REG_CAN_EXT_TXERC(0)));printstr("\r\n");
//      printnum(read_can_reg(REG_CAN_STATUS(0)));printstr("\r\n");
    }
    else if(reg & 0x8)
    {
        printk("data ovreflow int.\r\n");
    }
    else if(reg & 0x20)
    {
        printk("passive error int.\r\n");
    }
    else if(reg & 0x40)
    {
        printk("miss arbitrate int.\r\n");
    }
    else if(reg & 0x80)
    {
        printk("bus error int.\r\n");
//      printstr("sta:");printnum(read_can_reg(REG_CAN_STATUS(0)));printstr("\r\n");
        if ((read_can_reg(REG_CAN_STATUS(0)) & 0x20) == 0x20)
        {
            //取消发送
            write_can_reg(REG_CAN_CMD(canId), 0x2);
            //等到状态寄存器的值变成默认状态
//      while(read_can_reg(REG_CAN_STATUS(canId)) & 0xff != 0x0c)
//      {
//          timeDelay(0x100);
//      }
            timeDelay(0x1000);

            set_can_into_reset(canId);
            write_can_reg(REG_CAN_EXT_TXERC(canId), 0x0);
            write_can_reg(REG_CAN_EXT_RXERC(canId), 0x0);
            set_can_into_operating(canId);
        }
        else
        {
#if 1
            write_can_reg(REG_CAN_CMD(canId), 0x4);
//          timeDelay(0x1000);
            set_can_into_reset(canId);
            write_can_reg(REG_CAN_EXT_TXERC(canId), 0x0);
            write_can_reg(REG_CAN_EXT_RXERC(canId), 0x0);
            set_can_into_operating(canId);
#endif
        }
    }
}


void hrCanInt1()
{
    unsigned int reg = 0;
    unsigned int canId = 1;

    reg = read_can_reg(REG_CAN_INTR(canId));
    //
    if(reg & 0x1)
    {
        sja_receive_test(1, (void *)&g_basic_frame[1], g_mode[1]);
        printk("rec int.\r\n");
    }
    else if(reg & 0x2)
    {
//      printnum(read_can_reg(REG_CAN_EXT_TXERC(0)));printstr("\r\n");
        printk("send int.\r\n");
    }
    else if(reg & 0x4)
    {
        printk("222 error int.\r\n");
//      printnum(read_can_reg(REG_CAN_STATUS(0)));printstr("\r\n");
//      printnum(read_can_reg(REG_CAN_EXT_TXERC(0)));printstr("\r\n");
//      timeDelay(0x10000);
#if 0
        //取消发送
        write_can_reg(REG_CAN_CMD(canId), 0x2);
        //等到状态寄存器的值变成默认状态
        while(read_can_reg(REG_CAN_STATUS(canId)) & 0xff != 0x0c)
        {
            timeDelay(0x100);
        }
        timeDelay(0x100);

//      printstr("sta:");printnum(read_can_reg(REG_CAN_STATUS(0)));printstr("\r\n");
//      timeDelay(0x100000);
//      printstr("sta:");printnum(read_can_reg(REG_CAN_STATUS(0)));printstr("\r\n");
        set_can_into_reset(canId);
        write_can_reg(REG_CAN_EXT_TXERC(canId), 0x0);
        write_can_reg(REG_CAN_EXT_RXERC(canId), 0x0);
        set_can_into_operating(canId);
#endif
//      printnum(read_can_reg(REG_CAN_EXT_TXERC(0)));printstr("\r\n");
//      printnum(read_can_reg(REG_CAN_STATUS(0)));printstr("\r\n");
    }
    else if(reg & 0x8)
    {
        printk("data ovreflow int.\r\n");
    }
    else if(reg & 0x20)
    {
        printk("passive error int.\r\n");
    }
    else if(reg & 0x40)
    {
        printk("miss arbitrate int.\r\n");
    }
    else if(reg & 0x80)
    {
        printk("bus error int.\r\n");
//      printstr("sta:");printnum(read_can_reg(REG_CAN_STATUS(0)));printstr("\r\n");
        if ((read_can_reg(REG_CAN_STATUS(0)) & 0x20) == 0x20)
        {
            //取消发送
            write_can_reg(REG_CAN_CMD(canId), 0x2);
            //等到状态寄存器的值变成默认状态
//      while(read_can_reg(REG_CAN_STATUS(canId)) & 0xff != 0x0c)
//      {
//          timeDelay(0x100);
//      }
            timeDelay(0x1000);

            set_can_into_reset(canId);
            write_can_reg(REG_CAN_EXT_TXERC(canId), 0x0);
            write_can_reg(REG_CAN_EXT_RXERC(canId), 0x0);
            set_can_into_operating(canId);
        }
        else
        {
#if 1
            write_can_reg(REG_CAN_CMD(canId), 0x4);
//          timeDelay(0x1000);
            set_can_into_reset(canId);
            write_can_reg(REG_CAN_EXT_TXERC(canId), 0x0);
            write_can_reg(REG_CAN_EXT_RXERC(canId), 0x0);
            set_can_into_operating(canId);
#endif
        }
    }
}
#endif


/*中断初始化*/
void hrCanInstInit(void *pBoard)
{
	if(NULL == pBoard)
	{
		printf("<ERROR> [%s:__%d__]:: pBoard is NULL!\n",__FUNCTION__,__LINE__);
		return;
	}
    
	CAN_BOARD *can_info = (CAN_BOARD *)pBoard;
	u8 controller = can_info->controller;
	u8 irq = can_info->irq;
	char int_name[50] = {0};
	sprintf(int_name, "hr3can%d_isr", controller);
//	printf("<DEBUG> [%s:__%d__]:: %s install.\n",__FUNCTION__,__LINE__,int_name);
    int_install_handler(int_name, irq,  1, (INT_HANDLER)hr3CanIntHandler, (void*)pBoard);
	int_enable_pic(irq);

    return;
}


void write_can_mask_bits(unsigned long addr, unsigned int mask, unsigned int val)
{
	unsigned int tmp = 0;

	tmp = read_can_reg(addr);
	tmp = ((tmp) & (~(mask))) | ((val) & (mask));
	write_can_reg(addr, tmp);
} 
/* set can entry reset mode */
void set_can_into_reset(u8 controller)
{
	u32 val = 0;
	val = read_can_reg(REG_CAN_CTRL(controller));
	if (!(val & RESET_MODE))
	{
		write_can_reg(REG_CAN_CTRL(controller), (val | RESET_MODE));
		while(!(RESET_MODE & read_can_reg(REG_CAN_CTRL(controller))));
	}
}
/* set can to operating mode */
void set_can_into_operating(u8 controller)
{
	u32 val = 0;
	val = read_can_reg(REG_CAN_CTRL(controller));
	if (val & RESET_MODE)
	{
		write_can_reg(REG_CAN_CTRL(controller), (val & 0xfe));
//		write_can_reg(REG_CAN_CTRL(controller), ((val & 0xfe) | 0x8));  //afm
//		write_can_reg(REG_CAN_CTRL(controller), ((val & 0xfe) | 0x2));  //listen only
//		write_can_reg(REG_CAN_CTRL(controller), ((val & 0xfe) | 0x4));  //self test mode
		while(0x1 & read_can_reg(REG_CAN_CTRL(controller)));
	}
#if 0
	delay_cnt(200);
	val = read_can_reg(REG_CAN_CTRL(controller));
//	write_can_reg(REG_CAN_CTRL(controller), 0x1e);
	write_can_reg(REG_CAN_CTRL(controller), (val & 0xef));
#endif
}


// 等待至接收buffer中有数据
static void wait_rbs(u8 controller)
{
	u32 val = 0;
	unsigned int bTrue = 1;
	do {
		val = read_can_reg(REG_CAN_STATUS(controller));
		if((val & BIT_RBS))
		{
			break;
		}
	} while (bTrue);
}
static void init_basic_tx_buffer(void *frame, u8 *data, u8 idh, u8 idl, u8 rtr, u8 dlc)
{
	u8 i;
	BASIC_FRAME *p_basic = (BASIC_FRAME *)frame;
	p_basic->idh = idh;
	p_basic->idl = idl;
	p_basic->rtr = rtr;
	p_basic->dlc = dlc;
	if (p_basic->rtr == 0)
	{ //data frame
		for (i = 0 ; i < p_basic->dlc; i++)
		{
			p_basic->data[i] = *(data + i);
		}
	}
}
static void init_peli_tx_buffer(void *frame, u8 *data, u8 ff, u8 rtr, u8 dlc, u8 id1, u8 id2, u8 id3, u8 id4)
{
	u8 i = 0;
	PELICAN_FRAME *p_peli = (PELICAN_FRAME *)frame;
	p_peli->ff = ff;
	p_peli->rtr = rtr;
	p_peli->dlc = dlc;
	p_peli->id1 = id1;
	p_peli->id2 = id2;
	p_peli->id3 = id3;
	p_peli->id4 = id4;
	if (p_peli->rtr == 0)
	{
		for (i = 0 ; i < p_peli->dlc; i++)
		{
			p_peli->data[i] = *(data + i);
		}
	}
}
/* can read receive buffer in operating mode
 * REG_CAN_STATUS[4]: Receive Status should be 1'b0
 * REG_CAN_STATUS[0]: Receive Buffer Status should be 1'b1
 * return: 0: successed, 1: failed
 */
static u8 read_receive_buf(u8 controller, void *frame_type, u8 mode)
{
	u8 i = 0;
	//u32 count = MAX_RETRY_NUM;
 	u32 val;
 	CAN_PACKET *can_packet = (CAN_PACKET*)frame_type;
	BASIC_FRAME *basic;
	PELICAN_FRAME *pelican;
	unsigned int bTrue = 1;

	val = read_can_reg(REG_CAN_STATUS(controller));
	if(!(val & BIT_RBS))
	    return 0;
#if 0
	do {
	//	count--;
	//	if (count == 0)
	//		return 1;
		val = read_can_reg(REG_CAN_STATUS(controller));
		if((val & BIT_RBS))
		{
		//if((!(val & BIT_RS)) && (val & BIT_RBS))
			break;
		}
	} while (bTrue);
#endif
	{
		if (BASICCAN_MODE == mode) 
		{
			basic = &can_packet->basic;
			basic->idh = read_can_reg(REG_CAN_RB_IDH(controller));
			val = read_can_reg(REG_CAN_RB_IDL(controller));
			basic->idl = (((u8)val) & 0xe0) >> 5;
			basic->rtr = (((u8)val) & 0x10) >> 4;
			basic->dlc = (((u8)val) & 0x0f) >> 0;
			if (basic->rtr == 0) //数据帧
			{
				for (i = 0; i < basic->dlc; i++) 
				{
					basic->data[i] = read_can_reg(REG_CAN_RB_DATA(controller, i));
					//printk("%x  ", basic->data[i]);
				}
			}
		} 
		else if (PELICAN_MODE == mode) 
		{
			pelican = &can_packet->pelican;
			val = read_can_reg(REG_CAN_EXT_AC0(controller));
			pelican->ff = (((u8)val) & 0x80) >> 7;
			pelican->rtr = (((u8)val) & 0x40) >> 6;
			pelican->dlc = (((u8)val) & 0x0f) >> 0;
			pelican->id1 = read_can_reg(REG_CAN_EXT_AC1(controller));
			if (pelican->ff == 0) 
			{//sff
				val = read_can_reg(REG_CAN_EXT_AC2(controller));
				pelican->id2 = (((u8)val) & 0xe0) >> 5;
				pelican->id3 = 0;
				pelican->id4 = 0;
				if (pelican->rtr == 0) 
				{
					for (i = 0; i < pelican->dlc; i++) 
					{
						pelican->data[i] = read_can_reg(REG_CAN_SFF_DATA(controller, i));
					}
				}
			} 
			else if (pelican->ff == 1) 
			{//eff
				pelican->id2 = read_can_reg(REG_CAN_EXT_AC2(controller));
				pelican->id3 = read_can_reg(REG_CAN_EXT_AC3(controller));
				val = read_can_reg(REG_CAN_EXT_AM0(controller));
				pelican->id4 = (((u8)val) & 0xf8) >> 3;
				if (pelican->rtr == 0) 
				{
					for (i = 0; i < pelican->dlc; i++) 
					{
						pelican->data[i] = read_can_reg(REG_CAN_EFF_DATA(controller, i));
					}
				}
			}
		}
		return 0;
	}
}
/* can config transmit buffer in operating mode
 * REG_CAN_STATUS[5]: Transmit Status must be idle(1b'0)
 * REG_CAN_STATUS[2]: Transmit Buffer Status must be released(1b'1)
 * return: 0: successed, 1: failed
 */
static u8 write_transmit_buf(u8 controller, void *buffer, u8 mode)
{
	u8 i = 0;
	u8 count = MAX_RETRY_NUM;
	u32 val = 0;
	CAN_PACKET *can_packet = (CAN_PACKET*)buffer;
	BASIC_FRAME *basic;
	PELICAN_FRAME *pelican;
	unsigned int bTrue = 1;
	do {
		count--;
		val = read_can_reg(REG_CAN_STATUS(controller));
		if ((!(val & BIT_TS)) && (val & BIT_TBS))
		{
			break;
		}
	} while (bTrue);
	if (0 == count)
		return 1;
	{
		if (BASICCAN_MODE == mode)
		{
			basic = &can_packet->basic;
			
//			basic->idh = 0x0;
//			basic->idl = 0x1;
//			basic->dlc = 8;
//			basic->rtr = 0;
			
//			printk("\n\nB: send id-%x, rtr-%x, dlc-%x\r\n", (((u32)basic->idh << 8) | (basic->idl << 5)) >> 5, basic->rtr, basic->dlc);
//			for (i = 0; i < basic->dlc; i++) 
//			{
////				basic->data[i] = 0x61 + i;
//				printk("%x  ", basic->data[i]);
//			}
						
			write_can_reg(REG_CAN_TB_IDH(controller), basic->idh);
			write_can_reg(REG_CAN_TB_IDL(controller), ((basic->idl << 5) |
						        (basic->rtr << 4) |
							(basic->dlc << 0)));
			printk("\nB: can%d send id-%x, rtr-%x, dlc-%x\r\n", controller, \
					(((u32)basic->idh << 8) | (basic->idl << 5)) >> 5, basic->rtr, basic->dlc);
			if (basic->rtr == 0)
			{
				for (i = 0; i < basic->dlc; i++)
				{
					write_can_reg(REG_CAN_TB_DATA(controller, i), basic->data[i]);
					printk("0x%x ", basic->data[i]);
				}
				printk("\n");
			}
		}
		else if (PELICAN_MODE == mode)
		{
			pelican = &can_packet->pelican;
			write_can_reg(REG_CAN_EXT_AC0(controller), (pelican->ff << 7) |
							(pelican->rtr << 6 |
							(pelican->dlc << 0)));
			write_can_reg(REG_CAN_EXT_AC1(controller), pelican->id1);
			
			if (pelican->ff == 0)
			{//sff 标准帧
				write_can_reg(REG_CAN_EXT_AC2(controller), (pelican->id2 << 5));
				if (pelican->rtr == 0)
				{//data frame 数据帧
					for (i = 0; i < pelican->dlc; i++)
					{
						write_can_reg(REG_CAN_SFF_DATA(controller, i), pelican->data[i]);
					}
				}
			}
			else
			{//eff 扩展帧
				write_can_reg(REG_CAN_EXT_AC2(controller), pelican->id2);
				write_can_reg(REG_CAN_EXT_AC3(controller), pelican->id3);
				write_can_reg(REG_CAN_EXT_AM0(controller), (pelican->id4 << 3));
				if (pelican->rtr == 0)
				{//data frame 数据帧
					for (i = 0; i < pelican->dlc; i++)
					{
						write_can_reg(REG_CAN_EFF_DATA(controller, i), pelican->data[i]);
					}
				}
			}
#if 0/*DEBUG*/
			if (pelican->ff) 
			{
			    printk("P: can%d send ff-0x%x, rtr-0x%x, dlc-0x%x, id-0x%x\r\n", controller, pelican->ff, pelican->rtr, pelican->dlc,
					(((u32)pelican->id1 << 24) | ((u32)pelican->id2 << 16) | ((u32)pelican->id3 << 8) | (pelican->id4 << 3)) >> 3);
			}
			else if (pelican->ff == 0) 
			{
			    printk("P: can%d send ff-0x%x, rtr-0x%x, dlc-0x%x, id-0x%x\r\n", controller,pelican->ff, pelican->rtr, pelican->dlc,
					(((u32)pelican->id1 << 8) | (pelican->id2 << 5)) >> 5);
			}
			if (pelican->rtr == 0) //数据帧
			{
				for (i = 0; i < pelican->dlc; i++) 
				{
				    printk("0x%x ", pelican->data[i]);
				}
				printk("\n");
			} 
#endif
		}
		return 0;
	}
}

static void send_can_command(u8 controller, u8 cmd)
{
	write_can_reg(REG_CAN_CMD(controller), cmd);
}
static void wait_transmit_completed(u8 controller)
{
	u32 val = 0;
	u32 cnt = 0;
	do {
		val = read_can_reg(REG_CAN_STATUS(controller));
		cnt ++;
		if(cnt > 1000)
		    break;
	} while (!(val & BIT_TCS));
}
static int sja_transmit_test(u8 controller, void *buffer, u8 mode)
{
	CAN_PACKET *can_packet = (CAN_PACKET*)buffer;
	u8 type = (mode == BASICCAN_MODE) ? (can_packet->basic.rtr) : (can_packet->pelican.rtr);
	u8 size = (mode == BASICCAN_MODE) ? (can_packet->basic.dlc) : (can_packet->pelican.dlc);
	
	if((type > 1) || (type < 0))
	{
		printk("<ERROR> [%s:__%d__]:: Error type = %u\n",__FUNCTION__,__LINE__,type);
		return -1;
	}
	if((size > 8) || (size < 0))
	{
		printk("<ERROR> [%s:__%d__]:: Error size = %u\n",__FUNCTION__,__LINE__,size);
		return -1;
	}
	
#if 1
	//将数据写入数据buffer
	if(write_transmit_buf(controller, buffer, mode))
	{
		DBPRINT("wtb failed!\n");
		return -1;
	}
#endif

	//发出发送请求命令
	send_can_command(controller, CMD_TR);
//	send_can_command(controller, CMD_TR | CMD_EXT_SRR);
	wait_transmit_completed(controller);
	return 0;
}

static u8 get_rx_message_count(u8 controller)
{
	return read_can_reg(REG_CAN_EXT_RMC(controller));
}
/* select the corresponding bus timing configuration
 * baudrate:    0 - 5k
 *              1 - 10k
 *              2 - 20k
 *              3 - 50k
 *              4 - 100k
 *              5 - 125k
 *              6 - 250k
 *              7 - 500k
 *              8 - 800k
 *              9 - 1000k
 * canclk:      0 - 16M
 *              1 - 8M
 *              2 - 24M
 */
static void calc_sja_bus_timing(CAN_COMMON_T *can, u8 baudrate, u8 canclk)
{
	//bus timing config:                     sjw   brp   sam   tseg2 tseg1
	static const char canclk_16M[10][5] =  {{0x02, 0x3f, 0x01, 0x07, 0x0f},
											{0x00, 0x31, 0x00, 0x01, 0x0c},
											{0x00, 0x18, 0x00, 0x01, 0x0c},
											{0x00, 0x09, 0x00, 0x01, 0x0c},
											{0x00, 0x04, 0x00, 0x01, 0x0c},
											{0x00, 0x03, 0x00, 0x01, 0x0c},
											{0x00, 0x01, 0x00, 0x01, 0x0c},
											{0x00, 0x00, 0x00, 0x01, 0x0c},
											{0x00, 0x00, 0x00, 0x01, 0x06},
											{0x00, 0x00, 0x00, 0x01, 0x04}};
	static const char canclk_8M[10][5]  =  {{0x00, 0x31, 0x00, 0x01, 0x0c},
											{0x00, 0x18, 0x00, 0x01, 0x0c},
											{0x00, 0x13, 0x00, 0x01, 0x06},
											{0x00, 0x04, 0x00, 0x01, 0x0c},
											{0x00, 0x03, 0x00, 0x01, 0x06},
											{0x00, 0x01, 0x00, 0x01, 0x0c},
											{0x00, 0x00, 0x00, 0x01, 0x0c},
											{0x00, 0x00, 0x00, 0x01, 0x04},
											{0x00, 0x00, 0x00, 0x00, 0x02},
											{0x00, 0x00, 0x00, 0x00, 0x01}};
	static const char canclk_24M[10][5] =  {{0x00, 0x00, 0x00, 0x00, 0x00},
											{0x01, 0x2f, 0x01, 0x07, 0x0f},
											{0x00, 0x3b, 0x00, 0x01, 0x06},
											{0x00, 0x0e, 0x00, 0x01, 0x0c},
											{0x00, 0x0b, 0x00, 0x01, 0x06},
											{0x00, 0x05, 0x00, 0x01, 0x0c},
											{0x00, 0x02, 0x00, 0x01, 0x0c},
											{0x00, 0x01, 0x00, 0x01, 0x08},
											{0x00, 0x00, 0x00, 0x01, 0x0b},
											{0x00, 0x00, 0x00, 0x01, 0x08}};
	switch (canclk) {
			case CANCLK_16M:
					can->bt0.bit_info.sjw = canclk_16M[baudrate][0];
					can->bt0.bit_info.brp = canclk_16M[baudrate][1];
					can->bt1.bit_info.sam = canclk_16M[baudrate][2];
					can->bt1.bit_info.tseg2 = canclk_16M[baudrate][3];
					can->bt1.bit_info.tseg1 = canclk_16M[baudrate][4];
					break;
			case CANCLK_8M:/*0x00, 0x00, 0x00, 0x00, 0x02*/
					can->bt0.bit_info.sjw = canclk_8M[baudrate][0];
					can->bt0.bit_info.brp = canclk_8M[baudrate][1];
					can->bt1.bit_info.sam = canclk_8M[baudrate][2];
					can->bt1.bit_info.tseg2 = canclk_8M[baudrate][3];
					can->bt1.bit_info.tseg1 = canclk_8M[baudrate][4];
					break;
			case CANCLK_24M:
					can->bt0.bit_info.sjw = canclk_24M[baudrate][0];
					can->bt0.bit_info.brp = canclk_24M[baudrate][1];
					can->bt1.bit_info.sam = canclk_24M[baudrate][2];
					can->bt1.bit_info.tseg2 = canclk_24M[baudrate][3];
					can->bt1.bit_info.tseg1 = canclk_24M[baudrate][4];
					break;
			default:
					can->bt0.bit_info.sjw = canclk_16M[baudrate][0];
					can->bt0.bit_info.brp = canclk_16M[baudrate][1];
					can->bt1.bit_info.sam = canclk_16M[baudrate][2];
					can->bt1.bit_info.tseg2 = canclk_16M[baudrate][3];
					can->bt1.bit_info.tseg1 = canclk_16M[baudrate][4];
					break;
	}
	return;
}

/* set sja1000 clock division register, access in reset mode
 * parameter:
 *	clk_div: clock out freq selection
 *	clk_off: setting this bit allows the external CLKOUT pin of the SJA1000 tto be disable
 *	rxinten: set 1 allows the TX1 output to be used as a dedicated receive interrupt output
 *	cbp: set 1 allows to bypass the can input comparator
 * 	mode: basiccan mode or pelican mode
 */
static void set_can_clock_division(u8 controller, u8 clk_div, u8 clk_off, u8 rxinten, u8 cbp, u8 can_mode)
{
	u8 val = 0;
	val = (can_mode << 7) | (cbp << 6) | (rxinten << 5) | (clk_off << 3) | clk_div;
	
	if(can_mode == BASICCAN_MODE)
	{
		write_can_reg(REG_CAN_CLK_DIV(controller), val);
	}
	else if(can_mode == PELICAN_MODE)
	{
		write_can_reg(REG_CAN_EXT_CLK_DIV(controller), val);
	}
	return;
}
/* set can acceptance filter, access only in reser mode
 * parameter:
 * 	ac: acceptance code register val
 * 	am: acceptance mask register val
 * 	mode: basiccan mode or pelican mode
 *
 */
static void set_can_acceptance_filter(u8 controller, u8 *ac, u8 *am, u8 mode)
{
	u8 i = 0;
	if (BASICCAN_MODE == mode)
	{
		write_can_reg(REG_CAN_AC(controller), *ac);
		write_can_reg(REG_CAN_AM(controller), *am);
	}
	else if (PELICAN_MODE == mode)
	{
		for(i = 0; i < 4; i++)
		{
			write_can_reg((REG_CAN_EXT_AC0(controller) + i * 4), *(ac + i));
			write_can_reg((REG_CAN_EXT_AM0(controller) + i * 4), *(am + i));
		}
	}
}
/* set can bus timing
 * 
 * CAN_CLK_REF = 8MHz
 * 
 * */
static void set_can_bus_timing(u8 controller, u8 brp, u8 sjw, u8 tseg1, u8 tseg2, u8 sam)
{
	u8 bt0 = 0;
	u8 bt1 = 0;
	bt0 = (sjw << 6) | brp;/*默认bt0 = (0 << 6) | 0 = 0*/
	bt1 = (sam << 7) | (tseg2 << 4) | tseg1;/*默认 bt1 = (0 << 7) | (0 << 4) | 2 = 0x2*/
	write_can_reg(REG_CAN_BT0(controller), bt0);/*BUS_TIM0 = 0x0*/
	write_can_reg(REG_CAN_BT1(controller), bt1);/*BUS_TIM1 = 0x2*/
}
/* read intr register and clear the intr */
static void clear_can_intr(u8 controller, u8 mode)
{
	u8 val = 0;
	if (BASICCAN_MODE == mode)
	{
		val = read_can_reg(REG_CAN_CTRL(controller));
		val &= ~0x1e;
		write_can_reg(REG_CAN_CTRL(controller), val);
	}
	else if(PELICAN_MODE == mode)
	{
		write_can_reg(REG_CAN_EXT_IE(controller), 0x0);
	}
}
/* enable can interrupt bits
 * ena_bits: interrupt bits we want to enable
 * mode: basic or pelican mode
 */
static void enable_can_intr(u8 controller, u8 ena_bits, u8 mode)
{
	u8 val;
	if (PELICAN_MODE == mode)/*扩展模式*/
	{
		val = read_can_reg(REG_CAN_EXT_IE(controller));
		write_can_reg(REG_CAN_EXT_IE(controller), val | ena_bits);
	}
	else if (BASICCAN_MODE == mode)/*标准模式*/
	{
		val = read_can_reg(REG_CAN_CTRL(controller));
		write_can_reg(REG_CAN_CTRL(controller), val | ena_bits);
//		write_can_mask_bits(REG_CAN_CTRL(controller), 0x1e, ena_bits);
	}
}


/* can init
 * parameter:
 * 	mode: basic mode or entend mode
 */
static void can_init(u8 controller, CAN_COMMON_T *can)
{
	//first set can into reset mode and then config bus time, acc and acm
	set_can_into_reset(controller);
	/*时钟分频*/
	set_can_clock_division(controller, can->cd.bit_info.clk_div, can->cd.bit_info.clk_off, \
			can->cd.bit_info.rxinten, can->cd.bit_info.cbp, can->cd.bit_info.can_mode);
	/*帧过滤*/
	set_can_acceptance_filter(controller, &can->acf.ac[0], &can->acf.am[0], can->cd.bit_info.can_mode);
	/*波特率*/
	set_can_bus_timing(controller, can->bt0.bit_info.brp, can->bt0.bit_info.sjw,
			can->bt1.bit_info.tseg1, can->bt1.bit_info.tseg2, can->bt1.bit_info.sam);
//	set_can_output_control(controller);
	clear_can_intr(controller, can->cd.bit_info.can_mode);
	enable_can_intr(controller, can->intr_en, can->cd.bit_info.can_mode); //使能部分中断
	//set can to work mode
	set_can_into_operating(controller);
}

void init_sja1000_basic_fpga(u8 controller,u8 baudrate, u8 canclk)
{
	CAN_COMMON_T sja_info;
//	sja_info.cd.v = 0x00;//basic can mode
	sja_info.cd.bit_info.can_mode = BASICCAN_MODE;
	calc_sja_bus_timing(&sja_info, baudrate, canclk);
	sja_info.acf.ac[0] = pBrd[controller]->port.accCode[0];
	sja_info.acf.am[0] = pBrd[controller]->port.accMask[0];
	sja_info.intr_en = BIT_RIE | BIT_TIE | BIT_EIE | BIT_OIE | BIT_RR;
	
	can_init(controller, &sja_info);
//	g_mode[controller] = 0;
	
    pBrd[controller]->port.baudRate = baudrate;
    pBrd[controller]->port.canClk = canclk;
    pBrd[controller]->port.brp = sja_info.bt0.bit_info.brp;
    pBrd[controller]->port.sjw = sja_info.bt0.bit_info.sjw;
    pBrd[controller]->port.sam = sja_info.bt1.bit_info.tseg1;
    pBrd[controller]->port.tseg1 = sja_info.bt1.bit_info.tseg2;
    pBrd[controller]->port.tseg2 = sja_info.bt1.bit_info.sam;
	return;
}
void sja_receive_test(u8 controller, void *frame, u8 mode)
{
	u8 i = 0;
	u8 j = 0;
	u8 cnt = 0;
	CAN_PACKET *can_packet = (CAN_PACKET *)frame;
	
	BASIC_FRAME *p_basic = &can_packet->basic;
//	BASIC_FRAME basic;
	PELICAN_FRAME *p_peli;
//	PELICAN_FRAME peli;

//	u8 rtr_data[8] = {1, 2, 3, 4, 5, 6, 7, 8};/*TEST*/

	//等待直到接收buffer中有数据
	//wait_rbs(controller);

	if (BASICCAN_MODE == mode) 
	{
#define BASIC_MAX_READ_CNT 10
		while ((read_can_reg(REG_CAN_STATUS(controller)) & BIT_RBS) /*&& cnt <= BASIC_MAX_READ_CNT*/) 
		{
			read_receive_buf(controller, can_packet, mode);
			send_can_command(controller, CMD_RRB); //释放接收buffer空间
			p_basic = &can_packet->basic;
			printk("B: can%d recv id-0x%x, rtr-0x%x, dlc-0x%x\r\n", controller,\
					(((u32)p_basic->idh << 8) | (p_basic->idl << 5)) >> 5, p_basic->rtr, p_basic->dlc);
			if (p_basic->rtr == 0) //数据帧
			{
				for (i = 0; i < p_basic->dlc; i++) 
				{
				    printk("0x%x  ", p_basic->data[i]);
				}
				printk("\n");
			} 
#if 0/*TEST*/
			else //远程帧
			{
				init_basic_tx_buffer((void *)&basic, rtr_data, 0xef, 0x1, 0x0, p_basic->dlc);
				sja_transmit_test(controller, (void *)&basic, BASICCAN_MODE);
			}
#endif
			cnt++;
		}
	} 
	else if (PELICAN_MODE == mode) 
	{
		cnt = get_rx_message_count(controller);
		while (j < cnt) 
		{
			read_receive_buf(controller, can_packet, mode);
			send_can_command(controller, CMD_RRB); //释放接收buffer空间

			p_peli = &can_packet->pelican;
			if (p_peli->ff) 
			{
			    printk("P: can%d recv ff-0x%x, rtr-0x%x, dlc-0x%x, id-0x%x\r\n", controller,p_peli->ff, p_peli->rtr, p_peli->dlc,
					(((u32)p_peli->id1 << 24) | ((u32)p_peli->id2 << 16) | ((u32)p_peli->id3 << 8) | (p_peli->id4 << 3)) >> 3);
			}
			else if (p_peli->ff == 0) 
			{
			    printk("P: can%d recv ff-0x%x, rtr-0x%x, dlc-0x%x, id-0x%x\r\n", controller,p_peli->ff, p_peli->rtr, p_peli->dlc,
					(((u32)p_peli->id1 << 8) | (p_peli->id2 << 5)) >> 5);
			}
			if (p_peli->rtr == 0) //数据帧
			{
				for (i = 0; i < p_peli->dlc; i++) 
				{
				    printk("0x%x ", p_peli->data[i]);
				}
				printk("\n");
			} 
#if 0/*TEST*/
			else //远程帧
			{
				init_peli_tx_buffer((void *)&peli, rtr_data, 0x0, 0x0, p_peli->dlc, 0xa6, 0x23, 0x0, 0x0);
				sja_transmit_test(controller, (void *)&peli, PELICAN_MODE);
			}
#endif
			j++;
		}
	}

	//printk("\r\n");
	return;
}

void init_sja1000_pelican_fpga(u8 controller, u8 baudrate, u8 canclk)
{
	u8 i = 0;
	CAN_COMMON_T sja_info;
//	sja_info.cd.v = 0x0;
	sja_info.cd.bit_info.can_mode = PELICAN_MODE;
	sja_info.bt1.bit_info.sam = 1;
	calc_sja_bus_timing(&sja_info, baudrate, canclk);
	for (i = 0; i < 4; i++)
	{
		sja_info.acf.ac[i] = pBrd[controller]->port.accCode[i];/*验收码*/
		sja_info.acf.am[i] = pBrd[controller]->port.accMask[i];/*验收掩码*/
	}
	sja_info.intr_en = BIT_EXT_RIE | BIT_EXT_TIE | BIT_EXT_EIE \
				| BIT_EXT_DOIE | BIT_EXT_WUIE | BIT_EXT_EPIE | BIT_EXT_ALIE | BIT_EXT_BEIE;
	
	can_init(controller, &sja_info);
//	g_mode[controller] = 1;
	
    pBrd[controller]->port.baudRate = baudrate;
    pBrd[controller]->port.canClk = canclk;
    pBrd[controller]->port.brp = sja_info.bt0.bit_info.brp;
    pBrd[controller]->port.sjw = sja_info.bt0.bit_info.sjw;
    pBrd[controller]->port.sam = sja_info.bt1.bit_info.tseg1;
    pBrd[controller]->port.tseg1 = sja_info.bt1.bit_info.tseg2;
    pBrd[controller]->port.tseg2 = sja_info.bt1.bit_info.sam;

	return;
}


int hrCanInit(unsigned int controller,unsigned int mode,unsigned char baudRate, unsigned char canClk)
{
	if((controller > 1) || (controller < 0))
	{
		printk("<ERROR> [%s:__%d__]:: Error controller = %u\n",__FUNCTION__,__LINE__,controller);
		return -1;
	}
	if((BASICCAN_MODE != mode) && (PELICAN_MODE != mode))
	{
		printk("<ERROR> [%s:__%d__]:: Error mode = %u\n",__FUNCTION__,__LINE__,mode);
		return -1;
	}
	if((baudRate > 9) || (baudRate < 0))
	{
		printk("<ERROR> [%s:__%d__]:: Error baudRate = %u\n",__FUNCTION__,__LINE__,baudRate);
		return -1;
	}
	if((canClk > 2) || (canClk < 0))
	{
		printk("<ERROR> [%s:__%d__]:: Error canClk = %u\n",__FUNCTION__,__LINE__,canClk);
		return -1;
	}
	if(mode == BASICCAN_MODE)
	{
		init_sja1000_basic_fpga(controller, baudRate, canClk);
	}
	else if(mode == PELICAN_MODE)
	{
		init_sja1000_pelican_fpga(controller, baudRate, canClk);
	}

	return 0;
}

/*从消息队列取数据*/
int can_RxMsg(void *pBoard, int timeout)
{
	struct timespec ts;
	struct mq_attr mqstat;
	CAN_BOARD *can_info = (CAN_BOARD*)pBoard;
	u8 controller = can_info->controller;
	u8 can_mode = can_info->port.can_mode;
	CAN_PACKET RcvPacket = {0};

	g_CanRecvLens[controller] = 0;
	
	if (timeout < 0)
	{
//		printk("<DEBUG> [%s:__%d__]\n",__FUNCTION__,__LINE__);
		if(mq_receive(can_info->recMsgID,(char *)&RcvPacket, MAX_MSG_LEN, 0) < 0)
		{
//			printk("<DEBUG> [%s:__%d__]\n",__FUNCTION__,__LINE__);
	      	if (mq_getattr(can_info->recMsgID, &mqstat) == -1) 
			{
				printk("can%d Cannot get attr of recMsgID!\n", controller);
				g_CanRecvStatus[controller] = -1;
				return -1;
			}
			if (mqstat.mq_flags & O_NONBLOCK)
			{
				g_CanRecvStatus[controller] = 0;	//非阻塞 未收到 为0
				return 0;
			}
			else
			{
				printk("can%d error:mq_receive\n",controller);
				g_CanRecvStatus[controller] = -1;
				return -1;/*接收超时*/
			}
		}else{
//			printk("<DEBUG> [%s:__%d__]\n",__FUNCTION__,__LINE__);
			g_CanRecvStatus[controller] = 1;	//阻塞非阻塞 收到 均为1
		}
	}
	else
	{
		ts.tv_sec = timeout/sys_clk_rate_get();
		ts.tv_nsec = 0;
		if(mq_timedreceive(can_info->recMsgID,(char *)&RcvPacket,MAX_MSG_LEN,0,&ts) < 0)
		{
			g_CanRecvStatus[controller] = -1;	
			return -1;/*接收超时*/
		}else{
			g_CanRecvStatus[controller] = 1;
		}
	}
	
	if(BASICCAN_MODE == can_mode)
	{
		canRecvPacket[controller].basic.rtr = RcvPacket.basic.rtr;
		canRecvPacket[controller].basic.dlc = RcvPacket.basic.dlc;
		canRecvPacket[controller].basic.idh = RcvPacket.basic.idh;
		canRecvPacket[controller].basic.idl = RcvPacket.basic.idl;
		g_CanRecvLens[controller] = RcvPacket.basic.dlc;
		memcpy(canRecvPacket[controller].basic.data, RcvPacket.basic.data, RcvPacket.basic.dlc);
	}else{
		canRecvPacket[controller].pelican.ff = RcvPacket.pelican.ff;
		canRecvPacket[controller].pelican.rtr = RcvPacket.pelican.rtr;
		canRecvPacket[controller].pelican.dlc = RcvPacket.pelican.dlc;
		canRecvPacket[controller].pelican.id1 = RcvPacket.pelican.id1;
		canRecvPacket[controller].pelican.id2 = RcvPacket.pelican.id2;
		canRecvPacket[controller].pelican.id3 = RcvPacket.pelican.id3;
		canRecvPacket[controller].pelican.id4 = RcvPacket.pelican.id4;
		g_CanRecvLens[controller] = RcvPacket.pelican.dlc;
		memcpy(canRecvPacket[controller].pelican.data, RcvPacket.pelican.data, RcvPacket.pelican.dlc);
	}
//	printk("<DEBUG> [%s:__%d__]:: g_CanRecvLens[%d] = %d\n",__FUNCTION__,__LINE__,controller,g_CanRecvLens[controller]);
	return (g_CanRecvLens[controller]);
}

int CanGetReceiveEvent(void *pBoard, int timeout)
{
	int retCode;
	CAN_BOARD *can_info = (CAN_BOARD*)pBoard;
	u8 controller = can_info->controller;
	struct mq_attr mqstat;
	
	/*从消息队列取数据放到canRecvPacket*/
//	printk("<DEBUG> [%s:__%d__]\n",__FUNCTION__,__LINE__);
	retCode = can_RxMsg(can_info, timeout);
//	printk("<DEBUG> [%s:__%d__]\n",__FUNCTION__,__LINE__);

	if (mq_getattr(can_info->recMsgID, &mqstat) == -1) 
	{
		semGive(canRxSem[controller]);
		printk("can%d Cannot get attr of recMsgID!\n", controller);
		return -1;
	}
	if (mqstat.mq_flags & O_NONBLOCK)
	{
		semGive(canRxSem[controller]);
		return retCode;
	}
	else
	{
		if(0 == retCode && g_CanRecvStatus[controller] != 1)	//排除阻塞模式下收到len=0的帧的情况
			printk("can%d mode is not set or mode is TRANSMIT!\n",controller);
		if(OS_ERROR == retCode)
			printk("can%d receive timeout!\n",controller);
	}

//	printk("<DEBUG> [%s:__%d__]\n",__FUNCTION__,__LINE__);
	semGive(canRxSem[controller]);
//	printk("<DEBUG> [%s:__%d__]\n",__FUNCTION__,__LINE__);
	
	return retCode;
}

#if 0
int hrCanRec(void *pBoard, void *buffer)
{
	int datalen;
	CAN_BOARD *can_info = (CAN_BOARD*)pBoard;
	u8 controller = can_info->controller; 
	CAN_PACKET *packet = (CAN_PACKET*)buffer;
	u8 mode = can_info->port.can_mode;
	
	memset(packet, 0, sizeof(CAN_PACKET));
	
	if((controller > 1) || (controller < 0))
	{
		printk("CAN接收数据时，参数 controller 错误\n");
		return -1;
	}
	if((mode > 1) || (mode < 0))
	{
		printk("CAN接收数据时，参数 mode 错误\n");
		return -1;
	}
	//TODO: 实际应用时将该函数中的查询缓存中有无数据改为获取接收中断的信号量
//	sja_receive_test(controller, buffer, mode);
	
	/*上层read从消息队列取数据*/
	datalen = CanGetReceiveEvent(pBoard, -1);
	semTake(canRxSem[controller], -1);
	
	if(0 == g_CanRecvStatus[controller])//当通过ioctl(fd,CAN_SET_READ_MOD,O_NONBLOCK)改为非阻塞时 就会返回0 所以这边返回给上层read的值为0
		return 0;
	if(OS_ERROR == g_CanRecvStatus[controller])	//返回-1时才表示接收错误 返回-1
	{
		printk("can%d hrCanRec error!\n",controller);	
		return -1;
	}
	
	if(BASICCAN_MODE == mode)
	{
		packet->basic.rtr = canRecvPacket[controller].basic.rtr;
		packet->basic.dlc = canRecvPacket[controller].basic.dlc;
		packet->basic.idh = canRecvPacket[controller].basic.idh;
		packet->basic.idl = canRecvPacket[controller].basic.idl;
		memcpy(packet->basic.data, canRecvPacket[controller].basic.data, canRecvPacket[controller].basic.dlc);
	}else{
		packet->pelican.ff = canRecvPacket[controller].pelican.ff;
		packet->pelican.rtr = canRecvPacket[controller].pelican.rtr;
		packet->pelican.dlc = canRecvPacket[controller].pelican.dlc;
		packet->pelican.id1 = canRecvPacket[controller].pelican.id1;
		packet->pelican.id2 = canRecvPacket[controller].pelican.id2;
		packet->pelican.id3 = canRecvPacket[controller].pelican.id3;
		packet->pelican.id4 = canRecvPacket[controller].pelican.id4;
		memcpy(packet->pelican.data, canRecvPacket[controller].pelican.data, canRecvPacket[controller].pelican.dlc);
	}
	
#if 1	//实现驱动层的ECHO功能
	if(can_info->can_echo_flag)	//增加loopback模式的切换  通过ioctl来控制  ioctl(fd,4,1)设置回环模式   ioctl(fd,4,0)退出回环模式
		CanSendMsg(can_info, packet, 0);
#endif

//	printk("<DEBUG> [%s:__%d__]:: datalen = %d\n",__FUNCTION__,__LINE__,datalen);
	return datalen;
}

int hrCanSend(void *pBoard, const void *buffer)
{
	CAN_BOARD *can_info = (CAN_BOARD*)pBoard;
	u8 controller = can_info->controller;
	u8 mode = can_info->port.can_mode;

	if((controller > 1) || (controller < 0))
	{
		printk("<ERROR> [%s:__%d__]:: Error controller = %u\n",__FUNCTION__,__LINE__,controller);
		return -1;
	}
	if((mode > 1) || (mode < 0))
	{
		printk("<ERROR> [%s:__%d__]:: Error mode = %u\n",__FUNCTION__,__LINE__,mode);
		return -1;
	}
	if(buffer == NULL)
	{
		printk("<ERROR> [%s:__%d__]:: Parameter error! buffer is NULL\n",__FUNCTION__,__LINE__);
		return -1;
	}


	pthread_mutex_lock(&canTxSem[controller]);
	
	sja_transmit_test(controller, buffer, mode);

	pthread_mutex_unlock(&canTxSem[controller]);
	
	return 0;
}
#endif

/********************************** CAN file operations *************************************************/
void *CanOpenDriver(void *pBoard, const char *pathname, int flags, mode_t mode)
{
//	printf("<DEBUG> [%s:__%d__]:: IN\n",__FUNCTION__,__LINE__);
	CAN_BOARD *can_info = (CAN_BOARD *)pBoard;
	u8 controller = can_info->controller;
#if 0
	CAN_COMMON_T sja_info;
	u8 val;
	
//	u8 can_mode = BASICCAN_MODE;//每次open默认标准模式，需要通过ioctl进行修改
	u8 can_mode = PELICAN_MODE;
	can_info->port.can_mode = can_mode;
	
	if(can_mode == BASICCAN_MODE)
	{
		val = read_can_reg(REG_CAN_CLK_DIV(controller));
		val &= ~(0x1<<7);
		write_can_reg(REG_CAN_CLK_DIV(controller), val | (can_mode << 7));
	}
	else if(can_mode == PELICAN_MODE)
	{
		val = read_can_reg(REG_CAN_EXT_CLK_DIV(controller));
		val &= ~(0x1<<7);
		write_can_reg(REG_CAN_EXT_CLK_DIV(controller), val | (can_mode << 7));
	}

	
	if(can_mode == BASICCAN_MODE)
	{
		sja_info.intr_en = BIT_RIE | BIT_TIE | BIT_EIE | BIT_OIE | BIT_RR;
	}
	else if(can_mode == PELICAN_MODE)
	{
		sja_info.intr_en = BIT_EXT_RIE | BIT_EXT_TIE | BIT_EXT_EIE \
			| BIT_EXT_DOIE | BIT_EXT_WUIE | BIT_EXT_EPIE | BIT_EXT_ALIE | BIT_EXT_BEIE;
	}

	set_can_into_reset(controller);
//	clear_can_intr(controller, can_mode);
	enable_can_intr(controller, sja_info.intr_en, can_mode);
	set_can_into_operating(controller);
#endif
	
//	g_mode[controller] = can_mode;
	g_CanRecvStatus[controller] = 0;
	g_CanRecvLens[controller] = 0;
	memset(canRecvPacket, 0, sizeof(canRecvPacket));
	
//	int_enable_pic(can_info->irq);
	
//	printf("<DEBUG> [%s:__%d__]:: OUT\n",__FUNCTION__,__LINE__);
	return ((void *)can_info);
}

int CanCloseDriver(void *pBoard)
{
//	printf("<DEBUG> [%s:__%d__]:: IN\n",__FUNCTION__,__LINE__);
	
	CAN_BOARD *can_info = (CAN_BOARD *)pBoard;
	u8 controller = can_info->controller;
#if 0
	/* Put the controller into reset mode */
	set_can_into_reset(controller);
	
	/* Disable receive interrupt */
	clear_can_intr(controller, can_info->port.can_mode);
#endif	
	//	int_disable_pic(can_info->irq);
	
	g_CanRecvStatus[controller] = 0;
	g_CanRecvLens[controller] = 0;
	memset(canRecvPacket, 0, sizeof(canRecvPacket));
	
//	printf("<DEBUG> [%s:__%d__]:: OUT\n",__FUNCTION__,__LINE__);
	return 0;
}

ssize_t CanRecMsg(void *pBoard, void *buffer, size_t count)
{
	int datalen;
	CAN_BOARD *can_info = (CAN_BOARD*)pBoard;
	u8 controller = can_info->controller; 
	CAN_PACKET *packet = (CAN_PACKET*)buffer;
	u8 mode = can_info->port.can_mode;
	
	memset(packet, 0, sizeof(CAN_PACKET));
	
	if((controller > 1) || (controller < 0))
	{
		printk("<ERROR> [%s:__%d__]:: Error controller = %u\n",__FUNCTION__,__LINE__,controller);
		return -1;
	}
	if((BASICCAN_MODE != mode) && (PELICAN_MODE != mode))
	{
		printk("<ERROR> [%s:__%d__]:: Error mode = %u\n",__FUNCTION__,__LINE__,mode);
		return -1;
	}
	//TODO: 实际应用时将该函数中的查询缓存中有无数据改为获取接收中断的信号量
//	sja_receive_test(controller, buffer, mode);
	
	/*上层read从消息队列取数据*/
	datalen = CanGetReceiveEvent(pBoard, -1);
	semTake(canRxSem[controller], -1);
	
	if(0 == g_CanRecvStatus[controller])//当通过ioctl(fd,CAN_SET_READ_MOD,O_NONBLOCK)改为非阻塞时 就会返回0 所以这边返回给上层read的值为0
		return 0;
	if(OS_ERROR == g_CanRecvStatus[controller])	//返回-1时才表示接收错误 返回-1
	{
		printk("can%d hrCanRec error!\n",controller);	
		return -1;
	}
	
	if(BASICCAN_MODE == mode)
	{
		packet->basic.rtr = canRecvPacket[controller].basic.rtr;
		packet->basic.dlc = canRecvPacket[controller].basic.dlc;
		packet->basic.idh = canRecvPacket[controller].basic.idh;
		packet->basic.idl = canRecvPacket[controller].basic.idl;
		memcpy(packet->basic.data, canRecvPacket[controller].basic.data, canRecvPacket[controller].basic.dlc);
	}else{
		packet->pelican.ff = canRecvPacket[controller].pelican.ff;
		packet->pelican.rtr = canRecvPacket[controller].pelican.rtr;
		packet->pelican.dlc = canRecvPacket[controller].pelican.dlc;
		packet->pelican.id1 = canRecvPacket[controller].pelican.id1;
		packet->pelican.id2 = canRecvPacket[controller].pelican.id2;
		packet->pelican.id3 = canRecvPacket[controller].pelican.id3;
		packet->pelican.id4 = canRecvPacket[controller].pelican.id4;
		memcpy(packet->pelican.data, canRecvPacket[controller].pelican.data, canRecvPacket[controller].pelican.dlc);
	}
	
#if 1	//实现驱动层的ECHO功能
	if(can_info->can_echo_flag)	//增加loopback模式的切换  通过ioctl来控制  ioctl(fd,4,1)设置回环模式   ioctl(fd,4,0)退出回环模式
		CanSendMsg(can_info, packet, 0);
#endif

//	printk("<DEBUG> [%s:__%d__]:: datalen = %d\n",__FUNCTION__,__LINE__,datalen);
	return datalen;
}

ssize_t CanSendMsg(void *pBoard, const void *buffer, size_t count)
{
	CAN_BOARD *can_info = (CAN_BOARD*)pBoard;
	u8 controller = can_info->controller;
	u8 mode = can_info->port.can_mode;

	if((controller > 1) || (controller < 0))
	{
		printk("<ERROR> [%s:__%d__]:: Error controller = %u\n",__FUNCTION__,__LINE__,controller);
		return -1;
	}
	if((BASICCAN_MODE != mode) && (PELICAN_MODE != mode))
	{
		printk("<ERROR> [%s:__%d__]:: Error mode = %u\n",__FUNCTION__,__LINE__,mode);
		return -1;
	}
	if(buffer == NULL)
	{
		printk("<ERROR> [%s:__%d__]:: Parameter error! buffer is NULL\n",__FUNCTION__,__LINE__);
		return -1;
	}


	pthread_mutex_lock(&canTxSem[controller]);
	
	sja_transmit_test(controller, buffer, mode);

	pthread_mutex_unlock(&canTxSem[controller]);
	
	return 0;
}

int CanConfigPort(void *pBoard, int cmd, void *arg)
{
//	printk("<DEBUG> [%s:__%d__]:: IN\n",__FUNCTION__,__LINE__);

	CAN_BOARD *can_info = (CAN_BOARD *)pBoard;
	PORT_STRUCT *can_port;
	u8 controller = can_info->controller;
	u8 can_mode;
	u8 brp,tseg1,tseg2;
	u8 sjw,sam;
	u8 val;
	u8 accCode;
	u8 accMask;
	u32	canClk;
	u32	baudRate;
	struct mq_attr mqstat;
	CAN_COMMON_T sja_info;
	int i;
	
	pthread_mutex_lock(&canPortSem[controller]);
	
    switch (cmd)
	{
#if 0
    	case CAN_SET_MODE:	
		{	
			/*0-标准模式， 1-扩展模式, 注意这里是指 标准or扩展 模式，而不是 标准or扩展 帧，
		     *标准模式下有2种帧格式：标准数据帧、标准远程帧，
		     *扩展模式下有4种帧格式：标准数据帧、标准远程帧 or 扩展数据帧、扩展远程帧*/
			
			/*ioctl(fd, CAN_SET_MODE, BASICCAN_MODE);*/
			printk("<DEBUG> [%s:__%d__]:: CAN_SET_MODE\n",__FUNCTION__,__LINE__);
			
			can_mode = (u8)arg;
			if((BASICCAN_MODE != can_mode) && (PELICAN_MODE != can_mode))
			{
				printk("<ERROR> [%s:__%d__]:: can%d set mode 0x%x error\n",__FUNCTION__,__LINE__,controller,can_mode);
				pthread_mutex_unlock(&canPortSem[controller]);
				return -1;
			}
			else
			{
//				int_disable_pic(can_info->irq);
				
				if(can_mode == BASICCAN_MODE)
				{
					val = read_can_reg(REG_CAN_CLK_DIV(controller));
					val &= ~(0x1<<7);
					write_can_reg(REG_CAN_CLK_DIV(controller), val | (can_mode << 7));
				}
				else if(can_mode == PELICAN_MODE)
				{
					val = read_can_reg(REG_CAN_EXT_CLK_DIV(controller));
					val &= ~(0x1<<7);
					write_can_reg(REG_CAN_EXT_CLK_DIV(controller), val | (can_mode << 7));
				}
				
				can_info->port.can_mode = can_mode;/*更新mode*/
//				int_enable_pic(can_info->irq);
			}
			
			break;
		}
#endif
		case CAN_SET_FILTER: /*设置CAN模块的接收滤波方式及值,1表示是单滤波方式，0表示双滤波方式*/
		{
			printk("<DEBUG> [%s:__%d__]:: CAN_SET_FILTER\n",__FUNCTION__,__LINE__);
			
			can_port = (PORT_STRUCT*)arg;
			can_mode = can_info->port.can_mode;
			for(i=0;i<4;i++)
			{
				can_info->port.accCode[i] = can_port->accCode[i];
				can_info->port.accMask[i] = can_port->accMask[i];
			}
			set_can_into_reset(controller);
			set_can_acceptance_filter(controller, &can_port->accCode[0], &can_port->accMask[0], can_mode);
			set_can_into_operating(controller);
			
			break;
		}
		case CAN_SET_BAUDRATE: //设置波特率
		{
			/* * baudrate:    0 - 5k
				 *              1 - 10k
				 *              2 - 20k
				 *              3 - 50k
				 *              4 - 100k
				 *              5 - 125k
				 *              6 - 250k
				 *              7 - 500k
				 *              8 - 800k
				 *              9 - 1000k*/
			printk("<DEBUG> [%s:__%d__]:: CAN_SET_BAUDRATE\n",__FUNCTION__,__LINE__);
			
			can_mode = can_info->port.can_mode;
			baudRate = (u32)arg;

			set_can_into_reset(controller);
			calc_sja_bus_timing(&sja_info, baudRate, can_info->port.canClk);
			set_can_bus_timing(controller, sja_info.bt0.bit_info.brp, sja_info.bt0.bit_info.sjw,
					sja_info.bt1.bit_info.tseg1, sja_info.bt1.bit_info.tseg2, sja_info.bt1.bit_info.sam);
			set_can_into_operating(controller);
			
			can_info->port.baudRate = baudRate;
			can_info->port.brp = sja_info.bt0.bit_info.brp;
			can_info->port.sjw = sja_info.bt0.bit_info.sjw;
			can_info->port.sam = sja_info.bt1.bit_info.tseg1;
			can_info->port.tseg1 = sja_info.bt1.bit_info.tseg2;
			can_info->port.tseg2 = sja_info.bt1.bit_info.sam;
			
			break;
		}
		case CAN_SET_READ_MOD: //设置从消息队列取数据 阻塞或非阻塞
		{	/*ioctl(fd,CAN_SET_READ_MOD,O_NONBLOCK);	//进入非阻塞模式 
			 * ioctl(fd,CAN_SET_READ_MOD,0);	//退出非阻塞模式 */

			printk("<DEBUG> [%s:__%d__]:: CAN_SET_READ_MOD\n",__FUNCTION__,__LINE__);
			
			if (mq_getattr(can_info->recMsgID, &mqstat) == -1) 
			{
				printk("<ERROR> [%s:__%d__]:: can%d Cannot get attr of recMsgID!\n",__FUNCTION__,__LINE__,controller);
				pthread_mutex_unlock(&canPortSem[controller]);
				return -1;
			}
			mqstat.mq_flags &= ~O_NONBLOCK;
			mqstat.mq_flags |= ((u32)arg & O_NONBLOCK);
			if (mq_setattr(can_info->recMsgID, &mqstat, NULL) != 0)      
			{
				printk("<ERROR> [%s:__%d__]:: can%d Cannot set attr of recMsgID!\n",__FUNCTION__,__LINE__,controller);
				pthread_mutex_unlock(&canPortSem[controller]);
				return -1;
			}
			break;
		}
		case CAN_SET_LOOPBACK_MODE: //回环模式
		{
			printk("<DEBUG> [%s:__%d__]:: CAN_SET_LOOPBACK_MODE\n",__FUNCTION__,__LINE__);
			break;
		}
		case CAN_SET_ECHO: //回显模式
		{	/*ioctl(fd,CAN_SET_ECHO,1);*/
			printk("<DEBUG> [%s:__%d__]:: CAN_SET_ECHO\n",__FUNCTION__,__LINE__);
			can_info->can_echo_flag = (int)arg;
			break;
		}
		default:
			break;
	}
    
    pthread_mutex_unlock(&canPortSem[controller]);
//    printk("<DEBUG> [%s:__%d__]:: OUT\n",__FUNCTION__,__LINE__);
    
	return 0;
}

/*注册字符设备驱动框架*/
struct file_operations can_ops = {
	open:	CanOpenDriver,
	close:	CanCloseDriver,
	read:	CanRecMsg,
	write:	CanSendMsg,
	ioctl:	CanConfigPort,
};

int can_module_init(u8 controller)
{
	int ret, i;
	dev_t major;
	dev_t minor;
	char path[1024];
	CAN_BOARD *pBoard = NULL;
//	struct mq_attr attr;
	
	if(NULL == (pBrd[controller] = (struct CAN_Board *)malloc(sizeof(struct CAN_Board))))
	{
		printf("<ERROR> [%s:__%d__]:: malloc failed!\n",__FUNCTION__,__LINE__);
		return (ERROR);
	}

	if(controller == 0)
		pBrd[controller]->irq = HRCAN0_INT_VEC; 
	else 
		pBrd[controller]->irq = HRCAN1_INT_VEC;	
	
	pBrd[controller]->channelAddr = CAN_BASE_ADDR(controller);
	pBrd[controller]->controller = controller;
//	pBrd[controller]->port.can_mode = BASICCAN_MODE; /*默认标准模式*/
	pBrd[controller]->port.can_mode = PELICAN_MODE;	/*默认扩展模式*/
	/*
	 *   * baudrate:    0 - 5k
		 *              1 - 10k
		 *              2 - 20k
		 *              3 - 50k
		 *              4 - 100k
		 *              5 - 125k
		 *              6 - 250k
		 *              7 - 500k
		 *              8 - 800k
		 *              9 - 1000k
		 * canclk:      0 - 16M
		 *              1 - 8M
		 *              2 - 24M
		 *              */
	pBrd[controller]->port.baudRate = 6; /*默认波特率250K*/
	pBrd[controller]->port.canClk = 1; /*默认时钟8MHz*/
	pBrd[controller]->can_echo_flag = 0;		//can初始化时将回显flag设为0-不开启
	pBrd[controller]->loop_back_mode_flag = 0;	//can初始化时将回环flag设为0  TODO: 暂未实现该功能
	
	u8 ac_val[4] = {0x00, 0x00, 0x00, 0x00};/*验收码*/
	u8 am_val[4] = {0xff, 0xff, 0xff, 0xff};/*验收掩码*/
	for (i = 0; i < 4; i++)
	{
		pBrd[controller]->port.accCode[i] = ac_val[i];
		pBrd[controller]->port.accMask[i] = am_val[i];
	}
	
	g_CanRecvStatus[controller] = 0;
	g_CanRecvLens[controller] = 0;
	memset(canRecvPacket, 0, sizeof(canRecvPacket));
	
	canRxSem[controller] = semCCreate(SEM_Q_FIFO, 0);
	if(NULL == canRxSem[controller])
	{
		printf("<ERROR> [%s:__%d__]:: semCCreate canRxSem[%d] failed!\n",__FUNCTION__,__LINE__,controller);
		return (ERROR);
	}
	
	ret = pthread_mutex_init2(&canTxSem[controller],PTHREAD_MUTEX_DEFAULT, \
					RE_MUTEX_PRIO_NONE | RE_MUTEX_WAIT_FIFO, PTHREAD_MUTEX_CEILING);
	if(ret < 0)
	{
		printf("<ERROR> [%s:__%d__]:: pthread_mutex_init2 canTxSem[%d] failed!\n",__FUNCTION__,__LINE__,controller);
		return (ERROR);
	}
	
	ret = pthread_mutex_init2(&canPortSem[controller],PTHREAD_MUTEX_DEFAULT, \
					RE_MUTEX_PRIO_NONE | RE_MUTEX_WAIT_FIFO, PTHREAD_MUTEX_CEILING);
	if(ret < 0)
	{
		printf("<ERROR> [%s:__%d__]:: pthread_mutex_init2 canTxSem[%d] failed!\n",__FUNCTION__,__LINE__,controller);
		return (ERROR);
	}
	
//	attr.mq_msgsize = MAX_MSG_LEN;
//	attr.mq_maxmsg = MAX_MSGS;
//	attr.mq_waitqtype = PTHREAD_WAITQ_FIFO;
	pBrd[controller]->recMsgID = mq_create(O_NONBLOCK, MAX_MSGS, MAX_MSG_LEN, PTHREAD_WAITQ_FIFO);
	if(pBrd[controller]->recMsgID < 0)
	{
		printf("message quene create faild\n");
		return (ERROR);
	}
	
	pBoard = pBrd[controller];
	
	/*CAN控制器初始化, 默认标准模式, 默认波特率800K*/
	ret = hrCanInit(controller,pBoard->port.can_mode,pBoard->port.baudRate, pBoard->port.canClk);
	if(ret < 0)
	{
		printf("<ERROR> [%s:__%d__]:: hrCanInit failed!\n",__FUNCTION__,__LINE__);
		return (ERROR);
	}
	/*中断初始化*/
	hrCanInstInit(pBoard);
	
	/*注册字符设备驱动程序框架*/
	ret = register_driver(CAN_MAJOR + controller, &can_ops, &major);
	if(ret < 0)
	{
		printf("<ERROR> [%s:__%d__]:: CAN%u register_driver failed!\n",__FUNCTION__,__LINE__,controller);
		return (ERROR);
	}
//	printf("<DEBUG> [%s:__%d__]:: CAN%u register_driver ok, major = %u\n",__FUNCTION__,__LINE__,controller,major);
	
	ret = chrdev_minor_request(major, 0, 1, 255, &minor);
	if(ret < 0)
	{
		printf("<ERROR> [%s:__%d__]:: CAN%u chrdev_minor_request failed!\n",__FUNCTION__,__LINE__,controller);
		return (ERROR);
	}
//	printf("<DEBUG> [%s:__%d__]:: CAN%u chrdev_minor_request ok, minor = %u\n",__FUNCTION__,__LINE__,controller,minor);
	
	pBoard->dev = MKDEV(major, minor);
//	printf("<DEBUG> [%s:__%d__]:: CAN%u dev_num = %u\n",__FUNCTION__,__LINE__,controller,pBoard->dev);
	memset(path, 0, 1024);
	sprintf(path, "/dev/can%d", controller);
	ret = register_chrdev(path, pBoard->dev, pBoard);
	if(ret < 0)
	{
		printf("<ERROR> [%s:__%d__]:: CAN%u register_chrdev failed!\n",__FUNCTION__,__LINE__,controller);
		return (ERROR);
	}
	
	printf("CAN %d init ok!\n",controller);
	
	return (OK);
}



/*********************************** TEST *****************************************************/
void hcm_can_test(void)
{
    printf("0 status 0x%x\r\n", read_can_reg(REG_CAN_STATUS(0)));
    printf("1 status 0x%x\r\n", read_can_reg(REG_CAN_STATUS(1)));
    //hrCanInit(0, 0, 8, 1);
    hrCanInit(1, 0, 8, 1);
    test_basiccan_tx_rx(0);
}

void test_basiccan_rx()
{
	CAN_PACKET can_packet;
	sja_receive_test(0, (void *)&can_packet, BASICCAN_MODE);
}

void test_basiccan_tx()
{
	BASIC_FRAME b_f;
	CAN_PACKET can_packet;
	u8 data1[] = {0xe4, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70, 0x80};

	init_basic_tx_buffer((void *)&b_f, data1, 0xfc, 0x1, 0x0, 0x8);
	memcpy((u8*)&can_packet.basic, (u8*)&b_f, sizeof(BASIC_FRAME));
	sja_transmit_test(0, (void *)&can_packet, BASICCAN_MODE);
}
/* test case */
void test_basiccan_tx_rx(u8 controller)
{
	BASIC_FRAME b_f;
	CAN_PACKET can_packet;
	u8 data1[] = {0xe4, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8};

   	init_sja1000_basic_fpga(controller, 8, 1);
   	memcpy((u8*)&can_packet.basic, (u8*)&b_f, sizeof(BASIC_FRAME));
	/* receive basic packet and send basic packet to basic receiver */
	//sja_receive_test((controller + 1) % 2, (void *)&b_f, BASICCAN_MODE);

	init_basic_tx_buffer((void *)&b_f, data1, 0, 0xe5, 0x0, 0x8);
	//init_basic_tx_buffer((void *)&g0b_f, data1, 0, 0xe5, 0x0, 0x8);
	memcpy((u8*)&can_packet.basic, (u8*)&b_f, sizeof(BASIC_FRAME));
	sja_transmit_test(controller, (void *)&can_packet, BASICCAN_MODE);
#if 0

	init_basic_tx_buffer((void *)&b_f, data1, 0xfd, 0x1, 0x0, 0x8);
	sja_transmit_test(controller, (void *)&b_f, BASICCAN_MODE);

	init_basic_tx_buffer((void *)&b_f, data1, 0xfe, 0x1, 0x1, 0x8);
	sja_transmit_test(controller, (void *)&b_f, BASICCAN_MODE);

	/* receive extend packet and send basic packet to extend receiver */
    sja_receive_test(controller, (void *)&b_f, BASICCAN_MODE);
#endif

}
#if 1
void test_pelican_rx()
{
	CAN_PACKET can_packet;
	enable_can_intr(0, 0xff, PELICAN_MODE);
	sja_receive_test(0, (void *)&can_packet, PELICAN_MODE); //receive basic packet
}
void test_pelican_tx0()
{
	PELICAN_FRAME p_f;
	CAN_PACKET can_packet;
	u8 data2[] = {0x13, 0x13, 0x3c, 0x2d, 0x1e, 0xe1, 0xd2, 0xc3, 0xb4, 0xa5};
	enable_can_intr(0, 0xff, PELICAN_MODE);
	init_peli_tx_buffer((void *)&p_f, data2, 0x1, 0x0, 0x8, 0xfc, 0x02, 0x23, 0x3); //send eff frame to basic receiver
	memcpy((u8*)&can_packet.pelican, (u8*)&p_f, sizeof(PELICAN_FRAME));
	sja_transmit_test(0, (void *)&can_packet, PELICAN_MODE);
}
void test_pelican_tx()
{
	PELICAN_FRAME p_f;
	CAN_PACKET can_packet;
	u8 data2[] = {0x23, 0x3, 0x3c, 0x2d, 0x1e, 0xe1, 0xd2, 0xc3, 0xb4, 0xa5};
	enable_can_intr(0, 0xff, PELICAN_MODE);
	init_peli_tx_buffer((void *)&p_f, data2, 0x1, 0x0, 0x8, 0xfc, 0x01, 0x23, 0x3); //send eff frame to basic receiver
	memcpy((u8*)&can_packet.pelican, (u8*)&p_f, sizeof(PELICAN_FRAME));
	sja_transmit_test(0, (void *)&can_packet, PELICAN_MODE);
}
void test_pelican_tx_rx(u8 controller)
{
//    u32 val;
	PELICAN_FRAME p_f;
	CAN_PACKET can_packet;
	u8 data2[] = {0x5a, 0x4b, 0x3c, 0x2d, 0x1e, 0xe1, 0xd2, 0xc3, 0xb4, 0xa5};

	init_sja1000_pelican_fpga(controller, 9, 1);
	enable_can_intr(controller, 0xff, PELICAN_MODE);
	//sja_receive_test((controller + 1) % 2, (void *)&p_f, PELICAN_MODE); //receive basic packet
	init_peli_tx_buffer((void *)&p_f, data2, 0x1, 0x0, 0x8, 0xfc, 0x01, 0x23, 0x3); //send eff frame to basic receiver
	memcpy((u8*)&can_packet.pelican, (u8*)&p_f, sizeof(PELICAN_FRAME));
	sja_transmit_test(controller, (void *)&can_packet, PELICAN_MODE);
//    init_sja1000_pelican_simu(1);
	init_peli_tx_buffer((void *)&p_f, data2, 0x0, 0x0, 0x8, 0xfd, 0x01, 0x23, 0x3); //send eff frame to basic receiver
	memcpy((u8*)&can_packet.pelican, (u8*)&p_f, sizeof(PELICAN_FRAME));
	sja_transmit_test(controller, (void *)&can_packet, PELICAN_MODE);
//	init_peli_tx_buffer((void *)&p_f, data2, 0x0, 0x0, 0x8, 0xfe, 0x01, 0x23, 0x3); //send eff frame to basic receiver
//	sja_transmit_test(controller, (void *)&p_f, PELICAN_MODE);
//	init_peli_tx_buffer((void *)&p_f, data2, 0x1, 0x0, 0x8, 0xff, 0x01, 0x23, 0x3); //send eff frame to basic receiver
//	sja_transmit_test(controller, (void *)&p_f, PELICAN_MODE);
	init_peli_tx_buffer((void *)&p_f, data2, 0x1, 0x0, 0x8, 0xfa, 0x01, 0x23, 0x3); //send eff frame to basic receiver
	memcpy((u8*)&can_packet.pelican, (u8*)&p_f, sizeof(PELICAN_FRAME));
	sja_transmit_test(controller, (void *)&can_packet, PELICAN_MODE);

  //    delay_cnt(80);
//	send_can_command(controller, CMD_AT);
//	init_peli_tx_buffer((void *)&p_f, data2, 0x1, 0x0, 0x8, 0xfc, 0x01, 0x23, 0x4); //send eff frame to basic receiver
//	sja_transmit_test(controller, (void *)&p_f, PELICAN_MODE);
  //  delay_cnt(80);
//	send_can_command(controller, CMD_AT);
//	init_peli_tx_buffer((void *)&p_f, data2, 0x1, 0x0, 0x8, 0xfc, 0x00, 0x5a, 0x15); //send eff frame to basic receiver
//	sja_transmit_test(controller, (void *)&p_f, PELICAN_MODE);
//    delay_cnt(80);
//	send_can_command(controller, CMD_AT);
	sja_receive_test(controller, (void *)&can_packet, PELICAN_MODE); //receive basic packet
}
#endif
