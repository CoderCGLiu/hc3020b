#ifndef _HC3080_CAN_H
#define _HC3080_CAN_H

#include "../h/tool/hr3Macro.h"
#include "semLib.h"
#include <irq.h>
#include <devnum.h>
#include <driver.h>
#include <mqueue.h>
#include <semaphore.h>
#include <pthread.h>
#include <clock.h>

#define MAX_NUM_BORAD   2
#define CAN_MAJOR 	80
#define MAX_MSGS (700) 
#define MAX_MSG_LEN sizeof(CAN_PACKET)

/*ioctrl cmd*/
//#define	CAN_SET_MODE			0
#define	CAN_SET_FILTER			1
#define	CAN_SET_BAUDRATE		2
#define CAN_SET_READ_MOD		3
#define CAN_SET_LOOPBACK_MODE	4
#define CAN_SET_ECHO			5

#define HRCAN0_NAME "hc3080Can0"
#define HRCAN0_INT_VEC 				INT_CAN0

#define HRCAN1_NAME "hc3080Can1"
#define HRCAN1_INT_VEC              INT_CAN1

#define CAN_BASE_ADDR(_controller)	(0xffffffffbf090000 + ((_controller) << 15))

/* basic mode */
#define REG_CAN_CTRL(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x00 * 4) /* control register */
#define BIT_RR			(0x1 << 0)
#define BIT_RIE			(0x1 << 1)
#define BIT_TIE			(0x1 << 2)
#define BIT_EIE			(0x1 << 3)
#define BIT_OIE			(0x1 << 4)

#define REG_CAN_CMD(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x01 * 4) /* commond register */
#define REG_CAN_STATUS(ctrl)	(CAN_BASE_ADDR(ctrl) + 0x02 * 4) /* status register */
#define BIT_RBS			(0x1 << 0) //receive buffer status
#define BIT_DOS			(0x1 << 1)
#define BIT_TBS			(0x1 << 2) // transmit buffer status
#define BIT_TCS			(0x1 << 3)
#define	BIT_RS			(0x1 << 4) //receive status
#define BIT_TS			(0x1 << 5) //transmit status
#define BIT_ES			(0x1 << 6)
#define BIT_BS			(0x1 << 7)

#define REG_CAN_INTR(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x03 * 4) /* interrupt register */
#define REG_CAN_AC(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x04 * 4) /* acceptance code, reset mode*/
#define REG_CAN_AM(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x05 * 4) /* acceptance mask, reset mode */
#define REG_CAN_BT0(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x06 * 4) /* bus timing 0, reset mode */
#define REG_CAN_BT1(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x07 * 4) /* bus timing 1, reset mode */
#define REG_CAN_OC(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x08 * 4) /* output control, reset mode */
#define REG_CAN_TEST(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x09 * 4) /* test*/
#define REG_CAN_TB_IDH(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x0a * 4) /* transmit buffer identifier 10 to 3 */
#define REG_CAN_TB_IDL(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x0b * 4) /* transmit buffer identifier 2 to 0, RTR abd DLC */
#define REG_CAN_TB_DATA(ctrl, n)	(CAN_BASE_ADDR(ctrl) + (0x0c + (n)) * 4) /* transmit buffer data byte n, n = 0 ~ 7 */
#define REG_CAN_RB_IDH(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x14 * 4) /* receive buffer identifier 10 to 3 */
#define REG_CAN_RB_IDL(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x15 * 4) /* receive buffer identifier 2 to 0, RTR abd DLC */
#define REG_CAN_RB_DATA(ctrl, n)	(CAN_BASE_ADDR(ctrl) + (0x16 + (n)) * 4) /* receive buffer data byte n, n = 0 ~ 7 */
#define REG_CAN_CLK_DIV(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x1f * 4) /* clock divider */

/* extend mode */
#define REG_CAN_EXT_MODE(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x00 * 4)
#define BIT_LOM			(0x1 << 1) /* listen only mode */
#define BIT_STM			(0x1 << 2) /* self test mode */
#define BIT_AFM			(0x1 << 3) /* acceptance filter mode */
#define BIT_SM			(0x1 << 4) /* sleep mode */
#define REG_CAN_EXT_CMD(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x01 * 4)
#define REG_CAN_EXT_STATUS(ctrl)	(CAN_BASE_ADDR(ctrl) + 0x02 * 4)
#define REG_CAN_EXT_INTR(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x03 * 4)
#define REG_CAN_EXT_IE(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x04 * 4) /* interrupt enable register */
#define BIT_EXT_RIE		(0x1 << 0)
#define BIT_EXT_TIE		(0x1 << 1)
#define BIT_EXT_EIE		(0x1 << 2)
#define BIT_EXT_DOIE		(0x1 << 3)
#define BIT_EXT_WUIE		(0x1 << 4)
#define BIT_EXT_EPIE		(0x1 << 5)
#define BIT_EXT_ALIE		(0x1 << 6)
#define BIT_EXT_BEIE		(0x1 << 7)

#define REG_CAN_EXT_BT0(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x06 * 4) /* extend mode bus timing register 0 */
#define REG_CAN_EXT_BT1(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x07 * 4) /* extend mode bus timing register 1 */
#define REG_CAN_EXT_OC(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x08 * 4) /* output control register */
#define REG_CAN_EXT_TEST(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x09 * 4) /* test register */
#define REG_CAN_EXT_ALC(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x0b * 4) /* arbitration lost capture */
#define REG_CAN_EXT_ECC(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x0c * 4) /* error code capture */
#define REG_CAN_EXT_EWL(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x0d * 4) /* error warning limit */
#define REG_CAN_EXT_RXERC(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x0e * 4) /* receive error count */
#define REG_CAN_EXT_TXERC(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x0f * 4) /* transmit error count */
#define REG_CAN_EXT_AC0(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x10 * 4) /* reset mode: acceptance code 0, op mode: rx/tx frame info */
#define REG_CAN_EXT_AC1(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x11 * 4) /* reset mode: acceptance code 1, op mode: rx/tx identifier 1 */
#define REG_CAN_EXT_AC2(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x12 * 4) /* reset mode: acceptance code 2, op mode: rx/tx identifier 2 */
#define REG_CAN_EXT_AC3(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x13 * 4) /* reset mode: acceptance code 3, op mode: rx/tx data1(sff) or rx/tx id 3(eff) */
#define REG_CAN_EXT_AM0(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x14 * 4) /* reset mode: acceptance mask 0, op mode: rx/tx data2(sff) or rx/tx id 4(eff) */
#define REG_CAN_EXT_AM1(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x15 * 4) /* reset mode: acceptance mask 1, op mode: rx/tx data3(sff) or rx/tx data1(eff) */
#define REG_CAN_EXT_AM2(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x16 * 4) /* reset mode: acceptance mask 2, op mode: rx/tx data4(sff) or rx/tx data2(eff) */
#define REG_CAN_EXT_AM3(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x17 * 4) /* reset mode: acceptance mask 3, op mode: rx/tx data5(sff) or rx/tx data3(eff) */
#define REG_CAN_EXT_DATA6(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x18 * 4) /* op mode: rx/tx data6(sff) or rx/tx data4(eff) */
#define REG_CAN_EXT_DATA7(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x19 * 4) /* op mode: rx/tx data7(sff) or rx/tx data5(eff) */
#define REG_CAN_EXT_DATA8(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x1a * 4) /* op mode: rx/tx data8(sff) or rx/tx data6(eff) */
#define REG_CAN_EXT_EFF_DATA7(ctrl)	(CAN_BASE_ADDR(ctrl) + 0x1b * 4) /*eff: tx/rx data 7 */ 
#define REG_CAN_EXT_EFF_DATA8(ctrl)	(CAN_BASE_ADDR(ctrl) + 0x1c * 4) /*eff: tx/rx data 8 */ 
#define REG_CAN_EXT_RMC(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x1d * 4) /* rx message conter, read only */ 
#define REG_CAN_EXT_RBSA(ctrl)		(CAN_BASE_ADDR(ctrl) + 0x1e * 4) /* rx buffer start adderss, read only */
#define REG_CAN_EXT_CLK_DIV(ctrl)	(CAN_BASE_ADDR(ctrl) + 0x1f * 4) /* clock divider register */

#define REG_CAN_SFF_DATA(ctrl, n)	(CAN_BASE_ADDR(ctrl) + (0x13 + (n)) * 4)
#define REG_CAN_EFF_DATA(ctrl, n)	(CAN_BASE_ADDR(ctrl) + (0x15 + (n)) * 4)

#define RESET_MODE     0x1
#define OPERATING_MODE 0x0

#define BASICCAN_MODE  0x0 /*标准模式*/
#define PELICAN_MODE   0x1 /*扩展模式*/

#define SINGLE_FILTER	BIT_AFM
#define DUAL_FILTER	0x0

#define MAX_RETRY_NUM 100

enum{BAUDRATE_5K = 0, BAUDRATE_10K, BAUDRATE_20K, BAUDRATE_50K,
        BAUDRATE_100K, BAUDRATE_125K, BAUDRATE_250K, BAUDRATE_500K,
        BAUDRATE_800K, BAUDRATE_1000K};
enum{CANCLK_16M = 0, CANCLK_8M, CANCLK_24M};

enum {
	CMD_TR = 0x01,
	CMD_AT = 0x02,
	CMD_RRB = 0x04,
	CMD_CDO = 0x08,
	CMD_BASIC_GTS = 0x10,
	CMD_EXT_SRR = 0x10
};

typedef struct _CAN_CFG_T_ 
{
	union _CD_U_ 
	{
		u8 v;
		struct 
		{
			u8 clk_div : 3;
			u8 clk_off : 1;
			u8 rsvd0 : 1;
			u8 rxinten : 1;
			u8 cbp : 1;
			u8 can_mode : 1;
		} bit_info;
	} cd;

	union _BT0_U_ 
	{
		u8 v;
		struct 
		{
			u8 brp : 6; // baud rate prescaler
			u8 sjw : 2; // synchronization jump width
		} bit_info;
	} bt0;

	union _BT1_U_ 
	{
		u8 v;
		struct 
		{
			u8 tseg1 : 4; //time segment 1
			u8 tseg2 : 3; //time segment 2
			u8 sam : 1; //sampling
		} bit_info;
	} bt1;
	
	struct _ACF_T 
	{
		u8 ac[4]; // acceptance code
		u8 am[4]; // acceptance mask
	} acf;

	u8 intr_en;
} CAN_COMMON_T;

typedef struct 
{
	u8 rtr; //[4]  /*0-数据帧，1-远程帧*/
	u8 dlc; //[3:0]
	u8 idh;
	u8 idl; //identifier low 3 bits, [7:5]
	u8 data[8];
} BASIC_FRAME;/*标准模式*/

typedef struct
{
	u8 rtr; //remote transmission request, [6]    /*0-数据帧，1-远程帧*/
	u8 dlc; //data code length, [3:0]
	u8 id1;
	u8 id2;
	u8 data[8];
	u8 id3;
	u8 id4;
	u8 ff;  //frame format, [7]   /*0-标准帧，1-扩展帧*/
} PELICAN_FRAME;/*扩展模式*/

typedef struct _tagPORT_STRUCT
{
	u8 				can_mode;/*0-标准模式， 1-扩展模式，
								 *标准模式下有2种帧格式：标准数据帧、标准远程帧，
								 *扩展模式下有4种帧格式：标准数据帧、标准远程帧 or 扩展数据帧、扩展远程帧*/
	u8			 	brp,tseg1,tseg2;
	u8			 	sjw,sam;
	u8 				accCode[4];
	u8			 	accMask[4];
	u32				canClk;
	u32				baudRate;
}PORT_STRUCT;


/*0-标准模式， 1-扩展模式，
 *标准模式下有2种帧格式：标准数据帧、标准远程帧，
 *扩展模式下有4种帧格式：标准数据帧、标准远程帧 or 扩展数据帧、扩展远程帧*/
typedef union _tagCAN_PACKET
{
	BASIC_FRAME		basic;
	PELICAN_FRAME	pelican;
}CAN_PACKET;

typedef struct CAN_Board
{
	u8 				controller;
	u8           	irq;
	PORT_STRUCT		port;
	int				can_echo_flag;
	int				loop_back_mode_flag;
	dev_t 			dev;
	mqd_t 			recMsgID;
    u64             channelAddr;
}CAN_BOARD;

#endif 

