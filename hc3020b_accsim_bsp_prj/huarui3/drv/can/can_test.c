#include <reworks/types.h>
#include <wdg.h>
#include <pthread.h>
#include <time.h>
#include <reworksio.h>
#include <tty_ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <net/if.h>
#include "hr3Can.h"

	/*
	 *   * baudrate:    0 - 5k
		 *              1 - 10k
		 *              2 - 20k
		 *              3 - 50k
		 *              4 - 100k
		 *              5 - 125k
		 *              6 - 250k
		 *              7 - 500k
		 *              8 - 800k
		 *              9 - 1000k
		 * canclk:      0 - 16M
		 *              1 - 8M
		 *              2 - 24M
		 *              */
#define CANBAUDRATE		5
#define CANCLK			1

static char *can_dev_list[2] = {
								"/dev/can0",
								"/dev/can1",
								};
static char *can_1_task_name_list[2] = {
								"can0_test1_task",
								"can1_test1_task",
								};
static struct can_task_arg
{
	int can_devno;
	unsigned int can_baudrate;
	unsigned int can_clk;
};
static struct can_task_arg can_arg[sizeof(can_dev_list) / sizeof(can_dev_list[0])];
static SEM_ID sem_can_test[2];
static SEM_ID sem_can_exit;

#define _CAN_TEST_P2P_/*两路CAN对接测试*/

//#define _B_DATA_			/*标准模式 - 标准数据帧*/ /*当前固定初始化为扩展模式，不能使用标准模式的数据结构*/
//#define _B_REMOTE_		/*标准模式 - 标准远程帧*/ /*当前固定初始化为扩展模式，不能使用标准模式的数据结构*/
//#define _P_DATA_			/*扩展模式 - 标准数据帧*/
//#define _P_REMOTE_		/*扩展模式 - 标准远程帧*/
#define _P_EXT_DATA_		/*扩展模式 - 扩展数据帧*/
//#define _P_EXT_REMOTE_	/*扩展模式 - 扩展远程帧*/

#ifdef _CAN_TEST_P2P_
void canSend(struct can_task_arg *arg)
{
	int fd = arg->can_devno;
	CAN_PACKET can_packet;
	int i;
	int ret;
	unsigned long CAN_ID;
	unsigned char id1,id2,id3,id4;
	u8 can_mode;

	/*CAN_SET_FILTER*/
//	PORT_STRUCT can_port;
//	can_port.accCode = 0x1A2B4C5F;	//验收码 	
//	can_port.accMask = 0xFFFFFFFF;	//验收掩码

//	ioctl(fd,CAN_SET_ECHO,1);/*设置回显模式*/
#ifndef _CAN_TEST_P2P_
	while(1)
#endif
	{
/********** 1 - canSend() write start ***********************************************************/
		/*LOOP1：CAN工装发送ID为0x1，length为8，data为"abcdefgh"的标准帧 	
		 * LOOP2：CAN工装发送ID为0x101，length为8，data为"abcdefgh"的扩展帧
		 * LOOP3：CAN工装发送ID为0x1的远程标准帧
		 * LOOP4：CAN工装发送ID为0x101的远程扩展帧
		 * LOOP5：CAN工装发送数据区为'q'的标准帧*/
		
#ifdef _CAN_TEST_P2P_
		semTake(sem_can_test[0], -1);/*两路CAN对测时需要*/
#endif
		
#if 1
		printk("\n\n=====> 1 - canSend() start write\n");	
#ifdef _B_DATA_
		id1 = 0x1;
		id2 = 0x2;
		CAN_ID = (((u32)id1 << 8) | (id2 << 5)) >> 5;
		can_packet.basic.idh = id1;
		can_packet.basic.idl = id2;
		can_packet.basic.dlc = 8;
		can_packet.basic.rtr = 0;/*数据帧*/
		printk("=====> write can rtr = 0x%x\n",can_packet.basic.rtr);
		printk("=====> write can id = 0x%x\n",CAN_ID);
		printk("=====> write can dlc = 0x%x\n",can_packet.basic.dlc);
		for(i = 0; i < 8; i++)
		{
			can_packet.basic.data[i] = 0x61 + i;
			printk("=====> write can_packet[%d] = 0x%x\n",i,can_packet.basic.data[i]);
		}
#endif
#ifdef	_B_REMOTE_
		id1 = 0x1;
		id2 = 0x2;
		CAN_ID = (((u32)id1 << 8) | (id2 << 5)) >> 5;
		can_packet.basic.idh = id1;
		can_packet.basic.idl = id2;
		can_packet.basic.dlc = 0;
		can_packet.basic.rtr = 1;/*远程帧*/
		printk("=====> write can rtr = 0x%X\n",can_packet.basic.rtr);
		printk("=====> write can id = 0x%x\n",CAN_ID);
		printk("=====> write can dlc = 0x%x\n",can_packet.basic.dlc);
#endif
#ifdef	_P_DATA_
		id1 = 0x1;
		id2 = 0x2;
//		id3 = 0x3;
//		id4 = 0x4;
//		CAN_ID = ((((u32)id1 << 24) | ((u32)id2 << 16) | ((u32)id3 << 8) | id4 << 3)) >> 3;
		CAN_ID = (((u32)id1 << 8) | (id2 << 5)) >> 5;
		can_packet.pelican.id1 = id1;
		can_packet.pelican.id2 = id2;
//		can_packet.pelican.id3 = id3;
//		can_packet.pelican.id4 = id4;
		can_packet.pelican.dlc = 8;
		can_packet.pelican.rtr = 0;/*数据帧*/
		can_packet.pelican.ff = 0;/*标准帧*/
		printk("=====> write can rtr = 0x%x\n",can_packet.pelican.rtr);
		printk("=====> write can ff = 0x%x\n",can_packet.pelican.ff);
		printk("=====> write can id = 0x%x\n",CAN_ID);
		printk("=====> write can dlc = 0x%x\n",can_packet.pelican.dlc);
		for(i = 0; i < 8; i++)
		{
			can_packet.basic.data[i] = 0x61 + i;
			printk("=====> write can_packet[%d] = 0x%x\n",i,can_packet.basic.data[i]);
		}
#endif
#ifdef	_P_REMOTE_
		id1 = 0x1;
		id2 = 0x2;
//		id3 = 0x3;
//		id4 = 0x4;
//		CAN_ID = ((((u32)id1 << 24) | ((u32)id2 << 16) | ((u32)id3 << 8) | id4 << 3)) >> 3;
		CAN_ID = (((u32)id1 << 8) | (id2 << 5)) >> 5;
		can_packet.pelican.id1 = id1;
		can_packet.pelican.id2 = id2;
//		can_packet.pelican.id3 = id3;
//		can_packet.pelican.id4 = id4;
		can_packet.pelican.dlc = 8;
		can_packet.pelican.rtr = 1;/*远程帧*/
		can_packet.pelican.ff = 0;/*标准帧*/
		printk("=====> write can rtr = 0x%x\n",can_packet.pelican.rtr);
		printk("=====> write can ff = 0x%x\n",can_packet.pelican.ff);
		printk("=====> write can id = 0x%x\n",CAN_ID);
		printk("=====> write can dlc = 0x%x\n",can_packet.pelican.dlc);
#endif
#ifdef	_P_EXT_DATA_
		id1 = 0x1A;
		id2 = 0x2B;
		id3 = 0x3C;
		id4 = 0x4D;
		CAN_ID = ((((u32)id1 << 24) | ((u32)id2 << 16) | ((u32)id3 << 8) | id4 << 3)) >> 3;
		can_packet.pelican.id1 = id1;
		can_packet.pelican.id2 = id2;
		can_packet.pelican.id3 = id3;
		can_packet.pelican.id4 = id4;
		can_packet.pelican.dlc = 8;
		can_packet.pelican.rtr = 0;/*数据帧*/
		can_packet.pelican.ff = 1;/*扩展帧*/
		printk("[%d]%s=====> write can rtr = 0x%x\n",__LINE__, __func__, can_packet.pelican.rtr);
		printk("=====> write can ff = 0x%x\n",can_packet.pelican.ff);
		printk("=====> write can id = 0x%x\n",CAN_ID);
		printk("=====> write can dlc = 0x%x\n",can_packet.pelican.dlc);
		for(i = 0; i < 8; i++)
		{
			can_packet.basic.data[i] = 0x61 + i;
			printk("=====> write can_packet[%d] = 0x%x\n",i,can_packet.basic.data[i]);
		}
#endif
#ifdef	_P_EXT_REMOTE_
		id1 = 0x1;
		id2 = 0x2;
		id3 = 0x3;
		id4 = 0x4;
		CAN_ID = ((((u32)id1 << 24) | ((u32)id2 << 16) | ((u32)id3 << 8) | id4 << 3)) >> 3;
		can_packet.pelican.id1 = id1;
		can_packet.pelican.id2 = id2;
		can_packet.pelican.id3 = id3;
		can_packet.pelican.id4 = id4;
		can_packet.pelican.dlc = 8;
		can_packet.pelican.rtr = 1;/*远程帧*/
		can_packet.pelican.ff = 1;/*扩展帧*/
		printk("=====> write can rtr = 0x%x\n",can_packet.pelican.rtr);
		printk("=====> write can ff = 0x%x\n",can_packet.pelican.ff);
		printk("=====> write can id = 0x%x\n",CAN_ID);
		printk("=====> write can dlc = 0x%x\n",can_packet.pelican.dlc);
#endif

		ret = write(fd, &can_packet, sizeof(can_packet));
		if(ret < 0)
		{
			printk("<ERROR> [%s:__%d__]:: write error!\n",__FUNCTION__,__LINE__);
			return;
		}
#endif		
		
#ifdef _CAN_TEST_P2P_
		semGive(sem_can_test[1]);/*两路CAN对测时需要*/
#endif
/********** 1 - canSend() write end ***********************************************************/
		
/********** 4 - canSend() read start ***********************************************************/
		/*LOOP1：CAN工装接收到回发的标准帧
		 * LOOP2：CAN工装接收到回发的扩展帧
		 * LOOP3：CAN工装接收到回发的远程标准帧
		 * LOOP4：CAN工装接收到回发的远程扩展帧*/
		
#ifdef _CAN_TEST_P2P_
		semTake(sem_can_test[0], -1);/*两路CAN对测时需要*/
#endif
		
#if 1
		printk("\n\n=====> 4 - canSend() start read\n");
		memset(&can_packet, 0, sizeof(can_packet));
		
		ret = read(fd, &can_packet, sizeof(can_packet));
		if(ret < 0)
		{
			printk("<ERROR> [%s:__%d__]:: read error!\n",__FUNCTION__,__LINE__);
			return;
		}
#ifdef _B_DATA_
		id1 = can_packet.basic.idh;
		id2 = can_packet.basic.idl;
		CAN_ID = (((u32)id1 << 8) | (id2 << 5)) >> 5;
		printk("=====> read can rtr = 0x%x\n",can_packet.basic.rtr);
		printk("=====> read can id = 0x%x\n",CAN_ID);
		printk("=====> read can dlc = 0x%x\n",can_packet.basic.dlc);
		for(i = 0; i < 8; i++)
		{
			can_packet.basic.data[i] = 0x61 + i;
			printk("=====> read can_packet[%d] = 0x%x\n",i,can_packet.basic.data[i]);
		}
#endif
#ifdef	_B_REMOTE_
		id1 = can_packet.basic.idh;
		id2 = can_packet.basic.idl;
		CAN_ID = (((u32)id1 << 8) | (id2 << 5)) >> 5;
		printk("=====> read can rtr = 0x%X\n",can_packet.basic.rtr);
		printk("=====> read can id = 0x%x\n",CAN_ID);
		printk("=====> read can dlc = 0x%x\n",can_packet.basic.dlc);
#endif
#ifdef	_P_DATA_
		id1 = can_packet.pelican.id1;
		id2 = can_packet.pelican.id2;
//		id3 = can_packet.pelican.id3;
//		id4 = can_packet.pelican.id4;
//		CAN_ID = ((((u32)id1 << 24) | ((u32)id2 << 16) | ((u32)id3 << 8) | id4 << 3)) >> 3;
		CAN_ID = (((u32)id1 << 8) | (id2 << 5)) >> 5;
		printk("=====> read can rtr = 0x%x\n",can_packet.pelican.rtr);
		printk("=====> read can ff = 0x%x\n",can_packet.pelican.ff);
		printk("=====> read can id = 0x%x\n",CAN_ID);
		printk("=====> read can dlc = 0x%x\n",can_packet.pelican.dlc);
		for(i = 0; i < 8; i++)
		{
			can_packet.basic.data[i] = 0x61 + i;
			printk("=====> read can_packet[%d] = 0x%x\n",i,can_packet.basic.data[i]);
		}
#endif
#ifdef	_P_REMOTE_
		id1 = can_packet.pelican.id1;
		id2 = can_packet.pelican.id2;
//		id3 = can_packet.pelican.id3;
//		id4 = can_packet.pelican.id4;
//		CAN_ID = ((((u32)id1 << 24) | ((u32)id2 << 16) | ((u32)id3 << 8) | id4 << 3)) >> 3;
		CAN_ID = (((u32)id1 << 8) | (id2 << 5)) >> 5;
		printk("=====> read can rtr = 0x%x\n",can_packet.pelican.rtr);
		printk("=====> read can ff = 0x%x\n",can_packet.pelican.ff);
		printk("=====> read can id = 0x%x\n",CAN_ID);
		printk("=====> read can dlc = 0x%x\n",can_packet.pelican.dlc);
#endif
#ifdef	_P_EXT_DATA_
		id1 = can_packet.pelican.id1;
		id2 = can_packet.pelican.id2;
		id3 = can_packet.pelican.id3;
		id4 = can_packet.pelican.id4;
		CAN_ID = ((((u32)id1 << 24) | ((u32)id2 << 16) | ((u32)id3 << 8) | id4 << 3)) >> 3;
		printk("=====> read can rtr = 0x%x\n",can_packet.pelican.rtr);
		printk("=====> read can ff = 0x%x\n",can_packet.pelican.ff);
		printk("=====> read can id = 0x%x\n",CAN_ID);
		printk("=====> read can dlc = 0x%x\n",can_packet.pelican.dlc);
		for(i = 0; i < 8; i++)
		{
			can_packet.basic.data[i] = 0x61 + i;
			printk("=====> read can_packet[%d] = 0x%x\n",i,can_packet.basic.data[i]);
		}
#endif
#ifdef	_P_EXT_REMOTE_
		id1 = can_packet.pelican.id1;
		id2 = can_packet.pelican.id2;
		id3 = can_packet.pelican.id3;
		id4 = can_packet.pelican.id4;
		CAN_ID = ((((u32)id1 << 24) | ((u32)id2 << 16) | ((u32)id3 << 8) | id4 << 3)) >> 3;
		printk("=====> read can rtr = 0x%x\n",can_packet.pelican.rtr);
		printk("=====> read can ff = 0x%x\n",can_packet.pelican.ff);
		printk("=====> read can id = 0x%x\n",CAN_ID);
		printk("=====> read can dlc = 0x%x\n",can_packet.pelican.dlc);
#endif
#endif
		
#ifdef _CAN_TEST_P2P_
		semGive(sem_can_test[1]);/*两路CAN对测时需要*/
#endif
/********** 4 - canSend() read end ***********************************************************/
#ifdef _CAN_TEST_P2P_
		semGive(sem_can_exit);/*退出测试*/  /*两路CAN对测时需要*/
#endif
	}
	printk("\n\n=====> canSend end\n");
}
#endif

#if 1
void canRecv(struct can_task_arg *arg)
{
	int fd = arg->can_devno;
	PORT_STRUCT can_port;
	CAN_PACKET can_packet;
	int i;
	int ret;
	unsigned long CAN_ID;
	unsigned char id1,id2,id3,id4;
	u8 can_mode;

//	can_port.accCode = 0x1A2B4C5F;	//验收码 	1A(左边的)对应验收ID的[28:21] 2B对应验收ID的[20:13] 4C对应验收ID的[12:5] 5F的高5位(其他的位没有用)对应验收ID的[4:0]存到验收代码3的[7:3] 
//	can_port.accMask = 0xFFFFFFFF;	//验收掩码

#ifndef _CAN_TEST_P2P_
	while(1) 
#endif
	{
/********** 2 - canRecv() read start ***********************************************************/
		/*LOOP1：CAN工装发送ID为0x1，length为8，data为"abcdefgh"的标准帧 	
		 * LOOP2：CAN工装发送ID为0x101，length为8，data为"abcdefgh"的扩展帧
		 * LOOP3：CAN工装发送ID为0x1的远程标准帧
		 * LOOP4：CAN工装发送ID为0x101的远程扩展帧
		 * LOOP5：CAN工装发送数据区为'q'的标准帧*/
		/*LOOP1：CAN工装接收到回发的标准帧
		 * LOOP2：CAN工装接收到回发的扩展帧
		 * LOOP3：CAN工装接收到回发的远程标准帧
		 * LOOP4：CAN工装接收到回发的远程扩展帧*/
		
#ifdef _CAN_TEST_P2P_
		semTake(sem_can_test[1], -1);/*两路CAN对测时需要*/
#endif
		
#if 1
		printk("\n\n=====> 2 - canRecv() start read\n");
		memset(&can_packet, 0, sizeof(can_packet));

		ret = read(fd, &can_packet, sizeof(can_packet));
		if(ret < 0)
		{
			printk("<ERROR> [%s:__%d__]:: read error!\n",__FUNCTION__,__LINE__);
			return;
		}

#ifdef _B_DATA_
		id1 = can_packet.basic.idh;
		id2 = can_packet.basic.idl;
		CAN_ID = (((u32)id1 << 8) | (id2 << 5)) >> 5;
		printk("=====> read can rtr = 0x%x\n",can_packet.basic.rtr);
		printk("=====> read can id = 0x%x\n",CAN_ID);
		printk("=====> read can dlc = 0x%x\n",can_packet.basic.dlc);
		for(i = 0; i < 8; i++)
		{
			can_packet.basic.data[i] = 0x61 + i;
			printk("=====> read can_packet[%d] = 0x%x\n",i,can_packet.basic.data[i]);
		}
#endif
#ifdef	_B_REMOTE_
		id1 = can_packet.basic.idh;
		id2 = can_packet.basic.idl;
		CAN_ID = (((u32)id1 << 8) | (id2 << 5)) >> 5;
		printk("=====> read can rtr = 0x%X\n",can_packet.basic.rtr);
		printk("=====> read can id = 0x%x\n",CAN_ID);
		printk("=====> read can dlc = 0x%x\n",can_packet.basic.dlc);
#endif
#ifdef	_P_DATA_
		id1 = can_packet.pelican.id1;
		id2 = can_packet.pelican.id2;
//		id3 = can_packet.pelican.id3;
//		id4 = can_packet.pelican.id4;
//		CAN_ID = ((((u32)id1 << 24) | ((u32)id2 << 16) | ((u32)id3 << 8) | id4 << 3)) >> 3;
		CAN_ID = (((u32)id1 << 8) | (id2 << 5)) >> 5;
		printk("=====> read can rtr = 0x%x\n",can_packet.pelican.rtr);
		printk("=====> read can ff = 0x%x\n",can_packet.pelican.ff);
		printk("=====> read can id = 0x%x\n",CAN_ID);
		printk("=====> read can dlc = 0x%x\n",can_packet.pelican.dlc);
		for(i = 0; i < 8; i++)
		{
			can_packet.basic.data[i] = 0x61 + i;
			printk("=====> read can_packet[%d] = 0x%x\n",i,can_packet.basic.data[i]);
		}
#endif
#ifdef	_P_REMOTE_
		id1 = can_packet.pelican.id1;
		id2 = can_packet.pelican.id2;
//		id3 = can_packet.pelican.id3;
//		id4 = can_packet.pelican.id4;
//		CAN_ID = ((((u32)id1 << 24) | ((u32)id2 << 16) | ((u32)id3 << 8) | id4 << 3)) >> 3;
		CAN_ID = (((u32)id1 << 8) | (id2 << 5)) >> 5;
		printk("=====> read can rtr = 0x%x\n",can_packet.pelican.rtr);
		printk("=====> read can ff = 0x%x\n",can_packet.pelican.ff);
		printk("=====> read can id = 0x%x\n",CAN_ID);
		printk("=====> read can dlc = 0x%x\n",can_packet.pelican.dlc);
#endif
#ifdef	_P_EXT_DATA_
		id1 = can_packet.pelican.id1;
		id2 = can_packet.pelican.id2;
		id3 = can_packet.pelican.id3;
		id4 = can_packet.pelican.id4;
		CAN_ID = ((((u32)id1 << 24) | ((u32)id2 << 16) | ((u32)id3 << 8) | id4 << 3)) >> 3;
		printk("[%d]%s=====> read can rtr = 0x%x\n",__LINE__, __func__, can_packet.pelican.rtr);
		printk("=====> read can ff = 0x%x\n",can_packet.pelican.ff);
		printk("=====> read can id = 0x%x\n",CAN_ID);
		printk("=====> read can dlc = 0x%x\n",can_packet.pelican.dlc);
		for(i = 0; i < 8; i++)
		{
			can_packet.basic.data[i] = 0x61 + i;
			printk("=====> read can_packet[%d] = 0x%x\n",i,can_packet.basic.data[i]);
		}
#endif
#ifdef	_P_EXT_REMOTE_
		id1 = can_packet.pelican.id1;
		id2 = can_packet.pelican.id2;
		id3 = can_packet.pelican.id3;
		id4 = can_packet.pelican.id4;
		CAN_ID = ((((u32)id1 << 24) | ((u32)id2 << 16) | ((u32)id3 << 8) | id4 << 3)) >> 3;
		printk("=====> read can rtr = 0x%x\n",can_packet.pelican.rtr);
		printk("=====> read can ff = 0x%x\n",can_packet.pelican.ff);
		printk("=====> read can id = 0x%x\n",CAN_ID);
		printk("=====> read can dlc = 0x%x\n",can_packet.pelican.dlc);
#endif
#endif
/********** 2 - canRecv() read end ***********************************************************/
		
/********** 3 - canRecv() write start ***********************************************************/
#if 1		
		printk("\n\n=====> 3 - canRecv() start write\n");
		ret = write(fd, &can_packet, sizeof(can_packet));
		if(ret < 0)
		{
			printk("<ERROR> [%s:__%d__]:: write error!\n",__FUNCTION__,__LINE__);
			return;
		}
		/*LOOP1：CAN工装接收到回发的标准帧
		 * LOOP2：CAN工装接收到回发的扩展帧
		 * LOOP3：CAN工装接收到回发的远程标准帧
		 * LOOP4：CAN工装接收到回发的远程扩展帧*/
#ifdef _B_DATA_
		id1 = can_packet.basic.idh;
		id2 = can_packet.basic.idl;
		CAN_ID = (((u32)id1 << 8) | (id2 << 5)) >> 5;
		printk("=====> write can rtr = 0x%x\n",can_packet.basic.rtr);
		printk("=====> write can id = 0x%x\n",CAN_ID);
		printk("=====> write can dlc = 0x%x\n",can_packet.basic.dlc);
		for(i = 0; i < 8; i++)
		{
			can_packet.basic.data[i] = 0x61 + i;
			printk("=====> write can_packet[%d] = 0x%x\n",i,can_packet.basic.data[i]);
		}
#endif
#ifdef	_B_REMOTE_
		id1 = can_packet.basic.idh;
		id2 = can_packet.basic.idl;
		CAN_ID = (((u32)id1 << 8) | (id2 << 5)) >> 5;
		printk("=====> write can rtr = 0x%X\n",can_packet.basic.rtr);
		printk("=====> write can id = 0x%x\n",CAN_ID);
		printk("=====> write can dlc = 0x%x\n",can_packet.basic.dlc);
#endif
#ifdef	_P_DATA_
		id1 = can_packet.pelican.id1;
		id2 = can_packet.pelican.id2;
//		id3 = can_packet.pelican.id3;
//		id4 = can_packet.pelican.id4;
//		CAN_ID = ((((u32)id1 << 24) | ((u32)id2 << 16) | ((u32)id3 << 8) | id4 << 3)) >> 3;
		CAN_ID = (((u32)id1 << 8) | (id2 << 5)) >> 5;
		printk("=====> write can rtr = 0x%x\n",can_packet.pelican.rtr);
		printk("=====> write can ff = 0x%x\n",can_packet.pelican.ff);
		printk("=====> write can id = 0x%x\n",CAN_ID);
		printk("=====> write can dlc = 0x%x\n",can_packet.pelican.dlc);
		for(i = 0; i < 8; i++)
		{
			can_packet.basic.data[i] = 0x61 + i;
			printk("=====> write can_packet[%d] = 0x%x\n",i,can_packet.basic.data[i]);
		}
#endif
#ifdef	_P_REMOTE_
		id1 = can_packet.pelican.id1;
		id2 = can_packet.pelican.id2;
//		id3 = can_packet.pelican.id3;
//		id4 = can_packet.pelican.id4;
//		CAN_ID = ((((u32)id1 << 24) | ((u32)id2 << 16) | ((u32)id3 << 8) | id4 << 3)) >> 3;
		CAN_ID = (((u32)id1 << 8) | (id2 << 5)) >> 5;
		printk("=====> write can rtr = 0x%x\n",can_packet.pelican.rtr);
		printk("=====> write can ff = 0x%x\n",can_packet.pelican.ff);
		printk("=====> write can id = 0x%x\n",CAN_ID);
		printk("=====> write can dlc = 0x%x\n",can_packet.pelican.dlc);
#endif
#ifdef	_P_EXT_DATA_
		id1 = can_packet.pelican.id1;
		id2 = can_packet.pelican.id2;
		id3 = can_packet.pelican.id3;
		id4 = can_packet.pelican.id4;
		CAN_ID = ((((u32)id1 << 24) | ((u32)id2 << 16) | ((u32)id3 << 8) | id4 << 3)) >> 3;
		printk("=====> write can rtr = 0x%x\n",can_packet.pelican.rtr);
		printk("=====> write can ff = 0x%x\n",can_packet.pelican.ff);
		printk("=====> write can id = 0x%x\n",CAN_ID);
		printk("=====> write can dlc = 0x%x\n",can_packet.pelican.dlc);
		for(i = 0; i < 8; i++)
		{
			can_packet.basic.data[i] = 0x61 + i;
			printk("=====> write can_packet[%d] = 0x%x\n",i,can_packet.basic.data[i]);
		}
#endif
#ifdef	_P_EXT_REMOTE_
		id1 = can_packet.pelican.id1;
		id2 = can_packet.pelican.id2;
		id3 = can_packet.pelican.id3;
		id4 = can_packet.pelican.id4;
		CAN_ID = ((((u32)id1 << 24) | ((u32)id2 << 16) | ((u32)id3 << 8) | id4 << 3)) >> 3;
		printk("=====> write can rtr = 0x%x\n",can_packet.pelican.rtr);
		printk("=====> write can ff = 0x%x\n",can_packet.pelican.ff);
		printk("=====> write can id = 0x%x\n",CAN_ID);
		printk("=====> write can dlc = 0x%x\n",can_packet.pelican.dlc);
#endif
#endif
		
#ifdef _CAN_TEST_P2P_
		semGive(sem_can_test[0]);
#endif
/********** 3 - canRecv() write end ***********************************************************/
	}
#ifndef _CAN_TEST_P2P_
	semGive(sem_can_exit);/*用can盒子测试记得注释这里*/
#endif
	printk("\n\n=====> canRecv end\n");
}
#endif
void canTest1(unsigned int baudrate)
{
	int i; 
	int ret;
	pthread_t tid[sizeof(can_dev_list) / sizeof(can_dev_list[0])];
	
	/*设置波特率*/
	ioctl(can_arg[0].can_devno, CAN_SET_BAUDRATE, baudrate);
	ioctl(can_arg[1].can_devno, CAN_SET_BAUDRATE, baudrate);
	
	/*设置帧过滤*/
	PORT_STRUCT can_port;
	u8 accCode[4] = {0x1A, 0x2B, 0x3C, 0x4D};//验收码 
	u8 accMask[4] = {0xFF, 0xFF, 0xFF, 0xFF};//验收掩码
	for(i=0;i<4;i++)
	{
		can_port.accCode[i] = accCode[i];		
		can_port.accMask[i] = accMask[i];
	}
	ioctl(can_arg[0].can_devno, CAN_SET_FILTER, (void*)&can_port);
	
	printk("\n\n========== CAN TEST START ==============\n\n");
#ifdef _CAN_TEST_P2P_
		ret = pthread_create2(&tid[0], can_1_task_name_list[0], 170, 0, 65536, canSend, &can_arg[0]);
		if(ret!=0)
		{
		/*任务创建失败*/
			printk("%s pthread_create2 -error %d!\n",can_1_task_name_list[0], ret);
			return ERROR;
		}
		ret = pthread_detach(tid[0]);
		if(0 != ret)
		{
			printk("%s pthread_detach failed\n",can_1_task_name_list[0]);
			return ERROR;
		}
		/*这边最好的方式不是pthread_delay而是在调用ioctl的前后上锁 使在配置过程中不被其他任务抢占*/
		pthread_delay(3*sys_clk_rate_get());
		
		ret = pthread_create2(&tid[1], can_1_task_name_list[1], 170, 0, 65536, canRecv, &can_arg[1]);
		if(ret!=0)
		{
		/*任务创建失败*/
			printk("%s pthread_create2 -error %d!\n",can_1_task_name_list[1], ret);
			return ERROR;
		}
		ret = pthread_detach(tid[1]);
		if(0 != ret)
		{
			printk("%s pthread_detach failed\n",can_1_task_name_list[1]);
			return ERROR;
		}
		pthread_delay(3*sys_clk_rate_get());
#else
		for(i = 0; i < sizeof(can_dev_list) / sizeof(can_dev_list[0]); i++)
		{
			ret = pthread_create2(&tid[i], can_1_task_name_list[i], 170, 0, 65536, canRecv, &can_arg[i]);
			if(ret!=0)
			{
			/*任务创建失败*/
				printk("%s pthread_create2 -error %d!\n",can_1_task_name_list[i], ret);
				return ERROR;
			}
			ret = pthread_detach(tid[i]);
			if(0 != ret)
			{
				printk("%s pthread_detach failed\n",can_1_task_name_list[i]);
				return ERROR;
			}
			/*这边最好的方式不是pthread_delay而是在调用ioctl的前后上锁 使在配置过程中不被其他任务抢占*/
			pthread_delay(3*sys_clk_rate_get());
		}
#endif
}

static void *can_1_task(void *arg)
{
	int ret;
	int i;
	int fd = -2;
	
	/*设置mode*//*初始化时固定为扩展模式了，去掉CAN_SET_MODE功能*/
//#if defined(_B_DATA_)||defined(_B_REMOTE_)
//	u8 can_mode = BASICCAN_MODE;
//#endif
//#if defined(_P_DATA_)||defined(_P_REMOTE_)||defined(_P_EXT_DATA_)||defined(_P_EXT_REMOTE_)
//	can_mode = PELICAN_MODE;
//#endif
	
	Dump_CAN_BOARD();
	
	for(i = 0; i < sizeof(can_dev_list) / sizeof(can_dev_list[0]); i++)
	{
		fd = open(can_dev_list[i], O_RDWR, 0777);
		if(OS_ERROR == fd)
		{
			printk("[ERROR] open can(%s)\n", can_dev_list[i]);
			return NULL;	
		}
		can_arg[i].can_devno = fd;
		/*必须在产生接收中断之前就设置好mode，否则一旦对方发送了数据，
		 * 我方就会产生接收中断，此时就会按默认设置的mode进行读取buffer*/
//		ioctl(fd, CAN_SET_MODE, can_mode);/*初始化时固定为扩展模式了，去掉CAN_SET_MODE功能*/
	}
	
	canTest1(CANBAUDRATE);

	Dump_CAN_BOARD();
	return NULL;
}

void can_1_close_can_dev(void)
{
	int i;
	for(i = 0; i < sizeof(can_dev_list) / sizeof(can_dev_list[0]); i++)
		close(can_arg[i].can_devno);
}

static void* can_monitor_task(void* arg)
{
	printk("<DEBUG> [%s:__%d__]\n",__FUNCTION__,__LINE__);
	semTake(sem_can_exit, -1);
	
	can_1_close_can_dev();

	semDelete(sem_can_test[0]);
	semDelete(sem_can_test[1]);
	semDelete(sem_can_exit);
	printk("\n\n========== CAN TEST END ==============\n\n");
	return NULL;
}

/************************************************
 * 测试名称：CAN总线测试
 * 测试标识：BSP_BUS_CAN_SR1
 * 用例版本：2021-05-18
 * 测试内容：CAN标准帧/扩展帧/远程帧收发测试。
 * 测试说明：无。 
 ***********************************************/
int case_bsp_tests_can_1(void)
{
	pthread_t tid,tid2;
	int ret;
	
	sem_can_test[0] = semCCreate(SEM_Q_FIFO, 1);
	sem_can_test[1] = semCCreate(SEM_Q_FIFO, 0);
	sem_can_exit = semCCreate(SEM_Q_FIFO, 0);
	
	ret = pthread_create2(&tid, "can_1_task", 155, 0, 65536, can_1_task, NULL);
	if(ret!=0)
	{
	/*任务创建失败*/
		printk("pthread_create2 -error %d!\n", ret);
		return -1;
	}
	
	ret = pthread_create2(&tid2, "can_monitor_task", 155, 0, 4096, can_monitor_task, NULL);
	if(ret!=0)
	{
	/*任务创建失败*/
		printk("pthread_create2 -error %d!\n", ret);
		return -1;
	}
	
	ret = pthread_detach(tid);
	if(0 != ret)
	{
		printk("pthread_detach failed\n");
		return -1;
	}
	ret = pthread_detach(tid2);
	if(0 != ret)
	{
		printk("pthread_detach failed\n");
		return -1;
	}
	return 0;
}

void can_test_init(void)
{
	return;
}
