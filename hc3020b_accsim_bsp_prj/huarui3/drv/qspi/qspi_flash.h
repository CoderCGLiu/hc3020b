#ifndef  __QSPI_FLASH_H__
#define  __QSPI_FLASH_H__

#define SUB_SECTOR_SIZE 	(0x1000UL)//4KB
#define SECTOR_SIZE 		(0x10000UL)//64KB

/*cmd for standard, dual, qual mode*/
#define CMD_RDID                0x9f   /*Read Identification*/
#define CMD_READ                0x03   /*Read Data Bytes*/
#define CMD_READ4BYTE           0x13   /*Read Data Bytes using 4 Bytes Address*/
#define CMD_FAST_READ           0x0B   /*Read Data Bytes at Higher Speed*/
#define CMD_FAST_READ4BYTE      0x0C   /*Read Data Bytes at Higher Speed using 4 Bytes Address*/
#define CMD_RDSFDP              0x5A   /*Read Serial Flash Discovery Parameter*/
#define CMD_WREN                0x06   /*Write Enable*/
#define CMD_WRDI                0x04   /*Write Disable*/
#define CMD_PP                  0x02   /*Page Program*/
#define CMD_PP4BYTE             0x12   /*Page Program using 4 Bytes Address*/
#define CMD_SSE                 0x20   /*SubSector Erase*/
#define CMD_SE                  0xD8   /*Sector Erase*/
#define CMD_BE                  0xC7   /*Bulk Erase*/
#define CMD_SE4BYTE             0xDC   /*Sector Erase using 4 Bytes Address*/
#define CMD_SSE4BYTE            0x21   /*SubSector Erase using 4 Bytes Address*/
#define CMD_RDSR                0x05   /*Read Status Register*/
#define CMD_WRSR                0x01   /*Write Status Register*/

#define CMD_RDLR                0xE8   /*Read Lock Register*/
#define CMD_WRLR                0xE5   /*Write to Lock Register*/
#define CMD_RFSR                0x70   /*Read Flag Status Register*/
#define CMD_CLFSR               0x50   /*Clear Flag Status Register*/
#define CMD_EN4BYTEADDR         0xB7   /*Enter 4-byte address mode*/
#define CMD_EX4BYTEADDR         0xE9   /*Exit 4-byte address mode*/
#define CMD_WREAR               0xC5   /*Write Extended Address Register*/
#define CMD_RDEAR               0xC8   /*Read Extended Address Register*/
#define CMD_RSTEN               0x66   /*Reset Enable*/
#define CMD_RST                 0x99   /*Reset Memory*/

/*dual cmd*/
#define DUAL_CMD_DCFR           0x0b    /*Dual Command Fast Read*/
#define DUAL_CMD_WREN           0x06    /*Write Enable*/
#define DUAL_CMD_WRDI           0x04    /*Write Disable*/
#define DUAL_CMD_DCPP           0xA2    /*Dual Command Page Program*/
#define DUAL_CMD_SE             0xD8   /*Sector Erase*/
#define DUAL_CMD_BE             0xC7   /*Bulk Erase*/
#define DUAL_CMD_DIOR           0xBB    /*Dual Command dual IO Read*/
#define DUAL_CMD_4DIOR          0xBC    /*Dual Command dual IO Read, 4Byte addr*/

/*quad cmd*/
#define QUAD_CMD_QCFR           0x0b
#define QUAD_CMD_WREN           0x06
#define QUAD_CMD_WRDI           0x04
#define QUAD_CMD_QCPP           0x02
#define QUAD_CMD_4PP            0x12
#define QUAD_CMD_SE             0xD8   /*Sector Erase*/
#define QUAD_CMD_BE             0xC7   /*Bulk Erase*/
#define QUAD_CMD_QIOR           0xEB    /*Quad Command quad IO Read*/
#define QUAD_CMD_4QIOR          0xEC    /*Quad Command quad IO Read, 4Byte Addr*/


#define CMD_WVCR               	0x81        //WRITE VOLATILE  CONFIGURATION REGISTER
#define CMD_RVCR               	0x85        //WRITE VOLATILE  CONFIGURATION REGISTER
#define CMD_WEVCR              	0x61        //WRITE_ENHANCED_VOLATILE_CONFIGURATION_REGISTER
#define CMD_REVCR              	0x65        //WRITE_ENHANCED_VOLATILE_CONFIGURATION_REGISTER

#define CMD_WNVCR				0xB1        //WRITE NONVOLATILE CONFIGURATION REGISTER
#define CMD_RNVCR				0xB5        //READ NONVOLATILE CONFIGURATION_REGISTER

#define CMD_MULTIRDID     		0xAf   /*Read Identification*/

#define DUAL_INPUT_FAST_PROGRAM 0xA2
#define QUAD_INPUT_FAST_PROGRAM 0x32

typedef union {
	volatile unsigned char v;
	struct {
		volatile unsigned int WRITE_IN_PROGRESS   :1; //1-busy 0-ready
		volatile unsigned int WRITE_ENABLE_LATCH  :1; //1-set  0-clear
		volatile unsigned int BP0                 :3; //protected area tables
		volatile unsigned int TOP_BOTTOM          :1; //0-top  1-bottom
		volatile unsigned int BP1                 :1; //protected area tables
		volatile unsigned int WRITE_DISABLED      :1; //0-enabled   1-disabled
	} bits;
} QSPI_FLASH_STATUS;

typedef union {
	volatile unsigned short int v;
	struct {
		volatile unsigned int ADDRESS_MODE        :1;/*0-4B, 1-3B(Default)*/
		volatile unsigned int SEGMENT_SELECT      :1;/*0 = Highest 128Mb segment, 1 = Lowest 128Mb segment (Default)*/
		volatile unsigned int DUAL_IO_PROTOCOL    :1;
		volatile unsigned int QUAD_IO_PROTOCOL    :1;
		volatile unsigned int RESET_HOLD          :1;
		volatile unsigned int __RESVED1           :1;
		volatile unsigned int OUTPUT_DRIVER       :3;
		volatile unsigned int XIP_MODE_AT_RESET   :3;
		volatile unsigned int DUMMY_CYCLES        :4;
	} bits;
} QSPI_FLASH_NONVOLATILE_CONFIGURATION;

typedef union {
	volatile unsigned char v;
	struct {
		volatile unsigned int OUTPUT_DRIVER       :3;
		volatile unsigned int __RESV              :1; //1
		volatile unsigned int REST_HOLD           :1; //0-disable, 1-enable
		volatile unsigned int DOUBLE_TRANS_RATE   :1; //0-enable, 1-disable
		volatile unsigned int DUAL_PROTOCAL       :1; //0-enable, 1-disable
		volatile unsigned int QUAD_PROTOCAL       :1; //0-enable, 1-disable
	} bits;
} QSPI_FLASH_ENHANCED_VOLATILE_CONF_REG;

typedef union {
	volatile unsigned char v;
	struct {
		volatile unsigned int ADDRESSING                    :1;
		volatile unsigned int PROTECTION                    :1;
		volatile unsigned int PROGRAM_SUSPEND               :1;
		volatile unsigned int __RESVED                      :1;
		volatile unsigned int PROGRAM                       :1;
		volatile unsigned int ERASE                         :1;
		volatile unsigned int ERASE_SUSPEND                 :1;
		volatile unsigned int PROGRAM_OR_ERASE_CONTROLLER   :1;/*0 = Busy, 1 = Ready*/
	} bits;
} QSPI_FLASH_FLAG_STATUS;

#endif  /* __QSPI_FLASH_H__ */
