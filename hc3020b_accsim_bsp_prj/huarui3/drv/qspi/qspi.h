#ifndef __QSPI_H_
#define __QSPI_H_

#define QSPI_BASE_ADDR             0x1f910000
#define QSPI_CTRL                  0x0
#define QSPI_APB_TIMEOUT_VAL       0xffff0000
#define QSPI_APB_TIMEOUT_EN        0x00000200
#define QSPI_BIG_MODE_EN           0x00000100
#define QSPI_LINE_MODE             0x000000c0
#define QSPI_CPOL                  0x00000020
#define QSPI_CPHA                  0x00000010
#define QSPI_CLK_DIV               0x0000000f

#define STANDARD_MODE              0x00
#define SINGLE_MODE                0x40
#define DUAL_MODE                  0x80
#define QUAD_MODE                  0xc0
#define CLK_10DIV                  0x4

#define QSPI_TIMING                0x4
#define QSPI_SETUP_TIME_VAL        0x000000ff
#define QSPI_HOLD_TIME_VAL         0x0000ff00
#define QSPI_WAIT_TIME_VAL         0x00ff0000

#define QSPI_DATA                  0x8

#define QSPI_STATUS                0xc
#define QSPI_RXFIFO_NUM            0x0000003f
#define QSPI_TXFIFO_NUM            0x000007c0
#define QSPI_CMDFIFO_NUM           0x00007800
#define QSPI_INSIDE_STAT           0x00078000
#define QSPI_TXFIFO_UNDERFLOW      0x00080000
#define QSPI_RXFIFO_OVERFLOW       0x00100000

#define QSPI_FIFO_CLR              0x10
#define QSPI_CLR_RXFIFO            0x00000001
#define QSPI_CLR_TXFIFO            0x00000002
#define QSPI_CLR_BOOTCMDFIFO       0x00000004

#define QSPI_BOOT_CONFIG           0x14
#define BOOT_4LINE_CMD             0x0000000f
#define BOOT_4LINE_DUMMY_CYC       0x00000f00
#define BOOT_CMD_MODE              0x00001000

/*cmd for standard, dual, qual mode*/
#define CMD_RDID                0x9f   /*Read Identification*/
#define CMD_READ                0x03   /*Read Data Bytes*/
#define CMD_READ4BYTE           0x13   /*Read Data Bytes using 4 Bytes Address*/
#define CMD_FAST_READ           0x0B   /*Read Data Bytes at Higher Speed*/
#define CMD_FAST_READ4BYTE      0x0C   /*Read Data Bytes at Higher Speed using 4 Bytes Address*/
#define CMD_RDSFDP              0x5A   /*Read Serial Flash Discovery Parameter*/
#define CMD_WREN                0x06   /*Write Enable*/
#define CMD_WRDI                0x04   /*Write Disable*/
#define CMD_PP                  0x02   /*Page Program*/
#define CMD_SSE                 0x20   /*SubSector Erase*/
#define CMD_SE                  0xD8   /*Sector Erase*/
#define CMD_BE                  0xC7   /*Bulk Erase*/
#define CMD_4SE                 0xDC   /*Erase 64k/256k*/
#define CMD_RDSR                0x05   /*Read Status Register*/
#define CMD_WRSR                0x01   /*Write Status Register*/
#define CMD_RDCR                0x35   /*Read Configuration Register1*/

#define CMD_RDLR                0xE8   /*Read Lock Register*/
#define CMD_WRLR                0xE5   /*Write to Lock Register*/
#define CMD_RFSR                0x70   /*Read Flag Status Register*/
#define CMD_CLFSR               0x50   /*Clear Flag Status Register*/
#define CMD_EN4BYTEADDR         0xB7   /*Enter 4-byte address mode*/
#define CMD_EX4BYTEADDR         0xE9   /*Exit 4-byte address mode*/
#define CMD_WREAR               0xC5   /*Write Extended Address Register*/
#define CMD_RDEAR               0xC8   /*Read Extended Address Register*/
#define CMD_RSTEN               0x66   /*Reset Enable*/
#define CMD_RST                 0x99   /*Reset Memory*/
#define CMD_RDAR                0x65   /*Read any Registor*/
#define CMD_WRAR                0x71   /*Write any Registor*/
#define CMD_RDSR1               0x05   /*Read Flag Status Register 1*/
#define CMD_4BAM                0xb7    /*ENter 4Byte Addr Mode*/
/*dual cmd*/
#define DUAL_CMD_DCFR           0x0b    /*Dual Command Fast Read*/
#define DUAL_CMD_WREN           0x06    /*Write Enable*/
#define DUAL_CMD_WRDI           0x04    /*Write Disable*/
#define DUAL_CMD_DCPP           0x02    /*Dual Command Page Program*/
#define DUAL_CMD_SE             0xD8   /*Sector Erase*/
#define DUAL_CMD_BE             0xC7   /*Bulk Erase*/
#define DUAL_CMD_DIOR           0xBB    /*Dual Command dual IO Read*/
#define DUAL_CMD_4DIOR          0xBC    /*Dual Command dual IO Read, 4Byte addr*/

/*quad cmd*/
#define QUAD_CMD_QCFR           0x0b
#define QUAD_CMD_WREN           0x06
#define QUAD_CMD_WRDI           0x04
#define QUAD_CMD_QCPP           0x02
#define QUAD_CMD_4PP            0x12
#define QUAD_CMD_SE             0xD8   /*Sector Erase*/
#define QUAD_CMD_BE             0xC7   /*Bulk Erase*/
#define QUAD_CMD_QIOR           0xEB    /*Quad Command quad IO Read*/
#define QUAD_CMD_4QIOR          0xEC    /*Quad Command quad IO Read, 4Byte Addr*/


#define WRITE_BIT          0x100
#define CONTINUOUS_CMD     0x200
#define DUMMY_CLK_EN       0x800000
#define DUMMY_12CYCLE      0xb0000
#define DUMMY_10CYCLE      0x90000
#define DUMMY_8CYCLE       0x70000
#define DUMMY_CYCLE_EN     0x800

#define CMD_WVCR               0x81        //WRITE VOLATILE  CONFIGURATION REGISTER
#define CMD_WEVCR              0x61        //WRITE_ENHANCED_VOLATILE_CONFIGURATION_REGISTER

/*S25FS reg map*/
#define CR1NV              0x00000002
#define CR2NV              0x00000003
#define CR3NV              0x00000004
#define SR1V               0x00800000
#define SR2V               0x00800001
#define CR1V               0x00800002
#define CR2V               0x00800003
#define CR3V               0x00800004
#define CR4V               0x00800005


#endif /* __QSPI_H_ */
