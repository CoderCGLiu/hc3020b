#ifndef  __QSPI_LIB_V2_C__
#define  __QSPI_LIB_V2_C__
#include <stdio.h>
#include "./qspi_lib.h"
#include "./qspi_flash.h"
#include "../../h/drvCommon.h"
#include <pthread.h>

static pthread_t muteSem;/*ldf 20230612 add*/
static int qspi_init_flag = 0;/*ldf 20230612 add*/


//MT25U256ABA --micron device id: 0x20 0xbb 0x19 0x10
#define DEFAULT_WAIT_CNT 1000000
#define wait_if(expr, cnt, desc) ({\
        int _i = 0; \
        for (;_i < cnt; _i++) { if (!(expr)) break; if (0 == _i%100) /*printf(".")*/;} \
        _i;\
        })

#if 0/*ldf 20230614*/
#define uassert(x)              \
    do {                   \
        if (!(x)) {    \
            printf("[PANIC][%s:%d][%s] assertion failed! %s", __FILE__, __LINE__, __FUNCTION__, #x); \
        } \
    }while(0)

#define ustatic_assert(x)   \
    switch(x) {        \
        case 0:    \
        case (x):; \
    }

#define uoffsetof(type, member) \
    ((unsigned long)(&((type*)0)->member))

#define ROUNDDOWN(a, n) \
    ({ \
     unsigned long __a = (unsigned long)(a);\
     (typeof(a))(__a - __a%(n));\
     })
#define ROUNDUP(a, n) \
    ({ \
     unsigned long __n = (unsigned long)n; \
     (typeof(a))(ROUNDDOWN((unsigned long)a + __n -1, __n)); \
     })
static void assert_qspi_defines()
{
	ustatic_assert(0x8 ==  uoffsetof(struct qspi_hc_regs, qspi_hc_data));
	ustatic_assert(0x10 ==  uoffsetof(struct qspi_hc_regs, qspi_hc_fifo_clr));
	ustatic_assert(0x1c==  uoffsetof(struct qspi_hc_regs, qspi_hc_int_status));
}
#endif

void libq_clear_fifo(struct qspi_hc_regs *host)
{
	/*clear tx,rx,boot fifo*/
	QSPI_HC_FIFO_CLR clr_val = {.bits.CLR_RXFIFO=1, .bits.CLR_TXFIFO=1};
	host->qspi_hc_fifo_clr = clr_val;
	return;
}

void libq_flash_soft_reset(struct qspi_hc_regs* host)
{
	QSPI_HC_CMD cmd = {.v=0};
	cmd.bits.IS_WRITE_READ = 1;
	cmd.bits.RD_NUM_OR_WR_DATA = CMD_RSTEN;
	host->qspi_hc_data.v = cmd.v;
	wait_if((0 != host->qspi_hc_status.bits.TXFIFOCNT), DEFAULT_WAIT_CNT, "TXFIFOCNT != 0");

	cmd.bits.RD_NUM_OR_WR_DATA = CMD_RST;
	host->qspi_hc_data.v = cmd.v;
	wait_if((0 != host->qspi_hc_status.bits.TXFIFOCNT), DEFAULT_WAIT_CNT, "TXFIFOCNT != 0");
}

void libq_delay_cnt(int cnt)
{
	while(cnt--);
}

//0-spi 1-1line 2-2line 3-4line
void libq_set_mode_line(struct qspi_hc_regs* host, int mode)
{
	host->qspi_hc_ctrl.bits.MODE = mode;
}

//isbig: 0 little-endian ,  1 big-endian
void libq_set_mode_bl(struct qspi_hc_regs *host, int isbig)
{
	host->qspi_hc_ctrl.bits.BLMODE = isbig;
}

void libq_set_mode_cpol(struct qspi_hc_regs* host, int cpol)
{
	host->qspi_hc_ctrl.bits.CPOL = cpol;
}

void libq_set_mode_cpha(struct qspi_hc_regs* host, int cpha)
{
	host->qspi_hc_ctrl.bits.CPHA = cpha;
}

void libq_set_mode_fdiv(struct qspi_hc_regs* host, int fdiv)
{
	host->qspi_hc_ctrl.bits.FDIV = fdiv;
}

void libq_format_print(unsigned char *buf, unsigned int len, unsigned int break_len)
{
	int i=0;
	for (i=0; i<len; i++)
	{
		if ((i+1)%break_len == 1)
			printf("0x%x: ", i);
		printf("%x ", buf[i]);
		if ((i+1)%break_len == 0)
			printf("\r\n");
	}
	printf("\r\n");
}

QSPI_FLASH_FLAG_STATUS libq_read_flag_status_reg(struct qspi_hc_regs *host)
{
	QSPI_HC_CMD cmd = {.v=0};
	cmd.bits.IS_WRITE_READ = 1;
	cmd.bits.IS_CONTINUE_SINGLE = 1;
	cmd.bits.RD_NUM_OR_WR_DATA = CMD_RFSR;
	host->qspi_hc_data.v = cmd.v;
	host->qspi_hc_data.v = 0x0;
	wait_if((0 == host->qspi_hc_status.bits.RXFIFOCNT), DEFAULT_WAIT_CNT, "RXFIFO_NUM == 0");
	QSPI_FLASH_FLAG_STATUS ret = {.v = (unsigned char)(host->qspi_hc_data.v)};
	return ret;
}

void libq_clear_flag_status_reg(struct qspi_hc_regs *host)
{
	QSPI_HC_CMD cmd = {.v=0};
	cmd.bits.IS_WRITE_READ = 1;
	cmd.bits.IS_CONTINUE_SINGLE = 0;
	cmd.bits.RD_NUM_OR_WR_DATA = CMD_CLFSR;
	host->qspi_hc_data.v = cmd.v;
}

QSPI_FLASH_STATUS libq_read_status_reg(struct qspi_hc_regs *host)
{
	QSPI_HC_CMD cmd = {.v=0};
	cmd.bits.IS_WRITE_READ = 1;
	cmd.bits.IS_CONTINUE_SINGLE = 1;
	cmd.bits.RD_NUM_OR_WR_DATA = CMD_RDSR;
	host->qspi_hc_data.v = cmd.v;
	host->qspi_hc_data.v = 0x0;
	wait_if((0 == host->qspi_hc_status.bits.RXFIFOCNT), DEFAULT_WAIT_CNT, "RXFIFO_NUM == 0");
	QSPI_FLASH_STATUS ret = {.v = (unsigned char)(host->qspi_hc_data.v)};
	return ret;
}

QSPI_FLASH_NONVOLATILE_CONFIGURATION libq_read_nonvolatile_config_reg(struct qspi_hc_regs* host)/*ldf 20230613 add*/
{
	QSPI_HC_CMD cmd = {.v=0};
	
	cmd.bits.IS_WRITE_READ = 1;
	cmd.bits.IS_CONTINUE_SINGLE = 1;
	cmd.bits.RD_NUM_OR_WR_DATA = CMD_RNVCR;
	host->qspi_hc_data.v = cmd.v;
	host->qspi_hc_data.v = 0x0;
	wait_if((0 == host->qspi_hc_status.bits.RXFIFOCNT), DEFAULT_WAIT_CNT, "RXFIFO_NUM == 0");
	QSPI_FLASH_NONVOLATILE_CONFIGURATION ret = {.v = (unsigned char)(host->qspi_hc_data.v)};
	return ret;
}

void libq_write_enable(struct qspi_hc_regs *host)
{
	QSPI_HC_CMD cmd = {.v=0};
	cmd.bits.IS_WRITE_READ = 1;
	cmd.bits.RD_NUM_OR_WR_DATA = CMD_WREN;
	host->qspi_hc_data.v = cmd.v;
	wait_if((0 != host->qspi_hc_status.bits.TXFIFOCNT), DEFAULT_WAIT_CNT, "TXFIFO_NUM != 0");
}

void libq_write_disable(struct qspi_hc_regs *host)
{
	QSPI_HC_CMD cmd = {.v=0};
	cmd.bits.IS_WRITE_READ = 1;
	cmd.bits.RD_NUM_OR_WR_DATA = CMD_WRDI;
	host->qspi_hc_data.v = cmd.v;
	wait_if((0 != host->qspi_hc_status.bits.TXFIFOCNT), DEFAULT_WAIT_CNT, "TXFIFO_NUM != 0");
}

void libq_flash_wait_write_enable(struct qspi_hc_regs* host)/*ldf 20230614 add*/
{
	wait_if(libq_read_status_reg(host).bits.WRITE_ENABLE_LATCH != 1, 1000, "QSPI_FLASH_STATUS.WRITE_ENABLE_LATCH != 1");
}

void libq_flash_wait_write_idle(struct qspi_hc_regs* host)/*ldf 20230614 add*/
{
	wait_if(libq_read_status_reg(host).bits.WRITE_IN_PROGRESS == 1, 1000, "QSPI_FLASH_STATUS.WRITE_IN_PROGRESS==1");
}

void libq_flash_wait_erase_idle(struct qspi_hc_regs* host)/*ldf 20230614 add*/
{
	wait_if(libq_read_flag_status_reg(host).bits.PROGRAM_OR_ERASE_CONTROLLER != 1, 1000000, "QSPI_FLASH_FLAG_STATUS.PROGRAM_OR_ERASE_CONTROLLER != 1");
}

void libq_subsector_erase(struct qspi_hc_regs *host, unsigned int addr, int is_4B_mode)    //subsector
{
	QSPI_HC_CMD cmd = {.v=0};
	
	//lock();
	pthread_mutex_lock2(&muteSem,WAIT, NO_TIMEOUT);/*ldf 20230612 add*/
	
	cmd.bits.IS_WRITE_READ = 1;
	cmd.bits.IS_CONTINUE_SINGLE = 1;
	cmd.bits.RD_NUM_OR_WR_DATA = (is_4B_mode == 1) ? CMD_SSE4BYTE : CMD_SSE;/*ldf 20230612 add*/
	host->qspi_hc_data.v = cmd.v;
	
	if(is_4B_mode)/*ldf 20230612 add*/
	{
	    cmd.bits.IS_WRITE_READ = 1;
	    cmd.bits.IS_CONTINUE_SINGLE = 1;
		cmd.bits.RD_NUM_OR_WR_DATA = (addr>>24)&0xff;
		host->qspi_hc_data.v = cmd.v;
	}
	
	cmd.bits.IS_WRITE_READ = 1;
	cmd.bits.IS_CONTINUE_SINGLE = 1;
	cmd.bits.RD_NUM_OR_WR_DATA = (addr>>16)&0xff;
	host->qspi_hc_data.v = cmd.v;
	
	cmd.bits.IS_WRITE_READ = 1;
	cmd.bits.IS_CONTINUE_SINGLE = 1;
	cmd.bits.RD_NUM_OR_WR_DATA = (addr>>8)&0xff;
	host->qspi_hc_data.v = cmd.v;
	
	cmd.bits.IS_WRITE_READ = 1;
	cmd.bits.IS_CONTINUE_SINGLE = 0;
	cmd.bits.RD_NUM_OR_WR_DATA = (addr>>0)&0xff;
	host->qspi_hc_data.v = cmd.v;
	
	wait_if((0 != host->qspi_hc_status.bits.TXFIFOCNT), DEFAULT_WAIT_CNT, "TXFIFO_NUM != 0");
	wait_if(({
				libq_delay_cnt(1000);
				(libq_read_status_reg(host).bits.WRITE_IN_PROGRESS == 1);
				}), DEFAULT_WAIT_CNT, "program/erase busy:bit7==1");
	
	//unlock();
	pthread_mutex_unlock(&muteSem);/*ldf 20230612 add*/
}

void libq_sector_erase(struct qspi_hc_regs *host, unsigned int addr, int is_4B_mode)    //subsector
{
	QSPI_HC_CMD cmd = {.v=0};
	
	//lock();
	pthread_mutex_lock2(&muteSem,WAIT, NO_TIMEOUT);/*ldf 20230612 add*/
	
	cmd.bits.IS_WRITE_READ = 1;
	cmd.bits.IS_CONTINUE_SINGLE = 1;
	cmd.bits.RD_NUM_OR_WR_DATA = (is_4B_mode == 1) ? CMD_SE4BYTE : CMD_SE;/*ldf 20230612 add*/
	host->qspi_hc_data.v = cmd.v;
	
	if(is_4B_mode)/*ldf 20230612 add*/
	{
	    cmd.bits.IS_WRITE_READ = 1;
	    cmd.bits.IS_CONTINUE_SINGLE = 1;
		cmd.bits.RD_NUM_OR_WR_DATA = (addr>>24)&0xff;
		host->qspi_hc_data.v = cmd.v;
	}
	
    cmd.bits.IS_WRITE_READ = 1;
    cmd.bits.IS_CONTINUE_SINGLE = 1;
	cmd.bits.RD_NUM_OR_WR_DATA = (addr>>16)&0xff;
	host->qspi_hc_data.v = cmd.v;
	
    cmd.bits.IS_WRITE_READ = 1;
    cmd.bits.IS_CONTINUE_SINGLE = 1;
	cmd.bits.RD_NUM_OR_WR_DATA = (addr>>8)&0xff;
	host->qspi_hc_data.v = cmd.v;

	cmd.bits.IS_WRITE_READ = 1;
	cmd.bits.IS_CONTINUE_SINGLE = 0;
	cmd.bits.RD_NUM_OR_WR_DATA = (addr>>0)&0xff;
	host->qspi_hc_data.v = cmd.v;
	
	wait_if((0 != host->qspi_hc_status.bits.TXFIFOCNT), DEFAULT_WAIT_CNT, "TXFIFO_NUM != 0");
//	wait_if(({
//				libq_delay_cnt(1000);
//				(libq_read_status_reg(host).bits.WRITE_IN_PROGRESS == 1);
//				}), DEFAULT_WAIT_CNT, "program/erase busy:bit7==1");
	
	//unlock();
	pthread_mutex_unlock(&muteSem);/*ldf 20230612 add*/
}

void libq_bulk_erase(struct qspi_hc_regs *host)
{
	QSPI_HC_CMD cmd = {.v=0};
	
	//lock();
	pthread_mutex_lock2(&muteSem,WAIT, NO_TIMEOUT);/*ldf 20230612 add*/
	
	cmd.bits.IS_WRITE_READ = 1;
	cmd.bits.RD_NUM_OR_WR_DATA = CMD_BE;
	host->qspi_hc_data.v = cmd.v;
	wait_if((0 != host->qspi_hc_status.bits.TXFIFOCNT), DEFAULT_WAIT_CNT, "TXFIFO_NUM != 0");
//	wait_if(({
//				libq_delay_cnt(1000);
//				(libq_read_status_reg(host).bits.WRITE_IN_PROGRESS == 1);
//				}), DEFAULT_WAIT_CNT, "program/erase busy:bit0==1");
	
	//unlock();
	pthread_mutex_unlock(&muteSem);/*ldf 20230612 add*/
	
	return;
}

void libq_read_device_id(struct qspi_hc_regs* host, unsigned char *pdata)
{
	QSPI_HC_CMD cmd = {.v=0};
	cmd.bits.IS_WRITE_READ = 1;
	cmd.bits.IS_CONTINUE_SINGLE = 1;
	cmd.bits.RD_NUM_OR_WR_DATA = CMD_RDID;
	host->qspi_hc_data.v = cmd.v;


	cmd.v = 0;
	cmd.bits.IS_WRITE_READ = 0;
	cmd.bits.RD_NUM_OR_WR_DATA = 3;
	host->qspi_hc_data.v = cmd.v;

	wait_if((0 == host->qspi_hc_status.bits.RXFIFOCNT), DEFAULT_WAIT_CNT, "RXFIFOCNT == 0");
	int i=0;
	for(i=0; i<4; i++){
		libq_delay_cnt(100);
		pdata[i] = (unsigned char)host->qspi_hc_data.v;
	}
	return;
}

void libq_read(struct qspi_hc_regs *host, unsigned int addr, unsigned char* buffer, unsigned int len, int is_4B_mode)
{
	unsigned int MAX_CMD_LEN=128;
	unsigned int loop_cnt =  (len+MAX_CMD_LEN-1)/MAX_CMD_LEN;
	unsigned int remain_len = len%MAX_CMD_LEN;
	unsigned int loop_len = 0;
	unsigned int buf_index = 0;
	QSPI_HC_CMD cmd = {.v=0};
	
	while(loop_cnt--)
	{
		if (loop_cnt == 0 && remain_len != 0)
		{
			loop_len = remain_len;
		} else {
			loop_len = MAX_CMD_LEN;
		}
		
		//lock();
		pthread_mutex_lock2(&muteSem,WAIT, NO_TIMEOUT);/*ldf 20230612 add*/
		
		//send command
		cmd.v = 0;
		cmd.bits.IS_WRITE_READ = 1;
		cmd.bits.IS_CONTINUE_SINGLE = 1;
		cmd.bits.RD_NUM_OR_WR_DATA = (is_4B_mode == 1) ? CMD_READ4BYTE : CMD_READ;/*ldf 20230612 add*/
		host->qspi_hc_data.v = cmd.v;
		
		if(is_4B_mode)/*ldf 20230612 add*/
		{
			cmd.bits.RD_NUM_OR_WR_DATA = (addr>>24)&0xff;
			host->qspi_hc_data.v = cmd.v;
		}
		
		cmd.bits.RD_NUM_OR_WR_DATA = (addr>>16)&0xff;
		host->qspi_hc_data.v = cmd.v;
		
		cmd.bits.RD_NUM_OR_WR_DATA = (addr>>8)&0xff;
		host->qspi_hc_data.v = cmd.v;
		
		cmd.bits.RD_NUM_OR_WR_DATA = (addr)&0xff;
		host->qspi_hc_data.v = cmd.v;

		cmd.v = 0;
		cmd.bits.IS_WRITE_READ = 0;
		cmd.bits.IS_CONTINUE_SINGLE = 0;
		cmd.bits.RD_NUM_OR_WR_DATA = loop_len - 1;
		host->qspi_hc_data.v = cmd.v;
		wait_if((0 != host->qspi_hc_status.bits.TXFIFOCNT), DEFAULT_WAIT_CNT, "TXFIFOCNT != 0");
		wait_if((MAX_CMD_LEN > host->qspi_hc_status.bits.RXFIFOCNT), DEFAULT_WAIT_CNT, "MAX_CMD_LEN > RXFIFOCNT");
		//read to buf
		addr += loop_len;
		while(loop_len--)
		{
			buffer[buf_index++] = host->qspi_hc_data.bits.BYTE0;
		}
		
		//unlock();
		pthread_mutex_unlock(&muteSem);/*ldf 20230612 add*/
	}
}

void libq_dual_read(struct qspi_hc_regs* host, unsigned int addr, unsigned char *buffer, unsigned int len)
{
	unsigned int MAX_CMD_LEN=128;
	unsigned int loop_cnt =  (len+MAX_CMD_LEN-1)/MAX_CMD_LEN;
	unsigned int remain_len = len%MAX_CMD_LEN;
	unsigned int loop_len = 0;
	unsigned int buf_index = 0;

	QSPI_HC_CMD cmd = {.v=0};
	while(loop_cnt--)
	{
		if (loop_cnt == 0 && remain_len != 0)
		{
			loop_len = remain_len;
		} else {
			loop_len = MAX_CMD_LEN;
		}
		
		//lock();
		pthread_mutex_lock2(&muteSem,WAIT, NO_TIMEOUT);/*ldf 20230612 add*/
		
		//send command
		cmd.v = 0;
		cmd.bits.IS_WRITE_READ = 1;
		cmd.bits.IS_CONTINUE_SINGLE = 1;
		cmd.bits.RD_NUM_OR_WR_DATA = DUAL_CMD_4DIOR;
		host->qspi_hc_data.v = cmd.v;

		cmd.bits.RD_NUM_OR_WR_DATA = (addr>>24)&0xff;
		host->qspi_hc_data.v = cmd.v;
		cmd.bits.RD_NUM_OR_WR_DATA = (addr>>16)&0xff;
		host->qspi_hc_data.v = cmd.v;
		cmd.bits.RD_NUM_OR_WR_DATA = (addr>>8)&0xff;
		host->qspi_hc_data.v = cmd.v;

		cmd.bits.DUMMY_CYCLE_CLK_EN = 1;
		cmd.bits.DUMMY_CYCLE_EN = 1;
		cmd.bits.DUMMY_CYCLES = 7;
		cmd.bits.RD_NUM_OR_WR_DATA = (addr)&0xff;
		host->qspi_hc_data.v = cmd.v;

		cmd.v = 0;
		cmd.bits.IS_WRITE_READ = 0;
		cmd.bits.IS_CONTINUE_SINGLE = 0;
		cmd.bits.RD_NUM_OR_WR_DATA = loop_len-1;
		host->qspi_hc_data.v = cmd.v;
		wait_if((0 != host->qspi_hc_status.bits.TXFIFOCNT), DEFAULT_WAIT_CNT, "TXFIFOCNT != 0");
		wait_if((MAX_CMD_LEN > host->qspi_hc_status.bits.RXFIFOCNT), DEFAULT_WAIT_CNT, "MAX_CMD_LEN > RXFIFOCNT");
		//read to buf
		addr += loop_len;
		while(loop_len--)
		{
			buffer[buf_index++] = (unsigned char)(host->qspi_hc_data.v);
		}
		
		//unlock();
		pthread_mutex_unlock(&muteSem);/*ldf 20230612 add*/
	}
}

void libq_quad_read(struct qspi_hc_regs* host, unsigned int addr, unsigned char *buffer, unsigned int len)
{
	unsigned int MAX_CMD_LEN=128;
	unsigned int loop_cnt =  (len+MAX_CMD_LEN-1)/MAX_CMD_LEN;
	unsigned int remain_len = len%MAX_CMD_LEN;
	unsigned int loop_len = 0;
	unsigned int buf_index = 0;

	QSPI_HC_CMD cmd = {.v=0};
	while(loop_cnt--)
	{
		if (loop_cnt == 0 && remain_len != 0)
		{
			loop_len = remain_len;
		} else {
			loop_len = MAX_CMD_LEN;
		}
		
		//lock();
		pthread_mutex_lock2(&muteSem,WAIT, NO_TIMEOUT);/*ldf 20230612 add*/
		
		//send command
		cmd.v = 0;
		cmd.bits.IS_WRITE_READ = 1;
		cmd.bits.IS_CONTINUE_SINGLE = 1;
		cmd.bits.RD_NUM_OR_WR_DATA = QUAD_CMD_4QIOR;
		host->qspi_hc_data.v = cmd.v;

		cmd.bits.RD_NUM_OR_WR_DATA = (addr>>24)&0xff;
		host->qspi_hc_data.v = cmd.v;
		cmd.bits.RD_NUM_OR_WR_DATA = (addr>>16)&0xff;
		host->qspi_hc_data.v = cmd.v;
		cmd.bits.RD_NUM_OR_WR_DATA = (addr>>8)&0xff;
		host->qspi_hc_data.v = cmd.v;

		cmd.bits.DUMMY_CYCLE_CLK_EN = 1;
		cmd.bits.DUMMY_CYCLE_EN = 1;
		cmd.bits.DUMMY_CYCLES = 9;
		cmd.bits.RD_NUM_OR_WR_DATA = (addr)&0xff;
		host->qspi_hc_data.v = cmd.v;

		cmd.v = 0;
		cmd.bits.IS_WRITE_READ = 0;
		cmd.bits.IS_CONTINUE_SINGLE = 0;
		cmd.bits.RD_NUM_OR_WR_DATA = loop_len-1;
		host->qspi_hc_data.v = cmd.v;
		wait_if((0 != host->qspi_hc_status.bits.TXFIFOCNT), DEFAULT_WAIT_CNT, "TXFIFOCNT != 0");
		wait_if((MAX_CMD_LEN > host->qspi_hc_status.bits.RXFIFOCNT), DEFAULT_WAIT_CNT, "MAX_CMD_LEN > RXFIFOCNT");
		addr += loop_len;
		//read to buf
		while(loop_len--)
		{
			buffer[buf_index++] = host->qspi_hc_data.bits.BYTE0;
		}
		
		//unlock();
		pthread_mutex_unlock(&muteSem);/*ldf 20230612 add*/
	}
}

void libq_program(struct qspi_hc_regs *host, unsigned int addr, unsigned char* buffer, unsigned int len, int is_4B_mode)
{
	unsigned int MAX_CMD_LEN=128;
	unsigned int loop_cnt =  (len+MAX_CMD_LEN-1)/MAX_CMD_LEN;
	unsigned int remain_len = len%MAX_CMD_LEN;
	unsigned int loop_len = 0;
	unsigned int buf_index = 0;
	unsigned char val = 0;
	QSPI_HC_CMD cmd = {.v=0};
	int i=0;

	while(loop_cnt--)
	{
		if (loop_cnt == 0 && remain_len != 0)
		{
			loop_len = remain_len;
		} else {
			loop_len = MAX_CMD_LEN;
		}

		//lock();
		pthread_mutex_lock2(&muteSem,WAIT, NO_TIMEOUT);/*ldf 20230612 add*/
		
		libq_write_enable(host);
		libq_flash_wait_write_enable(host);/*ldf 20230614 add*/
		
		/*send command*/
		cmd.bits.IS_WRITE_READ = 1;
		cmd.bits.IS_CONTINUE_SINGLE = 1;
		cmd.bits.RD_NUM_OR_WR_DATA = (is_4B_mode == 1) ? CMD_PP4BYTE : CMD_PP;/*ldf 20230612 add*/
		host->qspi_hc_data.v = cmd.v;
		
		if(is_4B_mode)/*ldf 20230612 add*/
		{
			cmd.bits.RD_NUM_OR_WR_DATA = (addr >>24)&0xff;
			host->qspi_hc_data.v = cmd.v;
		}
		
		cmd.bits.RD_NUM_OR_WR_DATA = (addr >>16)&0xff;
		host->qspi_hc_data.v = cmd.v;
		
		cmd.bits.RD_NUM_OR_WR_DATA = (addr >>8)&0xff;
		host->qspi_hc_data.v = cmd.v;
		
		cmd.bits.RD_NUM_OR_WR_DATA = (addr >>0)&0xff;
		host->qspi_hc_data.v = cmd.v;

		/*write data*/
		for (i = 0; i < loop_len; i++) {
			if (i == (loop_len-1)) {
				cmd.bits.IS_CONTINUE_SINGLE = 0;
				cmd.bits.RD_NUM_OR_WR_DATA = buffer[i+buf_index];
				host->qspi_hc_data.v = cmd.v;
			} else {
				cmd.bits.IS_CONTINUE_SINGLE = 1;
				cmd.bits.RD_NUM_OR_WR_DATA = buffer[i+buf_index];
				host->qspi_hc_data.v = cmd.v;
			}
		}
//		wait_if(({
//					(libq_read_status_reg(host).bits.WRITE_IN_PROGRESS == 1);
//					}), DEFAULT_WAIT_CNT, "program/erase busy:bit7==1");
		libq_flash_wait_write_idle(host);/*ldf 20230612 add*/
		
		//unlock();
		pthread_mutex_unlock(&muteSem);/*ldf 20230612 add*/
		
		//udebug("addr=0x%x,buf_index=0x%x\r\n", addr, buf_index);
		buf_index += loop_len;
		addr += loop_len;
	}
}

void libq_dual_program(struct qspi_hc_regs*host, unsigned int addr, unsigned char *buffer, unsigned int len)
{
	unsigned int MAX_CMD_LEN=128;
	unsigned int loop_cnt =  (len+MAX_CMD_LEN-1)/MAX_CMD_LEN;
	unsigned int remain_len = len%MAX_CMD_LEN;
	unsigned int loop_len = 0;
	unsigned int buf_index = 0;
	unsigned char val = 0;
	QSPI_HC_CMD cmd = {.v=0};
	int i=0;

	while(loop_cnt--)
	{
		if (loop_cnt == 0 && remain_len != 0)
		{
			loop_len = remain_len;
		} else {
			loop_len = MAX_CMD_LEN;
		}
		
		//lock();
		pthread_mutex_lock2(&muteSem,WAIT, NO_TIMEOUT);/*ldf 20230612 add*/
		
		cmd.bits.IS_WRITE_READ = 1;
		cmd.bits.IS_CONTINUE_SINGLE = 1;
		cmd.bits.RD_NUM_OR_WR_DATA = DUAL_INPUT_FAST_PROGRAM;
		host->qspi_hc_data.v = cmd.v;
		cmd.bits.RD_NUM_OR_WR_DATA = (addr >>16)&0xff;
		host->qspi_hc_data.v = cmd.v;
		cmd.bits.RD_NUM_OR_WR_DATA = (addr >>8)&0xff;
		host->qspi_hc_data.v = cmd.v;
		cmd.bits.RD_NUM_OR_WR_DATA = (addr >>0)&0xff;
		host->qspi_hc_data.v = cmd.v;

		for (i = 0; i < loop_len; i++) {
			if (i == (loop_len-1)) {
				cmd.bits.IS_CONTINUE_SINGLE = 0;
				cmd.bits.RD_NUM_OR_WR_DATA = buffer[i+buf_index];
				host->qspi_hc_data.v = cmd.v;
			} else {
				cmd.bits.IS_CONTINUE_SINGLE = 1;
				cmd.bits.RD_NUM_OR_WR_DATA = buffer[i+buf_index];
				host->qspi_hc_data.v = cmd.v;
			}
		}
		wait_if(({
					(libq_read_status_reg(host).bits.WRITE_IN_PROGRESS == 1);
					}), DEFAULT_WAIT_CNT, "program/erase busy:bit7==1");
		
		//unlock();
		pthread_mutex_unlock(&muteSem);/*ldf 20230612 add*/
		
		buf_index += loop_len;
		addr += loop_len;

	}
}

void libq_quad_program(struct qspi_hc_regs*host, unsigned int addr, unsigned char *buffer, unsigned int len)
{
	unsigned int MAX_CMD_LEN=128;
	unsigned int loop_cnt =  (len+MAX_CMD_LEN-1)/MAX_CMD_LEN;
	unsigned int remain_len = len%MAX_CMD_LEN;
	unsigned int loop_len = 0;
	unsigned int buf_index = 0;
	unsigned char val = 0;
	QSPI_HC_CMD cmd = {.v=0};
	int i=0;

	while(loop_cnt--)
	{
		if (loop_cnt == 0 && remain_len != 0)
		{
			loop_len = remain_len;
		} else {
			loop_len = MAX_CMD_LEN;
		}

		//lock();
		pthread_mutex_lock2(&muteSem,WAIT, NO_TIMEOUT);/*ldf 20230612 add*/
		
		cmd.bits.IS_WRITE_READ = 1;
		cmd.bits.IS_CONTINUE_SINGLE = 1;
		cmd.bits.RD_NUM_OR_WR_DATA = QUAD_INPUT_FAST_PROGRAM;
		host->qspi_hc_data.v = cmd.v;
		cmd.bits.RD_NUM_OR_WR_DATA = (addr >>16)&0xff;
		host->qspi_hc_data.v = cmd.v;
		cmd.bits.RD_NUM_OR_WR_DATA = (addr >>8)&0xff;
		host->qspi_hc_data.v = cmd.v;
		cmd.bits.RD_NUM_OR_WR_DATA = (addr >>0)&0xff;
		host->qspi_hc_data.v = cmd.v;

		for (i = 0; i < loop_len; i++) {
			if (i == (loop_len-1)) {
				cmd.bits.IS_CONTINUE_SINGLE = 0;
				cmd.bits.RD_NUM_OR_WR_DATA = buffer[i+buf_index];
				host->qspi_hc_data.v = cmd.v;
			} else {
				cmd.bits.IS_CONTINUE_SINGLE = 1;
				cmd.bits.RD_NUM_OR_WR_DATA = buffer[i+buf_index];
				host->qspi_hc_data.v = cmd.v;
			}
		}
		wait_if(({
					(libq_read_status_reg(host).bits.WRITE_IN_PROGRESS == 1);
					}), DEFAULT_WAIT_CNT, "program/erase busy:bit7==1");
		
		//unlock();
		pthread_mutex_unlock(&muteSem);/*ldf 20230612 add*/
		
		buf_index += loop_len;
		addr += loop_len;
	}
}

QSPI_FLASH_ENHANCED_VOLATILE_CONF_REG libq_read_enhanced_volatile_configuration_reg(struct qspi_hc_regs* host)
{
	QSPI_HC_CMD cmd = {.v=0};
	cmd.bits.IS_WRITE_READ = 1;
	cmd.bits.IS_CONTINUE_SINGLE = 1;
	cmd.bits.RD_NUM_OR_WR_DATA = CMD_REVCR;
	host->qspi_hc_data.v = cmd.v;

	cmd.bits.IS_WRITE_READ = 0;
	cmd.bits.IS_CONTINUE_SINGLE = 0;
	cmd.bits.RD_NUM_OR_WR_DATA = 0;
	host->qspi_hc_data.v = cmd.v;
	wait_if((0 != host->qspi_hc_status.bits.TXFIFOCNT), DEFAULT_WAIT_CNT, "TXFIFOCNT != 0");
	wait_if((0 == host->qspi_hc_status.bits.RXFIFOCNT), DEFAULT_WAIT_CNT, "RXFIFOCNT == 0");
	QSPI_FLASH_ENHANCED_VOLATILE_CONF_REG ret = {.v = (unsigned char)(host->qspi_hc_data.v)};
	return ret;
}

void libq_write_enhanced_volatile_configuration_reg(struct qspi_hc_regs* host, QSPI_FLASH_ENHANCED_VOLATILE_CONF_REG data)    //WRITE_ENHANCED_VOLATILE_CONFIGURATION_REGISTER
{
	libq_write_enable(host);
	libq_flash_wait_write_enable(host);/*ldf 20230614 add*/
	
	QSPI_HC_CMD cmd = {.v=0};
	cmd.bits.IS_WRITE_READ = 1;
	cmd.bits.IS_CONTINUE_SINGLE = 1;
	cmd.bits.RD_NUM_OR_WR_DATA = CMD_WEVCR;
	host->qspi_hc_data.v = cmd.v;
	cmd.bits.IS_CONTINUE_SINGLE = 0;
	cmd.bits.RD_NUM_OR_WR_DATA = data.v;
	host->qspi_hc_data.v = cmd.v;
	wait_if((0 != host->qspi_hc_status.bits.TXFIFOCNT), DEFAULT_WAIT_CNT, "TXFIFOCNT != 0");
}

void libq_cmd_transfer(struct qspi_hc_regs* host, unsigned int cmdone, int is_write, int is_continue)
{
	QSPI_HC_CMD cmd = {.v=0};
	unsigned int val = 0;
	
	//lock();
	pthread_mutex_lock2(&muteSem,WAIT, NO_TIMEOUT);/*ldf 20230612 add*/
	
	if (is_write)
	{
		libq_write_enable(host);
		libq_flash_wait_write_enable(host);/*ldf 20230614 add*/
	}

	cmd.bits.IS_WRITE_READ = is_write;
	cmd.bits.IS_CONTINUE_SINGLE = is_continue;
	cmd.bits.RD_NUM_OR_WR_DATA = cmdone;
	host->qspi_hc_data.v = cmd.v;
	wait_if((0 != host->qspi_hc_status.bits.TXFIFOCNT), DEFAULT_WAIT_CNT, "TXFIFOCNT != 0");

	if (is_write == 0)
	{
		wait_if((0 == host->qspi_hc_status.bits.RXFIFOCNT), DEFAULT_WAIT_CNT, "RXFIFOCNT == 0");
		int i = 0;
		printf("read: ");
		for (i=0; i<cmdone+1; i++)
		{
			val = host->qspi_hc_data.bits.BYTE0;
			printf("0x%x  ", val);
		}
		printf("\r\n");
	}
	
	//unlock();
	pthread_mutex_unlock(&muteSem);/*ldf 20230612 add*/
}

void libq_set_addr_mode(unsigned int is_3byte_addr)/*ldf 20230613 add*/
{
	struct qspi_hc_regs* host = QSPI0_HC_REGS;
	QSPI_FLASH_NONVOLATILE_CONFIGURATION ret = {.v = 0};
	QSPI_HC_CMD cmd = {.v=0};
	
	ret = libq_read_nonvolatile_config_reg(host);
	printk("<**DEBUG**> [%s():_%d_]:: read nonvolatile config: 0x%x\n", __FUNCTION__, __LINE__,ret);
	ret.bits.ADDRESS_MODE = is_3byte_addr;/*0 = Enable 4B address, 1 = Enable 3B address (Default)*/

	cmd.bits.IS_WRITE_READ = 1;
	cmd.bits.IS_CONTINUE_SINGLE = 1;
	cmd.bits.RD_NUM_OR_WR_DATA = CMD_WNVCR;
	host->qspi_hc_data.v = cmd.v;
	cmd.bits.RD_NUM_OR_WR_DATA = ret.v >> 8;;
	host->qspi_hc_data.v = cmd.v;
	cmd.bits.RD_NUM_OR_WR_DATA = ret.v & 0xff;
	host->qspi_hc_data.v = cmd.v;
	wait_if(({
				(libq_read_status_reg(host).bits.WRITE_IN_PROGRESS == 1);
				}), DEFAULT_WAIT_CNT, "program/erase busy:bit0==1");
	
	usleep(100000);
	
	ret = libq_read_nonvolatile_config_reg(host);
	if(ret.bits.ADDRESS_MODE != is_3byte_addr)
	{
		printk("<ERROR> [%s():_%d_]:: set addr mode failed! ret=0x%x\n", __FUNCTION__, __LINE__,ret);
	}
}

void qspi_module_init(void)/*ldf 20230608 add*/
{
	if(qspi_init_flag)/*ldf 20230612 add*/
		return;
	
	struct qspi_hc_regs* host = QSPI0_HC_REGS;
	u32 val = 0;

#if 0 /*xxx ldf 20230614:: uboot下会初始化一遍，这里可以注释掉*/
    /*配置其它寄存器的通道解锁*/
    phx_write_u32(0x1f0c0028, 0xa5accede); // unlock
	/*qspi配置release reset*/
	val = phx_read_u32(0x1f0c0004);
    phx_write_u32(0x1f0c0004, val & (~(1<<19)));
    
	libq_set_mode_line(host, 0);
	libq_set_mode_bl(host, 1);
	libq_set_mode_cpol(host, 1);
	libq_set_mode_cpha(host, 1);
	libq_set_mode_fdiv(host, 4);

	libq_set_addr_mode(0);/*ldf 20230613 add:: 设置为4byte addr*/
#endif
	pthread_mutex_init2(&muteSem,PTHREAD_MUTEX_RECURSIVE,
			RE_MUTEX_PRIO_NONE |RE_MUTEX_WAIT_FIFO ,PTHREAD_MUTEX_CEILING); /*ldf 20230612 add*/
	
	qspi_init_flag = 1;/*ldf 20230612 add*/
}

int QspiFlashEraseAll(void)
{
	struct qspi_hc_regs* host = QSPI0_HC_REGS;
	
	libq_write_enable(host);
	libq_flash_wait_write_enable(host);/*ldf 20230614 add*/
	
	libq_bulk_erase(host);
	
	sleep(3);/*ldf 20230612 add*/
	libq_flash_wait_write_idle(host);/*ldf 20230612 add*/
	libq_flash_wait_erase_idle(host);/*ldf 20230613 add*/
	
	return 0;
}
int QspiFlashErase(unsigned int addr, unsigned int len)
{
	unsigned int erase_size = SECTOR_SIZE;
	struct qspi_hc_regs* host = QSPI0_HC_REGS;
	unsigned int erase_addr = 0;
	int erase_loop = 0;
	int i = 0;
	
	if(len < erase_size)
	{
		printf("<ERROR> len(0x%x) is too small! must >= 0x%x\n",len,erase_size);
		return;
	}
	
	erase_loop = len / erase_size;
	
	for(i=0;i<erase_loop;i++)
	{
		erase_addr = addr + i * erase_size;
		
		libq_write_enable(host);
		libq_flash_wait_write_enable(host);/*ldf 20230614 add*/
		
		libq_sector_erase(host, erase_addr, 0);
		
		usleep(10000);/*ldf 20230612 add*/
		libq_flash_wait_write_idle(host);/*ldf 20230612 add*/
		libq_flash_wait_erase_idle(host);/*ldf 20230613 add*/
	}
	
	return 0;
}
unsigned int QspiFlashReadid(void)
{
	struct qspi_hc_regs* host = QSPI0_HC_REGS;
	unsigned int id;;
	
	libq_read_device_id(host, (unsigned char*)&id);
	
	return id;
}
int QspiFlashRead(unsigned int addr, unsigned char* rbuf, unsigned int len)
{
	struct qspi_hc_regs* host = QSPI0_HC_REGS;
	
	libq_read(host, addr, rbuf, len, 0);
}
int QspiFlashWrite(unsigned int addr, unsigned char* wbuf, unsigned int len)
{
	struct qspi_hc_regs* host = QSPI0_HC_REGS;
	
	libq_program(host, addr, wbuf, len, 0);
}

static int CheckUint8(const unsigned char *pSrc, const unsigned char *pDst, unsigned int length)
{
    int index = 0;
    unsigned char valSrc = 0;
    unsigned char valDst = 0;
    int err_num = 0;
    
    printf(" ##CheckData## pSrc: 0x%016lX, pDst: 0x%016lX, length: %lu = 0x%016lX\n", (unsigned long)pSrc, (unsigned long)pDst, length, length);
    for(index = 0; index < length; index ++)
    {
    	valSrc = *(pSrc + index);
    	valDst = *(pDst + index);
		if(valSrc != valDst)
		{
			err_num++;
			if(err_num <= 0x10)
			{
				printf("<Check Error> index=0x%x, val=0x%x, except_val=0x%x\n",\
						index,valDst,valSrc);
			}
//			return -1;
		}
    }
    
    if(err_num == 0)
    	printf("   Check OK!\n");
    else
    	printf("   Check Failed! err_num = 0x%x\n",err_num);
    
	return err_num;
}
static void DumpUint8(const unsigned char *pAddr, unsigned int length)
{
    int index = 0;

    printf("pAddr: 0x%016lX, length: %lu = 0x%016lX", (unsigned long)pAddr, length, length);
    for (index = 0; index < length; index++)
    {
        if (index % 0x10 == 0)
        {
            printf("\n0x%08lX: ", index);
        }
        printf("0x%02X ", *(unsigned char *)(pAddr + index));
    } 

    printf("\n");
}
void Dump_qspi_flash(unsigned int addr, unsigned int len)
{
	int i;
	
	u8* rbuf = malloc(len);
	
	QspiFlashRead(addr, rbuf, len);
//	printf("<DEBUG> [%s:__%d__]\n",__FUNCTION__,__LINE__);
	DumpUint8(rbuf, len);
//	printf("<DEBUG> [%s:__%d__]\n",__FUNCTION__,__LINE__);
//	printf("val = ");
//	for(i=0;i<len; i++)
//	{
//		printf("0x%x ", rbuf[i]);
//	}
//	printf("\n");
	
	free(rbuf);
	return;
}



void test_qspiflash_write(unsigned int addr, unsigned int len, unsigned char first_data)
{
    unsigned char *data = (unsigned char *)malloc(len);
    int i = 0;
    
    memset(data, 0, len);
    
    for(i = 0; i < len; i ++)
    {
        data[i] = i + first_data;
//        printf("0x%x ", data[i]);
    }
//    printf("\n");
    
    printf("\nafter write =====================\n");
    QspiFlashWrite(addr, data, len);
    
    free(data);
    
	return;
}

void test_qspiflash_rw(unsigned int addr, unsigned int len, unsigned char first_data, int is_DumpData)
{
    unsigned char *buf = (unsigned char *)malloc(len);
    unsigned char *data = (unsigned char *)malloc(len);
    int i = 0;
    unsigned int retid;
    int err_num = 0;
    
    memset(buf, 0, len);
    memset(data, 0, len);

    printf("\n=======QSPI FLASH TEST START=========\n");
    
    qspi_module_init();

    retid = QspiFlashReadid();
    printf("Qspi flash id= 0x%x\n", retid);

    printf("\nfirst read =====================\n");
    QspiFlashRead(addr, buf, len);
	if(is_DumpData)
		DumpUint8(buf, len);
    
    printf("\nafter erase =====================\n"); 
    QspiFlashErase(addr, len);
    memset(buf, 0, len);
    QspiFlashRead(addr, buf, len);
	if(is_DumpData)
		DumpUint8(buf, len);
    memset(data, 0xff, len);
    err_num = CheckUint8(data, buf, len);
    if(err_num > 0)
    	printf("  Qspi Flash Erase Test Failed!, err_num = 0x%x\n",err_num); 
    else
    	printf("  Qspi Flash Erase Test OK!\n"); 
    
    for(i = 0; i < len; i ++)
    {
        data[i] = i + first_data;
//        printf("0x%x ", data[i]);
    }
//    printf("\n");
    
    printf("\nafter write =====================\n");
    QspiFlashWrite(addr, data, len);
    memset(buf, 0, len);
    QspiFlashRead(addr, buf, len);
	if(is_DumpData)
		DumpUint8(buf, len);
    err_num = CheckUint8(data, buf, len);
    if(err_num > 0)
    	printf("  Qspi Flash Write Test Failed!, err_num = 0x%x\n",err_num); 
    else
    	printf("  Qspi Flash Write Test OK!\n");
	
    printf("\n=======QSPI FLASH TEST END=========\n");
    
    free(buf);
    free(data);
}

#endif  /* __QSPI_LIB_V2_C__ */
