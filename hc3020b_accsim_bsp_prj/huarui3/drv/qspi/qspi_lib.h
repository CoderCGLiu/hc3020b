/* ******************************************************************************************
 * FILE NAME   : qspi_lib.h
 * PROGRAMMER  : zhaozz
 * DESCRIPTION : qspi controller
 * DATE        : 2022-04-13 15:12:20
 * *****************************************************************************************/
#ifndef  __QSPI_LIB_V2_H__
#define  __QSPI_LIB_V2_H__

typedef union {
	volatile unsigned int v;
	struct {
		volatile unsigned int FDIV         :4;
		volatile unsigned int CPHA         :1;
		volatile unsigned int CPOL         :1;
		volatile unsigned int MODE         :2; //7-6  0-spi 1-1line 2-2line 3-4line
		volatile unsigned int BLMODE       :1; //0-little  1-big
		volatile unsigned int APBTO_EN     :1;
		volatile unsigned int READDELAY    :1;
		volatile unsigned int BOOTBLMODE   :1; //0-little  1-big
		volatile unsigned int __RESV       :4;
		volatile unsigned int APBTO        :16;
	} bits;
} QSPI_HC_CTRL; //default 0x0fff0700

typedef union {
	volatile unsigned int v;
	struct {
		volatile unsigned int SETUP        :8;
		volatile unsigned int HOLD         :8;
		volatile unsigned int WAIT1        :8;
		volatile unsigned int __RESV       :8;
	} bits;
} QSPI_HC_TIMING; //default 0x00010101

typedef union {
	volatile unsigned int v;
	struct {
		volatile unsigned int BYTE0        :8;
		volatile unsigned int BYTE1        :8;
		volatile unsigned int BYTE2        :8;
		volatile unsigned int BYTE3        :8;
	} bits;
} QSPI_HC_DATA;

typedef union {
	volatile unsigned int v;
	struct {
		volatile unsigned int RXFIFOCNT     :8;
		volatile unsigned int TXFIFOCNT     :5;
		volatile unsigned int __RESV        :3;
		volatile unsigned int CMDFIFOCNT    :4;
		volatile unsigned int INNERFSM      :4;
		volatile unsigned int __RESV1       :11; // 8 /* lcg 20230920 �޸� */ 
	} bits;
} QSPI_HC_STATUS;

typedef union {
	volatile unsigned int v;
	struct {
		volatile unsigned int CLR_RXFIFO    :1;
		volatile unsigned int CLR_TXFIFO    :1;
		volatile unsigned int CLR_CMDFIFO   :1;
	} bits;
} QSPI_HC_FIFO_CLR;

typedef union {
	volatile unsigned int v;
	struct {
		volatile unsigned int LINE4_RLEN    :8;
		volatile unsigned int LINE4_DUMMY   :4;
		volatile unsigned int MODE          :1; //0-gen default boot cmd , 1-gen boot cmd using QSPI_HC_BOOT_CFG
	} bits;
} QSPI_HC_BOOT_CFG; //default 0x60B

typedef union {
	volatile unsigned int v;
	struct {
		volatile unsigned int INT_STS_BIT0_EN  :1;
		volatile unsigned int INT_STS_BIT1_EN  :1;
		volatile unsigned int INT_STS_BIT2_EN  :1;
		volatile unsigned int INT_STS_BIT3_EN  :1;
		volatile unsigned int INT_STS_BIT0_CLR :1;
		volatile unsigned int INT_STS_BIT1_CLR :1;
		volatile unsigned int INT_STS_BIT2_CLR :1;
		volatile unsigned int INT_STS_BIT3_CLR :1;
		volatile unsigned int INT_TRIGGER      :8; //15-8
	} bits;
} QSPI_HC_INT_EN_CFG;

typedef union {
	volatile unsigned int v;
	struct {
		volatile unsigned int RXFIFO_TRIGGER      :1;
		volatile unsigned int RXFIFO_OVERFLOW     :1;
		volatile unsigned int TXFIFO_UNDERFLOW    :1;
		volatile unsigned int APB_TIMEOUT         :1;
		volatile unsigned int CLEAR_MODE          :4; // 1 /* lcg 200230920 �޸� */ //0-read clear,  1-write clear
	} bits;
} QSPI_HC_INT_STATUS;

typedef union {
	volatile unsigned int v;
	struct {
		volatile unsigned int RD_NUM_OR_WR_DATA   :8;
		volatile unsigned int IS_WRITE_READ       :1;
		volatile unsigned int IS_CONTINUE_SINGLE  :1;
		volatile unsigned int BUFFER_MODE_EN      :1;
		volatile unsigned int DUMMY_CYCLE_EN      :1; //11
		volatile unsigned int BUFFER_NUM          :4; //15-12
		volatile unsigned int DUMMY_CYCLES        :4; //19-16
		volatile unsigned int AUTO_MODE           :2;
		volatile unsigned int AUTO_MODE_CONFIG    :1;
		volatile unsigned int DUMMY_CYCLE_CLK_EN  :1;
		volatile unsigned int BOOT                :2; //25-24
	} bits;
} QSPI_HC_CMD;

struct qspi_hc_regs {
	volatile QSPI_HC_CTRL         qspi_hc_ctrl;
	volatile QSPI_HC_TIMING       qspi_hc_timing;
	volatile QSPI_HC_DATA         qspi_hc_data;
	volatile QSPI_HC_STATUS       qspi_hc_status;
	volatile QSPI_HC_FIFO_CLR     qspi_hc_fifo_clr;
	volatile QSPI_HC_BOOT_CFG     qspi_hc_boot_cfg;
	volatile QSPI_HC_INT_EN_CFG   qspi_hc_int_en_cfg;
	volatile QSPI_HC_INT_STATUS   qspi_hc_int_status;
};




#define QSPI_REG_BASE (0x9000000000000000 | 0x1F910000)
#define QSPI0_HC_REGS ((struct qspi_hc_regs*)QSPI_REG_BASE)


#endif  /* __QSPI_LIB_V2_H__ */
