/*
 * porting.c
 *
 *  Created on: 2018-5-24
 *      Author: Administrator
 */
#include <stdio.h>
#include <taskLib.h>
#include <spinLocklib.h>

#include <cpu.h>
u32 vxCpuIndexGet()
{
	return cpu_id_get();
}

extern u32 get_c0_count();
u32 sysCountGet (void)
{
	return get_c0_count();
}

FILE **	__stdin(void)
{
	return NULL;
}

FILE **	__stdout(void)
{
	return NULL;
}

FILE **	__stderr(void)
{
	return NULL;
}

#if 1
void * t_spinlockIsrInit()
{
	spinlockIsr_t * pIsrLock;
	pIsrLock = malloc(sizeof(spinlockIsr_t));
	if(pIsrLock == NULL)
	{
		printf("!!!Error,t_spinlockIsrInit:: Init failed.\r\n");
		return NULL;
	}

	spinLockIsrInit(pIsrLock, 0);
	return (void *)pIsrLock;
}
void t_spinlockIsrTake(void *lock_id)
{
	if(lock_id == NULL)
	{
		printf("!!!Error,t_spinlockIsrTake:: lock id is Null.\r\n");
		return;
	}

	spinLockIsrTake((spinlockIsr_t *)lock_id);
}
void t_spinlockIsrGive(void *lock_id)
{
	if(lock_id == NULL)
	{
		printf("!!!Error,t_spinlockIsrGive:: lock id is Null.\r\n");
		return;
	}

	spinLockIsrGive((spinlockIsr_t *)lock_id);
}
#endif
