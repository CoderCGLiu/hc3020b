/*! 
 * Copyright (c) 2014, ZZKK, CETC32
 * All rights reserved.
 * 版          权：  中国电子科技集团公司第三十二研究所
 * 文件名称: netUtils.c
 * 文件标识：自定义四华睿网络接口
 * 文件简介：本文件定义了通过ftp/tftp操作文件及加载模块的接口 * 
 * 当前版本: 1.0
 * 作          者： FuKai
 * 修改日期： 2014年12月8日
 */

#include <vxWorks.h>
#include <stdio.h>
#include <stdLib.h>
#include <string.h>
#include <ftpLib.h>

#include <reworksio.h>
//#undef _DEBUG
//#include "tool/netUtils.h"

#define _BUFSIZE     BUFSIZ
#define _TMPDIR		"/tmpld"
#define _MAX_PATH	256

#define FTP_FUNC_DEF(func, args...)  func(char * pFtpName, char * pUser, char * pPasswd,##args)
#define FTP_FUNC_CALL(func, args...)  func(pFtpName, pUser, pPasswd,##args)
#define CHECK_NOEMPTY_STR(f) ((f != NULL)? (strlen(f) > 0) : FALSE)

#define PATH2DIR(p) if (p[strlen(p)-1] == '/') p[strlen(p)-1]='\0';
#define DIR2PATH(p) if (p[strlen(p)-1] != '/') {p[strlen(p)]='/';p[strlen(p)+1]='\0';}
#define NULL2EMPTY(str)  (str == NULL ? "": str)
#define TRIM_TAIL(str) \
{\
	char * p = str + strlen(str)-1 ;\
	while((*p == 9) || (*p == 10) || (*p == 13)||(*p == 32)) \
	{ \
		*p = '\0'; \
		if (p == str) break; \
		else p--;\
	}\
}

#define GOTO_END_OF_FUNC(ret) \
	retVal = ret; \
	goto end_of_func;

LOCAL int checkModuleFileName(char * pOutfile);
LOCAL BOOL checkDirExists(char * dirname, BOOL bMkDirIfNotExists, BOOL bLogErr);
LOCAL BOOL checkFileExists(char * filename);
LOCAL void splitFilePath(char * filepath, char ** dir, char ** fname, char ** ext);

IMPORT STATUS ftpXfer(char *,char *,char *,char *,char *,char *,char *,int *, int * );


static char ftpServer[16] = {0};
static char ftpUser[16] = {0};
static char ftpPasswd[16] = {0};
void ftpServerConfig(char * server, char * user, char * passwd)
{
	printf("Change ftp server: \"%s\" \"%s\" \"%s\" ==> ", ftpServer, ftpUser, ftpPasswd);
	strncpy(ftpServer, NULL2EMPTY(server), 15);
	strncpy(ftpUser, NULL2EMPTY(user), 15);
	strncpy(ftpPasswd, NULL2EMPTY(passwd), 15);
	printf("\"%s\" \"%s\" \"%s\"\n", ftpServer, ftpUser, ftpPasswd);
}

#define SIMPLE_FTP_FUNC_DEF(func,arg0,args...) func##_s(arg0,##args) 
#define SIMPLE_FTP_FUNC_CALL(func,arg0,args...) func(ftpServer, ftpUser, ftpPasswd,arg0,##args)

#define FREE(p) if(p){free(p);p =  NULL;}

/**!
 * 函数名：	ftpGetFile
 * 功     能 ：	通过FTP获取单个文件
 * 编写者：	FuKai
 * 简     介：	通过ftp将远程文件下载到本地路径
 * 输入参数：
 * 		pFtpName          	FTP服务器ip或网络名                                                
 * 		pUser           	用户名                                                                    
 * 		pPasswd         	密码
 * 		pRemoteFile     	远程文件名（可带路径 ）
 * 		pLocalDir			本地目录路径
 * 		bMkDirIfNotExists	如果目录不存在是否创建
 *  
 * 返回值:	
 * 		执行成功则返回获取的字节数，否则返回 ERROR(-1);
 * 备注：
 */	
int FTP_FUNC_DEF(ftpGetFile, char *pRemoteFile, char * pLocalDir, int bMkDirIfNotExists)
{
	UINT32 *pBuffer = NULL;
	char * pRemoteDir = NULL;
	char * pFileName = NULL;
	UINT32 nBytes = 0, nBytes_tmpRead = 0, nBytes_tmpWrite = 0;
	STATUS retVal = OK;
	int ctrlSock = 0;
	int dataSock = 0;
	char * pLocalModPath = NULL;
	int pathLen = 0;

	FILE * outfile = 0;
	
	if (!CHECK_NOEMPTY_STR(pRemoteFile)) return ERROR;
	if (!CHECK_NOEMPTY_STR(pFtpName)) return ERROR;
	
	if (FALSE == checkDirExists(pLocalDir, TRUE, TRUE)) return ERROR;
	
	splitFilePath(pRemoteFile, &pRemoteDir, &pFileName, 0);
	if ((!pRemoteDir) || (!pFileName))
	{
		GOTO_END_OF_FUNC((ERROR));
	}
	
	if (FTP_FUNC_CALL(ftpXfer,"","RETR %s", pRemoteDir, pFileName, &ctrlSock, &dataSock) == ERROR)
	{
		fprintf(stderr, "Connect to FTP Server[%s] via User[%s] failed !\n", pFtpName, pUser);	
		GOTO_END_OF_FUNC((ERROR));
	}
	
#ifdef _DEBUG
	printf("Server[%s] Connected via User[%s]\n", pFtpName, pUser);	
#endif
	
	pathLen = strlen(pFileName);
	if(pLocalDir)
	{
		if (strlen(pLocalDir))
		{
			pathLen += strlen(pLocalDir);						
		}
	}
	pLocalModPath = (char*)malloc(pathLen + 1);	
	if (!pLocalModPath)
	{
		fprintf(stderr, "Malloc pLocalModPath failed!!! bufSize=%d\n", pathLen+1);
		GOTO_END_OF_FUNC((ERROR));
	}
	
	sprintf(pLocalModPath, "%s%s%s", pLocalDir,
			(pLocalDir[strlen(pLocalDir)-1] == '/' ? "":"/"), pFileName);
	
	outfile = fopen(pLocalModPath, "w+b");
	if (!outfile)
	{
		printf("Openfile failed!!! file path:%s\n", pLocalModPath);
		GOTO_END_OF_FUNC((ERROR));
	}
	
	pBuffer = (UINT32 *)malloc(_BUFSIZE+1);
	if(pBuffer == NULL)
	{
		printf("Malloc pBuffer fail!!! bufSize=%d\n", _BUFSIZE+1);
		GOTO_END_OF_FUNC((ERROR));
	}
	memset(pBuffer, 0, _BUFSIZE+1);	
	
#ifdef _DEBUG
	printf("Begin retriving file[%s]\n", pRemoteFile);
#endif
	
	while (TRUE)
	{
		nBytes_tmpRead =read(dataSock, (char *)pBuffer, _BUFSIZE);
		if (nBytes_tmpRead==0) break;
		if (nBytes_tmpRead > _BUFSIZE) break;
		nBytes += nBytes_tmpRead;			
		nBytes_tmpWrite = fwrite(pBuffer, sizeof(char), nBytes_tmpRead, outfile);
		if (nBytes_tmpWrite != nBytes_tmpRead)
		{
			fprintf(stderr, "Write file [ %s ] failed ! \n", pLocalModPath);
			GOTO_END_OF_FUNC((ERROR));			
		}
	}
	
#ifdef _DEBUG
	printf ("Retriving num of Bytes: %d\n", nBytes);
#endif	
	
	if (ftpReplyGet (ctrlSock, TRUE) != FTP_COMPLETE)
	{
		GOTO_END_OF_FUNC((ERROR));
	}

	if (ftpCommand (ctrlSock, "QUIT", 0, 0, 0, 0, 0, 0) != FTP_COMPLETE)
	{
		GOTO_END_OF_FUNC((ERROR));
	}
	
#ifdef _DEBUG
	printf("Retriving complete\n");
#endif
	
	retVal = nBytes;
	
end_of_func:
    FREE(pRemoteDir);
    FREE(pFileName);
    FREE(pLocalModPath);
    if (outfile) fclose(outfile);
    if (dataSock) close(dataSock);
    if (ctrlSock) close(ctrlSock);
    FREE(pBuffer);	
	return retVal;	
}

int SIMPLE_FTP_FUNC_DEF(ftpGetFile, char *pRemoteFile, char * pLocalDir, int bMkDirIfNotExists)
{
	return SIMPLE_FTP_FUNC_CALL(ftpGetFile, pRemoteFile, pLocalDir, bMkDirIfNotExists);
}

/**!  
 * 函数名：	ftpPutFile
 * 功     能 ：	通过FTP上传单个文件
 * 编写者：	FuKai
 * 简     介：	通过ftp将要加载的模块传输到本地路径，再调用ld加载
 * 输入参数：
 * 		pFtpName          	FTP服务器ip或网络名                                                
 * 		pUser           	用户名                                                                    
 * 		pPasswd         	密码 * 		
 * 		pLocalFile			本地文件名（可带路径）
 * 		pRemoteFile     	远程目录名（可带路径 ）
 *  
 * 返回值:	
 * 		执行成功则返回获取的字节数，否则返回 ERROR(-1);
 * 备注：
 */	

STATUS FTP_FUNC_DEF(ftpPutFile, char *pLocalFile, char * pRemoteDir)
{
	UINT32 *pBuffer = NULL;
	char * pLocalDir = NULL;
	char * pFileName = NULL;
	UINT32 nBytes = 0, nBytes_tmpRead = 0, nBytes_tmpWrite = 0;
	STATUS retVal = OK;
	int ctrlSock = 0;
	int dataSock = 0;
	FILE * outfile = 0;
	
	if (!CHECK_NOEMPTY_STR(pFtpName)) return ERROR;
	if (!CHECK_NOEMPTY_STR(pLocalFile)) return ERROR;	
	
	if(!checkFileExists(pLocalFile)) 
		{
		fprintf(stderr, "[ERROR] file [ %s ] not exists !\n", pLocalFile);
		return ERROR;
		}

	
	splitFilePath(pLocalFile, &pLocalDir, &pFileName, 0);
	if ((!pLocalDir) || (!pFileName))
	{
		GOTO_END_OF_FUNC((ERROR));
	}
	
	if (FTP_FUNC_CALL(ftpXfer,"","STOR %s", 
			(CHECK_NOEMPTY_STR(pRemoteDir)? pRemoteDir : "/"), pFileName,
			&ctrlSock, &dataSock) == ERROR)
	{
		fprintf(stderr, "Connect to FTP Server[%s] Connected via User[%s] failed !\n", pFtpName, pUser);	
		GOTO_END_OF_FUNC((ERROR));
	}
	
#ifdef _DEBUG
	printf("Server[%s] Connected via User[%s]\n", pFtpName, pUser);	
#endif
	
	outfile = fopen(pLocalFile, "r+b");//modify by ww 20190618 from r+ to r+b
	if (!outfile)
	{
		printf("Openfile failed!!! file path:%s\n", pLocalFile);
		GOTO_END_OF_FUNC((ERROR));
	}
	
	pBuffer = (UINT32 *)malloc(_BUFSIZE+1);
	if(pBuffer == NULL)
	{
		printf("Malloc pBuffer fail!!! bufSize=%d\n", _BUFSIZE+1);
		GOTO_END_OF_FUNC((ERROR));
	}
	
	
#ifdef _DEBUG
	printf("Begin Sending file[%s]\n", pLocalFile);
#endif
	
	while (TRUE)
	{
		memset(pBuffer, 0, _BUFSIZE+1);	
		nBytes_tmpRead = fread(pBuffer, sizeof(char), _BUFSIZE, outfile);	
		if (nBytes_tmpRead==0) break;
		if (nBytes_tmpRead > _BUFSIZE) break;
		nBytes += nBytes_tmpRead;	
		
		nBytes_tmpWrite =write(dataSock, (char *)pBuffer, nBytes_tmpRead);
		if (nBytes_tmpWrite != nBytes_tmpRead)
		{
			fprintf(stderr, "Send file [ %s ] failed ! \n", pLocalFile);
			GOTO_END_OF_FUNC((ERROR));			
		}
		if (nBytes_tmpRead < _BUFSIZE) break;
	}
	
#ifdef _DEBUG
	printf ("Retriving num of Bytes: %d\n", nBytes);
#endif	
	
	
	/*modify by ymk 20190626 add*/
	if(dataSock)
	{
		close(dataSock);
		dataSock = 0;
	}
	/*modify by ymk 20190626 add*/
	
	if (ftpReplyGet (ctrlSock, TRUE) != FTP_COMPLETE)
	{
		GOTO_END_OF_FUNC((ERROR));
	}

	if (ftpCommand (ctrlSock, "QUIT", 0, 0, 0, 0, 0, 0) != FTP_COMPLETE)
	{
		GOTO_END_OF_FUNC((ERROR));
	}
	
#ifdef _DEBUG
	printf("Retriving complete\n");
#endif
	
	retVal = nBytes;
	
end_of_func:
    FREE(pLocalDir);
    FREE(pFileName);
    if (outfile) fclose(outfile);
    if (dataSock) close(dataSock);
    if (ctrlSock) close(ctrlSock);
    FREE(pBuffer);	
	return retVal;	
}

STATUS SIMPLE_FTP_FUNC_DEF(ftpPutFile, char *pLocalFile, char * pRemoteDir)
{
	return SIMPLE_FTP_FUNC_CALL(ftpPutFile, pLocalFile, pRemoteDir);
}

/**!
 * 函数名：	ftpld
 * 功     能：	判断数据正确与否函数
 * 编写者：	FuKai
 * 简     介：	通过ftp将要加载的模块传输到本地路径，再调用ld加载
 * 输入参数：
 * 		pFtpName          	FTP服务器ip或网络名                                                
 * 		pUser           	用户名                                                                    
 * 		pPasswd         	密码
 * 		pRemoteFile     	远程文件名（可带路径 ）
 * 		bUpdateIfExists		如果存在是否更新
 *  
 * 返回值:	
 * 		正确时返回 加载的地址，错误返回0;
 * 备注：
 */
extern void * moduleFindByFilename(char *);
extern int unld(char *);
extern void *ld(char *file_name, char *module_name);

void * FTP_FUNC_DEF(ftpld, char *pRemoteFile, BOOL bUpdateIfExists)
{
	
#if 0
	void * retVal = 0;
	char * pFileName = NULL;	
	char * modPathName;
	
	if (!CHECK_NOEMPTY_STR(pRemoteFile)) 
	{
		fprintf(stderr, "[ERROR]file name [ %s ] should not empty!!!\n", pRemoteFile);
		return 0;
	}
	
	splitFilePath(pRemoteFile, 0, &pFileName, 0);	
	if(!pFileName)
	{
		fprintf(stderr, "[ERROR]file name [ %s ] is INVALID!!!\n", pRemoteFile);
		return 0;
	}
	
	/*检查文件名*/
	if (!checkModuleFileName(pFileName))
	{
		fprintf(stderr, "[ERROR]module file name [ %s ] is INVALID!!!\n", 
				(pFileName?pFileName:""));
		GOTO_END_OF_FUNC(0);
	}
	
	modPathName = malloc(strlen(pFileName) + strlen(_TMPDIR));
	memset(modPathName, 0, strlen(pFileName) + strlen(_TMPDIR));
	sprintf(modPathName, "%s/%s", _TMPDIR, pFileName);	
	
	if (moduleFindByFilename(modPathName))
	{
		if (bUpdateIfExists)
		{
			if (-1 == unld(modPathName))
			{
				fprintf(stderr, "[ERROR]module [ %s ] exists, but cannot unld it ! \n", modPathName);
				GOTO_END_OF_FUNC(0);
			}
		}
		else
		{
			fprintf(stderr, "[ERROR]module [ %s ] already exists ! \n", modPathName);
			GOTO_END_OF_FUNC(0);
		}
	}
	
	if (checkFileExists(modPathName))
	{
		if (-1 == rm(modPathName)) 
		{
			fprintf(stderr, "[ERROR]cannot delete last tmp file [ %s ] ! \n", modPathName);
			GOTO_END_OF_FUNC(0);
		}
	}
	
	if (ftpGetFile(pFtpName, pUser, pPasswd, pRemoteFile, _TMPDIR, 1) <= 0)
	{
		fprintf(stderr, "[ERROR]retrieve remote file [ %s ] failed ! \n", pRemoteFile);
		GOTO_END_OF_FUNC(0);
	}
    retVal = ld(modPathName,pFileName);
    
	rm(modPathName);//加载完后删除临时文件,失败则忽略。
	rm(_TMPDIR);

end_of_func:
    FREE(pFileName);
    FREE(modPathName);
	return retVal;
#endif
return 0;
}


void * SIMPLE_FTP_FUNC_DEF(ftpld, char *pRemoteFile, BOOL bUpdateIfExists)
{
	return SIMPLE_FTP_FUNC_CALL(ftpld, pRemoteFile, bUpdateIfExists);
}

void ftpldTest()
{
//	ftpld("168.168.168.210","cc","cc", "outhrasm.out", TRUE);
	ftpld("192.168.100.200","cc","cc", "outhrasm.out", TRUE);
}

void ftpRWTest()
{
//	ftpGetFile("10.0.6.210","cc","cc", "outhrasm.out", "/ram0", TRUE);
	ftpPutFile("192.168.100.200","hr","hr", "/clb/outhrasm.out", "/ddd1");
}

BOOL checkModuleFileName(char * pOutfile)
{
	/*检查文件名*/
	char * pos;
	if (!CHECK_NOEMPTY_STR(pOutfile)) return FALSE;	
	if (strlen(pOutfile)<= 4) return FALSE;
	pos = strrchr(pOutfile, '.');
	if (!pos) return FALSE;
	if (strncasecmp(pos, ".out",4)) return FALSE;
	return TRUE;
}

BOOL checkDirExists(char * dirname, BOOL bMkDirIfNotExists, BOOL bLogErr)
{
	struct stat st;
	int status = stat(dirname, &st);
	if (status != 0)
	{
		if (bMkDirIfNotExists)
		{
			if (mkdir_path(dirname, 0777)) {
				if (bLogErr) fprintf(stderr, "[ERROR] create dir [ %s ] \n", dirname);
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
		else
		{
			if (bLogErr) fprintf(stderr, "[ERROR] in stat(path[ %s ]) \n", dirname);
			return FALSE;
		}		
	}	
	if (!S_ISDIR(st.st_mode))
	{
		if (bLogErr) fprintf(stderr,"[ %s ] is not directory\n", dirname);
		return FALSE;
	}
	return TRUE;
}

BOOL checkFileExists(char * filename)
{
	struct stat st;
	int filestat;
	filestat = stat(filename, &st);
	if (filestat != 0)
	{
		return FALSE;		
	}
	
	if (!S_ISREG(st.st_mode))
	{
		return FALSE;
	}
	return TRUE;
}

void splitFilePath(char * filepath, char ** dir, char ** fname, char ** ext)
{
	char * pos = 0;
	
	if (ext) {
		pos = strrchr(filepath, '.');
		if (pos) {
			*ext = strdup(pos);
		}
	}	
	if (fname || dir)
	{
		pos = strrchr(filepath, '\\');
		if (pos == 0)
			pos = strrchr(filepath, '/');
		if (pos) {
			if (fname) {
				*fname = strdup(pos + 1);
			}
			if (dir) {
				*dir = strdup(filepath);
				*((*dir) + (pos - filepath)) = '\0';
			}
		}
		else
		{
			if (fname) {
				*fname = strdup(filepath);
			}
			if (dir) {
				*dir = strdup(".");
			}			
		}
	}
	
#ifdef _DEBUG
	{
		printf("filepath: %s ==> ", filepath);
		if (dir) printf("dir: %s \t", *dir);
		if (fname) printf("fname: %s \t", *fname);
		if (ext) printf("ext: %s", *ext);
		printf("\n");
	}
#endif
}

void makeFilePath(char * filepath, char * dir, char * fname, char * ext)
{
	int remain = _MAX_PATH;
	
	if (dir)
	{
		strncpy(filepath, dir, remain);
		PATH2DIR(filepath);
		remain -= strlen(filepath);
	}
	
	if (fname)
	{		
		DIR2PATH(filepath);	
		strncat(filepath, fname, --remain);
		remain -= strlen(fname);
	}
	
	if (ext)
	{
		if (*ext != '.') strncat(filepath, ".", remain--);
		strncat(filepath, ext, remain);
	}
	
#ifdef _DEBUG
	{		
		if (dir) printf("dir: %s ", dir);
		if (fname) printf("fname: %s ", fname);
		if (ext) printf("ext: %s ", ext);
		printf(" ==> filepath: %s ", filepath);
		printf("\n");
	}
#endif
}

STATUS replaceBasePath(char * newfilepath, char * newBase, char * oldfilepath, char * oldBase)
{
	if (!newfilepath) return ERROR;
	if (!CHECK_NOEMPTY_STR(newBase)) return ERROR;
	if (!CHECK_NOEMPTY_STR(oldfilepath)) return ERROR;
	int remain = _MAX_PATH;
	strncpy(newfilepath, newBase, remain);
	remain -= strlen(newBase);
	if (!CHECK_NOEMPTY_STR(oldBase))
		strncat(newfilepath, oldfilepath, remain);
	else
	{
		char * p = strstr(oldfilepath, oldBase);
		if (!p)	strncat(newfilepath, oldfilepath, remain);
		else strncat(newfilepath, p + strlen(oldBase), remain);		
	}
	return TRUE;
}

BOOL FTP_FUNC_DEF(ftpIsDir, char * pRemoteDir)
{
    int     dataSock = 0;
    int     cntrlSock = 0;
    int     retVal = TRUE;
/*    char    buffer [BUFSIZ];
    int     nChars;*/
    
	if (!CHECK_NOEMPTY_STR(pRemoteDir)) return (FALSE);	
    if (FTP_FUNC_CALL(ftpXfer, "", "PWD", pRemoteDir, "", &cntrlSock, &dataSock) != OK) {
		return (FALSE);
	}   
    
/*    while ((nChars = read (dataSock, (char *) buffer, BUFSIZ)) > 0)
    {
        if (bLsIfDir) write (STD_OUT, (char *) buffer, nChars);
    }*/
    
/*    if (ftpReplyGet (cntrlSock, TRUE) != FTP_COMPLETE) 
        {
    	GOTO_END_OF_FUNC(TRUE);
        }*/

    if (ftpCommand (cntrlSock, "QUIT",0, 0, 0, 0, 0, 0) != FTP_COMPLETE) 
        {
    	GOTO_END_OF_FUNC(TRUE);
        }
end_of_func:
    if (cntrlSock) close (cntrlSock);
    if (dataSock) close (dataSock);
    return (retVal);
}

int FTP_FUNC_DEF(ftpExecCmd, char * pCmdFormat, char * pCurrentDir, char * pRemoteDir, char * pLocalfile)
{
    int     dataSock = 0, cntrlSock = 0;
    char *  pBuffer = NULL;
	UINT32  nBytes = 0, nBytes_tmpRead = 0, nBytes_tmpWrite = 0;
	STATUS  retVal = OK;
	FILE *  tmpfile = NULL;
	printf("SERVER=\"%s\", USER=\"%s\", PASSWD=\"%s\"\n",NULL2EMPTY(pFtpName), NULL2EMPTY(pUser), NULL2EMPTY(pPasswd));
	printf("COMMAND=\"%s\", CWD=\"%s\", RMD=\"%s\", LOG=\"%s\"\n",NULL2EMPTY(pCmdFormat), NULL2EMPTY(pCurrentDir), NULL2EMPTY(pRemoteDir), NULL2EMPTY(pLocalfile));

	if (!CHECK_NOEMPTY_STR(pCmdFormat)) return (FALSE);	
 	
	if (FTP_FUNC_CALL(ftpXfer, "", pCmdFormat, NULL2EMPTY(pCurrentDir),NULL2EMPTY(pRemoteDir), &cntrlSock, &dataSock) != OK) {
		fprintf(stderr, "[ERROR]Exec command Failed \n ");
		return (ERROR);
	}  
    
	if (CHECK_NOEMPTY_STR(pLocalfile))
	{
		tmpfile = fopen(pLocalfile, "w+");
		if (!tmpfile) {
			fprintf(stderr, "[ERROR]Open log file failed!!!\n");
			GOTO_END_OF_FUNC((ERROR));
		}
	}
	else
		tmpfile = stdout;
	
	printf("\nSHOW RESULT --> %s\n", (tmpfile == stdout ? "<STDOUT>":pLocalfile));
	printf("=====================================================\n");
	
	if (!dataSock || !cntrlSock)
	{
		GOTO_END_OF_FUNC((TRUE));
	}
		
	
	pBuffer = (char *)malloc(_BUFSIZE+1);
	if(!pBuffer)
	{
		fprintf(stderr, "[ERROR]Malloc pBuffer fail!!! bufSize=%d\n", _BUFSIZE+1);
		GOTO_END_OF_FUNC((ERROR));
	}
	memset(pBuffer, 0, _BUFSIZE+1);	
	
	while (TRUE)
	{
		nBytes_tmpRead =read(dataSock, (char *)pBuffer, _BUFSIZE);
		if (nBytes_tmpRead==0) break;
		if (nBytes_tmpRead > _BUFSIZE) break;
		nBytes += nBytes_tmpRead;			
		nBytes_tmpWrite = fwrite(pBuffer, sizeof(char), nBytes_tmpRead, tmpfile);
		if (nBytes_tmpWrite != nBytes_tmpRead)
		{
			fprintf(stderr, "[ERROR]Write log file failed ! \n");
			GOTO_END_OF_FUNC((ERROR));		
		}
	}
	
    /* Close the sockets opened by ftpXfer */
    if (ftpReplyGet (cntrlSock, TRUE) != FTP_COMPLETE) 
    {
    	GOTO_END_OF_FUNC((ERROR));	
    }

    if (ftpCommand (cntrlSock, "QUIT",0, 0, 0, 0, 0, 0) != FTP_COMPLETE) 
    {
    	GOTO_END_OF_FUNC((ERROR));	
    }
end_of_func:
    if (cntrlSock) close (cntrlSock);
    if (dataSock)  close (dataSock);
    if (tmpfile && (tmpfile != stdout)) fclose(tmpfile);
    FREE(pBuffer);
    return (retVal);
}

typedef struct _FTP_RMT_FILE FtpRmtFile;
struct _FTP_RMT_FILE
{
	char csName[_MAX_PATH+1];
/*	char csRemoteDir[_MAX_PATH+1];*/
	BOOL bIsDir;
	BOOL bIsHead;
	FtpRmtFile * prev;
	FtpRmtFile * next;
};

static FtpRmtFile * ftpRmtFileAlloc(char * pFileName, char * pRemoteDir)
{
	FtpRmtFile * obj = (FtpRmtFile*) malloc(sizeof(FtpRmtFile));
	obj->bIsDir = FALSE;
	obj->bIsHead = FALSE;	
	makeFilePath(obj->csName, pRemoteDir, pFileName, 0);
	obj->prev = NULL;
	obj->next = NULL;
	return obj;
}

static int ftpRmtFileFree(FtpRmtFile ** _obj, BOOL bRecursive)
{
	if (!_obj) return 0;
	if (!(*_obj))return 0;
	int numOfFree = 0;
	FtpRmtFile * obj = *_obj;
	if (bRecursive && obj->next)
	{
		if (!obj->next->bIsHead)
		{
			numOfFree += ftpRmtFileFree(&(obj->next), bRecursive);
			obj->next = NULL;
		}
	}	
	FREE(obj);
	numOfFree++;
	*_obj = NULL;
	return numOfFree;
}

#define TMPFILE_FTP_LIST_DIR "ftplsdir.tmp"
int FTP_FUNC_DEF(ftpListDir, char * pRemoteDir, 
		FtpRmtFile ** pRmtFileList, UINT * pNumOfFile, BOOL bRecursive)
{
    int     dataSock = 0, cntrlSock = 0;
    char *  pBuffer = NULL;
	UINT32  nBytes = 0, nBytes_tmpRead = 0, nBytes_tmpWrite = 0;
	STATUS  retVal = OK;
	FILE * tfile = NULL;	
	char csFile[_MAX_PATH+1] = {0};
	if (!CHECK_NOEMPTY_STR(pFtpName)) return ERROR;

    if (FTP_FUNC_CALL(ftpXfer, "", "NLST %s", "", NULL2EMPTY(pRemoteDir), &cntrlSock, &dataSock) != OK) {
		fprintf(stderr, "[ERROR]Can't open directory \"%s\"", pRemoteDir);
		return (ERROR);
	}   
    
	pBuffer = (char *)malloc(_BUFSIZE+1);
	if(!pBuffer)
	{
		fprintf(stderr, "[ERROR]Malloc pBuffer fail!!! bufSize=%d\n", _BUFSIZE+1);
		GOTO_END_OF_FUNC((ERROR));
	}
	memset(pBuffer, 0, _BUFSIZE+1);		
	tfile = fopen(TMPFILE_FTP_LIST_DIR, "w+");	
	while (TRUE)
	{
		nBytes_tmpRead =read(dataSock, (char *)pBuffer, _BUFSIZE);
		if (nBytes_tmpRead==0) break;
		if (nBytes_tmpRead > _BUFSIZE) break;
		nBytes += nBytes_tmpRead;			
		nBytes_tmpWrite = fwrite(pBuffer, sizeof(char), nBytes_tmpRead, tfile);
		if (nBytes_tmpWrite != nBytes_tmpRead)
		{
			fprintf(stderr, "[ERROR]Write tmp file failed ! \n");
			GOTO_END_OF_FUNC((ERROR));		
		}
	}	
    /* Close the sockets opened by ftpXfer */
    if (ftpReplyGet (cntrlSock, TRUE) != FTP_COMPLETE) 
    {
    	GOTO_END_OF_FUNC((ERROR));	
    }
    if (ftpCommand (cntrlSock, "QUIT",0, 0, 0, 0, 0, 0) != FTP_COMPLETE) 
    {
    	GOTO_END_OF_FUNC((ERROR));	
    }
    close (cntrlSock);
    close (dataSock);
    cntrlSock = 0;
    dataSock = 0;
    FREE(pBuffer);
	
	if (!pRmtFileList) 
	{
		GOTO_END_OF_FUNC((ERROR));	
	}
	
	fseek(tfile, 0, SEEK_SET);
	
	FtpRmtFile * pHead = NULL, *pList = NULL, *pNext = NULL; 
	int numOfFile = 0;
	
	while(fgets(csFile, _MAX_PATH, tfile))
	{
		TRIM_TAIL(csFile);
		pNext = ftpRmtFileAlloc(csFile, pRemoteDir);
		if (!pNext)
		{
			fprintf(stderr, "[ERROR]malloc failed ! \n");
			GOTO_END_OF_FUNC((ERROR));
		}
		if (!pList)	 //第一个
		{
			pList = pNext;			
			pHead = pNext;
			pHead->bIsHead = TRUE;
			pNext->next = pHead;
			pNext->prev = pHead;
		}	
		else
		{			
			pNext->prev = pList;
			pNext->next = pHead;
			pHead->prev = pNext;
			pList->next = pNext;
			pList = pNext;	
			pList->bIsHead = FALSE;
		}
		pList->bIsDir = FTP_FUNC_CALL(ftpIsDir, pList->csName);
		numOfFile++;		
	}
	pNext = NULL;
	fclose(tfile);
	tfile = 0;
	*pNumOfFile += numOfFile;	
	if (bRecursive && pHead)
	{
		FtpRmtFile * pTmpList; 
		pNext = pList = pHead;
		while (pNext)
		{
			pList = pNext;
			if (pNext->next->bIsHead) pNext = NULL;
			else pNext = pList->next;			
			if (pList->bIsDir)
			{
				pTmpList = NULL;
				FTP_FUNC_CALL(ftpListDir, pList->csName, &(pTmpList), pNumOfFile, bRecursive);
				if (pTmpList)
				{
					pTmpList->bIsHead = FALSE;
					pTmpList->prev->next = pList->next;
					pList->next->prev = pTmpList->prev;
					pList->next = pTmpList;
					pTmpList->prev = pList;
				}
			}	
		}
		pNext = NULL;
	}
	*pRmtFileList = pHead;
	pHead = NULL;
end_of_func:    
    if (cntrlSock) close (cntrlSock);
    if (dataSock)  close (dataSock);
    if (tfile) fclose(tfile);
    FREE(pBuffer);
    if (retVal == ERROR && pHead) 
    	{
    	printf("Free %d files totally \n",ftpRmtFileFree(&pHead, TRUE));
    	}
    return (retVal);
}

int SIMPLE_FTP_FUNC_DEF(ftpListDir, char * pRemoteDir, 
		FtpRmtFile ** pRmtFileList, UINT * pNumOfFile, BOOL bRecursive)
{
	return SIMPLE_FTP_FUNC_CALL(ftpListDir, pRemoteDir,	
			pRmtFileList, pNumOfFile, bRecursive);
}

int FTP_FUNC_DEF(ftpGetDir, char *pRemoteDir, char * pLocalDir, BOOL bStopIfError)
{
	STATUS retVal = OK;
	
	if (!CHECK_NOEMPTY_STR(pRemoteDir)) return ERROR;
	if (!CHECK_NOEMPTY_STR(pFtpName)) return ERROR;
	
	if (FALSE == checkDirExists(pLocalDir, TRUE, TRUE)) return ERROR;
	
	if (FALSE == FTP_FUNC_CALL(ftpIsDir, pRemoteDir))
	{
		fprintf(stderr, "[ERROR] %s is not directory !\n", pRemoteDir);
		return ERROR;
	}
	
	FtpRmtFile * pRmtFileList  = NULL;
	UINT numOfRmtFileOrDir = 0;
	
	if (ERROR == FTP_FUNC_CALL(ftpListDir, pRemoteDir, &pRmtFileList, &numOfRmtFileOrDir, TRUE))
	{
		fprintf(stderr, "[ERROR] Fail to Get Remote file list of %s !\n", pRemoteDir);
		GOTO_END_OF_FUNC(ERROR);
	}
	
	printf("Find %d files under remote dir %s \n", numOfRmtFileOrDir, pRemoteDir);
	
	int numOfFile = 0, numOfDir = 0, numOfBytes = 0;
	
	if (pRmtFileList && numOfRmtFileOrDir)
	{
		FtpRmtFile * pTmpFile = pRmtFileList;
		char csLocalFile[_MAX_PATH] = {0};
		char *pTmpDir = NULL;
		while(pTmpFile)
		{
			replaceBasePath(csLocalFile, pLocalDir, pTmpFile->csName, pRemoteDir);
			if (pTmpFile->bIsDir)
			{				
				if (mkdir_path(csLocalFile, 0777))
				{
					fprintf(stderr, "[ERROR] create dir [ %s ] failed \n", csLocalFile);
					if (bStopIfError) GOTO_END_OF_FUNC(ERROR);
				}
				else
				{
					numOfDir++;
					numOfRmtFileOrDir--;
				}
			}
			else
			{
				splitFilePath(csLocalFile, &pTmpDir, 0, 0);
				if (pTmpDir)
				{
					retVal = FTP_FUNC_CALL(ftpGetFile, pTmpFile->csName, pTmpDir, TRUE);
					if (ERROR == retVal) {
						fprintf(stderr, "[ERROR] get file [ %s ] failed \n",
								pTmpFile->csName);
						if (bStopIfError)
						{
							FREE(pTmpDir);
							GOTO_END_OF_FUNC(ERROR);
						}
					} else {
						numOfBytes += retVal;
						numOfFile++;
						numOfRmtFileOrDir--;
					}
					FREE(pTmpDir);
				}
			}
			
			if (!pTmpFile->next) break;
			if (pTmpFile->next->bIsHead) break;
			pTmpFile = pTmpFile->next;
		}
		retVal = numOfBytes;
#ifdef _DEBUG
		printf("Transfer %d dirs, %d files, total %d bytes; %d fileOrDir Failed\n", numOfDir, numOfFile, numOfBytes, numOfRmtFileOrDir);
#endif
	}
	
	
end_of_func:
	if (pRmtFileList) 
	{
		printf("Free %d files totally \n", ftpRmtFileFree(&pRmtFileList, TRUE));
	}
	return retVal;	
}

int SIMPLE_FTP_FUNC_DEF(ftpGetDir, char *pRemoteDir, 
		char * pLocalDir, BOOL bStopIfError)
{
	return SIMPLE_FTP_FUNC_CALL(ftpGetDir, pRemoteDir, 
			pLocalDir, bStopIfError);
}


void netUtils_module_init()
{
	return;
}
