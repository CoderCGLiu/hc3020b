/*!************************************************************
 * @file: hr2APIs.c
 * @brief: 根据驱动封装的上层接口
 * @author: cetc
 * @date: 06,01,2020
 * @version: 1.0.0
 * @note:
 * ------------------------------------------------------------
 * Change History:
 * <Date> | <Author> | <Description>
 * 2020/01/06 | xupeng | 代码整理与规范
 * 2020/01/30 | xupeng | 合并了PCIE版本的时间戳接口
 * 
 **************************************************************/
/*
 * 头文件
 * */
#include <vxworks.h>
#include <cpu.h>
#ifndef ETHER_ADDR_LEN 
#define	ETHER_ADDR_LEN		6
#endif
#include "stdio.h" 
#include "string.h"
#include "ioLib.h"
#include "dosFsLib.h"
#include <ftpLib.h>
#include "stdlib.h"
#include "ipProto.h"
//#include "net/utils/ifconfig.h"
#include "usrFsLib.h"
#include "bootLib.h"
#include "sysLib.h"
#include <taskLib.h> 
#include "huarui3/h/tool/hr3SysTools.h"
#include "huarui3/h/tool/hr3APIs.h"
#include "huarui3/drv/generaldma/hr3GeneralDma.h"
/*
 * 宏定义
 * */
#ifdef __HUARUI3__

#define INCLUDE_USERMEM
#define INCLUDE_TFFS
//#define HGMAC_DEV0
//#define DRV_CDMA
#define DRV_GDMA
#define DRV_TIMER0
#define DRV_TIMER1
#define DRV_LPC
#define DRV_GPIO
#define DRV_RIOC0
#define DRV_RIOC1
#define DRV_NAND
#define DRV_I2C

#endif
/**
 * 外部函数声明
 */      
extern unsigned int sysGetSlotNum();
extern unsigned int sysGetCaseNum();
extern unsigned long long hrKmToPhys(void *addr_mapped);
extern unsigned int bslProcGetId(void);
extern void sysReset(unsigned int resetType);
extern void sysLEDSetState(unsigned int state);
extern unsigned int sysLEDGetState();
extern unsigned char readLPCFlashByte(unsigned int addr);
extern unsigned int sysCpuGetID();
extern unsigned int vxCpuIdGet (void);
/**
 * 外部变量声明 
 */      
extern int g_gdmaDbgBscSign; /* 记录基本调试信息的标识，0表示不记录，1表示记录*/
extern int g_gdmaDbgDtlSign; /* 记录详细调试信息的标识，0表示不记录，1表示记录*/
extern HRGDMADBGINFO * g_pGdmaDbgInfo;

/*
 * 功能：模块板级复位或整个插箱复位。
 * 输入：
 * 		resetType，复位类型，0为板级复位，1为整个插箱复位。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	函数执行成功，返回0，失败返回-1。
 */
int bslBoardReset(unsigned int resetType)
{
	if((resetType > 1) || (resetType < 0))
	{
		printf("[DSP%d]复位函数resetType取值只能为0、1\n",sysCpuGetID());
		return ERROR;
	}
	sysReset(resetType);
	return OK;
}

/*
 * 功能：获取模块的机柜号、插箱号、槽位号。
 * 输入：
 * 		无。
 * 输出：
 * 		cabinetNum，机柜号；
 * 		caseNum，	插箱号；
 * 		slotNum		槽位号。
 * 返回值：
 * 	 	函数执行成功，返回0,失败返回-1。
 */
int bslModuleGetInfo(unsigned int *cabinetNum, unsigned int *caseNum,unsigned int * slotNum)
{		
#ifndef __PCIE__	
	if((cabinetNum == NULL) || (caseNum == NULL) || (slotNum == NULL))
	{
		printf("[DSP%d]获取模块的机柜号、插箱号、槽位号函数参数不能为NULL！\n",sysCpuGetID());
		return ERROR;
	}
	
	*slotNum = sysGetSlotNum();
	*caseNum = sysGetCaseNum();
	*cabinetNum = 0; /* 机柜号暂未提供 */	
#else
	unsigned short int tmp = 0;
	
	if((cabinetNum == NULL) || (caseNum == NULL) || (slotNum == NULL))
	{
		printf("[DSP%d]获取模块的机柜号、插箱号、槽位号函数参数不能为NULL！\n",sysCpuGetID());
		return ERROR;
	}
	tmp = *(unsigned short int *)MODULE_ID_REG;
	
	*slotNum = tmp & 0x00FF;
	*caseNum = (tmp >> 8) & 0x00FF;
	*cabinetNum = 0; /* 机柜号暂未提供 */
	
#endif
	
	return OK;
}
/*
 * 功能：获取模块的基准时钟(单位为1us)。
 * 输入：
 * 		无。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	模块当前的基准时钟。
 */

extern unsigned long long usr_get_time(struct timespec *ts);
unsigned long long bslGetTimeUsec(void)
{
#if 0
	unsigned long long usec;
	struct timespec ts;
	usec = usr_get_time(&ts);
	return usec;
#else
	unsigned long long usec;
	usec = sys_timestamp();
	usec = usec * 1000000 / sys_timestamp_freq(); /* 微秒以下零头被舍去 */
	return usec;
#endif
}


/*
 * 功能：获取CPU CORE电压和ZYNQ的温度。
 * 输入：
 * 		无。
 * 输出：
 * 		coreVcc，	CPU CORE电压；
 * 		zynqTemp，	ZYNQ的温度。
 * 返回值：
 * 	 	函数执行成功，返回0。
 */
int bslFPGAGetSense(float * coreVcc, float * zynqTemp)
{
	if((coreVcc == NULL) || (zynqTemp == NULL))
	{
		printf("[DSP%d]获取CPU CORE电压和ZYNQ的温度函数参数不能为NULL！\n",sysCpuGetID());
		return ERROR;		
	}
		
	* coreVcc = *(float *)CORE_VCC_REG;
	* zynqTemp = *(float *)ZYNQ_TEMP_REG;
	
	return OK;
}
/*
 * 功能：板上4片CPU的串口切换。
 * 输入：
 * 		cpuIndex，CPU的序号，取值0、1、2、3。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	函数执行成功，返回0；函数执行失败，返回-1。
 */
extern int sysUartSwitch(unsigned int cpuIndex);
int bslUartSwitch(unsigned int cpuIndex)
{
	return sysUartSwitch(cpuIndex);
}



/*
 * 功能：读取温度传感器模块测量的华睿2号芯片温度值。
 * 输入：
 * 		无。
 * 输出：
 * 		无
 * 返回值：
 * 	 	函数执行成功，返回华睿2号芯片温度值。
 */
unsigned int bslSenseGetTemp(void)
{
	unsigned int temp = 0;
	
	return temp;
}

extern int taskCreateCore(char * name,int priority,int options,int stackSize,FUNCPTR entryPt,unsigned int core,_Vx_usr_arg_t arg0,_Vx_usr_arg_t arg1,_Vx_usr_arg_t arg2,_Vx_usr_arg_t arg3,_Vx_usr_arg_t arg4,_Vx_usr_arg_t arg5,_Vx_usr_arg_t arg6,_Vx_usr_arg_t arg7,_Vx_usr_arg_t arg8,_Vx_usr_arg_t arg9);
/*
 * 功能：绑核执行任务。
 * 输入：
 * 		name，		任务名称；
 * 		priority，	任务优先级；
 * 		stackSize，	任务堆栈大小；
 * 		entryPt，	任务函数入口；
 * 		core，		绑核的核号；
 * 		arg0~arg9，	任务函数参数1~10。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	函数执行成功，返回任务ID；函数执行失败，返回-1。
 */
TASK_ID bslTaskCreateCore(char * name,int priority,int options,int stackSize,FUNCPTR entryPt,unsigned int core,_Vx_usr_arg_t arg0,_Vx_usr_arg_t arg1,_Vx_usr_arg_t arg2,_Vx_usr_arg_t arg3,_Vx_usr_arg_t arg4,_Vx_usr_arg_t arg5,_Vx_usr_arg_t arg6,_Vx_usr_arg_t arg7,_Vx_usr_arg_t arg8,_Vx_usr_arg_t arg9)
{
	if(name == NULL)
	{
		printf("[DSP%d]绑核任务名称不能为空\n",sysCpuGetID());
		return (TASK_ID)ERROR;
	}
	if(stackSize <= 0)
	{
		printf("[DSP%d]绑核任务堆栈空间不能为0\n",sysCpuGetID());
		return (TASK_ID)ERROR;
	}
	if(entryPt == NULL)
	{
		printf("[DSP%d]绑核任务函数入口不能为NULL\n",sysCpuGetID());
		return (TASK_ID)ERROR;		
	}
	if ((core < 0) || (core > 7))
	{
		printf("[DSP%d]绑核任务core取值只能为0 ~ 7\n",sysCpuGetID());
		return (TASK_ID)ERROR;			
	}
	
	return taskCreateCore(name,priority,options,stackSize,entryPt,core,arg0,arg1,arg2,arg3,arg4,arg5,arg6,arg7,arg8,arg9);
}



/*
 * 功能：获取芯片ID号。
 * 输入：
 * 		无。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	芯片ID，取值范围为0、1、2、3。
 */
unsigned int bslProcGetId(void)
{
	return sysCpuGetID();
}

/*
 * 功能：获取芯片各个核的ID号。
 * 输入：
 * 		无。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	核ID，取值范围为0 ~ 7。
 */
unsigned int bslCoreGetId(void)
{
	return cpu_id_get();
}

/*
 * 功能：获取槽位号。
 * 输入：
 * 		无。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	函数执行成功，返回槽位号，失败返回-1。
 */
unsigned int bslGetSlotNum()
{
	return sysGetSlotNum();/*xxx ldf 20230605:: 此函数暂未实现*/
}

/*
 * 功能：设置LED灯的状态。
 * 输入：
 * 		state，LED状态，0为灯灭，1为灯亮，大于1时为闪烁。
 * 		freq，   LED闪烁的频率	。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	无。
 */
void bslLEDSetState(unsigned int state,unsigned int freq)
{
	unsigned int i = 0;
	
	if(freq == 0)
		freq = 1;
	
	if(state <= 1)
		sysLEDSetState(state);
	else
	{
		for(i = 0; i < state; i ++)
		{
			sysLEDSetState(0);
			taskDelay(sysClkRateGet()/freq/2);
			sysLEDSetState(1);
			taskDelay(sysClkRateGet()/freq/2);
		}
	}
}
/*
 * 功能：转换LED灯的状态，即从灯亮->熄灭或者由熄灭->灯亮。
 * 输入：
 * 		无。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	无。
 */
void bslLEDToggle(void)
{
	sysLEDSetState(!sysLEDGetState());
}

#if defined(INCLUDE_TFFS) && defined(DRV_NAND)
/*
 * 功能：DSPA norflash与nandflash(文件系统)之间的切换。
 * 输入：
 * 		flashIndex，flash的序号，取值0、1，0表示norflash，1表示nandflash。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	函数执行成功，返回0；函数执行失败，返回-1。
 */
extern void switchToNand();
extern void switchToNor();

int bslFlashSwitch(unsigned int flashIndex)
{
	if(sysCpuGetID() != 0)
	{
		printf("[DSP%d]Flash切换函数只能在DSPA上执行\n",sysCpuGetID());
		return ERROR;
	}
	if((flashIndex != 0) && (flashIndex != 1))
	{
		printf("[DSP%d]Flash切换函数参数flashIndex只能取值0、1\n",sysCpuGetID());
		return ERROR;
	}
	
	if(flashIndex == 0)
		switchToNor();
	else if(flashIndex == 1)
		switchToNand();
	
	return OK;
}
#endif

#if defined(INCLUDE_TFFS) && defined(HGMAC_DEV0)
#include "dosFsLib.h"
#include "stat.h"
#include "usrFsLib.h"
extern STATUS usrTffsConfig (int drive, int removable, char *  fileName);
extern STATUS sysTffsFormat (unsigned int val);
extern int flashSectorErase(unsigned int baseAddr,unsigned short sectorNum);
extern int flashBufferWrite(unsigned int addr, char * buf,unsigned int wordNum);
extern int flashWrite(unsigned int addr, unsigned short int val);

/*
 * 功能：通过网络烧写bootrom。
 * 输入：
 * 		fileName，烧写的bootrom的文件名称	。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	函数执行成功，返回0；函数执行失败，返回-1。
 */
int bslBurnBootrom(char * fileName)
{
	unsigned int i = 0; 
	unsigned char * pbuf = NULL;
	unsigned char * pbuf1 = NULL;
	unsigned int fileLength = 0;
	unsigned int mallocLength = 0;
	unsigned int sectorNumErase = 0;
	unsigned int flashPtr = 0xB0000000;
	
	FILE * fp = NULL;
	char str[100];

	if(fileName == NULL)
	{
		printf("通过网络烧写bootrom函数参数fileName不能为NULL！\n");
		return ERROR;		
	}
	strcpy(str, "host:");
	strcat(str, fileName);

	fp = fopen(str, "rb");
	if (fp == NULL) 
	{
		/* printf("open the config file error ! \n"); */
		return ERROR;
	}

	fseek(fp, 0, SEEK_END);
	fileLength = ftell(fp);
	fclose(fp);
	
	if(fileLength <= 0)
	{
		printf("open the bootrom.bin failed.\n");
		return ERROR;
	}
	if(fileLength % 2 == 0)
	{
		mallocLength = fileLength;
	}
	else
	{
		mallocLength = fileLength + 1;
	}
	printf("fileLength = %d\n",fileLength);
	pbuf = malloc(mallocLength);
	if(pbuf == NULL)
	{
		printf("malloc memery for load bootrom failed !\n");
		return ERROR;
	}

	pbuf[mallocLength - 1] = 0xFF;
	
	pbuf1 = malloc(mallocLength);
	if(pbuf1 == NULL)
	{
		printf("malloc memery for bootrom check failed !\n");
		return ERROR;
	}
	pbuf1[mallocLength - 1] = 0xFF;
	
	if(sysNetReadFile(fileName, pbuf, fileLength) == ERROR)
	{
		printf("load bootrom error !");
		free(pbuf);
		free(pbuf1);
		return ERROR;
	}

	if(fileLength % 0x20000 == 0)
	{
		sectorNumErase = fileLength / 0x20000;
	}
	else
	{
		sectorNumErase = fileLength / 0x20000 + 1;
	}
	
	printf("sectorNum to Erase = %d\n",sectorNumErase);
	printf("flash erase begin \n");
	for(i = 0; i < sectorNumErase; i ++)
	{
		if(flashSectorErase(0xb0000000,i) != OK)
		{
			free(pbuf);
			free(pbuf1);
			return ERROR;
		}
	}
	printf("flash erase finish \n");
	
	printf("burn bootrom begin \n");
	
	for(i = 0;i < mallocLength / 2 /32; i ++)              
	{
		flashBufferWrite(flashPtr + i * 32 * 2, (char *)(pbuf + i * 32 * 2),32);
	}
	
	flashBufferWrite(flashPtr + i * 32 * 2, (char *)(pbuf + i * 32 * 2),(mallocLength % 64) / 2);

	printf("burn bootrom finish \n");

	free(pbuf);
	free(pbuf1);
	
	return OK;
}

#if 1
/*
 * 功能：格式化norflash并建立文件系统。该函数会对norflash进行tffs格式化和dosfs格式化，
 * 		若文件系统已存在，此操作会清空原文件系统里的所有内容。
 * 输入：
 * 		无。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	函数执行成功，返回0；函数执行失败，返回-1。
 */

int bslFlashCreateFileSys(void) 
{
	if(sysCpuGetID() != 0)
	{
		printf("[DSP%d]该函数只能在DSPA上执行\n",sysCpuGetID());
		return ERROR;
	}
	sysTffsFormat(0);
	usrTffsConfig(0, 0, "/tffs0");
	dosfsDiskFormat("/tffs0");
	
	return OK;
}

/*
 * 功能：配置norflash文件系统。该操作挂载文件系统，不会格式化flash。
 * 输入：
 * 		无。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	函数执行成功，返回0；函数执行失败，返回-1。
 */

int bslFlashOpen(void) 
{
	if(sysCpuGetID() != 0)
	{
		printf("[DSP%d]该函数只能在DSPA上执行\n",sysCpuGetID());
		return ERROR;
	}
	
	return usrTffsConfig(0, 0, "/tffs0");
}
#endif

#endif /*INCLUDE_TFFS*/

#ifdef DRV_GDMA
#if 0  //modified by liuw on 20181012 for 64bit os
extern int hrGdmaChkParams(unsigned int srcAddr,unsigned int dstAddr,unsigned int size);
extern void hrGdma1DTransConfig(unsigned int channel,unsigned int srcAddr,unsigned int dstAddr,int size);
extern void hrGdmaStart(unsigned int channel,unsigned int transMode,unsigned int elementSize);
extern void hrGdma2DTransConfig(unsigned int channel,unsigned int total,unsigned int srcChunk,
		unsigned int srcBlk,unsigned int srcAddr,unsigned int dstAddr,unsigned int dstBlk);
extern void hrGdmaMatrixTransConfig(unsigned int channel,unsigned int chunk,unsigned int total,
		unsigned int block,unsigned int srcAddr,unsigned int dstAddr,
		unsigned int rownum,unsigned int colnum,unsigned int dstblk);
#else
extern int hrGdmaChkParams (unsigned long  ulSrcAddr, unsigned long  ulDstAddr, unsigned int  uiSrcTotalSize, unsigned int uiDstTotalSize);
extern void hrGdma1DTransConfig (unsigned int  uiChannel, char *  pcSrcAddr, char *  pcDstAddr, unsigned int  uiSize);
extern void hrGdmaStart (unsigned int  uiChannel, unsigned int  uiTransMode, unsigned int  uiElementSize);
extern void hrGdma2DTransConfig (unsigned int  uiChannel, unsigned int   uiTotal,   unsigned int   uiSrcChunk,
		unsigned int  uiSrcBlk,  char *  pcSrcAddr, char *  pcDstAddr, unsigned int  uiDstBlk);
extern void hrGdmaMatrixTransConfig (unsigned int  uiChannel, unsigned int   uiTotal,   unsigned int   uiChunk,
		unsigned int  uiBlock,   char *  pcSrcAddr, char *  pcDstAddr,
		unsigned int  uiRownum,  unsigned int   uiColnum,  unsigned int   uiDstblk);

#endif
/*********************************************************************************************************
** 函数名称: bslGDMA1DStart
** 功能描述: 启动通用DMA 一维传输
** 输　入  : pcSrcAddr    源地址(虚拟地址)
**           pcDstAddr    目的地址(虚拟地址)
**           totalSize    传输数据量大小(B)。
** 输　出  : 函数执行成功，返回 0，失败返回 -1。
** 全局变量:
** 调用模块:
*********************************************************************************************************/
int  bslGDMA1DStart (char *  pcSrcAddr, char *  pcDstAddr, unsigned int  uiTotalSize)
{
    if (g_gdmaDbgBscSign == 1) {
        g_pGdmaDbgInfo->apiInNum = g_pGdmaDbgInfo->apiInNum + 1;
    }

    if (hrGdmaChkParams((unsigned long)pcSrcAddr, (unsigned long)pcDstAddr,
                        uiTotalSize, uiTotalSize) == ERROR) {
        return  (ERROR_PARA_INPUT);
    }

    pcSrcAddr = (char *)hrKmToPhys((void *)pcSrcAddr);
    pcDstAddr = (char *)hrKmToPhys((void *)pcDstAddr);

    hrGdma1DTransConfig(0, pcSrcAddr, pcDstAddr, uiTotalSize);
    hrGdmaStart(0, 0, 0);
    if (g_gdmaDbgBscSign == 1) {
        g_pGdmaDbgInfo->apiOutNum = g_pGdmaDbgInfo->apiOutNum + 1;
    }

    return  (OK);
}

/*********************************************************************************************************
** 函数名称: bslGDMA2DStart
** 功能描述: 启动通用 DMA 二维传输
**           通用 DMA 二维传输时，数据既可以是连续地址的，也可以是不连续地址的。
**           uiSrcRowSize 是连续数据串的大小，uiSrcRowSpan 为两个 uiSrcRowSize 数据串的起始地址差,
**           支持了不连续数据的传输。每次 DMA 搬运的总数据大小是 32 位的，最大支持 4GB 数据传输。
** 输　入  : uiTransType        传输模式，0 为二维至二维传输；1 为二维至一维传输；2 为一维至二维传输；
**           pcSrcAddr          源地址；
**           uiSrcRowSize       每次传输的串大小；
**           uiSrcRowSpan       源矩阵的行跨度，其大小应不小于 uiSrcRowSize；
**           pcDstAddr          目的地址；
**           uiDstRowSpan       目的矩阵的行跨度，其大小应不小于 uiSrcRowSize；
**           uiTotalSize        传输数据量大小(B)，最大支持 4GB 数据传输。。
** 输　出  : 函数执行成功，返回 0，失败返回 -1。
** 全局变量:
** 调用模块:
*********************************************************************************************************/
int  bslGDMA2DStart (unsigned int   uiTransType,  char *  pcSrcAddr,
		unsigned int   uiSrcRowSize, unsigned int   uiSrcRowSpan, char *  pcDstAddr,
		unsigned int   uiDstRowSpan, unsigned int   uiTotalSize)
{

    if (g_gdmaDbgBscSign == 1) {
        g_pGdmaDbgInfo->apiInNum = g_pGdmaDbgInfo->apiInNum + 1;
    }

    if ((uiTransType < 0) || (uiTransType > 2)) {
        printf("通用DMA传输类型错误！\n");
        return  (ERROR_PARA_INPUT);
    }

    if (hrGdmaChkParams((unsigned long)pcSrcAddr,
                        (unsigned long)pcDstAddr,
                        ((uiTotalSize - 1) / uiSrcRowSize) *
                        (uiSrcRowSpan - uiSrcRowSize) + uiTotalSize,
                        ((uiTotalSize - 1) / uiSrcRowSize) *
                        (uiSrcRowSpan - uiSrcRowSize) + uiTotalSize) == ERROR) {
        return  (ERROR_PARA_INPUT);
    }

    if ((uiSrcRowSpan < uiSrcRowSize) || (uiDstRowSpan < uiSrcRowSize)) {
        printf("通用DMA二维传输需要满足条件：srcRowSpan(dstRowSpan) >=srcRowSize ！\n");
        return  (ERROR_PARA_INPUT);
    }

    pcSrcAddr = (char *)hrKmToPhys((void *)pcSrcAddr);
    pcDstAddr = (char *)hrKmToPhys((void *)pcDstAddr);

    hrGdma2DTransConfig(0, uiTotalSize, uiSrcRowSize, uiSrcRowSpan,
                        pcSrcAddr, pcDstAddr, uiDstRowSpan);
    hrGdmaStart(0, uiTransType + 1,0);

    if (g_gdmaDbgBscSign == 1) {
        g_pGdmaDbgInfo->apiOutNum = g_pGdmaDbgInfo->apiOutNum + 1;
    }

    return  (OK);
}

/*********************************************************************************************************
** 函数名称: bslGDMAMtxStart
** 功能描述: 启动通用 DMA 矩阵转置传输(简化版)
**           在源矩阵和目的矩阵的数据都是连续的情况下使用。
**           若需要对源矩阵或目的矩阵的数据是不连续的情况进行操作，则调用函数 bslGDMAMtxStartCpx。
**           实现对存放在内存中的矩阵，进行从源矩阵到目标矩阵的行列转置功能。
** 输　入  : pcSrcAddr          源地址；
**           pcDstAddr          目的地址；
**           uiSrcRowNum        源矩阵的行数；
**           uiSrcColNum        源矩阵的列数;
**           uiElementSign      矩阵元素大小(B)，取值4、8。
** 输　出  : 函数执行成功，返回 0，失败返回 -1。
** 全局变量:
** 调用模块:
*********************************************************************************************************/
int  bslGDMAMtxStart (char *  pcSrcAddr,   char *  pcDstAddr, unsigned int  uiSrcRowNum,
		unsigned int   uiSrcColNum, unsigned int   uiElementSize)
{
    if (g_gdmaDbgBscSign == 1) {
        g_pGdmaDbgInfo->apiInNum = g_pGdmaDbgInfo->apiInNum + 1;
    }

    if (hrGdmaChkParams((ULONG)pcSrcAddr, (ULONG)pcDstAddr,
                        uiSrcRowNum * uiSrcColNum * uiElementSize,
                        uiSrcRowNum * uiSrcColNum * uiElementSize) == ERROR) {
        return  (ERROR_PARA_INPUT);
    }

    if ((uiElementSize != 4) && (uiElementSize != 8)) {
        printf("通用DMA矩阵转置传输矩阵元素大小错误，只能取值4,8！\n" );
        return  (ERROR_PARA_INPUT);
    }

    if (((void *)uiSrcRowNum == NULL) || ((void *)uiSrcColNum == NULL)) {
        printf("通用DMA矩阵转置传输srcRowNum或srcColNum错误！\n");
        return  (ERROR_PARA_INPUT);
    }

    pcSrcAddr = (char *)hrKmToPhys((void *)pcSrcAddr);
    pcDstAddr = (char *)hrKmToPhys((void *)pcDstAddr);

    hrGdmaMatrixTransConfig(0,
                            uiSrcRowNum * uiSrcColNum * uiElementSize,
                            uiSrcColNum * uiElementSize,
                            uiSrcColNum * uiElementSize,
                            pcSrcAddr, pcDstAddr, uiSrcRowNum, uiSrcColNum,
                            uiSrcRowNum * uiElementSize);
    hrGdmaStart(0, 4, uiElementSize);

    if (g_gdmaDbgBscSign == 1) {
        g_pGdmaDbgInfo->apiOutNum = g_pGdmaDbgInfo->apiOutNum + 1;
    }

    return  (OK);
}


/*********************************************************************************************************
** 函数名称: bslGDMAMtxStartCpx
** 功能描述: 启动通用 DMA 矩阵转置传输
**           实现对存放在内存中的矩阵，进行从源矩阵到目标矩阵的行列转置功能。
**           通用 DMA 矩阵转置传输时，数据既可以是连续地址的，也可以是不连续地址的。
**           uiSrcRowSize 是连续数据串的大小，uiSrcRowSpan 为两个 uiSrcRowSize 数据串的起始地址差,
**           支持了不连续数据的传输。每次 DMA 搬运的总数据大小是 32 位的，最大支持 4GB 数据传输。
** 输　入  : pcSrcAddr          源地址；
**           uiSrcRowSize       每次传输的串大小；
**           uiSrcRowSpan       源矩阵的行跨度，其大小应不小于 uiSrcRowSize；
**           pcDstAddr          目的地址；
**           uiDstRowSpan       目的矩阵的行跨度，其大小应不小于 uiSrcRowSize；
**           uiSrcRowNum        源矩阵的行数；
**           uiSrcColNum        源矩阵的列数;
**           uiElementSize      矩阵元素大小(B)，取值4、8。
** 输　出  : 函数执行成功，返回 0，失败返回 -1。
** 全局变量:
** 调用模块:
*********************************************************************************************************/
int  bslGDMAMtxStartCpx (char *  pcSrcAddr,   unsigned int   uiSrcRowSize,
		unsigned int   uiSrcRowSpan, char *  pcDstAddr,   unsigned int  uiDstRowSpan,
		unsigned int   uiSrcRowNum,  unsigned int   uiSrcColNum, unsigned int  uiElementSize)
{
    if (g_gdmaDbgBscSign == 1) {
        g_pGdmaDbgInfo->apiInNum = g_pGdmaDbgInfo->apiInNum + 1;
    }

    if (hrGdmaChkParams((ULONG)pcSrcAddr, (ULONG)pcDstAddr,
                        uiSrcRowSpan * (uiSrcRowNum - 1) + uiSrcColNum * uiElementSize,
                        uiDstRowSpan * (uiSrcColNum - 1) + uiSrcRowNum * uiElementSize) == ERROR) {
        return  (ERROR_PARA_INPUT);
    }

    if((uiElementSize != 4) && (uiElementSize != 8)) {
        printf("通用DMA矩阵转置传输矩阵元素大小错误，只能取值4,8！\n");
        return  (ERROR_PARA_INPUT);
    }

    if ((uiSrcRowSpan < uiSrcColNum * uiElementSize) ||
        (uiDstRowSpan < uiSrcRowNum * uiElementSize)) {
        printf("通用DMA矩阵转置传输需要满足条件："
               "srcRowSpan(dstRowSpan) >= elementSize * srcColNum(srcRowNum) \n");
        return  (ERROR_PARA_INPUT);
    }

    if ((uiSrcRowNum == 0) || (uiSrcColNum == 0)) {
        printf("通用DMA矩阵转置传输srcRowNum或srcColNum错误！\n");
        return  (ERROR_PARA_INPUT);
    }

    pcSrcAddr = (char *)hrKmToPhys((void *)pcSrcAddr);
    pcDstAddr = (char *)hrKmToPhys((void *)pcDstAddr);

    hrGdmaMatrixTransConfig(0,
                            uiSrcRowNum * uiSrcColNum * uiElementSize,
                            uiSrcRowSize,
                            uiSrcRowSpan,
                            pcSrcAddr,
                            pcDstAddr,
                            uiSrcRowNum,
                            uiSrcColNum,
                            uiDstRowSpan);
    hrGdmaStart(0, 4, uiElementSize);

    if (g_gdmaDbgBscSign == 1) {
        g_pGdmaDbgInfo->apiOutNum = g_pGdmaDbgInfo->apiOutNum + 1;
    }

    return  (OK);
}
#endif



#ifdef DRV_CDMA
#if 0//modified by liuw on 20181012 for 64bit os
extern int hrCdmaChkParams(unsigned int srcAddr,unsigned int dstAddr,unsigned int size);
extern void hrCdma1DTransConfig(unsigned int channel,unsigned int srcAddr,unsigned int dstAddr,unsigned int size);
extern void hrCdma2DTransConfig(unsigned int channel,
		unsigned int total,unsigned int chunck,unsigned int srcBlk,unsigned int dstBlk,
		unsigned int srcAddr,unsigned int dstAddr);
extern void hrCdmaMatrixTransConfig(unsigned int channel,
		unsigned int total,unsigned int chunck,unsigned int srcBlk,unsigned int dstBlk,
		unsigned int rowNum,unsigned int colNum,unsigned int srcAddr,unsigned int dstAddr);
extern void hrCdmaStart(unsigned int channel,unsigned int transMode,unsigned int elementSize,unsigned int wrCacheType,unsigned int rdCacheType);
#else
extern int hrCdmaChkParams (unsigned long  ulSrcAddr, unsigned long  ulDstAddr, unsigned int  uiSrcTotalSize, unsigned int  uiDstTotalSize);
extern void hrCdma1DTransConfig (unsigned int  uiChannel, char *  pcSrcAddr, char *  pcDstAddr, unsigned int  uiSize);
extern void hrCdma2DTransConfig (unsigned int  uiChannel, unsigned int  uiTotal,  unsigned int   uiChunck,
		unsigned int  uiSrcBlk,  unsigned int  uiDstBlk, char *  pcSrcAddr, char *  pcDstAddr);
extern void hrCdmaMatrixTransConfig (unsigned int   uiChannel, unsigned int   uiTotal,  unsigned int  uiChunck,
		unsigned int   uiSrcBlk,  unsigned int   uiDstBlk, unsigned int  uiRowNum, unsigned int  uiColNum,
                               char *  pcSrcAddr, char *  pcDstAddr);
extern void hrCdmaStart (unsigned int  uiChannel,     unsigned int  uiTransMode, unsigned int  uiElementSize,
		unsigned int  uiWrCacheType, unsigned int  uiRdCacheType);
#endif


#include "huarui3/drv/cachedma/hr3CacheDma.h"
extern int g_cdmaDbgBscSign; /* 记录基本调试信息的标识，0表示不记录，1表示记录*/
extern int g_cdmaDbgDtlSign; /* 记录详细调试信息的标识，0表示不记录，1表示记录*/
extern int g_cdmaTransState[CDMA_NUM];
extern HRCDMADBGINFO * g_pCdmaDbgInfo[CDMA_NUM];

/*********************************************************************************************************
** 函数名称: bslCDMA1DStart
** 功能描述: 启动核内 DMA 一维传输
** 输　入  : pcSrcAddr    源地址
**           pcDstAddr    目的地址
**           totalSize    传输数据量大小(B)。
** 输　出  : 函数执行成功，返回 0，失败返回 -1。
** 全局变量:
** 调用模块:
*********************************************************************************************************/
int  bslCDMA1DStart (char *  pcSrcAddr, char *  pcDstAddr, unsigned int  uiTotalSize)
{
    if (g_cdmaDbgBscSign == 1) {
        g_pCdmaDbgInfo[sysCpuGetID()]->apiInNum = g_pCdmaDbgInfo[sysCpuGetID()]->apiInNum + 1;
    }

    if (hrCdmaChkParams((ULONG)pcSrcAddr, (ULONG)pcDstAddr, uiTotalSize, uiTotalSize) == ERROR) {
        return  (ERROR_PARA_INPUT);
    }

    pcSrcAddr = (char *)hrKmToPhys((void *)pcSrcAddr);
    pcDstAddr = (char *)hrKmToPhys((void *)pcDstAddr);

    hrCdma1DTransConfig(0, pcSrcAddr, pcDstAddr, uiTotalSize);

    hrCdmaStart(0, 0, 0, 0, 0);/*ldf 20230515:: 改为uncache会不会速率快些??*/

//	printk("[%s],[%d],[%d]\n",__FUNCTION__,__LINE__,sysCpuGetID());
    if (g_cdmaDbgBscSign == 1) {
        g_pCdmaDbgInfo[sysCpuGetID()]->apiOutNum = g_pCdmaDbgInfo[sysCpuGetID()]->apiOutNum + 1;
    }
//	printk("[%s],[%d]\n",__FUNCTION__,__LINE__);

    return  (OK);
}

/*********************************************************************************************************
** 函数名称: bslCDMA2DStart
** 功能描述: 启动核内 DMA 二维传输
**           核内 DMA 二维传输时，数据既可以是连续地址的，也可以是不连续地址的。
**           uiSrcRowSize 是连续数据串的大小，uiSrcRowSpan 为两个 uiSrcRowSize 数据串的起始地址差,
**           支持了不连续数据的传输。每次 DMA 搬运的总数据大小是 32 位的，最大支持 4GB 数据传输。
** 输　入  : pcSrcAddr        源地址；
**           uiSrcRowSize     每次传输的串大小；
**           uiSrcRowSpan     源矩阵的行跨度，其大小应不小于 uiSrcRowSize；
**           pcDstAddr        目的地址；
**           uiDstRowSpan     目的矩阵的行跨度，其大小应不小于 uiSrcRowSize；
**           uiTotalSize      传输数据量大小(B)。
** 输　出  : 函数执行成功，返回 0，失败返回 -1。
** 全局变量:
** 调用模块:
*********************************************************************************************************/
int  bslCDMA2DStart (char *  pcSrcAddr,    unsigned int   uiSrcRowSize,
		unsigned int   uiSrcRowSpan, char *  pcDstAddr,
		unsigned int   uiDstRowSpan, unsigned int   uiTotalSize)
{
    if (g_cdmaDbgBscSign == 1) {
        g_pCdmaDbgInfo[sysCpuGetID()]->apiInNum = g_pCdmaDbgInfo[sysCpuGetID()]->apiInNum + 1;
    }

    if (hrCdmaChkParams((ULONG)pcSrcAddr, (ULONG)pcDstAddr,
                        ((uiTotalSize - 1) / uiSrcRowSize) *
                        (uiSrcRowSpan - uiSrcRowSize) + uiTotalSize,
                        ((uiTotalSize - 1) / uiSrcRowSize) *
                        (uiDstRowSpan - uiSrcRowSize) + uiTotalSize) == ERROR) {
        return  (ERROR);
    }

    if ((uiSrcRowSpan < uiSrcRowSize) || (uiDstRowSpan < uiSrcRowSize)) {
        printf("核内DMA二维传输需要满足条件：block(dstblock) >=chunk ！\n");
        return  (ERROR);
    }

    pcSrcAddr = (char *)hrKmToPhys((void *)pcSrcAddr);
    pcDstAddr = (char *)hrKmToPhys((void *)pcDstAddr);

    hrCdma2DTransConfig(0,  uiTotalSize, uiSrcRowSize,
                        uiSrcRowSpan, uiDstRowSpan,
                        pcSrcAddr,    pcDstAddr);
    hrCdmaStart(0, 0, 0, 0, 0);

    if (g_cdmaDbgBscSign == 1) {
        g_pCdmaDbgInfo[sysCpuGetID()]->apiOutNum = g_pCdmaDbgInfo[sysCpuGetID()]->apiOutNum + 1;
    }

    return  (OK);
}

/*********************************************************************************************************
** 函数名称: bslCDMAMtxStart
** 功能描述: 启动核内 DMA 矩阵转置传输(简化版)，在源矩阵和目的矩阵的数据都是连续的情况下使用。
**           若需要对源矩阵或目的矩阵的数据是不连续的情况进行操作，则调用函数 bslCDMAMtxStart 即可实现。
** 输　入  : pcSrcAddr        源地址；
**           pcDstAddr        目的地址；
**           uiSrcRowNum      源矩阵的行数；
**           uiSrcColNum      源矩阵的列数；
**           uiElementSize    矩阵元素大小(B)，取值1、2、4、8。
** 输　出  : 函数执行成功，返回 0，失败返回 -1。
** 全局变量:
** 调用模块:
*********************************************************************************************************/
int  bslCDMAMtxStart (char *  pcSrcAddr,   char *  pcDstAddr, unsigned int  uiSrcRowNum,
		unsigned int   uiSrcColNum, unsigned int   uiElementSize)
{
    if (g_cdmaDbgBscSign == 1) {
        g_pCdmaDbgInfo[sysCpuGetID()]->apiInNum = g_pCdmaDbgInfo[sysCpuGetID()]->apiInNum + 1;
    }

    if (hrCdmaChkParams((ULONG)pcSrcAddr, (ULONG)pcDstAddr,
                        uiSrcRowNum * uiSrcColNum * uiElementSize,
                        uiSrcRowNum * uiSrcColNum * uiElementSize) == (ERROR)) {
        return  (ERROR_PARA_INPUT);
    }

    if ((uiElementSize != 1) && (uiElementSize != 2) &&
        (uiElementSize != 4) && (uiElementSize != 8)) {
        printf("核内DMA矩阵转置传输矩阵元素大小错误，只能取值1,2,4,8！\n");
        return  (ERROR_PARA_INPUT);
    }

    if (((void*)uiSrcRowNum == NULL) || ((void*)uiSrcColNum == NULL)) {
        printf("核内DMA矩阵转置传输srcRowNum或srcColNum错误！\n");
        return  (ERROR_PARA_INPUT);
    }

    pcSrcAddr = (char *)hrKmToPhys((void *)pcSrcAddr);
    pcDstAddr = (char *)hrKmToPhys((void *)pcDstAddr);

    hrCdmaMatrixTransConfig(0,
                            uiSrcRowNum * uiSrcColNum * uiElementSize,
                            uiSrcColNum * uiElementSize,
                            uiSrcColNum * uiElementSize,
                            uiSrcRowNum * uiElementSize,
                            uiSrcRowNum, uiSrcColNum, pcSrcAddr, pcDstAddr);
    hrCdmaStart(0, 1, uiElementSize, 0, 0);

    if (g_cdmaDbgBscSign == 1) {
        g_pCdmaDbgInfo[sysCpuGetID()]->apiOutNum = g_pCdmaDbgInfo[sysCpuGetID()]->apiOutNum + 1;
    }

    return  (OK);
}

/*********************************************************************************************************
** 函数名称: bslCDMAMtxStartCpx
** 功能描述: 启动核内 DMA 矩阵转置传输。
**           实现对存放在内存中的矩阵，进行从源矩阵到目标矩阵的行列转置功能。
**           核内 DMA 矩阵转置传输时，数据既可以是连续地址的，也可以是不连续地址的。
**           uiSrcRowSize 是连续数据串的大小，uiSrcRowSpan 为两个 uiSrcRowSize 数据串的起始地址差,
**           支持了不连续数据的传输。每次 DMA 搬运的总数据大小是 32 位的，最大支持 4GB 数据传输。
** 输　入  : pcSrcAddr        源地址；
**           uiSrcRowSize     每次传输的串大小；
**           uiSrcRowSpan     源矩阵的行跨度，其大小应不小于 uiSrcRowSize；
**           uiDstAddr        目的地址；
**           uiDstRowSpan     目的矩阵的行跨度，其大小应不小于 uiSrcRowSize；
**           uiSrcRowNum      源矩阵的行数；
**           uiSrcColNum      源矩阵的列数；
**           uiWrCacheType    dma 写的模式，0 为 cache write; 1 为 uncache write；
**           uiRdCacheType    dma 读的模式，0 为 cache read;  1 为 uncache read；
**           uiElementSize    矩阵元素大小(B)，取值1、2、4、8。
** 输　出  : 函数执行成功，返回 0，失败返回 -1。
** 全局变量:
** 调用模块:
*********************************************************************************************************/
int  bslCDMAMtxStartCpx (char *  pcSrcAddr,   unsigned int  uiSrcRowSize,  unsigned int  uiSrcRowSpan,
		char *  pcDstAddr,   unsigned int  uiDstRowSpan,  unsigned int  uiSrcRowNum,
		unsigned int   uiSrcColNum, unsigned int  uiWrCacheType, unsigned int  uiRdCacheType,
		unsigned int   uiElementSize)
{
    if (g_cdmaDbgBscSign == 1) {
        g_pCdmaDbgInfo[sysCpuGetID()]->apiInNum = g_pCdmaDbgInfo[sysCpuGetID()]->apiInNum + 1;
    }

    if (hrCdmaChkParams((ULONG)pcSrcAddr, (ULONG)pcDstAddr,
                        uiSrcRowSpan * (uiSrcRowNum - 1) + uiSrcColNum * uiElementSize,
                        uiDstRowSpan * (uiSrcColNum - 1) + uiSrcRowNum * uiElementSize) == ERROR) {
        return  (ERROR_PARA_INPUT);
    }

    if ((uiElementSize != 1) && (uiElementSize != 2) &&
        (uiElementSize != 4) && (uiElementSize != 8)) {
        printf("核内DMA矩阵转置传输矩阵元素大小错误，只能取值1,2,4,8！\n");
        return  (ERROR_PARA_INPUT);
    }

    if ((uiSrcRowSpan < uiSrcColNum * uiElementSize) ||
        (uiDstRowSpan < uiSrcRowNum * uiElementSize)) {
        printf("核内DMA二维传输需要满足条件："
               "srcRowSpan(dstRowSpan) >= elementSize * srcColNum(srcRowNum) \n");
        return  (ERROR_PARA_INPUT);
    }

    if (uiWrCacheType > 1) {
        printf("核内DMA矩阵转置传输wrCacheType错误！\n");
        return  (ERROR_PARA_INPUT);
    }

    if (uiRdCacheType > 1) {
        printf("核内DMA矩阵转置传输rdCacheType错误！\n");
        return  (ERROR_PARA_INPUT);
    }

    if((uiSrcRowNum == 0) || (uiSrcColNum == 0)) {
        printf("核内DMA矩阵转置传输srcRowNum或srcColNum错误！\n");
        return  (ERROR_PARA_INPUT);
    }

    pcSrcAddr = (char *)hrKmToPhys((void *)pcSrcAddr);
    pcDstAddr = (char *)hrKmToPhys((void *)pcDstAddr);

    hrCdmaMatrixTransConfig(0, uiSrcRowNum * uiSrcColNum * uiElementSize,
                            uiSrcRowSize, uiSrcRowSpan, uiDstRowSpan,
                            uiSrcRowNum,  uiSrcColNum,  pcSrcAddr, pcDstAddr);
    hrCdmaStart(0, 1, uiElementSize, uiWrCacheType, uiRdCacheType);

    if (g_cdmaDbgBscSign == 1) {
        g_pCdmaDbgInfo[sysCpuGetID()]->apiOutNum = g_pCdmaDbgInfo[sysCpuGetID()]->apiOutNum + 1;
    }

    return  (OK);
}

#endif


#if defined(DRV_TIMER0) || defined(DRV_TIMER1)
extern unsigned int timer0LoopCount;
extern unsigned int timer1LoopCount;
extern void hrTimerInit(unsigned int timerIndex,VOIDFUNCPTR routine,int param);
extern void  hrTimerModuleInit (u32 index);

/*
 * 精度为1us
 */
extern void hrTimerSetValue(u32 index, u64 value);
extern u64 hrTimerGetValue(u32 index);
/*
 * mode = 0:周期的；mode = 1:一次的
 */
extern void hrTimerSetMode(u32 index, u32 mode);
extern int hrTimerStart(u32 timerIndex);
extern void hrTimerStop(u32 timerIndex);
extern void hrTimerInstConnect(u32 index, FUNCPTR routine, u64 param);
/*
 * 功能：Timer的初始化，并完成用户程序的挂接。
 * 输入：
 * 		timerIndex，	Timer的序号，取值0或1；
 * 		mode，		工作模式，取值0或1，设置为0时，表示周期性的循环计数；设置为1时，表示一次性的计数；
 * 		value，		Timer要初始化的值；
 * 		callback，	用户程序，即计时中断产生时用户要做的工作；
 * 		param，		用户程序的参数。
 * 输出：
 * 		无
 * 返回值：
 * 	 	函数执行成功，返回0；函数执行失败，返回-1 
 */
//extern int  hr_bsl_timer_callback_in_int;   //added by liuw for spy cpu   2018-05-07
int bslTimerInit(unsigned int timerIndex, unsigned int mode,unsigned long long value,FUNCPTR callback,int param)
{
//	unsigned int timerIndex = 0;
#if 1
	if((timerIndex < 0) || (timerIndex > 1))
	{
		printf("[DSP%d]timerIndex的值只能取0、1\n",sysCpuGetID());
		return ERROR;
	}
#endif
	if((mode < 0) || (mode > 1))
	{
		printf("[DSP%d]mode的值只能取0、1\n",sysCpuGetID());
		return ERROR;
	}
	if(value <= 0)
	{
		printf("[DSP%d]value的值只能取大于0的数\n",sysCpuGetID());
		return ERROR;		
	}
	if(callback == NULL)
	{
		printf("[DSP%d]函数指针callback不能为空\n",sysCpuGetID());
		return ERROR;		
	}
 
	hrTimerModuleInit(timerIndex);
	hrTimerInstConnect(timerIndex, (FUNCPTR)callback, param);
	hrTimerSetValue(timerIndex,value);
	hrTimerSetMode(timerIndex,mode);
	
//added by liuw on 2018-05-07 for spy cpu&mem	
//    if(timerIndex)
//    	hr_bsl_timer_callback_in_int =1;
//    else
//    	hr_bsl_timer_callback_in_int =0;
//end
	return OK;
}

/*
 * 功能：设置定时器状态(开始计时或停止计时)。
 * 输入：
 * 		timerIndex，Timer的序号，取值0或1；
 * 		state,		定时器状态，0为停止计时，1为开始计时，大于1时为起state次计时中断。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	函数执行成功，返回0；函数执行失败，返回-1。
 */
int bslTimerSetState(unsigned int timerIndex,unsigned int state)
 {
//	unsigned int timerIndex = 0;
#if 0	
	if((timerIndex < 0) || (timerIndex > 1)) 
	{
		printf("[DSP%d]timerIndex的值只能取0、1\n",sysCpuGetID());
		return ERROR;
	}
#endif
	if((state < 0) || (state > 1)) 
	{
		printf("[DSP%d]state的值只能取0、1\n",sysCpuGetID());
		return ERROR;
	}
	if(state == 1)
	{
		return hrTimerStart(timerIndex);
	}
	else if(state == 0)
	{
		hrTimerStop(timerIndex);
	}
	else
	{
		if(timerIndex == 0)
		{
			timer0LoopCount = state;
		}
		else if(timerIndex == 1)
		{
			timer1LoopCount = state;
		}
		
		hrTimerSetMode(timerIndex,0);
		hrTimerStart(timerIndex);
	}
	return OK;
}
#endif

#if	defined(DRV_PCIE) && defined(DRV_GPIO)
extern void hrPcieDMARecInit(PFUNC_PCIE_CBACK func,int param0,int param1,int param2);
extern void hrPcieDMAWrite(UINT64 src_addr, UINT64 dst_addr,UINT32 size);
extern void hrPcieDMARead( UINT64 src_addr, UINT64 dst_addr,UINT32 size);
extern void hrPcieSetInbound(unsigned int devType,/*unsigned char barNum,*/ unsigned int axiAddr);
extern void hrGpioIPISet(unsigned int remoteCpuId);


/*
 * 功能：设置PCIE INBOUND窗口。
 * 输入：
 * 		devType，		PCIE设备类型，0表示EP，1表示RC；
 * 		dstAddr，		INBOUND窗对应的本地DDR地址。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	无。
 */
int bslPcieSetInbound(unsigned int devType,/*unsigned char barNum,*/ unsigned int dstAddr)
{
//	if((devType < 0) || (devType > 1))
//	{
//		printf("[DSP%d]设置PCIE INBOUND窗口时参数devType错误！\n",sysCpuGetID());
//		return ERROR;		
//	}
//	hrPcieSetInbound(devType,/*unsigned char barNum,*/ dstAddr);
////	pcie_ctrl_ib(0, 0, 0, 31);
	return OK;
}


/*
 * 功能：初始化PCIE的接收任务。
 * 输入：
 * 		func，		用户函数，即PCIE DMA接收中断产生时用户要做的工作；
 * 		param0，		发送方 CPU ID 号,
 * 		param1，		接收方偏移地址
 * 		param2，		接收数据包大小
 * 输出：
 * 		无
 * 返回值：
 * 	 	函数执行成功，返回0；函数执行失败，返回-1。
 */
extern int pcie_dma_recv_handler(int cpu_id,int offset,int buf_len);
extern int pcie_mem_read_recv_handler(int dspID,int offset,int buf_len);

int bslPcieRecInit(PFUNC_PCIE_CBACK func,void *param0,void *param1,void *param2)
{
//#if 1
//	if(func == NULL )
//	{
//		hrPcieMSIInit(pcie_mem_read_recv_handler,param0,param1,param2);
//	}
//	else
//	{
//		hrPcieMSIInit(func,param0,param1,param2);
//	}
//#else/*xxx ldf 20230601:: DMA还存在问题，暂时先不使用DMA*/
//	if(func == NULL )
//	{
//		hrPcieDMARecInit(pcie_dma_recv_handler,param0,param1,param2);
//	}
//	else
//	{
//		hrPcieDMARecInit(func,param0,param1,param2);
//	}
//#endif

	return OK;
}
/*
 * 功能：PCIE DMA实现片间数据传输。
 * 默认数据接收区：0x20000000；
 * 默认数据发送区：0x30000000。
 * 目前片间传输方式如下：
 * A片可以访问B、C、D (此时的DMA读或写的数据量不能大于1MB)；
 * B片可以访问C		(此时的DMA读或写的数据量不能大于1MB)；
 * C片可以访问D		(此时的DMA读或写的数据量不能大于1MB)；
 * D片可以访问A		(此时的DMA读或写的数据量不能大于1MB)。
 * 输入：
 * 		dmaType，		dma传输类型，0表示dma读，1表示dma写；
 * 		localAddr，		本地DDR地址，需128B对齐；
 * 		remoteCpuId，	CPU ID号，取值0,1,2,3；
 * 		offset，			DMA操作时remoteCpu端的偏移量，DMA读时为OUTBOUND窗口的偏移量，DMA写时为INBOUND窗口的偏移量，需128B对齐；
 * 		size，			传输数据量大小(B)。
 * 输出：
 * 		无。
 * 返回值：
 * 	 	函数执行成功，返回0；函数执行失败，返回-1。
 */
int bslPcieDMAStart(unsigned int dmaType, unsigned long localAddr, unsigned int remoteCpuId,unsigned int offset,unsigned int size)
{
	unsigned long remoteAddr = 0;
	
//	if(localAddr % 0x80 != 0)
//	{
//		printf("[DSP%d]PCIE DMA 传输时参数localAddr必须是128B对齐！\n",sysCpuGetID());
//		return ERROR;
//	}
//	if(dmaType > 1)
//	{
//		printf("[DSP%d]PCIE DMA 传输时参数dmaType错误！\n",sysCpuGetID());
//		return ERROR;		
//	}
//	if(remoteCpuId >= 4)
//	{
//		printf("[DSP%d]PCIE DMA 传输时参数remoteCpuId错误！\n",sysCpuGetID());
//		return ERROR;
//	}
//	if(bslProcGetId() != 0)
//	if((size > 0x100000) || (size == 0x0))
//	{
//		printf("[DSP%d]PCIE DMA 传输时参数size错误！\n",sysCpuGetID());
//		return ERROR;
//	}
//	
//	switch (bslProcGetId()) 
//	{
//	case 0:
//		if(remoteCpuId == 0)
//		{
//			printf("[DSP%d]PCIE DMA 传输时，A片只能与B、C、D片进行片间数据传输！\n",sysCpuGetID());
//			return ERROR;
//		}
//		break;
//	case 1:
//		if(/*(remoteCpuId != 0) &&*/ (remoteCpuId != 2))
//		{
//			printf("[DSP%d]PCIE DMA 传输时，B片只能与C片进行片间数据传输！\n",sysCpuGetID());
//			return ERROR;
//		}
//		break;
//	case 2:
//		if(/*(remoteCpuId != 1) &&*/ (remoteCpuId != 3))
//		{
//			printf("[DSP%d]PCIE DMA 传输时，C片只能与D片进行片间数据传输！\n",sysCpuGetID());
//			return ERROR;
//		}
//		break;
//	case 3:
//		if(/*(remoteCpuId != 3) &&*/ (remoteCpuId != 0))
//		{
//			printf("[DSP%d]PCIE DMA 传输时，D片只能与A片进行片间数据传输！\n",sysCpuGetID());
//			return ERROR;
//		}
//		break;
//	default:
//		break;
//	}
	
//	switch(remoteCpuId)
//	{
//	case 0:
//		remoteAddr = 0x30000000 + offset;
//		break;
//	case 1:
//		remoteAddr = 0x18e00000 + offset;
//		break;
//	case 2:
//		remoteAddr = 0x18600000 + offset;
//		break;
//	case 3:
//		remoteAddr = 0x18a00000 + offset;
//		break;
//	default:
//		break;
//	}
	
	remoteAddr = remoteCpuId?(0x31000000 + offset):(2089000000 + offset);
	
	localAddr = hrKmToPhys((void *)localAddr);
	
	if(/*dmaType*/sysCpuGetID() == 0)
		hrPcieDMARead(remoteAddr, localAddr, size);
	else
	{
		hrPcieDMAWrite(localAddr, remoteAddr, size);
		*(unsigned int *)PCIE_PARAM0_REG = sysCpuGetID();
		*(unsigned int *)PCIE_PARAM1_REG = offset;
		*(unsigned int *)PCIE_PARAM2_REG = size;
	}
	//通过片间中断通知对方CPU
//	hrGpioIPISet(remoteCpuId);/*xxx ldf 20230608:: 未实现,后续可改为MSI*/
	
	return OK;
}

#endif

#if defined(DRV_GPIO) 	

extern void  hrGpioInstInit (void);
extern int hrGpioGetIntSem(unsigned int gpioIndex);
extern int hrGpioSetTransDirection(u32 gpioIndex, u32 direction);
extern u32 hrGpioSetOutputVal(u32 gpioIndex, u32 outputVal);
extern u32 hrGpioGetOutputVal(u32 gpioIndex);
extern void  hrGpioIntEnable (int  gpioIndex);
extern void  hrGpioIntDisable (int  gpioIndex);
extern u32 hrGpioGetIntNum(u32 gpioIndex);
extern int hrGpioSetIntMode(u32 gpioIndex, u32 mode);
extern int hrGpioSetIntEdge(u32 gpioIndex, u32 mode);
extern  int hrGpioGetRawIntStatus(u32 gpioIndex);
extern int hrGpioGetIntStatus(u32 gpioIndex);
extern int hrGpioGetDbgInfo();

/*
 * 功能：获取外部中断产生时释放的信号量。
 * 输入：
 *		gpioIndex，GPIO ID号，取值0~23；
 * 输出：无。
 * 返回值：成功，0； 
 *        失败，-1。
 */
int bslGpioGetIntSem(unsigned int gpioIndex)
{
	if((gpioIndex < 0) || (gpioIndex > 23))
	{
		printf("[DSP%d]获取GPIO外部中断信号量时，参数gpioIndex错误！\n",sysCpuGetID());
		return -1;
	}
	hrGpioGetIntSem(gpioIndex);

	return 0;
}

void bslUsrgGpioInit(void)
{
	hrGpioInstInit();
	return;
}

void bslUsrgGpioConnect(u32 gpioIndex,GPIO_INT_HANDLER routine,void *param)
{
	hrGpioIntConnect(gpioIndex, routine, param);
	return;
}
#endif

#ifdef INCLUDE_USERMEM
extern int hrMemChkParams(void * buf, unsigned int nBytes);
extern void * hrUserMemAllocSendBuf(unsigned int sendBufIndex,  unsigned int align, unsigned int nBytes);
extern void * hrUserMemAllocRecvBuf(unsigned int recvBufIndex,  unsigned int align, unsigned int nBytes);
extern int hrUserMemSendBufInit(unsigned int initType,	
		void * sendBuf0, unsigned int sendBuf0Size,
		void * sendBuf1, unsigned int sendBuf1Size,
		void * sendBuf2, unsigned int sendBuf2Size,
		void * sendBuf3, unsigned int sendBuf3Size);
extern int hrUserMemRecvBufInit(unsigned int initType,	
		void * recvBuf0, unsigned int recvBuf0Size,
		void * recvBuf1, unsigned int recvBuf1Size,
		void * recvBuf2, unsigned int recvBuf2Size,
		void * recvBuf3, unsigned int recvBuf3Size);

/*
 * 功能：
 * 		对于4核芯片：
 * 		初始化发送或接收缓存，默认情况下，系统建立四块发送缓存，
 * 		其中两块在DDR0地址空间，基地址分别为0x10000000和0x20000000，大小为0x10000000，
 * 		另外两块在DDR1地址空间， 基地址分别是0x60000000和0x70000000，大小均是0x10000000。
 * 		 系统建立四块接收缓存，
 * 		 其中两块在DDR0地址空间，基地址分别为0x30000000和0x40000000，大小为0x10000000，
 * 		 另外两块在DDR1地址空间，基地址分别是0xd0000000和0xe0000000，大小均是0x10000000。
 * 		应用可以调用此函数重新定义发送和接收缓存的起始地址和空间大小。
 * 输入：
 * 		bufType， 	缓存类型，0为发送缓存，1为接收缓存；
 * 		initType，	初始化类型，0为重新建立缓存，1为接着建立缓存；
 *		buf0，		缓存0基地址；
 *		buf0Size，	缓存0容量；
 *		buf1，		缓存1基地址；
 *		buf1Size，	缓存1容量；
 *		buf2，		缓存2基地址；
 *		buf2Size，	缓存2容量；
 *		buf3，		缓存3基地址；
 *		buf3Size，	缓存3容量。
 * 输出：无。
 * 返回值：成功，0； 
 *        失败，-1。
 */
int bslUserMemBufInit(unsigned int bufType,unsigned int initType,
		void * buf0, unsigned int buf0Size,
		void * buf1, unsigned int buf1Size,
		void * buf2, unsigned int buf2Size,
		void * buf3, unsigned int buf3Size)
{
	if(bufType > 1)
	{
		printf("[DSP%d]建立用户空间缓存时，参数bufType错误！\n",sysCpuGetID());
		return ERROR;
	}
	if(initType > 1)
	{
		printf("[DSP%d]建立用户空间缓存时，参数initType错误！\n",sysCpuGetID());
		return ERROR;
	}
	if(hrMemChkParams(buf0, buf0Size))
		return ERROR_PARA_INPUT;
	
	if(hrMemChkParams(buf1, buf1Size))
		return ERROR_PARA_INPUT;
	
	if(hrMemChkParams(buf2, buf2Size))
		return ERROR_PARA_INPUT;
	
	if(hrMemChkParams(buf3, buf3Size))
		return ERROR_PARA_INPUT;
	
	if(bufType == 0)
	{
		return hrUserMemSendBufInit(initType,
					buf0, buf0Size,buf1, buf1Size,buf2, buf2Size,buf3, buf3Size);
	}
	else
	{
		return hrUserMemRecvBufInit(initType,
					buf0, buf0Size,buf1, buf1Size,buf2, buf2Size,buf3, buf3Size);
	}
}

/*
 * 功能：默认情况下，在DDR0中分配了512MB的发送和接收缓存，在DDR1中分配了512MB的发送和接收缓存，
 * 		均定义为循环缓冲区,这个函数从其中一个缓冲区分配指定长度的DDR空间。注意，这个循环
 * 		缓冲区不检测空间使用状态，用户需要控制在一次数据循环需要缓存的总数据量不得溢出，
 * 		溢出后旧数据被抛弃，不管是否被使用。申请到的缓存地址为USER_MEM_ALLOC_ALIGN 
 * 		(默认为128字节)对齐，每次分配的长度大小受限于宏USER_MEM_ALLOC_MAX_LEN(默认为8MB)
 * 		的定义，若大于该值，则按该值指定空间申请。应用程序可以用这个地址作为计算结果地址，
 * 		而不用考虑自己去建乒乓或环形缓冲区。
 *	输入：
 *		bufType， 	缓存类型，0为发送缓存，1为接收缓存；
 *		bufIndex， 	发送缓存区序号；
 *		align，		对齐方式，必须为4的倍数，作为RIO发送地址时，要求至少128字节对齐；
 *		nBytes，		申请的空间大小(字节)。
 *	输出：无。
 *	返回值：成功，0； 
 *         失败，-1。
 */
void * bslUserMemAllocBuf(unsigned int bufType,unsigned int bufIndex, unsigned int align, unsigned int nBytes)
{
	if(bufType > 1)
	{
		printf("[DSP%d]申请用户空间缓存时，参数bufType错误！\n",sysCpuGetID());
		return NULL;
	}

	if(bufType == 0)
	{
		return hrUserMemAllocSendBuf(bufIndex, align, nBytes);
	}
	else
	{
		return hrUserMemAllocRecvBuf(bufIndex, align, nBytes);
	}
	
}
#endif

#ifdef DRV_RASP0
extern void hr28GetRasp(int raspId);
/*
 * 功能：获取RASP计算完成后释放的信号量。
 * 输入：
 *		raspId，RASP ID号，取值0、1、2、3；
 * 输出：无。
 * 返回值：成功，0； 
 *        失败，-1。
 */
int bslRaspGetDoneSem(int raspId)
{
	if(raspId > 3)
	{
		printf("[DSP%d]获取RASP完成中断信号量时，参数raspId错误！\n",sysCpuGetID());
		return ERROR;
	}
	hr28GetRasp(raspId);
	//hrRaspGetDoneSem(raspId);
	return OK;
}
#endif

#ifdef DRV_I2C
//#include "../huarui3/drv/i2c_v2/i2c_v2.h"
#define I2C_BASE 0x1F0D0000
#define I2C0_BASE (0x9000000000000000 | ((u64)I2C_BASE))
#define I2C1_BASE (0x9000000000000000 | ((u64)I2C_BASE + 0x8000))
#define I2CS_BASE_ADDR(channel)	(channel ? I2C1_BASE : I2C0_BASE)
/*
 * 功能：设置I2C slave模式下RAM空间值。
 * 输入：
 *		devIndex，	设备序号，取值0、1；
 *		offset，		偏移量，取值0~255；
 *		data，		要设置的值。
 * 输出：无。
 * 返回值：成功，0； 
 *        失败，-1。
 */
int bslI2CSSetData(unsigned int devIndex,unsigned int offset,unsigned int data)
{
	if(devIndex > 1)
	{
		printf("[DSP%d]设置I2C slave模式下RAM空间值时参数devIndex 错误!\n",sysCpuGetID());
		return ERROR;
	}

	if(offset > 255)
	{
		printf("[DSP%d]设置I2C slave模式下RAM空间值时参数memAddr 错误!\n",sysCpuGetID());
		return ERROR;
	}
	
	
	*(unsigned int *)(I2CS_BASE_ADDR(devIndex) + offset) = data;
	
	return OK;
}
/*
 * 功能：I2C master模式下与slave设备传输数据。
 * 输入：
 *		devIndex，	设备序号，取值0、1；
 *		transType，	传输类型，0为从slave设备读取值，1为向slave设备写入值；
 *		ddrAddr，	本地DDR地址，slave设备读取时为目标地址，向slave设备写入时为源数据地址；
 *		length，		传输数据长度(字节)；
 *		slaveId，	slave设备ID地址；
 *		memAddr，	slave设备RAM地址，取值0~255。
 * 输出：无。
 * 返回值：成功，0； 
 *        失败，-1。
 */
int bslI2CMTransData(unsigned int devIndex,unsigned int transType,unsigned char* ddrAddr,unsigned int length,unsigned int slaveId,unsigned int memAddr)
{
	if(devIndex > 1)
	{
		printf("[DSP%d]I2C传输数据时参数devIndex 错误!\n",sysCpuGetID());
		return ERROR;
	}
	if(transType > 1)
	{
		printf("[DSP%d]I2C传输数据时参数transType 错误!\n",sysCpuGetID());
		return ERROR;
	}
	if(memAddr > 0xffffffff)
	{
		printf("[DSP%d]I2C传输数据时参数memAddr 错误!\n",sysCpuGetID());
		return ERROR;
	}
	
	if(transType == 0) //读取
	{
		i2cBusTransfer(devIndex, slaveId, memAddr, ddrAddr, length, 1);
//		i2c_lib_master_recv_data(I2CS_BASE_ADDR(devIndex), slaveId, memAddr, ddrAddr, length, HZ/10000, 0);
//		i2cBusRead(devIndex, slaveId, memAddr, /*2, 1,*/ ddrAddr, length);
//		i2c_master_receive_data(devIndex, (u8 *)ddrAddr, length, ADDR_MODE_7BIT, slaveId, NO_START_BYTE, memAddr, SLV_MEMADDR_1BYTE, TRUE);
	}
	else if(transType == 1) //设置
	{
		i2cBusTransfer(devIndex, slaveId, memAddr, ddrAddr, length, 0);
//		i2c_lib_master_send_data(I2CS_BASE_ADDR(devIndex), slaveId, memAddr, ddrAddr, length, HZ/10000, 0);
//		i2cBusWrite(devIndex, slaveId, memAddr, /*2, 1,*/ ddrAddr, length);
//		i2c_master_send_data(devIndex, (u8 *)ddrAddr, length, ADDR_MODE_7BIT, slaveId, NO_START_BYTE, memAddr, SLV_MEMADDR_1BYTE, TRUE);
	}
	return OK;
}
#endif


#ifdef	DRV_LPC
//#include "../drv/lpcflash/hcm_LPCFlash.h"
extern int lpc_write_data(unsigned int lpc_addr, unsigned int byte_mode, void* data, int cycle);
extern int lpc_read_data(unsigned int lpc_addr, unsigned int byte_mode, void* data, int cycle);


/*
 * 功能：LPC空间读接口。
 * 输入：
 * 		add:地址；
 * 		len:字节数
 * 输出：
 *		buf
 * 返回值：
 * 	 	函数执行成功，返回0,失败返回-1。
 */
int bslLpcReadData(unsigned int addr, unsigned int len, unsigned char *buf)
{
	int ret = 0;
	
	//	printk("<***DEBUG***> [%s:_%d_]:: addr: 0x%lx\n",__FUNCTION__,__LINE__,addr);
	ret = lpc_read_data(addr, 1, (void*)buf, len);
	
//	libl_flash_read_bytes(LPC_REGS, addr, buf, len);
	

	return ret;
}

/*
 * 功能：LPC空间写接口。
 * 输入：
 * 		add:地址；
 * 		len:字节数
 * 		buf: 要写入的数据
 * 输出：
 *		
 * 返回值：
 * 	 	函数执行成功，返回0,失败返回-1。
 */
int bslLpcWriteData(unsigned int addr, unsigned int len, unsigned char *buf)
{
	int ret = 0;
	
	//	printk("<***DEBUG***> [%s:_%d_]:: addr: 0x%lx\n",__FUNCTION__,__LINE__,addr);
	ret = lpc_write_data(addr, 1, (void*)buf, len);
	
//	libl_flash_program_bytes(LPC_REGS, addr, buf, len);
	
	return ret;
}


#endif


extern void get_bsp_info(MOD_INFO *bsp_info);
extern void get_board_info(BOARD_INFO *board_info);
extern void get_os_info(OS_INFO *os_info);
/*打印BSP版本信息*/
void bslBspVersionRead(void)
{
	MOD_INFO bsp_info;
	BOARD_INFO board_info;
	OS_INFO os_info;
	
	get_bsp_info(&bsp_info);
	get_board_info(&board_info);
	get_os_info(&os_info);
	
	return;
}

#if defined(DRV_RIOC0)||defined(DRV_RIOC1)

/*
 * 功能:设置本地rapidio的节点及路由
 * 参数:
 * u16 rio0ID:rio0的节点号
 * u16 rio1ID:rio1的节点号
 */
void bslRioSetLocalRoute(u16 rio0ID,u16 rio1ID)
{
	u32 _temp;
	u16 _port;

//	if( m_RioInit==0 )
//	{
//		printf("[DSP%d]bslRioSetLocalRoute::bslRioInit not intialized.\r\n",sysCpuGetID());
//		return;
//	}
	
	bslRioSetID(0,rio0ID);
	bslRioSetID(1,rio1ID);
	
	//配置RapidIO控制器0的维护窗口
	bslRioSetMaintOW(0);
	printf("[DSP%d]set rab0 maintance ow ok.\n",sysCpuGetID());
	//get rab0 - 1848 port number
	bslRioMaintRead(0,0xff,0,0x14,&_port);
	_port = _port&0xff;	
	bslRioMaintWrite(0,0xff,0,0x70,rio0ID);
	bslRioMaintWrite(0,0xff,0,0x74,_port);
	//enable 1848 port0 tx/rx
	bslRioMaintRead(0,0xff,0,0x15c+0x20*_port,&_temp);
	_temp = _temp|(0x3<<21);
	bslRioMaintWrite(0,0xff,0,0x15c+0x20*_port,_temp);

	bslRioMaintRead(0,rio0ID, 1, 0x60, &_temp);
	printf("[DSP%d]read rab0 id from maintance: id = %x.\n",sysCpuGetID(),_temp);

	//配置RapidIO控制器1的维护窗口
	bslRioSetMaintOW(1);
	printf("[DSP%d]set rab1 maintance ow ok.\n",sysCpuGetID());
	//get rab1 - 1848 port number
	bslRioMaintRead(1,0xff,0,0x14,&_port);
	_port = _port&0xff;	
	bslRioMaintWrite(1,0xff,0,0x70,rio1ID);
	bslRioMaintWrite(1,0xff,0,0x74,_port);		
	//enable 1848 port1 tx/rx
	bslRioMaintRead(1,0xff,0,0x15c+0x20*_port,&_temp);
	_temp = _temp|(0x3<<21);	
	bslRioMaintWrite(1,0xff,0,0x15c+0x20*_port,_temp);	

	bslRioMaintRead(1,rio1ID, 1, 0x60, &_temp);
	printf("[DSP%d]read rab1 id from maintance: id = %x.\n",sysCpuGetID(),_temp);
}

#endif

void hr3API_module_init()
{
	return;
}
