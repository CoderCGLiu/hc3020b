/*
 * hr3SysTools.c
 *
 *  Created on: 2018-6-6
 *      Author: wangwei
 */
#include <vxWorks.h>
#include <time.h>
#include <stdio.h>
#include "tool/hr3Macro.h"
#include "tool/hr3SysTools.h"
#include <stdarg.h>
#include <spinLockLib.h>
#include <taskLib.h>
#include "bsp_sys_io.h"
#include "../h/drvCommon.h"

unsigned char verbosity_ = 10;
//static struct tm localTime;

struct ADDRINFO
{
	char * dev;
	unsigned long addr;
};

const struct ADDRINFO addrInfo[] = 
{
	//监测
	{DBG_CORE0_NAME, DBG_CORE0_REG_BASE_ADDR},
	{DBG_CORE1_NAME, DBG_CORE1_REG_BASE_ADDR},
	{DBG_CORE2_NAME, DBG_CORE2_REG_BASE_ADDR},
	{DBG_CORE3_NAME, DBG_CORE3_REG_BASE_ADDR},
	
	{DBG_CORE0_DMA_NAME, DBG_CORE0_DMA_BASE_ADDR},
	{DBG_CORE1_DMA_NAME, DBG_CORE1_DMA_BASE_ADDR},
	{DBG_CORE2_DMA_NAME, DBG_CORE2_DMA_BASE_ADDR},
	{DBG_CORE3_DMA_NAME, DBG_CORE3_DMA_BASE_ADDR},
	
	{DBG_GDMA_NAME, DBG_GDMA_BASE_ADDR},
	
	{DBG_RIO0_NAME, DBG_RIO0_BASE_ADDR},
	{DBG_RIO1_NAME, DBG_RIO1_BASE_ADDR},
	
	{DBG_GMAC0_NAME, DBG_GMAC0_BASE_ADDR},
	{DBG_GMAC1_NAME, DBG_GMAC1_BASE_ADDR},
	{DBG_GMAC2_NAME, DBG_GMAC2_BASE_ADDR},
	
	{DBG_GPIO1_NAME, DBG_GPIO1_BASE_ADDR},
	{DBG_GPIO2_NAME, DBG_GPIO2_BASE_ADDR},
	{DBG_GPIO3_NAME, DBG_GPIO3_BASE_ADDR},
	{DBG_GPIO4_NAME, DBG_GPIO4_BASE_ADDR},
	{DBG_GPIO5_NAME, DBG_GPIO5_BASE_ADDR},
	{DBG_GPIO6_NAME, DBG_GPIO6_BASE_ADDR},
	{DBG_GPIO7_NAME, DBG_GPIO7_BASE_ADDR},
	
	{DBG_PCIE_NAME, DBG_PCIE_BASE_ADDR},
	
	{DBG_UART_NAME, DBG_UART_BASE_ADDR},
	
	{DBG_RASP0_NAME, DBG_RASP0_BASE_ADDR},
	{DBG_RASP1_NAME, DBG_RASP1_BASE_ADDR},
	{DBG_RASP2_NAME, DBG_RASP2_BASE_ADDR},
	{DBG_RASP3_NAME, DBG_RASP3_BASE_ADDR},
	
	{DBG_NANDFLASH_NAME, DBG_NANDFLASH_BASE_ADDR},
	{DBG_NORFLASH_NAME, DBG_NORFLASH_BASE_ADDR},
	
	{DBG_CAN_NAME, DBG_CAN_BASE_ADDR},
	
	{DBG_IIC_NAME, DBG_IIC_BASE_ADDR},
	
	{DBG_WDOG_NAME, DBG_WDOG_BASE_ADDR},
	
	{DBG_TIMER0_NAME, DBG_TIMER0_BASE_ADDR},
	{DBG_TIMER1_NAME, DBG_TIMER1_BASE_ADDR},
	
	//性能测试
	{PERF_MEM_NAME, PERF_MEM_BASE_ADDR},
	{PERF_NAND_NAME, PERF_NAND_BASE_ADDR},
	{PERF_NOR_NAME, PERF_NOR_BASE_ADDR},
	{PERF_IPI_NAME, PERF_IPI_BASE_ADDR},
	{PERF_PCIE_NAME, PERF_PCIE_BASE_ADDR},
	{PERF_TIMER_NAME, PERF_TIMER_BASE_ADDR},
	{PERF_GMAC_NAME, PERF_GMAC_BASE_ADDR},
	{PERF_SRIO_NAME, PERF_SRIO_BASE_ADDR},
	{PERF_SNDPIPE_NAME, PERF_SNDPIPE_BASE_ADDR},
	{PERF_RCVPIPE_NAME, PERF_RCVPIPE_BASE_ADDR},
};
void printstr(char *s) 
{
	unsigned long port = HR2_UART_PORT0;
	while (*s) 
	{
		*(unsigned char*) port = *s;
		s++;
	}
}
void printnum10(unsigned long long n) 
{
	int i = 0;
	int j = 0;
	unsigned char a[40];
	unsigned long port = HR2_UART_PORT0;

	do {
		a[i] = n % 10;
		n = n / 10;
		i++;
	} while (n);

	for (j = i - 1; j >= 0; j--) 
	{
		if (a[j] >= 10)
		{
			*(unsigned char*) port = 'a' + a[j] - 10;
		}
		else 
		{
			*(unsigned char*) port = '0' + a[j];
		}
	}
}

void printnum(unsigned long long n) 
{
	int i = 0;
	int j = 0;
	unsigned char a[40];
	unsigned long port = HR2_UART_PORT0;

	do {
		a[i] = n % 16;
		n = n / 16;
		i++;
	} while (n);

	for (j = i - 1; j >= 0; j--) 
	{
		if (a[j] >= 10) 
		{
			*(unsigned char*) port = 'a' + a[j] - 10;
		}
		else 
		{
			*(unsigned char*) port = '0' + a[j];
		}
	}
}

//tmp add by fukai,2018-5-22
void printnum1(unsigned long long n) 
{
	int i = 0;
	int j = 0;
	unsigned char a[40];
	unsigned long port = HR2_UART_PORT0;

	do {
		a[i] = n % 16;
		n = n / 16;
		i++;
	} while (n);

	for (j = i - 1; j >= 0; j--) 
	{
		if (a[j] >= 10) 
		{
			*(unsigned char*) port = 'a' + a[j] - 10;
		}
		else 
		{
			*(unsigned char*) port = '0' + a[j];
		}
	}
	*(unsigned char*) port = '#';
	*(unsigned char*) port = '#';
	*(unsigned char*) port = '\r';
	*(unsigned char*) port = '\n';
}

//delay_cnt(100);//约1us
void delay_cnt(unsigned int count)
{
	volatile unsigned int i = 0;
	for(i=0;i<count;i++)
	{
		;
	}
}
/*******************************************************************************
*
* sysDelay - allow recovery time for port accesses
*
* This routine provides a brief delay used between accesses to the same serial
* port chip.
* 
* RETURNS: N/A
*/
void sysDelay400ns (unsigned int ns400counts)
{
	int i = 0;
	for(i=0; i<ns400counts;i++)
	{
		(void) sysInByte (0xbfc00000UL);   /* it takes about 400ns */
	}
}

void pprintf(int priority, const char *format, ...) 
{
	char buffer[4096];
	va_list args;
	unsigned int length;

	if (priority > verbosity_)   
		return;

#ifdef PRINT_DIO		
   	va_start(args, format);  
	length = vsnprintf(buffer, sizeof buffer, format, args);	
	if (length >= sizeof(buffer)) 
	{
    		length = sizeof buffer;
    		buffer[sizeof buffer - 1] = '\n';		
  	}	
	printstr(buffer);
	va_end(args);
#else	
	
	va_start(args, format);  
	length = vsnprintf(buffer, sizeof buffer, format, args);	
	if (length >= sizeof(buffer)) 
	{
    		length = sizeof buffer;
    		buffer[sizeof buffer - 1] = '\n';		
  	}	
	printf(buffer);
	va_end(args);
#endif
}


BOOL t_usleep(unsigned int microseconds)
{
  struct timespec req;
  req.tv_sec = microseconds / 1000000;
  // Convert microseconds argument to nano seconds.
  req.tv_nsec = (microseconds % 1000000) * 1000;
  return nanosleep(&req, NULL) == 0;
}

BOOL t_sleep(time_t seconds) 
{
  struct timespec req;
  req.tv_sec = seconds;
  req.tv_nsec = 0;
  return nanosleep(&req, NULL) == 0;
}

void * spinlockTaskInit()
{
	spinlockTask_t * pTaskLock = NULL;
	
	pTaskLock = malloc(sizeof(spinlockTask_t));
	if(pTaskLock == NULL) 
		return NULL;
	
	spinLockTaskInit(pTaskLock, 0);  
	return (void *)pTaskLock;
}
void spinlockTaskTake(void *lock_id)
{
	if(lock_id == NULL) 
		return;
	
	spinLockTaskTake((spinlockTask_t *)lock_id);
}
void spinlockTaskGive(void *lock_id)
{
	if(lock_id == NULL) 
		return;
	
	spinLockTaskGive((spinlockTask_t *)lock_id);
}


void timeDelay(unsigned int loopNum)
{
	unsigned int i = 0;
	unsigned int j = 0;
	for(i = 0; i < loopNum; i ++)
	{
		j = j + 1;
	}
}


unsigned int sysCpuGetID()
{
	unsigned int cpuId = 0;

	cpuId = (*(unsigned int *)DSP_NUM_REG & 0x3);
	
//	printk("<DEBUG> [%s:__%d__]:: cpuId = %d\n",__FUNCTION__,__LINE__,cpuId);

	return cpuId;
}


/* 
 * 获取槽位号
 * 使用SRIO启动时不能使用PCIe相关的东西，否则会系统启动失败
 * */
unsigned int sysGetSlotNum()
{
	unsigned short int tmp = 0;
	
#if 0
	//ww mod 20190218 add 
//	return 0xff;
	//ww mod 20190218 add 
	
	
//	tmp = *(unsigned short int *)(0xffffffffb8300000);
	tmp = *(unsigned short int *) MODULE_ID_REG;
	tmp = tmp & 0x00FF;
#else
	tmp = 1;
#endif
	return tmp;
}


/* 获取插箱号 */
unsigned int sysGetCaseNum()
{
	unsigned short int tmp = 0;
	
	//ww mod 20190218 add 
//	return 1;
	//ww mod 20190218 add 
#if 0
//	tmp = *(unsigned short int *)(0xffffffffb8300000);
	tmp = *(unsigned short int *) MODULE_ID_REG;
	tmp = (tmp >> 8) & 0x00FF;	
#else
	tmp = 1;
#endif
	return tmp;
}

/* 获取机柜号 */
unsigned int sysGetCabnNum()
{
	unsigned short int tmp = 0;
	
	//tmp = *(unsigned short int *)MODULE_ID_REG;
	
	return tmp;
}



/*
 * 功能：板上4片CPU的串口切换
 * 输入：
 * 		cpuIndex，CPU的序号，取值0、1、2、3
 * 输出：
 * 		无
 * 返回值：
 * 	 	函数执行成功，返回0；函数执行失败，返回-1
 */
int sysUartSwitch(unsigned int cpuIndex)
{
	if((cpuIndex < 0) || (cpuIndex > 3))
	{
		printf("串口切换函数参数cpuIndex错误，只能取值0、1、2、3.\n");
		return ERROR;
	}
	
	*(volatile unsigned int *)UART_SWITCH_TX_REG = 0;
	*(volatile unsigned int *)UART_SWITCH_RX_REG = 0;

	*(volatile unsigned int *)UART_SWITCH_TX_REG = (1 << cpuIndex);
	*(volatile unsigned int *)UART_SWITCH_RX_REG = (1 << cpuIndex);

  
	return OK;
}


unsigned long long usr_get_time(struct timespec *ts)
{
	unsigned long long tv_sec = 0, tv_nsec = 0;
	
    clock_gettime(CLOCK_MONOTONIC, ts);
    tv_sec = ts->tv_sec;
    tv_nsec = ts->tv_nsec;
	printf("<DEBUG> [%s:__%d__]:: tv_sec=%llu, tv_nsec=%llu\n",__FUNCTION__,__LINE__,tv_sec,tv_nsec);
    return (tv_sec * 1000000 + tv_nsec / 1000);//us
}



void sysLEDSetState(unsigned int state)
{
//	*(unsigned int *)(0xffffffffbf088804) = !state;
	*(volatile unsigned int *)LED_CTRL_REG = !state;
}

/*
 * 功能：获取LED状态
 * 输入：
 * 		无。	
 * 输出：
 * 		无
 * 返回值：
 * 	 	LED的状态，0为灯灭；1为灯亮。
 */
unsigned int sysLEDGetState()
{
	return !(*(unsigned int *)LED_CTRL_REG);
}

void sysLEDReverseState()
{
	sysLEDSetState(!sysLEDGetState());
}

void sysLEDTwinkle(int cnt)
{
//	static int led = 0;
	while(1)
	{
		delay_cnt(0x10000000);
//		sysLEDSetState(led);
//		led = !led;	
		sysLEDReverseState();
		if (--cnt == 0) break; 
	}
}

/*
 * 功能：板级复位或插箱复位
 * 输入：
 * 		resetType，复位类型，0为板级复位，4片DSP会复位，FPGA不会复位；
 * 		1为插箱复位。	
 * 输出：
 * 		无
 * 返回值：
 * 	 	无。
 * 	 	
 * 	 	
 * 	 	？？？？？如果是插箱复位的话，会不会复位整个插箱的立面除了华睿2号以外的其他模块？？？
 */
void sysReset(unsigned int resetType)
{
	//必须加volatile,不然需要把优化关掉
	u32 temp = 0;
	if(resetType == 0)
	{
//		HR2_REG_WRITE8(RESET_CTRL_REG, 0x1);
//		HR2_REG_WRITE8(RESET_CTRL_REG, 0x0);
		
		phx_write_u32(0x1f0c0028, 0xA5ACCEDE);/*配置其它寄存器的通道解锁*/
		temp = phx_read_u32(0x1f0c0008);
		temp |= 0x1<<0;
		phx_write_u32(0x1f0c0008, temp);/*ldf 20230523 add:: 板级全局复位*/
	}
	else if(resetType == 1)
	{
//		HR2_REG_WRITE8(RESET_CTRL_REG, 0x2);
	}
  
}

//#include <vxWorks.h>
/* 任务绑核 */
TASK_ID taskCreateCore(char * name,int priority,int options,size_t stackSize,FUNCPTR entryPt,unsigned int core,
		_Vx_usr_arg_t arg0,_Vx_usr_arg_t arg1,_Vx_usr_arg_t arg2,_Vx_usr_arg_t arg3,_Vx_usr_arg_t arg4,_Vx_usr_arg_t arg5,_Vx_usr_arg_t arg6,_Vx_usr_arg_t arg7,_Vx_usr_arg_t arg8,_Vx_usr_arg_t arg9)
{
	TASK_ID tid = 0;
	cpuset_t affinity = 0;
//	options |= VX_UNBREAKABLE;
//	printk("%s %d options 0x%lx stackSize 0x%lx entryPt 0x%lx\n",__FUNCTION__,__LINE__,options,stackSize,entryPt);
	tid = taskCreate(name,priority,options,stackSize,entryPt,arg0,arg1,arg2,arg3,arg4,arg5,arg6,arg7,arg8,arg9);
	if(tid == NULL)
	{
		printf("%s 任务创建失败！\n",name);
		return 0;
	}
	
	CPUSET_ZERO(affinity);
	CPUSET_SET(affinity,core);
//	printk("%s %d tid 0x%lx affinity 0x%lx sizeof(TASK_ID) %d\n",__FUNCTION__,__LINE__,tid,affinity,sizeof(TASK_ID));
	if(taskCpuAffinitySet(tid,affinity) == ERROR)
	{
		printf("%s 任务绑核失败！\n",name);
		taskDelete(tid);
		return 0;
	}
	
	taskActivate(tid);
	return tid;
}

#if 0

/*64位虚拟地址映射到64位物理地址*/
unsigned long long hrKmToPhys(unsigned long long addr_mapped)
{
	
	/*modify by ww 20190726,其实是映射到DMA地址*/
	if(addr_mapped>=0xffffffff80000000 && addr_mapped<0xffffffff90000000)//mips体系架构决定的
		return ((addr_mapped - 0xffffffff80000000) & 0x0fffffff);
	
	if(addr_mapped>=0xffffffff90000000 && addr_mapped<0xffffffffa0000000)//mips体系架构决定的
		return ((addr_mapped - 0xffffffff80000000) & 0x1fffffff);
	
	if(addr_mapped>=0xffffffffa0000000 && addr_mapped<0xffffffffb0000000)//mips体系架构决定的
		return ((addr_mapped - 0xffffffffa0000000) & 0x0fffffff);
	
	if(addr_mapped>=0xffffffffb0000000 && addr_mapped<0xffffffffc0000000)//mips体系架构决定的
		return ((addr_mapped - 0xffffffffa0000000) & 0x1fffffff);

	//for hr3
#if 0
	if(addr_mapped>=0x10000000UL && addr_mapped<0x60000000UL)
		return (addr_mapped - 0x10000000UL+0x20000000UL);
	
	else if(addr_mapped>=0x60000000UL && addr_mapped<0x80000000UL)
		return (addr_mapped - 0x60000000UL+0x70000000UL);
	
	else if(addr_mapped>=0xFFFFFFFFD0000000UL && addr_mapped<0xFFFFFFFFF0000000UL)
		return (addr_mapped - 0xFFFFFFFFD0000000UL+0x90000000UL);
#else
//	if(addr_mapped>=0x810000000UL && addr_mapped<(0x810000000UL+0x80000000UL))
//		return addr_mapped ;
	if(addr_mapped>=0x20000000UL && addr_mapped<(0x20000000UL+0x40000000UL))
		return addr_mapped + 0x60000000 ;
#endif
	
	else printk("virt_to_phys error 0x%x\n",addr_mapped);
		return addr_mapped;
	

}


/*64位物理地址映射到64位虚拟地址*/
unsigned long hrPhysToKm(unsigned long addr_phys)
{

	//printk("addr_phys = 0x%lx \r\n",addr_phys);
	/*modify by ww 20190726,其实是映射到DMA地址*/
	if(addr_phys>=0x00000000 && addr_phys<0x20000000)//映射大小0x40000000即为1GB大小
		return (addr_phys + 0xffffffff80000000);/*cache空间***********************88*/

	//for hr3
#if 0

	else if(addr_phys>=0x20000000UL && addr_phys<0x70000000UL)
		return (addr_phys - 0x20000000UL+0x10000000UL);
	
	else if(addr_phys>=0x70000000UL && addr_phys<0x90000000UL)
		return (addr_phys - 0x70000000UL+0x60000000UL);
	
	else if(addr_phys>=0x90000000UL && addr_phys<0xB0000000UL)
		return (addr_phys - 0x90000000UL+0xFFFFFFFFD0000000UL);
#else
//	if(addr_phys>=0x810000000UL && addr_phys<(0x810000000UL+0x80000000UL))
//		return addr_phys ;

	else if( (addr_phys>=0x80000000) && (addr_phys<(0x80000000+0x40000000)) )
		return (addr_phys - 0x60000000);
	else if( (addr_phys>=0xffffffff80000000UL) && (addr_phys<(0xffffffff80000000UL+0x40000000)) )
		return (addr_phys - 0x60000000); // 转换为0x20000000 虚地址
#endif

	else printk("phys_to_virt error %x\n",addr_phys);
		return addr_phys;

}
#else


/*64位虚拟地址映射到64位物理地址*/
extern ptrdiff_t sys_virtual_to_phy(ptrdiff_t virtual_addr);
unsigned long long hrKmToPhys(void *addr_mapped)
{
#if 0
	/*modify by ww 20190726,其实是映射到DMA地址*/
	if(addr_mapped>=0xffffffff80000000 && addr_mapped<0xffffffff90000000)//mips体系架构决定的
		return ((addr_mapped - 0xffffffff80000000) & 0x0fffffff);
	
	if(addr_mapped>=0xffffffff90000000 && addr_mapped<0xffffffffa0000000)//mips体系架构决定的
		return ((addr_mapped - 0xffffffff80000000) & 0x1fffffff);
	
	if(addr_mapped>=0xffffffffa0000000 && addr_mapped<0xffffffffb0000000)//mips体系架构决定的
		return ((addr_mapped - 0xffffffffa0000000) & 0x0fffffff);
	
	if(addr_mapped>=0xffffffffb0000000 && addr_mapped<0xffffffffc0000000)//mips体系架构决定的
		return ((addr_mapped - 0xffffffffa0000000) & 0x1fffffff);
	

	else if(addr_mapped>=0x90000000UL && addr_mapped<(0x90000000UL + 0x70000000UL))
		return (addr_mapped);
#if 0 /*HR3测试板*/
	else if(addr_mapped>=0x1080000000UL && addr_mapped<(0x1080000000UL+0x180000000UL))
		return (addr_mapped);
#else /*HR3 VPX*/
	else if(addr_mapped>=0x1080000000UL && addr_mapped<(0x1080000000UL+0x80000000UL))
		return (addr_mapped);
	else if(addr_mapped>=0x1800000000UL && addr_mapped<(0x1800000000UL+0x100000000UL))
		return (addr_mapped);
#endif
	else printk("virt_to_phys error %x\n",addr_mapped);
		return addr_mapped;
#else
		
		return sys_virtual_to_phy((ptrdiff_t)addr_mapped);/*ldf 20230409*/
#endif
}

/*64位物理地址映射到64位虚拟地址*/
extern ptrdiff_t sys_phy_to_virtual(ptrdiff_t addr_phys);
unsigned long long hrPhysToKm(void *addr_phys)
{

#if 0
	/*modify by ww 20190726,其实是映射到DMA地址*/
	if(addr_phys>=0x00000000 && addr_phys<0x20000000)//映射大小0x40000000即为1GB大小
		return (addr_phys + 0xffffffff80000000);/*cache空间***********************88*/

	else if(addr_phys>=0x90000000UL && addr_phys<(0x90000000UL + 0x70000000UL))
		return (addr_phys);
#if 0 /*HR3 测试板*/
	else if(addr_phys>=0x1080000000UL && addr_phys<(0x1080000000UL+0x180000000UL))
		return (addr_phys);
#else /*HR3 VPX*/
	else if(addr_phys>=0x1080000000UL && addr_phys<(0x1080000000UL+0x80000000UL))
		return (addr_phys);
	else if(addr_phys>=0x1800000000UL && addr_phys<(0x1800000000UL+0x100000000UL))
		return (addr_phys);
#endif
	else printk("phys_to_virt error %x\n",addr_phys);
		return addr_phys;
#else
		
		return sys_phy_to_virtual((ptrdiff_t)addr_phys);/*ldf 20230409*/
#endif
}

#endif


unsigned int sysUsrInfoGetAddr(char * devName)
{
	unsigned int i = 0;
	unsigned int elemNum = 0;
	
	if(devName == NULL)
	{
		printf("函数sysUsrInfoGetAddr中参数devName不能为空.\n");
		return NULL;
	}
	elemNum = sizeof(addrInfo)/sizeof(struct ADDRINFO);
	
	for(i = 0; i < elemNum; i++)
	{
		if(strcmp(devName, addrInfo[i].dev) == 0)
		{
			return addrInfo[i].addr;
		}
	}
	printf("未找到设备%s对应的信息地址.\n",devName);
	return NULL;
}


/*
 * 功能：读取芯片DSP(RASP)、DDR、AXI和ACE的配置情况。
 * 输入：
 * 		无。	
 * 输出：
 * 		无。
 * 返回值：
 * 	 	OK。
 */
int sysChipGetConfig(void)
{
	unsigned int regValue  = 0;
	/* DSP(RASP) FREQ */
#if 1
    extern unsigned int  sysDspGetConfig (void);
    regValue = sysDspGetConfig();
    printf("    DSP FREQ: %u MHz\n", regValue);
#else
	regValue = *(unsigned int *)DSP_FREQ_REG;
	switch(regValue)
	{
	case 0x00014032:
		printf("DSP FREQ: 208.33 MHz\n");
		break;
	case 0x0001203c:
		printf("DSP FREQ: 500 MHz\n");
		break;
	case 0x00012048:
		printf("DSP FREQ: 600 MHz\n");
		break;
	case 0x0001204e:
		printf("DSP FREQ: 650 MHz\n");
		break;
	case 0x00012054:
		printf("DSP FREQ: 700 MHz\n");
		break;
	case 0x0001205a:
		printf("DSP FREQ: 750 MHz\n");
		break;
	case 0x00012060:
		printf("DSP FREQ: 800 MHz\n");
		break;
	case 0x0001904c:
		printf("DSP FREQ: 844.44 MHz\n");
		break;
	case 0x00011036:
		printf("DSP FREQ: 900 MHz\n");
		break;
	case 0x00019056:
		printf("DSP FREQ: 955.56 MHz\n");
		break;
	case 0x0001103c:
		printf("DSP FREQ: 1000 MHz\n");
		break;
	case 0x00011040:
		printf("DSP FREQ: 1066.67 MHz\n");
		break;
	case 0x00011042:
		printf("DSP FREQ: 1100 MHz\n");
		break;
	case 0x00011046:
		printf("DSP FREQ: 1166.67 MHz\n");
		break;
	case 0x00011048:
		printf("DSP FREQ: 1199.88 MHz\n");
		break;
	default:
		printf("DSP FREQ: 非法值！\n");
		break;
	}
#endif
	
	/* ACE FREQ */
	regValue = *(unsigned int *)ACE_FREQ_REG;
	switch(regValue)
	{
	case 0x00014032:
		printf("ACE FREQ: 208.33 MHz\n");
		break;
	case 0x00014060:
		printf("ACE FREQ: 400 MHz\n");
		break;
	case 0x0001203c:
		printf("ACE FREQ: 500 MHz\n");
		break;
	case 0x00012042:
		printf("ACE FREQ: 550 MHz\n");
		break;
	case 0x00012048:
		printf("ACE FREQ: 600 MHz\n");
		break;
	case 0x0001204e:
		printf("ACE FREQ: 650 MHz\n");
		break;
	case 0x00012054:
		printf("ACE FREQ: 700 MHz\n");
		break;
	default:
		printf("ACE FREQ: 非法值！ 0x%x\n",regValue);
		break;
	}
	/* DDR FREQ */
	regValue = *(unsigned int *)DDR_FREQ_REG;
	switch(regValue)
	{
	case 0x00024060:
		printf("DDR FREQ: 800 MHz\n");
		break;
	case 0x00014040:
		printf("DDR FREQ: 1066 MHz\n");
		break;
	case 0x00014048:
		printf("DDR FREQ: 1200 MHz\n");
		break;
	case 0x00014050:
		printf("DDR FREQ: 1333 MHz\n");
		break;
	case 0x00014060:
		printf("DDR FREQ: 1600 MHz\n");
		break;
	case 0x00012038:
		printf("DDR FREQ: 1867 MHz\n");
		break;
	case 0x00012040:
		printf("DDR FREQ: 2133 MHz\n");
		break;
	default:
		printf("DDR FREQ: 非法值！\n");
		break;
	}
	
	regValue = *(unsigned int *)AXI_FREQ_REG;
	switch(regValue)
	{
	case 0x00014032:
		printf("AXI FREQ: 208.33 MHz\n");
		break;
	case 0x00014060:
		printf("AXI FREQ: 400 MHz\n");
		break;
	case 0x0001a05c:
		printf("AXI FREQ: 511 MHz\n");
		break;
	case 0x00012044:
		printf("AXI FREQ: 566 MHz\n");
		break;
	case 0x00012048:
		printf("AXI FREQ: 600 MHz\n");
		break;
	case 0x0001204c:
		printf("AXI FREQ: 633 MHz\n");
		break;
	case 0x0001a07c:
		printf("AXI FREQ: 688 MHz\n");
		break;
	default:
		printf("AXI FREQ: 非法值！\n");
		break;
	}

	return OK;
}


