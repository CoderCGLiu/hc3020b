#ifndef _HR2_MACRO_H
#define _HR2_MACRO_H
/*******************************************************************************
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件是从中电十四所bsp包中移植过来
 * 修改：
 */

#ifndef BOOL
typedef int BOOL;
#endif

#ifndef TRUE
#define TRUE		(1)
#endif

#ifndef FALSE
#define FALSE		(0)
#endif

/*#ifndef bool
typedef int bool;
#endif
*/
#ifndef true
#define true		(1)
#endif

#ifndef false
#define false		(0)
#endif

//#ifndef u64
//typedef unsigned long long		u64;
//#endif

#ifndef u32
typedef unsigned int		u32;
#endif

#ifndef u16
typedef unsigned short		u16;
#endif

#ifndef u8
typedef unsigned char		u8;
#endif

#ifndef uint
typedef unsigned int uint;
#endif

#ifndef ulong
typedef unsigned long ulong;
#endif

#ifndef uchar
typedef unsigned char uchar;
#endif

#ifndef ushort
typedef unsigned short ushort;
#endif

#ifndef DBPRINT
#define DBPRINT printf
#endif

#define IMPORT extern

#ifndef NULL
#define NULL 0
#endif

union datacast_64 {
	unsigned long long l64;
	struct 
	{
		unsigned int l;
		unsigned int h;
	} l32;
};


//#define HR2_REG64_WRITE32(reg,val)		*((volatile u32 * )(ADDR2_LP64(reg))) = val
//#define HR2_REG64_READ32(reg)        		(*(volatile u32 * )(ADDR2_LP64(reg)))
//#define HR2_REG64_WRITE8(reg, val)		*((volatile u8 * )(ADDR2_LP64(reg))) = val
//#define HR2_REG64_READ8(reg)        		*(volatile u8 * )(ADDR2_LP64(reg))


#if 1 /*def REWORKS_LP64*/

#define HR2_REG_WRITE32(reg,val)		*((volatile u32 * )(reg)) = val
#define HR2_REG_READ32(reg)        		(*(volatile u32 * )(reg))
#define HR2_REG_WRITE8(reg, val)		*((volatile u8 * )(ADDR2_LP64(reg))) = val
#define HR2_REG_READ8(reg)        		*(volatile u8 * )(ADDR2_LP64(reg))

#else /*REWORKS_LP64*/

#define HR2_REG_WRITE32(reg, val)		*((volatile u32 * )(reg)) = val
#define HR2_REG_READ32(reg)        		(*(volatile u32 * )(reg))

#define HR2_REG_WRITE8(reg, val)		*((volatile u8 * )(reg)) = val
#define HR2_REG_READ8(reg)        		*(volatile u8 * )(reg)

#endif /*REWORKS_LP64*/


#define SYS_CTRL					0xffffffffbf088000

#define REG_SYS_CONFIG1				(SYS_CTRL + 0x08)
#define SYSCFG1_I2C0_RST			(0x1 << 0)
#define SYSCFG1_I2C1_RST			(0x1 << 1)

#define REG_SYS_CONFIG2				(SYS_CTRL + 0x0c)
#define SYSCFG2_NN_MODE				(0x3 << 0)
#define SYSCFG2_I2C0_MODE			(0x1 << 2) //0 -> MST, 1 -> SLV
#define SYSCFG2_I2C1_MODE			(0x1 << 3)
#define SYSCFG2_CAN0_MODE			(0x1 << 8)
#define SYSCFG2_CAN1_MODE			(0x1 << 12)
#define SYSCFG1_SPI0_RST			(0x1 << 4)
#define SYSCFG1_SPI1_RST			(0x1 << 5)
#define REG_SYS_GMAC0_CFG			(SYS_CTRL + 0x1c)
#define REG_SYS_GMAC1_CFG			(SYS_CTRL + 0x20)
#define REG_SYS_GMAC2_CFG			(SYS_CTRL + 0x24)
#define BIT_GMAC_WR_ADDR_HI			(0xff << 24)
#define BIT_GMAC_RD_ADDR_HI			(0xff << 16)

#endif

