/* hwconf.c - Hardware configuration support module */

/*
 * Copyright (c) 2006-2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01t,10sep08,slk  don't allocate 4 MIPS int controllers if only 3 CPUs
                 configured
01s,29aug08,kab  Change _WRS_VX_SMP to _WRS_CONFIG_SMP
01r,01may08,slk  add ipi support on all ipi input pins
01q,31oct07,slk  add legacy interrupt support example
01p,27sep07,bwa  fixed WIND105938 (redefinition of vxTas)
01o,02jul07,bwa  updated dshmSm.h path
01n,22jun07,bwa  added support for concurrent SM and DSHM.
01m,22jun07,bwa  added DSHM devices.
01l,18jun07,slk  add shared memory device and move the sb1FfsTbl table
                 back to sysLib.c
01k,07jun07,h_k  added "bcmFamily" parameter.
01j,24may07,slk  fix UART B regBase defines based on sb1250 or sb1480 BSP
01i,03may07,slk  clean up warnings
01h,27apr07,slk  Move MIPS_CPU_NUM_INTERRUPT_INPUTS to config.h
01g,04apr07,slk  add MIPS interrupt controllers
01f,03apr07,slk  Removed #include <drv/multi/sb1Lib.h>
                 This was causing many redefinitions
		 and preventing the kernel from booting
01e,05mar07,ami  Timer Device Resources Added
01d,05feb07,wap  Add 4th sbe ethernet port
01c,14nov06,pmr  serial on second core
01b,21sep06,pmr  add support for DRV_SIO_SB1
01a,28jun06,wap  written
*/

#include <vxWorks.h>
//#include <vsbConfig.h>
#include <vxBusLib.h>
#include <hwif/vxbus/vxBus.h>
#include <hwif/vxbus/hwConf.h>
#include <hwif/vxbus/vxbIntrCtlr.h>
#include <hwif/util/vxbParamSys.h>
#include <hwif/util/hwMemLib.h>


#include "config.h"
#include "malta.h" /* added by yinwx, 20100106 */

#include <vxbus/sb1Lib.h>

#ifdef DRV_INTCTLR_I8259
#include <hwif/intCtlr/vxbI8259IntCtlr.h>
#endif

#ifdef INCLUDE_DRV_STORAGE_INTEL_AHCI
#include "vxbIntelAhciStorage.h"
#endif

#ifdef INCLUDE_DRV_STORAGE_INTEL_AHCI
IMPORT void usrAhciInit (void);

IMPORT struct ahciLocation ahciLocation[AHCI_MAX_CTRLS];
IMPORT AHCI_CTRL ahciCtrl[AHCI_MAX_CTRLS];
#endif


//ww mod 20180919 mod 默认开启驱动中所有功能
#if 0
#define DRV_CDMA
#define DRV_TIMER0
#define DRV_TIMER1
#define DRV_GDMA
#else

#if defined(__HUARUI2__)
#define DRV_CDMA
#define DRV_GDMA
#define HGMAC_DEV0
#define HGMAC_DEV1
#define HGMAC_DEV2 
#define DRV_TIMER0
#define DRV_TIMER1
#define DRV_I2C
#define DRV_RIOC0
#define DRV_RIOC1
#define DRV_PCIE
#define INCLUDE_USERMEM

#elif defined(__HUARUI3__)

//#define INCLUDE_USERMEM
//#define INCLUDE_TFFS
//#define HGMAC_DEV0
//#define DRV_CDMA
//#define DRV_GDMA
//#define DRV_TIMER0
//#define DRV_TIMER1
//#define DRV_LPC
//#define DRV_GPIO1
//#define DRV_RIOC0
//#define DRV_RIOC1

#endif /*HUARUI2_SMP_64*/



#endif
//ww mod 20180919 mod

/* added by yinwx, for Ls3A frequecy, 20100518 */
#define LS3A_FREQ /*(66000000*8)*/  (400000000*2)
IMPORT unsigned int usbMemToPci(void* pMem);
IMPORT void* usbPciToMem(unsigned int pciAdrs);
#ifdef DRV_INTCTLR_I8259

const struct intrCtlrInputs i8259Inputs0[] =
    {
    /* pin, driver, unit, index */
	#if 0 /*test*/
    {0, "i8253TimerDev", 0, 0},
    #endif
    #if defined(INCLUDE_PC_CONSOLE) || defined (INCLUDE_WINDML)
    {1, "i8042Kbd", 0, 0},
    #endif
    #if 0
    {2, "i8259Pic", 1, 0}, 
    {3, "ns16550", 2, 0},
    {4, "ns16550", 1, 0},
    {5, "reserved", 0, 0},
    {6, "floppyDev", 0, 0},
    {7, "parallelPort", 0, 0}
    #else
       {2, "i8259Pic", 1, 0}, 
#ifdef INCLUDE_RTL8169_VXB_END
	{ 3,"rtg", 0, 0}, 
#endif
#ifdef INCLUDE_GEI825XX_VXB_END
	{ 3,"gei", 0, 0},
	{ 6,"gei", 1, 0},
#endif
    #ifdef INCLUDE_EHCI
       {6,"vxbPciUsbEhci",0,0},
	 {6,"vxbPciUsbEhci",1,0},
    #endif
    
    #ifdef INCLUDE_OHCI
    	{6,"vxbPciUsbOhci",0,0},
			
	{6,"vxbPciUsbOhci",1,0},
		
	{6,"vxbPciUsbOhci",2,0},
		
	{6,"vxbPciUsbOhci",3,0},
		
	{6,"vxbPciUsbOhci",4,0},
    #endif
    #endif
};

/* malta interrupt controller 0 input pin to output pin assignments */

const struct intrCtlrXBar mipsMaltaIntCtlrXBar0[] =
    {
    { 0, 0 },
    { 1, 0 },
    { 2, 0 },
    { 3, 0 },
    { 4, 0 },
    { 5, 0 },
    { 6, 0 },
    { 7, 0 },
    };

const struct hcfResource i8259DevResources0[] = {
    { "regBase", HCF_RES_INT, {(void *)(0xb8000020)}},
    { "input", HCF_RES_ADDR, {(void *)&i8259Inputs0[0]}},
    { "inputTableSize", HCF_RES_INT, {(void *)NELEMENTS(i8259Inputs0)}},
    /*{ "crossBar", HCF_RES_ADDR, {(void *)&mipsMaltaIntCtlrXBar0[0]} },
    { "crossBarTableSize", HCF_RES_INT, \
       {(void *)NELEMENTS(mipsMaltaIntCtlrXBar0)}},*/
    { "regInterval",HCF_RES_INT, {(void *)1}},
    { "icw1", HCF_RES_INT, {(void *)(I8259_PIC_ICW1 | I8259_PIC_ICW4_NEEDED)}},
    { "icw2", HCF_RES_INT, {(void *)0}},
    { "icw3", HCF_RES_INT, {(void *) 0x4}},
    { "icw4", HCF_RES_INT, {(void *)(I8259_I8259_PIC_8086_MODE| I8259_PIC_AUTO_EOI)}},
    { "ocw1", HCF_RES_INT, {(void *)0xfb}},
    { "icw3Init", HCF_RES_INT, {(void *)TRUE}},
      { "nonSpecificEOIFunc", HCF_RES_ADDR, {(void *)0}},
    { "specificEOIFlag", HCF_RES_INT, {(void *)0}},  /*tyt*/
};

#define i8259DevNum0 NELEMENTS(i8259DevResources0)


const struct intrCtlrInputs i8259Inputs1[] =
    {
    /* pin, driver, unit, index */
#if 0
    {0, "rtc", 0, 0},
    {1, "smBus", 0, 0},
    {2, "legacy", 0, 0},      /* int AB */
    {3, "vxbPciUsbOhci", 0, 0},      /* int CD */
    {3, "vxbPciUsbEhci", 0, 0},      /* int CD */
#if 0
    {4, "i8042Mse", 0, 0},
#endif
    {5, "reserved", 0, 0},
#endif
#if 0
    {6, "legacy", 0,344},        /* primaryIDE */
#endif
#if defined(INCLUDE_WINDML)
    {4, "i8042Mse", 0, 0},
#endif
    {7, "legacy", 0, 348},        /* secondaryIDE */

};

/* malta interrupt controller 1 input pin to output pin assignments */

const struct intrCtlrXBar mipsMaltaIntCtlrXBar1[] =
    {
    { 0, 0 },
    { 1, 0 },
    { 2, 0 },
    { 3, 0 },
    { 4, 0 },
    { 5, 0 },
    { 6, 0 },
    { 7, 0 },
    };

const struct hcfResource i8259DevResources1[] = {
    { "regBase", HCF_RES_INT, {(void *)(0xb80000a0 /*PIC2_BASE_ADR*/ )}},
    { "input", HCF_RES_ADDR, {(void *)&i8259Inputs1[0]}},
    { "inputTableSize", HCF_RES_INT, {(void *)NELEMENTS(i8259Inputs1)}},
  /*  { "crossBar", HCF_RES_ADDR, {(void *)&mipsMaltaIntCtlrXBar1[0]} },
    { "crossBarTableSize", HCF_RES_INT, \
       {(void *)NELEMENTS(mipsMaltaIntCtlrXBar1)}},*/
    { "regInterval",HCF_RES_INT, {(void *)1}},
    { "icw1", HCF_RES_INT, {(void *)(I8259_PIC_ICW1 | I8259_PIC_ICW4_NEEDED)}},
    { "icw2", HCF_RES_INT, {(void *)(8)}},
    { "icw3", HCF_RES_INT, {(void *) 0x02}},
    { "icw4", HCF_RES_INT, {(void *)(I8259_I8259_PIC_8086_MODE| I8259_PIC_AUTO_EOI)}},
    { "ocw1", HCF_RES_INT, {(void *)0xff}},
    { "icw3Init", HCF_RES_INT, {(void *)TRUE}},
    { "nonSpecificEOIFunc", HCF_RES_ADDR, {(void *)0}},
    { "specificEOIFlag", HCF_RES_INT, {(void *)0}}, /*tyt*/
};

#define i8259DevNum1 NELEMENTS(i8259DevResources1)
#endif /* DRV_INTCTLR_I8259 */
#ifdef INCLUDE_SBE0
LOCAL const struct hcfResource sbeVxbEnd0Resources[] = {
    { "regBase", HCF_RES_INT, { (void *)(PHYS_TO_K1(A_MAC_BASE_0)) } },
};
#define sbeVxbEnd0Num NELEMENTS(sbeVxbEnd0Resources)
#endif

#ifdef INCLUDE_SBE1
LOCAL const struct hcfResource sbeVxbEnd1Resources[] = {
    { "regBase", HCF_RES_INT, { (void *)(PHYS_TO_K1(A_MAC_BASE_1)) } },
};
#define sbeVxbEnd1Num NELEMENTS(sbeVxbEnd1Resources)
#endif

#ifdef INCLUDE_SBE2
LOCAL const struct hcfResource sbeVxbEnd2Resources[] = {
    { "regBase", HCF_RES_INT, { (void *)(PHYS_TO_K1(A_MAC_BASE_2)) } },
};
#define sbeVxbEnd2Num NELEMENTS(sbeVxbEnd2Resources)
#endif

#ifdef INCLUDE_SBE3
LOCAL const struct hcfResource sbeVxbEnd3Resources[] = {
    { "regBase", HCF_RES_INT, { (void *)(PHYS_TO_K1(A_MAC_BASE_3)) } },
};
#define sbeVxbEnd3Num NELEMENTS(sbeVxbEnd3Resources)
#endif

#ifdef	DRV_SIO_NS16550 /*zxj*/
struct hcfResource ns16550Dev0Resources[] = {
    { "regBase",     HCF_RES_INT, {(void *)0xbfe001e0} },
    { "clkFreq",     HCF_RES_INT, {(void *)33000000} },
    { "regInterval", HCF_RES_INT, {(void *)1} },
};
#define ns16550Dev0Num NELEMENTS(ns16550Dev0Resources)
#endif	/* DRV_SIO_NS16550 */


#ifdef INCLUDE_DRV_STORAGE_INTEL_AHCI
struct hcfResource AHCIDevResources[] = {
    { "ahciLocation",     HCF_RES_ADDR, {(void *)ahciLocation} },
    { "ahciCtrl",     HCF_RES_ADDR, {(void *)ahciCtrl} },
    { "usrAhciInit", HCF_RES_ADDR, {(void *)usrAhciInit} },
};
#define AHCIDevNum NELEMENTS(AHCIDevResources)
#endif




#ifdef	INCLUDE_GT64120A_PCI /* modified by yinwx, 20100121 */
const struct hcfResource g64120aPci0Resource[] = {
    { "regBase", HCF_RES_INT, { (void *)PHYS_TO_K1(MALTA_CORECTRL_BASE) } },
	/* prefetch mem */
    { "mem32Addr", HCF_RES_ADDR, { (void *)NULL } },
    { "mem32Size", HCF_RES_INT, { (void *)0 } },
    /* conventional mem */
    { "memIo32Addr", HCF_RES_ADDR, { (void *)LOONGSON_PCIMEM0_BASE /*MALTA_PCIMEM0_BASE*/ /* changed!! */ } },
    { "memIo32Size", HCF_RES_INT, { (void *)LOONGSON_PCIMEM0_SIZE  /*MALTA_PCIMEM0_SIZE*/ } },
    /* prefetch io */
    { "io32Addr", HCF_RES_ADDR, { (void *)NULL/*(MALTA_PCIIO0_32BIT_OFFSET)*/ } },
    { "io32Size", HCF_RES_INT, { (void *)0/*MALTA_PCIIO0_32BIT_SIZE*/ } }, 
    /* conventional io */
    { "io16Addr", HCF_RES_ADDR, { (void *)LOONGSON_PCIIO16_BASE/*(MALTA_PCIIO0_16BIT_OFFSET)*/ } },
    { "io16Size", HCF_RES_INT, { (void *)LOONGSON_PCIIO16_SIZE/*MALTA_PCIIO0_16BIT_SIZE*/ } }, 
    { "fbbEnable", HCF_RES_INT, { (void *)FALSE } },
    { "cacheSize", HCF_RES_INT, { (void *)4 } },
    { "maxLatAllSet", HCF_RES_INT, { (void *)PCI_LAT_TIMER } },
    { "autoIntRouteSet", HCF_RES_INT, { (void *)TRUE } }, 
    #if 0
    { "includeFuncSet", HCF_RES_ADDR, { (void *)sysPciAutoConfigInclude } },
    
    { "intAssignFuncSet", HCF_RES_ADDR, { (void *)sysPciAutoConfigIntAsgn } },
    #endif
	/* only used in Bridge conf of g64120, but not used in loongson PCI ctlr */
    { "mem0Start", HCF_RES_ADDR, { (void *)MALTA_PCIMEM0_BASE } },
    { "mem0Size", HCF_RES_INT, { (void *)MALTA_PCIMEM0_SIZE } },
    { "mem1Start", HCF_RES_ADDR, { (void *)0 } },
    { "mem1Size", HCF_RES_INT, { (void *)0 } },
    /* used for convert func */
    { "ioStart", HCF_RES_ADDR, { (void *)LOONGSON_PCIIO0_BASE /* MALTA_PCIIO0_BASE*/ } },
    /* only used in Bridge conf of g64120, but not used in loongson PCI ctlr */
    { "ioSize", HCF_RES_INT, { (void *)MALTA_PCIIO0_SIZE } },
    { "ioRemap", HCF_RES_INT, { (void *)0 } },
    { "pciConfigMechanism",HCF_RES_INT, { (void *)(0)}},
    { "maxBusSet", HCF_RES_INT, { (void *)32 } },
#ifdef INCLUDE_USB
    { "cpuToBus", HCF_RES_ADDR, {(void *) usbMemToPci}},
    { "busToCpu", HCF_RES_ADDR, {(void *) usbPciToMem}},
#endif
    { "autoConfig",HCF_RES_INT, { (void *)(TRUE)}}
};
#define g64120aPci0Num NELEMENTS(g64120aPci0Resource)
#endif	/* INCLUDE_GT64120A_PCI */ /* yinwx 20100105 */


struct hcfResource r4KTimerDevResources[] =  {
    {"regBase", HCF_RES_INT, {(void *)0} },
    {"minClkRate",HCF_RES_INT, {(void *)SYS_CLK_RATE_MIN} },
    {"maxClkRate",HCF_RES_INT, {(void *)SYS_CLK_RATE_MAX} },
    {"cpuClkRate", /*HCF_RES_ADDR zxj copied*/HCF_RES_INT, {(void *) (LS3A_FREQ / 2)/*vxbR4KTimerFrequencyGet*/} }
};
#define r4TimerDevNum NELEMENTS(r4KTimerDevResources)

#ifdef DRV_TIMER_SB1
struct hcfResource sb1TimerDevResources0 [] =  {
#if defined(SB1_CPU_1)
    {"regBase", HCF_RES_INT, {(void *) PHYS_TO_K1(A_SCD_TIMER_BASE(1))} },
#else /* default to timer 0 */
    {"regBase", HCF_RES_INT, {(void *) PHYS_TO_K1(A_SCD_TIMER_BASE(0))} },
#endif
    {"minClkRate",HCF_RES_INT, {(void *)AUX_CLK_RATE_MIN} },
    {"maxClkRate",HCF_RES_INT, {(void *) AUX_CLK_RATE_MAX} },
#ifdef _SIMULATOR_
    {"cpuClkRate", HCF_RES_INT, {(void *) 10000} }
#else
    {"cpuClkRate", HCF_RES_INT, {(void *) 1000000} }
#endif
};
#define sb1TimerDevNum0         NELEMENTS(sb1TimerDevResources0)
#endif /* DRV_TIMER_SB1 */

#if defined (INCLUDE_PC_CONSOLE) || defined (INCLUDE_WINDML)
/* keyboard Controller 8042 */

const struct hcfResource godson3ai8042KbdResources[] =
    {
        { "regBase",     HCF_RES_INT, {(void *)(0xb8000060)} },
        { "regInterval", HCF_RES_INT, {(void *)4} }, 
        /*{ "irqLevel",    HCF_RES_INT, {(void *)1} },*/
		{ "mode",	 HCF_RES_INT, {(void *)1} }
    };
#define godson3ai8042KbdNum NELEMENTS(godson3ai8042KbdResources)

const struct hcfResource godson3ai8042MseResources[] =
    {
        { "regBase",     HCF_RES_INT, {(void *)(0xb8000060)} },
        { "regInterval", HCF_RES_INT, {(void *)4} }, 
        /*{ "irqLevel",    HCF_RES_INT, {(void *)1} },*/
    };
#define godson3ai8042MseNum NELEMENTS(godson3ai8042MseResources)

const struct hcfResource godson3aVgaResources[] =
    {
        { "regBase",     HCF_RES_INT, {(void *) /*CTRL_SEL_REG*/0xb80003d4} },
        { "memBase",     HCF_RES_INT, {(void *) /*CTRL_MEM_BASE*/0x40000000} },
        { "colorMode",   HCF_RES_INT, {(void *) /*COLOR_MODE*/1} },
        /*{ "colorSetting",HCF_RES_INT, {(void *) DEFAULT_ATR} },*/
    };
#define godson3aVgaNum NELEMENTS(godson3aVgaResources)

#endif  /* INCLUDE_PC_CONSOLE || INCLUDE_WINDML */

#ifdef INCLUDE_SM_COMMON
struct hcfResource smDevResources[] =  {
    {"regBase", HCF_RES_INT, {(void *)0} },
};
#define smDevNum NELEMENTS(smDevResources)
#endif /* INCLUDE_SM_COMMON */

/* sibyte interrupt controller input pin to device assignments */

const struct intrCtlrInputs mipsSbIntCtlrInputs[] =
    {
    /* pin, driver, unit, index */

    /* aux. clock input pin */
#ifdef DRV_TIMER_SB1
/* secondary CPU (SB1_CPU_1) defaults to timer 1 */
#if defined(SB1_CPU_1)
    {K_INT_TIMER_1, "sb1TimerDev", 0, 0},
#else /* default primary to timer 0 */
    {K_INT_TIMER_0, "sb1TimerDev", 0, 0},
#endif /* defined(SB1_CPU_1) */
#endif /* DRV_TIMER_SB1 */

    /* end device input pin */
#ifdef INCLUDE_SBE0
    {K_INT_MAC_0, "sbe", 0, 0},
#endif /* INCLUDE_SBE0 */
#ifdef INCLUDE_SBE1
    {K_INT_MAC_1, "sbe", 1, 0},
#endif /* INCLUDE_SBE1 */
#ifdef INCLUDE_SBE2
    {K_INT_MAC_2, "sbe", 2, 0},
#endif /* INCLUDE_SBE2 */
#ifdef INCLUDE_SBE3
    {K_INT_MAC_3, "sbe", 3, 0},
#endif /* INCLUDE_SBE3 */

#ifdef DRV_INTCTLR_I8259
{0x18, "i8259Pic", 0, 0}, 
#endif

#if 0 /*lyj*/

	{ 13, "intelAhciSata" ,0 ,0 },
		

#endif
#ifdef DRV_SIO_NS16550  /*zxj*/
	{10, "ns16550", 0, 0},
#endif
    /* shared memory device input pin */
#ifndef INCLUDE_DSHM
#ifdef INCLUDE_SM_COMMON
#if (BCM_FAMILY == BCM_SB1A)
    {K_INT_MBOX_0_3, "sm", 0, 0},
#elif (BCM_FAMILY == BCM_SB1)
    {K_INT_MBOX_3, "sm", 0, 0}, /* shared memory or IPI */
#endif /* (BCM_FAMILY == BCM_SB1A) */
#endif /* INCLUDE_SM_COMMON */

    /* distributed shared memory device input pin */
#else
#if (BCM_FAMILY == BCM_SB1A)
    {K_INT_MBOX_0_3, "dshmBusCtlrSibyte", 0, 0},
#elif (BCM_FAMILY == BCM_SB1)
    {K_INT_MBOX_3, "dshmBusCtlrSibyte", 0, 0}, /* shared memory or IPI */
#endif /* (BCM_FAMILY == BCM_SB1A) */
#endif /* INCLUDE_DSHM */

    /* mailbox interrupts used for IPIs.  the mailbox 0 input pin is
     * used for CPCs in SMP mode
     */
#if (BCM_FAMILY == BCM_SB1A)
    {56, "ipi", 0, 0},
    {57, "ipi", 0, 1},
    {58, "ipi", 0, 2},
    {59, "ipi", 0, 3},
#elif (BCM_FAMILY == BCM_SB1)
    {K_INT_MBOX_0, "ipi", 0, 0},
    {K_INT_MBOX_1, "ipi", 0, 1},
    {K_INT_MBOX_2, "ipi", 0, 2},
    {K_INT_MBOX_3, "ipi", 0, 3},
#endif /* (BCM_FAMILY == BCM_SB1A) */

    };

/* sibyte interrupt controller input pin to output pin assignments */

const struct intrCtlrXBar mipsSbIntCtlrXBar[] =
    {

    /* aux. clock output pin */
#ifdef DRV_TIMER_SB1
/* secondary CPU (SB1_CPU_1) defaults to timer 1 */
#if defined(SB1_CPU_1)
    { K_INT_TIMER_1, 2 },
#else /* primary CPU defaults to timer 0 */
    { K_INT_TIMER_0, 2 },
#endif /* defined(SB1_CPU_1) */
#endif /* DRV_TIMER_SB1 */

    /* end device output pin */
#ifdef INCLUDE_SBE0
    { K_INT_MAC_0, 3},
#endif /* INCLUDE_SBE0 */
#ifdef INCLUDE_SBE1
    { K_INT_MAC_1, 3},
#endif /* INCLUDE_SBE1 */
#ifdef INCLUDE_SBE2
    { K_INT_MAC_2, 3},
#endif /* INCLUDE_SBE2 */
#ifdef INCLUDE_SBE3
    { K_INT_MAC_3, 3},
#endif /* INCLUDE_SBE3 */

#ifdef DRV_SIO_NS16550 /*zxj*/
	{10, 0},
#endif
#ifdef DRV_INTCTLR_I8259
      {0x18, 1}, 
#endif

    /* mailbox interrupts used for IPIs.  output pin 4 is reserved for
     * CPC IPIs when in SMP mode
     */
#if (BCM_FAMILY == BCM_SB1A)
    {56, 4},
    {57, 4}, /*zxj modified*/
    {58, 4},
    {59, 4},
#elif (BCM_FAMILY == BCM_SB1)
    {K_INT_MBOX_0, 4},
    {K_INT_MBOX_1, 4},
    {K_INT_MBOX_2, 4},
    {K_INT_MBOX_3, 4},
#endif /* (BCM_FAMILY == BCM_SB1A) */

    };

#if defined(_WRS_CONFIG_SMP)  /* only route to multiple CPUs in SMP */

/* sibyte interrupt controller input pin to destination CPU assignments */

const struct intrCtlrCpu mipsSbIntCtlrCpuRoute[] =
    {

    /* aux. clock dest. cpu */
#ifdef DRV_TIMER_SB1
/* secondary CPU (SB1_CPU_1) defaults to timer 1 */
#if defined(SB1_CPU_1)
    { K_INT_TIMER_1, 0 },
#else /* primary CPU defaults to timer 0 */
    { K_INT_TIMER_0, 0 },
#endif /* defined(SB1_CPU_1) */
#endif /* DRV_TIMER_SB1 */

    /* end device dest. cpu */
#ifdef INCLUDE_SBE0
    { K_INT_MAC_0, 0 },
#endif /* INCLUDE_SBE0 */
#ifdef INCLUDE_SBE1
    { K_INT_MAC_1, 0 },
#endif /* INCLUDE_SBE1 */
#ifdef INCLUDE_SBE2
    { K_INT_MAC_2, 0 },
#endif /* INCLUDE_SBE2 */
#ifdef INCLUDE_SBE3
    { K_INT_MAC_3, 0 },
#endif /* INCLUDE_SBE3 */

#ifdef DRV_SIO_NS16550 /*zxj*/
	{10, 0},
#endif

    };
#endif /* defined(_WRS_CONFIG_SMP) */

/* sibyte interrupt controller including all resources defined above */

const struct hcfResource mipsSbIntCtlrResources[] = {
    { "regBase",        HCF_RES_INT,    {(void *)TRUE} },

    { "input",  HCF_RES_ADDR, {(void *)&mipsSbIntCtlrInputs[0]} },
    { "inputTableSize", HCF_RES_INT, {(void *)NELEMENTS(mipsSbIntCtlrInputs)}},
    { "crossBar", HCF_RES_ADDR, {(void *)&mipsSbIntCtlrXBar[0]} },
    { "crossBarTableSize", HCF_RES_INT, {(void *)NELEMENTS(mipsSbIntCtlrXBar)}},
    { "bcmFamily", HCF_RES_INT, {(void *)BCM_FAMILY} },

#if defined(_WRS_CONFIG_SMP)  /* only route to multiple CPUs in SMP */
    { "cpuRoute", HCF_RES_ADDR, {(void *)&mipsSbIntCtlrCpuRoute[0]} },
    { "cpuRouteTableSize", HCF_RES_INT, {(void *)NELEMENTS(mipsSbIntCtlrCpuRoute)} }
#endif /* defined(_WRS_CONFIG_SMP) */

};
#define mipsSbIntCtlrNum NELEMENTS(mipsSbIntCtlrResources)

/* CPU 0 interrupt controller input pin to device assignments
 * pin 7 has an example entry added for support of legacy
 * API calls intConnect and intDisconnect.  the first three fields
 * of the entry need to be the input pin number followed by the 
 * device name "legacy" with a unit number of 0.  the final field
 * specifies the index (old style vector) for the ISR and this number
 * needs to match the vector specified in the intConnect/intDisconnect
 * call.
 */

const struct intrCtlrInputs mipsIntCtlr0Inputs[] =
    {
    /* pin, driver, unit, index */

    /*  interrupts inputs into cpu */
    {0, "swtrap", 0, 0},
    {1, "swtrap", 1, 0},
    {2, "mipsSbIntCtlr", 0, 0},
    {3, "mipsSbIntCtlr", 0, 1},
    {4, "mipsSbIntCtlr", 0, 2},
    {5, "mipsSbIntCtlr", 0, 3},
    {6, "mipsSbIntCtlr", 0, 4},

    /* MIPS cpu decrement counter */
    {7, "r4KTimerDev", 0, 0},
    {7, "legacy", 0, 7},

    };
const struct hcfResource cpu0Resources[] = {
    { "regBase",       HCF_RES_INT,    {(void *)TRUE} },

    { "input",  HCF_RES_ADDR,  {(void *)&mipsIntCtlr0Inputs[0]} },
    { "inputTableSize", HCF_RES_INT, {(void *)NELEMENTS(mipsIntCtlr0Inputs)}},

};
#define cpu0Num NELEMENTS(cpu0Resources)

#if (VX_SMP_NUM_CPUS > 1) && defined(_WRS_CONFIG_SMP)

/* CPU 1 interrupt controller input pin to device assignments  */

const struct intrCtlrInputs mipsIntCtlr1Inputs[] =
    {
    /* pin, driver, unit, index */

    /*  interrupts inputs into cpu */
    {0, "swtrap", 0, 1},
    {1, "swtrap", 1, 1},
    {2, "mipsSbIntCtlr", 0, 8},
    {3, "mipsSbIntCtlr", 0, 9},
    {4, "mipsSbIntCtlr", 0, 10},
    {5, "mipsSbIntCtlr", 0, 11},
    {6, "mipsSbIntCtlr", 0, 12},
    {7, "r4KTimerDev", 1, 0},

    };
const struct hcfResource cpu1Resources[] = {
    { "regBase",       HCF_RES_INT,    {(void *)TRUE} },

    { "input",  HCF_RES_ADDR,  {(void *)&mipsIntCtlr1Inputs[0]} },
    { "inputTableSize", HCF_RES_INT, {(void *)NELEMENTS(mipsIntCtlr1Inputs)}},

};
#define cpu1Num NELEMENTS(cpu1Resources)

#if (VX_SMP_NUM_CPUS > 2)

/* CPU 2 interrupt controller input pin to device assignments  */

const struct intrCtlrInputs mipsIntCtlr2Inputs[] =
    {
    /* pin, driver, unit, index */

    /*  interrupts inputs into cpu */
    {0, "swtrap", 0, 2},
    {1, "swtrap", 1, 2},
    {2, "mipsSbIntCtlr", 0, 16},
    {3, "mipsSbIntCtlr", 0, 17},
    {4, "mipsSbIntCtlr", 0, 18},
    {5, "mipsSbIntCtlr", 0, 19},
    {6, "mipsSbIntCtlr", 0, 20},
    {7, "r4KTimerDev", 2, 0},

    };
const struct hcfResource cpu2Resources[] = {
    { "regBase",       HCF_RES_INT,    {(void *)TRUE} },

    { "input",  HCF_RES_ADDR, {(void *)&mipsIntCtlr2Inputs[0]} },
    { "inputTableSize", HCF_RES_INT, {(void *)NELEMENTS(mipsIntCtlr2Inputs)}},

};
#define cpu2Num NELEMENTS(cpu2Resources)

#if (VX_SMP_NUM_CPUS > 3)

/* CPU 3 interrupt controller input pin to device assignments  */

const struct intrCtlrInputs mipsIntCtlr3Inputs[] =
    {
    /* pin, driver, unit, index */

    /*  interrupts inputs into cpu */
    {0, "swtrap", 0, 3},
    {1, "swtrap", 1, 3},
    {2, "mipsSbIntCtlr", 0, 24},
    {3, "mipsSbIntCtlr", 0, 25},
    {4, "mipsSbIntCtlr", 0, 26},
    {5, "mipsSbIntCtlr", 0, 27},
    {6, "mipsSbIntCtlr", 0, 28},
    {7, "r4KTimerDev", 3, 0},

    };
const struct hcfResource cpu3Resources[] = {
    { "regBase",       HCF_RES_INT,    {(void *)TRUE} },

    { "input",  HCF_RES_ADDR, {(void *)&mipsIntCtlr3Inputs[0]} },
    { "inputTableSize", HCF_RES_INT, {(void *)NELEMENTS(mipsIntCtlr3Inputs)}},

};
#define cpu3Num NELEMENTS(cpu3Resources)

#endif /* (VX_SMP_NUM_CPUS > 3) */
#endif /* (VX_SMP_NUM_CPUS > 2) */
#endif /* defined(_WRS_CONFIG_SMP) && (VX_SMP_NUM_CPUS > 1) */

/* Distributed SHared Memory support
 */
#ifdef INCLUDE_DSHM
#include <dshm/util/dshmSm.h>

void * dshmRegBaseDummy [10];   /* needed to appease vxbus */

#ifdef INCLUDE_SM_COMMON
    /* if SM is included, push DSHM after it */
    #include <smLib.h>
    #define DSHM_ANCHOR_OFFSET  (ROUND_UP (sizeof (SM_ANCHOR), \
                                    _CACHE_ALIGN_SIZE))
#else
    extern BOOL vxTas (void *);
    #define DSHM_ANCHOR_OFFSET  0
#endif

#define DSHM_SIBYTE_N_CORES 2
#define DSHM_SM_POOL_SIZE   (0x80000)
#define DSHM_ANCHOR_BASE    (0x80000600 + DSHM_ANCHOR_OFFSET)
#define DSHM_ANCHOR_CORE(core) \
    (DSHM_ANCHOR_BASE + \
        core * ROUND_UP (DSHM_ANCHOR_SIZE(DSHM_SIBYTE_N_CORES), \
                                    _CACHE_ALIGN_SIZE))

/* add entries here if more cores are supported */
#if ((defined SB1_CPU_0) || (defined SB_CPU_0))
    #define DSHM_ANCHOR_LOCAL DSHM_ANCHOR_CORE(0)
#elif ((defined SB1_CPU_1) || (defined SB_CPU_1))
    #define DSHM_ANCHOR_LOCAL DSHM_ANCHOR_CORE(1)
#endif

const struct hcfResource dshmBusCtlrSibyteRes[] = {
    { "regBase",    RES_INT,  (void *) dshmRegBaseDummy },
    { "bus_type",   RES_ADDR, (void *) "plb" },
    { "anchor",     RES_ADDR, (void *) DSHM_ANCHOR_LOCAL },
    { "smStart",    RES_ADDR, (void *) -1 },
    { "szPool",     RES_INT,  (void *) DSHM_BUS_PLB_POOLSIZE },
    { "node",       RES_INT,  (void *) DSHM_BUS_PLB_NODE },
    { "rmw",        RES_ADDR, (void *) vxTas },
    { "nRetries",   RES_INT,  (void *) DSHM_BUS_PLB_NRETRIES },
    { "nNodesMax",  RES_INT,  (void *) DSHM_BUS_PLB_MAXNODES },
    { "nEntries",   RES_INT,  (void *) DSHM_BUS_PLB_NENTRIES },
    { "szEntry",    RES_INT,  (void *) DSHM_BUS_PLB_ENTRY_SIZE },
};

/* add entries here if more cores are supported */
const struct hcfResource dshmPeerVxSibyte0Res[] = {
    { "regBase",    RES_INT,  (void *)dshmRegBaseDummy },
    { "node",       RES_INT,  (void *)0 },
    { "anchor",     RES_ADDR, (void *)DSHM_ANCHOR_CORE(0) },
};

const struct hcfResource dshmPeerVxSibyte1Res[] = {
    { "regBase",    RES_INT,  (void *)dshmRegBaseDummy },
    { "node",       RES_INT,  (void *)1 },
    { "anchor",     RES_ADDR, (void *)DSHM_ANCHOR_CORE(1) },
};
#endif  /* INCLUDE_DSHM */


#ifdef DRV_RIOC0
#define HRRIOC0_VXBNAME "hrRiodevc0"
#define HRRIOC1_VXBNAME "hrRiodevc1"
struct hcfResource hrRIOC0Resources[] = {	
	{ "regBase",HCF_RES_INT,{(void *)(0)}},
};
#define hrRIOC0Num NELEMENTS(hrRIOC0Resources)
#endif /*DRV_RIOC0*/

#ifdef DRV_RIOC1
struct hcfResource hrRIOC1Resources[] = {	
	{ "regBase",HCF_RES_INT,{(void *)(0)}},
};
#define hrRIOC1Num NELEMENTS(hrRIOC1Resources)
#endif /*DRV_RIOC1*/

#ifdef DRV_I2C
#define HRI2CM0_VXBNAME "hri2cdevm0"
#define HRI2CM1_VXBNAME "hri2cdevm1"
#define HRI2CS0_VXBNAME "hri2cdevs0"
#define HRI2CS1_VXBNAME "hri2cdevs1"

struct hcfResource hrI2CM0Resources[] = {	
	{ "regBase",HCF_RES_INT,{(void *)(0)}},
};
#define hrI2CM0Num NELEMENTS(hrI2CM0Resources)

struct hcfResource hrI2CM1Resources[] = {	
		{ "regBase",HCF_RES_INT,{(void *)(0)}},
};
#define hrI2CM1Num NELEMENTS(hrI2CM1Resources)

struct hcfResource hrI2CS0Resources[] = {	
		{ "regBase",HCF_RES_INT,{(void *)(0)}},
};
#define hrI2CS0Num NELEMENTS(hrI2CS0Resources)

struct hcfResource hrI2CS1Resources[] = {	
		{ "regBase",HCF_RES_INT,{(void *)(0)}},
};
#define hrI2CS1Num NELEMENTS(hrI2CS1Resources)

#endif /*DRV_I2C*/

  
#ifdef HGMAC_DEV0
//#include "../huarui3/drv/em/vxblltemac.h"
//#define HRGMAC_VXBNAME 		"hr2GMacdev"
//typedef enum {
//	PHY_INTERFACE_MODE_MII,
//	PHY_INTERFACE_MODE_GMII,
//	PHY_INTERFACE_MODE_SGMII,
//	PHY_INTERFACE_MODE_TBI,
//	PHY_INTERFACE_MODE_RMII,
//	PHY_INTERFACE_MODE_RGMII,
//	PHY_INTERFACE_MODE_RGMII_ID,
//	PHY_INTERFACE_MODE_RGMII_RXID,
//	PHY_INTERFACE_MODE_RGMII_TXID,
//	PHY_INTERFACE_MODE_RTBI,
//	PHY_INTERFACE_MODE_XGMII,
//	PHY_INTERFACE_MODE_NONE	/* Must be last */
//} phy_interface_t;

struct hcfResource hrGMAC0Resources[] = {
	{ "deviceId", HCF_RES_INT, { (void *)(0) } },
#ifdef REWORKS_LP64
//	{ "macRegBase", HCF_RES_LONG, { (void *)(0xffffffffbf060000) } },
	{ "macRegBase", HCF_RES_LONG, { (void *)(0xffffffffbf300000) } },
#else
	{ "macRegBase", HCF_RES_INT, { (void *)(0xbf060000) } },
#endif
	//{ "interface", HCF_RES_INT, { (void *)(PHY_INTERFACE_MODE_SGMII) } },
	{ "intr0", HCF_RES_INT, { (void *)(0) } },
	{ "intr0Level", HCF_RES_INT, { (void *)(0) } },
	{ "cacheLineSize", HCF_RES_INT, { (void *)(128) } },
	{ "mtuSize", HCF_RES_INT, { (void *)(1500) } },
	{ "bufferAddr", HCF_RES_INT, { (void *)(0) } },
	{ "bufferSize", HCF_RES_INT, { (void *)(0) } },
	{ "bdAddr", HCF_RES_INT, { (void *)(0) } },
	{ "bdSize", HCF_RES_INT, { (void *)(0) } },
	{ "rxBdCount", HCF_RES_INT, { (void *)(0) } },
	{ "txBdCount", HCF_RES_INT, { (void *)(0) } },
};
#define hrGMAC0Num NELEMENTS(hrGMAC0Resources)
#endif /*#ifdef HGMAC_DEV0*/


//ww mod 20181213 add for gmac2 and gmac3

#ifdef HGMAC_DEV1

struct hcfResource hrGMAC1Resources[] = {
	{ "deviceId", HCF_RES_INT, { (void *)(1) } },
#ifdef REWORKS_LP64
	{ "macRegBase", HCF_RES_LONG, { (void *)(0xffffffffbf068000) } },
#else
	{ "macRegBase", HCF_RES_INT, { (void *)(0xbf068000) } },
#endif
	{ "interface", HCF_RES_INT, { (void *)(PHY_INTERFACE_MODE_SGMII) } },
	{ "intr0", HCF_RES_INT, { (void *)(0) } },
	{ "intr0Level", HCF_RES_INT, { (void *)(0) } },
	{ "cacheLineSize", HCF_RES_INT, { (void *)(128) } },
	{ "mtuSize", HCF_RES_INT, { (void *)(1500) } },
	{ "bufferAddr", HCF_RES_INT, { (void *)(0) } },
	{ "bufferSize", HCF_RES_INT, { (void *)(0) } },
	{ "bdAddr", HCF_RES_INT, { (void *)(0) } },
	{ "bdSize", HCF_RES_INT, { (void *)(0) } },
	{ "rxBdCount", HCF_RES_INT, { (void *)(0) } },
	{ "txBdCount", HCF_RES_INT, { (void *)(0) } },
};
#define hrGMAC1Num NELEMENTS(hrGMAC1Resources)
#endif

#ifdef HGMAC_DEV2
struct hcfResource hrGMAC2Resources[] = {
	{ "deviceId", HCF_RES_INT, { (void *)(2) } },
#ifdef REWORKS_LP64
	{ "macRegBase", HCF_RES_LONG, { (void *)(0xffffffffbf070000) } },
#else
	{ "macRegBase", HCF_RES_INT, { (void *)(0xbf070000) } },
#endif
	{ "interface", HCF_RES_INT, { (void *)(PHY_INTERFACE_MODE_SGMII) } },
	{ "intr0", HCF_RES_INT, { (void *)(0) } },
	{ "intr0Level", HCF_RES_INT, { (void *)(0) } },
	{ "cacheLineSize", HCF_RES_INT, { (void *)(128) } },
	{ "mtuSize", HCF_RES_INT, { (void *)(1500) } },
	{ "bufferAddr", HCF_RES_INT, { (void *)(0) } },
	{ "bufferSize", HCF_RES_INT, { (void *)(0) } },
	{ "bdAddr", HCF_RES_INT, { (void *)(0) } },
	{ "bdSize", HCF_RES_INT, { (void *)(0) } },
	{ "rxBdCount", HCF_RES_INT, { (void *)(0) } },
	{ "txBdCount", HCF_RES_INT, { (void *)(0) } },
};
#define hrGMAC2Num NELEMENTS(hrGMAC2Resources)
#endif


#ifdef DRV_CDMA
#include "drv/cachedma/hr3CacheDma.h"
//#define CDMA_NUM	4
//#define HRCDMA_VXBNAME "hrCdma"
struct hcfResource hrCDMAResources[] = {
#ifdef REWORKS_LP64
	{ "regBase",HCF_RES_LONG,{(void *)(0)}}	
#else
	{ "regBase",HCF_RES_INT,{(void *)(0)}}
#endif
};
#define hrCDmaNum NELEMENTS(hrCDMAResources)
#endif /*DRV_CDMA*/

#ifdef DRV_TIMER0
#define HRTIMER0_VXBNAME "hrTimer0"
struct hcfResource hrTimer0Resources[] = {	
#ifdef REWORKS_LP64
	{ "deviceId", HCF_RES_LONG, { (void *)(0) } },
#else
	{ "deviceId", HCF_RES_INT, { (void *)(0) } },
#endif
};
#define hrTimer0Num NELEMENTS(hrTimer0Resources)
#endif  /*DRV_TIMER0*/

#ifdef DRV_TIMER1
#define HRTIMER1_VXBNAME "hrTimer1"
struct hcfResource hrTimer1Resources[] = {	
#ifdef REWORKS_LP64
	{ "deviceId", HCF_RES_LONG, { (void *)(1) } },
#else
	{ "deviceId", HCF_RES_INT, { (void *)(1) } },
#endif
};
#define hrTimer1Num NELEMENTS(hrTimer1Resources)
#endif /*DRV_TIMER1*/

#ifdef DRV_GDMA
#include "huarui3/drv/generaldma/hr3GeneralDma.h"
#define HRGDMA_VXBNAME "hrGdma"
struct hcfResource hrGdmaResources[] = {	
#ifdef REWORKS_LP64
	{ "regBase",HCF_RES_LONG,{(void *)(0x900000001f900000UL)}},
#else
	{ "regBase",HCF_RES_INT,{(void *)(0xbf000000)}},
#endif
};
#define hrGdmaNum NELEMENTS(hrGdmaResources)
#endif /*DRV_GDMA*/

  
//ww mod 20190218 add
#ifdef DRV_PCIE
#define HRPCIE_VXBNAME "hr2dwcPci"
const struct hcfResource hr2dwcPciRes[] = {
		/* added by syj begin 20140731 */
#if 1
		{ "regBase",	HCF_RES_LONG,	{(void *)0xffffffffbf050000}},
		{ "regBase1",	HCF_RES_LONG,	{(void *)0xffffffffb8000000}},
		
		{ "memIo32Addr",HCF_RES_ADDR,	{(void *)0x18200000}}, 
		//{ "memIo32Addr",HCF_RES_ADDR,	{(void *)0xc0200000}},
		{ "memIo32Size",HCF_RES_INT,	{(void *)0x400000}},

		{ "mem32Addr",	HCF_RES_ADDR,	{(void *)0x18600000}},
		//{ "mem32Addr",	HCF_RES_ADDR,	{(void *)0xc0600000}},
		{ "mem32Size",	HCF_RES_INT,	{(void *)0x1000000}},
#if 0		
		{ "io32Addr",	HCF_RES_ADDR,	{(void *)0x19600000}},
		//{ "io32Addr",	HCF_RES_ADDR,	{(void *)0xc1400000}}, 
		{ "io32Size",	HCF_RES_INT,	{(void *)0x100000}},
#endif
#endif
		{ "maxBusSet",	HCF_RES_INT, 	{(void *)20}},
		{ "autoConfig",	HCF_RES_INT,	{(void*)1}},
		{ "pciExpressHost",	HCF_RES_INT,	{(void*)1}},
#if 0
		{ "ibMemAxi32Addr",	HCF_RES_ADDR,	{(void*)0x10000000}}, /* set value for reading norflash when system boot */
		{ "ibMemPci32Addr",	HCF_RES_ADDR,	{(void*)0x30000000/*0x18200000*/}}, /* PCI REGION ADDR */
		{ "ibMemSize",	HCF_RES_INT,	{(void*)0x100000}},
		{ "ibIoAxi32Addr",	HCF_RES_ADDR,	{(void*)0x20100000}}, 
		{ "ibIoPci32Addr",	HCF_RES_ADDR,	{(void*)0x32000000/*0x18e00000*/}}, /* PCI REGION ADDR */
		{ "ibIoSize",	HCF_RES_INT,	{(void*)0x200000}},
		{ "ibBar0AxiAddr",	HCF_RES_ADDR,	{(void*)0x18300000}}, 
		{ "ibBar4AxiAddr",	HCF_RES_ADDR,	{(void*)0x18500000}},
#endif
};
#define hr2dwcPciNum NELEMENTS(hr2dwcPciRes)
#define HRPCIE_DEVICE_DESC \
	{ HRPCIE_VXBNAME , 0, VXB_BUSID_PLB, 0, hr2dwcPciNum, hr2dwcPciRes}
#endif
//ww mod 20190218 add


#ifdef INCLUDE_USERMEM
#define HRUSERMEM_VXBNAME "hrUserMem"
struct hcfResource hrUserMemResources[] = {
#ifdef REWORKS_LP64
	{ "deviceId", HCF_RES_LONG, { (void *)(0) } },
#else
	{ "deviceId", HCF_RES_INT, { (void *)(0) } },
#endif
};
#define hrUserMemNum NELEMENTS(hrUserMemResources)

#endif /* INCLUDE_USERMEM */

/* device list */

#if 0
const struct hcfDevice hcfDeviceList[] = {

///* sibyte and MIPS cpu interrupt controllers */
//#ifdef DRV_INTCTLR_MIPS
//    { "mipsSbIntCtlr", 0, VXB_BUSID_PLB, 0, mipsSbIntCtlrNum, \
//      mipsSbIntCtlrResources },
//    { "mipsIntCtlr", 0, VXB_BUSID_PLB, 0, cpu0Num, cpu0Resources },
//#if defined(_WRS_CONFIG_SMP) && (VX_SMP_NUM_CPUS > 1)
//    { "mipsIntCtlr", 1, VXB_BUSID_PLB, 0, cpu1Num, cpu1Resources },
//#if (VX_SMP_NUM_CPUS > 2)
//    { "mipsIntCtlr", 2, VXB_BUSID_PLB, 0, cpu2Num, cpu2Resources },
//#if (VX_SMP_NUM_CPUS > 3)
//    { "mipsIntCtlr", 3, VXB_BUSID_PLB, 0, cpu3Num, cpu3Resources },
//#endif /* (VX_SMP_NUM_CPUS > 3) */
//#endif /* (VX_SMP_NUM_CPUS > 2) */
//#endif /* defined(_WRS_CONFIG_SMP) && (VX_SMP_NUM_CPUS > 1) */
//#endif /* DRV_INTCTLR_MIPS */
//
///* end devices */
//#ifdef INCLUDE_SBE0
//    { "sbe", 0, VXB_BUSID_PLB, 0, sbeVxbEnd0Num, sbeVxbEnd0Resources },
//#endif
//#ifdef INCLUDE_SBE1
//    { "sbe", 1, VXB_BUSID_PLB, 0, sbeVxbEnd1Num, sbeVxbEnd1Resources },
//#endif
//#ifdef INCLUDE_SBE2
//    { "sbe", 2, VXB_BUSID_PLB, 0, sbeVxbEnd2Num, sbeVxbEnd2Resources },
//#endif
//#ifdef INCLUDE_SBE3
//    { "sbe", 3, VXB_BUSID_PLB, 0, sbeVxbEnd3Num, sbeVxbEnd3Resources },
//#endif
//
///* uart devices */
//#ifdef	DRV_SIO_NS16550 /*zxj*/
//    { "ns16550", 0, VXB_BUSID_PLB, 0, ns16550Dev0Num, ns16550Dev0Resources },    
//#endif	/* DRV_SIO_NS16550 */
//
//#if defined (INCLUDE_PC_CONSOLE) || defined (INCLUDE_WINDML)
//    { "i8042Kbd", 0, VXB_BUSID_PLB, 0, godson3ai8042KbdNum, godson3ai8042KbdResources },   
//    { "m6845Vga", 0, VXB_BUSID_PLB, 0, godson3aVgaNum, godson3aVgaResources },
//    { "i8042Mse", 0, VXB_BUSID_PLB, 0, godson3ai8042MseNum, godson3ai8042MseResources },   
//#endif /* INCLUDE_PC_CONSOLE || INCLUDE_WINDML */

#ifdef DRV_CDMA	
//	{ HRCDMA_VXBNAME , 0, VXB_BUSID_PLB, 0, hrCDmaNum, hrCDMAResources},
	HRCDMA_DEVICE_DESC,
#endif
	
#ifdef DRV_TIMER0		
	{ HRTIMER0_VXBNAME , 0, VXB_BUSID_PLB, 0, hrTimer0Num, hrTimer0Resources},
#endif
	
#ifdef DRV_TIMER1
	{ HRTIMER1_VXBNAME , 0, VXB_BUSID_PLB, 0, hrTimer1Num, hrTimer1Resources},
#endif
	
#ifdef DRV_I2C
	{ HRI2CM0_VXBNAME , 0, VXB_BUSID_PLB, 0, hrI2CM0Num, hrI2CM0Resources},
	
	{ HRI2CM1_VXBNAME , 0, VXB_BUSID_PLB, 0, hrI2CM1Num, hrI2CM1Resources},

	{ HRI2CS0_VXBNAME , 0, VXB_BUSID_PLB, 0, hrI2CS0Num, hrI2CS0Resources},

	{ HRI2CS1_VXBNAME , 0, VXB_BUSID_PLB, 0, hrI2CS1Num, hrI2CS1Resources},
#endif
	
#ifdef DRV_RIOC0
	{ HRRIOC0_VXBNAME , 0, VXB_BUSID_PLB, 0, hrRIOC0Num, hrRIOC0Resources},	
#endif 
	
#ifdef DRV_RIOC1
	{ HRRIOC1_VXBNAME , 0, VXB_BUSID_PLB, 0, hrRIOC1Num, hrRIOC1Resources},
#endif /* DRV_RIOC1 */

#ifdef HGMAC_DEV0 //HGMAC_DEV
//	{ HRGMAC_VXBNAME , 0, VXB_BUSID_PLB, 0, hrGMAC0Num, hrGMAC0Resources},
	HRGMAC0_DEVICE_DESC,

#endif
	
	
#ifdef HGMAC_DEV1 //HGMAC_DEV
//	{ HRGMAC_VXBNAME , 0, VXB_BUSID_PLB, 0, hrGMAC0Num, hrGMAC0Resources},
	HRGMAC1_DEVICE_DESC,

#endif
	
	
#ifdef HGMAC_DEV2 //HGMAC_DEV
//	{ HRGMAC_VXBNAME , 0, VXB_BUSID_PLB, 0, hrGMAC0Num, hrGMAC0Resources},
	HRGMAC2_DEVICE_DESC,
#endif
	 
	
#ifdef DRV_GDMA		
//	{ HRGDMA_VXBNAME , 0, VXB_BUSID_PLB, 0, hrGdmaNum, hrGdmaResources},
	HRGDMA_DEVICE_DESC,
#endif
	
#ifdef DRV_PCIE	
//	{ HRGDMA_VXBNAME , 0, VXB_BUSID_PLB, 0, hrGdmaNum, hrGdmaResources},
	HRPCIE_DEVICE_DESC,
#endif
	
	
#ifdef INCLUDE_USERMEM	
	{ HRUSERMEM_VXBNAME , 0, VXB_BUSID_PLB, 0, hrUserMemNum, hrUserMemResources},
#endif /* INCLUDE_USERMEM */

#ifdef	INCLUDE_GT64120A_PCI
//    { "g64120aPci", 0, VXB_BUSID_PLB, 0, g64120aPci0Num, g64120aPci0Resource },
#endif	/* INCLUDE_GT64120A_PCI */ /* yinwx 20100105 */
//#ifdef DRV_INTCTLR_I8259
//    { "i8259Pic", 0, VXB_BUSID_PLB, 0, i8259DevNum0, i8259DevResources0 },
//    { "i8259Pic", 1, VXB_BUSID_PLB, 0, i8259DevNum1, i8259DevResources1 },
//#endif  /* DRV_INTCTLR_I8259 */
///* system clock */
//    { "r4KTimerDev", 0, VXB_BUSID_PLB, 0, r4TimerDevNum, r4KTimerDevResources},
//#if defined(_WRS_CONFIG_SMP) && (VX_SMP_NUM_CPUS > 1)
//    { "r4KTimerDev", 1, VXB_BUSID_PLB, 0, r4TimerDevNum, r4KTimerDevResources},
//#if (VX_SMP_NUM_CPUS > 2)
//    { "r4KTimerDev", 2, VXB_BUSID_PLB, 0, r4TimerDevNum, r4KTimerDevResources},
//    { "r4KTimerDev", 3, VXB_BUSID_PLB, 0, r4TimerDevNum, r4KTimerDevResources},
//#endif /* (VX_SMP_NUM_CPUS > 2) */
//#endif /* defined(_WRS_CONFIG_SMP) && (VX_SMP_NUM_CPUS > 1) */
//
///* sibyte aux. clock */
//#ifdef DRV_TIMER_SB1
//    { "sb1TimerDev", 0, VXB_BUSID_PLB, 0, sb1TimerDevNum0, sb1TimerDevResources0},
//#endif /* DRV_TIMER_SB1 */
//
//    /* shared memory device */
//#ifdef INCLUDE_SM_COMMON
//    { "sm", 0, VXB_BUSID_PLB, 0, smDevNum, smDevResources},
//#endif /* INCLUDE_SM_COMMON */
//
//#ifdef INCLUDE_DSHM
//#ifdef INCLUDE_DSHM_BUS_PLB
//    { "dshmBusCtlrSibyte",  0, VXB_BUSID_PLB,     0,
//        NELEMENTS(dshmBusCtlrSibyteRes), dshmBusCtlrSibyteRes },
//    { "dshmPeerVxSibyte", 0, VXB_BUSID_VIRTUAL, 0,
//        NELEMENTS(dshmPeerVxSibyte0Res), dshmPeerVxSibyte0Res },
//    { "dshmPeerVxSibyte", 1, VXB_BUSID_VIRTUAL, 0,
//        NELEMENTS(dshmPeerVxSibyte1Res), dshmPeerVxSibyte1Res },
//#endif  /* INCLUDE_DSHM_BUS_PLB */
//#endif  /* INCLUDE_DSHM */
//
//
//#ifdef INCLUDE_DRV_STORAGE_INTEL_AHCI
//{ "intelAhciSata",0,VXB_BUSID_PCI,0,AHCIDevNum, AHCIDevResources},
//#endif

};


const int hcfDeviceNum = NELEMENTS(hcfDeviceList);

#endif


VXB_INST_PARAM_OVERRIDE sysInstParamTable[] =
    {
    { NULL, 0, NULL, VXB_PARAM_END_OF_LIST, {(void *)0} }
    };

extern	int	  (* _func_intEnableRtn) (int);
extern	int	  (* _func_intDisableRtn) (int) ;
extern int  int_enable_pic(u32 irq);
extern int int_disable_pic(u32 irq);
void vxbus_int_compate()
{ 
	_func_intEnableRtn = int_enable_pic;
	_func_intDisableRtn = int_disable_pic;
}
