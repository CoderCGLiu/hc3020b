#include <vxWorks.h>
#include <hwif/vxbus/vxBus.h>
#include <hwif/vxbus/vxbMethodDecl.h>

int usrNetEndLibInit() 
{  
//	printk("%s %d muxDevConnect_desc:%s\n",__FUNCTION__,__LINE__,&muxDevConnect_desc);
	vxbDevMethodRun(&muxDevConnect_desc, NULL);
	   
	return (OK);
}
static boolean net_init_flag = FALSE;
void vxbus_net_drv_init(void)
{ 
	if(net_init_flag == FALSE)
	{
//		printk("%s %d\n",__FUNCTION__,__LINE__);
		endLibInit();//net_drv_lib_init();     
//		printk("%s %d\n",__FUNCTION__,__LINE__);
		usrNetEndLibInit();
//		printk("%s %d vxbus_net_drv_init OK\n",__FUNCTION__,__LINE__);
		net_init_flag = TRUE; 
	}
	  
	return;
	
}
