#ifndef _CP0_ASM_COMMON_H
#define _CP0_ASM_COMMON_H
#include <sys/types.h>
//#include "vcstypes.h"
#define __read_64bit_c2_register(source, sel)       \
({                                                  \
    unsigned long __res;                       \
    if (sel == 0)                                   \
        __asm__ __volatile__(                       \
            ".set\tmips3\n\t"                       \
            "cfc2\t%0, " #source "\n\t"             \
            ".set\tmips0"                           \
            : "=r" (__res));                        \
    else                                            \
        __asm__ __volatile__(                       \
            ".set\tmips64\n\t"                      \
            "cfc2\t%0, " #source ", " #sel "\n\t"   \
            ".set\tmips0"                           \
            : "=r" (__res));                        \
    __res;                                          \
})

#define __write_64bit_c2_register(register, sel, value)     \
do {                                                        \
    if (sel == 0)                                           \
        __asm__ __volatile__(                               \
            ".set\tmips3\n\t"                               \
            "ctc2\t%z0, " #register "\n\t"                  \
            ".set\tmips0"                                   \
            : : "Jr" (value));                              \
    else                                                    \
        __asm__ __volatile__(                               \
            ".set\tmips64\n\t"                              \
            "ctc2\t%z0, " #register ", " #sel "\n\t"        \
            ".set\tmips0"                                   \
            : : "Jr" (value));                              \
} while (0)

#define __read_64bit_dma_register(source, sel)              \
({                                                          \
    unsigned long __res;                               \
    if (sel == 0)                                           \
        __asm__ __volatile__(                               \
            ".set\tmips3\n\t"                               \
            "cfdma\t%0, " #source "\n\t"                    \
            ".set\tmips0"                                   \
            : "=r" (__res));                                \
    else                                                    \
        __asm__ __volatile__(                               \
            ".set\tmips64\n\t"                              \
            "cfdma\t%0, " #source ", " #sel "\n\t"          \
            ".set\tmips0"                                   \
            : "=r" (__res));                                \
    __res;                                                  \
})

#define __write_64bit_dma_register(register, sel, value)    \
do {                                                        \
    if (sel == 0)                                           \
        __asm__ __volatile__(                               \
            ".set\tmips3\n\t"                               \
            "ctdma\t" #register ", %z0\n\t"                 \
            ".set\tmips0"                                   \
            : : "Jr" (value));                              \
    else                                                    \
        __asm__ __volatile__(                               \
            ".set\tmips64\n\t"                              \
            "ctdma\t%z0, " #register ", " #sel "\n\t"       \
            ".set\tmips0"                                   \
            : : "Jr" (value));                              \
} while (0)

#define __read_64bit_c0_split(source, sel)              \
({                                                      \
    unsigned long __val;                           \
    unsigned long __flags;                              \
                                                        \
    if (sel == 0)                                       \
        __asm__ __volatile__(                           \
            ".set\tmips64\n\t"                          \
            "dmfc0\t%M0, " #source "\n\t"               \
            "dsll\t%L0, %M0, 32\n\t"                    \
            "dsrl\t%M0, %M0, 32\n\t"                    \
            "dsrl\t%L0, %L0, 32\n\t"                    \
            ".set\tmips0"                               \
            : "=r" (__val));                            \
    else                                                \
        __asm__ __volatile__(                           \
            ".set\tmips64\n\t"                          \
            "dmfc0\t%M0, " #source ", " #sel "\n\t"     \
            "dsll\t%L0, %M0, 32\n\t"                    \
            "dsrl\t%M0, %M0, 32\n\t"                    \
            "dsrl\t%L0, %L0, 32\n\t"                    \
            ".set\tmips0"                               \
            : "=r" (__val));                            \
                                                        \
    __val;                                              \
})

#define __write_64bit_c0_split(source, sel, val)        \
do {                                                    \
    unsigned long __flags;                              \
                                                        \
    if (sel == 0)                                       \
        __asm__ __volatile__(                           \
            ".set\tmips64\n\t"                          \
            "dsll\t%L0, %L0, 32\n\t"                    \
            "dsrl\t%L0, %L0, 32\n\t"                    \
            "dsll\t%M0, %M0, 32\n\t"                    \
            "or\t%L0, %L0, %M0\n\t"                     \
            "dmtc0\t%L0, " #source "\n\t"               \
            ".set\tmips0"                               \
            : : "r" (val));                             \
    else                                                \
        __asm__ __volatile__(                           \
            ".set\tmips64\n\t"                          \
            "dsll\t%L0, %L0, 32\n\t"                    \
            "dsrl\t%L0, %L0, 32\n\t"                    \
            "dsll\t%M0, %M0, 32\n\t"                    \
            "or\t%L0, %L0, %M0\n\t"                     \
            "dmtc0\t%L0, " #source ", " #sel "\n\t"     \
            ".set\tmips0"                               \
            : : "r" (val));                             \
} while (0)

#define __read_64bit_c0_register(source, sel)           \
({                                                      \
    unsigned long __res;                           \
    if (sizeof(unsigned long) == 4)                     \
        __res = __read_64bit_c0_split(source, sel);     \
    else if (sel == 0)                                  \
        __asm__ __volatile__(                           \
            ".set\tmips3\n\t"                           \
            "dmfc0\t%0, " #source "\n\t"                \
            ".set\tmips0"                               \
            : "=r" (__res));                            \
    else                                                \
        __asm__ __volatile__(                           \
            ".set\tmips64\n\t"                          \
            "dmfc0\t%0, " #source ", " #sel "\n\t"      \
            ".set\tmips0"                               \
            : "=r" (__res));                            \
    __res;                                              \
})

#define __write_64bit_c0_register(register, sel, value) \
do {                                                    \
    if (sizeof(unsigned long) == 4)                     \
        __write_64bit_c0_split(register, sel, value);   \
    else if (sel == 0)                                  \
        __asm__ __volatile__(                           \
            ".set\tmips3\n\t"                           \
            "dmtc0\t%z0, " #register "\n\t"             \
            ".set\tmips0"                               \
            : : "Jr" (value));                          \
    else                                                \
        __asm__ __volatile__(                           \
            ".set\tmips64\n\t"                          \
            "dmtc0\t%z0, " #register ", " #sel "\n\t"   \
            ".set\tmips0"                               \
            : : "Jr" (value));                          \
} while (0)

#define __write_32bit_c0_register(register, sel, value) \
do {                                                    \
    if (sel == 0)                                       \
        __asm__ __volatile__(                           \
            "mtc0\t%z0, " #register "\n\t"              \
            : : "Jr" ((unsigned int)(value)));          \
    else                                                \
        __asm__ __volatile__(                           \
            ".set\tmips32\n\t"                          \
            "mtc0\t%z0, " #register ", " #sel "\n\t"    \
            ".set\tmips0"                               \
            : : "Jr" ((unsigned int)(value)));          \
} while (0)

#define __read_32bit_c0_register(source, sel)           \
({ int __res;                                           \
    if (sel == 0)                                       \
        __asm__ __volatile__(                           \
            "mfc0\t%0, " #source "\n\t"                 \
            : "=r" (__res));                            \
    else                                                \
        __asm__ __volatile__(                           \
            ".set\tmips32\n\t"                          \
            "mfc0\t%0, " #source ", " #sel "\n\t"       \
            ".set\tmips0\n\t"                           \
            : "=r" (__res));                            \
    __res;                                              \
})

#define read_c0_count()                 __read_32bit_c0_register($9, 0)
#define write_c0_count(val)             __write_32bit_c0_register($9, 0, val)

#define read_c0_compare()               __read_32bit_c0_register($11, 0)
#define write_c0_compare(val)           __write_32bit_c0_register($11, 0, val)

#define read_c0_status()                __read_32bit_c0_register($12, 0)
#define write_c0_status(val)            __write_32bit_c0_register($12, 0, val)

#define read_c0_epc()                   __read_64bit_c0_register($14, 0)
#define write_c0_epc(val)               __write_64bit_c0_register($14, 0, val)

#define read_c0_errorepc()              __read_64bit_c0_register($30, 0)
#define write_c0_errorepc(val)          __write_64bit_c0_register($30, 0, val)

#define read_c0_prid()                  __read_32bit_c0_register($15, 0)
#define write_c0_prid(val)              __write_32bit_c0_register($15, 0, val)

#define read_c0_ebase()                 __read_32bit_c0_register($15, 1)
#define write_c0_ebase(val)             __write_32bit_c0_register($15, 1, val)

#define read_c0_cause()                 __read_32bit_c0_register($13, 0)
#define write_c0_cause(val)             __write_32bit_c0_register($13, 0, val)

#define read_c0_entryhi()               __read_64bit_c0_register($10, 0)
#define write_c0_entryhi(val)           __write_64bit_c0_register($10, 0, val)

#define read_c0_badvaddr()              __read_64bit_c0_register($8, 0)
#define write_c0_badvaddr(val)          __write_64bit_c0_register($8, 0, val)

#define read_c0_pagemask()              __read_64bit_c0_register($5, 0)
#define write_c0_pagemask(val)          __write_64bit_c0_register($5, 0, val)

#define read_c0_entrylo0()              __read_64bit_c0_register($2, 0)
#define write_c0_entrylo0(val)          __write_64bit_c0_register($2, 0, val)

#define read_c0_entrylo1()              __read_64bit_c0_register($3, 0)
#define write_c0_entrylo1(val)          __write_64bit_c0_register($3, 0, val)

#define read_c0_context()               __read_64bit_c0_register($4, 0)
#define write_c0_context(val)           __write_64bit_c0_register($4, 0, val)

#define read_c0_contextconfig()         __read_32bit_c0_register($4, 1)
#define write_c0_contextconfig(val)     __write_32bit_c0_register($4, 1, val)

#define read_c0_userlocal()             __read_64bit_c0_register($4, 2)
#define write_c0_userlocal(val)         __write_64bit_c0_register($4, 2, val)

#define read_c0_xcontextconfig()        __read_64bit_c0_register($4, 3)
#define write_c0_xcontextconfig(val)    __write_64bit_c0_register($4, 3, val)

#define read_c0_pagegrain()             __read_32bit_c0_register($5, 1)
#define write_c0_pagegrain(val)         __write_32bit_c0_register($5, 1, val)

#define read_c0_index()                 __read_32bit_c0_register($0, 0)
#define write_c0_index(val)             __write_32bit_c0_register($0, 0, val)

#define read_c0_random()                __read_32bit_c0_register($1, 0)
#define write_c0_random(val)            __write_32bit_c0_register($1, 0, val)

#define read_c0_wired()                 __read_32bit_c0_register($6, 0)
#define write_c0_wired(val)             __write_32bit_c0_register($6, 0, val)

#define read_c0_hwrena()                __read_32bit_c0_register($7, 0)
#define write_c0_hwrena(val)            __write_32bit_c0_register($7, 0, val)

#define read_c0_intctl()                __read_32bit_c0_register($12, 1)
#define write_c0_intctl(val)            __write_32bit_c0_register($12, 1, val)

#define read_c0_config()                __read_32bit_c0_register($16, 0)
#define write_c0_config(val)            __write_32bit_c0_register($16, 0, val)

#define read_c0_config1()               __read_32bit_c0_register($16, 1)
#define write_c0_config1(val)           __write_32bit_c0_register($16, 1, val)

#define read_c0_config2()               __read_32bit_c0_register($16, 2)
#define write_c0_config2(val)           __write_32bit_c0_register($16, 2, val)

#define read_c0_config3()               __read_32bit_c0_register($16, 3)
#define write_c0_config3(val)           __write_32bit_c0_register($16, 3, val)

#define read_c0_config4()               __read_32bit_c0_register($16, 4)
#define write_c0_config4(val)           __write_32bit_c0_register($16, 4, val)

/* cache */
#define read_c0_taglo0()                __read_64bit_c0_register($28, 0)
#define write_c0_taglo0(val)            __write_64bit_c0_register($28, 0, val)

#define read_c0_taglo2()                __read_64bit_c0_register($28, 2)
#define write_c0_taglo2(val)            __write_64bit_c0_register($28, 2, val)

#define read_c0_taghi0()                __read_64bit_c0_register($29, 0)
#define write_c0_taghi0(val)            __write_64bit_c0_register($29, 0, val)

#define read_c0_taghi2()                __read_64bit_c0_register($29, 2)
#define write_c0_taghi2(val)            __write_64bit_c0_register($29, 2, val)

#define read_c0_datahi1()               __read_64bit_c0_register($29, 1)
#define write_c0_datahi1(val)           __write_64bit_c0_register($29, 1, val)

#define read_c0_datalo1()               __read_64bit_c0_register($28, 1)
#define write_c0_datalo1(val)             __write_64bit_c0_register($28, 1, val)
#define read_c0_datalo3()               __read_64bit_c0_register($28, 3)
#define write_c0_datalo3(val)           __write_64bit_c0_register($28, 3, val)

#define read_c0_datahi3()               __read_64bit_c0_register($29, 3)
#define write_c0_datahi3(val)           __write_64bit_c0_register($29, 3, val)

#define read_c0_percntl0()              __read_32bit_c0_register($25, 0)
#define write_c0_percntl0(val)          __write_32bit_c0_register($25, 0, val)

#define read_c0_percont0()              __read_32bit_c0_register($25, 1)
#define write_c0_percont0(val)          __write_32bit_c0_register($25, 1, val)

#define read_c0_percntl1()              __read_32bit_c0_register($25, 2)
#define write_c0_percntl1(val)          __write_32bit_c0_register($25, 2, val)

#define read_c0_percont1()              __read_32bit_c0_register($25, 3)
#define write_c0_percont1(val)          __write_32bit_c0_register($25, 3, val)

#define read_c0_percntl2()              __read_32bit_c0_register($25, 4)
#define write_c0_percntl2(val)          __write_32bit_c0_register($25, 4, val)

#define read_c0_percont2()              __read_32bit_c0_register($25, 5)
#define write_c0_percont2(val)          __write_32bit_c0_register($25, 5, val)

#define read_c0_percntl3()              __read_32bit_c0_register($25, 6)
#define write_c0_percntl3(val)          __write_32bit_c0_register($25, 6, val)

#define read_c0_percont3()              __read_32bit_c0_register($25, 7)
#define write_c0_percont3(val)          __write_32bit_c0_register($25, 7, val)

#define read_c0_pcintctl()              __read_32bit_c0_register($9, 7)
#define write_c0_pcintctl(val)          __write_32bit_c0_register($9, 7, val)

#define tlbwi()                         __asm__ __volatile__("tlbwi"::)

#define TLB_ENTRY 255

#define NUM_CPU_REGS 32
#define SIZE_CPU_REG sizeof(unsigned long)

enum {
    mask_1k   = 0UL,                  /*  0:   1K */
    mask_4k   = 0x3UL<<11,            /*  1:   4K */
    mask_16k  = 0xfUL<<11,            /*  2:  16K */
    mask_64k  = 0x3fUL<<11,           /*  3:  64K */
    mask_256k = 0xffUL<<11,           /*  4: 256K */
    mask_1m   = 0x3ffUL<<11,          /*  5:   1M */
    mask_4m   = 0xfffUL<<11,          /*  6:   4M */
    mask_16m  = 0x3fffUL<<11,         /*  7:  16M */
    mask_64m  = 0xffffUL<<11,         /*  8:  64M */
    mask_256m = 0x3ffffUL<<11,        /*  9: 256M */
    mask_1g   = 0xfffffUL<<11,        /* 10:   1G */
    mask_4g   = 0x3fffffUL<<11,       /* 11:   4G */
    mask_16g  = 0xffffffUL<<11,       /* 12:  16G */
    mask_64g  = 0x3ffffffUL<<11,      /* 13:  64G */
    mask_256g = 0xfffffffUL<<11,      /* 14: 256G */
    mask_1t   = 0x3fffffffUL<<11,     /* 15:   1T */
    mask_4t   = 0xffffffffUL<<11,     /* 16:   4T */
    mask_16t  = 0x3ffffffffUL<<11,    /* 17:  16T */
    mask_64t  = 0xfffffffffUL<<11,    /* 18:  64T */
    mask_256t = 0x3fffffffffUL<<11,   /* 19: 256T */
};

#define phys_cache(pyhs)    (0xFFFFFFFF80000000+pyhs)

/*TODO: ldf 20230411, 另一处定义在cpu.h*/
//typedef struct      /* REG_SET - MIPS architecture register set */
//{
//    unsigned long gpreg[32];        /* data registers */
//} REG_SET;

#define zeroReg     gpreg[0]    /* wired zero */
#define atReg       gpreg[1]    /* assembler temp */
#define v0Reg       gpreg[2]    /* function return 0 */
#define v1Reg       gpreg[3]    /* function return 1 */
#define a0Reg       gpreg[4]    /* parameter 0 */
#define a1Reg       gpreg[5]    /* parameter 1 */
#define a2Reg       gpreg[6]    /* parameter 2 */
#define a3Reg       gpreg[7]    /* parameter 3 */
#define t0Reg       gpreg[8]    /* caller saved 0 */
#define t1Reg       gpreg[9]    /* caller saved 1 */
#define t2Reg       gpreg[10]   /* caller saved 2 */
#define t3Reg       gpreg[11]   /* caller saved 3 */
#define t4Reg       gpreg[12]   /* caller saved 4 */
#define t5Reg       gpreg[13]   /* caller saved 5 */
#define t6Reg       gpreg[14]   /* caller saved 6 */
#define t7Reg       gpreg[15]   /* caller saved 7 */
#define s0Reg       gpreg[16]   /* callee saved 0 */
#define s1Reg       gpreg[17]   /* callee saved 1 */
#define s2Reg       gpreg[18]   /* callee saved 2 */
#define s3Reg       gpreg[19]   /* callee saved 3 */
#define s4Reg       gpreg[20]   /* callee saved 4 */
#define s5Reg       gpreg[21]   /* callee saved 5 */
#define s6Reg       gpreg[22]   /* callee saved 6 */
#define s7Reg       gpreg[23]   /* callee saved 7 */
#define t8Reg       gpreg[24]   /* caller saved 8 */
#define t9Reg       gpreg[25]   /* caller saved 8 */
#define k0Reg       gpreg[26]   /* kernal reserved 0 */
#define k1Reg       gpreg[27]   /* kernal reserved 1 */
#define gpReg       gpreg[28]   /* global pointer */
#define spReg       gpreg[29]   /* stack pointer */
#define s8Reg       gpreg[30]   /* callee saved 8 */
#define fpReg       s8Reg       /* gcc says this is frame pointer */
#define raReg       gpreg[31]   /* return address */
#define reg_pc      pc          /* program counter */
#define reg_sp      spReg       /* stack pointer */
#define reg_fp      fpReg       /* frame pointer */

/*
 * Bits in the coprozessor 0 config register.
 */
#define CONF_UNCACHED               2
#define CONF_CACHABLE_NONCOHERENT   3
#define CONF_CACHABLE_COHERENT      6
#define CONF_CMASK                  7

#if 0
#define SYS_CPU0_INIT   cpu0_test
#define SYS_CPU1_INIT   cpu1_test
#define LOCK            0x400000
#define LOCK_XUSEG      0x400000
#define LOCK_XKPHYS     0xb000000000400000

#ifdef CONFIG_MANUAL_SET_MAX_CPU_NUMBER
#define NR_CPUS         CONFIG_MAX_CPU_NUMBER
#else

#ifdef CONFIG_HRDSP22
#define NR_CPUS         2
#endif

#ifdef CONFIG_HRDSP48
#define NR_CPUS         4
#endif

#endif // MANUAL_SET_MAX_CPU_NUMBER

#define CPU1            0xffffffff80800000
#ifdef CONFIG_HRDSP48
#define CPU2            0xffffffff80800008
#define CPU3            0xffffffff80800010
#endif
#define CPU1_BUF0       0xffffffff80800008
#define SYS_CPU1_ENALBE 0x1f088008
#define FIN_CPU(a)      *(volatile long *)(a)

#define SMP_BUF0        0xffffffff80a00000
#define SMP_ATOMIC_NUM  0x10
#define SMP_ATOMIC_LOOP 0x1
#endif//410

/* For interrupt use */
#define  CAUSEB_EXCCODE         2
#define  CAUSEF_EXCCODE         (_ULCAST_(31)  <<  2)
#define  CAUSEB_IP              8
#define  CAUSEF_IP              (_ULCAST_(255) <<  8)
#define  CAUSEB_IP0             8
#define  CAUSEF_IP0             (_ULCAST_(1)   <<  8)
#define  CAUSEB_IP1             9
#define  CAUSEF_IP1             (_ULCAST_(1)   <<  9)
#define  CAUSEB_IP2             10
#define  CAUSEF_IP2             (_ULCAST_(1)   << 10)
#define  CAUSEB_IP3             11
#define  CAUSEF_IP3             (_ULCAST_(1)   << 11)
#define  CAUSEB_IP4             12
#define  CAUSEF_IP4             (_ULCAST_(1)   << 12)
#define  CAUSEB_IP5             13
#define  CAUSEF_IP5             (_ULCAST_(1)   << 13)
#define  CAUSEB_IP6             14
#define  CAUSEF_IP6             (_ULCAST_(1)   << 14)
#define  CAUSEB_IP7             15
#define  CAUSEF_IP7             (_ULCAST_(1)   << 15)
#define  CAUSEB_IV              23
#define  CAUSEF_IV              (_ULCAST_(1)   << 23)
#define  CAUSEB_DC              27
#define  CAUSEF_DC              (_ULCAST_(1)   << 27)
#define  CAUSEB_TI              30
#define  CAUSEF_TI              (_ULCAST_(1)   << 30)
#define  CAUSEB_CE              28
#define  CAUSEF_CE              (_ULCAST_(3)   << 28)
#define  CAUSEB_BD              31
#define  CAUSEF_BD              (_ULCAST_(1)   << 31)

//#include <mipsregs.h>

static inline void disable_count(void)
{
        int cause;
        cause = read_c0_cause();
        cause |= CAUSEF_DC; // disable counting of count.
        write_c0_cause(cause);
}

static inline void enable_count(void)
{
        int cause;
        cause = read_c0_cause();
        cause &= ~CAUSEF_DC; // disable counting of count.
        write_c0_cause(cause);
}

static inline void enable_interrupt(void)
{
        int status;
        status = read_c0_status();
        status |= 0x20/*ST0_IE*/;
        write_c0_status(status);
}

static inline void disable_interrupt(void)
{
        int status;
        status = read_c0_status();
        status &= ~0x20/*ST0_IE*/;
        write_c0_status(status);
}

typedef struct {
    volatile unsigned int lock;
} spinlock_t;

typedef union {
    /*
     * bits  0..15 : serving_now
     * bits 16..31 : ticket
     */
    u32 lock;
    struct {
        u16 serving_now;
        u16 ticket;
    } h;
} arch_spinlock_t;

#define SPIN_LOCK_UNLOCKED (spinlock_t) { 0 }
#define spin_lock_init(x)   do { (x)->lock = 0; } while(0)

#define ARCH_SPIN_LOCK_UNLOCKED { .lock = 0 }
#define DEFINE_RAW_SPINLOCK(x)  arch_spinlock_t x = ARCH_SPIN_LOCK_UNLOCKED
#define arch_spin_lock_init(x)  do { (x)->lock = 0; } while(0)

static inline void spin_lock(spinlock_t *lock)
{
    unsigned int tmp;

    __asm__ __volatile__(
    ".set\tnoreorder\t\t\t# spin_lock\n"
    "1:\tll\t%1, %2\n\t"
    "bnez\t%1, 1b\n\t"
    " li\t%1, 1\n\t"
    "sc\t%1, %0\n\t"
    "beqz\t%1, 1b\n\t"
    " sync\n\t"
    ".set\treorder"
    : "=m" (lock->lock), "=&r" (tmp)
    : "m" (lock->lock)
    : "memory");
}

static inline void spin_unlock(spinlock_t *lock)
{
    __asm__ __volatile__(
    ".set\tnoreorder\t\t\t# spin_unlock\n\t"
    "sw\t$0, %0\n\t"
    "sync\n\t"
    ".set\treorder"
    : "=m" (lock->lock)
    : "m" (lock->lock)
    : "memory");
}

typedef struct {
    volatile unsigned int lock;
} rwlock_t;

#define RW_LOCK_UNLOCKED (rwlock_t) { 0 }

#define rwlock_init(x)  do { *(x) = RW_LOCK_UNLOCKED; } while(0)

static inline void read_lock(rwlock_t *rw)
{
    unsigned int tmp;

    __asm__ __volatile__(
    ".set\tnoreorder\t\t\t# read_lock\n"
    "1:\tll\t%1, %2\n\t"
    "bltz\t%1, 1b\n\t"
    " addu\t%1, 1\n\t"
    "sc\t%1, %0\n\t"
    "beqz\t%1, 1b\n\t"
    " sync\n\t"
    ".set\treorder"
    : "=m" (rw->lock), "=&r" (tmp)
    : "m" (rw->lock)
    : "memory");
}

static inline void read_unlock(rwlock_t *rw)
{
    unsigned int tmp;

    __asm__ __volatile__(
    ".set\tnoreorder\t\t\t# read_unlock\n"
    "1:\tll\t%1, %2\n\t"
    "sub\t%1, 1\n\t"
    "sc\t%1, %0\n\t"
    "beqz\t%1, 1b\n\t"
    " sync\n\t"
    ".set\treorder"
    : "=m" (rw->lock), "=&r" (tmp)
    : "m" (rw->lock)
    : "memory");
}

static inline void write_lock(rwlock_t *rw)
{
    unsigned int tmp;

    __asm__ __volatile__(
    ".set\tnoreorder\t\t\t# write_lock\n"
    "1:\tll\t%1, %2\n\t"
    "bnez\t%1, 1b\n\t"
    " lui\t%1, 0x8000\n\t"
    "sc\t%1, %0\n\t"
    "beqz\t%1, 1b\n\t"
    " sync\n\t"
    ".set\treorder"
    : "=m" (rw->lock), "=&r" (tmp)
    : "m" (rw->lock)
    : "memory");
}

static inline void write_unlock(rwlock_t *rw)
{
    __asm__ __volatile__(
    ".set\tnoreorder\t\t\t# write_unlock\n\t"
    "sw\t$0, %0\n\t"
    "sync\n\t"
    ".set\treorder"
    : "=m" (rw->lock)
    : "m" (rw->lock)
    : "memory");
}

static inline void arch_spin_lock(arch_spinlock_t *lock)
{
    int my_ticket;
    int tmp;
    int inc = 0x10000;

    __asm__ __volatile__ (
    "   .set push       # arch_spin_lock    \n"
    "   .set noreorder                  \n"
    "                           \n"
    "1: ll  %[ticket], %[ticket_ptr]        \n"
    "   addu    %[my_ticket], %[ticket], %[inc]     \n"
    "   sc  %[my_ticket], %[ticket_ptr]     \n"
    "   beqz    %[my_ticket], 1b            \n"
    "    srl    %[my_ticket], %[ticket], 16     \n"
    "   andi    %[ticket], %[ticket], 0xffff        \n"
    "   bne %[ticket], %[my_ticket], 4f     \n"
    "    subu   %[ticket], %[my_ticket], %[ticket]  \n"
    "2:                         \n"
    "   .subsection 2                   \n"
    "4: andi    %[ticket], %[ticket], 0x1fff        \n"
    "   sll %[ticket], 5                \n"
    "                           \n"
    "6: bnez    %[ticket], 6b               \n"
    "    subu   %[ticket], 1                \n"
    "                           \n"
    "   lhu %[ticket], %[serving_now_ptr]       \n"
    "   beq %[ticket], %[my_ticket], 2b     \n"
    "    subu   %[ticket], %[my_ticket], %[ticket]  \n"
    "   b   4b                  \n"
    "    subu   %[ticket], %[ticket], 1         \n"
    "   .previous                   \n"
    "   .set pop                    \n"
    "sync                           \n\t"
    : [ticket_ptr] "+m" (lock->lock),
      [serving_now_ptr] "+m" (lock->h.serving_now),
      [ticket] "=&r" (tmp),
      [my_ticket] "=&r" (my_ticket)
    : [inc] "r" (inc));
}

static inline void arch_spin_unlock(arch_spinlock_t *lock)
{
    int my_ticket;
    int tmp;

    __asm__ __volatile__ (
    "   .set push       # arch_spin_lock    \n"
    "   .set noreorder                  \n"
    "                           \n"
    "   lh  %[ticket], %[serving_now_ptr]       \n"
    "       addi    %[ticket], %[ticket], 1                 \n"
    "   sh  %[ticket], %[serving_now_ptr]       \n"
    "       sync                            \n"
    "   .set pop                    \n"
    : [serving_now_ptr] "+m" (lock->h.serving_now),
      [ticket] "=&r" (tmp)
    : );
}

/* Spin lock delay, created by weiwei */
static inline void spin_lock_delay(spinlock_t *lock)
{
       unsigned int tmp;

       __asm__ __volatile__(
       ".set\tnoreorder\t\t\t# spin_lock\n"
       "1:\tssnop\n\t"
       "ssnop\n\t"
       "ssnop\n\t"
       "ssnop\n\t"
       "ssnop\n\t"
       "ssnop\n\t"
       "ssnop\n\t"
       "ssnop\n\t"
       "ssnop\n\t"
       "ssnop\n\t"
       "ssnop\n\t"
       "ssnop\n\t"
       "ssnop\n\t"
       "ssnop\n\t"
       "ssnop\n\t"
       "ssnop\n\t"
       "ssnop\n\t"
       "ll\t%1, %2\n\t"
       "bnez\t%1, 1b\n\t"
       " li\t%1, 1\n\t"
       "sc\t%1, %0\n\t"
       "beqz\t%1, 1b\n\t"
       " sync\n\t"
       ".set\treorder"
       : "=m" (lock->lock), "=&r" (tmp)
       : "m" (lock->lock)
       : "memory");
}

#endif /*_CP0_ASM_COMMON_H*/
