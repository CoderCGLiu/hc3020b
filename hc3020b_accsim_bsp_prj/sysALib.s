#define ASM_LANGUAGE
#define _ASMLANGUAGE

#include <mips_asm.h>
#include <gs464_cpu.h>
#include <asm_linkage.h>

#ifndef _HC3020B_
#define CORE_IPI_MAILBOX(core) (0x900000001f9304c0 +  core*64 )
#else /*ldf 20230920 add:: for hc3020b*/
#define CORE_IPI_MAILBOX(core) (0x900000001f9304b0 +  core*64 )
#endif

#include "hr2_regdef.h"
	.globl HR2_cpu_start
	.ent HR2_cpu_start
HR2_cpu_start: 
	li	t0,0x1
	bne a0,t0,1f
	nop
	dli	t0,CORE_IPI_MAILBOX(1)
	li      v1, 'z'
    dli	    v0, 0x900000001f050030
    sb      v1, 0(v0)
	b	2f
1:
	li	t0,0x2
	bne	a0,t0,1f
	nop
	dli	t0,CORE_IPI_MAILBOX(2)
	li      v1, 'x'
    dli	    v0, 0x900000001f050030
    sb      v1, 0(v0)
	b	2f
1:
	li	t0,0x3
	bne	a0,t0,1f
	nop
	dli	t0,CORE_IPI_MAILBOX(3)
	li      v1, 'y'
    dli	    v0, 0x900000001f050030
    sb      v1, 0(v0)
	b	2f
1:
	li	t0,0x4
	bne	a0,t0,1f
	nop
	dli	t0,CORE_IPI_MAILBOX(4)
	li      v1, 'y'
    dli	    v0, 0x900000001f050030
    sb      v1, 0(v0)
	b	2f
1:
	li	t0,0x5
	bne	a0,t0,1f
	nop
	dli	t0,CORE_IPI_MAILBOX(5)
	li      v1, 'y'
    dli	    v0, 0x900000001f050030
    sb      v1, 0(v0)
	b	2f
1:
	li	t0,0x6
	bne	a0,t0,1f
	nop
	dli	t0,CORE_IPI_MAILBOX(6)
	li      v1, 'y'
    dli	    v0, 0x900000001f050030
    sb      v1, 0(v0)
	b	2f
1:
	li	t0,0x7
	bne	a0,t0,1f
	nop
	dli	t0,CORE_IPI_MAILBOX(7)
	li      v1, 'y'
    dli	    v0, 0x900000001f050030
    sb      v1, 0(v0)
	b	2f
2:
	li      v1, '2'
    dli	    v0, 0x900000001f050030
    sb      v1, 0(v0)
    nop
    sd		a4,		0x18(t0)
	sd		a3,		0x10(t0)
	sd		a2,		8(t0)
	sd		a1,		0(t0)  /*pc最后存入mailbox，避免sp还未存入就被读取.2018.11.2*/
1:
	jr	ra
.end HR2_cpu_start
