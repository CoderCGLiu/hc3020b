/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件实现板级硬件资源的初始化
 * 修改：
 * 		 2013-08-21， 张静，建立。 
*/

#include <reworks/printk.h>
#include <bsp.h>

/**
 * 函数声明
 */
extern u32 get_c0_sr();
extern void set_c0_sr(int val);
extern int mips64_readw(u32 cpu_num, u32 addr);
extern void mips64_writew(u32 cpu_num, u32 addr, u32 val);

/*******************************************************************************
 * 
 * 硬件资源及配置检测
 * 
 *     本程序实现对龙芯3A部分硬件资源的配置进行检测，对不正确功能重新设置，避免产生
 *  隐患。
 *     
 * 输入：
 *       无
 * 输出：
 *       无
 * 返回：
 *       成功返回0；失败返回-1
 */
static void ls3a_board_check(void)
{
	int val = 0;
	int i = 0;
	int win_base;
	
	/*PCI 控制器retry次数设置:ISR58 bit8-15. 00-无限重试*/
	val = *(unsigned int *)(LS3A_PCICTRL_BASE + LS3A_PCI_ISR58);
	
#ifdef DEBUG
	printk("The ISR58 value is 0x%x\n",val);
#endif
	
	if(!((val >> 8) & 0xff)) 
	{
		val |= (0xff << 8); /*重试次数设置为255次,取值范围为1-255*/
		*(unsigned int *)(LS3A_PCICTRL_BASE + LS3A_PCI_ISR58) = val;
	}
	
	/*避免猜测访问*/
	for(i = 0; i < 7; i++)
	{
		win_base = mips64_readw(0, LS3A_WIN_BASE + i * 8); /*基地址寄存器*/
		if(win_base == 0)
		{
			break;
		}
		else
		{
			val = mips64_readw(0, LS3A_WIN_BASE + i * 8 + 0x80);
			
			if(win_base == 0x10000000)
			{
				val &= ~((1<<4)|(1<<5));
				mips64_writew(0, LS3A_WIN_BASE + i * 8 + 0x80, val);
			}
		}
		
	}
}

//800M
//1.6G

/*******************************************************************************
 * 
 * 板级硬件资源初始化
 * 
 *     本程序实现板级硬件资源初始化
 *     
 * 输入：
 *       无
 * 输出：
 *       无
 * 返回：
 *       成功返回0；失败返回-1
 */
void ls3a_hw_init(void)
{
	/*使能64位的kernel段访问，保证能使用64位数据*/
	u32 val = get_c0_sr();
//	val |=0x80;//SR_KX;
	val |=0x20;//SR_UX;
	set_c0_sr(val); 
	
	/*硬件配置检测*/
//	ls3a_board_check();
	
	/*F81866芯片初始化，主要针对串口*/
//	extern void superio_init(void);
//	superio_init();

	return;
}
