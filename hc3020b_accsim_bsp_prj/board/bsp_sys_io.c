/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件实现了龙芯3A I/O读写的外部接口
 * 修改：
 * 		 1. 2014-03-07，张静，实现I/O读写接口 
 */

#include <reworks/types.h>
#include <bsp.h>

/**
 * 函数声明
 */
//u8   sys_in_byte(u32 address);
//void sys_out_byte(u32 address, u8 data);
//u16  sys_in_word(u32 address);
//void sys_out_word(u32 address, u16 data);
//u32  sys_in_long(u32 address);
//void sys_out_long(u32 address, u32 data);
//void sys_out_longstring(u32 port, u32 *address, int count);
//void sys_in_longstring(u32 port, u32 *address, int count);
//void sys_out_wordstring(u32 addr,u16 *src,int length);
//void sys_in_wordstring(u32 addr,u16 *dest,int length);
//void sys_in_word_stringrev(u32 port, u16 *address, int count);



/**
 * 宏定义
 */
#define PBYTESWAP(x)    (x)
#define PSWAP(x)        (x)

/*******************************************************************************
 * 
 * 按字节读I/O地址
 * 
 * 		本程序用于按字节读I/O地址。
 * 
 * 输入：
 * 		address I/O地址
 * 输出：
 * 		无
 * 返回：
 * 		I/O地址的值
 */
u8  sys_in_byte(u32 address)
   {
		u64 address_64;
		u8 retval;
		/* force addresses <64k into PCI I/O space */
		if (!(address & 0xffff0000))
		{
			address_64 = address + 0xffffffffb8000000;
			retval = *(volatile u8 *)address_64;
		}else{
			address_64 = (u64)address|0xffffffff00000000;
			retval = *(volatile u8 *)address_64;
		}		
		return (retval);
    }
#if 0
u8  sysInByte(u32 address)
{
	return sys_in_byte(address);
}
#endif

/*******************************************************************************
 * 
 * 按字节写I/O地址
 * 
 * 		本程序用于按字节写I/O地址。
 * 
 * 输入：
 * 		address I/O地址
 * 		data 数据值
 * 输出：
 * 		无
 * 返回：
 * 		无
 */
void sys_out_byte(u32 address, u8 data)
  {
    
	u64 address_64;
	if (!(address & 0xffff0000))
	{
		address_64 = address + 0xffffffffb8000000;
		*(volatile u8 *)address_64 = data;
	}else{
		address_64 = (u64)address|0xffffffff00000000;
		*(volatile u8 *)address_64 = data;
	}
		
    return;
  }
#if 0
void sysOutByte(u32 address, u8 data)
{
	return sys_out_byte(address,data);
}
#endif
/*******************************************************************************
 * 
 * 按字读I/O地址
 * 
 * 		本程序用于按字读I/O地址。
 * 
 * 输入：
 * 		address I/O地址
 * 输出：
 * 		无
 * 返回：
 * 		I/O地址的值
 */
u16  sys_in_word(u32 address)
    {
	u16 retval;	
	u64 address_64;
	/* force addresses <64k into PCI I/O space */
	if (!(address & 0xffff0000))
	{
		address_64 = address + 0xffffffffb8000000;
		retval = *(volatile u16 *)address_64;
	}else{
		address_64 = (u64)address|0xffffffff00000000;
		retval = *(volatile u16 *)address_64;
	}		
    return (retval);
    }
#if 0
u16  sysInWord(u32 address)
{
	return sys_in_word(address);
}
#endif
/*******************************************************************************
 * 
 * 按字写I/O地址
 * 
 * 		本程序用于按字写I/O地址。
 * 
 * 输入：
 * 		address I/O地址
 * 		data 要写的数值
 * 输出：
 * 		无
 * 返回：
 * 		无
 */
void sys_out_word(u32 address, u16 data)
  {
   
	u64 address_64;
	if (!(address & 0xffff0000))
	{
		address_64 = address + 0xffffffffb8000000;
		*(volatile u16 *)address_64 = (u16)data;
	}else{
		address_64 = (u64)address|0xffffffff00000000;
		*(volatile u16 *)address_64 = (u16)data;
	}  
    return;
  }
#if 0
void sysOutWord(u32 address, u16 data)
{
	return sys_out_word(address,data);
}
#endif
/*******************************************************************************
 * 
 * 按整数大小读I/O地址
 * 
 * 		本程序用于按整数大小读I/O地址。
 * 
 * 输入：
 * 		address I/O地址
 * 输出：
 * 		无
 * 返回：
 * 		I/O地址的值
 */
u32  sys_in_long(u32 address)
  {
    u32 retval;
	u64 address_64;
	/* force addresses <64k into PCI I/O space */
	if (!(address & 0xffff0000))
	{
		address_64 = address + 0xffffffffb8000000;
		retval = *(volatile u32 *)address_64;
	}else{
		address_64 = (u64)address|0xffffffff00000000;
		retval = *(volatile u32 *)address_64;
	}	   
    return (retval);
  }
#if 0
u32 sysInLong(u32 address)
{
	return sys_in_long(address);
}
#endif
/*******************************************************************************
 * 
 * 按整数大小写I/O地址
 * 
 * 		本程序用于按整数大小写I/O地址。
 * 
 * 输入：
 * 		address I/O地址
 * 		data 要写的数值
 * 输出：
 * 		无
 * 返回：
 * 		无
 */
void sys_out_long(u32 address, u32 data)
   {
	u64 address_64;
	if (!(address & 0xffff0000))
	{
		address_64 = address + 0xffffffffb8000000;
		*(volatile u32 *)address_64 = (u32)data;
	}else{
		address_64 = (u64)address|0xffffffff00000000;
		*(volatile u32 *)address_64 = (u32)data;
	}     
    return;
    }
#if 0
void sysOutLong(u32 address, u32 data)
{
	return sys_out_long(address,data);
}
#endif
/*******************************************************************************
 * 
 * 将字符串按整形大小写到I/O地址中
 * 
 * 		本程序用于按整数大小写I/O地址。
 * 
 * 输入：
 * 		port I/O地址
 * 		address 要写到I/O地址中的数据地址
 * 		count 要写入的数据长度
 * 输出：
 * 		无
 * 返回：
 * 		无
 */
void sys_out_longstring(u32 port, u32 *address, int count)
 {
    register u32 data;
    u64 address_64;
    if (!(port & 0xffff0000))
    	address_64 = port + 0xffffffffb8000000;
    else
    	address_64 = (u64)port|0xffffffff00000000;
    while (count--)
	    {
        data = *(address++);
	    *(volatile u32 *)address_64 = PSWAP(data);
	    }
    
    return;
 }

#if 0
void sysOutLongString(u32 port, u32 *address, int count)
{
	return sys_out_longstring( port, address, count);
}
#endif
/*******************************************************************************
 * 
 * 将I/O地址中的值按整形大小读出到字符串中
 * 
 * 		本程序用于将I/O地址中的值按整形大小读出到字符串中。
 * 
 * 输入：
 * 		port I/O地址
 * 		address 存放数据的字符串地址
 * 		count 要读的数据长度
 * 输出：
 * 		无
 * 返回：
 * 		无
 */
void sys_in_longstring(u32 port, u32 *address, int count)
  {
	  u64 address_64;
	  if (!(port & 0xffff0000))
		address_64 = port + 0xffffffffb8000000;
	  else
		address_64 = (u64)port|0xffffffff00000000;

    while (count--)
	    {
	    *(address++) = PSWAP (*(volatile u32 *)(address_64));
	    }
    
    return;
  }
#if 0
void sysInLongString(u32 port, u32 *address, int count)
{
	return sys_in_longstring(port,address,count);
}
#endif
/*******************************************************************************
 * 
 * 将字符串按字大小写到I/O地址中
 * 
 * 		本程序用于按整数大小写I/O地址。
 * 
 * 输入：
 * 		port I/O地址
 * 		address 要写到I/O地址中的数据地址
 * 		count 要写入的数据长度
 * 输出：
 * 		无
 * 返回：
 * 		无
 */
void sys_out_wordstring(u32 addr,u16 *src,int length)
  {
	register u16 data;
	u64 address_64;
	if (!(addr & 0xffff0000))
	address_64 = addr + 0xffffffffb8000000;
	else
	address_64 = (u64)addr|0xffffffff00000000;
    /* Do a series of 16 bit writes until we have written all data. */
    while (length--)
        {
    		data = (u16)*src++;
    		*(volatile u16 *)address_64 = PBYTESWAP (data);
	    }
    
    return;
  }
#if 0
void sysOutWordString(u32 addr,u16 *src,int length)
{
	return sys_out_wordstring(addr,src,length);
}
#endif
/*******************************************************************************
 * 
 * 将I/O地址中的值按字大小读出到字符串中
 * 
 * 		本程序用于将I/O地址中的值按字大小读出到字符串中。
 * 
 * 输入：
 * 		port I/O地址
 * 		address 存放数据的字符串地址
 * 		count 要读的数据长度
 * 输出：
 * 		无
 * 返回：
 * 		无
 */
void sys_in_wordstring(u32 addr,u16 *dest,int length)
 {
	u64 address_64;
	if (!(addr & 0xffff0000))
	address_64 = addr + 0xffffffffb8000000;
	else
	address_64 = (u64)addr|0xffffffff00000000;
    while (length--)
        {
	    *dest++ = PBYTESWAP (*(volatile u16 *)(address_64));
	    }
    
    return;
}
#if 0
void sysInWordString(u32 addr,u16 *dest,int length)
{
	return sys_in_wordstring(addr,dest,length);
}
#endif
/*******************************************************************************
 * 
 * sys_in_wordstring的字节交换实现
 * 
 * 		本程序用于将I/O地址中的值按字大小读出到字符串中。
 * 
 * 输入：
 * 		port I/O地址
 * 		address 存放数据的字符串地址
 * 		count 要读的数据长度
 * 输出：
 * 		无
 * 返回：
 * 		无
 */
void sys_in_word_stringrev(u32 port, u16 *address, int count)
  {
	u64 address_64;
	if (!(port & 0xffff0000))
	address_64 = port + 0xffffffffb8000000;
	else
	address_64 = (u64)port|0xffffffff00000000;
    while (count--)
	    {
	    *(address++) = PBYTESWAP (*(volatile u16 *)(address_64));
	    }
    
    return;
  }
#if 0
void sysInWordStringRev(u32 port, u16 *address, int count)
{
	return  sys_in_word_stringrev(port,address,count);
}
#endif
