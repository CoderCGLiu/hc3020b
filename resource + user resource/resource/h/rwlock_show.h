/*! @file rwlock_show.h
    @brief ReWorks读写锁信息显示操作接口的头文件
    
 * 本文件定义了任务读写锁对象相关信息的获取/显示操作接口。
 *
* @version 4.7
* 
* @see 无
*/

#ifndef _REWORKS_RWLOCKSHOW_H_
#define _REWORKS_RWLOCKSHOW_H_

#ifdef __cplusplus
extern "C"
{
#endif
	
#include <reworks/types.h>
#include <pthread.h>

/** 
 * @defgroup group_os_reworks_rwlock_show ReWorks任务读写锁显示
 * @ingroup group_os_reworks_task
 * @{ */
 
/*! \struct pthread_rwlock_info
 * \brief 任务读写锁信息结构定义
 *
 * 该数据结构定义了任务读写锁的相关信息，包括读写锁的状态、读者线程的数量、读者/写者等待线程的数量等。
 */
typedef struct pthread_rwlock_info
{
	pthread_rwlock_states_t state;    //!< 读写锁的状态
	u32 reader_num;    //!< 读者线程数目
	u32 waiting_writer_num;    //!< 写者等待线程数量
	u32 waiting_reader_num;    //!< 读者等待线程数量
#define MAX_THREAD 20    //!< 最大等待线程标识符列表长度
	pthread_t waiting_writer_list[MAX_THREAD];    //!< 写者等待线程标识符列表
	pthread_t waiting_reader_list[MAX_THREAD];    //!< 读者等待线程标识符列表
} pthread_rwlock_info_t; //!< 任务读写锁信息结构定义

/**
 * @brief 获取读写锁的信息。
 * 
 * 该接口用于获取指定的读写锁的概要信息，包括读写锁的状态、读者线程数目等信息。
 * 
 * @param 	rwlock	 指向读写锁对象的指针
 * @param 	rwlock_info	存放读写锁信息的指针
 * 
 * @return 	0 		函数执行成功
 * @return 	EINVAL 	参数rwlock或rwlock_info为无效指针，或者rwlock指定的读写锁不存在
 * @return  EMNOTINITED 读写锁模块尚未初始化
 * 
 * @exception EINVAL 参数无效
 * @exception EMNOTINITED 模块未初始化
 */
int pthread_rwlock_getinfo(const pthread_rwlock_t *rwlock,
		pthread_rwlock_info_t *rwlock_info);

/**
 * @brief 显示读写锁的信息。
 * 
 * 该接口用于显示指定的读写锁的相关信息。
 * 
 * @param 	rwlock	 指向读写锁对象的指针
 * @param 	level	显示读写锁信息的详细程度，0为显示概要信息，1为显示详细信息
 * 
 * @return 	0		函数执行成功
 * @return 	EINVAL 	rwlock为无效指针，或者level取值不合法，或者rwlock指定的读写锁不存在
 * @return  EMNOTINITED 读写锁模块尚未初始化
 * 
 * @exception EINVAL 参数无效
 * @exception EMNOTINITED 模块未初始化
 */
int pthread_rwlock_show(const pthread_rwlock_t *rwlock, int level);


/** @example example_pthread_rwlock_getinfo.c
 * 下面的例子演示了如何使用 pthread_rwlock_getinfo().
 */ 

/** @example example_pthread_rwlock_show.c
 * 下面的例子演示了如何使用 pthread_rwlock_show().
 */

/*******************************************************************************/
/*  @} */

#ifdef __cplusplus
}
#endif
#endif
