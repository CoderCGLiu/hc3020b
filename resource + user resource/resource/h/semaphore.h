/*! @file semaphore.h
	@brief ReWorks信号量操作的头文件
	
 * 本文件定义了信号量相关的操作接口。
 * 
 * @version 4.7
 * 
 * @see 无
 */
#ifndef _REWORKS_SEMAPHORE_H_
#define _REWORKS_SEMAPHORE_H_

#ifdef __cplusplus
extern "C"
{
#endif 

#include <sys/time.h>
#include <reworks/types.h>
	
/** @defgroup group_semaphore ReWorks信号量管理
 * @ingroup group_os_reworks
 *	在支持多任务的系统中，任务间需要同步其运行，并协调对共享资源的互斥访问，ReWorks系统中提供了信号量
 *（semphore）的机制来保护共享资源。根据其初始值和应用不同，可以分为二进制信号量和计数信号量两种。二
 *进制信号量的值只能是0或1，计数信号量可以是任意非负值。二进制信号量在逻辑上相当于一个互斥信号量，从
 *概念上来说计数信号量是一个非负整数计数。信号量通常用来协调对资源的访问，其中信号量计数会初始化为可
 *用资源的数目。任务在增加资源时会增加计数，在删除资源时会减小计数，这些操作都以原子方式执行。如果信
 *号量计数变为零，则表明已无可用资源。信号量可以是未命名的，也可以是命名的。命名信号量可能可供多个任
 *务使用，具体取决于信号量的分配和初始化的方式。	<br/>
 *	ReWorks操作系统为二进制信号量和计数信号量提供了初始化/销毁未命名的信号量、打开/关闭命名信号量的操作
 *接口和等待/发送信号量的操作接口，同时提供了用于获取与显示信号量对象相关信息的操作接口。	

 * 头文件：
 * 
 * #include <semaphore.h> \n
 * #include <sem_show.h>
 * @{ */
 
extern int max_sems;  //!< 信号量对象的最大数目
typedef unsigned long sem_t;    //!< 信号量ID数据类型

#define SEM_VALUE_MAX   1024    //!< 信号量的最大取值
#define SEM_BINARY	  1	//!<信号量类型——二进制信号量
#define SEM_COUNTING  2 //!<信号量类型——计数信号量
#define SEM_FAILED ((sem_t *)-1) //!< 错误的信号量ID

/**
 * @brief 初始化系统信号量模块
 *
 * 该接口初始化系统的信号量模块，主要用于系统配置。
 *
 * @param	sems_num 	系统支持的信号量对象的数目
 * 
 * @return	0	函数执行成功
 * @return 	-1	函数执行失败
 *
 * @exception EINVAL 参数sems_num取值不合法
 * @exception EMINITED 信号量模块已被初始化
 */
int sem_module_init(int sems_num);

/**
 * @brief 初始化未命名的计数信号量
 *
 * 该接口用于初始化未命名 的计数信号量，信号量的初始值为 value。在成功调用
 * sem_init()后， 信号量可以在调用接口 sem_wait()，sem_timedwait()，sem_trywait()，
 * sem_post()和 sem_destroy()时使用，直到信号量被销毁前都是可用的。 
 *
 * @param 	sem		  存放信号量对象的指针
 * @param 	pshared	  共享标志位，当前暂不支持
 * @param 	value	  信号量的初始值，最大取值为SEM_VALUE_MAX
 *
 * @return 	0 	函数执行成功，并完成信号量 sem 的初始化
 * @return 	-1 	函数执行失败，设置 errno 指出错误
 *
 * @exception EINVAL sem是无效指针， 或 value 的值超过了 SEM_VALUE_MAX 
 * @exception ENOSPC 用来初始化信号量的资源被耗尽了，或到达了信号量的上限SEM_NSEMS_MAX
 * @exception EMNOTINITED 信号量模块尚未初始化
 * @exception ECALLEDINISR 接口不能在中断上下文中调用
 *
 * @see sem_destroy()
 */
int sem_init(sem_t *sem, int pshared, unsigned int value);

/**
 * @brief 初始化未命名的二进制信号量/计数信号量
 *
 * 该接口用于初始化未命名的二进制信号量/计数信号量。在成功调用 sem_init2()后，信号量
 * 可以在调用接口 sem_wait()，sem_timedwait()，sem_trywait()，sem_post()和 sem_destroy()
 * 时使用，信号量被销毁前都是可用的。 
 *
 * @param 	sem		  存放信号量对象的指针
 * @param 	pshared	  共享标志位，当前暂不支持
 * @param	sem_type 信号量类型，取值包括SEM_BINARY和SEM_COUNTING. <br/>
 *				如果sem_type取值为SEM_BINARY，则表示信号量为二进制信号量; <br/>
 *				如果sem_type取值为SEM_COUNTING，则表示信号量为计数信号量
 * @param	waitq_type 等待信号量资源的任务排队策略，取值包括PTHREAD_WAITQ_FIFO（先进先出策略）和PTHREAD_WAITQ_PRIO（优先级策略）
 * @param 	value	  信号量的初始值，二进制信号量的初始值只能为0或1，计数信号量的初始值范围为[0,SEM_VALUE_MAX)
 *
 * @return 	0 	函数执行成功，并完成信号量 sem 的初始化
 * @return 	-1 	函数执行失败，设置 errno 指出错误
 *
 * @exception EINVAL sem是无效指针， 或 者sem_type, waitq_type或value 的取值不合法 
 * @exception ENOSPC 用来初始化信号量的资源被耗尽了，或到达了信号量的上限SEM_NSEMS_MAX
 * @exception EMNOTINITED 信号量模块尚未初始化 
 * @exception ECALLEDINISR 接口不能在中断上下文中调用
 *
 * @see sem_destroy()
 */
int sem_init2(sem_t *sem, int pshared, int sem_type, WaitQ_Type waitq_type, unsigned int value);

/**
 * @brief 销毁未命名的信号量
 *
 * 该接口用于销毁 sem 指定的未命名信号量。只有用sem_init()和sem_init2()接口创建的信
 *号量才能用这个函数来销毁。在当前没有阻塞任务的情况下，使用 sem_destroy()是安全的。
 * 如果要销毁一个当前有任务阻塞的信号量，或者销毁由sem_open()创建的信号量，都会返回错误。
 *
 * @param	sem	信号量标识
 *
 * @return 	0 	函数执行成功
 * @return 	-1 	函数执行失败，设置 errno 指出错误。
 *
 * @exception EINVAL sem不是一个合法的信号量 ，或者sem是由sem_open()/sem_b_open()创建的信号量
 * @exception EBUSY  sem指定的信号量上有任务阻塞
 * @exception EMNOTINITED 信号量模块尚未初始化
 * @exception ECALLEDINISR 接口不能在中断上下文中调用
 *
 * @attention 该接口不能用于关闭由sem_open()/sem_b_open()接口创建的命名信号量
 */
int sem_destroy(sem_t *sem);

/**
 * @brief 创建或打开命名的计数信号量
 *
 * 该接口用于创建或打开命名的计数信号量。成功调用该接口之后，任务可以通过返回的信号
 * 量ID来引用name关联的信号量，在 sem_wait()、sem_timedwait()、sem_trywait()、sem_post()
 * 和sem_close()接口的调用中使用。 
 *
 * @param	name	 信号量对象的名字，用相同的name值调用sem_open()的任务会引用相同的信号量对象。
 * @param	oflag	 信号量对象的标志位，决定sem_open()是要创建还是仅仅访问这个信号量，oflag的取值包括： <br/> 
    	 		O_CREAT 表示在信号量不存在时创建信号量。如果设置了O_CREAT标志且信号量已存在，那么 
       			O_CREAT将不起作用，除非设置了O_EXCL标志；否则，sem_open( )会创建一个命名的信号量。 <br/> 
     			O_EXCL 在设置了 O_EXCL 和 O_CREAT的情况下，如果name指定名称的信号量存在的话，sem_open()
       			函数调用会失败。如果信号量不存在，检测信号量存在的操作和创建信号量的操作是原子性的，
       			以免其他任务以O_EXCL 和 O_CREAT的标志位来执行 sem_open() 。<br/> 
       		... 	创建信号量还需要四个附加参数：<br/>
       			mode	类型为mode_t，表示信号量的权限位，本系统中忽略该值； <br/>
       			value	类型为unsigned，表示信号量创建时的初始值 ，合法的计数信号量初始值要小于等于SEM_VALUE_MAX ，合法的二进制信号量初始值只能为0或1；<br/>
       			sem_type	类型为int，表示信号量的类型，取值包括 SEM_BINARY（二进制信号量）和SEM_COUNTING（计数信号量）；<br/>
       			waitq_type	类型为WaitQ_Type，表示等待信号量资源的任务的排队策略，取值包括 PTHREAD_WAITQ_FIFO（先进先出策略）和PTHREAD_WAITQ_PRIO（优先级策略）。 <br/>
 *
 * @return 	信号量ID		函数执行成功，返回信号量的地址
 * @return 	SEM_FAILED	函数执行失败，并设定 errno 来指出错误
 *
 * @exception EEXIST	设置了O_CREAT 和 O_EXCL标志，但name命名的信号量已存在
 * @exception EINVAL  	name是无效指针，或是oflag中指定了O_CREAT标志且 value大于 SEM_VALUE_MAX
 * @exception ENAMETOOLONG  name 的长度超过了 NAME_MAX
 * @exception ENOENT  	name命名的信号量不存在且没有设置O_CREAT标志
 * @exception ENOSPC	用来初始化信号量的资源被耗尽了，或到达了信号量的上限SEM_NSEMS_MAX 
 * @exception EMNOTINITED 信号量模块尚未初始化
 * @exception ECALLEDINISR 接口不能在中断上下文中创建新的信号量
 *
 * @attention 该接口创建的是命名的计数信号量，其初始值必须小于等于SEM_VALUE_MAX。
 * 
 * @see sem_b_open()
 * @see sem_close()
 * @see sem_unlink()
 */
#ifdef __sparcv8__
sem_t *sem_open(const char *name, int oflag, mode_t mode,u32 value,int sem_type, WaitQ_Type waitq_type);
#else
sem_t *sem_open(const char *name, int oflag, ...);
#endif
/**
 * @brief 关闭命名的信号量
 *
 * 该接口表示调用任务已经完成对sem所指定的信号量的使用，并关闭该命名信号量。如果信号量还
 *没有调用 sem_unlink()移除，那么sem_close()对信号量的状态没有影响。如果信号量在以O_CREAT
 *调用sem_open()后成功调用sem_unlink()，那么当所有打开该信号量的任务关闭它后，该信号量就不能被访问了。
 *
 * @param	sem		信号量标识
 *
 * @return 	0 	函数执行成功
 * @return 	-1 	函数执行失败，设置 errno 指出错误。
 *
 * @exception EINVAL sem不是一个合法的信号量标识符，或者sem指定的信号量是未命名的信号量
 * @exception EMNOTINITED 信号量模块尚未初始化
 * @exception ECALLEDINISR 不能在中断上下文中关闭信号量
 * 
 * @attention 该接口不能用于关闭由sem_init()/sem_init2()接口创建的未命名信号量，否则返回错误。
 * 
 * @see sem_open()
 */
int sem_close(sem_t *sem);

/**
 * @brief 删除命名的信号量
 *
 * 该接口用于删除参数name指定的信号量。如果该信号量当前还被其他任务引用，那么sem_unlink()
 *不会影响信号量的状态，其析构操作会推迟，直到所有到该信号量的引用都通过调用 sem_close()
 *关闭。在所有引用被关闭前，sem_unlink()调用不会被阻塞，它会立即返回。
 *
 * @param 	name	信号量名称
 *
 * @return 	0 	函数执行成功
 * @return 	-1 	函数执行失败，不会改变信号量，设置 errno 指出错误。
 *
 * @exception EINVAL	name是无效指针
 * @exception ENAMETOOLONG 变量 name 的长度大于 NAME_MAX 
 * @exception ENOENT	 命名的信号量不存在
 * @exception EMNOTINITED 信号量模块尚未初始化
 * @exception ECALLEDINISR 不能在中断上下文中删除信号量
 *
 */
int sem_unlink(const char *name);

/**
 * @brief 锁定信号量
 *
 * 该接口用来锁定sem指定的信号量。如果当前信号量的值为0，那么调用任务不会从sem_wait()中返回，
 *除非它成功锁定信号量或是被信号中断。如果该接口调用成功，信号量状态变为锁定状态，且保持到sem_post() 
 *接口被成功调用。 
 *
 * @param	sem		信号量标识
 *
 * @return 	0		调用任务在 sem指定的信号量上成功执行了信号量锁定操作
 * @return 	-1		函数执行失败，设置 errno 指出错误，信号量的状态不发生改变。
 *
 * @exception EINTR  函数被一个信号中断
 * @exception EINVAL sem指定的信号量不合法 
 * @exception EOBJDELETED 因信号量对象被删除而终止阻塞等待
 * @exception EMNOTINITED 信号量模块尚未初始化
 * @exception ECALLEDINISR 接口不能在中断上下文中调用
 * @exception ECANCELED 通过sem_flush解除的锁定
 *
 * @see sem_trywait()
 * @see sem_timedwait()
 * @see sem_post()
 */
int sem_wait(sem_t *sem);

/**
 * @brief 尝试锁定信号量
 *
 * 该接口会尝试在信号量sem上执行锁定操作，只有在信号量未被锁定，即信号量的取值为正数的情况下，
 *该接口才能成功锁定信号量，否则该接口将无法锁定信号量。该函数不会阻塞，若获取不到信号量，则直接返回。
 *
 * @param	sem		信号量标识
 *
 * @return 	0 		调用任务在 sem 指定的信号量上成功执行了信号量锁定操作
 * @return 	-1 		函数执行失败，设置 errno 指出错误。信号量的状态不发生改变。
 *
 * @exception EINTR  函数被一个信号中断
 * @exception EINVAL sem指定的信号量不合法
 * @exception EAGAIN 无法锁定信号量
 * @exception EMNOTINITED 信号量模块尚未初始化
 * @exception ECALLEDINISR 接口不能在中断上下文中调用
 * 
 * @see sem_wait() 
 * @see sem_timedwait()
 * @see sem_post()
 */
int sem_trywait(sem_t *sem);

/**
 * @brief 限时锁定信号量
 *
 * 该接口的功能与 sem_wait()接口类似，都是用来锁定sem引用的信号量，但如果在指定的限时
 *时间内没有其他任务执行sem_post()对信号量解锁的话，那么该接口将终止等待，返回错误。
 *如果任务等待的时间超过abs_timeout指定的绝对时间，或者调用接口时已经超过了abs_timeout
 *指定的绝对时间，就会发生超时。如果信号量可以被立即锁定的话，接口调用就不会因超时而失败。
 *如果信号量可以被立即锁定的话，不会检测 sem_timedwait的有效性。
 *
 * @param	sem		信号量标识
 * @param	abs_timeout	指定的绝对时间
 *
 * @return 	0 		调用任务在 sem 指定的信号量上成功执行锁定操作
 * @return 	-1 		函数执行失败，设置 errno 指出错误，信号量的状态不会变化
 * 
 * @exception EINVAL 	sem指定的信号量不合法，或者任务被阻塞的情况下 abs_timeout指定了一个小于0或大于等于1'000'000'000的纳秒值 
 * @exception ETIMEDOUT	在指定超时到期前不能锁定信号量
 * @exception EINTR  	函数被一个信号中断
 * @exception EOBJDELETED 因信号量对象被删除而终止阻塞等待
 * @exception EMNOTINITED 信号量模块尚未初始化
 * @exception ECALLEDINISR 接口不能在中断上下文中调用
 * @exception ECANCELED 通过sem_flush解除的锁定
 * 
 * @see sem_wait() 
 * @see sem_trywait()
 * @see sem_post()
 * 
 * @attention 该接口的参数abs_timeout指定的是阻塞等待信号量的一个绝对时间点，
 * 因此通过date()或clock_settime()接口修改日期/时间时会影响线程的等待时间，
 * 特别是将时间往前调整的情况，有可能会导致程序长时间得不到响应。
 */
int sem_timedwait(sem_t *sem, const struct timespec *abs_timeout);

/**
 * @brief 锁定信号量
 *
 * 该接口用来锁定sem引用的信号量，并通过参数is_wait和timeout来设置是否阻塞等待。如果参数is_wait
 *的取值为WAIT，即任务会阻塞等待信号量被解锁，那么timedout就用来设置任务等待的超时时间，
 *此时，若将timedout设为NO_TIMEOUT，就表示任务会一直阻塞等待直到互斥量被解锁，设为其他值，
 *则表示任务只会阻塞等待timedout设置的时间间隔；如果参数is_wait取值为NO_WAIT，那么任务
 *在没有锁定互斥量的情况下会立即返回，而不会阻塞。
 *
 * @param	sem		信号量标识
 * @param 	is_wait 等待标志，取值包括WAIT 和 NO_WAIT
 * @param	timeout	等待时间，单位是tick
 *
 * @return 	0 		调用任务在 sem 指定的信号量上成功执行锁定操作
 * @return 	-1 		函数执行失败，设置 errno 指出错误，信号量的状态不会变化
 * 
 * @exception EINVAL 	sem指定的信号量不合法，或者is_wait取值不合法
 * @exception ETIMEDOUT	在指定超时到期前不能锁定信号量
 * @exception EAGAIN 	is_wait取值为NO_WAIT的情况下，无法锁定信号量
 * @exception EINTR  	函数被一个信号中断
 * @exception EOBJDELETED 因信号量对象被删除而终止阻塞等待
 * @exception EMNOTINITED 信号量模块尚未初始化
 * @exception ECALLEDINISR 接口不能在中断上下文中调用
 * @exception ECANCELED 通过sem_flush解除的锁定
 * 
 * @see sem_wait() 
 * @see sem_trywait()
 * @see sem_timedwait()
 * @see sem_post()
 * 
 * @attention 该接口的参数timeout指定的是阻塞等待信号量的时间间隔，而非一个绝对时间点，
 * 因此通过date()或clock_settime()接口修改日期/时间时并不会影响线程的等待时间。
 */
int sem_wait2(sem_t *sem, int is_wait, unsigned int timeout);

/**
 * @brief 解除锁定信号量
 *
 * 该接口用来解锁sem指定的信号量。 如果从该接口的操作结果中得到的信号量的值是正数，
 *就表示没有任务在阻塞等待该信号量解锁，信号量的值只需要简单地递增；如果从该接口的
 *操作结果中得到的信号量的值是0，那么其中一个阻塞等待该信号量的任务将成功从sem_wait()
 *接口调用中返回。阻塞在信号量上的任务按优先级策略解除阻塞，即解除任务的阻塞时会选择
 *最高优先级且等待时间最长的任务。
 *
 * @param	sem	  	信号量标识
 *
 * @return 	0 		函数执行成功
 * @return 	-1 		函数执行失败，设置 errno 指出错误
 *
 * @exception EINVAL sem指定的信号量不合法 
 * @exception EMNOTINITED 信号量模块尚未初始化
 *
 * @attention 阻塞在信号量上的任务按照优先级策略解除阻塞。
 * 
 * @see sem_wait() 
 * @see sem_trywait()
 * @see sem_timedwait()
 */
int sem_post(sem_t *sem);

/** 
 * @brief 将所有阻塞在这个信号量上的任务解除阻塞。
 *
 * 该接口将所有阻塞在sem指定的信号量上的任务解除阻塞，这些解除阻塞的任务全部进入就绪队列，
 * 此信号量的状态不会被改变。此外，那些导致任务阻塞的接口sem_wait/sem_wait2/sem_timedwait
 * 在通过sem_flush解除阻塞后的返回值将为-1，错误号设为ECANCELED，用于区分其与正常获取信号量的情况。
 * 在同步应用中，sem_flush可以用来进行广播。
 * 
 * @param	sem	  	信号量标识
 * 
 * @return 	0 		函数执行成功
 * @return  -1 		函数执行失败
 * 
 * @exception  EINVAL sem指定的信号量不合法 
 * @exception EMNOTINITED 信号量模块尚未初始化
 * 
 * @see sem_wait() 
 * @see sem_trywait()
 * @see sem_timedwait()
 * @see sem_post()
 */
int sem_flush(sem_t *sem);

/**
 * @brief 获取信号量的值
 *
 * 该接口用来获取sem指定的信号量的值，并存放在参数sval指定的地址中，该接口并不影响
 *信号量的状态。如果 sem 被锁定了，那么 sval 指向的对象会被设为0或负数，如果是负数，
 *其绝对值等于正在等待信号量的任务数目。
 *
 * @param 	sem		信号量标识
 * @param	sval	存放信号量的值的指针
 *
 * @return 	0 		函数执行成功
 * @return 	-1 		函数执行失败，设置 errno 指出错误。
 *
 * @exception EINVAL sem指定的信号量不合法，或者sval是无效指针
 * @exception EMNOTINITED 信号量模块尚未初始化 
 *
 */
int sem_getvalue(sem_t *sem, int *sval);

/** @example example_sem_init.c
 * 下面的例子演示了如何使用sem_init().
 */
 
 /** @example example_sem_init2.c
 * 下面的例子演示了如何使用sem_init2().
 */
 
 /** @example example_sem_destroy.c
 * 下面的例子演示了如何使用sem_destroy().
 */
 
  /** @example example_sem_open.c
 * 下面的例子演示了如何使用sem_open().
 */
  
 /** @example example_sem_close.c
 * 下面的例子演示了如何使用sem_close().
 */
 /** @example example_sem_unlink.c
 * 下面的例子演示了如何使用sem_unlink().
 */
 
 /** @example example_sem_wait.c
 * 下面的例子演示了如何使用sem_wait().
 */
 
 /** @example example_sem_trywait.c
 * 下面的例子演示了如何使用sem_trywait().
 */
 
 /** @example example_sem_timedwait.c
 * 下面的例子演示了如何使用sem_timedwait().
 */
 
 /** @example example_sem_wait2.c
 * 下面的例子演示了如何使用sem_wait2().
 */
 
 /** @example example_sem_post.c
 * 下面的例子演示了如何使用sem_post().
 */
 
 
 /** @example example_sem_flush.c
 * 下面的例子演示了如何使用sem_flush().
 */
 
 /** @example example_sem_getvalue.c
 * 下面的例子演示了如何使用sem_getvalue().
 */


/* @}*/

#ifdef __cplusplus
}

#endif 

#endif 


