/*! @file wdg_show.h
    @brief ReWorks看门狗定时器信息获取/显示头文件
    
 * 本文件定义了看门狗定时器对象相关信息的获取/显示操作接口。
 *
 * @version 4.7
 * 
 * @see 无
 */
#ifndef _REWORKS_WDGSHOW_H_
#define _REWORKS_WDGSHOW_H_

#ifdef __cplusplus
extern "C"
{
#endif 

#include <wdg.h>

/** 
 * @addtogroup group_timer
 * @{ */	
 
/*!\struct wdg_info
 * \brief 看门狗定时器信息结构定义
 *
 * 该数据结构定义了看门狗定时器对象的状态信息和到期时间。
 */
typedef struct wdg_info
{
	unsigned int wdg_status;	//!< 看门狗状态 ，取值包括WDOG_INACTIVE、WDOG_IN_START、WDOG_ACTIVE
	unsigned int wdg_interval;	//!< 看门狗到期时间
} wdg_info_t; //!< 看门狗定时器信息结构定义

/**
 * @brief    获取看门狗定时器的概要信息
 * 
 * 该接口用来获取wdg_id指定的定时器的概要信息，包括定时器相关的信号信息、看门狗ID、
 *时钟ID、任务ID、定时器状态、定时器启动信息等。
 *
 * @param    wdg_id 	定时器ID
 * @param    wdg_info 存放返回的定时器信息的结构地址
 *
 * @return    0		函数执行成功
 * @return    -1	函数执行失败
 *
 * @exception EINVAL  定时器标识wdg_id无效，或者wdg_info是无效指针
 * @exception EMNOTINITED 看门狗模块尚未初始化
 *
 */
int wdg_getinfo(wdg_t wdg_id, wdg_info_t *wdg_info);


/**
 * @brief     显示看门狗定时器信息
 * 
 * 该接口用来显示定时器的信息。
 * 
 * @param    wdg_id	定时器ID
 *
 * @return    0		函数执行成功
 * @return    -1	函数执行失败
 *
 * @exception EINVAL  定时器标识wdg_id无效
 * @exception EMNOTINITED 看门狗模块尚未初始化
 *
 */
int wdg_show(wdg_t wdg_id);

/** @example example_wdg_getinfo.c
 * 下面的例子演示了如何使用 wdg_getinfo().
 */
 
 /** @example example_wdg_show.c
 * 下面的例子演示了如何使用 wdg_show().
 */
/* @}*/

#ifdef __cplusplus
}
#endif 
	
#endif
