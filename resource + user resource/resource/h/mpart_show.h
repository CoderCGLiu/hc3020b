/*! @file mpart_show.h
    @brief ReWorks内存分区信息获取/显示头文件
    
 * 本文件定义了内存分区相关信息的获取/显示操作接口。
 *
 * @version 4.7
 * 
 * @see 无
 */

#ifndef _REWORKS_MPARTSHOW_H_
#define _REWORKS_MPARTSHOW_H_

#ifdef __cplusplus
extern "C"
{
#endif
	
#include <mpart.h>

/**  
 * @addtogroup group_mpart
 * @{ */	

/*! \struct partinfo
 * \brief 
 *
 * 该数据结构定义了内存分区的内存信息，包括内存使用量和内存总数。
 */
typedef struct partinfo 
{
	/* !!-- 改为size_t --!! */
	size_t bytes_allocated; //!< 内存分区的内存使用量 
	size_t bytes_total;	//!< 内存分区的内存总数
} partinfo_t; //!< 内存分区信息结构定义

/**  
 * @brief    获取内存分区使用信息
 * 
 * 该接口获取内存分区的使用信息，包括内存分区中总的内存大小和已分配的内存大小。 
 *
 * @param   mid	内存分区标识
 * @param   pi  存放内存信息的指针
 *
 * @return  0	函数执行成功，并将内存信息存放在pi中
 * @return	-1	函数执行失败
 *
 * @exception EINVAL 参数pi是无效指针，或者mid指定的内存分区不存在
 * @exception EMNOTINITED 内存分区模块尚未初始化
 *
 */
int mpart_getinfo(mpart_id mid, struct partinfo *pi);

/* @}*/

#ifdef __cplusplus
}
#endif 

#endif
