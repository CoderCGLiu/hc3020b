/*! @file wdg.h
    @brief ReWorks看门狗操作的头文件
    
 * 本文件定义了看门狗定时器相关的操作接口。
 * 			
 * @version 4.7
 * 
 * @see 无
 */

#ifndef _REWORKS_WATCHDOG_H_
#define _REWORKS_WATCHDOG_H_

#ifdef __cplusplus
extern "C"
{
#endif 

#include <reworks/types.h>

/**  
 * @addtogroup group_timer
 *	看门狗定时器（Watchdog Timer）通过将一个特定的时间间隔与一个C调用过程联系起来，
 *提供了指定程序精确定时启动的功能，即操作系统在确定的时间延迟（以时钟滴答ticks为
 *单位）后以中断级别执行看门狗定时器指定的程序。 <br/>
 *	ReWorks操作系统为看门狗定时器的创建、启动、取消、删除提供了相应的操作接口，
 *同时提供了用于获取与显示看门狗对象相关信息的操作接口。

 * 头文件：
 *
 * #include <wdg.h>  <br/>
 * #include <wdg_show.h> 
 *
 *  @{ */	
 	
/**
 * 看门狗状态定义
 */
#define WDOG_INACTIVE   0x0  //!< 看门狗处于无效状态
#define WDOG_IN_START   0x1  //!< 看门狗处于启动状态
#define WDOG_ACTIVE     0x2  //!< 看门狗处于活动状态

/**
 * @brief    初始化看门狗模块
 * 
 * 该接口初始化系统的看门狗模块，主要用于系统配置。
 *
 * @param    max_wdgs 系统中看门狗的最大数量    
 *
 * @return	0	函数执行成功
 * @return 	-1	函数执行失败
 *
 * @exception EINVAL 参数max_wdgs取值不合法
 * @exception EMINITED 看门狗模块已被初始化
 */
int wdg_module_init(int max_wdgs);

/** 
 * @brief 创建看门狗
 * 
 * wdg_create()接口用于创建一个看门狗，并将看门狗ID通过参数wdg_id输出。看门狗创建完成后，
 *将处于WDOG_INACTIVE状态。
 * 
 * @param 	wdg_id 指向新建看门狗的ID的指针
 * 
 * @return 	0 	创建看门狗成功
 * @return 	-1 	创建看门狗失败
 * 
 * @exception  EAGAIN  系统中没有足够的看门狗资源
 * @exception  EMNOTINITED 看门狗模块尚未初始化
 * @exception  ECALLEDINISR 该接口不能在中断上下文中调用
 * 
 * @see wdg_delete()
 */
int wdg_create(wdg_t *wdg_id);

/** 
 * @brief 启动看门狗
 * 
 * 该接口用于启动wdg_id指定的看门狗，在ticks指定的时间到达后，看门狗服务程序将从中断级被调用。
 *可以多次使用同一个看门狗ID来调用wdg_start()，以更新延迟的ticks数或将要执行的服务程序，
 *对于同一个看门狗，只有最近一次wdg_start()调用有效。如果要在指定的ticks计数到达前取消计时器，
 *可以调用wdg_cancel()。如果应用程序需要周期性的执行，看门狗服务程序可以周期结束之前调用
 *wdg_start()来重启看门狗。看门狗服务程序服从所有的中断服务程序的约束。
 * 
 * @param 	wdg_id	看门狗ID
 * @param 	ticks	看门狗的延迟时间，应为正值
 * @param 	func	看门狗触发时的执行函数
 * @param 	arg 	执行函数的参数
 * 
 * @return	0	启动看门狗成功
 * @return 	-1	启动看门狗失败
 * 
 * @exception EINVAL wdg_id指定的看门狗不存在，或者ticks小于零
 * @exception EMNOTINITED 看门狗模块尚未初始化
 * 
 * @see wdg_cancel()
 */
int wdg_start(wdg_t wdg_id, int ticks, void(*func)(void *arg), void *arg);

/** 
 * @brief 取消看门狗
 * 
 * 该接口通过将看门狗的延迟计数归零来取消wdg_id指定的看门狗。如果看门狗被成功取消，
 *则看门狗的状态将转换为WDOG_INACTIVE状态。
 * 
 * @param 	wdg_id 	看门狗ID
 * 
 * @return 	0 	取消看门狗成功
 * @return 	-1 	取消看门狗失败
 * 
 * @exception EINVAL wdg_id指定的看门狗不存在
 * @exception EMNOTINITED 看门狗模块尚未初始化
 * 
 * @see wdg_start()
 */
int wdg_cancel(wdg_t wdg_id);

/** 
 * @brief 删除看门狗
 * 
 * 该接口用来删除wdg_id指定的看门狗。如果看门狗已经被启动，它将被先取消再删除。
 * 
 * @param 	wdg_id 	看门狗ID
 * 
 * @return 	0 	删除看门狗成功
 * @return 	-1 	删除看门狗失败
 * 
 * @exception EINVAL wdg_id指定的看门狗不存在
 * @exception EMNOTINITED 看门狗模块尚未初始化
 * @exception ECALLEDINISR 该接口不能在中断上下文中调用
 * 
 * @see wdg_create()
 */
int wdg_delete(wdg_t wdg_id);


/** @example example_wdg_create.c
 * 下面的例子演示了如何使用 wdg_create().
 */
 
 /** @example example_wdg_start.c
 * 下面的例子演示了如何使用 wdg_start().
 */ 
 
 /** @example example_wdg_cancel.c
 * 下面的例子演示了如何使用 wdg_cancel().
 */
 
 /** @example example_wdg_delete.c
 * 下面的例子演示了如何使用 wdg_delete().
 */
/* @}*/

#ifdef __cplusplus
}

#endif 

#endif
