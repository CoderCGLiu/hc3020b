#ifndef __REWORKS_LOG4RE_H__
#define __REWORKS_LOG4RE_H__
#ifdef __cplusplus
extern "C"
{
#endif

#include <logger/log.h>

extern void (*log_message_ptr)(struct logger *logger, char *func, int line, struct log_level *level, char *message, ...);

/*log模块struct logger的指针 */
extern struct logger *logger_isr;
extern struct logger *logger_mutex;
extern struct logger *logger_sched;
extern struct logger *logger_kernel;
extern struct logger *logger_mmu;
extern struct logger *logger_csp;
extern struct logger *logger_bsp;
extern struct logger *logger_io;
extern struct logger *logger_fs;
extern struct logger *logger_ipnet;

/*log级别对象 */
extern struct log_level log_level_off;
extern struct log_level log_level_fatal;
extern struct log_level log_level_error;
extern struct log_level log_level_warn;
extern struct log_level log_level_info;
extern struct log_level log_level_debug;
extern struct log_level log_level_trace;
extern struct log_level log_level_all;

#define LOG_MSG_FATAL(logger, msg, ...)  if (log_message_ptr) log_message_ptr(logger, __FUNCTION__, __LINE__, &log_level_fatal, msg, ##__VA_ARGS__)
#define LOG_MSG_ERROR(logger, msg, ...)  if (log_message_ptr) log_message_ptr(logger, __FUNCTION__, __LINE__, &log_level_error, msg, ##__VA_ARGS__)
#define LOG_MSG_WARN(logger, msg, ...)   if (log_message_ptr) log_message_ptr(logger, __FUNCTION__, __LINE__, &log_level_warn, msg, ##__VA_ARGS__)
#define LOG_MSG_INFO(logger, msg, ...)   if (log_message_ptr) log_message_ptr(logger, __FUNCTION__, __LINE__, &log_level_info, msg, ##__VA_ARGS__)
#define LOG_MSG_DEBUG(logger, msg, ...)  if (log_message_ptr) log_message_ptr(logger, __FUNCTION__, __LINE__, &log_level_debug, msg, ##__VA_ARGS__)
#define LOG_MSG_TRACE(logger, msg, ...)  if (log_message_ptr) log_message_ptr(logger, __FUNCTION__, __LINE__, &log_level_trace, msg, ##__VA_ARGS__)

#define LOG_MSG(level, logger, msg, ...) if (log_message_ptr) log_message_ptr(logger, __FUNCTION__, __LINE__, level, msg,##__VA_ARGS__)

/**
 * @brief  系统日志功能初始化
 * 
 * 该接口用于向任务控制块注册事件相关的域，一般由系统在初始化时调用。
 *
 * @param	options		日志配置模块，取值包括：<\br>
 * 			- LOG_KERNEL，核心模块
 * 			- LOG_ISR，中断模块
 * 			- LOG_SCHED，调度模块
 * 			- LOG_MUTEX，互斥量模块
 * 			- LOG_MMU，MMU模块
 * 			- LOG_CSP，CSP模块
 * 			- LOG_BSP，BSP模块
 * 			- LOG_IO，IO模块
 * 			- LOG_FS，文件系统模块
 * 			- LOG_IPNET，网络协议栈模块
 * 			- LOG_ALL，所有模块
 * @param	priority	日志输出优先级，取值包括以下取值（其对应的日志优先级依次降低）：<\br>
 * 			- LOG_LEVEL_OFF，关闭日志输出
 * 			- LOG_LEVEL_FATAL，致命级日志
 * 			- LOG_LEVEL_ERROR，错误级日志
 * 			- LOG_LEVEL_WARN，警告级日志
 * 			- LOG_LEVEL_INFO，信息级日志
 * 			- LOG_LEVEL_DEBUG，调试级日志
 * 			- LOG_LEVEL_TRACE，跟踪级日志
 * 			- LOG_LEVEL_ALL，所有级别日志
 * @param	out_schema	日志输出方式，取值包括：
 * 			- STDOUT_SCHEMA，日志直接输出到终端
 * 			- KLOG_SCHEMA，日志缓存于内存，通过调用logger_show()接口输出
 *	
 * @return	0 	函数执行成功
 * @return 	-1	函数执行失败
 * 
 * @exception EINVAL 参数取值错误
 * 
 * @attention 该模块可重复配置。
 * @attention 参数priority用于配置日志输出优先级，取值列表为优先级由高到低排序，日志记录输出的是所有等于和高于配置优先级的日志信息。
 **/
extern int log4re_module_init(int options, int priority, int out_schema);

#ifdef __cplusplus
}
#endif
#endif /*__REWORKS_LOG4RE_H__*/

