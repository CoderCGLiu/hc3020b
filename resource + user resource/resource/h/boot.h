#ifndef _REWORKS_BOOT_H_
#define _REWORKS_BOOT_H_

#ifdef __cplusplus
extern "C"
{
#endif

/*==============================================================================
 * 
 * ReWorks启动时的模块初始化接口
 * 
 * 输入：
 * 		无
 * 输出：
 * 		无
 * 返回：
 * 		成功返回OS_OK；失败返回OS_ERROR
 */
/* BSP模块初始化 */
int bsp_module_init(void);

/* CSP模块初始化 */
int csp_module_init(void);

/* RTC模块初始化 */
int rtc_module_init(void);

/* 浮点模块初始化 */
int fpu_module_init(void);

/* 中断模块初始化 */
int int_module_init(void);

/* 异常模块初始化 */
int exception_module_init(void);

/* 时钟模块初始化 */
int clock_module_init(void);

/* CPU检测接口 */
int cpu_probe(void);

/*==============================================================================
 * 
 * 自引导工程模板接口
 * 
 */
/* 系统配置初始化 */
void sysConfigInition(void);

/* 用户配置初始化 */
void userConfigInition(void);

/*==============================================================================
 * 
 * ReWorks启动时所需的其它接口
 * 
 */
/* 时钟启动 */
void sys_clock_start(void);

/* 时钟响应 */
int sys_clock_ack(void);

/* 时钟关闭 */
void sys_clock_off(void);

/* 设置时钟速率 */
u32 sys_clock_rate_set(int ticks_per_second);

#ifdef __cplusplus
}
#endif

#endif
