
/*! @file cpuset.h
    @brief ReWorks系统表示CPU集合文件
     
 * 本文件定义了ReWorks系统CPU集合与操作。
 * 
 * @version @reworks_version
 * 
 * @see 无
 */
#ifndef _CPUSET_H_
#define _CPUSET_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <reworks/types.h>
#include <bitops.h>

/**
 * @addtogroup group_multi_task
 * @{*/

typedef unsigned long cpuset_t; //!< cpu集合类型

#define CPUSET_ATOMICCOPY(cpusetDst, cpusetSrc)				   \
	(atomic_set ((atomic_t *) &cpusetDst, (atomic_t) cpusetSrc))    //!< cpu集合拷贝的原子操作
#define CPUSET_ATOMICSET(cpuset, n)					    \
	(atomic_or ((atomic_t *) &cpuset, (1UL) << (n)))                      //!< cpu集合设置的原子操作
#define CPUSET_ATOMICCLR(cpuset, n)					    \
	(atomic_and ((atomic_t *) &cpuset, ~((1UL) << (n))))                  //!< cpu集合清除的原子操作

#define CPUSET_ISZERO(cpuset)	      (cpuset == 0 ? TRUE : FALSE)	//!< cpu集合是否为空
#define CPUSET_SET(cpuset, n)         (cpuset |= ((1UL) << (n)))          //!< 设置核n至cpu集合
#define CPUSET_ISSET(cpuset, n)       ((cpuset) & ((1UL) << (n)))           //!< 检测cpu集合中是否设置了核n
#define CPUSET_CLR(cpuset, n)         (cpuset &= ~(((1UL) << (n))))         //!< 清除cpu集合的核n设置
#define CPUSET_ZERO(cpuset)           (cpuset = 0)                  //!< 将cpu集合置为空

/** 
 * @brief  设置cpu集合的首位
 * 
 * @param 	cpuset cpu集合
 * 
 * @return 	0 成功
 * @return 	-1失败
 * 			
 */
static inline int CPUSET_FIRST_INDEX(cpuset_t cpuset){
	register u16 ret; //lijuan 此处需以u16定义变量，否则x86平台下ffs16()会出错
	if(cpuset == 0){
		return -1;
	}
	ffs16(cpuset,ret);
	return ret;
}

/** 
 * @brief  获取当前SMP系统配置的CPU个数
 * 
 * @param 	无
 * 
 * @return 	CPU个数   函数执行成功
 * @return 	-1	          函数执行失败
 * 			
 */
extern int num_online_cpus(void);

/** 
 * @brief  获取SMP系统当前运行CPU的cpu集合
 * 
 * @param 	无
 * 
 * @return 	CPU集合   函数执行成功
 * @return 	-1	          函数执行失败
 * 			
 */
extern cpuset_t get_up_cpuset();


/** 
 * @brief  获取SMP系统当前运行CPU的ID
 * 
 * @param 	无
 * 
 * @return 	CPU ID   函数执行成功
 * @return 	-1	          函数执行失败
 * 			
 */
extern int cpu_id_get();

/** 
 * @brief  判定指定CPU是否处于运行状态
 * 
 * @param 	cpu	CPU ID
 * 
 * @return 	1   CPU处于运行状态
 * @return 	0	CPU不处于运行状态
 * 			
 */
boolean cpu_isup(int cpu);

/* @}*/

#ifdef __cplusplus
}
#endif


#endif


