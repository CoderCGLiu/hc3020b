#ifdef __multi_core__
/*! @file fastmutex.h
    @brief ReWorks系统快速互斥量
     
 * 本文件定义了ReWorks系统快速互斥量的操作。
 * 
 * @version 4.7
 * 
 * @see 无
 */
#ifndef _FAST_MUTEX_H_
#define _FAST_MUTEX_H_


#ifdef __cplusplus
extern "C"
{
#endif 

/**
 * @{*/
typedef void* fastmutex_t; //!< 互斥量类型

/** 
 * @brief  快速互斥量创建
 * 
 * @param 	无
 * 
 * @return 	互斥量指针
 * 			
 */
fastmutex_t fastmutex_create();

/** 
 * @brief  获取快速互斥量
 * 
 * @param 	fmutex 互斥量指针
 * 
 * @return 	0 成功
 * @return 	-1失败
 * 			
 */
int fastmutex_take(fastmutex_t fmutex);

/** 
 * @brief  释放快速互斥量
 * 
 * @param 	fmutex 互斥量指针
 * 
 * @return 	0 成功
 * @return 	-1失败
 * 			
 */
int fastmutex_give(fastmutex_t fmutex);

/** 
 * @brief  销毁快速互斥量
 * 
 * @param 	fmutex 互斥量指针
 * 
 * @return 	0 成功
 * @return 	-1失败
 * 			
 */
int fastmutex_destroy(fastmutex_t fmutex);

/* @}*/
#ifdef __cplusplus
}

#endif 

#endif
#endif
