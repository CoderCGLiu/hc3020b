/*! @file show.h
    @brief ReWorks信息获取/显示头文件
    
 *
 * @version 4.7
 * 
 * @see 无
 */

#ifndef _REWORKS_SHOW_H_
#define _REWORKS_SHOW_H_

#ifdef __cplusplus
extern "C"
{
#endif

void show_module_init(void);
	
#ifdef __cplusplus
}
#endif

#endif
