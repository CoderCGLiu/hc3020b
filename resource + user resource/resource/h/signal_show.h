/*! @file signal_show.h
    @brief ReWorks信号信息获取/显示头文件
    
 * 本文件定义了信号相关信息的获取/显示操作接口。
 *
 * @version 4.7
 * 
 * @see 无
 */
#ifndef _REWORKS_SIGNALSHOW_H_
#define _REWORKS_SIGNALSHOW_H_
 
#ifdef __cplusplus
extern "C"
{
#endif

/**  
 * @addtogroup group_signal
 * @{ */
 
/*! \struct sig_info
 * \brief 信号信息结构定义
 *
 * 该数据结构定义了与调用任务相关的信号信息，包括在任务上阻塞、未决、忽略的信号集，
 *任务阻塞等待的信号集以及任务处理例程的执行状态等。
 */
typedef struct sig_info
{
	sigset_t	sigt_blocked;  //!< 阻塞的信号集
	sigset_t	sigt_killing;  //!< 被kill的信号集
	sigset_t	sigt_pending;  //!< 未决的信号集
	sigset_t	sigt_wait;     //!< 任务阻塞等待的信号集
	int 		in_sig_processing;  //!< 信号处理例程执行状态
} sig_info_t; //!< 信号信息结构定义

/**
* @brief  获取指定任务中信号的相关信息
* 
* 该接口用于获取指定任务信号的相关信息，并存放在sig_info所指向的数据结构中。
*
* @param	tid	指定的任务id
* @param	pinfo	存储信号信息的数据结构体指针
*
* @return	0	函数执行成功
* @return	-1	函数执行失败
* 
* @exception	ESRCH	tid指定的任务不存在
*
* @note	该函数只用于调试时使用，并不能保证输出结果的准确性和时效性。
*/
 extern int sig_getinfo(pthread_t tid, sig_info_t *pinfo);
 
/**
* @brief  打印输出指定任务中信号的相关信息
* 
* 该接口用于输出指定任务信号的相关信息
*
* @param	tid		指定的任务id
* @param	level	指定任务输出内容的层次
*
* @return 	无
* 
* @exception 无
*
* @note	该接口只用于调试时使用，并不能保证输出结果是当前准确的值。
*/
 extern void sig_show(pthread_t tid, int level);
 
 /** @example example_sig_getinfo.c
  * 下面的例子演示了如何使用接口 sig_getinfo( ).
  */
 /** @example example_sig_show.c
  * 下面的例子演示了如何使用接口 sig_show( ).
  */ 
 
/* @}*/

#ifdef __cplusplus
}
#endif


#endif
