/*! @file pthread_hook.h
    @brief ReWorks任务钩子函数头文件
    
*	本文件定义了任务钩子函数相关的操作接口。
*	
* @version 4.7
* 
* @see 无
*/
#ifndef _REWORKS_PTHREADHOOK_H_
#define _REWORKS_PTHREADHOOK_H_

#ifdef __cplusplus
extern "C" 
{
#endif
	
#include <reworks/thread.h>

/** @defgroup group_pthread_hook ReWorks任务钩子函数管理
 * @ingroup group_os_reworks
 *	ReWorks操作系统在关键的系统调用点（如任务创建、上下文切换和删除等）提供了钩子函数，
 *允许用户注入扩展函数，对系统的关键行为设定前置/后置处理函数，帮助用户观察系统行为并事后分析，
 *增强系统的功能。	
 
 * 头文件：
 *
 * #include <pthread_hook.h>
 * @{ */
 
/** 
 * @brief 添加任务创建的钩子函数
 * 
 * 该接口用于添加任务创建的钩子函数，指定的函数在任务创建时被调用，该函数执行失败将导致任务创建失败。
 * 
 * @param 	createHook 任务创建的钩子函数
 * 
 * @return 	0	 	添加钩子函数成功
 * @return	EINVAL	createHook为空，不是一个有效指针
 * @return 	EAGAIN	超过了系统限制的任务创建相关的钩子函数总数
 * 
 * @exception EINVAL 参数无效
 * @exception EAGAIN 系统资源不足
 * 
 * @see pthread_create_hook_delete()
 */	
int pthread_create_hook_add(int (* createHook)(thread_t));

/**
* @brief   删除任务创建的钩子函数
*
* 该接口用于从任务创建的钩子函数列表删除指定的钩子函数。 
*
* @param  	createHook 	要删除的钩子函数
*
* @return 	0		删除钩子函数成功 
* @return	EINVAL	createHook为空，不是一个有效指针，或者钩子函数表中找不到指定的钩子函数 
*
* @exception EINVAL 参数无效
*
* @see  pthread_create_hook_add() 
*/
int pthread_create_hook_delete(int (* createHook)(thread_t));

/**
* @brief   添加任务切换的钩子函数
*
* 该接口用于添加一个指定的钩子函数至任务切换的钩子函数列表。
*
* @param 	switchHook 	任务切换的钩子函数
* 
* @return   0		添加钩子函数成功 
* @return	EINVAL	switchHook为空，不是一个有效指针
* @return 	EAGAIN	超过了系统限制的任务切换相关的钩子函数总数
* 
* @exception EINVAL 参数无效
* @exception EAGAIN 系统资源不足
*
* @see  pthread_switch_hook_delete()
*/
int pthread_switch_hook_add(void (* switchHook)(thread_t, thread_t));

/**
* @brief   删除任务切换的钩子函数
*
* 该接口用于从任务切换的钩子函数列表删除指定的钩子函数。
*
* @param	switchHook 要删除的钩子函数 
* 
* @return 	0		删除钩子函数成功 
* @return	EINVAL	switchHook为空，不是一个有效指针，或者钩子函数表中找不到指定的钩子函数 
*
* @exception EINVAL 参数无效
*
* @see  pthread_switch_hook_add() 
*/
int pthread_switch_hook_delete( void (* switchHook)(thread_t, thread_t));

/**
* @brief   添加任务删除的钩子函数
*
* 该接口用于添加一个指定的钩子函数至任务删除的钩子函数列表。
* 
* @param	closeHook 	任务删除的钩子函数 
*
* @return   0	添加钩子函数成功
* @return	EINVAL	closeHook为空，不是一个有效指针
* @return 	EAGAIN	超过了系统限制的任务删除相关的钩子函数总数
* 
* @exception EINVAL 参数无效
* @exception EAGAIN 系统资源不足
*
* @see  pthread_close_hook_delete()
*/
int pthread_close_hook_add( void (* closeHook)(thread_t));

/**
* @brief   删除任务删除的钩子函数
*
* 该接口用于从任务删除的钩子函数列表删除指定的钩子函数。 
*
* @param   	closeHook 	要删除的钩子函数
* 
* @return 	0	删除钩子函数成功 
* @return	EINVAL	closeHook为空，不是一个有效指针，或者钩子函数表中找不到指定的钩子函数 
*
* @exception EINVAL 参数无效
*
* @see  pthread_close_hook_add()
*/
int pthread_close_hook_delete( void (* closeHook)(thread_t));



/** @example example_pthread_create_hook_add.c
 * 下面的例子演示了如何使用 pthread_create_hook_add().
 */ 

/** @example example_pthread_create_hook_delete.c
 * 下面的例子演示了如何使用 pthread_create_hook_delete().
 */
 
 /** @example example_pthread_switch_hook_add.c
 * 下面的例子演示了如何使用 pthread_switch_hook_add().
 */ 

/** @example example_pthread_switch_hook_delete.c
 * 下面的例子演示了如何使用 pthread_switch_hook_delete().
 */
 
 /** @example example_pthread_close_hook_add.c
 * 下面的例子演示了如何使用 pthread_close_hook_add().
 */ 

/** @example example_pthread_close_hook_delete.c
 * 下面的例子演示了如何使用 pthread_close_hook_delete().
 */
/* @}*/

#ifdef __cplusplus
}
#endif
#endif
