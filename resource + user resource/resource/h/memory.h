/*! @file memory.h
    @brief ReWorks内存管理头文件
    
 * 本文件定义了核心堆和用户堆内存相关的操作接口。
 *
 * @version 4.7
 * 
 * @see 无
 */
#ifndef _REWORKS_MEMORY_H_
#define _REWORKS_MEMORY_H_

#ifdef __cplusplus
extern "C"
{
#endif 

#include <reworks/types.h>

/** @defgroup group_memory ReWorks内存管理
 * @ingroup group_os_reworks
 *
 *	ReWorks操作系统核心按照最小碎片、最小管理负载和确定的分配时间的原则来进行内存管理，
 *主要是对核心堆内存和用户堆内存进行管理。核心堆内存的管理采用Buddy分配算法，
 *用户堆内存的管理则可以选择Buddy分配算法和First-Fit分配算法，用户可以在系统初始化选择。<br/>
 *	这一部分提供了分配、释放内存的相关操作接口，以及用于获取与显示内存使用情况的操作接口。
 
 * 头文件：
 *
 * #include <memory.h> <br/>
 * #include <memory_show.h>
 * @{ */	
 
#define    BUDDY_ALLOCATION     0	//!< 指示用户堆内存管理的Buddy算法
#define    FIRST_FIT_ALLOCATION 1	//!< 指示用户堆内存管理的First-Fit算法

//华睿2号需要至少6个内存段数,FuKai,2018-7-11
#define MAX_SEGS (6) //!< 指示系统最大支持内存段数

	
/*! \struct mem_seg_info
 * \brief 系统内存段信息结构定义
 *
 * 该数据结构定义了系统内存段信息，描述了系统内存中一块连续的内存区域的起始地址和大小。
 */
struct mem_seg_info
{
	ptrdiff_t start_addr; //!< 起始地址
	size_t size;  //!< 内存段大小
	size_t heap_start_addr; //!< 可用于用户堆内存段起始地址
	size_t heap_size;  //!< 可用户用户堆内存段大小
	size_t resv_size; //!< 为固件运行而保留内存大小
};

extern struct mem_seg_info mem_seg_info_array[MAX_SEGS];
#if defined(__loongson2__) || defined(__loongson2_64__) || defined(_GS464_)
int mem_segs_get(struct mem_seg_info *mem_segs, int array_size);
#else
int mem_segs_set(u32 ddr_size, u32 resv_size);
#endif /* #if defined(__loongson2__) || defined(__loongson2_64__) */

typedef int (*FUNC_FREE_WALK_CB) (void * mem_block, u32 mem_size, int cb_arg,
				     boolean * is_continue); //!< 回调函数指针

#define MEM_WALK_UP			0
#define MEM_WALK_DOWN		1

/**  
 * @brief    初始化核心堆内存
 * 
 * 该接口由c_main()调用，初始化核心堆内存的管理对象，建立核心堆内存的管理机制。 
 *
 * @param   mm_type 核心堆内存算法
 *
 * @return	0	函数执行成功
 * @return 	-1	函数执行失败
 *
 * @exception EINVAL 核心堆起始地址kernel_area_start是无效指针或mm_type无效
 * @exception EMINITED 核心堆模块已被初始化
 *
 */
int sys_mem_init(u32 mm_type);


/**  
 * @brief    分配核心堆内存
 * 
 * 该接口分配核心堆内存，所分配内存的大小由size指出。 
 *
 * @param    size 	要求分配的内存长度
 *
 * @return   非0	函数执行成功，并返回内存块地址
 * @return   0		函数执行失败，设置errno指示错误信息
 *
 * @exception ENOMEM 	没有足够的内存空间分配
 * @exception EINVAL	参数size取值为零
 * @exception EMNOTINITED 核心堆模块未被初始化
 */
void *kmalloc(size_t size);

/**  
 * @brief    按对齐方式分配核心堆内存
 * 
 * 该接口指定对齐字节数分配核心堆内存，所分配内存的大小由size指出。 
 *
 * @param    size 	要求分配的内存长度
 * @param    align_size 	要求对齐长度
 *
 * @return   非0	函数执行成功，并返回内存块地址
 * @return   0		函数执行失败，设置errno指示错误信息
 *
 * @exception ENOMEM 	没有足够的内存空间分配
 * @exception EINVAL	参数size取值为零
 * @exception EMNOTINITED 核心堆模块未被初始化
 */
void * kmalloc_align(size_t size, int align_size);


/**  
 * @brief    分配核心堆DMA内存
 * 
 * 该接口分配由参数size指定大小的核心堆内存，该内存区用于DMA操作。该接口会关闭
 * CACHE 使能操作。
 *
 * @param    size 	要求分配的内存长度
 *
 * @return   非0	函数执行成功，并返回内存块地址
 * @return   0		函数执行失败，设置errno指示错误信息
 *
 * @exception ENOMEM 	没有足够的内存空间分配
 * @exception EINVAL	参数size取值为零
 * @exception EMNOTINITED 核心堆模块未被初始化
 *
 * @attention 目前该接口只支持pentium平台。
 */
void *kmalloc_dma(size_t size);

/**  
 * @brief    释放核心堆内存
 * 
 * 该接口将指定的内存块释放到核心堆中。 
 *
 * @param    addr	内存地址
 *
 * @return   无
 *
 * @exception EINVAL 	参数addr是无效指针，或者addr指定的内存地址无效
 * @exception EMNOTINITED 核心堆模块未被初始化
 *
 * @attention kfree()接口操作过程中出错会打印错误信息，并设置errno。
 */
void kfree(void *addr);


/**  
 * @brief    初始化用户堆内存
 * 
 * 该接口由sysConfigInition()调用，初始化用户堆内存的管理对象，建立用户堆内存的管理机制。
 *用户堆的内存管理提供Buddy分配算法和First-Fit分配算法，由参数flag指定用户堆的内存管理使用何种算法，
 *取值包括BUDDY_ALLOCATION和FIRST_FIT_ALLOCATION。
 *
 * @param	flag 算法标志，取值包括BUDDY_ALLOCATION和FIRST_FIT_ALLOCATION
 *
 * @return	0	函数执行成功
 * @return 	-1	函数执行失败
 *
 * @exception EINVAL 	参数flag取值不合法
 * @exception EMINITED  用户堆模块已被初始化
 *
 */
int heap_mem_init(int flag);


/**  
 * @brief    分配用户堆内存
 * 
 * 该接口用于分配用户堆内存块，参数size指定所分配内存的大小。 
 *
 * @param	size 	要求分配的内存大小
 *
 * @return	非0		函数执行成功，并返回内存块地址
 * @return 	NULL	函数执行失败，设置errno指示错误信息
 *
 * @exception ENOMEM 	没有足够的内存空间分配
 * @exception EINVAL 	参数size取值为零
 * @exception EMNOTINITED 用户堆模块尚未初始化
 * 
 * @attention 新分配内存的内容是不确定的。
 * @attention 针对malloc接口参数size的取值为零时，VxWorks5.5与ReWorks操作系统的处理结果是不同的：VxWorks5.5中函数返回非空指针；
 * 而ReWorks则返回空，且设置错误号为EINVAL。
 */
extern void *malloc(size_t size);

/**  
 * @brief    以相邻块的方式分配用户堆内存
 * 
 * 该接口用来配置nelem个相邻的内存块，每块的大小为elsize，返回指向第一个元素的指针。
 *这和使用下列的方式效果相同：malloc(nelem*elsize)，不同的是，利用calloc()
 *配置内存时会将内存内容初始化为0。 
 *
 * @param    nelem 	要求分配的内存块数量
 * @param    elsize 要求分配的每一个内存块的长度
 *
 * @return   非0	函数执行成功，并返回内存块地址
 * @return   NULL	函数执行失败，设置errno指示错误信息
 * 
 * @exception ENOMEM 	没有足够的内存空间分配
 * @exception EINVAL	参数nelem或elsize的取值为零
 * @exception EMNOTINITED 用户堆模块尚未初始化
 * 
 * @attention 针对calloc接口参数nelem或elsize的取值为零时，VxWorks5.5与ReWorks操作系统的处理结果是不同的：VxWorks5.5中函数返回非空指针；
 * 而ReWorks则返回空，且设置错误号为EINVAL。
 */
extern void *calloc(size_t nelem, size_t elsize);


/**  
 * @brief    重新分配用户堆内存
 * 
 * 该接口用来将ptr所指向的内存空间的大小改变为给定的大小size。 
 *
 * @param    ptr 	要求重新分配的内存块，若ptr的取值为NULL，则相当于调用malloc(size)。
 * @param    size 	要求分配的每一个内存块的长度，其取值可以是任意大小，大于或小于原尺寸都可以。<br/>			
 * 				若size值小于原先分配的内存长度，则内存内容不会改变；  <br/>
 * 				若size值大于原先分配的内存长度，则内存内容不会改变，但不一定返回原先的内存起始地址，并且新增加的内存未设置初始值； <br/>
 * 				若size为0，则相当于调用free(ptr)。
 *
 * @return   非0	函数执行成功，并返回内存块地址
 * @return   NULL	函数执行失败，设置errno指示错误信息
 *
 * @exception ENOMEM 	没有足够的内存空间分配
 * @exception EINVAL 	参数ptr指定的内存无效
 * @exception EMNOTINITED 用户堆模块尚未初始化
 * 
 * @attention 参数ptr不为NULL的情况下，如果函数执行失败，该接口不会释放ptr指定的内存。
 */
extern void *realloc(void *ptr, size_t size);

/**  
 * @brief    按对齐方式分配用户堆内存
 * 
 * 该接口从用户堆内存分配一块大小为size的缓存，同时它保证所分配缓存的开始地址按照边界对齐。
 *
 * @param	memptr 		存放所分配的用户堆内存地址
 * @param	alignment 	需要对齐的内存边界要求，其取值必须是类型【void *】长度的2的N次方倍
 * @param	size 		需要分配用户堆内存的字节长度
 *
 * @return	0			函数执行成功
 * @return	EINVAL		memptr是无效指针，或alignment不是【void *】类型长度的2的N次方倍数，或者size取值不合法
 * @return  ENOMEM 		没有足够的内存空间分配
 * @return	EMNOTINITED 用户堆模块尚未初始化
 *
 * @exception EINVAL 	参数无效
 * @exception ENOMEM 	内存不足
 * @exception EMNOTINITED 用户堆模块尚未初始化
 *
 */
extern int posix_memalign(void **memptr, size_t alignment, size_t size);

/**  
 * @brief    按对齐方式分配用户堆内存
 * 
 * 该接口从用户堆内存分配一块大小为size的缓存，同时它保证所分配缓存的开始地址按照边界对齐。
 *
 * @param	memptr 		存放所分配的用户堆内存地址
 * @param	alignment 	需要对齐的内存边界要求，其取值必须是类型【void *】长度的2的N次方倍
 * @param	size 		需要分配用户堆内存的字节长度
 *
 * @return   非0	函数执行成功，并返回内存块地址
 * @return   NULL	函数执行失败，设置errno指示错误信息
 * 
 * @exception EINVAL		memptr是无效指针，或alignment不是【void *】类型长度的2的N次方倍数，或者size取值不合法
 * @exception ENOMEM 		没有足够的内存空间分配
 * @exception EMNOTINITED 用户堆模块尚未初始化
 *
 * @attention 针对memalign接口参数size的取值为零时，VxWorks5.5与ReWorks操作系统的处理结果是不同的：VxWorks5.5中函数返回非空指针；
 * 而ReWorks则返回空，且设置错误号为EINVAL。
 */
extern void * memalign(size_t align_size, size_t size );

/**  
 * @brief    分配用户堆DMA内存
 * 
 * 该接口分配由参数size指定大小的用户堆内存，所分配的内存区用于DMA操作。该接口会关闭CACHE
 * 使能操作。
 *
 * @param	size	要求分配的内存长度
 *
 * @return	非0		函数执行成功，并返回内存块地址
 * @return  0		函数执行失败，设置errno指示错误信息
 *
 * @exception ENOMEM 	没有足够的内存空间分配
 * @exception EINVAL	参数size取值为零
 * @exception EMNOTINITED 用户堆模块尚未初始化
 *  
 * @attention 目前该接口只支持pentium平台。
 */
void *malloc_dma(size_t size);

/**  
 * @brief  释放用户堆内存
 * 
 * 该接口将指定的内存地址释放到用户堆中。如果参数addr为NULL，该接口将直接返回。  
 *
 * @param    addr	内存地址
 *
 * @return   无
 *
 * @exception EINVAL 参数addr是无效指针，或者addr指定的内存地址无效
 * @exception EMNOTINITED 用户堆模块未被初始化
 *
 * @attention free()接口操作过程中出错会打印错误信息，并设置errno。
 *
 */
extern void free(void * addr);

/**  
 * @brief    在用户堆中查找最大的空闲内存块
 * 
 * 该接口在用户堆中查找最大的空闲内存块，并返回它的大小。
 *
 * @param	无
 *
 * @return	>0	最大可用内存块的大小，以字节为单位。
 * @return	-1	函数执行失败，设置errno指示错误信息
 *
 * @exception EMNOTINITED  内存分区模块尚未初始化
 */
ssize_t mem_findmax(void);

/**  
 * @brief    添加统计内存分配释放的后置钩子函数
 * 
 *  添加统计内存分配释放的后置钩子函数，在内存分配与释放成功后将执行钩子函数，
 *  在分配内存统计的钩子函数执行时将输入分配内存地址与大小，在释放内存统计钩子函数
 *  执行时将输入释放内存地址。
 *
 * @param	malloc_hook		函数指针类型为：void (*)(void *, int)	函数参数分别为分配内存地址与大小
 * @param	free_hook		函数指针类型为： void (*)(void *)		函数参数为释放内存地址
 *
 * @return	无
 *
 * @exception 无
 */
void mem_stat_hook_add(void (*malloc_hook)(void *, int), void (*free_hook)(void *));

/**  
 * @brief    移除统计内存分配释放的后置钩子函数
 * 
 * 该接口用来移除统计内存分配释放的后置钩子函数。
 *
 * @param	无
 *
 * @return	无
 *
 * @exception 无
 */
void mem_stat_hook_delete(void);

/** @example example_kmalloc.c
* 下面的例子演示了如何使用接口 kmalloc( ) 和 kfree( ).
*/
/** @example example_kmalloc_dma.c
* 下面的例子演示了如何使用接口 kmalloc_dma( ).
*/
/** @example example_malloc.c
* 下面的例子演示了如何使用接口 malloc( ), free(), mem_getinfo( ).
*/
/** @example example_malloc_dma.c
* 下面的例子演示了如何使用接口 malloc_dma( ).
*/
/** @example example_realloc.c
* 下面的例子演示了如何使用接口 realloc().
*/
/** @example example_calloc.c
* 下面的例子演示了如何使用接口 calloc( ).
*/
/** @example example_posix_memalign.c
* 下面的例子演示了如何使用接口 posix_memalign().
*/
/** @example example_mem_findmax.c
* 下面的例子演示了如何使用接口 mem_findmax().
*/
/** @example_memory_stat_hook.c
* 下面的例子演示了如何使用接口 mem_stat_hook_add(), mem_stat_hook_delete().
*/

/* @}*/

#ifdef __cplusplus
}
#endif 

#endif		// __REWORKS_MEMORY_H__

