#ifndef __LS3A_ARCH_SEG_H__
#define __LS3A_ARCH_SEG_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Segment base addresses and sizes
 */

#define	K0BASE		0x80000000
#define	K0SIZE		0x20000000
#define	K1BASE		0xA0000000
#define	K1SIZE		0x20000000

#define KMBASE		"Do not use" /* 0xC0000000 *//* mapped kernel space */
#define	KMSIZE		"Do not use" /* 0x40000000 */

#define	KSBASE		"Do not use" /* 0xC0000000 */
#define	KSSIZE		"Do not use" /* 0x20000000 */
#define	K2BASE		0xC0000000
#define	K2SIZE		0x20000000
#define	K3BASE		0xE0000000
#define	K3SIZE		0x20000000

/*
 * Address conversion macros
 */

#ifdef _ASMLANGUAGE
#define	K0_TO_K1(x)	((x)|0xA0000000)	/* kseg0 to kseg1 */
#define	K1_TO_K0(x)	((x)&0x9FFFFFFF)	/* kseg1 to kseg0 */

#define	K0_TO_PHYS(x)	((x)&0x1FFFFFFF)	/* kseg0 to physical */
#define	K1_TO_PHYS(x)	((x)&0x1FFFFFFF)	/* kseg1 to physical */

#define	PHYS_TO_K0(x)	((x)|0x80000000)	/* physical to kseg0 */
#define	PHYS_TO_K1(x)	((x)|0xA0000000)	/* physical to kseg1 */

/* any kseg to phys, kseg0, kseg1 or kseg2 */
#define KX_TO_PHYS(x)   ((x)&0x1FFFFFFF)
#define KX_TO_K0(x)     (((x) & ADDRESS_SPACE_MASK) | K0BASE)
#define KX_TO_K1(x)     (((x) & ADDRESS_SPACE_MASK) | K1BASE)
#define KX_TO_K2(x)     (((x) & ADDRESS_SPACE_MASK) | K2BASE)
#else /* _ASMLANGUAGE */
#define	K0_TO_K1(x)	((unsigned)(x)|0xA0000000)	/* kseg0 to kseg1 */
#define	K1_TO_K0(x)	((unsigned)(x)&0x9FFFFFFF)	/* kseg1 to kseg0 */

#define	K0_TO_PHYS(x)	((unsigned)(x)&0x1FFFFFFF)	/* kseg0 to physical */
#define	K1_TO_PHYS(x)	((unsigned)(x)&0x1FFFFFFF)	/* kseg1 to physical */

#define	PHYS_TO_K0(x)	((unsigned)(x)|0x80000000)	/* physical to kseg0 */
#define	PHYS_TO_K1(x)	((unsigned)(x)|0xA0000000)	/* physical to kseg1 */

/* any kseg to phys, kseg0, kseg1 or kseg2 */
#define KX_TO_PHYS(x)   ((unsigned)(x)&0x1FFFFFFF)
#define KX_TO_K0(x)     (((unsigned)(x) & ADDRESS_SPACE_MASK) | K0BASE)
#define KX_TO_K1(x)     (((unsigned)(x) & ADDRESS_SPACE_MASK) | K1BASE)
#define KX_TO_K2(x)     (((unsigned)(x) & ADDRESS_SPACE_MASK) | K2BASE)
#endif /* _ASMLANGUAGE */

/*
 * The "KM" conversion macros only work for limited sections of the
 * mapped kernel space.
 * 
 */

#define ADDRESS_SPACE_MASK	(~(7<<29))
#ifdef _ASMLANGUAGE
#define	KM_TO_K0(x)	(((x) & ADDRESS_SPACE_MASK) | K0BASE)
#define	KM_TO_K1(x)	(((x) & ADDRESS_SPACE_MASK) | K1BASE)
#define	KM_TO_PHYS(x)	((x)&0x1FFFFFFF)
#define	PHYS_TO_KM(x)	((x) | K2BASE)

#else /* _ASMLANGUAGE */
#define	KM_TO_K0(x)	(((unsigned)(x) & ADDRESS_SPACE_MASK) | K0BASE)
#define	KM_TO_K1(x)	(((unsigned)(x) & ADDRESS_SPACE_MASK) | K1BASE)

#define	KM_TO_PHYS(x)	((unsigned)(x)&0x1FFFFFFF)
#define	PHYS_TO_KM(x)	((unsigned)(x) | K2BASE)
#define K0_TO_KM(x)	PHYS_TO_KM(K0_TO_PHYS(x))


/*
 * Address predicates
 */

#define	IS_KSEG0(x)	((unsigned)(x) >= K0BASE && (unsigned)(x) < K1BASE)
#define	IS_KSEG1(x)	((unsigned)(x) >= K1BASE && (unsigned)(x) < K2BASE)
#define	IS_KSEG2(x)	((unsigned)(x) >= K2BASE)
#define	IS_KUSEG(x)	((unsigned)(x) < K0BASE)

#define IS_UNMAPPED(x)	(((unsigned)(x) & 0xc0000000) == 0x80000000)
#define IS_KSEGM(x)     (!(IS_UNMAPPED(x)))

#endif /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /*  */
