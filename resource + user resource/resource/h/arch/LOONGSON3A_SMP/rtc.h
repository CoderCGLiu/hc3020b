#ifndef _LS3A_RTC_H_
#define _LS3A_RTC_H_

#ifdef __cplusplus
extern "C"
{
#endif

//#include "../io.h"
#define outb_p(x, y) (*(volatile unsigned char *)(0xbfd00000+(y)) = x)
#define inb_p(x) (*(volatile unsigned char *)(0xbfd00000+(x)))

#define	DS_REG_SEC	0x00		/* Seconds reg */
#define	DS_REG_ASEC	0x01		/* Alarm Seconds reg */
#define	DS_REG_MIN	0x02		/* Minutes reg */
#define	DS_REG_AMIN	0x03		/* Alarm Minutes reg */
#define	DS_REG_HOUR	0x04		/* Hours reg */
#define	DS_REG_AHOUR	0x05		/* Alarm Hours reg */
#define	DS_REG_WDAY	0x06		/* Day of week reg */
#define	DS_REG_DATE	0x07		/* Day of month reg */
#define	DS_REG_MONTH	0x08		/* Month reg */
#define	DS_REG_YEAR	0x09		/* Year reg */

#define	DS_REG_CTLA	0x0a		/* Control reg A */
#define	DS_REG_CTLB	0x0b		/* Control reg B */
#define	DS_REG_CTLC	0x0c		/* Control reg A */
#define	DS_REG_CTLD	0x0d		/* Control reg B */


#define	DS_CTLA_RS0	0x01		/* Rate Select */
#define	DS_CTLA_RS1	0x02		/* Rate Select */
#define	DS_CTLA_RS2	0x04		/* Rate Select */
#define	DS_CTLA_RS3	0x08		/* Rate Select */
#define	DS_CTLA_DV0	0x10		/* Bank Select */
#define	DS_CTLA_DV1	0x20		/* Osc. Enable */
#define	DS_CTLA_DV2	0x40		/* Countdown Chain */
#define	DS_CTLA_UIP	0x80		/* Update In Progress flag 1 */

#define	DS_CTLB_DSE	0x01		/* Daylight savings enable */
#define	DS_CTLB_24	0x02		/* 24hr enable */
#define	DS_CTLB_DM	0x04		/* Data mode */
#define	DS_CTLB_SQWE	0x08		/* Square wave enable */
#define	DS_CTLB_UIE	0x10		/* Update ended interrupt enable */
#define	DS_CTLB_AIE	0x20		/* Alarm interrupt enable */
#define	DS_CTLB_PIE	0x40		/* Periodic interrupt enable */
#define	DS_CTLB_SET	0x80		/* Set registers enable */

static inline unsigned char CMOS_READ(unsigned char addr)
{
        unsigned char val;
        outb_p(addr, 0x70);
        val = inb_p(0x71);
        return val;
}

static inline void CMOS_WRITE(unsigned char val, unsigned char addr)
{
        outb_p(addr, 0x70);
        outb_p(val, 0x71);
}

OS_STATUS rtc_read(struct tm * the_tm);
OS_STATUS rtc_write(struct tm * the_tm);

#ifdef __cplusplus
}
#endif

#endif /* #define RTC_H */
