
/*******************************************************************************
 * 
 * ??:?????????????????
 * ??:??????????????????
 * ??:       
 * 		2014-03-07, ??, ??. 
 */

#ifndef _REWORKS_EXCVEC_H_
#define _REWORKS_EXCVEC_H_

#ifdef __cplusplus
extern "C" {
#endif


/* ???? */
#define MIPS_EXC_INT	0		/* ???? */
#define MIPS_EXC_TLBM	1		/* TLB???? */
#define MIPS_EXC_TLBL	2		/* TLB???? (fetch) */
#define MIPS_EXC_TLBS	3		/* TLB???? (store) */
#define MIPS_EXC_ADEL	4		/* ?????? (fetch) */
#define MIPS_EXC_ADES	5		/* ?????? (store) */
#define MIPS_EXC_IBE 	6		/* bus???? (?? fetch) */
#define MIPS_EXC_DBE	7		/* bus???? (?? load or store) */
#define MIPS_EXC_SYS	8		/* ?????? */
#define MIPS_EXC_BP 	9		/* ???? */
#define MIPS_EXC_RI		10		/* ?????? */
#define MIPS_EXC_FPU	11		/* ?????? */
#define MIPS_EXC_OVF	12		/* ?????? */
#define MIPS_EXC_TRAP	13		/* trap?? */
#define MIPS_EXC_FPE	15		/* ???? */
#define MIPS_EXC_WATCH	23		/* watchpoint?? */

#define MIPS_EXC_FPE_INVALID	55	/* ?????? */
#define MIPS_EXC_FPE_DIV0		56	/* ?? */
#define MIPS_EXC_FPE_OVERFLOW	57	/* ?? */
#define MIPS_EXC_FPE_UNDERFLOW	58	/* ?? */
#define MIPS_EXC_FPE_INEXACT	59	/* ?????? */

#ifdef __cplusplus
}
#endif
#endif
