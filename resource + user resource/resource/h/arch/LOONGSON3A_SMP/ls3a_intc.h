/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了龙芯3A中断控制器相关寄存器
 * 修改：
 * 		  2013-08-21，张静，创建 
 */

#ifndef __LS3A_INTC_H_
#define __LS3A_INTC_H_

#ifdef __cplusplus
extern "C"
{
#endif

/* HT控制器寄存器*/
#define HT_INTVEC0    (0x80)
#define HT_INTVEC1    (0x84)
#define HT_INTVEC2    (0x88)
#define HT_INTVEC3    (0x8C)
#define HT_INTVEC4    (0x90)
#define HT_INTVEC5    (0x94)
#define HT_INTVEC6    (0x98)
#define HT_INTVEC7    (0x9C)


#define HT_INTENABLE0 (0xA0)
#define HT_INTENABLE1 (0xA4)
#define HT_INTENABLE2 (0xA8)
#define HT_INTENABLE3 (0xAC)
#define HT_INTENABLE4 (0xB0)
#define HT_INTENABLE5 (0xB4)
#define HT_INTENABLE6 (0xB8)
#define HT_INTENABLE7 (0xBC)

/**
 * 龙芯3A中断源数目
 */
#define LEVEL1_INT_CNT (32)

/**
 * 龙芯3A 32个中断源对应中断号
 */
#define SYS_INT0  	          (0)
#define SYS_INT1  	          (1)
#define SYS_INT2  	          (2)
#define SYS_INT3  	          (3)

#define PCI_INT0  	          (4)
#define PCI_INT1 	          (5)
#define PCI_INT2  	          (6)
#define PCI_INT3  	          (7)

#define MATRIX_INT0           (8)
#define MATRIX_INT1           (9)

#define LPC_INT 	          (10)
#define MC0_INT 	          (11)
#define MC1_INT 	          (12)

#define BARRIER_INT 	      (13)
#define RESERVED_INT 	      (14)
#define PCI_PERR_INT          (15)

#define HT0_INT0              (16)
#define HT0_INT1              (17)
#define HT0_INT2              (18)
#define HT0_INT3              (19)
#define HT0_INT4              (20)
#define HT0_INT5              (21)
#define HT0_INT6              (22)
#define HT0_INT7              (23)

#define HT1_INT0              (24)
#define HT1_INT1              (25)
#define HT1_INT2              (26)
#define HT1_INT3              (27)
#define HT1_INT4              (28)
#define HT1_INT5              (29)
#define HT1_INT6              (30)
#define HT1_INT7              (31)

/**
 * 中断路由寄存器地址
 */
#define SYS_INT0_ENTRY  	  (0x3ff01400)
#define SYS_INT1_ENTRY  	  (0x3ff01401)
#define SYS_INT2_ENTRY  	  (0x3ff01402)
#define SYS_INT3_ENTRY  	  (0x3ff01403)

#define PCI_INT0_ENTRY  	  (0x3ff01404)
#define PCI_INT1_ENTRY 	      (0x3ff01405)
#define PCI_INT2_ENTRY  	  (0x3ff01406)
#define PCI_INT3_ENTRY  	  (0x3ff01407)

#define MATRIX_INT0_ENTRY     (0x3ff01408)
#define MATRIX_INT1_ENTRY     (0x3ff01409)

#define LPC_INT_ENTRY 	      (0x3ff0140a)
#define MC0_INT_ENTRY 	      (0x3ff0140b)
#define MC1_INT_ENTRY 	      (0x3ff0140c)

#define BARRIER_INT_ENTRY 	  (0x3ff0140d)
#define RESERVED_INT_ENTRY 	  (0x3ff0140e)
#define PCI_PERR_INT_ENTRY    (0x3ff0140f)

#define HT0_INT0_ENTRY        (0x3ff01410)
#define HT0_INT1_ENTRY        (0x3ff01411)
#define HT0_INT2_ENTRY        (0x3ff01412)
#define HT0_INT3_ENTRY        (0x3ff01413)
#define HT0_INT4_ENTRY        (0x3ff01414)
#define HT0_INT5_ENTRY        (0x3ff01415)
#define HT0_INT6_ENTRY        (0x3ff01416)
#define HT0_INT7_ENTRY        (0x3ff01417)

#define HT1_INT0_ENTRY        (0x3ff01418)
#define HT1_INT1_ENTRY        (0x3ff01419)
#define HT1_INT2_ENTRY        (0x3ff0141a)
#define HT1_INT3_ENTRY        (0x3ff0141b)
#define HT1_INT4_ENTRY        (0x3ff0141c)
#define HT1_INT5_ENTRY        (0x3ff0141d)
#define HT1_INT6_ENTRY        (0x3ff0141e)
#define HT1_INT7_ENTRY        (0x3ff0141f)

/**
 * CPU中断控制器
 */
#define INTISR_REG            (0x3ff01420) /*32位中断状态寄存器*/
#define INTEN_REG             (0x3ff01424) /*32位中断使能状态寄存器*/
#define INTENSET_REG          (0x3ff01428) /*32位中断使能设置寄存器*/
#define INTENCLR_REG          (0x3ff0142c) /*32位中断清除使能寄存器*/
#define INTENEDGE_REG         (0x3ff01438) /*32位触发方式寄存器*/

#define INTISR_CORE0_REG      (0x3ff01440) /*路由给core0的中断状态*/
#define INTISR_CORE1_REG      (0x3ff01448) /*路由给core0的中断状态*/
#define INTISR_CORE2_REG      (0x3ff01450) /*路由给core0的中断状态*/
#define INTISR_CORE3_REG      (0x3ff01458) /*路由给core0的中断状态*/

/**
 * SB710中断号设置寄存器
 */
#define PCI_INTR_INDEX (0xc00)
#define PCI_INTR_DATA  (0xc01)

/**
 * SB710中断触发方式寄存器
 */
#define INT_EDGE_CTRL0  (0x4d0) /*16位寄存器*/
#define INT_EDGE_CTRL1  (0x4d1) /*16位寄存器*/


#ifdef __cplusplus
}
#endif

#endif /* __LS3A_INTC_H_ */
