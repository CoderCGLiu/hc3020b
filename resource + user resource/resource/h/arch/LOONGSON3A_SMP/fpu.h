#ifndef _REWORKS_FPU_H_
#define _REWORKS_FPU_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
 *  mips/fpu.h for loongson2
 *
 */
#include <cpu.h>

#ifndef ASM_LANGUAGE	

#define NUM_FPU_REGS	32

#define FPREG_SET FP_CONTEXT

/*
 *  mips_fpu_context_save
 *
 *  This routine saves the floating point context passed to it.
 */
void mips_fpu_context_save(Fp_Context *fp_context_ptr);

/*
 *  mips_fpu_context_save
 *
 *  This routine restores the floating point context passed to it.
 */
void mips_fpu_context_restore(Fp_Context *fp_context_ptr);

void fpu_init(void);

void fpu_context_dump(Fp_Context *context);

#endif /* #ifndef ASM_LANGUAGE */

#ifdef __cplusplus
}
#endif

#endif
