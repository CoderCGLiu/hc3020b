#ifndef _REWORKS_ARCH_CACHE_H_
#define _REWORKS_ARCH_CACHE_H_

#ifdef __cplusplus
extern "C" 
{
#endif
#include <reworks/types.h>



#ifdef LOONGSON3A_1000
#define CACHE_LINE_SIZE (32)
#else /*2K1000 3A3000*/
#define CACHE_LINE_SIZE (64)
#endif


#ifdef __cplusplus
}
#endif
#endif /* _REWORKS_CACHE_H_ */
