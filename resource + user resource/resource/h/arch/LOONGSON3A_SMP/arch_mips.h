#ifndef	_REWORKS_ARCH_MIPS_H_
#define _REWORKS_ARCH_MIPS_H_
#include "cpu.h"
#if 1//defined(LOONGSON3A_SMP)
#define _WRS_INT_REGISTER_SIZE		8
#define _WRS_FP_REGISTER_SIZE		8
#define _WRS_INT_REGISTER_SHIFT		3
#define _WRS_FP_REGISTER_SHIFT		3

#define FUNC(func)          func
#define FUNC_LABEL(func)    func:

#define _RTypeSize			_WRS_INT_REGISTER_SIZE

/*
 * These macros are used to declare assembly language symbols that need
 * to be typed properly(func or data) to be visible to the OMF tool.  
 * So that the build tool could mark them as an entry point to be linked
 * by another PD.
 */

#define GTEXT(sym) FUNC(sym) ;  .type   FUNC(sym),@function
#define GDATA(sym) FUNC(sym) ;  .type   FUNC(sym),@object

/* Return the size of the frame for the named routine */
#define FRAMESZ(routine)	_##routine##Fsize
#ifdef __mips_abi_n32__
#define FRAMEASZ(routine)	_##routine##ARsize
#endif

/* The location at which to store the return address */
#define FRAMERA(routine) 	(FRAMESZ(routine)-8)

/* Locations at which to store locally used registers */

#ifdef __mips_abi_n32__
#define FRAMER(routine,regn) 	(FRAMEASZ(routine)+(_RTypeSize)*(regn))
#else /* __mips_abi_n32__ */
#define FRAMER(routine,regn) 	((_RTypeSize)*(4+(regn)))
#endif /* __mips_abi_n32__ */

#define FRAMER0(routine) FRAMER(routine,0)
#define FRAMER1(routine) FRAMER(routine,1)
#define FRAMER2(routine) FRAMER(routine,2)
#define FRAMER3(routine) FRAMER(routine,3)

/* Locations at which to store argument registers */
#ifdef __mips_abi_n32__
#define FRAMEA(routine, regn) (                 (_RTypeSize)*(regn))
#else
#define FRAMEA(routine, regn) (FRAMESZ(routine)+(_RTypeSize)*(regn))
#endif
#define FRAMEA0(routine) FRAMEA(routine,0)
#define FRAMEA1(routine) FRAMEA(routine,1)
#define FRAMEA2(routine) FRAMEA(routine,2)
#define FRAMEA3(routine) FRAMEA(routine,3)
#ifdef __mips_abi_n32__
#define FRAMEA4(routine) FRAMEA(routine,4)
#define FRAMEA5(routine) FRAMEA(routine,5)
#define FRAMEA6(routine) FRAMEA(routine,6)
#define FRAMEA7(routine) FRAMEA(routine,7)
#endif /* __mips_abi_n32__ */
#define	FRAMETID(routine)	FRAMER0(routine)
#define	FRAMEEPC(routine)	FRAMER1(routine)
#define FRAMEREG(routine)	FRAMER2(routine)	/*ADD BY HB */

#define SETFRAME(routine,naregs,nregs) \
	SETFRAME_EXTRA(routine,naregs,nregs,0)
#ifdef __mips_abi_n32__
/*
 * Calculate the frame size for the named routine
 * up to 8 register slots allocated for subroutines to store argument registers
 * 8 bytes are allocated for saving RA (independent of register size).
 * nregs register locations are reserved for storing locally used registers.
 * stack is kept 16-byte aligned.
 */
#define SETFRAME_EXTRA(routine,naregs,nregs,extra) \
   FRAMEASZ(routine) = ((_RTypeSize)*(naregs)); \
   FRAMESZ(routine) = ((((_RTypeSize)*((naregs)+(nregs)))+8+(extra)+15) & ~0x0f)  
#else /*  __mips_abi_n32__ */
 
/*
 * Calculate the frame size for the named routine
 * 4 register slots allocated for subroutines to store argument registers
 * 8 bytes are allocated for saving RA (independent of register size).
 * nregs register locations are reserved for storing locally used registers.
 * stack is kept 8-byte aligned.
 */
#define SETFRAME_EXTRA(routine,naregs,nregs,extra) \
	FRAMESZ(routine) = ((((_RTypeSize)*(4+(nregs)))+8+(extra)+7) & ~0x07)
#endif /*  __mips_abi_n32__ */

#define FP_EXC_I	0x1000		/* inexact operation */
#define FP_EXC_U	0x2000		/* underflow */
#define FP_EXC_O	0x4000		/* overflow */
#define FP_EXC_Z	0x8000		/* divide by zero */
#define FP_EXC_V	0x10000		/* invalid operation */
#define FP_EXC_E	0x20000		/* unimplemented operation */

/* exception code */

#define EXC_CODE_INT	0		/* externel interrupt */	/*cpu.h*/
#define EXC_CODE_TLBM	1		/* TLB modification exception */

#define EXC_CODE_TLBL	2		/* TLB miss exception (load or 
					 * instruction fetch) */
#define EXC_CODE_IBND   2               /* R4650: IBound exception */
#define EXC_CODE_TLBS	3		/* TLB miss exception (store) */
#define EXC_CODE_DBND   3               /* R4650: DBound exception */

#define EXC_CODE_ADEL	4		/* address error exception (load/
					 * instruction fetch ) */
#define EXC_CODE_ADES	5		/* address error exception (store) */
#define EXC_CODE_BEI 	6		/* bus error(for instruction fetch) */
#define EXC_CODE_BED	7		/* bus error (data load or store) */
#define EXC_CODE_SYS	8		/* system call exception */
#define EXC_CODE_BP 	9		/* breakpoint exception */
#define EXC_CODE_RI	10		/* reserved instruction exception */
#define EXC_CODE_FPU	11		/* coprocessor unusable exception */
#define EXC_CODE_OVF	12		/* arithmetic overflow exception */

#define EXC_CODE_TRAP	13		/* trap exception */
#define EXC_CODE_VCEI	14		/* instr virtual coherency exception */
#define EXC_CODE_FPE	15		/* floating point exception */
#define EXC_CODE_WATCH	23		/* watchpoint exception */
#define EXC_CODE_VCED	31		/* data virtual coherency exception */

#define EXC_CODE_FPE_INVALID	55	/* invalid fpe operation */
#define EXC_CODE_FPE_DIV0		56	/* divide by zero */
#define EXC_CODE_FPE_OVERFLOW	57	/* overflow */
#define EXC_CODE_FPE_UNDERFLOW	58	/* underflow */
#define EXC_CODE_FPE_INEXACT	59	/* inexact result */

/* constants for the OPCODE field for some general instructions */
#define	OPCODE_SHIFT	26
#define	OPCODE_GMASK	0x3f
#define	OPCODE_SPECIAL	0x00
#define	OPCODE_BCOND	0x01
#define	OPCODE_REGIMM	0x01
#define	OPCODE_J	0x02
#define	OPCODE_JAL	0x03
#define	OPCODE_BEQ	0x04
#define	OPCODE_C1	0x11
#define	OPCODE_C1X	0x13		/* bits 31-26 */
#define C1XSBCD_SHIFT   0x3		/* shift value to get sub code */
#define C1XSBCD_MASK    0x7		/* mask value to get sub code */
#define SBCODE_MADD     0x4		/* sub code for MADD */
#define SBCODE_NMADD    0x6		/* sub code for NMADD */
#define SBCODE_MSUB     0x5		/* sub code for NMADD */
#define SBCODE_NMSUB    0x7		/* sub code for NMADD */
#define SBCODE_FMT_MASK 0x7		/* format mask value */
#define SBCODE_FMT_S    0		/* 32 bit precision */
#define SBCODE_FMT_D    1		/* 64 bit precision */
#define SBCODE_FMT_PS   0x6             /* paired single mode */

#define C1XREG_FD_SFT   0x6		/* shift value for fd reg */
#define C1XREG_FS_SFT   0xb		/* shift value for fs reg */
#define C1XREG_FT_SFT   0x10		/* shift value for ft reg */
#define C1XREG_FR_SFT   0x15		/* shift value for fr reg */
#define C1XREG_MASK     0x1f		/* mask value registers */

/* A MIPS Hazard is defined as any combination of instructions which
 * would cause unpredictable behavior in terms of pipeline delays,
 * cache misses, and exceptions.  Hazards are defined by the number
 * of CPU cycles that must pass between certain combinations of
 * instructions.  Because some MIPS CPUs single-issue nop instructions
 * while others dual-issue, the CPU cycles defined below are done so
 * according to the instruction issue mechanism available.
 */
#define SINGLE_ISSUE 0 	
#define DUAL_ISSUE   1 
#define CPU_CYCLES              DUAL_ISSUE

/* Using the issue mechanism definitions above, the MIPS CPU cycles
 * are defined below.
 */

#define	ssnop		.word 0x00000040

#if (CPU_CYCLES == SINGLE_ISSUE)
#define CPU_CYCLES_ONE          ssnop
#define CPU_CYCLES_TWO          ssnop; ssnop
#elif (CPU_CYCLES == DUAL_ISSUE)
#define CPU_CYCLES_ONE          ssnop; ssnop
#define CPU_CYCLES_TWO          ssnop; ssnop; ssnop; ssnop
#endif

#define HAZARD_TLB       CPU_CYCLES_TWO
#define HAZARD_ERET      CPU_CYCLES_TWO
#define HAZARD_CP_READ   CPU_CYCLES_ONE
#define HAZARD_CP_WRITE  CPU_CYCLES_TWO
#define HAZARD_CACHE_TAG CPU_CYCLES_ONE
#define HAZARD_CACHE     CPU_CYCLES_TWO
#define HAZARD_INTERRUPT CPU_CYCLES_TWO

#define C1_FMT_SHIFT	21
#define	C1_FMT_MASK	0xf
#define C1_FMT_SINGLE	0
#define C1_FMT_DOUBLE	1
#define C1_FMT_EXTENDED	2
#define C1_FMT_QUAD	3
#define C1_FMT_WORD	4
#define C1_FMT_MAX	5

/*
* MIPS Coprocessor 0 regs
*/
#define C0_IBASE	$0	/* R4650: instruction base xlate address */
#define C0_IBOUND	$1	/* R4650: instruction xlate address bound */
#define C0_DBASE	$2	/* R4650: data base xlate address */
#define C0_DBOUND	$3	/* R4650: data xlate address bound */
#define	C0_INX		$0	/* tlb index */
#define	C0_RAND		$1	/* tlb random */

/* Begin CPUs: R3000, CW4000, CW4011 */
#define	C0_TLBLO	$2	/* tlb entry low */
/* End R3000, CW4000, CW4011 */

/* Begin CPUs: R4000, VR5000, VR5400, VR4100 */
#define C0_TLBLO0	$2	/* tlb entry low 0 */
#define C0_TLBLO1	$3	/* tlb entry low 1 */
/* End R4000, VR5000, VR5400, VR4100 */

#define	C0_CTXT		$4	/* tlb context */

/* Begin CPUs: R4000, VR5000, VR5400, VR4100 */
#define C0_PAGEMASK	$5	/* page mask */
#define C0_WIRED	$6	/* lb wired entries */
/* End R4000, VR5000, VR5400, VR4100 */

#define	C0_BADVADDR	$8		/* bad virtual address */

/* Begin CPUs: R4000, R4650, VR5000, VR5400, CW4011, VR4100 */
#define	C0_COUNT	$9	/* count */
/* End R4000, R4650, VR5000, VR5400, CW4011, VR4100 */

/* Begin CPUs: R4000, VR5000, VR5400, R3000, CW4000, CW4011, VR4100 */
#define	C0_TLBHI	$10	/* tlb entry hi */
/* End R4000, VR5000, VR5400, R3000, CW4000, CW4011, VR4100 */

/* Begin CPUs: R4000, VR5000, VR5400, R4650, CW4011, VR4100 */
#define	C0_COMPARE	$11	/* compare */
/* End R4000, VR5000, VR5400, R4650, CW4011, VR4100*/

#define	C0_SR		$12	/* status register */
#define	C0_CAUSE	$13	/* exception cause */
#define	C0_EPC		$14	/* exception pc */

#define C0_PRID		$15

/* Begin CPUs: R4000, R4650, VR5000, VR5400, VR4100, CW4011 */
#define C0_CONFIG	$16

#define C0_CALG		$17	/* R4650: cache algorithm register */
#define C0_LLADDR	$17

#define C0_IWATCH	$18	/* R4650: instruction virt addr for watch */
#define C0_WATCHLO	$18

#define C0_DWATCH	$19	/* R4650: data virt addr for watch */
#define C0_WATCHHI	$19

#define C0_ECC		$26
#define C0_CACHEERR	$27
#define C0_TAGLO	$28

/* Begin CPUs: R4000, VR5000, VR5400, VR4100 */
#define C0_TAGHI	$29
/* End R4000, VR5000, VR5400, VR4100 */

#define C0_ERRPC	$30
/* End R4000, R4650, VR5000, VR5400, VR4100, CW4011 */

/*
*  MIPS floating point coprocessor register definitions
*/

#define fp0	$f0	/* return reg 0 */
#define fp1	$f1	/* return reg 1 */
#define fp2	$f2	/* return reg 2 */
#define fp3	$f3	/* return reg 3 */
#define fp4	$f4	/* caller saved 0 */
#define fp5	$f5	/* caller saved 1 */
#define fp6	$f6	/* caller saved 2 */
#define fp7	$f7	/* caller saved 3 */
#define fp8	$f8	/* caller saved 4 */
#define fp9	$f9	/* caller saved 5 */
#define fp10	$f10	/* caller saved 6 */
#define fp11	$f11	/* caller saved 7 */
#define fp12	$f12	/* arg reg 0 */
#define fp13	$f13	/* arg reg 1 */
#define fp14	$f14	/* arg reg 2 */
#define fp15	$f15	/* arg reg 3 */
#define fp16	$f16	/* caller saved 8 */
#define fp17	$f17	/* caller saved 9 */
#define fp18	$f18	/* caller saved 10 */
#define fp19	$f19	/* caller saved 11 */
#define fp20	$f20	/* callee saved 0 */
#define fp21	$f21	/* callee saved 1 */
#define fp22	$f22	/* callee saved 2 */
#define fp23	$f23	/* callee saved 3 */
#define fp24	$f24	/* callee saved 4 */
#define fp25	$f25	/* callee saved 5 */
#define fp26	$f26	/* callee saved 6 */
#define fp27	$f27	/* callee saved 7 */
#define fp28	$f28	/* callee saved 8 */
#define fp29	$f29	/* callee saved 9 */
#define fp30	$f30	/* callee saved 10 */
#define fp31	$f31	/* callee saved 11 */

#define C1_IR $0	/* implementation/revision reg */
#define C1_SR $31	/* control/status reg */

/* 
 * These constants refer to the fields of coprocessor store/load instructions.
 */

#define	FPU_TYPE_SHIFT	16		/* shift instruction 16 bits to right */
#define	FPU_I_INST_MASK	0x03c0    	/* I-type instruction mask */
#define	FPU_TYPE_MASK	0x0200    	/* I-type instruction mask */
#define	FPU_INST_MFC	0x0000    	/* mfc1 instructio */
#define	FPU_INST_MTC	0x0080    	/* mtc1 instructio */
#define	FPU_INST_CFC	0x0040    	/* cfc1 instructio */
#define	FPU_INST_CTC	0x00c0    	/* ctc1 instructio */
#define FPU_I_RT_MASK	0x001f		/* rt bits */
#define FPU_I_RS_MASK	0xf800		/* rs bits */
#define FPU_STLD1_MASK	0xfc00		/* swc1/lwc1 code mask 16 bit right 
					 * shifted */
#define	FPU_SW_INSTR	0xe400		/* swc1 instruction */
#define	FPU_LW_INSTR	0xc400		/* lwc1 instruction */
#define	OFFSET_MASK	0xffff		/* offset mask for I-type instruction */
#define	SLW_BASE_MASK	0x03e0		/* base maske for I-type instruction */




#define C1_FUNC_MASK	0x3f
#define C1_FUNC_DIV	3
#define C1_FUNC_NEG	7
#define C1_FUNC_ROUNDL	8
#define C1_FUNC_FLOORL	0x0b
#define C1_FUNC_ROUNDW	0x0c
#define C1_FUNC_FLOORW	0x0f
#define C1_FUNC_CVTS	0x20
#define C1_FUNC_CVTD	0x21
#define C1_FUNC_CVTW	0x24
#define C1_FUNC_CVTL	0x25
#define C1_FUNC_1stCMP	0x30

/*
 * These constants refer to single format floating-point values
 */
#define	SEXP_SHIFT	23
#define	SEXP_MASK	0xff
#define	SEXP_NAN	0xff
#define	SEXP_INF	0xff
#define	SEXP_BIAS	127
#define	SEXP_MAX	127
#define	SEXP_MIN	-126
#define	SEXP_OU_ADJ	192
#define	SIMP_1BIT	0x00800000
#define	SFRAC_LEAD0S	8
#define	SFRAC_BITS	23
#define	SFRAC_MASK	0x007fffff
#define	SFRAC_LEAST_MAX	0x007fffff

#define	SSNANBIT_MASK	0x00400000
#define	SQUIETNAN_LEAST	0x7fbfffff

/*
 * These constants refer to fields in the floating-point status and control
 * register.
 */
#define	CSR_CBITSHIFT	23
#define	CSR_CBITMASK	0x1
#define	CSR_CBITSET	0x00800000
#define	CSR_CBITCLEAR	0xff7fffff

#define	CSR_EXCEPT	0x0003f000
#define	UNIMP_EXC	0x00020000
#define	INVALID_EXC	0x00010040
#define	DIVIDE0_EXC	0x00008020
#define	OVERFLOW_EXC	0x00004010
#define	UNDERFLOW_EXC	0x00002008
#define	INEXACT_EXC	0x00001004

#define CSR_ENABLE		0x00000f80
#define	INVALID_ENABLE		0x00000800
#define	DIVIDE0_ENABLE		0x00000400
#define	OVERFLOW_ENABLE		0x00000200
#define	UNDERFLOW_ENABLE	0x00000100
#define	INEXACT_ENABLE		0x00000080


/*
 * These constants refer to double format floating-point values
 */
#define	DEXP_SHIFT	20
#define	DEXP_MASK	0x7ff
#define	DEXP_NAN	0x7ff
#define	DEXP_INF	0x7ff
#define	DEXP_BIAS	1023
#define	DEXP_MAX	1023
#define	DEXP_MIN	-1022
#define	DEXP_OU_ADJ	1536
#define	DIMP_1BIT	0x00100000
#define	DFRAC_LEAD0S	11
#define	DFRAC_BITS	52
#define	DFRAC_MASK	0x000fffff
#define	DFRAC_LESS_MAX	0x000fffff
#define	DFRAC_LEAST_MAX	0xffffffff

#define	DSNANBIT_MASK	0x00080000
#define	DQUIETNAN_LESS	0x7ff7ffff
#define	DQUIETNAN_LEAST	0xffffffff

/*
 * These constants refer to floating-point values for all formats
 */
#define	SIGNBIT		0x80000000

#define	GUARDBIT	0x80000000
#define	STKBIT		0x20000000

/*
 * These constants refer to the fields of coprocessor instructions not
 * cpu instructions (ie the RS and RD fields are different).
 */
#define BASE_SHIFT	21
#define FPE_BASE_MASK	0x1f
#define RT_SHIFT	16
#define	FPE_RT_MASK	0x1f
#define	RT_FPRMASK	0x1e
#define RS_SHIFT	11
#define	FPE_RS_MASK	0x1f
#define	RS_FPRMASK	0x1e
#define RD_SHIFT	6
#define	FPE_RD_MASK	0x1f
#define	RD_FPRMASK	0x1e

#define IMMED_SHIFT	16

#define C1_FMT_SHIFT	21
#define	C1_FMT_MASK	0xf
#define C1_FMT_SINGLE	0
#define C1_FMT_DOUBLE	1
#define C1_FMT_EXTENDED	2
#define C1_FMT_QUAD	3
#define C1_FMT_WORD	4
#define C1_FMT_MAX	5

#define C1_FUNC_MASK	0x3f
#define C1_FUNC_DIV	3
#define C1_FUNC_NEG	7
#define C1_FUNC_ROUNDL	8
#define C1_FUNC_FLOORL	0x0b
#define C1_FUNC_ROUNDW	0x0c
#define C1_FUNC_FLOORW	0x0f
#define C1_FUNC_CVTS	0x20
#define C1_FUNC_CVTD	0x21
#define C1_FUNC_CVTW	0x24
#define C1_FUNC_CVTL	0x25
#define C1_FUNC_1stCMP	0x30

#define COND_UN_MASK	0x1
#define COND_EQ_MASK	0x2
#define COND_LT_MASK	0x4
#define COND_IN_MASK	0x8


#define	CSR_RM_MASK	0x3
#define	CSR_RM_RN	0
#define	CSR_RM_RZ	1
#define	CSR_RM_RPI	2
#define	CSR_RM_RMI	3

/* vector numbers of specific MIPS exceptions */

#define IV_LOW_VEC              0       /* lowest vector initialized    */

#define IV_RESVD1_VEC           1       /* R4650: reserved entry        */
#define IV_IBND_VEC             2       /* R4650: instruction bound vector */
#define IV_DBND_VEC             3       /* R4650: data bound vector     */

#define IV_TLBMOD_VEC           1       /* tlb mod vector               */
#define IV_TLBL_VEC             2       /* tlb load vector              */
#define IV_TLBS_VEC             3       /* tlb store vector             */
#define IV_ADEL_VEC             4       /* address load vector          */
#define IV_ADES_VEC             5       /* address store vector         */
#define IV_BUS_VEC		6	/* CW4011: bus error vector	*/
#define IV_IBUS_VEC             6       /* instr. bus error vector      */
#define IV_RESVD7_VEC		7	/* CW4011: reserved entry	*/
#define IV_DBUS_VEC             7       /* data bus error vector        */
#define IV_SYSCALL_VEC          8       /* system call vector           */
#define IV_BP_VEC               9       /* break point vector           */
#define IV_RESVDINST_VEC        10      /* rsvd instruction vector      */
#define IV_CPU_VEC              11      /* coprocessor unusable vector  */
#define IV_OVF_VEC              12      /* overflow vector              */
#define IV_TRAP_VEC             13      /* trap vector                  */

#define IV_RESVD14_VEC          14      /* R4650: reserved entry        */

#define IV_VCEI_VEC             14      /* virtual coherency inst. vec  */
#define IV_FPE_VEC              15      /* floating point vector        */
#define IV_RESVD16_VEC          16      /* reserved entry               */
#define IV_RESVD17_VEC          17      /* reserved entry               */
#define IV_RESVD18_VEC          18      /* reserved entry               */
#define IV_RESVD19_VEC          19      /* reserved entry               */
#define IV_RESVD20_VEC          20      /* reserved entry               */
#define IV_RESVD21_VEC          21      /* reserved entry               */
#define IV_RESVD22_VEC          22      /* reserved entry               */
#define IV_WATCH_VEC            23      /* watchpoint vector            */
#define IV_RESVD24_VEC          24      /* reserved entry               */
#define IV_RESVD25_VEC          25      /* reserved entry               */
#define IV_RESVD26_VEC          26      /* reserved entry               */
#define IV_RESVD27_VEC          27      /* reserved entry               */
#define IV_RESVD28_VEC          28      /* reserved entry               */
#define IV_RESVD29_VEC          29      /* reserved entry               */
#define IV_RESVD30_VEC          30      /* reserved entry               */
#define IV_VCED_VEC             31      /* virtual coherency data vec   */
#define IV_SWTRAP0_VEC          32      /* software trap 0              */
#define IV_SWTRAP1_VEC          33      /* software trap 1              */
#define IV_FPA_UNIMP_VEC        54      /* unimplemented FPA oper       */
#define IV_FPA_INV_VEC          55      /* invalid FPA operation        */
#define IV_FPA_DIV0_VEC         56      /* FPA div by zero              */
#define IV_FPA_OVF_VEC          57      /* FPA overflow exception       */
#define IV_FPA_UFL_VEC          58      /* FPA underflow exception      */
#define IV_FPA_PREC_VEC         59      /* FPA inexact operation        */
#define IV_BUS_ERROR_VEC        60      /* bus error vector             */
#define USER_VEC_START          32      /* start of user interrupt vectors */
#define LOW_VEC                 32      /* lowest autovector initialized */
#define HIGH_VEC                255     /* highest  autovector initialized */
#define IV_FPA_BASE_VEC         IV_FPA_UNIMP_VEC

/*
 * These constants refer to word values
 */
#define	WORD_MIN	0x80000000
#define	WORD_MAX	0x7fffffff
#define	WEXP_MIN	-1
#define	WEXP_MAX	30
#define	WQUIETNAN_LEAST	0x7fffffff


/*
 * These constants refer to 64-bit long values
 */
#define	LONG_MIN_HIGH	0x80000000
#define	LONG_MIN_LOW	0x00000000
#define	LONG_MAX_HIGH	0x7fffffff
#define	LONG_MAX_LOW	0xffffffff
#define	LEXP_MIN	-1
#define	LEXP_MAX	62
#define	LQUIETNAN_LEAST_HIGH	0x7fffffff
#define	LQUIETNAN_LEAST_LOW	0xffffffff

/* instruction and sub-instruction opcode masks */

#define GENERAL_OPCODE_MASK	(077<<26)
#define SPECIAL_OPCODE_MASK 	077		/* and FPCP function mask */
#define BCOND_OPCODE_MASK	(0x1f<<16)
#define COPZ_OPCODE_MASK	(0x1f<<21)
#define COP0_OPCODE_MASK     	0x1f
#define COP1_OPCODE_MASK	(0xf<<22)
#define SHAMT_MASK		(0x1f<<6)
#define RT_POS			16          	/* bit position */
#define RT_MASK			(0x1f<<RT_POS)  /* target register mask */
#define RS_POS			21          	/* bit position */
#define RS_MASK			(0x1f<<RS_POS)  /* source register mask */
#define RD_POS	        	11          	/* bit position */
#define RD_MASK			(0x1f<<RD_POS)  /* destination register mask */
#define BEQ_MASK		(RS_MASK|RT_MASK)
#define REGIMM			(0x4<<24)
#define CC_POS			18
#define CC_MASK			(0x7<<CC_POS)

/* Instruction OP CODE definitions */

#define BEQ_INSTR		0x10000000
#define BNE_INSTR		0x14000000
#define BLEZ_INSTR		0x18000000
#define BGTZ_INSTR		0x1c000000
#define BEQL_INSTR		0x50000000
#define BNEL_INSTR		0x54000000
#define BLEZL_INSTR		0x58000000
#define BGTZL_INSTR		0x5c000000
#define	BC1F_INSTR		0x45000000
#define	BC1T_INSTR		0x45010000
#define	BC1FL_INSTR		0x45020000
#define	BC1TL_INSTR		0x45030000
#define BLTZ_INSTR		0x00000000
#define BGEZ_INSTR		0x00010000
#define BLTZL_INSTR		0x00020000
#define BGEZL_INSTR		0x00030000
#define BLTZAL_INSTR		0x00100000
#define BGEZAL_INSTR		0x00110000
#define BLTZALL_INSTR		0x00120000
#define BGEZALL_INSTR		0x00130000
#define	JR_INSTR		0x00000008
#define	JALR_INSTR		0x00000009
#define	J_INSTR			0x08000000
#define	JAL_INSTR		0x0c000000
#define	BREAK_INSTR		0x0000000d
#define LW_INSTR		0x8c000000
#define SW_INSTR		0xac000000
#define LD_INSTR		0xdc000000
#define SD_INSTR		0xfc000000
#define ADDIU_INSTR		0x24000000

/* Instruction decode definitions */

#define	OPCODE_POS	26
#define BCOND		0x04000000
#define SPECIAL		0x00000000
#define BASE_MASK	0x03e00000  /* base register mask */
#define BASE_POS	21          /* bit position */
#define OFFSET16_MASK	0x0000ffff  /* offset from the base register value */
#define TARGET_MASK	0x03ffffff  /* target of a J or JAL instruction */
#define	TARGET_POS	26	    /* bit position */
#define RA		31          /* return address register number */
#define SP		29          /* stack pointer register number */
#define BCOND_MASK	0x001f0000  /* Branch CONDitional op codes */
#define BCF_MASK	0x03ff0000  /* Branch on coprocessor false */
#define BCT_MASK	0x03ff0000  /* Branch on coprocessor true */
#define SPECIAL_MASK	0x0000003f  /* SPECIAL op codes */
#define	SPECIAL_POS	26
#define BREAK_CODE_MASK	0x00ff0000
#define BREAK_CODE_POS	16
#define	IMMEDIATE_POS	16
#define	CP1_VALUE	0x800000


#if (_WRS_INT_REGISTER_SIZE == 4)
#define SW	sw
#define LW	lw
#define MFC0	mfc0
#define MTC0	mtc0
#elif (_WRS_INT_REGISTER_SIZE == 8)
#define SW	sd		/* storing machine registers */
#define LW	ld		/* loading machine registers */
#define MFC0	dmfc0		/* reading wide cop0 register */
#define MTC0	dmtc0		/* writing wide cop0 register */
#else	/* _WRS_INT_REGISTER_SIZE */
#error "invalid _WRS_INT_REGISTER_SIZE value"
#endif	/* _WRS_INT_REGISTER_SIZE */



/* FP_CONTEXT structure offsets */
#if (_WRS_FP_REGISTER_SIZE==4)

#define	FPX		0x0	/* OFFSET(FP_CONTEXT, fpx[0])		*/
#define	FP0		0x0	/* OFFSET(fpx[0])			*/
#define	FP1		0x4	/* OFFSET(fpx[1])			*/
#define	FP2		0x8	/* OFFSET(fpx[2])			*/
#define	FP3		0xc	/* OFFSET(fpx[3])			*/
#define	FP4		0x10	/* OFFSET(fpx[4])			*/
#define	FP5		0x14	/* OFFSET(fpx[5])			*/
#define	FP6		0x18	/* OFFSET(fpx[6])			*/
#define	FP7		0x1c	/* OFFSET(fpx[7])			*/
#define	FP8		0x20	/* OFFSET(fpx[8])			*/
#define	FP9		0x24	/* OFFSET(fpx[9])			*/
#define	FP10		0x28	/* OFFSET(fpx[10])			*/
#define	FP11		0x2c	/* OFFSET(fpx[11])			*/
#define	FP12		0x30	/* OFFSET(fpx[12])			*/
#define	FP13		0x34	/* OFFSET(fpx[13])			*/
#define	FP14		0x38	/* OFFSET(fpx[14])			*/
#define	FP15		0x3c	/* OFFSET(fpx[15])			*/
#define	FP16		0x40	/* OFFSET(fpx[16])			*/
#define	FP17		0x44	/* OFFSET(fpx[17])			*/
#define	FP18		0x48	/* OFFSET(fpx[18])			*/
#define	FP19		0x4c	/* OFFSET(fpx[19])			*/
#define	FP20		0x50	/* OFFSET(fpx[20])			*/
#define	FP21		0x54	/* OFFSET(fpx[21])			*/
#define	FP22		0x58	/* OFFSET(fpx[22])			*/
#define	FP23		0x5c	/* OFFSET(fpx[23])			*/
#define	FP24		0x60	/* OFFSET(fpx[24])			*/
#define	FP25		0x64	/* OFFSET(fpx[25])			*/
#define	FP26		0x68	/* OFFSET(fpx[26])			*/
#define	FP27		0x6c	/* OFFSET(fpx[27])			*/
#define	FP28		0x70	/* OFFSET(fpx[28])			*/
#define	FP29		0x74	/* OFFSET(fpx[29])			*/
#define	FP30		0x78	/* OFFSET(fpx[30])			*/
#define	FP31		0x7c	/* OFFSET(fpx[31])			*/
#define	FPCSR		0x80	/* OFFSET(FP_CONTEXT, fpcsr)		*/

#elif ( _WRS_FP_REGISTER_SIZE==8)

#define	FPX		0x0	/* OFFSET(FP_CONTEXT, fpx[0])		*/
#define	FP0		0x0	/* OFFSET(fpx[0])			*/
#define	FP1		0x8	/* OFFSET(fpx[1])			*/
#define	FP2		0x10	/* OFFSET(fpx[2])			*/
#define	FP3		0x18	/* OFFSET(fpx[3])			*/
#define	FP4		0x20	/* OFFSET(fpx[4])			*/
#define	FP5		0x28	/* OFFSET(fpx[5])			*/
#define	FP6		0x30	/* OFFSET(fpx[6])			*/
#define	FP7		0x38	/* OFFSET(fpx[7])			*/
#define	FP8		0x40	/* OFFSET(fpx[8])			*/
#define	FP9		0x48	/* OFFSET(fpx[9])			*/
#define	FP10		0x50	/* OFFSET(fpx[10])			*/
#define	FP11		0x58	/* OFFSET(fpx[11])			*/
#define	FP12		0x60	/* OFFSET(fpx[12])			*/
#define	FP13		0x68	/* OFFSET(fpx[13])			*/
#define	FP14		0x70	/* OFFSET(fpx[14])			*/
#define	FP15		0x78	/* OFFSET(fpx[15])			*/
#define	FP16		0x80	/* OFFSET(fpx[16])			*/
#define	FP17		0x88	/* OFFSET(fpx[17])			*/
#define	FP18		0x90	/* OFFSET(fpx[18])			*/
#define	FP19		0x98	/* OFFSET(fpx[19])			*/
#define	FP20		0xa0	/* OFFSET(fpx[20])			*/
#define	FP21		0xa8	/* OFFSET(fpx[21])			*/
#define	FP22		0xb0	/* OFFSET(fpx[22])			*/
#define	FP23		0xb8	/* OFFSET(fpx[23])			*/
#define	FP24		0xc0	/* OFFSET(fpx[24])			*/
#define	FP25		0xc8	/* OFFSET(fpx[25])			*/
#define	FP26		0xd0	/* OFFSET(fpx[26])			*/
#define	FP27		0xd8	/* OFFSET(fpx[27])			*/
#define	FP28		0xe0	/* OFFSET(fpx[28])			*/
#define	FP29		0xe8	/* OFFSET(fpx[29])			*/
#define	FP30		0xf0	/* OFFSET(fpx[30])			*/
#define	FP31		0xf8	/* OFFSET(fpx[31])			*/
#define	FPCSR		0x100	/* OFFSET(FP_CONTEXT, fpcsr)		*/
#define	FPEXTRA		0x108	/* OFFSET(FP_CONTEXT, fpxtra)		*/

#else	/* _WRS_FP_REGISTER_SIZE */
#error "invalid _WRS_FP_REGISTER_SIZE value"
#endif	/* _WRS_FP_REGISTER_SIZE */

#endif
//1.FRAMER1(softFp)(sp)
//#define FRAMER1(routine) FRAMER(routine,1)
//2.FRAMER(softFp,1)(sp)
//#define FRAMER(routine,regn) 	(FRAMEASZ(routine)+(_RTypeSize)*(regn))
//3.(FRAMEASZ(softFp)+(8)*(1))(sp)
//#define FRAMEASZ(routine)	_##routine##ARsize
//(_softFpARsize+(8)*(1))(sp)
#endif
