
/*******************************************************************************
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义用户保留内存管理函数.
 * 修改：
 * 		 07/30/2013, 宋伟, 创建. 
 ******************************************************************************/

#ifndef _REWORKS_UMEM_H_
#define _REWORKS_UMEM_H_

#ifdef __cplusplus
extern "C"
{
#endif

extern int umem_init(int size);
extern unsigned int  umem_malloc(unsigned int bytes);
extern unsigned int  umem_memalign(unsigned int bytes, int aligment);

#ifdef __cplusplus
}
#endif

#endif /* __UMEM_H__ */
