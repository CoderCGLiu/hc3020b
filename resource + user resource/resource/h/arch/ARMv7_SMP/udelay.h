#ifndef _REWORKS_UDELAY_H_
#define _REWORKS_UDELAY_H_
#ifdef __cplusplus
extern "C"
{
#endif 
	
extern void udelay(u32);

#ifdef __cplusplus
}
#endif 

#endif /* _REWORKS_UDELAY_H_ */
