#ifndef _REWORKS_ARCH_IRQ_H_
#define _REWORKS_ARCH_IRQ_H_

#ifdef __cplusplus
extern "C" {
#endif


/*******************************************************************************
 * 
 * BSP中断控制器需注册的操作接口
 * 
 * 		BSP中断控制器需注册的操作接口在BSP中实现，并通过注册接口注册到CSP以供中断框架
 * 调用。BSP中断控制器需注册的操作接口一般需要调用内部中断控制器（如果存在，在CSP文档
 * 中说明）和外部中断控制器（由BSP实现，如果存在）的相应操作。
 * 
 * 
 */
 
#define  IRQ_NUM  (1020)
/**
 * 中断响应函数类型 
 */
typedef int(* INT_ACK_FUNCPTR)(int);

/**
 * 中断使能函数类型 
 */
typedef int (* INT_ENBALE_FUNCPTR)(int);

/**
 * 中断屏蔽函数类型
 */
typedef int (* INT_DISBALE_FUNCPTR)(int);

/**
 * 获取中断号函数类型
 */
//typedef int (* INT_INUM_FUNCPTR)(void);

/**
 * 初始化中断控制器接口
 * （返回值为中断控制器支持的中断数量）
 */
typedef int (* INT_INIT_FUNPTR)(void);

/**
 * 定义中断接口结构体
 */
//struct int_operations
//{
//	INT_ACK_FUNCPTR 			ack;
//	INT_ENBALE_FUNCPTR			enable;
//	INT_DISBALE_FUNCPTR			disable;
//	INT_INUM_FUNCPTR			inum;
//	INT_INIT_FUNPTR				init;
//};
	
/* 硬件中断操作注册接口 */
//int int_operations_register(struct int_operations *ops);

int exception_module_init();

#ifdef __cplusplus
}
#endif
#endif /* _REWORKS_ARCH_IRQ_H_ */


