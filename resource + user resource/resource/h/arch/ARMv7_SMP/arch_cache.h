
#ifndef _REWORKS_ARCH_CACHE_H_
#define _REWORKS_ARCH_CACHE_H_

#include <reworks/types.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef	unsigned int	RE_CACHE_MODE;		/* CACHE_MODE */
    
#define	ENTIRE_CACHE	(__LONG_MAX__ * 2UL + 1UL)
/* Cache mode soft bit masks */
#define	CACHE_DISABLED			0x00	/* No cache or disabled */
#define	CACHE_WRITETHROUGH		0x01	/* Write-through Mode */
#define	CACHE_COPYBACK			0x02	/* Copyback Mode */
#define	CACHE_WRITEALLOCATE		0x04	/* Write Allocate Mode */
#define	CACHE_NO_WRITEALLOCATE	0x08
#define	CACHE_SNOOP_ENABLE		0x10	/* Bus Snooping */
#define	CACHE_SNOOP_DISABLE		0x20
#define	CACHE_BURST_ENABLE		0x40	/* Cache Burst Cycles */
#define	CACHE_BURST_DISABLE		0x80


#ifdef __cplusplus
}
#endif

#endif
