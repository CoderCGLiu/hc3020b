/*! @file fpu.h
 * 
 *  @brief 浮点任务支持功能头文件
 *  
 *  本文件定义了浮点寄存器上下文结构，	以及浮点运算支持函数声明。
 *  
 * 版权：
 *      中国电子科技集团公司第三十二研究所
 * 修改：
 *      2016/2/25, 董方秀创建
 *
 */
#ifndef _REWORKS_FPU_H_
#define _REWORKS_FPU_H_

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _ASMLANGUAGE	

#include <reworks/types.h>

#ifndef HAVE_FP_CONTEXT
#define HAVE_FP_CONTEXT
/* cortex a8 vfpv3-D32
 * cortex a9 vfpv3-D16
 * 为与任务调试和系统调试中的定义保持统一，将浮点寄存器个数设置为32
 * cortex a9 只用前16个*/
#define FP_NUM_DREGS	32

typedef u64 fpureg_t;

typedef struct _Fp_Context
{
	fpureg_t vfp_reg[FP_NUM_DREGS];
	u32 fpscr;			/*status and control register*/
	u32 pad;			/*pad to match 8 byte alignment*/	
} FP_CONTEXT;

typedef FP_CONTEXT Fp_Context;

#endif

/*
 *  arm_fpu_context_save
 *
 *  This routine saves the floating point context passed to it.
 */
void arm_fpu_context_save(Fp_Context *fp_context_ptr);

/*
 *  arm_fpu_context_save
 *
 *  This routine restores the floating point context passed to it.
 */
void arm_fpu_context_restore(Fp_Context *fp_context_ptr);

void fpu_init(void);

void fpu_context_dump(Fp_Context *context);

int fpu_module_init(void);

#endif /* #ifndef ASM_LANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /*_CSP_FPU_H_*/
