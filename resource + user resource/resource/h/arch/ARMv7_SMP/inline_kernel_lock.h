/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了CSP模块中pentium体系结构中断管理的接口
 * 修改：
 * 		 1. 2014-07-15，规范CSP 
 */
#ifndef _REWORKS_KERNEL_LOCK_H_
#define _REWORKS_KERNEL_LOCK_H_

#ifdef __cplusplus
extern "C" {
#endif
	
#include <reworks/types.h>
#include <irq.h>

__attribute__((always_inline)) static inline boolean inline_if_key_enabling_int(int key)
{
	return ((key & 0xc0) == 0x0);//hw 暂时设为FIQ和IRQ都没屏蔽
}

__attribute__((always_inline)) static inline int inline_int_lock()
{
    int level;
    int reg;
    asm volatile ("mrs	%0, cpsr \n"
                  "orr  %1, %0, #0xc0 \n"
                  "msr  cpsr, %1 \n"
                   : "=&r" (level), "=&r" (reg)
                   ::"memory");

    return level;
}


__attribute__((always_inline)) static inline void inline_simple_int_unlock(int level)
{
    asm volatile ( "msr cpsr_c,%0 \n" : : "r"(level):"memory");
    asm volatile("msr spsr_c,%0 \n" : : "r"(level));
}

//__attribute__((always_inline)) static inline void inline_full_int_unlock(int key)
//{
//	register int temp;
//	
//    asm volatile
//	(
//	"mrs  	%0, cpsr\n"
//	"and  	%1,%1,#0xc0\n"
//	"orr	%1,%0,%1\n"/*cpsr I F位是0有效，因此不能用orr*/
//	"msr	cpsr, %1\n"
//	: "=&r" (temp)
//	: "r" (key)
//	: "memory"			/* memory */
//	);
//}

/* huangyuan20140409：inline_int_flash相当于CPU_CRITICAL_EXIT_SWITCH_INCLUDED与
 * CPU_CRITICAL_ENTER的结合， 必须仔细思考放开中断调度切换之后，状态寄存器中断使能位
 * 之外的位是否有全局性修改的情况。 */
__attribute__((always_inline)) static inline void inline_int_flash()
{
	register int temp1, temp2;
    
    asm volatile
	(
	"mrs  	%0, cpsr\n"
	"orr	%1, %0, #0xc0\n"/*关中断*/
	"mvn	%0, #0xc0\n"
	"and	%0, %0, %1\n"/*开中断*/		
	"msr	cpsr, %0\n"
	"isb \n"        /*为了保守目的而加，可以去掉*/
	"dsb \n" 		  /*为了保守目的而加，可以去掉*/
	"msr	cpsr, %1\n"
	: "=&r" (temp1), "=&r" (temp2)
	: 
	: "memory"			/* memory */
	);
}

#define CPU_CRITICAL_ENTER(level) 					level = inline_int_lock()
#define CPU_CRITICAL_EXIT(level) 					inline_simple_int_unlock(level)
#define CPU_CRITICAL_EXIT_SWITCH_INCLUDED(level) 	inline_simple_int_unlock(level)

#ifdef __cplusplus
}
#endif

#endif
