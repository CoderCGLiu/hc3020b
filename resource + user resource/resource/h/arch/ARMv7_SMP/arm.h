/**
 * @file    arm.h
 * @brief   ARM架构定义头文件
 * @note    2016-2-24，庄金峰，用Doxygen格式将源代码文档化
 */
#ifndef _REWORKS_ARM_H_
#define _REWORKS_ARM_H_

#ifdef __cplusplus
extern "C" {
#endif

/* bits in the PSR */
#define	V_BIT			(1<<28)
#define	C_BIT			(1<<29)
#define	Z_BIT			(1<<30)
#define	N_BIT			(1<<31)
#define I_BIT   		(1<<7)
#define F_BIT   		(1<<6)
#define	T_BIT			(1<<5)


/* The coprocessor number of the MMU System Control Processor */
#define CP_MMU 			15

/*
 * Arm Arch HW specific bits used by cache routines...
 */
#define CACHE_MM_ENABLE   (1<<0)    /* MMU enable */
#define CACHE_DC_ENABLE   (1<<2)    /* (data) cache enable */
#define CACHE_WB_ENABLE   (1<<3)    /* write buffer enable */
#define CACHE_IC_ENABLE   (1<<12)   /* Instruction cache enable */

#ifdef __cplusplus
}
#endif

#endif
