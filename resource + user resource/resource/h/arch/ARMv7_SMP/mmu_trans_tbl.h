#ifndef _ARMV7_MMU_TRANS_TBL_H_
#define _ARMV7_MMU_TRANS_TBL_H_


#ifndef _ASMLANGUAGE

#include <reworks/types.h>

/* little-endian */
/* First level page descriptors */
typedef struct
{
    u32 type   : 2;        /* descriptor type, 1 => page */
    u32 pad1   : 2;        /* SBZ */
    u32 pad2   : 1;        /* SBO */
    u32 domain : 4;        /* domain number */
    u32 pbit   : 1;        /* 'P' bit */
    u32 addr   : 22;       /* base address of page table */
} PAGE_DESC_FIELD;

/* First level "section" descriptor */
typedef struct
{
    u32 type   : 2;        /* descriptor type, 2 => section */
    u32 bbit   : 1;        /* bufferable-bit */
    u32 cbit   : 1;        /* cacheable-bit */
    u32 pad1   : 1;        /* SBO */
    u32 domain : 4;        /* domain number */
    u32 pbit   : 1;        /* XSCALE 'P' bit */
    u32 ap 	   : 2;        	/* AP */
    u32 tex    : 3;        /* TEX */
    u32 apx    : 1;        /* APX */
    u32 sbit   : 1;        /* S-bit */
    u32 pad3   : 1;        /* SBO */
    u32 super  : 1;        /* 1 => supersection */
    u32 pad4   : 1;        /* SBO */
    u32 addr   : 12;       /* base address of page table */
} SECTION_DESC_FIELD;

/* First level "supersection" descriptor */
typedef struct
{
    u32 type   : 2;        /* descriptor type, 2 => section */
    u32 bbit   : 1;        /* bufferable-bit */
    u32 cbit   : 1;        /* cacheable-bit */
    u32 pad1   : 1;        /* SBO */
    u32 addrHi2: 4;        /* phys address 39:36 */
    u32 pbit   : 1;        /* XSCALE 'P' bit */
    u32 ap 	   : 2;        /* AP */
    u32 tex    : 3;        /* TEX */
    u32 apx    : 1;        /* APX */
    u32 sbit   : 1;        /* S-bit */
    u32 pad3   : 1;        /* SBO */
    u32 super  : 1;        /* 1 => supersection */
    u32 pad4   : 1;        /* SBO */
    u32 addrHi : 4;        /* phys address 35:32 */
    u32 addrLo : 8;        /* phys address 31:24 */
} SUPERSECTION_DESC_FIELD;

/* Layout of Page Table Entries (PTEs), actually small page descriptors */
typedef struct
{
    u32 type   : 2;            /* page type, 3 => extended small page */
    u32 cb     : 2;            /* cacheable/bufferable bits */
    u32 ap     : 2;            /* access permission */
    u32 tex    : 3;            /* type extension field */
    u32 apx    : 1;        	/* access permission extension field */
    u32 sbz    : 2;            /* should be zero */
    u32 addr   : 20;           /* page base address */
} PTE_FIELD;

/* First level descriptor access */
typedef union {
    PAGE_DESC_FIELD         fields;
    SECTION_DESC_FIELD      sectionFields;
    SUPERSECTION_DESC_FIELD supersectionFields;
    u32                  	bits;	
}MMU_LEVEL_1_DESC;

/* Second level descriptor access */
typedef union
{
    PTE_FIELD fields;
    u32 	  bits;
} PTE;

/*
 * Structure for MMU translation table access - normally used to get a pointer
 * to the first-level translation table.
 * To support page table optimization, a second first-level table and a
 * semaphore are needed.
 * The initial, unoptimized set of page tables will be set as the active
 * translation tables (via pLevel1Table) while optimization or deoptimization
 * is in progress using the inactive second translation table (pAltLevel1Table).
 * After each page table optimization or deoptimization operation has
 * completed, the second translation table will be set as the active
 * translation table.  The initial set of page tables retain their
 * links to level 2 pages, which may otherwise be lost after optimization.
 * Any non-optimization changes to the active first-level table will also be
 * made to the inactive first-level table.
 * The semaphore is used to prevent corruption of the tables.
 */
typedef struct {
	MMU_LEVEL_1_DESC	*pLevel1Table;
	/* ... for RTP */
}MMU_TRANS_TBL;

#endif /* _ASMLANGUAGE */

#endif
