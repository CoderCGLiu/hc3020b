#ifndef _REWORKS_BD_OPT_H_
#define _REWORKS_BD_OPT_H_
#include <reworks/types.h>
typedef struct {
	    u32 r0;
	    u32 r1;
	    u32 r2;
	    u32 r3;
	    u32 r4;
	    u32 r5;
	    u32 r6;
	    u32 r7;
	    u32 r8;
	    u32 r9;
	    u32 r10;
	    u32 fp;/*register_11*/
	    u32 r12;   
	    u32 lr;/*register_14*/
	    u32 cpsr;
	    u32 pc;/*register_15*/
	    u32 esp;
} Context_Ctrl_BD;
#endif
