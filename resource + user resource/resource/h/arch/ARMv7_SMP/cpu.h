#ifndef _REWORKS_CPU_H_
#define _REWORKS_CPU_H_

#ifdef __cplusplus
extern "C" {
#endif

#ifndef __multi_core__
#define MAX_SMP_CPUS	1
#else
#define MAX_SMP_CPUS	32
#endif

#ifndef _ASMLANGUAGE

#include <reworks/types.h>
#include <machine/endian.h>
	
#define CPU_STACK_MIN_SIZE          4096
#define CPU_ALIGNMENT				4
#define CPU_EFLAGS_INTERRUPTS_ON  	0x53
#define CPU_EFLAGS_INTERRUPTS_OFF 	0xd3

#ifdef  _LITTLE_ENDIAN	
#undef  _LITTLE_ENDIAN
#endif

#ifdef  _BIG_ENDIAN
#undef  _BIG_ENDIAN
#endif

#define _LITTLE_ENDIAN		LITTLE_ENDIAN
#define _BIG_ENDIAN			BIG_ENDIAN
#define	_BYTE_ORDER			_LITTLE_ENDIAN

#ifdef BYTE_ORDER
#undef BYTE_ORDER
#define BYTE_ORDER	_BYTE_ORDER
#endif

/*  The following routine swaps the endian format of an unsigned int.
 *  It must be static because it is referenced indirectly.
 *
 *  This version will work on any processor, but if there is a better
 *  way for your CPU PLEASE use it.  The most common way to do this is to:
 *
 *     swap least significant two bytes with 16-bit rotate
 *     swap upper and lower 16-bits
 *     swap most significant two bytes with 16-bit rotate
 *
 *  Some CPUs have special instructions which swap a 32-bit quantity in
 *  a single instruction (e.g. i486).  It is probably best to avoid
 *  an "endian swapping control bit" in the CPU.  One good reason is
 *  that interrupts would probably have to be disabled to insure that
 *  an interrupt does not try to access the same "chunk" with the wrong
 *  endian.  Another good reason is that on some CPUs, the endian bit
 *  endianness for ALL fetches -- both code and data -- so the code
 *  will be fetched incorrectly.
 */

 
static inline unsigned int CPU_swap_u32(
  unsigned int value
)
{
    unsigned32 tmp = value; /* make compiler warnings go away */
    asm volatile ("EOR   %1, %0, %0, ROR #16\n" 
    			  "BIC   %1, %1, #0xff0000\n"   
                  "MOV   %0, %0, ROR #8\n"      
                  "EOR   %0, %0, %1, LSR #8\n"  
                  : "=r" (value), "=r" (tmp)  
                  : "0" (value), "1" (tmp));

    return value;
}

static inline unsigned16 CPU_swap_u16(unsigned16 value)
{
    unsigned16 lower;
    unsigned16 upper;

    value = value & (unsigned16) 0xffff;
    lower = (value >> 8) ;
    upper = (value << 8) ;

    return (lower | upper);
}


/*
 * Save the current interrupt enable state & disable IRQs
 */
#define local_irq_save(x)					\
	({							\
		unsigned long temp;				\
	__asm__ __volatile__(					\
	"mrs	%0, cpsr		@ local_irq_save\n"	\
"	orr	%1, %0, #128\n"					\
"	msr	cpsr_c, %1"					\
	: "=r" (x), "=r" (temp)					\
	:							\
	: "memory");						\
	})

/*
 * restore saved IRQ & FIQ state
 */
#define local_irq_restore(x)					\
	__asm__ __volatile__(					\
	"msr	cpsr_c, %0		@ local_irq_restore\n"	\
	:							\
	: "r" (x)						\
	: "memory")

typedef struct {
  void       (*idle_task)( void );
  boolean      do_zero_of_workspace;
  u32   idle_task_stack_size;
  u32   interrupt_stack_size;
}   rtos_cpu_table;

/*Data structure for exception handler by hy*/
typedef struct { 
  u32  edi;
  u32  esi;
  u32  ebp;
  u32  esp;
  u32  ebx;
  u32  edx;
  u32  ecx;
  u32  eax;
  u32  hdl;
  u32  vector;
  u32  error_code;
  u32  eip;  
} pt_regs_t;

typedef struct {
    u32 r0;
    u32 r1;
    u32 r2;
    u32 r3;
    u32 r4;
    u32 r5;
    u32 r6;
    u32 r7;
    u32 r8;
    u32 r9;
    u32 r10;
    u32 fp;/*register_11*/
    u32 r12;   
   /* u32 register_sp;*//*register_13*/
    u32 lr;/*register_14*/
    u32 cpsr;
    u32 pc;/*register_15*/
 } Context_Ctrl;
 
 typedef struct {
    u32 r0;
    u32 r1;
    u32 r2;
    u32 r3;
    u32 r4;
    u32 r5;
    u32 r6;
    u32 r7;
    u32 r8;
    u32 r9;
    u32 r10;
    u32 fp;/*register_11*/
    u32 r12;   
    u32 lr;/*register_14*/
    u32 cpsr;
    u32 pc;/*register_15*/
    u32 esp;
 } REG_SET;

typedef struct
{
    int     cpuid_max;	    	/* EAX=0: highest integer value */
    char    cpu_vendor[13];	/* EAX=0: vendor identification string */
    int     cpu_family;
    int     cpu_ext_family;
    int     cpu_model;
    int     cpu_step;
    int     feature_ebx;		/* EAX=1: feature flags EBX */
    int     feature_ecx;		/* EAX=1: feature flags ECX */
    int     feature_edx;		/* EAX=1: feature flags EDX */
    int     cache_eax;		/* EAX=2: config parameters EAX */
    int     cache_ebx;		/* EAX=2: config parameters EBX */
    int     cache_ecx;		/* EAX=2: config parameters ECX */
    int     cache_edx;		/* EAX=2: config parameters EDX */
    int     serial_no[2];		/* EAX=3: lower 64 of 96 bit serial no */
    int     amd64_supported;
    char    cpu_brandstr[48];	/* EAX=0x8000000[234]: brand strings */
} Cpuid_Ctrl;

void context_init( Context_Ctrl *the_context, u32  isr,void *entry_point, void *prev_entry); 
void context_switch(Context_Ctrl **run,Context_Ctrl *heir);
void context_restore(Context_Ctrl *heir);

/**
 * 获取处理器时间戳接口
 */
extern u64 sys_timestamp(void);

/**
 * 获取处理器频率的接口
 */
extern u32 sys_timestamp_freq(void);

/*ref DDI0406B_arm_architecture_reference_manual.pdf
 * dmb: Data Memory Barrier is a memory barrier that ensures the ordering of observations of memory accesses*/

#define MEM_BARRIER_R() __asm volatile ("dmb" ::: "memory")
#define MEM_BARRIER_W() __asm volatile ("dmb" ::: "memory")
#define MEM_BARRIER_RW() __asm volatile ("dmb" ::: "memory")

#define isb(option) __asm__ __volatile__ ("isb " #option : : : "memory")
#define dsb(option) __asm__ __volatile__ ("dsb " #option : : : "memory")
#define dmb(option) __asm__ __volatile__ ("dmb " #option : : : "memory")

#define smp_mb()	dmb(ish)
#define smp_rmb()	smp_mb()
#define smp_wmb()	dmb(ishst)

#endif

#ifdef __cplusplus
}
#endif

#endif


