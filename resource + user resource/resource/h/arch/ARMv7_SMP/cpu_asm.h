/**
 * @file    cpu_asm.h
 * @brief   ARM架构定义头文件
 * @note    2016-2-24，庄金峰，用Doxygen格式将源代码文档化
 */
#ifndef _REWORKS_CPU_ASM_H_
#define _REWORKS_CPU_ASM_H_

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _ASMLANGUAGE
/* Registers saved in context switch: */
.set REG_CPSR,   0
.set REG_R4,     4
.set REG_R5,     8
.set REG_R6,     12
.set REG_R7,     16
.set REG_R8,     20
.set REG_R9,     24
.set REG_R10,    28
.set REG_R11,    32
.set REG_SP,     36
.set REG_LR,     40
.set REG_PC,     44
.set SIZE_REGS,  REG_PC + 4
#endif

#ifdef __cplusplus
}
#endif

#endif
