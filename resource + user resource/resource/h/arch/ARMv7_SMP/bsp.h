
/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义BSP中使用到的宏定义等，这些信息和BSP软件实现有关，由BSP开发人员根据
 *       应用场景和实际开发情况定义，这些定义是自由的。
 * 
 */

#ifndef _REWORKS_BSP_H_
#define _REWORKS_BSP_H_

/* Define Byte size */
#define SZ_4KB				(  4 << 10)
#define SZ_8KB				(  8 << 10)
#define SZ_16KB				( 16 << 10)
#define SZ_32KB				( 32 << 10)
#define SZ_64KB				( 64 << 10)
#define SZ_128KB			(128 << 10)
#define SZ_256KB			(256 << 10)
#define SZ_512KB			(512 << 10)
#define SZ_1MB				(  1 << 20)
#define SZ_2MB				(  2 << 20)
#define SZ_4MB				(  4 << 20)
#define SZ_8MB				(  8 << 20)
#define SZ_16MB				( 16 << 20)	
#define SZ_32MB				( 32 << 20)
#define SZ_64MB				( 64 << 20)
#define SZ_128MB			(128 << 20)
#define SZ_256MB			(256 << 20)
#define SZ_512MB			(512 << 20)
#define SZ_1GB				(  1 << 30)		
#define SZ_2GB				(  2 << 30)	

#ifdef __cplusplus
extern "C"
{
#endif
	


#ifdef __cplusplus
}
#endif

#endif
