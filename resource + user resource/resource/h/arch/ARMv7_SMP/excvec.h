
/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：定义异常管理框架中用到的宏和结构体。
 * 修改：       
 * 		2014-03-07, 宋伟, 创建. 
 */

#ifndef _REWORKS_EXCVEC_H_
#define _REWORKS_EXCVEC_H_

#ifdef __cplusplus
extern "C" {
#endif



/*异常向量号*/
#define EXC_CODE_UDFI	1       /*undefined instruction exception*/
#define EXC_CODE_SWI	2		/*software interrupt exception*/
#define EXC_CODE_DABT	3		/*data abort exception*/
#define	EXC_CODE_PFABT	4		/*pre-fetch abort exception*/

#ifdef __cplusplus
}
#endif
#endif
