#ifndef _REWORKS_CORE_DELAY_H_
#define _REWORKS_CORE_DELAY_H_

#ifdef __cplusplus
extern "C" {
#endif

#define LPS_PREC 8  										// 校准精度，精度越高校准时间越长
#define LPS_STEP (1<<1)
#define OPTIMIZE(level) __attribute__((optimize(level)))    // 为防止udelay函数被优化，故定义还宏用以修改udelay相关函数

OPTIMIZE("O0") inline void __delay(volatile unsigned int loops)
{
	__asm__ __volatile__ (
	"	.set	noreorder				\n"
	"	.align	3					\n"
	"1:	bnez	%0, 1b					\n"
	"	subu	%0, 1					\n"
	"	.set	reorder					\n"
	: "=r" (loops)
	: "0" (loops));
}

static __attribute__((always_inline)) void __delay_2(volatile unsigned int us)
{
	extern u32 g_udelay_loop_count;
	__delay(us * g_udelay_loop_count);//g_udelay_loop_count 为DM3730测试出来的参考值
}

#ifdef __cplusplus
}
#endif
#endif /* _REWORKS_CORE_DELAY_H_ */
