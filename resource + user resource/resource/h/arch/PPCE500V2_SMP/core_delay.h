#ifndef _REWORKS_CORE_DELAY_H_
#define _REWORKS_CORE_DELAY_H_

#ifdef __cplusplus
extern "C" {
#endif

#define LPS_PREC 8  										// 校准精度，精度越高校准时间越长
#define OPTIMIZE(level) __attribute__((optimize(level)))    // 为防止udelay函数被优化，故定义还宏用以修改udelay相关函数
#define LPS_STEP (1<<1)										// udelay延迟单元步进，可调节延迟精度

OPTIMIZE("O0") static inline void __delay(volatile unsigned int loops)
{
	__asm volatile(
            "   addze 7,7;		"		/* clear carry */
            "   mtctr %0;		"		/* load loop count */
            "1:				"
            "   nop;			"		
            "   nop;			"
            "   nop;			"		
            "   nop;			"
            "   bdnz 1b;		"		/* loop */
            : "+r"(loops)
            : "r"(loops)
            : "7", "8");				/* clobber r7, r8 */	
}

u64 static inline __sys_timestamp (void)
{
	union
	{
		u64 time64;
		struct
		{
			u32 tbu;
			u32 tbl;
		}time32;
	}tm;
	
	asm volatile( "mftb  %0" : "=r" (tm.time32.tbl) );
	asm volatile( "mftbu  %0" : "=r" (tm.time32.tbu) );
	
	return tm.time64;
}

extern u32 bsp_clicks_per_usec;	// 每一微妙多少个CLK，该CLK即为DEC和TB的时钟频率。

static inline void __delay_2(u32 us)
{
	u64 endTime = bsp_clicks_per_usec * us + __sys_timestamp(); 
	volatile u64 t;

	do
	{
		t = __sys_timestamp();
	}while(t < endTime);
}
#ifdef __cplusplus
}
#endif
#endif /* _REWORKS_CORE_DELAY_H_ */
