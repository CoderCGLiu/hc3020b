#ifndef _REWORKS_INLINE_CPU_ID_GET_H_
#define _REWORKS_INLINE_CPU_ID_GET_H_

#ifdef __cplusplus
extern "C" {
#endif

__attribute__((always_inline)) static inline int inline_cpu_id_get()
{
	
    register int number;
    
    asm volatile
	(
	"mfspr  %0, 286\n"
	: "=&r" (number)		/* output: key is %0 */
	:					/* no input */
	: "memory"			/* memory */
						/* clobber for code barrier */
	);
    
    return number;
}

#ifdef __cplusplus
}
#endif
#endif /* _REWORKS_INLINE_CPU_ID_GET_H_ */
