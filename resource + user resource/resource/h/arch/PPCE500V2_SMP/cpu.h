/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了CSP模块中PPC603体系结构处理器定义
 * 修改：
 * 		 1. 2011-10-31，规范CSP 
 */
#ifndef _REWORKS_CPU_H_
#define _REWORKS_CPU_H_

#ifdef __cplusplus
extern "C" {
#endif
#include <machine/endian.h>

#ifdef  _LITTLE_ENDIAN					/* 网络头文件中需要使用 */
#undef  _LITTLE_ENDIAN
#endif

#ifdef  _BIG_ENDIAN
#undef  _BIG_ENDIAN
#endif

#define _LITTLE_ENDIAN    LITTLE_ENDIAN
#define _BIG_ENDIAN       BIG_ENDIAN
#define _BYTE_ORDER       _BIG_ENDIAN

#ifdef BYTE_ORDER
#undef BYTE_ORDER
#define BYTE_ORDER	_BYTE_ORDER
#endif

#define PPC_REGS_SIZE    0xa8 /* 保持和CPU_ALIGNMENT一致对齐 */

 /*             
  *	栈指针SP会在三种情况下发生变化：中断、异常、函数调用。      
  *	SP指向的空间总是保存本函数调用前的SP空间，即前一帧。
  * 所不同的是，中断和函数调用时使用的栈是任务栈，任何情况下都是可逆的，即可以从中断
  * 返回被中断任务或从当前函数返回上一层函数；而异常栈总是重复使用的，是不可逆的，
  * 即新异常产生时会覆盖原异常时的栈内容。
  
   HIGH +-----------------+--------------------------
	|   |     SP          |    
	|   |-----------------|
	|   |     DAR         |
	|   |-----------------|
	|   |     DSISR       |
	|   |-----------------|
	|   |     MSR         |	    
   \|/  |-----------------|
    |   |     CTR         |	    
   LOW  |-----------------|
	    |     XER         |	    
	    |-----------------|
	    |     SRR0/SPRG0  |	    
	    |-----------------|
	    |     CR          |	    
	    |-----------------|
	    |     R5 - R31    |
	    |-----------------|
	    |     R4/SPRG1    |
	    |-----------------|
	    |     R3/SPRG3    |
	    |-----------------|
	    |     R2          |
	    |-----------------|
	    |     R0          |
	    |-----------------|
	    |     SRR1/SPRG2  |
	    |-----------------|
	    |     LR          |
	    +-----------------+
	SP=>|     SP          |  中断和异常产生时的栈集
	    |=================|==========================
	    |    Registers    |
	    |-----------------|
		|    Local        |
	    |-----------------|		
		|    parameter    |				
	    |-----------------|		
		|   LR save word  |
	    |-----------------|		
	SP=>|   Back chain    |  函数调用时产生的栈
		|=================|==========================                                
        |     ......      |        
		     栈指针SP演化过程	
*/
	
/*the order of the saved register in the stack of the thread*/
#define GPR1  		(0)
#define GP_LR 		(0 + 4)
#define GP_SRR1 	(GP_LR + 4)
#define GPR0    	(GP_SRR1 + 4)
#define GPR2  		(GPR0 + 4)
#define GPR3  		(GPR2 + 4)
#define GPR4  		(GPR3 + 4)
#define GPR5  		(GPR4 + 4)
#define GPR6  		(GPR5 + 4)
#define GPR7  		(GPR6 + 4)
#define GPR8  		(GPR7 + 4)
#define GPR9  		(GPR8 + 4)
#define GPR10 		(GPR9 + 4)
#define GPR11 		(GPR10 + 4)
#define GPR12 		(GPR11 + 4)
#define GPR13 		(GPR12 + 4)
#define GPR14 		(GPR13 + 4)
#define GPR15 		(GPR14 + 4)
#define GPR16 		(GPR15 + 4)
#define GPR17 		(GPR16 + 4)
#define GPR18 		(GPR17 + 4)
#define GPR19 		(GPR18 + 4)
#define GPR20 		(GPR19 + 4)
#define GPR21	 	(GPR20 + 4)
#define GPR22 		(GPR21 + 4)
#define GPR23 		(GPR22 + 4)
#define GPR24 		(GPR23 + 4)
#define GPR25 		(GPR24 + 4)
#define GPR26 		(GPR25 + 4)
#define GPR27 		(GPR26 + 4)
#define GPR28 		(GPR27 + 4)
#define GPR29 		(GPR28 + 4)
#define GPR30 		(GPR29 + 4)
#define GPR31 		(GPR30 + 4)
#define GP_CR    	(GPR31 + 4)
#define GP_SRR0  	(GP_CR + 4)
#define GP_XER   	(GP_SRR0 + 4)
#define GP_CTR   	(GP_XER + 4)
#define GP_MSR   	(GP_CTR + 4)
#define GP_DSISR 	(GP_MSR + 4)
#define GP_DEAR   	(GP_DSISR + 4)
#define GP_DEC   	(GP_DAR + 4)

#ifndef _ASMLANGUAGE

#include <reworks/types.h>
	
#define CPU_STACK_MIN_SIZE          4096
#define CPU_ALIGNMENT				8
#define CPU_EFLAGS_INTERRUPTS_ON  	0xa000
#define CPU_EFLAGS_INTERRUPTS_OFF 	0x00

#define MAX_SMP_CPUS 2   //核的个数
int cpu_id_get();           

//内存屏障
#define MEM_BARRIER_R() asm volatile ("sync" ::: "memory")
#define MEM_BARRIER_W() asm volatile ("sync" ::: "memory")
#define MEM_BARRIER_RW() asm volatile ("sync" ::: "memory")

typedef struct
{
    char    cpu_vendor[13];	/* EAX=0: vendor identification string */
} Cpuid_Ctrl;

typedef struct 
{
  void       (*idle_task)( void );
  boolean      do_zero_of_workspace;
  u32   idle_task_stack_size;
  u32   interrupt_stack_size;
} rtos_cpu_table;


typedef struct 
{
	u32 gpr1;        /*0x0 up_level_func_sp*/
	u32 gp_lr;       /*0x4*/
    u32 gp_srr1;     /*0x8*/
    u32 gpr0;        /*0xc*/
    u32 gpr2;	     /*0x10*//* TOC in PowerOpen, reserved SVR4, section ptr EABI + */
    u32 gpr3;        /*0x14*/
    u32 gpr4;        /*0x18*/
    u32 gpr5;        /*0x1c*/
    u32 gpr6;		 /*0x20*/
    u32 gpr7;		 /*0x24*/
    u32 gpr8;   	 /*0x28*/
    u32 gpr9;		 /*0x2c*/
    u32 gpr10;	     /*0x30*/
    u32 gpr11;       /*0x34*/
    u32 gpr12;		 /*0x3c*/
    u32 gpr13;		 /*0x40*//* First non volatile PowerOpen, section ptr SVR4/EABI */
    u32 gpr14;	/*0x44*//* Non volatile for all */
    u32 gpr15;	/*0x48*//* Non volatile for all */
    u32 gpr16;	/*0x4c*//* Non volatile for all */
    u32 gpr17;	/*0x50*//* Non volatile for all */
    u32 gpr18;	/*0x54*//* Non volatile for all */
    u32 gpr19;	/*0x58*//* Non volatile for all */
    u32 gpr20;	/*0x5c*//* Non volatile for all */
    u32 gpr21;	/*0x60*//* Non volatile for all */
    u32 gpr22;	/*0x64*//* Non volatile for all */
    u32 gpr23;	/*0x68*//* Non volatile for all */
    u32 gpr24;	/*0x6c*//* Non volatile for all */
    u32 gpr25;	/*0x70*//* Non volatile for all */
    u32 gpr26;	/*0x74*//* Non volatile for all */
    u32 gpr27;	/*0x78*//* Non volatile for all */
    u32 gpr28;	/*0x7c*//* Non volatile for all */
    u32 gpr29;	/*0x80*//* Non volatile for all */
    u32 gpr30;	/*0x84*//* Non volatile for all */
    u32 gpr31;	/*0x88*//* Non volatile for all */
    union		/*0x8c*/
    {
        u32 gp_cr;	/* PART of the CR is non volatile for all */
        u32 cr;
    };
    union            /*0x90*/
    {
        u32 gp_srr0;
        u32 pc;
    };
    u32 gp_xer; /*0x94*/
    u32 gp_ctr; /*0x98*/
    u32 msr;	/*0x9c*//* Initial interrupt level */
    u32 gp_dsisr; /*0xa0*//*exception handling reg,the cause of DSI(0x300) and alignment exceptions(0x600)*/
    u32 gp_dear;   /*0xa4*//*after a dsi or an alignment exception ,dar is set to the effective address generated by the faulting instruction */
    u32 padding;	  /*0xa8*/ /* 为了8字节对齐，中断嵌套问题 */
} Context_Ctrl;

 typedef struct 
 {
	u32 gpr1;         /*0x0 up_level_func_sp*/
	u32 gp_lr;        /*0x4*/
    u32 gp_srr1;     /*0x8*/
    u32 gpr0;        /*0xc*/

    u32 gpr2;	     /*0x10*//* TOC in PowerOpen, reserved SVR4, section ptr EABI + */
    u32 gpr3;        /*0x14*/
    u32 gpr4;        /*0x18*/
    u32 gpr5;        /*0x1c*/
    u32 gpr6;		 /*0x20*/
    u32 gpr7;		 /*0x24*/
    u32 gpr8;   	 /*0x28*/
    u32 gpr9;		 /*0x2c*/
    u32 gpr10;	     /*0x30*/
    u32 gpr11;       /*0x34*/
    u32 gpr12;		 /*0x3c*/
    u32 gpr13;		 /*0x40*//* First non volatile PowerOpen, section ptr SVR4/EABI */
    u32 gpr14;	/*0x44*//* Non volatile for all */
    u32 gpr15;	/*0x48*//* Non volatile for all */
    u32 gpr16;	/*0x4c*//* Non volatile for all */
    u32 gpr17;	/*0x50*//* Non volatile for all */
    u32 gpr18;	/*0x54*//* Non volatile for all */
    u32 gpr19;	/*0x58*//* Non volatile for all */
    u32 gpr20;	/*0x5c*//* Non volatile for all */
    u32 gpr21;	/*0x60*//* Non volatile for all */
    u32 gpr22;	/*0x64*//* Non volatile for all */
    u32 gpr23;	/*0x68*//* Non volatile for all */
    u32 gpr24;	/*0x6c*//* Non volatile for all */
    u32 gpr25;	/*0x70*//* Non volatile for all */
    u32 gpr26;	/*0x74*//* Non volatile for all */
    u32 gpr27;	/*0x78*//* Non volatile for all */
    u32 gpr28;	/*0x7c*//* Non volatile for all */
    u32 gpr29;	/*0x80*//* Non volatile for all */
    u32 gpr30;	/*0x84*//* Non volatile for all */
    u32 gpr31;	/*0x88*//* Non volatile for all */
    union		/*0x8c*/
    {
        u32 gp_cr;	/* PART of the CR is non volatile for all */
        u32 cr;
    };
    union            /*0x90*/
    {
        u32 gp_srr0;
        u32 pc;
    };
    u32 gp_xer; /*0x94*/
    u32 gp_ctr; /*0x98*/
    u32 msr;	/*0x9c*//* Initial interrupt level */
    u32 gp_dsisr; /*0xa0*//*exception handling reg,the cause of DSI(0x300) and alignment exceptions(0x600)*/
    u32 gp_dear;   /*0xa4*//*after a dsi or an alignment exception ,dar is set to the effective address generated by the faulting instruction */
  /*  u32 pc1;*/	  /*0xa8*/
    u32 esp;
} REG_SET;

#define ACC_SIZE          2     /* UINT32 */   
#define SPE_NUM_REGS      32
#define RESV_REGS      8

/* This structure should be aligned to the size of a cache block */ 
typedef struct		/* SPE_REG_SET */
{
	unsigned long long gpr[SPE_NUM_REGS];	/* general pourpose registers */
	unsigned int acc[ACC_SIZE];
	unsigned int resv[RESV_REGS];
} SPE_CONTEXT;

typedef SPE_CONTEXT Spe_Context;
typedef SPE_CONTEXT FP_CONTEXT;
typedef SPE_CONTEXT Fp_Context;
#define SPEREG_SET SPE_CONTEX

#define HAVE_FP_CONTEXT



typedef void  (*FUNCPTRFORDEBUG) (REG_SET *, int) ;

void context_init(Context_Ctrl *the_context, u32 isr, void *entry_point, void *prev_entry);
void context_switch(Context_Ctrl **run,Context_Ctrl *heir);
void context_restore(Context_Ctrl *new_context);
void signal_context_restore(Context_Ctrl*ctx);


/**
 * 获取处理器时间戳接口
 */
extern u64 sys_timestamp (void);

/**
 * 获取处理器频率的接口
 */
extern u32 sys_timestamp_freq(void);



extern SYSTEM_BASIC_INFO * sys_info_get();
//移动到exc.h中
//typedef void(*EXC_HANDLER)(int, void *);
//EXC_HANDLER exception_handler_set(EXC_HANDLER the_exc_handler);
    
#endif

#ifdef __cplusplus
}
#endif

#endif
