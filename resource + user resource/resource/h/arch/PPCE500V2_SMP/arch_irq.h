/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了CSP模块中ppc603体系结构中断管理的接口
 * 修改：
 * 		 1. 2011-10-31，规范CSP 
 */
#ifndef _REWORKS_ARCH_IRQ_H_
#define _REWORKS_ARCH_IRQ_H_

#ifdef __cplusplus
extern "C" {
#endif
	
#include <reworks/types.h>
struct irq_data
{
	INT_HANDLER		handler;
	void			*param;
	char  *name;   
	int    pri;    
};	

/*******************************************************************************
 * 
 * BSP中断控制器需注册的操作接口
 * 
 * 		BSP中断控制器需注册的操作接口在BSP中实现，并通过注册接口注册到CSP以供中断框架
 * 调用。BSP中断控制器需注册的操作接口一般需要调用内部中断控制器（如果存在，在CSP文档
 * 中说明）和外部中断控制器（由BSP实现，如果存在）的相应操作。
 * 
 * 
 */
 
/**
 * 定义中断接口结构体
 */
struct int_operations
{
	int (*ack)		(int);	/* 中断响应 */
	int	(*enable)	(int);	/* 中断使能 */
	int	(*disable)(int);	/* 中断禁止 */
	int	(*inum)		(void);	/* 中断号获取 */
	int	(*init)		(void);	/* 中断控制器初始化 */
	int (*inum_nest_prohibited)(int); /* 指定中断是否允许被抢占 */
};
	
/* 硬件中断操作注册接口 */
int int_operations_register(struct int_operations *ops);

#ifdef __cplusplus
}
#endif
#endif
