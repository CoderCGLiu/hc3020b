
/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：定义异常管理框架中用到的宏和结构体。
 * 修改：       
 * 		2014-03-07, 宋伟, 创建. 
 */

#ifndef __EXCVEC_H__
#define __EXCVEC_H__

#ifdef __cplusplus
extern "C" {
#endif

#define MSW(x) 			(((x) >> 16) & 0xffff) 
#define LSW(x) 			((x) & 0xffff)
#define	SIGN_OF_LO(x)	(0x1 & (((CPU_INT32U)(x)) >> 15))

/*异常向量号*/
#define	EXC_OFF_CRTL		0x0100	/* Critical Input */
#define	EXC_OFF_MACH		0x0200	/* Machine Check */
#define	EXC_OFF_DATA		0x0300	/* Data Storage */
#define	EXC_OFF_INST		0x0400	/* Instruction Storage */
#define	EXC_OFF_INTR		0x0500	/* External Input */
#define	EXC_OFF_ALIGN		0x0600	/* Alignment */
#define	EXC_OFF_PROG		0x0700	/* Program */
#define	EXC_OFF_FPU			0x0800	/* Floating Point Unavailable */
#define	EXC_OFF_SYSCALL		0x0900	/* System Call */
#define	EXC_OFF_APU			0x0a00	/* Auxiliary Processor Unavailable */
#define	EXC_OFF_DECR		0x0b00	/* Decrementer */
#define	EXC_OFF_FIT			0x0c00	/* Fixed Interval Timer */
#define	EXC_OFF_WD			0x0d00	/* Watchdog Timer */
#define	EXC_OFF_DATA_MISS	0x0e00	/* Data TLB Error */
#define	EXC_OFF_INST_MISS	0x0f00	/* Instruction TLB Error */
#define	EXC_OFF_DBG			0x1000	/* Debug exception */

/*异常向量号*/
#define _EXC_CRITICAL_INPUT			0x100		//Critical Input	
#define _EXC_MACHINE_CHECK			0x200		//Machine Check Exception
#define _EXC_DSI					0x300		//Data Storage Interrupt
#define _EXC_ISI					0x400		//Instruction Storage Interrupt
#define _EXC_ALIGN_INT				0x600		//Alignment Interrupt
#define _EXC_PROG_INT				0x700		//Program Interrupt
#define _EXC_SYS_CALL_INT			0x900		//System Call Interrupt
#define _EXC_FIX_INT_TIMER			0xC00		//Fixed interval timer
#define _EXC_WATCHDOG				0xD00		//Watchdog
#define _EXC_DATA_TLB_ERROR			0xE00		//Data TLB error
#define _EXC_INST_TLB_ERROR			0xF00		//Instruction TLB Error Interrupt
#define _EXC_DEBUG_INT				0x1000		//Debug Interrupt
#define _EXC_SPE_APU_UNAVAILABLE	0x1100		//SPE/APU unavailable
#define _EXC_SPE_DATA				0x1200		//SPE data exception
#define _EXC_SPE_ROUND				0x1300		//SPE round exception
#define _EXC_PERF_MONITOR			0x1400		//Performance monitor

#ifdef __cplusplus
}
#endif
#endif
