
#ifndef _REWORKS_UDELAY_H_
#define _REWORKS_UDELAY_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <reworks/types.h>

extern u32 bsp_clicks_per_usec ;

static inline  void udelay(unsigned long usecs)
{
	unsigned long 	start,  now, ticks;	

	asm volatile( "mftb  %0" : "=r" (start) );

	asm volatile("mullwo %0,%1,%2" : "=r" (ticks) :
		"r" (usecs), "r" (bsp_clicks_per_usec));

	do {
		asm volatile( "mftb  %0" : "=r" (now) );
	}while (now - start < ticks);
}

#ifdef __cplusplus
}
#endif
#endif /* _PPC_DELAY_H__ */



