/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了CSP模块中PPC603体系结构IO操作
 * 修改：
 * 		 1. 2011-10-31，规范CSP 
 */

#ifndef _REWORKS_IO_ASM_H_
#define _REWORKS_IO_ASM_H_

#ifdef __cplusplus
extern "C" {
#endif

#define _IO_BASE (0)	/* From mpc8260 */

/*
 * We have to handle possible machine checks here on powermacs
 * and potentially some CHRPs -- paulus.
 */

#define __do_in_asm(name, op)				\
static __inline__ unsigned int name(unsigned int port)	\
{							\
	unsigned int x;					\
	__asm__ __volatile__(				\
		op " %0,0,%1\n"				\
		"1:	sync\n"				\
		"2:\n"					\
		".section .fixup,\"ax\"\n"		\
		"3:	li	%0,-1\n"		\
		"	b	2b\n"			\
		".previous\n"				\
		".section __ex_table,\"a\"\n"		\
		"	.align	2\n"			\
		"	.long	1b,3b\n"		\
		".previous"				\
		: "=&r" (x)				\
		: "r" (port + _IO_BASE));		\
	return x;					\
}

#define __do_out_asm(name, op)				\
static __inline__ void name(unsigned int val, unsigned int port) \
{							\
	__asm__ __volatile__(				\
		op " %0,0,%1\n"				\
		"1:	sync\n"				\
		"2:\n"					\
		".section __ex_table,\"a\"\n"		\
		"	.align	2\n"			\
		"	.long	1b,2b\n"		\
		".previous"				\
		: : "r" (val), "r" (port + _IO_BASE));	\
}

__do_in_asm(inb, "lbzx")
__do_in_asm(inw, "lhbrx")
__do_in_asm(inl, "lwbrx")
__do_out_asm(outb, "stbx")
__do_out_asm(outw, "sthbrx")
__do_out_asm(outl, "stwbrx")

#ifdef __cplusplus
}
#endif
#endif /* __PPC_IO_H__ */



