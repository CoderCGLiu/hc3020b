
#ifndef __REWORKS_PPC_ALIB_H__
#define __REWORKS_PPC_ALIB_H__

#include <reworks/types.h>

#ifdef __cplusplus
extern "C" {
#endif

extern u32	ppc_pvr_get();

extern u32 ppc_pir_get();

extern u32 ppc_msr_get();
extern void ppc_msr_set(u32);

extern u32 ppc_hid0_get();
extern void ppc_hid0_set(u32);

extern u32 ppc_hid1_get();
extern void ppc_hid1_set(u32);

extern u32 ppc_hid2_get();
extern void ppc_hid2_set(u32);

extern u32 ppc_dec_get();
extern void ppc_dec_set(u32);

extern u32 ppc_tbu_get();
extern u32 ppc_tbl_get();

extern u32 ppc_tcr_get();
extern void ppc_tcr_set();

extern void ppc_tsr_set(u32);
extern u32 ppc_tsr_get(void);

extern void ppc_l1csr0_set(u32);
extern u32 ppc_l1csr0_get(void);

extern void ppc_l1csr1_set(u32);
extern u32 ppc_l1csr1_get(void);

extern void ppc_l1cfg0_set(u32);
extern u32 ppc_l1cfg0_get(void);

extern void ppc_l1cfg1_set(u32);
extern u32 ppc_l1cfg1_get(void);

#ifdef __cplusplus
}
#endif

#endif
