/** 
 * @file shared_irq.h
 * 
 * @brief  本文件定义了共享中断相关宏与声明
 * @note   2016/2/24，黄河，规范格式化
 */

#ifndef _AARCH64_SHARED_IRQ_H_
#define _AARCH64_SHARED_IRQ_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef struct shared_node			
{
    struct shared_node 			*next;		/* 当前节点的后继结点指针 */
    struct shared_node 			*previous;	/* 当前节点的前驱结点指针 */
} SHARED_NODE;			/* PCI中断链表节点 */

typedef struct			
{
    SHARED_NODE 				*head;		/* 表头指针 */
    SHARED_NODE 				*tail;		/* 表尾指针 */
} SHARED_LIST;		/* 链表结构 */



/* 查找链表头结点 */
#define SHARED_FIRST(shared_list)		\
(					\
    (((SHARED_LIST *)(shared_list))->head)	\
)

/* 查找当前节点的后继结点 */
#define SHARED_NEXT(shared_node)			\
(					\
    (((SHARED_NODE *)(shared_node))->next)	\
)

/* 检测当前链表是否为空 */
#define SHARED_EMPTY(shared_list)			\
(						\
    (((SHARED_LIST *)shared_list)->head == NULL)		\
)

/**
 * @brief 初始化PCI中断链表
 * 
 * @param shared_list PCI中断链表指针
 * 
 * @return OS_OK PCI中断支持库初始化成功
 */
static int shared_intlist_init(SHARED_LIST *shared_list)
{
    shared_list->head	 = NULL;
    shared_list->tail  = NULL;

    return (OS_OK);
}

/**
 * @brief 添加PCI中断链表节点
 * 
 * 初始化PCI中断操作链表和中断服务程序
 * 
 * @param shared_list PCI中断链表指针
 * @param shared_node 欲添加的PCI链表节点
 * 
 * @return N/A
 */
static void shared_intlist_add(SHARED_LIST *shared_list,        
							SHARED_NODE *shared_node)        
{
    SHARED_NODE *shared_node_next;

    if (shared_list->tail == NULL)
	{				
    	/* 链表为空，则头结点为新结点 */
    	shared_node_next = shared_list->head;
    	shared_list->head = shared_node;
	}
    else
	{				
    	/* 链表不为空，则把新结点添加至表尾 */
    	shared_node_next = shared_list->tail->next;
    	shared_list->tail->next = shared_node;
	}

    if (shared_node_next == NULL)
	{
    	shared_list->tail = shared_node;/* 新节点无后继结点，则队尾指针指向新节点 */
	}
    else
	{
    	shared_node_next->previous = shared_node;	
	}


    /* 设置新节点的前驱、后继结点 */
    shared_node->next		= shared_node_next;
    shared_node->previous	= shared_list->tail;
}

/**
 * @brief 删除PCI中断链表节点
 * 
 * @param shared_list PCI中断链表指针
 * @param shared_node 欲删除的PCI链表节点
 * 
 * @return N/A
 */
static void shared_intlist_remove(SHARED_LIST *shared_list,             
							   SHARED_NODE *shared_node)             
{
    if (shared_node->previous == NULL)
	{
    	shared_list->head = shared_node->next;
	}
    else
	{
    	shared_node->previous->next = shared_node->next;
	}

    if (shared_node->next == NULL)
	{
    	shared_list->tail = shared_node->previous;
	}
    else
	{
    	shared_node->next->previous = shared_node->previous;
	}
}


#ifndef NELEMENTS
#	define NELEMENTS(array)		/* 求数组元素个数 */ \
		(sizeof (array) / sizeof ((array) [0]))
#endif

#ifdef __cplusplus
}
#endif

#endif
