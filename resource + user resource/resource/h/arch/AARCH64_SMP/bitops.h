#ifndef _REWORKS_BITOPS_H_
#define _REWORKS_BITOPS_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <cpu.h>
#include <reworks/types.h>
 
extern u8 ffs_lsb_tbl[];

#if 0
#define ffs16( _value, _output ) \
{ \
    register u32 __value = (u32) (_value); \
    register const unsigned char *__p = ffs_lsb_tbl; \
    \
    if ( __value & 0xff ) \
      (_output) = __p[ __value & 0xff]; \
    else \
      (_output) = __p[ __value >> 8 ] + 8; \
}
#else
#define ffs16( _value, _output ) \
{	\
	register unsigned long __value = (u64) (_value);	\
	register const unsigned char *__p = ffs_lsb_tbl;	\
	\
	if(__value & 0xFF)	\
		(_output) = __p[ __value & 0xFF]; \
	else if(__value & 0xFF00)	\
		(_output) = __p[ ((__value >> 8) &0xFF)] +8; \
	else if(__value & 0xFF0000)	\
		(_output) = __p[ ((__value >> 16) &0xFF)] +16; \
	else if(__value & 0xFF000000)	\
		(_output) = __p[ ((__value >> 24) &0xFF)] +24; \
	else if(__value & 0xFF00000000)	\
		(_output) = __p[ ((__value >> 32) &0xFF)] +32; \
	else if(__value & 0xFF0000000000)	\
		(_output) = __p[ ((__value >> 40) &0xFF)] +40; \
	else if(__value & 0xFF000000000000)	\
		(_output) = __p[ ((__value >> 48) &0xFF)] +48; \
	else if(__value & 0xFF00000000000000)	\
		(_output) = __p[ ((__value >> 56) &0xFF)] +56; \
	else	\
		(_output) = 0;	\
}
#endif
/*
 * ffs: find first bit set. This is defined the same way as
 * the libc and compiler builtin ffs routines, therefore
 * differs in spirit from the above ffz (man ffs).
 */

static inline int generic_ffs(int x)
{
	int r = 1;

	if (!x)
		return 0;
	if (!(x & 0xffff)) {
		x >>= 16;
		r += 16;
	}
	if (!(x & 0xff)) {
		x >>= 8;
		r += 8;
	}
	if (!(x & 0xf)) {
		x >>= 4;
		r += 4;
	}
	if (!(x & 3)) {
		x >>= 2;
		r += 2;
	}
	if (!(x & 1)) {
		x >>= 1;
		r += 1;
	}
	return r;
}

/*
 * These functions are the basis of our bit ops.
 * First, the atomic bitops.
 *
 * The endian issue for these functions is handled by the macros below.
 */
static inline void
____atomic_set_bit_mask(unsigned int mask, volatile unsigned char *p)
{
	unsigned int flags;

	local_irq_save(flags);
	*p |= mask;
	local_irq_restore(flags);
}

static inline void
____atomic_clear_bit_mask(unsigned int mask, volatile unsigned char *p)
{
	unsigned int flags;

	local_irq_save(flags);
	*p &= ~mask;
	local_irq_restore(flags);
}

static inline void
____atomic_change_bit_mask(unsigned int mask, volatile unsigned char *p)
{
	unsigned int flags;

	local_irq_save(flags);
	*p ^= mask;
	local_irq_restore(flags);
}

static inline int
____atomic_test_and_set_bit_mask(unsigned int mask, volatile unsigned char *p)
{
	unsigned int flags;
	unsigned int res;

	local_irq_save(flags);
	res = *p;
	*p = res | mask;
	local_irq_restore(flags);

	return res & mask;
}

static inline int
____atomic_test_and_clear_bit_mask(unsigned int mask, volatile unsigned char *p)
{
	unsigned int flags;
	unsigned int res;

	local_irq_save(flags);
	res = *p;
	*p = res & ~mask;
	local_irq_restore(flags);

	return res & mask;
}

static inline int
____atomic_test_and_change_bit_mask(unsigned int mask, volatile unsigned char *p)
{
	unsigned int flags;
	unsigned int res;

	local_irq_save(flags);
	res = *p;
	*p = res ^ mask;
	local_irq_restore(flags);

	return res & mask;
}

/*
 * Now the non-atomic variants.  We let the compiler handle all optimisations
 * for these.
 */
static inline void ____nonatomic_set_bit(int nr, volatile void *p)
{
	((unsigned char *) p)[nr >> 3] |= (1U << (nr & 7));
}

static inline void ____nonatomic_clear_bit(int nr, volatile void *p)
{
	((unsigned char *) p)[nr >> 3] &= ~(1U << (nr & 7));
}

static inline void ____nonatomic_change_bit(int nr, volatile void *p)
{
	((unsigned char *) p)[nr >> 3] ^= (1U << (nr & 7));
}

static inline int ____nonatomic_test_and_set_bit(int nr, volatile void *p)
{
	unsigned int mask = 1 << (nr & 7);
	unsigned int oldval;

	oldval = ((unsigned char *) p)[nr >> 3];
	((unsigned char *) p)[nr >> 3] = oldval | mask;
	return oldval & mask;
}

static inline int ____nonatomic_test_and_clear_bit(int nr, volatile void *p)
{
	unsigned int mask = 1 << (nr & 7);
	unsigned int oldval;

	oldval = ((unsigned char *) p)[nr >> 3];
	((unsigned char *) p)[nr >> 3] = oldval & ~mask;
	return oldval & mask;
}

static inline int ____nonatomic_test_and_change_bit(int nr, volatile void *p)
{
	unsigned int mask = 1 << (nr & 7);
	unsigned int oldval;

	oldval = ((unsigned char *) p)[nr >> 3];
	((unsigned char *) p)[nr >> 3] = oldval ^ mask;
	return oldval & mask;
}

/*
 * This routine doesn't need to be atomic.
 */
static inline int ____test_bit(int nr, const void * p)
{
    return ((volatile unsigned char *) p)[nr >> 3] & (1U << (nr & 7));
}	

/*
 *  A note about Endian-ness.
 *  -------------------------
 *
 * When the ARM is put into big endian mode via CR15, the processor
 * merely swaps the order of bytes within words, thus:
 *
 *          ------------ physical data bus bits -----------
 *          D31 ... D24  D23 ... D16  D15 ... D8  D7 ... D0
 * little     byte 3       byte 2       byte 1      byte 0
 * big        byte 0       byte 1       byte 2      byte 3
 *
 * This means that reading a 32-bit word at address 0 returns the same
 * value irrespective of the endian mode bit.
 *
 * Peripheral devices should be connected with the data bus reversed in
 * "Big Endian" mode.  ARM Application Note 61 is applicable, and is
 * available from http://www.arm.com/.
 *
 * The following assumes that the data bus connectivity for big endian
 * mode has been followed.
 *
 * Note that bit 0 is defined to be 32-bit word bit 0, not byte 0 bit 0.
 */

/*
 * Little endian assembly bitops.  nr = 0 -> byte 0 bit 0.
 */
extern void _set_bit_le(int nr, volatile void * p);
extern void _clear_bit_le(int nr, volatile void * p);
extern void _change_bit_le(int nr, volatile void * p);
extern int _test_and_set_bit_le(int nr, volatile void * p);
extern int _test_and_clear_bit_le(int nr, volatile void * p);
extern int _test_and_change_bit_le(int nr, volatile void * p);
extern int _find_first_zero_bit_le(void * p, unsigned size);
extern int _find_next_zero_bit_le(void * p, int size, int offset);

/*
 * Big endian assembly bitops.  nr = 0 -> byte 3 bit 0.
 */
extern void _set_bit_be(int nr, volatile void * p);
extern void _clear_bit_be(int nr, volatile void * p);
extern void _change_bit_be(int nr, volatile void * p);
extern int _test_and_set_bit_be(int nr, volatile void * p);
extern int _test_and_clear_bit_be(int nr, volatile void * p);
extern int _test_and_change_bit_be(int nr, volatile void * p);
extern int _find_first_zero_bit_be(void * p, unsigned size);
extern int _find_next_zero_bit_be(void * p, int size, int offset);


/*
 * The __* form of bitops are non-atomic and may be reordered.
 */
/*#define	ATOMIC_BITOP_LE(name,nr,p)		\
	(__builtin_constant_p(nr) ?		\
	 ____atomic_##name##_mask(1 << ((nr) & 7), \
			((unsigned char *)(p)) + ((nr) >> 3)) : \
	 _##name##_le(nr,p))*/
	 

#define	ATOMIC_BITOP_LE(name,nr,p)		\
	(____atomic_##name##_mask(1 << ((nr) & 7), \
			((unsigned char *)(p)) + ((nr) >> 3)))

#define	ATOMIC_BITOP_BE(name,nr,p)		\
	(__builtin_constant_p(nr) ?		\
	 ____atomic_##name##_mask(1 << ((nr) & 7), \
			((unsigned char *)(p)) + (((nr) >> 3) ^ 3)) : \
	 _##name##_be(nr,p))

#define NONATOMIC_BITOP_LE(name,nr,p)	\
	(____nonatomic_##name(nr, p))

#define NONATOMIC_BITOP_BE(name,nr,p)	\
	(____nonatomic_##name(nr ^ 0x18, p))

#ifndef __ARMEB__
/*
 * These are the little endian, atomic definitions.
 */
#define set_bit(nr,p)			ATOMIC_BITOP_LE(set_bit,nr,p)
#define clear_bit(nr,p)			ATOMIC_BITOP_LE(clear_bit,nr,p)
#define change_bit(nr,p)		ATOMIC_BITOP_LE(change_bit,nr,p)
#define test_and_set_bit(nr,p)		ATOMIC_BITOP_LE(test_and_set_bit,nr,p)
#define test_and_clear_bit(nr,p)	ATOMIC_BITOP_LE(test_and_clear_bit,nr,p)
#define test_and_change_bit(nr,p)	ATOMIC_BITOP_LE(test_and_change_bit,nr,p)
#define test_bit(nr,p)			____test_bit(nr,p)
#define find_first_zero_bit(p,sz)	_find_first_zero_bit_le(p,sz)
#define find_next_zero_bit(p,sz,off)	_find_next_zero_bit_le(p,sz,off)

/*
 * These are the little endian, non-atomic definitions.
 */
#define __set_bit(nr,p)			NONATOMIC_BITOP_LE(set_bit,nr,p)
#define __clear_bit(nr,p)		NONATOMIC_BITOP_LE(clear_bit,nr,p)
#define __change_bit(nr,p)		NONATOMIC_BITOP_LE(change_bit,nr,p)
#define __test_and_set_bit(nr,p)	NONATOMIC_BITOP_LE(test_and_set_bit,nr,p)
#define __test_and_clear_bit(nr,p)	NONATOMIC_BITOP_LE(test_and_clear_bit,nr,p)
#define __test_and_change_bit(nr,p)	NONATOMIC_BITOP_LE(test_and_change_bit,nr,p)
#define __test_bit(nr,p)		____test_bit(nr,p)

#else

/*
 * These are the big endian, atomic definitions.
 */
#define set_bit(nr,p)			ATOMIC_BITOP_BE(set_bit,nr,p)
#define clear_bit(nr,p)			ATOMIC_BITOP_BE(clear_bit,nr,p)
#define change_bit(nr,p)		ATOMIC_BITOP_BE(change_bit,nr,p)
#define test_and_set_bit(nr,p)		ATOMIC_BITOP_BE(test_and_set_bit,nr,p)
#define test_and_clear_bit(nr,p)	ATOMIC_BITOP_BE(test_and_clear_bit,nr,p)
#define test_and_change_bit(nr,p)	ATOMIC_BITOP_BE(test_and_change_bit,nr,p)
#define test_bit(nr,p)			____test_bit((nr) ^ 0x18, p)
#define find_first_zero_bit(p,sz)	_find_first_zero_bit_be(p,sz)
#define find_next_zero_bit(p,sz,off)	_find_next_zero_bit_be(p,sz,off)

/*
 * These are the big endian, non-atomic definitions.
 */
#define __set_bit(nr,p)			NONATOMIC_BITOP_BE(set_bit,nr,p)
#define __clear_bit(nr,p)		NONATOMIC_BITOP_BE(clear_bit,nr,p)
#define __change_bit(nr,p)		NONATOMIC_BITOP_BE(change_bit,nr,p)
#define __test_and_set_bit(nr,p)	NONATOMIC_BITOP_BE(test_and_set_bit,nr,p)
#define __test_and_clear_bit(nr,p)	NONATOMIC_BITOP_BE(test_and_clear_bit,nr,p)
#define __test_and_change_bit(nr,p)	NONATOMIC_BITOP_BE(test_and_change_bit,nr,p)
#define __test_bit(nr,p)		____test_bit((nr) ^ 0x18, p)

#endif

/*
 * ffz = Find First Zero in word. Undefined if no zero exists,
 * so code should check against ~0UL first..
 */
static inline unsigned long ffz(unsigned long word)
{
	int k;

	word = ~word;
	k = 31;
	if (word & 0x0000ffff) { k -= 16; word <<= 16; }
	if (word & 0x00ff0000) { k -= 8;  word <<= 8;  }
	if (word & 0x0f000000) { k -= 4;  word <<= 4;  }
	if (word & 0x30000000) { k -= 2;  word <<= 2;  }
	if (word & 0x40000000) { k -= 1; }
        return k;
}

#ifdef __cplusplus
}
#endif

#endif /* _REWORKS_BITOPS_H_ */
