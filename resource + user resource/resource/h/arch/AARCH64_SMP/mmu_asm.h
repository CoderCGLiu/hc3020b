/**
 * @file    mmu_asm.h
 * @brief   ARM架构定义头文件
 * @note    2016-2-24，庄金峰，用Doxygen格式将源代码文档化
 */
#ifndef _AARCH64_MMU_ASM_H_
#define _AARCH64_MMU_ASM_H_

#ifdef __cplusplus
extern "C" {
#endif

#ifndef ASM
#define ASM
#endif

/*
 *  Recent versions of GNU cpp define variables which indicate the
 *  need for underscores and percents.  If not using GNU cpp or
 *  the version does not support this, then you will obviously
 *  have to define these as appropriate.
 */

#ifndef __USER_LABEL_PREFIX__
#define __USER_LABEL_PREFIX__
#endif

#ifndef __REGISTER_PREFIX__
#define __REGISTER_PREFIX__
#endif

/* ANSI concatenation macros.  */

#define CONCAT1(a, b) CONCAT2(a, b)
#define CONCAT2(a, b) a ## b

/* Use the right prefix for global labels.  */

#define SYM(x) CONCAT1 (__USER_LABEL_PREFIX__, x)

/* Use the right prefix for registers.  */

#define REG(x) CONCAT1 (__REGISTER_PREFIX__, x)

/*
 *  define macros for all of the registers on this CPU
 *
 *  EXAMPLE:     #define d0 REG (d0)
 */

#define r0  REG(r0)
#define r1  REG(r1)
#define r2  REG(r2)
#define r3  REG(r3)
#define r4  REG(r4)
#define r5  REG(r5)
#define r6  REG(r6)
#define r7  REG(r7)
#define r8  REG(r8)
#define r9  REG(r9)
#define r10 REG(r10)
#define r11 REG(r11)
#define r12 REG(r12)
#define r13 REG(r13)
#define r14 REG(r14)
#define r15 REG(r15)

#define CPSR REG(CPSR)

#define SPSR REG(SPSR)

/*
 *  Define macros to handle section beginning and ends.
 */


#define BEGIN_CODE_DCL .text
#define END_CODE_DCL
#define BEGIN_DATA_DCL .data
#define END_DATA_DCL
#define BEGIN_CODE .text
#define END_CODE
#define BEGIN_DATA
#define END_DATA
#define BEGIN_BSS
#define END_BSS
#define END

/*
 *  Following must be tailor for a particular flavor of the C compiler.
 *  They may need to put underscores in front of the symbols.
 */

#define PUBLIC(sym) .globl SYM (sym)
#define EXTERN(sym) .globl SYM (sym)

#ifdef __cplusplus
}
#endif

#endif
/* end of include file */


