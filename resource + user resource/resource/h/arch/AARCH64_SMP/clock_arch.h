
#ifndef _REWORKS_CLOCK_ARCH_H_
#define _REWORKS_CLOCK_ARCH_H_

#ifdef __cplusplus
extern "C" {
#endif

/* 时钟启动 */
typedef void (*CLOCK_START_FUNCPTR)(void);


/* 时钟关闭 */
typedef void (*CLOCK_OFF_FUNCPTR)(void);

/* 设置时钟速率 */
typedef int (*CLOCK_CONFIG_FUNCPTR)(int ticks_per_second);



/* 硬件操作数据结构定义 */
struct clock_operations 
{
	CLOCK_START_FUNCPTR		start;
	CLOCK_OFF_FUNCPTR			off;
	CLOCK_CONFIG_FUNCPTR	config;

};

/* 硬件操作注册 */
int clock_operations_register(struct clock_operations *ops);	
		

#ifdef __cplusplus
}
#endif

#endif
