/**
 * @file    arm.h
 * @brief   ARM架构定义头文件
 * @note    2016-2-24，庄金峰，用Doxygen格式将源代码文档化
 */
#ifndef _REWORKS_ARM_H_
#define _REWORKS_ARM_H_

#ifdef __cplusplus
extern "C" {
#endif

/* bits in the PSR */
#define	V_BIT			(1<<28)
#define	C_BIT			(1<<29)
#define	Z_BIT			(1<<30)
#define	N_BIT			(1<<31)
#define I_BIT   		(1<<7)
#define F_BIT   		(1<<6)
#define	T_BIT			(1<<5)


/* mode bits */
#define MODE_SYSTEM32	0x1F
#define MODE_UNDEF32	0x1B
#define	MODE_ABORT32	0x17
#define MODE_SVC32		0x13
#define MODE_IRQ32      0x12
#define MODE_FIQ32      0x11
#define MODE_USER32		0x10

/* masks for getting bits from PSR */

#define MASK_MODE		0x0000003F
#define	MASK_32MODE		0x0000001F
#define	MASK_SUBMODE	0x0000000F
#define MASK_INT		0x000000C0
#define	MASK_CC			0xF0000000

/* shifts to access bits in the PSR */
#define INT_MASK_SHIFT	6

/* The coprocessor number of the MMU System Control Processor */
#define CP_MMU 			15

/*
 * Arm Arch HW specific bits used by cache routines...
 */
#define CACHE_MM_ENABLE   (1<<0)    /* MMU enable */
#define CACHE_DC_ENABLE   (1<<2)    /* (data) cache enable */
#define CACHE_WB_ENABLE   (1<<3)    /* write buffer enable */
#define CACHE_IC_ENABLE   (1<<12)   /* Instruction cache enable */

#ifdef __cplusplus
}
#endif

#endif
