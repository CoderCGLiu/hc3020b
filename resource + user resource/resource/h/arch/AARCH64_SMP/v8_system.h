//
// Defines for v8 System Registers
//
// Copyright(c) 2012-2016 ARM Limited.  All Rights Reserved
//

#ifndef _AARCH64_V8_SYSTEM_H_
#define _AARCH64_V8_SYSTEM_H_

//
// AArch64 SPSR
//
#define AARCH64_SPSR_EL3h 0b1101
#define AARCH64_SPSR_EL3t 0b1100
#define AARCH64_SPSR_EL2h 0b1001
#define AARCH64_SPSR_EL2t 0b1000
#define AARCH64_SPSR_EL1h 0b0101
#define AARCH64_SPSR_EL1t 0b0100
#define AARCH64_SPSR_EL0t 0b0000
#define AARCH64_SPSR_RW (1 << 4)
#define AARCH64_SPSR_F  (1 << 6)
#define AARCH64_SPSR_I  (1 << 7)
#define AARCH64_SPSR_A  (1 << 8)
#define AARCH64_SPSR_D  (1 << 9)
#define AARCH64_SPSR_IL (1 << 20)
#define AARCH64_SPSR_SS (1 << 21)
#define AARCH64_SPSR_V  (1 << 28)
#define AARCH64_SPSR_C  (1 << 29)
#define AARCH64_SPSR_Z  (1 << 30)
#define AARCH64_SPSR_N  (1 << 31)

//
// Multiprocessor Affinity Register
//
#define MPIDR_EL1_AFF3_LSB 32
#define MPIDR_EL1_U  (1 << 30)
#define MPIDR_EL1_MT (1 << 24)
#define MPIDR_EL1_AFF2_LSB 16
#define MPIDR_EL1_AFF1_LSB  8
#define MPIDR_EL1_AFF0_LSB  0
#define MPIDR_EL1_AFF_WIDTH 8

//
// Data Cache Zero ID Register
//
#define DCZID_EL0_BS_LSB   0
#define DCZID_EL0_BS_WIDTH 4
#define DCZID_EL0_DZP_LSB  5
#define DCZID_EL0_DZP (1 << 5)

//
// System Control Register
//
#define SCTLR_EL1_UCI     (1 << 26)
#define SCTLR_ELx_EE      (1 << 25)
#define SCTLR_EL1_E0E     (1 << 24)
#define SCTLR_ELx_WXN     (1 << 19)
#define SCTLR_EL1_nTWE    (1 << 18)
#define SCTLR_EL1_nTWI    (1 << 16)
#define SCTLR_EL1_UCT     (1 << 15)
#define SCTLR_EL1_DZE     (1 << 14)
#define SCTLR_ELx_I       (1 << 12)
#define SCTLR_EL1_UMA     (1 << 9)
#define SCTLR_EL1_SED     (1 << 8)
#define SCTLR_EL1_ITD     (1 << 7)
#define SCTLR_EL1_THEE    (1 << 6)
#define SCTLR_EL1_CP15BEN (1 << 5)
#define SCTLR_EL1_SA0     (1 << 4)
#define SCTLR_ELx_SA      (1 << 3)
#define SCTLR_ELx_C       (1 << 2)
#define SCTLR_ELx_A       (1 << 1)
#define SCTLR_ELx_M       (1 << 0)

//
// Architectural Feature Access Control Register
//
#define CPACR_EL1_TTA     (1 << 28)
#define CPACR_EL1_FPEN    (3 << 20)

//
// Architectural Feature Trap Register
//
#define CPTR_ELx_TCPAC (1 << 31)
#define CPTR_ELx_TTA   (1 << 20)
#define CPTR_ELx_TFP   (1 << 10)

//
// Secure Configuration Register
//
#define SCR_EL3_TWE  (1 << 13)
#define SCR_EL3_TWI  (1 << 12)
#define SCR_EL3_ST   (1 << 11)
#define SCR_EL3_RW   (1 << 10)
#define SCR_EL3_SIF  (1 << 9)
#define SCR_EL3_HCE  (1 << 8)
#define SCR_EL3_SMD  (1 << 7)
#define SCR_EL3_EA   (1 << 3)
#define SCR_EL3_FIQ  (1 << 2)
#define SCR_EL3_IRQ  (1 << 1)
#define SCR_EL3_NS   (1 << 0)

//
// Hypervisor Configuration Register
//
#define HCR_EL2_ID   (1 << 33)
#define HCR_EL2_CD   (1 << 32)
#define HCR_EL2_RW   (1 << 31)
#define HCR_EL2_TRVM (1 << 30)
#define HCR_EL2_HVC  (1 << 29)
#define HCR_EL2_TDZ  (1 << 28)

/* CTR_EL0 - Cache Type Register */
#define	CTR_RES1		(1 << 31)
#define	CTR_TminLine_SHIFT	32
#define	CTR_TminLine_MASK	(UL(0x3f) << CTR_TminLine_SHIFT)
#define	CTR_TminLine_VAL(reg)	((reg) & CTR_TminLine_MASK)
#define	CTR_DIC_SHIFT		29
#define	CTR_DIC_MASK		(0x1 << CTR_DIC_SHIFT)
#define	CTR_DIC_VAL(reg)	((reg) & CTR_DIC_MASK)
#define	CTR_IDC_SHIFT		28
#define	CTR_IDC_MASK		(0x1 << CTR_IDC_SHIFT)
#define	CTR_IDC_VAL(reg)	((reg) & CTR_IDC_MASK)
#define	CTR_CWG_SHIFT		24
#define	CTR_CWG_MASK		(0xf << CTR_CWG_SHIFT)
#define	CTR_CWG_VAL(reg)	((reg) & CTR_CWG_MASK)
#define	CTR_CWG_SIZE(reg)	(4 << (CTR_CWG_VAL(reg) >> CTR_CWG_SHIFT))
#define	CTR_ERG_SHIFT		20
#define	CTR_ERG_MASK		(0xf << CTR_ERG_SHIFT)
#define	CTR_ERG_VAL(reg)	((reg) & CTR_ERG_MASK)
#define	CTR_ERG_SIZE(reg)	(4 << (CTR_ERG_VAL(reg) >> CTR_ERG_SHIFT))
#define	CTR_DLINE_SHIFT		16
#define	CTR_DLINE_MASK		(0xf << CTR_DLINE_SHIFT)
#define	CTR_DLINE_VAL(reg)	((reg) & CTR_DLINE_MASK)
#define	CTR_DLINE_SIZE(reg)	(4 << (CTR_DLINE_VAL(reg) >> CTR_DLINE_SHIFT))
#define	CTR_L1IP_SHIFT		14
#define	CTR_L1IP_MASK		(0x3 << CTR_L1IP_SHIFT)
#define	CTR_L1IP_VAL(reg)	((reg) & CTR_L1IP_MASK)
#define	 CTR_L1IP_VPIPT		(0 << CTR_L1IP_SHIFT)
#define	 CTR_L1IP_AIVIVT	(1 << CTR_L1IP_SHIFT)
#define	 CTR_L1IP_VIPT		(2 << CTR_L1IP_SHIFT)
#define	 CTR_L1IP_PIPT		(3 << CTR_L1IP_SHIFT)
#define	CTR_ILINE_SHIFT		0
#define	CTR_ILINE_MASK		(0xf << CTR_ILINE_SHIFT)
#define	CTR_ILINE_VAL(reg)	((reg) & CTR_ILINE_MASK)
#define	CTR_ILINE_SIZE(reg)	(4 << (CTR_ILINE_VAL(reg) >> CTR_ILINE_SHIFT))

#define __string(x) #x
#define read_cpureg(reg) ({					\
	u64 __val;						\
	asm volatile("mrs %0, " __string(reg) : "=r" (__val));	\
	__val;							\
})

#define	write_cpureg(reg, _val)					\
	__asm __volatile("msr	" __string(reg) ", %0" : : "r"((u64)_val))
#endif // V8_SYSTEM_H
