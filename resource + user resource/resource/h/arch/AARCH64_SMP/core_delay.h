#ifndef _REWORKS_CORE_DELAY_H_
#define _REWORKS_CORE_DELAY_H_

#ifdef __cplusplus
extern "C" {
#endif

#define LPS_PREC 8  										// 校准精度，精度越高校准时间越长
#define OPTIMIZE(level) __attribute__((optimize(level)))    // 为防止udelay函数被优化，故定义还宏用以修改udelay相关函数
#define LPS_STEP (1<<1)		
extern u32 bsp_clicks_per_usec;
extern u32 g_udelay_loop_count; 

OPTIMIZE("O0") static inline void __delay(volatile unsigned int loops)
{
	__asm__ __volatile__ (
	"	.align	4					\n"
	"l1:	subs    %0, %0, #1		\n"
	"	bhi l1						\n"
	: "=r" (loops)
	: "0" (loops));
}

u64 __attribute__((always_inline)) static  __sys_timestamp (void)
{
//	u64 stamp;
//	asm volatile (
//			"dmfc0 %0,$25,1 \n"
//			:"=r"(stamp)
//			 );
//	return stamp;
}


static __attribute__((always_inline)) void __delay_2(volatile unsigned int us)
{
#if 0
	volatile unsigned int loops;
	
	loops = us * g_udelay_loop_count;
	
	__asm__ __volatile__ (
		"	.align	4					\n"
		"l2:	subs    r0, r0, #1		\n"
		"	bhi l2						\n"
		: "=r" (loops)
		: "0" (loops));
#else
	__delay(us * g_udelay_loop_count);//g_udelay_loop_count 为DM3730测试出来的参考值
#endif
}

#ifdef __cplusplus
}
#endif
#endif /* _REWORKS_CORE_DELAY_H_ */
