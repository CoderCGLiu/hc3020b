#ifndef _REWORKS_ARCH_IRQ_H_
#define _REWORKS_ARCH_IRQ_H_

#ifdef __cplusplus
extern "C" {
#endif


/*******************************************************************************
 * 
 * BSP中断控制器需注册的操作接口
 * 
 * 		BSP中断控制器需注册的操作接口在BSP中实现，并通过注册接口注册到CSP以供中断框架
 * 调用。BSP中断控制器需注册的操作接口一般需要调用内部中断控制器（如果存在，在CSP文档
 * 中说明）和外部中断控制器（由BSP实现，如果存在）的相应操作。
 * 
 * 
 */
#define  IRQ_NUM  (1020)

#ifdef __cplusplus
}
#endif
#endif /* _REWORKS_ARCH_IRQ_H_ */


