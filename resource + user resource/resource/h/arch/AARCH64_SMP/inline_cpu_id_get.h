#ifndef _REWORKS_INLINE_CPU_ID_GET_H_
#define _REWORKS_INLINE_CPU_ID_GET_H_

#ifdef __cplusplus
extern "C" {
#endif

extern int cpu_id_map[16][4];
__attribute__((always_inline)) static inline int inline_cpu_id_get()
{
    int cpuid ;
    register u64 number;
    register int cpu_cluster = 0;
    asm volatile
	(
	"MRS %0, MPIDR_EL1\n"	
	: "=&r" (number)		/* output: key is %0 */
	:					/* no input */
	: "memory"			/* memory */
						/* clobber for code barrier */
	);
    
   
    number = (number & 0xFFFF);
    cpu_cluster = number >> 8;
    cpuid = cpu_id_map[cpu_cluster][number & 0x3];
    return cpuid;
}

#ifdef __cplusplus
}
#endif
#endif /* _REWORKS_INLINE_CPU_ID_GET_H_ */
