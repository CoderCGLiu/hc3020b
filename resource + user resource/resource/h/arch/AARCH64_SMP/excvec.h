
/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：定义异常管理框架中用到的宏和结构体。
 */

#ifndef _REWORKS_AARCH64_EXCVEC_H_
#define _REWORKS_AARCH64_EXCVEC_H_

#ifdef __cplusplus
extern "C" {
#endif

/* 异常编码 */
#define AARCH64_EXC_UNDFI	1		/* undefined interrupt exception */
#define AARCH64_EXC_INTR	2		/* software interrupt exception */
#define AARCH64_EXC_DABORT	3		/* data abort exception */
#define AARCH64_EXC_PREF	4		/* pre-fetch abort exception */


#ifdef __cplusplus
}
#endif
#endif
