/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了CSP模块中loongson2f平台的IO操作
 * 修改：
 * 		 1. 2012-5-17，规范CSP 
 */

#ifndef _REWORKS_IO_ASM_H_
#define _REWORKS_IO_ASM_H_

#ifdef __cplusplus
extern "C" 
{
#endif

/* only for longson2f */
#define mips_io_port_base 0xbfd00000


#ifndef ASM_LANGUAGE

#include <reworks/types.h>


#define le16_to_cpu(x)	(x)
#define le32_to_cpu(x)	(x)

#define cpu_to_le16(x)	(x)
#define cpu_to_le32(x)	(x)

#define __swizzle_addr_b(port)	(port)
#define __swizzle_addr_w(port)	(port)
#define __swizzle_addr_l(port)	(port)


/*
 * Sane hardware offers swapping of PCI/ISA I/O space accesses in hardware;
 * less sane hardware forces software to fiddle with this...
 *
 * Regardless, if the host bus endianness mismatches that of PCI/ISA, then
 * you can't have the numerical value of data and byte addresses within
 * multibyte quantities both preserved at the same time.  Hence two
 * variations of functions: non-prefixed ones that preserve the value
 * and prefixed ones that preserve byte addresses.  The latters are
 * typically used for moving raw data between a peripheral and memory (cf.
 * string I/O functions), hence the "__mem_" prefix.
 */
# define ioswabb(a,x)		(x)
# define __mem_ioswabb(a,x)	(x)
# define ioswabw(a,x)		(x)
# define __mem_ioswabw(a,x)	cpu_to_le16(x)
# define ioswabl(a,x)		(x)
# define __mem_ioswabl(a,x)	cpu_to_le32(x)

/*
 * Raw operations are never swapped in software.  OTOH values that raw
 * operations are working on may or may not have been swapped by the bus
 * hardware.  An example use would be for flash memory that's used for
 * execute in place.
 */
#define __raw_ioswabb(a,x)	(x)
#define __raw_ioswabw(a,x)	(x)
#define __raw_ioswabl(a,x)	(x)


/*
 * 读写慢速设备时，需要等待
 */
#define __SLOW_DOWN_IO \
	__asm__ __volatile__( \
		"sb\t$0,0x80(%0)" \
		: : "r" (mips_io_port_base));

#ifdef CONF_SLOWDOWN_IO
#ifdef REALLY_SLOW_IO
#define SLOW_DOWN_IO { __SLOW_DOWN_IO; __SLOW_DOWN_IO; __SLOW_DOWN_IO; __SLOW_DOWN_IO; }
#else
#define SLOW_DOWN_IO __SLOW_DOWN_IO
#endif
#else
#define SLOW_DOWN_IO
#endif


/*
 * 建立内存读写接口
 */
#define __BUILD_MEMORY_SINGLE(pfx, bwlq, type)	\
														\
static inline void pfx##write##bwlq(type val,	 volatile void *mem)	\
{												\
	volatile type *__mem;						\
	type __val;									\
									\
	__mem = (void *)__swizzle_addr_##bwlq((unsigned long)(mem));	\
												\
	__val = pfx##ioswab##bwlq(__mem, val);		\
												\
	*__mem = __val;								\
}												\
												\
static inline type pfx##read##bwlq(const volatile void *mem)	\
{												\
	volatile type *__mem;						\
	type __val;									\
												\
	__mem = (void *)__swizzle_addr_##bwlq((unsigned long)(mem));	\
												\
	__val = *__mem;								\
												\
	return pfx##ioswab##bwlq(__mem, __val);	\
}


/*
 * 建立端口读写接口
 */
#define __BUILD_IOPORT_SINGLE(pfx, bwlq, type, p, slow)	\
												\
static inline void pfx##out##bwlq##p(type val, unsigned long port)	\
{												\
	volatile type *__addr;						\
	type __val;									\
												\
	__addr = (void *)__swizzle_addr_##bwlq(mips_io_port_base + port);	\
												\
	__val = pfx##ioswab##bwlq(__addr, val);		\
												\
	*__addr = __val;							\
	slow;										\
}												\
												\
static inline type pfx##in##bwlq##p(unsigned long port)	\
{												\
	volatile type *__addr;						\
	type __val;									\
												\
	__addr = (void *)__swizzle_addr_##bwlq(mips_io_port_base + port);	\
												\
	__val = *__addr;							\
	slow;										\
												\
	return pfx##ioswab##bwlq(__addr, __val);	\
}



#define __BUILD_MEMORY_PFX(bus, bwlq, type)	\
__BUILD_MEMORY_SINGLE(bus, bwlq, type)


#define BUILDIO_MEM(bwlq, type)	\
__BUILD_MEMORY_PFX(__raw_, bwlq, type)		\
__BUILD_MEMORY_PFX(, bwlq, type)			\
__BUILD_MEMORY_PFX(__mem_, bwlq, type)

/*
 * 建立内存读写接口：
 * 
 * __raw_readb(const volatile void *mem);
 * __raw_readw(const volatile void *mem);
 * __raw_readl(const volatile void *mem);
 * __raw_writeb(u8 val,	 volatile void *mem);
 * __raw_writew(u16 val,	 volatile void *mem);
 * __raw_writel(u32 val,	 volatile void *mem);
 * 
 * readb(const volatile void *mem);
 * readw(const volatile void *mem);
 * readl(const volatile void *mem);
 * writeb(u8 val,	 volatile void *mem);
 * writew(u16 val,	 volatile void *mem);
 * writel(u32 val,	 volatile void *mem);
 * 
 * __mem_readb(const volatile void *mem);
 * __mem_readw(const volatile void *mem);
 * __mem_readl(const volatile void *mem);
 * __mem_writeb(u8 val,	 volatile void *mem);
 * __mem_writew(u16 val,	 volatile void *mem);
 * __mem_writel(u32 val,	 volatile void *mem);
 * 
 */
BUILDIO_MEM(b, u8)
BUILDIO_MEM(w, u16)
BUILDIO_MEM(l, u32)


#define __BUILD_IOPORT_PFX(bus, bwlq, type)			\
	__BUILD_IOPORT_SINGLE(bus, bwlq, type, ,)			\
	__BUILD_IOPORT_SINGLE(bus, bwlq, type, _p, SLOW_DOWN_IO)

#define BUILDIO_IOPORT(bwlq, type)				\
	__BUILD_IOPORT_PFX(, bwlq, type)			\
	__BUILD_IOPORT_PFX(__mem_, bwlq, type)

/*
 * 建立端口读写接口：
 * 
 * outb(u8 val, unsigned long port);
 * outw(u16 val, unsigned long port);
 * outl(u32 val, unsigned long port);
 * inb(unsigned long port);
 * inw(unsigned long port);
 * inl(unsigned long port);
 * 
 * outb_p(u8 val, unsigned long port);
 * outw_p(u16 val, unsigned long port);
 * outl_p(u32 val, unsigned long port);
 * inb_p(unsigned long port);
 * inw_p(unsigned long port);
 * inl_p(unsigned long port);
 * 
 * __mem_outb(u8 val, unsigned long port);
 * __mem_outw(u16 val, unsigned long port);
 * __mem_outl(u32 val, unsigned long port);
 * __mem_inb(unsigned long port);
 * __mem_inw(unsigned long port);
 * __mem_inl(unsigned long port);
 * 
 * __mem_outb_p(u8 val, unsigned long port);
 * __mem_outw_p(u16 val, unsigned long port);
 * __mem_outl_p(u32 val, unsigned long port);
 * __mem_inb_p(unsigned long port);
 * __mem_inw_p(unsigned long port);
 * __mem_inl_p(unsigned long port);
 * 
 */
BUILDIO_IOPORT(b, u8)
BUILDIO_IOPORT(w, u16)
BUILDIO_IOPORT(l, u32)



#define __BUILD_MEMORY_STRING(bwlq, type)	\
													\
static inline void writes##bwlq(volatile void *mem, const void *addr, unsigned int count)	\
{													\
	const volatile type *__addr = addr;			\
													\
	while (count--) {								\
		__mem_write##bwlq(*__addr, mem);			\
		__addr++;									\
	}												\
}													\
													\
static inline void reads##bwlq(volatile void *mem, void *addr,	unsigned int count)	\
{													\
	volatile type *__addr = addr;					\
													\
	while (count--) {								\
		*__addr = __mem_read##bwlq(mem);			\
		__addr++;									\
	}												\
}

#define __BUILD_IOPORT_STRING(bwlq, type)	\
													\
static inline void outs##bwlq(unsigned long port, const void *addr, unsigned int count)	\
{													\
	const volatile type *__addr = addr;			\
													\
	while (count--) {								\
		__mem_out##bwlq(*__addr, port);				\
		__addr++;									\
	}												\
}													\
													\
static inline void ins##bwlq(unsigned long port, void *addr, unsigned int count)	\
{													\
	volatile type *__addr = addr;					\
													\
	while (count--) {								\
		*__addr = __mem_in##bwlq(port);				\
		__addr++;									\
	}												\
}

#define BUILDSTRING(bwlq, type)	\
__BUILD_MEMORY_STRING(bwlq, type)	\
__BUILD_IOPORT_STRING(bwlq, type)


/*
 * 建立内存和端口的连续读写接口：
 * 
 * readsb(volatile void *mem, void *addr,	unsigned int count);
 * readsw(volatile void *mem, void *addr,	unsigned int count);
 * readsl(volatile void *mem, void *addr,	unsigned int count);
 * writesb(volatile void *mem, const void *addr, unsigned int count);
 * writesw(volatile void *mem, const void *addr, unsigned int count);
 * writesl(volatile void *mem, const void *addr, unsigned int count);
 * 
 * outsb(unsigned long port, const void *addr, unsigned int count);
 * outsw(unsigned long port, const void *addr, unsigned int count);
 * outsl(unsigned long port, const void *addr, unsigned int count);
 * insb(unsigned long port, void *addr, unsigned int count);
 * insw(unsigned long port, void *addr, unsigned int count);
 * insl(unsigned long port, void *addr, unsigned int count);
 */
BUILDSTRING(b, u8)
BUILDSTRING(w, u16)
BUILDSTRING(l, u32)

#endif /* ASM_LANGUAGE */
#ifdef __cplusplus
}
#endif
#endif /* _REWORKS_IO_ASM_H_ */
