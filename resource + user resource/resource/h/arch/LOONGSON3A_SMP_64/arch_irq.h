/*******************************************************************************
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了 loongson 处理器体系结构的中断定义
 * 修改：
 * 		 1. 
 */
#ifndef _REWORKS_ARCH_IRQ_H_
#define _REWORKS_ARCH_IRQ_H_

#ifdef __cplusplus
extern "C"
{
#endif

#define  IRQ_NUM  (128)

#ifdef __cplusplus
}
#endif

#endif /* _IRQ_H_ */
