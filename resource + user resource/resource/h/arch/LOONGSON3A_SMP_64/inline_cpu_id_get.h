#ifndef _REWORKS_INLINE_CPU_ID_GET_H_
#define _REWORKS_INLINE_CPU_ID_GET_H_

#ifdef __cplusplus
extern "C" {
#endif

__attribute__((always_inline)) static inline int inline_cpu_id_get()
{
	
    register int number;
    
    asm volatile
	(
	"mfc0   %0, $15, 1\n"
	/*"andi	%0, 0x3ff\n"*/
	"andi   %0,0x7\n"	
	: "=&r" (number)		/* output: key is %0 */
	:					/* no input */
	: "memory"			/* memory */
						/* clobber for code barrier */
	);
    
    return number;
}

#ifdef __cplusplus
}
#endif
#endif /* _REWORKS_INLINE_CPU_ID_GET_H_ */
