#ifndef _REWORKS_BITOPS_H_
#define _REWORKS_BITOPS_H_

#ifdef __cplusplus
extern "C" 
{
#endif

/********************************************************************
 * huangyuan20080414: 
 * platform-independent code added for threads-scheduling.. etc.
 */
#include <cpu.h>
#include <reworks/types.h>

extern u8 ffs_lsb_tbl[];

#define ffs16( _value, _output ) \
{ \
    register u32 __value = (u32) (_value); \
    register const unsigned char *__p = ffs_lsb_tbl; \
    \
    if ( __value & 0xff ) \
      (_output) = __p[ __value & 0xff]; \
    else \
      (_output) = __p[ __value >> 8 ] + 8; \
}

//#define SZLONG_LOG 5
//#define SZLONG_MASK 31UL
#define SZLONG_LOG 6
#define SZLONG_MASK 63UL
//#define __LL		"ll	"
//#define __SC		"sc	"
#define __LL		"lld	"
#define __SC		"scd	"
#define __INS		"dins	"
#define __EXT		"dext	"
# define __SC_BEQZ "beqz	"
#define GCC_OFF_SMALL_ASM() "ZC"
# define __LLSC_CLOBBER		"memory"
#define kernel_uses_llsc   1
#define BITS_PER_LONG 64
#define BIT_WORD(nr)		((nr) / BITS_PER_LONG)
#define BIT(x)  (1UL << (x))

#ifdef __multi_core__
#define __smp_mb__before_atomic()
#define smp_mb__before_atomic()	__smp_mb__before_atomic()
# define smp_llsc_mb()		do { } while (0)

#define __bit_op(mem, insn, inputs...) do {			\
	unsigned long __temp;					\
								\
	asm volatile(						\
	"	.set		push			\n"	\
	"	.set		 mips64r2 	\n"	\
	"1:	" __LL		"%0, %1			\n"	\
	"	" insn		"			\n"	\
	"	" __SC		"%0, %1			\n"	\
	"	" __SC_BEQZ	"%0, 1b			\n"	\
	"	.set		pop			\n"	\
	: "=&r"(__temp), "+" GCC_OFF_SMALL_ASM()(mem)		\
	: inputs						\
	: __LLSC_CLOBBER);					\
} while (0)

#define __test_bit_op(mem, ll_dst, insn, inputs...) ({		\
	unsigned long __orig, __temp;				\
								\
	asm volatile(						\
	"	.set		push			\n"	\
	"	.set		mips64r2	\n"	\
	"1:	" __LL		ll_dst ", %2		\n"	\
	"	" insn		"			\n"	\
	"	" __SC		"%1, %2			\n"	\
	"	" __SC_BEQZ	"%1, 1b			\n"	\
	"	.set		pop			\n"	\
	: "=&r"(__orig), "=&r"(__temp),				\
	  "+" GCC_OFF_SMALL_ASM()(mem)				\
	: inputs						\
	: __LLSC_CLOBBER);					\
								\
	__orig;							\
})



/**
 * test_bit - Determine whether a bit is set
 * @nr: bit number to test
 * @addr: Address to start counting from
 */
static inline int test_bit(int nr, const volatile unsigned long *addr)
{
	return 1UL & (addr[BIT_WORD(nr)] >> (nr & (BITS_PER_LONG-1)));
}
/*
 * set_bit - Atomically set a bit in memory
 * @nr: the bit to set
 * @addr: the address to start counting from
 *
 * This function is atomic and may not be reordered.  See __set_bit()
 * if you do not require the atomic guarantees.
 * Note that @nr may be almost arbitrarily large; this function is not
 * restricted to acting on a single-word quantity.
 */
static inline void set_bit(unsigned long nr, volatile unsigned long *addr)
{
	volatile unsigned long *m = &addr[BIT_WORD(nr)];
	int bit = nr % BITS_PER_LONG;

//	if (!kernel_uses_llsc) {
//		__mips_set_bit(nr, addr);
//		return;
//	}
//
//	if ((MIPS_ISA_REV >= 2) && __builtin_constant_p(bit) && (bit >= 16)) {
//		__bit_op(*m, __INS "%0, %3, %2, 1", "i"(bit), "r"(~0));
//		return;
//	}

	__bit_op(*m, "or\t%0, %2", "ir"(BIT(bit)));
}

/*
 * clear_bit - Clears a bit in memory
 * @nr: Bit to clear
 * @addr: Address to start counting from
 *
 * clear_bit() is atomic and may not be reordered.  However, it does
 * not contain a memory barrier, so if it is used for locking purposes,
 * you should call smp_mb__before_atomic() and/or smp_mb__after_atomic()
 * in order to ensure changes are visible on other processors.
 */
static inline void clear_bit(unsigned long nr, volatile unsigned long *addr)
{
	volatile unsigned long *m = &addr[BIT_WORD(nr)];
	int bit = nr % BITS_PER_LONG;

//	if (!kernel_uses_llsc) {
//		__mips_clear_bit(nr, addr);
//		return;
//	}
//
//	if ((MIPS_ISA_REV >= 2) && __builtin_constant_p(bit)) {
//		__bit_op(*m, __INS "%0, $0, %2, 1", "i"(bit));
//		return;
//	}

	__bit_op(*m, "and\t%0, %2", "ir"(~BIT(bit)));
}


/*
 * change_bit - Toggle a bit in memory
 * @nr: Bit to change
 * @addr: Address to start counting from
 *
 * change_bit() is atomic and may not be reordered.
 * Note that @nr may be almost arbitrarily large; this function is not
 * restricted to acting on a single-word quantity.
 */
static inline void change_bit(unsigned long nr, volatile unsigned long *addr)
{
	volatile unsigned long *m = &addr[BIT_WORD(nr)];
	int bit = nr % BITS_PER_LONG;

//	if (!kernel_uses_llsc) {
//		__mips_change_bit(nr, addr);
//		return;
//	}

	__bit_op(*m, "xor\t%0, %2", "ir"(BIT(bit)));
}


/*
 * test_and_set_bit_lock - Set a bit and return its old value
 * @nr: Bit to set
 * @addr: Address to count from
 *
 * This operation is atomic and implies acquire ordering semantics
 * after the memory operation.
 */
static inline int test_and_set_bit_lock(unsigned long nr,
	volatile unsigned long *addr)
{
	volatile unsigned long *m = &addr[BIT_WORD(nr)];
	int bit = nr % BITS_PER_LONG;
	unsigned long res, orig;

	orig = __test_bit_op(*m, "%0",
				 "or\t%1, %0, %3",
				 "ir"(BIT(bit)));
	res = (orig & BIT(bit)) != 0;
	
	smp_llsc_mb();

	return res;
}

/*
 * test_and_set_bit - Set a bit and return its old value
 * @nr: Bit to set
 * @addr: Address to count from
 *
 * This operation is atomic and cannot be reordered.
 * It also implies a memory barrier.
 */
static inline int test_and_set_bit(unsigned long nr,
	volatile unsigned long *addr)
{
	smp_mb__before_atomic();
	return test_and_set_bit_lock(nr, addr);
}

/*
 * test_and_clear_bit - Clear a bit and return its old value
 * @nr: Bit to clear
 * @addr: Address to count from
 *
 * This operation is atomic and cannot be reordered.
 * It also implies a memory barrier.
 */
static inline int test_and_clear_bit(unsigned long nr,
	volatile unsigned long *addr)
{
	volatile unsigned long *m = &addr[BIT_WORD(nr)];
	int bit = nr % BITS_PER_LONG;
	unsigned long res, orig;

	smp_mb__before_atomic();

	
	orig = __test_bit_op(*m, "%0",
				 "or\t%1, %0, %3;"
				 "xor\t%1, %1, %3",
				 "ir"(BIT(bit)));
	res = (orig & BIT(bit)) != 0;
	

	smp_llsc_mb();

	return res;
}

/*
 * test_and_change_bit - Change a bit and return its old value
 * @nr: Bit to change
 * @addr: Address to count from
 *
 * This operation is atomic and cannot be reordered.
 * It also implies a memory barrier.
 */
static inline int test_and_change_bit(unsigned long nr,
	volatile unsigned long *addr)
{
	volatile unsigned long *m = &addr[BIT_WORD(nr)];
	int bit = nr % BITS_PER_LONG;
	unsigned long res, orig;

	smp_mb__before_atomic();


	orig = __test_bit_op(*m, "%0",
				 "xor\t%1, %0, %3",
				 "ir"(BIT(bit)));
	res = (orig & BIT(bit)) != 0;
	

	smp_llsc_mb();

	return res;
}

#undef __bit_op
#undef __test_bit_op
#else
#define CP0_BARRIER 
#define raw_local_irq_disable()		arch_local_irq_disable()
#define raw_local_irq_enable()		arch_local_irq_enable()

static inline int arch_local_irq_save()
{
	
    int key;
    int temp1;
    
    asm volatile
	(
/*	"di   %0\n"*/
/*	"di   %0\n"
	"nop\n"
	"nop\n"
	"nop\n"
	"nop\n"*/
	"mfc0 %0, $12\n"
	"li   %1, 0xfffffffe\n"
	"and  %1, %0, %1\n"
	"mtc0 %1, $12\n"
	""CP0_BARRIER"\n"
	: "=&r" (key), "=&r" (temp1)		/* output: key is %0 */
	:					/* no input */
	: "memory"			/* memory */
						/* clobber for code barrier */
	);
    
    return key;
}

 static inline void arch_local_irq_restore(int key)
{

    asm volatile
	(
	"mtc0	%0, $12\n"
	""CP0_BARRIER"\n"
	:
	: "r" (key)
	);
}

#define raw_local_irq_save(flags)			\
	do {						\
		flags = arch_local_irq_save();		\
	} while (0)

#define raw_local_irq_restore(flags)			\
	do {						\
		arch_local_irq_restore(flags);		\
	} while (0)
/*
 * set_bit - Atomically set a bit in memory
 * @nr: the bit to set
 * @addr: the address to start counting from
 *
 * This function is atomic and may not be reordered.  See __set_bit()
 * if you do not require the atomic guarantees.
 * Note that @nr may be almost arbitrarily large; this function is not
 * restricted to acting on a single-word quantity.
 */
static inline void set_bit(unsigned long nr, volatile unsigned long *addr)
{
	volatile unsigned long *a = &addr[BIT_WORD(nr)];
	unsigned int bit = nr % BITS_PER_LONG;
	unsigned long mask;
	unsigned long flags;

	mask = 1UL << bit;
	raw_local_irq_save(flags);
	*a |= mask;
	raw_local_irq_restore(flags);
}

/*
 * clear_bit - Clears a bit in memory
 * @nr: Bit to clear
 * @addr: Address to start counting from
 *
 * clear_bit() is atomic and may not be reordered.  However, it does
 * not contain a memory barrier, so if it is used for locking purposes,
 * you should call smp_mb__before_clear_bit() and/or smp_mb__after_clear_bit()
 * in order to ensure changes are visible on other processors.
 */
static inline void clear_bit(unsigned long nr, volatile unsigned long *addr)
{
	volatile unsigned long *a = &addr[BIT_WORD(nr)];
	unsigned int bit = nr % BITS_PER_LONG;
	unsigned long mask;
	unsigned long flags;

	mask = 1UL << bit;
	raw_local_irq_save(flags);
	*a &= ~mask;
	raw_local_irq_restore(flags);
}

/*
 * change_bit - Toggle a bit in memory
 * @nr: Bit to change
 * @addr: Address to start counting from
 *
 * change_bit() is atomic and may not be reordered.
 * Note that @nr may be almost arbitrarily large; this function is not
 * restricted to acting on a single-word quantity.
 */
static inline void change_bit(unsigned long nr, volatile unsigned  long *addr)
{
	volatile unsigned long *a = &addr[BIT_WORD(nr)];
	unsigned int bit = nr % BITS_PER_LONG;
	unsigned long mask;
	unsigned long flags;

	mask = 1UL << bit;
	raw_local_irq_save(flags);
	*a ^= mask;
	raw_local_irq_restore(flags);
}

/*
 * test_and_set_bit - Set a bit and return its old value
 * @nr: Bit to set
 * @addr: Address to count from
 *
 * This operation is atomic and cannot be reordered.
 * It also implies a memory barrier.
 */
static inline int test_and_set_bit(unsigned long nr,
	volatile unsigned long *addr)
{
	volatile unsigned long *a = &addr[BIT_WORD(nr)];
	unsigned int bit = nr % BITS_PER_LONG;
	unsigned long mask;
	unsigned long flags;
	int res;

	mask = 1UL << bit;
	raw_local_irq_save(flags);
	res = (mask & *a) != 0;
	*a |= mask;
	raw_local_irq_restore(flags);

	return res;
}

/*
 * test_and_clear_bit - Clear a bit and return its old value
 * @nr: Bit to clear
 * @addr: Address to count from
 *
 * This operation is atomic and cannot be reordered.
 * It also implies a memory barrier.
 */
static inline int test_and_clear_bit(unsigned long nr,
	volatile unsigned long *addr)
{
	volatile unsigned long *a = &addr[BIT_WORD(nr)];
	unsigned int bit = nr % BITS_PER_LONG;
	unsigned long mask;
	unsigned long flags;
	int res;

	mask = 1UL << bit;
	raw_local_irq_save(flags);
	res = (mask & *a) != 0;
	*a &= ~mask;
	raw_local_irq_restore(flags);
	return res;
}

/**
 * test_bit - Determine whether a bit is set
 * @nr: bit number to test
 * @addr: Address to start counting from
 */
static inline int test_bit(int nr, const volatile unsigned long *addr)
{
	return 1UL & (addr[BIT_WORD(nr)] >> (nr & (BITS_PER_LONG-1)));
}

/*
 * test_and_change_bit - Change a bit and return its old value
 * @nr: Bit to change
 * @addr: Address to count from
 *
 * This operation is atomic and cannot be reordered.
 * It also implies a memory barrier.
 */
static inline int test_and_change_bit(unsigned long nr,
	volatile unsigned long *addr)
{
	volatile unsigned long *a = &addr[BIT_WORD(nr)];
	unsigned int bit = nr % BITS_PER_LONG;
	unsigned long mask;
	unsigned long flags;
	int res;

	mask = 1UL << bit;
	raw_local_irq_save(flags);
	res = (mask & *a) != 0;
	*a ^= mask;
	raw_local_irq_restore(flags);
	return res;
}

#endif

#ifdef __cplusplus
}
#endif
#endif /* _REWORKS_BITOPS_H */
