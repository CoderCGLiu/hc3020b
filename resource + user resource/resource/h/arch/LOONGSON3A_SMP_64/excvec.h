
/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：定义异常管理框架中用到的宏和结构体。
 * 修改：       
 * 		2014-03-07, 宋伟, 创建. 
 */

#ifndef _REWORKS_EXCVEC_H_
#define _REWORKS_EXCVEC_H_

#ifdef __cplusplus
extern "C" {
#endif

/* 异常编码 */
#define MIPS_EXC_INT	0		/* 外部中断 */
#define MIPS_EXC_TLBM	1		/* TLB修改异常 */
#define MIPS_EXC_TLBL	2		/* TLB缺页异常 (fetch) */
#define MIPS_EXC_TLBS	3		/* TLB缺页异常 (store) */
#define MIPS_EXC_ADEL	4		/* 地址错误异常 (fetch) */
#define MIPS_EXC_ADES	5		/* 地址错误异常 (store) */
#define MIPS_EXC_IBE 	6		/* bus总线异常 (指令 fetch) */
#define MIPS_EXC_DBE	7		/* bus总线异常 (数据 load or store) */
#define MIPS_EXC_SYS	8		/* 系统调用异常 */
#define MIPS_EXC_BP 	9		/* 断点异常 */
#define MIPS_EXC_RI		10		/* 保留指令异常 */
#define MIPS_EXC_FPU	11		/* 协处理器异常 */
#define MIPS_EXC_OVF	12		/* 计算溢出异常 */
#define MIPS_EXC_TRAP	13		/* trap异常 */
#define MIPS_EXC_FPE	15		/* 浮点异常 */
#define MIPS_EXC_WATCH	23		/* watchpoint异常 */

#define MIPS_EXC_FPE_INVALID	55	/* 无效浮点操作 */
#define MIPS_EXC_FPE_DIV0		56	/* 除零 */
#define MIPS_EXC_FPE_OVERFLOW	57	/* 上溢 */
#define MIPS_EXC_FPE_UNDERFLOW	58	/* 下溢 */
#define MIPS_EXC_FPE_INEXACT	59	/* 非确定性结果 */

#ifdef __cplusplus
}
#endif
#endif
