#ifndef _REWORKS_INLINE_CPU_ID_GET_H_
#define _REWORKS_INLINE_CPU_ID_GET_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
 * 0xFEE00020 is the address of the local APIC ID Register.
 */
#define _IA32_LOC_APIC_ID       0xFEE00020

#define _IA32_CPU_INDEX_SHIFT   (0x18)
#define _IA32_CPU_INDEX_MASK    (0xFF)

extern u8 mpCpuIndexTable[256];
extern unsigned int vxBaseCpuPhysIndex;
extern volatile int vxCpuLibInitFlag;

__attribute__((always_inline)) static inline int inline_cpu_id_get()
{

    unsigned int cpu, tmp1, tmp2;

    __asm__ volatile 
    (
    	"movl    $0xFEE00020,%1 \n"
		"movl    0x0(%1),%1 \n"                    
		"ror     $0x18,%1 \n"        
		"andl    $0xFF,%1 \n"         
		"leal    mpCpuIndexTable(%1),%2 \n"
		"mov     0x0(%2),%1 \n"                   
		"andl    $0xFF,%1   \n"       
		"subl    vxBaseCpuPhysIndex, %1 \n"
    	"movl    %1,%0 \n"
    	: "=&a" (cpu),"=&r" (tmp1),"=&r" (tmp2): 
    	: "memory");

    return cpu;
    
}

#ifdef __cplusplus
}
#endif
#endif /* _REWORKS_INLINE_CPU_ID_GET_H_ */

