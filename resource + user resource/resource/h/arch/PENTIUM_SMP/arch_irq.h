/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了CSP模块中x86体系结构中断管理的接口
 * 修改：
 * 		 1. 2011-10-31，规范CSP 
 */
#ifndef _REWORKS_ARCH_IRQ_H_
#define _REWORKS_ARCH_IRQ_H_

#ifdef __cplusplus
extern "C"
{
#endif 

struct irq_data
{
	INT_HANDLER		handler;
	void			*param;
	char  *name;   
	int    pri;    
};	

#ifdef __cplusplus
}
#endif 

#endif 
