
#ifndef _REWORKS_ARCH_CACHE_H_
#define _REWORKS_ARCH_CACHE_H_

#ifdef __cplusplus
extern "C" {
#endif

#define	CLFLUSH_DEF_BYTES	64	/* def bytes flushed by CLFLUSH */
#define	CLFLUSH_MAX_BYTES	255	/* max bytes flushed with CLFLUSH */

#ifndef	_ASMLANGUAGE
#include <reworks/types.h>
#include <limits.h>

typedef	struct					/* Cache程序指针 */
{
    FUNCPTR	enableRtn;			/* 使能cacheEnable() */
    FUNCPTR	disableRtn;			/* 非能cacheDisable() */
    FUNCPTR	lockRtn;			/* 锁cacheLock() */
    FUNCPTR	unlockRtn;			/* 开cacheUnlock() */
    FUNCPTR	flushRtn;			/* 刷cacheFlush() */
    FUNCPTR	invalidateRtn;		/* 无效cacheInvalidate() */
    FUNCPTR	clearRtn;			/* 清除cacheClear() */
    
    FUNCPTR	textUpdateRtn;		/* 更新cacheTextUpdate() */
    FUNCPTR	pipeFlushRtn;		/* cachePipeFlush() */
    FUNCPTR	dmaMallocRtn;		/* cacheDmaMalloc() */
    FUNCPTR	dmaFreeRtn;			/* cacheDmaFree() */
    FUNCPTR	dmaVirtToPhysRtn;	/* 虚拟到物理virtual-to-Physical Translation */
    FUNCPTR	dmaPhysToVirtRtn;	/* 物理到虚拟physical-to-Virtual Translation */

} RE_CACHE_LIB;

typedef	unsigned int	RE_CACHE_MODE;		/* CACHE_MODE */
    
extern  RE_CACHE_LIB re_cache_lib;

#define	ENTIRE_CACHE	(ULONG_MAX)
/* Cache mode soft bit masks */
#define	CACHE_DISABLED			0x00	/* No cache or disabled */
#define	CACHE_WRITETHROUGH		0x01	/* Write-through Mode */
#define	CACHE_COPYBACK			0x02	/* Copyback Mode */
#define	CACHE_WRITEALLOCATE		0x04	/* Write Allocate Mode */
#define	CACHE_NO_WRITEALLOCATE	0x08
#define	CACHE_SNOOP_ENABLE		0x10	/* Bus Snooping */
#define	CACHE_SNOOP_DISABLE		0x20
#define	CACHE_BURST_ENABLE		0x40	/* Cache Burst Cycles */
#define	CACHE_BURST_DISABLE		0x80

/* Cache macros */
extern int cache_arch_init
(
    RE_CACHE_MODE	instMode,	/* instruction cache mode */
    RE_CACHE_MODE	dataMode	/* data cache mode */
);
extern int cache_arch_enable (RE_CACHE_TYPE cache);
extern int cache_arch_disable (RE_CACHE_TYPE cache);
extern int cache_arch_lock (RE_CACHE_TYPE cache, void * address, size_t bytes);
extern int cache_arch_unlock (RE_CACHE_TYPE cache,
				void * address, size_t bytes);
extern int cache_arch_clear (RE_CACHE_TYPE cache, void * address, size_t bytes);
extern int cache_arch_flush (RE_CACHE_TYPE cache, void * address, size_t bytes);
extern int cache_arch_text_update (void * address, size_t bytes);
extern int cache_arch_clear_entry (RE_CACHE_TYPE cache, void * address);

extern void cache_i86_reset (void);
extern void cache_i86_enable (void);
extern void cache_i86_disable (void);
extern void cache_i86_lock (void);
extern void cache_i86_unlock (void);
extern void cache_i86_clear (void);
extern void cache_i86_flush (void);
extern void cache_pen4_clear (RE_CACHE_TYPE cache, void * address, size_t bytes);
extern void cache_pen4_flush (RE_CACHE_TYPE cache, void * address, size_t bytes);
extern int cache_dma_free(void * pBuf );
extern void * cache_dma_malloc(size_t bytes);

#define	REWORKS_CACHE_FLUSH(adrs, bytes)		cache_flush(DATA_CACHE,adrs,bytes)

#define	REWORKS_CACHE_INVALIDATE(adrs, bytes)	cache_invalidate(DATA_CACHE,adrs,bytes)

#define	REWORKS_CACHE_DMA_MALLOC(bytes)				cache_dma_malloc(bytes)

#define	REWORKS_CACHE_DMA_FREE(adrs)				cache_dma_free(adrs)

#define	REWORKS_CACHE_PIPE_FLUSH()	\
        ((re_cache_lib.pipeFlushRtn == NULL) ? OK :	\
        (re_cache_lib.pipeFlushRtn) ())

#define	CACHE_TEXT_UPDATE(adrs, bytes) \
		((re_cache_lib.textUpdateRtn == NULL) ? OK :(re_cache_lib.textUpdateRtn)((adrs), (bytes)))
#endif
#ifdef __cplusplus
}
#endif

#endif
