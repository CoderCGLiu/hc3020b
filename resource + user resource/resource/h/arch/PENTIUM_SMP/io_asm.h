/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了CSP模块中x86体系结构IO操作
 * 修改：
 * 		 1. 2011-10-31，规范CSP 
 */
#ifndef _REWORKS_IO_ASM_H_
#define _REWORKS_IO_ASM_H_

#ifdef __cplusplus
extern "C"
{
#endif 
	
#include <reworks/types.h>

#ifdef SLOW_IO_BY_JUMPING
#	define __SLOW_DOWN_IO "\njmp 1f\n1:\tjmp 1f\n1:"
#else 
#	define __SLOW_DOWN_IO "\noutb %%al,$0x80"
#endif 

#ifdef REALLY_SLOW_IO
#	define __FULL_SLOW_DOWN_IO __SLOW_DOWN_IO __SLOW_DOWN_IO __SLOW_DOWN_IO __SLOW_DOWN_IO
#else 
#	define __FULL_SLOW_DOWN_IO __SLOW_DOWN_IO
#endif 

#define __OUT1(s,x) 	\
	static inline void out##s(unsigned x value, unsigned short port) {

#define __OUT2(s,s1,s2) \
	__asm__ __volatile__ ("out" #s " %" s1 "0,%" s2 "1"

#define __OUT0(s,s1,x) \
	__OUT1(s,x) __OUT2(s,s1,"w") : : "a" (value), "Nd" (port)); } \
	__OUT1(s##_p,x) __OUT2(s,s1,"w") __FULL_SLOW_DOWN_IO : : "a" (value), "Nd" (port));} \

#define __IN1(s) 		\
	static inline RETURN_TYPE in##s(unsigned short port) { RETURN_TYPE _v;

#define __IN2(s,s1,s2) \
	__asm__ __volatile__ ("in" #s " %" s2 "1,%" s1 "0"

#define __IN0(s,s1,i...) \
	__IN1(s) __IN2(s,s1,"w") : "=a" (_v) : "Nd" (port) ,##i ); return _v; } \
	__IN1(s##_p) __IN2(s,s1,"w") __FULL_SLOW_DOWN_IO : "=a" (_v) : "Nd" (port) ,##i ); return _v; } \

#define __INS(s) 		\
	static inline void ins##s(unsigned short port, void * addr, unsigned long count) \
	{ __asm__ __volatile__ ("rep ; ins" #s 	\
	: "=D" (addr), "=c" (count) : "d" (port),"0" (addr),"1" (count)); }

#define __OUTS(s) 		\
	static inline void outs##s(unsigned short port, const void * addr, unsigned long count) \
	{ __asm__ __volatile__ ("rep ; outs" #s 	\
	: "=S" (addr), "=c" (count) : "d" (port),"0" (addr),"1" (count)); }

#define RETURN_TYPE unsigned char
__IN0(b, "")
#undef RETURN_TYPE
#define RETURN_TYPE unsigned short
__IN0(w, "")
#undef RETURN_TYPE
#define RETURN_TYPE unsigned int
__IN0(l, "")
#undef RETURN_TYPE

__OUT0(b, "b", char)__OUT0(w, "w", short)__OUT0(l, , int)

__INS(b)__INS(w)__INS(l)

__OUTS(b)__OUTS(w)__OUTS(l)

#ifdef __cplusplus
}
#endif 

#endif
