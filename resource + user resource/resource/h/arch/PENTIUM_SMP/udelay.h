#ifndef _REWORKS_UDELAY_H_
#define _REWORKS_UDELAY_H_

#ifdef __cplusplus
extern "C"
{
#endif 

extern void __bad_udelay(void);

extern void __udelay(unsigned long usecs);
extern void __ndelay(unsigned long nsecs);

extern void __const_udelay(unsigned long usecs);

extern void __delay(unsigned long loops);

#define rdtscl(low) \
	__asm__ __volatile__("rdtsc" : "=a" (low) : : "edx")

#define udelay(n) (__builtin_constant_p(n) ? \
	((n) > 20000 ? __bad_udelay() : __const_udelay((n) * 0x10c6ul)) : \
	__udelay(n))

/* REP NOP (PAUSE) is a good thing to insert into busy-wait loops. */
static inline void rep_nop(void)
{
    __asm__ __volatile__("rep;nop"::: "memory");
}

#ifdef __cplusplus
}
#endif 

#endif 
