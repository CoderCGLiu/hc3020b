/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了CSP模块中x86体系结构处理器定义
 * 修改：
 * 		 1. 2011-10-31，规范CSP 
 */
#ifndef _REWORKS_CPU_H_
#define _REWORKS_CPU_H_

#ifdef __cplusplus
extern "C"
{
#endif 

#include <machine/endian.h>

#ifndef __multi_core__
#define MAX_SMP_CPUS	1	
#else
#define MAX_SMP_CPUS	4	
#endif

#ifdef  _LITTLE_ENDIAN	
#undef  _LITTLE_ENDIAN
#endif

#ifdef  _BIG_ENDIAN
#undef  _BIG_ENDIAN
#endif

#define _LITTLE_ENDIAN			LITTLE_ENDIAN //4321
#define _BIG_ENDIAN				BIG_ENDIAN //1234
#define _BYTE_ORDER				_LITTLE_ENDIAN

#ifdef BYTE_ORDER
#undef BYTE_ORDER
#define BYTE_ORDER	_BYTE_ORDER
#endif

#ifndef _ASMLANGUAGE

#include <reworks/types.h>

#define rdtsc64(result) 		\
	{asm volatile(".byte 0x0F, 0x31" : "=A" (result));}

#define CPU_STACK_MIN_SIZE          	4096
#define CPU_ALIGNMENT                   4
#define CPU_EFLAGS_INTERRUPTS_ON 		0x00003202
#define CPU_EFLAGS_INTERRUPTS_OFF 		0x00003002
#define IDT_SIZE 						256
#define I386_HAS_BSWAP 					1

static inline unsigned int i386_swap_U32(unsigned int value)
{
	unsigned long lout;

#if (I386_HAS_BSWAP == 0)
	asm volatile("rorw  $8,%%ax;""rorl  $16,%0;""rorw  $8,%%ax": "=a"(lout): "0"(value));
#else 
	__asm__ volatile("bswap %0": "=r"(lout): "0"(value));
#endif 
	return (lout);
}

static inline unsigned int i386_swap_U16(unsigned int value)
{
	unsigned short sout;

	__asm__ volatile("rorw $8,%0": "=r"(sout): "0"(value));
	return (sout);
}

#define CPU_swap_u32( _value )  	i386_swap_U32( _value )
#define CPU_swap_u16( _value )  	i386_swap_U16( _value )

/**
 * 处理器ID定义
 */
typedef struct
{
    int 			cpuid_max;
    char 			cpu_vendor[13]; 
    int 			cpu_family;
    int 			cpu_ext_family;
    int 			cpu_model;
    int 			cpu_step;
    int 			feature_ebx; 
    int 			feature_ecx; 
    int 			feature_edx; 
    int 			cache_eax; 
    int 			cache_ebx;
    int 			cache_ecx;
    int 			cache_edx;
    int 			serial_no[2];
    int 			amd64_supported;
    char 			cpu_brandstr[48];
}Cpuid_Ctrl;

/**
 * 上下文控制结构定义
 */
typedef struct
{
    u32 				edi;
    u32 				esi;
    u32 				edx;
    u32  				ecx;
    u32  				ebx;
    u32  				eax;
    u32  				ebp;
    u32  				pc;
    u32  				return_cs;
    u32  				eflags;
}Context_Ctrl;

/**
 * 寄存器组定义
 */
typedef struct
{
    u32  				edi;
    u32  				esi;
    u32  				edx;
    u32  				ecx;
    u32  				ebx;
    u32  				eax;
    u32  				ebp;
    u32  				pc;
    u32  				return_cs;
    u32  				eflags;
    u32  				esp;
} REG_SET;

typedef struct
{
    u32  				edi;
    u32  				esi;
    u32  				ebp;
    u32  				esp;
    u32  				ebx;
    u32  				edx;
    u32  				ecx;
    u32  				eax;
    u32  				vector;
    u32  				error_code;
    u32  				eip;
    u32  				cs;
    u32  				eflags;
}pt_regs_t;

typedef struct
{
    void(*idle_task)	(void);
    boolean 			do_zero_of_workspace;
    u32 				idle_task_stack_size;
    u32 				interrupt_stack_size;
}rtos_cpu_table;

/* number of FP/MM and XMM registers on coprocessor */
#define FP_NUM_REGS		    		8	/* number of FP/MM registers */
#define XMM_NUM_REGS				8	/* number of XMM registers */
#define FP_NUM_RESERVED				14	/* reserved area in FPX_CONTEXT */

typedef struct
{
    u8 				f[10]; /* ST[0-7] or MM[0-7] */
} Double;

/* DOUBLEX_SSE - double extended precision used in FPX_CONTEXT for SSE */
typedef struct
{
    u8 				f[10]; /* ST[0-7] or MM[0-7] */
    u8 				r[6]; /* reserved */
} Double_SSE;

typedef struct fpOcontext
{
    int 			fpcr; /* 4    control word */
    int 			fpsr; /* 4    status word */
    int 			fptag; /* 4    tag word */
    int 			ip; /* 4    instruction pointer */
    short			cs; /* 2    instruction pointer selector */
    short 			op; /* 2    last FP instruction op code */
    int 			dp; /* 4    data pointer */
    int 			ds; /* 4    data pointer selector */
    Double 			fpx[FP_NUM_REGS]; /* 8*10 FR[0-7] non-TOS rel. order */
} Fp_Context_Old; /* 108  bytes total */

/* FPX_CONTEXT - New FP context used by fxsave/fxrstor instruction */
typedef struct fpXcontext
{
    short 			fpcr; /* 2     control word */
    short 			fpsr; /* 2     status word */
    short 			fptag; /* 2     tag word */
    short 			op; /* 2     last FP instruction op code */
    int 			ip; /* 4     instruction pointer */
    int 			cs; /* 4     instruction pointer selector */
    int 			dp; /* 4     data pointer */
    int 			ds; /* 4     data pointer selector */
    int 			reserved0; /* 4     reserved */
    int 			reserved1; /* 4     reserved */
    Double_SSE 		fpx[FP_NUM_REGS]; /* 8*16  FR[0-7] non-TOS rel. order */
    Double_SSE 		xmm[XMM_NUM_REGS]; /* 8*16  XMM[0-7] */
    Double_SSE 		res2[FP_NUM_RESERVED]; /* 14*16 reserved */
} Fp_Context_Fast; /* 512   bytes total */

/* 定义浮点上下文 */
typedef struct _Fp_Context
{
    union
    {
        Fp_Context_Old 		o; /* old FPO_CONTEXT for fsave/frstor */
        Fp_Context_Fast 	n; /* new FPX_CONTEXT for fxsave/fxrstor */
    };
}Fp_Context;

#define HAVE_FP_CONTEXT

/**
 * 异常处理接口类型
 */
typedef void(*EXC_HANDLER)(int, void *);

/**
 * 处理器类型
 */
extern int cpu_type;

/**
 * 浮点协处理器类型
 */
extern int fpu_type;

/**
 * 处理器ID
 */
extern Cpuid_Ctrl cpu_id;

extern int i386_cr0_get(void);
extern int i386_cr2_get(void);
extern int i386_cr3_get(void);
extern int i386_cr4_get(void);
extern int i386_ebp_get(void);
extern void i386_cr0_set(int cr0);
extern void i386_cr2_set(int cr2);
extern void i386_cr3_set(int cr3);
extern void i386_cr4_set(int cr4);

/**
 * 上下文初始化接口
 */
extern void context_init(Context_Ctrl *the_context, u32 isr, void *entry_point, void *prev_entry);

/**
 * 上下文切换接口
 */
extern void context_switch(Context_Ctrl **run, Context_Ctrl *heir);

/**
 * 上下文恢复接口
 */
extern void context_restore(Context_Ctrl *new_context);

/**
 * 设置异常处理钩子
 */
extern EXC_HANDLER exception_handler_set(EXC_HANDLER the_exc_handler);

/**
 * 将上下文内容转换为寄存器组
 */
extern int context_ctrl_to_reg_set(REG_SET *pRegs, Context_Ctrl *sp);

/**
 * 以指定的接口打印函数调用栈信息
 */
extern void print_stack(int *frame_ptr, int *eip, int (*print_func)(const char *arg, ...));

/**
 * 获取x86处理器时间戳接口
 */
extern u64 sys_timestamp(void);

/**
 * 获取x86处理器频率的接口
 */
extern u32 sys_timestamp_freq(void);

extern SYSTEM_BASIC_INFO *sys_info_get(void);

#ifdef __multi_core__
#define MEM_BARRIER_R() __asm volatile ("lfence" ::: "memory")
#define MEM_BARRIER_W() __asm volatile ("sfence" ::: "memory")
#define MEM_BARRIER_RW() __asm volatile ("mfence" ::: "memory")
#define INSTR_BARRIER()  __asm volatile ("mfence" ::: "memory")
#endif

#endif 

#ifdef __cplusplus
}
#endif 

#endif
