#ifndef _REWORKS_CORE_DELAY_H_
#define _REWORKS_CORE_DELAY_H_

#ifdef __cplusplus
extern "C" {
#endif

#define LPS_PREC 8  										// 校准精度，精度越高校准时间越长
#define OPTIMIZE(level) __attribute__((optimize(level)))    // 为防止udelay函数被优化，故定义还宏用以修改udelay相关函数
#define LPS_STEP (1<<1)		

extern void __udelay(unsigned long usecs);

OPTIMIZE("O0") inline void __delay(volatile unsigned int loops)
{
		int d0;
		__asm__ __volatile__(
			"\tjmp 1f\n"
			".align 16\n"
			"1:\tjmp 2f\n"
			".align 16\n"
			"2:\tdecl %0\n\tjns 2b"
			:"=&a" (d0)
			:"0" (loops));
}

static inline void __delay_2(u32 us)
{
	return __udelay(us);
}
#ifdef __cplusplus
}
#endif
#endif /* _REWORKS_CORE_DELAY_H_ */
