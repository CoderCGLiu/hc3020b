/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：LOONGARCH汇编语言相关定义的头文件
 * 修改：
 * 		
 */

#ifndef _MIPS_ASM_H_
#define _MIPS_ASM_H_

#ifdef __cplusplus
extern "C"
{
#endif

#ifdef ASM_LANGUAGE

#define LEAF(name) \
  	.text; \
  	.globl	name; \
name:

#define XLEAF(name) \
  	.text; \
  	.globl	name; \
  	.aent	name; \
name:

#define WLEAF(name) \
  	.text; \
  	.weakext name; \
  	.ent	name; \
name:

#define SLEAF(name) \
  	.text; \
  	.ent	name; \
name:

#define END(name) \
  	.size name,.-name; \
  	.end	name

#define SEND(name) END(name)
#define WEND(name) END(name)

#endif /* ASM_LANGUAGE */

#ifdef __cplusplus
}
#endif
#endif 
