#ifdef __multi_core__
#ifndef _LS3A_IPI_H_
#define _LS3A_IPI_H_

#ifdef __cplusplus
extern "C"
{
#endif

#define IPI_BASE 0x800000001fe01000

#define IPI_BASE_OFFSET 0x100
#define IPI_CORE0_OFFSET 0x0
#define IPI_CORE1_OFFSET 0x100
#define IPI_CORE2_OFFSET 0x200
#define IPI_CORE3_OFFSET 0x300

#define IPI_STATUS 0x0
#define IPI_ENABLE 0x4
#define IPI_SET 0x8
#define IPI_CLEAR 0xc
#define IPI_MAILBOX 0x20
#define IPI_MAILBOX_OFFSET 0x8

/*group1 IPI相关寄存器设置, GROUP1的实际基地址应为0x10003ff01000，
 *但 32位指针不能表示40位的地址，故在读写这些寄存器的汇编中添加偏移
 */
/* ls dual3a */
#define GROUP1_IPI_BASE_LS3A 0x800000001fe01000
#define GROUP1_IPI_BASE_LS3B 0x800010001fe01000

//#define GROUP1_IPI_BASE (((get_c0_prid() & 0x0ffff) == PRID_LSN3A) ? GROUP1_IPI_BASE_LS3A : GROUP1_IPI_BASE_LS3B)
#define IPI_CORE4_OFFSET 0x0
#define IPI_CORE5_OFFSET 0x100
#define IPI_CORE6_OFFSET 0x200
#define IPI_CORE7_OFFSET 0x300

#ifdef __cplusplus
}
#endif

#endif
#endif
