#ifndef __ASM_TLB_H
#define __ASM_TLB_H

#include <asm/loongarchregs.h>
/*
 * TLB Invalidate Flush
 */
static inline void tlbclr(void)
{
	__asm__ __volatile__("tlbclr");
}

static inline void tlbflush(void)
{
	__asm__ __volatile__("tlbflush");
}

/*
 * TLB R/W operations.
 */
static inline void tlb_probe(void)
{
	__asm__ __volatile__("tlbsrch");
}

static inline void tlb_read(void)
{
	__asm__ __volatile__("tlbrd");
}

static inline void tlb_write_indexed(void)
{
	__asm__ __volatile__("tlbwr");
}

static inline void tlb_write_random(void)
{
	__asm__ __volatile__("tlbfill");
}

/*
 * Guest TLB Invalidate Flush
 */
static inline void guest_tlbflush(void)
{
	__asm__ __volatile__(
		".word 0x6482401\n\t");
}

/*
 * Guest TLB R/W operations.
 */
static inline void guest_tlb_probe(void)
{
	__asm__ __volatile__(
		".word 0x6482801\n\t");
}

static inline void guest_tlb_read(void)
{
	__asm__ __volatile__(
		".word 0x6482c01\n\t");
}

static inline void guest_tlb_write_indexed(void)
{
	__asm__ __volatile__(
		".word 0x6483001\n\t");
}

static inline void guest_tlb_write_random(void)
{
	__asm__ __volatile__(
		".word 0x6483401\n\t");
}

extern void handle_tlb_load(void);
extern void handle_tlb_store(void);
extern void handle_tlb_modify(void);
extern void handle_tlb_refill(void);
extern void handle_tlb_rixi(void);

#endif /* __ASM_TLB_H */
