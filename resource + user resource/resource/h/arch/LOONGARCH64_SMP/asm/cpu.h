/*
*  loongson3A架构相关定义的头文件
*/
#ifndef __LS3A_CPU_H__
#define __LS3A_CPU_H__

#define MAX_SMP_CPUS  4

#define	CAUSE_IP12	0x00001000	/* External level 11 pending */
#define	CAUSE_IP11	0x00000800	/* External level 11 pending */
#define	CAUSE_IP10	0x00000400	/* External level 10 pending */
#define	CAUSE_IP9	0x00000200	/* External level 9 pending */
#define	CAUSE_IP8	0x00000100	/* External level 8 pending */
#define	CAUSE_IP7	0x00000080	/* External level 7 pending */
#define	CAUSE_IP6	0x00000040	/* External level 6 pending */
#define	CAUSE_IP5	0x00000020	/* External level 5 pending */
#define	CAUSE_IP4	0x00000010	/* External level 4 pending */
#define	CAUSE_IP3	0x00000008	/* External level 3 pending */
#define	CAUSE_IP2	0x00000004	/* External level 2 pending */
#define	CAUSE_SW1	0x00000002	/* Software level 1 pending */
#define	CAUSE_SW0	0x00000001	/* Software level 0 pending */
#define	CAUSE_IPMASK	0x00001FFF	/* Pending interrupt mask */


#define	SR_IBIT12	0x00001000	/* bit level 11  */
#define	SR_IBIT11	0x00000800	/* bit level 11  */
#define	SR_IBIT10	0x00000400	/* bit level 10  */
#define	SR_IBIT9	0x00000200	/* bit level 9  */
#define	SR_IBIT8	0x00000100	/* bit level 8  */
#define	SR_IBIT7	0x00000080	/* bit level 7  */
#define	SR_IBIT6	0x00000040	/* bit level 6  */
#define	SR_IBIT5	0x00000020	/* bit level 5  */
#define	SR_IBIT4	0x00000010	/* bit level 4  */
#define	SR_IBIT3	0x00000008	/* bit level 3  */
#define	SR_IBIT2	0x00000004	/* bit level 2  */
#define	SR_IBIT1	0x00000002	/* bit level 1  */
#define	SR_IBIT0	0x00000001	/* bit level 0  */

#endif /* __LS3A_CPU_H__ */
