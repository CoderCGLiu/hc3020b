#ifndef _ASM_STACKFRAME_H
#define _ASM_STACKFRAME_H
#include <asm/regdef.h>
#include <asm/asm-offset.h>
#include <asm/loongarchregs.h>

	.macro BACKUP_T0T1
	csrwr	t0, EXCEPTION_KS0
	csrwr	t1, EXCEPTION_KS1
	.endm

	.macro RELOAD_T0T1
	csrrd   t0, EXCEPTION_KS0
	csrrd   t1, EXCEPTION_KS1
	.endm
   
	.macro	SAVE_TEMP  ctx_sp
	st.d	t0, \ctx_sp , T0_OFFSET * R_SZ
	st.d	t1, \ctx_sp , T1_OFFSET * R_SZ
	st.d	t2, \ctx_sp , T2_OFFSET * R_SZ
	st.d	t3, \ctx_sp , T3_OFFSET * R_SZ
	st.d	t4, \ctx_sp , T4_OFFSET * R_SZ
	st.d	t5, \ctx_sp , T5_OFFSET * R_SZ
	st.d	t6, \ctx_sp , T6_OFFSET * R_SZ
	st.d	t7, \ctx_sp , T7_OFFSET * R_SZ
	st.d	t8, \ctx_sp , T8_OFFSET * R_SZ
	.endm

	.macro	SAVE_STATIC  ctx_sp
	st.d	s0, \ctx_sp , S0_OFFSET * R_SZ
	st.d	s1, \ctx_sp , S1_OFFSET * R_SZ
	st.d	s2, \ctx_sp , S2_OFFSET * R_SZ
	st.d	s3, \ctx_sp , S3_OFFSET * R_SZ
	st.d	s4, \ctx_sp , S4_OFFSET * R_SZ
	st.d	s5, \ctx_sp , S5_OFFSET * R_SZ
	st.d	s6, \ctx_sp , S6_OFFSET * R_SZ
	st.d	s7, \ctx_sp , S7_OFFSET * R_SZ
	st.d	s8, \ctx_sp , S8_OFFSET * R_SZ
	.endm
	
	.macro	SAVE_SOME
	csrrd	t1, LOONGARCH_CSR_CRMD
	andi	t1, t1, 0x3	            /* extract pplv bit */
	move	t0, sp
	beqz	t1, 8f
	/* Called from user mode, new stack. */
	/* TODO:: get_saved_sp docfi=\docfi tosp=1 */
8:
	addi.d  sp, sp, -THR_INT_CONTEXT_CONTROL_SIZE
	st.d	t0, sp, SP_OFFSET * R_SZ
	st.d	zero, sp, ZO_OFFSET * R_SZ
	
	li.w	t1, INTERRUPT_TYPE
	st.w	t1, sp, 0
	
	csrrd	t0, LOONGARCH_CSR_PRMD
	st.d	t0, sp, PRMD_OFFSET * R_SZ
	csrrd	t0, LOONGARCH_CSR_CRMD
	st.d	t0, sp, CRMD_OFFSET * R_SZ
	csrrd	t0, LOONGARCH_CSR_ECFG
	st.d	t0, sp, ECFG_OFFSET * R_SZ
	csrrd	t0, LOONGARCH_CSR_EUEN
	st.d    t0, sp, EUEN_OFFSET * R_SZ
	csrrd	t0, LOONGARCH_CSR_ESTAT
	st.d    t0, sp, ESTAT_OFFSET * R_SZ
	csrrd	t0, LOONGARCH_CSR_BADV
	st.d    t0, sp, BVADDR_OFFSET * R_SZ
	
	st.d	ra, sp, RA_OFFSET * R_SZ
	st.d	a0, sp, A0_OFFSET * R_SZ
	st.d	a1, sp, A1_OFFSET * R_SZ
	st.d	a2, sp, A2_OFFSET * R_SZ
	st.d	a3, sp, A3_OFFSET * R_SZ
	st.d	a4, sp, A4_OFFSET * R_SZ
	st.d	a5, sp, A5_OFFSET * R_SZ
	st.d	a6, sp, A6_OFFSET * R_SZ
	st.d	a7, sp, A7_OFFSET * R_SZ
	
	csrrd	t0, LOONGARCH_CSR_EPC
	st.d	t0, sp, EPC_OFFSET * R_SZ
	st.d	tp, sp, TP_OFFSET * R_SZ
	st.d	fp, sp, FP_OFFSET * R_SZ

	/* Set thread_info if we're coming from user mode */
/*	csrrd	t0, LOONGARCH_CSR_PRMD
	andi	t0, t0, 0x3	
	beqz	t0, 9f */

/*  ��ȡջ����ַ,TODO:: */
/*	li.d	tp, ~_THREAD_MASK
	and	    tp, tp, sp
	st.d    x0, sp, X0_OFFSET * R_SZ
9:  */
	.endm

	.macro	SAVE_ALL 
	SAVE_SOME 
	SAVE_TEMP    sp
	SAVE_STATIC  sp
	li.w t0, INTERRUPT_TYPE
	st.w t0, sp, 0
	.endm

	.macro	RESTORE_TEMP  ctx_sp
	ld.d	t0, \ctx_sp , T0_OFFSET * R_SZ
	ld.d	t1, \ctx_sp , T1_OFFSET * R_SZ
	ld.d	t2, \ctx_sp , T2_OFFSET * R_SZ
	ld.d	t3, \ctx_sp , T3_OFFSET * R_SZ
	ld.d	t4, \ctx_sp , T4_OFFSET * R_SZ
	ld.d	t5, \ctx_sp , T5_OFFSET * R_SZ
	ld.d	t6, \ctx_sp , T6_OFFSET * R_SZ
	ld.d	t7, \ctx_sp , T7_OFFSET * R_SZ
	ld.d	t8, \ctx_sp , T8_OFFSET * R_SZ
	.endm

	.macro	RESTORE_STATIC ctx_sp
	ld.d	s0, \ctx_sp , S0_OFFSET * R_SZ
	ld.d	s1, \ctx_sp , S1_OFFSET * R_SZ
	ld.d	s2, \ctx_sp , S2_OFFSET * R_SZ
	ld.d	s3, \ctx_sp , S3_OFFSET * R_SZ
	ld.d	s4, \ctx_sp , S4_OFFSET * R_SZ
	ld.d	s5, \ctx_sp , S5_OFFSET * R_SZ
	ld.d	s6, \ctx_sp , S6_OFFSET * R_SZ
	ld.d	s7, \ctx_sp , S7_OFFSET * R_SZ
	ld.d	s8, \ctx_sp , S8_OFFSET * R_SZ
	.endm

	.macro	RESTORE_SOME    ctx_sp
	/* LoongArch clear IE and PLV */
	ld.d	v0, \ctx_sp, PRMD_OFFSET * R_SZ
	csrwr	v0, LOONGARCH_CSR_PRMD
	ld.d	v0, \ctx_sp, EPC_OFFSET * R_SZ
	csrwr	v0, LOONGARCH_CSR_EPC
	andi    v0, v0, 0x3	/* extract pplv bit */
	beqz    v0, 8f
	ld.d    x0, \ctx_sp, X0_OFFSET * R_SZ
8:
	ld.d	ra, \ctx_sp, RA_OFFSET * R_SZ
	ld.d	a0, \ctx_sp, A0_OFFSET * R_SZ
	ld.d	a1, \ctx_sp, A1_OFFSET * R_SZ
	ld.d	a2, \ctx_sp, A2_OFFSET * R_SZ
	ld.d	a3, \ctx_sp, A3_OFFSET * R_SZ
	ld.d	a4, \ctx_sp, A4_OFFSET * R_SZ
	ld.d	a5, \ctx_sp, A5_OFFSET * R_SZ
	ld.d	a6, \ctx_sp, A6_OFFSET * R_SZ
	ld.d	a7, \ctx_sp, A7_OFFSET * R_SZ
	ld.d	tp, \ctx_sp, TP_OFFSET * R_SZ
	ld.d	fp, \ctx_sp, FP_OFFSET * R_SZ
	.endm
	
	.macro	RESTORE_SP    ctx_sp
	ld.d	sp, \ctx_sp, SP_OFFSET * R_SZ
	.endm
	
	.macro	RESTORE_SP_AND_RET   ctx_sp
	RESTORE_SP   \ctx_sp
	ertn
	.endm
	
	.macro	RESTORE_ALL ctx_sp
	RESTORE_TEMP \ctx_sp
	RESTORE_STATIC \ctx_sp
	RESTORE_SOME   \ctx_sp
	RESTORE_SP     \ctx_sp
	.endm

/* Move to kernel mode and disable interrupts. */
	.macro	CLI
	li.w	t0, 0x7
	csrxchg	zero, t0, LOONGARCH_CSR_CRMD
	/*csrrd	x0, PERCPU_BASE_KS*/
	.endm

/* Move to kernel mode and enable interrupts. */
	.macro	STI
	li.w	t0, 0x7
	li.w	t1, (1 << 2)
	csrxchg	t1, t0, LOONGARCH_CSR_CRMD
	/*csrrd	x0, PERCPU_BASE_KS*/
	.endm

/* Just move to kernel mode and leave interrupts as they are. */
	.macro	KMODE
	/*csrrd	x0, PERCPU_BASE_KS*/  /*TODO::*/
	.endm
	
#endif 
