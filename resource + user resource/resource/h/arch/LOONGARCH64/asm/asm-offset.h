#ifndef _ASM_OFFSET_H
#define _ASM_OFFSET_H

#define THREAD_SIZE (16 * 1024)
#define THREAD_MASK (THREAD_SIZE - 1UL)
#define R_SZ 8

/* THR\INT CONTEXT SIZE */
#define THR_INT_CONTEXT_CONTROL_SIZE  344

/* GENERAL REGISTER OFFSET */
#define ZO_OFFSET 1
#define RA_OFFSET 2
#define TP_OFFSET 3
#define SP_OFFSET 4
#define V0_OFFSET 5
#define V1_OFFSET 6
#define A0_OFFSET 7
#define A1_OFFSET 8
#define A2_OFFSET 9
#define A3_OFFSET 10
#define A4_OFFSET 11
#define A5_OFFSET 12
#define A6_OFFSET 13
#define A7_OFFSET 14
#define T0_OFFSET 15
#define T1_OFFSET 16
#define T2_OFFSET 17
#define T3_OFFSET 18
#define T4_OFFSET 19
#define T5_OFFSET 20
#define T6_OFFSET 21
#define T7_OFFSET 22
#define T8_OFFSET 23
#define X0_OFFSET 24
#define FP_OFFSET 25
#define S0_OFFSET 26
#define S1_OFFSET 27
#define S2_OFFSET 28
#define S3_OFFSET 29
#define S4_OFFSET 30
#define S5_OFFSET 31
#define S6_OFFSET 32
#define S7_OFFSET 33
#define S8_OFFSET 34
#define EPC_OFFSET    35
#define BVADDR_OFFSET    36
#define CRMD_OFFSET 37
#define PRMD_OFFSET 38
#define EUEN_OFFSET 39
#define ECFG_OFFSET 40
#define ESTAT_OFFSET  41
#define ORIG_A0_OFFSET   42

/* FPU OFFSET */
#define FV0_OFFSET  0
#define FV1_OFFSET  1
#define FA0_OFFSET  2
#define FA1_OFFSET  3
#define FA2_OFFSET  4
#define FA3_OFFSET  5
#define FA4_OFFSET  6
#define FA5_OFFSET  7
#define FA6_OFFSET  8
#define FA7_OFFSET  9
#define FT0_OFFSET 10
#define FT1_OFFSET 11
#define FT2_OFFSET 12
#define FT3_OFFSET 13
#define FT4_OFFSET 14
#define FT5_OFFSET 15
#define FT6_OFFSET 16
#define FT7_OFFSET 17
#define FT8_OFFSET 18
#define FT9_OFFSET 19
#define FT10_OFFSET 20
#define FT11_OFFSET 21
#define FT12_OFFSET 22
#define FT13_OFFSET 23
#define FS0_OFFSET 24
#define FS1_OFFSET 25
#define FS2_OFFSET 26
#define FS3_OFFSET 27
#define FS4_OFFSET 28
#define FS5_OFFSET 29
#define FS6_OFFSET 30
#define FS7_OFFSET 31
#define FCSR0_OFFSET 32
#define FCSR1_OFFSET 33
#define FCSR2_OFFSET 34
#define FCSR3_OFFSET 35
#define FVCSR16_OFFSET 36

/* FPU CONTEXT SIZE */
#define FPU_CONTEXT_CONTROL_SIZE  288

#define FPU_REG_BASE 0

#endif 
