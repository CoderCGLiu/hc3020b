/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：LOONGARCH汇编语言相关定义的头文件
 * 修改：
 * 		
 */

#ifndef _MIPS_ASM_H_
#define _MIPS_ASM_H_

#ifdef __cplusplus
extern "C"
{
#endif

#define _LOONGARCH_SZPTR 64
/*
 * How to add/sub/load/store/shift C int variables.
 */
#if (_LOONGARCH_SZINT == 32)
#define INT_ADDU	add.w
#define INT_ADDIU	addi.w
#define INT_SUBU	sub.w
#define INT_L		ld.w
#define INT_S		st.w
#define INT_SLL		slli.w
#define INT_SLLV	sll.w
#define INT_SRL		srli.w
#define INT_SRLV	srl.w
#define INT_SRA		srai.w
#define INT_SRAV	sra.w
#endif

#if (_LOONGARCH_SZINT == 64)
#define INT_ADDU	add.d
#define INT_ADDIU	addi.d
#define INT_SUBU	sub.d
#define INT_L		ld.d
#define INT_S		st.d
#define INT_SLL		slli.d
#define INT_SLLV	sll.d
#define INT_SRL		srli.d
#define INT_SRLV	srl.d
#define INT_SRA		sra.w
#define INT_SRAV	sra.d
#endif

/*
 * How to add/sub/load/store/shift C long variables.
 */
#if (_LOONGARCH_SZLONG == 32)
#define LONG_ADDU	add.w
#define LONG_ADDIU	addi.w
#define LONG_SUBU	sub.w
#define LONG_L		ld.w
#define LONG_S		st.w
#define LONG_SP		swp
#define LONG_SLL	slli.w
#define LONG_SLLV	sll.w
#define LONG_SRL	srli.w
#define LONG_SRLV	srl.w
#define LONG_SRA	srai.w
#define LONG_SRAV	sra.w

#ifdef __ASSEMBLY__
#define LONG		.word
#endif
#define LONGSIZE	4
#define LONGMASK	3
#define LONGLOG		2
#endif

#if (_LOONGARCH_SZLONG == 64)
#define LONG_ADDU	add.d
#define LONG_ADDIU	addi.d
#define LONG_SUBU	sub.d
#define LONG_L		ld.d
#define LONG_S		st.d
#define LONG_SP		sdp
#define LONG_SLL	slli.d
#define LONG_SLLV	sll.d
#define LONG_SRL	srli.d
#define LONG_SRLV	srl.d
#define LONG_SRA	sra.w
#define LONG_SRAV	sra.d

#ifdef __ASSEMBLY__
#define LONG		.dword
#endif
#define LONGSIZE	8
#define LONGMASK	7
#define LONGLOG		3
#endif

/*
 * How to add/sub/load/store/shift pointers.
 */
#if (_LOONGARCH_SZPTR == 32)
#define PTR_ADDU	add.w
#define PTR_ADDIU	addi.w
#define PTR_SUBU	sub.w
#define PTR_L		ld.w
#define PTR_S		st.w
#define PTR_LI		li.w
#define PTR_SLL		slli.w
#define PTR_SLLV	sll.w
#define PTR_SRL		srli.w
#define PTR_SRLV	srl.w
#define PTR_SRA		srai.w
#define PTR_SRAV	sra.w

#define PTR_SCALESHIFT	2

#define PTR		.word
#define PTRSIZE		4
#define PTRLOG		2
#endif

#if (_LOONGARCH_SZPTR == 64)
#define PTR_ADDU	add.d
#define PTR_ADDIU	addi.d
#define PTR_SUBU	sub.d
#define PTR_L		ld.d
#define PTR_S		st.d
#define PTR_LI		li.d
#define PTR_SLL		slli.d
#define PTR_SLLV	sll.d
#define PTR_SRL		srli.d
#define PTR_SRLV	srl.d
#define PTR_SRA		srai.d
#define PTR_SRAV	sra.d

#define PTR_SCALESHIFT	3

#define PTR		.dword
#define PTRSIZE		8
#define PTRLOG		3
#endif

/* This info is needed when parsing the symbol table */
#define STB_LOCAL  0
#define STB_GLOBAL 1
#define STB_WEAK   2

#define STT_NOTYPE  0
#define STT_OBJECT  1
#define STT_FUNC    2
#define STT_SECTION 3
#define STT_FILE    4
#define STT_COMMON  5
#define STT_TLS     6

#define TEXT(msg)				\
	.pushsection .data;			\
8:	.asciz msg;				\
	.popsection;
/*
 * Print formatted string
 */
#ifdef CONFIG_PRINTK
#define ASM_PRINT(string)			\
	la	a0, 8f;				\
	bl	printk;				\
	TEXT(string)
#else
#define ASM_PRINT(string)
#endif

/*
*  LOONGARCH register definitions
*/
#include <regdef.h>
#define LEAF(name) \
  	.text; \
  	.globl	name; \
name:

#define XLEAF(name) \
  	.text; \
  	.globl	name; \
  	.aent	name; \
name:

#define WLEAF(name) \
  	.text; \
  	.weakext name; \
  	.ent	name; \
name:

#define SLEAF(name) \
  	.text; \
  	.ent	name; \
name:

#define END(name) \
	.type name 2;	  \
  	.size name,.-name; \

#define SEND(name) END(name)
#define WEND(name) END(name)

/*define .macro func,data start/end */
/* SYM_T_FUNC -- type used by assembler to mark functions */
#ifndef SYM_T_FUNC
#define SYM_T_FUNC				STT_FUNC
#endif

/* SYM_T_OBJECT -- type used by assembler to mark data */
#ifndef SYM_T_OBJECT
#define SYM_T_OBJECT				STT_OBJECT
#endif

/* ASM_NL* -- semicolon */
#ifndef ASM_NL
#define ASM_NL		 ;
#endif

/* SYM_L_* -- linkage of symbols */
#define SYM_L_GLOBAL(name)			.globl name
#define SYM_L_WEAK(name)			.weak name
#define SYM_L_LOCAL(name)			/* nothing */

#define  ALIGN		.align 3
#define __ALIGN		.align 2
#define __ALIGN_STR	".align 2"
/* SYM_A_* -- align the symbol? */
#define SYM_A_ALIGN				__ALIGN
#define SYM_A_NONE				/* nothing */

/* SYM_T_NONE -- type used by assembler to mark entries of unknown type */
#ifndef SYM_T_NONE
#define SYM_T_NONE				STT_NOTYPE
#endif

/* SYM_ENTRY -- use only if you have to for non-paired symbols */
#ifndef SYM_ENTRY
#define SYM_ENTRY(name, linkage, align...)		\
	.text;                               \
	linkage(name) ASM_NL				\
	ALIGN ASM_NL					\
	name:
#endif

/* SYM_START -- use only if you have to */
#ifndef SYM_START
#define SYM_START(name, linkage, align...)		\
	SYM_ENTRY(name, linkage, align)
#endif
/* SYM_END -- use only if you have to */
#ifndef SYM_END
#define SYM_END(name, sym_type)				\
	.type name sym_type ASM_NL			\
	.size name, .-name
#endif

#define SYM_FUNC_START(name)				\
	SYM_START(name, SYM_L_GLOBAL, SYM_A_ALIGN)	\
	.cfi_startproc;                              \

#define SYM_FUNC_END(name)				\
	.cfi_endproc;                       \
	SYM_END(name, SYM_T_FUNC)

/* SYM_DATA_START -- global data symbol */
#ifndef SYM_DATA_START
#define SYM_DATA_START(name)				\
	SYM_START(name, SYM_L_GLOBAL, SYM_A_NONE)
#endif

/* SYM_DATA_END -- the end of SYM_DATA_START symbol */
#ifndef SYM_DATA_END
#define SYM_DATA_END(name)				\
	SYM_END(name, SYM_T_OBJECT)
#endif

/* SYM_INNER_LABEL -- only for labels in the middle of code */
#ifndef SYM_INNER_LABEL
#define SYM_INNER_LABEL(name, linkage)		\
	.type name SYM_T_NONE ASM_NL			\
	SYM_ENTRY(name, linkage, SYM_A_NONE)
#endif

/* SYM_CODE_START -- use for non-C (special) functions */
#ifndef SYM_CODE_START
#define SYM_CODE_START(name)				\
	SYM_START(name, SYM_L_GLOBAL, SYM_A_ALIGN)
#endif

/* SYM_CODE_END -- the end of SYM_CODE_START_LOCAL, SYM_CODE_START, ... */
#ifndef SYM_CODE_END
#define SYM_CODE_END(name)				\
	SYM_END(name, SYM_T_NONE)      
#endif

#ifdef __cplusplus
}
#endif
#endif /* LOONGARCH_ASM */
