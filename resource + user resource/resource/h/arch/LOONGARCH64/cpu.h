/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了CSP模块中loongson2f处理器定义
 * 修改：
 * 		 1. 2012-5-17，规范CSP 
 */

#ifndef _REWORKS_CPU_H_
#define _REWORKS_CPU_H_

#ifdef __cplusplus
extern "C" 
{
#endif

#define MAX_SMP_CPUS 1
	
/* 上下文类型 */
#define SWITCH_TYPE		0xa5a50101
#define INTERRUPT_TYPE	0x10105a5a


#ifndef ASM_LANGUAGE

#include <reworks/types.h>
#include <machine/endian.h>

#define CPU_STACK_MIN_SIZE          4096
#define CPU_ALIGNMENT_4KB           0x1000
#define CPU_ALIGNMENT               16
#define CACHE_ALIGNMENT             32

#ifdef  _LITTLE_ENDIAN	
#undef  _LITTLE_ENDIAN
#endif

#ifdef  _BIG_ENDIAN
#undef  _BIG_ENDIAN
#endif

#define _LITTLE_ENDIAN			LITTLE_ENDIAN
#define _BIG_ENDIAN				BIG_ENDIAN
#define _BYTE_ORDER				_LITTLE_ENDIAN

#ifdef BYTE_ORDER
#undef BYTE_ORDER
#define BYTE_ORDER	_BYTE_ORDER
#endif

typedef struct 
{
  void       (*idle_task)( void );
  boolean      do_zero_of_workspace;
  u32   idle_task_stack_size;
  u32   interrupt_stack_size;
}   rtos_cpu_table;

typedef struct {
	u32 type;
    u32 reserved;
    u64 lzero;
	u64 lra;
	u64 ltp;
	u64 lsp;
	u64 lv0;
	u64 lv1;
	u64 la0;
	u64 la1;
	u64 la2;
	u64 la3;
	u64 la4;
	u64 la5;
	u64 la6;
	u64 la7;	
    u64 lt0;
    u64 lt1;
    u64 lt2;
    u64 lt3;
    u64 lt4;
    u64 lt5;
    u64 lt6;
    u64 lt7;
    u64 lt8;
    u64 lx0;   
    u64 lfp;   
    u64 ls0;
    u64 ls1;
    u64 ls2;
    u64 ls3;
    u64 ls4;
    u64 ls5;
    u64 ls6;
    u64 ls7;
    u64 ls8;
	u64 pc;
	u64 csr_badvaddr;
	u64 csr_crmd;
	u64 csr_prmd;
	u64 csr_euen;
	u64 csr_ecfg;
	u64 csr_estat;
	u64 orig_a0;
} Context_Ctrl;

typedef Context_Ctrl REG_SET;

#define HAVE_FP_CONTEXT

typedef u64 fpureg_t;
typedef u32 fpuctrlreg_t;

typedef struct _Fp_Context{
	fpureg_t fp0;
	fpureg_t fp1;
	fpureg_t fp2;
	fpureg_t fp3;
	fpureg_t fp4;
	fpureg_t fp5;
	fpureg_t fp6;
	fpureg_t fp7;
	fpureg_t fp8;
	fpureg_t fp9;
	fpureg_t fp10;
	fpureg_t fp11;
	fpureg_t fp12;
	fpureg_t fp13;
	fpureg_t fp14;
	fpureg_t fp15;
	fpureg_t fp16;
	fpureg_t fp17;
	fpureg_t fp18;
	fpureg_t fp19;
	fpureg_t fp20;
	fpureg_t fp21;
	fpureg_t fp22;
	fpureg_t fp23;
	fpureg_t fp24;
	fpureg_t fp25;
	fpureg_t fp26;
	fpureg_t fp27;
	fpureg_t fp28;
	fpureg_t fp29;
	fpureg_t fp30;
	fpureg_t fp31;
	fpuctrlreg_t fpcs;
} FP_CONTEXT;

typedef FP_CONTEXT Fp_Context;

/* 异常时的上下文 */  
typedef struct Exc_Regs
{
	u32 reg_val_cause;
	u64 reg_val_badaddr;
	Context_Ctrl reg_context_gp;
	Fp_Context reg_context_fp;
} Exc_Regs;

int cpu_id_get();
void context_init(Context_Ctrl *the_context, u32 isr, void *entry_point, void *prev_entry);
void context_switch(Context_Ctrl **run, Context_Ctrl *heir);
void context_restore(Context_Ctrl *heir);
/**
 * 将TCB中异常发生时的上下文拷贝给用户
 * @param thread_ptr 待获取上下文的任务
 * @param p_reg_val 用户层传入的Exc_Regs类型指针，用于保存异常发生时的上下文。
 */
void exc_context_get(void *thread_ptr, Exc_Regs *p_reg_val);

/**
 * 设置需要跳过的异常。
 * @param exc_nu 异常类型编号
 * @param is_skip 打开或关闭跳过，1代表打开，0代表关闭
 */
void set_skip_exc(int exc_nu, int is_skip);

/**
 * 设置是否打印异常信息。
 * @param b_p	0：不打印。
 * 				1: 打印。				
 */
void exc_print(int b_p);

int num_online_cpus(void);

#define __swap16gen(x) ({						\
	u16 __swap16gen_x = (x);					\
									\
	(u16)((__swap16gen_x & 0xff) << 8 |			\
	    (__swap16gen_x & 0xff00) >> 8);				\
})

#define __swap32gen(x) ({						\
	u32 __swap32gen_x = (x);					\
									\
	(u32)((__swap32gen_x & 0xff) << 24 |			\
	    (__swap32gen_x & 0xff00) << 8 |				\
	    (__swap32gen_x & 0xff0000) >> 8 |				\
	    (__swap32gen_x & 0xff000000) >> 24);			\
})

#define CPU_swap_u16( _value )  __swap16gen(_value)
#define CPU_swap_u32( _value )  __swap32gen(_value)

/* 这是一个叶子函数，它用于设置状态寄存器的值。*/
extern void set_csr_sr(u32);

/* 这是一个叶子函数，它用于取得计数寄存器的值。*/
extern u32 get_csr_count();

/**
 * 获取处理器时间戳接口
 */
extern u64 sys_timestamp (void);

/**
 * 获取处理器频率的接口
 */
extern u32 sys_timestamp_freq(void);

#define MEM_BARRIER_R() __asm volatile ("dbar 0" ::: "memory")
#define MEM_BARRIER_W() __asm volatile ("dbar 0" ::: "memory")
#define MEM_BARRIER_RW() __asm volatile ("dbar 0" ::: "memory")

extern SYSTEM_BASIC_INFO *sys_info_get(void);

#endif /* #ifndef ASM_LANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* _REWORKS_CPU_H_ */
