#ifndef _REWORKS_BITOPS_H_
#define _REWORKS_BITOPS_H_

#ifdef __cplusplus
extern "C" 
{
#endif

/********************************************************************
 * huangyuan20080414: 
 * platform-independent code added for threads-scheduling.. etc.
 */
#include <cpu.h>
#include <reworks/types.h>

extern u8 ffs_lsb_tbl[];

#define ffs16( _value, _output ) \
{ \
    register u32 __value = (u32) (_value); \
    register const unsigned char *__p = ffs_lsb_tbl; \
    \
    if ( __value & 0xff ) \
      (_output) = __p[ __value & 0xff]; \
    else \
      (_output) = __p[ __value >> 8 ] + 8; \
}

#define __LL		"ll.d	"
#define __SC		"sc.d	"
#define __AMADD		"amadd.d	"
#define __AMAND_SYNC	"amand_db.d	"
#define __AMOR_SYNC	"amor_db.d	"
#define __AMXOR_SYNC	"amxor_db.d	"

#define BITS_PER_LONG		(64)
#define BIT_WORD(nr)		((nr) / BITS_PER_LONG)

/*
 * set_bit - Atomically set a bit in memory
 * @nr: the bit to set
 * @addr: the address to start counting from
 *
 * This function is atomic and may not be reordered.  See __set_bit()
 * if you do not require the atomic guarantees.
 * Note that @nr may be almost arbitrarily large; this function is not
 * restricted to acting on a single-word quantity.
 */
static inline void set_bit(unsigned long nr, volatile unsigned long *addr)
{
	int bit = nr % BITS_PER_LONG;
	volatile unsigned long *m = &addr[BIT_WORD(nr)];

	__asm__ __volatile__(
	"   " __AMOR_SYNC "$zero, %1, %0        \n"
	: "+ZB" (*m)
	: "r" (1UL << bit)
	: "memory");
}

/*
 * clear_bit - Clears a bit in memory
 * @nr: Bit to clear
 * @addr: Address to start counting from
 *
 * clear_bit() is atomic and may not be reordered.  However, it does
 * not contain a memory barrier, so if it is used for locking purposes,
 * you should call smp_mb__before_atomic() and/or smp_mb__after_atomic()
 * in order to ensure changes are visible on other processors.
 */
static inline void clear_bit(unsigned long nr, volatile unsigned long *addr)
{
	int bit = nr % BITS_PER_LONG;
	volatile unsigned long *m = &addr[BIT_WORD(nr)];

	__asm__ __volatile__(
	"   " __AMAND_SYNC "$zero, %1, %0       \n"
	: "+ZB" (*m)
	: "r" (~(1UL << bit))
	: "memory");
}

/*
 * clear_bit_unlock - Clears a bit in memory
 * @nr: Bit to clear
 * @addr: Address to start counting from
 *
 * clear_bit() is atomic and implies release semantics before the memory
 * operation. It can be used for an unlock.
 */
static inline void clear_bit_unlock(unsigned long nr, volatile unsigned long *addr)
{
	clear_bit(nr, addr);
}

/*
 * change_bit - Toggle a bit in memory
 * @nr: Bit to change
 * @addr: Address to start counting from
 *
 * change_bit() is atomic and may not be reordered.
 * Note that @nr may be almost arbitrarily large; this function is not
 * restricted to acting on a single-word quantity.
 */
static inline void change_bit(unsigned long nr, volatile unsigned long *addr)
{
	int bit = nr % BITS_PER_LONG;
	volatile unsigned long *m = &addr[BIT_WORD(nr)];

	__asm__ __volatile__(
	"   " __AMXOR_SYNC "$zero, %1, %0       \n"
	: "+ZB" (*m)
	: "r" (1UL << bit)
	: "memory");
}
/**
 * test_bit - Determine whether a bit is set
 * @nr: bit number to test
 * @addr: Address to start counting from
 */
static inline int test_bit(unsigned long nr, const volatile unsigned long *addr)
{
	return 1UL & (addr[BIT_WORD(nr)] >> (nr & (BITS_PER_LONG-1)));
}
/*
 * test_and_set_bit - Set a bit and return its old value
 * @nr: Bit to set
 * @addr: Address to count from
 *
 * This operation is atomic and cannot be reordered.
 * It also implies a memory barrier.
 */
static inline int test_and_set_bit(unsigned long nr,
	volatile unsigned long *addr)
{
	int bit = nr % BITS_PER_LONG;
	unsigned long res;
	volatile unsigned long *m = &addr[BIT_WORD(nr)];

	__asm__ __volatile__(
	"   " __AMOR_SYNC "%1, %2, %0       \n"
	: "+ZB" (*m), "=&r" (res)
	: "r" (1UL << bit)
	: "memory");

	res = res & (1UL << bit);

	return res != 0;
}

/*
 * test_and_change_bit - Set a bit and return its old value
 * @nr: Bit to set
 * @addr: Address to count from
 *
 * This operation is atomic and cannot be reordered.
 * It also implies a memory barrier.
 */
static inline int test_and_change_bit(unsigned long nr,
	volatile unsigned long *addr)
{
	int bit = nr % BITS_PER_LONG;
	unsigned long res;
	volatile unsigned long *m = &addr[BIT_WORD(nr)];

	__asm__ __volatile__(
	"   " __AMXOR_SYNC "%1, %2, %0       \n"
	: "+ZB" (*m), "=&r" (res)
	: "r" (1UL << bit)
	: "memory");

	res = res & (1UL << bit);

	return res != 0;
}
/*
 * test_and_set_bit_lock - Set a bit and return its old value
 * @nr: Bit to set
 * @addr: Address to count from
 *
 * This operation is atomic and implies acquire ordering semantics
 * after the memory operation.
 */
static inline int test_and_set_bit_lock(unsigned long nr,
	volatile unsigned long *addr)
{
	int bit = nr % BITS_PER_LONG;
	unsigned long res;
	volatile unsigned long *m = &addr[BIT_WORD(nr)];

	__asm__ __volatile__(
	"   " __AMOR_SYNC "%1, %2, %0       \n"
	: "+ZB" (*m), "=&r" (res)
	: "r" (1UL << bit)
	: "memory");

	res = res & (1UL << bit);

	return res != 0;
}
/*
 * test_and_clear_bit - Clear a bit and return its old value
 * @nr: Bit to clear
 * @addr: Address to count from
 *
 * This operation is atomic and cannot be reordered.
 * It also implies a memory barrier.
 */
static inline int test_and_clear_bit(unsigned long nr,
	volatile unsigned long *addr)
{
	int bit = nr % BITS_PER_LONG;
	unsigned long res, temp;
	volatile unsigned long *m = &addr[BIT_WORD(nr)];

	__asm__ __volatile__(
	"   " __AMAND_SYNC "%1, %2, %0      \n"
	: "+ZB" (*m), "=&r" (temp)
	: "r" (~(1UL << bit))
	: "memory");

	res = temp & (1UL << bit);

	return res != 0;
}
#ifdef __cplusplus
}
#endif
#endif /* _REWORKS_BITOPS_H */
