/*-------------------------------------------------------------------------+
|
| 版权：中国电子科技集团公司第三十二研究所
| 描述：硬件目标板的二级缓存相关定义的头文件 
|
+--------------------------------------------------------------------------*/

#ifndef _MIPS_CACHE_OP_H_
#define _MIPS_CACHE_OP_H_

#ifdef LOONGSON3A1000
#define _CACHE_ALIGN_SIZE (32)
#else /*2K1000 3A3000*/
#define _CACHE_ALIGN_SIZE (64)
#endif

/*
 * 龙芯CACHE 操作
 */
#define	CACHE_INVALIDATE_IC             	    0x00         /* 0       0 */
#define	CACHE_WRITEBACK_INVALIDATE_DC           0x01         /* 0       1 */
#define	CACHE_HIT_INVALIDATE_IC                 0x10        /* 4       0 */
#define	CACHE_HIT_INVALIDATE_DC                 0x11        /* 4       1 */
#define	CACHE_HIT_WRITEBACK_INVALIDATE_DC       0x15        /* 5       1 */
#define	CACHE_WRITEBACK_INVALIDATE_SC           0x03         /* 0       3 */
#define	CACHE_HIT_INVALIDATE_SC                 0x13        /* 4       3 */
#define	CACHE_HIT_WRITEBACK_INVALIDATE_SC       0x17        /* 5       3 */
#define CACHE_STORE_SC               			0x0B         /* 2       3 */

#endif
