#ifndef _GCC_HCW_H
#define _GCC_HCW_H

#if !defined(__mips_hcw_vector_rev)
# error "You must select -march=hcw or -march=hcw to use hcw.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

typedef uint8_t uint8x32_t __attribute__((vector_size (32)));
typedef int8_t int8x32_t __attribute__((vector_size (32)));
typedef uint16_t uint16x16_t __attribute__((vector_size (32)));
typedef int16_t int16x16_t __attribute__((vector_size (32)));
typedef uint32_t uint32x8_t __attribute__((vector_size (32)));
typedef int32_t int32x8_t __attribute__((vector_size (32)));
typedef uint64_t uint64x4_t __attribute__((vector_size (32)));
typedef int64_t int64x4_t __attribute__((vector_size (32)));
typedef uint64_t uint64x2_t __attribute__((vector_size (16)));
typedef int64_t int64x2_t __attribute__((vector_size (16)));
//typedef (unsigned __int128_t) uint128x2_t __attribute__((vector_size (32)));
//typedef __int128_t int128x2_t __attribute__((vector_size (32)));

typedef uint8_t uint8x16_t __attribute__((vector_size (16)));
typedef int8_t int8x16_t __attribute__((vector_size (16)));
typedef uint8_t uint8x8_t __attribute__((vector_size (8)));
typedef int8_t int8x8_t __attribute__((vector_size (8)));
typedef uint8_t uint8x4_t __attribute__((vector_size (4)));
typedef int8_t int8x4_t __attribute__((vector_size (4)));
typedef uint16_t uint16x8_t __attribute__((vector_size (16)));
typedef int16_t int16x8_t __attribute__((vector_size (16)));
typedef uint16_t uint16x4_t __attribute__((vector_size (8)));
typedef int16_t int16x4_t __attribute__((vector_size (8)));
typedef uint32_t uint32x4_t __attribute__((vector_size (16)));
typedef int32_t int32x4_t __attribute__((vector_size (16)));

typedef float floatx1_t __attribute__((vector_size (4)));
typedef float floatx4_t __attribute__((vector_size (16)));
typedef float floatx8_t __attribute__((vector_size (32)));
typedef double doublex1_t __attribute__((vector_size (8)));
typedef double doublex4_t __attribute__((vector_size (32)));

typedef float __m256f __attribute__ ((__vector_size__ (32),
				     __may_alias__));
typedef long long __m256i __attribute__ ((__vector_size__ (32),
					  __may_alias__));
typedef double __m256d __attribute__ ((__vector_size__ (32),
				       __may_alias__));

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpaddb_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vpaddb_u (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpaddh_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vpaddh_u (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpaddw_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vpaddw_u (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpaddd_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vpaddd_u (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpaddb_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vpaddb_s (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpaddh_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vpaddh_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpaddw_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vpaddw_s (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpaddd_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vpaddd_s (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpsubb_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vpsubb_u (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpsubh_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vpsubh_u (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpsubw_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vpsubw_u (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpsubd_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vpsubd_u (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpsubb_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vpsubb_s (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpsubh_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vpsubh_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpsubw_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vpsubw_s (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpsubd_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vpsubd_s (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vphaddb_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vphaddb_u (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vphaddh_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vphaddh_u (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vphaddw_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vphaddw_u (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vphaddd_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vphaddd_u (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vphaddb_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vphaddb_s (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vphaddh_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vphaddh_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vphaddw_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vphaddw_s (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vphaddd_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vphaddd_s (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vphsubb_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vphsubb_u (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vphsubh_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vphsubh_u (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vphsubw_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vphsubw_u (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vphsubd_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vphsubd_u (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vphsubb_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vphsubb_s (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vphsubh_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vphsubh_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vphsubw_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vphsubw_s (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vphsubd_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vphsubd_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpsums2h1ws (int16x16_t s, int32x8_t t)
{
  return __builtin_hcw_vpsums2h1ws (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpsums4b1ws (int8x32_t s, int32x8_t t)
{
  return __builtin_hcw_vpsums4b1ws (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpsumu2h1ws (uint16x16_t s, uint32x8_t t)
{
  return __builtin_hcw_vpsumu2h1ws (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpsumu4b1ws (uint8x32_t s, uint32x8_t t)
{
  return __builtin_hcw_vpsumu4b1ws (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpaddbs (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vpaddbs (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpaddhs (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vpaddhs (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpaddws (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vpaddws (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpaddds (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vpaddds (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vphaddbs (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vphaddbs (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vphaddhs (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vphaddhs (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vphaddws (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vphaddws (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vphaddds (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vphaddds (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpsubbs (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vpsubbs (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpsubhs (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vpsubhs (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpsubws (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vpsubws (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpsubds (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vpsubds (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vphsubbs (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vphsubbs (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vphsubhs (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vphsubhs (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vphsubws (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vphsubws (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vphsubds (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vphsubds (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpaddubs (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vpaddubs (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpadduhs (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vpadduhs (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpadduws (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vpadduws (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpadduds (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vpadduds (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vphaddubs (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vphaddubs (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vphadduhs (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vphadduhs (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vphadduws (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vphadduws (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vphadduds (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vphadduds (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpsububs (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vpsububs (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpsubuhs (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vpsubuhs (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpsubuws (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vpsubuws (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpsubuds (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vpsubuds (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vphsububs (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vphsububs (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vphsubuhs (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vphsubuhs (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vphsubuws (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vphsubuws (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vphsubuds (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vphsubuds (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpminub (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vpminub (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpminuh (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vpminuh (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpminuw (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vpminuw (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpminud (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vpminud (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpcmpgtsb (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vpcmpgtsb (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpcmpgtsh (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vpcmpgtsh (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpcmpgtsw (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vpcmpgtsw (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpcmpgtsd (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vpcmpgtsd (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpcmpgtub (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vpcmpgtub (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpcmpgtuh (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vpcmpgtuh (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpcmpgtuw (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vpcmpgtuw (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpcmpgtud (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vpcmpgtud (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpcmpltsb (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vpcmpltsb (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpcmpltsh (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vpcmpltsh (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpcmpltsw (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vpcmpltsw (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpcmpltsd (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vpcmpltsd (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpcmpltub (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vpcmpltub (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpcmpltuh (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vpcmpltuh (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpcmpltuw (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vpcmpltuw (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpcmpltud (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vpcmpltud (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpcmpgtssb (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vpcmpgtssb (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpcmpgtssh (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vpcmpgtssh (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpcmpgtssw (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vpcmpgtssw (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpcmpgtssd (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vpcmpgtssd (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpcmpgtsub (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vpcmpgtsub (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpcmpgtsuh (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vpcmpgtsuh (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpcmpgtsuw (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vpcmpgtsuw (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpcmpgtsud (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vpcmpgtsud (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpcmpltssb (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vpcmpltssb (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpcmpltssh (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vpcmpltssh (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpcmpltssw (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vpcmpltssw (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpcmpltssd (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vpcmpltssd (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpcmpltsub (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vpcmpltsub (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpcmpltsuh (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vpcmpltsuh (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpcmpltsuw (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vpcmpltsuw (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpcmpltsud (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vpcmpltsud (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpminsb (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vpminsb (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpminsh (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vpminsh (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpminsw (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vpminsw (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpminsd (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vpminsd (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpmaxub (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vpmaxub (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpmaxuh (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vpmaxuh (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpmaxuw (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vpmaxuw (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpmaxud (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vpmaxud (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpcmpeqb_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vpcmpeqb_u (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpcmpeqh_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vpcmpeqh_u (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpcmpeqw_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vpcmpeqw_u (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpcmpeqd_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vpcmpeqd_u (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpcmpeqb_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vpcmpeqb_s (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpcmpeqh_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vpcmpeqh_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpcmpeqw_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vpcmpeqw_s (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpcmpeqd_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vpcmpeqd_s (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpcmpeqsb_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vpcmpeqsb_u (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpcmpeqsh_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vpcmpeqsh_u (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpcmpeqsw_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vpcmpeqsw_u (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpcmpeqsd_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vpcmpeqsd_u (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpcmpeqsb_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vpcmpeqsb_s (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpcmpeqsh_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vpcmpeqsh_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpcmpeqsw_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vpcmpeqsw_s (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpcmpeqsd_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vpcmpeqsd_s (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpmaxsb (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vpmaxsb (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpmaxsh (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vpmaxsh (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpmaxsw (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vpmaxsw (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpmaxsd (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vpmaxsd (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpsignb (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vpsignb (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpsignh (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vpsignh (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpsignw (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vpsignw (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpsignd (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vpsignd (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpabsb (int8x32_t s)
{
  return __builtin_hcw_vpabsb (s);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpabsh (int16x16_t s)
{
  return __builtin_hcw_vpabsh (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpabsw (int32x8_t s)
{
  return __builtin_hcw_vpabsw (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpabsd (int64x4_t s)
{
  return __builtin_hcw_vpabsd (s);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpavgb (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vpavgb (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpavgh (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vpavgh (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpavgw (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vpavgw (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpavgd (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vpavgd (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpsadbh (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vpsadbh (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpsadhw (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vpsadhw (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpmullb (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vpmullb (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpmullh (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vpmullh (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpmullw (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vpmullw (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpmulhb (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vpmulhb (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpmulhh (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vpmulhh (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpmulhw (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vpmulhw (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpmullub (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vpmullub (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpmulluh (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vpmulluh (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpmulluw (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vpmulluw (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpmulhub (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vpmulhub (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpmulhuh (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vpmulhuh (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpmulhuw (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vpmulhuw (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpmulhxl (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vpmulhxl (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpmulwxl (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vpmulwxl (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpmuldxl (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vpmuldxl (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpmuluhxl (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vpmuluhxl (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpmuluwxl (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vpmuluwxl (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpmuludxl (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vpmuludxl (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpmulhxh (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vpmulhxh (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpmulwxh (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vpmulwxh (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpmuldxn (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vpmuldxh (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpmuluhxh (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vpmuluhxh (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpmuluwxh (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vpmuluwxh (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpmuludxh (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vpmuludxh (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpmaddbhs (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vpmaddbhs (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpmaddhws (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vpmaddhws (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpmaddwds (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vpmaddwds (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpmaddubshs (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vpmaddubshs (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpmadduhsws (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vpmadduhsws (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpmadduwsds (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vpmadduwsds (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpmulhrsh (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vpmulhrsh (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vaddps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vaddps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vaddpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vaddpd (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vsubps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vsubps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vsubpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vsubpd (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vmulps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vmulps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vmulpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vmulpd (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vmuladdps (floatx8_t s, floatx8_t t, floatx8_t r)
{
  return __builtin_hcw_vmuladdps (s, t, r);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vmuladdpd (doublex4_t s, doublex4_t t, doublex4_t r)
{
  return __builtin_hcw_vmuladdpd (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vmulsubps (floatx8_t s, floatx8_t t, floatx8_t r)
{
  return __builtin_hcw_vmulsubps (s, t, r);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vmulsubpd (doublex4_t s, doublex4_t t, doublex4_t r)
{
  return __builtin_hcw_vmulsubpd (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vnmuladdps (floatx8_t s, floatx8_t t, floatx8_t r)
{
  return __builtin_hcw_vnmuladdps (s, t, r);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vnmuladdpd (doublex4_t s, doublex4_t t, doublex4_t r)
{
  return __builtin_hcw_vnmuladdpd (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vnmulsubps (floatx8_t s, floatx8_t t, floatx8_t r)
{
  return __builtin_hcw_vnmulsubps (s, t, r);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vnmulsubpd (doublex4_t s, doublex4_t t, doublex4_t r)
{
  return __builtin_hcw_vnmulsubpd (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vhaddps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vhaddps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vhaddpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vhaddpd (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vhsubps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vhsubps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vhsubpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vhsubpd (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vaddsubps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vaddsubps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vaddsubpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vaddsubpd (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vsubaddps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vsubaddps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vsubaddpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vsubaddpd (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vmaddsubps (floatx8_t s, floatx8_t t, floatx8_t r)
{
  return __builtin_hcw_vmaddsubps (s, t, r);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vmaddsubpd (doublex4_t s, doublex4_t t, doublex4_t r)
{
  return __builtin_hcw_vmaddsubpd (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vmsubaddps (floatx8_t s, floatx8_t t, floatx8_t r)
{
  return __builtin_hcw_vmsubaddps (s, t, r);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vmsubaddpd (doublex4_t s, doublex4_t t, doublex4_t r)
{
  return __builtin_hcw_vmsubaddpd (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vminps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vminps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vminpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vminpd (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vmaxps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vmaxps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vmaxpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vmaxpd (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vnegps (floatx8_t s)
{
  return __builtin_hcw_vnegps (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vnegpd (doublex4_t s)
{
  return __builtin_hcw_vnegpd (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vabsps (floatx8_t s)
{
  return __builtin_hcw_vabsps (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vabspd (doublex4_t s)
{
  return __builtin_hcw_vabspd (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vdivps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vdivps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vdivpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vdivpd (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vrcpps (floatx8_t s)
{
  return __builtin_hcw_vrcpps (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vrcppd (doublex4_t s)
{
  return __builtin_hcw_vrcppd (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vsqrtps (floatx8_t s)
{
  return __builtin_hcw_vsqrtps (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vsqrtpd (doublex4_t s)
{
  return __builtin_hcw_vsqrtpd (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vrsqrtps (floatx8_t s)
{
  return __builtin_hcw_vrsqrtps (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vrsqrtpd (doublex4_t s)
{
  return __builtin_hcw_vrsqrtpd (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vsinps (floatx8_t s)
{
  return __builtin_hcw_vsinps (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcosps (floatx8_t s)
{
  return __builtin_hcw_vcosps (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vatanps (floatx8_t s)
{
  return __builtin_hcw_vatanps (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vpolarlps (floatx8_t s)
{
  return __builtin_hcw_vpolarlps (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vpolarhps (floatx8_t s)
{
  return __builtin_hcw_vpolarhps (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vexp2ps (floatx8_t s)
{
  return __builtin_hcw_vexp2ps (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vlog2ps (floatx8_t s)
{
  return __builtin_hcw_vlog2ps (s);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpandb_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vpandb_u (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpandh_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vpandh_u (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpandw_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vpandw_u (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpandd_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vpandd_u (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpandb_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vpandb_s (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpandh_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vpandh_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpandw_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vpandw_s (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpandd_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vpandd_s (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vpandps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vpandps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vpandpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vpandpd (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vporb_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vporb_u (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vporh_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vporh_u (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vporw_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vporw_u (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpord_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vpord_u (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vporb_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vporb_s (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vporh_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vporh_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vporw_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vporw_s (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpord_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vpord_s (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vporps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vporps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vporpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vporpd (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpxorb_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vpxorb_u (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpxorh_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vpxorh_u (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpxorw_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vpxorw_u (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpxord_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vpxord_u (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpxorb_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vpxorb_s (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpxorh_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vpxorh_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpxorw_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vpxorw_s (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpxord_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vpxord_s (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vpxorps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vpxorps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vpxorpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vpxorpd (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpandnb_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vpandnb_u (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpandnh_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vpandnh_u (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpandnw_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vpandnw_u (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpandnd_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vpandnd_u (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpandnb_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vpandnb_s (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpandnh_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vpandnh_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpandnw_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vpandnw_s (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpandnd_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vpandnd_s (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vpandnps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vpandnps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vpandnpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vpandnpd (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpnorb_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vpnorb_u (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpnorh_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vpnorh_u (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpnorw_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vpnorw_u (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpnord_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vpnord_u (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpnorb_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vpnorb_s (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpnorh_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vpnorh_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpnorw_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vpnorw_s (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpnord_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vpnord_s (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vpnorps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vpnorps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vpnorpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vpnorpd (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpopcntb_u (uint8x32_t s)
{
  return __builtin_hcw_vpopcntb_u (s);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpopcnth_u (uint16x16_t s)
{
  return __builtin_hcw_vpopcnth_u (s);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpopcntw_u (uint32x8_t s)
{
  return __builtin_hcw_vpopcntw_u (s);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpopcntd_u (uint64x4_t s)
{
  return __builtin_hcw_vpopcntd_u (s);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpopcntb_s (int8x32_t s)
{
  return __builtin_hcw_vpopcntb_s (s);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpopcnth_s (int16x16_t s)
{
  return __builtin_hcw_vpopcnth_s (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpopcntw_s (int32x8_t s)
{
  return __builtin_hcw_vpopcntw_s (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpopcntd_s (int64x4_t s)
{
  return __builtin_hcw_vpopcntd_s (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vpopcntps (floatx8_t s)
{
  return __builtin_hcw_vpopcntps (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vpopcntpd (doublex4_t s)
{
  return __builtin_hcw_vpopcntpd (s);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vzerob_u (void)
{
  return __builtin_hcw_vzerob_u (0);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vzeroh_u (void)
{
  return __builtin_hcw_vzeroh_u (0);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vzerow_u (void)
{
  return __builtin_hcw_vzerow_u (0);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vzerod_u (void)
{
  return __builtin_hcw_vzerod_u (0);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vzerob_s (void)
{
  return __builtin_hcw_vzerob_s (0);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vzeroh_s (void)
{
  return __builtin_hcw_vzeroh_s (0);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vzerow_s (void)
{
  return __builtin_hcw_vzerow_s (0);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vzerod_s (void)
{
  return __builtin_hcw_vzerod_s (0);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vzerops (void)
{
  return __builtin_hcw_vzerops (0);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vzeropd (void)
{
  return __builtin_hcw_vzeropd (0);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vmovb_u (uint8x32_t s)
{
  return __builtin_hcw_vmovb_u (s);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vmovh_u (uint16x16_t s)
{
  return __builtin_hcw_vmovh_u (s);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vmovw_u (uint32x8_t s)
{
  return __builtin_hcw_vmovw_u (s);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vmovd_u (uint64x4_t s)
{
  return __builtin_hcw_vmovd_u (s);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vmovb_s (int8x32_t s)
{
  return __builtin_hcw_vmovb_s (s);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vmovh_s (int16x16_t s)
{
  return __builtin_hcw_vmovh_s (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vmovw_s (int32x8_t s)
{
  return __builtin_hcw_vmovw_s (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vmovd_s (int64x4_t s)
{
  return __builtin_hcw_vmovd_s (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vmovps (floatx8_t s)
{
  return __builtin_hcw_vmovps (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vmovpd (doublex4_t s)
{
  return __builtin_hcw_vmovpd (s);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vmovsldup_u (uint32x8_t s)
{
  return __builtin_hcw_vmovsldup_u (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vmovsldup_s (int32x8_t s)
{
  return __builtin_hcw_vmovsldup_s (s);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vmovshdup_u (uint32x8_t s)
{
  return __builtin_hcw_vmovshdup_u (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vmovshdup_s (int32x8_t s)
{
  return __builtin_hcw_vmovshdup_s (s);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vmovdldup_u (uint64x4_t s)
{
  return __builtin_hcw_vmovdldup_u (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vmovdldup_s (int64x4_t s)
{
  return __builtin_hcw_vmovdldup_s (s);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vmovdhdup_u (uint64x4_t s)
{
  return __builtin_hcw_vmovdhdup_u (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vmovdhdup_s (int64x4_t s)
{
  return __builtin_hcw_vmovdhdup_s (s);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpunpcklbh_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vpunpcklbh_u (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpunpcklbh_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vpunpcklbh_s (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpunpcklhw_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vpunpcklhw_u (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpunpcklhw_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vpunpcklhw_s (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpunpcklwd_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vpunpcklwd_u (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpunpcklwd_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vpunpcklwd_s (s, t);
}

/*__extension__ static __inline uint128x2_t __attribute__ ((__always_inline__))
    vpunpckldq_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vpunpckldq_u (s, t);
}*/

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpunpckldq_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vpunpckldq_u (s, t);
}

/*__extension__ static __inline int128x2_t __attribute__ ((__always_inline__))
    vpunpckldq_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vpunpckldq_s (s, t);
}*/

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpunpckldq_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vpunpckldq_s (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpunpckhbh_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vpunpckhbh_u (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpunpckhbh_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vpunpckhbh_s (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpunpckhhw_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vpunpckhhw_u (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpunpckhhw_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vpunpckhhw_s (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpunpckhwd_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vpunpckhwd_u (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpunpckhwd_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vpunpckhwd_s (s, t);
}

/*__extension__ static __inline uint128x2_t __attribute__ ((__always_inline__))
    vpunpckhdq_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vpunpckhdq_u (s, t);
}*/

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpunpckhdq_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vpunpckhdq_u (s, t);
}

/*__extension__ static __inline int128x2_t __attribute__ ((__always_inline__))
    vpunpckhdq_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vpunpckhdq_s (s, t);
}*/

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpunpckhdq_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vpunpckhdq_s (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpackshsbs (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vpackshsbs (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpackswshs (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vpackswshs (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpacksdsws (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vpacksdsws (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpackuhubs (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vpackuhubs (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpackuwuhs (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vpackuwuhs (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpackuduws (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vpackuduws (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpackshubs (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vpackshubs (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpackswuhs (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vpackswuhs (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpacksduws (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vpacksduws (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpackluhubm (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vpackluhubm (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpackluwuhm (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vpackluwuhm (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpackluduwm (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vpackluduwm (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpackhuhubm (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vpackhuhubm (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpackhuwuhm (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vpackhuwuhm (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpackhuduwm (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vpackhuduwm (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpmovsxbh (int8x32_t s)
{
  return __builtin_hcw_vpmovsxbh (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpmovsxbw (int8x32_t s)
{
  return __builtin_hcw_vpmovsxbw (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpmovsxbd (int8x32_t s)
{
  return __builtin_hcw_vpmovsxbd (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpmovsxhw (int16x16_t s)
{
  return __builtin_hcw_vpmovsxhw (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpmovsxhd (int16x16_t s)
{
  return __builtin_hcw_vpmovsxhd (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpmovsxwd (int32x8_t s)
{
  return __builtin_hcw_vpmovsxwd (s);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpmovzxbh (uint8x32_t s)
{
  return __builtin_hcw_vpmovzxbh (s);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpmovzxbw (uint8x32_t s)
{
  return __builtin_hcw_vpmovzxbw (s);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpmovzxbd (uint8x32_t s)
{
  return __builtin_hcw_vpmovzxbd (s);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpmovzxhw (uint16x16_t s)
{
  return __builtin_hcw_vpmovzxhw (s);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpmovzxhd (uint16x16_t s)
{
  return __builtin_hcw_vpmovzxhd (s);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpmovzxwd (uint32x8_t s)
{
  return __builtin_hcw_vpmovzxwd (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcvtw2ps (int32x8_t s)
{
  return __builtin_hcw_vcvtw2ps (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vcvtps2w (floatx8_t s)
{
  return __builtin_hcw_vcvtps2w (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcvtd2ps (int64x4_t s)
{
  return __builtin_hcw_vcvtd2ps (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vcvtpd2w (doublex4_t s)
{
  return __builtin_hcw_vcvtpd2w (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcvtw2pd (int32x8_t s)
{
  return __builtin_hcw_vcvtw2pd (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vcvtlps2d (floatx8_t s)
{
  return __builtin_hcw_vcvtlps2d (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vcvthps2d (floatx8_t s)
{
  return __builtin_hcw_vcvthps2d (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcvtd2pd (int64x4_t s)
{
  return __builtin_hcw_vcvtd2pd (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vcvtpd2d (doublex4_t s)
{
  return __builtin_hcw_vcvtpd2d (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vcvttps2w (floatx8_t s)
{
  return __builtin_hcw_vcvttps2w (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vcvttpd2w (doublex4_t s)
{
  return __builtin_hcw_vcvttpd2w (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vcvttps2d (floatx8_t s)
{
  return __builtin_hcw_vcvttps2d (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vcvttpd2d (doublex4_t s)
{
  return __builtin_hcw_vcvttpd2d (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vroundps (floatx8_t s, const int i)
{
  return __builtin_hcw_vroundps (s, i);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vroundpd (doublex4_t s, const int i)
{
  return __builtin_hcw_vroundpd (s, i);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsblend (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vcsblend (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcdblend (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vcdblend (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpblendb (int8x32_t s, int8x32_t t, int8x32_t r)
{
  return __builtin_hcw_vpblendb (s, t, r);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpermutb (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vpermutb (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpermute (int8x32_t s, int8x32_t t, int8x32_t r)
{
  return __builtin_hcw_vpermute (s, t, r);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpalignrib (int8x32_t s, int8x32_t t, const int i)
{
  return __builtin_hcw_vpalignrib (s, t, i);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpalignrih (int16x16_t s, int16x16_t t, const int i)
{
  return __builtin_hcw_vpalignrih (s, t, i);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpalignriw (int32x8_t s, int32x8_t t, const int i)
{
  return __builtin_hcw_vpalignriw (s, t, i);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpalignrid (int64x4_t s, int64x4_t t, const int i)
{
  return __builtin_hcw_vpalignrid (s, t, i);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vpalignrips (floatx8_t s, floatx8_t t, const int i)
{
  return __builtin_hcw_vpalignrips (s, t, i);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vpalignripd (doublex4_t s, doublex4_t t, const int i)
{
  return __builtin_hcw_vpalignripd (s, t, i);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpalignrb (int8x32_t s, int8x32_t t, int8x32_t r)
{
  return __builtin_hcw_vpalignrb (s, t, r);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpalignrh (int16x16_t s, int16x16_t t, int16x16_t r)
{
  return __builtin_hcw_vpalignrh (s, t, r);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpalignrw (int32x8_t s, int32x8_t t, int32x8_t r)
{
  return __builtin_hcw_vpalignrw (s, t, r);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpalignrd (int64x4_t s, int64x4_t t, int64x4_t r)
{
  return __builtin_hcw_vpalignrd (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vpalignrps (floatx8_t s, floatx8_t t, floatx8_t r)
{
  return __builtin_hcw_vpalignrps (s, t, r);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vpalignrpd (doublex4_t s, doublex4_t t, doublex4_t r)
{
  return __builtin_hcw_vpalignrpd (s, t, r);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpalignlib (int8x32_t s, int8x32_t t, const int i)
{
  return __builtin_hcw_vpalignlib (s, t, i);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpalignlih (int16x16_t s, int16x16_t t, const int i)
{
  return __builtin_hcw_vpalignlih (s, t, i);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpalignliw (int32x8_t s, int32x8_t t, const int i)
{
  return __builtin_hcw_vpalignliw (s, t, i);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpalignlid (int64x4_t s, int64x4_t t, const int i)
{
  return __builtin_hcw_vpalignlid (s, t, i);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vpalignlips (floatx8_t s, floatx8_t t, const int i)
{
  return __builtin_hcw_vpalignlips (s, t, i);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vpalignlipd (doublex4_t s, doublex4_t t, const int i)
{
  return __builtin_hcw_vpalignlipd (s, t, i);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpalignlb (int8x32_t s, int8x32_t t, int8x32_t r)
{
  return __builtin_hcw_vpalignlb (s, t, r);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpalignlh (int16x16_t s, int16x16_t t, int16x16_t r)
{
  return __builtin_hcw_vpalignlh (s, t, r);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpalignlw (int32x8_t s, int32x8_t t, int32x8_t r)
{
  return __builtin_hcw_vpalignlw (s, t, r);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpalignld (int64x4_t s, int64x4_t t, int64x4_t r)
{
  return __builtin_hcw_vpalignld (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vpalignlps (floatx8_t s, floatx8_t t, floatx8_t r)
{
  return __builtin_hcw_vpalignlps (s, t, r);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vpalignlpd (doublex4_t s, doublex4_t t, doublex4_t r)
{
  return __builtin_hcw_vpalignlpd (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsmulj (floatx8_t s)
{
  return __builtin_hcw_vcsmulj (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsmulnj (floatx8_t s)
{
  return __builtin_hcw_vcsmulnj (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdmulj (doublex4_t s)
{
  return __builtin_hcw_vcdmulj (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdmulnj (doublex4_t s)
{
  return __builtin_hcw_vcdmulnj (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsco (floatx8_t s)
{
  return __builtin_hcw_vcsco (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdco (doublex4_t s)
{
  return __builtin_hcw_vcdco (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcscoadd (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vcscoadd (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcscosub (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vcscosub (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdcoadd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vcdcoadd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdcosub (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vcdcosub (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsmul1 (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vcsmul1 (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsmul2 (floatx8_t s, floatx8_t t, floatx8_t r)
{
  return __builtin_hcw_vcsmul2 (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcscomul1 (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vcscomul1 (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcscomul2 (floatx8_t s, floatx8_t t, floatx8_t r)
{
  return __builtin_hcw_vcscomul2 (s, t, r);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdmul1 (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vcdmul1 (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdmul2 (doublex4_t s, doublex4_t t, doublex4_t r)
{
  return __builtin_hcw_vcdmul2 (s, t, r);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdcomul1 (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vcdcomul1 (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdcomul2 (doublex4_t s, doublex4_t t, doublex4_t r)
{
  return __builtin_hcw_vcdcomul2 (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsmuljadd (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vcsmuljadd (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsmuljsub (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vcsmuljsub (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdmuljadd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vcdmuljadd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdmuljsub (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vcdmuljsub (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdmaddf (doublex4_t s, doublex4_t t, doublex4_t r)
{
  return __builtin_hcw_vcdmaddf (s, t, r);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdmsubf (doublex4_t s, doublex4_t t, doublex4_t r)
{
  return __builtin_hcw_vcdmsubf (s, t, r);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdmaddb (doublex4_t s, doublex4_t t, doublex4_t r)
{
  return __builtin_hcw_vcdmaddb (s, t, r);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdmsubb (doublex4_t s, doublex4_t t, doublex4_t r)
{
  return __builtin_hcw_vcdmsubb (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsmaddf (floatx8_t s, floatx8_t t, floatx8_t r)
{
  return __builtin_hcw_vcsmaddf (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsmsubf (floatx8_t s, floatx8_t t, floatx8_t r)
{
  return __builtin_hcw_vcsmsubf (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsmaddb (floatx8_t s, floatx8_t t, floatx8_t r)
{
  return __builtin_hcw_vcsmaddb (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsmsubb (floatx8_t s, floatx8_t t, floatx8_t r)
{
  return __builtin_hcw_vcsmsubb (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsffts1l (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vcsffts1l (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsffts1h (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vcsffts1h (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsffts2e (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vcsffts2e (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsffts2o (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vcsffts2o (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsmod (floatx8_t s)
{
  return __builtin_hcw_vcsmod (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsslope (floatx8_t s)
{
  return __builtin_hcw_vcsslope (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcshadd (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vcshadd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdhadd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vcdhadd (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcshsub (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vcshsub (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdhsub (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vcdhsub (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vsrlb_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vsrlb_u (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vsrlh_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vsrlh_u (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vsrlw_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vsrlw_u (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vsrld_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vsrld_u (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vsrlb_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vsrlb_s (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vsrlh_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vsrlh_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vsrlw_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vsrlw_s (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vsrld_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vsrld_s (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vsrab_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vsrab_u (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vsrah_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vsrah_u (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vsraw_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vsraw_u (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vsrad_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vsrad_u (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vsrab_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vsrab_s (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vsrah_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vsrah_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vsraw_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vsraw_s (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vsrad_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vsrad_s (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vsllb_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vsllb_u (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vsllh_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vsllh_u (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vsllw_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vsllw_u (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vslld_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vslld_u (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vsllb_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vsllb_s (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vsllh_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vsllh_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vsllw_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vsllw_s (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vslld_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vslld_s (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vsrrb_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hcw_vsrrb_u (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vsrrh_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hcw_vsrrh_u (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vsrrw_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hcw_vsrrw_u (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vsrrd_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hcw_vsrrd_u (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vsrrb_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hcw_vsrrb_s (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vsrrh_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hcw_vsrrh_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vsrrw_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hcw_vsrrw_s (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vsrrd_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hcw_vsrrd_s (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vsrlbi_u (uint8x32_t s, const int i)
{
  return __builtin_hcw_vsrlbi_u (s, i);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vsrlhi_u (uint16x16_t s, const int i)
{
  return __builtin_hcw_vsrlhi_u (s, i);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vsrlwi_u (uint32x8_t s, const int i)
{
  return __builtin_hcw_vsrlwi_u (s, i);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vsrldi_u (uint64x4_t s, const int i)
{
  return __builtin_hcw_vsrldi_u (s, i);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vsrlbi_s (int8x32_t s, const int i)
{
  return __builtin_hcw_vsrlbi_s (s, i);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vsrlhi_s (int16x16_t s, const int i)
{
  return __builtin_hcw_vsrlhi_s (s, i);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vsrlwi_s (int32x8_t s, const int i)
{
  return __builtin_hcw_vsrlwi_s (s, i);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vsrldi_s (int64x4_t s, const int i)
{
  return __builtin_hcw_vsrldi_s (s, i);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vsrabi_u (uint8x32_t s, const int i)
{
  return __builtin_hcw_vsrabi_u (s, i);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vsrahi_u (uint16x16_t s, const int i)
{
  return __builtin_hcw_vsrahi_u (s, i);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vsrawi_u (uint32x8_t s, const int i)
{
  return __builtin_hcw_vsrawi_u (s, i);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vsradi_u (uint64x4_t s, const int i)
{
  return __builtin_hcw_vsradi_u (s, i);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vsrabi_s (int8x32_t s, const int i)
{
  return __builtin_hcw_vsrabi_s (s, i);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vsrahi_s (int16x16_t s, const int i)
{
  return __builtin_hcw_vsrahi_s (s, i);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vsrawi_s (int32x8_t s, const int i)
{
  return __builtin_hcw_vsrawi_s (s, i);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vsradi_s (int64x4_t s, const int i)
{
  return __builtin_hcw_vsradi_s (s, i);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vsllbi_u (uint8x32_t s, const int i)
{
  return __builtin_hcw_vsllbi_u (s, i);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vsllhi_u (uint16x16_t s, const int i)
{
  return __builtin_hcw_vsllhi_u (s, i);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vsllwi_u (uint32x8_t s, const int i)
{
  return __builtin_hcw_vsllwi_u (s, i);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vslldi_u (uint64x4_t s, const int i)
{
  return __builtin_hcw_vslldi_u (s, i);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vsllbi_s (int8x32_t s, const int i)
{
  return __builtin_hcw_vsllbi_s (s, i);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vsllhi_s (int16x16_t s, const int i)
{
  return __builtin_hcw_vsllhi_s (s, i);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vsllwi_s (int32x8_t s, const int i)
{
  return __builtin_hcw_vsllwi_s (s, i);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vslldi_s (int64x4_t s, const int i)
{
  return __builtin_hcw_vslldi_s (s, i);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vsrrbi_u (uint8x32_t s, const int i)
{
  return __builtin_hcw_vsrrbi_u (s, i);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vsrrhi_u (uint16x16_t s, const int i)
{
  return __builtin_hcw_vsrrhi_u (s, i);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vsrrwi_u (uint32x8_t s, const int i)
{
  return __builtin_hcw_vsrrwi_u (s, i);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vsrrdi_u (uint64x4_t s, const int i)
{
  return __builtin_hcw_vsrrdi_u (s, i);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vsrrbi_s (int8x32_t s, const int i)
{
  return __builtin_hcw_vsrrbi_s (s, i);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vsrrhi_s (int16x16_t s, const int i)
{
  return __builtin_hcw_vsrrhi_s (s, i);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vsrrwi_s (int32x8_t s, const int i)
{
  return __builtin_hcw_vsrrwi_s (s, i);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vsrrdi_s (int64x4_t s, const int i)
{
  return __builtin_hcw_vsrrdi_s (s, i);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcvtpls2pd (floatx8_t s)
{
  return __builtin_hcw_vcvtpls2pd (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcvtphs2pd (floatx8_t s)
{
  return __builtin_hcw_vcvtphs2pd (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcvtpd2ps (doublex4_t s)
{
  return __builtin_hcw_vcvtpd2ps (s);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vtestalloneb_s (int8x32_t s)
{
  return __builtin_hcw_vtestalloneb_s (s);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vtestalloneh_s (int16x16_t s)
{
  return __builtin_hcw_vtestalloneh_s (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vtestallonew_s (int32x8_t s)
{
  return __builtin_hcw_vtestallonew_s (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vtestalloned_s (int64x4_t s)
{
  return __builtin_hcw_vtestalloned_s (s);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vtestalloneb_u (uint8x32_t s)
{
  return __builtin_hcw_vtestalloneb_u (s);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vtestalloneh_u (uint16x16_t s)
{
  return __builtin_hcw_vtestalloneh_u (s);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vtestallonew_u (uint32x8_t s)
{
  return __builtin_hcw_vtestallonew_u (s);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vtestalloned_u (uint64x4_t s)
{
  return __builtin_hcw_vtestalloned_u (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vtestalloneps (floatx8_t s)
{
  return __builtin_hcw_vtestalloneps (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vtestallonepd (doublex4_t s)
{
  return __builtin_hcw_vtestallonepd (s);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vtesthasoneb_s (int8x32_t s)
{
  return __builtin_hcw_vtesthasoneb_s (s);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vtesthasoneh_s (int16x16_t s)
{
  return __builtin_hcw_vtesthasoneh_s (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vtesthasonew_s (int32x8_t s)
{
  return __builtin_hcw_vtesthasonew_s (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vtesthasoned_s (int64x4_t s)
{
  return __builtin_hcw_vtesthasoned_s (s);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vtesthasoneb_u (uint8x32_t s)
{
  return __builtin_hcw_vtesthasoneb_u (s);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vtesthasoneh_u (uint16x16_t s)
{
  return __builtin_hcw_vtesthasoneh_u (s);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vtesthasonew_u (uint32x8_t s)
{
  return __builtin_hcw_vtesthasonew_u (s);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vtesthasoned_u (uint64x4_t s)
{
  return __builtin_hcw_vtesthasoned_u (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vtesthasoneps (floatx8_t s)
{
  return __builtin_hcw_vtesthasoneps (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vtesthasonepd (doublex4_t s)
{
  return __builtin_hcw_vtesthasonepd (s);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vtestallzerob_s (int8x32_t s)
{
  return __builtin_hcw_vtestallzerob_s (s);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vtestallzeroh_s (int16x16_t s)
{
  return __builtin_hcw_vtestallzeroh_s (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vtestallzerow_s (int32x8_t s)
{
  return __builtin_hcw_vtestallzerow_s (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vtestallzerod_s (int64x4_t s)
{
  return __builtin_hcw_vtestallzerod_s (s);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vtestallzerob_u (uint8x32_t s)
{
  return __builtin_hcw_vtestallzerob_u (s);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vtestallzeroh_u (uint16x16_t s)
{
  return __builtin_hcw_vtestallzeroh_u (s);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vtestallzerow_u (uint32x8_t s)
{
  return __builtin_hcw_vtestallzerow_u (s);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vtestallzerod_u (uint64x4_t s)
{
  return __builtin_hcw_vtestallzerod_u (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vtestallzerops (floatx8_t s)
{
  return __builtin_hcw_vtestallzerops (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vtestallzeropd (doublex4_t s)
{
  return __builtin_hcw_vtestallzeropd (s);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vtesthaszerob_s (int8x32_t s)
{
  return __builtin_hcw_vtesthaszerob_s (s);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vtesthaszeroh_s (int16x16_t s)
{
  return __builtin_hcw_vtesthaszeroh_s (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vtesthaszerow_s (int32x8_t s)
{
  return __builtin_hcw_vtesthaszerow_s (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vtesthaszerod_s (int64x4_t s)
{
  return __builtin_hcw_vtesthaszerod_s (s);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vtesthaszerob_u (uint8x32_t s)
{
  return __builtin_hcw_vtesthaszerob_u (s);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vtesthaszeroh_u (uint16x16_t s)
{
  return __builtin_hcw_vtesthaszeroh_u (s);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vtesthaszerow_u (uint32x8_t s)
{
  return __builtin_hcw_vtesthaszerow_u (s);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vtesthaszerod_u (uint64x4_t s)
{
  return __builtin_hcw_vtesthaszerod_u (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vtesthaszerops (floatx8_t s)
{
  return __builtin_hcw_vtesthaszerops (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vtesthaszeropd (doublex4_t s)
{
  return __builtin_hcw_vtesthaszeropd (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmpeqps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vcmpeqps (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmpltps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vcmpltps (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmpleps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vcmpleps (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmpneps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vcmpneps (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmpgtps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vcmpgtps (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmpgeps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vcmpgeps (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmpodps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vcmpodps (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmpunps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vcmpunps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmpeqpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vcmpeqpd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmpltpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vcmpltpd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmplepd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vcmplepd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmpnepd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vcmpnepd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmpgtpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vcmpgtpd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmpgepd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vcmpgepd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmpodpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vcmpodpd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmpunpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vcmpunpd (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmpeqsps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vcmpeqsps (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmpltsps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vcmpltsps (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmplesps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vcmplesps (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmpnesps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vcmpnesps (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmpgtsps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vcmpgtsps (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmpgesps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vcmpgesps (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmpodsps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vcmpodsps (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmpunsps (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vcmpunsps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmpeqspd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vcmpeqspd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmpltspd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vcmpltspd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmplespd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vcmplespd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmpnespd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vcmpnespd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmpgtspd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vcmpgtspd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmpgespd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vcmpgespd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmpodspd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vcmpodspd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmpunspd (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vcmpunspd (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vscscmpeq (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vscscmpeq (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vscscmpne (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vscscmpne (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vscscmpod (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vscscmpod (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vscscmpun (floatx8_t s, floatx8_t t)
{
  return __builtin_hcw_vscscmpun (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vscdcmpeq (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vscdcmpeq (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vscdcmpne (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vscdcmpne (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vscdcmpod (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vscdcmpod (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vscdcmpun (doublex4_t s, doublex4_t t)
{
  return __builtin_hcw_vscdcmpun (s, t);
}

__extension__ static __inline int __attribute__ ((__always_inline__))
    vmtr_s (int32x8_t s)
{
  return __builtin_hcw_vmtr_s (s);
}

__extension__ static __inline unsigned int __attribute__ ((__always_inline__))
    vmtr_u (uint32x8_t s)
{
  return __builtin_hcw_vmtr_u (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vmfr_s (int s)
{
  return __builtin_hcw_vmfr_s (s);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vmfr_u (unsigned int s)
{
  return __builtin_hcw_vmfr_u (s);
}

__extension__ static __inline long long __attribute__ ((__always_inline__))
    vmtrd_s (int64x4_t s)
{
  return __builtin_hcw_vmtrd_s (s);
}

__extension__ static __inline unsigned long long __attribute__ ((__always_inline__))
    vmtrd_u (uint64x4_t s)
{
  return __builtin_hcw_vmtrd_u (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vmfrd_s (long long s)
{
  return __builtin_hcw_vmfrd_s (s);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vmfrd_u (unsigned long long s)
{
  return __builtin_hcw_vmfrd_u (s);
}

__extension__ static __inline float __attribute__ ((__always_inline__))
    vmtfs (floatx8_t s)
{
  return __builtin_hcw_vmtfs (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vmffs (float s)
{
  return __builtin_hcw_vmffs (s);
}

__extension__ static __inline double __attribute__ ((__always_inline__))
    vmtfd (doublex4_t s)
{
  return __builtin_hcw_vmtfd (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vmffd (double s)
{
  return __builtin_hcw_vmffd (s);
}

#endif
