#ifndef _REWORKS_ARCH_CACHE_H_
#define _REWORKS_ARCH_CACHE_H_

#ifdef __cplusplus
extern "C" 
{
#endif
#include <reworks/types.h>

//typedef	struct				/* Cache Routine Pointers */
//{
//    FUNCPTR	enableRtn;		/* cacheEnable() */
//    FUNCPTR	disableRtn;		/* cacheDisable() */
//    FUNCPTR	lockRtn;		/* cacheLock() */
//    FUNCPTR	unlockRtn;		/* cacheUnlock() */
//    FUNCPTR	flushRtn;		/* cacheFlush() */
//    FUNCPTR	invalidateRtn;		/* cacheInvalidate() */
//    FUNCPTR	clearRtn;		/* cacheClear() */
//
//    FUNCPTR	textUpdateRtn;		/* cacheTextUpdate() */
//    FUNCPTR	pipeFlushRtn;		/* cachePipeFlush() */
//    FUNCPTR	dmaMallocRtn;		/* cacheDmaMalloc() */
//    FUNCPTR	dmaFreeRtn;		/* cacheDmaFree() */
//    FUNCPTR	dmaVirtToPhysRtn;	/* virtual-to-Physical Translation */
//    FUNCPTR	dmaPhysToVirtRtn;	/* physical-to-Virtual Translation */
//} RE_CACHE_LIB;

//#ifdef LOONGSON3A_1000
//#define CACHE_LINE_SIZE (32)
//#else /*2K1000 3A3000*/
//#define CACHE_LINE_SIZE (64)
//#endif



//extern  RE_CACHE_LIB re_cache_lib;
extern int cache_flush(u32	 cache_type,void *	addr,size_t	size);
extern int cache_invalidate(u32	  cache_type,	void *	addr, size_t size);
//extern int cache_dma_free(void * pBuf );
extern void * cache_dma_malloc(size_t bytes);

#define	REWORKS_CACHE_FLUSH(adrs, bytes)		cache_flush(DATA_CACHE,adrs,bytes)

#define	REWORKS_CACHE_INVALIDATE(adrs, bytes)	cache_invalidate(DATA_CACHE,adrs,bytes)

#define	REWORKS_CACHE_DMA_MALLOC(bytes)				cache_dma_malloc(bytes)

#define	REWORKS_CACHE_DMA_FREE(adrs)				cache_dma_free(adrs)

#define	REWORKS_CACHE_PIPE_FLUSH()	\
        ((re_cache_lib.pipeFlushRtn == NULL) ? OK :	\
        (re_cache_lib.pipeFlushRtn) ())

#define	REWORKS_CACHE_IS_WRITE_COHERENT()	\
	(re_cache_lib.flushRtn == NULL)

#define	REWORKS_CACHE_IS_READ_COHERENT()	\
	(re_cache_lib.invalidateRtn == NULL)

#ifdef __cplusplus
}
#endif
#endif /* _REWORKS_CACHE_H_ */
