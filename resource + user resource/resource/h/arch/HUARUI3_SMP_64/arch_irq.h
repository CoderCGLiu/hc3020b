/*******************************************************************************
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了 loongson 处理器体系结构的中断定义
 * 修改：
 * 		 1. 
 */
#ifndef _REWORKS_ARCH_IRQ_H_
#define _REWORKS_ARCH_IRQ_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include "hr3_intc_defs.h"
//#include "hr2_intc_defs.h"
//#define IRQ_NUM				66
#define IRQ_NUM  HR3_INT_CNT
	

int shared_int_module_init(void);

#ifdef __cplusplus
}
#endif

#endif /* _IRQ_H_ */
