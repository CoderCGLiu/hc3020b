/*
	MIPS汇编语言相关定义的头文件
 */
#ifndef _MIPS_ASM_H_
#define _MIPS_ASM_H_

#ifdef __cplusplus
extern "C"
{
#endif

#ifdef ASM_LANGUAGE
/*
*  MIPS register definitions
*/
#include <regdef.h>
#define AT	$at	/* assembler temp */

#define LEAF(name) \
  	.text; \
  	.globl	name; \
  	.ent	name; \
name:

#define XLEAF(name) \
  	.text; \
  	.globl	name; \
  	.aent	name; \
name:

#define WLEAF(name) \
  	.text; \
  	.weakext name; \
  	.ent	name; \
name:

#define SLEAF(name) \
  	.text; \
  	.ent	name; \
name:

#define END(name) \
  	.size name,.-name; \
  	.end	name

#define SEND(name) END(name)
#define WEND(name) END(name)

#endif /* ASM_LANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* MIPS_ASM */
