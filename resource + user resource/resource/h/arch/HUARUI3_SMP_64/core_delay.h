#ifndef _REWORKS_CORE_DELAY_H_
#define _REWORKS_CORE_DELAY_H_

#ifdef __cplusplus
extern "C" {
#endif

#define LPS_PREC 8  										// 校准精度，精度越高校准时间越长
#define OPTIMIZE(level) __attribute__((optimize(level)))    // 为防止udelay函数被优化，故定义还宏用以修改udelay相关函数
#define LPS_STEP (1<<8)	

extern u32 bsp_clicks_per_usec;
OPTIMIZE("O0") static inline void __delay(unsigned int loops)
{
	__asm__ __volatile__ (
	"	.set	noreorder				\n"
	"	.align	3					\n"
	"1:	bnez	%0, 1b					\n"
	"	subu	%0, 1					\n"
	"	.set	reorder					\n"
	: "=r" (loops)
	: "0" (loops));
}

extern u64 sys_timestamp ();
u64 __attribute__((always_inline)) static  __sys_timestamp (void)
{
#if 0
	u64 stamp;
	asm volatile (
			"dmfc0 %0,$25,1 \n"
			:"=r"(stamp)
			 );
	return stamp;
#else
	return sys_timestamp();
#endif
}


static __attribute__((always_inline)) void __delay_2(u32 us)
{
	u64 endTime = (u64)bsp_clicks_per_usec * us + __sys_timestamp(); 
	register u64 t;

	do
	{
		t = __sys_timestamp();
	}while(t < endTime);
}
#ifdef __cplusplus
}
#endif
#endif /* _REWORKS_CORE_DELAY_H_ */
