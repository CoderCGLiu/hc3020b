/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了CSP模块中loongson2f处理器定义
 * 修改：
 * 		 1. 2012-5-17，规范CSP 
 */

#ifndef _REWORKS_CPU_H_
#define _REWORKS_CPU_H_

#ifdef __cplusplus
extern "C" 
{
#endif

#ifndef _HC3020B_
#define  _HC3020B_  /*ldf  20230921 add:: 临时添加*/
#endif

#ifndef _HC3020B_
#define MAX_SMP_CPUS 8
#else /*ldf 20230920 add:: for 3020b*/
#define MAX_SMP_CPUS 2
#endif

/* 上下文类型 */
#define SWITCH_TYPE		0xa5a50101
#define INTERRUPT_TYPE	0x10105a5a

#ifdef ASM_LANGUAGE
	
#define R_SZ 8

#define THR_CONTEXT_CONTROL_SIZE 112

#define PC_OFFSET 1
#define S0_OFFSET 2
#define S1_OFFSET 3
#define S2_OFFSET 4
#define S3_OFFSET 5
#define S4_OFFSET 6
#define S5_OFFSET 7
#define S6_OFFSET 8
#define S7_OFFSET 9
#define GP_OFFSET 10
#define SP_OFFSET 11
#define FP_OFFSET 12
#define RA_OFFSET 13

#define INT_CONTEXT_CONTROL_SIZE 272

#define INT_AT 14
#define INT_V0 15
#define INT_V1 16
#define INT_A0 17
#define INT_A1 18
#define INT_A2 19
#define INT_A3 20
#define INT_T0 21
#define INT_T1 22
#define INT_T2 23
#define INT_T3 24
#define INT_T4 25
#define INT_T5 26
#define INT_T6 27
#define INT_T7 28
#define INT_T8 29
#define INT_T9 30
#define INT_K1 31
#define INT_MULHI 32
#define INT_MULLO 33

#define FP0_OFFSET  0
#define FP1_OFFSET  1
#define FP2_OFFSET  2
#define FP3_OFFSET  3
#define FP4_OFFSET  4
#define FP5_OFFSET  5
#define FP6_OFFSET  6
#define FP7_OFFSET  7
#define FP8_OFFSET  8
#define FP9_OFFSET  9
#define FP10_OFFSET 10
#define FP11_OFFSET 11
#define FP12_OFFSET 12
#define FP13_OFFSET 13
#define FP14_OFFSET 14
#define FP15_OFFSET 15
#define FP16_OFFSET 16
#define FP17_OFFSET 17
#define FP18_OFFSET 18
#define FP19_OFFSET 19
#define FP20_OFFSET 20
#define FP21_OFFSET 21
#define FP22_OFFSET 22
#define FP23_OFFSET 23
#define FP24_OFFSET 24
#define FP25_OFFSET 25
#define FP26_OFFSET 26
#define FP27_OFFSET 27
#define FP28_OFFSET 28
#define FP29_OFFSET 29
#define FP30_OFFSET 30
#define FP31_OFFSET 31
#define FPCS_OFFSET 32

#define FPU_CONTEXT_CONTROL_SIZE 260

#define FPU_REG_BASE 0

#endif /* #ifdef ASM_LANGUAGE */

#define CPU_ALIGNMENT               32
#ifndef ASM_LANGUAGE

#include <reworks/types.h>
#include <machine/endian.h>

#define CPU_STACK_MIN_SIZE          4096
#define CACHE_ALIGNMENT             32

#ifdef  _LITTLE_ENDIAN	
#undef  _LITTLE_ENDIAN
#endif

#ifdef  _BIG_ENDIAN
#undef  _BIG_ENDIAN
#endif

#define _LITTLE_ENDIAN			4321
#define _BIG_ENDIAN				1234
#define _BYTE_ORDER				_LITTLE_ENDIAN

#ifdef BYTE_ORDER
#undef BYTE_ORDER
#define BYTE_ORDER	_BYTE_ORDER
#endif

typedef struct 
{
  void       (*idle_task)( void );
  boolean      do_zero_of_workspace;
  u32   idle_task_stack_size;
  u32   interrupt_stack_size;
}   rtos_cpu_table;

typedef struct {
	u32 type;
    u32 c0_sr;
	u64 pc;
    u64 s0;
    u64 s1;
    u64 s2;
    u64 s3;
    u64 s4;
    u64 s5;
    u64 s6;
    u64 s7;
    u64 gp;
    u64 sp;
    u64 fp;
    u64 ra;
	u64 AT;
	u64 v0;
	u64 v1;
	u64 a0;
	u64 a1;
	u64 a2;
	u64 a3;
    u64 t0;
    u64 t1;
    u64 t2;
    u64 t3;
    u64 t4;
    u64 t5;
    u64 t6;
    u64 t7;
    u64 t8;
    u64 t9;
    u64 k1;
    u64 mulhi;
    u64 mullo;
} Context_Ctrl;

typedef Context_Ctrl REG_SET;

#define HAVE_FP_CONTEXT

typedef u64 fpureg_t;
typedef u32 fpuctrlreg_t;

typedef struct _Fp_Context{
	fpureg_t fp0;
	fpureg_t fp1;
	fpureg_t fp2;
	fpureg_t fp3;
	fpureg_t fp4;
	fpureg_t fp5;
	fpureg_t fp6;
	fpureg_t fp7;
	fpureg_t fp8;
	fpureg_t fp9;
	fpureg_t fp10;
	fpureg_t fp11;
	fpureg_t fp12;
	fpureg_t fp13;
	fpureg_t fp14;
	fpureg_t fp15;
	fpureg_t fp16;
	fpureg_t fp17;
	fpureg_t fp18;
	fpureg_t fp19;
	fpureg_t fp20;
	fpureg_t fp21;
	fpureg_t fp22;
	fpureg_t fp23;
	fpureg_t fp24;
	fpureg_t fp25;
	fpureg_t fp26;
	fpureg_t fp27;
	fpureg_t fp28;
	fpureg_t fp29;
	fpureg_t fp30;
	fpureg_t fp31;
	fpuctrlreg_t fpcs;
} FP_CONTEXT;

typedef FP_CONTEXT Fp_Context;

/* 异常时的上下文 */  
typedef struct Exc_Regs
{
	u32 reg_val_cause;
	u64 reg_val_badaddr;
	Context_Ctrl reg_context_gp;
	Fp_Context reg_context_fp;
} Exc_Regs;

int cpu_id_get();
void context_init(Context_Ctrl *the_context, u32 isr, void *entry_point, void *prev_entry);
void context_switch(Context_Ctrl **run, Context_Ctrl *heir);
void context_restore(Context_Ctrl *heir);

/**
 * 将TCB中异常发生时的上下文拷贝给用户
 * @param thread_ptr 待获取上下文的任务
 * @param p_reg_val 用户层传入的Exc_Regs类型指针，用于保存异常发生时的上下文。
 */
void exc_context_get(void *thread_ptr, Exc_Regs *p_reg_val);

/**
 * 设置需要跳过的异常。
 * @param exc_nu 异常类型编号
 * @param is_skip 打开或关闭跳过，1代表打开，0代表关闭
 */
void set_skip_exc(int exc_nu, int is_skip);

/**
 * 设置是否打印异常信息。
 * @param b_p	0：不打印。
 * 				1: 打印。				
 */
void exc_print(int b_p);

int num_online_cpus(void);

#if 0
#define __sti() \
	__asm__ __volatile__( \
		".set\tpush\n\t" \
		".set\treorder\n\t" \
		".set\tnoat\n\t" \
		"mfc0\t$1,$12\n\t" \
		"ori\t$1,0x1f\n\t" \
		"xori\t$1,0x1e\n\t" \
		"mtc0\t$1,$12\n\t" \
		".set\tpop\n\t" \
		: /* no outputs */ \
		: /* no inputs */ \
		: "$1", "memory")

/*
 * For cli() we have to insert nops to make shure that the new value
 * has actually arrived in the status register before the end of this
 * macro.
 * R4000/R4400 need three nops, the R4600 two nops and the R10000 needs
 * no nops at all.
 */
#define __cli() \
	__asm__ __volatile__( \
		".set\tpush\n\t" \
		".set\treorder\n\t" \
		".set\tnoat\n\t" \
		"mfc0\t$1,$12\n\t" \
		"ori\t$1,1\n\t" \
		"xori\t$1,1\n\t" \
		".set\tnoreorder\n\t" \
		"mtc0\t$1,$12\n\t" \
		"nop\n\t" \
		"nop\n\t" \
		"nop\n\t" \
		".set\tpop\n\t" \
		: /* no outputs */ \
		: /* no inputs */ \
		: "$1", "memory")

#define __save_flags(x)                  \
__asm__ __volatile__(                    \
	".set\tpush\n\t"		 \
	".set\treorder\n\t"              \
	"mfc0\t%0,$12\n\t"               \
	".set\tpop\n\t"                      \
	: "=r" (x)                       \
	: /* no inputs */                \
	: "memory")

#define __save_and_cli(x)                \
__asm__ __volatile__(                    \
	".set\tpush\n\t"		 \
	".set\treorder\n\t"              \
	".set\tnoat\n\t"                 \
	"mfc0\t%0,$12\n\t"               \
	"ori\t$1,%0,1\n\t"               \
	"xori\t$1,1\n\t"                 \
	".set\tnoreorder\n\t"		 \
	"mtc0\t$1,$12\n\t"               \
	"nop\n\t"                        \
	"nop\n\t"                        \
	"nop\n\t"                        \
	".set\tpop\n\t"                  \
	: "=r" (x)                       \
	: /* no inputs */                \
	: "$1", "memory")

#define __restore_flags(x) \
	__asm__ __volatile__( \
		".set\tpush\n\t" \
		".set\treorder\n\t" \
		"mfc0\t$8,$12\n\t" \
		"li\t$9,0xff00\n\t" \
		"and\t$8,$9\n\t" \
		"nor\t$9,$0,$9\n\t" \
		"and\t%0,$9\n\t" \
		"or\t%0,$8\n\t" \
		".set\tnoreorder\n\t" \
		"mtc0\t%0,$12\n\t" \
		"nop\n\t" \
		"nop\n\t" \
		"nop\n\t" \
		".set\tpop\n\t" \
		: \
		: "r" (x) \
		: "$8", "$9", "memory")

/*
 * Non-SMP versions ...
 */
#define sti() __sti()
#define cli() __cli()
#define save_flags(x) __save_flags(x)
#define save_and_cli(x) __save_and_cli(x)
#define restore_flags(x) __restore_flags(x)

/* For spinlocks etc */
#define local_irq_save(x)	__save_and_cli(x);
#define local_irq_restore(x)	__restore_flags(x);
#define local_irq_disable()	__cli();
#define local_irq_enable()	__sti();
#endif

#define __swap16gen(x) ({						\
	u16 __swap16gen_x = (x);					\
									\
	(u16)((__swap16gen_x & 0xff) << 8 |			\
	    (__swap16gen_x & 0xff00) >> 8);				\
})

#define __swap32gen(x) ({						\
	u32 __swap32gen_x = (x);					\
									\
	(u32)((__swap32gen_x & 0xff) << 24 |			\
	    (__swap32gen_x & 0xff00) << 8 |				\
	    (__swap32gen_x & 0xff0000) >> 8 |				\
	    (__swap32gen_x & 0xff000000) >> 24);			\
})

#define CPU_swap_u16( _value )  __swap16gen(_value)
#define CPU_swap_u32( _value )  __swap32gen(_value)

/* 这是一个叶子函数，它用于取得状态寄存器的值。*/
extern u32 get_c0_sr();

/* 这是一个叶子函数，它用于设置状态寄存器的值。*/
extern void set_c0_sr(u32);

/* 这是一个叶子函数，它用于取得计数寄存器的值。*/
extern u32 get_c0_count();

/**
 * 获取处理器时间戳接口
 */
extern u64 sys_timestamp (void);

/**
 * 获取处理器频率的接口
 */
extern u32 sys_timestamp_freq(void);

#define MEM_BARRIER_R() __asm volatile ("sync" ::: "memory")
#define MEM_BARRIER_W() __asm volatile ("sync" ::: "memory")
#define MEM_BARRIER_RW() __asm volatile ("sync" ::: "memory")

extern SYSTEM_BASIC_INFO *sys_info_get(void);

#endif /* #ifndef ASM_LANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* _REWORKS_CPU_H_ */
