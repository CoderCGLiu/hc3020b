/*
*  loongson2e架构相关定义的头文件
*/
#ifndef _MIPS_ARCH_CPU_H_
#define _MIPS_ARCH_CPU_H_
 
#ifdef __cplusplus
extern "C"
{
#endif

#ifdef ASM_LANGUAGE

#define	C0_INDEX	$0	/* TLB操作表项索引寄存器 */
#define	C0_RANDOM	$1	/* TLB随机表项索引寄存器 */
#define	C0_ENTRYLO0	$2
#define	C0_ENTRYLO1	$3

#define	C0_PAGEMASK	$5
#define C0_WIRED	$6	/* TLB固定表项索引寄存器 */

#define	C0_BADVADDR	$8	/* 错误虚地址寄存器 */
#define	C0_COUNT	$9	/* 计数寄存器 */
#define	C0_ENTRYHI	$10
#define	C0_COMPARE	$11	/* 比较寄存器 */
#define	C0_SR		$12	/* 状态寄存器 */
#define	C0_CAUSE	$13	/* 异常原因寄存器 */
#define	C0_EPC		$14	/* 异常pc寄存器 */
#define C0_PRID		$15 /* 处理器修订标识寄存器 */
#define C0_CONFIG	$16 /* 配置寄存器 */
#define C0_WATCHLO  $18 /* 监视寄存器 */

#ifdef _BAO_MOD
#define C0_WATCHHI  $19
#endif

#define C0_PERF_CTL	$24 /* 性能计数控制寄存器 */

#define C0_ERRORPC	$30

#define C0_DERR0 $26 /* CFC */
#define C0_DERR1 $27 /* CFC */
#endif /* ASM_LANGUAGE */


#define PM_4K		0x00000000
#define PM_16K		0x00006000
#define PM_64K		0x0001e000
#define PM_256K		0x0007e000
#define PM_1M		0x001fe000
#define PM_4M		0x007fe000
#define PM_16M		0x01ffe000

/*
 * 状态寄存器各位的定义
 */
#define	SR_CUMASK	0xf0000000	/* coproc usable bits */
#define	SR_CU3		0x80000000	/* Coprocessor 3 usable */
#define	SR_CU2		0x40000000	/* Coprocessor 2 usable */
#define	SR_CU1		0x20000000	/* Coprocessor 1 usable */
#define	SR_CU0		0x10000000	/* Coprocessor 0 usable */
#define SR_RP		0x08000000      /* Use reduced power mode */
#define SR_FR		0x04000000      /* Enable extra floating point regs */
#define SR_RE		0x02000000      /* Reverse endian in user mode */
#define	SR_BEV		0x00400000	/* use boot exception vectors */
#define	SR_TS		0x00200000	/* TLB shutdown */
#define SR_SR		0x00100000	/* soft reset occurred */
#define	SR_CH		0x00040000	/* cache hit */
#define	SR_CE		0x00020000	/* use ECC reg */
#define	SR_DE		0x00010000	/* disable cache errors */
#define	SR_IMASK	0x0000ff00	/* Interrupt mask */
#define	SR_IMASK8	0x00000000	/* mask level 8 */
#define	SR_IMASK7	0x00008000	/* mask level 7 */
#define	SR_IMASK6	0x0000c000	/* mask level 6 */
#define	SR_IMASK5	0x0000e000	/* mask level 5 */
#define	SR_IMASK4	0x0000f000	/* mask level 4 */
#define	SR_IMASK3	0x0000f800	/* mask level 3 */
#define	SR_IMASK2	0x0000fc00	/* mask level 2 */
#define	SR_IMASK1	0x0000fe00	/* mask level 1 */
#define	SR_IMASK0	0x0000ff00	/* mask level 0 */
#define	SR_IBIT8	0x00008000	/* bit level 8 */
#define	SR_IBIT7	0x00004000	/* bit level 7 */
#define	SR_IBIT6	0x00002000	/* bit level 6 */
#define	SR_IBIT5	0x00001000	/* bit level 5 */
#define	SR_IBIT4	0x00000800	/* bit level 4 */
#define	SR_IBIT3	0x00000400	/* bit level 3 */
#define	SR_IBIT2	0x00000200	/* bit level 2 */
#define	SR_IBIT1	0x00000100	/* bit level 1 */

/* loongson2e specific */
#define	SR_KX		0x00000080	/* enable kernel 64bit addresses, always 1 */
#define	SR_SX		0x00000040	/* enable supervisor 64bit addresses, always 1 */
#define	SR_UX		0x00000020	/* enable user 64bit addresses, always 1 */
#define	SR_KSUMASK	0x00000018	/* mode mask */
#define SR_KSU_K	0x00000000	/* kernel mode */
#define SR_KSU_S	0x00000008	/* supervisor mode */
#define SR_KSU_U	0x00000010	/* user mode */
#define	SR_ERL		0x00000004	/* Error Level */
#define	SR_EXL		0x00000002	/* Exception Level */
#define	SR_IE		0x00000001	/* interrupt enable, 1 => enable */

#define	CAUSE_IP7	0x00008000	/* External level 7 pending */
#define	CAUSE_IP6	0x00004000	/* External level 6 pending */
#define	CAUSE_IP5	0x00002000	/* External level 5 pending */
#define	CAUSE_IP4	0x00001000	/* External level 4 pending */
#define	CAUSE_IP3	0x00000800	/* External level 3 pending */
#define	CAUSE_IP2	0x00000400	/* External level 2 pending */
#define	CAUSE_SW1	0x00000200	/* Software level 1 pending */
#define	CAUSE_SW0	0x00000100	/* Software level 0 pending */
#define	CAUSE_IPMASK	0x0000FF00	/* Pending interrupt mask */

#define	CAUSE_EXCMASK	0x0000007C	/* Cause code bits */
#define	CAUSE_EXCSHIFT	2
#define	EXC_CODE_INT	0		/* externel interrupt */
/*以下错误号在核心exc.h中定义*/
//#define EXC_CODE_TLBMOD	1       /*TLB Modify*/
//#define EXC_CODE_TLBL	2		/* TLB miss exception (load or instruction fetch) */
//#define EXC_CODE_TLBS	3		/* TLB miss exception (store) */
//#define	EXC_CODE_ADEL	4		/* address error exception (load/instruction fetch) */
//#define EXC_CODE_ADES	5		/* address error exception (store) */
//#define	EXC_CODE_IBE	6		/* Instrution bus error */
//#define EXC_CODE_DBE	7		/* data bus error */
//#define EXC_CODE_SYS	8		/* system call */
//#define	EXC_CODE_BP 	9		/* breakpoint exception */
//#define EXC_CODE_RI		10		/* reserved instruction exception */
//#define EXC_CODE_CPU	11		/* coprocessor unusable exception */
//#define EXC_CODE_OV     12      /* overflow vector              */
//#define EXC_CODE_TRAP   13      /* trap vector                  */
//#define EXC_CODE_FPE	15		/* floating point exception */
//#define EXC_CODE_WATCH	23		/* watchpoint exception */

#define	PRID_LSN2F	0x00006303
#define	PRID_LSN3A	0x00006305
#define PRID_LSN3B	0x00006307

#define CONFIG_DB		0x00000010 /* Data cache block size */
#define CONFIG_IB		0x00000020 /* Instruction cache block size */
#define	CONFIG_DCMASK	0x000001C0 /* Data cache size */
#define	CONFIG_DCSHIFT	6
#define	CONFIG_ICMASK	0x00000e00 /* Instruction cache size */
#define	CONFIG_ICSHIFT	9
#define CONFIG_SC		0x00020000	/* Secondary cache absent */
#define CONFIG_SBMASK	0x00c00000	/* Secondary cache block size */
#define CONFIG_SBSHIFT	22
#define CONFIG_K0MASK	0x00000007	/* KSEG0 coherency algorithm */
#define CONFIG_K0SHIFT	0
#define CONFIG_K0_CACHED	0x3 /* KSEG0一致性算法：使用高速缓存 */
#define CONFIG_K0_UNCACHED	0x2 /* KSEG0一致性算法：不使用高速缓存 */
#define K0_UNCACHED_ACCELERATED 0x7 /* KSEG0一致性算法：不使用高速缓存，加速访问 */

#ifndef ASM_LANGUAGE

#include <reworks/types.h>

#ifdef _BAO_MOD

u32 get_c0_config();
void set_c0_config(u32);
u32 get_c0_cause();
u32 get_c0_perf_ctl();

u32 read_c0_index();
u64 read_c0_entryhi();
u64 read_c0_entrylo0();
u64 read_c0_entrylo1();
u64 read_c0_pagemask();
void write_c0_pagemask(u64);
void write_c0_index(u32);
void write_c0_entryhi(u64);
void write_c0_entrylo0(u64);
void write_c0_entrylo1(u64);
u64 read_c0_badvaddr();
void write_c0_random(u32);
void write_c0_wired(u32);
u64 read_c0_watchlo();
void write_c0_watchlo(u64);


#endif /*_BAO_MOD*/

void set_watchpoint(void *watch_addr, boolean r, boolean w);
void unset_watchpoint();

#define TLB_INDEX_MAX		0x0000003f
#define TLB_ENTRIES   256        /* set to size of largest supported core */

#define RESET_EXC_VEC		0xffffffffbfc00000 //lvchen
#define TLB_REFILL_EXC_VEC	0xffffffff80000000
#define XTLB_REFILL_EXC_VEC	0xffffffff80000080
#define CACHE_ERR_EXC_VEC	0xffffffff80000100
#define GEN_EXC_VEC			0xffffffff80000180

void fill_exc_vec_code(void *exc_vec,
		void *exc_handler_start, void *exc_handler_end);
int cpu_id_get();
#endif /* #ifndef ASM_LANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* CPU_H */
