/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了华睿3号中断控制器相关寄存器及中断定义
 * 修改：
 * 		  2020-09-07，符凯，创建 
 */

#ifndef __HUARUI2_INTC_DEFS_H_
#define __HUARUI2_INTC_DEFS_H_

#ifdef __cplusplus
extern "C"
{
#endif

#define HR3_INT_START   0

/**
 * 华睿3号 外部64个中断源 + 内部2个中断源
 */

#ifndef _HC3020B_
#define  _HC3020B_  /*ldf  20230921 add:: 临时添加*/
#endif

#ifndef _HC3020B_
typedef enum _HR3_INT
{
	INT_UART0 = HR3_INT_START,	/*(0)*/
 	INT_UART1,					/*(1)*/
 	INT_I2C0,					/*(2)*/
 	INT_I2C1,	    			/*(3)*/
 	INT_SPI0,	    			/*(4)*/
 	INT_SPI1,	    			/*(5)*/
 	INT_CAN0,	    			/*(6)*/
 	INT_CAN1,	    			/*(7)*/
 	INT_RTC,    				/*(8)*/
 	INT_GPIO_TMP_MS0,			   		/*(9)*/
 	INT_GPIO_TMP_MS1,	   		/*(10)*/
 	INT_GPIO_TMP_MS2,	   		/*(11)*/
 	INT_GPIO_TMP_MS3,	   		/*(12)*/
 	INT_GPIO_PWM_MS0,	   		/*(13)*/
 	INT_GPIO_PWM_MS1,	    		/*(14)*/
 	INT_GPIO_PWM_MS2,				/*(15)*/
 	INT_GPIO_PWM_MS3,	    		/*(16)*/
 	INT_GPIO_MS0,	    		/*(17)*/
 	INT_GPIO_MS1,	    		/*(18)*/
 	INT_GPIO_MS2,	    		/*(19)*/
 	INT_GPIO_MS3,	    		/*(20)*/
 	INT_GPIO_MS4,	    		/*(21)*/
 	INT_GPIO_MS5,	    		/*(22)*/
 	INT_GPIO_MS6,	    			/*(23)*/
 	INT_GPIO_MS7,	    			/*(24)*/
 	INT_GPIO_MS8,	    			/*(25)*/
 	INT_GPIO_MS9,	    		/*(26)*/
 	INT_GPIO_MS10,				/*(27)*/
 	INT_GPIO_MS11,	    			/*(28)*/
 	INT_GPIO_MS12,	    			/*(29)*/
 	INT_GPIO_MS13,	    			/*(30)*/
 	INT_GPIO_MS14,	    			/*(31)*/
 	INT_GPIO_MS15,	    			/*(32)*/
 	INT_GPIO_MS16,	    			/*(33)*/
 	INT_GPIO_MS17,	    			/*(34)*/
 	INT_GPIO_MS18,	    			/*(35)*/
 	INT_GPIO_MS19,	    			/*(36)*/
 	INT_GPIO_MS20,	    			/*(37)*/
 	INT_GPIO_MS21,	    			/*(38)*/
 	INT_GPIO_MS22,	    			/*(39)*/
 	INT_GPIO_MS23,	    			/*(40)*/
 	INT_WDOG,	    			/*(41)*/
 	INT_TIMER0,	    			/*(42)*/
 	INT_TIMER1,	    			/*(43)*/
 	INT_DSP0,	    		/*(44)*/
 	INT_DSP1,	    			/*(45)*/
 	INT_DSP2,	    		/*(46)*/
 	INT_DSP3,	  			/*(47)*/
 	INT_DSP4,    				/*(48)*/
 	INT_DSP5,   			/*(49)*/
 	INT_DSP6,    				/*(50)*/
 	INT_DSP7,    				/*(51)*/
 	O_PVT0_INT0, 					/*(52)*/
 	O_PVT0_INT1, 					/*(53)*/
 	O_PVT0_INT2, 					/*(54)*/
 	O_PVT0_INT3, 					/*(55)*/
 	O_PVT0_INT4,   			/*(56)*/
 	O_PVT0_INT5,   			/*(57)*/
 	O_PVT0_INT6,   			/*(58)*/
 	O_PVT0_INT7,   			/*(59)*/
 	INT_IPC,   			/*(60)*/
 	INT_SERIRQ,   			/*(61)*/
 	INT_QSPI,   			/*(62)*/
 	INT_NAND,   			/*(63)*/
	INT_SD0,   			/*(64)*/
 	INT_SD1,   			/*(65)*/
 	INT_PRAB0_PHY,	    			/*(66)*/
 	INT_RAB0,	    			/*(67)*/
 	INT_PRAB0_PCIEX1,	    			/*(68)*/
 	INT_PRAB0_PCIEX4,	    			/*(69)*/
 	INT_PRAB1_PHY,	    			/*(70)*/
 	INT_RAB1,	    			/*(71)*/
 	INT_PRAB1_PCIEX1,	    			/*(72)*/
 	INT_PRAB1_PCIEX4,	    			/*(73)*/
 	INT_LINK0,	    			/*(74)*/
 	INT_LINK1,	    			/*(75)*/
 	INT_LINK2,	    			/*(76)*/
 	INT_LINK3,	    			/*(77)*/
 	INT_PCIE_PHY,	    			/*(78)*/
 	INT_GMAC0,			/*(79)*/
 	INT_GMAC1,		/*(80)*/
 	INT_GMAC2, 			/*(81)*/		/*FuKai,2020-9-9 以时钟中断为界，后面的都是片内中断*/
 	INT_GMAC3,			/*(82)*/
 	INT_MSIO_LINK0,			/*(83)*/
 	INT_MSIO_LINK1,			/*(84)*/
 	INT_MSIO_PHY,			/*(85)*/
 	INT_RASP0_ERR_AI,			/*(86)*/
 	INT_RASP0_DONE_AI,			/*(87)*/
 	INT_RASP0_ERR_DSP,			/*(88)*/
 	INT_RASP0_DONE_DSP,			/*(89)*/
 	INT_RASP1_ERR_AI,			/*(90)*/
 	INT_RASP1_DONE_AI,			/*(91)*/
 	INT_RASP1_ERR_DSP,			/*(92)*/
 	INT_RASP1_DONE_DSP,			/*(93)*/
 	INT_RASP2_ERR_AI,			/*(94)*/
 	INT_RASP2_DONE_AI,			/*(95)*/
 	INT_RASP2_ERR_DSP,			/*(96)*/
 	INT_RASP2_DONE_DSP,			/*(97)*/
 	INT_RASP3_ERR_AI,			/*(98)*/
 	INT_RASP3_DONE_AI,			/*(99)*/
 	INT_RASP3_ERR_DSP,			/*(100)*/
 	INT_RASP3_DONE_DSP,			/*(101)*/
 	INT_GPDMA,			/*(102)*/
 	INT_CONTROLLER0,			/*(103)*/
 	INT_CONTROLLER1,			/*(104)*/
 	INT_UNCORRECTABLE_IRQ,			/*(105)*/
 	INT_CORRECTABLE_IRQ,			/*(106)*/
 	INT_SRAM_ECCERR0_INTSET_I,			/*(107)*/
 	INT_SRAM_ECCERR1_INTSET_I,			/*(108)*/
 	INT_RESV0,			/*(109)*/
 	INT_RESV1,			/*(110)*/
 	INT_RESV2,			/*(111)*/
 	INT_RESV3,			/*(112)*/
 	INT_RESV4,			/*(113)*/
 	INT_RESV5,			/*(114)*/
 	INT_RESV6,			/*(115)*/
 	INT_RESV7,			/*(116)*/
 	INT_RESV8,			/*(117)*/
 	INT_RESV9,			/*(118)*/
 	INT_RESV10,			/*(119)*/
 	INT_RESV11,			/*(120)*/
 	INT_RESV12,			/*(121)*/
 	INT_RESV13,			/*(122)*/
 	INT_RESV14,			/*(123)*/
 	INT_RESV15,			/*(124)*/
 	INT_RESV16,			/*(125)*/
 	INT_RESV17,			/*(126)*/
 	INT_RESV18,			/*(127)*/
 	INT_CLOCK, 		/*(128)*/			/*FuKai,2020-9-9 以时钟中断为界，后面的都是片内中断*/
 	INT_IPI,       /*(129)*/
 	HR3_INT_END
}HR3_INT;
#else /*ldf 20230920 add:: for 3020b*/
typedef enum _HR3_INT
{
	INT_UART0 = HR3_INT_START,	/*(0)*/
 	INT_UART1,					/*(1)*/
 	INT_UART2,					/*(2)*/
 	INT_UART3,					/*(3)*/
 	INT_UART4,					/*(4)*/
 	INT_UART5,					/*(5)*/
 	INT_UART6,					/*(6)*/
 	INT_UART7,					/*(7)*/
 	INT_I2C0,					/*(8)*/
 	INT_I2C1,	    			/*(9)*/
 	INT_SPI0,	    			/*(10)*/
 	INT_SPI1,	    			/*(11)*/
 	INT_CAN0,	    			/*(12)*/
 	INT_CAN1,	    			/*(13)*/
 	INT_RTC,    				/*(14)*/
 	INT_GPIO_TMP_MS0,			/*(15)*/
 	INT_GPIO_TMP_MS1,	   		/*(16)*/
 	INT_GPIO_TMP_MS2,	   		/*(17)*/
 	INT_GPIO_TMP_MS3,	   		/*(18)*/
 	INT_GPIO_PWM_MS0,	   		/*(19)*/
 	INT_GPIO_PWM_MS1,	    	/*(20)*/
 	INT_GPIO_PWM_MS2,			/*(21)*/
 	INT_GPIO_PWM_MS3,	    	/*(22)*/
 	INT_GPIO_MS0,	    		/*(23)*/
 	INT_GPIO_MS1,	    		/*(24)*/
 	INT_GPIO_MS2,	    		/*(25)*/
 	INT_GPIO_MS3,	    		/*(26)*/
 	INT_GPIO_MS4,	    		/*(27)*/
 	INT_GPIO_MS5,	    		/*(28)*/
 	INT_GPIO_MS6,	    		/*(29)*/
 	INT_GPIO_MS7,	    		/*(30)*/
 	INT_GPIO_MS8,	    		/*(31)*/
 	INT_GPIO_MS9,	    		/*(32)*/
 	INT_GPIO_MS10,				/*(33)*/
 	INT_GPIO_MS11,	    		/*(34)*/
 	INT_GPIO_MS12,	    		/*(35)*/
 	INT_GPIO_MS13,	    		/*(36)*/
 	INT_GPIO_MS14,	    		/*(37)*/
 	INT_GPIO_MS15,	    		/*(38)*/
 	INT_GPIO_MS16,	    		/*(39)*/
 	INT_GPIO_MS17,	    		/*(40)*/
 	INT_GPIO_MS18,	    		/*(41)*/
 	INT_GPIO_MS19,	    		/*(42)*/
 	INT_GPIO_MS20,	    		/*(43)*/
 	INT_GPIO_MS21,	    		/*(44)*/
 	INT_GPIO_MS22,	    		/*(45)*/
 	INT_GPIO_MS23,	    		/*(46)*/
 	INT_WDOG0,	    			/*(47)*/
 	INT_WDOG1,	    			/*(48)*/
 	INT_WDOG2,	    			/*(49)*/
 	INT_WDOG3,	    			/*(50)*/
 	INT_TIMER0,	    			/*(51)*/
 	INT_TIMER1,	    			/*(52)*/
 	INT_TIMER2,	    			/*(53)*/
	INT_TIMER3,	    			/*(54)*/
	INT_TIMER4,	    			/*(55)*/
	INT_TIMER5,	    			/*(56)*/
	INT_TIMER6,	    			/*(57)*/
	INT_TIMER7,	    			/*(58)*/
 	INT_DSP0,	    			/*(59)*/
 	INT_DSP1,	    			/*(60)*/
 	O_PVT0_INT0, 				/*(61)*/
 	O_PVT0_INT1, 				/*(62)*/
 	INT_IPC,   					/*(63)*/
 	INT_SERIRQ,   				/*(64)*/
 	INT_QSPI,   				/*(65)*/
	INT_SD0,   					/*(66)*/
 	INT_SD1,   					/*(67)*/
 	INT_LBC,   					/*(68)*/
 	INT_PRAB0_PHY,	    		/*(69)*/
 	INT_RAB0,	    			/*(70)*/
 	INT_PRAB0_PCIEX1,	    	/*(71)*/
 	INT_PRAB0_PCIEX4,	    	/*(72)*/
 	INT_PRAB1_PHY,	    		/*(73)*/
 	INT_RAB1,	    			/*(74)*/
 	INT_PRAB1_PCIEX1,	    	/*(75)*/
 	INT_PRAB1_PCIEX4,	    	/*(76)*/
 	INT_GMAC0,					/*(77)*/
 	INT_GMAC1,					/*(78)*/
 	INT_RASP_ERR,				/*(79)*/
 	INT_RASP_DON,				/*(80)*/
 	INT_GPDMA,					/*(81)*/
 	INT_CONTROLLER,				/*(82)*/
 	INT_UNCORRECTABLE_IRQ,		/*(83)*/
 	INT_CORRECTABLE_IRQ,		/*(84)*/
 	INT_AICORE_DONE,			/*(85)*/
 	INT_AICORE_ERR,				/*(86)*/
 	INT_SRAM_ECCERR_INTSET_I,	/*(87)*/
 	
 	INT_RESV0,			/*(88)*/
 	INT_RESV1,			/*(89)*/
 	INT_RESV2,			/*(90)*/
 	INT_RESV3,			/*(91)*/
 	INT_RESV4,			/*(92)*/
 	INT_RESV5,			/*(93)*/
 	INT_RESV6,			/*(94)*/
 	INT_RESV7,			/*(95)*/
 	INT_RESV8,			/*(96)*/
 	INT_RESV9,			/*(97)*/
 	INT_RESV10,			/*(98)*/
 	INT_RESV11,			/*(99)*/
 	INT_RESV12,			/*(100)*/
 	INT_RESV13,			/*(101)*/
 	INT_RESV14,			/*(102)*/
 	INT_RESV15,			/*(103)*/
 	INT_RESV16,			/*(104)*/
 	INT_RESV17,			/*(105)*/
 	INT_RESV18,			/*(106)*/
  	INT_RESV19,			/*(107)*/
  	INT_RESV20,			/*(108)*/
  	INT_RESV21,			/*(109)*/
  	INT_RESV22,			/*(110)*/
  	INT_RESV23,			/*(111)*/
  	INT_RESV24,			/*(112)*/
  	INT_RESV25,			/*(113)*/
  	INT_RESV26,			/*(114)*/
  	INT_RESV27,			/*(115)*/
  	INT_RESV28,			/*(116)*/
  	INT_RESV29,			/*(117)*/
	INT_RESV30,			/*(118)*/
  	INT_RESV31,			/*(119)*/
  	INT_RESV32,			/*(120)*/
  	INT_RESV33,			/*(121)*/
  	INT_RESV34,			/*(122)*/
  	INT_RESV35,			/*(123)*/
  	INT_RESV36,			/*(124)*/
  	INT_RESV37,			/*(125)*/	
  	
 	INT_IPI0, 			/*(126)*/
 	INT_IPI1,       	/*(127)*/
 	
 	INT_CLOCK, 			/*(128)*/
 	HR3_INT_END
}HR3_INT;

#endif

#define HR3_INTERNAL_INT_START  INT_CLOCK
#define HR3_INTERNAL_INT_END    HR3_INT_END
#define HR3_EXTERNAL_INT_START  HR3_INT_START
#define HR3_EXTERNAL_INT_END    INT_CLOCK
/**
 * 华睿3号内外部中断源数目
 */
#define HR3_INT_CNT        		((int)(HR3_INT_END-HR3_INT_START))
#define HR3_EXTERNAL_INT_CNT  	((int)(HR3_EXTERNAL_INT_END-HR3_EXTERNAL_INT_START))
#define HR3_INTERNAL_INT_CNT  	((int)(HR3_INTERNAL_INT_END-HR3_INTERNAL_INT_START))
#define IS_HR3_INT_VALID(irq)  (((irq) >= HR3_INT_START) && ((irq) < HR3_INT_END))
#define IS_HR3_INT_EXTERNAL(irq)   (((irq) >= HR3_EXTERNAL_INT_START) && ((irq) < HR3_EXTERNAL_INT_END))
#define IS_HR3_INT_INTERNAL(irq)   (((irq) >= HR3_INTERNAL_INT_START) && ((irq) < HR3_INTERNAL_INT_END))

//extern char* g_hr3_int_name[HR3_INT_CNT];
extern inline const char * const get_hr3_int_name(HR3_INT irq);


/* defines */
#define INTERRUPT_BASE_ADDR				0x1f930000
#define INTERRUPT_SEL_REG(x)         	(INTERRUPT_BASE_ADDR+(x<<3))

#define INTERRUPT_ENABLE_L_REG         	(INTERRUPT_BASE_ADDR+0x400)
#define INTERRUPT_ENABLE_H_REG         	(INTERRUPT_BASE_ADDR+0x408)

#define INTERRUPT_MASK_L_REG            (INTERRUPT_BASE_ADDR+0x410)
#define INTERRUPT_MASK_H_REG            (INTERRUPT_BASE_ADDR+0x418)

#define INTERRUPT_MODE_L_REG            (INTERRUPT_BASE_ADDR+0x420)
#define INTERRUPT_MODE_H_REG            (INTERRUPT_BASE_ADDR+0x428)

#define INTERRUPT_CLR_L_REG         		(INTERRUPT_BASE_ADDR+0x430)
#define INTERRUPT_CLR_H_REG         		(INTERRUPT_BASE_ADDR+0x438)

#define INTERRUPT_STAT_L_REG         	(INTERRUPT_BASE_ADDR+0x440)
#define INTERRUPT_STAT_H_REG         	(INTERRUPT_BASE_ADDR+0x448)

#define INTERRUPT_RAWSTA_L_REG        (INTERRUPT_BASE_ADDR+0x450)
#define INTERRUPT_RAWSTA_H_REG        (INTERRUPT_BASE_ADDR+0x458)

#define INTERRUPT_REQSTA_L_REG         (INTERRUPT_BASE_ADDR+0x460)
#define INTERRUPT_REQSTA_H_REG         (INTERRUPT_BASE_ADDR+0x468)

#define INTERRUPT_PIC_INDEX_REG         (INTERRUPT_BASE_ADDR+0x470)/*ldf 20230921 add:: for hc3020b*/

#define INTERRUPT_INTERVAL				4

#define PIC_INT_SEL(index)    (INTERRUPT_BASE_ADDR  + 8 * (index))         /*  中断路由寄存器           Add by gongchao 2022.09.01    */
//#define INTENSET_REG          (0x1f0781428) /*32位中断使能设置寄存器*/



#define R_IMR_MAILBOX_READ_CPU		0x0000 /* 0x1000 liuw, 20140117 */
#define R_IMR_MAILBOX_ENABLE_CPU	0x0008 /* 0x1008,liuw, 20140117 */
#define R_IMR_MAILBOX_SET_CPU		0x0010 /* 0x1010 liuw, 20140117 */
#define R_IMR_MAILBOX_CLR_CPU		0x0018 /* 0x100c liuw, 20140117 */
#define R_IMR_MAILBOX0_CPU          0x0020 /* added by liuw 20140120 */

#define MAILBOX_READ                    R_IMR_MAILBOX_READ_CPU
#define MAILBOX_ENABLE					R_IMR_MAILBOX_ENABLE_CPU /* added by liuw, 20140117 */
#define MAILBOX_SET                     R_IMR_MAILBOX_SET_CPU
#define MAILBOX_CLR                     R_IMR_MAILBOX_CLR_CPU
#define MAILBOX0_BASE                   R_IMR_MAILBOX0_CPU

#define A_IMR_CPU0_BASE             0x1f078100 /* 0x0010020000 by liuw 20140117 */
#define A_IMR_CPU1_BASE             0x1f078200 /* 0x0010022000 by liuw 20140117  */
#define A_IMR_CPU2_BASE             0x1f078300 /* 0x0010024000 by liuw 20140117  */
#define A_IMR_CPU3_BASE             0x1f078400 /* 0x0010026000 by liuw 20140117  */
#define IMR_REGISTER_SPACING        0x100 /* 0x2000 by liuw, 20140117 */
#define IMR_REGISTER_SPACING_SHIFT  8 /* 13 by liuw 20140117 */

#define IPI_INTR_ID_CPC		0

#define A_IMR_MAPPER(cpu)       (A_IMR_CPU0_BASE+(cpu)*IMR_REGISTER_SPACING)
#define A_IMR_REGISTER(cpu,reg) (A_IMR_MAPPER(cpu)+(reg))

#define IMR_REGISTER(cpunum, reg)       /*PHYS_TO_K1 20091211*/(A_IMR_REGISTER(cpunum,(reg)))
		
#define SMP_CPC_MAILBOX_INT_BIT		(1UL<<16) /* (1LL<<48) yinwx, 20091219 */
#define MAILBOX_INT_BIT(ipi)		(1UL<<ipi) /* (1LL<<ipi) yinwx, 20091219 */



#ifdef __cplusplus
}
#endif

#endif /* __HUARUI2_INTC_DEFS_H_ */
