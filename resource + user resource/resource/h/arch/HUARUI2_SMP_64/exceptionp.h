/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了ReWorks CSP模块异常相关接口  
 * 修改：
 * 		 2013-01-06，唐立三，建立 
 */
#ifndef _MIPS_EXCEPTION_PRIVATE_H_
#define _MIPS_EXCEPTION_PRIVATE_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <exception.h>
	
/**
 * 异常信息头
 */
#define EXC_PRINT_HEADER(tid, exc_code, cpu)	\
	do {\
		printk("\n-----------------Exception Information----------------------\n");	\
		printk("\t---------------Exception Cause--------------\n");	\
		printk("EXCEPTION CPU  : %d\n", \
				cpu); \
		printk("EXCEPTION TID  : 0x%x, %s\n", \
				tid, \
				thread_name((thread_t)tid));\
		printk("EXCEPTION CODE : %d\n", exc_code);	\
		printk("EXCEPTION NAME : ");	\
	}while(0);
	

/* 异常消息 */
struct exc_internal_msg
{
	struct exc_report_msg	emsg;		/* 公开消息 */
	Context_Ctrl 			context;	/* 任务上下文 */	
	u64						badvaddr;	/* 保存错误地址 */
	u32                     cause_reg;  /*cause寄存器值*/
};


/**
 * 异常信息显示任务名称
 */
typedef void (* EXC_MSG_RECV)(Context_Ctrl *context, thread_t tid, int exc_code, int cpu_id, u64 badaddr,u32 cause);
extern EXC_MSG_RECV exc_msg_recv_handler;

/**
 * 异常信息输出标识
 */
#define EXC_SEND_MSG		0x00000001
#define EXC_BACK_TRACE		0x00000002
#define EXC_WAIT_FOREVER	0x00000004
#define EXC_ACCESS_ADDR		0x00000008	
#define EXC_REG_DISPLAY		0x00000010	
#define EXC_ERROR_ADDR		0x00000020
#define EXC_UNALIGNED_FIX	0x00000040
#define EXC_PRINT			0x00000080

#define EXC_TOTAL	42

extern u32 exception_options ;
	
#ifdef __cplusplus
}
#endif	

#endif	
