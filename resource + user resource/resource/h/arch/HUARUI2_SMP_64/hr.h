//#define _GCC_HR_H
#ifndef _GCC_HR_H
#define _GCC_HR_H

#if !defined(__mips_hr_vector_rev)
# error "You must select -march=hr2 or -march=hr2 to use hr.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

typedef uint8_t uint8x32_t __attribute__((vector_size (32)));
typedef int8_t int8x32_t __attribute__((vector_size (32)));
typedef uint16_t uint16x16_t __attribute__((vector_size (32)));
typedef int16_t int16x16_t __attribute__((vector_size (32)));
typedef uint32_t uint32x8_t __attribute__((vector_size (32)));
typedef int32_t int32x8_t __attribute__((vector_size (32)));
typedef uint64_t uint64x4_t __attribute__((vector_size (32)));
typedef int64_t int64x4_t __attribute__((vector_size (32)));
typedef uint64_t uint64x2_t __attribute__((vector_size (16)));
typedef int64_t int64x2_t __attribute__((vector_size (16)));
//typedef (unsigned __int128_t) uint128x2_t __attribute__((vector_size (32)));
//typedef __int128_t int128x2_t __attribute__((vector_size (32)));

typedef uint8_t uint8x16_t __attribute__((vector_size (16)));
typedef int8_t int8x16_t __attribute__((vector_size (16)));
typedef uint8_t uint8x8_t __attribute__((vector_size (8)));
typedef int8_t int8x8_t __attribute__((vector_size (8)));
typedef uint8_t uint8x4_t __attribute__((vector_size (4)));
typedef int8_t int8x4_t __attribute__((vector_size (4)));
typedef uint16_t uint16x8_t __attribute__((vector_size (16)));
typedef int16_t int16x8_t __attribute__((vector_size (16)));
typedef uint16_t uint16x4_t __attribute__((vector_size (8)));
typedef int16_t int16x4_t __attribute__((vector_size (8)));
typedef uint32_t uint32x4_t __attribute__((vector_size (16)));
typedef int32_t int32x4_t __attribute__((vector_size (16)));

typedef float floatx1_t __attribute__((vector_size (4)));
typedef float floatx4_t __attribute__((vector_size (16)));
typedef float floatx8_t __attribute__((vector_size (32)));
typedef double doublex1_t __attribute__((vector_size (8)));
typedef double doublex4_t __attribute__((vector_size (32)));

typedef float __m256f __attribute__ ((__vector_size__ (32),
				     __may_alias__));
typedef long long __m256i __attribute__ ((__vector_size__ (32),
					  __may_alias__));
typedef double __m256d __attribute__ ((__vector_size__ (32),
				       __may_alias__));

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpaddb_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vpaddb_u (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpaddh_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vpaddh_u (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpaddw_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vpaddw_u (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpaddd_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vpaddd_u (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpaddb_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vpaddb_s (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpaddh_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vpaddh_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpaddw_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vpaddw_s (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpaddd_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vpaddd_s (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpsubb_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vpsubb_u (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpsubh_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vpsubh_u (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpsubw_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vpsubw_u (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpsubd_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vpsubd_u (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpsubb_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vpsubb_s (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpsubh_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vpsubh_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpsubw_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vpsubw_s (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpsubd_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vpsubd_s (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vphaddb_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vphaddb_u (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vphaddh_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vphaddh_u (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vphaddw_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vphaddw_u (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vphaddd_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vphaddd_u (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vphaddb_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vphaddb_s (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vphaddh_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vphaddh_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vphaddw_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vphaddw_s (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vphaddd_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vphaddd_s (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vphsubb_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vphsubb_u (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vphsubh_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vphsubh_u (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vphsubw_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vphsubw_u (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vphsubd_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vphsubd_u (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vphsubb_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vphsubb_s (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vphsubh_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vphsubh_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vphsubw_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vphsubw_s (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vphsubd_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vphsubd_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpsums2h1ws (int16x16_t s, int32x8_t t)
{
  return __builtin_hr_vpsums2h1ws (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpsums4b1ws (int8x32_t s, int32x8_t t)
{
  return __builtin_hr_vpsums4b1ws (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpsumu2h1ws (uint16x16_t s, uint32x8_t t)
{
  return __builtin_hr_vpsumu2h1ws (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpsumu4b1ws (uint8x32_t s, uint32x8_t t)
{
  return __builtin_hr_vpsumu4b1ws (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpaddbs (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vpaddbs (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpaddhs (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vpaddhs (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpaddws (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vpaddws (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpaddds (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vpaddds (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vphaddbs (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vphaddbs (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vphaddhs (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vphaddhs (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vphaddws (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vphaddws (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vphaddds (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vphaddds (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpsubbs (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vpsubbs (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpsubhs (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vpsubhs (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpsubws (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vpsubws (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpsubds (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vpsubds (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vphsubbs (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vphsubbs (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vphsubhs (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vphsubhs (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vphsubws (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vphsubws (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vphsubds (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vphsubds (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpaddubs (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vpaddubs (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpadduhs (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vpadduhs (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpadduws (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vpadduws (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpadduds (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vpadduds (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vphaddubs (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vphaddubs (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vphadduhs (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vphadduhs (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vphadduws (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vphadduws (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vphadduds (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vphadduds (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpsububs (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vpsububs (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpsubuhs (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vpsubuhs (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpsubuws (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vpsubuws (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpsubuds (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vpsubuds (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vphsububs (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vphsububs (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vphsubuhs (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vphsubuhs (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vphsubuws (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vphsubuws (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vphsubuds (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vphsubuds (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpminub (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vpminub (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpminuh (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vpminuh (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpminuw (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vpminuw (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpminud (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vpminud (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpcmpgtsb (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vpcmpgtsb (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpcmpgtsh (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vpcmpgtsh (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpcmpgtsw (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vpcmpgtsw (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpcmpgtsd (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vpcmpgtsd (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpcmpgtub (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vpcmpgtub (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpcmpgtuh (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vpcmpgtuh (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpcmpgtuw (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vpcmpgtuw (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpcmpgtud (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vpcmpgtud (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpcmpltsb (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vpcmpltsb (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpcmpltsh (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vpcmpltsh (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpcmpltsw (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vpcmpltsw (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpcmpltsd (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vpcmpltsd (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpcmpltub (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vpcmpltub (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpcmpltuh (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vpcmpltuh (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpcmpltuw (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vpcmpltuw (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpcmpltud (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vpcmpltud (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpcmpgtssb (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vpcmpgtssb (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpcmpgtssh (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vpcmpgtssh (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpcmpgtssw (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vpcmpgtssw (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpcmpgtssd (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vpcmpgtssd (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpcmpgtsub (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vpcmpgtsub (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpcmpgtsuh (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vpcmpgtsuh (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpcmpgtsuw (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vpcmpgtsuw (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpcmpgtsud (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vpcmpgtsud (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpcmpltssb (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vpcmpltssb (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpcmpltssh (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vpcmpltssh (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpcmpltssw (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vpcmpltssw (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpcmpltssd (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vpcmpltssd (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpcmpltsub (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vpcmpltsub (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpcmpltsuh (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vpcmpltsuh (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpcmpltsuw (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vpcmpltsuw (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpcmpltsud (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vpcmpltsud (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpminsb (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vpminsb (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpminsh (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vpminsh (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpminsw (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vpminsw (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpminsd (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vpminsd (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpmaxub (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vpmaxub (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpmaxuh (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vpmaxuh (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpmaxuw (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vpmaxuw (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpmaxud (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vpmaxud (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpcmpeqb_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vpcmpeqb_u (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpcmpeqh_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vpcmpeqh_u (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpcmpeqw_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vpcmpeqw_u (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpcmpeqd_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vpcmpeqd_u (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpcmpeqb_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vpcmpeqb_s (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpcmpeqh_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vpcmpeqh_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpcmpeqw_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vpcmpeqw_s (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpcmpeqd_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vpcmpeqd_s (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpcmpeqsb_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vpcmpeqsb_u (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpcmpeqsh_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vpcmpeqsh_u (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpcmpeqsw_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vpcmpeqsw_u (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpcmpeqsd_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vpcmpeqsd_u (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpcmpeqsb_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vpcmpeqsb_s (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpcmpeqsh_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vpcmpeqsh_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpcmpeqsw_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vpcmpeqsw_s (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpcmpeqsd_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vpcmpeqsd_s (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpmaxsb (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vpmaxsb (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpmaxsh (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vpmaxsh (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpmaxsw (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vpmaxsw (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpmaxsd (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vpmaxsd (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpsignb (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vpsignb (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpsignh (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vpsignh (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpsignw (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vpsignw (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpsignd (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vpsignd (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpabsb (int8x32_t s)
{
  return __builtin_hr_vpabsb (s);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpabsh (int16x16_t s)
{
  return __builtin_hr_vpabsh (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpabsw (int32x8_t s)
{
  return __builtin_hr_vpabsw (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpabsd (int64x4_t s)
{
  return __builtin_hr_vpabsd (s);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpavgb (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vpavgb (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpavgh (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vpavgh (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpavgw (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vpavgw (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpavgd (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vpavgd (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpsadbh (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vpsadbh (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpsadhw (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vpsadhw (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpmullb (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vpmullb (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpmullh (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vpmullh (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpmullw (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vpmullw (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpmulhb (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vpmulhb (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpmulhh (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vpmulhh (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpmulhw (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vpmulhw (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpmullub (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vpmullub (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpmulluh (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vpmulluh (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpmulluw (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vpmulluw (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpmulhub (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vpmulhub (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpmulhuh (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vpmulhuh (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpmulhuw (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vpmulhuw (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpmulhxl (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vpmulhxl (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpmulwxl (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vpmulwxl (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpmuldxl (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vpmuldxl (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpmuluhxl (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vpmuluhxl (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpmuluwxl (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vpmuluwxl (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpmuludxl (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vpmuludxl (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpmulhxh (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vpmulhxh (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpmulwxh (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vpmulwxh (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpmuldxn (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vpmuldxh (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpmuluhxh (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vpmuluhxh (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpmuluwxh (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vpmuluwxh (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpmuludxh (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vpmuludxh (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpmaddbhs (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vpmaddbhs (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpmaddhws (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vpmaddhws (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpmaddwds (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vpmaddwds (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpmaddubshs (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vpmaddubshs (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpmadduhsws (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vpmadduhsws (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpmadduwsds (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vpmadduwsds (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpmulhrsh (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vpmulhrsh (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vaddps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vaddps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vaddpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vaddpd (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vsubps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vsubps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vsubpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vsubpd (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vmulps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vmulps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vmulpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vmulpd (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vmuladdps (floatx8_t s, floatx8_t t, floatx8_t r)
{
  return __builtin_hr_vmuladdps (s, t, r);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vmuladdpd (doublex4_t s, doublex4_t t, doublex4_t r)
{
  return __builtin_hr_vmuladdpd (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vmulsubps (floatx8_t s, floatx8_t t, floatx8_t r)
{
  return __builtin_hr_vmulsubps (s, t, r);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vmulsubpd (doublex4_t s, doublex4_t t, doublex4_t r)
{
  return __builtin_hr_vmulsubpd (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vnmuladdps (floatx8_t s, floatx8_t t, floatx8_t r)
{
  return __builtin_hr_vnmuladdps (s, t, r);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vnmuladdpd (doublex4_t s, doublex4_t t, doublex4_t r)
{
  return __builtin_hr_vnmuladdpd (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vnmulsubps (floatx8_t s, floatx8_t t, floatx8_t r)
{
  return __builtin_hr_vnmulsubps (s, t, r);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vnmulsubpd (doublex4_t s, doublex4_t t, doublex4_t r)
{
  return __builtin_hr_vnmulsubpd (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vhaddps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vhaddps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vhaddpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vhaddpd (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vhsubps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vhsubps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vhsubpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vhsubpd (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vaddsubps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vaddsubps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vaddsubpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vaddsubpd (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vsubaddps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vsubaddps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vsubaddpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vsubaddpd (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vmaddsubps (floatx8_t s, floatx8_t t, floatx8_t r)
{
  return __builtin_hr_vmaddsubps (s, t, r);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vmaddsubpd (doublex4_t s, doublex4_t t, doublex4_t r)
{
  return __builtin_hr_vmaddsubpd (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vmsubaddps (floatx8_t s, floatx8_t t, floatx8_t r)
{
  return __builtin_hr_vmsubaddps (s, t, r);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vmsubaddpd (doublex4_t s, doublex4_t t, doublex4_t r)
{
  return __builtin_hr_vmsubaddpd (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vminps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vminps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vminpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vminpd (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vmaxps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vmaxps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vmaxpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vmaxpd (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vnegps (floatx8_t s)
{
  return __builtin_hr_vnegps (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vnegpd (doublex4_t s)
{
  return __builtin_hr_vnegpd (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vabsps (floatx8_t s)
{
  return __builtin_hr_vabsps (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vabspd (doublex4_t s)
{
  return __builtin_hr_vabspd (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vdivps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vdivps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vdivpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vdivpd (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vrcpps (floatx8_t s)
{
  return __builtin_hr_vrcpps (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vrcppd (doublex4_t s)
{
  return __builtin_hr_vrcppd (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vsqrtps (floatx8_t s)
{
  return __builtin_hr_vsqrtps (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vsqrtpd (doublex4_t s)
{
  return __builtin_hr_vsqrtpd (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vrsqrtps (floatx8_t s)
{
  return __builtin_hr_vrsqrtps (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vrsqrtpd (doublex4_t s)
{
  return __builtin_hr_vrsqrtpd (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vsinps (floatx8_t s)
{
  return __builtin_hr_vsinps (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcosps (floatx8_t s)
{
  return __builtin_hr_vcosps (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vatanps (floatx8_t s)
{
  return __builtin_hr_vatanps (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vpolarlps (floatx8_t s)
{
  return __builtin_hr_vpolarlps (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vpolarhps (floatx8_t s)
{
  return __builtin_hr_vpolarhps (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vexp2ps (floatx8_t s)
{
  return __builtin_hr_vexp2ps (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vlog2ps (floatx8_t s)
{
  return __builtin_hr_vlog2ps (s);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpandb_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vpandb_u (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpandh_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vpandh_u (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpandw_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vpandw_u (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpandd_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vpandd_u (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpandb_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vpandb_s (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpandh_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vpandh_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpandw_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vpandw_s (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpandd_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vpandd_s (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vpandps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vpandps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vpandpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vpandpd (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vporb_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vporb_u (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vporh_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vporh_u (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vporw_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vporw_u (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpord_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vpord_u (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vporb_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vporb_s (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vporh_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vporh_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vporw_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vporw_s (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpord_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vpord_s (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vporps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vporps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vporpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vporpd (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpxorb_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vpxorb_u (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpxorh_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vpxorh_u (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpxorw_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vpxorw_u (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpxord_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vpxord_u (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpxorb_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vpxorb_s (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpxorh_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vpxorh_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpxorw_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vpxorw_s (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpxord_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vpxord_s (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vpxorps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vpxorps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vpxorpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vpxorpd (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpandnb_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vpandnb_u (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpandnh_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vpandnh_u (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpandnw_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vpandnw_u (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpandnd_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vpandnd_u (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpandnb_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vpandnb_s (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpandnh_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vpandnh_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpandnw_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vpandnw_s (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpandnd_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vpandnd_s (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vpandnps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vpandnps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vpandnpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vpandnpd (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpnorb_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vpnorb_u (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpnorh_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vpnorh_u (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpnorw_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vpnorw_u (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpnord_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vpnord_u (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpnorb_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vpnorb_s (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpnorh_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vpnorh_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpnorw_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vpnorw_s (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpnord_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vpnord_s (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vpnorps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vpnorps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vpnorpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vpnorpd (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpopcntb_u (uint8x32_t s)
{
  return __builtin_hr_vpopcntb_u (s);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpopcnth_u (uint16x16_t s)
{
  return __builtin_hr_vpopcnth_u (s);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpopcntw_u (uint32x8_t s)
{
  return __builtin_hr_vpopcntw_u (s);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpopcntd_u (uint64x4_t s)
{
  return __builtin_hr_vpopcntd_u (s);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpopcntb_s (int8x32_t s)
{
  return __builtin_hr_vpopcntb_s (s);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpopcnth_s (int16x16_t s)
{
  return __builtin_hr_vpopcnth_s (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpopcntw_s (int32x8_t s)
{
  return __builtin_hr_vpopcntw_s (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpopcntd_s (int64x4_t s)
{
  return __builtin_hr_vpopcntd_s (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vpopcntps (floatx8_t s)
{
  return __builtin_hr_vpopcntps (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vpopcntpd (doublex4_t s)
{
  return __builtin_hr_vpopcntpd (s);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vzerob_u (void)
{
  return __builtin_hr_vzerob_u (0);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vzeroh_u (void)
{
  return __builtin_hr_vzeroh_u (0);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vzerow_u (void)
{
  return __builtin_hr_vzerow_u (0);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vzerod_u (void)
{
  return __builtin_hr_vzerod_u (0);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vzerob_s (void)
{
  return __builtin_hr_vzerob_s (0);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vzeroh_s (void)
{
  return __builtin_hr_vzeroh_s (0);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vzerow_s (void)
{
  return __builtin_hr_vzerow_s (0);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vzerod_s (void)
{
  return __builtin_hr_vzerod_s (0);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vzerops (void)
{
  return __builtin_hr_vzerops (0);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vzeropd (void)
{
  return __builtin_hr_vzeropd (0);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vmovb_u (uint8x32_t s)
{
  return __builtin_hr_vmovb_u (s);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vmovh_u (uint16x16_t s)
{
  return __builtin_hr_vmovh_u (s);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vmovw_u (uint32x8_t s)
{
  return __builtin_hr_vmovw_u (s);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vmovd_u (uint64x4_t s)
{
  return __builtin_hr_vmovd_u (s);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vmovb_s (int8x32_t s)
{
  return __builtin_hr_vmovb_s (s);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vmovh_s (int16x16_t s)
{
  return __builtin_hr_vmovh_s (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vmovw_s (int32x8_t s)
{
  return __builtin_hr_vmovw_s (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vmovd_s (int64x4_t s)
{
  return __builtin_hr_vmovd_s (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vmovps (floatx8_t s)
{
  return __builtin_hr_vmovps (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vmovpd (doublex4_t s)
{
  return __builtin_hr_vmovpd (s);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vmovsldup_u (uint32x8_t s)
{
  return __builtin_hr_vmovsldup_u (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vmovsldup_s (int32x8_t s)
{
  return __builtin_hr_vmovsldup_s (s);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vmovshdup_u (uint32x8_t s)
{
  return __builtin_hr_vmovshdup_u (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vmovshdup_s (int32x8_t s)
{
  return __builtin_hr_vmovshdup_s (s);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vmovdldup_u (uint64x4_t s)
{
  return __builtin_hr_vmovdldup_u (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vmovdldup_s (int64x4_t s)
{
  return __builtin_hr_vmovdldup_s (s);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vmovdhdup_u (uint64x4_t s)
{
  return __builtin_hr_vmovdhdup_u (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vmovdhdup_s (int64x4_t s)
{
  return __builtin_hr_vmovdhdup_s (s);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpunpcklbh_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vpunpcklbh_u (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpunpcklbh_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vpunpcklbh_s (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpunpcklhw_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vpunpcklhw_u (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpunpcklhw_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vpunpcklhw_s (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpunpcklwd_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vpunpcklwd_u (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpunpcklwd_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vpunpcklwd_s (s, t);
}


__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpunpckldq_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vpunpckldq_u (s, t);
}


__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpunpckldq_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vpunpckldq_s (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpunpckhbh_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vpunpckhbh_u (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpunpckhbh_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vpunpckhbh_s (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpunpckhhw_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vpunpckhhw_u (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpunpckhhw_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vpunpckhhw_s (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpunpckhwd_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vpunpckhwd_u (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpunpckhwd_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vpunpckhwd_s (s, t);
}



__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpunpckhdq_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vpunpckhdq_u (s, t);
}



__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpunpckhdq_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vpunpckhdq_s (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpackshsbs (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vpackshsbs (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpackswshs (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vpackswshs (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpacksdsws (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vpacksdsws (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpackuhubs (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vpackuhubs (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpackuwuhs (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vpackuwuhs (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpackuduws (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vpackuduws (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpackshubs (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vpackshubs (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpackswuhs (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vpackswuhs (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpacksduws (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vpacksduws (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpackluhubm (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vpackluhubm (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpackluwuhm (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vpackluwuhm (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpackluduwm (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vpackluduwm (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vpackhuhubm (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vpackhuhubm (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpackhuwuhm (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vpackhuwuhm (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpackhuduwm (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vpackhuduwm (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpmovsxbh (int8x32_t s)
{
  return __builtin_hr_vpmovsxbh (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpmovsxbw (int8x32_t s)
{
  return __builtin_hr_vpmovsxbw (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpmovsxbd (int8x32_t s)
{
  return __builtin_hr_vpmovsxbd (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpmovsxhw (int16x16_t s)
{
  return __builtin_hr_vpmovsxhw (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpmovsxhd (int16x16_t s)
{
  return __builtin_hr_vpmovsxhd (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpmovsxwd (int32x8_t s)
{
  return __builtin_hr_vpmovsxwd (s);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vpmovzxbh (uint8x32_t s)
{
  return __builtin_hr_vpmovzxbh (s);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpmovzxbw (uint8x32_t s)
{
  return __builtin_hr_vpmovzxbw (s);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpmovzxbd (uint8x32_t s)
{
  return __builtin_hr_vpmovzxbd (s);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vpmovzxhw (uint16x16_t s)
{
  return __builtin_hr_vpmovzxhw (s);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpmovzxhd (uint16x16_t s)
{
  return __builtin_hr_vpmovzxhd (s);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vpmovzxwd (uint32x8_t s)
{
  return __builtin_hr_vpmovzxwd (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcvtw2ps (int32x8_t s)
{
  return __builtin_hr_vcvtw2ps (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vcvtps2w (floatx8_t s)
{
  return __builtin_hr_vcvtps2w (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcvtd2ps (int64x4_t s)
{
  return __builtin_hr_vcvtd2ps (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vcvtpd2w (doublex4_t s)
{
  return __builtin_hr_vcvtpd2w (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcvtw2pd (int32x8_t s)
{
  return __builtin_hr_vcvtw2pd (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vcvtlps2d (floatx8_t s)
{
  return __builtin_hr_vcvtlps2d (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vcvthps2d (floatx8_t s)
{
  return __builtin_hr_vcvthps2d (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcvtd2pd (int64x4_t s)
{
  return __builtin_hr_vcvtd2pd (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vcvtpd2d (doublex4_t s)
{
  return __builtin_hr_vcvtpd2d (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vcvttps2w (floatx8_t s)
{
  return __builtin_hr_vcvttps2w (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vcvttpd2w (doublex4_t s)
{
  return __builtin_hr_vcvttpd2w (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vcvttps2d (floatx8_t s)
{
  return __builtin_hr_vcvttps2d (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vcvttpd2d (doublex4_t s)
{
  return __builtin_hr_vcvttpd2d (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vroundps (floatx8_t s, const int i)
{
  return __builtin_hr_vroundps (s, i);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vroundpd (doublex4_t s, const int i)
{
  return __builtin_hr_vroundpd (s, i);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsblend (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vcsblend (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcdblend (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vcdblend (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpblendb (int8x32_t s, int8x32_t t, int8x32_t r)
{
  return __builtin_hr_vpblendb (s, t, r);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpermutb (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vpermutb (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpermute (int8x32_t s, int8x32_t t, int8x32_t r)
{
  return __builtin_hr_vpermute (s, t, r);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpalignrib (int8x32_t s, int8x32_t t, const int i)
{
  return __builtin_hr_vpalignrib (s, t, i);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpalignrih (int16x16_t s, int16x16_t t, const int i)
{
  return __builtin_hr_vpalignrih (s, t, i);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpalignriw (int32x8_t s, int32x8_t t, const int i)
{
  return __builtin_hr_vpalignriw (s, t, i);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpalignrid (int64x4_t s, int64x4_t t, const int i)
{
  return __builtin_hr_vpalignrid (s, t, i);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vpalignrips (floatx8_t s, floatx8_t t, const int i)
{
  return __builtin_hr_vpalignrips (s, t, i);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vpalignripd (doublex4_t s, doublex4_t t, const int i)
{
  return __builtin_hr_vpalignripd (s, t, i);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vpalignrb (int8x32_t s, int8x32_t t, int8x32_t r)
{
  return __builtin_hr_vpalignrb (s, t, r);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vpalignrh (int16x16_t s, int16x16_t t, int16x16_t r)
{
  return __builtin_hr_vpalignrh (s, t, r);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vpalignrw (int32x8_t s, int32x8_t t, int32x8_t r)
{
  return __builtin_hr_vpalignrw (s, t, r);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vpalignrd (int64x4_t s, int64x4_t t, int64x4_t r)
{
  return __builtin_hr_vpalignrd (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vpalignrps (floatx8_t s, floatx8_t t, floatx8_t r)
{
  return __builtin_hr_vpalignrps (s, t, r);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vpalignrpd (doublex4_t s, doublex4_t t, doublex4_t r)
{
  return __builtin_hr_vpalignrpd (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsmulj (floatx8_t s)
{
  return __builtin_hr_vcsmulj (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsmulnj (floatx8_t s)
{
  return __builtin_hr_vcsmulnj (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdmulj (doublex4_t s)
{
  return __builtin_hr_vcdmulj (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdmulnj (doublex4_t s)
{
  return __builtin_hr_vcdmulnj (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsco (floatx8_t s)
{
  return __builtin_hr_vcsco (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdco (doublex4_t s)
{
  return __builtin_hr_vcdco (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcscoadd (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vcscoadd (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcscosub (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vcscosub (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdcoadd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vcdcoadd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdcosub (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vcdcosub (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsmul1 (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vcsmul1 (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsmul2 (floatx8_t s, floatx8_t t, floatx8_t r)
{
  return __builtin_hr_vcsmul2 (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcscomul1 (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vcscomul1 (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcscomul2 (floatx8_t s, floatx8_t t, floatx8_t r)
{
  return __builtin_hr_vcscomul2 (s, t, r);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdmul1 (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vcdmul1 (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdmul2 (doublex4_t s, doublex4_t t, doublex4_t r)
{
  return __builtin_hr_vcdmul2 (s, t, r);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdcomul1 (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vcdcomul1 (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdcomul2 (doublex4_t s, doublex4_t t, doublex4_t r)
{
  return __builtin_hr_vcdcomul2 (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsmuljadd (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vcsmuljadd (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsmuljsub (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vcsmuljsub (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdmuljadd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vcdmuljadd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdmuljsub (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vcdmuljsub (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdmaddf (doublex4_t s, doublex4_t t, doublex4_t r)
{
  return __builtin_hr_vcdmaddf (s, t, r);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdmsubf (doublex4_t s, doublex4_t t, doublex4_t r)
{
  return __builtin_hr_vcdmsubf (s, t, r);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdmaddb (doublex4_t s, doublex4_t t, doublex4_t r)
{
  return __builtin_hr_vcdmaddb (s, t, r);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdmsubb (doublex4_t s, doublex4_t t, doublex4_t r)
{
  return __builtin_hr_vcdmsubb (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsmaddf (floatx8_t s, floatx8_t t, floatx8_t r)
{
  return __builtin_hr_vcsmaddf (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsmsubf (floatx8_t s, floatx8_t t, floatx8_t r)
{
  return __builtin_hr_vcsmsubf (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsmaddb (floatx8_t s, floatx8_t t, floatx8_t r)
{
  return __builtin_hr_vcsmaddb (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsmsubb (floatx8_t s, floatx8_t t, floatx8_t r)
{
  return __builtin_hr_vcsmsubb (s, t, r);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsffts1l (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vcsffts1l (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsffts1h (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vcsffts1h (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsffts2e (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vcsffts2e (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsffts2o (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vcsffts2o (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsmod (floatx8_t s)
{
  return __builtin_hr_vcsmod (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcsslope (floatx8_t s)
{
  return __builtin_hr_vcsslope (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcshadd (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vcshadd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdhadd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vcdhadd (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcshsub (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vcshsub (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcdhsub (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vcdhsub (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vsrlb_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vsrlb_u (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vsrlh_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vsrlh_u (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vsrlw_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vsrlw_u (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vsrld_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vsrld_u (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vsrlb_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vsrlb_s (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vsrlh_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vsrlh_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vsrlw_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vsrlw_s (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vsrld_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vsrld_s (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vsrab_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vsrab_u (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vsrah_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vsrah_u (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vsraw_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vsraw_u (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vsrad_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vsrad_u (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vsrab_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vsrab_s (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vsrah_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vsrah_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vsraw_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vsraw_s (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vsrad_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vsrad_s (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vsllb_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vsllb_u (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vsllh_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vsllh_u (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vsllw_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vsllw_u (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vslld_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vslld_u (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vsllb_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vsllb_s (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vsllh_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vsllh_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vsllw_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vsllw_s (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vslld_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vslld_s (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vsrrb_u (uint8x32_t s, uint8x32_t t)
{
  return __builtin_hr_vsrrb_u (s, t);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vsrrh_u (uint16x16_t s, uint16x16_t t)
{
  return __builtin_hr_vsrrh_u (s, t);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vsrrw_u (uint32x8_t s, uint32x8_t t)
{
  return __builtin_hr_vsrrw_u (s, t);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vsrrd_u (uint64x4_t s, uint64x4_t t)
{
  return __builtin_hr_vsrrd_u (s, t);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vsrrb_s (int8x32_t s, int8x32_t t)
{
  return __builtin_hr_vsrrb_s (s, t);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vsrrh_s (int16x16_t s, int16x16_t t)
{
  return __builtin_hr_vsrrh_s (s, t);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vsrrw_s (int32x8_t s, int32x8_t t)
{
  return __builtin_hr_vsrrw_s (s, t);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vsrrd_s (int64x4_t s, int64x4_t t)
{
  return __builtin_hr_vsrrd_s (s, t);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vsrlbi_u (uint8x32_t s, const int i)
{
  return __builtin_hr_vsrlbi_u (s, i);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vsrlhi_u (uint16x16_t s, const int i)
{
  return __builtin_hr_vsrlhi_u (s, i);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vsrlwi_u (uint32x8_t s, const int i)
{
  return __builtin_hr_vsrlwi_u (s, i);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vsrldi_u (uint64x4_t s, const int i)
{
  return __builtin_hr_vsrldi_u (s, i);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vsrlbi_s (int8x32_t s, const int i)
{
  return __builtin_hr_vsrlbi_s (s, i);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vsrlhi_s (int16x16_t s, const int i)
{
  return __builtin_hr_vsrlhi_s (s, i);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vsrlwi_s (int32x8_t s, const int i)
{
  return __builtin_hr_vsrlwi_s (s, i);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vsrldi_s (int64x4_t s, const int i)
{
  return __builtin_hr_vsrldi_s (s, i);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vsrabi_u (uint8x32_t s, const int i)
{
  return __builtin_hr_vsrabi_u (s, i);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vsrahi_u (uint16x16_t s, const int i)
{
  return __builtin_hr_vsrahi_u (s, i);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vsrawi_u (uint32x8_t s, const int i)
{
  return __builtin_hr_vsrawi_u (s, i);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vsradi_u (uint64x4_t s, const int i)
{
  return __builtin_hr_vsradi_u (s, i);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vsrabi_s (int8x32_t s, const int i)
{
  return __builtin_hr_vsrabi_s (s, i);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vsrahi_s (int16x16_t s, const int i)
{
  return __builtin_hr_vsrahi_s (s, i);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vsrawi_s (int32x8_t s, const int i)
{
  return __builtin_hr_vsrawi_s (s, i);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vsradi_s (int64x4_t s, const int i)
{
  return __builtin_hr_vsradi_s (s, i);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vsllbi_u (uint8x32_t s, const int i)
{
  return __builtin_hr_vsllbi_u (s, i);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vsllhi_u (uint16x16_t s, const int i)
{
  return __builtin_hr_vsllhi_u (s, i);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vsllwi_u (uint32x8_t s, const int i)
{
  return __builtin_hr_vsllwi_u (s, i);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vslldi_u (uint64x4_t s, const int i)
{
  return __builtin_hr_vslldi_u (s, i);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vsllbi_s (int8x32_t s, const int i)
{
  return __builtin_hr_vsllbi_s (s, i);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vsllhi_s (int16x16_t s, const int i)
{
  return __builtin_hr_vsllhi_s (s, i);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vsllwi_s (int32x8_t s, const int i)
{
  return __builtin_hr_vsllwi_s (s, i);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vslldi_s (int64x4_t s, const int i)
{
  return __builtin_hr_vslldi_s (s, i);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vsrrbi_u (uint8x32_t s, const int i)
{
  return __builtin_hr_vsrrbi_u (s, i);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vsrrhi_u (uint16x16_t s, const int i)
{
  return __builtin_hr_vsrrhi_u (s, i);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vsrrwi_u (uint32x8_t s, const int i)
{
  return __builtin_hr_vsrrwi_u (s, i);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vsrrdi_u (uint64x4_t s, const int i)
{
  return __builtin_hr_vsrrdi_u (s, i);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vsrrbi_s (int8x32_t s, const int i)
{
  return __builtin_hr_vsrrbi_s (s, i);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vsrrhi_s (int16x16_t s, const int i)
{
  return __builtin_hr_vsrrhi_s (s, i);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vsrrwi_s (int32x8_t s, const int i)
{
  return __builtin_hr_vsrrwi_s (s, i);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vsrrdi_s (int64x4_t s, const int i)
{
  return __builtin_hr_vsrrdi_s (s, i);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcvtpls2pd (floatx8_t s)
{
  return __builtin_hr_vcvtpls2pd (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcvtphs2pd (floatx8_t s)
{
  return __builtin_hr_vcvtphs2pd (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcvtpd2ps (doublex4_t s)
{
  return __builtin_hr_vcvtpd2ps (s);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vtestalloneb_s (int8x32_t s)
{
  return __builtin_hr_vtestalloneb_s (s);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vtestalloneh_s (int16x16_t s)
{
  return __builtin_hr_vtestalloneh_s (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vtestallonew_s (int32x8_t s)
{
  return __builtin_hr_vtestallonew_s (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vtestalloned_s (int64x4_t s)
{
  return __builtin_hr_vtestalloned_s (s);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vtestalloneb_u (uint8x32_t s)
{
  return __builtin_hr_vtestalloneb_u (s);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vtestalloneh_u (uint16x16_t s)
{
  return __builtin_hr_vtestalloneh_u (s);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vtestallonew_u (uint32x8_t s)
{
  return __builtin_hr_vtestallonew_u (s);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vtestalloned_u (uint64x4_t s)
{
  return __builtin_hr_vtestalloned_u (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vtestalloneps (floatx8_t s)
{
  return __builtin_hr_vtestalloneps (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vtestallonepd (doublex4_t s)
{
  return __builtin_hr_vtestallonepd (s);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vtesthasoneb_s (int8x32_t s)
{
  return __builtin_hr_vtesthasoneb_s (s);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vtesthasoneh_s (int16x16_t s)
{
  return __builtin_hr_vtesthasoneh_s (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vtesthasonew_s (int32x8_t s)
{
  return __builtin_hr_vtesthasonew_s (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vtesthasoned_s (int64x4_t s)
{
  return __builtin_hr_vtesthasoned_s (s);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vtesthasoneb_u (uint8x32_t s)
{
  return __builtin_hr_vtesthasoneb_u (s);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vtesthasoneh_u (uint16x16_t s)
{
  return __builtin_hr_vtesthasoneh_u (s);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vtesthasonew_u (uint32x8_t s)
{
  return __builtin_hr_vtesthasonew_u (s);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vtesthasoned_u (uint64x4_t s)
{
  return __builtin_hr_vtesthasoned_u (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vtesthasoneps (floatx8_t s)
{
  return __builtin_hr_vtesthasoneps (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vtesthasonepd (doublex4_t s)
{
  return __builtin_hr_vtesthasonepd (s);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vtestallzerob_s (int8x32_t s)
{
  return __builtin_hr_vtestallzerob_s (s);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vtestallzeroh_s (int16x16_t s)
{
  return __builtin_hr_vtestallzeroh_s (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vtestallzerow_s (int32x8_t s)
{
  return __builtin_hr_vtestallzerow_s (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vtestallzerod_s (int64x4_t s)
{
  return __builtin_hr_vtestallzerod_s (s);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vtestallzerob_u (uint8x32_t s)
{
  return __builtin_hr_vtestallzerob_u (s);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vtestallzeroh_u (uint16x16_t s)
{
  return __builtin_hr_vtestallzeroh_u (s);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vtestallzerow_u (uint32x8_t s)
{
  return __builtin_hr_vtestallzerow_u (s);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vtestallzerod_u (uint64x4_t s)
{
  return __builtin_hr_vtestallzerod_u (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vtestallzerops (floatx8_t s)
{
  return __builtin_hr_vtestallzerops (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vtestallzeropd (doublex4_t s)
{
  return __builtin_hr_vtestallzeropd (s);
}

__extension__ static __inline int8x32_t __attribute__ ((__always_inline__))
    vtesthaszerob_s (int8x32_t s)
{
  return __builtin_hr_vtesthaszerob_s (s);
}

__extension__ static __inline int16x16_t __attribute__ ((__always_inline__))
    vtesthaszeroh_s (int16x16_t s)
{
  return __builtin_hr_vtesthaszeroh_s (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vtesthaszerow_s (int32x8_t s)
{
  return __builtin_hr_vtesthaszerow_s (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vtesthaszerod_s (int64x4_t s)
{
  return __builtin_hr_vtesthaszerod_s (s);
}

__extension__ static __inline uint8x32_t __attribute__ ((__always_inline__))
    vtesthaszerob_u (uint8x32_t s)
{
  return __builtin_hr_vtesthaszerob_u (s);
}

__extension__ static __inline uint16x16_t __attribute__ ((__always_inline__))
    vtesthaszeroh_u (uint16x16_t s)
{
  return __builtin_hr_vtesthaszeroh_u (s);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vtesthaszerow_u (uint32x8_t s)
{
  return __builtin_hr_vtesthaszerow_u (s);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vtesthaszerod_u (uint64x4_t s)
{
  return __builtin_hr_vtesthaszerod_u (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vtesthaszerops (floatx8_t s)
{
  return __builtin_hr_vtesthaszerops (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vtesthaszeropd (doublex4_t s)
{
  return __builtin_hr_vtesthaszeropd (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmpeqps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vcmpeqps (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmpltps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vcmpltps (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmpleps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vcmpleps (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmpneps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vcmpneps (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmpgtps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vcmpgtps (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmpgeps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vcmpgeps (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmpodps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vcmpodps (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmpunps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vcmpunps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmpeqpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vcmpeqpd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmpltpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vcmpltpd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmplepd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vcmplepd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmpnepd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vcmpnepd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmpgtpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vcmpgtpd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmpgepd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vcmpgepd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmpodpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vcmpodpd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmpunpd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vcmpunpd (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmpeqsps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vcmpeqsps (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmpltsps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vcmpltsps (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmplesps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vcmplesps (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmpnesps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vcmpnesps (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmpgtsps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vcmpgtsps (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmpgesps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vcmpgesps (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmpodsps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vcmpodsps (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vcmpunsps (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vcmpunsps (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmpeqspd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vcmpeqspd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmpltspd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vcmpltspd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmplespd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vcmplespd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmpnespd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vcmpnespd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmpgtspd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vcmpgtspd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmpgespd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vcmpgespd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmpodspd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vcmpodspd (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vcmpunspd (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vcmpunspd (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vscscmpeq (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vscscmpeq (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vscscmpne (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vscscmpne (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vscscmpod (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vscscmpod (s, t);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vscscmpun (floatx8_t s, floatx8_t t)
{
  return __builtin_hr_vscscmpun (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vscdcmpeq (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vscdcmpeq (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vscdcmpne (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vscdcmpne (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vscdcmpod (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vscdcmpod (s, t);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vscdcmpun (doublex4_t s, doublex4_t t)
{
  return __builtin_hr_vscdcmpun (s, t);
}

__extension__ static __inline int __attribute__ ((__always_inline__))
    vmtr_s (int32x8_t s)
{
  return __builtin_hr_vmtr_s (s);
}

__extension__ static __inline unsigned int __attribute__ ((__always_inline__))
    vmtr_u (uint32x8_t s)
{
  return __builtin_hr_vmtr_u (s);
}

__extension__ static __inline int32x8_t __attribute__ ((__always_inline__))
    vmfr_s (int s)
{
  return __builtin_hr_vmfr_s (s);
}

__extension__ static __inline uint32x8_t __attribute__ ((__always_inline__))
    vmfr_u (unsigned int s)
{
  return __builtin_hr_vmfr_u (s);
}

__extension__ static __inline long long __attribute__ ((__always_inline__))
    vmtrd_s (int64x4_t s)
{
  return __builtin_hr_vmtrd_s (s);
}

__extension__ static __inline unsigned long long __attribute__ ((__always_inline__))
    vmtrd_u (uint64x4_t s)
{
  return __builtin_hr_vmtrd_u (s);
}

__extension__ static __inline int64x4_t __attribute__ ((__always_inline__))
    vmfrd_s (long long s)
{
  return __builtin_hr_vmfrd_s (s);
}

__extension__ static __inline uint64x4_t __attribute__ ((__always_inline__))
    vmfrd_u (unsigned long long s)
{
  return __builtin_hr_vmfrd_u (s);
}

__extension__ static __inline float __attribute__ ((__always_inline__))
    vmtfs (floatx8_t s)
{
  return __builtin_hr_vmtfs (s);
}

__extension__ static __inline floatx8_t __attribute__ ((__always_inline__))
    vmffs (float s)
{
  return __builtin_hr_vmffs (s);
}

__extension__ static __inline double __attribute__ ((__always_inline__))
    vmtfd (doublex4_t s)
{
  return __builtin_hr_vmtfd (s);
}

__extension__ static __inline doublex4_t __attribute__ ((__always_inline__))
    vmffd (double s)
{
  return __builtin_hr_vmffd (s);
}

#endif
