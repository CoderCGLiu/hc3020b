/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了ReWorks CSP模块异常相关接口  
 * 修改：
 * 		 2013-01-06，唐立三，建立 
 */
#ifndef _MIPS_CSP_EXCEPTION_H_
#define _MIPS_CSP_EXCEPTION_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <reworks/types.h>
#include <pthread.h>
	
	
/**
 * @defgroup group_os_csp_exception CSP异常模块
 * @ingroup group_os_csp
 * 
 * @{
 * 
 */	

/*!
 * \struct exception_info
 * 
 * \brief 异常消息
 */	
struct exc_report_msg
{
	thread_t 	tid;
	int 	excno;
	int		cpu;
};

/**
 * 最大支持的异常钩子数
 */
#define EXC_MAX_HANDLER				10


/*!
 * \def  EXC_HANDLER_TYPE
 * 
 * \brief 定义钩子函数类型
 */	
typedef int (*EXC_HANDLER_TYPE)(struct exc_report_msg *msg);


/*!
 * \fn int exc_handler_register(EXC_HANDLER_TYPE handler)
 * 
 * \brief 注册异常钩子接口
 */	
int exc_handler_register(EXC_HANDLER_TYPE handler);


/*!
 * \fn int exc_handler_unregister(EXC_HANDLER_TYPE handler)
 * 
 * \brief 注销异常钩子接口
 */	
int exc_handler_unregister(EXC_HANDLER_TYPE handler);


/*!
 * \fn int exc_reg_disp_set(void)
 * 
 * \brief 设置显示所有寄存器内容的开关
 * 
 * \param 无
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int exc_reg_disp_set(void);


/*!
 * \fn int exc_reg_disp_clear(void)
 * 
 * \brief 清除显示所有寄存器内容的开关
 * 
 * \param 无
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int exc_reg_disp_clear(void);


/*!
 * \fn int exc_display(struct exc_report_msg *msg)
 * 
 * \brief 显示指定的任务异常
 * 
 * \param msg 异常消息
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int exc_display(struct exc_report_msg *msg);


/*!
 * \fn int exc_report_msg_del(struct exc_report_msg *msg)
 * 
 * \brief 删除异常分析模块中的异常信息接口
 * 
 * \param msg 异常消息
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int exc_report_msg_del(struct exc_report_msg *msg);


/*!
 * \fn int exc_task_del(struct exc_report_msg *msg)
 * 
 * \brief 将发生异常的任务强制删除
 * 
 * \param msg 异常消息
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int exc_task_del(struct exc_report_msg *msg);


/*!
 * \fn int exc_auto_handling(int flag)
 * 
 * \brief 在调用异常分析模块后，自动删除异常消息和异常任务
 * 
 * \param flag 1-自动删除任务，0-不自动删除任务
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int exc_auto_handling(int flag);


/*!
 * \fn void unaligned_fix_on()
 * 
 * \brief 打开非对齐地址异常处理。
 * 
 * \param 无
 * 
 * \return 无
 * 
 */
void unaligned_fix_on();

/*!
 * \fn void unaligned_fix_off()
 * 
 * \brief 关闭非对齐地址异常处理。
 * 
 * \param 无
 * 
 * \return 无
 * 
 */
void unaligned_fix_off();
	
/* @} */	
	
#ifdef __cplusplus
}
#endif	

#endif
