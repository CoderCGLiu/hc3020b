/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了华睿2号中断控制器相关寄存器及中断定义
 * 修改：
 * 		  2018-05-22，符凯，创建 
 */

#ifndef __HUARUI2_INTC_DEFS_H_
#define __HUARUI2_INTC_DEFS_H_

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * 华睿2号 64个中断源对应中断号
 */
typedef enum _HR2_INT
{
	INT_GPDMA = 0, 		/*(0)*/
	INT_PCIE,			/*(1)*/
	INT_RAB0,			/*(2)*/
	INT_RAB1,			/*(3)*/
	INT_RASP0,			/*(4)*/
	INT_RASP1,			/*(5)*/
	INT_RASP2,			/*(6)*/
	INT_RASP3,			/*(7)*/
	INT_NAND,			/*(8)*/
	INT_GMAC0,			/*(9)*/
	INT_GMAC1,			/*(10)*/
	INT_GMAC2,			/*(11)*/
	DMA_RD_INT,			/*(12)*/
	DMA_WR_INT,			/*(13)*/
	INT_UART0,			/*(14)*/
	INT_UART1,			/*(15)*/
	INT_SPI0,			/*(16)*/
	INT_SPI1,			/*(17)*/
	INT_CAN0,			/*(18)*/
	INT_CAN1,			/*(19)*/
	INT_I2CS0,			/*(20)*/
	INT_I2CS1,			/*(21)*/
	INT_I2CM0,			/*(22)*/
	INT_I2CM1,			/*(23)*/
	INT_IPC,			/*(24)*/
	INT_WDOG,			/*(25)*/
	INT_TIMER0,			/*(26)*/
	INT_TIMER1,			/*(27)*/
	INT_TSSC,			/*(28)*/
	INT_RESERVED1,  	/*(29)*/
	INT_DSP0,  			/*(30)*/
	INT_DSP1,  			/*(31)*/
	INT_DSP2,  			/*(32)*/
	INT_DSP3,  			/*(33)*/
	INT_RESERVED2, 		/*(34)*/
	INT_RESERVED3, 		/*(35)*/
	GPIO_INT_MS0, 		/*(36)*/
	GPIO_INT_MS1, 		/*(37)*/
	GPIO_INT_MS2, 		/*(38)*/
	GPIO_INT_MS3, 		/*(39)*/
	GPIO_INT_MS4, 		/*(40)*/
	GPIO_INT_MS5, 		/*(41)*/
	GPIO_INT_MS6, 		/*(42)*/
	GPIO_INT_MS7, 		/*(43)*/
	GPIO_INT_MS8, 		/*(44)*/
	GPIO_INT_MS9, 		/*(45)*/
	GPIO_INT_MS10, 		/*(46)*/
	GPIO_INT_MS11, 		/*(47)*/
	GPIO_PWM_INT_MS0, 	/*(48)*/
	GPIO_PWM_INT_MS1, 	/*(49)*/
	GPIO_PWM_INT_MS2, 	/*(50)*/
	GPIO_PWM_INT_MS3, 	/*(51)*/
	INT_DDR0, 			/*(52)*/
	INT_DDR1, 			/*(53)*/
	INT_DDR2, 			/*(54)*/
	RASP0_ERR_INTE, 	/*(55)*/
	RASP1_ERR_INTE, 	/*(56)*/
	RASP2_ERR_INTE, 	/*(57)*/
	RASP3_ERR_INTE, 	/*(58)*/
	ACE_INT,		 	/*(59)*/
	AXI_CROSSBAR_DEADLOCK_INT,	/*(60)*/
	INT_RESERVED4,		/*(61)*/
	INT_RESERVED5,	 	/*(62)*/
	INT_RESERVED6, 		/*(63)*/
	INT_CLOCK,          /*(64)*/
	INT_IPI,            /*(65)*/
	HR2_INT_END 		/*(66)*/
}HR2_INT;

/**
 * 华睿2号中断源数目
 */
#define HR2_INT_CNT ((int)HR2_INT_END)


/* defines */
#define INTERRUPT_BASE_ADDR				0x1f078000
#define INTERRUPT_SEL_REG(x)         	(INTERRUPT_BASE_ADDR+0x800+(x<<3))
#define INTERRUPT_ENABLE_REG         	(INTERRUPT_BASE_ADDR+0xa00)
#define INTERRUPT_MASK_REG            (INTERRUPT_BASE_ADDR+0xa08)
#define INTERRUPT_MODE_REG            (INTERRUPT_BASE_ADDR+0xa10)
#define INTERRUPT_CLR_REG         		(INTERRUPT_BASE_ADDR+0xa18)
#define INTERRUPT_STAT_REG         	(INTERRUPT_BASE_ADDR+0xa20)
#define INTERRUPT_RAWSTA_REG        (INTERRUPT_BASE_ADDR+0xa28)
#define INTERRUPT_REQSTA_REG         (INTERRUPT_BASE_ADDR+0xa30)
#define INTERRUPT_INTERVAL				4


#define INTENSET_REG          (0x1f0781428) /*32位中断使能设置寄存器*/



#define R_IMR_MAILBOX_READ_CPU		0x0000 /* 0x1000 liuw, 20140117 */
#define R_IMR_MAILBOX_ENABLE_CPU	0x0008 /* 0x1008,liuw, 20140117 */
#define R_IMR_MAILBOX_SET_CPU		0x0010 /* 0x1010 liuw, 20140117 */
#define R_IMR_MAILBOX_CLR_CPU		0x0018 /* 0x100c liuw, 20140117 */
#define R_IMR_MAILBOX0_CPU          0x0020 /* added by liuw 20140120 */

#define MAILBOX_READ                    R_IMR_MAILBOX_READ_CPU
#define MAILBOX_ENABLE					R_IMR_MAILBOX_ENABLE_CPU /* added by liuw, 20140117 */
#define MAILBOX_SET                     R_IMR_MAILBOX_SET_CPU
#define MAILBOX_CLR                     R_IMR_MAILBOX_CLR_CPU
#define MAILBOX0_BASE                   R_IMR_MAILBOX0_CPU

#define A_IMR_CPU0_BASE             0x1f078100 /* 0x0010020000 by liuw 20140117 */
#define A_IMR_CPU1_BASE             0x1f078200 /* 0x0010022000 by liuw 20140117  */
#define A_IMR_CPU2_BASE             0x1f078300 /* 0x0010024000 by liuw 20140117  */
#define A_IMR_CPU3_BASE             0x1f078400 /* 0x0010026000 by liuw 20140117  */
#define IMR_REGISTER_SPACING        0x100 /* 0x2000 by liuw, 20140117 */
#define IMR_REGISTER_SPACING_SHIFT  8 /* 13 by liuw 20140117 */

#define IPI_INTR_ID_CPC		0

#define A_IMR_MAPPER(cpu)       (A_IMR_CPU0_BASE+(cpu)*IMR_REGISTER_SPACING)
#define A_IMR_REGISTER(cpu,reg) (A_IMR_MAPPER(cpu)+(reg))

#define IMR_REGISTER(cpunum, reg)       /*PHYS_TO_K1 20091211*/(A_IMR_REGISTER(cpunum,(reg)))
		
#define SMP_CPC_MAILBOX_INT_BIT		(1UL<<16) /* (1LL<<48) yinwx, 20091219 */
#define MAILBOX_INT_BIT(ipi)		(1UL<<ipi) /* (1LL<<ipi) yinwx, 20091219 */



#ifdef __cplusplus
}
#endif

#endif /* __HUARUI2_INTC_DEFS_H_ */
