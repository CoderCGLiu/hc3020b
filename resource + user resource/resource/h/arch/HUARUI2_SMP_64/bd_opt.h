/*
 *  带延迟槽优化的地址异常处理相关的头文件
 */
#ifndef _REWORKS_BD_OPT_H_
#define _REWORKS_BD_OPT_H_

#ifdef __cplusplus
extern "C"
{
#endif

#ifndef ASM_LANGUAGE

#include <cpu.h>
#include <reworks/types.h>

typedef struct {
	u32 type;
    u32 c0_sr;
	u64 pc;
    u64 mulhi;
    u64 mullo;
	u64 zero;
	u64 AT;
	u64 v0;
	u64 v1;
	u64 a0;
	u64 a1;
	u64 a2;
	u64 a3;
    u64 t0;
    u64 t1;
    u64 t2;
    u64 t3;
    u64 t4;
    u64 t5;
    u64 t6;
    u64 t7;
    u64 s0;
    u64 s1;
    u64 s2;
    u64 s3;
    u64 s4;
    u64 s5;
    u64 s6;
    u64 s7;
    u64 t8;
    u64 t9;
    u64 k0;
    u64 k1;
    u64 gp;
    u64 sp;
    u64 fp;
    u64 ra;
    u32 cause;
} Context_Ctrl_BD;

extern void context_to_bd(Context_Ctrl *s,Context_Ctrl_BD *bd, u32 cause);
extern void bd_to_context(Context_Ctrl_BD *bd, Context_Ctrl *s);

extern int lw_unaligned_exc(void *dummy, Context_Ctrl_BD *context_bd);

#endif /* #ifndef ASM_LANGUAGE */

#define R_SZ 8
#define M_E_STK_EPC (1 * R_SZ)
#define M_E_STK_SR (4)
#define M_E_STK_ZERO (4 * R_SZ)
#define M_E_STK_AT (5 * R_SZ)
#define M_E_STK_V0 (6 * R_SZ)
#define M_E_STK_V1 (7 * R_SZ)
#define M_E_STK_A0 (8 * R_SZ)
#define M_E_STK_A1 (9 * R_SZ)
#define M_E_STK_A2 (10 * R_SZ)
#define M_E_STK_A3 (11 * R_SZ)
#define M_E_STK_T0 (12 * R_SZ)
#define M_E_STK_T1 (13 * R_SZ)
#define M_E_STK_T2 (14 * R_SZ)
#define M_E_STK_T3 (15 * R_SZ)
#define M_E_STK_T4 (16 * R_SZ)
#define M_E_STK_T5 (17 * R_SZ)
#define M_E_STK_T6 (18 * R_SZ)
#define M_E_STK_T7 (19 * R_SZ)
#define M_E_STK_S0 (20 * R_SZ)
#define M_E_STK_S1 (21 * R_SZ)
#define M_E_STK_S2 (22 * R_SZ)
#define M_E_STK_S3 (23 * R_SZ)
#define M_E_STK_S4 (24 * R_SZ)
#define M_E_STK_S5 (25 * R_SZ)
#define M_E_STK_S6 (26 * R_SZ)
#define M_E_STK_S7 (27 * R_SZ)
#define M_E_STK_T8 (28 * R_SZ)
#define M_E_STK_T9 (29 * R_SZ)
#define M_E_STK_K0 (30 * R_SZ)
#define M_E_STK_K1 (31 * R_SZ)
#define M_E_STK_GP (32 * R_SZ)
#define M_E_STK_SP (33 * R_SZ)
#define M_E_STK_FP (34 * R_SZ)
#define M_E_STK_RA (35 * R_SZ)
#define M_E_STK_CAUSE (36 * R_SZ)

#define F_E_STK_FPCSR (32 * R_SZ)
//#define E_STK_GREG_BASE	(12*4+2*_RTypeSize)
#define E_STK_GREG_BASE	(4*R_SZ)

#ifdef __cplusplus
}
#endif

#endif /* BD_OPT_H */
