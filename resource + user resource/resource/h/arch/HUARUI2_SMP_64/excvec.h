
/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：定义异常管理框架中用到的宏和结构体。
 * 修改：       
 * 		2014-03-07, 宋伟, 创建. 
 */

#ifndef _REWORKS_EXCVEC_H_
#define _REWORKS_EXCVEC_H_

#ifdef __cplusplus
extern "C" {
#endif


/*异常向量号*/
#define EXC_CODE_TLBMOD	1       /*TLB Modify*/
#define EXC_CODE_TLBL	2		/* TLB miss exception (load or instruction fetch) */
#define EXC_CODE_TLBS	3		/* TLB miss exception (store) */
#define	EXC_CODE_ADEL	4		/* address error exception (load/instruction fetch) */
#define EXC_CODE_ADES	5		/* address error exception (store) */
#define	EXC_CODE_IBE	6		/* Instrution bus error */
#define EXC_CODE_DBE	7		/* data bus error */
#define EXC_CODE_SYS	8		/* system call */
#define	EXC_CODE_BP 	9		/* breakpoint exception */
#define EXC_CODE_RI		10		/* reserved instruction exception */
#define EXC_CODE_CPU	11		/* coprocessor unusable exception */
#define EXC_CODE_OV     12      /* overflow vector              */
#define EXC_CODE_TRAP   13      /* trap vector                  */
#define EXC_CODE_FPE	15		/* floating point exception */
#define EXC_CODE_WATCH	23		/* watchpoint exception */

#ifdef __cplusplus
}
#endif
#endif
