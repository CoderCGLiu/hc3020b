/*! @file bsp_int.h
    @brief ReWorks BSP中断框架头文件
    
 * 本文件定义了ReWorks系统中的中断框架接口。
 *
 * @version @reworks_version
 * 
 * @see 无
 */
/*******************************************************************************
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了中断框架接口 
 * 修改：
 * 		 2011-11-02，修改BSP中断函数注册方式
 * 		 2011-09-22，规范BSP
 */
#ifndef _MIPS_BSP_INT_H_
#define _MIPS_BSP_INT_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <reworks/types.h>	
	
/**  
 * @defgroup group_os_bsp_int 中断控制器模块
 * @ingroup group_os_bsp
 *		
 *  @{ 
 */	
	
/*******************************************************************************
 * 
 * 内部中断控制器提供的接口
 * 
 * 		内部中断控制器是否需提供如下接口取决于硬件平台，与硬件相关的如下操作应在CSP
 * 说明文档中提供。内部中断控制器接口一般在BSP中断操作框架接口中调用。
 * 
 */
/*!
 * \fn int internal_int_ack(int irq)
 * 
 * \brief 内部中断控制器中断响应接口
 * 
 * \param irq 中断号
 * 
 * \return 成功返回0
 * \return 失败返回-1
 */ 
int internal_int_ack(int irq);

/*!
 * \fn int internal_int_enable(u32 irq)
 * 
 * \brief 内部中断控制器中断使能接口
 * 
 * \param irq 中断号
 * 
 * \return 成功返回0
 * \return 失败返回-1
 */ 
int internal_int_enable(u32 irq);

/*!
 * \fn int internal_int_disable(u32 irq)
 * 
 * \brief 内部中断控制器中断非能接口
 * 
 * \param irq 中断号
 * 
 * \return 成功返回0
 * \return 失败返回-1
 */ 
int internal_int_disable(u32 irq);

/*!
 * \fn int internal_int_inum(u32 interrupt_pending)
 * 
 * \brief 内部中断控制器取中断号接口
 * 
 * \param interrupt_pending 中断源
 * 
 * \return 中断号
 */
int internal_int_inum(u32 interrupt_pending);

/*!
 * \fn int internal_int_init(void)
 * 
 * \brief 内部中断控制器初始化接口
 * 
 * \param 无
 * 
 * \return 成功返回硬件平台支持的中断数量
 * \return 失败返回-1
 */
int internal_int_init(void);


/*******************************************************************************
 * 
 * BSP中断控制器需注册的操作接口
 * 
 * 		BSP中断控制器需注册的操作接口在BSP中实现，并通过注册接口注册到CSP以供中断框架
 * 调用。BSP中断控制器需注册的操作接口一般需要调用内部中断控制器（如果存在，在CSP文档
 * 中说明）和外部中断控制器（由BSP实现，如果存在）的相应操作。
 * 
 * 
 */
/*!
 * \typedef INT_ACK_FUNCPTR
 * 
 * \brief 中断响应函数类型 
 */
typedef int(* INT_ACK_FUNCPTR)(int);

/*!
 * \typedef INT_ENBALE_FUNCPTR
 * 
 * \brief 中断使能函数类型 
 */
typedef int (* INT_ENBALE_FUNCPTR)(u32);

/*!
 * \typedef INT_DISBALE_FUNCPTR
 * 
 * \brief 中断屏蔽函数类型
 */
typedef int (* INT_DISBALE_FUNCPTR)(u32);

/*!
 * \typedef INT_INUM_FUNCPTR
 * 
 * \brief 获取中断号函数类型
 */
typedef int (* INT_INUM_FUNCPTR)(u32, u32[] , u32);
/*!
 * \typedef INT_INIT_FUNPTR
 * 
 * \brief 初始化中断控制器接口
 * （返回值为中断控制器支持的中断数量）
 */
typedef int (* INT_INIT_FUNPTR)(void);

/*!
 * \typedef IPI_HANDLER
 * 
 * \brief 核间中断处理接口
 */
typedef void (*IPI_HANDLER) (void*); 

/*!
 * \typedef BSP_INT_HOOK
 * 
 * \brief 中断处理钩子函数
 */
typedef void (* BSP_INT_HOOK)(void);

/*!
 * \typedef BSP_INT_HOOK
 * 
 * \brief 中断使能钩子函数
 */
typedef void (* BSP_INT_HOOK_ENABLE)(int);

/*!
 * \typedef BSP_INT_HOOK
 * 
 * \brief 中断屏蔽钩子函数
 */
typedef void (* BSP_INT_HOOK_DISABLE)(int);

/*!
 * \typedef INT_SPECIAL_FUNCPTR
 * 
 * \brief 特殊中断处理函数
 */
typedef int (* INT_SPECIAL_FUNCPTR)(void *);


/*!
 * \struct int_operations
 *
 * \brief 定义中断接口结构体
 */
struct int_operations
{
	INT_ACK_FUNCPTR 			ack;		//!< 相应操作
	INT_ENBALE_FUNCPTR			enable;		//!< 使能操作
	INT_DISBALE_FUNCPTR			disable;	//!< 非能操作
	INT_INUM_FUNCPTR			inum;		//!< 获取中断号操作
	INT_INIT_FUNPTR				init;		//!< 初始化操作
	INT_SPECIAL_FUNCPTR			special1;
};

/*!
 * \struct ipi_data
 *
 * \brief 定义核间中断挂接的结构体
 */
struct ipi_data {
	IPI_HANDLER handler;
	void *param;
	int valid;
};

/*!
 * \fn  int int_operations_register(struct int_operations *ops)
 * 
 * \brief 硬件中断操作注册接口 
 * 
 * \param ops 中断控制器操作
 * 
 * \return 成功返回0
 * \return 失败返回-1
 */
int int_operations_register(struct int_operations *ops);

/*!
 * \fn  int ipi_enable(int cpuid)
 * 
 * \brief 核间中断使能接口
 * 
 * \param ipiId 要使能的核间中断
 * 
 * \return 成功返回0
 * \return 失败返回-1
 */
int ipi_enable(int cpuid);

/*!
 * \fn  int ipi_disable(int cpuid)
 * 
 * \brief 核间中断关闭接口
 * 
 * \param ipiId 要关闭的核间中断
 * 
 * \return 成功返回0
 * \return 失败返回-1
 */
int ipi_disable(int cpuid);

/*!
 * \fn  int ipi_set(int cpuid, int value)
 * 
 * \brief 发送核间中断给其他CPU
 * 
 * \param cpuid CPU号
 * \param ipi 核间中断号
 * 
 * \return 成功返回0
 * \return 失败返回-1
 */
int ipi_set(int cpuid, int value);

/*!
 * \fn OS_STATUS ipi_install_handler(int ipiId, IPI_HANDLER handler, void* arg)
 * 
 * \brief 挂接核间中断处理函数
 * 
 * \param ipiId 核间中断号
 * \param handler 核间中断处理函数
 * \param arg     参数
 * 
 * \return 成功返回OS_OK
 * \return 失败返回OS_ERROR
 */
OS_STATUS ipi_install_handler(int ipiId, IPI_HANDLER handler, void* arg);

/* @} */

#ifdef __cplusplus
}
#endif

#endif
