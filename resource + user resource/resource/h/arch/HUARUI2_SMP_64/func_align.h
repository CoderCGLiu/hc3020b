#ifndef _REWORKS_FUNC_ALIGN_H_
#define _REWORKS_FUNC_ALIGN_H_

#ifdef __cplusplus
extern "C" {
#endif

#define FUNC_CACHELINE_ALIGNED __attribute__ ((aligned(32)))

#ifdef __cplusplus
}
#endif
#endif /* _REWORKS_FUNC_ALIGN_H_ */
