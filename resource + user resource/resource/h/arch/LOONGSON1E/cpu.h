/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了CSP模块中loongson2f处理器定义
 * 修改：
 * 		 1. 2012-5-17，规范CSP 
 */

#ifndef _REWORKS_CPU_H_
#define _REWORKS_CPU_H_

#ifdef __cplusplus
extern "C" 
{
#endif

#define MAX_SMP_CPUS 1

/* 上下文类型 */
#define SWITCH_TYPE		0xa5a50101
#define INTERRUPT_TYPE	0x10105a5a

#ifdef ASM_LANGUAGE
	
#define R_SZ 4

#define THR_CONTEXT_CONTROL_SIZE 60

#define PC_OFFSET 1
#define S0_OFFSET 2
#define S1_OFFSET 3
#define S2_OFFSET 4
#define S3_OFFSET 5
#define S4_OFFSET 6
#define S5_OFFSET 7
#define S6_OFFSET 8
#define S7_OFFSET 9
#define GP_OFFSET 10
#define SP_OFFSET 11
#define FP_OFFSET 12
#define RA_OFFSET 13
#define C0_SR_OFFSET 14

#define INT_CONTEXT_CONTROL_SIZE 140

#define INT_AT 15
#define INT_V0 16
#define INT_V1 17
#define INT_A0 18
#define INT_A1 19
#define INT_A2 20
#define INT_A3 21
#define INT_T0 22
#define INT_T1 23
#define INT_T2 24
#define INT_T3 25
#define INT_T4 26
#define INT_T5 27
#define INT_T6 28
#define INT_T7 29
#define INT_T8 30
#define INT_T9 31
#define INT_K1 32
#define INT_MULHI 33
#define INT_MULLO 34

#endif /* #ifdef ASM_LANGUAGE */

#ifndef ASM_LANGUAGE

#include <reworks/types.h>

#define CPU_STACK_MIN_SIZE          4096
#define CPU_ALIGNMENT               8
#define CACHE_ALIGNMENT             32

#define _LITTLE_ENDIAN			4321
#define _BIG_ENDIAN				1234
#define _BYTE_ORDER				_LITTLE_ENDIAN

typedef struct 
{
  void       (*idle_task)( void );
  boolean      do_zero_of_workspace;
  u32   idle_task_stack_size;
  u32   interrupt_stack_size;
}   rtos_cpu_table;

typedef struct 
{
	u32 type;
	u32 pc;
    u32 s0;
    u32 s1;
    u32 s2;
    u32 s3;
    u32 s4;
    u32 s5;
    u32 s6;
    u32 s7;
    u32 gp;
    u32 sp;
    u32 fp;
    u32 ra;
    u32 c0_sr;
	u32 AT;
	u32 v0;
	u32 v1;
	u32 a0;
	u32 a1;
	u32 a2;
	u32 a3;
    u32 t0;
    u32 t1;
    u32 t2;
    u32 t3;
    u32 t4;
    u32 t5;
    u32 t6;
    u32 t7;
    u32 t8;
    u32 t9;
    u32 k1;
    u32 mulhi;
    u32 mullo;
} Context_Ctrl;

typedef Context_Ctrl REG_SET;

void context_init(Context_Ctrl *the_context, u32 isr, void *entry_point, void *prev_entry);
void context_switch(Context_Ctrl **run, Context_Ctrl *heir);
void context_restore(Context_Ctrl *heir);

#if 0
#define __sti() \
	__asm__ __volatile__( \
		".set\tpush\n\t" \
		".set\treorder\n\t" \
		".set\tnoat\n\t" \
		"mfc0\t$1,$12\n\t" \
		"ori\t$1,0x1f\n\t" \
		"xori\t$1,0x1e\n\t" \
		"mtc0\t$1,$12\n\t" \
		".set\tpop\n\t" \
		: /* no outputs */ \
		: /* no inputs */ \
		: "$1", "memory")

/*
 * For cli() we have to insert nops to make shure that the new value
 * has actually arrived in the status register before the end of this
 * macro.
 * R4000/R4400 need three nops, the R4600 two nops and the R10000 needs
 * no nops at all.
 */
#define __cli() \
	__asm__ __volatile__( \
		".set\tpush\n\t" \
		".set\treorder\n\t" \
		".set\tnoat\n\t" \
		"mfc0\t$1,$12\n\t" \
		"ori\t$1,1\n\t" \
		"xori\t$1,1\n\t" \
		".set\tnoreorder\n\t" \
		"mtc0\t$1,$12\n\t" \
		"nop\n\t" \
		"nop\n\t" \
		"nop\n\t" \
		".set\tpop\n\t" \
		: /* no outputs */ \
		: /* no inputs */ \
		: "$1", "memory")

#define __save_flags(x)                  \
__asm__ __volatile__(                    \
	".set\tpush\n\t"		 \
	".set\treorder\n\t"              \
	"mfc0\t%0,$12\n\t"               \
	".set\tpop\n\t"                      \
	: "=r" (x)                       \
	: /* no inputs */                \
	: "memory")

#define __save_and_cli(x)                \
__asm__ __volatile__(                    \
	".set\tpush\n\t"		 \
	".set\treorder\n\t"              \
	".set\tnoat\n\t"                 \
	"mfc0\t%0,$12\n\t"               \
	"ori\t$1,%0,1\n\t"               \
	"xori\t$1,1\n\t"                 \
	".set\tnoreorder\n\t"		 \
	"mtc0\t$1,$12\n\t"               \
	"nop\n\t"                        \
	"nop\n\t"                        \
	"nop\n\t"                        \
	".set\tpop\n\t"                  \
	: "=r" (x)                       \
	: /* no inputs */                \
	: "$1", "memory")

#define __restore_flags(x) \
	__asm__ __volatile__( \
		".set\tpush\n\t" \
		".set\treorder\n\t" \
		"mfc0\t$8,$12\n\t" \
		"li\t$9,0xff00\n\t" \
		"and\t$8,$9\n\t" \
		"nor\t$9,$0,$9\n\t" \
		"and\t%0,$9\n\t" \
		"or\t%0,$8\n\t" \
		".set\tnoreorder\n\t" \
		"mtc0\t%0,$12\n\t" \
		"nop\n\t" \
		"nop\n\t" \
		"nop\n\t" \
		".set\tpop\n\t" \
		: \
		: "r" (x) \
		: "$8", "$9", "memory")

/*
 * Non-SMP versions ...
 */
#define sti() __sti()
#define cli() __cli()
#define save_flags(x) __save_flags(x)
#define save_and_cli(x) __save_and_cli(x)
#define restore_flags(x) __restore_flags(x)

/* For spinlocks etc */
#define local_irq_save(x)	__save_and_cli(x);
#define local_irq_restore(x)	__restore_flags(x);
#define local_irq_disable()	__cli();
#define local_irq_enable()	__sti();
#endif

#define __swap16gen(x) ({						\
	u16 __swap16gen_x = (x);					\
									\
	(u16)((__swap16gen_x & 0xff) << 8 |			\
	    (__swap16gen_x & 0xff00) >> 8);				\
})

#define __swap32gen(x) ({						\
	u32 __swap32gen_x = (x);					\
									\
	(u32)((__swap32gen_x & 0xff) << 24 |			\
	    (__swap32gen_x & 0xff00) << 8 |				\
	    (__swap32gen_x & 0xff0000) >> 8 |				\
	    (__swap32gen_x & 0xff000000) >> 24);			\
})

#define CPU_swap_u16( _value )  __swap16gen(_value)
#define CPU_swap_u32( _value )  __swap32gen(_value)

/* 这是一个叶子函数，它用于取得状态寄存器的值。*/
extern u32 get_c0_sr();

/* 这是一个叶子函数，它用于设置状态寄存器的值。*/
extern void set_c0_sr(u32);

/* 这是一个叶子函数，它用于取得计数寄存器的值。*/
extern u32 get_c0_count();

/**
 * 获取处理器时间戳接口
 */
extern u64 sys_timestamp (void);

/**
 * 获取处理器频率的接口
 */
extern u32 sys_timestamp_freq(void);

#define MEM_BARRIER_R() __asm volatile ("sync" ::: "memory")
#define MEM_BARRIER_W() __asm volatile ("sync" ::: "memory")
#define MEM_BARRIER_RW() __asm volatile ("sync" ::: "memory")

extern SYSTEM_BASIC_INFO *sys_info_get(void);

#endif /* #ifndef ASM_LANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* _REWORKS_CPU_H_ */
