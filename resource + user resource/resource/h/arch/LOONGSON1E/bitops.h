#ifndef _REWORKS_BITOPS_H
#define _REWORKS_BITOPS_H

/********************************************************************
 * huangyuan20080414: 
 * platform-independent code added for threads-scheduling.. etc.
 */
#include <cpu.h>
#include <reworks/types.h>

extern u8 ffs_lsb_tbl[];

#define ffs16( _value, _output ) \
{ \
    register u32 __value = (u32) (_value); \
    register const unsigned char *__p = ffs_lsb_tbl; \
    \
    if ( __value & 0xff ) \
      (_output) = __p[ __value & 0xff]; \
    else \
      (_output) = __p[ __value >> 8 ] + 8; \
}

#define SZLONG_LOG 5
#define SZLONG_MASK 31UL
#define __LL		"ll	"
#define __SC		"sc	"


#define __bi_flags
#define __bi_local_irq_save(x)
#define __bi_local_irq_restore(x)

/*
 * set_bit - Atomically set a bit in memory
 * @nr: the bit to set
 * @addr: the address to start counting from
 *
 * This function is atomic and may not be reordered.  See __set_bit()
 * if you do not require the atomic guarantees.
 * Note that @nr may be almost arbitrarily large; this function is not
 * restricted to acting on a single-word quantity.
 */
static inline void set_bit(unsigned long nr, volatile unsigned long *addr)
{
		volatile unsigned long *a = addr;
		unsigned long mask;
		__bi_flags;

		a += nr >> SZLONG_LOG;
		mask = 1UL << (nr & SZLONG_MASK);
		__bi_local_irq_save(flags);
		*a |= mask;
		__bi_local_irq_restore(flags);
}

/*
 * clear_bit - Clears a bit in memory
 * @nr: Bit to clear
 * @addr: Address to start counting from
 *
 * clear_bit() is atomic and may not be reordered.  However, it does
 * not contain a memory barrier, so if it is used for locking purposes,
 * you should call smp_mb__before_clear_bit() and/or smp_mb__after_clear_bit()
 * in order to ensure changes are visible on other processors.
 */
static inline void clear_bit(unsigned long nr, volatile unsigned long *addr)
{
		volatile unsigned long *a = addr;
		unsigned long mask;
		__bi_flags;

		a += nr >> SZLONG_LOG;
		mask = 1UL << (nr & SZLONG_MASK);
		__bi_local_irq_save(flags);
		*a &= ~mask;
		__bi_local_irq_restore(flags);
}

/*
 * change_bit - Toggle a bit in memory
 * @nr: Bit to change
 * @addr: Address to start counting from
 *
 * change_bit() is atomic and may not be reordered.
 * Note that @nr may be almost arbitrarily large; this function is not
 * restricted to acting on a single-word quantity.
 */
static inline void change_bit(unsigned long nr, volatile unsigned int/*long*/ *addr)
{
		volatile unsigned int/*long*/ *a = addr;
		unsigned long mask;
		__bi_flags;

		a += nr >> SZLONG_LOG;
		mask = 1UL << (nr & SZLONG_MASK);
		__bi_local_irq_save(flags);
		*a ^= mask;
		__bi_local_irq_restore(flags);
}

/*
 * test_and_set_bit - Set a bit and return its old value
 * @nr: Bit to set
 * @addr: Address to count from
 *
 * This operation is atomic and cannot be reordered.
 * It also implies a memory barrier.
 */
static inline int test_and_set_bit(unsigned long nr,
	volatile unsigned long *addr)
{
		volatile unsigned long *a = addr;
		unsigned long mask;
		int retval;
		__bi_flags;

		a += nr >> SZLONG_LOG;
		mask = 1UL << (nr & SZLONG_MASK);
		__bi_local_irq_save(flags);
		retval = (mask & *a) != 0;
		*a |= mask;
		__bi_local_irq_restore(flags);

		return retval;
}

/*
 * test_and_clear_bit - Clear a bit and return its old value
 * @nr: Bit to clear
 * @addr: Address to count from
 *
 * This operation is atomic and cannot be reordered.
 * It also implies a memory barrier.
 */
static inline int test_and_clear_bit(unsigned long nr,
	volatile unsigned long *addr)
{
		volatile unsigned long *a = addr;
		unsigned long mask;
		int retval;
		__bi_flags;

		a += nr >> SZLONG_LOG;
		mask = 1UL << (nr & SZLONG_MASK);
		__bi_local_irq_save(flags);
		retval = (mask & *a) != 0;
		*a &= ~mask;
		__bi_local_irq_restore(flags);

		return retval;
}

#define BITS_PER_LONG 32
#define BITOP_WORD(nr)	((nr) / BITS_PER_LONG)
/**
 * test_bit - Determine whether a bit is set
 * @nr: bit number to test
 * @addr: Address to start counting from
 */
static inline int test_bit(int nr, const volatile unsigned long *addr)
{
	return 1UL & (addr[BITOP_WORD(nr)] >> (nr & (BITS_PER_LONG-1)));
}

/*
 * test_and_change_bit - Change a bit and return its old value
 * @nr: Bit to change
 * @addr: Address to count from
 *
 * This operation is atomic and cannot be reordered.
 * It also implies a memory barrier.
 */
static inline int test_and_change_bit(unsigned long nr,
	volatile unsigned int/*long*/ *addr)
{
		volatile unsigned int/*long*/ *a = addr;
		unsigned long mask, retval;
		__bi_flags;

		a += nr >> SZLONG_LOG;
		mask = 1UL << (nr & SZLONG_MASK);
		__bi_local_irq_save(flags);
		retval = (mask & *a) != 0;
		*a ^= mask;
		__bi_local_irq_restore(flags);

		return retval;
}

#undef __bi_flags
#undef __bi_local_irq_save
#undef __bi_local_irq_restore

#endif /* _REWORKS_BITOPS_H */
