
/*! @file spinlock.h
    @brief ReWorks系统自旋锁头文件
     
 * 本文件定义了ReWorks系统用于互斥和同步的自旋锁的操作。
 * 
 * @version 4.7
 * 
 * @see 无
 */
#ifndef _SPINLOCK_H_
#define _SPINLOCK_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <cpu.h>
#include <atomic.h>

/**
 * @addtogroup group_spinlock
 * 
 * spinlock为ReWorks 多核扩展提供了一种用于实现短期互斥和同步操作的措施，它们必须被显式地获取或者释放。
 * 
 * ReWorks在获取自旋锁和释放自旋锁之间采用完全内存屏障，完全内存屏障保证读内存和写内存完全按照顺序执行，
 * 在占用自旋锁期间对数据结构的更新可以完全处理完毕。在ReWorks SMP当前自旋锁可以分成两类：<br/>
 * (1)关闭中断的自旋锁：它用于中断ISR之间，以及中断ISR与任务之间的竞争。在获取中断自旋锁期间它会锁本核中断，
                若被任务调用，它们将禁止本核上的任务抢占；<br/>
   (2)禁止抢占的自旋锁：用于任务之间，它们将禁止本核任务抢占。
 * @{*/

/*! \struct spinlock_t
 * \brief 普通自旋锁结构定义
 *
 */

typedef struct{
	atomic_t lock; //!< 代表锁的原子值
	int level;     //!< 解锁中断的状态值
	char * func;   //!< 调用函数名
	int line;      //!< 调用函数行号
}spinlock_t; //!< 普通自旋锁结构体

/*! \struct spin_lock_cpu
 * \brief 实时确定性自旋锁各核数据的结构定义
 *
 */
typedef struct
{
	atomic_t flag; //!< 当前核获取锁的状态
	unsigned int nest; //!< 同一核上试图获取锁的次数 
} spin_lock_cpu; //!< 确定性实时自旋锁结构各核数据的结构体

/*! \struct spinlock_irq_rt_dtm_t
 * \brief 实时确定性自旋锁结构定义
 *
 */
typedef struct
{
	atomic_t owner; //!< 锁的拥有者
	int level; //!< 解锁中断的状态值
	int ints_done; //!< 拥有者核获取锁时处理的中断个数
	void * thread; //!< 拥有者任务
	spin_lock_cpu cpu[MAX_SMP_CPUS]; //!< 各核数据
} spinlock_rt_dtm_t; //!< 确定性实时自旋锁结构体

/** 
 * @brief  普通自旋锁初始化
 * 
 * @param 	lock 普通自旋锁
 * 
 * @return 	无
 * 			
 */
void spin_lock_init(spinlock_t *lock);

/** 
 * @brief  获取朴素自旋锁
 * 
 * @param 	lock 普通自旋锁
 * 
 * @return 	无
 * 
 * @note 不附带任何同步互斥机制的的纯粹自旋锁，仅限于高级用户按需使用，不建议一般用户使用。
 * 			
 */
void spin_lock(spinlock_t *lock);

/** 
 * @brief  释放朴素自旋锁
 * 
 * @param 	lock 普通自旋锁
 * 
 * @return 	无
 * 			
 */
void spin_unlock(spinlock_t *lock);

/** 
 * @brief  获取任务级自旋锁
 * 
 * @param 	lock 普通自旋锁
 * 
 * @return 	无
 * 
 * @note 在自旋与获取期间，核上中断始终任务抢占，释放时解除抢占。
 * 任务级自旋锁仅限于任务间的互斥，中断上下文中调用可能会引起死锁。
 * 			
 */
void spin_lock_thr(spinlock_t *lock);

/** 
 * @brief  释放任务级自旋锁
 * 
 * @param 	lock 普通自旋锁
 * 
 * @return 	无
 * 			
 */
void spin_unlock_thr(spinlock_t *lock);

/** 
 * @brief  获取关闭中断的自旋锁
 * 
 * @param 	lock 自旋锁
 * 
 * @return 	无
 * 
 * @note 在自旋与获取期间，核上中断始终屏蔽，释放时解除屏蔽。			
 */
void spin_lock_irq(spinlock_t *lock);

/** 
 * @brief  释放关闭中断的自旋锁
 * 
 * @param 	lock 普通自旋锁
 * 
 * @return 	无
 * 			
 */
void spin_unlock_irq(spinlock_t *lock);

/** 
 * @brief  带死锁检测的获取中断自旋锁
 * 
 * @param 	lock 普通自旋锁
 * @param 	func 调用函数名
 * @param 	line 调用函数行号
 * 
 * @return 	无
 * 
 * @note 释放依旧可以使用spin_unlock_irq()
 */
void spin_lock_irq_d(spinlock_t *lock, const char * func, int line);

/** 
 * @brief  获取实时自旋锁
 * 
 * @param 	lock 普通自旋锁
 * 
 * @return 	无
 * 
 * @note 实时性是指在自旋了一定时间之后，会间歇性解除中断屏蔽，允许
 * 中断与任务抢占，一定程度上保证系统的实时性。
 * 			
 */
void spin_lock_rt(spinlock_t *lock);


/** 
 * @brief  释放实时自旋锁
 * 
 * @param 	lock 普通自旋锁
 * 
 * @return 	无
 * 			
 */
void spin_unlock_rt(spinlock_t *lock);

/** 
 * @brief  实时确定性自旋锁初始化
 * 
 * @param 	lock 实时确定性自旋锁
 * 
 * @return 	无
 * 			
 */
void spin_lock_rt_dtm_init(spinlock_rt_dtm_t *lock); 

/** 
 * @brief  获取实时确定性自旋锁
 * 
 * @param 	lock 实时确定性自旋锁
 * 
 * @return 	无
 * 
 * @note 这里的确定性是指，在自旋时，并不会无休止地屏蔽中断，
 * 而是会在超过确定的间隔时放开中断屏蔽，但仍禁止任务抢占，
 * 等待的各处理器之间按顺序轮流获得自旋锁，这样可以一定程度上
 * 维护系统的响应性确定性行为。
 * 			
 */
void spin_lock_rt_dtm(spinlock_rt_dtm_t *lock);

/** 
 * @brief  释放实时确定性自旋锁
 * 
 * @param 	lock 实时确定性自旋锁
 * 
 * @return 	无
 */
void spin_unlock_rt_dtm(spinlock_rt_dtm_t *lock);

/* @}*/

#ifdef __cplusplus
}
#endif


#endif

