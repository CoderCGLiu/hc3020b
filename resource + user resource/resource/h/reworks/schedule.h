#ifndef __SCHEDULE_H__
#define __SCHEDULE_H__

#ifdef __cplusplus
extern "C"
{
#endif 

#include <cpu.h> /* Context_Ctrl * */
#include <reworks/types.h>

#ifdef __sparcv8__
Context_Ctrl* schedule( boolean flag );
#else
void schedule(boolean);
#endif

u32 get_rr_slice();
OS_STATUS kernel_timeslice_set(int ticks);
u32 kernel_timeslice_get();
void reschedule();

#ifdef __sparcv8__
#ifdef __multi_core__
void save_thread_context(int cpu, int irq_no, Context_Ctrl *context);
Context_Ctrl *context_restore_or_switch_to_another_thread_if_necessary(int cpu,Context_Ctrl *context);
#else
Context_Ctrl *context_restore_or_switch_to_another_thread_if_necessary(Context_Ctrl *unsaved_context);
#endif

#else
#ifdef __multi_core__
void save_thread_context(int cpu, int irq_no, Context_Ctrl *context);
void context_restore_or_switch_to_another_thread_if_necessary(int cpu, Context_Ctrl *context);
#else
void context_restore_or_switch_to_another_thread_if_necessary(Context_Ctrl *unsaved_context);
#endif
#endif
#ifdef __cplusplus
}

#endif 

#endif
