#ifdef __multi_core__
/*******************************************************************************
 * 
 * 版          权：	中国电子科技集团公司第三十二研究所
 * 功能描述：	
 * 调用接口：	
 * 运行条件：	
 * 运行方式：	
 * 修改日期：	2015-1-13
 * 
 */
#ifndef _CPC_H_
#define _CPC_H_
#ifdef __cplusplus
extern "C" 
{
#endif

#include <cpuset.h>

/**
 * @addtogroup group_cpc ReWorks核间调用模块
 * @{*/

#define REWORKS_CPC_SYNC        1  //!< 主叫核直到被叫核执行完成CPC call函数后才解除等待。
#define REWORKS_CPC_ASYNC       2  //!< 主叫核在被叫核执行CPC call函数前解除等待
#define REWORKS_CPC_SYNC_MANUAL 3  //!< 由CPC call函数来处理主叫核和被叫核之间的同步，该同步可通过调用cpc_sync()函数完成。

typedef void (*CPC_FUNC) (void *);  //!< cpc函数指针类型

/** 
 * @brief  当前CPU使其他某个CPU产生中断并执行由cpc_call指定的函数。
 * 
 * 	该接口在主叫核需要其他核执行指定的函数时使用，主叫核通过向被叫核发送核间中断并指定中断处理
 * 	函数所需执行的代码接口，来实现上述需求。该函数参数flags指定了三种同步机制：
 * 	(1)REWORKS_CPC_ASYNC
 * 		它表示主叫核在被叫核执行CPC call函数前结束cpc_call函数调用。
 * 	(2)REWORKS_CPC_SYNC
 * 		该模式下主叫核只有在被叫核执行完成CPC call函数后才结束cpc_call函数调用。
 * 	(3)REWORKS_CPC_SYNC_MANUAL
 *  	这种模式下，主叫核上cpc_call函数的完成取决于CPC call函数自身。因为CPC中断处理函数
 *  不提供同步。该调用需要调用cpc_sync()来确保CPC操作已经完成。
 *  
 * @param 	cpu 	需要产生核间中断的CPU ID
 * @param 	func	cpc函数指针
 * @param 	param	cpc函数调用参数
 * @param 	flags	同步模式:REWORKS_CPC_ASYNC, REWORKS_CPC_SYNC 或 REWORKS_CPC_SYNC_MANUAL
 * 
 * @return 	无
 * 			
 */
void cpc_call(int cpu, CPC_FUNC func,void* param, int flags);

/** 
 * @brief  主叫核使其他一组CPU产生中断并执行由cpc_broadcast指定的函数。
 * 
 * 	该接口在主叫核需要其他核执行指定的函数时使用，主叫核通过向被叫核发送核间中断并指定中断处理
 * 	函数所需执行的代码接口，来实现上述需求。该函数参数flags指定了三种同步机制：
 * 	(1)REWORKS_CPC_ASYNC
 * 		它表示主叫核在被叫核执行CPC call函数前结束cpc_call函数调用。
 * 	(2)REWORKS_CPC_SYNC
 * 		该模式下主叫核只有在被叫核执行完成CPC call函数后才结束cpc_call函数调用。
 * 	(3)REWORKS_CPC_SYNC_MANUAL
 *  	这种模式下，主叫核上cpc_call函数的完成取决于CPC call函数自身。因为CPC中断处理函数
 *  不提供同步。该调用需要调用cpc_sync()来确保CPC操作已经完成。
 *  
 * @param 	cpuset 	需要产生核间中断的cpu集合
 * @param 	func	cpc函数指针
 * @param 	param	cpc函数调用参数
 * @param 	flags	同步模式:REWORKS_CPC_ASYNC, REWORKS_CPC_SYNC 或 REWORKS_CPC_SYNC_MANUAL
 *  
 * @return 	无
 * 			
 */
void cpc_broadcast(cpuset_t cpuset, CPC_FUNC func,void* param, int flags);

/** 
 * @brief  指示CPC操作完成。
 * 该函数用于主叫核和被叫核的同步。当在REWORKS_CPC_SYNC_MANUAL同步模式下，CPC call函数调用该接口
 * 来表明CPC call请求的完成。
 * 
 * @param 	无
 *  
 * @return 	无
 * 			
 */
void cpc_sync(void);


/** @example example_cpc_call_1.c
 * 下面的例子演示了如何使用 cpc_call().
 */
/** @example example_cpc_call_2.c
 * 下面的例子演示了如何使用 cpc_call().
 */
/** @example example_cpc_call_3.c
 * 下面的例子演示了如何使用 cpc_call(), cpc_sync().
 */
/** @example example_cpc_broadcast_1.c
 * 下面的例子演示了如何使用 cpc_broadcast().
 */
/** @example example_cpc_broadcast_2.c
 * 下面的例子演示了如何使用 cpc_broadcast().
 */
/** @example example_cpc_broadcast_3.c
 * 下面的例子演示了如何使用 cpc_broadcast(), cpc_sync().
 */


/* @}*/


#ifdef __cplusplus
}
#endif
#endif /* _CPC_H_ */

#endif /* __multi_core__ */
