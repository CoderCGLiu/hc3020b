#ifndef __REWORKS_TYPES_h
#define __REWORKS_TYPES_h

#ifdef __cplusplus
extern "C"
{
#endif 

#include <stdint.h>
#include <sys/types.h>
	
//#define VX55_NOT_COMPAT_64BIT /* 在地址空间为64位的系统下定义 */
#undef VX55_NOT_COMPAT_64BIT /* 默认，在地址空间为32位的系统下定义 */
	
typedef unsigned long block_t;

#if 0
typedef struct ObjectManagerRecord Object_Manager;
#endif

typedef struct heap_mem_part *PART_ID;
//typedef struct mpart_stats MEM_PART_STATS; //该结构体仅在vx兼容层使用，故放入memLib.h中

typedef int WaitQ_Type;

typedef unsigned long 	wdg_t;
typedef long			thread_arg_t;
typedef unsigned long			thread_exit_t;
typedef __int64_t		ticks64_t;
typedef int				ticks_t;
typedef unsigned long 	reg_t;

typedef struct Mutex_Ctrl Mutex_Ctrl;

#define  REG_BASE	0x00	/* data reg's base offset to REG_SET */
#define  REG_OFFSET(n)	(REG_BASE + (n)*sizeof(u32))
#define  REG64_OFFSET(n)	(REG_BASE + (n)*sizeof(u64))

typedef struct regs_index
{
    char	*reg_name;	/* pointer to register name */
    int		reg_off;		/* offset to entry in REG_SET */
 } REGS_INDEX;

#define LIKELY(X) __builtin_expect(!!(X), 1)
#define UNLIKELY(X) __builtin_expect(!!(X), 0)

#define OFFSET(structure, member)	/* byte offset of member in structure*/\
		((ptrdiff_t) &(((structure *) 0) -> member))

#ifdef REWORKS_LP64
typedef long _re_usr_arg_t;
#else
typedef int _re_usr_arg_t;
#endif /* REWORKS_LP64 */

#ifdef __cplusplus
typedef int 		(*FUNCPTR) (...);     /* ptr to function returning int */
typedef void 		(*VOIDFUNCPTR) (...); /* ptr to function returning void */
#else
typedef int 		(*FUNCPTR) ();	   	  /* ptr to function returning int */
typedef void 		(*VOIDFUNCPTR) ();    /* ptr to function returning void */
#endif			/* _cplusplus */

#define DBG_ERR(x)	x
#define	roundup(x, align)	 (( (x) + (align - 1)) & ~(align - 1))
#define rounddown(x, align)	 ((x) & ~(align - 1))
#ifdef __cplusplus
}
#endif 

#endif
