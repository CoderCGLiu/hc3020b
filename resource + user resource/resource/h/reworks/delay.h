#ifndef __REWORKS_DELAY_H__
#define __REWORKS_DELAY_H__

#ifdef __cplusplus
extern "C"
{
#endif 

#include <udelay.h>



#ifndef MAX_UDELAY_MS
#define MAX_UDELAY_MS	5
#endif 



#ifdef notdef
#define mdelay(n) (\
{unsigned long __ms=(n); while (__ms--) udelay(1000);})
#else 
#define mdelay(n) (\
(__builtin_constant_p(n) && (n)<=MAX_UDELAY_MS) ? udelay((n)*1000) : \
({unsigned long __ms=(n); while (__ms--) udelay(1000);}))
#endif 

#ifndef ndelay
#define ndelay(x)	udelay(((x)+999)/1000)
#endif 

#ifdef __cplusplus
}

#endif 

#endif /* __REWORKS_DELAY_H__ */
