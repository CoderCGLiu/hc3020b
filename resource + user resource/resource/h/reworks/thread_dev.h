/*******************************************************************************
 * 
 * 版          权：	中国电子科技集团公司第三十二研究所
 * 功能描述：	开发工具使用的和任务相关的头文件
 * 修改日期：	2013-3-29
 * 
 */

#ifndef THREAD_DEV_H_
#define THREAD_DEV_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <reworks/types.h>
#include <pthread.h>

extern pthread_t init_thread; /* 初始化任务的ID */
extern pthread_t idle_thread; /* 空闲任务的ID */

OS_STATUS pthread_debugger_suspend(pthread_t id);
OS_STATUS pthread_debugger_resume(pthread_t id);
OS_BOOLEAN pthread_is_debugger_suspended(pthread_t id);

#ifdef __cplusplus
}
#endif

#endif /* THREAD_DEV_H_ */
