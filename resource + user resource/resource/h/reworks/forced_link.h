
#ifndef _REWORKS_FORCED_LINK_H_
#define _REWORKS_FORCED_LINK_H_

#ifdef __cplusplus
extern "C" {
#endif

#define FORCED_LINKAGE_SYMBOL(x) static void (* __##x##_fp)(void) = (void (*)(void))&x;

#ifdef __cplusplus
}
#endif

#endif /* FORCED_LINK.H */

