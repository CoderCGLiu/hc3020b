/*******************************************************************************
 * 
 * 版权：             中国电子科技集团公司第三十二研究所
 * 描述：            本文件为监测cpu使用率定义头文件
 * 
 */

#ifndef REWORKS_MONITOR_CPU_H_INCLUDED__
#define REWORKS_MONITOR_CPU_H_INCLUDED__

#ifdef __cplusplus
extern "C" 
{
#endif
#include <cpu.h>
#include <cpuset.h>
#include <reworks/thread.h>

#ifdef __multi_core__
/* 2013-1-19 added by yellowriver for monitor */
//CPU开启情况表，低四位，每一位为1表示该核已开启
extern cpuset_t cpuset_up;
#endif

typedef struct cpuuse_result_node
{
	char name[32];					//任务名
	thread_t  tid;       				//任务tid
	u64  clock_cnt[MAX_SMP_CPUS]; 	//任务tid在core上的执行时间
	float 		cpuuse_rate[MAX_SMP_CPUS]; //任务tid在core上的利用率
	struct cpuuse_result_node *next;
}cpuuse_node;

typedef struct cpuuse_result_head
{
	u64 count[MAX_SMP_CPUS];        //记录各个CPU上总的执行时间
	cpuuse_node *head;				//记录每个任务各个CPU核上 的执行时间
}cpuuse_head;

extern int thread_cpu_use_module_init();

/** 
 * @brief 获取idle任务id
 * 
 * 该接口用于获取获取指定核上idle任务的id
 * 
 * @param 	core 核ID
 * 
 * @return	u32 idle任务id
 * 
 */
ptrdiff_t thread_get_idle_id(u32 core);

/** 
 * @brief 打印64位数
 * 
 * 该接口用于打印64位数
 * 
 * @param 	u64 num
 * 
 * @return	无
 * 
 */
void showU64(u64 num);

/** 
 * @brief 开始监控系统中CPU的使用情况
 * @param 无
 * @return  0 获取数据成功
 * @return -1 获取任务失败
 */
int cpu_utilization_monitor_start();

/** 
 * @brief   停止监控
 * @param   无
 * @return  0 获取数据成功
 * @return -1 获取任务失败
 */
int cpu_utilization_monitor_stop();

/** 
 * @brief   初始化CPU使用监视数据链表头
 * @param   cpu_info_head 指向CPU监控数据链表的头地址
 * @return  0 获取数据成功
 * @return -1 获取任务失败
 */
int cpu_utilization_monitor_data_init(cpuuse_head *cpu_info_head);

/** 
 * @brief 该函数获得从开始监控时刻cpu_utilization_monitor_start()
   或者上次调用该函数的时刻至此次调用期间，系统中各个CPU的使用信息
 * @param cpu_info_head 指向CPU监控数据链表的头地址
 * @return  >0 获取当前采集到的任务数
 * @return -1 获取任务失败
 */
int cpu_utilization_monitor_get_data(cpuuse_head *cpu_info_head);

/** 
 * @brief   释放CPU使用监视数据链表占用的内存
 * @param   cpu_info_head 指向CPU监控数据链表的头地址
 * @return  0 获取数据成功
 * @return -1 获取任务失败
 */
int cpu_utilization_monitor_data_free(cpuuse_head *cpu_info_head);

/** 
 * @brief   显示CPU使用监视数据
 * @param   sec 监视周期（1秒到30秒之间）
 * @return  无
 */
void cpuuse_show(int sec);

#ifdef __cplusplus
}
#endif
#endif

