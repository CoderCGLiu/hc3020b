#ifndef REDE_SYSTEMVIEWER_H
#define REDE_SYSTEMVIEWER_H

#ifndef REDE_SYSTEMVIEWER
#define REDE_SYSTEMVIEWER
#endif

#ifdef __cplusplus
extern "C"
{
#endif 

/* 该文件包含了用于系统查看器、资源浏览器功能相关的宏定义 */

#include <cpu.h>

#define CLASS_NONE    			0x00000000 

#define TV_CLASS_1    			0x00000001   /* 上下文切换 */
#define TV_CLASS_2    			0x00000003   /* 任务状态转换 */
#define TV_CLASS_3    			0x00000007   /* 对象和系统库 */
#define TV_CLASS_TASKNAMES_PRESERVE	0x00001000
#define TV_ON           		0x10000000
#define TV_CLASS_1_ON  			0x10000001
#define TV_CLASS_2_ON 			0x10000003
#define TV_CLASS_3_ON  			0x10000007
#define TV_CLASS_TASKNAMES_PRESERVE_ON	0x10001000


/* types */

#ifndef _ASMLANGUAGE

typedef unsigned short event_t;

#include "reworks/types.h"

typedef int(*_FUNCPTR)(); /* ptr to function returning int */
typedef void(*VOID_FUNCPTR)(); /* ptr to function returning void */
typedef double(*DBL_FUNCPTR)(); /* ptr to function returning double*/
typedef float(*FLT_FUNCPTR)(); /* ptr to function returning float */

extern VOID_FUNCPTR _func_evt_log_O;
extern VOID_FUNCPTR _func_evt_log_O_intlock;

extern VOID_FUNCPTR _func_evt_log_m0;
extern VOID_FUNCPTR _func_evt_log_m1;
extern VOID_FUNCPTR _func_evt_log_m2;
extern VOID_FUNCPTR _func_evt_log_m3;

extern VOID_FUNCPTR _func_evt_log_t0;
extern VOID_FUNCPTR _func_evt_log_t0_noint;
extern VOID_FUNCPTR _func_evt_log_t1;
extern VOID_FUNCPTR _func_evt_log_t1_nots;
extern VOID_FUNCPTR _func_evt_log_t_sched;

extern VOID_FUNCPTR _func_evt_log_string;
extern _FUNCPTR _func_evt_log_point;
extern _FUNCPTR _func_evt_log_reserve_taskname;

extern boolean tvInstIsOn; /* windview instrumentation ON/OFF */
extern boolean tvObjIsEnabled; /* Level 1 event collection enable */

//extern VOID_FUNCPTR _func_trgCheck;
extern u32 evt_action;
extern u32 sv_evt_class;
//extern u32 trgEvtClass;

#define ACTION_IS_SET			(evt_action != 0)

#define TV_ACTION_SET                   \
{                               \
evt_action |= 0x0100;             \
TV_EVTCLASS_SET(TV_ON);         \
}

#define TV_ACTION_UNSET                 \
{                               \
TV_EVTCLASS_UNSET(TV_ON);               \
evt_action &= ~(0x0100);            \
}

#define TV_ACTION_IS_SET		( (evt_action&0xff00) == 0x0100)

#define TV_EVTCLASS_IS_SET(class) 	((sv_evt_class&(class)) == (class))
#define TV_EVTCLASS_SET(class) 		(sv_evt_class |= (class))
#define TV_EVTCLASS_UNSET(class)    (sv_evt_class &= ~(class))
#define TV_EVTCLASS_EMPTY	 		(sv_evt_class = CLASS_NONE)

/*
 * EVT_TASK_CTX_0 - 任务上下文切换事件日志
 */

#define EVT_TASK_CTX_0(evtId)                   \
if (UNLIKELY(ACTION_IS_SET))					\
{                                               \
if ( TV_EVTCLASS_IS_SET(TV_CLASS_1|TV_ON) )     \
( * _func_evt_log_t0) (evtId);                  \
}


/*
 * EVT_TASK_CTX_1 - 任务上下文切换事件日志（带1个参数）
 */

#define EVT_TASK_CTX_1(evtId, arg)              \
if (UNLIKELY(ACTION_IS_SET))					\
{                                               \
if ( TV_EVTCLASS_IS_SET(TV_CLASS_1|TV_ON) )     \
( * _func_evt_log_t1) (evtId, arg);             \
}



/*
 * EVT_TASK_CTX_IDLE - EVENT_THREAD_EXIT_IDLE事件日志
 */

#define EVT_TASK_CTX_IDLE(evtId, arg1)          \
if ( ACTION_IS_SET && (Q_FIRST (arg1) == NULL))	\
{                                               \
if (TV_EVTCLASS_IS_SET(TV_CLASS_1|TV_ON))		\
(* _func_evt_log_t0) (evtId);                 	\
}


/*
 * EVT_TASK_CTX_DISP - 重新调度的任务上下文切换事件日志  
 */


#define EVT_TASK_CTX_DISP(arg1, arg2, arg3)           	    \
if (UNLIKELY(ACTION_IS_SET))								\
{                                                       	\
if (arg3 > arg2)                                            \
{	    	                                                \
if (TV_EVTCLASS_IS_SET(TV_CLASS_1|TV_ON))			        \
(* _func_evt_log_t_sched) (EVENT_THREAD_EXIT_DISPATCH_PI, arg1, arg2);  \
}                                                           \
else                                                        \
{                                                           \
if (TV_EVTCLASS_IS_SET(TV_CLASS_1|TV_ON))			        \
(* _func_evt_log_t_sched) (EVENT_THREAD_EXIT_DISPATCH, arg1, arg2);     \
}                                                           \
}

/*
 * EVT_TASK_CTX_NODISP - 重新调度的任务上下文切换事件日志  
 */

#define EVT_TASK_CTX_NODISP(arg1, arg2, arg3)           	\
if (UNLIKELY(ACTION_IS_SET))                                \
{                                                           \
if (arg3 > arg2)                                            \
{                                                           \
if (TV_EVTCLASS_IS_SET(TV_CLASS_1|TV_ON))			        \
(* _func_evt_log_t_sched) (EVENT_THREAD_EXIT_NODISPATCH_PI, arg1, arg2);\
}                                                           \
else                                                        \
{                                                           \
if (TV_EVTCLASS_IS_SET(TV_CLASS_1|TV_ON))			        \
(* _func_evt_log_t_sched) (EVENT_THREAD_EXIT_NODISPATCH, arg1, arg2);\
}                                                           \
}

/*
 * EVT_TASK_CTX_TASKINFO - 任务上下文切换事件日志，记录任务信息
 */

#define EVT_TASK_CTX_TASKINFO(evtId, state, pri, lockCnt, tid, name)  	\
if (UNLIKELY(ACTION_IS_SET))						     				\
{                                                       	     		\
if ( TV_EVTCLASS_IS_SET(TV_CLASS_1|TV_ON) )                          	\
{								     									\
( * _func_evt_log_string) (evtId, state, pri, lockCnt, tid, name); 		\
if (TV_EVTCLASS_IS_SET(TV_CLASS_TASKNAMES_PRESERVE))	     			\
( * _func_evt_log_reserve_taskname) (evtId, state, pri, lockCnt, tid, name);	\
}								     									\
}

/******************************************************************************
 *
 * EVT_TASK_CTX_BUF - 任务上下文切换事件日志（带有事件buffer）
 *
 * This macro stores eventpoint and user events into the event buffer.
 *
 */

#define EVT_TASK_CTX_BUF(evtId, addr, bufSize, BufferAddr)		\
if (UNLIKELY(ACTION_IS_SET))									\
{                                                               \
if ( TV_EVTCLASS_IS_SET(TV_CLASS_1|TV_ON) )                     \
( * _func_evt_log_point) (evtId, addr, bufSize, BufferAddr); 	\
}

/*
 * EVT_TASK_STATE_0 - 任务状态转换事件日志
 */

#define EVT_TASK_STATE_0(evtId)                 \
if (UNLIKELY(ACTION_IS_SET))					\
{                                               \
if ( TV_EVTCLASS_IS_SET(TV_CLASS_2|TV_ON) )     \
(* _func_evt_log_m0) (evtId);                   \
}

/*
 * EVT_TASK_STATE_1 - 任务状态转换事件日志（带1个参数）
 */

#define EVT_TASK_STATE_1(evtId, arg)        	\
do 												\
{ 												\
if (UNLIKELY(ACTION_IS_SET))					\
{                                               \
if ( TV_EVTCLASS_IS_SET(TV_CLASS_2|TV_ON) )     \
(* _func_evt_log_m1) (evtId, arg);              \
}  												\
} while (0)


/*
 * EVT_TASK_STATE_2 - 任务状态转换事件日志（带2个参数）
 */

#define EVT_TASK_STATE_2(evtId, arg1, arg2)     	\
if (UNLIKELY(ACTION_IS_SET))						\
{                                                   \
if ( TV_EVTCLASS_IS_SET(TV_CLASS_2|TV_ON) )         \
(* _func_evt_log_m2) (evtId, arg1, arg2);           \
}

/*
 * EVT_TASK_STATE_3 - 任务状态转换事件日志（带3个参数）
 */

#define EVT_TASK_STATE_3(evtId, arg1, arg2, arg3)        	\
if (UNLIKELY(ACTION_IS_SET))						\
{                                                   \
if ( TV_EVTCLASS_IS_SET(TV_CLASS_2|TV_ON) )         \
(* _func_evt_log_m3) (evtId, arg1, arg2, arg3);     \
}

/*
 * EVT_OBJ_TASKSPAWN - 对象状态事件日志，创建任务
 */

#define EVT_OBJ_TASKSPAWN(evtId, arg1, arg2, arg3, arg4, arg5)  \
if (UNLIKELY(ACTION_IS_SET))									\
{                                                               \
if ( TV_EVTCLASS_IS_SET(TV_CLASS_3|TV_ON) )                     \
( * _func_evt_log_O_intlock) (evtId, 5, arg1, arg2, arg3, arg4, arg5); \
}

//#endif	/* CPU */			/* end target-only section */
#endif /* _ASMLANGUAGE */

#define TV_REV_ID_T2 0xb0b00000
#define TV_REV_ID_T3 0xcdcd0000

#define TV_EVT_PROTO_REV_1_0_FCS 1

#define TV_EVT_PROTO_REV_2_0_FCS 2
#define TV_EVT_PROTO_REV_3_0_FCS 3
#define TV_EVT_PROTO_REV_3_1_FCS 4
#define TV_EVT_PROTO_REV_2_2_FCS 5

#define TV_REV_ID_CURRENT TV_REV_ID_T2
#define TV_EVT_PROTO_REV_CURRENT TV_EVT_PROTO_REV_2_2_FCS

/* ranges for the different classes of events */

#define CONTROL_ID_MIN 	0
#define CONTROL_ID_MAX	49
#define CLASS1_ID_MIN	50
#define CLASS1_ID_MAX	599
#define INT_ID_MIN	102
#define PPC_DECR_INT_ID	599
#define INT_ID_MAX	599
#define CLASS2_ID_MIN	600
#define CLASS2_ID_MAX	9999
#define CLASS3_ID_MIN	10000
#define CLASS3_ID_MAX	19999 
#define RESERVE_ID_MIN	20000
#define RTI_RESERVE_ID_MIN      39900
#define RTI_RESERVE_ID_MAX      39999
#define RESERVE_ID_MAX	39999 
#define USER_ID_MIN	40000
#define USER_ID_MAX	65535

#define INT_LEVEL(event_num) ((event_num)-INT_ID_MIN)

#define CONTROL_EVENT(id) (CONTROL_ID_MIN + id)
#define CLASS1_EVENT(id)  (CLASS1_ID_MIN + id)
#define CLASS2_EVENT(id)  (CLASS2_ID_MIN + id)
#define CLASS3_EVENT(id)  (CLASS3_ID_MIN + id)
#define INT_EVENT(id)     (INT_ID_MIN + id)


/* EVENT Id's */

/* Interrupt events */

#define EVENT_INT_ENT(k) 		((k) + INT_ID_MIN)

#define EVENT_INT_ENTER         EVENT_INT_ENT(0)
/* Param:
 *   short EVENT_INT_ENT(k), 
 *   int timeStamp
 */

#define EVENT_INT_EXIT 	 		(INT_ID_MIN - 1)
/* Param:
 *   short EVENT_INT_EXIT, 
 *   int timeStamp
 */

#define EVENT_INT_EXIT_K        (INT_ID_MIN - 2)
/* Param:
 *   short EVENT_INT_EXIT_K,
 *   int timeStamp
 */

/* Control events */

#define EVENT_BEGIN                     CONTROL_EVENT(0)
/* Param:
 *   short EVENT_BEGIN, 
 *   int CPU, 
 *   int bspSize, 
 *   char * bspName, 
 *   int taskIdCurrent, 
 *   int collectionMode, 
 *   int revision
 */


#define EVENT_END                       CONTROL_EVENT(1)
/* Param:
 *   short EVENT_END
 */


#define EVENT_TIMER_ROLLOVER            CONTROL_EVENT(2)
/* Param:
 *   short EVENT_TIMER_ROLLOVER
 *   int timeStamp, 
 */

#define EVENT_TASKNAME			CONTROL_EVENT(3)
/* Param:
 *   short EVENT_TASKNAME, 
 *   int status, 
 *   int priority, 
 *   int taskLockCount
 *   int tid, 
 *   int nameSize, 
 *   char * name
 */

#define EVENT_CONFIG              	CONTROL_EVENT(4)
/* Param:
 *   int revision,                  TV_REV_ID | TV_EVT_PROTO_REV
 *   int timestampFreq, 
 *   int timestampPeriod, 
 *   int autoRollover, 
 *   int clkRate,
 *   int collectionMode,
 *   int processorNum
 */


#define EVENT_BUFFER                    CONTROL_EVENT(5)
/* Param:
 *   short EVENT_BUFFER,
 *   int taskIdCurrent
 */


#define EVENT_TIMER_SYNC		CONTROL_EVENT(6)
/* Param:
 *   int timeStamp,
 *   int spare
 */

#define EVENT_LOGCOMMENT              CONTROL_EVENT(7)
/* Param:
 *   int length
 *   string comment
 */

#define EVENT_ANY_EVENT			CONTROL_EVENT(48)
/* This is added for triggering, when no particular event is required */

/* CLASS2 events */

#define EVENT_THREADCREATE				CLASS2_EVENT(0)
/* Param:
 *   short EVENT_THREADCREATE, 
 *   int pTcb, 
 *   int priority
 */

#define EVENT_THREADDELETE			CLASS2_EVENT(1)
/* Param:
 *   short EVENT_THREADDELETE, 
 *   int pTcb
 */

#define EVENT_THREADSUSPEND			CLASS2_EVENT(2)
/* Param:
 *   short EVENT_THREADSUSPEND, 
 *   int pTcb
 */

#define EVENT_THREADRESUME			CLASS2_EVENT(3)
/* Param:
 *   short EVENT_THREADRESUME, 
 *   int pTcb
 */

#define EVENT_THREADPRIORITYSETRAISE		CLASS2_EVENT(4)
/* Param:
 *   short EVENT_THREADPRIORITYSETRAISE, 
 *   int pTcb, 
 *   int oldPriority, 
 *   int priority
 */


#define EVENT_THREADPRIORITYSETLOWER		CLASS2_EVENT(5)
/* Param:
 *   short EVENT_THREADPRIORITYSETLOWER, 
 *   int pTcb, 
 *   int oldPriority, 
 *   int priority
 */

#define EVENT_UCORESEMDELETE			CLASS2_EVENT(6)
/* Param:
 *   short EVENT_UCORESEMDELETE, 
 *   int semId
 */

#define EVENT_UCORECLOCKTICKTMSLC		CLASS2_EVENT(7)
/* Param:
 *   short EVENT_UCORECLOCKTICKTMSLC
 */

#define EVENT_UCORECLOCKTICKTMRWD		CLASS2_EVENT(8)
/* Param:
 *   short EVENT_UCORECLOCKTICKTMRWD, 
 *   int wdId
 */

#define EVENT_THREADDELAY				CLASS2_EVENT(9)
/* Param:
 *   short EVENT_THREADDELAY
 *   int ticks
 */

#define EVENT_THREADUNDELAY			CLASS2_EVENT(10)
/* Param:
 *   short EVENT_THREADUNDELAY, 
 *   int pTcb
 */

#define EVENT_UCOREWDSTART			CLASS2_EVENT(11)
/* Param:
 *   short EVENT_UCOREWDSTART, 
 *   int wdId
 */

#define EVENT_UCOREWDCANCEL			CLASS2_EVENT(12)
/* Param:
 *   short EVENT_UCOREWDCANCEL, 
 *   int wdId
 */

#define EVENT_UCOREWAITQDEQ			CLASS2_EVENT(13)
/* Param:
 *   short EVENT_UCOREWAITQDEQ, 
 *   int pTcb
 */

#define EVENT_UCOREWAITQFLUSH			CLASS2_EVENT(14)
/* Param:
 *   short EVENT_UCOREWAITQFLUSH, 
 *   int pTcb
 */

#define EVENT_UCOREWAITQENQ			CLASS2_EVENT(15)
/* Param:
 *   short EVENT_UCOREWAITQENQ
 */

#define EVENT_UCOREWAITQTERMINATE		CLASS2_EVENT(17)
/* Param:
 *   short EVENT_UCOREWAITQTERMINATE, 
 *   int pTcb
 */

#define EVENT_UCORECLOCKTICKUNDELAY			CLASS2_EVENT(18)
/* Param:
 *   short EVENT_UCORECLOCKTICKUNDELAY, 
 *   int pTcb
 */

#define EVENT_OBJ_THREAD				CLASS2_EVENT(19)
/* Param:
 *   short EVENT_OBJ_THREAD, 
 *   int pTcb
 */

#define EVENT_OBJ_SEMGIVE			CLASS2_EVENT(20)
/* Param:
 *   short EVENT_OBJ_SEMGIVE, 
 *   int semId
 */

#define EVENT_OBJ_SEMTAKE			CLASS2_EVENT(21)
/* Param:
 *   short EVENT_OBJ_SEMTAKE, 
 *   int semId
 */

#define EVENT_OBJ_SEMFLUSH			CLASS2_EVENT(22)
/* Param:
 *   short EVENT_OBJ_SEMFLUSH, 
 *   int semId
 */

#define EVENT_OBJ_MSGSEND			CLASS2_EVENT(23)
/* Param:
 *   short EVENT_OBJ_MSGSEND, 
 *   int msgQId
 */

#define EVENT_OBJ_MSGRECEIVE			CLASS2_EVENT(24)
/* Param:
 *   short EVENT_OBJ_MSGRECEIVE, 
 *   int msgQId
 */

#define EVENT_OBJ_MSGDELETE			CLASS2_EVENT(25)
/* Param:
 *   short EVENT_OBJ_MSGDELETE, 
 *   int msgQId
 */

#define EVENT_OBJ_SIGPAUSE			CLASS2_EVENT(26)
/* Param:
 *   short EVENT_OBJ_SIGPAUSE, 
 *   int qHead
 */

#define EVENT_OBJ_SIGSUSPEND			CLASS2_EVENT(27)
/* Param:
 *   short EVENT_OBJ_SIGSUSPEND, 
 *   int sigset
 */

#define EVENT_OBJ_SIGKILL			CLASS2_EVENT(28)
/* Param:
 *   short EVENT_OBJ_SIGKILL, 
 *   int tid
 */

#define EVENT_UCORECLOCKTICKUNBLOCK                   CLASS2_EVENT(31)
/* Param:
 *   short EVENT_UCORECLOCKTICKUNBLOCK,
 *   int tId
 */

#define EVENT_OBJ_SIGWAIT                       CLASS2_EVENT(32)
/* Param:
 *   short EVENT_OBJ_SIGWAIT, 
 *   int tid
 */

#define EVENT_OBJ_EVENTSEND			CLASS2_EVENT(35)
/* Param:
 *   short EVENT_OBJ_EVENTSEND 
 */

#define EVENT_OBJ_EVENTRECEIVE			CLASS2_EVENT(36)
/* Param:
 *   short EVENT_OBJ_EVENTRECEIVE
 */

#define EVENT_OBJ_TIMESLICE			CLASS2_EVENT(50)
#define EVENT_OBJ_WDG_PROCESS			CLASS2_EVENT(51)
#define EVENT_OBJ_GDB_SUSPEND			CLASS2_EVENT(52)
#define EVENT_OBJ_GDB_RESUME			CLASS2_EVENT(53)
/* Param:
 *   short EVENT_OBJ_EVENTRECEIVE
 */

/* CLASS1 events */

#define EVENT_THREAD_EXIT_DISPATCH        	CLASS1_EVENT(2)
/* Param:
 *   short EVENT_THREAD_EXIT_DISPATCH, 
 *   int timestamp,
 *   int tId,
 *   int priority
 */

#define EVENT_THREAD_EXIT_NODISPATCH        	CLASS1_EVENT(3)
/* Param:
 *   short EVENT_THREAD_EXIT_NODISPATCH, 
 *   int timestamp,
 *   int priority
 */

#define EVENT_THREAD_EXIT_DISPATCH_PI        	CLASS1_EVENT(4)
/* Param:
 *   short EVENT_THREAD_EXIT_DISPATCH_PI, 
 *   int timestamp,
 *   int tId,
 *   int priority
 */

#define EVENT_THREAD_EXIT_NODISPATCH_PI        	CLASS1_EVENT(5)
/* Param:
 *   short EVENT_THREAD_EXIT_NODISPATCH_PI, 
 *   int timestamp,
 *   int priority
 */

#define EVENT_DISPATCH_OFFSET   0xe
#define EVENT_NODISPATCH_OFFSET   0xa
/*
 * This definition is needed for the logging of the above _PI events
 */

#define EVENT_THREAD_EXIT_IDLE			CLASS1_EVENT(6)
/* Param:
 *   short EVENT_THREAD_EXIT_IDLE, 
 *   int timestamp
 */

#define EVENT_THREADLOCK				CLASS1_EVENT(7)
/* Param:
 *   short EVENT_THREADLOCK, 
 *   int timeStamp
 */

#define EVENT_THREADUNLOCK			CLASS1_EVENT(8)
/* Param:
 *   short EVENT_THREADUNLOCK, 
 *   int timeStamp
 */

#define EVENT_CLOCKTICK			CLASS1_EVENT(9)
/* Param:
 *   short EVENT_CLOCKTICK, 
 *   int timeStamp
 */

#define EVENT_EXCEPTION				CLASS1_EVENT(10)
/* Param:
 *   short EVENT_EXCEPTION, 
 *   int timeStamp, 
 *   int exception
 */

#define EVENT_TASK_STATECHANGE			CLASS1_EVENT(11)
/* Param:
 *   short EVENT_STATECHANGE
 *   int timeStamp,
 *   int taskId,
 *   int newState
 */

/* pseudo events - generated host side */

#define MIN_PSEUDO_EVENT                        CLASS1_EVENT(20)

#define EVENT_STATECHANGE                       CLASS1_EVENT(20)
/* Param:
 *   short EVENT_STATECHANGE
 *   int newState
 */

#define EVENT_NIL                               CLASS1_EVENT(21)
/* Param:
 *   short EVENT_NIL
 */

#define EVENT_VALUE                             CLASS1_EVENT(22)
/* Param:
 *   short EVENT_VALUE
 *   int value
 */

#define MAX_PSEUDO_EVENT                        CLASS1_EVENT(24)

#define IS_PSEUDO_EVENT(event) \
((event >= MIN_PSEUDO_EVENT) && (event <= MAX_PSEUDO_EVENT))



#ifndef _BYTE_ORDER
#error "_BYTE_ORDER must be defined"
#endif /* _BYTE_ORDER */

/*
 * Alignment macros used to store unaligned short (16 bits) and unaligned
 * int (32 bits).
 */

#define EVT_STORE_UINT16(pBuf, event_id) \
*pBuf++ = (event_id)

#if (_BYTE_ORDER==_BIG_ENDIAN)

/* unaligned access not supported */
#define EVT_STORE_UINT32(pBuf, value) \
do { *((short *) pBuf) = (value) >> 16; \
*(((short *) pBuf) + 1) = (value); \
pBuf++; } while (0)

#define EVT_STORE_UINT64(pBuf, value) \
do { *((short *) pBuf) = (value) >> 48; \
*(((short *) pBuf) + 1) = (value) >> 32; \
*(((short *) pBuf) + 2) = (value) >> 16; \
*(((short *) pBuf) + 3) = (value); \
pBuf +=2; } while (0)

#else /* (_BYTE_ORDER==_BIG_ENDIAN) */

#define EVT_STORE_UINT32(pBuf, value) \
do { *((short *) pBuf) = (value); \
*(((short *) pBuf) + 1) = (value) >> 16; \
pBuf++; } while (0)

#define EVT_STORE_UINT64(pBuf, value) \
do { *((short *) pBuf) = (value); \
*(((short *) pBuf) + 1) = (value) >> 16; \
*(((short *) pBuf) + 2) = (value) >> 32; \
*(((short *) pBuf) + 3) = (value) >> 48; \
pBuf +=2; } while (0)

#endif /* (_BYTE_ORDER==_BIG_ENDIAN) */	     

#ifdef __cplusplus
}
#endif 

#endif 
