#ifndef _CACHE_H
#define _CACHE_H
#ifdef __cplusplus
extern "C"
{
#endif 

/* Cache程序指针 */
typedef	struct					
{
	int		(*cache_enable)();			/* 使能cache */
	int		(*cache_disable)();			/* 关闭cache */
	int		(*cache_flush)();			/* 刷新指定地址范围cache */
	int		(*cache_invalidate)();	/* 无效cache */
    
	int		(*text_update)();		/* 更新 */
	void*	(*dma_malloc)(size_t);	    /*分配DMA内存（非cache）*/	
	void	(*dma_free)(void *);			/*释放DMA内存（非cache）*/
	int		(*dma_virt_to_phys)();	/* 虚拟地址转物理地址 */
	int		(*dma_phys_to_virt)();	/* 物理地址转虚拟地址*/

} RE_CACHE_LIB;

typedef	unsigned int	RE_CACHE_MODE;		/* CACHE_MODE */

extern  RE_CACHE_LIB re_cache_lib;
/*access mode*/
#define WRITE_THROUGH	0x01	/* Write-through Mode */
#define WRITE_BACK		0x02	/* Write-back Mode */

/*cache类型*/
#define _INST_CACHE      0x0    /*instruction cache*/
#define _DATA_CACHE      0x1    /*data cache*/

#ifndef	_ASMLANGUAGE
#include <reworks/types.h>

#if defined(DATA_CACHE)
#undef DATA_CACHE
#endif

typedef enum				/* CACHE_TYPE */
{
	INST_CACHE = _INST_CACHE,
    DATA_CACHE = _DATA_CACHE,
} RE_CACHE_TYPE;


/*数据cache行大小*/
extern int d_cache_line_size ; 
/*指令ache行大小*/
extern int i_cache_line_size ; 

extern int (*cache_text_update_ptr)(void *start_address , size_t size);
/*-------------------------------------------------------------------------+
|
|   函数：cache_module_init - cache模块的初始化
|
|   描述：根据指令cache以及数据cache的访问模式初始化cache模块,目前系统支持的
|         cache访问模式是WRITE_THROUGH和WRITE_BACK。
|
| 输入值：inst_cache_mode - 指令cache访问模式
|         data_cache_mode - 数据cache访问模式
| 输出值：无
|
| 返回值：初始化成功是否
| 头文件: #include<cache.h>
+--------------------------------------------------------------------------*/
#ifdef REWORKS_LP64
	#ifdef __HUARUI3__
			int cache_module_init(u32 inst_cache_mode,  /*指令cache访问模式*/
				u32 data_cache_mode /*数据cache访问模式*/
			);
	#else
	int cache_module_init(void );
	#endif
#else
int cache_module_init(u32 inst_cache_mode,  /*指令cache访问模式*/
		u32 data_cache_mode /*数据cache访问模式*/
);
#endif


/*-------------------------------------------------------------------------+
|
|   函数：cache_flush - 将指定范围的cache中的内容写回内存。
|
|   描述：该例程将数据cache或指令cache内指定范围内的内容写回内存.根据不同的cache
|         设计，这个操作可能引起cache tags无效。如果cache 的访问模式是WRITE_THROUGH,
|         不需要调用该函数。
|
| 输入值：cache_type - cache类型
|         addr       - 内存逻辑地址
|         size       - 内存大小(单位:字节)
|
| 输出值：无
|
| 返回值：刷新成功是否
|头文件: #include<cache.h>
+--------------------------------------------------------------------------*/
int cache_flush(u32 cache_type,  /* cache类型 */
void *addr,  /* 内存逻辑地址 */
size_t size /* 内存大小(单位:字节)*/
);

/*-------------------------------------------------------------------------+
|
|   函数：cache_invalidate -- 使指定范围cache无效
|
|   描述：该例程使数据cache或指令cache内的指定空间为无效.根据不同的cache
|         设计，这个操作可能直接无效cache tags或有刷新的操作。
| 输入值:cache_type      - cache类型
|          addr            - 内存逻辑地址
|          size            - 内存大小(单位:字节)
|
| 输出值：无
|
| 返回值：无效cache成功是否
|
| 头文件: #include<cache.h>
+--------------------------------------------------------------------------*/
int cache_invalidate(u32 cache_type,  /* cache类型  */
void *addr,  /* 内存逻辑地址*/
size_t size /* 内存大小(单位:字节) */
);

/*-------------------------------------------------------------------------+
|
|   函数：cache_dma_malloc - 分配DMA（非cache内存）
|
|   描述：为设备DMA分配DMA内存
|
| 输入值：bytes    - 待分配的内存大小(单位:字节)
|
| 输出值：无
|
| 返回值：分配的内存地址
|
| 头文件: #include<cache.h>
*/
void * cache_dma_malloc(size_t bytes);

/*-------------------------------------------------------------------------+
|
|   函数：cache_dma_free - 释放DMA（非cache内存）
|
|   描述：释放为设备DMA分配的DMA内存
|
| 输入值：bytes    - 待分配的内存大小(单位:字节)
|
| 输出值：无
|
| 返回值：分配的内存地址
|
| 头文件: #include<cache.h>
*/
void cache_dma_free(void * pBuf );
extern int cache_pipe_flush();

#define	REWORKS_CACHE_FLUSH(adrs, bytes)		cache_flush(DATA_CACHE,adrs,bytes)

#define	REWORKS_CACHE_INVALIDATE(adrs, bytes)	cache_invalidate(DATA_CACHE,adrs,bytes)

#define	REWORKS_CACHE_DMA_MALLOC(bytes)				cache_dma_malloc(bytes)

#define	REWORKS_CACHE_DMA_FREE(adrs)				cache_dma_free(adrs)

#define	REWORKS_CACHE_PIPE_FLUSH()	cache_pipe_flush()

#define	REWORKS_CACHE_IS_WRITE_COHERENT()	\
	(re_cache_lib.cache_flush == NULL)

#define	REWORKS_CACHE_IS_READ_COHERENT()	\
	(re_cache_lib.cache_invalidate == NULL)
#endif

#ifdef __cplusplus
}

#endif 


#endif
