#ifndef _REWORKS_MPARTLIB_H_
#define _REWORKS_MPARTLIB_H_

#ifdef __cplusplus
extern "C" 
{
#endif
	
#include <reworks/types.h>
#include <reworks/list.h>
#include <coremutex.h>

#define OFFSET(structure, member)	/* byte offset of member in structure*/\
		((off_t) &(((structure *) 0) -> member))

/* partition options */

/* optional check for bad blocks */
#define MEM_BLOCK_CHECK			0x10

/* response to errors when allocating memory */
#define MEM_ALLOC_ERROR_LOG_FLAG	0x20
#define MEM_ALLOC_ERROR_SUSPEND_FLAG	0x40

/* response to errors when freeing memory */
#define MEM_BLOCK_ERROR_LOG_FLAG	0x80
#define MEM_BLOCK_ERROR_SUSPEND_FLAG	0x100

/* macros for getting to next and previous blocks */
#define NEXT_HDR(pHdr)  ((BLOCK_HDR *) ((char *) (pHdr) + (2 * (pHdr)->nWords)))
#define PREV_HDR(pHdr)	((pHdr)->pPrevHdr)

/* macros for converting between the "block" that caller knows
 * (actual available data area) and the block header in front of it */
#define HDR_TO_BLOCK(pHdr)	((char *) ((ptrdiff_t) pHdr + sizeof (BLOCK_HDR)))
#define BLOCK_TO_HDR(pBlock)	((BLOCK_HDR *) ((ptrdiff_t) pBlock - \
						sizeof(BLOCK_HDR)))

/* macros for converting between the "node" that is strung on the freelist
 * and the block header in front of it */
#define HDR_TO_NODE(pHdr)	(& ((FREE_BLOCK *) pHdr)->node)
#define NODE_TO_HDR(pNode)	((BLOCK_HDR *) ((ptrdiff_t) pNode - \
						OFFSET (FREE_BLOCK, node)))
	
/* types */

typedef struct heap_mem_part
{
//    OBJ_CORE	objCore;		/* object management */
	struct object_record obj;
	struct list_head	freeList;		/* list of free blocks */
	Mutex_Ctrl	sem;			/* partition semaphore */
	size_t	totalWords;		/* total number of words in pool */
	size_t	minBlockWords;		/* min blk size in words includes hdr */
	unsigned	options;		/* options */

	/* allocation statistics */

	size_t curBlocksAllocated;	/* current # of blocks allocated */
	size_t curWordsAllocated;		/* current # of words allocated */
	size_t cumBlocksAllocated;	/* cumulative # of blocks allocated */
	size_t cumWordsAllocated;		/* cumulative # of words allocated */

} MEM_PARTITION;

/* Partition statistics structure */
struct mpart_stats 
{

    unsigned long numBytesFree;	   /* Number of Free Bytes in Partition       */
    unsigned long numBlocksFree;   /* Number of Free Blocks in Partition      */
    unsigned long maxBlockSizeFree;/* Maximum block size that is free.	      */
    unsigned long numBytesAlloc;   /* Number of Allocated Bytes in Partition  */
    unsigned long numBlocksAlloc;  /* Number of Allocated Blocks in Partition */

} ;

/* 
 * Allocated block header. 
 * NOTE: The size of this data structure must be aligned on _ALLOC_ALIGN_SIZE.
 */
typedef struct blockHdr		/* BLOCK_HDR */
{
    struct blockHdr *	pPrevHdr;	/* pointer to previous block hdr */
#ifndef REWORKS_LP64
    size_t		nWords : 31;	/* size in words of this block */
  #else
    size_t		nWords : 63;	/* size in words of this block */
  #endif
    size_t		free   : 1;	/* TRUE = this block is free */
#ifndef __ARMV8__    
#if (CPU_ALIGNMENT == 16)
#ifndef REWORKS_LP64
   u32		pad[2];		/* 8 byte pad for round up */
  #else
   u32		pad[3];
  #endif
#elif (CPU_ALIGNMENT == 32)
  #ifndef REWORKS_LP64
   u32		pad[5];		/* 8 byte pad for round up */
  #else
   u32		pad[4];
  #endif
#endif	/* CPU_ALIGNMENT == 16 */
#endif
} BLOCK_HDR;

/* 
 * Free block header. 
 * NOTE: The size of this data structure is already aligned on 
 * _ALLOC_ALIGN_SIZE, for 4, 8, and 16 bytes alignments.
 */
typedef struct			/* FREE_BLOCK */
{
    struct
    {
		struct blockHdr *   pPrevHdr;	/* pointer to previous block hdr */
#ifndef REWORKS_LP64
    size_t		nWords : 31;	/* size in words of this block */
  #else
    size_t		nWords : 63;	/* size in words of this block */
  #endif
    size_t		free   : 1;	/* TRUE = this block is free */
	} hdr;
	
	struct list_head	node;		/* freelist links */
} FREE_BLOCK;

typedef struct
{
	PART_ID  mpart_ptr;
	void *	 block_addr;
	size_t block_size;
	struct list_head node;
} MPART_BLOCKS;

typedef struct list_head * (*FUNC_GET_FREE_BLOCKS)(PART_ID part_id, struct list_head *max_free_block_node, size_t free_block_size);

/* variable declarations */
extern PART_ID memSysPartId;/* system partition */
extern unsigned	int mmpart_options_default;	/* default partition options */
extern unsigned int mem_default_alignment;	/* default alignment */

extern object_manager mem_part_mgr;

/* function declarations */
extern int mmpart_create (PART_ID partId, char *pPool, size_t poolSize);
extern int mmpart_init (PART_ID partId, char *pPool, size_t poolSize);
extern int 	mmpart_addtopool (PART_ID partId, char *pPool, size_t poolSize);
extern void mem_addtopool (PART_ID partId, char *pPool, register size_t poolSize);
extern void *mmpart_alloc (PART_ID partId, size_t nBytes);
extern void *mmpart_aligned_alloc (PART_ID partId, size_t nBytes, size_t alignment);
extern int 	mmpart_free (PART_ID partId, char *pBlock);
extern ssize_t 	mem_findmax (void);
extern ssize_t 	mmpart_findmax (PART_ID partId);
extern void *mmpart_realloc (PART_ID partId, char *pBlock, size_t nBytes);
extern int	 mmpart_destroy (PART_ID partId);
extern int	mmpart_block_isvalid (PART_ID partId, BLOCK_HDR *pHdr,
				     int isFree);

#ifdef __cplusplus
}
#endif

#endif /* _REWORKS_MPARTLIB_H_ */
