#ifndef _REWORKS_MPARTHEAP_H_
#define _REWORKS_MPARTHEAP_H_

#ifdef __cplusplus
extern "C" 
{
#endif	
	
#include <mpart_lib.h>

/* function declarations */
extern void* memalign_pt (size_t align_size, size_t size); 
extern void* malloc_pt ( size_t nBytes );				// 2011
extern void  free_pt ( void *ptr );
extern void* valloc_pt (size_t size);
extern void* calloc_pt ( size_t elemNum, size_t elemSize );
extern void* realloc_pt (void *pBlock, size_t newSize );
extern ssize_t mem_findmax_pt(void);
extern int mpart_init(PART_ID part_id, char *addr, size_t size);

#ifdef __cplusplus
}
#endif

#endif /* _REWORKS_MPARTHEAP_H_ */
