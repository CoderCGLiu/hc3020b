#ifndef _HOOK_H_
#define _HOOK_H_

#ifdef __cplusplus
extern "C"
{
#endif 
#ifndef __multi_core__
#define     MAX_TASK_HOOK_RTNS  16
#endif
#include <reworks/thread.h>

OS_STATUS thread_create_hook_execute(thread_t thread_ptr);
void thread_begin_hook_execute(thread_t thread_ptr);
#ifdef __multi_core__
void thread_switch_hook_execute(thread_t executing, thread_t heir);
#endif
void thread_delete_hook_execute(thread_t thread_ptr);

typedef void(*THREAD_HOOK_FUNCPTR_ARG)();
typedef void(*THREAD_HOOK_FUNCPTR_ARG1)(thread_t);
typedef void(*THREAD_HOOK_FUNCPTR_ARG2)(thread_t, thread_t);
typedef int(*THREAD_CREATE_HOOK_FUNCPTR)(thread_t);

u32 thread_create_hook_add(THREAD_CREATE_HOOK_FUNCPTR create_hook);
u32 thread_create_hook_delete(THREAD_CREATE_HOOK_FUNCPTR create_hook);
u32 thread_begin_hook_add(THREAD_HOOK_FUNCPTR_ARG1 begin_hook);
u32 thread_begin_hook_delete(THREAD_HOOK_FUNCPTR_ARG1 begin_hook);
u32 thread_switch_hook_add(THREAD_HOOK_FUNCPTR_ARG2 switch_hook);
u32 thread_switch_hook_delete(THREAD_HOOK_FUNCPTR_ARG2 switch_hook);
u32 thread_delete_hook_add(THREAD_HOOK_FUNCPTR_ARG1 delete_hook);
u32 thread_delete_hook_delete(THREAD_HOOK_FUNCPTR_ARG1 delete_hook);

OS_EXTERN THREAD_CREATE_HOOK_FUNCPTR thread_create_hook;
OS_EXTERN THREAD_HOOK_FUNCPTR_ARG1 thread_begin_hook;
OS_EXTERN THREAD_HOOK_FUNCPTR_ARG2 thread_switch_hook;
OS_EXTERN THREAD_HOOK_FUNCPTR_ARG1 thread_delete_hook;

u32 thread_switch_hook_add_without_lock(THREAD_HOOK_FUNCPTR_ARG2 switch_hook);
u32 thread_switch_hook_delete_without_lock(THREAD_HOOK_FUNCPTR_ARG2 switch_hook);


#ifdef __cplusplus
}

#endif 

#endif
