#ifndef KERNEL_LOCK_H
#define KERNEL_LOCK_H
#ifdef __cplusplus
extern "C"
{
#endif 

#include <reworks/types.h>

extern int	    kernelLockTake(char *,int);
extern boolean	kernelLockTry (char *, int);
extern void	    kernelLockGive (void);
extern boolean	kernelLockOwnedByMe (void);
extern int 		kernelLockOwnerGet (void);
extern void	    kernelLockInit (void);

#ifdef __multi_core__
#define KERNEL_LOCK_TAKE()          (kernelLockTake(__FILE__, __LINE__))
#define KERNEL_LOCK_TRY()          (kernelLockTry(__FILE__, __LINE__))
#define KERNEL_LOCK_GIVE()          (kernelLockGive())
#define KERNEL_LOCK_OWNED_BY_ME()   (kernelLockOwnedByMe())
#define KERNEL_LOCK_OWNER_GET()	    (kernelLockOwnerGet())

#define ENTER_KERNEL KERNEL_LOCK_TAKE()

#define kernel_state KERNEL_LOCK_OWNED_BY_ME()
#else
extern int kernel_state;

#define ENTER_KERNEL \
	do {kernel_state = 1;} while (0)
#endif

#ifdef __cplusplus
}
#endif 
#endif
