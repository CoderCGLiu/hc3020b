#ifndef MMU_H
#define MMU_H
#ifdef __cplusplus
extern "C"
{
#endif 

#include <stdio.h>
#include <reworks/types.h>
#include <reworks/list.h>

/*--------------------虚拟内存管理属性说明---------------+
|
| 虚拟内存基本属性包括:有效属性,写保护属性，cache属性(是否允许使用快速缓存)。
| 以下是对属性值的说明:
| 1)MMU_ATTR_VALID, MMU_ATTR_VALID_NOT
|   如果尝试访问表示为无效属性的内存空间，系统会产生异常。
|   设置内存空间为无效的目的是防止"错误引用"产生的内存破坏。
|
| 2)MMU_ATTR_WRITABLE, MMU_ATTR_WRITABLE_NOT
|   如果内存空间定义为写保护属性(MMU_ATTR_WRITABLE_NOT),尝试写该内存空间会引发异常。
|
| 3)MMU_ATTR_CACHEABLE, MMU_ATTR_CACHEABLE_NOT
|    定义对该内存空间的访问是否经过cache。
|
|以下提供的掩码说明:
| 1)VM_STATE_MASK_VALID
| 2)VM_STATE_MASK_CACHEABLE
| 3)VM_STATE_MASK_WRITABLE
| 指定需要设置的属性种类。
|
| 备注:此外，不同的体系可能有额外的属性定义，目前支持以上三种基本属性。
+-------------------------------------------------------*/

#define MMU_ATTR_VALID       0x01
#define MMU_ATTR_VALID_NOT   0x00

/*写保护，supervisor和user 是一样的*/
#define MMU_ATTR_WRITABLE       0x04
#define MMU_ATTR_WRITABLE_NOT   0x00
#define MMU_ATTR_CACHEABLE      0x10
#define MMU_ATTR_CACHEABLE_NOT  0x00


#define MMU_ATTR_MASK_VALID             0x03
#define MMU_ATTR_MASK_CACHEABLE         0x30
#define MMU_ATTR_MASK_WRITABLE          0x0c


/*MMU类型说明*/
#define MMU_TYPE_INST 0x1
#define MMU_TYPE_DATA 0x2


typedef enum
{
	MMU_PAGE_MODE = 0,
	MMU_SEGMENT_MODE = 1
}MMU_MAP_MODE;

typedef struct
{
    struct list_head the_head;
    MMU_MAP_MODE map_mode;
    void *virtual_addr;
    void *physical_addr;
    u32 len;
    u32 is_cacheable;
    char description[32];
} MMU_MAP_ITEM;
typedef struct
{
    struct list_head the_head;
    u32 effAddr;
    u32 realAddrExt;
    u32 realAddr;
    u32 attr;
    char description[32];


} TLB_ENTRY_DESC_ITEM;
/*函数声明*/
/*-------------------------------------------------------------------------+
|
|   函数：mmu_module_init - 虚拟内存管理模块的初始化
|
|   描述：初始化虚拟内存管理抽象层与底层结构相关层的相互转换表，只在系统初始化时
| 		 调用一次
|
| 输入值：   type  -  内存管理支持的类型，MMU_DATA_SUPPORT/MMU_INST_SUPPORT --- 数据空间/指令空间
|	     mem_size  - 内存管理的空间大小
|
| 输出值：无
|
| 返回值：初始化成功是否
|
+--------------------------------------------------------------------------*/

extern int mmu_is_enable_flag;

extern int(*ptr_mmu_attr_per_page_get)(void *, u32);

extern int mmu_module_init(int type);

struct list_head *mmu_config_init();
struct list_head *static_tbl_config_init();
void mmu_config_item_add(char *description, MMU_MAP_MODE map_mode, u32 virtual_addr, u32 physical_addr, u32 len, u32 is_cacheable);
void static_tbl_config_item_add(char *description, u32 eff_addr, u32 real_addr_ext, u32 real_addr, u32 attr);
/*-------------------------------------------------------------------------+
|
|   函数：mmu_attr_set - 设置虚拟内存对应页表的属性
|
|   描述：该例程修改指定虚拟内存空间的属性。
| 输入值：viraddr  - 虚拟内存地址
|         attr_mask - 属性掩码
|         attr      - 属性值
|         size      - 内存长度(单位:字节)
|
|
| 输出值：无
|
| 返回值：成功是否
|
+--------------------------------------------------------------------------*/
extern int mmu_attr_set(void *viraddr,  /* 虚拟内存地址 */
u32 attr_mask,  /* 属性掩码     */
u32 attr,  /* 属性值       */
size_t size /* 内存长度     */
);


/*-------------------------------------------------------------------------+
|
|   函数：mmu_attr_per_page_get - 获取虚拟内存对应页表的属性
|
|   描述：获取虚拟内存对应页表的属性。
|
| 输入值：page_addr - 虚拟内存地址,应该以页对齐。
|
| 输出值：pattr      - 对应属性值
|
| 返回值：成功是否
|
+--------------------------------------------------------------------------*/
extern int mmu_attr_per_page_get(void *page_addr,  /* 虚拟内存地址 */
u32 *pattr /* 输出页属性 */
);


/*-------------------------------------------------------------------------+
|
|   函数：mmu_map - 将内存物理地址向虚拟地址映射
|
|   描述：将内存物理地址向虚拟地址映射，映射的空间大小size应该是页大小的整数倍。
|
| 输入值：viraddr -  虚拟地址,应该以页对齐
|         phyaddr -  物理地址,应该以页对齐
|         size    - 内存长度 (单位:字节)
|
| 输出值：无
|
| 返回值：成功是否
|
+--------------------------------------------------------------------------*/
extern int mmu_map(void *viraddr,  /* 虚拟内存地址 */
void *phyaddr,  /* 物理地址 */
size_t size /* 内存长度 */
);


/*-------------------------------------------------------------------------+
|
|   函数：mmu_text_protect - 将代码段内存空间设置为写保护
|
|   描述：将代码段内存空间设置为写保护
|
| 输入值：无
|
| 输出值：无
|
| 返回值：设置成功是否
|
+--------------------------------------------------------------------------*/
extern int mmu_text_protect();

/*-------------------------------------------------------------------------+
|
|   函数：mmu_text_unprotect - 取消代码段内存空间写保护
|
|   描述：取消代码段内存空间写保护
|
| 输入值：无
|
| 输出值：无
|
| 返回值：取消成功是否
|
+--------------------------------------------------------------------------*/
extern int mmu_text_unprotect();
/*-------------------------------------------------------------------------+
|
|   函数：mmu_enable - 启动内存管理
|
|   描述：启动内存管理,
|
| 输入值：无
|
| 输出值：无
|
| 返回值：设置成功是否
|
+--------------------------------------------------------------------------*/
extern int mmu_enable();


/*-------------------------------------------------------------------------+
|
|   函数：mmu_disable - 停止内存管理
|
|   描述：停止内存管理
|
| 输入值：无
|
| 输出值：无
|
| 返回值：设置成功是否
|
+--------------------------------------------------------------------------*/
extern int mmu_disable();
/*-------------------------------------------------------------------------+
|
|   函数：mmu_disable - 是否已经启动内存管理
|
|   描述：是否已经启动内存管理
|
| 输入值：无
|
| 输出值：无
|
+--------------------------------------------------------------------------*/

extern int mmu_is_enable();

/*-------------------------------------------------------------------------+
|
|   函数：mmu_page_size_get - 内存管理的页大小
|
|   描述：内存管理的页大小
|
| 输入值：无
|
| 输出值：无
|
| 返回值：内存管理的页大小
|
+--------------------------------------------------------------------------*/
extern int mmu_page_size_get();


/*-------------------------------------------------------------------------+
|
|   函数：mmu_info_show - 内存管理信息显示
|
|   描述：显示当前纳入内存管理的虚拟地址，物理地址和设置的属性。
|
| 输入值：无
|
| 输出值：无
|
| 返回值：成功是否
|
+--------------------------------------------------------------------------*/
extern int mmu_info_show();
#ifdef __cplusplus
}

#endif 

#endif
