﻿/*! @file thread.h
    @brief ReWorks任务管理（内部）
    
 * 本文件定义了内部任务管理操作接口。
 *
 * @version @reworks_version
 * 
 * @see 无
 */

#ifndef __THREAD_H__
#define __THREAD_H__

#ifdef __cplusplus
extern "C"
{
#endif 
	/** 
	* 
	* 
	*/
/** 
 * @defgroup group_os_reworks_coretask ReWorks任务管理（内部）
 * @ingroup group_os_reworks_task
 * @{ */
	
#include <reworks/types.h>
#include <stddef.h> 
#include <cpu.h> 

#define Thread_Modl_Error_def \
	__err__( S_ThreadModl_OS_OBJ_NAME_INVALID,          "S_ThreadModl_OS_OBJ_NAME_INVALID (os thread name invalid)") \
	__err__( S_ThreadModl_OS_SIZE_INVALID,              "S_ThreadModl_OS_SIZE_INVALID (os thread invalid size)") \
	__err__( S_ThreadModl_OS_PRI_INVALID,               "S_ThreadModl_OS_PRI_INVALID (os thread invalid priority)") \
	__err__( S_ThreadModl_OS_MP_NOT_CONFIGURED,         "S_ThreadModl_OS_MP_NOT_CONFIGURED (multiprocessing not configured)") \
	__err__( S_ThreadModl_OS_OBJ_NUM_EXCESS,            "S_ThreadModl_OS_OBJ_NUM_EXCESS (excess object task)") \
	__err__( S_ThreadModl_OS_UNSATISFIED,               "S_ThreadModl_OS_UNSATISFIED (os unsatisfied)") \
	__err__( S_ThreadModl_OS_REMOTE_OBJ_ILLEGAL,        "S_ThreadModl_OS_REMOTE_OBJ_ILLEGAL (thread remote object is illegal)") \
	__err__( S_ThreadModl_OS_OBJ_ID_INVALID,            "S_ThreadModl_OS_OBJ_ID_INVALID (os thread object ID invalid)") \
	__err__( S_ThreadModl_OS_ADDR_INVALID,              "S_ThreadModl_OS_ADDR_INVALID (invalid os addr)") \
	__err__( S_ThreadModl_OS_STATE_WRONG,               "S_ThreadModl_OS_STATE_WRONG (os state is wrong)") \
	__err__( S_ThreadModl_OS_THREAD_ALREADY_SUSPENDED,  "S_ThreadModl_OS_THREAD_ALREADY_SUSPENDED (thread is already suspended)") \
	__err__( S_ThreadModl_OS_COUNT_INVALID,             "S_ThreadModl_OS_COUNT_INVALID (os count invalid)") \
	__err__( S_ThreadModl_OS_NO_MEMORY,                 "S_ThreadModl_OS_NO_MEMORY (os have no memory)") \
	__err__( S_ThreadModl_OS_UNDEFINED,                 "S_ThreadModl_OS_UNDEFINED (os undefined)") \
	__err__( S_ThreadModl_OS_CLOCK_INVALID,             "S_ThreadModl_OS_CLOCK_INVALID (os thread invalid clock)") \
	__err__( S_ThreadModl_OS_THREAD_DORMANTING,         "S_ThreadModl_OS_THREAD_DORMANTING (os thread state is dormant)") \
	__err__( S_ThreadModl_OS_THREAD_SAFED,              "S_ThreadModl_OS_THREAD_SAFED (os thread is safed)") //!< 任务错误信息定义

/*!
 * @enum ThreadErr
 * 
 * @brief 任务错误信息输出
 */
enum ThreadErr{
	M_ThreadModl = 13<<16,    
#define __err__(a,b)	a,
	Thread_Modl_Error_def
#undef __err__
	THREAD_MAX_ERROR_NUM
};  

/*! 
 * @def POSIX_PRI_MODE
 * 
 * @brief posix任务优先级风格定义
 * 
 * @arg true 使用posix的任务优先级风格(0<255); 
 * @arg false 使用系统默认的任务优先级风格(0>255) 
 * （以用户的视角，系统内部的优先级风格是 0>255）
 */
#define POSIX_PRI_MODE (true) 

#define PRI_MIN      0         //!< 最高的系统任务优先级 
#define PRI_MAX      255       //!< 最低的系统任务优先级 

/* 任务状态 */
#define STATES_READY            0x00001     //!< 就绪 
#define STATES_RUNNING          0x00002     //!< 运行 
#define STATES_WAIT             0x00003     //!< 等待 
#ifdef __multi_core__
#define STATES_TRANSITION       0x00004     //!< 状态正在改变 
#endif

/* 任务等待状态的子状态——引起等待的原因 */
#define STATES_SUSPENDED                0x00008     //!< 挂起 
#define STATES_DEBUGGER_SUSPENDED       0x00010     //!< 调试挂起 
#define STATES_INTERNAL_SUSPENDED       0x00020     //!< 操作系统内部挂起 
#define STATES_BLOCKED                  0x00040     //!< 阻塞 
#define STATES_WAIT_FOR_EVENT           0x00080     //!< 等待事件 
#define STATES_SLEEP                    0x00100     //!< 睡眠 
#define STATES_INTERRUPTIBLE            0x00200     //!< 可被信号中断的 
#define STATES_DORMANT                	0x00400		//!< 初始化/结束 
#define STATES_STOP						0x00800		//!< 暂停
#define STATES_EXEC_SUSPENDED           0x08000     //!< 异常挂起

/* 任务选项 */
#define RE_UNBREAKABLE		0x0002  //!< 忽略断点。系统任务、中断任务将缺省设置。 
#define RE_DEALLOC_STACK  	0x0004	//!< 删除任务时释放任务栈（默认）
#define RE_FP_TASK	   		0x0008	//!< 浮点支持 
#define RE_KERNEL_TASK		0x0010  //!< 内核任务, 堆栈分配在内核空间 
#define RE_NO_STACK_FILL	0x0100	//!< 分配栈是否用0xee填充
#define RE_USER_ALLOC_TCB	0x0800  //!< 用户来分配任务控制块存储空间（废弃）
#define RE_NO_TIMESLICE     0x1000	//!< 是否需要时间片轮转调度 
#if defined(__HUARUI2__) || defined(__HUARUI3__)
#define RE_VEC_TASK	   		0x2000	//!< 向量支持，包含浮点支持
#define RE_FULL_OPTIONS     0x3fff  //!< 现有ReWorks任务选项和VxWorks任务选项的总合，如有增减请变更。
#else
#define RE_FULL_OPTIONS     0x1fff  //!< 现有ReWorks任务选项和VxWorks任务选项的总合，如有增减请变更。 
#endif
#define NO_DEALLOC_STACK (0)      //!< 删除任务时不释放的任务栈
#define DEALLOC_STACK (1)        //!< 删除任务时释放的任务栈

#define HELD_FAST_MUTEX_PRI_CHANGE  (0) //!< 表示因为拥有快速互斥量而提升了优先级
#define THREAD_LOCK_PRI_CHANGE      (1) //!< 表示因为锁抢占而提升了优先级
#define MUTEX_PRI_CHANGE            (2) //!< 表示因为拥有互斥量而提升了优先级

//#define THREAD_LOCKED_PRI (33)
	
typedef u8 task_type_t;        //!< 任务类型标示符类型 
#define MAX_TYPE_NUM 4         //!< 最大任务类型注册数，估计够用了 
#define RE_TASK (task_type_t)0 //!< 任务通用类型标识符 

typedef void *thread_t;  //!< 任务ID的变量类型
typedef void *usr_thread_t;  //!< 用户任务ID的变量类型

#define THREAD_T_GET        0 //!< 用于thread_get2的第三个参数，表示获取任务控制块的总起始地址
#define USR_THREAD_T_GET    1 //!< 用于thread_get2的第三个参数，表示获取任务控制块的用户类型部分起始地址

#ifdef __multi_core__
#define THREAD_CURRENT_GET (thread_current_get())
#else
#define THREAD_CURRENT_GET (thread_current)
#endif	

#ifdef __multi_core__
#include <cpuset.h>
#endif


/*! 
 * @struct Thread_Info
 * 
 * @brief 任务信息数据结构
 * 
 */
typedef struct  
{
    thread_t td_id;       //!< 任务ID
    task_type_t td_type; //!< 任务类型
    char *td_name;       //!< 任务名称
    int td_priority;     //!< 任务优先级
    u32 td_status;       //!< 任务状态
    u32 td_waitstatus;   //!< 任务等待状态
    u32 td_real_pri ;    //!< 任务真实优先级
    int td_options;      //!< 任务选项
    void(*td_entry)();   //!< 任务函数入口地址
    ptrdiff_t td_sp;           //!< 当前栈指针
#if defined(__loongson2__) || defined(__loongson2_64__)
    u64 td_pc;
#else
    unsigned long td_pc;           //!< pc值
#endif
    void *td_pStackBase; //!< 栈基指针
    size_t td_stackSize;    //!< 栈大小
    size_t td_stackHigh;    //!< 曾经最大栈使用量（字节） 
    size_t td_stackMargin;  //!< 当前栈空余（字节） 
    size_t td_stackCurrent; //!< 当前栈使用量（字节） 
    int td_errorStatus;  //!< 任务错误状态
    int td_timeout;      //!< 任务延迟tick数
#ifdef __multi_core__
    cpuset_t td_affinity;    //!< 任务绑定的CPU集 
    int      td_running_cpu; //!< 任务运行的CPU ID
#endif
} Thread_Info; //!< 任务信息数据结构

#if defined( MONITOR )

/*! \struct stack_info
 * \brief 任务栈信息数据结构
 * 
 */
struct stack_info
{
    size_t size; //!< 栈大小（字节） 
    size_t high; //!< 曾经最大栈使用量（字节） 
    size_t margin; //!< 当前栈空余（字节） 
    size_t current; //!< 当前栈使用量（字节） 
};

#endif

/*! 
 * @struct pthread_create_args
 * 
 * @brief 任务入口函数参数结构体
 * 
 */
typedef struct
{
    void *entry; //!< 任务入口函数地址
    void *arg1; //!< 任务入口函数参数1
    u32 arg2; //!< 任务入口函数参数2
    u32 arg3; //!< 任务入口函数参数3
    u32 arg4; //!< 任务入口函数参数4
    u32 arg5; //!< 任务入口函数参数5
    u32 arg6; //!< 任务入口函数参数6
    
} pthread_create_args; //!< 任务入口函数参数结构体

#define TASKTYPE_STR_MAXLENGTH 28 //!< 任务类型名最大长度（为了让结构体大小保持在32而选的这个数 ）
typedef char threadtype_name_t[TASKTYPE_STR_MAXLENGTH]; //!< 任务类型名

#define FIELD_STR_MAXLENGTH 28 //!< 任务控制块结构的注册字段名称最大长度（为了让结构体大小保持在48而选的这个数） 
typedef char tcb_field_name_t[FIELD_STR_MAXLENGTH]; //!< 任务控制块结构的注册字段名称

#define pri_is_valid(min_pri, max_pri, pri) (pri >= min_pri && pri <= max_pri)  //!< 任务优先级合法性检查
#define posix_core_pri_convert(pri) (POSIX_PRI_MODE ? (255-pri) : pri) //!< 转化任务优先级为posix风格 

#ifndef __multi_core__
extern thread_t thread_current; //!< 当前正在执行的任务ID 
#endif

extern int(*context_ctrl_to_reg_set_func)();//!< 获取任务上下文信息 函数指针

#ifndef __multi_core__
extern void (*sigtcb_free_ptr)(thread_t thread); //!< 信号控制块的释放函数指针
#endif

/**
 * @brief  任务钩子模块初始化函数
 * 
 * 任务钩子模块初始化函数
 *
 * @param	无
 *
 * @return  无
 *  
 */
void thread_hook_init(void);

/**
 * @brief  任务模块初始化函数
 * 
 * 任务模块初始化函数，该函数在系统初始化时调用
 *
 * @param	无
 *
 * @return  无
 *  
 */
void thread_module_init(int);

/**
 * @brief  添加任务控制块结构的注册字段
 * 
 * 添加任务控制块结构的注册字段
 *
 * @param	name 注册字段的名称
 * @param	dumpRtn 能够解析字段内容的函数的地址
 * @param   offset 注册字段相对控制块起始地址的偏移量
 *
 * @return  0 函数执行成功
 * @return  -1 函数执行失败
 *  
 */
int add_registered_field(tcb_field_name_t name, int(*dumpRtn)(thread_t), size_t *offset);

/**
 * @brief  删除任务控制块结构的注册字段
 * 
 * 删除任务控制块结构的注册字段
 *
 * @param	offset 注册字段的偏移量
 *
 * @return  0 函数执行成功
 * @return  -1 函数执行失败
 *  
 */
int remove_registered_field(size_t offset);

/**
 * @brief  获取当前正在执行的任务控制块
 * 
 * 获取当前正在执行的任务控制块
 *
 * @param	无
 *
 * @return  当前正在执行的任务控制块
 *  
 */
thread_t thread_current_get(); 

/**
 * @brief  获取指定任务的任务控制块
 * 
 * 通过任务ID获取任务控制块
 *
 * @param	id 任务ID
 * @param   task_type 任务类型
 * @param   addr_type 任务控制块地址类型
 *
 * @return  任务控制块
 * @return 	NULL
 *  
 */
thread_t thread_get2(thread_t id, task_type_t task_type, u32 addr_type);


/**
 * @brief  任务延迟
 * 
 * 该接口使任务延迟指定的滴答数。
 *
 * @param	ticks 延迟的滴答数
 *
 * @return  0 
 * @return 	未延迟足的滴答数（在延迟时间小于制定时间的情况下）
 *  
 */
u32 thread_delay(u32 ticks);

/**
 * @brief  任务延迟（用于实现绝对时间的睡眠）
 * 
 * 该接口使任务延迟指定的滴答数。
 *
 * @param	ticks 延迟的滴答数
 *
 * @return  0 
 * @return 	未延迟足的滴答数（在延迟时间小于制定时间的情况下）
 *  
 */
u32 thread_absdelay(u32 ticks);

/**
 * @brief  获取任务名
 * 
 * 该接口用于获取指定任务的名称。
 *
 * @param	tid 任务ID
 *
 * @return  任务名称的指针 函数执行成功 
 * @return 	NULL 	tid指定的任务不存在
 *  
 */
char *thread_name(void *tid);

/**
 * @brief 获得当前任务的任务类型
 * 
 * 获得当前任务的任务类型
 * 
 * @param 	thread  任务id
 * 
 * @return 	任务类型 函数执行成功
 * @return  -1 函数执行失败
 *
 */
task_type_t thread_type(thread_t thread);

/**
 * @brief 获得当前任务的id
 * 
 * 获得指定类型的当前任务的id
 * 
 * @param 	task_type  任务类型
 * 
 * @return 	任务ID 函数执行成功 
 * @return  0 当前任务和指定任务类型不匹配
 *
 */
thread_t thread_self(task_type_t task_type); /* 2 */

/**
 * @brief 显示任务的栈信息
 * 
 * 显示任务的栈信息
 * 
 * @param 	id 	     任务id
 * @param 	type  任务类型
 * 
 * @return 	无
 *
 */
void thread_check_stack(thread_t id, task_type_t type);
/**
 * @brief 任务信息显示
 * 
 * 显示当前任务或所有系统存在任务的基本信息。若参数为0则显示所有系统存在任务的基本信息，若参数不为0，
 * 则按参数指定任务显示其基本信息。
 * 
 * @param 	thread_name_or_id 	任务ID
 * 
 * @return 	无
 *
 */
void i(thread_t thread_name_or_id);
/**
 * @brief 任务信息显示
 * 
 * 显示当前任务的详细信息
 * 
 * @param 	thread_name_or_id 	任务ID
 * 
 * @return 	无
 *
 */
void ti(thread_t thread_name_or_id);

/**
 * @brief 任务是否支持浮点协处理器
 * 
 * 任务是否支持浮点协处理器
 * 
 * @param 	thread_ptr 	任务ID
 * 
 * @return 	1 	支持浮点协处理器
 * @return 	0	不支持
 *
 */
OS_BOOLEAN thread_is_fp(thread_t thread_ptr);

//void thread_entry_change(thread_t thread, void(*entry_point)(void*), void *param);

/**
 * @brief 显示任务的栈回溯信息
 * 
 * 显示指定任务的栈回溯信息
 * 
 * @param 	id 	任务ID
 * 
 * @return 	无
 *
 * @see pthread_showstackframe
 */
void thread_stackframe_show(thread_t id);

/**
 * @brief 获取任务对应的错误号
 * 
 * errno_of_thread_get()接口用来获取任务对应的错误号
 * 
 * @param 	thread_id 	任务ID
 * 
 * @return 	err 	函数运行成功，返回任务对应的错误号
 * @return 	-1		函数运行失败
 *
 * @exception ESRCH thread_id指定的任务不存在
 *
 * @see errno_of_thread_set
 */
int errno_of_thread_get(thread_t thread_id);

/**
 * @brief 设置任务对应的错误号
 * 
 * errno_of_thread_set()接口用来设置任务对应的错误号
 * 
 * @param 	thread_id 		任务ID
 * @param 	error_valule 	要设置的错误号
 * 
 * @return 	0 		函数运行成功
 * @return 	-1		函数运行失败
 *
 * @exception ESRCH thread_id指定的任务不存在
 *
 * @see errno_of_thread_get
 */
int errno_of_thread_set(thread_t thread_id, int error_valule);

#ifndef __multi_core__
/**
 * @brief 创建一个POSIX任务
 * 
 * 该接口用于创建一个POSIX任务，并自动回收
 * 
 * @param 	start_routine 	任务入口函数地址
 * @param   arg1            任务入口函数的参数
 * @param   arg2            任务入口函数的参数
 * @param   arg3            任务入口函数的参数
 * @param   arg4            任务入口函数的参数
 * @param   arg5            任务入口函数的参数
 * @param   arg6            任务入口函数的参数
 * @param   priority        任务优先级，若输入<=0或>255，则设默认为POSIX 155
 * @param 	stack_size 	          任务栈大小，若输入<4096，则设默认为200000字节
 * 
 * @return 	任务ID	创建成功
 * @return 	-1		创建失败
 *
 */
#else	
/**
 * @brief 创建一个POSIX任务
 * 
 * 该接口用于创建一个POSIX任务，并自动回收
 * 
 * @param 	start_routine 	任务入口函数地址
 * @param   arg1            任务入口函数的参数
 * @param   arg2            任务入口函数的参数
 * @param   arg3            任务入口函数的参数
 * @param   arg4            任务入口函数的参数
 * @param   arg5            任务入口函数的参数
 * @param   arg6            任务入口函数的参数
 * @param   priority        任务优先级，若输入<=0或>255，则设默认为POSIX 155
 * @param 	stack_size 	          任务栈大小，若输入<4096，则设默认为200000字节
 * @param 	cpuset 	                     任务绑定CPU亲和性，若输入非绑定cpuset核，默认为不绑定
 * 
 * @return 	任务ID	创建成功
 * @return 	-1		创建失败
 *
 */
#endif
int pthcrt(	void*(*start_routine)(void*), 
		    int		arg1,		/* first of nine args to pass to create pthread */
		    int		arg2,
		    int		arg3,
		    int		arg4,
		    int		arg5,
		    int		arg6,
		    int		priority,
		    int		stack_size
	#ifdef __multi_core__
		    , cpuset_t cpuset
	#endif
);

/* @} */

#ifdef __cplusplus
}
#endif 

#endif
