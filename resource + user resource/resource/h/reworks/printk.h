#ifndef _REWORKS_PRINTK_H
#define _REWORKS_PRINTK_H

#ifdef __cplusplus
extern "C"
{
#endif 
	
#include <stdarg.h>

unsigned long simple_strtoul(const char *cp, char **endp, unsigned int base);

typedef void(*PrintkcFunc)(char c);

void printk_init(PrintkcFunc func);

int printk(const char *fmt, ...);
int vprintk(const char *fmt, va_list ap);

typedef int *(*GetBpFunc)();

void printe_init(GetBpFunc func);

/* 内核的错误输出处理函数 */
void printe(const char *fmt, ...);

void printek(const char *fmt, ...);

int sprintk (char *s, const char *format, ...);

#ifdef __cplusplus
}

#endif 

#endif
