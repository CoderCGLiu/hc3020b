
/*******************************************************************************
 * 
 * 版权：             中国电子科技集团公司第三十二研究所
 * 描述：            本文件为监测模块加载后占用内存使用情况信息定义头文件
 * 
 */

#ifndef REWORKS_MODULE_MONITOR_H_INCLUDED__
#define REWORKS_MODULE_MONITOR_H_INCLUDED__

#ifdef __cplusplus
extern "C" 
{
#endif
/*
 * 文件模块加载后占用的内存使用情况
 * 输入：file_name 加载的模块绝对路径文件名
 * 输出： 模块专用内存大小size_t
 * 返回：0	模块文件未加载
 *       大于0		成功
 */
size_t module_get_footprint(const char *file_name);

#ifdef __cplusplus
}
#endif
#endif

