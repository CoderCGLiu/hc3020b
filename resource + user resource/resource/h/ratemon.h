/*! @file ratemon.h
    @brief ReWorks单调速率定时器操作的头文件
    
*	本文件定义了单调速率定时器相关的操作接口，包括创建、删除、启动、取消等。	
*	
* @version @reworks_version
* 
* @see 无
*/

#ifndef __REWORKS_RATEMON_H__
#define __REWORKS_RATEMON_H__

#ifdef __cplusplus
extern "C"
{
#endif 

#include <reworks/types.h>
#include <sys/times.h>
#include <reworks/thread.h>

/**  
 * @addtogroup group_timer
 *	单调速率管理提供管理周期任务执行的手段。它支持使用单调速率调度算法 （RMS）的应用程序，
 *将保证他们的周期任务即使在短暂的超载条件下，也不会到达他们的截止时间。单调速率管理程序
 *要求时钟/定时器管理（看门狗）的支持。	<br/>
 *	ReWorks操作系统提供了创建、删除、启动和取消单调速率定时器的操作接口,同时提供了用于获
 *取与显示单调速率定时器对象相关信息的操作接口。
 
 * 头文件：
 *
 * #include <ratemon.h> <br/>
 * #include <ratemon_show.h>
 *
 * @{ */	
 
/**
 * 单调速率定时器的状态
 */
typedef enum
{
    RMS_INACTIVE,  	//!< 无效状态  /* off chain, never initialized */
    RMS_OWNER_IS_BLOCKING,  //!< 阻塞状态  /* on chain, owner is blocking on it */
    RMS_ACTIVE,  	//!< 运行状态 /* on chain, running continuously */
    RMS_EXPIRED 	//!< 超时状态 /* off chain, will be reset by next */
} rms_states;


/*! \struct rms_status
 * \brief 单调速率定时器状态的结构定义
 *
 * 该数据结构定义了单调速率定时器的状态信息。
 */
typedef struct
{
	rms_states state;						//!< 单调速率定时器状态
	u32 ticks_since_last_period;			//!< 本次启动至今的ticks数
	u32 ticks_executed_since_last_period; 	//!< 本次启动执行的ticks数
} rms_status;

/**
 * 单调速率定时器标识对应的数据类型 
 */
typedef unsigned long rms_t;

/**
 * @brief 初始化单调速率定时器模块
 *
 * 该接口用于初始化系统的单调速率定时器模块，主要用于系统配置。
 *
 * @param	max_rmses 	系统支持的单调速率定时器对象的数目
 * 
 * @return	0	函数执行成功
 * @return 	-1	函数执行失败
 *
 * @exception EINVAL 参数max_rmses取值不合法
 * @exception EMINITED 单调速率定时器模块已被初始化
 * 
 */
int rms_module_init(int max_rmses);

/**
 * @brief    创建单调速率定时器
 * 
 * 该接口用于创建单调速率定时器。该接口在对象池中分配一个单调速率定时器，
 *同时分配一个其所使用的资源。
 *
 * @param	rms_id 	存放返回的单调速率定时器标识    
 *
 * @return	0		函数执行成功
 * @return	-1		函数执行失败
 *
 * @exception EINVAL rms_id是无效指针
 * @exception EAGAIN 没有足够的单调速率定时器资源或看门狗资源
 * @exception EMNOTINITED 单调速率定时器模块尚未初始化
 * @exception ECALLEDINISR 该接口不能在中断上下文中调用
 *
 */
int rms_create(rms_t *rms_id);

/**
 * @brief    初始化并启动单调速率定时器
 * 
 * 该接口用于初始化并启动rms_id指定的单调速率定时器的执行。在参数periods的取值代表下一个单调速率
 *周期长度。当创建单调速率定时器后第一次或取消单调速率定时器后再次执行该接口时，只是会按照periods
 *建立时限，使得定时器变为活动（ACTIVE）状态而不会等待。若单调速率定时器已经是活动状态，则该接口
 *将令调用任务等待直至下一周期，然后以periods指定的周期启动下一单调速率周期，并返回以继续运行
 *任务。若单调速率定时器处在过期（EXPIRED）状态下，调用该接口不会等待，但仍会启动下一单调速率周期，
 *只是返回不为成功。在参数periods的取值为NO_TIMEOUT的情况下，则该接口只是用来获取单调速率定时器
 *的状态并返回。
 * 
 * @param	rms_id		单调速率定时器标识
 * @param 	periods		单调速率定时器的长度
 * 
 * @return	0			函数执行成功，并且单调速率定时器已启动
 * @return	-1			函数执行失败，并设置错误号errno以指示错误
 * @return	EINVAL		periods取值为NO_TIMEOUT的情况下才返回该值，表示单调速率定时器当前处于不活动（INACTIVE）状态
 * @return	ETIMEDOUT 	periods取值为NO_TIMEOUT的情况下，表示单调速率定时器当前处于过期（EXPIRED）状态；
 *  					periods取值不为NO_TIMEOUT的情况下，表示之前的单调速率定时器当前周期已过期
 *
 * @exception  EINVAL  	单调速率定时器标识rms_id无效
 * @exception  EPERM   	当前任务不是拥有单调速率定时器的任务
 * @exception  EOBJDELETED 因单调速率定时器被删除而终止等待当前周期
 * @exception  EMNOTINITED 单调速率定时器模块尚未初始化
 * @exception  ECALLEDINISR 该接口不能在中断上下文中调用
 * 
 * @attention 该接口的调用者必须是单调速率定时器的所有者。
 */
int rms_run(rms_t rms_id, unsigned int periods);

/**
 * @brief    取消单调速率定时器
 * 
 * 该接口用于取消rms_id指定的单调速率定时器最近一个周期的执行。该接口
 *将单调速率定时器的状态改为不活动（INACTIVE）状态。
 *
 * @param   rms_id 	单调速率定时器ID    
 *
 * @return	0		函数执行成功
 * @return	-1		函数执行失败
 *
 * @exception  EINVAL 单调速率定时器标识rms_id无效
 * @exception  EPERM  当前任务不是单调速率定时器的所有者
 * @exception  EMNOTINITED 单调速率定时器模块尚未初始化
 * @exception  ECALLEDINISR 该接口不能在中断上下文中调用
 * 
 * @attention 该接口的调用任务必须是对单调速率定时器所拥有的任务。
 */
int rms_cancel(rms_t rms_id);

/**
 * @brief    删除单调速率定时器
 * 
 * 该接口用于删除rms_id指定的单调速率定时器。该接口会取消并释放与单调速率定时器对象及关联的
 *资源。在调用该接口删除单调速率定时器时，如果相关单调速率任务等待指定的
 *单调速率定时器周期结束时，则任务会解除等待，再执行删除对象操作。
 *
 * @param	rms_id 	单调速率定时器ID
 *
 * @return	0		函数执行成功
 * @return	-1		函数执行失败
 *
 * @exception  EINVAL 单调速率定时器标识rms_id无效
 * @exception  EMNOTINITED 单调速率定时器模块尚未初始化
 * @exception  ECALLEDINISR 该接口不能在中断上下文中调用
 *
 */
int rms_delete(rms_t rms_id);

/**
 * @brief    获取单调速率定时器状态
 * 
 * 该接口用于获取一个单调速率定时器当前的运行状态。单调速率定时器的状态信息包括单调速率定时器
 *当前的状态、单调速率定时器最近一个周期至今的流逝滴答数（tick）和执行滴答数。
 * 
 * @param    rms_id 	单调速率定时器ID
 * @param    status 	存放返回的单调速率定时器状态的地址
 *
 * @return   0		函数执行成功
 * @return   -1		函数执行失败
 *
 * @exception EINVAL  单调速率定时器标识rms_id无效，或者status是无效指针
 * @exception EMNOTINITED 单调速率定时器模块尚未初始化
 *
 */
int rms_getstatus(rms_t rms_id, rms_status *status);

/** @example example_rms_create.c
 * 下面的例子演示了如何使用接口 rms_create( ).
 */
/** @example example_rms_run.c
 * 下面的例子演示了如何使用接口 rms_run( ).
 */
/** @example example_rms_cancel.c
 * 下面的例子演示了如何使用接口 rms_cancel( ).
 */
/** @example example_rms_delete.c
 * 下面的例子演示了如何使用接口 rms_delete( ).
 */
/** @example example_rms_getstatus.c
 * 下面的例子演示了如何使用接口 rms_getstatus( ).
 */

/* @}*/

#ifdef __cplusplus
}
#endif 

#endif	// __REWORKS_RATEMON_H__
