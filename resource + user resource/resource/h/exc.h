
/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：定义异常管理框架中用到的宏和结构体。
 * 修改：       
 * 		2014-03-07, 宋伟, 创建. 
 */

#ifndef _REWORKS_EXC_H_
#define _REWORKS_EXC_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <cpu.h>
#include <excvec.h>

typedef void (*EXC_HANDLER)(int, REG_SET *);

/* 异常向量表数据结构 */
typedef struct {
	int			vecnum;	
	EXC_HANDLER handler;
}EXC_TBL;

/* 异常挂接接口 */
EXC_HANDLER exception_handler_set(EXC_HANDLER the_exc_handler);
int exc_install_handler(int vec, EXC_HANDLER handler);
int exc_uninstall_handler(int vec);
EXC_HANDLER exc_handler_get(int vec);

int exception_module_init();

#ifdef __cplusplus
}
#endif
#endif
