/*! @file event.h
	@brief ReWorks事件操作的头文件
	
 * 本文件定义了事件相关的操作接口。
 *  
 * @version 4.7
 * 
 * @see 无
 */
#ifndef _EVENT_H_
#define _EVENT_H_

#ifdef __cplusplus
extern "C"
{
#endif
	
#include <reworks/types.h>
#include <pthread.h>

/** @defgroup group_event ReWorks事件管理
 * @ingroup group_os_reworks
 *	ReWorks内核提供一个特殊的寄存器作为每个任务控制块的一部分，即事件（event）寄存器。
 *它是一个属于任务的对象，并由一组跟踪指定事件值的二进制值事件标志组成。ReWorks
 *中一个事件寄存器的长度为32bit，寄存器中每一位作为二进制标志(事件标志)可以被设置或删除。<br/>
 *该部分提供ReWorks的事件功能接口，任务与ISR可以使用事件标志来向其他任务发送信号。	
 
 * 头文件：
 *
 * #include <event.h>
 * @{ */	
 
#define OS_ALL_EVENTS  0xFFFFFFFF	 //!< 所有事件对应的宏定义

#define OS_EVENT_ALL    0x00000000   //!< 等待所有事件
#define OS_EVENT_ANY    0x00000002   //!< 等待任意一个事件

#define OS_EVENT_0     0x00000001    //!< 用户自定义事件0
#define OS_EVENT_1     0x00000002    //!< 用户自定义事件1
#define OS_EVENT_2     0x00000004    //!< 用户自定义事件2
#define OS_EVENT_3     0x00000008    //!< 用户自定义事件3
#define OS_EVENT_4     0x00000010    //!< 用户自定义事件4
#define OS_EVENT_5     0x00000020    //!< 用户自定义事件5
#define OS_EVENT_6     0x00000040    //!< 用户自定义事件6
#define OS_EVENT_7     0x00000080    //!< 用户自定义事件7
#define OS_EVENT_8     0x00000100    //!< 用户自定义事件8
#define OS_EVENT_9     0x00000200    //!< 用户自定义事件9
#define OS_EVENT_10    0x00000400    //!< 用户自定义事件10
#define OS_EVENT_11    0x00000800    //!< 用户自定义事件11
#define OS_EVENT_12    0x00001000    //!< 用户自定义事件12
#define OS_EVENT_13    0x00002000    //!< 用户自定义事件13
#define OS_EVENT_14    0x00004000    //!< 用户自定义事件14
#define OS_EVENT_15    0x00008000    //!< 用户自定义事件15
#define OS_EVENT_16    0x00010000    //!< 用户自定义事件16
#define OS_EVENT_17    0x00020000    //!< 用户自定义事件17
#define OS_EVENT_18    0x00040000    //!< 用户自定义事件18
#define OS_EVENT_19    0x00080000    //!< 用户自定义事件19
#define OS_EVENT_20    0x00100000    //!< 用户自定义事件20
#define OS_EVENT_21    0x00200000    //!< 用户自定义事件21
#define OS_EVENT_22    0x00400000    //!< 用户自定义事件22
#define OS_EVENT_23    0x00800000    //!< 用户自定义事件23
#define OS_EVENT_24    0x01000000    //!< 用户自定义事件24
#define OS_EVENT_25    0x02000000    //!< 用户自定义事件25
#define OS_EVENT_26    0x04000000    //!< 用户自定义事件26
#define OS_EVENT_27    0x08000000    //!< 用户自定义事件27
#define OS_EVENT_28    0x10000000    //!< 用户自定义事件28
#define OS_EVENT_29    0x20000000    //!< 用户自定义事件29
#define OS_EVENT_30    0x40000000    //!< 用户自定义事件30
#define OS_EVENT_31    0x80000000    //!< 用户自定义事件31
 

/**
 * @brief  事件模块的初始化
 * 
 * 该接口用于向任务控制块注册事件相关的域，一般由系统在初始化时调用。
 *
 * @param	无
 *	
 * @return	0 	函数执行成功
 * @return 	-1	函数执行失败
 * 
 * @exception EMINITED 事件模块已被初始化
 *
 */
int event_module_init(void);

/**
 * @brief 向指定的任务发送事件
 * 
 * 该接口用于发送一个事件到指定的任务。如果发送的事件满足指定任务等待的事件条件，
 *则该任务将进入就绪状态。该接口可以发送事件或事件集，例如要发送事件OS_EVENT_1和OS_EVENT_2，
 *则参数event_in设置为OS_EVENT_1|OS_EVENT_2。
 *
 * @param 	id		    接收事件的任务ID，取值为0时，表示给当前任务发送事件
 * @param	event_in	发送的事件，可以是OS_EVENT_0到OS_EVENT_31中的任意事件或事件组合
 *
 * @return	0	发送事件成功
 * @return 	-1	发送事件失败
 *
 * @exception ESRCH		没有找到指定的任务
 * @exception EINVAL	event_in的取值为0
 * @exception ECALLEDINISR 接口参数id为0的情况下，接口不能在中断上下文中调用
 * @exception EMNOTINITED 事件模块尚未初始化
 *
 * @see event_recv() 
 */
int event_send(pthread_t id, unsigned int event_in);
	
/** 
 * @brief 接收事件
 *
 * 该接口用于接收指定的事件条件， 接收的事件可以是OS_EVENT_0到OS_EVENT_31的任意事件或事件组合，
 *也可以是OS_ALL_EVEN。<br/>
 * 参数options取值中的WAIT和NO_WAIT选项用来指定任务是否等待事件条件满足，OS_EVENT_ANY和
 *OS_EVENT_ALL用来指定是否事件条件中的任意一个事件或全部事件必须满足。<br/>
 * 如果调用任务选择等待，那么它将因等待事件条件而阻塞，参数ticks被用来指定最大的等待时间，
 *如果将ticks设置为NO_TIMEOUT，那么调用任务将永远等待。
 *
 * @param	event_in 	要接收的事件，其取值可以是OS_EVENT_0到OS_EVENT_31中的任意事件或
 * 						事件组合，也可以是OS_ALL_EVEN
 * @param	options		事件选项，分为WAIT或NO_WAIT、OS_EVENT_ANY或OS_EVENT_ALL <br/>
 * 						WAIT表示任务阻塞等待事件，NO_WAIT表示任务不阻塞等待事件；<br/>
 * 						OS_EVENT_ANY表示任务满足参数event_in中的任一事件即可解除阻塞，
 * 						OS_EVENT_ALL表示任务必须满足参数event_in中的全部事件才可解除阻塞
 * @param	ticks 		等待事件的超时时间，单位为tick，若设为NO_TIMEOUT，则调用任务将永远等待
 * @param	event_out	存放使任务事件条件满足的事件值，如果接收到的事件满足事件条件，那么
 *						event_out被置为使条件满足的事件，并且任务接收到的事件集中相应的事件被清除；
 						如果事件条件不满足且调用任务选择不等待，或者任务等待超时，那么event_out
 						被置为任务当前接收到的事件中符合事件条件的事件集合。
 *
 * @return	0	 接收事件成功
 * @return	-1	 接收事件失败
 *
 * @exception ETIMEDOUT	在options设置了WAIT并且指定了超时时间ticks的情况下，任务接收事件超时
 * @exception EAGAIN	在options设置了NO_WAIT的情况下，没有接收到指定的事件
 * @exception ECALLEDINISR 接口不能在中断上下文中调用
 * @exception EMNOTINITED 事件模块尚未初始化
 * 
 * @see event_send()
 * 
 */
int event_recv(unsigned int event_in, unsigned int options, unsigned int ticks, unsigned int *event_out);

/** 
 * @brief 清除任务接收事件
 *
 * 该接口用于清除调用该接口任务的接收事件
 *
 * @param	无
 *
 * @return	0	 清除事件成功
 * @return	-1	 清除事件失败
 *
 * @exception ECALLEDINISR 接口不能在中断上下文中调用
 * @exception EMNOTINITED 事件模块尚未初始化
 * 
 * @see event_send()
 * 
 */
int event_clear(void);

/** @example example_event_send.c
 * 下面的例子演示了如何使用 event_send().
 */ 

/** @example example_event_recv.c
 * 下面的例子演示了如何使用 event_recv().
 */
/* @}*/
	
#ifdef __cplusplus
}

#endif 

#endif
