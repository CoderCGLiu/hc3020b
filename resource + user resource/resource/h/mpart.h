/*! @file mpart.h
    @brief ReWorks内存分区管理头文件
    
 * 本文件定义了内存分区相关的操作接口。
 * 
 * @version 4.7
 * 
 * @see 无
 */
#ifndef _REWORKS_MPART_H_
#define _REWORKS_MPART_H_

#ifdef __cplusplus
extern "C"
{
#endif 

#include <reworks/types.h>

/**  
 * @defgroup group_mpart ReWorks内存分区管理
 * @ingroup group_os_reworks
 *
 *	内存分区管理用于将已分配的用户堆内存组成一个或多个统一的内存分区，实现内存的二次分配、
 *释放等操作，使一个嵌入式应用能够使用相对固定的内存区域。<br/>
 *	ReWorks核心提供了内存分区的创建、销毁、添加内存块以及分配、释放内存的相关操作接口，
 *同时提供了用于获取内存分区对象相关信息的操作接口。	
 
 * 头文件：
 *
 * #include <mpart.h> <br/>
 * #include <mpart_show.h>
 * @{ */	
 

typedef unsigned long mpart_id;	//!< 指示内存分区标识符的数据类型
/**  
* @brief  初始化内存分区模块，创建内存分区对象池
* 
* 该接口由sysConfigInition()程序调用，用于初始化内存分区模块，由参数max_mparts
*指定系统中能创建的内存分区对象的最大数目。
*
* @param    max_parts	内存分区的最大数目
*
* @return	0	函数执行成功
* @return 	-1	函数执行失败
*
* @exception EINVAL 参数max_parts取值不合法
* @exception EMINITED 内存分区模块已被初始化
*
*/
int mpart_module_init(int max_parts);

/**  
* @brief    创建内存分区
* 
* 该接口用于在指定的内存块上创建一个内存分区。新创建的内存分区的标识存放在参数mid中，
*该mid可以用来调用内存分区的其它管理接口。内存分区能够管理任何数量的不连续的内存块。
*
* @param    addr	内存地址
* @param    size	内存长度
* @param    mid		存放内存分区标识的指针
*
* @return    0	函数执行成功
* @return    -1	函数执行失败
*
* @exception EINVAL mid是无效指针，或者addr指定的内存无效，或者size取值无效
* @exception EAGAIN 系统资源不足，或已超过系统限制的内存分区总数
* @exception EMNOTINITED  内存分区模块尚未初始化
*
*/
int mpart_create(char *addr, size_t size, mpart_id *mid);

/**  
* @brief    增加内存块至内存分区
* 
* 该接口用于将指定的内存添加指定的内存分区，添加的内存和内存分区之前的内存无需是连续的。
*
* @param    mid 	内存分区标识
* @param    addr 	新增加的内存地址
* @param    size 	内存长度
*
* @return    0		函数执行成功
* @return    -1		函数执行失败
*
* @exception EINVAL mid指定的内存分区不存在，或者addr指定的内存无效
* @exception EMNOTINITED 内存分区模块尚未初始化
*
* @attention 内存分区中的内存块之间不能重叠
*/
int mpart_addmem(mpart_id mid, char *addr, size_t size);

/**  
* @brief    从内存分区分配内存
* 
* 该接口在指定内存分区中分配大小为size的内存。
*
* @param    mid 	内存分区标识
* @param    size 	需要分配的内存长度
*
* @return    非0	成功分配内存
* @return    NULL	分配失败
*
* @exception EINVAL mid指定的内存分区不存在，或者size取值为0
* @exception ENOMEM 没有足够的内存可供分配
* @exception EMNOTINITED 内存分区模块尚未初始化
*
*/
void *mpart_alloc(mpart_id mid, size_t size);

/**  
* @brief    从内存分区分配边界对齐的内存
* 
* 该接口从指定的内存分区分配大小为size的缓存，它保证分配的缓存的开始地址按照边界对齐。
*
* @param    mid 	内存分区标识
* @param    alignment 需要对齐的内存边界要求，其取值必须是【void *】类型长度的2的N次方倍数
* @param    size 	需要分配的内存长度
*
* @return    非0	成功分配内存
* @return    NULL	分配失败
*
* @exception EINVAL mid指定的内存分区不存在，或者alignment不是【void *】类型长度的2的N次方倍数，
* 					或者size取值为0
* @exception ENOMEM 没有足够的内存可供分配
* @exception EMNOTINITED 内存分区模块尚未初始化
*/
void *mpart_memalign(mpart_id mid, size_t alignment, size_t size);

/**  
* @brief    从内存分区重新分配内存
* 
* 该接口改变指定内存块的大小并返回一个指向新内存块的指针，新内存块中原有的内容保持不变。
*
* @param    mid 	内存分区标识
* @param    addr 	需要重新分配的内存地址，如果addr为NULL，则该接口等同于mpart_alloc()。
* @param    size 	需要分配的内存长度 <br/>
*			若size值小于原先分配的内存长度，则内存内容不会改变，且返回原先的内存起始地址； <br/>
*			若size值大于原先分配的内存长度，则内存内容不会改变，但不一定返回原先的内存起始地址，并且新增加的内存未设置初始值； <br/>
*			若size为0，相当于调用mpart_free()。
*
* @return    非0	成功分配内存
* @return    NULL	分配失败
*
* @exception EINVAL mid指定的内存分区不存在，或者addr指定的内存无效
* @exception ENOMEM 没有足够的内存可供分配
* @exception EMNOTINITED 内存分区模块尚未初始化
*
* @attention 新内存块的对齐边界不能保证和原内存块相同。
*/
void *mpart_realloc(mpart_id mid, char *addr, size_t size);

/**  
* @brief    向内存分区释放内存
* 
* 该接口将之前通过 mpart_alloc(), mpart_memalign() 或 mpart_realloc() 所分配的内存块释放至内存分区的空闲内存列表中。
*
* @param    mid  内存分区标识
* @param    addr 需要释放的内存地址，若addr为空指针，则该接口直接返回
*
* @return   0	函数执行成功
* @return	-1	函数执行失败
*
* @exception EINVAL	mid指定的内存分区不存在，或者addr指定的内存无效
* @exception EMNOTINITED 内存分区模块尚未初始化
*
*/
int mpart_free(mpart_id mid, char *addr);

/** 
* @brief    在内存分区中查找最大的空闲内存块
* 
* 该接口在指定内存分区的空闲列表中查找最大的空闲内存块，并返回它的大小。
*
* @param    mid 内存分区标识
*
* @return   >0	最大可用内存块的大小，以字节为单位。
* @return	-1	函数执行失败
*
* @exception EINVAL	mid指定的内存分区不存在
* @exception EMNOTINITED 内存分区模块尚未初始化
*/
ssize_t mpart_findmaxfree(mpart_id mid);

/** 
* @brief    删除内存分区
* 
* 该接口用于删除指定的内存分区，将内存分区对象回收到内存分区对象池中。
*
* @param    mid 内存分区标识
*
* @return   0	函数执行成功
* @return	-1	函数执行失败
*
* @exception EINVAL	mid指定的内存分区不存在
* @exception EMNOTINITED 内存分区模块尚未初始化
*
*/
int mpart_delete(mpart_id mid);

/** @example example_mpart_create.c
* 下面的例子演示了如何使用接口 mpart_create( ) 和 mpart_getinfo( ).
*/
/** @example example_mpart_addmem.c
* 下面的例子演示了如何使用接口 mpart_addmem( ).
*/
/** @example example_mpart_alloc.c
* 下面的例子演示了如何使用接口 mpart_alloc( ).
*/
/** @example example_mpart_memalign.c
* 下面的例子演示了如何使用接口 mpart_memalign( ).
*/
/** @example example_mpart_realloc.c
* 下面的例子演示了如何使用接口 mpart_realloc( ).
*/
/** @example example_mpart_findmaxfree.c
* 下面的例子演示了如何使用接口 mpart_findmaxfree( ).
*/
/** @example example_mpart_free.c
* 下面的例子演示了如何使用接口 mpart_free( ).
*/
/** @example example_mpart_delete.c
* 下面的例子演示了如何使用接口 mpart_delete( ).
*/

/* @}*/

#ifdef __cplusplus
}
#endif 

#endif		// __REWORKS_MEMORY_H__
