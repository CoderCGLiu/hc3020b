/*! @file object_show.h
    @brief ReWorks对象管理信息获取/显示头文件
    
 * 本文件定义了对象管理信息获取/显示相关的操作接口。
 *
 * @version 4.7
 * 
 * @see 无
 */
#ifndef _REWORKS_OBJECTSHOW_H_
#define _REWORKS_OBJECTSHOW_H_

#ifdef __cplusplus
extern "C"
{
#endif 

#include <reworks/types.h>

/** @defgroup group_object ReWorks对象管理
 * @ingroup group_os_reworks
 *
 *	ReWorks操作系统核心在内部使用对象管理机制来控制核心对象的创建、删除、使用。<br/>
 *	这一部分仅提供获取与显示对象使用情况的操作接口供程序调试和系统监控程序调用。

 * 头文件：
 *
 * #include <object_show.h>
 * @{ */

typedef unsigned long object_manager_id_t; //!< 对象管理器标识符类型

/*! \struct object_manager_info
 * \brief 对象管理器信息结构定义
 *
 * 该数据结构定义了对象管理器的相关信息，包括管理的对象名称、已分配对象数、最大允许分配对象数。
 */
typedef struct object_manager_info_t
{
	const char *name;       //!< 对象管理器管理的这一类对象的名称
	u32	alloc_obj_num; 		//!< 对象管理器已分配对象数
	u32 max_obj_num;      	//!< 对象管理器最大允许分配对象数
} object_manager_info_t; //!< 对象管理器信息结构定义

/** 
 * @brief 对象管理器信息显示模块初始化。
 * 
 * 模块初始化接口。
 * 
 * @param 	无
 * 
 * @return 	无
 * 
 * @note 该接口用于初始化，由ReDe生成代码在初始化阶段调用它。
 */
void object_show_init();

/** 
 * @brief 显示指定对象管理器的信息。
 * 
 * 该接口用于显示指定对象管理器的相关信息。
 * 
 * @param 	mgr_id	指定对象管理器的标识符
 * 
 * @return 	0		函数执行成功
 * @return 	-1	 	mgr_id无效
 * 
 * @exception EINVAL 参数无效
 */
int object_manager_show(object_manager_id_t mgr_id);

/** 
 * @brief 获取指定对象管理器的概要信息。
 * 
 * 该接口用于获取指定对象管理器的概要信息，包括管理的对象名称、已分配对象数、
 * 最大允许分配对象数等信息。
 * 
 * @param 	mgr_id	 指定对象管理器的标识符
 * @param 	mgr_info	存放对象管理器信息的指针
 * 
 * @return 	0 		函数执行成功
 * @return 	-1  	参数mgr_id或mgr_info为无效指针，或者mgr_id指定的对象管理器不存在
 * 
 * @exception EINVAL 参数无效
 */
int object_manager_getinfo(object_manager_id_t mgr_id, object_manager_info_t *mgr_info);


/**  
 * @brief    获得系统中对象管理器的个数
 * 
 * 出于遍历系统对象管理器的需要，提供返回对象管理器个数的功能。 
 *
 * @param    无
 *
 * @return   系统中对象管理器的个数
 *
 * @exception 无
 */
u32 object_manager_gettotalnum();

/**  
 * @brief    批量获得系统中对象管理器的标识符
 * 
 * 出于遍历系统对象管理器的需要，提供批量返回对象管理器个数的功能。 
 *
 * @param    list 用户分配的保存标志符的数组，它的长度应等于list_len所指向的内存中保存的数值大小。
 * @param    list_len 它是一个输入/输出的参数，输入时它所指向的内存保存的数值表示list的长度，输出时
 * 					它指向的内存保存的数值表示获取的标识符个数。输出的值不会大于输入的值。
 *
 * @return   0 执行成功
 * @return   -1 执行失败
 *
 * @exception list_len指向的变量的数值为0
 */
int object_manager_getlist(object_manager_id_t list[], u32 *list_len);

/**  
 * @brief    获得指定下标的对象管理器的标识符
 * 
 * 在特殊的情况（如变量数量受限）下，提供依次返回对象管理器标识符的功能。 
 * 系统在初始化完成后，对象管理器的情况就固定下来了（不增不减），所以可将对象管理器的集合视作
 * 一个长度为object_manager_gettotalnum()的静态列表。
 *
 * @param    idx 对象管理器的下标，它不应达到或超过object_manager_gettotalnum()的返回值
 *
 * @return   0表示错误
 * @return   其余表示对象管理器的标识符
 *
 * @exception EINVAL idx无效
 */
object_manager_id_t object_manager_getbynum(u32 idx);

/**  
 * @brief    显示对象信息
 * 
 * 该接口供用户在标准输出上观察对象管理信息使用，它会显示对象类别的名称，
 * 该类对象最大允许分配数，已分配数，并根据内存剩余情况，估算实际最大可分配数（及可再分配数）。
 * 估算情况仅供参考，由于内存分配算法实际运行情况，对象可能额外需要分配内存（例如任务
 * 需要分配栈空间、消息队列需要分配消息池）这些可变因素，实际还可分配对象数可能会小于
 * 估算数值。
 *
 * @param   无
 *
 * @return	无
 *
 * @exception 无
 *
 */
void oi();

/** @example example_object_manager_usecase.c
* 下面的例子演示了如何使用接口object_manager_getinfo()、object_manager_gettotalnum()、
* object_manager_getlist()、oi()
*/

/* @}*/

#ifdef __cplusplus
}
#endif 

#endif		// __REWORKS_MEMORY_H__

