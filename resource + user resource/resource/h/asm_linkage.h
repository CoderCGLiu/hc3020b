#ifndef _ASM_LINKAGE_H_
#define _ASM_LINKAGE_H_

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _ASMLANGUAGE

#ifndef ENTRY
#define ENTRY(name) \
  .text; \
  .globl name; \
  name:
#endif

#ifndef WEAK
#define WEAK(name)	   \
	.weak name;	   \
	name:
#endif

#ifndef END
#if defined(__arm__) || defined(__i386__) || defined(__ppc__)
#define END(name) \
  .size name, .-name
#else
#define END(name) \
  .end	name; \
  .size name, .-name
#endif
#endif

/* If symbol 'name' is treated as a subroutine (gets called, and returns)
 * then please use ENDPROC to mark 'name' as STT_FUNC for the benefit of
 * static analysis tools such as stack depth analyzer.
 */
#ifndef ENDPROC
#if defined(__arm__) || defined(__i386__) || defined(__ppc__)
#define ENDPROC(name) \
  .type name, %function; \
  .size name, .-name
#else
#define ENDPROC(name) \
  .type name, @function; \
  END(name)
#endif
#endif


#endif

#ifdef __cplusplus
}
#endif

#endif
