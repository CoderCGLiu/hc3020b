/*! @file mqueue.h
    @brief ReWorks消息队列操作的头文件
    
 * 本文件定义了消息队列相关的操作接口。
 * 	
 * @version 4.7
 * 
 * @see 无
 */
#ifndef _REWORKS_MQUEUE_H_
#define _REWORKS_MQUEUE_H_

#ifdef __cplusplus
extern "C"
{
#endif 

#include <reworks/types.h>
#include <sys/signal.h>
#include <limits.h>	

/* \cond */
#define _POSIX_OPEN_MAX     (1024)
#define _POSIX_MQ_OPEN_MAX  (1024)
#define MSGQ_SEND_REQUEST   INT_MIN
#define MSGQ_URGENT_REQUEST INT_MAX
/* \endcond */

/** @defgroup group_msg_queue ReWorks消息队列管理
 * @ingroup group_os_reworks
 *	消息队列是一个类似于缓冲区的对象，为任务间的数据通信提供了一种有效的机制。
 *任务可以通过它发送消息，也可以通过它接收消息，从而实现数据的同步及通信。
 *消息队列会暂时保存来自发送者的消息，直到有意的接收者准备读这些消息，
 *这个临时缓存区把发送任务和接收任务分开，也就是说它将消息在发送和接收消息的任务间进行传递。<br/>
 *  ReWorks操作系统核心提供了用于创建/打开消息队列、发送/接收消息的操作接口，
 *同时提供了用于获取与显示消息队列对象相关信息的操作接口。
 
 * 头文件：
 *
 * #include <mqueue.h> <br/>
 * #include <mqueue_show.h>
 * @{ */	
 
#define MQ_PRIO_MAX     (32)  //!< 消息的优先级限制，须小于该值


typedef unsigned long mqd_t;    //!< 消息队列描述符
/*! \struct mq_attr
 * \brief 消息队列属性结构定义
 *
 * 该数据结构定义了消息队列的相关属性，包括消息队列的标志、可存放的消息数、消息的最大长度、当前消息数目和任务等待策略。
 */
struct mq_attr
{
    long mq_flags;     //!< 消息队列标志
    long mq_maxmsg;    //!< 消息队列中可存放的消息数目
    long mq_msgsize;   //!< 消息队列中消息的最大长度
    long mq_curmsgs;   //!< 消息队列中当前的消息数目
    int mq_waitqtype;  //!< 阻塞在消息队列上的任务的等待策略，取值包括 PTHREAD_WAITQ_FIFO和PTHREAD_WAITQ_PRIO <br/>
    				//!< PTHREAD_WAITQ_FIFO表示任务按照先进先出的策略排队等待；PTHREAD_WAITQ_PRIO表示任务按照优先级的策略排队等待。
};

/**
 * @brief 初始化系统消息队列模块
 *
 * 该接口初始化系统的消息队列模块，主要用于系统配置。
 *
 * @param	max_msgqs 	系统支持的消息队列对象的数目
 * 
 * @return	0	函数执行成功
 * @return 	-1	函数执行失败
 *
 * @exception EINVAL 参数max_msgqs取值不合法
 * @exception EMINITED 消息队列模块已被初始化
 */
int mq_module_init(int max_msgqs);

/**
 * @brief 打开消息队列
 *
 * 该接口用于创建一个新的命名消息队列，或打开一个已存在的命名消息队列。
 *该接口的返回值称为消息队列描述符，其他接口可以用这个描述符来指定消息队列。
 *如果其他任务以同样的名字调用该接口，那么任务打开的消息队列描述符将指向同一个消息队列。
 *
 * @param	name	消息队列名，其长度限制为NAME_MAX，如果name不是已存在的消息队列的名字，
 * 				并且oflag参数中没有包含O_CREAT标志，那么该接口将失败。
 * @param	oflag 	消息队列选项，用来指定对消息队列的接收和/或发送访问权限。<br/>
 *				其值可以是O_RDONLY、O_WRONLY或O_RDWR之一，并可按位OR上O_CREAT、O_EXCL或
 				O_NONBLOCK。这些标志的意义如下：<br/>
 　　O_RDONLY - 打开的消息队列用来接收消息。任务可以使用返回的消息队列描述符调用 mq_receive()，
			但不能调用 mq_send()，消息队列可以被相同或不同的接收消息任务打开多次。 <br/> 
 　　O_WRONLY - 打开的消息队列用来发送消息。任务可以使用返回的消息队列描述符调用 mq_send()，
			但不能调用 mq_receive() 。消息队列可以被相同或不同的发送消息的任务打开多次。<br/>  
 　　O_RDWR - 打开的消息队列用来接收和发送消息。任务可以使用返回的消息队列描述符调用任何接口。
			一个消息队列可以在相同或不同的发送/接收消息的任务中打开多次。<br/>  
 　　O_CREAT - 创建一个消息队列，它要求两个附加参数：mode_t 类型的mode和 mq_attr 类型的指针attr。
			这两个附加参数的说明列于本参数之后。如果用name来创建一个已经存在消息队列，那么这个标志位不起作用，
			否则会创建一个空的消息队列。<br/>
 　　O_EXCL - 如果同时设置 O_EXCL和O_CREAT，那么该接口只在指定name的消息队列不存在时才创建新的消息队列；
			如果name指定的消息队列已存在，该接口将会失败。  <br/>
 　　O_NONBLOCK - 该标志使得一个消息队列在队列为空时的读和队列填满时的写不被阻塞，详见 mq_send()
			和 mq_receive() 接口的说明
 * @param 	... 打开模式（oflag的O_CREAT标志所要求的附加参数）。ReWorks暂时不支持此选项。用户可直接设置为：S_IRWXU | S_IRWXG | S_IRWXO，即0777；
 * @param  	... 消息队列属性（oflag的O_CREAT标志所要求的附加参数）。消息队列属性为struct mq_attr类型，主要用于设置消息队列支持的最大消息数，
 * 			每条消息的最大长度、任务等待类型等属性。如果用户输入NULL，则ReWorks使用默认属性来设置
 * 			消息队列：按优先级等待、最多支持10条消息、每条消息长度为1024。这里需要注意调用接收消息接口时，输入的接收缓冲区长度不能小于消息长度，
 *			也就是说，如果使用默认属性，则接收缓冲区不能小于1024，否则接口将返回失败并设置错误号EMSGSIZE。
 * 
 * @return 	消息队列描述符	函数执行成功
 * @return （mqd_t）-1		函数执行失败，并设定 errno 指出错误
 *
 * @exception EEXIST  	同时设置了O_EXCL 和 O_CREAT，且指定的消息队列已存在  
 * @exception EINVAL  	name或者oflag取值不合法，或者在oflag中指定了O_CREAT，并且attr的值不为空的情况下，
 * 						mq_maxmsg或mq_msgsize小于等于0 ，或mq_waitqtype取值不合法   
 * @exception ENFILE  	系统中打开了过多的消息队列  
 * @exception EMFILE  	系统中打开了过多的消息队列描述符 
 * @exception ENAMETOOLONG 参数 name 的长度超过了NAME_MAX   
 * @exception ENOENT  	没有设置O_CREAT，且指定的消息队列不存在
 * @exception ENOMEM	内存不足
 * @exception ECALLEDINISR 该接口不能在中断上下文中调用
 * @exception EMNOTINITED  消息队列模块尚未初始化
 * 
 * @see mq_close()
 * @see mq_unlink()  
 * 
 * @attention 调用该接口创建新的消息队列时，除了name和oflag参数外，还需要另外两个参数：
 * 			mode和attr，mode是mode_t类型的参数变量，本系统中没有实际意义；attr为 mq_attr
 * 			类型的指针变量，用于指定消息队列的属性，如果为NULL，则创建默认属性的消息队列，
 * 			消息队列的缺省属性为{mq_waitqtype=PTHREAD_WAITQ_PRIO, mq_maxmsg=10, mq_msgsize=1024}
 * @attention oflag只能设置O_RDONLY、O_WRONLY或O_RDWR之一
 */
mqd_t mq_open(const char *name, int oflag, ...);

/**
 * @brief 关闭消息队列
 *
 * 该接口用于关闭一个命名的消息队列。该接口的功能与关闭一个已打开文件的close函数类似，
 *会解除消息队列描述符mqdes和消息队列间的关联关系，调用任务关闭消息队列后，
 *将不能再使用该消息队列描述符，但其消息队列并不会从系统中删除，除非已经有任务调用
 *mq_unlink() 删除了该消息队列，并且当前对该接口的调用所关闭的是最后一个对消息队列
 *mqdes的引用。
 *
 * @param	mqdes	消息队列描述符
 * 
 * @return 	0		函数执行成功
 * @return 	-1		函数执行失败，并设定 errno 指出错误。
 *
 * @exception EBADF  mqdes指定的消息队列描述符不合法  
 * @exception EINVAL mqdes指定的消息队列是未命名的消息队列
 * @exception ECALLEDINISR 该接口不能在中断上下文中调用
 * @exception EMNOTINITED  消息队列模块尚未初始化
 * 
 * @attention 该接口不能关闭由mq_create()创建的未命名消息队列。
 * 
 * @see mq_open()
 */
int mq_close(mqd_t mqdes);

/**
 * @brief 删除消息队列
 *
 * 该接口用于删除 name指定的消息队列。在成功调用 mq_unlink() 后，如果在没有设置oflag中的
 *O_CREAT标志的情况下以name调用 mq_open()，调用会失败。在消息队列没有真正删除前，以name
 *调用 mq_open()重新创建消息队列会返回新的消息队列描述符。如果在调用 mq_unlink() 时，
 *有一个或多个任务打开了消息队列，那么消息队列的删除操作会推迟，直到所有对该消息队列的引用都被关闭
 *(即最后一个 mq_close() 被调用)， mq_unlink() 调用会在删除消息队列名字后立即返回，不需要阻塞。
 *
 * @param	name	消息队列名
 *
 * @return 	0		函数执行成功
 * @return 	-1		函数执行失败，不会改变消息队列，设定 errno 指出错误。
 *
 * @exception EINVAL		name是无效指针
 * @exception ENAMETOOLONG 	变量 name 的长度大于 NAME_MAX 
 * @exception ENOENT 		指定的命名消息队列不存在  
 * @exception ECALLEDINISR 	该接口不能在中断上下文中调用
 * @exception EMNOTINITED  	消息队列模块尚未初始化
 * 
 * @see mq_open()
 * @see mq_close()
 */
int mq_unlink(const char *name);

/**
 * @brief 创建未命名的消息队列
 *
 * 该接口用于创建一个不指定名称的消息队列。该消息队列可容纳max_msgs条消息，每条消息的最大长度为msg_size。
 *
 * @param	mq_blockflag 消息队列阻塞标志，取值可以是0或O_NONBLOCK
 * @param	max_msgs 	消息队列中可存放的消息数，取值为0则使用默认值10
 * @param 	msg_size   	消息队列中的消息长度，取值为0则使用默认值1024
 * @param	waitq_type 	阻塞在消息队列上的任务的等待策略，取值包括PTHREAD_WAITQ_FIFO或PTHREAD_WAITQ_PRIO
 *
 * @return 	消息队列描述符	函数执行成功
 * @return 	（mqd_t）-1		函数执行失败
 *
 * @exception EINVAL  	max_msgs或msg_size小于0 ，或waitq_type取值不合法   
 * @exception EMFILE  	系统中打开了过多的消息队列描述符
 * @exception ENOMEM	内存不足
 * @exception ECALLEDINISR 该接口不能在中断上下文中调用
 * @exception EMNOTINITED  消息队列模块尚未初始化 
 * 
 * @see mq_delete()
 */
mqd_t mq_create( int mq_blockflag, int max_msgs, int msg_size, int waitq_type);

/**
 * @brief 删除未命名的消息队列
 *
 * 该接口用于删除 mq_create() 所创建的未命名消息队列。所有因调用 mq_send() 等待发送消息和调用 
 *mq_receive() 等待接收消息的任务的阻塞都会被解除，并返回错误。该函数调用后，mqdes
 *对应的消息队列描述符就不再指向一个有效的消息队列。若使用该接口删除由 mq_open()
 *接口创建的消息队列，会返回错误。
 *
 * @param	mqdes	消息队列描述符
 *
 * @return 	0		函数执行成功
 * @return 	-1		函数执行失败，不会改变消息队列，设定 errno 指出错误。
 *
 * @exception ENOENT 指定的消息队列不存在  
 * @exception EINVAL 所删除的消息队列不是由mq_create()创建的未命名消息队列
 * @exception ECALLEDINISR 该接口不能在中断上下文中调用
 * @exception EMNOTINITED  消息队列模块尚未初始化
 * 
 * @attention 若使用该接口删除由mq_open()接口创建的消息队列，会返回错误。
 * 
 * @see mq_create()
 */
int mq_delete(mqd_t mqdes);

/**
 * @brief 发送消息
 *
 * 该接口用来将 msg_ptr指向的消息添加到 mqdes指定的消息队列中。<br/>
 * 如果指定消息队列不是满的，该接口会把消息插入到 msg_prio指定的位置。msg_prio
 *值较大的消息会插入到msg_prio值较小的前面。如果 msg_prio相等，消息插入到其他msg_prio
 *相等的消息的后面。<br/>
 * 如果消息队列已满，且消息队列描述符mqdes中没有设置O_NONBLOCK标志，那么调用任务会阻塞，
 *直到消息队列空间可用或者调用被信号中断；如果设置了O_NONBLOCK 标志，调用任务就不会阻塞，
 *直接返回错误。如果有多个任务在等待发送，那么在消息队列空间可用时，等待的任务会按照消息队列的属性
 *mq_waitqtype的设置来解除任务的阻塞。如果mq_waitqtype设置为PTHREAD_WAITQ_PRIO，
 *则按照优先级策略解除任务的阻塞；如果mq_waitqtype设置为PTHREAD_WAITQ_FIFO，则按照FIFO
 *的策略解除任务的阻塞。
 *
 * @param	mqdes  	 消息队列描述符
 * @param	msg_ptr  存放消息的缓存地址
 * @param	msg_len  消息长度，其取值应小于等于消息队列的 msg_msgsize属性，否则调用会失败。
 * @param	msg_prio 消息的优先级，其取值应小于 MQ_PRIO_MAX
 *
 * @return 	0	函数执行成功
 * @return 	-1	函数执行失败，消息不会入队，设定 errno 指出错误。
 *
 * @exception EINVAL 	msg_ptr是无效指针，或者msg_prio的取值不合法
 * @exception EBADF 	mqdes 指定的消息队列描述符不合法，或者mqdes指定的消息队列是只读的  
 * @exception EMSGSIZE 	msg_len指定的消息长度 超过了消息队列的最大消息长度属性  
 * @exception EAGAIN 	消息队列设置了O_NONBLOCK标志，并且调用该接口时消息队列已满
 * @exception EINTR		调用任务被信号中断
 * @exception EOBJDELETED 因为消息队列被删除而中止阻塞
 * @exception ECALLEDINISR 	消息队列在没有设置O_NONBLOCK的情况下，该接口不能在中断上下文中调用
 * @exception EMNOTINITED  消息队列模块尚未初始化
 * 
 * @see mq_timedsend()
 * @see mq_receive()
 * 
 * @attention msg_prio的取值应小于 MQ_PRIO_MAX。
 */
int mq_send(mqd_t mqdes, const char *msg_ptr, size_t msg_len, unsigned int msg_prio);

/**
 * @brief 限时发送消息 
 *
 * 该接口的功能与 mq_send() 类似，用来向 mqdes 指定的消息队列中添加一个消息，但是如果指定消息队列已满，
 *并且消息队列描述符mqdes中没有设置O_NONBLOCK标志，那么在超时到期时会终止任务的阻塞。
 *如果消息队列设置了O_NONBLOCK 标志，该接口等价于 mq_send()。 <br/>
 * 当调用任务阻塞等待超过了abs_timeout指定的绝对时间，或者调用该接口时已经过了abs_timeout
 *指定的绝对时间，就会发生超时。如果接口调用时消息队列中有足够的空间使得消息可以立即添加到队列中，
 *那么该接口将不会发生超时错误， 并且不必检查参数abs_timeout的合法性。
 *
 * @param	mqdes  	  	消息队列描述符
 * @param	msg_ptr		存放消息的缓存地址 
 * @param	msg_len		消息长度
 * @param	msg_prio	消息的优先级
 * @param	abs_timeout 等待时间
 *
 * @return 	0		函数执行成功，mq_send() 和 mq_timedsend() 函数返回0。
 * @return 	-1		函数执行失败，消息不会入队，设定 errno 指出错误。
 *
 * @exception EINVAL 	msg_ptr是无效指针，或者msg_prio的取值不合法，或者在任务被阻塞的情况下
 * 						abs_timeout 指定的纳秒值小于0或大于等于1'000'000'000
 * @exception EBADF 	mqdes 指定的消息队列描述符不合法，或者mqdes指定的消息队列是只读的  
 * @exception ETIMEDOUT 消息队列打开时 没有设置 O_NONBLOCK 标志，并且在消息添加进消息队列之前到达了超时时间  
 * @exception EMSGSIZE 	msg_len指定的消息长度 超过了消息队列的最大消息长度属性  
 * @exception EAGAIN 	消息队列设置了O_NONBLOCK标志，并且调用该接口时消息队列已满
 * @exception EINTR		调用任务被信号中断
 * @exception EOBJDELETED 	因为消息队列被删除而中止阻塞
 * @exception ECALLEDINISR 	消息队列没有设置O_NONBLOCK，且消息队列已满的情况下，该接口不能在中断上下文中调用
 * @exception EMNOTINITED  	消息队列模块尚未初始化
 * 
 * @see mq_send()
 * @see mq_receive()
 * 
 * @attention 该接口的参数abs_timeout指定的是阻塞等待消息队列的一个绝对时间点，
 * 因此通过date()或clock_settime()接口修改日期/时间时会影响线程的等待时间，
 * 特别是将时间往前调整的情况，有可能会导致程序长时间得不到响应。
 */
int mq_timedsend(mqd_t mqdes, const char *msg_ptr, size_t msg_len, unsigned int msg_prio, const struct timespec *abs_timeout);

/**
 * @brief 接收消息
 *
 * 该接口用来会从mqdes指定的消息队列中接收最高优先级消息中最早的消息。如果由参数msg_len
 *指定的缓存区大小小于消息队列属性mq_msgsize，调用会失败并返回错误，否则，选定的消息会从队列中删除，
 *并复制到 msg_ptr 指向的缓存区中。<br/>
 * 如果指定的消息队列为空，且消息队列描述符mqdes没有设置O_NONBLOCK标志位， 调用该接口的任务会阻塞，
 *直到一个消息加入到队列中或接口调用被信号中断。如果有多个任务在等待接收消息，当消息到达时，
 *等待的任务会按照消息队列的属性 mq_waitqtype 的设置来解除任务的阻塞。mq_waitqtype的说明参见 
 *mq_send() 接口的说明。<br/>
 * 如果指定的消息队列为空，且消息队列描述符mqdes设置了O_NONBLOCK标志位，则返回错误。 
 *
 * @param	mqdes	  消息队列描述符
 * @param	msg_ptr	  存放消息的缓存地址 
 * @param	msg_len	  存放消息的缓存区的长度
 * @param	msg_prio  存放消息优先级的指针，如果msg_prio不为空，选定消息的优先级会保存在msg_prio引用的位置
 *
 * @return 	>0		函数执行成功，从队列中删除选定的消息，返回选定消息的字节长度
 * @return 	-1		函数执行失败，不删除消息，设定 errno 指出错误
 *
 * @exception EINVAL   参数msg_ptr是无效指针
 * @exception EBADF    mqdes 指定的消息队列描述符不合法，或者mqdes指定的消息队列是只写的   
 * @exception EMSGSIZE msg_len的值小于消息队列的消息大小属性
 * @exception EAGAIN   消息队列描述符mqdes中设置了O_NONBLOCK标志位，且接口调用时消息队列是空的
 * @exception EINTR	   接口调用被信号中断
 * @exception EOBJDELETED  因为消息队列被删除而中止阻塞
 * @exception ECALLEDINISR 该接口不能在中断上下文中调用
 * @exception EMNOTINITED  消息队列模块尚未初始化
 * 
 * @see mq_send()
 * @see mq_timedreceive()
 */
ssize_t mq_receive(mqd_t mqdes, char *msg_ptr, size_t msg_len, unsigned int *msg_prio);

/**
 * @brief 限时接收消息
 *
 * 该接口的功能与 mq_receive() 类似，从 mqdes 指定的消息队列中接收最高优先级消息最早的消息，
 *但是如果任务在通过 mq_open() 打开消息队列时没有设置O_NONBLOCK 标志，且队列中没有适合接收的消息，
 *那么经过指定时间后会终止等待消息。如果设置了O_NONBLOCK标志，那么该接口等同于 mq_receive()。<br/>
 *当调用任务阻塞等待超过了abs_timeout指定的绝对时间，或者调用该接口时已经过了abs_timeout指定的绝对时间，
 *就会发生超时。如果任务调用该接口时可以立即从消息队列中接收消息，就不会发生超时错误，
 *并且不检查参数abs_timeout的合法性。
 *
 * @param	mqdes	  消息队列描述符
 * @param	msg_ptr	  存放消息的缓存地址
 * @param	msg_len	  存放消息的缓存区的长度
 * @param	msg_prio  存放消息优先级的地址，如果msg_prio不为空，选定消息的优先级会保存在msg_prio引用的位置
 * @param	abs_timeout 等待时间
 *
 * @return 	>0	函数执行成功，从队列中删除选定的消息，并返回消息的字节长度
 * @return 	-1	函数执行失败，不删除消息，设定 errno 指出错误
 * 
 * @exception EINVAL 	msg_ptr是无效指针，或者任务被阻塞的情况下，参数 abs_timeout指定的纳秒值小于0或
 *                      大于等于1'000'000'000  
 * @exception EBADF 	mqdes 指定的消息队列描述符不合法，或者mqdes指定的消息队列是只写的   
 * @exception EMSGSIZE  msg_len的值小于消息队列的消息大小属性
 * @exception EAGAIN    消息队列描述符mqdes中设置了O_NONBLOCK标志位，且接口调用时消息队列是空的
 * @exception EINTR	    接口调用被信号中断
 * @exception EOBJDELETED  	因为消息队列被删除而中止阻塞
 * @exception ETIMEDOUT 	消息队列打开时没有设置标志 O_NONBLOCK，但在指定时间到达前没有消息可接收
 * @exception ECALLEDINISR 	该接口不能在中断上下文中调用
 * @exception EMNOTINITED  	消息队列模块尚未初始化
 * 
 * @see mq_send() 
 * @see mq_receive()
 * 
 * @attention 该接口的参数abs_timeout指定的是阻塞等待消息队列的一个绝对时间点，
 * 因此通过date()或clock_settime()接口修改日期/时间时会影响线程的等待时间，
 * 特别是将时间往前调整的情况，有可能会导致程序长时间得不到响应。
 */
int mq_timedreceive(mqd_t mqdes, char *msg_ptr, size_t msg_len, unsigned int *msg_prio, const struct timespec *abs_timeout);

/**
 * @brief 通知任务消息可用
 *
 * 该接口用来设置/取消异步事件通知。<br/>
 * 如果参数notification不为空，该接口会注册调用任务，当mqdes指定的消息队列为空且有消息到达该消息队列时，
 *调用任务会得到通知。在任何时刻，只能有一个任务可以注册指定消息队列的通知，
 *如果已有任务注册了指定消息队列的消息到达通知，那么之后对消息队列的注册将会失败。如果参数
 *notification 为空，且当前任务注册了指定的消息队列的通知，那么现有的注册会被撤销。<br/>
 * 当通知发送到注册任务时，任务的注册会被撤销，其他任务就可以来注册该消息队列了。
 *在有一个消息到达某个先前为空的队列 ，且已有一个任务注册了消息队列的通知时，只有在没有任务阻塞在
 *mq_receive() 中等待接收消息的前提下，通知才会发出。也就是说， mq_receive() 调用中的阻塞比任何通知的注册都优先，
 *到达的消息会先满足适当的 mq_receive() 的运行。
 *
 * @param	mqdes        消息队列描述符
 * @param	notification 指向通知的指针，如果notification不为空，则为调用任务注册通知；否则撤销现有注册
 *
 * @return 	0	函数执行成功
 * @return 	-1	函数执行失败，设定 errno 指出错误。
 *
 * @exception EBADF  mqdes 指定的消息队列描述符不合法  
 * @exception EBUSY  已经有任务通过消息队列注册了通知  
 * @exception ECALLEDINISR 该接口不能在中断上下文中调用 
 * @exception EMNOTINITED  消息队列模块尚未初始化
 *
 */
int mq_notify(mqd_t mqdes, const struct sigevent *notification);

/**
 * @brief 设置消息队列属性
 *
 * 该接口用于设置mqdes所指定的消息队列的相关属性。 该接口调用成功后，消息队列的属性对应的 
 *mq_attr 结构中定义的成员mq_flags会被设置为指定的值，而其他成员将被忽略，mq_flags可以被设置为
 *O_NONBLOCK或0。如果 omqstat 非空，该接口会把消息队列的当前属性(mq_flags、mq_maxmsg、mq_msgsize、
 *mq_waitqtype)和当前队列状态(mq_curmsgs)保存在 omqstat 引用的位置，这些属性值与在这一点调用
 *mq_getattr() 返回的值相同。
 *
 * @param	mqdes	消息队列描述符
 * @param	mqstat	指向要设置的消息队列属性的指针
 * @param	omqstat	存放消息队列属性的缓存地址，如果不为空，omqstat用来存放消息队列的当前属性和当前状态
 *
 * @return 	0	函数执行成功，把消息队列的属性改为指定设置
 * @return 	-1	函数执行失败，不改变消息队列属性，设定 errno 指出错误。
 * 
 * @exception EINVAL 参数 mqstat 是无效指针
 * @exception EBADF  mqdes 指定的消息队列描述符不合法，或者mqdes指定的消息队列是只读的 
 * @exception EMNOTINITED  消息队列模块尚未初始化
 * 
 * @see mq_open()
 * @see mq_getattr()
 * 
 * @attention 该接口只能设置mq_attr结构中的mq_flags属性，并且只能设置/清除其中的O_NONBLOCK标志位。
 */
int mq_setattr(mqd_t mqdes, const struct mq_attr *mqstat, struct mq_attr *omqstat);

/**
 * @brief 获得消息队列属性
 *
 * 该接口用来获取消息队列的属性和状态信息，结果会返回到参数 mqstat 引用的数据结构 mq_attr 中。
 *接口调用成功后，结构变量mqstat的成员 mq_flags被设置为打开消息队列时设置的值或调用 mq_setattr()
 *时修改的值；mq_maxmsg和 mq_msgsize被设置为打开消息队列时所设置的值； mq_curmsgs
 *则被设置为消息队列的当前状态，标示当前队列上的消息数。
 *
 * @param	mqdes	消息队列描述符 
 * @param	mqstat  存放消息队列属性的缓存地址
 *
 * @return 	0	函数执行成功
 * @return 	-1	函数执行失败，设定 errno 指出错误。
 *
 * @exception EINVAL 参数 mqstat 是空指针
 * @exception EBADF  mqdes 指定的消息队列描述符不合法
 * @exception EMNOTINITED  消息队列模块尚未初始化
 *
 * @see mq_open()
 * @see mq_setattr()
 */
int mq_getattr(mqd_t mqdes, struct mq_attr *mqstat);

/** @example example_mq_open.c
 * 下面的例子演示了如何使用mq_open().
 */
 
/** @example example_mq_close.c
 * 下面的例子演示了如何使用mq_close().
 */
/** @example example_mq_unlink.c
 * 下面的例子演示了如何使用mq_unlink().
 */
 
 /** @example example_mq_create.c
 * 下面的例子演示了如何使用mq_create().
 */
 
 /** @example example_mq_delete.c
 * 下面的例子演示了如何使用mq_delete().
 */
 
 /** @example example_mq_send.c
 * 下面的例子演示了如何使用mq_send().
 */
 
 /** @example example_mq_timedsend.c
 * 下面的例子演示了如何使用mq_timedsend().
 */

/** @example example_mq_receive.c
 * 下面的例子演示了如何使用mq_receive().
 */

/** @example example_mq_timedreceive.c
 * 下面的例子演示了如何使用mq_timedreceive().
 */
 
 /** @example example_mq_notify.c
 * 下面的例子演示了如何使用mq_notify().
 */ 

/** @example example_mq_getattr.c
 * 下面的例子演示了如何使用mq_getattr().
 */ 

/** @example example_mq_setattr.c
 * 下面的例子演示了如何使用mq_setattr().
 */


/* @}*/

#ifdef __cplusplus
}
#endif 

#endif 
