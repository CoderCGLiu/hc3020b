/*! @file cd_monitor.h
    @brief ReWorks系统监控头文件
     
 * 本文件定义了ReWorks系统监控的操作接口。
 * 
 * @version 4.7
 * 
 * @see 无
 */

#ifndef CD_MONITOR_H_
#define CD_MONITOR_H_

#ifdef __cplusplus
extern "C" 
{
#endif

/**
 * @{*/

/** 
 * @brief   获取任务创建互斥量的数量
 * 
 * @param 	thread 待获取的任务。
 * 
 * @return 	任务创建互斥量的数量
 * 			-1 表示获取失败。
 */
extern int pthread_get_created_mutex_num (pthread_t thread);

/** 
 * @brief   获取任务删除互斥量的数量
 * 
 * @param 	thread 待获取的任务。
 * 
 * @return 	任务删除互斥量的数量
 * 			-1 表示获取失败。
 */
extern int pthread_get_destroyed_mutex_num (pthread_t thread);

/** 
 * @brief   获取任务创建信号量的数量
 * 
 * @param 	thread 待获取的任务。
 * 
 * @return 	任务创建信号量的数量
 * 			-1 表示获取失败。
 */
extern int pthread_get_created_sem_num (pthread_t thread);

/** 
 * @brief   获取任务删除信号量的数量
 * 
 * @param 	thread 待获取的任务。
 * 
 * @return 	任务删除信号量的数量
 * 			-1 表示获取失败。
 */
extern int pthread_get_destroyed_sem_num (pthread_t thread);

/** 
 * @brief   获取任务创建消息队列的数量
 * 
 * @param 	thread 待获取的任务。
 * 
 * @return 	任务创建消息队列的数量
 * 			-1 表示获取失败。
 */
extern int pthread_get_created_mq_num (pthread_t thread);

/** 
 * @brief   获取任务删除消息队列的数量
 * 
 * @param 	thread 待获取的任务。
 * 
 * @return 	任务删除消息队列的数量
 * 			-1 表示获取失败。
 */
extern int pthread_get_destroyed_mq_num (pthread_t thread);

/* @}*/

#ifdef __cplusplus
}
#endif
#endif /* CD_MONITOR_H_ */

