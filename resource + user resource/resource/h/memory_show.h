/*! @file memory_show.h
    @brief ReWorks内存信息获取/显示头文件
    
 * 本文件定义了内存相关信息的获取/显示操作接口。
 *
 * @version 4.7
 * 
 * @see 无
 */

#ifndef _REWORKS_MEMORYSHOW_H_
#define _REWORKS_MEMORYSHOW_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <reworks/types.h>

/**  
 * @addtogroup group_memory
 * @{ */	


/*! \struct meminfo
 * \brief 内存使用信息结构定义
 *
 * 该数据结构定义了内存使用信息，包括核心堆内存和用户堆内存的使用量和总数。
 */
typedef struct meminfo 
{
	/* !!-- 全部修改为size_t --!! */
	size_t sysmemused;		//!< 核心堆内存使用量
	size_t totalsysmem;	//!< 核心堆内存总数
	size_t heapmemused;	//!< 用户堆内存使用量
	size_t totalheapmem;	//!< 用户堆内存总数
} meminfo_t; //!< 内存使用信息结构定义


struct pthread_meminfo
{
	u32 sigma_alloc_size; //!< 分配内存累计数
	u32 alloc_cnt; //!< 分配次数
	u32 sigma_free_size; //!< 释放内存累计数
	u32 free_cnt; //!< 释放次数
};

/**  
 * @brief    获取指定任务的内存使用信息
 * 
 * 该接口获取指定任务的内存使用信息，包括内存分配、释放的次数及累计数。 
 *
 * @param   thread 	指定的任务号
 * @param	info	指向任务内存使用信息的指针
 *
 * @return  0		函数执行成功，并将内存使用信息存放在info中
 * @return	-1		函数执行失败，设置errno指示错误信息
 *
 * @exception ESRCH	 找不到指定的任务
 *
 */
extern int pthread_get_meminfo(pthread_t thread, struct pthread_meminfo *info);



/**  
 * @brief    获取内存使用信息
 * 
 * 该接口获取内存的使用信息，包括内存类型、内存总量、已使用内存数、空闲内存数等。 
 *
 * @param   mi 	存放内存信息的指针
 *
 * @return  0	函数执行成功，并将内存信息存放在mi中
 * @return	-1	函数执行失败，设置errno指示错误信息
 *
 * @exception EINVAL 参数mi是无效指针
 * @exception EMNOTINITED 用户堆模块尚未初始化
 *
 */
int mem_getinfo(struct meminfo *mi);


/**  
 * @brief    显示内存使用信息
 * 
 * 该接口显示内存的部署信息和当前使用信息。<br/>
 * 内存部署信息包括内存类型、起始地址和大小，部署信息中显示的各个部分大小总和等于用户配置
 * 的系统内存大小(SYS_MEM_SIZE)，kernel的大小等于用户配置的核心内存大小(SYS_KERNEL_MEM_SIZE)，
 * 核心内存大小又分为两部分：internal use用于系统内部维护核心内存的部分；
 * alloc area表示用户实际可分配部分；<br/>
 * 内存当前使用信息主要是核心堆和用户堆的当前使用情况，具体信息包括内存类型、内存总量、
 * 已使用内存数、空闲内存数等。 
 * 
 * @param	无
 *
 * @return  无
 *
 * @exception 无
 *
 */

void mem_show(void);

/**  
 * @brief    显示内存使用信息
 * 
 * 该接口显示内存的部署信息和当前使用信息。<br/>
 * 内存部署信息包括内存类型、起始地址和大小，部署信息中显示的各个部分大小总和等于用户配置
 * 的系统内存大小(SYS_MEM_SIZE)，kernel的大小等于用户配置的核心内存大小(SYS_KERNEL_MEM_SIZE)，
 * 核心内存大小又分为两部分：internal use用于系统内部维护核心内存的部分；
 * alloc area表示用户实际可分配部分；<br/>
 * 内存当前使用信息主要是核心堆和用户堆的当前使用情况，具体信息包括内存类型、内存总量、
 * 已使用内存数、空闲内存数等。 
 * 
 * @param	无
 *
 * @return  无
 *
 * @exception 无
 *
 */
void mi(void);

/* @}*/

#ifdef __cplusplus
}
#endif 

#endif
