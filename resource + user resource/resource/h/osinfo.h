/*! @file osinfo.h
    @brief ReWorks 版本信息头文件
    
 *	本文件提供了ReWorks 版本信息定义
 *	
 * @version 4.7
 * 
 * @see 无
 */
#ifndef _REWORKS_INFORMATION_H_
#define _REWORKS_INFORMATION_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <reworks/types.h>
#include <reworks/list.h>

/**
 * @addtogroup group_osinfo
 * @{*/

/*! \struct module_info
 * \brief 模块信息结构定义
 *
 */
typedef struct module_info
{
	struct list_head 	node;			//!< 链接节点 
	char				*name;			//!< 模块名称 
	char 				*version;		//!< 版本 
	char				*subversion;	//!< 子版本 
	char				*builddate;		//!< 构建日期 
	char				*buildtime;		//!< 构建时间 
	char				*description;	//!< 模块描述 
}MOD_INFO; //!<  模块信息结构体

/*! \struct board_info
 * \brief 开发板信息结构定义
 *
 */
typedef struct board_info
{
	char				*board;			//!< 开发板 
	char				*cpu;			//!< 处理器 
	char 				*mem;			//!< 内存 
	char				*net;			//!< 网卡 
	char				*storage;		//!< 存储 
	char				*vedio;			//!< 显示 
	char				*audio;			//!< 音频 
	char				*serial;		//!< 串行 
	char				*usb;			//!< USB 
	char				*other;			//!< 其他 
}BOARD_INFO; //!<  开发板信息结构体

/*! \struct os_info
 * \brief 系统信息结构定义
 *
 */
typedef struct os_info
{
	char				*osname;		//!< 操作系统 
	char				*version;		//!< 版本 
	char				*subversion;	//!< 子版本 
	char				*releasetime;	//!< 发布日期 
	char				*builddate;		//!< 构建日期 
	char				*buildtime;		//!< 构建时间 
}OS_INFO; //!<  系统信息结构体

/** 
 * @brief  模块信息注册接口
 * 
 * @param 	info 待注册的模块信息指针
 * 
 * @return 	0 成功
 * @return 	-1失败
 * 			
 */
int register_module_info(MOD_INFO *info);

/** 
 * @brief  平台信息注册接口
 * 
 * @param 	info 待注册的平台信息指针
 * 
 * @return 	0 成功
 * @return 	-1失败
 * 			
 */
int register_board_info(BOARD_INFO *info);
/* @}*/

#ifdef __cplusplus
}
#endif

#endif
