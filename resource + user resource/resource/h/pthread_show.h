/*! @file pthread_show.h
    @brief ReWorks任务信息获取/显示头文件
    
 * 本文件定义了任务相关信息的获取/显示操作接口。
 *
* @version 4.7
* 
* @see 无
*/

#ifndef _REWORKS_PTHREADSHOW_H_
#define _REWORKS_PTHREADSHOW_H_

#ifdef __cplusplus
extern "C"
{
#endif
	
#include <reworks/thread.h>

/** 
 * @addtogroup group_os_reworks_pthread_show
 * @{ */
 
typedef Thread_Info pthread_info_t; //!< 任务控制块的相关信息

/**
 * @brief 获取指定任务的信息
 *
 * 该接口用于获取指定任务的信息。
 *
 * @param	thread	任务ID
 * @param 	pthread_info 存放任务信息的指针
 *
 * @return   0	函数执行成功 
 * @return 	EINVAL 	pthread_info是无效的指针
 * @return 	ESRCH	thread指定的任务不存在
 * 
 * @exception EINVAL 参数无效
 * @exception ESRCH  找不到指定的任务
 * 
 * @note 该接口也可适用于taskSpawn()创建的任务 
 */
int pthread_getinfo(pthread_t thread, pthread_info_t * pthread_info);


/**
 * @brief 显示任务信息
 * 
 * 该接口用于显示任务信息，参数level取值为0，显示的是指定任务的基本信息，包括任务名称、
 *任务执行体、任务ID、任务优先级、任务状态、任务pc寄存器的值、任务栈指针、任务超时时间；
 *如果参数 level 取值为1，显示的是指定任务的详细信息，包括任务的基本信息、任务的可选项
 *信息和任务的栈信息；如果level取值为2，则显示系统中所有任务的基本信息。
 *
 * @param	thread  任务ID
 * @param	level   信息显示级别，取值包括0(任务基本信息）, 1（任务详细信息）, 2（所有任务的基本信息）
 * 
 * @return	0	函数执行成功
 * @return	EINVAL 	参数level取值不合法
 * @return 	ESRCH	thread指定的任务不存在
 *
 * @exception EINVAL 参数无效   
 * @exception ESRCH	 找不到指定的任务
 * 
 * @note 该接口也可适用于taskSpawn()创建的任务 
 */
 int pthread_show(pthread_t thread, int level);
 
 /**
 * @brief 显示任务的栈信息
 * 
 * 该接口用于显示任务的栈信息，如果参数 thread 不为零，则显示指定任务的栈信息，否则将
 *显示系统中所有任务的栈信息。显示的任务栈信息包括任务的名称、任务执行函数、任务ID、
 *任务栈的基址、任务栈总的大小、任务栈当前已使用的数量、任务栈使用的峰值、任务栈的剩余情况。
 *
 * @param	thread  任务ID，thread取值为零时，则显示所有POSIX任务的栈信息
 * 
 * @return	无
 *   
 * @exception ESRCH	找不到指定的任务
 * 
 * @note 该接口也可适用于taskSpawn()创建的任务 
 */
void pthread_showstack(pthread_t thread);

/**
 * @brief 显示任务的栈回溯信息
 * 
 * 该接口用于显示任务的栈回溯信息，但不能用于显示调用任务自身的栈回溯信息。
 *
 * @param	thread  任务ID
 * 
 * @return	无
 *   
 * @exception 无
 * 
 * @note 该接口也可适用于taskSpawn()创建的任务 
 * 
 * @attention 该接口在回溯任务栈期间，如果被回溯任务运行导致栈的数据结构发生改变，可能会导致回溯过程或结果异常。
 */
 void pthread_showstackframe(pthread_t thread);
 
 /**
 * @brief 显示任务的上下文信息
 * 
 * 该接口用于显示指定任务的上下文信息。
 *
 * @param	thread  任务ID
 * 
 * @return	无
 *   
 * @exception ESRCH	找不到指定的任务
 * @exception ENOTSUP 系统不支持的操作
 * @exception ETYPENOTMATCH 任务类型不匹配
 * 
 * @see pthread_getregs()
 */
void pthread_showregs(pthread_t thread);
/**  
 * @brief    将任务状态以字符串形式输出
 * 
 * 该接口将指定任务的状态以字符串形式存储到pString指向的内存空间中 
 *
 * @param   thread	指定的任务id
 * @param   str  存储任务状态信息的指针
 *
 * @return  0	函数执行成功，并将任务状态存储到pString指向的内存空间中
 * @return	-1	函数执行失败
 *
 *@note	RUNNING表示任务处于运行状态。
 *@note	READY表示任务处于就绪状态。
 *@note	DORMANT表示任务处于僵死状态。
 *@note	DELAY表示任务处于睡眠状态。
 *@note	EVENT表示任务等待某事件发生。
 *@note	BLOCKED表示任务处于阻塞状态。
 *@note	BLOCKED+I表示任务处于阻塞且可被信号打断的状态。
 *@note	INTR表示任务处于等待信号状态。
 *@note	US表示使用操作系统用户接口(pthread_suspend()/taskSuspend())的挂起状态。
 *@note	DS表示任务调试代理的挂起状态。
 *@note	IS表示使用操作系统内部接口的临时挂起状态。
 *@note	如果任务同时处于多种状态则用"+"号连接各种状态。
 *@note	+C代表任务处于互斥量预防死锁协议引起的临时优先级提升状态。
 */
OS_STATUS pthread_get_status_str(pthread_t thread, char *str);

 /** @example example_pthread_getinfo.c
 * 下面的例子演示了如何使用pthread_getinfo().
 */ 
 
 /** @example example_pthread_show.c
 * 下面的例子演示了如何使用pthread_show().
 */

/** @example example_pthread_showstack.c
* 下面的例子演示了如何使用pthread_showstack().
*/
  /** @example example_pthread_showstackframe.c
 * 下面的例子演示了如何使用pthread_showstackframe().
 */ 
 
 /** @example example_pthread_showregs.c
 * 下面的例子演示了如何使用pthread_showregs().
 */

 /** @example example_pthread_get_status_str.c
 * 下面的例子演示了如何使用pthread_get_status_str().
 */
/* @}*/
#ifdef __cplusplus
}
#endif 

#endif
