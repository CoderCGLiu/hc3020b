/*! @file dma_memory.h
    @brief DMA内存分区管理头文件
    
 * 本文件定义了DMA内存分区相关的操作接口。
 * 
 * @version 4.7
 * 
 * @see 无
 */


#ifndef __DMA_MEMORY_H__
#define __DMA_MEMORY_H__

#ifdef __cplusplus
extern "C"
{
#endif 

#include <reworks/types.h>
#include <mpart.h>

#define __DMA_MEMORY_PREASSIGNED(name, size, alignment)	\
\
__attribute__((aligned(alignment))) char dma_mem_##name[size] __attribute__((__section__(".dma")));

extern int dma_mem_inited;

extern PART_ID  memDmaPartId;

/*! \struct _DMA_param_s
 * \brief DMA内存段信息结构定义
 *
 * 该数据结构定义了DMA内存段信息，描述了DMA内存中一块连续的内存区域的起始地址和结束地址。
 */
typedef struct _DMA_param_s
{
	void *dma_start;
	void *dma_end;
} DMA_param;


/**  
* @brief  添加DMA内存分区段。
* 
* @param    dma_start 	DMA内存分区的起始地址
* @param    dma_end 	DMA内存分区的结束地址
*
*/
void dma_mempart_add(void *dma_start, void *dma_end);

/**
 * @brief  根据添加的内存分区段，创建DMA内存分区对象池
 * 
 * @return    0		创建DMA内存分区成功
 * @return    -1	创建DMA内存分区失败
 * 
 */
int dma_mem_init(void);

/**  
* @brief    从DMA内存分区分配内存
* 
* 该接口在DMA内存分区中分配大小为size的内存。
*
* @param    size 	需要分配的内存长度
*
* @return    非0	成功分配内存
* @return    NULL	分配失败
*
* @exception EINVAL DMA内存分区不存在，或者size取值为0
* @exception ENOMEM 没有足够的内存可供分配
* @exception EMNOTINITED 内存分区模块尚未初始化
*
*/
void *malloc_dma(size_t size);

/**  
* @brief    从DMA内存分区分配内存,并将该内存内容初始化为0
* 
* 该接口在DMA内存分区中分配大小为size的内存。
*
* @param    nelem 	内存块个数
* @param    elsize 	内存块大小
*
* @return    非0	成功分配内存
* @return    NULL	分配失败
*
* @exception EINVAL DMA内存分区不存在，或者size取值为0
* @exception ENOMEM 没有足够的内存可供分配
* @exception EMNOTINITED 内存分区模块尚未初始化
*
*/
void *calloc_dma( size_t nelem, size_t elsize );

/**  
* @brief    从DMA内存分区分配边界对齐的内存
* 
* 该接口从DMA内存分区分配大小为size的缓存，它保证分配的缓存的开始地址按照边界对齐。
*
* @param    alignment 需要对齐的内存边界要求，其取值必须是【void *】类型长度的2的N次方倍数
* @param    size 	需要分配的内存长度
*
* @return    非0	成功分配内存
* @return    NULL	分配失败
*
* @exception EINVAL DMA内存分区不存在，或者alignment不是【void *】类型长度的2的N次方倍数，
* 					或者size取值为0
* @exception ENOMEM 没有足够的内存可供分配
* @exception EMNOTINITED 内存分区模块尚未初始化
*/
void *memalign_dma(size_t alignment, size_t size);


/**  
* @brief    从DMA内存分区重新分配内存
* 
* 该接口改变内存块的大小并返回一个指向新内存块的指针，新内存块中原有的内容保持不变。
*
* @param    addr 	需要重新分配的内存地址，如果addr为NULL，则该接口等同于mpart_alloc()。
* @param    size 	需要分配的内存长度 <br/>
*			若size值小于原先分配的内存长度，则内存内容不会改变，且返回原先的内存起始地址； <br/>
*			若size值大于原先分配的内存长度，则内存内容不会改变，但不一定返回原先的内存起始地址，并且新增加的内存未设置初始值； <br/>
*			若size为0，相当于调用mpart_free()。
*
* @return    非0	成功分配内存
* @return    NULL	分配失败
*
* @exception EINVAL DMA内存分区不存在，或者addr指定的内存无效
* @exception ENOMEM 没有足够的内存可供分配
* @exception EMNOTINITED 内存分区模块尚未初始化
*
* @attention 新内存块的对齐边界不能保证和原内存块相同。
*/
void *realloc_dma(char *addr, size_t size);

/**  
* @brief    向DMA内存分区释放内存
* 
* 该接口将之前通过 mpart_alloc(), mpart_memalign() 或 mpart_realloc() 所分配的内存块释放至内存分区的空闲内存列表中。
*
* @param    addr 需要释放的内存地址，若addr为空指针，则该接口直接返回
*
*
* @exception EINVAL	DMA内存分区不存在，或者addr指定的内存无效
* @exception EMNOTINITED 内存分区模块尚未初始化
*
*/
void free_dma(void *addr);


#ifdef __cplusplus
}
#endif 

#endif		// __DMA_MEMORY_H__
