﻿/*! @file exccode.h
    @brief ReWorks 异常信息编码定义
    
 *	本文件提供了ReWorks 异常信息编码定义
 *	
 * @version @reworks_version
 * 
 * @see 无
 */

#ifndef _EXCEPTION_CODE_H_
#define _EXCEPTION_CODE_H_

#ifdef __cplusplus
extern "C" {
#endif

/** @defgroup group_os_exception_code ReWorks 异常信息码详解
 */

/** @defgroup group_os_module_number 模块类别编码规则
* @ingroup group_os_exception_code
* 
* @{*/

#define M_CSP			(0 << 16)	//!<  CSP模块编码:		0x00000
#define M_REWORKS		(1 << 16)	//!<  核心模块编码:		0x10000
#define M_VX			(2 << 16)	//!<  VX兼容模块编码:	0x20000
#define M_BSP			(3 << 16)	//!<  BSP模块编码:		0x30000
#define M_IO			(4 << 16)	//!<  IO模块编码:		0x40000
#define M_DOSFS			(5 << 16)	//!<  DOSF模块编码:		0x50000
#define M_HRFS			(6 << 16)	//!<  HRFS模块编码:		0x60000
#define M_IPNET			(7 << 16)	//!<  IPNET模块编码:		0x70000
#define M_FTP			(8 << 16)	//!<  FTP模块编码:		0x80000
#define M_TELNET		(9 << 16)	//!<  TELNET模块编码:	0x90000
#define M_FLASHFX		(10 << 16)	//!<  FLASHFX模块编码:	0xa0000
#define M_DEBUG			(11 << 16)	//!<  调试模块编码:		0xb0000
#define M_SYMTBL		(12 << 16)	//!<  符号表模块编码:	0xc0000
#define M_SHELL			(13 << 16)	//!<  SHELL模块编码:		0xd0000

/* @}*/


/* REWORKS库错误分类码 */

#define S_REWORKS_ILLEGAL_RECURSIVE_KERNEL_LOCK		(M_REWORKS | 0 << 12)
#define S_REWORKS_MODULE_NOT_INIT				(M_REWORKS | 1 << 12)
#define S_REWORKS_KERNEL_TASK_NOT_SUPPORT				(M_REWORKS | 2 << 12)
#define S_REWORKS_MEMERY_CFG_ERROR				(M_REWORKS | 3 << 12)
#define S_REWORKS_PARAMETER_ERROR				(M_REWORKS | 4 << 12)

/* CSP体系结构分类码 */
#define S_CSP_ARM				(M_CSP | 1 << 12)
#define S_CSP_LOONGSON				(M_CSP | 2 << 12)
#define S_CSP_MCORE				(M_CSP | 3 << 12)
#define S_CSP_PENTIUM				(M_CSP | 4 << 12)
#define S_CSP_PPCE300_E600				(M_CSP | 5 << 12)
#define S_CSP_PPCE500V2_E500MC				(M_CSP | 6 << 12)
#define S_CSP_SHENWEI				(M_CSP | 7 << 12)
#define S_CSP_SPARC				(M_CSP | 8 << 12)

#define PRINT_EXCEPTION_CODE	"[EXCEPTION HELP CODE: 0x%x]"

/**
 * 非法递归获取内核锁
 */
#define PRINT_NOT_ISR_BUT_KERNEL_STATE_INFO() \
	do {	\
				printek(PRINT_EXCEPTION_CODE"Illegal recursive take kernel lock!\n", C_ILLEGAL_RECURSIVE_KERNEL_LOCK);	\
		}while(0);

/**
 * 检查模块是否已初始化 
 */

#define PRINT_MODULE_MOT_INIT_INFO(name, code)	\
	do {	\
			printk(PRINT_EXCEPTION_CODE"%s module has not been initialized!\n", code, name);	\
	}while(0);

/**
 * 内核任务不支持的操作 
 */
#define PRINT_KERNEL_TASK_NOT_SUPPORT(opt, code)	\
	do {	\
			printk(PRINT_EXCEPTION_CODE"The kernel task can not be %s !\n", code, opt);	\
	}while(0);


/**
 * 用户堆内存算法不支持的操作 
 */
#define PRINT_HEAP_MEM_ALGORITHM_NOT_SUPPORT()	\
	do {	\
			printk(PRINT_EXCEPTION_CODE"Heap memory algorithms is not support for %s !\n", C_HEAP_MEM_ALGORITHEM_NOT_SUPPORT, __FUNCTION__);	\
	}while(0);


/**
 * 核心内存算法不支持的操作 
 */
#define PRINT_KERNEL_MEM_ALGORITHM_NOT_SUPPORT()	\
	do {	\
			printk(PRINT_EXCEPTION_CODE"Kernel memory algorithms is not support for %s !\n", C_KERNEL_MEM_ALGORITHEM_NOT_SUPPORT, __FUNCTION__);	\
	}while(0);

/**
 * 预分配内存过大 
 */
#define PRINT_ALLOC_MEM_IS_OVERFLOW(size)	\
	do {	\
			printk(PRINT_EXCEPTION_CODE"Size of memory [size: 0x%x] is too large to be allocated!\n", C_KERNEL_MEM_IS_OVERFLOW, size);	\
	}while(0);

/** @defgroup group_os_help_0x00001 异常信息码0x00001
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_EXCEPTION_HANDLER_IS_OVERFLOW
 * @brief 异常信息码：0x00001
 * @todo 异常钩子注册数目已超过上限。
 * 		请取消异常钩子注册操作。
 * 
 */
#define C_EXCEPTION_HANDLER_IS_OVERFLOW			(M_CSP | 1 )
/* @}*/

/** @defgroup group_os_help_0x00002 异常信息码0x00002
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_EXCEPTION_HANDLER_CANNOT_FOUND
 * @brief 异常信息码：0x00002
 * @todo 申请注销的异常钩子未被注册。
 * 		请检查申请注销的异常钩子函数是否被成功注册。
 * 
 */
#define C_EXCEPTION_HANDLER_CANNOT_FOUND			(M_CSP | 2 )
/* @}*/

/** @defgroup group_os_help_0x02001 异常信息码0x02001
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_TLBMOD
 * @brief 异常信息码：0x02001
 * @details TLB修改异常。
 * 		当写内存操作的虚拟地址引用与TLB中某项匹配，但该项并没有被标注为”脏“，因此
 * 		该项不可写时，TLB修改例外发生。
 * 		发生该异常时，BadVAddr、Context、XContext和EntryHi寄存器保存了那条地址转换
 * 		失败的虚拟地址。如果引发异常的指令不是位于延迟槽内的指令，那么EPC寄存器保存了
 * 		该治理那个的地址，否则，EPC寄存器保存了之前的分支指令的地址，并且Cause寄存器
 * 		的BD位被置为1. 
 * 		
 */
#define C_TLBMOD			(S_CSP_LOONGSON | 1 )
/* @}*/

/** @defgroup group_os_help_0x02002 异常信息码0x02002
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_TLBL
 * @brief 异常信息码：0x02002
 * @details TLB读取操作无效异常。
 * 		当一个虚拟地址引用匹配到一项被标记的无效的TLB项（TLB有效位被清掉）时，TLB无效异常发生。 
 * 		发生该异常时，BadVAddr、Context、XContext和EntryHi寄存器保存了那条地址转换
 * 		失败的虚拟地址。如果引发异常的指令不是位于延迟槽内的指令，那么EPC寄存器保存了
 * 		该治理那个的地址，否则，EPC寄存器保存了之前的分支指令的地址，并且Cause寄存器
 * 		的BD为被置为1. 
 */
#define C_TLBL			(S_CSP_LOONGSON | 2 )
/* @}*/

/** @defgroup group_os_help_0x02003 异常信息码0x02003
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_TLBS
 * @brief 异常信息码：0x02003
 * @details TLB存储操作无效异常。
 * 		当一个虚拟地址引用匹配到一项被标记的无效的TLB项（TLB有效位被清掉）时，TLB无效异常发生。 
 * 		发生该异常时，BadVAddr、Context、XContext和EntryHi寄存器保存了那条地址转换
 * 		失败的虚拟地址。如果引发异常的指令不是位于延迟槽内的指令，那么EPC寄存器保存了
 * 		该治理那个的地址，否则，EPC寄存器保存了之前的分支指令的地址，并且Cause寄存器
 * 		的BD为被置为1. 
 */
#define C_TLBS			(S_CSP_LOONGSON | 3 )
/* @}*/

/** @defgroup group_os_help_0x02004 异常信息码0x02004
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_ADEL
 * @brief 异常信息码：0x02004
 * @details 读取地址操作异常。
 * 		当执行以下情况时，会发生地址错误异常：
 * 		（1）引用非法地址；
 * 		（2）取一个双字，但双字不对齐于双字边界；
 * 		（3）取一个字，但字不对齐与字的边界；
 * 		（4）取一个半字，但半字不对齐于半字的边界。
 * 		发生异常时，BadVAddr寄存器保存了没有正确对齐的虚地址。
 * 		 
 */
#define C_ADEL			(S_CSP_LOONGSON | 4 )
/* @}*/


/** @defgroup group_os_help_0x02005 异常信息码0x02005
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_ADES
 * @brief 异常信息码：0x02005
 * @details 存储地址操作异常。
 * 		当执行以下情况时，会发生地址错误异常：
 * 		（1）引用非法地址；
 * 		（2）存一个双字，但双字不对齐于双字边界；
 * 		（3）存一个字，但字不对齐与字的边界；
 * 		（4）存一个半字，但半字不对齐于半字的边界。
 * 		发生异常时，BadVAddr寄存器保存了没有正确对齐的虚地址。
 * 		 
 */
#define C_ADES			(S_CSP_LOONGSON | 5 )
/* @}*/

/** @defgroup group_os_help_0x02006 异常信息码0x02006
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_IBE
 * @brief 异常信息码：0x02006
 * @details 指令总线错误异常。
 * 		当处理器进行数据块的读取、更新或双字/单字/半字的请求时收到外部的ERR
 * 		完成应答信号，总线错误异常发生。
 * 		 
 */
#define C_IBE			(S_CSP_LOONGSON | 6 )
/* @}*/

/** @defgroup group_os_help_0x02007 异常信息码0x02007
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_DBE
 * @brief 异常信息码：0x02007
 * @details 数据总线错误异常。
 * 		当处理器进行数据块的读取、更新或双字/单字/半字的请求时收到外部的ERR
 * 		完成应答信号，总线错误异常发生。
 * 		 
 */
#define C_DBE			(S_CSP_LOONGSON | 7 )
/* @}*/

/** @defgroup group_os_help_0x02008 异常信息码0x02008
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_SYS
 * @brief 异常信息码：0x02008
 * @details 系统调用异常。
 * 		当执行SYSCALL指令的时候，系统调用异常发生，并且Cause寄存器的ExcCode字段
 * 		被置为SYS编码值。
 * 		如果SYSCALL指令没有在分支延迟槽中，则EPC寄存器保存这条指令的地址；否则，保存
 * 		之前的分支指令的地址。
 * 		如果SYSCALL指令在分支延迟槽中，则状态寄存器的BD位被置为1，否则该位被清0。
 */
#define C_SYS			(S_CSP_LOONGSON | 8 )
/* @}*/

/** @defgroup group_os_help_0x02009 异常信息码0x02009
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_BP
 * @brief 异常信息码：0x02009
 * @details 断点异常。
 * 		当执行一条断点指令时，发生断点异常，并且Cause寄存器的ExcCode字段被置为BP编码值。
 *		如果断点指令没有在分支延迟槽中，则EPC寄存器保存这条指令的地址；否则，保存
 * 		之前的分支指令的地址。
 * 		如果断点指令在分支延迟槽中，则状态寄存器的BD位被置为1，否则该位被清0。
 */
#define C_BP			(S_CSP_LOONGSON | 8 )
/* @}*/

/** @defgroup group_os_help_0x0200a 异常信息码0x0200a
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_RI
 * @brief 异常信息码：0x0200a
 * @details 保留指令异常。
 * 		当执行一条没有在MIPS64 Release2定义并且非龙芯自定义的指令时，保留指令异常发生。
 */
#define C_RI			(S_CSP_LOONGSON | 10 )
/* @}*/

/** @defgroup group_os_help_0x0200b 异常信息码0x0200b
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_CPU
 * @brief 异常信息码：0x0200b
 * @details 协处理器不可用异常。
 * 		试图执行以下任意一条协处理指令，将会导致协处理器不可用异常发生：
 * 		（1）相应的协处理器单元(CP1或CP2)没有被标记为可用
 * 		（2）CP0单元没有被标记为可用，并且进程性在用户或者超级用户的模式下的CP0指令。
 */
#define C_CPU			(S_CSP_LOONGSON | 11 )
/* @}*/

/** @defgroup group_os_help_0x0200c 异常信息码0x0200c
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_OV
 * @brief 异常信息码：0x0200c
 * @details 整型溢出异常。
 * 		当一条ADD、ADDI、SUB、DADD、DADDI或DSUB指令执行，导致结果的补码溢出时，整型溢出
 * 		例外发生，并且Cause寄存器的ExcCode字段被置为OV编码值。
 */
#define C_OV			(S_CSP_LOONGSON | 12 )
/* @}*/

/** @defgroup group_os_help_0x0200d 异常信息码0x0200d
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_TRAP
 * @brief 异常信息码：0x0200d
 * @details 陷阱异常。
 * 		当TGE、TGUE、TLT、TLTU、TEQ、TNE、TGEI、TGEUI、TLTI、TLTUI、TEQI、TNEI指令执行，条件结果为真时，
 * 		陷阱异常发生。
 */
#define C_TRAP			(S_CSP_LOONGSON | 13 )
/* @}*/

/** @defgroup group_os_help_0x0200f 异常信息码0x0200f
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_FPE
 * @brief 异常信息码：0x0200f
 * @details 浮点异常。
 * 		浮点协处理器使用浮点异常，浮点控制/状态寄存器的内容指示这个例外产生的原因。
 */
#define C_FPE			(S_CSP_LOONGSON | 15 )
/* @}*/

/** @defgroup group_os_help_0x02017 异常信息码0x02017
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_WATCH
 * @brief 异常信息码：0x02017
 * @details 观察点异常。
 * 		当取或存指令引用了在系统控制协处理器（CP0）的寄存器Watch所设定的虚地址的时候，
 * 		Watch异常发生。Watch寄存器的最低2位指定是读取或存储指令引发了这个异常。
 * 		Watch异常用于调试目的，通常异常处理程序把控制权转交给调试程序，并允许用户检查
 * 		当前状态。
 * 	
 */
#define C_WATCH			(S_CSP_LOONGSON | 23 )
/* @}*/


/** @defgroup group_os_help_0x10001 异常信息码0x10001
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_ILLEGAL_RECURSIVE_KERNEL_LOCK
 * @brief 异常信息码：0x10001
 * @todo 非法的内核锁嵌套，即已占有内核锁的CPU试图再次获取内核锁。
 * 		可根据栈回溯信息找到内核锁嵌套的位置，分析上下文，并查看是否存在CPU退出内核态但未释放内核锁的情况。
 * 
 */
#define C_ILLEGAL_RECURSIVE_KERNEL_LOCK			(S_REWORKS_ILLEGAL_RECURSIVE_KERNEL_LOCK | 1 )
/* @}*/

/** @defgroup group_os_help_0x10002 异常信息码0x10002
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_KERNEL_JOB_OVERFLOW
 * @brief 异常信息码：0x10002
 * @todo ReWorks操作系统内核任务工作队列溢出。
 * 		请检查系统中断处理函数是否被频繁调用或者阻塞或者死循环，导致内核任务没有机会运行。
 * 
 */
#define C_KERNEL_JOB_OVERFLOW			(S_REWORKS_ILLEGAL_RECURSIVE_KERNEL_LOCK | 2 )
/* @}*/

/** @defgroup group_os_help_0x10003 异常信息码0x10003
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_KERNEL_LOCK_RELEASED_NOT_BY_OWNER
 * @brief 异常信息码：0x10003
 * @todo 释放内核锁的CPU并非是该内核锁的拥有者。
 * 		请检查系统中是否存在不合理的释放内核锁操作。
 * 
 */
#define C_KERNEL_LOCK_RELEASED_NOT_BY_OWNER			(S_REWORKS_ILLEGAL_RECURSIVE_KERNEL_LOCK | 3 )
/* @}*/

/** @defgroup group_os_help_0x10004 异常信息码0x10004
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_SCHEDULE_ASSERTION_FAILED
 * @brief 异常信息码：0x10004
 * @todo ???
 * 
 */
#define C_SCHEDULE_ASSERTION_FAILED			(S_REWORKS_ILLEGAL_RECURSIVE_KERNEL_LOCK | 4 )
/* @}*/

/** @defgroup group_os_help_0x11001 异常信息码0x11001
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_PTHREAD_COND_VAR
 * @brief 异常信息码：0x11001
 * @todo 任务条件变量模块未初始化, 请尝试勾选资源配置->核心模块->任务条件变量模块解决该问题。
 * 
 */
#define C_PTHREAD_COND_VAR			(S_REWORKS_MODULE_NOT_INIT | 1 )
/* @}*/

/** @defgroup group_os_help_0x11002 异常信息码0x11002
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_MSGQ
 * @brief 异常信息码：0x11002
 * @todo 消息队列模块未初始化, 请尝试勾选资源配置->核心模块->消息队列模块解决该问题。
 * 
 */
#define C_MSGQ							(S_REWORKS_MODULE_NOT_INIT | 2 )
/* @}*/

/** @defgroup group_os_help_0x11003 异常信息码0x11003
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_EVENT
 * @brief 异常信息码：0x11003
 * @todo 事件模块未初始化, 请尝试勾选资源配置->核心模块->事件模块解决该问题。
 * 
 */
#define C_EVENT							(S_REWORKS_MODULE_NOT_INIT | 3 )
/* @}*/

/** @defgroup group_os_help_0x11004 异常信息码0x11004
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_HEAP_MEMORY
 * @brief 异常信息码：0x11004
 * @todo 用户堆模块未初始化, 请尝试勾选资源配置->核心模块->用户堆模块解决该问题。
 * 
 */
#define C_HEAP_MEMORY					(S_REWORKS_MODULE_NOT_INIT | 4 )
/* @}*/

/** @defgroup group_os_help_0x11005 异常信息码0x11005
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_MEMORY_PARTITION
 * @brief 异常信息码：0x11005
 * @todo 内存分区模块未初始化, 请尝试勾选资源配置->核心模块->内存分区模块解决该问题。
 * 
 */
#define C_MEMORY_PARTITION				(S_REWORKS_MODULE_NOT_INIT | 5 )
/* @}*/

/** @defgroup group_os_help_0x11006 异常信息码0x11006
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_MUTEX
 * @brief 异常信息码：0x11006
 * @todo 互斥量模块未初始化, 请尝试勾选资源配置->核心模块->互斥量模块解决该问题。
 * 
 */
#define C_MUTEX							(S_REWORKS_MODULE_NOT_INIT | 6 )
/* @}*/

/** @defgroup group_os_help_0x11007 异常信息码0x11007
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_PTHREAD_KEY
 * @brief 异常信息码：0x11007
 * @todo 任务私有数据模块未初始化, 请尝试勾选资源配置->核心模块->任务私有数据模块解决该问题。
 * 
 */
#define C_PTHREAD_KEY					(S_REWORKS_MODULE_NOT_INIT | 7 )
/* @}*/

/** @defgroup group_os_help_0x11008 异常信息码0x11008
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_PTHREAD_VAR
 * @brief 异常信息码：0x11008
 * @todo 任务变量模块未初始化, 请尝试勾选资源配置->核心模块->任务变量模块解决该问题。
 * 
 */
#define C_PTHREAD_VAR					(S_REWORKS_MODULE_NOT_INIT | 8 )
/* @}*/

/** @defgroup group_os_help_0x11009 异常信息码0x11009
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_RATEMON_TIMER
 * @brief 异常信息码：0x11009
 * @todo 单调速率周期模块未初始化, 请尝试勾选资源配置->核心模块->定时器模块解决该问题。
 * 
 */
#define C_RATEMON_TIMER					(S_REWORKS_MODULE_NOT_INIT | 9 )
/* @}*/

/** @defgroup group_os_help_0x1100a 异常信息码0x1100a
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_RWLOCK
 * @brief 异常信息码：0x1100a
 * @todo 任务读写锁模块未初始化, 请尝试勾选资源配置->核心模块->任务读写锁模块解决该问题。
 * 
 */
#define C_RWLOCK						(S_REWORKS_MODULE_NOT_INIT | 10 )
/* @}*/

/** @defgroup group_os_help_0x1100b 异常信息码0x1100b
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_SEMAPHORE
 * @brief 异常信息码：0x1100b
 * @todo 信号量模块未初始化, 请尝试勾选资源配置->核心模块->信号量模块解决该问题。
 * 
 */
#define C_SEMAPHORE						(S_REWORKS_MODULE_NOT_INIT | 11 )
/* @}*/

/** @defgroup group_os_help_0x1100c 异常信息码0x1100c
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_SIGNAL
 * @brief 异常信息码：0x1100c
 * @todo 信号模块未初始化, 请尝试勾选资源配置->核心模块->信号模块解决该问题。
 * 
 */
#define C_SIGNAL						(S_REWORKS_MODULE_NOT_INIT | 12 )
/* @}*/

/** @defgroup group_os_help_0x1100d 异常信息码0x1100d
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_SYS_MEMORY
 * @brief 异常信息码：0x1100d
 * @todo 核心内存模块未初始化, 请尝试勾选资源配置->核心模块->核心内存模块解决该问题。
 * 
 */
#define C_SYS_MEMORY					(S_REWORKS_MODULE_NOT_INIT | 13 )
/* @}*/

/** @defgroup group_os_help_0x1100e 异常信息码0x1100e
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_TIMER
 * @brief 异常信息码：0x1100e
 * @todo 定时器模块未初始化, 请尝试勾选资源配置->核心模块->定时器模块解决该问题。
 * 
 */
#define C_TIMER							(S_REWORKS_MODULE_NOT_INIT | 14 )
/* @}*/

/** @defgroup group_os_help_0x1100f 异常信息码0x1100f
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_WDG
 * @brief 异常信息码：0x1100f
 * @todo 看门狗模块未初始化, 请尝试勾选资源配置->核心模块->定时器模块解决该问题。
 * 
 */
#define C_WDG							(S_REWORKS_MODULE_NOT_INIT | 15 )
/* @}*/

/** @defgroup group_os_help_0x11010 异常信息码0x11010
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_BARRIER
 * @brief 异常信息码：0x11010
 * @todo 任务线程屏障模块未初始化, 请尝试勾选资源配置->核心模块->任务线程屏障模块解决该问题。
 * 
 */
#define C_BARRIER							(S_REWORKS_MODULE_NOT_INIT | 16 )
/* @}*/

/** @defgroup group_os_help_0x12001 异常信息码0x12001
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_KERNEL_TASK_NOT_SUPPORT_SET_PRIO
 * @brief 异常信息码：0x12001
 * @todo 内核任务不允许重置优先级，建议取消对内核任务改变优先级的操作。
 * 
 */
#define C_KERNEL_TASK_NOT_SUPPORT_SET_PRIO			(S_REWORKS_KERNEL_TASK_NOT_SUPPORT | 1 )
/* @}*/

/** @defgroup group_os_help_0x12002 异常信息码0x12002
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_KERNEL_TASK_NOT_SUPPORT_SUSPEND
 * @brief 异常信息码：0x12002
 * @todo 内核任务不允许被挂起，建议取消对内核任务挂起的操作。
 * 
 */
#define C_KERNEL_TASK_NOT_SUPPORT_SUSPEND			(S_REWORKS_KERNEL_TASK_NOT_SUPPORT | 2 )
/* @}*/

/** @defgroup group_os_help_0x12003 异常信息码0x12003
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_KERNEL_TASK_NOT_SUPPORT_RESUME
 * @brief 异常信息码：0x12003
 * @todo 内核任务不许与被挂起，当然也不允许被唤醒，建议取消对内核任务的唤醒操作。
 * 
 */
#define C_KERNEL_TASK_NOT_SUPPORT_RESUME			(S_REWORKS_KERNEL_TASK_NOT_SUPPORT | 3 )
/* @}*/

/** @defgroup group_os_help_0x13001 异常信息码0x13001
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_HEAP_MEM_ALGORITHEM_NOT_SUPPORT
 * @brief 异常信息码：0x13001
 * @todo 该接口不支持当前配置的用户堆内存算法，目前操作系统用户堆内存管理仅支持FIRST_FIT分配算法。
 * 		请检查资源配置参数文件usrArgs.h中的REWORKS_HEAP_ALGORITHM参数是否合法，1为FIRST_FIT算法。
 * 
 */
#define C_HEAP_MEM_ALGORITHEM_NOT_SUPPORT			(S_REWORKS_MEMERY_CFG_ERROR | 1 )
/* @}*/

/** @defgroup group_os_help_0x13002 异常信息码0x13002
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_KERNEL_MEM_ALGORITHEM_NOT_SUPPORT
 * @brief 异常信息码：0x13002
 * @todo 该接口不支持当前配置的核心内存算法，目前操作系统核心内存管理支持BUDDY算法和支持FIRST_FIT分配算法。
 * 		请检查资源配置参数文件usrArgs.h中的REWORKS_HEAP_ALGORITHM参数是否合法，0为BUDDY算法，1为FIRST_FIT算法。
 * 
 */
#define C_KERNEL_MEM_ALGORITHEM_NOT_SUPPORT			(S_REWORKS_MEMERY_CFG_ERROR | 2 )
/* @}*/

/** @defgroup group_os_help_0x13003 异常信息码0x13003
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_ALLOC_MEM_SIZE_IS_OVERFLOW
 * @brief 异常信息码：0x13003
 * @todo 预分配内存过大，超过了可用的内存大小。
 * 
 */
#define C_ALLOC_MEM_SIZE_IS_OVERFLOW			(S_REWORKS_MEMERY_CFG_ERROR | 3 )
/* @}*/

/** @defgroup group_os_help_0x13004 异常信息码0x13004
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_RECV_MEM_IS_OVERFLOW
 * @brief 异常信息码：0x13004
 * @todo 预留内存过大，超过了可用的用户堆内存大小。请适当的减小预留内存大小，保证该值在合理的范围内。
 * 
 */
#define C_RECV_MEM_IS_OVERFLOW			(S_REWORKS_MEMERY_CFG_ERROR | 4 )
/* @}*/

/** @defgroup group_os_help_0x13005 异常信息码0x13005
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_KERNEL_MEM_IS_OVERFLOW
 * @brief 异常信息码：0x13005
 * @todo 核心内存配置过大，超过了系统内存。
 * 		请检查内存配置模块中核心内存大小的值SYS_KERNEL_MEM_SIZE是否超过了内存总大小的值SYS_MEM_SIZE。
 * 
 */
#define C_KERNEL_MEM_IS_OVERFLOW			(S_REWORKS_MEMERY_CFG_ERROR | 5 )
/* @}*/

/** @defgroup group_os_help_0x13006 异常信息码0x13006
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_HEAP_MEM_ADDR_IS_OVERFLOW
 * @brief 异常信息码：0x13006
 * @todo 用户堆内存起始地址超过系统内存起始地址加主内存大小。该问题仅在龙芯平台出现。
 * 		请检查内存配置模块中核心内存大小的值SYS_KERNEL_MEM_SIZE是否超过了主内存大小。
 * 		目前龙芯平台的主内存大小最多支持256MB。
 * 
 */
#define C_HEAP_MEM_ADDR_IS_OVERFLOW			(S_REWORKS_MEMERY_CFG_ERROR | 6 )
/* @}*/

/** @defgroup group_os_help_0x13007 异常信息码0x13007
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_MAIN_MEMPART_NOT_FOUND
 * @brief 异常信息码：0x13007
 * @todo 未找到主内存。该问题仅在龙芯平台出现。
 * 		请检查内存配置模块中系统内存起始地址SYS_MEM_START_ADDRESS是否正确。
 * 
 */
#define C_MAIN_MEMPART_NOT_FOUND			(S_REWORKS_MEMERY_CFG_ERROR | 7 )
/* @}*/

/** @defgroup group_os_help_0x14001 异常信息码0x14001
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_INVALID_MEM_POINTER
 * @brief 异常信息码：0x14001
 * @todo 未在状态表中获取到指定内存地址被分配的信息 。
 * 		请检查输入内存地址指向的内存是否已被释放或还未分配内存。
 * 
 */
#define C_INVALID_MEM_POINTER			(S_REWORKS_PARAMETER_ERROR | 1 )
/* @}*/

/** @defgroup group_os_help_0x14002 异常信息码0x14002
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_INVALID_MSGQ_POINTER
 * @brief 异常信息码：0x14002
 * @todo 非法的消息队列对象 。
 * 		请检查输入的消息队列对象参数的合法性，即该消息队列是否被正常创建或者已经被销毁。
 * 
 */
#define C_INVALID_MSGQ_POINTER			(S_REWORKS_PARAMETER_ERROR | 2 )
/* @}*/

/** @defgroup group_os_help_0x14003 异常信息码0x14003
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_INVALID_THREAD_POINTER
 * @brief 异常信息码：0x14003
 * @todo 非法的任务对象 。
 * 		请检查输入的任务对象参数的合法性，即该任务是否被正常创建或者已经被销毁。
 * 
 */
#define C_INVALID_THREAD_POINTER			(S_REWORKS_PARAMETER_ERROR | 3 )
/* @}*/

/** @defgroup group_os_help_0x14004 异常信息码0x14004
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_INVALID_MUTEX_POINTER
 * @brief 异常信息码：0x14004
 * @todo 非法的互斥量对象 。
 * 		请检查输入的互斥量对象参数的合法性，即该互斥量是否被正常创建或者已经被销毁。
 * 
 */
#define C_INVALID_MUTEX_POINTER			(S_REWORKS_PARAMETER_ERROR | 4 )
/* @}*/

/** @defgroup group_os_help_0x14005 异常信息码0x14005
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_INVALID_SEMAPHORE_POINTER
 * @brief 异常信息码：0x14005
 * @todo 非法的信号量对象 。
 * 		请检查输入的信号量对象参数的合法性，即该信号量是否被正常创建或者已经被销毁。
 * 
 */
#define C_INVALID_SEMAPHORE_POINTER			(S_REWORKS_PARAMETER_ERROR | 5 )
/* @}*/

/** @defgroup group_os_help_0x14006 异常信息码0x14006
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_PARAMETER_IS_NULL
 * @brief 异常信息码：0x14006
 * @todo 输入参数为空。
 * 		请检查输入参数是否为空指针，若为空请为其分配内存。
 * 
 */
#define C_PARAMETER_IS_NULL			(S_REWORKS_PARAMETER_ERROR | 6 )
/* @}*/

/** @defgroup group_os_help_0x14007 异常信息码0x14007
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_PRIORITY_IS_INVALID
 * @brief 异常信息码：0x14007
 * @todo 创建任务时，用户指定的优先级参数不合法。
 * 		 ReWorks操作系统任务优先级范围是0~255。
 * 
 */
#define C_PRIORITY_IS_INVALID			(S_REWORKS_PARAMETER_ERROR | 7 )
/* @}*/

/** @defgroup group_os_help_0x14008 异常信息码0x14008
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_OPTION_IS_INVALID
 * @brief 异常信息码：0x14008
 * @todo 创建任务时，用户指定的选项参数不合法。
 * @see pthread_create2	 
 * 
 */
#define C_OPTION_IS_INVALID			(S_REWORKS_PARAMETER_ERROR | 8 )
/* @}*/

/** @defgroup group_os_help_0x14009 异常信息码0x14009
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_CLOCKID_IS_INVALID
 * @brief 异常信息码：0x14009
 * @todo 时钟标识参数clock_id不合法。
 * 		ReWorks操作系统中时钟标识仅支持CLOCK_REALTIME和CLOCK_MONOTONIC时钟。
 * @see clock_nanosleep	timer_gettime timer_settime
 * 
 */
#define C_CLOCKID_IS_INVALID			(S_REWORKS_PARAMETER_ERROR | 9 )
/* @}*/

/** @defgroup group_os_help_0x1400a 异常信息码0x1400a
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_DMA_SIZE_IS_ZERO
 * @brief 异常信息码：0x1400a
 * @todo DMA预留内存大小为零。
 * 		请确认系统板级支持包是否需要预留DMA内存，若不需要，则请勿勾选资源配置->核心模块->DMA管理模块。
 * 
 */
#define C_DMA_SIZE_IS_ZERO			(S_REWORKS_PARAMETER_ERROR | 10 )
/* @}*/

/** @defgroup group_os_help_0x1400b 异常信息码0x1400b
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_THREAD_NUM_IS_OVERFLOW
 * @brief 异常信息码：0x1040b
 * @todo 任务数量超过了上限。
 * 		显示信息的任务数目超过了最大值限制，目前系统只能显示最多128个任务 的信息
 * 
 */
#define C_THREAD_NUM_IS_OVERFLOW			(S_REWORKS_PARAMETER_ERROR | 11 )
/* @}*/

/** @defgroup group_os_help_0x1400c 异常信息码0x1400c
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_SAFE_COUNT_IS_OVERFLOW
 * @brief 异常信息码：0x1400c
 * @todo 保护当前任务不被删除的计数值溢出。
 * 		请检查系统中是否存在pthread_safe()与pthread_unsafe()未成对出现的情况。
 * 
 */
#define C_SAFE_COUNT_IS_OVERFLOW			(S_REWORKS_PARAMETER_ERROR | 12 )
/* @}*/

/** @defgroup group_os_help_0x1400d 异常信息码0x1400d
* @ingroup group_os_exception_code
* 
* @{*/
/** 
 * @def C_MUTEX_MAX_IS_SMALL
 * @brief 异常信息码：0x1400d
 * @todo 互斥量最大对象数小于已分配互斥量个数。
 * 		请检查资源配置->核心模块->互斥量模块中的参数REWORKS_MAX_MUTEXES值是否小于1024。
 * 
 */
#define C_MUTEX_MAX_IS_SMALL			(S_REWORKS_PARAMETER_ERROR | 13 )
/* @}*/

/* @}*/

#ifdef __cplusplus
}
#endif

#endif /* _EXCEPTION_CODE_H_ */
