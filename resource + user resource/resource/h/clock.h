/*! @file clock.h
    @brief ReWorks时钟和时间接口的头文件
     
 * 本文件定义了时钟和时间相关的操作接口。
 * 
 * @version 4.7
 * 
 * @see 无
 */

#ifndef _REWORKS_CLOCK_H_
#define _REWORKS_CLOCK_H_

#ifdef __cplusplus
extern "C"
{
#endif
	
#include <sys/times.h>
#include <reworks/types.h>
#include <sys/time.h>
/**
 * @addtogroup group_time
 * @{*/
 
#define TIMER_RELATIVE_C   0  //!< 指示时间是相对值，相对于当前时间
#define TIMER_ABSTIME	4  //!< 指示时间是绝对值
	
#define MICROSECONDS_PER_SECOND     ((unsigned int)1000000)	//!< 每秒时间对应的微秒数
#define NANOSECONDS_PER_SECOND      ((unsigned int)1000000000)//!< 每秒时间对应的纳秒数
#define NANOSECONDS_PER_MICROSECOND ((unsigned int)1000)	//!< 每微秒时间对应的纳秒数
	
/**
 * @brief 获取系统时钟滴答计数值。
 * 
 * 该接口返回系统时钟滴答计数器的当前计数值。该值在启动时被设置为零，伴随每次时钟中断
 *里调用clock_tick()而增加，也可通过tick_set()重新设置。
 * 
 * @param 	无
 * 
 * @return 	>=0	当前系统时钟滴答计数值。
 * 
 * @exception 无
 * 
 * @see tick_set
 */
u64 tick_get(void);

/**
 * @brief 设置系统时钟滴答计数值。
 * 
 * 该接口将系统时钟滴答计数器设置为指定的值。新的计数值可以通过tick_get()反馈得到。
 *设置操作不会改变任何任务的任何延迟或超时等待。
 * 
 * @param 	ticks	系统时钟滴答计数值
 * 
 * @return 	无
 * 
 * @exception 无
 * 
 * @see tick_get
 */
void tick_set(u64 ticks);

/**
 * @brief 获取系统时钟频率。
 * 
 * 该接口用于获取系统时钟频率，频率值为系统时钟每秒的时钟滴答数。
 * 
 * @param 	无
 * 
 * @return 	>=0	系统时钟频率
 * 
 * @exception 无
 * 
 * @see sys_clk_rate_set
 */
u32 sys_clk_rate_get(void);

/**
 * @brief 设置系统时钟频率。
 * 
 * 该接口通过调用板级支持包中设置时钟频率的接口完成对硬件定时器的设置。
 * 
 * @param 	ticks_per_second 要设置的时钟频率，即每秒钟的tick数。
 * 
 * @return 	0 	设置成功
 * @return 	-1 	设置失败
 * 
 * @exception 无
 * 
 * @see sys_clk_rate_get
 */
int sys_clk_rate_set(int ticks_per_second);

/**
 * @brief 打开系统时钟。
 * 
 * 该接口作用是打开系统时钟时根据系统配置参数设置系统时钟频率。该接口被板级支持包中
 * 时钟模块初始化函数clock_module_init调用。
 * 
 * @param 	无
 * 
 * @return 	无
 * 
 * @exception 无
 * 
 * @see clock_off
 * @see sys_clk_rate_set
 * 
 * @attention 该接口用于支持板级支持包，不作为应用编程接口提供。
 */
void clock_on(void);

/**
 * @brief 关闭系统时钟。
 * 
 * 该接口用于关闭系统时钟，使之不再发生中断。该接口会调用板级支持包中关闭时钟的接口
 *完成对硬件定时器的设置。
 * 
 * @param 	无
 * 
 * @return 	无
 * 
 * @exception 无
 * 
 * @see clock_on
 */
void clock_off(void);

/**
 * @brief 处理系统时钟一个滴答。
 * 
 * 该接口作用是系统时钟发生中断时进行更新相关的时间计数器，更新系统软件看门狗定时器
 *的状态，处理任务的时间片等操作。该接口被板级支持包中公共中断服务函数调用。
 * 
 * @param	无
 * 
 * @return 	无
 * 
 * @exception 无
 * 
 * @attention 该接口用于支持板级支持包，不作为应用编程接口提供。
 */
void clock_tick( void );

/**
 * @brief 将类型为struct timespec的时间值转换为系统时钟滴答数。
 * 
 * 该接口将类型为struct timespec的时间值转换为系统时钟滴答数。如果时间值转换中有零余，
 *将对转换后系统时钟滴答数做进1处理。
 * 	
 * @param 	tp 		指向时间值的指针
 * 
 * @return 	>=0		转换后系统时钟滴答数。
 * 
 * @exception 无
 * 
 * @see ticks_to_timespec
 */
u64 timespec_to_ticks(const struct timespec *tp);

/**
 * @brief 将系统时钟滴答数转换为类型为struct timespec的时间值。
 * 
 * 该接口将系统时钟滴答数转换为struct timespec类型的时间值。该接口根据系统时钟频率数
 *将系统时钟滴答数转换为秒数，其零余转换为纳秒数。
 * 
 * @param 	ticks 	系统时钟滴答数
 * @param 	time 	指向待保存的时间的指针
 * 
 * @return 	无
 * 
 * @exception 无
 * 
 * @see timespec_to_ticks
 */
void ticks_to_timespec(u32 ticks, struct timespec *time);

/**
 * @brief 计算两个类型为struct timespec的时间之间的差值。
 * 
 * 该接口用于计算类型为struct timespec的时间之间的差值。它将end指定的时间值减去start
 *指定的时间值，时间差存放在result中。
 * 
 * @param 	start 	起始时间
 * @param 	end 	结束时间
 * @param 	result 	时间差
 * 
 * @return 	无
 * 
 * @exception 无
 * 
 */
void timespec_subtract(const struct timespec *start,
		const struct timespec *end, 
		struct timespec *result);

/**
 * @brief 访问任务CPU时间时钟
 *
 * 该接口用于获取由pid指定任务CPU时间时钟的标识符。 
 *
 * @param 	pid 	  任务标识符，如果pid为0，该接口的返回值为当前任务CPU时间时钟的标识符
 * @param 	clock_id 时钟标识符
 *
 * @return 	0 		运行成功
 * @return 	EINVAL 	clock_id是无效指针
 * @return 	ESRCH 	找不到pid指定的任务
 * @return 	ENOTSUP 系统不支持该功能
 *
 * @exception EINVAL 参数无效
 * @exception ESRCH  找不到指定的任务
 * @exception ENOTSUP 系统不支持该功能
 * 
 * @note CPU时间时钟标识符仅支持CLOCK_THREAD_CPUTIME_ID
 */
extern int clock_getcpuclockid(pid_t pid, clockid_t *clock_id);  

/**
 * @brief 获取指定时钟的当前值
 *
 * 该接口用于获取指定时钟clock_id的当前值，并存放在参数tp指定的地址中。时钟可以是
 *系统范围的（即对所有任务可见），或是任务范围的（即度量的时间只对一个任务有意义）。 <br/>
 * 时钟CLOCK_REALTIME代表系统的实时时钟，对于这个时钟，clock_gettime()的返回值表示从
 *UTC时间公元1970年1月1日的0时0分0秒以来的时间总量（用秒和纳秒计算）。<br/>
 * 时钟CLOCK_MONOTONIC代表系统的单调时钟，对于这个时钟，clock_gettime()的返回值表示
 *系统自启动以来的时间总量（用秒和纳秒计算）。<br/>
 * 时钟CLOCK_THREAD_CPUTIME_ID/CLOCK_PROCESS_CPUTIME_ID代表任务的CPU时间时钟，对于
 *这个时钟，clock_gettime()的返回值表示任务执行时间的总量。
 *
 * @param 	clock_id 时钟标识符，其取值范围为[CLOCK_REALTIME, CLOCK_THREAD_CPUTIME_ID]
 * @param 	tp 		 时钟的当前值
 *
 * @return 	0 	调用成功
 * @return 	-1 	出错，并设置错误号指出错误
 * 
 * @exception EINVAL 参数tp为非法指针，或者参数clock_id的取值不合法
 *
 * @see clock_settime
 */
extern int clock_gettime(clockid_t clock_id, struct timespec *tp);     

/**
 * @brief 设置指定时钟的时间值
 *
 * 该接口会把clock_id指定的时钟设为tp指定的值。如果时间值是介于指定时钟精度的两个连续
 *非负整倍数间，那么它会被截取为较小的精度倍数。时钟可以是系统范围的（即对所有任务可见），
 *或是按任务的（度量的时间只对一个任务有意义）。<br/>
 * 时钟CLOCK_MONOTONIC代表系统的实时时钟，基于该时钟的时间值表示从UTC时间公元1970年1月1日
 *的0时0分0秒以来的时间总量。<br/>
 * 如果通过该接口来设置时钟CLOCK_REALTIME 的时间值，新的时间值会用来确定基于CLOCK_REALTIME
 *时钟的绝对时间服务的时间到期与否。这和启动了的绝对定时器的到期时间点相关。如果在这样
 *的时间服务调用时请求的绝对时间点位于新的时钟值之前，时间服务会立即到期，相当于时钟
 *正常到达了请求时间。<br/> 
 * 如果通过该接口来设定CLOCK_REALTIME时钟的值，不会影响阻塞在基于该时钟的相对时间服务
 *的任务，包括nanosleep()接口；也不会影响基于该时钟的相对定时器的到期时间。只有当经过
 *了被请求的相对时间间隔，这些时间服务程序才会到期，这一切都独立于新的或旧的时钟值。
 *不能通过该接口来设定CLOCK_MONOTONIC时钟的值。如果以CLOCK_MONOTONIC作为参数clock_id
 *调用它，这个接口会运行失败。<br/>
 * 时钟CLOCK_PROCESS_CPUTIME_ID/CLOCK_THREAD_CPUTIME_ID表示了调用任务的CPU时间时钟。
 *对于这些时钟，clock_settime()指定的值代表了任务执行时间总量。
 *
 * @param 	clock_id 时钟标识符
 * @param 	tp 		   时间值
 *
 * @return 	0 	调用成功
 * @return 	-1 	出错，并设置错误号指出错误
 * 
 * @exception EINVAL 参数clock_id的取值不合法，或者赋给变量tp的纳秒值小于0或大于等于1,000,000,000
 *
 * @attention 该接口不能用来设定CLOCK_MONOTONIC时钟的时间值。
 * @see clock_getres
 * @see clock_gettime
 */
extern int clock_settime(clockid_t clock_id, const struct timespec *tp);

/**
 * @brief 获取时钟精度
 *
 * 该接口用来获取clock_id 指定的时钟的精度。如果调用clock_settime()时设置的tp参数不是res的倍数，
 * 那么这个tp值会被截取为res的倍数。 
 *
 * @param 	clock_id	时钟标识符
 * @param 	res 		存放时钟精度的地址，如果不为空，指定时钟的精度会保存在res指向的位置
 *
 * @return 	0 	调用成功
 * @return 	-1 	出错，并设置错误号指出错误
 * 
 * @exception EINVAL 参数clock_id取值不合法，或者res是无效指针
 *
 * @see clock_gettime
 * @see clock_settime
 */
extern int clock_getres(clockid_t clock_id, struct timespec *res);    

/**
 * @brief 获取当前任务的CPU执行时间
 *
 * 该接口将填充buffer指向的struct tms结构，各字段计时单位为系统时钟滴答数。<br/>
 * 结构成员tms_utime是进程的用户态的CPU执行时间。（不使用）<br/>
 * 结构成员tms_stime是进程的内核态的CPU执行时间。（使用，对应为任务）<br/>
 * 结构成员tms_cutime是进程的已结束子进程的用户态的CPU执行时间。（不使用）<br/>
 * 结构成员tms_cstime是进程的已结束子进程的内核态的CPU执行时间。（不使用）<br/>
 *
 * @param	buffer	存放任务执行时间的地址
 *
 * @return 	(clock_t)>=0  CPU执行时间总和
 * @return 	(clock_t)-1        出错，并设置错误号指出错误。
 *
 * @exception EFAULT 参数buffer为非法指针
 *
 * @see clock
 */
extern clock_t times(struct tms *buffer);  

/**
 * @brief 报告当前任务使用的CPU执行时间
 *
 * 该接口用来获取自当前任务创建以来的CPU执行时间的累计值。
 * 
 * @param 	无
 *
 * @return 	(clock_t)>=0  任务执行时间，该时间值如果要转换为以秒为单位的值，需要除以CLOCKS_PER_SEC
 * @return 	(clock_t)-1	 CPU计时不可获取或者它的值不能被表达
 * 
 * @see times
 */
extern clock_t clock(void);

/**
 * @brief 当前任务休眠指定的时间（以秒为计时单位）
 *
 * 该接口将使调用的任务休眠，直到经过参数seconds指定的实时秒数，或者有信号发送到调用
 *任务且信号的动作是调用信号捕捉函数或终止任务。<br/>
 * 由于系统其他活动的调度，任务休眠的时间可能长于请求的时间。 
 *
 * @param 	seconds	任务休眠的秒数  
 *
 * @return 	0 		函数执行成功
 * @return 	>0 		任务被信号打断，返回值为请求时间与去实际休眠时间的差值（以秒计算）
 *
 * @exception 无
 *
 * @see usleep
 * @see nanosleep
 */
extern unsigned int sleep(unsigned int seconds);  

/**
 * @brief 当前任务休眠指定的时间（以微秒为计时单位）
 *
 * 该接口将使调用的任务休眠，直到经过参数seconds指定的实时秒数，或者有信号发送到调用
 *任务且信号的动作是调用信号捕捉函数或终止任务。<br/>
 * 由于系统其他活动的调度，任务休眠的时间可能长于请求的时间。 
 *
 * @param 	useconds	任务休眠的微秒数，应小于1,000,000。<br/>
 				如果useconds的值为0，任务调用该接口相当于主动把CPU让给同等优先级的任务运行
 *
 * @return 	0 	函数执行成功
 * @return 	-1 	出错，并设置错误号指出错误
 *
 * @exception EINVAL useconds的取值大于等于1,000,000
 * @exception EINTR	任务休眠被信号中断
 *
 * @see sleep
 * @see nanosleep
 * 
 */
extern int usleep(useconds_t useconds);


/**
 * @brief 高精度休眠
 *
 * 该接口将使调用的任务休眠，直到经过参数 rqtp指定的时间间隔。信号被发送给任务且信号
 *的动作是调用信号捕捉函数或终止任务，也会打断任务的休眠。<br/>
 * 由于参数值被舍入为休眠精度的整数倍，或是系统其他活动的调度，任务休眠的时间可能长于
 *请求的时间，除了被信号中断的情况。<br/>
 * 休眠时间是用系统时钟CLOCK_REALTIME来度量的。使用该接口不会对任何信号的行为或阻塞产生影响。
 *
 * @param 	rqtp 	请求的休眠时间
 * @param 	rmtp 	剩余时间如果
 * 参数rmtp不为空，它引用的时间值会被更新，使之包含休眠剩下时间量（即请求时间减去实际休眠时间）。
 * 如果参数rmtp为空，则不返回剩余时间。
 *
 * @return 	0 	经过了请求时间
 * @return 	-1 	被信号中断或者运行失败，并设置错误号指出错误
 * 
 * @exception EINTR  函数被一个信号中断
 * @exception EINVAL 参数rqtp指定的秒值小于0，或纳秒值小于0或大于等于1,000,000,000
 *
 * @see sleep
 * @see usleep
 */
extern int nanosleep(const struct timespec *rqtp, struct timespec *rmtp);  

/**
 * @brief 任务延迟
 *
 * 该接口使调用任务在指定的时间（以tick为单位）内放弃CPU。该接口通常被认为是手动设置重新调度，
 *但是，当等待一些和中断无关的外部条件时也有效。如果调用任务收到一个非阻塞或忽略的信号，
 *该接口会返回错误号EINTR。
 *
 * @param	ticks 任务延迟的tick数，该值为整型，如果ticks取值为零，则任务将主动把CPU让给同等优先级的任务运行。
 *              如果ticks取值为负数，则会默认转换成这个负数的补码
 * 
 * @return	0 		函数执行成功
 * @return	ECALLEDINISR 当前处于中断上下文中，不能执行延迟任务的操作
 * @return 	EINTR	任务收到一个非阻塞或忽略的信号
 *   
 * @exception ECALLEDINISR 接口不能在中断上下文中调用
 * @exception EINTR   在中断级别中调用或者任务收到一个非阻塞或忽略的信号
 * 
 * @note 该接口也可适用于taskSpawn()创建的任务   
 */
int pthread_delay(int ticks);

/**
 * @brief 指定时钟的高精度休眠
 *
 * 该接口用于使调用任务休眠，并以参数clock_id指定的时钟来度量时间。除非被信号中断，相对
 *clock_nanosleep()（指flags 设置为TIMER_RELATIVE_C）的休眠时间不会少于rqtp指定的时间，
 *绝对clock_nanosleep()（指设置了TIMER_ABSTIME标志）的休眠在相关时钟到达rqtp指定的绝对
 *时间点到达之前，将一直有效。<br/>
 * 由于参数值被舍入为休眠精度的整数倍，或是系统其他活动的调度，任务休眠的时间可能长于
 *请求的时间。
 * 
 * @param 	clock_id 	时钟标识符，取值范围为[CLOCK_REALTIME, CLOCK_THREAD_CPUTIME_ID]，
 * 						系统中该接口仅支持CLOCK_REALTIME和CLOCK_MONOTONIC时钟
 * @param 	flags 		定时器标志，取值包括TIMER_ABSTIME和TIMER_RELATIVE_C <br/>
 *					如果flags设置为TIMER_RELATIVE_C，该接口将使调用的任务休眠，直到经过参数
 * 					rqtp指定的时间间隔，或任务被信号中断。<br/>
 *    				如果flags设置为TIMER_ABSTIME，该接口将使调用的任务休眠，直到clock_id
 * 					指定的时钟到达了参数rqtp指定的绝对时间点，或者任务被信号中断。
 * @param 	rqtp 		指定休眠的时间，如果在调用接口时rqtp指定的时间值小于或等于
 					指定时钟当前的时间值，该接口会立即返回，且调用任务不会休眠。
 * @param 	rmtp 		存放剩余时间的指针，如果rmtp不为空，它引用的时间值会被更新，
 * 					使之包含休眠剩余时间量（即请求时间减去实际休眠时间）。如果参数
 *					rmtp为空，则不返回剩余时间。
 *
 * @return 	0 			函数执行成功
 * @return 	EINTR		任务被信号中断
 * @return 	EINVAL  参数clock_id或flags取值不合法，或者参数reqtp指定的秒值小于0，或纳秒值小于0或大于等于1,000,000,000
 * @return 	ENOTSUP 参数clock_id的取值为CLOCK_THREAD_CPUTIME_ID或CLOCK_PROCESS_CPUTIME_ID
 * 
 * @exception EINTR   发生信号中断
 * @exception EINVAL  参数无效
 * @exception ENOTSUP 系统不支持的操作
 *
 * @attention 系统不支持时钟clock_id的取值为CLOCK_THREAD_CPUTIME_ID和CLOCK_PROCESS_CPUTIME_ID的情况。
 *
 * @note 使用clock_nanosleep()接口不会对任何信号的行为或阻塞产生影响。
 *
 * @see nanosleep
 * @see clock_settime
 */
extern int clock_nanosleep(clockid_t clock_id, int flags, const struct timespec *rqtp, struct timespec *rmtp);

/**
 * @brief  获取系统时间
 *
 * 该接口会返回从公元1970年1月1日的UTC时间从0时0分0秒算起到现在所经过的秒数。
 *
 * @param 	tloc 	存储系统时间的指针，如果不为空，则系统时间将存放在tloc指向的地址中
 *
 * @return	 >0 	系统时间值 
 * @return 	(time_t)-1 运行失败
 *
 * @exception 无
 *
 * @see ctime
 * @see ftime
 * @see gettimeofday 
 */
extern time_t time(time_t *tloc);

/**
 * @brief 获取时间和日期
 * 
 * 该接口用于获取当前时间，并把时间表示为自从公元1970年1月1日的UTC时间从0时0分0秒
 * 算起到现在所经过的秒数和微秒数。该接口把所获取的时间值保存在tp所指的内存地址中。
 * ReWorks尚不支持时区特性，所以tzp不使用。
 * 
 * @param 	tp 	存放当前时间的地址空间
 * @param 	tzp 存放时区的地址空间
 * 
 * @return 	0 	函数执行成功
 * @return 	-1	函数执行失败，并设置错误号指出错误
 * 
 * @exception EINVAL 参数tp是无效指针
 * 
 * @see time
 * @see ctime
 * @see ftime
 */
extern int gettimeofday(struct timeval *tp, struct timezone *tzp); 

/** @example example_tick_get.c
 * 下面的例子演示了如何使用接口 tick_get( ).
 */

/** @example example_tick_set.c
 * 下面的例子演示了如何使用接口 tick_set( ).
 */

/** @example example_sys_clk_rate_get.c
 * 下面的例子演示了如何使用接口 sys_clk_rate_get( ).
 */

/** @example example_sys_clk_rate_set.c
 * 下面的例子演示了如何使用接口 sys_clk_rate_set( ).
 */

/** @example example_timespec_to_ticks.c
 * 下面的例子演示了如何使用接口 timespec_to_ticks( ).
 */

/** @example example_ticks_to_timespec.c
 * 下面的例子演示了如何使用接口 ticks_to_timespec( ).
 */

/** @example example_timespec_subtract.c
 * 下面的例子演示了如何使用接口 timespec_subtract( ).
 */

/** @example example_clock_getcpuclockid.c
 * 下面的例子演示了如何使用接口 clock_getcpuclockid( ).
 */

/** @example example_clock_gettime.c
 * 下面的例子演示了如何使用接口 clock_gettime( ).
 */

/** @example example_clock_settime.c
 * 下面的例子演示了如何使用接口 clock_settime( ).
 */

/** @example example_clock_getres.c
 * 下面的例子演示了如何使用接口 clock_getres( ).
 */

/** @example example_times.c
 * 下面的例子演示了如何使用接口 times( ).
 */

/** @example example_clock.c
 * 下面的例子演示了如何使用接口 clock( ).
 */

/** @example example_sleep.c
 * 下面的例子演示了如何使用接口 sleep( ).
 */

/** @example example_usleep.c
 * 下面的例子演示了如何使用接口 usleep( ).
 */

/** @example example_nanosleep.c
 * 下面的例子演示了如何使用接口 nanosleep( ).
 */

/** @example example_clock_nanosleep.c
 * 下面的例子演示了如何使用接口 clock_nanosleep( ).
 */

/** @example example_time.c
 * 下面的例子演示了如何使用接口 time( ).
 */

/** @example example_gettimeofday.c
 * 下面的例子演示了如何使用接口 gettimeofday( ).
 */

/* @}*/

#ifdef __cplusplus
}
#endif

#endif
