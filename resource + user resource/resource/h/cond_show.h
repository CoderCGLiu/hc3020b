/*! @file cond_show.h
    @brief ReWorks条件变量信息获取/显示头文件
    
 * 本文件定义了条件变量对象相关信息的获取/显示操作接口。
 *
 * @version 4.7
 * 
 * @see 无
 */
#ifndef _REWORKS_CONDSHOW_H_
#define _REWORKS_CONDSHOW_H_

#ifdef __cplusplus
extern "C"
{
#endif
	
#include <sys/types.h>

/**  
 * @defgroup group_pthread_cond ReWorks任务条件变量显示
 * @ingroup group_os_reworks_task
 * @{ */	
 
/*! \struct pthread_cond_info
 * \brief 条件变量信息结构定义
 *
 * 该数据结构定义了条件变量的相关信息，包括条件便利的ID、任务共享属性、相关联的互斥量、当前等待的任务数。
 */
typedef struct pthread_cond_info
{
	pthread_cond_t id; //!< 条件变量ID
	int process_shared; //!< 条件变量对象的任务共享属性
	int wait_count; //!< 等待条件变量对象的任务数目
	pthread_mutex_t mutex; //!< 条件变量对象对应的互斥量
	clockid_t clock_id; //!< 条件变量对象对应的计时时钟
}pthread_cond_info_t; //!< 条件变量信息结构定义

/** 
 * @brief 显示指定条件变量的信息。
 * 
 * 该接口用于显示指定的条件变量的相关信息。
 * 
 * @param 	cond	 	指向条件变量对象的指针
 * @param 	level		显示条件变量信息的详细程度，0为显示概要信息，1为显示条件变量详细信息
 * 
 * @return 	0		函数执行成功
 * @return 	EINVAL 	cond为无效指针，或者level取值不合法，或者cond指定的条件变量不存在
 * @return 	EMNOTINITED 条件变量模块尚未初始化
 * 
 * @exception EINVAL 参数无效
 * @exception EMNOTINITED 模块尚未初始化
 */
int pthread_cond_show(pthread_cond_t *cond, int level);

/** 
 * @brief 获取指定条件变量的概要信息。
 * 
 * 该接口用于获取指定的条件变量的概要信息，包括任务共享属性、等待任务数目、与之关联的互斥量等信息。
 * 
 * @param 	cond	  指向条件变量对象的指针
 * @param 	cond_info 存放条件变量信息的指针
 * 
 * @return 	0 		函数执行成功
 * @return 	EINVAL 	参数cond或cond_info为无效指针，或者cond指定的条件变量不存在
 * @return 	EMNOTINITED 条件变量模块尚未初始化
 * 
 * @exception EINVAL 参数无效
 * @exception EMNOTINITED 模块尚未初始化
 */
int pthread_cond_getinfo(pthread_cond_t *cond, pthread_cond_info_t *cond_info);

/** @example example_pthread_cond_show.c
 * 下面的例子演示了如何使用 pthread_cond_show().
 */
 
 /** @example example_pthread_cond_getinfo.c
 * 下面的例子演示了如何使用 pthread_cond_getinfo().
 */ 

/* @}*/

#ifdef __cplusplus
}
#endif 

#endif
