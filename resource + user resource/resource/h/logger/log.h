/*** rework log 功能用法
 * 1、调用初始化接口：
 *   	#include <logger/log.h>
 *   	void log_reworks_init(int options, int priority, int out_schema);
 *  	 	 optinos      log模块初始化 选项(多选用按位或);
 *  	 	 priority     log模块输出优先级选项，默认为 LOG_LEVEL_OFF
 *   		 out_schema   log信息输出模式： STDOUT_SCHEMA(标准输出)和 KLOG_SCHEMA(内核日志模式)
 *   
 * 2、 用户可自定义log模块：
 *   在应用程序里 通过以下宏添加log信息：
 *  	#include <logger/log.h>
 *   	LOG_MSG_FATAL(logger_app_xx, msg, ...), 
 *		LOG_MSG_ERROR(logger_app_xx, msg, ...), 
 *		LOG_MSG_WARN(logger_app_xx, msg, ...),  
 *		LOG_MSG_INFO(logger_app_xx, msg, ...),   
 *		LOG_MSG_DEBUG(logger_app_xx, msg, ...),  
 *		LOG_MSG_TRACE(logger_app_xx, msg, ...); 
 *   	其中logger_app_xx为用户自定义的细分模块。
 *   若需要输出信息，调用以下程序初始化。
 *   	#include <logger/log.h>
 *   	struct logger *logger_app_xx;
 *   	int logger_app_module_init(struct logger *logger_app_xx,int priority, int out_schema);
 *  	 	 logger_app_xx   用户自定义的logger;
 *  	 	 priority        log模块输出优先级选项，默认为 LOG_LEVEL_OFF
 *   		 out_schema      log信息输出模式： STDOUT_SCHEMA(标准输出)和 KLOG_SCHEMA(内核日志模式)     
 */
#ifndef _LOGGER_LOG_H_
#define _LOGGER_LOG_H_
#include <stdarg.h>

#define  LOG_USING_REWORKS

/*log信息输出模式： STDOUT_SCHEMA(标准输出)和 KLOG_SCHEMA(内核日志模式)*/
#define  STDOUT_SCHEMA    0
#define  KLOG_SCHEMA      1

/*log模块初始化 选项(多选用按位或)*/
#define LOG_KERNEL  0x0001
#define LOG_ISR     0x0002
#define LOG_SCHED   0x0004
#define LOG_MUTEX   0x0008
#define LOG_MMU     0x0010
#define LOG_CSP     0x0020
#define LOG_BSP     0x0040
#define LOG_IO      0x0080
#define LOG_FS      0x0100
#define LOG_IPNET   0x0200
#define LOG_ALL     0x03ff
#define LOG_INDEX   10     /*若log模块细分化,该值需做相应调整 */

/*log模块输出优先级选项，默认为 LOG_LEVEL_OFF*/
#define LOG_LEVEL_OFF   (0)
#define LOG_LEVEL_FATAL (100)
#define LOG_LEVEL_ERROR (200)
#define LOG_LEVEL_WARN  (300)
#define LOG_LEVEL_INFO  (400)
#define LOG_LEVEL_DEBUG (500)
#define LOG_LEVEL_TRACE (600)
#define LOG_LEVEL_ALL   (1000)

struct log_level {
    int int_level;
    char *name;
};

struct logger {
    char *name;
    int  priority;
    void (*log_message) (struct logger *logger, char *func, int line, struct log_level *level, char *message, va_list va);
};

struct logger_context {
    struct logger *(*stdout_logger) (char *name);
    struct logger *(*klog_logger) (char *name);
};

extern int logger_app_module_init(struct logger *logger_app,int priority, int out_schema);
extern int logger_module_init(int options, int priority, int out_schema );
extern struct logger *logmanager_get_logger(char *name,int out_schema);
extern void logger_log_message(struct logger *logger, char *func, int line, struct log_level *level, char *message, ...);
extern int logger_is_priority_enabled(const struct logger *logger,int priority);
extern int logger_set_priority(struct logger *logger,struct log_level *level);
extern void klog_log(struct logger *logger, char *func, int line, struct log_level *log_level, char* msg, va_list ap);

#if defined(LOG_USING_REWORKS) 

#else
#define LOG_MSG_FATAL(logger, msg, ...) 
#define LOG_MSG_ERROR(logger, msg, ...)  
#define LOG_MSG_WARN(logger, msg, ...)  
#define LOG_MSG_INFO(logger, msg, ...)   
#define LOG_MSG_DEBUG(logger, msg, ...)  
#define LOG_MSG_TRACE(logger, msg, ...)  

#define LOG_MSG(level, logger, msg, ...)
#endif

#define LOG_ENTRY(logger)
#define LOG_EXIT(logger)

#endif /* _LOGGER_LOG_H_ */
