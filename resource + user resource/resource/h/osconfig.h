/*! @file osconfig.h
    @brief ReWorks BSP配置头文件
    
 *	本文件提供了ReWorks BSP配置使用变量。	
 *	
 * @version 4.7
 * 
 * @see 无
 */
#ifndef _REWORKS_CONFIG_H_
#define _REWORKS_CONFIG_H_

#ifdef __cplusplus
extern "C"
{
#endif 
	
#include <reworks/types.h>
#include <reworks/thread.h>
#include <memory.h>

#ifdef CONFIGURE_INIT

/**
 * 定义是否诊断
 */
#ifndef DIAGNOSE_REWORKS	
int diagnose_reworks = 0;
#else 
int diagnose_reworks = 1;
#endif 

/*******************************************************************************
 * 标准设备 
 * */
/* 标准输入设备 */
#ifdef STD_INPUT_DEVICE_NAME
char *std_input_device_name = STD_INPUT_DEVICE_NAME;
#else
char *std_input_device_name = NULL;
#endif
/* 标准输出设备*/
#ifdef STD_OUTPUT_DEVICE_NAME
char *std_output_device_name = STD_OUTPUT_DEVICE_NAME;
#else
char *std_output_device_name = NULL;
#endif

/*******************************************************************************
 * 
 * 控制台配置
 */
/* 输入设备 */
#ifdef CONSOLE_INPUT_DEVICE_NAME
char *console_input_device_name = CONSOLE_INPUT_DEVICE_NAME;
#else
char *console_input_device_name = NULL;
#endif
/* 输出设备 */
#ifdef CONSOLE_OUTPUT_DEVICE_NAME
char *console_output_device_name = CONSOLE_OUTPUT_DEVICE_NAME;
#else
char *console_output_device_name = NULL;
#endif

#ifndef SYS_CLOCK_FREQ
#	define SYS_CLOCK_FREQ 			1193180 
#endif

/* 系统配置变量 */
extern sys_cfg_tbl sys_cfg; //!< 系统配置变量 

/**
 * 系统配置变量的初始值
 */
sys_cfg_tbl sys_cfg = 
{
	SYS_MEM_START_ADDRESS,				//!< 内存起始地址
    SYS_KERNEL_MEM_SIZE << 20,			//!< 系统内存大小
    1000000 / SYS_TICKS_PER_SEC,		//!< 每秒tick数
    SYS_TICKS_PER_TIMESLICE,			//!< 每个时间片的tick数
    SYS_MEM_SIZE << 20,					//!< 系统内存大小
    SYS_CLOCK_FREQ						//!< 系统时钟频率
};

u32 sv_evt_class;

#ifdef INCLUDE_CORE_DUMP
u32 core_dump_reserve_memsize = RESERVE_MEM_SIZE << 20; //!< core dump 保留内存
#else
u32 core_dump_reserve_memsize = 0;
#endif
/*shell用户登录的配置参数*/
#ifdef INCLUDE_SHELL
char *shell_username 		= SHELL_USERNAME;  //!< SHELL登录用户名
char *shell_password 		= SHELL_PASSWORD;  //!< SHELL登录用户密码
#endif
/*******************************************************************************
 * 
 * 网络服务配置
 */
/* ftp服务的配置参数 */
#ifdef INCLUDE_FTP
char *ftp_username 		= FTP_USERNAME;  //!< FTP用户名
char *ftp_password 		= FTP_PASSWORD;  //!< FTP用户密码
int   ftp_priority		= FTP_PRIORITY;  //!< FTP服务主线程的优先级
#endif

/* telnet服务的配置参数 */
#ifdef INCLUDE_TELNET
char *telnet_username 	= TELNET_USERNAME;  //!< Telnet用户名
char *telnet_password 	= TELNET_PASSWORD;  //!< Telnet用户密码
int   telnet_priority 	= TELNET_PRIORITY;  //!< Telnet主线程的优先级
#else
char *telnet_username 	= "";  //!< Telnet用户名
char *telnet_password 	= "";  //!< Telnet用户密码
#endif


/*******************************************************************************
 * 
 * 图形系统配置
 */
/* 键盘设备名称 */
#ifdef GRAPHICS_KBD_DEVICE_NAME
char *graphic_kbd_device_name = GRAPHICS_KBD_DEVICE_NAME;
#else
char *graphic_kbd_device_name = NULL;
#endif
#ifdef GRAPHICS_KBD_DEVICE_TYPE
int graphic_kbd_device_type = GRAPHICS_KBD_DEVICE_TYPE;		/* USB */
#else
int graphic_kbd_device_type = 0;		/* PS2 */
#endif

/* 鼠标设备名称 */
#ifdef GRAPHICS_MSE_DEVICE_NAME
char *graphic_mse_device_name = GRAPHICS_MSE_DEVICE_NAME;
#else
char *graphic_mse_device_name = NULL;
#endif
#ifdef GRAPHICS_MSE_DEVICE_TYPE
int graphic_mse_device_type = GRAPHICS_MSE_DEVICE_TYPE;	/* USB */
#else
int graphic_mse_device_type = 0;		/* PS2 */
#endif

/* 触摸屏设备名称 */
#ifdef GRAPHICS_TOUCH_SCREEN_DEVICE_NAME
char *graphic_ts_device_name = GRAPHICS_TOUCH_SCREEN_DEVICE_NAME;
#else
char *graphic_ts_device_name = NULL;
#endif


/* FrameBuffer设备名称 */
#ifdef FB_DEVICE_NAME
char *fb_device_name = FB_DEVICE_NAME;
#else
char *fb_device_name = NULL;
#endif

/* FrameBuffer分辨率 */
#ifdef FB_DEVICE_RESOLVE
char *fb_device_resolve = FB_DEVICE_RESOLVE;
#else
char *fb_device_resolve = NULL;
#endif

/* FrameBuffer色深*/
#ifdef FB_DEVICE_DEPTH
unsigned int fb_device_depth = FB_DEVICE_DEPTH;
#else
unsigned int fb_device_depth = 0;
#endif

/* 系统信息 */
char *reworks_building_date = __DATE__;
char *reworks_building_time = __TIME__;

#endif

#ifndef __multi_core__
/* U盘Mount后设备信息 */
#ifdef UMASS_MOUNT_DEVICE_NAME
char *umass_mount_name = UMASS_MOUNT_DEVICE_NAME;
#endif
#endif

#ifdef __cplusplus
}
#endif 

#endif 
