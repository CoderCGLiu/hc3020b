﻿/*! @file deadlock.h
	@brief ReWorks死锁检测与恢复操作的头文件
	
 * 本文件定义了死锁检测与恢复相关的操作接口。
 *  
 * @version @reworks_version
 * 
 * @see 无
 */
#ifndef _DEADLOCK_H_
#define _DEADLOCK_H_

#ifdef __cplusplus
extern "C"
{
#endif

/** @defgroup group_deadlock ReWorks死锁检测与恢复
 * @ingroup group_os_reworks
 *		
 * @{ */

/**
 * @brief  触发一次死锁检测与恢复。
 * 
 * 该接口用于触发一次死锁检测，并在检测到有死锁发生时，通过终止形成死锁的任务队列中优先级最低者达到解除死锁的目的。
 *
 * @param	无
 *	
 * @return	无
 * 
 * @exception 无
 *
 */
void deadlock_detect_trigger();

/**
 * @brief  创建并启动周期性触发死锁检测与恢复的定时器。
 * 
 * 该接口用于创建并启动周期性触发死锁检测与恢复的定时器。
 * 
 * @param	无
 *	
 * @return	无
 * 
 * @exception 无
 *
 */
void deadlock_detect_timmer();

/* @}*/

#ifdef __cplusplus
}

#endif 

#endif
