
/*! @file precond_hook.h
    @brief ReWorks系统前置条件钩子头文件
     
 * 本文件定义了ReWorks系统前置条件钩子函数。
 * 
 * @version 4.7
 * 
 * @see 无
 */

#ifndef PRECOND_HOOK_H_
#define PRECOND_HOOK_H_

#ifdef __cplusplus
extern "C" 
{
#endif
/**
 * @{*/

/** 
 * @brief  初始化前置条件钩子
 * 
 * @param 	无
 * 
 * @return 	无
 * 			
 */
extern void precond_hook_init(void);

/** 
 * @brief 添加内存分配（malloc接口）的前置条件钩子函数
 * 
 * @param 	precond_hook 待添加的钩子。
 * 
 * @return 	0 表示添加成功
 * @return	-1 表示添加失败。
 */
extern int malloc_precond_hook_add(int (*precond_hook)(size_t));

/** 
 * @brief 删除内存分配（malloc接口）的前置条件钩子函数
 * 
 * @param 	precond_hook 待删除的钩子。
 * 
 * @return 	0 表示删除成功
 * @return	-1 表示删除失败。
 */
extern int malloc_precond_hook_delete(int (*precond_hook)(size_t));

/** 
 * @brief 添加文件描述符打开（open接口）的前置条件钩子函数
 * 
 * @param 	precond_hook 待添加的钩子。
 * 
 * @return 	0 表示添加成功
 * @return	-1 表示添加失败。
 */
extern int open_precond_hook_add(int (*precond_hook)());

/** 
 * @brief 删除文件描述符打开（open接口）的前置条件钩子函数
 * 
 * @param 	precond_hook 待删除的钩子。
 * 
 * @return 	0 表示删除成功
 * @return	-1 表示删除失败。
 */
extern int open_precond_hook_delete(int (*precond_hook)());

/** 
 * @brief 添加信号量创建（sem_open /sem_init/sem_init2接口）的前置条件钩子函数
 * 
 * @param 	precond_hook 待添加的钩子。
 * 
 * @return 	0 表示添加成功
 * @return	-1 表示添加失败。
 */
extern int sem_init_precond_hook_add(int (*precond_hook)());

/** 
 * @brief 删除信号量创建（sem_open /sem_init/sem_init2接口）的前置条件钩子函数
 * 
 * @param 	precond_hook 待删除的钩子。
 * 
 * @return 	0 表示删除成功
 * @return	-1 表示删除失败。
 */
extern int sem_init_precond_hook_delete(int (*precond_hook)());

/** 
 * @brief 添加互斥量创建（pthread_mutex_init/pthread_mutex_init2接口）的前置条件钩子函数
 * 
 * @param 	precond_hook 待添加的钩子。
 * 
 * @return 	0 表示添加成功
 * @return 	-1 表示添加失败。
 */
extern int mutex_init_precond_hook_add(int (*precond_hook)());

/** 
 * @brief 删除互斥量创建（pthread_mutex_init/pthread_mutex_init2接口）的前置条件钩子函数
 * 
 * @param 	precond_hook 待删除的钩子。
 * 
 * @return 	0 表示删除成功
 * @return 	-1 表示删除失败。
 */
extern int mutex_init_precond_hook_delete(int (*precond_hook)());

/** 
 * @brief 添加消息队列创建（mq_open/mq_create接口）的前置条件钩子函数
 * 
 * @param 	precond_hook 待添加的钩子。
 * 
 * @return 	0 表示添加成功
 * @return	-1 表示添加失败。
 */
extern int mq_open_precond_hook_add(int (*precond_hook)());

/** 
 * @brief 删除消息队列创建（mq_open/mq_create接口）的前置条件钩子函数
 * 
 * @param 	precond_hook 待删除的钩子。
 * 
 * @return 	0 表示删除成功
 * @return	-1 表示删除失败。
 */
extern int mq_open_precond_hook_delete(int (*precond_hook)());

/* @}*/


#ifdef __cplusplus
}
#endif
#endif
