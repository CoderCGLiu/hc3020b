/*! @file atomic.h
    @brief ReWorks系统原子操作头文件
     
 * 本文件定义了ReWorks系统原子操作接口。
 * 
 * @version 4.7
 * 
 * @see 无
 */
#ifndef _ATOMIC_H_
#define _ATOMIC_H_

#ifdef __cplusplus
extern "C"
{
#endif
	
#include <reworks/types.h>

/**
 * @addtogroup group_atomic
 * 
 * ReWorks操作系统提供的原子操作是利用CPU提供的支持原子地访问内存。
 * 原子操作把一组操作组合成不会被中断的单个操作。因此，原子操作为一些简单地操作组合（比如增一、减一）提供了互斥。
 * @{*/


typedef long atomic_t; //!< 原子操作对象类型
/** 
 * @brief  原子加
 * 
 * @param 	target 原子操作变量的地址
 * @param   value  原子值
 * 
 * @return 	原子加操作之前的变量值
 * 			
 */
extern atomic_t atomic_add (atomic_t * target, atomic_t value);

/** 
 * @brief  原子与
 * 
 * @param 	target 原子操作变量的地址
 * @param   value  原子值
 * 
 * @return 	原子与操作之前的变量值
 * 			
 */
extern atomic_t atomic_and (atomic_t * target, atomic_t value);

/** 
 * @brief  原子减一
 * 
 * @param 	target 原子操作变量的地址
 * 
 * @return 	原子减一操作之前的变量值
 * 			
 */
extern atomic_t atomic_dec (atomic_t * target);

/** 
 * @brief  原子增一
 * 
 * @param 	target 原子操作变量的地址
 * 
 * @return 	原子增一操作之前的变量值
 * 			
 */
extern atomic_t atomic_inc (atomic_t * target);

/** 
 * @brief  原子与非
 * 
 * @param 	target 原子操作变量的地址
 * @param   value  原子值
 * 
 * @return 	原子与非操作之前的变量值
 * 			
 */
extern atomic_t atomic_nand (atomic_t * target, atomic_t value);

/** 
 * @brief  原子或
 * 
 * @param 	target 原子操作变量的地址
 * @param   value  原子值
 * 
 * @return 	原子或操作之前的变量值
 * 			
 */
extern atomic_t atomic_or (atomic_t * target, atomic_t value);

/** 
 * @brief  原子减
 * 
 * @param 	target 原子操作变量的地址
 * @param   value  原子值
 * 
 * @return 	原子减操作之前的变量值
 * 			
 */
extern atomic_t atomic_sub (atomic_t * target, atomic_t value);

/** 
 * @brief  原子异或
 * 
 * @param 	target 原子操作变量的地址
 * @param   value  原子值
 * 
 * @return 	原子异或操作之前的变量值
 * 			
 */
extern atomic_t atomic_xor (atomic_t * target, atomic_t value);

/** 
 * @brief  原子清除
 * 
 * @param 	target 原子操作变量的地址
 * 
 * @return 	原子清除操作之前的变量值
 * 			
 */
extern atomic_t atomic_clear (atomic_t * target);

/** 
 * @brief  获取原子变量值
 * 
 * @param 	target 原子操作变量的地址
 * 
 * @return 	获取原子变量值的结果
 * 			
 */
extern atomic_t atomic_get (atomic_t * target);

/** 
 * @brief  设置原子变量值
 * 
 * @param 	target 原子操作变量的地址
 * @param   value  原子值
 * 
 * @return 	设置原子变量操作之前的变量值
 * 			
 */
extern atomic_t atomic_set (atomic_t * target, atomic_t value);

/** 
 * @brief  原子比较交换
 * 
 * @param 	target 原子操作变量的地址
 * @param   oldValue  旧值
 * @param   newValue  新值
 * 
 * @return 	成功将返回1，失败返回0
 * 			
 */
extern boolean atomic_cas (atomic_t * target, atomic_t oldValue, 
		   atomic_t newValue);

/** 
 * @brief  原子测试并设置
 * 
 * @param 	addr 设置标志的地址，将地址的0设置为1
 * 
 * @return 	成功将返回1，失败返回0
 * 			
 */
extern boolean atomic_tas (void *addr);

/* @}*/

#ifdef __cplusplus
}
#endif

#endif

