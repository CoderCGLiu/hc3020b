/*! @file timer_show.h
    @brief ReWorks POSIX定时器信息获取/显示头文件
    
 * 本文件定义了POSIX定时器对象相关信息的获取/显示操作接口。
 *
 * @version 4.7
 * 
 * @see 无
 */
#ifndef _REWORKS_TIMERSHOW_H_
#define _REWORKS_TIMERSHOW_H_

#ifdef __cplusplus
extern "C"
{
#endif 

#include <signal.h>
	
/**  
 * @addtogroup group_timer
 * @{ */	
 
/*! \struct timer_info
 * \brief 定时器信息结构定义
 *
 * 该数据结构定义了POSIX定时器对象的相关信息，包括定时器相关的信号信息、时钟标识、
 *任务标识、状态信息等。
 */
typedef struct timer_info
{
	struct sigevent timer_sigevent;  //!< 定时器相关信号信息	
	clockid_t timer_clockid; 	//!< 时钟标识 		
	pthread_t timer_thread;		//!< 任务标识
	char timer_state;			//!< 定时器状态 ，取值包括STATE_CREATE_NEW_C、STATE_CREATE_RUN_C和STATE_CREATE_STOP_C
	unsigned int timer_charac;		 //!< 定时器标志，取值包括TIMER_ABSTIME和TIMER_RELATIVE_C
	struct timespec timer_timestamp; //!<  定时器启动时的时间戳	
	struct itimerspec timer_data; 	 //!< 定时器超时时间（绝对/相对）和重载时间 
	unsigned int timer_ticks_expired;  //!< 定时器启动时的超时tick数 ，相对启动时间
	unsigned int timer_overrun;		 //!<  定时器完全运行次数  
} timer_info_t; //!< 定时器信息结构定义

/**
 * @brief    获取定时器的概要信息
 * 
 * 该接口用来获取timer_id指定的定时器的概要信息，包括定时器相关的信号信息、看门狗ID、
 *时钟ID、任务ID、定时器状态、定时器启动信息等。
 *
 * @param    timer_id 	定时器ID
 * @param    timer_info 存放返回的定时器信息的结构地址
 *
 * @return    0		函数执行成功
 * @return    -1	函数执行失败
 *
 * @exception EINVAL  定时器标识timer_id无效，或者timer_info是无效指针
 * @exception EMNOTINITED 定时器模块尚未初始化
 *
 */
int timer_getinfo(timer_t timer_id, timer_info_t *timer_info);


/**
 * @brief     显示定时器信息
 * 
 * 该接口用来显示定时器的信息。
 * 
 * @param    timer_id	定时器ID
 *
 * @return    0		函数执行成功
 * @return    -1	函数执行失败
 *
 * @exception EINVAL  定时器标识timer_id无效
 * @exception EMNOTINITED 定时器模块尚未初始化
 *
 */
int timer_show(timer_t timer_id);

/** @example example_timer_getinfo.c
 * 下面的例子演示了如何使用 timer_getinfo().
 */ 

/** @example example_timer_show.c
 * 下面的例子演示了如何使用 timer_show().
 */
/* @}*/

#ifdef __cplusplus
}
#endif 
	
#endif
