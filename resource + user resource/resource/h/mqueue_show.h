/*! @file mqueue_show.h
    @brief ReWorks消息队列信息获取/显示头文件
    
 * 本文件定义了消息队列对象相关信息的获取/显示操作接口。
 *
 * @version 4.7
 * 
 * @see 无
 */

#ifndef _REWORKS_MQUEUESHOW_H_
#define _REWORKS_MQUEUESHOW_H_

#ifdef __cplusplus
extern "C"
{
#endif
	
#include <mqueue.h>
/** 
 * @addtogroup group_msg_queue 
 * @{ */	
 
/*! \struct mq_info
 * \brief 消息队列信息结构定义
 *
 * 该数据结构定义了消息队列的相关信息，包括消息队列的名字、可存放的消息数目、消息的最大长度、当前的消息数目等。
 */
typedef struct mq_info
{
	char *mq_name;	   //!< 消息队列名字，如果是未命名的消息队列，则为空
	long mq_flags;     //!< 消息队列标志
	long mq_maxmsg;    //!< 消息队列中可存放的消息数目
	long mq_msgsize;   //!< 消息队列中消息的最大长度
	long mq_curmsgs;   //!< 消息队列中当前的消息数目
	int mq_waitqtype;  //!< 阻塞在消息队列上的任务的等待策略
	int mq_waitthreads;  //!< 阻塞在消息队列上的任务数目
	int mq_opencnt;	   //!< 消息队列被打开的次数
	pthread_t mq_thread_notified; //!< 等待通知的任务ID
} mq_info_t; //!< 消息队列信息结构定义


/** 
 * @brief 获取指定消息队列的概要信息。
 * 
 * 该接口用于获取指定的消息队列的概要信息，包括消息队列的标志、可存放的消息数目、消息最大长度、
 *当前的消息数目、消息队列的等待策略、阻塞的任务数、被打开的次数、等待通知的任务ID等信息。
 * 
 * @param 	mqdes	消息队列描述符
 * @param 	mq_info	存放消息队列信息的指针
 * 
 * @return 	0 		函数执行成功
 * @return 	-1	 	函数执行失败
 * 
 * @exception EINVAL 参数mq_info是无效指针
 * @exception EBADF	 mqdes指定的消息队列描述符不合法
 * @exception EMNOTINITED  消息队列模块尚未初始化
 */
int mq_getinfo(mqd_t mqdes, mq_info_t *mq_info);

/** 
 * @brief 显示指定消息队列的信息。
 * 
 * 该接口用于显示指定的消息队列的相关信息。
 * 
 * @param 	mqdes	消息队列描述符
 * @param 	level	显示消息队列信息的详细程度，0为显示概要信息，1为显示详细信息
 * 
 * @return 	0		函数执行成功，并打印消息队列的信息
 * @return 	-1		函数执行失败
 * 
 * @exception EINVAL level取值不合法
 * @exception EBADF	 mqdes指定的消息队列描述符不合法
 * @exception EMNOTINITED  消息队列模块尚未初始化
 */
int mq_show(mqd_t mqdes, int level);

/** @example example_mq_getinfo.c
 * 下面的例子演示了如何使用 mq_getinfo().
 */ 

/** @example example_mq_show.c
 * 下面的例子演示了如何使用 mq_show().
 */


/* @}*/

#ifdef __cplusplus
}
#endif 

#endif
