/*! @file ratemon_show.h
    @brief ReWorks单调速率定时器信息获取/显示头文件
    
 * 本文件定义了单调速率定时器对象相关信息的获取/显示操作接口。
 *
 * @version @reworks_version
 * 
 * @see 无
 */
#ifndef _REWORKS_RATEMONSHOW_H_
#define _REWORKS_RATEMONSHOW_H_

#ifdef __cplusplus
extern "C"
{
#endif 

#include <ratemon.h>

/**  
 * @addtogroup group_timer
 * @{ */	
 
/*! \struct rms_info
 * \brief 单调速率定时器信息的结构定义
 *
 * 该数据结构定义了单调速率定时器的相关信息，包括单调速率定时器的状态、启动时间、周期长度等。
 */
typedef struct rms_info
{
    rms_states rms_state;	//!< 单调速率定时器的状态
    u64 rms_owner_ticks_at_period; //!< 单调速率定时器启动的时间点，以其所有者运行的滴答（tick）数为基准
    u64 rms_time_at_period;	//!< 单调速率定时器启动的时间点，以系统启动以来流逝的滴答（tick）数为基准
	u32 rms_owner_ticks_since_period; 	//!< 最近一个周期启动至今执行的滴答（tick）数
	u32 rms_time_since_period;			//!< 最近一个周期启动至今流逝的滴答（tick）数
    u32 rms_next_periods;	//!< 单调速率定时器的周期长度
    pthread_t rms_owner;	//!< 单调速率定时器的所有者
} rms_info_t; //!< 单调速率定时器信息的结构定义

/**
 * @brief    获取单调速率定时器的概要信息
 * 
 * 该接口用于获取rms_id指定的单调速率定时器的概要信息，包括单调速率定时器的当前状态、启动开始时间、
 *执行时间、周期长度、所有者等。
 *
 * @param    rms_id 	单调速率定时器ID
 * @param    rms_info 	存放返回的单调速率定时器信息的结构地址
 *
 * @return    0		函数执行成功
 * @return    -1	函数执行失败
 *
 * @exception EINVAL  单调速率定时器标识rms_id无效，或者rms_info是无效指针
 * @exception EMNOTINITED 单调速率定时器模块尚未初始化
 *
 */
int rms_getinfo(rms_t rms_id, rms_info_t *rms_info);


/**
 * @brief     显示单调速率定时器信息
 * 
 * 该接口用于显示单调速率定时器的信息。
 * 
 * @param    rms_id 单调速率定时器ID
 *
 * @return    0		函数执行成功
 * @return    -1	函数执行失败
 *
 * @exception EINVAL  单调速率定时器标识rms_id无效
 * @exception EMNOTINITED 单调速率定时器模块尚未初始化
 *
 */
int rms_show(rms_t rms_id);

/** @example example_rms_getinfo.c
 * 下面的例子演示了如何使用接口 rms_getinfo( ).
 */
/** @example example_rms_show.c
 * 下面的例子演示了如何使用接口 rms_show( ).
 */
 
/* @}*/

#ifdef __cplusplus
}
#endif 
	
#endif
