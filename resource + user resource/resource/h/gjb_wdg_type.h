#ifndef __GJB_WDG_TYPE_H__
#define __GJB_WDG_TYPE_H__

#include <reworks/types.h>

/*! \struct Watchdog_Ctrl
 * \brief 看门狗定时器控制块
 *
 * 该数据结构定义了看门狗定时器对象的控制块信息。
 */
typedef struct
{
    u32 id; //!< 看门狗定时器ID
    int ticks; //!< 看门狗定时器定时时间
    wdg_t the_wdg; //!< 看门狗定时器核心对象
} Watchdog_Ctrl;

#endif

