/*! @file mutex_show.h
    @brief ReWorks互斥量信息获取/显示头文件
    
 * 本文件定义了互斥量对象相关信息的获取/显示操作接口。
 *
 * @version 4.7
 * 
 * @see 无
 */
#ifndef _REWORKS_MUTEXSHOW_H_
#define _REWORKS_MUTEXSHOW_H_

#ifdef __cplusplus
extern "C"
{
#endif
	
#include <sys/types.h>
#include <atomic.h>
/** 
 * 
 * @addtogroup group_pthread_mutex_show ReWorks互斥量显示
 * @ingroup group_os_reworks_task
 * @{ */
 
/*! \struct pthread_mutex_info
 * \brief 互斥量信息结构定义
 *
 * 该数据结构定义了互斥量的相关信息，包括互斥量的类型、优先级天花板、阻塞排队策略、所有者等。
 */
typedef struct pthread_mutex_info
{
	int	type; 		   	//!< 互斥量的类型
	int protocol;      	//!< 互斥量的优先级倒置的避免协议
	int prio_ceiling;  	//!< 互斥量的优先级天花板
	int waitq_type;    	//!< 互斥量的阻塞排队策略
	int cancel_safe;   	//!< 互斥量的任务取消安全属性
	pthread_t owner;	//!< 互斥量的拥有者
	atomic_t recursive_cnt;	//!< 互斥量嵌套锁定的数目
	int tasks_blocked; //!< 阻塞在互斥量上的任务数
} pthread_mutex_info_t; //!< 互斥量信息结构定义

/** 
 * @brief 显示指定互斥量的信息。
 * 
 * 该接口用于显示指定的互斥量的相关信息。
 * 
 * @param 	mutex	指向互斥量对象的指针
 * @param 	level	显示互斥量信息的详细程度，0为显示概要信息，1为显示互斥量详细信息
 * 
 * @return 	0		函数执行成功
 * @return 	EINVAL 	mutex为无效指针，或者level取值不合法，或者mutex指定的互斥量不存在
 * @return 	EMNOTINITED 互斥量模块尚未初始化
 * 
 * @exception EINVAL 参数无效
 * @exception EMNOTINITED 模块尚未初始化
 */
int pthread_mutex_show(pthread_mutex_t *mutex, int level);

/** 
 * @brief 获取指定互斥量的概要信息。
 * 
 * 该接口用于获取指定的互斥量的概要信息，包括互斥量的类型、优先级倒置避免协议、优先级
 *天花板、阻塞排队策略、任务取消安全属性、拥有者等信息。
 * 
 * @param 	mutex	 指向互斥量对象的指针
 * @param 	mutex_info	存放互斥量信息的指针
 * 
 * @return 	0 		函数执行成功
 * @return 	EINVAL 	参数mutex或mutex_info为无效指针，或者mutex指定的互斥量不存在
 * @return 	EMNOTINITED 互斥量模块尚未初始化
 * 
 * @exception EINVAL 参数无效
 * @exception EMNOTINITED 模块尚未初始化
 */
int pthread_mutex_getinfo(pthread_mutex_t *mutex, pthread_mutex_info_t *mutex_info);

/** @example example_pthread_mutex_show.c
 * 下面的例子演示了如何使用 pthread_mutex_show().
 */ 

/** @example example_pthread_mutex_getinfo.c
 * 下面的例子演示了如何使用 pthread_mutex_getinfo().
 */
 
/* @}*/

#ifdef __cplusplus
}
#endif 

#endif

