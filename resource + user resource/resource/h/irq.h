/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 */

#ifndef _REWORKS_IRQ_H_
#define _REWORKS_IRQ_H_

#ifdef __cplusplus
extern "C"
{
#endif 
	
#include <reworks/types.h>
	
/* 中断处理函数类型 */
typedef void (* INT_HANDLER)(void *);

/**
 * 中断响应函数类型 
 */
typedef int(* INT_ACK_FUNCPTR)(int);

/**
 * 中断使能函数类型 
 */
#if defined (__HUARUI2__) || defined (__HUARUI3__)
	typedef int (* INT_ENBALE_FUNCPTR)(u32);
#else
	typedef int (* INT_ENBALE_FUNCPTR)(int);
#endif

/**
 * 中断屏蔽函数类型
 */
#if defined (__HUARUI2__) || defined (__HUARUI3__)
	typedef int (* INT_DISBALE_FUNCPTR)(u32);
#else
typedef int (* INT_DISBALE_FUNCPTR)(int);
#endif

/**
 * 获取中断号函数类型
 */
#if defined (__HUARUI2__) || defined (__HUARUI3__)
	typedef int (* INT_INUM_FUNCPTR)(u32, u32[] , u32);
#else
	typedef int (* INT_INUM_FUNCPTR)(int);
#endif

/**
 * 初始化中断控制器接口
 * （返回值为中断控制器支持的中断数量）
 */
typedef int (* INT_INIT_FUNPTR)(void);

/*!
 * \typedef INT_SPECIAL_FUNCPTR
 * 
 * \brief 特殊中断处理函数
 */
typedef int (* INT_SPECIAL_FUNCPTR)(void *);

/*!
 * \struct int_operations
 *
 * \brief 定义中断接口结构体
 */
struct int_operations
{
	INT_ACK_FUNCPTR 			ack;		//!< 相应操作
	INT_ENBALE_FUNCPTR			enable;		//!< 使能操作
	INT_DISBALE_FUNCPTR			disable;	//!< 非能操作
	INT_INUM_FUNCPTR			inum;		//!< 获取中断号操作
	INT_INIT_FUNPTR				init;		//!< 初始化操作
	INT_SPECIAL_FUNCPTR			special1;
};

extern u32 isr_nest_level();
extern u32 int_count();
extern int int_install_handler(char *name,int vecnum,int pri, INT_HANDLER handler, void *param);
extern int int_uninstall_handler(int vecnum);
extern int shared_int_install(int vecnum, INT_HANDLER handler, void *param);
extern int shared_int_uninstall(int vecnum, INT_HANDLER handler);
extern int int_enable_pic(u32 irq_line);
extern int int_disable_pic(u32 irq_line);

#ifdef __multi_core__ 
int int_cpu_lock(void);
void int_cpu_unlock(int level);
#define int_lock int_cpu_lock
#define int_unlock int_cpu_unlock
#else
extern int int_lock(void);
extern void int_unlock(int level);	
#endif

struct irq_data
{
	INT_HANDLER		handler;
	void			*param;
	char  *name;   
	int    pri;    
};	

int shared_int_module_init(void);

/* 硬件中断操作注册接口 */
int int_operations_register(struct int_operations *ops);

#include <arch_irq.h>

#ifdef __cplusplus
}
#endif 

#endif 
