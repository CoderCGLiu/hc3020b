/*! @file mutex3.h
    @brief ReWorks互斥量初始化及销毁的头文件
    
 * 本文件定义了一种互斥量初始化/销毁操作的接口。
 *
 * @version @reworks_version
 * 
 * @see 无
 */
#ifndef _REWORKS_MUTEX3_H_
#define _REWORKS_MUTEX3_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <sys/types.h>

/** 
 * 
 * @addtogroup group_os_reworks_mutex ReWorks互斥量管理
 * @ingroup group_os_reworks_task
 * @{ */

//!互斥量的实际内容
#ifndef REWORKS_LP64
#define DEFINE_PREALLOC_PTHREAD_MUTEX(m) \
	int placeholder1[2]; \
	pthread_mutex_t m; \
	char placeholder2[72]
  #else
  #define DEFINE_PREALLOC_PTHREAD_MUTEX(m) \
	long placeholder1[2]; \
	pthread_mutex_t m; \
	char placeholder2[120]
  #endif
/** 
* @brief 初始化互斥量
* 
* 该接口使用参数指定的互斥量属性值来初始化 mutex 指定的互斥量，该互斥量必须由DEFINE_PREALLOC_PTHREAD_MUTEX定义。初始化成功的话，互斥量
*的状态变为已初始化且未锁定的状态。 尝试对一个已初始化的互斥量进行初始化，函数执行失败。
* 
* @param 	mutex 	存放互斥量对象的指针
* @param 	type 	互斥量的类型，取值范围：PTHREAD_MUTEX_NORMAL/PTHREAD_MUTEX_ERRORCHECK/
*                  PTHREAD_MUTEX_RECURSIVE/PTHREAD_MUTEX_DEFAULT
* @param	options 互斥量的可选项，包括RE_MUTEX_PRIO_NONE/RE_MUTEX_PRIO_INHERIT/RE_MUTEX_PRIO_PROTECT/
* 					RE_MUTEX_WAIT_FIFO/RE_MUTEX_CANCEL_SAFE
* @param	prioceiling 互斥量的优先级天花板，范围为[PTHREAD_PRIO_MIN, PTHREAD_PRIO_MAX]，
* 					取值为-1时，使用默认值PTHREAD_MUTEX_CEILING				
* 
* @return 	0 		函数运行成功
* @return 	EINVAL  mutex是无效指针，或者type/options/prioceiling的取值不合法，或者mutex指定的互斥量不存在
* @return 	ENOTSUP options的设置存在冲突
* @return 	EMNOTINITED 互斥量模块尚未初始化
* @return 	ECALLEDINISR 当前处于中断上下文中，不能执行该操作
* 
* @exception EINVAL 参数错误 
* @exception ENOTSUP 系统不支持的设置
* @exception EMNOTINITED 模块尚未初始化
* @exception ECALLEDINISR 接口不能在中断上下文中调用
* 
* @see pthread_mutex_destroy3()
* @attention
* 		用该接口初始化互斥量之前，互斥量必须由DEFINE_PREALLOC_PTHREAD_MUTEX来定义。
* 		参数options可选取值的详细说明参见<sys/types.h>，其中RE_MUTEX_PRIO_NONE/RE_MUTEX_PRIO_INHERIT/
* 		RE_MUTEX_PRIO_PROTECT三者只能设置其一，或者不设置，否则返回错误EINVAL，不设置时使用缺省值RE_MUTEX_PRIO_NONE；
* 	    options中设置RE_MUTEX_WAIT_FIFO的情况下，只能选择RE_MUTEX_PRIO_NONE，否则返回错误ENOTSUP。
* 			
*/
int pthread_mutex_init3(pthread_mutex_t *mutex, int type, int options, int prioceiling);

/** 
 * @brief 销毁互斥量
 *
 * 该接口用于销毁mutex引用的互斥量对象。互斥量对象销毁后可以通过 pthread_mutex_init3()
 *重新初始化，在对象销毁后对它进行其它形式的引用都会出错。 销毁一个未锁定的已初始化的
 *互斥量是安全的，销毁一个被锁定的互斥量，函数执行失败，返回错误号EBUSY。
 * 
 * @param 	mutex 	指向互斥量对象的指针
 * 
 * @return 	0 		函数运行成功
 * @return 	EINVAL	mutex是一个无效的指针，或者mutex指定的互斥量不存在
 * @return 	EBUSY	互斥量被锁定
 * @return 	EMNOTINITED 互斥量模块尚未初始化
 * @return 	ECALLEDINISR 当前处于中断上下文中，不能执行该操作
 * 
 * @exception EBUSY	 对象忙
 * @exception EINVAL 参数无效
 * @exception EMNOTINITED 模块尚未初始化
 * @exception ECALLEDINISR 接口不能在中断上下文中调用
 * 
 * @see pthread_mutex_init3()
 */
int pthread_mutex_destroy3(pthread_mutex_t *mutex);

/** @example example_pthread_mutex_init3.c
* 下面的例子演示了如何使用 pthread_mutex_init3().
*/
/** @example example_pthread_pthread_mutex_destroy3.c
* 下面的例子演示了如何使用 pthread_mutex_destroy3().
*/

/* @}*/
#ifdef __cplusplus
}
#endif 

#endif
