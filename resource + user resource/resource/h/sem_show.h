/*! @file sem_show.h
    @brief ReWorks信号量信息获取/显示头文件
    
 * 本文件定义了信号量对象相关信息的获取/显示操作接口。
 *
 * @version 4.7
 * 
 * @see 无
 */
#ifndef _REWORKS_SEMSHOW_H_
#define _REWORKS_SEMSHOW_H_

#ifdef __cplusplus
extern "C"
{
#endif
	
#include <semaphore.h>
/** 
 * @addtogroup group_semaphore
 * @{ */
 
/* 信号量类型 */
#define SEM_TYPE_COUNTING_NAMED  	0x00 //!< 命名的计数信号量
#define SEM_TYPE_BINARY_NAMED 		0x01 //!< 命名的二进制信号量
#define SEM_TYPE_COUNTING_UNNAMED  	0x02 //!< 未命名的计数信号量
#define SEM_TYPE_BINARY_UNNAMED 	0x03 //!< 未命名的二进制信号量
	
/*! \struct sem_info
 * \brief 信号量信息结构定义
 *
 * 该数据结构定义了信号量相关的信息，包括信号量的类型、名字、最大值、当前值、打开次数等。
 */
typedef struct sem_info
{
	int sem_type; 		//!< 信号量的类型	
	char *sem_name;		//!< 信号量的名字
	int sem_maxvalue;	//!< 信号量的最大值
	int sem_curvalue;	//!< 信号量的当前值
	int sem_opencnt;	//!< 信号量的打开次数
	int sem_linked;		//!< 信号量的link标志
	int tasks_blocked;  //!< 阻塞在信号量上的任务数目
} sem_info_t; //!< 信号量信息结构定义

/** 
 * @brief 获取指定信号量的概要信息。
 * 
 * 该接口用于获取指定的信号量的概要信息，包括信号量的类型、名字、最大值、当前值、
 * 打开次数、link标记和阻塞在指定信号量上的任务数目等信息。
 * 
 * @param 	sem	 信号量标识
 * @param 	sem_info	存放信号量信息的指针
 * 
 * @return 	0 		函数执行成功
 * @return 	-1	 	函数执行失败
 * 
 * @exception EINVAL sem指定的信号量不合法，或者sem_info是无效指针
 * @exception EMNOTINITED 信号量模块尚未初始化
 */
int sem_getinfo(sem_t  *sem, sem_info_t  *sem_info);

/** 
 * @brief 显示指定信号量的信息。
 * 
 * 该接口用于显示sem指定的信号量的相关信息。
 * 
 * @param 	sem	 	信号量标识
 * @param 	level	显示信号量信息的详细程度，0为显示概要信息，1为显示详细信息
 * 
 * @return 	0		函数执行成功，并打信号量的信息
 * @return 	-1		函数执行失败
 * 
 * @exception EINVAL sem指定的信号量不合法，或者level取值不合法
 * @exception EMNOTINITED 信号量模块尚未初始化
 */
int sem_show(sem_t *sem, int level);

 /** @example example_sem_getinfo.c
 * 下面的例子演示了如何使用 sem_getinfo().
 */
 
 /** @example example_sem_show.c
 * 下面的例子演示了如何使用 sem_show().
 */
 
/* @}*/

#ifdef __cplusplus
}

#endif 

#endif 
