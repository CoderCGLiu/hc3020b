﻿/*! @file bsp.h
    @brief ReWorks BSP初始化头文件
    
 * 本文件定义了ReWorks系统中BSP初始化所需实现的接口。
 *
 * @version @reworks_version
 * 
 * @see 无
 */
 
/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了BSP初始化所需实现的接口
 * 修改：
 * 		 1. 2011-11-17，规范BSP 
 */
#ifndef _REWORKS_BSP_H_
#define _REWORKS_BSP_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <reworks/types.h>
//#include "bsp_printk_debug.h"
#include "bsp_rtc.h"
#include "bsp_int.h"
#include "bsp_macro.h"			/* BSP常用宏定义 */
#include "bsp_sys_io.h"
/**  
 * @defgroup group_os_bsp_general 初始化模块
 * @ingroup group_os_bsp
 *		
 *  @{ 
 */	

extern sys_cfg_tbl *sys_cfg_ptr; //!< 系统配置函数指针

extern ptrdiff_t free_mem_start; //!< 操作系统管理的内存的起始地址
	
extern int set_mem_start_phy_addr(void *addr); //!< 设置物理内存起始地址 
	
/*!
 * \fn int bsp_module_init(void)
 * 
 * \brief BSP初始化接口
 * 
 * \param 无
 *
 * \return 成功返回0
 * \return 失败返回-1
 */	
int bsp_module_init(void);

/*!
 * \fn void reboot()
 * 
 * \brief 硬件热重启接口，实现板卡的复位功能
 * 
 * \param 无
 *
 * \return 无
 */	
void reboot();

/*!
 * \fn int is_va_valid(unsigned long addr)
 * 
 * \brief 判断地址是否有效
 * 
 * \param addr 要判断的地址
 *
 * \return 地址有效返回1
 * \return 地址无效返回0
 */	
int is_va_valid(unsigned long addr);

/*!
 * \fn int satadev_create(int ctrl, int drive, const char *devname)
 * 
 * \brief 创建SATA设备.
 * 
 * 		SATA硬盘设备使用参数控制器号ctrl和驱动器号drive指定。本接口由ATA设备驱动程序
 * 提供。SATA设备驱动程序调用块设备注册接口将SATA设备注册到I/O模块，然后I/O模块分析注册的块设备分区
 * 情况，并为SATA设备及其分区创建相应的设备文件。
 * 		本接口可适用于SATA硬盘设备
 *  
 * \param ctrl 控制器号
 * \param drive 驱动器号
 * \param devname 注册的设备名称
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int satadev_create(int ctrl, int drive, const char *devname);

/*!
 * \fn int atadev_create(int ctrl, int drive, const char *devname)
 * 
 * \brief 创建ATA设备.
 * 
 * 		ATA设备使用参数控制器号ctrl和驱动器号drive指定。本接口由ATA设备驱动程序
 * 提供。ATA设备驱动程序调用块设备注册接口将ATA设备注册到I/O模块，然后I/O模块分析注册的块设备分区
 * 情况，并为ATA设备及其分区创建相应的设备文件。
 * 		本接口可适用于ATA/IDE设备、CF卡设备的创建。
 *  
 * \param ctrl 控制器号
 * \param drive 驱动器号
 * \param devname 注册的设备名称
 * 
 * \return 成功返回0
 * \return 失败返回-1
 * 
 */
int atadev_create(int ctrl, int drive, const char *devname);

/* @} */

#ifdef __cplusplus
}
#endif

#endif
