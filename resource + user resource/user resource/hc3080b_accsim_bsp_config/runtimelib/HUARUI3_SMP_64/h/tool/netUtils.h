/*! 
 * Copyright (c) 2014, ZZKK, CETC32
 * All rights reserved.
 * 
 * @file  netUtils.h
 * @brief 自定义四华睿网络接口
 * 
 * 		本文件定义了通过ftp/tftp操作文件及加载模块的接口
 * 
 * @version 1.0
 * @author  FuKai
 * @date 	2014.12.8
 */

#ifndef NETUTILS_H_
#define NETUTILS_H_


/**  
 * @brief    通过ftp获取单个文件
 *
 * @param   pFtpName			ftp服务器IP或名称    
 * @param   pUser  				ftp用户名
 * @param	pPasswd				ftp密码
 * @param 	pRemoteFile			远程文件名称（可以带路径）
 * @param	pLocalDir			本地目录
 * @param	bMkDirIfNotExists	是否创建目录（如果不存在）
 *
 * @return	OK(0)		函数执行成功
 * @return 	ERROR(-1)	函数执行失败
 *
 */
int ftpGetFile(char *pFtpName, char *pUser, char *pPasswd,
		char *pRemoteFile, char * pLocalDir, BOOL bMkDirIfNotExists);

/**! 
 * 
 * @brief    通过FTP上传单个文件
 *
 * @param	pFtpName          	FTP服务器ip或网络名                                                
 * @param	pUser           	用户名                                                                    
 * @param	pPasswd         	密码 * 		
 * @param	pLocalFile			本地文件名（可带路径）
 * @param	pRemoteFile     	远程目录名（可带路径 ）
 *
 * @return	OK(0)		函数执行成功
 * @return 	ERROR(-1)	函数执行失败
 */	
STATUS ftpPutFile(char *pFtpName, char *pUser, char *pPasswd, 
		char *pLocalFile, char * pRemoteDir)

/**  
 * @brief    通过ftp加载单个模块
 *
 * @param   pFtpName			ftp服务器IP或名称    
 * @param   pUser  				ftp用户名
 * @param	pPasswd				ftp密码
 * @param 	pRemoteFile			远程文件名称（可以带路径）
 * @param	pLocalDir			本地目录
 * @param	bMkDirIfNotExists	是否创建目录（如果不存在）
 *
 * @return	OK(0)		函数执行成功
 * @return 	ERROR(-1)	函数执行失败
 *
 */
void * ftpld(char *pFtpName, char *pUser, char *pPasswd, 
		char *pRemoteFileName, BOOL bUpdateIfExists);

void netUtils_module_init();

#endif /* NETUTILS_H_ */
