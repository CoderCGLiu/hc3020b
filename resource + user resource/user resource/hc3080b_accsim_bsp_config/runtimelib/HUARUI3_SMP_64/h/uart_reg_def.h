#ifndef UART_REG_H
#define UART_REG_H
//#include "hw_addr.h"
#include <reworks/types.h>

/*register define*/
typedef struct 
{
	volatile u32 CTRL;
	volatile u32 MODE;
	volatile u32 IER;
	volatile u32 IDR;
	volatile u32 IMR;
	volatile u32 CISR;
	volatile u32 BRGR;
	volatile u32 RTOR;
	volatile u32 RTR;
	volatile u32 MCR;
	volatile u32 MSR;
	volatile u32 CSR;
	volatile u32 FIFO;
	volatile u32 BDIVR;
	volatile u32 FCDR;
	volatile u32 RPWR;
	volatile u32 TPWR;
	volatile u32 TTR;
	volatile u32 RBSR;
}uart_def;

#define UART_BASE_ADDR	0x1F080000
#if (CPU == MIPS64)
#define UART0_BASE_ADDR (0x9000000000000000 | ((u64)UART_BASE_ADDR))
#define UART1_BASE_ADDR (0x9000000000000000 | ((u64)(UART_BASE_ADDR + 0x8000)))

#else
#define UART0_BASE_ADDR (UART_BASE_ADDR)
#define UART1_BASE_ADDR (UART_BASE_ADDR + 0x8000)

#endif


#define UART_CTRL_MASK					(0x000001ff)
#define UART_MODE_MASK					(0x00003fff)
#define UART_IER_MASK					(0x00003fff)
#define UART_IDR_MASK					(0x000001ff)
#define UART_IMR_MASK					(0x000001ff)
#define UART_CISR_MASK					(0x000001ff)
#define UART_BRGR_MASK					(0x0000ffff)
#define UART_RTOR_MASK					(0x000000ff)
#define UART_RTR_MASK					(0x0000001f)
#define UART_MCR_MASK					(0x00000023)
#define UART_MSR_MASK					(0x000000ff)

#define UART_CSR_MASK					(0x0000ffff)
#define UART_CSR_TNFUL_MASK				(1<14)
#define UART_CSR_REMPTY_MASK			(0x00000002)
#define UART_CSR_TEMPTY_MASK			(0x00000008)
#define UART_CSR_TFUL_MASK				(1<<4)

#define UART_FIFO_MASK					(0xffffffff)
#define UART_BDIVR_MASK					(0x000000ff)
#define UART_FCDR_MASK					(0x0000001f)
#define UART_RPWR_MASK					(0x0000ffff)
#define UART_TPWR_MASK					(0x000000ff)
#define UART_TTR_MASK					(0x0000001f)
#define UART_RBSR_MASK					(0x000007ff)

#define UART_INT_RBRK_MASK				(1<<13)
#define UART_INT_TOVR_MASK				(1<<12)
#define UART_INT_TNFUL_MASK				(1<<11)
#define UART_INT_TTRIG_MASK				(1<<10)
#define UART_INT_DMSI_MASK				(1<<9)
#define UART_INT_TIMEOUT_MASK			(1<<8)
#define UART_INT_PARE_MASK				(1<<7)
#define UART_INT_FRAME_MASK				(1<<6)
#define UART_INT_ROVR_MASK				(1<<5)
#define UART_INT_TFUL_MASK				(1<<4)
#define UART_INT_TEMPTY_MASK			(1<<3)
#define UART_INT_RFUL_MASK				(1<<2)
#define UART_INT_REMPTY_MASK			(1<<1)
#define UART_INT_RTRIG_MASK				(1<<0)


/* CDNS UART */

#define CDNS_UART_FIFO_SIZE	64	/* FIFO size */
#define CDNS_UART_REGISTER_SPACE	0x1000
#define TX_TIMEOUT		500000

/* Register offsets for the UART. */
#define CDNS_UART_CR		0x00  /* Control Register */
#define CDNS_UART_MR		0x04  /* Mode Register */
#define CDNS_UART_IER		0x08  /* Interrupt Enable */
#define CDNS_UART_IDR		0x0C  /* Interrupt Disable */
#define CDNS_UART_IMR		0x10  /* Interrupt Mask */
#define CDNS_UART_ISR		0x14  /* Interrupt Status */
#define CDNS_UART_BAUDGEN	0x18  /* Baud Rate Generator */
#define CDNS_UART_RXTOUT	0x1C  /* RX Timeout */
#define CDNS_UART_RXWM		0x20  /* RX FIFO Trigger Level */
#define CDNS_UART_MODEMCR	0x24  /* Modem Control */
#define CDNS_UART_MODEMSR	0x28  /* Modem Status */
#define CDNS_UART_SR		0x2C  /* Channel Status */
#define CDNS_UART_FIFO		0x30  /* FIFO */
#define CDNS_UART_BAUDDIV	0x34  /* Baud Rate Divider */
#define CDNS_UART_FLOWDEL	0x38  /* Flow Delay */
#define CDNS_UART_IRRX_PWIDTH	0x3C  /* IR Min Received Pulse Width */
#define CDNS_UART_IRTX_PWIDTH	0x40  /* IR Transmitted pulse Width */
#define CDNS_UART_TXWM		0x44  /* TX FIFO Trigger Level */
#define CDNS_UART_RXBS		0x48  /* RX FIFO byte status register */


/* Control Register Bit Definitions */
#define CDNS_UART_CR_STOPBRK	0x00000100  /* Stop TX break */
#define CDNS_UART_CR_STARTBRK	0x00000080  /* Set TX break */
#define CDNS_UART_CR_TX_DIS	0x00000020  /* TX disabled. */
#define CDNS_UART_CR_TX_EN	0x00000010  /* TX enabled */
#define CDNS_UART_CR_RX_DIS	0x00000008  /* RX disabled. */
#define CDNS_UART_CR_RX_EN	0x00000004  /* RX enabled */
#define CDNS_UART_CR_TXRST	0x00000002  /* TX logic reset */
#define CDNS_UART_CR_RXRST	0x00000001  /* RX logic reset */
#define CDNS_UART_CR_RST_TO	0x00000040  /* Restart Timeout Counter */
#define CDNS_UART_RXBS_PARITY    0x00000001 /* Parity error status */
#define CDNS_UART_RXBS_FRAMING   0x00000002 /* Framing error status */
#define CDNS_UART_RXBS_BRK       0x00000004 /* Overrun error status */


/*
 * Mode Register:
 * The mode register (MR) defines the mode of transfer as well as the data
 * format. If this register is modified during transmission or reception,
 * data validity cannot be guaranteed.
 */
#define CDNS_UART_MR_CLKSEL		0x00000001  /* Pre-scalar selection */
#define CDNS_UART_MR_CHMODE_L_LOOP	0x00000200  /* Local loop back mode */
#define CDNS_UART_MR_CHMODE_NORM	0x00000000  /* Normal mode */
#define CDNS_UART_MR_CHMODE_MASK	0x00000300  /* Mask for mode bits */

#define CDNS_UART_MR_STOPMODE_2_BIT	0x00000080  /* 2 stop bits */
#define CDNS_UART_MR_STOPMODE_1_BIT	0x00000000  /* 1 stop bit */

#define CDNS_UART_MR_PARITY_NONE	0x00000020  /* No parity mode */
#define CDNS_UART_MR_PARITY_MARK	0x00000018  /* Mark parity mode */
#define CDNS_UART_MR_PARITY_SPACE	0x00000010  /* Space parity mode */
#define CDNS_UART_MR_PARITY_ODD		0x00000008  /* Odd parity mode */
#define CDNS_UART_MR_PARITY_EVEN	0x00000000  /* Even parity mode */

#define CDNS_UART_MR_CHARLEN_6_BIT	0x00000006  /* 6 bits data */
#define CDNS_UART_MR_CHARLEN_7_BIT	0x00000004  /* 7 bits data */
#define CDNS_UART_MR_CHARLEN_8_BIT	0x00000000  /* 8 bits data */


/*
 * Interrupt Registers:
 * Interrupt control logic uses the interrupt enable register (IER) and the
 * interrupt disable register (IDR) to set the value of the bits in the
 * interrupt mask register (IMR). The IMR determines whether to pass an
 * interrupt to the interrupt status register (ISR).
 * Writing a 1 to IER Enables an interrupt, writing a 1 to IDR disables an
 * interrupt. IMR and ISR are read only, and IER and IDR are write only.
 * Reading either IER or IDR returns 0x00.
 * All four registers have the same bit definitions.
 */
#define CDNS_UART_IXR_RBRK	    0x00002000 /* 1<<13 Receiver break detect. */
#define CDNS_UART_IXR_TXOVR	    0x00001000 /* 1<<12 Transmitter FIFO Overflow. */
#define CDNS_UART_IXR_TXNFUL	0x00000800 /* 1<<11 Transmitter FIFO Nearly Full. */
#define CDNS_UART_IXR_TXTRIG	0x00000400 /* 1<<10 Transmitter FIFO Trigger. */
#define CDNS_UART_IXR_DMSI	    0x00000200 /* 1<<09 Delta Modem Status Indicator */
#define CDNS_UART_IXR_RXTOUT	0x00000100 /* 1<<08 Receiver Timeout Error */
#define CDNS_UART_IXR_PARITY		0x00000080 /* 1<<07 Receiver Parity Error */
#define CDNS_UART_IXR_RXFRAM	0x00000040 /* 1<<06 Receiver Framing Error. */
#define CDNS_UART_IXR_RXOVR		0x00000020 /* 1<<05 Receiver Overflow Error */
#define CDNS_UART_IXR_TXFULL	0x00000010 /* 1<<04 Transmitter FIFO Full. */
#define CDNS_UART_IXR_TXEMPTY	0x00000008 /* 1<<03 Transmitter FIFO Empty. */
#define CDNS_UART_IXR_RXFULL	0x00000004 /* 1<<02 Receiver FIFO Full. */
#define CDNS_UART_IXR_RXEMPTY	0x00000002 /* 1<<01 Receiver FIFO Empty. */
#define CDNS_UART_IXR_RXTRIG	0x00000001 /* 1<<00 Receiver FIFO Trigger. */

/*
 * Do not enable parity error interrupt for the following
 * reason: When parity error interrupt is enabled, each Rx
 * parity error always results in 2 events. The first one
 * being parity error interrupt and the second one with a
 * proper Rx interrupt with the incoming data.  Disabling
 * parity error interrupt ensures better handling of parity
 * error events. With this change, for a parity error case, we
 * get a Rx interrupt with parity error set in ISR register
 * and we still handle parity errors in the desired way.
 */

#define CDNS_UART_RX_IRQS	(CDNS_UART_IXR_RXFRAM | \
		CDNS_UART_IXR_RXOVR | \
			 CDNS_UART_IXR_RXTRIG |	 \
			 CDNS_UART_IXR_RXTOUT)


/* Goes in read_status_mask for break detection as the HW doesn't do it*/
#define CDNS_UART_IXR_BRK	0x00002000

#define CDNS_UART_RXBS_SUPPORT BIT(1)
/*
 * Modem Control register:
 * The read/write Modem Control register controls the interface with the modem
 * or data set, or a peripheral device emulating a modem.
 */
#define CDNS_UART_MODEMCR_FCM	0x00000020 /* Automatic flow control mode */
#define CDNS_UART_MODEMCR_RTS	0x00000002 /* Request to send output control */
#define CDNS_UART_MODEMCR_DTR	0x00000001 /* Data Terminal Ready */

/*
 * Channel Status Register:
 * The channel status register (CSR) is provided to enable the control logic
 * to monitor the status of bits in the channel interrupt status register,
 * even if these are masked out by the interrupt mask register.
 */
#define CDNS_UART_SR_RXEMPTY	0x00000002 /* RX FIFO empty */
#define CDNS_UART_SR_TXEMPTY	0x00000008 /* TX FIFO empty */
#define CDNS_UART_SR_TXFULL	0x00000010 /* TX FIFO full */
#define CDNS_UART_SR_RXTRIG	0x00000001 /* Rx Trigger */
#define CDNS_UART_SR_TACTIVE	0x00000800 /* TX state machine active */


/* Valid RX bit mask 所有与receive相关的位*/
#define CDNS_UART_IXR_RXMASK	(CDNS_UART_IXR_RXTRIG | CDNS_UART_IXR_RXEMPTY | CDNS_UART_IXR_RXFULL \
		| CDNS_UART_IXR_RXOVR | CDNS_UART_IXR_RXFRAM | CDNS_UART_IXR_PARITY | CDNS_UART_IXR_RXTOUT | CDNS_UART_IXR_RBRK)
/* Valid TX bit mask 所有与transmit相关的位*/
#define CDNS_UART_IXR_TXMASK    (CDNS_UART_IXR_TXEMPTY | CDNS_UART_IXR_TXFULL | CDNS_UART_IXR_TXTRIG \
		| CDNS_UART_IXR_TXNFUL | CDNS_UART_IXR_TXOVR)

#define UART0	((uart_def *) UART0_BASE_ADDR)
#define UART1	((uart_def *) UART1_BASE_ADDR)

#endif

