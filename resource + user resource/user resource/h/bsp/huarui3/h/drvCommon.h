#ifndef __H_HR3_DRV_COMMOM__
#define __H_HR3_DRV_COMMOM__

#include "uart_reg_def.h"
#include <sys/types.h>

#define writec(v,a)	{ \
	(*(volatile u8 *)((a)|0x9000000000000000) = (v));}

#define writes(v,a)	{ \
	(*(volatile u16 *)((a)|0x9000000000000000) = (v));}

#define writel(v,a)	{ \
	(*(volatile u32 *)((a)|0x9000000000000000) = (v));}

#define write_cache_l(v,a)	{ \
	(*(volatile u32 *)((a)|0xb000000000000000) = (v));}

#define readc(c)	(*(volatile u8 *)((c)|0x9000000000000000))

#define reads(c)	(*(volatile u16 *)((c)|0x9000000000000000))

#define readl(c)	(*(volatile u32 *)((c)|0x9000000000000000))

#define read_cache_l(c)	(*(volatile u32 *)((c)|0xb000000000000000))

#define phx_write_u8(a,v)  writec(v,a)
#define phx_read_u8(v)	readc(v)
#define phx_write_u16(a,v)  writes(v,a)
#define phx_read_u16(v)	reads(v)
#define phx_write_u32(a,v)  writel(v,a)
#define phx_read_u32(v)	readl(v)


#define phx_write_cache_u32(a,v) write_cache_l(v,a)
#define phx_read_cache_u32(v)	readl(v)

#endif

//if (a == (UART0_BASE_ADDR + CDNS_UART_IER)) printk("ier:%s[%d] = 0x%x\n", __FUNCTION__, __LINE__, v); \
//#if (a == (UART0_BASE_ADDR + CDNS_UART_IDR)) printk("idr:%s[%d] = 0x%x\n", __FUNCTION__, __LINE__, v); \
