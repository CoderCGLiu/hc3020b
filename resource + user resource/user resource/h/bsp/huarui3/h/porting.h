/*
 * porting.h
 *
 *  Created on: 2018-7-5
 *      Author: Administrator
 */

#ifndef PORTING_H_
#define PORTING_H_
#include <sys/types.h>


/**
 * 如果是64位编译器，那么操作的地址必须是64位的， 编译器不会自动进行扩展
 */
#ifdef REWORKS_LP64

#define ADDR_LP64(addr)   ((addr & 0x80000000UL)?(0xffffffff00000000 | (u64)addr):((u64)addr))
/* 原来的驱动中有些地方使用了32位地址，如果第31位为1，需要把高32位扩展成0xffffffff。
 * 在使用8G内存后，虚拟地址会用到0x00000000E0000000等地址，不能再用原来的方法扩展高32位。
 * 临时解决方法是仅扩展0x80000000~0xE0000000之间的地址。后续会淘汰这个宏，不使用地址扩展。*/
#define ADDR2_LP64(addr)   ((((u64)(addr) >= 0x80000000UL) && ((u64)(addr) < 0xE0000000UL))?((0xffffffff00000000 | (u64)(addr))):((u64)(addr)))

#else
#define ADDR_LP64(addr)   (addr)
#define ADDR2_LP64(addr)  (addr)
#endif

#endif /* PORTING_H_ */
