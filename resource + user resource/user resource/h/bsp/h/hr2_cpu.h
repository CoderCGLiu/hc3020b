/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：loongson2f架构相关定义的头文件
 * 
 */
#ifndef _HR2_CPU_H
#define _HR2_CPU_H



/* 华睿2号Cache一致性属性 */
#define HR2_ENTRYLO_CACHE_MSK   0x00000038
#define HR2_ENTRYLO_CACHE_OFFSET  0x3
#define HR2_MMU_ATTR_CACHE_RESERVED1    0x0
#define HR2_MMU_ATTR_CACHE_RESERVED2    0x1
#define HR2_MMU_ATTR_CACHE_UNCACHED     0x2
#define HR2_MMU_ATTR_CACHE_NOCACHEABLE_COHERENT    0x3
#define HR2_MMU_ATTR_CACHE_RESERVED3    0x4
#define HR2_MMU_ATTR_CACHE_RESERVED4    0x5
#define HR2_MMU_ATTR_CACHE_CACHEABLE_COHERENT      0x6
#define HR2_MMU_ATTR_CACHE_RESERVED5    0x7

#define HR2_ENTRYLO_DIRTY_MSK   0x00000004
#define HR2_ENTRYLO_DIRTY_OFFSET  0x2
#define HR2_MMU_ATTR_DIRTY     	0x1

#define HR2_ENTRYLO_VALID_MSK	0x00000002
#define HR2_ENTRYLO_VALID_OFFSET  0x1
#define HR2_MMU_ATTR_VALID     0x1

#define HR2_ENTRYLO_GLOBAL_MSK  0x00000001
#define HR2_ENTRYLO_GLOBAL_OFFSET  0x0
#define HR2_MMU_ATTR_GLOBAL     0x1

#define HR2_ENTRYLO_OPT(cache, dirty,valid, global)   (((cache << HR2_ENTRYLO_CACHE_OFFSET)&HR2_ENTRYLO_CACHE_MSK) | \
	((dirty << HR2_ENTRYLO_DIRTY_OFFSET) & HR2_ENTRYLO_DIRTY_MSK) | ((valid << HR2_ENTRYLO_VALID_OFFSET) & HR2_ENTRYLO_VALID_MSK) | \
	((global << HR2_ENTRYLO_GLOBAL_OFFSET) & HR2_ENTRYLO_GLOBAL_MSK))

#endif /* _HR2_CPU_H */
