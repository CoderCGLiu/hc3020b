/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了SB710芯片内部RTC寄存器定义及访问方式
 * 修改：
 * 		  2013-08-21，张静，创建 
 */

#ifndef __ARCH_RTC_H_
#define __ARCH_RTC_H_

#ifdef __cplusplus
extern "C"
{
#endif
	
#include <time.h>
#include <bsp.h>

extern void sys_64_outb(u8 val,u8 reg);
extern u8 sys_64_inb(u8 reg);
/**
 * 寄存器定义
 */
#define	DS_REG_SEC     	     0x00		/* 秒 寄存器*/
#define	DS_REG_ASEC          0x01		/* 秒定时 寄存器 */
#define	DS_REG_MIN	         0x02		/* 分钟寄存器 */
#define	DS_REG_AMIN	         0x03		/* 分钟定时寄存器 */
#define	DS_REG_HOUR	         0x04		/* 小时寄存器 */
#define	DS_REG_AHOUR         0x05		/* 分钟定时寄存器 */
#define	DS_REG_WDAY	         0x06		/* 星期寄存器 */
#define	DS_REG_DATE	         0x07		/* 日期寄存器 */
#define	DS_REG_MONTH         0x08		/* 月份寄存器 */
#define	DS_REG_YEAR	         0x09		/* 年份寄存器*/
#define	DS_REG_CTLA	         0x0a		/* RTC控制寄存器A */
#define	DS_REG_CTLB	         0x0b		/* RTC控制寄存器B */
#define	DS_REG_CTLC	         0x0c		/* RTC控制寄存器C */
#define	DS_REG_CTLD	         0x0d		/* RTC控制寄存器D */
	
#define DS_REG_ALTCEN        0x32     /* 世纪寄存器 */
#define DS_REG_CEN     		 0x48     /* 世纪寄存器 */
	
#define DS_REG_EXTRAMADDR    0x50     /* RTC外部内存地址端口寄存器 */
#define DS_REG_EXTRAMDATA    0x53     /* RTC外部内存数据端口寄存器*/

#define DS_REG_RTCCLEAR      0x7E     /* RTC时间清除寄存器 */
#define DS_REG_RAMENABLE     0x7F     /* RTC RAM使能寄存器 */

/**
 * CTRLA寄存器定义
 */
#define	DS_CTLA_RS0	         0x01		/* 时钟频率选择 */
#define	DS_CTLA_RS1	         0x02		/* 时钟频率选择*/
#define	DS_CTLA_RS2	         0x04		/* 时钟频率选择 */
#define	DS_CTLA_RS3	         0x08		/* 时钟频率选择 */
#define	DS_CTLA_DV0	         0x10		/* Bank 选择 */
#define	DS_CTLA_UIP	         0x80		

/**
 * CTRLB寄存器定义
 */
#define	DS_CTLB_DSE	         0x01		
#define	DS_CTLB_24	         0x02		/* 小时模式，0-12小时制；1-24小时制 */
#define	DS_CTLB_UIE	         0x10		/* 周期更新中断使能 */
#define	DS_CTLB_AIE	         0x20		/* 定时中断使能 */
#define	DS_CTLB_PIE	         0x40		/* 周期中断使能 */
#define	DS_CTLB_SET	         0x80		/* 设置寄存器使能 */

/**
 * RTC IO地址
 * 通过南桥 LPC ISA Bridge中的0x48寄存器（IO/MEM Port Decode Enable Regsiter 5）设置IO地址使能
 */
#define RTC_ADDR_PORT        0x70
#define RTC_DATA_PORT        0x71
	
/**
 * RTC寄存器读写接口
 */
static inline unsigned char RTC_READ(unsigned char addr)
{
	unsigned char val;
	
	sys_64_outb(addr, RTC_ADDR_PORT);
	val = sys_64_inb(RTC_DATA_PORT);
	
	return val;
}


static inline void RTC_WRITE(unsigned char val, unsigned char addr)
{
	sys_64_outb(addr, RTC_ADDR_PORT);	 
	sys_64_outb(val, RTC_DATA_PORT);
}
	
#ifdef __cplusplus
}
#endif

#endif
