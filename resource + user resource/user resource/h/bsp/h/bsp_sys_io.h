/*! @file bsp_sys_io.h
    @brief ReWorks BSP I/O操作的头文件
    
 * 本文件定义了龙芯3A BSP中I/O操作的外部接口。
 *
 * @version @reworks_version
 * 
 * @see 无
 */
 /*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了io操作要实现的对外接口
 * 修改：
 * 		 1. 2014-03-07，张静，规范BSP 
 */
#ifndef _REWORKS_BSP_SYS_IO_H_
#define _REWORKS_BSP_SYS_IO_H_

#ifdef __cplusplus
extern "C"
{
#endif
	

#include <reworks/types.h>

#define sysInByte 	sys_in_byte
#define sysOutByte 	sys_out_byte
#define sysInWord	sys_in_word
#define sysOutWord 	sys_out_word
#define sysInLong	sys_in_long
#define sysOutLong	sys_out_long
#define sysOutLongString 	sys_out_longstring
#define sysInLongString 	sys_in_longstring
#define sysOutWordString 	sys_out_wordstring
#define sysInWordString 	sys_in_wordstring
#define sysInWordStringRev 	sys_in_word_stringrev

u8   sys_in_byte(u32 address);
void sys_out_byte(u32 address, u8 data);
u16  sys_in_word(u32 address);
void sys_out_word(u32 address, u16 data);
u32  sys_in_long(u32 address);
void sys_out_long(u32 address, u32 data);
void sys_out_longstring(u32 port, u32 *address, int count);
void sys_in_longstring(u32 port, u32 *address, int count);
void sys_out_wordstring(u32 addr,u16 *src,int length);
void sys_in_wordstring(u32 addr,u16 *dest,int length);
void sys_in_word_stringrev(u32 port, u16 *address, int count);

#ifdef __cplusplus
}
#endif

#endif
