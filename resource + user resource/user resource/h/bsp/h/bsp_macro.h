/*******************************************************************************
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义BSP中常用的宏定义
 * 修改：
 * 		 2014-03-07， 张静， 创建 
 */
#ifndef __BSP_MACRO_H__
#define __BSP_MACRO_H__

#ifdef __cplusplus
extern "C"
{
#endif

/* Define Bits */
#ifndef BIT
#define BIT(x)				(1 << (x))
#endif

/* 定义内存大小 */
#define SZ_4KB				(  4 << 10)
#define SZ_8KB				(  8 << 10)
#define SZ_16KB				( 16 << 10)
#define SZ_32KB				( 32 << 10)
#define SZ_64KB				( 64 << 10)
#define SZ_128KB			(128 << 10)
#define SZ_256KB			(256 << 10)
#define SZ_512KB			(512 << 10)
#define SZ_1MB				(  1 << 20)
#define SZ_2MB				(  2 << 20)
#define SZ_4MB				(  4 << 20)
#define SZ_8MB				(  8 << 20)
#define SZ_16MB				( 16 << 20)	
#define SZ_32MB				( 32 << 20)
#define SZ_64MB				( 64 << 20)
#define SZ_128MB			(128 << 20)
#define SZ_256MB			(256 << 20)
#define SZ_512MB			(512 << 20)
#define SZ_1GB				(  1 << 30)		
#define SZ_2GB				(  2 << 30)	

/*IO空间基地址*/
#ifndef REWORKS_LP64
#define LS3A_IO_BASE		(0xb8000000)
#else
#define LS3A_IO_BASE		(0x90000efdfc000000UL)
#endif
/*UART0串口基地址*/
#define LS3A_UART_PORT_BASE (0xffffffffbfe00000) 

/*龙芯3A PCI/PCI-X控制器基地址*/
#define LS3A_PCICTRL_BASE   (0xffffffffbfe00000)

/*龙芯3A PCI/PCI-X控制器 ISR58寄存器*/
#define LS3A_PCI_ISR58   (0x58)

/*龙芯3A 地址窗口寄存器基地址*/
#define LS3A_WIN_BASE   (0x3ff00000)

/**
 * IO空间访问宏定义
 */
#if 0
#define sys_outb(x, y) (*(volatile unsigned char *)(LS3A_IO_BASE+(y)) = x)
#define sys_inb(x)     (*(volatile unsigned char *)(LS3A_IO_BASE+(x)))
#endif
/**
 * TLB相关
 */
#define TLB_ENTRIES	64
#define	K0BASE		0x80000000
#define	K0SIZE		0x20000000
#define	K1BASE		0xA0000000
#define	K1SIZE		0x20000000

#define DRAM_CACHE_VIRT_BASE		K0BASE
#define DRAM_NONCACHE_VIRT_BASE		K1BASE
#define TLBHI_VPN2MASK          0xffffe000
#define TLBLO_CMASK             0x00000038
#define TLBLO_PAGEMASK		0x01ffe000

#define TLBLO_NC                0x00000010 /* uncached */
#define TLBLO_D                 0x4             /* writeable */
#define TLBLO_V                 0x2             /* valid bit */
#define TLBLO_G                 0x1             /* global bit */

#define TLBLO_MODE		(TLBLO_CMASK | TLBLO_D | TLBLO_V | TLBLO_G)

#define TLBLO_PAGEMASK_SIZE	(TLBLO_PAGEMASK | TLBLO_MODE | 0x1000)
#define TLBLO_PFNMASK           0x3fffffc0

/*调试信息开关*/
//#define DEBUG 

#ifdef __cplusplus
}
#endif
#endif
