﻿/*! @file bsp_rtc.h
    @brief ReWorks BSP实时时钟操作的头文件
    
 * 本文件定义了ReWorks系统中RTC实时时钟要实现的对外接口。
 *
 * @version @reworks_version
 * 
 * @see 无
 */
 /*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了RTC实时时钟要实现的对外接口
 * 修改：
 * 		 1. 2011-09-19，唐立三，规范BSP 
 */
#ifndef _REWORKS_BSP_RTC_H_
#define _REWORKS_BSP_RTC_H_

#ifdef __cplusplus
extern "C"
{
#endif
	

#include <time.h>
/**  
 * @defgroup group_os_bsp_rtc 实时时钟模块
 * @ingroup group_os_bsp
 *		
 *  @{ 
 */	

/*! \fn int rtc_read(struct tm *the_tm)
 *	\brief 读取RTC时钟
 *	\param the_tm 要获取的时间
 *
 * \return 成功返回0
 * \return 失败返回-1
 */
int rtc_read(struct tm *the_tm);

/*! \fn int rtc_write(struct tm *the_tm)
 *	\brief 获取RTC时钟
 *	\param the_tm 要设置的时间
 *
 * 请注意在设置tm结构体中的参数时，避免输入非正常的日历时间，如2月30日、2月31日等
 *
 * \return 成功返回0
 * \return 失败返回-1
 */
int rtc_write(struct tm *the_tm);	

/*! \fn int rtc_module_init(void)
 * \brief RTC模块初始化
 * 
 * \param 无
 *
 * \return 成功返回0
 * \return 失败返回-1
 */
int rtc_module_init(void);		

	/* @} */

#ifdef __cplusplus
}
#endif

#endif
