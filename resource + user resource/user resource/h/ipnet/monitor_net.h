/*******************************************************************************
 * 
 * 版权：             中国电子科技集团公司第三十二研究所
 * 描述：            本文件为监测网络吞吐量等I/O信息定义头文件
 * 
 */

#ifndef REWORKS_MONITOR_NET_H_INCLUDED__
#define REWORKS_MONITOR_NET_H_INCLUDED__

#ifdef __cplusplus
extern "C" 
{
#endif
#include <net/if.h>

struct nic_data
{	
	u32	ipackets;		/* 接收的packet数 */
	u32	ierrors;		/* 接收的packet中的错误数 */
	u32	opackets;		/* 发送的packet 数*/
	u32	oerrors;		/* 发送的packet中的错误数 */
	u64	ibytes;		/* 接收的字节数 */
	u64	obytes;		/* 发送的字节数*/
	u32	collisions;		/* CSMA碰撞次数 */
	u32	imcasts;		/* 接收的组播packet数 */
	u32	omcasts;		/* 发送的组播packet数 */
	u32	iqdrops;		/* 丢弃的packet数 */
	u32	noproto;		/* 不支持协议的packet数 */
};


/*-------------------------------------------------------------------------+
|
|  函数名称：nic_io_throughput_get_data
|      描述：name给出待查询网口名，返回其对应的其他data信息
|    输入值：name待查询网口名
|			 返回data待查询对应网口的网络信息。
|    输出值：name网口名对应的网络data信息
|    返回值：0	成功
|            小于0 失败
|				-1 没有找到对应网口名
|				-2 输入的网口名为空
|      其他：
|
+--------------------------------------------------------------------------*/
int nic_io_throughput_get_data(const char *name, struct nic_data *data);

#ifdef __cplusplus
}
#endif
#endif

