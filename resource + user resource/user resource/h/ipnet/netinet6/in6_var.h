/* in6_var.h - IPv6 interface definitions */

#ifndef _REWORKS__NETINET6_IN6_VAR_H_
#define _REWORKS__NETINET6_IN6_VAR_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <net/if.h>        /* needed for IFNAMSIZ */

#include <netinet6/in6.h>  //wn 2013.1.24
	
/*
 * pltime/vltime are just for future reference (required to implements 2
 * hour rule for hosts).  they should never be modified by nd6_timeout or
 * anywhere else.
 *	userland -> kernel: accept pltime/vltime
 *	kernel -> userland: throw up everything
 *	in kernel: modify preferred/expire only
 */
struct in6_addrlifetime {
	time_t ia6t_expire;	/* valid lifetime expiration time */
	time_t ia6t_preferred;	/* preferred lifetime expiration time */
	u_int32_t ia6t_vltime;	/* valid lifetime */
	u_int32_t ia6t_pltime;	/* prefix lifetime */
};

struct	in6_ifreq {
	char	ifr_name[IFNAMSIZ];          /* if name, e.g. "eth0" */
	union {
		struct	sockaddr_in6 ifru_addr;     /* address */
		struct	sockaddr_in6 ifru_dstaddr;  /* other end of p-to-p link */
		unsigned	ifru_prefixlen;         /* address prefix length */
	} ifr_ifru;
};

struct	in6_aliasreq {
	char	ifra_name[IFNAMSIZ];
	struct	sockaddr_in6 ifra_addr;      /* The address to set */
	struct	sockaddr_in6 ifra_dstaddr;   /* Peer address for PPP interfaces */
	struct	sockaddr_in6 ifra_prefixmask; /* Mask to get the nework ID */
	int	ifra_flags;                       /* IN6_IFF_xxx flags */
	struct in6_addrlifetime ifra_lifetime;
};


#define IN6_IFF_TEMPORARY	0x01	/* temporary (anonymous) address. */
#define IN6_IFF_TENTATIVE	0x02	/* tentative address */
#define IN6_IFF_HOMEADDRESS	0x04	/* MIP6 home address */
#define IN6_IFF_DEPRECATED	0x08	/* deprecated address */
#define IN6_IFF_AUTOMATIC	0x10	/* create automatically by IPNET */ 
#define IN6_IFF_AUTONOMOUS	0x20	/* create based on prefix adv */ 
#define IN6_IFF_ANYCAST		0x40	/* anycast address */

#ifdef __cplusplus
}
#endif

#endif /* _NETINET6_IN6_VAR_H_ */
