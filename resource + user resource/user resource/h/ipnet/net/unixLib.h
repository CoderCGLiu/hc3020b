#ifndef _REWORKS_NET_UNIX_LIB_H_
#define _REWORKS_NET_UNIX_LIB_H_

#ifdef __cplusplus
extern "C" {
#endif

//#include <sys/times.h>
//#include <semLib.h> 

#define MALLOC(space, cast, size, type, flags) { \
    (space) = (cast) netClusterGet (_pNetSysPool,                 \
                                    netClPoolIdGet (_pNetSysPool, \
				                    size, TRUE)); \
}						 

#define FREE(addr, type) { \
    netClFree (_pNetSysPool, (unsigned char *) addr); \
}			   

#define DATA_TO_MBLK(pBuf)	\
     (*((struct mbuf **)((char *)(pBuf) - sizeof(struct mbuf **))))
            
#if defined(__STDC__) || defined(__cplusplus)

extern void	panic(const char *, ...);

#else	/* __STDC__ */

extern void 	panic ();

#endif	/* __STDC__ */

/*
 * BSD defines wakeup() to wake up all tasks sleeping on
 * the specified channel. wakeup_one() wakes up a single process
 * (more or less). VxWorks' tsleep() and ksleep() have been modified to
 * give up splSemId and take the awaited semaphore atomically, which is
 * necessary whether semFlush() or semGive() is used.
 * Note, VxWorks has in the past incorrectly made wakeup() equivalent
 * to semGive() rather than semFlush().
 */

//#define wakeup(semId) ((void)semFlush((semId)))
//#define wakeup_one(semId) ((void)semGive((semId)))//yzf 2013.10.10 去掉vx接口的sem 这两个宏没有地方使用

#ifdef __cplusplus
}
#endif

#endif 
