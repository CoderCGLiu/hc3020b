#ifndef _REWORKS_NET_IF_ARP_H_
#define	_REWORKS_NET_IF_ARP_H_

#include <sys/socket.h> 
#include <netinet/in.h> 
#include <net/if_dl.h>
#include <net/if.h>

#ifdef __cplusplus
 extern "C" {
#endif
     
/*
 * Address Resolution Protocol.
 */
struct	arphdr {
	u_short	ar_hrd;		/* format of hardware address */
#define ARPHRD_ETHER 	   1	/* ethernet hardware format */
#define ARPHRD_EETHER 	   2	/* experimental ethernet format */
#define ARPHRD_AX25        3    /* AX.25 Level 2 */
#define ARPHRD_PRONET      4    /* PROnet token ring */
#define ARPHRD_CHAOS       5    /* Chaosnet */
#define ARPHRD_IEEE802	   6	/* token-ring hardware format */
#define ARPHRD_ARCNET	   7	/* arcnet hardware format */
#define ARPHRD_HYPERCH     8    /* Hyper channel */
#define ARPHRD_LANSTAR     9    /* LanStar */
#define ARPHRD_ULTRALINK  13    /* UltraLink */
#define ARPHRD_FRELAY 	  15	/* frame relay hardware format */
#define ARPHRD_DLCI 	  ARPHRD_FRELAY
#define ARPHRD_ATM        19    /* ATM */
#define ARPHRD_METRICOM   23    /* Metricom STRIP (new IANA id) */
#define ARPHRD_IEEE1394   24    /* IEEE 1394 IPv4 - RFC 2734 */
#define ARPHRD_EUI64      27    /* EUI-64 */
	u_short	ar_pro;		/* format of protocol address */
	u_char	ar_hln;		/* length of hardware address */
	u_char	ar_pln;		/* length of protocol address */
	u_short	ar_op;		/* one of: */
#define	ARPOP_REQUEST	1	/* request to resolve address */
#define	ARPOP_REPLY	2	/* response to previous request */
#define	ARPOP_REVREQUEST 3	/* request protocol address given hardware */
#define	ARPOP_REVREPLY	4	/* response giving protocol address */
#define ARPOP_INVREQUEST 8 	/* request to identify peer */
#define ARPOP_INVREPLY	9	/* response identifying peer */
/*
 * The remaining fields are variable in size,
 * according to the sizes above.
 */
#ifdef COMMENT_ONLY
	u_char	ar_sha[];	/* sender hardware address */
	u_char	ar_spa[];	/* sender protocol address */
	u_char	ar_tha[];	/* target hardware address */
	u_char	ar_tpa[];	/* target protocol address */
#endif
};

#define ar_sha(ap)	(((caddr_t)((ap)+1)) +   0)
#define ar_spa(ap)	(((caddr_t)((ap)+1)) +   (ap)->ar_hln)
#define ar_tha(ap)	(((caddr_t)((ap)+1)) +   (ap)->ar_hln + (ap)->ar_pln)
#define ar_tpa(ap)	(((caddr_t)((ap)+1)) + 2*(ap)->ar_hln + (ap)->ar_pln)

#define arphdr_len2(ar_hln, ar_pln)					\
	(sizeof(struct arphdr) + 2*(ar_hln) + 2*(ar_pln))
#define arphdr_len(ap)	(arphdr_len2((ap)->ar_hln, (ap)->ar_pln))

/*
 * ARP ioctl request
 */
struct arpreq {
	struct	sockaddr arp_pa;		/* protocol address */ 
	struct	sockaddr_dl arp_ha;		/* hardware address */
	int	arp_flags;			/* flags */
};
/*  arp_flags and at_flags field values */
#define ATF_COM		0x02	/* completed entry (enaddr valid) */
#define	ATF_PERM	0x04	/* permanent entry */
#define	ATF_PUBL	0x08	/* publish entry (respond for other host) */
#define ATF_GRAT	0x100	/* send gratuitous ARP request */
#define ATF_BLACKHOLE	0x200	/* set blackhole flag on entry */

extern u_char	etherbroadcastaddr[6];

#ifdef __cplusplus
}
#endif
    
#endif 
