﻿#ifndef _REWORKS_NET_TFTP_H_
#define _REWORKS_NET_TFTP_H_

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @defgroup group_os_net_tftp TFTP客户端用户接口
 * @ingroup group_os_net
 * 
 * @{
 * 
 */


/*!
 * \fn int tftp(char *host, char *remote_f, char *cmd, char *mod, char *local_f)
 * @brief 该接口通过TFTP协议传输文件
 *
 *	例如从TFTP服务器"192.168.1.100"获取ASCII文件"/folk/vw/xx.yy"，并且将其存储于本地文件"localfile"。
 *  参考代码如下： tftp ("192.168.1.100", 0, "/folk/vw/xx.yy", "get", "ascii", "localfile")。
 * 
 * @param	host		TFTP服务器IP地址
 * @param	remote_f	远程文件名
 * @param	cmd			TFTP命令，可使用"put或者"get"表示上传文件或者下载文件。
 * @param	mod			传输模式，支持"ascii"、"netascii"、"binary"、"image"或者"octet"
 * @param	local_f		本地文件名
 *
 * @return	0			成功
 * @return  -1			失败
 *
 */
extern int tftp(char *host, char *remote_f, char *cmd, char *mod, char *local_f);


/* @} */

#ifdef __cplusplus
}
#endif

#endif 
