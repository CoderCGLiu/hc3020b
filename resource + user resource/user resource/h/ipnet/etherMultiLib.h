/* etherMultiLib.h - definitions for the Ethernet multicast library */
 
/*
DESCRIPTION

This header file describes data structures and function prototypes for
the ethernet multicast library.

INCLUDE FILES:
*/

#ifndef __INCetherMultiLibh
#define __INCetherMultiLibh

/* includes */
#include <end.h>
#include <netinet/if_ether.h>

#ifdef __cplusplus
extern "C" {
#endif

/* defints */

/* typedefs */

/* globals */

/* locals */

/* forward declarations */

#if defined(__STDC__) || defined(__cplusplus)

extern int etherMultiAdd(IPLIST *pList, char* pAddress );
extern int etherMultiDel(IPLIST* pList, char* pAddress);
extern int etherMultiGet(IPLIST *pList, MULTI_TABLE* pTable);
#else	/* __STDC__ */

extern int etherMultiAdd();
extern int etherMultiDel();
extern int etherMultiGet();
#endif /* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* __INCetherMultiLibh */
