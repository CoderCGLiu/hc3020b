/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了ReWorks 网络协议栈缓冲区所需的类型和接口定义
 * 修改：
 * 		 2013-9-25，规范
 * 
 */
#ifndef _REWORKS_NET_BUF_LIB_H_
#define _REWORKS_NET_BUF_LIB_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <autoconf.h>  
#include <ip/cfgDefs.h> 
//#include <ip/netffs.h>
#ifdef _WRS_CONFIG_SMP 
#include <spinlock.h>
#endif
#include <semaphore.h>
#include <net_types.h>
#include <errno.h>
#include <sys/vwModNum.h>
//#include "../../include/drv/intLib.h"

/* 状态码 */
#define	S_netBufLib_MEMSIZE_INVALID		(M_netBufLib | 1)
#define	S_netBufLib_CLSIZE_INVALID		(M_netBufLib | 2)
#define	S_netBufLib_NO_SYSTEM_MEMORY		(M_netBufLib | 3)
#define	S_netBufLib_MEM_UNALIGNED		(M_netBufLib | 4)
#define	S_netBufLib_MEMSIZE_UNALIGNED		(M_netBufLib | 5)
#define	S_netBufLib_MEMAREA_INVALID		(M_netBufLib | 6)
#define S_netBufLib_MBLK_INVALID		(M_netBufLib | 7)
#define S_netBufLib_NETPOOL_INVALID		(M_netBufLib | 8)
#define S_netBufLib_INVALID_ARGUMENT		(M_netBufLib | 9)
#define S_netBufLib_NO_POOL_MEMORY		(M_netBufLib | 10)
#define S_netBufLib_LIB_NOT_INITIALIZED		(M_netBufLib | 11)
#define S_netBufLib_POOL_FEATURE_NOT_SUPPORTED	(M_netBufLib | 12)
#define S_netBufLib_DUPLICATE_POOL 		(M_netBufLib | 13)
#define S_netBufLib_POOL_RELEASED 		(M_netBufLib | 14)
#define S_netBufLib_POOL_RELEASE_IGNORE		(M_netBufLib | 15)

/* Tuple 长度相关宏 */
#define TUP_LEN_GET(pMblk)	     ((int) (pMblk)->mBlkHdr.mLen)
#define TUP_LEN_SET(pMblk,length)    ((pMblk)->mBlkHdr.mLen = (length))
#define TUP_LEN_ZERO(pMblk)          ((pMblk)->mBlkHdr.mLen = 0)
#define TUP_CL_LEN_GET(pMblk)        ((int) (pMblk)->pClBlk->clSize)
#define TUP_CL_AVIL_LEN_GET(pMblk)   ((int) (pMblk)->pClBlk->clSize - \
                                      ((int) (pMblk)->mBlkHdr.mLen) - \
                                      (((int) (pMblk)->mBlkHdr.mData) - \
                                       ((int)(pMblk)->pClBlk->clNode.pClBuf))) 

/* Next refers to the next Tuple in a Tuple chain */
#define TUP_NEXT_GET(pMblk)		((M_BLK_ID) (pMblk)->mBlkHdr.mNext)
#define TUP_NEXT_SET(pMblk,pNext)	((pMblk)->mBlkHdr.mNext = \
                                         (M_BLK_ID) (pNext))
#define TUP_NEXT_NULL(pMblk)		((pMblk)->mBlkHdr.mNext = \
                                         (M_BLK_ID) NULL)

/* Next Packet refers to the next Tuple (List of Tuples) */
#define TUP_NEXT_PK_GET(pMblk)		((M_BLK_ID) (pMblk)->mBlkHdr.mNextPkt)
#define TUP_NEXT_PK_SET(pMblk,pNext)	((pMblk)->mBlkHdr.mNextPkt = \
                                         (M_BLK_ID) (pNext))
#define TUP_NEXT_PK_NULL(pMblk)		((pMblk)->mBlkHdr.mNextPkt = \
                                         (M_BLK_ID) NULL)

/* mBlk type */
#define TUP_TYPE_GET(pMblk)		((pMblk)->mBlkHdr.mType)
#define TUP_TYPE_SET(pMblk,type)	((pMblk)->mBlkHdr.mType = type)

/* Cluster Buffer Address */
#define TUP_BUF_GET(pMblk)		((pMblk)->mBlkHdr.mData)
#define TUP_BUF_SET(pMblk,addr)		((pMblk)->mBlkHdr.mData = addr)

#define NET_POOL_NAME_SZ	(16) /* Length of pool name */

/* netBufLib Attributes required for buffer migration */

#define NB_ALIGN_OFFSET		0U			/* Bits 0, 1 & 2 */
#define NB_ALIGN_MASK		(0x7U << NB_ALIGN_OFFSET)
#define NB_ALIGN_NONE		(0x0U << NB_ALIGN_OFFSET) 
#define NB_ALIGN_INT		(0x1U << NB_ALIGN_OFFSET)     
#define NB_ALIGN_CACHE		(0x2U << NB_ALIGN_OFFSET)     
#define NB_ALIGN_PAGE		(0x3U << NB_ALIGN_OFFSET)     

#define NB_SHARABLE		0x0400U			/* Bit 10 */  
#define NB_ISR_SAFE		0x0800U			/* Bit 11 */

#define NB_MODE_OFFSET		12U			/* Bits 12, 13 */
#define NB_MODE_MASK		(0x3U << NB_MODE_OFFSET)
#define NB_MODE_5_5BKWRDS	(0x0U << NB_MODE_OFFSET)

/*
 * Pseudo-attribute bit, not saved with other attributes.
 * If this bit is on in a call to netBufCreate(), the implicit
 * leading cluster pad space is not added for qualifying
 * driver pools.
 */
#define NB_NO_LEADING_SPACE	(1U << 15)

/* Flag bits used in pNetPool->flag */
#define NB_CREATE		0x01	/* netPool created via netPoolCreate */
#define NB_RELEASE		0x02	/* netPool is in a release state */
#define NB_RELEASE_START	0x04	/* started the release state */
#define NB_DESTROY_START	0x08	/* started the delete process */

/* netPoolRlease() 宏 */
#define NET_REL_IN_TASK		0x00	/* Release netPool in task context */
#define NET_REL_IN_CONTEXT	0x01	/* Release netPool in call context */
#define NET_REL_NAME		"tPoolRel"	/* Task name */
#define NET_REL_PRI		netPoolRelPri	/* see: netBufLib.c */
#define NET_REL_OPT		0		/* task options */
#define NET_REL_STACK		5000		/* task stack */

/* 有效的属性组合 */
/* Int Aligned, Sharable, ISR Safe */
#define ATTR_AI_SH_ISR 		(NB_ALIGN_INT   | NB_ISR_SAFE | NB_SHARABLE | \
                                 NB_MODE_5_5BKWRDS)

#define ATTR_AC_SH_ISR 		(NB_ALIGN_CACHE | NB_ISR_SAFE | NB_SHARABLE | \
                                 NB_MODE_5_5BKWRDS)

/* Int Aligned, Private, ISR Safe */
#define ATTR_AI_ISR    		(NB_ALIGN_INT   | NB_ISR_SAFE | \
                                 NB_MODE_5_5BKWRDS)

#define ATTR_AC_ISR    		(NB_ALIGN_CACHE | NB_ISR_SAFE | \
                                 NB_MODE_5_5BKWRDS)

/* pMemReqRtn 函数的缓冲区类型 */
#define NB_BUFTYPE_CLUSTER	0  /* clusters */
#define NB_BUFTYPE_M_BLK	1  /* M_BLKs */
#define NB_BUFTYPE_CL_BLK	2  /* CL_BLKs */

#define MAX_MBLK_TYPES			256	/* max number of mBlk types */

/* cluster 定义 */
#define CL_LOG2_16			4
#define CL_LOG2_32			5
#define CL_LOG2_64			6
#define CL_LOG2_128			7
#define CL_LOG2_256			8
#define CL_LOG2_512			9
#define CL_LOG2_1024			10
#define CL_LOG2_2048			11
#define CL_LOG2_4096	 		12
#define CL_LOG2_8192 			13
#define CL_LOG2_16384			14
#define CL_LOG2_32768			15
#define CL_LOG2_65536			16

#define CL_SIZE_16			16
#define CL_SIZE_32			32
#define CL_SIZE_64			64
#define CL_SIZE_128			128
#define CL_SIZE_256			256
#define CL_SIZE_512			512
#define CL_SIZE_1024			1024
#define CL_SIZE_2048			2048
#define CL_SIZE_4096			4096
#define CL_SIZE_8192			8192
#define CL_SIZE_16384			16384
#define CL_SIZE_32768			32768
#define CL_SIZE_65536			65536

#define CL_LOG2_MIN			CL_LOG2_16
#define CL_LOG2_MAX			CL_LOG2_65536
#define CL_SIZE_MAX			(1 << CL_LOG2_MAX)
#define CL_SIZE_MIN			(1 << CL_LOG2_MIN)
#define CL_INDX_MIN			0
#define CL_INDX_MAX			(CL_LOG2_MAX - CL_LOG2_MIN)
#define CL_TBL_SIZE			(CL_INDX_MAX + 1)

#define CL_LOG2_TO_CL_INDEX(x)		((x) - CL_LOG2_MIN)
#define CL_LOG2_TO_CL_SIZE(x)		(1 << (x))
#define SIZE_TO_LOG2(size)		(FFS(((UINT32)(size))))
#define CL_SIZE_TO_CL_INDEX(clSize) 	(SIZE_TO_LOG2(clSize) - CL_LOG2_MIN)

/*
 * Minimum cluster alignment
 */
#define CL_ALIGN_MIN	4

/* mBlk 类型 */
#define	MT_FREE		0	/* should be on free list */
#define	MT_DATA		1	/* dynamic (data) allocation */
#define	MT_HEADER	2	/* packet header */
#define	MT_SOCKET	3	/* socket structure */
#define	MT_PCB		4	/* protocol control block */
#define	MT_RTABLE	5	/* routing tables */
#define	MT_HTABLE	6	/* IMP host tables */
#define	MT_ATABLE	7	/* address resolution tables */
#define	MT_SONAME	8	/* socket name */
#define MT_ZOMBIE       9       /* zombie proc status */
#define	MT_SOOPTS	10	/* socket options */
#define	MT_FTABLE	11	/* fragment reassembly header */
#define	MT_RIGHTS	12	/* access rights */
#define	MT_IFADDR	13	/* interface address */
#define MT_CONTROL	14	/* extra-data protocol message */
#define MT_OOBDATA	15	/* expedited data  */
#define	MT_IPMOPTS	16	/* internet multicast options */
#define	MT_IPMADDR	17	/* internet multicast address */
#define	MT_IFMADDR	18	/* link-level multicast address */
#define	MT_MRTABLE	19	/* multicast routing tables */
#define	MT_TAG		20	/* volatile metadata associated to pkts */

#define NUM_MBLK_TYPES	21	/* number of mBlk types defined */

/* mBlk flags */
#define	M_EXT		0x0001	/* has an associated cluster */
#define	M_PKTHDR	0x0002	/* start of record */
#define	M_EOR		0x0004	/* end of record */

/* flags 0x0008 - 0x0080 are used in mbuf.h */

/* mBlk pkthdr flags, also in mFlags */
#define	M_BCAST		0x0100	/* send/received as link-level broadcast */
#define	M_MCAST		0x0200	/* send/received as link-level multicast */

/* flags 0x0400 - 0x1000 are used in mbuf.h */
#define M_FORWARD       0x2000  /* to be fast forwarded */
#define M_PROXY         0x4000  /* broadcast forwarded through proxy */
#define M_PROMISC	0x8000	/* indicates unipromisc for multi receive */
#define M_HEADER	M_PROMISC /* NPT transmit, full header present */

#define M_EOB           M_EOR

/* flags to mClGet/mBlkGet */
#define	M_DONTWAIT	1	/* don't wait if buffers not available */
#define	M_WAIT		0	/* wait if buffers not available */

/* length to copy to copy all data from the mBlk chain*/
#define	M_COPYALL	1000000000

/* check to see if an mBlk is associated with a cluster */
#define M_HASCL(pMblk)	((pMblk)->mBlkHdr.mFlags & M_EXT)

#define	M_BLK_SZ	sizeof(struct mBlk) 	/* size of an mBlk */
#define M_BLK_SZ_ALIGNED	64		/* See linkBufPool */
 #ifndef MSIZE
#define	MSIZE		M_BLK_SZ	/* size of an mBlk */
#endif

#define CL_BLK_SZ	sizeof(struct clBlk)	/* size of cluster block */
#define CL_BLK_SZ_ALIGNED	64		/* See linkBufPool */

/* macro to get to the netPool pointer from the mBlk */

#define MBLK_TO_NET_POOL(pMblk) \
    (*(NET_POOL_ID *)((char *)pMblk - sizeof(NET_POOL_ID *)))
#define CLBLK_TO_NET_POOL(pClBlk) 	(pClBlk->pNetPool)

/* macro to get to the cluster pool ptr from the cluster buffer */

#define CL_BUF_TO_CL_POOL(pClBuf) \
    (*(CL_POOL_ID *)((char *)pClBuf - sizeof(CL_POOL_ID *)))

/* macros for accessing the functions of net pool directly */
#define poolInit(poolId,pMclBlkConfig,pClDescTbl,tblNumEnt,fromKheap)	\
    		(*(((NET_POOL_ID)(poolId))->pFuncTbl->pInitRtn)) 	\
                 ((poolId), (pMclBlkConfig), (pClDescTbl), (tblNumEnt), \
		  (fromKheap))
    
#define mBlkFree(poolId,pMblk)						\
    		(*(((NET_POOL_ID)(poolId))->pFuncTbl->pMblkFreeRtn)) 	\
                 ((poolId), (pMblk))

#define clBlkFree(poolId,pClBlk)					\
    		(*(((NET_POOL_ID)(poolId))->pFuncTbl->pClBlkFreeRtn)) 	\
                 ((pClBlk))
                    
#define mBlkClFree(poolId,pMblk)					\
    		(*(((NET_POOL_ID)(poolId))->pFuncTbl->pMblkClFreeRtn)) 	\
                 ((poolId), (pMblk))

#define mBlkGet(poolId,canWait,type)					\
                 (*(((NET_POOL_ID)(poolId))->pFuncTbl->pMblkGetRtn)) 	\
                 ((poolId), (canWait), (type))

#define clBlkGet(poolId,canWait)					\
                 (*(((NET_POOL_ID)(poolId))->pFuncTbl->pClBlkGetRtn)) 	\
                 ((poolId), (canWait))
                    
#ifdef NETBUF_DEBUG
#define clusterGet(poolId,pClPool)					\
	  _dbgClusterGet (poolId, pClPool, FID, __FILE__, __LINE__)

#define clFree(poolId,pClBuf)					\
	  _dbgClFree (poolId, pClBuf, FID, __FILE__, __LINE__)
#else
#define clusterGet(poolId,pClPool)					\
    		(*(((NET_POOL_ID)(poolId))->pFuncTbl->pClGetRtn)) 	\
                 ((poolId), (pClPool))

#define clFree(poolId,pClBuf)						\
    		(*(((NET_POOL_ID)(poolId))->pFuncTbl->pClFreeRtn)) 	\
                 ((poolId), (pClBuf))
#endif /* NETBUF_DEBUG */
                    
#define mClGet(poolId,pMblk,bufSize,canWait,noSmaller)			\
    		(*(((NET_POOL_ID)(poolId))->pFuncTbl->pMblkClGetRtn)) 	\
                 ((poolId), (pMblk), (bufSize), (canWait), (noSmaller))

#define clPoolIdGet(poolId,bufSize,noSmaller)				\
    		(*(((NET_POOL_ID)(poolId))->pFuncTbl->pClPoolIdGetRtn))	\
                 ((poolId), (bufSize), (noSmaller))

#define netMemReq(poolId, type, num, size)				\
		(((NET_POOL_ID)(poolId))->pFuncTbl->pMemReqRtn		\
		 ((type), (num), (size)))

#define ML_SIZE \
    ROUND_UP(sizeof(M_BLK) + sizeof(CL_BLK) + sizeof(NET_POOL_ID), \
             NETBUF_ALIGN)
#define CL_SIZE(x) (ROUND_UP((x), NETBUF_ALIGN))

#ifndef NB_FULL_BKWARD_MODE
#define NB_FULL_BKWARD_MODE
#endif

#ifdef _WRS_CONFIG_SMP
#ifdef _WRS_CONFIG_DETERMINISTIC
#define NET_POOL_LOCK(x, k) do { (void)(k); spin_lock_rt_dtm ((x)); } while ((0))
#define NET_POOL_UNLOCK(x, k) do { spin_unlock_rt_dtm ((x)); (void)(k); } while ((0))
#define NET_POOL_LOCK_INIT(x) do { spin_lock_rt_dtm_init ((x)); } while ((0))
#else
#define NET_POOL_LOCK(x, k) do { spin_lock_rt((x)); k = (x)->level; } while ((0))
#define NET_POOL_UNLOCK(x, k) do {(x)->level = k; spin_unlock_rt ((x)); } while ((0))
#define NET_POOL_LOCK_INIT(x) do { spin_lock_init ((x)); } while ((0))
#endif /* _RWS_CONFIG_DETERMINISTIC */
#else
#define NET_POOL_LOCK(x, k) do {k = INT_CPU_LOCK ();} while ((0))
#define NET_POOL_UNLOCK(x, k) do {INT_CPU_UNLOCK (k);} while ((0))
#define NET_POOL_LOCK_INIT(x) do { } while ((0))
#endif /* _WRS_CONFIG_SMP */

/*
 * 用于配置一个 netPool中的 mBlks 和 cluster blocks 的数量
 */
typedef struct
    {
    int		mBlkNum;		/* number of mBlks */
    int		clBlkNum;		/* number of cluster Blocks */
    char * 	memArea;		/* pre allocated memory area */
    size_t	memSize;		/* pre allocated memory size */
    } M_CL_CONFIG;

/*
 *  定义网络代码利用 clusters 的方式
 */
typedef struct clDesc
    {
    int		clSize;			/* cluster type */
    int		clNum;			/* number of clusters */
    char *	memArea;		/* pre allocated memory area */
    size_t	memSize;		/* pre allocated memory size */
    } CL_DESC; 

/*
 * 用于将配置参数传送给 mbinit，定义系统和数据网络内存池的配置
 */
typedef struct netPoolParams
    {
    CFG_DATA_HDR  cfgh;                  /* standard cfg header */

    M_CL_CONFIG * sysMblkClBlkConf;      /* mblks and clblks for system pool */
    CL_DESC *     sysClDesc;             /* cluster info for system pool */
    unsigned int  sysClDescNum;          /* no. of clDesc for system pool */

    M_CL_CONFIG * pktMblkClBlkConf;      /* mblks and clblks for data pool */
    CL_DESC *     pktClDesc;             /* cluster info for data pool */
    unsigned int  pktClDescNum;          /* no. of clDesc for data pool */
    RE_FUNCPTR       cfg_privInitSysctl;    /* Do sysctl initialization ? */
    } NET_POOL_CONFIG_PARAMS;

/* 定义了 cluster */
typedef struct clBuff
    {
    struct clBuff *	pClNext; 	/* pointer to the next clBuff */
    } CL_BUF;

typedef CL_BUF * CL_BUF_ID; 

/* 定义了每个 cluster 池 */
typedef struct clPool
    {
    int			clSize;		/* cluster size */
    int			clLg2;		/* cluster log 2 size */
    int			clNum; 		/* number of clusters */
    int			clNumFree; 	/* number of clusters free */
    unsigned int	clUsage;	/* number of times used */
    int			clMinAlloc;	/* smallest block size requested */
    int			clMaxAlloc;	/* largest block size requested */
    unsigned long	clAllocFail;	/* # of cluster allocation failures */
    CL_BUF_ID		pClHead;	/* pointer to the cluster head */
    struct netPool *	pNetPool;	/* pointer to the netPool */
    /* 
     * Define the following unconditionally so that we don't run into
     * problems if some part of the code is compiled with NETBUF_DEBUG and
     * other part without.
     */
#if 1 /* NETBUF_DEBUG */
    CL_BUF_ID		pClTail;	/* pointer to the last free cluster */
    char *		pBackPointer;	/* back pointer to pool */
    char *		pStartMem;	/* points to start of first cluster */
    char *		pEndMem;	/* points to start of last cluster */
    char *		pLoanedOut;	/* array of cluster loan status */
    unsigned int        clPaddedSize;   /* size of padded cluster */
#endif /* NETBUF_DEBUG */
    } CL_POOL; 

#if 1 /* NETBUF_DEBUG */
#define IS_CLPOOL_OK(cp) \
    ((cp) != NULL && (cp)->pBackPointer == (char *)(cp) && \
     (((cp)->pClHead == NULL && (cp)->pClTail == NULL) ||\
      ((cp)->pClHead != NULL && (cp)->pClTail != NULL && \
       (cp)->pClTail->pClNext == NULL)))

/*
 * Check if cluster is in range. It qualifies if it lies between the
 * start and end of the pool and is on the cluster boundary
 */
#define IS_CLUSTER_IN_RANGE(cp, cl) ((char *)(cl) >= (cp)->pStartMem && \
                                  (char *)(cl) <= (cp)->pEndMem && \
                                  ((((char *)(cl) - (cp)->pStartMem) % \
				    (cp)->clPaddedSize == 0)))

/* Given the cluster address, get an index into the loanedOut array */
#define CL_NDX(cp, cl) (((char *)cl - (cp)->pStartMem) / (cp)->clPaddedSize )

#endif

typedef CL_POOL * CL_POOL_ID; 

/* header at beginning of each mBlk */
typedef struct mHdr
    {
    struct mBlk *	mNext;		/* next buffer in chain */
    struct mBlk *	mNextPkt;	/* next chain in queue/record */
    char *		mData;		/* location of data */
    int			mLen;		/* amount of data in this mBlk */
    unsigned short		mType;		/* type of data in this mBlk */
    unsigned short		mFlags;		/* flags; see below */
    unsigned short		reserved; 
    unsigned char		offset1;	/* network service offset */
    unsigned char		offset2;	/* TX checksum offload: offset from
					   start of IP header to TL header */
    } M_BLK_HDR;

/* record/packet header in first mBlk of chain; valid if M_PKTHDR set */
typedef struct mBlk     M_BLK;
typedef M_BLK * 	M_BLK_ID;

typedef struct	pktHdr
    {
    void *              rcvif;		/* rcv interface */
    void *              header;         /* pointer to packet header */

    int			len;		/* total packet length */
    UINT32              csum_flags;     /* checksum flags */
    UINT32              csum_data;      /* used by csum routines */
    UINT16		qnum;		/* priority queueing number */
    UINT16		vlan;		/* vlan control information */

    M_BLK_ID            aux;            /* extra data buffer */  
    /* 
     * This is a private field to save (IPv4/v6) header position in mbuf for 
     * fast access by altq later.
     */
    void	*altq_hdr;
    } M_PKT_HDR;

typedef union clBlkList
    {
    struct clBlk * 	pClBlkNext;	/* pointer to the next clBlk */
    char * 		pClBuf;		/* pointer to the buffer */
    } CL_BLK_LIST;
    
/* description of external storage mapped into mBlk, valid if M_EXT set */
typedef struct clBlk
    {
    CL_BLK_LIST 	clNode;		/* union of next clBlk, buffer ptr */
    unsigned int		clSize;		/* cluster size */
    int			clRefCnt;	/* reference count of the cluster */
    RE_FUNCPTR		pClFreeRtn;	/* pointer to cluster free routine */
    int			clFreeArg1;	/* free routine arg 1 */
    int			clFreeArg2;	/* free routine arg 2 */
    int			clFreeArg3;	/* free routine arg 3 */
    struct netPool *	pNetPool;	/* pointer to the netPool */
    } CL_BLK;

/* mBlk */
struct mBlk
    {
    M_BLK_HDR 	mBlkHdr; 		/* header */
    M_PKT_HDR	mBlkPktHdr;		/* pkthdr */
    CL_BLK *	pClBlk;			/* pointer to cluster blk */
    };

/* mBlk statistics used to show data pool by show routines */
typedef struct mbstat
    {
	unsigned long	mNum;			/* mBlks obtained from page pool */
    unsigned long	mDrops;			/* times failed to find space */
    unsigned long	mWait;			/* times waited for space */
    unsigned long	mDrain;			/* times drained protocols for space */
    unsigned long	mTypes[256];		/* type specific mBlk allocations */
    } M_STAT;

typedef struct _M_LINK
    {
    M_BLK	mBlk;
    CL_BLK	clBlk;
    } M_LINK;

#define CL_BLK_TO_M_LINK(pClBlk) \
        (M_LINK *) ((char *)(pClBlk) - OFFSET (M_LINK, clBlk))

#define NETBUF_ALIGN 64
#define NETBUF_LEADING_SPACE 64

typedef CL_BLK *		CL_BLK_ID;	/* Cluster Block ID */
typedef struct netPool 		NET_POOL;	/* Pool ID */
typedef struct poolFunc 	POOL_FUNC;	/* Pool Function Table */
typedef NET_POOL * 		NET_POOL_ID;	/* Pool ID (pointer) */
typedef M_LINK *		M_LINK_ID;	/* mBlk Cluster Block set */
typedef struct poolAttr * 	ATTR_ID;	/* Pool Attribute ID */
typedef struct iovec 	 	IOV;		/* IOV structure (See uio.h) *///shjj2012.5.18
typedef struct poolRel	 	POOL_REL;	/* netPoolRelease Support */

typedef struct bufVirToPhy
    {
    M_BLK_ID	pMBlk;		/* Associated mBlk */
    char *	pVAddr;		/* Virtual Address */
    char *	pPAddr;		/* Physical Address */
    int		length;		/* length of segment */
    } BUF_VTOP; 

struct	poolFunc			/* POOL_FUNC */
    {
    /* pointer to the pool initialization routine */
    int	(*pInitRtn) (NET_POOL_ID pNetPool, M_CL_CONFIG * pMclBlkConfig,
                             CL_DESC * pClDescTbl, int clDescTblNumEnt,
			     int fromKheap);

    /* pointer to mBlk free routine */
    void	(*pMblkFreeRtn) (NET_POOL_ID pNetPool, M_BLK_ID pMblk);

    /* pointer to cluster Blk free routine */
    void	(*pClBlkFreeRtn) (CL_BLK_ID pClBlk);

    /* pointer to cluster free routine */
    void	(*pClFreeRtn) (NET_POOL_ID pNetPool, char * pClBuf);

    /* pointer to mBlk/cluster pair free routine */
    M_BLK_ID 	(*pMblkClFreeRtn) (NET_POOL_ID pNetPool, M_BLK_ID pMblk);

    /* pointer to mBlk get routine */
    M_BLK_ID	(*pMblkGetRtn) (NET_POOL_ID pNetPool, int canWait, unsigned char type);

    /* pointer to cluster Blk get routine */
    CL_BLK_ID	(*pClBlkGetRtn) (NET_POOL_ID pNetPool, int canWait);
    
    /* pointer to a cluster buffer get routine */
    char *	(*pClGetRtn) (NET_POOL_ID pNetPool, CL_POOL_ID pClPool);

    /* pointer to mBlk/cluster pair get routine */
    int	(*pMblkClGetRtn) (NET_POOL_ID pNetPool, M_BLK_ID pMblk,
                                  int bufSize, int canWait, int noSmaller);

    /* pointer to cluster pool Id get routine */
    CL_POOL_ID	(*pClPoolIdGetRtn) (NET_POOL_ID pNetPool, int	bufSize,
                                    int noSmaller);

    /* netBuf 6.0 Pluggable API */
    /* pointer to pool delete routine */
    int (*pNetPoolDeleteRtn)       (NET_POOL_ID pNetPool);

    /* pointer to pool destroy routine */
    int (*pNetPoolDestroyRtn)      (NET_POOL_ID pNetPool);

    /* pointer to pool release routine */
    int (*pNetPoolReleaseRtn)      (NET_POOL_ID pNetPool);

    /* pointer to Tuple get routine */
    M_BLK_ID (*pNetTupleGetRtn)       (NET_POOL_ID pNetPool,
                                       int	bufSize,
                                       int	canWait,
                                       unsigned char type,
                                       int noSmaller);

    /* pointer to Tuple get routine */
    M_BLK_ID (*pNetTupleFreeRtn)      (M_BLK_ID pMblk);
    
    /* pointer to Tuple List Get routine */
    int (*pNetTupleListGetRtn)        (NET_POOL_ID pNetPool, 
                                       CL_POOL_ID pClPool,
                                       int count, 
                                       int type, 
                                       M_BLK_ID * pMblk);

    /* pointer to Tuple List Free routine */
    int (*pNetTupleListFreeRtn)    (M_BLK_ID pMblk);

    /* pointer to Tuple Migrate routine */
    M_BLK_ID (*pNetTupleMigrateRtn)   (NET_POOL_ID pDstNetPool,
                                       M_BLK_ID pMblk);

    /* pointer to memory requirement function */
    ssize_t (*pMemReqRtn) (int type, int num, int size);
    };

struct	poolRel				/* POOL_REL */
    {
    sem_t	releaseSemId;               /* Release Semaphore ID */  //wn 2012.5.8 //wn 2012.7.11
    pthread_t		releaseTaskId;              /* Release task ID */
    
    /* pointer to Tuple netPoolReleaseStart routine */
    int (*pNetPoolReleaseStartRtn) (NET_POOL_ID pNetPool,
                                       POOL_FUNC * pReleaseFunc);

    /* pointer to Tuple netPoolReleaseEnd routine */
    int (*pNetPoolReleaseEndRtn)   (NET_POOL_ID pNetPool);
    };

/* 属性集 */
typedef struct poolAttr
    {
	unsigned int	attribute;	/* Attribute Bits */
    int		refCount;	/* Registered objects sharing this set */
    PART_ID     ctrlPartId;	/* Memory partition for control structures */
    PART_ID 	bMemPartId;	/* Memory partition for clusters */
    void *	pDomain;	/* NULL = Kernel */			
    int 	ctrlAlign;	/* Alignment Control Structures */
    int		clusterAlign;	/* Alignment Clusters (Buffers) */
    struct poolAttr * pNext;	/* Next in Link List */
    } POOL_ATTR;

struct netPool				/* NET_POOL */
    {
    M_BLK_ID	pmBlkHead;		    /* head of mBlks */
    CL_BLK_ID	pClBlkHead;	    	    /* head of cluster Blocks */
    int		mBlkCnt;        	    /* number of mblks */
    int		mBlkFree;	            /* number of free mblks - 
                                               deprecated, use 
                                               pPoolStat->mTypes[MT_FREE]
                                               instead */
    unsigned	clOffset;		    /* leading pad space in cluster */
    int		clBlkOutstanding;	    /* Number of clBlks in use */
    int		clMask;			    /* cluster availability mask */
    int		clLg2Max;		    /* cluster log2 maximum size */
    int		clSizeMax;		    /* maximum cluster size */
    int		clLg2Min;		    /* cluster log2 minimum size */
    int		clSizeMin;		    /* minimum cluster size */
    CL_POOL * 	clTbl [CL_TBL_SIZE];	    /* pool table */
    M_STAT *	pPoolStat;		    /* pool statistics */
    POOL_FUNC *	pFuncTbl;		    /* ptr to function ptr table */
    int 	flag;			    /* Pool Flag */
    ATTR_ID  	pAttrSet;		    /* Attribute Reference */
    PART_ID     poolPartId;		    /* Pool Struct Memory Partition */
    int		attachRefCount;		    /* Attach Ref Count */
    int		bindRefCount;		    /* Attach Ref Count */
    char *	pControlMemArea;	    /* Memory Area for mClBlk */
    PART_ID	controlMemPartId;	    /* memory partition for mClBlk */
    char *	pClusterMemArea;	    /* Memory Area for clusters */
    PART_ID	clusterMemPartId;	    /* memory partition for clusters */
    char        poolName[NET_POOL_NAME_SZ]; /* Pool name */
    POOL_REL    poolRelease;                /* netPoolRelease Support */ // wn 2012.7.11
    struct netPool * pParent;		    /* Parent netPool */
    struct netPool * pNext;		    /* next in link list */

#ifdef _WRS_CONFIG_SMP
#ifdef _WRS_CONFIG_DETERMINISTIC
    spinlock_rt_dtm_t poolLock;
#else
    spinlock_t poolLock;
#endif
#endif
    };

/* Cluster description structure for a buffer pool see netPoolCreate () */
typedef struct netBufClDesc
    {
    int clSize;				/* Cluster Size */
    int clNum;				/* Number of clusters in pool */
    } NETBUF_CL_DESC;
    
/* Pool configuration structure see netPoolCreate () */
typedef struct netBufCfg
    {
    char *          pName;	     /* Pool Name */
    unsigned int          attributes;	     /* pool attributes */
    void *          pDomain;         /* RTP ID or NULL for kernel */
    int             ctrlNumber;	     /* # of ctrl structures to pre-allocate */
    PART_ID         ctrlPartId;	     /* Mem Partition for Control structures */
                                     /*     NULL = use Kernel partition */
    int             bMemExtraSize;   /* Additional memory for runtime buffers */
    PART_ID         bMemPartId;	     /* Mem Partition for buffers */
                                     /*     NULL = default for kernel or RTP */
    NETBUF_CL_DESC * pClDescTbl;     /* desired cluster sizes and count */
    int             clDescTblNumEnt; /* num of entries in cluster table */
    } NETBUF_CFG;

/* 
* NET_POOL_CFG: 
* Optimized allocation and freeing for netPool and function table 
*/
typedef struct netPoolCfg 
    {
    NET_POOL	netPool;	/* netPool - Must be 1st Entry */
    POOL_FUNC 	funcTbl;	/* Function ptr table */
    } NET_POOL_CFG, *pNET_POOL_CFG;

/* CLTAG:
*    Cluster Prefix and appended tags
*    NB_BUF_SANITY - Optional buffer corruption check 
*    NB_BUF_USRTAG - Used by drivers to associate a cluster
*                    address with an Tuple.
*     
*/ 
typedef struct clPreTag
    {
#ifdef NB_BUF_SANITY
    uint32_t 	tag;		/* Used to validate cluster */    
#endif		/* NB_BUF_SANITY */
#ifdef NB_BUF_USRTAG
    uint32_t 	usrTag;		/* User tag */
#endif		/* NB_BUF_USRTAG */
#ifdef NB_FULL_BKWARD_MODE
    NET_POOL_ID	poolId;		/* Pool ID. Req for 5.5 compatibility */
#endif		/* NB_BUF_USRTAG */
    } CL_PRE_TAG;
    
typedef struct clPostTag
    {
    uint32_t 	tag;		/* Used to validate cluster */    
    } CL_POST_TAG;

#ifdef NETBUF_DEBUG

#define GCC_VERSION \
	(__GNUC__ * 10000 + __GNUC_MINOR__ * 100 + __GNUC_PATCHLEVEL__)

/* XXX verify exact version numbers for _HAVE__FUNCTION__ and _HAVE__func__ */
#if defined (__GNUC__) || (defined (__DCC__) && __VERSION_NUMBER__ >= 5200)
#define _HAVE__FUNCTION__
#endif

#if (GCC_VERSION >= 30200)
#define _HAVE__func__
#endif

#ifdef _HAVE__func__
#define FID __func__
#elif defined (_HAVE__FUNCTION__)
#define FID __FUNCTION__
#else
#define FID ""
#endif

#define netClusterGet(pNetPool, pClPool) \
	_netClusterGet (pNetPool, pClPool, FID, __FILE__, __LINE__)
#define netClFree(pNetPool, pClBuf) \
	_netClFree (pNetPool, pClBuf, FID, __FILE__, __LINE__)
#endif /* NETBUF_DEBUG */
    
extern PART_ID memNetPartId;
    
extern int 		netBufLibInitialize (int drvClOffset);
extern int 		netBufLibInit (void);
extern int 		netBufPoolInit (void); 

extern int 		linkBufPoolInit (void); 
extern int 		vxmux_null_buf_init (void);
extern int 		netPoolInit (NET_POOL_ID pNetPool,
                                     M_CL_CONFIG * pMclBlkConfig,
                                     CL_DESC * pClDescTbl, int clDescTblNumEnt,
                                     POOL_FUNC * pFuncTbl);
extern int		netPoolDelete (NET_POOL_ID);
extern void		netMblkFree (NET_POOL_ID pNetPool, M_BLK_ID pMblk);
extern void		netClBlkFree (NET_POOL_ID pNetPool, CL_BLK_ID pClBlk);
#ifdef NETBUF_DEBUG
extern void 		_netClFree (NET_POOL_ID pNetPool, unsigned char * pClBuf, 
				    char *, char *, int);
extern char *	 	_netClusterGet (NET_POOL_ID pNetPool, CL_POOL_ID pClPool, 
					char *, char *, int);
#else
extern void 		netClFree (NET_POOL_ID pNetPool, unsigned char * pClBuf);
extern char *	 	netClusterGet (NET_POOL_ID pNetPool,
                                       CL_POOL_ID pClPool);
#endif /* NETBUF_DEBUG */

extern M_BLK_ID 	netMblkClFree (M_BLK_ID pMblk);
extern void		netMblkClChainFree (M_BLK_ID pMblk);
extern M_BLK_ID 	netMblkGet (NET_POOL_ID pNetPool, int canWait,
		unsigned char type);
extern CL_BLK_ID	netClBlkGet (NET_POOL_ID pNetPool, int canWait);
extern int 	 	netMblkClGet (NET_POOL_ID pNetPool, M_BLK_ID pMblk,
                                      int bufSize, int canWait,
				      int noSmaller);
extern M_BLK_ID		netTupleGet (NET_POOL_ID pNetPool, int bufSize,
                                     int canWait, unsigned char type, int noSmaller);
extern M_BLK_ID 	netTupleGet2 (NET_POOL_ID, int, int);
extern CL_BLK_ID  	netClBlkJoin (CL_BLK_ID pClBlk, char * pClBuf,
                                      int size, RE_FUNCPTR pFreeRtn, int arg1,
                                      int arg2, int arg3);
extern M_BLK_ID  	netMblkClJoin (M_BLK_ID pMblk, CL_BLK_ID pClBlk);
extern CL_POOL_ID 	netClPoolIdGet (NET_POOL_ID pNetPool, int bufSize,
                                        int noSmaller);

extern int 		netMblkToBufCopy (M_BLK_ID pMblk, char * pBuf,
		RE_FUNCPTR pCopyRtn);
extern int		netMblkOffsetToBufCopy (M_BLK_ID pMblk, int offset,
                                                char * pBuf, int len,
                                                RE_FUNCPTR	pCopyRtn);
extern M_BLK_ID 	netMblkDup (M_BLK_ID pSrcMblk, M_BLK_ID	pDestMblk);
extern M_BLK_ID 	netMblkChainDup (NET_POOL_ID pNetPool, M_BLK_ID	pMblk,
                                         int offset, int len, int canWait);
extern unsigned int           netMblkFromBufCopy(M_BLK_ID pMblk, char* pBuf,
		unsigned int offset, unsigned int len,
		int canWait, RE_FUNCPTR pCopyRtn);
extern void             netPoolShow (NET_POOL_ID pNetPool);
extern void 	        mbufShow (void);
extern void             netStackSysPoolShow (void);
extern void             netStackDataPoolShow (void);

/* NetBuf 函数 */
extern NET_POOL_ID netPoolCreate          (NETBUF_CFG * pnetBufCfg, 
                                           POOL_FUNC * pFuncTbl);
extern int	   netBufAdvLibInit	  (void);
extern NET_POOL_ID netPoolAttach	  (const char * pName);
extern int      netPoolDetach	  (NET_POOL_ID pNetPool);
extern int      netPoolBind            (NET_POOL_ID pNetPoolChild, 
                                           const char *pParentName);
extern int      netPoolUnbind          (NET_POOL_ID pNetPool);                           
extern int      netPoolRelease	  (NET_POOL_ID pNetPool,
                                           int destroyContext);
extern M_BLK_ID    netTupleFree           (M_BLK_ID pMblk);
extern int         netTupleListGet	  (NET_POOL_ID pNetPool, 
                                           CL_POOL_ID pClPool, 
                                           int count, 
                                           int type, 
                                           M_BLK_ID * pMblk);
extern int      netTupleListFree	  (M_BLK_ID);
extern M_BLK_ID    netTupleMigrate        (NET_POOL_ID pDstNetPool,
                                           M_BLK_ID pMblk);
extern int      netTuplePhysAddrMap	  (M_BLK_ID pMblk, 
                                           BUF_VTOP * pVtopTbl, 
                                           int * pElementCount);
extern NET_POOL_ID netPoolIdGet		  (const char * pPoolName);
extern char *      netPoolNameGet	  (NET_POOL_ID pNetPool);
extern ssize_t	   _netMemReqDefault	  (int type, int num, int size);
#ifdef NETBUF_DEBUG
extern char *	_dbgClusterGet (NET_POOL_ID pNetPool, CL_POOL_ID pClPool, 
					char *, char *, int);
extern void 	_dbgClFree (NET_POOL_ID pNetPool, char * pClBuf, 
				    char *, char *, int);
#endif /* NETBUF_DEBUG */

extern POOL_FUNC * _pNetPoolFuncTbl;
extern POOL_FUNC * _pLinkPoolFuncTbl;
extern void (*_func_netPoolShow) (NET_POOL_ID pNetPool);

/*
 * The following routines and variables are considered private and should
 * not be used outside of the netBufLib/netBufAdvLib implementation.
 */
extern int      netPoolCreateValidate (NET_POOL_ID pNetPool);
extern POOL_ATTR * (*_func_netAttrReg) (unsigned int attribute, PART_ID ctrlPartId,
					PART_ID bMemPartId, void * pDomain);
extern int      (*_func_netAttrDeReg) (POOL_ATTR * pBufAttr);
extern sem_t      netBufLock; 
extern NET_POOL_ID netPoolIdGetWork (const char * pPoolName);

#ifdef __cplusplus
}
#endif

#endif 
