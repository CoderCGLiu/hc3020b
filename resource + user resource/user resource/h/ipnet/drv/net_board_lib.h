#ifndef _REWORKS_NET_BOARD_LIB_H_
#define _REWORKS_NET_BOARD_LIB_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "net_board.h"
#include <drv/vxcompat/endLib.h>

#define NET_MCACHE_SIZE		END_MCACHE_SIZE
#define NET_MCACHE_TAG		END_MCACHE_TAG
#define NET_MCACHE_CLSIZE	END_MCACHE_CLSIZE
#define NET_JUMBO_CLSIZE	END_JUMBO_CLSIZE
#define NET_MCACHE_INC(x)	(x) = ((x + 1) & (NET_MCACHE_SIZE - 1))

#define NET_NET_POOL_CREATE     END_NET_POOL_CREATE
#define NET_NET_POOL_INIT       END_NET_POOL_INIT

#define NET2_RCV_RTN_CALL(pNet, pkt) \
    do  { \
        (pNet)->receiveRtn ((pNet), (pkt)); \
        } while ((0))

#define NET2_RXPKT_START_SET(pkt, mtu, maxlinkhdr) \
    do  { \
        (pkt)->start = ((((pkt)->maxlen - mtu) & ~0x3) - maxlinkhdr); \
        } while ((0))

#define NET2_PKT_START(pkt, mtu, maxlinkhdr) \
    ((((pkt)->maxlen - mtu) & ~0x3) - maxlinkhdr)

#ifdef NETDRV_MACROS
#undef NETDRV_MACROS
#endif
#ifdef NETDRV_MACROS

#define NETDRV_RCV_RTN_CALL(pEnd,pMblk) \
            { \
	    if ((pNet)->receiveRtn) \
		{ \
		(pNet)->receiveRtn ((pNet), pMblk); \
		} \
            else \
		netMblkClChainFree (pMblk); \
            }

#define TK_RCV_RTN_CALL(pNet,pMblk, netSvcOffset, netSvcType, \
			uniPromiscuous, pSpareData) \
            { \
            if ((pNet)->receiveRtn) \
                { \
                (pNet)->receiveRtn ((pNet), pMblk, netSvcOffset, netSvcType, \
				    uniPromiscuous, pSpareData); \
                } \
            else \
		netMblkClChainFree (pMblk); \
            }

#ifdef END_TX_SEM_INLINE
#define NETDRV_TX_SEM_TAKE(pNet, tmout) \
            (semMTake_inline ((pNet)->txSem, tmout, NETDRV_TXSEM_INLINE_OPTS))

#define NETDRV_TX_SEM_GIVE(pNet) \
            (semMGive_inline ((pNet)->txSem, NETDRV_TXSEM_INLINE_OPTS))
#elif defined (NETDRV_TX_SEM_SCALABLE)
#define NETDRV_TX_SEM_TAKE(pNet, tmout) \
            (semMTakeScalable ((pNet)->txSem, tmout, NETDRV_TXSEM_SCALABLE_OPTS))

#define NETDRV_TX_SEM_GIVE(pNet) \
            (semMGiveScalable ((pNet)->txSem, NETDRV_TXSEM_SCALABLE_OPTS))
#elif defined (NETDRV_TX_SEM_MINIMAL)
#define NETDRV_TX_SEM_TAKE(pNet, tmout) (semMMinTake ((pNet)->txSem, tmout))

#define NETDRV_TX_SEM_GIVE(pNet) (semMMinGive ((pNet)->txSem))
#else
#define NETDRV_TX_SEM_TAKE(pNet, tmout) \
	    (semTake ((pNet)->txSem,tmout))

#define NETDRV_TX_SEM_GIVE(pNet) \
	    (semGive ((pNet)->txSem))
#endif /* END_TX_SEMMINIMAL */

#define	NETDRV_FLAGS_CLR(pNet,clrBits) \
            ((pNet)->flags &= ~(clrBits))

#define	NETDRV_FLAGS_SET(pNet,setBits) \
            ((pNet)->flags |= (setBits))

#define	NETDRV_FLAGS_GET(pNet) \
	    ((pNet)->flags)

#define NETDRV_MULTI_LST_CNT(pNet) \
	    (iplstCount (&(pNet)->multiList))

#define NETDRV_MULTI_LST_FIRST(pNet) \
	    (ETHER_MULTI *)(iplstFirst (&(pNet)->multiList))

#define NETDRV_MULTI_LST_NEXT(pCurrent) \
	    (ETHER_MULTI *)(iplstNext (&pCurrent->node))

#define NETDRV_DEV_NAME(end) \
	    (end.devObject.name)
		
#define NETDRV_OBJECT_UNLOAD(pNet) \
	    (net_drv_object_unload (pNet))

#define	NETDRV_OBJ_INIT(pNet,pDev,name,unit,pFuncs, pDesc) \
	    net_drv_obj_init ((pNet),pDev,name,unit,pFuncs, pDesc)

#define NETDRV_OBJ_READY(pNet, flags) \
	    net_drv_obj_flag_set ((pNet),flags)

#define NETDRV_ERR_ADD(pNet,code,value) \
	    (mib2ErrorAdd(&(pNet)->mib2Tbl, code, value))

#define NETDRV_MIB_INIT(pNet,type,addr,addrLen,mtu,speed) \
	    (mib2Init(&(pNet)->mib2Tbl, type,addr, addrLen, mtu, speed))
#else

#define NETDRV_RCV_RTN_CALL(pNet,pData) \
                endRcvRtnCall((pNet), pData)
#if 0             
#define TK_RCV_RTN_CALL(pNet,pData, netSvcOffset, netSvcType, \
			uniPromiscuous, pSpareData) \
            tkRcvRtnCall((pNet), pData, netSvcOffset, netSvcType, \
			     uniPromiscuous, pSpareData)
#endif
#define NETDRV_TX_SEM_TAKE(pNet,tmout) \
                net_drv_tx_sem_take((pNet), tmout)

#define NETDRV_TX_SEM_GIVE(pNet) \
                net_drv_tx_sem_give(pNet)

#define	NETDRV_FLAGS_CLR(pNet,clrBits) \
                net_drv_flags_clr((pNet), clrBits)

#define	NETDRV_FLAGS_SET(pNet,setBits) \
                net_drv_flags_set((pNet), setBits)

#define	NETDRV_FLAGS_GET(pNet) \
                (net_drv_flags_get((pNet)))

#define NETDRV_MULTI_LST_CNT(pNet) \
                (endMultiLstCnt((pNet)))

#define NETDRV_MULTI_LST_FIRST(pNet) \
                (net_drv_multi_list_first((pNet)))

#define NETDRV_MULTI_LST_NEXT(pCurrent) \
                (net_drv_multi_list_next((pCurrent)))

#define NETDRV_DEV_NAME(pNet) \
                (endDevName((pNet)))
		
#define NETDRV_OBJECT_UNLOAD(pNet) \
                net_drv_object_unload((pNet))
                
#define	NETDRV_OBJ_INIT(pNet,pDev,name,unit,pFuncs, pDescription) \
	    net_drv_obj_init ((pNet),pDev,name,unit,pFuncs, pDescription)

#define NETDRV_OBJ_READY(pNet, flags) \
	    net_drv_obj_flag_set ((pNet),flags)

#define NETDRV_ERR_ADD(pNet,code,value) \
	    (mib2ErrorAdd(&(pNet)->mib2Tbl, code, value))

#define NETDRV_MIB_INIT(pNet,type,addr,len,mtu,speed) \
	    (mib2Init(&(pNet)->mib2Tbl, type, addr, len, mtu, speed))
#endif /* NETDRV_MACROS */
                
/* These four match prototypes from NET_FUNCS in net_board.h: */
#define _func_net_drv_ether_address_form _func_endEtherAddressForm
#define _func_net_drv_8023_address_form _func_end8023AddressForm
#define _func_net_drv_ether_packet_data_get _func_endEtherPacketDataGet
#define _func_net_drv_ether_packet_addr_get _func_endEtherPacketAddrGet

/* forward declarations */
#define    	net_drv_2_lib_init 				end2LibInit
#define    	net_drv_lib_init 				endLibInit
#define 	net_drv_mib_if_init 			endMibIfInit
#define 	net_drv_obj_init 				endObjInit

#define		net_drv_obj_flag_set 			endObjFlagSet

#define		net_drv_tx_sem_take 			endTxSemTake
#define		net_drv_tx_sem_give 			endTxSemGive

#define		net_drv_object_unload 			endObjectUnload
#define		net_drv_flags_set 				endFlagsSet
#define		net_drv_flags_clr 				endFlagsClr
#define		net_drv_flags_get 				endFlagsGet
#define		net_drv_multi_list_first 		endMultiLstFirst
#define		net_drv_multi_list_next 		endMultiLstNext
#define    	net_drv_ether_address_form 		endEtherAddressForm
#define    	net_drv_8023_address_form 		end8023AddressForm
#define  	net_drv_ether_packet_data_get 	endEtherPacketDataGet
#define  	net_drv_ether_packet_addr_get 	endEtherPacketAddrGet
#define 	net_drv_ether_resolve 			endEtherResolve

#define  	net_drv_2_ether_hdr_parse 			end2EtherHdrParse
#define  	net_drv_2_ether_II_form_link_hdr 	end2EtherIIFormLinkHdr
#define  	net_drv_2_buffer_pool_config 		end2BufferPoolConfig

#define 	net_drv_poll_stats_init 			endPollStatsInit
#define 	net_drv_M2_init 					endM2Init
#define 	net_drv_M2_free 					endM2Free
#define 	net_drv_M2_ioctl 					endM2Ioctl

#define 	net_drv_pool_create 				endPoolCreate
#define 	net_drv_pool_jumbo_create 			endPoolJumboCreate
#define 	net_drv_pool_destroy 				endPoolDestroy

#undef END_POOL_MCACHE
#ifdef END_POOL_MCACHE
M_BLK_ID endPoolTupleGet (NET_POOL_ID);
void   netPoolTupleFree (M_BLK_ID);
void netMcacheFlush (void);
M_BLK_ID netMcacheGet (void);
int netMcachePut (M_BLK_ID);
#else
#define netPoolTupleFree(__m) netMblkClChainFree (__m)
#define netMcacheFlush() do {} while ((0))
#define netMcacheGet() (NULL)
#define netMcachePut() (ERROR)
#endif /* END_POOL_MCACHE */

#define netEtherCrc32BeGet endEtherCrc32BeGet
#define netEtherCrc32LeGet endEtherCrc32LeGet

#ifdef __cplusplus
}
#endif

#endif 
