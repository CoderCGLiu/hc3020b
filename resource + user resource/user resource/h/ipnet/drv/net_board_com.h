#ifndef _REWORKS_NET_COM_H_
#define _REWORKS_NET_COM_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <ip/m2Lib.h>	 
//#include <ioctl.h> // 这个在 vx 兼容目录下 
#include <io_ctl.h>
#include <drv/vxcompat/endCommon.h>

#define NETDRV_STYLE_END2  END_STYLE_END2
#define NETDRV_STYLE_END   END_STYLE_END
#define NETDRV_STYLE_NPT   END_STYLE_NPT

#define NUM_END_STYLES	3

#define NETDRV_DEBUG 			END_DEBUG

#define NETDRV_MIB_2233   		END_MIB_2233  

#define NETDRV_INIT_STR_MAX 	END_INIT_STR_MAX
#define NETDRV_PROTO_NAME_MAX 	END_PROTO_NAME_MAX

/* 设备名的最大长度 */
#define NETDRV_NAME_MAX 		END_NAME_MAX         
/* 设备描述的最大长度 */
#define NETDRV_DESC_MAX 		END_DESC_MAX        

/*
 * EIOCGIFCAP 和 EIOCSIFCAP ioctls 使用这个结构体
 * */
typedef END_CAPABILITIES NET_CAPABILITIES;

/*
 * EIOCGIFMEDIA 和 EIOCSIFMEDIA ioctls 使用这个结构体
 */
typedef END_MEDIA NETDRV_MEDIA;

/*
 * EIOCGMEDIALIST ioctl 使用这个结构体
 */
typedef END_MEDIALIST NETDRV_MEDIALIST;

/*
 * EIOCVLANGET 使用这个结构体返回为接口配置的 VLANs 的列表
 */
typedef END_VLAN_REQ NETDRV_VLAN_REQ;

/*
 * 
 * End 特殊的 IOCTL 命令
 * EIOC 表示 END ioctl，SIOC 表示 socket ioctl
 */
#undef NETDRV_IOCTL_BIN_COMPAT

typedef END_PHYADDR NETDRV_PHYADDR ;
typedef END_NAME NETDRV_NAME ;

#ifdef __cplusplus
}
#endif

#endif
