/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了ReWorks 网络协议栈作业队列所需的类型和接口定义
 * 修改：
 * 		 2013-9-25，规范
 * 
 */
#ifndef _REWORKS_NET_JOBQUEUE_LIB_H_
#define _REWORKS_NET_JOBQUEUE_LIB_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "netBufLib.h" 

#define member_to_object(pMember, type, memberName) \
    ((type *)((char *)(pMember) - offsetof (type, memberName)))

#define JOBQ_PRI_MASKABLE   /* enable priority masking */

/*
 * The QJOB_BUSY bit in the priInfo member of a pre-allocated QJOB may be
 * used for any purpose by the jobLib client.  jobLib does not modify this
 * bit, and is not affected by it.
 */

#define QJOB_BUSY	0x20 /* managed by client */

#define QJOB_IS_BUSY(pJob) ((pJob)->priInfo & QJOB_BUSY)
#define QJOB_SET_BUSY(pJob) ((pJob)->priInfo |= QJOB_BUSY)
#define QJOB_CLEAR_BUSY(pJob) ((pJob)->priInfo &= ~QJOB_BUSY)

#define QJOB_PRI_LOWEST		0		/* 最低优先级 */
#define QJOB_PRI_HIGHEST 	31		/* 最高优先级 */
#define QJOB_NUM_PRI  		32		/* 优先级的数量 */
#define QJOB_PRI_MASK 		0x1f	/* 优先级位的掩码 */

#define QJOB_SET_PRI(pJob, pri) \
    ((pJob)->priInfo = ((pJob)->priInfo & ~QJOB_PRI_MASK) | \
     ((pri) & QJOB_PRI_MASK))

#define QJOB_GET_PRI(pJob) ((pJob)->priInfo & QJOB_PRI_MASK)

typedef void (* QJOB_FUNC) (void *);

/*
 * A QJOB is typically a member of a larger structure with information
 * used by the handler function 'func'. func() is passed a pointer to
 * the QJOB as its only argument.
 */
typedef struct _QJOB
    {
    struct _QJOB *	pNext;
    unsigned int 	priInfo;	/* 使用 QJOB_SET_PRI ()修改 */
    QJOB_FUNC		func;		/* 要执行的函数 */
    } QJOB;

typedef struct _QJOB_STANDARD
    {
    QJOB job;
    void * arg1;
    void * arg2;
    void * arg3;
    void * arg4;
    void * arg5;
    } QJOB_STANDARD;

struct _JOB_QUEUE;

typedef struct _JOB_QUEUE * JOB_QUEUE_ID;

struct _QJOB_STD_POOL;

typedef struct _QJOB_STD_POOL * QJOB_STD_POOL_ID;

extern JOB_QUEUE_ID (*_func_netDaemonIxToQId) (int index);

/* 失败返回 -1 */
extern int (*_func_netDaemonQIdToIx) (JOB_QUEUE_ID qId);

extern int (* jobQueueProcessFunc) (JOB_QUEUE_ID pJobQ);
extern void	jobQueueLibInit (void);
extern int	jobQueuePost (JOB_QUEUE_ID jobQueueId, QJOB * pJob);

//typedef void    (*VOIDFUNCPTR2)(void *);
extern int	jobQueueStdPost (JOB_QUEUE_ID jobQueueId, int pri, 
		RE_VOIDFUNCPTR pFunc, void * arg1, void * arg2,  
				 void * arg3, void * arg4, void * arg5);

extern JOB_QUEUE_ID	jobQueueCreate (QJOB_STD_POOL_ID stdPoolId);
extern int		jobQueueInit (JOB_QUEUE_ID jobQueueId,
				      QJOB_STD_POOL_ID stdPoolId);

extern int	jobQueueProcess (JOB_QUEUE_ID jobQId);
extern pthread_t	jobQueueTask (JOB_QUEUE_ID jobQueueId);

#ifdef JOBQ_PRI_MASKABLE
extern unsigned int	jobQueuePriorityMask (JOB_QUEUE_ID jobQId, unsigned int disablePri,
		unsigned int enablePri);
#endif /* JOBQ_PRI_MASKABLE */

/* 标准池 */
extern int	jobQueueStdPoolInit (QJOB_STD_POOL_ID stdPoolId);
extern int	jobQueueStdJobsAlloc (QJOB_STD_POOL_ID stdPoolId,
				      int nStdJobs);

/* jobQueueUtilLib */
extern void	jobQueueUtilInit (void);
extern int	jobQueueQuit (JOB_QUEUE_ID jobQId, int pri, int quitValue,
			      RE_VOIDFUNCPTR pFunc, void * arg1, void * arg2);
extern int	jobQueueProcessWrapper (JOB_QUEUE_ID jobQId);
extern int	jobQueueDelete (JOB_QUEUE_ID jobQId);
extern int	jobQueueTerminate (JOB_QUEUE_ID jobQId);
extern int	jobQueueStdPoolJobsFree (QJOB_STD_POOL_ID pStdPool);

#ifdef __cplusplus
}
#endif

#endif 
