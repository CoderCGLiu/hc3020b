#ifndef _REWORKS_NET_BOARD_H_
#define _REWORKS_NET_BOARD_H_

#include <drv/vxcompat/end.h>
#include <drv/net_board_com.h>

#ifdef __cplusplus
extern "C" {
#endif


/*
 * 设备阻塞在传输上的返回值
 */
#define NETDRV_ERR_BLOCK  END_ERR_BLOCK

#define NETDRV_ERR_INFO     END_ERR_INFO   		/* Information only */
#define NETDRV_ERR_WARN     END_ERR_WARN   		/* 警告 */
#define NETDRV_ERR_RESET    END_ERR_RESET   	/* Device has reset. */
#define NETDRV_ERR_DOWN     END_ERR_DOWN  	 	/* Device has gone down. */
#define NETDRV_ERR_UP       END_ERR_UP  		/* Device has come back on line. */
#define NETDRV_ERR_FLAGS	END_ERR_FLAGS   	/* Device flags changed */
#define NETDRV_ERR_NO_BUF   END_ERR_NO_BUF   	/* Device's cluster pool exhausted. */
#define NETDRV_ERR_LINKDOWN END_ERR_LINKDOWN   	/* Device's link is down */
#define NETDRV_ERR_LINKUP   END_ERR_LINKUP   	/* Device's link is up */

/*
 * 层 2 通知 
 */    
#define NETDRV_ERR_L2NOTIFY       END_ERR_L2NOTIFY  		/* 层 2 port notification */
#define NETDRV_ERR_L2VID_NOTIFY   END_ERR_L2VID_NOTIFY  	/* 层 2 vlan notification */
#define NETDRV_ERR_L2PVID_NOTIFY  END_ERR_L2PVID_NOTIFY  	/* 层 2 port-based vlan notification */

typedef END_ERR NETDRV_ERR;

struct proto_entry;
struct protocol_binding;

struct Ipcom_pkt_struct; 

typedef END_CLOSURE NETDRV_CLOSURE;

typedef END_OUTPUT_FILTER NETDRV_OUTPUT_FILTER;

typedef END_OBJ NET_OBJ;

typedef END_MUX2SEND_RTN  NETDRV_MUX2SEND_RTN;
				 
typedef END_MUXSEND_RTN NETDRV_MUXSEND_RTN;
typedef END_TKSEND_RTN NETDRV_TKSEND_RTN;
typedef END2_RCV_RTN NETDRV2_RCV_RTN;
typedef END_RCV_RTN NETDRV_RCV_RTN;
typedef END2_POLLRCV_RTN NETDRV2_POLLRCV_RTN;
typedef END_POLLRCV_RTN NETDRV_POLLRCV_RTN;
typedef END2_FORM_LINKHDR_RTN NETDRV2_FORM_LINKHDR_RTN;

/*
 * 驱动使用的基本结构体
 */
#define net_drv_object end_object

#define NETDRV_MIB_STATS(pNet, mbr) \
    pNet->end_m2Id.m2Data.mibIfTbl.mbr
#define NETDRV_MIB_2233_STATS(pNet, mbr) \
	pNet->end_m2Id.m2Data.mibXIfTbl.mbr

#if 0
typedef END2_NET_FUNCS NETDRV2_NET_FUNCS;

/* 
 * BSP 在启动时，构建一个网卡设备表时使用的结构体
 */
typedef END_TBL_ENTRY  NET_TBL_ENTRY; 
    
typedef END_TBL_END NETDRV_TBL_END;
#else
/* 
 * NET_FUNCS - 驱动函数表
 */
typedef struct _rx_net_funcs
    {
	int (*start) (NET_OBJ*);		   		/* 驱动程序的启动函数 */
    int (*stop) (NET_OBJ*);		   			/* 驱动程序的停止函数 */
    int (*unload) (NET_OBJ*);		   		/* 驱动程序的卸载函数 */
    int (*ioctl) (NET_OBJ*, int, caddr_t);  /* 驱动程序的控制函数 */
    int (*send) (NET_OBJ* , M_BLK_ID);	    /* 驱动程序的发送函数 */
    int (*mCastAddrAdd) (NET_OBJ*, char*);  /* 驱动程序的添加多播地址函数 */
    int (*mCastAddrDel) (NET_OBJ*, char*);  /* 驱动程序的删除多播地址函数 */
    int (*mCastAddrGet) (NET_OBJ*, MULTI_TABLE*);
                                            /* 驱动程序的获得多播地址表函数 */
    int (*pollSend) (NET_OBJ*, M_BLK_ID);   /* 驱动程序的轮询发送函数 */
    int (*pollRcv) (NET_OBJ*, M_BLK_ID);    /* 驱动程序的轮询接收函数 */
    /* 
     * The minimum required arguments for (*formAddress)() are: 
     * (*formAddress)(M_BLK_ID, M_BLK_ID, M_BLK_ID, int)
     */
    M_BLK_ID (*formAddress) ();         	/* 驱动程序的地址组合函数 */
    /*
     * The minimum required arguments for (*packetDataGet)() are
     * (*packetDataGet)(M_BLK_ID, LL_HDR_INFO *)
     */
    int (*packetDataGet) ();         		/* 驱动程序的包数据获得函数 */
    int (*addrGet) (M_BLK_ID, M_BLK_ID, M_BLK_ID, M_BLK_ID, M_BLK_ID);
                                            /* 驱动程序的包地址获得函数 */
    int (*endBind) (void*, void*, void*, long type); 
											/* 在网络服务和网络驱动程序之间进行信息交换的函数 */
    } RX_NET_FUNCS;

typedef struct _NETDRV2_NET_FUNCS
    {
	RX_NET_FUNCS funcs;
    void (*llhiComplete) (struct Ipcom_pkt_struct * pkt, LL_HDR_INFO * llhi);
    } NETDRV2_NET_FUNCS;

/* 
 * BSP 在启动时，构建一个网卡设备表时使用的结构体
 */
typedef struct NET_TBL_ENTRY // 未使用到，可以删除
    {
    int unit;                                /* 这个设备的单元 */
    NET_OBJ* (*net_load_func) (char*, void*);  /* 加载函数 */
    char* net_load_string;                     /* 加载字符串 */
    int net_loan;                            /* Do we loan buffers? */
    void* pBSP;                              /* BSP private */
    int processed;                          /* Has this been processed? */
    } NET_TBL_ENTRY;
    
#define NETDRV_TBL_END NULL
#endif
/*
 * polled stats counters 的定义和结构体
 */
typedef endCounter net_counter;

typedef END_IFCOUNTERS NET_IFCOUNTERS;

#define NETDRV_IFINERRORS_VALID		END_IFINERRORS_VALID
#define NETDRV_IFINDISCARDS_VALID		END_IFINDISCARDS_VALID
#define NETDRV_IFINUNKNOWNPROTOS_VALID	END_IFINUNKNOWNPROTOS_VALID
#define NETDRV_IFINOCTETS_VALID		END_IFINOCTETS_VALID
#define NETDRV_IFINUCASTPKTS_VALID		END_IFINUCASTPKTS_VALID
#define NETDRV_IFINMULTICASTPKTS_VALID	END_IFINMULTICASTPKTS_VALID
#define NETDRV_IFINBROADCASTPKTS_VALID	END_IFINBROADCASTPKTS_VALID

#define NETDRV_IFOUTERRORS_VALID		END_IFOUTERRORS_VALID
#define NETDRV_IFOUTDISCARDS_VALID		END_IFOUTDISCARDS_VALID
#define NETDRV_IFOUTUNKNOWNPROTOS_VALID	END_IFOUTUNKNOWNPROTOS_VALID
#define NETDRV_IFOUTOCTETS_VALID		END_IFOUTOCTETS_VALID
#define NETDRV_IFOUTUCASTPKTS_VALID		END_IFOUTUCASTPKTS_VALID
#define NETDRV_IFOUTMULTICASTPKTS_VALID	END_IFOUTMULTICASTPKTS_VALID
#define NETDRV_IFOUTBROADCASTPKTS_VALID	END_IFOUTBROADCASTPKTS_VALID

typedef END_IFDRVCONF NET_IFDRVCONF;

/*
 * EIOCGRCVJOBQ ioctl 使用的结构体 
 */
typedef END_RCVJOBQ_INFO NETDRV_RCVJOBQ_INFO;

#define net_dev_load  muxDevLoad 			 
#define net_dev_start muxDevStart  		 

#ifdef __cplusplus
}
#endif

#endif
