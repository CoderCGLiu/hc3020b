#ifndef _REWORKS_NET_LIB_H_
#define _REWORKS_NET_LIB_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "jobQueueLib.h" 
#include "netBufLib.h"  
	
#define NET_TASK_QJOB_PRI	16

extern JOB_QUEUE_ID netJobQueueId;

extern void * netGtfTickInformation; /* ֻ�� */

#if defined(__STDC__) || defined(__cplusplus)

extern int   netJobAdd (RE_FUNCPTR routine, int param1, int param2, int param3,
                           int param4, int param5);
extern int   netLibInitialize (unsigned int numDaemons, int affinityLock,
			  int (*netTaskFunc) (JOB_QUEUE_ID qId, int qIndex));
extern unsigned int netDaemonsCount (void);
extern JOB_QUEUE_ID netDaemonQ (int i);
extern int netDaemonQnum (JOB_QUEUE_ID qId);

extern void     netErrnoSet (int status);
extern void     mbinit (void * pNetPoolCfg);
extern int	netJobAlloc (u_int numJobs);

extern void	netGtfTickFunc (void * pTm, void * arg);

#else   /* __STDC__ */
extern int   netJobAdd ();
extern int   netLibInitialize ();
extern unsigned int netDaemonsCount ();
extern JOB_QUEUE_ID netDaemonQ ();
extern void     netErrnoSet ();
extern void     mbinit ();
extern int   netJobAlloc ();
extern void     netGtfTickFunc ();
#endif  /* __STDC__ */

#define netJobAdd(f, a1, a2, a3, a4, a5) \
    (jobQueueStdPost (netJobQueueId, NET_TASK_QJOB_PRI,		    \
		      (RE_VOIDFUNCPTR)(f), (void *)(a1), (void *)(a2), \
		      (void *)(a3), (void *)(a4), (void *)(a5)))

#ifdef __cplusplus
}
#endif

#endif

