#ifndef AUTOCONF_H_
#define AUTOCONF_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <cpu.h>

#if MAX_SMP_CPUS > 1
//#define _RWS_CONFIG_DETERMINISTIC 1	
#define _WRS_CONFIG_SMP 1
#endif

#ifndef _WRS_KERNEL
#define _WRS_KERNEL
#endif

#ifdef __cplusplus
}
#endif

#endif
