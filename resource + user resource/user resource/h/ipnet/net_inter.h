#ifndef __NET_INTER_H__
#define __NET_INTER_H__

#ifdef __cplusplus
extern "C" {
#endif

extern int if_addr_set(char *interfaceName, char *interfaceAddress);
extern int if_mask_set(char *interfaceName, int netMask);
extern int if_flag_change(char *interfaceName, int flags, int on);
extern int ip_attach(int unit, char * pDevice);

extern int if_addr6_add(char *interfaceName, char *interfaceAddress, int prefix_len, int flags);
extern int if_addr6_set(char *interfaceName, char *interfaceAddress, int prefix_len, int flags);

#ifdef __cplusplus
}
#endif

#endif /* __NET_INTER_H__ */
