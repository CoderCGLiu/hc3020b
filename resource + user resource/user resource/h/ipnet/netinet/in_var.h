#ifndef _NETINET_IN_VAR_H_
#define _NETINET_IN_VAR_H_

#include <sys/queue.h>

//#ifdef _WRS_KERNEL //wn 2012.8.21
//#include <sys/fnv_hash.h> //wn 2012.8.8
//#endif

#include <net/if.h>
#include <netinet/in.h>
#ifdef __cplusplus
 extern "C" {
#endif

struct	in_aliasreq {
	char	ifra_name[IFNAMSIZ];		/* if name, e.g. "en0" */
	struct	sockaddr_in ifra_addr;
	struct	sockaddr_in ifra_broadaddr;
#define ifra_dstaddr ifra_broadaddr
	struct	sockaddr_in ifra_mask;
};

#ifdef __cplusplus
}
#endif
    
#ifdef INET6
#include <netinet6/in6_var.h>
#endif /* INET6 */

#endif /* _NETINET_IN_VAR_H_ */
