#ifndef _REWORKS_NETINET_IF_ETHER_H_
#define _REWORKS_NETINET_IF_ETHER_H_

#include <netinet/in.h>
#include <net/ethernet.h>
#include <net/if_arp.h>

//#include "ip/cfgDefs.h" 

#ifdef __cplusplus
 extern "C" {
#endif
     
/* Ether header for tagged frames */
struct ether_tag_header
    {
    u_char   ether_dhost [6];
    u_char   ether_shost [6];
    u_char   etherTag [4];
    u_short  ether_type;
    };

/*
 * Clarinet 
 * The following macros are required as they are used in all the 
 * driver header files which include if_ether.h
 */

/*
 * for compilers that have 4-bytes structure alignment rule,
 * it is understood that sizeof (ether_header) is 0x10
 */
#define SIZEOF_ETHERHEADER      0xe

 /* For tagged Ethernet headers */
#define SIZEOF_TAG_ETHERHEADER      0x12

 /* minimal size of an ethernet frame including the headers supported
  * by most ethernet chips.
  */
#define ETHERSMALL      60

/* End of Clarinet introduced macros */

/*
 * Macro to map an IP multicast address to an Ethernet multicast address.
 * The high-order 25 bits of the Ethernet address are statically assigned,
 * and the low-order 23 bits are taken from the low end of the IP address.
 */
#define ETHER_MAP_IP_MULTICAST(ipaddr, enaddr) \
	/* struct in_addr *ipaddr; */ \
	/* u_char enaddr[ETHER_ADDR_LEN];	   */ \
{ \
	(enaddr)[0] = 0x01; \
	(enaddr)[1] = 0x00; \
	(enaddr)[2] = 0x5e; \
	(enaddr)[3] = ((u_char *)ipaddr)[1] & 0x7f; \
	(enaddr)[4] = ((u_char *)ipaddr)[2]; \
	(enaddr)[5] = ((u_char *)ipaddr)[3]; \
}
/*
 * Macro to map an IP6 multicast address to an Ethernet multicast address.
 * The high-order 16 bits of the Ethernet address are statically assigned,
 * and the low-order 32 bits are taken from the low end of the IP6 address.
 */
#define ETHER_MAP_IPV6_MULTICAST(ip6addr, enaddr)			\
/* struct	in6_addr *ip6addr; */					\
/* u_char	enaddr[ETHER_ADDR_LEN]; */				\
{                                                                       \
	(enaddr)[0] = 0x33;						\
	(enaddr)[1] = 0x33;						\
	(enaddr)[2] = ((u_char *)ip6addr)[12];				\
	(enaddr)[3] = ((u_char *)ip6addr)[13];				\
	(enaddr)[4] = ((u_char *)ip6addr)[14];				\
	(enaddr)[5] = ((u_char *)ip6addr)[15];				\
}

#define ETHER_MULTICAST(enaddr) \
      ((enaddr)[0] == 0x1) && ((enaddr)[1] == 0x0) && ((enaddr)[2] == 0x5e) 

/*
 * Ethernet Address Resolution Protocol.
 */
struct	ether_arp {
	struct	arphdr ea_hdr;	/* fixed-size header */
	u_char	arp_sha[ETHER_ADDR_LEN];	/* sender hardware address */
	u_char	arp_spa[4];	/* sender protocol address */
	u_char	arp_tha[ETHER_ADDR_LEN];	/* target hardware address */
	u_char	arp_tpa[4];	/* target protocol address */
};
#define	arp_hrd	ea_hdr.ar_hrd
#define	arp_pro	ea_hdr.ar_pro
#define	arp_hln	ea_hdr.ar_hln
#define	arp_pln	ea_hdr.ar_pln
#define	arp_op	ea_hdr.ar_op

#ifdef __cplusplus
}
#endif

#endif 
