#ifndef _REWORKS_NETINET_ICMP_VAR_H_
#define _REWORKS_NETINET_ICMP_VAR_H_

#ifdef __cplusplus
extern "C"
{
#endif 

#include "ip_icmp.h"

struct	icmpstat {
/* statistics related to icmp packets generated */
	u_long	icps_error;		/* # of calls to icmp_error */
	u_long	icps_oldshort;		/* no error 'cuz old ip too short */
	u_long	icps_oldicmp;		/* no error 'cuz old was icmp */
	u_long	icps_outhist[ICMP_MAXTYPE + 1];
/* statistics related to input messages processed */
 	u_long	icps_badcode;		/* icmp_code out of range */
	u_long	icps_tooshort;		/* packet < ICMP_MINLEN */
	u_long	icps_checksum;		/* bad checksum */
	u_long	icps_badlen;		/* calculated bound mismatch */
	u_long	icps_reflect;		/* number of responses */
	u_long	icps_inhist[ICMP_MAXTYPE + 1];
	u_long	icps_bmcastecho; 	/* b/mcast echo requests dropped */
	u_long	icps_bmcasttstamp; 	/* b/mcast tstamp requests dropped */
};

/*
 * Names for ICMP sysctl objects
 */
#define	ICMPCTL_MASKREPL	1	/* allow replies to netmask requests */
#define	ICMPCTL_STATS		2	/* statistics (read-only) */
#define ICMPCTL_MAXID		3

#define ICMPCTL_NAMES { \
	{ 0, 0 }, \
	{ "maskrepl", CTLTYPE_INT }, \
	{ "stats", CTLTYPE_STRUCT }, \
}

#ifdef KERNEL
SYSCTL_DECL(_net_inet_icmp);
extern struct	icmpstat icmpstat;
#endif

#ifdef __cplusplus
}
#endif

#endif
