#ifndef _REWORKS_NETINET_IN_H_
#define _REWORKS_NETINET_IN_H_

#ifdef __cplusplus
extern "C" {
#endif

//#include <machine/endian.h>  //wn 2013.9.29
//#include <net/mbuf.h> 
#include <sys/socket.h>
#include <cpu.h>	

#include <netinet6/in6.h>
    
/*
 * Constants and structures defined by the internet system,
 * Per RFC 790, September 1981, and numerous additions.
 */

/*
 * Protocols (RFC 1700)
 */
#define	IPPROTO_IP		0		/* dummy for IP */
#define	IPPROTO_HOPOPTS		0		/* IP6 hop-by-hop options */
#define	IPPROTO_ICMP		1		/* control message protocol */
#define	IPPROTO_IGMP		2		/* group mgmt protocol */
#define IPPROTO_IPV4		4 		/* IPv4 encapsulation */
#define IPPROTO_IPIP		IPPROTO_IPV4	/* for compatibility */
#define	IPPROTO_TCP		6		/* tcp */
#define	IPPROTO_UDP		17		/* user datagram protocol */
#define	IPPROTO_IPV6		41		/* IP6 header */
#define	IPPROTO_RSVP		46 		/* resource reservation */
#define	IPPROTO_GRE		47		/* General Routing Encap. */
#define	IPPROTO_ESP		50		/* IP6 Encap Sec. Payload */
#define	IPPROTO_AH		51		/* IP6 Auth Header */
#define	IPPROTO_MOBILE		55		/* IP Mobility */
#define	IPPROTO_ICMPV6		58		/* ICMP6 */
#define	IPPROTO_OSPFIGP		89		/* OSPFIGP */
#define	IPPROTO_PIM		103		/* Protocol Independent Mcast */
#define IPPROTO_L2TP    115     /* L2TP   */    
#define	IPPROTO_MH		135		/* IPv6 Mobility Header */
#define IPPROTO_UDPLITE 136     /* UDP-Lite (RFC3828) */
/* 255: Reserved */
/* BSD Private, local use, namespace incursion */
#define	IPPROTO_RAW		255		/* raw IP packet */
#define	IPPROTO_MAX		256

/* last return value of *_input(), meaning "all job for this pkt is done".  */
#define	IPPROTO_DONE		257

/*
 * Ports < IPPORT_RESERVED are reserved for
 * privileged processes (e.g. root).         (IP_PORTRANGE_LOW)
 * Ports > IPPORT_USERRESERVED are reserved
 * for servers, not necessarily privileged.  (IP_PORTRANGE_DEFAULT)
 */
#define	IPPORT_RESERVED		1024
#define	IPPORT_USERRESERVED	5000

/*
 * Default local port range to use by setting IP_PORTRANGE_HIGH
 */
#define	IPPORT_HIFIRSTAUTO	49152
#define	IPPORT_HILASTAUTO	65535

/*
 * Scanning for a free reserved port return a value below IPPORT_RESERVED,
 * but higher than IPPORT_RESERVEDSTART.  Traditionally the start value was
 * 512, but that conflicts with some well-known-services that firewalls may
 * have a fit if we use.
 */
#define IPPORT_RESERVEDSTART	600

/*
 * Internet address (a structure for historical reasons)
 */
typedef unsigned int        in_addr_t;

struct in_addr {
	in_addr_t s_addr;
};

/*
 * Definitions of bits in internet address integers.
 * On subnets, the decomposition of addresses to host and net parts
 * is done according to subnet mask, not the masks here.
 */
#define	IN_CLASSA(i)		(((u_int32_t)(i) & 0x80000000) == 0)
#define	IN_CLASSA_NET		0xff000000
#define	IN_CLASSA_NSHIFT	24
#define	IN_CLASSA_HOST		0x00ffffff
#define	IN_CLASSA_MAX		128

#define	IN_CLASSB(i)		(((u_int32_t)(i) & 0xc0000000) == 0x80000000)
#define	IN_CLASSB_NET		0xffff0000
#define	IN_CLASSB_NSHIFT	16
#define	IN_CLASSB_HOST		0x0000ffff
#define	IN_CLASSB_MAX		65536

#define	IN_CLASSC(i)		(((u_int32_t)(i) & 0xe0000000) == 0xc0000000)
#define	IN_CLASSC_NET		0xffffff00
#define	IN_CLASSC_NSHIFT	8
#define	IN_CLASSC_HOST		0x000000ff

#define	IN_CLASSD(i)		(((u_int32_t)(i) & 0xf0000000) == 0xe0000000)
#define	IN_CLASSD_NET		0xf0000000	/* These ones aren't really */
#define	IN_CLASSD_NSHIFT	28		/* net and host fields, but */
#define	IN_CLASSD_HOST		0x0fffffff	/* routing needn't know.    */
#define	IN_MULTICAST(i)		IN_CLASSD(i)

#define	IN_EXPERIMENTAL(i)	(((u_int32_t)(i) & 0xf0000000) == 0xf0000000)
#define	IN_BADCLASS(i)		(((u_int32_t)(i) & 0xf0000000) == 0xf0000000)

#define IN_LOCAL_GROUP(i)	(((u_int32_t)(i) & 0xffffff00) == 0xe0000000)

#define	INADDR_ANY			(u_int32_t)0x00000000
#define	INADDR_LOOPBACK		(u_int32_t)0x7f000001
#define	INADDR_BROADCAST	(u_int32_t)0xffffffff	/* must be masked */
#define	INADDR_NONE			0xffffffff		/* -1 return */

#define	INADDR_UNSPEC_GROUP			(u_int32_t)0xe0000000	/* 224.0.0.0 */
#define	INADDR_ALLHOSTS_GROUP		(u_int32_t)0xe0000001	/* 224.0.0.1 */
#define	INADDR_ALLRTRS_GROUP		(u_int32_t)0xe0000002	/* 224.0.0.2 */
#define	INADDR_NEW_ALLRTRS_GROUP	(u_int32_t)0xe0000016	/* 224.0.0.22 */
#define	INADDR_MAX_LOCAL_GROUP		(u_int32_t)0xe00000ff	/* 224.0.0.255 */

#define	IN_LOOPBACKNET		127			/* official! */

#define INADDR_LINKLOCAL		0xA9FE0000	/* 169.254 */
#define IN_ADDR_IS_LINKLOCAL(i)	(((u_int32_t)(i) & 0xFFFF0000) == INADDR_LINKLOCAL)

#if _BYTE_ORDER == _BIG_ENDIAN

#define ntohl(x)        (x)
#define ntohs(x)        (x)
#define htonl(x)        (x)
#define htons(x)        (x)

#define NTOHL(x)        do {} while (FALSE)
#define NTOHS(x)        do {} while (FALSE)
#define HTONL(x)        do {} while (FALSE)
#define HTONS(x)        do {} while (FALSE)

#else

/* add by yzf 2014.01.06
 * nfs模块使用大小端转换函数出现问题，调整为bsd网络协议栈转换函数实现
 * from net/h/arch/endian.h 
 * */
#define __swap16gen(x) ({						\
	u16 __swap16gen_x = (x);					\
									\
	(u16)((__swap16gen_x & 0xff) << 8 |			\
	    (__swap16gen_x & 0xff00) >> 8);				\
})

#define __swap32gen(x) ({						\
	u32 __swap32gen_x = (x);					\
									\
	(u32)((__swap32gen_x & 0xff) << 24 |			\
	    (__swap32gen_x & 0xff00) << 8 |				\
	    (__swap32gen_x & 0xff0000) >> 8 |				\
	    (__swap32gen_x & 0xff000000) >> 24);			\
})

#define CPU_swap_u16( _value )  __swap16gen(_value)
#define CPU_swap_u32( _value )  __swap32gen(_value)


#define       ntohl(_x)        ((unsigned long)  CPU_swap_u32((unsigned32)_x))
#define       ntohs(_x)        ((unsigned short) CPU_swap_u16((unsigned16)_x))
#define       htonl(_x)        ((unsigned long)  CPU_swap_u32((unsigned32)_x))
#define       htons(_x)        ((unsigned short) CPU_swap_u16((unsigned16)_x))


/* #define ntohl(x)        ((((x) & 0x000000ff) << 24) | \
                         (((x) & 0x0000ff00) <<  8) | \
                         (((x) & 0x00ff0000) >>  8) | \
                         (((x) & 0xff000000) >> 24))

#define htonl(x)        ((((x) & 0x000000ff) << 24) | \
                         (((x) & 0x0000ff00) <<  8) | \
                         (((x) & 0x00ff0000) >>  8) | \
                         (((x) & 0xff000000) >> 24))

#define ntohs(x)        ((((x) & 0x00ff) << 8) | \
                         (((x) & 0xff00) >> 8))

#define htons(x)        ((((x) & 0x00ff) << 8) | \
                         (((x) & 0xff00) >> 8))
*/
/*
 * modified by yzf end
 * from net/h/arch/endian.h 
 * */
#define NTOHL(x)        do {(x) = ntohl((unsigned int)(x)); } while (FALSE)
#define NTOHS(x)        do {(x) = ntohs((u_short)(x)); } while (FALSE)
#define HTONL(x)        do {(x) = htonl((unsigned int)(x)); } while (FALSE)
#define HTONS(x)        do {(x) = htons((u_short)(x)); } while (FALSE)

#endif /* _BYTE_ORDER */


/*
 * Socket 地址
 */
struct sockaddr_in {
	unsigned char	sin_len;
	unsigned char	sin_family;
	unsigned short	sin_port;
	struct	in_addr sin_addr;
	char	sin_zero[8];
};

#define	INET_ADDRSTRLEN                 16

/*
 * Options for use with [gs]etsockopt at the IP level.
 * First word of comment is data type; bool is stored in int.
 */
#define	IP_OPTIONS		1    /* char[0..40]; set/get IP options */
#define	IP_HDRINCL		2    /* int; header is included with data */
#define	IP_TOS			3    /* int; IP type of service and preced. */
#define	IP_TTL			4    /* int; IP time to live */
#define	IP_RECVDSTADDR		7    /* int; receive IP dst addr w/dgram */
#define	IP_PKTINFO		8    /* int; turn packet information ancillary 
                                      * data" on/off 
                                      */
#define	IP_MULTICAST_IF		9    /* struct in_addr or struct ip_mreq or 
                                  * struct ip_mreqn. Which one is used 
                                  * is determined by size of argument.
                                  * Controls which interface multicast 
                                  * packets sent from this socket should 
                                  * use 
                                  */
#define	IP_MULTICAST_TTL	10   /* u_char; set/get IP multicast ttl */
#define	IP_MULTICAST_LOOP	11   /* u_char; set/get IP multicast loopback */
#define	IP_ADD_MEMBERSHIP	12   /* struct ip_mreq or struct ip_mreqn.
                                  * Which one that is used is determiend 
                                  * by size of argument. drop an IPv4 
                                  * group membership 
                                  */
#define IP_JOIN_GROUP       IP_ADD_MEMBERSHIP
#define IP_DROP_MEMBERSHIP  13   /* struct ip_mreq or struct ip_mreqn.
                                  * Which one that is used is determiend
                                  * by size of argument. drop an IPv4
                                  * group membershi
                                  */
#define IP_LEAVE_GROUP      IP_DROP_MEMBERSHIP
#define IP_RECVIF           20   /* int; receive reception if w/dgram */

#define	IP_ROUTER_ALERT		21   /* u_char; accept router alert packets */
#define IP_DONTFRAG             22   /* u_char; set the DF bit on each packet 
                                      * sent from this socket 
                                      */
#define IP_NOLOCALFRAG      23   /* int; enable/disable support for
                                  * local fragmentation (default off,
                                  * which means that local
                                  * fragmentation is allowed)
                                  */
#define IP_RECVTTL              31  /* int; turn "get the time-to-live 
                                     * ancillary data" on/off 
                                     */
#define IP_RECVTOS              32  /* int; turn "get the type-of-service ancillary data" on/off */
#define IP_UDP_XCHKSUM		40  /* int; Turn on/off checksum for outgoing 
				     * UDP packages (default on) 
				     */
/* NOTE: 42-47 is used by MCAST_xxx macros */
#define IP_NAT               50  /* Ipnet_nat_ctrl; Network address translation control */
#define IP_FW			51  /* Ipfirewall_ctrl; Firewall control */
#define IP_NEXTHOP		52  /* struct sockaddr_in; specify the next 
				     * hop router as ancillary data 
				     */
#define IP_X_SENDERLINKADDR	100 /* int; recvmsg() returns a struct 
				     * sockaddr_dl that will contain the link 
				     * address of the sender 
				     */
#define IP_X_VRID		101 /* u_char; specify the virtual router ID 
				     * as ancillary data 
				     */

/* MCAST_* sockopts number should not be duplicated with IPv6-level sockopts */
/* 
 * Any-Source Multicast API; cannot be mixed with Source-Specific 
 * Multicast API 
 */
#define	MCAST_JOIN_GROUP           42  /* join an any-source group */
#define	MCAST_BLOCK_SOURCE         43  /* block a given source */
#define	MCAST_UNBLOCK_SOURCE       44  /* unblock a given source */
#define	MCAST_LEAVE_GROUP          45  /* leave an any-source group */

/* 
 * Source-Specific Multicast API; cannot be mixed with Any-Source 
 * Multicast API 
 */
#define	MCAST_JOIN_SOURCE_GROUP     46  /* join a source-specific group */
#define	MCAST_LEAVE_SOURCE_GROUP    47  /* leave a source-specific grou */

/*
 * Defaults and limits for options
 */
#define	IP_DEFAULT_MULTICAST_TTL  1	/* normally limit m'casts to 1 hop  */
#define	IP_DEFAULT_MULTICAST_LOOP 1	/* normally hear sends if a member  */
#define	IP_MAX_MEMBERSHIPS	20	/* per socket */
#define	IP_MAX_SOURCE_FILTER	128	/* max number of MSF per group */
#define	SO_MAX_SOURCE_FILTER	64	/* max number of MSF per socket */

/*
 * Argument structure for IP_ADD_MEMBERSHIP and IP_DROP_MEMBERSHIP.
 */
struct ip_mreq {
	struct	in_addr imr_multiaddr;	/* IP multicast address of group */
	struct	in_addr imr_interface;	/* local IP address of interface */
};

#define	MCAST_INCLUDE		0	/* Exclude filter mode */
#define	MCAST_EXCLUDE		1	/* Include filter mode */

/*
 * Argument structure for IP_ADD_MEMBERSHIP and IP_DROP_MEMBERSHI.
 */
struct ip_mreqn
{
    struct  in_addr   imr_multiaddr;   /* IPv4 class D multicast addr */
    struct  in_addr   imr_interface;   /* IPv4 address of local interface */
    unsigned int         imr_ifindex;     /* Interface index of local interface */
};

/*
 * Argument structure for MCAST_JOIN_GROUP and MCAST_LEAVE_GROUP
 */
struct group_req {
	unsigned int   gr_interface;	/* interface index */
	struct sockaddr_storage	gr_group;	/* group address */
};

/*
 * Argument structure for MCAST_BLOCK_SOURCE, MCAST_UNBLOCK_SOURCE, 
 * MCAST_JOIN_SOURCE_GROUP and MCAST_LEAVE_SOURCE_GROUP
 */
struct group_source_req {
	unsigned int gsr_interface;	/* interface index */
	struct  sockaddr_storage	gsr_group;	/* group address */
	struct  sockaddr_storage	gsr_source;	/* source address */
};
/*
 * Ancillary data for IP_PKTINFO. It can be use toghether with
 * sendmsg() to specify egress interface and/or source address. It
 * will be returned by recvmsg() if reception of packet information
 * has been enabled via the IP_PKTINFO socket option.
 */
struct in_pktinfo {
    u_int32_t      ipi_ifindex; /* egress/ingress interface index */
    struct in_addr ipi_addr;    /* local address of packet */
};

/*
 * Argument for IP_PORTRANGE:
 * - which range to search when port is unspecified at bind() or connect()
 */
#define	IP_PORTRANGE_DEFAULT	0	/* default range */
#define	IP_PORTRANGE_HIGH	1	/* "high" - request firewall bypass */
#define	IP_PORTRANGE_LOW	2	/* "low" - vouchsafe security */

/* Packet reassembly stuff */
#define IPREASS_NHASH_LOG2      6
#define IPREASS_NHASH           (1 << IPREASS_NHASH_LOG2)
#define IPREASS_HMASK           (IPREASS_NHASH - 1)
#define IPREASS_HASH(x,y) \
	(((((x) & 0xF) | ((((x) >> 8) & 0xF) << 4)) ^ (y)) & IPREASS_HMASK)

#define RTQ_TIMEOUT	60*10	/* run no less than once every ten minutes */

/*
 * Definitions for inet sysctl operations.
 *
 * Third level is protocol number.
 * Fourth level is desired variable within that protocol.
 */
#define	IPPROTO_MAXID	(IPPROTO_AH + 1)	/* don't list to IPPROTO_MAX */

/* INET6 stuff, temporary workaround */
#define	__KAME_NETINET_IN_H_INCLUDED_
#undef __KAME_NETINET_IN_H_INCLUDED_

void     inet_ntoa_b (struct in_addr, char *);
char 	*inet_ntoa (struct in_addr); /* in libkern */ 

#ifdef __cplusplus
}
#endif
    
#endif
