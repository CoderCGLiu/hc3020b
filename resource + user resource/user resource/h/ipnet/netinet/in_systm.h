#ifndef _REWORKS_NETINET_IN_SYSTM_H_
#define _REWORKS_NETINET_IN_SYSTM_H_

#ifdef __cplusplus
extern "C"
{
#endif 

#include <autoconf.h>

/*
 * Network types.
 *
 * Internally the system keeps counters in the headers with the bytes
 * swapped so that VAX instructions will work on them.  It reverses
 * the bytes before transmission at each protocol level.  The n_ types
 * represent the types with the bytes in ``high-ender'' order.
 */
typedef u_int16_t n_short;		/* short as received from the net */
typedef u_int32_t n_long;		/* long as received from the net */

typedef	u_int32_t n_time;		/* ms since 00:00 GMT, byte rev */

//#ifdef _WRS_KERNEL
//n_time	 iptime __P((void));
//#endif

#ifdef __cplusplus
}
#endif

#endif
