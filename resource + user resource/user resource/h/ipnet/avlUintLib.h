#ifndef __INCavlUintLibh
#define __INCavlUintLibh

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _ASMLANGUAGE

/* 
 * Binary tree node definition that uses an unsigned integer as the sorting 
 * key.
 */
typedef struct avlu_node
    {
    struct avlu_node *  left;	/* pointer to the left subtree */
    struct avlu_node *  right;	/* pointer to the right subtree */
    int    height; 		/* height of the subtree rooted at this node */
    unsigned int   key;			/* sorting key */
    } AVLU_NODE;

typedef AVLU_NODE * AVLU_TREE;	/* points to the root node of the tree */

/* callback routines for avlUintTreeWalk */
typedef int (*AVLU_CALLBACK)(AVLU_NODE *pNode, void * pArg);

int      avlUintInsert (AVLU_TREE * pRoot, AVLU_NODE * pNode);
AVLU_NODE * avlUintDelete (AVLU_TREE * pRoot, unsigned int key);
AVLU_NODE * avlUintSearch (AVLU_TREE root, unsigned int key);
AVLU_NODE * avlUintSuccessorGet (AVLU_TREE root, unsigned int key);
AVLU_NODE * avlUintPredecessorGet (AVLU_TREE root, unsigned int key);
AVLU_NODE * avlUintMinimumGet (AVLU_TREE root);
AVLU_NODE * avlUintMaximumGet (AVLU_TREE root);
int      avlUintTreeWalk (AVLU_TREE pRoot, AVLU_CALLBACK preRtn, 
			     void * preArg, AVLU_CALLBACK inRtn, void * inArg,
			     AVLU_CALLBACK postRtn, void * postArg);

#endif /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif 
