#ifndef _RTOS_BSDNET_
#define _RTOS_BSDNET_

#ifdef __cplusplus
extern "C"
{
#endif 

#include <reworks/types.h>
#include <sys/cdefs.h>
#include <netinet/in.h>

#if defined(__INSIDE_RTOS_BSD_TCPIP_STACK__)
#undef _COMPILING_BSD_KERNEL_
#undef KERNEL
#undef INET
#undef NFS
#undef DIAGNOSTIC
#undef BOOTP_COMPAT

#define _COMPILING_BSD_KERNEL_
#define KERNEL
#define INET
#define NFS
#define DIAGNOSTIC
#define BOOTP_COMPAT
#endif

/*
 * Values that may be obtained by BOOTP
 */
extern struct in_addr rtos_bsdnet_bootp_server_address;
extern char *rtos_bsdnet_bootp_server_name;
extern char *rtos_bsdnet_bootp_boot_file_name;
extern char *rtos_bsdnet_bootp_cmdline;
extern struct in_addr rtos_bsdnet_ntpserver[];
extern int rtos_bsdnet_ntpserver_count;
extern long rtos_bsdnet_timeoffset;
extern  void network_init(int pollmode);          

/*
 * Manipulate routing tables
 */
struct sockaddr;
struct rtentry;
int rtos_bsdnet_rtrequest (
    int req,
    struct sockaddr *dst,
    struct sockaddr *gateway,
    struct sockaddr *netmask,
    int flags,
    struct rtentry **net_nrt);

/*
 * Diagnostics
 */
void routes_show (void);
void mbuf_show (void);
void if_show (void);
void ip_show (void);
void icmp_show (void);
void udp_show (void);
void tcp_show (void);

void arp_show(void);
int arp_add(char* host_ip,char* eth_addr);
int arp_delete(char* host_ip);

/*
 * Network configuration
 */
struct rtos_bsdnet_ifconfig {
	/*
	 * These three entries must be supplied for each interface.
	 */
	char		*name;

	/*
	 * This function now handles attaching and detaching an interface.
	 * The parameter attaching indicates the operation being invoked.
	 * For older attach functions which do not have the extra parameter
	 * it will be ignored.
	 */
	int		(*attach)(struct rtos_bsdnet_ifconfig *conf, int attaching);

	/*
	 * Link to next interface
	 */
	struct rtos_bsdnet_ifconfig *next;

	/*
	 * The following entries may be obtained
	 * from BOOTP or explicitily supplied.
	 */
	char		*ip_address;
	char		*ip_netmask;
	void		*hardware_address;

	/*
	 * The driver assigns defaults values to the following
	 * entries if they are not explicitly supplied.
	 */
	int		ignore_broadcast;
	int		mtu;
	int		rbuf_count;
	int		xbuf_count;

	/*
	 * For external ethernet controller board the following
	 * parameters are needed
	 */
	unsigned int	port;   /* port of the board */
	unsigned int	irno;   /* irq of the board */
	unsigned int	bpar;   /* memory of the board */

  /*
   * Driver control block pointer. Typcially this points to the driver's
   * controlling structure. You set this when you have the structure allocated
   * externally to the driver.
   */
  void *drv_ctrl;

};
int ifconfig(char *cmdstring);
//int ifconfig(char *name , char *ip_address , char *ip_netmask);
void netdriver_add(struct rtos_bsdnet_ifconfig * driver_config);

struct rtos_bsdnet_config {
	/*
	 * This entry points to the head of the ifconfig chain.
	 */
	struct rtos_bsdnet_ifconfig *ifconfig;

	/*
	 * This entry should be OSBsdnetBootpDo if BOOTP
	 * is being used to configure the network, and NULL
	 * if BOOTP is not being used.
	 */
	void			(*bootp)(void);

	/*
	 * The remaining items can be initialized to 0, in
	 * which case the default value will be used.
	 */
	unsigned32	network_task_priority;	/* 100		*/
	unsigned long		mbuf_bytecount;		/* 64 kbytes	*/
	unsigned long		mbuf_cluster_bytecount;	/* 128 kbytes	*/
	char			*hostname;		/* BOOTP	*/
	char			*domainname;		/* BOOTP	*/
	char			*gateway;		/* BOOTP	*/
	char			*log_host;		/* BOOTP	*/
	char			*name_server[3];	/* BOOTP	*/
	char			*ntp_server[3];		/* BOOTP	*/
};

/*
 * Default global device configuration structure. This is scanned
 * by the initialize network function. Check the network demo's for
 * an example of the structure. Like the RTOS configuration tables,
 * they are not part of RTOS but part of your application or bsp
 * code.
 */
extern struct rtos_bsdnet_config rtos_bsdnet_config;

/*
 * Initialise the BSD stack, attach and `up' interfaces
 * in the `rtos_bsdnet_config'. RTOS must already be initialised.
 */
int OSBsdnetInitNetwork (void);

/*
 * Dynamic interface control. Drivers must free any resources such as
 * memory, interrupts, io regions claimed during the `attach' and/or
 * `up' operations when asked to `detach'.
 * You must configure the interface after attaching it.
 */
void rtos_bsdnet_attach (struct rtos_bsdnet_ifconfig *ifconfig);
void rtos_bsdnet_detach (struct rtos_bsdnet_ifconfig *ifconfig);

/*
 * Interface configuration. The commands are listed in `sys/sockio.h'.
 */
int rtos_bsdnet_ifconfig (const char *ifname, unsigned32 cmd, void *param);

void OSBsdnetBootpDo (void);
void OSBsdnetBootpAndRootfsDo (void);

int rtos_bsdnet_synchronize_ntp (int interval, unsigned32 priority);

/*
 * Callback to report BSD malloc starvation.
 * The default implementation just prints a message but an application
 * can provide its own version.
 */
void rtos_bsdnet_malloc_starvation(void);

/* these two are like the netcmd "route add/del" */
OS_STATUS route_add(char *destination, char *netmask, char *gateway);
OS_STATUS route_del(char *destination, char *netmask, char *gateway);

#ifdef __cplusplus
}
#endif

#endif /* _RTOS_BSDNET_ */
