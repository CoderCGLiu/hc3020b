/* iplstLib.h - doubly linked list library header */

/* Copyright 1984-2001 Wind River Systems, Inc. */

/*
modification history
--------------------
01n,19sep01,pcm  added lstLibInit () (SPR 20698)
01m,02apr93,edm  ifdef'd out non-ASMLANGUAGE portions
01l,22sep92,rrr  added support for c++
01k,04jul92,jcf  cleaned up.
01j,26may92,rrr  the tree shuffle
01i,04oct91,rrr  passed through the ansification filter
		  -changed VOID to void
		  -changed copyright notice
01h,23oct90,shl  included "vxWorks.h" so include file order isn't crucial.
01g,05oct90,shl  added ANSI function prototypes.
                 made #endif ANSI style.
                 added copyright notice.
01f,10aug90,dnw  added declaration of lstInsert().
01e,07aug90,shl  added IMPORT type to function declarations.
01d,21may86,llk	 added forward declaration of lstNStep.
01c,03jun84,dnw  changed list.{head,tail} to list.node.
		 added declarations of lst{First,Last,Next,Previous}.
01b,27jan84,ecs  added inclusion test.
01b,15mar83,dnw  changed name from lstlb to lstLib
*/

#ifndef __INIPClstLibh
#define __INIPClstLibh

#ifndef _ASMLANGUAGE

#ifdef __cplusplus
extern "C" {
#endif

#include <net_types.h>


/* function declarations */

#if defined(__STDC__) || defined(__cplusplus)

extern void	iplstLibInit (void);
extern IPNODE *	iplstFirst (IPLIST *pList);
extern IPNODE *	iplstGet (IPLIST *pList);
extern IPNODE *	iplstLast (IPLIST *pList);
extern IPNODE *	iplstNStep (IPNODE *pNode, int nStep);
extern IPNODE *	iplstNext (IPNODE *pNode);
extern IPNODE *	iplstNth (IPLIST *pList, int nodenum);
extern IPNODE *	iplstPrevious (IPNODE *pNode);
extern int 	iplstCount (IPLIST *pList);
extern int 	iplstFind (IPLIST *pList, IPNODE *pNode);
extern void 	iplstAdd (IPLIST *pList, IPNODE *pNode);
extern void 	iplstConcat (IPLIST *pDstList, IPLIST *pAddList);
extern void 	iplstDelete (IPLIST *pList, IPNODE *pNode);
extern void 	iplstExtract (IPLIST *pSrcList, IPNODE *pStartNode, IPNODE *pEndNode,
	  		    IPLIST *pDstList);
extern void 	iplstFree (IPLIST *pList);
extern void 	iplstInit (IPLIST *pList);
extern void 	iplstInsert (IPLIST *pList, IPNODE *pPrev, IPNODE *pNode);

#else	/* __STDC__ */

extern void	iplstLibInit ();
extern IPNODE *	iplstFirst ();
extern IPNODE *	iplstGet ();
extern IPNODE *	iplstLast ();
extern IPNODE *	iplstNStep ();
extern IPNODE *	iplstNext ();
extern IPNODE *	iplstNth ();
extern IPNODE *	iplstPrevious ();
extern int 	iplstCount ();
extern int 	iplstFind ();
extern void 	iplstAdd ();
extern void 	iplstConcat ();
extern void 	iplstDelete ();
extern void 	iplstExtract ();
extern void 	iplstFree ();
extern void 	iplstInit ();
extern void 	iplstInsert ();

#endif	/* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif /* ~ _ASMLANGUAGE */

#endif /* __INClstLibh */
