#ifndef _REWORKS_SYS_SOCKET_H_
#define	_REWORKS_SYS_SOCKET_H_

#ifdef __cplusplus
 extern "C" {
#endif

#include <drv/netBufLib.h>     
#include <sys/uio.h>
/*
 * Definitions related to sockets: types, address families, options.
 */
/*
 * ��������
 */
typedef unsigned char sa_family_t;
#ifdef SOCKLEN_T_UNSIGNED
typedef	unsigned int socklen_t;
#else
typedef	int socklen_t;
#endif

/*
 * Types
 */
#define	SOCK_STREAM	1		/* stream socket */
#define	SOCK_DGRAM	2		/* datagram socket */
#define	SOCK_RAW	3		/* raw-protocol interface */
#define	SOCK_RDM	4		/* reliably-delivered message */
#define	SOCK_SEQPACKET	5	/* sequenced packet stream */
#define	SOCK_PACKET	10		/* linux packet socket API. */

/*
 * Option flags per-socket.
 */
#define	SO_DEBUG		0x0001		/* turn on debugging info recording */
#define	SO_REUSEADDR	0x0004		/* allow local address reuse */
#define	SO_KEEPALIVE	0x0008		/* keep connections alive */
#define	SO_DONTROUTE	0x0010		/* just use interface addresses */
#define	SO_RCVLOWAT     0x0012		/* receive low-water mark */
#define	SO_SNDLOWAT     0x0013		/* send low-water mark */
#define SO_SNDTIMEO		0x1005		/* send timeout */
#define	SO_ACCEPTCONN	0x001e		/* socket has had listen() */
#define	SO_BROADCAST	0x0020		/* permit sending of broadcast msgs */
#define	SO_USELOOPBACK	0x0040		/* bypass hardware when possible */
#define	SO_LINGER		0x0080		/* linger on close if data present */
#define	SO_REUSEPORT	0x0200		/* allow local address & port reuse */

#define SO_VLAN         0x8000      /* get/set VLAN ID and user priority */

/*
 * Additional options, not kept in so_options.
 */
#define SO_SNDBUF	0x1001		/* send buffer size */
#define SO_RCVBUF	0x1002		/* receive buffer size */
#define SO_RCVTIMEO	0x1006		/* receive timeout */
#define	SO_ERROR	0x1007		/* get error status and clear */
#define	SO_TYPE		0x1008		/* get socket type */
#define	SO_BINDTODEVICE	0x1010		/* bind to specific device */
#define	SO_OOBINLINE	0x1011		/* leave received OOB data in line */
#define SO_CONNTIMEO    0x100a          /* connection timeout for */

/* Various SOL_SOCKET options from ipcom. See ipcom_sock.h. */

#define SO_X_VR           0x1020  /* int; Set virtual router */
#define SO_X_PROBEDELAY   0x1021  /* int; number of seconds a connection
				          (with keep-alive active) must be idle
					  before the first probe is sent */
#define SO_X_COOKIE       0x1022  /* void *; socket IPCOM port specific
				             cookie */

/* SO_X_EVENTCB_RO is not usable in VxWorks. */

#define SO_X_PKT_MPRIO    0x1025  /* int; ipcom_pkt_malloc() priority */
#define SO_X_BYPASS_FLOW  0x1026  /* int; Turn on/off bypass flow */
#define SO_X_DRAINABLE    0x1027  /* int;
				     ==0 packets in the receive queue must not
				         be freed even in low memory situations
					 (default)
                                     !=0 the stack might discard already queued
				         packets in low memory situations */
#define SO_X_GRE_KEY      0x1028  /* struct Ip_gre_key;
				     set the GRE flow key for traffic sent from
				     this socket */

/*
 * Structure used for manipulating linger option.
 */
struct	linger {
	int	l_onoff;		/* option on/off */
	int	l_linger;		/* linger time */
};

/*
 * Structure used for manipulating GRE key option
 */
struct gre_key {
    int      gk_onoff;  /* Option on/off */
    uint32_t gk_key;    /* RFC 2890 GRE key in host byte order */
};

/*
 * VxWorks-specific socket structure for manipulating SO_VLAN option
 */
struct sovlan {

    /*
     * If so_onff is set, the vlan id and/or user priority will be copied
     * to the socket structure and SO_VLAN so_option will be set. If so_onff
     * is not set, the SO_VLAN so_option for the given socket will be cleared.
     */
    int	vlan_onoff;		/* option on/off */

    /* 
     * The priority_tagged boolean must be set to true if application using
     * socket-based vlan requires to egress 802.1P priority-tagged frame
     * (i.e. the value of vid is zero). Defaults to false. If set to true,
     * the value specified by the vid will be ignored.
     */
    boolean priority_tagged; 

    unsigned short vid;  /* VLAN ID, valid vid: 1..4094 */
    unsigned short upriority;   /* User Priority, valid priority: 0..7 */
};

/*
 * Level number for (get/set)sockopt() to apply to socket itself.
 */
#define	SOL_SOCKET	0xffff		/* options for socket level */

/*
 * Address families.
 */
#define	AF_UNSPEC	0		/* unspecified */
#define	AF_LOCAL	1		/* local to host (pipes, portals) */
#define	AF_UNIX		AF_LOCAL	/* backward compatibility */
#define	AF_INET		2		/* internetwork: UDP, TCP, etc. */
#define	AF_NETLINK	16		/* Linux Netlink, RFC 3549 */
#define	AF_ROUTE	17		/* Internal Routing Protocol */
#define	AF_LINK		18		/* Link layer interface */
#define	AF_PACKET	19		/* Linux packet family */
#define	pseudo_AF_KEY	27		/* Internal key-management function */
#define	AF_KEY		pseudo_AF_KEY	/* backward compatibility */
#define	AF_INET6	28		/* IPv6 */
#define	AF_MPLS		30		/* Used by the IPMPLS module */
#define	AF_SOCKDEV	31		/* Socket connected to a network interface */
#define AF_TIPC		33		/* Tranparent Inter-Process Communic. */
#define	AF_MIPC		34		/* Multi-OS Inter-Processor Communic. */
#define AF_MIPC_SAFE    35              /* SafeIPC */
#define AF_MAX          36

/*
 * Structure used by kernel to store most
 * addresses.
 */
struct sockaddr {// wn 2012.5.11 //wn 2012.7.30
	unsigned char	sa_len;		/* total length */
	sa_family_t	sa_family;	/* address family */
	char		sa_data[14];	/* actually longer; address value */
};

#define	SOCK_MAXADDRLEN	255		/* longest possible addresses */
typedef struct sockaddr SOCKADDR; //wn 2012.7.30

/*
 * RFC 2553: protocol-independent placeholder for socket addresses
 */
#define	_SS_MAXSIZE		32
#define	_SS_ALIGNSIZE	(sizeof(unsigned int))
#define	_SS_PAD1SIZE	(_SS_ALIGNSIZE - sizeof(unsigned char) - sizeof(sa_family_t))
#define	_SS_PAD2SIZE	(_SS_MAXSIZE - sizeof(unsigned char) - sizeof(sa_family_t) - \
				_SS_PAD1SIZE - _SS_ALIGNSIZE)

struct sockaddr_storage {
	unsigned char		ss_len;		/* address length */
	sa_family_t	ss_family;	/* address family */
	char		__ss_pad1[_SS_PAD1SIZE];
	signed int  __ss_align;	/* force desired structure storage alignment */
	char		__ss_pad2[_SS_PAD2SIZE];
};

#ifdef INET6
typedef struct sockaddr_storage SOCKADDR_STORAGE_T;
#else
typedef struct sockaddr         SOCKADDR_STORAGE_T;
#endif /* INET6 */

/*
 * Protocol families, same as address families for now.
 */
#define	PF_UNSPEC	AF_UNSPEC
#define	PF_LOCAL	AF_LOCAL
#define	PF_UNIX		PF_LOCAL	/* backward compatibility */
#define	PF_INET		AF_INET
#define	PF_NETLINK	AF_NETLINK
#define	PF_ROUTE	AF_ROUTE
#define	PF_LINK		AF_LINK
#define	PF_PACKET	AF_PACKET
#define	PF_KEY		AF_KEY
#define	PF_INET6	AF_INET6
#define	PF_MPLS		AF_MPLS
#define	PF_SOCKDEV	AF_SOCKDEV
#define	PF_TIPC		AF_TIPC
#define PF_MIPC		AF_MIPC
#define PF_MIPC_SAFE	AF_MIPC_SAFE
#define	PF_MAX		AF_MAX

/*
 * Maximum queue length specifiable by listen.
 */
#define	SOMAXCONN	128


/* Networking sysctl's */
     
#define NET_RT_DUMP     1               /* dump; may limit to a.f. */
#define NET_RT_FLAGS    2               /* by flags, e.g. RESOLVING */
#define NET_RT_IFLIST   3               /* survey interface list */
#define NET_RT_MAXID    4


/*
 * Message header for recvmsg and sendmsg calls.
 * Used value-result for recvmsg, value only for sendmsg.
 */
struct msghdr {
	void		*msg_name;		/* optional address */
	socklen_t	 msg_namelen;		/* size of address */
	struct iovec	*msg_iov;		/* scatter/gather array */
	int		 msg_iovlen;		/* # elements in msg_iov */
	void		*msg_control;		/* ancillary data, see below */
	socklen_t	 msg_controllen;	/* ancillary data buffer len */
	int		 msg_flags;		/* flags on received message */
};

#define	MSG_OOB		0x1		/* process out-of-band data */
#define	MSG_PEEK	0x2		/* peek at incoming message */
#define	MSG_DONTROUTE	0x4		/* send without using routing tables */
#define	MSG_EOR		0x8		/* data completes record */
#define	MSG_TRUNC	0x10		/* data discarded before delivery */
#define	MSG_CTRUNC	0x20		/* control data lost before delivery */
#define	MSG_WAITALL	0x40		/* wait for full request or error */
#define	MSG_DONTWAIT	0x80		/* this message should be nonblocking */
#define	MSG_EOF		0x100		/* data completes connection */
#define MSG_EXP         0x200           /* COMP-only: send express message */
#define MSG_MBUF        0x400           /* WRS extension */
#define MSG_NOTIFICATION 0x800          /* notification message */
#define MSG_COMPAT      0x8000		/* used in sendit() */


/*
 * Header for ancillary data objects in msg_control buffer.
 * Used for additional information with/about a datagram
 * not expressible by flags.  The format is a sequence
 * of message elements headed by cmsghdr structures.
 */
struct cmsghdr {
	socklen_t	cmsg_len;		/* data byte count, including hdr */
	int		cmsg_level;		/* originating protocol */
	int		cmsg_type;		/* protocol-specific type */
/* followed by	u_char  cmsg_data[]; */
};

#define _ALIGNBYTES (sizeof (long) - 1)
#define _ALIGN(p)   (((unsigned long) (p) + _ALIGNBYTES) & ~_ALIGNBYTES)

/* given pointer to struct cmsghdr, return pointer to data */
#define	CMSG_DATA(cmsg)		((u_char *)(cmsg) + \
				 _ALIGN(sizeof(struct cmsghdr)))

/* given pointer to struct cmsghdr, return pointer to next cmsghdr */
#define	CMSG_NXTHDR(mhdr, cmsg)	\
	(((caddr_t)(cmsg) + _ALIGN((cmsg)->cmsg_len) + \
	  _ALIGN(sizeof(struct cmsghdr)) > \
	    (caddr_t)(mhdr)->msg_control + (mhdr)->msg_controllen) ? \
	    (struct cmsghdr *)NULL : \
	    (struct cmsghdr *)((caddr_t)(cmsg) + _ALIGN((cmsg)->cmsg_len)))

#define	CMSG_FIRSTHDR(mhdr)	((struct cmsghdr *)(mhdr)->msg_control)

/* RFC 2292 additions */
	
#define	CMSG_SPACE(l)		(_ALIGN(sizeof(struct cmsghdr)) + _ALIGN(l))
#define	CMSG_LEN(l)		(_ALIGN(sizeof(struct cmsghdr)) + (l))

#define	CMSG_ALIGN(n)	_ALIGN(n)

/*
 * howto arguments for shutdown(2), specified by Posix.1g.
 */
#define	SHUT_RD		0		/* shut down the reading side */
#define	SHUT_WR		1		/* shut down the writing side */
#define	SHUT_RDWR	2		/* shut down both sides */

#if defined(__STDC__) || defined(__cplusplus)
extern int   sockLibInit (void);
extern int 	accept (int s, struct sockaddr *addr, socklen_t *addrlen);
extern int 	bind (int s, const struct sockaddr *name, socklen_t namelen);
extern int 	connect (int s, const struct sockaddr *name, socklen_t namelen);
extern int 	connectWithTimeout (int sock, const struct sockaddr *adrs,
				    socklen_t adrsLen, const struct timeval *timeVal);
extern int 	getpeername (int s, struct sockaddr *name, socklen_t *namelen);
extern int 	getsockname (int s, struct sockaddr *name, socklen_t *namelen);
extern int 	getsockopt (int s, int level, int optname, void *optval,
			    socklen_t *optlen);
extern int 	listen (int s, int backlog);


extern ssize_t 	recv (int s, void *buf, size_t bufLen, int flags);
extern ssize_t 	recvfrom (int s, void *buf, size_t bufLen, int flags,
			  struct sockaddr *from, socklen_t *pFromLen);
extern ssize_t 	recvmsg (int sd, struct msghdr *mp, int flags);
extern ssize_t 	send (int s, const void *buf, size_t bufLen, int flags);
extern ssize_t 	sendmsg (int sd, const struct msghdr *mp, int flags);
extern ssize_t 	sendto (int s, const void *buf, size_t bufLen, int flags,
			const struct sockaddr *to, socklen_t tolen);
extern int 	setsockopt (int s, int level, int optname, const void *optval,
			    socklen_t optlen);

extern int 	shutdown (int s, int how);
extern int 	socket (int domain, int type, int protocol);

#else	/* __STDC__ */
#ifdef _WRS_KERNEL
extern int 	sockLibInit ();
extern int 	sockLibAdd ();
#endif

extern int 	bind ();
extern int 	connect ();
extern int 	connectWithTimeout ();
extern int 	getpeername ();
extern int 	getsockname ();
extern int 	getsockopt ();
extern int 	listen ();
extern int 	setsockopt ();
extern int 	shutdown ();
extern int 	accept ();
extern int 	recv ();
extern int 	recvfrom ();
extern int 	recvmsg ();
extern int 	send ();
extern int 	sendmsg ();
extern int 	sendto ();
extern int 	socket ();

#endif	/* __STDC__ */
#ifdef __cplusplus
}
#endif
    
#endif 
