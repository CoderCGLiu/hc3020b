#ifndef _REWORKS_NET_ROUTE_LIB_H_
#define _REWORKS_NET_ROUTE_LIB_H_

#ifdef __cplusplus
extern "C" {
#endif

/* ״̬�� */
#define S_routeLib_ILLEGAL_INTERNET_ADDRESS		(M_routeLib | 1)
#define S_routeLib_ILLEGAL_NETWORK_NUMBER		(M_routeLib | 2)

extern int 	routeAdd (char *destination, char *gateway);
extern int 	routeDelete (char *destination, char *gateway);
extern int 	routeNetAdd (char *destination, char *gateway);
extern int 	routeCmd (int destInetAddr, int gateInetAddr, int ioctlCmd);
extern int   mRouteAdd (char *, char *, unsigned int, int, int);
extern int   mRouteEntryAdd (unsigned int , unsigned int, unsigned int, int, int, int);
extern int   mRouteEntryDelete (unsigned int , unsigned int, unsigned int, int, int, int);
extern int   mRouteDelete (char *, unsigned int, int, int);
extern void     oldRouteLibInit(void);

#ifdef __cplusplus
}
#endif

#endif
