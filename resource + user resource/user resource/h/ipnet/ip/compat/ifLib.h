#ifndef _REWORKS_NET_IF_LIB_H_
#define _REWORKS_NET_IF_LIB_H_

#ifdef __cplusplus
extern "C" {
#endif

extern void	    ifLibInit (void);
extern int   ifAddrAdd (char *interfaceName, char *interfaceAddress,
                           char *broadcastAddress, int subnetMask);
extern int   ifAddrDelete (char *interfaceName, char *interfaceAddress);
extern int 	ifAddrSet (char *interfaceName, char *interfaceAddress);
extern int 	ifAddrGet (char *interfaceName, char *interfaceAddress);
extern int 	ifBroadcastSet (char *interfaceName, char *broadcastAddress);
extern int 	ifBroadcastGet (char *interfaceName, char *broadcastAddress);
extern int 	ifDstAddrSet (char *interfaceName, char *dstAddress);
extern int 	ifDstAddrGet (char *interfaceName, char *dstAddress);
extern int 	ifMaskSet (char *interfaceName, int netMask);
extern int 	ifMaskGet (char *interfaceName, int *netMask);
extern int 	ifFlagChange (char *interfaceName, int flags, int on);
extern int 	ifFlagSet (char *interfaceName, int flags);
extern int 	ifFlagGet (char *interfaceName, int *flags);
extern int 	ifMetricSet (char *interfaceName, int metric);
extern int 	ifMetricGet (char *interfaceName, int *pMetric);

extern int   ifProxyArpDisable (char *interfaceName);
extern int   ifProxyArpEnable (char *interfaceName);

extern int   ifAllRoutesDelete(char *ifName, int unit);

extern int   ifIndexToIfName (unsigned short ifIndex, char * ifname);
extern unsigned short ifNameToIfIndex (char * ifname);

extern int arpResolve ( char  *targetAddr, char  *pHwAddr, int   numTries, int   numTicks);

#ifdef __cplusplus
}
#endif

#endif
