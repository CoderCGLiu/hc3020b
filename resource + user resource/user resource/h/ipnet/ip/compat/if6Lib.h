#ifndef _REWORKS_NET_IF6_LIB_H_
#define _REWORKS_NET_IF6_LIB_H_

#ifdef __cplusplus
extern "C" {
#endif

#if defined(__STDC__) || defined(__cplusplus)
extern void if6LibInit(void);
extern int if6AddrGet(char *interfaceName, char *interfaceAddress);
extern int if6AddrAdd(char *interfaceName, char *interfaceAddress,
			 int prefixLen, int flags);
extern int if6AddrDelete(char *interfaceName, char *interfaceAddress);
extern int if6DstAddrGet(char *interfaceName, char *interfaceAddress,
			    char *dstAddress);
extern int if6DstAddrSet(char *interfaceName, char *interfaceAddress,
			    char *dstAddress);
extern int if6PrefixlenGet(char *interfaceName, char *interfaceAddress,
			      int *prefixLen);
extern int if6PrefixlenSet(char *interfaceName, char *interfaceAddress,
			      int prefixLen);
extern int if6FlagGet(char *interfaceName, char *interfaceAddress,
			 int *flags);
extern int if6FlagSet(char *interfaceName, char *interfaceAddress,
			 int flags);
extern int if6FlagChange(char *interfaceName, char *interfaceAddress,
			    int flags, int on);
extern int if6LifetimeGet(char *interfaceName, char *interfaceAddress,
		unsigned int  *vltime, unsigned int *pltime);
extern int if6LifetimeSet(char *interfaceName, char *interfaceAddress,
		unsigned int  vltime, unsigned int pltime);
#else	/* __STDC__ */
extern void if6LibInit();
extern int if6AddrGet();
extern int if6AddrAdd();
extern int if6AddrDelete();
extern int if6DstAddrGet();
extern int if6DstAddrSet();
extern int if6PrefixlenGet();
extern int if6PrefixlenSet();
extern int if6FlagGet();
extern int if6FlagSet();
extern int if6FlagChange();
extern int if6LifetimeGet();
extern int if6LifetimeSet();
#endif	/* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif 
