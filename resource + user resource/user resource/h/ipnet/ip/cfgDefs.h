#ifndef _REWORKS_NET_CFG_H_
#define _REWORKS_NET_CFG_H_

#ifdef __cplusplus
extern "C" {
#endif

/* all default configuration data structures must have this structure at
   the beginning */
typedef struct cfg_data_hdr{
	int len;
} CFG_DATA_HDR;

#ifdef __cplusplus
}
#endif

#endif
