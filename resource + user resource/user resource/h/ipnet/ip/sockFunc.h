#ifndef __INCsockFunch
#define __INCsockFunch

#ifdef __cplusplus
extern "C" {
#endif

//#ifdef _WRS_KERNEL  //wn 2012.8.21

/* includes */
//#include <netVersion.h> // wn 2012.7.25
//#include <sys/times.h>
#include <sys/socket.h>
//#if defined (_WRS_VXWORKS_MAJOR) && (_WRS_VXWORKS_MAJOR >= 6) //wn 2012.8.1
//#include <ioLib.h> /* For type DEV_HDR */
//#else //wn 2012.8.1
//#include <iosLib.h>
//#endif /* defined (_WRS_VXWORKS_MAJOR) && (_WRS_VXWORKS_MAJOR >= 6) */

/* HIDDEN */

typedef struct sockFunc SOCK_FUNC;

struct socket;  /* defined in net/socketvar.h */

typedef SOCK_FUNC * (*SOCK_FUNC_INIT) (void);

typedef int (*SOCK_FUNC_ACCEPT)
    (struct socket **pso,
     struct sockaddr * addr,
     socklen_t * pAddrlen);

typedef STATUS (*SOCK_FUNC_BIND)
    (struct socket * so,
     const struct sockaddr * name,
     socklen_t namelen);

typedef STATUS (*SOCK_FUNC_CONNECT)
    (struct socket * so,
     const struct sockaddr * name,
     socklen_t namelen);

typedef STATUS (*SOCK_FUNC_CONN_TIMEO)
    (struct socket * so,
     const struct sockaddr * name,
     socklen_t namelen,
     const struct timeval * tv);

typedef STATUS (*SOCK_FUNC_GETPEERNAME)
    (struct socket * so,
     struct sockaddr * addr,
     socklen_t * pAddrlen);

typedef STATUS (*SOCK_FUNC_GETSOCKNAME)
    (struct socket * so,
     struct sockaddr * addr,
     socklen_t * pAddrlen);

typedef STATUS (*SOCK_FUNC_LISTEN)
    (struct socket * so,
     int backlog);

typedef ssize_t (*SOCK_FUNC_RECV)
    (struct socket * so,
     void * buf,
     size_t bufLen,
     int flags);

typedef ssize_t (*SOCK_FUNC_RECVFROM)
    (struct socket * so,
     void * buf,
     size_t bufLen,
     int flags,
     struct sockaddr * from,
     socklen_t * pFromLen);

typedef ssize_t (*SOCK_FUNC_RECVMSG)
    (struct socket * so,
     struct msghdr * msg,
     int flags);

typedef ssize_t (*SOCK_FUNC_SEND)
    (struct socket * so,
     const void * buf,
     size_t bufLen,
     int flags);

typedef ssize_t (*SOCK_FUNC_SENDTO)
    (struct socket * so,
     const void * buf,
     size_t bufLen,
     int flags,
     const struct sockaddr * name,
     socklen_t namelen);

typedef ssize_t (*SOCK_FUNC_SENDMSG)
    (struct socket * so,
     const struct msghdr * msg,
     int flags);

typedef STATUS (*SOCK_FUNC_SHUTDOWN)
    (struct socket * so,
     int how);

typedef STATUS (*SOCK_FUNC_SOCKET)
    (int domain,
     int type,
     int protocol,
     struct socket ** pso);

typedef STATUS (*SOCK_FUNC_GETSOCKOPT)
    (struct socket * so,
     int level,
     int optname,
     void * optval,
     socklen_t * optlen);

typedef STATUS (*SOCK_FUNC_SETSOCKOPT)
    (struct socket * so,
     int level,
     int optname,
     const void * optval,
     socklen_t optlen);

typedef BOOL (*SOCK_FUNC_ZBUF) (void);  /* Does the back end support zbufs? */

typedef int (*SOCK_FUNC_CLOSE)
    (struct socket * so);

typedef ssize_t (*SOCK_FUNC_WRITE)
    (struct socket * so,
     const char * buf,
     size_t maxBytes);

typedef ssize_t (*SOCK_FUNC_READ)
    (struct socket * so,
     char * buf,
     size_t maxBytes);

typedef int (*SOCK_FUNC_IOCTL)
    (struct socket *,
     int code,
     _ip_usr_arg_t arg,
     void * nullForKernel); /* last arg is NULL if from kernel, non-NULL for
			       system call originating in user space. */

struct sockFunc			/* SOCK_FUNC */
    {
    SOCK_FUNC_INIT	libInitRtn;		/* sockLibInit()	     */
    SOCK_FUNC_ACCEPT	acceptRtn;		/* accept()		     */
    SOCK_FUNC_BIND	bindRtn;		/* bind()		     */
    SOCK_FUNC_CONNECT	connectRtn;		/* connect()		     */
    SOCK_FUNC_CONN_TIMEO	connectWithTimeoutRtn;	/* connectWithTimeout()	     */
    SOCK_FUNC_GETPEERNAME	getpeernameRtn;		/* getpeername()	     */
    SOCK_FUNC_GETSOCKNAME	getsocknameRtn;		/* getsockname()	     */
    SOCK_FUNC_LISTEN	listenRtn;		/* listen()		     */
    SOCK_FUNC_RECV	recvRtn;		/* recv()		     */
    SOCK_FUNC_RECVFROM	recvfromRtn;		/* recvfrom()		     */
    SOCK_FUNC_RECVMSG	recvmsgRtn;		/* recvmsg()		     */
    SOCK_FUNC_SEND	sendRtn;		/* send()		     */
    SOCK_FUNC_SENDTO	sendtoRtn;		/* sendto()		     */
    SOCK_FUNC_SENDMSG	sendmsgRtn;		/* sendmsg()		     */
    SOCK_FUNC_SHUTDOWN	shutdownRtn;		/* shutdown()		     */
    SOCK_FUNC_SOCKET	socketRtn;		/* socket()		     */
    SOCK_FUNC_GETSOCKOPT	getsockoptRtn;		/* getsockopt()		     */
    SOCK_FUNC_SETSOCKOPT	setsockoptRtn;		/* setsockopt()		     */
    SOCK_FUNC_ZBUF     zbufRtn;		/* ZBUF support 	     */

    /*
     * The following IO-system handlers are called via wrappers in
     * sockLib.c:
     */
    SOCK_FUNC_CLOSE     closeRtn;               /* close()                   */
    SOCK_FUNC_READ	readRtn;		/* read()                    */
    SOCK_FUNC_WRITE     writeRtn;		/* write()                   */
    SOCK_FUNC_IOCTL	ioctlRtn;		/* ioctl()                   */
    } ;

//extern DEV_HDR socketDevHdr;   /* I/O system device driver header */  //wn 2012.3.19

/* END_HIDDEN */

/* Memory validation function pointers */

//extern int    (*pSockIoctlMemVal) (unsigned int cmd, void * data);
//extern STATUS (*pUnixIoctlMemVal) (unsigned int cmd, const void * pData);

//#endif /* _WRS_KERNEL */

#ifdef __cplusplus
}
#endif

#endif /* __INCsockFunch */

