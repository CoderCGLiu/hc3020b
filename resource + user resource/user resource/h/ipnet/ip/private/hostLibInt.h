#ifndef _REWORKS_NET_HOST_LIB_INT_H_ 
#define _REWORKS_NET_HOST_LIB_INT_H_ 

#ifdef __cplusplus
extern "C"
{
#endif

struct _hostconf {
    FUNCPTR    hostGetByAddr;
    FUNCPTR    dnsGetByAddr;
};

#define HOST_LIST_SIZE   1
#define HOST_LIST_BUF    2

#if defined(__STDC__) || defined(__cplusplus)

/* external functions that are shared within hostLib */

extern struct addrinfo *hostTblSearchByName2 (char *name, const struct addrinfo *hints);
extern struct hostent *hostTblSearchByAddr2 (const void *addr, int af);
extern struct hostent *getipnodebyaddr (const void *src, size_t len, int af, int *errp);
#ifdef INET6
extern int ip6_str2scopeid (char *scope, struct sockaddr_in6 *sin6, u_int32_t *);
#endif
extern struct hostent * _hpcopy (struct hostent * hp);
extern struct netent *getnetbyname (const char *name);

#else	/* __STDC__ */

extern struct addrinfo *hostTblSearchByName2 ();
extern struct hostent *hostTblSearchByAddr2 ();
extern struct hostent *getipnodebyaddr ();
#ifdef INET6
extern int ip6_str2scopeid ();
#endif
extern struct hostent *_hpcopy ();

extern struct netent *getnetbyname ();

#endif	/* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif 
