#ifndef _REWORKS_WRAPPER_HOST_LIB_H_
#define _REWORKS_WRAPPER_HOST_LIB_H_

#ifdef __cplusplus
extern "C" {
#endif


extern int sethostname(const char *name,  size_t nameLen);

extern int gethostname(char *name,  size_t nameLen);



#ifdef __cplusplus
}
#endif

#endif 
