#ifndef _REWORKS_NET_SOCK_LIB_H_
#define _REWORKS_NET_SOCK_LIB_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/times.h>
#include <sys/socket.h>

/* typedefs */
#include <ip/sockFunc.h>
typedef struct sockLibMap
    {
    int			domainMap;	/* mapping address family  */
    int			domainReal;	/* real address family     */
    SOCK_FUNC *		pSockFunc;	/* socket function table   */
    struct sockLibMap *	pNext;		/* next socket lib mapping */
    } SOCK_LIB_MAP;
	
struct sockFunc; /* See sockFunc.h */

typedef struct sockFunc * (*SOCK_LIB_INIT_RTN) (void);

    

/* function declarations */
extern int   sockLibInit (void);
//extern int 	sockLibAdd (RE_FUNCPTR sockLibInitRtn, int domainMap,
//                            int domainReal);
extern int 	accept (int s, struct sockaddr *addr, socklen_t *addrlen);
extern int 	bind (int s, const struct sockaddr *name, socklen_t namelen);
extern int 	connect (int s, const struct sockaddr *name, socklen_t namelen);
extern int 	connectWithTimeout (int sock, const struct sockaddr *adrs,
				    socklen_t adrsLen, const struct timeval *timeVal);
extern int 	getpeername (int s, struct sockaddr *name, socklen_t *namelen);
extern int 	getsockname (int s, struct sockaddr *name, socklen_t *namelen);
extern int 	getsockopt (int s, int level, int optname, void *optval,
			    socklen_t *optlen);
extern int 	listen (int s, int backlog);


extern ssize_t 	recv (int s, void *buf, size_t bufLen, int flags);
extern ssize_t 	recvfrom (int s, void *buf, size_t bufLen, int flags,
			  struct sockaddr *from, socklen_t *pFromLen);
extern ssize_t 	recvmsg (int sd, struct msghdr *mp, int flags);
extern ssize_t 	send (int s, const void *buf, size_t bufLen, int flags);
extern ssize_t 	sendmsg (int sd, const struct msghdr *mp, int flags);
extern ssize_t 	sendto (int s, const void *buf, size_t bufLen, int flags,
			const struct sockaddr *to, socklen_t tolen);
extern int 	setsockopt (int s, int level, int optname, const void *optval,
			    socklen_t optlen);
extern int 	shutdown (int s, int how);
extern int 	socket (int domain, int type, int protocol);

#ifdef __cplusplus
}
#endif

#endif 
