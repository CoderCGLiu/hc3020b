#ifndef _REWORKS_NET_HOST_LIB_H_
#define _REWORKS_NET_HOST_LIB_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <netinet/in.h>
#include <sys/socket.h>
#include <netdb.h>

/*add by yzf end 2013.10.11*/
//#include <vwModNum.h>

#include <net/if.h>

#ifdef INET6
#include <netinet6/in6.h>
#endif /* INET6 */


#ifndef MAXHOSTNAMELEN
#define MAXHOSTNAMELEN 	64    /* XXX already defined in param.h */
#endif

#define HOST_INET6_ADDR_LEN (USR_NET_IP6ADDR_LEN + IFNAMESIZ)

/* status messages */

#define S_hostLib_UNKNOWN_HOST			(M_hostLib | 1)
#define S_hostLib_HOST_ALREADY_ENTERED		(M_hostLib | 2)
#define S_hostLib_INVALID_PARAMETER             (M_hostLib | 3)
/* former h_errno values */
#define S_hostLib_NETDB_INTERNAL                (M_hostLib | 4)  /* see errno */
/* Authoritative Answer Host not found */
#define S_hostLib_HOST_NOT_FOUND                (M_hostLib | 5)
/* Non-Authoritative Host not found, or SERVERFAIL */
#define S_hostLib_TRY_AGAIN                     (M_hostLib | 6)
/* Valid name, no data record of requested type */
#define S_hostLib_NO_RECOVERY                   (M_hostLib | 7)

/* Definitions for sysctl nodes under CTL_NET */

#define HOSTLIB    3
#define NAMEINFO   4
#define ADDRINFO   5    

/* Options for the host library sysctl handler */

#define HOSTADD          1
#define HOSTDELETE       2
#define HOSTADDRSEARCH   3
#define HOSTNAMESEARCH   4
#define HOSTNAMESEARCH2  5

/* Current hostentAlloc() limits: */

/* space allocated for address in hostent::h_addr_list[0] */
#define HOSTENT_ALLOC_MAXADDRS	24
/* space allocated for name in hostent::h_name */
#define HOSTENT_ALLOC_MAXNAME	(NI_MAXHOST + 1)
/* Reentrant implementation, requiring hostentFree() of result? */
#define GETHOSTBYNAME_REENTRANT
#define GETHOSTBYADDR_REENTRANT

/*
 * Structure shared by hostAdd, hostDelete and hostTblSearchByName2
 * for passing arguments to sysctl.
 */
struct hostArgs
    {
    char * hostName;
    int    hostNameLen;
    char * hostAddr;
    int    hostAddrLen;
    const struct addrinfo * pHints;
    };

/*
 * This structure is used to pass the getnameinfo arguments from user-space
 * to the kernel using sysctl.
 */
struct nameinfoArgs
    {
    const struct sockaddr * sa;
    socklen_t               salen;
    char *                  host;
    size_t                  hostlen;
    char *                  serv;
    size_t                  servlen;
    int                     flags;
    };

/*
 * This structure is used to pass the getaddrinfo arguments from user-space
 * to the kernel using sysctl.
 */
struct addrinfoArgs
    {
    const char 		   * hostname;
    int			     hostnamelen;
    const char             * servname;
    int			     servnamelen;
    const struct addrinfo  * hints;
    };

/* typdef's for hosttable access */
typedef struct hostname_struct
    {
    struct hostname_struct *link;
    char *name;
    } HOSTNAME;

typedef struct addr4_struct
    {
    struct in_addr addr;
    } ADDR4;

#ifdef INET6
typedef struct addr6_struct
    {
    struct in6_addr addr;
    uint32_t scopeId;
    char scopeStr [IFNAMSIZ];
    } ADDR6;
#endif

typedef union addr_union
    {
    ADDR4 addr4;
#ifdef INET6
    ADDR6 addr6;
#endif
    } ADDRU;

typedef struct
    {
    IPNODE node;
    HOSTNAME hostName;
    int family;                   /* AF_INET or AF_INET6 */
    ADDRU netAddr;                /* ADDR4 or ADDR6 */
    } HOSTENTRY;

/* function declarations */

#if defined(__STDC__) || defined(__cplusplus)

extern int 	hostAdd (char *hostName, char *hostAddr); 
extern int 	hostDelete (char *name, char *addr);
extern int 	hostGetByAddr (int addr, char *name);
extern int 	hostGetByName (char *name);
extern struct hostent * hostentAlloc (void);
extern void hostentFree (struct hostent *);
extern struct	 hostent *_dns_ghbyaddr(const void *addr, int addrlen, int af, int *errp);

extern int 	hostTblInit (void);
extern int   hostTblSetup (char * pRemoteName, char * pRemoteAddr,
			      char * pLocalName, char * pLocalAddr);
extern int      hostTblSearchByName (char *name);
extern int   hostTblSearchByAddr (int addr, char *name);
extern int   rtpHostLibInit (void);

#else	/* __STDC__ */

extern int 	hostAdd ();
extern int 	hostDelete ();
extern int 	hostGetByAddr ();
extern int 	hostGetByName ();
extern struct hostent * hostentAlloc ();
extern void hostentFree ();
extern struct   hostent *_dns_ghbyaddr();


extern int 	hostTblInit ();
extern int   hostTblSetup ();
extern int      hostTblSearchByName ();
extern int   hostTblSearchByAddr ();
extern int   rtpHostLibInit ();

#endif	/* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif 
