#ifndef _REWORKS_NET_INET_LIB_H_
#define _REWORKS_NET_INET_LIB_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <netinet/in.h>
#include <sys/types.h>
//#include <vwModNum.h>

/* 状态码 */
#define S_inetLib_ILLEGAL_INTERNET_ADDRESS		(M_inetLib | 1)
#define S_inetLib_ILLEGAL_NETWORK_NUMBER		(M_inetLib | 2)

/* length of ASCII represention of inet addresses, eg. "90.0.0.0" */
#define	INET_ADDR_LEN	18
#define INET6_ADDR_LEN  46

#define INETLIB         5 /* first available node */

#if defined(__STDC__) || defined(__cplusplus)

//extern int inet_lnaof (int inetAddress);
//extern int inet_netof (struct in_addr inetAddress);
//extern unsigned long inet_network (char *);
//extern struct in_addr inet_makeaddr (int, int); // 和 arpa/inet.h 重复定义了
	
extern void inet_makeaddr_b (int netAddr, int hostAddr, 
                             struct in_addr *pInetAddr);
extern void inet_ntoa_b (struct in_addr inetAddress, char *pString);

#else	/* __STDC__ */

//extern int inet_lnaof ();
//extern int inet_netof ();
//extern unsigned long 	inet_network ();
//struct in_addr inet_makeaddr();
extern void inet_makeaddr_b ();
extern void inet_ntoa_b ();
extern int inet_aton ();

#endif	/* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif 
