/* etherLib.h - ethernet hook routines header */

#ifndef __INCetherLibh
#define __INCetherLibh

//#include <netVersion.h>

#ifdef __cplusplus
extern "C" {
#endif

/* defints */
typedef struct enet_hdr
    {
    char dst [6];
    char src [6];
    USHORT type;
    } ENET_HDR;

#define ENET_HDR_SIZ        sizeof(ENET_HDR)
#define ENET_HDR_REAL_SIZ   14

#ifdef __cplusplus
}
#endif

#endif /* __INCetherLibh */
