#ifndef _REWORKS_NET_TYPES_H_
#define _REWORKS_NET_TYPES_H_


#ifdef __cplusplus

#ifndef _M_FUNCPTR
#define _M_FUNCPTR
typedef int 		(*FUNCPTR) (...);     /* ptr to function returning int */
#endif

#ifndef _M_VOIDFUNCPTR
#define _M_VOIDFUNCPTR
typedef void 		(*VOIDFUNCPTR) (...); /* ptr to function returning void */
#endif

#ifndef _M_DBLFUNCPTR
#define _M_DBLFUNCPTR
typedef double 		(*DBLFUNCPTR) (...);  /* ptr to function returning double*/
#endif

#ifndef _M_FLTFUNCPTR
#define _M_FLTFUNCPTR
typedef float 		(*FLTFUNCPTR) (...);  /* ptr to function returning float */
#endif

extern "C" {
#endif

#include <reworks/types.h>

#define OK		0
#define ERROR		(-1)

#ifndef RE_NET_TYPES
#define RE_NET_TYPES
#endif

typedef void *		(*IPNET_FUNCPTR) (void * );
typedef int 		(*RE_FUNCPTR) ();
typedef void    (*RE_VOIDFUNCPTR)(void *);


#ifdef _WRS_CONFIG_LP64
typedef long _ip_usr_arg_t;
#else
typedef int _ip_usr_arg_t;
#endif /* _WRS_CONFIG_LP64 */

typedef unsigned int _ip_ticks_t;
typedef unsigned long long _ip_ticks64_t;

typedef struct _ipnode /* Node of a linked list. */
{
	struct _ipnode *next; /* Points at the next node in the list */
	struct _ipnode *previous; /* Points at the previous node in the list */
} IPNODE;

typedef struct /* Header for a linked list. */
{
	IPNODE node; /* Header list node */
	int count; /* Number of nodes in list */
} IPLIST;


#ifndef _M_INT8
typedef	char		INT8;
#define _M_INT8
#endif

#ifndef _M_INT16
typedef	short		INT16;
#define _M_INT16
#endif

#ifndef _M_INT32
typedef	int		INT32;
#define _M_INT32
#endif

#ifndef _M_INT64
typedef	long long	INT64;
#define _M_INT64
#endif

#ifndef _M_UINT8
typedef	unsigned char	UINT8;
#define _M_UINT8
#endif

#ifndef _M_UINT16
typedef	unsigned short	UINT16;
#define _M_UINT16
#endif

#ifndef _M_UINT32
typedef	unsigned int	UINT32;
#define _M_UINT32
#endif

#ifndef _M_UINT64
typedef	unsigned long long UINT64;
#define _M_UINT64
#endif

#ifndef _M_UCHAR
typedef	unsigned char	UCHAR;
#define _M_UCHAR
#endif

#ifndef _M_USHORT
typedef unsigned short	USHORT;
#define _M_USHORT
#endif

#ifndef _M_UINT
typedef	unsigned int	UINT;
#define _M_UINT
#endif

#ifndef _M_ULONG
typedef unsigned long	ULONG;
#define _M_ULONG
#endif


#ifndef _M_BOOL
typedef	int		BOOL;
#define _M_BOOL
#endif

#ifndef _M_STATUS
typedef	int		STATUS;
#define _M_STATUS
#endif

#ifndef _M_ARGINT
typedef int 		ARGINT;
#define _M_ARGINT
#endif

#ifndef _M_VOID
typedef void		VOID;
#define _M_VOID
#endif




#ifndef IMPORT
#define IMPORT extern
#endif

#ifdef __cplusplus
}
#endif

#endif /* _REWORKS_NET_TYPES_H_ */
