#ifndef NETINIT_H_
#define NETINIT_H_

#ifdef __cplusplus
extern "C" {
#endif

#define IPCOM_LOG_DEBUG

void net_module_init();

int if_addr_set(char *interfaceName, char *interfaceAddress);
int if_mask_set(char *interfaceName, int netMask);
int if_flag_change(char *interfaceName, int flags, int on);
int ip_attach(int unit, char * pDevice);

extern int ifconfig (char *);
extern int netstat (char * options);
extern int routec (char *options);

//extern void 	arpShow (void);
extern void 	hostShow (void);
extern void     hostShowInit (void);
extern void 	ifShow (char *ifName);

extern int ipAttach (int unit, char * pDevice);
extern int ipDetach (int unit, char * pDevice);

extern int arp(char * paramString );
extern int pcap(char * paramString );

extern int ttcp(char * paramString );
extern int tracert(char * paramString );
extern int sockperf(char * paramString );

extern int ipd(char * paramString );

extern int 	ping6 (char *, int);
extern int ndp(char * paramString );

#ifdef __cplusplus
}
#endif

#endif
