#ifndef _REWORKS_NET_NETDB_H_
#define _REWORKS_NET_NETDB_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <reworks/types.h>
#include "sys/socket.h"    /* for socklen_t */ 

/*
 * Structures returned by network data base library.  All addresses are
 * supplied in host order, and returned in network order (suitable for
 * use in system calls).
 */
struct	hostent {
	char	*h_name;	/* official name of host */
	char	**h_aliases;	/* alias list */
	int	h_addrtype;	/* host address type */
	int	h_length;	/* length of address */
	char	**h_addr_list;	/* list of addresses from name server */
#define	h_addr	h_addr_list[0]	/* address, for backward compatiblity */
};

/*
 * Assumption here is that a network number
 * fits in an unsigned long -- probably a poor one.
 */
struct	netent {
	char		*n_name;	/* official name of net */
	char		**n_aliases;	/* alias list */
	int		n_addrtype;	/* net address type */
	unsigned long	n_net;		/* network # */
};

struct	servent {
	char	*s_name;	/* official service name */
	char	**s_aliases;	/* alias list */
	int	s_port;		/* port # */
	char	*s_proto;	/* protocol to use */
};

struct	protoent {
	char	*p_name;	/* official protocol name */
	char	**p_aliases;	/* alias list */
	int	p_proto;	/* protocol # */
};

struct addrinfo 
    {
    int     ai_flags;           /* AI_PASSIVE, AI_CANONNAME, AI_NUMERICHOST */
    int     ai_family;          /* PF_xxx */
    int     ai_socktype;        /* SOCK_xxx */
    int     ai_protocol;        /* 0 or IPPROTO_xxx for IPv4 and IPv6 */
    size_t  ai_addrlen;         /* length of ai_addr */
    char   *ai_canonname;       /* canonical name for hostname */
    struct sockaddr *ai_addr;   /* binary address */
    struct addrinfo *ai_next;   /* next structure in linked list */
    };

/*
 * Error return codes from getaddrinfo()
 */
/* #define	EAI_ADDRFAMILY	 1	(obsoleted) */
#define	EAI_AGAIN	 2	/* temporary failure in name resolution */
#define	EAI_BADFLAGS	 3	/* invalid value for ai_flags */
#define	EAI_FAIL	 4	/* non-recoverable failure in name resolution */
#define	EAI_FAMILY	 5	/* ai_family not supported */
#define	EAI_MEMORY	 6	/* memory allocation failure */
/* #define	EAI_NODATA	 7	(obsoleted) */
#define	EAI_NONAME	 8	/* hostname nor servname provided, or not known */
#define	EAI_SERVICE	 9	/* servname not supported for ai_socktype */
#define	EAI_SOCKTYPE	10	/* ai_socktype not supported */
#define	EAI_SYSTEM	11	/* system error returned in errno */
#define	EAI_BADHINTS	12	/* Invalid value for hints */
#define	EAI_PROTOCOL	13	/* Resolved protocol is unknown */
#define	EAI_OVERFLOW	14	/* Argument buffer overflow */
#define	EAI_MAX		15

/*
 * Flag values for getaddrinfo()
 */
#define	AI_PASSIVE	0x00000001 /* get address to use bind() */
#define	AI_CANONNAME	0x00000002 /* fill ai_canonname */
#define	AI_NUMERICHOST	0x00000004 /* prevent name resolution */
#define	AI_ALL		0x00000100 /* IPv6 and IPv4-mapped (with AI_V4MAPPED) */
#define	AI_V4MAPPED_CFG	0x00000200 /* accept IPv4-mapped if kernel supports */
#define	AI_V4MAPPED	0x00000800 /* accept IPv4-mapped IPv6 address */

/* valid flags for getaddrinfo (not a standard def, apps should not use it) */
#define AI_MASK \
    (AI_PASSIVE | AI_CANONNAME | AI_NUMERICHOST | AI_NUMERICSERV | \
     AI_ADDRCONFIG | AI_ALL | AI_V4MAPPED_CFG | AI_V4MAPPED)

#define	AI_ADDRCONFIG	0x00000400 /* only if any address is assigned */
#define AI_NUMERICSERV	0x00001000 /* prevent service resolution */

/* special recommended flags for getipnodebyname */
#define	AI_DEFAULT	AI_V4MAPPED_CFG | AI_ADDRCONFIG

/*
 * Constants for getnameinfo()
 */
#define	NI_MAXHOST	1025
#define	NI_MAXSERV	32

/*
 * Flag values for getnameinfo()
 */
#define	NI_NOFQDN	0x00000001
#define	NI_NUMERICHOST	0x00000002
#define	NI_NAMEREQD	0x00000004
#define	NI_NUMERICSERV	0x00000008
#define	NI_DGRAM	0x00000010
#if 0 /* obsolete */
#define NI_WITHSCOPEID	0x00000020
#endif
#define NI_NUMERICSCOPE	0x00000040

/*
 * Scope delimit character
 */
#define	SCOPE_DELIMITER	'%'

/*
 * Error return codes from res_query.c
 */
#define	HOST_NOT_FOUND	1 /* Authoritative Answer Host not found */
#define	TRY_AGAIN	2 /* Non-Authoritive Host not found, or SERVERFAIL */
#define	NO_RECOVERY	3 /* Non recoverable errors, FORMERR, REFUSED, NOTIMP */
#define	NO_DATA		4 /* Valid name, no data record of requested type */
#define	NO_ADDRESS	NO_DATA		/* no address, look for MX record */

extern int    getnameinfo (const struct sockaddr *sa, socklen_t salen, char *host,
                           size_t hostlen, char *serv, size_t servlen, int flags);
extern int    getaddrinfo (const char *hostname, const char *servname,
                           const struct addrinfo *hints, struct addrinfo **result);
extern void   freeaddrinfo (struct addrinfo * ai);
extern char * gai_strerror (int ecode);

/*add by yangzifeng beijing*/
void		endhostent (void);
void		endnetent (void);
void		endprotoent (void);
void		endservent (void);
struct hostent	*gethostbyaddr (const char *, int, int);
struct hostent	*gethostbyname (const char *);
struct hostent	*gethostbyname2 (const char *, int);
struct hostent	*gethostent (void);
struct netent	*getnetbyaddr (unsigned long, int);
struct netent	*getnetbyname (const char *);
struct netent	*getnetent (void);
struct protoent	*getprotobyname (const char *);
struct protoent	*getprotobynumber (int);
struct protoent	*getprotoent (void);
struct servent	*getservbyname (const char *, const char *);
struct servent	*getservbyport (int, const char *);
struct servent	*getservent (void);
void		herror (const char *);
__const char	*hstrerror (int);
void		sethostent (int);
void		setnetent (int);
void		setprotoent (int);
void		setservent (int);
void		initialHlist (); // by huangyuan
void		destroyHlist (); // by huangyuan
//int			hostAdd(char* hostName, char* hostAddr);	// by huangyuan
//int			hostDelete(char* hostName, char* hostAddr); // by huangyuan
/*add by yangzifeng end beijing*/	




#ifdef __cplusplus
}
#endif

#endif 
