/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了ReWorks FTP server 所需的类型和接口定义
 * 修改：
 * 		 2013-9-25，规范
 * 
 */
#ifndef _REWORKS_NET_FTP_H_
#define	_REWORKS_NET_FTP_H_

#ifdef __cplusplus
extern "C"
{
#endif

/*
 * 应答状态码首位数字及其含义
 */
#define PRELIM		1	/* 命令正确，服务器在接收下一条命令之前将发回另外一条响应信息 */
#define COMPLETE	2	/* 命令完全正确，服务器等待接收下一条请求命令 */
#define CONTINUE	3	/* 命令已正确接收，但是服务器需要进一步的信息 */
#define TRANSIENT	4	/* 命令发生了暂时错误，可以随后再次输入同样的命令 */
#if 0
#define ERROR		5	/* 命令发生了永久性错误，不能再次输入同样的命令 */
#endif

/*
 * 文件类型，FTP 可在数据连接上传输以下 3 种类型的文件
 */
#define	TYPE_A		1	/* ASCII 文件，这是传输文本文件的默认类型*/
#define	TYPE_E		2	/* EBCDIC 文件，作为双方使用 EBCDIC 编码情况下的文件类型*/
#define	TYPE_I		3	/* image 文件，这是传输二进制文件的默认类型 */
#define	TYPE_L		4	/* 本地文件类型，该类型在具有不同字节大小的主机间传输二进制文件 */

#ifdef FTP_NAMES
char *typenames[] =  {"0", "ASCII", "EBCDIC", "Image", "Local" };
#endif

/*
 * 格式控制，该选项仅对 ASCII 饥饿 EBCDIC 文件类型有效
 */
#define	FORM_N		1	/* 非打印（默认选择）文件中不含有垂直格式信息 */
#define	FORM_T		2	/* 远程登录（telnet）格式控制，文件含有向打印机解释的远程登录垂直格式控制 */
#define	FORM_C		3	/* Fortran 回车控制 (ASA)，每行首字符是 Fortran 格式控制符 */
#ifdef FTP_NAMES
char *formnames[] =  {"0", "Nonprint", "Telnet", "Carriage-control" };
#endif

/*
 * 数据结构，FTP 可以在数据连接上使用如下结构之一，对传输的文件数据进行解释
 */
#define	STRU_F		1	/* 文件结构，这是默认结构，实际代表被传输的文件没有任何结构，即被传输的文件是连续字节流 */
#define	STRU_R		2	/* 记录结构，被传输的文件可以划分为多个记录集（结构体），只能用于文本文件传输 */
#define	STRU_P		3	/* 页结构，被传输的文件分为多个页，每页都有页号和页头 */
#ifdef FTP_NAMES
char *strunames[] =  {"0", "File", "Record", "Page" };
#endif

/*
 * 传输模式
 */

/* 流模式，这是默认方式，
 * 数据被看做连续的字节流从 FTP 层向 TCP 提交，
 * TCP 负责将字节流按照合适的大小组成为 TCP 分段 */
#define	MODE_S		1	

/* 块模式，数据被看做块从 FTP 层向 TCP 提交，
 * 每个数据块被附上一个 3 个字节的头部，
 * 头部的第一个字节称为块描述符，
 * 后 2 个字节定义该数据块的大小 */
#define	MODE_B		2	

/* 压缩模式，如果文件较大，
 * 则可以压缩后再传输。
 * 对于文本文件，通常空格被压缩，
 * 对于二进制文件，通常 null 字符被压缩 */
#define	MODE_C		3	

#ifdef FTP_NAMES
char *modenames[] =  {"0", "Stream", "Block", "Compressed" };
#endif

/*
 * Record Tokens
 */
#define	REC_ESC		'\377'	/* Record-mode Escape */
#define	REC_EOR		'\001'	/* Record-mode End-of-Record */
#define REC_EOF		'\002'	/* Record-mode End-of-File */

/*
 * Block Header
 */
#define	BLK_EOR		0x80	/* Block is End-of-Record */
#define	BLK_EOF		0x40	/* Block is End-of-File */
#define BLK_ERRORS	0x20	/* Block is suspected of containing errors */
#define	BLK_RESTART	0x10	/* Block is Restart Marker */

#define	BLK_BYTECOUNT	2	/* Bytes in this block */

#ifdef __cplusplus
}
#endif

#endif 
