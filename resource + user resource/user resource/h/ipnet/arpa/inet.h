#ifndef _ARPA_INET_H_
#define	_ARPA_INET_H_

#ifdef __cplusplus
extern "C"
{
#endif

/* External definitions for functions in inet(3), addr2ascii(3) */

#include <sys/types.h>
#include <sys/cdefs.h>
#include <netinet/in.h>

struct in_addr;

__BEGIN_DECLS
uint32_t	 inet_addr (const char *);
int		 inet_aton (const char *, struct in_addr *);
int		 inet_lnaof (int);
struct in_addr	 inet_makeaddr (int , int);
int	 inet_netof (struct in_addr);
unsigned long	 inet_network (register char *);
char		*inet_ntoa (struct in_addr);
int          inet_pton (int, const char *, void *);
const char	*inet_ntop (int, const void *, char *, size_t);
__END_DECLS

#ifdef __cplusplus
}
#endif

#endif /* !_INET_H_ */
