#ifndef IPNET_FLUX_INFO_H_
#define IPNET_FLUX_INFO_H_

#ifdef __cplusplus
extern "C" {
#endif

struct net_flux_info
{
	int if_count;//网卡数
	char if_name[16];//网卡名
	int if_baudrate;//传输速率，只有SLIP接口才设置baudrate
	long if_ibytes;//RX bytes
	long if_obytes;//TX bytes
	
};
void ipnet_get_flux_info(struct net_flux_info* flux_info);
int ipnet_get_ifcount();

#ifdef __cplusplus
}
#endif

#endif
