#ifndef _REWORKS_MUX_LIB_H_
#define _REWORKS_MUX_LIB_H_

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include <drv/vxcompat/end.h>

/* defints */

/* MUX_MAX_IFTYPE associates with IFT_MAX_TYPE as default. If the user wants
 * to add a resource function, verify it's smaller than MUX_MAX_IFTYPE.
 */

#define MUX_MAX_IFTYPE M2_ifType_pmp

/* Error defines. */
#define S_muxLib_LOAD_FAILED                  (M_muxLib | 1)
#define S_muxLib_NO_DEVICE                    (M_muxLib | 2)
#define S_muxLib_INVALID_ARGS                 (M_muxLib | 3)
#define S_muxLib_ALLOC_FAILED                 (M_muxLib | 4)
#define S_muxLib_ALREADY_BOUND                (M_muxLib | 5)
#define S_muxLib_UNLOAD_FAILED                (M_muxLib | 6)
#define S_muxLib_NOT_A_TK_DEVICE              (M_muxLib | 7)
#define S_muxLib_NO_TK_DEVICE                 (M_muxLib | 8)
#define S_muxLib_END_BIND_FAILED              (M_muxLib | 9)
#define S_muxLib_MISSING_WRAPPERS	      (M_muxLib | 10)


/* These are above all SAPs but still in the ethernet size range */
/* so won't interfere with RFC 1700 */
#define MUX_PROTO_PROMISC	0x100	
#define MUX_PROTO_SNARF         0x101	
#define MUX_PROTO_OUTPUT        0x102

/*
 * Note that these bind styles are correlated in number
 * and order with the END styles in endCommon.h
 */
#define BIND_STYLE_MUX2BIND	0
#define BIND_STYLE_MUXBIND	1
#define BIND_STYLE_MUXTKBIND	2

/* NUM_BIND_STYLES is defined as 3 in end.h */

/* 
 * The following defines are used for the various if-protocol functions 
 * that are maintained by the MUX function table.
 */

#define ADDR_RES_FUNC           1
#define IF_OUTPUT_FUNC          2
#define MULTI_ADDR_RES_FUNC     4

/* typedefs */

/* structure passed for SIOCMUXPASSTRHU ioctl */

typedef struct _MUX_PASSTHRU
    {
    char name[END_NAME_MAX];	/* device name, without unit number */
    int unit;			/* unit number. */
    u_int cmd;			/* Tunnelled ioctl command. Must properly
				   encode size and IOC_IN/IOC_OUT/IOC_VOID,
				   and IOC_USER must be set if RTP access is
				   allowed. */
    void * pData;		/* Tunnelled ioctl data argument */
    } MUX_PASSTHRU;


/*
 * These data structures define the 2D list of address resolution
 * functions.  This list is ordered by ifType and the protocol.
 */
typedef struct mux_addr_rec
    {
    IPNODE node;
    long protocol;
    int  setFuncs;
    FUNCPTR addrResFunc;
    FUNCPTR ifOutputFunc;
    FUNCPTR multiAddrResFunc;
    } MUX_ADDR_REC;

/* function pointers for scalability */

typedef struct _MUX_FUNCS
    {
    void * (*endTake) (const char * pName, int unit);
    void   (*endGive) (void * pCookie);
    int    (*ioctl) (void* pCookie, int cmd, caddr_t data);
    /* ADD MORE HERE AS NEEDED */
    } MUX_FUNCS;

/*
 * PROTO_COOKIE is the opaque pointer type returned by mux2Bind(),
 * muxBind(), and muxTkBind().  It represents the binding of a protocol
 * to a network device.  It is used in calls to muxIoctl(), muxUnbind(),
 * muxSend(), muxPollSend(), muxPollReceive(), muxTkSend(), muxTkPollSend(),
 * muxTkPollReceive(), muxMCastAddrAdd(), muxMCastAddrDel(),
 * muxMCastAddrGet(), muxLinkHeaderCreate(), muxAddressForm(),
 * muxPacketDataGet(), and muxPacketAddrGet().
 *
 * Note that mux2Send(), mux2PollSend(), and mux2PollReceive() require
 * a DEV_COOKIE, not a PROTO_COOKIE.
 */
typedef void * PROTO_COOKIE;

/*
 * DEV_COOKIE is the opaque pointer type returned by muxDevLoad() and
 * muxDevAcquire(), and passed to muxDevRelease(), muxDevStart(), and
 * muxDevStop(), muxError(), mux2Send(), mux2PollSend(), and mux2PollReceive().
 * It represents the network device. (Pssst: it's an END_OBJ *)
 *
 * For VxWorks 6.7 and later: a DEV_COOKIE may be used a fake
 * PROTO_COOKIE, for the purposes of calling muxSend(), muxIoctl(),
 * muxPollSend(), muxPollReceive(), muxTkSend(), muxTkPollSend(),
 * muxMCastAddrAdd(), muxMCastAddrDel(), muxMCastAddrGet(),
 * muxLinkHeaderCreate(), muxAddressForm(), muxPacketDataGet(), and
 * muxPacketAddrGet().
 *
 * Don't use a DEV_COOKIE for a call to muxUnbind() or muxTkPollReceive();
 * they work only with a real PROTO_COOKIE.
 */
typedef void * DEV_COOKIE;

/*
 * Protocol information. Returned by muxProtoInfoGet().  Note that
 * the actual signatures of the callback functions depends upon the
 * binding style.
 */
typedef struct _MUX_PROTO_INFO
    {
    char name[END_PROTO_NAME_MAX];  /* String name for this protocol */
    int netSvcType;                 /* Network service type */
    int bindStyle;		    /* MUX bind style (BIND_STYLE_* above) */
    int (*stackRcvRtn) ();         /* Protocol's receive routine */
    int (*stackTxRestartRtn) (); /* Callback for restarting blocked tx. */
    int (*stackShutdownRtn) ();  /* The routine to call to shut down */
    void (*stackErrorRtn) ();       /* Callback for device errors/events */
    DEV_COOKIE pEnd;
    void * callbackArg;
    } MUX_PROTO_INFO;

typedef struct _MUX_PROTOS_INFO
    {
    int nprotos;		/* Number of PROTO_INFOs in plist */
    MUX_PROTO_INFO * plist;		/* Array of PROTO_INFO structures */
    } MUX_PROTOS_INFO;

struct Ipcom_pkt_struct; /* declaration in ipcom_pkt.h */

/* globals */

extern MUX_FUNCS _func_mux;

/* locals */

/* function declarations */

/* common routines shared by all device styles and protocol binding styles */

extern int muxCommonInit (void);
extern DEV_COOKIE muxDevAcquire (const char * name, int unit);
extern void       muxDevRelease (DEV_COOKIE pEnd);

extern END_OBJ  * endFindByName (char* pName, int unit);

extern DEV_COOKIE muxDevLoad (int unit, END_OBJ* (*endLoad) (char *, void*),
				char *initString, int loaning, void*pBSP);
extern int muxDevUnload (char* pName, int unit);

extern int muxDevStart (DEV_COOKIE pEnd);
extern int muxDevStop (DEV_COOKIE pEnd);
extern int muxDevStopAll (int timeout);

extern int  muxEndListGet (END_OBJ ** pEnds, int nSlots);
extern void muxShow (char* pDevName, int unit);

extern int muxUnbind(PROTO_COOKIE cookie, long type,
			FUNCPTR stackShutdownRtn);

extern int muxIoctl (PROTO_COOKIE cookie, int cmd, caddr_t data);

extern int muxMCastAddrAdd (PROTO_COOKIE cookie, char* pAddress);
extern int muxMCastAddrDel (PROTO_COOKIE cookie, char* pAddress);
extern int muxMCastAddrGet (PROTO_COOKIE cookie, MULTI_TABLE* pTable);

extern void muxTxRestart(void * pEnd);

extern void muxErrorSkip (DEV_COOKIE pEnd, END_ERR * pError,
			  PROTO_COOKIE skipProtoCookie);
extern void muxErrorPost(END_OBJ * pEnd, INT32 errCode,
			 char * pMesg, void * pSpare);

extern JOB_QUEUE_ID muxEndRxJobQueue (END_OBJ * pEnd);
extern int muxEndQnumGet (char * instName, int unit);

extern int    muxProtoListGet (END_OBJ * pEnd, MUX_PROTOS_INFO * pInfo);
extern int muxProtoInfoGet (PROTO_COOKIE cookie, MUX_PROTO_INFO * pInfo);

/* routines for END-style devices and muxBind() protocols */

extern int muxLibInit (void);
extern PROTO_COOKIE muxBind (char * pName, int unit,
                  int (*stackRcvRtn) (void*, long,M_BLK_ID, LL_HDR_INFO *,
                                       void*),
                  int (*stackShutdownRtn) (void*, void*),
                  int (*stackTxRestartRtn) (void*, void*),
                  void (*stackErrorRtn) (END_OBJ*, END_ERR*, void*),
                  long type, char* pProtoName, void* pSpare);

extern int muxSend (PROTO_COOKIE cookie, M_BLK_ID pMblk);
extern int muxPollSend (PROTO_COOKIE cookie, M_BLK_ID pMblk);
extern int muxPollReceive (PROTO_COOKIE cookie, M_BLK_ID pMblk);

extern M_BLK_ID muxLinkHeaderCreate(PROTO_COOKIE cookie, M_BLK_ID pPacket,
				    M_BLK_ID pSrcAddr, M_BLK_ID pDstAddr,
				    int bcastFlag);

extern M_BLK_ID muxAddressForm (PROTO_COOKIE cookie, M_BLK_ID pMblk,
				M_BLK_ID pSrcAddr, M_BLK_ID pDstAddr);
extern int muxPacketDataGet(PROTO_COOKIE cookie, M_BLK_ID pMblk,
			       LL_HDR_INFO *);
extern int muxPacketAddrGet (PROTO_COOKIE cookie, M_BLK_ID pMblk,
                                M_BLK_ID pSrc,
                                M_BLK_ID pDst,
                                M_BLK_ID pESrc,
                                M_BLK_ID pEDst);

extern int muxDevExists (char* pName, int unit);

/* These functions are deprecated */
extern int muxAddrResFuncDel (long ifType, long protocol);
extern FUNCPTR muxAddrResFuncGet (long ifType, long protocol);
extern int muxAddrResFuncAdd (long ifType, long protocol,
				 FUNCPTR addrResFunc);

extern int muxIfFuncBaseInit (void);
extern int muxIfFuncDel (long ifType, long protocol, int funcType);
extern FUNCPTR muxIfFuncGet (long ifType, long protocol, int funcType);
extern int muxIfFuncAdd (long ifType, long protocol, int funcType, 
			    FUNCPTR ifFunc);
extern END_OBJ* endFirstUnitFind (char* pDevName);

/* Routines for mux2Bind() protocols and END2-style devices */

void mux2LibInit (void);
void * mux2Bind (char * name, int unit,
	 int   (*stackRcvRtn) (void * callbackArg,
				struct Ipcom_pkt_struct * pkt),
	 int (*stackShutdownRtn) (PROTO_COOKIE cookie, void * callbackArg),
	 int (*stackTxRestartRtn) (void * callbackArg),
	 void   (*stackErrorRtn) (void * callbackArg, END_ERR * err),
	 unsigned short type, char * pProtoName, void * callbackArg);

extern int mux2Send (END_OBJ * pEnd, struct Ipcom_pkt_struct * pkt);
extern int mux2PollSend (END_OBJ * pEnd, struct Ipcom_pkt_struct * pkt);
extern int mux2PollReceive (END_OBJ * pEnd, struct Ipcom_pkt_struct * pkt);

/* Routines called (mostly) by network device drivers */

extern void mux2Receive (END_OBJ * pEnd, struct Ipcom_pkt_struct * pkt);
extern int muxReceive (void * pEnd, M_BLK_ID pMblk);

extern void muxError (DEV_COOKIE pEnd, END_ERR * pError);
extern void muxLinkUpNotify (END_OBJ * pEnd);
extern void muxLinkDownNotify (END_OBJ * pEnd);

/* utility routines (deprecate these?) */

extern int muxIterate (FUNCPTR pCallbackRtn, void * pCallbackArg);
extern int muxIterateByName (char * pName, FUNCPTR pCallbackRtn,
				void * pCallbackArg);

/* cross protocol / device */

extern void muxOverEnd2Init (void);
extern void mux2OverEndInit (void);

#ifdef __cplusplus
}
#endif

#endif /* _REWORKS_MUX_LIB_H_ */
