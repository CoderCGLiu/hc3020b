/*! @file symtbl.h
    @brief ReWorks符号表头文件
    
 * 本文件定义了ReWorks系统中符号表操作相关的接口。
 *
 * @version 4.7
 * 
 * @see 无
 */
/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了符号表接口
 * 修改：
 * 		 2012-06-27，唐立三，整理 
 */
#ifndef _REWORKS_SYSTEM_SYMBOL_API_H_
#define _REWORKS_SYSTEM_SYMBOL_API_H_

#ifdef __cplusplus
extern "C" 
{
#endif

#include <stdio.h>
#include <sysSymbol.h> 

/**
 * @defgroup group_os_io_symbol_api 符号表接口
 * @ingroup group_os_develop
 * 
 * @{
 * 
 */	

/*!
 * \typedef MODULE_ID
 * 
 * \brief 模块ID类型
 */	
typedef void* MODULE_ID;

/*!
 * \typedef SYMTAB_ID
 * 
 * \brief 符号表ID类型
 */
typedef void * SYMTAB_ID;

/*!
 * \typedef void (*LOADER_HOOK)(const char *)
 * 
 * \brief 加载钩子函数指针
 */	
typedef void (*LOADER_HOOK)(const char *);

/*!
 * \var ld_rede_hook
 * 
 * \brief 动态加载ReDe钩子
 */
extern LOADER_HOOK ld_rede_hook;

/*!
 * \var unld_rede_hook
 * 
 * \brief 动态卸载ReDe钩子
 */
extern LOADER_HOOK unld_rede_hook;

/*!
 * \typedef const char *(*FUNCNAME_RESOLVE_HANDLER)(const void *addr, unsigned int *offset)
 * 
 * \brief 符号解析接口类型 
 * 
 */
typedef const char *(*FUNCNAME_RESOLVE_HANDLER)(const void *addr, unsigned int *offset);


/*!
 * \var funcname_resolve_handler
 * 
 * \brief 函数名称解析钩子
 */
extern FUNCNAME_RESOLVE_HANDLER funcname_resolve_handler;

/*!
 * \var sysSymTbl
 * 
 * \brief 全局符号表链表指针
 */
extern SYMTAB_ID sysSymTbl;

/*!
 * \typedef  MODULE_INFO
 * 
 * \brief 模块信息类型
 */
typedef struct
{
	char		name [1023];/* module name */
	void		*codeSeg;	/* actual code memory */
	void		*dataSeg;	/* actual data memory */
} MODULE_INFO; 

/*!
 * \fn int symbol_lkup(const char *name, void **value, SymType *type)
 * 
 * \brief 根据符号名称查询符号的内存地址值和符号的类型
 * 
 * \param name 要查询的符号名称。（输入参数）
 * \param value 查询到符号的内存地址值。若为函数，则该值的含义为函数起始地址。（输出参数）
 * \param type 查询到符号的类型，用于判断是否为函数或变量等。（输出参数）
 * 
 * \return 成功返回0
 * \return 失败返回-1
 */
int symbol_lkup(const char *name, void **value, SymType *type);


/*!
 * \fn MODULE_ID ld(char *file_name, char *module_name)
 * 
 * \brief 动态加载模块
 * 
 * \param file_name 加载模块的文件名
 * \param module_name 模块加载到内存中后模块名
 * 
 * \return 成功返回加载到内存模块的句柄
 * \return 失败返回空指针
 */
MODULE_ID ld(char *file_name, char *module_name);


/*!
 * \fn int unld(char *file_name)
 * 
 * \brief 以文件名卸载已动态加载模块
 * 
 * \param file_name 已加载模块的文件名
 * 
 * \return 成功返回0 
 * \return 失败返回-1
 */
int unld(char *file_name);


/*!
 * \fn int unld_by_module_name(char *module_name)
 * 
 * \brief 以模块名卸载已动态加载模块
 * 
 * \param module_name 已加载模块的模块名
 * 
 * \return 成功返回0 
 * \return 失败返回-1
 */
int unld_by_module_name(char *module_name);


/*!
 * \fn int unld_by_module_id(void *module_id)
 * 
 * \brief 以模块ID卸载已动态加载模块
 * 
 * \param module_id 已加载模块的句柄（如ld成功加载后的返回值）
 * 
 * \return 成功返回0 
 * \return 失败返回-1
 */
int unld_by_module_id(void *module_id);


/*!
 * \fn MODULE_ID reld(char *file_name, char *module_name)
 * 
 * \brief 重新加载已动态加载模块
 * 
 * \param file_name 加载模块的文件名
 * \param module_name 模块加载到内存中后模块名
 * 
 * \return 成功返回加载到内存模块的句柄
 * \return 失败返回空指针
 */
MODULE_ID reld(char *file_name, char *module_name);


/*!
 * \fn void module_info(void)
 * 
 * \brief 显示已动态加载模块信息
 * 
 * \param 无
 * 
 * \return 无
 */
void module_info(void);


/*!
 * \fn int symtbl_module_init(void)
 * 
 * \brief 符号表模块初始化接口
 * 
 * \param 无
 * 
 * \return 成功返回0 
 * \return 失败返回-1
 */
int symtbl_module_init(void);

typedef short	ModuleID;	/* Id < 0 means INVALID */

typedef struct app_module_info
{
	MODULE_ID 			module_id;			/* unique ID */
	unsigned long		code_start;	/* actual code memory */
	unsigned long		code_size;	/* size of the loaded code stuff */
	unsigned long		data_start;	/* actual data memory */
	unsigned long		data_size;	/* size of the loaded data stuff */
} module_info_t;

/* 获取系统模块之外的其他模块信息 */
int module_get_info(module_info_t *mod_info, unsigned int buf_size);

/* 获取系统模块之外的其他模块数目*/
int module_compute_count();

/*
 * 获取除系统模块外的所有模块信息
 * 参数：
 * 	mod_info - 存储模块信息的buffer地址 
 * 	buf_size - 存储模块信息的buffer大小
 * 返回值：
 * 	成功返回实际获取模块的数目；失败返回-1
 */
int coredump_get_moduleinfo(module_info_t *mod_info, unsigned int buf_size);

/*****************************************************************
 * reworks_module_load是Rede的目标机代理程序进行动态加载的接口
 * 输入:
 * 	char	*file_name		为动态加载模块的文件名
 * 	char	*module_name	模块加载到内存后的模块名，如果为NULL则处理为去路径后的文件名
 *	char	**err_msg		此为动态加载过程中产生的错误信息，在加载成功时为 NULL
 * 	size_t	*length			此为err_msg错误信息的长度，在加载成功时为NULL为 0
 * 输出：
 * 	如果加载成功，返回模块的句柄
 *  如果加载失败，返回0 以及错误信息err_msg，err_msg的长度length
 * 	如果错误信息生成时出错, 若是内存分配问题，则错误信息和长度置空。其他相关信息
 * 则在err_msg中
 *
 * 注意：
 * 	*err_msg所指向的内存块由动态分配而得，释放方式请看此头文件最后的具体说明
 */
/*!
 * \fn MODULE_ID reworks_module_load(char *file_name, char *module_name,
	char** err_msg, size_t* length)
 * 
 * \brief Rede的目标机代理程序进行动态加载的接口
 * 
 * \param file_name		为动态加载模块的文件名
 * \param module_name	模块加载到内存后的模块名，如果为NULL则处理为去路径后的文件名
 * \param err_msg		此为动态加载过程中产生的错误信息，在加载成功时为 NULL
 * \param length	          此为err_msg错误信息的长度，在加载成功时为NULL为 0
 * 
 * \return 成功返回模块的句柄 
 * \return 失败返回0 以及错误信息err_msg，err_msg的长度length，（如果错误信息生成时出错, 
 *         若是内存分配问题，则错误信息和长度置空。其他相关信息则在err_msg中）
 *         
 */
MODULE_ID reworks_module_load(char *file_name, char *module_name,
	char** err_msg, size_t* length);

/*****************************************************************
 * reworks_module_unload是Rede的目标机代理程序对已动态加载模块进行卸载的接口
 * 输入:
 * 	char	*file_name		为动态加载模块的文件名
 * 	char	**err_msg		此为动态加载过程中产生的错误信息，在加载成功时为 NULL
 * 	size_t	*length			此为err_msg错误信息的长度，在加载成功时为NULL为 0
 * 输出：
 * 	如果加载成功，返回0
 *  如果加载失败，返回-1 以及错误信息err_msg，err_msg的长度length
 * 	如果错误信息生成时出错, 若是内存分配问题，则错误信息和长度置空。其他相关信息
 * 则在err_msg中
 *
 * 注意：
 * 	*err_msg所指向的内存块由动态分配而得，释放方式请看此头文件最后的具体说明
 */
/*!
 * \fn int reworks_module_unload(char *file_name, char** err_msg, size_t* length)
 * 
 * \brief Rede的目标机代理程序对已动态加载模块进行卸载的接口
 * 
 * \param file_name		为动态加载模块的文件名
 * \param err_msg		此为动态加载过程中产生的错误信息，在加载成功时为 NULL
 * \param length	          此为err_msg错误信息的长度，在加载成功时为NULL为 0
 * 
 * \return 成功返回0 
 * \return 失败返回-1 以及错误信息err_msg，err_msg的长度length，（如果错误信息生成时出错, 
 *         若是内存分配问题，则错误信息和长度置空。其他相关信息则在err_msg中）
 *         
 */
int reworks_module_unload(char *file_name, char** err_msg, size_t* length);

/*
 * 如何释放err_msg与length？在目前的实现方式下，从可能出现的四种情况来看：
 * (1)err_msg==NULL length==0 情景：realloc发生错误，内存不足
 * (2)err_msg==NULL length!=0 情景：不可能，
 *     因为length!=0说明已经有错误读入，但读入必为err_msg分配内存空间的步骤。
 * (3)err_msg!=NULL length==0 情景：错误输出文件中没有错误信息
 * (4)err_msg!=NULL length!=0 情景：有出错信息的时候
 * 
 * 建议：
 * 1.在length>0的情况下才读errMsg中的内容，err_msg所指向内存块的长度为length，其中数据
 * 依...\n...\n组织，请以'\n'为分隔符，length为边界值读取。
 * 2.一定要有释放err_msg的步骤，但是在情景(1)下有可能err_msg是NULL，所以释放前请判断
 * err_msg是否为NULL。
 */

/** @example example_symbol_lkup.c
 * 下面的例子演示了如何使用 symbol_lkup().
 */

/* @} */
#ifdef __cplusplus
}
#endif 

#endif /*SYMTBL_H_*/
