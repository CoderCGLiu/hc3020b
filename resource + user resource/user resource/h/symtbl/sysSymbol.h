/*! @file sysSymbol.h
    @brief ReWorks符号类型头文件
    
 * 本文件定义了ReWorks系统中的符号类型信息。
 *
 * @version 4.7
 * 
 * @see 无
 */
/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：本文件定义了符号类型信息
 * 修改：
 * 		 2012-06-27，唐立三，整理 
 */
#ifndef _REWORKS_SYSTEM_SYMBOL_TYPE_H_
#define _REWORKS_SYSTEM_SYMBOL_TYPE_H_


#ifdef __cplusplus
extern "C" 
{
#endif
	
/**
 * @defgroup group_os_io_symbol_type 符号类型
 * @ingroup group_os_develop
 * 
 * @{
 * 
 */	

/*!
 * \enum SymType
 * 
 * \brief 符号类型
 */
typedef enum 
{
    TVoid		= 0,                                             //!< void类型
    TVoidP      = 0  | (sizeof(unsigned char) << 8) | (1 << 0),  //!< void指针类型
    TUChar      = 2  | (sizeof(unsigned char) << 8),             //!< 无符号char类型
    TUCharP     = 2  | (sizeof(unsigned char) << 8) | (1 << 0),  //!< 无符号char指针类型
    TUShort     = 4  | (sizeof(unsigned short) << 8),            //!< 无符号short类型
    TUShortP    = 4  | (sizeof(unsigned short) << 8) | (1 << 0), //!< 无符号short指针类型
    TULong      = 6  | (sizeof(unsigned long) << 8),             //!< 无符号long类型
    TULongP     = 6  | (sizeof(unsigned long) << 8) | (1 << 0),  //!< 无符号long指针类型
    TFloat      = 8  | (sizeof(float) << 8),                     //!< float类型
    TFloatP     = 8  | (sizeof(float) << 8) | (1 << 0),          //!< float指针类型
    TDouble     = 10 | (sizeof(double) << 8),                    //!< double类型
    TDoubleP    = 10 | (sizeof(double)<< 8) | (1 << 0),          //!< double指针类型
    TFuncP      = TULong  | (1 << 6) | (1 << 0),                 //!< long函数指针类型
    TDFuncP     = TDouble | (1 << 6) | (1 << 0)                  //!< double函数指针类型
}SymType;

/*!
 * \union SymValU_
 * 
 * \brief 符号值类型
 */
typedef union  SymValU_ 
{
		void				*p;	//!< 指针类型
		unsigned char		c; //!< 无符号字符类型
		unsigned short		s; //!< 无符号短整型类型
		unsigned long		l; //!< 无符号长整型类型
		float				f; //!< 浮点类型
		double				d; //!< 双精度浮点类型
}SymValU /*! 符号值类型数据结构 */, *SymVal; //!< 符号值类型数据结构

/*!
 * \struct SymTypedAddrRec_
 * 
 * \brief 符号地址记录
 */
typedef struct SymTypedAddrRec_ 
{
	SymVal		ptv; //!< 符号值
	SymType		type; //!< 符号类型
} SymTypedAddrRec; //!< 符号地址记录

/*!
 * \struct symRec_
 * 
 * \brief 符号记录
 */
typedef struct symRec_ 
{
	const char			*name; //!< 符号名称
	SymTypedAddrRec		value; //!< 符号地址及类型
	int					size; //!< 符号大小
	unsigned			flags; //!< 符号
	char				*help; //!< 符号说明
} symRec; //!< 符号记录 

typedef struct symRec_	 *Symbol; //!< 符号记录 数据结构对应的指针类型

/*!
 * \def SYMFLG_GLBL
 * 
 * \brief 全局符号
 */
#define SYMFLG_GLBL		(1 << 0) 

/*!
 * \def SYMFLG_WEAK
 * 
 * \brief 弱符号
 */
#define SYMFLG_WEAK		(1 << 1) 

/*!
 * \def SYMFLG_MALLOC_HELP
 * 
 * \brief 帮助信息为静态的还是分配的
 */
#define SYMFLG_MALLOC_HELP	( 1<< 3)

/*!
 * \def SYMFLG_SECT
 * 
 * \brief 段符号
 */
#define SYMFLG_SECT		(1 << 4) 

/* @} */

#ifdef __cplusplus
}
#endif 

#endif 
