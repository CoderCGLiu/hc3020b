/*! @file pcibus.h
    @brief ReWorks PCI头文件
    
 * 本文件定义了ReWorks系统中PCI相关宏定义以及接口。
 *
 * @version 4.7
 * 
 * @see 无
 */
/*******************************************************************************
 * 
 * 版权：中国电子科技集团公司第三十二研究所
 * 描述：PCI头文件定义
 * 修改：
 * 		 2011-10-10，邵金剑、吴锡臻整理 
 *       2013-09-04，姚秋果、李娟整理
 */
#ifndef _REWORKS_PCIBUS_H_
#define _REWORKS_PCIBUS_H_
 
#ifdef __cplusplus
extern "C"
{
#endif

/**  
 * @ingroup group_os_pci PCI模块
 *		
 * @{ 
 */	
	
/**************************** PCI配置空间支持库 **************************/
/* PCI空间访问机制 */
#define PCI_MECHANISM_0     0   //!< 用户自定义接口 
#define	PCI_MECHANISM_1	    1   //!< 当前PC-AT硬件机制 
#define	PCI_MECHANISM_2	    2   //!< 非首选 



#ifndef PCI_MAX_BUS
#   define PCI_MAX_BUS	255   //!< PCI最大总线数 
#endif  


#ifndef PCI_MAX_DEV
#   define PCI_MAX_DEV	32  //!< PCI最大设备数 
#endif  


#ifndef PCI_MAX_FUNC
#   define PCI_MAX_FUNC	8  //!< PCI最大功能数 
#endif  

/* 
 * 标准设备配置寄存器偏移定义
 * 注意只有模4地址才能被写入地址寄存器中
 */
#define PCI_VENDOR_ID_REG           0x00  //!< 标准设备配置寄存器偏移定义
#define PCI_DEVICE_ID_REG           0x02  //!< 标准设备配置寄存器偏移定义
#define PCI_COMMAND_REG             0x04  //!< 标准设备配置寄存器偏移定义
#define PCI_STATUS_REG              0x06  //!< 标准设备配置寄存器偏移定义
#define PCI_REVISION_REG            0x08  //!< 标准设备配置寄存器偏移定义
#define PCI_PROGRAMMING_IF_REG      0x09  //!< 标准设备配置寄存器偏移定义
#define PCI_SUBCLASS_REG            0x0a  //!< 标准设备配置寄存器偏移定义
#define PCI_CLASS_REG               0x0b  //!< 标准设备配置寄存器偏移定义
#define PCI_CACHE_LINE_SIZE_REG     0x0c  //!< 标准设备配置寄存器偏移定义
#define PCI_LATENCY_TIMER_REG       0x0d  //!< 标准设备配置寄存器偏移定义
#define PCI_HEADER_TYPE_REG         0x0e  //!< 标准设备配置寄存器偏移定义
#define PCI_BIST_REG                0x0f  //!< 标准设备配置寄存器偏移定义
#define PCI_BASE_ADDRESS_0      	0x10  //!< 标准设备配置寄存器偏移定义
#define PCI_BASE_ADDRESS_1      	0x14  //!< 标准设备配置寄存器偏移定义
#define PCI_BASE_ADDRESS_2      	0x18  //!< 标准设备配置寄存器偏移定义
#define PCI_BASE_ADDRESS_3     		0x1c  //!< 标准设备配置寄存器偏移定义
#define PCI_BASE_ADDRESS_4      	0x20  //!< 标准设备配置寄存器偏移定义
#define PCI_BASE_ADDRESS_5      	0x24  //!< 标准设备配置寄存器偏移定义
#define PCI_CIS_REG                 0x28  //!< 标准设备配置寄存器偏移定义
#define PCI_SUB_VENDER_ID_REG       0x2c  //!< 标准设备配置寄存器偏移定义之PCI子系统开发商ID
#define PCI_SUB_SYSTEM_ID_REG       0x2e  //!< 标准设备配置寄存器偏移定义之PCI子系统系统ID
#define PCI_EXPANSION_ROM_REG       0x30  //!< 标准设备配置寄存器偏移定义
#define PCI_CAP_PTR_REG             0x34  //!< 标准设备配置寄存器偏移定义
#define PCI_RESERVED_0          	0x35  //!< 标准设备配置寄存器偏移定义
#define PCI_RESERVED_1          	0x38  //!< 标准设备配置寄存器偏移定义
#define PCI_DEV_INT_LINE_REG        0x3c  //!< 标准设备配置寄存器偏移定义
#define PCI_DEV_INT_PIN_REG         0x3d  //!< 标准设备配置寄存器偏移定义
#define PCI_MIN_GRANT_REG           0x3e  //!< 标准设备配置寄存器偏移定义
#define PCI_MAX_LATENCY_REG         0x3f  //!< 标准设备配置寄存器偏移定义
#define PCI_SPECIAL_USE        		0x41  //!< 标准设备配置寄存器偏移定义
#define PCI_CFG_MODE_REG            0x43  //!< 标准设备配置寄存器偏移定义

/* 
 * PCI-PCI 桥配置寄存器偏移定义 
 * 注意只有模4地址才能被写入地址寄存器中
 */
#define PCI_PRIMARY_BUS         0x18  //!< PCI-PCI 桥配置寄存器偏移定义
#define PCI_SECONDARY_BUS       0x19  //!< PCI-PCI 桥配置寄存器偏移定义
#define PCI_SUBORDINATE_BUS     0x1a  //!< PCI-PCI 桥配置寄存器偏移定义
#define PCI_SEC_LATENCY         0x1b  //!< PCI-PCI 桥配置寄存器偏移定义
#define PCI_IO_BASE             0x1c  //!< PCI-PCI 桥配置寄存器偏移定义
#define PCI_IO_LIMIT            0x1d  //!< PCI-PCI 桥配置寄存器偏移定义
#define PCI_SEC_STATUS          0x1e  //!< PCI-PCI 桥配置寄存器偏移定义
#define PCI_MEM_BASE            0x20  //!< PCI-PCI 桥配置寄存器偏移定义
#define PCI_MEM_LIMIT           0x22  //!< PCI-PCI 桥配置寄存器偏移定义
#define PCI_PRE_MEM_BASE        0x24  //!< PCI-PCI 桥配置寄存器偏移定义
#define PCI_PRE_MEM_LIMIT       0x26  //!< PCI-PCI 桥配置寄存器偏移定义
#define PCI_PRE_MEM_BASE_U      0x28  //!< PCI-PCI 桥配置寄存器偏移定义
#define PCI_PRE_MEM_LIMIT_U     0x2c  //!< PCI-PCI 桥配置寄存器偏移定义
#define PCI_IO_BASE_U           0x30  //!< PCI-PCI 桥配置寄存器偏移定义
#define PCI_IO_LIMIT_U          0x32  //!< PCI-PCI 桥配置寄存器偏移定义
#define PCI_ROM_BASE            0x38  //!< PCI-PCI 桥配置寄存器偏移定义
#define PCI_BRG_INT_LINE        0x3c  //!< PCI-PCI 桥配置寄存器偏移定义
#define PCI_BRG_INT_PIN         0x3d  //!< PCI-PCI 桥配置寄存器偏移定义
#define PCI_BRIDGE_CONTROL      0x3e  //!< PCI-PCI 桥配置寄存器偏移定义
#define PCI_EXT_CAP_MSI         0x05                                        

/* PCI命令位 */
#define PCI_CMD_IO		0x0001	//!< 使能I/O接收 
#define PCI_CMD_MEM		0x0002	//!< 使能内存接收 
#define PCI_CMD_MASTER	0x0004	//!< 使能总线控制 
#define PCI_CMD_MON		0x0008	//!< 使能监控指定周期 
#define PCI_CMD_WI		0x0010	//!< 使能写和无效操作 
#define PCI_CMD_SNOOP	0x0020	//!< 使能探听器 
#define PCI_CMD_PERR	0x0040	//!< 使能奇偶校验 
#define PCI_CMD_WC		0x0080	//!< 使能周期等待 
#define PCI_CMD_SERR	0x0100	//!< 使能系统错误报告 
#define PCI_CMD_FBTB	0x0200	//!< 使能快速总线切换 
#define PCI_CMD_INTX    0x0400  /* prevent INTx interrupts */

/* PCI状态位 */                           
#define  PCI_STATUS_INTERRUPT			0x08	//!< PCI状态位
#define  PCI_STATUS_CAP_LIST			0x10	//!< PCI状态位
#define  PCI_STATUS_66MHZ				0x20	//!< PCI状态位
#define  PCI_STATUS_UDFN				0x40	//!< PCI状态位
#define  PCI_STATUS_FAST_BACK			0x80	//!< PCI状态位
#define  PCI_STATUS_PARITY				0x100	//!< PCI状态位
#define  PCI_STATUS_DEVSEL_MASK			0x600	//!< PCI状态位
#define  PCI_STATUS_DEVSEL_FAST			0x000	//!< PCI状态位
#define  PCI_STATUS_DEVSEL_MEDIUM		0x200	//!< PCI状态位
#define  PCI_STATUS_DEVSEL_SLOW			0x400	//!< PCI状态位
#define  PCI_STATUS_SIG_TARGET_ABORT	0x800	//!< PCI状态位
#define  PCI_STATUS_REC_TARGET_ABORT	0x1000	//!< PCI状态位
#define  PCI_STATUS_REC_MASTER_ABORT	0x2000	//!< PCI状态位
#define  PCI_STATUS_SIG_SYSTEM_ERROR	0x4000	//!< PCI状态位
#define  PCI_STATUS_DETECTED_PARITY		0x8000	//!< PCI状态位

/* PCI子系统信息 */
#define	PCI_SUB_VENDER_ID	0x2c
#define	PCI_SUB_SYSTEM_ID	0x2e

/* PCI缓存大小 */
#ifndef PCI_CACHE_LINE_SIZE
#   if defined(_CACHE_ALIGN_SIZE)
#      define PCI_CACHE_LINE_SIZE	(_CACHE_ALIGN_SIZE/4) //!< PCI缓存大小
#   else
#      define PCI_CACHE_LINE_SIZE	(32/4) //!< PCI缓存大小
#   endif
#endif 

/* PCI等待定时器 */
#ifndef PCI_LATENCY_TIMER
#define PCI_LATENCY_TIMER      	0xff   //!< PCI等待定时器
#endif 


/* Header type 2 (CardBus bridges) */
#define PCI_CB_CAPABILITY_LIST	0x14

#define PCI_FIND_CAP_TTL 0x48
#define CAP_START_POS 0x40

#define PCI_BAR_SPACE_MASK  	(0x01)
#define PCI_BAR_MEM_TYPE_MASK   (0x06)
#define PCI_BAR_MEM_PREF_MASK   (0x08)
#define PCI_BAR_ALL_MASK        (PCI_BAR_SPACE_MASK | \
                                PCI_BAR_MEM_TYPE_MASK | \
                                PCI_BAR_MEM_PREF_MASK)
#define PCI_BAR_SPACE_IO    	(0x01)

/* PCI基地址掩码 */
#define PCI_MEM_BASE_MASK    	~0xf    //!< 内存基地址掩码 
#define PCI_IO_BASE_MASK     	~0x3    //!< IO基地址掩码 
#define PCI_BASE_IO    			 0x1 	//!< IO空间指示器 
#define PCI_BASE_BELOW_1M   	0x2 	//!< 小于1MB的内存地址 
#define PCI_BASE_IN_64BITS  	0x4 	//!< 64位的内存地址 
#define PCI_BASE_PREFETCH   	0x8 	//!< 提前分配的空间 
                                       
/* 基地址寄存器的内存/IO属性位 */                  
#define PCI_BAR_SPACE_MASK  	(0x01) //!< 基地址寄存器空间掩码
#define PCI_BAR_SPACE_IO    	(0x01) //!< IO空间指示器
#define PCI_BAR_SPACE_MEM   	(0x00) //!< 内存空间指示器
#define PCI_BAR_MEM_TYPE_MASK   (0x06) //!< 内存空间类型掩码
#define PCI_BAR_MEM_TYPE_ADDR32      (0x00) //!< 64位的内存地址
#define PCI_BAR_MEM_TYPE_BELOW_1MB   (0x02) //!< 小于1MB的内存地址
#define PCI_BAR_MEM_TYPE_ADDR64      (0x04) //!< 64位的内存地址
#define PCI_BAR_MEM_TYPE_RESERVED    (0x06) //!< 保留
#define PCI_BAR_MEM_PREF_MASK   (0x08) //!< 内存预取掩码
#define PCI_BAR_MEM_PREFETCH    (0x08) //!< 内存预取
#define PCI_BAR_MEM_NON_PREF    (0x00) //!< 无内存预取
#define PCI_BAR_ALL_MASK        (PCI_BAR_SPACE_MASK | \
                                 PCI_BAR_MEM_TYPE_MASK | \
                                 PCI_BAR_MEM_PREF_MASK) //!< 基地址寄存器掩码

#define PCI_CFG_SEC_BUS_RESET   0x40  //!< 次总线重置位

/* 拓展性能 */
#define  PCI_EXT_CAP_ID_PM		0x01	//!< 拓展性能
#define  PCI_EXT_CAP_ID_AGP		0x02	//!< 拓展性能
#define  PCI_EXT_CAP_ID_VPD		0x03	//!< 拓展性能
#define  PCI_EXT_CAP_ID_SLOTID	0x04	//!< 拓展性能
#define  PCI_EXT_CAP_ID_MSI		0x05	//!< 拓展性能
#define  PCI_EXT_CAP_ID_CHSWP	0x06	//!< 拓展性能
#define  PCI_EXT_CAP_ID_PCIX	0x07	//!< 拓展性能
#define  PCI_EXT_CAP_ID_HT		0x08	//!< 拓展性能
#define  PCI_EXT_CAP_ID_VNDR	0x09	//!< 拓展性能
#define  PCI_EXT_CAP_ID_DBG		0x0A	//!< 拓展性能
#define  PCI_EXT_CAP_ID_CCRC	0x0B	//!< 拓展性能
#define  PCI_EXT_CAP_ID_SHPC	0x0C	//!< 拓展性能
#define  PCI_EXT_CAP_ID_SSVID	0x0D	//!< 拓展性能
#define  PCI_EXT_CAP_ID_AGP3	0x0E	//!< 拓展性能
#define  PCI_EXT_CAP_ID_SECDEV	0x0F	//!< 拓展性能
#define  PCI_EXT_CAP_ID_EXP		0x10	//!< 拓展性能
#define  PCI_EXT_CAP_ID_MSIX	0x11	//!< 拓展性能

/* 简单PCI热插拔控制和状态寄存器 (HSCSR)定义 */
#define  PCI_CHSWP_DHA		0x01	
#define  PCI_CHSWP_EIM		0x02	//!< ENUM中断掩码 
#define  PCI_CHSWP_PIE		0x04	
#define  PCI_CHSWP_LOO		0x08	//!< Blue LED开关 
#define  PCI_CHSWP_PI		0x30	
#define  PCI_CHSWP_EXT		0x40	//!< ENUM EXTract状态 
#define  PCI_CHSWP_INS		0x80	//!< ENUMINSert状态 

/* PCI头部类型位 */
#define PCI_HEADER_TYPE_MASK    0x7f  //!< PCI头部类型掩码 
#define PCI_HEADER_PCI_PCI  	0x01  //!< PCI头部类型位之PCI-PCI桥标识 
#define PCI_HEADER_TYPE0        0x00  //!< PCI头部类型位之常规设备
#define PCI_HEADER_MULTI_FUNC   0x80  //!< PCI头部类型位之多功能设备 
#define PCI_HEADER_TYPE_CARDBUS 0x02
/* PCI设备模式 */
#define PCI_SNOOZE_MODE             0x40  //!< PCI设备模式之睡眠模式 
#define PCI_SLEEP_MODE_DIS          0x00  //!< PCI设备模式之非能睡眠模式 
                                        

/*!
 * \fn int pci_config_init(int mechanism, unsigned long addr0, 
 *                      unsigned long addr1, unsigned long addr2)
 * \brief 初始化PCI配置空间库
 * 
 * 初始化PCI配置的接入方式和地址
 * 
 * \param mechanism PCI配置支持库初始化机制(mechanism 0,1,2)
 * \param addr0 user_read_routine/CAR/CSE
 * \param addr1 user_write_routine/CDR/CFR
 * \param addr2 user_special_routine/NULL/base_address
 * 
 * \return 0 PCI配置初始化成功
 * \return -1 PCI配置初始化失败
 */
int pci_config_init(int mechanism, unsigned long addr0, 
                       unsigned long addr1, unsigned long addr2);

/*!
 * \fn int pci_find_device(int vendor, int device, int index, int *dev_bus,
 *                   int *dev_dev, int *dev_func)
 *                   
 * \brief 设备查找
 * 
 * 根据供应商ID和设备ID查找设备 
 * 
 * \param vendor 设备制造商ID
 * \param device 设备ID
 * \param index 实例号
 * \param dev_bus 查找到的BUS号
 * \param dev_dev 查找到的设备号
 * \param dev_func 查找到的设备功能号
 * 
 * \return 0 查找成功
 * \return -1 查找失败
 */
int pci_find_device(int vendor, int device, int index, int *dev_bus,
                    int *dev_dev, int *dev_func);


/*!
 * \fn int pci_find_class(int class_code, int index,int *dev_bus, 
 *                  int *dev_dev, int *dev_func)
 *                  
 * \brief 设备查找
 * 
 * 根据设备的类别查找设备 
 * 
 * \param class_code 24位设备类别号
 * \param index 实例号
 * \param dev_bus 查找到的BUS号
 * \param dev_dev 查找到的设备号
 * \param dev_func 查找到的设备功能号
 * 
 * \return 0 查找成功
 * \return -1 查找失败
 */
int pci_find_class(int class_code, int index,int *dev_bus, 
                   int *dev_dev, int *dev_func);


/*!
 * \fn int pci_dev_config(int bus, int dev, int func, unsigned int IO_base_addr,        
 *                  unsigned int mem_base_addr, unsigned int command)
 *                  
 * \brief 设备配置
 * 
 * 根据指定的设备参数配置设备 ,只限于配置空间头部类型为0的PCI设备
 * 
 * \param bus 设备BUS号
 * \param dev 设备号
 * \param func 设备功能号
 * \param IO_base_addr 设备IO入口地址
 * \param mem_base_addr 设备内存入口号
 * \param command 设备控制指令
 *
 * \return 0 设备配置成功
 * \return -1 设备配置失败
 */
int pci_dev_config(int bus, int dev, int func, unsigned int IO_base_addr,        
                   unsigned int mem_base_addr, unsigned int command);

/*!
 * \fn int pci_config_bdf_pack(int bus, int dev, int func)
 * 
 * \brief 把总线、设备、功能号组成整数型
 * 
 * 把给定的总线、设备、功能号整合成一个整型数返回
 * 
 * \param bus 设备所属BUS号
 * \param dev 设备号
 * \param func 设备功能号
 *
 * \return bdf_pack bdf组合
 */
int pci_config_bdf_pack(int bus, int dev, int func);

/*!
 * \fn int pci_config_extcap_find(unsigned char extcap, int bus, int dev,           
 *                          int func, unsigned char *offset)
 *                          
 * \brief 查找拓展功能
 * 
 * 将指定拓展性能的寄存器地址提供给用户
 * 
 * \param extcap 要查找的拓展性能ID
 * \param bus 设备BUS号
 * \param dev 设备号
 * \param func 设备功能号
 * \param offset 存储指定性能的偏移量
 * 
 * \return 0 成功查找到指定性能
 * \return -1 未找到指定性能
 */
int pci_config_extcap_find(unsigned char extcap, int bus, int dev,           
                           int func, unsigned char *offset);

/*!
 * \fn int pci_read_config_byte(int bus, int dev, int func,     
 *                        int offset, unsigned char *data)
 *                        
 * \brief 从配置空间读取数据
 * 
 * 从配置空间的指定寄存器中读取一字节数据
 * 
 * \param bus 设备BUS号
 * \param dev 设备号
 * \param func 设备功能号
 * \param offset 配置空间偏移量
 * \param data 存储读取的数据（一字节）
 *
 * \return 0 读取成功
 * \return -1 读取失败
 */
int pci_read_config_byte(int bus, int dev, int func,     
                         int offset, unsigned char *data);

/*!
 * \fn int pci_read_config_word(int bus, int dev, int func,     
 *                        int offset, unsigned short *data)
 *                        
 * \brief 从配置空间读取数据
 * 
 * 从配置空间的指定寄存器中读取一个字数据
 * 
 * \param bus 设备BUS号
 * \param dev 设备号
 * \param func 设备功能号
 * \param offset 配置空间偏移量
 * \param data 存储读取的数据（一个字）
 *
 * \return 0 读取成功
 * \return -1 读取失败
 */
int pci_read_config_word(int bus, int dev, int func,     
                         int offset, unsigned short *data);


/*!
 * \fn int pci_read_config_dword(int bus, int dev, int func,    
 *                         int offset, unsigned int *data)
 *                         
 * \brief 从配置空间读取数据
 * 
 * 从配置空间的指定寄存器中读取一个长字数据
 * 
 * \param bus 设备BUS号
 * \param dev 设备号
 * \param func 设备功能号
 * \param offset 配置空间偏移量
 * \param data 存储读取的数据（一个长字）
 *
 * \return 0 读取成功
 * \return -1 读取失败
 */
int pci_read_config_dword(int bus, int dev, int func,    
                          int offset, unsigned int *data);

/*!
 * \fn int pci_write_config_byte(int bus, int dev, int func,   
 *                         int offset, unsigned char data)
 *                         
 * \brief 向配置空间写数据
 * 
 * 向配置空间的指定寄存器中写入一个字节数据
 * 
 * \param bus 设备BUS号
 * \param dev 设备号
 * \param func 设备功能号
 * \param offset 配置空间偏移量
 * \param data 欲写入的数据（一字节）
 *
 * \return 0 写入成功
 * \return -1 写入失败
 */
int pci_write_config_byte(int bus, int dev, int func,   
                          int offset, unsigned char data);

/*!
 * \fn int pci_write_config_word(int bus, int dev, int func,  
 *                         int offset, unsigned short data)
 *                         
 * \brief 向配置空间写数据
 * 
 * 向配置空间的指定寄存器中写入一个字数据
 * 
 * \param bus 设备BUS号
 * \param dev 设备号
 * \param func 设备功能号
 * \param offset 配置空间偏移量
 * \param data 欲写入的数据（一个字）
 *
 * \return 0 写入成功
 * \return -1 写入失败
 */
int pci_write_config_word(int bus, int dev, int func,  
                          int offset, unsigned short data);


/*!
 * \fn int pci_write_config_dword(int bus, int dev, int func,   
 *                          int offset, unsigned int data)
 *                          
 * \brief 向配置空间写数据
 * 
 * 向配置空间的指定寄存器中写入一个长字数据
 * 
 * \param bus 设备BUS号
 * \param dev 设备号
 * \param func 设备功能号
 * \param offset 配置空间偏移量
 * \param data 欲写入的数据（一个长字）
 *
 * \return 0 写入成功
 * \return -1 写入失败
 */
int pci_write_config_dword(int bus, int dev, int func,   
                           int offset, unsigned int data);


/*!
 * \fn int pci_modify_config_dword(int bus, int dev, int func,        
 *                           int offset, unsigned int bitMask, unsigned int data)
 *                           
 * \brief 更新配置空间数据
 * 
 * 更新配置空间一个长字数据
 * 
 * \param bus 设备BUS号
 * \param dev 设备号
 * \param func 设备功能号
 * \param offset 配置空间偏移量
 * \param bitMask 掩码，标识被更改的位
 * \param data 欲更新的数据（一个长字）
 *
 * \return 0 更新成功
 * \return -1 更新失败
 */
int pci_modify_config_dword(int bus, int dev, int func,        
                            int offset, unsigned int bitMask, unsigned int data);


/*!
 * \fn int pci_modify_config_word(int bus, int dev, int func,         
 *                          int offset, unsigned short bitMask, unsigned short data)
 *                          
 * \brief 更新配置空间数据
 * 
 * 更新配置空间命令寄存器一个字数据
 * 
 * \param bus 设备BUS号
 * \param dev 设备号
 * \param func 设备功能号
 * \param offset 配置空间偏移量
 * \param bitMask 掩码，标识被更改的位
 * \param data 欲更新的数据（一个字）
 *
 * \return 0 更新成功
 * \return -1 更新失败
 */
int pci_modify_config_word(int bus, int dev, int func,         
                           int offset, unsigned short bitMask, unsigned short data);


/*!
 * \fn int pci_modify_config_byte(int bus, int dev, int func,         
 *                          int offset, unsigned char bitMask, unsigned char data)
 *                          
 * \brief 更新配置空间数据
 * 
 * 更新配置空间命令寄存器一个字节数据
 * 
 * \param bus 设备BUS号
 * \param dev 设备号
 * \param func 设备功能号
 * \param offset 配置空间偏移量
 * \param bitMask 掩码，标识被更改的位
 * \param data 欲更新的数据（一个字节）
 *
 * \return 0 更新成功
 * \return -1 更新失败
 */
int pci_modify_config_byte(int bus, int dev, int func,         
                           int offset, unsigned char bitMask, unsigned char data);


/*!
 * \fn int pci_special_cycle(int bus, unsigned int message)
 * 
 * \brief 广播消息
 * 
 * 在指定总线上广播消息 
 * 
 * \param bus 	总线号
 * \param message 广播消息
 * 
 * \return 0 广播成功
 * \return -1 广播失败
 */
int pci_special_cycle(int bus, unsigned int message);


/*!
 * \typedef PCI_TRAVERSE_FUNC
 * 
 * \brief 遍历功能操作
 */
typedef int (*PCI_TRAVERSE_FUNC)(int bus, int dev, int func, void *pArg);

/*!
 * \fn int pci_config_traverse_func(unsigned char bus, int recurse,          
                            PCI_TRAVERSE_FUNC check_rtn, void *arg)
                            
 * \brief 在总线设备上运行指定功能函数
 * 
 * 在指定总线上，对总线上的所有设备运行指定的功能函数
 * 
 * \param bus BUS号
 * \param recurse 是否递归查找 
 * \param check_rtn 指定的功能函数
 * \param arg 功能函数参数
 *
 * \return 0 操作成功
 * \return -1 操作失败
 */
//int pci_config_traverse_func(unsigned char bus, int recurse,          
//                            PCI_TRAVERSE_FUNC check_rtn, void *arg);
int pci_config_traverse_func(unsigned char bus,			    
                            int recurse,		   
                            PCI_TRAVERSE_FUNC check_rtn,  
                            void *arg, int prebus); /*ldf 20230901 add:: 解决pci_config_head_show会重复扫描的问题*/


/****************************  PCI类 **************************/
#define PCI_CLASS_PRE_PCI20				0x00	 //!< PCI类
#define PCI_CLASS_MASS_STORAGE			0x01     //!< 大容量存储
#define PCI_CLASS_NETWORK_CTLR			0x02     //!< 网络控制 
#define PCI_CLASS_DISPLAY_CTLR			0x03     //!< 显示 控制
#define PCI_CLASS_MMEDIA_DEVICE			0x04     //!< 多媒体设备 
#define PCI_CLASS_MEM_CTLR				0x05     //!< 内存控制  
#define PCI_CLASS_BRIDGE_CTLR			0x06     //!< 总线桥接控制  
#define PCI_CLASS_COMM_CTLR				0x07     //!< 简单交互控制  
#define PCI_CLASS_BASE_PERIPH			0x08     //!< 基础系统  
#define PCI_CLASS_INPUT_DEVICE			0x09     //!< 输入设备  
#define PCI_CLASS_DOCK_DEVICE			0x0A     //!< 插接设备 
#define PCI_CLASS_PROCESSOR				0x0B     //!< 处理器 
#define PCI_CLASS_SERIAL_BUS			0x0C     //!< 串口总线  
#define PCI_CLASS_WIRELESS				0x0D     //!< 无线 
#define PCI_CLASS_INTLGNT_IO			0x0E     //!< 智能I/O 
#define PCI_CLASS_SAT_COMM				0x0F     //!< 卫星设备交互  
#define PCI_CLASS_EN_DECRYPTION			0x10     //!< 编码/解码  
#define PCI_CLASS_DAQ_DSP				0x11     //!< 数据捕获和信号处理 
#define PCI_CLASS_UNDEFINED				0xFF     //!< 空函数入口 
                                                 
                                                 
/* PCI子类定义 */
#define PCI_SUBCLASS_00         		0x00  //!< PCI子类定义
#define PCI_SUBCLASS_01         		0x01  //!< PCI子类定义
#define PCI_SUBCLASS_02         		0x02  //!< PCI子类定义
#define PCI_SUBCLASS_03        	 		0x03  //!< PCI子类定义
#define PCI_SUBCLASS_04        	 		0x04  //!< PCI子类定义
#define PCI_SUBCLASS_05         		0x05  //!< PCI子类定义
#define PCI_SUBCLASS_06         		0x06  //!< PCI子类定义
#define PCI_SUBCLASS_07        	 		0x07  //!< PCI子类定义
#define PCI_SUBCLASS_08         		0x08  //!< PCI子类定义
#define PCI_SUBCLASS_10         		0x10  //!< PCI子类定义
#define PCI_SUBCLASS_20         		0x20  //!< PCI子类定义
#define PCI_SUBCLASS_40         		0x40  //!< PCI子类定义
#define PCI_SUBCLASS_80         		0x80  //!< PCI子类定义

/* 大容量存储子类 */
#define PCI_SUBCLASS_MASS_SCSI			(PCI_SUBCLASS_00)  //!< 大容量存储子类_SCSI控制器
#define PCI_SUBCLASS_MASS_IDE			(PCI_SUBCLASS_01)  //!< 大容量存储子类_IDE控制器
#define PCI_SUBCLASS_MASS_FLOPPY		(PCI_SUBCLASS_02)  //!< 大容量存储子类_软盘控制器
#define PCI_SUBCLASS_MASS_IPI			(PCI_SUBCLASS_03)  //!< 大容量存储子类_新闻学会控制器
#define PCI_SUBCLASS_MASS_RAID			(PCI_SUBCLASS_04)  //!< 大容量存储子类_RAID控制器
#define PCI_SUBCLASS_MASS_OTHER			(PCI_SUBCLASS_80)  //!< 大容量存储子类_其它大容量存储控制器

/* 网络子类 */
#define PCI_SUBCLASS_NET_ETHERNET		(PCI_SUBCLASS_00)  //!< 网络子类_以太网控制器
#define PCI_SUBCLASS_NET_TOKEN_RING		(PCI_SUBCLASS_01)  //!< 网络子类_令牌环控制器
#define PCI_SUBCLASS_NET_FDDI			(PCI_SUBCLASS_02)  //!< 网络子类_FDDI控制器
#define PCI_SUBCLASS_NET_ATM			(PCI_SUBCLASS_03)  //!< 网络子类_ATM控制器
#define PCI_SUBCLASS_NET_OTHER			(PCI_SUBCLASS_80)  //!< 网络子类_其它网络控制器

/* 显示子类 */
#define PCI_SUBCLASS_DISPLAY_VGA		(PCI_SUBCLASS_00) //!< 显示子类_VGA兼容控制器
#define PCI_REG_IF_VGA_STD		  		0x00              //!< 显示子类_VGA寄存器设置STD
#define PCI_REG_IF_VGA_8514		  		0x01              //!< 显示子类_VGA寄存器设置8514
#define PCI_SUBCLASS_DISPLAY_XGA		(PCI_SUBCLASS_01) //!< 显示子类_XGA控制器
#define PCI_SUBCLASS_DISPLAY_3D			(PCI_SUBCLASS_02) //!< 显示子类_3D控制器
#define PCI_SUBCLASS_DISPLAY_OTHER		(PCI_SUBCLASS_80) //!< 显示子类_其它显示控制器

/* 多媒体子类  */
#define PCI_SUBCLASS_MMEDIA_VIDEO		(PCI_SUBCLASS_00) //!< 多媒体子类_视频设备
#define PCI_SUBCLASS_MMEDIA_AUDIO		(PCI_SUBCLASS_01) //!< 多媒体子类_音频设备
#define PCI_SUBCLASS_MMEDIA_OTHER		(PCI_SUBCLASS_80) //!< 多媒体子类_其它多媒体设备

/* 内存子类  */
#define PCI_SUBCLASS_MEM_RAM			0x00  //!< 内存子类_RAM控制器
#define PCI_SUBCLASS_MEM_FLASH			0x01  //!< 内存子类_闪存控制器
#define PCI_SUBCLASS_MEM_OTHER			0x80  //!< 内存子类_其它内存控制器

/* 总线桥接设备子类  */
#define PCI_SUBCLASS_HOST_PCI_BRIDGE	(PCI_SUBCLASS_00) //!< 总线桥接设备子类_主机
#define PCI_SUBCLASS_ISA_BRIDGE			(PCI_SUBCLASS_01) //!< 总线桥接设备子类_ISA桥
#define PCI_SUBCLASS_P2P_BRIDGE			(PCI_SUBCLASS_04) //!< 总线桥接设备子类_P2P桥
#define PCI_SUBCLASS_PCMCIA_BRIDGE		(PCI_SUBCLASS_05) //!< 总线桥接设备子类_PCMCIA桥
#define PCI_SUBCLASS_CARDBUS_BRIDGE		(PCI_SUBCLASS_07) //!< 总线桥接设备子类_CARDBUS桥

#define PCI_SUBCLASS_BRDG_HOST			(PCI_SUBCLASS_00) //!< 总线桥接设备子类_主机/PCI桥
#define PCI_SUBCLASS_BRDG_ISA			(PCI_SUBCLASS_01) //!< 总线桥接设备子类_PCI/ISA桥
#define PCI_SUBCLASS_BRDG_EISA			(PCI_SUBCLASS_02) //!< 总线桥接设备子类_PCI/EISA桥
#define PCI_SUBCLASS_BRDG_MCA			(PCI_SUBCLASS_03) //!< 总线桥接设备子类_PCI/微通道桥
#define PCI_SUBCLASS_BRDG_P2P			(PCI_SUBCLASS_04) //!< 总线桥接设备子类_PCI/PCI桥
#define PCI_REG_IF_P2P_STD		  		0x00     //!< 总线桥接设备子类_P2P寄存器设置STD
#define PCI_REG_IF_P2P_SUB_DECODE	  	0x01     //!< 总线桥接设备子类_P2P子类编码
#define PCI_SUBCLASS_BRDG_PCMCIA		(PCI_SUBCLASS_05) //!< 总线桥接设备子类_PCI/PCMCIA桥
#define PCI_SUBCLASS_BRDG_NUBUS			(PCI_SUBCLASS_06) //!< 总线桥接设备子类_PCI/NUBUS桥
#define PCI_SUBCLASS_BRDG_CARDBUS		(PCI_SUBCLASS_07) //!< 总线桥接设备子类_PCI/CARDBUS桥
#define PCI_SUBCLASS_BRDG_RACEWAY		(PCI_SUBCLASS_08) //!< 总线桥接设备子类_RACEWAY
#define PCI_REG_IF_RACEWAY_XPARENT	  	0x00    //!< 总线桥接设备子类_RACEWAY寄存器设置XPARENT
#define PCI_REG_IF_RACEWAY_END_PNT	  	0x01    //!< 总线桥接设备子类_RACEWAY寄存器设置END_PNT
#define PCI_SUBCLASS_BRDG_OTHER			(PCI_SUBCLASS_80) //!< 总线桥接设备子类_其它

/* 简单交互控制子类  */
#define PCI_SUBCLASS_SCC_SERIAL			(PCI_SUBCLASS_00) //!< 简单交互控制子类_串行控制器
#define PCI_REG_IF_SERIAL_XT		 	0x00   //!< 简单交互控制子类_XT兼容通用串行控制器
#define PCI_REG_IF_SERIAL_16450	  		0x01   //!< 简单交互控制子类_16450兼容串行控制器
#define PCI_REG_IF_SERIAL_16550	  		0x02   //!< 简单交互控制子类_16550兼容串行控制器
#define PCI_SUBCLASS_SCC_PARLEL			(PCI_SUBCLASS_01) //!< 简单交互控制子类_并行端口
#define PCI_REG_IF_PARLEL_XT		  	0x00   //!< 简单交互控制子类_XT并行端口
#define PCI_REG_IF_PARLEL_BIDIR	  		0x01   //!< 简单交互控制子类_双向并行端口
#define PCI_REG_IF_PARLEL_ECP		  	0x02   //!< 简单交互控制子类_ECP并行端口
#define PCI_SUBCLASS_SCC_MULTI			(PCI_SUBCLASS_02) //!< 简单交互控制子类_MULTI
#define PCI_SUBCLASS_SCC_OTHER			(PCI_SUBCLASS_80) //!< 简单交互控制子类_其它


/* 基础系统子类  */
#define PCI_SUBCLASS_BASESYS_PIC		(PCI_SUBCLASS_00)  //!< 基础系统子类_可编程中断控制器
#define PCI_REG_IF_PIC_GEN8259	  		0x00    //!< 基础系统子类_8259可编程中断控制器
#define PCI_REG_IF_PIC_ISA		  		0x01    //!< 基础系统子类_ISA可编程中断控制器
#define PCI_REG_IF_PIC_EISA		  		0x02    //!< 基础系统子类_EISA可编程中断控制器
#define PCI_REG_IF_PIC_APIC		  		0x10    //!< 基础系统子类_APIC
#define PCI_SUBCLASS_BASESYS_DMA		(PCI_SUBCLASS_01)  //!< 基础系统子类_DMA
#define PCI_REG_IF_DMA_GEN8237	  		0x00    //!< 基础系统子类_DMA寄存器设置GEN8237
#define PCI_REG_IF_DMA_ISA		  		0x01    //!< 基础系统子类_ISA DMA
#define PCI_REG_IF_DMA_EISA		  		0x02    //!< 基础系统子类_EISA DMA
#define PCI_SUBCLASS_BASESYS_TIMER		(PCI_SUBCLASS_02)  //!< 基础系统子类_定时器
#define PCI_REG_IF_TIMER_GEN8254	  	0x00    //!< 基础系统子类_8254定时器
#define PCI_REG_IF_TIMER_ISA		  	0x01    //!< 基础系统子类_ISA定时器      
#define PCI_REG_IF_TIMER_EISA		  	0x02    //!< 基础系统子类_EISA定时器
#define PCI_SUBCLASS_BASESYS_RTC		(PCI_SUBCLASS_03)  //!< 基础系统子类_实时时钟
#define PCI_REG_IF_RTC_GENERIC	  		0x00    //!< 基础系统子类_通用实时时钟控制器
#define PCI_REG_IF_RTC_ISA		  		0x01    //!< 基础系统子类_ISA实时时钟控制器
#define PCI_SUBCLASS_BASESYS_HOTPLUG	(PCI_SUBCLASS_04)  //!< 基础系统子类_HOTPLUG
#define PCI_SUBCLASS_BASESYS_OTHER		(PCI_SUBCLASS_80)  //!< 基础系统子类_其它

/* 输入设备子类  */
#define PCI_SUBCLASS_INPUT_KEYBD		(PCI_SUBCLASS_00) //!< 输入设备子类_键盘控制器
#define PCI_SUBCLASS_INPUT_PEN			(PCI_SUBCLASS_01) //!< 输入设备子类_数字笔转换器
#define PCI_SUBCLASS_INPUT_MOUSE		(PCI_SUBCLASS_02) //!< 输入设备子类_鼠标控制器
#define PCI_SUBCLASS_INPUT_SCANR		(PCI_SUBCLASS_03) //!< 输入设备子类_SCANR控制器
#define PCI_SUBCLASS_INPUT_GAMEPORT		(PCI_SUBCLASS_04) //!< 输入设备子类_游戏端口
#define PCI_REG_IF_GAMEPORT_GENERIC	  	0x00  //!< 输入设备子类_通用游戏端口
#define PCI_REG_IF_GAMEPORT_LEGACY	  	0x10  //!< 输入设备子类_过时游戏端口
#define PCI_SUBCLASS_INPUT_OTHER		(PCI_SUBCLASS_80)//!< 输入设备子类_其它
                                                        
/* 插接设备子类  */
#define PCI_SUBCLASS_DOCSTATN_GENERIC	(PCI_SUBCLASS_00)  //!< 插接设备子类_通用
#define PCI_SUBCLASS_DOCSTATN_OTHER		(PCI_SUBCLASS_80)  //!< 插接设备子类_其它


/* 处理器子类 */
#define PCI_SUBCLASS_PROCESSOR_386		(PCI_SUBCLASS_00) //!< 处理器子类_386
#define PCI_SUBCLASS_PROCESSOR_486		(PCI_SUBCLASS_01) //!< 处理器子类_486
#define PCI_SUBCLASS_PROCESSOR_PENTIUM	(PCI_SUBCLASS_02) //!< 处理器子类_奔腾
#define PCI_SUBCLASS_PROCESSOR_ALPHA	(PCI_SUBCLASS_10) //!< 处理器子类_阿尔法
#define PCI_SUBCLASS_PROCESSOR_POWERPC	(PCI_SUBCLASS_20) //!< 处理器子类_POWERPC
#define PCI_SUBCLASS_PROCESSOR_COPROC	(PCI_SUBCLASS_40) //!< 处理器子类_协处理器
                                                         
/* 串口总线子类  */                                            
#define PCI_SUBCLASS_SERBUS_FIREWIRE	(PCI_SUBCLASS_00) //!< 串口总线子类_火线（IEEE1394）
#define PCI_SUBCLASS_SERBUS_ACCESS		(PCI_SUBCLASS_01) //!< 串口总线子类_访问总线
#define PCI_SUBCLASS_SERBUS_SSA			(PCI_SUBCLASS_02) //!< 串口总线子类_SSA串行存储体系结构
#define PCI_SUBCLASS_SERBUS_USB			(PCI_SUBCLASS_03) //!< 串口总线子类_USB通用串行总线	
#define PCI_REG_IF_USB_UHCI		  		0x00 //!< 串口总线子类_USB寄存器设置UHCI
#define PCI_REG_IF_USB_OHCI		  		0x10 //!< 串口总线子类_USB寄存器设置OHCI
#define PCI_SUBCLASS_SERBUS_FIBRE_CHAN	(PCI_SUBCLASS_04)  //!< 串口总线子类_FIBRE_CHAN
#define PCI_SUBCLASS_SERBUS_SMBUS		(PCI_SUBCLASS_05)  //!< 串口总线子类_SMBUS

/* 无线子类 */
#define PCI_SUBCLASS_WIRELESS_IRDA		(PCI_SUBCLASS_00)  //!< 无线子类_IRDA
#define PCI_SUBCLASS_WIRELESS_OTHER_IR	(PCI_SUBCLASS_01)  //!< 无线子类_其他
#define PCI_SUBCLASS_WIRELESS_RF		(PCI_SUBCLASS_10)  //!< 无线子类_RF
#define PCI_SUBCLASS_WIRELESS_OTHER		(PCI_SUBCLASS_80)  //!< 无线子类_其它


/* 智能I/O子类 */
#define PCI_SUBCLASS_INTELIO			(PCI_SUBCLASS_00)  //!< 智能I/O子类
#define PCI_REG_IF_INTELIO_MSG_FIFO	  	0x00 	//!< 智能I/O子类寄存器设置MSG_FIFO

/* 卫星设备交互子类  */
#define PCI_SUBCLASS_SATCOM_TV			(PCI_SUBCLASS_00)  //!< 卫星设备交互子类_电视
#define PCI_SUBCLASS_SATCOM_AUDIO		(PCI_SUBCLASS_01)  //!< 卫星设备交互子类_声音
#define PCI_SUBCLASS_SATCOM_VOICE		(PCI_SUBCLASS_03)  //!< 卫星设备交互子类_话音
#define PCI_SUBCLASS_SATCOM_DATA		(PCI_SUBCLASS_04)  //!< 卫星设备交互子类_数据

/* 编码/解码子类  */
#define PCI_SUBCLASS_EN_DECRYP_NETWORK	(PCI_SUBCLASS_00)  //!< 编码/解码子类_网络
#define PCI_SUBCLASS_EN_DECRYP_ENTRTMNT	(PCI_SUBCLASS_01)  //!< 编码/解码子类_ENTRTMNT
#define PCI_SUBCLASS_EN_DECRYP_OTHER	(PCI_SUBCLASS_80)  //!< 编码/解码子类_其它

/* 数据捕获和信号处理子类 */
#define PCI_SUBCLASS_DAQ_DSP_DPIO		(PCI_SUBCLASS_00)   //!< 数据捕获和信号处理子类_DPIO 
#define PCI_SUBCLASS_DAQ_DSP_OTHER		(PCI_SUBCLASS_80)   //!< 数据捕获和信号处理子类_其他

/* PCI设备模式 */                                                    
#define SNOOZE_MODE             0x40  //!< PCI设备模式之睡眠模式     
#define SLEEP_MODE_DIS          0x00  //!< PCI设备模式之非能睡眠模式 

/****************************  PCI模块初始化 **************************/
/**
 * \brief 初始化PCI模块
 * 
 * 初始化PCI模块
 * 
 * \return 0 PCI配置初始化成功
 * \return -1 PCI配置初始化失败
 */
int pci_module_init(void);

/****************************  PCI设备显示模块 **************************/
/*!
 * \struct pciHdrDev
 * 
 * \brief PCI设备头部结构定义  
 */
typedef struct pciHdrDev
{
    short	vendor;   		//!< 设备制造商ID 
    short	device;	    	//!< 设备ID 
    short	command;		//!< 命令寄存器 
    short	status;			//!< 状态寄存器 
    char	revision;		//!< 修订ID 
    char	class_code;		//!< 类别码
    char	sub_class;		//!< 子类别码 
    char	prog_if;    	//!< 编程接口 
    char	cache_line;		//!< 缓存大小 
    char	latency;		//!< 等待时间 
    char	header_type;	//!< 头部类型 
    char	bist;			//!< 内置自测 
    int		base0;			//!< 基地址0 
    int		base1;			//!< 基地址1 
    int		base2;			//!< 基地址2 
    int		base3;			//!< 基地址3 
    int		base4;			//!< 基地址4 
    int		base5;			//!< 基地址5 
    int		cis;			//!< cardBus CIS指针 
    short	sub_vendor;	   	//!< 子系统设备制造商 ID 
    short	sub_system;	   	//!< 子系统ID 
    int		rom_base;	   	//!< 拓展ROM基地址 
    int		reserved0;	   	//!< 保留 
    int		reserved1;	   	//!< 保留 
    char	int_line;	   	//!< IRQ编号 
    char	int_pin;	   	//!< 中断引脚 
    char	min_grant;	   	//!< min Grant 
    char	max_latency;   	//!< 最大等待时间 
} PCI_HDR_DEV; //!< PCI设备头部结构定义

/*!
 * \struct pciHdrBrg
 * 
 * \brief PCI桥头部结构定义
 */
typedef struct pciHdrBrg
{
    short	vendor;	      	//!< 设备制造商ID 
    short	device;	      	//!< 设备ID 
    short	command;	  	//!< 命令寄存器 
    short	status;		  	//!< 状态寄存器 
    char	revision;	  	//!< 修订ID 
    char	class_code;	  	//!< 类别码 
    char	sub_class;	  	//!< 子类别码 
    char	prog_if;      	//!< 编程接口 
    char	cache_line;	  	//!< 缓存大小 
    char	latency;	  	//!< 等待时间 
    char	header_type;  	//!< 头部类型 
    char	bist;		  	//!< 内置自测 
    int		base0;		 	//!< 基地址 0 
    int		base1;		  	//!< 基地址 1 
    char	pri_bus;	  	//!< 主总线 
    char	sec_bus;	  	//!< 次总线 
    char	sub_bus;	  	//!< 从属总线数量 
    char	sec_latency;  	//!< 次级总线等待时间 
    char	io_base;	  	//!< IO基址 
    char	io_limit;	  	//!< IO空间大小 
    short	sec_status;	  	//!< 次级总线状态 
    short	mem_base;	  	//!< 内存基址 
    short	mem_limit;	  	//!< 内存大小  
    short	pre_base;	  	//!< 可预取空间基地址
    short	pre_limit;	  	//!< 可预取空间大小 
    int		pre_base_upper;	//!< 32位可预取内存基址 
    int		pre_limit_upper;//!< 32位可预取内存大小 
    short	io_base_upper;	//!< 16位IO基地址 
    short	io_limit_upper;	//!< 16位IO空间大小 
    int		reserved;		//!< 保留 
    int		rom_base;		//!< 拓展ROM基地址 
    char	int_line;		//!< IRQ编号 
    char	int_pin;		//!< 中断引脚 
    short	control;		//!< 桥控制 
} PCI_HDR_BRG; //!< PCI桥头部结构定义

/*!
 * \fn int pci_config_status_word_show(int bus,	int dev,	
								   int func, void *arg)
								   
 * \brief 显示配置状态
 * 
 * 显示配置状态的解码值
 * 
 * \param bus 总线
 * \param dev 设备号
 * \param func 功能号
 * \param arg 暂时无用
 * 
 * \return 0
 */
int pci_config_status_word_show(int bus,	int dev,	
								   int func, void *arg);

/*!
 * \fn int pci_config_cmd_word_show(int bus, int dev,	
								int func, void *arg)
								
 * \brief 显示配置命令
 * 
 * 显示配置命令的解码值
 * 
 * \param bus 总线
 * \param dev 设备号
 * \param func 功能号
 * \param arg 暂时无用
 * 
 * \return 0
 */
int pci_config_cmd_word_show(int bus, int dev,	
								int func, void *arg);

/*!
 * \fn int pci_config_func_show(int bus, int dev,	
						 int func, void *arg)
						 
 * \brief 显示配置功能
 * 
 * 显示关于功能的配置详细信息
 * 
 * \param bus 总线
 * \param dev 设备号
 * \param func 功能号
 * \param arg 暂时无用
 * 
 * \return 0
 */
int pci_config_func_show(int bus, int dev,	
						 int func, void *arg);

/*!
 * \fn void pci_config_topo_show(void)
 * 
 * \brief 显示配置拓扑
 * 
 * 显示PCI拓扑
 * 
 * \return N/A
 */
void pci_config_topo_show(void);


/*!
 * \fn int pci_device_show(int bus)
 * 
 * \brief 打印指定总线上PCI设备信息
 * 
 * 扫描总线上的设备，并将总线上PCI设备信息进行展示
 * 
 * \param bus 指定总线
 * 
 * \return 0 打印成功
 * \return -1 系统未初始化
 */
int pci_device_show(int bus);

/*!
 * \fn int pci_header_show(int	bus, int dev, int func)
 * 
 * \brief 打印指定PCI设备头部结构
 * 
 * 通过总线号，设备号，功能号查找设备头部信息，并进行打印
 * 
 * \param bus 总线号
 * \param dev 设备号
 * \param func 设备功能号
 * 
 * \return 0 打印成功
 * \return -1 系统未初始化
 */
int pci_header_show(int	bus, int dev, int func);

/*!
 * \fn int pci_find_device_show(int vendor, int device, int index)
 * 
 * \brief 打印指定PCI设备信息
 * 
 * 通过设备制造商ID，设备ID，设备实例号查找设备，并进行打印相关信息
 * 
 * \param vendor 设备制造商ID
 * \param device 设备ID
 * \param index 设备实例号
 * 
 * \return 0 打印成功
 * \return -1 系统未初始化
 */
int pci_find_device_show(int vendor, int device, int index);

/*!
 * \fn int pci_find_class_show(int	class_code, int index)
 * 
 * \brief 打印指定PCI设备信息
 * 
 * 通过24位类别码、设备实例号查找设备，并进行打印相关信息
 * 
 * \param class_code 24位类别码
 * \param index 设备实例号
 * 
 * \return 0 打印成功
 * \return -1 系统未初始化
 */
int pci_find_class_show(int	class_code, int index);


/*!
 * \fn int pci_show_module_init(void)
 * 
 * \brief PCI显示模块初始化接口
 * 
 * \param 无
 * 
 * \return 0 打印成功
 * \return -1 系统未初始化
 */
int pci_show_module_init(void);

/* @} */

#ifdef __cplusplus
}
#endif

#endif	
