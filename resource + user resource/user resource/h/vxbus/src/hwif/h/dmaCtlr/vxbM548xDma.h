/* vxbM548xDma.h - header file for Coldfire MCD slave DMA VxBus driver */

/*
 * Copyright (c) 2007 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,26jan07,wap  written
*/

#ifndef __INCvxbM548xDmah
#define __INCvxbM548xDmah

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void m548xDmaDrvRegister (void);

#ifndef BSP_VERSION

#include <../src/hwif/h/dmaCtlr/MCD_dma.h>
#include <../src/hwif/h/dmaCtlr/dma_utils.h>

typedef struct mcdChanInfo
    {
    int mcd_initiator;
    int mcd_chanId;
    int mcd_channel;
    int mcd_priority;
    } MCD_CHAN_INFO;

typedef struct mcdDedicatedInfo
    {
    UINT32 mcd_magic;
    int mcd_chanId;
    int mcd_priority;
    } MCD_DEDICATED_INFO;

#define MCD_MAGIC	0x4D434400

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbM548xDmah */
