/* vxbIbmMalDma.h - header file for IBM/AMCC MAL slave DMA VxBus driver */

/*
 * Copyright (c) 2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,15jan08,wap  written
*/

#ifndef __INCvxbIbmMalDmah
#define __INCvxbIbmMalDmah

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void vxbMalRegister (void);

#define MAL_TYPE_DUAL		1
#define MAL_TYPE_QUAD		2

#define MAL_TYPE_405GP		MAL_TYPE_DUAL
#define MAL_TYPE_440GP		MAL_TYPE_DUAL
#define MAL_TYPE_440EP		MAL_TYPE_DUAL
#define MAL_TYPE_440GX		MAL_TYPE_QUAD

#ifndef BSP_VERSION

typedef struct malChanInfo
    {
    VXB_DEVICE_ID pDev;
    pVXB_DMA_COMPLETE_FN completeFunc;
    void * pArg;
    int flags;
    int mal_chanId;
    int mal_channel;
    int mal_priority;
    void * mal_descmem;
    } MAL_CHAN_INFO;

typedef struct malDedicatedInfo
    {
    UINT32 mal_magic;
    int mal_emacUnit;
    int mal_priority;
    } MAL_DEDICATED_INFO;

#define MAL_MAGIC	0x4D414C00

#define MAL_CFG		0x0000
#define MAL_ESR		0x0001
#define MAL_IER		0x0002
#define MAL_DBR		0x0003	/* debug register */
#define MAL_TXCASR	0x0004
#define MAL_TXCARR	0x0005
#define MAL_TXEOBISR	0x0006
#define MAL_TXDEIR	0x0007
#define MAL_TXTATTRR	0x0008	/* GX parts only */
#define MAL_TXBADDR	0x0009	/* GX parts only */
#define MAL_RXCASR	0x0010
#define MAL_RXCARR	0x0011
#define MAL_RXEOBISR	0x0012
#define MAL_RXDEIR	0x0013
#define MAL_RXTATTRR	0x0014	/* GX parts only */
#define MAL_RXBADDR	0x0015	/* GX parts only */
#define MAL_TXCTP0R	0x0020
#define MAL_TXCTP1R	0x0021
#define MAL_TXCTP2R	0x0022
#define MAL_TXCTP3R	0x0023
#define MAL_RXCTP0R	0x0040
#define MAL_RXCTP1R	0x0041
#define MAL_RXCTP2R	0x0042	/* GX parts only */
#define MAL_RXCTP3R	0x0043	/* GX parts only */
#define MAL_RCBS0	0x0060
#define MAL_RCBS1	0x0061
#define MAL_RCBS2	0x0062	/* GX parts only */
#define MAL_RCBS3	0x0063	/* GX parts only */

/* Configuration register */

#define MAL_CFG_SRST	0x80000000	/* Software reset */
#define MAL_CFG_PLBP	0x00C00000	/* PLB priority */
#define MAL_CFG_GA	0x00200000	/* guarded active */
#define MAL_CFG_OA	0x00100000	/* ordered active */
#define MAL_CFG_PLBLE	0x00080000	/* PLB lock error */
#define MAL_CFG_PLBLT	0x00078000	/* PLB latency timer */
#define MAL_CFG_PLBB	0x00004000	/* PLB burst */
#define MAL_CFG_OPBBL	0x00000080	/* OPB bus lock */
#define MAL_CFG_EOPIE	0x00000004	/* end of packet int enable */
#define MAL_CFG_LEA	0x00000002	/* locked error active */
#define MAL_CFG_SD	0x00000001	/* scroll descriptor */

/* Configuration register bits for GX parts */

#define MAL_CFGGX_SRST	0x80000000	/* Software reset */
#define MAL_CFGGX_RPP	0x00C00000	/* Read PLB priority */
#define MAL_CFGGX_RMBS	0x00300000	/* Read max burst size */
#define MAL_CFGGX_WPP	0x000C0000	/* Write PLB priority */
#define MAL_CFGGX_WMBS	0x00030000	/* Write max burst size */
#define MAL_CFGGX_PLBLE	0x00008000	/* PLB lock error */
#define MAL_CFGGX_PLBB	0x00004000	/* PLB burst */
#define MAL_CFGGX_OPBBL	0x00000080	/* OPB bus lock */
#define MAL_CFGGX_EOPIE	0x00000004	/* end of packet int enable */
#define MAL_CFGGX_LEA	0x00000002	/* locked error active */
#define MAL_CFGGX_SD	0x00000001	/* scroll descriptor */

#define MAL_RPP_LO	0x00000000
#define MAL_RPP_MEDLO	0x00400000
#define MAL_RPP_MEDHI	0x00800000
#define MAL_RPP_HI	0x00C00000

#define MAL_RMBS_4BEATS		0x00000000
#define MAL_RMBS_8BEATS		0x00100000
#define MAL_RMBS_16BEATS	0x00200000
#define MAL_RMBS_32BEATS	0x00300000

#define MAL_WPP_LO	0x00000000
#define MAL_WPP_MEDLO	0x00040000
#define MAL_WPP_MEDHI	0x00080000
#define MAL_WPP_HI	0x000C0000

#define MAL_WMBS_4BEATS		0x00000000
#define MAL_WMBS_8BEATS		0x00010000
#define MAL_WMBS_16BEATS	0x00020000
#define MAL_WMBS_32BEATS	0x00030000

/* Error status register */

#define MAL_ESR_EVB	0x80000000	/* Error valid */
#define MAL_ESR_CID	0x7F000000	/* Channel ID */
#define MAL_ESR_DE	0x00100000	/* Descriptor error */
#define MAL_ESR_ONE	0x00080000	/* OPB non-fullword error */
#define MAL_ESR_OTE	0x00040000	/* OPB timeout error */
#define MAL_ESR_OSR	0x00020000	/* OPB slave error */
#define MAL_ESR_PEIN	0x00010000	/* PLB bus error */
#define MAL_ESR_DEI	0x00000010	/* descriptor error interrupt */
#define MAL_ESR_ONEI	0x00000008	/* OPB non-fullword error interrupt */
#define MAL_ESR_OTEI	0x00000004	/* OPB timeout error interrupt */
#define MAL_ESR_OSRI	0x00000002	/* OPB slave error interrupt */
#define MAL_ESR_PEINI	0x00000001	/* PLB bus error interrupt */

/* Error status register bits for GX parts */

#define MAL_ESRGX_CIDT	0x40000000	/* Channel type */
#define MAL_ESRGX_CID	0x3F000000	/* Channel ID */

#define MAL_CID(x)	((x) >> 24)
#define MAL_CIDT(x)	((x) >> 30)

/* Interrupt enable register */

#define MAL_IER_DEI	0x00000010	/* descriptor error interrupt */
#define MAL_IER_ONEI	0x00000008	/* OPB non-fullword error interrupt */
#define MAL_IER_OTEI	0x00000004	/* OPB timeout error interrupt */
#define MAL_IER_OSRI	0x00000002	/* OPB slave error interrupt */
#define MAL_IER_PEINI	0x00000001	/* PLB bus error interrupt */

/* TX channel active set */

#define MAL_TXCASR_CH0		0x80000000
#define MAL_TXCASR_CH1		0x40000000
#define MAL_TXCASR_CH2		0x20000000
#define MAL_TXCASR_CH4		0x10000000

#define MAL_TXCA_SET(x)		(1 << (31 - (x)))

/* TX channel active reset */

#define MAL_TXCARR_CH0		0x80000000
#define MAL_TXCARR_CH1		0x40000000
#define MAL_TXCARR_CH2		0x20000000
#define MAL_TXCARR_CH4		0x10000000

#define MAL_TXCA_RESET(x)	(1 << (31 - (x)))

/* RX channel active set */

#define MAL_RXCASR_CH0		0x80000000
#define MAL_RXCASR_CH1		0x40000000
#define MAL_RXCASR_CH2		0x20000000
#define MAL_RXCASR_CH3		0x10000000

#define MAL_RXCA_SET(x)		(1 << (31 - (x)))

/* RX channel active reset */

#define MAL_RXCARR_CH0		0x80000000
#define MAL_RXCARR_CH1		0x40000000
#define MAL_RXCARR_CH2		0x20000000
#define MAL_RXCARR_CH3		0x10000000

#define MAL_RXCA_RESET(x)	(1 << (31 - (x)))

/* TX end of buffer ISR */

#define MAL_TXEOBISR_CH0	0x80000000
#define MAL_TXEOBISR_CH1	0x40000000
#define MAL_TXEOBISR_CH2	0x20000000
#define MAL_TXEOBISR_CH3	0x10000000

#define MAL_TXEOBISR_SET(x)	(1 << (31 - (x)))

/* RX end of buffer ISR */

#define MAL_RXEOBISR_CH0	0x80000000
#define MAL_RXEOBISR_CH1	0x40000000
#define MAL_RXEOBISR_CH2	0x20000000
#define MAL_RXEOBISR_CH3	0x10000000

#define MAL_RXEOBISR_SET(x)	(1 << (31 - (x)))

/* TX channel error interrupt */

#define MAL_TXDEIR_CH0		0x80000000
#define MAL_TXDEIR_CH1		0x40000000
#define MAL_TXDEIR_CH2		0x20000000
#define MAL_TXDEIR_CH3		0x10000000

#define MAL_TXDEIR_GET(x)	(1 << (31 - (x)))

/* RX channel error interrupt */

#define MAL_RXDEIR_CH0		0x80000000
#define MAL_RXDEIR_CH1		0x40000000
#define MAL_RXDEIR_CH2		0x20000000
#define MAL_RXDEIR_CH3		0x10000000

#define MAL_RXDEIR_GET(x)	(1 << (31 - (x)))

/*
 * The MAL channel layout varies depending on the number of EMACs
 * present in the device. Some chips have only two EMACs, but some
 * have four. In the dual EMAC case, the devices are both 10/100.
 * In the four controller case, two EMACs are 10/100 and the other
 * two are 10/100/1000. In the dual EMAC configuration, each EMAC
 * has two TX channels allocated to it, which allows for two TX
 * DMA queues. Our EMAC driver only uses one channel.
 */

/* Dual EMAC MAL channel layout */

#define MAL_DUAL_EMAC0_RX	0
#define MAL_DUAL_EMAC1_RX	1

#define MAL_DUAL_EMAC0_TX0	0
#define MAL_DUAL_EMAC0_TX1	1
#define MAL_DUAL_EMAC1_TX0	2
#define MAL_DUAL_EMAC1_TX1	3

/* Quad EMAC MAL channel layout */

#define MAL_QUAD_EMAC0_RX	0
#define MAL_QUAD_EMAC1_RX	1
#define MAL_QUAD_EMAC2_RX	2
#define MAL_QUAD_EMAC3_RX	3

#define MAL_QUAD_EMAC0_TX	0
#define MAL_QUAD_EMAC1_TX	1
#define MAL_QUAD_EMAC2_TX	2
#define MAL_QUAD_EMAC3_TX	3


/*
 * MAL descriptor format, used for both RX and TX queues. Note that
 * in the 440GX parts, the upper 4 bits of the mal_len field can be
 * used to specify an additional 4 bits of buffer address, making the
 * buffer pointer effectively 36 bits wide. In the other parts, the
 * upper 4 bits are reserved.
 */

typedef struct mal_desc
    {
    volatile UINT16	mal_ctlsts;
    volatile UINT16	mal_len;
    volatile UINT32	mal_ptr;
    } MAL_DESC;

#define MAL_TXSTS_READY		0x8000
#define MAL_TXSTS_WRAP		0x4000
#define MAL_TXSTS_CONTINUOUS	0x2000
#define MAL_TXSTS_LAST		0x1000
#define MAL_TXSTS_INTR		0x0400

#define MAL_RXSTS_EMPTY		0x8000
#define MAL_RXSTS_WRAP		0x4000
#define MAL_RXSTS_CONTINUOUS	0x2000
#define MAL_RXSTS_LAST		0x1000
#define MAL_RXSTS_FIRST		0x0800
#define MAL_RXSTS_INTR		0x0400

#define MAL_MAX_CHANNELS	32
#define MAL_MAX_EMACS		4
#define MAL_CHAN_IDX(x)		(1 << (31 - x))

typedef struct mal_drv_ctrl
    {
    VXB_DEVICE_ID	malDev;
    void *		malBar;
    void *		malHandle;
    int			malType;
    VXB_DMA_RESOURCE_ID	malRxChans[MAL_MAX_CHANNELS];
    VXB_DMA_RESOURCE_ID	malTxChans[MAL_MAX_CHANNELS];
    spinlockIsr_t	malLock;

    VXB_DMA_TAG_ID	malParentTag;
    VXB_DMA_TAG_ID	malDescTag;
    VXB_DMA_MAP_ID	malDescMap;
    MAL_DESC *		malDescMem;
    void *		malRxChanAddr[MAL_MAX_CHANNELS];
    void *		malTxChanAddr[MAL_MAX_CHANNELS];

    } MAL_DRV_CTRL;

#define MAL_RX_DESC_CNT		64
#define MAL_TX_DESC_CNT		64

#define MAL_DESC_CNT ((MAL_RX_DESC_CNT + MAL_TX_DESC_CNT) * MAL_MAX_EMACS)

#define DCR_READ_4(pDev, offset)	\
	dcrInLong((UINT32)(pDev)->pRegBase[0] + offset)
#define DCR_WRITE_4(pDev, offset, val)	\
	dcrOutLong((UINT32)(pDev)->pRegBase[0] + offset, val)

#define DCR_SETBIT_4(pDev, offset, val)          \
        DCR_WRITE_4(pDev, offset, DCR_READ_4(pDev, offset) | (val))

#define DCR_CLRBIT_4(pDev, offset, val)          \
        DCR_WRITE_4(pDev, offset, DCR_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbIbmMalDmah */

