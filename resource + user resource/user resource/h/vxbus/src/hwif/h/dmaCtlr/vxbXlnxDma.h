/* vxbXlnxDma.h - header file for Xlnx DMA VxBus driver */

/*
 * Copyright (c) 2009-2010 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,17nov10,e_d  fix name issue for function
01a,24jun09,e_d  written
*/

#ifndef __INCvxbXlnxDmah
#define __INCvxbXlnxDmah

#ifdef __cplusplus
extern "C"
{
#endif

/* DMA DCR Address Map */

#define XLNX_DMA_TXNXDPTR  0x80  /* TX Next Descriptor Pointer Register */
#define XLNX_DMA_TXCUADDR  0x81  /* TX Current Buffer Address Register */
#define XLNX_DMA_TXCULENR  0x82  /* TX Current Buffer Length Register */
#define XLNX_DMA_TXCUPTR   0x83  /* TX Current Descriptor Pointer */
#define XLNX_DMA_TXTAILPTR 0x84  /* TX Tail Descriptor Pointer Register */
#define XLNX_DMA_TXCTLR    0x85  /* TX Channel Control Register */
#define XLNX_DMA_TXIRQR    0x86  /* TX Interrupt Register */
#define XLNX_DMA_TXSTSR    0x87  /* TX Status Register */
#define XLNX_DMA_RXNXDPTR  0x88  /* RX Next Descriptor Pointer Register */
#define XLNX_DMA_RXCUADDR  0x89  /* RX Current Buffer Address Register */
#define XLNX_DMA_RXCULENR  0x8A  /* RX Current Buffer Length Register */
#define XLNX_DMA_RXCUPTR   0x8B  /* RX Current Descriptor Pointer */
#define XLNX_DMA_RXTAILPTR 0x8C  /* RX Tail Descriptor Pointer Register */
#define XLNX_DMA_RXCTLR    0x8D  /* RX Channel Control Register */
#define XLNX_DMA_RXIRQR    0x8E  /* RX Interrupt Register */
#define XLNX_DMA_RXSTSR    0x8F  /* RX Status Register */
#define XLNX_DMA_CTLR      0x90  /* DMA Control Register */

/* XLNX_DMA0_TXCULENR */

#define XLNX_DMA_TXCULENR_SET(x)  (((x)<<8) & 0xffffff00)

/* XLNX_DMA0_TXCTLR */

#define XLNX_DMA_TXCTLR_CMIE  (1<<0)  /* Coalescing interrupt Enable */
#define XLNX_DMA_TXCTLR_DTMIE (1<<1)  /* Delay Time interrupt Enalbe */
#define XLNX_DMA_TXCTLR_EDMIE (1<<2)  /* Error Detection interrupt Enable */
#define XLNX_DMA_TXCTLR_MIE   (1<<7)  /* Master interrupt Enable */
#define XLNX_DMA_TXCTLR_LICC  (1<<8)  /* Load interrupt Coalescing Counter */
#define XLNX_DMA_TXCTLR_UIOEM (1<<9)  /* Use the interrupt-On-End Mechanism */
#define XLNX_DMA_TXCTLR_U1IC  (1<<10) /* Use 1-bit interrupt Counters */

#define XLNX_DMA_TXCTLR_MSBADD(x)  (((x)<<12) & 0xf0000)
#define XLNX_DMA_TXCTLR_ICCV(x)    (((x)<<16) & 0xff0000)
#define XLNX_DMA_TXCTLR_IDTV(x)    (((x)<<24) & 0xff000000)

/* XLNX_DMA_TXIRQR */

#define XLNX_DMA_TXIRQR_CCI   (1<<0) /* Coalescing interrupt */
#define XLNX_DMA_TXIRQR_DTI   (1<<1) /* Delay Time interrupt */
#define XLNX_DMA_TXIRQR_EI    (1<<2) /* Error Detection interrupt */
#define XLNX_DMA_TXIRQR_PWENI (1<<3) /*PLB Write Error Non-Maskable Int */
#define XLNX_DMA_TXIRQR_PRENI (1<<4) /*PLB Read Error Non-Maskable Int */

#define XLNX_DMA_TXIRQR_DTIC(x) (((x)>>8) & 0x03)
#define XLNX_DMA_TXIRQR_CIC(x)  (((x)>>10) & 0x0f)
#define XLNX_DMA_TXIRQR_CCV(x)  (((x)>>16) & 0xff)
#define XLNX_DMA_TXIRQR_DTV(x)  (((x)>>24) & 0xff)

/* XLNX_DMA_TXSTSR */

#define XLNX_DMA_TXSTSR_TCB  (1<<1)  /* Tx Channel Busy */
#define XLNX_DMA_TXSTSR_DEP  (1<<2)  /* DMA End of Packet */
#define XLNX_DMA_TXSTSR_DSP  (1<<3)  /* DMA Start of  Packet */
#define XLNX_DMA_TXSTSR_DC   (1<<4)  /* DMA Completed */
#define XLNX_DMA_TXSTSR_DSOE (1<<5)  /* DMA Stop On End */
#define XLNX_DMA_TXSTSR_DIOE (1<<6)  /* DMA Interrupt on End */
#define XLNX_DMA_TXSTSR_DE   (1<<7)  /* DMA Error */

/* XLNX_DMA0_RXCULENR */

#define XLNX_DMA_RXCULENR_SET(x)  (((x)<<8) & 0xffffff00)

/* XLNX_DMA0_RXCTLR */

#define XLNX_DMA_RXCTLR_CMIE  (1<<0)  /* Coalescing interrupt Enable */
#define XLNX_DMA_RXCTLR_DTMIE (1<<1)  /* Delay Time interrupt Enalbe */
#define XLNX_DMA_RXCTLR_EDMIE (1<<2)  /* Error Detection interrupt Enable */
#define XLNX_DMA_RXCTLR_MIE   (1<<7)  /* Master interrupt Enable */
#define XLNX_DMA_RXCTLR_LICC  (1<<8)  /* Load interrupt Coalescing Counter */
#define XLNX_DMA_RXCTLR_UIOEM (1<<9)  /* Use the interrupt-On-End Mechanism */
#define XLNX_DMA_RXCTLR_U1IC  (1<<10) /* Use 1-bit interrupt Counters */

#define XLNX_DMA_RXCTLR_MSBADD(x)  (((x)<<12) & 0xf0000)
#define XLNX_DMA_RXCTLR_ICCV(x)    (((x)<<16) & 0xff0000)
#define XLNX_DMA_RXCTLR_IDTV(x)    (((x)<<24) & 0xff000000)

/* XLNX_DMA_RXIRQR */

#define XLNX_DMA_RXIRQR_CCI   (1<<0)  /* Coalescing interrupt */
#define XLNX_DMA_RXIRQR_DTI   (1<<1)  /* Delay Time interrupt */
#define XLNX_DMA_RXIRQR_EI    (1<<2)  /* Error Detection interrupt */
#define XLNX_DMA_RXIRQR_PWENI (1<<3)  /*PLB Write Error Non-Maskable Int */
#define XLNX_DMA_RXIRQR_PRENI (1<<4)  /*PLB Read Error Non-Maskable Int */
#define XLNX_DMA_RXIRQR_WCQS  (1<<15) /* Write Command Queue Empty Status */

#define XLNX_DMA_RXIRQR_DTIC(x) (((x)>>8) & 0x03)
#define XLNX_DMA_RXIRQR_CIC(x)  (((x)>>10) & 0x0f)
#define XLNX_DMA_RXIRQR_CCV(x)  (((x)>>16) & 0xff)
#define XLNX_DMA_RXIRQR_DTV(x)  (((x)>>24) & 0xff)

/* XLNX_DMA_RXSTSR */

#define XLNX_DMA_RXSTSR_TCB  (1<<1)  /* Tx Channel Busy */
#define XLNX_DMA_RXSTSR_DEP  (1<<2)  /* DMA End of Packet */
#define XLNX_DMA_RXSTSR_DSP  (1<<3)  /* DMA Start of  Packet */
#define XLNX_DMA_RXSTSR_DC   (1<<4)  /* DMA Completed */
#define XLNX_DMA_RXSTSR_DSOE (1<<5)  /* DMA Stop On End */
#define XLNX_DMA_RXSTSR_DIOE (1<<6)  /* DMA Interrupt on End */
#define XLNX_DMA_RXSTSR_DE   (1<<7)  /* DMA Error */

/* XLNX_DMA_CTLR */

#define XLNX_DMA_CTLR_SOFTRST (1<<0)  /* Software Reset */
#define XLNX_DMA_CTLR_TPE     (1<<2)  /* Tail Pointer Enable */
#define XLNX_DMA_CTLR_TXOCEID (1<<3)  /* Tx Overflow Counter Error Interrupt Disable */
#define XLNX_DMA_CTLR_RXOCEID (1<<4)  /* Rx Overflow Counter Error Interrupt Disable */
#define XLNX_DMA_CTLR_PED     (1<<5)  /* PLB Error Disable */
#define XLNX_DMA_CTLR_RXPAUSE 0x10000000 /* Pause Rx Dma channel */
#define XLNX_DMA_CTLR_TXPAUSE 0x20000000 /* Pause Tx Dma channel */

/* XLNX DMA descriptor format, used for both RX and TX queues. */

typedef struct xlnxdma_desc
    {
    volatile UINT32 xlnxdma_next;
    volatile UINT32 xlnxdma_bufpointer;
    volatile UINT32 xlnxdma_buflen;
    volatile UINT32 xlnxdma_ctlsts;
    volatile UINT32 xlnxdma_usrdefine1;
    volatile UINT32 xlnxdma_usrdefine2;
    volatile UINT32 xlnxdma_usrdefine3;
    volatile UINT32 xlnxdma_usrdefine4;
    } XLNXDMA_DESC;

/* XLNX DMA descriptor status/control bit define */

#define XLNXDMA_DESC_ERR     (1<<31) /* DMA_ERROR */
#define XLNXDMA_DESC_IOE     (1<<30) /* DMA_INT_ON_END */
#define XLNXDMA_DESC_SOE     (1<<29) /* DMA_STOP_ON_END */
#define XLNXDMA_DESC_COMPLET (1<<28) /* DMA_COMPLETE */
#define XLNXDMA_DESC_SOP     (1<<27) /* DMA_START_OF_PACKERT */
#define XLNXDMA_DESC_EOP     (1<<26) /* DMA_END_OF_PACKERT */
#define XLNXDMA_DESC_CB      (1<<25) /* DMA_CHANNEL_BUSY */

#define XLNXDMA_RX_DESC_CNT  128
#define XLNXDMA_TX_DESC_CNT  64

#define XLNXDMA_DESC_CNT ((XLNXDMA_RX_DESC_CNT + XLNXDMA_TX_DESC_CNT))

typedef struct xlnxDmaChanInfo
    {
    VXB_DEVICE_ID pDev;
    pVXB_DMA_COMPLETE_FN completeFunc;
    void * pArg;
    int flags;
    int xlnxdma_chanId;
    int xlnxdma_channel;
    int xlnxdma_bdfree;
    void * xlnxdma_descmem;
    } XLNXDMA_CHAN_INFO;

typedef struct xlnxdma_drv_ctrl
    {
    VXB_DEVICE_ID xlnxDmaDev;
    void * xlnxDmaBar;
    void * xlnxDmaHandle;
    int xlnxDmaType;
    int xlnxRxIntVec;
    int xlnxTxIntVec;
    VXB_DMA_RESOURCE_ID xlnxDmaRxChans;
    VXB_DMA_RESOURCE_ID xlnxDmaTxChans;
    spinlockIsr_t xlnxDmaLock;
    VXB_DMA_TAG_ID xlnxDmaParentTag;
    VXB_DMA_TAG_ID xlnxDmaDescTag;
    VXB_DMA_MAP_ID xlnxDmaDescMap;
    XLNXDMA_DESC * xlnxDmaDescMem;
    int xlnxDmaTxTail;
    int xlnxDmaRxTail;
    int xlnxDmaTxHead;
    int xlnxDmaRxHead;
    void * xlnxDmaTxChan;
    void * xlnxDmaRxChan;
    } XLNXDMA_DRV_CTRL;

typedef struct xlnxDmaDedicatedInfo
    {
    UINT32 xlnddma_magic;
    int xlnxdma_emacUnit;
    int xlnxdma_priority;
    } XLNXDMA_DEDICATED_INFO;

#define DCR_READ_4(pDev, offset)        \
    dcrInLong((UINT32)(pDev)->pRegBase[0] + offset)

#define DCR_WRITE_4(pDev, offset, val)  \
    dcrOutLong((UINT32)(pDev)->pRegBase[0] + offset, val)

#define DCR_SETBIT_4(pDev, offset, val)          \
        DCR_WRITE_4(pDev, offset, DCR_READ_4(pDev, offset) | (val))

#define DCR_CLRBIT_4(pDev, offset, val)          \
        DCR_WRITE_4(pDev, offset, DCR_READ_4(pDev, offset) & ~(val))

#define DMA_INC_DESC(x, y)      (x) = ((x + 1) & (y - 1))

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbXlnxDmah */
