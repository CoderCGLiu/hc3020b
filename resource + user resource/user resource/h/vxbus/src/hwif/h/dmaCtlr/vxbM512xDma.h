/* vxbM512xDma.h - Freescale MPC512x DMA controller driver */

/*
 * Copyright (c) 2007-2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01e,30jun11,e_d  add MPC8306 related macros
01d,31jul10,syt  add MPC8308 related macros
01c,04mar09,b_m  add DMA transfer flag definitions.
01b,02jan08,b_m  update to use new vxbus access routines.
01a,30oct07,b_m  written.
*/

#ifndef __INCvxbM512xDmah
#define __INCvxbM512xDmah

#ifdef __cplusplus
extern "C" {
#endif

#define SVR_MPC5121E        0x80180000
#define SVR_MPC8308         0x81010000
#define SVR_MPC8306         0x81100000

/* DMA transfer flags */

#define DMA_XFER_NORMAL     0
#define DMA_XFER_FIXED_SRC  1
#define DMA_XFER_FIXED_DST  2
#define DMA_XFER_FIXED_ALL  4

#ifndef BSP_VERSION

/* DMA controller register offsets */

#define DMA_CR      0x0000  /* Control Register */
#define DMA_ES      0x0004  /* Error Register */
#define DMA_ERQH    0x0008  /* Enable Request High (Channels 63-32) */
#define DMA_ERQL    0x000c  /* Enable Request Low (Channels 31-00) */
#define DMA_EEIH    0x0010  /* Enable Error Interrupt High (Channels 63-32) */
#define DMA_EEIL    0x0014  /* Enable Error Interrupt Low (Channels 31-00) */
#define DMA_SERQ    0x0018  /* Set Enable Request */
#define DMA_CERQ    0x0019  /* Clear Enable Request */
#define DMA_SEEI    0x001a  /* Set Enable Error Interrupt */
#define DMA_CEEI    0x001b  /* Clear Enable Error Interrupt */
#define DMA_CINT    0x001c  /* Clear Interrupt Request */
#define DMA_CERR    0x001d  /* Clear Error */
#define DMA_SSRT    0x001e  /* Set Start Bit */
#define DMA_CDNE    0x001f  /* Clear Done Status Bit */
#define DMA_INTH    0x0020  /* Interrupt Request High (Channels 63-32) */
#define DMA_INTL    0x0024  /* Interrupt Request Low (Channels 31-00) */
#define DMA_ERRH    0x0028  /* Error High (Channels 63-32) */
#define DMA_ERRL    0x002c  /* Error Low (Channels 31-00) */
#define DMA_HRSH    0x0030  /* Hardware Request Status High (Channels 63-32) */
#define DMA_HRSL    0x0034  /* Hardware Request Status Low (Channels 31-00) */
#define DMA_IHSA    0x0038  /* Interrupt High Select AXE (Channels 63-32) */
#define DMA_ILSA    0x003c  /* Interrupt Low Select AXE (Channels 31-00) */

/* DMA channel priority */

#define DMA_CHPRI(n)    (0x0100 + n)    /* n = channel number (0<=n<=63) */

/* TCD base address & size  */

#define DMA_TCD_BASE            0x1000
#define DMA_TCD_SIZE            0x20

/* DMA controller register bit fields */

/* Control Resgister */

/* clock dynamic gating */

#define DMA_CLK_DYN_GATING_DIS  0x00000000
#define DMA_CLK_DYN_GATING_EN   0x80000000

/* group priorities value */

#define DMA_GRP_PRI_HIGHEST     3
#define DMA_GRP_PRI_HIGHER      2
#define DMA_GRP_PRI_LOWER       1
#define DMA_GRP_PRI_LOWEST      0

/* group priorities shift */

#define DMA_GRP3_PRI_SHIFT      14
#define DMA_GRP2_PRI_SHIFT      12
#define DMA_GRP1_PRI_SHIFT      10
#define DMA_GRP0_PRI_SHIFT      8

/* group priorities mask */

#define DMA_GRP3_PRI_MASK       0x0000c000
#define DMA_GRP2_PRI_MASK       0x00003000
#define DMA_GRP1_PRI_MASK       0x00000c00
#define DMA_GRP0_PRI_MASK       0x00000300

/* group priority policy */

#define DMA_GRP_PRI_FIXED       0x00000000
#define DMA_GRP_PRI_RNDROBIN    0x00000008

/* channel priority policy */

#define DMA_CHAN_PRI_FIXED      0x00000000
#define DMA_CHAN_PRI_RNDROBIN   0x00000004

/* debug */

#define DMA_DEBUG_EN            0x00000002
#define DMA_DEBUG_DIS           0x00000000

/* buffered writes */

#define DMA_BUFFERED_WR_EN      0x00000001
#define DMA_BUFFERED_WR_DIS     0x00000000

/* Erros Status Register */

#define DMA_ERR_VLD             0x80000000  /* error indication */
#define DMA_ERR_GPE             0x00008000  /* group priority error */
#define DMA_ERR_CPE             0x00004000  /* channel priority error */
#define DMA_ERR_SAE             0x00000080  /* source address error */
#define DMA_ERR_SOE             0x00000040  /* source offset error */
#define DMA_ERR_DAE             0x00000020  /* destination address error */
#define DMA_ERR_DOE             0x00000010  /* destination offset error */
#define DMA_ERR_NCE             0x00000008  /* nbytes/citer config error */
#define DMA_ERR_SGE             0x00000004  /* scatter/gather config error */
#define DMA_ERR_SBE             0x00000002  /* source bus error */
#define DMA_ERR_DBE             0x00000001  /* destination bus error */

/* Clear all interrupt request */

#define DMA_CLR_ALL_INT_REQUEST 64

/* error channel number mask */

#define DMA_ERR_CHAN_NO_MASK    0x00003c00

/* Channel Priority Register */

/* channel preemption */

#define DMA_CHAN_PREEMPTION_EN  0x80
#define DMA_CHAN_PREEMPTION_DIS 0x00

/* channel priority mask */

#define DMA_CHAN_PRI_MASK       0x0f

/* max channels */

#define M5121_DMA_MAX_CHANNELS    64
#define M8308_DMA_MAX_CHANNELS    16

/* max groups */

#define DMA_MAX_GROUPS      4

/* channels per group */

#define DMA_CHANS_PER_GRP   16

/* channel number -> group number */

#define DMA_CHANNO_TO_GRPNO(chan)   (chan / DMA_CHANS_PER_GRP)

/* transfer control descriptors */

typedef struct m512xDmaTcd
    {
    volatile INT32 saddr;       /* source address */
    volatile INT16 attr;        /* transfer attributes */
    volatile INT16 soff;        /* signed source address offset */
    volatile INT32 nbytes;      /* inner minor byte count */
    volatile INT32 slast;       /* last source address adjustment */
    volatile INT32 daddr;       /* destination address */
    volatile INT16 citer;       /* current major iteration count */
    volatile INT16 doff;        /* signed destination address offset */
    volatile INT32 dlast_sga;   /* last dest addr adjustment/scatter gather addr */
    volatile INT16 biter;       /* beginning major iteration count */
    volatile INT16 chcs;        /* channel control/status */
    } M512XDMA_TCD;

/* TCD control & status bit fields */

#define DMA_TCD_BW_CTRL0        0x0000
#define DMA_TCD_BW_CTRL1        0x4000
#define DMA_TCD_BW_CTRL2        0x8000
#define DMA_TCD_BW_CTRL3        0xc000

#define DMA_TCD_DONE            0x0080
#define DMA_TCD_ACTIVE          0x0040
#define DMA_TCD_REQ_DIS         0x0008
#define DMA_TCD_INT_HALF        0x0004
#define DMA_TCD_INT_MAJ         0x0002
#define DMA_TCD_START           0x0001

/* DMA channel data */

typedef struct m512xDmaChan
    {
    UINT32          grpNum;                 /* DNA channel group number */
    UINT32          chanNum;                /* DMA channel number */
    UINT32          grpPri;                 /* DMA channel group priority */
    UINT32          chanPri;                /* DMA channel priority */
    void            (*isr)(void * pArg);    /* interrupt handler */
    void *          pArg;                   /* ISR parameter */
    M512XDMA_TCD    *tcd;                   /* channel TCD address */
    } M512XDMA_CHAN;

/* DMA resource data */

typedef struct m512xDmaRes
    {
    BOOL            used;       /* DMA channel used or not */
    M512XDMA_CHAN * pChan;      /* DMA channel data */
    } M512XDMA_RES;

/* DMA driver control */

typedef struct m512xDmaDrvCtrl
    {
    VXB_DEVICE_ID   pDev;
    SEM_ID          chanMutex;
    UINT32          chanMaxNum;
    UINT32          cpuSvr;
    M512XDMA_RES *  chanRes;
    void *          regBase;
    void *          handle;
    } M512XDMA_DRV_CTRL;

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCvxbM512xDmah */
