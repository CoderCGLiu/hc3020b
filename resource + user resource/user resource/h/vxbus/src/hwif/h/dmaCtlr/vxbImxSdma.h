/* vxbImxSdma.h - Freescale SDMA controller driver */

/*
 * Copyright (c) 2011, 2012, 2014 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
21nov14,y_f  Fix the crash during sdma transfer (VXW6-83821)
01c,17may12,hcl  add i.MX6 ECSPI support.
01b,01apr12,hcl  remove sdma_code. (WIND00295511)
01a,25apr11,hcl  written.
*/

#ifndef __INCvxbImxSdmah
#define __INCvxbImxSdmah

#ifdef __cplusplus
extern "C" {
#endif

/* driver name */

#define DMA_DRV_NAME                "imxSdma"

#define SDMA_CHAN_NUM               32
#define SDMA_REQ_NUM                48

#define SDMA_DEF_PRIORITY           0
#define SDMA_MIN_PRIORITY           1
#define SDMA_MAX_PRIORITY           7
#define SDMA_MDL_PRIORITY           5

/* SDMA request */

#define SDMA_REQ_CSPI1_RX           6
#define SDMA_REQ_CSPI1_TX           7
#define SDMA_REQ_CSPI2_RX           8
#define SDMA_REQ_CSPI2_TX           9
#define SDMA_REQ_UART2_RX           16
#define SDMA_REQ_UART2_TX           17
#define SDMA_REQ_UART1_RX           18
#define SDMA_REQ_UART1_TX           19
#define SDMA_REQ_UART3_RX           43
#define SDMA_REQ_UART3_TX           44

/* i.MX6 SDMA request */

#define IMX6_SDMA_REQ_ECSPI1_RX     3
#define IMX6_SDMA_REQ_ECSPI1_TX     4

/* Memory map for the AP control SDMA registers */

#define SDMA_MC0PTR         0x00    /* AP (MCU) Channel 0 Pointer */
#define SDMA_INTR           0x04    /* Channel Interrupts */
#define SDMA_STOP_STAT      0x08    /* Channel Stop/Channel Status */
#define SDMA_HSTART         0x0C    /* Channel Start */
#define SDMA_EVTOVR         0x10    /* Channel Event Override */
#define SDMA_DSPOVR         0x14    /* Channel BP Override */
#define SDMA_HOSTOVR        0x18    /* Channel AP Override */
#define SDMA_EVTPEND        0x1C    /* Channel Event Pending */
#define SDMA_RESET          0x24    /* Reset Register */
#define SDMA_EVTERR         0x28    /* DMA Request Error Register */
#define SDMA_INTRMASK       0x2C    /* Channel AP Interrupt Mask */
#define SDMA_PSW            0x30    /* Schedule Status */
#define SDMA_EVTERRDBG      0x34    /* DMA Request Error Register */
#define SDMA_CONFIG         0x38    /* Configuration Register */
#define SDMA_SDMA_LOCK      0x3C    /* SDMA LOCK */
#define SDMA_ONCE_ENB       0x40    /* OnCE Enable */
#define SDMA_ONCE_DATA      0x44    /* OnCE Data Register */
#define SDMA_ONCE_INSTR     0x48    /* OnCE Instruction Register */
#define SDMA_ONCE_STAT      0x4C    /* OnCE Status Register */
#define SDMA_ONCE_CMD       0x50    /* OnCE Command Register */
#define SDMA_ILLINSTADDR    0x58    /* Illegal Instruction Trap Addr */
#define SDMA_CHN0ADDR       0x5C    /* Channel 0 Boot Address */
#define SDMA_EVT_MIRROR     0x60    /* DMA Requests */
#define SDMA_EVT_MIRROR2    0x64    /* DMA Requests 2 */
#define SDMA_XTRIG_CONF1    0x70    /* Cross-Trigger Events Conf reg1 */
#define SDMA_XTRIG_CONF2    0x74    /* Cross-Trigger Events Conf reg2 */
#define SDMA_OTB            0x78    /* Once Trace Buffer Register */
#define SDMA_PRF_CNT(n)     (0x7C + 4 * (n)) 	/* Profile counter Registers */
#define SDMA_PRF_CFG        0x94    /* Profile config/status Register */
#define SDMA_CHNPRI(n)      (0x100 + 4 * (n))   /* Channel Priority Registers */
#define SDMA_CHNENBL(n)     (0x200 + 4 * (n))   /* Channel Enable RAM */

/* registers bits definition */

#define SDMA_CHN0ADDR_SMSZ      (0x1 << 14)

#define SDMA_CHNPRI_CHNPRI_MASK (0x7 << 0)

#define SDMA_RESET_RESET        (0x1 << 0)
#define SDMA_RESET_RESCHED      (0x1 << 1)

#define SDMA_CONFIG_GCM_MASK    (0x3 << 0)
#define SDMA_CONFIG_GCM_STC     (0x0 << 0)
#define SDMA_CONFIG_GCM_DLP     (0x1 << 0)
#define SDMA_CONFIG_GCM_DNP     (0x2 << 0)
#define SDMA_CONFIG_GCM_DNC     (0x3 << 0)
#define SDMA_CONFIG_ACR         (0x1 << 4)

/* Buffer Descriptor */

#define SDMA_BD_CMD_OFFSET      24

/* SDMA peripheral types */

typedef enum
    {
    SDMA_PERIPH_TYPE_UART = 0,  /* MCU domain UART */
    SDMA_PERIPH_TYPE_MEMORY,    /* memory */
    SDMA_PERIPH_TYPE_CSPI       /* SPI */
    } SDMA_PERIPH_TYPE;

/* SDMA transfer types */

typedef enum
    {
    SDMA_TRANSFER_TYPE_EMI_2_EMI = 0,   /* EMI memory to EMI memory */
    SDMA_TRANSFER_TYPE_EMI_2_PER,       /* EMI memory to peripheral */
    SDMA_TRANSFER_TYPE_PER_2_EMI        /* Peripheral to EMI memory */
    } SDMA_TRANSFER_TYPE;

/* Channel Zero Buffer Descriptor Commands */

typedef enum
    {
    SDMA_C0_SET_DM  = 0x01,
    SDMA_C0_GET_DM  = 0x02,
    SDMA_C0_GETCTXT = 0x03,
    SDMA_C0_SET_PM  = 0x04,
    SDMA_C0_GET_PM  = 0x06,
    SDMA_C0_SETCTX  = 0x07
    } SDMA_CMD_C0;

/* Buffer descriptor flags */

typedef enum
    {
    SDMA_FLAGS_DONE  = (1 << 16),
    SDMA_FLAGS_WRAP  = (1 << 17),
    SDMA_FLAGS_CONT  = (1 << 18),
    SDMA_FLAGS_INT   = (1 << 19),
    SDMA_FLAGS_ERROR = (1 << 20),
    SDMA_FLAGS_LAST  = (1 << 21)
    } SDMA_BD_FLAGS;

/* Data transfer length */

typedef enum
    {
    SDMA_DATA_32BIT  = 0,
    SDMA_DATA_8BIT   = 1,
    SDMA_DATA_16BIT  = 2,
    SDMA_DATA_24BIT  = 3
    } SDMA_DATA_TYPE;

/* Channel Control Block */

typedef struct
    {
    volatile UINT32 currentBDptr;
    volatile UINT32 baseBDptr;
    volatile UINT32 chanDesc;
    volatile UINT32 status;
    } SDMA_CHAN_CTRL_BLOCK;

/* Buffer Descriptor */

typedef struct
    {
    volatile UINT32 mode;
    volatile UINT32 bufAddr;
    volatile UINT32 extBufAddr;
    } SDMA_CHAN_BUF_DESC;

/* Channel Context */

typedef struct
    {
    volatile UINT32 PC;
    volatile UINT32 SPC;
    volatile UINT32 GReg[8];
    volatile UINT32 MDA;
    volatile UINT32 MSA;
    volatile UINT32 MS;
    volatile UINT32 MD;
    volatile UINT32 PDA;
    volatile UINT32 PSA;
    volatile UINT32 PS;
    volatile UINT32 PD;
    volatile UINT32 Reserved1;
    volatile UINT32 Reserved2;
    volatile UINT32 DDA;
    volatile UINT32 DSA;
    volatile UINT32 DS;
    volatile UINT32 DD;
    volatile UINT32 ScratchRAM[8];
    } SDMA_CHAN_CONTEXT;

/* Channel Parameters */

typedef struct
    {
    UINT32  wmLevel;            /* watermark level */
    UINT32  bufDescNum;         /* chnnel's BD number */
    UINT32  event;              /* event */
    UINT32  eventMask0;         /* mask 0 */
    UINT32  eventMask1;         /* mask 1 */
    UINT32  scriptAddr;         /* chnnel script start address */
    UINT32  peripAddr;          /* peripheral address */
    SDMA_PERIPH_TYPE periphType;    /* peripheral type */
    SDMA_TRANSFER_TYPE tansferType; /* tansfer type */
    } SDMA_CHAN_EXT_PARAM;

/* Channel Parameters */

typedef struct
    {
    UINT32  chanNo;                     /* channel number */
    BOOL    chanUsed;                   /* channel occupied flag*/
    UINT32  chanPriority;               /* channel priority */
    void    (*callback)(void * pArg);   /* channel callback function */
    void    *pArg;                      /* callback parameters */
    SDMA_CHAN_EXT_PARAM * extParams;    /* channel extend params */
    } SDMA_CHAN_PARAM;

/* SDMA Resource */

typedef struct
    {
    SDMA_CHAN_CTRL_BLOCK    chanCtrlBlk[SDMA_CHAN_NUM]; /* CCB */
    SDMA_CHAN_PARAM         chanParam[SDMA_CHAN_NUM];   /* channel parameters */
    SDMA_CHAN_BUF_DESC      chanBufDesc[SDMA_CHAN_NUM]; /* BD */
    SDMA_CHAN_CONTEXT       chanContext;                /* context */
    } IMX_SDMA_RES;

/* SDMA driver controller */

typedef struct imxSdmaDrvCtrl
    {
    VXB_DEVICE_ID       pInst;
    void *              regBase;
    void *              handle;
    SEM_ID              chanMutex;
    spinlockIsr_t       chanLock;
    IMX_SDMA_RES *      sdmaRes;
    } IMX_SDMA_DRV_CTRL;

/* SDMA ROM scripts start addresses and sizes */

#define start_ADDR                  0
#define start_SIZE                  24

#define core_ADDR8                  0
#define core_SIZE                   232

#define common_ADDR                 312
#define common_SIZE                 330

#define ap_2_ap_ADDR                642
#define ap_2_ap_SIZE                41

#define app_2_mcu_ADDR              683
#define app_2_mcu_SIZE              64

#define mcu_2_app_ADDR              747
#define mcu_2_app_SIZE              70

#define uart_2_mcu_ADDR             817
#define uart_2_mcu_SIZE             75

#define shp_2_mcu_ADDR              892
#define shp_2_mcu_SIZE              69

#define mcu_2_shp_ADDR              961
#define mcu_2_shp_SIZE              72

#define app_2_per_ADDR              1033
#define app_2_per_SIZE              66

#define per_2_app_ADDR              1099
#define per_2_app_SIZE              74

#define per_2_shp_ADDR              1173
#define per_2_shp_SIZE              78

#define shp_2_per_ADDR              1251
#define shp_2_per_SIZE              72

#define uartsh_2_mcu_ADDR           323
#define uartsh_2_mcu_SIZE           69

#define mcu_2_ata_ADDR              1392
#define mcu_2_ata_SIZE              81

#define ata_2_mcu_ADDR              1473
#define ata_2_mcu_SIZE              96

#define loop_DMAs_routines_ADDR1    569
#define loop_DMAs_routines_SIZE     227

#define test_ADDR                   1796
#define test_SIZE                   63

#define signature_ADDR              1023
#define signature_SIZE              1

/* SDMA RAM scripts start addresses and sizes */

#define RAM_CODE_START_ADDR         6144
#define RAM_CODE_SIZE               498

#define ext_mem_ipu_ram_ADDR        6144
#define ext_mem_ipu_ram_SIZE        123

#define firi_2_mcu_ADDR             6267
#define firi_2_mcu_SIZE             97

#define mcu_2_firi_ADDR             6364
#define mcu_2_firi_SIZE             79

#define mcu_2_spdif_ADDR            6443
#define mcu_2_spdif_SIZE            59

#define uart_2_per_ADDR             6502
#define uart_2_per_SIZE             73

#define uartsh_2_per_ADDR           6575
#define uartsh_2_per_SIZE           67

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCvxbImxSdmah */
