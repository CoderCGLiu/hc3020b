/* MCD_tasksInit.h - function prototypes for initializing task tables */

/*
Copyright (c) 2005-2006 Wind River Systems, Inc.

Copyright 1995-2004 Freescale Semiconductor, Inc.

This software is licensed pursuant to the terms of the
Freescale Semiconductor software license agreement for
M54x5EVB dBUG source code.
*/

/*
modification history
--------------------
01b,01feb06,j_b  add copyright for Freescale originated source; formatting
01a,20oct05,lsg  Written based on Freescale M54x5EVB dBUG source.
*/

/*
This file contains function prototypes for initializing variable tables
of different types of tasks.
*/

#ifndef __INCmcd_tasksinith
#define __INCmcd_tasksinith

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Do not edit!
 */


/*
 * Task 0
 */
void  MCD_startDmaChainNoEu(int *currBD, short srcIncr, short destIncr,
                            int xferSize, short xferSizeIncr, int *cSave,
                            volatile TaskTableEntry *taskTable, int channel);


/*
 * Task 1
 */
void  MCD_startDmaSingleNoEu(char *srcAddr, short srcIncr, char *destAddr,
                             short destIncr, int dmaSize, short xferSizeIncr,
                             int flags, int *currBD, int *cSave,
                             volatile TaskTableEntry *taskTable, int channel);


/*
 * Task 2
 */
void  MCD_startDmaChainEu(int *currBD, short srcIncr, short destIncr,
                          int xferSize, short xferSizeIncr, int *cSave,
                          volatile TaskTableEntry *taskTable, int channel);


/*
 * Task 3
 */
void  MCD_startDmaSingleEu(char *srcAddr, short srcIncr, char *destAddr,
                           short destIncr, int dmaSize, short xferSizeIncr,
                           int flags, int *currBD, int *cSave,
                           volatile TaskTableEntry *taskTable, int channel);


/*
 * Task 4
 */
void  MCD_startDmaENetRcv(char *bDBase, char *currBD, char *rcvFifoPtr,
                          volatile TaskTableEntry *taskTable, int channel);


/*
 * Task 5
 */
void  MCD_startDmaENetXmit(char *bDBase, char *currBD, char *xmitFifoPtr,
                           volatile TaskTableEntry *taskTable, int channel);

#ifdef __cplusplus
}
#endif

#endif /* __INCmcd_tasksinith */

