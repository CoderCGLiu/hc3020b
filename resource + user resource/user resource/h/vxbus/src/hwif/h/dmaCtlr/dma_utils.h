/* dma_utils.h - header file for multi-channel DMA general purpose utilities */

/*
Copyright (c) 2005-2006 Wind River Systems, Inc.

Copyright 1995-2004 Freescale Semiconductor, Inc.

This software is licensed pursuant to the terms of the
Freescale Semiconductor software license agreement for
M54x5EVB dBUG source code.
*/

/*
modification history
--------------------
01c,06feb07,wap  Cleanup
01b,01feb06,j_b  add copyright for Freescale originated source; formatting
01a,20oct05,lsg  Written based on Freescale M54x5EVB dBUG source.
*/

/*
This file includes definitions for the multi-channel DMA
general purpose utilities.
*/


#ifndef __INCdma_utilsh
#define __INCdma_utilsh

#ifdef __cplusplus
extern "C" {
#endif

#ifndef INT8
#define INT8    char
#endif

#ifndef INT16
#define INT16   int
#endif

#ifndef INT32
#define INT32   long
#endif

/********************************************************************/

void   dma_irq_enable (VXB_DEVICE_ID, UINT8, UINT8);
void   dma_irq_disable (VXB_DEVICE_ID);
int    dma_set_initiator (VXB_DEVICE_ID, int);
UINT32 dma_get_initiator (int);
void   dma_free_initiator (int);
int    dma_set_channel (int, void (*)(void *), void *);
int    dma_get_channel (int);
void   dma_free_channel (int);
void   displayDmaRegs(VXB_DEVICE_ID);
void   dmaProgReport(int channel);
int    dmaStatus(int channel);
void   dma_interrupt_handler (VXB_DEVICE_ID);

/********************************************************************/

/*
 * Create identifiers for each initiator/requestor
 */

#define DMA_ALWAYS      (0)
#define DMA_DSPI_RX     (1)
#define DMA_DSPI_TX     (2)
#define DMA_DREQ0       (3)
#define DMA_PSC0_RX     (4)
#define DMA_PSC0_TX     (5)
#define DMA_USBEP0      (6)
#define DMA_USBEP1      (7)
#define DMA_USBEP2      (8)
#define DMA_USBEP3      (9)
#define DMA_PCI_TX      (10)
#define DMA_PCI_RX      (11)
#define DMA_PSC1_RX     (12)
#define DMA_PSC1_TX     (13)
#define DMA_I2C_RX      (14)
#define DMA_I2C_TX      (15)
#define DMA_FEC0_RX     (16)
#define DMA_FEC0_TX     (17)
#define DMA_FEC1_RX     (18)
#define DMA_FEC1_TX     (19)
#define DMA_DREQ1       (20)
#define DMA_CTM0        (21)
#define DMA_CTM1        (22)
#define DMA_CTM2        (23)
#define DMA_CTM3        (24)
#define DMA_CTM4        (25)
#define DMA_CTM5        (26)
#define DMA_CTM6        (27)
#define DMA_CTM7        (28)
#define DMA_USBEP4      (29)
#define DMA_USBEP5      (30)
#define DMA_USBEP6      (31)
#define DMA_PSC2_RX     (32)
#define DMA_PSC2_TX     (33)
#define DMA_PSC3_RX     (34)
#define DMA_PSC3_TX     (35)
#define DMA_FEC_RX(x)   ((x == 0) ? DMA_FEC0_RX : DMA_FEC1_RX)
#define DMA_FEC_TX(x)   ((x == 0) ? DMA_FEC0_TX : DMA_FEC1_TX)

/********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* __INCdma_utilsh */
