/* vxbIntelEdma.h - EDMA header file */

/*
 * Copyright (c) 2007-2009 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01c,19jan09,z_l  use callback routine saved by node. (WIND00151147)
01b,05nov08,j_z  add VXB_DMA_RESOURCE_ID in struct EDMA_CHAN
01a,10Oct07,d_z  created
*/

#ifndef __INCvxbIntelEdmah
#define __INCvxbIntelEdmah

#ifdef __cplusplus
extern "C" {
#endif

#include <lstLib.h>
#include <semLib.h>

/* edma registers, fields and masks */

/* EDMA specific PCI Configuration space registers */

#define EDMA_LBAR               0x10
#define EDMA_CTL                0x40
#define EDMA_FERR               0x80
#define EDMA_NERR               0x84
#define EDMA_EMASK              0x88
#define EDMA_SCICMD             0xA0
#define EDMA_SMICMD             0xA4
#define EDMA_SERRCMD            0xA8
#define EDMA_MCERRCMD           0xAC

#define EDMA_CTL_ENABLE         0x80
#define EDMA_CTL_PARITY         0x01
#define EDMA_FERR_RESET         0xffffffff
#define EDMA_EMASK_DSCPEM       0x80
#define EDMA_EMASK_DSCPAE       0x40
#define EDMA_EMASK_SRCEM        0x20
#define EDMA_EMASK_DESTEM       0x04
#define EDMA_EMASK_MDPARERR     0x02
#define EDMA_EMASK_IWERR        0x01

/* PCI Express registers */

#define MSICR               0xB0
#define MSIAR               0xB4
#define MSIDR               0xB8

/* register offsets */

#define EDMA_CCR            0x00 /* Channel Control Register                  */
#define EDMA_CSR            0x04 /* Channel Status Register                   */
#define EDMA_CDAR           0x08 /* Current Descriptor Address Register       */
#define EDMA_CDUAR          0x0C /* Current Descriptor Upper Address Register */
#define EDMA_SAR            0x10 /* Source Address Register                   */
#define EDMA_SUAR           0x14 /* Source Upper Address Register             */
#define EDMA_DAR            0x18 /* Destination Address Register              */
#define EDMA_DUAR           0x1C /* Destination Upper Address Register        */
#define EDMA_NDAR           0x20 /* Next Descriptor Address Register          */
#define EDMA_NDUAR          0x24 /* Next Descriptor Upper Address Register    */
#define EDMA_TCR            0x28 /* Transfer Count Register                   */
#define EDMA_DCR            0x2C /* Descriptor Control Register               */

#define EDMA_DCGC           0x100 /* EDMA Controller Global Command Register  */
#define EDMA_DCGS           0x104 /* EDMA Controller Global Status Register   */


/* bit fields */

#define EDMA_CCR_CRSM               0x00000008
#define EDMA_CCR_STPDMA             0x00000004
#define EDMA_CCR_SUSDMA             0x00000002
#define EDMA_CCR_STRTDMA            0x00000001
#define EDMA_CSR_CACTV              0x00000020
#define EDMA_CSR_DABRT              0x00000010
#define EDMA_CSR_DSTP               0x00000008
#define EDMA_CSR_DSUS               0x00000004
#define EDMA_CSR_EOT                0x00000002
#define EDMA_CSR_EOC                0x00000001
#define EDMA_CDAR_MASK              0xffffffe0
#define EDMA_CDUAR_MASK             0xffffffff
#define EDMA_SAR_MASK               0xffffffff
#define EDMA_SUAR_MASK              0xffffffff
#define EDMA_DAR_MASK               0xffffffff
#define EDMA_DUAR_MASK              0xffffffff
#define EDMA_NDAR_MASK              0xffffffff
#define EDMA_NDUAR_MASK             0xffffffff
#define EDMA_TCR_MASK               0x01ffffff
#define EDMA_DCR_TRAFFIC_MASK       0xe0000000
#define EDMA_DCR_1BYTE              0x00000000
#define EDMA_DCR_2BYTE              0x00020000
#define EDMA_DCR_4BYTE              0x00040000
#define EDMA_DCR_DST_CONSTANT       0x00004000  /* fifo mode */
#define EDMA_DCR_SRC_DECREMENT      0x00001000
#define EDMA_DCR_BUFFER_INIT        0x00002000
#define EDMA_DCR_SRCC               0x00000100
#define EDMA_DCR_DSTC               0x00000080
#define EDMA_DCR_SRCT               0x00000040
#define EDMA_DCR_DSTT               0x00000020
#define EDMA_DCR_DABRTIE            0x00000010
#define EDMA_DCR_DSTPIE             0x00000008
#define EDMA_DCR_DSUSIE             0x00000004
#define EDMA_DCR_EOTIE              0x00000002
#define EDMA_DCR_EOCIE              0x00000001
#define EDMA_DCGC_PCENBL            0x00000004
#define EDMA_DCGC_PCSLT3            0x00000003
#define EDMA_DCGC_PCSLT2            0x00000002
#define EDMA_DCGC_PCSLT1            0x00000001
#define EDMA_DCGC_PCSLT0            0x00000000
#define EDMA_DCGS_EIC               0x00000001
#define EDMA_DCGS_NIC               0x00000002

/* PCI identifiers */

#define EDMA_VENDOR_ID              0x8086
#define EDMA_DEVICE_ID              0x35B5

/* special constants */

#define EDMA_TCR_16MB_MAX           0x01000000
#define EDMA_DESCRIPTOR_ALIGNMENT   0x20

/* user configurable */

#define EDMA_NUM_DESCRIPTORS        0x10

typedef struct edma_chan
    {
    unsigned int    chanId;
    SEM_ID          mutex;
    FUNCPTR         callback;
    unsigned int    csr;            /* copy of channel CSR register */
    LIST            dList;
    VXB_DMA_RESOURCE_ID pRes;
    } EDMA_CHAN;

typedef struct edma_descriptor
    {
    unsigned int    sar;
    unsigned int    suar;
    unsigned int    dar;
    unsigned int    duar;
    unsigned int    ndar;
    unsigned int    nduar;
    unsigned int    tcr;
    unsigned int    dcr;
    } EDMA_DESCRIPTOR;

typedef struct edma_node
    {
    struct node *       next;       /* Points at the next node in the list */
    struct node *       previous;   /* Points at the previous node in the list */
    EDMA_DESCRIPTOR *   descriptor; /* pointer to 0x20 byte aligned EDMA descriptor */
    FUNCPTR             callback;
    unsigned int        user_data;  /* 32bit field for data use */
    unsigned int        cookie;     /* descriptors in the same batch have the same cookie */
    } EDMA_NODE;

/* 4KB memory region */

#define EDMA_LBAR_SIZE              0x1000
#define EDMA_NUM_CHANNELS           4

typedef struct edma_device
    {
    VXB_DEVICE_ID   vxbDev;
    PHYS_ADDR       base;
    void *          baseHandle;     /*type of base, mem or io*/
    LIST            freeList;
    EDMA_NODE       nodeList[EDMA_NUM_DESCRIPTORS];
    EDMA_CHAN*      clients[EDMA_NUM_CHANNELS];
    } EDMA_DEVICE;

typedef struct edmaChanInfo
    {
    EDMA_CHAN *     pDmaChan;
    } EDMA_CHAN_INFO;

extern STATUS edmaMemCopy
    (
    EDMA_CHAN *     pChan,
    UINT32          dcr,
    PHYS_ADDR       dst,
    PHYS_ADDR       src,
    UINT32          size,
    FUNCPTR         completeFunc,
    UINT32          user_data
    );

typedef enum {
    write_error        = EDMA_EMASK_IWERR,
    parity_error       = EDMA_EMASK_MDPARERR,
    dst_address_error  = EDMA_EMASK_DESTEM,
    src_address_error  = EDMA_EMASK_SRCEM,
    ndar_align_error   = EDMA_EMASK_DSCPAE,
    ndar_address_error = EDMA_EMASK_DSCPEM
} EDMA_ERROR;

enum
    {
    INTEL_EDMA_VENDOR_ID            = 0x8086,
    INTEL_3100_EDMA_DEVICE_ID       = 0x35B5,
    INTEL_TOLAPAI_EDMA_DEVICE_ID    = 0x5023
    };

/* public declarations */

IMPORT STATUS edmaInit (void);
IMPORT EDMA_CHAN * edmaReserveChannel (void);
IMPORT EDMA_CHAN * edmaReservePriorityChannel (void);
IMPORT STATUS edmaReleaseChannel (EDMA_CHAN * pChannel);
IMPORT STATUS edmaHookAdd (EDMA_CHAN * pChan, FUNCPTR pCallback);
IMPORT STATUS edmaHookDelete (EDMA_CHAN * pChan);
IMPORT STATUS edmaSuspend (EDMA_CHAN * pChan);
IMPORT STATUS edmaResume (EDMA_CHAN * pChan);
IMPORT STATUS edmaStop (EDMA_CHAN * pChan);
IMPORT EDMA_ERROR edmaGetError (EDMA_CHAN * pChan);
IMPORT STATUS edmaPollStatus (EDMA_CHAN * pChan);


#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif  /* __INCvxbIntelEdmah */
