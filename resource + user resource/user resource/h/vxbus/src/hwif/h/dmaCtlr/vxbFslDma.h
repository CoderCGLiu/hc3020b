/* vxbFslDma.h - FreeScale DMA controller driver header file */

/*
 * Copyright (c) 2009, 2010, 2012-2015 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
21aug15,wyt  add DMA_EXC_TASK_PRI. (VXW6-84774)
09jan15,p_x  add definitions to support extended chaining mode. (VXW6-82903)
24nov14,wyt  add APIs to support 40-bit physical address. (VXW6-83774)
13nov14,wyt  fix prevent error.
01f,14may13,y_y  add one variable in FSL_DMA_DRV_CTRL.
01e,02apr13,y_y  update the code. (WIND00405789)
01d,12nov12,syt  add a parameter chanNum to specify the DMA channel number.
01c,28mar12,sye  remove compile warnings. (WIND00288572)
01b,19Aug10,y_c  redefine the struct fslDmaNode.(WIND00227589)
01a,10jun09,syt  created
*/

#ifndef __INCvxbFslDmah
#define __INCvxbFslDmah

#ifdef __cplusplus
extern "C" {
#endif

#include <lstLib.h>
#include <semLib.h>

/* DMA registers, fields and masks */

/* DMA register offsets */

#define DMA_MR                  0x00
#define DMA_SR                  0x04
#define DMA_ECLNDAR             0x08
#define DMA_CLNDAR              0x0C
#define DMA_SATR                0x10
#define DMA_SAR                 0x14
#define DMA_DATR                0x18
#define DMA_DAR                 0x1C
#define DMA_BCR                 0x20
#define DMA_ENLNDAR             0x24
#define DMA_NLNDAR              0x28
#define DMA_ECLSDAR             0x30
#define DMA_CLSDAR              0x34
#define DMA_ENLSDAR             0x38
#define DMA_NLSDAR              0x3C
#define DMA_SSR                 0x40
#define DMA_DSR                 0x44
#define DMA_DGSR                0x200
                                
#define DMA_CHAN_OFFSET         0x80

/* Registers bit fields */

/* Mode Registers bits */

#define DMA_MR_BWC_BIT          7                      
#define DMA_MR_EMP_EN_BIT       10
#define DMA_MR_EMP_EN           (0x01 << (31 - DMA_MR_EMP_EN_BIT))
#define DMA_MR_EMS_EN_BIT       13
#define DMA_MR_EMS_EN           (0x01 << (31 - DMA_MR_EMS_EN_BIT))
#define DMA_MR_DAHTS_BIT        14
#define DMA_MR_SAHTS_BIT        16
#define DMA_MR_DAHE_BIT         18
#define DMA_MR_DAHE             (0x01 << (31 - DMA_MR_DAHE_BIT  ))
#define DMA_MR_SAHE_BIT         19
#define DMA_MR_SAHE             (0x01 << (31 - DMA_MR_SAHE_BIT  ))
#define DMA_MR_SRW_BIT          21
#define DMA_MR_SRW              (0x01 << (31 - DMA_MR_SRW_BIT   ))
#define DMA_MR_EOSIE_BIT        22
#define DMA_MR_EOSIE            (0x01 << (31 - DMA_MR_EOSIE_BIT ))
#define DMA_MR_EOLNIE_BIT       23
#define DMA_MR_EOLNIE           (0x01 << (31 - DMA_MR_EOLNIE_BIT))
#define DMA_MR_EOLSIE_BIT       24
#define DMA_MR_EOLSIE           (0x01 << (31 - DMA_MR_EOLSIE_BIT))
#define DMA_MR_EIE_BIT          25
#define DMA_MR_EIE              (0x01 << (31 - DMA_MR_EIE_BIT   ))
#define DMA_MR_XFE_BIT          26
#define DMA_MR_XFE              (0x01 << (31 - DMA_MR_XFE_BIT   ))
#define DMA_MR_CDSM_BIT         27
#define DMA_MR_CDSM             (0x01 << (31 - DMA_MR_CDSM_BIT  ))
#define DMA_MR_CA_BIT           28
#define DMA_MR_CA               (0x01 << (31 - DMA_MR_CA_BIT    ))
#define DMA_MR_CTM_BIT          29
#define DMA_MR_CTM              (0x01 << (31 - DMA_MR_CTM_BIT   ))
#define DMA_MR_CC_BIT           30
#define DMA_MR_CC               (0x01 << (31 - DMA_MR_CC_BIT    ))
#define DMA_MR_CS_BIT           31
#define DMA_MR_CS               (0x01 << (31 - DMA_MR_CS_BIT    ))

#define DMA_BW_256B             (0x08 << (31 - DMA_MR_BWC_BIT   ))
#define DMA_MR_BWC_MASK         (0x0f << (31 - DMA_MR_BWC_BIT   ))
#define DMA_MR_DAHTS_MASK       (0x03 << (31 - DMA_MR_DAHTS_BIT ))
#define DMA_MR_SAHTS_MASK       (0x03 << (31 - DMA_MR_SAHTS_BIT ))
                      
/* Status Registers bits */

#define DMA_SR_TE_BIT           24
#define DMA_SR_TE               (0x01 << (31 - DMA_SR_TE_BIT    ))   
#define DMA_SR_CH_BIT           26 
#define DMA_SR_CH               (0x01 << (31 - DMA_SR_CH_BIT    ))   
#define DMA_SR_PE_BIT           27
#define DMA_SR_PE               (0x01 << (31 - DMA_SR_PE_BIT    ))   
#define DMA_SR_EOLNI_BIT        28
#define DMA_SR_EOLNI            (0x01 << (31 - DMA_SR_EOLNI_BIT ))
#define DMA_SR_CB_BIT           29
#define DMA_SR_CB               (0x01 << (31 - DMA_SR_CB_BIT    ))   
#define DMA_SR_EOSI_BIT         30
#define DMA_SR_EOSI             (0x01 << (31 - DMA_SR_EOSI_BIT  )) 
#define DMA_SR_EOLSI_BIT        31
#define DMA_SR_EOLSI            (0x01 << (31 - DMA_SR_EOLSI_BIT ))
#define CLNDS_MASK              0xfffffff0
#define DMA_SR_MASK             0xffffffff

/* General Status Register bit filed */

#define	DGSR_TE_BIT             7
#define DGSR_TE                 (0x01 << DGSR_TE_BIT    )
#define DGSR_CH_BIT             5
#define DGSR_CH                 (0x01 << DGSR_CH_BIT    )
#define DGSR_PE_BIT             4
#define DGSR_PE                 (0x01 << DGSR_PE_BIT    )
#define DGSR_EOLNI_BIT          3
#define DGSR_EOLNI              (0x01 << DGSR_EOLNI_BIT )
#define DGSR_CB_BIT             2
#define DGSR_CB                 (0x01 << DGSR_CB_BIT    )
#define DGSR_EOSI_BIT           1
#define DGSR_EOSI               (0x01 << DGSR_EOSI_BIT  )
#define DGSR_EOLSI_BIT          0
#define DGSR_EOLSI              (0x01 << DGSR_EOLSI_BIT )
#define DGSR_MASK               0xff

/* Next Link Descriptor Address Registers bit filed */

#define NLNDAR_NLNDA_MASK       0xffffffe0
#define NLNDAR_NDEOSIE          (0x01 << 3)
#define NLNDAR_EOLND            (0x01 << 0)

/* Next List Descriptor Address Registers bit filed */

#define NLSDAR_NLSDA_MASK       0xffffffe0
#define NLSDAR_EOLSD            (0x01 << 0)

/* Channel Modes */

#define FSL_DMA_MODE_BC          0x0    /* basic chaining mode */
#define FSL_DMA_MODE_BD          0x1    /* basic direct mode */
#define FSL_DMA_MODE_EC          0x2    /* extended chaining mode */
#define FSL_DMA_MODE_ED          0x3    /* extended direct mode */

/* special constants */

#define DMA_LINK_DESC_NUM       0x10
#define FSL_DMA_DESC_ALIGN      0x20
#define FSL_DMA_DESC_SIZE       0x20
#define DMA_CHAN_NUM            0x04
#define DMA_TRANS_MAX_SIZE      (0x4000000 - 1)
#define FSL_DMA_READ_TYPE       0x00050000
#define FSL_DMA_WRITE_TYPE      0x00050000

#define DMA_MR_INIT_VALUE       (DMA_BW_256B | DMA_MR_EOLNIE | \
                                 DMA_MR_EIE) 

#define DMA_EVENTS_ERROR_BIT_OFFSET 8
#define DMA_EXC_TASK_PRI        60

#define DMA_BAR(p)      (((FSL_DMA_DRV_CTRL *)(p)->pDrvCtrl)->regBase)
#define DMA_HANDLE(p)   (((FSL_DMA_DRV_CTRL *)(p)->pDrvCtrl)->handle)

#define DMA_READ_32(pDev, addr)                     \
            vxbRead32(DMA_HANDLE(pDev),             \
                      (UINT32 *)((char *)DMA_BAR(pDev) + addr))
#define DMA_WRITE_32(pDev, addr, data)              \
            vxbWrite32(DMA_HANDLE(pDev),            \
                      (UINT32 *)((char *)DMA_BAR(pDev) + addr), data)

/* 
 * For eight-channel type DMA engine, the channels are made up two groups, four
 * channels in each group. In the same group, channel register bank offset is 0x80,
 * but between the two group, the offset is 0x180. The following macro is used to
 * fit for this characteristic.
 */

#define DMA_CHAN_GROUP_OFFSET(chan)                 \
           (chan / DMA_CHAN_NUM) * 0x100

#define DMA_CHAN_REG_READ(pDev, chan, reg)          \
            DMA_READ_32(pDev, (reg + (chan * DMA_CHAN_OFFSET) + DMA_CHAN_GROUP_OFFSET(chan)))
#define DMA_CHAN_REG_WRITE(pDev, chan, reg, value)  \
            DMA_WRITE_32(pDev, (reg + (chan * DMA_CHAN_OFFSET) + DMA_CHAN_GROUP_OFFSET(chan)), value)

#define CHAN_SYNC_TAKE semTake(pChan->mutex, WAIT_FOREVER)
#define CHAN_SYNC_GIVE (void)semGive(pChan->mutex)

#define LST_SYNC_TAKE semTake(pDrvCtrl->lstMutex, WAIT_FOREVER)
#define LST_SYNC_GIVE (void)semGive(pDrvCtrl->lstMutex)

/* type define */

typedef volatile unsigned int   VUINT32;

typedef struct fslDmaEcmData
    {
    void  *              pDescPool;
    pVXB_DMA_COMPLETE_FN callback;
    void  *              pArg;
    UINT32               cookie;
    } FSL_DMA_ECM_DATA;

typedef struct fslDmaChan
    {
    UINT32              chanId;
    SEM_ID              mutex;
    UINT32              csr;
    LIST                dList;
    VXB_DMA_RESOURCE_ID pRes;
    UINT32              mode;
    FSL_DMA_ECM_DATA    ecmData; /* Extended Chaining Mode Data */
    } FSL_DMA_CHAN;

typedef struct fslDmaLinkDesc
    {
    VUINT32             srcAttr;
    VUINT32             srcAddr;
    VUINT32             dstAttr;
    VUINT32             dstAddr;
    VUINT32             nextLinkDescExAddr;
    VUINT32             nextLinkDescAddr;
    VUINT32             byteCnt;
    VUINT32             reserved;
    } FSL_DMA_LINK_DESC;

typedef struct fslDmaListDesc
    {
    VUINT32             nextListDescExAddr;
    VUINT32             nextListDescAddr;
    VUINT32             firstLinkDescExAddr;
    VUINT32             firstLinkDescAddr;
    VUINT32             srcStride;
    VUINT32             desStride;
    VUINT32             reserved1;
    VUINT32             reserved2;
    } FSL_DMA_LIST_DESC;

typedef struct fslDmaNode
    {
    struct node *       next;
    struct node *       previous;
    FSL_DMA_LINK_DESC * descriptor;
    pVXB_DMA_COMPLETE_FN callback;
    UINT32              userData;
    UINT32              cookie;
    } FSL_DMA_NODE;

typedef struct flsDmaDevice
    {
    VXB_DEVICE_ID       vxbDev;
    VOID *              regBase;
    VOID *              handle;     /*type of base, mem or io*/
    LIST                freeLink;
    FSL_DMA_CHAN **     pClients;
    FSL_DMA_LINK_DESC * pDesc;
    UINT32              chanNum;
    SEM_ID              lstMutex;
    SEM_ID              fslDmaSemId;
    SEM_ID              fslDmaSyncSem;
    atomicVal_t         fslDmaEvents;
    BOOL                dmaInitialized;
    TASK_ID             fslDmaExcId;
    int                 fslDmaPhase;
    int                 numOfLinks;
    BOOL                polling;
    } FSL_DMA_DRV_CTRL;

/* externs */

IMPORT STATUS vxbFslDmaReadPhy40 (VXB_DMA_RESOURCE_ID, PHYS_ADDR, PHYS_ADDR,
                         int, int, UINT32, pVXB_DMA_COMPLETE_FN, void *);
IMPORT STATUS vxbFslDmaWritePhy40 (VXB_DMA_RESOURCE_ID, PHYS_ADDR, PHYS_ADDR,
                         int, int, UINT32, pVXB_DMA_COMPLETE_FN, void *);
IMPORT STATUS vxbFslDmaSyncReadPhy40 (VXB_DMA_RESOURCE_ID, PHYS_ADDR,
                         PHYS_ADDR, int *, int, UINT32);
IMPORT STATUS vxbFslDmaSyncWritePhy40  (VXB_DMA_RESOURCE_ID, PHYS_ADDR,
                         PHYS_ADDR, int *, int, UINT32);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif  /* __INCvxbFslDmah */

