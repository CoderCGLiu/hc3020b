/* vxbPl330Dma.h - header file for PL330 DMA VxBus driver */

/*
 * Copyright (c) 2012, 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01c,25jun13,swu  added be8 support 
01b,08may13,y_f  added memory to device transfers
01a,03sep12,fao  written.
*/

#ifndef __INCvxbPl330Dmah
#define __INCvxbPl330Dmah

#if __cplusplus
extern "C" {
#endif

/* defines */

#define PL330_DMA_DRV_NAME          "pl330dma"

/* pl330 DMA logic channel count */

#define PL330_DMA_CHAN_COUNT        8

/* DMA DCR Address Map */

#define PL330_DMA_DSR               (0x00000000)
#define PL330_DMA_DPC               (0x00000004)
#define PL330_DMA_INTEN             (0x00000020)
#define PL330_DMA_INT_EVENT_RIS     (0x00000024)
#define PL330_DMA_INTMIS            (0x00000028)
#define PL330_DMA_INTCLR            (0x0000002C)
#define PL330_DMA_FSRD              (0x00000030)
#define PL330_DMA_FSRC              (0x00000034)
#define PL330_DMA_FTRD              (0x00000038)
#define PL330_DMA_FTR0              (0x00000040)
#define PL330_DMA_FTR1              (0x00000044)
#define PL330_DMA_FTR2              (0x00000048)
#define PL330_DMA_FTR3              (0x0000004C)
#define PL330_DMA_FTR4              (0x00000050)
#define PL330_DMA_FTR5              (0x00000054)
#define PL330_DMA_FTR6              (0x00000058)
#define PL330_DMA_FTR7              (0x0000005C)
#define PL330_DMA_CSR0              (0x00000100)
#define PL330_DMA_CPC0              (0x00000104)
#define PL330_DMA_CSR1              (0x00000108)
#define PL330_DMA_CPC1              (0x0000010C)
#define PL330_DMA_CSR2              (0x00000110)
#define PL330_DMA_CPC2              (0x00000114)
#define PL330_DMA_CSR3              (0x00000118)
#define PL330_DMA_CPC3              (0x0000011C)
#define PL330_DMA_CSR4              (0x00000120)
#define PL330_DMA_CPC4              (0x00000124)
#define PL330_DMA_CSR5              (0x00000128)
#define PL330_DMA_CPC5              (0x0000012C)
#define PL330_DMA_CSR6              (0x00000130)
#define PL330_DMA_CPC6              (0x00000134)
#define PL330_DMA_CSR7              (0x00000138)
#define PL330_DMA_CPC7              (0x0000013C)
#define PL330_DMA_SAR0              (0x00000400)
#define PL330_DMA_DAR0              (0x00000404)
#define PL330_DMA_CCR0              (0x00000408)
#define PL330_DMA_LC0_0             (0x0000040C)
#define PL330_DMA_LC1_0             (0x00000410)
#define PL330_DMA_SAR1              (0x00000420)
#define PL330_DMA_DAR1              (0x00000424)
#define PL330_DMA_CCR1              (0x00000428)
#define PL330_DMA_LC0_1             (0x0000042C)
#define PL330_DMA_LC1_1             (0x00000430)
#define PL330_DMA_SAR2              (0x00000440)
#define PL330_DMA_DAR2              (0x00000444)
#define PL330_DMA_CCR2              (0x00000448)
#define PL330_DMA_LC0_2             (0x0000044C)
#define PL330_DMA_LC1_2             (0x00000450)
#define PL330_DMA_SAR3              (0x00000460)
#define PL330_DMA_DAR3              (0x00000464)
#define PL330_DMA_CCR3              (0x00000468)
#define PL330_DMA_LC0_3             (0x0000046C)
#define PL330_DMA_LC1_3             (0x00000470)
#define PL330_DMA_SAR4              (0x00000480)
#define PL330_DMA_DAR4              (0x00000484)
#define PL330_DMA_CCR4              (0x00000488)
#define PL330_DMA_LC0_4             (0x0000048C)
#define PL330_DMA_LC1_4             (0x00000490)
#define PL330_DMA_SAR5              (0x000004A0)
#define PL330_DMA_DAR5              (0x000004A4)
#define PL330_DMA_CCR5              (0x000004A8)
#define PL330_DMA_LC0_5             (0x000004AC)
#define PL330_DMA_LC1_5             (0x000004B0)
#define PL330_DMA_SAR6              (0x000004C0)
#define PL330_DMA_DAR6              (0x000004C4)
#define PL330_DMA_CCR6              (0x000004C8)
#define PL330_DMA_LC0_6             (0x000004CC)
#define PL330_DMA_LC1_6             (0x000004D0)
#define PL330_DMA_SAR7              (0x000004E0)
#define PL330_DMA_DAR7              (0x000004E4)
#define PL330_DMA_CCR7              (0x000004E8)
#define PL330_DMA_LC0_7             (0x000004EC)
#define PL330_DMA_LC1_7             (0x000004F0)
#define PL330_DMA_DBGSTATUS         (0x00000D00)
#define PL330_DMA_DBGCMD            (0x00000D04)
#define PL330_DMA_DBGINST0          (0x00000D08)
#define PL330_DMA_DBGINST1          (0x00000D0C)
#define PL330_DMA_CR0               (0x00000E00)
#define PL330_DMA_CR1               (0x00000E04)
#define PL330_DMA_CR2               (0x00000E08)
#define PL330_DMA_CR3               (0x00000E0C)
#define PL330_DMA_CR4               (0x00000E10)
#define PL330_DMA_CRD               (0x00000E14)
#define PL330_DMA_WD                (0x00000E80)
#define PL330_DMA_PERIPH_ID_0       (0x00000FE0)
#define PL330_DMA_PERIPH_ID_1       (0x00000FE4)
#define PL330_DMA_PERIPH_ID_2       (0x00000FE8)
#define PL330_DMA_PERIPH_ID_3       (0x00000FEC)
#define PL330_DMA_PCELL_ID_0        (0x00000FF0)
#define PL330_DMA_PCELL_ID_1        (0x00000FF4)
#define PL330_DMA_PCELL_ID_2        (0x00000FF8)
#define PL330_DMA_PCELL_ID_3        (0x00000FFC)

#define PL330_DMA_FTR(n)            (PL330_DMA_FTR0 + 0x04 * (n))
#define PL330_DMA_CSR(n)            (PL330_DMA_CSR0 + 0x08 * (n))
#define PL330_DMA_CPC(n)            (PL330_DMA_CPC0 + 0x08 * (n))
#define PL330_DMA_SAR(n)            (PL330_DMA_SAR0 + 0x20 * (n))
#define PL330_DMA_DAR(n)            (PL330_DMA_DAR0 + 0x20 * (n))
#define PL330_DMA_CCR(n)            (PL330_DMA_CCR0 + 0x20 * (n))
#define PL330_DMA_LC0(n)            (PL330_DMA_LC0_0 + 0x20 * (n))
#define PL330_DMA_LC1(n)            (PL330_DMA_LC1_0 + 0x20 * (n))

#define DMA_STATUS_MASK             0xF
#define DMA_STATUS_STOPED           0x0
#define DMA_STATUS_FAULTING_COMP    0xE
#define DMA_STATUS_FAULTING         0xF

#define CRD_DATA_BUS_WIDTH_32BIT    (0x02)
#define CRD_DATA_BUS_WIDTH_64BIT    (0x03)
#define CRD_DATA_BUS_WIDTH_128BIT   (0x04)

#define CCR_BURST_SIZE_1            (0x00)
#define CCR_BURST_SIZE_2            (0x01)
#define CCR_BURST_SIZE_4            (0x02)
#define CCR_BURST_SIZE_8            (0x03)
#define CCR_BURST_SIZE_16           (0x04)

#define CCR_SWAP_NO                 (0x00 << 28)
#define CCR_DST_CACHE_CTRL          (0x00 << 25)
#define CCR_SRC_CACHE_CTRL          (0x00 << 11)
#define CCR_DST_PROT_CTRL           (0x00 << 22)
#define CCR_SRC_PROT_CTRL           (0x00 << 8)
#define CCR_DST_BURST_LEN           (0x00 << 18)
#define CCR_DST_BURST_SIZE          (0x00 << 15)
#define CCR_DST_INC                 (0x01 << 14)
#define CCR_DST_FIX                 (0x01 << 14)
#define CCR_SRC_BURST_LEN           (0x00 << 4)
#define CCR_SRC_BURST_SIZE          (0x00 << 1)
#define CCR_SRC_INC                 (0x01 << 0)
#define CCR_SRC_FIX                 (0x00 << 0)

#define SAR                         0
#define CCR                         1
#define DAR                         2

#define MEM_TO_MEM                  0
#define MEM_TO_DEVICE               1
#define DEVICE_TO_MEM               2

#define DBGSTATUS_BUSY              (1)
#define PL330_MICROCODE_SIZE        0x500
#define PL330_TIMEOUT               50000
#define MAXLOOP                     (256 * 256)
#define DMA_ELEMENT_MAX             MAXLOOP

/* read & write low level access routines */

#define DMA_BAR(p)      ((PL330_DMA_DRV_CTRL *)(p)->pDrvCtrl)->regBase
#define DMA_HANDLE(p)   ((PL330_DMA_DRV_CTRL *)(p)->pDrvCtrl)->handle

#ifdef ARMBE8
#    define SWAP32 vxbSwap32
#else
#    define SWAP32 
#endif /* ARMBE8 */

#define DMA_READ_4(pDev, reg)                               \
        SWAP32(vxbRead32 (DMA_HANDLE (pDev),                \
                   (UINT32 *)((char *)DMA_BAR (pDev) + reg)))

#define DMA_WRITE_4(pDev, reg, data)                        \
        vxbWrite32 (DMA_HANDLE (pDev),                      \
                    (UINT32 *)((char *)DMA_BAR (pDev) + reg), SWAP32(data))

#define DMAGO(pBuf, ns, cn, imm)                            \
       *(UINT8 *)pBuf = 0xA0 | ((ns << 1) & 0x2);           \
       *(UINT8 *)(pBuf + 1) = (cn) & 0x7;                   \
       *((UINT32 *)(pBuf + 2)) = SWAP32(imm);                      
 
#define DMAMOV(pBuf, rd, imm)                               \
       *(UINT8 *)pBuf = 0xBC;                               \
       *(UINT8 *)(pBuf + 1) = (rd) & 0x7;                   \
       *(UINT32 *)(pBuf + 2) = SWAP32(imm);                 \
       pBuf += 6

#define DMALD(pBuf)  *(UINT8 *)pBuf++ = 0x04
#define DMAST(pBuf)  *(UINT8 *)pBuf++ = 0x08
#define DMANOP(pBuf) *(UINT8 *)pBuf++ = 0x18
#define DMARMB(pBuf) *(UINT8 *)pBuf++ = 0x12
#define DMAWMB(pBuf) *(UINT8 *)pBuf++ = 0x13

#define DMAWFP(pBuf, event)                                 \
       *(UINT8 *)pBuf = (UINT8)0x30;                        \
       *(UINT8 *)(pBuf + 1) = (UINT8)((event) << 3);        \
       pBuf += 2

#define DMASEV(pBuf, event)                                 \
       *(UINT8 *)pBuf = (UINT8)0x34;                        \
       *(UINT8 *)(pBuf + 1) = (UINT8)((event) << 3);        \
       pBuf += 2

#define DMALP(pBuf, lc, iter)                               \
       *(UINT8 *)pBuf = (UINT8)(0x20 | (((lc) << 1) & 0x2)); \
       *(UINT8 *)(pBuf + 1) = (UINT8)((iter)-1);            \
       pBuf += 2

#define DMALPEND(pBuf, lc, bj)                              \
       *(UINT8 *)pBuf = (UINT8)(0x38 | (((lc) << 2) & 0x4)); \
       *(UINT8 *)(pBuf + 1) = (UINT8)((UINT32)pBuf - (UINT32)bj); \
       pBuf += 2

#define DMAEND(pBuf)  *(UINT8 *)pBuf++ = 0x0

/* typedefs */

/* DMA channel specific register params */

typedef struct pl330DmaChanParam
    {
    UINT32  endianSwapSize; /* endian swap size*/
    UINT32  dstAddr;        /* destination start address */
    UINT32  dstBurstLen;    /* destination burst len */
    UINT32  dstBurstSize;   /* destination burst size */
    UINT32  dstCacheCtrl;   /* destination cache control */
    UINT32  dstProtoCtrl;   /* destination proto control */
    UINT32  dstInc;         /* destination inc mode */
    UINT32  srcAddr;        /* source start address */
    UINT32  srcBurstLen;    /* source burst len */     
    UINT32  srcBurstSize;   /* source burst size */    
    UINT32  srcCacheCtrl;   /* source cache control */ 
    UINT32  srcProtoCtrl;   /* source proto control */ 
    UINT32  srcInc;         /* source inc mode */
    UINT32  transferSize;   /* transfer size */
    UINT32  tansferType;    /* tansfer type (mem<-->mem or mem<-->device) */
    UINT32  peripheral;     /* peripheral number */
    } PL330_DMA_CHAN_PARAM;

/* DMA channel data */

typedef struct pl330DmaChan
    {
    UINT32                  chanNum;                    /* DMA channel number */
    UINT32                  enabledIrqs;                /* enabled irqs */
    void                    (*callback)(void * pArg);   /* interrupt handler */
    void *                  pArg;                       /* ISR parameter */
    PL330_DMA_CHAN_PARAM *  pChanParam;                 /* channel params */
    UINT8 *                 pMicroCodeBuf;              /* dma micro code */
    } PL330_DMA_CHAN;

/* DMA resource data */

typedef struct pl330DmaRes
    {
    BOOL                used;   /* DMA channel used or not */
    PL330_DMA_CHAN *    pChan;  /* DMA channel data */
    } PL330_DMA_RES;

/* DMA driver controllor */

typedef struct pl330DmaDrvCtrl
    {
    VXB_DEVICE_ID   pDev;
    void *          regBase;
    void *          handle;
    SEM_ID          chanMutex;
    UINT32          maxChanNum;
    UINT32          maxPeriphReq;
    UINT32          dataBusWidth;
    PL330_DMA_RES   chanRes[PL330_DMA_CHAN_COUNT];
    } PL330_DMA_DRV_CTRL;

#ifdef __cplusplus
}
#endif /* __cplusplus  */

#endif /* __INCvxbPl330Dmah */
