/* vxbOmap35xxDma.h - TI Omap35xx DMA controller driver */

/*
 * Copyright (c) 2010 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,22jul10,hcl  written.
*/

#ifndef __INCvxbOmap35xxDmah
#define __INCvxbOmap35xxDmah

#ifdef __cplusplus
extern "C" {
#endif

/* driver name */

#define DMA_DRV_NAME                       "omap35xxDma"

/* omap35xx DMA logic channel count */

#define OMAP_DMA_CHAN_COUNT                32

/* omap35xx global DMA registers */

#define OMAP_DMA_IRQSTATUS_L0               0x08
#define OMAP_DMA_IRQSTATUS_L1               0x0c
#define OMAP_DMA_IRQSTATUS_L2               0x10
#define OMAP_DMA_IRQSTATUS_L3               0x14
#define OMAP_DMA_IRQENABLE_L0               0x18
#define OMAP_DMA_IRQENABLE_L1               0x1c
#define OMAP_DMA_IRQENABLE_L2               0x20
#define OMAP_DMA_IRQENABLE_L3               0x24
#define OMAP_DMA_SYSSTATUS                  0x28
#define OMAP_DMA_OCP_SYSCONFIG              0x2c
#define OMAP_DMA_CAPS_0                     0x64
#define OMAP_DMA_CAPS_2                     0x6c
#define OMAP_DMA_CAPS_3                     0x70
#define OMAP_DMA_CAPS_4                     0x74
#define OMAP_DMA_GCR                        0x78

/* omap35xx DMA channel specific registers */

#define OMAP_DMA_CH_BASE(n)                 (0x80 + 0x60 * (n))
#define OMAP_DMA_CCR(n)                     (0x80 + 0x60 * (n))
#define OMAP_DMA_CLNK_CTRL(n)               (0x84 + 0x60 * (n))
#define OMAP_DMA_CICR(n)                    (0x88 + 0x60 * (n))
#define OMAP_DMA_CSR(n)                     (0x8c + 0x60 * (n))
#define OMAP_DMA_CSDP(n)                    (0x90 + 0x60 * (n))
#define OMAP_DMA_CEN(n)                     (0x94 + 0x60 * (n))
#define OMAP_DMA_CFN(n)                     (0x98 + 0x60 * (n))
#define OMAP_DMA_CSSA(n)                    (0x9c + 0x60 * (n))
#define OMAP_DMA_CDSA(n)                    (0xa0 + 0x60 * (n))
#define OMAP_DMA_CSEI(n)                    (0xa4 + 0x60 * (n))
#define OMAP_DMA_CSFI(n)                    (0xa8 + 0x60 * (n))
#define OMAP_DMA_CDEI(n)                    (0xac + 0x60 * (n))
#define OMAP_DMA_CDFI(n)                    (0xb0 + 0x60 * (n))
#define OMAP_DMA_CSAC(n)                    (0xb4 + 0x60 * (n))
#define OMAP_DMA_CDAC(n)                    (0xb8 + 0x60 * (n))
#define OMAP_DMA_CCEN(n)                    (0xbc + 0x60 * (n))
#define OMAP_DMA_CCFN(n)                    (0xc0 + 0x60 * (n))
#define OMAP_DMA_COLOR(n)                   (0xc4 + 0x60 * (n))

/* Channel Status Register bits */

#define OMAP_DMA_DROP_IRQ                   (1 << 1)
#define OMAP_DMA_HALF_IRQ                   (1 << 2)
#define OMAP_DMA_FRAME_IRQ                  (1 << 3)
#define OMAP_DMA_LAST_IRQ                   (1 << 4)
#define OMAP_DMA_BLOCK_IRQ                  (1 << 5)
#define OMAP_DMA_SYNC_IRQ                   (1 << 6)
#define OMAP_DMA_PKT_IRQ                    (1 << 7)
#define OMAP_DMA_TRANS_ERR_IRQ              (1 << 8)
#define OMAP_DMA_SECURE_ERR_IRQ             (1 << 9)
#define OMAP_DMA_SUPERVISOR_ERR_IRQ         (1 << 10)
#define OMAP_DMA_MISALIGNED_ERR_IRQ         (1 << 11)

/* hardware synchronization mode */

#define OMAP_DMA_SRC_SYNC                   0x01
#define OMAP_DMA_DST_SYNC                   0x00

/* the default setting of GCR register */

#define DMA_DEFAULT_FIFO_DEPTH              0x80
#define DMA_DEFAULT_ARB_RATE                0x01

/* DMA logic channel priority */

#define DMA_CH_PRIO_HIGH                    0x1
#define DMA_CH_PRIO_LOW                     0x0

/* bits defination */

#define BITS_1_MASK                         0x1
#define BITS_2_MASK                         0x3
#define BITS_3_MASK                         0x7
#define BITS_4_MASK                         0xf
#define BITS_8_MASK                         0xff

/* DMA data burst mode */

enum omap35xxDmaBurstMode
    {
    OMAP_DMA_DATA_BURST_DIS = 0,
    OMAP_DMA_DATA_BURST_4,
    OMAP_DMA_DATA_BURST_8,
    OMAP_DMA_DATA_BURST_16
    };

/* DMA data write mode */

enum omap35xxDmaWriteMode
    {
    OMAP_DMA_WRITE_NON_POSTED = 0,
    OMAP_DMA_WRITE_POSTED,
    OMAP_DMA_WRITE_LAST_NON_POSTED
    };

/* the type of the data moved in the channel */

#define OMAP_DMA_DATA_TYPE_S8               0x00
#define OMAP_DMA_DATA_TYPE_S16              0x01
#define OMAP_DMA_DATA_TYPE_S32              0x02

/* DMA data synchronization type */

#define OMAP_DMA_SYNC_ELEMENT               0x00
#define OMAP_DMA_SYNC_FRAME                 0x01
#define OMAP_DMA_SYNC_BLOCK                 0x02
#define OMAP_DMA_SYNC_PACKET                0x03
#define OMAP_DMA_SYNC_MASK                  0x03

/* DMA source or destination synchronization */

#define OMAP_DMA_SRC_SYNC                   0x01
#define OMAP_DMA_DST_SYNC                   0x00

/* DMA transfer flags */

#define OMAP_DMA_SYNC_DEV_OFFSET            0
#define OMAP_DMA_SYNC_OFFSET                8
#define OMAP_DMA_AMODE_SRC_OFFSET           10
#define OMAP_DMA_AMODE_DEST_OFFSET          12
#define OMAP_DMA_ISREAD_OFFSET              14
#define OMAP_DMA_FRAME_NUM_OFFSET           16

#define DMA_DTA_TYPE_BYTES_1                1
#define DMA_DTA_TYPE_BYTES_2                2
#define DMA_DTA_TYPE_BYTES_4                4

/* the DMA address modes */

#define OMAP_DMA_AMODE_CONSTANT             0x00
#define OMAP_DMA_AMODE_POST_INC             0x01
#define OMAP_DMA_AMODE_SINGLE_IDX           0x02
#define OMAP_DMA_AMODE_DOUBLE_IDX           0x03

/* the default DMA arbitration switching rate */

#define OMAP_DMA_ARB_RATE_SHIFT             16

/* each DMA channel registers number */

#define OMAP_DMA_CHAN_REGS_NUM              0x44

#define OMAP_DMA_CCR_EN                     (0x1 << 7)
#define OMAP_DMA_CSR_CLEAR_MASK             0xffe

/* CSDP register bits */

#define CSDP_DATA_TYPE_MASK                 0x3

/* CCR register bits */

#define CCR_SYNCHRO_LOW_MASK                0x1f
#define CCR_SYNCHRO_UPPER_MASK              (0x3 << 19)
#define CCR_SYNCHRO_UPPER_OFFSET            14
#define CCR_SYNC_FRAME_MASK                 (0x1 << 5)
#define CCR_SYNC_BS_MASK                    (0x1 << 18)
#define CCR_SRC_AMODE_SHIFT                 12
#define CCR_SRC_AMODE_MASK                  (0x3 << 12)
#define CCR_DST_AMODE_SHIFT                 14
#define CCR_DST_AMODE_MASK                  (0x3 << 14)
#define CCR_SRC_DST_SYNC_MASK               (0x1 << 24)

/* DMA channel specific register params */

typedef struct omap35xxDmaChanParam
    {
    UINT32  srcAddr;        /* source start address */
    UINT32  srcAddrMode;    /* source address mode */
    INT32   srcElemIndex;   /* source element index */
    INT32   srcFrameIndex;  /* source frame index */
    UINT32  destAddr;       /* destination start address */
    UINT32  destAddrMode;   /* destination address mode */
    INT32   destElemIndex;  /* destination element index */
    INT32   destFrameIndex; /* destination Frame index */
    UINT32  dataType;       /* transfer data type */
    UINT32  frameCount;     /* number of frame pertransfer block */
    UINT32  elemCount;      /* number of element per frame */
    UINT32  syncMode;       /* synchronization transfer mode */
    UINT32  syncDev;        /* synchronization device */
    UINT32  isRead;         /* True for read and FALSE for write */
    UINT32  writeMode;      /* write mode */
    UINT32  readPri;        /* read priority */
    UINT32  writePri;       /* write priority */
    UINT32  srcBurstMode;   /* source burst mode */
    UINT32  destBurstMode;  /* destination burst mode */
    } OMAP35XX_DMA_CHAN_PARAM;

/* DMA channel data */

typedef struct omap35xxDmaChan
    {
    UINT32                      chanNum;            /* DMA channel number */
    UINT32                      enabledIrqs;        /* enabled irqs */
    void                        (*callback)(void * pArg);/* interrupt handler */
    void *                      pArg;               /* ISR parameter */
    OMAP35XX_DMA_CHAN_PARAM *   pChanParam;         /* channel params */
    } OMAP35XX_DMA_CHAN;

/* DMA resource data */

typedef struct omap35xxDmaRes
    {
    BOOL                used;   /* DMA channel used or not */
    OMAP35XX_DMA_CHAN * pChan;  /* DMA channel data */
    } OMAP35XX_DMA_RES;

/* DMA driver controllor */

typedef struct omap35xxDmaDrvCtrl
    {
    VXB_DEVICE_ID       pDev;
    void *              regBase;
    void *              handle;
    SEM_ID              chanMutex;
    OMAP35XX_DMA_RES    chanRes[OMAP_DMA_CHAN_COUNT];
    } OMAP35XX_DMA_DRV_CTRL;

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCvxbOmap35xxDmah */
