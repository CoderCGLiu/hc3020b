/* vxbTiEdma3.h - TI EDMA3 header file */

/*
 * Copyright (c) 2011-2012, 2015 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
29jan15,m_w updated to support AM437x, which only has 3 queues. (US51994)
05dec12,c_t updated to support different increase mode.
25may12,my_ fix fifo width definition
22jul11,my_ written 
*/

#ifndef __INCvxbTiEdma3h
#define __INCvxbTiEdma3h

#ifdef __cplusplus
extern "C" {
#endif

#define DMA_DRV_NAME                       "tiedma3"

#ifndef BIT
#   define BIT(n)                          (1 << (n))
#endif

#define TI_EDMA3_SH_ER                     0x00
#define TI_EDMA3_SH_ERH                    0x04
#define TI_EDMA3_SH_ECR                    0x08
#define TI_EDMA3_SH_ECRH                   0x0C
#define TI_EDMA3_SH_ESR                    0x10
#define TI_EDMA3_SH_ESRH                   0x14
#define TI_EDMA3_SH_CER                    0x18
#define TI_EDMA3_SH_CERH                   0x1C
#define TI_EDMA3_SH_EER                    0x20
#define TI_EDMA3_SH_EERH                   0x24
#define TI_EDMA3_SH_EECR                   0x28
#define TI_EDMA3_SH_EECRH                  0x2C
#define TI_EDMA3_SH_EESR                   0x30
#define TI_EDMA3_SH_EESRH                  0x34
#define TI_EDMA3_SH_SER                    0x38
#define TI_EDMA3_SH_SERH                   0x3C
#define TI_EDMA3_SH_SECR                   0x40
#define TI_EDMA3_SH_SECRH                  0x44
#define TI_EDMA3_SH_IER                    0x50
#define TI_EDMA3_SH_IERH                   0x54
#define TI_EDMA3_SH_IECR                   0x58
#define TI_EDMA3_SH_IECRH                  0x5C
#define TI_EDMA3_SH_IESR                   0x60
#define TI_EDMA3_SH_IESRH                  0x64
#define TI_EDMA3_SH_IPR                    0x68
#define TI_EDMA3_SH_IPRH                   0x6C
#define TI_EDMA3_SH_ICR                    0x70
#define TI_EDMA3_SH_ICRH                   0x74
#define TI_EDMA3_SH_IEVAL                  0x78
#define TI_EDMA3_SH_QER                    0x80
#define TI_EDMA3_SH_QEER                   0x84
#define TI_EDMA3_SH_QEECR                  0x88
#define TI_EDMA3_SH_QEESR                  0x8c
#define TI_EDMA3_SH_QSER                   0x90
#define TI_EDMA3_SH_QSECR                  0x94
#define TI_EDMA3_SH_SIZE                   0x200

/* Offsets for EDMA CC global registers */

#define TI_EDMA3_REV                       0x0000
#define TI_EDMA3_CCCFG                     0x0004
#define TI_EDMA3_DCHMAP(ch)                (0x0100 + 4 * (ch))
#define TI_EDMA3_QCHMAP(ch)                (0x0200 + 4 * (ch))
#define TI_EDMA3_DMAQNUM(ch)               (0x0240 + 4 * (ch))
#define TI_EDMA3_QDMAQNUM                  0x0260
#define TI_EDMA3_QUETCMAP                  0x0280
#define TI_EDMA3_QUEPRI                    0x0284
#define TI_EDMA3_EMR                       0x0300
#define TI_EDMA3_EMCR                      0x0308
#define TI_EDMA3_EMCRH                     0x030C
#define TI_EDMA3_QEMR                      0x0310
#define TI_EDMA3_QEMCR                     0x0314
#define TI_EDMA3_CCERR                     0x0318
#define TI_EDMA3_CCERRCLR                  0x031c
#define TI_EDMA3_EEVAL                     0x0320
#define TI_EDMA3_DRAE0                     0x0340
#define TI_EDMA3_DRAEH0                    0x0344
#define TI_EDMA3_QRAE                      0x0380
#define TI_EDMA3_QUEEVTENTRY               0x0400
#define TI_EDMA3_QSTAT                     0x0600
#define TI_EDMA3_QWMTHRA                   0x0620
#define TI_EDMA3_QWMTHRB                   0x0624
#define TI_EDMA3_CCSTAT                    0x0640
#define TI_EDMA3_MPFAR                     0x0800
#define TI_EDMA3_MPFSR                     0x0804
#define TI_EDMA3_MPFCR                     0x0808
#define TI_EDMA3_MPFAG                     0x080c
#define TI_EDMA3_GLOBAL                    0x1000
#define TI_EDMA3_SHADOW0                   0x2000
#define TI_EDMA3_PARM                      0x4000

#define TI_EDMA3_LINK_OFFSET(param_no)     (((param_no) & 0x3f) << 5)
#define TI_EDMA3_PARM_OFFSET(param_no)     (TI_EDMA3_PARM + (((param_no) & 0x3f) << 5))
#define TI_EDMA3_OFFSET_TO_PARAMNO(offset) ((offset - TI_EDMA3_PARM) >> 5)
#define TI_EDMA3_CHMAP_EXIST               BIT(24)
#define TI_EDMA3_USER_CONTROL              BIT(31)

#define TI_EDMA3_OPT_SAM_CA                0x1
#define TI_EDMA3_OPT_DAM_CA                0x2
#define TI_EDMA3_SYNCDIM_A                 0x0
#define TI_EDMA3_SYNCDIM_AB                0x4
#define TI_EDMA3_STATIC                    0x8
#define TI_EDMA3_FIFO_WIDTH_8              (0 << 8)
#define TI_EDMA3_FIFO_WIDTH_16             (1 << 8) 
#define TI_EDMA3_FIFO_WIDTH_32             (2 << 8)
#define TI_EDMA3_FIFO_WIDTH_64             (3 << 8)
#define TI_EDMA3_FIFO_WIDTH_128            (4 << 8)
#define TI_EDMA3_FIFO_WIDTH_256            (5 << 8)
#define TI_EDMA3_TCCMODE_NORMAL            0x0
#define TI_EDMA3_TCCMODE_EARLY             (0x1 << 11)
#define TI_EDMA3_TCINTEN                   (0x1 << 20)
#define TI_EDMA3_ITCINTEN                  (0x1 << 21)
#define TI_EDMA3_TCCHEN                    (0x1 << 22)
#define TI_EDMA3_ITCCHEN                   (0x1 << 23)
#define TI_EDMA3_PRIV_USER                 (0x0)
#define TI_EDMA3_PRIV_SUP                  (0x1 << 31)
#define TI_EDMA3_TCC_SHIFT                 (12)
#define TI_EDMA3_LINK_END                  (0xffff)

/* DMA increase flags */

#define TI_EDMA3_SRC_MSK                   (0x0F) /* bit 0~3 */
#define TI_EDMA3_DST_MSK                   (0xF0) /* bit 4~7 */
#define TI_EDMA3_SRC_FIXED                 (1)
#define TI_EDMA3_DST_FIXED                 (0x10)
#define TI_EDMA3_SRC_DECREASE              (2)
#define TI_EDMA3_DST_DECREASE              (0x20)
#define TI_EDMA3_SRC_INCREASE              (0)
#define TI_EDMA3_DST_INCREASE              (0)

/* register low level access routines */

#define TI_EDMA3_BAR(p) \
        ((TI_EDMA3_DRV_CTRL *)(p)->pDrvCtrl)->regBase
#define TI_EDMA3_HANDLE(p) \
        ((TI_EDMA3_DRV_CTRL *)(p)->pDrvCtrl)->handle
    
#define EDMA3_MAX_PARAMS                   512
#define TI_EDMA3_PARAM_PAD_SIZE            0x8
#define EDMA3_MAX_CHANNELS                 64
#define EDMA3_MAX_DMA_QUEUES               4
#define DMA_CHAN_FREE                      0x2
#define DMA_CHAN_USED                      0x1
#define TI_EDMA3_NR_SHADOW_REGIONS         8

#define TI_EDMA3_GET_MAP_VAL(x) \
        ((x.mapping[0]) | (x.mapping[1] << 4) | (x.mapping[2] << 8) | \
        (x.mapping[3] << 12) | (x.mapping[4] << 16) | (x.mapping[5] << 20) | \
        (x.mapping[6] << 24) | (x.mapping[7] << 28))

/* typedefs */

typedef struct tiEdma3Param
{
    UINT32 opt;
    UINT32 src;
    UINT16 acnt;
    UINT16 bcnt;
    UINT32 dst;
    INT16  srcbidx;
    INT16  dstbidx;
    UINT16 link;
    UINT16 bcntrld;
    INT16  srccidx;
    INT16  dstcidx;
    UINT16 ccnt;
    UINT16 pad;
    INT32  reserved;
}TI_EDMA3_PARAM;

typedef struct tiEdma3ChanAllocInfo
{
    int                 requestedChan;
    BOOL                autoTrigger;
    BOOL                usrProvideParam;
    void              * data;
    UINT32              incrFlag;
}TI_EDMA3_CHAN_ALLOC_INFO;

typedef struct tiEdma3Channel
{ 
    /* associated param set */
    
    TI_EDMA3_PARAM      * param;

    /* channel flags */
    
    TI_EDMA3_CHAN_ALLOC_INFO * pInfo;
    UINT32                num;
    UINT32                queueDepth;

    /* sync sem and user provided callback */
    
    SEM_ID                sem;
    pVXB_DMA_COMPLETE_FN  callback;
    void                * arg;
}TI_EDMA3_CHAN;

/*
 * there are 4 transmit queues in TI EDMA3
 * there are 64 channels in the controller,
 * each channel can use any transmit queue
 */

typedef struct tiEdma3ChanMap
{
    UINT8               mapping[8];
}TI_EDMA3_CHAN_MAP;

typedef struct tiEdma3DrvCtrl
{
    VXB_DEVICE_ID       pDev;
    void              * regBase;
    void              * handle;
    spinlockIsr_t       lock;

    /* user provided infomation */
    
    UINT8             * queuePri;
    TI_EDMA3_CHAN_MAP * chanMapping;

    TI_EDMA3_CHAN       channels[EDMA3_MAX_CHANNELS];
    atomic_t            chanStatus[EDMA3_MAX_CHANNELS];
    atomic_t          * paramStatus;
    UINT32              maxParamNum;
    UINT32              maxQueueNum;
}TI_EDMA3_DRV_CTRL;

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* __INCvxbTiEdma3h */
