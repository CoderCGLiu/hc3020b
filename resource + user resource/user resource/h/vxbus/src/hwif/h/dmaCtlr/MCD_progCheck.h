/* MCD_progCheck.h - defines constants used for progress checking */

/*
Copyright (c) 2005-2006 Wind River Systems, Inc.

Copyright 1995-2004 Freescale Semiconductor, Inc.

This software is licensed pursuant to the terms of the
Freescale Semiconductor software license agreement for
M54x5EVB dBUG source code.
*/

/*
modification history
--------------------
01b,01feb06,j_b  add copyright for Freescale originated source; formatting
01a,20oct05,lsg  Written based on Freescale M54x5EVB dBUG source.
*/

/*
This file includes definitions for the multi-channel DMA
progress checking.
*/

#ifndef __INCmcd_progChecksh
#define __INCmcd_progChecksh

#ifdef __cplusplus
extern "C" {
#endif

#define CURRBD 4
#define DCOUNT 6
#define DESTPTR 5
#define SRCPTR 7

#ifdef __cplusplus
}
#endif

#endif /* __INCmcd_progChecksh */
