/* vxbAt91I2c.h - Atmel TWI hardware defintions */

/*
 * Copyright (c) 2012 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,12apr12,e_d  written based on the vxbFslI2c.c.
*/

#ifndef __INCvxbAt91I2ch
#define __INCvxbAt91I2ch

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* defines */

#define AT91_I2C_NAME              "at91I2c"
#define AT91I2C_POLLING_TIMEOUT    0x5000
#define AT91I2C_TIMEOUT            10
#define AT91SRST_DELAY             1000
#define BUS_IDLE                   0x1
#define TRANS_DONE                 0x2
#define AT91I2CCLKFEQ_DEF          132096000
#define AT91I2CBUSCLK_MAX          400000

/* Register Addresses */

#define AT91I2C_CR           0x00 /* (AT91I2C) Control Register               */
#define AT91I2C_MMR          0x04 /* (AT91I2C) Master Mode Register           */
#define AT91I2C_SMR          0x08 /* (AT91I2C) Slave Mode Register            */
#define AT91I2C_IADR         0x0C /* (AT91I2C) Internal Address Register      */
#define AT91I2C_CWGR         0x10 /* (AT91I2C) Clock Waveform Generator Register */
#define AT91I2C_SR           0x20 /* (AT91I2C) Status Register                */
#define AT91I2C_IER          0x24 /* (AT91I2C) Interrupt Enable Register      */
#define AT91I2C_IDR          0x28 /* (AT91I2C) Interrupt Disable Register     */
#define AT91I2C_IMR          0x2C /* (AT91I2C) Interrupt Mask Register        */
#define AT91I2C_RHR          0x30 /* (AT91I2C) Receive Holding Register       */
#define AT91I2C_THR          0x34 /* (AT91I2C) Transmit Holding Register      */

/* Twi CR define */

#define AT91I2C_CR_START     (0x1 <<  0) /* Send a START Condition            */
#define AT91I2C_CR_STOP      (0x1 <<  1) /* Send a STOP Condition             */
#define AT91I2C_CR_MSEN      (0x1 <<  2) /* AT91I2C Master Transfer Enabled   */
#define AT91I2C_CR_MSDIS     (0x1 <<  3) /* AT91I2C Master Transfer Disabled  */
#define AT91I2C_CR_SVEN      (0x1 <<  4) /* AT91I2C Slave mode Enabled        */
#define AT91I2C_CR_SVDIS     (0x1 <<  5) /* AT91I2C Slave mode Disabled       */
#define AT91I2C_CR_SWRST     (0x1 <<  7) /* Software Reset                    */

/* AT91I2C MMR define */

#define AT91I2C_MMR_IADRSZ        (0x3 <<  8) /* Internal Device Address Size */
#define AT91I2C_MMR_IADRSZ_NO     (0x0 <<  8) /* No internal device address */
#define AT91I2C_MMR_IADRSZ_1_BYTE (0x1 <<  8) /* One-byte internal device address */
#define AT91I2C_MMR_IADRSZ_2_BYTE (0x2 <<  8) /* Two-byte internal device address */
#define AT91I2C_MMR_IADRSZ_3_BYTE (0x3 <<  8) /* Three-byte internal device address */
#define AT91I2C_MMR_MREAD         (0x1 << 12)         /* Master Read Direction */
#define AT91I2C_MMR_DADR(x)       ((x & 0x7F) << 16)  /* Device Address */

/* AT91I2C SMR define */

#define AT91I2C_SMR_SADR          (0x7F << 16) /* Slave Address */

/* AT91I2C CWGR define */

#define AT91I2C_CWGR_CLDIV        (0xFF <<  0) /* Clock Low Divider */
#define AT91I2C_CWGR_CHDIV        (0xFF <<  8) /* Clock High Divider */
#define AT91I2C_CWGR_CKDIV        (0x7 << 16)  /*  Clock Divider */

/* AT91I2C SR define */

#define AT91I2C_SR_TXCOMP_SLAVE   (0x1 <<  0)  /* Transmission Completed */
#define AT91I2C_SR_TXCOMP_MASTER  (0x1 <<  0)  /* Transmission Completed */
#define AT91I2C_SR_RXRDY          (0x1 <<  1)  /* Receive holding register Ready */
#define AT91I2C_SR_TXRDY_MASTER   (0x1 <<  2)  /* Transmit holding register Ready */
#define AT91I2C_SR_TXRDY_SLAVE    (0x1 <<  2)  /* Transmit holding register Ready */
#define AT91I2C_SR_SVREAD         (0x1 <<  3)  /* Slave read */
#define AT91I2C_SR_SVACC          (0x1 <<  4)  /* Slave ACCess */
#define AT91I2C_SR_GACC           (0x1 <<  5)  /* General Call ACcess */
#define AT91I2C_SR_OVRE           (0x1 <<  6)  /* Overrun Error */
#define AT91I2C_SR_NACK           (0x1 <<  8)  /* Not Acknowledged */
#define AT91I2C_SR_ARBLST_MULTI_MASTER (0x1 <<  9) /* Arbitration Lost */
#define AT91I2C_SR_SCLWS          (0x1 << 10)  /* Clock Wait State */
#define AT91I2C_SR_EOSACC         (0x1 << 11)  /* End Of Slave ACCess */
#define AT91I2C_SR_ENDRX          (0x1 << 12)  /* End of Rx */
#define AT91I2C_SR_ENDTX          (0x1 << 13)  /* End of Tx */
#define AT91I2C_SR_RXBUFF         (0x1 << 14)  /* RXBUFF Interrupt */
#define AT91I2C_SR_TXBUFE         (0x1 << 15)  /* (AT91I2C) TXBUFE Interrupt */

/* AT91I2C IER define */

#define AT91I2C_IER_TXCOMP_SLAVE         AT91I2C_SR_TXCOMP_SLAVE  /* Transmission Completed */
#define AT91I2C_IER_TXCOMP_MASTER        AT91I2C_SR_TXCOMP_MASTER /* Transmission Completed */
#define AT91I2C_IER_RXRDY                AT91I2C_SR_RXRDY         /* Receive holding register Ready */
#define AT91I2C_IER_TXRDY_MASTER         AT91I2C_SR_TXRDY_MASTER  /* Transmit holding register Ready */
#define AT91I2C_IER_TXRDY_SLAVE          AT91I2C_SR_TXRDY_SLAVE   /* Transmit holding register Ready */
#define AT91I2C_IER_SVREAD               AT91I2C_SR_SVREAD        /* Slave read */
#define AT91I2C_IER_SVACC                AT91I2C_SR_SVACC         /* Slave ACCess */
#define AT91I2C_IER_GACC                 AT91I2C_SR_GACC          /* General Call ACcess */
#define AT91I2C_IER_OVRE                 AT91I2C_SR_OVRE          /* Overrun Error */
#define AT91I2C_IER_NACK                 AT91I2C_SR_NACK       /* Not Acknowledged */
#define AT91I2C_IER_ARBLST_MULTI_MASTER  AT91I2C_SR_ARBLST_MULTI_MASTER /* Arbitration Lost */
#define AT91I2C_IER_SCLWS                AT91I2C_SR_SCLWS         /* Clock Wait State */
#define AT91I2C_IER_EOSACC               AT91I2C_SR_EOSACC        /* End Of Slave ACCess */
#define AT91I2C_IER_ENDRX                AT91I2C_SR_ENDRX         /* End of Rx */
#define AT91I2C_IER_ENDTX                AT91I2C_SR_ENDTX         /* End of Tx */
#define AT91I2C_IER_RXBUFF               AT91I2C_SR_RXBUFF        /* RXBUFF Interrupt */
#define AT91I2C_IER_TXBUFE               AT91I2C_SR_TXBUFE        /* (AT91I2C) TXBUFE Interrupt */

/* structure holding the instance specific details */

typedef struct at91i2c_drv_ctrl
    {
    VXB_DEVICE_ID       i2cDev;
    void *              i2cHandle;
    SEM_ID              i2cDevSem;
    SEM_ID              i2cDataDone;
    UINT32              clkFrequency;
    UINT32              busSpeed;
    BOOL                polling;
    int                 actualSpeed;
    int                 i2cInitPhase;
    } AT91I2C_DRV_CTRL;

/* typedefs */

typedef enum
    {
    I2C_READ_RANDOM = 0x01,
    I2C_READ_SEQUENTIAL = 0x02,
    I2C_WRITE_BYTE = 0x04,
    I2C_WRITE_PAGE = 0x08
    } I2C_MODE_TYPE;

/* I2C controllor read and write interface */

#define AT91_I2C_BAR(p)       ((char *)((p)->pRegBase[0]))
#define AT91_I2C_HANDLE(p)    (((AT91I2C_DRV_CTRL *)(p)->pDrvCtrl)->i2cHandle)

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (AT91_I2C_HANDLE(pDev), (UINT32 *)((char *)AT91_I2C_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)               \
    vxbWrite32 (AT91_I2C_HANDLE(pDev),                \
        (UINT32 *)((char *)AT91_I2C_BAR(pDev) + addr), data)

#define CSR_CLRBIT_4(pDev, offset, val)             \
    CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & (val))

#define CSR_SETBIT_4(pDev, offset, val)             \
    CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_SETCLRBIT_4(pDev, offset, val1, val2)          \
        CSR_WRITE_4(pDev, offset, (CSR_READ_4(pDev, offset) & val1) | val2)

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCvxbAt91I2ch */

