/* vxbI2cPcfRtc.h - header file for I2C RTC driver */

/*
 * Copyright (c) 2012 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01c,14oct13,xms  modify macro define. (WIND00438418)
01b,24jun12,fao  cleaned up
01a,13Jun12,fao  written
*/

#ifndef __INCvxI2cPcfRtch
#define __INCvxI2cPcfRtch

#ifdef __cplusplus
extern "C" {
#endif

#ifndef BSP_VERSION

#define PCF_RTC_NAME            "rtc"

/* RTC register addresses */
/*  7 distinct registers */

#define RTC_CTR1_REG            0x0
#define RTC_CTR2_REG            0x1
#define RTC_SEC_REG             0x2
#define RTC_MIN_REG             0x3
#define RTC_HOUR_REG            0x4
#define RTC_DAY_REG             0x5
#define RTC_WEEKDAY_REG         0x6
#define RTC_MONTH_REG           0x7
#define RTC_YEAR_REG            0x8

#define RTC_MIN_ALARM_REG       0x9
#define RTC_HOUR_ALARM_REG      0xA
#define RTC_DAY_ALARM_REG       0xB
#define RTC_WEEKDAY_ALARM_REG   0xC

/* macros */

/*
 * The year register only can save 00-99, so the BASE_YEAR is needed,
 * Currently, setting the BASE_YEAR as 2000.
 */

#define MIN_YEAR                2000
#define MAX_YEAR                2099

#define INIT_WEEKDAY            5
#define I2C_RTC_WRDELAY         0

/* misc defines */

#define SECS_PER_YEAR           (60 * 60 * 24 * 365)
#define SECS_PER_DAY            (60 * 60 * 24)
#define SECS_PER_HOUR           (60 * 60)
#define SECS_PER_MIN            (60)
#define TEST_YEAR                2000
#define DAYS_IN_YEAR             365
#define DAY_IN_WEEK              7
#define JANUARY                  1
#define FEBRUARY                 2
#define DECEMBER                 12
#define IS_LEAP_YEAR(year)      ((((year) % 4 == 0) && ((year) % 100 != 0)) \
                                || (year) % 400 == 0)

/* month days in one year */

static const int month_days[12] = 
    {
    31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    };

static const char * week_days[7] = 
    {
    "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"
    };

/* typedef */

typedef struct i2cRtcCtlr
    {
    VXB_DEVICE_ID pInst;
    int           index;
    struct        vxbRtcFunctionality rtcFunc;
    } I2C_RTC_CTRL;

/* I2C_RTC device struct */

struct rtc_info
    {
    char  name[MAX_DRV_NAME_LEN];
    UINT8 rtcRegNum;
    UINT8 alarmOffset;
    };

/* This structure holds the temperature values */

typedef struct rtcTemp
    {
    UINT8 tempHig;  /* Temperature Register (Upper Byte) */
    UINT8 tempLow;  /* Temperature Register (Lower Byte) */
    } TEMP_DATA;

/*
 * the following macros convert from BCD to binary and back.
 * Be careful that the arguments are chars, only char width returned.
 */

#define BCD_TO_BIN(bcd) (( ((((bcd) & 0xf0) >> 4) * 10) + ((bcd) & 0xf) ) & 0xff)
#define BIN_TO_BCD(bin) (( (((bin) / 10) << 4) + ((bin) % 10) ) & 0xff)

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxI2cPcfRtch */
