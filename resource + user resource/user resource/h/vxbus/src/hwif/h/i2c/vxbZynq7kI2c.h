/* vxbZynq7kI2c.h - ZYNQ7K I2C hardware defintions */

/*
 * Copyright (c) 2012-2015 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01g,25aug15,sye  added I2C controller reset callback. (VXW6-84692)
01f,29jul15,sye  added I2C_MSG type of transfer. (VXW6-84022)
01e,08jan15,c_l  Improvement I2C driver. (VXW6-83848)
01d,05mar14,swu  fix i2c error. (VXW6-69999 VXW6-70001)
01c,31may13,y_y  remove the LOCAL for zynq7kI2cRead
                 and zynq7kI2cWrite. (WIND00419486)
01b,11apr13,y_y  clean the gnu build warnings.
01a,29may12,fao  written
*/

#ifndef __INCvxbZynq7kI2ch
#define __INCvxbZynq7kI2ch

#include <vxWorks.h>
#include <semLib.h>
#include "../h/vxbus/vxbAccess.h"
#include <hwif/vxbus/vxBus.h>
#include <isrDeferLib.h>

#ifdef __cplusplus
extern "C" {
#endif

/* defines */

#define ZYNQ7K_I2C_NAME         "zynq7kI2c"

/* register definitions for I2C */

#define I2C_CONTROL             (0x00)
#define I2C_STATUS              (0x04)
#define I2C_ADDR                (0x08)
#define I2C_DATA                (0x0C)
#define I2C_IRQSTATUS           (0x10)
#define I2C_TRANSFER_SIZE       (0x14)
#define I2C_SLAVE_MON_PAUSE     (0x18)
#define I2C_TIME_OUT            (0x1C)
#define I2C_IRQ_MASK            (0x20)
#define I2C_IRQ_ENABLE          (0x24)
#define I2C_IRQ_DISABLE         (0x28)

#define I2C_CON_CLR_FIFO        (0x1 << 6 )
#define I2C_CON_SLVMON          (0x1 << 5 )
#define I2C_CON_HOLD            (0x1 << 4 )
#define I2C_CON_ACKEN           (0x1 << 3 )
#define I2C_CON_NEA             (0x1 << 2 )
#define I2C_CON_MS              (0x1 << 1 )
#define I2C_CON_RW              (0x1 << 0 )

#define I2C_STATUS_BA           (0x1 << 8)
#define I2C_STATUS_RXOVF        (0x1 << 7)
#define I2C_STATUS_TXDV         (0x1 << 6)
#define I2C_STATUS_RXDV         (0x1 << 5)
#define I2C_STATUS_RXRW         (0x1 << 3)

#define I2C_IRQENABLE_ARB_LOST  (0x1 << 9)
#define I2C_IRQENABLE_RX_UNF    (0x1 << 7)
#define I2C_IRQENABLE_TX_OVF    (0x1 << 6)
#define I2C_IRQENABLE_RX_OVF    (0x1 << 5)
#define I2C_IRQENABLE_SLV_RDY   (0x1 << 4)
#define I2C_IRQENABLE_TO        (0x1 << 3)
#define I2C_IRQENABLE_NACK      (0x1 << 2)
#define I2C_IRQENABLE_DATA      (0x1 << 1)
#define I2C_IRQENABLE_COMP      (0x1 << 0)

#define I2C_IRQSTATUS_ARB_LOST  (0x1 << 9)
#define I2C_IRQSTATUS_RX_UNF    (0x1 << 7)
#define I2C_IRQSTATUS_TX_OVF    (0x1 << 6)
#define I2C_IRQSTATUS_RX_OVF    (0x1 << 5)
#define I2C_IRQSTATUS_SLV_RDY   (0x1 << 4)
#define I2C_IRQSTATUS_TO        (0x1 << 3)
#define I2C_IRQSTATUS_NACK      (0x1 << 2)
#define I2C_IRQSTATUS_DATA      (0x1 << 1)
#define I2C_IRQSTATUS_COMP      (0x1 << 0)

#define I2C_IRQ_ALL_MASK        (0x2ff)
#define I2C_IRQ_EANBLE_MASK     (I2C_IRQSTATUS_ARB_LOST  | \
                                 I2C_IRQSTATUS_RX_UNF    | \
                                 I2C_IRQSTATUS_TX_OVF    | \
                                 I2C_IRQSTATUS_RX_OVF    | \
                                 I2C_IRQSTATUS_NACK      | \
                                 I2C_IRQSTATUS_DATA      | \
                                 I2C_IRQSTATUS_COMP)

#define I2C_FIFO_SIZE           (16)
#define I2C_RCV_FIFO_SIZE       (I2C_FIFO_SIZE - 2)
#define I2C_RCV_CHECK_SIZE      (I2C_FIFO_SIZE + 1)
#define I2C_RCV_TRANSFER_SIZE   (255)

#define I2C_CTR_VAL             (I2C_CON_CLR_FIFO |\
                                 I2C_CON_NEA |\
                                 I2C_CON_MS)
#define I2C_TIMEOUT_MAX         (0xff)
#define I2C_TIMEOUT             (20000)
#define SEM_TIMEOUT             (5)

#define I2C_CONTROL_DIVA_MAX    (4)
#define I2C_CONTROL_DIVB_MAX    (64)

typedef struct i2c_drv_ctrl
    {
    VXB_DEVICE_ID       i2cDev;
    void *              i2cHandle;
    SEM_ID              i2cDevSem;
    SEM_ID              i2cDataSem;
    UINT32              clkFreq;        /* controller clk freq */
    UINT32              busSpeed;       /* parameter for compatibility */
    UINT32              busFreq;        /* real bus freq for I2C operation */
    BOOL                polling;        /* use poll or interrupt mode */
    UINT8 *             dataBuf;
    UINT32              dataLength;
    BOOL                intEnabled;
    BOOL                lastMsg;
    BOOL                reTransfer;
    BOOL                readLenFromSlave;
    STATUS              irqStatus;
    ISR_DEFER_QUEUE_ID  queueId;
    ISR_DEFER_JOB       isrDef;
    UINT16              irqSrc;
    VOIDFUNCPTR         ctrlReset;
} I2C_DRV_CTRL;

/* I2C controller read and write interface */

#define ZYNQ7K_I2C_BAR(p)       ((char *)((p)->i2cDev->pRegBase[0]))
#define ZYNQ7K_I2C_HANDLE(p)    ((p)->i2cHandle)

#undef CSR_READ_2
#define CSR_READ_2(pCtrl, addr)                         \
    vxbRead16 (ZYNQ7K_I2C_HANDLE(pCtrl),                \
        (UINT16 *)((char *)ZYNQ7K_I2C_BAR(pCtrl) + addr))

#undef CSR_WRITE_2
#define CSR_WRITE_2(pCtrl, addr, data)                  \
    vxbWrite16 (ZYNQ7K_I2C_HANDLE(pCtrl),               \
        (UINT16 *)((char *)ZYNQ7K_I2C_BAR(pCtrl) + addr), data)

#undef CSR_SETBIT_2
#define CSR_SETBIT_2(pCtrl, addr, data)                 \
        CSR_WRITE_2 (pCtrl, addr, CSR_READ_2 (pCtrl, addr) | (data))

#undef CSR_CLRBIT_2
#define CSR_CLRBIT_2(pCtrl, addr, data)                 \
        CSR_WRITE_2 (pCtrl, addr, CSR_READ_2 (pCtrl, addr) & ~(data))

#undef CSR_READ_1
#define CSR_READ_1(pCtrl, addr)                         \
    vxbRead8 (ZYNQ7K_I2C_HANDLE(pCtrl),                 \
        (UINT8 *)((char *)ZYNQ7K_I2C_BAR(pCtrl) + addr))

#undef CSR_WRITE_1
#define CSR_WRITE_1(pCtrl, addr, data)                   \
    vxbWrite8 (ZYNQ7K_I2C_HANDLE(pCtrl),                \
        (UINT8 *)((char *)ZYNQ7K_I2C_BAR(pCtrl) + addr), data)

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbZynq7kI2ch */

