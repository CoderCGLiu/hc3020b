/* vxbAm35xxI2c.h - AM35xx I2C hardware defintions */

/*
 * Copyright (c) 2011, 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01c,31may13,y_y  remove LOCAL for am35xxI2cRead and am35xxI2cWrite. (WIND00419486)
01b,10apr13,y_y  move registers defines from vxbAm35xxI2c.c to this file.
01a,01dec11,hcl  written
*/

#ifndef __INCvxbAm35xxI2ch
#define __INCvxbAm35xxI2ch

#ifdef __cplusplus
extern "C" {
#endif

/* defines */

#define AM35XX_I2C_NAME         "am35xxI2c"

#define I2C_CNT_MAX         65535
#define I2C_TIMEOUT         10000
#define SEM_TIMEOUT         2

/* TI I2C registers defines */

#define I2C_REV             0x00
#define I2C_IE              0x04
#define I2C_STAT            0x08
#define I2C_WE              0x0C
#define I2C_SYSS            0x10
#define I2C_BUF             0x14
#define I2C_CNT             0x18
#define I2C_DATA            0x1C
#define I2C_SYSC            0x20
#define I2C_CON             0x24
#define I2C_OA0             0x28
#define I2C_SA              0x2C
#define I2C_PSC             0x30
#define I2C_SCLL            0x34
#define I2C_SCLH            0x38
#define I2C_SYSTEST         0x3C
#define I2C_BUFSTAT         0x40
#define I2C_OA1             0x44
#define I2C_OA2             0x48
#define I2C_OA3             0x4C
#define I2C_ACTOA           0x50
#define I2C_SBLOCK          0x54

/* Control register */

#define I2C_CON_STT         0x0001
#define I2C_CON_STP         0x0002
#define I2C_CON_XOA3        0x0010
#define I2C_CON_XOA2        0x0020
#define I2C_CON_XOA1        0x0040
#define I2C_CON_XOA0        0x0080
#define I2C_CON_XSA         0x0100
#define I2C_CON_TRX         0x0200
#define I2C_CON_MST         0x0400
#define I2C_CON_STB         0x0800
#define I2C_CON_OPMODE_FS   0x0000
#define I2C_CON_OPMODE_HS   0x1000
#define I2C_CON_OPMODE_SCCB 0x2000
#define I2C_CON_OPMODE_MSK  0x3000
#define I2C_CON_EN          0x8000

/* Status register */

#define I2C_STAT_AL         0x0001
#define I2C_STAT_NACK       0x0002
#define I2C_STAT_ARDY       0x0004
#define I2C_STAT_RRDY       0x0008
#define I2C_STAT_XRDY       0x0010
#define I2C_STAT_GC         0x0020
#define I2C_STAT_STC        0x0040
#define I2C_STAT_AEER       0x0080
#define I2C_STAT_BF         0x0100
#define I2C_STAT_AAS        0x0200
#define I2C_STAT_XUDF       0x0400
#define I2C_STAT_ROVR       0x0800
#define I2C_STAT_BB         0x1000
#define I2C_STAT_RDR        0x2000
#define I2C_STAT_XDR        0x4000

#define I2C_STAT_MASK       (I2C_STAT_AL    | I2C_STAT_NACK | I2C_STAT_ARDY | \
                            I2C_STAT_RRDY   | I2C_STAT_XRDY | I2C_STAT_ROVR | \
                            I2C_STAT_XUDF)

/* interrupt enable register */

#define I2C_IE_AL           0x0001
#define I2C_IE_NACK         0x0002
#define I2C_IE_ARDY         0x0004
#define I2C_IE_RRDY         0x0008
#define I2C_IE_XRDY         0x0010
#define I2C_IE_GC           0x0020
#define I2C_IE_STC          0x0040
#define I2C_IE_AEER         0x0080
#define I2C_IE_BF           0x0100
#define I2C_IE_AAS          0x0200
#define I2C_IE_XUDF         0x0400
#define I2C_IE_ROVR         0x0800
#define I2C_IE_RDR          0x2000
#define I2C_IE_XDR          0x4000

#define I2C_IE_MASK         (I2C_IE_AL  | I2C_IE_NACK | I2C_IE_ARDY | \
                            I2C_IE_RRDY | I2C_IE_XRDY)

#define I2C_SYSC_SRST       (1 << 1)
#define I2C_SYSS_RDONE      (1 << 0)

/* I2C controller read and write interface */

#define I2C_BAR(p)       ((p)->pRegBase[0])
#define I2C_HANDLE(p)    (((I2C_DRV_CTRL *)(p)->pDrvCtrl)->i2cHandle)

#define CSR_READ_2(pDev, addr)                          \
    vxbRead16 (I2C_HANDLE(pDev),                        \
        (UINT16 *)((char *)I2C_BAR(pDev) + addr))

#define CSR_WRITE_2(pDev, addr, data)                   \
    vxbWrite16 (I2C_HANDLE(pDev),                       \
        (UINT16 *)((char *)I2C_BAR(pDev) + addr), data)

/* forward decleration */

STATUS am35xxI2cRead (VXB_DEVICE_ID, UINT8, UINT32, UINT8 *, UINT32);
STATUS am35xxI2cWrite (VXB_DEVICE_ID, UINT8, UINT32, UINT8 *, UINT32);

/* structure holding the instance specific details */

typedef struct i2c_drv_ctrl {
    VXB_DEVICE_ID   i2cDev;
    void *          i2cHandle;
    SEM_ID          i2cDevSem;
    SEM_ID          i2cDataSem;
    UINT32          fClkFreq;
    UINT32          iClkFreq;
    UINT32          busSpeed;
    BOOL            polling;
    UINT16          slaveAddr;
    UINT8 *         dataBuf;
    UINT8 *         dataBufHead;
    UINT32          dataLength;	
} I2C_DRV_CTRL;

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbAm35xxI2ch */
