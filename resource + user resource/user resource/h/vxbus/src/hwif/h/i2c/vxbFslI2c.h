/* vxbFslI2c.h - Freescale I2C hardware defintions */

/*
 * Copyright (c) 2012-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01e,22nov13,d_l  update FI2C_TIMEOUT. (WIND00437119)
01d,12oct13,xms  Fix semTake with WAIT_FORVER issue. (WIND00434535)
01c,14sep12,c_l  add Freesacle i.MX53 support
01b,19jul12,my_  add Freesacle i.MX support
01a,14feb12,y_y  written based on 01a of hwif/h/resource/vxbFslI2c.h
*/

#ifndef __INCvxbFslI2ch
#define __INCvxbFslI2ch

#ifdef __cplusplus
extern "C" {
#endif

#include <vxBusLib.h>

/* defines */

#define FSL_I2C_NAME    "fslI2c"
#define FI2C_TIMEOUT    100000
#define FI2C_WAIT_SECS  (3)
#define BUS_IDLE        0x1
#define TRANS_DONE      0x2

/* Freescale I2C registers defines */

/* Register Addresses */

#define FI2C_ADDR           0x00    /* Address register */
#define FI2C_FDR            0x04    /* Freq divider register */
#define FI2C_CTRL           0x08    /* Control register */
#define FI2C_STAT           0x0C    /* Status register */
#define FI2C_DATA           0x10    /* Data register */
#define FI2C_DFSRR          0x14    /* Filter sample rate */

/* Control register */

#define FI2C_CTRL_MEN       0x80    /* Module enable */
#define FI2C_CTRL_MIEN      0x40    /* Interrupt enable */
#define FI2C_CTRL_MSTA      0x20    /* Start transaction */
#define FI2C_CTRL_MTX       0x10    /* Transmit */
#define FI2C_CTRL_TXAK      0x08    /* TX acknowledge */
#define FI2C_CTRL_RSTA      0x04    /* Repeated start */
#define FI2C_CTRL_BCST      0x01    /* Broadcast enable */

/* Status register */

#define FI2C_STAT_MCF       0x80    /* Transfer in progress */
#define FI2C_STAT_MAAS      0x40    /* Addressed as slave */
#define FI2C_STAT_MBB       0x20    /* Bus busy */
#define FI2C_STAT_MAL       0x10    /* Arbitration lost */
#define FI2C_STAT_BCSTM     0x08    /* Broadcast match */
#define FI2C_STAT_SRW       0x04    /* Slave read/write */
#define FI2C_STAT_MIF       0x02    /* Module interrupt */
#define FI2C_STAT_RXAK      0x01    /* RX acknowledge */

/* I2C controllor read and write interface */

#undef CSR_READ_1
#undef CSR_WRITE_1

#define FSL_I2C_BAR(p)       ((char *)((p)->pRegBase[0]))
#define FSL_I2C_HANDLE(p)    (((I2C_DRV_CTRL *)(p)->pDrvCtrl)->i2cHandle)

#define CSR_READ_1(pDev, addr)                                  \
    vxbRead8 (FSL_I2C_HANDLE(pDev), (UINT8 *)((char *)FSL_I2C_BAR(pDev) + addr))

#define CSR_WRITE_1(pDev, addr, data)               \
    vxbWrite8 (FSL_I2C_HANDLE(pDev),                \
        (UINT8 *)((char *)FSL_I2C_BAR(pDev) + addr), data)

#define CSR_CLRBIT_1(pDev, offset, val)             \
    CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) & (val))

#define CSR_SETBIT_1(pDev, offset, val)             \
    CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) | (val))

#define CSR_SETCLRBIT_1(pDev, offset, val1, val2)          \
        CSR_WRITE_1(pDev, offset, (CSR_READ_1(pDev, offset) & val1) | val2)

/* structure holding the instance specific details */

typedef struct i2c_drv_ctrl
    {
    VXB_DEVICE_ID       i2cDev;
    void *              i2cHandle;
    SEM_ID              i2cDevSem;
    SEM_ID              i2cMcfLock;
    UINT32              clkFrequency;
    UINT32              busSpeed;
    BOOL                polling;
    int                 socType;
    int                 actualSpeed;
    int                 i2cInitPhase;
    } I2C_DRV_CTRL;

/* I2C Bus Speed */

typedef struct i2c_speed
    {
    UINT16      i2c_divider;
    UINT8       i2c_dfsr;
    UINT8       i2c_fdr;
    } I2C_SPEED;

/* typedefs */

typedef enum
    {
    I2C_READ_RANDOM = 0x01,
    I2C_READ_SEQUENTIAL = 0x02,
    I2C_WRITE_BYTE = 0x04,
    I2C_WRITE_PAGE = 0x08
    } I2C_MODE_TYPE;

/* supported SoC type */

#define I2C_FSL   0x0
#define I2C_IMX6x 0x1
#define I2C_IMX53 0x2

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbFslI2ch */
