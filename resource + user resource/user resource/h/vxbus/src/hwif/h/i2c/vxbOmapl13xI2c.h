/* vxbOmapl13xI2c.h - TI Omapl13x I2C hardware defintions */

/*
 * Copyright (c) 2011, 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01d,16sep13,sye  added iccd resource. (WIND00400220)
01c,31may13,y_y  remove LOCAL for omapl13xI2cRead and
                 omapl13xI2cWrite. (WIND00419486) 
01b,10apr13,y_y  move registers defines from source file to this file.
01a,01dec11,hcl  written
*/

#ifndef __INCvxbOmapl13xI2ch
#define __INCvxbOmapl13xI2ch

#ifdef __cplusplus
extern "C" {
#endif

/* defines */

#define OMAPL13X_I2C_NAME       "omapl13xI2c"

#define I2C0_REG_BASE           0X01C22000
#define I2C_TIMEOUT             10000
#define I2C_STOP_DELAY          100
#define SEM_TIMEOUT             2

#define  SWITCH_ADDRESS         0x5D
#define  CODEC_ADDRESS          0x1B
#define  EEPROM_ADDRESS         0x50

/* I2c register define */

#define  ICOAR                  0x00
#define  ICIMR                  0x04
#define  ICSTR                  0x08
#define  ICCLKL                 0x0C
#define  ICCLKH                 0x10
#define  ICCNT                  0x14
#define  ICDRR                  0x18
#define  ICSAR                  0x1C
#define  ICDXR                  0x20
#define  ICMDR                  0x24
#define  ICIVR                  0x28
#define  ICEMDR                 0x2C
#define  ICPSC                  0x30
#define  REVID1                 0x34
#define  REVID2                 0x38
#define  ICPFUNC                0x48
#define  ICPDIR                 0x4C
#define  ICPDIN                 0x50
#define  ICPDOUT                0x54
#define  ICPDSET                0x58
#define  ICPDCLR                0x5C

#define  ICMDR_NACKMOD          0x8000
#define  ICMDR_FREE             0x4000
#define  ICMDR_STT              0x2000
#define  ICMDR_IDLEEN           0x1000
#define  ICMDR_STP              0x0800
#define  ICMDR_MST              0x0400
#define  ICMDR_TRX              0x0200
#define  ICMDR_XA               0x0100
#define  ICMDR_RM               0x0080
#define  ICMDR_DLB              0x0040
#define  ICMDR_IRS              0x0020
#define  ICMDR_STB              0x0010
#define  ICMDR_FDF              0x0008
#define  ICMDR_BC_MASK          0x0007

#define  ICSTR_SDIR             0x4000
#define  ICSTR_NACKINT          0x2000
#define  ICSTR_BB               0x1000
#define  ICSTR_RSFULL           0x0800
#define  ICSTR_XSMT             0x0400
#define  ICSTR_AAS              0x0200
#define  ICSTR_AD0              0x0100
#define  ICSTR_SCD              0x0020
#define  ICSTR_ICXRDY           0x0010
#define  ICSTR_ICRRDY           0x0008
#define  ICSTR_ARDY             0x0004
#define  ICSTR_NACK             0x0002
#define  ICSTR_AL               0x0001

#define  ICIMR_AL               0x0001
#define  ICIMR_NACK             0x0002
#define  ICIMR_ARDY             0x0004
#define  ICIMR_ICRRDY           0x0008
#define  ICIMR_ICXRDY           0x0010
#define  ICIMR_SCD              0x0020
#define  ICIMR_AAS              0x0040

#define  ICIVR_AL               0x01
#define  ICIVR_NACK             0x02
#define  ICIVR_ARDY             0x03
#define  ICIVR_ICRRDY           0x04
#define  ICIVR_ICXRDY           0x05
#define  ICIVR_SCD              0x06
#define  ICIVR_AAS              0x07

/* I2C controller read and write interface */

#define TI_I2C_BAR(p)           ((UINT16 *)((p)->pRegBase[0]))
#define TI_I2C_HANDLE(p)        (((I2C_DRV_CTRL *)(p)->pDrvCtrl)->i2cHandle)

#define CSR_READ_2(pDev, addr)                          \
    vxbRead16 (TI_I2C_HANDLE(pDev),                     \
        (UINT16 *)((char *)TI_I2C_BAR(pDev) + addr))

#define CSR_WRITE_2(pDev, addr, data)                   \
    vxbWrite16 (TI_I2C_HANDLE(pDev),                    \
        (UINT16 *)((char *)TI_I2C_BAR(pDev) + addr), data)

typedef struct i2c_drv_ctrl {
    VXB_DEVICE_ID   i2cDev;         
    void *          i2cHandle;      
    SEM_ID          i2cDevSem;      
    SEM_ID          i2cDataSem;   
    UINT32          clkFreq;        
    UINT32          busSpeed;
    UINT32          iccd;
    BOOL            polling;        
} I2C_DRV_CTRL;

/* forward decleration */

STATUS omapl13xI2cRead(VXB_DEVICE_ID, UINT8, UINT32, UINT8 *, UINT32);
STATUS omapl13xI2cWrite(VXB_DEVICE_ID, UINT8, UINT32, UINT8 *, UINT32);

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbOmapl13xI2ch */
