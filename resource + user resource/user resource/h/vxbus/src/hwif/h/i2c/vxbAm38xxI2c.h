/* vxbAm38xxI2c.h - AM38xx I2C hardware defintions */

/*
 * Copyright (c) 2011, 2013, 2015 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
26jan15,swu  Added support for AM437x. (US51994)
01b,10apr13,y_y  move registers defines from source file to this file.
01a,01dec11,hcl  written
*/

#ifndef __INCvxbAm38xxI2ch
#define __INCvxbAm38xxI2ch

#ifdef __cplusplus
extern "C" {
#endif

/* defines */

#define AM38XX_I2C_NAME         "am38xxI2c"

/* register definitions for I2C */

#define I2C_REVNB_LO        0x00    /* Module Revision Register (LOW BYTES) */
#define I2C_REVNB_HI        0x04    /* Module Revision Register (HIGH BYTES) */
#define I2C_SYSC            0x10    /* System configuration register */
#define I2C_EOI             0x20    /* I2C End of Interrupt Register */
#define I2C_IRQSTATUS_RAW   0x24    /* I2C Status Raw Register */
#define I2C_IRQSTATUS       0x28    /* I2C Status Register */
#define I2C_IRQENABLE_SET   0x2c    /* I2C Interrupt Enable Set Register */
#define I2C_IRQENABLE_CLR   0x30    /* I2C Interrupt Enable Clear Register */
#define I2C_WE              0x34    /* I2C Wakeup Enable Register */
#define I2C_DMARXENABLE_SET 0x38    /* Receive DMA Enable Set Register */
#define I2C_DMATXENABLE_SET 0x3c    /* Transmit DMA Enable Set Register */
#define I2C_DMARXENABLE_CLR 0x40    /* Receive DMA Enable Clear Register */
#define I2C_DMATXENABLE_CLR 0x44    /* Transmit DMA Enable Clear Register */
#define I2C_DMARXWAKE_EN    0x48    /* Receive DMA Wakeup Register */
#define I2C_DMATXWAKE_EN    0x4c    /* Transmit DMA Wakeup Register */
#define I2C_SYSS            0x90    /* System Status Register */
#define I2C_BUF             0x94    /* Buffer Configuration Register */
#define I2C_CNT             0x98    /* Data Counter Register */
#define I2C_DATA            0x9c    /* Data Access Register */
#define I2C_CON             0xa4    /* I2C Configuration Register */
#define I2C_OA              0xa8    /* I2C Own Address Register */
#define I2C_SA              0xac    /* I2C Slave Address Register */
#define I2C_PSC             0xb0    /* I2C Clock Prescaler Register */
#define I2C_SCLL            0xb4    /* I2C SCL Low Time Register */
#define I2C_SCLH            0xb8    /* I2C SCL High Time Register */
#define I2C_SYSTEST         0xbc    /* System Test Register */
#define I2C_BUFSTAT         0xc0    /* I2C Buffer Status Register */
#define I2C_OA1             0xc4    /* I2C Own Address 1 Register */
#define I2C_OA2             0xc8    /* I2C Own Address 2 Register */
#define I2C_OA3             0xcc    /* I2C Own Address 3 Register */
#define I2C_ACTOA           0xd0    /* Active Own Address Register */
#define I2C_SBLOCK          0xd4    /* I2C Clock Blocking Enable Registe */

#define I2C_IRQENABLE_XDR   (0x1 << 14)
#define I2C_IRQENABLE_RDR   (0x1 << 13)
#define I2C_IRQENABLE_ROVR  (0x1 << 11)
#define I2C_IRQENABLE_XUDF  (0x1 << 10)
#define I2C_IRQENABLE_AAS   (0x1 << 9)
#define I2C_IRQENABLE_BF    (0x1 << 8)
#define I2C_IRQENABLE_AERR  (0x1 << 7)
#define I2C_IRQENABLE_STC   (0x1 << 6)
#define I2C_IRQENABLE_GC    (0x1 << 5)
#define I2C_IRQENABLE_XRDY  (0x1 << 4)
#define I2C_IRQENABLE_RRDY  (0x1 << 3)
#define I2C_IRQENABLE_ARDY  (0x1 << 2)
#define I2C_IRQENABLE_NACK  (0x1 << 1)
#define I2C_IRQENABLE_AL    (0x1 << 0)

#define I2C_IRQENABLE_ALL       (0xFFFF)
#define I2C_IRQENABLE_DEFAULT   (I2C_IRQENABLE_NACK     | \
                                 I2C_IRQENABLE_AL)

#define I2C_IRQSTATUS_XDR   (0x1 << 14)
#define I2C_IRQSTATUS_RDR   (0x1 << 13)
#define I2C_IRQSTATUS_BB    (0x1 << 12)
#define I2C_IRQSTATUS_ROVR  (0x1 << 11)
#define I2C_IRQSTATUS_XUDF  (0x1 << 10)
#define I2C_IRQSTATUS_AAS   (0x1 << 9)
#define I2C_IRQSTATUS_BF    (0x1 << 8)
#define I2C_IRQSTATUS_AERR  (0x1 << 7)
#define I2C_IRQSTATUS_STC   (0x1 << 6)
#define I2C_IRQSTATUS_GC    (0x1 << 5)
#define I2C_IRQSTATUS_XRDY  (0x1 << 4)
#define I2C_IRQSTATUS_RRDY  (0x1 << 3)
#define I2C_IRQSTATUS_ARDY  (0x1 << 2)
#define I2C_IRQSTATUS_NACK  (0x1 << 1)
#define I2C_IRQSTATUS_AL    (0x1 << 0)

#define I2C_CON_EN          (0x1 << 15)
#define I2C_CON_STB         (0x1 << 11)
#define I2C_CON_MST         (0x1 << 10)
#define I2C_CON_TRX         (0x1 << 9 )
#define I2C_CON_XSA         (0x1 << 8 )
#define I2C_CON_STP         (0x1 << 1 )
#define I2C_CON_STT         (0x1 << 0 )

/* I2C buffer management mask */

#define I2C_BUF_RXFIFO_CLR      (0x1 << 14)
#define I2C_BUF_TXFIFO_CLR      (0x1 << 6)
#define I2C_BUFSTAT_RXTRSH_SHIFT        (8)

#define I2C_BUFSTAT_FIFODEPTH_MASK      (0xC000)
#define I2C_BUFSTAT_FIFODEPTH_SHIFT     (14)

#define I2C_BUFSTAT_RXSTAT_MASK         (0x3F00)
#define I2C_BUFSTAT_RXSTAT_SHIFT        (8)

#define I2C_BUFSTAT_FIFODEPTH_8         (0x0)
#define I2C_BUFSTAT_FIFODEPTH_16        (0x1)
#define I2C_BUFSTAT_FIFODEPTH_32        (0x2)
#define I2C_BUFSTAT_FIFODEPTH_64        (0x3)

#define I2C_SYSC_SRST       (1 << 1)
#define I2C_SYSS_RDONE      (1 << 0)

#define I2C_CNT_MAX         65535
#define I2C_TIMEOUT         1000000
#define SEM_TIMEOUT         2
#define MAX_I2C_NUM         4

/* 
 * Timeout value is determined by assuming I2C bus's transmit speed is
 *     (I2C Bus Speed / 100) 
 * bytes per second (worst case). 
 *
 * For example, if I2C Bus Speed is 400KHz, then I2C bus's transmit speed is
 * 4K bytes per second (worst case)
 */

#define SEM_TIMEOUT_BYTES_FACTOR        (100)

/* default read delay time */

#define I2C_READDELAY_DEFAULT           (500)

/* I2C controller read and write interface */

#define AM38XX_I2C_BAR(p)       ((char *)((p)->pRegBase[0]))
#define AM38XX_I2C_HANDLE(p)    (((I2C_DRV_CTRL *)(p)->pDrvCtrl)->i2cHandle)

#define CSR_READ_4(pDev, addr)                          \
    vxbRead32 (AM38XX_I2C_HANDLE(pDev),                 \
        (UINT32 *)((char *)AM38XX_I2C_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)                   \
    vxbWrite32 (AM38XX_I2C_HANDLE(pDev),                \
        (UINT32 *)((char *)AM38XX_I2C_BAR(pDev) + addr), data)

/* structure holding the instance specific details */

typedef struct i2c_drv_ctrl {
    VXB_DEVICE_ID   i2cDev;
    void *          i2cHandle;
    SEM_ID          i2cDevSem;
    SEM_ID          i2cDataSem;
    UINT32          fClkFreq;
    UINT32          iClkFreq;
    UINT32          busSpeed;
    BOOL            polling;
    BOOL            i2cStatus;
    BOOL            fifoEnabled;
    UINT16          slaveAddr;
    UINT8 *         dataBuf;
    UINT8 *         dataBufHead;
    UINT32          dataLength;
    UINT32          fifoLen;
    UINT32          rxThreshold;
    UINT32          txThreshold;
    UINT32          readDelay;
} I2C_DRV_CTRL;

/* forward decleration */

LOCAL STATUS am38xxI2cRead (VXB_DEVICE_ID, UINT8, UINT32, UINT8 *, UINT32);
LOCAL STATUS am38xxI2cWrite (VXB_DEVICE_ID, UINT8, UINT32, UINT8 *, UINT32);

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbAm38xxI2ch */
