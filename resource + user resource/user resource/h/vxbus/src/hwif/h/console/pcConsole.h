/* pcConsole.h - PC Keyboard and VGA Controller header file */

/* 
 * Copyright (c) 1984-2007, 2010 Wind River Systems, Inc.
 * 
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01f,09feb10,jc0  LP64 adaptation.
01e,20feb07,pmr  converted to VxBus.
01d,06dec01,jlb  options to send scan codes and set LEDs
01c,12oct99,jmb  fix build warning, conflict with SH cpu type
01b,29sep98,fle  made it refgen parsable
01a,24sep93,vin  created

*/

#ifndef __INCpcConsoleh
#define __INCpcConsoleh

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _ASMLANGUAGE

#include <tyLib.h>

/* typedefs */

typedef int (*PC_CON_WRITE_FUNCPTR)(void *);
IMPORT TY_DEV * pcConDevBind (int, PC_CON_WRITE_FUNCPTR, void *);

/* pc console device descriptor */

typedef struct				/* PC_CON_DEV */
    {
    TY_DEV	tyDev;
    BOOL	created;		/* true if this device is created */
    int		number;			/* number for this console */
    PC_CON_WRITE_FUNCPTR pWriteFunc;	/* video tyWrite routine */
    void *	pWriteArg;		/* argument for this routine */
    } PC_CON_DEV;

typedef struct
    {
    int		unit;
    int *	pArg;
    BOOL	get;
    } VGA_QUERY;

/* ioctl and attribute definitions */

#define CONIOSETATRB		1001
#define CONIOGETATRB		1002
#define CONIOSETKBD		1003
#define CONIOSCREENREV		1004
#define CONIOBEEP		1005
#define CONIOCURSORON		1006
#define CONIOCURSOROFF		1007
#define CONIOCURSORMOVE		1008
#define CONIOCURCONSOLE		1009
#define CONIOCONVERTSCAN	1010
#define CONIOLEDS		1011

#define UNDERLINE               0x01   /* only if monochrome */
#define ATRB_FG_BLACK		0x00
#define ATRB_FG_BLUE		0x01
#define ATRB_FG_GREEN		0x02
#define ATRB_FG_CYAN		0x03
#define ATRB_FG_RED		0x04
#define ATRB_FG_MAGENTA		0x05
#define ATRB_FG_BROWN		0x06
#define ATRB_FG_WHITE		0x07
#define ATRB_BRIGHT		0x08
#define ATRB_FG_GRAY		(ATRB_FG_BLACK   | ATRB_BRIGHT)
#define ATRB_FG_LIGHTBLUE	(ATRB_FG_BLUE    | ATRB_BRIGHT)
#define ATRB_FG_LIGHTGREEN	(ATRB_FG_GREEN   | ATRB_BRIGHT)
#define ATRB_FG_LIGHTCYAN	(ATRB_FG_CYAN    | ATRB_BRIGHT)
#define ATRB_FG_LIGHTRED	(ATRB_FG_RED     | ATRB_BRIGHT)
#define ATRB_FG_LIGHTMAGENTA	(ATRB_FG_MAGENTA | ATRB_BRIGHT)
#define ATRB_FG_YELLOW		(ATRB_FG_BROWN   | ATRB_BRIGHT)
#define ATRB_FG_BRIGHTWHITE	(ATRB_FG_WHITE   | ATRB_BRIGHT)
#define ATRB_BG_BLACK		0x00
#define ATRB_BG_BLUE		0x10
#define ATRB_BG_GREEN		0x20
#define ATRB_BG_CYAN		0x30
#define ATRB_BG_RED		0x40
#define ATRB_BG_MAGENTA		0x50
#define ATRB_BG_BROWN		0x60
#define ATRB_BG_WHITE		0x70
#define ATRB_BLINK		0x80
#define ATRB_CHR_REV		0x0100

#define MONO			0
#define COLOR			1

#define JAPANES_KBD		0
#define ENGLISH_KBD		1

/* function declarations */
#if defined(__STDC__) || defined(__cplusplus)

extern	int	pcConDrv (void);
extern	int	pcConDevCreate (char *name, FAST int channel, size_t rdBufSize,
			      size_t wrtBufSize);

#else

extern	int	pcConDrv ();
extern	int	pcConDevCreate ();

#endif  /* __STDC__ */

#endif  /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCpcConsoleh */
