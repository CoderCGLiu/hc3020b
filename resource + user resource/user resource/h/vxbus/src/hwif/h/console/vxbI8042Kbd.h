/* vxbI8042Kbd.h - PC Keyboard and VGA Controller header file */

/* 
 * Copyright (c) 1984-2008, 2010 Wind River Systems, Inc.
 * 
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01d,09feb10,jc0  LP64 adaptation.
01c,02oct08,pmr  Fix 122701: use atomic access for timeout vars
01b,17aug07,pmr  new register access methods
01a,20feb07,pmr  adapted for VxBus from version 01d.
*/

#ifndef __INCvxbI8042Kbdh
#define __INCvxbI8042Kbdh

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _ASMLANGUAGE

#include <tyLib.h>
#include <vxBusLib.h>
#include <hwif/vxbus/vxBus.h>

/* method ids */
METHOD_DECL(kbdLedGet); 
METHOD_DECL(kbdLedSet);
METHOD_DECL(kbdCurrConSet);
METHOD_DECL(kbdConvertCharSet);
METHOD_DECL(kbdModeSet);
METHOD_DECL(kbdInit);
METHOD_DECL(kbdNumSet);

/* key board device descriptor */

typedef struct 
    {
    VXB_DEVICE_ID pDev;
    TY_DEV *	pTyDev;
    BOOL	curMode;		/* cursor mode TRUE / FALSE */
    int		kbdMode;		/* keyboard mode Japanese/English */
    UINT16	kbdFlags;		/* 16 bit keyboard flags */
    UINT16	kbdState;		/* unshift :shift :cntrl:numeric */
    BOOL	convertChar;		/* convert scan codes to ASCII */
    UCHAR       oldLedStat;		/* saved LED status */
    WDOG_ID     kbdWdid;		/* watchdog id */
    int		kbdWdticks;		/* watchdog expiration in ticks */
    atomic32_t	kbdTimeout;		/* true when watchdog expires */
    atomic32_t	kbdTimeoutCnt;		/* allow multiple tries */
    void *      regHandle;		/* register access map handle */
    } KBD_CON_DEV;

#define KBD_DATA_REG 	0
#define KBD_STAT_REG	4
#define KBD_CMD_REG	4

/*
 * Keyboard  definitions
 */

#define WAIT_MAX	100	/* Maximum wait time for keyboard */
#define E0_BASE		0x80	/* enhanced keyboard base */
#define EXTND_SIZE	16	/* no keys extra with extended code 0xe0 */

/* keyboard function table Index */
#undef SH		/* conflicts with SH CPU type */

#define AS	0	/* normal character index */
#define SH	1	/* shift index */
#define CN	2	/* control index */
#define NM	3	/* numeric lock index */
#define CP	4	/* capslock index */
#define ST	5	/* stop output index */
#define EX	6	/* extended code index */
#define ES	7	/* escape and extended code index */

/* Keyboard special key flags */

#define NORMAL		0x0000		/* normal key */
#define STP		0x0001  	/* capslock flag */
#define NUM		0x0002  	/* numeric lock flag */
#define CAPS		0x0004  	/* scroll lock stop output flag */
#define SHIFT		0x0008  	/* shift flag */
#define CTRL		0x0010  	/* control flag */
#define EXT		0x0020  	/* extended scan code 0xe0 */
#define ESC		0x0040  	/* escape key press */
#define	EW		EXT|ESC 	/* escape and Extend */
#define E1		0x0080  	/* extended scan code with 0xE1 */
#define PRTSC		0x0100  	/* print screen flag */
#define BRK		0x0200		/* make break flag for keyboard */

/* keyboard  on off defines */

#define	K_ON  		0xff	/* key  */
#define	K_OFF  		0x00	/* key  */

#endif /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbI8042Kbdh */
