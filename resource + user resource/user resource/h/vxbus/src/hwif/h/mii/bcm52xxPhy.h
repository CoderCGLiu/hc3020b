/* bcm52xxphy.h - register definitions for the BCM52xx PHY chips */

/*
 * Copyright (c) 2005-2006 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01c,30aug06,wap  Correct some macro names, add definition for high power
                 transmit mode
01b,17jan06,wap  Add ID for BCM5222
01a,15sep05,wap  written
*/

#ifndef __INCbcm52xxPhyh
#define __INCbcm52xxPhyh

IMPORT void bmtPhyRegister (void);

#define MII_OUI_BROADCOM        0x001018        /* Broadcom Corporation */
#define MII_OUI_xxBROADCOM      0x000818        /* Broadcom Corporation */  

#define MII_MODEL_BROADCOM_BCM5201      0x0021
#define MII_MODEL_BROADCOM_BCM5221      0x001e
#define MII_MODEL_BROADCOM_BCM5222      0x0032

/*
 * The following constants describe the vendor-specific registers
 * in the Broadcom BCM5201 and BCM5202 10/100 PHY chips. Registers
 * 0-15 are the same as defined in the MII spec. These also seem
 * to work with other BCM52xx chips, including the BCM5222.
 */

/* Auxiliary control register */

#define BMT_ACTL	16

#define BMT_ACTL_TXDIS		0x2000	/* transmitter disable */
#define BMT_ACTL_4B5B_BYPASS	0x0400	/* bypass 4b5b encoder */
#define BMT_ACTL_SCR_BYPASS	0x0200	/* bypass scrambler */
#define BMT_ACTL_NRZI_BYPASS	0x0100	/* bypass NRZI encoder */
#define BMT_ACTL_RXALIGN_BYPASS	0x0080	/* bypass rx symbol alignment */
#define BMT_ACTL_BASEWANDER_DIS	0x0040	/* disable baseline wander correction */
#define BMT_ACTL_FEF_EN		0x0020	/* far-end fault enable */

/* Auxiliary statur register */

#define BMT_ASTS	17

#define BMT_ASTS_FX_MODE	0x0400	/* 100base-FX mode (strap pin) */
#define BMT_ASTS_LOCKED		0x0200	/* descrambler locked */
#define BMT_ASTS_100BASE_LINK	0x0100	/* 1 = 100base link */
#define BMT_ASTS_REMFAULT	0x0080	/* remote fault */
#define BMT_ASTS_DISCON_STATE	0x0040	/* disconnect state */
#define BMT_ASTS_FCARDET	0x0020	/* false carrier detected */
#define BMT_ASTS_BAD_ESD	0x0010	/* bad ESD detected */
#define BMT_ASTS_RXERROR	0x0008	/* Rx error detected */
#define BMT_ASTS_TXERROR	0x0004	/* Tx error detected */
#define BMT_ASTS_LOCKERROR	0x0002	/* lock error detected */
#define BMT_ASTS_MLT3ERROR	0x0001	/* MLT3 code error detected */

/* 100base-X Rx error counter */

#define BMT_RXERRCNT	18

#define BMT_RXERRCNT_MASK	0x00ff

/* 100base-X false carrier counter */

#define BMT_FCSCNT	19

#define BMT_FCSCNT_MASK		0x00ff

/* 100base-X disconnect counter */

#define BMT_DISCNT	20

#define BMT_DISCTN_MASK		0x00ff

/* PHY test */

#define BMT_PTEST	23

/* Auxiliary control/status register */

#define BMT_ACSR	24

#define BMT_ACSR_JABBER_DIS	0x8000	/* jabber disable */
#define BMT_ACSR_FLINK		0x4000	/* force 10baseT link pass */
#define BMT_ACSR_TXPWR		0x0100	/* 10Mbps tx power: 0 = low, 1 = hi */
#define BMT_ACSR_HSQ		0x0080	/* SQ high */
#define BMT_ACSR_LSQ		0x0040	/* SQ low */
#define BMT_ACSR_ER1		0x0020	/* edge rate 1 */
#define BMT_ACSR_ER0		0x0010	/* edge rate 0 */
#define BMT_ACSR_ANEG		0x0008	/* auto-negotiation activated */
#define BMT_ACSR_F100		0x0004	/* force 100base */
#define BMT_ACSR_SPEED		0x0002	/* 1 = 100, 0 = 10 */
#define BMT_ACSR_FDX		0x0001	/* full-duplex */

/* Auxiliary status summary */

#define BMT_ASS		25

#define BMT_ASS_ACOMP		0x8000	/* auto-negotiation complete */
#define BMT_ASS_ACOMP_ACK	0x4000	/* auto-negotiation compl. ack */
#define BMT_ASS_AACK_DET	0x2000	/* auto-neg. ack detected */
#define BMT_ASS_ANLPAD		0x1000	/* auto-neg. link part. ability det */
#define BMT_ASS_ANEG_PAUSE	0x0800	/* pause operation bit */
#define BMT_ASS_HCD		0x0700	/* highest common denominator */
#define BMT_ASS_HCD_NONE	0x0000	/*    none */
#define BMT_ASS_HCD_10T		0x0100	/*    10baseT */
#define BMT_ASS_HCD_10T_FDX	0x0200	/*    10baseT-FDX */
#define BMT_ASS_HCD_100TX	0x0300	/*    100baseTX-FDX */
#define BMT_ASS_HCD_100T4	0x0400	/*    100baseT4 */
#define BMT_ASS_HCD_100TX_FDX	0x0500	/*    100baseTX-FDX */
#define BMT_ASS_PDF		0x0080	/* parallel detection fault */
#define BMT_ASS_LPRF		0x0040	/* link partner remote fault */
#define BMT_ASS_LPPR		0x0020	/* link partner page received */
#define BMT_ASS_LPANA		0x0010	/* link partner auto-neg able */
#define BMT_ASS_SPEED		0x0008	/* 1 = 100, 0 = 10 */
#define BMT_ASS_LINK		0x0004	/* link pass */
#define BMT_ASS_ANEN		0x0002	/* auto-neg. enabled */
#define BMT_ASS_JABBER		0x0001	/* jabber detected */

/* Interrupt register */

#define BMT_INTR	26
#define BMT_INTR_FDX_LED	0x8000	/* full-duplex led enable */
#define BMT_INTR_BMT_INTR_EN	0x4000	/* interrupt enable */
#define BMT_INTR_FDX_MASK	0x0800	/* full-dupled intr mask */
#define BMT_INTR_SPD_MASK	0x0400	/* speed intr mask */
#define BMT_INTR_LINK_MASK	0x0200	/* link intr mask */
#define BMT_INTR_BMT_INTR_MASK	0x0100	/* master interrupt mask */
#define BMT_INTR_FDX_CHANGE	0x0008	/* full-duplex change */
#define BMT_INTR_SPD_CHANGE	0x0004	/* speed change */
#define BMT_INTR_LINK_CHANGE	0x0002	/* link change */
#define BMT_INTR_BMT_INTR_STS	0x0001	/* interrupt status */

/* Auliliary mode 2 */

#define BMT_AMODE2	27

#define BMT_AMODE2_BLOCK_RXDV	0x0200	/* block RXDV mode enabled */
#define BMT_AMODE2_ANPDQ	0x0100	/* auto-neg parallel detection Q mode */
#define BMT_AMODE2_TRAFFIC_LED	0x0040	/* traffic meter led enable */
#define BMT_AMODE2_FXMTRCV_LED	0x0020	/* force Tx and Rx LEDs */
#define BMT_AMODE2_HS_TOKEN	0x0010	/* high-speed token ring mode */
#define BMT_AMODE2_AUTO_LP	0x0008	/* auto low-power mode */
#define BMT_AMODE2_TWOLINK_LED	0x0004	/* two link LEDs */
#define BMT_AMODE2_SQE_DIS	0x0002	/* disable SQE pulse */

/* Auxiliary error */

#define	BMT_AERR	28

#define	BMT_AERR_MANCHESTER	0x0400	/* Manchester code error */
#define	BMT_AERR_EOF		0x0200	/* EOF detection error */
#define	BMT_AERR_POLARITY	0x0100	/* polarity inversion */
#define	BMT_AERR_ANEG		0x0008	/* autonegotiation enabled */
#define	BMT_AERR_F100		0x0004	/* force 100base */
#define	BMT_AERR_SPEED		0x0002	/* 1 = 100, 0 = 10 */
#define	BMT_AERR_FDX		0x0001	/* full-duplex */

/* Auxiliary mode */

#define	BMT_AMODE	29

#define	BMT_AMODE_ACT_LED_DIS	0x0010	/* activity LED disable */
#define	BMT_AMODE_LINK_LED_DIS	0x0008	/* link LED disable */
#define	BMT_AMODE_BLOCK_TXEN	0x0002	/* enable block TXEN */


/* Auxiliary multiple phy register */

#define	BMT_AMPHY	30

#define	BMT_AMPHY_HCD_TX_FDX	0x8000	/* res. is 100baseTX-FDX */
#define	BMT_AMPHY_HCD_T4	0x4000	/* res. is 100baseT4 */
#define	BMT_AMPHY_HCD_TX	0x2000	/* res. is 100baseTX */
#define	BMT_AMPHY_HCD_10T_FDX	0x1000	/* res. is 10baseT-FDX */
#define	BMT_AMPHY_HCD_10T	0x0800	/* res. is 10baseT */
#define	BMT_AMPHY_RES_ANEG	0x0100	/* restart auto-negotiation */
#define	BMT_AMPHY_ANEG_COMP	0x0080	/* auto-negotiation complete */
#define	BMT_AMPHY_ACK_COMP	0x0040	/* acknowledge complete */
#define	BMT_AMPHY_ACK_DET	0x0020	/* acknowledge detected */
#define	BMT_AMPHY_ABILITY_DET	0x0010	/* waiting for LP ability */
#define	BMT_AMPHY_SUPER_ISO	0x0008	/* super-isolate mode */
#define	BMT_AMPHY_10T_SERIAL	0x0002	/* 10baseT serial mode */

#endif /* __INCbcm52xxPhyh */
