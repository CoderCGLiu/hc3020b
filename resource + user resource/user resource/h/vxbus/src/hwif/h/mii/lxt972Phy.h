/* lxt972phy.h - register definitions for the LXT971/LXT972 phy chips */

/*
 * Copyright (c) 2005, 2006, 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01c,24mar13,fao  Correct LXT_RISETIME_4_0NS macro.(WIND00414538)
01b,24aug06,wap  Add support for 8260Atm on-board PHY
01a,15sep05,wap  written
*/

#ifndef __INClxt972Phyh
#define __INClxt972Phyh

IMPORT void lxtPhyRegister (void);

#define MII_OUI_LEVEL1  0x00207b        /* Level 1 */  
/* Level 1 is completely different - from right to left.
        (Two bits get lost in the third OUI byte.) */
#define MII_OUI_xxLEVEL1        0x1e0400        /* Level 1 */  

#define MII_MODEL_xxLEVEL1_LXT970       0x0000

#define MII_OUI_8260ATM		0x0004de
#define MII_MODEL_8260ATM	0x000e
/*
 * The following constants describe the vendor-specific registers
 * in the Intel LXT972 PHY. Registers 0-15 are the same as defined
 * in the MII spec. Note that the LXT971 is functionally equivalent
 * to the LXT972 and hence uses the same register layout. Also note
 * that the LXT970 is _not_ equivalent: its register layout is totally
 * different. I believe this chip is out of production, however.
 */

/* Configuration register */

#define LXT_CR		16

#define LXT_CR_FORCELNK		0x4000	/* force link pass */
#define LXT_CR_TXDIS		0x2000	/* disable transmit */
#define LXT_CR_SCRAM		0x1000	/* bypass scrambler/descrambler */
#define LXT_CR_JABDIS		0x0400	/* disable jabber detection */
#define LXT_CR_SQE		0x0200	/* enable heartbeat */
#define LXT_CR_TPLOOPDIS	0x0100	/* disable loopback in half-duplex */
#define LXT_CR_CRS		0x0080	/* CRS select */
#define LXT_CR_PREAMBLE		0x0020	/* preamble enable */
#define LXT_CR_ALTNP		0x0002	/* alternate next-page feature */

/* Status register */

#define LXT_STS		17

#define LXT_STS_SPD100		0x4000	/* 1 == 100Mbps, 0 == 10Mbps */
#define LXT_STS_TX		0x2000	/* Transmission in progress */
#define LXT_STS_RX		0x1000	/* Reception in progress */
#define LXT_STS_COLL		0x0800	/* Collision in progress */
#define LXT_STS_LINK		0x0400	/* 1 == link up, 0 == link down */
#define LXT_STS_FDX		0x0200	/* 1 == FDX, 0 == HDX */
#define LXT_STS_ANEG		0x0100	/* Autoneg in progress */
#define LXT_STS_ANEGDONE	0x0080	/* Autoneg complete */
#define LXT_STS_POLARITY	0x0020	/* 1 == polarity reversed */
#define LXT_STS_PAUSE		0x0010	/* 1 == pause capable */
#define LXT_STS_ERROR		0x0008	/* 1 == remote fault */

/* Interrupt mask register */

#define LXT_IMR		18

#define LXT_IMR_ANEG		0x0080	/* 1 == aneg done int enabled */
#define LXT_IMR_SPD		0x0040	/* 1 == speed change int enabled */
#define LXT_IMR_DPX		0x0020	/* 1 == duplex change int enabled */
#define LXT_IMR_LNK		0x0010	/* 1 == link change int enabled */
#define LXT_IMR_INTEN		0x0002	/* 1 == interrupts enabled */
#define LXT_IMR_TINT		0x0001	/* 1 == trigger an intterupt */

/*
 * Interrupt status register
 * Event bits are 'read to clear.'
 */

#define LXT_ISR		19

#define LXT_ISR_ANEG		0x0080	/* 1 == aneg done int fired */
#define LXT_ISR_SPD		0x0040	/* 1 == speed change int fired */
#define LXT_ISR_DPX		0x0020	/* 1 == duplex change int fired */
#define LXT_ISR_LNK		0x0010	/* 1 == link change int fired */
#define LXT_ISR_MDINT		0x0004	/* 1 == MII interrupt pending */

/* LED configuration register */

#define LXT_LEDCFG	20

#define LXT_LEDCFG_LED1		0xF000	/* LED1 configration */
#define LXT_LEDCFG_LED2		0x0F00	/* LED2 configuration */
#define LXT_LEDCFG_LED3		0x00F0	/* LED3 configuration */
#define LXT_LEDCFG_FREQ		0x000C	/* freq config */
#define LXT_LEDCFG_PULSESTRETCH	0x0002	/* enable pulse stretching */

#define LXT_LEDFREQ_30MS	0x0000	/* stretch to 30 ms */
#define LXT_LEDFREQ_60MS	0x0004	/* stretch to 60 ms */
#define LXT_LEDFREQ_100MS	0x0008	/* stretch to 100 ms */
#define LXT_LEDFREQ_RSVD	0x000C	/* reserved */

#define LXT_LED_SPD		0	/* display speed status */
#define LXT_LED_TX		1	/* display transmit status */
#define LXT_LED_RX		2	/* display receive status */
#define LXT_LED_COLL		3	/* display collision status */
#define LXT_LED_LNK		4	/* display link status */
#define LXT_LED_DPX		5	/* display duplex status */
#define LXT_LED_UNUSED1		6
#define LXT_LED_ACT		7	/* display RX or TX activity */
#define LXT_LED_ON		8	/* turn LED on */
#define LXT_LED_OFF		9	/* turn LED off */
#define LXT_LED_BLINKFAST	10	/* make LED blink quickly */
#define LXT_LED_BLINKSLOW	11	/* make LED blink slowly */
#define LXT_LED_LNK_RX		12	/* combined link and RX status */
#define LXT_LED_LNK_ACT		13	/* combined link and activity status */
#define LXT_LED_DPX_COLL	14	/* combine duplex and collision sts */
#define LXT_LED_UNUSED2		15

#define LXT_LED1(x)		((x) << 12)
#define LXT_LED2(x)		((x) << 8)
#define LXT_LED3(x)		((x) << 4)

/* Digital configuration register */

#define LXT_DIGCFG	26

#define LXT_DIGCFG_MIIDRIVE	0x0800	/* 1 == increased MII drive strength */
#define LXT_DIGCFG_SYMERR	0x0200	/* 1 == map symbol error to RXER */

/* Transmit control register #2 */

#define LXT_TXCTL	30

#define LXT_TXCTL_LOWPWR	0x1000	/* 1 == low pwr, 0-differential TX */
#define LXT_TXCTL_RISETIME	0x0C00	/* port rise time control */

#define LXT_RISETIME_3_0NS	0x0000	/* risetime == 3.0ns */
#define LXT_RISETIME_3_4NS	0x0400	/* risetime == 3.4ns */
#define LXT_RISETIME_3_9NS	0x0800	/* risetime == 3.9ns */
#define LXT_RISETIME_4_0NS	0x0C00	/* risetime == 4.0ns */

#endif /* __INClxt972Phyh */
