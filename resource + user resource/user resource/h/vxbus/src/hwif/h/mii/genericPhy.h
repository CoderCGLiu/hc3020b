/* genericPhy.h - register definitions for generic PHY chips */

/*
 * Copyright (c) 2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,01jun06,wap  Fix compiler warning
01a,15sep05,wap  written
*/

#ifndef __INCgenericPhyh
#define __INCgenericPhyh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void genPhyRegister (void);

#ifdef __cplusplus
}
#endif

#endif /* __INCgenericPhyh */
