/* vxbRmiMdio.h - header file for Raza Micro MDIO driver */

/*
 * Copyright (c) 2009 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,22may09,wap  written
*/

#ifndef __INCvxbRmiMdioh
#define __INCvxbRmiMdioh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void rmRegister (void);

#ifndef BSP_VERSION

#define TM_TIMEOUT 10000

/* Private instance context */

typedef struct rm_drv_ctrl
    {
    VXB_DEVICE_ID	rmDev;
    void *		rmBar;
    void *		rmHandle;
    SEM_ID		rmMiiSem;
    UINT32 *		rmShared;
    UINT32		rmDummy;
    } RM_DRV_CTRL;

#undef CSR_READ_4
#undef CSR_WRITE_4
#undef CSR_SETBIT_4
#undef CSR_CLRBIR_4

#define RM_BAR(p)   ((RM_DRV_CTRL *)(p)->pDrvCtrl)->rmBar
#define RM_HANDLE(p)   ((RM_DRV_CTRL *)(p)->pDrvCtrl)->rmHandle

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (RM_HANDLE(pDev), (UINT32 *)((char *)RM_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (RM_HANDLE(pDev),                             \
        (UINT32 *)((char *)RM_BAR(pDev) + addr), data)

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbRmiMdioh */
