/* vxbDummyMdio.h - header file for dummy MII MDIO driver */

/*
 * Copyright (c) 2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,21mar08,z_l  written
*/

#ifndef __INCvxbDummyMdioh
#define __INCvxbDummyMdioh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void dummyMdioRegister (void);

/* Private instance context */

typedef struct dummy_mdio_drv_ctrl
    {
    VXB_DEVICE_ID  dummyDev;
    UINT32         fullDuplex;
    UINT32         speed;
    } DUMMY_MDIO_DRV_CTRL;

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCvxbDummyMdioh */

