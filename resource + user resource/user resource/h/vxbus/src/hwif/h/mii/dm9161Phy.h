/* dm9161Phy.h - Davicom DM9191 PHY register description */

/*
 * Copyright (c) 2005-2006, 2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01c,18may08,x_s  Add MII_MODEL_DAVICOM_DM9161A
01b,06feb06,wap  Add model number for 9161E part
01a,15sep05,wap  written
*/

#ifndef __INCdm9161Phyh
#define __INCdm9161Phyh

IMPORT void dmPhyRegister (void);

#define MII_OUI_DAVICOM 0x00606e        /* Davicom Semiconductor */

/* Don't know what's going on here. */
#define MII_OUI_xxDAVICOM       0x006040        /* Davicom Semiconductor */

/* Davicom Semiconductor PHYs */
#define MII_MODEL_xxDAVICOM_DM9101      0x0000
#define MII_MODEL_DAVICOM_DM9161        0x0008
#define MII_MODEL_DAVICOM_DM9161A       0x000a
/*
 * The following constants describe the vendor-specific registers
 * in the Davicom DM9161(A) PHY. Registers 0-15 are the same as defined
 * in the MII spec.
 */

/* Specified configuration register */

#define DAVICOM_SCR	16

#define DAVICOM_SCR_BP_4B5B	0x8000	/* bypass 4B5B encoding/decoding */
#define DAVICOM_SCR_BP_SCRAM	0x4000	/* bypass scrambler/descrambler */
#define DAVICOM_SCR_BP_ALIGN	0x2000	/* bypass symbol alignment */
#define DAVICOM_SCR_BP_ADPOK	0x1000	/* bypass ADPOK */
#define DAVICOM_SCR_REPEATER	0x0800	/* repeater mode enable */
#define DAVICOM_SCR_TX		0x0400	/* 1 = 100baseTX, 0 = 100baseFX */
#define DAVICOM_SCR_FEF		0x0200	/* far end fault enable */
#define DAVICOM_SCR_RMII	0x0100	/* reduced MII enable */
#define DAVICOM_SCR_FORCE100	0x0080	/* force good 100Mbps link */
#define DAVICOM_SCR_SPDLED	0x0040	/* speed LED enable */
#define DAVICOM_SCR_COLLED	0x0020	/* collision LED enable */
#define DAVICOM_SCR_RPWREN	0x0010	/* reduced power down enable */
#define DAVICOM_SCR_SMRST	0x0008	/* reset state machine */
#define DAVICOM_SCR_MFPSC	0x0004	/* management preamble suppression */
#define DAVICOM_SCR_SLEEP	0x0002	/* sleep mode */
#define DAVICOM_SCR_RLOUT	0x0001	/* remote loopout control */

/* Specified config & status register */

#define DAVICOM_SCSR	17

#define DAVICOM_SCSR_100FDX	0x8000	/* current mode is 100Mbps FDX */
#define DAVICOM_SCSR_100HDX	0x4000	/* current mode is 100Mbps HDX */
#define DAVICOM_SCSR_10FDX	0x2000	/* current mode is 10Mbps FDX */
#define DAVICOM_SCSR_10HDX	0x1000	/* current mode is 10Mbps HDX */
#define DAVICOM_SCSR_PHYADDR	0x01D0
#define DAVICOM_SCSR_ANEGMON	0x0008	/* autoneg monitor */

#define DAVICOM_ANEG_DONE_OK            0x8
#define DAVICOM_ANEG_PARALLEL_NOSIGNAL  0x7
#define DAVICOM_ANEG_PARALLEL_SIGNAL    0x6
#define DAVICOM_ANEG_CONSISTENCY_FAIL   0x5
#define DAVICOM_ANEG_CONSISTENCY_OK     0x4
#define DAVICOM_ANEG_ACK_MATCH_FAIL     0x3
#define DAVICOM_ANEG_ACK_MATCH_OK       0x2
#define DAVICOM_ANEG_ABILITY_MATCH      0x1
#define DAVICOM_ANEG_IDLE               0x0

/* 10baseT config/status register */

#define DAVICOM_10BTCSR	18

#define DAVICOM_10BTCSR_LPEN	0x4000	/* enable link pulse transmission */
#define DAVICOM_10BTCSR_HBE	0x2000	/* heartbeat enable */
#define DAVICOM_10BTCSR_SQUELCH	0x1000	/* squelch enable */
#define DAVICOM_10BTCSR_JABEN	0x0800	/* jabber detect enable */
#define DAVICOM_10BTCSR_10BTSER	0x0400	/* 0 = 10BT MII, 1 == 10BT GPSI */
#define DAVICOM_10BTCSR_POLR	0x0001	/* polarity reversal indicator */

/* Interrupt register */

#define DAVICOM_INT	21

#define DAVICOM_INT_IPEND	0x8000	/* interrupt pending */
#define DAVICOM_INT_FDXM	0x0800	/* FDX change mask */
#define DAVICOM_INT_SPDM	0x0400	/* speed change mask */
#define DAVICOM_INT_LINKM	0x0200	/* link change mask */
#define DAVICOM_INT_INTRM	0x0100	/* all intr mask */
#define DAVICOM_INT_FDX		0x0010	/* FDX change event */
#define DAVICOM_INT_SPD		0x0008	/* speed change event */
#define DAVICOM_INT_LINK	0x0004	/* link change event */
#define DAVICOM_INT_ISTS	0x0001	/* interrupt status */

/* RX error count register */

#define DAVICOM_RXERR	22

/* Disconnect count register */

#define DAVICOM_DISCCNT	23

#define DAVICOM_DISCCNT_CNT	0x00FF

/* Hardware reset latch state register */

#define DAVICOM_HWLATCH	24

#define DAVICOM_HWLATCH_LEDST	0x2000
#define DAVICOM_HWLATCH_CSTS	0x1000
#define DAVICOM_HWLATCH_RMII	0x0800
#define DAVICOM_HWLATCH_SCRAM	0x0400
#define DAVICOM_HWLATCH_RPTR	0x0200
#define DAVICOM_HWLATCH_TSTMOD	0x0100
#define DAVICOM_HWLATCH_OP2	0x0080
#define DAVICOM_HWLATCH_OP1	0x0040
#define DAVICOM_HWLATCH_OP0	0x0020

#endif /* __INCdm9161Phyh */
