/* mv88E1113phy.h - register definitions for the Marvell 88E1113 fiber PHY */

/*
 * Copyright (c) 2007 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,30jan07,wap  Correct function name
01a,04jan07,wap  written
*/

#ifndef __INCmv88E1113Phyh
#define __INCmv88E1113Phyh

IMPORT void mvfPhyRegister (void);

#define MVF_DEFMODE_DEFAULT	0	/* Use power up default */
#define MVF_DEFMODE_100FX	1	/* Force 100baseFX mode */
#define MVF_DEFMODE_SGMII	2	/* Force SGMII mode */
#define MVF_DEFMODE_1000X	3	/* Force 1000baseX only mode */

#define MII_OUI_MARVELL 0x005043        /* Marvell Semiconductor */

#define MII_MODEL_MARVELL_E1113 0x0009

/*
 * The following constants describe the vendor-specific registers
 * in the Marvell 88E1113 fiber gigabit PHY chips. Marvell PHYs
 * tend to have multiple register pages. The 88E1113 is no exception.
 * We list the registers in page groups, starting with page 1.
 */

/* Autoneg advertisement register, page 1 */

#define MVF_ANAR		4

#define MVF_ANAR_NP		0x8000	/* Next page */
#define MVF_ANAR_RF		0x3000	/* Remote fault */
#define MVF_ANAR_FC		0x0180	/* Flow control */
#define MVF_ANAR_1000_HD	0x0040	/* 1000baseSX/HDX capable */
#define MVF_ANAR_1000_FD	0x0020	/* 1000baseSX/FDX capable */

/* Autoneg advertisement register, page 1 */

#define MV_ALNPAR		5

#define MVF_ANLPAR_NP		0x8000	/* Next page */
#define MVF_ANLPAR_RF		0x3000	/* Remote fault */
#define MVF_ANLPAR_FC		0x0180	/* Flow control */
#define MVF_ANLPAR_1000_HD	0x0040	/* 1000baseSX/HDX capable */
#define MVF_ANLPAR_1000_FD	0x0020	/* 1000baseSX/FDX capable */


/* Fiber specific control register, page 1 */

#define MVF_FSPEC		16
#define MVF_FSPEC_FEFI			0x0800
#define MVF_FSPEC_FORCELINK		0x0400
#define MVF_FSPEC_SIGDET_POLARITY	0x0200
#define MVF_FSPEC_FIBER_TX_DISABLE	0x0008

/* Mac specific control register, page 2 */

#define MVF_MSPEC		16

#define MVF_MSPEC_TX_FIFO_DEPTH		0xC000
#define MVF_MSPEC_RX_FIFO_DEPTH		0x3000
#define MVF_MSPEC_MODE			0x0380
#define MVF_MSPEC_SGMII_PDOWN		0x0008
#define MVF_MSPEC_SGMII_ENHANCED	0x0004

#define MVF_TXFIFO_8_16			0x0000
#define MVF_TXFIFO_12_24		0x4000
#define MVF_TXFIFO_16_32		0x8000
#define MVF_TXFIFO_20_40		0xC000

#define MVF_RXFIFO_8_16			0x0000
#define MVF_RXFIFO_12_24		0x1000
#define MVF_RXFIFO_16_32		0x2000
#define MVF_RXFIFO_20_40		0x3000

#define MVF_MODE_100BASEFX		0x0000
#define MVF_MODE_SGMII			0x0300
#define MVF_MODE_1000BASEX		0x0380


/* PHY specific status register, page 1 */

#define MVF_STS		17

#define MVF_STS_SPEED		0xC000	/* current link speed, 100baseFX */
#define MVF_STS_DUPLEX		0x2000	/* current duplex state */
#define MVF_STS_PGRX		0x1000	/* page received */
#define MVF_STS_SPDDPX_OK	0x0800	/* speed and duplex resolved */
#define MVF_STS_LINK		0x0400	/* link status */
#define MVF_STS_TXPAUSE		0x0200	/* TX pause enabled */
#define MVF_STS_RXPAUSE		0x0100	/* RX pause enabled */
#define MVF_STS_SGMII_1000_SYNC	0x0040	/* 1 = synced, 0 = no sync */
#define MVF_STS_ANEG_BYPASSED	0x0020	/* 1 = bypass timer expired */
#define MVF_STS_SIGLOSS		0x0010	/* loss of signal */

#define MVF_SPEED_100		0x4000
#define MVF_SPEED_1000		0x0000

/* Interrupt mask register, set a bit to enable the interrupt, page 1.  */

#define MVF_IMR		18

#define MVF_IMR_DUPLEX		0x2000	/* duplex changed */
#define MVF_IMR_PGRX		0x1000	/* page received */
#define MVF_IMR_ANEGDONE	0x0800	/* autoneg completed */
#define MVF_IMR_LINK		0x0400	/* link status changed */
#define MVF_IMR_SYMERR		0x0200	/* symbol error */
#define MVF_IMR_FALSCAR		0x0100	/* false carrier */

/* Interrupt register, page 1 */

#define MVF_ISR		19

#define MVF_ISR_DUPLEX		0x2000	/* duplex changed */
#define MVF_ISR_PGRX		0x1000	/* page received */
#define MVF_ISR_ANEGDONE	0x0800	/* autoneg completed */
#define MVF_ISR_LINK		0x0400	/* link status changed */
#define MVF_ISR_SYMERR		0x0200	/* symbol error */
#define MVF_ISR_FALSCAR		0x0100	/* false carrier */

/* Page select register, visible in all pages */

#define MVF_PAGESEL	22

#endif /* __INCmv88E1113Phyh */
