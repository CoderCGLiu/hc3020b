/* geiTbiPhy.h - register definitions for Intel PRO/1000 TBI fiber ports */

/*
 * Copyright (c) 2007 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,08mar07,wap  written
*/

#ifndef __INCgeiTbiPhyh
#define __INCgeiTbiPhyh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void geiTbiPhyRegister (void);

#define GTBI_ID1	0x9191
#define GTBI_ID2	0x1919

/* Top half of TXCW register */

#define GTBI_TXCFG	16

#define GTBI_TXCFG_NP		0x8000	/* next page request */
#define GTBI_TXCFG_REMFAULT	0x3000	/* remote fault indication */
#define GTBI_TXCFG_PAUSE	0x0180	/* Pause */
#define GTBI_TXCFG_1000HD	0x0040	/* 1000Mbps half duplex */
#define GTBI_TXCFG_1000FD	0x0020	/* 1000Mbps full duplex */

/* Bottom half of TXCW register */

#define GTBI_TXCTL	17

#define GTBI_TXCTL_ANE		0x8000	/* autoneg enable */
#define GTBI_TXCTL_CFGCTL	0x4000	/* TX cfg control bit */

/* Top half of RXCW register */

#define GTBI_RXCFG	18

#define GTBI_RXCFG_NP		0x8000	/* next page request */
#define GTBI_RXCFG_REMFAULT	0x3000	/* remote fault indication */
#define GTBI_RXCFG_PAUSE	0x0180	/* Pause */
#define GTBI_RXCFG_1000HD	0x0040	/* 1000Mbps half duplex */
#define GTBI_RXCFG_1000FD	0x0020	/* 1000Mbps full duplex */

/* Bottom half of RXCW register */

#define GTBI_RXCTL	19

#define GTBI_RXCTL_ANC		0x8000	/* Autoneg complete */
#define GTBI_RXCTL_SYNCHED	0x4000	/* 1 = sync, 0 = lost sync */
#define GTBI_RXCTL_CFG		0x2000	/* received /C/ ordered sets */
#define GTBI_RXCTL_CFGCHANGE	0x1000	/* RX config word changed */
#define GTBI_RXCTL_CFGINVALID	0x0800	/* RX configuration invalid */
#define GTBI_RXCTL_CFGNOCARR	0x0400	/* No carrier */

#ifdef __cplusplus
}
#endif

#endif /* __INCgeiTbiPhyh */
