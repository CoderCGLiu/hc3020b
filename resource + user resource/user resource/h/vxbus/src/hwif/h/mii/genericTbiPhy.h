/* genericTbiPhy.h - register definitions for generic TBI support */

/*
 * Copyright (c) 2007, 2012-2013, 2015 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
24jul15,d_l  add TBI_ANAR_PAUSE_SHIFT. (VXW6-84701)
01c,16jan13,wap  Add SGMII mode definitions to TBI_ANAR register
01b,26oct12,y_c  add TBI_ANAR register define. (WIND00316241)
01a,14jul07,wap  written
*/

#ifndef __INCgenericTbiPhyh
#define __INCgenericTbiPhyh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void genTbiPhyRegister (void);

#define TBI_ID1			0x1212
#define TBI_ID2			0x2121

#define TBI_CR			0

#define TBI_CR_1000             0x0040          /* 1 = 1000mb */
#define TBI_CR_FDX              0x0100          /* FDX =1, half duplex =0 */
#define TBI_CR_RESTART          0x0200          /* restart auto negotiation */
#define TBI_CR_ISOLATE          0x0400          /* isolate PHY from MII */
#define TBI_CR_POWER_DOWN       0x0800          /* power down */
#define TBI_CR_AUTO_EN          0x1000          /* auto-negotiation enable */
#define TBI_CR_LOOPBACK         0x4000          /* 0 = normal, 1 = loopback */
#define TBI_CR_RESET            0x8000          /* 0 = normal, 1 = PHY reset */

#define TBI_SR			1

#define TBI_SR_EXT_AB		0x0001		/* extended ability capable */
#define TBI_SR_LINK_STATUS      0x0004          /* link Status -- 1 = link */
#define TBI_SR_AUTO_SEL         0x0008          /* auto speed select capable */
#define TBI_SR_REMOTE_FAULT     0x0010          /* Remote fault detect */
#define TBI_SR_AUTO_NEG         0x0020          /* auto negotiation complete */
#define TBI_SR_NO_PRE           0x0100          /* preamble supression cap */
#define TBI_SR_EXT_STS          0x0100          /* extended sts in reg 15 */

#define TBI_ANAR		4

#define TBI_ANAR_NP             0x8000		/* next page request */
#define TBI_ANAR_REMFAULT       0x3000		/* remote fault indication */
#define TBI_ANAR_PAUSE          0x0180		/* Pause */
#define TBI_ANAR_1000HD         0x0040		/* 1000Mbps half duplex */
#define TBI_ANAR_1000FD         0x0020		/* 1000Mbps full duplex */
#define TBI_ANAR_SGMII          0x0001		/* SGMII mode */
#define TBI_ANAR_ACK            0x4000		/* ACK */
#define TBI_ANAR_PAUSE_SHIFT    (7)

#define TBI_ANLPAR		5

#define TBI_ANLPAR_NP           0x8000		/* next page request */
#define TBI_ANLPAR_ACK          0x4000		/* acknowledge */
#define TBI_ANLPAR_REMFAULT     0x3000		/* remote fault indication */
#define TBI_ANLPAR_PAUSE        0x0180		/* Pause */
#define TBI_ANLPAR_1000HD       0x0040		/* 1000Mbps half duplex */
#define TBI_ANLPAR_1000FD       0x0020		/* 1000Mbps full duplex */

#ifdef __cplusplus
}
#endif

#endif /* __INCgenericTbiPhyh */
