/* mdioDrv.h - header file for generic MII MDIO driver */

/*
 * Copyright (c) 2006 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,16jul07,wap  Switch to new register access API (WIND00100585)
01a,04jul06,wap  written
*/

#ifndef __INCmdioDrvh
#define __INCmdioDrvh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void mdioRegister (void);
IMPORT char * mdioEnable_desc;
IMPORT char * mdioDisable_desc;

#ifndef BSP_VERSION

/* Private instance context */

typedef struct mdio_drv_ctrl
    {
    VXB_DEVICE_ID	mdioDev;
    void *		mdioHandle;
    VXB_DEVICE_ID	mdioMiiBus;
    SEM_ID		mdioMiiSem;
    UINT                mdioClkDirInBit;
    UINT		mdioClkDirOutBit;
    UINT		mdioClkBit;
    UINT		mdioDataDirInBit;
    UINT		mdioDataDirOutBit;
    UINT		mdioDataInBit;
    UINT		mdioDataOutBit;
    UINT		mdioRegWidth;
    } MDIO_DRV_CTRL;

#define MDIO_CLK_DIR_IN_REG	0
#define MDIO_CLK_DIR_OUT_REG	1
#define MDIO_CLK_BIT_REG	2
#define MDIO_DATA_DIR_IN_REG	3
#define MDIO_DATA_DIR_OUT_REG	4
#define MDIO_DATA_IN_REG	5
#define MDIO_DATA_OUT_REG	6

LOCAL UINT64 csr_read_4
    ( 
    VXB_DEVICE_ID pDev,
    UINT32 addr
    )
    {
    UINT32 data;
    MDIO_DRV_CTRL * pDrvCtrl;

    if (pDev->pRegBase[addr] == 0)
        return (0);

    pDrvCtrl = pDev->pDrvCtrl;

    switch (pDrvCtrl->mdioRegWidth)
        {
        case 1:
            data = vxbRead8 (pDrvCtrl->mdioHandle, pDev->pRegBase[addr]);
            break;
        case 2:
            data = vxbRead16 (pDrvCtrl->mdioHandle, pDev->pRegBase[addr]);
            break;
#ifdef notdef
        case 8:
            data = vxbRead64 (pDrvCtrl->mdioHandle, pDev->pRegBase[addr]);
            break;
#endif
        case 4:
        default:
            data = vxbRead32 (pDrvCtrl->mdioHandle, pDev->pRegBase[addr]);
            break;
        }

    return (data);
    }

LOCAL void csr_write_4
    (
    VXB_DEVICE_ID pDev,
    UINT32 addr,
    UINT64 data
    )
    {
    UINT32 _data = data;
    MDIO_DRV_CTRL * pDrvCtrl;

    if (pDev->pRegBase[addr] == 0)
        return;

    pDrvCtrl = pDev->pDrvCtrl;

    switch (pDrvCtrl->mdioRegWidth)
        {
        case 1:
            vxbWrite8 (pDrvCtrl->mdioHandle, pDev->pRegBase[addr], data);
            break;
        case 2:
            vxbWrite16 (pDrvCtrl->mdioHandle, pDev->pRegBase[addr], data);
            break;
#ifdef notdef
        case 8:
            vxbWrite64 (pDrvCtrl->mdioHandle, pDev->pRegBase[addr], data);
            break;
#endif
        case 4:
        default:
            vxbWrite32 (pDrvCtrl->mdioHandle, pDev->pRegBase[addr], data);
            break;
        }

    return;
    }

#define CSR_READ csr_read_4
#define CSR_WRITE csr_write_4

#define CSR_SETBIT(pDev, offset, val)		\
        CSR_WRITE(pDev, offset, CSR_READ(pDev, offset) | (val))

#define CSR_CLRBIT(pDev, offset, val)		\
        CSR_WRITE(pDev, offset, CSR_READ(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCmdioDrvh */
