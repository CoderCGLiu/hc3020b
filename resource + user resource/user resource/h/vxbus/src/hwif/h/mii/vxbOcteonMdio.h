/* vxbOcteonMdio.h - header file for Octeon MDIO driver */

/*
 * Copyright (c) 2007, 2010 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01c,11oct10,d_c  Moved from Cavium BSPs (cav_*)
01b,13sep10,d_c  Add smiInterface resource to selecte SMI interface number.
01a,04jun07,wap  written
*/

#ifndef __INCmdioDrvh
#define __INCmdioDrvh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void octeonMdioRegister (void);

#ifndef BSP_VERSION

#define MDIO_TIMEOUT 10000

/* Private instance context */

typedef struct mdio_drv_ctrl
    {
    VXB_DEVICE_ID	mdioDev;
    VXB_DEVICE_ID	mdioMiiBus;
    SEM_ID		mdioMiiSem;
    int                 smiInterface;
    BOOL                mdioClause22;
    } MDIO_DRV_CTRL;

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCmdioDrvh */
