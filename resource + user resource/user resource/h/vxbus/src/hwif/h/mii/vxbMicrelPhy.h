/* vxbMicrelPhy.h - header file for micrel phy driver */

/*
 * Copyright (c) 2014 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
25aug14,m_w  written. (VXW6-83238)
*/

#ifndef __INCvxbMicrelPhyh
#define __INCvxbMicrelPhyh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void micrelPhyRegister (void);

/* PHY ID of Micrel KSZ9021 */

#define PHY_ID_KSZ9021                              0x00221610
#define PHY_ID_KSZ9021RLRN                          0x00221611 

/* Write/read to/from extended registers */
#define MII_KSZPHY_EXTREG                           0x0b
#define KSZPHY_EXTREG_WRITE                         0x8000

#define MII_KSZPHY_EXTREG_WRITE                     0x0c
#define MII_KSZPHY_EXTREG_READ                      0x0d

/* Extended registers */
#define MII_KSZ9021PHY_CLK_CONTROL_PAD_SKEW         0x104
#define MII_KSZ9021PHY_RX_DATA_PAD_SKEW             0x105
#define MII_KSZ9021PHY_TX_DATA_PAD_SKEW             0x106

#define MII_KSZ9021_RXC_PAD_SKEW_OFST               12
#define MII_KSZ9021_RXDV_PAD_SKEW_OFST              8
#define MII_KSZ9021_TXC_PAD_SKEW_OFST               4
#define MII_KSZ9021_TXEN_PAD_SKEW_OFST              0

#define MICREL_MII_OUI(id1, id2)                    (((id1) << 16) | (id2))

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCvxbMicrelPhyh */

