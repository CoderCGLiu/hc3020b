/* vxbIntelGcu.h - header file for Intel GCU driver */

/*
 * Copyright (c) 2007-2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,20aug08,b_m  add gcuMiiSem to INTEL_GCU_DRV_CTRL.
01a,15oct07,d_z  created
*/

#ifndef __INCvxbIntelGcuh
#define __INCvxbIntelGcuh

#ifdef __cplusplus
extern "C" {
#endif

#define INTEL_GCU_TIMEOUT               10000

#define INTEL_GCU_MDIO_STATUS_REG       0x10
#define INTEL_GCU_MDIO_CMD_REG          0x14

#define INTEL_GCU_MDIO_RD_ERR           (1 << 31)

#define INTEL_GCU_MDIO_GO               (1 << 31)
#define INTEL_GCU_MDIO_WR               (1 << 26)
#define INTEL_GCU_MDIO_WR_DATA_MSK      0xFFFF
#define INTEL_GCU_MDIO_RD_DATA_MSK      0xFFFF
#define INTEL_GCU_MDIO_ADDR_MSK         0x1F
#define INTEL_GCU_MDIO_ADDR_SHIFT       21
#define INTEL_GCU_MDIO_REG_MSK          0x1F
#define INTEL_GCU_MDIO_REG_SHIFT        16

enum
    {
    INTEL_GCU_VENDOR_ID  = 0x8086,
    INTEL_GCU_DEVICE_ID  = 0x503e
    };

typedef struct intelGcuDrvCtrl
    {
    VXB_DEVICE_ID   gcuDev;
    SEM_ID          gcuMiiSem;
    } INTEL_GCU_DRV_CTRL;

#define CSR_READ_4(pDev, addr)                              \
    *(volatile UINT32 *)((UINT32)pDev->pRegBase[0] + addr)

#define CSR_WRITE_4(pDev, addr, data)                       \
    do {                                                    \
        volatile UINT32 *pReg =                             \
            (UINT32 *)((UINT32)pDev->pRegBase[0] + addr);   \
        *(pReg) = (UINT32)(data);                           \
    } while ((0))

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCvxbIntelGcuh */
