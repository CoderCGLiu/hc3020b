/* uccMdio.h - header file for TSEC MDIO driver */

/*
 * Copyright (c) 2007, 2010 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,12dec10,y_y  add QE pins expose routine for p1021mds board
01a,29jun07,wap  written
*/

#ifndef __INCuccMdioh
#define __INCuccMdioh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void umRegister (void);

#ifndef BSP_VERSION

#define UM_TIMEOUT 10000

/* Private instance context */

typedef struct um_drv_ctrl
    {
    VXB_DEVICE_ID	umDev;
    void *		umBar;
    void *		umHandle;
    SEM_ID		umMiiSem;
    UINT32 *		umShared;
    UINT32		umDummy;
    VOIDFUNCPTR umMiiExpose;
    VOIDFUNCPTR umMiiRelease;
    } UM_DRV_CTRL;

#ifdef notdef
#define CSR_READ_4(pDev, addr)                                  \
        *(volatile UINT32 *)((UINT32)pDev->pRegBase[0] + addr)

#define CSR_WRITE_4(pDev, addr, data)                           \
        do {                                                    \
            volatile UINT32 *pReg =                             \
                (UINT32 *)((UINT32)pDev->pRegBase[0] + addr);   \
            *(pReg) = (UINT32)(data);                           \
            WRS_ASM("eieio");                                   \
        } while ((0))
#endif

#undef CSR_READ_4
#undef CSR_WRITE_4
#undef CSR_SETBIT_4
#undef CSR_CLRBIR_4

#define UM_BAR(p)   ((UM_DRV_CTRL *)(p)->pDrvCtrl)->umBar
#define UM_HANDLE(p)   ((UM_DRV_CTRL *)(p)->pDrvCtrl)->umHandle

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (UM_HANDLE(pDev), (UINT32 *)((char *)UM_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (UM_HANDLE(pDev),                             \
        (UINT32 *)((char *)UM_BAR(pDev) + addr), data)

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCuccMdioh */
