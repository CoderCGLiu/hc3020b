/* rtl8201Phy.h - register definitions for RealTek RTL8201 10/100 PHY chip */

/*
 * Copyright (c) 2006, 2014 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
09jul14,wyt  add PHY ID of RTL8211. (VXW6-82960)
01a,01jun06,wap  written
*/

#ifndef __INCrtl8201Phyh
#define __INCrtl8201Phyh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void rtlPhyRegister (void);

#define MII_OUI_REALTEK 0x000020        /* RealTek Semicondctor */
#define MII_OUI_REALTEK_1 0x000732

#define MII_MODEL_REALTEK_RTL8201L      0x0020
#define MII_MODEL_REALTEK_RTL8211       0x0011

#define RTL_VENDORID		0x10EC
#define RTL_DEVICEID_8139	0x8139

/* Media status register in 8139/8100 MAC */

#define RTL_MEDIASTAT           0x0058

#define RTL_MEDIASTAT_RXPAUSE   0x01
#define RTL_MEDIASTAT_TXPAUSE   0x02
#define RTL_MEDIASTAT_LINK      0x04    /* 0 = link ok, 1 = link down */
#define RTL_MEDIASTAT_SPEED10   0x08    /* 1 = 10Mbps, 0 = 100Mbps */
#define RTL_MEDIASTAT_AUXPRW    0x10
#define RTL_MEDIASTAT_RXFLOWCTL 0x40    /* duplex mode */
#define RTL_MEDIASTAT_TXFLOWCTL 0x80    /* duplex mode */

/*
 * The following constants describe the vendor-specific registers
 * in the RealTek RTL8201L PHY. Registers 0-15 are the same as defined
 * in the MII spec.
 */

/* Specified configuration register */

#define RTL_NWS		16		/* NWAY setup register */

#define RTL_NWS_ANEG_LSCHECK	0x0001	/* link status check state */
#define RTL_NWS_ANEG_PDCHECK	0x0002	/* parallel detection check state */
#define RTL_NWS_ANEG_ADCHECK	0x0004	/* ability detect check state */
#define RTL_NWS_NWAYLOOP	0x0200	/* NWAY set to loopback mode */
#define RTL_NWS_ANEG_TIMERFAST	0x0400	/* NWAY timer has sped up */
#define RTL_NWS_LED4_LINKPULSE	0x0800	/* LED4 indicates link pulse */

#define RTL_LBREMR	17		/* loopback/bypass/rxerr */

#define RTL_LBREMR_RMII		0x0001	/* RMII mode enabled */
#define RTL_LBREMR_FX		0x0002	/* fiber mode */
#define RTL_LBREMR_PKTERR	0x0004	/* force packet 722ms timeout error */
#define RTL_LBREMR_LINKERR	0x0008	/* force link error */
#define RTL_LBREMR_PMEERR	0x0010	/* force premature end error */
#define RTL_LBREMR_CODEERR	0x0020	/* force symbol error */
#define RTL_LBREMR_JBEN		0x0040	/* enable jabber detection */
#define RTL_LBREMR_LNK100	0x0080	/* force 100Mbps link good */
#define RTL_LBREMR_LNK10	0x0100	/* force 10Mbps link good */
#define RTL_LBREMR_LOOP		0x0200	/* enable DSP loopback */
#define RTL_LBREMR_ANALOG_PDWN	0x0800	/* analog function power down */
#define RTL_LBREMR_LDPS		0x1000	/* enable link down power save mode */
#define RTL_LBREMR_BYPASS_SCRAM	0x2000	/* bypass scrambler */
#define RTL_LBREMR_BYPASS_4B5B	0x4000	/* bypass 4B5B encoder */
#define RTL_LBREMR_REPEATER	0x8000	/* force repeater mode */

#define RTL_RXERR_CNT	18		/* RX error counter */

#define RTL_SNR		19		/* signal to noise display */

#define RTL_SNR_SNR		0x000F

#define RTL_TEST	25		/* test register */

#define RTL_TEST_LINK100	0x0001	/* 100Mbps link established */
#define RTL_TEST_LNK10		0x0002	/* 10Mbps link established */
#define RTL_TEST_PHYADDR	0x0F80	/* strapped PHY address */

#ifdef __cplusplus
}
#endif

#endif /* __INCgenericPhyh */
