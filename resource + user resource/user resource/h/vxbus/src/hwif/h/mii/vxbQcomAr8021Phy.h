/* vxbQcomAr8021Phy.h - Qualcomm AR80xx GbE PHY driver header file */

/*
 * Copyright (c) 2012, 2013, 2015 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
12dec15,m_w  Add support for AR8031. (VXW6-85047)
01c,20may13,wyt  Add support for AR8035
01b,23apr12,d_l  Fix compiler warning
01a,13apr12,x_z  written
*/

#ifndef __INCvxbQcomAr8021Phyh
#define __INCvxbQcomAr8021Phyh

#ifdef __cplusplus
extern "C" {
#endif

#define AR80XX_PHY_NAME                     "ar80xxPhy"

/* Vendor/Device ID */

#define ATHEROS_OUI_ID                      0x001374

#define AR8021_MII_MODEL                    0x0004
#define AR803X_MII_MODEL                    0x0007

#define AR8031_PHY_ID2                      0xD074
#define AR8033_PHY_ID2                      0xD074 /* same as 8031 */
#define AR8035_PHY_ID2                      0xD072

/*
 * Three types of registers are present on AR8031/AR8035:
 *
 * TYPE 1:
 *      IEEE defined standard MII registers, which are accessed directly
 *      through the management frame.
 * TYPE 2:
 *      Atheros defined debug registers.
 * TYPE 3:
 *      IEEE defined MDIO Manageable Device (MMD) register.
 */

/* MII standard register */
#define MII_PSE_CTRL_REG                    0xb
#define MII_PSE_STATUS_REG                  0xc
#define MII_MMD_ACCESS_CTRL_REG             0xd
#define MII_MMD_ACCESS_ADDR_DATA_REG        0xe

/* MII vendor register */
#define MII_FUNCTION_CTRL_REG               0x10
#define MII_SPECIFIC_STATUS_REG             0x11
#define MII_INTERRUPT_ENABLE_REG            0x12
#define MII_INTERRUPT_STATUS_REG            0x13
#define MII_SMART_SPEED_REG                 0x14
#define MII_CDT_CTRL_REG                    0x16
#define MII_LED_CTRL_REG                    0x18
#define MII_MANUAL_LED_REG                  0x19
#define MII_COPPER_FIBER_STATUS_REG         0x1b
#define MII_CABLE_DIAG_STATUS_REG           0x1c
#define MII_DEBUG_IDX_REG                   0x1d
#define MII_DEBUG_DATA_REG                  0x1e
#define MII_CHIP_CONFIG_REG                 0x1f

/*
 * Debug Register
 * The valid offset in debug register is listed as below:
 */
#define AR_DBG_OFFSET_00                    0x00
#define AR_DBG_OFFSET_05                    0x05
#define AR_DBG_OFFSET_10                    0x10
#define AR_DBG_OFFSET_0B                    0x0b
#define AR_DBG_OFFSET_11                    0x11
#define AR_DBG_OFFSET_12                    0x12
#define AR_DBG_OFFSET_29                    0x29

/*
 * MMD device register and offset,
 * there is only two device address was used in AR8031/AR8035:
 * address 3 and address 7.
 */
#define AR_MMD_DEV_ADDR_3                   0x3
#define AR_MMD_DEV_ADDR_7                   0x7

/* below register are controlled by MMD device 3 */
#define AR_MMD_PCS_CTRL                     0x0000
#define AR_MMD_PCS_STATUS                   0x0001
#define AR_MMD_EEE_CAPABILITY               0x0014
#define AR_MMD_WAKE_ERR_COUNTER             0x0016
#define AR_MMD_P1588_CTRL                   0x8012
#define AR_MMD_P1588_RX_SEQID               0x8013
#define AR_MMD_P1588_RX_SRCPORT_ID1         0x8014
#define AR_MMD_P1588_RX_SRCPORT_ID2         0x8015
#define AR_MMD_P1588_RX_SRCPORT_ID3         0x8016
#define AR_MMD_P1588_RX_SRCPORT_ID4         0x8017
#define AR_MMD_P1588_RX_SRCPORT_ID5         0x8018
#define AR_MMD_P1588_RX_TIME_STAMP1         0x8019
#define AR_MMD_P1588_RX_TIME_STAMP2         0x801a
#define AR_MMD_P1588_RX_TIME_STAMP3         0x801b
#define AR_MMD_P1588_RX_TIME_STAMP4         0x801c
#define AR_MMD_P1588_RX_TIME_STAMP5         0x801d
#define AR_MMD_P1588_RX_FRAC_NANO_1         0x801e
#define AR_MMD_P1588_RX_FRAC_NANO_2         0x801f
#define AR_MMD_P1588_TX_SEQID               0x8020
#define AR_MMD_P1588_TX_SRCPORT_ID1         0x8021
#define AR_MMD_P1588_TX_SRCPORT_ID2         0x8022
#define AR_MMD_P1588_TX_SRCPORT_ID3         0x8023
#define AR_MMD_P1588_TX_SRCPORT_ID4         0x8024
#define AR_MMD_P1588_TX_SRCPORT_ID5         0x8025
#define AR_MMD_P1588_TX_TIME_STAMP1         0x8026
#define AR_MMD_P1588_TX_TIME_STAMP2         0x8027
#define AR_MMD_P1588_TX_TIME_STAMP3         0x8028
#define AR_MMD_P1588_TX_TIME_STAMP4         0x8029
#define AR_MMD_P1588_TX_TIME_STAMP5         0x802a
#define AR_MMD_P1588_TX_FRAC_NANO_1         0x802b
#define AR_MMD_P1588_TX_FRAC_NANO_2         0x802c
#define AR_MMD_P1588_ORGIN_CORECT_O1        0x802d
#define AR_MMD_P1588_ORGIN_CORECT_O2        0x802e
#define AR_MMD_P1588_ORGIN_CORECT_O3        0x802f
#define AR_MMD_P1588_ORGIN_CORECT_O4        0x8030
#define AR_MMD_P1588_INGRESS_TRIG_TIME_O1   0x8031
#define AR_MMD_P1588_INGRESS_TRIG_TIME_O2   0x8032
#define AR_MMD_P1588_INGRESS_TRIG_TIME_O3   0x8033
#define AR_MMD_P1588_INGRESS_TRIG_TIME_O4   0x8034
#define AR_MMD_P1588_TX_LATENCY_O           0x8035
#define AR_MMD_P1588_INC_VALUE_O1           0x8036
#define AR_MMD_P1588_INC_VALUE_O2           0x8037
#define AR_MMD_P1588_NANO_OFFSET_O1         0x8038
#define AR_MMD_P1588_NANO_OFFSET_O2         0x8039
#define AR_MMD_P1588_SEC_OFFSET_O1          0x803a
#define AR_MMD_P1588_SEC_OFFSET_O2          0x803b
#define AR_MMD_P1588_SEC_OFFSET_O3          0x803c
#define AR_MMD_P1588_REAL_TIME_I_1          0x803d
#define AR_MMD_P1588_REAL_TIME_I_2          0x803e
#define AR_MMD_P1588_REAL_TIME_I_3          0x803f
#define AR_MMD_P1588_REAL_TIME_I_4          0x8040
#define AR_MMD_P1588_REAL_TIME_I_5          0x8041
#define AR_MMD_P1588_RTC_FRAC_NANO_I_1      0x8042
#define AR_MMD_P1588_RTC_FRAC_NANO_I_2      0x8043
#define AR_MMD_WAKE_ON_LAN_ADDR_1           0x804a
#define AR_MMD_WAKE_ON_LAN_ADDR_2           0x804b
#define AR_MMD_WAKE_ON_LAN_ADDR_3           0x804c
#define AR_MMD_REM_PHY_LPBK                 0x805a
#define AR_MMD_SMARTEEE_CTRL_1              0x805b
#define AR_MMD_SMARTEEE_CTRL_2              0x805c
#define AR_MMD_SMARTEEE_CTRL_3              0x805d

/* below register are controlled by MMD device 7 */
#define AR_MMD_AUTO_NEG_CTRL_1              0x0000
#define AR_MMD_AUTO_NEG_STATUS              0x0001
#define AR_MMD_AUTO_NEG_XNP_TX              0x0016
#define AR_MMD_AUTO_NEG_XNP_TX_1            0x0017
#define AR_MMD_AUTO_NEG_XNP_TX_2            0x0018
#define AR_MMD_AUTO_NEG_LP_XNP_ABILITY      0x0019
#define AR_MMD_AUTO_NEG_LP_XNP_ABILITY_1    0x001a
#define AR_MMD_AUTO_NEG_LP_XNP_ABILITY_2    0x001b
#define AR_MMD_EEE_ADV                      0x003c
#define AR_MMD_EEE_LP_ADV                   0x003d
#define AR_MMD_EEE_AUTO_EG_RESULT           0x8000
#define AR_MMD_SGMII_CTRL_1                 0x8005
#define AR_MMD_SGMII_CTRL_2                 0x8011
#define AR_MMD_SGMII_CTRL_3                 0x8012
#define AR_MMD_CLK_25M_SEL                  0x8016
#define AR_MMD_1588_CLK_SEL                 0x8017

/* Bit defines */

#define AR_TXCLK_DELAY_ENABLE               0x0100  /* TX CLK delay */
#define AR_SMARTEEE_CTL_3_LPI_EN            0x0100  /* LPI enable or disable */

#define AR_MMD_ACCESS_CONTROL_ADDR          0x0000
#define AR_MMD_ACCESS_CONTROL_DATA          0x4000

/*
 * According AR8031 Data Sheet August 2011 Version 1.1,
 * there are eight output frequency of CLK_25M, which is
 * controlled by bit 4:2 of register device address(7) and
 * offset(0x8016)
 */

#define AR_8031_CLK_25M_FROM_CRYSTAL_XOUT    (0 << 2)
#define AR_8031_CLK_25M_FROM_DSP_1G          (1 << 2)
#define AR_8031_CLK_50M_FROM_LOCAL_PLL       (2 << 2)
#define AR_8031_CLK_50M_FROM_DSP             (3 << 2)
#define AR_8031_CLK_62DOT5M_FROM_LOCAL_PLL   (4 << 2)
#define AR_8031_CLK_62DOT5M_FROM_DSP         (5 << 2)
#define AR_8031_CLK_125M_FROM_LOCAL_PLL      (6 << 2)
#define AR_8031_CLK_125M_FROM_DSP            (7 << 2)

/*
 * According AR8035 Data Sheet March 2012 Ver. 2.1,
 * there are four output frequency of CLK_25M, which is
 * controlled by bit 4:3 of register device address(7) and
 * offset(0x8016)
 */

#define AR_8035_CLK_25M                     (0 << 3)
#define AR_8035_CLK_50M                     (1 << 3)
#define AR_8035_CLK_62DOT5M                 (2 << 3)
#define AR_8035_CLK_125M                    (3 << 3)

/* retry times for reset status read */

#define AR80XX_PHY_TIMES                    1000

/* select clk string name in DTS */

#define AR803X_PHY_CLK_SELECT_STR           "clkSelect"

/* some macro definition used by debug function  */

#define AR803X_FMT_STR       "%2d - %34s : 0x%04x\n"
#define AR803X_DBG_REG_STR   "%39s : 0x%04x\n"
#define AR803X_MMD_REG_STR   "%39s : 0x%04x\n"
#define TO_VALUE_STR(x)      x, #x
#define TO_STR(x)            #x

IMPORT void qcomAr8021PhyRegister (void);

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbQcomAr8021Phyh */
