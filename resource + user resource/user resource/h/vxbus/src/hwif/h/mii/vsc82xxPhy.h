/* vsc82xxPhy.h - register definitions for the Vitesse Single chip PHY */

/*
 * Copyright (c) 2005 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01d,31mar08,wap  Add support for 8201 chip
01c,29nov06,wap  Clean up
01b,21feb06,dtr  Export phy register - SPR#117719.
01a,28jul05,dtr  written
*/

#ifndef __INCvitessePhyh
#define __INCvitessePhyh

IMPORT void * vigPhyMethodsGet (void);
IMPORT void vigPhyRegister (void);
/*
 * The following constants describe the vendor-specific registers
 * in the Cicada copper gigabit PHY chips now bought out by vitesse.
 */

#define MII_OUI_VITESSE        0x00000F        /* Broadcom Corporation */
#define MII_OUI_xxVITESSE      0x0003F1        /* Broadcom Corporation */

#define MII_MODEL_xxVITESSE_VSC8201    0x0001
#define MII_MODEL_xxVITESSE_VSC8204    0x0004
#define MII_MODEL_xxVITESSE_VSC82xx    0x0004  /* Will be 0x0004-7 */

#define VIGPHY_RGMII_MODE 1
#define VIGPHY_RGMII_BASIC_MODE 2

#define VIGPHY_BPCTL	0x12	/* Bypass control */

#define VIGPHY_BPCTL_TX_DISABLE 0x8000
#define VIGPHY_BPCTL_BP4B5B     0x4000
#define VIGPHY_BPCTL_SCRAMBLER  0x2000
#define VIGPHY_BPCTL_DESCRAM    0x1000
#define VIGPHY_BPCTL_PCS_RX     0x0800
#define VIGPHY_BPCTL_PCS_TX     0x0400
#define VIGPHY_BPCTL_LF_INHIBIT 0x0200
#define VIGPHY_BPCTL_TX_TEST_CLK   0x0100 
#define VIGPHY_BPCTL_FORCE_BCM5400 0x0080
#define VIGPHY_BPCTL_BP_BCM5400    0x0040
#define VIGPHY_BPCTL_DIS_PAIR_SWAP 0x0020 
#define VIGPHY_BPCTL_DIS_POLARITY  0x0010
#define VIGPHY_BPCTL_PARALLEL_DETECT  0x0008
#define VIGPHY_BPCTL_DIS_PULSE_FILTER 0x0004
#define VIGPHY_BPCTL_DIS_1000BT_EXC   0x0002
#define VIGPHY_BPCTL_125MHZ_CLK       0x0001

#define VIGPHY_10BTCTL  0x16

#define VIGPHY_10BTCTL_LINK_DIS       0x8000
#define VIGPHY_10BTCTL_JAB_DET        0x4000
#define VIGPHY_10BTCTL_DIS_ECHO       0x2000
#define VIGPHY_10BTCTL_SQE_DIS        0x1000
#define VIGPHY_10BTCTL_100BT_SQ_NORM  0x0000
#define VIGPHY_10BTCTL_100BT_SQ_LOW   0x0400
#define VIGPHY_10BTCTL_100BT_SQ_HIGH  0x0800
#define VIGPHY_10BTCTL_100BT_SQ_RSVD  0x0C00
#define VIGPHY_10BTCTL_EOF_ERR        0x0100
#define VIGPHY_10BTCTL_10BT_DISC      0x0080
#define VIGPHY_10BTCTL_10BT_LINK      0x0040



#define VIGPHY_EXTCTL1	0x17	/* PHY extended control */

#define VIGPHY_EXTCTL1_MAC_IFMODE        0xF000
#define VIGPHY_EXTCTL1_MAC_VOLTAGE       0x0E00  
#define VIGPHY_EXTCTL1_RGMII_SKEW        0x0100
#define VIGPHY_EXTCTL1_TBI_EWRAP         0x0080
#define VIGPHY_EXTCTL1_TBI_BIT_REVERSE   0x0040
#define VIGPHY_EXTCTL1_LED_BRATE_20      0x0000
#define VIGPHY_EXTCTL1_LED_BRATE_10      0x0008
#define VIGPHY_EXTCTL1_LED_BRATE_5       0x0010
#define VIGPHY_EXTCTL1_LED_BRATE_2_5     0x0018
#define VIGPHY_EXTCTL1_LED_BLINK_ENABLE  0x0004
#define VIGPHY_EXTCTL1_LED_COMB_LINKACT  0x0002

#define VIGPHY_IFMODE_GMII   0x0000
#define VIGPHY_IFMODE_RGMII  0x1000
#define VIGPHY_IFMODE_TBI    0x2000
#define VIGPHY_IFMODE_RTBI   0x4000

#define VIGPHY_VOLTAGE_3_3V      0x0000  
#define VIGPHY_VOLTAGE_2_5V      0x0200

#define VIGPHY_EXTCTL2	0x18	/* PHY extended control */

#define VIGPHY_EXTCTL2_ANALOG_LOOPBACK  0x0001

#define VIGPHY_ISR 0x19

#define VIGPHY_ISR_PIN                0x8000
#define VIGPHY_ISR_LSP_CHG            0x4000
#define VIGPHY_ISR_LNK_CHG            0x2000
#define VIGPHY_ISR_DUP_CHG            0x1000
#define VIGPHY_ISR_AUTONEG_ERR        0x0800
#define VIGPHY_ISR_AUTONEG_DONE       0x0400
#define VIGPHY_ISR_PAGE_RECVD         0x0200
#define VIGPHY_ISR_SYMBOL_ERR         0x0100
#define VIGPHY_ISR_DESCRAMBLE_LOCK    0x0080
#define VIGPHY_ISR_MDI_XOVER          0x0040
#define VIGPHY_ISR_POLARITY           0x0020
#define VIGPHY_ISR_JAB_DETECT         0x0010
#define VIGPHY_ISR_FALSE_CARRIER      0x0008
#define VIGPHY_ISR_PARALLEL           0x0004
#define VIGPHY_ISR_MASTER_SLAVE       0x0002
#define VIGPHY_ISR_RX_ER              0x0001

#define VIGPHY_IMR 0x1A

#define VIGPHY_IMR_PIN                0x8000
#define VIGPHY_IMR_LSP_CHG            0x4000
#define VIGPHY_IMR_LNK_CHG            0x2000
#define VIGPHY_IMR_DUP_CHG            0x1000
#define VIGPHY_IMR_AUTONEG_ERR        0x0800
#define VIGPHY_IMR_AUTONEG_DONE       0x0400
#define VIGPHY_IMR_PAGE_RECVD         0x0200
#define VIGPHY_IMR_SYMBOL_ERR         0x0100
#define VIGPHY_IMR_DESCRAMBLE_LOCK    0x0080
#define VIGPHY_IMR_MDI_XOVER          0x0040
#define VIGPHY_IMR_POLARITY           0x0020
#define VIGPHY_IMR_JAB_DETECT         0x0010
#define VIGPHY_IMR_FALSE_CARRIER      0x0008
#define VIGPHY_IMR_PARALLEL           0x0004
#define VIGPHY_IMR_MASTER_SLAVE       0x0002
#define VIGPHY_IMR_RX_ER              0x0001

#define VIGPHY_AUXSTS 0x1C

#define VIGPHY_AUXSTS_AUTONEG_COMP  0x8000
#define VIGPHY_AUXSTS_AUTONEG_DIS   0x4000
#define VIGPHY_AUXSTS_MDI_MDIX      0x2000
#define VIGPHY_AUXSTS_CD_PAIR_SWAP  0x1000
#define VIGPHY_AUXSTS_A_POLARITY    0x0800
#define VIGPHY_AUXSTS_b_POLARITY    0x0400
#define VIGPHY_AUXSTS_C_POLARITY    0x0200
#define VIGPHY_AUXSTS_D_POLARITY    0x0100
#define VIGPHY_AUXSTS_FDX           0x0020
#define VIGPHY_AUXSTS_SPEED_10BT    0x0000
#define VIGPHY_AUXSTS_SPEED_100BT   0x0008
#define VIGPHY_AUXSTS_SPEED_1000BT  0x0010
#define VIGPHY_AUXSTS_MODE_DUPLEX   0x0004

#define VIGPHY_AUXSTS_AN_RES 0x38
#define VIGPHY_RES_1000FD 0x30
#define VIGPHY_RES_1000HD 0x10
#define VIGPHY_RES_100FD  0x28
#define VIGPHY_RES_100HD  0x08
#define VIGPHY_RES_10FD  0x20
#define VIGPHY_RES_10HD  0x00

#endif /* __INCcis52xxPhyh */
