/* bcm54xxphy.h - register definitions for the BCM54xx PHY chips */

/*
 * Copyright (c) 2005, 2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,31jul08,y_w  add support for BCM5481
01a,15sep05,wap  written
*/

#ifndef __INCbcm54xxPhyh
#define __INCbcm54xxPhyh

IMPORT void brgPhyRegister (void);

/*
 * The following constants describe the vendor-specific registers
 * in the Broadcom BCM54xx copper gigabit PHY chips.
 */

#define MII_OUI_BROADCOM        0x001018        /* Broadcom Corporation */
#define MII_OUI_xxBROADCOM      0x000818        /* Broadcom Corporation */

/*
 * Broadcom Corporation's OUI (Organizationally Unique Identifier) is 0x000AF7,
 * the bit order is
 * 
 * Hex   : 000AF7
 * Binary: 0000 0000 0000 1010 1111 0111
 * Order:  8       1 16      9 24     17
 *
 * Here, 0x0050EF is another order of OUI - 1:24
 */

#define MII_OUI_BROADCOM_REORDER        0x0050ef

#define MII_MODEL_xxBROADCOM_BCM5400    0x0004
#define MII_MODEL_xxBROADCOM_BCM5401    0x0005
#define MII_MODEL_xxBROADCOM_BCM5411    0x0007
#define MII_MODEL_xxBROADCOM_BCM5481    0x000a
#define MII_MODEL_xxBROADCOM_BCM5412    0x000e
#define MII_MODEL_xxBROADCOM_BCM5701    0x0011
#define MII_MODEL_xxBROADCOM_BCM5703    0x0016
#define MII_MODEL_xxBROADCOM_BCM5704    0x0019
#define MII_MODEL_xxBROADCOM_BCM5705    0x001a
#define MII_MODEL_xxBROADCOM_BCM5750    0x0018
#define MII_MODEL_xxBROADCOM_BCM5714    0x0034

#define BRGPHY_EXTCTL	0x10	/* PHY extended control */

#define BRGPHY_EXTCTL_MAC_PHY	0x8000	/* 10BIT/GMI-interface */
#define BRGPHY_EXTCTL_DIS_CROSS	0x4000	/* Disable MDI crossover */
#define BRGPHY_EXTCTL_TX_DIS	0x2000	/* Tx output disable d*/
#define BRGPHY_EXTCTL_INT_DIS	0x1000	/* Interrupts disabled */
#define BRGPHY_EXTCTL_F_INT	0x0800	/* Force interrupt */
#define BRGPHY_EXTCTL_BY_45	0x0400	/* Bypass 4B5B-Decoder */
#define BRGPHY_EXTCTL_BY_SCR	0x0200	/* Bypass scrambler */
#define BRGPHY_EXTCTL_BY_MLT3	0x0100	/* Bypass MLT3 encoder */
#define BRGPHY_EXTCTL_BY_RXA	0x0080	/* Bypass RX alignment */
#define BRGPHY_EXTCTL_RES_SCR	0x0040	/* Reset scrambler */
#define BRGPHY_EXTCTL_EN_LTR	0x0020	/* Enable LED traffic mode */
#define BRGPHY_EXTCTL_LED_ON	0x0010	/* Force LEDs on */
#define BRGPHY_EXTCTL_LED_OFF	0x0008	/* Force LEDs off */
#define BRGPHY_EXTCTL_EX_IPG	0x0004	/* Extended TX IPG mode */
#define BRGPHY_EXTCTL_3_LED		0x0002	/* Three link LED mode */
#define BRGPHY_EXTCTL_HIGH_LA	0x0001	/* GMII Fifo Elasticy (?) */

#define BRGPHY_EXTSTS	0x11	/* PHY extended status */

#define BRGPHY_EXTSTS_CROSS_STS	0x2000	/* MDI crossover status */
#define BRGPHY_EXTSTS_INT_STAT	0x1000	/* Interrupt status */
#define BRGPHY_EXTSTS_RRS	0x0800	/* Remote receiver status */
#define BRGPHY_EXTSTS_LRS	0x0400	/* Local receiver status */
#define BRGPHY_EXTSTS_LOCKED	0x0200	/* Locked */
#define BRGPHY_EXTSTS_LS	0x0100	/* Link status */
#define BRGPHY_EXTSTS_RF	0x0080	/* Remove fault */
#define BRGPHY_EXTSTS_CE_ER	0x0040	/* Carrier ext error */
#define BRGPHY_EXTSTS_BAD_SSD	0x0020	/* Bad SSD */
#define BRGPHY_EXTSTS_BAD_ESD	0x0010	/* Bad ESS */
#define BRGPHY_EXTSTS_RX_ER	0x0008	/* RX error */
#define BRGPHY_EXTSTS_TX_ER	0x0004	/* TX error */
#define BRGPHY_EXTSTS_LOCK_ER	0x0002	/* Lock error */
#define BRGPHY_EXTSTS_MLT3_ER	0x0001	/* MLT3 code error */

#define BRGPHY_RXERRCNT	0x12	/* RX error counter */

#define BRGPHY_FCERRCNT	0x13	/* false carrier sense counter */
#define BGRPHY_FCERRCNT_MASK	0x00FF	/* False carrier counter */

#define BRGPHY_RXNOCNT	0x14	/* RX not OK counter */
#define BRGPHY_RXNOCNT_LOCAL	0xFF00	/* Local RX not OK counter */
#define BRGPHY_RXNOCNT_REMOTE	0x00FF	/* Local RX not OK counter */

#define BRGPHY_DSP_RW_PORT	0x15	/* DSP coefficient r/w port */

#define BRGPHY_DSP_ADDR_REG	0x17	/* DSP coefficient addr register */

#define BRGPHY_DSP_TAP_NUM_MASK			0x00
#define BRGPHY_DSP_AGC_A			0x00
#define BRGPHY_DSP_AGC_B			0x01
#define BRGPHY_DSP_MSE_PAIR_STATUS		0x02
#define BRGPHY_DSP_SOFT_DECISION		0x03
#define BRGPHY_DSP_PHASE_REG			0x04
#define BRGPHY_DSP_SKEW				0x05
#define BRGPHY_DSP_POWER_SAVER_UPPER_BOUND	0x06
#define BRGPHY_DSP_POWER_SAVER_LOWER_BOUND	0x07
#define BRGPHY_DSP_LAST_ECHO			0x08
#define BRGPHY_DSP_FREQUENCY			0x09
#define BRGPHY_DSP_PLL_BANDWIDTH		0x0A
#define BRGPHY_DSP_PLL_PHASE_OFFSET		0x0B

#define BRGPHY_DSP_FILTER_DCOFFSET	0x0C00
#define BRGPHY_DSP_FILTER_FEXT3		0x0B00
#define BRGPHY_DSP_FILTER_FEXT2		0x0A00
#define BRGPHY_DSP_FILTER_FEXT1		0x0900
#define BRGPHY_DSP_FILTER_FEXT0		0x0800
#define BRGPHY_DSP_FILTER_NEXT3		0x0700
#define BRGPHY_DSP_FILTER_NEXT2		0x0600
#define BRGPHY_DSP_FILTER_NEXT1		0x0500
#define BRGPHY_DSP_FILTER_NEXT0		0x0400
#define BRGPHY_DSP_FILTER_ECHO		0x0300
#define BRGPHY_DSP_FILTER_DFE		0x0200
#define BRGPHY_DSP_FILTER_FFE		0x0100

#define BRGPHY_DSP_CONTROL_ALL_FILTERS	0x1000

#define BRGPHY_DSP_SEL_CH_0		0x0000
#define BRGPHY_DSP_SEL_CH_1		0x2000
#define BRGPHY_DSP_SEL_CH_2		0x4000
#define BRGPHY_DSP_SEL_CH_3		0x6000

#define BRGPHY_AUXCTL	0x18	/* AUX control */
#define BRGPHY_AUXCTL_LOW_SQ	0x8000	/* Low squelch */
#define BRGPHY_AUXCTL_LONG_PKT	0x4000	/* RX long packets */
#define BRGPHY_AUXCTL_ER_CTL	0x3000	/* Edgerate control */
#define BRGPHY_AUXCTL_TX_TST	0x0400	/* TX test, always 1 */
#define BRGPHY_AUXCTL_DIS_PRF	0x0080	/* dis part resp filter */
#define BRGPHY_AUXCTL_DIAG_MODE	0x0004	/* Diagnostic mode */

#define BRGPHY_MISCCTL          0x18    /* Misc. control */
#define BRGPHY_MISCCTL_WE       0x8000  /* Write Enable */
#define BRGPHY_MISCCTL_SKEW     0x0100  /* RGMII RXD to RXC Skew  */
#define BRGPHY_MISCCTL_SRS      0x0007  /* Shadow Register Select */

#define BRGPHY_AUXSTS	0x19	/* AUX status */
#define BRGPHY_AUXSTS_ACOMP	0x8000	/* autoneg complete */
#define BRGPHY_AUXSTS_AN_ACK	0x4000	/* autoneg complete ack */
#define BRGPHY_AUXSTS_AN_ACK_D	0x2000	/* autoneg complete ack detect */
#define BRGPHY_AUXSTS_AN_NPW	0x1000	/* autoneg next page wait */
#define BRGPHY_AUXSTS_AN_RES	0x0700	/* AN HDC */
#define BRGPHY_AUXSTS_PDF	0x0080	/* Parallel detect. fault */
#define BRGPHY_AUXSTS_RF	0x0040	/* remote fault */
#define BRGPHY_AUXSTS_ANP_R	0x0020	/* AN page received */
#define BRGPHY_AUXSTS_LP_ANAB	0x0010	/* LP AN ability */
#define BRGPHY_AUXSTS_LP_NPAB	0x0008	/* LP Next page ability */
#define BRGPHY_AUXSTS_LINK	0x0004	/* Link status */
#define BRGPHY_AUXSTS_PRR	0x0002	/* Pause resolution-RX */
#define BRGPHY_AUXSTS_PRT	0x0001	/* Pause resolution-TX */

#define BRGPHY_RES_1000FD	0x0700	/* 1000baseT full duplex */
#define BRGPHY_RES_1000HD	0x0600	/* 1000baseT half duplex */
#define BRGPHY_RES_100FD	0x0500	/* 100baseT full duplex */
#define BRGPHY_RES_100T4	0x0400	/* 100baseT4 */
#define BRGPHY_RES_100HD	0x0300	/* 100baseT half duplex */
#define BRGPHY_RES_10FD		0x0200	/* 10baseT full duplex */
#define BRGPHY_RES_10HD		0x0100	/* 10baseT half duplex */

#define BRGPHY_ISR		0x1A	/* interrupt status */
#define BRGPHY_ISR_PSERR	0x4000	/* Pair swap error */
#define BRGPHY_ISR_MDXI_SC	0x2000	/* MDIX Status Change */
#define BRGPHY_ISR_HCT		0x1000	/* counter above 32K */
#define BRGPHY_ISR_LCT		0x0800	/* all counter below 128 */
#define BRGPHY_ISR_AN_PR	0x0400	/* Autoneg page received */
#define BRGPHY_ISR_NO_HDCL	0x0200	/* No HCD Link */
#define BRGPHY_ISR_NO_HDC	0x0100	/* No HCD */
#define BRGPHY_ISR_USHDC	0x0080	/* Negotiated Unsupported HCD */
#define BRGPHY_ISR_SCR_S_ERR	0x0040	/* Scrambler sync error */
#define BRGPHY_ISR_RRS_CHG	0x0020	/* Remote RX status change */
#define BRGPHY_ISR_LRS_CHG	0x0010	/* Local RX status change */
#define BRGPHY_ISR_DUP_CHG	0x0008	/* Duplex mode change */
#define BRGPHY_ISR_LSP_CHG	0x0004	/* Link speed changed */
#define BRGPHY_ISR_LNK_CHG	0x0002	/* Link status change */
#define BRGPHY_ISR_CRCERR	0x0001	/* CEC error */

#define BRGPHY_IMR		0x1B	/* interrupt mask */
#define BRGPHY_IMR_PSERR	0x4000	/* Pair swap error */
#define BRGPHY_IMR_MDXI_SC	0x2000	/* MDIX Status Change */
#define BRGPHY_IMR_HCT		0x1000	/* counter above 32K */
#define BRGPHY_IMR_LCT		0x0800	/* all counter below 128 */
#define BRGPHY_IMR_AN_PR	0x0400	/* Autoneg page received */
#define BRGPHY_IMR_NO_HDCL	0x0200	/* No HCD Link */
#define BRGPHY_IMR_NO_HDC	0x0100	/* No HCD */
#define BRGPHY_IMR_USHDC	0x0080	/* Negotiated Unsupported HCD */
#define BRGPHY_IMR_SCR_S_ERR	0x0040	/* Scrambler sync error */
#define BRGPHY_IMR_RRS_CHG	0x0020	/* Remote RX status change */
#define BRGPHY_IMR_LRS_CHG	0x0010	/* Local RX status change */
#define BRGPHY_IMR_DUP_CHG	0x0008	/* Duplex mode change */
#define BRGPHY_IMR_LSP_CHG	0x0004	/* Link speed changed */
#define BRGPHY_IMR_LNK_CHG	0x0002	/* Link status change */
#define BRGPHY_IMR_CRCERR	0x0001	/* CEC error */

#endif /* __INCbcm54xxPhyh */
