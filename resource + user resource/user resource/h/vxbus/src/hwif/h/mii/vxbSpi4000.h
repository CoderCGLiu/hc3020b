/* vxbSpi4000.h - header file for SPI4000 driver */

/*
 * Copyright (c) 2009-2010 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01c,11oct10,d_c  Moved from cav_cn[35]xxx_mipsi64r2sf BSPs
01b,19aug10,d_c  Remove SDK references
01a,27jan09,dlk  Written
*/

#ifndef __INCvxbSpi4000h
#define __INCvxbSpi4000h

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Provided by vxbSpi4000Mdio.c :
 */
IMPORT void spi4000MdioRegister (void);

/*
 * Provided by spi4000Phy.c :
 */

IMPORT void spi4000PhyRegister (void);

IMPORT void * spi4000_funcs;  /* for use in funcsI resource in hwconf.c */

/*
 * Fake PHY OUI / Model number for the Marvell 88E1141 PHYs
 * on the SPI4000 board.  Used to select the spi4000Phy driver.
 * The actual PHY Id register values read from the devices
 * were 0x0141 and 0x0cd4 (on registers 2 and 3 respectively),
 * corresponding to OUI 0x005043 (MII_OUI_MARVELL) and Model
 * 0x0d (MII_MODEL_MARVELL_E1145) and revision 4.
 */

#define MII_OUI_SPI4000		0x007FA0  /* Hint: FA0 in decimal is ... */
#define MII_MODEL_SPI4000_0	0x0

#define SPI4000_TIMEOUT 10000

/* Private instance context */

typedef struct spi4000_drv_ctrl
    {
    VXB_DEVICE_ID	spi4000Dev;
    /*
     * Last value written to the
     * IXF1010_REG_MDI_SINGLE_READ_WRITE_DATA register
     * on the SPI4000 that this unit applies to.
     */
    int lastWriteVal;
    } SPI4000_DRV_CTRL;

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbSpi4000h */
