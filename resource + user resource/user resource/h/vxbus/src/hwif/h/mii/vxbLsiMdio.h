/* vxbLsiMdio.h - header file for LSI MDIO driver */

/*
 * Copyright (c) 2010 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,05may10,x_z  written based tescMdio.h v01a.
*/

#ifndef __INClsiMdioh
#define __INClsiMdioh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void lsiMdioRegister (void);

#ifndef BSP_VERSION

/* driver generic defines */

#define LSI_MDIO_TIMEOUT            10000
#define LSI_MDIO_LOCK_TIMEOUT       10

/* registers offset */

#define LSI_MDIO_CTRL               0x00 /* Control */
#define LSI_MDIO_STATUS             0x04 /* Status */
#define LSI_MDIO_CLK_OFFSET         0x08 /* Clock offset */
#define LSI_MDIO_CLK_PERIOD         0x0C /* Clock Period */

/* bit definitions for Control Register */

#define LSI_MDIO_BUSY           0x80000000 /* Busy status */
#define LSI_MDIO_PRE            0x40000000 /* Preamble Suppress */
#define LSI_MDIO_CLAUSE_45      0x20000000 /* Clause 45 Enable */
#define LSI_MDIO_CLAUSE_22      0x00000000 /* Clause 45 Enable */
#define LSI_MDIO_OPCODE_MASK    0x18000000 /* opcode*/
#define LSI_MDIO_OPCODE_ADRS    0x00000000 /* 00 - address */
#define LSI_MDIO_OPCODE_RD      0x10000000 /* 10 - read */
#define LSI_MDIO_OPCODE_WR      0x08000000 /* 01 - write */
#define LSI_MDIO_SEL_0          0x00000000 /* interface 0 select */
#define LSI_MDIO_SEL_1          0x04000000 /* interface 1 select */
#define LSI_MDIO_DEVAD_MASK     0x03E00000 /* MMD address for Clause 45 */
#define LSI_MDIO_DEVAD_SHIFT    21         /* or register address for Clause 22 */
#define LSI_MDIO_PRTAD_MASK     0x001F0000 /* port address (PHY address) */
#define LSI_MDIO_PRTAD_SHIFT    16
#define LSI_MDIO_DATA_ADRS_MASK 0x0000FFFF /* data or register address for */
                                           /* Clause 45 */

/* bit definitions for Status Register */

#define LSI_MDIO_DONE           0x40000000 /* MDIO done  */

/* bit definitions for Clock offset/period Register */

#define LSI_MDIO_CLK_MASK       0xFF

/* Private instance context */

typedef struct lsiMdio_drv_ctrl
    {
    VXB_DEVICE_ID   lsiMdioDev;
    void *          lsiMdioHandle;
    void *          lsiMdioBar;
    SEM_ID          lsiMdioMiiSem;
    UINT32 *        lsiMdioShared;
    UINT32          lsiMdioDummy;
    } LSI_MDIO_DRV_CTRL;

#define LSI_MDIO_BAR(p)     ((LSI_MDIO_DRV_CTRL *)(p)->pDrvCtrl)->lsiMdioBar
#define LSI_MDIO_HANDLE(p)  ((LSI_MDIO_DRV_CTRL *)(p)->pDrvCtrl)->lsiMdioHandle

#define CSR_READ_4(pDev, addr)           \
    vxbRead32(LSI_MDIO_HANDLE (pDev),     \
        (UINT32 *) ((char *) LSI_MDIO_BAR (pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)   \
    vxbWrite32(LSI_MDIO_HANDLE (pDev),   \
        (UINT32 *) ((char *) LSI_MDIO_BAR (pDev) + addr), data)

#define CSR_SETBIT_4(pDev, addr, val) \
    CSR_WRITE_4(pDev, addr, CSR_READ_4 (pDev, addr) | (val))

#define CSR_CLRBIT_4(pDev, addr, val) \
    CSR_WRITE_4(pDev, addr, CSR_READ_4 (pDev, addr) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INClsiMdioh */
