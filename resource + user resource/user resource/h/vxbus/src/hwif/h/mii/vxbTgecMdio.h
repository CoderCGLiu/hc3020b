/* vxbTgecMdio.h - header file for TGEC MDIO driver */

/*
 * Copyright (c) 2010, 2012 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,10aug12,d_l  Add BSP hook to enable mdio mux.
01a,10jun10,wap  written
*/

#ifndef __INCvxbTgecMdioh
#define __INCvxbTgecMdioh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void tgmRegister (void);

#ifndef BSP_VERSION

#define TGM_TIMEOUT 10000

#define TGM_MDIO_CFGSTS       0x0030  /* config/status */
#define TGM_MDIO_CTRL         0x0034  /* control */
#define TGM_MDIO_DATA         0x0038  /* data */
#define TGM_MDIO_ADDR         0x003C  /* register address */

/* MDIO configuration and status */

#define TGM_MDIO_CFGSTS_CLKDIV		0x0000FF00 /* clock divisor */
#define TGM_MDIO_CFGSTS_MDIO_HOLD	0x0000000C /* hold time */
#define TGM_MDIO_CFGSTS_RD_ERR		0x00000002 /* read error */
#define TGM_MDIO_CFGSTS_BSY		0x00000001 /* MDIO busy */

#define TGM_CLKDIV(x)		(((x) << 8) & TGM_MDIO_CFGSTS_CLKDIV)

#define TGM_HOLD_1CYCLE			0x00000000
#define TGM_HOLD_2CYCLE			0x00000004
#define TGM_HOLD_3CYCLE			0x00000008
#define TGM_HOLD_4CYCLE			0x0000000C

/* MDIO control */

#define TGM_MDIO_CTRL_READ		0x00008000 /* initiate read */
#define TGM_MDIO_CTRL_POSTINC		0x00004000 /* read w/ post-increment */
#define TGM_MDIO_CTRL_SCAN_EN		0x00000800 /* scan mode enable */
#define TGM_MDIO_CTRL_PRE_DIS		0x00000400 /* preamble disable */
#define TGM_MDIO_CTRL_PORT_ADDR		0x000003E0 /* port address */
#define TGM_MDIO_CTRL_DEV_ADDR		0x0000001F /* device address */

#define TGM_PORT(x)		(((x) << 5) & TGM_MDIO_CTRL_PORT_ADDR)
#define TGM_DEV(x)		((x) & TGM_MDIO_CTRL_DEV_ADDR)

/* MDIO data register */

#define TGM_MDIO_DATA_BSY		0x80000000
#define TGM_MDIO_DATA_DATA		0x0000FFFF

#define TGM_DATA(x)		((x) & TGM_MDIO_DATA_DATA)

/* MDIO register address register */

#define TGM_MDIO_ADDR_ADDR		0x0000FFFF

/* Private instance context */

typedef struct tgm_drv_ctrl
    {
    VXB_DEVICE_ID	tgmDev;
    SEM_ID		tgmMiiSem;
    UINT32 *		tgmShared;
    UINT32		tgmDummy;
    VXB_DEVICE_ID	tgmGpioDev;
    UINT8		tgmFm0Gpio;
    UINT8		tgmFm1Gpio;
    FUNCPTR		tgmMuxEnable;
    } TM_DRV_CTRL;

#undef CSR_READ_4
#undef CSR_WRITE_4

#define CSR_READ_4(pDev, addr)                                  \
        *(volatile UINT32 *)((UINT32)pDev->pRegBase[0] + addr)

#define CSR_WRITE_4(pDev, addr, data)                           \
        do {                                                    \
            volatile UINT32 *pReg =                             \
                (UINT32 *)((UINT32)pDev->pRegBase[0] + addr);   \
            *(pReg) = (UINT32)(data);                           \
            WRS_ASM("eieio");                                   \
        } while ((0))

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbTgecMdioh */
