/* vxbIbmRgmii.h - header file for IBM/AMCC PPC4xx RGMII bridge driver */

/*
 * Copyright (c) 2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01c,01aug08,wap  add support for disabling and re-enabling ports
01b,03jul08,b_m  add mdio pin control.
01a,15jan08,wap  written
*/

#ifndef __INCvxbIbmRgmiih
#define __INCvxbIbmRgmiih

#ifdef __cplusplus
extern "C" {
#endif

#define RGMII_EMAC_RTBI		1
#define RGMII_EMAC_RGMII	2
#define RGMII_EMAC_TBI		3
#define RGMII_EMAC_GMII		4

IMPORT void rgmiiRegister (void);

#ifndef BSP_VERSION

#define RGMII_SPEED_10           RGMII_SSR_10
#define RGMII_SPEED_100          RGMII_SSR_100
#define RGMII_SPEED_1000         RGMII_SSR_1000

IMPORT STATUS rgmiiSpeedSelect (int, int);
IMPORT STATUS rgmiiPortDisable (int);
IMPORT STATUS rgmiiPortEnable (int);

/*
 * The PPC440GX family has 4 EMAC controllers, of which EMAC2 and EMAC3
 * can be configured for gigabit operation. For gigabit, they must
 * be routed via the RGMII bridge.
 */

#define RGMII_FER	0x00	/* function enable */
#define RGMII_SSR	0x04	/* speed select */

/* Function enable register */

#define RGMII_FER_RGMII2	0x0000000F
#define RGMII_FER_RGMII3	0x000000F0

#define RGMII_FER_DISABLED	0x0
#define RGMII_FER_RTBI		0x4
#define RGMII_FER_RGMII		0x5
#define RGMII_FER_TBI		0x6
#define RGMII_FER_GMII		0x7

#define RGMII_FER_EMAC2(x)	(x)
#define RGMII_FER_EMAC3(x)	((x) << 4)

#define RGMII_FER_EMAC(x, y)     ((y) << ((x) * 4))
#define RGMII_FER_EMAC_GET(x, y) (((y) >> ((x) * 4)) & 0xF)

#define RGMII_FER_MDIO_EN	0x000C0000
#define RGMII_FER_MDIO(x)	(1 << (31 - (12 + x)))

/* Speed select register */

#define RGMII_SSR_RGMII2	0x00000007
#define RGMII_SSR_RGMII3	0x00000700

#define RGMII_SSR_10		0
#define RGMII_SSR_100		2
#define RGMII_SSR_1000		4

#define RGMII_SSR_EMAC2(x)	(x)
#define RGMII_SSR_EMAC3(x)	((x) << 8)

#define RGMII_SSR_EMAC(x, y)     ((y) << ((x) * 8))

typedef struct rgmii_drv_ctrl
    {
    VXB_DEVICE_ID       rgmiiDev;
    void *              rgmiiBar;
    void *              rgmiiHandle;
    SEM_ID              rgmiiSem;
    UINT8               rgmiiMode[4];
    } RGMII_DRV_CTRL;

#define RGMII_BAR(p)   ((RGMII_DRV_CTRL *)(p)->pDrvCtrl)->rgmiiBar
#define RGMII_HANDLE(p)   ((RGMII_DRV_CTRL *)(p)->pDrvCtrl)->rgmiiHandle

#ifndef CSR_READ_4

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (RGMII_HANDLE(pDev), (UINT32 *)((char *)RGMII_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (RGMII_HANDLE(pDev),                             \
        (UINT32 *)((char *)RGMII_BAR(pDev) + addr), data)

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#endif

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbIbmRgmiih */
