/* vxbQorIQMemacMdio.h - header file for MEMAC MDIO driver */

/*
 * Copyright (c) 2012-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01d,04feb13,wap  Apply updates from review
01c,11oct12,wap  Handle EMI selection correctly
01b,10oct12,wap  testing
01a,14aug12,wap  written
*/

#ifndef __INCvxbQorIQMemacMdioh
#define __INCvxbQorIQMemacMdioh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void qmmRegister (void);

#ifndef BSP_VERSION

#define QMM_TIMEOUT 10000

#define QMM_DEVADDR_NONE	0xFF

#define QMM_MDIO_CFGSTS		0x0030  /* config/status */
#define QMM_MDIO_CTRL		0x0034  /* control */
#define QMM_MDIO_DATA		0x0038  /* data */
#define QMM_MDIO_ADDR		0x003C  /* register address */

/* MDIO configuration and status */

#define QMM_MDIO_CFGSTS_BSY2		0x80000000 /* Another busy bit (?) */
#define QMM_MDIO_CFGSTS_CMP		0x40000000 /* Command complete */
#define QMM_MDIO_CFGSTS_CIM		0x20000000 /* Cmd complete intmask */
#define QMM_MDIO_CFGSTS_CLKDIV		0x0000FF00 /* clock divisor */
#define QMM_MDIO_CFGSTS_ENC45		0x00000040 /* enable clause 45 */
#define QMM_MDIO_CFGSTS_PRE_DIS		0x00000020 /* disable preamble */
#define QMM_MDIO_CFGSTS_MDIO_HOLD	0x0000001C /* hold time */
#define QMM_MDIO_CFGSTS_RD_ERR		0x00000002 /* read error */
#define QMM_MDIO_CFGSTS_BSY		0x00000001 /* MDIO busy */

#define QMM_CLKDIV(x)			(((x) << 8) & QMM_MDIO_CFGSTS_CLKDIV)

#define QMM_HOLD_1CYCLE			0x00000000
#define QMM_HOLD_3CYCLE			0x00000004
#define QMM_HOLD_5CYCLE			0x00000008
#define QMM_HOLD_7CYCLE			0x0000000C
#define QMM_HOLD_9CYCLE			0x00000010
#define QMM_HOLD_11CYCLE		0x00000014
#define QMM_HOLD_13CYCLE		0x00000018
#define QMM_HOLD_15CYCLE		0x0000001C

/* MDIO control */

#define QMM_MDIO_CTRL_READ		0x00008000 /* initiate read */
#define QMM_MDIO_CTRL_POSTINC		0x00004000 /* read w/ post-increment */
#define QMM_MDIO_CTRL_PORT_ADDR		0x000003E0 /* port address */
#define QMM_MDIO_CTRL_DEV_ADDR		0x0000001F /* device address */

#define QMM_PORT(x)			(((x) << 5) & QMM_MDIO_CTRL_PORT_ADDR)
#define QMM_DEV(x)			((x) & QMM_MDIO_CTRL_DEV_ADDR)

/* MDIO data register */

#define QMM_MDIO_DATA_BSY		0x80000000
#define QMM_MDIO_DATA_DATA		0x0000FFFF

#define QMM_DATA(x)			((x) & QMM_MDIO_DATA_DATA)

/* MDIO register address register */

#define QMM_MDIO_ADDR_ADDR		0x0000FFFF

/* Private instance context */

typedef UINT32 (*qmmEmiSelectFunc)(int fmanNum, int memacNum);

typedef struct qmm_drv_ctrl
    {
    VXB_DEVICE_ID	qmmDev;
    SEM_ID		qmmMiiSem;
    UINT32 *		qmmShared;
    UINT32		qmmDummy;
    qmmEmiSelectFunc	qmmEmiSelectFunc;
    } QMM_DRV_CTRL;

#undef CSR_READ_4
#undef CSR_WRITE_4

#define CSR_READ_4(pDev, addr)                                  \
        *(volatile UINT32 *)((UINT32)pDev->pRegBase[0] + addr)

#define CSR_WRITE_4(pDev, addr, data)                           \
        do {                                                    \
            volatile UINT32 *pReg =                             \
                (UINT32 *)((UINT32)pDev->pRegBase[0] + addr);   \
            *(pReg) = (UINT32)(data);                           \
            WRS_ASM("eieio");                                   \
        } while ((0))

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbQorIQMemacMdioh */
