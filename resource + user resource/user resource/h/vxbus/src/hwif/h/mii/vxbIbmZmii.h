/* vxbIbmZmii.h - header file for IBM/AMCC PPC4xx ZMII bridge driver */

/*
 * Copyright (c) 2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,15jan08,wap  written
*/

#ifndef __INCvxbIbmZmiih
#define __INCvxbIbmZmiih

#ifdef __cplusplus
extern "C" {
#endif

#define ZMII_EMAC_MII		1
#define ZMII_EMAC_SMII		2
#define ZMII_EMAC_RMII		3


IMPORT void zmiiRegister (void);

#ifndef BSP_VERSION

#define ZMII_SPEED_10		0
#define ZMII_SPEED_100		ZMII_SSR_SP

IMPORT STATUS zmiiSpeedSelect (int, int);

/*
 * The ZMII bus appears on both GP/EP parts and GX parts. In the GP/EP
 * parts, there are only two EMACs. On the GX parts, there are 4. EMAC0
 * and EMAC1 are always 10/100. EMAC2 and EMAC3 can be either 10/100 or
 * 10/100/1000. When configured for 10/100/1000, EMAC2 and EMAC3 are
 * routed through the RGMII/RTBI bridge instead of the ZMII bridge.
 */

#define ZMII_FER		0x0	/* Function enable register */
#define ZMII_SSR		0x4	/* Speed select register */
#define ZMII_SMIISR		0x8	/* ZMII SMII Status register */

/* Function enable register */

#define ZMII_FER_MDI0		0x80000000	/* MDI enable */
#define ZMII_FER_SMII0		0x40000000	/* Serial MII enable */
#define ZMII_FER_RMII0		0x20000000	/* Reduced pin MII enable */
#define ZMII_FER_MII0		0x10000000	/* MII enable */
#define ZMII_FER_MDI1		0x08000000
#define ZMII_FER_SMII1		0x04000000
#define ZMII_FER_RMII1		0x02000000
#define ZMII_FER_MII1		0x01000000
#define ZMII_FER_MDI2		0x00800000
#define ZMII_FER_SMII2		0x00400000
#define ZMII_FER_RMII2		0x00200000
#define ZMII_FER_MII2		0x00100000
#define ZMII_FER_MDI3		0x00080000
#define ZMII_FER_SMII3		0x00040000
#define ZMII_FER_RMII3		0x00020000
#define ZMII_FER_MII3		0x00010000

#define ZMII_FER_MDI		0x8
#define ZMII_FER_SMII		0x4
#define ZMII_FER_RMII		0x2
#define ZMII_FER_MII		0x1

#define ZMII_FER_EMAC(x, y)	((y) << (28 - ((x) * 4)))

#define ZMII_FER_EMAC0(x)	((x) << 28)
#define ZMII_FER_EMAC1(x)	((x) << 24)
#define ZMII_FER_EMAC2(x)	((x) << 20)
#define ZMII_FER_EMAC3(x)	((x) << 16)

/* Speed select register */

#define ZMII_SSR_SCI0		0x40000000	/* supress coll. indication */
#define ZMII_SSR_FSS0		0x20000000	/* Force speed (SMII only) */
#define ZMII_SSR_SP0		0x10000000	/* Speed select, 0=10, 1=100 */
#define ZMII_SSR_SCI1		0x04000000
#define ZMII_SSR_FSS1		0x02000000
#define ZMII_SSR_SP1		0x01000000
#define ZMII_SSR_SCI2		0x00400000
#define ZMII_SSR_FSS2		0x00200000
#define ZMII_SSR_SP2		0x00100000
#define ZMII_SSR_SCI3		0x00040000
#define ZMII_SSR_FSS3		0x00020000
#define ZMII_SSR_SP3		0x00010000

#define ZMII_SSR_SCI		0x4
#define ZMII_SSR_FSS		0x2
#define ZMII_SSR_SP		0x1

#define ZMII_SSR_EMAC0(x)	((x) << 28)
#define ZMII_SSR_EMAC1(x)	((x) << 24)
#define ZMII_SSR_EMAC2(x)	((x) << 20)
#define ZMII_SSR_EMAC3(x)	((x) << 16)

#define ZMII_SSR_EMAC(x, y)	((y) << (28 - ((x) * 4)))

/* SMII Status register */

#define ZMII_SMIISR_E01		0x80000000	/* set to 1 */
#define ZMII_SMIISR_E0C		0x40000000	/* false carrier */
#define ZMII_SMIISR_E0N		0x20000000	/* upper nibble valid */
#define ZMII_SMIISR_E0J		0x10000000	/* jabber */
#define ZMII_SMIISR_E0L		0x08000000	/* link, 0 = down, 1 = up */
#define ZMII_SMIISR_E0D		0x04000000	/* duplex, 0 = half, 1 = full */
#define ZMII_SMIISR_E0S		0x02000000	/* speed, 0 = 10, 1 = 100 */
#define ZMII_SMIISR_E0F		0x01000000	/* error in previous frame */
#define ZMII_SMIISR_E11		0x00800000
#define ZMII_SMIISR_E1C		0x00400000
#define ZMII_SMIISR_E1N		0x00200000
#define ZMII_SMIISR_E1J		0x00100000
#define ZMII_SMIISR_E1L		0x00080000
#define ZMII_SMIISR_E1D		0x00040000
#define ZMII_SMIISR_E1S		0x00020000
#define ZMII_SMIISR_E1F		0x00010000
#define ZMII_SMIISR_E21		0x00008000
#define ZMII_SMIISR_E2C		0x00004000
#define ZMII_SMIISR_E2N		0x00002000
#define ZMII_SMIISR_E2J		0x00001000
#define ZMII_SMIISR_E2L		0x00000800
#define ZMII_SMIISR_E2D		0x00000400
#define ZMII_SMIISR_E2S		0x00000200
#define ZMII_SMIISR_E2F		0x00000100
#define ZMII_SMIISR_E31		0x00000080
#define ZMII_SMIISR_E3C		0x00000040
#define ZMII_SMIISR_E3N		0x00000020
#define ZMII_SMIISR_E3J		0x00000010
#define ZMII_SMIISR_E3L		0x00000008
#define ZMII_SMIISR_E3D		0x00000004
#define ZMII_SMIISR_E3S		0x00000002
#define ZMII_SMIISR_E3F		0x00000001

#define ZMII_SMIISR_E1		0x80
#define ZMII_SMIISR_EC		0x40
#define ZMII_SMIISR_EN		0x20
#define ZMII_SMIISR_EJ		0x10
#define ZMII_SMIISR_EL		0x08
#define ZMII_SMIISR_ED		0x04
#define ZMII_SMIISR_ES		0x02
#define ZMII_SMIISR_EF		0x01

#define ZMII_SMIISR_EMAC0(x)	((x) << 24)
#define ZMII_SMIISR_EMAC1(x)	((x) << 16)
#define ZMII_SMIISR_EMAC2(x)	((x) << 8)
#define ZMII_SMIISR_EMAC3(x)	(x)

typedef struct zmii_drv_ctrl
    {
    VXB_DEVICE_ID	zmiiDev;
    void *		zmiiBar;
    void *		zmiiHandle;
    SEM_ID		zmiiSem;
    } ZMII_DRV_CTRL;

#define ZMII_BAR(p)   ((ZMII_DRV_CTRL *)(p)->pDrvCtrl)->zmiiBar
#define ZMII_HANDLE(p)   ((ZMII_DRV_CTRL *)(p)->pDrvCtrl)->zmiiHandle

#ifndef CSR_READ_4

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (ZMII_HANDLE(pDev), (UINT32 *)((char *)ZMII_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (ZMII_HANDLE(pDev),                             \
        (UINT32 *)((char *)ZMII_BAR(pDev) + addr), data)

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#endif

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbIbmZmiih */
