/* rtl8169Phy.h - register definitions for RealTek RTG8169 10/100/1000 PHY */

/*
 * Copyright (c) 2006 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,01jun06,wap  written
*/

#ifndef __INCrtl8169Phyh
#define __INCrtl8169Phyh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void rtgPhyRegister (void);

/* This is the OUI of the gigE PHY in the RealTek 8169S/8110S chips */
#define MII_OUI_xxREALTEK       0x000732        /*  */

#define MII_MODEL_xxREALTEK_RTL8169S    0x0011

#define RTG_VENDORID		0x10EC
#define RTG_DEVICEID_8139	0x8139

/* Gigabit media status, 8169 chip */

#define RTG_GMEDIASTAT          0x006C

#define RTG_GMEDIASTAT_FDX      0x01    /* full duplex */
#define RTG_GMEDIASTAT_LINK     0x02    /* link up */
#define RTG_GMEDIASTAT_10MBPS   0x04    /* 10mps link */
#define RTG_GMEDIASTAT_100MBPS  0x08    /* 100mbps link */
#define RTG_GMEDIASTAT_1000MBPS 0x10    /* gigE link */
#define RTG_GMEDIASTAT_RXFLOW   0x20    /* RX flow control on */
#define RTG_GMEDIASTAT_TXFLOW   0x40    /* TX flow control on */
#define RTG_GMEDIASTAT_TBI      0x80    /* TBI enabled */

#ifdef __cplusplus
}
#endif

#endif /* __INCgenericPhyh */
