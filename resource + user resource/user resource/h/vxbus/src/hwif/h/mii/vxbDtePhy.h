/* vxbDtePhy.h - register definitions for generic DTE PMD support */

/*
 * Copyright (c) 2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,09may08,wap  written
*/

#ifndef __INCvxbDtePhyh
#define __INCvxbDtePhyh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void dtePhyRegister (void);

#define DTE_ID1			0x3434
#define DTE_ID2			0x4343

#define DTE_ADDR		5

#define DTE_CR			0		/* Control register 1 */

#define DTE_CR_SPD2		0x003C          /* 0 = 10Gbps */
#define DTE_CR_SPD1		0x0040		/* 1 = 10Gbps */
#define DTE_CR_FDX              0x0100          /* FDX =1, half duplex =0 */
#define DTE_CR_RESTART          0x0200          /* restart auto negotiation */
#define DTE_CR_ISOLATE          0x0400          /* isolate PHY from MII */
#define DTE_CR_POWER_DOWN       0x0800          /* power down */
#define DTE_CR_SPD0             0x1000          /* 1 = 10Gbps */
#define DTE_CR_LOOPBACK         0x4000          /* 0 = normal, 1 = loopback */
#define DTE_CR_RESET            0x8000          /* 0 = normal, 1 = PHY reset */

#define DTE_SR1			1		/* Status register 1 */

#define DTE_SR1_LOW_PRW		0x0002		/* low power supported */
#define DTE_SR1_LINK_STATUS     0x0004          /* link Status -- 1 = link */
#define DTE_SR1_FAULT           0x0080          /* fault */

#define DTE_DEVIDR1		2		/* Device identifier 1 */
#define DTE_DEVIDR2		3		/* Device identifier 2 */

#define DTE_SPEED		4		/* Speed ability */

#define DTE_SPEED_10GB		0x0001		/* 10G capable */

#define DTE_DEVS		5		/* Devices in package */


#define DTE_DEVS_VS2		0x8000		/* Vendor specific 2 */
#define DTE_DEVS_VS1		0x4000		/* Vendor specific 1 */
#define DTE_DEVS_DTE		0x0020		/* DTE present */
#define DTE_DEVS_PHY		0x0010		/* PHY present */
#define DTE_DEVS_PCS		0x0008		/* PCS present */
#define DTE_DEVS_WIS		0x0004		/* WIS present */
#define DTE_DEVS_PMDPMA		0x0002		/* PMD/PMA present */
#define DTE_DEVS_CLAUSE22	0x0001		/* Clause 22 registers present */

#define DTE_SR2			8		/* Status register 1 */

#define DTE_SR2_PRESENT		0xC000		/* Device present */
#define DTE_SR2_TXFAULT		0x0800		/* Transmit fault */
#define DTE_SR2_RXFAULT		0x0400		/* Receive fault */

#define DTE_PRESENT_RESPONDING	0x8000		/* Device responding at addr. */

#define DTE_PKGIDR1		14		/* Package identifier 1 */
#define DTE_PKGIDR2		15		/* Package identifier 2 */

#define DTE_LANESTS		24		/* Lane status */

#define DTE_LANESTS_ALIGN	0x1000		/* Lane alignment status */
#define DTE_LANESTS_PTEST	0x0800		/* Pattern test ability */
#define DTE_LANESTS_L3SYNC	0x0008		/* Lane 3 synchronized */
#define DTE_LANESTS_L2SYNC	0x0004		/* Lane 2 synchronized */
#define DTE_LANESTS_L1SYNC	0x0002		/* Lane 1 synchronized */
#define DTE_LANESTS_L0SYNC	0x0001		/* Lane 0 synchronized */

#define DTE_TEST		25		/* Test control*/

#define DTE_TEST_TX		0x0004		/* Transmit test pattern */
#define DTE_TEST_SELECT		0x0003		/* Test pattern select */

#define DTE_PATTERN_HIGH	0x0000		/* High frequency pattern */
#define DTE_PATTERN_MED		0x0001		/* Medium frequency pattern */
#define DTE_PATTERN_LOW		0x0002		/* Low frequency pattern */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbDtePhyh */
