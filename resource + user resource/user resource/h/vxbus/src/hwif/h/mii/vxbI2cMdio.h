/* vxbI2cMdio.h - header file for I2C MDIO driver */

/*
 * Copyright (c) 2010, 2012 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,14feb12,y_y  update to new vxBus I2C driver.
01a,22oct09,wap  written
*/

#ifndef __INCvxbI2cMdioh
#define __INCvxbI2cMdioh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void imRegister (void);

#ifndef BSP_VERSION

#define IM_TIMEOUT 10000

/* I2C wire addresses for SFP devices */

#define IM_SFP_ID_PHY       0x56
#define IM_SFP_ID_SERDES    0x53
#define IM_SFP_ID_SERIAL    0x50

/* Read/write modifier bits */

#define IM_SFP_READ         0x01
#define IM_SFP_WRITE        0x00

/* Offsets within serial EEPROM */

#define IM_SFP_EE_VENDOR    0x14
#define IM_SFP_EE_PART      0x28
#define IM_SFP_EE_REV       0x38

typedef STATUS (*GPIO_SET_FUNC) (int , UINT32 *, UINT32 *);

/* Support 4 PHY chips, the value shouldn't never exceed 8 */

#define MAX_PHY_CHIP        4 

#define IM_SHARED_DELAY     10

/* Private instance context */

typedef struct im_drv_ctrl
    {
    VXB_DEVICE_ID    imDev;
    SEM_ID           imMiiSem;
    UINT32 *         imShared;
    UINT32           imDummy;
    VXB_DEVICE_ID    imGpioDev;
    VXB_DEVICE_ID    imI2cDev;
    UINT32           imGpioSelect[MAX_PHY_CHIP];
    UINT32           imGpioEnable[MAX_PHY_CHIP];
    } IM_DRV_CTRL;

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbI2cMdioh */
