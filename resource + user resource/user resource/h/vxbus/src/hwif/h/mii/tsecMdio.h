/* tsecMdio.h - header file for TSEC MDIO driver */

/*
 * Copyright (c) 2007 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,29jun07,wap  written
*/

#ifndef __INCtsecMdioh
#define __INCtsecMdioh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void tmRegister (void);

#ifndef BSP_VERSION

#define TM_TIMEOUT 10000

/* Private instance context */

typedef struct tm_drv_ctrl
    {
    VXB_DEVICE_ID	tmDev;
    SEM_ID		tmMiiSem;
    UINT32 *		tmShared;
    UINT32		tmDummy;
    } TM_DRV_CTRL;

#define CSR_READ_4(pDev, addr)                                  \
        *(volatile UINT32 *)((UINT32)pDev->pRegBase[0] + addr)

#define CSR_WRITE_4(pDev, addr, data)                           \
        do {                                                    \
            volatile UINT32 *pReg =                             \
                (UINT32 *)((UINT32)pDev->pRegBase[0] + addr);   \
            *(pReg) = (UINT32)(data);                           \
            WRS_ASM("eieio");                                   \
        } while ((0))

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCtsecMdioh */
