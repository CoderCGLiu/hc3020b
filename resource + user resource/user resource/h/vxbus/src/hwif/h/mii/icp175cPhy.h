/* icp175cPhy.h - header file for ICplus IP175C switch */

/*
 * Copyright (c) 2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,29jun07,wap  written
*/

#ifndef __INCicp175cPhyh
#define __INCicp175cPhyh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void icpRegister (void);


/*
 * The ICPlus IP175C shows up on the MII bus at 8 different PHY
 * addresses. The first 5 (0-4) are the standard MII addresses
 * for all 5 physical ports. The last 3 (29-31) are special devices
 * for manipulating the switch controller.
 */

#define ICP_PORT0_ADDR		0
#define ICP_PORT1_ADDR		1
#define ICP_PORT2_ADDR		2
#define ICP_PORT3_ADDR		3
#define ICP_PORT4_ADDR		4
#define ICP_MII0_ADDR		5
#define ICP_MII1_ADDR		6

#ifndef BSP_VERSION

#define ICP_SWCTL0_ADDR		29
#define ICP_SWCTL1_ADDR		30
#define ICP_SWCTL2_ADDR		31


/* Switch control register */

#define ICP0_SCTL		18

#define ICP0_SCTL_LEDSEL	0xC000	/* LED mode selection */
#define ICP0_SCTL_FLOWCTL	0x2000	/* 802.3x flow control enable */
#define ICP0_SCTL_BK_EN		0x1000	/* backpressure enable */
#define ICP0_SCTL_BF_STM_EN	0x0800	/* bcast storm surpression enable */
#define ICP0_SCTL_MAC_FLOWCTL	0x0400	/* MII0/MII2 MAC flow control enable */
#define ICP0_SCTL_HASHMODE	0x0010	/* hashing algorithm */
#define ICP0_SCTL_AGING_EN	0x0008	/* Address aging enable (~280 secs) */


/* VLAN control register */

#define ICP0_VCTL0		19

#define ICP0_VCTL0_P0_TBL_EN	0x8000	/* Enable port 0 lookup table */
#define ICP0_VCTL0_P0_COS_EN	0x4000	/* Port 0 class of service enable */
#define ICP0_VCTL0_P0_HPRIO_EN	0x2000	/* Port 0 high priority enable */
#define ICP0_VCTL0_P0_TBL	0x1F00	/* Port 0 lookup table */
#define ICP0_VCTL0_P1_TBL_EN	0x0080	/* Enable port 1 lookup table */
#define ICP0_VCTL0_P1_COS_EN	0x0040	/* Port 1 class of service enable */
#define ICP0_VCTL0_P1_HPRIO_EN	0x0020	/* Port 1 high priority enable */
#define ICP0_VCTL0_P1_TBL	0x001F	/* Port 1 lookup table */


/* VLAN control register */

#define ICP0_VCTL1		20

#define ICP0_VCTL1_P2_TBL_EN	0x8000	/* Enable port 2 lookup table */
#define ICP0_VCTL1_P2_COS_EN	0x4000	/* Port 2 class of service enable */
#define ICP0_VCTL1_P2_HPRIO_EN	0x2000	/* Port 2 high priority enable */
#define ICP0_VCTL1_P2_TBL	0x1F00	/* Port 2 lookup table */
#define ICP0_VCTL1_P3_TBL_EN	0x0080	/* Enable port 3 lookup table */
#define ICP0_VCTL1_P3_COS_EN	0x0040	/* Port 3 class of service enable */
#define ICP0_VCTL1_P3_HPRIO_EN	0x0020	/* Port 3 high priority enable */
#define ICP0_VCTL1_P3_TBL	0x001F	/* Port 3 lookup table */


/* VLAN control register */

#define ICP0_VCTL2		21

#define ICP0_VCTL2_P4_TBL_EN	0x8000	/* Enable port 4 lookup table */
#define ICP0_VCTL2_P4_COS_EN	0x4000	/* Port 4 class of service enable */
#define ICP0_VCTL2_P4_HPRIO_EN	0x2000	/* Port 4 high priority enable */
#define ICP0_VCTL2_P4_TBL	0x1F00	/* Port 4 lookup table */


/* Mode forcing register */

#define ICP0_FRC		22

#define ICP0_FRC_P4_EN		0x8000	/* Enable mode forcing for port 4 */
#define ICP0_FRC_P3_EN		0x4000	/* Enable mode forcing for port 3 */
#define ICP0_FRC_P2_EN		0x2000	/* Enable mode forcing for port 2 */
#define ICP0_FRC_P1_EN		0x1000	/* Enable mode forcing for port 1 */
#define ICP0_FRC_P0_EN		0x0800	/* Enable mode forcing for port 0 */
#define ICP0_FRC_P4_100		0x0400	/* Force port 4 to 100Mbps */
#define ICP0_FRC_P3_100		0x0200	/* Force port 3 to 100Mbps */
#define ICP0_FRC_P2_100		0x0100	/* Force port 2 to 100Mbps */
#define ICP0_FRC_P1_100		0x0080	/* Force port 1 to 100Mbps */
#define ICP0_FRC_P0_100		0x0040	/* Force port 0 to 100Mbps */
#define ICP0_FRC_P4_FDX		0x0020	/* Force port 4 to full duplex */
#define ICP0_FRC_P3_FDX		0x0010	/* Force port 4 to full duplex */
#define ICP0_FRC_P2_FDX		0x0008	/* Force port 4 to full duplex */
#define ICP0_FRC_P1_FDX		0x0004	/* Force port 4 to full duplex */
#define ICP0_FRC_P0_FDX		0x0002	/* Force port 4 to full duplex */


/* Tag register 1 */

#define ICP0_TAG		23

#define ICP0_TAG_P4_INSTAG	0x8000	/* Port 4 inserts tag on xmit */
#define ICP0_TAG_P3_INSTAG	0x4000	/* Port 3 inserts tag on xmit */
#define ICP0_TAG_P2_INSTAG	0x2000	/* Port 2 inserts tag on xmit */
#define ICP0_TAG_P1_INSTAG	0x1000	/* Port 1 inserts tag on xmit */
#define ICP0_TAG_P0_INSTAG	0x0800	/* Port 0 inserts tag on xmit */
#define ICP0_TAG_P4_STRTAG	0x0400	/* Port 4 strips tag on xmit */
#define ICP0_TAG_P3_STRTAG	0x0200	/* Port 3 strips tag on xmit */
#define ICP0_TAG_P2_STRTAG	0x0100	/* Port 2 strips tag on xmit */
#define ICP0_TAG_P1_STRTAG	0x0080	/* Port 1 strips tag on xmit */
#define ICP0_TAG_P0_STRTAG	0x0040	/* Port 0 strips tag on xmit */
#define ICP0_TAG_P5_INSTAG	0x0020	/* Port 5 inserts tag on xmit */
#define ICP0_TAG_P5_STRTAG	0x0010	/* Port 5 strips tag on xmit */


/* Port 0-5 VLAN tag registers */

#define ICP0_PORT0_VLID		24
#define ICP0_PORT1_VLID		25
#define ICP0_PORT2_VLID		26
#define ICP0_PORT3_VLID		27
#define ICP0_PORT4_VLID		28
#define ICP0_PORT5_VLID		30


/* Compatibility enable register */

#define ICP0_COMPAT		31

#define ICP0_COMPAT_IP175A	0xFFFF	/* Compatibility value */

#define ICP0_IP175A		0x175A
#define ICP0_IP175C		0x175C


/* Software reset register */

#define ICP1_RST		0

#define ICP1_RST_SWRST		0xFFFF	/* Software reset */

#define ICP1_RST_175C		0x175C	/* Write this to SWRST to reset */


/* VLAN 0 membership register */

#define ICP1_VLAN0		1
#define ICP1_VLAN1		2
#define ICP1_VLAN2		3
#define ICP1_VLAN3		4
#define ICP1_VLAN4		5
#define ICP1_VLAN5		6
#define ICP1_VLAN6		7
#define ICP1_VLAN7		8

#define ICP1_VLAN0_PORT0	0x0001	/* Port 0 belongs to VLAN 0 */
#define ICP1_VLAN0_PORT1	0x0002	/* Port 1 belongs to VLAN 0 */
#define ICP1_VLAN0_PORT2	0x0004	/* Port 2 belongs to VLAN 0 */
#define ICP1_VLAN0_PORT3	0x0008	/* Port 3 belongs to VLAN 0 */
#define ICP1_VLAN0_PORT4	0x0010	/* Port 4 belongs to VLAN 0 */
#define ICP1_VLAN0_PORT5	0x0020	/* Port 5 belongs to VLAN 0 */
#define ICP1_VLAN1_PORT0	0x0100	/* Port 0 belongs to VLAN 1 */
#define ICP1_VLAN1_PORT1	0x0200	/* Port 1 belongs to VLAN 1 */
#define ICP1_VLAN1_PORT2	0x0400	/* Port 2 belongs to VLAN 1 */
#define ICP1_VLAN1_PORT3	0x0800	/* Port 3 belongs to VLAN 1 */
#define ICP1_VLAN1_PORT4	0x1000	/* Port 4 belongs to VLAN 1 */
#define ICP1_VLAN1_PORT5	0x2000	/* Port 5 belongs to VLAN 1 */


/* Router control register */

#define ICP1_RCTL0		9

#define ICP1_RCTL0_WAN_PORT4	0x1000	/* Port 4 is a WAN port */
#define ICP1_RCTL0_WAN_PORT3	0x0800	/* Port 3 is a WAN port */
#define ICP1_RCTL0_WAN_PORT2	0x0400	/* Port 2 is a WAN port */
#define ICP1_RCTL0_WAN_PORT1	0x0200	/* Port 1 is a WAN port */
#define ICP1_RCTL0_WAN_PORT0	0x0100	/* Port 0 is a WAN port */
#define ICP1_RCTL0_VLAN_TAG	0x0080	/* VLAN tag enable */
#define ICP1_RCTL0_VID_IDX	0x0070	/* low bits of VLID */
#define ICP1_RCTL0_ROUTER_EN	0x0008	/* Router enabled */
#define ICP1_RCTL0_VLAN_GROUPS	0x0007	/* VLAN port groups */


/* Router control register 2 */

#define ICP1_RCTL1		10

#define ICP1_RTCL1_LOCK_PORT5	0x0020	/* Lock port 5 */
#define ICP1_RTCL1_LOCK_PORT4	0x0010	/* Lock port 4 */
#define ICP1_RTCL1_LOCK_PORT3	0x0008	/* Lock port 3 */
#define ICP1_RTCL1_LOCK_PORT2	0x0004	/* Lock port 2 */
#define ICP1_RTCL1_LOCK_PORT1	0x0002	/* Lock port 1 */
#define ICP1_RTCL1_LOCK_PORT0	0x0001	/* Lock port 0 */


/* MAC address registers */

#define ICP1_MAC0_LO		20
#define ICP1_MAC0_MED		21
#define ICP1_MAC0_HI		22
#define ICP1_MAC1_LO		23
#define ICP1_MAC1_MED		24
#define ICP1_MAC1_HI		25

/* Private instance context */

typedef struct icp_drv_ctrl
    {
    VXB_DEVICE_ID	icpDev;
    void *		icpBar;
    void *		icpHandle;
    VXB_DEVICE_ID       icpMiiDev;
    FUNCPTR             icpMiiPhyRead;
    FUNCPTR             icpMiiPhyWrite;
    int			icpFakeLinkDown;
    SEM_ID		icpMiiSem;
    VXB_DEVICE_ID	icpMiiBus;
    } ICP_DRV_CTRL;

#undef CSR_READ_4
#undef CSR_WRITE_4
#undef CSR_SETBIT_4
#undef CSR_CLRBIR_4

#define ICP_BAR(p)   ((ICP_DRV_CTRL *)(p)->pDrvCtrl)->icpBar
#define ICP_HANDLE(p)   ((ICP_DRV_CTRL *)(p)->pDrvCtrl)->icpHandle

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (ICP_HANDLE(pDev), (UINT32 *)((char *)ICP_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (ICP_HANDLE(pDev),                             \
        (UINT32 *)((char *)ICP_BAR(pDev) + addr), data)

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCicp175cPhyh */
