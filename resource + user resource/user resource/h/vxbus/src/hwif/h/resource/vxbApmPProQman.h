/* vxbApmPProQman.h - Queue Manager driver header for APM PacketPro */

/*
 * Copyright (c) 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
DESCRIPTION
This file contains the definitions of registers, queue, Prefetech Buffer, and
message for the  APM PacketPro Queue Manager.
*/

/*
modification history
--------------------
01d,16nov11,x_z  added RType macros for mailbox issue.
01c,15nov11,x_z  fixed doucmentation issue.
01b,06sep11,x_z  added VQ support.
01a,17may11,x_z  written.
*/

#ifndef __INCvxbApmPProQmanh
#define __INCvxbApmPProQmanh

#ifdef __cplusplus
extern "C" {
#endif

/* QMAN Queue resource */

#define APM_QMAN_QID_MASK       0xFF
#define APM_QMAN_QUE_NUM        256 /* QID number supplied by HW */

/* QMAN Slave ID */

#define APM_QMAN_SLVID_DMA      0x0
#define APM_QMAN_SLVID_OCMM     0x1 /* On-Chip Memory Manager */
#define APM_QMAN_SLVID_SEC      0x2
#define APM_QMAN_SLVID_CLE      0x3 /* Classifer */
#define APM_QMAN_SLVID_SLIMP    0x4 /* SLIMpro */
#define APM_QMAN_SLVID_ETH      0x5
#define APM_QMAN_SLVID_UNCOM1   0x6 /* Uncommitted */
#define APM_QMAN_SLVID_UNCOM2   0x7 /* Uncommitted */
#define APM_QMAN_SLVID_PPC      0xF

#define APM_QMAN_SLVID_MASK     0xF
#define APM_QMAN_SLV_NUM        16

/*
 * For each subsystem, the QMan maintains a Subsystem Prefetech Buffer State
 * that indicates how many messages it has sent to the prefetch buffers in that
 * subsystem.
 */

#define APM_QMAN_PBN_WQ_NUM_DMA         4
#define APM_QMAN_PBN_FP_NUM_DMA         8

#define APM_QMAN_PBN_WQ_NUM_OCMM        3
#define APM_QMAN_PBN_FP_NUM_OCMM        3

#define APM_QMAN_PBN_WQ_NUM_SEC         2
#define APM_QMAN_PBN_FP_NUM_SEC         8

#define APM_QMAN_PBN_WQ_NUM_CLE         1 /* Classifer */
#define APM_QMAN_PBN_FP_NUM_CLE       8

#define APM_QMAN_PBN_WQ_NUM_SLIMP       2
#define APM_QMAN_PBN_FP_NUM_SLIMP       8
#define APM_QMAN_PBN_FP_NUM_SLIMP_QML   1 /* used for QM-Lite*/

/*
 * Each ethernet interface has 8 PBs for work queue, total 16 PBs for 2
 * interfaces.
 */

#define APM_QMAN_PBN_WQ_NUM_PER_ETH     8   /* 8 WQs per ethernet port */
#define APM_QMAN_PBN_WQ_NUM_ETH         16
#define APM_QMAN_PBN_WQ_NUM_ETH_QML     1
#define APM_QMAN_PBN_FP_NUM_ETH         16
#define APM_QMAN_PBN_FP_NUM_ETH_QML     2 /* used for QM-Lite*/

#define APM_QMAN_PBN_WQ_NUM_UNCOM1      1
#define APM_QMAN_PBN_FP_NUM_UNCOM1      1

#define APM_QMAN_PBN_WQ_NUM_UNCOM2      1
#define APM_QMAN_PBN_FP_NUM_UNCOM2      1

#define APM_QMAN_PBN_WQ_NUM_PPC         16
#define APM_QMAN_PBN_FP_NUM_PPC         16

#define APM_QMAN_PBN_FP_START           0x20

#define APM_QMAN_PBN_MAX                0x30
#define APM_QMAN_PBN_MASK               0x3F

/*
 * PPC sub-system has 16 mailboxes for enqueue, 16 mailboxes for dequeue, and 16
 * mailboxes for Free Pool allocation.
 *
 * Within each mailbox, QMan supports up to 8 transfer slots, so that the PPC
 * system and QMan can be transferring simultaneously to different slots.
 */

#define APM_QMAN_MBID_MASK          0xF
#define APM_QMAN_MBOX_NUM           16 /* available PPC Mailbox number */

#define APM_QMAN_MBOX_SLOTID_MASK   0x7
#define APM_QMAN_MBOX_SLOT_NUM      8 /* available slot number of PPC Mailbox */

/* Base Address for Mailboxes must be aligned with 4K bytes */

#define APM_QMAN_MBOX_ADRS_ALIGN  0x1000

/* QMAN Message Type */

#define APM_QMAN_MSG_TYPE_16B   0x1 /* Message of 16 Bytes, for Free Pool */
#define APM_QMAN_MSG_TYPE_32B   0x2 /* Message of 32 Bytes, for Work Queue */
#define APM_QMAN_MSG_TYPE_64B   0x3 /* Message of 64 Bytes, for Work Queue */
#define APM_QMAN_MSG_TYPE_MASK  0x3

/* QMAN CSR Interface */

/* Register Offsets */

#define APM_QMAN_CSR_IPBRR      0x000 /* Block Id And Revision */

#define APM_QMAN_CSR_CFG        0x004 /* Config */

/* QMan Prefetch Buffer Manager State Table Access */

#define APM_QMAN_CSR_PBM        0x008 /* Address */
#define APM_QMAN_CSR_PBM_WR     0x00C /* Write Data */
#define APM_QMAN_CSR_PBM_RD     0x010 /* Read Data */

#define APM_QMAN_CSR_PBM_COAL   0x014 /* Interrupt Coalescence Base Timer */
                                      /* Control */
#define APM_QMAN_CSR_PBM_CTICK0 0x018 /* interrupt coalescence tap control 0 */
#define APM_QMAN_CSR_PBM_CTICK1 0x01C /* interrupt coalescence tap control 0 */

/* Threshold 0/1 and Hysteresis Set of 8 Threshold Sets  */

#define APM_QMAN_THR_GROUP_NUM      8
#define APM_QMAN_CSR_THR0_SETn(n)   (0x020 + (n) * 8)
#define APM_QMAN_CSR_THR1_SETn(n)   (0x024 + (n) * 8)
#define APM_QMAN_CSR_HYSTERESIS     0x060

/* QMan(Queue and TM Credit) State Table Indirect Access */

#define APM_QMAN_CSR_QSTATE         0x064 /* Address */

/* Queue State */

#define APM_QMAN_QSTATE_WORD_NUM    4
#define APM_QMAN_CSR_QSTATE_WRn(n)  (0x068 + ((n) * 4)) /* Write Data */
#define APM_QMAN_CSR_QSTATE_RDn(n)  (0x078 + ((n) * 4)) /* Read Data */

/* TM Credit State */

#define APM_QMAN_CSTATE_WORD_NUM    2
#define APM_QMAN_CSR_CSTATE_WRn(n)  (0x088 + ((n) * 4)) /* Write Data */
#define APM_QMAN_CSR_CSTATE_RDn(n)  (0x090 + ((n) * 4)) /* Read Data */

#define APM_QMAN_CSR_CU_TIMER   0x098 /* TM credit update timer reload value */

/* PPC Mailbox Base Address */

#define APM_QMAN_CSR_PPC_MBOX_BASE  0x09c /* Dequeue */
#define APM_QMAN_CSR_PPC_FPOOL_BASE 0x0A0 /* Allocate Free Pool */
#define APM_QMAN_CSR_ENQ_BASE_A     0x0A4 /* Enqueue/Deallocate */
#define APM_QMAN_CSR_ENQ_BASE_B     0x0A8 /* Enqueue/Deallocate */

/* PPC Mailbox Enqueue/Dealloc Busy Status */

#define APM_QMAN_CSR_ENQ_STATUS_0   0x0AC
#define APM_QMAN_CSR_ENQ_STATUS_1   0x0B0
#define APM_QMAN_CSR_ENQ_STATUS_2   0x0B4
#define APM_QMAN_CSR_ENQ_STATUS_3   0x0B8

/* Interrup Control and Status */

#define APM_QMAN_CSR_INTERRUPT          0x0BC /* Overview Interrupt Status */
#define APM_QMAN_CSR_INTERRUPTMASK      0x0C0 /* Overview Interrupt Mask */

#define APM_QMAN_CSR_PBM_ERRINF         0x0C4 /* PBM Error Information */
#define APM_QMAN_CSR_MSGRD_ERRINF       0x0C8 /* Enq/Deq Error Information */

#define APM_QMAN_CSR_QM_SAB_PPC0        0x0CC /* SAB critical interrupt for */
                                              /* CPU 0 */
#define APM_QMAN_CSR_QM_SAB_PPC0MASK    0x0D0

#define APM_QMAN_CSR_QM_SAB_PPC1        0x0D4 /* SAB critical interrupt for */
                                              /* CPU 1 */
#define APM_QMAN_CSR_QM_SAB_PPC1MASK    0x0D8

#define APM_QMAN_CSR_QM_MBOX_NE         0x0DC /* Deq mailbox not empty status */

/* SAB critical region status */

#define APM_QMAN_CSR_PPC_SAB0           0x0E4
#define APM_QMAN_CSR_PPC_SAB1           0x0E8
#define APM_QMAN_CSR_PPC_SAB2           0x0EC
#define APM_QMAN_CSR_PPC_SAB3           0x0F0
#define APM_QMAN_CSR_PPC_SAB4           0x0F4
#define APM_QMAN_CSR_PPC_SAB5           0x0F8
#define APM_QMAN_CSR_PPC_SAB6           0x0FC
#define APM_QMAN_CSR_PPC_SAB7           0x100

/* Prefetch buffer base address for slave 6 and 7 */

#define APM_QMAN_CSR_PB_SLAVE6          0x104
#define APM_QMAN_CSR_PB_SLAVE7          0x108

/* QMan Enqueue/Dequeue Statistics Configuration */

#define APM_QMAN_CSR_QM_STATS_CFG       0x110 /* QID assignment */
#define APM_QMAN_CSR_ENQ_STATISTICS     0x114 /* Enqueue statistics */
#define APM_QMAN_CSR_DEQ_STATISTICS     0x118 /* Dequeue statistics */

#define APM_QMAN_CSR_FIFO_STATUS        0x11C /* various FIFO level status */

#define APM_QMAN_CSR_QM_RAM_MARGIN      0x3F8 /* RAM margin control register */

#define APM_QMAN_CSR_QM_TESTINT         0x3FC /* interrupt test */

/* Register Details */

/* Bit Definitions for APM_QMAN_CSR_IPBRR */

#define APM_QMAN_CSR_IPBRR_REV_MASK     0x3 /* Revision */
#define APM_QMAN_CSR_IPBRR_REV_SHIFT    8
#define APM_QMAN_CSR_IPBRR_REV_DEF      0
#define APM_QMAN_CSR_IPBRR_REV(x)      \
    (((x) >> APM_QMAN_CSR_IPBRR_REV_SHIFT) & APM_QMAN_CSR_IPBRR_REV_MASK)

#define APM_QMAN_CSR_IPBRR_BUSID_MASK   0x7 /* Bus ID */
#define APM_QMAN_CSR_IPBRR_BUSID_SHIFT  5
#define APM_QMAN_CSR_IPBRR_BUSID_DEF    0x3
#define APM_QMAN_CSR_IPBRR_BUSID(x)      \
    (((x) >> APM_QMAN_CSR_IPBRR_BUSID_SHIFT) & APM_QMAN_CSR_IPBRR_BUSID_MASK)

#define APM_QMAN_CSR_IPBRR_DEVID_MASK   0x1F /* Device ID */
#define APM_QMAN_CSR_IPBRR_DEVID_DEF    0x10
#define APM_QMAN_CSR_IPBRR_DEVID(x)      \
    ((x) & APM_QMAN_CSR_IPBRR_DEVID_MASK)

/* Bit Definitions for APM_QMAN_CSR_CFG */

#define APM_QMAN_CSR_CFG_EN             0x80000000 /* Enable QMan */
#define APM_QMAN_CSR_CFG_EN_ERRQUE      0x40000000 /* Enable Error Queue */
#define APM_QMAN_CSR_CFG_ERRQUEID_MASK  0xFF       /* Error Queue ID */

/* Bit Definitions for APM_QMAN_CSR_PBM */

#define APM_QMAN_CSR_PBM_SLVID_MASK         0xF /* Slave ID */
#define APM_QMAN_CSR_PBM_SLVID_SHIFT        6
#define APM_QMAN_CSR_PBM_PBN_MASK           0x3F /* Prefetch Buffer ID */
#define APM_QMAN_CSR_PBM_VAL(slvId, pbn)    \
    ((((slvId) & APM_QMAN_CSR_PBM_SLVID_MASK) << \
    APM_QMAN_CSR_PBM_SLVID_SHIFT) | ((pbn) & APM_QMAN_CSR_PBM_PBN_MASK))
/*
 * Bit Definitions for APM_QMAN_CSR_PBM_COAL
 *
 * Note: base period = (reload value +1) * qm_clock_period,
 * default = 1us at 266MHz.
 */

#define APM_QMAN_CSR_QNE_CTICK_SEL_MASK    0x7 /* tap select for Queue Not */
#define APM_QMAN_CSR_QNE_CTICK_SEL_SHIFT   28  /* Empty Interrupt, 0 means  */
                                               /* interrupt coalescence off */
#define APM_QMAN_CSR_COAL_RELOAD_VAL_MASK   0xFFFF /* base_timer reload value */
#define APM_QMAN_CSR_COAL_RELOAD_VAL_DEF    0x109

/*
 * Bit Definitions for APM_QMAN_CSR_PBM_CTICK0/APM_QMAN_CSR_PBM_CTICK1
 *
 * Note: APM_QMAN_CSR_PBM_CTICK0 is for PPC Mailbox 0 ~ 7, and
 * APM_QMAN_CSR_PBM_CTICK1 is for 8 ~ 15.
 */

#define APM_QMAN_CSR_PBM_CTICKn(n)              \
    (APM_QMAN_CSR_PBM_CTICK0 + (((n) >> 3) << 2))
#define APM_QMAN_CSR_PBM_CTICK_VAL_MASK         0x7
#define APM_QMAN_CSR_PBM_CTICK_VALn_SHIFT(n)    (32 - ((((n) & 7) + 1) << 2))

/* Bit Definitions for APM_QMAN_CSR_THR0_SETn */

#define APM_QMAN_CSR_THR_MASK   0x3FFFF

/* Bit Definitions for APM_QMAN_CSR_HYSTERESIS */

#define APM_QMAN_CSR_HYSTERESIS_VAL_MASK        0xF
#define APM_QMAN_CSR_HYSTERESIS_VALn_SHIFT(n)   (32 - (((n) + 1) << 2))

/* Bit Definitions for APM_QMAN_CSR_QSTATE */

#define APM_QMAN_CSR_QSTATE_VAL(qid) ((qid) & APM_QMAN_QID_MASK)

/*
 * Bit Definitions for APM_QMAN_CSR_CU_TIMER
 *
 * Note: This is the reload value of a down counter used to trigger the credit
 * update process for AVB arbitration. The period will be this value plus one,
 * divided by the QMan clock.
 *
 * The default gives 125us fo a 250MHZ QM clock.
 */

#define APM_QMAN_CSR_CU_TIMER_VAL(x)    ((x) & 0xFFFFF)
#define APM_QMAN_CSR_CU_TIMER_VAL_MASK  0xFFFFF /* Counter Reload Value */
#define APM_QMAN_CSR_CU_TIMER_DEFAULT   0x07A11

/*
 * Bit Definitions for PPC Mailbox Configuration Registers
 * (APM_QMAN_CSR_PPC_MBOX_BASE/APM_QMAN_CSR_PPC_FPOOL_BASE/
 * APM_QMAN_CSR_ENQ_BASE_A/APM_QMAN_CSR_ENQ_BASE_B)
 *
 * 1. The dequeue mailbox base address is a 4K-Byte aligned, system address to
 * the start of the dequeue mailboxes., and 4K Bytes is necessary.
 *
 * 2.The allocate free pool mailbox base address is 4K-Byte aligned, but only
 * 2K Bytes is necessary.
 *
 * 3. The enqueue/deallocate mailbox base address is 4K-Byte aligned, and 4K
 * Bytes is necessary. APM_QMAN_CSR_ENQ_BASE_A controls the first 12 mailboxes,
 * and APM_QMAN_CSR_ENQ_BASE_B controls the last 4 mailboxes.
 *
 */

#define APM_QMAN_MBOX_ARRAY_SIZE        0x1000

#define APM_QMAN_CSR_PPC_MBOX_COHERENT  0x80000000 /* read/write with */
                                                   /* coherent attribute set*/

/*
 * For dequeue and free pool mailbox base address, 36-bit system address must
 * be right shifted by 12-bits and then written to the registers.
 */

#define APM_QMAN_CSR_PPC_MBOX_BASE_VAL_SHIFT    12
#define APM_QMAN_CSR_PPC_MBOX_BASE_VAL_MASK     0x00FFFFFF

#define APM_QMAN_CSR_PPC_MBOX_BASE_DEF          0x003FFFFE
#define APM_QMAN_CSR_PPC_FPOOL_BASE_DEF         0x003FFFFF

#define APM_QMAN_CSR_PPC_MBOX_BASE_VAL(x)       \
    (((UINT32)((x) >> APM_QMAN_CSR_PPC_MBOX_BASE_VAL_SHIFT)) & \
    APM_QMAN_CSR_PPC_MBOX_BASE_VAL_MASK)

/*
 * For enqueue/deallocate mailbox base address, 36-bit system address must
 * be right shifted by 10-bits and then written to the registers.
 */

#define APM_QMAN_CSR_PPC_ENQ_MBOX_BASE_VAL_SHIFT    10
#define APM_QMAN_CSR_PPC_ENQ_MBOX_BASE_A_VAL_MASK   0x03FFFFFC
#define APM_QMAN_CSR_PPC_ENQ_MBOX_BASE_B_VAL_MASK   0x03FFFFFF
#define APM_QMAN_CSR_PPC_ENQ_MBOX_BASE_A_VAL(x)     \
    (((UINT32)((x) >> APM_QMAN_CSR_PPC_ENQ_MBOX_BASE_VAL_SHIFT)) & \
    APM_QMAN_CSR_PPC_ENQ_MBOX_BASE_A_VAL_MASK)

/*
 * APM_QMAN_CSR_ENQ_BASE_B can be set to (to APM_QMAN_CSR_ENQ_BASE_A + 3)
 * If you want to make these mailboxes contiguous with the first 12 mailboxes.
 */

#define APM_QMAN_CSR_PPC_ENQ_MBOX_BASE_B_VAL_A(x)   \
    (APM_QMAN_CSR_PPC_ENQ_MBOX_BASE_A_VAL (x) + 3)

#define APM_QMAN_CSR_ENQ_BASE_A_DEF             0x00FFFFF4
#define APM_QMAN_CSR_ENQ_BASE_B_DEF             0x00FFFFF7

/*
 * Bit Definitions for APM_QMAN_CSR_ENQ_STATUS_0/1/2/3
 *
 * Each Mailbox has 8 slots, and then holds 8 contiguous bits. And so every
 * status register can cover 4 mailboxes.
 */

#define APM_QMAN_CSR_ENQ_STATUSm(m)             \
    (APM_QMAN_CSR_ENQ_STATUS_0 + (((m) >> 2) << 2))

#define APM_QMAN_CSR_ENQ_STATUS_MBm_SLOTn(m, n) \
    (1 << (((3 - ((m) & 3)) << 3) + (n)))

#define APM_QMAN_CSR_ENQ_STATUS_MBm_SLOT(x, m)  \
    (((x) >> ((3 - ((m) & 3)) << 3)) & 0xFF)

/*
 * Bit Definitions for APM_QMAN_CSR_INTERRUPT/APM_QMAN_CSR_INTERRUPTMASK
 *
 * Writing 1 to clear staus or mask the interrupt.
 *
 * To enable Queue Not Empty interrupt, at least APM_QMAN_CSR_INT_CSRn in
 * APM_QMAN_CSR_INTERRUPTMASK MUST be unmasked. And APM_QMAN_CSR_INT_CSRn
 * should be used with APM_QMAN_CSR_INT_QNE.
 *
 * Each APM_QMAN_CSR_INT_CSRn points to one of 8 SAB critical status CSRs
 * (APM_QMAN_CSR_PPC_SABn), which indicate the queues that are not empty. In
 * order to use this feature, software must enable the cfgNotifyQNE and
 * cfgSabEn fields in the qstate.
 */

#define APM_QMAN_CSR_INT_QNE        0x80000000 /* Queue not empty interrupt */
                                               /* from coalescense timer */
#define APM_QMAN_CSR_INT_CSRn(n)    (1 << (31 - ((n) + 8)))
#define APM_QMAN_CSR_INT_CSR_ALL    0x00FF0000
#define APM_QMAN_CSR_INT_QP_ACR     0x00000004 /* QPcore Error */
#define APM_QMAN_CSR_INT_DEQ_AXI    0x00000002 /* AXI error on dequeue */
#define APM_QMAN_CSR_INT_PBM_DEC    0x00000001 /* PBM decrement error */

/*
 * Bit Definitions for APM_QMAN_CSR_PBM_ERRINF
 *
 * detail for PBM dequeue error(APM_QMAN_CSR_INT_PBM_DEC)
 */

#define APM_QMAN_CSR_PBM_ERRINF_CFG 0x00020000  /* PB was configured for VQ, */
                                                /* but assigned Queue was not */
#define APM_QMAN_CSR_PBM_ERRINF_DEC 0x00010000  /* underrun */

#define APM_QMAN_CSR_PBM_ERRINF_SLVID_MASK  0xF /* Slave ID for error PB */
#define APM_QMAN_CSR_PBM_ERRINF_SLVID_SHIFT 6
#define APM_QMAN_CSR_PBM_ERRINF_SLVID(x)    \
    (((x) >> APM_QMAN_CSR_PBM_ERRINF_SLVID_SHIFT) & \
    APM_QMAN_CSR_PBM_ERRINF_SLVID_MASK)

#define APM_QMAN_CSR_PBM_ERRINF_PBN_MASK    0x3F /* PBN for error PB */
#define APM_QMAN_CSR_PBM_ERRINF_PBN(x)    \
    ((x) & APM_QMAN_CSR_PBM_ERRINF_PBN_MASK)

/*
 * Bit Definitions for APM_QMAN_CSR_MSGRD_ERRINF
 *
 * detail for APM_QMAN_CSR_INT_QP_ACR/APM_QMAN_CSR_INT_DEQ_AXI
 */

/* Error information for APM_QMAN_CSR_INT_QP_ACR */

#define APM_QMAN_CSR_MSGRD_ERRINF_QPDIS         0x08000000 /* QPcore disabled */

#define APM_QMAN_CSR_MSGRD_ERRINF_DROP_MASK     0x7 /* QPcore Drop Code */
#define APM_QMAN_CSR_MSGRD_ERRINF_DROP_SHIFT    24
#define APM_QMAN_CSR_MSGRD_ERRINF_DROP_ENQVQ    3   /* Error Queue is VQ */
#define APM_QMAN_CSR_MSGRD_ERRINF_DROP_ENQDQ    4   /* Error Queue disabled */
#define APM_QMAN_CSR_MSGRD_ERRINF_DROP_ENQOV    5   /* Error Queue overrun */
#define APM_QMAN_CSR_MSGRD_ERRINF_DROP_DEQVQ    6   /* Dequeue error message */
                                                    /* from VQ */
#define APM_QMAN_CSR_MSGRD_ERRINF_DROP_DEQDQ    7   /* Dequeue error message */
                                                    /* from DQ */
#define APM_QMAN_CSR_MSGRD_ERRINF_DROP_CODE(x)  \
    (((x) >> APM_QMAN_CSR_MSGRD_ERRINF_DROP_SHIFT) & \
    APM_QMAN_CSR_MSGRD_ERRINF_DROP_MASK)

#define APM_QMAN_CSR_MSGRD_ERRINF_ACR_QID_MASK  0xFF
#define APM_QMAN_CSR_MSGRD_ERRINF_ACR_QID_SHIFT 16
#define APM_QMAN_CSR_MSGRD_ERRINF_ACR_QID(x)    \
    (((x) >> APM_QMAN_CSR_MSGRD_ERRINF_ACR_QID_SHIFT) & \
    APM_QMAN_CSR_MSGRD_ERRINF_ACR_QID_MASK)

/* Error information for APM_QMAN_CSR_INT_DEQ_AXI */

#define APM_QMAN_CSR_MSGRD_ERRINF_DEQERR_MASK       0x3 /* QPcore Drop Code */
#define APM_QMAN_CSR_MSGRD_ERRINF_DEQERR_SHIFT      8
#define APM_QMAN_CSR_MSGRD_ERRINF_DEQERR_DATA       2   /* Data Error */
#define APM_QMAN_CSR_MSGRD_ERRINF_DEQERR_ADRS       3   /* Address Error */

#define APM_QMAN_CSR_MSGRD_ERRINF_DEQERR_CODE(x)    \
    (((x) >> APM_QMAN_CSR_MSGRD_ERRINF_DEQERR_SHIFT) & \
    APM_QMAN_CSR_MSGRD_ERRINF_DEQERR_MASK)

#define APM_QMAN_CSR_MSGRD_ERRINF_DEQ_QID_MASK      0xFF
#define APM_QMAN_CSR_MSGRD_ERRINF_DEQ_QID(x)        \
    ((x) & APM_QMAN_CSR_MSGRD_ERRINF_DEQ_QID_MASK)

/*
 * Bit Definitions for APM_QMAN_CSR_QM_SAB_PPC0/APM_QMAN_CSR_QM_SAB_PPC1
 * Bit Definitions for APM_QMAN_CSR_QM_SAB_PPC0MASK/APM_QMAN_CSR_QM_SAB_PPC1MASK
 *
 * Each bit is corresponding to one APM_QMAN_CSR_PPC_SABn register.
 *
 * Note: For staus registers, read will clear the corresponding interrupt;
 * for mask registers, writing 1 will mask the corresponding interrupt.
 */

#define APM_QMAN_CSR_QM_SAB_PPC_INTn(n)     (1 << (7 - (n)))

/* Bit Definitions for APM_QMAN_CSR_QM_MBOX_NE */

#define APM_QMAN_CSR_QM_MBOX_NE_MBOXn(n)    (1 << (31 - (n)))

/*
 * Bit Definitions for APM_QMAN_CSR_PPC_SAB0/1/2/3/4/5/6/7
 *
 * Each APM_QMAN_CSR_PPC_SABn inludes the status of 32 queues.
 */

#define APM_QMAN_CSR_PPC_SABn(n)        \
    (APM_QMAN_CSR_PPC_SAB0 + (((n) >> 5) << 2))

#define APM_QMAN_CSR_PPC_SAB_QUEn(n)    (1 << (31 - ((n) & 31)))


/*
 * Bit Definitions for APM_QMAN_CSR_PB_SLAVE6/7
 *
 * Note: 36-bit system address must be right shifted by 12-bits and then
 * written to the registers.
 */

#define APM_QMAN_CSR_PB_SLAVE_ADRS_SHIFT    12
#define APM_QMAN_CSR_PB_SLAVE_ADRS_MASK     0x00FFFFFF

/* Bit Definitions for APM_QMAN_CSR_QM_STATS_CFG */

#define APM_QMAN_CSR_QM_STATS_CFG_QID_MASK      0xFF
#define APM_QMAN_CSR_QM_STATS_CFG_ENQID_SHIFT   24 /* for ENQ statistics */
#define APM_QMAN_CSR_QM_STATS_CFG_DEQID_SHIFT   16 /* for DEQ statistics */

/* Bit Definitions for APM_QMAN_CSR_FIFO_STATUS */

/*
 * This bit will be set when AVB credit update took longer than time period
 * programmed in APM_QMAN_CSR_CU_TIMER.
 *
 * This bit is cleaed by loading a new value into csr_cu_timer.
 */

#define APM_QMAN_CSR_FIFO_EPOCH_OV      0x80000000

#define APM_QMAN_CSR_FIFO_WAIT_MASK      0x1F /* Qpcore is waiting for */
#define APM_QMAN_CSR_FIFO_WAIT_SHIFT     22   /* one subsystem to ACK */
#define APM_QMAN_CSR_FIFO_WAIT_SAB       0x10
#define APM_QMAN_CSR_FIFO_WAIT_QSTATE    0x08
#define APM_QMAN_CSR_FIFO_WAIT_TM        0x04
#define APM_QMAN_CSR_FIFO_WAIT_MSGWR     0x02
#define APM_QMAN_CSR_FIFO_WAIT_MSGRD     0x01

#define APM_QMAN_CSR_FIFO_MSGWR_CNT_MASK  3  /* 0 to 4 outstanding */
#define APM_QMAN_CSR_FIFO_MSGWR_CNT_SHIFT 19 /* message writes on HBF */

#define APM_QMAN_CSR_FIFO_AWAIT_MSGWR_CNT_MASK  0xF /* 0 to 4 message writes */
#define APM_QMAN_CSR_FIFO_AWAIT_MSGWR_CNT_SHIFT 15  /* awaiting HBF response */

#define APM_QMAN_CSR_FIFO_ACTCMD_CNT_MASK  7  /* 0 to 4 active commands in */
#define APM_QMAN_CSR_FIFO_ACTCMD_CNT_SHIFT 12 /* msgrd dequeue process */

#define APM_QMAN_CSR_FIFO_MBMSG_CNT_MASK  0xF /* 0 to 8 PPC Mailbox message */
#define APM_QMAN_CSR_FIFO_MBMSG_CNT_SHIFT 12  /* fetches pending */

/* a one-hot vector indicating 0 to 8 enqueues in the service FIFO */

#define APM_QMAN_CSR_FIFO_ACR_ALLOCATED_MASK    0xFF
#define APM_QMAN_CSR_FIFO_ACR_ALLOCATEDn(n)     (1 << (n))

/* Bit Definitions for APM_QMAN_CSR_QM_RAM_MARGIN */

#define APM_QMAN_CSR_RAM_MARGIN_SET_MASK        0x3

/* Qstate RAM Margin A */

#define APM_QMAN_CSR_QRAM_MARGIN_EN_A         0x00002000 /* Enable */
#define APM_QMAN_CSR_QRAM_MARGIN_SET_A_SHIFT  10         /* Setting */

/* Qstate RAM Margin B */

#define APM_QMAN_CSR_QRAM_MARGIN_EN_B         0x00002000 /* Enable */
#define APM_QMAN_CSR_QRAM_MARGIN_SET_B_SHIFT  8          /* Setting */

/* Cstate RAM Margin A */

#define APM_QMAN_CSR_CRAM_MARGIN_EN_A         0x00000020 /* Enable */
#define APM_QMAN_CSR_CRAM_MARGIN_SET_A_SHIFT  2          /* Setting */

/* Cstate RAM Margin B */

#define APM_QMAN_CSR_CRAM_MARGIN_EN_B         0x00000010 /* Enable */
#define APM_QMAN_CSR_CRAM_MARGIN_SET_B_SHIFT  0          /* Setting */

/* Bit Definitions for APM_QMAN_CSR_QM_TESTINT */

#define APM_QMAN_CSR_TESTINT_MBOXm(m)   (1 << (31 - (m))) /* MBOX Interrupts */
#define APM_QMAN_CSR_TESTINT_QM         0x8000 /* QMan Interrupt */
#define APM_QMAN_CSR_TESTINT_SAB0       0x4000 /* SAB PPC0 Interrupt */
#define APM_QMAN_CSR_TESTINT_SAB1       0x2000 /* SAB PPC1 Interrupt */

/* QMAN HBF interface */

/* QMAN Enqueue interface */

#define APM_QMAN_ENQ_CMD_ADRS(queId)    (((queId) & APM_QMAN_QID_MASK) << 6)

#define APM_QMAN_INC_MBID_SHIFT         5
#define APM_QMAN_INC_SLOTID_SHIFT       2
#define APM_QMAN_INC_MSG(mbId, slotId, msgType) \
    ((((mbId) & APM_QMAN_MBID_MASK) << APM_QMAN_INC_MBID_SHIFT) | \
    (((slotId) & APM_QMAN_MBOX_SLOTID_MASK) << APM_QMAN_INC_SLOTID_SHIFT) | \
    ((msgType) & APM_QMAN_MSG_TYPE_MASK))

/* QMAN Dequeue interface */

#define APM_QMAN_DEC_CMD_ADRS               0x3c

#define APM_QMAN_DEC_SLVID_SHIFT            9
#define APM_QMAN_DEC_PBN_SHIFT              3
#define APM_QMAN_DEC_NUM_MASK               0x7
#define APM_QMAN_DEC_MSG(slvid, pbn, decNum) \
    ((((slvid) & APM_QMAN_SLVID_MASK) << APM_QMAN_DEC_SLVID_SHIFT) | \
    (((pbn) & APM_QMAN_PBN_MASK) << APM_QMAN_DEC_PBN_SHIFT) | \
    ((decNum) & APM_QMAN_DEC_NUM_MASK))

/* PQ filed of VQ state defination */

typedef union
    {
    UINT32  raw;

    struct
        {
        UINT32 qSel         : 8; /* Select one of the PQs that are mapped */
        UINT32 qReqVld      : 1; /* packets valid in this PQ */
        UINT32 qTxAllowed   : 1; /* If set with q0ReqVld, this queue will */
                                 /* be considered for the current */
                                 /* arbitration round. */
        UINT32 qSelArb      : 2; /* arbitration mode */
        } field;
    } APM_QMAN_QSTATE_CHILD_PQ;

/* QMAN Queue State defination */

typedef union
    {
    struct
        {
        UINT32 word0;
        UINT32 word1;
        UINT32 word2;
        UINT32 word3;
        } raw;

    /* Physical Queue and Free Pool */

    struct
        {
        UINT32 cfgQType         : 2; /* Queue Type */
        UINT32 cfgSelThrsh      : 3; /* Threshold Group Index */
        UINT32 qStateLock       : 1; /* Lock status (read only) */
        UINT32 cfgQSize         : 3; /* Queue Size */
        UINT32 fpMode           : 3; /* Free Pool Mode */
        UINT32 cfgAcceptLERR    : 1; /* Error messages acception enabled */
        UINT32 resv0            : 15;
        UINT64 cfgStartAdrs     : 28; /* High 28 bits of address */
        UINT32 resv1            : 8;
        UINT32 resv2            : 1;
        UINT32 headPtr          : 15; /* point to next message to be dequeued */
        UINT32 numMsg           : 16; /* number of messages in the queue */
        UINT32 resv3            : 5;
        UINT32 regID            : 3; /* Current Region ID of queue fill level */
        UINT32 cfgPPCSab        : 2; /* PPC Sab notification via interrupts */
        UINT32 cfgCRID          : 1; /* critical regID config */
                                     /* 0 - Almost full for PQ and almost */
                                     /* empty for FP */
                                     /* 1 - Full for PQ and empty for FP */
                                     /* Only valid when cfgSabEn = 1. */
        UINT32 cfgNotifyQNE     : 1; /* enable queue not empty interrupt */
                                     /* Only valid when cfgSabEn = 1. */
        UINT32 cfgSabEn         : 1; /* enable SAB broadcasting */
        UINT32 cfgtmVQ          : 8; /* ID of parent VQ */
        UINT32 cfgtmVQEn        : 1; /* enable PQ to belong to one VQ */
        UINT32 resv4            : 10;
        }_WRS_PACK_ALIGN (1) pqfp;

    /* Virtual Queue */

    struct
        {

        UINT32 cfgQType         : 2; /* Queue Type*/
        UINT32 cfgSelThrsh      : 3; /* Threshold Group Index*/

        UINT32 qSel0            : 8;
        UINT32 q0ReqVld         : 1;
        UINT32 q0TxAllowed      : 1;
        UINT32 q0SelArb         : 2;

        UINT32 qSel1            : 8;
        UINT32 q1ReqVld         : 1;
        UINT32 q1TxAllowed      : 1;
        UINT32 q1SelArb         : 2;

        UINT32 qSel2            : 8;
        UINT32 q2ReqVld         : 1;
        UINT32 q2TxAllowed      : 1;
        UINT32 q2SelArb         : 2;

        UINT32 qSel3            : 8;
        UINT32 q3ReqVld         : 1;
        UINT32 q3TxAllowed      : 1;
        UINT32 q3SelArb         : 2;

        UINT32 qSel4            : 8;
        UINT32 q4ReqVld         : 1;
        UINT32 q4TxAllowed      : 1;
        UINT32 q4SelArb         : 2;

        UINT32 qSel5            : 8;
        UINT32 q5ReqVld         : 1;
        UINT32 q5TxAllowed      : 1;
        UINT32 q5SelArb         : 2;

        UINT32 qSel6            : 8;
        UINT32 q6ReqVld         : 1;
        UINT32 q6TxAllowed      : 1;
        UINT32 q6SelArb         : 2;

        UINT32 qSel7            : 8;
        UINT32 q7ReqVld         : 1;
        UINT32 q7TxAllowed      : 1;
        UINT32 q7SelArb         : 2;

        UINT32 regID            : 3; /* Current Region ID of queue fill level */
        UINT32 cfgPPCSab        : 2; /* PPC Sab notification via interrupts */
        UINT32 cfgCRID          : 1; /* critical regID config */
                                     /* 0 - Almost full for PQ and almost */
                                     /* empty for FP */
                                     /* 1 - Full for PQ and empty for FP */
                                     /* Only valid when cfgSabEn = 1. */
        UINT32 cfgNotifyQNE     : 1; /* enable queue not empty interrupt */
                                     /* Only valid when cfgSabEn = 1. */
        UINT32 cfgSabEn         : 1; /* enable SAB broadcasting */
        UINT32 resv0            : 1;
        UINT32 numMsg           : 18; /* number of messages in the queue */
        } _WRS_PACK_ALIGN (1) vq;
    } APM_QMAN_QSTATE;

/* 8 PQs can be supported at most  */

#define APM_QMAN_VQ_NUM_PQ      8

/* cfgQType field defination */

#define APM_QMAN_QTYPE_DIS      0x0 /* Disabled Queue */
#define APM_QMAN_QTYPE_PQ       0x1 /* Physical Queue */
#define APM_QMAN_QTYPE_FP       0x2 /* Free Pool */
#define APM_QMAN_QTYPE_VQ       0x3 /* Virtual Queue */

/* cfgQSize field defination */

#define APM_QMAN_QSIZE_CODE_512B     0x0 /* 512 Bytes */
#define APM_QMAN_QSIZE_CODE_2KB      0x1 /* 2K Bytes */
#define APM_QMAN_QSIZE_CODE_16KB     0x2 /* 16K Bytes */
#define APM_QMAN_QSIZE_CODE_64KB     0x3 /* 64K Bytes */
#define APM_QMAN_QSIZE_CODE_512KB    0x4 /* 512K Bytes */

#define APM_QMAN_QSIZE_512B         0x200 /* 512 Bytes */
#define APM_QMAN_QSIZE_2KB          0x800 /* 2K Bytes */
#define APM_QMAN_QSIZE_16KB         0x4000 /* 16K Bytes */
#define APM_QMAN_QSIZE_64KB         0x10000 /* 64K Bytes */
#define APM_QMAN_QSIZE_512KB        0x80000 /* 512K Bytes */

/* fpMode field defination (used when message is returned to Free Pool ) */

#define APM_QMAN_FPMODE_0       0x0 /* No modification */
#define APM_QMAN_FPMODE_1       0x1 /* realigned dataAdrs to buffer start and */
                                    /* reset BufDataLen to BufSize */
#define APM_QMAN_FPMODE_2       0x2 /* Reduce BufDataLen to */
                                    /* (BufSize - DataAddr(LSBs)). */
#define APM_QMAN_FPMODE_3       0x3 /* reset BufDataLen to BufSize */

/* cfgStartAdrs field defination */

#define APM_QMAN_QUE_ADRS_ALIGN     256

#define APM_QMAN_QUE_ADRS_SHIFT     8
#define APM_QMAN_QUE_ADRS_MASK      0xFFFFFFF
#define APM_QMAN_QUE_ADRS(x)        \
    (((x) >> APM_QMAN_QUE_ADRS_SHIFT) & APM_QMAN_QUE_ADRS_MASK)

/* headPtr field defination */

#define APM_QMAN_QUE_HEAD_PTR_SHIFT 4 /* HeadPtr is 16 Byte aligned */

/* numMsg field defination */

#define APM_QMAN_FP_MSG_SIZE    4 /* Message Numebr in Free Pool is */
                                  /* calculated by 16 Bytes */
#define APM_QMAN_PQ_MSG_SIZE    8 /* Message Numebr in Physical Queue is */
                                  /* calculated by 32 Bytes */

/* regID field defination(queue fill level) */

#define APM_QMAN_QUE_RID_EMPTY      0
#define APM_QMAN_QUE_RID_EMPTY_NEAR 1
#define APM_QMAN_QUE_RID_NORMAL     2
#define APM_QMAN_QUE_RID_FULL_NEAR  3
#define APM_QMAN_QUE_RID_FULL       4

/* cfgPPCSab field defination */

#define APM_QMAN_QUE_SAB_PPC0       0x1 /* notify CPU0 */
#define APM_QMAN_QUE_SAB_PPC1       0x2 /* notify CPU1 */

/* QxSelArb field defination */

#define APM_QMAN_QUE_ARB_DIS        0x0 /* disabled for dequeue */
#define APM_QMAN_QUE_ARB_PRI_SP     0x1 /* strict priority Arbiter */
#define APM_QMAN_QUE_ARB_PRI_DRR    0x2 /* Deficit Round Robin Arbiter  */

/* QMAN Queue TM Credit State defination */

typedef union
    {
    struct
        {
        UINT32 word0;
        UINT32 word1;
        } raw;

    struct
        {
        UINT64 resv         : 21;
        UINT64 initCredit   : 21; /* Higher Credit relative to this PQ in the */
                                  /* VQ will give this PQ more bandwidth */
        UINT64 curCredit    : 22;
        } _WRS_PACK_ALIGN (1) field;
    } APM_QMAN_CSTATE;

/* QMAN queue information  */

typedef struct
    {
    UINT8               slaveId;     /* Destination sub-system ID for egress  */
                                     /* queue or PPC sub-system ID filled by */
                                     /* this driver for ingress queue  */
    UINT8               slaveUnitId; /* used when one sub-system has several */
                                     /* instances such as PPC core, ethernet */
                                     /* ports */
    UINT8               queType;     /* refer to cfgQType field */
    UINT8               queDir;      /* 0 - ingress 1 - egress */
    UINT32              queSize;     /* valid for PQ/FP only */
    UINT32              numMsgMax;   /* number of messages supported at most */

    UINT8               queId;
    UINT8               mboxId;      /* For ingress PQ/VQ, it is just pbn; */
                                     /* For ingress FP, it is index of FP */
                                     /* allocation Mailbox; */
                                     /* For ingress PQ that belongs to one */
                                     /* VQ, it is invalid; */
                                     /* For egress PQ/VQ/FP, it can be anyone */
    UINT8               pbn;         /* For egress PQ/VQ/FP, it is index of */
                                     /* prefetch buffer that the queue is */
                                     /* related with; */
                                     /* For ingress PQ/VQ, it is index of */
                                     /* PPC dequeue mailbox; */
                                     /* For ingress FP, it is index of PPC FP */
                                     /* prefetch buffer; */
                                     /* For PQ that belongs to one VQ, it is */
                                     /* invalid */

    /* valid for PQ only */

    UINT8               parentVqEn;
    UINT8               parentVqId;
    UINT8               pqNum; /* number of PQs that belong to the VQ */
    UINT8               resv[2];

    /* valid for VQ only */

    UINT8               qSel[APM_QMAN_VQ_NUM_PQ];    /* ID of child PQ */
    UINT8               qSelArb[APM_QMAN_VQ_NUM_PQ]; /* Arbitration of child */
                                                     /*PQ */

    /*
     * initial credit value for PQs that use DRR Arbiter
     *
     * The initial credit value assigns a weight to each child PQs, then the
     * ratio between the total number of bytes dequeued from each PQ will be the
     * ratio of the initial credit value.
     *
     * For maximum accuracy, each initial credit value should be multiple of
     * the maximum expected packet size for each PQ.
     */

    UINT32              qSelCredit[APM_QMAN_VQ_NUM_PQ];

    APM_QMAN_QSTATE     qState;
    } APM_QMAN_QUE_INFO;

/* QMAN Prefetech Buffer State defination */

typedef union
    {
    UINT32 raw;

    struct
        {
        UINT32 overwrite    : 1; /* enable overwrite to slotNum and numMsg */
        UINT32 resv0        : 12;
        UINT32 inServiceClr : 1; /* clear PB inservice status */
        UINT32 slotNum      : 3; /* Next Available Slot Number */
        UINT32 en           : 1; /* Enable PB to allow dequeues to happen */
        UINT32 isFp         : 1; /* Is Free Pool */
        UINT32 tlvq         : 1; /* Is VQ */
        UINT32 qid          : 8; /* Queue ID to be assigned to this PB */
        UINT32 numMsg       : 4; /* number of messages in PB */
        } wr;

    struct
        {
        UINT32 resv0        : 13;
        UINT32 inService    : 1; /* PB is inservice */
        UINT32 slotNum      : 3; /* Next Available Slot Number */
        UINT32 en           : 1; /* Enable PB to allow dequeues to happen */
        UINT32 isFp         : 1; /* Ss Free Pool */
        UINT32 tlvq         : 1; /* Is VQ */
        UINT32 qid          : 8; /* Queue ID to be assigned to this PB */
        UINT32 numMsg       : 4; /* number of messages in PB */
        } rd;
    } APM_QMAN_PB_STATE;

/* QMAN Prefetech Buffer information  */

typedef struct
    {
    UINT8               slaveId;
    UINT8               pbn;
    APM_QMAN_PB_STATE   pb;
    } APM_QMAN_PB_INFO;

/* QMAN Message defination */

/* Common Bytes (first 16 Bytes) */

typedef struct
    {
    UINT32 resv0        : 6;  /* Reserved bits */
    UINT32 srcDomID     : 2;  /* source domain ID(Reserved on */
                              /* APM86290/APM86190) */
    UINT32 l2Snoop      : 1;  /* snoop to L2 Cache when reading/writing */
    UINT32 bufDataLen   : 15;
    UINT64 dataAdrs     : 40; /* host memory address points to data start for */
                              /* message of WQ or buffer start of empty */
                              /* buffer for FP */

    UINT32 HL           : 1; /* Hold last buffer */
    UINT32 LErr         : 3; /* Local Error information */
    UINT32 RType        : 4; /* Resource Type used by SW and unmodified by HW */
    UINT32 PV           : 1; /* Prefetch Valid(used when S) */
    UINT32 SB           : 1; /* Prefetch State Buffer */
    UINT32 HB           : 1; /* Prefetch Header Buffer */
    UINT32 PB           : 1; /* Prefetch Packet Buffers */
    UINT32 LL           : 1; /* Link List Valid(used for more than 5 buffers) */
    UINT32 NV           : 1; /* Next Buffer Addr Valid(used for more than 1 */
                             /* buffers)*/
    UINT32 HC           : 2; /* Hop Count */
    UINT32 ELErr        : 2; /* Local Error information expansion(Reserved) */
    UINT32 resv1        : 6; /* Reserved bits */
    UINT32 FPQNum       : 8; /* ID of FP Queue to which the first buffer */
                             /* should be returned after the last Consumer is */
                             /* done processing the contents. */

    union
        {
        UINT32          raw;

        /* encodings for Enqueue Error message built by QMan (LErr == 6) */

        struct
            {
            UINT32 err          : 3;
            UINT32 resv0        : 5;  /* Reserved Bits */
            UINT32 cmdAcrEnqErr : 2;
            UINT32 resv1        : 14; /* Reserved Bits */
            UINT32 cmdAcrEnqQid : 8;  /* QID of original request */
            } enqErr;

        /* encodings for Dequeue Error message built by QMan (LErr == 7) */

        struct
            {
            UINT32 err          : 3;
            UINT32 resv0        : 16; /* Reserved Bits */
            UINT32 deqSlvId     : 4;  /* slave ID of requester */
            UINT32 deqPbn       : 6;  /* PBN of requester */
            UINT32 deqSlotNum   : 3;  /* PPC mailbox slot number */
            } deqErr;
        } userInfo;
    }_WRS_PACK_ALIGN (1) APM_QMAN_MSG_COMMON, APM_QMAN_MSG_16B;

/* HOP information */

typedef struct
    {
    UINT32 resv0      : 2;  /* Reserved Bits */
    UINT32 HopFPSel   : 4;  /* PBN used by Hop Consumer to get buffers */
    UINT32 HR         : 1;  /* HopFPSel enabled */
    UINT32 HE         : 1;  /* Hop enabled */
    UINT32 hopDR      : 1;  /* If not set, then the HopInfo field contains a */
                            /* pointer to state information stored. If set, */
                            /* then HopInfo contains literal information */
    UINT32 hopSZ      : 1;  /* see the following comments */
    UINT32 resv1      : 6;  /* Reserved Bits */
    UINT32 HopEnqNum  : 8;  /* ID of Queue used to store result for Hop */

    union
        {

        /* 40-bit literal information */

        struct
            {
            UINT32 hi  : 8;
            UINT32 lo;
            } _WRS_PACK_ALIGN (1) raw;

        /* 4-bit command and 36-bit memory address */

        struct
            {
            UINT32 cmd    : 4;
            UINT64 adrs   : 36;
            } _WRS_PACK_ALIGN (1) cmdAdrs;

        /* 8-bit command and 32-bit pointer to memory */

        struct
            {
            UINT32 cmd    : 8;
            void * ptr;
            } _WRS_PACK_ALIGN (1) cmdPtr;

         /* Ethernet sub-system H0Info */

        struct
            {
            UINT32 type     : 4;
            UINT32 IC       : 1; /* Insert CRC */
            UINT32 IV       : 1; /* Insert VLAN ID */
            UINT32 resv     : 5;
            UINT32 ED       : 1;
            UINT32 EM       : 1;
            UINT32 ES       : 1;
            UINT32 V        : 1; /* IP Version */
            UINT32 IS       : 1; /* IP Protocol Select(TCP or UDP) */
            UINT32 ET       : 1; /* Enable TSO */
            UINT32 EC       : 1; /* Enable TSO Checksum */
            UINT32 MSS      : 2; /* MSS Select */
            UINT32 EthHdr   : 8; /* Ethernet Header */
            UINT32 IPHdr    : 6; /* IP Header */
            UINT32 TCPHdr   : 6; /* TCP Header */
            } _WRS_PACK_ALIGN (1) ethH0Info;

        /* Ethernet sub-system H1Info */

        struct
            {
            UINT32 resv     : 30;
            UINT32 MM       : 1;
            UINT32 IM       : 1;
            UINT32 IPSecSAP : 8;
            } _WRS_PACK_ALIGN (1) ethH1Info;

        } _WRS_PACK_ALIGN (1) hopInfo;
    }APM_QMAN_MSG_HOP_INFO;

/* Next Buffer information */

typedef struct
    {
    UINT32 nextFPId         : 8; /* ID of FP Queue to which the next buffer */
                                 /* should be returned after the last */
                                 /* Consumer is done processing the contents */
    UINT32 resv0            : 1;
    UINT32 nextBufDataLen   : 15;
    UINT32 resv1            : 4;
    UINT64 nextDataAdrs     : 36;
    } _WRS_PACK_ALIGN (1) APM_QMAN_MSG_NEXT_BUF_INFO;

/*
 * Next Link information
 *
 * Note: the item of link list is defined by APM_QMAN_MSG_NEXT_BUF_INFO.
 */

typedef struct
    {
    UINT32 totalLengthOfLink    : 20; /* total data length of all the items */
                                      /* in the link list */
    UINT32 linkSize             : 8;  /* number of items in the link list */
    UINT64 linkListAdrs         : 36; /* address of link list */
    } _WRS_PACK_ALIGN (1) APM_QMAN_MSG_NEXT_LINK_INFO;

/* QMan Messsage defination */

#define APM_QMAN_MSG_NEXT_BUF_MAX   4
#define APM_QMAN_MSG_LLBUF_MAX      255 /* 255 items in link list at most  */
#define APM_QMAN_MSG_LLBUF_SIZE     (256 * sizeof (APM_QMAN_MSG_NEXT_BUF_INFO))
#define APM_QMAN_MSG_HC_MAX         2

typedef struct
    {
    APM_QMAN_MSG_COMMON     common;
    APM_QMAN_MSG_HOP_INFO   hopInfo[APM_QMAN_MSG_HC_MAX];
    } APM_QMAN_MSG_32B;

typedef struct
    {
    APM_QMAN_MSG_COMMON     common;
    APM_QMAN_MSG_HOP_INFO   hopInfo[APM_QMAN_MSG_HC_MAX];

    union
        {
         APM_QMAN_MSG_NEXT_BUF_INFO nextBuf[APM_QMAN_MSG_NEXT_BUF_MAX];

         struct
            {
            APM_QMAN_MSG_NEXT_BUF_INFO  nextBuf[APM_QMAN_MSG_NEXT_BUF_MAX - 1];
            APM_QMAN_MSG_NEXT_LINK_INFO llInfo;
            } nextBufLink;
        } ext32;
    } APM_QMAN_MSG_64B;

/*
 * bufDataLen/nextBufDataLen field defination
 *
 * BufDataLength[14] == 0 ? Buffer is 16KB
 * BufDataLength[14:12] == 3b100 ? Buffer is 4KB
 * BufDataLength[14:12] == 3b101 ? Buffer is 2KB
 * BufDataLength[14:12] == 3b110 ? Buffer is 1KB
 * BufDataLength[14:12] == 3b111 ? Buffer is 256B
 *
 * Note: if data length filed is 0, data size is just buffer length.
 */

#define APM_QMAN_MSG_BUFLEN_MASK_16K    0x4000
#define APM_QMAN_MSG_DATALEN_16K(x)     ((x) & 0x3FFF)

#define APM_QMAN_MSG_BUFLEN_MASK        0x7000
#define APM_QMAN_MSG_BUFLEN_4K          0x4000
#define APM_QMAN_MSG_BUFLEN_2K          0x5000
#define APM_QMAN_MSG_BUFLEN_1K          0x6000
#define APM_QMAN_MSG_BUFLEN_256         0x7000
#define APM_QMAN_MSG_BUFLEN_LASTBUF     0x7800 /* used when previous buffer */
                                               /* is last buffer */
#define APM_QMAN_MSG_DATALEN(x)         ((x) & 0x0FFF)

/* LErr field defination */

#define APM_QMAN_MSG_LERR_NO        0

/* Common */

#define APM_QMAN_MSG_LERR_SIZE      1 /* size error(Either an FP received a */
                                      /* 32-byte message, or a PQ received a */
                                      /* 16-byte message */
#define APM_QMAN_MSG_LERR_HC        2 /* Hop count error(QM received a */
                                      /* message with a hop count of 3 */
#define APM_QMAN_MSG_LERR_ENQ2VQ    3 /* Enqueue to a VQ */
#define APM_QMAN_MSG_LERR_ENQ2DQ    4 /* Enqueue to a disabled Queue */
#define APM_QMAN_MSG_LERR_OF        5 /* Queue would have be overfilled, so */
                                      /* Message is sent to error queue */
                                      /* instead */
#define APM_QMAN_MSG_LERR_ENQ       6 /* Enqueue Error message built by QMan */
#define APM_QMAN_MSG_LERR_DEQ       7 /* Dequeue Error message built by QMan */

/* Ethernet */

#define APM_QMAN_MSG_LERR_ETH_AXIWR     1 /* AXI write data error due to RSIF */
#define APM_QMAN_MSG_LERR_ETH_INGCRC    2 /* Rx packet had CRC */
#define APM_QMAN_MSG_LERR_ETH_AXIRD     3 /* HBF read data error when */
                                          /* processing a work message in */
                                          /* TSIF */
#define APM_QMAN_MSG_LERR_ETH_LLRD      4 /* HBF Link List read error when */
                                          /* processing a work message in */
                                          /* TSIF */
#define APM_QMAN_MSG_LERR_ETH_ING       5 /* Rx packet had ingress processing */
                                          /* error */
#define APM_QMAN_MSG_LERR_ETH_BADMSG    6 /* Bad message to subsytem */
#define APM_QMAN_MSG_LERR_ETH_MISC      7 /* Other ingress processing error */

/* userinfo encodings for Enqueue Error message built by QMan (LErr == 6) */

#define APM_QMAN_MSG_LERR_ENQ_ERR_HBFRD 0 /* HBF read error for PPC mailbox */
#define APM_QMAN_MSG_LERR_ENQ_ERR_VQ    3 /* Alternate Enqueue Command to VQ */
#define APM_QMAN_MSG_LERR_ENQ_ERR_DQ    4 /* Alternate Enqueue Command to */
                                          /* Disabled Queue */
#define APM_QMAN_MSG_LERR_ENQ_ERR_OF    5 /* Alternate Enqueue Command */
                                          /* overfills a Queue */

#define APM_QMAN_MSG_LERR_ENQ_ACR_NO    0 /* no HBF error on PPC mailbox read */
#define APM_QMAN_MSG_LERR_ENQ_ACR_SLV   1 /* HBF slave error on mailbox read */
#define APM_QMAN_MSG_LERR_DEQ_ACR_SLV   2 /* HBF decode error on mailbox read */

/* u encodings for Dequeue Error message built by QMan (LErr == 7) */

#define APM_QMAN_MSG_LERR_DEQ_VQ        6 /* VQ was assigned as child of */
                                          /* another VQ  */
#define APM_QMAN_MSG_LERR_DEQ_DQ        7 /* dequeue was requested from a */
                                          /* disabled PQ */

/* QMAN Message RType usage */

#define APM_QMAN_MSG_RTYPE_CORE         0
#define APM_QMAN_MSG_RTYPE_ETH0         1
#define APM_QMAN_MSG_RTYPE_ETH1         2
#define APM_QMAN_MSG_RTYPE_SEC          3
#define APM_QMAN_MSG_RTYPE_DMA          4

#define APM_QMAN_MSG_RTYPE_RESV         15 /* reserved for dequeue/allocate */
                                           /*operation */
#define APM_QMAN_MSG_RTYPE_NUM          16

/* Used to initialize mailbox slot */

#define APM_QMAN_MSG_RTYPE_OFFSET_IN_WD 2
#define APM_QMAN_MSG_RTYPE_IN_WD(x)     (((x) & 0xF) << 24)

/*
 * HC field defination
 *
 * This field is used to insure that a message does not get inadvertently
 * programmed into an endless loop of next hop processing. This field is
 * incremented by the QMTM whenever it dequeues the message and sends it to a
 * subsystem for processing, and is checked by the QMTM before it enqueues the
 * message to a queue. It expects a value of less than 3 in this field. If it
 * encounters a value of 3, then the QMTM sends this message to the Error Queue.
 *
 * Note: This field should be copied unmodified by all subsystems other than
 * QMan regardless of whether the message is being written to a Completion Queue
 * or Free Queue.
 */

/*
 * hopFPSel and HR field defination
 *
 * The Hop0 Consumer will get buffers from PBN indicated in HopFPSel if the HR
 * bit is set and the Hop Consumer needs buffers to store its results. If the HR
 * bit is not set and the Hop Consumer needs buffers to store its results then
 * it writes the results back to the incoming buffer.
 *
 * Note: Subsystems implement a maximum of 16 PBN for Free Pools and should
 * ignore the two msbs of HopFPSel.
 */

#define APM_QMAN_MSG_HOPFP_MASK 0xF

/*
 * hopSZ field defination
 *
 * If the hopDR is set, then this bit provides further information about how the
 * literal field in the H0Info field should be interpreted.
 *
 * If the hopDR bit is not set, then this bit indicates the size of the state
 * information associated with the Hop. If the hopSZ bit is 0, then the state
 * information is 256 bytes. If the hopSZ bit is 1, then the state information
 * is 1KBytes.
 */

#define APM_QMAN_MSG_HOPSZ(x)   (256 << ((x) << 1))

/* Ethernet sub-system H0Info type field defination */

#define APM_QMAN_MSG_ETHH0_TYPE_TIMESTAMP   0
#define APM_QMAN_MSG_ETHH0_TYPE_WORK        1

/* Ethernet sub-system H0Info IP Protocol Select field defination */

#define APM_QMAN_MSG_ETHH0_IS_UDP           0
#define APM_QMAN_MSG_ETHH0_IS_TCP           1

/* Ethernet sub-system H0Info MSS Select field defination */

#define APM_QMAN_MSG_ETHH0_MSS_64B          0 /* Default size 64B */
#define APM_QMAN_MSG_ETHH0_MSS_256B         1 /* Default size 256B */
#define APM_QMAN_MSG_ETHH0_MSS_1024B        2 /* Default size 1024B */
#define APM_QMAN_MSG_ETHH0_MSS_1518B        3 /* Default size 1518B */
#define APM_QMAN_MSG_ETHH0_MSS(x)           ((x) & 3)

/* QMAN message information  */

typedef struct
    {
    UINT8               mboxId;
    UINT8               pbn; /* used for dequeue/allocate operation */
    UINT8               qid; /* used for enqueue/deallocate operation */
    UINT8               qType;
    UINT8               msgType; /* used for enqueue/deallocate operation */
    void *              msg;
    } APM_QMAN_MSG_INFO;

/* QMAN function list */

typedef struct
    {
    STATUS  (*queQry)(VXB_DEVICE_ID, APM_QMAN_QUE_INFO *);
    STATUS  (*pbQry)(VXB_DEVICE_ID, APM_QMAN_PB_INFO *);
    STATUS  (*queAlloc)(VXB_DEVICE_ID, APM_QMAN_QUE_INFO *);
    STATUS  (*queFree)(VXB_DEVICE_ID, APM_QMAN_QUE_INFO *);
    STATUS  (*enque)(VXB_DEVICE_ID, APM_QMAN_MSG_INFO *);
    STATUS  (*deque)(VXB_DEVICE_ID, APM_QMAN_MSG_INFO *);
    STATUS  (*mbIntEn)(VXB_DEVICE_ID, UINT8, VOIDFUNCPTR, void *, UINT32);
    STATUS  (*mbIntDis)(VXB_DEVICE_ID, UINT8, VOIDFUNCPTR, void *, UINT32);
    } APM_QMAN_FUNC;

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbApmPProQmanh */
