/* vxbQorIQLaw.h - Local access window driver header for QorIQ QorIQ */

/*
 * Copyright (c) 2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
DESCRIPTION

This file contains register information for the Local Access Window registers
on the Freescale QorIQ integrated processor. The LAW registers are used to
create mappings used to access various in-core devices in the processor,
including the PCI bridges, RapidIO interface, buffer manager and queue manager.

There are 32 sets of registers, allowing up to 32 separate mappings.
*/

/*
modification history
--------------------
01a,02dec08,wap  written
*/

#ifndef __INCvxbQorIQLawh
#define __INCvxbQorIQLawh

#ifdef __cplusplus
extern "C" {
#endif

#define QORIQ_LAWBARH0		0x0000
#define QORIQ_LAWBARL0		0x0004
#define QORIQ_LAWAR0		0x0008
#define QORIQ_LAWBARH1		0x0010
#define QORIQ_LAWBARL1		0x0014
#define QORIQ_LAWAR1		0x0018
#define QORIQ_LAWBARH2		0x0020
#define QORIQ_LAWBARL2		0x0024
#define QORIQ_LAWAR2		0x0028
#define QORIQ_LAWBARH3		0x0030
#define QORIQ_LAWBARL3		0x0034
#define QORIQ_LAWAR3		0x0038
#define QORIQ_LAWBARH4		0x0040
#define QORIQ_LAWBARL4		0x0044
#define QORIQ_LAWAR4		0x0048
#define QORIQ_LAWBARH5		0x0050
#define QORIQ_LAWBARL5		0x0054
#define QORIQ_LAWAR5		0x0058
#define QORIQ_LAWBARH6		0x0060
#define QORIQ_LAWBARL6		0x0064
#define QORIQ_LAWAR6		0x0068
#define QORIQ_LAWBARH7		0x0070
#define QORIQ_LAWBARL7		0x0074
#define QORIQ_LAWAR7		0x0078
#define QORIQ_LAWBARH8		0x0080
#define QORIQ_LAWBARL8		0x0084
#define QORIQ_LAWAR8		0x0088
#define QORIQ_LAWBARH9		0x0090
#define QORIQ_LAWBARL9		0x0094
#define QORIQ_LAWAR9		0x0098
#define QORIQ_LAWBARH10		0x00A0
#define QORIQ_LAWBARL10		0x00A4
#define QORIQ_LAWAR10		0x00A8
#define QORIQ_LAWBARH11		0x00B0
#define QORIQ_LAWBARL11		0x00B4
#define QORIQ_LAWAR11		0x00B8
#define QORIQ_LAWBARH12		0x00C0
#define QORIQ_LAWBARL12		0x00C4
#define QORIQ_LAWAR12		0x00C8
#define QORIQ_LAWBARH13		0x00D0
#define QORIQ_LAWBARL13		0x00D4
#define QORIQ_LAWAR13		0x00D8
#define QORIQ_LAWBARH14		0x00E0
#define QORIQ_LAWBARL14		0x00E4
#define QORIQ_LAWAR115		0x00E8
#define QORIQ_LAWBARH15		0x00F0
#define QORIQ_LAWBARL15		0x00F4
#define QORIQ_LAWAR15		0x00F8
#define QORIQ_LAWBARH16		0x0000
#define QORIQ_LAWBARL16		0x0004
#define QORIQ_LAWAR16		0x0008
#define QORIQ_LAWBARH17		0x0010
#define QORIQ_LAWBARL17		0x0014
#define QORIQ_LAWAR17		0x0018
#define QORIQ_LAWBARH18		0x0020
#define QORIQ_LAWBARL18		0x0024
#define QORIQ_LAWAR18		0x0028
#define QORIQ_LAWBARH19		0x0030
#define QORIQ_LAWBARL19		0x0034
#define QORIQ_LAWAR19		0x0038
#define QORIQ_LAWBARH20		0x0040
#define QORIQ_LAWBARL20		0x0044
#define QORIQ_LAWAR20		0x0048
#define QORIQ_LAWBARH21		0x0050
#define QORIQ_LAWBARL21		0x0054
#define QORIQ_LAWAR21		0x0058
#define QORIQ_LAWBARH22		0x0060
#define QORIQ_LAWBARL22		0x0064
#define QORIQ_LAWAR22		0x0068
#define QORIQ_LAWBARH23		0x0070
#define QORIQ_LAWBARL23		0x0074
#define QORIQ_LAWAR23		0x0078
#define QORIQ_LAWBARH24		0x0080
#define QORIQ_LAWBARL24		0x0084
#define QORIQ_LAWAR24		0x0088
#define QORIQ_LAWBARH25		0x0090
#define QORIQ_LAWBARL25		0x0094
#define QORIQ_LAWAR25		0x0098
#define QORIQ_LAWBARH26		0x00A0
#define QORIQ_LAWBARL26		0x00A4
#define QORIQ_LAWAR26		0x00A8
#define QORIQ_LAWBARH27		0x00B0
#define QORIQ_LAWBARL27		0x00B4
#define QORIQ_LAWAR27		0x00B8
#define QORIQ_LAWBARH28		0x00C0
#define QORIQ_LAWBARL28		0x00C4
#define QORIQ_LAWAR28		0x00C8
#define QORIQ_LAWBARH29		0x00D0
#define QORIQ_LAWBARL29		0x00D4
#define QORIQ_LAWAR29		0x00D8
#define QORIQ_LAWBARH30		0x00E0
#define QORIQ_LAWBARL30		0x00E4
#define QORIQ_LAWAR30		0x00E8
#define QORIQ_LAWBARH31		0x00F0
#define QORIQ_LAWBARL31		0x00F4
#define QORIQ_LAWAR31		0x00F8

#define QORIQ_LAWAR_ENABLE	0x80000000 /* Window enabled */
#define QORIQ_LAWAR_TGTID	0x0FF00000 /* Target ID */
#define QORIQ_LAWAR_CSDID	0x000FF000 /* Coherency subdomain ID */
#define QORIQ_LAWAR_SIZE	0x0000003F /* Window size */

#define QORIQ_LAWSIZE_4KB	0x0000000B
#define QORIQ_LAWSIZE_8KB	0x0000000C
#define QORIQ_LAWSIZE_16KB	0x0000000D
#define QORIQ_LAWSIZE_32KB	0x0000000E
#define QORIQ_LAWSIZE_64KB	0x0000000F
#define QORIQ_LAWSIZE_128KB	0x00000010
#define QORIQ_LAWSIZE_256KB	0x00000011
#define QORIQ_LAWSIZE_512KB	0x00000012
#define QORIQ_LAWSIZE_1MB	0x00000013
#define QORIQ_LAWSIZE_2MB	0x00000014
#define QORIQ_LAWSIZE_4MB	0x00000015
#define QORIQ_LAWSIZE_8MB	0x00000016
#define QORIQ_LAWSIZE_16MB	0x00000017
#define QORIQ_LAWSIZE_32MB	0x00000018
#define QORIQ_LAWSIZE_64MB	0x00000019
#define QORIQ_LAWSIZE_128MB	0x0000001A
#define QORIQ_LAWSIZE_256MB	0x0000001B
#define QORIQ_LAWSIZE_512MB	0x0000001C
#define QORIQ_LAWSIZE_1GB	0x0000001D
#define QORIQ_LAWSIZE_2GB	0x0000001E
#define QORIQ_LAWSIZE_4GB	0x0000001F
#define QORIQ_LAWSIZE_8GB	0x00000020
#define QORIQ_LAWSIZE_16GB	0x00000021
#define QORIQ_LAWSIZE_32GB	0x00000022
#define QORIQ_LAWSIZE_64GB	0x00000023
#define QORIQ_LAWSIZE_128GB	0x00000024

#define QORIQ_TGTID(x)		(((x) << 20) & QORIQ_LAWAR_TGTID)
#define QORIQ_CSDID(x)		(((x) << 12) & QORIQ_LAWAR_CSDID)

#define QORIQ_LAW_CNT		32
#define QORIQ_LAW_SIZE		16
#define QORIQ_LAW_MIN		0

#define QORIQ_LAW_NAME	"QorIQLaw"

typedef struct law_handle
    {
    void *		lawBase;	/* Window base address */
    UINT32		lawSize;	/* Window size */
    int			lawTgt;		/* Window target ID */ 
    int			lawCsd;
    int			lawIdx;		/* Array index */
    VXB_DEVICE_ID	lawDev;		/* Device using the window */
    } LAW_HANDLE;

typedef struct law_drv_ctrl
    {
    VXB_DEVICE_ID       lawDev;
    void *              lawBar;
    void *              lawHandle;
    SEM_ID              lawSem;
    LAW_HANDLE		lawArray[QORIQ_LAW_CNT];
    int			lawCnt;
    } LAW_DRV_CTRL;

#define LAW_BAR(p)   ((LAW_DRV_CTRL *)(p)->pDrvCtrl)->lawBar
#define LAW_HANDLE(p)   ((LAW_DRV_CTRL *)(p)->pDrvCtrl)->lawHandle

#define _LAW_CSRREAD_4(pDev, addr)                                  \
    vxbRead32 (LAW_HANDLE(pDev), (UINT32 *)((char *)LAW_BAR(pDev) + addr))

#define _LAW_CSRWRITE_4(pDev, addr, data)                           \
    vxbWrite32 (LAW_HANDLE(pDev),                             \
        (UINT32 *)((char *)LAW_BAR(pDev) + addr), data)

#define LAW_READ(pDev, idx, addr)		\
	_LAW_CSRREAD_4(pDev, addr + (idx * QORIQ_LAW_SIZE))

#define LAW_WRITE(pDev, idx, addr, data)	\
	_LAW_CSRWRITE_4(pDev, addr + (idx * QORIQ_LAW_SIZE), data)

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbQorIQLawh */
