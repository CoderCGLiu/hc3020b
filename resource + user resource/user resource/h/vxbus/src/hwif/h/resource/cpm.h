/* cpmDrv.h - header file for PowerQUICC CPM driver */

/*
 * Copyright (c) 2006 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,01aug07,wap  Convert to new register access method (WIND00100585)
01a,03aug06,wap  written
*/

#ifndef __INCcpmDrvh
#define __INCcpmDrvh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void cpmRegister (void);
IMPORT STATUS cpmCommand (UINT8 cmd, UINT8 block, UINT8 page, UINT8 channel);

#ifndef BSP_VERSION

/* Private instance context */

typedef struct cpm_drv_ctrl
    {
    VXB_DEVICE_ID       cpmDev;
    void *		cpmBar;
    void *		cpmHandle;
    SEM_ID		cpmSem;
    } CPM_DRV_CTRL;

#define CPM_BAR(p)   ((CPM_DRV_CTRL *)(p)->pDrvCtrl)->cpmBar
#define CPM_HANDLE(p)   ((CPM_DRV_CTRL *)(p)->pDrvCtrl)->cpmHandle

#define CSR_READ(pDev, addr)                                  \
    vxbRead32 (CPM_HANDLE(pDev), (UINT32 *)((char *)CPM_BAR(pDev) + addr))

#define CSR_WRITE(pDev, addr, data)                           \
    vxbWrite32 (CPM_HANDLE(pDev),                             \
        (UINT32 *)((char *)CPM_BAR(pDev) + addr), data)

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCmdioDrvh */

