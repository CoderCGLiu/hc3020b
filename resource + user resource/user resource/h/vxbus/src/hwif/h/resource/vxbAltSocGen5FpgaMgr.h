/* vxbAltSocGen5FpgaMgr.h - FPGA Manager driver hardware defintions */

/*
 * Copyright (c) 2013, 2015 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
25may15,swu  fixed FPGA_to_HPS SDRAM interface issue. (VXW6-84221)
13aug13,swu  created
*/

#ifndef __INCvxbAltSocGen5FpgaMgrh
#define __INCvxbAltSocGen5FpgaMgrh

#ifdef __cplusplus
extern "C" {
#endif

#define FPGA_MGR_DRIVER_NAME    "fpgaMgr"

#define FPGA_MGR_RESET_TIMEOUT  2048

/* CRC32 polynomial */

#define FPGA_MGR_CRC32_CALC_VAL 0xedb88320

/* header size */

#define FPGA_MGR_EXTRA_HEADER_SIZE      (4)
#define FFPGA_MGR_EXTRA_HEADER_CRC      (4)

#define FPGA_MGR_STATE          0x0
#define FPGA_MGR_CTRL           0x4
#define FPGA_MGR_DCLK_CNT       0x8
#define FPGA_MGR_DCLK_STATE     0xC
#define FPGA_MGR_GPO            0x10
#define FPGA_MGR_GPI            0x14
#define FPGA_MGR_MISCI          0x18

#define FPGA_MGR_STATE_MSEL_MSK         0xF8
#define FPGA_MGR_STATE_MSEL_MSK_SHIFT   3

#define FPGA_MGR_STATE_MODE_MSK         0x7
#define FPGA_MGR_STATE_MODE_MSK_SHIFT   0

/* FPGA state mode */

#define FPGA_MGR_STATE_MODE_POWEROFF    0x0
#define FPGA_MGR_STATE_MODE_RESET       0x1
#define FPGA_MGR_STATE_MODE_CONFIG      0x2
#define FPGA_MGR_STATE_MODE_INIT        0x3
#define FPGA_MGR_STATE_MODE_USER        0x4
#define FPGA_MGR_STATE_MODE_UNKNOWN     0x5

/* FPGA CTRL */

#define FPGA_MGR_CTRL_EN                (0x1 << 0)
#define FPGA_MGR_CTRL_NCE               (0x1 << 1)
#define FPGA_MGR_CTRL_NCFG_PULL         (0x1 << 2)
#define FPGA_MGR_CTRL_NSTATUS           (0x1 << 3)
#define FPGA_MGR_CTRL_CONF_DONE         (0x1 << 4)
#define FPGA_MGR_CTRL_PARTIAL_CFG       (0x1 << 5)
#define FPGA_MGR_CTRL_AXI_CFG_EN        (0x1 << 8)
#define FPGA_MGR_CTRL_AXI_CFG_DWIDTH    (0x1 << 9)

#define FPGA_MGR_CTRL_CDRATIO_E_MSK             0xFFFFFF3F
#define FPGA_MGR_CTRL_CDRATIO_E_X1              (0x0 << 6)
#define FPGA_MGR_CTRL_CDRATIO_E_X2              (0x1 << 6)
#define FPGA_MGR_CTRL_CDRATIO_E_X4              (0x2 << 6)
#define FPGA_MGR_CTRL_CDRATIO_E_X8              (0x3 << 6)

#define FPGA_MGR_CTRL_CFG_WIDTH_MSK             (0x1 << 9)
#define FPGA_MGR_CTRL_CFG_WIDTH_16              (0x0 << 9)
#define FPGA_MGR_CTRL_CFG_WIDTH_32              (0x1 << 9)

/* FPGA DCLK STATE */

#define FPGA_MGR_DCLK_STATE_COUNTDONE           0x1

#define FPGA_MGR_CON_MON                0x800

#define FPGA_MGR_GPIO_INTEN             (0x30 + FPGA_MGR_CON_MON)
#define FPGA_MGR_GPIO_INTMASK           (0x34 + FPGA_MGR_CON_MON)
#define FPGA_MGR_GPIO_INTTYPE_LEVEL     (0x38 + FPGA_MGR_CON_MON)
#define FPGA_MGR_GPIO_INT_POLARITY      (0x3C + FPGA_MGR_CON_MON)
#define FPGA_MGR_GPIO_INT_STATUS        (0x40 + FPGA_MGR_CON_MON)
#define FPGA_MGR_GPIO_RAW_STATUS        (0x44 + FPGA_MGR_CON_MON)
#define FPGA_MGR_GPIO_PORTA_EOI         (0x4C + FPGA_MGR_CON_MON)
#define FPGA_MGR_GPIO_EXT_PORTA         (0x50 + FPGA_MGR_CON_MON)

#define FPGA_MGR_GPIO_INT_STATUS_NSTATUS        (0x1 << 0)
#define FPGA_MGR_GPIO_INT_STATUS_CFG_DONE       (0x1 << 1)

#define FPGA_MGR_GPIO_PORTA_EOI_NS       (0x1 << 0) 
#define FPGA_MGR_GPIO_PORTA_EOI_CD       (0x1 << 1)
#define FPGA_MGR_GPIO_PORTA_EOI_ID       (0x1 << 2)
#define FPGA_MGR_GPIO_PORTA_EOI_CRC      (0x1 << 3)
#define FPGA_MGR_GPIO_PORTA_EOI_CCD      (0x1 << 4)
#define FPGA_MGR_GPIO_PORTA_EOI_PRR      (0x1 << 5)
#define FPGA_MGR_GPIO_PORTA_EOI_PRE      (0x1 << 6)
#define FPGA_MGR_GPIO_PORTA_EOI_PRD      (0x1 << 7)
#define FPGA_MGR_GPIO_PORTA_EOI_NCP      (0x1 << 8)
#define FPGA_MGR_GPIO_PORTA_EOI_NSP      (0x1 << 9)
#define FPGA_MGR_GPIO_PORTA_EOI_CDP      (0x1 << 10)
#define FPGA_MGR_GPIO_PORTA_EOI_FPO      (0x1 << 11)

#define FPGA_MGR_GPIO_EXT_PORTA_MSK     0xFFF

#define FPGA_MGR_GPIO_EXT_PORTA_STATUS          (0x1 << 0)
#define FPGA_MGR_GPIO_EXT_PORTA_CONF_DONE       (0x1 << 1)
#define FPGA_MGR_GPIO_EXT_PORTA_INIT_DONE       (0x1 << 2)
#define FPGA_MGR_GPIO_EXT_PORTA_CRC_ERROR       (0x1 << 3)
#define FPGA_MGR_GPIO_EXT_PORTA_CVP_CONF_DONE   (0x1 << 4)
#define FPGA_MGR_GPIO_EXT_PORTA_PR_READT        (0x1 << 5)
#define FPGA_MGR_GPIO_EXT_PORTA_PR_ERROR        (0x1 << 6)
#define FPGA_MGR_GPIO_EXT_PORTA_PR_DONE         (0x1 << 7)
#define FPGA_MGR_GPIO_EXT_PORTA_CONFIG_PIN      (0x1 << 8)
#define FPGA_MGR_GPIO_EXT_PORTA_STATUS_PIN      (0x1 << 9)
#define FPGA_MGR_GPIO_EXT_PORTA_CONF_DONE_PIN   (0x1 << 10)
#define FPGA_MGR_GPIO_EXT_PORTA_FPGA_PWR_ON     (0x1 << 11)

#define FPGA_MGR_CFG_MODE_PP16_FAST_NOAES_NODC  0x0
#define FPGA_MGR_CFG_MODE_PP16_FAST_AES_NODC    0x1
#define FPGA_MGR_CFG_MODE_PP16_FAST_AESOPT_DC   0x2
#define FPGA_MGR_CFG_MODE_PP16_SLOW_NOAES_NODC  0x4
#define FPGA_MGR_CFG_MODE_PP16_SLOW_AES_NODC    0x5
#define FPGA_MGR_CFG_MODE_PP16_SLOW_AESOPT_DC   0x6
#define FPGA_MGR_CFG_MODE_PP32_FAST_NOAES_NODC  0x8
#define FPGA_MGR_CFG_MODE_PP32_FAST_AES_NODC    0x9
#define FPGA_MGR_CFG_MODE_PP32_FAST_AESOPT_DC   0xa
#define FPGA_MGR_CFG_MODE_PP32_SLOW_NOAES_NODC  0xc
#define FPGA_MGR_CFG_MODE_PP32_SLOW_AES_NODC    0xd
#define FPGA_MGR_CFG_MODE_PP32_SLOW_AESOPT_DC   0xe
#define FPGA_MGR_CFG_MODE_UNKNOWN               0x20

/* structure holding the instance specific details */

typedef struct fpga_mgr_drv_ctrl {
    VXB_DEVICE_ID   fpgaMgrDev;
    void *          fpgaMgrHandle;
    SEM_ID          fpgaMgrDevSem;
    BOOL            statue;
    void *          dataBase;
    char *          firmware;
    void *          firmwareOffset;
    UINT32          firmwareMaxSize;

    FUNCPTR         sysFpgaSetApplyBit;
    
    STATUS (*sysFlashGet)
    (
    char *  string,     /* where to copy flash memory      */
    int     byteLen,    /* maximum number of bytes to copy */
    int     offset      /* byte offset into flash memory   */
    );

    STATUS (*sysFlashSet)
    (
    char *  string,     /* string to be copied into flash memory */
    int     byteLen,    /* maximum number of bytes to copy       */
    int     offset      /* byte offset into flash memory         */
    );

    FUNCPTR         sysResetMgrBridgeReset;
    FUNCPTR         sysResetMgrBridgeEnable;
    int             sectorSize;
} FPGA_MGR_DRV_CTRL;

/* FPGA Manager controller read and write interface */

#define FPGA_MGR_BAR(p)       ((char *)((p)->pRegBase[0]))
#define FPGA_MGR_HANDLE(p)    (((FPGA_MGR_DRV_CTRL *)(p)->pDrvCtrl)->fpgaMgrHandle)
        
#ifdef __cplusplus
}
#endif

#endif /* __INCvxbAltSocGen5FpgaMgrh */
