/* vxbZynqDevCfg.h - header file for Device Configuration Interface of Xilinx Zynq */

/*
 * Copyright (c) 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,20jul13,y_c  update struct devcfg_drv_ctrl. (WIND00426873)
01a,15may13,y_c  written
*/

#ifndef __INCvxbZynqDevCfgh
#define __INCvxbZynqDevCfgh

#ifdef __cplusplus
extern "C" {
#endif

/* defines */

#define DEVC_PRINT(fmt,a,b,c,d,e,f) \
    if (_func_logMsg) \
       (* _func_logMsg) (fmt,a,b,c,d,e,f)

/* DEVC register */

#define DEVCFG_CTRL                   0x00 /* Control Register */
#define DEVCFG_LOCK                   0x04 /* Lock Register */
#define DEVCFG_CFG                    0x08 /* Configuration Register */
#define DEVCFG_INT_STS                0x0C /* Interrupt Status Register */
#define DEVCFG_INT_MASK               0x10 /* Interrupt Mask Register */
#define DEVCFG_STATUS                 0x14 /* Status Register */
#define DEVCFG_DMA_SRC_ADDR           0x18 /* DMA Src Add Register */
#define DEVCFG_DMA_DEST_ADDR          0x1C /* DMA Desn Add Register */
#define DEVCFG_DMA_SRC_LEN            0x20 /* DMA Src Transfer Len Register*/
#define DEVCFG_DMA_DEST_LEN           0x24 /* DMA Des Transfer Register*/
#define DEVCFG_SW_ID                  0x30 /* Software ID Register */
#define DEVCFG_UNLOCK                 0x34 /* Unlock Register */
#define DEVCFG_MCTRL                  0x80 /* Miscellaneous Control Register */

/* control register bit definitions */

#define DEVCFG_CTRL_PCFG_PROG_B_MASK    0x40000000 /* FPGA Clear */
#define DEVCFG_CTRL_PCAP_PR_MASK        0x08000000 /* Select PCAP for config */
#define DEVCFG_CTRL_PCAP_MODE_MASK      0x04000000 /* Enable PCAP */

#define DEVCFG_UNLOCK_DATA              0x757BDF0D /* DEVCFG UNLOCK data*/

/* status register bit definitions */

#define DEVCFG_STATUS_PCFG_INIT_MASK    0x00000010 /* FPGA init status */

/* MCtrl register bit definitions */

#define DEVCFG_INT_PCAP_LPBK_MASK       0x00000010 /* Internal PCAP loopback */

/* interrupt status/mask register bit definitions */

#define DEVCFG_IXR_DMA_DONE_MASK        0x00002000 /* DMA transfers Done */
#define DEVCFG_IXR_D_P_DONE_MASK        0x00001000 /* DMA and PCAP transfer Done */
#define DEVCFG_IXR_PCFG_DONE_MASK       0x00000004 /* FPGA programme Done */
#define DEVCFG_IXR_ALL_MASK             (DEVCFG_IXR_DMA_DONE_MASK  \
                                        | DEVCFG_IXR_D_P_DONE_MASK \
                                        | DEVCFG_IXR_PCFG_DONE_MASK)

/* constant values */

#define DEVCFG_DMA_INVALID_ADDRESS      0xFFFFFFFF  /* Invalid DMA address */

#define DEVCFG_DMA_LAST_TRANSFER        1

#define DEVCFG_POLL_TIMEOUT             10000000

#define DMA_BLOCK_SIZE        65536  /*equal to COPY_BLOCK_SIZE in usrFsLib.c*/

#define DEVCFG_DEVICE_NAME    "/devCfg"

/* register define for PS-XADC interface */

#define DEVCFG_XADC_CFG          0x100  /* XADC Configuration Register */
#define DEVCFG_XADC_INT_STS      0x104  /* XADC Interrupt Status Register */
#define DEVCFG_XADC_INT_MASK     0x108  /* XADC Interrupt Mask Register */
#define DEVCFG_XADC_MSTS         0x10C  /* XADC Misc status register */
#define DEVCFG_XADC_CMDFIFO      0x110  /* XADC Command FIFO Register */
#define DEVCFG_XADC_RDFIFO       0x114  /* XADC Date FIFO Register */
#define DEVCFG_XADC_MCTL         0x118  /* XADC Misc control register */

/* command FIFO register mask */

#define XADC_DATA_MASK           0x0000FFFF             /* Data Mask */
#define XADC_ADDR_MASK           0x03FF0000             /* Addr Mask */
#define XADC_ADDR_SHIFT          16                      /* Shift of the Addr */
#define XADC_CMD_MASK            0x3C000000             /* Cmd Mask*/
#define XADC_CMD_WRITE           (0x2 << XADC_CMD_SHIFT) /* CMD Write */
#define XADC_CMD_READ            (0x1 << XADC_CMD_SHIFT) /* CMD Read */
#define XADC_CMD_SHIFT           26                      /* Shift of the Cmd */

/* configuration register mask */

#define XADC_CFG_ENABLE_MASK     0x80000000  /* Access Enable from PS */
#define XADC_CFG_CFIFOTH_MASK    0x00F00000  /* Command FIFO Threshold */
#define XADC_CFG_DFIFOTH_MASK    0x000F0000  /* Data FIFO Threshold */

#define CmdPrepare(RegOffset, Data, ReadWrite) \
    ((ReadWrite & XADC_CMD_MASK) | \
     ((RegOffset << XADC_ADDR_SHIFT) & XADC_ADDR_MASK) | \
     (Data & XADC_DATA_MASK))

/* typedefs */

typedef struct devCfgIoDevice
    {
    DEV_HDR     devHdr;         /* I/O device header */
    int         devCfgDrvNum;
    } DEVCFG_IO_DEVICE;

/* private instance context */

typedef struct devcfg_drv_ctrl
    {
    DEVCFG_IO_DEVICE devCfgIoDev;
    VXB_DEVICE_ID    devcfgDev;
    SEM_ID           devcfgDevSem;
    SEM_ID           xadcIfDevSem;
    void *           devcfgBar;
    void *           devcfgHandle;
    FUNCPTR          devCfgLvlShftrSet;
    FUNCPTR          devCfgFpgaResetSet;
    } DEVCFG_DRV_CTRL;

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbZynqDevCfgh */
