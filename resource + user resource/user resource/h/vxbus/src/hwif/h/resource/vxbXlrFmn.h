/* vxbXlrFmn.h - xlr message ring function api */

/*
 * Copyright 2007, 2012 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,26jul12,sye  Removed duplicate method declaration. (WIND00365654)
01a,28aug07,l_z  created 
*/

/*
DESCRIPTION 
This module contains FMN driver, include message sending and receiving 
interface.
*/

#ifndef __INCvxbxlrfmnh
#define __INCvxbxlrfmnh

/* includes */

#include <vxWorks.h>
#include <hwif/vxbus/vxBus.h>
#include "lstLib.h" 

#ifdef __cplusplus
extern "C" {
#endif

/* device driver call back function when receive required type message */

typedef struct callBackNode
    {
    VOIDFUNCPTR callBackFunc;
    int arg;
    }CALL_BACK_NODE;

/* message struct */

typedef struct 
    {
    UINT64 msg0;
    UINT64 msg1;
    UINT64 msg2;
    UINT64 msg3;
    UINT8 ucId;
    UINT8 ucSize;
    UINT8 ucCode;
    UINT8 ucReserve;
    struct stMessageNode *pMsgNodeLink;
    }MESSAGE, *PMESSAGE;

/* Message Node  struct */

typedef struct stMessageNode
    {
    NODE links;
    MESSAGE msg;
    }MESSAGE_NODE;

/* fmn run mode */

typedef enum
    {
    INTERRUPT,
    POLLING
    }FMN_MODE;

/* typedefs */

typedef void   (*FMN_HANDLER_FUNC)(void * fmnArg);
typedef STATUS (*FMN_CONNECT_FUNC)(VXB_DEVICE_ID pInst, INT32 pinInput, 
                FMN_HANDLER_FUNC fmnHandler, void * fmnArg);
typedef STATUS (*FMN_DISCONN_FUNC)(VXB_DEVICE_ID pInst, INT32 pinInput, 
                FMN_HANDLER_FUNC fmnHandler, void * fmnArg);
typedef STATUS (*FMN_ENABLE_FUNC )(VXB_DEVICE_ID pInst, INT32 pinInput);
typedef STATUS (*FMN_DISABLE_FUNC)(VXB_DEVICE_ID pInst, INT32 pinInput);

/* globals */

typedef struct vxFmnCntrlInit
    {
    FMN_CONNECT_FUNC     fmnConnectFunc; /* Install an FMN int handler  */
    FMN_ENABLE_FUNC      fmnEnableFunc;  /* Enable int                  */
    FMN_DISABLE_FUNC     fmnDisableFunc; /* Disable int                 */
    FMN_DISCONN_FUNC     fmnDisconnFunc; /* Disconnect handler          */
    VXB_DEVICE_ID        pCtlr;          /* Interrupt Controller        */
    } VXFMN_CTRL_INIT, * VXFMN_CTRL_INIT_PTR;

/* default FMN queues entry number */
#define DEFAULT_MESSAGE_NUM 1024
#define FMN_STATION_NUM     128

#ifdef __cplusplus
}
#endif
      
#endif /* __INCvxbxlrfmnh */ 
