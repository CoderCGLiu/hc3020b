/* vxbIntelTopcliffIoh.h - header file for Intel Topcliff I/O hub driver */

/*
 * Copyright (c) 2010 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,29jul10,wap  written
*/

#ifndef __INCvxbIntelTopcliffIohh
#define __INCvxbIntelTopcliffIohh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void iohRegister (void);

#ifndef BSP_VERSION

#define INTEL_VENDORID                  0x8086
#define INTEL_DEVICEID_TOPCLIFF_IOH     0x8801

#define IOH_ROM_OFFSET			0x14

/* ethernet address offsets within serial ROM */

#define IOH_ROM_MAC_0			0x03
#define IOH_ROM_MAC_1			0x02
#define IOH_ROM_MAC_2			0x01
#define IOH_ROM_MAC_3			0x00
#define IOH_ROM_MAC_4			0x0B
#define IOH_ROM_MAC_5			0x0A

IMPORT STATUS iohRomRead (int, UINT8 *);

typedef struct ioh_drv_ctrl
    {
    VXB_DEVICE_ID       iohDev;
    void *              iohBar;
    void *              iohHandle;
    void *              iohRomBar;
    BOOL		iohIsMapped;
    SEM_ID              iohSem;
    } IOH_DRV_CTRL;

#define IOH_BAR(p)   ((IOH_DRV_CTRL *)(p)->pDrvCtrl)->iohBar
#define IOH_ROM_BAR(p)   ((IOH_DRV_CTRL *)(p)->pDrvCtrl)->iohRomBar
#define IOH_HANDLE(p)   ((IOH_DRV_CTRL *)(p)->pDrvCtrl)->iohHandle

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (IOH_HANDLE(pDev), (UINT32 *)((char *)IOH_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (IOH_HANDLE(pDev),                             \
        (UINT32 *)((char *)IOH_BAR(pDev) + addr), data)

#define ROM_READ_1(pDev, addr)                                  \
    vxbRead8 (IOH_HANDLE(pDev), (UINT8 *)((char *)IOH_ROM_BAR(pDev) + addr))

#define ROM_WRITE_1(pDev, addr, data)                           \
    vxbWrite8 (IOH_HANDLE(pDev),                             \
        (UINT8 *)((char *)IOH_ROM_BAR(pDev) + addr), data)

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbIntelTopcliffIohh */
