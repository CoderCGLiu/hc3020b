/* vxbXlpFmn.h - XLP message ring driver header file */

/*
 * Copyright (c) 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,12jul11,l_z  add watchdog for hardware issue
01a,25may11,x_f  created
*/

/*
DESCRIPTION 
This module contains XLP FMN driver header.
*/

#ifndef __INCvxbXlpFmnh
#define __INCvxbXlpFmnh

#ifdef __cplusplus
extern "C" {
#endif

#include <endLib.h>
#include <netLib.h>
#include <arch/mips/xlxLib.h>

/* defines */

#define XLP_FMN_INT_WDG

#define MSG_STATUS_RXQVCE(a)    (a >> 28)            /* Return Receive Queue VC */
#define MSG_STATUS1_VC_PEND(a)  (((a) >> 16) & 0xF)  /* Return Interrupt pending for vectors 0 to 3 */
#define MSG_STATUS1_VC_EMPTY(a) (((a) >> 24) & 0xF)

#define FMN_TIMEOUT                     1000

/* registers defines */

#define R_FMN_OQ_CONFIG_REG(i)          ((i) << 3)
#define R_FMN_OQ_CREDIT_CONFIG_REG      0x2000

#define M_FMN_OQ_CONFIG_OUTQ_EN         0x8000000000000000ULL
#define M_FMN_OQ_CONFIG_SPILL_EN        0x4000000000000000ULL
#define M_FMN_OQ_CONFIG_INT_EN          0x0800000000000000ULL
#define M_FMN_OQ_CONFIG_LT_HI           0x0080000000000000ULL
#define M_FMN_OQ_CONFIG_OS              0x000000000000001fULL

#define S_FMN_OQ_CONFIG_SB              27
#define S_FMN_OQ_CONFIG_SL              21
#define S_FMN_OQ_CONFIG_SS              15
#define S_FMN_OQ_CONFIG_OB              10
#define S_FMN_OQ_CONFIG_OL              5

#define FMN_SPILL_BASE                  (256 << 20)
#define FMN_SPILL_PAGE_SIZE             (4 << 10)
#define FMN_SPILL_PAGES                 4
#define FMN_RAM_BASE                    0
#define FMN_RAM_PAGE_ENTRIES            32
#define FMN_RAM_PAGES                   1
#define FMN_CREDIT_NUM                  50
#define FMN_VC_NUM                      1024

#define CREDIT_GEN(src, dst, val)      \
    (((src) & 0x3ff) | (((dst) & 0xfff) << 12) | (((val) & 0xffff) << 24))

#define XLP_STNID_CPU0                  0x00
#define XLP_STNID_CPU1                  0x10
#define XLP_STNID_CPU2                  0x20
#define XLP_STNID_CPU3                  0x30
#define XLP_STNID_CPU4                  0x40
#define XLP_STNID_CPU5                  0x50
#define XLP_STNID_CPU6                  0x60
#define XLP_STNID_CPU7                  0x70
#define XLP_STNID_PCIE0                 0x100
#define XLP_STNID_PCIE1                 0x102
#define XLP_STNID_PCIE2                 0x104
#define XLP_STNID_PCIE3                 0x106
#define XLP_STNID_GDX                   0x108
#define XLP_STNID_RSA_ECC               0x110
#define XLP_STNID_CRYPTO                0x119
#define XLP_STNID_CMP                   0x129
#define XLP_STNID_POE                   0x180
#define XLP_STNID_NAE_TX                0x1DC
#define XLP_STNID_NAE_RX                0x3E8

/* message defines */

#define MSG_STATUS_TQF                  0x00000004
#define MSG_STATUS_IFME                 0x00000002
#define MSG_STATUS_DFCF                 0x00000001
#define MSG_SW_CODE_OFF                 24
#define MSG_SIZE_OFF                    16
#define MSG_DEST_ID_OFF                 0
#define MSG_DEST_ID_MASK                0x0FFF

#define XLP_FMN_MAX_RX                  32

#define XLP_MAX_CHANNELS_PER_CPU        4  /* four channels per thread */

/* message struct */

typedef struct 
    {
    UINT64 msg0;
    UINT64 msg1;
    UINT64 msg2;
    UINT64 msg3;
    UINT32 ucId;
    UINT32 ucSize;
    UINT32 ucCode;
    UINT32 ucDestId;
    } MESSAGE, *PMESSAGE;

typedef void   (*INT_HANDLER_FUNC)(void * fmnArg);
typedef STATUS (*FMN_CONNECT_FUNC)(VXB_DEVICE_ID pInst, INT32 pinInput, 
                INT_HANDLER_FUNC fmnHandler, void * fmnArg);
typedef STATUS (*FMN_DISCONNECT_FUNC)(VXB_DEVICE_ID pInst, INT32 pinInput, 
                INT_HANDLER_FUNC fmnHandler, void * fmnArg);
typedef STATUS (*FMN_ENABLE_FUNC )(VXB_DEVICE_ID pInst, INT32 pinInput);
typedef STATUS (*FMN_DISABLE_FUNC)(VXB_DEVICE_ID pInst, INT32 pinInput);

typedef struct vxFmnCntrlInit
    {
    FMN_CONNECT_FUNC     fmnConnectFunc; /* Install an FMN int handler  */
    FMN_ENABLE_FUNC      fmnEnableFunc;  /* Enable int                  */
    FMN_DISABLE_FUNC     fmnDisableFunc; /* Disable int                 */
    FMN_DISCONNECT_FUNC  fmnDisconnFunc; /* Disconnect handler          */
    VXB_DEVICE_ID        pCtlr;          /* Interrupt Controller        */
    } VXFMN_CTRL_INIT, * VXFMN_CTRL_INIT_PTR;

typedef enum fmn_mode
    {
    INTERRUPT_MODE,
    POLLING_MODE
    } FMN_MODE;

typedef VOID   (*FMN_CONN_FUNC)(VXB_DEVICE_ID, VOIDFUNCPTR, int, int);
typedef VOID   (*FMN_DISCONN_FUNC)(VXB_DEVICE_ID, int);
typedef VOID   (*FMN_MODE_SET_FUNC)(VXB_DEVICE_ID, FMN_MODE);

typedef struct vxbXlpFmnFunc
    {
    FMN_CONN_FUNC        xlpFmnIntConnect;
    FMN_DISCONN_FUNC     xlpFmnIntDisconnect;
    FMN_MODE_SET_FUNC    xlpFmnModeSet;
    } VXB_XLP_FMN_FUNC;

typedef VXB_XLP_FMN_FUNC * (* xlpFmnControlGetPrototype) (void);

/* device driver call back function when receive required type message */

typedef struct callBackNode
    {
    VOIDFUNCPTR callBackFunc;
    int         arg;
    } CALL_BACK_NODE;

#define FMN_BAR(p)      ((XLP_FMN_DRVCTRL *)(p)->pDrvCtrl)->fmnBar
#define FMN_HANDLE(p)   ((XLP_FMN_DRVCTRL *)(p)->pDrvCtrl)->fmnHandle

#define CSR_READ_8(pDev, addr)                             \
        vxbRead64 (FMN_HANDLE(pDev), (UINT64 *)((char *)FMN_BAR(pDev) + addr))

#define CSR_WRITE_8(pDev, addr, data)                      \
        vxbWrite64 (FMN_HANDLE(pDev),                      \
            (UINT64 *)((char *)FMN_BAR(pDev) + addr), data)

#define CSR_SETBIT_8(pDev, addr, data)                     \
        CSR_WRITE_8(pDev, addr,                            \
                    CSR_READ_8(pDev, addr) | (data))

#define CSR_CLRBIT_8(pDev, addr, data)                     \
        CSR_WRITE_8(pDev, addr,                            \
                    CSR_READ_8(pDev, addr) & ~(data))
/* declarations */

STATUS fmnMsgSend (PMESSAGE pMsg);
STATUS fmnSingleMsgSend(PMESSAGE pMsg);
STATUS fmnSafeSingleMsgSend (PMESSAGE pMsg);
STATUS fmnMsgRecv (UINT32 vc, PMESSAGE pMsg);
STATUS fmnSingleMsgRecv (UINT32 vc, PMESSAGE pMsg);
STATUS fmnSafeSingleMsgRecv (UINT32 vc, PMESSAGE pMsg);

#ifndef XLR_CP2_READ32
#define XLR_CP2_READ32(reg, sel) xlrCp2Rd32Reg##reg##Sel##sel()
#endif

#ifndef XLR_CP2_WRITE32
#define XLR_CP2_WRITE32(reg, sel, value) xlrCp2Wr32Reg##reg##Sel##sel(value)
#endif

#ifndef XLR_CP2_READ64
#define XLR_CP2_READ64(reg, sel) xlrCp2Rd64Reg##reg##Sel##sel()
#endif

#ifndef XLR_CP2_WRITE64
#define XLR_CP2_WRITE64(reg, sel, value) xlrCp2Wr64Reg##reg##Sel##sel(value)
#endif
#ifdef __cplusplus
}
#endif

#endif /* __INCvxbXlpFmnh */
