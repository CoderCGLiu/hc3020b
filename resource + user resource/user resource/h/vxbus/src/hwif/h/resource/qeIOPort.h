/* qeIOPort.h - QUICC Engine I/O Port header file */

/*
 * Copyright (c) 1999, 2007, 2009 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01c,08jul09,b_m  consolidate QE IO port pin mux for both 83xx and 85xx.
01b,01aug07,h_k  added typedefs.
01a,12sep99,ms_  created from m8260Sio.h, 01b
*/

/*
 * This file contains constants for the I/O Ports in the Freescale
 * PowerQUICC II/III integrated Communications Processor with
 * QUICC Engine.
 */

#ifndef __INCqeIOPorth
#define __INCqeIOPorth

#ifdef __cplusplus
extern "C" {
#endif

/* IO Port Register Offset */

#define QE_CPODR        0x00
#define QE_CPDAT        0x04
#define QE_CPDIR1       0x08
#define QE_CPDIR2       0x0C
#define QE_CPPAR1       0x10
#define QE_CPPAR2       0x14

#define QE_PPORT_PIN2BIT(pin)   (0x80000000 >> pin)

#define QE_PORTA        0
#define QE_PORTB        1
#define QE_PORTC        2
#define QE_PORTD        3
#define QE_PORTE        4
#define QE_PORTF        5
#define QE_PORTG        6

#define QE_PORT_NUM     7

typedef struct qeIoPortTable
    {
    UINT32  port;
    UINT32  pin;
    UINT32  direction;
    BOOL    openDrain;
    UINT32  assignment;
    } QE_IOPORT_TABLE;

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCqeIOPorth */
