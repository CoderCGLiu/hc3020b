/* qeCp.h - QUICC Engine header file */

/*
 * Copyright (c) 1999, 2009, 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01w,25jul11,y_c  Added SDMR GLB bit setting for snooping.
01d,09sep09,x_z  update for UCC UART mode
01c,09sep09,x_z  update for MPC8569
01b,21aug09,x_z  add support for firmware uploading.
01a,12sep99,ms_  created from qeCpm.h, 01d.
*/

/*
 * This file contains constants for Freescale QUICC Engine.
 */

#ifndef __INCqeCph
#define __INCqeCph

#ifdef __cplusplus
extern "C" {
#endif

#ifndef M8260ABBREVIATIONS
#define M8260ABBREVIATIONS

#ifdef  _ASMLANGUAGE
#   define CAST(x)
#else   /* _ASMLANGUAGE */
typedef volatile UCHAR              VCHAR;   /* shorthand for volatile UCHAR */
typedef volatile INT32              VINT32;  /* volatile unsigned word       */
typedef volatile INT16              VINT16;  /* volatile unsigned halfword   */
typedef volatile INT8               VINT8;   /* volatile unsigned byte       */
typedef volatile UINT32             VUINT32; /* volatile unsigned word       */
typedef volatile UINT16             VUINT16; /* volatile unsigned halfword   */
typedef volatile UINT8              VUINT8;  /* volatile unsigned byte       */
#   define CAST(x)                  (x)
#endif  /* _ASMLANGUAGE */

#endif  /* M8260ABBREVIATIONS */

/* QE SDMA registers */

#define QE_SDMR_OFFSET              0x4004

#define QE_SDMR_GLB_1_MSK           0x80000000

/* I-RAM Register definitions */

#define QE_IADD                     0x0000
#define QE_IDATA                    0x0004
#define QE_IREADY                   0x000c      /* MPC8569E only         */

#define QE_IADD_AIE                 0x80000000  /* auto increment enable */
#define QE_IADD_BADDR               0x00080000  /* base address          */

#define QE_IREADY_READY             0x80000000  /* I-RAM ready           */

/* Command Register definitions */

#define QE_CPCR                     0x0100
#define QE_CECDR                    0x0108
#define QE_CERCR                    0x0138

#define QE_CPCR_LATENCY             (65536 * 400) /* worst case exec latency  */

#define QE_CPCR_RESET               0x80000000    /* software reset command   */
#define QE_CPCR_SBC_MSK             0x03f00000    /* sub-block code           */
#define QE_CPCR_RES1                0x000e0000    /* reserved                 */
#define QE_CPCR_FLG                 0x00010000    /* flag - command executing */
#define QE_CPCR_RES2                0x0000c000    /* reserved                 */
#define QE_CPCR_MCN_MSK             0x00003fc0    /* MCC channel number       */
#define QE_CPCR_RES3                0x00000030    /* reserved                 */
#define QE_CPCR_OP_MSK              0x0000000f    /* command opcode           */
#define QE_CPCR_SBC_SHIFT           0x15          /* get to the SBC field     */
#define QE_CPCR_MCN_SHIFT           0x6           /* get to the MCC field     */
#define QE_CPCR_OP_SHIFT            0x0           /* get to the opcode field  */
#define QE_CPCR_MCN_HDLC            0x0           /* protocol code: HDLC      */
#define QE_CPCR_MCN_UART            0x4           /* protocol code: UART      */
#define QE_CPCR_MCN_ATM             0xa           /* protocol code: ATM       */
#define QE_CPCR_MCN_ETH             0xc           /* protocol code: ETH       */
#define QE_CPCR_MCN_TRANS           0xf           /* protocol code: TRANS     */

#define QE_CPCR_OP(x)               \
    (((x) << QE_CPCR_OP_SHIFT) & QE_CPCR_OP_MSK)

#define QE_CPCR_SBC(x)              \
    (((x) << QE_CPCR_SBC_SHIFT) & QE_CPCR_SBC_MSK)

#define QE_CPCR_MCN(x)              \
    (((x) << QE_CPCR_MCN_SHIFT) & QE_CPCR_MCN_MSK)

/* CPCR - Part Sub-block code */

/* UCC channel */

#define QE_CPCR_UCC1                        0x00
#define QE_CPCR_UCC2                        0x01
#define QE_CPCR_UCC3                        0x02
#define QE_CPCR_UCC4                        0x03
#define QE_CPCR_UCC5                        0x04
#define QE_CPCR_UCC6                        0x05
#define QE_CPCR_UCC7                        0x06
#define QE_CPCR_UCC8                        0x07

/* Channel Protocols Mode */

#define QE_CPCR_SLOW                        0x00
#define QE_CPCR_FAST                        0x10

/* CPCR - opcodes (Fast Protocols Mode) */

#define QE_CPCR_RT_INIT                     0x00    /* Init rx and tx        */
#define QE_CPCR_RX_INIT                     0x01    /* init rx only          */
#define QE_CPCR_TX_INIT                     0x02    /* init tx only          */
#define QE_CPCR_HUNT                        0x03    /* rx frame hunt mode    */
#define QE_CPCR_TX_STOP                     0x04    /* stop tx               */
#define QE_CPCR_TX_GRSTOP                   0x05    /* gracefully stop tx    */
#define QE_CPCR_TX_RESTART                  0x06    /* restart tx            */
#define QE_CPCR_L2_SWITCH                   0x07    /* l2switch command      */
#define QE_CPCR_SET_GROUP                   0x08    /* set group address     */
#define QE_CPCR_INSERT_CELL                 0x09    /* insert cell           */
#define QE_CPCR_ATM_TRANSMIT                0x0a    /* atm transmit command  */
#define QE_CPCR_CELL_POOL_GET               0x0b    /* cell pool get         */
#define QE_CPCR_CELL_POOL_PUT               0x0c    /* cell pool put         */
#define QE_CPCR_IMA_HOST                    0x0d    /* ima host command      */
#define QE_CPCR_PUSHSCHED                   0x0f    /* pushsched command     */
#define QE_CPCR_ATM_MULTITHREAD_INIT        0x11    /* atm multithread init  */
#define QE_CPCR_ASSIGN_PAGE                 0x12    /* assign page           */
#define QE_CPCR_SET_LAST_RECEICE_REQ_THR    0x13    /* set last receive      */
                                                    /* request threshold     */
#define QE_CPCR_START_FLOW_CTRL             0x14    /* start flow control    */
#define QE_CPCR_STOP_FLOW_CTRL              0x15    /* stop flow control     */
#define QE_CPCR_ASSIGN_PAGE_TO_DEVICE       0x16    /* assign page to device */
#define QE_CPCR_RX_GRSTOP                   0x1a    /* graceful stop receive */
#define QE_CPCR_RX_RESTART                  0x1b    /* restart receive       */

/* CERCR */

#define QE_CERCR_MEE                        0x8000  /* MURAM ECC enable */
#define QE_CERCR_IEE                        0x4000  /* I-RAM ECC enable */
#define QE_CERCR_CIR                        0x0800  /* Common instruction RAM */

/*
 * BRG Configuration Registers
 *
 * 16 BRGs are supported except 8323. BRG12, BRG13,and BRG14 is unsupported on
 * 8323.
 */

#define QE_BRGCn(n)                         (0x640 + (n - 1) * 4)

#define QE_BRGC_RST                         0x20000 /* Reset BRG             */
#define QE_BRGC_EN                          0x10000 /* Enable BRG count      */
#define QE_BRGC_EXTC_MASK                   0x0c000 /* External clock source */
#define QE_BRGC_EXTC_0                      0x00000
#define QE_BRGC_EXTC_1                      0x04000
#define QE_BRGC_EXTC_2                      0x08000
#define QE_BRGC_EXTC_3                      0x0c000
#define QE_BRGC_ATB                         0x02000 /* Autobaud              */
#define QE_BRGC_DIV16                       0x00001 /* Divide-by-16          */

#define QE_BRGC_CD_MASK                     0xFFF   /* Clock Divider Mask    */
#define QE_BRGC_CD_SHIFT                    1       /* Clock Divider Shift   */

#ifndef _ASMLANGUAGE

#define QE_UCC_RX_THREADS                   9
#define QE_UCC_TX_THREADS                   8
#define QE_RISC_ALLOC_RISC1                 1   /* RISC1                      */
#define QE_RISC_ALLOC_RISC2                 2   /* RISC2 (invalid on MPC8323) */
#define QE_RISC_ALLOC_RISC3                 4   /* RISC3 (MPC8569 only)       */
#define QE_RISC_ALLOC_RISC4                 8   /* RISC4 (MPC8569 only)       */

typedef struct
    {
    UINT8   resvd[7];
    UINT8   largestLookupKeySize;
    struct
    {
    UINT32  rgf:4;
    UINT32  tgf:4;
    UINT32  rxGlobalParam:18;
    UINT32  resvd:2;
    UINT32  riscAlloc:4;
    } rxRamPage;

    UINT32  rxThreadInfo[QE_UCC_RX_THREADS];
    UINT8   resvd2[8];
    struct
    {
    UINT32  resvd1:8;
    UINT32  txGlobalParam:18;
    UINT32  resvd2:2;
    UINT32  riscAlloc:4;
    } txRamPage;
    UINT32  txThreadInfo[QE_UCC_TX_THREADS];
    UINT8   prefTxBDCount;
    } CECDR_RXTX_INIT;

typedef union
    {
    struct
    {
    UINT32  reset:1;
    UINT32  resvd1:7;
    UINT32  snum:7;
    UINT32  bitEnable:1;
    UINT32  resvd2:10;
    UINT32  opcode:6;
    } pageAssign;
    struct
    {
    UINT32  reset:1;
    UINT32  resvd1:5;
    UINT32  subBlockCode:5;
    UINT32  resvd:4;
    UINT32  flag:1;
    UINT32  protocol:10;
    UINT32  opcode:6;
    } qeCommand;
    UINT32 cpcr;
    } CPCR_COMMAND;

typedef struct
    {
    UINT32  cpcrVal;
    UINT32  cecdrVal;
    }QE_CPCR_DESC;

/* QE firmware file structure */

/* magic ID */

#define QE_FW_MAGIC_NUM_0                   'Q'
#define QE_FW_MAGIC_NUM_1                   'E'
#define QE_FW_MAGIC_NUM_2                   'F'

/* CRC32 polynomial */

#define QE_CRC32_CALC_VAL                   0xedb88320

/* QE RSP Register definitions */

#define QE_RSP_BASE                         0x4100
#define QE_RSP_SIZE                         0x0100
#define QE_RSP_TIBCR_OFF                    0x0
#define QE_RSP_TIBCR_NUM                    0x10
#define QE_RSP_ECCR_OFF                     0xf0

#define QE_RSP_TIBCR(x, y)                  (QE_RSP_BASE + x * QE_RSP_SIZE + \
                                             QE_RSP_TIBCR_OFF + y * 4)
#define QE_RSP_ECCR(x)                      (QE_RSP_BASE + x * QE_RSP_SIZE + \
                                             QE_RSP_ECCR_OFF)

/* QE firmware ucode descriptor */

typedef struct
    {
    VUINT8  ucodeId[32];    /* ucode ID                   */
    VUINT32 trapAdrs[16];   /* trap addresses: 0: ignore  */
    VUINT32 eccrVal;        /* value for ECCR             */
    VUINT32 iramOffset;     /* offset into I-RAM          */
    VUINT32 ucodeLen;       /* ucode length in 4 bytes    */
    VUINT32 ucodeOffset;    /* offset into firmware       */
    VUINT8  ucodeMajor;     /* ucode version major        */
    VUINT8  ucodeMinor;     /* ucode version minor        */
    VUINT8  ucodeRev;       /* ucode revision             */
    VUINT8  res[5];
    } QE_UCODE_DESC;

/* QE firmware file descriptor */

typedef struct {
    VUINT32 fwLen;          /* firmware file size in bytes       */
    VUINT8  magicNum[3];    /* magic number 'Q' 'E' 'F'          */
    VUINT8  fwVer;          /* firmware file structure version   */
    VINT8   fwId[62];       /* firmware ID                       */
    VUINT8  iramUsage;      /* 0: shared I-RAM, 1: split I-RAM   */
    VUINT8  ucodeNum;       /* number of ucode descriptors       */
    VUINT16 socModel;       /* SOC model                         */
    VUINT8  socMajor;       /* SOC revision major                */
    VUINT8  socMinor;       /* SOC revision minor                */
    VUINT8  res0[4];
    VUINT8  exModes[8];     /* extended modes                    */
    VUINT32 vTraps[8];      /* virtual trap addresses            */
    VUINT8  res1[4];
    } QE_FIRMWARE_DESC;

#endif /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCqeCph */
