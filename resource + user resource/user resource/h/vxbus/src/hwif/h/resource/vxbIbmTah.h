/* vxbIbmTah.h - header file for IBM/AMCC TCP acceleration driver */

/*
 * Copyright (c) 2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,13aug08,mdo  WIND00130408
01a,30jan08,wap  written
*/

#ifndef __INCvxbIbmTahh
#define __INCvxbIbmTahh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void tahRegister (void);

#ifndef BSP_VERSION

#define TAH_REVID	0x0050
#define TAH_MR		0x0060
#define TAH_SSR0	0x0064
#define TAH_SSR1	0x0068
#define TAH_SSR2	0x006C
#define TAH_SSR3	0x0070
#define TAH_SSR4	0x0074
#define TAH_SSR5	0x0078
#define TAH_TSR		0x0080

#define TAH_MR_CVR	0x80000000	/* Checksum verification on RX */
#define TAH_MR_SR	0x40000000	/* Soft reset */
#define TAH_MR_ST	0x3F000000	/* Send threshold */
#define TAH_MR_TFS	0x00E00000	/* TX fifo size */
#define TAH_MR_DTFP	0x00100000	/* Disable TX FIFO parity protection */
#define TAH_MR_DIG	0x00080000	/* Disable interrupt generation */

#define TAH_ST(x)	(((x) << 24) & TAH_MR_ST)

#define TAH_TFS_2K	0x00200000
#define TAH_TFS_4K	0x00400000
#define TAH_TFS_6K	0x00600000
#define TAH_TFS_8K	0x00800000
#define TAH_TFS_10K	0x00A00000

#define TAH_TIMEOUT	10000

/* Private instance context */

typedef struct tah_drv_ctrl
    {
    VXB_DEVICE_ID       tahDev;
    void *		tahBar;
    void *		tahHandle;
    } TAH_DRV_CTRL;

#define TAH_BAR(p)   ((TAH_DRV_CTRL *)(p)->pDrvCtrl)->tahBar
#define TAH_HANDLE(p)   ((TAH_DRV_CTRL *)(p)->pDrvCtrl)->tahHandle

#define CSR_READ(pDev, addr)                                  \
    vxbRead32 (TAH_HANDLE(pDev), (UINT32 *)((char *)TAH_BAR(pDev) + addr))

#define CSR_WRITE(pDev, addr, data)                           \
    vxbWrite32 (TAH_HANDLE(pDev),                             \
        (UINT32 *)((char *)TAH_BAR(pDev) + addr), data)

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbIbmTahh */
