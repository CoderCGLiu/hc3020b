/* dma.h -- VxBus DMA support */

/*********************************************************************
*
* Copyright (C) 2006  Wind River Systems, Inc. All rights are reserved.
*
*/

/*
modification history
--------------------
01b,13jun07,tor  remove VIRT_ADDR
01a,06Apr06,tor  created
*/

/* includes */

#include <vxWorks.h>
#include <types/vxTypes.h>

/* defines */

/* operation modes */
#define DMA_COPY_MODE_MASK      0x00000001
#define DMA_COPY_MODE_DEVBUF    0x00000000
#define DMA_COPY_MODE_PIO       0x00000001
#define DMA_COPY_MODE_NO_SOFT   0x00000010
#define DMA_COPY_MODE_NO_HW     0x00000020

/* status flags */
#define DMA_STATUS_ALLOC_MASK   0x00000003
#define DMA_STATUS_FREE         0x00000001
#define DMA_STATUS_ALLOCATED    0x00000002
#define DMA_STATUS_AVAIL        0x00000010
#define DMA_STATUS_WORKING      0x00000020
#define DMA_STATUS_ERROR        0x80000000

/* typedefs */

typedef struct vxbDmaResource
    {
    UINT32 *    pStatus;                /* status of channel */
    UINT32 *    pErrVal;                /* error indicator */
    int         queueSize;              /* size of DMA ring/buffer */

    /* Read buffer */
    STATUS      (*dmaRead)
                    (
                    struct vxbDmaResource * pChan,
                    char * src,
                    char * dest,
                    int   size,
                    int   unitSize,
                    void (*dmaComplete)(struct vxbDmaResource * pChan, char * pArg),
                    char *   pArg
                    );

    /* Write buffer */
    STATUS      (*dmaWrite)
                    (
                    struct vxbDmaResource * pChan,
                    char * src,
                    char * dest,
                    int   size,
                    int   unitSize,
                    void (*dmaComplete)(struct vxbDmaResource * pChan, char * pArg),
                    char *   pArg
                    );

    /* Read buffer, don't return until complete */ 
    STATUS      (*dmaReadAndWait)
                    (
                    struct vxbDmaResource * pChan,
                    char *   src,
                    char *   dest,
                    int         size,
                    int         unitSize
                    );

    /* Write buffer, don't return until complete */
    STATUS      (*dmaWriteAndWait) 
                    (
                    struct vxbDmaResource * pChan,
                    char *   src,    
                    char *   dest,
                    int         size,
                    int         unitSize
                    );
    
    /* Free the DMA channel */ 
    void        (*chanFree)(struct vxbDmaResource * pChan); 
    
    VXB_DEVICE_ID       chanProvider;   /* driver providing service */
    VXB_DEVICE_ID       chanOwner;      /* owner of channel */
    } * VXB_DMA_RESOURCE_ID;

typedef struct vxbDmaRequest
    {
    VXB_DEVICE_ID       requestor;      /* instance requesting a channel */
    int                 minQueueDepth;  /* minimum size of ring/buffer */
    UINT32              flags;          /* flags specifying operating mode */
    VXB_DMA_RESOURCE_ID pChan;          /* channel identifier */
    } VXB_DMA_REQUEST;
    
/* globals */
    
METHOD_DECL(dmaChanAlloc)
    
/* locals */

/* forward declarations */

IMPORT VXB_DMA_RESOURCE_ID dmaChanAlloc
    (
    VXB_DEVICE_ID       pInst,
    UINT32              minQueueDepth,
    UINT32              flags
    );

