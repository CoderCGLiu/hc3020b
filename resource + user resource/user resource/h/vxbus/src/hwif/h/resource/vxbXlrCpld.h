/* vxbXlrCpld.h - header file for Raza Micro XLR CPLD driver */

/*
 * Copyright (c) 2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,02may08,wap  written
*/

#ifndef __INCvxbXlrCpldh
#define __INCvxbXlrCpldh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void cpldRegister (void);

#ifndef BSP_VERSION

#define CPLD_RESET1	0x0004
#define CPLD_MISCCTL	0x0010
#define CPLD_RESET2	0x0012

/* Reset register 1 */

#define CPLD_RESET1_BOARD_RESET		0x0040	/* Board reset */
#define CPLD_RESET1_GMAC_PHY_HRESET	0x0020	/* GMAC PHY hard reset */
#define CPLD_RESET1_GMAC_PHY_SRESET	0x0010	/* GMAC PHY soft reset */
#define CPLD_RESET1_RTC_RESET		0x0008	/* RTC reset */
#define CPLD_RESET1_HT_RESET		0x0004	/* HT reset */
#define CPLD_RESET1_XGS1_PHY_RESET	0x0002	/* XGMAC/SPI4.2 PHY reset 1 */
#define CPLD_RESET1_XGS0_PHY_RESET	0x0001	/* XGMAC/SPI4.2 PHY reset 0 */

/* Miscellaneous control register */

#define CPLD_MISCCTL_CFLASH_IOMODE	0x0080	/* compact flast io mode */
#define CPLD_MISCCTL_FLASH_PROG		0x0040	/* flash program enable */
#define CPLD_MISCCTL_HT_STOP		0x0020	/* HT stop (active low) */
#define CPLD_MISCCTL_XPAK1_TX_OFF	0x0010	/* turn off XPAC 1 transmit */
#define CPLD_MISCCTL_XPAK0_TX_OFF	0x0008	/* turn off XPAC 0 transmit */

/* Reset register 2 */

#define CPLD_RESET2_XPAC1_RESET		0x0004	/* XPAC 1 reset */
#define CPLD_RESET2_XPAC0_RESET		0x0002	/* XPAC 0 reset */
#define CPLD_RESET1_TCAM_RESET		0x0001	/* Reset tcam */

/* Private instance context */

typedef struct cpld_drv_ctrl
    {
    VXB_DEVICE_ID       cpldDev;
    void *		cpldBar;
    void *		cpldHandle;
    } CPLD_DRV_CTRL;

#define CPLD_BAR(p)   ((CPLD_DRV_CTRL *)(p)->pDrvCtrl)->cpldBar
#define CPLD_HANDLE(p)   ((CPLD_DRV_CTRL *)(p)->pDrvCtrl)->cpldHandle

#define CSR_READ(pDev, addr)                                  \
    vxbRead8 (CPLD_HANDLE(pDev), (UINT8 *)((char *)CPLD_BAR(pDev) + addr))

#define CSR_WRITE(pDev, addr, data)                           \
    vxbWrite8 (CPLD_HANDLE(pDev),                             \
        (UINT8 *)((char *)CPLD_BAR(pDev) + addr), data)

#define CSR_SETBIT(pDev, offset, val)          \
        CSR_WRITE(pDev, offset, CSR_READ(pDev, offset) | (val))

#define CSR_CLRBIT(pDev, offset, val)          \
        CSR_WRITE(pDev, offset, CSR_READ(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbXlrCpldh */
