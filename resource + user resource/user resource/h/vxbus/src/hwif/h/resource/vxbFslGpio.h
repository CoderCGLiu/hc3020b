/* vxbFslGpio.h - Freescale GPIO hardware defintions */

/*
 * Copyright (c) 2009 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,21sep09,wap  written
*/

#ifndef __INCvxbFslGpioh
#define __INCvxbFslGpioh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void fslGpioRegister (void);

#ifndef BSP_VERSION

#define FGPIO_GPDIR		0x000	/* Direction register */
#define FGPIO_GPODR		0x004	/* Open drain register */
#define FGPIO_GPDAT		0x008	/* Data register */
#define FGPIO_GPIER		0x00C	/* Interrupt event register */
#define FGPIO_GPIMR		0x010	/* Interrupt mask register */
#define FGPIO_GPICR		0x014	/* Ext. int. control register */

#define FSL_GPIO_NAME "fslGpio"

/*
 * GPIO pins 0-7 are mapped to bits 31-24 in the GPIO registers.
 * This macro allows us to map from a pin number to the right
 * register bit.
 */

#define FGPIO_PIN2BIT(x)	(1 << (31 - (x)))

/* There are 8 GPIO pins. */

#define FGPIO_CNT		32

typedef void (*gpioIntFunc)(void *);

#define FGPIO_BUSY	(GPIO_INTREC *)(0x12345678)

typedef struct gpioIntRec
    {
    gpioIntFunc		gInt_func;
    void *              gInt_ctx;
    } GPIO_INTREC;

typedef struct gpio_drv_ctrl
    {
    VXB_DEVICE_ID       gpioDev;
    void *              gpioBar;
    void *              gpioHandle;
    GPIO_INTREC *	gpioIntRecs[FGPIO_CNT];
    spinlockIsr_t	gpioLock;
    } GPIO_DRV_CTRL;

#define GPIO_BAR(p)	((GPIO_DRV_CTRL *)(p)->pDrvCtrl)->gpioBar
#define GPIO_HANDLE(p)	((GPIO_DRV_CTRL *)(p)->pDrvCtrl)->gpioHandle

#undef CSR_READ_4
#undef CSR_WRITE_4

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (GPIO_HANDLE(pDev), (UINT32 *)((char *)GPIO_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)				\
    vxbWrite32 (GPIO_HANDLE(pDev),				\
        (UINT32 *)((char *)GPIO_BAR(pDev) + addr), data)

#define CSR_SETBIT_4(pDev, offset, val)				\
	CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)				\
	CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

IMPORT STATUS gpioOutputSelect (VXB_DEVICE_ID, UINT32, UINT8, UINT8);
IMPORT STATUS gpioInputSelect (VXB_DEVICE_ID, UINT32);
IMPORT STATUS gpioInputGet (VXB_DEVICE_ID, UINT32, UINT8 *);
IMPORT STATUS gpioIntConnect (VXB_DEVICE_ID, UINT32, gpioIntFunc, void *);
IMPORT STATUS gpioIntDisconnect (VXB_DEVICE_ID, UINT32, gpioIntFunc, void *);

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbFslGpioh */
