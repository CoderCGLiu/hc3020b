/* qeDpramLib.h - QUICC Engine MURAM allocation header file */

/*
 * Copyright (c) 2006-2007, 2009 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01d,09sep09,x_z  rewrite MURAM pool routines
01c,01aug07,h_k  removed qeDpramFreeX qeDpramMallocX.
01b,22mar07,jmg  Changed POOL to QE_POOL
01a,16apr06,dtr  created from m82xxDpramLib.h
*/

/*
DESCRIPTION
This module contains QE MURAM allocation driver.
*/

#ifndef __INCqeDpramLibh
#define __INCqeDpramLibh

#ifdef __cplusplus
extern "C" {
#endif

#include <vxWorks.h>
#include <dllLib.h>

typedef struct
    {
    DL_NODE       node;         /* double link list         */
    UINT32        start;
    UINT32        size;
    UINT32        align;
    VXB_DEVICE_ID pOwnerInst;   /* record of the node owner */
    } QE_ADDR_RESOURCE;

typedef struct
    {
    UINT32  size;
    UINT32  align;
    void *  addr;
    } QE_MURAM_DESC;

typedef struct
    {
    UINT32        threadNum;
    BOOL          free;
    VXB_DEVICE_ID pOwnerInst; /* record of the thread owner */
    } QE_THREAD_DESC;

#ifdef __cplusplus
}
#endif

#endif /* __INCqeDpramLibh */
