/* vxbIfcm.h -  header for IFC FCM driver */

/*
 * Copyright (c) 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,29oct13,ywu  written
*/

#ifndef __INCvxbIfcmh
#define __INCvxbIfcmh

#ifdef  __cplusplus
 extern "C" {
#endif

#define IFCM_DRV_NAME   "ifcm"

/* platform type*/

typedef enum ifcm_plat_type
   {
   IFCM_PLAT_FSL_P1010_RDB = 1,     /* freescale p1010 rdb board */
   IFCM_PLAT_FSL_T4240_QDS = 2,     /* freescale t4240 qds board */
   } IFCM_PLAT_TYPE;

/* flag defines */

#define IFCM_FLAG_HWECCDIS  0x01    /* disable IFCM hardware ECC */
#define IFCM_FLAG_HWECCBCH4 0x02    /* use 4bit IFCM hardware ECC */
#define IFCM_FLAG_HWECCBCH8 0x03    /* use 8bit IFCM hardware ECC */

#ifdef  __cplusplus
}
#endif

#endif /* __INCvxbIfcmh */
