/* vxbIfcmCore.h -  core code header for IFC IFCM driver */

/*
 * Copyright (c) 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,29oct13,ywu  written.
*/

#ifndef __INCvxbIfcmCoreh
#define __INCvxbIfcmCoreh

#ifdef  __cplusplus
 extern "C" {
#endif

#include <vxWorks.h>
#include <stdlib.h>
#include <string.h>
#include <logLib.h>
#include <hwif/vxbus/vxBus.h>
#include "vxbNandFlash.h"
#include "vxbNandFlashLib.h"
#include "vxbIfcm.h"

#define IFCM_MAX_CS             0xc
#define IFCM_P1010_MAX_CS       0x4
#define IFCM_T4240_MAX_CS       0x4

#define IFCM_P1010_LAWAR_TGTIF  (0x4 << 20)

/* IFCM CS Property Register Macros */

#define IFCM_CSPR_VALID_BIT     (0x1 << 0)      /* valid bit */

#define IFCM_CSPR_MSEL_NOR      (0x0 << 1)      /* NOR Flash selected */
#define IFCM_CSPR_MSEL_NAND     (0x1 << 1)      /* NAND Flash selected */
#define IFCM_CSPR_MSEL_GPCM     (0x2 << 1)      /* GPCM selected */
#define IFCM_CSPR_MSEL_MASK     (0x3 << 1)      /* machine select mask */

#define IFCM_CSPR_WP_RW         (0x0 << 6)      /* read/write allowed */
#define IFCM_CSPR_WP_RO         (0x1 << 6)      /* only read allowed */
#define IFCM_CSPR_WP_MASK       (0x1 << 6)      /* write protect mask */

#define IFCM_CSPR_PS_8BIT       (0x1 << 7)      /* 8-bit port width */
#define IFCM_CSPR_PS_16BIT      (0x2 << 7)      /* 16-bit port width */
#define IFCM_CSPR_PS_MASK       (0x3 << 7)      /* port width mask */

#define IFCM_CSPR_BA_MASK       0xffff          /* 16 bit base address mask */

#define IFCM_CSAM_MASK          0xfffff         /* 1MB AM mask */

/* IFCM CS Option Register Macros */

#define IFCM_CSOR_BCTLD         (0x1 << 0)      /* BCTL is disabled */
#define IFCM_CSOR_TRHS_MASK     (0x3 << 2)      /* 3-bit TRHS */

#define IFCM_CSOR_PB_32         (0x0 << 8)      /* pages per block */
#define IFCM_CSOR_PB_64         (0x1 << 8)
#define IFCM_CSOR_PB_128        (0x2 << 8)
#define IFCM_CSOR_PB_256        (0x3 << 8)

#define IFCM_CSOR_SPRZ_16       (0x0 << 13)     /* byte spare size */
#define IFCM_CSOR_SPRZ_64       (0x1 << 13)
#define IFCM_CSOR_SPRZ_128      (0x2 << 13)
#define IFCM_CSOR_SPRZ_210      (0x3 << 13)
#define IFCM_CSOR_SPRZ_218      (0x4 << 13)
#define IFCM_CSOR_SPRZ_224      (0x5 << 13)

#define IFCM_CSOR_PGS_512       (0x0 << 19)     /* page size */
#define IFCM_CSOR_PGS_2K        (0x1 << 19)
#define IFCM_CSOR_PGS_4K        (0x2 << 19)
#define IFCM_CSOR_PGS_MASK      (0x3 << 19)

#define IFCM_CSOR_RAL_1BYTE     (0x0 << 23)     /* row address length */
#define IFCM_CSOR_RAL_2BYTE     (0x1 << 23)
#define IFCM_CSOR_RAL_3BYTE     (0x2 << 23)
#define IFCM_CSOR_RAL_4BYTE     (0x3 << 23)

#define IFCM_CSOR_DECC_EN       (0x1 << 26)     /* ECC decoding enabled */
#define IFCM_CSOR_ECC_4BIT      (0x0 << 28)     /* 4-bit ecc correction */
#define IFCM_CSOR_ECC_8BIT      (0x1 << 28)     /* 8-bit ecc correction */
#define IFCM_CSOR_EECC_EN       (0x1 << 31)     /* ECC encoding enabled */

/* IFCM Flash timing Register Macros */

#define IFCM_FTIM0_TWH_OFF      0               /* TWH offset */
#define IFCM_FTIM0_TWCHT_OFF    8               /* TWCHT offset */
#define IFCM_FTIM0_TWP_OFF      16              /* TWP offset */
#define IFCM_FTIM0_TCCST_OFF    25              /* TCCST offset */

#define IFCM_FTIM1_TRP_OFF      0               /* TRP offset */
#define IFCM_FTIM1_TRR_OFF      8               /* TRR offset */
#define IFCM_FTIM1_TWBE_OFF     16              /* TWBE offset */
#define IFCM_FTIM1_TADLE_OFF    24              /* TADLE offset */

#define IFCM_FTIM2_TWHRE_OFF    0               /* TWHRE offset */
#define IFCM_FTIM2_TREH_OFF     11              /* TREH offset */
#define IFCM_FTIM2_TRAD_OFF     21              /* TRAD offset */

#define IFCM_FTIM3_TWW_OFF      24              /* TWW offset */

/* IFCM NAND Flash Register Macros */
#define IFCM_NAND_CSEL_OFF      26              /* NAND CSEL bit offset */

/* IFCM Nand Event and error enable Register Macros */


#define IFCM_NEVTERN_ECCEREN    (1 << 25)       /* ECC error log enable */
#define IFCM_NEVTERN_WPEREN     (1 << 26)       /* wp error check enable */
#define IFCM_NEVTERN_FTOEREN    (1 << 27)       /* flash timeout check enable */
#define IFCM_NEVTERN_PGRDCMPLEN (1 << 29)       /* read complet event enable */
#define IFCM_NEVTERN_OPCEN      (1 << 31)       /* operation complete enable */

/* IFCM NAND Event and  Error Status Regsiter Macros */

#define IFCM_NEVTER_BBI_SCHSEL  (1 << 11)       /* bad block indicator search */
#define IFCM_NEVTER_BOOT_DN     (1 << 14)       /* boot loading */
#define IFCM_NEVTER_ECCER       (1 << 25)       /* ecc error */
#define IFCM_NEVTER_WPER        (1 << 26)       /* write protect error */
#define IFCM_NEVTER_FTOER       (1 << 27)       /* flash timeout error */
#define IFCM_NEVTER_OPC         (1 << 31)       /* nand operation complete */

/* IFCM Flash Command Register Macros */

#define IFCM_NFCR0_CMD0_OFFSET  24              /* NFCR CMD0 offset */
#define IFCM_NFCR0_CMD1_OFFSET  16              /* NFCR CMD1 offset */
#define IFCM_NFCR0_CMD2_OFFSET  8               /* NFCR CMD2 offset */
#define IFCM_NFCR0_CMD3_OFFSET  0               /* NFCR CMD3 offset */

#define IFCM_NFCR1_CMD4_OFFSET  24              /* NFCR CMD4 offset */
#define IFCM_NFCR1_CMD5_OFFSET  16              /* NFCR CMD5 offset */
#define IFCM_NFCR1_CMD6_OFFSET  8               /* NFCR CMD6 offset */
#define IFCM_NFCR1_CMD7_OFFSET  0               /* NFCR CMD7 offset */

/* IFCM Flash Instruction Register Macros */

#define IFCM_NFIR_OP_NOP        0x0             /* nop or end of op sequence */

#define IFCM_NFIR_OP_CA0        0x1             /* issue column addr in COL0 */
#define IFCM_NFIR_OP_CA1        0x2             /* issue column addr in COL1 */
#define IFCM_NFIR_OP_CA2        0x3             /* issue column addr in COL2 */
#define IFCM_NFIR_OP_CA3        0x4             /* issue column addr in COL3 */

#define IFCM_NFIR_OP_RA0        0x5             /* issue row addr in ROW0 */
#define IFCM_NFIR_OP_RA1        0x6             /* issue row addr in ROW1 */
#define IFCM_NFIR_OP_RA2        0x7             /* issue row addr in ROW2 */
#define IFCM_NFIR_OP_RA3        0x8             /* issue row addr in ROW3 */

#define IFCM_NFIR_OP_CMD0       0x9             /* issue cmd in NFCR[CMD0] */
#define IFCM_NFIR_OP_CMD1       0xa             /* issue cmd in NFCR[CMD1] */
#define IFCM_NFIR_OP_CMD2       0xb             /* issue cmd in NFCR[CMD2] */
#define IFCM_NFIR_OP_CMD3       0xc             /* issue cmd in NFCR[CMD3] */
#define IFCM_NFIR_OP_CMD4       0xd             /* issue cmd in NFCR[CMD4] */
#define IFCM_NFIR_OP_CMD5       0xe             /* issue cmd in NFCR[CMD5] */
#define IFCM_NFIR_OP_CMD6       0xf             /* issue cmd in NFCR[CMD6] */
#define IFCM_NFIR_OP_CMD7       0x10            /* issue cmd in NFCR[CMD7] */

#define IFCM_NFIR_OP_CW0        0x11            /* wait then issue NFCR[CMD0] */
#define IFCM_NFIR_OP_CW1        0x12            /* wait then issue NFCR[CMD1] */
#define IFCM_NFIR_OP_CW2        0x13            /* wait then issue NFCR[CMD2] */
#define IFCM_NFIR_OP_CW3        0x14            /* wait then issue NFCR[CMD3] */
#define IFCM_NFIR_OP_CW4        0x15            /* wait then issue NFCR[CMD4] */
#define IFCM_NFIR_OP_CW5        0x16            /* wait then issue NFCR[CMD5] */
#define IFCM_NFIR_OP_CW6        0x17            /* wait then issue NFCR[CMD6] */
#define IFCM_NFIR_OP_CW7        0x18            /* wait then issue NFCR[CMD7] */

#define IFCM_NFIR_OP_WBCD       0x19            /* write BC bytes from buf */
#define IFCM_NFIR_OP_RBCD       0x1a            /* read BC bytes to buf */
#define IFCM_NFIR_OP_RTRD       0x1b            /* read BC bytes for boot */
#define IFCM_NFIR_OP_RDSTAT     0x1c            /* wait and read 1byte to RS0 */
#define IFCM_NFIR_OP_NWAIT      0x1d            /* wait NCFG[NUM_WAIT] cycles */
#define IFCM_NFIR_OP_WFR        0x1e            /* wait tWB time, poll Ready */
#define IFCM_NFIR_OP_SBRD       0x1f            /* wait and read 1byte to MDR */
#define IFCM_NFIR_OP_UA         0x20            /* issue row addr in ROW3 */
#define IFCM_NFIR_OP_RB         0x21            /* wait/read BC bytes to buf */

#define IFCM_NFIR0_OP0_OFFSET   26              /* NFIR OP0 offset */
#define IFCM_NFIR0_OP1_OFFSET   20              /* NFIR OP1 offset */
#define IFCM_NFIR0_OP2_OFFSET   14              /* NFIR OP2 offset */
#define IFCM_NFIR0_OP3_OFFSET   8               /* NFIR OP3 offset */
#define IFCM_NFIR0_OP4_OFFSET   2               /* NFIR OP4 offset */

#define IFCM_NFIR1_OP5_OFFSET   26              /* NFIR OP5 offset */
#define IFCM_NFIR1_OP6_OFFSET   20              /* NFIR OP6 offset */
#define IFCM_NFIR1_OP7_OFFSET   14              /* NFIR OP7 offset */
#define IFCM_NFIR1_OP8_OFFSET   8               /* NFIR OP8 offset */
#define IFCM_NFIR1_OP9_OFFSET   2               /* NFIR OP9 offset */

#define IFCM_NFIR2_OP10_OFFSET  26              /* NFIR OP10 offset */
#define IFCM_NFIR2_OP11_OFFSET  20              /* NFIR OP11 offset */
#define IFCM_NFIR2_OP12_OFFSET  14              /* NFIR OP12 offset */
#define IFCM_NFIR2_OP13_OFFSET  8               /* NFIR OP13 offset */
#define IFCM_NFIR2_OP14_OFFSET  2               /* NFIR OP14 offset */

/* IFCM Address register Macros */

#define IFCM_COL_MS_OFFSET      31              /* m/s region  locator offset */
#define IFCM_COL_CA_MASK        0xfff           /* column address mask */

/* IFCM NAND Chip Select Register Macros */

#define IFCM_CSEL_OFFSET        26              /* csel offset */

/* IFCM NAND Operation Sequence Start Register Macros */

#define IFCM_NSEQST_AUTO_STAT_RD    (1 << 11)   /* automatic status read */
#define IFCM_NSEQST_AUTO_RD         (1 << 14)   /* automatic read */
#define IFCM_NSEQST_AUTO_CPB        (1 << 17)   /* automatic copyback */
#define IFCM_NSEQST_AUTO_PGM        (1 << 20)   /* automatic program */
#define IFCM_NSEQST_AUTO_ERA        (1 << 23)   /* automatic erase */
#define IFCM_NSEQST_NAND_FIR_STRT   (1 << 31)   /* nand operation start */

/* IFCM NAND ECC State and Result Register Macros */

#define IFCM_ECC_STATE_MASK     0xf             /* 4 bit mask for ECC state */

/* IFCM NAND Flash status  Register Macros */

#define IFCM_FSR_RS0_OFFSET     24              /* first byte of FSR */
#define IFCM_FSR_RS1_OFFSET     16              /* second byte of FSR */

/* register map */

typedef struct ifcm_bank_reg {                  /* 0xc byte bank reg */
    UINT32  reg;
    UINT8   reserved [0x8];
    } IFCM_BANK_REG;

typedef struct ifcm_ftim_reg {                  /* 0x30 byte time reg */
    UINT32  ftim [0x4];
    UINT8   reserved [0x20];
    } IFCM_FTIM_REG;

typedef struct ifcm_addr_reg {                  /* 0x10 byte address reg */
    UINT32  row;
    UINT8   reserved0[0x4];
    UINT32  col;
    UINT8   reserved1[0x4];
    } IFCM_ADDR_REG;

typedef struct ifcm_regs
    {
    UINT32          rev;                  /* 0x000 - IFC revision number reg */
    UINT8           pad1 [0xc];           /* 0x004 - reserved */
    IFCM_BANK_REG   cspr [IFCM_MAX_CS];   /* 0x010 - chip-select property reg */
    IFCM_BANK_REG   csam [IFCM_MAX_CS];   /* 0x0a0 - address mask reg, */
    IFCM_BANK_REG   csor [IFCM_MAX_CS];   /* 0x130 - chip-select option reg, */
    IFCM_FTIM_REG   csft [IFCM_MAX_CS];   /* 0x1c0 - chip-select timing reg */
    /*
     * the actual number of chip select registers could be less
     * than 12, and rest part should be reserved unused. The reason to keep
     * all of them  defined is in order to unify reg struct of different
     * platforms, the real access should be controlled by pIfcm->uMaxBankCnt.
     */
    UINT32          rbstat;               /* 0x400 - ready busy status reg */
    UINT8           pad2 [0x8];           /* 0x404 - reserved */
    UINT32          gcr;                  /* 0x40c - general control reg */
    UINT8           pad3 [0x8];           /* 0x410 - reserved */
    UINT32          cevterstat;           /* 0x418 - common event/err status */
    UINT8           pad4 [0x8];           /* 0x41c - reserved */
    UINT32          cevteren;             /* 0x424 - common event/err enable */
    UINT8           pad5 [0x8];           /* 0x428 - reserved */
    UINT32          cevterie;             /* 0x430 - event/err intr enable */
    UINT8           pad6 [0x8];           /* 0x434 - reserved */
    UINT32          eatr0;                /* 0x43c - transfer errot attr reg0 */
    UINT32          eatr1;                /* 0x440 - transfer errot attr reg1 */
    UINT8           pad7 [0x8];           /* 0x444 - reserved */
    UINT32          ccr;                  /* 0x44c - clock control reg */
    UINT32          csr;                  /* 0x450 - clock status reg */
    UINT8           pad8 [0xbac];         /* 0x454 - reserved */
    UINT32          ncfgr;                /* 0x1000 - nand configuration reg */
    UINT8           pad9 [0x10];          /* 0x1004 - reserved */
    UINT32          nfcr0;                /* 0x1014 - nand flash command reg0 */
    UINT32          nfcr1;                /* 0x1018 - nand flash command reg1 */
    UINT8           pad10 [0x20];         /* 0x101c - reserved */
    IFCM_ADDR_REG   addr [0x4];           /* 0x103c - flash address reg */
    UINT8           pad11 [0x8c];         /* 0x107c - reserved */
    UINT32          nbcr;                 /* 0x1108 - nand byte count reg */
    UINT8           pad12 [0x4];          /* 0x110c - reserved */
    UINT32          nfir0;                /* 0x1110 - nand instruction reg0 */
    UINT32          nfir1;                /* 0x1114 - nand instruction reg1 */
    UINT32          nfir2;                /* 0x1118 - nand instruction reg2 */
    UINT8           pad13 [0x40];         /* 0x111c - reserved */
    UINT32          ncsel;                /* 0x115c - nand chip-select reg */
    UINT8           pad14 [0x4];          /* 0x1160 - reserved */
    UINT32          nseqstrt;             /* 0x1164 - nand seq start reg */
    UINT8           pad15 [0x4];          /* 0x1168 - reserved */
    UINT32          nevterstat;           /* 0x116c - nand event/error status */
    UINT8           pad16 [0x4];          /* 0x1170 - reserved */
    UINT32          rdcmplevt;            /* 0x1174 - rd complet event status */
    UINT8           pad17 [0x8];          /* 0x1178 - reserved */
    UINT32          nevteren;             /* 0x1180 - nand event/error enable */
    UINT8           pad18 [0x8];          /* 0x1184 - reserved */
    UINT32          nevterie;             /* 0x118c - event/error intr enable */
    UINT8           pad19 [0x8];          /* 0x1190 - reserved */
    UINT32          nerattr0;             /* 0x1198 - transfer error attr 0 */
    UINT32          nerattr1;             /* 0x119c - transfer error attr 1 */
    UINT8           pad20 [0x40];         /* 0x11a0 - reserved */
    UINT32          nfsr;                 /* 0x11e0 - nand status reg */
    UINT8           pad21 [0x4];          /* 0x11e4 - reserved */
    UINT32          eccstat[0x4];         /* 0x11e8 - ecc status and result */
    UINT8           pad22 [0x80];         /* 0x11f8 - reserved */
    UINT32          ncr;                  /* 0x1278 - nand control reg */
    UINT8           pad23 [0x8];          /* 0x127c - reserved */
    UINT32          nabtrgr;              /* 0x1284 - autoboot trigger reg */
    UINT8           pad24 [0x4];          /* 0x1288 - reserved */
    UINT32          nmdr;                 /* 0x128c - nand memory data reg */
/*  UINT8           pad25 [0xd70];           0x1290 - reserved */
    } IFCM_REGS;

/* CS type */

typedef enum ifcm_cs_type
   {
   IFCM_CS_TYPE_OTHER = 0,
   IFCM_CS_TYPE_NAND_8BIT,                /* 8-bit NAND Flash */
   IFCM_CS_TYPE_NAND_16BIT                /* 16-bit NAND Flash */
   } IFCM_CS_TYPE;

/* controller information */

typedef struct ifcm
    {
    VXB_DEVICE_ID           pDev;
    void *                  pRegHandle;
    void *                  pCsHandle;
    ULONG                   uRegBase;     /* register base */
    ULONG                   uCsBase;      /* CS base */
    ULONG                   uCsSize;      /* CS size */
    UINT32                  uPlatType;    /* platform type */
    UINT32                  uFlag;
    IFCM_CS_TYPE            uCsType[IFCM_MAX_CS];
    UINT8                   uNandCsCnt;   /* CS count for NAND */
    UINT8                   uMaxBankCnt;  /* max bank count */
    DL_LIST                 chipList;

    FLASH_COL_ADDR_T        tColAddr;     /* tmp coladdr for  mutl-cycle */
    FLASH_ADDR_T            tPageAddr;    /* tmp pageaddr for multi-cycle */
    UINT32                  tBufAddr;     /* tmp address for rw buffer */
    UINT32                  tBufIndex;    /* tmp index for rw buffer addr */
    } IFCM, *pIFCM;

/* address space dfines */

#define IFCM_REG_BAR_ID     0             /* CCSBAR register address space */
#define IFCM_CS_BAR_ID      1             /* NAND buffer address space */

/* controller access defines */

#define IFCM_REG_READ(pIfcm, member)                                           \
    vxbRead32 ((pIfcm)->pRegHandle,                                            \
        (void *)((pIfcm)->uRegBase + (ULONG) (offsetof (IFCM_REGS, member))))

#define IFCM_REG_WRITE(pIfcm, member, data)                                    \
    vxbWrite32 ((pIfcm)->pRegHandle,                                           \
        (void *)((pIfcm)->uRegBase + (ULONG) (offsetof (IFCM_REGS, member))),  \
        (ULONG) (data))

#define IFCM_DBG_NONE       0
#define IFCM_DBG_BASIC      1
#define IFCM_DBG_VERBOSE    2
#define IFCM_DBG_ALL        3

/* debug information */

#define IFCM_DBG_MSG(level, fmt, a, b, c, d, e, f)                             \
    do                                                                         \
        {                                                                      \
        if (((level) <= gIfcmDbgLvl) && (_func_logMsg != NULL))                \
            _func_logMsg (IFCM_DRV_NAME": "fmt, a, b, c, d, e, f);             \
        } while (0)

#define IFCM_DBG_ASSERT(val)                                                   \
    do                                                                         \
        {                                                                      \
        if ((gIfcmDbgLvl > IFCM_DBG_BASIC) &&                                  \
            (_func_logMsg != NULL) && (val))                                   \
            printf ("!ASSERT FAIL at %s, %d\n",__FUNCTION__, __LINE__);        \
        } while (0)

/* Function Declaration */

int  vxbIfcmFlashInit   (pIFCM pIfcm, BOOL flag);
void vxbIfcmFlashClean  (pIFCM pIfcm);

#ifdef  __cplusplus
}
#endif

#endif /* __INCvxbIfcmCoreh */
