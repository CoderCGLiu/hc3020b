/* vxbZynq7kSio.h - Xilinx Zynq-7000 VxBus Serial header file */

/*
 * Copyright (c) 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */ 

/*
modification history
--------------------
01a,11may11,rab  written.
*/

/*
DESCRIPTION
This module contains constants and defines for the Xilinx Zynq-7000 Sio.
*/

#ifndef __INCvxbZynq7kSioh 
#define __INCvxbZynq7kSioh

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _ASMLANGUAGE

#include <sioLib.h>
#include <spinLockLib.h>
#include <isrDeferLib.h>

/* Register offsets from base address */

#define ZYNQ_US_CR      0x00        /* USART Control */
#define ZYNQ_US_MR      0x04        /* USART Mode */
#define ZYNQ_US_IER     0x08        /* USART Interrupt Enable */
#define ZYNQ_US_IDR     0x0C        /* USART Interrupt Disable */
#define ZYNQ_US_IMR     0x10        /* USART Interrupt Mask */
#define ZYNQ_US_CSR     0x14        /* USART Channel Interrupt Status */
#define ZYNQ_US_BRGR    0x18        /* USART Baud-Rate Generator */
#define ZYNQ_US_RTOR    0x1C        /* USART Receiver Time Out */
#define ZYNQ_US_RXTR    0x20        /* USART Receiver FIFO Trigger */
#define ZYNQ_US_CHSR    0x2C        /* USART Channel Status */
#define ZYNQ_US_FIFO    0x30        /* USART TX and RX FIFO */
#define ZYNQ_US_BRDV    0x34        /* USART Baud-Rate Divider */
#define ZYNQ_US_FLDL    0x38        /* USART Flow Control Delay */
#define ZYNQ_US_TXTR    0x44        /* USART Transmitter FIFO Trigger */

/* bit definitions within US_CR register */

#define ZYNQ_US_RSTRX   (1 << 0)      /* reset receiver */
#define ZYNQ_US_RSTTX   (1 << 1)      /* reset transmitter */
#define ZYNQ_US_RXEN    (1 << 2)      /* enable received */
#define ZYNQ_US_RXDIS   (1 << 3)      /* disable receiver */
#define ZYNQ_US_TXEN    (1 << 4)      /* enable transmitter */
#define ZYNQ_US_TXDIS   (1 << 5)      /* disable transmitter */
#define ZYNQ_US_STTTO   (1 << 6)      /* start RX timeout */
#define ZYNQ_US_STBRK   (1 << 7)      /* start TX break */
#define ZYNQ_US_STPBRK  (1 << 8)      /* stop TX break */

/* bit definitions within US_MR register */

#define ZYNQ_US_NOPAR   (4 << 3)      /* set no parity */
#define ZYNQ_US_CHRL_8  0x00        /* set 8-bit characters */

/* bit definitions within US_IER, IMR, ISR and CSR registers */

#define ZYNQ_US_RXTRIG  ( 1<<0 )      /* Receiver trigger */
#define ZYNQ_US_RXEMPTY ( 1<<1 )      /* Receiver empty */
#define ZYNQ_US_RXFULL  ( 1<<2 )      /* Receiver full */
#define ZYNQ_US_TXEMPTY ( 1<<3 )      /* Transmitter empty */
#define ZYNQ_US_TXFULL  ( 1<<4 )      /* Transmitter full */
#define ZYNQ_US_OVRE    ( 1<<5 )      /* Receiver overflow error */
#define ZYNQ_US_FRAME   ( 1<<6 )      /* Receiver frame error */
#define ZYNQ_US_PARE    ( 1<<7 )      /* Receiver parity error */
#define ZYNQ_US_TIMEOUT ( 1<<8 )      /* Receiver timeout */
#define ZYNQ_US_TXTRIG  ( 1<<10 )     /* Transmitter trigger */
#define ZYNQ_US_TNFULL  ( 1<<11 )     /* Transmitter nearly full */
#define ZYNQ_US_TXOVR   ( 1<<12 )     /* Transmitter overflow error */

typedef struct vxbZynqChan
    {
    /* must be first */

    SIO_DRV_FUNCS * pDrvFuncs;          /* driver functions */

    /* callbacks */

    STATUS          (*getTxChar) ();    /* installed Tx callback routine */
    STATUS          (*putRcvChar) ();   /* installed Rx callback routine */
    void *          getTxArg;           /* argument to Tx callback routine */
    void *          putRcvArg;          /* argument to Rx callback routine */
    
    UINT32          channelMode;        /* such as INT, POLL modes */
    int             baudRate;           /* the current baud rate */
    UINT32          xtal;               /* UART clock frequency */  

    int             regIndex;           /* bus subsystem: reg base index */
    VXB_DEVICE_ID   pDev;               /* bus subsystem: device ID */
    int             channelNo;          /* channel number */

    UINT32          irq;                /* IRQ level */
    UINT32          intLevel;
    UINT32          clkRate;
    UINT32          divisor;
    UINT32          mode;
    UINT32          options;            /* hardware setup options */
    BOOL            isUsart;            /* DBGU is compatible with USART */
    
    /* need for SMP and ISR deferral model */

    spinlockIsr_t   spinlockIsr;        /* ISR-callable spinlock */

    /*
     * Each device may have multiple channels.  To handle this,
     * we keep a pointer to a pChan in pDev->pDrvCtrl, and maintain
     * a linked list of channels per controller.
     */

    struct vxbZynqChan * pNext;         /* next channel for this device */

    } VXB_ZYNQ_CHAN;

#endif /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbZynq7kSioh */
