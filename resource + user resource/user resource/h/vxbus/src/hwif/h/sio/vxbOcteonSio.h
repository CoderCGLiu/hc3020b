/* vxbOcteonSio.h - Octeon 16552ish DUART header file */

/*
 * Copyright (c) 2007-2008, 2010 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01d,15jul10,d_c  Change to use WR native functions for I/O.
                 Remove Cavium SDK references.
01c,08jan10,h_k  LP64 adaptation.
01b,07apr08,agf  redesign UART register accesses (WIND00120190)
01a,04may07,jmt  written from ns16552.h.
*/

#ifndef __INCvxbOcteonSioh
#define __INCvxbOcteonSioh

#ifdef __cplusplus
extern "C" {
#endif

/*
 *********************************************************************
 *      Copyright (c) 1990,1991 Intel Corporation
 *
 * Intel hereby grants you permission to copy, modify, and
 * distribute this software and its documentation.  Intel grants
 * this permission provided that the above copyright notice
 * appears in all copies and that both the copyright notice and
 * this permission notice appear in supporting documentation.  In
 * addition, Intel grants this permission provided that you
 * prominently mark as not part of the original any modifications
 * made to this software or documentation, and that the name of
 * Intel Corporation not be used in advertising or publicity
 * pertaining to distribution of the software or the documentation
 * without specific, written prior permission.
 *
 * Intel Corporation does not warrant, guarantee or make any
 * representations regarding the use of, or the results of the use
 * of, the software and documentation in terms of correctness,
 * accuracy, reliability, currentness, or otherwise; and you rely
 * on the software, documentation and results solely at your own risk.
 *
 *\NOMANUAL
 **********************************************************************
*/

/*
 ******************************************************************************
 *
 * REGISTER DESCRIPTION OF NATIONAL 16552 DUART
 *
 * $Id: ns16552.h,v 2.1 1993/06/07 15:07:59 wise active $
 *
 *\NOMANUAL
 *******************************************************************************
*/

#ifndef _ASMLANGUAGE

#include <sioLib.h>
#include <spinLockLib.h>
#include <isrDeferLib.h>

/* Octeon CN3xxx UART registers are in extended mem space at the following address */

#define CAV_IO_SEG_ADDR  0x8001180000000000ull


/* Register offsets from base address */

#define RBR     0x00    /* receiver buffer register */
#define IER     0x08    /* interrupt enable register */
#define IIR     0x10    /* interrupt identification register */
#define LCR     0x18    /* line control register */
#define MCR     0x20    /* modem control register */
#define LSR     0x28    /* line status register */
#define MSR     0x30    /* modem status register */
#define SCR     0x38    /* scratch register */
#define THR     0x40    /* transmit holding register */
#define FCR     0x50    /* FIFO control register */
#define DLL     0x80    /* divisor latch low */
#define DLH     0x88    /* divisor latch high */
#define FAR     0x120   /* FIFO access register */
#define TFR     0x128   /* transmit FIFO read */
#define RFW     0x130   /* receive FIFO write */
#define USR     0x138   /* uart status register */
#define TFL     0x200   /* transmit FIFO level */
#define RFL     0x208   /* receive FIFO level */
#define SRR     0x210   /* software reset register */
#define SRTS    0x218   /* shadow request to send */
#define SBCR    0x220   /* shadow break control */
#define SFE     0x230   /* shadow fifo enable */
#define SRT     0x238   /* shadow recv trigger */
#define STT     0x300   /* shadow trans trigger */
#define HTX     0x308   /* halt transmit */

#define BAUD_LO(baud)  ((XTAL/(16*baud)) & 0xff)
#define BAUD_HI(baud)  (((XTAL/(16*baud)) & 0xff00) >> 8)

/* Line Control Register */

#define CHAR_LEN_5  0x00    /* 5bits data size */
#define CHAR_LEN_6  0x01    /* 6bits data size */
#define CHAR_LEN_7  0x02    /* 7bits data size */
#define CHAR_LEN_8  0x03    /* 8bits data size */
#define LCR_STB     0x04    /* 2 stop bits */
#define ONE_STOP    0x00    /* one stop bit */
#define LCR_PEN     0x08    /* parity enable */
#define PARITY_NONE 0x00    /* parity disable */
#define LCR_EPS     0x10    /* even parity select */
#define LCR_SP      0x20    /* stick parity select */
#define LCR_SBRK    0x40    /* break control bit */
#define LCR_DLAB    0x80    /* divisor latch access enable */
#define DLAB        LCR_DLAB

/* Line Status Register */

#define LSR_DR      0x01    /* data ready */
#define RxCHAR_AVAIL    LSR_DR  /* data ready */
#define LSR_OE      0x02    /* overrun error */
#define LSR_PE      0x04    /* parity error */
#define LSR_FE      0x08    /* framing error */
#define LSR_BI      0x10    /* break interrupt */
#define LSR_THRE    0x20    /* transmit holding register empty */
#define LSR_TEMT    0x40    /* transmitter empty */
#define LSR_FERR    0x80    /* in fifo mode, set when PE,FE or BI error */

/* Interrupt Identification Register */

#define IIR_BUSY      0x07
#define IIR_IP      0x01
#define IIR_ID      0x0e
#define IIR_RLS     0x06    /* received line status */
#define Rx_INT      IIR_RLS /* received line status */
#define IIR_RDA     0x04    /* received data available */
#define RxFIFO_INT  IIR_RDA /* received data available */
#define IIR_THRE    0x02    /* transmit holding register empty */
#define TxFIFO_INT  IIR_THRE 
#define IIR_MSTAT   0x00    /* modem status */
#define IIR_TIMEOUT 0x0c    /* char receive timeout */

/* Interrupt Enable Register */

#define IER_ERDAI   0x01    /* received data avail. & timeout int */
#define RxFIFO_BIT  IER_ERDAI
#define IER_ETHREI  0x02    /* transmitter holding register empty int */
#define TxFIFO_BIT  IER_ETHREI
#define IER_ELSI    0x04    /* receiver line status int enable */
#define Rx_BIT      IER_ELSI
#define IER_EMSI    0x08    /* modem status int enable */

/* Modem Control Register */

#define MCR_DTR     0x01    /* dtr output */
#define DTR     MCR_DTR
#define MCR_RTS     0x02    /* rts output */
#define MCR_OUT1    0x04    /* output #1 */
#define MCR_OUT2    0x08    /* output #2 */
#define MCR_LOOP    0x10    /* loopback enable */

/* Modem Status Register */

#define MSR_DCTS    0x01    /* cts change */
#define MSR_DDSR    0x02    /* dsr change */
#define MSR_TERI    0x04    /* ring indicator change */
#define MSR_DDCD    0x08    /* data carrier indicator change */
#define MSR_CTS     0x10    /* complement of cts */
#define MSR_DSR     0x20    /* complement of dsr */
#define MSR_RI      0x40    /* complement of ring signal */
#define MSR_DCD     0x80    /* complement of dcd */

/* FIFO Control Register */

#define FCR_EN      0x01    /* enable xmit and rcvr */
#define FIFO_ENABLE FCR_EN
#define FCR_RXCLR   0x02    /* clears rcvr fifo */
#define RxCLEAR     FCR_RXCLR
#define FCR_TXCLR   0x04    /* clears xmit fifo */
#define TxCLEAR     FCR_TXCLR
/* #define FCR_DMA     0x08 */    /* dma */
#define FCR_RXTRIG_L    0x40    /* rcvr fifo trigger lvl low */
#define FCR_RXTRIG_H    0x80    /* rcvr fifo trigger lvl high */

typedef  struct octeonVxbChan     /* OCTEONVXB_CHAN * */
    {
    /* always goes first */

    SIO_DRV_FUNCS *     pDrvFuncs;      /* driver functions */

    /* callbacks */

    STATUS      (*getTxChar) (); /* pointer to xmitr function */
    STATUS      (*putRcvChar) (); /* pointer to rcvr function */
    void *      getTxArg;
    void *      putRcvArg;

    UINT8   *regs;      /* NS16550 registers */
    UINT8   level;      /* 8259a interrupt level for this device */
    UINT8   ier;        /* copy of ier */
    UINT8   lcr;        /* copy of lcr, not used by ns16552 driver */
    UINT8   mcr;        /* copy of modem control register */
    UINT8   pad1;

    UINT16      channelMode;    /* such as INT, POLL modes */
    UINT16      regDelta;   /* register address spacing */
    int         baudRate;
    int         xtal;       /* UART clock frequency     */
    int         options;    /* hardware setup options */

    int     regIndex;   /* bus subsystem: reg base index */
    VXB_DEVICE_ID pDev;     /* bus subsystem: device ID */

    int     channelNo;  /* channel number */
    int (*pDivisorFunc)(int, int); /* allow BSP to calulate divisor */
    spinlockIsr_t spinlockIsr;  /* ISR-callable spinlock */
    BOOL    txDefer;    /* TX interrupt is currently deferred */
    ISR_DEFER_QUEUE_ID queueId;
    ISR_DEFER_JOB isrDefRd;
    ISR_DEFER_JOB isrDefWr;

    /*
     * Each device may have multiple channels.  To handle this,
     * we keep a pointer to a pChan in pDev->pDrvCtrl, and maintain
     * a linked list of channels per controller.
     */
    struct octeonVxbChan * pNext; /* next channel for this device */

    UINT32		initLevel;

    } OCTEONVXB_CHAN;

/* 16550 initialization structure for plb based vxBus devices */

typedef struct octeonVxbChanParams
    {
    ULONG   vector;         /* Actual architectural vector */
    ULONG   baseAdrs;       /* Virt address of register set */
    ULONG   regSpace;       /* Increment between registers - usually 1 */
    ULONG   intLevel;       /* interrupt level */
    } VX_BUS_OCTEONVXB_CHAN_PARAS;

/* function declarations */

#if defined(__STDC__) || defined(__cplusplus)

extern void octeonVxbInt (VXB_DEVICE_ID pDev);
extern void octeonVxbDevInit (OCTEONVXB_CHAN *);

#else

extern void octeonvxbInt ();
extern void octeonvxbDevInit ();

#endif  /* __STDC__ */

METHOD_DECL(sioEnable);

#endif  /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif
 
#endif /* __INCvxbOcteonSioh */
