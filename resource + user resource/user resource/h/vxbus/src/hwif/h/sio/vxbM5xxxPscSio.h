/* vxbM5xxxPscSio.h - MPC5xxx Programmable Serial Controller UART Driver */

/*
 * Copyright (c) 2007, 2009, 2014 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
14aug14,p_x  add FIFOC count and size definitions. (VXW6-83175)
01d,12mar09,b_m  unified FIFO allocation.
01c,18dec07,b_m  add spinlock definitions.
01b,12sep07,b_m  add MPC5121e specific FIFO definitions.
01a,08aug07,b_m  written.
*/

#ifndef __INCvxbM5xxxPscSioh
#define __INCvxbM5xxxPscSioh

#ifdef __cplusplus
extern "C" {
#endif

/* PSC UART register offsets */

#define PSC_UART_MR1        0x00
#define PSC_UART_MR2        0x00
#define PSC_UART_SR         0x04
#define PSC_UART_CSR        0x04
#define PSC_UART_CR         0x08
#define PSC_UART_RB         0x0c
#define PSC_UART_TB         0x0c
#define PSC_UART_IPCR       0x10
#define PSC_UART_ACR        0x10
#define PSC_UART_ISR        0x14
#define PSC_UART_IMR        0x14
#define PSC_UART_CTUR       0x18
#define PSC_UART_CTLR       0x1c
#define PSC_UART_IP         0x34
#define PSC_UART_OP1        0x38
#define PSC_UART_OP0        0x3c
#define PSC_UART_SICR       0x40

/* FIFOC register offsets (MPC512x specific) */

#define FIFOC_TX_CMD        0x80
#define FIFOC_TX_ALARM      0x84
#define FIFOC_TX_SR         0x88
#define FIFOC_TX_ISR        0x8c
#define FIFOC_TX_IMR        0x90
#define FIFOC_TX_COUNT      0x94
#define FIFOC_TX_POINTER    0x98
#define FIFOC_TX_SIZE       0x9c
#define FIFOC_TX_DATA       0xbc
#define FIFOC_RX_CMD        0xc0
#define FIFOC_RX_ALARM      0xc4
#define FIFOC_RX_SR         0xc8
#define FIFOC_RX_ISR        0xcc
#define FIFOC_RX_IMR        0xd0
#define FIFOC_RX_COUNT      0xd4
#define FIFOC_RX_POINTER    0xd8
#define FIFOC_RX_SIZE       0xdc
#define FIFOC_RX_DATA       0xfc

/* PSC UART register fields */

#define PSC_UART_SICR_UART_MODE     0x00000000

/* MR1 - mode register 1 */

/* data bits */

#define PSC_UART_MR1_BITS_CHAR_5    0x00
#define PSC_UART_MR1_BITS_CHAR_6    0x01
#define PSC_UART_MR1_BITS_CHAR_7    0x02
#define PSC_UART_MR1_BITS_CHAR_8    0x03

/* data parity */

#define PSC_UART_MR1_PARITY_NO      0x10
#define PSC_UART_MR1_PARITY_EVEN    0x00
#define PSC_UART_MR1_PARITY_ODD     0x04
#define PSC_UART_MR1_PARITY_MULTI   0X18

/* Rx RTS control */

#define PSC_UART_MR1_RX_RTS         0x80

/* MR2 - mode register 2 */

/* stop bits */

#define PSC_UART_MR2_STOP_BITS_1    0x07
#define PSC_UART_MR2_STOP_BITS_2    0x0F

/* Tx CTS control */

#define PSC_UART_MR2_TX_CTS         0x10

/* Tx RTS control */

#define PSC_UART_MR2_TX_RTS         0x20

/* SR - status register */

#define PSC_UART_SR_ERR             0x0040
#define PSC_UART_SR_CDE             0x0080
#define PSC_UART_SR_RXRDY           0x0100
#define PSC_UART_SR_FFULL           0x0200
#define PSC_UART_SR_TXRDY           0x0400
#define PSC_UART_SR_TXEMP           0x0800
#define PSC_UART_SR_OE              0x1000
#define PSC_UART_SR_PE              0x2000
#define PSC_UART_SR_FE              0x4000
#define PSC_UART_SR_RB              0x8000

/* CSR - clock select register */

#define PSC_UART_CSR_TIMER_TX       0x0D00
#define PSC_UART_CSR_TIMER_RX       0xD000

/*
 * Note:
 *   MPC5200B prescaler = 32 or 4
 *   MPC512x prescaler = 16 or 10
 */

#define PSC_UART_CLOCK_PRESCALE_MPC5200     32
#define PSC_UART_CLOCK_PRESCALE_MPC512X     16

/* CR - command register */

/* Rx control */

#define PSC_UART_CR_RX_ENABLE       0x01
#define PSC_UART_CR_RX_DISABLE      0x02

/* Tx control */

#define PSC_UART_CR_TX_ENABLE       0x04
#define PSC_UART_CR_TX_DISABLE      0x08

/* misc control */

#define PSC_UART_CR_RESET_MODE_PTR  0x10
#define PSC_UART_CR_RESET_RX        0x20
#define PSC_UART_CR_RESET_TX        0x30
#define PSC_UART_CR_RESET_ERR       0x40
#define PSC_UART_CR_RESET_BRK       0x50

/* ACR - auxiliary control register */

#define PSC_UART_ACR_IEC            0x01

/* IMR - interrupt mask register */

#define PSC_UART_IMR_TXRDY          0x0100      /* transmitter ready */
#define PSC_UART_IMR_RXRDY          0x0200      /* receiver ready */
#define PSC_UART_IMR_DB             0x0400      /* delta break */
#define PSC_UART_IMR_COS            0x8000      /* change of CTS state */

/* ISR - interrupt status register */

#define PSC_UART_ISR_TXRDY          0x0100      /* transmitter ready */
#define PSC_UART_ISR_RXRDY          0x0200      /* receiver ready */
#define PSC_UART_ISR_DB             0x0400      /* delta break */
#define PSC_UART_ISR_COS            0x8000      /* change of CTS state */


/* FIFOC register fields */

/* FIFOC command */

#define FIFOC_CMD_RESET_SLICE	    0x80
#define FIFOC_CMD_ENABLE_SLICE	    0x01

/* FIFOC size & alarm */

#define FIFOC_ALARM_LVL             1
#define FIFOC_SIZE                  16
#define FIFOC_SIZE_MASK             0x07FF

/* FIFOC status */

#define FIFOC_STATUS_MEM            0x20
#define FIFOC_STATUS_OR             0x10
#define FIFOC_STATUS_UR             0x08
#define FIFOC_STATUS_ALARM          0x04
#define FIFOC_STATUS_FULL           0x02
#define FIFOC_STATUS_EMPTY          0x01

/* FIFOC count */

#define FIFOC_COUNT_MASK            0x1FFF

/* MPC5xxx PSC SIO driver control block */

typedef struct m5xxxPscSioChan
    {
    SIO_DRV_FUNCS *     pDrvFuncs;
    STATUS              (*getTxChar)();
    STATUS              (*putRcvChar)();
    void *              getTxArg;
    void *              putRcvArg;
    VXB_DEVICE_ID       pDev;
    int                 channelNo;
    UINT32              clkRate;
    UINT32              baudRate;
    UINT32              options;
    UINT32              sioMode;
    UINT8               acrCopy;
    UINT16              imrCopy;
    UINT32              fifoMode;       /* for MPC512x specific fifo mode */
    UINT32              pscNum;
    VXB_DEVICE_ID       fifocDev;
    FUNCPTR             fifoAlloc;
    void *              regBase;
    void *              handle;
    spinlockIsr_t       spinlockIsr;    /* ISR callable spinlock */
    } M5XXXPSC_SIO_CHAN;

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCvxbM5xxxPscSioh */
