/* vcbUccSio.h - Freescale UCC UART driver header file */

/*
 * Copyright (c) 2009 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,24sep09,x_z  written based on vxbTemplateSio.h 01a.
*/

#ifndef __INCvxbUccSioh
#define __INCvxbUccSioh

#ifdef __cplusplus
extern "C" {
#endif

#include <sioLib.h>
#include <spinLockLib.h>
#include <wdLib.h>

/* defines */

/* UCC Memory Map */

#define UCC_SIO_GUMR_L  0x00 /* General UCC Mode Register Low Order (32bit)   */
#define UCC_SIO_GUMR_H  0x04 /* General UCC Mode Register High Order (32bit)  */
#define UCC_SIO_UPSMR   0x08 /* Protocol Specific Mode Register (16bit)       */
#define UCC_SIO_UTODR   0x0C /* Transmit On Demand Register (16bit)           */
#define UCC_SIO_UDSR    0x0E /* UCC Data Synchronization Register (16bit)     */
#define UCC_SIO_UCCE    0x10 /* UCC Event Register (16bit)                    */
#define UCC_SIO_UCCM    0x14 /* UCC Mask Register (16bit)                     */
#define UCC_SIO_UCCS    0x17 /* UCC Status Register (8bit) - Read Only        */
#define UCC_SIO_UTPT    0x3C /* UCC Transmit Polling Timer (16bit)            */
#define UCC_SIO_GUEMR   0x90 /* General UCC Extended Mode Register (8bit)     */

/* UCC_SIO_GUMR_L bit definitions */

#define UCC_SIO_GUMR_L_TDCR_MASK    0x00030000 /* Tx oversampling rate        */
#define UCC_SIO_GUMR_L_TDCR_1       0x00000000 /* 00 - 1x clock mode          */
#define UCC_SIO_GUMR_L_TDCR_8       0x00010000 /* 01 - 8x clock mode          */
#define UCC_SIO_GUMR_L_TDCR_16      0x00020000 /* 10 - 16x clock mode         */
#define UCC_SIO_GUMR_L_TDCR_32      0x00030000 /* 11 - 32x clock mode         */

#define UCC_SIO_GUMR_L_RDCR_MASK    0x0000C000 /* Rx oversampling rate        */
#define UCC_SIO_GUMR_L_RDCR_1       0x00000000 /* 00 - 1x clock mode          */
#define UCC_SIO_GUMR_L_RDCR_8       0x00004000 /* 01 - 8x clock mode          */
#define UCC_SIO_GUMR_L_RDCR_16      0x00008000 /* 10 - 16x clock mode         */
#define UCC_SIO_GUMR_L_RDCR_32      0x0000C000 /* 11 - 32x clock mode         */

#define UCC_SIO_GUMR_L_DIAG_MASK    0x000000C0 /* Diagnostic mode             */
#define UCC_SIO_GUMR_L_DIAG_NORM    0x00000000 /* 00 - Normal operation       */
#define UCC_SIO_GUMR_L_DIAG_LOOP    0x00000040 /* 01 - Local loopback mode    */
#define UCC_SIO_GUMR_L_DIAG_ECHO    0x00000080 /* 10 - Automatic echo mode    */
#define UCC_SIO_GUMR_L_DIAG_LE      0x000000C0 /* 11 - Loopback & echo mode   */

#define UCC_SIO_GUMR_L_ENR          0x00000020 /* Enable receive              */
#define UCC_SIO_GUMR_L_ENT          0x00000010 /* Enable transmit             */

#define UCC_SIO_GUMR_L_MODE_MASK    0x0000000F /* Channel protocol mode       */
#define UCC_SIO_GUMR_L_MODE_UART    0x00000004 /* UART                        */

/* UCC_SIO_GUMR_H bit definitions */

#define UCC_SIO_GUMR_H_CDP          0x00000400 /* CD pulse                    */
#define UCC_SIO_GUMR_H_CTSP         0x00000200 /* CTS pulse                   */
#define UCC_SIO_GUMR_H_CDS          0x00000100 /* CD sampling                 */
#define UCC_SIO_GUMR_H_CTSS         0x00000080 /* CTS sampling                */
#define UCC_SIO_GUMR_H_TFL          0x00000040 /* Transmit FIFO length        */
#define UCC_SIO_GUMR_H_RFW          0x00000020 /* Rx FIFO width               */

/* UCC_SIO_UPSMR bit definitions */

#define UCC_SIO_UPSMR_FLC           0x8000 /* Flow control                    */
#define UCC_SIO_UPSMR_SL            0x4000 /* Stop length                     */

#define UCC_SIO_UPSMR_CL_MASK       0x3000 /* Character length                */
#define UCC_SIO_UPSMR_CL_5          0x0000 /* 00 - 5 data bits                */
#define UCC_SIO_UPSMR_CL_6          0x1000 /* 01 - 6 data bits                */
#define UCC_SIO_UPSMR_CL_7          0x2000 /* 10 - 7 data bits                */
#define UCC_SIO_UPSMR_CL_8          0x3000 /* 11 - 8 data bits                */

#define UCC_SIO_UPSMR_UM_MASK       0x0C00 /* UART mode                       */
#define UCC_SIO_UPSMR_UM_NORMAL     0x0000 /* Normal UART operation           */
#define UCC_SIO_UPSMR_UM_MAN_MULTI  0x0400 /* Manual multidrop mode           */
#define UCC_SIO_UPSMR_UM_AUTO_MULTI 0x0C00 /* Automatic multidrop mode        */

#define UCC_SIO_UPSMR_FRZ           0x0200 /* Freeze transmission             */
#define UCC_SIO_UPSMR_RZS           0x0100 /* Receive zero stop bits          */
#define UCC_SIO_UPSMR_SYN           0x0080 /* Synchronous mode                */
#define UCC_SIO_UPSMR_DRT           0x0040 /* Disable Rx while transmitting   */
#define UCC_SIO_UPSMR_PEN           0x0010 /* Parity enable                   */

#define UCC_SIO_UPSMR_RPM_MASK      0x000C /* Receiver parity mode            */
#define UCC_SIO_UPSMR_RPM_ODD       0x0000 /* Odd parity.                     */
#define UCC_SIO_UPSMR_RPM_LOW       0x0004 /* Low parity                      */
#define UCC_SIO_UPSMR_RPM_EVEN      0x0008 /* Even parity                     */
#define UCC_SIO_UPSMR_RPM_HIGH      0x000C /* High parity                     */

#define UCC_SIO_UPSMR_TPM_MASK      0x0003 /* Transmitter parity mode         */
#define UCC_SIO_UPSMR_TPM_ODD       0x0000 /* Odd parity.                     */
#define UCC_SIO_UPSMR_TPM_LOW       0x0001 /* Low parity                      */
#define UCC_SIO_UPSMR_TPM_EVEN      0x0002 /* Even parity                     */
#define UCC_SIO_UPSMR_TPM_HIGH      0x0003 /* High parity                     */

/* UCC_SIO_UTODR bit definitions */

#define UCC_SIO_UTODR_TOD           0x8000 /* Transmit on demand              */

/* UCC_SIO_UDSR bit definitions */

#define UCC_SIO_UDSR_FSB_MASK       0x7800 /* Fractional stop bits            */

/* UCC_SIO_UCCS bit definitions */

#define UCC_SIO_UCCS_ID             0x0100 /* Idle status                     */

/* UCC_SIO_GUEMR bit definitions */

#define UCC_SIO_GUEMR_RESV          0x10 /* Must be set                       */

#define UCC_SIO_GUEMR_URMODE_MASK   0x02 /* UCC Rx Mode                       */
#define UCC_SIO_GUEMR_URMODE_SLOW   0x00 /* 0 - slow                          */
#define UCC_SIO_GUEMR_URMODE_FAST   0x02 /* 1 - fast                          */

#define UCC_SIO_GUEMR_UTMODE_MASK   0x01 /* UCC Tx Mode                       */
#define UCC_SIO_GUEMR_UTMODE_SLOW   0x00 /* 0 - slow                          */
#define UCC_SIO_GUEMR_UTMODE_FAST   0x01 /* 1 - fast                          */

/* Parameter RAM Map */

#define UCC_SIO_PRAM_RBASE          0x00 /* RX BD base address (16bit)        */
#define UCC_SIO_PRAM_TBASE          0x02 /* TX BD base address (16bit)        */
#define UCC_SIO_PRAM_RBMR           0x04 /* RX bus mode (8bit)                */
#define UCC_SIO_PRAM_TBMR           0x05 /* TX bus mode (8bit)                */
#define UCC_SIO_PRAM_MRBLR          0x06 /* Max. Rx buffer length (16bit)     */
#define UCC_SIO_PRAM_RSTATE         0x08 /* Rx interna lstate (32bit)         */
#define UCC_SIO_PRAM_RPTR           0x0c /* Rx buffer pointer (32bit)         */
#define UCC_SIO_PRAM_RBPTR          0x10 /* Rx BD Pointer (16bit)             */
#define UCC_SIO_PRAM_RCOUNT         0x12 /* Rx internal byte count (16bit)    */
#define UCC_SIO_PRAM_RTEMP          0x14 /* Rx temp (32bit)                   */
#define UCC_SIO_PRAM_TSTATE         0x18 /* Tx interna lstate (32bit)         */
#define UCC_SIO_PRAM_TPTR           0x1c /* Tx buffer pointer (32bit)         */
#define UCC_SIO_PRAM_TBPTR          0x20 /* Tx BD Pointer (16bit)             */
#define UCC_SIO_PRAM_TCOUNT         0x22 /* Tx internal byte count (16bit)    */
#define UCC_SIO_PRAM_TTEMP          0x24 /* Tx temp (32bit)                   */
#define UCC_SIO_PRAM_RCRC           0x28 /* Temp Rx CRC (32bit)               */
#define UCC_SIO_PRAM_TCRC           0x2C /* Temp Tx CRC (32bit)               */

#define UCC_SIO_PRAM_MAX_IDL        0x38 /* Max. idle characters (16bit)      */
#define UCC_SIO_PRAM_IDLC           0x3A /* Temporary idle counter (16bit)    */
#define UCC_SIO_PRAM_BRKCR          0x3C /* Tx Break count  (16bit)           */
#define UCC_SIO_PRAM_PAREC          0x3E /* Rx parity error counter (16bit)   */
#define UCC_SIO_PRAM_FRMEC          0x40 /* Rx framing error counter (16bit)  */
#define UCC_SIO_PRAM_NOSEC          0x42 /* Rx noise error counter (16bit)    */
#define UCC_SIO_PRAM_BRKEC          0x44 /* Rx break counter (16bit)          */
#define UCC_SIO_PRAM_BRKLN          0x46 /* Last Rx break length (16bit)      */
#define UCC_SIO_PRAM_UADDR1         0x48 /* UART address character 1 (16bit)  */
#define UCC_SIO_PRAM_UADDR2         0x4A /* UART address character 2 (16bit)  */
#define UCC_SIO_PRAM_TOSEQ          0x4E /* Transmit out-of-sequence (16bit)  */
#define UCC_SIO_PRAM_CCR1           0x50 /* Control character 1 (16bit)       */
#define UCC_SIO_PRAM_CCR2           0x52 /* Control character 1 (16bit)       */
#define UCC_SIO_PRAM_CCR3           0x54 /* Control character 1 (16bit)       */
#define UCC_SIO_PRAM_CCR4           0x56 /* Control character 1 (16bit)       */
#define UCC_SIO_PRAM_CCR5           0x58 /* Control character 1 (16bit)       */
#define UCC_SIO_PRAM_CCR6           0x5A /* Control character 1 (16bit)       */
#define UCC_SIO_PRAM_CCR7           0x5C /* Control character 1 (16bit)       */
#define UCC_SIO_PRAM_CCR8           0x5E /* Control character 1 (16bit)       */
#define UCC_SIO_PRAM_RCCM           0x60 /* Rx control character mask (16bit) */
#define UCC_SIO_PRAM_RCCR           0x62 /* Rx control character (16bit)      */
#define UCC_SIO_PRAM_RLBC           0x64 /* Rx last break character (16bit)   */

/* UCC_SIO_PRAM_RBMR bit definitions */

#define UCC_SIO_BMR_GBL             0x20 /* Global (snoop enable)             */
#define UCC_SIO_BMR_BO_BE           0x10 /* Big-endian byte ordering          */
#define UCC_SIO_BMR_CETM            0x04 /* QUICC Engine Transaction Mark     */
#define UCC_SIO_BMR_DTB             0x02 /* what bus the data is located on   */
#define UCC_SIO_BMR_BDB             0x01 /* what bus the BDs is located on    */

/* UCC_SIO_PRAM_CCRn bit definitions */

#define UCC_SIO_PRAM_CCR_E          0x8000 /* End of table (valid. flag)      */
#define UCC_SIO_PRAM_CCR_R          0x4000 /* Reject character                */

/* UCC_SIO_PRAM_RCCM_RESV bit definitions */

#define UCC_SIO_PRAM_RCCM_RESV      (UCC_SIO_PRAM_CCR_E | UCC_SIO_PRAM_CCR_R)

/* UCC event definitions */

#define UCC_SIO_EVENT_AB            0x0200 /* Autobaud.                       */
#define UCC_SIO_EVENT_IDLE          0x0100 /* Idle sequence status changed    */
#define UCC_SIO_EVENT_GRA           0x0080 /* Graceful stop complete          */
#define UCC_SIO_EVENT_BRKE          0x0040 /* Break end                       */
#define UCC_SIO_EVENT_BRKS          0x0020 /* Break start.                    */
#define UCC_SIO_EVENT_CCR           0x0008 /* Control character received      */
                                           /* rejected                        */
#define UCC_SIO_EVENT_BSY           0x0004 /* Busy                            */
#define UCC_SIO_EVENT_TX            0x0002 /* Tx event                        */
#define UCC_SIO_EVENT_RX            0x0001 /* Rx event                        */

/* Rx  BD status definitions */

#define UCC_SIO_RBD_E               0x8000 /* Empty                           */
#define UCC_SIO_RBD_W               0x2000 /* Wrap                            */
#define UCC_SIO_RBD_I               0x1000 /* Interrupt                       */
#define UCC_SIO_RBD_C               0x0800 /* Control character received      */
#define UCC_SIO_RBD_A               0x0400 /* Address received                */
#define UCC_SIO_RBD_CM              0x0200 /* Continous mode                  */
#define UCC_SIO_RBD_ID              0x0100 /* Buffer closed on too many idles */
#define UCC_SIO_RBD_AM              0x0080 /* Address match                   */
#define UCC_SIO_RBD_BR              0x0020 /* Break received                  */
#define UCC_SIO_RBD_FR              0x0010 /* Framing error                   */
#define UCC_SIO_RBD_PR              0x0008 /* Parity error                    */
#define UCC_SIO_RBD_OV              0x0002 /* Overrun                         */
#define UCC_SIO_RBD_CD              0x0001 /* Carrier detect lost             */

#define UCC_SIO_RBD_ERR             (UCC_SIO_RBD_FR | UCC_SIO_RBD_PR)

/* Tx  BD status definitions */

#define UCC_SIO_TBD_R               0x8000 /* Ready                           */
#define UCC_SIO_TBD_W               0x2000 /* Wrap                            */
#define UCC_SIO_TBD_I               0x1000 /* Interrupt                       */
#define UCC_SIO_TBD_CR              0x0800 /* Clear-to-send report            */
#define UCC_SIO_TBD_A               0x0400 /* Address.                        */
#define UCC_SIO_TBD_CM              0x0200 /* Continous mode                  */
#define UCC_SIO_TBD_P               0x0100 /* Transmit preamble               */
#define UCC_SIO_TBD_NS              0x0080 /* No stop bitsent                 */

/* clock configuration */

#define UCC_SIO_BRGCLK_RATIO        2   /* BRGCLK : QE Frequency = 1:2        */
#define UCC_SIO_CLK_RATIO           16  /* oversampling rate                  */

/* size and align for MURAM malloc */

#define UCC_SIO_PRAM_SIZE           256
#define UCC_SIO_PRAM_ALIGN          64

#define UCC_SIO_BD_ALIGN            8

/*
 * The QUICC Engine module closes the current buffer, generates a maskable
 * interrupt, and starts receiving data into the next buffer after a full
 * receive buffer is detected. So the Rx buffer length must be 1 for console.
 */

#define UCC_SIO_RBUF_LEN            1
#define UCC_SIO_TBUF_LEN            8

/* BD numbers */

#define UCC_SIO_RBD_NUM             16
#define UCC_SIO_TBD_NUM             4

/* UCC register low level access routines */

#define UCC_SIO_REG_READ_1(pChan, addr) \
        vxbRead8(pChan->uccHandle, (UINT8 *) ((pChan)->uccBase + addr))

#define UCC_SIO_REG_WRITE_1(pChan, addr, data) \
        vxbWrite8(pChan->uccHandle, (UINT8 *) ((pChan)->uccBase + addr), data)

#define UCC_SIO_REG_SETBIT_1(pChan, addr, val)      \
        UCC_SIO_REG_WRITE_1(pChan, addr,            \
                            UCC_SIO_REG_READ_1(pChan, addr) | (val))

#define UCC_SIO_REG_CLRBIT_1(pChan, addr, val)     \
        UCC_SIO_REG_WRITE_1(pChan, addr,            \
                            UCC_SIO_REG_READ_1(pChan, addr) & ~(val))

#define UCC_SIO_REG_READ_2(pChan, addr) \
        vxbRead16(pChan->uccHandle, (UINT16 *) ((pChan)->uccBase + addr))

#define UCC_SIO_REG_WRITE_2(pChan, addr, data) \
        vxbWrite16(pChan->uccHandle, (UINT16 *) ((pChan)->uccBase + addr), data)

#define UCC_SIO_REG_SETBIT_2(pChan, addr, val)      \
        UCC_SIO_REG_WRITE_2(pChan, addr,            \
                            UCC_SIO_REG_READ_2(pChan, addr) | (val))

#define UCC_SIO_REG_CLRBIT_2(pChan, addr, val)     \
        UCC_SIO_REG_WRITE_2(pChan, addr,            \
                            UCC_SIO_REG_READ_2(pChan, addr) & ~(val))

#define UCC_SIO_REG_READ_4(pChan, addr) \
        vxbRead32(pChan->uccHandle, (UINT32 *) ((pChan)->uccBase + addr))

#define UCC_SIO_REG_WRITE_4(pChan, addr, data) \
        vxbWrite32(pChan->uccHandle, (UINT32 *) ((pChan)->uccBase + addr), data)

#define UCC_SIO_REG_SETBIT_4(pChan, addr, val)      \
        UCC_SIO_REG_WRITE_4(pChan, addr,            \
                            UCC_SIO_REG_READ_4(pChan, addr) | (val))

#define UCC_SIO_REG_CLRBIT_4(pChan, addr, val)     \
        UCC_SIO_REG_WRITE_4(pChan, addr,            \
                            UCC_SIO_REG_READ_4(pChan, addr) & ~(val))

/* QUICC Engine register low level access routines */

#define UCC_SIO_QE_REG_READ_4(pChan, addr) \
        vxbRead32(pChan->qeHandle, (UINT32 *) ((pChan)->qeBase + addr))

#define UCC_SIO_QE_REG_WRITE_4(pChan, addr, data) \
        vxbWrite32(pChan->qeHandle, (UINT32 *) ((pChan)->qeBase + addr), data)

#define UCC_SIO_QE_REG_SETBIT_4(pChan, addr, val)      \
        UCC_SIO_QE_REG_WRITE_4(pChan, addr,            \
                               UCC_SIO_QE_REG_READ_4(pChan, addr) | (val))

#define UCC_SIO_QE_REG_CLRBIT_4(pChan, addr, val)     \
        UCC_SIO_QE_REG_WRITE_4(pChan, addr,            \
                               UCC_SIO_QE_REG_READ_4(pChan, addr) & ~(val))

/* Parameter RAM low level access routines */

#define UCC_SIO_PRAM_READ_1(pChan, addr) \
        *(volatile UINT8 *) ((pChan)->paramRamBase + addr)

#define UCC_SIO_PRAM_WRITE_1(pChan, addr, data) \
        *(volatile UINT8 *) ((pChan)->paramRamBase + addr) = (UINT8) (data)

#define UCC_SIO_PRAM_SETBIT_1(pChan, addr, val)      \
        UCC_SIO_PRAM_WRITE_1(pChan, addr,            \
                            UCC_SIO_PRAM_READ_1(pChan, addr) | (val))

#define UCC_SIO_PRAM_CLRBIT_1(pChan, addr, val)     \
        UCC_SIO_PRAM_WRITE_1(pChan, addr,            \
                            UCC_SIO_PRAM_READ_1(pChan, addr) & ~(val))

#define UCC_SIO_PRAM_READ_2(pChan, addr) \
        *(volatile UINT16 *) ((pChan)->paramRamBase + addr)

#define UCC_SIO_PRAM_WRITE_2(pChan, addr, data) \
        *(volatile UINT16 *) ((pChan)->paramRamBase + addr) = (UINT16) (data)

#define UCC_SIO_PRAM_SETBIT_2(pChan, addr, val)      \
        UCC_SIO_PRAM_WRITE_2(pChan, addr,            \
                            UCC_SIO_PRAM_READ_2(pChan, addr) | (val))

#define UCC_SIO_PRAM_CLRBIT_2(pChan, addr, val)     \
        UCC_SIO_PRAM_WRITE_2(pChan, addr,            \
                            UCC_SIO_PRAM_READ_2(pChan, addr) & ~(val))

#define UCC_SIO_PRAM_READ_4(pChan, addr) \
        *(volatile UINT32 *) ((pChan)->paramRamBase + addr)

#define UCC_SIO_PRAM_WRITE_4(pChan, addr, data) \
        *(volatile UINT32 *) ((pChan)->paramRamBase + addr) = (UINT32) (data)

#define UCC_SIO_PRAM_SETBIT_4(pChan, addr, val)      \
        UCC_SIO_PRAM_WRITE_4(pChan, addr,            \
                            UCC_SIO_PRAM_READ_4(pChan, addr) | (val))

#define UCC_SIO_PRAM_CLRBIT_4(pChan, addr, val)     \
        UCC_SIO_PRAM_WRITE_4(pChan, addr,            \
                            UCC_SIO_PRAM_READ_4(pChan, addr) & ~(val))

/* typedefs */

typedef struct
    {
    volatile UINT16 status;
    volatile UINT16 bufLen;
    volatile char * bufPtr;
    } UCC_SIO_BD_INFO;

typedef struct uccSioChan
    {
    /* must be first */

    SIO_DRV_FUNCS *     pDrvFuncs;      /* driver functions                   */

    /* chanle info */

    UINT32              channelMode;    /* such as INT, POLL modes            */
    int                 baudRate;       /* the current baud rate              */
    int                 channelNo;      /* channel number                     */
    VXB_DEVICE_ID       pDev;           /* bus subsystem: device ID           */
    VXB_DEVICE_ID       pQeDev;         /* QUICC Engine device ID             */

    /* configuration info */

    UINT32              uccNum;         /* UCC number                         */
    UINT32              rxBrgNum;       /* Rx BRG number                      */
    UINT32              txBrgNum;       /* Tx BRG number                      */
    UINT32              brgClkrate;     /* BRGCLK                             */

    /* device private info */

    UINT32              uccBase;        /* UCC Register base address          */
    void *              uccHandle;      /* access handle for uccBase          */
    UINT32              qeBase;         /* QUICC Engine Register base address */
    void *              qeHandle;       /* access handle for qeBase           */
    char *              paramRamBase;   /* Parameter RAM base address         */
    UCC_SIO_BD_INFO *   rxBdBase;       /* Rx BD table base address           */
    UCC_SIO_BD_INFO *   txBdBase;       /* Tx BD table base address           */
    UINT32              rxBdIndex;      /* Rx BD table index                  */
    UINT32              txBdIndex;      /* Rx BD table index                  */
    ISR_DEFER_QUEUE_ID  queueId;        /* deferral queue ID                  */
    ISR_DEFER_JOB       rxDeferHandle;  /* Rx routine for deferral job        */
    ISR_DEFER_JOB       txDeferHandle;  /* Tx routine for deferral job        */

    /* QUICC Engine routtines */

    STATUS              (*qeCmdFunc) (VXB_DEVICE_ID, VXB_DEVICE_ID,void *);
    STATUS              (*qeMuAllocFunc) (VXB_DEVICE_ID, VXB_DEVICE_ID,void *);
    STATUS              (*qeMuFreeFunc) (VXB_DEVICE_ID, VXB_DEVICE_ID,void *);
    STATUS              (*qeMuOffsetGetFunc) (VXB_DEVICE_ID, UINT32);

    /* callbacks */

    STATUS              (*getTxChar) ();  /* installed Tx callback routine    */
    STATUS              (*putRcvChar) (); /* installed Rx callback routine    */
    void *              getTxArg;         /* argument to Tx callback routine  */
    void *              putRcvArg;        /* argument to Rx callback routine  */

    /* need for SMP and ISR deferral model */

    spinlockIsr_t       spinlockIsr;      /* ISR-callable spinlock */

    /*
     * Each device may have multiple channels.  To handle this,
     * we keep a pointer to a pChan in pDev->pDrvCtrl, and maintain
     * a linked list of channels per controller.
     */

    struct uccSioChan * pNext;            /* next channel for this device     */
    } UCC_SIO_CHAN;

#ifdef __cplusplus
}
#endif

#endif  /* __INCvxbUccSioh */
