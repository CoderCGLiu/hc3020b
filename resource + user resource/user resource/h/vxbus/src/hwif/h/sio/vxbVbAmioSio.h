/* vbVbAmioSio.h - Application Multiplexed I/O (AMIO) header file */

/*
 * Copyright (c) 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,02aug11,c_t  written
*/

#ifndef __INCvxbVbAmioSioh
#define __INCvxbVbAmioSioh

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _ASMLANGUAGE

#define VXB_AMIO_SIO_BUFLEN 1024

typedef  struct  vxbVbAmioChan
    {
    /* always goes first */

    SIO_DRV_FUNCS *    pDrvFuncs;       /* driver functions */

    /* callbacks */

    STATUS             (*getTxChar) (); /* pointer to xmitr function */
    STATUS             (*putRcvChar) ();/* pointer to rcvr function */

    void *             getTxArg;
    void *             putRcvArg;

    intrDeviceChannelBuffer *pIdc;      /* IDC pointer */
    VBI_PDC_HANDLE          pdcHandle;  /* PDC handle */
    
    UINT16             channelMode;     /* such as INT, POLL modes */
    int                channelNo;       /* channel number */

    struct vxbVbAmioChan * pNext;       /* next channel for this device */
    } VXB_VBAMIO_CHAN;

#endif  /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif
 
#endif /* __INCvxbVbAmioSioh */

