/* vxbColdFireSio.h - Motorola ColdFire internal UART header file */

/*
 * Copyright (c) 2006-2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01e,17apr08,h_k  added handle field.
01d,13mar08,rec  move header files to hwif
01c,12dec07,rec  add support for generic coldfire UART
01b,06jun07,h_k  renamed coldFireSio.
01a,12apr06,pdg	 created, based on target/h/drv/coldFireSio.h.
*/

/*
This file contains constants and defines for the UART contained in several
of the Motorola ColdFire ports.
*/

#ifndef __INCvxbColdFireSioh
#define __INCvxbColdFireSioh

#ifdef __cplusplus
extern "C" {
#endif

#ifndef	_ASMLANGUAGE

#include <sioLib.h>
#include <hwif/sio/mcfUart.h>

#define _D_(i)   COLDFIRESIO_ ## i

typedef struct coldFireChan
    {
    SIO_DRV_FUNCS  * pDrvFuncs;	/* Driver functions */
				/* CallBacks */
    STATUS   (*getTxChar)();
    STATUS   (*putRcvChar)();
    void *   getTxArg;
    void *   putRcvArg;

    UINT		clkRate;	/* system clock rate */
    UINT16		mode;		/* SIO_MODE */
    int			options;
    int			intEnable;
    UINT		baudRate;
    UCHAR		oprCopy;
    UCHAR		acrCopy;
    UCHAR		imrCopy;

    VXB_DEVICE_ID	pDev;		/* bus subsystem: device ID */
    void *		handle;		/* register access handle */
    int			channelNo;	/* channel number */
    /*
     * Each device may have multiple channels.  To handle this,
     * we keep a pointer to a pChan in pDev->pDrvCtrl, and maintain
     * a linked list of channels per controller.
     */
    struct coldFireChan * pNext;	/* next channel for this device */

    } COLDFIRE_SIO_CHAN;

#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbColdFireSioh */
