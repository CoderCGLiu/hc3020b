/* vxbK70Sio.h - K70 UART header file */

/*
 * Copyright (c) 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,24jun,mpc  written.
*/

#ifndef __INCvxbK70Sioh
#define __INCvxbK70Sioh

#ifdef __cplusplus
extern "C" {
#endif

#define K70_SIO_NAME "vxbK70Sio"

#define K70_UARTx_BDH           0x00    /* Baud Rate Registers: High */
#define K70_UARTx_BDH_SBR_WIDTH 5       /* SBR width */
#define K70_UARTx_BDH_SBR_MASK  ((1 << 5) - 1)          /* SBR MASK */

#define K70_UARTx_BDL           0x01    /* Baud Rate Registers: Low */

#define K70_UARTx_C1            0x02    /* UART Control Register 1 */
#define K70_UARTx_C1_PE         (1 << 1)        /* Parity Enable */
#define K70_UARTx_C1_PT         (1 << 0)        /* Parity Type */
#define K70_UARTx_C1_M          (1 << 4)        /* 9-bit or 8-bit Mode Select */

#define K70_UARTx_C2            0x03    /* UART Control Register 2 */
#define K70_UARTx_C2_TE         (1 << 3)        /* enable Tx */
#define K70_UARTx_C2_RE         (1 << 2)        /* enable Rx */
#define K70_UARTx_C2_RIE        (1 << 5)        /* Receiver Full Interrupt */
#define K70_UARTx_C2_TCIE       (1 << 6)        /* Tx Complete Interrupt */

#define K70_UARTx_C3            0x06    /* UART Control Register 3 */

#define K70_UARTx_C4            0x0a    /* UART Control Register 4 */
#define K70_UARTx_C4_BRFA_WIDTH	5       /* Baud Rate Fine Adjust width */
#define K70_UARTx_C4_BRFA_MSK	((1 << 5) - 1)  /* Baud Rate Fine Adjust mask */

#define K70_UARTx_C5            0x0b    /* UART Control Register 5 */

#define K70_UARTx_S1            0x04    /* UART Status Register 1 */
#define K70_UARTx_S1_TDRE       (1 << 7)        /* Transmit Empty Flag */
#define K70_UARTx_S1_RDRF       (1 << 5)        /* Receive Full Flag */

#define K70_UARTx_S2            0x05    /* UART Status Register 2 */

#define K70_UARTx_D             0x07    /* UART Data Register */

#define K70_UARTx_MODEM         0x0d    /* UART Modem Register */
#define K70_UARTx_MODEM_RXRTSE  (1 << 3)        /* Receiver RTS enable*/
#define K70_UARTx_MODEM_TXCTSE  (1 << 0)        /* Transmitter CTS enable*/

#define K70_UARTx_PFIFO         0x10    /* UART FIFO Parameters */
#define K70_UARTx_PFIFO_TXSIZE          (7 << 4)    /* UART TX FIFO SIZE Mask */
#define K70_UARTx_PFIFO_RXSIZE          (7 << 0)    /* UART RX FIFO SIZE Mask */
#define K70_UARTx_PFIFO_TXFE            (1 << 7)    /* UART TX FIFO Enable */
#define K70_UARTx_PFIFO_RXFE            (1 << 0)    /* UART RX FIFO Enable */

#define K70_UARTx_CFIFO         0x11    /* UART FIFO Control Register */
#define K70_UARTx_SFIFO         0x12    /* UART FIFO Status Register */
#define K70_UARTx_TWFIFO        0x13    /* UART FIFO Transmit Watermark */
#define K70_UARTx_TCFIFO        0x14    /* UART FIFO Transmit Count */
#define K70_UARTx_RWFIFO        0x15    /* UART FIFO Receive Watermark */
#define K70_UARTx_RCFIFO        0x16    /* UART FIFO Receive Count */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbK70Sioh */
