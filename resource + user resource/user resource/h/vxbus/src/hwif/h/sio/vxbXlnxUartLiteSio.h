/* vxbXlnxUartLiteSio.h - Xilinx ml507 Board Lite UART Driver Header */

/*
 * Copyright (c) 2009 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,08dec09,e_d  written.
*/

#ifndef __INCvxbXlnxUartLiteSioh
#define __INCvxbXlnxUartLiteSioh

#ifdef __cplusplus
extern "C" {
#endif

/* LITE UART register offsets */

#define LITE_UART_RX        0x00
#define LITE_UART_TX        0x04
#define LITE_UART_STAT      0x08
#define LITE_UART_CTRL      0x0c

/* STAT - status register */

#define LITE_UART_STAT_RX_VDATA    (1 << 0)
#define LITE_UART_STAT_RX_FULL     (1 << 1)
#define LITE_UART_STAT_TX_EMPTY    (1 << 2)
#define LITE_UART_STAT_TX_FULL     (1 << 3)
#define LITE_UART_STAT_INT_EN      (1 << 4)
#define LITE_UART_STAT_ERR_OVERRUN (1 << 5)
#define LITE_UART_STAT_ERR_FRAME   (1 << 6)
#define LITE_UART_STAT_ERR_PAR     (1 << 7)

/* CTRL - control register */

#define LITE_UART_CTRL_RST_TX      (1 << 0)
#define LITE_UART_CTRL_RST_RX      (1 << 1)
#define LITE_UART_CTRL_INT_EN      (1 << 4)

/* XLNX UART LITE SIO driver control block */

typedef struct xlnxUartLiteChan
    {
    SIO_DRV_FUNCS *     pDrvFuncs;
    STATUS              (*getTxChar)();
    STATUS              (*putRcvChar)();
    void *              getTxArg;
    void *              putRcvArg;
    VXB_DEVICE_ID       pDev;
    int                 channelNo;
    UINT32              baudRate;
    UINT32              options;
    UINT32              sioMode;
    void *              regBase;
    void *              handle;
    spinlockIsr_t       spinlockIsr;    /* ISR callable spinlock */
    } XLNXUART_LITE_CHAN;

/* defines */

#define XLNXUARTLITE_DEFAULT_OPTIONS     (CLOCAL | CREAD | CS8)
#define XLNXUARTLITE_DEFAULT_BAUD_RATE   9600

/* read & write low level access routines */

#define LITEUART_BAR(p)      ((XLNXUART_LITE_CHAN *)(p)->pDrvCtrl)->regBase
#define LITEUART_HANDLE(p)   ((XLNXUART_LITE_CHAN *)(p)->pDrvCtrl)->handle

#define LITEUART_READ_8(pDev, addr)          \
        vxbRead8(LITEUART_HANDLE(pDev), (UINT8 *)((char *)LITEUART_BAR(pDev) + addr))

#define LITEUART_READ_16(pDev, addr)         \
        vxbRead16(LITEUART_HANDLE(pDev), (UINT16 *)((char *)LITEUART_BAR(pDev) + addr))

#define LITEUART_READ_32(pDev, addr)         \
        vxbRead32(LITEUART_HANDLE(pDev), (UINT32 *)((char *)LITEUART_BAR(pDev) + addr))

#define LITEUART_WRITE_8(pDev, addr, data)   \
        vxbWrite8(LITEUART_HANDLE(pDev), (UINT8 *)((char *)LITEUART_BAR(pDev) + addr), data)

#define LITEUART_WRITE_16(pDev, addr, data)  \
        vxbWrite16(LITEUART_HANDLE(pDev), (UINT16 *)((char *)LITEUART_BAR(pDev) + addr), data)

#define LITEUART_WRITE_32(pDev, addr, data)  \
        vxbWrite32(LITEUART_HANDLE(pDev), (UINT32 *)((char *)LITEUART_BAR(pDev) + addr), data)    

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCvxbXlnxUartLiteSioh */

