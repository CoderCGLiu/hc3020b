/* vxbAt91sam9260Sio.h - AT91SAM9260 VxBus Serial header file */

/*
 * Copyright (c) 2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */ 

/*
modification history
--------------------
01a,25apr08,x_s  written.
*/

/*
DESCRIPTION
This module contains constants and defines for the AT91SAM9260 Sio.
*/

#ifndef __INCvxbAt91sam9260Sioh 
#define __INCvxbAt91sam9260Sioh

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _ASMLANGUAGE

#include <sioLib.h>
#include <spinLockLib.h>
#include <isrDeferLib.h>

/* Register offsets from base address */

#define AT91_US_CR      0x00        /* USART Control */
#define AT91_US_MR      0x04        /* USART Mode */
#define AT91_US_IER     0x08        /* USART Interrupt Enable */
#define AT91_US_IDR     0x0C        /* USART Interrupt Disable */
#define AT91_US_IMR     0x10        /* USART Interrupt Mask */
#define AT91_US_CSR     0x14        /* USART Channel Status */
#define AT91_US_RHR     0x18        /* USART Receiver Holding */
#define AT91_US_THR     0x1C        /* USART Transmitter Holding */

#define AT91_US_BRGR    0x20        /* USART Baud-Rate Generator */
#define AT91_US_RTOR    0x24        /* USART Receiver Time Out */
#define AT91_US_TTGR    0x28        /* USART Transmitter Time Guard */

#define AT91_US_RPR     0x100       /* USART Receiver Pointer */
#define AT91_US_RCR     0x104       /* USART Receiver Counter */
#define AT91_US_TPR     0x108       /* USART Transmitter Pointer */
#define AT91_US_TCR     0x10C       /* USART Trasnmitter Counter */
#define AT91_US_PTCR    0x120

/* bit definitions within US_CR register */

#define AT91_US_RSTRX   (1<<2)      /* reset receiver */
#define AT91_US_RSTTX   (1<<3)      /* reset transmitter */
#define AT91_US_RXEN    (1<<4)      /* enable received */
#define AT91_US_RXDIS   (1<<5)      /* disable receiver */
#define AT91_US_TXEN    (1<<6)      /* enable transmitter */
#define AT91_US_TXDIS   (1<<7)      /* disable transmitter */
#define AT91_US_RSTSTA  (1<<8)      /* reset status */
#define AT91_US_STTTO   (1<<11)     /* start timeout */

/* bit definitions within US_MR register */

#define AT91_US_NOPAR   (4<<9)      /* set no parity */
#define AT91_US_CHRL_8  (3<<6)      /* set 8-bit characters */


/* bit definitions within US_IER, IMR, ISR and CSR registers */

#define AT91_US_RXRDY   (1<<0)      /* Receiver Ready */
#define AT91_US_TXRDY   (1<<1)      /* Transmitter Ready */
#define AT91_US_RXBRK   (1<<2)      /* Receive Break */
#define AT91_US_ENDRX   (1<<3)      /* End of Rx Transfer */
#define AT91_US_ENDTX   (1<<4)      /* End of Tx Transfer */
#define AT91_US_OVRE    (1<<5)      /* Receiver Overrun */
#define AT91_US_FRAME   (1<<6)      /* Frame Error */
#define AT91_US_PARE    (1<<7)      /* Parity Error */
#define AT91_US_TIMEOUT (1<<8)      /* Receiver Timeout */
#define AT91_US_TXEMPTY (1<<9)      /* Transmitter empty */

/* -------- PDC_PTCR : (PDC Offset: 0x20) PDC Transfer Control Register ----- */

/* (PDC) Receiver Transfer Enable     */ 

#define AT91_US_PDC_RXTEN       ((unsigned int) 0x1 <<  0)

/* (PDC) Receiver Transfer Disable    */ 

#define AT91_US_PDC_RXTDIS      ((unsigned int) 0x1 <<  1)

/* (PDC) Transmitter Transfer Enable  */
#define AT91_US_PDC_TXTEN       ((unsigned int) 0x1 <<  8)

/* (PDC) Transmitter Transfer Disable */

#define AT91_US_PDC_TXTDIS      ((unsigned int) 0x1 <<  9)

#ifndef AT91_RXBUFF_SIZE
#   define AT91_RXBUFF_SIZE    32       /* Size of Rx buffers */
#endif /* AT91_RXBUFF_SIZE */
#ifndef AT91_TXBUFF_SIZE
#   define AT91_TXBUFF_SIZE    32       /* Size of Tx buffer */
#endif /* AT91_TXBUFF_SIZE */

typedef struct vxbAt91Chan
    {
    /* must be first */

    SIO_DRV_FUNCS * pDrvFuncs;          /* driver functions */

    /* callbacks */

    STATUS          (*getTxChar) ();    /* installed Tx callback routine */
    STATUS          (*putRcvChar) ();   /* installed Rx callback routine */
    void *          getTxArg;           /* argument to Tx callback routine */
    void *          putRcvArg;          /* argument to Rx callback routine */
    
    UINT32          channelMode;        /* such as INT, POLL modes */
    int             baudRate;           /* the current baud rate */
    UINT32          xtal;               /* UART clock frequency */  

    int             regIndex;           /* bus subsystem: reg base index */
    VXB_DEVICE_ID   pDev;               /* bus subsystem: device ID */
    int             channelNo;          /* channel number */

    UINT32          irq;                /* IRQ level */
    UINT32          intLevel;
    UINT32          clkRate;
    UINT32          divisor;
    UINT32          mode;
    UINT32          options;            /* hardware setup options */
    BOOL            usePdc;
    BOOL            isUsart;            /* DBGU is compatible with USART */

    int             buffInUse;          /* 0 => buff A, 1 => buff B */
    UINT8 *         rxBuffA;            /* Rx buffer */
    UINT8 *         rxBuffB;            /* Rx buffer */
    UINT8 *         txBuff;             /* Tx buffer */
    
    /* need for SMP and ISR deferral model */

    spinlockIsr_t   spinlockIsr;        /* ISR-callable spinlock */

    /*
     * Each device may have multiple channels.  To handle this,
     * we keep a pointer to a pChan in pDev->pDrvCtrl, and maintain
     * a linked list of channels per controller.
     */

    struct vxbAt91Chan * pNext;         /* next channel for this device */

    } VXB_AT91_CHAN;

#endif    /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif
 
#endif /* __INCvxbAt91sam9260Sioh */
