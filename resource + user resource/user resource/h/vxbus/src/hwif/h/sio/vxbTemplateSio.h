/* vxbTemplateSio.h - header file for template serial driver for VxBus */

/*
 * Copyright (c) 2008  Wind River Systems, Inc. All rights are reserved.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement.
 */

/*
modification history
--------------------
TODO -  Remove the template modification history and begin a new history
starting with version 01a and growing the history upward with
each revision.

01a,11sep08,h_k  written based on target/h/drv/sio/templateSio.h 01f.
*/

#ifndef __INCvxbTemplateSioh
#define __INCvxbTemplateSioh

#include <sioLib.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 * This driver for a generic SIO device only uses 8 bit reads/writes.
 * These are the register offsets for our fictitious device.
 *
 * Control Register	 	0x0	(write)
 * Status Register	 	0x0	(read)
 * Data register		0x1	(read/write)
 * Modem Status Register	0x2	(read)
 * Modem Control Register	0x2	(write)
 * Baud Rate Control Reg.       0x3	(read/write)
 *
 * The template driver uses 3 interrupt vectors.
 * Receive Interrupt		0x0
 * Transmit Interrupt		0x1
 * Error/Modem Interrupt	0x2
 */

#define TEMPLATE_CSR_ID		0x0	/* register offsets */
#define TEMPLATE_DATA_ID	0x1
#define TEMPLATE_MSR_ID		0x2
#define TEMPLATE_BAUD_ID	0x3

#define TEMPLATE_RXINT_ID	0x0	/* vector offsets */
#define TEMPLATE_TXINT_ID	0x1
#define TEMPLATE_ERRINT_ID	0x2

#ifdef __cplusplus
}
#endif

#endif  /* __INCvxbTemplateSioh */
