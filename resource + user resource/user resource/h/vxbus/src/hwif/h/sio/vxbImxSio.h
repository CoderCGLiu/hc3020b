/* vxbImxSio.h - i.MX UART for VxBus header file */

/*
 * Copyright (c) 2008-2009, 2011, 2014 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
19sep14,c_l  add ISR defer support. (VXW6-83427)
01d,05may11,hcl  add SDMA support.
01c,19mar09,syt  add UART_CTL2(UART Control Register 2)'s ATEN bit definition.
01b,08may08,l_z  code clean up.
01a,25mar08,l_z  written based on fsl_imx31lite/pimeCellSio.h.
*/

#ifndef __INCvxbImxSioh
#define __INCvxbImxSioh

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _ASMLANGUAGE

#include <sioLib.h>
#include <spinLockLib.h>
#include <isrDeferLib.h>
#include <hwif/util/vxbDmaLib.h>
#include <../src/hwif/h/dmaCtlr/vxbImxSdma.h>

/* Register description OF ARM AMBA UART */

/* Register offsets from base address */

#define UARTRX      0x00    /* UART data register (R/W) */
#define UARTTX      0x40    /* UART data register (R/W) */
#define UARTCTL1    0x80    /* Rx data status register (R/O) */
#define UARTCTL2    0x84    /* Rx data status register (R/O) */
#define UARTCTL3    0x88    /* Rx data status register (R/O) */
#define UARTCTL4    0x8c    /* Rx data status register (R/O) */
#define UARTFIFO    0x90    /* Clr modem status changed int (W/O) */
#define UARTSTS1    0x94    /* Clr modem status changed int (W/O) */
#define UARTSTS2    0x98    /* Clr modem status changed int (W/O) */
#define UARTESC     0x9c    /* Clr modem status changed int (W/O) */
#define UARTEST     0xa0    /* Clr modem status changed int (W/O) */
#define UARTBIR1    0xa4    /* Clr modem status changed int (W/O) */
#define UARTBIR2    0xa8    /* Clr modem status changed int (W/O) */
#define UARTBRC     0xac    /* Clr modem status changed int (W/O) */
#define UARTONEMS   0xb0    /* Clr modem status changed int (W/O) */
#define UARTTEST    0xb4    /* Clr modem status changed int (W/O) */

/* bit definitions within H_UBRLCR register */

#define PARITY_NONE    0x00        /* set no parity */
#define ONE_STOP       0x00        /* set one stop bit */
#define FIFO_ENABLE    0x10        /* Enable both FIFOs */
#define WORD_LEN_5     (0x00 << 5) /* Set UART word lengths */
#define WORD_LEN_6     (0x01 << 5)
#define WORD_LEN_7     (0x02 << 5)
#define WORD_LEN_8     (0x03 << 5)


/* bit definitions within UARTCR register */

#define UART_RTIE           0x40    /* Receive Timeout Int Enable */
#define UART_TIE            (8)     /* Transmit Int Enable */
#define UART_RIE            (1)     /* Receive Int Enable */

/* bit definitions within UARTCTL1 register */

#define UART_CTL1_RRDYTEN   (1 << 9)  /* receiver ready interrupt enable */
#define UART_CTL1_RXDMAEN   (1 << 8)  /* Receive Ready DMA Enable */
#define UART_CTL1_TXDMAEN   (1 << 3)  /* Transmitter Ready DMA Enable */
#define UART_CTL1_UARTEN    (1 << 0)  /* uart enable */

/* bit definitions within UARTCTL2 register */

#define UART_CTL2_IRTS    (1 << 14)   /* Ignore UARTx_RTS */
#define UART_CTL2_CTSC    (1 << 13)   /* UARTx_CTS control */
#define UART_CTL2_CTS     (1 << 12)   /* UARTx_CTS is low */
#define UART_CTL2_PREN    (1 << 8)    /* Parity enable */
#define UART_CTL2_PROD    (1 << 7)    /* Odd Parity enable */
#define UART_CTL2_STPB    (1 << 6)    /* 2 stop bit */
#define UART_CTL2_WS8     (1 << 5)    /* word size 8 */
#define UART_CTL2_ATEN    (1 << 3)    /* aging timer enable */
#define UART_CTL2_TXEN    (1 << 2)    /* tx enable */
#define UART_CTL2_RXEN    (1 << 1)    /* rx enable */
#define UART_CTL2_SRST    (1 << 0)    /* software reset */


/* bit definitions within UARTCTL3 register */

#define UART_CTL3_DSR          (1<<10)   /* RXD Muxed Input Selected */
#define UART_CTL3_DCD          (1<<9)    /* RXD Muxed Input Selected */
#define UART_CTL3_RXDMUXSEL    (1<<2)    /* RXD Muxed Input Selected */



/* bit definitions within UARTTEST register */

#define FLG_UTXFE            (0x01 << 6)    /* UART Tx FIFO Empty */
#define FLG_URXFE            (0x01 << 5)    /* UART Rx FIFO Empty */
#define FLG_UTXFF            (0x01 << 4)    /* UART Tx FIFO Full */
#define FLG_URXFF            (0x01 << 3)    /* UART Rx FIFO Full */

/* bit definitions within UARTIIR/ICR register */

#define UART_RX_RDY     (1 << 15)   /* Receive Int Status */
#define UART_TIS        0x8         /* Transmit Int Status */
#define UART_RIS        0x1         /* Receive Int Status */
#define UART_MIS        0x01        /* Modem Int Status */
#define UART_TFS        0x04        /* Modem Status Int Enable */

#define UART_DENOM 50000                      /* baudrate Denominator */
#define UART_RFDIV_MASK (~0x00000380)        /* divisor's mask */


typedef struct vxbImxChan
    {
    /* must be first */

    SIO_DRV_FUNCS * pDrvFuncs;    /* driver functions */

    /* callbacks */

    STATUS    (*getTxChar) ();    /* installed Tx callback routine */
    STATUS    (*putRcvChar) ();   /* installed Rx callback routine */
    void *    getTxArg;           /* argument to Tx callback routine */
    void *    putRcvArg;          /* argument to Rx callback routine */

    UINT32    channelMode;        /* such as INT, POLL modes */
    int       baudRate;           /* the current baud rate */
    UINT32    xtal;               /* UART clock frequency */

    int        regIndex;          /* bus subsystem: reg base index */
    VXB_DEVICE_ID pDev;           /* bus subsystem: device ID */
    int        channelNo;         /* channel number */

    /* need for SMP and ISR deferral model */

    spinlockIsr_t spinlockIsr;    /* ISR-callable spinlock */

    BOOL                spinlockReady;
    BOOL                txDefer;    /* TX interrupt is currently deferred */
    BOOL                rxDefer;    /* RX interrupt is currently deferred */
    ISR_DEFER_QUEUE_ID  queueId;
    ISR_DEFER_JOB       isrDefRd;
    ISR_DEFER_JOB       isrDefWr;

    /*
     * Each device may have multiple channels.  To handle this,
     * we keep a pointer to a pChan in pDev->pDrvCtrl, and maintain
     * a linked list of channels per controller.
     */

    struct vxbImxChan * pNext;    /* next channel for this device */

    volatile BOOL           txDone;			/* indicate whether TX finish */
    volatile UINT8          txData; 		/* buffer to hold TX data */
    volatile UINT32         rxData; 		/* buffer to hold RX data */
    VXB_DMA_RESOURCE_ID     dmaResTx;		/* TX DMA resource */
    VXB_DMA_RESOURCE_ID     dmaResRx;		/* RX DMA resource */
    SDMA_CHAN_EXT_PARAM *   pChanParamTx;	/* TX DMA parameters */
    SDMA_CHAN_EXT_PARAM *   pChanParamRx;	/* RX DMA parameters */
    } VXB_IMX_CHAN;

/* function declarations */

#endif    /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbImxSioh */
