
/* vxbStm32fSio.h - STM32F20x/STM32F21x serial port header file */

/*
 * Copyright (c) 2012 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,19apr12,rec  written.
*/

/*
DESCRIPTION

This file provides the ARM Cortex-M STM32F20x/STM32F21x USART specific 
definitions.

*/
#ifndef __INCvxbStm32fSioh
#define __INCvxbStm32fSioh

#ifdef __cplusplus
extern "C" {
#endif

#define STM32FUSART_NAME "vxbStm32fSio"

/* Status Register */
#define STM32FUSART_SR          0x0000 /* Register offset */
#define STM32FUSART_SRPE    0x00000001 /* Parity error */
#define STM32FUSART_SRFE    0x00000002 /* Framing error */
#define STM32FUSART_SRNF    0x00000004 /* Noise filter */
#define STM32FUSART_SRORE   0x00000008 /* Overrun error */
#define STM32FUSART_SRIDLE  0x00000010 /* Idle */
#define STM32FUSART_SRRXNE  0x00000020 /* Read data reg not empty */
#define STM32FUSART_SRTC    0x00000040 /* Transmission complete */
#define STM32FUSART_SRTXE   0x00000080 /* Transmit data reg not empty */
#define STM32FUSART_SRLBD   0x00000100 /* LIN break detection flag */
#define STM32FUSART_SRCTS   0x00000200 /* CTS flag */
    /* receiver error bits */
#define STM32F_RCV_ERROR \
(STM32FUSART_SRPE|STM32FUSART_SRFE|STM32FUSART_SRNF|STM32FUSART_SRORE)

/* Data Register */
#define STM32FUSART_DR          0x0004 /* Register offset */
#define STM32FUSART_DRMSK   0x000001FF /* Data register mask */

/* Baud Rate Register */
#define STM32FUSART_BRR         0x0008 /* Register offset */
#define STM32FUSART_BRRFRAC 0x0000000F /* Fraction mask */
#define STM32FUSART_BRRMANT 0x0000FFF0 /* Mantissa mask */
#define STM32FUSART_BRRMSK  0x0000FFFF /* Fraction + Mantissa mask */

#define STM32FUSART_BRRMANTSHFT      4 /* Mantissa shift */

/* Control Register 1 */
#define STM32FUSART_CR1         0x000C /* Register offset */
#define STM32FUSART_CR1SBK  0x00000001 /* Send break */
#define STM32FUSART_CR1RWU  0x00000002 /* Receiver wakeup */
#define STM32FUSART_CR1RE   0x00000004 /* Receiver enable */
#define STM32FUSART_CR1TE   0x00000008 /* Transmitter enable */
#define STM32FUSART_CR1IDLEIE \
                            0x00000010 /* Idle interrupt enable */
#define STM32FUSART_CR1RXNEIE \
                            0x00000020 /* RXNE interrupt enable */
#define STM32FUSART_CR1TCIE 0x00000040 /* Transmit complete interrupt enable */
#define STM32FUSART_CR1TXEIE \
                            0x00000080 /* TXE interrupt enable */
#define STM32FUSART_CR1PEIE 0x00000100 /* PE interrupt enable */
#define STM32FUSART_CR1PS   0x00000200 /* Parity selection (1=odd) */
#define STM32FUSART_CR1PCE  0x00000400 /* Parity control enable */
#define STM32FUSART_CR1WAKE 0x00000800 /* Wakeup method */
#define STM32FUSART_CR1M    0x00001000 /* Word length (1=9-bits) */
#define STM32FUSART_CR1UE   0x00002000 /* USART enable */
#define STM32FUSART_CR1OVER8 \
                            0x00008000 /* Oversampling (0=16, 1=8) */

/* Control Register 2 */
#define STM32FUSART_CR2         0x0010 /* Register offset */
#define STM32FUSART_CR2ADDMSK \
                            0x0000000F /* Address of the USART node */
#define STM32FUSART_CR2LBDL 0x00000020 /* LIN break detect length (1=11bits) */
#define STM32FUSART_CR2LBDIE \
                            0x00000040 /* LIN break detect interrupt enable */

#define STM32FUSART_CR2LBCL 0x00000100 /* Last bit clock pulse */
#define STM32FUSART_CR2CPHA 0x00000200 /* Clock phase (1=second clock) */
#define STM32FUSART_CR2CPOL 0x00000400 /* Clock polarity (1=steady high) */
#define STM32FUSART_CR2CLKEN \
                            0x00000800 /* Clock enable (1=SCLK enable) */
#define STM32FUSART_CR2STOP1\
                            0x00000000 /* STOP bits (1 stop bit) */
#define STM32FUSART_CR2STOPH\
                            0x00001000 /* STOP bits (.5 stop bit) */
#define STM32FUSART_CR2STOP2\
                            0x00002000 /* STOP bits (2 stop bits) */
#define STM32FUSART_CR2STOP1H\
                            0x00003000 /* STOP bits (1.5 stop bit) */
#define STM32FUSART_CR2STOPMSK\
                            0x00003000 /* STOP bit mask */
#define STM32FUSART_CR2LINEN \
                            0x00004000 /* LIN mode enable */

/* Control Register 3 */
#define STM32FUSART_CR3         0x0014 /* Register offset */
#define STM32FUSART_CR3EIE  0x00000001 /* Error interrupt enable */
#define STM32FUSART_CR3IREN 0x00000002 /* IrDA mode enable */
#define STM32FUSART_CR3IRLP 0x00000004 /* IrDA low-power */
#define STM32FUSART_CR3HDSEL \
                            0x00000008 /* Half-duplex select */
#define STM32FUSART_CR3NACK 0x00000010 /* Smartcard NACK enble */
#define STM32FUSART_CR3SCEN 0x00000020 /* Smartcard mode enable */
#define STM32FUSART_CR3DMAR 0x00000040 /* DMA enable receiver */
#define STM32FUSART_CR3DMAT 0x00000080 /* DMA enable transmitter */
#define STM32FUSART_CR3RTSE 0x00000100 /* RTS enable */
#define STM32FUSART_CR3CTSE 0x00000200 /* CTS enable */
#define STM32FUSART_CR3CTSIE \
                            0x00000400 /* CTS Interrupt enable */
#define STM32FUSART_CR3ONEBIT \
                            0x00000800 /* One sample bit method enable */

/* Guard Time and Prescaler Register */
#define STM32FUSART_GTPR        0x0018 /* Register offset */
#define STM32FUSART_GTPRPSCMSK \
                            0x000000FF /* Prescaler mask */
#define STM32FUSART_GTPRGTMSK \
                            0x0000FF00 /* Guard time value */
#define STM32FUSART_GTPRGTSHFT       8 /* Guard time shift */


#ifdef __cplusplus
}
#endif

#endif /* __INCvxbStm32fSioh */
