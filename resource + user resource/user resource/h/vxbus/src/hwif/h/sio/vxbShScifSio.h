/* vxbShScifSio.h - header file for Renesas SH on-chip SCIF driver for VxBus */

/*
 * Copyright (c) 2006-2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement.
 */

/*
modification history
--------------------
01b,14apr08,h_k  added handle field.
01a,29sug06,h_k  written based on shScifSio.h 01q.
*/

#ifndef __INCvxbShScifSioh
#define __INCvxbShScifSioh

#ifdef __cplusplus
extern "C" {
#endif

#include <sioLib.h>

/* device channel structure */

typedef struct vxbScifChan
    {
    /* always goes first */

    SIO_DRV_FUNCS *	pDrvFuncs;	/* driver functions */

    /* callbacks */

    STATUS		(*getTxChar) ();
    STATUS		(*putRcvChar) ();
    void *		getTxArg;
    void *		putRcvArg;

    /* info */

    int			mode;		/* current mode (interrupt or poll) */
    int			baud;		/* baud rate */
    UINT		options;	/* data format options */

    UINT		tfdr;		/* transmit data count set reg offset */
    UINT		rfdr;		/* data count set register offset */
    UINT		fifoLen;	/* FIFO length */
    int			regIndex;	/* bus subsystem: reg base index */
    VXB_DEVICE_ID	pDev;		/* bus subsystem: device ID */
#ifndef	VXB_SHSCIF_REG_DIRECT_ACCESS
    void *		handle;
#endif	/* !VXB_SHSCIF_REG_DIRECT_ACCESS */
    int			channelNo;	/* channel number */
    UINT		fcrTrg;		/* FCR TRG default value */
    BOOL		irqOpt;		/* connect one IRQ */

    /*
     * Each device may have multiple channels.  To handle this,
     * we keep a pointer to a pChan in pDev->pDrvCtrl, and maintain
     * a linked list of channels per controller.
     */

    struct vxbScifChan * pNext;		/* next channel for this device */
    } VXB_SCIF_CHAN;

/* entry structure for baud-rate table */

typedef struct
    {
    int   rate;		/* baud-rate to be set */
    UINT8 cksVal;	/* clock select bits for smr */
    UINT8 brrVal;	/* value to set in brr */
    } VXB_SCIF_BAUD;

#define SZ_SMR2 UINT16
#define SZ_SCR2 UINT16
#define SZ_FCR2 UINT16

/* Maximum FIFO length */

#define SCIF_FIFO_LEN		16

/* Extra bit values for scr (serial control register) */

#define SCIF_SCR2_EXIE		0

/* Bit values for FIFO control register */

#define SCIF_FCR2_RTRG		0xc0	/* Read mask for receive trigger */
#define SCIF_FCR2_RTRG_1	0x00    /* 1 char in FIFO triggers receive */
#define SCIF_FCR2_RTRG_4	0x40    /* 4 chars in FIFO triggers receive */
#define SCIF_FCR2_RTRG_8	0x80    /* 8 chars in FIFO triggers receive */
#define SCIF_FCR2_RTRG_14	0xc0    /* 14 chars in FIFO triggers receive */

#define SCIF_FCR2_TTRG		0x30    /* Read mask for transmit trigger */
#define SCIF_FCR2_TTRG_8	0x00    /* */
#define SCIF_FCR2_TTRG_4	0x10    /* */
#define SCIF_FCR2_TTRG_2	0x20    /* */
#define SCIF_FCR2_TTRG_1	0x30    /* */

#define SCIF_FCR2_TRG_DEFAULT	(SCIF_FCR2_RTRG | SCIF_FCR2_TTRG)

#define SCIF_FCR2_MCE		0x8     /* Enable RTS/CTS signals */
#define SCIF_FCR2_TFRST		0x4	/* Clear transmit FIFO on reset */
#define SCIF_FCR2_RFRST		0x2     /* Clear receive RIFO on reset */
#define SCIF_FCR2_LOOP		0x1     /* Loopback mode */

/* bit values for ssr (serial status register) */

#define	SCIF_SSR2_ER		0x80	/* framing or parity error */
#define	SCIF_SSR2_TEND		0x40	/* tranmission end indicator */
#define	SCIF_SSR2_TDFE		0x20	/* transmit data FIFO empty */
#define	SCIF_SSR2_BRK		0x10	/* break signal received */
#define	SCIF_SSR2_FER		0x08	/* framing error */
#define	SCIF_SSR2_PER		0x04	/* parity error */
#define	SCIF_SSR2_RDF		0x02	/* receive FIFO full */
#define	SCIF_SSR2_DR		0x01	/* data ready in receive FIFO */

/* bit value for lsr (line status register) */

#define SCIF_LSR2_ORER		0x1	/* overrun error */

/* bit value for ss2r (serial status 2 register) */

#define SCIF_SS2R2_ORER		0x1	/* overrun error */

#define SCIF_FREQ		SYS_PCLK_FREQ

/*
 * To calculate BRR value, the following formula used. 
 *   BRR = (FREQ / (FORM_VAL * 2^(2n-1)) * BaudRate)) - 1 
 *
 * In the following macros are used
 *    SMR_VALUE is n
 *    CLK_DIVIDE(y) 4^n
 *
 * In the BRR_VALUE macro the extra mutiplication, division and addition 
 * is used to get correct rounding.
 */

#define BRR_FORM_VAL	64

#define SMR_VALUE(x)	(((SCIF_FREQ/(BRR_FORM_VAL*32*(x))) >= 64) ? 3 : \
			 ((SCIF_FREQ/(BRR_FORM_VAL*32*(x))) >= 16) ? 2 : \
			 ((SCIF_FREQ/(BRR_FORM_VAL*32*(x))) >=  4) ? 1 : 0)

#define CLK_DIVIDE(y)	(((SCIF_FREQ/(BRR_FORM_VAL*32*(y))) >= 64) ? 64 : \
		 	 ((SCIF_FREQ/(BRR_FORM_VAL*32*(y))) >= 16) ? 16 : \
		 	 ((SCIF_FREQ/(BRR_FORM_VAL*32*(y))) >=  4) ?  4 : 1)

#define BRR_FORMULA(s)	(((20*SCIF_FREQ)/ \
			 (BRR_FORM_VAL*CLK_DIVIDE(s)*(s))-5)/10)

#define BRR_VALUE(z)		((BRR_FORMULA(z) > 255) ? 0 : BRR_FORMULA(z))

#define SCIF_SMR_CKS_50          SMR_VALUE(50)
#define SCIF_SMR_CKS_75          SMR_VALUE(75)
#define SCIF_SMR_CKS_110         SMR_VALUE(110)
#define SCIF_SMR_CKS_134         SMR_VALUE(134)
#define SCIF_SMR_CKS_150         SMR_VALUE(150)
#define SCIF_SMR_CKS_200         SMR_VALUE(200)
#define SCIF_SMR_CKS_300         SMR_VALUE(300)
#define SCIF_SMR_CKS_600         SMR_VALUE(600)
#define SCIF_SMR_CKS_1200        SMR_VALUE(1200)
#define SCIF_SMR_CKS_1800        SMR_VALUE(1800)
#define SCIF_SMR_CKS_2400        SMR_VALUE(2400)
#define SCIF_SMR_CKS_3600        SMR_VALUE(3600)
#define SCIF_SMR_CKS_4800        SMR_VALUE(4800)
#define SCIF_SMR_CKS_7200        SMR_VALUE(7200)
#define SCIF_SMR_CKS_9600        SMR_VALUE(9600)
#define SCIF_SMR_CKS_19200       SMR_VALUE(19200)
#define SCIF_SMR_CKS_31250       SMR_VALUE(31250)
#define SCIF_SMR_CKS_38400       SMR_VALUE(38400)
#define SCIF_SMR_CKS_57600       SMR_VALUE(57600)
#define SCIF_SMR_CKS_115200      SMR_VALUE(115200)
#define SCIF_SMR_CKS_500000      SMR_VALUE(500000)
 
/* bit values for smr (serial mode register) */

#define	SCIF_SMR_ASYNC		0x00	/* async mode */
#define	SCIF_SMR_SYNC		0x80	/* synchronous mode */
#define	SCIF_SMR_8_BITS		0x00	/* 8 bits data */
#define	SCIF_SMR_7_BITS		0x40	/* 7 bits data */
#define	SCIF_SMR_PAR_DIS	0x00	/* parity disable */
#define	SCIF_SMR_PAR_EN		0x20	/* parity enable */
#define	SCIF_SMR_PAR_EVEN	0x00	/* even parity */
#define	SCIF_SMR_PAR_ODD	0x10	/* odd parity  */
#define	SCIF_SMR_1_STOP		0x00	/* 1 stop bit */
#define	SCIF_SMR_2_STOP		0x08	/* 2 stop bit */
#define	SCIF_SMR_MP_DIS		0x00	/* multiprocessor format disable */
#define	SCIF_SMR_MP_EN		0x04	/* multiprocessor format enable */
#define	SCIF_SMR_CKS		0x03	/* clock select bits */
#define	SCIF_SMR_CKS_MASK	0xfc	/* mask for clock select bits */

/* bit values for brr (bit rate register) */

#define SCIF_BRR_50      	BRR_VALUE(50)
#define SCIF_BRR_75      	BRR_VALUE(75)
#define SCIF_BRR_110     	BRR_VALUE(110)
#define SCIF_BRR_134     	BRR_VALUE(134)
#define SCIF_BRR_150     	BRR_VALUE(150)
#define SCIF_BRR_200     	BRR_VALUE(200)
#define SCIF_BRR_300   		BRR_VALUE(300) 
#define SCIF_BRR_600   		BRR_VALUE(600) 
#define SCIF_BRR_1200  		BRR_VALUE(1200) 
#define SCIF_BRR_1800  		BRR_VALUE(1800) 
#define SCIF_BRR_2400  		BRR_VALUE(2400) 
#define SCIF_BRR_3600  		BRR_VALUE(3600) 
#define SCIF_BRR_4800  		BRR_VALUE(4800) 
#define SCIF_BRR_7200  		BRR_VALUE(9600) 
#define SCIF_BRR_9600  		BRR_VALUE(9600) 
#define SCIF_BRR_19200 		BRR_VALUE(19200) 
#define SCIF_BRR_31250 		BRR_VALUE(31250) 
#define SCIF_BRR_38400 		BRR_VALUE(38400) 
#define SCIF_BRR_57600   	BRR_VALUE(57600) 
#define SCIF_BRR_115200   	BRR_VALUE(115200) 
#define SCIF_BRR_500000   	BRR_VALUE(500000) 

/* bit values for scr (serial control register) */

#define	SCIF_SCR_TIE		0x80	/* transmit interrupt enable  */
#define	SCIF_SCR_RIE		0x40	/* receive interrupt enable  */
#define	SCIF_SCR_TXE		0x20	/* transmit enable */
#define	SCIF_SCR_RXE		0x10	/* receive enable */
#define	SCIF_SCR_TEIE		0x04	/* transmit end interrupt enable */
#define	SCIF_SCR_CKE		0x03	/* clock enable bits */


/* bit values for ssr (serial status register) */

#define	SCIF_SSR_TDRE		0x80	/* transmit data register empty */
#define	SCIF_SSR_RDRF		0x40	/* receive data register full */
#define	SCIF_SSR_ORER		0x20	/* overrun error */
#define	SCIF_SSR_FER		0x10	/* framing error */
#define	SCIF_SSR_PER		0x08	/* parity error */
#define	SCIF_SSR_TEND		0x04	/* transmit end */

/* function prototypes */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif  /* __INCvxbShScifSioh */

