/* vxbPrimeCellSio.h - ARM AMBA UART for VxBus header file */

/*
 * Copyright (c) 2007, 2010 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement.
 */

/*
modification history
--------------------
01b,02jul10,x_z  Added one item into VXB_AMBA_CHAN for vxBus access method.
01a,25apr07,h_k  written based on arm_ebmpcore/pimeCellSio.h 1b.
*/

#ifndef __INCvxbPrimeCellSioh 
#define __INCvxbPrimeCellSioh 

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _ASMLANGUAGE

#include <sioLib.h>
#include <spinLockLib.h>
#include <isrDeferLib.h>

/* Register description OF ARM AMBA UART */

/* Register offsets from base address */
#define	UARTDR		0x00		/* UART data register (R/W) */
#define	UARTRSR		0x04		/* Rx data status register (R/O) */
#define	UARTECR		0x04		/* Clr modem status changed int (W/O) */
#define	UARTFR		0x18		/* Flag register */
#define	UARTILPR	0x20		/* IrDA low-power counter register */
#define	UARTIBRD	0x24		/* Integer baud rate divisor */
#define	UARTFBRD	0x28		/* Fractional baud Rate divisor */
#define	UARTLCR_H       0x2C            /* Line control */
#define	UARTCR          0x30            /* Control register */
#define	UARTIFLS        0x34            /* Interrupt FIFO level select */
#define	UARTIMSC        0x38            /* Interrupt mask Set/Clear */
#define	UARTRIS         0x3C            /* Raw Interrupt Status */
#define	UARTMIS         0x40            /* Masked Interrupt Status */
#define	UARTICR         0x44            /* Interrupt clear register */
#define	UARTDMACR       0x48            /* DMA control register */

/* bit definitions for DMA control reg, DMACR */

#define UART_RX_DMA_ENABLE        (1 << 0)
#define UART_TX_DMA_ENABLE        (1 << 1)


/* bit definitions for FIFO level select, UARTIFLS */

#define UART_TX_ONE_EIGHTH        0x00
#define UART_TX_ONE_FOURTH        0x01
#define UART_TX_ONE_HALF          0x02
#define UART_TX_THREE_FOUTHS      0x03
#define UART_TX_SEVEN_EIGHTHS     0x04
#define UART_RX_ONE_EIGHTH        0x00
#define UART_RX_ONE_FOURTH        0x08
#define UART_RX_ONE_HALF          0x10
#define UART_RX_THREE_FOUTHS      0x18
#define UART_RX_SEVEN_EIGHTHS     0x20


/* bit definitions within UARTLCR_H register */

#define SEND_BREAK      (1 << 0)
#define PARITY_NONE	(0 << 1)	/* set no parity */
#define PARITY_ENABLED  (1 << 1)
#define EVEN_PARITY     (1 << 2)
#define ONE_STOP        (0 << 3)        /* set one stop bit */
#define TWO_STOP        (1 << 3)
#define FIFO_ENABLE	(1 << 4)	/* Enable both FIFOs */
#define WORD_LEN_5      (0x00 << 5)
#define WORD_LEN_6	(0x01 << 5)
#define WORD_LEN_7	(0x02 << 5)
#define WORD_LEN_8	(0x03 << 5)

/* bit definitions within UARTCR register */

#define UART_ENABLE	(1 <<  0)	/* Enable the UART */
#define UART_LBE	(1 <<  7)	/* Loop Back Enable */
#define UART_TXE	(1 <<  8)	/* Transmit Int Enable */
#define UART_RXE	(1 <<  9)	/* Receive Int Enable */
#define UART_DTR	(1 << 10)	/* Data Transmit Int Enable */
#define UART_RTS	(1 << 14)	/* RTS H/W Flow Control Int Enable */


/* bit definitions within UARTFR register */

#define FLG_UTXFE	(1 << 7)	/* UART Tx FIFO Empty */
#define FLG_URXFF	(1 << 6)	/* UART Rx FIFO Full */
#define FLG_UTXFF	(1 << 5)	/* UART Tx FIFO Full */
#define FLG_URXFE	(1 << 4)	/* UART Rx FIFO Empty */
#define FLG_BUSY	(1 << 3)	/* UART Busy */

/* bit definitions within UARTIMSC register */

#define IMSC_RIMIM      (1 <<  0)       /* RI Modem Int */
#define IMSC_CTSMIM     (1 <<  1)       /* CTS Modem Int */
#define IMSC_DCDMIM     (1 <<  2)       /* DCD Modem Int */
#define IMSC_DSRMIM     (1 <<  3)       /* DSR Modem Int */
#define IMSC_RXIM       (1 <<  4)       /* Rx Int */
#define IMSC_TXIM       (1 <<  5)       /* Tx Int */
#define IMSC_RTIM       (1 <<  6)       /* Rx Timeout Int */
#define IMSC_FEIM       (1 <<  7)       /* Framing Err Int */
#define IMSC_PEIM       (1 <<  8)       /* Parity Err Int */ 
#define IMSC_BEIM       (1 <<  9)       /* Break Err Int */
#define IMSC_OEIM       (1 << 10)       /* Overrun Err Int */

/* bit definitions within UARTMIS register */

#define UART_RIMMIS     (1 <<  0)       /* RI Modem Int */
#define UART_CTSMMIS    (1 <<  1)       /* CTS Modem Int */
#define UART_DCDMMIS    (1 <<  2)       /* DCD Modem Int */
#define UART_DSRMMIS    (1 <<  3)       /* DSR Modem Int */
#define UART_RXMIS      (1 <<  4)       /* Rx Int */
#define UART_TXMIS      (1 <<  5)       /* Tx Int */
#define UART_RTMIS      (1 <<  6)       /* Rx Timeout Int */
#define UART_FEMIS      (1 <<  7)       /* Framing Err Int */
#define UART_PEMIS      (1 <<  8)       /* Parity Err Int */ 
#define UART_BEMIS      (1 <<  9)       /* Break Err Int */
#define UART_OEMIS      (1 << 10)       /* Overrun Err Int */

/* bit definitions within UARTICR register */

#define UART_RIMIC      (1 <<  0)       /* RI Modem Int Clear */
#define UART_CTSMIS     (1 <<  1)       /* CTS Modem Int Clear */
#define UART_DCDMIC     (1 <<  2)       /* DCD Modem Int Clear */
#define UART_DSRMIC     (1 <<  3)       /* DSR Modem Int Clear */
#define UART_RXIC       (1 <<  4)       /* Rx Int Clear */
#define UART_TXIC       (1 <<  5)       /* Tx Int Clear */
#define UART_RTIC       (1 <<  6)       /* Rx Timeout Int Clear */
#define UART_FEIC       (1 <<  7)       /* Framing Err Int Clear */
#define UART_PEIC       (1 <<  8)       /* Parity Err Int Clear */ 
#define UART_BEIC       (1 <<  9)       /* Break Err Int Clear */
#define UART_OEIC       (1 << 10)       /* Overrun Err Int Clear */


typedef struct vxbAmbaChan
    {
    /* must be first */

    SIO_DRV_FUNCS * pDrvFuncs;	/* driver functions */
    void *          handle;

    /* callbacks */

    STATUS	(*getTxChar) ();  /* installed Tx callback routine */
    STATUS	(*putRcvChar) (); /* installed Rx callback routine */
    void *	getTxArg;	/* argument to Tx callback routine */
    void *	putRcvArg;	/* argument to Rx callback routine */

    UINT32	channelMode;	/* such as INT, POLL modes */
    int		baudRate;	/* the current baud rate */
    UINT32	xtal;		/* UART clock frequency */     

    int		regIndex;	/* bus subsystem: reg base index */
    VXB_DEVICE_ID pDev;		/* bus subsystem: device ID */
    int		channelNo;	/* channel number */

    /* need for SMP and ISR deferral model */

    spinlockIsr_t spinlockIsr;	/* ISR-callable spinlock */
    BOOL	txDefer;	/* TX interrupt is currently deferred */
    ISR_DEFER_QUEUE_ID	queueId;
    ISR_DEFER_JOB	isrDefRd;
    ISR_DEFER_JOB	isrDefWr;

    /*
     * Each device may have multiple channels.  To handle this,
     * we keep a pointer to a pChan in pDev->pDrvCtrl, and maintain
     * a linked list of channels per controller.
     */

    struct vxbAmbaChan * pNext;	/* next channel for this device */
    } VXB_AMBA_CHAN;

/* function declarations */

#endif	/* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif
 
#endif /* __INCvxbPrimeCellSioh */
