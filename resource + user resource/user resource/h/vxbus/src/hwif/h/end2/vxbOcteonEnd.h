/* vxbOcteonEnd.h - header file for Octeon RGMII VxBus END driver */

/*
 * Copyright (c) 2007-2012 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01s,11may12,l_z  Fix build error for OCT_MAX_PORTS_PER_IFACE
01r,09feb12,x_f  Add OCT_TAG_TX_TYPE_MAGIC define
01q,31jan11,d_c  Fix for Defect WIND00250323
                 Forwarding updates on behalf of Zihang Ruan
01p,27jan11,d_c  Add a statistic to track work queue post errors.
01o,26nov10,zhr  Used vsb parameter corresponding to the BSP changes 
01n,11oct10,d_c  Move from Cavium BSPs (cav_*).
01m,23sep10,d_c  Change to use VSB parameters for port forwarding.
01l,31aug10,d_c  Change to use SGMII helper driver for CN56XX and CN63XX
01k,25aug10,pgh  Remove OCTEON_MODEL
01j,18aug10,pgh  Add octeonBoard.h
01i,06aug10,d_c  Remove direct SDK access. Use Wind River "cav" API
01h,14oct09,l_z  Add hwstat for CN50XX. (WIND00183824)
01g,05jan09,dlk  Remove output queue lock member and 'flow control pool'.
01f,12jan09,rme  Cleanup after generalisation
01e,01sep08,rme  Support for FW statistics
01d,01sep08,rme  Support for FW specific input queue
01c,26may08,l_z  Add CN50XX support
01b,18jun08,rme  Added support for multiple IP forwarders
01a,04jun07,wap  written
*/


#ifndef __INCvxbOcteonEndh
#define __INCvxbOcteonEndh

#include <vsbConfig.h>
#include <arch/mips/cavPacket.h>
#include <arch/mips/caviumLib.h>

#ifdef __cplusplus
extern "C" {
#endif

#undef OCT_ENABLE_STATISTICS

IMPORT void octRgmiiRegister (void);

#define OCTEON_POW_RXTX_GROUP 15
#define OCTEON_POW_RECEIVE_GROUP 15  /* Alias for OCTEON_POW_RXTX_GROUP */
#define OCTEON_POW_SEND_GROUP 14     /* Used only in polled-mode (WDB debug) */

/*
 * 72 128-byte cache lines == 9216 bytes
 * Note that with an MTU of 9000 bytes,
 * a 14 byte Ethernet header, stacked VLAN (8 bytes),
 * plus CRC, we have 9000 + 14 + 8 + 4 = 9026.
 * Actually, since we leave the IPD programmed to
 * strip the FCS/CRC, we only have 9022 bytes.
 * The space reserved at the start of the first
 * buffer is CVMX_HELPER_FIRST_MBUFF_SKIP = 184
 * (see executive-config.h), and
 * 9022 + 184 = 9206, which leaves a margin
 * of up to 10 bytes for aligning the IP header.
 * The padding added to align the IP header is
 * documented to be 0-7 bytes, as the IPD tries
 * to align IPv4 headers on an address congruent
 * to 4 modulo 8, and to align IPv6 headers
 * congruent to 0 modulo 8.
 * So, we're OK.
 */

#define OCT_FPA_JUMBO_PACKET_POOL_SIZE (72 * 128)

#define OCT_MTU 1500
#define OCT_JUMBO_MTU 9000
#define OCT_CLSIZE      1600
#define OCT_NAME        "pkt"  /* for the main driver's instance */
#define OCT_TIMEOUT 10000
#define OCT_INTRS 0
#define OCT_RXINTRS 0
#define OCT_TXINTRS 0
#define OCT_LINKINTRS 0

#define OCT_RX_DESC_CNT 256
#define OCT_TX_DESC_CNT 256

#define OCT_MAX_RXWORK 16
#define OCT_MAX_TXWORK 32

#define OCT_TAG_TX_TYPE_MAGIC 0x55665566

#ifndef _WRS_CONFIG_OCTEON_NUM_FORWARDERS
#define _WRS_CONFIG_OCTEON_NUM_FORWARDERS               0
#endif

#define OCT_FLOW_CTRL_RESERVE 3

/* Private TX work queue entry, used for TX cleanup */

/*
 * This value of MAXSEGS is chosen so that
 * OCT_TXWQE fits in a single cache block,
 * which is the size of the work queue entries.
 */
#define OCT_MAXSEGS 13

typedef struct octTxWqe
    {
    UINT64              octWord0;
    UINT64              octWord1;
    void *              octPkt;
    void *              pad;
    CAV_BUF_PTR_T       octSeg[OCT_MAXSEGS];
    } OCT_TXWQE;

typedef struct octTxWqe78xx
    {
    UINT64              octWord0;
    UINT64              octWord1;
    void *              octPkt;
    void *              pad;
    CAV_PKO_BUF_PTR_T   octSeg[OCT_MAXSEGS];
    } OCT_TXWQE_78XX;

/*
 * Private adapter context structure.
 */

typedef struct oct_pkt_drv_ctrl OCT_PKT_DRV_CTRL;

#ifdef _WRS_CONFIG_COMPONENT_IPFORWARDER
typedef struct non_fw_s
{
    UINT32 frames;
    UINT32 bytes;

} non_fw_t;
#endif

typedef struct fwd_s {

    int     octPort;
#ifdef  _WRS_CONFIG_COMPONENT_OCTEON_PRIORITY_QUEUE
    int     octQueueLenReg;
#endif
    int     octQueue;
    UINT64 *octTxChunk;
    int     octTxFree;
    void   *pdrv;

} fwd_t;

#ifdef OCT_ENABLE_STATISTICS
typedef struct oct_dev_statistics
    {
    uint32_t rxDeliver;
    uint32_t rxFail;
    uint32_t txHandle;
    uint32_t txRestart;
    uint32_t txNoWqe;
    uint32_t txNoChunk;
    uint32_t txSend;
    uint32_t txStall;
    uint32_t txInactive;
    } OCT_DEV_STATISTICS;

typedef struct oct_statistics
    {
    uint32_t ints;
    uint32_t reposts;
    uint32_t unmask;
    uint32_t postErr;
    } OCT_STATISTICS;
#endif /* OCT_ENABLE_STATISTICS */

typedef struct oct_drv_ctrl
    {
    END_OBJ             octEndObj;
    VXB_DEVICE_ID       octDev;
    void                *octMuxDevCookie;

    int                 octUnit;
    INT16               octPort;
    INT16               octIndex;
    INT16               octSendGroup;
    INT16               octIface;
    fwd_t *             fw;
    BOOL                octTxStall;
    int                 octTxPending;

    BOOL                octPolling;

    UINT8               octAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES    octCaps;

    /* Begin MII/ifmedia required fields. */
    END_MEDIALIST *     octMediaList;
    END_ERR             octLastError;
    UINT32              octCurMedia;
    UINT32              octCurStatus;
    VXB_DEVICE_ID       octMiiBus;
    VXB_DEVICE_ID       octMiiDev;
    VXB_DEVICE_ID       octPrivDev;
    FUNCPTR             octMiiPhyRead;
    FUNCPTR             octMiiPhyWrite;
    int                 octMiiPhyAddr;
    /* End MII/ifmedia required fields */

    int                 octMaxMtu;

    SEM_ID              octDevSem;

#ifdef _WRS_CONFIG_COMPONENT_IPFORWARDER
    non_fw_t            non_fw;
#endif
#ifdef OCT_ENABLE_STATISTICS
    OCT_DEV_STATISTICS  stats;
#endif
    } OCT_DRV_CTRL;

#ifndef OCT_MAX_PORTS_PER_IFACE
#define OCT_MAX_PORTS_PER_IFACE 16
#endif /* OCT_MAX_PORTS_PER_IFACE */

#define OCT_IFACE_ALL_PORTS     0xffff

typedef struct oct_pkt_iface_info OCT_PKT_IFACE_INFO;

typedef struct oct_pkt_iface_funcs
    {
    /*
     * Check if the sub-driver can handle the packet interface ifaceNum.
     * The current packet interface mode (from GMXX_INF_MODE low 2 bits)
     * is available in pInfo->hwstate. Called from octInstInit().
     */
    STATUS (*probe) (int ifaceNum, OCT_PKT_IFACE_INFO * pInfo);

    /*
     * Set the ports that can be handled for this packet interface.
     * pPorts points at the a bitmap of the requested local port
     * numbers for the interface.  The sub-driver should overwrite
     * *pPorts with the set of ports that can actually be handled.
     * (If possible, this should be a subset of the requested ports.)
     * Called from octInstInit().
     */
    void (*portsAvailable) (int ifaceNum, uint32_t *pPorts);

    /*
     * Sequentially allocate unit numbers controlled by this driver.
     */
    int (*allocUnitNumber) (void);

    /*
     * sub-driver specific hardware configuration done at the end
     * octInstInit2().
     */
    STATUS (*init) (int ifaceNum, OCT_PKT_IFACE_INFO * pInfo, uint32_t flags);

    /*
     * Fully implements octEndRxConfig() in the sub-driver.
     * The main driver has already taken driver mutual exclusion.
     */
    void (*rxConfig) (OCT_DRV_CTRL * pDrvCtrl);
    /*
     * Fully implements octEndHashTblPopulate() in the sub-driver.
     * May be a no-op if all-multicast mode always is used.
     */
    void (*hashTblPopulate) (OCT_DRV_CTRL * pDrvCtrl);
    /*
     * linkUpdate: program the MAC according to the settings in
     * pDrvCtrl->octCurMedia and pDrvCtrl->octCurStatus, just read
     * by miiBusModeGet().
     * Mutual exclusion is already taken care of by the main
     * vxbOcteonEnd driver.
     * The linkUpdate() routine should also set
     * pDrvCtrl->octEndObj.mib2Tbl.ifSpeed and
     * pDrvCtrl->octEndObj.pMib2Tbl->m2Data.mibXIfTbl.ifHighSpeed
     * according to the new speed.
     *
     *The main driver will take care of notifying the stack when
     * the link state changes.
     */
    void (*linkUpdate) (OCT_DRV_CTRL * pDrvCtrl);

    /*
     * start1() is called from early in octEndStart() before the call
     * to cvmx_pko_config_port().  Does early port HW setup, but
     * does not actually enable the port -- that's left for
     * rxtx_enable() near the end of octEndStart().
     */
    STATUS (*start1) (OCT_DRV_CTRL * pDrvCtrl);

    /*
     * Enable RX and TX on the port specified.
     * This routine is called from octEndStop(), with the port's
     * END TX mutex and octDevSem mutex held.
     */
    void (*rxtx_enable) (OCT_DRV_CTRL * pDrvCtrl);

    /*
     * Disable RX and TX on the port specified.
     * This routine is called from octEndStop(), with the port's
     * END TX mutex and octDevSem mutex held.
     */
    void (*rxtx_disable) (OCT_DRV_CTRL * pDrvCtrl);

    /*
     * This routine is called by muxDevLoad() as the
     * END load routine for ports on this packet interface.
     * This function is provided because the sub-driver
     * knows the driver name to use, e.g. 'spi' vs. 'eth'
     * without any other context (none is provided by
     * muxDevLoad() on its first call to the load routine).
     * The sub-driver should handle the first call (when the
     * <loadStr> argument points to a NUL byte), the <pBsp> argument
     * passed by muxDevLoad() has no useful information, by
     * copying the appropriate driver name to the <loadStr>
     * buffer.
     * The sub-driver may delegate part of the second call to its
     * endLoad() routine by calling octEndLoad(), which is left
     * globally visible only for this reason. On the 2nd
     * call (when loadStr points to a non-NUL character),
     * <pBsp> is actually the OCT_DRV_CTRL pointer for the
     * device. The sub-driver should pass this through to
     * octEndLoad() after
     *
     * 1. Writing the appropriate MAC address for the unit into
     *    pDrvCtrl->octAddr.
     * 2. Calling END_OBJ_INIT().
     */
    END_OBJ * (*endLoad) (char * loadStr, void * pBsp);

    } OCT_PKT_IFACE_FUNCS;

struct oct_pkt_iface_info
    {
    int hwstate;
    uint32_t configured_ports;
    OCT_PKT_IFACE_FUNCS * funcs;
    void * cookie;      /* for sub-driver use */
    };

struct oct_pkt_drv_ctrl
    {
    /* Cache line aligned! */
    VXB_DEVICE_ID       pktDev;
    JOB_QUEUE_ID        octJobQueue;

    QJOB                octRxTxIntJob;
    atomicVal_t         octRxTxIntPending;
    
    /*
     * This array is indexed by octeon input port number (0 ->
     * (octMaxInputPorts-1)) and gives the OCT_DRV_CTRL for each
     * input port (NULL if the port is not enabled).
     *
     * Given the sizes of the preceding members, 26 entries of this
     * array ought to fit in the first cache line.  That should cover
     * all the entries actually in use for RGMII, SGMII, or SPI4000.
     */
    
    OCT_DRV_CTRL **     octDevs;

    int                 totalPorts;
    uint32_t            flags;
#define OCT_END_FLAG_JUMBO      1


    UINT64              octRxTxIntMask;

    void *              octPool;
    void *              wqePool;
    void *              txChunkPool;

    BOOL                muxConnected;

#ifdef OCT_ENABLE_STATISTICS
    OCT_STATISTICS      stats;
#endif
    };

/* Provided by vxbOcteonEnd.c, referenced by sub-drivers: */

IMPORT END2_NET_FUNCS octNetFuncs;
IMPORT OCT_PKT_IFACE_INFO *octInterface;

IMPORT END_OBJ * octEndLoad (char * loadStr, void * pBsp);


#ifdef __cplusplus
}
#endif

#endif /* __INCvxbOcteonEndh */
