/* vxbArchAccess.h - vxBus arch access routines header file */

/* Copyright (c) 2005-2007 Wind River Systems, Inc. */

/* 
modification history
--------------------
01f,12jun07,tor  remove VIRT_ADDR
01e,26oct05,mdo  add extern function declarations
01d,20sep05,pdg  Fix for vxbus access routines errors (SPR #112197)
01c,10aug05,mdo  Phase in new access method
01b,01aug05,mdo  Updating bus access methods
01a,25Jul05,gpd  written
*/

#ifndef __INCvxbArchAccessh
#define __INCvxbArchAccessh

#ifdef __cplusplus
extern "C" {
#endif

extern STATUS   _archRegProbe
    (
    VXB_DEVICE_ID   pDevInfo,       /* device info */
    void    *       pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset,     /* offset, in bytes, of register */
    UINT32          transactionSize,/* register size */
    char *          pProbeDatum,    /* value to write */
    char *          pRetVal,        /* value read back */
    UINT32 *        pFlags          /* Flags used */
    );
extern STATUS   _archRegisterRead8
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT8 *         pDataBuf,   /* buffer to put data in */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archRegisterRead16
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT16 *        pDataBuf,   /* buffer to put data in */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archRegisterRead32
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT32 *        pDataBuf,   /* buffer to put data in */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archRegisterRead64
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT64 *        pDataBuf,   /* buffer to put data in */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archRegisterWrite8
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT8 *         pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );                         

extern STATUS   _archRegisterWrite16
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT16 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archRegisterWrite32
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT32 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );
 
extern STATUS   _archRegisterWrite64
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT64 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );


extern STATUS   _archVolatileRegisterWrite
    (
    VXB_DEVICE_ID   pDevInfo,       /* device info */
    void *          pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset,     /* offset, in bytes, of register */
    UINT32          transactionSize,/* transaction size, in bytes */
    char *          pDataBuf,       /* buffer to read-from/write-to */
    UINT32 *        pFlags          /* flags */
    );

extern STATUS   _archVolatileRegisterRead
    (
    VXB_DEVICE_ID   pDevInfo,       /* device info */
    void *          pRegBase,       /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset,     /* offset, in bytes, of register */
    UINT32          transactionSize,/* transaction size, in bytes */
    char *          pDataBuf,       /* buffer to read-from */
    UINT32 *        pFlags          /* flags */
    );

extern STATUS   _archDevControl
    (
    VXB_DEVICE_ID   devID,          /* device info */
    pVXB_DEVCTL_HDR pBusDevControl  /* parameter */
    );

extern STATUS   _archOptRegWr64_00
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT64 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegWr64_07
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT64 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegWr64_20
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT64 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegWr64_27
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT64 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegWr64_10
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT64 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegWr64_17
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT64 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegWr64_30
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT64 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegWr64_37
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT64 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );


extern STATUS   _archOptRegWr32_00
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT32 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegWr32_03
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT32 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archOptRegWr32_20
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32         byteOffset, /* offset, in bytes, of register */
    UINT32 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archOptRegWr32_23
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT32 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archOptRegWr32_10
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT32 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archOptRegWr32_13
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT32 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archOptRegWr32_30
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT32 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archOptRegWr32_33
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT32 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegWr16_00
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT16 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegWr16_01
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT16 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archOptRegWr16_20
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT16 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archOptRegWr16_21
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT16 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegWr16_10
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT16 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegWr16_11
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT16 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archOptRegWr16_30
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT16 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archOptRegWr16_31
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT16 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegWr8_00
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT8 *         pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archOptRegWr8_20
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT8 *         pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archOptRegWr8_10
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT8 *         pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegWr8_30
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT8 *         pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );            

extern STATUS   _archOptRegWrRd64_00
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT64 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegWrRd64_07
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT64 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegWrRd64_20
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT64 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegWrRd64_27
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT64 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegWrRd64_10
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT64 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegWrRd64_17
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT64 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegWrRd64_30
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT64 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegWrRd64_37
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT64 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );


extern STATUS   _archOptRegWrRd32_00
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT32 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegWrRd32_03
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT32 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archOptRegWrRd32_20
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32         byteOffset, /* offset, in bytes, of register */
    UINT32 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archOptRegWrRd32_23
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT32 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archOptRegWrRd32_10
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT32 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archOptRegWrRd32_13
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT32 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archOptRegWrRd32_30
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT32 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archOptRegWrRd32_33
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT32 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegWrRd16_00
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT16 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegWrRd16_01
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT16 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archOptRegWrRd16_20
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT16 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archOptRegWrRd16_21
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT16 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegWrRd16_10
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT16 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegWrRd16_11
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT16 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archOptRegWrRd16_30
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT16 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archOptRegWrRd16_31
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT16 *        pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegWrRd8_00
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT8 *         pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archOptRegWrRd8_20
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT8 *         pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archOptRegWrRd8_10
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT8 *         pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegWrRd8_30
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT8 *         pDataBuf,   /* buffer to copy data from */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archOptRegRd64_00
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT64 *        pDataBuf,   /* buffer to put data in */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archOptRegRd64_07
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT64 *        pDataBuf,   /* buffer to put data in */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegRd64_20
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT64 *        pDataBuf,   /* buffer to put data in */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegRd64_27
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT64 *        pDataBuf,   /* buffer to put data in */
    UINT32 *        pFlags      /* flags */
    );
    
extern STATUS   _archOptRegRd32_00
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT32 *        pDataBuf,   /* buffer to put data in */
    UINT32 *        pFlags      /* flags */
    );    
extern STATUS   _archOptRegRd32_03
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT32 *        pDataBuf,   /* buffer to put data in */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archOptRegRd32_20
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT32 *        pDataBuf,   /* buffer to put data in */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archOptRegRd32_23
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT32 *        pDataBuf,   /* buffer to put data in */
    UINT32 *        pFlags      /* flags */
    );

extern STATUS   _archOptRegRd16_00
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT16 *        pDataBuf,   /* buffer to put data in */
    UINT32 *        pFlags      /* flags */
    );                
extern STATUS   _archOptRegRd16_01
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT16 *        pDataBuf,   /* buffer to put data in */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archOptRegRd16_20
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT16 *        pDataBuf,   /* buffer to put data in */
    UINT32 *        pFlags      /* flags */
    );
extern STATUS   _archOptRegRd16_21
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT16 *        pDataBuf,   /* buffer to put data in */
    UINT32 *        pFlags      /* flags */
    );      
    
extern STATUS   _archOptRegRd8_00
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT8 *         pDataBuf,   /* buffer to put data in */
    UINT32 *        pFlags      /* flags */
    );          
    
extern STATUS   _archOptRegRd8_20
    (
    VXB_DEVICE_ID   pDevInfo,   /* device info */
    void *          pRegBase,   /* VXB_DEVICE::pRegBase[] entry */
    UINT32          byteOffset, /* offset, in bytes, of register */
    UINT8 *         pDataBuf,   /* buffer to put data in */
    UINT32 *        pFlags      /* flags */
    );    

extern UINT8 _archIORead8
    (
    ULONG	address     /* I/O address to read the byte from */
    );

extern UINT16	_archIORead16
    (
    ULONG	address
    );

extern UINT32	_archIORead32
    (
    ULONG	address
    );

extern void	_archIOWrite8
    (
    ULONG	address,
    UINT8	data
    );

extern void	_archIOWrite16
    (
    ULONG	address,
    UINT16	data
    );

extern void	_archIOWrite32
    (
    ULONG	address,
    UINT32	data
    );

extern STATUS	vxIOArchProbe
    (
    void *	adrs,		/* address to be probed */
    int		mode,		/* VX_READ or VX_WRITE */
    int		length,		/* 1, 2, 4, 8, or 16 */
    void *	pVal 		/* where to return value, */
				/* or ptr to value to be written */
    );
#ifdef __cplusplus
}
#endif

#endif /* __INCvxbArchAccessh */
