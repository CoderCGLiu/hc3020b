/* vxbRapidIO.h - RapidIO Bus type header file */

/*
 * Copyright (c) 2005-2009, 2012 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01c,05nov12,j_z  added some RapdIO common defines, swith information, etc.
01b,25mar09,h_k  added prototypes for vxbRapidIOCfg() and
                 vxbRapidIOHcfTableOverride().
01a,07Oct05,tor  created
*/

#ifndef __INC_vxbRapidIOH
#define __INC_vxbRapidIOH

/* includes */

#include "vxWorks.h"
#include <hwif/vxbus/hwConf.h>
#include <hwif/vxbus/vxBus.h>

/* defines */

/* I/O Register Map */

#define RIO_CAR_DEVVEND_ID              0x00
   #define  RIO_CAR_VID_FSL                     0x0002
   #define  RIO_CAR_VID_LSI                     0x000A
#define RIO_CAR_DEVICE_INFO             0x04
#define RIO_CAR_ASSEMBLY_ID             0x08
#define RIO_CAR_ASSEMBLY_INFO           0x0c
#define RIO_CAR_PE_FEATURES             0x10
   #define  RIO_CAR_PEF_BRIDGE                  0x80000000 /* Bridge */
   #define  RIO_CAR_PEF_MEMORY                  0x40000000 /* MMIO */
   #define  RIO_CAR_PEF_PROCESSOR               0x20000000 /* Processor */
   #define  RIO_CAR_PEF_SWITCH                  0x10000000 /* Switch */
   #define  RIO_CAR_PEF_MULTIPORT               0x08000000 /* Multiport */
   #define  RIO_CAR_PEF_INB_MBOX                0x00f00000 /* Mailboxes */
   #define  RIO_CAR_PEF_INB_MBOX0               0x00800000 /* Mailbox 0 */
   #define  RIO_CAR_PEF_INB_MBOX1               0x00400000 /* Mailbox 1 */
   #define  RIO_CAR_PEF_INB_MBOX2               0x00200000 /* Mailbox 2 */
   #define  RIO_CAR_PEF_INB_MBOX3               0x00100000 /* Mailbox 3 */
   #define  RIO_CAR_PEF_INB_DOORBELL            0x00080000 /* Doorbells */
   #define  RIO_CAR_PEF_EXT_RT                  0x00000200 /* Extended route table support */
   #define  RIO_CAR_PEF_STD_RT                  0x00000100 /* Standard route table support */
   #define  RIO_CAR_PEF_CTLS                    0x00000010 /* CTLS */
   #define  RIO_CAR_PEF_EXT_FEATURES            0x00000008 /* EFT_PTR valid */
   #define  RIO_CAR_PEF_ADDR_66                 0x00000004 /* 66 bits */
   #define  RIO_CAR_PEF_ADDR_50                 0x00000002 /* 50 bits */
   #define  RIO_CAR_PEF_ADDR_34                 0x00000001 /* 34 bits */

#define RIO_CAR_SWITCH_PORT             0x14 /* Switch Port Information CAR */
   #define  RIO_CAR_SWP_INFO_PORT_TOTAL_MASK    0x0000ff00     /* Total number of ports */
   #define  RIO_CAR_SWP_INFO_PORT_NUM_MASK      0x000000ff     /* Maintenance transaction port number */
   #define  RIO_CAR_GET_TOTAL_PORTS(x)         ((x & RIO_CAR_SWP_INFO_PORT_TOTAL_MASK) >> 8)
   #define  RIO_CAR_GET_PORT_NUM(x)            (x & RIO_CAR_SWP_INFO_PORT_NUM_MASK)

#define RIO_CAR_SRC_OPERATIONS          0x18
   #define  RIO_CAR_SRC_OPS_READ                0x00008000 /* Read op */
   #define  RIO_CAR_SRC_OPS_WRITE               0x00004000 /* Write op */
   #define  RIO_CAR_SRC_OPS_STREAM_WRITE        0x00002000 /* Str-write op */
   #define  RIO_CAR_SRC_OPS_WRITE_RESPONSE      0x00001000 /* Write/resp op */
   #define  RIO_CAR_SRC_OPS_DATA_MSG            0x00000800 /* Data msg op */
   #define  RIO_CAR_SRC_OPS_DOORBELL            0x00000400 /* Doorbell op */
   #define  RIO_CAR_SRC_OPS_ATOMIC_TST_SWP      0x00000100 /* Atomic TAS op */
   #define  RIO_CAR_SRC_OPS_ATOMIC_INC          0x00000080 /* Atomic inc op */
   #define  RIO_CAR_SRC_OPS_ATOMIC_DEC          0x00000040 /* Atomic dec op */
   #define  RIO_CAR_SRC_OPS_ATOMIC_SET          0x00000020 /* Atomic set op */
   #define  RIO_CAR_SRC_OPS_ATOMIC_CLR          0x00000010 /* Atomic clr op */
   #define  RIO_CAR_SRC_OPS_PORT_WRITE          0x00000004 /* Port-write op */

#define RIO_CAR_DEST_OPERATIONS        0x1c
   #define  RIO_CAR_DST_OPS_READ                0x00008000 /* Read op */
   #define  RIO_CAR_DST_OPS_WRITE               0x00004000 /* Write op */
   #define  RIO_CAR_DST_OPS_STREAM_WRITE        0x00002000 /* Str-write op */
   #define  RIO_CAR_DST_OPS_WRITE_RESPONSE      0x00001000 /* Write/resp op */
   #define  RIO_CAR_DST_OPS_DATA_MSG            0x00000800 /* Data msg op */
   #define  RIO_CAR_DST_OPS_DOORBELL            0x00000400 /* Doorbell op */
   #define  RIO_CAR_DST_OPS_ATOMIC_TST_SWP      0x00000100 /* Atomic TAS op */
   #define  RIO_CAR_DST_OPS_ATOMIC_INC          0x00000080 /* Atomic inc op */
   #define  RIO_CAR_DST_OPS_ATOMIC_DEC          0x00000040 /* Atomic dec op */
   #define  RIO_CAR_DST_OPS_ATOMIC_SET          0x00000020 /* Atomic set op */
   #define  RIO_CAR_DST_OPS_ATOMIC_CLR          0x00000010 /* Atomic clr op */
   #define  RIO_CAR_DST_OPS_PORT_WRITE          0x00000004 /* Port-write op */

#define  RIO_CAR_OPS_READ                       0x00008000 /*  Read op */
#define  RIO_CAR_OPS_WRITE                      0x00004000 /*  Write op */
#define  RIO_CAR_OPS_STREAM_WRITE               0x00002000 /*  Str-write op */
#define  RIO_CAR_OPS_WRITE_RESPONSE             0x00001000 /*  Write/resp op */
#define  RIO_CAR_OPS_DATA_MSG                   0x00000800 /*  Data msg op */
#define  RIO_CAR_OPS_DOORBELL                   0x00000400 /*  Doorbell op */
#define  RIO_CAR_OPS_ATOMIC_TST_SWP             0x00000100 /*  Atomic TAS op */
#define  RIO_CAR_OPS_ATOMIC_INC                 0x00000080 /*  Atomic inc op */
#define  RIO_CAR_OPS_ATOMIC_DEC                 0x00000040 /*  Atomic dec op */
#define  RIO_CAR_OPS_ATOMIC_SET                 0x00000020 /*  Atomic set op */
#define  RIO_CAR_OPS_ATOMIC_CLR                 0x00000010 /*  Atomic clr op */
#define  RIO_CAR_OPS_PORT_WRITE                 0x00000004 /*  Port-write op */

#define RIO_CSR_WRITE_PORT             0x44
#define RIO_CSR_PE_LL_CTRL             0x4c
#define RIO_CSR_LCS_HIGHBAR            0x58
#define RIO_CSR_LCS_BAR                0x5c

#define RIO_CSR_DID                    0x60 /* Base Device ID  */
#define RIO_CSR_HOSTDID_LOCK           0x68 /* Host Base Device ID Lock  */
#define RIO_CSR_COMPONENT_TAG          0x6c /* Component Tag  */

#define RIO_CSR_STD_DESTID_SEL         0x70
   #define RIO_CSR_STD_EXTCFGEN                 0x80000000
#define RIO_CSR_STD_PORT_SEL           0x74
#define RIO_CSR_STD_DEFAULT_PORT       0x78

#define RIO_EXTFE_BASE                 0x0100
#define RIO_EXTENDED_FEATURES_TOP      0xfffC

#define RIO_IMPLEMENTATION_SPACE_BASE  0x010000
#define RIO_IMPLEMENTATION_SPACE_TOP   0xfffffc

#define RIO_EXT_PORT_N_ERR_STS_CSR(a) (0x0058 + (a)*0x20)
   #define  RIO_EXT_PORT_N_ERR_STS_PW_OUT_ES    0x00010000
   #define  RIO_EXT_PORT_N_ERR_STS_PW_INP_ES    0x00000100
   #define  RIO_EXT_PORT_N_ERR_STS_PW_PEND      0x00000010
   #define  RIO_EXT_PORT_N_ERR_STS_PORT_ERR     0x00000004
   #define  RIO_EXT_PORT_N_ERR_STS_PORT_OK      0x00000002
   #define  RIO_EXT_PORT_N_ERR_STS_PORT_UNINIT  0x00000001

#define RIO_EXT_PORT_GEN_CTL_CSR       0x003c
#define  RIO_EXT_PORT_GEN_HOST                  0x80000000
#define  RIO_EXT_PORT_GEN_MASTER                0x40000000
#define  RIO_EXT_PORT_GEN_DISCOVERED            0x20000000

#define RIO_EXT_PORT_N_CTL_CSR(a)     (0x005c + (a)*0x20)
#define  RIO_EXT_PORT_N_CTL_PWIDTH              0xc0000000
#define  RIO_EXT_PORT_N_CTL_PWIDTH_1            0x00000000
#define  RIO_EXT_PORT_N_CTL_PWIDTH_4            0x40000000
#define  RIO_EXT_PORT_N_CTL_P_TYP_SER           0x00000001
#define  RIO_EXT_PORT_N_CTL_LOCKOUT             0x00000002
#define  RIO_EXT_PORT_N_CTL_EN_RX_SER           0x00200000
#define  RIO_EXT_PORT_N_CTL_EN_TX_SER           0x00400000
#define  RIO_EXT_PORT_N_CTL_EN_RX_PAR           0x08000000
#define  RIO_EXT_PORT_N_CTL_EN_TX_PAR           0x40000000
#define  RIO_EXT_PORT_N_EN_RX_TX                0x00600000
#define  RIO_EXT_PORT_N_FORCE_SIGNLE_LANE       0x02000000

/* Register bits */

/* DEVVEND */
/* bits 0..15 => deviceID */
/* bits 16..31 => vendorID */
/* DEVICE_INFO */
/* indicates device revision level */
/* ASSEMBLY_ID */
/* bits 0..15 => assembly ID */
/* bits 16..31 => assembly vendorID */
/* ASSEMBLY_INFO */
/* bits 0..15 => assembly revision */
/* bits 16-31 => extended features pointer */

/* PE_FEATURES */

#define	RIO_FEATURES_BRIDGE             0x0001
#define RIO_FEATURES_MEMORY             0x0002
#define RIO_FEATURES_PROCESSOR          0x0004
#define RIO_FEATURES_SWITCH             0x0008
#define RIO_FEATURES_EXTCAP             0x1000
#define RIO_FEATURES_EXTADDR_34         0x2000
#define RIO_FEATURES_EXTADDR_50         0x4000
#define RIO_FEATURES_EXTADDR_66         0x8000
#define RIO_FEATURES_EXTADDR_34_50      0x6000
#define RIO_FEATURES_EXTADDR_34_66      0xa000
#define RIO_FEATURES_EXTADDR_34_50_66   0xe000
#define RIO_FEATURES_EXTADDR_MASK       0xe000

/* SWITCH_PORT */

#define RIO_SWITCH_PORT_MASK            0x00ff0000
#define RIO_SWITCH_PORT_COUNT(x)       ((x) & RIO_SWITCH_PORT_MASK) >> 16)

/* the port number from which the maintenance read operation occurred */

#define RIO_SWITCH_PORT_SRC_MASK        0xff000000
#define RIO_SWITCH_PORT_ACCESS(x)      ((x) & RIO_SWITCH_PORT_SRC_MASK) >> 24)

/* SRC_OPERATIONS and DEST_OPERATIONS */

/* read operation */
#define RIO_OP_SUPPORTED_READ           0x00010000
/* write operation */
#define RIO_OP_SUPPORTED_WRITE          0x00020000
/* streaming write operation */
#define RIO_OP_SUPPORTED_SWRITE         0x00040000
/* write with response operation */
#define RIO_OP_SUPPORTED_RWRITE         0x00080000
/* atomic test and swap operation */
#define RIO_OP_SUPPORTED_ATSWAP         0x00800000
/* atomic increment */
#define RIO_OP_SUPPORTED_AINC           0x01000000
/* atomic decrement */
#define RIO_OP_SUPPORTED_ADEC           0x02000000
/* atomic set */
#define RIO_OP_SUPPORTED_ASET           0x04000000
/* atomic clear */
#define RIO_OP_SUPPORTED_ACLEAR         0x08000000
/* port-write */
#define RIO_OP_SUPPORTED_PWRITE         0x20000000

/* WRITE_PORT */

#define RIO_WPCSR_AVAILABLE             0x01000000
#define RIO_WPCSR_FULL                  0x02000000
#define RIO_WPCSR_EMPTY                 0x04000000
#define RIO_WPCSR_BUSY                  0x08000000
#define RIO_WPCSR_FAILED                0x10000000
#define RIO_WPCSR_ERROR                 0x20000000

/*
 * RapidIO keeps the configuration space address
 * in pRegBase[RIO_CFG_INDEX].
 */

#define RIO_CFG_INDEX                5

#define RIO_CHANNEL_RESERVED (UINT32)(-1)
#define RIO_CHANNEL_UNRESERVED       0
#define RIO_CHANNEL_MAINT            1
#define RIO_CHANNEL_SM               2
#define RIO_CHANNEL_TAS_SET          3
#define RIO_CHANNEL_TAS_CLEAR        4
#define RIO_CHANNEL_DOORBELL         5
#define RIO_CHANNEL_CFG              6

#define RIO_TGTID_GENERIC            0

#define MAX_NUM_RIO_TARGETS          10
#define RIO_MAX_OUTBOUND_CHANNELS    9
#define RIO_MAX_INBOUND_CHANNELS     5
#define RIO_MAX_CHANNELS             15
#define RIO_DIRECTION                2
#define RIO_DIRECTION_OUTBOUND       0
#define RIO_DIRECTION_INBOUND        1

#define RIO_BUS_ADRS_BASE            0x80000000
#define RIO_BUS_SM_ADRS_BASE         RIO_BUS_ADRS_BASE
#define RIO_BUS_ADRS_SIZE            0x40000000

#define RIO_SM_WIN_SIZE_1M           0x0100000
#define RIO_SM_WIN_SIZE_2M           0x0200000
#define RIO_SM_WIN_SIZE_4M           0x0400000
#define RIO_SM_WIN_SIZE_8M           0x0800000
#define RIO_SM_WIN_SIZE_16M          0x1000000
#define RIO_SM_WIN_SIZE_32M          0x2000000
#define RIO_SM_WIN_SIZE_64M          0x4000000
#define RIO_SM_WIN_SIZE_128M         0x8000000

/*the general default device ID assigned to non-host and non-boot code end*/

#define RIO_ANY_DESTID               0xFFFF

/* the default device ID assigned to boot code devices */

#define RIO_BOOT_DFLT_DID            0xFFFE

/* the default device ID assigned to host devices */

#define RIO_HOST_DFLT_DID            0
#define RIO_SUCCESS                  0
#define RIO_ACQUIRE_LOCK_RETRY       20

/* support 16 rapidio host port */

#define RIO_MAX_PORT                 16

/* Host base device ID lock has taken by other host */

#define RIO_DID(size, x) (size ? (x & 0xffff) : ((x & 0x000000ff) << 16))

#define RIO_MAX_ROUTE_INDEX(size) (size ? (1 << 16) : (1 << 8))

#define RIO_GBLTABLE                 0xFF
#define RIO_INVROUTE                 0xFF

#define RIO_DBELL_DATA_LEN           2

/* typedefs */

enum RIO_TYPE
    {
    RIO_TYPE0 = 0,
    RIO_TYPE1,
    RIO_TYPE2,
    RIO_TYPE3,
    RIO_TYPE4,
    RIO_TYPE5,
    RIO_TYPE6,
    RIO_TYPE7,
    RIO_TYPE8,
    RIO_TYPE9,
    RIO_TYPE10,
    RIO_TYPE11,
    RIO_TYPE_NUM,
    RIO_TYPE_DSTR =  RIO_TYPE9,
    RIO_TYPE_DBELL = RIO_TYPE10,
    RIO_TYPE_MBOX =  RIO_TYPE11
    };

/* information structure used for driver registration */

typedef struct vxbRapidIOID
    {
    UINT32  regBase;  /* used for device recognition */
    UINT32  devID;
    UINT32  devVend;
    UINT32  assembly;
    UINT32  devType;
    UINT32  hopCount;
    UINT32  cfgBase;
    } RAPIDIO_DEVVEND;

/* registration structure passed to VxBus */

typedef struct vxbRapidIORegInfo
    {
    struct vxbDevRegInfo  b;
    int                   idListLen;
    struct vxbRapidIOID * idList;
    } RAPIDIO_DRIVER_REGISTRATION;

typedef struct rio_channel
    {
    UINT32 channelType;
    UINT32 channelDirection;
    UINT32 channelNumber;
    } RIO_CHANNEL_INFO;

typedef struct rioCpuInfo
    {
    VXB_DEVICE_ID devID;
    struct vxbRapidIODevInfo * pDevInfo;
    } RIO_CPU_INFO;

typedef struct rioParameter
    {
    UINT16 destId;
    UINT8  transType;
    UINT8  mbox;
    UINT8  letter;
    UINT8  mboxObChan;
    } RIO_TRANS_PARAMETER;

typedef struct rioSwitchInfo
    {
    VXB_DEVICE_ID devID;
    struct vxbRapidIODevInfo * pDevInfo;
    } RIO_SWITCH_INFO;

typedef struct rioSwitch
    {
    UINT8 * routeTable;
    STATUS (*addEntry) (VXB_DEVICE_ID pDev, UINT16 destid, UINT8 hopcount,
        UINT16 table, UINT16 routeDestid, UINT8 routePort);
    STATUS (*getEntry) (VXB_DEVICE_ID pDev, UINT16 destid, UINT8 hopcount,
        UINT16 table, UINT16 routeDestid, UINT8 *routePort);
    STATUS (*clrTable) (VXB_DEVICE_ID pDev, UINT16 destid, UINT8 hopcount,
        UINT16 table);
    } VXB_RIO_SWITCH;

/* common RapidIO operation routines */

typedef struct vxbRioOps
    {
    STATUS (*localRead4) (VXB_DEVICE_ID pDev, UINT32 offset, UINT32 *data);

    STATUS (*localWrite4) (VXB_DEVICE_ID pDev, UINT32 offset, UINT32 data);

    STATUS (*maintRead4) (VXB_DEVICE_ID pDev, UINT32 destid, UINT32 hopcount,
        UINT32 offset, UINT32 *data);

    STATUS (*maintWrite4) (VXB_DEVICE_ID pDev, UINT32 destid, UINT32 hopcount,
        UINT32 offset, UINT32 data);

    void (*mmapInBoundWinset)(VXB_DEVICE_ID pDev, void * bar, UINT32 winIdx,
        void * busAdrs, UINT32 size, void * loAdrs, UINT32 rType, UINT32 wType);

    void (*mmapOutBoundWinset)(VXB_DEVICE_ID pDev, void * bar, UINT32 winIdx,
        void * loAdrs, UINT32 size, UINT32 destID, void * busAdrs, UINT32 rType,
        UINT32 wType);

    STATUS (*dbelSend) (VXB_DEVICE_ID pDev, UINT16 destid, UINT16 data);
    } VXB_RIO_OPS;

typedef struct vxbRapidIODevInfo
    {
    HCF_DEVICE *       pHcfDevice;
    UINT32             devID;
    UINT32             devVend;
    UINT32             assembly;
    UINT32             devType;
    UINT32             hopCount;
    UINT32             cfgBase;
    UINT32             devUnit;
    UINT32             targetID;
    UINT32             devInfo;
    UINT32             assenblyInfo;
    UINT16             efPtr;
    UINT32             peFeature;
    UINT32             swpInfo;
    UINT32             srcOps;
    UINT32             dstOps;
    UINT32             compTag;
    UINT32             physEFptr;
    UINT32             emEFptr;
    RIO_CHANNEL_INFO * pChannelInfo;
    RIO_CPU_INFO     * pCpuInfo;
    VXB_RIO_SWITCH     rioSwitch;
    VXB_RIO_OPS      * pBusMethod;
    VXB_DEVICE_ID      busCtrlID;
    } RAPIDIO_DEVICE_INFORMATION;

typedef struct rioWindowAlloc
    {
    UINT32 targetID;
    UINT32 channelType;
    UINT32 channelDirection;
    UINT32 channelNumber;
    VXB_DEVICE_ID cpuID;
    } RIO_WINDOW_ALLOC;

typedef struct
    {
    UINT32 targetID;
    UINT32 channelNumber;
    VXB_DEVICE_ID cpuID;
    } RIO_CFG_INFO;

enum rioPhyType
    {
    RIO_PHY_PARALLEL,
    RIO_PHY_SERIAL
    };

typedef struct rioHport
    {
    VXB_DEVICE_ID   busCtlrID; /* host bus controller ID */
    VXB_RIO_OPS *   pBusAccess;/* basic access method */
    UINT32          portnum;
    UINT32          host;      /* Host Device or Slave device */
    UINT32          systemSize;/* 0 - 256 devices, 1-  65536 devices. */
    enum rioPhyType phyType;   /* RapidIO phy type */
    } RIO_MPORT_INFO, * pRIO_MPORT_INFO;

/* forward declarations */

IMPORT int    vxbRapidIOCfg (VXB_DEVICE_ID, VXB_ACCESS_LIST *,
           STATUS (*busOverride)(VXB_DEVICE_ID ));
IMPORT STATUS vxbRapidIOHcfTableOverride (VXB_DEVICE_ID);
IMPORT STATUS rapidIoRegister ();
IMPORT STATUS vxbRapidIOBusTypeInit (VXB_DEVICE_ID);
IMPORT STATUS rioBusAnnounceDevices (VXB_DEVICE_ID);
IMPORT STATUS vxbRioAcquireLock (VXB_DEVICE_ID, UINT32, UINT32, UINT32);

#endif /* __INC_vxbRapidIOH */

