/* vxbPciBus.h - PCI Bus type header file */

/*
 * Copyright (c) 2005-2009 Wind River Systems, Inc.
 *
 * The right to copy, distribute or otherwise make use of this software
 * may be licensed only pursuant to the terms of an applicable Wind River
 * license agreement.
 */

/* 
modification history
--------------------
01g,21apr09,h_k  updated for LP64.
                 moved vxbPciConfigBdfPack, vxbPciBusTypeInit, vxbPciIntLibInit
                 vxbPciConfigLibInit, vxbPciAutoConfig, vxbPciIntConnect to
                 vxbPciLib.h.
01f,04mar09,h_k  updated for LP64 support. (CQ:160416)
01e,10mar08,h_k  updated vxbPciConfigLibInit().
01d,02aug07,h_k  changed pciIntrVectorInfo and pciIntrEntry to typedefs.
01c,07feb07,dtr  Add some function declarations.
01b,22sep05,pdg  Fix for pci interrupts not working (SPR #112678)
01a,27jul05,pdg  written
*/

#ifndef __INCvxbPciBush
#define __INCvxbPciBush

#ifdef __cplusplus
extern "C" {
#endif

#include <hwif/vxbus/vxbPciLib.h>

/*
 * struct pciIntrVectorInfo
 * Structure holding the details of the vector like the vector address
 * and the index which can be used by the upstream bus
 * for identifying the vector.
 */

typedef struct pciIntrVectorInfo
    {
    VOIDFUNCPTR * intVector;  /* vector address */
    UINT32          index;      /* index into the lower bus */
    } PCI_INTR_VECTOR_INFO;

/*
 * struct pciIntrEntry
 * Structure holding the interrupt information like the number of
 * supported vectors and a pointer to the array of vector information
 * structures. The size of the array is dependent on the number of vectors.
 */

typedef struct pciIntrEntry
    {
    int             numVectors; /* number of vectors */
    struct pciIntrVectorInfo	intVecInfo[1]; /* interrupt info array */
    } PCI_INTR_ENTRY;

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbPciBush */

