/* usbUhcdSupport.h - header file for USB UHCD HCD register access*/

/*
 * Copyright (c) 2003-2010 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2003-2010 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
Modification history
--------------------
01n,18may10,w_x  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
01m,13jan10,ghs  vxWorks 6.9 LP64 adapting
01l,20mar09,w_x  Corrected USB_UHCD_DWORD_IN, added USB_UHCD_WRITE/READ_BYTE
01k,04jun08,w_x  Added HC halt/restart macros;
                 added comments for updating QH and TD fields;
                 removed usbUhcdJobQueueAdd;
                 removed redundant vxbAccess.h #include;
                 (WIND00121282 fix)
01j,07aug07,jrp  Register access method change
01i,09aug07,jrp  Moving usbUhcdJobQueueAdd to this file
01h,27mar07,sup  use the read/write general routines. The flag is now passed
                 as a parameter
01g,22mar07,sup  Edited register access for proper flag value
01f,07oct06,ami  Changes for USB-vxBus porting
01e,28mar05,pdg  non-PCI changes
01d,05oct04,mta  SPR100704- Removal of floating point math
01c,05oct04,mta  SPR100704- Removal of floating point math
01b,26jun03,amn  changing the code to WRS standards.
01a,25apr03,ram  written.
*/

/*
DESCRIPTION

This file contains the constants and function prototypes
which are used for accessing the registers.
*/

/*
INTERNAL
 *******************************************************************************
 * Filename         : usbUhcdSupport.h
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      :  This file contains the constants and function prototypes
 *                     which are used for accessing the registers.
 *
 *
 */

#ifndef __INCusbUhciSupporth
#define __INCusbUhciSupporth

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include <hwif/vxbus/vxBus.h>
#include <../src/hwif/h/vxbus/vxbAccess.h>
#include <usb/usbOsal.h>
#include "usbUhcdScheduleQueue.h"

/* defines */

#define USB_UHCD_DWORD_OUT(pDEVICE , OFFSET, VALUE)                    \
    vxbWrite32 (((pUSB_UHCD_DATA)(pDEVICE->pDrvCtrl))->pRegAccessHandle,   \
        (VOID*)(((pUSB_UHCD_DATA)(pDEVICE->pDrvCtrl))->regBase + OFFSET),  \
            VALUE)

#define USB_UHCD_DWORD_IN(pDEVICE, OFFSET)                             \
    vxbRead32 (((pUSB_UHCD_DATA)(pDEVICE->pDrvCtrl))->pRegAccessHandle,    \
        (VOID*)(((pUSB_UHCD_DATA)(pDEVICE->pDrvCtrl))->regBase + OFFSET))

#define USB_UHCD_WRITE_WORD(pDEVICE, OFFSET, VALUE)                    \
    vxbWrite16 (((pUSB_UHCD_DATA)(pDEVICE->pDrvCtrl))->pRegAccessHandle,   \
        (VOID*)(((pUSB_UHCD_DATA)(pDEVICE->pDrvCtrl))->regBase + OFFSET),  \
        VALUE)

#define USB_UHCD_READ_WORD(pDEVICE, OFFSET)                            \
    vxbRead16 (((pUSB_UHCD_DATA)(pDEVICE->pDrvCtrl))->pRegAccessHandle,    \
        (VOID*)(((pUSB_UHCD_DATA)(pDEVICE->pDrvCtrl))->regBase + OFFSET))


#define USB_UHCD_WRITE_BYTE(pDEVICE, OFFSET, VALUE)                    \
    vxbWrite8 (((pUSB_UHCD_DATA)(pDEVICE->pDrvCtrl))->pRegAccessHandle,    \
        (VOID*)(((pUSB_UHCD_DATA)(pDEVICE->pDrvCtrl))->regBase + OFFSET),  \
        VALUE)

#define USB_UHCD_READ_BYTE(pDEVICE, OFFSET)                            \
    vxbRead8 (((pUSB_UHCD_DATA)(pDEVICE->pDrvCtrl))->pRegAccessHandle,     \
        (VOID*)(((pUSB_UHCD_DATA)(pDEVICE->pDrvCtrl))->regBase + OFFSET))

/* Interrupt status values */

#define USB_UHCD_COMPLETION_INT                0x01
#define USB_UHCD_TRANSACTION_ERR               0x02
#define USB_UHCD_TRANSACTION_ERR_IOC           0x03
#define USB_UHCD_RESUME_DETECT                 0x04
#define USB_UHCD_HST_SYSTEM_ERR                0x08
#define USB_UHCD_HC_PROCESS_ERR                0x10
#define USB_UHCD_HC_HALTED                     0x20


#ifdef __USE_UHCD_MACRO__

#define usbUhcdUpdateReg  USB_UHCD_UPDATE_REG

#define usbUhcdRegxSetBit USB_UHCD_REG_SET_BIT

#define usbUhcdRegxClearBit USB_UHCD_REG_CLEAR_BIT

#define usbUhcdPortSetBit USB_UHCD_PORT_SET_BIT

#define usbUhcdPortClearBitWriting1 USB_UHCD_PORT_CLEAR_BIT_WTITING_1

#define usbUhcdIsBitSet USB_UHCD_IS_BIT_SET

#define usbUhcdIsBitReset USB_UHCD_IS_BIT_RESET

#define usbUhcdGetBit USB_UHCD_GET_BIT

#else

/* function declarations */

/***************************************************************************
 * Function Name    : usbUhcdHcreset
 * Description      : This function is used to reset the HC.
 * Parameters       : void.
 * Return Type      : N/A
 */

extern void usbUhcdHcReset (pUSB_UHCD_DATA pHCDData);

/***************************************************************************
 * Function Name    : usbUhcdRegxSetBit
 * Description      : This function is used to set a bit of the UHCD's register.
 * Parameters       : usbUhcdRegBaseAddr IN Offset from the base address
 *                    setBit             IN Bit number which is to be set
 * Return Type      : N/A
 */

extern void usbUhcdRegxSetBit (pUSB_UHCD_DATA pHCDData,
                               UINT8 usbUhcdRegBaseOffset,
                               UINT8 setBit);

/***************************************************************************
 * Function Name    : usbUhcdRegxClearBit
 * Description      : This function is used to clear a bit of the UHCD's
 *                    register.
 * Parameters       : usbUhcdRegBaseAddr  IN Offset from the base address
 *                    clearBit            IN Bit number which is to be cleared
 * Return Type      : N/A
 */

extern void usbUhcdRegxClearBit (pUSB_UHCD_DATA pHCDData,
                                 UINT8 usbUhcdRegBaseOffset,
                                 UINT8 clearBit);

/***************************************************************************
 * Function Name    : usbUhcdPortSetBit
 * Description      : This function is used to set a bit of the UHCD's port
 *                    register
 * Parameters       : usbUhcdRegBaseAddr IN Offset from the base address
 *                    setBit             IN Bit number which is to be set
 * Return Type      : N/A
 */

extern void usbUhcdPortSetBit (pUSB_UHCD_DATA pHCDData,
                               UINT8 usbUhcdRegBaseOffset,
                               UINT8 setBit);

/***************************************************************************
 * Function Name    : usbUhcdPortClearBit
 * Description      : This function is used to clear a bit of the UHCD's port
 *                    register.
 * Parameters       : usbUhcdRegBaseAddr  IN Offset from the base address
 *                    clearBit            IN Bit number which is to be cleared
 * Return Type      : void
 */

extern void usbUhcdPortClearBit (pUSB_UHCD_DATA pHCDData,
                                 UINT8 usbUhcdRegBaseOffset,
                                 UINT8 clearBit);

/***************************************************************************
 * Function Name    : usbUhcdIsBitSet
 * Description      : This function is used to check whether a bit is set
 *                    in the UHCD's register.
 * Parameters       : usbUhcdRegBaseAddr  IN Offset from the base address
 *                    which_bit           IN Bit number which is to be checked
 * Return Type      : BOOLEAN
 *                    Returns
 *                       TRUE  - If bit is set.
 *                       FALSE - If bit is not set.
 */

extern BOOLEAN usbUhcdIsBitSet (pUSB_UHCD_DATA pHCDData,
                                UINT8 usbUhcdRegBaseOffset,
                                UINT8 whichBit);

/***************************************************************************
 * Function Name    : usbUhcdIsBitReset
 * Description      : This function is used to check whether a bit is
 *                    reset in the UHCD's register
 * Parameters       : usbUhcdRegBaseAddr  IN Offset from the base address
 *                    which_bit           IN Bit number which is to be checked
 * Return Type      : BOOLEAN
 *                    Returns
 *                       TRUE  - If bit is reset
 *                       FALSE - If bit is not reset
 */

extern BOOLEAN usbUhcdIsBitReset (pUSB_UHCD_DATA pHCDData,
                                  UINT8 usbUhcdRegBaseOffset,
                                  UINT8 whichBit);

/***************************************************************************
 * Function Name    : usbUhcdGetBit
 * Description      : This function is used to get the bit value of the
 *                    UHCD's register.
 * Parameters       : usbUhcdRegBaseAddr  IN Offset from the base address
 *                    which_bit           IN Bit number whose value is to be
 *                                           retrieved.
 * Return Type      : UINT8
 *                    Returns the value at the bit specified
 */

extern UINT8 usbUhcdGetBit (pUSB_UHCD_DATA pHCDData,
                            UINT8 usbUhcdRegBaseOffset,
                            UINT8 whichBit);


/*******************************************************************************
 * Function Name    : usbUhcdReadReg
 * Description      : This function is used to read a value from the UHCD's register.
 * Parameters       : pHCDData IN pointer to UHC Data Structure
 *                    usbUhcdRegBaseOffse IN Offset from the base address
 * Return Type      : INT16
 * Global Variables : uhcUhcdUsbBase.
 * Calls            : UHCD_READ_WORD
 *                    UHCD_WRITE_WORD
 * Called by        : None
 * To Do            : None
 ******************************************************************************/
extern INT16 usbUhcdReadReg(pUSB_UHCD_DATA pHCDData, UINT8 usbUhcdRegBaseOffset );

/*******************************************************************************
*
* usbUhcdWriteReg - write a value to the UHCD's register.
*
* This routine writes a value to the UHCD's register.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbUhcdWriteReg
    (
    pUSB_UHCD_DATA pHCDData,
    UINT8 usbUhcdRegBaseOffset,
    UINT16 valueToWrite
    );

/*******************************************************************************
 * Function Name    : usbUhcdIsBitSetReg
 * Description      : This function is used to check whether a bit is set
 *                    of the given value.
 * Parameters       : regValue           IN any Value
 *                    whichBit           IN Bit number which is to be checked
 * Return Type      : BOOLEAN
 *                    Returns
 *                       TRUE  - If bit is set.
 *                       FALSE - If bit is not set.
 * Calls            : None
 *
 * Called by        : None
 * To Do            : None
 ******************************************************************************/

/***************************************************************************
*
* usbUhcdIsBitSetReg - check whether a bit is set of the UHCI's register
*
* RETURNS: TRUE, FALSE if bit is not set
*/

extern BOOLEAN usbUhcdIsBitSetReg(UINT16 regValue,UINT8 whichBit);

/*
* Note for updating QH or TD elements:
*
* There is a fact that UHCI doesn't have a way to temporarily disable a specific
* schedule and enable it again as EHCI, so updating TD or QH would have chance
* to occur concurrently with the host controller; We can work around this by halting
* the whole host controller and do the updates and enable it again, but that may cause
* some performance down. However, we haven't seen any issue without halting it;
* so we just keep the normal way.
*
* If you really cares about this consideration, you can try to enable the following
* macro to add support for halt/restart mechnism.
*/
/* #define USB_UHCD_ENABLE_HC_HALT_RESTART_MECHANISM */
#ifdef USB_UHCD_ENABLE_HC_HALT_RESTART_MECHANISM
/*******************************************************************************
*
* USB_UHCD_HALT_HC - Halt the HC hardware
*
* This routine is to halt the HC hardware, this is an attempt to disable the processing
* before a QH or TD update.
*
* Note: However, it is currently not used since UHCI seems not  too much depend on this.
*
* RETURNS: NONE
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/
#define USB_UHCD_HALT_HC(pHCDData)                                 \
    {                                                              \
    /* Stop the HC */                                              \
    usbUhcdRegxClearBit((pHCDData), USB_UHCD_USBCMD, 0);           \
    /* Wait until the current transaction is completed */          \
    while(!usbUhcdIsBitSet ((pHCDData), USB_UHCD_USBSTS, 5)){};    \
    }

/*******************************************************************************
*
* USB_UHCD_RESTART_HC - restart the HC hardware from a halt
*
* This routine is to un-halt the HC hardware, this is an attempt to enable the processing
* after a QH or TD update.
*
* Note: However, it is currently not used since UHCI seems not  too much depend on this.
*
* RETURNS: NONE
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/
#define USB_UHCD_RESTART_HC(pHCDData)                              \
    {                                                              \
    /* Restart the HC */                                           \
    usbUhcdRegxSetBit((pHCDData), USB_UHCD_USBCMD, 0);             \
    /* Wait until the current transaction is completed */          \
    while(usbUhcdIsBitSet ((pHCDData), USB_UHCD_USBSTS, 5)){};     \
    }
#else
#define USB_UHCD_HALT_HC(pHCDData)
#define USB_UHCD_RESTART_HC(pHCDData)
#endif
#endif

/***************************************************************************
*
* usbUhciDelay -
*
* RETURNS: N/A
*/
extern void usbUhciDelay(pUSB_UHCD_DATA pHCDData,
                         UINT16     uDelay);

/***************************************************************************
*
* Function Name    : usbUhciLog2
* Description      : This function is used to calculate log to the base 2
*
* Return Type      : UINT32
*                    Returns the log value to the base 2
*/
extern UINT32 usbUhciLog2(UINT32 value);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCusbUhcdSupporth */

