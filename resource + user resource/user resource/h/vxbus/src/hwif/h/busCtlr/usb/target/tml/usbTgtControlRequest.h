/* usbTgtControlRequest.h - Definitions of USB Target Control Request Module */

/*
 * Copyright (c) 2010-2012 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
Modification history
--------------------
01f,18may12,s_z  Add support for USB 3.0 target (WIND00326012)
01e,08apr11,ghs  Fix code coverity issue(WIND00264893)
01d,28mar11,s_z  Add the definition of USBTGT_TCD_VALIDATION_RETURN_STATUS
01c,22mar11,s_z  Code clean up based on the code review
01b,08mar11,s_z  Code clean up
01a,17may10,s_z  created
*/

/*
DESCRIPTION

*/

#ifndef INCusbTgtControlRequesth
#define INCusbTgtControlRequesth


#ifdef __cplusplus
extern "C" {
#endif


IMPORT BOOL usbTgtControlErpUsedFlagSet
    (
    pUSBTGT_TCD pTcd
    );

IMPORT void usbTgtControlErpUsedFlagClear
    (
    pUSBTGT_TCD pTcd
    );

IMPORT VOID usbTgtInitSetupErp
    (
    pUSBTGT_TCD pTcd
    );

IMPORT VOID usbTgtInitDataErp
    (
    pUSBTGT_TCD         pTcd,         /* TCD pointer */
    pVOID               pFuncSpecific,/* Function driver specific pointer */
    pUINT8              pBuf,         /* Buffer for data */
    UINT16              ulength,      /* Data length */
    ERP_CALLBACK        userCallback, /* USB Target Applcaition Callback */
    BOOL                isDataIn,     /* Data In stage */
    USB_ERP_FLAGS       erpFlags      /* The ERP transaction flags */
    );

IMPORT VOID usbTgtInitStatusErp
    (
    pUSBTGT_TCD pTcd,
    BOOL        isStatusIn
    );

IMPORT STATUS usbTgtSubmitControlErp
    (
    pUSBTGT_TCD pTcd
    );

IMPORT STATUS usbTgtControlPipeStall
    (
    pUSBTGT_TCD pTcd /* The TCD pointer */
    );

#define USBTGT_TCD_VALIDATION_RETURN_STATUS(pTcd)                        \
    do {                                                                 \
    if (NULL == pTcd)                                                    \
        {                                                                \
        USB_TGT_ERR("Invalid parameter pTcd is NULL\n",                  \
                    1, 2, 3, 4, 5, 6);                                   \
        return ERROR;                                                    \
        }                                                                \
    }while(0)



#ifdef __cplusplus
}
#endif

#endif /* INCusbTgtControlRequesth */

