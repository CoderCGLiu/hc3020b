/* usbMhdrcDebug.h - MHCI debug message macros definition */

/*
 * Copyright (c) 2010 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,16aug10,w_x  VxWorks 64 bit audit and warning removal
01a,02jun10,s_z  create
*/

/*
DESCRIPTION

This contains the debug message macros which are used by the Mentor Graphics
USB Host Controller Driver.

*/
#ifndef __INCusbMhdrcDebug
#define __INCusbMhdrcDebug

#ifdef __cplusplus
extern "C" {
#endif

#include <vsbConfig.h>
#include <usb/usbOsalDebug.h>
#include <usb/usbd.h>

IMPORT void usbMhdrcHcdRequestShow
        (
        pUSB_MHCD_REQUEST_INFO pRequestInfo
        );

IMPORT void usbMhdrcHcdRequestListShow
        (
        LIST * pList,
        UINT8  uShowLevel
        );

IMPORT void usbMhdrcHcdPipeShow
        (
        pUSB_MHCD_PIPE  pHCDPipe,
        UINT8           uShowLevel
        );

IMPORT void usbMhdrcHcdPipetListShow
        (
        LIST * pList,
        UINT8  uShowLevel
        );

IMPORT void usbMhdrcHcdDeviceInfoShow
        (
        pUSBD_DEVICE_INFO pDeviceInfo
        );

IMPORT void usbMhdrcHcdDriverDataShow
        (
        pUSBHST_DEVICE_DRIVER pDriverData
        );

IMPORT void usbMhdrcHcdRegisterWrite
        (
        UINT8   uBusIndex, /* Bus Index */
        UINT32  uOffset,   /* Register offset */
        UINT32  uValue,    /* value to be write */
        UINT8   uBytes     /* 1  byte, 2 wolds , 4 dwords*/
        );

IMPORT void usbMhdrcHcdRegisterRead
        (
        UINT8   uBusIndex, /* Bus Index */
        UINT32  uOffset,   /* Register offset */
        UINT8   uBytes     /* 1  byte, 2 wolds , 4 dwords*/
        );

IMPORT void usbMhdrcHcdRegisterReadP
        (
        UINT8   uBusIndex, /* Bus Index */
        UINT32  uOffset    /* Register offset */
        );

IMPORT void usbMhdrcHcdRegisterWriteP
        (
        UINT8    uBusIndex, /* Bus Index */
        UINT32   uOffset,   /* Register offset */
        UINT32   uValue     /* Value to be write */
        );

IMPORT void usbMhdrcHcdRegShow
        (
        UINT8   uBusIndex /* Bus Index */
        );

IMPORT void usbMhdrcHcdDataShow
        (
        UINT8 uShowLevel
        );

#ifdef __cplusplus
}
#endif

#endif /* __INCusbMhdrcDebug */


