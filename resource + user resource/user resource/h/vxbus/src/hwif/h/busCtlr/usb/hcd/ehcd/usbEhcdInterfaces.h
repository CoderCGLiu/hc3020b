/* ehcd_Interfaces.h - Utility Functions for EHCI */

/*
 * Copyright (c) 2004, 2010 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2004, 2010 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
modification history
--------------------
01b,18may10,w_x  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
01a,13jan10,ghs  vxWorks 6.9 LP64 adapting
*/

/*
DESCRIPTION
This contains the interfaces which are registered with
the USBD.
*/

/*
INTERNAL
 *******************************************************************************
 * Filename         : usbEhcdInterfaces.h
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 *
 * Description      :  This contains the interfaces which are registered with
 *                     the USBD.
 *
 *
 ******************************************************************************/
#ifndef __INCusbEhcdInterfacesh
#define __INCusbEhcdInterfacesh

#ifdef  __cplusplus
extern "C" {
#endif

extern USBHST_STATUS usbEhcdPipeControl
    (
    UINT8  uBusIndex,
    ULONG  uPipeHandle,
    USBHST_PIPE_CONTROL_INFO * pPipeCtrl
    );

extern USBHST_STATUS  usbEhcdCreatePipe
    (
    UINT8   uBusIndex,
    UINT8   uDeviceAddress,
    UINT8   uDeviceSpeed,
    UCHAR   * pEndpointDescriptor,
    UINT16  uHighSpeedHubInfo,
    ULONG * puPipeHandle
    );

extern USBHST_STATUS  usbEhcdDeletePipe
    (
    UINT8   uBusIndex,
    ULONG   uPipeHandle
    );

extern USBHST_STATUS  usbEhcdModifyDefaultPipe
    (
    UINT8   uBusIndex,
    ULONG   uDefaultPipeHandle,
    UINT8   uDeviceSpeed,
    UINT8   uMaxPacketSize,
    UINT16   uHighSpeedHubInfo
    );

extern USBHST_STATUS  usbEhcdIsBandwidthAvailable
    (
    UINT8       uBusIndex,
    UINT8       uDeviceAddress,
    UINT8   uDeviceSpeed,
    UCHAR  * pCurrentDescriptor,
    UCHAR  * pNewDescriptor
    );

extern USBHST_STATUS usbEhcdSubmitURB
    (
    UINT8   uBusIndex,
    ULONG   uPipeHandle,
    pUSBHST_URB pURB
    );

extern USBHST_STATUS usbEhcdCancelURB
    (
    UINT8   uBusIndex,
    ULONG   uPipeHandle,
    pUSBHST_URB pURB
    );

extern USBHST_STATUS  usbEhcdIsRequestPending
    (
    UINT8    uBusIndex,
    ULONG    uPipeHandle
    );

extern USBHST_STATUS usbEhcdGetFrameNumber
    (
    UINT8   uBusIndex,
    UINT16  * puFrameNumber
    );

extern USBHST_STATUS usbEhcdSetBitRate
    (
    UINT8   uBusIndex,
    BOOL    bIncrement,
    UINT32  * puCurrentFrameWidth
    );


extern USBHST_STATUS   usbEhcdResetTTRequestComplete
    (
    UINT8  uRelativeBusIndex,
    VOID *        pContext,
    USBHST_STATUS nStatus
    );

extern USBHST_STATUS   usbEhcdClearTTRequestComplete
    (
    UINT8       uRelativeBusIndex,
    VOID *        pContext,
    USBHST_STATUS nStatus
    );

#ifdef  __cplusplus
}
#endif

#endif /* End of __INCusbEhcdInterfacesh */
/********************** End of file __INCehcdInterfacesh***********************/
