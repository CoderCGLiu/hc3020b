/* usbUhcdScheduleQueue.h - header for scheduling and queuing USB UHCD HCD */

/*
 * Copyright (c) 2003, 2005-2012 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2003, 2005-2012 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
Modification history
--------------------
01t,05nov12,ljg  Remove member halted from USB_UHCD_QH (WIND00386978)
01s,07jan11,ghs  Clean up compile warnings (WIND00247082)
01r,18may10,w_x  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
01q,13jan10,ghs  vxWorks 6.9 LP64 adapting
01p,18sep09,w_x  Correct USB_UHCD_CONVERT_TO_BUS_MEM called with invalid
                 address issue (WIND00182432)
01o,20jun09,w_x  Correct modification time for WIND00156002
01n,20mar09,w_x  Removed usbUhcdSemCheckPort;
                 Added magic in USB_UHCD_DATA to watch out for shared interrupt;
                 Added forceReconnect[] in USB_UHCD_DATA (WIND00105458)
01m,20mar09,w_x  Change the non-isoc request queue management (WIND00156002)
01l,15feb09,w_x  Move spinLockIsrUhci[] into USB_UHCD_DATA as spinLockIsr
01k,14aug08,w_x  Remove unnecessary port manage task and clean up root hub
                 emulation code (WIND00130272)
01j,04jun08,w_x  Added pRequestQueue in TD structure;
                 Adjusted some element offset of TD and QH;
                 (WIND00121282 fix)
01i,30aug07,adh  Defect WIND00089969 fix
01h,31aug07,p_g  Added code for Non-PCI host controller over vxBus
01g,07aug07,jrp  Register access method change
01f,07oct06,ami  Changes for USB-vxBus porting
01e,28mar05,pdg  non-PCI changes
01d,03mar05,mta  SPR 96604
01c,18jan05,ami  New member outTokScheduled added (SPR #104979 added)
01b,26jun03,mat  changing the code to WRS standards.
01a,25apr03,ram  written.
*/

/*
DESCRIPTION

This file contains the data structures maintained by the USB UHCD and the HCD
and the prototypes of the functions used for the scheduling and the queuing
of transfers requests.
*/


/*
INTERNAL
 *******************************************************************************
 * Filename         : usbUhcdScheduleQueue.h
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      :  This file contains the data structures maintained by the
 *                     HC and the HCD and the prototypes of the functions used
 *                     for the scheduling and the queuing of transfers requests.
 *
 *
 */
#ifndef __INCusbUhcdScheduleQueueh
#define __INCusbUhcdScheduleQueueh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include <usb/usbOsal.h>
#include <usb/usbHst.h>
#include <hwif/vxbus/vxBus.h>
#include <hwif/util/vxbDmaBufLib.h>

/* defines */

/*
 * UHCD link pointers, including frame list pointers and the pointers found
 * in USB_UHCD_TD and USB_UHCD_QH structures, use the same basic structure as defined
 * below.  The exception is the VF (Depth/Breadth select) which is only used
 * in the USB_UHCD_TD structure.
 */

#define USBUHCD_LINK_PTR_MASK   0xfffffff0  /* For the frame list ptr : Not used
                                                                      directly*/
#define USBUHCD_LINK_PTR_ROT        4

#define USBUHCD_LINK_VF         0x00000004  /* for the vf bit */
                                            /* 1=Depth first, 0=breadth first */


#define USBUHCD_LINK_QH         0x00000002  /* For the q bit */

#define USBUHCD_LINK_TD         0x00000000  /* 1=QH, 0=TD */


#define USBUHCD_LINK_TERMINATE  0x00000001  /*for the t bit */
                                            /* 1=End of list, 0=pointer valid */

/* Macros to retrieve/format link ptr */

#define USBUHCD_LINK_PTR(x) \
    ((UINT32)((((UINT32)(ULONG)(x)) & USBUHCD_LINK_PTR_MASK) >> USBUHCD_LINK_PTR_ROT))

#define USBUHCD_LINK_PTR_FMT(x) \
    ((UINT32)((((UINT32)(ULONG)(x)) << USBUHCD_LINK_PTR_ROT) & USBUHCD_LINK_PTR_MASK))

/* Frame List related macros */

#define USBUHCD_FRAME_LIST_ENTRIES     1024
#define USBUHCD_FRAME_LIST_ALIGNMENT   4096

/* Transfer Descriptor related macros */

#define UHCI_TD_LEN                     32 /* TD length required by UHCD hardware */
#define UHCI_TD_ACTLEN                  sizeof (USB_UHCD_TD)

/*
 * USB_UHCD_TD.dWord1Td definitions.
 */

#define USBUHCD_TDCS_SHORT          0x20000000  /* Short Packet Detect */
                                                /* 1=Enable, 0=Disable */

#define USBUHCD_TDCS_ERRCTR_MASK    0x18000000  /* Error Counter Mask*/
#define USBUHCD_TDCS_ERRCTR_NOLIM   0x00000000  /* No limit */
#define USBUHCD_TDCS_ERRCTR_1ERR    0x08000000  /* 1 error */
#define USBUHCD_TDCS_ERRCTR_2ERR    0x10000000  /* 2 errors */
#define USBUHCD_TDCS_ERRCTR_3ERR    0x18000000  /* 3 errors : used to set */
                                                /* error count */

#define USBUHCD_TDCS_LOWSPEED       0x04000000  /* Low Speed Device */
                                                /* 1=low speed, 0=full speed */
#define USBUHCD_TDCS_ISOCH          0x02000000  /* Isochronous Select */
                                                /* 1=isoch TD, 0=non-isoch TD */
#define USBUHCD_TDCS_COMPLETE       0x01000000  /* Interrupt on Complete */
                                                /* 1=Issue Ioc*/

/* USB_UHCD_TD.dWord1Td Status bits */

#define USBUHCD_TDCS_STS_MASK       0x00ff0000  /* Mask for status bits */
#define USBUHCD_TDCS_STS_ACTIVE     0x00800000  /* Active */
#define USBUHCD_TDCS_STS_STALLED    0x00400000  /* Stalled */
#define USBUHCD_TDCS_STS_DBUFERR    0x00200000  /* Data buffer error */
#define USBUHCD_TDCS_STS_BABBLE     0x00100000  /* Babble Detected */
#define USBUHCD_TDCS_STS_NAK        0x00080000  /* NAK Received */
#define USBUHCD_TDCS_STS_TIME_CRC   0x00040000  /* CRC/Timeout error */
#define USBUHCD_TDCS_STS_BITSTUFF   0x00020000  /* Bitstuff error */


/* USB_UHCD_TD.dWord1Td Actlen Related macros */

#define USBUHCD_TDCS_ACTLEN_MASK    0x000007ff  /* Actual Length mask */
#define USBUHCD_TDCS_ACTLEN_ROT     0           /* Actual length bit loc */
#define USBUHCD_TDCS_ACTLEN_ZERO    0x7ff       /* Actual length of zero bytes */

#define USBUHCD_PACKET_SIZE_MASK    0x000007ff  /* Mask for 11-bit field */


/* Macro to retrieve USBUHCD_TDCS_ACTLEN
 *
 * NOTE: UHCD actlen is one less than the real length.
 * The macro compensates automatically.
 */

#define USBUHCD_TDCS_ACTLEN(x) \
    ((((x) >> USBUHCD_TDCS_ACTLEN_ROT) ) & USBUHCD_PACKET_SIZE_MASK)

/* USB_UHCD_TD.dWord1Td Token related macros */


#define USBUHCD_TDTOK_MAXLEN_MASK   0xffe00000  /* Maximum length mask */
#define USBUHCD_TDTOK_MAXLEN_ROT    21          /* Maximum length bit loc */
#define USBUHCD_TDTOK_MAXLEN_ZERO   0x7FFU      /* Maximum length of zero */

#define USBUHCD_TDTOK_DATA_TOGGLE   0x00080000  /* Data Toggle */
                                                /* 0=DATA0, 1=DATA1 */

#define USBUHCD_TDTOK_ENDPT_MASK    0x00078000  /* Endpoint Mask */
#define USBUHCD_TDTOK_ENDPT_ROT     15          /* Endpoint bit loc */

#define USBUHCD_TDTOK_DEVADRS_MASK  0x00007f00  /* Device Address Mask */
#define USBUHCD_TDTOK_DEVADRS_ROT    8          /* Device Address bit loc */

#define USBUHCD_TDTOK_PID_MASK      0x000000ff  /* Packet ID mask */
#define USBUHCD_TDTOK_PID_IN        0x69        /* PID IN */
#define USBUHCD_TDTOK_PID_OUT       0xE1        /* PID OUT */
#define USBUHCD_TDTOK_PID_SETUP     0x2D        /* PID SETUP */
#define USBUHCD_TDTOK_PID_ROT       0           /* Packet ID bit loc */


/* Macros to retrieve/format USBUHCD_TDTOK_MAXLEN
 *
 * NOTE: UHCD maxlen is one less than the real length.    The macro
 * compensates automatically.
 */

#define USBUHCD_TDTOK_MAXLEN(x) \
    ((((x) >> USBUHCD_TDTOK_MAXLEN_ROT) ) & USBUHCD_PACKET_SIZE_MASK)



#define USBUHCD_TDTOK_MAXLEN_FMT(x) \
    ((((x)) & USBUHCD_PACKET_SIZE_MASK) << USBUHCD_TDTOK_MAXLEN_ROT)


/* Macros to retrieve/format USBUHCD_TDTOK_ENDPT */

#define USBUHCD_TDTOK_ENDPT(x) \
    (((x) & USBUHCD_TDTOK_ENDPT_MASK) >> USBUHCD_TDTOK_ENDPT_ROT)



#define USBUHCD_TDTOK_ENDPT_FMT(x) \
    (((x) << USBUHCD_TDTOK_ENDPT_ROT) & USBUHCD_TDTOK_ENDPT_MASK)


/* Macros to retrieve/format USBUHCD_TDTOK_DEVADRS */

#define USBUHCD_TDTOK_DEVADRS(x) \
    (((x) & USBUHCD_TDTOK_DEVADRS_MASK) >> USBUHCD_TDTOK_DEVADRS_ROT)

#define USBUHCD_TDTOK_DEVADRS_FMT(x) \
    (((x) << USBUHCD_TDTOK_DEVADRS_ROT) & USBUHCD_TDTOK_DEVADRS_MASK)

/* Macros to retrieve/format USBUHCD_TDTOK_PID */

#define USBUHCD_TDTOK_PID(x) \
    (((x) & USBUHCD_TDTOK_PID_MASK) >> USBUHCD_TDTOK_PID_ROT)

#define USBUHCD_TDTOK_PID_FMT(x) \
    (((x) << USBUHCD_TDTOK_PID_ROT) & USBUHCD_TDTOK_PID_MASK)

/* Queue Head related macros */

#define UHCI_QH_ALIGNMENT       16
#define UHCI_QH_LEN             8   /* QH length required by UHCI hardware */
#define UHCI_QH_ACTLEN          sizeof (USB_UHCD_QH)


#define USBUHCD_END_OF_LIST(INDEX)   \
        USB_UHCD_SWAP_DATA(INDEX, USBUHCD_LINK_TERMINATE)


/* interrupt bits */

#define UHCI_STS_HALTED     0x0020  /* Host Controller Halted */
#define UHCI_STS_PROCERR    0x0010  /* Host Controller Process Error */
#define UHCI_STS_HOSTERR    0x0008  /* Host System Error */
#define UHCI_STS_RESUME     0x0004  /* Resume Detect */
#define UHCI_STS_USBERR     0x0002  /* USB Error Interrupt */
#define UHCI_STS_USBINT     0x0001  /* USB Interrupt */

#define UHC_INT_PENDING_MASK    (UHCI_STS_PROCERR | UHCI_STS_HOSTERR |  \
                                UHCI_STS_RESUME | UHCI_STS_USBERR |     \
                                UHCI_STS_USBINT)


#define USB_UHCD_MAX_QH_SIZE               sizeof(USB_UHCD_QH)
#define USB_UHCD_MAX_TD_SIZE               sizeof(USB_UHCD_TD)

#define USB_UHCD_MAX_TD_RETRY_COUNT        3

#define USB_UHCD_DESC_BOUNDARY_LIMIT        4096      /* Software must ensure
                                                       * that no interface data
                                                       * structure reachable by
                                                       * the EHCI host
                                                       * controller spans a
                                                       * 4K page boundary.
                                                       */

#define USB_UHCD_MAX_FRAMELIST_SIZE         4096      /* Maximum size of the
                                                       * frame list
                                                       */

#define USB_UHCD_FRAMELIST_ALIGNMENT        4096      /* Alignment size for
                                                       * periodic frame list
                                                       */

#define USB_UHCD_HC_DATA_STRUCTURE_ALIGNMENT   64     /* Alignment size of the data
                                                       * structures of the Host
                                                       * Controller
                                                       */

#define USB_UHCD_CTRL_MAX_DATA_SIZE         (1 << 16)    /* Maximum size ctrl
                                                        DATA Transaction */

#define USB_UHCD_DIR_IN                     0x80 /* Value indicating IN
                                                  * direction
                                                  */
#define USB_UHCD_DIR_OUT                    0x00 /* Value indicating OUT
                                                   * direction
                                                   */

/* Get the low 32 bit of the BUS address */

#define USB_UHCD_BUS_ADDR_LO32(VALUE)    \
                    ((UINT32)((UINT64)(VALUE)))

/* Get the high 32 bit of the BUS address */

#define USB_UHCD_BUS_ADDR_HI32(VALUE)    \
                    ((UINT32)((UINT64)(VALUE) >> 32))

/* Get the low 32 bit of the descriptor BUS address */

#define USB_UHCD_DESC_LO32(DESC)    \
            USB_UHCD_BUS_ADDR_LO32((DESC)->dmaMapId->fragList[0].frag)

/* Get the high 32 bit of the descriptor BUS address */

#define USB_UHCD_DESC_HI32(DESC)    \
            USB_UHCD_BUS_ADDR_HI32((DESC)->dmaMapId->fragList[0].frag)

/* typedefs */

typedef struct usbUhcdQH  *         pUSB_UHCD_QH;
typedef struct usbUhcdTD  *         pUSB_UHCD_TD;
typedef struct usbUhcdRequestInfo * pUSB_UHCD_REQUEST_INFO;
typedef struct usbUhcdPipe *        pUSB_UHCD_PIPE;
typedef struct usbUhcdData *        pUSB_UHCD_DATA;

/*
 * Structure holding the format of the Transfer Descriptor
 * This is to be created for every packet to be transmitted in the USB bus.
 */

typedef struct usbUhcdTD
    {

    /* HC understood Transfer Descriptor format - start */

    UINT32              dWord0Td;   /* DWORD 0 of TD - TD LINK POINTER */

    UINT32              dWord1Td;   /* DWORD 1 of TD - TD CONTROL AND STATUS */

    UINT32              dWord2Td;   /* DWORD 2 of TD - TD TOKEN */

    UINT32              dWord3Td;   /* DWORD 3 of TD - TD BUFFER POINTER */

    /* HC understood Transfer Descriptor format - End */

    /* HCD understood Transfer Descriptor format - start */

    /* Pointer to the next TD in the queue of requests created by the HCD */

    struct usbUhcdTD *  hNextTd;

    /* Pointer to the previous TD in the queue of requests created by the HCD */

    struct usbUhcdTD *  hPrevTd;

    /* Pointer to the next TD in the queue maintained by the HC */

    struct usbUhcdTD *  vNextTd;

    /* Pointer to the previous TD in the queue maintained by the HC */

    struct usbUhcdTD *  vPrevTd;

    VXB_DMA_MAP_ID      dmaMapId;   /* Our DMA MAP ID */

    UINT8               uToggle;     /* Data uToggle */

    /* HCD understood Transfer Descriptor format - End */

    }/* _WRS_PACK_ALIGN(16) */ USB_UHCD_TD;


/*
 * Structure holding the format of the Queue Head.
 * This is to be created for every bulk, control and interrupt endpoint
 * created.
 */

typedef struct usbUhcdQH
    {

    /* HC understood Queue Head format - start */

    UINT32 dWord0Qh;     /* DWORD 0 of QH - QUEUE HEAD LINK POINTER */

    UINT32 dWord1Qh;     /* DWORD 1 of QH - QUEUE ELEMENT LINK POINTER */

    /* HC understood Queue Head format - End */

    /* HCD maintained Queue Head format - start */

    /* Pointer to the TD which is attached to the queue */

    USB_UHCD_TD *       pTD;

    /* Pointer to the previous QH in the schedule */

    struct usbUhcdQH *  pPrevQH;

    /* Pointer to the next QH in the schedule */

    struct usbUhcdQH *  pNextQH;

    /* Pointer to the pipe that is associated with this QH */

    struct usbUhcdPipe * pPipe;

    VXB_DMA_MAP_ID      dmaMapId;   /* Our DMA MAP ID */

    /* To hold the data uToggle field */

    UINT8 uToggle;

    /* HCD maintained Queue Head format - End */

    }/* _WRS_PACK_ALIGN(16) */ USB_UHCD_QH;

/*
 * Structure holding the format of the frame list of the HC
 * Each element in the list is accessed by the HC on every frame.
 */

typedef struct usbUhcdFrameList
    {
    UINT32 pfl[USBUHCD_FRAME_LIST_ENTRIES];
    } USB_UHCD_FRAME_LIST;

/*
 * The UHCD HCD maintains an array of 1024 elements to hold the
 * information about the isochronous TDs in a frame.
 * This structure holds information about each element of the array
 */

typedef struct usbUhcdPeriodicTable
    {
    UINT32 bandWidth;     /*
                           * Bandwidth occupied by the isochronous
                           * TDs in the frame
                           */
    USB_UHCD_TD *pTD;      /* Pointer to the first isochronous TD */
    }USB_UHCD_PERIODIC_TABLE_ENTRY;

/*
 * The UHCD HCD maintains a tree to hold the information about the
 * interrupt transfers which need to be serviced at different polling intervals.
 * This structure holds information about each node of the tree.
 */

typedef struct usbUhcdPeriodicTree
    {
    UINT32 bandWidth;   /*
                         * Bandwidth occupied by the interrupt transfers
                         * in a particular node
                         */
    USB_UHCD_QH *pQH;    /* Pointer to the first interrupt TD in the node */
    }USB_UHCD_PERIODIC_TREE_NODE;

/*
 * The HCD creates a pipe for every endpoint which needs to be
 * serviced and maintains it in a list.
 * This structure holds information about the HCD maintained pipe
 */

typedef struct usbUhcdPipe
    {
    UINT8           uSpeed;                   /* Device speed */
    UINT8           uAddress;                 /* Device number */
    UINT8           uEndpointAddress;         /* Endpoint number */
    UINT8           uEndpointDir;             /* Direction of the endpoint */
    UINT8           uEndpointType;            /* Type of the endpoint */
    UINT16          uMaximumPacketSize;

    USB_UHCD_QH *   pQH;          /*
                                   * Pointer to the QH
                                   * representing the endpoint
                                   */

    VXB_DMA_TAG_ID ctrlSetupDmaTagId; /* DMA TAG ID used for control setup */
    VXB_DMA_TAG_ID usrBuffDmaTagId;   /* DMA TAG ID used for user data */

    size_t   uMaxTransferSize; /* Maxium possible transfer size of this pipe */

    size_t   uMaxNumReqests;   /* Maxium possible transfer requests at a time */

    UINT32   uFlags;  /* Any possible flags */

    BOOL bOutTokenScheduled; /* Out Token Scheduled for Short Packet Transfer */
    BOOL bMarkedForDeletion; /* Is this pipe marked for deletion */

    OS_EVENT_ID    PipeSynchEventID;   /*
                                        * Event ID used for synchronisation
                                        * requests for this pipe.
                                        */
    INT32 bandwidth;              /*
                                   * To hold the bandwidth
                                   * occupied by the endpoint
                                   */
    UINT32 listIndex;             /* Index of the node pNext in the list */


    UINT32 lastIndexAllocated;    /* For an Isochronous pipe this field stores the
                                   * the Index allocated to the last request for
                                   * this endpoint(pipe)
                                   */
    UINT32 uLastStartIndex;       /* Start index of the last request */

    struct usbUhcdPipe * pNext;  /*
                                  * Pointer to the pNext pipe structure in the
                                  * list of pipes maintained by the HCD
                                  */

    struct usbUhcdPipe * pAltNext;
                                  /*
                                   * Pointer to the next UHCD_PIPE pointer in the
                                   * alternate list which is used for situations
                                   * such as delayed pipe addition or removal
                                   */

    pUSB_UHCD_REQUEST_INFO pRequestQueueHead;  /*
                                                * Pointer to the head of the request
                                                * queue structure.
                                                */
    pUSB_UHCD_REQUEST_INFO pRequestQueueTail;  /*
                                                * Pointer to the tail of the request
                                                * queue structure.
                                                */

    pUSB_UHCD_REQUEST_INFO pFreeRequestQueueHead;  /*
                                                * Pointer to the head of the request
                                                * queue structure.
                                                */
    pUSB_UHCD_REQUEST_INFO pFreeRequestQueueTail;  /*
                                                * Pointer to the tail of the request
                                                * queue structure.
                                                */

    pVOID pFreeTDHead; /* To hold the head of the Free TD list */


    pVOID pFreeTDTail; /* To hold the tail of the Free TD list */

    }USB_UHCD_PIPE;

/*
 * The HCD maintains a queue of non-isochronous requests
 * This gives the format of one element of the queue
 */

typedef struct usbUhcdRequestInfo
    {

    USBHST_URB *    pUrb;             /* Pointer to the URB */
    pUSB_UHCD_PIPE  pHCDPipe;         /* Pointer to pHCDPipe structure */

    UINT32          uDataDir;         /* Direction of the data transfer if any */
    UINT32          uRequestLength;   /* The request length of the current URB */

    INT32           startIndex; /*
                                 * Index of the frame in which the transfer
                                 * should be initiated (isoch pipe only)
                                 */

    UINT32          uRetryCount; /* Number of retries on failure */

    VXB_DMA_MAP_ID  ctrlSetupDmaMapId;   /* DMA MAP ID used for control setup */
    VXB_DMA_MAP_ID  usrDataDmaMapId;     /* DMA MAP ID used for user data */

    pUSB_UHCD_TD    pHead;            /*
                                       * Pointer to the TD which
                                       * forms the pHead of this request
                                       */
    pUSB_UHCD_TD    pTail;             /*
                                       * Pointer to the TD which forms
                                       * the pTail of this request
                                       */

    pUSB_UHCD_REQUEST_INFO pNext;
                               /*
                                * Pointer to the next
                                * request element in
                                * the endpoint matained
                                * queue
                                */
    }USB_UHCD_REQUEST_INFO;

/* This data structure holds the details of the Root hub */

typedef struct usbUhcdRhData
    {
    PUCHAR  pPortStatus;
    USB_UHCD_PIPE *  pControlPipe;
    USB_UHCD_PIPE *  pInterruptPipe;
    UINT8       bInterruptEndpointHalted;
    UINT8       uDeviceAddress;  /* Address of the Root hub. */
    UINT8       uConfigValue;    /* Value of the configuration which is set. */
    }USB_UHCD_RH_DATA;

typedef struct usbUhcdRhData *PUSB_UHCD_RH_DATA;


/* This is the data structure maintained for every UHCD Host Controller. */

typedef struct usbUhcdData
    {
    UINT8        uBusIndex;           /* Index into the array of host controllers */

    UINT32       descAlignment;    /* Transfer descriptor alignment */

    bus_addr_t   usrDmaTagLowAddr;  /* User buffer DMA TAG lowAddr */

    int          usrDmaMapFlags;  /* User buffer DMA MAP flags */

    VXB_DMA_TAG_ID uhciParentTag; /* Our parent TAG ID */

    VXB_DMA_TAG_ID qhDmaTagId;   /* DMA TAG ID used for the QH */
    VXB_DMA_TAG_ID tdDmaTagId;   /* DMA TAG ID used for the TD */

    VXB_DMA_TAG_ID frameListDmaTagId; /* DMA TAG ID for periodic frame list */
    VXB_DMA_MAP_ID frameListDmaMapId; /* DMA MAP ID for periodic frame list */

    USB_UHCD_FRAME_LIST * pFrameList;
                                /* To hold the UHCD's frame list.
                                 * The UHCD Host controller maintains an array of indices
                                 * which are pointers to the list of requests to be serviced.
                                 */

    USB_UHCD_PERIODIC_TABLE_ENTRY * pPeriodicTable;
                                 /*
                                  * To hold the pointer to the table containing the
                                  * bandwidth information of the isochronous transfers
                                  */

    USB_UHCD_PERIODIC_TREE_NODE * pPeriodicTree;
                                 /* To hold the tip of the periodic
                                  * tree maintained for interrupt transfers
                                  */


    pUSB_UHCD_QH    pControlQH;   /* Control Queue Head */

    pUSB_UHCD_QH    pBulkQH;      /* Bulk Queue Head */


    pUSB_UHCD_QH    pBandwidthReclaimQH; /* Bandwidth Reclamation Queue Head */


    pUSB_UHCD_PIPE  pDefaultPipe;/* Default control pipe */

    pUSB_UHCD_PIPE  pNonIsochPipeList; /* List of pipes belonging to host conroller */

    pUSB_UHCD_PIPE  pIsochPipeList; /* List of Isoch pipes belonging to host conroller */

    pUSB_UHCD_PIPE  pDelayedPipeAdditionList;
                                       /*
                                        * Pointer to the head of the list
                                        * of pipes that are delayed to be
                                        * added into the active pipe list
                                        * if the pipe creating is called
                                        * in the context of the URB
                                        * completion processing task
                                        */
    pUSB_UHCD_PIPE   pDelayedPipeRemovalList;
                                       /*
                                        * Pointer to the head of the list
                                        * of pipes that are delayed to be
                                        * removed from the active pipe list
                                        * if the pipe deletion is called
                                        * in the context of the URB
                                        * completion processing task
                                        */

    OS_EVENT_ID    ActivePipeListSynchEventID;
                                        /*
                                         * Event ID used for
                                         * synchronisation of the UHCD_PIPE
                                         * active list.
                                         */

    OS_EVENT_ID    ReclamationListSynchEventID;

                                        /*
                                         * Event ID used for
                                         * synchronisation of the UHCD_PIPE
                                         * reclamation list.
                                         */


    USB_UHCD_RH_DATA rootHub;   /* Root Hub Details of host controller*/

    UINT8 usbUhcdResetChange[2] ;/* Flag used for monitoring Reset Change on ports */

    UINT16 savedPortStatus[2] ;  /* Save the port status value */

    BOOL forceReconnect[2] ;   /* force reconnect */

    UCHAR usbUhcdRhConfig;/* Value indicating the configuration number which is set */

    OS_THREAD_ID IntHandlerThreadID; /* Thread ID for the thread created for handling transfer completion */

    SEM_ID tdCompleteSem; /* Semaphore used to indicate a safe cleanup of resources */

    SEM_ID BandwidthMutexEvent;  /*
                                 * Semphore for mutual exclusion. This semaphore
                                 * for handling shared global data structures
                                 */

    UINT8  flagRhIntDel; /* Flag used for checking the deletion of the interrupt
                          * task for Root Hub
                          */
    UINT16 uIntStatus;  /* place holder to store the Interrupt register value */

    struct vxbDev *       pDev;     /* struct vxbDev */

    void *    pRegAccessHandle;     /* Register access handle */

    ULONG       regBase;            /* Base address from vxBus */

    UINT32      regIndex;           /* Index of the regBase */

    UINT32     (*pDataSwap)(UINT32 data);  /* function pointer to hold the
                                            * function doing byte conversions
                                            */
    spinlockIsr_t spinLockIsr;  /* spinlock for the UHCI controller */

    UINT32        magic; /* magic value to watch out for shared interrupt */
    }USB_UHCD_DATA;


/* This data structure holds the details of the Endpoint of a New Interface */

typedef struct usbUhcdNewEndpointDetails
    {
    UINT8       endpointType;   /* Type of the endpoint*/
    INT32       Bw;             /* Bandwidth need by the endpoint. */
    UINT8       pollingInterval;/* Polling interval for the Interrupt pipe. */
    }USB_UHCD_NEW_ENDPOINT_DETAILS;

typedef struct usbUhcdNewEndpointDetails *PUSB_UHCD_NEW_ENDPOINT_DETAILS;

USBHST_STATUS usbUhcdUnSetupPipe
    (
    pUSB_UHCD_DATA pHCDData,
    pUSB_UHCD_PIPE pHCDPipe
    );

USBHST_STATUS usbUhcdSetupPipe
    (
    pUSB_UHCD_DATA              pHCDData,
    pUSB_UHCD_PIPE              pHCDPipe,
    pUSB_TRANSFER_SETUP_INFO    pSetupInfo
    );

USBHST_STATUS usbUhcdSetupControlPipe
    (
    pUSB_UHCD_DATA pHCDData,
    pUSB_UHCD_PIPE pHCDPipe
    );

BOOLEAN usbUhcdDeletePipeContext
    (
    pUSB_UHCD_DATA      pHCDData,
    pUSB_UHCD_PIPE      pHCDPipe
    );

/*******************************************************************************
 * Function Name    : usbUhcdQueueRhRequest
 * Description      : This function handles the request queued to the Root hub.
 * Parameters       : pUrb IN Pointer to the URB.
 * Return Type      : BOOLEAN
 *                    Returns
 *                       TRUE  - If the request for Root hub is queued
 *                               successfully
 *                       FALSE - If the request for the Root hub is not
 *                               queued.
 ******************************************************************************/

extern BOOLEAN usbUhcdQueueRhRequest (pUSB_UHCD_DATA pHCDData, USBHST_URB *pUrb ,ULONG pipeHandle);
extern USBHST_STATUS usbUhcdRhDeletePipe (pUSB_UHCD_DATA pHCDData, ULONG uPipeHandle);
extern USBHST_STATUS usbUhcdRhCreatePipe (pUSB_UHCD_DATA pHCDData,
                                          UINT8    uDeviceAddress,
                                          UINT8    uDeviceSpeed,
                                          PUCHAR  pEndpointDescriptor,
                                          ULONG  *puPipeHandle);
/*******************************************************************************
 * Function Name    : usbUhcdCreateFrameList
 * Description      : The HCD maintains
 *                    - a tree of 256 elements to maintain the bandwidth usage
 *                      in every frame for the interrupt transfers.
 *                    - an array of 1024 static isochronous Transfer
 *                      Descriptor elements.
 *                    This function allocates memory for the data structures and
 *                    initialises them. Also this function allocates memory for
 *                    the frame list maintained by the UHCD Host controller and
 *                    initialises the list.
 * Parameters       : None.
 * Return Type      : BOOLEAN
 *                    Returns
 *                        TRUE - If frame list creation is success
 *                        FALSE- If frame list creation is a failure
 ******************************************************************************/
extern BOOLEAN usbUhcdCreateFrameList (pUSB_UHCD_DATA pHCDData);

/*******************************************************************************
 * Function Name    : usbUhcdDeleteFrameList
 * Description      : The HCD maintains
 *                    - a tree of 256 elements to maintain the bandwidth usage
 *                      in every frame for the interrupt transfers.
 *                    - an array of 1024 static isochronous Transfer
 *                      Descriptor elements.
 *                    This function deallocates memory for the data structures
 * Parameters       : None.
 * Return Type      : BOOLEAN
 *                    Returns
 *                        TRUE - If frame list deletion is success
 *                        FALSE- If frame list deletion is a failure
 ******************************************************************************/
extern BOOLEAN usbUhcdDeleteFrameList (pUSB_UHCD_DATA pHCDData);

/*******************************************************************************
*
* usbUhcdPipeControl - control the pipe characteristics
*
* This routine is used to control the pipe characteristics.
*
* <uBusIndex> specifies the host controller bus index.
* <uPipeHandle> holds the pipe handle.
* <pPipeCtrl> is the pointer to the USBHST_PIPE_CONTROL_INFO holding
*             the request details.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the URB is submitted successfully.
*   USBHST_INVALID_PARAMETER - Returned if the parameters are not valid.
*   USBHST_INVALID_REQUEST - Returned if the request is not valid
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbUhcdPipeControl
    (
    UINT8  uBusIndex,                   /* Index of the host controller */
    ULONG  uPipeHandle,                 /* Pipe handle */
    USBHST_PIPE_CONTROL_INFO * pPipeCtrl /* Pointer to the pipe control info */
    );

/*******************************************************************************
 * Function Name    : usbUhcdCreatePipe
 * Description      : This function creates
 *                     - the HCD maintained pipe
 *                     - HC maintained QH for control, bulk and interrupt
 *                       transfers
 *                     - Updates the bandwidth utilised.
 * Parameters       : pipe IN Pointer to the Pipe data structure.
 * Return Type      : BOOLEAN
 *                    Returns
 *                        TRUE  - If pipe is created successfully
 *                        FALSE - If pipe is not created successfully.
 ******************************************************************************/
extern USBHST_STATUS usbUhcdCreatePipe (UINT8  uBusIndex,
                                        UINT8  uDeviceAddress,
                                        UINT8  uDeviceSpeed,
                                        UCHAR  *pEndpointDescriptor,
                                        UINT16  uHighSpeedHubInfo,
                                        ULONG  *puPipeHandle);
/*******************************************************************************
 * Function Name    : usbUhcdDeletePipe
 * Description      : This function is used to delete the HCD maintained pipe
 *                    and the QH created for the pipe.
 * Parameters       : pipe IN Pointer to the Pipe data structure.
 * Return Type      : BOOLEAN
 *                    Returns
 *                        TRUE  - If pipe is deleted successfully.
 *                        FALSE - If pipe is not deleted successfully.
 ******************************************************************************/
extern USBHST_STATUS usbUhcdDeletePipe (UINT8 uBusIndex,
                                        ULONG uPipeHandle);
/*******************************************************************************
 * Function Name    : UHCD_SubmitURB
 * Description      : This function is used to submit a transfer request to the
 *                    HCD.
 * Parameters       : pUrb IN Pointer to URB.
 * Return Type      : BOOLEAN
 *                    Returns
 *                        TRUE  - If request is submitted successfully
 *                        FALSE - If request is not submitted successfully
 ******************************************************************************/
extern USBHST_STATUS usbUhcdSubmitUrb (UINT8     uBusIndex,
                                       ULONG     uPipeHandle,
                                       USBHST_URB *pUrb);
/*******************************************************************************
 * Function Name    : usbUhcdCancelUrb
 * Description      : This function is used to cancel a request queued to
 *                    already
 * Parameters       : None.
 * Return Type      : BOOLEAN
 *                    Returns
 *                        TRUE  - If the request is cancelled successfully
 *                        FALSE - If the request is not cancelled
 *                                successfully.
 ******************************************************************************/
extern USBHST_STATUS usbUhcdCancelUrb (UINT8 uBusIndex,
                                       ULONG uPipeHandle,
                                       USBHST_URB *pUrb);


/*******************************************************************************
 * Function Name    : usbUhcdGetFrameNumber
 * Description      : This function is used to get the current frame number
 *                    of the Host Controller.
 * Parameters       : None.
 * Return Type      : UINT
 *                    Returns the least 11 bits of the current frame number.
 ******************************************************************************/
extern USBHST_STATUS usbUhcdGetFrameNumber (UINT8 uBusIndex,
                                            UINT16 *puFrameNumber);

/*******************************************************************************
 * Function Name    : usbUhcdSetBitRate
 * Description      : This function is used to set the bit rate
 *
 * Parameters       : .
 * Return Type      : USBHST_SUCCESS
 *                    Returns USBHST_SUCCESS, or USBHST_FAILURE in case of failure
 ******************************************************************************/

extern USBHST_STATUS usbUhcdSetBitRate (UINT8 uBusIndex,
                                        BOOL bIncrement,
                                        PUINT32 puCurrentFrameWidth);

/*******************************************************************************
 * Function Name    : usbUhcdIsRequestPending
 * Description      : This function is detrmine if request is pending on pipe
 *
 * Parameters       : uBusIndex Host Controller Bus Index
 *                    uPipeHandle : Handle to pipe
 * Return Type      : USBHST_SUCCESS
 *                    Returns USBHST_SUCCESS, or USBHST_FAILURE
 ******************************************************************************/


extern USBHST_STATUS usbUhcdIsRequestPending (UINT8 uBusIndex,
                                              ULONG uPipeHandle);

/*******************************************************************************
 * Function Name    : usbUhcdModifyDefaultPipe
 * Description      : This function is modify the default pipe
 *
 * Parameters       : uBusIndex Host Controller Bus Index
 *                    uDefaultPipeHandle : Handle to  default pipe
                      uDeviceSpeed : speed of device
                      uMaxPacketSize : max packet size
 * Return Type      : USBHST_SUCCESS
 *                    Returns USBHST_SUCCESS, or USBHST_FAILURE
 ******************************************************************************/


USBHST_STATUS usbUhcdModifyDefaultPipe (UINT8 uBusIndex,
                                        ULONG uDefaultPipeHandle,
                                        UINT8 uDeviceSpeed,
                                        UINT8 uMaxPacketSize,
                                        UINT16  uHighSpeedHubInfo);

/*******************************************************************************
 * Function Name    : usbUhcdProcessTransferCompletion
 * Description      : This function is used to handle the completed TDs.
 * Parameters       : pUSB_UHCD_DATA
 * Return Type      : void
 ******************************************************************************/
extern VOID usbUhcdProcessTransferCompletion(pUSB_UHCD_DATA pHCDData);


/*******************************************************************************
 * Function Name    : usbUhcdGetCurrentEndpointDetails
 * Description      : This function searches all for the pipes with a
 *                    particular device address.
 *
 * Parameters       :
 *                    uBusindex: Host Controller Bus Index.
 *                    uDeviceAddress : device address
 *                    pListOfCurrentEndpointPipes : Contains the list of the pipes
 *                                                  found
 *                    uNumberOfEndpoint         Store the number of pipes
 *
 * Return Type      : None
 * Global Variables : g_pUHCDData
 * Calls            :
 * Called by        :
 * To Do            : None
 ******************************************************************************/

extern VOID usbUhcdGetCurrentEndpointDetails(
                            UINT8    uBusIndex,
                            UINT8    uDeviceAddress,
                            USB_UHCD_PIPE **pListOfCurrentEndpointPipes,
                            pUSBHST_ENDPOINT_DESCRIPTOR pCurrentEndpointDesc,
                            UINT8 *uNumberOfEndpoint);


/*******************************************************************************
 * Function Name    : usbUhcdGetDetailsForEndpoints
 * Description      : This function Get details for the Endpoint
 *                    of the new interface
 *
 * Parameters       :
 *                    uDeviceSpeed :         IN       device Speed
 *                    uNumberOfEndpoints :   IN       Number of Endpoints
 *                    pEndpointDesc :        IN       all the endpoint desc for
 *                                                    the Interface
 *                    pNewEndpointDetails    OUT      Details of the Periodic endpoint
 *
 * Return Type      : None.
 * Global Variables : None
 * Calls            :
 * Called by        :
 * To Do            : None
 ******************************************************************************/
extern VOID usbUhcdGetDetailsForEndpoints(UINT8  uDeviceSpeed,
                                     UINT8 *uNumberOfEndpoints,
                                     pUSBHST_ENDPOINT_DESCRIPTOR *pEndpointDesc,
                                     PUSB_UHCD_NEW_ENDPOINT_DETAILS pNewEndpointDetails);


/* Macro to add pointer to USB_UHCD_PIPE in Delayed pipe addition List */

#define  USB_UHCD_ADD_TO_DELAYED_PIPE_ADDITION_LIST(pHCDData, pHCDPipe)        \
    {                                                                          \
    /* Exclusively access the list */                                          \
    OS_WAIT_FOR_EVENT(pHCDData->ReclamationListSynchEventID, OS_WAIT_INFINITE);\
                                                                               \
    /* Add to the list */                                                      \
    pHCDPipe->pAltNext = pHCDData->pDelayedPipeAdditionList;                   \
                                                                               \
    /* Update the delayed pipe addition list head */                           \
    pHCDData->pDelayedPipeAdditionList = pHCDPipe;                             \
                                                                               \
    /* Release the exclusive access */                                         \
    OS_RELEASE_EVENT(pHCDData->ReclamationListSynchEventID);                   \
    }

/* Macro to add pointer to USB_UHCD_PIPE in Delayed pipe removal List */

#define  USB_UHCD_ADD_TO_DELAYED_PIPE_REMOVAL_LIST(pHCDData, pHCDPipe)         \
    {                                                                          \
    /* Exclusively access the list */                                          \
    OS_WAIT_FOR_EVENT(pHCDData->ReclamationListSynchEventID, OS_WAIT_INFINITE);\
                                                                               \
    /* Add to the list */                                                      \
    pHCDPipe->pAltNext = pHCDData->pDelayedPipeRemovalList;                    \
                                                                               \
    /* Update the delayed pipe removal list head */                            \
    pHCDData->pDelayedPipeRemovalList = pHCDPipe;                              \
                                                                               \
    /* Release the exclusive access */                                         \
    OS_RELEASE_EVENT(pHCDData->ReclamationListSynchEventID);                   \
    }

/* Macro to add pointer to USB_UHCD_PIPE in Isoch pipe List */

#define  USB_UHCD_ADD_TO_ISOCH_PIPE_LIST(pHCDData, pHCDPipe)                   \
    {                                                                          \
    /* Exclusively access the list */                                          \
    OS_WAIT_FOR_EVENT(pHCDData->ActivePipeListSynchEventID, OS_WAIT_INFINITE); \
                                                                               \
    /* Add to the list */                                                      \
    pHCDPipe->pNext = pHCDData->pIsochPipeList;                                \
                                                                               \
    /* Update the isoch list head */                                           \
    pHCDData->pIsochPipeList = pHCDPipe;                                       \
                                                                               \
    /* Release the exclusive access */                                         \
    OS_RELEASE_EVENT(pHCDData->ActivePipeListSynchEventID);                    \
    }

/* Macro to add pointer to USB_UHCD_PIPE in Isoch pipe List */

#define  USB_UHCD_ADD_TO_NON_ISOCH_PIPE_LIST(pHCDData, pHCDPipe)               \
    {                                                                          \
    /* Exclusively access the list */                                          \
    OS_WAIT_FOR_EVENT(pHCDData->ActivePipeListSynchEventID, OS_WAIT_INFINITE); \
                                                                               \
    /* Add to the list */                                                      \
    pHCDPipe->pNext = pHCDData->pNonIsochPipeList;                             \
                                                                               \
    /* Update the non-isoch list head */                                       \
    pHCDData->pNonIsochPipeList = pHCDPipe;                                    \
                                                                               \
    /* Release the exclusive access */                                         \
    OS_RELEASE_EVENT(pHCDData->ActivePipeListSynchEventID);                    \
    }

/* Macro to remove USB_UHCD_PIPE in Non-Isoch pipe List */

#define  USB_UHCD_REMOVE_NON_ISOCH_PIPE(pHCDData, pHCDPipe)       \
    {                                                             \
    pUSB_UHCD_PIPE pTempPipe;                                     \
                                                                  \
    if (pHCDPipe == pHCDData->pNonIsochPipeList)                  \
        {                                                         \
        /* If it is the hesd element,                             \
         * update the head of list to next pointer */             \
         pHCDData->pNonIsochPipeList = pHCDPipe->pNext;           \
        }                                                         \
    else                                                          \
        {                                                         \
        /* Search for the pipe in the list */                     \
        for (pTempPipe = pHCDData->pNonIsochPipeList;             \
             (NULL != pTempPipe) &&                               \
             (pHCDPipe != pTempPipe->pNext);                      \
              pTempPipe = pTempPipe->pNext);                      \
                                                                  \
        /* If Pipe not found, Assert an error */                  \
        if (NULL != pTempPipe)                                    \
            pTempPipe->pNext = pHCDPipe->pNext;                   \
        }                                                         \
    }

/* Macro to remove USB_UHCD_PIPE in Isoch pipe List */

#define  USB_UHCD_REMOVE_ISOCH_PIPE(pHCDData, pHCDPipe)           \
    {                                                             \
    pUSB_UHCD_PIPE pTempPipe;                                     \
                                                                  \
    if (pHCDPipe == pHCDData->pIsochPipeList)                     \
        {                                                         \
        /* If it is the hesd element,                             \
         * update the head of list to next pointer */             \
         pHCDData->pIsochPipeList = pHCDPipe->pNext;              \
        }                                                         \
    else                                                          \
        {                                                         \
        /* Search for the pipe in the list */                     \
        for (pTempPipe = pHCDData->pIsochPipeList;                \
             (NULL != pTempPipe) &&                               \
             (pHCDPipe != pTempPipe->pNext);                      \
              pTempPipe = pTempPipe->pNext);                      \
                                                                  \
        /* If Pipe not found, Assert an error */                  \
        if (NULL != pTempPipe)                                    \
            pTempPipe->pNext = pHCDPipe->pNext;                   \
        }                                                         \
    }

#define USB_UHCD_QH_SET_LINK_T_BIT(uBusIndex, pQH)                    \
    {                                                                 \
    volatile UINT32 dword0;                                           \
                                                                      \
     /* do DWORD read in BUS endian */                                \
                                                                      \
    dword0 = (pQH)->dWord0Qh;                                         \
                                                                      \
    /* Set the T bit in BUS endian */                                 \
                                                                      \
    dword0 |= (USB_UHCD_SWAP_DATA(uBusIndex,                          \
                        USBUHCD_LINK_TERMINATE));                     \
                                                                      \
    /* do DWORD write in BUS endian */                                \
                                                                      \
    (pQH)->dWord0Qh = dword0;                                         \
    }                                                                 \

#define USB_UHCD_QH_CLR_LINK_T_BIT(uBusIndex, pQH)                    \
    {                                                                 \
    volatile UINT32 dword0;                                           \
                                                                      \
     /* do DWORD read in BUS endian */                                 \
                                                                      \
    dword0 = (pQH)->dWord0Qh;                                         \
                                                                      \
    /* Clr the T bit in BUS endian */                                 \
                                                                      \
    dword0 &= (~(USB_UHCD_SWAP_DATA(uBusIndex,                        \
                        USBUHCD_LINK_TERMINATE)));                    \
                                                                      \
    /* do DWORD write in BUS endian */                                \
                                                                      \
    (pQH)->dWord0Qh = dword0;                                         \
    }                                                                 \

#define USB_UHCD_QH_SET_ELEMENT_T_BIT(uBusIndex, pQH)                 \
    {                                                                 \
    volatile UINT32 dword0;                                           \
                                                                      \
     /* do DWORD read in BUS endian */                                \
                                                                      \
    dword0 = (pQH)->dWord1Qh;                                         \
                                                                      \
    /* Set the T bit in BUS endian */                                 \
                                                                      \
    dword0 |= (USB_UHCD_SWAP_DATA(uBusIndex,                          \
                        USBUHCD_LINK_TERMINATE));                     \
                                                                      \
    /* do DWORD write in BUS endian */                                \
                                                                      \
    (pQH)->dWord1Qh = dword0;                                         \
    }                                                                 \

#define USB_UHCD_QH_CLR_ELEMENT_T_BIT(uBusIndex, pQH)                 \
    {                                                                 \
    volatile UINT32 dword0;                                           \
                                                                      \
     /* do DWORD read in BUS endian */                                \
                                                                      \
    dword0 = (pQH)->dWord1Qh;                                         \
                                                                      \
    /* Clr the T bit in BUS endian */                                 \
                                                                      \
    dword0 &=  (~(USB_UHCD_SWAP_DATA(uBusIndex,                       \
                        USBUHCD_LINK_TERMINATE)));                    \
                                                                      \
    /* do DWORD write in BUS endian */                                \
                                                                      \
    (pQH)->dWord1Qh = dword0;                                         \
    }                                                                 \

#define USB_UHCD_QH_UNLINK_TD_ELEMENT(uBusIndex, pQH)                 \
    {                                                                 \
    volatile UINT32 dword0;                                           \
                                                                      \
     /* do DWORD read in BUS endian */                                \
                                                                      \
    dword0 = (pQH)->dWord1Qh;                                         \
                                                                      \
    /* Mask the LINK PTR in BUS endian */                             \
                                                                      \
    dword0 &= (~(USB_UHCD_SWAP_DATA(uBusIndex,                        \
                        USBUHCD_LINK_PTR_MASK)));                     \
                                                                      \
    /* Set the T bit in BUS endian */                                 \
                                                                      \
    dword0 |= (USB_UHCD_SWAP_DATA(uBusIndex,                          \
                        USBUHCD_LINK_TERMINATE));                     \
                                                                      \
    /* do DWORD write in BUS endian */                                \
                                                                      \
    (pQH)->dWord1Qh = dword0;                                         \
    }

#define USB_UHCD_QH_LINK_TD_ELEMENT_ACTIVE(uBusIndex, pQH, linkPtr)   \
    {                                                                 \
    volatile UINT32 dword0 = 0;                                       \
                                                                      \
    /* Set the LINK PTR BUS ADDR in CPU endian */                     \
                                                                      \
    dword0 |= USBUHCD_LINK_PTR_FMT(USB_UHCD_DESC_LO32(linkPtr) >> 4); \
                                                                      \
    /* Clear the T bit in CPU endian */                               \
                                                                      \
    dword0 &= ~(USBUHCD_LINK_TERMINATE);                              \
                                                                      \
    /* Convert to BUS endian and do DWORD write */                    \
                                                                      \
    (pQH)->dWord1Qh = USB_UHCD_SWAP_DATA(uBusIndex,dword0);           \
    }                                                                 \

#define USB_UHCD_TD_SET_LINK_T_BIT(uBusIndex, pTD)                    \
    {                                                                 \
    volatile UINT32 dword0;                                           \
                                                                      \
    /* do DWORD read in BUS endian */                                 \
                                                                      \
    dword0 = (pTD)->dWord0Td;                                         \
                                                                      \
    /* Set the T bit in BUS endian */                                 \
                                                                      \
    dword0 |= (USB_UHCD_SWAP_DATA(uBusIndex,                          \
                        USBUHCD_LINK_TERMINATE));                     \
                                                                      \
    /* do DWORD write in BUS endian */                                \
                                                                      \
    (pTD)->dWord0Td = dword0;                                         \
    }                                                                 \

#define USB_UHCD_TD_CLR_LINK_T_BIT(uBusIndex, pTD)                    \
    {                                                                 \
    volatile UINT32 dword0;                                           \
                                                                      \
    /* do DWORD read in BUS endian */                                 \
                                                                      \
    dword0 = (pTD)->dWord0Td;                                         \
                                                                      \
    /* Clr the T bit in BUS endian */                                 \
                                                                      \
    dword0 &= (~(USB_UHCD_SWAP_DATA(uBusIndex,                        \
                        USBUHCD_LINK_TERMINATE)));                    \
                                                                      \
    /* do DWORD write in BUS endian */                                \
                                                                      \
    (pTD)->dWord0Td = dword0;                                         \
    }                                                                 \

#define USB_UHCD_TD_SET_TOGGLE_BIT(uBusIndex, pTD)                    \
    {                                                                 \
    volatile UINT32 dword0;                                           \
                                                                      \
    /* do DWORD read in BUS endian */                                 \
                                                                      \
    dword0 = (pTD)->dWord2Td;                                         \
                                                                      \
    /* Set the Toggle bit in BUS endian */                            \
                                                                      \
    dword0 |= (USB_UHCD_SWAP_DATA(uBusIndex,                          \
                        USBUHCD_TDTOK_DATA_TOGGLE));                  \
                                                                      \
    /* do DWORD write in BUS endian */                                \
                                                                      \
    (pTD)->dWord2Td = dword0;                                         \
    }                                                                 \

#define USB_UHCD_TD_CLR_TOGGLE_BIT(uBusIndex, pTD)                    \
    {                                                                 \
    volatile UINT32 dword0;                                           \
                                                                      \
    /* do DWORD read in BUS endian */                                 \
                                                                      \
    dword0 = (pTD)->dWord2Td;                                         \
                                                                      \
    /* Clr the Toggle bit in BUS endian */                            \
                                                                      \
    dword0 &= (~(USB_UHCD_SWAP_DATA(uBusIndex,                        \
                        USBUHCD_TDTOK_DATA_TOGGLE)));                 \
                                                                      \
    /* do DWORD write in BUS endian */                                \
                                                                      \
    (pTD)->dWord2Td = dword0;                                         \
    }

#define USB_UHCD_TD_UNLINK_TD(uBusIndex, pTD)                         \
    {                                                                 \
    volatile UINT32 dword0;                                           \
                                                                      \
    /* do DWORD read in BUS endian */                                 \
                                                                      \
    dword0 = (pTD)->dWord0Td;                                         \
                                                                      \
    /* Mask the LINK PTR in BUS endian */                             \
                                                                      \
    dword0 &= (~(USB_UHCD_SWAP_DATA(uBusIndex,                        \
                        USBUHCD_LINK_PTR_MASK)));                     \
                                                                      \
    /* Set the T bit in BUS endian */                                 \
                                                                      \
    dword0 |= (USB_UHCD_SWAP_DATA(uBusIndex,                          \
                        USBUHCD_LINK_TERMINATE));                     \
                                                                      \
    /* do DWORD write in BUS endian */                                \
                                                                      \
    (pTD)->dWord0Td = dword0;                                         \
    }                                                                 \

#define USB_UHCD_TD_LINK_TD_ACTIVE(uBusIndex, pTD, linkPtr)           \
    {                                                                 \
    volatile UINT32 dword0 = 0;                                       \
                                                                      \
    /* Set the LINK PTR BUS ADDR in CPU endian */                     \
                                                                      \
    dword0 |= USBUHCD_LINK_PTR_FMT(USB_UHCD_DESC_LO32(linkPtr) >> 4); \
                                                                      \
    /* Clear the T bit in CPU endian */                               \
                                                                      \
    dword0 &= ~(USBUHCD_LINK_TERMINATE);                              \
                                                                      \
    /* Convert to BUS endian and do DWORD write */                    \
                                                                      \
    (pTD)->dWord0Td = USB_UHCD_SWAP_DATA(uBusIndex, dword0);          \
    }                                                                 \

/*******************************************************************************
*
* USB_UHCD_MAKE_TD_LE - convert the members of TD to Little Endian
*
* This macro converts the members of TD to Little Endian .
*
* \NOMANUAL
*/

#define USB_UHCD_MAKE_TD_LE(uBusIndex, pTD)                                 \
    {                                                                       \
    (pTD)->dWord0Td = USB_UHCD_SWAP_DATA((uBusIndex), (pTD)->dWord0Td);     \
    (pTD)->dWord1Td = USB_UHCD_SWAP_DATA((uBusIndex), (pTD)->dWord1Td);     \
    (pTD)->dWord2Td = USB_UHCD_SWAP_DATA((uBusIndex), (pTD)->dWord2Td);     \
    (pTD)->dWord3Td = USB_UHCD_SWAP_DATA((uBusIndex), (pTD)->dWord3Td);     \
    }

/*******************************************************************************
*
* USB_UHCD_MAKE_QH_LE - convert the members of TD to Little Endian
*
* This macro converts the members of TD to Little Endian.
*/

#define USB_UHCD_MAKE_QH_LE(uBusIndex, pQH)                                 \
    {                                                                       \
    (pQH)->dWord0Qh =  USB_UHCD_SWAP_DATA((uBusIndex), (pQH)->dWord0Qh);    \
    (pQH)->dWord1Qh =  USB_UHCD_SWAP_DATA((uBusIndex), (pQH)->dWord1Qh);    \
    }

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCusbUhcdScheduleQueueh */

