/* usbPlx338x.h - Definitions for PLX USB338x target controller */

/*
 * Copyright (c) 2012 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
Modification history
--------------------
01c,21nov12,s_z  Add Reset exit workaround based Errata Issue_4
                 Reset squeuence number after endpoint halt (WIND00389964)
01b,23oct12,s_z  Add U1/U2 rejection workaround based Errata (WIND00382685)
01a,18may12,s_z  Writen
*/

/*
DESCRIPTION

This module constants the definitions related to the PLX USB338x USB 
device (target) controller. Since most registers of USB338x are compatible
with PLX228x controller. This mode only defined for USB338x specificly.
*/

#ifndef __INCusbPlx338xh
#define __INCusbPlx338xh

#ifdef    __cplusplus
extern "C" {
#endif

/* defines */

/* USB338x specific registers */

#define USB_PLX_SS_SEL             0xB8       /* USB SEL register */
#define USB_PLX_SS_DEL             0xBC       /* USB DEL register */
#define USB_PLX_USBCTL2_REG        0xC8       /* USB Control 2 register */
#define USB_PLX_SS_ISODELAY        0xD0       /* ISO DELAY register */


/* Expand bitMaps of the USBSTAT susper speed definition */

#define USB_PLX_USBSTAT_SS         0x100 /* USBSTAT: Super Speed */
#define USB_PLX_USBSTAT_SUSPEND    0x200 /* USBSTAT: Super Speed */

/* BitMaps for the USBCTL2 */

#define USB_PLX_USBCTL2_LTMEN      (0x1 << 7) /* USB Control 2 register */
#define USB_PLX_USBCTL2_U2EN       (0x1 << 6) /* USB Control 2 register */
#define USB_PLX_USBCTL2_U1EN       (0x1 << 5) /* USB Control 2 register */


#define USB_PLX_PL_EP_CTRL                                0x810
#define USB_PLX_PL_EP_CTRL_ENDPOINT_SELECT_MASK           0x1F /* Bit 0:4 */
#define USB_PLX_PL_EP_CTRL_EP_INITIALIZED                 (0x1 << 16)
#define USB_PLX_PL_EP_CTRL_EP_SQU_NUM_RESET               (0x1 << 17)
#define USB_PLX_PL_EP_CTRL_CLEAR_ACK_ERROR_CODE           (0x1 << 20)

#define USB_PLX_PL_EP_CFG_4                               0x830
#define USB_PLX_PL_EP_CFG_4_NON_CTRL_IN_TOLERATE_BAD_DIR  (0x1 << 6)


#define USB_PLX_PL_EP_STATUS_1                            0x820
#define USB_PLX_PL_EP_STATUS_1_STATE_MASK                 0xFF0000 /* Bit 32 :16 */
#define USB_PLX_PL_EP_STATUS_1_ACK_GOOD_NORMAL            0x110000 /* Bit 32 :16 */
#define USB_PLX_PL_EP_STATUS_1_ACK_GOOD_MORE_ACKS_TO_COME 0x160000 /* Bit 32 :16 */


#define USB_PLX_LGO_UX_FSM_MAX_WAIT_LOOPS 200

#define USB_PLX_LGO_UX_FSM_FIELD 28

typedef enum usb_plx_lgo_ux_fsm
    {   
    /*
     * Undefined state applies to errors, debug and development.
     */
     
    USB_PLX_LGO_UX_FSM_UNDEFINED = 0,

    /* Waiting for Control Read:
     *  - A transition to this state indicates a fresh USB connection,
     *    before the first Setup Packet. The connection speed is not
     *    known. Firmware is waiting for the first Control Read.
     *  - Starting state: This state can be thought of as the FSM's typical
     *    starting state.
     *  - Tip: Upon the first SS Control Read the FSM never
     *    returns to this state.
     */
     
    USB_PLX_LGO_UX_FSM_WAITING_FOR_CONTROL_READ = 0x10000000,

    /*
     * Non-SS Control Read:
     *  - A transition to this state indicates detection of the first HS
     *    or FS Control Read.
     *  - Tip: Upon the first SS Control Read the FSM never
     *    returns to this state.
     */
     
    USB_PLX_LGO_UX_FSM_NON_SS_CONTROL_READ = 0x20000000,

    /*
     * SS Control Read:
     *  - A transition to this state indicates detection of the 
     *    first SS Control Read.
     *  - This state indicates workaround completion. Workarounds no longer
     *    need to be applied (as long as the chip remains powered up).
     *  - Tip: Once in this state the FSM state does not change (until
     *    the chip's power is lost and restored).
     *  - This can be thought of as the final state of the FSM;
     *    the FSM 'locks-up' in this state until the chip loses power.
     */
     
    USB_PLX_LGO_UX_FSM_SS_CONTROL_READ = 0x30000000,
    
    /*
     * A finite state machine for LGO_Ux rejection workaround is managed 
     * in the chip's SCRATCH register:
     *  - The nature of the LGO_Ux rejection workaround is that it must be 
     *    executed only once, after the chip powers up.
     *  - Using a chip register allows the FSM state to persist even when 
     *    the driver unloads and reloads.
     *  - Using a chip register allows the FSM state to automatically
     *    reset when the system recovers from hibernation.

     * The FSM's reset state is the chip register's power-on value:
     *  - The FSM state is managed in SCRATCH[31:28]
     *  - The chip's SCRATCH register power-on value is 0xFEEDFACE
     */
    
    USB_PLX_LGO_UX_FSM_RESET = 0xF0000000,
    
    } USB_PLX_LGO_UX_FSM, * pUSB_PLX_LGO_UX_FSM;

#ifdef __cplusplus
}
#endif

#endif /* __INCusbPlx338xh */

