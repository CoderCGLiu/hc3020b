/* usbTgtPrnLib.c - USB Target Printer Function Library module */

/*
 * Copyright (c) 2011-2013, 2015 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
Modification history
--------------------
01g,02feb15,lan  Modify for WCID support (VXW6-83565)
01f,06may13,s_z  Remove compiler warning (WIND00356717)
01e,01mar13,ghs  Start bulk out ERP when device configuration is set
                 (WIND00405773)
01d,04jan13,s_z  Remove compiler warning (WIND00390357)
01c,18may12,s_z  Add support for USB 3.0 target (WIND00326012)
01b,13dec11,m_y  Modify according to code check result (WIND00319317)
01a,21feb11,ljg  written
*/

/*
DESCRIPTION

This module contains code to exercise the usbTgtLib by emulating a rudimentary
USB printer. This module defines those routines directly referenced by the USB
peripheral stack; namely, the routines that intialize the data structure.
Additional routines are also provided which are specific to the target printer
function driver.

INCLUDE FILES: usb/usbTgt.h  usb/usbTgtPrn.h usbTgtFunc.h

*/

/* includes */

#include <usb/usbTgt.h>
#include <usb/usbTgtPrn.h>
#include <usbTgtFunc.h>

/* defines */

#define USBTGT_PRN_DEBUG_DUMP

#ifdef USBTGT_PRN_DEBUG_DUMP
UINT8 * pUsbTgtPrnDebugRcvBuf  = NULL;
UINT32 usbTgtPrnDebugRcvBufSiz = 1*1024*1024;
UINT32 usbTgtPrnDebugRcvBuflen = 0;
UINT32 tempDan;
#endif

/* import */

IMPORT USR_USBTGT_PRN_CONFIG usrUsbTgtPrnConfigTable[];

/* The global list to record all the Prn device */

LIST        gUsbTgtPrnList;
SEM_HANDLE  gUsbTgtPrnListSem;

/* local */

/* descriptor definitions */

LOCAL USB_INTERFACE_DESCR gUsbTgtPrnIfDescr =          /* Interface Descriptor */
    {
    USB_INTERFACE_DESCR_LEN,                           /* bLength */
    USB_DESCR_INTERFACE,                               /* bDescriptorType */
    USBTGT_PRN_INTERFACE_NUM,                          /* bInterfaceNumber */
    USBTGT_PRN_INTERFACE_ALT_SETTING,                  /* bAlternateSetting */
    USBTGT_PRN_NUM_ENDPOINTS,                          /* bNumEndpoints */
    USB_CLASS_PRINTER,                                 /* bInterfaceClass */
    USB_SUBCLASS_PRINTER,                              /* bInterfaceSubClass */
    USB_PROTOCOL_PRINTER_BIDIR,                        /* bInterfaceProtocol */
    0                                                  /* iInterface */
    };

LOCAL USB_ENDPOINT_DESCR gUsbTgtPrnBulkOutEpDescr =    /* Bulk Out Endpoint Descriptor */
    {
    USB_ENDPOINT_DESCR_LEN,                            /* bLength */
    USB_DESCR_ENDPOINT,                                /* bDescriptorType */
    USBTGT_PRN_BULK_OUT_ENDPOINT_NUM,                  /* bEndpointAddress */
    USB_ATTR_BULK,                                     /* bmAttributes */
    TO_LITTLEW(USBTGT_PRN_HIGH_SPEED_MAX_PACKET_SIZE), /* maxPacketSize */
    0                                                  /* bInterval */
    };

LOCAL USB_ENDPOINT_DESCR gUsbTgtPrnBulkInEpDescr =     /* Bulk In Endpoint Descriptor */
    {
    USB_ENDPOINT_DESCR_LEN,                            /* bLength */
    USB_DESCR_ENDPOINT,                                /* bDescriptorType */
    USBTGT_PRN_BULK_IN_ENDPOINT_NUM,                   /* bEndpointAddress */
    USB_ATTR_BULK,                                     /* bmAttributes */
    TO_LITTLEW(USBTGT_PRN_HIGH_SPEED_MAX_PACKET_SIZE), /* maxPacketSize */
    0                                                  /* bInterval */
    };

LOCAL USB_ENDPOINT_DESCR gUsbTgtPrnOtherSpeedBulkOutEpDescr =    /* Other Speed Bulk Out Endpoint */
                                                       /* Descriptor */
    {
    USB_ENDPOINT_DESCR_LEN,                            /* bLength */
    USB_DESCR_ENDPOINT,                                /* bDescriptorType */
    USBTGT_PRN_BULK_OUT_ENDPOINT_NUM,                  /* bEndpointAddress */
    USB_ATTR_BULK,                                     /* bmAttributes */
    TO_LITTLEW(USBTGT_PRN_FULL_SPEED_MAX_PACKET_SIZE), /* maxPacketSize */
    0                                                  /* bInterval */
    };

LOCAL USB_ENDPOINT_DESCR gUsbTgtPrnOtherSpeedBulkInEpDescr =    /* Other Speed Bulk In Endpoint */
                                                       /* Descriptor */
    {
    USB_ENDPOINT_DESCR_LEN,                            /* bLength */
    USB_DESCR_ENDPOINT,                                /* bDescriptorType */
    USBTGT_PRN_BULK_IN_ENDPOINT_NUM,                   /* bEndpointAddress */
    USB_ATTR_BULK,                                     /* bmAttributes */
    TO_LITTLEW(USBTGT_PRN_FULL_SPEED_MAX_PACKET_SIZE), /* maxPacketSize */
    0                                                  /* bInterval */
    };

LOCAL USB_ENDPOINT_DESCR gUsbTgtPrnSsBulkOutEpDescr =    /* Bulk Out Endpoint Descriptor */
    {
    USB_ENDPOINT_DESCR_LEN,                            /* bLength */
    USB_DESCR_ENDPOINT,                                /* bDescriptorType */
    USBTGT_PRN_BULK_OUT_ENDPOINT_NUM,                  /* bEndpointAddress */
    USB_ATTR_BULK,                                     /* bmAttributes */
    TO_LITTLEW(1024),                                  /* maxPacketSize */
    0                                                  /* bInterval */
    };
LOCAL USB_ENDPOINT_COMPANION_DESCR gUsbTgtPrnSsBulkOutEpCompDescr = /* companion descriptor */
    {
    USB_ENDPOINT_COMP_DESCR_LEN,                 /* bLength */
    USB_DESCR_SS_ENDPOINT_COMPANION,             /* bDescriptorType */
    0x0,                                         /* bMaxBurst */
    0x0,                                         /* bmAttributes */
    0x0                                          /* wBytesPerInterval */
    };

LOCAL USB_ENDPOINT_DESCR gUsbTgtPrnSsBulkInEpDescr =     /* Bulk In Endpoint Descriptor */
    {
    USB_ENDPOINT_DESCR_LEN,                            /* bLength */
    USB_DESCR_ENDPOINT,                                /* bDescriptorType */
    USBTGT_PRN_BULK_IN_ENDPOINT_NUM,                   /* bEndpointAddress */
    USB_ATTR_BULK,                                     /* bmAttributes */
    TO_LITTLEW(1024),                                  /* maxPacketSize */
    0                                                  /* bInterval */
    };
LOCAL USB_ENDPOINT_COMPANION_DESCR gUsbTgtPrnSsBulkInEpCompDescr = /* companion descriptor */
    {
    USB_ENDPOINT_COMP_DESCR_LEN,                 /* bLength */
    USB_DESCR_SS_ENDPOINT_COMPANION,             /* bDescriptorType */
    0x0,                                         /* bMaxBurst */
    0x0,                                         /* bmAttributes */
    0x0                                          /* wBytesPerInterval */
    };

LOCAL pUSB_DESCR_HDR gUsbTgtPrnOtherSpeedDescHdr[] =
    {
    (pUSB_DESCR_HDR )&gUsbTgtPrnIfDescr,                  /* Interface descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtPrnOtherSpeedBulkOutEpDescr, /* Endpoint descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtPrnOtherSpeedBulkInEpDescr,  /* Endpoint descriptor */
    NULL                                                  /*  */
    };

LOCAL pUSB_DESCR_HDR gUsbTgtPrnHighSpeedDescHdr[] =
    {
    (pUSB_DESCR_HDR )&gUsbTgtPrnIfDescr,                  /* Interface descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtPrnBulkOutEpDescr,           /* Endpoint descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtPrnBulkInEpDescr,            /* Endpoint descriptor */
    NULL                                                  /*  */
    };

LOCAL pUSB_DESCR_HDR gUsbTgtPrnSuperSpeedDescHdr[] =
    {
    (pUSB_DESCR_HDR )&gUsbTgtPrnIfDescr,                  /* Interface descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtPrnSsBulkOutEpDescr,         /* Endpoint descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtPrnSsBulkOutEpCompDescr,     /* BULK OUT EP comp descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtPrnSsBulkInEpDescr,          /* Endpoint descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtPrnSsBulkInEpCompDescr,      /* BULK IN EP comp descriptor */
    NULL                                                  /* END */
    };

LOCAL STATUS usbTgtPrnMngmtFunc
    (
    pVOID               param,
    USB_TARG_CHANNEL    targChannel,
    UINT16              mngmtCode,
    pVOID               pContext
    );

LOCAL STATUS usbTgtPrnFeatureClear
    (
    pVOID               param,
    USB_TARG_CHANNEL    targChannel,
    UINT8               requestType,
    UINT16              feature,
    UINT16              index
    );

LOCAL STATUS usbTgtPrnFeatureSet
    (
    pVOID               param,
    USB_TARG_CHANNEL    targChannel,
    UINT8               requestType,
    UINT16              feature,
    UINT16              index
    );

LOCAL STATUS usbTgtPrnConfigurationSet
    (
    pVOID               param,
    USB_TARG_CHANNEL    targChannel,
    UINT8               configuration
    );

LOCAL STATUS usbTgtPrnInterfaceGet
    (
    pVOID               param,
    USB_TARG_CHANNEL    targChannel,
    UINT16              interfaceIndex,
    pUINT8              pAlternateSetting
    );

LOCAL STATUS usbTgtPrnInterfaceSet
    (
    pVOID               param,
    USB_TARG_CHANNEL    targChannel,
    UINT16              interfaceIndex,
    UINT8               alternateSetting
    );

LOCAL STATUS usbTgtPrnVendorSpecific
    (
    pVOID               param,
    USB_TARG_CHANNEL    targChannel,
    UINT8               requestType,
    UINT8               request,
    UINT16              value,
    UINT16              index,
    UINT16              length
    );

LOCAL USB_TARG_CALLBACK_TABLE gUsbTgtPrnCallbackTable =  /* Callback Table */
    {
    usbTgtPrnMngmtFunc,             /* usbTgtPrnMngmtFunc */
    usbTgtPrnFeatureClear,          /* usbTgtPrnFeatureClear */
    usbTgtPrnFeatureSet,            /* usbTgtPrnFeatureSet */
    NULL,                           /* usbTgtPrnConfigurationGet */
    usbTgtPrnConfigurationSet,      /* usbTgtPrnConfigurationSet */
    NULL,                           /* usbTgtPrnDescriptorGet */
    NULL,                           /* descriptorSet */
    usbTgtPrnInterfaceGet,          /* usbTgtPrnInterfaceGet */
    usbTgtPrnInterfaceSet,          /* usbTgtPrnInterfaceSet */
    NULL,                           /* statusGet */
    NULL,                           /* addressSet */
    NULL,                           /* synchFrameGet */
    usbTgtPrnVendorSpecific         /* usbTgtPrnVendorSpecific */
    };

LOCAL BOOL  gUsbTgtPrnInited = FALSE;

/* functions */

/*******************************************************************************
*
* usbTgtPrnManagementNotify - send message to usbTgtPrnManagerTask
*
* This routine is used to send transfer message to usbTgtPrnManagerTask.
*
* RETURNS: N/A.
*
* ERRNO: N/A
*/

LOCAL void usbTgtPrnManagementNotify
    (
    pUSBTGT_PRN_DEV          pUsbTgtPrnDev,
    USBTGT_PRN_NOTIFY_CODE   devNotifyCode
    )
    {
    USBTGT_PRN_NOTIFY_INFO  prnNotifyInfo;

    if ((NULL == pUsbTgtPrnDev) ||
        (NULL == pUsbTgtPrnDev->prnTaskMsgQID) ||
        (USBTGT_PRN_NOTIFY_UNKNOWN == devNotifyCode))
        {
        USBTGT_PRN_ERR("MsgQueue doesn't exist.\n",
                       1, 2, 3, 4, 5 ,6);

        return;
        }

    /* Fill the information */

    prnNotifyInfo.pUsbTgtPrn = pUsbTgtPrnDev;
    prnNotifyInfo.notifyCode = devNotifyCode;

    /* Send mesQ to task */

    if (OK != msgQSend (pUsbTgtPrnDev->prnTaskMsgQID,
                        (char*) &prnNotifyInfo,
                        sizeof(USBTGT_PRN_NOTIFY_INFO),
                        USBTGT_WAIT_TIMEOUT,
                        MSG_PRI_NORMAL))
        {
        USBTGT_PRN_ERR("Message Send Failed.\n", 1, 2, 3, 4, 5 ,6);

        }
    return ;
    }

/******************************************************************************
*
* usbTgtPrnFeatureClear - clear the specified feature
*
* This routine implements the clear feature standard device request.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtPrnFeatureClear
    (
    pVOID               param,          /* TCD specific parameter */
    USB_TARG_CHANNEL    targChannel,    /* target channel */
    UINT8               requestType,    /* request type */
    UINT16              feature,        /* feature to clear */
    UINT16              index           /* index */
    )
    {
    pUSBTGT_PRN_DEV     pUsbTgtPrnDev = (pUSBTGT_PRN_DEV) param;
    USB_TARG_PIPE       pipeHandle = NULL;
    UINT8               uInterface;
    UINT8               uSuspendOp;

    /* check parameter */

    if (pUsbTgtPrnDev == NULL)
        {
        USBTGT_PRN_ERR("parameter error.\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* This request must be standard request from the host */

    if (((requestType & USB_RT_DIRECTION_MASK) != USB_RT_HOST_TO_DEV) ||
        ((requestType & USB_RT_CATEGORY_MASK) != USB_RT_STANDARD))
        {
        USBTGT_PRN_ERR("unsupport requestType.\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    requestType = (UINT8) (requestType & ~(USB_RT_DIRECTION_MASK | USB_RT_CATEGORY_MASK));

    if ((requestType == USB_RT_ENDPOINT) &&
        (feature == USB_FSEL_DEV_ENDPOINT_HALT))
        {
        USBTGT_PRN_DBG("Clear ENDPOINT_HALT Feature Ep index 0x%X\n", index,
                       2, 3, 4, 5, 6);

        pipeHandle = usbTgtFuncFindPipeHandleByEpIndex(pUsbTgtPrnDev->targChannel,
                                              (UINT8) (index & 0xFF));

        if (pipeHandle == pUsbTgtPrnDev->bulkOutPipeHandle)
            {
            pUsbTgtPrnDev->uBulkOutPipeStatus = USBTGT_PRN_PIPE_UNSTALL;

            USBTGT_PRN_DBG("Set Feature is called to inform the bulk out "
                           "pipe 0x%X with EpIndex 0x%X has been unstalled.\n",
                           (ULONG)pipeHandle, index, 3, 4, 5, 6);
            }
        else if (pipeHandle == pUsbTgtPrnDev->bulkInPipeHandle)
            {
            pUsbTgtPrnDev->uBulkInPipeStatus = USBTGT_PRN_PIPE_UNSTALL;

            USBTGT_PRN_DBG("Set Feature is called to inform the bulk in "
                           "pipe 0x%X with EpIndex 0x%X has been unstalled.\n",
                           (ULONG)pipeHandle, index, 3, 4, 5, 6);
            }
        else
            {
            USBTGT_PRN_ERR("pUsbTgtPipe 0x%X is not Prn bulk in or bulk out "
                           "pipe.\n", (ULONG)pipeHandle, index, 3, 4, 5, 6);
            return ERROR;
            }
        }
    else if ((requestType == USB_RT_INTERFACE) &&
             (feature == USB_FSEL_DEV_FUNCTION_SUSPEND))
        {
        /* The MSB is the specific suspend options */

        uSuspendOp = (UINT8) (index >> 8);
        uInterface = (UINT8) (index & 0xFF);

        USBTGT_PRN_DBG ("Clear interface %d suspendop %x\n",
                        uInterface, uSuspendOp, 3, 4, 5, 6);

        return OK;
        }

    USBTGT_PRN_DBG("requestType = 0x%X feature = 0x%X.\n", requestType,
                   feature, 3, 4, 5, 6);

    return OK;
    }

/*******************************************************************************
*
* usbTgtPrnFeatureSet - set the specified feature
*
* This routine implements the set feature standard device request.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtPrnFeatureSet
    (
    pVOID               param,          /* TCD specific parameter */
    USB_TARG_CHANNEL    targChannel,    /* target channel */
    UINT8               requestType,    /* request type */
    UINT16              feature,        /* feature to set */
    UINT16              index           /* wIndex */
    )
    {
    pUSBTGT_PRN_DEV     pUsbTgtPrnDev = (pUSBTGT_PRN_DEV) param;
    USB_TARG_PIPE       pipeHandle = NULL;
    UINT8               uInterface;
    UINT8               uSuspendOp;

    /* check parameter */

    if (pUsbTgtPrnDev == NULL)
        {
        USBTGT_PRN_ERR("parameter error.\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* This request must be standard request from the host */

    if (((requestType & USB_RT_DIRECTION_MASK) != USB_RT_HOST_TO_DEV) ||
        ((requestType & USB_RT_CATEGORY_MASK) != USB_RT_STANDARD))
        {
        USBTGT_PRN_ERR("unsupport requestType.\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    requestType = (UINT8) (requestType & ~(USB_RT_DIRECTION_MASK | USB_RT_CATEGORY_MASK));

    if ((requestType == USB_RT_ENDPOINT) &&
        (feature == USB_FSEL_DEV_ENDPOINT_HALT))
        {
        USBTGT_PRN_DBG("Clear ENDPOINT_HALT Feature Ep index 0x%X\n",
                       index, 2, 3, 4, 5, 6);

        pipeHandle = usbTgtFuncFindPipeHandleByEpIndex(pUsbTgtPrnDev->targChannel,
                                              (UINT8) (index & 0xFF));

        if (NULL == pipeHandle)
            {
            USBTGT_PRN_ERR("Can not find the pipe from the Ep index 0x%X\n",
                           index, 2, 3, 4, 5, 6);

            return ERROR;
            }

        if (pipeHandle == pUsbTgtPrnDev->bulkOutPipeHandle)
            {
            pUsbTgtPrnDev->uBulkOutPipeStatus = USBTGT_PRN_PIPE_STALL;

            USBTGT_PRN_DBG("Set Feature is called to inform the bulk out "
                           "pipe 0x%X with EpIndex 0x%X has been stalled.\n",
                           (ULONG)pipeHandle, index, 3, 4, 5, 6);
            }
        else if (pipeHandle == pUsbTgtPrnDev->bulkInPipeHandle)
            {
            pUsbTgtPrnDev->uBulkInPipeStatus = USBTGT_PRN_PIPE_STALL;

            USBTGT_PRN_DBG("Set Feature is called to inform the bulk in "
                           "pipe 0x%X with EpIndex 0x%X has been stalled.\n",
                           (ULONG)pipeHandle, index, 3, 4, 5, 6);
            }
        else
            {
            USBTGT_PRN_ERR("pUsbTgtPipe 0x%X is not Prn bulk in or bulk out "
                           "pipe.\n", (ULONG)pipeHandle, index, 3, 4, 5, 6);
            return ERROR;
            }
        }
    else if ((requestType == USB_RT_INTERFACE) &&
             (feature == USB_FSEL_DEV_FUNCTION_SUSPEND))
        {
        /* The MSB is the specific suspend options */

        uSuspendOp = (UINT8) (index >> 8);
        uInterface = (UINT8) (index & 0xFF);

        USBTGT_PRN_DBG ("Set interface %d suspendop %x\n",
                        uInterface, uSuspendOp, 3, 4, 5, 6);

        return OK;
        }

    USBTGT_PRN_DBG("requestType = 0x%X feature = 0x%X.\n", requestType,
                   feature, 3, 4, 5, 6);

    return OK;
    }

/*******************************************************************************
*
* usbTgtPrnInterfaceGet - get the specified interface
*
* This routine is used to get the selected alternate setting of the
* specified interface.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtPrnInterfaceGet
    (
    pVOID               param,              /* TCD specific parameter */
    USB_TARG_CHANNEL    targChannel,        /* target channel */
    UINT16              interfaceIndex,     /* interface index */
    pUINT8              pAlternateSetting   /* alternate setting */
    )
    {
    pUSBTGT_PRN_DEV     pUsbTgtPrnDev = (pUSBTGT_PRN_DEV) param;

    /* check parameter */

    if (pUsbTgtPrnDev == NULL)
        {
        USBTGT_PRN_ERR("parameter error.\n", 1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    if (pUsbTgtPrnDev->uConfigurationValue == 0)
        {
        USBTGT_PRN_ERR("configuration not set.\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    USBTGT_PRN_DBG("usbTgtPrnInterfaceGet(): uAlternateSetting = %d.\n",
                   pUsbTgtPrnDev->uAlternateSetting, 2, 3, 4, 5, 6);

    *pAlternateSetting = pUsbTgtPrnDev->uAlternateSetting;

    return OK;
    }

/*******************************************************************************
*
* usbTgtPrnInterfaceSet - set the specified interface
*
* This routine is used to select the alternate setting of the specified
* interface.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtPrnInterfaceSet
    (
    pVOID               param,              /* TCD specific parameter */
    USB_TARG_CHANNEL    targChannel,        /* target channel */
    UINT16              interfaceIndex,     /* interface index */
    UINT8               alternateSetting    /* alternate setting */
    )
    {
    pUSBTGT_PRN_DEV     pUsbTgtPrnDev = (pUSBTGT_PRN_DEV) param;

    /* check parameter */

    if (pUsbTgtPrnDev == NULL)
        {
        USBTGT_PRN_ERR("parameter error.\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    if (pUsbTgtPrnDev->uConfigurationValue == 0)
        {
        USBTGT_PRN_ERR("configuration not set.\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    USBTGT_PRN_DBG("usbTgtPrnInterfaceSet(): alternateSetting = %d.\n",
                   alternateSetting, 2, 3, 4, 5, 6);

    pUsbTgtPrnDev->uAlternateSetting = alternateSetting;

    return OK;
    }

/*******************************************************************************
*
* usbTgtPrnVendorSpecific - handle the VENDOR_SPECIFIC(Class-Specific) request
*
* This routine implements the vendor specific standard device request
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtPrnVendorSpecific
    (
    pVOID                     param,          /* TCD specific parameter */
    USB_TARG_CHANNEL          targChannel,    /* target channel */
    UINT8                     requestType,    /* request type */
    UINT8                     request,        /* request name */
    UINT16                    value,          /* wValue */
    UINT16                    index,          /* wIndex */
    UINT16                    length          /* wLength */
    )
    {
    pUSBTGT_PRN_DEV           pUsbTgtPrnDev = (pUSBTGT_PRN_DEV) param;

    UINT8                     bfr [USB_PRN_MAX_DEVICE_ID_LEN]; /* Buffer to hold data */
    pUSB_PRINTER_CAPABILITIES pCaps   = (pUSB_PRINTER_CAPABILITIES) bfr;
    UINT16                    capsLen =
        (UINT16)(strlen (usrUsbTgtPrnConfigTable[index].prnCapsString) + 2);
    STATUS                    status = ERROR;

    /* check parameter */

    if (pUsbTgtPrnDev == NULL ||
        pUsbTgtPrnDev->targChannel != targChannel)
        {
        USBTGT_PRN_ERR("parameter error.\n", 1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    USBTGT_PRN_DBG("usbTgtPrnVendorSpecific(): requestType %p request %p "
                   "value %p index %p length %p\n", requestType, request,
                   value, index, length, 6);

    if (requestType == (USB_RT_DEV_TO_HOST | USB_RT_CLASS | USB_RT_INTERFACE))
        {
        switch (request)
            {
            case USB_REQ_PRN_GET_DEVICE_ID:

                /* Send the IEEE-1284-style "device id" string. */
                /* The first two bytes are the length in big endian format. */

                pCaps->length = (UINT16)TO_BIGW (capsLen);

                /* The capsLen is not more than USB_PRN_MAX_DEVICE_ID_LEN. */

                memcpy (pCaps->caps,
                        usrUsbTgtPrnConfigTable[index].prnCapsString,
                        strlen (usrUsbTgtPrnConfigTable[index].prnCapsString));

                (void) usbTgtControlResponseSend (targChannel, capsLen,
                                           (pUINT8) pCaps,
                                           USB_ERP_FLAG_NORMAL);
                break;

            case USB_REQ_PRN_GET_PORT_STATUS:

                /* Initiate the data phase for sending the port status */

                (void) usbTgtControlResponseSend (targChannel,
                                           sizeof (pUsbTgtPrnDev->prnState),
                                           (pUINT8) &pUsbTgtPrnDev->prnState,
                                           USB_ERP_FLAG_NORMAL);
                break;

            default:
                USBTGT_PRN_ERR("Unsupport parameter request 0x%x.\n",
                               request, 2, 3, 4, 5, 6);
                return ERROR;
            }
        }

    else if (requestType == (USB_RT_HOST_TO_DEV | USB_RT_CLASS | USB_RT_OTHER))
        {
        switch (request)
            {
            case USB_REQ_PRN_SOFT_RESET:

                /* We accept the SOFT_RESET and initiate the status phase */

                pUsbTgtPrnDev->bBulkOutBufValid = FALSE;
                pUsbTgtPrnDev->bBulkInBufValid = FALSE;

                if (pUsbTgtPrnDev->bBulkOutErpUsed == TRUE)
                    {
                    if (usbTgtCancelErp(pUsbTgtPrnDev->bulkOutPipeHandle,
                                        &pUsbTgtPrnDev->bulkOutErp) != OK)
                        {
                        USBTGT_PRN_ERR("usbTgtCancelErp failed for Bulk Out.\n",
                                       1, 2, 3, 4, 5, 6);
                        return ERROR;
                        }

                    pUsbTgtPrnDev->bBulkOutErpUsed = FALSE;
                    }

                if (pUsbTgtPrnDev->bBulkInErpUsed == TRUE)
                    {
                    if (usbTgtCancelErp(pUsbTgtPrnDev->bulkInPipeHandle,
                                        &pUsbTgtPrnDev->bulkInErp) != OK)
                        {
                        USBTGT_PRN_ERR("usbTgtCancelErp failed for Bulk In.\n",
                                       1, 2, 3, 4, 5, 6);
                        return ERROR;
                        }

                    pUsbTgtPrnDev->bBulkInErpUsed = FALSE;
                    }

                status = usbTgtControlStatusSend (targChannel, FALSE);
                break;

            default:
                USBTGT_PRN_ERR("Unsupport parameter request 0x%x.\n",
                               request, 2, 3, 4, 5, 6);
                return ERROR;
            }
        }

    return status;
    }

/*******************************************************************************
*
* usbTgtPrnMngmtFunc - invoke the connection management events
*
* This function handles various management related events. <mngmtCode>
* consist of the management event function code that is reported by the
* TargLib layer. <pContext> is the argument sent for the management event to
* be handled.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtPrnMngmtFunc
    (
    pVOID               param,                    /* TCD specific parameter */
    USB_TARG_CHANNEL    targChannel,              /* target channel */
    UINT16              mngmtCode,                /* management code */
    pVOID               pContext                  /* context value */
    )
    {
    pUSBTGT_PRN_DEV         pUsbTgtPrnDev = (pUSBTGT_PRN_DEV) param;

    /* check parameter */

    if (pUsbTgtPrnDev == NULL)
        {
        USBTGT_PRN_ERR("parameter error.\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    switch (mngmtCode)
        {
        case TARG_MNGMT_ATTACH:
            USBTGT_PRN_DBG("usbTgtPrnMngmtFunc(): TARG_MNGMT_ATTACH targChannel"
                           " = %p\n", targChannel, 2, 3, 4, 5, 6);

            if (pContext == NULL)
                {
                USBTGT_PRN_ERR("usbTgtPrnMngmtFunc(): TARG_MNGMT_ATTACH "
                               "pContext = %p\n", pContext, 2, 3, 4, 5, 6);
                return ERROR;
                }

            pUsbTgtPrnDev->targChannel = targChannel;
            pUsbTgtPrnDev->uConfigurationValue = 0;
            pUsbTgtPrnDev->uAlternateSetting = 0;
            pUsbTgtPrnDev->bulkOutPipeHandle = NULL;
            pUsbTgtPrnDev->bulkInPipeHandle = NULL;

            break;

        case TARG_MNGMT_DETACH:
            USBTGT_PRN_DBG("usbTgtPrnMngmtFunc(): TARG_MNGMT_DETACH\n",
                           1, 2, 3, 4, 5, 6);

            /* Call the function to delete the bulk pipe created */

            if (pUsbTgtPrnDev->bulkInPipeHandle != NULL)
                {
                usbTgtDeletePipe(pUsbTgtPrnDev->bulkInPipeHandle);
                pUsbTgtPrnDev->bulkInPipeHandle = NULL;
                pUsbTgtPrnDev->bBulkInErpUsed = FALSE;
                }

            if (pUsbTgtPrnDev->bulkOutPipeHandle != NULL)
                {
                usbTgtDeletePipe (pUsbTgtPrnDev->bulkOutPipeHandle);
                pUsbTgtPrnDev->bulkOutPipeHandle = NULL;
                pUsbTgtPrnDev->bBulkOutErpUsed = FALSE;
                }

            /* De-allocate buffer */

            if (pUsbTgtPrnDev->pBulkOutBuf != NULL)
                {
                OSS_FREE (pUsbTgtPrnDev->pBulkOutBuf);
                pUsbTgtPrnDev->pBulkOutBuf = NULL;
                }

            if (pUsbTgtPrnDev->pBulkInBuf != NULL)
                {
                OSS_FREE (pUsbTgtPrnDev->pBulkInBuf);
                pUsbTgtPrnDev->pBulkInBuf = NULL;
                }

            pUsbTgtPrnDev->targChannel = 0;
            pUsbTgtPrnDev->uDeviceFeature = 0;

            break;

        case TARG_MNGMT_BUS_RESET:
            USBTGT_PRN_DBG("usbTgtPrnMngmtFunc(): TARG_MNGMT_BUS_RESET\n",
                           1, 2, 3, 4, 5, 6);

            pUsbTgtPrnDev->uConfigurationValue = 0;
            pUsbTgtPrnDev->uAlternateSetting = 0;
            pUsbTgtPrnDev->bBulkOutErpUsed = FALSE;
            pUsbTgtPrnDev->bBulkOutBufValid = FALSE;
            pUsbTgtPrnDev->bBulkInErpUsed = FALSE;
            pUsbTgtPrnDev->bBulkInBufValid = FALSE;

            /* Call the function to delete the created bulk pipes */

            if (pUsbTgtPrnDev->bulkInPipeHandle != NULL)
                {
                usbTgtDeletePipe(pUsbTgtPrnDev->bulkInPipeHandle);
                pUsbTgtPrnDev->bulkInPipeHandle = NULL;
                pUsbTgtPrnDev->bBulkInErpUsed = FALSE;
                }

            if (pUsbTgtPrnDev->bulkOutPipeHandle != NULL)
                {
                usbTgtDeletePipe (pUsbTgtPrnDev->bulkOutPipeHandle);
                pUsbTgtPrnDev->bulkOutPipeHandle = NULL;
                pUsbTgtPrnDev->bBulkOutErpUsed = FALSE;
                }

            break;

        case TARG_MNGMT_DISCONNECT:
            USBTGT_PRN_DBG("usbTgtPrnMngmtFunc(): TARG_MNGMT_DISCONNECT\n",
                           1, 2, 3, 4, 5, 6);

            usbTgtPrnConfigurationSet (param, targChannel, 0);

            break;

        case TARG_MNGMT_SUSPEND:
            USBTGT_PRN_DBG("usbTgtPrnMngmtFunc(): TARG_MNGMT_SUSPEND\n",
                           1, 2, 3, 4, 5, 6);

            /* when device change to suspend state, must not output */

            break;

        case TARG_MNGMT_RESUME:
            USBTGT_PRN_DBG("usbTgtPrnMngmtFunc(): TARG_MNGMT_RESUME\n",
                           1, 2, 3, 4, 5, 6);
            break;

        default:
            USBTGT_PRN_ERR("usbTgtPrnMngmtFunc(): parameter mngmtCode=0x%x.",
                           mngmtCode, 2, 3, 4, 5, 6);
            return ERROR;
        }

    return OK;
    }

/*******************************************************************************
*
* usbTgtPrnBulkErpInit - initialize the ERP bulk data structure
*
* This routine initializes the ERP bulk data structure.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbTgtPrnBulkErpInit
    (
    pUSB_ERP            pErp,
    UINT8 *             pData,          /* pointer to data */
    UINT32              size,           /* size of data */
    ERP_CALLBACK        erpCallback,    /* erp callback */
    pVOID               usrPtr,         /* user pointer */
    UINT8               direction
    )
    {
    /* check parameter */

    if (pErp == NULL)
        {
        USBTGT_PRN_ERR("parameter error. pErp is NULL. \n", 1, 2, 3, 4, 5, 6);
        return;
        }

    memset(pErp, 0, sizeof(USB_ERP));

    USBTGT_PRN_DBG("usbTgtPrnBulkErpInit(): pErp 0x%p,0x%x.\n",
                   pErp, * (pUINT32)pErp, 3, 4, 5, 6);

    pErp->erpLen = sizeof(USB_ERP);
    pErp->userCallback = erpCallback;
    pErp->targCallback = usbTgtErpCallback;
    pErp->bfrCount = 1;

    pErp->bfrList[0].pid = direction;
    pErp->bfrList[0].pBfr = pData;
    pErp->bfrList[0].bfrLen = size;
    pErp->userPtr = usrPtr;

    return;
    }

/*******************************************************************************
*
* usbTgtPrnBulkInErpInit - initialize the bulk-in ERP
*
* This routine initializes the Bulk In ERP.
*
* RETURNS: OK, or ERROR if unable to submit ERP.
*
* ERRNO: N/A
*/

LOCAL STATUS usbTgtPrnBulkInErpInit
    (
    pUSBTGT_PRN_DEV     pUsbTgtPrnDev,
    UINT8 *             pData,          /* pointer to data */
    UINT32              size,           /* size of data */
    ERP_CALLBACK        erpCallback,    /* erp callback */
    pVOID               usrPtr          /* user pointer */
    )
    {
    /* check parameter */

    if (pData == NULL ||
        pUsbTgtPrnDev == NULL ||
        pUsbTgtPrnDev->bBulkInErpUsed == TRUE)
        {
        USBTGT_PRN_ERR("parameter error.\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    usbTgtPrnBulkErpInit(&pUsbTgtPrnDev->bulkInErp, pData, size,
                         erpCallback, usrPtr, USB_PID_IN);

    pUsbTgtPrnDev->bBulkInErpUsed = TRUE;
    pUsbTgtPrnDev->bBulkInBufValid = FALSE;

    USBTGT_PRN_VDBG("usbTgtPrnBulkInErpInit(): submit erp data, len [%d]\n",
                    size, 2, 3, 4, 5, 6);

    if (usbTgtSubmitErp (pUsbTgtPrnDev->bulkInPipeHandle,
                         &pUsbTgtPrnDev->bulkInErp) != OK)
        {
        pUsbTgtPrnDev->bBulkInErpUsed = FALSE;
        USBTGT_PRN_ERR("usbTgtSubmitErp error in bulk in.\n",
                       1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    return OK;
    }

/***************************************************************************
*
* usbTgtPrnBulkOutErpInit - initialize the bulk-Out ERP
*
* This routine initializes the bulk Out ERP.
*
* RETURNS: OK, or ERROR if unable to submit ERP.
*
* ERRNO: N/A
*/

LOCAL STATUS usbTgtPrnBulkOutErpInit
    (
    pUSBTGT_PRN_DEV     pUsbTgtPrnDev,
    UINT8 *             pData,          /* pointer to buffer */
    UINT32              size,           /* size of data */
    ERP_CALLBACK        erpCallback,    /* IRP_CALLBACK */
    pVOID               usrPtr          /* user pointer */
    )
    {
    /* check parameter */

    if (pData == NULL ||
        pUsbTgtPrnDev == NULL ||
        pUsbTgtPrnDev->bBulkOutErpUsed == TRUE)
        {
        USBTGT_PRN_ERR("Invalid parameter %s is NULL\n",
                       (ULONG)((NULL == pData) ?
                       "pData" : (NULL == pUsbTgtPrnDev) ?
                       "pUsbTgtPrnDev" :
                       "pUsbTgtPrnDev->bBulkOutErpUsed == TRUE"),
                       2, 3, 4, 5, 6);

        return ERROR;
        }

    usbTgtPrnBulkErpInit(&pUsbTgtPrnDev->bulkOutErp, pData, size,
                         erpCallback, usrPtr, USB_PID_OUT);

    pUsbTgtPrnDev->bBulkOutErpUsed = TRUE;
    pUsbTgtPrnDev->bBulkOutBufValid = FALSE;

    USBTGT_PRN_VDBG("usbTgtPrnBulkOutErpInit(): submit erp data, len [%d]\n",
                    size, 2, 3, 4, 5, 6);

    if (usbTgtSubmitErp (pUsbTgtPrnDev->bulkOutPipeHandle,
                         &pUsbTgtPrnDev->bulkOutErp) != OK)
        {
        pUsbTgtPrnDev->bBulkOutErpUsed = FALSE;

        USBTGT_PRN_ERR("usbTgtSubmitErp error in USB_PID_OUT\n",
                       1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    return OK;
    }

/*******************************************************************************
*
* usbTgtPrnDestroyAllPipes - abort pending requests and destroy for all pipes
*
* This routine is used to destroy all pipes. Before destroying the pipes,
* all requests pending for these pipes are also aborted.
*
* RETURNS: OK, or ERROR if fails to perform the action.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtPrnDestroyAllPipes
    (
    pUSBTGT_PRN_DEV   pUsbTgtPrnDev
    )
    {
    /* if the bulk in pipe is in use, try to clear it */

    if (pUsbTgtPrnDev->bBulkInErpUsed == TRUE)
        {
        if (usbTgtCancelErp(pUsbTgtPrnDev->bulkInPipeHandle,
                            &pUsbTgtPrnDev->bulkInErp) != OK)
            {
            USBTGT_PRN_ERR("usbTgtCancelErp failed for BULK IN.\n",
                           1, 2, 3, 4, 5, 6);
            }

        pUsbTgtPrnDev->bBulkInErpUsed = FALSE;
        }

    /* if the bulk out pipe is in use, try to clear it */

    if (pUsbTgtPrnDev->bBulkOutErpUsed == TRUE)
        {
        if (usbTgtCancelErp(pUsbTgtPrnDev->bulkOutPipeHandle,
                            &pUsbTgtPrnDev->bulkOutErp) != OK)
            {
            USBTGT_PRN_ERR("usbTgtCancelErp failed for BULK OUT.\n",
                           1, 2, 3, 4, 5, 6);
            }

        pUsbTgtPrnDev->bBulkOutErpUsed = FALSE;
        }

    /* Destroy the bulk in pipe if already created */

    if (pUsbTgtPrnDev->bulkInPipeHandle != NULL)
        {
        usbTgtDeletePipe (pUsbTgtPrnDev->bulkInPipeHandle);
        pUsbTgtPrnDev->bulkInPipeHandle = NULL;
        pUsbTgtPrnDev->bBulkInErpUsed = FALSE;
        }

    /* Destroy the bulk out pipe if already created */

    if (pUsbTgtPrnDev->bulkOutPipeHandle != NULL)
        {
        usbTgtDeletePipe (pUsbTgtPrnDev->bulkOutPipeHandle);
        pUsbTgtPrnDev->bulkOutPipeHandle = NULL;
        pUsbTgtPrnDev->bBulkOutErpUsed = FALSE;
        }

    return OK;
    }


/*******************************************************************************
*
* usbTgtPrnCreateAllPipes - create all pipes needed by this module
*
* This routine is used to create all pipes needed by this module.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtPrnCreateAllPipes
    (
    pUSBTGT_PRN_DEV   pUsbTgtPrnDev,
    UINT8             uConfig
    )
    {
    pUSB_ENDPOINT_DESCR      pEpDesc = NULL;
    USBTGT_PIPE_CONFIG_PARAM ConfigParam;
    UINT8                    uIfBase;

    /* Validate parameters */

    if (NULL == pUsbTgtPrnDev)
        {
        USBTGT_PRN_ERR("Invalid parameter pUsbTgtKbdDev is NULL\n",
                       1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * This function driver may be part of one compiste device
     * Get the interface base.
     */

    if ( ERROR == usbTgtFuncInfoMemberGet(pUsbTgtPrnDev->targChannel,
                                     USBTGT_FUNC_INFO_MEM_TYPE_uIfNumMin,
                                     &uIfBase))
        {
        USBTGT_PRN_ERR("Can not get the interface base number\n",
                        1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Find the right endpoint descriptor */

    pEpDesc = usbTgtFuncEndpointDescFind (pUsbTgtPrnDev->targChannel,
                          uIfBase,            /* Only one interface */
                          0,                  /* Altsetting 0 */
                          USB_ATTR_BULK,      /* Bulk */
                          USB_ENDPOINT_IN,    /* IN */
                          0,                  /* Synch Type */
                          0,                  /* Usage */
                          1);                 /* The first one */

    /* Create the pipe for the endpoint */

    if (usbTgtCreatePipe (pUsbTgtPrnDev->targChannel,
                          pEpDesc,
                          uConfig,
                          uIfBase,
                          0,
                              &pUsbTgtPrnDev->bulkInPipeHandle) != OK)
            {
        USBTGT_PRN_ERR("Create for interrupt pipe failed.\n",
                           1, 2, 3, 4, 5, 6);

            return ERROR;
            }

    /* Set the configuration parameters */

    ConfigParam.uMaxErpCount = 1;
    ConfigParam.uMaxErpBufCount = 1;

    /* The max buffer size is 2K */

    ConfigParam.uMaxErpBufSize = USBTGT_PRN_MAX_ERP_BUF_SIZE;

    /* Configure the pipe */

    if (ERROR ==  usbTgtConfigPipe(pUsbTgtPrnDev->bulkInPipeHandle,
                                   &ConfigParam))
        {
        USBTGT_PRN_ERR("Configure for interrupt pipe failed.\n",
                       1, 2, 3, 4, 5, 6);

        usbTgtPrnDestroyAllPipes(pUsbTgtPrnDev);

        return ERROR;
        }

    /* Find the right endpoint descriptor */

    pEpDesc = usbTgtFuncEndpointDescFind (pUsbTgtPrnDev->targChannel,
                          uIfBase,            /* Only one interface */
                          0,                  /* Altsetting 0 */
                          USB_ATTR_BULK,      /* Bulk */
                          USB_ENDPOINT_OUT,   /* OUT */
                          0,                  /* Synch Type */
                          0,                  /* Usage */
                          1);                 /* The first one */

    /* Create the pipe for the endpoint */

    if (usbTgtCreatePipe (pUsbTgtPrnDev->targChannel,
                          pEpDesc,
                          uConfig,
                          uIfBase,
                          0,
                             &pUsbTgtPrnDev->bulkOutPipeHandle) != OK)
            {
        USBTGT_PRN_ERR("Create for interrupt pipe failed.\n",
                           1, 2, 3, 4, 5, 6);

            usbTgtPrnDestroyAllPipes(pUsbTgtPrnDev);

            return ERROR;
            }

    /* Set the configuration parameters */

    ConfigParam.uMaxErpCount = 1;
    ConfigParam.uMaxErpBufCount = 1;

    /* The max buffer size is 2K */

    ConfigParam.uMaxErpBufSize = USBTGT_PRN_MAX_ERP_BUF_SIZE;

    /* Configure the pipe */

    if (ERROR ==  usbTgtConfigPipe(pUsbTgtPrnDev->bulkOutPipeHandle,
                                   &ConfigParam))
        {
        USBTGT_PRN_ERR("Configure for interrupt pipe failed.\n",
                       1, 2, 3, 4, 5, 6);

        usbTgtPrnDestroyAllPipes(pUsbTgtPrnDev);

        return ERROR;
        }

    return OK;
    }

/*******************************************************************************
*
* usbTgtPrnBulkInErpCallback - process data sent on bulk-in pipe
*
* This routine is called on data sent on bulk-in pipe.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void usbTgtPrnBulkInErpCallback
    (
    pVOID               erp               /* USB_ERP endpoint request packet */
    )
    {
    USB_ERP *           pErp = (USB_ERP *)erp;      /* USB_ERP */
    pUSBTGT_PRN_DEV     pUsbTgtPrnDev;

    /* check parameter */

    if ((NULL == pErp) ||
        (NULL == pErp->userPtr))
        {
        USBTGT_PRN_ERR("Invalid parameter %s is NULL.\n",
                       (ULONG)((NULL == pErp) ?
                       "pErp" : (NULL == pErp->userPtr) ?
                       "userPtr" : "NULL"), 2, 3, 4, 5, 6);

        return;
        }

    pUsbTgtPrnDev = pErp->userPtr;

    pUsbTgtPrnDev->bBulkInErpUsed = FALSE;

    /* verify that the data is good */

    if ((pErp->result != OK) &&
        (pErp->result != S_usbTgtTcdLib_ERP_SUCCESS) &&
        (pErp->result != S_usbTgtTcdLib_ERP_RESTART))
        {
        USBTGT_PRN_ERR("Bulk in failed. pErp->result %p, exiting...\n",
                       pErp->result, 2, 3, 4, 5, 6);

        pUsbTgtPrnDev->bBulkInBufValid = FALSE;
        }

    /*  Send notify to submit one Bulk IN erp asking for more data */

    usbTgtPrnManagementNotify(pUsbTgtPrnDev, USBTGT_PRN_NOTIFY_BULK_IN);

    }

/*******************************************************************************
*
* usbTgtPrnBulkOutErpCallback - process data received on bulk-out pipe
*
* This routine processes the data which is received on the bulk out pipe.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void usbTgtPrnBulkOutErpCallback
    (
    pVOID               erp        /* USB_ERP endpoint request packet */
    )
    {
    USB_ERP *           pErp = (USB_ERP *)erp;      /* USB_ERP */
    pUSBTGT_PRN_DEV     pUsbTgtPrnDev;

    /* check parameter */

    if ((NULL == pErp) ||
        (NULL == pErp->userPtr))
        {
        USBTGT_PRN_ERR("Invalid parameter %s is NULL\n",
                       (ULONG)((NULL == pErp) ? "pErp" : "userPtr" )
                       , 2, 3, 4, 5, 6);

        return;
        }

    USBTGT_PRN_DBG ("usbTgtPrnBulkOutErpCallback recv 0x%X bytes data.\n",
                    pErp->bfrList[0].actLen, 2, 3, 4, 5, 6);

    if (pErp->bfrList[0].actLen == 0)
        return;

    pUsbTgtPrnDev = (pUSBTGT_PRN_DEV)pErp->userPtr;

    /* Report the received data to upper level */

    /* pUsbTgtPrnDev->bulkOutErp.bfrList [0].actLen and pErp->bfrList[0].actLen
     * are the same address. pUsbTgtPrnDev->pBulkOutBuf and (pErp->bfrList[0].pBfr
     * are the same address.
     */

    /* verify the data is good */

    if (pErp->result != OK &&
        pErp->result != S_usbTgtTcdLib_ERP_SUCCESS &&
        pErp->result != S_usbTgtTcdLib_ERP_RESTART)
        {
        USBTGT_PRN_ERR("usbTgtPrn bulk out data is not good.\n",
                       1, 2, 3, 4, 5, 6);

        pUsbTgtPrnDev->bBulkOutBufValid = FALSE;
        }
    else
        pUsbTgtPrnDev->bBulkOutBufValid = TRUE;

    pUsbTgtPrnDev->bBulkOutErpUsed = FALSE;

    /* Send notify to submit one Bulk out erp asking for more data */

    usbTgtPrnManagementNotify(pUsbTgtPrnDev, USBTGT_PRN_NOTIFY_BULK_OUT);

    return;
    }

/*******************************************************************************
*
* configurationSet - set the specified configuration
*
* This routine is used to set the current configuration to the configuration
* value sent by host. <configuration> consists of the value to set.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtPrnConfigurationSet
    (
    pVOID                 param,          /*  TCD specific parameter */
    USB_TARG_CHANNEL      targChannel,    /* target channel */
    UINT8                 configuration   /* configuration value to set */
    )
    {
    pUSBTGT_PRN_DEV       pUsbTgtPrnDev = (pUSBTGT_PRN_DEV) param;

    /* check parameter */

    if (pUsbTgtPrnDev == NULL ||
        pUsbTgtPrnDev->targChannel != targChannel )
        {
        USBTGT_PRN_ERR("parameter error.\n", 1, 2, 3, 4, 5, 6);

        return ERROR ;
        }

    if (configuration == 0)
        {
        USBTGT_PRN_DBG("Reset to the config 0, delete all created pipes.\n",
                       1, 2, 3, 4, 5, 6);

        pUsbTgtPrnDev->uConfigurationValue = configuration;

        usbTgtPrnDestroyAllPipes(pUsbTgtPrnDev);
        }

    else
        {
        /* if Prn was configured as multi-configuration device. The
         * configuration may not be Printer configuration value.
         */

        USBTGT_PRN_DBG("configuration = 0x%x.", configuration, 2, 3, 4, 5, 6);

        usbTgtPrnDestroyAllPipes(pUsbTgtPrnDev);

        if (ERROR == usbTgtPrnCreateAllPipes(pUsbTgtPrnDev, configuration))
            {
            usbTgtPrnDestroyAllPipes(pUsbTgtPrnDev);

            return ERROR;
            }

        /* Erp init for bulk and bulk out */

        if (usbTgtPrnBulkOutErpInit(pUsbTgtPrnDev,
                                    pUsbTgtPrnDev->pBulkOutBuf,
                                    USBTGT_PRN_MAX_BUFF_LEN,
                                    usbTgtPrnBulkOutErpCallback,
                                    pUsbTgtPrnDev) != OK)
            {
            USBTGT_PRN_ERR("usbTgtPrnBulkOutErpInit failed\n",
                           1, 2, 3, 4, 5, 6);

            usbTgtPrnDestroyAllPipes(pUsbTgtPrnDev);

            return ERROR;
            }

        USBTGT_PRN_DBG("usbTgtPrnConfigurationSet(): succeed.\n",
                       1, 2, 3, 4, 5, 6);
        }

    pUsbTgtPrnDev->uConfigurationValue = configuration;

    return OK;
    }

/*******************************************************************************
*
* usbTgtPrnRxData - handle bulk out data
*
* This routine handles the received data from printer host.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*/

STATUS usbTgtPrnRxData
    (
    pUSBTGT_PRN_DEV    pUsbTgtPrnDev,
    UINT8 *            pdataBuf,
    UINT32             dataLen
    )
    {
    /* Add user's process to handle bulk out data. */

#ifdef USBTGT_PRN_DEBUG_DUMP

    if ((usbTgtPrnDebugRcvBuflen + dataLen ) > usbTgtPrnDebugRcvBufSiz)
        {
        USBTGT_PRN_ERR("Mem lack. Already received %d data, another %d data "
                       "coming. Total debug mem size = 0x%x.",
        usbTgtPrnDebugRcvBuflen, dataLen, usbTgtPrnDebugRcvBufSiz, 4, 5, 6);
        }
    else
        {
        memcpy(pUsbTgtPrnDebugRcvBuf + usbTgtPrnDebugRcvBuflen,
               pdataBuf, dataLen);

        usbTgtPrnDebugRcvBuflen += dataLen;

        USBTGT_PRN_DBG ("Rcv len: 0x%x. Tol: 0x%x. addr: 0x%x. 0x%x, 0x%x, "
                        "0x%x.\n", dataLen, usbTgtPrnDebugRcvBuflen,
                        pUsbTgtPrnDebugRcvBuf, * (pdataBuf),
                        *((UINT8 *)(pdataBuf + 1)),
                        *((UINT8 *)(pdataBuf + 2)));
        }

#endif

    return OK;
    }

/*******************************************************************************
*
* usbTgtPrnTxData - handle bulk in data
*
* This routine sends data to host.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*/

STATUS usbTgtPrnTxData
    (
    pUSBTGT_PRN_DEV     pUsbTgtPrnDev,
    char *              pBuffer,
    UINT32              count
    )
    {
    UINT32 erpSize = 0;
    int ticksToWait;

    if (pUsbTgtPrnDev == NULL ||
        pBuffer == NULL)
        {
        USBTGT_PRN_ERR("Invalid parameter %s is NULL\n",
                       (ULONG)((NULL == pUsbTgtPrnDev) ?
                       "pUsbTgtPrnDev" : "pBuffer"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    ticksToWait = (sysClkRateGet () * pUsbTgtPrnDev->deviceTimeout) / 1000;

    ticksToWait = (ticksToWait ? ticksToWait : 1);

    while (TRUE)
        {
        if (count > USBTGT_PRN_MAX_BUFF_LEN)
            erpSize = USBTGT_PRN_MAX_BUFF_LEN;
        else
            erpSize = count;

        if (OK != OSS_SEM_TAKE(pUsbTgtPrnDev->bulkInErpSem, ticksToWait))
            {
            USBTGT_PRN_ERR("usbTgtPrn bulkInErpSem take failed.\n",
                           1, 2, 3, 4, 5, 6);
            return ERROR;
            }

        pUsbTgtPrnDev->bBulkInErpUsed = TRUE;

        memcpy(pUsbTgtPrnDev->pBulkInBuf, pBuffer, erpSize);

        if (OK != usbTgtPrnBulkInErpInit(pUsbTgtPrnDev,
                                         pUsbTgtPrnDev->pBulkInBuf,
                                         (UINT32)erpSize,
                                         usbTgtPrnBulkInErpCallback,
                                         pUsbTgtPrnDev))
            {
            USBTGT_PRN_ERR("usbTgtPrnBulkInErpInit failed.\n",
                           1, 2, 3, 4, 5, 6);

            usbTgtSetPipeStatus(pUsbTgtPrnDev->bulkInPipeHandle,
                                USBTGT_PIPE_STATUS_STALL);
            pUsbTgtPrnDev->uBulkInPipeStatus = USBTGT_PRN_PIPE_STALL;

            pUsbTgtPrnDev->bBulkInErpUsed = FALSE;
            OSS_SEM_GIVE(pUsbTgtPrnDev->bulkInErpSem);

            return ERROR;
            }

            if (erpSize < USBTGT_PRN_MAX_BUFF_LEN)
                break;

            count -= erpSize;
            pBuffer += erpSize;
        }

    pUsbTgtPrnDev->bBulkInErpUsed = FALSE;

    OSS_SEM_GIVE(pUsbTgtPrnDev->bulkInErpSem);

    return OK;
    }

/***************************************************************************
*
* usbTgtPrnBulkTask - hold bulk in and bulk out data
*
* This routine transfers bulk in data, copies data from user buffer to ERP
* buffer and initializes ERP and transfer ERP.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void usbTgtPrnBulkTask
    (
    pUSBTGT_PRN_DEV     pUsbTgtPrnDev
    )
    {
    USBTGT_PRN_NOTIFY_INFO   prnNotifyInfo;
    UINT8                    nBytes = 0;
    UINT32                   dataLen;

    /* check parameter */

    if (NULL == pUsbTgtPrnDev)
        {
        USBTGT_PRN_ERR("Invalid parameter %s is NULL\n",
                       "pUsbTgtPrn", 2, 3, 4, 5, 6);

        return;
        }

    while (TRUE)
        {
        if (pUsbTgtPrnDev->prnTaskMsgQID == NULL)
            {
            USBTGT_PRN_ERR("Invalid parameter, pTcd->TransferThreadMsgID "
                           "is NULL\n", 1, 2, 3, 4, 5 ,6);
            break;
            }

        /* Reset the transfer task information */

        OS_MEMSET(&prnNotifyInfo, 0, sizeof(USBTGT_PRN_NOTIFY_INFO));

        /* Wait on the signalling of the event */

        nBytes = (UINT8) msgQReceive (pUsbTgtPrnDev->prnTaskMsgQID,
                              (char*) & prnNotifyInfo,
                              sizeof(USBTGT_PRN_NOTIFY_INFO),
                              USBTGT_WAIT_TIMEOUT);

        USBTGT_PRN_DBG("prnNotifyInfo.NotifyCode = %d. \n",
                       prnNotifyInfo.notifyCode, 2, 3, 4, 5, 6);

        if ((NULL == prnNotifyInfo.pUsbTgtPrn) ||
            (USBTGT_PRN_NOTIFY_UNKNOWN == prnNotifyInfo.notifyCode) ||
            (nBytes != sizeof(USBTGT_PRN_NOTIFY_INFO)))
            {
            USBTGT_PRN_ERR("Invalid parameter %s .\n",
                           (ULONG)((NULL == prnNotifyInfo.pUsbTgtPrn) ?
                           "prnNotifyInfo.pUsbTgtPrn" :
                           (USBTGT_PRN_NOTIFY_UNKNOWN == prnNotifyInfo.notifyCode) ?
                           "prnNotifyInfo.NotifyCode" :
                           "pUsbTgtPrnDev->bBulkOutErpUsed == TRUE"),
                           2, 3, 4, 5, 6);

            continue;
            }

        switch (prnNotifyInfo.notifyCode)
            {
            /* TODO : add some case process */

            case USBTGT_PRN_NOTIFY_RESET:
                {
                USBTGT_PRN_VDBG("USBTGT_PRN_NOTIFY_DATA_CHANNEL_RESET\n",
                                1, 2, 3, 4, 5, 6);

                if (pUsbTgtPrnDev->bulkOutPipeHandle != NULL)
                    {
                    if (TRUE == pUsbTgtPrnDev->bBulkOutErpUsed)
                        {
                        if (OK != usbTgtCancelErp(pUsbTgtPrnDev->bulkOutPipeHandle,
                                        &pUsbTgtPrnDev->bulkOutErp))
                            {
                            USBTGT_RNDIS_DBG("Cancel bulkOutErp fail\n",
                                             1, 2, 3, 4, 5, 6);
                            }
                        }

                    if (usbTgtPrnBulkOutErpInit(pUsbTgtPrnDev,
                                                pUsbTgtPrnDev->pBulkOutBuf,
                                                USBTGT_PRN_MAX_BUFF_LEN,
                                                usbTgtPrnBulkOutErpCallback,
                                                pUsbTgtPrnDev) != OK)
                        {
                        usbTgtSetPipeStatus(pUsbTgtPrnDev->bulkOutPipeHandle,
                                            USBTGT_PIPE_STATUS_STALL);
                        pUsbTgtPrnDev->uBulkOutPipeStatus =
                            USBTGT_PRN_PIPE_STALL;
                        }
                    }

                if (pUsbTgtPrnDev->bulkInPipeHandle != NULL)
                    {
                    if (TRUE == pUsbTgtPrnDev->bBulkInErpUsed)
                        {
                        if (OK != usbTgtCancelErp(pUsbTgtPrnDev->bulkInPipeHandle,
                                        &pUsbTgtPrnDev->bulkInErp))
                            {
                            USBTGT_RNDIS_DBG("Cancel bulkInErp fail\n",
                                             1, 2, 3, 4, 5, 6);
                            }
                        }

                    if (usbTgtPrnBulkInErpInit(pUsbTgtPrnDev,
                                               pUsbTgtPrnDev->pBulkInBuf,
                                               USBTGT_PRN_MAX_BUFF_LEN,
                                               usbTgtPrnBulkInErpCallback,
                                               pUsbTgtPrnDev) != OK)
                        {
                        usbTgtSetPipeStatus(pUsbTgtPrnDev->bulkInPipeHandle,
                                            USBTGT_PIPE_STATUS_STALL);
                        pUsbTgtPrnDev->uBulkInPipeStatus =
                            USBTGT_PRN_PIPE_STALL;
                        }
                    }
                }
                break;

            case USBTGT_PRN_NOTIFY_BULK_IN:
                {
                USBTGT_PRN_VDBG ("USBTGT_PRN_NOTIFY_BULK_IN .\n",
                                 1, 2, 3, 4, 5, 6);

                if (FALSE == pUsbTgtPrnDev->bBulkInErpUsed)
                    {
                    OSS_SEM_GIVE(pUsbTgtPrnDev->bulkInErpSem);
                    }
                else
                    {
                    USBTGT_PRN_ERR (" Bulk in Erp is in used. \n",
                                    1, 2, 3, 4, 5, 6);
                    }
                }
                break;

            case USBTGT_PRN_NOTIFY_BULK_OUT:
                {
                /* Check the Bulk In channel is used or not */

                USBTGT_PRN_VDBG ("USBTGT_PRN_NOTIFY_BULK_OUT\n",
                                 1, 2, 3, 4, 5, 6);

                if (FALSE == pUsbTgtPrnDev->bBulkOutErpUsed)
                    {
                    /* Continue to receive data */

                    if (pUsbTgtPrnDev->bBulkOutBufValid == FALSE)
                        {
                        USBTGT_PRN_ERR("usbTgtPrn bulk out data is not good. ",
                                       1, 2, 3, 4, 5, 6);
                        }

                    dataLen = pUsbTgtPrnDev->bulkOutErp.bfrList [0].actLen;

                    usbTgtPrnRxData(pUsbTgtPrnDev, pUsbTgtPrnDev->pBulkOutBuf,
                                    dataLen);

                    if (usbTgtPrnBulkOutErpInit(pUsbTgtPrnDev,
                                                pUsbTgtPrnDev->pBulkOutBuf,
                                                USBTGT_PRN_MAX_BUFF_LEN,
                                                usbTgtPrnBulkOutErpCallback,
                                                pUsbTgtPrnDev) != OK)
                        {
                        usbTgtSetPipeStatus(pUsbTgtPrnDev->bulkOutPipeHandle,
                                            USBTGT_PIPE_STATUS_STALL);
                        pUsbTgtPrnDev->uBulkOutPipeStatus =
                            USBTGT_PRN_PIPE_STALL;
                        }
                    }
                else
                    {
                    USBTGT_PRN_ERR (" Can not Take the Bulk In sema and submit "
                                    "the erp.\n", 1, 2, 3, 4, 5, 6);
                    }
                }
                break;

            default:
                USBTGT_PRN_ERR ("Unknown notify command.\n", 1, 2, 3, 4, 5, 6);
                break;
            }
        }

    return;
    }

/*******************************************************************************
*
* usbTgtPrnDestroy - destroy one pUsbTgtPrn resource
*
* This routine destroys one pUsbTgtPrn resource which alloced by the
* usbTgtPrnCreate routine.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbTgtPrnDestroy
    (
    pUSBTGT_PRN_DEV   pUsbTgtPrn
    )
    {
    /* No need check parameter */
    /* Destroy the management task */

    if ((pUsbTgtPrn->prnTaskID != OS_THREAD_FAILURE) &&
        (pUsbTgtPrn->prnTaskID != 0))
        {
        OS_DESTROY_THREAD(pUsbTgtPrn->prnTaskID);
        pUsbTgtPrn->prnTaskID = OS_THREAD_FAILURE;
        }

    /* Delete the MsgQ ID*/

    if (pUsbTgtPrn->prnTaskMsgQID != NULL)
        {
        msgQDelete(pUsbTgtPrn->prnTaskMsgQID);
        pUsbTgtPrn->prnTaskMsgQID = NULL;
        }

    /* Delete the Bulk In Mutex */

    if (OSS_SEM_TAKE (pUsbTgtPrn->bulkInErpSem, USBTGT_WAIT_TIMEOUT) == OK)
        {
        /* Mutex should be taken before delete it */

        OSS_SEM_DESTROY (pUsbTgtPrn->bulkInErpSem);
        pUsbTgtPrn->bulkInErpSem = NULL;
        }

    /* Delete the Bulk Out Mutex */

    if (OSS_SEM_TAKE (pUsbTgtPrn->bulkOutErpSem, USBTGT_WAIT_TIMEOUT) == OK)
        {
        /* Mutex should be taken before delete it */

        OSS_SEM_DESTROY (pUsbTgtPrn->bulkOutErpSem);
        pUsbTgtPrn->bulkOutErpSem = NULL;
        }

    USBTGT_PRN_VDBG ("usbTgtPrnDestroy the dev 0x%X successfully.\n",
                     pUsbTgtPrn, 2, 3, 4, 5, 6);

    OSS_FREE(pUsbTgtPrn->pBulkOutBuf);
    OSS_FREE(pUsbTgtPrn->pBulkInBuf);

    OSS_FREE(pUsbTgtPrn);

    return;
    }

/*******************************************************************************
*
* usbTgtPrnCreate - create one new Prn device
*
* This routine creates one new Prn device.
*
* RETURNS: NULL or the pointer of pUSBTGT_PRN_DEV
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL pUSBTGT_PRN_DEV usbTgtPrnCreate
    (
    void
    )
    {
    pUSBTGT_PRN_DEV      pUsbTgtPrn = NULL;
    UCHAR                prnMTaskName[20];
    int                  index = 0;

    pUsbTgtPrn = (pUSBTGT_PRN_DEV)OSS_CALLOC(sizeof (USBTGT_PRN_DEV));
    USB_PRINTER_PORT_STATUS prnPortStatus = {USB_PRN_STS_SELECTED |
                                             USB_PRN_STS_NOT_ERROR};

    if (NULL == pUsbTgtPrn)
        {
        USBTGT_PRN_ERR("No enough memory to melloc for the Prn resource.\n",
                       1, 2, 3, 4, 5 ,6);

        return NULL;
        }

    /* Create the Bulk In Erp Mutex */

    if (OSS_SEM_CREATE( 1, 1, &pUsbTgtPrn->bulkInErpSem) != OK)
        {
        USBTGT_PRN_ERR ("Create bulkInErpSem failed..\n",
                        1, 2, 3, 4, 5, 6);

        usbTgtPrnDestroy(pUsbTgtPrn);

        return NULL;
        }

    pUsbTgtPrn->pBulkInBuf = (UINT8 *)OSS_CALLOC (USBTGT_PRN_MAX_BUFF_LEN);

    if (NULL == pUsbTgtPrn->pBulkInBuf)
        {
        USBTGT_PRN_ERR("No enough memory to melloc for Prn bulk in.\n",
                       1, 2, 3, 4, 5 ,6);

        usbTgtPrnDestroy (pUsbTgtPrn);

        return NULL;
        }

    /* Create the Bulk Out Erp Mutex */

    if (OSS_SEM_CREATE( 1, 1, &pUsbTgtPrn->bulkOutErpSem) != OK)
        {
        USBTGT_PRN_ERR ("Create bulkOutErpSem failed.\n",
                        1, 2, 3, 4, 5, 6);

        usbTgtPrnDestroy(pUsbTgtPrn);

        return NULL;
        }

    pUsbTgtPrn->pBulkOutBuf = (UINT8 *)OSS_CALLOC (USBTGT_PRN_MAX_BUFF_LEN);

    if (NULL == pUsbTgtPrn->pBulkOutBuf)
        {
        USBTGT_PRN_ERR("No enough memory to melloc for Prn bulk out.\n",
                       1, 2, 3, 4, 5 ,6);

        usbTgtPrnDestroy (pUsbTgtPrn);

        return NULL;
        }

    /* Create the MsgQ ID */

    pUsbTgtPrn->prnTaskMsgQID = msgQCreate(USBTGT_MAX_MSG_COUNT * 10,
                                           sizeof(USBTGT_PRN_NOTIFY_INFO),
                                           MSG_Q_FIFO);

    if (pUsbTgtPrn->prnTaskMsgQID == NULL)
        {
        USBTGT_PRN_ERR("Create Tcd PrnTaskMsgQID failed.\n", 1, 2, 3, 4, 5 ,6);

        usbTgtPrnDestroy (pUsbTgtPrn);

        return NULL;
        }

    pUsbTgtPrn->deviceTimeout = USBTGT_PRN_DEFAULT_TIMEOUT;

    pUsbTgtPrn->prnState = prnPortStatus;

    /* Take mutex to update the global list */

    (void)OSS_SEM_TAKE (gUsbTgtPrnListSem, USBTGT_WAIT_TIMEOUT);

    /*
     * Create the prn management task
     * Using the node index to mark the task number
     * we need lstFind the pUsbTgtPrn->prnNode,
     * since this node just added to the list, use lstCount to mark this
     */

    index = lstCount (&gUsbTgtPrnList);

    snprintf((char*)prnMTaskName, 20, "PRN_M%d", index);

    pUsbTgtPrn->prnTaskID = OS_CREATE_THREAD((char *)prnMTaskName,
                                             USBTGT_PRN_BULK_TASK_PRIORITY,
                                             usbTgtPrnBulkTask,
                                             pUsbTgtPrn);

    if (OS_THREAD_FAILURE == pUsbTgtPrn->prnTaskID)
        {
        USBTGT_PRN_ERR("Create the Management task for 0x%X failed.\n",
                       pUsbTgtPrn, 2, 3, 4, 5, 6);

        usbTgtPrnDestroy (pUsbTgtPrn);
        OSS_SEM_GIVE (gUsbTgtPrnListSem);

        return NULL;
        }

    /* Add to the global list */

    lstAdd (&gUsbTgtPrnList, &pUsbTgtPrn->prnNode);

    OSS_SEM_GIVE (gUsbTgtPrnListSem);

    USBTGT_PRN_VDBG ("usbTgtPrnCreate the Prn dev 0x%X.\n",
                     pUsbTgtPrn, 2, 3, 4, 5, 6);

    return pUsbTgtPrn;
    }

/*******************************************************************************
*
* usbTgtPrnUnInit - uninitialize the Prn driver and destroy resource
*
* This routine uninitializes the Prn driver and destroy resource which
* created before.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbTgtPrnUnInit
    (
    void
    )
    {
    pUSBTGT_PRN_DEV    pUsbTgtPrn = NULL;
    STATUS             status     = ERROR;
    int                index;
    int                count;

    /* Get the count of the global Prn device list */

    (void) OSS_SEM_TAKE (gUsbTgtPrnListSem, USBTGT_WAIT_TIMEOUT);

    count = lstCount(&gUsbTgtPrnList);

    for (index = 1; index <= count; index ++)
        {
        pUsbTgtPrn = (pUSBTGT_PRN_DEV)lstNth(&gUsbTgtPrnList, index);

        if (NULL != pUsbTgtPrn)
            {
            /* Un-register from the TML */

            (void)usbTgtFuncUnRegister(pUsbTgtPrn->targChannel);

            /* Destroy the pUsbTgtPrn data structure */

            usbTgtPrnDestroy(pUsbTgtPrn);

            /* Delete the resource from the global list */

            if (ERROR != lstFind(&gUsbTgtPrnList, &pUsbTgtPrn->prnNode))
                {
                lstDelete(&gUsbTgtPrnList, &pUsbTgtPrn->prnNode);
                status = OK;
                }
            else
                {
                USBTGT_PRN_ERR("lstFind failed.\n", 1, 2, 3, 4, 5 ,6);
                status = ERROR;
                break;
                }
            }

        else
            USBTGT_PRN_ERR("pUsbTgtPrn is NUll.\n", 1, 2, 3, 4, 5 ,6);
        }

    OSS_SEM_GIVE (gUsbTgtPrnListSem);

    gUsbTgtPrnInited = FALSE;

    return status;
    }

/*******************************************************************************
*
* usbTgtPrnInit - initialize the Prn struct for devices
*
* This routine initializes the struct for devices.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbTgtPrnInit
    (
    int devCount
    )
    {
    pUSBTGT_PRN_DEV       pUsbTgtPrn  = NULL;
    USBTGT_FUNC_INFO      usbTgtFuncInfo;
    int                   index;

    /* This module has been initialized */

    if (gUsbTgtPrnInited == TRUE)
        return OK;

    /* Init the global Prn devcie list */

    if (OSS_SEM_CREATE( 1, 1, &gUsbTgtPrnListSem) != OK)
        {
        USBTGT_PRN_ERR ("Create gUsbTgtPrnListSem failed.\n",
                        1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    lstInit(&gUsbTgtPrnList);

    /* Allocal memory resource */

    for (index = 0; index < devCount; index ++)
        {
        pUsbTgtPrn = usbTgtPrnCreate();

        if (pUsbTgtPrn == NULL)
            {
            USBTGT_PRN_ERR("Create new Prn device failed.\n", 1, 2, 3, 4, 5 ,6);

            usbTgtPrnUnInit();

            return ERROR;
            }

        usbTgtFuncInfo.ppFsFuncDescTable =
            (USB_DESCR_HDR **)gUsbTgtPrnOtherSpeedDescHdr;
        usbTgtFuncInfo.ppHsFuncDescTable =
            (USB_DESCR_HDR **)gUsbTgtPrnHighSpeedDescHdr;
        usbTgtFuncInfo.ppSsFuncDescTable =
            (USB_DESCR_HDR **)gUsbTgtPrnSuperSpeedDescHdr;
        usbTgtFuncInfo.pFuncCallbackTable =
            (USB_TARG_CALLBACK_TABLE *)&gUsbTgtPrnCallbackTable;

        usbTgtFuncInfo.pCallbackParam = (pVOID)pUsbTgtPrn;
        usbTgtFuncInfo.pFuncSpecific = (pVOID)pUsbTgtPrn;

        usbTgtFuncInfo.pFuncName = usrUsbTgtPrnConfigTable[index].prnName;
        usbTgtFuncInfo.pTcdName = usrUsbTgtPrnConfigTable[index].tcdName;
        usbTgtFuncInfo.uFuncUnit = usrUsbTgtPrnConfigTable[index].uPrnUnit;
        usbTgtFuncInfo.uTcdUnit = usrUsbTgtPrnConfigTable[index].uTcdUnit;
        usbTgtFuncInfo.uConfigToBind =
            usrUsbTgtPrnConfigTable[index].uConfigNum;
        usbTgtFuncInfo.pWcidString = NULL;		
        usbTgtFuncInfo.pSubWcidString = NULL;		
        usbTgtFuncInfo.uWcidVc= NULL;		

        /* Register to the TML */

        pUsbTgtPrn->targChannel = usbTgtFuncRegister(&usbTgtFuncInfo);

        if (USBTGT_TARG_CHANNEL_DEAD == pUsbTgtPrn->targChannel)
            {
            USBTGT_MSC_ERR("Register to the TML fail\n",
                             1, 2, 3, 4, 5, 6);

            usbTgtPrnUnInit();

            return ERROR;
            }

        }

    gUsbTgtPrnInited = TRUE;

#ifdef USBTGT_PRN_DEBUG_DUMP

    pUsbTgtPrnDebugRcvBuf = (UINT8 *)OSS_CALLOC(usbTgtPrnDebugRcvBufSiz);

    if (NULL == pUsbTgtPrnDebugRcvBuf)
        {
        USBTGT_PRN_ERR("Malloc faild for pUsbTgtPrnDebugRcvBuf.\n",
                       1, 2, 3, 4, 5 ,6);
        }
#endif /* USBTGT_PRN_DEBUG_DUMP */

    USBTGT_PRN_DBG("usbTgtPrnInit successfully.\n", 1, 2, 3, 4, 5 ,6);

    return OK;
    }


