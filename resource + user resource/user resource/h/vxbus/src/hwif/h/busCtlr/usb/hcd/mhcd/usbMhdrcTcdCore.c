/* usbMhdrcTcdCore.c - USB Mentor Graphics TCD core module */

/*
 * Copyright (c) 2010-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01q,03may13,wyy  Remove compiler warning (WIND00356717)
01p,27feb13,s_z  Correct typo (WIND00405361)
01o,25oct12,s_z  MHDRC TCD pass CV testing (WIND00385197)
01n,18may12,s_z  Adjust based USB 3.0 target stack update (WIND00326012)
01m,15sep11,m_y  Update routine usbMhdrcTcdSubmitErp
01l,16may11,w_x  Add bDmaActive in USB_MHDRC_TCD_REQUEST (WIND00276544)
01k,21apr11,m_y  correct the cancel status of erp
01j,08apr11,w_x  Clear Coverity report for FORWARD_NULL (WIND00264893)
01i,29mar11,m_y  modify to support more than one buffer list
01h,28mar11,w_x  Correct uNumEps usage (WIND00262862)
01g,24mar11,s_z  Changes for unused routines removed
01f,23mar11,m_y  code clean up based on the review result
01e,09mar11,s_z  Code clean up
01d,04mar11,m_y  use pRequestMutex to protect the process of request
01c,18feb11,m_y  modify usbMhdrcTcdPieStatusGet and usbMhdrcTcdPipeStatusSet
                 routines
01b,20oct10,m_y  write
01a,18may10,s_z  created
*/

/*
DESCRIPTION

This file provides core functions for the USB Mentor Graphics Target controller
Driver.

INCLUDE FILES: usb/usb.h, usb/ossLib.h, usb/usbOsal.h, usb/usbOsalDebug.h,
               usb/usbPlatform.h, usb/usbOtg.h, usbMhdrcTcd.h, usbMhdrcTcdIsr.h,
               usbMhdrcTcdCore.h, usbMhdrcTcdUtil.h
*/

/* includes */

#include <usb/usb.h>
#include <usb/ossLib.h>
#include <usb/usbOsal.h>
#include <usb/usbOsalDebug.h>
#include <usb/usbPlatform.h>
#include <usb/usbOtg.h>
#include "usbMhdrcTcd.h"
#include "usbMhdrcTcdIsr.h"
#include "usbMhdrcTcdCore.h"
#include "usbMhdrcTcdUtil.h"

/* Function declartion */

LOCAL STATUS usbMhdrcTcdCreatePipe
    (
    pUSBTGT_TCD         pTCD,
    pUSB_ENDPOINT_DESCR pEndpointDesc,      /* USB_ENDPOINT_DESCR */
    UINT16              uConfigurationValue,/* configuration value */
    UINT16              uInterface,         /* Number of the interface index */
    UINT16              uAltSetting,        /* Alternate Setting */
    pUSB_TARG_PIPE      pPipeHandle         /* Pointer to pipe handle */
    );
LOCAL STATUS usbMhdrcTcdDeletePipe
    (
    pUSBTGT_TCD         pTCD,
    USB_TARG_PIPE       pipeHandle         /* Pointer to pipe handle */
    );
LOCAL STATUS usbMhdrcTcdSubmitErp
    (
    pUSBTGT_TCD         pTCD,
    USB_TARG_PIPE       pipeHandle,         /* Pipe handle */
    pUSB_ERP            pErp
    );
LOCAL STATUS usbMhdrcTcdCancelErp
    (
    pUSBTGT_TCD         pTCD,
    USB_TARG_PIPE       pipeHandle,         /* Pipe handle */
    pUSB_ERP            pErp
    );
LOCAL STATUS usbMhdrcTcdPipeStatusSet
    (
    pUSBTGT_TCD         pTCD,
    USB_TARG_PIPE       pipeHandle,         /* Pointer to pipe handle */
    UINT16              uStatus             /* Pointer to pipe handle */
    );
LOCAL STATUS usbMhdrcTcdPipeStatusGet
    (
    pUSBTGT_TCD         pTCD,
    USB_TARG_PIPE       pipeHandle,         /* Pointer to pipe handle */
    UINT16 *            pStatus             /* Buffer to hold the pipe status */
    );
LOCAL STATUS usbMhdrcTcdGetFrameNum
    (
    pUSBTGT_TCD pTCD,
    UINT16 *    pFrameNum
    );
LOCAL void usbMhdrcTcdEpSet
    (
    pUSB_MHDRC_TCD_DATA    pTCDData,
    pUSB_MHDRC_TCD_PIPE    pTCDPipe
    );

/* glolabs */

USBTGT_TCD_FUNCS g_UsbMhdrcTcdDriver =
    {
    usbMhdrcTcdCreatePipe,    /* pTcdCreatePipe */
    usbMhdrcTcdDeletePipe,    /* pTcdDeletePipe */
    usbMhdrcTcdSubmitErp,     /* pTcdSubmitErp */
    usbMhdrcTcdCancelErp,     /* pTcdCancelErp */
    usbMhdrcTcdPipeStatusSet, /* pTcdPipeStatusSet */
    usbMhdrcTcdPipeStatusGet, /* pTcdPipeStatusGet */
    usbMhdrcTcdWakeUp,        /* pTcdWakeUp */
    usbMhdrcTcdGetFrameNum,   /* pTcdGetFrameNum */
    usbMhdrcTcdSoftConnect,   /* pTcdSoftConnect */
    NULL                      /* pTcdIoctl */
    };

/*******************************************************************************
*
* usbMhdrcTcdSuspend - suspend the target controller
*
* This routine is to suspend the target controller.
*
* RETURNS: always OK
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbMhdrcTcdSuspend
    (
    pUSBTGT_TCD pTCD
    )
    {
    pUSB_MHDRC_TCD_DATA pTCDData = NULL;

    /* Valid parameter */

    USB_MHDRC_COMMON_VAILD_TCD_PARAMETER(pTCD);

    /* Update the device suspend status as TRUE */

    pTCDData = (pUSB_MHDRC_TCD_DATA)pTCD->pTcdSpecific;
    pTCDData->isDeviceSuspend = TRUE;

    /* Notify the suspend event */

    usbTgtTcdEventNotify(pTCD, USBTGT_NOTIFY_SUSPEND_EVENT);

    return OK;
    }

/*******************************************************************************
*
* usbMhdrcTcdResume - resume the target controller
*
* This routine is to resume the target controller.
*
* RETURNS: always OK
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbMhdrcTcdResume
    (
    pUSBTGT_TCD pTCD
    )
    {
    pUSB_MHDRC_TCD_DATA pTCDData = NULL;

    /* Valid parameter */

    USB_MHDRC_COMMON_VAILD_TCD_PARAMETER(pTCD);

    /* Update the device suspend status as false */

    pTCDData = (pUSB_MHDRC_TCD_DATA)pTCD->pTcdSpecific;
    pTCDData->isDeviceSuspend = FALSE;

    /* Notify the resume event */

    usbTgtTcdEventNotify(pTCD, USBTGT_NOTIFY_RESUME_EVENT);

    return OK;
    }

/*******************************************************************************
*
* usbMhdrcTcdWakeUp - wake up the target controller
*
* This routine is to wake up the target controller.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbMhdrcTcdWakeUp
    (
    pUSBTGT_TCD pTCD
    )
    {
    UINT8               uPower;
    pUSB_MUSBMHDRC      pMHDRC = NULL;
    pUSB_MHDRC_TCD_DATA pTCDData = NULL;

    USB_MHDRC_COMMON_VAILD_TCD_PARAMETER(pTCD);

    pTCDData = (pUSB_MHDRC_TCD_DATA)pTCD->pTcdSpecific;
    pMHDRC = pTCDData->pMHDRC;

    if (pTCDData->isDeviceSuspend)
        {
        uPower = USB_MHDRC_REG_READ8(pMHDRC, USB_MHDRC_POWER);
        uPower |= USB_MHDRC_POWER_RESUME;
        USB_MHDRC_REG_WRITE8(pMHDRC, USB_MHDRC_POWER,uPower);

        /*
         * According to the datasheet, it need delay for 2~15ms
         * before clear this bit
         */

        taskDelay (5);

        uPower = USB_MHDRC_REG_READ8(pMHDRC, USB_MHDRC_POWER);
        uPower = (UINT8)(uPower & ~USB_MHDRC_POWER_RESUME);
        USB_MHDRC_REG_WRITE8(pMHDRC, USB_MHDRC_POWER,uPower);
        }
    else
        {
        USB_MHDRC_ERR("Try to wakeup the device no matter it suspend\n",
                      1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    return OK;
    }

/*******************************************************************************
*
* usbMhdrcTcdDisconnect - disconnect the target controller
*
* This routine is to disconnect the target controller.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbMhdrcTcdDisconnect
    (
    pUSBTGT_TCD pTCD
    )
    {
    UINT8               uDevctl;
    pUSB_MHDRC_TCD_DATA pTCDData = NULL;
    pUSB_MUSBMHDRC      pMHDRC = NULL;

    USB_MHDRC_COMMON_VAILD_TCD_PARAMETER(pTCD);

    /* Update the softconnect status of the device controller */

    pTCDData = (pUSB_MHDRC_TCD_DATA)pTCD->pTcdSpecific;
    pMHDRC = pTCDData->pMHDRC;

    pTCDData->isSoftConnected = FALSE;

    /* Clear the HOSTREQ */
    /* NOTICE Old: (uDevctl & USB_MHDRC_DEVCTL_SESSION)*/

    uDevctl = USB_MHDRC_REG_READ8(pMHDRC, USB_MHDRC_DEVCTL);
    USB_MHDRC_REG_WRITE8(pMHDRC,
                         USB_MHDRC_DEVCTL,
                         uDevctl & ~USB_MHDRC_DEVCTL_SESSION);

    /* Notify the disconnect event */

    usbTgtTcdEventNotify(pTCD, USBTGT_NOTIFY_DISCONNECT_EVENT);

    return OK;
    }

/*******************************************************************************
*
* usbMhdrcTcdReset - reset the target controller
*
* This routine is to reset the target controller.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbMhdrcTcdReset
    (
    pUSBTGT_TCD pTCD
    )
    {
    UINT8               uPower;
    pUSB_MUSBMHDRC      pMHDRC = NULL;
    pUSB_MHDRC_TCD_DATA pTCDData = NULL;

    /* Valid parameter */

    USB_MHDRC_COMMON_VAILD_TCD_PARAMETER(pTCD);

    pTCDData = (pUSB_MHDRC_TCD_DATA)pTCD->pTcdSpecific;
    pMHDRC = pTCDData->pMHDRC;

    /* Update devcie speed */

    uPower = USB_MHDRC_REG_READ8(pMHDRC, USB_MHDRC_POWER);

    /*
     * 1.1.3.1 USB Controller Peripheral Mode Operation
     * After a reset, the SOFTCONN bit of POWER register is cleared to
     * 0. The controller will therefore appear disconnected unctil the
     * software has set the SOFTCONN bit to 1.
     */

    usbMhdrcTcdSoftConnect(pTCD, TRUE);
    pTCDData->isSoftConnected =  TRUE;

    /* Update pTCD's device specific paramter */

    pTCD->uSpeed = (UINT8)((uPower & USB_MHDRC_POWER_HSMODE)?
                            USB_SPEED_HIGH : USB_SPEED_FULL);
    pTCD->uAddrToSet = 0;
    pTCD->uDeviceAddress = 0;

    /* Support self power don't support the remote wakeup */

    pTCD->DeviceInfo.uDeviceFeature = USB_DEV_STS_LOCAL_POWER;

    /* Set device status to default value */

    pTCDData->isDeviceSuspend = FALSE;
    pTCDData->ep0Stage = USB_MHDRC_TCD_STAGE_SETUP;


    /* Notify the reset event */

    usbTgtTcdEventNotify(pTCD, USBTGT_NOTIFY_RESET_EVENT);

    return OK;
    }

/*******************************************************************************
*
* usbMhdrcTcdGetFrameNum - get the frame number of the target controller
*
* This routine gets the frame number of the target controller.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbMhdrcTcdGetFrameNum
    (
    pUSBTGT_TCD pTCD,
    UINT16 *    pFrameNum
    )
    {
    pUSB_MHDRC_TCD_DATA pTCDData = NULL;
    pUSB_MUSBMHDRC      pMHDRC = NULL;

    USB_MHDRC_COMMON_VAILD_TCD_PARAMETER(pTCD);

    if (pFrameNum == NULL)
        {
        USB_MHDRC_ERR("Invalid parameter: pFrameNum\n",
                      1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    pTCDData = (pUSB_MHDRC_TCD_DATA)pTCD->pTcdSpecific;
    pMHDRC = pTCDData->pMHDRC;

    /* Get the frame number */

    *pFrameNum  = (UINT16)
               (USB_MHDRC_REG_READ16 (pMHDRC,USB_MHDRC_FRAME) &
                USB_MHDRC_FRAME_MASK);

    /* Record the current frame number */

    pMHDRC->uFrameNumberRecord  = *pFrameNum;
    return OK;
    }

/*******************************************************************************
*
* usbMhdrcTcdSoftConnect - do soft connect/disconnect of the controller
*
* This routine is to do soft connect/disconnect of the target controller.
*
* RETURNS: always OK
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbMhdrcTcdSoftConnect
    (
    pUSBTGT_TCD pTCD,
    BOOL        isConnectUp
    )
    {
    UINT8               uPower = 0;
    pUSB_MUSBMHDRC      pMHDRC = NULL;
    pUSB_MHDRC_TCD_DATA pTCDData = NULL;

    USB_MHDRC_COMMON_VAILD_TCD_PARAMETER(pTCD);

    pTCDData = (pUSB_MHDRC_TCD_DATA)pTCD->pTcdSpecific;
    pMHDRC = pTCDData->pMHDRC;

    USB_MHDRC_INFO("usbMhdrcTcdSoftConnect - %s\n",
                  isConnectUp ? "Connected" : "Disconnected", 2, 3, 4, 5, 6);

    /* If the current status is same as the expected, just return */

    if (isConnectUp == pTCDData->isSoftConnected)
        {
        USB_MHDRC_WARN("usbMhdrcTcdSoftConnect - Already %s\n",
                      isConnectUp ? "Connected" : "Disconnected", 2, 3, 4, 5, 6);
        return OK;
        }

    if (isConnectUp)
        {
        /* Soft connect the pin */

        uPower = USB_MHDRC_REG_READ8(pMHDRC, USB_MHDRC_POWER);
        uPower |= (UINT8)(USB_MHDRC_POWER_SOFTCONN | USB_MHDRC_POWER_HSEN);

        pTCDData->isSoftConnected = TRUE;

        USB_MHDRC_WARN("usbMhdrcTcdSoftConnect - CONNECT\n",
                      1, 2, 3, 4, 5, 6);
        }
    else
        {
        /* Soft disconnect the pin */

        uPower = USB_MHDRC_REG_READ8(pMHDRC, USB_MHDRC_POWER);
        uPower = (UINT8)(uPower & ~USB_MHDRC_POWER_SOFTCONN);
        pTCDData->isSoftConnected = FALSE;

        USB_MHDRC_WARN("usbMhdrcTcdSoftConnect - DISCONNECT\n",
                      1, 2, 3, 4, 5, 6);
        }

    USB_MHDRC_REG_WRITE8 (pMHDRC, USB_MHDRC_POWER, uPower);
    return OK;
    }

/*******************************************************************************
*
* usbMhdrcTcdCreatePipe - create a pipe specific to an endpoint
*
* This routine creates a pipe specific to an endpoint.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbMhdrcTcdCreatePipe
    (
    pUSBTGT_TCD         pTCD,
    pUSB_ENDPOINT_DESCR pEndpointDesc,      /* USB_ENDPOINT_DESCR */
    UINT16              uConfigurationValue,/* configuration value */
    UINT16              uInterface,         /* Number of the interface index */
    UINT16              uAltSetting,        /* Alternate Setting */
    pUSB_TARG_PIPE      pPipeHandle         /* Pointer to pipe handle */
    )
    {
    UINT8                  uEpAddr = 0;
    UINT8                  uEpDir = 0;
    pUSB_MUSBMHDRC         pMHDRC = NULL;
    pUSB_MHDRC_TCD_DATA    pTCDData = NULL;
    pUSB_MHDRC_TCD_PIPE    pTCDPipe = NULL;

    USB_MHDRC_COMMON_VAILD_TCD_PARAMETER(pTCD);

    if (pEndpointDesc == NULL)
        {
        USB_MHDRC_ERR("Invalid parameter: pEndpointDesc\n", 1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    if (pPipeHandle == NULL)
        {
        USB_MHDRC_ERR("Invalid parameter: pPipeHandle\n", 1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    pTCDData = (pUSB_MHDRC_TCD_DATA)pTCD->pTcdSpecific;
    pMHDRC = pTCDData->pMHDRC;

    /* Check if the endpoint address is in the right range */

    uEpAddr = (UINT8)(pEndpointDesc->endpointAddress & ~USB_ENDPOINT_DIR_MASK);
    uEpDir = (UINT8)((pEndpointDesc->endpointAddress & USB_ENDPOINT_DIR_MASK) ?
             USB_DIR_IN : USB_DIR_OUT);

    if (uEpAddr >= pMHDRC->uNumEps)
        {
        USB_MHDRC_ERR("Invalid parameter: Ep [%d] is bigger than uNumEps [%d]\n",
                      uEpAddr, pMHDRC->uNumEps, 3, 4, 5, 6);
        return ERROR;
        }

    if (NULL != usbMhdrcTcdGetPipeByEpAddr(pTCDData, uEpAddr, uEpDir))
        {
        USB_MHDRC_ERR("Already exist a pipe for Ep [%d] Dir [%d]\n",
                      uEpAddr, uEpDir, 3, 4, 5, 6);
        return ERROR;
        }

    pTCDPipe = OS_MALLOC(sizeof(USB_MHDRC_TCD_PIPE));
    if (pTCDPipe == NULL)
        {
        USB_MHDRC_ERR("Malloc pTCDPipe fail\n", 1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    OS_MEMSET(pTCDPipe, 0, sizeof(USB_MHDRC_TCD_PIPE));
    pTCDPipe->uPipeFlag = USB_MHDRC_TCD_PIPE_INIT;
    lstInit(&(pTCDPipe->requestList));
    lstInit(&(pTCDPipe->freeRequestList));

    pTCDPipe->pPipeSyncMutex = semMCreate(SEM_Q_PRIORITY |
                                          SEM_INVERSION_SAFE |
                                          SEM_DELETE_SAFE);
    if (pTCDPipe->pPipeSyncMutex == NULL)
        {
        USB_MHDRC_ERR("Create pTCDPipe->pPipeSyncMutex fail\n", 1, 2, 3, 4, 5, 6);
        OS_FREE(pTCDPipe);
        return ERROR;
        }

    pTCDPipe->pRequestMutex = semMCreate(SEM_Q_PRIORITY |
                                         SEM_INVERSION_SAFE |
                                         SEM_DELETE_SAFE);
    if (pTCDPipe->pRequestMutex == NULL)
        {
        USB_MHDRC_ERR("Create pTCDPipe->pRequestMutex fail\n", 1, 2, 3, 4, 5, 6);
        semDelete(pTCDPipe->pPipeSyncMutex);
        OS_FREE(pTCDPipe);
        return ERROR;
        }

    /* Init the element of the pipe */

    pTCDPipe->uMaxPacketSize = OS_UINT16_LE_TO_CPU(pEndpointDesc->maxPacketSize);
    pTCDPipe->uEpAddress = uEpAddr;
    pTCDPipe->uEpDir = uEpDir;
    pTCDPipe->uEpType = (UINT8)(pEndpointDesc->attributes & USB_ATTR_EPTYPE_MASK);
    pTCDPipe->uInterface = uInterface;

    /* Set the DMA channel number to illegal value */

    pTCDPipe->uDmaChannel = pMHDRC->uNumDmaChannels;

    /* Program the related Endpoint let them prepare for transfer */

    usbMhdrcTcdEpSet(pTCDData, pTCDPipe);

    /* Add pipe into list */

    OS_WAIT_FOR_EVENT(pTCDData->tcdSyncMutex, WAIT_FOREVER);
    if (ERROR == lstFind(&(pTCDData->pipeList), &(pTCDPipe->pipeNode)))
        lstAdd(&(pTCDData->pipeList), &(pTCDPipe->pipeNode));
    OS_RELEASE_EVENT(pTCDData->tcdSyncMutex);

    *pPipeHandle = pTCDPipe;

    USB_MHDRC_VDBG("usbMhdrcTcdCreatePipe(): Created Dev %d, Ep %x \n",
                   pTCDPipe->uDevAddress,
                   pTCDPipe->uEpAddress,
                   3, 4, 5 ,6);

    return OK;
    }

/*******************************************************************************
*
* usbMhdrcTcdDeletePipe - delete a pipe created already
*
* This routine deletes a pipe specific to an endpoint.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbMhdrcTcdDeletePipe
    (
    pUSBTGT_TCD         pTCD,
    USB_TARG_PIPE       pipeHandle         /* Pointer to pipe handle */
    )
    {
    pUSB_MHDRC_TCD_PIPE    pTCDPipe = NULL;
    pUSB_MHDRC_TCD_DATA    pTCDData = NULL;

    USB_MHDRC_COMMON_VAILD_TCD_PARAMETER(pTCD);

    pTCDData = (pUSB_MHDRC_TCD_DATA)pTCD->pTcdSpecific;
    pTCDPipe = (pUSB_MHDRC_TCD_PIPE)pipeHandle;

    if (pTCDPipe == NULL)
        {
        USB_MHDRC_ERR("Invalid Paramter: pipeHandle\n", 1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    if (0 != (pTCDPipe->uPipeFlag & USB_MHDRC_TCD_PIPE_DELETE))
        {
        USB_MHDRC_ERR("This pipe already in process of delete\n",
                      1, 2, 3, 4, 5, 6);
        return OK;
        }

    /*
     * Take the pipeSyncMutex to make sure the request list cann't be
     * accessed by other task
     * NOTE: In usbMhdrcTcdPipeFlush this mutex will be taken to remove
     * request list
     */
    OS_WAIT_FOR_EVENT(pTCDPipe->pRequestMutex, WAIT_FOREVER);
    OS_WAIT_FOR_EVENT(pTCDPipe->pPipeSyncMutex, WAIT_FOREVER);

    /* Update pipeflag to indicate the pipe is going to be deleted */

    pTCDPipe->uPipeFlag |= USB_MHDRC_TCD_PIPE_DELETE;

    /* Flush the pipe let all the requests cancelled */

    usbMhdrcTcdPipeFlush(pTCDPipe, S_usbTgtTcdLib_ERP_CANCELED);
    pTCDPipe->uPipeFlag = (UINT8)(pTCDPipe->uPipeFlag & ~USB_MHDRC_TCD_PIPE_DELETE);

    /* Remove the pipe form the pipelist */

    OS_WAIT_FOR_EVENT(pTCDData->tcdSyncMutex, WAIT_FOREVER);
    if (ERROR != lstFind(&(pTCDData->pipeList), &(pTCDPipe->pipeNode)))
        lstDelete(&(pTCDData->pipeList), &(pTCDPipe->pipeNode));
    OS_RELEASE_EVENT(pTCDData->tcdSyncMutex);

    /* Release the pipe resource */

    semDelete(pTCDPipe->pPipeSyncMutex);
    semDelete(pTCDPipe->pRequestMutex);
    pTCDPipe->pPipeSyncMutex = NULL;
    pTCDPipe->pRequestMutex = NULL;
    OS_FREE(pTCDPipe);

    return OK;
    }

/*******************************************************************************
*
* usbMhdrcTcdSubmitErp - submit a request to a pipe
*
* This routine is used to submit a request to the pipe.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbMhdrcTcdSubmitErp
    (
    pUSBTGT_TCD         pTCD,
    USB_TARG_PIPE       pipeHandle,         /* Pipe handle */
    pUSB_ERP            pErp
    )
    {
    int i;
    int listCount;
    pUSB_MHDRC_TCD_DATA          pTCDData = NULL;
    pUSB_MHDRC_TCD_PIPE          pTCDPipe = NULL;
    pUSB_MHDRC_TCD_REQUEST       pRequest = NULL;
    pUSB_MHDRC_TCD_REQUEST       pHeadRequest = NULL;
    pUSB_MHDRC_TCD_REQUEST       pTailRequest = NULL;
    pUSB_MUSBMHDRC               pMHDRC = NULL;

    USB_MHDRC_COMMON_VAILD_TCD_PARAMETER(pTCD);

    pTCDPipe = (pUSB_MHDRC_TCD_PIPE)pipeHandle;

    if ((pTCDPipe == NULL) || (pErp == NULL))
        {
        USB_MHDRC_ERR("Invalid Paramter: pTCDPipe %p, pErp %p\n",
                      pTCDPipe, pErp, 3, 4, 5, 6);
        return ERROR;
        }

    /* Check if all the buffer for this erp are right */

    for (i = 0; i < pErp->bfrCount; i++)
        {
        if ((pErp->bfrList[i].bfrLen != 0) && (pErp->bfrList[i].pBfr == NULL))
            {
            USB_MHDRC_ERR("Invalid Paramter: pBfr[%d] is NULL but bfrLen %d\n",
                          i, pErp->bfrList[i].bfrLen, 3, 4, 5, 6);
            return ERROR;
            }
        }

    if (0 != (pTCDPipe->uPipeFlag & USB_MHDRC_TCD_PIPE_DELETE))
        {
        USB_MHDRC_ERR("Submit Erp to delete pipe %p, dir %d, ep %d\n",
            pTCDPipe, pTCDPipe->uEpDir, pTCDPipe->uEpAddress, 4, 5, 6);
        return ERROR;
        }

    pTCDData = (pUSB_MHDRC_TCD_DATA)pTCD->pTcdSpecific;
    pMHDRC = pTCDData->pMHDRC;

    for (i = 0; i < pErp->bfrCount; i++)
        {
        pRequest = usbMhdrcRequestReserve(pTCDPipe);
        if (pRequest == NULL)
            {
            USB_MHDRC_ERR("usbMhdrcRequestReserve pRequest fail\n", 1, 2, 3, 4, 5, 6);
            while ((pRequest = pHeadRequest) != NULL)
                {
                pHeadRequest = pRequest->pNext;
                usbMhdrcRequestRelease(pTCDPipe, pRequest);
                }
            return ERROR;
            }

        pRequest->pErp = pErp;
        pRequest->uXferSize = pErp->bfrList[i].bfrLen;
        pRequest->uActLength = 0;
        pRequest->pCurrentBuffer = pErp->bfrList[i].pBfr;
        pRequest->uErpBufIndex = (UINT16)i;
        pRequest->uErpBufCount = pErp->bfrCount;
        pRequest->bDmaActive = FALSE;

        if (pMHDRC->bDmaEnabled && (pTCDPipe->uEpType != USB_ATTR_CONTROL))
            {
            if (pTCDPipe->uEpDir == USB_DIR_OUT)
                {
                /* Invalidate the cache so that next access is from RAM */

                cacheInvalidate(DATA_CACHE,
                                (void *)(pRequest->pCurrentBuffer),
                                pRequest->uXferSize);
                }
            else
                {
                /* Flush cache */

                cacheFlush(DATA_CACHE,
                           (void *)(pRequest->pCurrentBuffer),
                           pRequest->uXferSize);
                }
            }

        if (pHeadRequest == NULL)
            pHeadRequest = pRequest;

        if (pTailRequest != NULL)
            pTailRequest->pNext = pRequest;

        pTailRequest = pRequest;
        }

    pErp->tcdPtr = pTailRequest;

    /*
     * Add request into the pipe list update the
     * pTCDPipe pointer of the structure pRequest
     */

    OS_WAIT_FOR_EVENT(pTCDPipe->pPipeSyncMutex, WAIT_FOREVER);

    /* Get the current count of the request list */

    listCount = lstCount(&pTCDPipe->requestList);

    /* Add all the request into list */

    pRequest = pHeadRequest;
    while (pRequest != NULL)
        {
        if (ERROR == lstFind(&(pTCDPipe->requestList), &(pRequest->requestNode)))
            {
            pRequest->pTCDPipe = pTCDPipe;
            lstAdd(&(pTCDPipe->requestList), &(pRequest->requestNode));
            }
        pRequest = pRequest->pNext;
        }
    OS_RELEASE_EVENT(pTCDPipe->pPipeSyncMutex);

    /*
     * If the pipe only have one request, start to handle it immediately.
     * Or else we will wait until all the request before this is handled over.
     */

    if (listCount == 0)
        usbMhdrcTcdReqHandle(pTCDData, pHeadRequest);

    return OK;
    }

/*******************************************************************************
*
* usbMhdrcTcdCancelErp - cancel the request which already submitted to pipe
*
* This routine is to cancel the request which already submitted to pipe.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbMhdrcTcdCancelErp
    (
    pUSBTGT_TCD         pTCD,
    USB_TARG_PIPE       pipeHandle,         /* Pipe handle */
    pUSB_ERP            pErp
    )
    {
    NODE *                 pNode = NULL;
    pUSB_MHDRC_TCD_PIPE    pTCDPipe = NULL;
    pUSB_MHDRC_TCD_REQUEST pRequest = NULL;

    /* Valid parameter*/

    USB_MHDRC_COMMON_VAILD_TCD_PARAMETER(pTCD);

    pTCDPipe = (pUSB_MHDRC_TCD_PIPE)pipeHandle;

    if ((pTCDPipe == NULL) || (pErp == NULL))
        {
        USB_MHDRC_ERR("Invalid Paramter: pTCDPipe %p, pErp %p\n",
                      pTCDPipe, pErp, 3, 4, 5, 6);
        return ERROR;
        }

    OS_WAIT_FOR_EVENT(pTCDPipe->pPipeSyncMutex, WAIT_FOREVER);

    if (pErp->tcdPtr == NULL)
        {
        USB_MHDRC_ERR("Erp doesn't have a corresponding request\n",
                      1, 2, 3, 4, 5, 6);
        OS_RELEASE_EVENT(pTCDPipe->pPipeSyncMutex);
        return ERROR;
        }

    /* Find the request */

    pNode = lstFirst(&pTCDPipe->requestList);

    while (pNode != NULL)
        {
        pRequest = REQUEST_NODE_TO_USB_MHDRC_TCD_REQUEST(pNode);
        if ((pRequest != NULL) && (pRequest->pErp == pErp))
            {
            /*
             * Complete the request with cancel status.
             * NOTE: the Erp's callback routine may re-submit this erp
             * and will re-take the "pPipeSyncMutex".
             */

            usbMhdrcTcdReqComplete(pRequest, S_usbTgtTcdLib_ERP_CANCELED);
            }
        pNode = lstNext(pNode);
        }

    OS_RELEASE_EVENT(pTCDPipe->pPipeSyncMutex);
    return OK;
    }

/*******************************************************************************
*
* usbMhdrcTcdPipeStatusSet - set pipe status
*
* This routine is to set the pipe's status.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbMhdrcTcdPipeStatusSet
    (
    pUSBTGT_TCD         pTCD,
    USB_TARG_PIPE       pipeHandle,
    UINT16              uStatus
    )
    {
    pUSB_MHDRC_TCD_DATA    pTCDData = NULL;
    pUSB_MHDRC_TCD_PIPE    pTCDPipe = NULL;
    pUSB_MUSBMHDRC         pMHDRC = NULL;
    UINT16                 uCsrReg = 0;

    /* Valid parameter */

    USB_MHDRC_COMMON_VAILD_TCD_PARAMETER(pTCD);

    if ((pTCDPipe = (pUSB_MHDRC_TCD_PIPE)pipeHandle) == NULL)
        {
        USB_MHDRC_ERR("Invalid Paramter: pipeHandle\n", 1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    /* Currently, only support stall status */

    if ((uStatus != USBTGT_PIPE_STATUS_STALL) &&
        (uStatus != USBTGT_PIPE_STATUS_UNSTALL))
        {
        USB_MHDRC_ERR("Invalid Paramter: uStatus = %d\n", uStatus, 2, 3, 4, 5, 6);
        return ERROR;
        }

    pTCDData = (pUSB_MHDRC_TCD_DATA)pTCD->pTcdSpecific;
    pMHDRC = pTCDData->pMHDRC;

    if (pTCDPipe->uEpAddress == 0)
        {
        uCsrReg = pTCDData->uCsr0;
        switch (pTCDData->ep0Stage)
            {
            case USB_MHDRC_TCD_STAGE_DATA_IN:
                /* Fall through */
            case USB_MHDRC_TCD_STAGE_DATA_OUT:
                /* Fall through */
            case USB_MHDRC_TCD_STAGE_ACK_WAIT:
                /* For these stage we need to use the current CSR0 */

                uCsrReg = USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_PERI_CSR0);
                /* Fall through */
            case USB_MHDRC_TCD_STAGE_STATUS_IN:
                /* Fall through */
            case USB_MHDRC_TCD_STAGE_STATUS_OUT:
                if (uStatus == USBTGT_PIPE_STATUS_STALL)
                    uCsrReg |= USB_MHDRC_PERI_CSR0_SENDSTALL;
                else
                    uCsrReg = (UINT16)(uCsrReg & ~USB_MHDRC_PERI_CSR0_SENDSTALL);
                USB_MHDRC_REG_WRITE16(pMHDRC, USB_MHDRC_PERI_CSR0, uCsrReg);
                pTCDData->uCsr0 = 0;
                break;
            default:
                USB_MHDRC_ERR("Current ep0Stage %d cannot be haltted\n",
                              pTCDData->ep0Stage, 2, 3, 4, 5, 6);
                return ERROR;
            }
        }
    else
        {
        if (pTCDPipe->uEpDir == USB_DIR_IN)
            {
            uCsrReg = USB_MHDRC_REG_READ16(pMHDRC,
                                          USB_MHDRC_PERI_TXCSR_EP(pTCDPipe->uEpAddress));
            uCsrReg |= (USB_MHDRC_PERI_TXCSR_WZC_BITS |
                        USB_MHDRC_PERI_TXCSR_CLRDATATOG);

            if (uStatus == USBTGT_PIPE_STATUS_STALL)
                uCsrReg |= USB_MHDRC_PERI_TXCSR_SENDSTALL;
            else
                uCsrReg = (UINT16)(uCsrReg & ~(USB_MHDRC_PERI_TXCSR_SENDSTALL|
                                               USB_MHDRC_PERI_TXCSR_SENTSTALL));

            uCsrReg = (UINT16)(uCsrReg & ~ USB_MHDRC_PERI_TXCSR_TXPKTRDY);
            USB_MHDRC_REG_WRITE16(pMHDRC,
                                  USB_MHDRC_PERI_TXCSR_EP(pTCDPipe->uEpAddress),
                                  uCsrReg);
            }
        else
            {
            uCsrReg = USB_MHDRC_REG_READ16(pMHDRC,
                                      USB_MHDRC_PERI_RXCSR_EP(pTCDPipe->uEpAddress));
            uCsrReg |= (USB_MHDRC_PERI_RXCSR_WZC_BITS   |
                        USB_MHDRC_PERI_RXCSR_FLUSHFIFO  |
                        USB_MHDRC_PERI_RXCSR_CLRDATATOG) ;

            if (uStatus == USBTGT_PIPE_STATUS_STALL)
                uCsrReg |= USB_MHDRC_PERI_RXCSR_SENDSTALL;
            else
                uCsrReg = (UINT16)(uCsrReg & ~(USB_MHDRC_PERI_RXCSR_SENDSTALL|
                                               USB_MHDRC_PERI_RXCSR_SENTSTALL));

            USB_MHDRC_REG_WRITE16(pMHDRC,
                                  USB_MHDRC_PERI_RXCSR_EP(pTCDPipe->uEpAddress),
                                  uCsrReg);

            }
        }

    return OK;
    }

/*******************************************************************************
*
* usbMhdrcTcdPipeStatusGet - get pipe status
*
* This routine is to get the pipe's status.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbMhdrcTcdPipeStatusGet
    (
    pUSBTGT_TCD         pTCD,
    USB_TARG_PIPE       pipeHandle,         /* Pointer to pipe handle */
    UINT16 *            pStatus             /* Buffer to hold the pipe status */
    )
    {
    pUSB_MHDRC_TCD_DATA    pTCDData = NULL;
    pUSB_MHDRC_TCD_PIPE    pTCDPipe = NULL;
    pUSB_MUSBMHDRC         pMHDRC = NULL;
    UINT16                 uCsrReg = 0;

    /* Valid parameter */

    USB_MHDRC_COMMON_VAILD_TCD_PARAMETER(pTCD);

    pTCDPipe = (pUSB_MHDRC_TCD_PIPE)pipeHandle;

    if ((pTCDPipe == NULL) || (pStatus == NULL))
        {
        USB_MHDRC_ERR("Invalid Paramter: pTCDPipe %p, pStatus %p\n",
                      pTCDPipe, pStatus, 3, 4, 5, 6);
        return ERROR;
        }

    pTCDData = (pUSB_MHDRC_TCD_DATA)pTCD->pTcdSpecific;
    pMHDRC = pTCDData->pMHDRC;

    /* Get the pipe status */

    if (pTCDPipe->uEpAddress == 0)
        {
        uCsrReg = USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_PERI_CSR0);
        if (uCsrReg & USB_MHDRC_PERI_CSR0_SENTSTALL)
            *pStatus = USB_ENDPOINT_STS_HALT;
        else
            *pStatus = USB_ENDPOINT_STS_UNHALT;
        }
    else
        {
        if (pTCDPipe->uEpDir == USB_DIR_IN)
            {
            uCsrReg = USB_MHDRC_REG_READ16(pMHDRC,
                                           USB_MHDRC_PERI_TXCSR_EP(pTCDPipe->uEpAddress));

            if ((uCsrReg & USB_MHDRC_PERI_TXCSR_SENTSTALL) ||
                (uCsrReg & USB_MHDRC_PERI_TXCSR_SENDSTALL))
                *pStatus = USB_ENDPOINT_STS_HALT;
            else
                *pStatus = USB_ENDPOINT_STS_UNHALT;
            }
        else
            {
            uCsrReg = USB_MHDRC_REG_READ16(pMHDRC,
                                           USB_MHDRC_PERI_RXCSR_EP(pTCDPipe->uEpAddress));

            if ((uCsrReg & USB_MHDRC_PERI_RXCSR_SENTSTALL) ||
                (uCsrReg & USB_MHDRC_PERI_RXCSR_SENDSTALL))
                *pStatus = USB_ENDPOINT_STS_HALT;
            else
                *pStatus = USB_ENDPOINT_STS_UNHALT;
            }
        }
    return OK;
    }


/*******************************************************************************
*
* usbMhdrcTcdEpSet - set the endpoint and prepare for transfer
*
* This routine is used to set the endpoint and prepare for transfer.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcTcdEpSet
    (
    pUSB_MHDRC_TCD_DATA    pTCDData,
    pUSB_MHDRC_TCD_PIPE    pTCDPipe
    )
    {
    UINT8                  uEpAddr = 0;
    UINT16                 uCsrReg = 0;
    pUSBTGT_TCD            pTCD = NULL;
    pUSB_MUSBMHDRC         pMHDRC = NULL;

    /*
     * Assume all the parameters is valid
     * The calller should ensure this
     */

    pMHDRC = pTCDData->pMHDRC;
    pTCD = pTCDData->pTCD;
    uEpAddr = pTCDPipe->uEpAddress;

    if (uEpAddr != 0)
        {
        if (pTCDPipe->uEpDir == USB_DIR_IN)
            {
            /* Set max packet size */

            USB_MHDRC_REG_WRITE16(pMHDRC,
                                  USB_MHDRC_PERI_TXMAXP_EP(uEpAddr),
                                  pTCDPipe->uMaxPacketSize);

            uCsrReg =
             USB_MHDRC_REG_READ16(pMHDRC,
                                  USB_MHDRC_PERI_TXCSR_EP(uEpAddr));

            /* Set Endpoint type */

            if (pTCDPipe->uEpType != USB_ATTR_ISOCH)
                uCsrReg = (UINT16)(uCsrReg & ~USB_MHDRC_PERI_TXCSR_ISO);
            else
                uCsrReg |= USB_MHDRC_PERI_TXCSR_ISO;

            /*
             * If FIFO is not empty, flush the FIFO
             * If DBF should flush twice
             */

            if (uCsrReg & USB_MHDRC_PERI_TXCSR_FIFONOEMPTY)
                uCsrReg |= USB_MHDRC_PERI_TXCSR_FLUSHFIFO;

            uCsrReg = (UINT16)(uCsrReg & ~USB_MHDRC_PERI_TXCSR_FRCDATATOG);
            uCsrReg |= USB_MHDRC_PERI_TXCSR_CLRDATATOG;

            /*
             * section 1.1.3.1.3:
             * In interrupt in transactions, the endpoints support contiunus
             * toggle of the data toggle bit. This feature is enabled by setting
             * the FRCDATATOG bit in the PERI_TXCSR
             */

            if (pTCDPipe->uEpType == USB_ATTR_INTERRUPT)
                uCsrReg |= USB_MHDRC_PERI_TXCSR_FRCDATATOG;

            USB_MHDRC_REG_WRITE16(pMHDRC,
                                  USB_MHDRC_PERI_TXCSR_EP(uEpAddr),
                                  uCsrReg);

            }
        else
            {
            /* Set max packet size */

            USB_MHDRC_REG_WRITE16(pMHDRC,
                                  USB_MHDRC_PERI_RXMAXP_EP(uEpAddr),
                                  pTCDPipe->uMaxPacketSize);

            uCsrReg =
             USB_MHDRC_REG_READ16(pMHDRC,
                                  USB_MHDRC_PERI_RXCSR_EP(uEpAddr));

            if (pTCDPipe->uEpType != USB_ATTR_ISOCH)
                uCsrReg = (UINT16)(uCsrReg & ~USB_MHDRC_PERI_RXCSR_ISO);
            else
                uCsrReg |= USB_MHDRC_PERI_RXCSR_ISO;

            if (pTCD->uSpeed == USB_SPEED_HIGH)
                uCsrReg = (UINT16)(uCsrReg & ~USB_MHDRC_PERI_RXCSR_DISNYET);

            /*
             * If FIFO is not empty, flush the fifo
             * If DBF should flush twice
             */

            if (uCsrReg & USB_MHDRC_PERI_RXCSR_RXPKTRDY)
                uCsrReg |= USB_MHDRC_PERI_RXCSR_FLUSHFIFO;

            uCsrReg |= USB_MHDRC_PERI_RXCSR_CLRDATATOG;

            /*
             * For interrupt out transactions, the endpoint don't support PING
             * flow control. This means that the controller should never respond
             * with a NYET handshake. The DISNYET bit int the PERI_RXCSR register
             * should be set to disable the transmission of NYET handshakes in
             * the high speed mode.
             */

            if ((pTCDPipe->uEpType == USB_ATTR_INTERRUPT)&&
                (pTCD->uSpeed == USB_SPEED_HIGH))
                uCsrReg |= USB_MHDRC_PERI_RXCSR_DISNYET;

            USB_MHDRC_REG_WRITE16(pMHDRC,
                                  USB_MHDRC_PERI_RXCSR_EP(uEpAddr),
                                  uCsrReg);
            }
        }
    return;
    }

