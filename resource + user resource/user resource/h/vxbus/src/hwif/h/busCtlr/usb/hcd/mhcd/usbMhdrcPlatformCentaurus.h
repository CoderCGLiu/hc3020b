/* usbMhdrcPlatformCentaurus.h - MUSBMHDRC CENTAURUS specific definitions */

/*
 * Copyright (c) 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,13jun11,jws  created for CENTAURUS
*/

/*
DESCRIPTION

This file defines the CENTAURUS special register offsets and macros for the MHCI
Host Controller.

INCLUDE FILES: usbMhdrc.h
*/

#ifndef __INCusbMhdrcPlatformCentaurush
#define __INCusbMhdrcPlatformCentaurush

/* includes */

#include "usbMhdrc.h"

#ifdef __cplusplus
extern "C" {
#endif

/* defines */

/* Offset of the MHCI Controller Revision Register */
#define USB_MHDRC_CENTAURUS_REVISION            0x00

/* Offset of the MHCI Controller Control Register */
#define USB_MHDRC_CENTAURUS_CONTROL             0x14

/* Offset of the MHCI Controller Status Register */
#define USB_MHDRC_CENTAURUS_STATUS              0x18

/* Offset of the MHCI Controller IRQ_MERGED_STATUS Register */
#define USB_MHDRC_CENTAURUS_IRQMSTAT            0x20

/* Offset of the MHCI Controller IRQ_EOI Register */
#define USB_MHDRC_CENTAURUS_IRQEOI              0x24

/* Offset of the MHCI Controller IRQ_STATUS_RAW_0 Register */
#define USB_MHDRC_CENTAURUS_IRQSTATRAW0         0x28

/* Offset of the MHCI Controller IRQ_STATUS_RAW_1 Register */
#define USB_MHDRC_CENTAURUS_IRQSTATRAW1         0x2C

/* Offset of the MHCI Controller IRQ_STATUS_0 Register */
#define USB_MHDRC_CENTAURUS_IRQSTAT0            0x30

/* Offset of the MHCI Controller IRQ_STATUS_1 Register */
#define USB_MHDRC_CENTAURUS_IRQSTAT1            0x34

/* Offset of the MHCI Controller IRQ_ENABLE_SET_0 Register */
#define USB_MHDRC_CENTAURUS_IRQENABLESET0       0x38

/* Offset of the MHCI Controller IRQ_ENABLE_SET_1 Register */
#define USB_MHDRC_CENTAURUS_IRQENABLESET1       0x3C

/* Offset of the MHCI Controller IRQ_ENABLE_CLR_0 Register */
#define USB_MHDRC_CENTAURUS_IRQENABLECLR0       0x40

/* Offset of the MHCI Controller IRQ_ENABLE_CLR_1 Register */
#define USB_MHDRC_CENTAURUS_IRQENABLECLR1       0x44

/* Offset of the MHCI Controller Tx Mode Register */
#define USB_MHDRC_CENTAURUS_TXMODE              0x70

/* Offset of the MHCI Controller Rx Mode Register */
#define USB_MHDRC_CENTAURUS_RXMODE              0x74

/* Offset of the MHCI Controller Generic RNDIS Size EP1 */
#define USB_MHDRC_CENTAURUS_GENRNDISEP1         0x80

/* Offset of the MHCI Controller Generic RNDIS Size EP2 */
#define USB_MHDRC_CENTAURUS_GENRNDISEP2         0x84

/* Offset of the MHCI Controller Generic RNDIS Size EP3 */
#define USB_MHDRC_CENTAURUS_GENRNDISEP3         0x88

/* Offset of the MHCI Controller Generic RNDIS Size EP4 */
#define USB_MHDRC_CENTAURUS_GENRNDISEP4         0x8C

/* Offset of the MHCI Controller Generic RNDIS Size EP5 */
#define USB_MHDRC_CENTAURUS_GENRNDISEP5         0x90

/* Offset of the MHCI Controller Generic RNDIS Size EP6 */
#define USB_MHDRC_CENTAURUS_GENRNDISEP6         0x94

/* Offset of the MHCI Controller Generic RNDIS Size EP7 */
#define USB_MHDRC_CENTAURUS_GENRNDISEP7         0x98

/* Offset of the MHCI Controller Generic RNDIS Size EP8 */
#define USB_MHDRC_CENTAURUS_GENRNDISEP8         0x9C

/* Offset of the MHCI Controller Generic RNDIS Size EP9 */
#define USB_MHDRC_CENTAURUS_GENRNDISEP9         0xA0

/* Offset of the MHCI Controller Generic RNDIS Size EP10 */
#define USB_MHDRC_CENTAURUS_GENRNDISEP10        0xA4

/* Offset of the MHCI Controller Generic RNDIS Size EP11 */
#define USB_MHDRC_CENTAURUS_GENRNDISEP11        0xA8

/* Offset of the MHCI Controller Generic RNDIS Size EP12 */
#define USB_MHDRC_CENTAURUS_GENRNDISEP12        0xAC

/* Offset of the MHCI Controller Generic RNDIS Size EP13 */
#define USB_MHDRC_CENTAURUS_GENRNDISEP13        0xB0

/* Offset of the MHCI Controller Generic RNDIS Size EP14 */
#define USB_MHDRC_CENTAURUS_GENRNDISEP14        0xB4

/* Offset of the MHCI Controller Generic RNDIS Size EP15 */
#define USB_MHDRC_CENTAURUS_GENRNDISEP15        0xB8

/* Offset of the MHCI Controller Auto Req Register */
#define USB_MHDRC_CENTAURUS_CENTAURUS_AUTOREQ   0xD0

/* Offset of the MHCI Controller SRP Fix Time */
#define USB_MHDRC_CENTAURUS_SRPFIXTIME          0xD4

/* Offset of the MHCI Controller Teardown Register */
#define USB_MHDRC_CENTAURUS_TDOWN               0xD8

/* Offset of the MHCI Controller PHY UTMI Register */
#define USB_MHDRC_CENTAURUS_UTMI                0xE0

/* Offset of the MHCI Controller MGC UTMI Loopback Register */
#define USB_MHDRC_CENTAURUS_UTMILB              0xE4

/* Offset of the MHCI Controller Mode Register */
#define USB_MHDRC_CENTAURUS_MODE                0xE8

/* Offset of the MHCI Controller Core Regiter Base address */

#define USB_MHDRC_CENTAURUS_COMMON_REG_OFFSET   0x400

#define USB_MHDRC_CENTAURUS_MODE_IDDIG_A        0x00000000  
#define USB_MHDRC_CENTAURUS_MODE_IDDIG_B        0x00000100       
#define USB_MHDRC_CENTAURUS_IRQENABLE_SOF       0x00000008

#define USB_MHDRC_CENTAURUS_PRIVATE_REG_WRITE32(pMHDRC, OFFSET, VALUE)       \
    vxbWrite32(((pUSB_MUSBMHDRC)(pMHDRC))->pRegAccessHandle,                 \
        (void *)(((pUSB_MUSBMHDRC)(pMHDRC))->uRegBase + OFFSET),             \
        VALUE)

#define USB_MHDRC_CENTAURUS_PRIVATE_REG_READ32(pMHDRC, OFFSET)               \
    vxbRead32(((pUSB_MUSBMHDRC)(pMHDRC))->pRegAccessHandle,                  \
        (void *)(((pUSB_MUSBMHDRC)(pMHDRC))->uRegBase + OFFSET))


#ifdef __cplusplus
}
#endif

#endif /* __INCusbMhdrcPlatformCentaurush */


