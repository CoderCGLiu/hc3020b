/* usbTgtSerSio.c - USB CDC Serial Emulator SIO driver for VxBus */

/*
 * Copyright (c) 2009-2011, 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01r,05jun13,s_z  Remove NO_EFFECT issue (WIND00420327)
01q,06may13,s_z  Remove compiler warning (WIND00356717)
01p,04jan13,s_z  Remove compiler warning (WIND00390357)
01o,15may12,s_z  Correct coverity warning  
01n,13dec11,m_y  Modify according to code check result (WIND00319317)
01m,01aug11,ghs  Fix issue found during code review (WIND00250716)
01l,08apr11,ghs  Fix code coverity issue(WIND00264893)
01k,22mar11,ghs  Fix redirect message display and dummy buffer issue
01j,09mar11,ghs  Change parameter usage of calloc
01i,07mar11,ghs  Fix code review issues
01h,26jan11,ghs  Merge and port to new target platform,
                 base on usbTargSerSio.c
01g,07jan11,ghs  Clean up compile warnings (WIND00247082)
01f,09jul10,ghs  Do not use dummy buffer when serial port is not a console
                 (WIND00222364)
01e,10jun10,ghs  Put data into dummy buffer before device open
01d,13jan10,ghs  vxWorks 6.9 LP64 adapting
01c,18sep09,y_l  Code Coverity CID(2): add function return value check
                 (WIND00176509)
01b,10sep09,y_l  Code Coverity CID(18): maybe cause a NULL pointer operation,
                 (WIND00176509)
01a,07mar09,w_x  written based vxbTemplateSio.c
*/

/*

DESCRIPTION

usbTgtSerSio OVERVIEW

This is a USB CDC Serial Emulator serial driver for VxBus working on
Processor Local Bus.

This driver provides an interface for setting hardware
options; e.g., the number of stop bits, data bits, parity, etc,
and it provides an interface for polled communication which
can be used to provided external mode debugging (i.e., ROM monitor
style debugging) over a serial line. Currently only asynchronous mode
drivers are supported.

CALLBACKS
Servicing a "transmitter ready" interrupt involves making a callback to a
higher level library in order to get a character to transmit.
By default, this driver installs dummy callback routines which do
nothing. A higher layer library that wants to use this driver (e.g., ttyDrv)
will install its own callback routines using the SIO_INSTALL_CALLBACK
ioctl command. (See below).

The prototype for the transmit data callback SIO_CALLBACK_GET_TX_CHAR is:
\cs
    int sioTxCharGet
    (
    void * arg,             /@ callback argument @/
    char * pChar            /@ ptr to data location @/
    )
\ce
This callback routine should fetch the next character to send and store it
in the location pointed to by pChar.  It returns OK to indicate that a
character is ready and should be sent.  It returns ERROR to indicate that
no character was available and the transmitter can be placed in an idle state.

Likewise, a receiver interrupt handler makes a callback to pass the
character to the higher layer library.  It will be called by the driver
for each character received.  This routine should capture the data and pass
it along to other layers and eventually to the user.

The prototype for the receive data callback SIO_CALLBACK_PUT_RCV_CHAR is:
\cs
    void sioRxCharPut
    (
    void * arg,             /@ callback argument @/
    char data               /@ data byte @/
    )
\ce

MODES

Ideally the driver should support both polled and interrupt modes, and be
capable of switching modes dynamically. However this is not required.
VxWorks will be able to support a tty device on this driver even if
the driver only supports interrupt mode.
Polled mode is provided solely for WDB system mode usage.  Users can use
the polled mode interface directly, but there is no clear reason for doing so.
Normal access to SIO devices through ttyDrv only uses interrupt mode.

For dynamically switchable drivers, be aware that the driver may be
asked to switch modes in the middle of its input ISR. A driver's input ISR
will look something like this:

   inChar = *pDev->dr;          /@ read a char from data register @/
   *pDev->cr = GOT_IT;          /@ acknowledge the interrupt @/
   pDev->putRcvChar (...);      /@ give the character to the higher layer @/

If this channel is used as a communication path to an external mode
debug agent, and if the character received is a special "end of packet"
character, then the agent's callback will lock interrupts, switch
the device to polled mode, and use the device in polled mode for awhile.
Later on the agent will unlock interrupts, switch the device back to
interrupt mode, and return to the ISR.
In particular, the callback can cause two mode switches, first to polled mode
and then back to interrupt mode, before it returns.
This may require careful ordering of the callback within the interrupt
handler. For example, you may need to acknowledge the interrupt before
invoking the callback.

SEE ALSO: sioChanUtil and VxWorks Device Driver Developer's Guide.

INCLUDES: vxWorks.h, stdio.h, errnoLib.h, logLib.h, string.h, usb/usbOsal.h,
          usb/usbPlatform.h, usb/usb.h, usb/usbDescrCopyLib.h, usb/usbLib.h,
          usb/usbCdc.h, sioLib.h, hwif/vxbus/vxBus.h, usb/usbTgtSer.h,
          ../../../../../h/util/sioChanUtil.h

*/

/* includes */

#include <vxWorks.h>
#include <stdio.h>
#include <errnoLib.h>
#include <logLib.h>
#include <string.h>
#include <usb/usbOsal.h>
#include <usb/usbPlatform.h>
#include <usb/usb.h>
#include <usb/usbDescrCopyLib.h>
#include <usb/usbLib.h>
#include <usb/usbCdc.h>
#include <sioLib.h>
#include <hwif/vxbus/vxBus.h>
#include "../../../../../h/util/sioChanUtil.h"
#include <usb/usbTgtSer.h>

/* defines */

#define USBTGT_SER_CON_STARTUP_MSG              \
    "Console has been redirected to USB port...\n\r"

/* globals */

/* forward declarations */

LOCAL void usbTgtSerSioChanGet(VXB_DEVICE_ID, void *);

LOCAL void usbTgtSerSioChanConnect(VXB_DEVICE_ID, void *);

/*
 * The device_method_t table provides the list of driver
 * methods that this driver supports. For each driver
 * class supported by Wind River, one or more methods
 * are expected to be defined for the driver. For
 * sio driver class, the 'sioChanGet' and 'sioChanConnect' methods
 * are required to be supported.
 *
 * See VxWorks Device Driver Developer's Gude Volume 2, VxBus Driver
 * Methods in the Serial Drivers' section, for more information.
 */

device_method_t usbTgtSerSioDrvMethods[] =
    {
    DEVMETHOD(sioChanGet, usbTgtSerSioChanGet),
    DEVMETHOD(sioChanConnect, usbTgtSerSioChanConnect),
    {0, NULL}
    };

IMPORT USBTGT_ACM_SIO_CHAN * pUsbTgtSerSioChan[];

IMPORT int usbTgtSerDummyBfrSize;

IMPORT int usbTgtSerDefualtBaudrate;

IMPORT int usbTgtSerTxPriority;

IMPORT int usbTgtSerSysConNum;

IMPORT BOOL usbTgtSerDummyBufferEnable;

IMPORT BOOL usbTgtSerRedirectMsgEnable;

/* locals */

LOCAL int usbTgtSerIoctl(SIO_CHAN *, int, void *);

LOCAL int usbTgtSerTxStartup(SIO_CHAN *);

LOCAL int usbTgtSerCallbackInstall(SIO_CHAN *, int, STATUS (*)(void *,...), void *);

LOCAL int usbTgtSerPollInput(SIO_CHAN *, char *);

LOCAL int usbTgtSerPollOutput(SIO_CHAN *, char);

LOCAL int usbTgtSerModeSet(USBTGT_ACM_SIO_CHAN *, uint_t);

LOCAL int usbTgtSerOptSet(USBTGT_ACM_SIO_CHAN *, uint_t);

LOCAL void usbTgtSerSioTxTask(USBTGT_ACM_SIO_CHAN *);

LOCAL void usbTgtSerSioChannelConnect(VXB_DEVICE_ID, USBTGT_ACM_SIO_CHAN *);

LOCAL void usbTgtSerSioInit(USBTGT_ACM_SIO_CHAN *);

LOCAL void usbTgtSerDummyRxCallback(void *, char);

LOCAL STATUS usbTgtSerDummyTxCallback(void *, char *);

LOCAL STATUS usbTgtSerHup(USBTGT_ACM_SIO_CHAN *);

LOCAL STATUS usbTgtSerOpen(USBTGT_ACM_SIO_CHAN *);

/* driver functions */

LOCAL SIO_DRV_FUNCS usbTgtSerSioDrvFuncs =
    {
    usbTgtSerIoctl,
    usbTgtSerTxStartup,
    usbTgtSerCallbackInstall,
    usbTgtSerPollInput,
    usbTgtSerPollOutput
    };

LOCAL BOOL usbTgtSerDummyBfrFlushing;

/******************************************************************************
*
* usbTgtSerInstInit - first level initialization routine of SIO device
*
* This is the function called to perform the first level initialization of
* the usbTargSer SIO device.
*
* NOTE:
*
* This routine is called early during system initialization, and
* *MUST NOT* make calls to usb memory alloc functions like OSS_MALLOC
*
* SIO device initialization must be completed here, except interrupts source
* connection, to allow early console input/output in polling mode.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbTgtSerInstInit
    (
    VXB_DEVICE_ID           pDev
    )
    {
    int                     devId;
    USBTGT_ACM_SIO_CHAN *   pChan;

    /* check for vaild parameter */

    VXB_ASSERT_NONNULL_V (pDev);

    devId = pDev->pName[strlen(pDev->pName) - 1] - '0';

    /* allocate pChan */

    pChan = calloc(1, sizeof(USBTGT_ACM_SIO_CHAN));

    if (pChan == NULL)
        return;

    pUsbTgtSerSioChan[devId] = pChan;

    pChan->devId = devId;

    /* fill in bus subsystem fields */

    pChan->pDev = pDev;
    pDev->pDrvCtrl  = (void *) pChan;

    /* get the channel number */

    pChan->channelNo = sioNextChannelNumberAssign ();

    /* make non-standard methods available to OS layers */

    pDev->pMethods = &usbTgtSerSioDrvMethods[0];

    /* initialize device */

    usbTgtSerSioInit (pChan);
    }

/******************************************************************************
*
* usbTgtSerInstInit2 - second level initialization routine of SIO device
*
* This routine performs the second level initialization of the SIO device.
*
* NOTE:
*
* This routine is called later during system initialization. USB memory alloc
* functions like OSS_MALLOC are available at this time.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbTgtSerInstInit2
    (
    VXB_DEVICE_ID    pDev
    )
    {
    /* nothing is done here */
    }

/******************************************************************************
*
* usbTgtSerInstConnect - connect target dummy serial device to I/O system
*
* Nothing to do here. We want serial channels available reasonably
* early during the boot process.  Because this is a serial channel,
* we connect to the I/O system in sioChanConnect method
* instead of here.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbTgtSerInstConnect
    (
    VXB_DEVICE_ID    pDev
    )
    {
    /* nothing is done here */
    }

/******************************************************************************
*
* usbTgtSerSioChanGet - METHOD: get pChan for the specified interface
*
* This routine returns the pChan structure pointer for the
* specified interface.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/


LOCAL void usbTgtSerSioChanGet
    (
    VXB_DEVICE_ID    pDev,
    void *           pArg
    )
    {
    USBTGT_ACM_SIO_CHAN *   pChan;
    SIO_CHANNEL_INFO *      pInfo = (SIO_CHANNEL_INFO *) pArg;

    /* check for vaild parameter */

    if (pDev == NULL || pInfo == NULL)
        return;

    /* get the channel pointer */

    pChan = (USBTGT_ACM_SIO_CHAN *) (pDev->pDrvCtrl);

    /* if the channel number matches, pass the SIO_CHAN pointer */

    if ((pChan != NULL) && (pChan->channelNo == pInfo->sioChanNo))
        pInfo->pChan = pChan;
    }

/******************************************************************************
*
* usbTgtSerSioChanConnect - METHOD: connect the specified interface to /tyCo/X
*
* This routine connects the specified channel number to the I/O subsystem.
* If successful, it returns the pChan structure pointer of the channel
* connected in the SIO_CHANNEL_INFO struct.
*
* If the specified channel is -1, or if the specified channel matches
* any channel on this instance, then connect the channel.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbTgtSerSioChanConnect
    (
    VXB_DEVICE_ID     pDev,
    void *            pArg
    )
    {
    SIO_CHANNEL_INFO *      pInfo = (SIO_CHANNEL_INFO *)pArg;
    USBTGT_ACM_SIO_CHAN *   pChan;

    /* check for vaild parameter */

    if (pDev == NULL || pInfo == NULL)
        return;

    /* get the channel pointer */

    pChan = pDev->pDrvCtrl;

    if (pChan == NULL)
        return;

    /* connect all channels ? */

    if (pInfo->sioChanNo == -1)
        {
        /* yes, connect all channels */

        /* connect to I/O system and connect interrupts */

        usbTgtSerSioChannelConnect(pDev, pChan);
        }
    else
        {
        /* no, only connect specified channel */

        /* check previous instance success */

        if (pInfo->pChan != NULL)
            return;

        if (pChan->channelNo != pInfo->sioChanNo)
            return;

        /* connect to I/O system and connect interrupts */

        usbTgtSerSioChannelConnect(pDev, pChan);

        /* notify caller and downstream instances of success */

        pInfo->pChan = pChan;
        }
    }

/******************************************************************************
*
* usbTgtSerSioChannelConnect - connect the specified channel to /tyCo/X
*
* This routine connects the interrupts source for the specified channel
* and enable the interrupts.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbTgtSerSioChannelConnect
    (
    VXB_DEVICE_ID           pDev,
    USBTGT_ACM_SIO_CHAN *  pChan
    )
    {
    /* Nothing to do */
    }

/******************************************************************************
*
* usbTgtSerSioInit - initialize a on-chip serial communication interface
*
* This routine initializes the driver function pointers and then resets
* the chip in a quiescent state.
* The BSP must have already initialized all the device addresses and the
* baudFreq fields in the USBTGT_ACM_SIO_CHAN structure before
* passing it to this routine.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbTgtSerSioInit
    (
    USBTGT_ACM_SIO_CHAN *  pChan
    )
    {
    /* initialize a channel's driver function pointers */

    pChan->pDrvFuncs = &usbTgtSerSioDrvFuncs;

    pChan->ttyNum = -1;

    /* install dummy driver callbacks */

    pChan->getTxChar  = (STATUS (*)(void *, char *)) usbTgtSerDummyTxCallback;
    pChan->putRcvChar = (STATUS (*)(void *, char)) usbTgtSerDummyTxCallback;

    pChan->onClosing = FALSE;

    if (usbTgtSerDummyBufferEnable == TRUE)
        pChan->dummyBfr = calloc(1, usbTgtSerDummyBfrSize);
    else
        pChan->dummyBfr = NULL;

    if (pChan->dummyBfr != NULL)
        pChan->dummyBfrSize = usbTgtSerDummyBfrSize;
    else
        pChan->dummyBfrSize = 0;

    pChan->dummyBfrDataSize = 0;

    /* set initial baud rate */

    usbTgtSerIoctl ((SIO_CHAN *)pChan, SIO_BAUD_SET,
                    (void *)((ULONG)usbTgtSerDefualtBaudrate));

    /* setting polled mode is one way to make the device quiet */

    usbTgtSerIoctl ((SIO_CHAN *)pChan, SIO_MODE_SET, (void *)SIO_MODE_INT);
    }

/******************************************************************************
*
* usbTgtSerSioIntRcv - handle a channel's receive-character interrupt
*
* This routine handles a channel's receive-character interrupt for serial
* emulator when received data from out pipe. As USB Target serial emualtor,
* this is run in task mode.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbTgtSerSioIntRcv
    (
    USBTGT_ACM_SIO_CHAN *  pChan /* channel generating the interrupt */
    )
    {
    int count = 0;

    if (pChan->putRcvChar == NULL || pChan->pUsbTgtSerDev == NULL)
        {
        USBTGT_SER_ERR("parameter error\n", 1, 2, 3, 4, 5, 6);

        return;
        }

    /*
     * When received any data when this channel is a console, flush dummy buffer
     * of this channel.
     */

    if (usbTgtSerPortStateCheck(pChan) == TRUE)
        usbTgtSerSioFlushBuffer(pChan);

    for (count = 0; count < (int)pChan->actRxLen; count++)
        {
        (*pChan->putRcvChar)(pChan->putRcvArg,
                             (char)pChan->pUsbTgtSerDev->bulkOutData[count]);
        }
    }

/******************************************************************************
*
* usbTgtSerSioIntTx - handle a channels transmitter-ready interrupt
*
* This routine gets data from tty-lib layer and sends data by Erp or to dummy
* buffer. As USB Target serial emualtor, this is run in task mode.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbTgtSerSioIntTx
    (
    USBTGT_ACM_SIO_CHAN *  pChan /* channel generating the interrupt */
    )
    {
    int     count = 0;
    int     actLen;
    UINT8   bfr[USBTGT_SER_MAX_BUFF_LEN];

    if (pChan->getTxChar == NULL)
        {
        USBTGT_SER_ERR("pChan->getTxChar is NULL\n", 1, 2, 3, 4, 5, 6);

        return;
        }

    while ((*pChan->getTxChar) (pChan->getTxArg,
                                (char *)&bfr[count]) != ERROR)
        {
        count++;

        if (count >= USBTGT_SER_MAX_BUFF_LEN)
            break;
        }

    if (count > 0)
        {
        /*
         * If this channel is a console and port does not open, store the data
         * in dummy buffer.
         */

        USBTGT_SER_VDBG("usbTgtSerSioIntTx(): transfer data len [%d] [%s]\n",
                        count, bfr, 3, 4, 5, 6);

        if (usbTgtSerPortStateCheck(pChan) == TRUE)
            {
            if (pChan->dummyBfr != NULL)
                {
                actLen = (int)(pChan->dummyBfrSize - pChan->dummyBfrDataSize);

                actLen = actLen > count ? count : actLen;

                memcpy(pChan->dummyBfr + pChan->dummyBfrDataSize, bfr, actLen);

                pChan->dummyBfrDataSize = pChan->dummyBfrDataSize + actLen;
                }

            return;
            }

        /* if dummy buffer data is flushing, wait for finished */

        while(usbTgtSerDummyBfrFlushing == TRUE)
            ;

        /* drop the data if device not inited */

        if (pChan->pUsbTgtSerDev != NULL)
            {
            USBTGT_SER_VDBG("usbTgtSerSioIntTx(): call usbTgtSerBulkInErpInit\n",
                            1, 2, 3, 4, 5, 6);


            if (usbTgtSerBulkInTransfer(pChan->pUsbTgtSerDev, (char *)bfr, count) != OK)
                return;
            }

        /*
         * Wait for the BULK IN callback is called
         * which means the transmit is done; We can use
         * binary semaphore to do this but since it is
         * easy and simple here...
         */

        while(pChan->pUsbTgtSerDev != NULL
              && pChan->pUsbTgtSerDev->bulkInInUse == TRUE)
            {
            taskDelay(1);
            }
        }
    }

/******************************************************************************
*
* usbTgtSerSioTxTask - task routine to wait tx chars and transmit them
*
* This routine is the task routine that waits for tx chars from tty layer
* and finally transmit them using the USB target bulk in pipe.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbTgtSerSioTxTask
    (
    USBTGT_ACM_SIO_CHAN *  pChan
    )
    {

    while (pChan->onClosing == FALSE)
        {
        usbTgtSerSioIntTx(pChan);
        taskDelay(1);
        }

    pChan->txThread = 0;
    }

/******************************************************************************
*
* usbTgtSerSioFlushBufferTask - flush dummy buffer task
*
* This routine runs as a task to flush dummy buffer data to real device.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbTgtSerSioFlushBufferTask
    (
    USBTGT_ACM_SIO_CHAN *  pChan /* channel generating the interrupt */
    )
    {
    int i;

    usbTgtSerDummyBfrFlushing = TRUE;

    pChan->portState = USBTGT_SER_PORT_OPEN;

    if (usbTgtSerSysConNum == pChan->channelNo)
        {
        if (OK != usbTgtSerBulkInTransfer(pChan->pUsbTgtSerDev,
                                          pChan->dummyBfr,
                                          pChan->dummyBfrDataSize))
            {
            USBTGT_SER_DBG("usbTgtSerBulkInTransfer fail\n", 1, 2, 3, 4, 5, 6);
            }   
        }

    pChan->dummyBfrDataSize = 0;

    usbTgtSerDummyBfrFlushing = FALSE;
    }

/******************************************************************************
*
* usbTgtSerSioFlushBuffer - flush dummy buffer data to real device
*
* This routine is called when serial emualtor port is opened by host.
* It start a new task to flush data.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbTgtSerSioFlushBuffer
    (
    USBTGT_ACM_SIO_CHAN *  pChan /* channel generating the interrupt */
    )
    {
    if (usbTgtSerDummyBufferEnable == FALSE)
        return;

    if (pChan != NULL)
        OS_CREATE_THREAD("tUsbSerTxFlush", usbTgtSerTxPriority,
                         usbTgtSerSioFlushBufferTask, pChan);
    }

/*******************************************************************************
*
* usbTgtSerIoctl - special device control
*
* This routine handles the IOCTL messages from the user. It supports commands
* to get/set baud rate, mode(INT,POLL), hardware options(parity, number of
* data bits) and modem control(RTS/CTS and DTR/DSR handshakes).
* The ioctl commands SIO_HUP and SIO_OPEN are used to implement the HUPCL(hang
* up on last close) function.
*
* As on a UNIX system, requesting a baud rate of zero is translated into
* a hangup request.  The DTR and RTS lines are dropped.  This should cause
* a connected modem to drop the connection.  The SIO_HUP command will only
* hangup if the HUPCL option is active.  The SIO_OPEN function will raise
* DTR and RTS lines whenever it is called. Use the BAUD_RATE=0 function
* to hangup when HUPCL is not active.
*
* The CLOCAL option will disable hardware flow control.  When selected,
* hardware flow control is not used.  When not selected hardware flow control
* is based on the RTS/CTS signals.  CTS is the clear to send input
* from the other end.  It must be true for this end to begin sending new
* characters.  In most drivers, the RTS signal will be assumed to be connected
* to the opposite end's CTS signal and can be used to control output from
* the other end.  Raising RTS asserts CTS at the other end and the other end
* can send data.  Lowering RTS de-asserts CTS and the other end will stop
* sending data. (This is non-EIA defined use of RTS).
*
* RETURNS: OK on success, ENOSYS on unsupported request, EIO on failed
* request.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL int usbTgtSerIoctl
    (
    SIO_CHAN *      pSioChan,               /* device to control */
    int             request,                /* request code */
    void *          someArg                 /* some argument */
    )
    {
    USBTGT_ACM_SIO_CHAN *   pChan = (USBTGT_ACM_SIO_CHAN *) pSioChan;
    ULONG                   arg = (ULONG)someArg;

    switch (request)
        {
        case SIO_BAUD_SET:

            /*
             * like unix, a baud request for 0 is really a request to
             * hangup.
             */

            if (arg == 0)
                return (usbTgtSerHup (pChan));

            pChan->baudFreq = (int)arg;

            pChan->lineCoding.bCharFormat = (UINT8)arg;

            return (OK);

        case SIO_BAUD_GET:

            *(int *)someArg = pChan->baudFreq;
            return (OK);

        case SIO_MODE_SET:

            /*
             * Set the mode (e.g., to interrupt or polled). Return OK
             * or EIO for an unknown or unsupported mode.
             */

            return (usbTgtSerModeSet (pChan, (UINT32)arg));

        case SIO_MODE_GET:

            /* Get the current mode and return OK.  */

            *(int *)someArg = pChan->mode;
            return (OK);

        case SIO_AVAIL_MODES_GET:

            /* TODO - set the available modes and return OK.  */

            *(int *)someArg = SIO_MODE_INT | SIO_MODE_POLL;
            return (OK);

        case SIO_HW_OPTS_SET:

            /*
             * Optional command to set the hardware options (as defined
             * in sioLib.h).
             * Return OK, or ENOSYS if this command is not implemented.
             * Note: several hardware options are specified at once.
             * This routine should set as many as it can and then return
             * OK. The SIO_HW_OPTS_GET is used to find out which options
             * were actually set.
             */

            return (usbTgtSerOptSet (pChan, (UINT32)arg));

        case SIO_HW_OPTS_GET:

            /*
             * Optional command to get the hardware options (as defined
             * in sioLib.h). Return OK or ENOSYS if this command is not
             * implemented.  Note: if this command is unimplemented, it
             * will be assumed that the driver options are CREAD | CS8
             * (e.g., eight data bits, one stop bit, no parity, ints enabled).
             */

            *(UINT32 *)someArg = pChan->options;
            return (OK);

        case SIO_HUP:

                /* check if hupcl option is enabled */

                if (pChan->options & HUPCL)
                    return (usbTgtSerHup (pChan));
                return (OK);

        case SIO_OPEN:
            return (usbTgtSerOpen (pChan)); /* always open */

        default:
            return ENOSYS;
        }
    }

/******************************************************************************
*
* usbTgtSerTxStartup - start the interrupt transmitter
*
* This routine is to inform the device to start transmitting data again.
* The actual data will be obtained by calling the TxCharGet callback routine.
*
* This routine is usually just the same as the tx interrupt routine.  This
* routine runs at task level context and does not have to be interrupt safe.
*
* RETURNS: OK on success, ENOSYS if the device is polled-only, or
* EIO on hardware error.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL int usbTgtSerTxStartup
    (
    SIO_CHAN * pSioChan                 /* channel to start */
    )
    {
    USBTGT_ACM_SIO_CHAN *   pChan = (USBTGT_ACM_SIO_CHAN *)pSioChan;

    if (pChan == NULL)
        return ERROR;

    if (pChan->txThread == 0 && pChan->channelNo >= 0)
        {
        char taskName[12] = {"tUsbSerTx"};

        taskName[strlen(taskName)] = (char) (pChan->devId + '0');
        OSS_THREAD_CREATE((THREAD_PROTOTYPE)usbTgtSerSioTxTask,
                          pChan,
                          (UINT16)usbTgtSerTxPriority,
                          taskName,
                          &pChan->txThread);

        if (usbTgtSerSysConNum == pChan->channelNo
            && pChan->channelNo != 0
            && usbTgtSerRedirectMsgEnable == TRUE)
            {

            /* display redirect message */

            int fd;
            int tyBaudRate;

            fd = open("/tyCo/0", O_RDWR, 0);

            if (fd != NONE)
                {
                /* store old baud rate */
                (void) ioctl(fd, SIO_BAUD_GET, &tyBaudRate);

                /* set to console baud rate */
                (void) ioctl(fd, SIO_BAUD_SET, &pChan->baudFreq);

                /* write tip message */
                write(fd, USBTGT_SER_CON_STARTUP_MSG,
                      strlen(USBTGT_SER_CON_STARTUP_MSG));

                /* restore old baud rate */
                
                (void) ioctl(fd, SIO_BAUD_SET, tyBaudRate);

                (void) close(fd);
                }
            }
        }

    return (OK);
    }

/******************************************************************************
*
* usbTgtSerCallbackInstall - install ISR callbacks to get/put chars
*
* This driver allows interrupt callbacks for transmitting characters
* and receiving characters. In general, drivers may support other
* types of callbacks too.
*
* RETURNS: OK on success, or ENOSYS for an unsupported callback type.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL int usbTgtSerCallbackInstall
    (
    SIO_CHAN *      pSioChan,                   /* channel */
    int             callbackType,               /* type of callback */
    STATUS        (*callback)(void *,...),      /* callback */
    void *          callbackArg                 /* parameter to callback */
    )
    {
    USBTGT_ACM_SIO_CHAN *   pChan = (USBTGT_ACM_SIO_CHAN *)pSioChan;

    switch (callbackType)
        {
        case SIO_CALLBACK_GET_TX_CHAR:
            pChan->getTxChar    = (STATUS (*)(void *, char *))callback;
            pChan->getTxArg     = callbackArg;
            USBTGT_SER_DBG("usbTgtSerCallbackInstall(): "
                           "SIO_CALLBACK_GET_TX_CHAR\n", 1, 2, 3, 4, 5, 6);
            return (OK);

        case SIO_CALLBACK_PUT_RCV_CHAR:
            pChan->putRcvChar   = (STATUS (*)(void *, char))callback;
            pChan->putRcvArg    = callbackArg;
            USBTGT_SER_DBG("usbTgtSerCallbackInstall(): "
                           "SIO_CALLBACK_PUT_RCV_CHAR\n", 1, 2, 3, 4, 5, 6);
            return (OK);

        default:
            return (ENOSYS);
        }
    }

/******************************************************************************
*
* usbTgtSerPollInput - poll the device for input
*
* Polled mode operation takes place without any kernel or other OS
* services available.  Use extreme care to insure that this code does not
* call any kernel services.  Polled mode is only for WDB system mode use.
* Kernel services, semaphores, tasks, etc, are not available during WDB
* system mode.
*
* RETURNS: OK if a character arrived, EIO on device error, EAGAIN
* if the input buffer if empty, ENOSYS if the device is
* interrupt-only.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL int usbTgtSerPollInput
    (
    SIO_CHAN *    pSioChan,
    char *        thisChar
    )
    {
    USBTGT_ACM_SIO_CHAN *   pChan = (USBTGT_ACM_SIO_CHAN *)pSioChan;

    if (pChan->pUsbTgtSerDev == NULL)
        return ERROR;

    /* read character, store it, and return OK  */

    pChan->pUsbTgtSerDev->bulkOutDataInUse = TRUE;

    if (usbTgtSerBulkOutErpInit(pChan->pUsbTgtSerDev,
                                pChan->pUsbTgtSerDev->bulkOutData,
                                1,
                                usbTgtSerBulkOutErpCallback,
                                NULL) != OK) /* no usrPtr for callbak */
        {
        pChan->pUsbTgtSerDev->bulkOutDataInUse = FALSE;
        return ERROR;
        }

    while (pChan->pUsbTgtSerDev->bulkOutInUse == TRUE);

    *thisChar = (char)pChan->pUsbTgtSerDev->bulkOutData[0];

    pChan->pUsbTgtSerDev->bulkOutDataInUse = FALSE;

    return (OK);
    }

/*******************************************************************************
*
* usbTgtSerPollOutput - output a character in polled mode
*
* Polled mode operation takes place without any kernel or other OS
* services available.  Use extreme care to insure that this code does not
* call any kernel services.  Polled mode is only for WDB system mode use.
* Kernel services, semaphores, tasks, etc, are not available during WDB
* system mode.
*
* RETURNS: OK if a character arrived, EIO on device error, EAGAIN
* if the output buffer if full. ENOSYS if the device is
* interrupt-only.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL int usbTgtSerPollOutput
    (
    SIO_CHAN *      pSioChan,
    char            outChar
    )
    {
    USBTGT_ACM_SIO_CHAN *   pChan = (USBTGT_ACM_SIO_CHAN *)pSioChan;

    if (pChan->pUsbTgtSerDev == NULL)
        return ERROR;

    /* if this channel is console and port is not open, write to dummy buffer */

    if (usbTgtSerPortStateCheck(pChan) == TRUE)
        {
        if (pChan->dummyBfrDataSize < pChan->dummyBfrSize)
            {
            pChan->dummyBfr[pChan->dummyBfrDataSize++] = outChar;
            return OK;
            }
        else
            return ERROR;
        }

    /* transmit the character */

    if (usbTgtSerBulkInTransfer(pChan->pUsbTgtSerDev, &outChar, 1) != OK)
        return ERROR;

    while (pChan->pUsbTgtSerDev->bulkInInUse == TRUE);

    return (OK);
    }

/*******************************************************************************
*
* usbTgtSerHup - hang up the modem control lines
*
* Resets the RTS and DTR signals.
*
* RETURNS: OK always.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtSerHup
    (
    USBTGT_ACM_SIO_CHAN *   pChan     /* pointer to channel */
    )
    {
    /*
     * TODO - Use global intCpuLock if lockout time will be very short. If not,
     * use a device specific lockout that will not damage overall system
     * latency.
     */

    return (OK);
    }

/******************************************************************************
*
* usbTgtSerModeSet - toggle between interrupt and polled mode
*
* RETURNS: OK on success, EIO on unsupported mode.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL int usbTgtSerModeSet
    (
    USBTGT_ACM_SIO_CHAN *   pChan,        /* channel */
    uint_t                  newMode       /* new mode */
    )
    {
    if ((newMode != SIO_MODE_POLL) && (newMode != SIO_MODE_INT))
        return (EIO);

    if (pChan->mode == SIO_MODE_INT)
        {
        /* TODO - switch device to interrupt mode */
        USBTGT_SER_DBG("usbTgtSerModeSet(): SIO_MODE_INT\n", 1, 2, 3, 4, 5, 6);
        }
    else
        {
        /* TODO - switch device to polled mode */
        USBTGT_SER_DBG("usbTgtSerModeSet(): SIO_MODE_POLL\n", 1, 2, 3, 4, 5, 6);
        }

    /* activate  the new mode */

    pChan->mode = (int)newMode;

    return (OK);
    }

/******************************************************************************
*
* usbTgtSerOptSet - set hardware options
*
* This routine sets up the hardware according to the specified option
* argument.  If the hardware cannot support a particular option value, then
* it should ignore that portion of the request.
*
* RETURNS: OK upon success, or EIO for invalid arguments.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL int usbTgtSerOptSet
    (
    USBTGT_ACM_SIO_CHAN *   pChan,        /* channel */
    uint_t                  newOpts       /* new options */
    )
    {
    int dataBits = 8;
    int stopBits = 1;
    BOOL hdweFlowCtrl=TRUE;
    BOOL rcvrEnable = TRUE;
    int  lvl;

    if (pChan == NULL || newOpts & 0xffffff00)
        return EIO;

    /* do nothing if options already set */

    if (pChan->options == newOpts)
        return OK;

    /* ignore requests for unsupported options */

    /* decode individual request elements */

    switch (newOpts & CSIZE)
        {
        case CS5:
            dataBits = 5; break;
        case CS6:
            dataBits = 6; break;
        case CS7:
            dataBits = 7; break;
        default:
        case CS8:
            dataBits = 8; break;
        }

    pChan->lineCoding.bDataBits = (UINT8)dataBits;

    if ((int)newOpts & STOPB)
        {
        stopBits = 2;
        pChan->lineCoding.bCharFormat = USB_CDC_2_STOP_BITS;
        }
    else
        {
        stopBits = 1;
        pChan->lineCoding.bCharFormat = USB_CDC_1_STOP_BITS;
        }

    switch (newOpts & (PARENB|PARODD))
        {
        case PARENB|PARODD:
            /* enable odd parity */
            pChan->lineCoding.bParityType = USB_CDC_ODD_PARITY;
            break;

        case PARENB:
            /* enable even parity */
            pChan->lineCoding.bParityType = USB_CDC_EVEN_PARITY;
            break;

        case PARODD:
            /* invalid mode, not normally used. */
            break;

        default:
        case 0:
            /* no parity */
            pChan->lineCoding.bParityType = USB_CDC_NO_PARITY;
            break;
        }

    if (newOpts & CLOCAL)
        {

        /* clocal disables hardware flow control */

        hdweFlowCtrl = FALSE;
        }

    if ((newOpts & CREAD) == 0)
        rcvrEnable = FALSE;

    lvl = intCpuLock ();

    /*
     * TODO - Reset the device according to dataBits, stopBits, hdweFlowCtrl,
     * rcvrEnable, and parity selections.
     */

    intCpuUnlock (lvl);

    /*
     * TODO - Be sure that pChan->options reflects the actual
     * hardware settings.  If 5 data bits were requested, but unsupported,
     * then be sure pChan->options reflects the actual number of data bits
     * currently selected.
     */

    pChan->options = newOpts;

    return (OK);
    }

/*******************************************************************************
*
* usbTgtSerOpen - Set the modem control lines
*
* Set the modem control lines(RTS, DTR) TRUE if not already set.
*
* RETURNS: OK, always
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtSerOpen
    (
    USBTGT_ACM_SIO_CHAN *   pChan     /* pointer to channel */
    )
    {
    /*
     * TODO - Use global intCpuLock if lockout time will be very short. If not,
     * use a device specific lockout that will not damage overall system
     * latency.
     */

    return (OK);
    }

/*******************************************************************************
*
* usbTgtSerDummyTxCallback - dummy Tx callback routine
*
* RETURNS: ERROR.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtSerDummyTxCallback
    (
    void * callbackArg,     /* argument registered with callback */
    char * pChara           /* ptr to data location */
    )
    {
    return (ERROR);
    }

/*******************************************************************************
*
* usbTgtSerDummyRxCallback - dummy Rx callback routine
*
* RETURNS: N/A.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbTgtSerDummyRxCallback
    (
    void *  callbackArg,    /* argument registered with callback */
    char    data            /* receive data */
    )
    {
    return;
    }


/*******************************************************************************
*
* usbTgtSerTestInTask - input routine for serial test
*
* This routine is input task function for serial test
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbTgtSerTestInTask
    (
    int fd
    )
    {
    char c[2];

    if (fd == NONE)
        {
        printf("parameter error\n");
        return;
        }
    while (TRUE)
        {
        (void) read(fd, &c[0], 1);

        (void) putchar(c[0]);
        }

    }

/*******************************************************************************
*
* usbTgtSerTest - test a device input and output
*
* This routine is a test function used to test a device input and output.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbTgtSerTest
    (
    char *  devName
    )
    {
    int     fd;
    char    b[512];
#if  (_WRS_VXWORKS_MAJOR > 6) || \
     ((_WRS_VXWORKS_MAJOR == 6) && (_WRS_VXWORKS_MINOR >= 9))
    TASK_ID inTaskId;
#else
    int     inTaskId;
#endif

    /* open device */
    fd = open(devName, O_RDWR, 0);

    if (fd == NONE)
        {
        printf("Open device %s failed\n", devName);
        return;
        }

    /* create input task */

    inTaskId = taskSpawn("tUsbTgtSerTestInTask", 100, VX_UNBREAKABLE, 0x4000,
                         (FUNCPTR)usbTgtSerTestInTask, fd,
                         0, 0, 0, 0, 0, 0, 0, 0, 0);

    if (inTaskId == OS_THREAD_FAILURE)
        {
        printf("fail to spwan new task\n");
        
        (void) close(fd);
        
        return;
        }

    printf("Type \"quit\" to exit this mode\n");

    while(TRUE)
        {
        printf("Input>");
        fgets(b, 512, stdin);

        if (strcmp(b, "quit\n") == 0
            || strcmp(b, "quit\n\r") == 0
            || strcmp(b, "quit\r\n") == 0
            || strcmp(b, "quit\r") == 0)
            {
            taskDelete(inTaskId);
            break;
            }

        write(fd, b, strlen(b));
        }

    (void) close(fd);
    }

