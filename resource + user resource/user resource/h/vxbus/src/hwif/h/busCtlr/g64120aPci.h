/* g64120aPci.h - header file for Galileo 64120 PCI bus driver */

/*
 * Copyright (c) 2007-2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01f,18jun08,h_k  removed vxbDevControl
01e,11apr08,h_k  updated for new register access methods.
01d,10mar08,h_k  removed pciConfigMech.
01c,01aug07,wap  Convert intLock()/intUnlock() to spinlock (WIND00100190)
01b,05mar07,dtr  Support latest vxBus config lib.
01a,11jan07,wap  written
*/

#ifndef __INCg64120aPcih
#define __INCg64120aPcih

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void g64120aPciRegister (void);

#ifndef BSP_VERSION

typedef struct g64120aPciDrvCtrl
    {
    VXB_DEVICE_ID       pDev;
    int			gtUnit;
    spinlockIsr_t	gtLock;
    struct hcfDevice *  pHcf;
    void *		pRegHandle;
    FUNCPTR		gtIntEnable;
    UINT32      autoConfig;
    int         pciMaxBus;     /* Max number of sub-busses */
    struct vxbPciConfig *pPciConfig;
    struct vxbPciInt *pIntInfo;

    } GT_DRV_CTRL;

LOCAL UINT32 csr_read_4 (VXB_DEVICE_ID, UINT32);
LOCAL void csr_write_4 (VXB_DEVICE_ID, UINT32, UINT32);
LOCAL UINT16 csr_read_2 (VXB_DEVICE_ID, UINT32);
LOCAL void csr_write_2 (VXB_DEVICE_ID, UINT32, UINT16);
LOCAL UINT8 csr_read_1 (VXB_DEVICE_ID, UINT32);
LOCAL void csr_write_1 (VXB_DEVICE_ID, UINT32, UINT8);

LOCAL UINT32 csr_read_4
    ( 
    VXB_DEVICE_ID pDev,
    UINT32 addr
    )
    {
    return (vxbRead32 (((GT_DRV_CTRL *)(pDev->pDrvCtrl))->pRegHandle,
		       (UINT32 *)((UINT32)pDev->pRegBase[0] + addr)));
    }

LOCAL void csr_write_4
    (
    VXB_DEVICE_ID pDev,
    UINT32 addr,
    UINT32 data
    )
    {
    vxbWrite32 (((GT_DRV_CTRL *)(pDev->pDrvCtrl))->pRegHandle,
		(UINT32 *)((UINT32)pDev->pRegBase[0] + addr), data);
    }

LOCAL UINT16 csr_read_2
    ( 
    VXB_DEVICE_ID pDev,
    UINT32 addr
    )
    {
    return (vxbRead16 (((GT_DRV_CTRL *)(pDev->pDrvCtrl))->pRegHandle,
		       (UINT16 *)((UINT32)pDev->pRegBase[0] + addr)));
    }

LOCAL void csr_write_2
    (
    VXB_DEVICE_ID pDev,
    UINT32 addr,
    UINT16 data
    )
    {
    vxbWrite16 (((GT_DRV_CTRL *)(pDev->pDrvCtrl))->pRegHandle,
		(UINT16 *)((UINT32)pDev->pRegBase[0] + addr), data);
    }

LOCAL UINT8 csr_read_1
    ( 
    VXB_DEVICE_ID pDev,
    UINT32 addr
    )
    {
    return (vxbRead8 (((GT_DRV_CTRL *)(pDev->pDrvCtrl))->pRegHandle,
		      (UINT8 *)((UINT32)pDev->pRegBase[0] + addr)));
    }

LOCAL void csr_write_1
    (
    VXB_DEVICE_ID pDev,
    UINT32 addr,
    UINT8 data
    )
    {
    vxbWrite8 (((GT_DRV_CTRL *)(pDev->pDrvCtrl))->pRegHandle,
	       (UINT8 *)((UINT32)pDev->pRegBase[0] + addr), data);
    }

#define CSR_READ_4 csr_read_4
#define CSR_READ_2 csr_read_2
#define CSR_READ_1 csr_read_1
#define CSR_WRITE_4 csr_write_4
#define CSR_WRITE_2 csr_write_2
#define CSR_WRITE_1 csr_write_1

#define CSR_SETBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) | (val))

#define CSR_CLRBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) & ~(val))

#define CSR_SETBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) | (val))

#define CSR_CLRBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) & ~(val))

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#endif /* BSP_VERSION */

#ifdef __cplusplus
}
#endif

#endif /* __INCg64120aPcih */

