/* usbTgtSerLib.c - USB CDC ACM Serial Emulation library */

/*
 * Copyright (c) 2009-2013, 2015 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
Modification History
--------------------
01w,02feb15,lan  Modify for WCID support (VXW6-83565)
01v,06may13,s_z  Remove compiler warning (WIND00356717)
01u,28feb13,ghs  Start bulk out ERP when device configuration is set
                 (WIND00405773)
01t,04jan13,s_z  Remove compiler warning (WIND00390357)
01s,18may12,s_z  Add support for USB 3.0 target (WIND00326012)
01r,13dec11,m_y  Modify according to code check result (WIND00319317)
01q,08apr11,ghs  Fix code coverity issue(WIND00264893)
01p,07apr11,ghs  Destroy all pipes when function driver detached
                 (WIND00265861)
01o,22mar11,s_z  Code clean up based on the code review
01n,22mar11,ghs  Sumbit a zero size packet when pervious size is equal to max
                 packet size(WIND00261833)
01m,09mar11,s_z  Code clean up based on the code review
01l,09mar11,ghs  Remove usage of targCallback
01k,07mar11,ghs  Fix code review issues
01j,23feb11,s_z  Add function driver device descriptor support
01i,26jan11,ghs  Merge and port to new target platform,
                 base on usbTargSerLib.c
01h,16jul10,ghs  Add return value for valid vendor specific request
                 (WIND00222573)
01g,09jul10,ghs  Do not use dummy buffer when serial port is not a console
                 (WIND00222364)
01f,10jun10,ghs  Put data into dummy buffer before device open
01e,08jun10,ghs  Fix speed change issue(WIND00216359)
01d,03mar10,ghs  Fix LP64 adapting
01c,13jan10,ghs  vxWorks 6.9 LP64 adapting
01b,10sep09,y_l  Code Coverity CID(20): Add return value, (WIND00176509)
01a,06mar09,w_x  written.
*/

/*
DESCRIPTION

This module defines those routines directly referenced by the USB peripheral
stack; namely, the routines that intialize the USB_TARG_CALLBACK_TABLE data
structure. Additional routines are also provided which are specific to the
usb cdc serial driver.

INCLUDES: usb/usbCdc.h, usb/usbTgtSer.h, usbTgtFunc.h

*/

/* includes */

#include <usb/usbCdc.h>
#include <usb/usbTgtSer.h>
#include <usbTgtFunc.h>

/* defines */

#define SER_BULK_IN_ENDPOINT_NUM    0x81    /* BULK IN endpoint */
#define SER_BULK_OUT_ENDPOINT_NUM   0x1     /* BULK OUT endpoint */
#define SER_INT_IN_ENDPOINT_NUM     0x82    /* INT IN endpoint */

/* string identifiers and indexes for string descriptors */

#define SER_ID_STR_MFG              1    /* manufacture's id */
#define SER_ID_STR_MFG_VAL          "Wind River Systems"/* manufacture's name */

#define SER_ID_STR_PROD             2    /* product id */
#define SER_ID_STR_PROD_VAL         "USB CDC Serial emulator" /* product name */

#define SER_ID_STR_SERIAL           3    /* serial */
#define SER_ID_STR_SERIAL_VAL       "012345678901"  /* serial number */

#define SER_ID_STR_CONTROL          4    /* control interface */
#define SER_ID_STR_CONTROL_VAL      "USB CDC Serial Control Interface"

#define SER_ID_STR_DATA             5    /* data interface */
#define SER_ID_STR_DATA_VAL         "USB CDC Serial Data Interface"

/* SER configuration */

#define SER_NUM_CONFIG                      1   /* number of configuration */
#define SER_CONFIG_VALUE                    1   /* configuration value */

/* SER interface */

#define SER_NUM_INTERFACES                  2   /* number of interfaces */
#define SER_CONTROL_INTERFACE_NUM           0   /* control interface number */
#define SER_CONTROL_INTERFACE_ALT_SETTING   0   /* control alt setting */
#define SER_DATA_INTERFACE_NUM              1   /* data interface number */
#define SER_DATA_INTERFACE_ALT_SETTING      0   /* default data alt setting */

#define SER_BULK_MAX_PACKETSIZE             0x40/* bulk max packet size */

#define USB_SER_INT_EP_INTERVAL_EXP         3   /* 1 << 3 == 8 msec */
#define USB_SER_INT_EP_MAX_PACKET_SIZE      8

#define SER_SHOW_BOOL(b) (((b) == FALSE) ? "FALSE" : "TRUE")

/* globals */

INT32 gUsbTgtSerDebug = 0;

IMPORT USBTGT_ACM_SIO_CHAN * pUsbTgtSerSioChan[];

IMPORT int usbTgtSerMaxCount;

IMPORT int usbTgtSerSysConNum;

IMPORT int usbTgtSerTxPriority;

IMPORT BOOL usbTgtSerDummyBufferEnable;


IMPORT char * usbTgtSerTargetName;
IMPORT char * usbTgtSerFuncName;

IMPORT UINT8  usbTgtSerTargetUnit;
IMPORT UINT8  usbTgtSerConfigNum;

/* locals */

LOCAL USB_DEVICE_DESCR gSerDevDescr =   /* device descriptor */
    {
    USB_DEVICE_DESCR_LEN,           /* bLength */
    USB_DESCR_DEVICE,               /* bDescriptorType */
    TO_LITTLEW (USBTGT_VERSION_20), /* bcdUsb */
    USB_CLASS_COMM,                 /* bDeviceClass */
    0,                              /* bDeviceSubclass */
    0,                              /* bDeviceProtocol */
    USB_MIN_CTRL_PACKET_SIZE,       /* maxPacketSize0 */
    0,                              /* idVendor */
    0,                              /* idProduct */
    0,                              /* bcdDevice */
    SER_ID_STR_MFG,                 /* iManufacturer */
    SER_ID_STR_PROD,                /* iProduct */
    SER_ID_STR_SERIAL,              /* iSerialNumber */
    SER_NUM_CONFIG                  /* bNumConfigurations */
    };

/*
 * 3.5.1 Communications Class Endpoint Requirements
 *
 * The Communications Class interface requires one endpoint,
 * a management element. It optionally can have an additional endpoint,
 * the notification element. The management element uses the default
 * endpoint for all standard and Communications Class-specific requests.
 * The notification element normally uses an interrupt endpoint.
 */

LOCAL USB_INTERFACE_DESCR gSerControlIfDescr =    /* interface descriptor */
    {
    USB_INTERFACE_DESCR_LEN,            /* bLength */
    USB_DESCR_INTERFACE,                /* bDescriptorType */
    SER_CONTROL_INTERFACE_NUM,          /* bInterfaceNumber */
    SER_CONTROL_INTERFACE_ALT_SETTING,  /* bAlternateSetting - only 0 */
    1,                                  /* bNumEndpoints - 1 notify endpopint */
    USB_CLASS_COMM,                     /* bInterfaceClass */
    USB_CDC_SUBCLASS_ACM,               /* bInterfaceSubClass */
    USB_CDC_PROTO_NONE,                 /* bInterfaceProtocol */
    SER_ID_STR_CONTROL                  /* iInterface */
    };

LOCAL USB_ENDPOINT_DESCR gSerIntInEpDescr =
    {
    USB_ENDPOINT_DESCR_LEN,
    USB_DESCR_ENDPOINT,
    SER_INT_IN_ENDPOINT_NUM,
    USB_ATTR_INTERRUPT,
    TO_LITTLEW (USB_SER_INT_EP_MAX_PACKET_SIZE),
    (1 << USB_SER_INT_EP_INTERVAL_EXP),
    };

LOCAL USB_INTERFACE_DESCR gSerDataIfDescr =
    {
    USB_INTERFACE_DESCR_LEN,        /* bLength */
    USB_DESCR_INTERFACE,            /* bDescriptorType */
    1,                              /* bInterfaceNumber */
    0,                              /* bAlternateSetting */
    2,                              /* bNumEndpoints */
    USB_CLASS_CDC_DATA,             /* bInterfaceClass */
    0,                              /* bInterfaceSubClass */
    0,                              /* bInterfaceProtocol */
    SER_ID_STR_DATA,                /* iInterface */
    };

LOCAL USB_ENDPOINT_DESCR gSerBulkOutEpDescr =    /* OUT endpoint descriptor */
    {
    USB_ENDPOINT_DESCR_LEN,                 /* bLength */
    USB_DESCR_ENDPOINT,                     /* bDescriptorType */
    SER_BULK_OUT_ENDPOINT_NUM,              /* bEndpointAddress */
    USB_ATTR_BULK,                          /* bmAttributes */
    TO_LITTLEW (SER_BULK_MAX_PACKETSIZE),   /* max packet size */
    0                                       /* bInterval */
    };

LOCAL USB_ENDPOINT_DESCR gSerBulkInEpDescr =    /* IN endpoint descriptor */
    {
    USB_ENDPOINT_DESCR_LEN,                 /* bLength */
    USB_DESCR_ENDPOINT,                     /* bDescriptorType */
    SER_BULK_IN_ENDPOINT_NUM,               /* bEndpointAddress */
    USB_ATTR_BULK,                          /* bmAttributes */
    TO_LITTLEW (SER_BULK_MAX_PACKETSIZE),   /* max packet size */
    0                                       /* bInterval */
    };

LOCAL USB_ENDPOINT_DESCR gSerOtherSpeedBulkOutEpDescr = /* Other Speed OUT */
                                                        /* endpoint descriptor */
    {
    USB_ENDPOINT_DESCR_LEN,                     /* bLength */
    USB_DESCR_ENDPOINT,                         /* bDescriptorType */
    SER_BULK_OUT_ENDPOINT_NUM,                  /* bEndpointAddress */
    USB_ATTR_BULK,                              /* bmAttributes */
    TO_LITTLEW (USB_MAX_HIGH_SPEED_BULK_SIZE),  /* max packet size */
    0                                           /* bInterval */
    };

LOCAL USB_ENDPOINT_DESCR gSerOtherSpeedBulkInEpDescr = /* Other Speed IN */
                                                       /* endpoint descriptor */
    {
    USB_ENDPOINT_DESCR_LEN,                     /* bLength */
    USB_DESCR_ENDPOINT,                         /* bDescriptorType */
    SER_BULK_IN_ENDPOINT_NUM,                   /* bEndpointAddress */
    USB_ATTR_BULK,                              /* bmAttributes */
    TO_LITTLEW (USB_MAX_HIGH_SPEED_BULK_SIZE),  /* max packet size */
    0                                           /* bInterval */
    };

LOCAL USB_ENDPOINT_DESCR gSerOtherSpeedIntInEpDescr = /* Other Speed IN */
                                                       /* endpoint descriptor */
    {
    USB_ENDPOINT_DESCR_LEN,                     /* bLength */
    USB_DESCR_ENDPOINT,                         /* bDescriptorType */
    SER_INT_IN_ENDPOINT_NUM,                    /* bEndpointAddress */
    USB_ATTR_INTERRUPT,                         /* bmAttributes */
    TO_LITTLEW (0x40),                          /* max packet size */
    (1 << USB_SER_INT_EP_INTERVAL_EXP)          /* bInterval */
    };

/* Interrupt In endpoint companion descriptor */

LOCAL USB_ENDPOINT_COMPANION_DESCR gSerSsIntInEpCompDescr =
    {
    USB_ENDPOINT_COMP_DESCR_LEN,                 /* bLength */
    USB_DESCR_SS_ENDPOINT_COMPANION,             /* bDescriptorType */
    0x00,                                        /* bMaxBurst */
    0,                                           /* bmAttributes */
    0                                            /* wBytesPerInterval */
    };

LOCAL USB_ENDPOINT_DESCR gSerSsBulkOutEpDescr =    /* OUT endpoint descriptor */
    {
    USB_ENDPOINT_DESCR_LEN,                 /* bLength */
    USB_DESCR_ENDPOINT,                     /* bDescriptorType */
    SER_BULK_OUT_ENDPOINT_NUM,              /* bEndpointAddress */
    USB_ATTR_BULK,                          /* bmAttributes */
    TO_LITTLEW (USBTGT_EP_PKT_SIZE_1024),   /* max packet size */
    0                                       /* bInterval */
    };
/* Bulk OUT endpoint companion descriptor */

LOCAL USB_ENDPOINT_COMPANION_DESCR gSerSsBulkOutEpCompDescr =
    {
    USB_ENDPOINT_COMP_DESCR_LEN,                 /* bLength */
    USB_DESCR_SS_ENDPOINT_COMPANION,             /* bDescriptorType */
    0x0E,                                        /* bMaxBurst */
    0,                                           /* bmAttributes */
    0                                            /* wBytesPerInterval */
    };

LOCAL USB_ENDPOINT_DESCR gSerSsBulkInEpDescr =    /* IN endpoint descriptor */
    {
    USB_ENDPOINT_DESCR_LEN,                 /* bLength */
    USB_DESCR_ENDPOINT,                     /* bDescriptorType */
    SER_BULK_IN_ENDPOINT_NUM,               /* bEndpointAddress */
    USB_ATTR_BULK,                          /* bmAttributes */
    TO_LITTLEW (USBTGT_EP_PKT_SIZE_1024),   /* max packet size */
    0                                       /* bInterval */
    };

/* Bulk In endpoint companion descriptor */

LOCAL USB_ENDPOINT_COMPANION_DESCR gSerSsBulkInEpCompDescr =
    {
    USB_ENDPOINT_COMP_DESCR_LEN,                 /* bLength */
    USB_DESCR_SS_ENDPOINT_COMPANION,             /* bDescriptorType */
    0x0E,                                        /* bMaxBurst */
    0,                                           /* bmAttributes */
    0                                            /* wBytesPerInterval */
    };

LOCAL pUSB_DESCR_HDR gSerFuncDescHdr[] =
    {
    (pUSB_DESCR_HDR )&gSerDevDescr,         /* Device descriptor */
    (pUSB_DESCR_HDR )&gSerControlIfDescr,   /* Interface descriptor */
    (pUSB_DESCR_HDR )&gSerIntInEpDescr,     /* Endpint descriptor */
    (pUSB_DESCR_HDR )&gSerDataIfDescr,      /* Interface descriptor */
    (pUSB_DESCR_HDR )&gSerBulkInEpDescr,    /* Endpoint descriptor */
    (pUSB_DESCR_HDR )&gSerBulkOutEpDescr,   /* Endpoint descriptor */
    NULL                            /* End of descriptors, must have */
    };

LOCAL pUSB_DESCR_HDR gSerHsFuncDescHdr[] =
    {
    (pUSB_DESCR_HDR )&gSerDevDescr,                 /* Device descriptor */
    (pUSB_DESCR_HDR )&gSerControlIfDescr,           /* Interface descriptor */
    (pUSB_DESCR_HDR )&gSerOtherSpeedIntInEpDescr,   /* Endpint descriptor */
    (pUSB_DESCR_HDR )&gSerDataIfDescr,              /* Interface descriptor */
    (pUSB_DESCR_HDR )&gSerOtherSpeedBulkInEpDescr,  /* Endpoint descriptor */
    (pUSB_DESCR_HDR )&gSerOtherSpeedBulkOutEpDescr, /* Endpoint descriptor */
    NULL                            /* End of descriptors, must have */
    };

LOCAL pUSB_DESCR_HDR gSerSsFuncDescHdr[] =
    {
    (pUSB_DESCR_HDR )&gSerDevDescr,         /* Device descriptor */
    (pUSB_DESCR_HDR )&gSerControlIfDescr,   /* Interface descriptor */
    (pUSB_DESCR_HDR )&gSerIntInEpDescr,     /* Endpint descriptor */
    (pUSB_DESCR_HDR )&gSerSsIntInEpCompDescr,
    (pUSB_DESCR_HDR )&gSerDataIfDescr,      /* Interface descriptor */
    (pUSB_DESCR_HDR )&gSerSsBulkInEpDescr,    /* Endpoint descriptor */
    (pUSB_DESCR_HDR )&gSerSsBulkInEpCompDescr,
    (pUSB_DESCR_HDR )&gSerSsBulkOutEpDescr,   /* Endpoint descriptor */
    (pUSB_DESCR_HDR )&gSerSsBulkOutEpCompDescr,
    NULL                            /* End of descriptors, must have */
    };


/* forward declarations */

LOCAL VOID usbTgtSerSetLineCodingCallback(pVOID);

LOCAL STATUS usbTgtSerMngmtFunc(pVOID, USB_TARG_CHANNEL, UINT16, pVOID);

LOCAL STATUS usbTgtSerFeatureClear(pVOID, USB_TARG_CHANNEL, UINT8, UINT16,
                                   UINT16);

LOCAL STATUS usbTgtSerFeatureSet(pVOID, USB_TARG_CHANNEL, UINT8, UINT16,
                                 UINT16);

LOCAL STATUS usbTgtSerDescriptorGet(pVOID, USB_TARG_CHANNEL, UINT8, UINT8,
                                    UINT8, UINT16, UINT16, pUINT8, pUINT16);

LOCAL STATUS usbTgtSerConfigurationSet(pVOID, USB_TARG_CHANNEL, UINT8);

LOCAL STATUS usbTgtSerInterfaceGet(pVOID, USB_TARG_CHANNEL, UINT16,pUINT8);

LOCAL STATUS usbTgtSerInterfaceSet(pVOID, USB_TARG_CHANNEL, UINT16, UINT8);

LOCAL STATUS usbTgtSerVendorSpecific(pVOID, USB_TARG_CHANNEL, UINT8, UINT8,
                                     UINT16, UINT16, UINT16);

LOCAL STATUS usbTgtSerDestroyAllPipes(pUSBTGT_SER_DEV);
LOCAL STATUS usbTgtSerCreateAllPipes
    (
    pUSBTGT_SER_DEV pUsbTgtSerDev,
    UINT8           uConfig
    );

LOCAL USB_TARG_CALLBACK_TABLE usbTgtSerCallbackTable = /* callback table */
    {
    usbTgtSerMngmtFunc,                 /* mngmtFunc */
    usbTgtSerFeatureClear,              /* featureClear */
    usbTgtSerFeatureSet,                /* featureSet */
    NULL,                               /* configurationGet */
    usbTgtSerConfigurationSet,          /* configurationSet */
    usbTgtSerDescriptorGet,             /* descriptorGet */
    NULL,                               /* descriptorSet */
    usbTgtSerInterfaceGet,              /* interfaceGet */
    usbTgtSerInterfaceSet,              /* interfaceSet */
    NULL,                               /* statusGet */
    NULL,                               /* addressSet */
    NULL,                               /* synchFrameGet */
    usbTgtSerVendorSpecific             /* vendorSpecific */
    };

/******************************************************************************
*
* usbTgtSerFeatureClear - clear the specified feature
*
* This routine implements the clear feature standard device request.
*
* RETURNS: OK or ERROR if unable to clear the feature
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtSerFeatureClear
    (
    pVOID               param,          /* TCD specific parameter */
    USB_TARG_CHANNEL    targChannel,    /* target channel */
    UINT8               requestType,    /* request type */
    UINT16              feature,        /* feature to clear */
    UINT16              index           /* index */
    )
    {
    pUSBTGT_SER_DEV     pUsbTgtSerDev = (pUSBTGT_SER_DEV) param;
    USB_TARG_PIPE       pipeHandle = NULL;
    UINT8               uInterface;
    UINT8               uSuspendOp;

    /* check parameter */

    if (pUsbTgtSerDev == NULL ||
        pUsbTgtSerDev->targChannel != targChannel)
        {
        USBTGT_SER_ERR("parameter error\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* This request must be standard request from the host */

    if (((requestType & USB_RT_DIRECTION_MASK) != USB_RT_HOST_TO_DEV) ||
        ((requestType & USB_RT_CATEGORY_MASK) != USB_RT_STANDARD))
        {
        USBTGT_SER_ERR("unsupport requestType\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    USBTGT_SER_DBG("usbTgtSerFeatureClear(): requestType = %d feature = %d\n",
                    requestType, feature, 3, 4, 5, 6);

    requestType =
        (UINT8)(requestType& (~(USB_RT_DIRECTION_MASK | USB_RT_CATEGORY_MASK)));

    if (requestType == USB_RT_ENDPOINT && feature == USB_FSEL_DEV_ENDPOINT_HALT)
        {
        pipeHandle = usbTgtFuncFindPipeHandleByEpIndex(pUsbTgtSerDev->targChannel,
                                                       (UINT8)(index & 0xFF));
        if (pipeHandle == pUsbTgtSerDev->bulkInPipe)
            {
            pUsbTgtSerDev->bulkInStallStatus = FALSE;
            }
        else if (pipeHandle == pUsbTgtSerDev->bulkOutPipe)
            {
            pUsbTgtSerDev->bulkOutStallStatus = FALSE;
            }
        else
            {
            return ERROR;
            }

        return OK;
        }
    else if ((requestType == USB_RT_INTERFACE) &&
             (feature == USB_FSEL_DEV_FUNCTION_SUSPEND))
        {
        /* The MSB is the specific suspend options */

        uSuspendOp = (UINT8) (index >> 8);
        uInterface = (UINT8) (index & 0xFF);

        USBTGT_SER_DBG ("Clear interface %d suspendop %x\n",
                        uInterface, uSuspendOp, 3, 4, 5, 6);

        return OK;
        }

    return ERROR;
    }

/*******************************************************************************
*
* usbTgtSerFeatureSet - set the specified feature
*
* This routine implements the set feature standard device request.
*
* RETURNS: OK or ERORR if unable to set the feature.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtSerFeatureSet
    (
    pVOID               param,          /* TCD specific parameter */
    USB_TARG_CHANNEL    targChannel,    /* target channel */
    UINT8               requestType,    /* request type */
    UINT16              feature,        /* feature to set */
    UINT16              index           /* wIndex */
    )
    {
    pUSBTGT_SER_DEV     pUsbTgtSerDev = (pUSBTGT_SER_DEV) param;
    USB_TARG_PIPE       pipeHandle = NULL;
    UINT8               uInterface;
    UINT8               uSuspendOp;

    /* check parameter */

    if (pUsbTgtSerDev == NULL ||
        pUsbTgtSerDev->targChannel != targChannel)
        {
        USBTGT_SER_ERR("parameter error\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* This request must be standard request from the host */

    if (((requestType & USB_RT_DIRECTION_MASK) != USB_RT_HOST_TO_DEV) ||
        ((requestType & USB_RT_CATEGORY_MASK) != USB_RT_STANDARD))
        {
        USBTGT_SER_ERR("unsupport requestType\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    USBTGT_SER_DBG("usbTgtSerFeatureSet(): requestType = %d feature = %d\n",
                   requestType, feature, 3, 4, 5, 6);

    requestType =
        (UINT8)(requestType &(~(USB_RT_DIRECTION_MASK | USB_RT_CATEGORY_MASK)));

    if (requestType == USB_RT_ENDPOINT && feature == USB_FSEL_DEV_ENDPOINT_HALT)
        {
        pipeHandle = usbTgtFuncFindPipeHandleByEpIndex(pUsbTgtSerDev->targChannel,
                                                       (UINT8) (index & 0xFF));
        if (pipeHandle == pUsbTgtSerDev->bulkInPipe)
            {
            pUsbTgtSerDev->bulkInStallStatus = TRUE;
            }
        else if (pipeHandle == pUsbTgtSerDev->bulkOutPipe)
            {
            pUsbTgtSerDev->bulkOutStallStatus = TRUE;
            }
        else
            {
            return ERROR;
            }
        return OK;
        }
    else if ((requestType == USB_RT_INTERFACE) &&
             (feature == USB_FSEL_DEV_FUNCTION_SUSPEND))
        {
        /* The MSB is the specific suspend options */

        uSuspendOp = (UINT8) (index >> 8);
        uInterface = (UINT8) (index & 0xFF);

        USBTGT_SER_DBG ("Set interface %d suspendop %x\n",
                        uInterface, uSuspendOp, 3, 4, 5, 6);

        return OK;
        }

    return ERROR;
    }

/*******************************************************************************
*
* usbTgtSerDescriptorGet - get the specified descriptor
*
* This routine implements the get descriptor standard device request.
*
* RETURNS: OK or ERROR if unable to get the descriptor value
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtSerDescriptorGet
    (
    pVOID               param,          /*  TCD specific parameter */
    USB_TARG_CHANNEL    targChannel,    /* target chennel */
    UINT8               requestType,    /* request type */
    UINT8               descriptorType, /* descriptor type */
    UINT8               descriptorIndex,/* descriptor index */
    UINT16              languageId,     /* language id */
    UINT16              length,         /* length of descriptor */
    pUINT8              pBfr,           /* buffer to hold the descriptor */
    pUINT16             pActLen         /* actual length */
    )
    {
    pUSBTGT_SER_DEV     pUsbTgtSerDev = (pUSBTGT_SER_DEV) param;

    /* check parameter */

    if (pUsbTgtSerDev == NULL ||
        pUsbTgtSerDev->targChannel != targChannel ||
        pBfr == NULL)
        {
        USBTGT_SER_ERR("parameter error\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* This request must be standard request from the host */

    if (((requestType & USB_RT_DIRECTION_MASK) != USB_RT_DEV_TO_HOST) ||
        ((requestType & USB_RT_CATEGORY_MASK) != USB_RT_STANDARD) ||
        ((requestType & USB_RT_RECIPIENT_MASK) != USB_RT_DEVICE))
        {
        USBTGT_SER_ERR("unsupport requestType\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    USBTGT_SER_DBG("usbTgtSerDescriptorGet(): descriptorType = %d\n",
                   descriptorType, 2, 3, 4, 5, 6);

    if (descriptorType == USB_DESCR_STRING)
        {
        switch(descriptorIndex)
            {
            case SER_ID_STR_MFG:
                usbDescrStrCopy(pBfr, SER_ID_STR_MFG_VAL, length, pActLen);
                break;

            case SER_ID_STR_PROD:
                usbDescrStrCopy(pBfr, SER_ID_STR_PROD_VAL, length, pActLen);
                break;

            case SER_ID_STR_SERIAL:
                usbDescrStrCopy(pBfr, SER_ID_STR_SERIAL_VAL, length, pActLen);
                break;

            case SER_ID_STR_CONTROL:
                usbDescrStrCopy(pBfr, SER_ID_STR_CONTROL_VAL, length, pActLen);
                break;

            case SER_ID_STR_DATA:
                usbDescrStrCopy(pBfr, SER_ID_STR_DATA_VAL, length, pActLen);
                break;

            default:
                return ERROR;
            }
        }
    else
        return ERROR;

    return OK;
    }

/*******************************************************************************
*
* usbTgtSerConfigurationSet - set the specified configuration
*
* This routine is used to set the current configuration to the configuration
* value sent by host. <configuration> consists of the value to set.
*
* RETURNS: OK, or ERROR if unable to set specified configuration
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtSerConfigurationSet
    (
    pVOID               param,          /*  TCD specific parameter */
    USB_TARG_CHANNEL    targChannel,    /* target channel */
    UINT8               configuration   /* configuration value to set */
    )
    {
    pUSBTGT_SER_DEV     pUsbTgtSerDev = (pUSBTGT_SER_DEV) param;

    /* check parameter */

    if (pUsbTgtSerDev == NULL ||
        pUsbTgtSerDev->targChannel != targChannel ||
        pUsbTgtSerDev->pChan == NULL)
        {
        USBTGT_SER_ERR("parameter error\n", 1, 2, 3, 4, 5, 6);

        return ERROR ;
        }

    if (configuration > SER_CONFIG_VALUE)
        {
        USBTGT_SER_ERR("invalid configuration\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Set current configuration global static variable */

    if (configuration == 0)
        {
        USBTGT_SER_DBG("usbTgtSerConfigurationSet(): configuration = 0\n",
                       1, 2, 3, 4, 5, 6);

        (void)usbTgtSerDestroyAllPipes(pUsbTgtSerDev);

        return OK;
        }
    else
        {
        USBTGT_SER_VDBG("usbTgtSerConfigurationSet(): configuration = "
                        "SER_CONFIG_VALUE\n", 1, 2, 3, 4, 5, 6);

        /* Delete all teh pipes */

        (void)usbTgtSerDestroyAllPipes(pUsbTgtSerDev);

        /* Create all the pipes */

        if (ERROR == usbTgtSerCreateAllPipes(pUsbTgtSerDev, configuration))
            {
            (void)usbTgtSerDestroyAllPipes(pUsbTgtSerDev);

            return ERROR;
            }

        /* Initialize ERP to listen for data */

        if (usbTgtSerBulkOutErpInit(pUsbTgtSerDev, pUsbTgtSerDev->bulkOutData,
                                    USBTGT_SER_MAX_BUFF_LEN,
                                    usbTgtSerBulkOutErpCallback,
                                    pUsbTgtSerDev) != OK)
            {
            USBTGT_SER_ERR("Start bulk out ERP fail\n", 1, 2, 3, 4, 5, 6);

            (void)usbTgtSerDestroyAllPipes(pUsbTgtSerDev);

            return ERROR;
            }

        USBTGT_SER_DBG("usbTgtSerConfigurationSet(): Exiting...\n",
                       1, 2, 3, 4, 5, 6);
        }

    pUsbTgtSerDev->uConfiguration = configuration;

    return OK;

    }

/*******************************************************************************
*
* usbTgtSerInterfaceGet - get the specified interface
*
* This routine is used to get the selected alternate setting of the
* specified interface.
*
* RETURNS: OK, or ERROR if unable to return interface setting
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtSerInterfaceGet
    (
    pVOID               param,              /* TCD specific parameter */
    USB_TARG_CHANNEL    targChannel,        /* target channel */
    UINT16              interfaceIndex,     /* interface index */
    pUINT8              pAlternateSetting   /* alternate setting */
    )
    {
    pUSBTGT_SER_DEV     pUsbTgtSerDev = (pUSBTGT_SER_DEV) param;

    /* check parameter */

    if (pUsbTgtSerDev == NULL ||
        pUsbTgtSerDev->targChannel != targChannel)
        {
        USBTGT_SER_ERR("parameter error\n", 1, 2, 3, 4, 5, 6);
        return(ERROR);
        }

    if (pUsbTgtSerDev->uConfiguration == 0)
        {
        USBTGT_SER_ERR("configuration not set\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    USBTGT_SER_DBG("usbTgtSerInterfaceGet(): interfaceIndex = %d\n",
                   interfaceIndex, 2, 3, 4, 5, 6);

    if (interfaceIndex == SER_CONTROL_INTERFACE_NUM)
        {
        *pAlternateSetting = pUsbTgtSerDev->uControlIfAltSetting;
        }
    else if (interfaceIndex == SER_DATA_INTERFACE_NUM)
        {
        *pAlternateSetting = pUsbTgtSerDev->uDataIfAltSetting;
        }
    else
        return (ERROR);

    return (OK);
    }

/*******************************************************************************
*
* usbTgtSerInterfaceSet - set the specified interface
*
* This routine is used to select the alternate setting of he specified
* interface.
*
* RETURNS: OK, or ERROR if unable to set specified interface
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtSerInterfaceSet
    (
    pVOID               param,              /* TCD specific parameter */
    USB_TARG_CHANNEL    targChannel,        /* target channel */
    UINT16              interfaceIndex,     /* interface index */
    UINT8               alternateSetting    /* alternate setting */
    )
    {
    pUSBTGT_SER_DEV     pUsbTgtSerDev = (pUSBTGT_SER_DEV) param;

    /* check parameter */

    if (pUsbTgtSerDev == NULL ||
        pUsbTgtSerDev->targChannel != targChannel)
        {
        USBTGT_SER_ERR("parameter error\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    if (pUsbTgtSerDev->uConfiguration == 0)
        {
        USBTGT_SER_ERR("configuration not set\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    USBTGT_SER_DBG("usbTgtSerInterfaceSet(): interfaceIndex = %d "
                   "alternateSetting = %d\n", interfaceIndex, alternateSetting,
                   3, 4, 5, 6);

    if (interfaceIndex == SER_CONTROL_INTERFACE_NUM)
        {
        pUsbTgtSerDev->uControlIfAltSetting = alternateSetting;

        return OK;
        }
    else if (interfaceIndex == SER_DATA_INTERFACE_NUM)
        {
        pUsbTgtSerDev->uDataIfAltSetting = alternateSetting;

        return OK;
        }
    else
        return ERROR;
    }

/*******************************************************************************
*
* usbTgtSerVendorSpecific - invoke the VENDOR_SPECIFIC request
*
* This routine implements the vendor specific standard device request
*
* RETURNS: OK, or ERROR if unable to process vendor-specific request
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtSerVendorSpecific
    (
    pVOID               param,          /* TCD specific parameter */
    USB_TARG_CHANNEL    targChannel,    /* target channel */
    UINT8               requestType,    /* request type */
    UINT8               request,        /* request name */
    UINT16              value,          /* wValue */
    UINT16              index,          /* wIndex */
    UINT16              length          /* wLength */
    )
    {
    STATUS              retVal = ERROR;
    pUSBTGT_SER_DEV     pUsbTgtSerDev = (pUSBTGT_SER_DEV) param;

    /* check parameter */

    if (pUsbTgtSerDev == NULL ||
        pUsbTgtSerDev->targChannel != targChannel ||
        pUsbTgtSerDev->pChan == NULL)
        {
        USBTGT_SER_ERR("parameter error\n", 1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    USBTGT_SER_DBG("usbTgtSerVendorSpecific(): requestType %p request %p "
                   "value %p index %p length %p\n", requestType, request, value,
                   index, length, 6);

    if (requestType == (USB_RT_HOST_TO_DEV | USB_RT_CLASS | USB_RT_INTERFACE))
        {
        switch (request)
            {
            case USB_CDC_REQ_SET_LINE_CODING:
                /* Send the request to receive the data from the host */

                retVal = usbTgtControlPayloadRcv (targChannel,
                                      sizeof(USB_CDC_LINE_CODING),
                                      (pUINT8)&pUsbTgtSerDev->pChan->lineCoding,
                                      usbTgtSerSetLineCodingCallback);

                break;
            case USB_CDC_REQ_SET_CONTROL_LINE_STATE:
                pUsbTgtSerDev->pChan->controlLineStae = value;

                if (usbTgtSerPortStateCheck(pUsbTgtSerDev->pChan) == TRUE
                    && value > 0)
                    usbTgtSerSioFlushBuffer(pUsbTgtSerDev->pChan);

                break;
            case USB_CDC_REQ_SEND_BREAK:
                USBTGT_SER_VDBG("usbTgtSerVendorSpecific(): "
                                "USB_CDC_REQ_SEND_BREAK\n", 1, 2, 3, 4, 5, 6);

                break;
            case USB_CDC_SEND_ENCAPSULATED_COMMAND:
                USBTGT_SER_VDBG("usbTgtSerVendorSpecific(): "
                                "USB_CDC_SEND_ENCAPSULATED_COMMAND\n",
                                1, 2, 3, 4, 5, 6);
                break;
            }
        }
    else if (requestType == (USB_RT_DEV_TO_HOST | USB_RT_CLASS | USB_RT_INTERFACE))
        {
        switch (request)
            {
            case USB_CDC_REQ_GET_LINE_CODING:
                retVal = usbTgtControlResponseSend(targChannel,
                                          sizeof(USB_CDC_LINE_CODING),
                                          (pUINT8)&pUsbTgtSerDev->pChan->lineCoding,
                                          USB_ERP_FLAG_NORMAL);
                break;
            case USB_CDC_GET_ENCAPSULATED_RESPONSE:
                USBTGT_SER_VDBG("usbTgtSerVendorSpecific(): "
                                "USB_CDC_GET_ENCAPSULATED_RESPONSE\n",
                                1, 2, 3, 4, 5, 6);
                break;
            }
        }

    return(retVal);
    }

/*******************************************************************************
*
* usbTgtSerMngmtFunc - invoke the connection management function
*
* This routine handles various management related events. <mngmtCode>
* consist of the management event function code that is reported by the
* TargLib layer. <pContext> is the argument sent for the management event to
* be handled.
*
* RETURNS: OK if able to handle event, or ERROR if unable to handle event
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtSerMngmtFunc
    (
    pVOID               param,              /* TCD specific paramter */
    USB_TARG_CHANNEL    targChannel,        /* target channel */
    UINT16              mngmtCode,          /* management code */
    pVOID               pContext            /* Context value */
    )
    {
    pUSBTGT_SER_DEV     pUsbTgtSerDev = (pUSBTGT_SER_DEV) param;

    /* check parameter */

    if (pUsbTgtSerDev == NULL)
        {
        USBTGT_SER_ERR("parameter error\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    switch (mngmtCode)
        {
        case TARG_MNGMT_ATTACH:
            USBTGT_SER_DBG("usbTgtSerMngmtFunc(): TARG_MNGMT_ATTACH targChannel"
                           " = %p\n", targChannel, 2, 3, 4, 5, 6);

            pUsbTgtSerDev->targChannel = targChannel;
            pUsbTgtSerDev->uConfiguration = 0;
            pUsbTgtSerDev->uControlIfAltSetting = 0;
            pUsbTgtSerDev->uControlIfAltSetting = 0;
            pUsbTgtSerDev->bulkOutPipe = NULL;
            pUsbTgtSerDev->bulkInPipe = NULL;
            pUsbTgtSerDev->intInPipe  = NULL;

            break;

        case TARG_MNGMT_DETACH:
            USBTGT_SER_DBG("usbTgtSerMngmtFunc(): TARG_MNGMT_DETACH\n",
                           1, 2, 3, 4, 5, 6);

            gSerBulkInEpDescr.endpointAddress = SER_BULK_IN_ENDPOINT_NUM;
            gSerBulkOutEpDescr.endpointAddress = SER_BULK_OUT_ENDPOINT_NUM;
            gSerBulkInEpDescr.endpointAddress = SER_INT_IN_ENDPOINT_NUM;

            usbTgtSerDestroyAllPipes(pUsbTgtSerDev);

            break;

        case TARG_MNGMT_BUS_RESET:

            USBTGT_SER_DBG("usbTgtSerMngmtFunc(): TARG_MNGMT_BUS_RESET\n",
                           1, 2, 3, 4, 5, 6);

            pUsbTgtSerDev->uConfiguration = 0;
            pUsbTgtSerDev->uControlIfAltSetting = 0;
            pUsbTgtSerDev->uDataIfAltSetting = 0;

            usbTgtSerDestroyAllPipes(pUsbTgtSerDev);

            break;

        case TARG_MNGMT_DISCONNECT:
            USBTGT_SER_DBG("usbTgtSerMngmtFunc(): TARG_MNGMT_DISCONNECT\n",
                           1, 2, 3, 4, 5, 6);

            usbTgtSerConfigurationSet (param, targChannel, 0);

            break;

        case TARG_MNGMT_SUSPEND:
            USBTGT_SER_DBG("usbTgtSerMngmtFunc(): TARG_MNGMT_SUSPEND\n",
                           1, 2, 3, 4, 5, 6);

            /* when device change to suspend state, must not output */

            if (pUsbTgtSerDev->pChan != NULL)
                pUsbTgtSerDev->pChan->portState = USBTGT_SER_PORT_CLOSE;

            break;

        default:
            break;

        }

    return OK;
    }

/*******************************************************************************
*
* usbTgtSerBulkErpInit - initialize the ERP bulk data structure
*
* This routine initializes the ERP bulk data structure.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbTgtSerBulkErpInit
    (
    pUSB_ERP            pErp,
    UINT8 *             pData,          /* pointer to data */
    UINT32              size,           /* size of data */
    ERP_CALLBACK        erpCallback,    /* erp callback */
    pVOID               usrPtr,         /* user pointer */
    UINT8               direction
    )
    {
    if (pErp == NULL)
        return;

    memset(pErp, 0, sizeof(USB_ERP));

    pErp->erpLen = sizeof(USB_ERP);
    pErp->userCallback = erpCallback;
    pErp->targCallback = usbTgtErpCallback;
    pErp->bfrCount = 1;

    pErp->bfrList[0].pid = direction;
    pErp->bfrList[0].pBfr = pData;
    pErp->bfrList[0].bfrLen = size;
    pErp->userPtr = usrPtr;
    }

/*******************************************************************************
*
* usbTgtSerBulkInErpInit - initialize the bulk-in ERP
*
* This routine initializes the Bulk In ERP.
*
* RETURNS: OK, or ERROR if unable to submit ERP.
*
* ERRNO: N/A
*/

STATUS usbTgtSerBulkInErpInit
    (
    pUSBTGT_SER_DEV     pUsbTgtSerDev,
    UINT8 *             pData,          /* pointer to data */
    UINT32              size,           /* size of data */
    ERP_CALLBACK        erpCallback,    /* erp callback */
    pVOID               usrPtr          /* user pointer */
    )
    {
    if (pData == NULL ||
        pUsbTgtSerDev == NULL ||
        pUsbTgtSerDev->bulkInInUse == TRUE)
        {
        USBTGT_SER_ERR("parameter error\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    usbTgtSerBulkErpInit(&pUsbTgtSerDev->bulkInErp, pData, size,
                         erpCallback, usrPtr, USB_PID_IN);

    pUsbTgtSerDev->bulkInInUse = TRUE;
    pUsbTgtSerDev->bulkInBfrValid = FALSE;

    USBTGT_SER_VDBG("usbTgtSerBulkInErpInit(): submit erp data, len [%d]\n",
                    size, 2, 3, 4, 5, 6);

    if (usbTgtSubmitErp (pUsbTgtSerDev->bulkInPipe,
                         &pUsbTgtSerDev->bulkInErp) != OK)
        {
        pUsbTgtSerDev->bulkInInUse = FALSE;
        USBTGT_SER_ERR("usbTgtSubmitErp error in USB_PID_IN\n",
                       1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    return OK;
    }

/***************************************************************************
*
* usbTgtSerBulkInTransfer - transfer bulk in data
*
* This routine tansfers bulk in data, copies data from user buffer to ERP
* buffer and initializes ERP and transfers ERP.
*
* RETURNS: OK, or ERROR if fails to perform the action.
*
* ERRNO: N/A
*/

STATUS usbTgtSerBulkInTransfer
    (
    pUSBTGT_SER_DEV     pUsbTgtSerDev,
    char *              pBuffer,
    size_t              count
    )
    {
    if (pUsbTgtSerDev == NULL || pBuffer == NULL)
        return ERROR;

    pUsbTgtSerDev->bulkInDataInUse = TRUE;

    memcpy(pUsbTgtSerDev->bulkInData, pBuffer, count);

    if (usbTgtSerBulkInErpInit(pUsbTgtSerDev,
                               pUsbTgtSerDev->bulkInData,
                               (UINT32)count,
                               usbTgtSerBulkInErpCallback,
                               pUsbTgtSerDev) != OK)
        {
        USBTGT_SER_ERR("usbTgtSerBulkInErpInit failed\n",
                       1, 2, 3, 4, 5, 6);

        pUsbTgtSerDev->bulkInDataInUse = FALSE;

        return ERROR;
        }

    pUsbTgtSerDev->bulkInDataInUse = FALSE;

    return OK;
    }

/***************************************************************************
*
* usbTgtSerBulkOutErpInit - initialize the bulk-Out ERP
*
* This routine initializes the bulk Out ERP.
*
* RETURNS: OK, or ERROR if unable to submit ERP.
*
* ERRNO: N/A
*/

STATUS usbTgtSerBulkOutErpInit
    (
    pUSBTGT_SER_DEV     pUsbTgtSerDev,
    UINT8 *             pData,          /* pointer to buffer */
    UINT32              size,           /* size of data */
    ERP_CALLBACK        erpCallback,    /* IRP_CALLBACK */
    pVOID               usrPtr          /* user pointer */
    )
    {
    if (pData == NULL ||
        pUsbTgtSerDev == NULL ||
        pUsbTgtSerDev->bulkOutInUse == TRUE)
        {
        USBTGT_SER_ERR("parameter error\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    usbTgtSerBulkErpInit(&pUsbTgtSerDev->bulkOutErp, pData, size,
                         erpCallback, usrPtr, USB_PID_OUT);

    pUsbTgtSerDev->bulkOutInUse = TRUE;
    pUsbTgtSerDev->bulkOutBfrValid = FALSE;

    USBTGT_SER_VDBG("usbTgtSerBulkOutErpInit(): submit erp data, len [%d]\n",
                    size, 2, 3, 4, 5, 6);

    if (usbTgtSubmitErp (pUsbTgtSerDev->bulkOutPipe,
                         &pUsbTgtSerDev->bulkOutErp) != OK)
        {
        pUsbTgtSerDev->bulkOutInUse = FALSE;
        USBTGT_SER_ERR("usbTgtSubmitErp error in USB_PID_OUT\n",
                       1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    return OK;
    }

/*******************************************************************************
*
* usbTgtSerDestroyAllPipes - abort pending requests and destroy for all pipes
*
* This routine is used to destroy all pipes; before destroying the pipes,
* all requests pending for these pipes are also aborted.
*
* RETURNS: OK, or ERROR if fails to perform the action.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtSerDestroyAllPipes
    (
    pUSBTGT_SER_DEV pUsbTgtSerDev
    )
    {
    /* if the bulk in pipe is in use, try to clear it */

    if (pUsbTgtSerDev->bulkInInUse == TRUE)
        {
        if (usbTgtCancelErp(pUsbTgtSerDev->bulkInPipe,
                           &pUsbTgtSerDev->bulkInErp) != OK)
            {
            USBTGT_SER_ERR("usbTgtCancelErp failed for BULK IN\n",
                           1, 2, 3, 4, 5, 6);
            }

        pUsbTgtSerDev->bulkInInUse = FALSE;
        }

    /* if the bulk out pipe is in use, try to clear it */

    if (pUsbTgtSerDev->bulkOutInUse == TRUE)
        {
        if (usbTgtCancelErp(pUsbTgtSerDev->bulkOutPipe,
                           &pUsbTgtSerDev->bulkOutErp) != OK)
            {
            USBTGT_SER_ERR("usbTgtCancelErp failed for BULK OUT\n",
                           1, 2, 3, 4, 5, 6);
            }

        pUsbTgtSerDev->bulkOutInUse = FALSE;
        }

    if (pUsbTgtSerDev->intInInUse == TRUE)
        {
        if (usbTgtCancelErp(pUsbTgtSerDev->intInPipe,
                           &pUsbTgtSerDev->intInErp) != OK)
            {
            USBTGT_SER_ERR("usbTgtCancelErp failed for INT IN\n",
                           1, 2, 3, 4, 5, 6);
            }

        pUsbTgtSerDev->intInInUse = FALSE;
        }

    /* Destroy the bulk in pipe if already created */

    if (pUsbTgtSerDev->bulkInPipe != NULL)
        {
        usbTgtDeletePipe (pUsbTgtSerDev->bulkInPipe);
        pUsbTgtSerDev->bulkInPipe = NULL;
        pUsbTgtSerDev->bulkInInUse = FALSE;
        }

    /* Destroy the bulk out pipe if already created */
    if (pUsbTgtSerDev->bulkOutPipe != NULL)
        {
        usbTgtDeletePipe (pUsbTgtSerDev->bulkOutPipe);
        pUsbTgtSerDev->bulkOutPipe = NULL;
        pUsbTgtSerDev->bulkOutInUse = FALSE;
        }

    /* Destroy the int in pipe if already created */
    if (pUsbTgtSerDev->intInPipe != NULL)
        {
        usbTgtDeletePipe (pUsbTgtSerDev->intInPipe);
        pUsbTgtSerDev->intInPipe = NULL;
        pUsbTgtSerDev->intInInUse = FALSE;
        }

    return(OK);
    }


/*******************************************************************************
*
* usbTgtSerCreateAllPipes - create all the pipes of the serial device module
*
* This routine is used to create all the pipes of the serial device module.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtSerCreateAllPipes
    (
    pUSBTGT_SER_DEV pUsbTgtSerDev,
    UINT8           uConfig
    )
    {
    pUSB_ENDPOINT_DESCR pEpDesc = NULL;
    USBTGT_PIPE_CONFIG_PARAM ConfigParam;
    UINT8               uIfBase;
    STATUS              status = ERROR;

    if (NULL == pUsbTgtSerDev)
        {
        USBTGT_SER_ERR("Invalid parameter %s is NULL\n",
                       "pUsbTgtSerDev", 2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * This function driver may be part of one compiste device
     * Get the interface base.
     */

    if ( ERROR == usbTgtFuncInfoMemberGet(pUsbTgtSerDev->targChannel,
                                     USBTGT_FUNC_INFO_MEM_TYPE_uIfNumMin,
                                     &uIfBase))
        {
        USBTGT_SER_ERR("Can not get the interface base number\n",
                       1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Find the right endpoint descriptor */

    pEpDesc = usbTgtFuncEndpointDescFind (pUsbTgtSerDev->targChannel,
                          uIfBase,            /* Comm interface */
                          0,                  /* Altsetting 0 */
                          USB_ATTR_INTERRUPT, /* Interrupt */
                          USB_ENDPOINT_IN,    /* IN */
                          0,                  /* Synch Type */
                          0,                  /* Usage */
                          1);                 /* The first one */

    /* Create the pipe for the endpoint */

    if (usbTgtCreatePipe (pUsbTgtSerDev->targChannel,
                          pEpDesc,
                          uConfig,
                          uIfBase,
                          0,
                          &pUsbTgtSerDev->intInPipe) != OK)
        {
        USBTGT_SER_ERR("Create for interrupt pipe failed.\n",
                        1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Set the configuration parameters */

    ConfigParam.uMaxErpCount = 1;
    ConfigParam.uMaxErpBufCount = 1;

    /* The notification buffer size is 8 */

    ConfigParam.uMaxErpBufSize = 8;

    /* Configure the pipe */

    if (ERROR ==  usbTgtConfigPipe(pUsbTgtSerDev->intInPipe,
                                   &ConfigParam))
        {
        USBTGT_SER_ERR("Configure for interrupt pipe failed.\n",
                       1, 2, 3, 4, 5, 6);

        usbTgtSerDestroyAllPipes(pUsbTgtSerDev);

        return ERROR;
        }

    /* Find the right endpoint descriptor */

    pEpDesc = usbTgtFuncEndpointDescFind (pUsbTgtSerDev->targChannel,
                          (UINT8)(uIfBase + 1), /* Data interface */
                          0,                    /* Altsetting 0 */
                          USB_ATTR_BULK,        /* BULK */
                          USB_ENDPOINT_IN,      /* IN */
                          0,                    /* Synch Type */
                          0,                    /* Usage */
                          1);                   /* The first one */

    /* Create the pipe for the endpoint */

    if (usbTgtCreatePipe (pUsbTgtSerDev->targChannel,
                          pEpDesc,
                          uConfig,
                          uIfBase,
                          0,
                          &pUsbTgtSerDev->bulkInPipe) != OK)
        {
        USBTGT_SER_ERR("Create for interrupt pipe failed.\n",
                        1, 2, 3, 4, 5, 6);

        usbTgtSerDestroyAllPipes(pUsbTgtSerDev);

        return ERROR;
        }

    /* Set the configuration parameters */

    ConfigParam.uMaxErpCount = 1;
    ConfigParam.uMaxErpBufCount = 1;

    /* The max buffer size is 2K */

    ConfigParam.uMaxErpBufSize = USBTGT_SER_MAX_ERP_BUFF_SIZE;

    /* Configure the pipe */

    if (ERROR ==  usbTgtConfigPipe(pUsbTgtSerDev->bulkInPipe,
                                   &ConfigParam))
        {
        USBTGT_SER_ERR("Configure for interrupt pipe failed.\n",
                       1, 2, 3, 4, 5, 6);

        usbTgtSerDestroyAllPipes(pUsbTgtSerDev);

        return ERROR;
        }

    /* Find the right endpoint descriptor */

    pEpDesc = usbTgtFuncEndpointDescFind (pUsbTgtSerDev->targChannel,
                          (UINT8)(uIfBase + 1), /* Data interface */
                          0,                    /* Altsetting 0 */
                          USB_ATTR_BULK,        /* BULK */
                          USB_ENDPOINT_OUT,     /* OUT */
                          0,                    /* Synch Type */
                          0,                    /* Usage */
                          1);                   /* The first one */

    /* Create the pipe for the endpoint */

    if (usbTgtCreatePipe (pUsbTgtSerDev->targChannel,
                          pEpDesc,
                          uConfig,
                          uIfBase,
                          0,
                          &pUsbTgtSerDev->bulkOutPipe) != OK)
        {
        USBTGT_SER_ERR("Create for interrupt pipe failed.\n",
                        1, 2, 3, 4, 5, 6);

        usbTgtSerDestroyAllPipes(pUsbTgtSerDev);

        return ERROR;
        }

    /* Set the configuration parameters */

    ConfigParam.uMaxErpCount = 1;
    ConfigParam.uMaxErpBufCount = 1;

    /* The max buffer size is 2K */

    ConfigParam.uMaxErpBufSize = USBTGT_SER_MAX_ERP_BUFF_SIZE;

    /* Configure the pipe */

    if (ERROR ==  usbTgtConfigPipe(pUsbTgtSerDev->bulkOutPipe,
                                   &ConfigParam))
        {
        USBTGT_SER_ERR("Configure for interrupt pipe failed.\n",
                       1, 2, 3, 4, 5, 6);

        usbTgtSerDestroyAllPipes(pUsbTgtSerDev);

        return ERROR;
        }

    USBTGT_SER_DBG("Create pipes with interrupt %p, bulk in %p, bulk out %p\n",
                    pUsbTgtSerDev->intInPipe, pUsbTgtSerDev->bulkInPipe,
                    pUsbTgtSerDev->bulkOutPipe, 4, 5, 6);
    return OK;
    }

/*******************************************************************************
*
* usbTgtSerBulkInErpCallback - called on data sent on bulk-in pipe
*
* This routine is called on data sent on bulk-in pipe.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbTgtSerBulkInErpCallback
    (
    pVOID               erp        /* USB_ERP endpoint request packet */
    )
    {
    USB_ERP *           pErp = (USB_ERP *)erp;      /* USB_ERP */
    pUSBTGT_SER_DEV     pUsbTgtSerDev;

    if (erp == NULL)
        {
        USBTGT_SER_ERR("erp is NULL\n", 1, 2, 3, 4, 5, 6);

        return;
        }

    pUsbTgtSerDev = pErp->userPtr;

    if (pUsbTgtSerDev == NULL || pUsbTgtSerDev->pChan == NULL)
        {
        USBTGT_SER_ERR("pUsbTgtSerDev or pUsbTgtSerDev->pChan is NULL",
                       1, 2, 3, 4, 5, 6);

        return;
        }

    pUsbTgtSerDev->bulkInInUse = FALSE;

    /* verify that the data is good */

    if (pErp->result != OK
        && pErp->result != S_usbTgtTcdLib_ERP_SUCCESS
        && pErp->result != S_usbTgtTcdLib_ERP_RESTART)
        {
        USBTGT_SER_ERR("pErp->result %p, exiting...\n", pErp->result,
                       2, 3, 4, 5, 6);
        return;
        }

    /* if data length is equal to max buffer size, transfer a zero size packet */

    if (pErp->bfrList[0].actLen == USBTGT_SER_MAX_BUFF_LEN)
        {
        if (usbTgtSerBulkInTransfer(pUsbTgtSerDev,
                                    (char *)pUsbTgtSerDev->bulkInData,
                                    (UINT32)0) != OK)

            USBTGT_SER_ERR("Submit 0 size packet error!\n", 1, 2, 3, 4, 5, 6);
        }
    }

/*******************************************************************************
*
* usbTgtSerBulkOutErpCallback - process data received on bulk-out pipe
*
* This routine processes the data which is received on the bulk out pipe.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbTgtSerBulkOutErpCallback
    (
    pVOID               erp        /* USB_ERP endpoint request packet */
    )
    {
    USB_ERP *           pErp = (USB_ERP *)erp;      /* USB_ERP */
    UINT8 *             pData;
    UINT32              dataLen;
    pUSBTGT_SER_DEV     pUsbTgtSerDev;

    if (erp == NULL)
        {
        USBTGT_SER_ERR("erp is NULL\n", 1, 2, 3, 4, 5, 6);

        return;
        }

    pUsbTgtSerDev = pErp->userPtr;

    /*
     * NOTE: pUsbTgtSerDev == NULL may not be a failure,
     * it may be used by usbTgtSerPollInput
     */

    if (pUsbTgtSerDev == NULL || pUsbTgtSerDev->pChan == NULL)
        {
        USBTGT_SER_ERR("pUsbTgtSerDev or pUsbTgtSerDev->pChan is NULL",
                       1, 2, 3, 4, 5, 6);

        return;
        }

    pUsbTgtSerDev->bulkOutInUse = FALSE;

    /* verify that the data is good */

    if (pErp->result != OK
        && pErp->result != S_usbTgtTcdLib_ERP_SUCCESS
        && pErp->result != S_usbTgtTcdLib_ERP_RESTART)
        {
        USBTGT_SER_ERR("pErp->result %p, exiting...\n", pErp->result,
                       2, 3, 4, 5, 6);

        return;
        }

    dataLen = pErp->bfrList[0].actLen;
    pData = (UINT8 *)(pErp->bfrList[0].pBfr);

    pUsbTgtSerDev->pChan->actRxLen = dataLen;

    /* Report the received data to upper level */

    usbTgtSerSioIntRcv(pUsbTgtSerDev->pChan);

    /* Continue to receive data */

    if (usbTgtSerBulkOutErpInit(pUsbTgtSerDev, pUsbTgtSerDev->bulkOutData,
                            USBTGT_SER_MAX_BUFF_LEN, usbTgtSerBulkOutErpCallback,
                            pUsbTgtSerDev) != OK)
        {
        USBTGT_SER_ERR("Re-call usbTgtSerBulkOutErpInit fail\n",
                       1, 2, 3, 4, 5, 6);
        }

    return;
    }

/******************************************************************************
*
* usbTgtSerSetLineCodingCallback - USB_CDC_REQ_SET_LINE_CODING data callback
*
* This routine is invoked when USB_CDC_REQ_SET_LINE_CODING class specific
* request is received from the host. It acts as a dummy.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbTgtSerSetLineCodingCallback
    (
    pVOID    pErp            /* Pointer to ERP structure */
    )
    {
    USBTGT_SER_VDBG("usbTgtSerSetLineCodingCallback(): "
        "dwDTERate %d bCharFormat %p bParityType %p bDataBits %d\n",
        ((USB_CDC_LINE_CODING *)pErp)->dwDTERate,
        ((USB_CDC_LINE_CODING *)pErp)->bCharFormat,
        ((USB_CDC_LINE_CODING *)pErp)->bParityType,
        ((USB_CDC_LINE_CODING *)pErp)->bDataBits, 5, 6);

    return;
    }

/******************************************************************************
*
* usbTgtSerPortStateCheck - Check serial port state
*
* This routine return usb port state for serial to read and write
*
* RETURNS: TRUE when port state is ready for write or flush data
*          FALSE when port state is not ready or dummy buffer is not enable
*
* ERRNO: N/A
*/

BOOL usbTgtSerPortStateCheck
    (
    USBTGT_ACM_SIO_CHAN * pChan
    )
    {
    if (usbTgtSerDummyBufferEnable == TRUE)
        {
        if (pChan->portState != USBTGT_SER_PORT_OPEN
            && usbTgtSerSysConNum == pChan->channelNo)
            return TRUE;
        }

    return FALSE;
    }

/******************************************************************************
*
* usbTgtSerInit - initialize usb target serial emulator driver
*
* This routine initializes usb target serial emulator driver, register callback
* function table to target management layer, bind target serial emulator to
* dummy serial device.
*
* RETURNS: OK or ERROR if fails to perform the action.
*
* ERRNO: N/A
*/

STATUS usbTgtSerInit
    (
    int devNum
    )
    {
    pUSBTGT_SER_DEV     pUsbTgtSerDev;
    USBTGT_FUNC_INFO    usbTgtFuncInfo;
    int                 index;

    for (index = 0; index < devNum; index++)
        {
        pUsbTgtSerDev = OSS_CALLOC(sizeof(USBTGT_SER_DEV));

        if (pUsbTgtSerDev == NULL)
            {
            USBTGT_SER_ERR("memory leak for pUsbTgtSerDev\n", 1, 2, 3, 4, 5, 6);

            goto EXIT;
            }

        pUsbTgtSerDev->bulkInData = OSS_CALLOC(USBTGT_SER_MAX_BUFF_LEN);

        if (pUsbTgtSerDev->bulkInData == NULL)
            {
            USBTGT_SER_ERR("memory leak for bulkInData\n", 1, 2, 3, 4, 5, 6);

            OSS_FREE(pUsbTgtSerDev);

            goto EXIT;
            }

        pUsbTgtSerDev->bulkOutData= OSS_CALLOC(USBTGT_SER_MAX_BUFF_LEN);

        if (pUsbTgtSerDev->bulkOutData == NULL)
            {
            USBTGT_SER_ERR("memory leak for bulkOutData\n", 1, 2, 3, 4, 5, 6);

            OSS_FREE(pUsbTgtSerDev->bulkInData);

            OSS_FREE(pUsbTgtSerDev);

            goto EXIT;
            }

        pUsbTgtSerDev->pChan = pUsbTgtSerSioChan[index];

        if (pUsbTgtSerDev->pChan == NULL)
            {
            USBTGT_SER_ERR("serial channel not initialized\n", 1, 2, 3, 4, 5, 6);

            OSS_FREE(pUsbTgtSerDev->bulkOutData);

            OSS_FREE(pUsbTgtSerDev->bulkInData);

            OSS_FREE(pUsbTgtSerDev);

            goto EXIT;
            }

        pUsbTgtSerSioChan[index]->pUsbTgtSerDev = pUsbTgtSerDev;

        pUsbTgtSerDev->uConfiguration= 0;
        pUsbTgtSerDev->uControlIfAltSetting = 0;
        pUsbTgtSerDev->uDataIfAltSetting = 0;

        usbTgtFuncInfo.ppFsFuncDescTable = (USB_DESCR_HDR **)gSerFuncDescHdr;
        usbTgtFuncInfo.ppHsFuncDescTable = (USB_DESCR_HDR **)gSerHsFuncDescHdr;
        usbTgtFuncInfo.ppSsFuncDescTable = (USB_DESCR_HDR **)gSerSsFuncDescHdr;
        usbTgtFuncInfo.pFuncCallbackTable = &usbTgtSerCallbackTable;
        usbTgtFuncInfo.pCallbackParam = pUsbTgtSerDev;
        usbTgtFuncInfo.pFuncSpecific = (void *)pUsbTgtSerDev;

        usbTgtFuncInfo.pFuncName = usbTgtSerFuncName ;
        usbTgtFuncInfo.pTcdName = usbTgtSerTargetName;
        usbTgtFuncInfo.uFuncUnit = (UINT8) (index & 0xFF);
        usbTgtFuncInfo.uTcdUnit = usbTgtSerTargetUnit;
        usbTgtFuncInfo.uConfigToBind = usbTgtSerConfigNum;
        usbTgtFuncInfo.pWcidString = NULL;		
        usbTgtFuncInfo.pSubWcidString = NULL;		
        usbTgtFuncInfo.uWcidVc= NULL;		

        /* Register to the TML */

        pUsbTgtSerDev->targChannel = usbTgtFuncRegister(&usbTgtFuncInfo);

        if (USBTGT_TARG_CHANNEL_DEAD == pUsbTgtSerDev->targChannel)
            {
            USBTGT_MSC_ERR("Register to the TML fail\n",
                             1, 2, 3, 4, 5, 6);

            OSS_FREE(pUsbTgtSerDev->bulkOutData);

            OSS_FREE(pUsbTgtSerDev->bulkInData);

            OSS_FREE(pUsbTgtSerDev);

            pUsbTgtSerSioChan[index]->pUsbTgtSerDev = NULL;

            goto EXIT;
            }
        }

    return OK;

EXIT:
    for (index = 0; index < devNum; index++)
        {
        if (pUsbTgtSerSioChan[index] != NULL)
            {
            pUsbTgtSerDev = pUsbTgtSerSioChan[index]->pUsbTgtSerDev;

            if (pUsbTgtSerDev != NULL)
                {
                (void)usbTgtFuncUnRegister(pUsbTgtSerDev->targChannel);

                OSS_FREE(pUsbTgtSerDev->bulkOutData);

                OSS_FREE(pUsbTgtSerDev->bulkInData);

                OSS_FREE(pUsbTgtSerDev);
                }

            pUsbTgtSerSioChan[index] = NULL;
            }
        }

    return ERROR;
    }

/******************************************************************************
*
* usbTgtSerUnInit - uninitialize usb target serial emulator driver
*
* This routine uninitialize usb target serial emulator driver, remove callback
* function table from target management layer, unbind target serial emulator
* with dummy serial device, remove and delete tty device which is create by
* tty-lib.
*
* This routine does not really remove serial device because it is unsupport in
* vxWorks, but it can remove tty device which is displayed in "devs" command.
*
* RETURNS: OK or ERROR if fails to perform the action.
*
* ERRNO: N/A
*/

STATUS usbTgtSerUnInit
    (
    int devNum
    )
    {
    pUSBTGT_SER_DEV     pUsbTgtSerDev;
    int                 index;

    for (index = 0; index < devNum; index++)
        {
        pUsbTgtSerDev = pUsbTgtSerSioChan[index]->pUsbTgtSerDev;

        if (pUsbTgtSerDev == NULL)
            return ERROR;

        (void)usbTgtFuncUnRegister(pUsbTgtSerDev->targChannel);

        pUsbTgtSerDev->pChan->portState = USBTGT_SER_PORT_CLOSE;

        /* unlink pointer */

        pUsbTgtSerDev->pChan->pUsbTgtSerDev = NULL;

        pUsbTgtSerDev->pChan = NULL;


        /*
         * NOTE:
         *   When pUsbTgtSerDev->bulk**DataInUse is TRUE in this section,
         * It must be FALSE very soon, and can not change to be TRUE again.
         * Because that pUsbTgtSerDev has been unlinked from pChan, and
         * function driver has been removed, if there is still a unfinished
         * transfer, it must be failure and return.
         */

        while(pUsbTgtSerDev->bulkOutDataInUse == TRUE)
            ;

        if (pUsbTgtSerDev->bulkOutData != NULL)
            OSS_FREE(pUsbTgtSerDev->bulkOutData);

        while (pUsbTgtSerDev->bulkInDataInUse == TRUE)
            ;

        if (pUsbTgtSerDev->bulkInData != NULL)
            OSS_FREE(pUsbTgtSerDev->bulkInData);

        memset(pUsbTgtSerDev, 0, sizeof(USBTGT_SER_DEV));

        OSS_FREE(pUsbTgtSerDev);

        /* delete tty devices */

        usbTgtSerTtyRemove(index);
        }

    return OK;
    }

/******************************************************************************
*
* usbTgtSerShowDetail - show detail information of USB target serial device
*
* This routine show specific detail information of USB target serial device.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbTgtSerShowDetail
    (
    int index
    )
    {
    pUSBTGT_SER_DEV pUsbTgtSerDev;
    printf("Target Serial Emulator Index [%d]\n", index + 1);
    if (pUsbTgtSerSioChan[index] == NULL)
        {
        printf("Data struct not initialized\n");
        return;
        }
    printf("Channel number     : [%d]\n", pUsbTgtSerSioChan[index]->channelNo);
    printf("Port State         : [%d]\n", pUsbTgtSerSioChan[index]->portState);
    printf("Dummy Buffer Size  : [%ld]\n",
           pUsbTgtSerSioChan[index]->dummyBfrSize);
    printf("Options            : [0x%08X]\n",
           pUsbTgtSerSioChan[index]->options);
    printf("Baud rate          : [%d]\n", pUsbTgtSerSioChan[index]->baudFreq);
    printf("Mode               : [%s]\n",
           (pUsbTgtSerSioChan[index]->mode == SIO_MODE_INT) ? "INT" : "POOL");
    printf("Tranfer Task Id    : [%p]\n", pUsbTgtSerSioChan[index]->txThread);
    printf("pUsbTgtSerDev      : [%p]\n",
           pUsbTgtSerSioChan[index]->pUsbTgtSerDev);

    printf("ttyNum             : [%d]\n", pUsbTgtSerSioChan[index]->ttyNum);
    printf("onClosing          : [%d]\n", pUsbTgtSerSioChan[index]->onClosing);

    pUsbTgtSerDev = pUsbTgtSerSioChan[index]->pUsbTgtSerDev;
    if (pUsbTgtSerDev == NULL)
        {
        printf("Not Bind to Target Device\n");
        return;
        }

    printf("targChannel        : [0x%08X]\n", pUsbTgtSerDev->targChannel);
    printf("Configuration      : [0x%02X]\n", pUsbTgtSerDev->uConfiguration);
    printf("ControlIfAltSetting: [0x%02X]\n",
           pUsbTgtSerDev->uControlIfAltSetting);
    printf("DataIfAltSetting   : [0x%02X]\n",
           pUsbTgtSerDev->uDataIfAltSetting);

    printf("bulkOutPipe        : [%p]\n", pUsbTgtSerDev->bulkOutPipe);
    printf("bulkOutInUse       : [%s]\n",
           SER_SHOW_BOOL(pUsbTgtSerDev->bulkOutInUse));
    printf("bulkOutBfrValid    : [%s]\n",
           SER_SHOW_BOOL(pUsbTgtSerDev->bulkOutBfrValid));
    printf("bulkOutStallStatus : [%s]\n",
           SER_SHOW_BOOL(pUsbTgtSerDev->bulkOutStallStatus));
    printf("bulkInPipe         : [%p]\n", pUsbTgtSerDev->bulkInPipe);
    printf("bulkInInUse        : [%s]\n",
           SER_SHOW_BOOL(pUsbTgtSerDev->bulkInInUse));
    printf("bulkInBfrValid     : [%s]\n",
           SER_SHOW_BOOL(pUsbTgtSerDev->bulkInBfrValid));
    printf("bulkInStallStatus  : [%s]\n",
           SER_SHOW_BOOL(pUsbTgtSerDev->bulkInStallStatus));
    printf("intInPipe          : [%p]\n", pUsbTgtSerDev->intInPipe);
    printf("intInInUse         : [%s]\n",
           SER_SHOW_BOOL(pUsbTgtSerDev->intInInUse));
    printf("pChan              : [%p]\n\n", pUsbTgtSerDev->pChan);

    return;
    }

/******************************************************************************
*
* usbTgtSerShow - show detail information of all USB target serial devices
*
* This routine show detail information of all USB target serial device.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbTgtSerShow
    (
    int index
    )
    {
    if (index == 0)
        {
        for (index = 0; index < usbTgtSerMaxCount; index++)
            {
            usbTgtSerShowDetail(index);
            }
        }
    else
        usbTgtSerShowDetail(index - 1);
    }

