/* usbTgtMscUtil.h - Definitions of USB Target MSC Utility Module */

/*
 * Copyright (c) 2010, 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01f,25mar11,m_y  add two macros to release and reserve the transfer buffer
01e,23mar11,m_y  add usbTgtMscDevFind and usbTgtMscLunGetByName
01d,17mar11,m_y  modify code based on the code review and roll back the 01c 
                 change
01c,03mar11,m_y  add usbTgtMscCBWValid 
01b,28dec10,x_f  add more functions for util interface
01a,26sep10,m_y  written
*/

#ifndef __INCusbTgtMscUtilh
#define __INCusbTgtMscUtilh

#ifdef    __cplusplus
extern "C" {
#endif

/* common interface */

pUSB_TGT_MSC_LUN usbTgtMscLunGetByIndex
    (
    pUSB_TGT_MSC_DEV pUsbTgtMscDev,
    UINT32           lunIndex
    );
pUSB_TGT_MSC_LUN usbTgtMscLunGetByName
    (
    pUSB_TGT_MSC_DEV pUsbTgtMscDev,
    char *           devName
    );
pUSB_TGT_MSC_DEV usbTgtMscDevFind
    (
    char * tcdName,
    int    tcdUnit
    );
UINT8* usbTgtMscEmptyBufGet
    (
    pUSB_TGT_MSC_DEV pUsbTgtMscDev
    );

/* ERP related interface */
	
STATUS usbTgtMscBulkErpInit
    (
    pUSB_TGT_MSC_DEV pUsbTgtMscDev,  /* pointer to erp */
    UINT8 *          pData,          /* pointer to data */
    UINT32           size,           /* size of data */
    UINT32           pid,            /* pid of the data */
    ERP_CALLBACK     erpCallback     /* erp callback */
    );

/* Other util interface */

BOOLEAN usbTgtMscIsPartition
    (
    char * deviceName
    );

device_t usbTgtMscXBDPartHandleGet
    (
    char * deviceName
    );

void usbTgtMscDevBioDone
    (
    struct bio * pBio
    );

#define USBTGT_MSC_BUF_RELEASE(pUsbTgtMscDev)            \
        pUsbTgtMscDev->uBufState = USB_TGT_MSC_BUF_EMPTY;

#define USBTGT_MSC_BUF_RESERVE(pUsbTgtMscDev)            \
        pUsbTgtMscDev->uBufState = USB_TGT_MSC_BUF_BUSY;

#ifdef __cplusplus
}
#endif

#endif /* __INCusbTgtMscUtilh */

