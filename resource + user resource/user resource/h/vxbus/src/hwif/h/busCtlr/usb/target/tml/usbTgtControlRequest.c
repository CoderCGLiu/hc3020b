/* usbTgtControlRequest.c - The Control Request Process Module */

/*
 * Copyright (c) 2010-2015 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01y,24jun15,j_x  cleanup static analysis error (VXW6-84620)
01x,15may15,hma  fix the wrong call usbDescrCopy (VXW6-84424)
01w,05jan15,lan  Add WCID support (VXW6-83565)
01v,24sep14,lan  fix the Out-of-bounds read (VXW6-83516)
01u,10aug14,lan  check the buffer USBTGT_TCD::dataBfr overflow (VXW6-83141)
01t,02Sep14,bbz  clear the warnings of no check of the return value (VXW6-83095)
01s,01aug13,wyy  Correct misspelled uFuncStatus in struct usbtgt_func_driver
                 (WIND00428358)
01r,06may13,s_z  Remove compiler warning (WIND00356717)
01q,04jan13,s_z  Remove compiler warning (WIND00390357)
01p,25oct12,s_z  MHDRC TCD pass CV testing (WIND00385197)
01o,23oct12,s_z  Remove un-used logs (WIND00382685)
01n,17oct12,s_z  Fix short packet receive issue (WIND00374594)
01m,18sep12,s_z  Pass CV2.0 testing (WIND00375402)
01l,18may12,s_z  Add support for USB 3.0 target (WIND00326012)
01k,28jul11,s_z  Update usbTgtDescriptorGet routine to get descriptors based
                 on recipient.
01j,21apr11,s_z  Integrate control request callback process routine
01i,21apr11,s_z  Correct the misuse of ERP cancel status
01h,30mar11,s_z  Add the multiple language support
01g,28mar11,s_z  Correct some debug message description
01f,22mar11,s_z  Code clean up based on the code review
01e,08mar11,s_z  Code clean up
01d,08mar11,w_x  Report set non-default configuraiton event to OTG manager
01c,23feb11,s_z  Add IAD descriptor support
01b,14feb11,s_z  integrate normal device and OTG device fix for CV test 
01a,30nov10,s_z  created
*/

/*
DESCRIPTION

 The file is the control request process module which include the standard
 requests, class requests and vendor requests process interfaces. The standard
 requests will be processed in this module, and the class requests and vendor
 requests will be routed to and processed by its function driver.

 In chapter 9.4 of USB 2.0 Specification says:
 "USB devices must respond to standard device requests, even if the device has
 not yet been assigned an address or has not been configured."

 All USB devices must respond to a variety of requests called standard
 requests. These requests are used for configuring a device, controlling the
 state of its interfaces, the status of its endpoints, the OTG status(if the
 device is OTG capable), and with other miscellaneous features. All the 
 requests are issued by the host using the control transfer(Endpoint zero).
 Before the device configured, the device use the default address of zero to 
 responce the host standard request until on unique address be set for the 
 device during the configuration.

 Additionally, devices may also support class-specific requests. And such kinds
 of requests will be processed by the corresponding function driver. Similarly, 
 a device may support vendor specific requests that pertain to a given vendor's 
 implementation.

 Control transfers, which be used to transmit device requests, consist 
 minimally of a setup stage and a status stage, but may also include a data 
 stage, depending on the type of request being performed. 

 All the standard requests listed in chapter 9.4 list in the USB 2.0 
 specification included in this module.

 This file created based on the GEN1 target stack, usbTargDefaultPipe.c

CONTROL REQUESTS

 Followed lists all the control request which can be handled by this module.

\h 1.STANDARD REQUEST

 Following are the standard request in USB 2.0 specification. Some requests in
 USB 3.0 not list in this module.

\is
\i 1.1 USB_REQ_CLEAR_FEATURE
Clear feature request
\i 1.2 USB_REQ_SET_FEATURE
Set feature request
\i 1.3 USB_REQ_GET_CONFIGURATION
Get configuration request
\i 1.4 USB_REQ_SET_CONFIGURATION
Set configuration request
\i 1.5 USB_REQ_GET_DESCRIPTOR
Get descriptor request
\i 1.6 USB_REQ_SET_DESCRIPTOR
Set descriptor request
\i 1.7 USB_REQ_SET_INTERFACE
Set interface request
\i 1.8 USB_REQ_GET_INTERFACE
Get interface request
\i 1.9 USB_REQ_GET_STATUS
Get status request
\i 1.10 USB_REQ_SET_ADDRESS
Set address request
\i 1.11 USB_REQ_GET_SYNCH_FRAME
Get synch frame number request
\ie
 
\h 2.VENDOR/CLASS REQUEST
 
  This module do not process the vendor/Class requests. It will parse this request
  to the function drivers to process it.
  
 
CONTROL PIPE
 
 The control pipe is used for the control endpoint to process the control 
 requests. It will be created/re-created when the device be RESET by the 
 BUS. The USB target stack can create the pipe according to the hardware's
 capability(such as the USB target controller's speed).
 
CONTROL ERP
 
 The control ERP is used for the control pipe to carry the information for 
 the control endpoint on the SETUP/DATA/STATUS stage.
 
\h 1.Control ERP Initialization
 
 Before using the control pipe, the control ERP need be initialized correctly.
 There are several APIs can be used. 
 
\ml
\m 1.1 usbTgtInitSetupErp

Initialize the control ERP for the SETUP stage.

\m 1.2 usbTgtInitDataErp

Initialize the control ERP for the DATA IN/OUT stage.

\m 1.3 usbTgtInitStatusErp

Initialize the control ERP for the STATUS IN/OUT stage.

\m 1.4 usbTgtControlResponseSend

Initialize the control ERP for the DATA IN stage, which is an interface to 
the function drivers.

\m 1.5 usbTgtControlPayloadRcv

Initialize the control ERP for the DATA OUT stage, which is an interface to 
the function drivers.

\m 1.6 usbTgtControlStatusSend

Initialize the control ERP for the DATA OUT stage, which is an interface to 
the function drivers.

\me
 
\h 2.Control ERP Callback
 
 NOTE: The ERP will be called the targCallback by the TCD, and in the 
 targCallback routine, if 'userCallback' is valid, the user's userCallback will
 be called too to inform the user the ERP has completed.
 
 For different kind of TCD hardware specific, the ERP callback with different
 return result.
 
 
\ml
\m 2.1 S_usbTgtTcdLib_ERP_SUCCESS
 
 Indicate the ERP complete one stage successfully, and next stage need to be 
 processed. the developer need process next stage according to the SETUP - 
 DATA (If needed) - STATUS by schedule. This case used in many normal case, such
 as the TCD need some special command to specific the status stage. Meanwhile, 
 the result will be used when the DATA stage is needed.
 
\m 2.2 S_usbTgtTcdLib_ERP_RESTART
 
 Indicate the ERP complete with _no_ error, and next SETUP stage need to be 
 processed. The developer do _not_ need process next stage according to the SETUP - 
 DATA (If needed) - STATUS by schedule. This case used in some TCD. Especially
 for some TCD can send the STATUS with the DATA IN/OUT. In this case, the STATUS
 stage will no needed, and next SETUP stage should be specific.

 This result can also used to termite one round of transfer. Sometimes the 
 device may receive the termite signal from the host to stop the following
 data transaction.
 
\m 2.3 S_usbTgtTcdLib_ERP_CANCELED
 
 Indicate the ERP complete by cancelled error. It normally accord when the 
 control pipe was deleted or the ERP be cancelled by the up level. Process will
 be terminated if the canceled result received.

\m 2.4 Others

 Indicate the ERP complete by other error.

\me

INCLUDE FILES:  vxWorks.h usb/usb.h usb/usbDescrCopyLib.h usb/usbTgt.h 
                usb/usbOtg.h usbTgtControlRequest.h
*/

/* includes */

#include <vxWorks.h>
#include <usb/usb.h>
#include <usb/usbDescrCopyLib.h>
#include <usb/usbTgt.h>
#include <usb/usbOtg.h>
#include "usbTgtUtil.h"
#include "usbTgtControlRequest.h"

/* externs */

IMPORT BOOL usrUsbTgtIadDescEnableGet
    (
    void
    );

IMPORT STATUS usrUsbTgtStringDescGet
    (
    UINT16   uLangId,
    UINT8    uStringIndex,
    UINT16   uSrtLen,
    UINT8 *  pDescBuf,
    UINT16 * pActLen
    );

/* forward declaration */

LOCAL STATUS usbTgtControlRequestHandler
    (
    pUSBTGT_TCD pTcd
    );

LOCAL VOID usbTgtDataOUTErpCallback
    (
    pVOID pErpCallback   
    );

LOCAL VOID usbTgtDataINErpCallback
    (
    pVOID pErpCallback   
    );

LOCAL VOID usbTgtStatusErpCallback
    (
    pVOID pErpCallback   
    );

LOCAL VOID usbTgtSetupErpCallback
    (
    pVOID pErpCallback  
    );

/*******************************************************************************
*
* usbTgtControlPipeStall -  stall the control endpoint
*
* This routine is a used to stall the control endpoint. It will be called when
* something wrong on the control pipe.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO:N/A
*
* \NOMANUAL
*/

STATUS usbTgtControlPipeStall
    (
    pUSBTGT_TCD pTcd /* The TCD pointer */
    )
    {

    if ((NULL != pTcd) &&
        (NULL != pTcd->pTcdFuncs) &&
        (NULL != pTcd->pTcdFuncs->pTcdPipeStatusSet))
        {        
        return (pTcd->pTcdFuncs->pTcdPipeStatusSet)
                (pTcd,
                 pTcd->controlPipe,
                 USBTGT_PIPE_STATUS_STALL);
        }

    return ERROR;    
    }

/*******************************************************************************
*
* usbTgtSubmitControlErp - submits the control ERP
*
* This routine is used to submit the Endpoint Request Packet(ERP) over the
* default control pipes. If an error occurs, the control pipes are stalled.
*
* RETURNS: OK or ERROR if not able to submit the ERP
*
* ERRNO:N/A
*
*/

STATUS usbTgtSubmitControlErp
    (
    pUSBTGT_TCD pTcd    /* The TCD pointer */
    )
    {
    STATUS   status = ERROR;
    
    if ((NULL != pTcd) &&
        (NULL != pTcd->pTcdFuncs) &&
        (NULL != pTcd->pTcdFuncs->pTcdSubmitErp))
        {
        status = (pTcd->pTcdFuncs->pTcdSubmitErp)
                  (pTcd, pTcd->controlPipe, &pTcd->controlErp);
        
        if( status != OK)
            {
            (void)usbTgtControlPipeStall (pTcd);
            }
        }   
    
    return status;
    }


/*******************************************************************************
*
* usbTgtInitSetupErp - initialize setup ERP
*
* This routine is used to initialize the Setup ERP for the setup stage transfer
* one the control pipe of <pTcd>.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
*/

VOID usbTgtInitSetupErp
    (
    pUSBTGT_TCD pTcd /* The TCD pointer */
    )
    {
    /* Validate parameters */

    if (NULL == pTcd)
        {
        USB_TGT_ERR("Invalid parameter pTcd is NULL\n",
                    1, 2, 3, 4, 5, 6); 

        return;
        }
    
    USB_TGT_DBG("usbTgtInitSetupErp initialize the control ERP\n",
                1, 2, 3, 4, 5, 6);
    
    /* Initialize the setup buffer */

    OS_MEMSET (&pTcd->controlErp, 0 , sizeof(USB_ERP));

    /* Initialize the setup buffer */

    OS_MEMSET(&pTcd->SetupPkt, 0, sizeof(USB_SETUP));

    pTcd->controlErp.targPtr = (pVOID)pTcd;
    pTcd->controlErp.erpLen = sizeof (USB_ERP);
    pTcd->controlErp.targCallback = (ERP_CALLBACK)usbTgtSetupErpCallback;
    pTcd->controlErp.pPipeHandle = (pVOID)pTcd->controlPipe;
    pTcd->controlErp.dataToggle = USB_DATA0;
    pTcd->controlErp.bfrCount = 1;
    pTcd->controlErp.bfrList[0].pid = USB_PID_SETUP;
    pTcd->controlErp.bfrList[0].pBfr = (UINT8 *)&pTcd->SetupPkt;
    pTcd->controlErp.bfrList[0].bfrLen = sizeof (USB_SETUP);

    return;
    }

/*******************************************************************************
*
* usbTgtInitDataErp - initializes data ERP for control transfer 
*
* This routine is used to initialize the date ERP for the data phase on the
* control pipe. <pFuncDriver> is the parameter to indicate which function driver
* is the data to or from, if it is NULL, means the data is to or from the 
* device. <pBuf> is the parameter to indicate the buffer pointer where the data
* to receive or send. <ulength> is the parameter of the data length required or
* to send. <userCallback> is the target application callback parameter, which 
* used by the function driver or the user APIs. <isDataIn> indicates the DATA
* stage direction(eg, DATA IN/OUT). erpFlags> indicates the ERP transaction 
* flags, such as whether one ZLP pecket will be needed after all the data 
* transfered
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbTgtInitDataErp
    (
    pUSBTGT_TCD         pTcd,         /* TCD pointer */
    pVOID               pFuncSpecific,/* Function driver specific pointer */
    pUINT8              pBuf,         /* Buffer for data */
    UINT16              ulength,      /* Data length */
    ERP_CALLBACK        userCallback, /* USB Target Applcaition Callback */
    BOOL                isDataIn,     /* Data In stage */
    USB_ERP_FLAGS       erpFlags      /* The ERP transaction flags */ 
    )
    {
    /* Validate parameters */

    if (NULL == pTcd)
        {
        USB_TGT_ERR("Invalid parameter pTcd is NULL\n",
                    1, 2, 3, 4, 5, 6); 

        return;
        }
    
    USB_TGT_DBG("usbTgtInitDataErp\n",1, 2, 3, 4, 5, 6);

    OS_MEMSET (&pTcd->controlErp, 0 , sizeof (USB_ERP));

    pTcd->controlErp.targPtr = (pVOID)pTcd;

    /* 
     * NOTE: This pointer used for the calss/vendor request
     * for the Function driver. And can be NULL if used
     * for the standard request.
     */
     
    pTcd->controlErp.userPtr = pFuncSpecific; 
    
    pTcd->controlErp.erpLen = sizeof (USB_ERP);
    pTcd->controlErp.targCallback = isDataIn ? 
                      (ERP_CALLBACK)usbTgtDataINErpCallback : 
                      (ERP_CALLBACK)usbTgtDataOUTErpCallback;

    pTcd->controlErp.userCallback = userCallback;
    
    pTcd->controlErp.pPipeHandle = (pVOID)pTcd->controlPipe;
    pTcd->controlErp.dataToggle = USB_DATA1;
    pTcd->controlErp.bfrCount = 1;
    pTcd->controlErp.bfrList[0].pBfr = pBuf;
    pTcd->controlErp.bfrList[0].bfrLen = ulength;
    pTcd->controlErp.bfrList[0].pid = (UINT16) (isDataIn ? USB_PID_IN : USB_PID_OUT);
    pTcd->controlErp.erpFlags = erpFlags;
    
    return;
    }

/*******************************************************************************
*
* usbTgtInitStatusErp - initializes status ERP for control transfer 
*
* This function is used to initialize the status ERP for the status phase on 
* contol endpoint. <isStatusIn> is the parameter to indicate the status phase
* dirction, STATUS IN/OUT.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbTgtInitStatusErp
    (
    pUSBTGT_TCD pTcd,       /* The TCD pointer */
    BOOL        isStatusIn  /* Is it for status IN */
    )
    {
    /* Validate parameters */
    
    if (NULL == pTcd)
        {
        USB_TGT_ERR("Invalid parameter pTcd is NULL\n",
                    1, 2, 3, 4, 5, 6); 

        return;
        }
    
    USB_TGT_DBG("usbTgtInitStatusErp\n",1, 2, 3, 4, 5, 6);

    OS_MEMSET (&pTcd->controlErp, 0 , sizeof (USB_ERP));
    
    pTcd->controlErp.targPtr = (pVOID)pTcd;
    pTcd->controlErp.erpLen = sizeof (USB_ERP);
    pTcd->controlErp.targCallback = (ERP_CALLBACK)usbTgtStatusErpCallback;
    pTcd->controlErp.pPipeHandle =(pVOID) pTcd->controlPipe;
    pTcd->controlErp.dataToggle = USB_DATA1;
    pTcd->controlErp.bfrCount = 1;
    pTcd->controlErp.bfrList[0].pid = (UINT16) (isStatusIn ? USB_PID_IN : USB_PID_OUT);
    pTcd->controlErp.bfrList[0].pBfr = pTcd->dataBfr;
    pTcd->controlErp.bfrList[0].bfrLen = 0;

    return;
    }

/*******************************************************************************
*
* usbTgtSetupErpCallback - callback of the setup Erp
*
* This routine is the callback of the setup Erp. If this called, the hardware
* gets one setup packet and need the targetM leyer to handle.
* 
* The ERP callback will called with the following result.
*
* \is
* \i 1 S_usbTgtTcdLib_ERP_SUCCESS
* 
* Indicate the ERP complete one stage successed, and next stage need to be 
* processed. the developer need process next stage -- DATA (If neede) or STATUS 
* stage by schedule. This case used in many normal case, such
* as the TCD need some special command to specific the status stage. 
* 
*\i 2 S_usbTgtTcdLib_ERP_RESTART
* 
* Indicate the ERP complete with _no_ error, and new SETUP stage need to be 
* processed. The developer do _not_ need process next stage(normally STATUS
* stage). This case used in some TCD. Especially for some TCD can send the 
* STATUS with the DATA IN/OUT.In this case, the STATUS
* stage will no needed, and next SETUP stage should be specific.
* 
*\i 3 S_usbTgtTcdLib_ERP_CANCELED
* 
* Indicate the ERP complete by cancelled error. It normally accored when the 
* control pipe was deleted or the ERP be cancelled by the up level. Process will
* be termate if the canceled result recived.
*
*\i 4 Other Results
*
* Indicate the ERP complete by other error.The control pipe will be stall
*
*\ie
*
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbTgtSetupErpCallback
    (
    pVOID pErpCallback   /* Pointer to ERP structure */
    )
    {
    pUSBTGT_TCD pTcd = NULL;
    STATUS      status = ERROR;
    pUSB_ERP    pErp = (pUSB_ERP)pErpCallback;

    /* Validate parameters */

    if ((NULL == pErp) || 
        (NULL == pErp->targPtr))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pErp) ? "pErp" :
                    "pErp->targPtr"), 2, 3, 4, 5, 6); 
        
        return;
        }
    
    /* Retrive the tcd from erp structure */

    pTcd = (pUSBTGT_TCD)pErp->targPtr;

    USB_TGT_DBG("usbTgtSetupErpCallback called with result 0x%X actLen 0x%X\n",
                pErp->result, 
                pErp->bfrList [0].actLen,
                3, 4, 5, 6);

    /* Acquire Mutex */

    OSS_MUTEX_TAKE (pTcd->tcdMutex , USBTGT_WAIT_TIMEOUT);

    /* Check if there is some control erp pending already */

    if (TRUE == vxAtomicGet(&pTcd->controlErpUsed))
        {
        usbTgtControlErpUsedFlagClear (pTcd);
        
        /* Release Mutex */

        (void)OSS_MUTEX_RELEASE (pTcd->tcdMutex);

        /* The request has been canceled */
        
        if (S_usbTgtTcdLib_ERP_CANCELED == pErp->result)
            {
            USB_TGT_DBG("usbTgtSetupErpCallback called with S_usbTgtTcdLib_ERP_CANCELED\n",
                        1, 2, 3, 4, 5, 6);
            
            return;
            }
        
        switch (pErp->result)
            {
            case S_usbTgtTcdLib_ERP_SUCCESS:
            case OK:
                /* Process the control request */
                
                USB_TGT_DBG("usbTgtSetupErpCallback with normal process flag\n",
                            1, 2, 3, 4, 5, 6);

                status = usbTgtControlRequestHandler(pTcd);
                
                if (OK == status)
                    {                    
                    if (0 == pTcd->SetupPkt.length)
                        {
                        if (TRUE == usbTgtControlErpUsedFlagSet(pTcd))
                            {
                            /* Re-initialize the default control erp */
            
                            usbTgtInitStatusErp (pTcd, TRUE); /* Status IN stage*/
                            
                            /* submit the erp */
            
                            if (ERROR == usbTgtSubmitControlErp (pTcd))
                                usbTgtControlErpUsedFlagClear(pTcd);
                            
                            return;
                            }
                        }
                    else
                        {
                        /* This request has the data stage, do nothing here */
                        
                        USB_TGT_DBG("usbTgtSetupErpCallback normal pTcd->SetupPkt.length %p\n",
                                    pTcd->SetupPkt.length, 2, 3, 4, 5, 6);
                        return;
                        }
                    }  
                else
                    {
                    USB_TGT_DBG("usbTgtSetupErpCallback normal status %p\n",
                                status, 2, 3, 4, 5, 6);
                    
                    goto STALL_AND_RESTART;
                    }
                break;
            case S_usbTgtTcdLib_ERP_RESTART:

                USB_TGT_DBG("usbTgtSetupErpCallback with restart process flag\n",
                            1, 2, 3, 4, 5, 6);
                
                status = usbTgtControlRequestHandler(pTcd);

                if (OK == status)
                    {
                    if (0 == pTcd->SetupPkt.length)
                        {
                        if (TRUE == usbTgtControlErpUsedFlagSet(pTcd))
                            {
                            /* Initialize the setup ERP */
            
                            usbTgtInitSetupErp(pTcd);
            
                            /* Submit the Erp */
            
                            if (ERROR == usbTgtSubmitControlErp (pTcd))
                                usbTgtControlErpUsedFlagClear(pTcd);

                            return;
                            }
                        }
                    else
                        {
                        /* This request has the data stage, do nothing here */
                        
                        USB_TGT_DBG("usbTgtSetupErpCallback restart pTcd->SetupPkt.length %p\n",
                                    pTcd->SetupPkt.length, 2, 3, 4, 5, 6);

                        return;
                        }
                    }          
                else
                    {
                    USB_TGT_DBG("usbTgtSetupErpCallback restart status %p\n",
                                status, 2, 3, 4, 5, 6);
                    
                    goto STALL_AND_RESTART;
                    }
                break;
            default:
                
                USB_TGT_DBG("usbTgtSetupErpCallback default pErp->result %p\n",
                            pErp->result, 2, 3, 4, 5, 6);

                goto STALL_AND_RESTART;
                break;
            }
        }
    else
        {     
        USB_TGT_DBG("usbTgtSetupErpCallback vxAtomicGet(&pTcd->controlErpUsed) false\n",
                    1, 2, 3, 4, 5, 6);
        
        goto STALL_AND_RESTART;
        }

STALL_AND_RESTART:

        USB_TGT_DBG("usbTgtSetupErpCallback stall and restart\n",
                1, 2, 3, 4, 5, 6);

        /* Stall default endpoints */

        (void)usbTgtControlPipeStall (pTcd);
    
        if (TRUE == usbTgtControlErpUsedFlagSet(pTcd))
            {
            /* Initialize the setup ERP */
        
            usbTgtInitSetupErp(pTcd);
        
            /* Submit the Erp */
        
            if(ERROR == usbTgtSubmitControlErp (pTcd))
                usbTgtControlErpUsedFlagClear(pTcd);
            }
  
    return;
    }


/*******************************************************************************
*
* usbTgtDataOUTErpCallback - callback of OUT data transfer
*
* This routine is called on completion of OUT data transfer. If control
* ERP is not pending, it submits a setup packet on the control
* pipe. If control Erp is pending and no error has occured it initializes
* the status ERP and submits it, else it re-submits the setup packet.
*
* The ERP callback will called with the following result.
*
* \is
* \i 1 S_usbTgtTcdLib_ERP_SUCCESS
* 
* Indicate the ERP complete one stage successed, and next stage need to be 
* processed. The developer need process next stage (STATUS 
* stage) by schedule. This case used in many normal case, such
* as the TCD need some special command to specific the status stage. 
* 
*\i 2 S_usbTgtTcdLib_ERP_RESTART
* 
* Indicate the ERP complete with _no_ error, and new SETUP stage need to be 
* processed. The developer do _not_ need process next stage(normally STATUS
* stage). This case used in some TCD. Especially for some TCD can send the 
* STATUS with the DATA IN/OUT. In this case, the STATUS
* stage will no needed, and next SETUP stage should be specific.
* 
*\i 3 S_usbTgtTcdLib_ERP_CANCELED
* 
* Indicate the ERP complete by cancelled error. It normally accored when the 
* control pipe was deleted or the ERP be cancelled by the up level. Process will
* be termate if the canceled result recived.
*
*\i 4 Other Results
*
* Indicate the ERP complete by other error. The control pipe will be stall
*
*\ie
*
* RETURNS: N/A
* 
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbTgtDataOUTErpCallback
    (
    pVOID pErpCallback   /* Pointer to ERP structure */
    )
    {
    pUSBTGT_TCD pTcd = NULL;
    pUSB_ERP    pErp = (pUSB_ERP)pErpCallback;
    
    /* Validate parameters */

    if ((NULL == pErp) || 
        (NULL == pErp->targPtr))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pErp) ? "pErp" :
                    "pErp->targPtr"), 2, 3, 4, 5, 6); 
        
        return;
        }
    
    /* Retrive the tcd from erp structure */

    pTcd = (pUSBTGT_TCD)pErp->targPtr;

    USB_TGT_DBG("usbTgtDataOUTErpCallback called with result %d\n",
                pErp->result, 2, 3, 4, 5, 6);

    /* Acquire Mutex */

    OSS_MUTEX_TAKE (pTcd->tcdMutex , USBTGT_WAIT_TIMEOUT);

    /* Check if there is some control erp pending already */

    if (TRUE == vxAtomicGet(&pTcd->controlErpUsed))
        {
        usbTgtControlErpUsedFlagClear (pTcd);
        
        /* Release Mutex */

        (void)OSS_MUTEX_RELEASE (pTcd->tcdMutex);

        /* Call the user callback */

        if (pErp->userCallback != NULL )
            (*pErp->userCallback) (pErp);

        switch (pErp->result)
            {
            case S_usbTgtTcdLib_ERP_SUCCESS:
            case OK: 

                if (TRUE == usbTgtControlErpUsedFlagSet(pTcd))
                    {
                    /* Re-initialize the default control erp */
    
                    usbTgtInitStatusErp (pTcd, TRUE); /* Data out, status IN stage*/
                    
                    /* submit the erp */
    
                    if (ERROR == usbTgtSubmitControlErp (pTcd))
                        usbTgtControlErpUsedFlagClear(pTcd);
                    }
               
                break;
            case S_usbTgtTcdLib_ERP_RESTART:
                if (TRUE == usbTgtControlErpUsedFlagSet(pTcd))
                    {
                    /* Initialize the setup ERP */
    
                    usbTgtInitSetupErp(pTcd);
    
                    /* Submit the Erp */
    
                    if (ERROR == usbTgtSubmitControlErp (pTcd))
                        usbTgtControlErpUsedFlagClear(pTcd);
                    }
                break;
            case S_usbTgtTcdLib_ERP_CANCELED:
                break;
            default:
                (void)usbTgtControlPipeStall (pTcd);
    
                if (TRUE == usbTgtControlErpUsedFlagSet(pTcd))
                    {
                    /* Initialize the setup ERP */
                
                    usbTgtInitSetupErp(pTcd);
                
                    /* Submit the Erp */
                
                    if (ERROR == usbTgtSubmitControlErp (pTcd))
                        usbTgtControlErpUsedFlagClear(pTcd);
                    }       
                break;
            }
        }
    else
        {
        
        /* Stall default endpoints */

        (void)usbTgtControlPipeStall (pTcd);

        if (TRUE == usbTgtControlErpUsedFlagSet(pTcd))
            {
            /* Initialize the setup ERP */
        
            usbTgtInitSetupErp(pTcd);
        
            /* Submit the Erp */
        
            if (ERROR == usbTgtSubmitControlErp (pTcd))
                usbTgtControlErpUsedFlagClear(pTcd);
            }
        }

    return;
    }

/*******************************************************************************
*
* usbTgtDataINErpCallback - callback of IN data transfer
*
* This routine is called on completion of IN data transfer. If control
* ERP is not pending, it submits a setup packet on the control
* pipe. If control Erp is pending and no error has occured it initializes
* the status ERP and submits it, else it re-submits the setup packet.
* 
* The ERP callback will called with the following result.
*
* \is
* \i 1 S_usbTgtTcdLib_ERP_SUCCESS
* 
* Indicate the ERP complete one stage successed, and next stage need to be 
* processed. the developer need process next stage (STATUS 
* stage) by schedule. This case used in many normal case, such
* as the TCD need some special command to specific the status stage. 
* 
*\i 2 S_usbTgtTcdLib_ERP_RESTART
* 
* Indicate the ERP complete with _no_ error, and new SETUP stage need to be 
* processed. The developer do _not_ need process next stage(normally STATUS
* stage). This case used in some TCD. Especially for some TCD can send the 
* STATUS with the DATA IN/OUT.In this case, the STATUS
* stage will no needed, and next SETUP stage should be specific.
* 
*\i 3 S_usbTgtTcdLib_ERP_CANCELED
* 
* Indicate the ERP complete by cancelled error. It normally accored when the 
* control pipe was deleted or the ERP be cancelled by the up level. Process will
* be termate if the canceled result recived.
*
*\i 4 Other Results
*
* Indicate the ERP complete by other error.The control pipe will be stall
*
*\ie
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbTgtDataINErpCallback
    (
    pVOID pErpCallback   /* Pointer to ERP structure */
    )
    {
    pUSBTGT_TCD pTcd = NULL;
    pUSB_ERP    pErp = (pUSB_ERP)pErpCallback;
    
    /* Validate parameters */

    if ((NULL == pErp) || 
        (NULL == pErp->targPtr))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pErp) ? "pErp" :
                    "pErp->targPtr"), 2, 3, 4, 5, 6); 
        
        return;
        }
    
    /* Retrive the tcd from erp structure */

    pTcd = (pUSBTGT_TCD)pErp->targPtr;

    USB_TGT_DBG("usbTgtDataINErpCallback called with result %d\n",
                pErp->result, 2, 3, 4, 5, 6);
    
    /* Acquire Mutex */

    OSS_MUTEX_TAKE (pTcd->tcdMutex, USBTGT_WAIT_TIMEOUT);
    
    /* Check if there is some control erp pending already */

    if (TRUE == vxAtomicGet(&pTcd->controlErpUsed))
        {
        usbTgtControlErpUsedFlagClear (pTcd);
        
        /* Release Mutex */

        (void)OSS_MUTEX_RELEASE (pTcd->tcdMutex);

        switch (pErp->result)
            {
            case S_usbTgtTcdLib_ERP_SUCCESS:
            case OK:                 
                if (TRUE == usbTgtControlErpUsedFlagSet(pTcd))
                    {
                    /* Re-initialize the default control erp */
    
                    usbTgtInitStatusErp (pTcd, FALSE);
                    
                    /* submit the erp */
    
                    if(ERROR == usbTgtSubmitControlErp (pTcd))
                        usbTgtControlErpUsedFlagClear(pTcd);
                    }
                break;
            case S_usbTgtTcdLib_ERP_RESTART:

                if (TRUE == usbTgtControlErpUsedFlagSet(pTcd))
                    {
                    /* Initialize the setup ERP */
    
                    usbTgtInitSetupErp(pTcd);
    
                    /* Submit the Erp */
    
                    if(ERROR == usbTgtSubmitControlErp (pTcd))
                        usbTgtControlErpUsedFlagClear(pTcd);
                    }
                break;
            case S_usbTgtTcdLib_ERP_CANCELED:
                break;
            default:

                (void)usbTgtControlPipeStall (pTcd);
    
                if (TRUE == usbTgtControlErpUsedFlagSet(pTcd))
                    {
                    /* Initialize the setup ERP */
                
                    usbTgtInitSetupErp(pTcd);
                
                    /* Submit the Erp */
                
                    if(ERROR == usbTgtSubmitControlErp (pTcd))
                        usbTgtControlErpUsedFlagClear(pTcd);
                    }
   
                 break;
            }
        }
    else
        {     
        /* Stall default endpoints */

        (void)usbTgtControlPipeStall (pTcd);

        if (TRUE == usbTgtControlErpUsedFlagSet(pTcd))
            {
            /* Initialize the setup ERP */
        
            usbTgtInitSetupErp(pTcd);
        
            /* Submit the Erp */
        
            if(ERROR == usbTgtSubmitControlErp (pTcd))
                usbTgtControlErpUsedFlagClear(pTcd);
            }
        }

    return;
    }

/*******************************************************************************
*
* usbTgtStatusErpCallback - callback of status stage transfer
*
* This routine is called on completion of the status stage. It initialize 
* the Setup ERP and submits it to receive the next setup packet from
* host.
*
* The ERP callback will called with the following result.
*
* \is
* 
*\i 1 S_usbTgtTcdLib_ERP_CANCELED
* 
* Indicate the ERP complete by cancelled error. It normally accored when the 
* control pipe was deleted or the ERP be cancelled by the up level. Process will
* be termate if the canceled result recived.
*
*\i 2 Other Results
*
* Indicate the ERP complete with or without error.New setup stage will be 
* specific.
*
*\ie
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbTgtStatusErpCallback
    (
    pVOID pErpCallback /* Pointer to ERP structure */
    )
    {
    pUSBTGT_TCD pTcd = NULL; /* TARG_TCD */
    pUSB_ERP    pErp = (pUSB_ERP)pErpCallback;

    /* Validate Parameter */

    if ((NULL == pErp) || 
        (NULL == pErp->targPtr))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pErp) ? "pErp" :
                    "pErp->targPtr"), 2, 3, 4, 5, 6); 
        
        return;
        }

    /* Retrive the targTcd from erp structure */

    pTcd = (pUSBTGT_TCD)pErp->targPtr;

    usbTgtControlErpUsedFlagClear(pTcd);

    /*
     * If the ERP is cancelled, return from
     * the function 
     */

    if (pErp->result == S_usbTgtTcdLib_ERP_CANCELED)     
        {
        USB_TGT_DBG("usbTgtStatusErpCallback - S_usbTgtTcdLib_ERP_CANCELED\n",
                    1, 2, 3, 4, 5, 6); 
        
        return;
        }

    if (TRUE == usbTgtControlErpUsedFlagSet(pTcd))
        {
        USB_TGT_DBG("usbTgtStatusErpCallback - usbTgtControlErpUsedFlagSet\n",
                    1, 2, 3, 4, 5, 6);
        
        /* Initialize the setup ERP */
    
        usbTgtInitSetupErp(pTcd);
    
        /* Submit the setup ERP */
    
        if (ERROR == usbTgtSubmitControlErp(pTcd))
            usbTgtControlErpUsedFlagClear(pTcd);
        }
    else
        {
        USB_TGT_DBG("usbTgtStatusErpCallback - the pipe occupied\n",
                    1, 2, 3, 4, 5, 6);
        }

    return ;
    }

/*******************************************************************************
*
* usbTgtFeatureClear - process clear feature standard request
*
* This routine processes clear feature standard request.
*
* When this request received, the TCD will deal with the command first in 
* the low level. And call this routine to notisfy the targetM level to know. 
* And the targetM level will notisfy all the function drivers.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtFeatureClear
    (
    pUSBTGT_TCD pTcd /* The TCD pointer */
    )
    { 
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;
    pUSB_SETUP pSetup = NULL; /* USB_SETUP buffer */
    USBTGT_FEATURE_PARAM featureParam;
    UINT8      requestType;   /* request type */
    UINT16     feature;       /* feature to clear */
    UINT16     index;         /* index */
    STATUS     status = ERROR;

    /* Validate parameters */
    
    USBTGT_TCD_VALIDATION_RETURN_STATUS(pTcd);    

    pSetup = &pTcd->SetupPkt;

    /*
     * This request is not accepted when the device
     * is in the default state 
     */

    if (pTcd->uDeviceAddress <= 0)
        {
        USB_TGT_ERR("Feature Clear not accepted in default state\n",
                    1, 2, 3, 4, 5, 6); 

        return ERROR;
        }
    
    /* This request must be standard request from the host */

    if (((pSetup->requestType & USB_RT_DIRECTION_MASK) != USB_RT_HOST_TO_DEV) ||
        ((pSetup->requestType & USB_RT_CATEGORY_MASK) != USB_RT_STANDARD))
        {
        USB_TGT_ERR("Feature Clear must be standard request from the host\n",
                    1, 2, 3, 4, 5, 6); 
        
        return(ERROR);
        }
    
    requestType = (UINT8)( pSetup->requestType &
                 (~(USB_RT_DIRECTION_MASK | USB_RT_CATEGORY_MASK)));

    feature = FROM_LITTLEW (pSetup->value);
    index = FROM_LITTLEW (pSetup->index);

    switch (requestType)
        { 
        case USB_RT_DEVICE:
            {
            switch(feature)
                {                
                case USB_FSEL_DEV_REMOTE_WAKEUP:
                    /* 
                     * If the device supports remote wakeup,set the remote 
                     * wakeup feature.
                     * USB 3.0 do not support this 
                 */
                 
                pTcd->DeviceInfo.uDeviceFeature = (UINT16) (pTcd->DeviceInfo.uDeviceFeature & 
                                                   (~USB_DEV_STS_REMOTE_WAKEUP));  
                status = OK;
                    
                    break;
                case USB_FSEL_DEV_U1_ENABLE:
                case USB_FSEL_DEV_U2_ENABLE:
                case USB_FSEL_DEV_LTM_ENABLE:
                    if ((pTcd->uSpeed == USB_SPEED_SUPER) &&
                        (pTcd->uCurrentConfig != 0))
                        {
                        if (feature == USB_FSEL_DEV_U1_ENABLE)
                            {
                            pTcd->DeviceInfo.uDeviceFeature = (UINT16) (pTcd->DeviceInfo.uDeviceFeature & 
                                                                         (~ USB_DEV_STS_U1_ENABLE));  
                            }
                        else if (feature == USB_FSEL_DEV_U2_ENABLE)
                            {
                            pTcd->DeviceInfo.uDeviceFeature = (UINT16) (pTcd->DeviceInfo.uDeviceFeature & 
                                                                        (~ USB_DEV_STS_U2_ENABLE));  
                            }
                        else 
                            {
                            pTcd->DeviceInfo.uDeviceFeature = (UINT16) (pTcd->DeviceInfo.uDeviceFeature & 
                                                                         (~ USB_DEV_STS_LMT_ENABLE));  
                            }
                        if ((pTcd->pTcdFuncs) &&
                            (pTcd->pTcdFuncs->pTcdIoctl))
                            {
                            featureParam.uFeature = feature;
                            featureParam.uRequestType = requestType;
                            featureParam.uIndex = index;
                               
                            status = (0 == (pTcd->pTcdFuncs->pTcdIoctl)
                                            (pTcd,
                                             USBTGT_TCD_IOCTL_CMD_CLEAR_FEATURE,
                                             &featureParam)) ? OK : ERROR;
                            }      
                        }
                    break;
                default:
                    break;
                }                 
             }
            break;
        case USB_RT_INTERFACE:

            /* Not supported on USB 2.0 */

            /* FUNCTION_SUSPEND supported on USB 3.0 */

            if ((pTcd->uSpeed == USB_SPEED_SUPER) &&
                (pTcd->uCurrentConfig != 0))
                {
                if ((pTcd->pTcdFuncs) &&
                    (pTcd->pTcdFuncs->pTcdIoctl))
                    {
                    featureParam.uFeature = feature;
                    featureParam.uRequestType = requestType;
                    featureParam.uIndex = index;
                       
                    status = (0 == (pTcd->pTcdFuncs->pTcdIoctl)
                                    (pTcd,
                                     USBTGT_TCD_IOCTL_CMD_CLEAR_FEATURE,
                                     &featureParam)) ? OK : ERROR;
                    }                    

                /* The index is the endpoint address */
                
                pFuncDriver = usbTgtFindFuncByInterface (pTcd, (UINT8)(index & 0xFF));

               if ((NULL != pFuncDriver) &&
                   (NULL != pFuncDriver->pFuncCallbackTable) &&
                   (NULL != pFuncDriver->pFuncCallbackTable->featureClear))
                  {
                  status = (pFuncDriver->pFuncCallbackTable->featureClear)
                           (pFuncDriver->pCallbackParam, 
                            pFuncDriver->targChannel,
                            pSetup->requestType,
                            feature,
                            pSetup->index);

                    /* Update the function status */
                  
                  if((OK == status) &&
                    (pFuncDriver->uFuncStatus & 1))
                    {
                    if ((pSetup->index >> 8) & 0x2)
                        {
                        pFuncDriver->uFuncStatus = (UINT16)(pFuncDriver->uFuncStatus & ( ~(0x2)));
                        }
                    }
                  
                  /* 
                   * The best way is, do not scan all the function drivers.
                   * just find the right one, and call the callback.
                   */
                  }

                }
            
            break;
        case USB_RT_ENDPOINT:
            {
            if ((feature == USB_FSEL_DEV_ENDPOINT_HALT) &&
                (pTcd->uCurrentConfig != 0))
                {
                /* 
                 * It is endpoint clear feature request 
                 * Find the right endpoint and the right funtion driver
                 * Let the high level function driver to deal with this 
                 */

                 /* Call the tcd ioctl interface to clear such features */
                 
                if ((pTcd->pTcdFuncs) &&
                    (pTcd->pTcdFuncs->pTcdIoctl))
                    {
                    featureParam.uFeature = feature;
                    featureParam.uRequestType = requestType;
                    featureParam.uIndex = index;
                       
                    status = (0 == (pTcd->pTcdFuncs->pTcdIoctl)
                                    (pTcd,
                                     USBTGT_TCD_IOCTL_CMD_CLEAR_FEATURE,
                                     &featureParam)) ? OK : ERROR;
                    }                    
                     
                
                /* The index is the endpoint address */

                pFuncDriver = usbTgtFindFuncByEpIndex (pTcd, (UINT8) (index & 0xFF));

                if ((NULL != pFuncDriver) &&
                    (NULL != pFuncDriver->pFuncCallbackTable) &&
                    (NULL != pFuncDriver->pFuncCallbackTable->featureClear))
                   {
                   status = (pFuncDriver->pFuncCallbackTable->featureClear)
                             (pFuncDriver->pCallbackParam, 
                              pFuncDriver->targChannel,
                              pSetup->requestType,
                              feature,
                              index);
                  }
                }
            }
            break;
        default:
            break;
        }    

    if (ERROR == status)
        {
        USB_TGT_ERR("Process clear feature request error\n",
                    1, 2, 3, 4, 5, 6); 
        }

    return status;
    }

/*******************************************************************************
*
* usbTgtFeatureSet - process set feature standard request
*
* This routine processes set feature standard request.
*
* When this request received, the TCD will deal with the command first in 
* the low level. And call this routine to notisfy the targetM level to know. 
* And the targetM level will notisfy the right function drivers.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO:N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtFeatureSet
    (
    pUSBTGT_TCD pTcd /* The TCD pointer */
    )
    {
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;
    pUSB_SETUP pSetup = NULL;
    STATUS     status = ERROR;
    USBTGT_FEATURE_PARAM featureParam;
    UINT16     feature = 0;
    UINT16     index = 0; 
    UINT8      requestType;   /* request type */

    /* Validate parameters */
    
    USBTGT_TCD_VALIDATION_RETURN_STATUS(pTcd);    

    pSetup = &pTcd->SetupPkt;
    feature = FROM_LITTLEW (pSetup->value);
    index = FROM_LITTLEW (pSetup->index);
    requestType = pSetup->requestType;
    
    /* 
     * If the callback is not registered by the target application,
     * return an ERROR
     */

    if ((pTcd->uDeviceAddress == 0) && 
        (feature != USB_FSEL_DEV_TEST_MODE))
        {    
        USB_TGT_ERR("Feature set not accepted in default state\n",
                    1, 2, 3, 4, 5, 6); 
        
        return ERROR;
        }

    /* This request must be standard request from the host */

    if (((requestType & USB_RT_DIRECTION_MASK) != USB_RT_HOST_TO_DEV) ||
        ((requestType & USB_RT_CATEGORY_MASK) != USB_RT_STANDARD))
        {
        USB_TGT_ERR("Feature Set must be standard request from the host\n",
                    1, 2, 3, 4, 5, 6); 

        return(ERROR);
        }

    requestType = (UINT8)(requestType & ( ~(USB_RT_DIRECTION_MASK | USB_RT_CATEGORY_MASK)));

    switch (requestType)
        { 
        case USB_RT_DEVICE:
            {
            USB_TGT_DBG("Set the feature with %d\n", feature, 2, 3, 4, 5, 6);

            /* The TCD will process this request */

            switch (feature)
                {
                case USB_FSEL_DEV_REMOTE_WAKEUP:
                    /* 
                     * If the device supports remote wakeup,set the remote 
                     * wakeup feature.
                     * USB 3.0 do not support this 
                     */
                     
                    pTcd->DeviceInfo.uDeviceFeature |= USB_DEV_STS_REMOTE_WAKEUP;  
                    status = OK;                    
                    
                    break;
                case USB_FSEL_DEV_B_HNP_ENABLE:
                case USB_FSEL_DEV_A_HNP_SUPPORT:
                case USB_FSEL_DEV_A_ALT_HNP_SUPPORT:
                    USB_TGT_DBG("SET OTG uFeature %d\n",
                        feature, 2, 3, 4, 5, 6);
                
                    /*
                     * If usbOtgEventRaiseFunc is defined as non-NULL,
                     * OTG feature is included; raise an event in sync
                     * mode to get the handling result immediately.
                     */
                
                    if (usbOtgEventRaiseFunc)
                        {
                        pUSBOTG_SET_OTG_FEATURE_EVENT pEvent;
                        pEvent = (pUSBOTG_SET_OTG_FEATURE_EVENT)
                                    USBOTG_EVENT_DATA_GET();
                        if (pEvent)
                            {
                            pEvent->header.id = USBOTG_EVENT_ID_SET_OTG_FEATURE_RECEIVED;
                            pEvent->header.pDev = pTcd->pDev;
                            pEvent->header.result = ERROR;
                            pEvent->otgFeature = feature;
                            
                            usbOtgEventRaiseFunc(TRUE, pEvent);
                            
                            status = pEvent->header.result;
                            
                            USBOTG_EVENT_DATA_PUT(pEvent);
                            }
                        else
                            {
                            USB_TGT_WARN("No mem for USBOTG_SET_OTG_FEATURE_EVENT\n",
                                1, 2, 3, 4, 5, 6);
                            }
                        }
                    break;
                case USB_FSEL_DEV_U1_ENABLE:
                case USB_FSEL_DEV_U2_ENABLE:
                case USB_FSEL_DEV_LTM_ENABLE:

                    if ((pTcd->uCurrentConfig != 0)) 
                        {                        
                        if (feature == USB_FSEL_DEV_U1_ENABLE)
                            {
                            pTcd->DeviceInfo.uDeviceFeature |= USB_DEV_STS_U1_ENABLE;  
                            }
                        else if (feature == USB_FSEL_DEV_U2_ENABLE)
                            {
                            pTcd->DeviceInfo.uDeviceFeature |= USB_DEV_STS_U2_ENABLE;  
                            }
                        else 
                            {
                            pTcd->DeviceInfo.uDeviceFeature |= USB_DEV_STS_LMT_ENABLE;  
                            }
                        if ((pTcd->pTcdFuncs) &&
                            (pTcd->pTcdFuncs->pTcdIoctl))
                            {
                            featureParam.uFeature = feature;
                            featureParam.uRequestType = requestType;
                            featureParam.uIndex = index;
                               
                            status = (0 == (pTcd->pTcdFuncs->pTcdIoctl)
                                            (pTcd,
                                             USBTGT_TCD_IOCTL_CMD_SET_FEATURE,
                                             &featureParam)) ? OK : ERROR;
                            }                    
                        }
                    break;
                default:
                    break;
                }
                /* Call the tcd ioctl interface to clear such features */
           

                 }            
            break;
        case USB_RT_INTERFACE:

            /* Not supported on USB 2.0 */
            
            /* FUNCTION_SUSPEND supported on USB 3.0 */

            if ((pTcd->uSpeed == USB_SPEED_SUPER) &&
                (pTcd->uCurrentConfig != 0))
                {
                if ((pTcd->pTcdFuncs) &&
                    (pTcd->pTcdFuncs->pTcdIoctl))
                    {
                    featureParam.uFeature = feature;
                    featureParam.uRequestType = requestType;
                    featureParam.uIndex = index;
                       
                    status = (0 == (pTcd->pTcdFuncs->pTcdIoctl)
                                    (pTcd,
                                     USBTGT_TCD_IOCTL_CMD_SET_FEATURE,
                                     &featureParam)) ? OK : ERROR;
                    }                    

                /* The index is the endpoint address */
                
                pFuncDriver = usbTgtFindFuncByInterface (pTcd, (UINT8) (index & 0xFF));

               if ((NULL != pFuncDriver) &&
                   (NULL != pFuncDriver->pFuncCallbackTable) &&
                   (NULL != pFuncDriver->pFuncCallbackTable->featureSet))
                  {
                  status = (pFuncDriver->pFuncCallbackTable->featureSet)
                           (pFuncDriver->pCallbackParam, 
                            pFuncDriver->targChannel,
                            pSetup->requestType,
                            feature,
                            pSetup->index);

                  /* Update the function status */
                  
                  if ((OK == status) &&
                    (pFuncDriver->uFuncStatus & 1))
                    {
                    if ((pSetup->index >> 8) & 0x2)
                        {
                        pFuncDriver->uFuncStatus |= 0x2;
                        }
                    }
                  
                  /* 
                   * The best way is, do not scan all the function drivers.
                   * just find the right one, and call the callback.
                   */
                  }

                }

            /* Call the function resume interface */
  
            break;
        case USB_RT_ENDPOINT:
            {
            if ((feature == USB_FSEL_DEV_ENDPOINT_HALT) &&
                (pTcd->uCurrentConfig != 0))
                {
                 /* Call the tcd ioctl interface to clear such features */
                
                if ((pTcd->pTcdFuncs) &&
                    (pTcd->pTcdFuncs->pTcdIoctl))
                    {
                    featureParam.uFeature = feature;
                    featureParam.uRequestType = requestType;
                    featureParam.uIndex = index;
                       
                    status = (0 == (pTcd->pTcdFuncs->pTcdIoctl)
                                    (pTcd,
                                     USBTGT_TCD_IOCTL_CMD_SET_FEATURE,
                                     &featureParam)) ? OK : ERROR;
                    }                    
                 
                /* 
                 * It is endpoint clear feature request 
                 * Find the right endpoint and the right funtion driver
                 * Let the high level function driver to deal with this 
                 */
                
                /* The index is the endpoint address */

                pFuncDriver = usbTgtFindFuncByEpIndex (pTcd, (UINT8) (index & 0xFF));

               if ((NULL != pFuncDriver) &&
                   (NULL != pFuncDriver->pFuncCallbackTable) &&
                   (NULL != pFuncDriver->pFuncCallbackTable->featureSet))
                  {
                  status = (pFuncDriver->pFuncCallbackTable->featureSet)
                           (pFuncDriver->pCallbackParam, 
                            pFuncDriver->targChannel,
                            pSetup->requestType,
                            feature,
                            index);
  
                  /* 
                   * The best way is, do not scan all the function drivers.
                   * just find the right one, and call the callback.
                   */
                  }
                }
            }
            break;
        default:
            break;
        }    

     /* Callback with normal or force to restart ?*/

    if (ERROR == status)
        {
        USB_TGT_ERR("Process set feature request error\n",
                    1, 2, 3, 4, 5, 6); 
        
        return ERROR;
        }

    return status;
    }

/*******************************************************************************
*
* usbTgtConfigurationGet - process get configuration standard request
*
* This routine processes get configuration standard request.
*
* When this request received, this routine will return the right 
* configuration value.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO:N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtConfigurationGet
    (
    pUSBTGT_TCD pTcd /* The TCD pointer */
    )
    {
    STATUS status = ERROR;
  
    /* Validate parameters */
    
    USBTGT_TCD_VALIDATION_RETURN_STATUS(pTcd);    

    /*
     * This request is not accepted when the device
     * is in the default state 
     */

    if (pTcd->uDeviceAddress <= 0)
        {
        USB_TGT_ERR("Configuration get not accepted in default state\n",
                    1, 2, 3, 4, 5, 6); 

        return ERROR;
        }

    if (TRUE == usbTgtControlErpUsedFlagSet(pTcd))
        {
       
        pTcd->dataBfr[0] = pTcd->uCurrentConfig;
        
        /* Initialize the ERP for data phase */
    
        usbTgtInitDataErp (pTcd,
                           NULL, /* Common for all the function, set NULL */
                           &pTcd->dataBfr[0],    /* Data buffer */
                           1 ,                   /* Date length */
                           NULL,                 /* User callback */
                           TRUE,                 /* bDirIn */
                           USB_ERP_FLAG_NORMAL); /* erpFlags */
        
        /* Submit erp */
    
        status = usbTgtSubmitControlErp ( pTcd );
        
        if ( status != OK )
            {
            usbTgtControlErpUsedFlagClear(pTcd);
            }
        }
    
    USB_TGT_DBG("usbTgtConfigurationGet configuration %d with status %d\n",
                pTcd->uCurrentConfig, status, 3, 4, 5, 6); 

    return (status);
    }


/*******************************************************************************
*
* usbTgtConfigurationSet - process set configuration standard request
*
* This routine processes set configuration standard request.
*
* When this request received, this routine will notisfy all the funtion 
* drivers the configuration has been set.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO:N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtConfigurationSet
    (
    pUSBTGT_TCD pTcd /* The TCD pointer */
    )
    {
    NODE *              pNode = NULL;
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;
    pUSBTGT_CONFIG      pConfig = NULL;
    pUSB_SETUP          pSetup = NULL;
    STATUS              status = ERROR;
    UINT8               requestType;   /* request type */
    UINT16              index = 0; 
    UINT8               uConfig = 0;
    int                 i;
    int                 count;
    
    /* Validate parameters */
    
    USBTGT_TCD_VALIDATION_RETURN_STATUS(pTcd);    

    pSetup = &pTcd->SetupPkt;
    uConfig = (UINT8)(LSB(FROM_LITTLEW (pSetup->value)));
    index = FROM_LITTLEW (pSetup->index);
    requestType = pSetup->requestType;

    USB_TGT_VDBG("usbTgtConfigurationSet uConfig %p\n",
                 uConfig, 2, 3, 4, 5, 6); 


    /*
     * This request is invalid if received in a default state
     * or the configuration value is not expected
     */

    if ((pTcd->uDeviceAddress == 0) || 
        (uConfig > lstCount (&pTcd->configList)))
        {
        USB_TGT_ERR("Set configuration not specific with default stage "
                    "or ivalid config\n",
                    1, 2, 3, 4, 5, 6); 
        
        return ERROR;
        }
    
    /* Set current configuration global static variable */

    pConfig = (pUSBTGT_CONFIG)lstNth (&pTcd->configList,
                                      pTcd->uCurrentConfig ? 
                                      (pTcd->uCurrentConfig) : 
                                      (pTcd->uCurrentConfig + 1));

    /* Search all the function driver whith belong to the configation */

    count = lstCount (&pTcd->funcList);
    
    for (i = 1; i <= count; i ++)
        {
        pNode = lstNth (&pTcd->funcList, i);
        pFuncDriver = TCD_LIST_NODE_TO_USBTGT_FUNC(pNode);
        
        if ((NULL != pFuncDriver) &&
            (pFuncDriver->uCurrentConfig == uConfig))
            {
            USB_TGT_DBG("pFuncDriver->uCurrentConfig has been set as %x\n",
                        uConfig, 2,3,4,5,6);
            
            status = OK;
            }
        else
            {            
            if ((NULL != pFuncDriver) &&                
            (NULL != pFuncDriver->pFuncCallbackTable) &&
            (NULL != pFuncDriver->pFuncCallbackTable->configurationSet))
           {
               if ((pFuncDriver->pConfig != 0) && 
                   (pFuncDriver->pConfig != pConfig))
                   {
                   uConfig = 0;
                   }
               
           status = (pFuncDriver->pFuncCallbackTable->configurationSet)
                    (pFuncDriver->pCallbackParam, 
                     pFuncDriver->targChannel,
                     uConfig);
           
           if (ERROR == status)
               {
               USB_TGT_ERR("Set configuration in funtion 0x%X error\n ",
                           (ULONG)pFuncDriver, 2, 3, 4, 5, 6); 

               return ERROR;
               }

            /* Update the function configuration */
           
            pFuncDriver->uCurrentConfig = uConfig;
                }
            }
        }

    if (ERROR == status)
        {
        USB_TGT_ERR("Process set configuration request error, uConfig %d\n",
                    uConfig, 2, 3, 4, 5, 6); 
        
        return ERROR;
        }
    
    pTcd->uCurrentConfig = uConfig;
    

    /*
     * If usbOtgEventRaiseFunc is defined as non-NULL,
     * OTG feature is included; raise an event in sync
     * mode to notify that a non-default configuration 
     * is set. 
     */
    
    if ((uConfig != 0) && 
        (usbOtgEventRaiseFunc != NULL))
        {
        pUSBOTG_SET_CONFIG_RECEIVED_EVENT pEvent;
        pEvent = (pUSBOTG_SET_CONFIG_RECEIVED_EVENT)
                    USBOTG_EVENT_DATA_GET();
        if (pEvent)
            {
            USB_TGT_DBG("USBOTG_EVENT_ID_SET_CONFIG_RECEIVED - uConfig %d\n",
                        uConfig, 2, 3, 4, 5, 6);
            
            pEvent->header.id = USBOTG_EVENT_ID_SET_CONFIG_RECEIVED;
            pEvent->header.pDev = pTcd->pDev;
            pEvent->header.result = ERROR;
            
            usbOtgEventRaiseFunc(TRUE, pEvent);
            
            USBOTG_EVENT_DATA_PUT(pEvent);
            }
        else
            {
            USB_TGT_WARN("No mem for USBOTG_EVENT_ID_SET_CONFIG_RECEIVED\n",
                        1, 2, 3, 4, 5, 6);
            }
        }
    
    return status;  
    }


/*******************************************************************************
*
* usbTgtDescriptorGet - process get descriptor standard request
*
* This routine processes get descriptor standard request.
*
* When this request received, this routine will return the right descriptor. If
* needed, it will notisfy the right function driver.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO:N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtDescriptorGet
    (
    pUSBTGT_TCD pTcd /* The TCD pointer */
    )
    {
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;
    pUSBTGT_CONFIG      pConfig = NULL;
    UINT8 *             pBuf = NULL;
    UINT16              uAcdLen = 0;
    UINT8               requestType;   /* request type */
    UINT8               descriptorType;
    UINT8               descriptorIndex;
    UINT16              uLangIdOrIndex;
    UINT16              length;
    UINT16              tmpLen;
    UINT16              uConfigLenTemp = 0;
    NODE *              pNode = NULL;
    pUSBTGT_STRING      pString = NULL;
    int                 count;
    int                 index;
    BOOL                bStrFound = FALSE;
    STATUS              status = ERROR;
    UINT8               uRecipient;
    int                 i;
    
    /* Validate parameters */
    
    USBTGT_TCD_VALIDATION_RETURN_STATUS(pTcd);    

    requestType = pTcd->SetupPkt.requestType;
    length = FROM_LITTLEW (pTcd->SetupPkt.length);
    descriptorType = (UINT8)(MSB (FROM_LITTLEW (pTcd->SetupPkt.value)));
    descriptorIndex = (UINT8)(LSB (FROM_LITTLEW (pTcd->SetupPkt.value)));
    uLangIdOrIndex = FROM_LITTLEW (pTcd->SetupPkt.index);
    uRecipient = (UINT8) (requestType & USB_RT_RECIPIENT_MASK);
	
    pBuf = &pTcd->dataBfr[0];
    
    if (((requestType & USB_RT_DIRECTION_MASK) != USB_RT_DEV_TO_HOST) ||
        ((requestType & USB_RT_CATEGORY_MASK) != USB_RT_STANDARD))
        {
        USB_TGT_ERR("Descriptor Get must be standard request from the host\n",
                    1, 2, 3, 4, 5, 6); 
        
        return(ERROR);
        }

    switch(descriptorType)
        {
        case USB_DESCR_DEVICE:
        	tmpLen = min (length, pTcd->DeviceDesc.length);
            if (tmpLen > USBTGT_MAX_CONTORL_BUF_SIZE)
                {
                USB_TGT_ERR("Buffer is too small, please set USBTGT_MAX_CONTORL_BUF_SIZE bigger than 0x%x \n",
                             tmpLen, 2, 3, 4, 5, 6); 
                status = ERROR;
                } 
            else
                {
                /* copy device descriptor to pBfr and set pActLen */
                usbDescrCopy (pBuf, &pTcd->DeviceDesc, length, &uAcdLen);
            
            USB_TGT_DBG("usbTgtDescriptorGet: USB_DESCR_DEVICE length %d uAcdLen %d\n",
                        length, uAcdLen, 3, 4, 5, 6);
            
                status = OK;          
                }
            break;

        case USB_DESCR_DEVICE_QUALIFIER:

            USB_TGT_DBG("usbTgtDescriptorGet: USB_DESCR_DEVICE_QUALIFIER\n",
                        1, 2, 3, 4, 5, 6);
            tmpLen = min (length, pTcd->devQualifierDesc.length);
            if (tmpLen > USBTGT_MAX_CONTORL_BUF_SIZE)
                {
                USB_TGT_ERR("Buffer is too small, please set USBTGT_MAX_CONTORL_BUF_SIZE bigger than 0x%x \n",
                		tmpLen, 2, 3, 4, 5, 6); 
                status = ERROR;
                } 
			else
                {
                usbDescrCopy (pBuf, &pTcd->devQualifierDesc, length, &uAcdLen);
                status = OK;            
                }
            break;

        case USB_DESCR_OTHER_SPEED_CONFIGURATION:

            /* Marked as TODO: */
            
            USB_TGT_DBG("usbTgtDescriptorGet: USB_DESCR_OTHER_SPEED_CONFIGURATION\n",
                1, 2, 3, 4, 5, 6);

            /* Fall through */

        case USB_DESCR_CONFIGURATION:

            /*
             * copy configuration, interface, and endpoint descriptors
             * to pBfr and set pActLen
             */

            USB_TGT_DBG("usbTgtDescriptorGet: USB_DESCR_CONFIGURATION\n",
                        1, 2, 3, 4, 5, 6);

            pConfig = (pUSBTGT_CONFIG)lstNth (&pTcd->configList,
                                              pTcd->uCurrentConfig ? 
                                              (pTcd->uCurrentConfig) : 
                                              (pTcd->uCurrentConfig + 1));

            if(pConfig == NULL)
                {
                USB_TGT_ERR("Descriptor can't be gotten\n", 1, 2, 3, 4, 5, 6);

                break;
                }
            /*
             * If usbOtgEventRaiseFunc is defined as non-NULL,
             * OTG feature is included; raise an event in sync
             * mode to get the handling result immediately.
             */
            
            if (usbOtgEventRaiseFunc && (pConfig->OtgDesc.bLength == 0))
                {
                pUSBOTG_GET_OTG_DESCR_RECEIVED_EVENT pEvent;
                pEvent = (pUSBOTG_GET_OTG_DESCR_RECEIVED_EVENT)
                            USBOTG_EVENT_DATA_GET();
                if (pEvent)
                    {
                    USB_TGT_DBG("USBOTG_EVENT_ID_GET_OTG_DESCR_RECEIVED\n",
                                1, 2, 3, 4, 5, 6);
                    
                    pEvent->header.id = USBOTG_EVENT_ID_GET_OTG_DESCR_RECEIVED;
                    pEvent->header.pDev = pTcd->pDev;
                    pEvent->header.result = ERROR;
                    pEvent->pOtgDescr = (UINT8 *)&pConfig->OtgDesc;
                    
                    usbOtgEventRaiseFunc(TRUE, pEvent);
                    
                    USBOTG_EVENT_DATA_PUT(pEvent);
                    
                    uConfigLenTemp = pConfig->OtgDesc.bLength;
                    pConfig->ConfigDesc.totalLength = (UINT16) (pConfig->ConfigDesc.totalLength + 
                                                                 TO_LITTLEW(uConfigLenTemp));
                    }
                else
                    {
                    USB_TGT_WARN("No mem for USBOTG_GET_OTG_DESCR_RECEIVED_EVENT\n",
                                1, 2, 3, 4, 5, 6);
                    }
                }
            
            if (descriptorType == USB_DESCR_OTHER_SPEED_CONFIGURATION)
                {
                memcpy(&pConfig->OtherSpeedConfigDesc, 
                       &pConfig->ConfigDesc, USB_CONFIG_DESCR_LEN);
                
                pConfig->OtherSpeedConfigDesc.descriptorType = 
                    USB_DESCR_OTHER_SPEED_CONFIGURATION;
                
                usbDescrCopy (pBuf, 
                    &pConfig->OtherSpeedConfigDesc, USB_CONFIG_DESCR_LEN, &uAcdLen);
                }
            else
                {
                usbDescrCopy (pBuf, &pConfig->ConfigDesc, USB_CONFIG_DESCR_LEN, &uAcdLen);
                }

            /* 
             * Update the real length all the time 
             *
             * The mac host will only issue 2 bytes to get
             * the act descriptor length, we need notify 
             * the host the real lenght.
             */
                {
                pNode = lstFirst(&pTcd->funcList);

                while (pNode !=NULL)
                    {
                    pFuncDriver = TCD_LIST_NODE_TO_USBTGT_FUNC(pNode);
                    if ((pFuncDriver != NULL) && 
                        ((pFuncDriver->pSsDescBuf != NULL) ||
                          (pFuncDriver->pHsDescBuf != NULL) ||
                          (pFuncDriver->pFsDescBuf != NULL)))
                        {
                        if ((TRUE == usrUsbTgtIadDescEnableGet()) && 
                            (pFuncDriver->uIADDesc[3] > 1))
                            {
                            if((uAcdLen + USB_IAD_DESCR_LEN) > USBTGT_MAX_CONTORL_BUF_SIZE)
                                {
                                USB_TGT_ERR("Buffer is too small, please set USBTGT_MAX_CONTORL_BUF_SIZE bigger than 0x%x \n",
                                	         (uAcdLen + USB_IAD_DESCR_LEN), 2, 3, 4, 5, 6);
                                return ERROR;
                                }
                            memcpy ((pBuf + uAcdLen),                                     
                                     pFuncDriver->uIADDesc,
                                     USB_IAD_DESCR_LEN);
                            uAcdLen = (UINT16) (uAcdLen + USB_IAD_DESCR_LEN);
                            }
                        if (descriptorType == USB_DESCR_OTHER_SPEED_CONFIGURATION)
                            {
                            if (pTcd->uSpeed == USB_SPEED_HIGH)
                                {
                                if(uAcdLen>=USBTGT_MAX_CONTORL_BUF_SIZE ||  (uAcdLen + pFuncDriver->uFsDescBufLen) > USBTGT_MAX_CONTORL_BUF_SIZE)
                                    {
                                    USB_TGT_ERR("Buffer is too small, please set USBTGT_MAX_CONTORL_BUF_SIZE bigger than 0x%x \n",
                                                 (uAcdLen + pFuncDriver->uFsDescBufLen), 2, 3, 4, 5, 6);
                                    return ERROR;
                                    }
                                memcpy ((pBuf + uAcdLen),
                                        pFuncDriver->pFsDescBuf,
                                        pFuncDriver->uFsDescBufLen);
                                
                                uAcdLen = (UINT16) (uAcdLen + pFuncDriver->uFsDescBufLen);
                                }
                            else
                                {    
                                if(uAcdLen>=USBTGT_MAX_CONTORL_BUF_SIZE || (uAcdLen + pFuncDriver->uHsDescBufLen) > USBTGT_MAX_CONTORL_BUF_SIZE)
                                    {
                                    USB_TGT_ERR("Buffer is too small, please set USBTGT_MAX_CONTORL_BUF_SIZE bigger than 0x%x \n",
                                                 (uAcdLen + pFuncDriver->uHsDescBufLen), 2, 3, 4, 5, 6);
                                    return ERROR;
                                    }
                                memcpy ((pBuf + uAcdLen),
                                        pFuncDriver->pHsDescBuf,
                                        pFuncDriver->uHsDescBufLen);
                                
                                uAcdLen = (UINT16) (uAcdLen + pFuncDriver->uHsDescBufLen);
                                }
                            }
                        else
                            {
                            if (pTcd->uSpeed == USB_SPEED_FULL)
                                {
                                if(uAcdLen>=USBTGT_MAX_CONTORL_BUF_SIZE || (uAcdLen + pFuncDriver->uFsDescBufLen) > USBTGT_MAX_CONTORL_BUF_SIZE)
                                    {
                                    USB_TGT_ERR("Buffer is too small, please set USBTGT_MAX_CONTORL_BUF_SIZE bigger than 0x%x \n",
                                                 (uAcdLen + pFuncDriver->uFsDescBufLen), 2, 3, 4, 5, 6);
                                    return ERROR;
                                    }
                                memcpy ((pBuf + uAcdLen),
                                        pFuncDriver->pFsDescBuf,
                                        pFuncDriver->uFsDescBufLen);
                                
                                uAcdLen = (UINT16)(uAcdLen + pFuncDriver->uFsDescBufLen);
                                }
                            else if (pTcd->uSpeed == USB_SPEED_HIGH)
                                {       
                                if(uAcdLen>=USBTGT_MAX_CONTORL_BUF_SIZE || (uAcdLen + pFuncDriver->uHsDescBufLen) > USBTGT_MAX_CONTORL_BUF_SIZE)
                                    {
                                    USB_TGT_ERR("Buffer is too small, please set USBTGT_MAX_CONTORL_BUF_SIZE bigger than 0x%x \n",
                                                 (uAcdLen + pFuncDriver->uHsDescBufLen), 2, 3, 4, 5, 6);
                                    return ERROR;
                                    }
                                memcpy ((pBuf + uAcdLen),
                                        pFuncDriver->pHsDescBuf,
                                        pFuncDriver->uHsDescBufLen);
                                
                                uAcdLen = (UINT16) (uAcdLen + pFuncDriver->uHsDescBufLen);
                                }
                            else
                                {
                                if(uAcdLen>=USBTGT_MAX_CONTORL_BUF_SIZE || (uAcdLen + pFuncDriver->uSsDescBufLen) > USBTGT_MAX_CONTORL_BUF_SIZE)
                                    {
                                    USB_TGT_ERR("Buffer is too small, please set USBTGT_MAX_CONTORL_BUF_SIZE bigger than 0x%x \n",
                                                 (uAcdLen + pFuncDriver->uSsDescBufLen), 2, 3, 4, 5, 6);
                                    return ERROR;
                                    }
                                memcpy ((pBuf + uAcdLen),
                                        pFuncDriver->pSsDescBuf,
                                        pFuncDriver->uSsDescBufLen);
                                
                                uAcdLen = (UINT16) (uAcdLen + pFuncDriver->uSsDescBufLen);

                                }
                            }                        
                         }

                    pNode = lstNext(pNode);
                    }
                }
            
            if (pConfig->OtgDesc.bLength)
                {
                if(uAcdLen>=USBTGT_MAX_CONTORL_BUF_SIZE || (uAcdLen + pConfig->OtgDesc.bLength) > USBTGT_MAX_CONTORL_BUF_SIZE)
                    {
                    USB_TGT_ERR("Buffer is too small, please set USBTGT_MAX_CONTORL_BUF_SIZE bigger than 0x%x \n",
                                 (uAcdLen + pConfig->OtgDesc.bLength), 2, 3, 4, 5, 6);
                    return ERROR;
                    }
                memcpy ((pBuf + uAcdLen), 
                        &pConfig->OtgDesc, pConfig->OtgDesc.bLength);

                uAcdLen = (UINT16) (uAcdLen + pConfig->OtgDesc.bLength);

                USB_TGT_DBG("descriptorGet: OTG DESCR uAcdLen %d\n",
                            pConfig->OtgDesc.bLength, 2, 3, 4, 5, 6);
                }

            if(descriptorType == USB_DESCR_OTHER_SPEED_CONFIGURATION)
                {
                pConfig->OtherSpeedConfigDesc.totalLength = TO_LITTLEW (uAcdLen);
                memcpy (pBuf, &pConfig->OtherSpeedConfigDesc, USB_CONFIG_DESCR_LEN);      
                }
            else
                {
                pConfig->ConfigDesc.totalLength = TO_LITTLEW (uAcdLen);
                memcpy (pBuf, &pConfig->ConfigDesc, USB_CONFIG_DESCR_LEN);      
                }
            
            status = OK;

            break;

        case USB_DESCR_STRING:

            USB_TGT_DBG("usbTgtDescriptorGet: USB_DESCR_STRING\n",
                        1, 2, 3, 4, 5, 6);
            
            switch(descriptorIndex)
                {
                case USBTGT_LANG_DESC_INDEX:
                    
                    /* Copy language descriptor to pBfr and set pActLen */
                	tmpLen = min (length, pTcd->pUsbTgtLangDescr->length);
                    if (tmpLen > USBTGT_MAX_CONTORL_BUF_SIZE)
                        {
                        USB_TGT_ERR("Buffer is too small, please set USBTGT_MAX_CONTORL_BUF_SIZE bigger than 0x%x \n",
                        	         tmpLen, 2, 3, 4, 5, 6); 
                        status = ERROR;
                        } 
                    else
                        {
                        usbDescrCopy(pBuf,
                                     (pVOID)(pTcd->pUsbTgtLangDescr),
                                     length,
                                     &uAcdLen);
                    
                        status = OK;
                        }
                    break;
                case USBTGT_MFG_STRING_INDEX:

                    /* Get the manufacture string from the user configure */

                    status = usrUsbTgtStringDescGet(uLangIdOrIndex, 
                                                    USBTGT_MFG_STRING_INDEX,
                                                    length,
                                                    pBuf,
                                                    &uAcdLen);

                    if (ERROR == status)
                        {
                        /* 
                         * The user do not have the specific string descriptor
                         * Using the string configured by TCD
                         */
                        tmpLen = min(strlen(pTcd->pMfgString), (USB_MAX_DESCR_LEN - USB_DESCR_HDR_LEN) / 2);
                        tmpLen = (USB_DESCR_HDR_LEN + tmpLen * 2);
                        tmpLen = min (length, tmpLen);
                        if (tmpLen > USBTGT_MAX_CONTORL_BUF_SIZE)
                            {
                            USB_TGT_ERR("Buffer is too small, please set USBTGT_MAX_CONTORL_BUF_SIZE bigger than 0x%x \n",
                            	         tmpLen, 2, 3, 4, 5, 6); 
                            status = ERROR;
                            } 
                        else
                            {
                        	usbDescrStrCopy (pBuf, pTcd->pMfgString, length, &uAcdLen);
                        	status = OK;
                            }
                        }
                    else
                        status = OK;
                    break;
                case USBTGT_PROD_STRING_INDEX:
                    
                    /* Get the prodect string from the user configure */
                    
                    status = usrUsbTgtStringDescGet(uLangIdOrIndex, 
                                                    USBTGT_PROD_STRING_INDEX,
                                                    length,
                                                    pBuf,
                                                    &uAcdLen);

                    if (ERROR == status)
                        {
                        /* 
                         * The user do not have the specific string descriptor
                         * Using the string configured by TCD
                         */
                        tmpLen = min(strlen(pTcd->pProdString), (USB_MAX_DESCR_LEN - USB_DESCR_HDR_LEN) / 2);
                        tmpLen = (USB_DESCR_HDR_LEN + tmpLen * 2);
                        tmpLen = min (length, tmpLen);
                        if (tmpLen > USBTGT_MAX_CONTORL_BUF_SIZE)
                            {
                            USB_TGT_ERR("Buffer is too small, please set USBTGT_MAX_CONTORL_BUF_SIZE bigger than 0x%x \n",
                            	         tmpLen, 2, 3, 4, 5, 6); 
                            status = ERROR;
                            }
                        else
                            {
                            usbDescrStrCopy (pBuf, pTcd->pProdString, length, &uAcdLen);
                            status = OK;
                            }
                        }
                    else
                        {
                        status = OK;
                        }
                    break;
                case USBTGT_SERIAL_STRING_INDEX:
                    
                    /* Get the serial string from the user configure */
                    
                    status = usrUsbTgtStringDescGet(uLangIdOrIndex, 
                                                    USBTGT_SERIAL_STRING_INDEX,
                                                    length,
                                                    pBuf,
                                                    &uAcdLen);

                    if (ERROR == status)
                        {
                        /* 
                         * The user do not have the specific string descriptor
                         * Using the string configured by TCD
                         */
                        tmpLen = min(strlen(pTcd->pSerialString), (USB_MAX_DESCR_LEN - USB_DESCR_HDR_LEN) / 2);
                        tmpLen = (USB_DESCR_HDR_LEN + tmpLen * 2);
                        tmpLen = min (length, tmpLen);                    	
                        if (tmpLen > USBTGT_MAX_CONTORL_BUF_SIZE)
                            {
                            USB_TGT_ERR("Buffer is too small, please set USBTGT_MAX_CONTORL_BUF_SIZE bigger than 0x%x \n",
                            	         tmpLen, 2, 3, 4, 5, 6); 
                            status = ERROR;
                            }
                        else   
                            {
                            usbDescrStrCopy (pBuf, pTcd->pSerialString, length, &uAcdLen);
                            status = OK;
                            }
                        }
                    else
                        {
                        status = OK;
                        }
                    break;
                    case USBTGT_OS_DESC_STRING_INDEX:
						
                        logMsg("usbTgtDescriptorGet: USBTGT_OS_DESC_STRING_INDEX\n",
                                     1, 2, 3, 4, 5, 6);   
						
                        /* Check if Windows OS String Descriptor should be answered */

						pFuncDriver = usbTgtFindFuncByInterface(pTcd, (UINT8)(uLangIdOrIndex & 0xFF));
					
                        if (pFuncDriver != NULL && pFuncDriver->pWcidString != NULL)
                            {
                            /* Microsoft OS String Descriptor, copy Microsoft signature */
                         
                            usbDescrStrCopy (pBuf, "MSFT100", length, &uAcdLen);
                        
                            /* Set vendor code */
                        
                            pBuf[uAcdLen++] = pFuncDriver->uWcidVc;
                        
                            /* Padding */
                        
                            pBuf[uAcdLen++] = 0;
                        
                            /* Update pkt length */
                        
                            pBuf[0] = uAcdLen;
                            status = OK;   
                           }
                        else
                            status = ERROR;
                    break;
                default:
                    count = lstCount(&pTcd->strDescList);

                    for (index = 1; index <= count; index ++)
                        {
                        pString = (pUSBTGT_STRING)lstNth(&pTcd->strDescList, index);

                        if ((NULL != pString) &&
                            (pString->uLangId == uLangIdOrIndex) &&
                            (pString->uNewStrIndex == descriptorIndex))
                            {
                            /* 
                             * We find the string according to the language ID 
                             * and the index
                             */
                            tmpLen = min(strlen((char *)pString->strBuf), (USB_MAX_DESCR_LEN - USB_DESCR_HDR_LEN) / 2);
                            tmpLen = (USB_DESCR_HDR_LEN + tmpLen * 2);
                            tmpLen = min (length, tmpLen);                    	
                            if (tmpLen > USBTGT_MAX_CONTORL_BUF_SIZE)
                                {
                                USB_TGT_ERR("Buffer is too small, please set USBTGT_MAX_CONTORL_BUF_SIZE bigger than 0x%x \n",
                                	         tmpLen, 2, 3, 4, 5, 6); 
                                bStrFound = FALSE;
                                }
                            else   
                                {
                                usbDescrCopy (pBuf, (char *)pString->strBuf, USBTGT_STRING_LEN, &uAcdLen);
                                bStrFound = TRUE;                            	
                                }

                            break;
                            }
                        }
                            
                    status = (TRUE == bStrFound) ? OK : ERROR;
                    break;
                }
            break;
        case USB_DESCR_INTERFACE_POWER: /* INTERFACE_POWER */

            /* copy interface power descriptor to pBfr and set pActLen */

            break;
            /* Get the BOS descriptors */
        case USB_DESCR_BOS:
            {
            if (TO_LITTLEW(pTcd->DeviceDesc.bcdUsb) >= USBTGT_VERSION_21)
                {
                if (length > USBTGT_MAX_CONTORL_BUF_SIZE)
                    {
                    USB_TGT_ERR("Buffer is too small, please set USBTGT_MAX_CONTORL_BUF_SIZE bigger than 0x%x \n",
                    		length, 2, 3, 4, 5, 6);
                    uAcdLen = 0;
                    status = ERROR;
                    }
                else
                    {
                    memcpy (pBuf, pTcd->pBosBuf, length);
                    uAcdLen = length;
                    status = OK;
                    }
                }
            }
            break;
        default:
            {
            /* Do not know what kind of descriptors */
            
            USB_TGT_DBG("usbTgtDescriptorGet:descriptorType 0x%X and uRecipient %p\n",
                        descriptorType, uRecipient, 3, 4, 5, 6);
            
            /* The request based on the interface */
            
            if (USB_RT_INTERFACE == uRecipient)
                {
                
                pFuncDriver = usbTgtFindFuncByInterface(pTcd, (UINT8)(uLangIdOrIndex & 0xFF));
                
                if ((NULL != pFuncDriver) &&
                    (NULL != pFuncDriver->pFuncCallbackTable) &&
                    (NULL != pFuncDriver->pFuncCallbackTable->descriptorGet))
                     {
                     status = (pFuncDriver->pFuncCallbackTable->descriptorGet)
                              (pFuncDriver->pCallbackParam, 
                               pFuncDriver->targChannel,
                               requestType,
                               descriptorType,
                               descriptorIndex,                         
                               uLangIdOrIndex,
                               length > USBTGT_MAX_CONTORL_BUF_SIZE?USBTGT_MAX_CONTORL_BUF_SIZE:length,
                               &pTcd->dataBfr[0],
                               &uAcdLen);                 
                    }  
                }
            else if (USB_RT_ENDPOINT == uRecipient)
                {
                /* The request based on the endpoint */
                
                pFuncDriver = usbTgtFindFuncByEpIndex(pTcd, (UINT8)(uLangIdOrIndex & 0xFF));
                
                if ((NULL != pFuncDriver) &&
                    (NULL != pFuncDriver->pFuncCallbackTable) &&
                    (NULL != pFuncDriver->pFuncCallbackTable->descriptorGet))
                     {
                     status = (pFuncDriver->pFuncCallbackTable->descriptorGet)
                              (pFuncDriver->pCallbackParam, 
                               pFuncDriver->targChannel,
                               requestType,
                               descriptorType,
                               descriptorIndex,                         
                               uLangIdOrIndex,
                               length > USBTGT_MAX_CONTORL_BUF_SIZE?USBTGT_MAX_CONTORL_BUF_SIZE:length,
                               &pTcd->dataBfr[0],
                               &uAcdLen);                 
                    }  
                }
            else
                {
                /* 
                 * Others, roll all the function driver to find the first one
                 * to process the request. NOTE: This may be some potential
                 * issue if there are 2 or more function drivers support the 
                 * un-general descriptor request.
                 */
                
                count = lstCount (&pTcd->funcList);
                
                for (i = 1; i <= count; i ++)
                    {
                    pNode = lstNth (&pTcd->funcList, i);
                    pFuncDriver = TCD_LIST_NODE_TO_USBTGT_FUNC(pNode);
                    
                    if ((NULL != pFuncDriver) &&
                        (NULL != pFuncDriver->pFuncCallbackTable) &&
                        (NULL != pFuncDriver->pFuncCallbackTable->descriptorGet))
                       {
                       status = (pFuncDriver->pFuncCallbackTable->descriptorGet)
                                (pFuncDriver->pCallbackParam, 
                                 pFuncDriver->targChannel,
                                 requestType,
                                 descriptorType,
                                 descriptorIndex,                         
                                 uLangIdOrIndex,
                                 length > USBTGT_MAX_CONTORL_BUF_SIZE?USBTGT_MAX_CONTORL_BUF_SIZE:length,
                                 &pTcd->dataBfr[0],
                                 &uAcdLen);                 
                       
                       if (OK == status)
                           {
                           USB_TGT_DBG("Find one function driver to process this command\n ",
                                       (ULONG)pFuncDriver, 2, 3, 4, 5, 6); 

                           break;
                           }
                        }
                    }
                }
            }
            break;
        }

    /* Callback with normal or force to restart ? */

    if (ERROR == status)
        {
        USB_TGT_ERR("Process get descriptor request error\n",
                    1, 2, 3, 4, 5, 6); 
        
        return ERROR;
        }
    
    if (TRUE == usbTgtControlErpUsedFlagSet(pTcd))
        {

        /* Initialize the ERP for data phase */
    
        usbTgtInitDataErp (pTcd ,
                           NULL, /* Common for all the function, set NULL */
                           &pTcd->dataBfr[0],    /* Data buffer */
                           min(uAcdLen, length), /* Date length */
                           NULL,                 /* User callback */
                           TRUE,                 /* bDirIn */
                           USB_ERP_FLAG_NORMAL); /* erpFlags */
        
        /* Submit erp */
    
        status = usbTgtSubmitControlErp ( pTcd );
        
        if ( status != OK )
            {
            usbTgtControlErpUsedFlagClear(pTcd);
            }
        }
    else
        status = ERROR;
    
    USB_TGT_DBG("usbTgtDescriptorGet status %d \n",
                status, 2, 3, 4, 5, 6); 

    return (status);    
    }

/*******************************************************************************
*
* usbTgtDescriptorSet - process set descriptor standard request
*
* This routine processes set descriptor standard request.
*
* When this request received, this routine will parpare to receive the 
* descriptor.
*
* This is not supported in the current release.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO:N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtDescriptorSet
    (
    pUSBTGT_TCD pTcd /* The TCD pointer */
    )
    {
    /* Validate parameters */
    
    USBTGT_TCD_VALIDATION_RETURN_STATUS(pTcd);    

    USB_TGT_DBG("Received Set Descriptor request\n",
                1, 2, 3, 4, 5, 6); 

    return ERROR;    
    }

/*******************************************************************************
*
* usbTgtInterfaceGet - process get interface standard request
*
* This routine processes get interface standard request.
*
* When this request received, this routine will return the right 
* alternate interface value.
*
* RETURNS: OK or ERROR if invalid
*
* ERRNO:N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtInterfaceGet
    (
    pUSBTGT_TCD pTcd /* The TCD pointer */
    )
    {
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;
    pUSB_SETUP pSetup = NULL;
    STATUS     status = ERROR;
    UINT16     uAltSetting = 0;
    UINT16     uInterface = 0; 

    /* Validate parameters */
    
    USBTGT_TCD_VALIDATION_RETURN_STATUS(pTcd);    

    /*
     * This is an invalid request if the device is in 
     * default/addressed state 
     */
     
    if (pTcd->uCurrentConfig == 0)
        {
        USB_TGT_ERR("Interface Set not accepted in unconfigured state\n",
                    1, 2, 3, 4, 5, 6); 
        
        return ERROR;
        }
    
    pSetup = &pTcd->SetupPkt;
    uAltSetting  = FROM_LITTLEW (pSetup->value);
    uInterface = FROM_LITTLEW (pSetup->index);

    /* Get the interface setting from the target application. */
    
    pFuncDriver = usbTgtFindFuncByInterface(pTcd, (UINT8) (uInterface & 0xFF));

    if ((NULL != pFuncDriver) &&
       (NULL != pFuncDriver->pFuncCallbackTable) &&
       (NULL != pFuncDriver->pFuncCallbackTable->interfaceGet))
       {
       status = (pFuncDriver->pFuncCallbackTable->interfaceGet)
                (pFuncDriver->pCallbackParam, 
                 pFuncDriver->targChannel,
                 uInterface,
                 &pTcd->dataBfr[0]);

      /* 
       * The best way is, do not scan all the function drivers.
       * just find the right one, and call the callback.
       */
      }

     /* Callback with normal or force to restart ?*/

    if (ERROR == status)
        {
        USB_TGT_ERR("Process get Interface request error\n",
                    1, 2, 3, 4, 5, 6); 
        
        return ERROR;
        }
    
    if (TRUE == usbTgtControlErpUsedFlagSet(pTcd))
        {
        /* Initialize the ERP for data phase */
    
        usbTgtInitDataErp (pTcd,
                           NULL, /* Common for all the function, set NULL */
                           &pTcd->dataBfr[0],    /* Data buffer */
                           1 ,                   /* Date length */
                           NULL,                 /* User callback */
                           TRUE,                 /* bDirIn */
                           USB_ERP_FLAG_NORMAL); /* erpFlags */
        
        /* Submit erp */
    
        status = usbTgtSubmitControlErp ( pTcd );
        
        if ( status != OK )
            {
            usbTgtControlErpUsedFlagClear(pTcd);
            }
        }
    else
        status = ERROR;
    
    USB_TGT_DBG("usbTgtInterfaceGet status %d\n",
                status, 2, 3, 4, 5, 6); 

    return (status);
    }

/*******************************************************************************
*
* usbTgtInterfaceSet - process set interface standard request
*
* This routine processes set interface standard request.
*
* When this request received, this routine will find the right function driver 
* and set the alternate interface value.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO:N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtInterfaceSet
    (
    pUSBTGT_TCD pTcd /* The TCD pointer */
    )
    {
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;
    pUSB_SETUP pSetup = NULL;
    STATUS     status = ERROR;
    UINT16     uInterface = 0; 
    UINT8      uAltSetting = 0;
    
    /* Validate parameters */
    
    USBTGT_TCD_VALIDATION_RETURN_STATUS(pTcd);    

    /*
     * This is an invalid request if the device is in 
     * default/addressed state 
     */
     
    if (pTcd->uCurrentConfig == 0)
        {
        USB_TGT_ERR("Interface Set not accepted in unconfigured state\n",
                    1, 2, 3, 4, 5, 6); 
        
        return ERROR;
        }
    
    pSetup = &pTcd->SetupPkt;
    uAltSetting  = (UINT8) (LSB (FROM_LITTLEW (pSetup->value)));
    uInterface = FROM_LITTLEW (pSetup->index);


    /* Get the interface setting from the target application. */
    
    pFuncDriver = usbTgtFindFuncByInterface(pTcd, (UINT8)(uInterface & 0xFF));

    if ((NULL != pFuncDriver) &&
       (NULL != pFuncDriver->pFuncCallbackTable) &&
       (NULL != pFuncDriver->pFuncCallbackTable->interfaceSet))
       {
       status = (pFuncDriver->pFuncCallbackTable->interfaceSet)
                (pFuncDriver->pCallbackParam, 
                 pFuncDriver->targChannel,
                 uInterface,
                 uAltSetting);       
      }

     /* Callback with normal or force to restart ?*/

    if (ERROR == status)
        {
        USB_TGT_ERR("Process set Interface request error\n",
                    1, 2, 3, 4, 5, 6); 
        
        }

    return status;
    }


/*******************************************************************************
*
* usbTgtStatusGet - process get status standard request
*
* This routine processes get status standard request.
*
* When this request received, this routine will return the right 
* status, if needed, the right function driver will be notisfied.
*
* Normally, the status get reqest will be processed by the low level TCD, and
* never be here. In case some TCD do not realize it.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO:N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtStatusGet
    (
    pUSBTGT_TCD pTcd /* The TCD pointer */
    )
    {
    pUSB_SETUP pSetup = NULL;
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;
    pUSBTGT_PIPE        pUsbTgtPipe = NULL;
    
    UINT8      requestType;   /* request type */
    UINT16     feature;       /* feature to clear */
    UINT16     index;         /* index */
    STATUS     status = ERROR;

    /* Validate parameters */
    
    USBTGT_TCD_VALIDATION_RETURN_STATUS(pTcd);    

    /* This is an invalid request if received in default state */

    if (pTcd->uDeviceAddress == 0)
        {
        USB_TGT_ERR("Status Get not accepted in default state\n",
                    1, 2, 3, 4, 5, 6); 
        
        return ERROR;
        }
    
    pSetup = &pTcd->SetupPkt;
    feature = FROM_LITTLEW (pSetup->value);
    index = FROM_LITTLEW (pSetup->index);
    requestType = pSetup->requestType;

    /* this request must be standard request to the host */

    if (((requestType & USB_RT_DIRECTION_MASK) != USB_RT_DEV_TO_HOST) ||
        ((requestType & USB_RT_CATEGORY_MASK) != USB_RT_STANDARD))
        {
        USB_TGT_ERR("Status Get must be standard request %x from the target\n",
                    requestType, 2, 3, 4, 5, 6); 
        
        return(ERROR);
        }
    
    requestType = (UINT8) (requestType & (~(USB_RT_DIRECTION_MASK | USB_RT_CATEGORY_MASK)));

    switch (requestType)
        {
        case USB_RT_DEVICE:

            if (index == 0)
                {                    
                *(UINT16 *)&pTcd->dataBfr[0] = pTcd->DeviceInfo.uDeviceFeature;
                
                status = OK;
                }
            else if ((index == USBOTG_OTGSTS_SELECTOR) && usbOtgEventRaiseFunc)
                {
                /*
                 * If usbOtgEventRaiseFunc is defined as non-NULL,
                 * OTG feature is included; raise an event in sync
                 * mode to get the handling result immediately.
                 */
                 
                pUSBOTG_GET_OTG_STATUS_RECEIVED_EVENT pEvent;
                pEvent = (pUSBOTG_GET_OTG_STATUS_RECEIVED_EVENT)
                            USBOTG_EVENT_DATA_GET();

                if (pEvent)
                    {
                    pEvent->header.id = USBOTG_EVENT_ID_GET_OTG_STATUS_RECEIVED;
                    pEvent->header.pDev = pTcd->pDev;
                    pEvent->header.result = ERROR;
                    pEvent->otgStatus = 0;
                    
                    usbOtgEventRaiseFunc(TRUE, pEvent);
                                         
                    if (pEvent->header.result == OK)
                          *(UINT16 *)&pTcd->dataBfr[0] = pEvent->otgStatus;
                    
                    USBOTG_EVENT_DATA_PUT(pEvent);

                    status = pEvent->header.result;
                    }
                else
                    {
                    USB_TGT_WARN("No mem for USBOTG_GET_OTG_STATUS_RECEIVED_EVENT\n", 
                                 1, 2, 3, 4, 5, 6);
                    status = ERROR;
                    }
                }

            /* Or get OTG status TODO : */
            
            break;
        case USB_RT_INTERFACE:
            USB_TGT_DBG("USB_RT_INTERFACE\n", 1, 2, 3, 4, 5, 6); 
            
            if (pTcd->uSpeed == USB_SPEED_SUPER)
                {
                /* The index is the endpoint address */
                
                pFuncDriver = usbTgtFindFuncByInterface (pTcd, (UINT8) (index & 0xFF));

                if (NULL != pFuncDriver)
                    {
                    pTcd->dataBfr[0] = (UINT8) (pFuncDriver->uFuncStatus & 0xFF);
                    pTcd->dataBfr[1] = (UINT8) (pFuncDriver->uFuncStatus >> 8);
                    status = OK;
                    }

                }
            break;
        case USB_RT_ENDPOINT:

            /* 
             * The USB ENDPOINT status MUST be processed by the TCD
             * And the TML will not process this request.
             */
            
            /* 
             * If this request has processed by the TCD, the TCD should not
             * notify the TML for this request by the setup stage callback.
             * If TML got the request for endpoint here, response needed to 
             * the USB host.
             */

            if (0 == index)
                {
                /* Control pipe */
                status = (pTcd->pTcdFuncs->pTcdPipeStatusGet)
                          (pTcd,
                           pTcd->controlPipe,
                           (UINT16 *)&pTcd->dataBfr[0]);
                }
            else
                {
                /* Find function by the endpoint index */

                pFuncDriver = usbTgtFindFuncByEpIndex(pTcd, (UINT8)(index & 0xFF));

                pUsbTgtPipe = usbTgtFindPipeByEpIndex(pFuncDriver, (UINT8) (index & 0xFF));

                if ((NULL != pUsbTgtPipe) &&
                    (pTcd->pTcdFuncs) &&
                    (pTcd->pTcdFuncs->pTcdPipeStatusGet))
                    {
                    status = (pTcd->pTcdFuncs->pTcdPipeStatusGet)
                              (pTcd,
                               pUsbTgtPipe->pipeHandle,
                               (UINT16 *)&pTcd->dataBfr[0]);
                    }                
                }
            
            USB_TGT_DBG("USB_RT_ENDPOINT\n",
                        1, 2, 3, 4, 5, 6); 
            
            break;
        default:
            break;
        }

     /* Callback with normal or force to restart ?*/

    if (ERROR == status)
        {
        USB_TGT_ERR("Process get status request error\n",
                    1, 2, 3, 4, 5, 6); 
        
        return ERROR;
        }
    
    USB_TGT_DBG("Normal process the request, into data stage\n",
            1, 2, 3, 4, 5, 6); 

    if (TRUE == usbTgtControlErpUsedFlagSet(pTcd))
        {
        /* Initialize the ERP for data phase */
    
        usbTgtInitDataErp (pTcd ,
                           NULL, /* Common for all the function, set NULL */
                           &pTcd->dataBfr[0],    /* Data buffer */
                           2 ,                   /* Date length */
                           NULL,                 /* User callback */
                           TRUE,                 /* bDirIn */
                           USB_ERP_FLAG_NORMAL); /* erpFlags */
        
        /* Submit erp */
    
        status = usbTgtSubmitControlErp ( pTcd );
        
        if ( status != OK )
            {
            usbTgtControlErpUsedFlagClear(pTcd);
            }
        }
    else
        status = ERROR;           

    USB_TGT_DBG("usbTgtStatusGet status %d \n",
                status, 2, 3, 4, 5, 6); 

    return (status);    
    }

/*******************************************************************************
*
* usbTgtAddressSet - process set address standard request
*
* This routine processes set address standard request.
*
* When this request received, the TCD will deal with the command first in 
* the low level. And call this routine to notisfy the targetM level to know. 
* And the targetM level will notisfy all the function drivers the address has
* been set.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO:N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtAddressSet
    (
    pUSBTGT_TCD pTcd /* The TCD pointer */
    )
    {
    NODE *              pNode = NULL;
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;
    STATUS     status = ERROR;
    UINT16     uAddress = 0;
    int        count;
    int        i;

    /* Validate parameters */
    
    USBTGT_TCD_VALIDATION_RETURN_STATUS(pTcd);    

    uAddress = FROM_LITTLEW (pTcd->SetupPkt.value);

    if (pTcd->uCurrentConfig != 0)
        {
        /* From the USB 2.0 spec, set address is not specific in configured stage */
        
        USB_TGT_WARN("set address is not specific in configured stage \n",
                    1, 2, 3, 4, 5, 6); 
        
        }

    count = lstCount (&pTcd->funcList);
    
    for (i = 1; i <= count; i ++)
        {
        pNode = lstNth (&pTcd->funcList, i);
        pFuncDriver = TCD_LIST_NODE_TO_USBTGT_FUNC(pNode);
        
        if ((NULL != pFuncDriver) &&
            (NULL != pFuncDriver->pFuncCallbackTable) &&
            (NULL != pFuncDriver->pFuncCallbackTable->addressSet))
           {
           status = (pFuncDriver->pFuncCallbackTable->addressSet)
                    (pFuncDriver->pCallbackParam, 
                     pFuncDriver->targChannel,
                     uAddress);
           
           if (ERROR == status)
               {
               USB_TGT_WARN("Set Address in funtion 0x%X error\n ",
                           (ULONG)pFuncDriver, 2, 3, 4, 5, 6);
               
               /* Do not return till roll all the function drivers */
               }
            }
        }

    /*
     * No matter we notify the funcDriver or not, we should record the
     * address and keep going
     */

    pTcd->uDeviceAddress = (UINT8) (uAddress);

    /* Always return OK */
    
    return OK;
    }

/*******************************************************************************
*
* usbTgtSynchFrameGet - process get synch frame number standard request
*
* This routine processes get synch frame number standard request.
*
* When this request received, this routine will find the right function driver
* to get the frame number. This is only valid in the ISO endpoints.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO:N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtSynchFrameGet
    (
    pUSBTGT_TCD pTcd /* The TCD pointer */
    )
    {
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;
    pUSB_SETUP pSetup = NULL; /* USB_SETUP buffer */
    UINT8      requestType;   /* request type */
    UINT16     feature;       /* feature to clear */
    UINT16     index;         /* index */
    STATUS     status = ERROR;
    
    /* Validate parameters */
    
    USBTGT_TCD_VALIDATION_RETURN_STATUS(pTcd);    

    pSetup = &pTcd->SetupPkt;

    /*
     * This request is not accepted when the device
     * is in the default state 
     */

    if (pTcd->uCurrentConfig == 0)
        {
        USB_TGT_ERR("Synch Frame get not accepted in unconfigured state\n",
                    1, 2, 3, 4, 5, 6); 

        return ERROR;
        }
    
    /* This request must be standard request from the host */

    if (((pSetup->requestType & USB_RT_DIRECTION_MASK) != USB_RT_DEV_TO_HOST) ||
        ((pSetup->requestType & USB_RT_CATEGORY_MASK) != USB_RT_STANDARD))
        {
        USB_TGT_ERR("Synch Frame get must be standard request from the host\n",
                    1, 2, 3, 4, 5, 6); 
        
        return(ERROR);
        }
    
    requestType = (UINT8) (pSetup->requestType &
                 (~(USB_RT_DIRECTION_MASK | USB_RT_CATEGORY_MASK)));

    feature = FROM_LITTLEW (pSetup->value);
    index = FROM_LITTLEW (pSetup->index);

     pFuncDriver = usbTgtFindFuncByEpIndex (pTcd, (UINT8) (index & 0xFF));

    if ((NULL != pFuncDriver) &&
       (NULL != pFuncDriver->pFuncCallbackTable) &&
       (NULL != pFuncDriver->pFuncCallbackTable->synchFrameGet))
       {
       status = (pFuncDriver->pFuncCallbackTable->synchFrameGet)
                (pFuncDriver->pCallbackParam, 
                 pFuncDriver->targChannel,
                 index,
                 (pUINT16) &pTcd->dataBfr[0]);
 
       /* 
        * The best way is, do not scan all the function drivers.
        * just find the right one, and call the callback.
        */
       }
    
     if (ERROR == status)
        {
        USB_TGT_ERR("Process Synch Frame get request error\n",
                    1, 2, 3, 4, 5, 6); 
        
        return ERROR;
        }
     
    if (TRUE == usbTgtControlErpUsedFlagSet(pTcd))
        {
        /* Initialize the ERP for data phase */
    
        usbTgtInitDataErp (pTcd,
                           NULL, /* Common for all the function, set NULL */
                           &pTcd->dataBfr[0],    /* Data buffer */
                           2 ,                   /* Date length */
                           NULL,                 /* User callback */
                           TRUE,                 /* bDirIn */
                           USB_ERP_FLAG_NORMAL); /* erpFlags */
        
        /* Submit erp */
    
        status = usbTgtSubmitControlErp (pTcd);
        
        if ( status != OK )
            {
            usbTgtControlErpUsedFlagClear(pTcd);
            }
        }
    else
        status = ERROR;
    
    USB_TGT_DBG("usbTgtSynchFrameGet status %d\n",
                status, 2, 3, 4, 5, 6); 

    return (status);    
    }


/*******************************************************************************
*
* usbTgtSELSetCallback - process set descriptor standard request
*
* This routine processes set descriptor standard request.
*
* When this request received, this routine will parpare to receive the 
* descriptor.
*
* This is not supported in the current release.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO:N/A
*
* \NOMANUAL
*/

LOCAL VOID usbTgtSELSetCallback
    (
    pVOID pErpCallback   /* Pointer to ERP structure */
    )
    {
    pUSBTGT_TCD pTcd = NULL;
    pUSB_ERP    pErp = (pUSB_ERP)pErpCallback;

    /* Validate parameters */

    if ((NULL == pErp) || 
        (NULL == pErp->targPtr))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pErp) ? "pErp" :
                    "pErp->targPtr"), 2, 3, 4, 5, 6); 
        
        return;
        }
    
    /* Retrive the tcd from erp structure */

    pTcd = (pUSBTGT_TCD)pErp->targPtr;

    USB_TGT_DBG("usbTgtSetupErpCallback called with result 0x%X actLen 0x%X\n",
                pErp->result, 
                pErp->bfrList [0].actLen,
                3, 4, 5, 6);

    /* Here we get the systerm exit latency data */

    if ((NULL != pTcd->pTcdFuncs) &&
        (NULL != pTcd->pTcdFuncs->pTcdIoctl))
        {
        (void)(pTcd->pTcdFuncs->pTcdIoctl)(pTcd, 
                                           USB_REQ_SET_SEL, 
                                           (void *)pTcd->dataBfr);
        }
    }

/*******************************************************************************
*
* usbTgtSELSet - process set SEL standard request for USB 3.0.
*
* This routine processes set SEL standard request for USB 3.0.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO:N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtSELSet
    (
    pUSBTGT_TCD pTcd /* The TCD pointer */
    )
    {
    pUSB_SETUP pSetup = NULL;
    UINT16     length;
    STATUS     status = ERROR;
    
    /* Validate parameters */
    
    USBTGT_TCD_VALIDATION_RETURN_STATUS(pTcd);    

    USB_TGT_DBG("Received Set SEL request\n",
                1, 2, 3, 4, 5, 6); 

    pSetup = &pTcd->SetupPkt;
    length = FROM_LITTLEW (pSetup->length);

    if (TRUE == usbTgtControlErpUsedFlagSet(pTcd))
        {
        /* Re-initialize the defaultControlErp and update it */
    
        usbTgtInitDataErp (pTcd ,
                           NULL, /* Common for all the function, set NULL */
                           &pTcd->dataBfr[0],    /* Data buffer */
                           length ,              /* Date length */
                           usbTgtSELSetCallback, /* User callback */
                           FALSE,                /* bDirIn */
                           USB_ERP_FLAG_NORMAL); /* erpFlags */
        
        /* submit the erp */
    
        if ((status = usbTgtSubmitControlErp (pTcd)) != OK)
            {
            usbTgtControlErpUsedFlagClear(pTcd);
            }
        }

    return status;
    }

/*******************************************************************************
*
* usbTgtWcidCompatibleIDHandler - processes a wcid compatible ID specific setup packet
*
* This routine is called when a wcid compatible ID specific request is received from the host. 
* reference to https://github.com/pbatard/libwdi/wiki/WCID-Devices
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtWcidCompatibleIDHandler
    (
    pUSBTGT_TCD pTcd /* The TCD pointer */
    )
    {
    NODE *              pNode = NULL;
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;
    STATUS     status = ERROR;
    UINT8      uRecipient;
    UINT16     uIndex;
    int        i;
    int        count;
    UINT8 *    pBuf = NULL;
    UINT16     actLen;  
    uIndex = FROM_LITTLEW(pTcd->SetupPkt.index);
    uRecipient = (UINT8) (pTcd->SetupPkt.requestType & USB_RT_RECIPIENT_MASK);

    USB_TGT_DBG("usbTgtWcidCompatibleIDHandler: 0x%x 0x%x 0x%x\n",
                pTcd->SetupPkt.requestType & USB_RT_VENDOR,
                pTcd->SetupPkt.request,
                uIndex,4,5,6);
					
    count = lstCount (&pTcd->funcList);
	for (i = 1; i <= count; i ++)
        {
        pNode = lstNth (&pTcd->funcList, i);
        pFuncDriver = TCD_LIST_NODE_TO_USBTGT_FUNC(pNode);
        if( (pFuncDriver != NULL) && 
            (pFuncDriver->pWcidString != NULL) && 
            (pFuncDriver->pSubWcidString != NULL) && 
   			(pTcd->SetupPkt.requestType & USB_RT_VENDOR) && 
   		    (pTcd->SetupPkt.request == pFuncDriver->uWcidVc) && 
            (pTcd->SetupPkt.index == USBTGT_OS_EXT_COMP_STRING_INDEX)
            )
            {
            USB_TGT_DBG("usbTgtWcidCompatibleIDHandler: deal with"
                        "Microsoft Extended Compat ID OS Feature Descriptor\n",
                        1, 2, 3, 4, 5, 6);
				
            /* Microsoft Extended Compat ID OS Feature Descriptor
                                  received, construct response packet */		
                                  
            pBuf = &pTcd->dataBfr[0];
            OS_MEMSET (pBuf, 0, USB_MAX_DESCR_LEN);                
			
            /* Packet len DWORD (LE)  */

            pBuf[0] = 0x28;
			
            /* Version ('1.0') */
                 
            pBuf[4] = 0x0;
            pBuf[5] = 0x1;

            /* Compatibility ID Descriptor index  */
                 
            pBuf[6] = USBTGT_OS_EXT_COMP_STRING_INDEX;
            pBuf[7] = 0;

			/* Number of sections  */
            pBuf[8] = 0x1;

            /* Reserved */

            pBuf[17] = 1;
                 
            /* ASCII String Compatible ID   */

            strncpy(&pBuf[18], pFuncDriver->pWcidString, 8);
                    
            /* ASCII String SubCompatible ID */
                    
            strncpy(&pBuf[26], pFuncDriver->pSubWcidString, 8);

            if (TRUE == usbTgtControlErpUsedFlagSet(pTcd))
                {
                /* Initialize the ERP for data phase */
			
                usbTgtInitDataErp (pTcd ,
                                   NULL,
                                   &pTcd->dataBfr[0],	 /* Data buffer */
                                   pTcd->SetupPkt.length,/* Date length */
                                   NULL,				 /* User callback */
                                   TRUE,				 /* bDirIn */
                                   USB_ERP_FLAG_NORMAL); /* erpFlags */
			
                /* Submit erp */
			
                status = usbTgtSubmitControlErp ( pTcd );
                if ( status != OK )
                    {
                    USB_TGT_ERR("usbTgtUnStandardRequestHandler: submit Microsoft Extended Compat"
                                "ID OS Feature Descriptor error, status=0X%x\n",
                                status, 2, 3, 4, 5, 6);
                    usbTgtControlErpUsedFlagClear(pTcd);
                    }
                }	
            break;
            }
        }
	return status;
    }

/*******************************************************************************
*
* usbTgtUnStandardRequestHandler - processes a vendor/class specific setup packet
*
* This routine is called when any un-standard(vendor/class) specific request 
* is received from the host. The routine will forward this request to all the 
* function drivers. The right function driver who specify this packet will 
* deal with it.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtUnStandardRequestHandler
    (
    pUSBTGT_TCD pTcd /* The TCD pointer */
    )
    {
    NODE *              pNode = NULL;
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;
    STATUS     status = ERROR;
    UINT8      uRecipient;
    UINT16     uIndex;
    int        i;
    int        count;

    /* Validate parameters */
    
    USBTGT_TCD_VALIDATION_RETURN_STATUS(pTcd);    
    
    /* This is class or vendor request, foward to the funtion level to handle it */

     
    /* Find the right function driver */

    /* 
     * From the USB 2.0 Specification Chapter 9.3.4 wIndex
     *
     * The contents of this field vary according to the request. It is used to 
     * pass a parameter to the device, specif to the request.
     *
     * The wIndex field is often used in requests to specify an endpoint or an 
     * interface. 
     * So, this routine will check the recipient of the request. 
     * For ENDPOINT or INTERFACE, find the right function driver
     * or this vendor/class request need be pass though to all the function 
     * drivers listed on the TCD function driver list.
     */

    uIndex = FROM_LITTLEW(pTcd->SetupPkt.index);

    uRecipient = (UINT8) (pTcd->SetupPkt.requestType & USB_RT_RECIPIENT_MASK);
    
    switch (uRecipient)
        {
        case USB_RT_INTERFACE:            
            pFuncDriver = usbTgtFindFuncByInterface(pTcd, (UINT8)(uIndex & 0xFF));
            break;
        case USB_RT_ENDPOINT:            
            pFuncDriver = usbTgtFindFuncByEpIndex(pTcd, (UINT8) (uIndex & 0xFF)); 
            break;
        case USB_RT_DEVICE:
            return usbTgtWcidCompatibleIDHandler( pTcd );
            break;
        case USB_RT_OTHER:
        default:
            break;
        }

    if (NULL != pFuncDriver)
        {
        /* 
         * It must be the ENDPOINT or INTERFACE recipient, and the 
         * right function driver be found.
         */
         
        if ((pTcd->uCurrentConfig == pFuncDriver->uCurrentConfig) &&
            (NULL != pFuncDriver->pFuncCallbackTable) &&
            (NULL != pFuncDriver->pFuncCallbackTable->vendorSpecific))
            {
            status = (pFuncDriver->pFuncCallbackTable->vendorSpecific)
                     (pFuncDriver->pCallbackParam, 
                      pFuncDriver->targChannel,
                      pTcd->SetupPkt.requestType,
                      pTcd->SetupPkt.request,
                      FROM_LITTLEW(pTcd->SetupPkt.value),
                      FROM_LITTLEW(pTcd->SetupPkt.index),
                      FROM_LITTLEW(pTcd->SetupPkt.length));
            
            return status;
            }

        }
    else
        {
        if ((uRecipient != USB_RT_INTERFACE) &&
            (uRecipient != USB_RT_ENDPOINT))
            {

            count = lstCount (&pTcd->funcList);

            for (i = 1; i <= count; i ++)
                {
                pNode = lstNth (&pTcd->funcList, i);
                pFuncDriver = TCD_LIST_NODE_TO_USBTGT_FUNC(pNode);

                if ((NULL != pFuncDriver) &&
                    (pTcd->uCurrentConfig == pFuncDriver->uCurrentConfig) &&
                    (NULL != pFuncDriver->pFuncCallbackTable) &&
                    (NULL != pFuncDriver->pFuncCallbackTable->vendorSpecific))
                    {
                    status = (pFuncDriver->pFuncCallbackTable->vendorSpecific)
                             (pFuncDriver->pCallbackParam, 
                              pFuncDriver->targChannel,
                              pTcd->SetupPkt.requestType,
                              pTcd->SetupPkt.request,
                              FROM_LITTLEW(pTcd->SetupPkt.value),
                              FROM_LITTLEW(pTcd->SetupPkt.index),
                              FROM_LITTLEW(pTcd->SetupPkt.length));
          
                    /* 
                     * When the first function driver recongized the class or 
                     * vendor request and responsed, return OK.
                     */
                     
                    if (OK == status)
                        {
                        return OK;
                        }
                    }
                }
            }
        }

    return status;
    }

/*******************************************************************************
*
* usbTgtControlRequestHandler - parse/execute a control pipe request
*
* This routine is called when one setup packet with the success erp result. This
* routine will validate the received request to parse and execute it.
*
* NOTE: All the control requests will be processed first on the low level TCD.
* and call the callback rotine to inform the high level to know the request
* receive. <callbackFlag> is the parameter to indicate the next step to schedule
* the stages.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO:N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtControlRequestHandler
    (
    pUSBTGT_TCD pTcd /* The TCD pointer */
    )
    {
    pUSB_SETUP pSetup = NULL;  /* USB_SETUP buffer */
    UINT16     status = 0;     /* state of endpoint */

    /* Validate the setup packet */

    if ((NULL == pTcd) ||        
        (NULL == pTcd->pTcdFuncs) ||
        (NULL == pTcd->pTcdFuncs->pTcdPipeStatusGet) ||
        (NULL == pTcd->pTcdFuncs->pTcdPipeStatusSet) ||
        (pTcd->controlErp.bfrList [0].actLen != sizeof (USB_SETUP)))
        {
        USB_TGT_ERR("Invalid parameter %s len %d\n",
                    ((NULL == pTcd) ? "pTcd is NULL" :
                     (NULL == pTcd->pTcdFuncs) ? "pTcdFuncs" :
                     (NULL == pTcd->pTcdFuncs->pTcdPipeStatusGet) ? 
                     "pTcdPipeStatusGet" :
                     (NULL == pTcd->pTcdFuncs->pTcdPipeStatusSet) ? 
                     "pTcdPipeStatusSet" :
                     "actLen !=  8"),
                     ((NULL == pTcd) ? 0 :
                       pTcd->controlErp.bfrList[0].actLen),3, 4, 5, 6); 

        return ERROR;
        }


    /*
     * Check if default control endpoints are stalled,
     * if so, clear the stall conditions
     * Infect, for many usb target controller, even it stalled, it 
     * still will ack the setup packet when a new valid setup packet
     * received.
     */

    if ((pTcd->pTcdFuncs->pTcdPipeStatusGet) 
        (pTcd, pTcd->controlPipe, &status) != OK)
        {
        USB_TGT_ERR("pTcdPipeStatusGet return ERROR\n",
                    1, 2, 3, 4, 5, 6); 
        
        return ossStatus (S_usbTgtTcdLib_TCD_FAULT);
        }

    status = FROM_LITTLEW (status);

    if (status == USB_ENDPOINT_STS_HALT) 
        {
        USB_TGT_DBG("pTcdPipeStatusGet status stall, unstall it\n",
                    1, 2, 3, 4, 5, 6); 
        
        /* Endpoint 0 is stalled, unstall it */
        
        if ((pTcd->pTcdFuncs->pTcdPipeStatusSet) 
            (pTcd, pTcd->controlPipe, USBTGT_PIPE_STATUS_UNSTALL) != OK)
            {
            USB_TGT_ERR("pTcdPipeStatusSet set control pipe unstall fail\n",
                        1, 2, 3, 4, 5, 6); 
            return ossStatus (S_usbTgtTcdLib_TCD_FAULT);
            }
        }

    pSetup = &pTcd->SetupPkt;
    
    USB_TGT_VDBG("usbTgtControlRequestHandler %p %p %p %p %p\n",
                 pSetup->requestType,         
                 pSetup->request,              
                 FROM_LITTLEW(pSetup->value),
                 FROM_LITTLEW(pSetup->index),
                 FROM_LITTLEW(pSetup->length),
                 6);

    /* Execute based on the type of request. */

    if ((pSetup->requestType & USB_RT_CATEGORY_MASK) == USB_RT_STANDARD)
        {
        switch (pSetup->request)
            {
            case USB_REQ_CLEAR_FEATURE:
                return usbTgtFeatureClear (pTcd);

            case USB_REQ_SET_FEATURE:
                return usbTgtFeatureSet (pTcd);

            case USB_REQ_GET_CONFIGURATION:
                return usbTgtConfigurationGet (pTcd);

            case USB_REQ_SET_CONFIGURATION:
                return usbTgtConfigurationSet (pTcd);

            case USB_REQ_GET_DESCRIPTOR:
                return usbTgtDescriptorGet (pTcd);

            case USB_REQ_SET_DESCRIPTOR:
                return usbTgtDescriptorSet (pTcd);

            case USB_REQ_GET_INTERFACE:
                return usbTgtInterfaceGet (pTcd);

            case USB_REQ_SET_INTERFACE:
                return usbTgtInterfaceSet (pTcd);

            case USB_REQ_GET_STATUS:
                return usbTgtStatusGet (pTcd);

            case USB_REQ_SET_ADDRESS:
                return usbTgtAddressSet (pTcd);

            case USB_REQ_GET_SYNCH_FRAME:
                return usbTgtSynchFrameGet (pTcd);
            case USB_REQ_SET_SEL:
                return usbTgtSELSet(pTcd);
            case 49:
                return OK;
            default:
                return ERROR;
            }
        }
    else
        {
        USB_TGT_DBG("not USB_RT_STANDARD\n",
                    1, 2, 3, 4, 5, 6); 
        return usbTgtUnStandardRequestHandler (pTcd);
        }
    }

/*******************************************************************************
*
* usbTgtControlResponseSend - send data to host on the control pipe
*
* This routine sends data to host on the control pipe. It is used by the 
* function drivers which need send data to the host by the control endpoint 
* zero. It can be used to response the standard or unstandard requests.
*
* NOTE: 
* <'targChannel'> should be indicate to the pUSBTGT_FUNC_DRIVER pointer
* <'bfrLen'> is the length of the data to be send
* <'pBfr'> is the pointer of the buffer to store the data
* <'erpFlags'> indicates the ERP transaction flags, such as whether one ZLP 
* pecket will be needed after all the data transfered.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO: N/A
*/

STATUS usbTgtControlResponseSend
    (
    USB_TARG_CHANNEL targChannel, /* Target channel */
    UINT16           bfrLen,      /* Length of response 0 */
    pUINT8           pBfr,        /* Pointer to the data buffer */
    USB_ERP_FLAGS    erpFlags     /* The ERP transaction flags */
    )
    {
    pUSBTGT_FUNC_DRIVER pFuncDriver = usbTgtTargChannelToFuncDriver(targChannel);
    pUSBTGT_TCD         pTcd = NULL;
    STATUS              status = ERROR;
    
    /* Validate the endpoint descriptor */

    if ((NULL == pFuncDriver) ||
        (NULL == pFuncDriver->pTcd) ||
        ((NULL == pBfr) && (0 != bfrLen)))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pFuncDriver) ? "targChannel" :
                    (NULL == pFuncDriver->pTcd) ? "pFuncDriver->pTcd" :
                    "pBfr and bfrLen"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Is this supported the new driver */
    
    pTcd = pFuncDriver->pTcd;

    if (TRUE == usbTgtControlErpUsedFlagSet(pTcd))
        {
        /* Copy pBfr tp Targ_Tcd. dataBfr */

		if(bfrLen>USBTGT_MAX_CONTORL_BUF_SIZE)
			{
			USB_TGT_ERR("Please set USBTGT_MAX_CONTORL_BUF_SIZE bigger than 0x%x \n",
	  	  	 	                    bfrLen, 2, 3, 4, 5, 6);
			return ERROR;
			}
        OS_MEMCPY(&pTcd->dataBfr[0], pBfr, bfrLen);
    
        /* Re-initialize the defaultControlErp and update it */
    
        usbTgtInitDataErp (pTcd ,
                           pFuncDriver->pFuncSpecific, /* Specific for the function driver */
                           &pTcd->dataBfr[0], /* Data buffer */
                           bfrLen ,           /* Date length */
                           NULL,              /* User callback */
                           TRUE,              /* bDirIn */
                           erpFlags);         /* erpFlags */
    
        /* submit the erp */
    
        if ((status = usbTgtSubmitControlErp(pTcd)) != OK)
             usbTgtControlErpUsedFlagClear(pTcd);
        }
    else
        status = ERROR;

    return status;
    }

/*******************************************************************************
*
* usbTgtControlPayloadRcv - receive data on the default control pipe
*
* This routine receives data on the default control pipe from the host. It is 
* used by the function drivers which need receive data from the host by the 
* control endpoint zero. It can be used to response the standard or unstandard 
* requests.
*
* NOTE: 
* <targChannel> should be indicate to the pUSBTGT_FUNC_DRIVER pointer
* <bfrLen> is the length of the data to be recived
* <pBfr> is the pointer of the buffer to store the data
* <userCallback> is the callback routine which will be called after all the 
* data transfered.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO: N/A
*/

STATUS usbTgtControlPayloadRcv
    (
    USB_TARG_CHANNEL targChannel,    /* Target channel */
    UINT16           bfrLen,         /* Length of data to be received */
    pUINT8           pBfr,           /* Pointer to buffer */
    ERP_CALLBACK     userCallback    /* USB Target Applcaition Callback */
    )
    {
    pUSBTGT_FUNC_DRIVER pFuncDriver = usbTgtTargChannelToFuncDriver(targChannel);
    pUSBTGT_TCD         pTcd = NULL;
    STATUS              status = ERROR;
    
    /* Validate the endpoint descriptor */

    if ((NULL == pFuncDriver) ||
        (NULL == pFuncDriver->pTcd) ||
        ((NULL == pBfr) && (0 != bfrLen)))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pFuncDriver) ? "targChannel" :
                    (NULL == pFuncDriver->pTcd) ? "pFuncDriver->pTcd" :
                    "pBfr and bfrLen"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    pTcd = pFuncDriver->pTcd;

    if (TRUE == usbTgtControlErpUsedFlagSet(pTcd))
        {

        /* Re-initialize the defaultControlErp and update it */
    
        usbTgtInitDataErp (pTcd,
                           pFuncDriver->pFuncSpecific,/* Specific for the function driver */
                           pBfr,                 /* Data buffer */
                           bfrLen,               /* Date length */
                           userCallback,         /* User callback */
                           FALSE,                /* bDirIn */
                           USB_ERP_FLAG_NORMAL); /* erpFlags */
        
        /* submit the erp */
    
        if ((status = usbTgtSubmitControlErp (pTcd)) != OK)
            {
            usbTgtControlErpUsedFlagClear(pTcd);
            }
        }

    return status;
    }

/*******************************************************************************
*
* usbTgtControlStatusSend - send control transfer status to the host
*
* This routine is used to issue the status stage to the host. It will be called 
* by the function drivers, and used in some normal case which is for some
* hardware controller to specific the STATUS stage. <bStatusIn> is the parameter
* to indicate the direction STATUS IN/OUT.
* 
* NOTE: 
* <targChannel> should be indicate to the pUSBTGT_FUNC_DRIVER pointer
*
* <bStatusIn> indicates the direction of the STATUS stage , STATUS IN/OUT
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO: N/A
*/

STATUS usbTgtControlStatusSend
    (
    USB_TARG_CHANNEL targChannel,    /* Target channel */
    BOOL             bStatusIn       /* Is status in */
    )
    {
    pUSBTGT_FUNC_DRIVER pFuncDriver = usbTgtTargChannelToFuncDriver(targChannel);
    pUSBTGT_TCD         pTcd = NULL;
    STATUS              status = ERROR;
    
    /* Validate the endpoint descriptor */

    if ((NULL == pFuncDriver) ||
        (NULL == pFuncDriver->pTcd))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pFuncDriver) ? "targChannel" :
                    "pFuncDriver->pTcd"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    pTcd = pFuncDriver->pTcd;

    /* One channel indicate one function device */
    
    if (TRUE == usbTgtControlErpUsedFlagSet(pTcd))
        {

        /* Re-initialize the defaultControlErp and update it */
    
        usbTgtInitStatusErp(pTcd, bStatusIn);
    
        /* submit the erp */
    
        if ((status = usbTgtSubmitControlErp(pTcd))!= OK)
             usbTgtControlErpUsedFlagClear(pTcd);
        }

    return status;
    }

/*******************************************************************************
*
* usbTgtControlErpUsedFlagSet - set the control ERP pending flag
*
* This routine sets the control ERP pending flag to be 'TRUE'. It will check
* whether the 'controlErpUsed' flag has been set 'TRUE' or not. If yes, that
* means the control channel still used and the routine will return 'FALSE'. Or,
* the routine will set the flag to 'TRUE' and return 'TRUE' as the return value.
*
* NOTE: The caller should ensure the valid of the paramater
*
* RETURNS: TRUE of FALSE if the flag still be set.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOL usbTgtControlErpUsedFlagSet
    (
    pUSBTGT_TCD pTcd  /* The TCD pointer */
    )
    {
    /* The caller should ensure the valid of the paramater */

    return (!(vxAtomicSet(&pTcd->controlErpUsed, TRUE)));
    }
    
/*******************************************************************************
*
* usbTgtControlErpUsedFlagClear - clear the ERP pending flag
*
* This routine clears the ERP pending flag 'controlErpUsed' to indicate the 
* control pipe do not occupied and can be used now.
*
* NOTE: The caller should ensure the valid of the paramater
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbTgtControlErpUsedFlagClear
    (
    pUSBTGT_TCD pTcd  /* The TCD pointer */
    )
    {
    
    /* The caller should ensure the valid of the paramater */

    (void)(vxAtomicSet(&pTcd->controlErpUsed, FALSE));

    return;
    }

