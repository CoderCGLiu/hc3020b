/* usbTgtVrtlEnd.c - The USB Virtual END Agent Module for USB Target Function Driver */

/*
 * Copyright (c) 2011-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
Modification history
--------------------
01j,06may13,s_z  Remove compiler warning (WIND00356717)
01i,04jan13,s_z  Remove compiler warning (WIND00390357)
01h,06jun12,ljg  Check pointer pUsbTgtVrtlEnd before use (WIND00352953)
01g,15may12,s_z  Correct coverity warning 
01f,06jun12,ljg  Check pointer pUsbTgtVrtlEnd before use (WIND00352953)
01e,12jul11,s_z  Remove unused elements
01d,28mar11,s_z  Check usbTgtVrtlEndMCastAddrAdd/usbTgtVrtlEndMCastAddrDel
                 routines return value(WIND00259940)
01c,22mar11,s_z  Code clean up based on the code review
01b,23feb11,s_z  Return sub-routine's result to the network stack
01a,04jan11,s_z  written
*/

/*
DESCRIPTION

This file includes the USB Virtual END device driver module which can be used
by the USB target function driver (such as RNDIS function driver).

This virtual END driver is designed by the referance of <'templateVxbEnd.c'>.
which is a vxBus END driver supported by the WRS network stack.

INCLUDE FILES:  vxWorks.h usb/usbOsal.h usb/usbOsalDebug.h usbTgtVrtlEnd.h
                
*/

/* includes */

#include <vxWorks.h>
#include <usb/usbOsal.h>
#include <usb/usbOsalDebug.h>
#include <usbTgtVrtlEnd.h>


/* defines */

#define USBTGT_VRTL_END_SPEED_1000M          (1000000000)
#define USBTGT_VRTL_END_SPEED_100M           (100000000)
#define USBTGT_VRTL_END_SPEED_10M            (10000000)

/* external */

IMPORT char * usrUsbTgtVrtlEndNameGet 
    (
    void
    );
IMPORT int usrUsbTgtVrtlEndSpeedGet 
    (
    int index
    );
IMPORT STATUS usrUsbTgtVrtlEndLocalMacAddrGet 
    (
    int    index,    /* The END device unit index */
    char * pMacAddr  /* Buffer to store the MAC address */
    );
IMPORT STATUS usrUsbTgtVrtlEndRemoteMacAddrGet 
    (
    int    index,    /* The END device unit index */
    char * pMacAddr  /* Buffer to store the MAC address */
    );
IMPORT STATUS usrUsbTgtVrtlEndRemoteMacAddrSet 
    (
    int    index,    /* The END device unit index */
    char * pMacAddr  /* Buffer to store the MAC address */
    );

/* locals */

/* forward declarations */

LOCAL STATUS usbTgtVrtlEndStart(END_OBJ*);
LOCAL STATUS usbTgtVrtlEndStop(END_OBJ*);
LOCAL STATUS usbTgtVrtlEndUnload(END_OBJ*);
LOCAL int    usbTgtVrtlEndIoctl(END_OBJ*, int, caddr_t);
LOCAL STATUS usbTgtVrtlEndSend(END_OBJ*, M_BLK_ID);
LOCAL int    usbTgtVrtlEndMCastAddrAdd(END_OBJ*, char*);
LOCAL int    usbTgtVrtlEndMCastAddrDel(END_OBJ*, char*);
LOCAL int    usbTgtVrtlEndMCastAddrGet(END_OBJ*, MULTI_TABLE*);
LOCAL END_OBJ * usbTgtVrtlEndLoad(char * loadStr,void * pArg);

LOCAL STATUS usbTgtVrtlEndPhyRead
    (
    VXB_DEVICE_ID pDev,
    UINT8         phyAddr,
    UINT8         regAddr,
    UINT16 *      dataVal
    );
LOCAL STATUS usbTgtVrtlEndPhyWrite
    (
    VXB_DEVICE_ID pDev,
    UINT8         phyAddr,
    UINT8         regAddr,
    UINT16        dataVal
    );
LOCAL STATUS usbTgtVrtlEndLinkUpdate
    (
    VXB_DEVICE_ID pDev
    );
LOCAL void usbTgtVrtlEndMuxConnect
    (
    VXB_DEVICE_ID pDev,
    void *        unused
    );
LOCAL STATUS usbTgtVrtlEndInstUnlink
    (
    VXB_DEVICE_ID pDev,
    void *        unused
    );
LOCAL STATUS usbTgtVrtlEndDataUnInit
    (
    struct vxbDev * pDev
    );

/*
 * Declare our function table of the vxBus virtual END driver
 */

LOCAL NET_FUNCS usbTgtVrtlEndNetFuncs =
    {
    (FUNCPTR) usbTgtVrtlEndStart,                        /* start func. */
    (FUNCPTR) usbTgtVrtlEndStop,                         /* stop func. */
    (FUNCPTR) usbTgtVrtlEndUnload,                       /* unload func. */
    (FUNCPTR) usbTgtVrtlEndIoctl,                        /* ioctl func. */
    (FUNCPTR) usbTgtVrtlEndSend,                         /* send func. */
    (FUNCPTR) usbTgtVrtlEndMCastAddrAdd,                 /* multicast add func. */
    (FUNCPTR) usbTgtVrtlEndMCastAddrDel,                 /* multicast delete func. */
    (FUNCPTR) usbTgtVrtlEndMCastAddrGet,                 /* multicast get fun. */
    (FUNCPTR) NULL,                     /* polling send func. */
    (FUNCPTR) NULL,                     /* polling receive func. */
    endEtherAddressForm,                /* put address info into a NET_BUFFER */
    endEtherPacketDataGet,              /* get pointer to data in NET_BUFFER */
    endEtherPacketAddrGet               /* Get packet addresses */
    };

/* End methods of the vxBus virtual END driver */

struct vxbDeviceMethod usbTgtVrtlEndMethods[] =
   {
   DEVMETHOD(miiRead,       usbTgtVrtlEndPhyRead),
   DEVMETHOD(miiWrite,      usbTgtVrtlEndPhyWrite),
   DEVMETHOD(miiMediaUpdate,usbTgtVrtlEndLinkUpdate),
   DEVMETHOD(muxDevConnect, usbTgtVrtlEndMuxConnect),
   DEVMETHOD(vxbDrvUnlink,  usbTgtVrtlEndInstUnlink),   
   { 0, 0 }
   };   

/*******************************************************************************
*
* usbTgtVrtlEndPhyRead - miiBus miiRead method
*
* This routine implements an miiRead() method that allows PHYs
* on the miiBus to access our MII management registers. 
* Since this is used for the virtual END driver, we always return the some
* status data to the miiBus monitor. 
*
* RETURNS: OK, or ERROR if invalid PHY addr or register is specified
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtVrtlEndPhyRead
    (
    VXB_DEVICE_ID pDev,
    UINT8         phyAddr,
    UINT8         regAddr,
    UINT16 *      dataVal
    )
    {
    pUSBTGT_VRTL_END pUsbTgtVrtlEnd = NULL;

    /* Check if the parameters are valid */
    
    if ((NULL == pDev) ||
        (NULL == dataVal) ||
        (NULL == pDev->pDrvCtrl))
        {
        USBTGT_END_ERR("Invalid parameter %s is NULL\n",
                       ((NULL == pDev) ? "pDev" :
                       "pDev->pDrvCtrl"), 2, 3, 4, 5, 6);
        
        return ERROR;
        }

    pUsbTgtVrtlEnd = (pUSBTGT_VRTL_END)pDev->pDrvCtrl;

    /* 
     * The max available phy address is 32,
     * if needed, reroute the phyAddr
     */
    
    if (phyAddr > 31)
        {
        phyAddr = (UINT8) (phyAddr % 32);
        }

    /* Only one phy address is accepted */
    
    if (phyAddr != pUsbTgtVrtlEnd->dumyMiiPhyAddr)
        {
        *dataVal = 0xFFFF;
        return (ERROR);
        }
    
    USBTGT_END_DBG ("usbTgtVrtlEndPhyRead phyAddr 0x%X regAddr 0x%X\n", 
                     phyAddr,regAddr,3,4,5,6);
  
    switch (regAddr)
        {
        case MII_CTRL_REG:
             {
             * dataVal = 0;

             if(pUsbTgtVrtlEnd->uEndfullDuplex == 1)
                {
                *dataVal |= MII_CR_FDX;
                }
             
             if(pUsbTgtVrtlEnd->uEndSpeed == 1000)
                {
                *dataVal |= (MII_CR_100 | MII_CR_1000);
                }
             else if(pUsbTgtVrtlEnd->uEndSpeed == 100)
                {
                *dataVal |= MII_CR_100;
                }
            }
             break;
        case MII_STAT_REG:
             {
             *dataVal = MII_SR_LINK_STATUS | MII_SR_AUTO_NEG ;
             }
             break;
        case MII_PHY_ID1_REG:
             {
             *dataVal = 0;
             }
             break;
        case MII_PHY_ID2_REG:
             {
             *dataVal = 0;
             }
             break;
        case MII_MASSLA_STAT_REG:
             {
             *dataVal = MII_MASSLA_STAT_LOCAL_RCV | MII_MASSLA_STAT_REMOTE_RCV;
             }
             break;
        default:
             {
             *dataVal = 0xFFFF;
             }
             return (ERROR);                
        }

    USBTGT_END_DBG ("usbTgtVrtlEndPhyRead phyAddr 0x%X regAddr 0x%X with "
                    "value 0x%X\n", 
                    phyAddr, regAddr, * dataVal, 4, 5, 6);

    return (OK);
    }

/*******************************************************************************
*
* usbTgtVrtlEndPhyWrite - miiBus miiWrite method
*
* This routine implements an miiWrite() method that allows PHYs
* on the miiBus to access our MII management registers. 
*
* RETURNS: OK, or ERROR if invalid PHY addr or register is specified
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtVrtlEndPhyWrite
    (
    VXB_DEVICE_ID pDev,
    UINT8         phyAddr,
    UINT8         regAddr,
    UINT16        dataVal
    )
    {
    pUSBTGT_VRTL_END pUsbTgtVrtlEnd = NULL;
    
    /* Check if the parameters are valid */
    
    if ((NULL == pDev) ||
        (NULL == pDev->pDrvCtrl))
        {
        USBTGT_END_ERR("Invalid parameter %s is NULL\n",
                       ((NULL == pDev) ? "pDev" :
                       "pDev->pDrvCtrl"), 2, 3, 4, 5, 6);
        
        return ERROR;
        }
    
    pUsbTgtVrtlEnd = (pUSBTGT_VRTL_END)pDev->pDrvCtrl;

    /* 
     * The max available phy address is 32,
     * if needed, reroute the phyAddr
     */

    if (phyAddr > 31)
        phyAddr = (UINT8) (phyAddr % 32);
    
    if (phyAddr != pUsbTgtVrtlEnd->dumyMiiPhyAddr)
        {
        USBTGT_END_WARN("phyAddr != pUsbTgtVrtlEnd->dumyMiiPhyAddr\n",
                        1, 2, 3, 4, 5, 6);
        
        return ERROR;
        }

    /* TODO : add interface to configure the dumy MII and dumy phy */
    
    USBTGT_END_DBG ("usbTgtVrtlEndPhyWrite phyAddr 0x%X regAddr 0x%X valus 0x%X\n", 
                     phyAddr, regAddr, dataVal, 4, 5, 6);

    return (OK);
    }

/*****************************************************************************
*
* usbTgtVrtlEndLinkUpdate - miiBus miiLinkUpdate method
*
* This routine implements an miiLinkUpdate() method that allows
* miiBus to notify us of link state changes. This routine will be
* invoked by the miiMonitor task when it detects a change in link
* status. Normally, the miiMonitor task checks for link events every
* two seconds. 
*
* Once we determine the new link state, we will announce the change
* to any bound protocols via muxError(). We also update the ifSpeed
* fields in the MIB2 structures so that SNMP queries can detect the
* correct link speed.
*
* RETURNS: OK, or ERROR if obtaining the new media setting fails
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtVrtlEndLinkUpdate
    (
    VXB_DEVICE_ID pDev
    )
    {
    pUSBTGT_VRTL_END pUsbTgtVrtlEnd = NULL;
    UINT32          oldStatus;
    
    /* Check if the parameters are valid */
    
    if ((NULL == pDev) ||
        (NULL == pDev->pDrvCtrl))
        {
        USBTGT_END_ERR("Invalid parameter %s is NULL\n",
                       ((NULL == pDev) ? "pDev" :
                       "pDev->pDrvCtrl"), 2, 3, 4, 5, 6);
        
        return ERROR;
        }
    
    pUsbTgtVrtlEnd = (pUSBTGT_VRTL_END)pDev->pDrvCtrl;

    if (pUsbTgtVrtlEnd->dumyMiiBus == NULL)
        {
        USBTGT_END_ERR("Invalid parameter %s is NULL\n",
                       ("pUsbTgtVrtlEnd->dumyMiiBus"),
                       2, 3, 4, 5, 6);
        
        return (ERROR);
        }
    
    semTake (pUsbTgtVrtlEnd->endMutex, WAIT_FOREVER);
    
    oldStatus = pUsbTgtVrtlEnd->uCurStatus;

    if (miiBusModeGet(pUsbTgtVrtlEnd->dumyMiiBus,
                      &pUsbTgtVrtlEnd->uCurMedia, 
                      &pUsbTgtVrtlEnd->uCurStatus) == ERROR)
        {
        semGive (pUsbTgtVrtlEnd->endMutex);

        return (ERROR);
        }

    if (!(pUsbTgtVrtlEnd->EndObj.flags & IFF_UP))
        {
        semGive (pUsbTgtVrtlEnd->endMutex);
        return (OK);
        }
    
    /* If status went from down to up, announce link up. */

    if ((pUsbTgtVrtlEnd->uCurStatus & IFM_ACTIVE) && 
         !(oldStatus & IFM_ACTIVE)) 
        {
        if (IFM_SUBTYPE(pUsbTgtVrtlEnd->uCurMedia) == IFM_100_TX)
            {
            /* If the speed is IFM_100_TX */
            
            pUsbTgtVrtlEnd->EndObj.mib2Tbl.ifSpeed = USBTGT_VRTL_END_SPEED_100M;
            }
        else
            {
            /* IFM_10_TX */
            
            pUsbTgtVrtlEnd->EndObj.mib2Tbl.ifSpeed = USBTGT_VRTL_END_SPEED_10M;
            }
        
        if (pUsbTgtVrtlEnd->EndObj.pMib2Tbl != NULL)
            {
            pUsbTgtVrtlEnd->EndObj.pMib2Tbl->m2Data.mibIfTbl.ifSpeed =
                pUsbTgtVrtlEnd->EndObj.mib2Tbl.ifSpeed;
            }
        
        jobQueueStdPost (pUsbTgtVrtlEnd->endJobQueue,
                         NET_TASK_QJOB_PRI,
                         muxLinkUpNotify, 
                         &pUsbTgtVrtlEnd->EndObj,
                         NULL, NULL, NULL, NULL);
        }

    semGive (pUsbTgtVrtlEnd->endMutex);

    return (OK);
    }

/*****************************************************************************
*
* usbTgtVrtlEndMuxConnect - muxConnect method handler
*
* This routine handles muxConnect() events, which may be triggered
* manually or (more likely) by the bootstrap code. Most VxBus
* initialization occurs before the MUX has been fully initialized,
* so the usual muxDevLoad()/muxDevStart() sequence must be defered
* until the networking subsystem is ready. This routine will ultimately
* trigger a call to usbTgtVrtlEndLoad() to create the END interface instance.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbTgtVrtlEndMuxConnect
    (
    VXB_DEVICE_ID pDev,
    void *        unused
    )
    {
    pUSBTGT_VRTL_END  pUsbTgtVrtlEnd = NULL;

    /* Check if the parameters are valid */
    
    if ((NULL == pDev) ||
        (NULL == pDev->pDrvCtrl))
        {
        USBTGT_END_ERR("Invalid parameter %s is NULL\n",
                       ((NULL == pDev) ? "pDev" :
                       "pDev->pDrvCtrl"), 2, 3, 4, 5, 6);
        
        return;
        }

    pUsbTgtVrtlEnd = (pUSBTGT_VRTL_END)pDev->pDrvCtrl;

    /* Save the cookie. */

    pUsbTgtVrtlEnd->pMuxDevCookie = muxDevLoad (pDev->unitNumber,
                                               usbTgtVrtlEndLoad, "", 
                                               TRUE, 
                                               pDev);

    if (pUsbTgtVrtlEnd->pMuxDevCookie != NULL)
        {
        USBTGT_END_DBG ("usbTgtVrtlEndMuxConnect Dev start \n", 
                        1, 2, 3, 4, 5, 6);
        
        muxDevStart (pUsbTgtVrtlEnd->pMuxDevCookie);
        }

    USBTGT_END_DBG ("usbTgtVrtlEndMuxConnect done \n", 
                    1, 2, 3, 4, 5, 6);

    return;
    }

/*****************************************************************************
*
* usbTgtVrtlEndInstUnlink -  VxBus unlink handler
*
* This routine shuts down the USB virtual END device instance in response to an
* unlink event from VxBus. This may occur if our VxBus instance has
* been terminated, or if the USB virtual END driver has been unloaded. When an
* unlink event occurs, we must shut down and unload the END interface
* associated with this device instance and then release all the
* resources allocated during instance creation, We also must destroy our
* child miiBus and PHY devices.
*
* RETURNS: OK if device was successfully destroyed, otherwise ERROR
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtVrtlEndInstUnlink
    (
    VXB_DEVICE_ID pDev,
    void *        unused
    )
    { 
    pUSBTGT_VRTL_END pUsbTgtVrtlEnd = NULL;
    STATUS          status = ERROR;

    /* Check if the parameters are valid */
    
    if ((NULL == pDev) ||
        (NULL == pDev->pDrvCtrl))
        {
        USBTGT_END_ERR("Invalid parameter %s is NULL\n",
                       ((NULL == pDev) ? "pDev" :
                       "pDev->pDrvCtrl"), 2, 3, 4, 5, 6);
        
        return ERROR;
        }
    
    pUsbTgtVrtlEnd = pDev->pDrvCtrl;

    /*
     * Stop the device and detach from the MUX.
     * Note: it's possible someone might try to delete
     * us after our vxBus instantiation has completed,
     * but before anyone has called our muxConnect method.
     * In this case, there'll be no MUX connection to
     * tear down, so we can skip this step.
     */

    if (pUsbTgtVrtlEnd->pMuxDevCookie != NULL)
        {
        if (muxDevStop (pUsbTgtVrtlEnd->pMuxDevCookie) != OK)
            {
            USBTGT_END_ERR("Stop the virtual END error\n",
                           1, 2, 3, 4, 5, 6);
                           
            return (ERROR);
            }
        
        /* Detach from the MUX. */

        if (muxDevUnload (usrUsbTgtVrtlEndNameGet(), pDev->unitNumber) != OK)
            {
            USBTGT_END_ERR("Unload the virtual END error\n",
                           1, 2, 3, 4, 5, 6);

            return (ERROR);
            }
        }

    status = usbTgtVrtlEndDataUnInit(pDev);

    return (status);
    }


/*******************************************************************************
*
* usbTgtVrtlEndStart - start the device
* 
* This routine resets the device to put it into a known state and
* then configures it for RX and TX operation and the initial
* link state is configured.
*
* Note that this routine also checks to see if an alternate jobQueue
* has been specified via the vxbParam subsystem. This allows the driver
* to divert its work to an alternate processing task, such as may be
* done with TIPC. This means that the jobQueue can be changed while
* the system is running, but the device must be stopped and restarted
* for the change to take effect.
*
*
* RETURNS: OK, or ERROR if there is something wrong
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtVrtlEndStart
    (
    END_OBJ * pEnd
    )
    {
    pUSBTGT_VRTL_END pUsbTgtVrtlEnd = (pUSBTGT_VRTL_END)pEnd;

    if (NULL == pUsbTgtVrtlEnd)
        {
        return ERROR;
        }
    
    /* Initialize job queues */

    semTake(pUsbTgtVrtlEnd->endMutex, WAIT_FOREVER);

    END_TX_SEM_TAKE (pEnd, WAIT_FOREVER);

    if (pEnd->flags & IFF_UP)
        {
        END_TX_SEM_GIVE (pEnd);
        semGive (pUsbTgtVrtlEnd->endMutex);
        return (OK);
        }

    pUsbTgtVrtlEnd->endJobQueue = netJobQueueId;


    pUsbTgtVrtlEnd->uCurMedia = IFM_ETHER | IFM_NONE;
    pUsbTgtVrtlEnd->uCurStatus = IFM_AVALID;

    /* Set initial link state */
    
    END_FLAGS_SET (pEnd, (IFF_UP | IFF_RUNNING));

    /* Update the link status */
    
    if (ERROR == usbTgtVrtlEndLinkUpdate (pUsbTgtVrtlEnd->pDev))
        {
        USBTGT_END_WARN ("usbTgtVrtlEndLinkUpdate fail\n",
                        1, 2, 3, 4, 5, 6);
        }

    END_TX_SEM_GIVE (pEnd);
    
    semGive (pUsbTgtVrtlEnd->endMutex);

    return(OK);
    }


/*******************************************************************************
*
* usbTgtVrtlEndStop - stop the device
*
* This function calls BSP functions to disconnect interrupts and stop
* the device from operating in interrupt mode.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtVrtlEndStop
    (
    END_OBJ * pEnd
    )
    {
    pUSBTGT_VRTL_END pUsbTgtVrtlEnd = (pUSBTGT_VRTL_END)pEnd;

    if (NULL == pUsbTgtVrtlEnd)
        {
        return ERROR;
        }
    
    semTake (pUsbTgtVrtlEnd->endMutex, WAIT_FOREVER);

    if (!(pEnd->flags & IFF_UP))
        {
        semGive (pUsbTgtVrtlEnd->endMutex);
        return (OK);
        }

    END_FLAGS_CLR (pEnd, (IFF_UP | IFF_RUNNING));

    /*
     * Flush the recycle cache to shake loose any of our
     * mBlks that may be stored there.
     */

    endMcacheFlush ();
    
    USBTGT_END_DBG ("usbTgtVrtlEndStop\n", 1, 2, 3, 4, 5, 6);

    semGive (pUsbTgtVrtlEnd->endMutex);
    
    return OK;
    }


/*****************************************************************************
*
* usbTgtVrtlEndUnload - unload END driver instance
*
* This routine undoes the effects of usbTgtVrtlEndLoad(). The END object
* is destroyed, our network pool is released, the endM2 structures
* are released, and the polling stats watchdog is terminated.
*
* Note that the END interface instance can't be unloaded if the
* device is still running. The device must be stopped with muxDevStop()
* first.
*
* RETURNS: OK, or ERROR if device is still in the IFF_UP state
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtVrtlEndUnload
    (
    END_OBJ * pEnd
    )
    {
    pUSBTGT_VRTL_END pUsbTgtVrtlEnd = (pUSBTGT_VRTL_END)pEnd;

    /* We must be stopped before we can be unloaded. */
    
    USBTGT_END_DBG ("usbTgtVrtlEndUnload\n", 1, 2, 3, 4, 5, 6);
    
    if ((NULL == pEnd) ||
        (pEnd->flags & IFF_UP))
        {
        return (ERROR);
        }

    if (pUsbTgtVrtlEnd->MBlkId)
        {    
        endPoolTupleFree (pUsbTgtVrtlEnd->MBlkId);
        }

    /* Release our buffer pool */
    
    endPoolDestroy (pUsbTgtVrtlEnd->EndObj.pNetPool);

    endM2Free (&pUsbTgtVrtlEnd->EndObj);

    END_OBJECT_UNLOAD (&pUsbTgtVrtlEnd->EndObj);

    return (OK); 
    }

/*****************************************************************************
*
* usbTgtVrtlEndIoctl - the driver I/O control routine
*
* This routine processes ioctl requests supplied via the muxIoctl()
* routine. In addition to the normal boilerplate END ioctls, this
* driver supports the IFMEDIA ioctls, END capabilities ioctls, and
* polled stats ioctls.
* 
* This routine also will be used by the USB Medium binder agent to 
* register the send sub routine which used by the <'usbTgtVrtlEndSend'>
* routine, and used to notify the link status or data receive event.
*
* RETURNS: A command specific response, usually OK or ERROR.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL int usbTgtVrtlEndIoctl
    (
    END_OBJ * pEnd,
    int       cmd,
    caddr_t   data
    )
    {
    pUSBTGT_VRTL_END pUsbTgtVrtlEnd = (pUSBTGT_VRTL_END)pEnd;
    VXB_DEVICE_ID    pDev = NULL;

    END_MEDIALIST *    mediaList;
    END_CAPABILITIES * hwCaps;
    END_MEDIA *        pMedia;
    END_RCVJOBQ_INFO * qinfo;
    UINT32             nQs;
    INT32              value;
    int                error = OK;

    if ((NULL == pUsbTgtVrtlEnd) ||
        (NULL == pUsbTgtVrtlEnd->pDev))
        {
        USBTGT_END_ERR ("Invalid parameter %s is NULL\n",
                        ((NULL == pUsbTgtVrtlEnd) ? "pEnd" :
                        "pUsbTgtVrtlEnd->pDev"), 2, 3, 4, 5, 6);
        
        return ERROR;
        }

    pDev = pUsbTgtVrtlEnd->pDev;

    if ((EIOCPOLLSTART != cmd) && 
        (EIOCPOLLSTOP != cmd))
        {
        semTake (pUsbTgtVrtlEnd->endMutex, WAIT_FOREVER);
        }

    switch (cmd)
        {
        case EIOCSADDR:
            {
            USBTGT_END_DBG ("usbTgtVrtlEndIoctl(): Process %s\n",
                            "EIOCSADDR", 2, 3, 4, 5, 6);
            
            if (data == NULL)
                {
                USBTGT_END_ERR ("Invalid parameter %s is NULL\n",
                                "data", 2, 3, 4, 5, 6);
                
                error = EINVAL;
                }
            else
                {
                bcopy ((char *)data, (char *)pUsbTgtVrtlEnd->localMacAddr,
                        ETHER_ADDR_LEN);
                
                bcopy ((char *)data, 
                       (char *)pEnd->mib2Tbl.ifPhysAddress.phyAddress,
                       ETHER_ADDR_LEN);                
                }
            }
            break;
        case EIOCGADDR:
            {
            USBTGT_END_DBG ("usbTgtVrtlEndIoctl(): Process %s\n",
                            ("EIOCGADDR"), 2, 3, 4, 5, 6);
            
            if (data == NULL)
                {
                USBTGT_END_ERR ("Invalid parameter %s is NULL\n",
                                ("data"), 2, 3, 4, 5, 6);
                
                error = EINVAL;
                }
            else
                {
                bcopy ((char *)pUsbTgtVrtlEnd->localMacAddr, (char *)data,
                    ETHER_ADDR_LEN);            
                }
            }
            break;

        case UIOCSREMOTEADDR:
            {
            USBTGT_END_DBG ("usbTgtVrtlEndIoctl(): Process %s\n",
                            ("EIOCSADDR"), 2, 3, 4, 5, 6);
            
            if (data == NULL)
                {
                USBTGT_END_ERR ("Invalid parameter %s is NULL\n",
                                ("data"), 2, 3, 4, 5, 6);
                
                error = EINVAL;
                }
            else
                {
                usrUsbTgtVrtlEndRemoteMacAddrSet(pDev->unitNumber,(char *)data);
                }
            }
            break;
            
        case UIOCGREMOTEADDR:
            {
            USBTGT_END_DBG ("usbTgtVrtlEndIoctl(): Process %s\n",
                            ("EIOCGADDR"), 2, 3, 4, 5, 6);
            
            if (data == NULL)
                {
                USBTGT_END_ERR ("Invalid parameter %s is NULL\n",
                                ("data"), 2, 3, 4, 5, 6);
                
                error = EINVAL;
                }
            else
                {
                usrUsbTgtVrtlEndRemoteMacAddrGet(pDev->unitNumber,(char *)data);                
                }
            }
            break;    
        case EIOCSFLAGS:
            {
            value = (INT32) ((ULONG)data & 0xFFFFFFFF);
            
            USBTGT_END_DBG ("usbTgtVrtlEndIoctl(): Process %s value 0x%X \n",
                            ("EIOCSFLAGS"),(value < 0) ? 
                            ((- value) - 1) : (value), 3, 4, 5, 6);
            
            if (value < 0)
                {
                value = -value;
                value--;
                END_FLAGS_CLR (pEnd, value);
                
                USBTGT_END_DBG ("usbTgtVrtlEndIoctl(): Process %s clear "
                                "flags 0x%X\n",
                                ("EIOCSFLAGS"),value,3,4,5,6);
                
                }
            else
                {
                END_FLAGS_SET (pEnd, value);

                USBTGT_END_DBG ("usbTgtVrtlEndIoctl(): Process %s set "
                                "flags 0x%X\n",
                                ("EIOCSFLAGS"),value,3,4,5,6);
                }
            }
            break;

        case EIOCGFLAGS:
            {
            USBTGT_END_DBG ("usbTgtVrtlEndIoctl(): Process %s \n",
                            ("EIOCGFLAGS"), 2, 3, 4, 5, 6);
            
            if (data == NULL)
                {
                USBTGT_END_ERR ("Invalid parameter %s is NULL\n",
                                ("data"), 2, 3, 4, 5, 6);
                
                error = EINVAL;
                }
            else
                {
                *(long *)data = END_FLAGS_GET(pEnd);
                
                USBTGT_END_DBG ("usbTgtVrtlEndIoctl(): Process %s get flags 0x%X\n",
                                ("EIOCGFLAGS"),*(long *)data, 3, 4, 5, 6);
                
                }
            }
            break;

        case EIOCMULTIADD:
            {
            USBTGT_END_DBG ("usbTgtVrtlEndIoctl(): Process %s\n",
                            ("EIOCMULTIADD"), 2, 3, 4, 5, 6);
            
            error = usbTgtVrtlEndMCastAddrAdd (pEnd, (char *) data);
            }
            break;

        case EIOCMULTIDEL:
            {
            USBTGT_END_DBG ("usbTgtVrtlEndIoctl(): Process %s\n",
                            ("EIOCMULTIDEL"), 2, 3, 4, 5, 6);

            error = usbTgtVrtlEndMCastAddrDel (pEnd, (char *) data);
            }
            break;

        case EIOCMULTIGET:
            {
            USBTGT_END_DBG ("usbTgtVrtlEndIoctl(): Process %s\n",
                            ("EIOCMULTIGET"), 2, 3, 4, 5, 6);

            error = usbTgtVrtlEndMCastAddrGet (pEnd, (MULTI_TABLE *) data);
            }
            break;

        case EIOCGMIB2233:
        case EIOCGMIB2:
            {
            USBTGT_END_DBG ("usbTgtVrtlEndIoctl(): Process %s \n",
                            ((EIOCGMIB2233 == cmd) ? "EIOCGMIB2233" :
                            "EIOCGMIB2"), 2, 3, 4, 5, 6);

            error = endM2Ioctl (&pUsbTgtVrtlEnd->EndObj, cmd, data);
            }
            break;

        case EIOCGMEDIALIST:

            {
            USBTGT_END_DBG ("usbTgtVrtlEndIoctl(): Process %s \n",
                            ("EIOCGMEDIALIST"), 2, 3, 4, 5, 6);

            
            if (data == NULL)
                {
                USBTGT_END_ERR ("Invalid parameter %s is NULL\n",
                                ("data"), 2, 3, 4, 5, 6);
                
                error = EINVAL;
                break;
                }
            
            if ((NULL == pUsbTgtVrtlEnd->pEndMediaList) ||
                (0 == pUsbTgtVrtlEnd->pEndMediaList->endMediaListLen))
                {
                USBTGT_END_ERR ("Invalid parameter %s is NULL\n",
                                ("pEndMediaList"), 2, 3, 4, 5, 6);                
                
                error = ENOTSUP;
                break;
                }

            mediaList = (END_MEDIALIST *)data;
            
            if (mediaList->endMediaListLen <
                pUsbTgtVrtlEnd->pEndMediaList->endMediaListLen)
                {
                mediaList->endMediaListLen =
                    pUsbTgtVrtlEnd->pEndMediaList->endMediaListLen;
                
                error = ENOSPC;
                break;
                }

            bcopy((char *)pUsbTgtVrtlEnd->pEndMediaList, (char *)mediaList,
                  sizeof(END_MEDIALIST) + (sizeof(UINT32) *
                  pUsbTgtVrtlEnd->pEndMediaList->endMediaListLen));
            }
            break;

        case EIOCGIFMEDIA:
            {
            USBTGT_END_DBG ("usbTgtVrtlEndIoctl(): Process %s\n",
                            ("EIOCGIFMEDIA"), 2, 3, 4, 5, 6);
            
            if (data == NULL)
                {
                USBTGT_END_ERR ("Invalid parameter %s is NULL\n",
                                ("data"), 2, 3, 4, 5, 6);
                
                error = EINVAL;
                }
            else            
                {
                pMedia = (END_MEDIA *)data;
                pMedia->endMediaActive = pUsbTgtVrtlEnd->uCurMedia;

                USBTGT_END_DBG ("usbTgtVrtlEndIoctl(): Process %s value 0x%X \n",
                                ("EIOCGIFMEDIA"),
                                pUsbTgtVrtlEnd->uCurStatus,3 ,4,5,6);

                /* TODO : Always ACTIVE ? */

                pMedia->endMediaStatus = pUsbTgtVrtlEnd->uCurStatus | 
                                         (IFM_AVALID|IFM_ACTIVE);

                USBTGT_END_DBG ("usbTgtVrtlEndIoctl(): Process %s value 0x%X\n",
                                ("EIOCGIFMEDIA"),
                                pUsbTgtVrtlEnd->uCurStatus,3,4,5,6);

                }
            }
            break;

        case EIOCSIFMEDIA:
            {
            USBTGT_END_DBG ("usbTgtVrtlEndIoctl(): Process %s command\n",
                            ("EIOCSIFMEDIA"), 2, 3, 4, 5, 6);
            
            if (data == NULL)
                {
                USBTGT_END_ERR ("Invalid parameter %s is NULL\n",
                                ("data"), 2, 3, 4, 5, 6);
                
                error = EINVAL;
                }
            else            
                {
                pMedia = (END_MEDIA *)data;

                /* MII bus mode set */
                
                error = OK;
                
                if (ERROR == miiBusModeSet (pUsbTgtVrtlEnd->dumyMiiBus, 
                                            pMedia->endMediaActive))
                    {
                    USBTGT_END_ERR ("miiBusModeSet endMediaActive %x fail\n",
                                    pMedia->endMediaActive, 2, 3, 4, 5, 6);

                    error = EIO;
                    break;
                    }

                /* Update the link status */
                
                if (ERROR == usbTgtVrtlEndLinkUpdate (pUsbTgtVrtlEnd->pDev))
                    {
                    USBTGT_END_ERR ("usbTgtVrtlEndLinkUpdate fail\n",
                                    1, 2, 3, 4, 5, 6);

                    error = EIO;
                    break;
                    }
                }
            }
            break;

        case EIOCGIFCAP:
            {
            USBTGT_END_DBG ("usbTgtVrtlEndIoctl(): Process %s command\n",
                            ("EIOCGIFCAP"), 2, 3, 4, 5, 6);
            
            if (data == NULL)
                {
                USBTGT_END_ERR ("Invalid parameter %s is NULL\n",
                                ("data"), 2, 3, 4, 5, 6);
                
                error = EINVAL;
                break;
                }            
            
            hwCaps = (END_CAPABILITIES *)data;
            hwCaps->cap_available = pUsbTgtVrtlEnd->endCaps.cap_available;
            hwCaps->cap_enabled = pUsbTgtVrtlEnd->endCaps.cap_enabled;
            }
            break;

        /*
         * The only special capability we support is VLAN_MTU, and
         * it can never be turned off.
         */

        case EIOCSIFCAP:
            {
            USBTGT_END_DBG ("usbTgtVrtlEndIoctl(): Process %s command\n",
                            ("EIOCSIFCAP"), 2, 3, 4, 5, 6);
            
            error = ENOTSUP;
            }
            break;

        case EIOCGIFMTU:
            {
            USBTGT_END_DBG ("usbTgtVrtlEndIoctl(): Process %s command\n",
                            ("EIOCGIFMTU"), 2, 3, 4, 5, 6);
            
            if (data == NULL)
                {
                USBTGT_END_ERR ("Invalid parameter %s is NULL\n",
                                ("data"), 2, 3, 4, 5, 6);
                
                error = EINVAL;
                }     
            else
                {
                *(INT32 *)data = pEnd->mib2Tbl.ifMtu;
                }
            }
            break;

        case EIOCSIFMTU:
            {
            USBTGT_END_DBG ("usbTgtVrtlEndIoctl(): Process %s command\n",
                            ("EIOCSIFMTU"), 2, 3, 4, 5, 6);
            
            value = (INT32)((ULONG)data & 0xFFFFFFFF);
            if ((value <= 0) || 
                (value > pUsbTgtVrtlEnd->maxPktSize))
                {
                error = EINVAL;
                break;
                }
            pEnd->mib2Tbl.ifMtu = value;
            if (pEnd->pMib2Tbl != NULL)
                {
                pEnd->pMib2Tbl->m2Data.mibIfTbl.ifMtu = value;
                }
            }
            break;

        case EIOCGRCVJOBQ:
            {
            USBTGT_END_DBG ("usbTgtVrtlEndIoctl(): Process %s command\n",
                            ("EIOCGRCVJOBQ"), 2, 3, 4, 5, 6);  

            if (data == NULL)
                {
                USBTGT_END_ERR ("Invalid parameter %s is NULL\n",
                                ("data"), 2, 3, 4, 5, 6);
                
                error = EINVAL;
                break;
                }     
            
            qinfo = (END_RCVJOBQ_INFO *)data;
            nQs = qinfo->numRcvJobQs;
            qinfo->numRcvJobQs = 1;
            if (nQs < 1)
                error = ENOSPC;
            else
                qinfo->qIds[0] = pUsbTgtVrtlEnd->endJobQueue;
            }
            break;

        case UIOCSSUBSENDRTN:

            pUsbTgtVrtlEnd->sendSubRoutine = (VXB_END_SEND_SUB_RNT)data;
            
            break;

        default:
            
            /*
             * The virtual USB End driver do not support the polling mode
             * So no <'EIOCPOLLSTART'> and <'EIOCPOLLSTOP'> command 
             * supported.
             */
             
            USBTGT_END_VDBG("usbTgtVrtlEndIoctl(): Unsupported command 0x%X\n",
                            cmd, 2, 3, 4, 5, 6);
                           
            error = EINVAL;
            
            break;
        }

    if ((EIOCPOLLSTART != cmd) && 
        (EIOCPOLLSTOP != cmd))
        {
        semGive (pUsbTgtVrtlEnd->endMutex);
        }

    return (error);
    }

/*******************************************************************************
*
* usbTgtVrtlEndSend - the driver send routine
*
* This is the vxBus END compatible routine.It takes a M_BLK_ID and sends off 
* the data in the M_BLK_ID.The buffer must already have the addressing 
* information properly installed in it. This is done by a higher layer.
*
* muxSend() calls this routine each time it wants to send a packet. This routine
* will call the send sub routine which registered to the END driver to send
* the data to the USB low level channel.This sub routine can be used by RNDIS
* or CDC.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtVrtlEndSend
    (
    END_OBJ * pEnd,
    M_BLK_ID  pMblk
    )
    {
    pUSBTGT_VRTL_END pUsbTgtVrtlEnd = (pUSBTGT_VRTL_END)pEnd;
    STATUS           status = OK;

    if ((NULL == pUsbTgtVrtlEnd) ||
        (NULL == pMblk))
        {
        USBTGT_END_ERR("Invalid parameter %s is NULL\n",
                       ((NULL == pUsbTgtVrtlEnd) ? "pEnd" :
                       "pMblk"), 2, 3, 4, 5, 6);
        return ERROR;
        }

    /* 
     * If it has the send sub routine, It will c
     * all the sub routine to send the data to the 
     * USB interface. And the USB interface will prepare the data
     * with the header/CRC and transfer to the USB bus.
     * NOTE: the sub routine should call netMblkClChainFree to 
     * free the pMblk.
     *  
     * else, free the Mbuf.
     */
    
    if (pUsbTgtVrtlEnd->sendSubRoutine)
        {
        status = (pUsbTgtVrtlEnd->sendSubRoutine)(pEnd, pMblk);
        }
    else
        {
        netMblkClChainFree (pMblk);
        }

    return(status);    
    }


/*****************************************************************************
*
* usbTgtVrtlEndMCastAddrAdd - add a multicast address for the device
*
* This routine adds a multicast address to whatever the driver
* is already listening for.  It then resets the address filter.
*
* RETURNS: OK, ERROR or other return value of <'etherMultiAdd'>
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL int usbTgtVrtlEndMCastAddrAdd
    (
    END_OBJ * pEnd,
    char *    pAddr
    )
    {
    pUSBTGT_VRTL_END pUsbTgtVrtlEnd = (pUSBTGT_VRTL_END)pEnd;
    int             retVal;
    
    if ((NULL == pUsbTgtVrtlEnd) ||
        (NULL == pAddr))
        {
        USBTGT_END_ERR("Invalid parameter %s is NULL\n",
                       ((NULL == pUsbTgtVrtlEnd) ? "pEnd" :
                       "pAddr"), 2, 3, 4, 5, 6);
        return ERROR;
        }
    
    semTake (pUsbTgtVrtlEnd->endMutex, WAIT_FOREVER);

    if (!(pUsbTgtVrtlEnd->EndObj.flags & IFF_UP))
        {
        semGive (pUsbTgtVrtlEnd->endMutex);
        return (OK);
        }

    retVal = etherMultiAdd (&pEnd->multiList, pAddr);

    USBTGT_END_VDBG("usbTgtVrtlEndMCastAddrAdd etherMultiAdd %d\n",
                    retVal, 2, 3, 4, 5, 6);

    pEnd->nMulti++;

    semGive (pUsbTgtVrtlEnd->endMutex);

    /* Do not trate ENETRESET as error */
    
    if (ENETRESET == retVal)
        {
        retVal = OK;
        }
    
    return (retVal);
    }


/*****************************************************************************
*
* usbTgtVrtlEndMCastAddrDel - delete a multicast address for the device
*
* This routine removes a multicast address from whatever the driver
* is listening for. It then resets the address filter.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL int usbTgtVrtlEndMCastAddrDel
    (
    END_OBJ * pEnd,
    char *    pAddr
    )
    {
    pUSBTGT_VRTL_END pUsbTgtVrtlEnd = (pUSBTGT_VRTL_END)pEnd;
    int             retVal;

    if ((NULL == pUsbTgtVrtlEnd) ||
        (NULL == pAddr))
        {
        USBTGT_END_ERR("Invalid parameter %s is NULL\n",
                       ((NULL == pUsbTgtVrtlEnd) ? "pEnd" :
                       "pAddr"), 2, 3, 4, 5, 6);
        return ERROR;
        }
    
    semTake (pUsbTgtVrtlEnd->endMutex, WAIT_FOREVER);

    if (!(pUsbTgtVrtlEnd->EndObj.flags & IFF_UP))
        {
        semGive (pUsbTgtVrtlEnd->endMutex);
        return (OK);
        }

    retVal = etherMultiDel (&pEnd->multiList, pAddr);

    pEnd->nMulti--;

    semGive (pUsbTgtVrtlEnd->endMutex);
    
    /* Do not trate ENETRESET as error */
    
    if (ENETRESET == retVal)
        {
        retVal = OK;
        }
    
    return retVal;
    }


/*****************************************************************************
*
* usbTgtVrtlEndMCastAddrGet - get the multicast address list for the device
*
* This routine gets the multicast list of whatever the driver
* is already listening for.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL int usbTgtVrtlEndMCastAddrGet
    (
    END_OBJ *     pEnd,
    MULTI_TABLE * pTable
    )
    {
    pUSBTGT_VRTL_END pUsbTgtVrtlEnd = (pUSBTGT_VRTL_END)pEnd;
    int             retVal;
    
    if ((NULL == pUsbTgtVrtlEnd) ||
        (NULL == pTable))
        {
        USBTGT_END_ERR("Invalid parameter %s is NULL\n",
                       ((NULL == pUsbTgtVrtlEnd) ? "pEnd" :
                       "pTable"), 2, 3, 4, 5, 6);
        return ERROR;
        }

    semTake (pUsbTgtVrtlEnd->endMutex, WAIT_FOREVER);

    if (!(pUsbTgtVrtlEnd->EndObj.flags & IFF_UP))
        {
        semGive (pUsbTgtVrtlEnd->endMutex);
        return (OK);
        }

    retVal = etherMultiGet (&pEnd->multiList, pTable);

    semGive (pUsbTgtVrtlEnd->endMutex);

    return (retVal);
    }


/*****************************************************************************
*
* usbTgtVrtlEndLoad - END driver entry point
*
* This routine initializes the END interface instance associated
* with this device. In the legacy END drivers, this function is
* the only public interface, and it's typically invoked by a BSP
* driver configuration stub. With VxBus, the BSP stub code is no
* longer needed, and this function is now invoked automatically
* whenever this driver's muxConnect() method is called.
*
* For the legacy END drivers, the load string would contain various
* configuration parameters, but with VxBus this use is deprecated.
* The load string should just be an empty string. The second
* argument should be a pointer to the VxBus device instance
* associated with this device. Like the legacy END drivers, this routine
* will still return the device name if the init string is empty,
* since this behavior is still expected by the MUX. The MUX will
* invoke this function twice: once to obtain the device name,
* and then again to create the actual END_OBJ instance.
*
* When this routine is called the second time, it will initialize
* the END object, perform MIB2 setup, allocate a buffer pool, and
* initialize the supported END capabilities. The only special
* capability we support is VLAN_MTU, since we can receive slightly
* larger than normal frames.
*
* RETURNS: An END object pointer, or NULL on error, or 0 and the name
* of the device if the <loadStr> was empty.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL END_OBJ * usbTgtVrtlEndLoad
    (
    char * loadStr,
    void * pArg
    )
    {
    pUSBTGT_VRTL_END pUsbTgtVrtlEnd = NULL;    
    VXB_DEVICE_ID   pDev = NULL;
    char *          pName = NULL; 

    /* Make the MUX happy. */

    if (loadStr == NULL)
        {
        USBTGT_END_ERR("Invalid parameter %s is NULL\n",
                       ("loadStr"), 2, 3, 4, 5, 6);

        return NULL;
        }

    /* The first time, return the name */

    if (loadStr[0] == 0)
        {
        pName = usrUsbTgtVrtlEndNameGet();

        bcopy (pName, loadStr, strlen(pName));
        
        return NULL;
        }
    
    pDev = (VXB_DEVICE_ID)pArg;
    
    /* The 2nd time, we got here, check the parameters */
    
    if ((NULL == pDev) ||
        (NULL == pDev->pDrvCtrl))
        {
        USBTGT_END_ERR("Invalid parameter %s is NULL\n",
                       ((NULL == pDev) ? "pDev" :
                       "pDev->pDrvCtrl"), 2, 3, 4, 5, 6);
        
        return NULL;
        }
    
    pUsbTgtVrtlEnd = (pUSBTGT_VRTL_END)pDev->pDrvCtrl;

    /* END Obj Init */
    
    if (END_OBJ_INIT (&pUsbTgtVrtlEnd->EndObj, 
                      NULL, 
                      pDev->pName,
                      pDev->unitNumber,
                      &usbTgtVrtlEndNetFuncs,
                      "USB Target Virtual VxBus END Driver") == ERROR)
        {
        USBTGT_END_ERR("%s%d: END_OBJ_INIT failed\n", 
                       (ULONG)pDev->pName,
                       pDev->unitNumber, 0, 0, 0, 0);
        
        return (NULL);
        }

    /* endM2 Init */

    endM2Init (&pUsbTgtVrtlEnd->EndObj, 
               M2_ifType_ethernet_csmacd,
               (UCHAR *)pUsbTgtVrtlEnd->localMacAddr,
               ETHER_ADDR_LEN, 
               ETHERMTU, 
               usrUsbTgtVrtlEndSpeedGet(0),
               (IFF_NOTRAILERS | 
               IFF_SIMPLEX | 
               IFF_MULTICAST | 
               IFF_BROADCAST |
               IFF_RUNNING));

    pUsbTgtVrtlEnd->maxPktSize = USBTGT_VRTL_END_MAX_PKT;

    /* Allocate a buffer pool */

    if (endPoolCreate (USBTGT_VRTL_END_POOL_TUPLE_CNT, 
                       &pUsbTgtVrtlEnd->EndObj.pNetPool))
        {
        USBTGT_END_ERR("%s%d: pool creation failed\n",
                       pDev->pName,
                       pDev->unitNumber, 0, 0, 0, 0);
        
        return (NULL);
        }

    pUsbTgtVrtlEnd->MBlkId = endPoolTupleGet (pUsbTgtVrtlEnd->EndObj.pNetPool);

    /* Set up capabilities. */

    pUsbTgtVrtlEnd->endCaps.cap_available = IFCAP_VLAN_MTU;
    pUsbTgtVrtlEnd->endCaps.cap_enabled = IFCAP_VLAN_MTU;

    USBTGT_END_DBG ("usbTgtVrtlEndLoad done \n", 1, 2, 3, 4, 5, 6);

    return (&pUsbTgtVrtlEnd->EndObj);
    }



/*****************************************************************************
*
* usbTgtVrtlEndDataUnInit - destroy the resource created by the routine
* <'usbTgtVrtlEndDataInit'>.
*
* This routine destroys the resource created by the routine
* <'usbTgtVrtlEndDataInit'>.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtVrtlEndDataUnInit
    (
    struct vxbDev * pDev
    )
    {
    pUSBTGT_VRTL_END pUsbTgtVrtlEnd = NULL;
    
    /* Check if the parameters are valid */
    
    if ((NULL == pDev) ||
        (NULL == pDev->pDrvCtrl))
        {
        USBTGT_END_ERR("Invalid parameter %s is NULL\n",
                       ((NULL == pDev) ? "pDev" :
                       "pDev->pDrvCtrl"), 2, 3, 4, 5, 6);
        
        return ERROR;
        }
    
    pUsbTgtVrtlEnd = pDev->pDrvCtrl;
    
    /* Destroy our MII bus and child PHYs. */
    
    if (pUsbTgtVrtlEnd->dumyMiiBus)
        {
        /* This routine always return OK, do not check the status */
        
        miiBusDelete (pUsbTgtVrtlEnd->dumyMiiBus);
        }

    /* Delete the Mutex */
    
    if (pUsbTgtVrtlEnd->endMutex)
        {
        /* Mutex should be taken before delete it */

        semTake (pUsbTgtVrtlEnd->endMutex, WAIT_FOREVER);
        semDelete (pUsbTgtVrtlEnd->endMutex);
        pUsbTgtVrtlEnd->endMutex = NULL;
        }

    /* Destroy the data structure. */

    OS_FREE (pUsbTgtVrtlEnd);
    
    pDev->pDrvCtrl = NULL;

    return OK;
    }

/*****************************************************************************
*
* usbTgtVrtlEndDataInit - get the multicast address list for the device
*
* This routine gets the multicast list of whatever the driver
* is already listening for.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*/

STATUS usbTgtVrtlEndDataInit
    (
    struct vxbDev * pDev
    )
    {
    pUSBTGT_VRTL_END pUsbTgtVrtlEnd = NULL;
    STATUS           status = ERROR;
    
    /* Validate parameters */
    
    if (NULL == pDev)
        {
        USBTGT_END_ERR("Invalid parameter %s is NULL",
                      "pDev",  2, 3, 4, 5, 6);
        
        return ERROR;
        }
    
    pUsbTgtVrtlEnd = OSS_CALLOC (sizeof(USBTGT_VRTL_END));    

    if (NULL == pUsbTgtVrtlEnd)
        {
        USBTGT_END_ERR("Memory allocate failed for virtual END.\n",
                       1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    USBTGT_END_ERR("usbTgtVrtlEndDataInit in\n", 1,2,3,4,5, 6);
    
    /* Create our MII bus. */

    pUsbTgtVrtlEnd->endMutex = semMCreate (SEM_Q_PRIORITY  | 
                                          SEM_DELETE_SAFE |
                                          SEM_INVERSION_SAFE);

    if (NULL == pUsbTgtVrtlEnd->endMutex)
        {
        USBTGT_END_ERR("Create end Mutex fail\n",
                       1, 2, 3, 4, 5, 6);
        
        usbTgtVrtlEndDataUnInit(pDev);

        return ERROR; 
        }    

    /* Create our dumy MII bus. */

    status = miiBusCreate (pDev, &pUsbTgtVrtlEnd->dumyMiiBus);

    if (ERROR == status)
        {
        USBTGT_END_ERR("Create the mii Bus fail\n",
                       1, 2, 3, 4, 5, 6);
        
        usbTgtVrtlEndDataUnInit(pDev);

        return ERROR; 
        }
    
    status = miiBusMediaListGet (pUsbTgtVrtlEnd->dumyMiiBus, 
                        &pUsbTgtVrtlEnd->pEndMediaList);

    if (ERROR == status)
        {
        USBTGT_END_ERR("Get the mii Bus media list fail\n",
                       1, 2, 3, 4, 5, 6);
        
        usbTgtVrtlEndDataUnInit(pDev);

        return ERROR; 
        }
    
    status = miiBusModeSet (pUsbTgtVrtlEnd->dumyMiiBus,
                   pUsbTgtVrtlEnd->pEndMediaList->endMediaListDefault);

    if (ERROR == status)
        {
        /* This is the dumy MII bus, set the mode fail maybe happened */
        
        USBTGT_END_DBG("Set the mii Bus mode fail\n",
                       1, 2, 3, 4, 5, 6);
        }
    
    USBTGT_END_ERR("usbTgtVrtlEndDataInit in 2\n", 1,2,3,4,5, 6);

    pDev->pDrvCtrl = pUsbTgtVrtlEnd;
    
    pUsbTgtVrtlEnd->pDev = pDev;

    /* Get the Mac address */

    status = usrUsbTgtVrtlEndLocalMacAddrGet(pDev->unitNumber,
                                             pUsbTgtVrtlEnd->localMacAddr);
    
    if (ERROR == status)
        {
        USBTGT_END_ERR("Get the local MAC address fail\n",
                       1, 2, 3, 4, 5, 6);
        
        usbTgtVrtlEndDataUnInit(pDev);

        return ERROR; 
        }
    
    USBTGT_END_VDBG("usbTgtVrtlEndDataInit OK\n",
                   1, 2, 3, 4, 5, 6);
    
    return OK;
    }

/*****************************************************************************
*
* usbTgtVrtlEndDataShow - show the virtual END device's structure data
*
* This routine is used to show the virtual END device's structure data.
* <'pName'> and <'unit'> is the END device's 
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbTgtVrtlEndDataShow
    (
    char * pName,
    int    unit
    )
    {
    END_OBJ *       pEnd = NULL;
    pUSBTGT_VRTL_END pUsbTgtVrtlEnd = NULL;
    
    pEnd = endFindByName(pName, unit);

    if (NULL != pEnd)
        {
        pUsbTgtVrtlEnd = (pUSBTGT_VRTL_END)pEnd;
        
        printf ("pUsbTgtVrtlEnd %p\n",pUsbTgtVrtlEnd);
        printf ("pUsbTgtVrtlEnd->pDev %p\n",pUsbTgtVrtlEnd->pDev);
        printf ("pUsbTgtVrtlEnd->pMuxDevCookie %p\n",pUsbTgtVrtlEnd->pMuxDevCookie);




        printf ("pUsbTgtVrtlEnd->MBlkId %p\n",pUsbTgtVrtlEnd->MBlkId);
        printf ("pUsbTgtVrtlEnd->localMacAddr %02X:%02X:%02X:%02X:%02X:%02X\n",
                                           pUsbTgtVrtlEnd->localMacAddr[0],
                                           pUsbTgtVrtlEnd->localMacAddr[1],
                                           pUsbTgtVrtlEnd->localMacAddr[2],
                                           pUsbTgtVrtlEnd->localMacAddr[3],
                                           pUsbTgtVrtlEnd->localMacAddr[4],
                                           pUsbTgtVrtlEnd->localMacAddr[5]);
        printf ("pUsbTgtVrtlEnd->remoteMacAddr %02X:%02X:%02X:%02X:%02X:%02X\n",
                                           pUsbTgtVrtlEnd->remoteMacAddr[0],
                                           pUsbTgtVrtlEnd->remoteMacAddr[1],
                                           pUsbTgtVrtlEnd->remoteMacAddr[2],
                                           pUsbTgtVrtlEnd->remoteMacAddr[3],
                                           pUsbTgtVrtlEnd->remoteMacAddr[4],
                                           pUsbTgtVrtlEnd->remoteMacAddr[5]);

        
        }
    return;
    }


