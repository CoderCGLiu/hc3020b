/* usbUhcdCommon.h - header file for common USB UHCD HCD registers*/

/*
 * Copyright (c) 2003, 2007-2010 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2003, 2007-2010 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
Modification history
--------------------
01j,18may10,w_x  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
01i,13jan10,ghs  vxWorks 6.9 LP64 adapting
01h,11sep09,ghs  Redefine max hcd as global(WIND00152418)
01g,20mar09,w_x  Added USB_UHCD_MAGIC_ALIVE/USB_UHCD_MAGIC_DEAD
01f,20feb09,w_x  Added new defines for USBINTR register
01e,14aug08,w_x  Added some register bit defines to make the code more readable
01d,04jun08,w_x  Removed usbUhcdJobQueueAdd functions.(WIND00121282 fix)
01c,19may07,jrp  Added usbUhcdJobQueueAdd functions
01b,26jun03,mat  changing the code to WRS standards.
01a,25apr03,ram  written.
*/

/*
DESCRIPTION

This file contains the macros which are common for all the the
USB UHCD HCD functional modules.
*/


/*
INTERNAL
 *******************************************************************************
 * Filename         : usbUhcdCommon.h
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      :  This file contains the macros which are common all the
 *                     the UHCD HCD functional modules.
 *
 *
 */

#ifndef __INCusbUhcdCommonh
#define __INCusbUhcdCommonh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include <usbOsal.h>

/* defines */

#define USB_UHCD_USBCMD      0x00  /* Offset of the USBCMD register */
#define USB_UHCD_USBSTS      0x02  /* Offset of the USBSTS register */
#define USB_UHCD_USBINTR     0x04  /* Offset of the USBINTR register */
#define USB_UHCD_FRNUM       0x06  /* Offset of the FRNUM register */
#define USB_UHCD_FLBASEADD   0x08  /* Offset of the FLBASEADD register */
#define USB_UHCD_SOF_MODIFY  0x0C  /* Offset of the SOF_MODIFY register*/
#define USB_UHCD_PORT1       0x10  /* Offset of the PORT1 register */
#define USB_UHCD_PORT2       0x12  /* Offset of the PORT2 register */

/* To hold the loop iteration number used for settling time of the HC reset */

#define USB_UHCD_NUM_OF_RETRIES      1000000

#define USB_UHCD_PORTSC_CCS     (0) /* Current Connect Status bit position */
#define USB_UHCD_PORTSC_CSC     (1) /* Connect Status Change bit position */
#define USB_UHCD_PORTSC_PE      (2) /* Port Enable/Disable bit position */
#define USB_UHCD_PORTSC_PEC     (3) /* Port Enable/Disable change bit position */
#define USB_UHCD_PORTSC_LS_DP   (4) /* Line Status D+ bit position */
#define USB_UHCD_PORTSC_LS_DM   (5) /* Line Status D- bit position */
#define USB_UHCD_PORTSC_RD      (6) /* Resume Detect bit position */
#define USB_UHCD_PORTSC_LSDA    (8) /* Low Speed Device Attached bit position */
#define USB_UHCD_PORTSC_PR      (9) /* Port Reset bit position */
#define USB_UHCD_PORTSC_SUSP    (12)/* Suspend bit position */

#define USB_UHCD_USBCMD_RS      (0) /* Run/Stop bit position */
#define USB_UHCD_USBCMD_HCRESET (1) /* Host Controller Reset bit position */
#define USB_UHCD_USBCMD_GRESET  (2) /* Global Reset bit position */
#define USB_UHCD_USBCMD_EGSM    (3) /* Enter Global Suspend bit position */
#define USB_UHCD_USBCMD_FGR     (4) /* Force Global Resume bit position */
#define USB_UHCD_USBCMD_SWDBG   (5) /* Software debug bit position */
#define USB_UHCD_USBCMD_CF      (6) /* Configure Flag bit position */
#define USB_UHCD_USBCMD_MAXP64  (7) /* Max Packet 64/32 bit position */

#define USB_UHCD_USBSTS_USBINT  (0) /* USB Interrupt bit position */
#define USB_UHCD_USBSTS_USBERR  (1) /* USB Error Interrupt bit position */
#define USB_UHCD_USBSTS_RD      (2) /* Resume Detect bit position */
#define USB_UHCD_USBSTS_HSE     (3) /* Host System Error bit position */
#define USB_UHCD_USBSTS_HCPE    (4) /* Host Controller Process Error bit position */
#define USB_UHCD_USBSTS_HCHALT  (5) /* HC Halted bit position */
#define USB_UHCD_USBSTS_MASK    (0x3F) /* Mask value */

#define USB_UHCD_USBINTR_TMOCRC (0) /* Timeout CRC Interrupt Enable bit position */
#define USB_UHCD_USBINTR_RESUME (1) /* Resume Interrupt Enable bit position */
#define USB_UHCD_USBINTR_IOC    (2) /* Interrupt On Complete Enable bit position */
#define USB_UHCD_USBINTR_SHORT  (3) /* Short packet Interrupt Enable bit position */
#define USB_UHCD_USBINTR_MASK   (0x0F) /* Mask value */
#define USB_UHCD_USBINTR_ALL    (0x0F) /* Enable all */

#define USB_UHCD_MAGIC_ALIVE    0xbeefbeef
#define USB_UHCD_MAGIC_DEAD     0xdeadbeef

/*
 * Uncomment to use polling mode for UHCI
 */

/* #define UHCD_POLLING_MODE */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCusbUhcdCommonh */


