/* usbOhciTransferManagement.c - Transfer Management of USB OHCI */

/*
 * Copyright (c) 2002-2011, 2013-2014 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2002-2011, 2013-2014 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
modification history
--------------------
02r,25Feb14,wyy  Enable vxbDmaBuf alignment checking (VXW6-70065)
02q,03may13,wyy  Remove compiler warning (WIND00356717)
02p,21apr13,wyy  Cut the relationship among busIndex of HCDData, unitNumber of 
                 vxBus Device, and HC count (such as g_EHCDControllerCount or 
                 g_UhcdHostControllerCount) to dynamically release or announce
                 a HCD driver (WIND00362065)
02o,16jan13,s_z  Add protection for usbOhciReleaseRequestInfo() (WIND00393001)
02n,13dec11,m_y  Modify according to code check result (WIND00319317)
02m,17aug10,m_y  Modify routine usbOhciUpdateIsochronousTransferURBInformation
                 rollback "02j" by enlarging the scope of the semaphore to
                 resolve the issue (WIND00228503)
02l,05aug10,m_y  Modify for coding convention
02k,22jul10,m_y  Modify host controller index compare
02j,16jul10,m_y  Modify the sequence of the usbOhciCancelUrb (WIND00223843)
02i,08jul10,m_y  Correct the uBufferAddr in loop when fill TD (WIND00217342)
02h,02jul10,m_y  Modify routine usbOhciDeletePipe, usbOhciCancelUrb,
                 usbOhciResetPipe avoid frequently plug in/out GEN1
                 cbi crash issue (WIND00221377)
02g,25jun10,w_x  Remove _VXB_DMABUFSYNC_DMA_POSTWRITE for write (WIND00218556)
02f,17jun10,m_y  Call REMOVE_REQEUEST_FROM_CANCEL_LIST before routine
                 usbOhciReleaseRequestInfo to make sure the request is removed
                 from cancel list.(WIND00216859)
02e,18may10,m_y  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
02d,23mar10,j_x  Changed for USB debug (WIND00184542)
02c,16mar10,ghs  Fix vxWorks6.9 LP64 adapting
02b,13jan10,ghs  vxWorks 6.9 LP64 adapting
02a,24sep09,y_l  Code Coverity CID(5222): NULL pointer check, Last fix is
                 not enough and re-fix it (WIND00183549)
01z,17sep09,y_l  Code Coverity CID(5222): NULL pointer check (WIND00182326)
01y,09apr09,w_x  Call URB callback when cancel URB or delete pipe (WIND00160730)
01x,18mar09,j_x  Correct usbOhciIsNodeFoundInPeriodicList return
                 invalid value(WIND00149730)
01w,12mar09,j_x  Correct translation macro error in usbOhciCancelUrbForEndpoint
                 function (WIND00158839)
01v,21jul08,w_x  Merge fix for memory leak(WIND00112754)
01u,16jul08,w_x  Speaker sound jump issue fix merged (WIND00119020)
01t,30mar08,jrp  WIND00113466 more Leak removal (General defect)
01s,17Dec07,jrp  WIND00112754 memory leak deleting control pipe
01r,07Dec07,aag  Fixed crash attributed to nortel headeset.
01q,06oct06,j_l  Cleanup compiler warnings.
01p,08sep06,j_l  Modify for Nortel's headset driver
01o,21nov07,pdg  Fix WIND00111611(Fixing crash on BE targets while releasing
                 bandwidth allocated for periodic endpoints on an interface)
01n,10jul07,jrp  Removing OS_ENTER_CRITICAL_SECTION
01m,08oct06,ami  Changes for USB-vxBus changes
01l,23jan06,ami  Proper Check made on error in the function
                 usbOhciProcessGeneralTransferCompletion () (SPR #109810 Fix)
01k,26apr05,mta  Diab compiler warning removal
01j,29mar05,pdg  Fix for disconnect issue with ms7727se bsp
01i,28mar05,pdg  non-PCI changes
01h,02mar05,ami  SPR #106373 (OHCI Max Controller Issue)
01g,25feb05,mta  SPR 106276
01f,15oct04,ami  Apigen Changes
01e,19sep04,hch  Fix diab compilation error
01d,16aug04,pdg  Fix for print-stop-reprint
01c,03aug04,pdg  Fixed coverity errors
01b,27jun03,nld  Changing the code to WRS standards
01a,17mar02,ssh  Initial Version
*/

/*

DESCRIPTION

This file defines the transfer management of the USB OHCI driver.

INCLUDE FILES:    usbOhci.h, usbOhciRegisterInfo.h, usbOhciTransferManagement.h
                  usbOhciUtil.h
*/

/*
INTERNAL
 *******************************************************************************
 * Filename         : OHCI_TransferManagement.c
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 * Description      : This file defines the interrupt handler for the OHCI
 *                    driver.
 *
 *
 ******************************************************************************/

/* includes */

#include <usb/usbOsal.h>
#include <usb/usbHst.h>
#include <usbOhci.h>
#include <usbOhciRegisterInfo.h>
#include <usbOhciTransferManagement.h>
#include <usbOhciUtil.h>

/*************** THESE FUNCTIONS WILL BE REGISTERED WITH USBD *****************/

/* Function to create the pipes for the endpoint */

LOCAL USBHST_STATUS usbOhciCreatePipe (UINT8      uHostControllerIndex,
                                       UINT8      uDeviceAddress,
                                       UINT8      uSpeed,
                                       PUCHAR     pEndpointDescriptor,
                                       UINT16     uHighSpeedHubInfo,
                                       ULONG*     puPipeHandle);

/* Function to delete a pipe corresponding to an endpoint */

LOCAL USBHST_STATUS usbOhciDeletePipe (UINT8      uHostControllerIndex,
                                       ULONG      uPipeHandle);

/* This function is used to submit a request to the device */

USBHST_STATUS usbOhciSubmitUrb (UINT8           uHostControllerIndex,
                                ULONG           uPipeHandle,
                                pUSBHST_URB     pUrb);

/* This function is used to cancel a request submitted to the device */

USBHST_STATUS usbOhciCancelUrb (UINT8           uHostControllerIndex,
                                ULONG           uPipeHandle,
                                pUSBHST_URB     pUrb);

/*
 * Function to determine whether the bandwidth is available to support the
 * new configuration or an alternate interface.
 */

USBHST_STATUS usbOhciIsBandwidthAvailable (UINT8    uHostControllerIndex,
                                           UINT8    uDeviceAddress,
                                           UINT8    uDeviceSpeed,
                                           PUCHAR   pCurrentDescriptor,
                                           PUCHAR   pNewDescriptor);

/*
 * Function to modify the properties of the default control pipe, i.e. the
 * pipe corresponding to Address 0, Endpoint 0.
 */

USBHST_STATUS usbOhciModifyDefaultPipe (UINT8   uHostControllerIndex,
                                        ULONG   uDefaultPipeHandle,
                                        UINT8   uDeviceSpeed,
                                        UINT8   uMaximumPacketSize,
                                        UINT16  uHighSpeedHubInfo);

/* Function to check whether any request is pending on a pipe */

USBHST_STATUS usbOhciIsRequestPending (UINT8    uHostControllerIndex,
                                       ULONG    uPipeHandle);

/* Function to obtain the current frame number */

USBHST_STATUS usbOhciGetFrameNumber (UINT8      uHostControllerIndex,
                                     PUINT16    puCurrentFrameNumber);

/* Function to modify the frame width */

USBHST_STATUS usbOhciSetBitRate (UINT8      uHostContollerIndex,
                                 BOOL       bIncrement,
                                 PUINT32    puCurrentFrameWidth);


/****************** MODULE SPECIFIC FUNCTIONS DECLARATION *********************/
/***************** THESE FUNCTIONS WILL BE USED INTERNALLY ********************/

/*
 * Function is used to check whether the pipe corresponding to an endpoint
 * is valid.
 */

#ifdef DEBUG
LOCAL USBHST_STATUS usbOhciIsValidPipe (UINT8  uHostControllerIndex,
                                        ULONG  uPipeHandle);
#endif

/*
 * Function to handle the completion of the transfer. This function is
 * called to handle the done head interrupt.
 */

LOCAL VOID usbOhciProcessTransferCompletion (UINT32   uHostControllerIndex,
                                             PVOID    pTransferDescriptor);

/* Function to reverse the list of transfers completed */

LOCAL PUSB_OHCI_TD usbOhciReverseTransferCompletionList(
        pUSB_OHCD_DATA  pHCDData,
        PUSB_OHCI_TD    pTransferDescriptor);

/* Function to handle the completion of the a general transfer */

LOCAL VOID usbOhciProcessGeneralTransferCompletion
                                        (UINT32        uHostControllerIndex,
                                         PUSB_OHCI_TD  pTransferDescriptor);

/* Function to handle the completion of the an isochronous transfer */

LOCAL VOID usbOhciProcessIsochronousTransferCompletion
                                        (UINT32        uHostControllerIndex,
                                         PUSB_OHCI_TD  pTransferDescriptor);

/* Function to update the URB information for isochronous transfers */

LOCAL VOID usbOhciUpdateIsochronousTransferURBInformation(
    UINT32                      uHostControllerIndex,
    pUSB_OHCD_REQUEST_INFO      pRequest);

/******** HELPER FUNCTIONS USED BY OHCI_IsBandwidthAvailable() - BEGIN *******/

/* Function to release the bandwidth allocated for the current configuration */

LOCAL VOID
usbOhciReleaseBandwidthForCurrentConfiguration(
    UINT8       uHostControllerIndex,
    UINT8       uDeviceAddress,
    UINT8       uDeviceSpeed,
    PUINT32     puAvailableBandwidth);

/* Function to allocate bandwidth for the new configuration */

LOCAL USBHST_STATUS
usbOhciAllocateBandwidthForNewConfiguration(
    UINT8                               uHostControllerIndex,
    UINT8                               uDeviceAddress,
    UINT8                               uDeviceSpeed,
    PUSB_OHCI_CONFIGURATION_DESCRIPTOR  pConfigurationDescriptor,
    PUINT32                             puAvailableBandwidth);

/* Function to release the bandwidth allocated for the current interface */

LOCAL VOID
usbOhciReleaseBandwidthForCurrentInterface(
    UINT8                           uHostControllerIndex,
    UINT8                           uDeviceAddress,
    UINT8                           uDeviceSpeed,
    PUSB_OHCI_INTERFACE_DESCRIPTOR  pInterfaceDescriptor,
    PUINT32                         puAvailableBandwidth);

/* Function to allocate bandwidth for the new interface */

LOCAL USBHST_STATUS
usbOhciAllocateBandwidthForNewInterface(
    UINT8                           uHostControllerIndex,
    UINT8                           uDeviceAddress,
    UINT8                           uDeviceSpeed,
    PUSB_OHCI_INTERFACE_DESCRIPTOR  pInterfaceDescriptor,
    PUINT32                         puAvailableBandwidth);

/********* HELPER FUNCTIONS USED BY OHCI_IsBandwidthAvailable() - END ********/

/* Function to add a node to the endpoint descriptor list */

LOCAL USBHST_STATUS usbOhciAddNodeToEndpointDescriptorList
                    (UINT32                          uHostControllerIndex,
                     pUSB_OHCD_PIPE                  pHCDPipe,
                     UINT8                           uPollingInterval,
                     UINT32                          uEndpointBandwidth);

/* Function to obtain the node with the maximum available bandwidth */

LOCAL INT8 usbOhciGetNodeWithMaximumAvailableBandwidth
                                            (UINT32 uHostControllerIndex,
                                             UINT8  uPollingInterval,
                                             UINT32 uEndpointBandwidth);

/*
 * Function to check whether an endpoint descriptor is already present in
 * the periodic list.
 */

LOCAL BOOLEAN usbOhciIsNodeFoundInPeriodicList
                        (UINT8                            uHostControllerIndex,
                         UINT8                            uPeriodicListIndex,
                         pUSB_OHCD_PIPE                   pHCDPipe);

/*
 * Function to add the endpoint descriptor to a specific interval in the
 * periodic list.
 */

LOCAL USBHST_STATUS usbOhciAddPeriodicNodeAtSpecifedInterval
                        (UINT8                          uHostControllerIndex,
                         UINT8                          uInterruptListNodeIndex,
                         pUSB_OHCD_PIPE                 pHCDPipe,
                         UINT32                         uEndpointBandwidth);

USBHST_STATUS usbOhciSetupPipe
    (
    pUSB_OHCD_DATA                     pHCDData,
    pUSB_OHCD_PIPE                     pHCDPipe,
    pUSB_TRANSFER_SETUP_INFO           pSetupInfo
    );

USBHST_STATUS usbOhciSubmitControlUrb
    (
    pUSB_OHCD_DATA                  pHCDData,
    pUSB_OHCD_PIPE                  pHCDPipe,
    pUSBHST_URB                     pUrb,
    pUSB_OHCD_REQUEST_INFO          pRequest
    );

USBHST_STATUS usbOhciSubmitBulkOrInterruptUrb
    (
    pUSB_OHCD_DATA                  pHCDData,
    pUSB_OHCD_PIPE                  pHCDPipe,
    pUSBHST_URB                     pUrb,
    pUSB_OHCD_REQUEST_INFO          pRequest
    );

USBHST_STATUS usbOhciSubmitIsochronousUrb
    (
    pUSB_OHCD_DATA                  pHCDData,
    pUSB_OHCD_PIPE                  pHCDPipe,
    pUSBHST_URB                     pUrb,
    pUSB_OHCD_REQUEST_INFO          pRequest
    );

USBHST_STATUS usbOhciSetupControlPipe
    (
    pUSB_OHCD_DATA    pHCDData,
    pUSB_OHCD_PIPE    pHCDPipe
    );

USBHST_STATUS usbOhciResetPipe
    (
    pUSB_OHCD_DATA  pHCDData,
    pUSB_OHCD_PIPE  pHCDPipe
    );

USBHST_STATUS usbOhciPipeControl
    (
    UINT8                     uBusIndex,  /* Index of the host controller */
    ULONG                     uPipeHandle,/* Pipe handle */
    USBHST_PIPE_CONTROL_INFO * pPipeCtrl  /* Pointer to the pipe control info */
    );

LOCAL BOOL BufferPageCross
    (
    UINT32       uHostControllerIndex,
    PUSB_OHCI_TD pTransferDescriptor,
    UINT32       uPacketBufferOffset
    );

/* functions */

/*********************** GLOBAL FUNCTIONS DEFINITION **************************/

/* NONE */

/******************* MODULE SPECIFIC FUNCTIONS DEFINITION *********************/
/*************** THESE FUNCTIONS WILL BE REGISTERED WITH USBD *****************/

/***************************************************************************
*
* usbOhciSetupControlPipe - allocate transfer resources for a control pipe
*
* This routine is used to allocate transfer resources for a cotnrol pipe.
* The transfer resources, including request info structures and associated DMA
* tag and DMA map are created. This is done by calling usbOhciSetupControlPipe
* with maxiumn trasnfer request size for control pipe.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the OHCI. Assumption is that valid parameters are passed by the OHCI.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: USBHST_SUCCESS when all resources allocation is done
*          USBHST_INVALID_PARAMETER when any paramter is invalid
*          USBHST_INSUFFICIENT_MEMORY when any resource can not be allocated
*          USBHST_FAILURE if the pipe specified is not a control pipe
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbOhciSetupControlPipe
    (
    pUSB_OHCD_DATA  pHCDData,
    pUSB_OHCD_PIPE  pHCDPipe
    )
    {
    USBHST_STATUS hstSts = USBHST_FAILURE;

    USB_TRANSFER_SETUP_INFO pipeSetupInfo;

    pUSB_TRANSFER_SETUP_INFO pSetupInfo = &pipeSetupInfo;

    /* Setup a default transfer limitations */

    OS_MEMSET(&pipeSetupInfo, 0, sizeof(USB_TRANSFER_SETUP_INFO));

    /*
     * Set up the transfer characteristics such as
     * creating proper DMA TAG and DMA MAPs
     */

    switch (pHCDPipe->uEndpointType)
        {
        case USBHST_CONTROL_TRANSFER:
            {
            /* Control transfer will have a UINT16 limit in the Data phase */

            pSetupInfo->uMaxTransferSize = USB_OHCD_CTRL_MAX_DATA_SIZE;

            /* We allow only 1 control request for one pipe at a time */

            pSetupInfo->uMaxNumReqests = 1;

            /* No special flags */

            pSetupInfo->uFlags = 0;

            hstSts = usbOhciSetupPipe(pHCDData,
                                      pHCDPipe,
                                      pSetupInfo);
            }
            break;
         default:
            break;
        }
    return hstSts;
    }

/*******************************************************************************
*
* usbOhciUnSetupPipe - destroy any allocated transfer resources for a pipe
*
* This routine is used to destroy any allocated transfer resource for a pipe.
*
* RETURNS: USBHST_STATUS of the destroy result.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbOhciUnSetupPipe
    (
    pUSB_OHCD_DATA      pHCDData,
    pUSB_OHCD_PIPE      pHCDPipe
    )
    {
    pUSB_OHCD_REQUEST_INFO  pTempRequest = NULL;
	NODE                    *pNode = NULL;
    NODE                    *pNextNode = NULL;
    PUSB_OHCI_TD            pTempTD = NULL;

    /* Get the first element of the free request list */

	pNode = lstFirst(&(pHCDPipe->freeRequestList));

	while (pNode != NULL)
		{
		pTempRequest = LIST_NODE_TO_USB_OHCD_REQUEST_INFO(pNode);
        pNextNode = lstNext(pNode);
        if (pTempRequest != NULL)
            {
            REMOVE_REQEUEST_FROM_FREE_LIST(pHCDPipe, pTempRequest);
            usbOhciDeleteRequestInfo(pHCDData, pHCDPipe, pTempRequest);
            }
		pNode = pNextNode;
		}

    /* Destroy all the TDs in free list */

    /*
     * Just to make sure that the last TD's pHCDNextTransferDescriptor
     * is NULL. Or else we will free the dummy TD wrongly.
     */

    pTempTD = pHCDPipe->pFreeTDTail;

    if (pTempTD != NULL)
        {
        if (pTempTD->pHCDNextTransferDescriptor != NULL)
            {
            pTempTD->pHCDNextTransferDescriptor = NULL;
            }
        }

    if (pHCDPipe->pFreeTDHead != NULL)
        {
        usbOhciDestroyTDList(pHCDData,
                             pHCDPipe->pFreeTDHead);

        pHCDPipe->pFreeTDHead = NULL;
        pHCDPipe->pFreeTDTail = NULL;
        }

    /* Destroy the user Data DMA TAG */

    if (pHCDPipe->usrBuffDmaTagId != NULL)
        {
        USB_OHCD_VDBG("usbOhciUnSetupPipe - free usrBuffDmaTagId\n",
                       1, 2, 3, 4, 5, 6);
        (void) vxbDmaBufTagDestroy(pHCDPipe->usrBuffDmaTagId);
        pHCDPipe->usrBuffDmaTagId = NULL;
        }

    /* Destroy the control Setup DMA TAG */

    if (pHCDPipe->ctrlSetupDmaTagId != NULL)
        {
        USB_OHCD_VDBG("usbOhciUnSetupPipe - free ctrlSetupDmaTagId\n",
                       1, 2, 3, 4, 5, 6);
        (void) vxbDmaBufTagDestroy(pHCDPipe->ctrlSetupDmaTagId);
        pHCDPipe->ctrlSetupDmaTagId = NULL;
        }

    return USBHST_SUCCESS;
    }

/*******************************************************************************
*
* usbOhciSetupPipe - allocate transfer resources for a pipe
*
* This routine is used to allocate transfer resources for a pipe.
*
* RETURNS: USBHST_STATUS of the setup result.
*
* ERRNO: N/A
*
* \NOMANUAL
*/
USBHST_STATUS usbOhciSetupPipe
    (
    pUSB_OHCD_DATA              pHCDData,
    pUSB_OHCD_PIPE              pHCDPipe,
    pUSB_TRANSFER_SETUP_INFO    pSetupInfo
    )
    {
    /* Request information */

    pUSB_OHCD_REQUEST_INFO pRequest = NULL;

    /* Transfer request index */

    UINT32                  uReqIndex = 0x0;

    /* Check if the parameters are valid */

    if ((pHCDData == NULL) ||
        (pHCDPipe == NULL) ||
        (pSetupInfo == NULL) ||
        (pSetupInfo->uMaxNumReqests == 0)) /* Must have some transfer */
        {
        USB_OHCD_ERR("usbOhciSetupPipe - invalid parameter"
                     "pHCDData = %p,pHCDPipe = %p,pSetupInfo = %p\n",
                      pHCDData,
                      pHCDPipe,
                      pSetupInfo,
                      4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }

    if (pSetupInfo->uMaxNumReqests == 0)
        {
        USB_OHCD_ERR("pSetupInfo->uMaxNumReqests  == 0\n",
                     1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }
    /*
     * The pipe setup must be performed when there is no active transfers
     */

    if (lstCount(&(pHCDPipe->requestList)) != 0)
        {
        USB_OHCD_ERR("usbOhciSetupPipe - requestList is not null\n"
                     "There are still active transfers in progress\n",
                      1, 2, 3, 4, 5, 6);
        return USBHST_INVALID_REQUEST;
        }

    /* Remove any previous setting */

    usbOhciUnSetupPipe(pHCDData, pHCDPipe);

    /* Copy transfer setup info */

    pHCDPipe->uMaxTransferSize = pSetupInfo->uMaxTransferSize;

    pHCDPipe->uMaxNumReqests = pSetupInfo->uMaxNumReqests;

    pHCDPipe->uFlags = pSetupInfo->uFlags;

    /* Create tag for mapping Data buffer */

    pHCDPipe->usrBuffDmaTagId = vxbDmaBufTagCreate (pHCDData->pDev,
        pHCDData->ohciParentTag,        /* parent */
        1,                              /* alignment */
        0,                              /* boundary */
        VXB_SPACE_MAXADDR_32BIT,        /* lowaddr */
        VXB_SPACE_MAXADDR,              /* highaddr */
        NULL,                           /* filter */
        NULL,                           /* filterarg */
        pHCDPipe->uMaxTransferSize,     /* max size */
        1,                              /* nSegments */
        pHCDPipe->uMaxTransferSize,     /* max seg size */
        pHCDData->usrDmaMapFlags,       /* flags */
        NULL,                           /* lockfunc */
        NULL,                           /* lockarg */
        NULL);                          /* ppDmaTag */

    if (pHCDPipe->usrBuffDmaTagId == NULL)
        {
        USB_OHCD_ERR("usbOhciSetupPipe - allocate usrBuffDmaTagId fail\n",
                     1, 2, 3, 4, 5, 6);
        usbOhciUnSetupPipe(pHCDData, pHCDPipe);
        return USBHST_INSUFFICIENT_MEMORY;
        }

    /* Control pipe needs special treatment for the Setup */

    if (pHCDPipe->uEndpointType == USBHST_CONTROL_TRANSFER)
        {
        /*
         * Create tag for mapping Setup buffer.
         * We create the TAG with 32 bytes instead of
         * 8 bytes (Setup packet size)
         */

        pHCDPipe->ctrlSetupDmaTagId = vxbDmaBufTagCreate (pHCDData->pDev,
            pHCDData->ohciParentTag,        /* parent */
            1,                              /* alignment */
            0,                              /* boundary */
            VXB_SPACE_MAXADDR_32BIT,        /* lowaddr */
            VXB_SPACE_MAXADDR,              /* highaddr */
            NULL,                           /* filter */
            NULL,                           /* filterarg */
            32,                             /* max size */
            1,                              /* nSegments */
            32,                             /* max seg size */
            pHCDData->usrDmaMapFlags,       /* flags */
            NULL,                           /* lockfunc */
            NULL,                           /* lockarg */
            NULL);                          /* ppDmaTag */

        if (pHCDPipe->ctrlSetupDmaTagId == NULL)
            {
            USB_OHCD_ERR("usbOhciSetupPipe - allocate "
                         "ctrlSetupDmaTagId fail\n",
                         1, 2, 3, 4, 5, 6);
            usbOhciUnSetupPipe(pHCDData, pHCDPipe);
            return USBHST_INSUFFICIENT_MEMORY;
            }
        }

    /* Create the requested number of request info structures */

    for (uReqIndex = 0; uReqIndex < pHCDPipe->uMaxNumReqests; uReqIndex++)
        {
        /* Create the request info */

        pRequest = usbOhciCreateRequestInfo(pHCDData, pHCDPipe);

        if (pRequest == NULL)
            {
            USB_OHCD_ERR("usbOhciSetupPipe - allocate "
                         "pRequest fail\n", 1, 2, 3, 4, 5, 6);

            usbOhciUnSetupPipe(pHCDData, pHCDPipe);
            return USBHST_INSUFFICIENT_MEMORY;
            }
        }

    return USBHST_SUCCESS;
    }

/*******************************************************************************
*
* usbOhciResetPipe - reset a pipe created already
*
* This routine is used to reset a pipe specific to an endpoint, which cancels
* all the pending requests on the pipe and force the data toggle to return
* DATA0 in the next transfer.
*
* RETURNS: USBHST_STATUS of the reset result.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbOhciResetPipe
    (
    pUSB_OHCD_DATA  pHCDData,
    pUSB_OHCD_PIPE  pHCDPipe
    )
    {
    /* Check if the request is for the Root hub and route it */

    if ((ULONG)pHCDPipe == (ULONG)USB_OHCI_ROOT_HUB_PIPE_HANDLE)
        {
        USB_OHCD_WARN("usbOhciResetPipe - reset root hub pipe\n",
                      1, 2, 3, 4, 5 ,6);
        return USBHST_SUCCESS;
        }

    /*
     * To reset pipe:
     * 1: Update pipe state to USB_OHCD_PIPE_STATE_RESET
          if the pipe is in normal or cancel state
     * 2: Add all requests into cancel list
     * 3: Call URB's callback function to notify the upper layer.
     * 4: Set "SKIP" bit of the ED
     * 5: Add this ED into the "DisabledEndpointList"
     * 6: Enable "SOF" interrupt
     *
     * In interrupt thread, scan "DisabledEndpointList" and do the
     * real clean/cancel action.
     */

    OS_WAIT_FOR_EVENT(pHCDPipe->PipeSynchEventID, WAIT_FOREVER);

    /*
     * Update uCurPipeState to USB_OHCD_PIPE_STATE_RESET only if the pipe
     * is in normal or cancel state.
     *
     * Here we don't reset the uNextPipeState to USB_OHCD_PIPE_STATE_NORMAL
     * as the uNextPipeState may be seted as USB_OHCD_PIPE_STATE_DELETE
     * if uCurPipeState == USB_OHCD_PIPE_STATE_CANCEL
     */

    if (pHCDPipe->uCurPipeState < USB_OHCD_PIPE_STATE_RESET)
        pHCDPipe->uCurPipeState = USB_OHCD_PIPE_STATE_RESET;

    /* Add all requests into cancel list */

    usbOhciPipeRmAllRequests(pHCDPipe);

    OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

    /*
     * Call urb's callback function before access the pipe event
     * As some Callback function may re-submit urb.
     */

    usbOhciPipeReturnAll(pHCDPipe);

    /*
     * Wait for the 'endpoint list access event' before updating the
     * list. This event is important to prevent simultaneous updation of
     * the endpoint descriptor list.
     * Enlarge the sempahore scope to prevent other task add the pipe
     * into disable list
     */

    OS_WAIT_FOR_EVENT(pHCDData->pipeListAccessEvent, WAIT_FOREVER);

    /* Set the SKIP bit for the endpoint */

    OHCI_SET_ED_SKIP_BIT(pHCDData->uBusIndex,
                         pHCDPipe->uControlInformation);
    /*
     * See section 5.2.7.1.3 OHCI Specification
     */

    OS_DELAY_MS(2);     /* Wait at least one frame for sKip to take effect */

    /*
     * Enter the critical section
     *
     * NOTE: This is requried because the interrupt handler also modifies
     *       the list of pipes to be deleted. The critical section ensures
     *       the list is consistent
     */

    OS_WAIT_FOR_EVENT(pHCDData->ohciCommonMutex, WAIT_FOREVER);

    /* Add the pipe into disable list */

    USB_OHCI_ADD_PIPE_INTO_DISABLE_LIST(pHCDData, pHCDPipe);

    OS_RELEASE_EVENT(pHCDData->ohciCommonMutex);

    /*
     * Release the 'endpoint list access event' before updating the
     * list. This event is important to prevent simultaneous updation of
     * the endpoint descriptor list.
     */

    OS_RELEASE_EVENT(pHCDData->pipeListAccessEvent);

    /* Enable SOF interrupt */

#ifndef USB_OHCI_POLLING_MODE

    /* Disable the interrupts */

    USB_OHCI_REG_WRITE (pHCDData->pDev,
                        USB_OHCI_INTERRUPT_DISABLE_REGISTER_OFFSET,
                        USB_OHCI_INTERRUPT_MASK);

    /* Enable the SOF interrupt */

    USB_OHCI_REG_WRITE (pHCDData->pDev,
                        USB_OHCI_INTERRUPT_ENABLE_REGISTER_OFFSET,
                       (USB_OHCI_INTERRUPT_MASK |
                        USB_OHCI_INTERRUPT_STATUS_SOF));
#else
    OS_DELAY_MS(2);
#endif

    return USBHST_SUCCESS;
    }

/*******************************************************************************
*
* usbOhciPipeControl - control the pipe characteristics
*
* This routine is used to control the pipe characteristics.
*
* <uBusIndex> specifies the host controller bus index.
* <uPipeHandle> holds the pipe handle.
* <pPipeCtrl> is the pointer to the USBHST_PIPE_CONTROL_INFO holding
*             the request details.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the OHCD. Assumption is that valid parameters are passed by the OHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the URB is submitted successfully.
*   USBHST_INVALID_PARAMETER - Returned if the parameters are not valid.
*   USBHST_INVALID_REQUEST - Returned if the request is not valid
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbOhciPipeControl
    (
    UINT8                      uBusIndex,   /* Index of the host controller */
    ULONG                      uPipeHandle, /* Pipe handle */
    USBHST_PIPE_CONTROL_INFO * pPipeCtrl /* Pointer to the pipe control info */
    )
    {
    /* Pointer to the HCD maintained pipe */

    pUSB_OHCD_DATA      pHCDData = NULL;

    /* Pointer to the HCD specific data structure */

    pUSB_OHCD_PIPE      pHCDPipe = NULL;

    /* The control result */

    USBHST_STATUS   hstSts = USBHST_FAILURE;

    /* Check the validity of the parameters */

    if ((USB_MAX_OHCI_COUNT <= uBusIndex) ||
        (0 == uPipeHandle) ||
        (NULL == pPipeCtrl))
        {
        USB_OHCD_ERR("usbOhciPipeControl - parameters are not valid"
                     "uBusIndex = %d,uPipeHandle = %p,pPipeCtrl = %p\n",
                      uBusIndex, uPipeHandle, pPipeCtrl,
                      4, 5, 6);
        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the global data structure */

    pHCDData = &g_OHCDData[uBusIndex];;

    /* Return if the global pointer is not valid */

    if (NULL == pHCDData || pHCDData->pDev == NULL )
        {
        USB_OHCD_ERR("usbOhciPipeControl - pHCDData is NULL\n",
                     1, 2, 3, 4, 5, 6);
        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the USB_EHCD_PIPE data structure */

    pHCDPipe = (pUSB_OHCD_PIPE)uPipeHandle;

    /* Do the pipe control based on the control code selection */

    switch (pPipeCtrl->uControlCode)
        {
        case USBHST_CMD_PIPE_RESET:
            {
            /*
             * Do pipe reset so that the pipe will be in DATA0 state
             * in next transfer
             */

            hstSts = usbOhciResetPipe(pHCDData, pHCDPipe);
            }

            break;

        case USBHST_CMD_TRANSFER_SETUP:
            {
            pUSB_TRANSFER_SETUP_INFO pSetupInfo = NULL;

            /* Root hub doesn't need any real DMA transfer */

            if (uPipeHandle == (ULONG)USB_OHCI_ROOT_HUB_PIPE_HANDLE)
                {
                USB_OHCD_VDBG("usbOhciPipeControl - setup root hub pipe\n",
                              1, 2, 3, 4, 5 ,6);
                return USBHST_SUCCESS;
                }

            /* For non-root hub device the setup should really happen */

            pSetupInfo = (pUSB_TRANSFER_SETUP_INFO)pPipeCtrl->uControlParam;

            if (pSetupInfo == NULL)
                {
                USB_OHCD_ERR("usbOhciPipeControl - USBHST_CMD_TRANSFER_SETUP "
                             "pSetupInfo NULL\n",
                             1, 2, 3, 4, 5, 6);
                return USBHST_INVALID_PARAMETER;
                }

            USB_OHCD_VDBG(
                "usbOhciPipeControl - USBHST_CMD_TRANSFER_SETUP for EP %d:\n"
                "uMaxTransferSize 0x%x uMaxNumReqests %d uFlags 0x%x\n",
                OHCI_GET_ENDPOINT_NUMBER_FROM_ED(uBusIndex, pHCDPipe->uControlInformation),
                pSetupInfo->uMaxTransferSize,
                pSetupInfo->uMaxNumReqests,
                pSetupInfo->uFlags,
                5, 6);


            /* Exclusively access the pipe request list */

            OS_WAIT_FOR_EVENT(pHCDPipe->PipeSynchEventID, WAIT_FOREVER);

            /*
             * Set up the transfer characteristics such as
             * creating proper DMA TAG and DMA MAPs
             */

            hstSts = usbOhciSetupPipe(pHCDData, pHCDPipe, pSetupInfo);

            /* Release the exclusive access */

            OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);
            }

            break;

        default :
            break;
        }

    return hstSts;
    }

/***************************************************************************
*
* usbOhciCreatePipe - creates the pipes
*
* This function is used to create the pipes for the endpoint. The function
* takes the endpoint descriptor as parameter.
*
* PARAMETERS: <uHostControllerIndex (IN)> - Host controller index corresponding
* to the OHCI controller.
*
* <uDeviceAddress (IN)> - USB address for the device.
*
* <uSpeed (IN)> - Speed of the device.
*
* <pEndpointDescriptor (IN)> - Pointer to the endpoint descriptor.
*
* <uHighSpeedHubInfo (IN)> - Specifies the nearest high speed hub and the port
* number information. This information will be used to handle a split transfer
* to the full/low speed device. The high byte will hold the high speed hub
* address. The low byte will hold the port number.
*
* Note: This parameter is not used in OHCI driver.
*
* <puPipeHandle (OUT)> - Pointer to the pipe handle corresponding to endpoint.
*
* RETURNS: USBHST_SUCCESS on success,
* USBHST_INVALID_PARAMETER if the parameters are not valid
* USBHST_INSUFFICIENT_MEMORY if the memory allocation fails
*
* ERRNO:
*   None.
*/

LOCAL USBHST_STATUS usbOhciCreatePipe
    (
    UINT8      uHostControllerIndex,
    UINT8      uDeviceAddress,
    UINT8      uSpeed,
    PUCHAR     pEndpointDescriptor,
    UINT16     uHighSpeedHubInfo,
    ULONG*     puPipeHandle
    )
    {

    /* To hold the pointer to the OHCI endpoint descriptor */

    pUSB_OHCD_PIPE   pHCDPipe = NULL;

    /* To hold the temporary pointer to the OHCI endpoint descriptor */

    pUSB_OHCD_PIPE   pTempEndpointDescriptor = NULL;

    /* To hold the pointer to the empty general transfer descriptor */

    PUSB_OHCI_TD     pEmptyTD = NULL;

    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA   pHCDData = NULL;

    /*
     * To hold the status of adding an interrupt or isochronous endpoint
     * descriptor to the periodic endpoint descriptor list.
     */

    USBHST_STATUS    nStatus = USBHST_FAILURE;

    /* To hold the pointer to the USB Endpoint Descriptor */

    PUSB_ENDPOINT_DESCRIPTOR   pUsbEndpointDescriptor = 0;

    /* To hold the endpoint number */

    UINT8                           uEndpointNumber = 0;

    /* To hold the endpoint direction */

    UINT8                           uEndpointDirection = 0;

    /* To hold the endpoint transfer type */

    UINT8                           uEndpointTransferType = 0;

    /* To hold the maximum packet size for the endpoint */

    UINT16                          uEndpointMaximumPacketSize = 0;

    /* To hold the format of the transfer descriptor (TD) */

    UINT8                           uTransferDescriptorFormat = 0;

    /*
     * Check whether the OHCI host controller index is valid
     * Check whether the device address is valid
     * Check whether the device speed is valid
     * Check whether the pointer to the pipe handle is valid
     */

    if ((uHostControllerIndex >= USB_MAX_OHCI_COUNT)||
        (g_OHCDData[uHostControllerIndex].pDev == NULL) ||
        (uDeviceAddress > 127)||
        ((uSpeed != USBHST_FULL_SPEED) && (uSpeed != USBHST_LOW_SPEED))||
        (NULL == puPipeHandle))
        {
        USB_OHCD_ERR("usbOhciCreatePipe - parameters are not valid"
                     "uHostControllerIndex = %d,uDeviceAddress = %d,"
                     "uSpeed = %d,puPipeHandle = %p\n",
                      uHostControllerIndex, uDeviceAddress, uSpeed,
                      puPipeHandle, 5, 6);
        return USBHST_INVALID_PARAMETER;
        }

    /* Obtain the pointer to the host controller information */

    pHCDData = &g_OHCDData[uHostControllerIndex];

    /*
     * Check whether the request is addressed to the root hub.
     *
     * NOTE: Two levels of check are required.
     *
     *       a) Check whether the pipe is created when the root hub
     *          is in default state. This condition will arise when the
     *          default control pipe (address 0, endpoint 0) is created
     *          as part of the OHCI controller initialization.
     *       b) Check whether the device address corresponds to the
     *          the root hub address.
     */

    if ((pHCDData->uRootHubState != USB_OHCI_DEVICE_DEFAULT_STATE) &&
        (pHCDData->uRootHubAddress == uDeviceAddress))
        {
        /*
         * Update the pipe handle parameter.
         *
         * NOTE: This is a dummy pipe handle created for the endpoints
         *       on the root hub.
         */
        *puPipeHandle = (ULONG) USB_OHCI_ROOT_HUB_PIPE_HANDLE;

        /* Return success. No pipes are created for the root hub. */

        return USBHST_SUCCESS;
        }

    /*
     * Check whether the USB endpoint desriptor pointer is valid.
     *
     * NOTE: No pipes are created for the root hub. Hence, validate the
     *       pHCDPipe parameter after confirming the request
     *       is not for the root hub.
     */

    if (pEndpointDescriptor == NULL)
        {
        USB_OHCD_ERR("invalid endpoint descriptor pointer\n",
                     1, 2, 3, 4, 5, 6);
        return USBHST_INVALID_PARAMETER;
        }

    /* Initialize the pointer to the USB endpoint descritpor */

    pUsbEndpointDescriptor = (PUSB_ENDPOINT_DESCRIPTOR) pEndpointDescriptor;

    /* Check whether the USB endpoint descriptor is valid */

    if (pUsbEndpointDescriptor->bDescriptorType !=
        USB_OHCI_ENDPOINT_DESCRIPTOR_TYPE)
        {
        /* Debug print */
        USB_OHCD_ERR("invalid endpoint descriptor type %d != 5 \n",
        pUsbEndpointDescriptor->bDescriptorType, 2, 3, 4, 5, 6);
        return USBHST_INVALID_PARAMETER;
        }

    /*
     * Check whether an attempt is made to create a pipe for the default
     * control endpoint. Duplication of default pipe is not allowed.
     */
    if ((uDeviceAddress == 0) &&
        (pHCDData->pDefaultPipe != NULL))
        {
        USB_OHCD_ERR("duplication of default pipe is not allowed \n",
                     1, 2, 3, 4, 5, 6);
        return USBHST_INVALID_PARAMETER;
        }


    /*
     * Allocate memory for the OHCI endpoint descriptor
     * In this routine, we will create PipeSynchEventID of this pipe
     */

    pHCDPipe = usbOhciFormEmptyPipe(pHCDData);

    /* Check whether the memory allocation was successful */

    if (pHCDPipe == NULL)
        {
        USB_OHCD_ERR("memory allocate failed for endpoint descriptor \n",
                     1, 2, 3, 4, 5, 6);
        return USBHST_INSUFFICIENT_MEMORY;
        }
    /* Obtain the endpoint transfer type */

    uEndpointTransferType =
        (UINT8)USB_GET_ENDPOINT_TRANSFER_TYPE(pUsbEndpointDescriptor->bmAttributes);

    /* Do some default setup for control transfer */

    if (uEndpointTransferType == USBHST_CONTROL_TRANSFER)
        {
        if (USBHST_SUCCESS !=
            usbOhciSetupControlPipe(pHCDData, pHCDPipe))
            {
            usbOhciCleanUpPipe(pHCDData, pHCDPipe);
            return USBHST_FAILURE;
            }
        }

    /*
     *
     * The reason for the empty transfer descriptor list is as follows:
     *
     * "The host controller stops processing the transfers when the
     *  pointer to the head of the transfer list is same as the pointer
     *  to the tail of the transfer list."
     *
     * Assume that one node is added to the transfer list. Since the head
     * and the tail of the transfer list will point to the same node, the
     * host controller will not service the transfer. Hence an empty
     * transfer descriptor is required at the end of the transfer list.
     */

    pEmptyTD = usbOhciGetFreeTD(pHCDData, pHCDPipe);

    if (pEmptyTD == NULL)
        {
        usbOhciCleanUpPipe(pHCDData, pHCDPipe);
        USB_OHCD_ERR("memory allocate failed for transfer descriptor \n",
                     1, 2, 3, 4, 5, 6);
        return USBHST_INSUFFICIENT_MEMORY;
        }

    /* Obtain the endpoint number */

    uEndpointNumber =
        (UINT8)USB_GET_ENDPOINT_NUMBER(pUsbEndpointDescriptor->bEndpointAddress);

    /* Obtain the endpoint direction */

    uEndpointDirection =
        (UINT8)USB_GET_ENDPOINT_DIRECTION(pUsbEndpointDescriptor->bEndpointAddress);

    /* Obtain the maximum packet size for the endpoint */

    uEndpointMaximumPacketSize =
        OS_UINT16_CPU_TO_LE(pUsbEndpointDescriptor->wMaxPacketSize);

    /*
     * Format the endpoint direction based on the transfer type and the endpoint
     * direction. This value will be populated in the OHCI endpoint descriptor.
     */

    if (uEndpointTransferType == USB_OHCI_CONTROL_TRANSFER)
        {
        /* Bidirectional endpoint */
        uEndpointDirection = OHCI_BIDIRECTIONAL_ENDPOINT;
        }
    else if (uEndpointDirection == USB_IN_ENDPOINT)
        {
        /* IN endpoint */
        uEndpointDirection = OHCI_IN_ENDPOINT;
        }
    else
        {
        /* OUT endpoint */
        uEndpointDirection = OHCI_OUT_ENDPOINT;
        }

    /* Obtain the format of the transfer descriptor */

    if (uEndpointTransferType == USB_OHCI_ISOCHRONOUS_TRANSFER)
        {
        /* Isochronous transfer descriptor */
        uTransferDescriptorFormat = OHCI_ISOCHRONOUS_TD_FORMAT;
        }
    else
        {
        /*
         * General transfer descriptor for control, bulk or interrupt
         * transfers
         */
        uTransferDescriptorFormat = OHCI_GENERAL_TD_FORMAT;
        }

    /* Populate the OHCI endpoint descriptor */

    OHCI_POPULATE_ED_CONTROL_INFO(uHostControllerIndex,
                                  pHCDPipe->uControlInformation,
                                  uDeviceAddress,
                                  uEndpointNumber,
                                  uEndpointDirection,
                                  uSpeed,
                                  uTransferDescriptorFormat,
                                  uEndpointMaximumPacketSize);


    /*
     * Since no transfers are pending, set the SKIP bit for the OHCI Endpoint
     * Descriptor.
     *
     * Even though the controller will not schedule the transfers if the
     * TDQueueHead is equal to TDQueueTail, by setting the SKIP bit, the
     * endpoint descriptor will be totally ignored, i.e. the host controller
     * will not attempt to check TDQueueHead and TDQueueTail.
     */

    OHCI_SET_ED_SKIP_BIT(uHostControllerIndex,
                         pHCDPipe->uControlInformation);


    /*
     * Initialize the pointer to the head and tail of the transfer descriptor
     * list
     */

    pHCDPipe->uTDQueueTailPointer =
           USB_OHCD_SWAP_DATA(uHostControllerIndex, USB_OHCI_DESC_LO32(pEmptyTD));
    pHCDPipe->uTDQueueHeadPointer=
           USB_OHCD_SWAP_DATA(uHostControllerIndex, USB_OHCI_DESC_LO32(pEmptyTD));

    /* Record the endpoint type */

    pHCDPipe->uEndpointType = uEndpointTransferType;

    /*
     * Copy the TD pointers in the head and tail pointers of the ED.
     * This is required while traversing the list for adding further TDs
     */

    pHCDPipe->pHCDTDTail = pEmptyTD;

    /*
     * Wait for the 'endpoint list access event' before updating the
     * list. This event is important to prevent simultaneous updation of
     * the endpoint descriptor list.
     */

    OS_WAIT_FOR_EVENT(pHCDData->pipeListAccessEvent, WAIT_FOREVER);

    /*
     * Check the transfer type and add the endpoint descriptor to the
     * corresponding endpoint descriptor list.
     */
    switch (uEndpointTransferType)
        {
        case USB_OHCI_CONTROL_TRANSFER:

            /*
             * Obtain the pointer to the tail of the control endpoint descriptor
             * list
             */

            pTempEndpointDescriptor = pHCDData->pControlPipeListTail;

            /* Check whether the current endpoint descriptor list is valid */

            if (pTempEndpointDescriptor != NULL)
                {
                /*
                 * Append the endpoint descriptor to the tail of the control
                 * endpoint descriptor list.
                 */

                pTempEndpointDescriptor->uNextEDPointer =
                  USB_OHCD_SWAP_DATA(uHostControllerIndex,
                                     USB_OHCI_DESC_LO32(pHCDPipe));



                /*
                 * Store the pointer in the HCD maintained next pointer.
                 * This would be requried while the ED is being deleted
                 */

                pTempEndpointDescriptor->pHCDNextPipe = pHCDPipe;

                }
             else
                {
                USB_OHCI_REG_WRITE(pHCDData->pDev,
                                   USB_OHCI_CONTROL_HEAD_ED_REGISTER_OFFSET,
                                   USB_OHCI_DESC_LO32(pHCDPipe));
                }

            /*
             * Update the pointer to the tail of the control endpoint descriptor
             * list
             */

            pHCDData->pControlPipeListTail = pHCDPipe;

            break;

        case USB_OHCI_BULK_TRANSFER:

            /*
             * Obtain the pointer to the tail of the bulk endpoint descriptor
             * list
             */

            pTempEndpointDescriptor = pHCDData->pBulkPipeListTail;

            /* Check whether the current endpoint descriptor list is valid */

            if (pTempEndpointDescriptor != NULL)
                {
                /*
                 * Append the endpoint descriptor to the tail of the bulk
                 * endpoint descriptor list.
                 */

                pTempEndpointDescriptor->uNextEDPointer =
                     USB_OHCD_SWAP_DATA(uHostControllerIndex,
                                        USB_OHCI_DESC_LO32(pHCDPipe));

                /*
                 * Store the pointer in the HCD maintained next pointer.
                 * This would be requried while the ED is being deleted
                 */

                pTempEndpointDescriptor->pHCDNextPipe = pHCDPipe;

                }
            else
                {
                /* Program the HcBulkHeadED register */

                USB_OHCI_REG_WRITE(pHCDData->pDev ,
                                   USB_OHCI_BULK_HEAD_ED_REGISTER_OFFSET,
                                   USB_OHCI_DESC_LO32(pHCDPipe));

                }

            /*
             * Update the pointer to the tail of the bulk endpoint descriptor
             * list
             */

             pHCDData->pBulkPipeListTail = pHCDPipe;

            break;

        case USB_OHCI_INTERRUPT_TRANSFER:

            /* Compute the bandwidth required for the endpoint */

            uEndpointMaximumPacketSize =
                (UINT16)USB_OHCI_COMPUTE_BANDWIDTH_FOR_INTERRUPT_ENDPOINT(
                    uSpeed,
                    uEndpointDirection,
                    uEndpointMaximumPacketSize);

            /*
             * Call the function to add the new endpoint descriptor to the
             * interrupt endpoint descriptor list.
             */

            nStatus = usbOhciAddNodeToEndpointDescriptorList(
                          uHostControllerIndex,
                          pHCDPipe,
                          pUsbEndpointDescriptor->bInterval,
                          uEndpointMaximumPacketSize);

            /*
             * Check whether the new endpoint descriptor was successfully added
             * to the interrupt endpoint descriptor list.
             */

            if (nStatus != USBHST_SUCCESS)
                {
                usbOhciAddToFreeTDList(pHCDData,
                                       pHCDPipe,
                                       pHCDPipe->pHCDTDTail);

                pHCDPipe->pHCDTDTail = NULL;

                usbOhciCleanUpPipe(pHCDData, pHCDPipe);
                pHCDPipe = NULL;
                OS_RELEASE_EVENT(pHCDData->pipeListAccessEvent);
                return USBHST_INSUFFICIENT_BANDWIDTH;
                }
            break;

        case USB_OHCI_ISOCHRONOUS_TRANSFER:

            /* Compute the bandwidth required for the endpoint */

            uEndpointMaximumPacketSize =
                (UINT16)USB_OHCI_COMPUTE_BANDWIDTH_FOR_ISOCHRONOUS_ENDPOINT(
                    uSpeed,
                    uEndpointDirection,
                    uEndpointMaximumPacketSize);

            /*
             * Call the function to add the new endpoint descriptor to the
             * interrupt endpoint descriptor list.
             */

            nStatus = usbOhciAddNodeToEndpointDescriptorList(
                          uHostControllerIndex,
                          pHCDPipe,
                          USB_OHCI_ISOCHRONOUS_ENDPOINT_POLLING_INTERVAL,
                          uEndpointMaximumPacketSize);

            /*
             * Check whether the new endpoint descriptor was successfully added
             * to the interrupt endpoint descriptor list.
             */

            if (nStatus != USBHST_SUCCESS)
                {
                /* Delete the transfer descriptor */

                usbOhciAddToFreeTDList(pHCDData,
                                       pHCDPipe,
                                       pHCDPipe->pHCDTDTail);

                pHCDPipe->pHCDTDTail = NULL;

                usbOhciCleanUpPipe(pHCDData, pHCDPipe);
                pHCDPipe = NULL;
                OS_RELEASE_EVENT(pHCDData->pipeListAccessEvent);
                return USBHST_INSUFFICIENT_BANDWIDTH;
                }

            break;

        default:

            /* Free the transfer descriptor */

            usbOhciAddToFreeTDList(pHCDData,
                                   pHCDPipe,
                                   pHCDPipe->pHCDTDTail);

            pHCDPipe->pHCDTDTail = NULL;

            usbOhciCleanUpPipe(pHCDData, pHCDPipe);
            pHCDPipe = NULL;
            OS_RELEASE_EVENT(pHCDData->pipeListAccessEvent);
            return USBHST_FAILURE;
        };

    /*
     * Release the 'endpoint list access event' before updating the
     * list. This event is important to prevent simultaneous updation of
     * the endpoint descriptor list.
     */

    OS_RELEASE_EVENT(pHCDData->pipeListAccessEvent);

    /* Populate the endpoint descriptor (END) */

    /* Update the pipe parameter */

    *puPipeHandle = (ULONG) pHCDPipe;

    /* Return the status of the create pipe operation */

    return USBHST_SUCCESS;
    } /* End of function usbOhciCreatePipe () */


/***************************************************************************
*
* usbOhciDeletePipe - deletes a pipe
*
* This function is used to delete a pipe corresponding to an endpoint.
*
* PARAMETERS: <uHostControllerIndex (IN)> - Host controller index corresponding
* to the OHCI controller.
*
* <uPipeHandle (IN)> - Handle to the pipe to be deleted.
*
* RETURNS: USBHST_SUCCESS on success,
* USBHST_INVALID_PARAMETER if the parameters are not valid
*
* ERRNO:
*   None.
*/

LOCAL USBHST_STATUS usbOhciDeletePipe
    (
    UINT8  uHostControllerIndex,
    ULONG  uPipeHandle
    )
    {
    /* To hold the pointer to the endpoint descriptor */

    pUSB_OHCD_PIPE   pHCDPipe = NULL;

    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA   pHCDData = NULL;

    pUSBHST_URB      pUrb  = NULL;


    /* Check whether the OHCI host controller index is valid */
    /* Check whether the pipe parameter is valid */

    if ((uHostControllerIndex >= USB_MAX_OHCI_COUNT) ||
        (g_OHCDData[uHostControllerIndex].pDev == NULL) ||
        (uPipeHandle == USB_OHCI_INVALID_PIPE_HANDLE))
        {
        /* Debug print */
        USB_OHCD_ERR("invalid parameter:uHostControllerIndex = %d, "
                     "uPipeHandle =%p\n",
                     uHostControllerIndex, uPipeHandle,
                     3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Obtain the pointer to the host controller information */

    pHCDData = &g_OHCDData[uHostControllerIndex];

    /*
     * Check whether the pipe handle to corresponds to the pipes on
     * the root hub.
     *
     * NOTE: A dummy pipe handle created for the endpoints on the
     *       root hub.
     */
    if (uPipeHandle == (ULONG)USB_OHCI_ROOT_HUB_PIPE_HANDLE)
        {
        /*
         * Check whether the status change URB is pending for the
         * root hub
         */
        if (NULL != pHCDData->pRootHubInterruptRequest)
            {
            /* To hold the temporary pointer to the URB */
            pUrb = pHCDData->pRootHubInterruptRequest;

            /*
             * Reset the status change URB information in the OHCI
             * information
             */
            pHCDData->pRootHubInterruptRequest = NULL;

            /*
             * Update Urb' status as USBHST_TRANSFER_CANCELLED
             * call it's callback routine
             */

            USB_OHCD_CALL_URB_CALLBACK(pUrb, USBHST_TRANSFER_CANCELLED);
            }

        return USBHST_SUCCESS;
        }

    /*
     * NOTE: It is assumed that the USBD will provide a valid pipe handle.
     *       If this handle is invalid, the HCD driver might crash.
     *
     *       Since the pipe handle is visible only to HCD and USBD, the
     *       check has been intensionaly removed to reduce the code size.
     *
     *       However, if needed, the pipe handle can be compared against
     *       the list of endpoint descriptors for every transfer.
     */

    /*
     * To delete pipe, we will do the following action
     *
     * 1: If the pipe is already added into disable list for delete
     * 2: Add all request into cancel list
     * 3: Call URB's callback function to notify the upper
     * 4: return USBHST_SUCCESS to indicate success
     *
     * 1: if the pipe is already added into disable list for cancel or reset
     * 2: Set "uNextPipeState" to USB_OHCD_PIPE_STATE_DELETE
     * 3: Add all request into cancel list
     * 4: disable the transfer list processing
     * 5: Call URB's callback function to notify the upper
     * 6: Set "SKIP" bit in the ED
     * 7: Add ED into the disable list
     * 8: Enable SOF interrupt
     *
     * 1: If the pipe is not in the disable list
     * 2: Set "uCurPipeState" to USB_OHCD_PIPE_STATE_DELETE
     * 3: Add all request into cancel list
     * 4: disable the transfer list processing
     * 5: Call URB's callback function to notify the upper
     * 6: Set "SKIP" bit in the ED
     * 7: Add ED into the disable list
     * 8: Enable SOF interrupt
     *
     * In the interrupt thread, we will do the real clean action
     */

    /* Initialize the endpoint descriptor */

    pHCDPipe = (pUSB_OHCD_PIPE) uPipeHandle;

    OS_WAIT_FOR_EVENT(pHCDPipe->PipeSynchEventID, WAIT_FOREVER);

    /*
     * Check if the pipe is already added into disable list for other
     * clean request.
     */

    if ((pHCDPipe->uCurPipeState == USB_OHCD_PIPE_STATE_DELETE)||
        (pHCDPipe->uNextPipeState == USB_OHCD_PIPE_STATE_DELETE))
        {
        USB_OHCD_DBG("usbOhciDeletePipe - Pipe %p is alreay in delete state\n",
                     (ULONG)pHCDPipe, 2, 3, 4, 5, 6);

        /* Make sure all the request is added into cancel list */

        usbOhciPipeRmAllRequests(pHCDPipe);

        OS_RELEASE_EVENT(pHCDData->pipeListAccessEvent);

        /* Make sure all the URB's callback is called */

        usbOhciPipeReturnAll(pHCDPipe);

        OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

        return USBHST_SUCCESS;
        }
    else if (pHCDPipe->uCurPipeState != USB_OHCD_PIPE_STATE_NORMAL)
        {
        USB_OHCD_DBG("usbOhciDeletePipe - Pipe %p CurState %d != NORMAL\n",
                     (ULONG)pHCDPipe, pHCDPipe->uCurPipeState,
                     3, 4, 5, 6);
        pHCDPipe->uNextPipeState = USB_OHCD_PIPE_STATE_DELETE;
        }
    else
        {
        pHCDPipe->uCurPipeState = USB_OHCD_PIPE_STATE_DELETE;
        pHCDPipe->uNextPipeState = USB_OHCD_PIPE_STATE_NORMAL;
        }

    usbOhciPipeRmAllRequests(pHCDPipe);

    /* Disable the transfer list processing */

    USB_OHCI_DISABLE_TRANSFER_LIST(pHCDData, pHCDPipe);

    OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

    /* Call URB's callback function to notify the upper */

    usbOhciPipeReturnAll(pHCDPipe);

    /*
     * Wait for the 'endpoint list access event' before updating the
     * list. This event is important to prevent simultaneous updation of
     * the endpoint descriptor list.
     * Enlarge the sempahore scope to prevent other task add the pipe
     * into disable list
     */

    OS_WAIT_FOR_EVENT(pHCDData->pipeListAccessEvent, WAIT_FOREVER);

    /*
     * Set the SKIP bit for the endpoint . This is done as the
     * interrupt handler uses this as an indication for signalling an
     * event if the TDs are completed by the host controller
     */

    OHCI_SET_ED_SKIP_BIT(uHostControllerIndex,
                         pHCDPipe->uControlInformation);

    /*
     * Enter the critical section
     *
     * NOTE: This is requried because the interrupt handler also modifies
     *       the list of pipes to be deleted. The critical section ensures
     *       the list is consistent
     */

    OS_WAIT_FOR_EVENT(pHCDData->ohciCommonMutex, WAIT_FOREVER);

    /* Add the pipe into disable list */

    USB_OHCI_ADD_PIPE_INTO_DISABLE_LIST(pHCDData, pHCDPipe);

    /* Leave the critical section */

    OS_RELEASE_EVENT(pHCDData->ohciCommonMutex);

    /*
     * Release the 'endpoint list access event' before updating the
     * list. This event is important to prevent simultaneous updation of
     * the endpoint descriptor list.
     */

    OS_RELEASE_EVENT(pHCDData->pipeListAccessEvent);

    /* Enable SOF interrupt */

#ifndef USB_OHCI_POLLING_MODE

    /* Disable the interrupts */
    USB_OHCI_REG_WRITE (pHCDData->pDev,
                        USB_OHCI_INTERRUPT_DISABLE_REGISTER_OFFSET,
                        USB_OHCI_INTERRUPT_MASK);

    /* Enable the SOF interrupt */
    USB_OHCI_REG_WRITE (pHCDData->pDev,
                         USB_OHCI_INTERRUPT_ENABLE_REGISTER_OFFSET,
                         (USB_OHCI_INTERRUPT_MASK |
                         USB_OHCI_INTERRUPT_STATUS_SOF));
#else
    /* Ensure that the list is disabled */
    OS_DELAY_MS(2);
#endif

    return USBHST_SUCCESS;
    } /* End of function usbOhciDeletePipe () */

/***************************************************************************
*
* usbOhciSubmitUrb - submit a request
*
* This function is used to submit a request to the device.
*
* PARAMETERS: <uHostControllerIndex (IN)> - OHCI Host Controller index.
*
* <uPipeHandle (IN)> - Handle to the pipe. The URB will be submitted for this
* pipe.
*
* <pURB (IN)> - Pointer to the URB to be submitted.
*
* RETURNS: USBHST_SUCCESS on success,
* USBHST_INVALID_PARAMETER if the parameters are not valid
*
* ERRNO:
*   None.
*/

USBHST_STATUS usbOhciSubmitUrb
    (
    UINT8           uHostControllerIndex,
    ULONG           uPipeHandle,
    pUSBHST_URB     pUrb
    )
    {
    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA         pHCDData = NULL;

    /* To hold the pointer to the OHCI endpoint descriptor */

    pUSB_OHCD_PIPE         pHCDPipe = NULL;

    /* To hold the status of populating the transfer descriptor */

    USBHST_STATUS          nStatus = USBHST_SUCCESS;

    pUSB_OHCD_REQUEST_INFO pRequest = NULL;


    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT(
        USB_OHCI_WV_TRANSFER,
        "usbOhciSubmitUrb() starts",
        USB_OHCD_WV_FILTER);

    /* Check whether the OHCI host controller index is valid */
    /* Check whether the URB parameter is valid */

    if ((uHostControllerIndex >= USB_MAX_OHCI_COUNT)||
        (g_OHCDData[uHostControllerIndex].pDev == NULL)||
        (NULL == pUrb))
        {
        USB_OHCD_ERR("usbOhciSubmitUrb - "
            "invalid parameter:uHostControllerIndex = %d, pUrb = %p\n",
            uHostControllerIndex, pUrb, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }

    /*
     * Check the URB::pTransferBuffer. The ::pTransferBuffer can be NULL for
     * zero length transfer. However, in this case the URB::uTransferLength
     * should be zero.
     */

    if ((pUrb->pTransferBuffer == NULL) && (pUrb->uTransferLength != 0))
        {
        USB_OHCD_ERR("usbOhciSubmitUrb - invalid parameter:"
            "pUrb->pTransferBuffer = %p,pUrb->uTransferLength = %d\n",
            pUrb->pTransferBuffer, pUrb->uTransferLength, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Obtain the pointer to the host controller information */

    pHCDData = &g_OHCDData[uHostControllerIndex];

    /*
     * Check whether the root hub is configured.
     *
     * NOTE: If the root hub is not configured, all the requests will be
     *       directed to the root hub emulation module.
     */

    if (pHCDData->uRootHubState != USB_OHCI_DEVICE_CONFIGURED_STATE)
        {
        /* Call the root hub emulation function to handle the request */

        nStatus = usbOhciProcessRootHubRequest (uHostControllerIndex, pUrb);

        USB_OHCD_VDBG("usbOhciSubmitUrb - "
                      "root hub is not configured, all urb will submit to RH \n",
                      1, 2, 3, 4, 5, 6);

        /* Return the status of the root hub emulation function */

        return nStatus;
        }

    /* Check whether the request is addressed to the root hub */

    if (pHCDData->uRootHubAddress ==
        (pUrb->hDevice & USB_OHCI_DEVICE_ADDRESS_MASK))
        {
        USB_OHCD_VDBG("usbOhciSubmitUrb - submit a urb for root hub \n",
            1, 2, 3, 4, 5, 6);

        /* Call the root hub emulation function to handle the request */

        nStatus = usbOhciProcessRootHubRequest (uHostControllerIndex, pUrb);


        /* Return the status of the root hub emulation function */

        return nStatus;
        }

    /*
     * Check whether the pipe parameter is valid.
     *
     * NOTE: No pipes are created for the root hub. Hence validate the
     *       pipe parameter after confirming the request is not for the
     *       root hub.
     */

    if (uPipeHandle == USB_OHCI_INVALID_PIPE_HANDLE)
        {
        /* Debug print */

        USB_OHCD_ERR("usbOhciSubmitUrb - invalid pipe handle \n",
                     1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }

#ifdef DEBUG
    /* Check whether the pipe is valid */

    nStatus = usbOhciIsValidPipe (uHostControllerIndex, uPipeHandle);

    /* If the pipe is invalid, return */

    if (nStatus != USBHST_SUCCESS)
        {
        /* Debug print */

        USB_OHCD_ERR("usbOhciSubmitUrb - check it is a invalid pipe handle \n",
                     1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }
#else
    /*
     * NOTE: It is assumed that the USBD will provide a valid pipe handle.
     *       If this handle is invalid, the HCD driver might crash.
     *
     *       Since the pipe handle is visible only to HCD and USBD, the
     *       check has been intensionaly removed to reduce the code size.
     *
     *       However, if needed, the pipe handle can be compared against
     *       the list of endpoint descriptors for every transfer.
     */
#endif /* End of #ifdef DEBUG */

    /* Initialize the OHCI endpoint descriptor */

    pHCDPipe = (pUSB_OHCD_PIPE) uPipeHandle;

    if ((pHCDPipe->uCurPipeState == USB_OHCD_PIPE_STATE_DELETE)||
        (pHCDPipe->uNextPipeState == USB_OHCD_PIPE_STATE_DELETE))
        {
        USB_OHCD_ERR("usbOhciSubmitUrb - still submit urb to delte pipe\n",
                     1, 2, 3, 4, 5, 6);
        return USBHST_FAILURE;
        }

    /* Exclusively access the pipe request list */

    OS_WAIT_FOR_EVENT(pHCDPipe->PipeSynchEventID, WAIT_FOREVER);

    /* Allocate memory for the URB list element */

    pRequest = usbOhciReserveRequestInfo(pHCDData,
                                         pHCDPipe,
                                         pUrb);
    if (NULL == pRequest)
        {
        /* Debug print */

        USB_OHCD_ERR("usbOhciSubmitUrb - "
                     "usbOhciReserveRequestInfo fail \n",
                      1, 2, 3, 4, 5, 6);

        OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

        return USBHST_INSUFFICIENT_MEMORY;
        }

    /*
     * Check the transfer type for the endpoint and populate the
     * transfer descriptor accordingly.
     *
     * NOTE: The following is the logic for identifying the transfer type:
     *
     *       a) If the ENDPOINT_DESCRIPTOR::FORMAT bit is set, it indicates
     *          an isochronous endpoint. If not it can be a control, bulk
     *          or interrupt endpoint.
     *
     *       b) If the ENDPOINT_DESCRIPTOR::FORMAT bit is not set and
     *          the ENDPOINT_DESCRIPTOR::DIRECTION bit is either OUT or IN
     *          it means a bulk or interrupt endpoint.
     *
     *          The logic for populating the bulk or interrupt endpoint is
     *          same, i.e. the populating the transfer descriptor is same.
     *          However, the endpoint will be queued in the corresponding
     *          endpoint list. The queuing is done when the endpoint is
     *          created.
     *
     *       c) If the ENDPOINT_DESCRIPTOR::FORMAT bit is not set and
     *          the ENDPOINT_DESCRIPTOR::DIRECTION bit is GET DIRECTION
     *          FROM TD, it means a control endpoint.
     */

    if (OHCI_IS_CONTROL_ENDPOINT(
                uHostControllerIndex,
                pHCDPipe->uControlInformation) != 0)
        {
        nStatus = usbOhciSubmitControlUrb(pHCDData,
                                          pHCDPipe,
                                          pUrb,
                                          pRequest);
        }
    else if (OHCI_IS_BULK_OR_INTERRUPT_ENDPOINT(
                uHostControllerIndex,
                pHCDPipe->uControlInformation) != 0)
        {
        nStatus = usbOhciSubmitBulkOrInterruptUrb(pHCDData,
                                                  pHCDPipe,
                                                  pUrb,
                                                  pRequest);
        }
    else if (OHCI_IS_ISOCHRONOUS_ENDPOINT(
                uHostControllerIndex,
                pHCDPipe->uControlInformation) != 0)
        {
        nStatus = usbOhciSubmitIsochronousUrb(pHCDData,
                                              pHCDPipe,
                                              pUrb,
                                              pRequest);
        }

    if (USBHST_SUCCESS != nStatus)
        {
        USB_OHCD_ERR("usbOhciSubmitUrb - URB submit fail (%d)\n",
                     nStatus, 2, 3, 4, 5, 6);

        /* Return the request info to the pipe */

        usbOhciReleaseRequestInfo(pHCDData,
                                  pHCDPipe,
                                  pRequest);

        }

    /* Release the exclusive access */

    OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

    /* Return the status of submitting the transfer */

    return nStatus;
    } /* End of function usbOhciSubmitUrb () */

/***************************************************************************
*
* usbOhciCancelUrb - cancel a request
*
* This function is used to cancel a request submitted to the device.
*
* PARAMETERS: <uHostControllerIndex (IN)> - OHCI Host Controller index.
*
* <uPipeHandle (IN)> - Handle to the pipe. The URB to be cancelled should have
* been submitted for this pipe.
*
* <pURB (IN)> - Pointer to the URB to be cancelled.
*
* RETURNS: USBHST_SUCCESS on success, USBHST_INVALID_PARAMETER if the parameters
* are not valid, USBHST_INVALID_REQUEST if the request is not pending,
* USBHST_TRANSFER_COMPLETED if the request is completed before cancellation OR
* USBHST_INSUFFICIENT_MEMORY if the cancellation operation could not be
* initiated due to insufficient resources
*
* ERRNO:
*   None.
*/

USBHST_STATUS usbOhciCancelUrb
    (
    UINT8           uHostControllerIndex,
    ULONG           uPipeHandle,
    pUSBHST_URB     pUrb
    )
    {
    /* To hold the pointer to the endpoint descriptor */

    pUSB_OHCD_PIPE           pHCDPipe = NULL;

    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA           pHCDData = NULL;

    /* To hold the pointer of the request info */

    pUSB_OHCD_REQUEST_INFO   pRequest = NULL;

	NODE                    *pNode = NULL;

    /* WindView Instrumentation */
    USB_HCD_LOG_EVENT(
        USB_OHCI_WV_TRANSFER,
        "usbOhciCancelUrb() starts",
        USB_OHCD_WV_FILTER);

    /* Check whether the OHCI host controller index is valid */
    /* Check whether the pipe parameter is valid */

    if ((uHostControllerIndex >= USB_MAX_OHCI_COUNT)||
        (g_OHCDData[uHostControllerIndex].pDev == NULL)||
        (uPipeHandle == USB_OHCI_INVALID_PIPE_HANDLE) ||
        (pUrb == NULL))
        {
        /* Debug print */
        USB_OHCD_ERR(
            "Invalid parameter: "
            "uHostControllerIndex = %d, uPipeHandle = %p, pUrb = %p\n",
            uHostControllerIndex,
            uPipeHandle,
            pUrb,
            4, 5, 6);
        return USBHST_INVALID_PARAMETER;
        }

    /* Obtain the pointer to the host controller information */

    pHCDData = &g_OHCDData[uHostControllerIndex];

    /* Check whether the request is addressed to the root hub */

    if (pHCDData->uRootHubAddress ==
        (pUrb->hDevice & USB_OHCI_DEVICE_ADDRESS_MASK))
        {
        /* Reset the root hub URB pointer */

        pHCDData->pRootHubInterruptRequest = NULL;
        /* Debug print */

        USB_OHCD_VDBG("cancel a urb addressed for RH \n",
                      1, 2, 3, 4, 5, 6);
        return USBHST_SUCCESS;
        }

    /*
     * NOTE: It is assumed that the USBD will provide a valid pipe handle.
     *       If this handle is invalid, the HCD driver might crash.
     *
     *       Since the pipe handle is visible only to HCD and USBD, the
     *       check has been intensionaly removed to reduce the code size.
     *
     *       However, if needed, the pipe handle can be compared against
     *       the list of endpoint descriptors for every transfer.
     */

    /* Initialize the endpoint descriptor */

    pHCDPipe = (pUSB_OHCD_PIPE) uPipeHandle;

    OS_WAIT_FOR_EVENT(pHCDPipe->PipeSynchEventID, WAIT_FOREVER);

    /* Check if this URB has already been canncelled */

    if (pUrb->pHcdSpecific == pRequest)
        {
        /* Release the exclusive access */
        OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);
        return USBHST_INVALID_REQUEST;
        }

    /* Check if this URB are valid */

    pNode = lstFirst(&(pHCDPipe->requestList));

	while (pNode != NULL)
		{
		pRequest =  LIST_NODE_TO_USB_OHCD_REQUEST_INFO(pNode);
		if ((NULL == pRequest) || (pRequest->pUrb == pUrb))
            break;
		pNode = lstNext(pNode);
		}

    if (NULL == pRequest)
        {
        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);
        return USBHST_INVALID_REQUEST;
        }

    /*
     * If there is any real data transfer, we should do
     * vxbDmaBufSync and unload the DMA MAP.
     */

    if (pRequest->uRequestLength != 0)
        {
        vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
            pRequest->usrDataDmaMapId);
        }

    /* Control transfer should also deal with the Setup buffer */

    if (pHCDPipe->uEndpointType == USBHST_CONTROL_TRANSFER)
        {
        vxbDmaBufMapUnload (pHCDPipe->ctrlSetupDmaTagId,
                            pRequest->ctrlSetupDmaMapId);
        }

    /*
     * To Cancel a URB, we will do the following:
     * 1: Set "uCurPipeState" to USB_OHCD_PIPE_STATE_CANCEL if it is
     *    USB_OHCD_PIPE_STATE_NORMAL
     * 2: Add request into cancel list
     * 3: Call URB's callback function to notify the upper layer.
     * 4: Set "SKIP" bit of the ED
     * 5: Add this ED into the "DisabledEndpointList"
     * 6: Enable "SOF" interrupt
     * 7: Return USBHST_SUCCESS to indicate cancel succes.
     *
     * In interrupt thread, Scan "DisabledEndpointList" and do the
     * real clean/cancel action.
     */

    /*
     * Update uCurPipeState only if the pipe is in normal state
     * or else it means this pipe is already added into the disable
     * list and wait to do clean(cancel/reset/delete) action.
     */

    if (pHCDPipe->uCurPipeState < USB_OHCD_PIPE_STATE_CANCEL)
        {
        pHCDPipe->uCurPipeState = USB_OHCD_PIPE_STATE_CANCEL;
        pHCDPipe->uNextPipeState = USB_OHCD_PIPE_STATE_NORMAL;
        }

    ADD_REQEUEST_INTO_CANCEL_LIST(pHCDPipe, pRequest);

    OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

    /* Set urb's status as cancelled, call callback function to notify upper */

    USB_OHCD_CALL_URB_CALLBACK(pUrb, USBHST_TRANSFER_CANCELLED);

    /*
     * URB submitter may release the pUrb as soon as the callback routine is
     * called. So it's dangerous to access the resources of the pUrb
     * Set pRequest->pUrb to NULL to avoid this situation.
     */

    pRequest->pUrb = NULL;

    /*
     * Wait for the 'endpoint list access event' before updating the
     * list. This event is important to prevent simultaneous updation of
     * the endpoint descriptor list.
     * Enlarge the semaphore scope to prevent other task add the pipe
     * into disable list
     */

    OS_WAIT_FOR_EVENT(pHCDData->pipeListAccessEvent, WAIT_FOREVER);

    /* Set the SKIP bit for the endpoint */

    OHCI_SET_ED_SKIP_BIT(uHostControllerIndex,
                         pHCDPipe->uControlInformation);
    /*
     * See section 5.2.7.1.3 OHCI Specification
     */

    OS_DELAY_MS(2);     /* Wait at least one frame for sKip to take effect */

    /*
     * Enter the critical section
     *
     * NOTE: This is requried because the interrupt handler also modifies
     *       the list of pipes to be deleted. The critical section ensures
     *       the list is consistent
     */

    OS_WAIT_FOR_EVENT(pHCDData->ohciCommonMutex, WAIT_FOREVER);

    /* Add the pipe into disable list */

    USB_OHCI_ADD_PIPE_INTO_DISABLE_LIST(pHCDData, pHCDPipe);

    OS_RELEASE_EVENT(pHCDData->ohciCommonMutex);

    /*
     * Release the 'endpoint list access event' before updating the
     * list. This event is important to prevent simultaneous updation of
     * the endpoint descriptor list.
     */

    OS_RELEASE_EVENT(pHCDData->pipeListAccessEvent);

    /* Enable SOF interrupt */

#ifndef USB_OHCI_POLLING_MODE

    /* Disable the interrupts */

    USB_OHCI_REG_WRITE (pHCDData->pDev,
                        USB_OHCI_INTERRUPT_DISABLE_REGISTER_OFFSET,
                       USB_OHCI_INTERRUPT_MASK);

    /* Enable the SOF interrupt */

    USB_OHCI_REG_WRITE (pHCDData->pDev,
                        USB_OHCI_INTERRUPT_ENABLE_REGISTER_OFFSET,
                       (USB_OHCI_INTERRUPT_MASK |
                        USB_OHCI_INTERRUPT_STATUS_SOF));
#else
    OS_DELAY_MS(2);
#endif

    return USBHST_SUCCESS;
    } /* End of function usbOhciCancelUrb () */

/***************************************************************************
*
* usbOhciIsBandwidthAvailable - determines whether the bandwidth is available
*
* This function is used to determine whether the bandwidth is available to
* support the new configuration or an alternate interface.
*
* PARAMETERS: <uHostControllerIndex (IN)> - OHCI Host Controller index.
*
* <uDeviceAddress (IN)> - Specifies the address of the device.
*
* <uDeviceSpeed (IN)> - Specifies the speed of the device.
*
* <pCurrentDescriptor (IN)> - Pointer to the current configuration or interface
* descriptor.
*
* Note: For SetConfiguration() request, this parameter is not used.
*
* <pNewDescriptor (IN)> - Pointer to the new configuration or interface
* descriptor.
*
* Note: When the configuration descriptor is passed, it should include all the
* interface and endpoint descriptors.
*
* When the interface descriptor is passed, it should include all the endpoint
* descriptors corresponding to that interface.
*
* RETURNS: USBHST_SUCCESS on success,
* USBHST_INVALID_PARAMETER if the parameters are not valid
*
* ERRNO:
*   None.
*/

USBHST_STATUS usbOhciIsBandwidthAvailable
    (
    UINT8    uHostControllerIndex,
    UINT8    uDeviceAddress,
    UINT8    uDeviceSpeed,
    PUCHAR   pCurrentDescriptor,
    PUCHAR   pNewDescriptor
    )
    {
    /* To hold the available bandwidth information on the all the frames */

    UINT32  uAvailableBandwidth[USB_OHCI_MAXIMUM_POLLING_INTERVAL];

    /* To hold the polling interval */

    UINT32                          uPollingInterval = 0;

    /* To hold the pointer to the general descriptor */

    PUSB_OHCI_GENERIC_DESCRIPTOR    pGeneralDescriptor = NULL;

    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA                  pHCDData = NULL;

    /* To hold the pointer to the base address of the periodic list */

    PUINT32       pPeriodicList = NULL;

    /*
     * To hold the pointer to the current endpoint desriptor node while
     * traversing the endpoint descriptor list.
     */

    pUSB_OHCD_PIPE       pTempEndpointDescriptor = NULL;

    /*
     * To hold the status of reserving bandwidth for the new configuration or
     * interface.
     */

    USBHST_STATUS                   nStatus = USBHST_INSUFFICIENT_BANDWIDTH;


    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT(
        USB_OHCI_WV_TRANSFER,
        "usbOhciIsBandwidthAvailable() starts",
        USB_OHCD_WV_FILTER);

    /* Check whether the OHCI host controller index is valid */
    /* Check whether the new descriptor is valid */

    if ((USB_MAX_OHCI_COUNT <= uHostControllerIndex)||
        (g_OHCDData[uHostControllerIndex].pDev == NULL)||
        (NULL == pNewDescriptor))
        {
        return USBHST_INVALID_PARAMETER;
        }

    /* Initialize the pointer to the general descriptor */

    pGeneralDescriptor = (PUSB_OHCI_GENERIC_DESCRIPTOR)pNewDescriptor;

    /*
     * Check whether the new descriptor corresponds to the interface
     * descriptor, check whether the old interface descriptor pointer
     * is valid
     */

    if ((USBHST_INTERFACE_DESC == pGeneralDescriptor->bDescriptorType) &&
        (NULL == pCurrentDescriptor))
        {
        return USBHST_INVALID_PARAMETER;
        }

    /* Obtain the pointer to the OHCI host controller information */

    pHCDData = &g_OHCDData[uHostControllerIndex];

    /* If the request is for the root hub, return success */

    if (uDeviceAddress == pHCDData->uRootHubAddress)
        {
        return USBHST_SUCCESS;
        }

    /* Obtain the pointer to the periodic list */
    /*
     * The following sentence want to get the address of
     * pHCDData->pHcca->uHccaInterruptTable
     * But in DIAB compiler, we will get warning as
     * "dangerous to take address of member of packed or swapped structure"
     * The "uHccaInterruptTable" must be the first of the structure
     * So we will get the HCCA address to instead.
     */

    pPeriodicList = (PUINT32)(pHCDData->pHcca);

    /* Initialize the available bandwidth information */

    for (uPollingInterval = 0;
         uPollingInterval < USB_OHCI_MAXIMUM_POLLING_INTERVAL;
         uPollingInterval++)
        {
        /*
         * Obtain the pointer to the head of the list of endpoint
         * descriptors queued for the current interval.
         */

        pTempEndpointDescriptor = (pUSB_OHCD_PIPE)
          usbOhciPhyToVirt(pHCDData,
                           USB_OHCD_SWAP_DATA(uHostControllerIndex,
                                              pPeriodicList[uPollingInterval]),
                                              USB_OHCI_MEM_MAP_ED);

        /* Initialize the available bandwidth */

        if (NULL == pTempEndpointDescriptor)
            {
            /* Complete bandwidth is available */

            uAvailableBandwidth[uPollingInterval] =
                pHCDData->uMaximumBandwidthAvailable;
            }
        else
            {
            /*
             * Some endpoints are queued for the current polling interval
             * and the bandwidth available is less than the complete
             * bandwidth.
             */

            uAvailableBandwidth[uPollingInterval] =
                pTempEndpointDescriptor->uBandwidthAvailable;
            }
        }

    /* Initialize the pointer to the general descriptor */

    pGeneralDescriptor = (PUSB_OHCI_GENERIC_DESCRIPTOR)pNewDescriptor;

    /*
     * Check whether the new descriptor corresponds to a configuration
     * descriptor or an interface descriptor.
     */

    if (USBHST_CONFIG_DESC == pGeneralDescriptor->bDescriptorType)
        {
        /* Release the bandwidth allocated for the current configuration */

        usbOhciReleaseBandwidthForCurrentConfiguration(
            uHostControllerIndex,
            uDeviceAddress,
            uDeviceSpeed,
            uAvailableBandwidth);

        /* Allocate bandwidth for the new configuration */

        nStatus =
            usbOhciAllocateBandwidthForNewConfiguration(
                uHostControllerIndex,
                uDeviceAddress,
                uDeviceSpeed,
                (PUSB_OHCI_CONFIGURATION_DESCRIPTOR)pGeneralDescriptor,
                uAvailableBandwidth);
        }
    else if (USBHST_INTERFACE_DESC == pGeneralDescriptor->bDescriptorType)
        {
        /* Release the bandwidth allocated for the current interface */

        usbOhciReleaseBandwidthForCurrentInterface(
            uHostControllerIndex,
            uDeviceAddress,
            uDeviceSpeed,
            (PUSB_OHCI_INTERFACE_DESCRIPTOR)pCurrentDescriptor,
            uAvailableBandwidth);

        /* Allocate bandwidth for the new interface */

        nStatus =
            usbOhciAllocateBandwidthForNewInterface(
                uHostControllerIndex,
                uDeviceAddress,
                uDeviceSpeed,
                (PUSB_OHCI_INTERFACE_DESCRIPTOR)pGeneralDescriptor,
                uAvailableBandwidth);
        }

    return USBHST_SUCCESS;
    } /* End of function usbOhciIsBandwidthAvailable () */

/***************************************************************************
*
* usbOhciModifyDefaultPipe - modifies the properties of default control pipe
*
* This function is used to modify the properties of the default control pipe,
* i.e. the pipe corresponding to Address 0, Endpoint 0.
*
* PARAMETERS: <uHostControllerIndex (IN)> - OHCI Host Controller index.
*
* <uDefaultPipeHandle (IN)> - Handle to the default control pipe, i.e. the pipe
* corresponding to Address 0, Endpoint 0.
*
* <uDeviceSpeed (IN)> - Specifies the device speed.
*
* <uMaximumPacketSize (IN)> - Specifies the maximum packet size for endpoint.
*
* <uHighSpeedHubInfo (IN)> - Specifies the nearest high speed hub and the port
* number information. This information will be used to handle a split transfer
* to the full/low speed device.
*
* The high byte will hold the high speed hub address. The low byte will hold the
* port number.
*
* Note: This parameter is not used in OHCI driver.
*
* RETURNS: USBHST_SUCCESS on success,
* USBHST_INVALID_PARAMETER if the parameters are not valid
*
* ERRNO:
*   None.
*/

USBHST_STATUS usbOhciModifyDefaultPipe
    (
    UINT8   uHostControllerIndex,
    ULONG   uDefaultPipeHandle,
    UINT8   uDeviceSpeed,
    UINT8   uMaximumPacketSize,
    UINT16  uHighSpeedHubInfo
    )
    {
    /* To hold the pointer to the endpoint descriptor */

    pUSB_OHCD_PIPE   pHCDPipe = NULL;

    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA   pHCDData = NULL;

#ifdef DEBUG
    /* To hold the status of the operation */

    USBHST_STATUS nStatus = USBHST_SUCCESS;
#endif

    /* WindView Instrumentation */
    USB_HCD_LOG_EVENT(
        USB_OHCI_WV_TRANSFER,
        "usbOhciModifyDefaultPipe() starts",
        USB_OHCD_WV_FILTER);

    /*
     * NOTE: The default pipe is used only by the USB Driver. Further,
     *       the USB Driver will not try to access the default pipe
     *       simultaneously for two requests.
     */

    /* Check whether the OHCI host controller index is valid */
    /* Check whether the pipe parameter is valid */
    /* Check whether the device speed is valid */

    if ((uHostControllerIndex >= USB_MAX_OHCI_COUNT) ||
        (g_OHCDData[uHostControllerIndex].pDev == NULL) ||
        (uDefaultPipeHandle == USB_OHCI_INVALID_PIPE_HANDLE) ||
        ((uDeviceSpeed != USBHST_FULL_SPEED) &&
         (uDeviceSpeed != USBHST_LOW_SPEED)))
        {
        /* Debug print */
        USB_OHCD_ERR("Invalid parameter: uHostControllerIndex = %d, "
                     "uDefaultPipeHandle = %p,uDeviceSpeed= %d \n",
                     uHostControllerIndex, uDefaultPipeHandle, uDeviceSpeed,
                     4, 5, 6);
        return USBHST_INVALID_PARAMETER;
        }

    /* Check whether the maximum packet size is valid */

    if (uDeviceSpeed == USBHST_FULL_SPEED)
        {
        /*
         * For full speed, only 8, 16, 32 and 64 bytes of maximum packet size
         * are supported.
         */
        if ((uMaximumPacketSize != 8) && (uMaximumPacketSize != 16) &&
            (uMaximumPacketSize != 32) && (uMaximumPacketSize != 64))
            {
            /* Debug print */
            USB_OHCD_ERR("invalid maximum packet size %d for " \
                         "full speed\n",
                         uMaximumPacketSize, 2, 3, 4, 5, 6);
            return USBHST_INVALID_PARAMETER;
            }
        }
    else /* Low speed device */
        {
        /* For low speed, only 8 bytes of maximum packet size is supported */
        if (uMaximumPacketSize != 8)
            {
            /* Debug print */
            USB_OHCD_ERR("invalid maximum packet size %d for " \
                         "low speed\n",
                         uMaximumPacketSize, 2, 3, 4, 5, 6);
            return USBHST_INVALID_PARAMETER;
            }
        }

#ifdef DEBUG
    /* Check whether the pipe is valid */
    nStatus = usbOhciIsValidPipe (uHostControllerIndex, uDefaultPipeHandle);

    /* If the pipe is invalid, return */
    if (nStatus != USBHST_SUCCESS)
        {
        /* Debug print */
        USB_OHCD_ERR("check pipe handle is invalid \n",
                      1, 2, 3, 4, 5, 6);
        return USBHST_INVALID_PARAMETER;
        }
#else
    /*
     * NOTE: It is assumed that the USBD will provide a valid pipe handle.
     *       If this handle is invalid, the HCD driver might crash.
     *
     *       Since the pipe handle is visible only to HCD and USBD, the
     *       check has been intensionaly removed to reduce the code size.
     *
     *       However, if needed, the pipe handle can be compared against
     *       the list of endpoint descriptors for every transfer.
     */
#endif /* End of #ifdef DEBUG */

    pHCDData = &g_OHCDData[uHostControllerIndex];

    /* Initialize the endpoint descriptor */

    pHCDPipe = (pUSB_OHCD_PIPE) uDefaultPipeHandle;

    /* Set endpoint type */

    pHCDPipe->uEndpointType = USBHST_CONTROL_TRANSFER;

    /* Update the endpoint descriptor for the endpoint */

    OHCI_MODIFY_ED_SPEED_AND_MPS(uHostControllerIndex,
                                 pHCDPipe->uControlInformation,
                                 uDeviceSpeed,
                                 uMaximumPacketSize);

    return USBHST_SUCCESS;
    } /* End of function usbOhciModifyDefaultPipe () */

/***************************************************************************
*
* usbOhciIsRequestPending - checks pending requests
*
* This function is used to check whether any request is pending on a pipe.
*
* PARAMETERS: <uHostControllerIndex (IN)> - OHCI Host Controller index.
*
* <uPipeHandle (IN)> - Handle to the pipe.
*
* RETURNS: USBHST_SUCCESS, if the URB is pending
* USBHST_FAILURE, if the URB is not pending
* USBHST_INVALID_PARAMETERS, if the parameters are invalid
*
* ERRNO:
*   None.
*/

USBHST_STATUS usbOhciIsRequestPending
    (
    UINT8    uHostControllerIndex,
    ULONG    uPipeHandle
    )
    {
    /* To hold the pointer to the endpoint descriptor */

    pUSB_OHCD_PIPE   pHCDPipe = NULL;

#ifdef DEBUG
    /* To hold the status of the operation */

    USBHST_STATUS nStatus = USBHST_SUCCESS;
#endif

    /* WindView Instrumentation */
    USB_HCD_LOG_EVENT(
        USB_OHCI_WV_TRANSFER,
        "usbOhciIsRequestPending() starts",
        USB_OHCD_WV_FILTER);


    /* Check whether the OHCI host controller index is valid */
    /* Check whether the pipe parameter is valid */

    if ((uHostControllerIndex >= USB_MAX_OHCI_COUNT) ||
        (g_OHCDData[uHostControllerIndex].pDev == NULL)||
        (uPipeHandle == USB_OHCI_INVALID_PIPE_HANDLE))
        {
        USB_OHCD_ERR("Invalid parameter: uHostControllerIndex = %d,"
                     "uPipeHandle = %p\n",
                     uHostControllerIndex, uPipeHandle,
                     3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }

#ifdef DEBUG
    /* Check whether the pipe is valid */

    nStatus = usbOhciIsValidPipe (uHostControllerIndex, uPipeHandle);

    /* If the pipe is invalid, return */

    if (nStatus != USBHST_SUCCESS)
        {
        USB_OHCD_ERR("check pipe handle is invalid \n",
                      1, 2, 3, 4, 5, 6);
        return USBHST_INVALID_PARAMETER;
        }
#else
    /*
     * NOTE: It is assumed that the USBD will provide a valid pipe handle.
     *       If this handle is invalid, the HCD driver might crash.
     *
     *       Since the pipe handle is visible only to HCD and USBD, the
     *       check has been intensionaly removed to reduce the code size.
     *
     *       However, if needed, the pipe handle can be compared against
     *       the list of endpoint descriptors for every transfer.
     */
#endif /* End of #ifdef DEBUG */

    /* Initialize the endpoint descriptor */

    pHCDPipe = (pUSB_OHCD_PIPE) uPipeHandle;

    /* Check whether a request is pending for the endpoint */

    if (lstCount(&(pHCDPipe->requestList)) != 0)
        {
        /* Requests are pending for the endpoint */

        USB_OHCD_VDBG("requests are pending for the endpoint \n",
                      1, 2, 3, 4, 5, 6);

        return USBHST_SUCCESS;
        }

    /* Requests are not pending for the endpoint */

    USB_OHCD_VDBG("requests are not pending for the endpoint \n",
                  1, 2, 3, 4, 5, 6);

    return USBHST_FAILURE;
    } /* End of function usbOhciIsRequestPending () */

/***************************************************************************
*
* usbOhciGetFrameNumber - obtains the current frame number
*
* This function is used to obtain the current frame number.
*
* PARAMETERS: <uHostControllerIndex (IN)> - OHCI Host Controller index.
*
* <puCurrentFrameNumber (OUT)> - Pointer to the variable to hold the current
* frame number.
*
* RETURNS: USBHST_SUCCESS, if the URB is pending
* USBHST_INVALID_PARAMETERS, if the parameters are invalid
*
* ERRNO:
*   None.
*/

USBHST_STATUS usbOhciGetFrameNumber
    (
    UINT8      uHostControllerIndex,
    PUINT16    puCurrentFrameNumber
    )
    {

    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA          pHCDData = NULL;

    /* To hold the value read from the HcFmNumber register */

    UINT32                  uRegisterValue = 0;


    /* WindView Instrumentation */
    USB_HCD_LOG_EVENT(
        USB_OHCI_WV_TRANSFER,
        "usbOhciGetFrameNumber() starts",
        USB_OHCD_WV_FILTER);


    /* Check whether the OHCI host controller index is valid */
    /* Check whether the current frame number parameter is valid */

    if ((uHostControllerIndex >= USB_MAX_OHCI_COUNT) ||
        (g_OHCDData[uHostControllerIndex].pDev == NULL)||
        (puCurrentFrameNumber == NULL))
        {
        /* Debug print */

        USB_OHCD_ERR("invalid parameter: "
                     "uHostControllerIndex = %d, puCurrentFrameNumber = %p\n",
                     uHostControllerIndex, puCurrentFrameNumber,
                     3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }
    /* Obtain the pointer to the host controller information */

    pHCDData = &g_OHCDData[uHostControllerIndex];


    /* Read the current frame number from the HcFmNumber register */

    uRegisterValue =
        USB_OHCI_REG_READ(pHCDData->pDev,
                          USB_OHCI_FM_NUMBER_REGISTER_OFFSET);

    /* Update the current frame number parameter */

    *puCurrentFrameNumber = (UINT16) (uRegisterValue & 0xFFFF);

    return USBHST_SUCCESS;
    } /* End of function usbOhciGetFrameNumber () */

/***************************************************************************
*
* usbOhciSetBitRate - modify the frame width
*
* This function is used to modify the frame width.
*
* PARAMETERS: <uHostControllerIndex (IN)> - OHCI Host Controller index.
*
* <bIncrement (IN)> - Flag to specify whether the frame number should be
* incremented or decremented. If TRUE, the frame number will be incremented.
* Else decremented.
*
* <puCurrentFrameWidth (OUT)> - Pointer to hold the current frame width (after
* modification).
*
* RETURNS: USBHST_SUCCESS, if the URB is pending
* USBHST_INVALID_PARAMETERS, if the parameters are invalid
*
* ERRNO:
*   None.
*/

USBHST_STATUS usbOhciSetBitRate
    (
    UINT8      uHostControllerIndex,
    BOOL       bIncrement,
    PUINT32    puCurrentFrameWidth
    )
    {
    /* WindView Instrumentation */
    USB_HCD_LOG_EVENT(
        USB_OHCI_WV_TRANSFER,
        "usbOhciSetBitRate() starts",
        USB_OHCD_WV_FILTER);

    return USBHST_SUCCESS;

    } /* End of function usbOhciSetBitRate () */


/******************* MODULE SPECIFIC FUNCTIONS DEFINITION *********************/
/***************** THESE FUNCTIONS WILL BE USED INTERNALLY ********************/

#ifdef DEBUG

/***************************************************************************
*
* usbOhciIsValidPipe - checks for validity of a pipe
*
* This function is used to check whether the pipe corresponding to an endpoint
* is valid.
*
* PARAMETERS: <uHostControllerIndex (IN)> - Host controller index corresponding
* to the OHCI controller.
*
* <uPipeHandle (IN)> - Handle to the pipe to be validated.
*
* RETURNS: USBHST_SUCCESS, if the URB is pending
* USBHST_INVALID_PARAMETERS, if the parameters are invalid
*
* ERRNO:
*   None.
*/

LOCAL USBHST_STATUS usbOhciIsValidPipe
    (
    UINT8  uHostControllerIndex,
    ULONG  uPipeHandle
    )
    {
    /* To hold the pointer to the endpoint descriptor */

    pUSB_OHCD_PIPE pHCDPipe = NULL;

    /* To hold the pointer to the temporary endpoint descriptor */

    pUSB_OHCD_PIPE pTempEndpointDescriptor = NULL;

    /* To hold the pointer to the head of the periodic list */

    PUINT32  pInterruptTable = NULL;

    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA   pHCDData = NULL;

    /* To hold the polling interval for the periodic endpoints */

    UINT8   uPollingInterval = 0;

    /* To hold the loop index */

    UINT32  uIndex = 0;

    UINT32  uBufferAddr = 0x0;

    /* Check whether the OHCI host controller index is valid */
    /* Check whether the pipe parameter is valid */

    if ((uHostControllerIndex >= USB_MAX_OHCI_COUNT)||
         (g_OHCDData[uHostControllerIndex].pDev == NULL)||
        (uPipeHandle == USB_OHCI_INVALID_PIPE_HANDLE))
        {
        /* Debug print */
        USB_OHCD_ERR("invalid parameter: "
                     "uHostControllerIndex = %d, uPipeHandle = %p\n",
                     uHostControllerIndex, uPipeHandle,
                     3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }

    /*
     * Check whether the pipe handle to corresponds to the pipes on
     * the root hub.
     *
     * NOTE: A dummy pipe handle created for the endpoints on the
     *       root hub.
     */

    if (uPipeHandle == USB_OHCI_ROOT_HUB_PIPE_HANDLE)
        {
        /* Debug print */
        USB_OHCD_VDBG("usbOhciIsValidPipe - dummy pipe handle for RH \n",
                      1, 2, 3, 4, 5, 6);

        return USBHST_SUCCESS;
        }

    /* Obtain the pointer to the host controller information */

    pHCDData = &g_OHCDData[uHostControllerIndex];

    /* Initialize the endpoint descriptor */

    pHCDPipe = (pUSB_OHCD_PIPE) uPipeHandle;

    /* Check control list */

    if (pHCDPipe->uEndpointType == USBHST_CONTROL_TRANSFER)
        {
        /* Read the contents of the HcControlHeadED */

        uBufferAddr = USB_OHCI_REG_READ (pHCDData->pDev,
                       USB_OHCI_CONTROL_HEAD_ED_REGISTER_OFFSET);

        pTempEndpointDescriptor = (pUSB_OHCD_PIPE)
            usbOhciPhyToVirt(pHCDData, uBufferAddr, USB_OHCI_MEM_MAP_ED);

        /*
         * Traverse the control list to check whether pipe is present in
         * the list of control pipes
         */
        while ((pTempEndpointDescriptor != NULL) &&
               (pTempEndpointDescriptor != pHCDPipe))
            {
            pTempEndpointDescriptor =
                pTempEndpointDescriptor->pHCDNextPipe;
            }

        /* Check whether the pipe is present in the control list */

        if (pTempEndpointDescriptor == pHCDPipe)
            {
            /* Return from the function */
            /* Debug print */
            USB_OHCD_VDBG("usbOhciIsValidPipe - valid control pipe handle \n",
                          1, 2, 3, 4, 5, 6);
            return USBHST_SUCCESS;
            }
        }
    /* Check if the pipe in the bulk list */

    else if (pHCDPipe->uEndpointType == USBHST_BULK_TRANSFER)
        {
        /* Read the contents of the HcBulkHeadED */

        uBufferAddr = USB_OHCI_REG_READ (pHCDData->pDev,
                       USB_OHCI_BULK_HEAD_ED_REGISTER_OFFSET);

        pTempEndpointDescriptor = (pUSB_OHCD_PIPE)
          usbOhciPhyToVirt(pHCDData, uBufferAddr, USB_OHCI_MEM_MAP_ED);

        /*
         * Traverse the bulk list to check whether pipe is present in
         * the list of bulk pipes
         */

        while ((pTempEndpointDescriptor != NULL) &&
               (pTempEndpointDescriptor != pHCDPipe))
            {
            /* Traverse to the next element in the list */

            pTempEndpointDescriptor =
                            pTempEndpointDescriptor->pHCDNextPipe;
            }

        /* Check whether the pipe is present in the bulk list */

        if (pTempEndpointDescriptor == pHCDPipe)
            {
            /* Return from the function */
            /* Debug print */
            USB_OHCD_VDBG("usbOhciIsValidPipe - valid bulk pipe handle \n",
                          1, 2, 3, 4, 5, 6);

            return USBHST_SUCCESS;
            }
        }
    /* Check if the pipe in the periodic list */

    else
        {
        /* Obtain the polling interval from the endpoint descriptor */

        uPollingInterval = pHCDPipe->uPollingInterval;

        /*
         * The following sentence want to get the address of
         * pHCDData->pHcca->uHccaInterruptTable
         * But in DIAB compiler, we will get warning as
         * "dangerous to take address of member of packed or swapped structure"
         * The "uHccaInterruptTable" must be the first of the structure
         * So we will get the HCCA address to instead.
         */

        pInterruptTable = (PUINT32)(pHCDData->pHcca);

        /*
         * Search for the endpoint descriptor in the list of periodic pipes
         * for the specified polling interval
         */

        for (uIndex = 0; uIndex < uPollingInterval; uIndex++)
            {
            /*
             * Obtain the pointer to the head of the periodic list for
             * the specified polling interval.
             */

            uBufferAddr = pInterruptTable[uIndex];

            /* Get the bus address in CPU endian */

            uBufferAddr = USB_OHCD_SWAP_DATA(uHostControllerIndex, uBufferAddr);

            pTempEndpointDescriptor = (pUSB_OHCD_PIPE)
                usbOhciPhyToVirt(pHCDData,
                                 uBufferAddr,
                                 USB_OHCI_MEM_MAP_ED);

            /* Check for the pipe to be validated */

            while ((pTempEndpointDescriptor != NULL) &&
                   (pTempEndpointDescriptor != pHCDPipe))
                {
                /* Traverse to the next element in the periodic list */

                pTempEndpointDescriptor =
                    pTempEndpointDescriptor->pHCDNextPipe;
                }

            /* Check whether the pipe is found in the periodic list */

            if (pTempEndpointDescriptor == pHCDPipe)
                {
                /* Debug print */

                USB_OHCD_VDBG("usbOhciIsValidPipe - valid periodic pipe handle \n",
                              1, 2, 3, 4, 5, 6);

                return USBHST_SUCCESS;
                }
            }
        }

    USB_OHCD_ERR("invalid pipe handle \n",1, 2, 3, 4, 5, 6);

    return USBHST_FAILURE;
    } /* End of function usbOhciIsValidPipe () */
#endif

/*******************************************************************************
*
* usbOhciSubmitControlUrb - populate control trandsfer descriptor
*
* This routine populates the transfer descriptor for control transfer
*
* PARAMETERS: <uHostControllerIndex (IN)> - OHCI Host Controller index.
*
* <pHCDPipe (IN)> - Pointer to the endpoint descriptor.
*
* <pURB (IN)> - Pointer to the URB to be submitted.
*
* RETURNS: USBHST_SUCCESS on success,
* USBHST_INVALID_PARAMETER, if the parameters are not valid
*
* ERRNO: N/A
*/

USBHST_STATUS usbOhciSubmitControlUrb
    (
    pUSB_OHCD_DATA                  pHCDData,
    pUSB_OHCD_PIPE                  pHCDPipe,
    pUSBHST_URB                     pUrb,
    pUSB_OHCD_REQUEST_INFO          pRequest
    )
    {
    /* To hold the pointer of the setup TD */

    PUSB_OHCI_TD pSetupTD = NULL;

    /* To hold the pointer of the Status TD */

    PUSB_OHCI_TD pStatusTD = NULL;

    /* Pointer of the head of DATA TD */

    PUSB_OHCI_TD pDataTDHead = NULL;

    /* Pointer of the tail of DATA TD */

    PUSB_OHCI_TD pDataTDTail = NULL;

    /* Current TD for loop */

    PUSB_OHCI_TD pCurTD = NULL;

    /* Pointer to the Dummy TD */

    PUSB_OHCI_TD pDummyTD = NULL;

    /* To hold the pointer to the SETUP packet */

    pUSBHST_SETUP_PACKET  pSetupPacket = NULL;

    /* To hold the buffer address  */

    UINT32  uBufferAddr = 0;

    /* To hold the remaining transfer size */

    UINT32  uRemainingTransferSize = 0;

    /* To hold the number of TD in the data transfer stage */

    UINT32  uNumOfDataTD = 0;

    /* To hold the loop index */

    UINT32  uIndex = 0;

    /* To hold status */

    STATUS   vxbStatus;

    /* To hold the transfer size */

    UINT32   uSize = 0x0;

    /* To hold boolean status */

    BOOLEAN  bStatus = FALSE;

    /*
     * FIXME: remove some useless judgement
     * as the callling routine already check it!
     */

    /* Check whether the pointer to the SETUP packet buffer is valid */

    if (pUrb->pTransferSpecificData == NULL)
        {
        /* Debug print */

        USB_OHCD_ERR("usbOhciSubmitControlUrb - "
            "pUrb->pTransferSpecificData is null \n", 1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Obtain the pointer to the SETUP packet */

    pSetupPacket = (pUSBHST_SETUP_PACKET) pUrb->pTransferSpecificData;

    pSetupTD = usbOhciGetFreeTD(pHCDData, pHCDPipe);

    if (NULL == pSetupTD)
        {
        USB_OHCD_ERR("usbOhciSubmitControlUrb - get setup TD failed \n",
                1, 2, 3, 4, 5, 6);

        return USBHST_INSUFFICIENT_MEMORY;
        }

    pSetupTD->pRequestInfo = pRequest;

    pStatusTD = usbOhciGetFreeTD(pHCDData, pHCDPipe);

    if (NULL == pStatusTD)
        {
        USB_OHCD_ERR("usbOhciSubmitControlUrb - get status TD failed \n",
            1, 2, 3, 4, 5, 6);

        usbOhciAddToFreeTDList(pHCDData,
                               pHCDPipe,
                               pSetupTD);

        return USBHST_INSUFFICIENT_MEMORY;
        }

    pStatusTD->pRequestInfo = pRequest;

    vxbStatus = vxbDmaBufMapLoad (pHCDData->pDev,
                                  pHCDPipe->ctrlSetupDmaTagId,
                                  pRequest->ctrlSetupDmaMapId,
                                  pUrb->pTransferSpecificData,
                                  sizeof(USBHST_SETUP_PACKET),
                                  VXB_DMABUF_MAPLOAD_CACHEALIGNCHECK);

    if (vxbStatus != OK)
        {
        USB_OHCD_ERR("usbOhciSubmitControlUrb - "
                     "load ctrlSetupDmaMapId fail\n",
                     1, 2, 3, 4, 5, 6);

        usbOhciAddToFreeTDList(pHCDData,
                               pHCDPipe,
                               pSetupTD);

        usbOhciAddToFreeTDList(pHCDData,
                               pHCDPipe,
                               pStatusTD);

        return USBHST_FAILURE;
        }

    vxbStatus = vxbDmaBufSync(pHCDData->pDev,
                              pHCDPipe->ctrlSetupDmaTagId,
                              pRequest->ctrlSetupDmaMapId,
                              _VXB_DMABUFSYNC_DMA_PREWRITE);

    if (vxbStatus != OK)
        {
        USB_OHCD_ERR("usbOhciSubmitControlUrb - "
                     "sync ctrlSetupDmaMapId fail\n",
                     1, 2, 3, 4, 5, 6);

        vxbDmaBufMapUnload (pHCDPipe->ctrlSetupDmaTagId,
                            pRequest->ctrlSetupDmaMapId);

        usbOhciAddToFreeTDList(pHCDData,
                               pHCDPipe,
                               pSetupTD);

        usbOhciAddToFreeTDList(pHCDData,
                               pHCDPipe,
                               pStatusTD);
        return USBHST_FAILURE;
        }

    /*
     * Check whether the URB transfer buffer is valid. If the URB transfer
     * buffer is NULL, this is a no-data control transfer.
     */

    if (pUrb->pTransferBuffer != NULL)
        {
        vxbStatus = vxbDmaBufMapLoad (pHCDData->pDev,
                                      pHCDPipe->usrBuffDmaTagId,
                                      pRequest->usrDataDmaMapId,
                                      pUrb->pTransferBuffer,
                                      pUrb->uTransferLength,
                                      VXB_DMABUF_MAPLOAD_CACHEALIGNCHECK);

        if (vxbStatus != OK)
            {
            USB_OHCD_ERR("usbOhciSubmitControlUrb - "
                         "load usrDataDmaMapId fail\n",
                         1, 2, 3, 4, 5, 6);

            vxbDmaBufMapUnload (pHCDPipe->ctrlSetupDmaTagId,
                                pRequest->ctrlSetupDmaMapId);

            usbOhciAddToFreeTDList(pHCDData,
                                   pHCDPipe,
                                   pSetupTD);

            usbOhciAddToFreeTDList(pHCDData,
                                   pHCDPipe,
                                   pStatusTD);
            return USBHST_FAILURE;
            }

        /* Perform vxbDmaBufSync operation according to Data direction */

        if (USB_GET_CONTROL_TRANSFER_DIRECTION(pSetupPacket->bmRequestType) ==
            USB_CONTROL_IN_TRANSFER)
            {
            /* DMA cache invalidate before data receive */

            pRequest->uDataDir = USB_OHCD_DIR_IN;

            vxbStatus = vxbDmaBufSync(pHCDData->pDev,
                                      pHCDPipe->usrBuffDmaTagId,
                                      pRequest->usrDataDmaMapId,
                                      _VXB_DMABUFSYNC_DMA_PREREAD);
            }
        else
            {
            /* <copy to bounce buffer and> DMA cache flush */

            pRequest->uDataDir = USB_OHCD_DIR_OUT;

            vxbStatus = vxbDmaBufSync(pHCDData->pDev,
                                      pHCDPipe->usrBuffDmaTagId,
                                      pRequest->usrDataDmaMapId,
                                      _VXB_DMABUFSYNC_DMA_PREWRITE);
            }

        if (vxbStatus != OK)
            {
            USB_OHCD_ERR("usbOhciSubmitControlUrb - "
                         "vxbDmaBufSync usrDataDmaMapId fail \n",
                          1, 2, 3, 4, 5, 6);

            vxbDmaBufMapUnload (pHCDPipe->ctrlSetupDmaTagId,
                                pRequest->ctrlSetupDmaMapId);

            vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
                                pRequest->usrDataDmaMapId);

            usbOhciAddToFreeTDList(pHCDData,
                                   pHCDPipe,
                                   pSetupTD);

            usbOhciAddToFreeTDList(pHCDData,
                                   pHCDPipe,
                                   pStatusTD);
            return USBHST_FAILURE;
            }

        /* Compute the number of transfer descriptors to be allocated */

        uNumOfDataTD = pUrb->uTransferLength / USB_OHCI_TD_MAXIMUM_TRANSFER_LENGTH;

        /* Check for remaining transfer length */

        if ((pUrb->uTransferLength % USB_OHCI_TD_MAXIMUM_TRANSFER_LENGTH) != 0)
            {
            /* Increment the number of transfer descriptors to be allocated */

            uNumOfDataTD++;
            }

        /* Create general TD list for data transfer */

        bStatus = usbOhciCreateTDList(pHCDData,
                                      pHCDPipe,
                                      pRequest,
                                      &pDataTDHead,
                                      &pDataTDTail,
                                      uNumOfDataTD
                                      );

        if (TRUE != bStatus)
            {
            USB_OHCD_ERR("usbOhciSubmitControlUrb - "
                         "usbOhciCreateTDList fail \n",
                         1, 2, 3, 4, 5, 6);

            vxbDmaBufMapUnload (pHCDPipe->ctrlSetupDmaTagId,
                                pRequest->ctrlSetupDmaMapId);

            vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
                                pRequest->usrDataDmaMapId);

            usbOhciAddToFreeTDList(pHCDData,
                                   pHCDPipe,
                                   pSetupTD);

            usbOhciAddToFreeTDList(pHCDData,
                                   pHCDPipe,
                                   pStatusTD);

            return USBHST_INSUFFICIENT_MEMORY;
            }

        /* Update the request List pointer */

        pSetupTD->pHCDNextTransferDescriptor =  pDataTDHead;

        pDataTDTail->pHCDNextTransferDescriptor = pStatusTD;
        }
    else
        {
        pSetupTD->pHCDNextTransferDescriptor = pStatusTD;
        }

    /* Update the request's head & tail TD pointer */

    pRequest->pTDListHead= pSetupTD;

    pRequest->pTDListTail = pStatusTD;

    /*
     * A dummy TD always at the tail of the TD list.
     * Add new TD List after the dummy TD.
     */

    pDummyTD = pHCDPipe->pHCDTDTail;

    pDummyTD->pRequestInfo = pRequest;

    pDummyTD->uNextTDPointer =
        USB_OHCD_SWAP_DATA(pHCDData->uBusIndex,
                           USB_OHCI_DESC_LO32(pRequest->pTDListHead));

    pDummyTD->pHCDNextTransferDescriptor = pRequest->pTDListHead;

    /* Let the old dummy TD as the head of the TD list */

    pRequest->pTDListHead = pDummyTD;

    /* Let the last TD as new dummy TD */

    pDummyTD = pRequest->pTDListTail;

    pDummyTD->pRequestInfo = NULL;

    /*
     * NOTE: There is always an empty transfer descriptor at the end of
     *       the transfer descritpor list for the endpoint.
     *
     *       In order to append the transfer, the following algorithm is
     *       followed:
     *
     *       a) Create the transfer list for the transfer
     *       b) Append the transfer list to the transfer list for the
     *          endpoint.
     *       c) Populate the transfer information. The transfer information
     *          should be populated from the previous last node of the
     *          transfer list for the endpoint.
     */

    /* Fill setup TD */

    /* Reget the setup TD from the TD list. As the dummy TD is added. */

    pSetupTD = pRequest->pTDListHead;

    USB_OHCI_SET_TD_DATA_TOGGLE(pHCDData->uBusIndex,
                                pSetupTD->uControlInformation,
                                USB_OHCI_TD_DATA_TOGGLE0);
    uBufferAddr =
        USB_OHCI_BUS_ADDR_LO32(pRequest->ctrlSetupDmaMapId->fragList[0].frag);

    /* Fill the Setup TD */

    usbOhciFillTD(pHCDData->uBusIndex,
                  pSetupTD,
                  uBufferAddr,
                  USB_SETUP_PACKET_LENGTH);
    /*
     * When finish the TD, we will calculate the actual length
     * if (uStartAddressPointer != 0), the setupTD donn't need to
     * calculate the length, so reset the uStartAddressPointer to "0".
     */

    pSetupTD->uStartAddressPointer = 0x0;

    /* Get next TD to be filled */

    pCurTD = pSetupTD->pHCDNextTransferDescriptor;

    /* Fill Data TD */

    if (pUrb->pTransferBuffer != NULL)
        {

        uRemainingTransferSize = pUrb->uTransferLength;

        uBufferAddr =
            USB_OHCI_BUS_ADDR_LO32(pRequest->usrDataDmaMapId->fragList[0].frag);

        for (uIndex = 0; uIndex < uNumOfDataTD; uIndex++)
            {
            /* Check whether a short transfer is acceptable */

            if ((pUrb->uTransferFlags & USBHST_SHORT_TRANSFER_OK) ==
                            USBHST_SHORT_TRANSFER_OK)
                {
                /* Set the 'BufferRounding' bit in the transfer descriptor */

                USB_OHCI_SET_TD_BUFFER_ROUND(pHCDData->uBusIndex,
                                             pCurTD->uControlInformation);
                }

            /*
             * Set the PID for the transfer descriptor based on the direction
             * of the data stage for the control transfer.
             */

            if (USB_GET_CONTROL_TRANSFER_DIRECTION(pSetupPacket->bmRequestType) ==
                            USB_CONTROL_IN_TRANSFER)
                {
                /* Set the PID for IN transfer */

                USB_OHCI_SET_TD_PID_IN(pHCDData->uBusIndex, pCurTD->uControlInformation);
                }
            else
                {
                /* Set the PID for OUT transfer */

                USB_OHCI_SET_TD_PID_OUT(pHCDData->uBusIndex, pCurTD->uControlInformation);
                }

            /*
             * Set the data toggle for the first packet in the DATA stage.
             * DATA stage should should always have a data toggle of 1.
             *
             * For other data packets, the data toggle will be obtained from
             * the ED::ToggleCarry field.
             */

            if (uIndex == 0)
                {
                USB_OHCI_SET_TD_DATA_TOGGLE(pHCDData->uBusIndex,
                                            pCurTD->uControlInformation,
                                            USB_OHCI_TD_DATA_TOGGLE1);
                }

            uSize = (uRemainingTransferSize >
                     USB_OHCI_TD_MAXIMUM_TRANSFER_LENGTH) ?
                     USB_OHCI_TD_MAXIMUM_TRANSFER_LENGTH  :
                     uRemainingTransferSize;

            /* Fill the TD */

            usbOhciFillTD(pHCDData->uBusIndex,
                          pCurTD,
                          uBufferAddr,
                          uSize);

            uRemainingTransferSize -= uSize;

            uBufferAddr += uSize;

            pCurTD = pCurTD->pHCDNextTransferDescriptor;
            }
        }

    /* Fill Status TD */

    /* Reget the statusTD to be filled */

    pStatusTD = pCurTD;

    uBufferAddr =
        USB_OHCI_BUS_ADDR_LO32(pRequest->ctrlSetupDmaMapId->fragList[0].frag);

    if (USB_GET_CONTROL_TRANSFER_DIRECTION(pSetupPacket->bmRequestType) ==
                USB_CONTROL_IN_TRANSFER)
        {
        /* Set the PID for OUT transfer */

        USB_OHCI_SET_TD_PID_OUT(pHCDData->uBusIndex,
            pStatusTD->uControlInformation);
        }
    else
        {
        /* Set the PID for IN transfer */

        USB_OHCI_SET_TD_PID_IN(pHCDData->uBusIndex,
            pStatusTD->uControlInformation);
        }

    USB_OHCI_SET_TD_DATA_TOGGLE(pHCDData->uBusIndex,
                                pStatusTD->uControlInformation,
                                USB_OHCI_TD_DATA_TOGGLE1);

    usbOhciFillTD(pHCDData->uBusIndex,
                  pStatusTD,
                  0,
                  0);

    /*
     * As the dummy TD is add into the list,
     * we should update the taie pointer of the TD
     */

    pRequest->pTDListTail = pStatusTD;

    /*
     * Update the HCD maintained tail pointer
     * The last TD is the new dummy TD.
     */

    pHCDPipe->pHCDTDTail = pDummyTD;

    /*
     * Initialize the urb length to 0. This will be updated in the
     * the isr after completion of the URB
     */

    pUrb->uTransferLength = 0;

    /* Update the ED's tail TD pointer */

    uBufferAddr =  USB_OHCI_DESC_LO32(pDummyTD);

    pHCDPipe->uTDQueueTailPointer =
        USB_OHCD_SWAP_DATA(pHCDData->uBusIndex, uBufferAddr);
    /* Clear the skip bit in the endpoint descriptor */

    OHCI_CLEAR_ED_SKIP_BIT(pHCDData->uBusIndex,
                           pHCDPipe->uControlInformation);

    /*
     * Set CLF filed in the HcCommandStatus register
     * This bit is used to indicate whether there are any TDs on the
     * Control list. It is set by HCD whenever it adds a TD to an ED in
     * the Control list.
     */

    USB_OHCI_SET_CLF(pHCDData);

    return USBHST_SUCCESS;
    } /* End of function usbOhciSubmitControlUrb () */

/***************************************************************************
*
* usbOhciSubmitBulkOrInterruptUrb - populates bulk/int tr desc.
*
* This function populates transfer descriptor for bulk or interrupt transfers
*
* PARAMETERS: <uHostControllerIndex (IN)> - OHCI Host Controller index.
*
* <pHCDPipe (IN)> - Pointer to the endpoint descriptor.
*
* <pURB (IN)> - Pointer to the URB to be submitted.
*
* RETURNS: N/A
*
* ERRNO:
*   None.
*/

USBHST_STATUS usbOhciSubmitBulkOrInterruptUrb
    (
    pUSB_OHCD_DATA                  pHCDData,
    pUSB_OHCD_PIPE                  pHCDPipe,
    pUSBHST_URB                     pUrb,
    pUSB_OHCD_REQUEST_INFO          pRequest
    )
    {
    /* To hold the buffer address */

    UINT32  uBufferAddr = 0;

    /* To hold the remaining transfer size */

    UINT32  uRemainingTransferSize = 0;

    /* To hole the pointer of current TD */

    PUSB_OHCI_TD   pCurTD = NULL;

    /* To hold the pointer of dummy TD */

    PUSB_OHCI_TD   pDummyTD = NULL;

    /* To hold of status */

    STATUS vxbStatus;

    /* To hold boolean status */

    BOOLEAN bStatus = FALSE;

    /* To hold transfer size */

    UINT32 uSize = 0x0;

    /* To hold the number of TD */

    UINT32 uNumOfDataTD  = 0x0;

    UINT32 uIndex = 0x0;

    /* Load the DMA MAP */

    vxbStatus = vxbDmaBufMapLoad (pHCDData->pDev,
                                  pHCDPipe->usrBuffDmaTagId,
                                  pRequest->usrDataDmaMapId,
                                  pUrb->pTransferBuffer,
                                  pUrb->uTransferLength,
                                  VXB_DMABUF_MAPLOAD_CACHEALIGNCHECK);

    if (vxbStatus != OK)
        {
        USB_OHCD_ERR("usbOhciSubmitBulkOrInterruptUrb - "
                     "load usrDataDmaMapId fail\n",
                      1, 2, 3, 4, 5, 6);

        return USBHST_FAILURE;
        }

    /* Perform vxbDmaBufSync operation according to Data direction */

    if (OHCI_IN_ENDPOINT == OHCI_GET_ENDPOINT_DIRECTION_FROM_ED(pHCDData->uBusIndex,
                     pHCDPipe->uControlInformation))
        {
        pRequest->uDataDir = USB_OHCD_DIR_IN;
        vxbStatus = vxbDmaBufSync(pHCDData->pDev,
                                  pHCDPipe->usrBuffDmaTagId,
                                  pRequest->usrDataDmaMapId,
                                  _VXB_DMABUFSYNC_DMA_PREREAD);

        }
    else
        {
        pRequest->uDataDir = USB_OHCD_DIR_OUT;
        vxbStatus = vxbDmaBufSync(pHCDData->pDev,
                                  pHCDPipe->usrBuffDmaTagId,
                                  pRequest->usrDataDmaMapId,
                                  _VXB_DMABUFSYNC_DMA_PREWRITE);

        }

    if (vxbStatus != OK)
        {
        USB_OHCD_ERR("usbOhciSubmitBulkOrInterruptUrb - "
                     "vxbDmaBufSync usrDataDmaMapId fail \n",
                     1, 2, 3, 4, 5, 6);
        /* Unload the control Data DMA MAP */

        vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
                            pRequest->usrDataDmaMapId);
        return USBHST_FAILURE;
        }

    /*
     * Create general TDs for data transfer
     * if the pTransferBuffer == NULL we only need one TD
     */

    if (pUrb->pTransferBuffer == NULL)
        {
        uNumOfDataTD = 1;
        uBufferAddr = 0;
        }
    else
        {
        /* Compute the number of transfer descriptors to be allocated */

        uNumOfDataTD = pUrb->uTransferLength / USB_OHCI_TD_MAXIMUM_TRANSFER_LENGTH;

        /* Check for remaining transfer length */

        if ((pUrb->uTransferLength %
             USB_OHCI_TD_MAXIMUM_TRANSFER_LENGTH) != 0)
            {
            /* Increment the number of transfer descriptors to be allocated */
            uNumOfDataTD++;
            }

        /* update the start data address */

        uBufferAddr =
            USB_OHCI_BUS_ADDR_LO32(pRequest->usrDataDmaMapId->fragList[0].frag);
        }

    bStatus = usbOhciCreateTDList(pHCDData,
                                  pHCDPipe,
                                  pRequest,
                                  (&pRequest->pTDListHead),
                                  (&pRequest->pTDListTail),
                                  uNumOfDataTD);
    if (FALSE == bStatus)
        {
        USB_OHCD_ERR("usbOhciSubmitBulkOrInterruptUrb - "
                     "usbOhciCreateTDList fail \n",
                     1, 2, 3, 4, 5, 6);
        /* Unload the control Data DMA MAP */

        vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
                            pRequest->usrDataDmaMapId);
        return USBHST_FAILURE;
        }

    /*
     * A dummy TD always at the tail of the TD list.
     * Add new TD List after the dummy TD.
     */

    pDummyTD = pHCDPipe->pHCDTDTail;
    pDummyTD->pRequestInfo = pRequest;
    pDummyTD->uNextTDPointer =
        USB_OHCD_SWAP_DATA(pHCDData->uBusIndex,
                           USB_OHCI_DESC_LO32(pRequest->pTDListHead));
    pDummyTD->pHCDNextTransferDescriptor = pRequest->pTDListHead;

    /* Let the old dummy TD as the head of the TD list */

    pRequest->pTDListHead = pDummyTD;

    /* Let the last TD as new dummy TD */

    pDummyTD = pRequest->pTDListTail;
    pDummyTD->pRequestInfo = NULL;

    /*
     * NOTE: There is always an empty transfer descriptor at the end of
     *       the transfer descritpor list for the endpoint.
     *
     *       In order to append the transfer, the following algorithm is
     *       followed:
     *
     *       a) Create the transfer list for the transfer
     *       b) Append the transfer list to the transfer list for the
     *          endpoint.
     *       c) Populate the transfer information. The transfer information
     *          should be populated from the previous last node of the
     *          transfer list for the endpoint.
     */

    /* Fill general TD to prepare transfer */

    pCurTD = pRequest->pTDListHead;

    uRemainingTransferSize = pUrb->uTransferLength;

    /* In this loop we set zero packet */

    for (uIndex = 0; uIndex < uNumOfDataTD; uIndex++)
        {

        /* Check whether a short transfer is acceptable */

        if ((pUrb->uTransferFlags & USBHST_SHORT_TRANSFER_OK) ==
                        USBHST_SHORT_TRANSFER_OK)
            {
            /* Set the 'BufferRounding' bit in the transfer descriptor */
            USB_OHCI_SET_TD_BUFFER_ROUND(pHCDData->uBusIndex,
                                         pCurTD->uControlInformation);
            }

        /*
         * Set the PID for the transfer descriptor based on the direction
         * of the data stage for the control transfer.
         */
        if (pRequest->uDataDir == USB_OHCD_DIR_IN)
            {
            /* Set the PID for IN transfer */
            USB_OHCI_SET_TD_PID_IN(pHCDData->uBusIndex,
                                   pCurTD->uControlInformation);
            }
        else
            {
            /* Set the PID for OUT transfer */
            USB_OHCI_SET_TD_PID_OUT(pHCDData->uBusIndex,
                                    pCurTD->uControlInformation);
            }

        uSize = (uRemainingTransferSize >
                 USB_OHCI_TD_MAXIMUM_TRANSFER_LENGTH) ?
                 USB_OHCI_TD_MAXIMUM_TRANSFER_LENGTH  :
                 uRemainingTransferSize;

        usbOhciFillTD(pHCDData->uBusIndex,
                      pCurTD,
                      uBufferAddr,
                      uSize);

        /* Update the tail pointer of the TD, as dummy TD is added into */

        if (uIndex == (uNumOfDataTD - 1))
            pRequest->pTDListTail = pCurTD;

        uRemainingTransferSize -= uSize;
        uBufferAddr += uSize;
        pCurTD = pCurTD->pHCDNextTransferDescriptor;
        }

    /* Update the HCD maintained tail pointer to the last TD */

    pHCDPipe->pHCDTDTail = pDummyTD;

    /*
     * Initialize the URB length to 0. This will be updated in the ISR
     * after the URB completion
     */

    pUrb->uTransferLength = 0;

    /*
     * Update the tail pointer of the transfer list corresponding
     * to the endpoint.
     */
    uBufferAddr = USB_OHCI_DESC_LO32(pDummyTD);

    pHCDPipe->uTDQueueTailPointer =
        USB_OHCD_SWAP_DATA(pHCDData->uBusIndex, uBufferAddr);

    /*
     * Clear the skip bit in the endpoint descriptor.
     *
     * NOTE: This operation should be synchronized with the cancellation
     *       of transfers. If not, it might happen that the SKIP bit
     *       was set for cancelling a transfer and the SKIP bit got
     *       cleared due a transfer being submitted when the cancellation
     *       is pending.
     *
     *       One solution is not to use SKIP bit in OHCI_CreatePipe() and
     *       OHCI_SubmitURB(). Use it only in OHCI_CancelURB().
     *
     *       Also check the usage of EndpointListAccess event for
     *       OHCI_SubmitURB() and OHCI_CancelURB().
     *
     *       PENDING
     */
    OHCI_CLEAR_ED_SKIP_BIT(pHCDData->uBusIndex,
                           pHCDPipe->uControlInformation);

    /*
     * NOTE: This function is called for both bulk and interrupt transfers.
     *       However, the Bulk List Filled (BLF) flag should be set only
     *       for bulk transfers.
     *
     *       An interrupt endpoint will have a non zero polling interval.
     *       This information is used to identify an interrupt and bulk
     *       endpoint.
     */
    if (pHCDPipe->uPollingInterval == 0)
        {
        USB_OHCI_SET_BLF(pHCDData);
        }

    return USBHST_SUCCESS;
    } /* End of function usbOhciPopulateBulkOrInterruptTransferDescriptor () */

/***************************************************************************
*
* BufferPageCross - checks if there is page cross in transfer buffer
*
* This function checks the start and end of the transfer buffer, see if they
*
* is page cross in transfer buffer.
*
* PARAMETERS:
* <uHostControllerIndex> (IN) - index of the host controller.
*
* <pTransferDescriptor (IN)> - Pointer to transfer descriptor.
*
* <uPacketBufferOffset (IN)> - Packet buffer offset.
*
* RETURNS: TRUE if there is page cross, FALSE if not.
*
* ERRNO:
*   None.
*/

LOCAL BOOL BufferPageCross
    (
    UINT32       uHostControllerIndex,
    PUSB_OHCI_TD pTransferDescriptor,
    UINT32       uPacketBufferOffset
    )
    {
    ULONG uPacketBufferEndPage=0;
    ULONG uPacketBufferStartPage=0;

    /* Packet Buffer start and end Address High 8 bits */

    uPacketBufferEndPage = (ULONG)(USB_OHCD_SWAP_DATA(uHostControllerIndex,
             pTransferDescriptor->uCurrentBufferPointer)
          + uPacketBufferOffset) & 0xF000;

    uPacketBufferStartPage = (ULONG)USB_OHCD_SWAP_DATA(uHostControllerIndex,
          pTransferDescriptor->uCurrentBufferPointer)
          & 0xF000;

    if (uPacketBufferEndPage !=uPacketBufferStartPage)
        return TRUE;
    else
        return FALSE;
    }

/***************************************************************************
*
* usbOhciSubmitIsochronousUrb - populates isoch. tr. desc.
*
* This function populates the transfer descriptor for isochronous transfers
*
* PARAMETERS: <uHostControllerIndex (IN)> - OHCI Host Controller index.
*
* <pHCDPipe (IN)> - Pointer to the endpoint descriptor.
*
* <pURB (IN)> - Pointer to the URB to be submitted.
*
* RETURNS: USBHST_SUCCESS on success
* USBHST_INVALID_PARAMETER if the parameters are not valid
*
* ERRNO:
*   None.
*/

USBHST_STATUS usbOhciSubmitIsochronousUrb
    (
    pUSB_OHCD_DATA                  pHCDData,
    pUSB_OHCD_PIPE                  pHCDPipe,
    pUSBHST_URB                     pUrb,
    pUSB_OHCD_REQUEST_INFO          pRequest
    )
    {
    /*
     * To hold the starting frame number (as read from the host controller
     * SOF register)
     */

    UINT16                  uStartingFrameNumber = 0;

    /* To hold the starting frame number for the transfer */

    UINT32                  uStartingFrameNumberForTransfer = 0;

    /* To hold the ending frame number for the transfer */

    UINT32                  uEndingFrameNumberForTransfer = 0;

    /* To hold the frame number corresponding to the last queued transfer */

    UINT32                  uLastQueuedTransferFrameNumber = 0;

    /*
     * NOTE: The following variables are intentionally 32 bits.
     *
     *       - uStartingFrameNumber
     *       - uStartingFrameNumberForTransfer
     *       - uEndingFrameNumberForTransfer
     *       - uLastQueuedTransferFrameNumber
     *
     *       This is because the frame number information returned by the
     *       host controller is 16 bits. While computing the frame number
     *       required for the transfer, it is likely that the frame number
     *       for the transfer will cross the 16 bit boundary. In such scenario
     *       extra logic is required to handle the boundrary condition. This
     *       extra logic is avoided by having the above variables as 32 bits.
     */
    /* To hold the number of packets for this transfer */

    UINT32  uNumberOfPackets = 0;

    /* To hold the software overhead required to support the new transfer */

    UINT32                  uSoftwareOverheadRequired = 0;

    /* To hold the status of the operation */

    USBHST_STATUS   nStatus = USBHST_SUCCESS;

    /* To hold the packet buffer offset */

    UINT32  uPacketBufferOffset = 0;

    /* To hold the remaining transfer size */

    UINT32  uRemainingTransferSize = 0;

    /* To hold the transfer specific data for isochronous transfers */

    pUSBHST_ISO_PACKET_DESC pIsochronousDataInfo = NULL;

    /* To hold the loop index */

    UINT32  uIndex = 0;

    /* To hold the pointer of the request */

    pUSB_OHCD_REQUEST_INFO   pPrevRequest = NULL;

    /* To hold the pointer of the TD */

    PUSB_OHCI_TD pLastTD = NULL;
    PUSB_OHCI_TD pCurTD = NULL;
    PUSB_OHCI_TD pDummyTD = NULL;

    NODE         *pNode = NULL;

    /* To hold the status */

    BOOLEAN bStatus = FALSE;
    STATUS  vxbStatus;

    /* To hold the buffer address */

    UINT32 uBufferAddr = 0x0;

    /* To hold the buffer offset */

    UINT32 uBufferOffset = 0x0;

    /* To hold the number of ISO TD */

    UINT32 uNumOfIsoTD = 0x0;

    UINT32 tdDatasize = 0x0;

    /*
     * To hold the number of packets to be transferred in this
     * transfer descriptor
     */

    UINT32 uNumberOfPacketsForThisTd = 0x0;

     /* To hold the starting buffer address for the transfer descriptor */

    UINT32 uStartingBufferAddress = 0x0;

    int i = 0x0;

    /*
     * Check whether the isochronous transfer is scheduled for
     *
     * a) A relative frame
     * b) As soon as possible
     *
     * Note: In addition to the above condition check, we should also check
     *       whether there are any other queued transfers for the specified
     *       isochronous endpoint. If so, the request should be appended to
     *       the end of the current queue of transfers.
     *
     *       PENDING: The above logic discussed in the 'Note' should be
     *                implemented.
     */

    /*
     * Enter the critical section before attempting to access the
     * request queues or the transfer descriptors for the endpoint.
     */

    OS_WAIT_FOR_EVENT(pHCDData->ohciCommonMutex, WAIT_FOREVER);

    /*
     * Old code get free request later, but we get free request before
     * we enter this routine.
     * We should get the last TD of the "last request" to get the schedule
     * frame number.
     * Now, the "last request" should be the prev request of the last
     * request list. As the new request always be the last one of the list.
     * This should be pay attention to, as this is the reason why the
     * ISO transfer will lose some TDs.
     */

    pNode = &(pRequest->listNode);
    pNode = lstPrevious(pNode);
    if (pNode != NULL)
        pPrevRequest = LIST_NODE_TO_USB_OHCD_REQUEST_INFO(pNode);

    /* Get the last TD of the request */

    if (NULL != pPrevRequest)
        pLastTD = pPrevRequest->pTDListTail;

    if (NULL != pLastTD)
        {
        /* Obtain the frame number for the last transfer */

        uLastQueuedTransferFrameNumber =
          USB_OHCI_GET_ISO_TD_FRAME_NUM(pHCDData->uBusIndex,
                                        pLastTD->uControlInformation);
        /*
         * Compute the number of packets for the last transfer and update
         * the frame number corresponding to the last transfer for the
         * endpoint.
         */

        uLastQueuedTransferFrameNumber +=
        USB_OHCI_GET_ISO_TD_PACKET(pHCDData->uBusIndex,
                                   pLastTD->uControlInformation);

        /*
         * Increment the last queued frame number. This will be used
         * for queing the next request
         */
        uLastQueuedTransferFrameNumber += 1;

        }

    /* Leave the critical section */

    OS_RELEASE_EVENT(pHCDData->ohciCommonMutex);

    /*
     * Obtain the frame number corresponding to the last queued
     * transfer (END)
     */

    /* Obtain the current frame number */

    nStatus = usbOhciGetFrameNumber ((UINT8)pHCDData->uBusIndex,
                                     (PUINT16)(&uStartingFrameNumber));

    /* Check whether the frame number was obtained successfully */

    if (nStatus != USBHST_SUCCESS)
        {
        /* Debug print */
        USB_OHCD_ERR("usbOhciGetFrameNumber failed", 1, 2, 3, 4, 5, 6);
        return nStatus;
        }

    /* Compute the software overhead required to support the transfer (BEGIN) */

    /*
     * NOTE: To queue one TD, assume that the time taken is 'x'. To
     *       queue 'n' TDs, the time taken would be 'n * x'.
     *
     *       Based on the above logic, add the required software
     *       delay.
     */

    /*
     * Compute the softwrae delay based on the number of TD's
     * required
     */

    uSoftwareOverheadRequired =
        (pUrb->uNumberOfPackets / USB_OHCI_NUMBER_OF_ISOCHRONOUS_PACKETS_PER_TD) *
        USB_OHCI_ISOCHRONOUS_SCHEDULING_SOFTWARE_DELAY_PER_TD;

    /* Check whether there are any residue packets */

    if (0 != (pUrb->uNumberOfPackets %
                  USB_OHCI_NUMBER_OF_ISOCHRONOUS_PACKETS_PER_TD))
        {
        /* Update the starting frame for the transfer */
        uSoftwareOverheadRequired +=
            USB_OHCI_ISOCHRONOUS_SCHEDULING_SOFTWARE_DELAY_PER_TD;
        }

    /* Compute the software overhead required to support the transfer (END) */

    /*
     * Compute the starting frame number for the transfer based on the
     * URB::uTransferFlags and URB::uStartFrame
     */

    if (USBHST_START_ISOCHRONOUS_TRANSFER_ASAP ==
        (pUrb->uTransferFlags & USBHST_START_ISOCHRONOUS_TRANSFER_ASAP))
        {
        /*
         * NOTE: Schedule the transfer based on the software overhead and
         * the transfers already queued for the endpoint.
         */

        /*
         * Check whether the frame number for the last queued transfer
         * is ahead of the current SOF value.
         *
         * If true, the new transfer will be queued after the last
         * queued transfer. If false, the new transfer will be queued after
         * the current SOF with suitable software delay.
         */

        if (uLastQueuedTransferFrameNumber > uStartingFrameNumber)
            {
            /*
             * Schedule the transfer after the transfers queued for the
             * endpoint
             */
            uStartingFrameNumberForTransfer = uLastQueuedTransferFrameNumber;
            }
        else
            {
            /* Schedule the transfer after the current start of frame */
            uStartingFrameNumberForTransfer = uStartingFrameNumber;
            }

        /*
         * Based on the software delay requried, compute the starting
         * frame from the transfer
         */
        if (uStartingFrameNumberForTransfer <= uStartingFrameNumber)
            {
            /*
             * Reset the starting frame for the transfer after the
             * current start of frame plus the software delay required.
             */
            uStartingFrameNumberForTransfer =
                uStartingFrameNumber + uSoftwareOverheadRequired;
            }

        }

    else
        {
        /*
         * Initialize the starting frame number for the transfer.
         *
         * NOTE: uStartingFrameNumberForTransfer is 32 bits and
         *       URB::uStartFrame is 16 bits.
         *
         *       Hence compute the starting frame number for the transfer
         *       based on the SOF and the URB::uStartFrame
         */
        uStartingFrameNumberForTransfer =
            (uStartingFrameNumber & 0xFFFFFC00) + pUrb->uStartFrame;


        if ((uLastQueuedTransferFrameNumber > uStartingFrameNumberForTransfer) ||
            (g_OHCDData[pHCDData->uBusIndex].bLastFrameRollover
             == TRUE)) /* special case for frame number rollover*/
            {
            /*
             * Schedule the transfer after the transfers queued for the
             * endpoint
             */
            uStartingFrameNumberForTransfer = uLastQueuedTransferFrameNumber;
            }
        }

    /* Compute the ending frame number of the transfer */

    uEndingFrameNumberForTransfer =
        uStartingFrameNumberForTransfer + pUrb->uNumberOfPackets;

    /*
     * To check if there is frame number rollover occured, if there is,
     * the next schedule will take this into account
     */

   if ((uEndingFrameNumberForTransfer & 0x10000) == 0x10000)
       {
       g_OHCDData[pHCDData->uBusIndex].bLastFrameRollover =  TRUE;
       }
   else
       {
       g_OHCDData[pHCDData->uBusIndex].bLastFrameRollover =  FALSE;
       }

    /*
     * Check whether the transfer can be supported based on the available
     * isochronous frame window.
     *
     * NOTE: The following algorithm is used for validating whether the
     *       transfer can be supported withing the isochronous frame window.
     *
     *       a) Starting frame number of the new transfer should be greater
     *          than ending frame number for the last queued transfer.
     *
     *          If the above condition is true, the following condition
     *          should be true
     *
     *          Ending frame number of the transfer is greater than the
     *          the current SOF.
     *
     *                                      (OR)
     *
     *          Ending frame number of the transfer is less than the current
     *          SOF
     *
     *       b) If condition (a) is FALSE, the following condition should be
     *          met.
     *
     *          Starting frame number of the new transfer is less than the
     *          ending frame number of the last queued transfer.
     *
     *                                      (AND)
     *
     *          Starting frame number of the new transfer is less than the
     *          current SOF (hardware frame number).
     *
     *          If the above condition is true, the following condition
     *          should be true.
     *
     *          Ending frame number of the transfer is less than the current
     *          SOF
     *
     *       c) If condition (a) or condition (b) is not true, the transfer
     *          cannot be supported.
     */

    if (((uStartingFrameNumberForTransfer >= uLastQueuedTransferFrameNumber) &&
          (uEndingFrameNumberForTransfer > uStartingFrameNumber)) ||
        ((uStartingFrameNumberForTransfer >= uLastQueuedTransferFrameNumber) &&
          (uStartingFrameNumberForTransfer < uStartingFrameNumber)) ||
        ((uStartingFrameNumberForTransfer < uLastQueuedTransferFrameNumber) ||
         (uEndingFrameNumberForTransfer < uStartingFrameNumber)))
        {
        /* Proceed to support the transfer */
        }
    else
        {
        /* Display the frame in which the packet will be processed */
        USB_OHCD_WARN("SOF (%d), Last Transfer Frame (%d), Starting Frame (%d), "
                      "Ending Frame (%d) ... \n",
                      uStartingFrameNumber,
                      uLastQueuedTransferFrameNumber,
                      uStartingFrameNumberForTransfer,
                      uEndingFrameNumberForTransfer, 5, 6);

        /* The transfer cannot be supported */
        return USBHST_BAD_START_OF_FRAME;
        }

    vxbStatus = vxbDmaBufMapLoad (pHCDData->pDev,
                                  pHCDPipe->usrBuffDmaTagId,
                                  pRequest->usrDataDmaMapId,
                                  pUrb->pTransferBuffer,
                                  pUrb->uTransferLength,
                                  VXB_DMABUF_MAPLOAD_CACHEALIGNCHECK);

    if (vxbStatus != OK)
        {
        USB_OHCD_ERR("usbOhciSubmitIsochronousUrb - "
                     "load usrDataDmaMapId fail\n",
                     1, 2, 3, 4, 5, 6);

        return USBHST_FAILURE;
        }

    /* Perform vxbDmaBufSync operation according to Data direction */

    if (OHCI_IN_ENDPOINT ==
         OHCI_GET_ENDPOINT_DIRECTION_FROM_ED(pHCDData->uBusIndex,
                                             pHCDPipe->uControlInformation))
        {
        pRequest->uDataDir = USB_OHCD_DIR_IN;
        /* DMA cache invalidate before data receive */

        vxbStatus = vxbDmaBufSync(pHCDData->pDev,
                                  pHCDPipe->usrBuffDmaTagId,
                                  pRequest->usrDataDmaMapId,
                                  _VXB_DMABUFSYNC_DMA_PREREAD);
        }
    else
        {
        pRequest->uDataDir = USB_OHCD_DIR_OUT;
        /* <copy to bounce buffer and> DMA cache flush */

        vxbStatus = vxbDmaBufSync(pHCDData->pDev,
                                  pHCDPipe->usrBuffDmaTagId,
                                  pRequest->usrDataDmaMapId,
                                  _VXB_DMABUFSYNC_DMA_PREWRITE);
        }

    if (vxbStatus != OK)
        {
        USB_OHCD_ERR("usbOhciSubmitIsochronousUrb - vxbDmaBufSync "
                     "usrDataDmaMapId fail \n", 1, 2, 3, 4, 5, 6);

        /* Unload the control Data DMA MAP */

        vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
                            pRequest->usrDataDmaMapId);

        return USBHST_FAILURE;
        }

    /*
     * Check whether the URB transfer buffer is valid. If the URB transfer buffer
     * is NULL, a zero length transfer will be submitted.
     */

    if (pUrb->pTransferBuffer == NULL)
        {
        uNumOfIsoTD = 1;
        uBufferAddr = 0x0;
        }
    else
        {
        /* Compute the number of transfer descriptors to be allocated */

        uNumOfIsoTD =
            (pUrb->uNumberOfPackets /USB_OHCI_NUMBER_OF_ISOCHRONOUS_PACKETS_PER_TD);

        /* Check for remaining transfer length */

        if ((pUrb->uNumberOfPackets %
             USB_OHCI_NUMBER_OF_ISOCHRONOUS_PACKETS_PER_TD) != 0)
            {
            /* Increment the number of transfer descriptors to be allocated */
            uNumOfIsoTD++;
            }

        uBufferAddr =
            USB_OHCI_BUS_ADDR_LO32(pRequest->usrDataDmaMapId->fragList[0].frag);
        }

    bStatus = usbOhciCreateTDList(pHCDData,
                                  pHCDPipe,
                                  pRequest,
                                 (&pRequest->pTDListHead),
                                 (&pRequest->pTDListTail),
                                 uNumOfIsoTD);
    if (FALSE == bStatus)
        {
        vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
                            pRequest->usrDataDmaMapId);
        return USBHST_FAILURE;
        }

    /*
     * A dummy TD always at the tail of the TD list.
     * Add new TD List after the dummy TD.
     */

    pDummyTD = pHCDPipe->pHCDTDTail;
    pDummyTD->pRequestInfo = pRequest;
    pDummyTD->uNextTDPointer =
        USB_OHCD_SWAP_DATA(pHCDData->uBusIndex,
                           USB_OHCI_DESC_LO32(pRequest->pTDListHead));
    pDummyTD->pHCDNextTransferDescriptor = pRequest->pTDListHead;

    /* Let the old dummy TD as the head of the TD list */

    pRequest->pTDListHead = pDummyTD;

    /* Let the last TD as new dummy TD */

    pDummyTD = pRequest->pTDListTail;
    pDummyTD->pRequestInfo = NULL;

    /*
     * NOTE: There is always an empty transfer descriptor at the end of
     *       the transfer descritpor list for the endpoint.
     *
     *       In order to append the transfer, the following algorithm is
     *       followed:
     *
     *       a) Create the transfer list for the transfer
     *       b) Append the transfer list to the transfer list for the
     *          endpoint.
     *       c) Populate the transfer information. The transfer information
     *          should be populated from the previous last node of the
     *          transfer list for the endpoint.
     */

    /* Fill isochrounous TD to prepare transfer */

    pCurTD = pRequest->pTDListHead;

    uRemainingTransferSize = pUrb->uTransferLength;

    /* Obtain the number of packets for this transfer */

    uNumberOfPackets = pUrb->uNumberOfPackets;

    /* Initialize the transfer specific data from isochronous transfers */

    pIsochronousDataInfo =
        (pUSBHST_ISO_PACKET_DESC) (pUrb->pTransferSpecificData);

    uBufferOffset = 0x0;

    /* In this loop we set zero packet */

    for (uIndex = 0; uIndex < uNumOfIsoTD; uIndex++)
        {
        uStartingBufferAddress = uBufferAddr + uBufferOffset;
        tdDatasize = 0x0;
        pCurTD->uCurrentBufferPointer =
             (USB_OHCD_SWAP_DATA (pHCDData->uBusIndex,
                                  (uStartingBufferAddress & 0xFFFFF000)));

        pCurTD->uControlInformation |=
           (USB_OHCD_SWAP_DATA (pHCDData->uBusIndex,
                                ((uStartingFrameNumberForTransfer + uIndex * 8)
                                  & 0x0000FFFF)));

        /*
         * Compute the number of packets to be transferred in this transfer
         * descriptor (BEGIN)
         */

        uNumberOfPacketsForThisTd = uNumberOfPackets - (uIndex * 8);

        /*
         * Check whether the number of packets to be transferred in this
         * transfer descriptor is greater than 8.
         */
        /* PENDING: Should not be hardcoded */
        if (uNumberOfPacketsForThisTd > 8)
            {
            /*
             * Reset the number of packets that can be transferred for
             * this transfer descriptor.
             */
            /* PENDING: Should not be hardcoded */
            uNumberOfPacketsForThisTd = 8;
            }

        /*
         * Compute the number of packets to be transferred in this transfer
         * descriptor (END)
         */

        /*
         * Update the number of packets to be transferred for this transfer
         * descriptor.
         */
        /* PENDING: Should not be hardcoded */
        pCurTD->uControlInformation |=
            USB_OHCD_SWAP_DATA (pHCDData->uBusIndex,
                (((uNumberOfPacketsForThisTd - 1) << 24) & 0x07000000));

        /*
         * Populate the offset information for the packets to be transfered
         * in this frame (BEGIN)
         */

        /* Program the packet size for packet offset 0 ~ 7 */

        for (i = 0; i < uNumberOfPacketsForThisTd; i++)
            {
            /* Compute the packet offset */

            uPacketBufferOffset = (uStartingBufferAddress & 0x00000FFF);
            uPacketBufferOffset +=
                 (pIsochronousDataInfo[uIndex * 8 + i].uOffset - uBufferOffset);

            tdDatasize += (pIsochronousDataInfo[uIndex * 8 + i].uLength);

            /* If there is a page cross, mark the 12th bit as 1 */

            if (BufferPageCross(pHCDData->uBusIndex,
                                pCurTD,
                                uPacketBufferOffset))
                {
                uPacketBufferOffset |= (1 << 12);
                }

            /*
             * Indicate that the status of the TD is initially not_accessed. This is taken
             * to be 14, as bit 0 would be used for checking if there is a page cross
             */
            uPacketBufferOffset |= (14 << 12);

            /* Program the packet size for packet offset i */

            if ((i % 2) != 0)
                pCurTD->uOffsetAndStatus[i/2] |=
                    USB_OHCD_SWAP_DATA(pHCDData->uBusIndex,
                                       ((uPacketBufferOffset & 0x0000FFFF) << 16));
            else
                pCurTD->uOffsetAndStatus[i/2] |=
                    USB_OHCD_SWAP_DATA(pHCDData->uBusIndex,
                                       (uPacketBufferOffset & 0x0000FFFF));
            }


        /*
         * Populate the offset information for the packets to be transfered
         * in this frame (END)
         */

        /*
         * Check whether the transfer is greater than the maximum transfer
         * size supported by the transfer descriptor.
         */

        if (uRemainingTransferSize > tdDatasize)
            {
            /*
             * Initialize the end of buffer for the transfer to the end of
             * of the page
             */

            pCurTD->uBufferEndPointer =
                    USB_OHCD_SWAP_DATA(pHCDData->uBusIndex,
                                       (uStartingBufferAddress + tdDatasize - 1));

            /* Update the offset information */

            uBufferOffset += (tdDatasize);

            /* Update the remaining transfer size */

            uRemainingTransferSize -= (tdDatasize);
            }
        else if (pUrb->pTransferBuffer != NULL) /*
                                                 * Check for zero length
                                                 * transfer
                                                 */
            {
            /*
             * Initialize the end of buffer for the transfer to the remaining
             * transfer length minus 1.
             */

            pCurTD->uBufferEndPointer =
                USB_OHCD_SWAP_DATA(pHCDData->uBusIndex,
                                   (uStartingBufferAddress + uRemainingTransferSize - 1));
            }

        /*
         * Update the tail pointer of the request,
         * as dummy TD is added into the list
         */

        if (uIndex == (uNumOfIsoTD - 1))
            pRequest->pTDListTail = pCurTD;

        pCurTD = pCurTD->pHCDNextTransferDescriptor;
        }

    /* Let the last TD of the new list as the new dummy TD */

    pHCDPipe->pHCDTDTail = pDummyTD;

    /*
     * Update the tail pointer pointer of the transfer list corresponding
     * to the endpoint.
     */

    pHCDPipe->uTDQueueTailPointer =
              USB_OHCD_SWAP_DATA(pHCDData->uBusIndex,
                                 USB_OHCI_DESC_LO32(pDummyTD));
    /*
     * Clear the skip bit in the endpoint descriptor.
     *
     * NOTE: This operation should be synchronized with the cancellation
     *       of transfers. If not, it might happen that the SKIP bit
     *       was set for cancelling a transfer and the SKIP bit got
     *       cleared due a transfer being submitted when the cancellation
     *       is pending.
     *
     *       One solution is not to use SKIP bit in OHCI_CreatePipe() and
     *       OHCI_SubmitURB(). Use it only in OHCI_CancelURB().
     *
     *       Also check the usage of EndpointListAccess event for
     *       OHCI_SubmitURB() and OHCI_CancelURB().
     *
     *       PENDING
     */

    OHCI_CLEAR_ED_SKIP_BIT(pHCDData->uBusIndex,
                           pHCDPipe->uControlInformation);

    return USBHST_SUCCESS;
    } /* End of function usbOhciSubmitIsochronousUrb () */

/***************************************************************************
*
* usbOhciProcessTransferCompletion - handles transfer completion
*
* This function handles the completion of the transfer. It is called to handle
* the done head interrupt
*
* PARAMETERS: <uHostControllerIndex (IN)> - OHCI Host Controller index.
*
* <pTransferDescriptor (IN)> - Pointer to the list of transfers completed.
*
* RETURNS: N/A
*
* ERRNO:
*   None.
*/

LOCAL VOID usbOhciProcessTransferCompletion
    (
    UINT32   uHostControllerIndex,
    PVOID    pTransferDescriptor
    )
    {
    /* To hold the pointer to the endpoint descriptor */

    pUSB_OHCD_PIPE pHCDPipe = NULL;

    /* To hold the pointer to the next completed transfer descriptor */

    PUSB_OHCI_TD   pNextTD = NULL;

    /* To hold the pointer to the temp transfer descriptor */

    PUSB_OHCI_TD   pTempTD = NULL;

    pUSB_OHCD_DATA pHCDData = NULL;

    UINT32         uBufferAddr = 0x0;

    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT(
        USB_OHCI_WV_EVENT_HANDLER,
        "usbOhciProcessTransferCompletion() starts",
        USB_OHCD_WV_FILTER);

    /* Check whether the OHCI host controller index is valid */
    /* Check whether the transfer descriptor pointer is valid */

    if ((uHostControllerIndex >= USB_MAX_OHCI_COUNT) ||
        (g_OHCDData[uHostControllerIndex].pDev == NULL)||
        (pTransferDescriptor == NULL))
        {
        /* Debug print */
        USB_OHCD_ERR("Invalid parameter: "
                     "uHostControllerIndex = %d,pTransferDescriptor = %p",
                     uHostControllerIndex, pTransferDescriptor, 3, 4, 5, 6);

        return;
        }
    pHCDData = &g_OHCDData[uHostControllerIndex];

    /*
     * NOTE: The least significant bit of the HccaDoneHead is set to 1
     *       to indicate whether an unmasked HcInterruptStatus was set
     *       when HccaDoneHead was written.
     *
     *       Due the above reason, the completed transfer descriptors list
     *       head pointer stored at the HccaDoneHead should be masked
     *       before attempting to use the transfer descriptors.
     *
     *       The transfer descriptor list stored at HccaDoneHead can contain
     *       both general and isochronous TDs. However, due to the above
     *       reason, only the LSb will be set if an unmasked interrupt
     *       has occurred. Hence, the transfer descriptor at HccaDoneHead
     *       is masked with the general transfer descriptor alignement
     *       mask.
     */

    /* Reverse the list of transfers completed */

    pTempTD = usbOhciReverseTransferCompletionList (pHCDData ,
                                                    pTransferDescriptor);

    /* Process the completed transfers */

    while (pTempTD != NULL)
        {
        /* Obtain the pointer to the endpoint descriptor */

        pHCDPipe = pTempTD->pHCDPipe;

        /* Check whether the endpoint descriptor is valid */

        if (pHCDPipe == NULL)
            {
            /* Invalid transfer descriptor */

            USB_OHCD_ERR("Invalid transfer descriptor %p.....\n",
                         pTempTD, 2, 3, 4, 5, 6);

            return;
            }

        /*
         * Obtain the pointer to the next completed transfer descriptor.
         *
         * NOTE: This operation should be done before calling the following
         *       functions:
         *
         *       a) OHCI_ProcessIsochronousTransferCompletion
         *       b) OHCI_ProcessGeneralTransferCompletion
         *
         *       This is because the above functions might release (OS_FREE)
         *       the memory of the completed transfer descriptor.
         */

        uBufferAddr = pTempTD->uNextTDPointer;

        /* Get the bus address in CPU endian */

        uBufferAddr = USB_OHCD_SWAP_DATA(uHostControllerIndex, uBufferAddr);

        pNextTD =(PUSB_OHCI_TD)
            (usbOhciPhyToVirt(pHCDData,
                              uBufferAddr,
                              USB_OHCI_MEM_MAP_TD));

        /* Check the transfer type for the endpoint */

        if (OHCI_IS_ISOCHRONOUS_ENDPOINT(uHostControllerIndex,
                pHCDPipe->uControlInformation) != 0)
            {
            /* Handle completion of isochroonous transfer */

            usbOhciProcessIsochronousTransferCompletion (uHostControllerIndex,
                                                         pTempTD);
            }
        else
            {
            /* Handle completion of general transfer */

            usbOhciProcessGeneralTransferCompletion (uHostControllerIndex,
                                                     pTempTD);
            }

        /* Update the pointer to the next completed transfer descriptor */

        pTempTD = pNextTD;

        } /* End of while (NULL != pTempTD) */

    return;

    } /* End of function usbOhciProcessTransferCompletion () */

/***************************************************************************
*
* usbOhciReverseTransferCompletionList - reverses completed transfers list
*
* This function reverses the list of transfers completed.
*
* PARAMETERS: <pTransferDescriptor (IN)> - Pointer to the list of transfers
* completed.
*
* RETURNS: the pointer to the reversed list of transfers completed, otherwise
* NULL
*
* ERRNO:
*   None.
*/

LOCAL PUSB_OHCI_TD usbOhciReverseTransferCompletionList
    (
    pUSB_OHCD_DATA  pHCDData,
    PUSB_OHCI_TD    pTransferDescriptor
    )
    {
    PUSB_OHCI_TD    pCurTD = NULL;
    PUSB_OHCI_TD    pNextTD = NULL;
    PUSB_OHCI_TD    pPrevTD = NULL;
    UINT32          uBufferAddr = 0x0;

    /*
     * This routine only have one caller: usbOhciProcessTransferCompletion
     * And we already do parameter valid check in the caller routine,
     * so we don't double check in this routine.
     */

    /*
     * Reverse the list of completed transfer descriptor.
     *
     * NOTE: The host controller reverses the list of transfers submitted.
     *       For ex, if the following list was submitted:
     *
     *       TRANS1 -> TRANS2 -> TRANS3 -> NULL
     *
     *       The transfers will be scheduled by the host controller on
     *       the order of the list.
     *
     *       Before the transfers are scheduled, the done head queue is
     *       empty.
     *
     *       DONE_HEAD -> NULL
     *
     *       After the first transfer is completed, the done head queue
     *       will be as follows:
     *
     *       DONE_HEAD -> TRANS1 -> NULL
     *
     *       After the second transfer is completed, the done head queue
     *       will be as follows:
     *
     *       DONE_HEAD -> TRANS2 -> TRANS1 -> NULL
     *
     *       After the third transfer is completed, the done head queue
     *       will be as follows:
     *
     *       DONE_HEAD -> TRANS3 -> TRANS2 -> TRANS1 -> NULL
     *
     */

    /* Reverse the list of transfers */

    pCurTD = pTransferDescriptor;

    while (pCurTD != NULL)
        {
        /* Get next TD pointer store the virtual address */

        pNextTD = usbOhciPhyToVirt(
                     pHCDData,
                     USB_OHCD_SWAP_DATA(pHCDData->uBusIndex, pCurTD->uNextTDPointer),
                     USB_OHCI_MEM_MAP_TD);

        pCurTD->uNextTDPointer = uBufferAddr;

        pPrevTD = pCurTD;

        uBufferAddr = USB_OHCI_DESC_LO32(pCurTD);
        uBufferAddr = USB_OHCD_SWAP_DATA(pHCDData->uBusIndex, uBufferAddr);

        pCurTD = pNextTD;
        }
    return pPrevTD;

    } /* End of function usbOhciReverseTransferCompletionList () */

/***************************************************************************
*
* usbOhciProcessGeneralTransferCompletion - processes general tr. completion
*
* This function handles the completion of the general transfer
*
* PARAMETERS: <uHostControllerIndex (IN)> - OHCI Host Controller index.
*
* <pTransferDescriptor (IN)> - Pointer to the transfer descriptor.
*
* RETURNS: N/A
*
* ERRNO:
*   None.
*/

LOCAL VOID usbOhciProcessGeneralTransferCompletion
    (
    UINT32           uHostControllerIndex,
    PUSB_OHCI_TD     pTransferDescriptor
    )
    {
    /* To hold the pointer to the endpoint descriptor */

    pUSB_OHCD_PIPE pHCDPipe = NULL;

    /* To hold the temporary pointer to the temporary transfer descriptor */

    PUSB_OHCI_TD   pTempTD = NULL;

    /* To hold the pointer of Compare TD */

    PUSB_OHCI_TD   pCompareTD = NULL;

    /*
     * To hold the pointer to the first element of the list of transfers
     * pending.
     */

    pUSB_OHCD_REQUEST_INFO          pRequest = NULL;

    pUSB_OHCD_DATA                  pHCDData = NULL;

    UINT32                          uBufferAddr = 0x0;

    NODE                            *pNode = NULL;

    /* Obtain the transfer completion code */

    USB_OHCI_TRANSFER_STATUS    uTransferStatus = 0;

    /* To hold the URB status codes */

    USBHST_STATUS           nUrbStatus[16] =
        {
        USBHST_SUCCESS,                     /* Transfer successful */
        USBHST_FAILURE,                     /* CRC Error */
        USBHST_FAILURE,                     /* Bit stuffing error */
        USBHST_FAILURE,                     /* Data toggle mismatch */
        USBHST_STALL_ERROR,                 /* Stall */
        USBHST_DEVICE_NOT_RESPONDING_ERROR, /* Device not responding */
        USBHST_FAILURE,                     /* PID Check Failure */
        USBHST_FAILURE,                     /* Unexpected PID */
        USBHST_DATA_OVERRUN_ERROR,          /* Data overrun */
        USBHST_DATA_UNDERRUN_ERROR,         /* Data underrun */
        USBHST_FAILURE,                     /* Reserved */
        USBHST_FAILURE,                     /* Reserved */
        USBHST_BUFFER_OVERRUN_ERROR,        /* Buffer overrun */
        USBHST_BUFFER_UNDERRUN_ERROR,       /* Buffer underrun */
        USBHST_FAILURE,                     /* Not accessed */
        USBHST_FAILURE                      /* Not accessed */
        };

    pHCDData = &g_OHCDData[uHostControllerIndex];

    pTempTD = (PUSB_OHCI_TD)(pTransferDescriptor);

    /* Obtain the pointer to the endpoint descriptor */

    pHCDPipe = pTempTD->pHCDPipe;

    /* Get the head request of the ED request list */

    pNode = lstFirst(&(pHCDPipe->requestList));

    if (NULL != pNode)
       pRequest = LIST_NODE_TO_USB_OHCD_REQUEST_INFO(pNode);

    if (NULL == pRequest)
        {
        USB_OHCD_ERR("request is invalid \n",
                     1, 2, 3, 4, 5, 6);
        return;
        }


    OS_WAIT_FOR_EVENT(pHCDPipe->PipeSynchEventID, WAIT_FOREVER);

    /* Check whether the transfer completed successfully */

    if (USB_OHCI_IS_GENERAL_TRANSFER_SUCCESSFUL(uHostControllerIndex,
                     pTempTD->uControlInformation) == FALSE)
        {
        /* Check whether the endpoint is halted */

        if (OHCI_IS_ENDPOINT_HALTED(uHostControllerIndex,
                                    pHCDPipe) == TRUE)
            {
            /* Clear the endpoint halt condition */

            OHCI_CLEAR_ENDPOINT_HALT(uHostControllerIndex, pHCDPipe);
            }

        /* Obtain the transfer completion status */

        uTransferStatus = (UINT8)USB_OHCI_GET_TRANSFER_COMPLETION_CODE(
                              uHostControllerIndex,
                              pTempTD->uControlInformation);


        /* Display the error code */

        USB_OHCD_DBG(
            "ERROR: URB %p, Transfer Completion Code 0x%02X\n",
            pRequest->pUrb,
            uTransferStatus,
            3, 4, 5, 6);

       /* check if the transfer descriptor obtained the is last TD (dummy TD) */

       if (pTransferDescriptor != pRequest->pTDListTail)
           {
            /*
             * Update the list of transfer descriptors maintained for the
             * endpoint descriptor (BEGIN).
             */

            /*
             * NOTE: If the transfer resulted in an error, the transfers
             *       descriptors (TDs) corresponding to that transfer should
             *       be removed from the list of TDs maintained by the endpoint
             *       descriptor (ED).
             *
             *       For ex, consider a Control IN transfer. This would have the
             *       TDs for SETUP, DATA IN and DATA OUT phase.
             *
             *       If the SETUP phase results in an error, the TDs corresponding
             *       to the DATA IN and DATA OUT phase should be removed.
             *
             *       Similarly, for the other transfers.
             *
             * NOTE: It is assumed that the error corresponds to the transfer at
             *       the head of the transfer list. This assumption is valid
             *       because the OHCI controller services the transfers from the
             *       head of the list and not inbetween the list.
             */

            /*
             * Initialize the pointer to the head of the transfer descriptor
             * list
             */

            /* Convert into CPU endian */

            uBufferAddr = USB_OHCD_SWAP_DATA(uHostControllerIndex,
                                      pHCDPipe->uTDQueueHeadPointer);

            /* Get the actual bus address in CPU endian */

            uBufferAddr &= (~USB_OHCI_TD_ALIGN_MASK);

            pTempTD =(PUSB_OHCI_TD)usbOhciPhyToVirt(pHCDData,
                                                    uBufferAddr,
                                                    USB_OHCI_MEM_MAP_TD);


            /*
             * Search the transfer descriptor list to obtain the last transfer
             * descriptor corresponding to this URB.
             */

            while ((NULL != pTempTD) &&
                   (pTempTD != pRequest->pTDListTail))
                {
                /* Obtain the pointer to the next transfer descriptor */

                pTempTD = pTempTD->pHCDNextTransferDescriptor;
                } /* End of while ((NULL != pTempTD) && ... ) */
            }

        /*
         * Check whether the last transfer descriptor corresponding to this
         * URB is obtained.
         */

        if ((NULL != pTempTD) &&
            (pTempTD == pRequest->pTDListTail))
            {
            /* Obtain the pointer to the next transfer descriptor */

            pTempTD = pTempTD->pHCDNextTransferDescriptor;

            /*
             * Update the transfer descriptor list head for the transfer
             * The head should point to the tail element's next. The toggle
             * value should be carried forward.
             */

            uBufferAddr = USB_OHCI_DESC_LO32(pTempTD);

            pHCDPipe->uTDQueueHeadPointer =
                USB_OHCD_SWAP_DATA(uHostControllerIndex, uBufferAddr);
            }

        /*
         * Update the list of transfer descriptors maintained for the endpoint
         * descriptor (END).
         */

        /*
         * Obtain the pointer to the last transfer descriptor corresponding
         * to the transfer.
         */

        pTempTD = pRequest->pTDListTail;

        /* Terminate the list of transfers corresponding to the transfer */

        pTempTD->pHCDNextTransferDescriptor = NULL;

        /* Reset the last transfer completed for the endpoint */

        pHCDPipe->pLastCompletedTD = NULL;

        /* Release all the TDs of this request */

        usbOhciFreeRequestTDList(pHCDData,
                                 pHCDPipe,
                                 pRequest);

        /* Release the synchronization event */

        OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

        /*
         * If the pUrb is cancelled, we will call it's callback function
         * at cancelUrb routine and set the pUrb->pHcdSpecific to "NULL".
         * The "caller" routine can release the pUrb after that. So it's
         * dangerous to access(set) the pUrb's member if the pUrb->pHcdSpecific
         * is set to NULL.
         * So we only update "pUrb->nStatus" if (pUrb->pHcdSpecific != NULL)
         */

        USB_OHCD_CALL_URB_CALLBACK(pRequest->pUrb, nUrbStatus[uTransferStatus]);
        
        /* Take the synchronization event */
		
        OS_WAIT_FOR_EVENT(pHCDPipe->PipeSynchEventID, WAIT_FOREVER);

        /* Release the memory allocated for the URB list */

        usbOhciReleaseRequestInfo(pHCDData,
                                  pHCDPipe,
                                  pRequest);
        
        /* Release the synchronization event */

        OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

        /* Return from the function */

        return;
        }    /* End of if (USB_OHCI_IS_GENERAL_TRANSFER_SUCCESSFUL ... */

    /* Update the TD's actual length if the buffer pointer is valid */

   if (pTempTD->uStartAddressPointer != 0)
       {

       /*
        * The currentbuffer pointer is not NULL, when the size of data transfer
        * requested is more than the size of data actually transferred
        */

       if (pTempTD->uCurrentBufferPointer != 0)
           {
           pTempTD->uTDActualLength =
           (USB_OHCD_SWAP_DATA(uHostControllerIndex, pTempTD->uCurrentBufferPointer)
                                  - pTempTD->uStartAddressPointer);
           }
       /*
        * If the currentbufferpointer is NULL, then the size of the data transferred
        * is the same as that requested.
        */

       else
           {
           pTempTD->uTDActualLength =
           (USB_OHCD_SWAP_DATA(uHostControllerIndex, pTempTD->uBufferEndPointer)
                                  - (UINT32)pTempTD->uStartAddressPointer);

           pTempTD->uTDActualLength += 1;
           }
       }

    if (pRequest->pUrb != NULL)
        pRequest->pUrb->uTransferLength += pTempTD->uTDActualLength;

    /*
     * If the pLastCompletedTD is NULL
     * The complete TD must be the head of the TD list
     * If the pLastCompletedTD is not NULL
     * The complete TD must be the next TD of the pLastCompletedTD
     * Or else there must be some error happen
     */

    if (pHCDPipe->pLastCompletedTD == NULL)
        {
        pCompareTD = pRequest->pTDListHead;
        }
    else
        {
        pTempTD =  pHCDPipe->pLastCompletedTD;
        pCompareTD = pTempTD->pHCDNextTransferDescriptor;
        }

    /* Invalid transfer completed */

    if ((pTransferDescriptor != pCompareTD) && (pCompareTD != NULL))
        {
        USB_OHCD_ERR(
            "Invalid transfer completed. "
            "Expecting (%p), Completed (%p).\n",
            pCompareTD,
            pTransferDescriptor,
            3, 4, 5, 6);

        /* Release the synchronization event */

        OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

        return;
        }

    /*
     * If the TD equal to the tail of the TD list. It means we complete
     * the transfer without error, update URB status and call URB's
     * callback function to notify the upper.
     * Or else, we only need to update the pLastCompletedTD
     * to pointer to the new complete TD
     */

    if (pTransferDescriptor == pRequest->pTDListTail)
        {

        /*
         * Obtain the pointer to the last transfer descriptor corresponding
         * to the transfer.
         */

        pTempTD = (PUSB_OHCI_TD)pRequest->pTDListTail;

        /* Terminate the list of transfers corresponding to the transfer */

        pTempTD->pHCDNextTransferDescriptor = NULL;

        /* Reset the last transfer completed for the endpoint */

        pHCDPipe->pLastCompletedTD = NULL;

        /*
         * If there is any real data transfer, we should do
         * vxbDmaBufSync and unload the DMA MAP.
         */

        if (pRequest->uRequestLength != 0)
            {
            /* Check if the data transfer is IN direction */

            if (pRequest->uDataDir == USB_OHCD_DIR_IN)
                {
                /* DMA cache invalidate and <copy from bounce buffer> */

                vxbDmaBufSync(pHCDData->pDev,
                          pHCDPipe->usrBuffDmaTagId,
                          pRequest->usrDataDmaMapId,
                          _VXB_DMABUFSYNC_DMA_POSTREAD);

                }
            else
                {
#if 0 /* Don't use _VXB_DMABUFSYNC_DMA_POSTWRITE for now */
                /* DMA cache invalidate and <copy from bounce buffer> */

                vxbDmaBufSync(pHCDData->pDev,
                          pHCDPipe->usrBuffDmaTagId,
                          pRequest->usrDataDmaMapId,
                          _VXB_DMABUFSYNC_DMA_POSTWRITE);
#endif
                }

            /* Unload the Data DMA MAP */

            vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
            pRequest->usrDataDmaMapId);
            }

        /* Control transfer should also deal with the Setup buffer */

        if (pHCDPipe->uEndpointType == USBHST_CONTROL_TRANSFER)
            {
            /* Setup transaction is always DMA OUT */

#if 0 /* Don't use _VXB_DMABUFSYNC_DMA_POSTWRITE for now */

            /* DMA cache invalidate and <copy from bounce buffer> */

            vxbDmaBufSync(pHCDData->pDev,
                    pHCDPipe->ctrlSetupDmaTagId,
                    pRequest->ctrlSetupDmaMapId,
                    _VXB_DMABUFSYNC_DMA_POSTWRITE);
#endif
            /* Unload the control Setup DMA MAP */

            vxbDmaBufMapUnload (pHCDPipe->ctrlSetupDmaTagId,
                pRequest->ctrlSetupDmaMapId);
            }

        /* Release all the TDs of the request */

        usbOhciFreeRequestTDList(pHCDData,
                                 pHCDPipe,
                                 pRequest);

        /* Release the synchronization event */

        OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

        USB_OHCD_CALL_URB_CALLBACK(pRequest->pUrb, USBHST_SUCCESS);
        
        /* Take the synchronization event */
		
        OS_WAIT_FOR_EVENT(pHCDPipe->PipeSynchEventID, WAIT_FOREVER);

        /* Release the memory allocated for the URB list */

        usbOhciReleaseRequestInfo(pHCDData,
                                  pHCDPipe,
                                  pRequest);
								  
        /* Release the synchronization event */
								  
        OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

        }
    else
        {
        /* Update the last transfer completed for the endpoint */

        pHCDPipe->pLastCompletedTD = pTransferDescriptor;

        /* Release the synchronization event */

        OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);
        }
    return;
    } /* End of function usbOhciProcessGeneralTransferCompletion () */

/***************************************************************************
*
* usbOhciProcessIsochronousTransferCompletion - handles isoch. tr. completion
*
* This function handles the completion of the an isochronous transfer
*
* PARAMETERS: <uHostControllerIndex (IN)> - OHCI Host Controller index.
*
* <pTransferDescriptor (IN)> - Pointer to the transfer descriptor.
*
* RETURNS: N/A
*
* ERRNO:
*   None.
*/

LOCAL VOID usbOhciProcessIsochronousTransferCompletion
    (
    UINT32           uHostControllerIndex,
    PUSB_OHCI_TD     pTransferDescriptor
    )
    {
    pUSB_OHCD_DATA   pHCDData = NULL;

    /* To hold the pointer to the endpoint descriptor */

    pUSB_OHCD_PIPE   pHCDPipe = NULL;

    /* To hold the temporary pointer to the temporary transfer descriptor */

    PUSB_OHCI_TD     pTempTD = NULL;

    PUSB_OHCI_TD     pCompareTD = NULL;

    /* To hold the pointer to the request info */

    pUSB_OHCD_REQUEST_INFO    pRequest = NULL;

    NODE                      *pNode  = NULL;

    /* Obtain the transfer completion code */

    USB_OHCI_TRANSFER_STATUS    uTransferStatus = 0;

    /* To hold the URB status codes */

    USBHST_STATUS           nURBStatus[16] =
        {
        USBHST_SUCCESS,                     /* Transfer successful */
        USBHST_FAILURE,                     /* CRC Error */
        USBHST_FAILURE,                     /* Bit stuffing error */
        USBHST_FAILURE,                     /* Data toggle mismatch */
        USBHST_STALL_ERROR,                 /* Stall */
        USBHST_DEVICE_NOT_RESPONDING_ERROR, /* Device not responding */
        USBHST_FAILURE,                     /* PID Check Failure */
        USBHST_FAILURE,                     /* Unexpected PID */
        USBHST_DATA_OVERRUN_ERROR,          /* Data overrun */
        USBHST_DATA_UNDERRUN_ERROR,         /* Data underrun */
        USBHST_FAILURE,                     /* Reserved */
        USBHST_FAILURE,                     /* Reserved */
        USBHST_BUFFER_OVERRUN_ERROR,        /* Buffer overrun */
        USBHST_BUFFER_UNDERRUN_ERROR,       /* Buffer underrun */
        USBHST_FAILURE,                     /* Not accessed */
        USBHST_FAILURE                      /* Not accessed */
        };

    if (NULL == pTransferDescriptor )
        {
        USB_OHCD_ERR("usbOhciProcessIsochronousTransferCompletion -"
                     "invalid pTransferDescriptor \n",
                     1, 2, 3, 4, 5, 6);
        return;
        }

    pTempTD = (PUSB_OHCI_TD)pTransferDescriptor;

    pHCDData = &g_OHCDData[uHostControllerIndex];

    /* Obtain the pointer to the endpoint descriptor */

    pHCDPipe = pTempTD->pHCDPipe;

    /*
     * If pTempTransferDescriptor is NULL or
     * the endpoint's URB list head empty, return
     */

    pNode = lstFirst(&(pHCDPipe->requestList));

    if (pNode != NULL)
        pRequest = LIST_NODE_TO_USB_OHCD_REQUEST_INFO(pNode);

    if (NULL == pRequest)
        {
        USB_OHCD_ERR("usbOhciProcessIsochronousTransferCompletion - "
                     "request is null \n", 1, 2, 3, 4, 5, 6);
        return;
        }

    /* Exclusively access the synchronization event */

    OS_WAIT_FOR_EVENT(pHCDPipe->PipeSynchEventID, WAIT_FOREVER);

    /* Check whether the transfer completed successfully */

    if (USB_OHCI_IS_ISOCHRONOUS_TRANSFER_SUCCESSFUL(uHostControllerIndex,
                     pTempTD->uControlInformation) == FALSE)
        {

        /* Obtain the transfer completion status */

        uTransferStatus = (UINT8)USB_OHCI_GET_TRANSFER_COMPLETION_CODE(
                              uHostControllerIndex,
                              pTempTD->uControlInformation);

        /* Display the error code */

        USB_OHCD_DBG(
            "ERROR: URB %p, Transfer Completion Code 0x%02X\n",
            (ULONG) pRequest->pUrb,
            uTransferStatus,
            3, 4, 5, 6);

        /* Update the URB information for isochronous transfers */

        usbOhciUpdateIsochronousTransferURBInformation(
                   uHostControllerIndex, pRequest);

        /* Update the last completed transfer descriptor */

        pHCDPipe->pLastCompletedTD = pTempTD;

        /*
         * Check whether the last transfer descriptor corresponding to this
         * URB is obtained.
         */

        if (pTempTD == pRequest->pTDListTail)
            {

            /* Terminate the list of transfers corresponding to the transfer */

            pTempTD->pHCDNextTransferDescriptor = NULL;

            MOVE_REQUEST_FROM_PIPE_TO_FREE_LIST(pHCDPipe, pRequest);

            /* Reset the last transfer completed for the endpoint */

            pHCDPipe->pLastCompletedTD = NULL;

            /* Release all TDs of the request */

            usbOhciFreeRequestTDList(pHCDData,
                                     pHCDPipe,
                                     pRequest);

            /* Release the synchronization event */

            OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

            /*
             * Update the Urb's status and call it's callback function
             */

            USB_OHCD_CALL_URB_CALLBACK(pRequest->pUrb,
                                       nURBStatus[uTransferStatus]);
            
            /* Release the synchronization event */
			
            OS_WAIT_FOR_EVENT(pHCDPipe->PipeSynchEventID, WAIT_FOREVER);

            /* Release the memory allocated for the URB list */
            
            usbOhciReleaseRequestInfo(pHCDData,
                                      pHCDPipe,
                                      pRequest);
			
            /* Release the synchronization event */
            
            OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);
            }
        else
            {
            /* Release the synchronization event */
            OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);
            }

        /* Return from the function */
        return;
        }

    /*
     * If the pLastCompletedTD is NULL
     * The complete TD must be the head of the TD list
     * If the pLastCompletedTD is not NULL
     * The complete TD must be the next TD of the pLastCompletedTD
     * Or else there must be some error happen
     */

    if (pHCDPipe->pLastCompletedTD == NULL)
        {
        pCompareTD = pRequest->pTDListHead;
        }
    else
        {
        pTempTD =  pHCDPipe->pLastCompletedTD;
        pCompareTD = pTempTD->pHCDNextTransferDescriptor;
        }


    /* Invalid transfer completed */

    if (pTransferDescriptor != pCompareTD)
        {
        USB_OHCD_ERR(
           "Invalid transfer completed. "
           "Expecting (%p), Completed (%p).\n",
           (ULONG) pCompareTD,
           (ULONG) pTransferDescriptor,
           3, 4, 5, 6);

        /* Release the synchronization event */

        OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);
        return;
        }

    /*
     * If the TD equal to the tail of the TD list. It means we complete
     * the transfer without error, update URB status and call URB's
     * callback function to notify the upper.
     * Or else, we only need to update the pLastCompletedTD
     * to pointer to the new complete TD
     */

    if (pTransferDescriptor == pRequest->pTDListTail)
        {
        /*
         * If there is any real data transfer, we should do
         * vxbDmaBufSync and unload the DMA MAP.
         */

        if (pRequest->uRequestLength != 0)
            {
            /* Check if the data transfer is IN direction */

            if (pRequest->uDataDir == USB_OHCD_DIR_IN)
                {
                /* DMA cache invalidate and <copy from bounce buffer> */

                vxbDmaBufSync(pHCDData->pDev,
                              pHCDPipe->usrBuffDmaTagId,
                              pRequest->usrDataDmaMapId,
                              _VXB_DMABUFSYNC_DMA_POSTREAD);
                }
            else
                {
#if 0 /* Don't use _VXB_DMABUFSYNC_DMA_POSTWRITE for now */
                /* DMA cache invalidate and <copy from bounce buffer> */

                vxbDmaBufSync(pHCDData->pDev,
                              pHCDPipe->usrBuffDmaTagId,
                              pRequest->usrDataDmaMapId,
                              _VXB_DMABUFSYNC_DMA_POSTWRITE);
#endif
                }

            /* Unload the Data DMA MAP */

            vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
                                pRequest->usrDataDmaMapId);
            }

        /* Update the URB information for isochronous transfers */

        usbOhciUpdateIsochronousTransferURBInformation(uHostControllerIndex,
                                                       pRequest);
        /*
         * Obtain the pointer to the last transfer descriptor corresponding
         * to the transfer.
         */

        pTempTD = (PUSB_OHCI_TD)pRequest->pTDListTail;

        /* Terminate the list of transfers corresponding to the transfer */

        pTempTD->pHCDNextTransferDescriptor = NULL;

        /* Reset the last transfer completed for the endpoint */

        pHCDPipe->pLastCompletedTD = NULL;

        /* Release all the TDs of the request */

        usbOhciFreeRequestTDList(pHCDData,
                                 pHCDPipe,
                                 pRequest);

        /* Release the synchronization event */

        OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

        /* Update Urb's status and call it's callback routine */

        USB_OHCD_CALL_URB_CALLBACK(pRequest->pUrb, USBHST_SUCCESS);
		
        /* Take the synchronization event */
		
        OS_WAIT_FOR_EVENT(pHCDPipe->PipeSynchEventID, WAIT_FOREVER);

        /* Release the memory allocated for the URB list */

        usbOhciReleaseRequestInfo(pHCDData,
                                  pHCDPipe,
                                  pRequest);
        
        /* Release the synchronization event */

        OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

        }
    else
        {
        /* Update the last transfer completed for the endpoint */

        pHCDPipe->pLastCompletedTD = pTransferDescriptor;

        /* Release the synchronization event */

        OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);
        }

    return;
    } /* End of function usbOhciProcessIsochronousTransferCompletion () */

/***************************************************************************
*
* usbOhciAddNodeToEndpointDescriptorList - adds a node
*
* This function adds a node to the periodic endpoint descriptor list
*
* PARAMETERS: <uHostControllerIndex (IN)> - OHCI Host Controller index.
*
* <pNewEndpointDescriptor (IN)> - Pointer to the new endpoint descriptor.
*
* <uPollingInterval (IN)> - Polling interval for the endpoint.
*
* <uEndpointBandwidth (IN)> - Bandwidth required for the endpoint.
*
* RETURNS: N/A
*
* ERRNO:
*   None.
*/

LOCAL USBHST_STATUS usbOhciAddNodeToEndpointDescriptorList
    (
    UINT32                            uHostControllerIndex,
    pUSB_OHCD_PIPE                    pHCDPipe,
    UINT8                             uPollingInterval,
    UINT32                            uEndpointBandwidth
    )
    {
    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA           pHCDData = NULL;

    /*
     * To hold the status of the operation to add the new endpoint descriptor
     * to the periodic list.
     */

    USBHST_STATUS   nStatus = USBHST_SUCCESS;

    /*
     * To hold the index corresponding to the node with the maximum
     * free bandwidth available at present.
     */

    INT8    nMaximumBandwidthNodeIndex = (INT8) -1;

    /* To hold the loop index */

    UINT8   uIndex = 0;

    /*
     * To hold the interrupt list node index.
     *
     * For ex, if an endpoint descriptor should be created for 1 ms polling
     * interval, the endpoint descriptor will be added to all the
     * MAXIMUM_POLLING_INTERVAL (32) nodes. Similarly, if an endpoint
     * descriptor should be created for 2 ms polling interval, it will be
     * added to MAXIMUM_POLLING_INTERVAL / 2 (16) alternate nodes.
     */

    UINT8   uInterruptListNodeIndex = 0;

    /*
     * To hold the result of operation to check whether the endpoint
     * descriptor is already added to the queue.
     */

    BOOLEAN bEndpointDescriptorPresent = FALSE;

    /* Check whether the OHCI host controller index is valid */
    /* Check whether the pointer to the new endpoint descriptor is valid */

    if ((uHostControllerIndex >= USB_MAX_OHCI_COUNT)||
        (g_OHCDData[uHostControllerIndex].pDev == NULL)||
        (pHCDPipe == NULL))
        {
        USB_OHCD_ERR("invalid parameter"
                     " uHostControllerIndex = %d,pHCDPipe = %p \n",
                     uHostControllerIndex, pHCDPipe, 3, 4, 5, 6);
        return USBHST_INVALID_PARAMETER;
        }

    /* Modify the polling interval (BEGIN) */

    /*
     * NOTE: OHCI supports polling intervals of only 1, 2, 4, 8, 16, 32 milli
     *       seconds. Modify the polling interval requested accordingly.
     */

    /* Check whether the polling interval is greater than 32 ms */

    if (uPollingInterval >= 32)
        {
        /* Reduce the pollig interval to 32 ms */
        uPollingInterval = 32;
        }
    else if (uPollingInterval >= 16)
        {
        /* Reduce the polling interval to 16 ms */
        uPollingInterval = 16;
        }
    else if (uPollingInterval >= 8)
        {
        /* Reduce the polling interval to 8 ms */
        uPollingInterval = 8;
        }
    else if (uPollingInterval >= 4)
        {
        /* Reduce the polling interval to 4 ms */
        uPollingInterval = 4;
        }
    else if (uPollingInterval >= 2)
        {
        /* Reduce the polling interval to 2 ms */
        uPollingInterval = 2;
        }
    else
        {
        /* Reduce the polling interval to 1 ms */
        uPollingInterval = 1;
        }

    /* Modify the polling interval (END) */

    /* Obtain the node with the maximum bandwidth available */

    nMaximumBandwidthNodeIndex =
        usbOhciGetNodeWithMaximumAvailableBandwidth (uHostControllerIndex,
                                                      uPollingInterval,
                                                      uEndpointBandwidth);

    /* Check whether a node is available */

    if (nMaximumBandwidthNodeIndex == (INT8) -1)
        {
        return USBHST_INSUFFICIENT_BANDWIDTH;
        }

    /* Obtain the pointer to the OHCI host controller information */

    pHCDData = &g_OHCDData[uHostControllerIndex];

    /*
     * Initialize the interrupt list node index to the first node with
     * the maximum available bandwidth.
     *
     * The new endpoint descriptor will be added starting from this node.
     * For ex, if an endpoint descriptor with 1 ms polling interval is to
     * be created, it will be added to nodes with index 0, 1, 2, 3, ... 31.
     *
     * Similary, if an enpdoint descriptor with 2 ms polling interval is
     * to be created, it can be added to nodes with index 0, 2, 4, ... 30
     * (or) 1, 3, 5, ... 31.
     */

    uInterruptListNodeIndex = (UINT8) nMaximumBandwidthNodeIndex;

    /* Update the polling interval for the new endpoint descriptor */

    pHCDPipe->uPollingInterval = uPollingInterval;

    /*
     * Compute the location of the next endpoint descriptor corresponding
     * to the new endpoint descriptor. Append the new endpoint descriptor
     * to the endpoint descriptor list. (BEGIN)
     *
     * NOTE: The new endpoint descriptor will be added at the end of
     *       the list of endpoint descriptors pending for the specified
     *       polling interval.
     *
     *       For ex. if the polling interval for the new endpoint is 2,
     *       the endpoint will be added at the end of endpoint
     *       descriptors with polling interval 2. In this example, the next
     *       endpoint descriptor for the new endpoint descriptor will be
     *       the head of the endpoint descriptor list with  1 ms polling
     *       interval (or) NULL.
     */

    /*
     * Append the new endpoint descriptor to the head of the endpoint
     * descriptor list. For ex, if an endpoint descriptor for 2 ms
     * polling interval is added and the 'first node index' is 1,
     * update the head of the endpoint descriptor list for index
     * 1, 3, 5, ... 31.
     */

    while (uIndex < (USB_OHCI_MAXIMUM_POLLING_INTERVAL / uPollingInterval))
        {
        /*
         * Call the function to check whether the new endpoint descriptor
         * is already added to the specified interval.
         */

        bEndpointDescriptorPresent =
            usbOhciIsNodeFoundInPeriodicList ((UINT8)uHostControllerIndex,
                                                uInterruptListNodeIndex,
                                                pHCDPipe);

        /*
         * Check whether the endpoint descriptor is already present in
         * the list.
         */
        if (!bEndpointDescriptorPresent) /* Should be boolean */
            {
            /* Call the function to add the endpoint descriptor to the list */

            nStatus = usbOhciAddPeriodicNodeAtSpecifedInterval (
                          (UINT8)uHostControllerIndex,
                          uInterruptListNodeIndex,
                          pHCDPipe,
                          uEndpointBandwidth);

            /*
             * Check whether the new node was successfully added to the
             * periodic list.
             */
            if (nStatus != USBHST_SUCCESS)
                {
                return nStatus;
                }
            }

        /* Update the index corresponding to the next node element */

        uInterruptListNodeIndex =
            (UINT8)(uInterruptListNodeIndex + uPollingInterval);
        uInterruptListNodeIndex =
            (UINT8)(uInterruptListNodeIndex % USB_OHCI_MAXIMUM_POLLING_INTERVAL);

        /* Update the loop index */
        uIndex++;

        } /* End of while (uIndex < (USB_OHCI_MAXIMUM_POLLING_INTERVAL ... )) */

    return USBHST_SUCCESS;
    } /* End of function usbOhciAddNodeToEndpointDescriptorList () */

/***************************************************************************
*
* usbOhciGetNodeWithMaximumAvailableBandwidth - processes root hub status change
*
* This function obtains the node in the interrupt list with maximum available
* bandwidth for the specified polling interval.
*
* PARAMETERS: <uHostControllerIndex (IN)> - OHCI Host Controller index.
*
* <uPollingInterval (IN)> - Polling interval for the endpoint.
*
* <uEndpointBandwidth (IN)> - Bandwidth required for the endpoint.
*
* RETURNS: index of the node with the maximum available bandwidth, -1 otherwise.
*
* ERRNO:
*   None.
*/

LOCAL INT8 usbOhciGetNodeWithMaximumAvailableBandwidth
    (
    UINT32 uHostControllerIndex,
    UINT8  uPollingInterval,
    UINT32 uEndpointBandwidth
    )
    {
    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA                   pHCDData = NULL;

    /* To hold the pointer of the interrupt table */

    PUINT32                          pInterruptTable = NULL;

    /* To hold the buffer address */

    UINT32                           uBufferAddr = 0x0;

    /* To hold the maximum bandwidth available */

    UINT32  uMaximumBandwidth = 0;

    /*
     * To hold the index corresponding to the node with the maximum
     * bandwidth available.
     */

    INT8    nMaximumBandwidthNodeIndex = -1;

    /* To hold the loop index */

    UINT8   uIndex = 0;

    /* INTEGRATION TESTING CHANGE - Start */

    pUSB_OHCD_PIPE  pPeriodicPipe = NULL;

    /* INTEGRATION TESTING CHANGE - End */

    /* Check whether the OHCI host controller index is valid */

    if (uHostControllerIndex >= USB_MAX_OHCI_COUNT ||
        g_OHCDData[uHostControllerIndex].pDev == NULL)
        {
        return -1;
        }

    /* Obtain the pointer to the OHCI host controller information */

    pHCDData = &g_OHCDData[uHostControllerIndex];

    /* Obtain the pointer to the HccaInterruptTable */
    /*
     * The following sentence want to get the address of
     * pHCDData->pHcca->uHccaInterruptTable
     * But in DIAB compiler, we will get warning as
     * "dangerous to take address of member of packed or swapped structure"
     * The "uHccaInterruptTable" must be the first of the structure
     * So we will get the HCCA address to instead.
     */

    pInterruptTable = (PUINT32)(pHCDData->pHcca);

    /*
     * Traverse the endpoint descriptor list to obtain the node with
     * the maximum available bandwidth.
     */

    for (uIndex = 0; uIndex < uPollingInterval; uIndex++)
        {
        /* Get the physical address of the interrupt ED */

        uBufferAddr = pInterruptTable[uIndex];

        /* Get the bus address in CPU endian */

        uBufferAddr = USB_OHCD_SWAP_DATA(uHostControllerIndex,
                            uBufferAddr);

        /* Get the virtual address of the interrupt ED */

        pPeriodicPipe = (pUSB_OHCD_PIPE)
            usbOhciPhyToVirt(pHCDData,
                             uBufferAddr,
                             USB_OHCI_MEM_MAP_ED);

        /* Check whether the interrupt list is valid */

        if (pPeriodicPipe == NULL)
            {
            /*
             * Check whether a node has already been detected. If not mark the
             * current node as the node with the maximum available bandwidth.
             */

            if (uMaximumBandwidth <
                pHCDData->uMaximumBandwidthAvailable)
                {
                /* Update the maximum bandwidth available */

                uMaximumBandwidth =
                    pHCDData->uMaximumBandwidthAvailable;

                /* Update the node with the maximum bandwidth available */

                nMaximumBandwidthNodeIndex = (INT8) uIndex;
                }
            }
        else if (uEndpointBandwidth <
                 pPeriodicPipe->uBandwidthAvailable)
            {
            /*
             * Check whether the bandwidth available in the previously
             * identified node is less than the bandwidth available in the
             * current node.
             */

            if (uMaximumBandwidth < pPeriodicPipe->uBandwidthAvailable)
                {
                /* Update the maximum bandwidth available */
                uMaximumBandwidth = pPeriodicPipe->uBandwidthAvailable;

                /* Update the node with the maximum bandwidth available */
                nMaximumBandwidthNodeIndex = (INT8) uIndex;
                }
            }
        }

    return nMaximumBandwidthNodeIndex;
    } /* End of function usbOhciGetNodeWithMaximumAvailableBandwidth () */

/***************************************************************************
*
* usbOhciIsNodeFoundInPeriodicList - checks for presence of endpoint descriptor
*
* This function checks whether an endpoint descriptor is already added to
* the periodic list
*
* PARAMETERS: <uHostControllerIndex (IN)> - OHCI Host Controller index.
*
* <uPeriodicListIndex (IN)> - Index into the periodic list corresponding to the
* location where the node is to be searched.
*
* <pEndpointDesriptor (IN)> - Pointer to the endpoint descriptor which has to be
* compared in the periodic list.
*
* RETURNS: TRUE if the endpoint descriptor is already present, FALSE otherwise
*
* ERRNO:
*   None.
*/

LOCAL BOOLEAN usbOhciIsNodeFoundInPeriodicList
    (
    UINT8                           uHostControllerIndex,
    UINT8                           uPeriodicListIndex,
    pUSB_OHCD_PIPE                  pHCDPipe
    )
    {
    /* To hold the pointer to the current endpoint */

    pUSB_OHCD_PIPE     pTempPipe = NULL;

    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA     pHCDData = NULL;

    /* To hold the pointer of the HccaInterruptTable */

    PUINT32            pInterruptTable = NULL;

    /* To hold buffer address */

    UINT32             uBufferAddr = 0x0;

    /* Check whether the OHCI host controller index is valid */

    if (uHostControllerIndex >= USB_MAX_OHCI_COUNT ||
        g_OHCDData[uHostControllerIndex].pDev == NULL)
        {
        /* Debug print */
        USB_OHCD_ERR("invalid host controller index %d \n",
                     uHostControllerIndex,
                     2, 3, 4, 5, 6);
        return FALSE;
        }

    /* Obtain the pointer to the OHCI host controller information */

    pHCDData = &g_OHCDData[uHostControllerIndex];

    /* Obtain the pointer of HccaInterruptTable */
    /*
     * The following sentence want to get the address of
     * pHCDData->pHcca->uHccaInterruptTable
     * But in DIAB compiler, we will get warning as
     * "dangerous to take address of member of packed or swapped structure"
     * The "uHccaInterruptTable" must be the first of the structure
     * So we will get the HCCA address to instead.
     */

    pInterruptTable = (PUINT32)(pHCDData->pHcca);

    /* Get the physical address of the current interrupt ED */

    uBufferAddr = pInterruptTable[uPeriodicListIndex];

    /* Get the bus address in CPU endian */

    uBufferAddr = USB_OHCD_SWAP_DATA(uHostControllerIndex,
                        uBufferAddr);

    /* Get the virtual address of the current interrupt ED */

    pTempPipe = (pUSB_OHCD_PIPE)
     usbOhciPhyToVirt(pHCDData,
                      uBufferAddr,
                      USB_OHCI_MEM_MAP_ED);

    /* Check whether the endpoint descriptor is present in the HccaInteruptTable */

    while (pTempPipe != NULL)
        {
        /*
         * Check whether the current endpoint descriptor corresponds to the
         * endpoint descriptor to be checked.
         */

        if (pTempPipe == pHCDPipe)
            {
            /* Endpoint descriptor is found in the list */
            /* Debug print */

            USB_OHCD_VDBG("endpoint descriptor is found in the "
                          "periodic list \n", 1, 2, 3, 4, 5, 6);
            return TRUE;
            }

        /* Traverse to the next endpoint in the list */

        pTempPipe = pTempPipe->pHCDNextPipe;
        }

    /* Endpoint descriptor is not found in the list */
    return FALSE;

    } /* End of function usbOhciIsNodeFoundInPeriodicList () */

/***************************************************************************
*
* usbOhciAddPeriodicNodeAtSpecifedInterval - processes root hub status change
*
* This function adds the new endpoint descriptor to the specified location in
* the periodic list.
*
* PARAMETERS: <uHostControllerIndex (IN)> - OHCI Host Controller index.
*
* <uInterruptListNodeIndex (IN)> - Index into the periodic list corresponding to
* the location where the node will be inserted.
*
* <pNewEndpointDesriptor (IN)> - Pointer to the new endpoint descriptor to be
* added to the periodic list.
*
* RETURNS: N/A
*
* ERRNO:
*   None.
*/

LOCAL USBHST_STATUS usbOhciAddPeriodicNodeAtSpecifedInterval
    (
    UINT8                            uHostControllerIndex,
    UINT8                            uInterruptListNodeIndex,
    pUSB_OHCD_PIPE                   pHCDPipe,
    UINT32                           uEndpointBandwidth
    )
    {
    /*
     * To hold the pointer to the current endpoint desriptor node while
     * traversing the endpoint descriptor list.
     */

    pUSB_OHCD_PIPE pTempPipe = NULL;

    /*
     * To hold the pointer to the next endpoint descriptor in the endpoint
     * descriptor list.
     */

    pUSB_OHCD_PIPE pNextPipe = NULL;

    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA                pHCDData = NULL;

    /* To hold the pointer of the HccaInterruptTable */

    PUINT32                       pInterruptTable = NULL;

    /* To hole the physical address of the interrupt ED */

    UINT32    uBufferAddr = 0x0;

    /*
     * Compute the location of the next endpoint descriptor corresponding
     * to the new endpoint descriptor. Append the new endpoint descriptor
     * to the endpoint descriptor list.
     *
     * NOTE: The new endpoint descriptor will be added at the end of
     *       the list of endpoint descriptors pending for the specified
     *       polling interval.
     *
     *       For ex. if the polling interval for the new endpoint is 2,
     *       the endpoint will be added at the end of endpoint
     *       descriptors with polling interval 2. In this example, the next
     *       endpoint descriptor for the new endpoint descriptor will be
     *       the head of the endpoint descriptor list with  1 ms polling
     *       interval (or) NULL.
     */


    /* Check whether the OHCI host controller index is valid */
    /* Check whether the endpoint descriptor pointer is valid */

    if ((uHostControllerIndex >= USB_MAX_OHCI_COUNT)||
        (g_OHCDData[uHostControllerIndex].pDev == NULL) ||
        (pHCDPipe == NULL))
        {
        /* Debug print */
        USB_OHCD_ERR("invalid parameter:"
                     "uHostControllerIndex = %d, pHCDPipe = %p \n",
                     uHostControllerIndex, pHCDPipe, 3, 4, 5, 6);
        return USBHST_INVALID_PARAMETER;
        }

    /* Obtain the pointer to the OHCI host controller information */

    pHCDData = &g_OHCDData[uHostControllerIndex];

    /* Obtain the pointer to the periodic list */
    /*
     * The following sentence want to get the address of
     * pHCDData->pHcca->uHccaInterruptTable
     * But in DIAB compiler, we will get warning as
     * "dangerous to take address of member of packed or swapped structure"
     * The "uHccaInterruptTable" must be the first of the structure
     * So we will get the HCCA address to instead.
     */

    pInterruptTable = (PUINT32)(pHCDData->pHcca);

    /* Get the physical address of the interrupt ED */

    uBufferAddr = pInterruptTable[uInterruptListNodeIndex];

    /* Get the bus address in CPU endian */

    uBufferAddr = USB_OHCD_SWAP_DATA(uHostControllerIndex,
                        uBufferAddr);

    /* Get the virtual address of the interrupt ED */

    pTempPipe = (pUSB_OHCD_PIPE)
     usbOhciPhyToVirt(pHCDData,
                      uBufferAddr,
                      USB_OHCI_MEM_MAP_ED);


    /* Check whether the pointer to the head of the list is valid */

    if (pTempPipe != NULL)
        {
        /*
         * Check whether the polling interval of the head node is less than
         * than the polling interval of the new endpoint descriptor.
         */

        if (pTempPipe->uPollingInterval <
            pHCDPipe->uPollingInterval)
            {
            /*
             * Append the new endpoint descriptor to the head of the endpoint
             * descriptor list.
             */

            pHCDPipe->uNextEDPointer =
             USB_OHCD_SWAP_DATA(uHostControllerIndex,
                                USB_OHCI_DESC_LO32(pTempPipe));

            /* Store the value passed in the HCD's next pointer */

            pHCDPipe->pHCDNextPipe = pTempPipe;

            /* Update the bandwidth available in the endpoint descriptor list */

            pHCDPipe->uBandwidthAvailable =
              pTempPipe->uBandwidthAvailable - uEndpointBandwidth;

            /*
             * Append the new endpoint descriptor to the head of the endpoint
             * descriptor list
             */

            pInterruptTable[uInterruptListNodeIndex] =
               USB_OHCD_SWAP_DATA(uHostControllerIndex,
                                  USB_OHCI_DESC_LO32(pHCDPipe));

            }
        else /* Else of if (pTempEndpointDescriptor->uPollingInterval < ... ) */
            {
            /* Reset the pointer to the next endpoint descriptor */

            pNextPipe = NULL;

            /*
             * Traverse the list to obtain the pointer of the node to
             * which the new endpoint descriptor will be appended.
             */

            while (pTempPipe->uNextEDPointer != 0)
                {
                /* Update the pointer to the next endpoint descriptor */

                pNextPipe = pTempPipe->pHCDNextPipe;

                /*
                 * Check whether the polling interval of the next node in the
                 * list is less than the polling interval of the new endpoint
                 * descriptor.
                 */

                if (pNextPipe->uPollingInterval <
                    pHCDPipe->uPollingInterval)
                    {
                    /*
                     * Break from the loop. The new endpoint descriptor will
                     * be appended after the current endpoint descriptor.
                     */

                    break;
                    }

                /* Move to the next endpoint descriptor in the list */

                pTempPipe = pTempPipe->pHCDNextPipe;

                } /* End of while (NULL != pTempEndpointDescriptor->pNext ... ) */

            /*
             * Update the pointer to next endpoint descriptor following the
             * new endpoint descriptor.
             */

            /* Append the new endpoint descriptor to the interrupt list */

            uBufferAddr = USB_OHCI_DESC_LO32(pTempPipe->pHCDNextPipe);
            pHCDPipe->uNextEDPointer =
                USB_OHCD_SWAP_DATA(uHostControllerIndex, uBufferAddr);

            pHCDPipe->pHCDNextPipe = pTempPipe->pHCDNextPipe;

            /* Append the new endpoint descriptor to the interrupt list */

            uBufferAddr = USB_OHCI_DESC_LO32(pHCDPipe);

            pTempPipe->uNextEDPointer =
               USB_OHCD_SWAP_DATA(uHostControllerIndex, uBufferAddr);

            /* Update the HCD maintained next pointer */

            pTempPipe->pHCDNextPipe = pHCDPipe;

            /* Update the bandwidth available in the current interrupt list */

            pTempPipe->uBandwidthAvailable =
                        pTempPipe->uBandwidthAvailable -
                        uEndpointBandwidth;

            } /* End of else for if (pTempEndpointDescriptor->uPollingInte ... ) */

        }
    else /* Else of if (pTempEndpointDescriptor != NULL) */
        {
        /*
         * Append the new endpoint descriptor to the head of the endpoint
         * descriptor list
         */

        uBufferAddr = USB_OHCI_DESC_LO32(pHCDPipe);

        pInterruptTable[uInterruptListNodeIndex] =
            USB_OHCD_SWAP_DATA(uHostControllerIndex, uBufferAddr);

        pHCDPipe->uBandwidthAvailable =
           pHCDData->uMaximumBandwidthAvailable - uEndpointBandwidth;

        }

    return USBHST_SUCCESS;
    } /* End of function usbOhciAddPeriodicNodeAtSpecifedInterval () */

/***************************************************************************
*
* usbOhciUpdateIsochronousTransferURBInformation - updates URB structure
*
* This function is used to update the URB information for isochronous
* transfers. This function is called on completion of the transfer.
*
* PARAMETERS:
* <uHostControllerIndex> (IN)- index of the host controller.
*
* <pURBInformation (IN)> - Holds the information corresponding
* to the URB.
*
* RETURNS: N/A.
*
* ERRNO:
*   None.
*/


LOCAL VOID usbOhciUpdateIsochronousTransferURBInformation
    (
    UINT32                         uHostControllerIndex, /* index of HC */
    pUSB_OHCD_REQUEST_INFO         pRequest /* URB information */
    )
    {
    /* To hold the temporary pointer to the isochronous transfer descriptor */

    PUSB_OHCI_TD   pTempTransferDescriptor = NULL;

    /* To hold the transfer specific data for isochronous transfers */

    pUSBHST_ISO_PACKET_DESC                 pIsochronousDataInfo = NULL;

    /* To hold the number of packets for this transfer */

    UINT32                                  uNumberOfPackets = 0;

    /* To hold the packet index corresponding to the packet being updated */

    UINT32                                  uPacketIndex = 0;

    /* To hold the current packet status and length information */

    UINT16                                  uPacketInformation = 0;

    /* Check whether the URB information pointer is valid */

    if ((NULL == pRequest) || (NULL == pRequest->pUrb))
        {
        USB_OHCD_ERR("invalid parameter: pRequest or pRequest->pUrb is NULL\n",
                     1, 2, 3, 4, 5, 6);
        return;
        }

    /*
     * Initialize the pointer to the isochronous transfer descriptors
     * associated with the endpoint.
     */

    pTempTransferDescriptor = (PUSB_OHCI_TD)pRequest->pTDListHead;

    /* Obtain the number of packets for this transfer */

    uNumberOfPackets = pRequest->pUrb->uNumberOfPackets;

    /* Initialize the transfer specific data from isochronous transfers */

    pIsochronousDataInfo = (pUSBHST_ISO_PACKET_DESC)
                           (pRequest->pUrb->pTransferSpecificData);

    /*
     * Update the URB information based on the information obtained from
     * the transfer descriptors.
     */

    while ((NULL != pTempTransferDescriptor) &&
           (pTempTransferDescriptor !=
            pRequest->pTDListTail) &&
           (uPacketIndex < uNumberOfPackets))
        {
        /* Update the length and status information for the packet (BEGIN) */

        /* Program the packet size for packet offset 0 */

        if ((0 == (uPacketIndex % USB_OHCI_NUMBER_OF_ISOCHRONOUS_PACKETS_PER_TD)) &&
            (uPacketIndex < uNumberOfPackets))
            {
            /* Obtain the packet status and length information */

            uPacketInformation = (UINT16)
            (USB_OHCD_SWAP_DATA(uHostControllerIndex,
                                pTempTransferDescriptor->uOffsetAndStatus[0])
                                & 0x0000FFFF);

            /* Update the packet status */

            pIsochronousDataInfo[uPacketIndex].nStatus =
                (INT8)((uPacketInformation & 0xF000) >> 12);

            /* Update the packet length */

            pIsochronousDataInfo[uPacketIndex].uLength =
                (UINT32)(uPacketInformation & 0x0FFF);

            /* Update the packet index */

            uPacketIndex++;
            }

        /* Program the packet size for packet offset 1 */

        if ((1 == (uPacketIndex % USB_OHCI_NUMBER_OF_ISOCHRONOUS_PACKETS_PER_TD)) &&
            (uPacketIndex < uNumberOfPackets))
            {
            /* Obtain the packet status and length information */

            uPacketInformation = (UINT16)
            ((USB_OHCD_SWAP_DATA(uHostControllerIndex,
                                 pTempTransferDescriptor->uOffsetAndStatus[0]) &
                                 0xFFFF0000) >> 16);

            /* Update the packet status */

            pIsochronousDataInfo[uPacketIndex].nStatus =
                (INT8)((uPacketInformation & 0xF000) >> 12);

            /* Update the packet length */

            pIsochronousDataInfo[uPacketIndex].uLength =
                (UINT32)(uPacketInformation & 0x0FFF);

            /* Update the packet index */

            uPacketIndex++;
            }

        /* Program the packet size for packet offset 2 */

        if ((2 == (uPacketIndex % USB_OHCI_NUMBER_OF_ISOCHRONOUS_PACKETS_PER_TD)) &&
            (uPacketIndex < uNumberOfPackets))
            {
            /* Obtain the packet status and length information */

            uPacketInformation = (UINT16)
            (USB_OHCD_SWAP_DATA(uHostControllerIndex,
                                pTempTransferDescriptor->uOffsetAndStatus[1])
                                 & 0x0000FFFF);

            /* Update the packet status */

            pIsochronousDataInfo[uPacketIndex].nStatus =
                (INT8)((uPacketInformation & 0xF000) >> 12);

            /* Update the packet length */

            pIsochronousDataInfo[uPacketIndex].uLength =
                (UINT32)(uPacketInformation & 0x0FFF);

            /* Update the packet index */

            uPacketIndex++;
            }

        /* Program the packet size for packet offset 3 */

        if ((3 == (uPacketIndex % USB_OHCI_NUMBER_OF_ISOCHRONOUS_PACKETS_PER_TD)) &&
            (uPacketIndex < uNumberOfPackets))
            {
            /* Obtain the packet status and length information */

            uPacketInformation = (UINT16)
            ((USB_OHCD_SWAP_DATA(uHostControllerIndex,
                                 pTempTransferDescriptor->uOffsetAndStatus[1]) &
                                 0xFFFF0000) >> 16);

            /* Update the packet status */

            pIsochronousDataInfo[uPacketIndex].nStatus =
                (INT8)((uPacketInformation & 0xF000) >> 12);

            /* Update the packet length */

            pIsochronousDataInfo[uPacketIndex].uLength =
                (UINT32)(uPacketInformation & 0x0FFF);

            /* Update the packet index */

            uPacketIndex++;
            }

        /* Program the packet size for packet offset 4 */

        if ((4 == (uPacketIndex % USB_OHCI_NUMBER_OF_ISOCHRONOUS_PACKETS_PER_TD)) &&
            (uPacketIndex < uNumberOfPackets))
            {
            /* Obtain the packet status and length information */

            uPacketInformation = (UINT16)
            (USB_OHCD_SWAP_DATA(uHostControllerIndex,
                                pTempTransferDescriptor->uOffsetAndStatus[2])
                                & 0x0000FFFF);

            /* Update the packet status */

            pIsochronousDataInfo[uPacketIndex].nStatus =
                (INT8)((uPacketInformation & 0xF000) >> 12);

            /* Update the packet length */

            pIsochronousDataInfo[uPacketIndex].uLength =
                (UINT32)(uPacketInformation & 0x0FFF);

            /* Update the packet index */

            uPacketIndex++;
            }

        /* Program the packet size for packet offset 5 */

        if ((5 == (uPacketIndex % USB_OHCI_NUMBER_OF_ISOCHRONOUS_PACKETS_PER_TD)) &&
            (uPacketIndex < uNumberOfPackets))
            {
            /* Obtain the packet status and length information */

            uPacketInformation = (UINT16)
            ((USB_OHCD_SWAP_DATA(uHostControllerIndex,
                                 pTempTransferDescriptor->uOffsetAndStatus[2]) &
                                 0xFFFF0000) >> 16);

            /* Update the packet status */

            pIsochronousDataInfo[uPacketIndex].nStatus =
                (INT8)((uPacketInformation & 0xF000) >> 12);

            /* Update the packet length */

            pIsochronousDataInfo[uPacketIndex].uLength =
                (UINT32)(uPacketInformation & 0x0FFF);

            /* Update the packet index */

            uPacketIndex++;
            }

        /* Program the packet size for packet offset 6 */

        if ((6 == (uPacketIndex % USB_OHCI_NUMBER_OF_ISOCHRONOUS_PACKETS_PER_TD)) &&
            (uPacketIndex < uNumberOfPackets))
            {
            /* Obtain the packet status and length information */

            uPacketInformation = (UINT16)
            (USB_OHCD_SWAP_DATA(uHostControllerIndex,
                                pTempTransferDescriptor->uOffsetAndStatus[3])
                                & 0x0000FFFF);

            /* Update the packet status */

            pIsochronousDataInfo[uPacketIndex].nStatus =
                (INT8)((uPacketInformation & 0xF000) >> 12);

            /* Update the packet length */

            pIsochronousDataInfo[uPacketIndex].uLength =
                (UINT32)(uPacketInformation & 0x0FFF);

            /* Update the packet index */

            uPacketIndex++;
            }

        /* Program the packet size for packet offset 7 */

        if ((7 == (uPacketIndex % USB_OHCI_NUMBER_OF_ISOCHRONOUS_PACKETS_PER_TD)) &&
            (uPacketIndex < uNumberOfPackets))
            {
            /* Obtain the packet status and length information */

            uPacketInformation = (UINT16)
            ((USB_OHCD_SWAP_DATA(uHostControllerIndex,
                                 pTempTransferDescriptor->uOffsetAndStatus[3]) &
                                 0xFFFF0000) >> 16);

            /* Update the packet status */

            pIsochronousDataInfo[uPacketIndex].nStatus =
                (INT8)((uPacketInformation & 0xF000) >> 12);

            /* Update the packet length */

            pIsochronousDataInfo[uPacketIndex].uLength =
                (UINT32)(uPacketInformation & 0x0FFF);

            /* Update the packet index */

            uPacketIndex++;
            }

        /* Update the length and status information for the packet (END) */

        /* Obtain the pointer to the next transfer descriptor */

        pTempTransferDescriptor =
            pTempTransferDescriptor->pHCDNextTransferDescriptor;

        } /* End of while ((NULL != pTempTransferDescriptor) && ...) */

    return;

    } /* End of function usbOhciUpdateIsochronousTransferURBInformation() */

/***************************************************************************
*
* usbOhciReleaseBandwidthForCurrentConfiguration - Release BW for configuration
*
* This function is used to release the bandwidth allocated for the
* current configuration.
*
* PARAMETERS: <uHostControllerIndex (IN)> - OHCI Host Controller index.
*
* <uDeviceAddress (IN)> - Specifies the address of the device.
*
* <uDeviceSpeed (IN)> - Specifies the device speed.
*
* <puAvailableBandwidth (IN/OUT)> - Pointer to the array holding the
* bandwidth available.
*
* RETURNS: N/A.
*
* ERRNO:
*   None.
*/

LOCAL VOID usbOhciReleaseBandwidthForCurrentConfiguration
    (
    UINT8       uHostControllerIndex,
    UINT8       uDeviceAddress,
    UINT8       uDeviceSpeed,
    PUINT32     puAvailableBandwidth
    )
    {
    /* To hold the polling interval */

    UINT32                          uPollingInterval = 0;

    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA                  pHCDData = NULL;

    /*
     * To hold the pointer to the current endpoint desriptor node while
     * traversing the endpoint descriptor list.
     */

    pUSB_OHCD_PIPE   pTempEndpointDescriptor = NULL;

    /* To hold the endpoint address */

    UINT8                           uEndpointAddress = 0;

    /* To hold the direction of the endpoint */

    UINT8                           uEndpointDirection = OHCI_IN_ENDPOINT;

    /* To hold the maximum packet size for the endpoint */

    UINT16                          uMaximumPacketSize = 0;

    /* To hold the bandwidth required by the endpoint */

    UINT32                          uBandwidthRequiredForEndpoint = 0;

    UINT32                          uBufferAddr = 0x0;

    PUINT32                         pInterruptTable = NULL;

    /* Obtain the pointer to the OHCI host controller information */

    pHCDData = &g_OHCDData[uHostControllerIndex];

    /* Obtain the pointer to the periodic list */
    /*
     * The following sentence want to get the address of
     * pHCDData->pHcca->uHccaInterruptTable
     * But in DIAB compiler, we will get warning as
     * "dangerous to take address of member of packed or swapped structure"
     * The "uHccaInterruptTable" must be the first of the structure
     * So we will get the HCCA address to instead.
     */

    pInterruptTable = (PUINT32)(pHCDData->pHcca);

    /*
     * NOTE: The interrupt and isochronous endpoint list is as follows:
     *
     *       32 MS -> 16 MS -> 8 MS -> 4 MS -> 2 MS -> 1 MS.
     *
     *       Algorithm:
     *
     *       a) For all the endpoints corresponding to the specified
     *          device address, the bandwidth used by the endpoint is
     *          computed.
     *
     *       b) The bandwidth computed in the above step is added to
     *          the available bandwidth value for the corresponding
     *          polling interval.
     *
     *       c) All the paths through the interrupt and isochronous list
     *          are traversed and the bandwidth for all the endpoints
     *          corresponding to the specified device address is
     *          released.
     */

    /*
     * For all the polling intervals, search for the endpoints
     * corresponding to the specified device address and release the
     * bandwidth allocated for the endpoint.
     */

    for (uPollingInterval = 0;
         uPollingInterval < USB_OHCI_MAXIMUM_POLLING_INTERVAL;
         uPollingInterval++)
        {
        /*
         * Obtain the pointer to the head of the list of endpoint
         * descriptors queued for the current interval.
         */

        uBufferAddr = pInterruptTable[uPollingInterval];

        /* Get the bus address in CPU endian */

        uBufferAddr = USB_OHCD_SWAP_DATA(uHostControllerIndex,
                            uBufferAddr);

        pTempEndpointDescriptor = (pUSB_OHCD_PIPE)
            usbOhciPhyToVirt(pHCDData,
                             uBufferAddr,
                             USB_OHCI_MEM_MAP_ED);

        /*
         * Search the list of endpoints queued for the current polling
         * interval and release the bandwidth for the endpoint (if
         * the endpoint corresponds to the specified USB address.
         */
        while (NULL != pTempEndpointDescriptor)
            {
            /*
             * Check whether the endpoint corresponds to the specified
             * device address. If not, traverse to the next endpoint
             * descriptor in the list.
             */
            if (uDeviceAddress !=
                OHCI_GET_DEVICE_ADDRESS_FROM_ED(uHostControllerIndex,
                    pTempEndpointDescriptor->uControlInformation))
                {
                /* Traverse to the next endpoint descriptor in the list */
                pTempEndpointDescriptor =
                    pTempEndpointDescriptor->pHCDNextPipe;

                /*
                 * The logic after this if () statement apply only if
                 * the endpoint corresponds to the specified device
                 * address. Hence, start from the beginning of the loop
                 */
                continue;
                }

            /* Obtain the endpoint address */

            uEndpointAddress = (UINT8)
                OHCI_GET_ENDPOINT_NUMBER_FROM_ED(uHostControllerIndex,
                    pTempEndpointDescriptor->uControlInformation);

            /* Obtain the direction of the endpoint */

            uEndpointDirection = (UINT8)
                OHCI_GET_ENDPOINT_DIRECTION_FROM_ED(uHostControllerIndex,
                    pTempEndpointDescriptor->uControlInformation);

            /* Obtain the maximum packet size for the endpoint */

            uMaximumPacketSize = (UINT16)
                OHCI_GET_ENDPOINT_MAXIMUM_PACKET_SIZE_FROM_ED(
                    uHostControllerIndex,
                    pTempEndpointDescriptor->uControlInformation);

            /*
             * Obtain the transfer type of the endpoint. Based on the
             * transfer type, compute the bandwidth required for the
             * endpoint.
             *
             * NOTE: The transfer type of the endpoint can be obtained
             *       by determining whether the TDs associated with the
             *       endpoint descriptor correspond to an general /
             *       isochronous TD.
             */
            if (0 !=
                OHCI_IS_ISOCHRONOUS_ENDPOINT(uHostControllerIndex,
                    pTempEndpointDescriptor->uControlInformation))
                {
                /*
                 * Compute the bandwidth required for the interrupt
                 * endpoint.
                 */
                uBandwidthRequiredForEndpoint =
                    (UINT32)USB_OHCI_COMPUTE_BANDWIDTH_FOR_INTERRUPT_ENDPOINT(
                        uDeviceSpeed,
                        uEndpointDirection,
                        uMaximumPacketSize);
                }
            else
                {
                /*
                 * Compute the bandwidth required for the isochronous
                 * endpoint.
                 */
                uBandwidthRequiredForEndpoint =
                    (UINT32)USB_OHCI_COMPUTE_BANDWIDTH_FOR_ISOCHRONOUS_ENDPOINT(
                        uDeviceSpeed,
                        uEndpointDirection,
                        uMaximumPacketSize);
                }

            /* Release the bandwidth for the endpoint */

            puAvailableBandwidth[uPollingInterval] +=
                uBandwidthRequiredForEndpoint;

            /* Traverse to the next endpoint descriptor in the list */
            pTempEndpointDescriptor =
                pTempEndpointDescriptor->pHCDNextPipe;

            } /* End of while (NULL != pTempEndpointDescriptor) */

        } /* End of for (uPollingInterval = 0; ... ) */

    return;
    } /* End of function usbOhciReleaseBandwidthForCurrentConfiguration() */

/***************************************************************************
*
* usbOhciAllocateBandwidthForNewConfiguration - Allocate BW for configuration
*
* This function is used to allocate bandwidth for the new configuration.
*
* PARAMETERS: <uHostControllerIndex (IN)> - OHCI Host Controller index.
*
* <uDeviceAddress (IN)> - Specifies the address of the device.
*
* <uDeviceSpeed (IN)> - Specifies the speed of the device.
*
* <pConfigurationDescriptor (IN)> - Pointer to the new  descriptor.
*
* <puAvailableBandwidth (IN/OUT)> - Pointer to the array holding the
* bandwidth available.
*
* RETURNS: USBHST_SUCCESS on success
* USBHST_INSUFFICIENT_BANDWIDTH if the bandwidth for  new interface cannot
* be supported
*
* ERRNO:
*   None.
*/

LOCAL USBHST_STATUS usbOhciAllocateBandwidthForNewConfiguration
    (
    UINT8                               uHostControllerIndex,
    UINT8                               uDeviceAddress,
    UINT8                               uDeviceSpeed,
    PUSB_OHCI_CONFIGURATION_DESCRIPTOR  pConfigurationDescriptor,
    PUINT32                             puAvailableBandwidth
    )
    {
    /* To hold the status of supporting the new configuration */

    USBHST_STATUS               nStatus = USBHST_SUCCESS;

    /* To hold the total length of the configuration descriptor */

    UINT16                      uConfigurationDescriptorTotalLength = 0;

    /* To hold the length of the configuration descriptor parsed */

    UINT16                      uParsedConfigurationDescriptorLength = 0;

    /*
     * To hold the current interface number. This information is used to
     * identify the alternate setting zero for the interface.
     */

    UINT8                           uCurrentInterfaceNumber = 0;

    /* To hold the number of endpoints in the interface */

    UINT8                           uNumberOfEndpoints = 0;

    /* To hold the pointer to the general USB descriptor */

    PUSB_OHCI_GENERIC_DESCRIPTOR    pGeneralDescriptor = NULL;

    /* To hold the pointer to the USB interface descriptor */

    PUSB_OHCI_INTERFACE_DESCRIPTOR  pInterfaceDescriptor = NULL;

    /* Obtain the total length of the configuration descriptor */

    uConfigurationDescriptorTotalLength =
        OS_UINT16_LE_TO_CPU(pConfigurationDescriptor->wTotalLength);

    /* Initialize the pointer to the generic USB descriptor */

    pGeneralDescriptor =
        (PUSB_OHCI_GENERIC_DESCRIPTOR)pConfigurationDescriptor;

    /*
     * Parse the configuration descriptor and verify whether the
     * bandwidth requirements of the default alternate setting can be
     * satisfied.
     */

    while (uParsedConfigurationDescriptorLength <
           uConfigurationDescriptorTotalLength)
        {
        /*
         * Check whether the descriptor corresponds to an interface
         * descriptor.
         */
        if (USBHST_INTERFACE_DESC != pGeneralDescriptor->bDescriptorType)
            {
            /* Update the parsed configuration descriptor length */
            uParsedConfigurationDescriptorLength =
                (UINT16)(uParsedConfigurationDescriptorLength + pGeneralDescriptor->bLength);

            /* Move to the next descriptor */
            pGeneralDescriptor =
                (PUSB_OHCI_GENERIC_DESCRIPTOR)
                ((PUCHAR)pGeneralDescriptor + pGeneralDescriptor->bLength);

            /*
             * Since a non-interface descriptor was found, start the check
             * from the beginning of the loop.
             */
            continue;
            }

        /* Initialize the pointer to the interface descriptor */
        pInterfaceDescriptor = (PUSB_OHCI_INTERFACE_DESCRIPTOR)pGeneralDescriptor;

        /* Obtain the current interface number */
        uCurrentInterfaceNumber = pInterfaceDescriptor->bInterfaceNumber;

        /*
         * Check whether the interface descriptor corresponds to an alternate
         * interface zero of the current interface. If not, locate the
         * alternate setting zero for the interface.
         */
        while ((uCurrentInterfaceNumber ==
                pInterfaceDescriptor->bInterfaceNumber) &&
               (0 != pInterfaceDescriptor->bAlternateSetting) &&
               (uParsedConfigurationDescriptorLength <
                uConfigurationDescriptorTotalLength))
            {
            /* Obtain the number of endpoints in the current interface */
            uNumberOfEndpoints = pInterfaceDescriptor->bNumEndpoints;

            /*
             * Initialize the pointer to the descriptor following the
             * interface descriptor.
             */
            pGeneralDescriptor =
                (PUSB_OHCI_GENERIC_DESCRIPTOR)
                ((PUCHAR)pInterfaceDescriptor + pInterfaceDescriptor->bLength);

            /* Update the parsed configuration descriptor length */
            uParsedConfigurationDescriptorLength =
                (UINT16)(uParsedConfigurationDescriptorLength + pInterfaceDescriptor->bLength);

            /*
             * Skip the descriptors for all the endpoints in the current
             * interface.
             */
            while ((0 < uNumberOfEndpoints) &&
                   (uParsedConfigurationDescriptorLength <
                    uConfigurationDescriptorTotalLength))
                {
                /*
                 * Check whether the descriptor corresponds to an endpoint
                 * descriptor. If not, move the next descriptor.
                 */
                if (USBHST_ENDPOINT_DESC !=
                    pGeneralDescriptor->bDescriptorType)
                    {
                    /* Update the parsed configuration descriptor length */
                    uParsedConfigurationDescriptorLength =
                        (UINT16)(uParsedConfigurationDescriptorLength
                            + pGeneralDescriptor->bLength);

                    /* Move to the next descriptor */
                    pGeneralDescriptor =
                        (PUSB_OHCI_GENERIC_DESCRIPTOR)
                        ((PUCHAR)pGeneralDescriptor +
                            pGeneralDescriptor->bLength);
                    /*
                     * Since a non-endpoint descriptor was found, start the check
                     * from the beginning of the loop.
                     */
                    continue;
                    }

                /* Decrement the number of endpoints information */
                uNumberOfEndpoints--;

                /* Update the parsed configuration descriptor length */
                uParsedConfigurationDescriptorLength =
                    (UINT16)(uParsedConfigurationDescriptorLength +
                        pGeneralDescriptor->bLength);

                /* Move to the next descriptor */
                pGeneralDescriptor =
                    (PUSB_OHCI_GENERIC_DESCRIPTOR)
                    ((PUCHAR)pGeneralDescriptor +
                        pGeneralDescriptor->bLength);

                } /* End of while (0 < uNumberOfEndpoints) */

            } /* End of while ((uCurrentInterfaceNumber == ... ) */

        /*
         * Check whether the interface number and alternate interface number
         * are valid.
         */
        if ((uCurrentInterfaceNumber !=
             pInterfaceDescriptor->bInterfaceNumber) ||
            (0 != pInterfaceDescriptor->bAlternateSetting) ||
            (uParsedConfigurationDescriptorLength >=
             uConfigurationDescriptorTotalLength))
            {
            /*
             * Invalid alternate setting. Update the status and return
             * from the function.
             */
            nStatus = USBHST_INSUFFICIENT_BANDWIDTH;

            /* Break from the function */
            break;
            }

        /* Initialize the pointer to the interface descriptor */
        pInterfaceDescriptor =
            (PUSB_OHCI_INTERFACE_DESCRIPTOR)pGeneralDescriptor;

        /* Call the function to allocate bandwidth for the interface */
        nStatus =
            usbOhciAllocateBandwidthForNewInterface(uHostControllerIndex,
                                                    uDeviceAddress,
                                                    uDeviceSpeed,
                                                    pInterfaceDescriptor,
                                                    puAvailableBandwidth);

        /*
         * Check whether the bandwidth of the interface was allocated
         * successfully.
         */
        if (USBHST_SUCCESS != nStatus)
            {
            /*
             * The new configuration cannot be supported. Return from the
             * function.
             */
            break;
            }

        /* Skip all the other alternate settings for the current interface */
        while ((uCurrentInterfaceNumber ==
                pInterfaceDescriptor->bInterfaceNumber) &&
               (uParsedConfigurationDescriptorLength <
                uConfigurationDescriptorTotalLength))
            {
            /* Obtain the number of endpoints in the current interface */
            uNumberOfEndpoints = pInterfaceDescriptor->bNumEndpoints;

            /*
             * Initialize the pointer to the descriptor following the
             * interface descriptor.
             */
            pGeneralDescriptor =
                (PUSB_OHCI_GENERIC_DESCRIPTOR)
                ((PUCHAR)pInterfaceDescriptor + pInterfaceDescriptor->bLength);

            /* Update the parsed configuration descriptor length */
            uParsedConfigurationDescriptorLength =
                (UINT16)(uParsedConfigurationDescriptorLength
                    + pInterfaceDescriptor->bLength);

            /*
             * Skip the descriptors for all the endpoints in the current
             * interface.
             */
            while ((0 < uNumberOfEndpoints) &&
                   (uParsedConfigurationDescriptorLength <
                    uConfigurationDescriptorTotalLength))
                {
                /*
                 * Check whether the descriptor corresponds to an endpoint
                 * descriptor. If not, move the next descriptor.
                 */
                if (USBHST_ENDPOINT_DESC !=
                    pGeneralDescriptor->bDescriptorType)
                    {

                    /* Update the parsed configuration descriptor length */
                    uParsedConfigurationDescriptorLength =
                        (UINT16)(uParsedConfigurationDescriptorLength
                            + pGeneralDescriptor->bLength);

                    /* Move to the next descriptor */
                    pGeneralDescriptor =
                        (PUSB_OHCI_GENERIC_DESCRIPTOR)
                        ((PUCHAR)pGeneralDescriptor +
                            pGeneralDescriptor->bLength);

                    /*
                     * Since a non-endpoint descriptor was found, start the check
                     * from the beginning of the loop.
                     */
                    continue;
                    }

                /* Decrement the number of endpoints information */
                uNumberOfEndpoints--;

                /* Update the parsed configuration descriptor length */
                uParsedConfigurationDescriptorLength =
                    (UINT16)(uParsedConfigurationDescriptorLength
                        + pGeneralDescriptor->bLength);

                /* Move to the next descriptor */
                pGeneralDescriptor =
                    (PUSB_OHCI_GENERIC_DESCRIPTOR)
                    ((PUCHAR)pGeneralDescriptor +
                        pGeneralDescriptor->bLength);

                } /* End of while (0 < uNumberOfEndpoints) */

            /*
             * If there are valid descriptors, move to the next interface
             * descriptor.
             */
            while ((uParsedConfigurationDescriptorLength <
                    uConfigurationDescriptorTotalLength) &&
                   (USBHST_INTERFACE_DESC !=
                    pGeneralDescriptor->bDescriptorType))
                {
                /* Update the parsed configuration descriptor length */
                uParsedConfigurationDescriptorLength =
                    (UINT16)(uParsedConfigurationDescriptorLength
                        + pGeneralDescriptor->bLength);

                /* Move to the next descriptor */
                pGeneralDescriptor =
                    (PUSB_OHCI_GENERIC_DESCRIPTOR)
                    ((PUCHAR)pGeneralDescriptor +
                        pGeneralDescriptor->bLength);

                } /* End of while ((uParsedConfigurationDescriptorLength < .. ) */

            /* Initialize the pointer to the interface descriptor */
            pInterfaceDescriptor =
                (PUSB_OHCI_INTERFACE_DESCRIPTOR)pGeneralDescriptor;

            } /* End of while ((uCurrentInterfaceNumber == ... ) */

        } /* End of while (uParsedConfigurationDescriptorLength < ... ) */

    /* Return the status of supporting the new configuration */
    return nStatus;

    } /* End of function usbOhciAllocateBandwidthForNewConfiguration() */

/***************************************************************************
*
* usbOhciReleaseBandwidthForCurrentInterface - Release BW for curent interface
*
* This function is used to release bandwidth for the current interface.
*
* PARAMETERS: <uHostControllerIndex (IN)> - OHCI Host Controller index.
*
* <uDeviceAddress (IN)> - Specifies the address of the device.
*
* <uDeviceSpeed (IN)> - Specifies the speed of the device.
*
* <pInterfaceDescriptor (IN)> - Pointer to the current interface descriptor.
*
* <puAvailableBandwidth (IN/OUT)> - Pointer to the array holding the
* bandwidth available.
*
* RETURNS: N/A
*
* ERRNO:
*   None.
*/

LOCAL VOID usbOhciReleaseBandwidthForCurrentInterface
    (
    UINT8                           uHostControllerIndex,
    UINT8                           uDeviceAddress,
    UINT8                           uDeviceSpeed,
    PUSB_OHCI_INTERFACE_DESCRIPTOR  pInterfaceDescriptor,
    PUINT32                         puAvailableBandwidth
    )
    {
    /* To hold the polling interval */

    UINT32                          uPollingInterval = 0;

    /* To hold the pointer to the general descriptor */

    PUSB_OHCI_GENERIC_DESCRIPTOR    pGeneralDescriptor = NULL;

    /* To hold the pointer to the endpoint descriptor */

    PUSB_ENDPOINT_DESCRIPTOR        pHCDPipe = NULL;

    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA                  pHCDData = NULL;

    /*
     * To hold the pointer to the current endpoint desriptor node while
     * traversing the endpoint descriptor list.
     */

    pUSB_OHCD_PIPE   pTempEndpointDescriptor = NULL;

    /* To hold the number of endpoints in the interface */

    UINT8                           uNumberOfEndpoints = 0;

    /* To hold the endpoint address */

    UINT8                           uEndpointAddress = 0;

    /* To hold the direction of the endpoint */

    UINT8                           uEndpointDirection = OHCI_IN_ENDPOINT;

    /* To hold the bandwidth required by the endpoint */

    UINT32                          uBandwidthRequiredForEndpoint = 0;

    PUINT32                         pInterruptTable = NULL;

    UINT32                          uBufferAddr = 0x0;

    /* Obtain the pointer to the OHCI host controller information */

    pHCDData = &g_OHCDData[uHostControllerIndex];

    /* Obtain the pointer to the periodic list */
    /*
     * The following sentence want to get the address of
     * pHCDData->pHcca->uHccaInterruptTable
     * But in DIAB compiler, we will get warning as
     * "dangerous to take address of member of packed or swapped structure"
     * The "uHccaInterruptTable" must be the first of the structure
     * So we will get the HCCA address to instead.
     */

    pInterruptTable = (PUINT32)(pHCDData->pHcca);

    /* Obtain the number of endpoints in the current interface */

    uNumberOfEndpoints = pInterfaceDescriptor->bNumEndpoints;

    /*
     * Initialize the pointer to the descriptor following the interface
     * descriptor.
     */

    pGeneralDescriptor =
        (PUSB_OHCI_GENERIC_DESCRIPTOR)
        ((PUCHAR)pInterfaceDescriptor + pInterfaceDescriptor->bLength);

    /*
     * Release the bandwidth for all the periodic endpoints in the
     * current interface.
     */

    while (0 < uNumberOfEndpoints)
        {
        /*
         * Check whether the descriptor corresponds to an endpoint
         * descriptor. If not, move the next descriptor.
         */
        if (USBHST_ENDPOINT_DESC != pGeneralDescriptor->bDescriptorType)
            {
            /* Move to the next descriptor */
            pGeneralDescriptor =
                (PUSB_OHCI_GENERIC_DESCRIPTOR)
                ((PUCHAR)pGeneralDescriptor + pGeneralDescriptor->bLength);

            /*
             * Since a non-endpoint descriptor was found, start the check
             * from the beginning of the loop.
             */
            continue;
            }

        /* Initialize the pointer to the endpoint descriptor */
        pHCDPipe =
            (PUSB_ENDPOINT_DESCRIPTOR)pGeneralDescriptor;

        /* Obtain the direction of the endpoint */

        if (USB_IN_ENDPOINT ==
            (pHCDPipe->bEndpointAddress & USB_IN_ENDPOINT))
            {
            /* IN endpoint */
            uEndpointDirection = OHCI_IN_ENDPOINT;
            }
        else
            {
            /* OUT endpoint */
            uEndpointDirection = OHCI_OUT_ENDPOINT;
            }

        /* Check whether the endpoint corresponds to a periodic endpoint */

        if (USBHST_ISOCHRONOUS_TRANSFER ==
            (pHCDPipe->bmAttributes & 0x03))
            {
            /*
             * Compute the bandwidth required for the isochronous
             * endponint
             */
            uBandwidthRequiredForEndpoint =
                (UINT32)USB_OHCI_COMPUTE_BANDWIDTH_FOR_ISOCHRONOUS_ENDPOINT(
                    uDeviceSpeed,
                    uEndpointDirection,
                    pHCDPipe->wMaxPacketSize);
            }
        else if (USBHST_INTERRUPT_TRANSFER ==
                 (pHCDPipe->bmAttributes & 0x03))
            {
            /*
             * Compute the bandwidth required for the interrupt
             * endponint
             */
            uBandwidthRequiredForEndpoint =
                (UINT32)USB_OHCI_COMPUTE_BANDWIDTH_FOR_INTERRUPT_ENDPOINT(
                    uDeviceSpeed,
                    uEndpointDirection,
                    pHCDPipe->wMaxPacketSize);
            }
        else
            {
            /*
             * Bandwidth allocation is applicable only for periodic endpoints.
             * Hence, traverse to the next endpoint descriptor and start from
             * the begining of the loop.
             */

            /* Decrement the number of endpoints information */
            uNumberOfEndpoints--;

            /* Move to the next descriptor */
            pGeneralDescriptor =
                (PUSB_OHCI_GENERIC_DESCRIPTOR)
                ((PUCHAR)pGeneralDescriptor + pGeneralDescriptor->bLength);

            /* Start from the beginning of the loop */
            continue;
            }

        /*
         * Locate the endpoint in the periodic list and release the
         * bandwidth allocated for the endpoint.
         */

        for (uPollingInterval = 0;
             uPollingInterval < USB_OHCI_MAXIMUM_POLLING_INTERVAL;
             uPollingInterval++)
            {
            /*
             * Obtain the pointer to the head of the list of endpoint
             * descriptors queued for the current interval.
             */

            uBufferAddr = pInterruptTable[uPollingInterval];

            /* Get the bus address in CPU endian */

            uBufferAddr = USB_OHCD_SWAP_DATA(uHostControllerIndex,
                                uBufferAddr);

            /*
             * Search the list of endpoints queued for the current polling
             * interval and release the bandwidth for the endpoint (if
             * the endpoint corresponds to the specified USB address.
             */

            while (0 != uBufferAddr)
                {
                pTempEndpointDescriptor = (pUSB_OHCD_PIPE)
                    usbOhciPhyToVirt(pHCDData,
                                     uBufferAddr,
                                     USB_OHCI_MEM_MAP_ED);

                if (pTempEndpointDescriptor == NULL)
                    {
                    USB_OHCD_ERR ("Invalid virtual address \n", 1, 2, 3, 4, 5, 6);
                    break;
                    }

                /*
                 * Check whether the endpoint corresponds to the specified
                 * device address. If not, traverse to the next endpoint
                 * descriptor in the list.
                 */
                if (uDeviceAddress !=
                    OHCI_GET_DEVICE_ADDRESS_FROM_ED(uHostControllerIndex,
                        pTempEndpointDescriptor->uControlInformation))
                    {
                    /* Traverse to the next endpoint descriptor in the list */

                    uBufferAddr =
                        pTempEndpointDescriptor->uNextEDPointer;

                    /* Get the bus address in CPU endian */

                    uBufferAddr = USB_OHCD_SWAP_DATA(uHostControllerIndex,
                                        uBufferAddr);

                    /*
                     * The logic after this if () statement apply only if
                     * the endpoint corresponds to the specified device
                     * address. Hence, start from the beginning of the loop
                     */

                    continue;
                    }

                /* Obtain the endpoint address */

                uEndpointAddress =
                    (UINT8)OHCI_GET_ENDPOINT_NUMBER_FROM_ED(uHostControllerIndex,
                        pTempEndpointDescriptor->uControlInformation);

                /* Obtain the direction of the endpoint */
                uEndpointDirection =
                    (UINT8)OHCI_GET_ENDPOINT_DIRECTION_FROM_ED(uHostControllerIndex,
                        pTempEndpointDescriptor->uControlInformation);

                /*
                 * Update the endpoint address based on the endpoint
                 * direction
                 */
                if (OHCI_IN_ENDPOINT == uEndpointDirection)
                    {
                    /* Update the endpoint address */
                    uEndpointAddress |= USB_IN_ENDPOINT;
                    }

                /*
                 * Check whether the endpoint address and the endpoint
                 * direction corresponds to the current endpoint descriptor
                 * specified in the USB interface descriptor.
                 */
                if (uEndpointAddress != pHCDPipe->bEndpointAddress)
                    {
                    /* Traverse to the next endpoint descriptor in the list */
                    uBufferAddr =
                        pTempEndpointDescriptor->uNextEDPointer;

                    /* Get the bus address in CPU endian */

                    uBufferAddr = USB_OHCD_SWAP_DATA(uHostControllerIndex,
                                        uBufferAddr);

                    /*
                     * The logic after this if () statement apply only if
                     * the endpoint corresponds to the specified device
                     * address. Hence, start from the beginning of the loop
                     */
                    continue;
                    }

                /* Release the bandwidth for the endpoint */
                puAvailableBandwidth[uPollingInterval] +=
                    uBandwidthRequiredForEndpoint;

                /* Traverse to the next endpoint descriptor in the list */

                uBufferAddr =
                    pTempEndpointDescriptor->uNextEDPointer;

                /* Get the bus address in CPU endian */

                uBufferAddr = USB_OHCD_SWAP_DATA(uHostControllerIndex,
                                    uBufferAddr);

                } /* End of while (NULL != pTempEndpointDescriptor) */

            } /* End of for (uPollingInterval = 0; ... ) */

        /* Decrement the number of endpoints information */
        uNumberOfEndpoints--;

        /* Move to the next descriptor */
        pGeneralDescriptor =
            (PUSB_OHCI_GENERIC_DESCRIPTOR)
            ((PUCHAR)pGeneralDescriptor + pGeneralDescriptor->bLength);

        } /* End of while (0 < uNumberOfEndpoints) */

    return;
    } /* End of function usbOhciReleaseBandwidthForCurrentInterface() */

/***************************************************************************
*
* usbOhciAllocateBandwidthForNewInterface - allocate BW for new interface
*
* This function is used to allocate bandwidth for the new interface.
*
* PARAMETERS: <uHostControllerIndex (IN)> - OHCI Host Controller index.
*
* <uDeviceAddress (IN)> - Specifies the address of the device.
*
* <uDeviceSpeed (IN)> - Specifies the speed of the device.
*
* <pInterfaceDescriptor (IN)> - Pointer to the new interface descriptor.
*
* <puAvailableBandwidth (IN/OUT)> - Pointer to the array holding the
* bandwidth available.
*
* RETURNS: USBHST_SUCCESS on success
* USBHST_INSUFFICIENT_BANDWIDTH if the bandwidth for  new interface cannot
* be supported
*
* ERRNO:
*   None.
*/

LOCAL USBHST_STATUS usbOhciAllocateBandwidthForNewInterface
    (
    UINT8                           uHostControllerIndex,
    UINT8                           uDeviceAddress,
    UINT8                           uDeviceSpeed,
    PUSB_OHCI_INTERFACE_DESCRIPTOR  pInterfaceDescriptor,
    PUINT32                         puAvailableBandwidth
    )
    {
    /* To hold the polling interval */

    UINT32                          uPollingInterval = 0;

    /* To hold the pointer to the general descriptor */

    PUSB_OHCI_GENERIC_DESCRIPTOR    pGeneralDescriptor = NULL;

    /* To hold the pointer to the endpoint descriptor */

    PUSB_ENDPOINT_DESCRIPTOR        pHCDPipe = NULL;

    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA                  pHCDData = NULL;

    /* To hold the number of endpoints in the interface */

    UINT8                           uNumberOfEndpoints = 0;

    /* To hold the direction of the endpoint */

    UINT8                           uEndpointDirection = OHCI_IN_ENDPOINT;

    /* To hold the polling interval for the endpoint */

    UINT8                           uEndpointPollingInterval = 0;

    /* To hold the bandwidth required by the endpoint */

    UINT32                          uBandwidthRequiredForEndpoint = 0;


    /* Obtain the pointer to the OHCI host controller information */

    pHCDData = &g_OHCDData[uHostControllerIndex];

    /* Obtain the number of endpoints in the current interface */

    uNumberOfEndpoints = pInterfaceDescriptor->bNumEndpoints;

    /*
     * Initialize the pointer to the descriptor following the interface
     * descriptor.
     */

    pGeneralDescriptor =
        (PUSB_OHCI_GENERIC_DESCRIPTOR)
        ((PUCHAR)pInterfaceDescriptor + pInterfaceDescriptor->bLength);

    /*
     * Release the bandwidth for all the periodic endpoints in the
     * current interface.
     */

    while (0 < uNumberOfEndpoints)
        {
        /*
         * Check whether the descriptor corresponds to an endpoint
         * descriptor. If not, move the next descriptor.
         */
        if (USBHST_ENDPOINT_DESC != pGeneralDescriptor->bDescriptorType)
            {
            /* Move to the next descriptor */
            pGeneralDescriptor =
                (PUSB_OHCI_GENERIC_DESCRIPTOR)
                ((PUCHAR)pGeneralDescriptor + pGeneralDescriptor->bLength);

            /*
             * Since a non-endpoint descriptor was found, start the check
             * from the beginning of the loop.
             */
            continue;
            }

        /* Initialize the pointer to the endpoint descriptor */

        pHCDPipe = (PUSB_ENDPOINT_DESCRIPTOR)pGeneralDescriptor;

        /* Obtain the direction of the endpoint */

        if (USB_IN_ENDPOINT ==
            (pHCDPipe->bEndpointAddress & USB_IN_ENDPOINT))
            {
            /* IN endpoint */
            uEndpointDirection = OHCI_IN_ENDPOINT;
            }
        else
            {
            /* OUT endpoint */
            uEndpointDirection = OHCI_OUT_ENDPOINT;
            }

        /* Check whether the endpoint corresponds to a periodic endpoint */

        if (USBHST_ISOCHRONOUS_TRANSFER ==
            (pHCDPipe->bmAttributes & 0x03))
            {
            /*
             * Compute the bandwidth required for the isochronous
             * endponint
             */
            uBandwidthRequiredForEndpoint =
                (UINT32)USB_OHCI_COMPUTE_BANDWIDTH_FOR_ISOCHRONOUS_ENDPOINT(
                    uDeviceSpeed,
                    uEndpointDirection,
                    pHCDPipe->wMaxPacketSize);
            }
        else if (USBHST_INTERRUPT_TRANSFER ==
                 (pHCDPipe->bmAttributes & 0x03))
            {
            /*
             * Compute the bandwidth required for the interrupt
             * endponint
             */
            uBandwidthRequiredForEndpoint =
                (UINT32)USB_OHCI_COMPUTE_BANDWIDTH_FOR_INTERRUPT_ENDPOINT(
                    uDeviceSpeed,
                    uEndpointDirection,
                    pHCDPipe->wMaxPacketSize);
            }
        else
            {
            /*
             * Bandwidth allocation is applicable only for periodic endpoints.
             * Hence, traverse to the next endpoint descriptor and start from
             * the begining of the loop.
             */

            /* Decrement the number of endpoints information */

            uNumberOfEndpoints--;

            /* Move to the next descriptor */

            pGeneralDescriptor =
                (PUSB_OHCI_GENERIC_DESCRIPTOR)
                ((PUCHAR)pGeneralDescriptor + pGeneralDescriptor->bLength);

            /* Start from the beginning of the loop */
            continue;
            }

        /*
         * Based on the endpoint transfer type, obtain the polling interval
         * for the endpoint. For isochronous endpoints, the polling interval
         * is 1 ms.
         */

        if (USBHST_ISOCHRONOUS_TRANSFER ==
            (pHCDPipe->bmAttributes & 0x03))
            {
            /* Initialize the polling interval for the endpoint */
            uEndpointPollingInterval =
                USB_OHCI_ISOCHRONOUS_ENDPOINT_POLLING_INTERVAL;
            }
        else
            {
            /* Obtain the polling interval for the endpoint */
            uEndpointPollingInterval = pHCDPipe->bInterval;
            }

        /*
         * NOTE: OHCI supports only upto 32 ms of polling intervals. Hence,
         *       round off the polling interval computed.
         */
        if (USB_OHCI_MAXIMUM_POLLING_INTERVAL < uEndpointPollingInterval)
            {
            /* Round off the polling interval */
            uEndpointPollingInterval = USB_OHCI_MAXIMUM_POLLING_INTERVAL;
            }

        /*
         * NOTE: If the polling interval for the endpoint is 1 ms, the
         *       bandwidth should be allocated in all the frames.
         *
         *       If the polling interval for the endpoint is 4 ms, the
         *       bandwidth should be allocated in all the frames of 4 ms
         *       duration. For ex, 1, 5, 9, ... etc if we start from frame 1.
         *       or 2, 6, 10, ... etc if we start from frame 2.
         */

        /* Locate the frame on which the endpoint can be allocated bandwidth */

        for (uPollingInterval = 0;
             uPollingInterval < uEndpointPollingInterval;
             uPollingInterval++)
            {
            /*
             * Check whether the endpoint can be supported in the current
             * frame.
             */
            if (puAvailableBandwidth[uPollingInterval] >=
                uBandwidthRequiredForEndpoint)
                {
                /* The endpoint can be supported in the current frame */
                break;
                }
            }

        /* Check whether the endpoint can be supported */

        if (uPollingInterval == uEndpointPollingInterval)
            {
            /* The endpoint cannot be supported */
            return USBHST_INSUFFICIENT_BANDWIDTH;
            }

        /*
         * Check whether the endpoint can be supported in all the frames
         * of the required bandwidth. Also update the available bandwidth
         * for the corresponding frames.
         */

        for (;
             uPollingInterval < USB_OHCI_MAXIMUM_POLLING_INTERVAL;
             uPollingInterval += uEndpointPollingInterval)
            {
            /*
             * Check whether the endpoint can be supported in the current
             * frame
             */
            if (puAvailableBandwidth[uPollingInterval] <
                uBandwidthRequiredForEndpoint)
                {
                /* The endpoint cannot be supported */
                return USBHST_INSUFFICIENT_BANDWIDTH;
                }

            /* Update the bandwidth available for the frame */
            puAvailableBandwidth[uPollingInterval] -=
                uBandwidthRequiredForEndpoint;

            } /* End of for (uPollingInterval = 0; ... ) */

        /* Decrement the number of endpoints information */
        uNumberOfEndpoints--;

        /* Move to the next descriptor */
        pGeneralDescriptor =
            (PUSB_OHCI_GENERIC_DESCRIPTOR)
            ((PUCHAR)pGeneralDescriptor + pGeneralDescriptor->bLength);

        } /* End of while (0 < uNumberOfEndpoints) */

    /* Bandwidth for the new interface can be supported */
    return USBHST_SUCCESS;

    } /* End of function usbOhciAllocateBandwidthForNewInterface() */

/* End of File usbOhciTransferManagement.c */

