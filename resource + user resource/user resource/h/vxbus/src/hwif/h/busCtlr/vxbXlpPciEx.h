/* vxbXlpPciEx.h - XLP PCIEX Bridge head file for NetLogic XLP family */

/*
 * Copyright (c) 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */
 
/* 
modification history
--------------------
01a,11apr11,x_f  created
*/

/*
DESCRIPTION

This file supplies common structures and defines for XLP IO management and
XLP PCIEX Bridge.
*/

#ifndef __INCvxbXlpPciExh
#define __INCvxbXlpPciExh

/* typedefs */

typedef VOID (*XLP_PCIEX_INT_CONNECT_FUNC)(VXB_DEVICE_ID, VXB_DEVICE_ID, VOIDFUNCPTR, int);
typedef VOID (*XLP_PCIEX_INT_DISCONN_FUNC)(VXB_DEVICE_ID, VXB_DEVICE_ID, VOIDFUNCPTR, int);
typedef STATUS (*XLP_PCIEX_INT_ENABLE_FUNC)(VXB_DEVICE_ID, VXB_DEVICE_ID);
typedef STATUS (*XLP_PCIEX_INT_DISABLE_FUNC)(VXB_DEVICE_ID, VXB_DEVICE_ID);

typedef struct vxbXlpPciExFunc
    {
    XLP_PCIEX_INT_CONNECT_FUNC    xlpPciExIntConnect;
    XLP_PCIEX_INT_DISCONN_FUNC    xlpPciExIntDisconnect;
    XLP_PCIEX_INT_ENABLE_FUNC     xlpPciExIntEnable;
    XLP_PCIEX_INT_DISABLE_FUNC    xlpPciExIntDisable;
    } VXB_XLP_PCIEX_FUNC;

typedef VXB_XLP_PCIEX_FUNC * (* xlpPciExCtlrGetPrototype) (void);

#endif /* __INCvxbXlpPciExh */
