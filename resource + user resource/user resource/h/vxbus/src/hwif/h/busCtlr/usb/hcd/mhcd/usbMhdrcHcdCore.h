/* usbMhdrcTransferManagement.h - MHCI host controller transgfer management */

/*
 * Copyright (c) 2009 - 2010 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01c,16aug10,w_x  VxWorks 64 bit audit and warning removal
01b,05nov09,s_z  Rewrite according the structure redesign
01a,13jul09,j_x  initial version
*/

/*
DESCRIPTION

This file contains the function declarations for transfer management.
*/

#ifndef __INCusbMhdrcTransferManagementh
#define __INCusbMhdrcTransferManagementh

#ifdef __cplusplus
extern "C" {
#endif

/* defines */

#define USB_MHCI_EP_0                  0x00
#define USB_MHCI_EP_1                  0x01
#define USB_MHCI_EP_2                  0x02
#define USB_MHCI_EP_3                  0x03
#define USB_MHCI_EP_4                  0x04
#define USB_MHCI_EP_5                  0x05
#define USB_MHCI_EP_6                  0x06
#define USB_MHCI_EP_7                  0x07
#define USB_MHCI_EP_8                  0x08
#define USB_MHCI_EP_9                  0x09
#define USB_MHCI_EP_A                  0x0A
#define USB_MHCI_EP_B                  0x0B
#define USB_MHCI_EP_C                  0x0C
#define USB_MHCI_EP_D                  0x0D
#define USB_MHCI_EP_E                  0x0E
#define USB_MHCI_EP_F                  0x0F


#define USB_MHCI_MAX_PACKET_SIZE_64    0x40
#define USB_MHCI_MAX_PACKET_SIZE_128   0x80
#define USB_MHCI_MAX_PACKET_SIZE_256   0x100
#define USB_MHCI_MAX_PACKET_SIZE_512   0x200

/* externals */

IMPORT USBHST_STATUS usbMhdrcHcdGetFrameNumber
    (
    UINT8    uBusIndex,
    UINT16 * puFrameNumber
    );

IMPORT USBHST_STATUS usbMhdrcHcdSetBitRate
    (
    UINT8    uBusIndex,
    BOOL     bIncrement,
    UINT32 * puCurrentFrameWidth
    );

IMPORT USBHST_STATUS  usbMhdrcHcdIsBandwidthAvailable
    (
    UINT8   uBusIndex,
    UINT8   uDeviceAddress,
    UINT8   uDeviceSpeed,
    UCHAR * pCurrentDescriptor,
    UCHAR * pNewDescriptor
    );

IMPORT USBHST_STATUS usbMhdrcHcdModifyDefaultPipe
    (
    UINT8   uBusIndex,
    ULONG   uDefaultPipeHandle,
    UINT8   uDeviceSpeed,
    UINT8   uMaxPacketSize,
    UINT16  uHighSpeedHubInfo
    );

IMPORT USBHST_STATUS usbMhdrcHcdCreatePipe
    (
    UINT8    uBusIndex,
    UINT8    uDeviceAddress,
    UINT8    uDeviceSpeed,
    UCHAR *  pEndpointDescriptor,
    UINT16   uHighSpeedHubInfo,
    ULONG *  puPipeHandle
    );

IMPORT USBHST_STATUS usbMhdrcHcdDeletePipe
    (
    UINT8    uBusIndex,
    ULONG    uPipeHandle
    );

IMPORT USBHST_STATUS usbMhdrcHcdSubmitURB
    (
    UINT8        uBusIndex,
    ULONG        uPipeHandle,
    pUSBHST_URB  pURB
    );

IMPORT USBHST_STATUS usbMhdrcHcdCancelURB
    (
    UINT8       uBusIndex,
    ULONG       uPipeHandle,
    pUSBHST_URB pURB
    );

IMPORT USBHST_STATUS usbMhdrcHcdIsRequestPending
    (
    UINT8  uBusIndex,
    ULONG  uPipeHandle
    );

IMPORT USBHST_STATUS usbMhdrcHcdClearTTRequestComplete
    (
    UINT8         uRelativeBusIndex,
    void *        pContext,
    USBHST_STATUS nStatus
    );

IMPORT USBHST_STATUS   usbMhdrcHcdResetTTRequestComplete
    (
    UINT8         uRelativeBusIndex,
    void *        pContext,
    USBHST_STATUS nStatus
    );

#ifdef __cplusplus
}
#endif

#endif /* __INCusbMhdrcTransferManagementh */

