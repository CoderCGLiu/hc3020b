/* usbPlx228x.h - Definitions for PLX NetChip NET 228x target controller */

/*
 * Copyright (c) 2012 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
Modification history
--------------------
01b,23oct12,s_z  Add U1/U2 rejection workaround based Errata (WIND00382685)
01a,18may12,s_z  Based on usbNET2280.h.
*/

/*
DESCRIPTION

This file constants the definition related to the PLX NetChip NET228x 
USB device (target) controller.
*/

#ifndef __INCusbPlx228xh
#define __INCusbPlx228xh

#ifdef    __cplusplus
extern "C" {
#endif

/* defines */

/* Supported contoller with vendor/device ID */

#define USB_PLX_VENDOR_ID_17CC    0x17CC
#define USB_PLX_VENDOR_ID_10B5    0x10B5

#define USB_PLX_DEVICE_ID_2280    0x2280
#define USB_PLX_DEVICE_ID_2282    0x2282
#define USB_PLX_DEVICE_ID_2380    0x2380
#define USB_PLX_DEVICE_ID_3380    0x3380
#define USB_PLX_DEVICE_ID_3382    0x3382

#define USB_PLX_VERSION_NUM_AA    0xAA
#define USB_PLX_VERSION_NUM_AB    0xAB



/* From the data book, the max regBase number is 3 */

#define USB_PLX_MAX_REGBASE_COUNT 3

/* Endpoint Indices */

#define USB_PLX_ENDPT_0         0x0 /* Endpoint 0 */
#define USB_PLX_ENDPT_A         0x1 /* Endpoint A */
#define USB_PLX_ENDPT_B         0x2 /* Endpoint B */
#define USB_PLX_ENDPT_C         0x3 /* Endpoint C */
#define USB_PLX_ENDPT_D         0x4 /* Endpoint D */
#define USB_PLX_ENDPT_E         0x5 /* Endpoint E */
#define USB_PLX_ENDPT_F         0x6 /* Endpoint F */

/* The max available endpoints count */

#define USB_PLX_MAX_EP_COUNT    0x7

/* Dedicate endpoints */

#define USB_PLX_ENDPT_CFGOUT    0xA /* Endpoint CFG OUT */
#define USB_PLX_ENDPT_CFGIN     0xB /* Endpoint CFG IN */
#define USB_PLX_ENDPT_PCIOUT    0xC /* Endpoint PCI OUT */
#define USB_PLX_ENDPT_PCIIN     0xD /* Endpoint PCI IN */
#define USB_PLX_ENDPT_STATIN    0xF /* ENDpoint STATIN */

/* Main Control Registers */

#define USB_PLX_DEVINIT_REG     0x00 /* Device Initialization */
#define USB_PLX_EECTL_REG       0x04 /* EEPROM Control */
#define USB_PLX_EECLKFREQ_REG   0x08 /* EEPROM Clock Frequency */
#define USB_PLX_PCICTL_REG      0x0C /* PCI Control */
#define USB_PLX_PCIIRQENB0_REG  0x10 /* PCI Interrupt Request Enable 0*/
#define USB_PLX_PCIIRQENB1_REG  0x14 /* PCI Interrupt Request Enable 1*/
#define USB_PLX_CPUIRQENB0_REG  0x18 /* CPU Interrupt Request Enable 0*/
#define USB_PLX_CPUIRQENB1_REG  0x1C /* CPU Interrupt Request Enable 1*/
#define USB_PLX_USBIRQENB1_REG  0x24 /* USB Interrupt Request Enable 1*/
#define USB_PLX_IRQSTAT0_REG    0x28 /* Interrupt Request Status 0*/
#define USB_PLX_IRQSTAT1_REG    0x2C /* Interrupt Request Status 1*/
#define USB_PLX_IDXADDR_REG     0x30 /* Index Regsiter Address */
#define USB_PLX_IDXDATA_REG     0x34 /* Index Register Data */
#define USB_PLX_FIFOCTL_REG     0x38 /* FIFO Control */
#define USB_PLX_MEMADDR_REG     0x40 /* FIFO Memory Diagnostic addr*/
#define USB_PLX_MEMDATA0_REG    0x44 /* FIFO Memory Diagnostic Data0*/
#define USB_PLX_MEMDATA1_REG    0x48 /* FIFO Memory Diagnostic Data1*/
#define USB_PLX_GPIOCTL_REG     0x50 /* General Purpose Control Reg.*/
#define USB_PLX_GPIOSTAT_REG    0x54 /* General Purpose Status Reg */

/* USB Control Registers */

#define USB_PLX_STDRSP_REG      0x80 /* Standard Response Regsiter */
#define USB_PLX_PRODVENDID_REG  0x84 /* Product/Vendor ID */
#define USB_PLX_RELNUM_REG      0x88 /* Release Number */
#define USB_PLX_USBCTL_REG      0x8C /* USB Control */
#define USB_PLX_USBSTAT_REG     0x90 /* USB Status */
#define USB_PLX_XCVRDIAG_REG    0x94 /* Transceiver Mode Regsiter */
#define USB_PLX_SETUP0123_REG   0x98 /* Setup Bytes 0,1,2,3 */
#define USB_PLX_SETUP4567_REG   0x9C /* Setup Bytes 4,5,6,7 */
#define USB_PLX_OURADDR_REG     0xA4 /* Address Register */
#define USB_PLX_OURCONFIG_REG   0xA8 /* Configuration Register */

/* PCI Control Registers */

#define USB_PLX_PCIMSTCTL_REG   0x100 /* PCI Master Control */
#define USB_PLX_PCIMSTADDR_REG  0x104 /* PCI Master Address */
#define USB_PLX_PCIMSTDATA_REG  0x108 /* PCI Master Data */
#define USB_PLX_PCIMSTSTAT_REG  0x10C /* PCI Master Status */

/* DMA  Registers */

#define USB_PLX_DMACTL_OFFSET(x)    \
    (USB_PLX_DMA_REG_BASE(x) + 0x00)    /* DMA Control */
#define USB_PLX_DMASTAT_OFFSET(x)        \
    (USB_PLX_DMA_REG_BASE(x) + 0x04)    /* DMA Status */
#define USB_PLX_DMACOUNT_OFFSET(x)        \
    (USB_PLX_DMA_REG_BASE(x) + 0x10)    /* DMA Count */
#define USB_PLX_DMAADDR_OFFSET(x)        \
    (USB_PLX_DMA_REG_BASE(x) + 0x14)    /* DMA Address */
#define USB_PLX_DMADESC_OFFSET(x)        \
    (USB_PLX_DMA_REG_BASE(x) + 0x18)    /* DMA Descriptor */

/*
 * DMA channels are permanently associated with endpoints. The driver will
 * select the DMA channel based upon which endpoint is being used. The base
 * address should be added with the offset of the register
 */

#define USB_PLX_DMA_REG_BASE(x)    (0x160 + 0x20 * (x))

/* Dedicated Endpoint Registers */

#define USB_PLX_DEP_CFG_OFFSET      0x0   /* DEP_CFG Offset */
#define USB_PLX_DEP_RSP_OFFSET      0x4   /* DEP_RES Offset */

#define USB_PLX_DEP_CFGOUT_REG_BASE 0x200 /* CFG OUT Base */
#define USB_PLX_DEP_CFGIN_REG_BASE  0x210 /* CFG IN Base */
#define USB_PLX_DEP_PCIOUT_REG_BASE 0x220 /* PCI OUT Base */
#define USB_PLX_DEP_PCIIN_REG_BASE  0x230 /* PCI IN Base */
#define USB_PLX_DEP_STATIN_REG_BASE 0x240 /* STAT IN Base */
#define USB_PLX_DEP_RCIN_REG_BASE   0x250 /* STAT IN Base */

/* Configurable Endpoint / FIFO Registers */

#define USB_PLX_EP_CFG_OFFSET(x)    \
           (USB_PLX_EP_REG_BASE((UINT32)x) + 0x00)    /* endpt config. offset */
#define USB_PLX_EP_RSP_OFFSET(x)        \
           (USB_PLX_EP_REG_BASE((UINT32)x) + 0x04)    /* endpt response offset */
#define USB_PLX_EP_IRQENB_OFFSET(x)    \
           (USB_PLX_EP_REG_BASE((UINT32)x) + 0x08)    /* endpt intr enable offset */
#define USB_PLX_EP_STAT_OFFSET(x)        \
           (USB_PLX_EP_REG_BASE((UINT32)x) + 0x0C)    /* endpt status offset */
#define USB_PLX_EP_AVAIL_OFFSET(x)        \
           (USB_PLX_EP_REG_BASE((UINT32)x) + 0x10)    /* endpt avail offset */
#define USB_PLX_EP_DATA_OFFSET(x)        \
           (USB_PLX_EP_REG_BASE((UINT32)x) + 0x14)    /* endpt data offset */

/*
 * Every endpoint has a seperate set of register. Add the endpoint register
 * offset with  the base address
 */

#define USB_PLX_EP_REG_BASE(x)         (0x300 + 0x20 * (x))

#define USB_PLX_EP_FIFO_SIZE_BASE(x)   (0x500 + 0x20 * (x))

/* Indexed Registers */

#define USB_PLX_DIAG_IDX            0x00 /* Diagnostic Control Reg */
#define USB_PLX_PKTLEN_IDX          0x01 /* Packet Length Reg */
#define USB_PLX_FRAME_IDX           0x02 /* Frame Counter Reg */
#define USB_PLX_CHIPREV_IDX         0x03 /* Chip Revision Reg */
#define USB_PLX_HS_MAXPOWER_IDX     0x06 /* HS Max Power Reg */
#define USB_PLX_FS_MAXPOWER_IDX     0x07 /* FS Max Power Reg */
#define USB_PLX_HS_INTPOLL_RATE_IDX 0x08 /* HS Int Polling Rate */
#define USB_PLX_FS_INTPOLL_RATE_IDX 0x09 /* FS Int Poling Rate */
#define USB_PLX_HS_NAK_RATE_IDX     0x0A /* HS NAK Rate Reg */
#define USB_PLX_SCRATCH_IDX         0x0B /* Scratch Pad */

#define USB_PLX_EP_X_HS_MAXPKT_IDX(x)   (0x10 + (x) * 0x10)
#define USB_PLX_EP_X_FS_MAXPKT_IDX(x)   (0x11 + (x) * 0x10)
#define USB_PLX_EP_X_SS_MAXPKT_IDX(x)   (0x12 + (x) * 0x10)


#define USB_PLX_FRAME_MASK          (0x00007FF) /* FRAME mask */

/* Bits & Masks OURADDR register */

#define USB_PLX_OURADDR_REG_MASK    0x7F /* OURADDR: Address Mask */
#define USB_PLX_OURADDR_REG_FI      0x80 /* OURADDR: Force Immediate */

/* Bits & Masks USBSTAT register */

#define USB_PLX_USBSTAT_REG_MASK    0xF0 /* USBSTAT: Register Mask */
#define USB_PLX_USBSTAT_REG_GENDEVREMWKUP 0x10 /* Generate Remote Wakeup */
#define USB_PLX_USBSTAT_REG_GENRES  0x20 /* USBSTAT: Generate Resume */
#define USB_PLX_USBSTAT_FS          0x40 /* USBSTAT: Full Speed */
#define USB_PLX_USBSTAT_HS          0x80 /* USBSTAT: High Speed */

/* Bits & Masks USBCTL register */

#define USB_PLX_USBCTL_REG_MASK     0x00FF3EFF /* USBCTL: Reg Mask */
#define USB_PLX_USBCTL_REG_SERNUMID 0x00FF0000 /* USBCTL: Serial Index Number */
                                               
#define USB_PLX_USBCTL_REG_PRODIDEN 0x00002000 /* USBCTL: Prod ID */
#define USB_PLX_USBCTL_REG_VENDIDEN 0x00001000 /* USBCTL: Vend ID */
#define USB_PLX_USBCTL_REG_RPWE     0x00000800 /* USBCTL: Rem WKUP */
#define USB_PLX_USBCTL_REG_VBUSPIN  0x00000400 /* USBCTL: VBUS */
#define USB_PLX_USBCTL_REG_SUSPIMM  0x00000080 /* USBCTL: SUSP IMMD */
#define USB_PLX_USBCTL_REG_SPUD     0x00000040 /* USBCTL: Self Powered*/
#define USB_PLX_USBCTL_REG_RWS      0x00000020 /* USBCTL: Rem WKUP Sup */
#define USB_PLX_USBCTL_REG_USBDE    0x00000008 /* USBCTL: USB Detect Enb */
#define USB_PLX_USBCTL_REG_DRWUE    0x00000002 /* USBCTL: Dev Remote */
                                               /* Wake Enable*/
#define USB_PLX_USBCTL_REG_SPWRSTAT 0x00000001 /* USBCTL: Self Powered */
                                               /* Status */

/* Bits & Masks FRAME register */

#define USB_PLX_FRAME_REG_MASK      0x7FF      /* FRAME: Frame Mask */

/* Bits & Masks DEVINIT register */

#define USB_PLX_DEVINIT_REG_MASK    0xFFF      /* DEVINIT: Reg Mask */
#define USB_PLX_DEVINIT_REG_FRC_PCI_RESET 0x080 /* DEVINIT: Force PCI Reset */
#define USB_PLX_DEVINIT_REG_PCI_ID  0x040    /* DEVINIT: PCI ID */
#define USB_PLX_DEVINIT_PCIEN       0x020    /* DEVINIT: PCI Enable */
#define USB_PLX_DEVINIT_FIFO_RESET  0x010    /* DEVINIT: FIFO Reset */
#define USB_PLX_DEVINIT_CFG_RESET   0x008    /* DEVINIT: CFG Reset */
#define USB_PLX_DEVINIT_PCI_RESET   0x004    /* DEVINIT: PCI Reset */
#define USB_PLX_DEVINIT_USB_RESET   0x002    /* DEVINIT: USB Reset */
#define USB_PLX_DEVINIT_8051_RESET  0x001    /* DEVINIT: 851 Reset */
#define USB_PLX_DEVINIT_FULL_RESET  (USB_PLX_DEVINIT_FIFO_RESET |               \
                                     USB_PLX_DEVINIT_USB_RESET |                \
                                     USB_PLX_DEVINIT_8051_RESET)
#define USB_PLX_DEVINIT_CLK_FREQ    (8 << 8)/* DEVINIT: CLK FREQUENCY */

/* Interrupt enable registers (CPU, PCI, & USB) */

/* Bits & Masks XIRQENB0 register */

#define USB_PLX_XIRQENB0_SETUP          0x00000080
#define USB_PLX_XIRQENB0_EP(x)          (1 << (x))
#define USB_PLX_XIRQENB0_MASK           0x000000FF

/* Bits & Masks XIRQENB1 register */

#define USB_PLX_XIRQENB1_REG_MASK       0x8E1B3FDF  /* Reg. Mask */
#define USB_PLX_XIRQENB1_INTEN          0x80000000  /* XIRQENB1: PCI INT Enable*/
#define USB_PLX_XIRQENB1_PSCINTEN       0x08000000  /* XIRQENB1: PWR State */
                                                    /* Change Interrupt Enable */
#define USB_PLX_XIRQENB1_PCIARTTIMEOUT  0x04000000  /* XIRQENB1: PCI Arbiter */
                            /* Timeout Interrupt Enable */
#define USB_PLX_XIRQENB1_PCIPARITYERR   0x02000000  /* PCI Parity Error */
#define USB_PLX_XIRQENB1_PCIRTYABORT    0x00020000  /* PCI Retry Abort */
#define USB_PLX_XIRQENB1_PCIMASCYCLEDN  0x00010000  /* PCI Master Cycle Done */
#define USB_PLX_XIRQENB1_GPIO           0x00002000  /* General I/O */
#define USB_PLX_XIRQENB1_DMA(x)         (0x00000100 << (x)) /* DMA Int Enable */
#define USB_PLX_XIRQENB1_EEPD           0x00000100    /* EEPROM done */
#define USB_PLX_XIRQENB1_VBUS           0x00000080    /* VBUS change */
#define USB_PLX_XIRQENB1_CS             0x00000040    /* control status */
#define USB_PLX_XIRQENB1_RPRESET        0x00000010    /* root port reset */
#define USB_PLX_XIRQENB1_SUSP           0x00000008    /* suspend request */
#define USB_PLX_XIRQENB1_SUSREQCHG      0x00000004    /* susp. req. change */
#define USB_PLX_XIRQENB1_RESM           0x00000002    /* resume */
#define USB_PLX_XIRQENB1_SOF            0x00000001    /* start-of-frame */

/* Interrupt status registers - status bits are cleared by writing a 1 */

/* Bits & Masks IRQENB0 register */

#define USB_PLX_IRQENB0_SETUP           0x00000080
#define USB_PLX_IRQENB0_EP(x)           (1 << (x))
#define USB_PLX_IRQENB0_EPMASK          0x0000007F

/* Bits & Masks IRQENB1 register */

#define USB_PLX_IRQENB1_REG_MASK        0x8E1B3FDF  /* Reg. Mask */
#define USB_PLX_IRQENB1_INTEN           0x80000000  /* IRQENB1: PCI INT */
#define USB_PLX_IRQENB1_PSCINTEN        0x08000000  /* IRQENB1: PWR State */
                                                    /* Change Interrupt */
#define USB_PLX_IRQENB1_PCIARTTIMEOUT   0x04000000  /* XIRQENB1: PCI Arbiter */
                                                    /* Timeout Interrupt */
#define USB_PLX_IRQENB1_PCIPARITYERR    0x02000000  /* PCI Parity Error */
#define USB_PLX_IRQENB1_PCIRTYABORT     0x00020000  /* PCI Retry Abort */
#define USB_PLX_IRQENB1_PCIMASCYCLEDN   0x00010000  /* PCI Master Cycle Done */
#define USB_PLX_IRQENB1_GPIO            0x00002000  /* General I/O */
#define USB_PLX_IRQENB1_DMA(x)          (0x00000100 << (x)) /* DMA Int */
#define USB_PLX_IRQENB1_EEPD            0x00000100    /* EEPROM done */
#define USB_PLX_IRQENB1_VBUS            0x00000080    /* VBUS change */
#define USB_PLX_IRQENB1_CS              0x00000040    /* control status */
#define USB_PLX_IRQENB1_RPRESET         0x00000010    /* root port reset */
#define USB_PLX_IRQENB1_SUSP            0x00000008    /* suspend request */
#define USB_PLX_IRQENB1_SUSREQCHG       0x00000004    /* susp. req. change */
#define USB_PLX_IRQENB1_RESM            0x00000002    /* resume */
#define USB_PLX_IRQENB1_SOF             0x00000001    /* start-of-frame */
#define USB_PLX_IRQENB1_DMA_SHIFT       9               /* DMA shift value */

/* DMA registers */

/* Bits & Masks DMACTL register */

#define USB_PLX_DMACTL_SG_INT_EN        0x02000000    /* scatter/gather int */
#define USB_PLX_DMACTL_SGE              0x00010000    /* scatter/gather en */
#define USB_PLX_DMACTL_ASE              0x00000010    /* auto start enable */
#define USB_PLX_DMACTL_PREENB           0x00000008    /* dma preempt enable */
#define USB_PLX_DMACTL_FIFOVAL          0x00000004    /* dma fifo validate */
#define USB_PLX_DMACTL_EN               0x00000002    /* dma enable */
#define USB_PLX_DMACTL_ADDRHOLD         0x00000001    /* dma address hold */

/* Bits & Masks DMASTAT register */

#define USB_PLX_DMASTAT_SG_INT          0x02000000    /* scatter/gather */
#define USB_PLX_DMASTAT_TD_INT          0x01000000    /* transact. done */
#define USB_PLX_DMASTAT_ABORT           0x00000002    /* abort DMA trans. */
#define USB_PLX_DMASTAT_START           0x00000001    /* start DMA trans. */

/* Bits & Masks DMACOUNT register */

#define USB_PLX_DMACOUNT_VB             0x80000000    /* DMA valid bit */
#define USB_PLX_DMACOUNT_DIR            0x40000000    /* DMA direction */
#define USB_PLX_DMACOUNT_DIE            0x20000000    /* done int. enable */
#define USB_PLX_DMACOUNT_BC             0x00FFFFFF    /* Byte count */

/* Bits & Masks DMADESC register */

#define USB_PLX_DMADESC_VALID           0x80000000    /* DMA valid bit */
#define USB_PLX_DMADESC_DIR             0x40000000    /* DMA direction */
#define USB_PLX_DMADESC_DONE_INT_EN     0x20000000    /* Done int. enable */
#define USB_PLX_DMADESC_END_CHAIN       0x10000000    /* End of Chain */
#define USB_PLX_DMADESC_FIFO_VALID      0x08000000    /* FIFO vaildation */
#define USB_PLX_DMADESC_COUNT_MASK      0x00FFFFFF    /* Byte count */


/* Test Mode Register */

/* Bits & Masks XCVRDIAG register */

#define USB_PLX_XCVRDIAG_TEST_MASK      0x07000000    /* XCVRDIAG: Mask bits */
#define USB_PLX_XCVRDIAG_TEST_SHIFT_VAL 24
#define USB_PLX_XCVRDIAG_TEST_MODE_SET(x)                                       \
                       ((x) << USB_PLX_XCVRDIAG_TEST_SHIFT_VAL)

/* Endpoint registers */

/* Bits & Masks EP_STAT register */

#define USB_PLX_EP_STAT_FIFOVC          0x0F000000    /* FIFO valid count */
#define USB_PLX_EP_STAT_HBOUTPID        0x00C00000    /* high bw OUT PID */
#define USB_PLX_EP_STAT_TIMEOUT         0x00200000    /* time out */
#define USB_PLX_EP_STAT_STALLSNT        0x00100000    /* stall sent */
#define USB_PLX_EP_STAT_INNAKSNT        0x00080000    /* USB IN NAK Sent */
#define USB_PLX_EP_STAT_INACKRCV        0x00040000    /* USB IN ACK Sent */
#define USB_PLX_EP_STAT_OPNAKSNT        0x00020000    /* USB OUT NAK Sent */
#define USB_PLX_EP_STAT_OUTACKSNT       0x00010000    /* USB OUT ACK Sent */
#define USB_PLX_EP_STAT_FIFO_OVERF      0x00002000    /* USB FIFO Overflow */
#define USB_PLX_EP_STAT_FIFO_UNDERF     0x00001000    /* USB FIFO Underflow*/
#define USB_PLX_EP_STAT_FIFO_FULL       0x00000800    /* USB FIFO Full */
#define USB_PLX_EP_STAT_FIFO_EMPTY      0x00000400    /* USB FIFO Empty */
#define USB_PLX_EP_STAT_FIFO_FLUSH      0x00000200    /* USB FIFO Flush */
#define USB_PLX_EP_STAT_SPOD            0x00000040    /* Short Packet Out */
                                                      /* Done Interrupt */
#define USB_PLX_EP_STAT_SPT             0x00000020    /* Short Packet */
                                                      /* Transmitted Intpt */
#define USB_PLX_EP_STAT_NAKOUT          0x00000010    /* NAK OUT Packets */
#define USB_PLX_EP_STAT_DPR             0x00000008    /* Data Packet */
                                                      /* Received Interrupt */
#define USB_PLX_EP_STAT_DPT             0x00000004    /* Data Packet */
                                                      /* Trnasmitted Intpt */
#define USB_PLX_EP_STAT_DOPT            0x00000002    /* Data OUT Token Int */
#define USB_PLX_EP_STAT_DIT             0x00000001    /* Data IN Toke Int */

/* Bits & Masks EP_CFG register */

#define USB_PLX_EP_CFG_REG_MASK         0x0007078F    /* EP_CFG: Mask Value */
#define USB_PLX_EP_CFG_EBC              0x00070000    /* EP_CFG: */
                                                      /* Ept byte count */
#define USB_PLX_EP_CFG_ENABLE           0x00000400    /* EP_CFG: endpt enable*/
#define USB_PLX_EP_CFG_TYPE_INT         0x00000300    /* EP_CFG: interrupt */
                                                      /* endpt type */
#define USB_PLX_EP_CFG_TYPE_BULK        0x00000200    /* EP_CFG: bulk endpt */
#define USB_PLX_EP_CFG_TYPE_ISO         0x00000100    /* EP_CFG: iso endpt */
#define USB_PLX_EP_CFG_TYPE_MASK        0x00000300    /* EP_CFG: Ept type */
#define USB_PLX_EP_CFG_DIRECTION        0x00000080    /* EP_CFG: Ept dir */
#define USB_PLX_EP_CFG_NUMBER           0x0000000F    /* EP_CFG: Ept no */
#define USB_PLX_EP_CFG_DIR_SHIFT        7             /* EP_CFG: dir shift */
#define USB_PLX_EP_CFG_TYPE_SHIFT       8             /* EP_CFG: type shift */
#define USB_PLX_EP_CFG_BYTE_CNT_SHIFT   16

/* Endpoint response register - bits are cleared on 0:7 and set on 8:15 */

/* Bits & Masks EP_RSP register */

#define USB_PLX_EP_RSP_NAKOUT       0x00000080
#define USB_PLX_EP_RSP_HIDSTAT      0x00000040
#define USB_PLX_EP_RSP_FCRCERR      0x00000020
#define USB_PLX_EP_RSP_INTMODE      0x00000010
#define USB_PLX_EP_RSP_CSPH         0x00000008    /* Ctrl stat ph. h/s */
#define USB_PLX_EP_RSP_NAKOUTMOD    0x00000004
#define USB_PLX_EP_RSP_TOGGLE       0x00000002
#define USB_PLX_EP_RSP_STALL        0x00000001    /* Endpoint halt */
#define USB_PLX_EP_RSP_SET(x)       ((x) << 8)
#define USB_PLX_EP_RSP_CLEAR(x)     (x)

/* Endpoint Interrupts */

/* Bits & Masks EP_IRQENB register */

#define USB_PLX_EP_IRQENB_SPOD       0x00000040    /* Short pkt out done */
#define USB_PLX_EP_IRQENB_SPT        0x00000020    /* Short pkt trans. */
#define USB_PLX_EP_IRQENB_DPR        0x00000008    /* Data pkt recv'd */
#define USB_PLX_EP_IRQENB_DPT        0x00000004    /* Data pkt trans. */
#define USB_PLX_EP_IRQENB_DOPT       0x00000002    /* Data out/ping token */
#define USB_PLX_EP_IRQENB_DIT        0x00000001    /* Data in token */
#define USB_PLX_EP_IRQENB_DEFAULT    (USB_PLX_EP_IRQENB_SPOD |                  \
                                      USB_PLX_EP_IRQENB_SPT  |                  \
                                      USB_PLX_EP_IRQENB_DPR  |                  \
                                      USB_PLX_EP_IRQENB_DPT  |                  \
                                      USB_PLX_EP_IRQENB_DOPT |                  \
                                      USB_PLX_EP_IRQENB_DIT)
                    
#define USB_PLX_EP_IRQEBN_MASK      0x7F

/* NET 228x DMA channels */

#define USB_PLX_DMA_CHANS           4
#define USB_PLX_DMA_ENPT_MASK       0xF

/* FIFO Configurations */

#define USB_PLX_FIFOCTL_CFG0        0        /* configuration 0 */
#define USB_PLX_FIFOCTL_CFG1        1        /* configuration 1 */
#define USB_PLX_FIFOCTL_CFG2        2        /* configuration 2 */
#define USB_PLX_FIFOCTL_CFG_MASK    0x03     /* Fifo config mask */

/* Maximum Packet Size */

#define USB_PLX_MAXPSIZE_8          0x0008
#define USB_PLX_MAXPSIZE_16         0x0010
#define USB_PLX_MAXPSIZE_32         0x0020
#define USB_PLX_MAXPSIZE_64         0x0040
#define USB_PLX_MAXPSIZE_128        0x0080
#define USB_PLX_MAXPSIZE_256        0x0100
#define USB_PLX_MAXPSIZE_512        0x0200
#define USB_PLX_MAXPSIZE_1024       0x0400

/* Definitions for number of transactions per frame */

#define USB_PLX_NTRANS_1       0x0  /* 1 packet  per microframe */
#define USB_PLX_NTRANS_2       0x1  /* 2 packets per microframe */
#define USB_PLX_NTRANS_3       0x2  /* 3 packets per microframe */
#define USB_PLX_NTRANS_SHIFT   11


#ifdef __cplusplus
}
#endif

#endif /* __INCusbPlx228xh */

