/* usbMhdrcTcd.h - USB Mentor Graphics TCD interface definitions */

/*
 * Copyright (c) 2010, 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01i,16may11,w_x  Add bDmaActive in USB_MHDRC_TCD_REQUEST (WIND00276544)
01h,12apr11,m_y  modify USB_MHDRC_TCD_REQUEST structure (WIND00265911)
01g,29mar11,m_y  modify structure usb_mhdrc_tcd_request to support more than one
                 buffer list
01f,28mar11,w_x  Use spinlock for USB_MHDRC_TCD_REPORT_EVENT (WIND00262862)
01e,23mar11,m_y  code clean up based on the review result
01d,04mar11,m_y  add pRequestMutex into struct usb_mhdrc_tcd_pipe
                 to protect the process of request
01c,23feb11,m_y  modify interrupt thread priority 
01b,20oct10,m_y  write
01a,17may10,s_z  created
*/

/*
DESCRIPTION

This file contains the constants, data structures exposed by the USB Mentor 
Graphics Target Controller Driver.

INCLUDE FILES: usbMhdrc.h, usb/usbTgt.h, usb/usbOtg.h

*/

#ifndef __INCusbMhdrcTcdh
#define __INCusbMhdrcTcdh

/* includes */

#include <usbMhdrc.h>
#include <usb/usbTgt.h>
#include <usb/usbOtg.h>

#ifdef  __cplusplus
extern "C" {
#endif

/* defines */

#define USB_MHDRC_TCD_INT_THREAD_PRIORITY         (49)


/* TCD ISR magic */

#define USB_MHDRC_TCD_MAGIC_ALIVE             (0x353000CD) /* 3530 TCD */
#define USB_MHDRC_TCD_MAGIC_DEAD              (0x3530DEAD) /* 3530 DEAD */

#ifndef USB_MHDRC_TCD_NAME
#define USB_MHDRC_TCD_NAME "/usbTgt"
#endif

#define USB_MHDRC_TCD_EP0_MAX_FIFO_SIZE  (0x40)

#define MENTOR_NUM_ENDPOINTS    (0x10)

/* typedefs */

typedef enum  usb_mhdrc_tcd_stage
    {
    USB_MHDRC_TCD_STAGE_IDLE          = (0),
    USB_MHDRC_TCD_STAGE_SETUP         = (1),
    USB_MHDRC_TCD_STAGE_DATA_IN       = (2),
    USB_MHDRC_TCD_STAGE_DATA_OUT      = (3),
    USB_MHDRC_TCD_STAGE_STATUS_IN     = (4),
    USB_MHDRC_TCD_STAGE_STATUS_OUT    = (5),
    USB_MHDRC_TCD_STAGE_ACK_WAIT      = (6)
    }USB_MHDRC_TCD_STAGE;

typedef enum usb_mhdrc_tcd_pipe_state
    {
    USB_MHDRC_TCD_PIPE_INIT = 0,
    USB_MHDRC_TCD_PIPE_DELETE = 1
    }USB_MHDRC_TCD_PIPE_STATE;

/* TCD instance data structure */

typedef struct usb_mhdrc_tcd_data
    {
    pUSBTGT_TCD         pTCD;     /* TCD pointer */
    pUSB_MUSBMHDRC      pMHDRC;   /* Common hardware MHDRC structure */
    NODE                tcdNode;  /* TCD NODE */
    USB_MHDRC_TCD_STAGE ep0Stage; /* EP0 stage */

    OS_EVENT_ID         tcdSyncMutex;      /* Mutex to protect the common */
    OS_EVENT_ID         isrSemId;          /* ISR semphore ID */
    OS_THREAD_ID        intHandlerThread ; /* Interrupt Handler Thread ID */

    UINT16              uTcdTxIrqStatus; /* TX irq status */
    UINT16              uTcdRxIrqStatus; /* RX irq status */
    UINT8               uTcdUsbIrqStatus;/* USB irq status */

    LIST                pipeList;        /* Pipe list */

    UINT16              inEpUseFlag;     /* Endpoint Index being used */
    UINT16              outEpUseFlag;    /* Endpoint Index being used */
    UINT16              maxPacketLen[MENTOR_NUM_ENDPOINTS]; /* Max packet length */
    UINT16              uCsr0;           /* CSR0 register value */
    UINT16              uIntTxMask;      /* TX interrupt mask register value */
    UINT16              uIntRxMask;      /* RX interrupt mask register value */
    UINT8               uIntUsbMask;     /* USB interrupt mask register value */
    UINT8               uTestMode;       /* Current test mode*/
    BOOL                isDeviceSuspend; /* If the device is suspended */
    BOOL                isRemoteWakeup;  /* If the remote wakeup is enabled */
    BOOL                isSelfPower;     /* If the device is selfpower */
    BOOL                isSoftConnected; /* If the device is soft connected */
    } USB_MHDRC_TCD_DATA, *pUSB_MHDRC_TCD_DATA;

/* TCD pipe data structure */

typedef struct usb_mhdrc_tcd_pipe
    {
    NODE   pipeNode;            /* Node to attach to the */
    SEM_ID pPipeSyncMutex;      /* Protect the request list */
    SEM_ID pRequestMutex;       /* Protect the request handle */
    LIST   requestList;         /* Request list waiting to handle */
    LIST   freeRequestList;     /* Free request list */
    UINT16 uIsoPipeErpCount;    /* Use for ISO transfer */
    UINT16 uIsoPipeCurErpIndex; /* Use for ISO transfer */
    UINT16 uMaxPacketSize;      /* Max packet size */
    UINT16 uInterface;          /* Interface Num */
    UINT8  uStatus;             /* Status of the endpoint */
    UINT8  uDevAddress;         /* Address of the device */
    UINT8  uEpType;             /* Type of endpoint */
    UINT8  uEpAddress;          /* Address of the endpoint */
    UINT8  uEpDir;              /* Direction of the endpoint */
    UINT8  uDmaChannel;         /* Dma channel of the endpoint */
    UINT8  uPipeFlag;           /* Current pipe state */
    UINT8  bInterval;           /* Interval */
    }USB_MHDRC_TCD_PIPE,*pUSB_MHDRC_TCD_PIPE;

/* TCD request data structure */

typedef struct usb_mhdrc_tcd_request
    {
    NODE                           requestNode;              /* Request node */
    struct usb_mhdrc_tcd_request * pNext;                    /* Next request */
    UINT32                         uActLength;               /* Actual length of the transfer */
    UINT32                         uXferSize;                /* Bytes size we expect to transfer */
    UINT8 *                        pCurrentBuffer;           /* Data buffer address */
    pUSB_ERP                       pErp;                     /* Pointer to the pErp */
    pUSB_MHDRC_TCD_PIPE            pTCDPipe;                 /* Pointer to the pTCDPipe */
    UINT16                         uErpBufIndex;             /* Index of the bufList */
    UINT16                         uErpBufCount;             /* Count of the bufList */
    UINT8                          uDmaMode;                 /* Current DMA Mode */
    UINT32                         uCurXferLength;           /* Current transfer length */
    BOOL                           bDmaActive;
    }USB_MHDRC_TCD_REQUEST, * pUSB_MHDRC_TCD_REQUEST;

#define TCD_NODE_TO_USB_MHDRC_TCD_DATA(pNode)                              \
    ((USB_MHDRC_TCD_DATA *) ((char *) (pNode) -                            \
                          OFFSET(USB_MHDRC_TCD_DATA, tcdNode)))
                          
#define PIPE_NODE_TO_USB_MHDRC_TCD_PIPE(pNode)                             \
    ((USB_MHDRC_TCD_PIPE *) ((char *) (pNode) -                            \
                          OFFSET(USB_MHDRC_TCD_PIPE, pipeNode)))

#define REQUEST_NODE_TO_USB_MHDRC_TCD_REQUEST(pNode)                       \
    ((USB_MHDRC_TCD_REQUEST *) ((char *) (pNode) -                         \
                          OFFSET(USB_MHDRC_TCD_REQUEST, requestNode)))

#define USB_MHDRC_TCD_REPORT_EVENT(pMHDRC, pTCDData)                      \
    {                                                                     \
    SPIN_LOCK_ISR_TAKE(&(pMHDRC)->spinLock);                              \
    (pTCDData)->uTcdTxIrqStatus |= (pMHDRC)->uTxIrqStatus;                \
    (pTCDData)->uTcdRxIrqStatus |= (pMHDRC)->uRxIrqStatus;                \
    (pTCDData)->uTcdUsbIrqStatus |= (pMHDRC)->uUsbIrqStatus;              \
    (pMHDRC)->uTxIrqStatus = 0;                                           \
    (pMHDRC)->uRxIrqStatus = 0;                                           \
    (pMHDRC)->uUsbIrqStatus = 0;                                          \
    SPIN_LOCK_ISR_GIVE (&(pMHDRC)->spinLock);                             \
    OS_RELEASE_EVENT((pTCDData)->isrSemId);                               \
    }

#ifdef  __cplusplus
}
#endif

#endif  /* __INCusbMhdrcTcdh */


