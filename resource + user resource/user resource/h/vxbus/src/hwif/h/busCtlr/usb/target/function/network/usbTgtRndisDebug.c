/* usbTgtRndisDebug.c - USB Remote NDIS Function Device Debug Module */

/*
 * Copyright (c) 2011, 2012 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
Modification history
--------------------
01h,18may12,s_z  Add support for USB 3.0 target (WIND00326012) 
01g,13dec11,m_y  Modify according to code check result (WIND00319317)
01f,29sep11,s_z  Add direct callback mode to avoid asynch issue (WIND00306920)
01e,11apr11,s_z  Add Open Specifications Documentation description
01d,08apr11,ghs  Fix code coverity issue(WIND00264893)
01c,22mar11,s_z  Code clean up based on the code review
01b,08mar11,s_z  Code clean up
01a,04jan11,s_z  written
*/

/*
DESCRIPTION

This file includes the debug module for the USB Remote NDIS function driver.

This RNDIS function driver follows the [MS-RNDIS]:Remote Network Driver 
Interface Specification (RNDIS) Protocol Specification 1.0, which is one of the
Open Specifications Documentation.

Following is the Intellectual Property Rights Notice for Open Specifications 
Documentation:

\h Intellectual Property Rights Notice for Open Specifications Documentation

\h Technical Documentation. 

Microsoft publishes Open Specifications documentation for protocols, 
file formats, languages, standards as well as overviews of the interaction 
among each of these technologies.

\h Copyrights. 

This documentation is covered by Microsoft copyrights. Regardless of any other 
terms that are contained in the terms of use for the Microsoft website that
hosts this documentation, you may make copies of it in order to develop 
implementations of the technologies described in the Open Specifications 
and may distribute portions of it in your implementations using these 
technologies or your documentation as necessary to properly document the 
implementation. You may also distribute in your implementation, with or without 
modification, any schema, IDL's, or code samples that are included in the 
documentation. This permission also applies to any documents that are 
referenced in the Open Specifications.

\h No Trade Secrets.

Microsoft does not claim any trade secret rights in this documentation.

\h Patents.

Microsoft has patents that may cover your implementations of the technologies 
described in the Open Specifications. Neither this notice nor Microsoft's 
delivery of the documentation grants any licenses under those or any other 
Microsoft patents. However, a given Open Specification may be covered by 
Microsoft's Open Specification Promise (available here: 
http://www.microsoft.com/interop/osp) or the Community Promise 
(available here: http://www.microsoft.com/interop/cp/default.mspx).
If you would prefer a written license, or if the technologies described in 
the Open Specifications are not covered by the Open Specifications Promise 
or Community Promise, as applicable, patent licenses are available by 
contacting iplg@microsoft.com.

\h Trademarks. 

The names of companies and products contained in this 
documentation may be covered by trademarks or similar intellectual 
property rights. This notice does not grant any licenses under those rights.

\h Fictitious Names.

The example companies, organizations, products, 
domain names, e-mail addresses, logos, people, places, and events depicted in 
this documentation are fictitious. No association with any real company, 
organization, product, domain name, email address, logo, person, place, or 
event is intended or should be inferred.

\h Reservation of Rights. 

All other rights are reserved, and this notice does 
not grant any rights other than specifically described above, whether by 
implication, estoppel, or otherwise.

\h Tools. 

The Open Specifications do not require the use of Microsoft 
programming tools or programming environments in order for you to develop an 
implementation. If you have access to Microsoft programming tools and 
environments you are free to take advantage of them. Certain Open 
Specifications are intended for use in conjunction with publicly available 
standard specifications and network programming art, and assumes that the 
reader either is familiar with the aforementioned material or has immediate 
access to it.

INCLUDE FILES:  endLib.h usb/usbTgtRndis.h usbTgtNetMediumBinder.h

*/

/* includes */

#include <endLib.h>
#include <usb/usbTgtRndis.h>
#include "usbTgtNetMediumBinder.h"


IMPORT LIST  gUsbTgtRndisList;


/*******************************************************************************
*
* usbTgtRndisDataShow - show the usbtgt_rndis structure's information
*
* This routine shows the usbtgt_rndis structure's information
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbTgtRndisDataShow
    (
    pUSBTGT_RNDIS pUsbTgtRndis
    )
    {
    int    index;
    int    count;
    pUSBTGT_BINDER pBinder = NULL;

    if (pUsbTgtRndis == NULL)
        {
        printf("usbTgtRndisDataShow: Invalid pUSBTGT_RNDIS, show the default rndis Dev\n");

        pUsbTgtRndis = (pUSBTGT_RNDIS)lstFirst (&gUsbTgtRndisList);

        if (pUsbTgtRndis == NULL)
            return;
        }

    printf("rndisNode            = %p\n", &pUsbTgtRndis->rndisNode);
    printf("rndisMutex           = %p\n", &pUsbTgtRndis->rndisMutex);


    printf("pMediumName          = %s\n", pUsbTgtRndis->pMediumName);
    printf("uMediumUnit          = 0x%X\n", pUsbTgtRndis->uMediumUnit);


    printf("uMediumType          = 0x%X\n", pUsbTgtRndis->uMediumType);
#if defined (USBTGT_NET_TASK_MODE)
    printf("RndisTaskMsgQID      = %p\n", pUsbTgtRndis->RndisTaskMsgQID);

    printf("RndisTaskID          = %p\n", pUsbTgtRndis->RndisTaskID);
#endif /*USBTGT_NET_TASK_MODE */     
    printf("requestMsgBuf        = %p\n", &pUsbTgtRndis->requestMsgBuf);

    printf("replyMsgBuf          = %p\n", pUsbTgtRndis->replyMsgBuf);



    printf("uRequestMsgLen       = 0x%X\n", pUsbTgtRndis->uRequestMsgLen);
    printf("uReplyMsgLen         = 0x%X\n", pUsbTgtRndis->uReplyMsgLen);



    printf("intInPipeHandle      = %p\n", pUsbTgtRndis->intInPipeHandle);


    printf("intInErp             = %p\n", &pUsbTgtRndis->intInErp);
    printf("intInBuf             = %p\n", pUsbTgtRndis->intInBuf);
    printf("intInErpMutex        = %p\n", pUsbTgtRndis->intInErpMutex);
    printf("uIntInPipeStatus     = 0x%X\n", pUsbTgtRndis->uIntInPipeStatus);
    printf("bIntInErpUsed        = 0x%X\n", pUsbTgtRndis->bIntInErpUsed);


    printf("bulkOutPipeHandle    = %p\n", pUsbTgtRndis->bulkOutPipeHandle);
    printf("bulkOutErp           = %p\n", &pUsbTgtRndis->bulkOutErp);
    printf("bulkOutErpMutex      = %p\n", pUsbTgtRndis->bulkOutErpMutex);
    printf("uBulkOutPipeStatus   = 0x%X\n", pUsbTgtRndis->uBulkOutPipeStatus);
    printf("bBulkOutErpUsed      = 0x%X\n", pUsbTgtRndis->bBulkOutErpUsed);


    printf("bulkInPipeHandle     = %p\n", pUsbTgtRndis->bulkInPipeHandle);
    printf("bulkInErp            = %p\n", &pUsbTgtRndis->bulkInErp);
    printf("bulkInErpMutex       = %p\n", pUsbTgtRndis->bulkInErpMutex);
    printf("uBulkInPipeStatus    = 0x%X\n", pUsbTgtRndis->uBulkInPipeStatus);
    printf("bBulkInErpUsed       = 0x%X\n", pUsbTgtRndis->bBulkInErpUsed);


    printf("rndisState           = 0x%X\n", pUsbTgtRndis->rndisState);
    printf("rndisFilterValue     = 0x%X\n", pUsbTgtRndis->rndisFilterValue);
    printf("uConfigurationValue  = 0x%X\n", pUsbTgtRndis->uConfigurationValue);

    printf("uInterfaceNubmer     = 0x%X\n", pUsbTgtRndis->uInterfaceNubmer);
    printf("uAltSetting          = 0x%X\n", pUsbTgtRndis->uAltSetting);



    printf("uSpeed               = 0x%X\n", pUsbTgtRndis->uSpeed);

    pBinder = (pUSBTGT_BINDER)pUsbTgtRndis->pBinder;

    if (NULL == pBinder)
        {
        /* Do not check the pBinder bellow , return */

        printf("pUsbTgtRndis->pBinder = %p\n", pUsbTgtRndis->pBinder);

        return;
        }

    printf("freeXmitBufList count = 0x%X\n", lstCount (&pBinder->freeXmitBufList));


    count = lstCount (&pBinder->freeXmitBufList);

    for (index = 1; index <= count; index ++)
        {
        printf(" - buf Node             = %p\n", lstNth (&pBinder->freeXmitBufList,index));
        }

    printf("freeRcvBufList count = 0x%X\n", lstCount (&pBinder->freeRcvBufList));


    count = lstCount (&pBinder->freeRcvBufList);

    for (index = 1; index <= count; index ++)
        {
        printf(" - buf Node             = %p\n", lstNth (&pBinder->freeRcvBufList,index));
        }

    printf("xmitToMediumBufList count = 0x%X\n", lstCount (&pBinder->xmitToMediumBufList));
    count = lstCount (&pBinder->xmitToMediumBufList);

    for (index = 1; index <= count; index ++)
        {
        printf("- buf Node             = %p\n", lstNth (&pBinder->xmitToMediumBufList,index));
        }


    printf("rcvFromMediumBufList count  = 0x%X\n", lstCount (&pBinder->rcvFromMediumBufList));

    count = lstCount (&pBinder->rcvFromMediumBufList);

    for (index = 1; index <= count; index ++)
        {
        printf(" - buf Node             = %p\n", lstNth (&pBinder->rcvFromMediumBufList,index));
        }
    }




