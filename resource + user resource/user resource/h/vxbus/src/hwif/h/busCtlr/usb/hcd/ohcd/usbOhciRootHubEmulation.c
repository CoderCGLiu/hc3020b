/* usbOhciRootHubEmulation.c - USB OHCI Root hub Emulation */

/*
 * Copyright (c) 2002-2003, 2005-2006, 2009-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2002-2003, 2005-2006, 2009-2013 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
modification history
--------------------
01p,03may13,wyy  Remove compiler warning (WIND00356717)
01o,21apr13,wyy  Cut the relationship among busIndex of HCDData, unitNumber of
                 vxBus Device, and HC count (such as g_EHCDControllerCount or
                 g_UhcdHostControllerCount) to dynamically release or announce
                 a HCD driver (WIND00362065)
01n,08aug11,w_x  Fix handling for USB_OHCI_HUB_CLASS_DESCRIPTOR (WIND00272217)
01m,11jan11,ghs  Return directly if URB is NULL when copy interrupt status data
                 (WIND00237602)
01l,02jul10,m_y  Modify for coding convention
01k,18may10,m_y  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
01j,08mar10,j_x  Changed for USB debug (WIND00184542)
01i,13jan10,ghs  vxWorks 6.9 LP64 adapting
01h,15apr09,w_x  Corrected reset timing (WIND00158896)
01g,10jul07,jrp  Removing OS_ENTER_CRITICAL_SECTION
01f,08oct06,ami  Changes for USB-vxBus changes
01e,28mar05,pdg  non-PCI changes
01d,02mar05,ami  SPR #106373 (OHCI Max Controllers Issue)
01c,22Jul03,gpd  implemented the MIPS changes
01b,27jun03,nld  Changing the code to WRS standards
01a,04may02,ssh  Initial Version
*/

/*
DESCRIPTION

This file defines the functions for implementing root hub emulation

INCLUDE FILES:    usbOhci.h, usbOhciRegisterInfo.h, usbOhciTransferManagement.h
*/

/*
INTERNAL
 *******************************************************************************
 * Filename         : OHCI_RootHubEmulation.c
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 * Description      : This file defines the functions for implementing root
 *                    hub emulation.
 *
 *
 ******************************************************************************/

/* includes */

#include "usb/usbOsal.h"
#include "usbOhci.h"
#include "usbOhciRegisterInfo.h"
#include "usbOhciTransferManagement.h"
#include "usb/usbHst.h"

/* defines */

/********************** MODULE SPECIFIC MACRO DEFINITION **********************/

/*
 * To hold the mask value within the SETUP::bmRequestType field to indicate
 * the recipient of the request.
 */

#define USB_OHCI_CONTROL_TRANSFER_REQUEST_RECIPIENT_MASK        0x1F

/*
 * To hold the mask value withing the SETUP::bmRequestType field to indicate
 * the type of request (standard, class specific, etc)
 */

#define USB_OHCI_CONTROL_TRANSFER_REQUEST_TYPE_MASK             0x60

/* To hold the mask value to indicate a class specific request */

#define USB_OHCI_CLASS_SPECIFIC_REQUEST                         0x20

/*
 * To hold the offset of the descriptor type information in the
 * SETUP::wValue field of the GET_DESCRIPTOR request.
 */

#define USB_OHCI_GET_DESCRIPTOR_TYPE_OFFSET                     0x08

/* To hold the length of the root hub device descriptor length */

#define USB_OHCI_ROOT_HUB_DEVICE_DESCRIPTOR_LENGTH              0x12

/* To hold the length of the root hub configuration descriptor length */

#define USB_OHCI_ROOT_HUB_CONFIGURATION_DESCRIPTOR_LENGTH       0x19

/* To hold the size of the response to the GET_STATUS request */

#define USB_OHCI_GET_STATUS_REQUEST_RESPONSE_SIZE               0x02

/* Features selectors for the GET_STATUS standard request (BEGIN) */

/* To hold the value indicating the device is self powered */

#define USB_OHCI_DEVICE_SELF_POWERED                            0x0001

/* To hold the value indicating the device is remoted wakeup enabled */

#define USB_OHCI_DEVICE_REMOTE_WAKEUP_ENABLED                   0x0002

/* Features selectors for the GET_STATUS standard request (END) */

/* To hold the size of the response to the GET_CONFIGURATION request */

#define USB_OHCI_GET_CONFIGURATION_REQUEST_RESPONSE_SIZE        0x01

/* To hold the size of the response to the GET_INTERFACE request */

#define USB_OHCI_GET_INTERFACE_REQUEST_RESPONSE_SIZE            0x01

/* To hold the device states for the root hub (BEGIN) */

/* Root hub is in default state */

#define USB_OHCI_DEVICE_DEFAULT_STATE                           0x00

/* Root hub is in addressed state */

#define USB_OHCI_DEVICE_ADDRESS_STATE                           0x01

/* Root hub is in configured state */

#define USB_OHCI_DEVICE_CONFIGURED_STATE                        0x02

/* To hold the device states for the root hub (END) */

/* To hold the size of the response to the GET_HUB_STATUS request */

#define USB_OHCI_GET_HUB_STATUS_REQUEST_RESPONSE_SIZE           0x04

/* To hold the size of the response to the GET_PORT_STATUS request */

#define USB_OHCI_GET_PORT_STATUS_REQUEST_RESPONSE_SIZE          0x04

/* To hold the value of hub class descriptor type */

#define USB_OHCI_HUB_CLASS_DESCRIPTOR_TYPE                      0x29

/* To hold the feature selectors for the hub class requests (BEGIN) */

/* To hold the value of the hub local power change feature */

#define USB_OHCI_C_HUB_LOCAL_POWER                              0x00

/* To hold the value of the hub over current change feature */

#define USB_OHCI_C_HUB_OVER_CURRENT                             0x01

/* To hold the value of the port enable feature */

#define USB_OHCI_PORT_ENABLE                                    0x01

/* To hold the value of the port suspend feature */

#define USB_OHCI_PORT_SUSPEND                                   0x02

/* To hold the value of the port reset feature */

#define USB_OHCI_PORT_RESET                                     0x04

/* To hold the value of the port power feature */

#define USB_OHCI_PORT_POWER                                     0x08

/* To hold the value of the port connection change feature */

#define USB_OHCI_C_PORT_CONNECTION                              0x10

/* To hold the value of the port enable change feature */

#define USB_OHCI_C_PORT_ENABLE                                  0x11

/* To hold the value of the port suspend change feature */

#define USB_OHCI_C_PORT_SUSPEND                                 0x12

/* To hold the value of the port over current change feature */

#define USB_OHCI_C_PORT_OVER_CURRENT                            0x13

/* To hold the value of the port reset change feature */

#define USB_OHCI_C_PORT_RESET                                   0x14

/* Basic size for hub class descriptor not including the port info bytes */

#define USB_OHCI_HUB_CLASS_DESCR_BASIC_SIZE    7

/* To hold the feature selectors for the hub class requests (END) */

/* typedefs */

/****************** MODULE SPECIFIC DATA STRUCTURE DECLARATION ****************/

/* Structure to hold the configuration descriptor for the root hub */

typedef struct usbOhciRootHubConfigurationDescriptor
    {
    USBHST_CONFIG_DESCRIPTOR        configurationDescriptor;
                /* To hold the configuration descriptor information */
    USBHST_INTERFACE_DESCRIPTOR        interfaceDescriptor;
                /* To hold the interface descriptor information */
    USBHST_ENDPOINT_DESCRIPTOR        endpointDescriptor;
                /* To hold the endpoint descriptor information */
    } __attribute__((__packed__)) USB_OHCI_ROOT_HUB_CONFIGURATION_DESCRIPTOR;

typedef struct usbOhciRootHubConfigurationDescriptor *
                    PUSB_OHCI_ROOT_HUB_CONFIGURATION_DESCRIPTOR;


/* Structure to store the hub class descriptor for the device */

typedef struct usbOhciHubClassDescriptor
    {
    UINT8   bDescLength;            /* To hold the length of the descriptor */
    UINT8   bDescriptorType;        /* To hold the descriptor type */
    UINT8   bNbrPorts;              /* To hold the number of downstream ports */
    UINT16  wHubCharacteristics;    /* To hold the hub characteristics */
    UINT8   bPwrOn2PwrGood;         /* To hold the power on to power good time */
    UINT8   bHubContrCurrent;       /* To hold the maximum current required by */
    UINT8   pPortInfoBytes[4];      /* To hold MAX 16 ports information bits */
    } __attribute__((__packed__)) USB_OHCI_HUB_CLASS_DESCRIPTOR;

typedef struct usbOhciHubClassDescriptor *    PUSB_OHCI_HUB_CLASS_DESCRIPTOR;

/* locals */

/******************* MODULE SPECIFIC VARIABLES DEFINITION *********************/

/* To hold the device descriptor for the root hub */

LOCAL USBHST_DEVICE_DESCRIPTOR    usbOhciRootHubDeviceDescriptor =
    {
    0x12,       /* Size of this descriptor */
    0x01,       /* Descriptor type */
    OS_UINT16_CPU_TO_LE(0x0110),     /* BCD USB Specification Release Number  */
    0x09,       /* Device class code   */
    0x00,       /* Device sub class code   */
    0x00,       /* Device protocol code */
    0x40,       /* Maximum packet size for endpoint zero */
    0x0000,     /* Vendor ID of this interface  */
    0x0000,     /* Product ID of this interface  */
    0x0000,     /* BCD Device release number */
    0x00,       /* Index of string descriptor describing manufacturer */
    0x00,       /* Index of string descriptor describing product */
    0x00,       /* Index of string descriptor describing the device's */
                /* serial number */
    0x01        /* Number of possible configurations */
    };

/* To hold the configuration descriptor for the root hub */

LOCAL USB_OHCI_ROOT_HUB_CONFIGURATION_DESCRIPTOR
                        usbOhciRootHubConfigurationDescriptor =
    {
    /* Initialize the configuration descriptor */
        {
        0x09,   /* Size of this descriptor */
        0x02,   /* Descriptor type */
        OS_UINT16_CPU_TO_LE(0x0019), /* Total length of the configuration descriptor */
        0x01,   /* Number of interfaces in this configuration */
        0x01,   /* Value to set this configuration */
        0x00,   /* Index of the string describing this configuration */
        0xE0,   /* Self powered, remote wakeup enabled */
        0x00    /* Maximum power consumption from the USB bus */
        },

    /* Initialize the interface descriptor */
        {
        0x09,   /* Size of this descriptor */
        0x04,   /* Descriptor type */
        0x00,   /* Interface number */
        0x00,   /* Alternate setting number */
        0x01,   /* Number of endpoints in this interface */
        0x09,   /* Interface class code */
        0x00,   /* Interface sub class code */
        0x00,   /* Interface protocol code */
        0x00    /* Index of the string descriptor describing this interface */
        },

    /* Initialize the endpoint descriptor */
        {
        0x07,   /* Size of this descriptor */
        0x05,   /* Descriptor type */
        0x81,   /* Endpoint address */
        0x03,   /* Interrupt endpoint */
        OS_UINT16_CPU_TO_LE(0x0008), /* Maximum packet size for the endpoint */
        0xFF    /* Polling interval for the endpoint */
        }
    };


/* forward declarations */

/****************** MODULE SPECIFIC FUNCTIONS DECLARATION *********************/

/* Function to initialize the root hub emulation module */

LOCAL USBHST_STATUS usbOhciRootHubEmulationInit (UINT8 uHostControllerIndex);

/* Function to handle the request addressed to the root hub */

LOCAL USBHST_STATUS usbOhciProcessRootHubRequest (UINT8 uHostControllerIndex,
                                                  pUSBHST_URB    pUrb);

/* Function to process the control transfers addressed to the root hub */

LOCAL USBHST_STATUS usbOhciProcessRootHubControlTransfer
                        (UINT8 uHostControllerIndex, pUSBHST_URB    pUrb);

/* Function to process the interrpt transfers addressed to the root hub */

LOCAL USBHST_STATUS usbOhciProcessRootHubInterruptTransfer
                        (UINT8 uHostControllerIndex, pUSBHST_URB    pUrb);

/* Function to process the USB standard requests addressed to the root hub */

LOCAL USBHST_STATUS usbOhciProcessRootHubStandardRequest
                        (UINT8 uHostControllerIndex, pUSBHST_URB    pUrb);

/* Function to process hub class specific requests addressed to the root hub */

LOCAL USBHST_STATUS usbOhciProcessRootHubClassSpecificRequest
                        (UINT8 uHostControllerIndex, pUSBHST_URB    pUrb);

/* Function to process the status change on the root hub */

LOCAL VOID usbOhciProcessRootHubStatusChange (UINT8    uHostControllerIndex);

/* functions */

/************************ GLOBAL FUNCTIONS DEFINITION *************************/


/******************* MODULE SPECIFIC FUNCTIONS DEFINITION *********************/

/***************************************************************************
*
* usbOhciRootHubEmulationInit - Root Hub Emulation Initialisation
*
* This function initializes the root hub emulation module
*
* PARAMETERS: <nHostControllerIndex (IN)> - Host controller index corresponding
* to the host controller for which an interrupt has occurred.
*
* RETURNS: USBHST_SUCCESS on success,
* USBHST_INVALID_PARAMETER if the parameters are invalid
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbOhciRootHubEmulationInit
    (
    UINT8     uHostControllerIndex
    )
    {
    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA              pHCDData = NULL;

    /* To hold the value read from the registers */

    UINT32                      uRegisterValue = 0;

    /* Check whether the host controller index is valid */

    if (uHostControllerIndex >= USB_MAX_OHCI_COUNT ||
        g_OHCDData[uHostControllerIndex].pDev == NULL)
        {
        /* Invalid host controller index */

        USB_OHCD_ERR("invalid host controller index %d\n",
                     uHostControllerIndex,2, 3, 4, 5, 6);
        return USBHST_INVALID_PARAMETER;
        }
    /* Obtain the pointer to the host controller information */

    pHCDData = &g_OHCDData[uHostControllerIndex];

    /* Read the contents of the HcRhDescriptorA register */

    uRegisterValue =
    USB_OHCI_REG_READ(pHCDData->pDev,
                      USB_OHCI_RH_DESCRIPTOR_A_REGISTER_OFFSET);

    /* Update the number of downstream port supported by the root hub */

    pHCDData->uNumberOfDownStreamPorts =
        (UINT8)(uRegisterValue & USB_OHCI_RH_DESCRIPTOR_A_REGISTER_NDP_MASK);

    /* NOTE: Update the hub descriptors for endianess issues */

    return USBHST_SUCCESS;

    } /* End of function usbOhciRootHubEmulationInit () */

/***************************************************************************
*
* usbOhciProcessRootHubRequest - Root Hub Request Handler
*
* This function handles the request addressed to the root hub.
*
* PARAMETERS: <nHostControllerIndex (IN)> - Host controller index corresponding
* to the host controller for which an interrupt has occurred.
*
* <pURB (IN)> - Pointer to the URB.
*
* RETURNS: USBHST_SUCCESS on success,
* USBHST_INVALID_PARAMETER if the parameters are invalid
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbOhciProcessRootHubRequest
    (
    UINT8       uHostControllerIndex,
    pUSBHST_URB    pUrb
    )
    {
    /* To hold the status of the operation */

    USBHST_STATUS       nStatus = USBHST_SUCCESS;

    /* Check whether the host controller index is valid */

    if (uHostControllerIndex >= USB_MAX_OHCI_COUNT ||
        g_OHCDData[uHostControllerIndex].pDev == NULL)
        {
        /* Invalid host controller index */

        USB_OHCD_ERR("invalid host controller index %d\n",
                     uHostControllerIndex,2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Check whether the URB parameter is valid */

    if (pUrb == NULL)
        {
        /* Invalid URB parameter */
        return USBHST_INVALID_PARAMETER;
        }

    /*
     * Check the endpoint address to determine whether the request
     * corresponds to a control transfer or interrupt transfer.
     */

    if (pUrb->uEndPointAddress == 0x00)
        {
        /*
         * Call the function to process the control transfer on the
         * root hub.
         */
        nStatus =
            usbOhciProcessRootHubControlTransfer (uHostControllerIndex, pUrb);
        }
    else if (pUrb->uEndPointAddress == 0x81)
        {
        /*
         * Call the function to process the interrupt transfer on the
         * root hub.
         */
        nStatus =
            usbOhciProcessRootHubInterruptTransfer (uHostControllerIndex, pUrb);
        }
    else
        {
        /* Invalid endpoint number */
        nStatus = USBHST_INVALID_PARAMETER;
        }

    return nStatus;

    } /* End of function usbOhciProcessRootHubRequest () */

/***************************************************************************
*
* usbOhciProcessRootHubControlTransfer - processes the control transfers
*
* This function processes the control transfers addressed to the root hub
*
* PARAMETERS: <nHostControllerIndex (IN)> - Host controller index corresponding
* to the host controller for which an interrupt has occurred.
*
* <pURB (IN)> - Pointer to the URB.
*
* RETURNS: USBHST_SUCCESS on success,
* USBHST_INVALID_PARAMETER if the parameters are invalid
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbOhciProcessRootHubControlTransfer
    (
    UINT8       uHostControllerIndex,
    pUSBHST_URB pUrb
    )
    {
    /* To hold the status of the operation */

    USBHST_STATUS           nStatus = USBHST_SUCCESS;

    /* To hold the contents of the SETUP packet */

    pUSBHST_SETUP_PACKET    pSetupPacket = NULL;

    /* Check whether the SETUP packet is valid */

    if (pUrb->pTransferSpecificData == NULL)
        {
        /* Invalid setup packet buffer */

        USB_OHCD_ERR("invalid host controller index %d\n",
                     uHostControllerIndex,2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Check whether the transfer buffer is valid */

    if ((pUrb->uTransferLength != 0) && (pUrb->pTransferBuffer == NULL))
        {
        /* Invalid transfer buffer */

        USB_OHCD_ERR("invalid transfer buffer \n",
                     1, 2, 3, 4, 5, 6);
        return USBHST_INVALID_PARAMETER;
        }

    /* Obtain the pointer to the SETUP packet */

    pSetupPacket = (pUSBHST_SETUP_PACKET) pUrb->pTransferSpecificData;

    /*
     * Check whether the request is a USB standard request or a
     * hub class specific request.
     */

    if ((pSetupPacket->bmRequestType &
         USB_OHCI_CONTROL_TRANSFER_REQUEST_TYPE_MASK) == 0x00)
        {
        /*
         * Call the function to process the standard requests addressed
         * to the root hub.
         */
        nStatus =
            usbOhciProcessRootHubStandardRequest (uHostControllerIndex, pUrb);
        }
    else if ((pSetupPacket->bmRequestType &
              USB_OHCI_CONTROL_TRANSFER_REQUEST_TYPE_MASK) ==
                    USB_OHCI_CLASS_SPECIFIC_REQUEST)
        {
        /*
         * Call the function to process the hub class specific requests
         * addressed to the root hub.
         */
        nStatus =
        usbOhciProcessRootHubClassSpecificRequest (uHostControllerIndex, pUrb);
        }
    else
        {
        /* Invalid request */
        pUrb->nStatus = USBHST_INVALID_REQUEST;
        }

    /* Check whether the call back function for the URB is valid */

    if (pUrb->pfCallback != NULL)
        {
        /* Call the call back function for the URB */
        (*(pUrb->pfCallback))(pUrb);
        }

    return nStatus;

    } /* End of function usbOhciProcessRootHubControlTransfer () */

/***************************************************************************
*
* usbOhciProcessRootHubInterruptTransfer - processes the interrupt transfers
*
* This function processes the interrupt transfers addressed to the root hub
*
* PARAMETERS: <nHostControllerIndex (IN)> - Host controller index corresponding
* to the host controller for which an interrupt has occurred.
*
* <pURB (IN)> - Pointer to the URB.
*
* RETURNS: USBHST_SUCCESS on success,
* USBHST_INVALID_PARAMETER if the parameters are invalid
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbOhciProcessRootHubInterruptTransfer
    (
    UINT8          uHostControllerIndex,
    pUSBHST_URB    pUrb
    )
    {
    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA       pHCDData = NULL;

    /* Obtain the pointer to the host controller information */

    pHCDData = &g_OHCDData[uHostControllerIndex];

    /* Queue the status change interrupt request */

    pHCDData->pRootHubInterruptRequest = pUrb;

    /*
     * Enter the critical section
     *
     * NOTE: This is requried because the interrupt handler also checks
     *       for updating the status change request pending on the root
     *       hub.  For SMP, the called routine protects itself with a
     *       a semaphore.  Neither caller runs in interrupt context
     *       so a semaphore is legal.
     */
    /* Call the function to process the status change on the root hub */

    usbOhciProcessRootHubStatusChange (uHostControllerIndex);

    /* Return from the function */
    return USBHST_SUCCESS;

    } /* End of function usbOhciProcessRootHubInterruptTransfer () */

/***************************************************************************
*
* usbOhciProcessRootHubStandardRequest - processes the USB standard requests
*
* This function processes the USB standard requests addressed to the root hub
*
* PARAMETERS: <nHostControllerIndex (IN)> - Host controller index corresponding
* to the host controller for which an interrupt has occurred.
*
* <pURB (IN)> - Pointer to the URB.
*
* RETURNS: USBHST_SUCCESS on success,
* USBHST_INVALID_PARAMETER if the parameters are invalid
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbOhciProcessRootHubStandardRequest
    (
    UINT8        uHostControllerIndex,
    pUSBHST_URB  pUrb
    )
    {
    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA       pHCDData = NULL;

    /* To hold the contents of the SETUP packet */

    pUSBHST_SETUP_PACKET        pSetupPacket = NULL;

    /* To hold the value read from the registers */

    UINT32                      uRegisterValue = 0;

    /* Obtain the pointer to the host controller information */

    pHCDData = &g_OHCDData[uHostControllerIndex];

    /* Obtain the pointer to the SETUP packet */

    pSetupPacket = (pUSBHST_SETUP_PACKET) pUrb->pTransferSpecificData;

    /* Swap the contents of the setup data structure which has 16 bits */

    pSetupPacket->wValue = OS_UINT16_LE_TO_CPU(pSetupPacket->wValue);
    pSetupPacket->wIndex = OS_UINT16_LE_TO_CPU(pSetupPacket->wIndex);
    pSetupPacket->wLength = OS_UINT16_LE_TO_CPU(pSetupPacket->wLength);

    /* Check the request to be processed */

    switch (pSetupPacket->bRequest)
        {
        case USBHST_REQ_GET_STATUS: /* USB GET_STATUS request */

            /* Check the recipient of the request */

            switch (pSetupPacket->bmRequestType &
                    USB_OHCI_CONTROL_TRANSFER_REQUEST_RECIPIENT_MASK)
                {
                case USBHST_RECIPIENT_DEVICE: /* Device recipient */

                    /* Clear the URB transfer buffer */

                    OS_MEMSET (pUrb->pTransferBuffer,
                               0,
                               USB_OHCI_GET_STATUS_REQUEST_RESPONSE_SIZE);

                    /* Update the device status - Self powered */

                    pUrb->pTransferBuffer[0] = USB_OHCI_DEVICE_SELF_POWERED;

                    /* Update the device status - remote wakeup enabled */

                    if (pHCDData->bRemoteWakeupEnabled)
                        {
                        /* Remote wakeup is enabled */

                        pUrb->pTransferBuffer[0] |=
                            USB_OHCI_DEVICE_REMOTE_WAKEUP_ENABLED;
                        }

                    /* Update the URB transfer length */

                    pUrb->uTransferLength =
                        USB_OHCI_GET_STATUS_REQUEST_RESPONSE_SIZE;

                    /* Update the URB status */

                    pUrb->nStatus = USBHST_SUCCESS;

                    break;

                case USBHST_RECIPIENT_INTERFACE: /* Interface recipient */
                case USBHST_RECIPIENT_ENDPOINT:  /* Endpoint recipient */

                    /* Update the URB transfer buffer */

                    OS_MEMSET(pUrb->pTransferBuffer,
                                0,
                                USB_OHCI_GET_STATUS_REQUEST_RESPONSE_SIZE);

                    /* Update the URB transfer length */

                    pUrb->uTransferLength =
                        USB_OHCI_GET_STATUS_REQUEST_RESPONSE_SIZE;

                    /* Update the URB status */

                    pUrb->nStatus = USBHST_SUCCESS;

                    break;

                default:

                    /* Invalid device recipient */

                    pUrb->nStatus = USBHST_INVALID_REQUEST;

                    break;

                }

            break;

        case USBHST_REQ_CLEAR_FEATURE: /* USB CLEAR_FEATURE request */

            /* Check the recipient of the request */

            switch (pSetupPacket->bmRequestType &
                    USB_OHCI_CONTROL_TRANSFER_REQUEST_RECIPIENT_MASK)
                {
                case USBHST_RECIPIENT_DEVICE: /* Device recipient */

                    /* Check the feature selector */

                    if (pSetupPacket->wValue ==
                        USBHST_FEATURE_DEVICE_REMOTE_WAKEUP)
                        {
                        /* Disable device remote wakeup */

                        pHCDData->bRemoteWakeupEnabled = FALSE;

                        /*
                         * Clear the 'Remote Wakeup Enable' bit in the HcControl
                         * register (BEGIN)
                         */

                        /* Read the contents of the HcControl register */

                        uRegisterValue =
                            USB_OHCI_REG_READ(pHCDData->pDev,
                          USB_OHCI_CONTROL_REGISTER_OFFSET);

                        /* Clear the 'Remote Wakeup Enable (RWE)' bit */

                        uRegisterValue &= (~USB_OHCI_CONTROL_RWE);

                        /* Update the contents of the HcControl register */

                        USB_OHCI_REG_WRITE(pHCDData->pDev,
                                           USB_OHCI_CONTROL_REGISTER_OFFSET,
                              uRegisterValue);

                        /*
                         * Clear the 'Remote Wakeup Enable' bit in the HcControl
                         * register (END)
                         */

                        /*
                         * Set the 'Resume Detect' bit of the interrupt disable
                         * register
                         */

                        USB_OHCI_REG_WRITE(pHCDData->pDev,
                              USB_OHCI_INTERRUPT_DISABLE_REGISTER_OFFSET,
                              USB_OHCI_INTERRUPT_STATUS_RESUME_DETECT);
                        }

                    /* Update the URB status */

                    pUrb->nStatus = USBHST_SUCCESS;

                    break;

                case USBHST_RECIPIENT_INTERFACE: /* Interface recipient */
                case USBHST_RECIPIENT_ENDPOINT:  /* Endpoint recipient */

                    /* Update the URB status */

                    pUrb->nStatus = USBHST_SUCCESS;

                    break;

                default:

                    /* Invalid device recipient */
                    pUrb->nStatus = USBHST_INVALID_REQUEST;

                    break;

                }

            break;

        case USBHST_REQ_SET_FEATURE: /* USB SET_FEATURE request */

            /* Check the recipient of the request */

            switch (pSetupPacket->bmRequestType &
                USB_OHCI_CONTROL_TRANSFER_REQUEST_RECIPIENT_MASK)
                {
                case USBHST_RECIPIENT_DEVICE: /* Device recipient */

                    /* Check the feature selector */

                    if (pSetupPacket->wValue ==
                            USBHST_FEATURE_DEVICE_REMOTE_WAKEUP)
                        {
                        /* Enable device remote wakeup */

                        pHCDData->bRemoteWakeupEnabled = TRUE;

                        /*
                         * Set the 'Remote Wakeup Enable' bit in the HcControl
                         * register (BEGIN)
                         */

                        /* Read the contents of the HcControl register */

                        uRegisterValue =
                            USB_OHCI_REG_READ(pHCDData->pDev,
                           USB_OHCI_CONTROL_REGISTER_OFFSET);

                        /* Set the 'Remote Wakeup Enable (RWE)' bit */

                        uRegisterValue |= USB_OHCI_CONTROL_RWE;

                        /* Update the contents of the HcControl register */

                        USB_OHCI_REG_WRITE(pHCDData->pDev,
                                           USB_OHCI_CONTROL_REGISTER_OFFSET,
                              uRegisterValue);

                        /*
                         * Set the 'Remote Wakeup Enable' bit in the HcControl
                         * register (END)
                         */

                        /*
                         * Set the 'Resume Detect' bit of the interrupt enable
                         * register
                         */

                        USB_OHCI_REG_WRITE(pHCDData->pDev,
                           USB_OHCI_INTERRUPT_ENABLE_REGISTER_OFFSET,
                              USB_OHCI_INTERRUPT_STATUS_RESUME_DETECT);
                        }

                    /* Update the URB status */

                    pUrb->nStatus = USBHST_SUCCESS;

                    break;

                case USBHST_RECIPIENT_INTERFACE: /* Interface recipient */
                case USBHST_RECIPIENT_ENDPOINT:  /* Endpoint recipient */

                    /* Update the URB status */

                    pUrb->nStatus = USBHST_SUCCESS;

                    break;

                default:

                    /* Invalid device recipient */

                    pUrb->nStatus = USBHST_INVALID_REQUEST;

                    break;

                }

            break;

        case USBHST_REQ_SET_ADDRESS: /* USB SET_ADDRESS request */

            /* Check whether the address is valid */

            if (pSetupPacket->wValue == 0)
                {
                /* Invalid device address. Update the URB status */

                pUrb->nStatus = USBHST_INVALID_REQUEST;

                break;
                }

            /* Check the USB state of the root hub */

            if ((pHCDData->uRootHubState ==
                    USB_OHCI_DEVICE_DEFAULT_STATE) ||
                (pHCDData->uRootHubState ==
                    USB_OHCI_DEVICE_ADDRESS_STATE))
                {
                /* Update the root hub USB address */

                pHCDData->uRootHubAddress = (UINT8)pSetupPacket->wValue;

                /* Update the USB state of the root hub */
                pHCDData->uRootHubState =
                    USB_OHCI_DEVICE_ADDRESS_STATE;

                /* Update the URB status */
                pUrb->nStatus = USBHST_SUCCESS;
                }
            else
                {
                /* Invalid request. Update the URB status */
                pUrb->nStatus = USBHST_INVALID_REQUEST;
                }

            break;

        case USBHST_REQ_GET_DESCRIPTOR: /* USB GET_DESCRIPTOR request */

            /* Check the descriptor type */

            switch (pSetupPacket->wValue >> USB_OHCI_GET_DESCRIPTOR_TYPE_OFFSET)
                {
                case USBHST_DEVICE_DESC:

                    /* Check the length of descriptor requested */

                    if (pSetupPacket->wLength >=
                            USB_OHCI_ROOT_HUB_DEVICE_DESCRIPTOR_LENGTH)
                        {
                        /* Copy the descriptor to the URB transfer buffer */

                        OS_MEMCPY(pUrb->pTransferBuffer,
                                    (PUCHAR) (&usbOhciRootHubDeviceDescriptor),
                                    USB_OHCI_ROOT_HUB_DEVICE_DESCRIPTOR_LENGTH);

                        /* Update the URB transfer length */

                        pUrb->uTransferLength =
                            USB_OHCI_ROOT_HUB_DEVICE_DESCRIPTOR_LENGTH;
                        }
                    else
                        {
                        /* Copy the descriptor to the URB transfer buffer */

                        OS_MEMCPY(pUrb->pTransferBuffer,
                                    (PUCHAR) (&usbOhciRootHubDeviceDescriptor),
                                    pSetupPacket->wLength);

                        /* Update the URB transfer length */

                        pUrb->uTransferLength = pSetupPacket->wLength;
                        }

                    /* Update the URB status */

                    pUrb->nStatus = USBHST_SUCCESS;

                    break;

                case USBHST_CONFIG_DESC:

                    /* Check the length of descriptor requested */
                    if (pSetupPacket->wLength >=
                            USB_OHCI_ROOT_HUB_CONFIGURATION_DESCRIPTOR_LENGTH)
                        {
                        /* Copy the descriptor to the URB transfer buffer */
                        OS_MEMCPY(pUrb->pTransferBuffer,
                              (PUCHAR) (&usbOhciRootHubConfigurationDescriptor),
                              USB_OHCI_ROOT_HUB_CONFIGURATION_DESCRIPTOR_LENGTH);

                        /* Update the URB transfer length */
                        pUrb->uTransferLength =
                            USB_OHCI_ROOT_HUB_CONFIGURATION_DESCRIPTOR_LENGTH;
                        }
                    else
                        {
                        /* Copy the descriptor to the URB transfer buffer */
                        OS_MEMCPY(pUrb->pTransferBuffer,
                            (PUCHAR)(&usbOhciRootHubConfigurationDescriptor),
                              pSetupPacket->wLength);

                        /* Update the URB transfer length */
                        pUrb->uTransferLength = pSetupPacket->wLength;
                        }

                    /* Update the URB status */
                    pUrb->nStatus = USBHST_SUCCESS;

                    break;

                default:

                    /* Invalid descriptor type */
                    pUrb->nStatus = USBHST_INVALID_REQUEST;

                    break;

                }

            break;

        case USBHST_REQ_SET_DESCRIPTOR: /* USB SET_DESCRIPTOR request */

            /*
             * SET_DESCRIPTOR request is not support for the root hub.
             * Update the URB status.
             */

            pUrb->nStatus = USBHST_INVALID_REQUEST;

            break;

        case USBHST_REQ_GET_CONFIGURATION: /* USB GET_CONFIGURATION request */

            /*
             * Update the URB transfer buffer with the current configuration
             * value for the root hub
             */

            pUrb->pTransferBuffer[0] =
                pHCDData->bCurrentConfigurationValue;

            /* Update the URB transfer length */

            pUrb->uTransferLength =
                USB_OHCI_GET_CONFIGURATION_REQUEST_RESPONSE_SIZE;

            /* Update the URB status */

            pUrb->nStatus = USBHST_SUCCESS;

            break;

        case USBHST_REQ_SET_CONFIGURATION: /* USB SET_CONFIGURATION request */

            /* Check whether the configuration value specified is valid */

            if ((pSetupPacket->wValue != 0) && (pSetupPacket->wValue != 1))
                {
                /* Invalid configuration value. Update the URB status */
                pUrb->nStatus = USBHST_INVALID_REQUEST;

                break;
                }

            /* Update the current configuration value for the root hub */

            pHCDData->bCurrentConfigurationValue =
                (UINT8) pSetupPacket->wValue;

            /*
             * Check the current configuration value and update the root hub
             * device state
             */

            if (pHCDData->bCurrentConfigurationValue == 0)
                {
                /* Update the root hub device state to addressed */
                pHCDData->uRootHubState =
                    USB_OHCI_DEVICE_ADDRESS_STATE;
                }
            else if (pHCDData->bCurrentConfigurationValue == 1)
                {
                /* Update the root hub device state to configured */
                pHCDData->uRootHubState =
                    USB_OHCI_DEVICE_CONFIGURED_STATE;
                }

            /* Update the URB status */

            pUrb->nStatus = USBHST_SUCCESS;

            break;

        case USBHST_REQ_GET_INTERFACE: /* USB GET_INTERFACE request */

            /* Check the interface number */

            if (pSetupPacket->wIndex != 0)
                {
                /* Invalid interface number. Update the URB status */

                pUrb->nStatus = USBHST_INVALID_REQUEST;

                break;
                }

            /*
             * Update the URB transfer buffer with the current alternate setting
             * value for interface 0
             */
            pUrb->pTransferBuffer[0] = 0;

            /* Update the URB transfer length */

            pUrb->uTransferLength =
                USB_OHCI_GET_INTERFACE_REQUEST_RESPONSE_SIZE;

            /* Update the URB status */

            pUrb->nStatus = USBHST_SUCCESS;

            break;

        case USBHST_REQ_SET_INTERFACE: /* USB SET_INTERFACE request */

            /*
             * Alternate setting for the root hub interface is not supported.
             * Update the URB status.
             */

            pUrb->nStatus = USBHST_INVALID_REQUEST;

            break;

        case USBHST_REQ_SYNCH_FRAME: /* USB SYNCH_FRAME request */

            /*
             * SYNCH_FRAME request is not supported on the root hub.
             * Update the URB status.
             */

            pUrb->nStatus = USBHST_INVALID_REQUEST;

            break;

        }

    /* Swap back the contents of the setup data structure which has 16 bits */

    pSetupPacket->wValue = OS_UINT16_CPU_TO_LE(pSetupPacket->wValue);
    pSetupPacket->wIndex = OS_UINT16_CPU_TO_LE(pSetupPacket->wIndex);
    pSetupPacket->wLength = OS_UINT16_CPU_TO_LE(pSetupPacket->wLength);

    return USBHST_SUCCESS;

    } /* End of function usbOhciProcessRootHubStandardRequest () */

/***************************************************************************
*
* usbOhciProcessRootHubClassSpecificRequest - processes the hub class requests
*
* This function processes the hub class specific requests addressed to the
* root hub
*
* PARAMETERS: <nHostControllerIndex (IN)> - Host controller index corresponding
* to the host controller for which an interrupt has occurred.
*
* <pURB (IN)> - Pointer to the URB.
*
* RETURNS: USBHST_SUCCESS on success,
* USBHST_INVALID_PARAMETER if the parameters are invalid
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbOhciProcessRootHubClassSpecificRequest
    (
    UINT8           uHostControllerIndex,
    pUSBHST_URB     pUrb
    )
    {
    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA       pHCDData = NULL;

    /* To hold the contents of the SETUP packet */

    pUSBHST_SETUP_PACKET        pSetupPacket = NULL;

    /* To hold the offset of the root hub port status register */

    UINT8                       uRootHubPortStatusRegisterOffset = 0;

    /* To hold the value read from the registers */

    UINT32                      uRegisterValue = 0;

    /* To hold the length of the hub descriptor */

    UINT16                      uHubDescriptorLength = 0;

    /* To hold the hub descriptor */

    PUSB_OHCI_HUB_CLASS_DESCRIPTOR    pHubDescriptor = NULL;

    /*
     * To hold the number of bytes required to hold the 'DeviceRemovable'
     * and 'PortPwrCtrlMask' information of the device descriptor.
     */

    UINT8                         uNumberOfBytes = 0;

    /* Obtain the pointer to the host controller information */

    pHCDData = &g_OHCDData[uHostControllerIndex];

    /* Obtain the pointer to the SETUP packet */

    pSetupPacket = (pUSBHST_SETUP_PACKET) pUrb->pTransferSpecificData;

    /* Swap the contents of the setup data structure which has 16 bits */

    pSetupPacket->wValue = OS_UINT16_LE_TO_CPU(pSetupPacket->wValue);
    pSetupPacket->wIndex = OS_UINT16_LE_TO_CPU(pSetupPacket->wIndex);
    pSetupPacket->wLength = OS_UINT16_LE_TO_CPU(pSetupPacket->wLength);

    /* Check the request to be processed */

    switch (pSetupPacket->bRequest)
        {
        case USBHST_REQ_CLEAR_FEATURE:

            /* Check the recipient of the request */
            switch (pSetupPacket->bmRequestType &
                    USB_OHCI_CONTROL_TRANSFER_REQUEST_RECIPIENT_MASK)
                {
                case USBHST_RECIPIENT_DEVICE: /* ClearHubFeature() request */

                    /* Check the feature selector for the request */
                    switch (pSetupPacket->wValue)
                        {
                        case USB_OHCI_C_HUB_LOCAL_POWER:
                            /* Clear change in hub local power */

                            /* NOTE: This feature is not supported on the
                             * root hub
                             */

                            /* Update the URB status */
                            pUrb->nStatus = USBHST_INVALID_REQUEST;

                            break;

                        case USB_OHCI_C_HUB_OVER_CURRENT:
                            /* Clear change in over current indicator */

                            /*
                             * Update the contents of the HcRhStatus
                             * register
                             */

                            USB_OHCI_REG_WRITE(pHCDData->pDev,
                                    USB_OHCI_RH_STATUS_REGISTER_OFFSET,
                                    USB_OHCI_RH_STATUS_OCIC);

                            /* Update the URB status */
                            pUrb->nStatus = USBHST_SUCCESS;

                            break;

                        default:

                            /* Invalid feature selector. Update URB status */
                            pUrb->nStatus = USBHST_INVALID_REQUEST;

                            break;

                        }/* End of switch (pSetupPacket->wValue) */

                    break;

                case USBHST_RECIPIENT_OTHER: /* ClearPortFeature() request */

                    /* Check the port number */
                    if (pSetupPacket->wIndex >
                        pHCDData->uNumberOfDownStreamPorts)
                        {
                        /* Invalid port index. Update the URB status. */
                        pUrb->nStatus = USBHST_INVALID_REQUEST;

                        break;
                        }

                    /* Obtain the offset of the root hub port status register */
                    uRootHubPortStatusRegisterOffset =
                        (UINT8)USB_OHCI_RH_PORT_STATUS_REGISTER_OFFSET(
                            pSetupPacket->wIndex);

                    /*
                      * NOTE: Do not Read -> OR the value -> Update the register.
                      *       This is because the register is write 1 to clear or
                      *       set.
                      *
                      *       Further, writing a 1 to some fields will cause side
                      *       effects. Refer to the OHCI specification for more
                      *       information.
                      */

                    /* Check whether the feature selector for the request */
                    switch (pSetupPacket->wValue)
                        {
                        case USB_OHCI_PORT_ENABLE:
                            /* Clear the port enable state */

                            /*
                             * NOTE: Write 1 to the Current Connect Status (CCS)
                             * bit to clear the port enable state.
                             */

                            /* Clear the port enable state */
                            USB_OHCI_REG_WRITE(pHCDData->pDev,
                                    uRootHubPortStatusRegisterOffset,
                                    USB_OHCI_RH_PORT_STATUS_CCS);

                            /* Update the URB status */
                            pUrb->nStatus = USBHST_SUCCESS;

                            break;

                        case USB_OHCI_PORT_SUSPEND:
                            /* Clear the port suspend state */

                            /*
                             * NOTE: Write 1 to the Port Over Current Indicator
                             * (POCI) bit to clear the port suspend state.
                             */

                            /* Clear the port suspend state */
                            USB_OHCI_REG_WRITE(pHCDData->pDev,
                                    uRootHubPortStatusRegisterOffset,
                                    USB_OHCI_RH_PORT_STATUS_POCI);

                            /* Update the URB status */
                            pUrb->nStatus = USBHST_SUCCESS;

                            break;

                        case USB_OHCI_PORT_POWER:

                            /*
                             * NOTE: If the root hub supports ganged power on
                             *       the down stream ports, power off all the
                             *       downstream ports. Else power off the
                             *         specified port.
                             */

                            /*
                             * Read the contents of the root hub descriptor A
                             * register
                             */
                            uRegisterValue =
                                USB_OHCI_REG_READ(pHCDData->pDev,
                                USB_OHCI_RH_DESCRIPTOR_A_REGISTER_OFFSET);


                            /*
                             * Check whether the root hub supports ganged power
                             * on the down stream ports.
                             */

                            if ((uRegisterValue &
                                 USB_OHCI_RH_DESCRIPTOR_A_REGISTER__PSM_MASK)
                                     == 0)
                                {
                                /*
                                 * NOTE: Write 1 to the Local Power Status
                                 *       (LPS) bit to clear the port power
                                 *         state.
                                 */

                                /*
                                 * Root hub supports ganged power. Power off
                                 * all the down stream ports
                                 */
                                USB_OHCI_REG_WRITE(pHCDData->pDev,
                                        USB_OHCI_RH_STATUS_REGISTER_OFFSET,
                                        USB_OHCI_RH_STATUS__LPS);
                                }
                            else
                                {
                                /*
                                 * Read the contents of the root hub descriptor B
                                 * register
                                 */
                                uRegisterValue =
                                    USB_OHCI_REG_READ(pHCDData->pDev,
                                        USB_OHCI_RH_DESCRIPTOR_B_REGISTER_OFFSET);


                                /*
                                 * Check whether the Port Power Control Mask bit
                                 * is set for the specified port.
                                 *
                                 * a) If this bit is set, the port's power state
                                 *    is only affected by per-port power control
                                 *    (Set/ClearPortPower).
                                 * b) If this bit is cleared, th port is
                                 *    controlled by the global power switch
                                 *    (Set/ClearGlobalPower).
                                 */
                                if (0 ==
                                    (uRegisterValue &
                                        USB_OHCI_HC_RH_DESCRIPTOR_B_REGISTER__PPCM_MASK))
                                    {
                                    /*
                                     * Port power state is controlled by the
                                     * global power switch
                                     */

                                    /*
                                     * NOTE: Write 1 to the Local Power Status (LPS)
                                     *       bit to clear the port power state.
                                     */

                                    /*
                                     * Root hub supports ganged power. Power off
                                     * all the down stream ports
                                     */
                                    USB_OHCI_REG_WRITE(pHCDData->pDev,
                                            USB_OHCI_RH_STATUS_REGISTER_OFFSET,
                                        USB_OHCI_RH_STATUS__LPS);

                                }
                            else
                                {
                                /*
                                 * NOTE: Write 1 to the Low Speed Device Attach
                                 * (LSDA) bit to clear the port power state.
                                 */

                                /* Clear the port power state */

                                USB_OHCI_REG_WRITE(
                                                pHCDData->pDev,
                                                uRootHubPortStatusRegisterOffset,
                                                USB_OHCI_RH_PORT_STATUS_LSDA);
                                }
                            }

                            /* Update the URB status */

                            pUrb->nStatus = USBHST_SUCCESS;

                            break;

                        case USB_OHCI_C_PORT_CONNECTION:

                            /*
                             * NOTE: Write 1 to the connect status change (CSC)
                             * bit to clear the connect status change.
                             */

                            /* Clear the connect status change */

                            USB_OHCI_REG_WRITE(pHCDData->pDev,
                                               uRootHubPortStatusRegisterOffset,
                                               USB_OHCI_RH_PORT_STATUS_CSC);

                            /* Update the URB status */

                            pUrb->nStatus = USBHST_SUCCESS;

                            break;

                        case USB_OHCI_C_PORT_RESET:

                            /*
                             * NOTE: Write 1 to the port reset status change
                             * (PRSC) bit to clear the port reset status change.
                             */

                            /* Clear the port reset status change */

                            USB_OHCI_REG_WRITE(pHCDData->pDev,
                                               uRootHubPortStatusRegisterOffset,
                                               USB_OHCI_RH_PORT_STATUS_PRSC);

                            /* Update the URB status */

                            pUrb->nStatus = USBHST_SUCCESS;

                            break;

                        case USB_OHCI_C_PORT_ENABLE:

                            /*
                             * NOTE: Write 1 to the port enable status change
                             * (PESC) bit to clear port enable status change.
                             */

                            /* Clear the port enable status change */

                            USB_OHCI_REG_WRITE(pHCDData->pDev,
                                               uRootHubPortStatusRegisterOffset,
                                               USB_OHCI_RH_PORT_STATUS_PESC);

                            /* Update the URB status */

                            pUrb->nStatus = USBHST_SUCCESS;

                            break;

                        case USB_OHCI_C_PORT_SUSPEND:

                            /*
                             * NOTE: Write 1 to the port suspend status change
                             * bit to clear the port suspend status change.
                             */

                            /* Clear the port suspend status change */

                            USB_OHCI_REG_WRITE(pHCDData->pDev,
                                               uRootHubPortStatusRegisterOffset,
                                               USB_OHCI_RH_PORT_STATUS_PSSC);

                            /* Update the URB status */

                            pUrb->nStatus = USBHST_SUCCESS;

                            break;

                        case USB_OHCI_C_PORT_OVER_CURRENT:

                            /*
                             * NOTE: Write 1 to the over current indicator
                             * change (OCIC) bit to clear the port power state.
                             */

                            /* Clear the over current indicator change */

                            USB_OHCI_REG_WRITE(pHCDData->pDev,
                                               uRootHubPortStatusRegisterOffset,
                                               USB_OHCI_RH_PORT_STATUS_OCIC);

                            /* Update the URB status */

                            pUrb->nStatus = USBHST_SUCCESS;

                            break;

                        default:

                            /* Invalid request. Update the URB status */

                            pUrb->nStatus = USBHST_INVALID_REQUEST;

                        break;

                        } /* End of switch (pSetupPacket->wValue) */

                    break;

                default:

                    /* Invalid request. Update the URB Status */

                    pUrb->nStatus = USBHST_INVALID_REQUEST;

                    break;

                } /* End of switch (pSetupPacket->bmRequestType & ... ) */

            break;

        case USBHST_REQ_GET_DESCRIPTOR:

            /*
             * Check whether the descriptor type and decriptor index is valid.
             *
             * NOTE: For the root hub, only one hub descriptor is supported.
             *       Further, the descriptor type and descriptor index should
             *       be zero.
             */

            if (pSetupPacket->wValue != 0x2900)
                {
                /* Invalid descriptor type or index */

                pUrb->nStatus = USBHST_STALL_ERROR;

                break;
                }

            /*
             * Compute the number of bytes required to hold the
             * 'DeviceRemovable' and 'PortPwrCtrlMask' information of the device
             * descriptor.
             *
             * NOTE: HUB_CLASS_DESCRIPTOR structure can hold the above two
             *       information for maximum of 8 ports. If the number of ports
             *       on the root hub is greater than 8, additional memory
             *       should be allocated for the HUB_CLASS_DESCRIPTOR.
             */

            /*
             * Compute the number of bytes to copy based on the number of
             * ports supported on the root hub.
             */

            uNumberOfBytes = pHCDData->uNumberOfDownStreamPorts / 8;

            /* Check for remainder of the bytes */

            if ((pHCDData->uNumberOfDownStreamPorts % 8) != 0)
                {
                /* Increment the number of bytes to be copied */
                uNumberOfBytes++;
                }

            /*
             * We have two array of information, 'DeviceRemovable' and
             * 'PortPwrCtrlMask' fields, so we need to multiply by 2.
             */

            uNumberOfBytes = (UINT8)(uNumberOfBytes * 2);

            /*
             * Compute the length of the hub descriptor.
             *
             * NOTE: The 'DeviceRemovable' and 'PortPwrCtrlMask' fields are
             *       of variable length based on the number of ports. Further,
             *       these fields are rounded off to the byte granularity.
             */

            uHubDescriptorLength =
                (UINT16)(USB_OHCI_HUB_CLASS_DESCR_BASIC_SIZE + uNumberOfBytes);

            /* Allocate memory for the hub class descriptor */

            pHubDescriptor =
                (PUSB_OHCI_HUB_CLASS_DESCRIPTOR)OS_MALLOC(uHubDescriptorLength);

            /* Check whether the memory was successfully allocated */

            if (pHubDescriptor == NULL)
                {
                /* Update the status of the URB */
                pUrb->nStatus = USBHST_STALL_ERROR;

                break;
                }

            /* Populate the hub class descriptor (BEGIN) */

            /* Populate the descriptor length */

            pHubDescriptor->bDescLength = (UINT8)uHubDescriptorLength;

            /* Populate the descriptor type */

            pHubDescriptor->bDescriptorType =
                USB_OHCI_HUB_CLASS_DESCRIPTOR_TYPE;

            /* Populate the number of ports */

            pHubDescriptor->bNbrPorts =
                pHCDData->uNumberOfDownStreamPorts;

            /*
             * Populate the hub characteristics.
             *
             * NOTE: The hub characteristics of the OHCI host controller
             *       can be obtained from bits 8 - 12 of the HcRhDescriptorA
             *       register.
             */

            /* Read the contents of the HcRhDescriptorA register */

            uRegisterValue =
                USB_OHCI_REG_READ(pHCDData->pDev,
                  USB_OHCI_RH_DESCRIPTOR_A_REGISTER_OFFSET);


            /* Populate the hub characteristics information */

            pHubDescriptor->wHubCharacteristics =
                (UINT16) ((uRegisterValue >> 8) & 0x1F);

            /*
             * Populate the power on to power good information
             *
             * NOTE: The power on to power good information of the OHCI host
             *       controller can be obtained from bits 24 - 31 of the
             *       HcRhDescriptorA register.
             */

            pHubDescriptor->bPwrOn2PwrGood = (UINT8) ((uRegisterValue >> 24)
                                                            & 0xFF);

            /*
             * Populate the device removable information.
             *
             * NOTE: The device removable information of OHCI host controller
             *       can be obtained from bits 0 - 15 of the HcRhDescriptorB
             *       register.
             */

            /* Read the contents of the HcRhDescriptorB register */

            uRegisterValue =
                USB_OHCI_REG_READ(pHCDData->pDev,
                  USB_OHCI_RH_DESCRIPTOR_B_REGISTER_OFFSET);

            /*
             * Check whether the root hub has more than 8 downstream ports
             *
             * NOTE: As per HcRhDescriptorB register definition, root hub
             *       supports a maximum of 16 ports.
             */

            if (pHCDData->uNumberOfDownStreamPorts > 8)
                {
                /* Populate the device removable information */

                pHubDescriptor->pPortInfoBytes[0] =
                    (UINT8)(uRegisterValue & 0xFF);
                pHubDescriptor->pPortInfoBytes[1] =
                    (UINT8)((uRegisterValue >> 8) & 0xFF);

                /* Populate the port power control mask information */

                pHubDescriptor->pPortInfoBytes[2] =
                    (UINT8) ((uRegisterValue >> 16) & 0xFF);
                pHubDescriptor->pPortInfoBytes[3] =
                    (UINT8) ((uRegisterValue >> 24) & 0xFF);
                }
            else
                {
                /* Populate the device removable information */

                pHubDescriptor->pPortInfoBytes[0] =
                    (UINT8)(uRegisterValue & 0xFF);

                /* Populate the port power control mask information */

                pHubDescriptor->pPortInfoBytes[1] =
                    (UINT8) ((uRegisterValue >> 16) & 0xFF);

                }

            /* Check the number of bytes of hub descriptor requested */

            if (pSetupPacket->wLength >= uHubDescriptorLength)
                {
                /* Copy the descriptor to the URB transfer buffer */

                OS_MEMCPY(pUrb->pTransferBuffer,
                            (PUCHAR) pHubDescriptor,
                            uHubDescriptorLength);

                /* Update the URB transfer length */

                pUrb->uTransferLength = uHubDescriptorLength;
                }
            else
                {
                /* Copy the descriptor to the URB transfer buffer */

                OS_MEMCPY(pUrb->pTransferBuffer,
                            (PUCHAR) (pHubDescriptor),
                            pSetupPacket->wLength);

                /* Update the URB transfer length */

                pUrb->uTransferLength = pSetupPacket->wLength;
                }

            /* Update the URB status */

            pUrb->nStatus = USBHST_SUCCESS;

            /* Release the memory allocated for the hub descriptor */

            OS_FREE(pHubDescriptor);
            pHubDescriptor = NULL;

            break;

        case USBHST_REQ_GET_STATUS:

            /* Check the recipient of the request */

            switch (pSetupPacket->bmRequestType &
                        USB_OHCI_CONTROL_TRANSFER_REQUEST_RECIPIENT_MASK)
                {
                case USBHST_RECIPIENT_DEVICE: /* GetHubStatus() request */

                    /* Read the contents of the HcRhStatus register */

                    uRegisterValue =
                       USB_OHCI_REG_READ(pHCDData->pDev,
                    USB_OHCI_RH_STATUS_REGISTER_OFFSET);


                    /* INTEGRATION TESTING CHANGE - start */
                    /* Swap the value read from the register */

                    uRegisterValue = OS_UINT32_CPU_TO_LE(uRegisterValue);

                    /* INTEGRATION TESTING CHANGE - End */

                    /* Update the URB transfer buffer */

                    OS_MEMCPY(pUrb->pTransferBuffer,
                                (PUCHAR) (&uRegisterValue),
                                USB_OHCI_GET_HUB_STATUS_REQUEST_RESPONSE_SIZE);

                    /* Update the URB transfer length */

                    pUrb->uTransferLength =
                        USB_OHCI_GET_HUB_STATUS_REQUEST_RESPONSE_SIZE;

                    /* Update the URB status */

                    pUrb->nStatus = USBHST_SUCCESS;

                    break;

                case USBHST_RECIPIENT_OTHER: /* GetPortStatus() request */

                    /* Check the port number */

                    if (pSetupPacket->wIndex >
                            pHCDData->uNumberOfDownStreamPorts)
                        {
                        /* Invalid port index. Update the URB status. */
                        pUrb->nStatus = USBHST_INVALID_REQUEST;

                        break;
                        }

                    /* Obtain the offset of the root hub port status register */

                    uRootHubPortStatusRegisterOffset =
                        (UINT8)USB_OHCI_RH_PORT_STATUS_REGISTER_OFFSET(
                            pSetupPacket->wIndex);

                    /* Read the contents of the HcRhPortStatus register */
                    uRegisterValue =
                        USB_OHCI_REG_READ(pHCDData->pDev,
                           uRootHubPortStatusRegisterOffset);


                    /* INTEGRATION TESTING CHANGE - Start */
                    /* Swap the port status bytes */

                    uRegisterValue = OS_UINT32_CPU_TO_LE(uRegisterValue);

                    /* INTEGRATION TESTING CHANGE - End */

                    /* Update the URB transfer buffer */

                    OS_MEMCPY(pUrb->pTransferBuffer,
                                (PUCHAR) (&uRegisterValue),
                                USB_OHCI_GET_PORT_STATUS_REQUEST_RESPONSE_SIZE);

                    /* Update the URB transfer length */

                    pUrb->uTransferLength =
                        USB_OHCI_GET_PORT_STATUS_REQUEST_RESPONSE_SIZE;

                    /* Update the URB status */

                    pUrb->nStatus = USBHST_SUCCESS;

                    break;

                default:

                    /* Invalid request. Update the URB Status */

                    pUrb->nStatus = USBHST_INVALID_REQUEST;

                    break;

                } /* End of switch (pSetupPacket->bmRequestType & ... ) */

            break;

        case USBHST_REQ_SET_DESCRIPTOR:

            /*
             * SET_DESCRIPTOR request is not support for the root hub.
             * Update the URB status.
             */

            pUrb->nStatus = USBHST_INVALID_REQUEST;

            break;

        case USBHST_REQ_SET_FEATURE:

            /* Check the recipient of the request */

            switch (pSetupPacket->bmRequestType &
                    USB_OHCI_CONTROL_TRANSFER_REQUEST_RECIPIENT_MASK)
                {
                case USBHST_RECIPIENT_DEVICE: /* SetHubFeature() request */

                    /* Check the feature selector for the request */

                    switch (pSetupPacket->wValue)
                        {
                        case USB_OHCI_C_HUB_LOCAL_POWER:
                        case USB_OHCI_C_HUB_OVER_CURRENT:

                            /* NOTE: These features are not supported on the
                             * root hub
                             */

                            /* Update the URB status */

                            pUrb->nStatus = USBHST_INVALID_REQUEST;

                            break;

                        default:

                            /* Invalid feature selector. Update URB status */

                            pUrb->nStatus = USBHST_INVALID_REQUEST;

                            break;

                        } /* End of switch (pSetupPacket->wValue) */

                    break;

                case USBHST_RECIPIENT_OTHER: /* SetPortFeature() request */

                    /* Check the port number */

                    if (pSetupPacket->wIndex >
                        pHCDData->uNumberOfDownStreamPorts)
                        {
                        /* Invalid port index. Update the URB status. */

                        pUrb->nStatus = USBHST_INVALID_REQUEST;

                        break;
                        }

                    /* Obtain the offset of the root hub port status register */

                    uRootHubPortStatusRegisterOffset =
                        (UINT8)USB_OHCI_RH_PORT_STATUS_REGISTER_OFFSET(
                            pSetupPacket->wIndex);

                    /* Check whether the feature selector for the request */

                    switch (pSetupPacket->wValue)
                        {
                        case USB_OHCI_PORT_RESET:

                            /*
                             * NOTE: Write 1 to the port reset status (PRS) bit
                             * to set the port reset status.
                             */

                            /* Set the port reset status */

                            USB_OHCI_REG_WRITE(pHCDData->pDev,
                                               uRootHubPortStatusRegisterOffset,
                                               USB_OHCI_RH_PORT_STATUS_PRS);

                            /*
                             * The reset signal should be driven atleast for
                             * 50ms from the Root hub.
                             *
                             * Additional 10ms is given here for software
                             * delays.
                             */

                            /*
                             * 7.1.7.5 Reset Signaling
                             *
                             * A hub signals reset to a downstream port by
                             * driving an extended SE0 at the port. After the
                             * reset is removed, the device will be in the
                             * Default state (refer to Section 9.1).
                             *
                             * The reset signaling can be generated on any Hub
                             * or Host Controller port by request from the USB
                             * System Software. The reset signaling must be
                             * driven for a minimum of 10ms (TDRST). After the
                             * reset, the hub port will transition to the
                             * Enabled state (refer to Section 11.5).
                             *
                             * As an additional requirement, Host Controllers
                             * and the USB System Software must ensure that
                             * resets issued to the root ports drive reset
                             * long enough to overwhelm any concurrent resume
                             * attempts by downstream devices. It is required
                             * that resets from root ports have a duration
                             * of at least 50 ms (TDRSTR). It is not required
                             * that this be 50 ms of continuous Reset signaling.
                             * However, if the reset is not continuous, the
                             * interval(s) between reset signaling must be less
                             * than 3 ms (TRHRSI), and the duration of each
                             * SE0 assertion must be at least 10 ms (TDRST).
                             */

                            OS_DELAY_MS(60);

                            /* Update the URB status */

                            pUrb->nStatus = USBHST_SUCCESS;

                            break;

                        case USB_OHCI_PORT_SUSPEND:

                            /*
                             * NOTE: Write 1 to the port suspend status (PSS)
                             * bit to set the port suspend status.
                             */

                            /* Set the port suspend status */
                            USB_OHCI_REG_WRITE(pHCDData->pDev,
                                               uRootHubPortStatusRegisterOffset,
                                               USB_OHCI_RH_PORT_STATUS_PSS);

                            /* Update the URB status */
                            pUrb->nStatus = USBHST_SUCCESS;

                            break;

                        case USB_OHCI_PORT_POWER:
                            /*
                             * NOTE: If the root hub supports ganged power on
                             *       the down stream ports, power on all the
                             *       downstream ports. Else power on the
                             *         specified port.
                             */

                            /*
                             * Read the contents of the root hub descriptor A
                             * register
                             */

                            uRegisterValue =
                                USB_OHCI_REG_READ(pHCDData->pDev,
                                USB_OHCI_RH_DESCRIPTOR_A_REGISTER_OFFSET);

                            /*
                             * Check whether the root hub supports ganged power
                             * on the down stream ports.
                             */

                            if ((uRegisterValue &
                                 USB_OHCI_RH_DESCRIPTOR_A_REGISTER__PSM_MASK)
                                     == 0)
                                {
                                /*
                                 * NOTE: Write 1 to the Local Power Status
                                 *       Change (LPSC) bit to set the port power
                                 *         state.
                                 */

                                /*
                                 * Root hub supports ganged power. Power on all
                                 * the down stream ports
                                 */

                                USB_OHCI_REG_WRITE(pHCDData->pDev,
                                             USB_OHCI_RH_STATUS_REGISTER_OFFSET,
                                             USB_OHCI_RH_STATUS_LPSC);
                                }
                            else
                                {
                                /*
                                 * Read the contents of the root hub descriptor B
                                 * register
                                 */
                                uRegisterValue =
                                    USB_OHCI_REG_READ(pHCDData->pDev,
                                     USB_OHCI_RH_DESCRIPTOR_B_REGISTER_OFFSET);


                                /*
                                 * Check whether the Port Power Control Mask bit
                                 * is set for the specified port.
                                 *
                                 * a) If this bit is set, the port's power state
                                 *    is only affected by per-port power control
                                 *    (Set/ClearPortPower).
                                 * b) If this bit is cleared, th port is controlled
                                 *    by the global power switch
                                 *    (Set/ClearGlobalPower).
                                 */
                                if (0 == (uRegisterValue &
                                    USB_OHCI_HC_RH_DESCRIPTOR_B_REGISTER__PPCM_MASK))
                                    {
                                    /*
                                     * Port power state is controlled by the
                                     * global power switch
                                     */

                                    /*
                                     * NOTE: Write 1 to the Local Power Status Change
                                     *       (LPSC) bit to set the port power state.
                                     */

                                    /*
                                     * Root hub supports ganged power. Power on all
                                     * the down stream ports
                                     */
                                    USB_OHCI_REG_WRITE(pHCDData->pDev,
                                            USB_OHCI_RH_STATUS_REGISTER_OFFSET,
                                        USB_OHCI_RH_STATUS_LPSC);
                                    }
                                else
                                    {

                                    /*
                                     * NOTE: Write 1 to the port power status (PPS)
                                     * bit to set the port power status.
                                     */

                                    /* Set the port power status */
                                    USB_OHCI_REG_WRITE(pHCDData->pDev,
                                                    uRootHubPortStatusRegisterOffset,
                                                    USB_OHCI_RH_PORT_STATUS_PPS);
                                    }
                                }

                            /* Update the URB status */
                            pUrb->nStatus = USBHST_SUCCESS;

                            break;

                        case USB_OHCI_C_PORT_CONNECTION:
                        case USB_OHCI_C_PORT_RESET:
                        case USB_OHCI_C_PORT_ENABLE:
                        case USB_OHCI_C_PORT_SUSPEND:
                        case USB_OHCI_C_PORT_OVER_CURRENT:

                            /*
                             * NOTE: These features are not supported on the
                             * root hub.
                             */

                            /* Update the URB status */
                            pUrb->nStatus = USBHST_INVALID_REQUEST;

                            break;

                        default:

                            break;

                        } /* End of switch (pSetupPacket->wValue) */

                    break;

                default:

                    /* Invalid request. Update the URB Status */
                    pUrb->nStatus = USBHST_INVALID_REQUEST;

                    break;

                } /* End of switch (pSetupPacket->bmRequestType & ... ) */

            break;

        default:

            /*
             * Invalid request. Update the URB status.
             *
             * NOTE: GET_STATE request is not support for the root hub.
             */
            pUrb->nStatus = USBHST_INVALID_REQUEST;

            break;

        }/* End of switch (pSetupPacket->bRequest) */

    /* Swap back the contents of the setup data structure which has 16 bits */

    pSetupPacket->wValue = OS_UINT16_CPU_TO_LE(pSetupPacket->wValue);
    pSetupPacket->wIndex = OS_UINT16_CPU_TO_LE(pSetupPacket->wIndex);
    pSetupPacket->wLength = OS_UINT16_CPU_TO_LE(pSetupPacket->wLength);

    return USBHST_SUCCESS;

    } /* End of function usbOhciProcessRootHubClassSpecificRequest () */

/***************************************************************************
*
* usbOhciProcessRootHubStatusChange - processes root hub status change
*
* This function processes the status change on the root hub.
*
* PARAMETERS: <nHostControllerIndex (IN)> - Host controller index corresponding
* to the host controller for which an interrupt has occurred.
*
* RETURNS: N/A
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/

LOCAL VOID usbOhciProcessRootHubStatusChange
    (
    UINT8   uHostControllerIndex
    )
    {
    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA       pHCDData = NULL;

    /* To hold the offset of the root hub port status register */

    UINT8                       uRootHubPortStatusRegisterOffset = 0;


    /* To hold the value read from the registers */

    UINT32                      uRegisterValue = 0;

    /* To hold the status change bitmap for the root hub */

    UINT32                      uStatusChangeBitmap = 0;

    /* To hold the port number */

    UINT8                       uPortNumber = 0;

    /* To hold the pointer to the status change interrupt request */

    pUSBHST_URB                 pStatusChangeRequest = 0;


    /* Obtain the pointer to the host controller information */

    pHCDData = &g_OHCDData[uHostControllerIndex];


    /*
     * This routine could be invoked by the OHCI ISR task (not ISR context) as well
     * as programmatically.  This semaphore protects this routine from concurrent
     * updates
     */

    OS_WAIT_FOR_EVENT (pHCDData->ohciCommonMutex, WAIT_FOREVER);

    /* Check whether the status change request is pending for the root hub */

    if (pHCDData->pRootHubInterruptRequest == NULL)
        {
        /*
         * Since the status change request is not pending, return. The status
         * will be handled when the hub class driver submits the status
         * change request.
         */

        USB_OHCD_VDBG("the status change request is not pending " \
                      "for the root hub \n",
                      1, 2, 3, 4, 5, 6);

        OS_RELEASE_EVENT(pHCDData->ohciCommonMutex);
        return;
        }

    /* Read the contents of the HcRhStatus register */

    uRegisterValue = USB_OHCI_REG_READ(pHCDData->pDev,
                   USB_OHCI_RH_STATUS_REGISTER_OFFSET);

    /* Check whether a change is pending on the root hub */

    if ((uRegisterValue & USB_OHCI_RH_STATUS_CHANGE_MASK) != 0)
        {
        /*
         * Update the status change bitmap to indicate a change on the
         * root hub.
         */
        uStatusChangeBitmap = 1;
        }

    /*
     * For every port on the root hub, check whether a status change is pending
     * on the root hub port.
     */

    for (uPortNumber = 1;
         uPortNumber <= pHCDData->uNumberOfDownStreamPorts;
         uPortNumber++)
        {
        /* Obtain the offset of the root hub port status register */
        uRootHubPortStatusRegisterOffset =
            (UINT8)USB_OHCI_RH_PORT_STATUS_REGISTER_OFFSET(uPortNumber);

        /* Read the contents of the HcRhPortStatus register */
        uRegisterValue =
            USB_OHCI_REG_READ(pHCDData->pDev,
                    uRootHubPortStatusRegisterOffset);

        /* Check whether a status change is pending on the root hub port */

        if ((uRegisterValue & USB_OHCI_RH_PORT_STATUS_CHANGE_MASK) != 0)
            {
            /*
             * Update the status change bitmap to indicate a change on
             * the root hub port.
             */
            uStatusChangeBitmap |= (0x01 << uPortNumber);
            }
        }

    /* Check whether a status change is pending */

    if (uStatusChangeBitmap != 0)
        {
        /*
         * To hold the number of bytes to copy based on the number of
         * ports supported on the root hub.
         */
        UINT8 uNumberOfBytes = 0;

        /*
         * Compute the number of bytes to copy based on the number of
         * ports supported on the root hub.
         */
        uNumberOfBytes = pHCDData->uNumberOfDownStreamPorts / 8;

        /* Check for remainder of the bytes */

        if ((pHCDData->uNumberOfDownStreamPorts % 8) != 0)
            {
            /* Increment the number of bytes to be copied */
            uNumberOfBytes++;
            }

        /* Obtain the pointer to the status change request */

        pStatusChangeRequest = pHCDData->pRootHubInterruptRequest;

        if (pStatusChangeRequest == NULL ||
            pStatusChangeRequest->pTransferBuffer == NULL)
            {
            OS_RELEASE_EVENT(pHCDData->ohciCommonMutex);
            return;
            }

        /* Reset the pending status change request pointer */

        pHCDData->pRootHubInterruptRequest = NULL;

        /* Swap the data to LE format */

        uStatusChangeBitmap = OS_UINT32_CPU_TO_LE(uStatusChangeBitmap);

        /* Update the URB transfer buffer */

        OS_MEMCPY(pStatusChangeRequest->pTransferBuffer,
                  (PUCHAR) (&uStatusChangeBitmap),
                  uNumberOfBytes);

        /* Update the URB transfer length */

        pStatusChangeRequest->uTransferLength = uNumberOfBytes;

        /* Update the URB status */

        pStatusChangeRequest->nStatus = USBHST_SUCCESS;

        /* Check whether the call back function for the URB is valid */

        if (pStatusChangeRequest->pfCallback != NULL)
            {
            /* Call the call back function for the URB */
            (*(pStatusChangeRequest->pfCallback))(pStatusChangeRequest);
            }

        }

    OS_RELEASE_EVENT(pHCDData->ohciCommonMutex);

    return;

    } /* End of function usbOhciProcessRootHubStatusChange () */


/* End of File usbOhciRootHubEmulation.c */

