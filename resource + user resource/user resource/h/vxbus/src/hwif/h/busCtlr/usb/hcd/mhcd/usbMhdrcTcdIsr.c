/* usbMhdrcTcdIsr.c - USB Mentor Graphics TCD interrupt handler module */

/*
 * Copyright (c) 2010-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01v,03may13,wyy  Remove compiler warning (WIND00356717)
01u,27feb13,s_z  Remove compiler warning (WIND00405361)
01t,05jan13,ljg  Add CPPI DMA support for MHDRC TCD (WIND00398723)
01s,25oct12,s_z  MHDRC TCD pass CV testing (WIND00385197)
01r,15sep11,m_y  Using one structure to describe HSDMA and CPPIDMA
01q,13dec11,m_y  Modify according to code check result (WIND00319317)
01p,16aug11,s_z  Get the data count of the FIFO after RXPKTRDY bit be set (WIND00266409)
01o,08jun11,m_y  Seperate DMA request handler routine (WIND00265911)
01n,24may11,m_y  Restart to handle request after the pipe can be reused by feature
                 clear (WIND00267726)
01m,16may11,w_x  Seperate DMA/endpoint interrupt handling by flag (WIND00276544)
01l,21apr11,m_y  set default value of the uCurXferLength to avoid complete the
                 request wrongly (WIND00267726)
01k,12apr11,m_y  modify USB_MHDRC_TCD_REQUEST structure (WIND00265911)
01j,08apr11,w_x  Clear Coverity MISSING_BREAK/FORWARD_NULL (WIND00264893)
01i,28mar11,w_x  Correct uNumEps usage (WIND00262862)
01h,23mar11,m_y  code clean up based on the review result
01g,09mar11,s_z  Code clean up
01f,08mar11,w_x  Fix TCD/ERP buffer corruption in usbMhdrcTcdEp0InterruptHandler
01e,04mar11,m_y  use pRequestMutex to protect the process of request
01d,23feb11,m_y  modify debug message in usbMhdrcTcdHsDmaInterruptHandler
01c,18feb11,m_y  modify routines usbMhdrcTcdFeatureClear,
                 usbMhdrcTcdFeatureSet and usbMhdrcTcdStatusGet
01b,20oct10,m_y  write
01a,18may10,s_z  created
*/

/*
DESCRIPTION

This file provides the interrupt handling routines for USB Mentor Graphics Target
Controller Driver.

INCLUDE FILES: usb/usbPlatform.h, usb/ossLib.h, usb/usbOsal.h, usb/usbOtg.h,
               usbMhdrcTcd.h, usbMhdrcTcdIsr.h, usbMhdrcTcdUtil.h,
               usbMhdrcTcdCore.h, usbMhdrcHsDma.h, usbMhdrcPlatform.h
*/

/* includes */

#include <usb/usbPlatform.h>
#include <usb/ossLib.h>
#include <usb/usbOsal.h>
#include <usb/usbOtg.h>
#include "usbMhdrcTcd.h"
#include "usbMhdrcTcdIsr.h"
#include "usbMhdrcTcdUtil.h"
#include "usbMhdrcTcdCore.h"
#include "usbMhdrcHsDma.h"
#include "usbMhdrcPlatform.h"
#include "usbMhdrcCppiDma.h"

/* Definitions */

#define USB_MHDRC_TCD_HS_DMA_TX_LIMIT   (0x40)
#define USB_MHDRC_TCD_HS_DMA_RX_LIMIT   (0x40)

/* Function declartion */

LOCAL void usbMhdrcTcdCoreInterruptHandler
    (
    pUSB_MHDRC_TCD_DATA  pTCDData,
    UINT8                uDevctl,
    UINT8                uPower
    );
LOCAL void usbMhdrcTcdTxReqHandle
    (
    pUSB_MHDRC_TCD_DATA      pTCDData,
    pUSB_MHDRC_TCD_REQUEST   pRequest
    );
LOCAL void usbMhdrcTcdRxReqHandle
    (
    pUSB_MHDRC_TCD_DATA      pTCDData,
    pUSB_MHDRC_TCD_REQUEST   pRequest
    );
LOCAL void usbMhdrcTcdTxEpHandle
    (
    pUSB_MHDRC_TCD_DATA  pTCDData,
    UINT8                uIndex
    );
LOCAL void usbMhdrcTcdRxEpHandle
    (
    pUSB_MHDRC_TCD_DATA  pTCDData,
    UINT8                uIndex
    );
LOCAL void usbMhdrcTcdEp0InterruptHandler
    (
    pUSB_MHDRC_TCD_DATA  pTCDData
    );
LOCAL void usbMhdrcTcdEp0TxReqHandle
    (
    pUSB_MHDRC_TCD_DATA      pTCDData,
    pUSB_MHDRC_TCD_REQUEST   pRequest
    );
LOCAL void usbMhdrcTcdEp0RxReqHandle
    (
    pUSB_MHDRC_TCD_DATA      pTCDData,
    pUSB_MHDRC_TCD_REQUEST   pRequest
    );
LOCAL void usbMhdrcTcdAddressSet
    (
    pUSB_MHDRC_TCD_DATA pTCDData,
    UINT16              uAddr
    );
LOCAL void usbMhdrcTcdFeatureClear
    (
    pUSB_MHDRC_TCD_DATA    pTCDData,
    pUSB_MHDRC_TCD_REQUEST pRequest,
    UINT8                  uRecipient,
    UINT16                 uFeature,
    UINT16                 uIndex
    );
LOCAL void usbMhdrcTcdFeatureSet
    (
    pUSB_MHDRC_TCD_DATA    pTCDData,
    pUSB_MHDRC_TCD_REQUEST pRequest,
    UINT8                  uRecipient,
    UINT16                 uFeature,
    UINT16                 uIndex
    );
LOCAL void usbMhdrcTcdSetupHandle
    (
    pUSB_MHDRC_TCD_DATA    pTCDData,
    pUSB_MHDRC_TCD_PIPE    pTCDPipe,
    pUSB_MHDRC_TCD_REQUEST pRequest
    );
LOCAL void usbMhdrcTcdDmaTxReqHandle
    (
    pUSB_MHDRC_TCD_DATA       pTCDData,
    pUSB_MHDRC_TCD_REQUEST    pRequest
    );
LOCAL void usbMhdrcTcdDmaRxReqHandle
    (
    pUSB_MHDRC_TCD_DATA       pTCDData,
    pUSB_MHDRC_TCD_REQUEST    pRequest
    );

/*******************************************************************************
*
* usbMhdrcTcdInterruptHandler - handler for handling USB MHDRC interrupts
*
* This routine is used to handle the USB MHDRC interrupts
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcTcdInterruptHandler
    (
    pUSBTGT_TCD pTCD
    )
    {
    UINT8               uIndex;
    UINT8               uDevctl;
    UINT8               uPower;
    UINT16              uRxStatus;
    UINT16              uTxStatus;
    pUSB_MUSBMHDRC      pMHDRC = NULL;
    pUSB_MHDRC_TCD_DATA pTCDData = NULL;

    /* Parameter verification */

    if ((NULL == pTCD) ||
        (NULL == (pTCDData = (pUSB_MHDRC_TCD_DATA)pTCD->pTcdSpecific)))
        {
        USB_MHDRC_ERR("Ivalid parameter: pTCD", 1, 2, 3, 4, 5, 6);
        return ;
        }

    if (NULL == (pMHDRC = pTCDData->pMHDRC))
        {
        USB_MHDRC_ERR("Ivalid parameter: pMHDRC", 1, 2, 3, 4, 5, 6);
        return ;
        }

    while (TRUE)
        {
        OS_WAIT_FOR_EVENT(pTCDData->isrSemId, WAIT_FOREVER);

        /* Handle the interrupt here */

        USB_MHDRC_VDBG("usbMhdrcTcdInterruptHandler(): "
                       "IRQ tx %p rx %p usb %p \n",
                       pTCDData->uTcdTxIrqStatus,
                       pTCDData->uTcdRxIrqStatus,
                       pTCDData->uTcdUsbIrqStatus,
                       4, 5, 6);

        SPIN_LOCK_ISR_TAKE(&pMHDRC->spinLock);
        uTxStatus = pTCDData->uTcdTxIrqStatus;
        uRxStatus = pTCDData->uTcdRxIrqStatus;
        pTCDData->uTcdTxIrqStatus = 0;
        pTCDData->uTcdRxIrqStatus = 0;
        SPIN_LOCK_ISR_GIVE(&pMHDRC->spinLock);

        while (uTxStatus != 0)
            {
            uIndex = (UINT8)USB_MHDRC_FIND_FIRST_SET_BIT(uTxStatus);
            if (uIndex == 0)
                {
                /* Handle EP0 interrupt */

                usbMhdrcTcdEp0InterruptHandler(pTCDData);
                }
            else
                {
                usbMhdrcTcdTxEpHandle(pTCDData, uIndex);
                }
            uTxStatus &= (UINT16)(~(1 << uIndex));
            }

        /* Handle Rx interrupt */

        while (uRxStatus != 0)
            {
            uIndex = (UINT8)USB_MHDRC_FIND_FIRST_SET_BIT(uRxStatus);
            usbMhdrcTcdRxEpHandle(pTCDData, uIndex);
            uRxStatus &= (UINT16)(~(1 << uIndex));
            }

        /* Handle usb core irqs */

        if (pTCDData->uTcdUsbIrqStatus)
            {
            uDevctl = USB_MHDRC_REG_READ8(pMHDRC, USB_MHDRC_DEVCTL);
            uPower = USB_MHDRC_REG_READ8(pMHDRC, USB_MHDRC_POWER);
            usbMhdrcTcdCoreInterruptHandler(pTCDData, uDevctl, uPower);
            }

        /*
         * Clear EOIR to acknowledge the completion of the USB core interrupt
         * for some platfrom
         */

        if (pMHDRC->PlatformData.pUsbIsrDone)
            {
            (pMHDRC->PlatformData.pUsbIsrDone)((void *)pMHDRC);
            }

        }
    }

/*******************************************************************************
*
* usbMhdrcTcdHsDmaInterruptHandler - interrupt handler for internal DMA
*
* This routine is to handle the interanl DMA's interrupt.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcTcdHsDmaInterruptHandler
    (
    pUSB_MUSBMHDRC  pMHDRC,
    int             uChannel
    )
    {
    pUSB_MHDRC_DMA_CHANNEL    pDMAChannel = NULL;
    pUSB_MHDRC_DMA_DATA       pDMAData = NULL;
    pUSB_MHDRC_TCD_REQUEST    pRequest = NULL;
    pUSB_MHDRC_TCD_DATA       pTCDData = NULL;
    pUSB_MHDRC_TCD_PIPE       pTCDPipe = NULL;
    ULONG                     uDmaAddress = 0;
    UINT32                    uDmaCtl     = 0;
    UINT32                    uDmaCount   = 0;

    /* Parameter verification */

    if ((NULL == pMHDRC) ||
        (NULL == pMHDRC->pTCDData) ||
        (uChannel > pMHDRC->uNumDmaChannels))
        {
        /* Debug print */

        USB_MHDRC_ERR("usbMhdrcTcdHsDmaInterruptHandler(): "
                      "Invalid parameter \n",
                      1, 2, 3, 4, 5 ,6);
        return;
        }

    pTCDData = (pUSB_MHDRC_TCD_DATA)pMHDRC->pTCDData;
    pDMAData = &(pMHDRC->dmaData);
    pDMAChannel = &(pDMAData->dmaChannel[uChannel]);

    uDmaCtl = USB_MHDRC_REG_READ32(pMHDRC,
                                   USB_MHDRC_HS_DMA_CTNL_CH(uChannel));

    uDmaAddress = USB_MHDRC_REG_READ32(pMHDRC,
                                       USB_MHDRC_HS_DMA_ADDR_CH(uChannel));

    /* Release the related DMA channel */

    pDMAData->pDmaChannelRelease(pDMAData, uChannel);

    if (uDmaCtl & USB_MHDRC_HS_DMA_CTNL_ERR)
        {
        USB_MHDRC_ERR("usbMhdrcTcdHsDmaInterruptHandler(): DMA ERR\n",
                     1, 2, 3, 4, 5, 6);
        pDMAChannel->uStatus = USB_MHDRC_DMA_CHANNEL_ERR;
        }
    else
        {
        pDMAChannel->uStatus = USB_MHDRC_DMA_CHANNEL_FREE;
        pRequest = (pUSB_MHDRC_TCD_REQUEST)pDMAChannel->pRequest;
        uDmaCount = (UINT32)(uDmaAddress - pDMAChannel->uAddr);

        if ((pRequest == NULL) ||
            ((pTCDPipe = pRequest->pTCDPipe) == NULL))
            {
            USB_MHDRC_ERR("Invalid request for the DMA channel %d pRequest %p pTCDPipe %p uChannel %d uDmaCount %d \n",
                          uChannel, pRequest, pTCDPipe, pDMAChannel->uChannel, uDmaCount, 6);

            /* This request may be already released */

            pDMAChannel->pRequest = NULL;

            return;
            }

        if (uDmaCount == 0)
            {
            USB_MHDRC_ERR("Invalid uDmaCount 0 DMA channel %d uDmaAddress %p pDMAChannel->uAddr %p \n",
                          uChannel, uDmaAddress, pDMAChannel->uAddr, 4, 5, 6);
            return;
            }

        if (uChannel != pDMAChannel->uChannel)
            {
            USB_MHDRC_ERR("Invalid channel %d pDMAChannel->uChannel %p \n",
                          uChannel, pDMAChannel->uChannel, 3, 4, 5, 6);

            return;
            }

        pRequest->uCurXferLength = uDmaCount;
        pDMAChannel->pRequest = NULL;
        pDMAChannel->uChannel = pMHDRC->uNumDmaChannels;

        if (pTCDPipe->uEpDir == USB_DIR_IN)
            {
            /* Handle Tx Request */

            usbMhdrcTcdDmaTxReqHandle(pTCDData, pRequest);
            }
        else
            {
            /* Handle Rx Request */

            pRequest->uActLength += uDmaCount;
            pRequest->pCurrentBuffer += uDmaCount;

            usbMhdrcTcdDmaRxReqHandle(pTCDData, pRequest);
            }



        }

    return;
    }

/*******************************************************************************
*
* usbMhdrcTcdDmaTxReqHandle - handle tx request transferred by DMA
*
* This routine handle tx request that transferred by DMA.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcTcdDmaTxReqHandle
    (
    pUSB_MHDRC_TCD_DATA       pTCDData,
    pUSB_MHDRC_TCD_REQUEST    pRequest
    )
    {
    UINT16                    uCsrReg = 0;
    UINT8                     uIndex;
    pUSB_MUSBMHDRC            pMHDRC = NULL;
    pUSB_MHDRC_TCD_PIPE       pTCDPipe = NULL;

    /* Valid parameter */

    USB_MHDRC_TCD_VAILD_TCDDATA_PARAMETER(pTCDData);

    pMHDRC = pTCDData->pMHDRC;
    pTCDPipe = pRequest->pTCDPipe;
    uIndex = pTCDPipe->uEpAddress;

    if ((uIndex > pMHDRC->uNumEps)||(uIndex == 0))
        {
        USB_MHDRC_ERR("Invalid parameter: uIndex %d, uNumEps = %d\n",
                      uIndex, pMHDRC->uNumEps, 3, 4, 5, 6);
        return;
        }

    /*
     * Get the status register for Ep[uIndex] to know the real reason
     * of the interrupt
     */

    uCsrReg = USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_PERI_TXCSR_EP(uIndex));

    if (uCsrReg & USB_MHDRC_PERI_TXCSR_SENTSTALL)
        {
        uCsrReg |= USB_MHDRC_PERI_TXCSR_WZC_BITS;
        uCsrReg = (UINT16)(uCsrReg & ~USB_MHDRC_PERI_TXCSR_SENTSTALL);
        USB_MHDRC_REG_WRITE16(pMHDRC,
                              USB_MHDRC_PERI_TXCSR_EP(uIndex),
                              uCsrReg);
        return;
        }

    if (uCsrReg & USB_MHDRC_PERI_TXCSR_UNDERRUN)
        {
        uCsrReg |= USB_MHDRC_PERI_TXCSR_WZC_BITS;
        uCsrReg = (UINT16)(uCsrReg & ~(USB_MHDRC_PERI_TXCSR_UNDERRUN|USB_MHDRC_PERI_TXCSR_TXPKTRDY));
        USB_MHDRC_REG_WRITE16(pMHDRC,
                              USB_MHDRC_PERI_TXCSR_EP(uIndex),
                              uCsrReg);
        }

    OS_WAIT_FOR_EVENT(pTCDPipe->pRequestMutex, WAIT_FOREVER);

    if (uCsrReg & USB_MHDRC_PERI_TXCSR_DMAEN)
        {
        pRequest->uActLength += pRequest->uCurXferLength;
        pRequest->pCurrentBuffer += pRequest->uCurXferLength;
        pRequest->uCurXferLength = 0;

        if (pMHDRC->uPlatformType == USB_MHDRC_PLATFORM_CENTAURUS)
            {
            if (pRequest->uXferSize == pRequest->uActLength)
                {
                uCsrReg |= USB_MHDRC_PERI_TXCSR_WZC_BITS;
                uCsrReg = (UINT16)(uCsrReg & ~(USB_MHDRC_PERI_TXCSR_DMAEN |
                                               USB_MHDRC_PERI_TXCSR_UNDERRUN |
                                               USB_MHDRC_PERI_TXCSR_TXPKTRDY));
                USB_MHDRC_REG_WRITE16(pMHDRC,
                                      USB_MHDRC_PERI_TXCSR_EP(uIndex),
                                      uCsrReg);

                usbMhdrcTcdReqComplete(pRequest, S_usbTgtTcdLib_ERP_SUCCESS);
                }

                OS_RELEASE_EVENT(pTCDPipe->pRequestMutex);
                return;
            }

        uCsrReg |= USB_MHDRC_PERI_TXCSR_WZC_BITS;
        uCsrReg = (UINT16)(uCsrReg & ~(USB_MHDRC_PERI_TXCSR_DMAEN |
                                       USB_MHDRC_PERI_TXCSR_UNDERRUN |
                                       USB_MHDRC_PERI_TXCSR_TXPKTRDY));
        USB_MHDRC_REG_WRITE16(pMHDRC,
                              USB_MHDRC_PERI_TXCSR_EP(uIndex),
                              uCsrReg);

        /*
         * If we use DMA and use mode[0] do the transfer
         * we should set the TXPKTRDY manually
         */

        if ((pRequest->uDmaMode == 0) ||
             (pRequest->uActLength & (pTCDPipe->uMaxPacketSize - 1)))
            {
            uCsrReg = USB_MHDRC_REG_READ16(pMHDRC,
                              USB_MHDRC_PERI_TXCSR_EP(uIndex));

            if (uCsrReg & USB_MHDRC_PERI_TXCSR_TXPKTRDY)
                {
                USB_MHDRC_WARN("Internal DMA FIFO still not available yet\n",
                               1, 2, 3, 4, 5, 6);
                OS_RELEASE_EVENT(pTCDPipe->pRequestMutex);
                return;
                }

            USB_MHDRC_REG_WRITE16(pMHDRC,
                                  USB_MHDRC_PERI_TXCSR_EP(uIndex),
                                  (USB_MHDRC_PERI_TXCSR_TXPKTRDY|
                                   USB_MHDRC_PERI_TXCSR_MODE));

            /*
             * After we set the TXPKTRDY there will be an endpoint interrupt
             * wait for the endpoint interrupt to handle this request
             */

            OS_RELEASE_EVENT(pTCDPipe->pRequestMutex);
            return;
            }

        /*
         * If this is the last packet of the request,
         * complete the request with success status. Or else, continue the
         * transfer
         */

        if (pRequest->uXferSize == pRequest->uActLength)
            usbMhdrcTcdReqComplete(pRequest, S_usbTgtTcdLib_ERP_SUCCESS);
        else
            usbMhdrcTcdReqHandle(pTCDData, pRequest);
        }

    OS_RELEASE_EVENT(pTCDPipe->pRequestMutex);
    return;
    }

/*******************************************************************************
*
* usbMhdrcTcdDmaRxReqHandle - handle rx request transferred by DMA
*
* This routine handle the rx request that transferred by DMA
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcTcdDmaRxReqHandle
    (
    pUSB_MHDRC_TCD_DATA      pTCDData,
    pUSB_MHDRC_TCD_REQUEST   pRequest
    )
    {
    UINT16                    uCsrReg;
    UINT8                     uIndex;
    pUSB_MUSBMHDRC            pMHDRC = NULL;
    pUSB_MHDRC_TCD_PIPE       pTCDPipe = NULL;

    /* Valid parameter */

    USB_MHDRC_TCD_VAILD_TCDDATA_PARAMETER(pTCDData);

    pMHDRC = pTCDData->pMHDRC;
    pTCDPipe = pRequest->pTCDPipe;
    uIndex = pTCDPipe->uEpAddress;

    if ((uIndex > pMHDRC->uNumEps) || (uIndex == 0))
        {
        USB_MHDRC_ERR("Invalid parameter: uIndex %d uNumEps %d\n",
                      uIndex, pMHDRC->uNumEps, 3, 4, 5, 6);
        return;
        }

    /*
     * Get the status register for Ep[uIndex] to know the real reason
     * of the interrupt
     */

    uCsrReg = USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_PERI_RXCSR_EP(uIndex));

    if (uCsrReg & USB_MHDRC_PERI_RXCSR_SENTSTALL)
        {
        uCsrReg |= (USB_MHDRC_PERI_RXCSR_WZC_BITS);
        uCsrReg = (UINT16)(uCsrReg & ~USB_MHDRC_PERI_RXCSR_SENTSTALL);
        USB_MHDRC_REG_WRITE16(pMHDRC,
                              USB_MHDRC_PERI_RXCSR_EP(uIndex),
                              uCsrReg);
        return;
        }

    if (uCsrReg & USB_MHDRC_PERI_RXCSR_OVERRUN)
        {
        uCsrReg = (UINT16)(uCsrReg & ~USB_MHDRC_PERI_RXCSR_OVERRUN);
        USB_MHDRC_REG_WRITE16(pMHDRC,
                              USB_MHDRC_PERI_RXCSR_EP(uIndex),
                              uCsrReg);
        }

    if (uCsrReg & USB_MHDRC_PERI_RXCSR_DMAEN)
        {
        uCsrReg = (UINT16)(uCsrReg & ~(USB_MHDRC_PERI_RXCSR_AUTOCLEAR|
                                       USB_MHDRC_PERI_RXCSR_DMAEN |
                                       USB_MHDRC_PERI_RXCSR_DMAMODE));

        USB_MHDRC_REG_WRITE16(pMHDRC,
                              USB_MHDRC_PERI_RXCSR_EP(uIndex),
                              uCsrReg | USB_MHDRC_PERI_RXCSR_WZC_BITS);


        /*
         * If we use mode[0] transfer data, or the ActLen is bigger
         * than 0 and smaller than the MaxPacketSize
         * we shoule clear the RXPKTRDY manually
         */

        if ((pRequest->uDmaMode == 0) ||
            (pRequest->uCurXferLength & (pTCDPipe->uMaxPacketSize - 1)))
            {
            uCsrReg = (UINT16)(uCsrReg & ~USB_MHDRC_PERI_RXCSR_RXPKTRDY);
            USB_MHDRC_REG_WRITE16(pMHDRC,
                                  USB_MHDRC_PERI_RXCSR_EP(uIndex),
                                  uCsrReg);
            }
        /*
         * if we complete the request or we receive a small packet
         * we will complete the current request or else continue to
         * transfer the request
         */

        if ((pRequest->uActLength == pRequest->uXferSize) ||
            (pRequest->uCurXferLength < pTCDPipe->uMaxPacketSize))
            usbMhdrcTcdReqComplete(pRequest, S_usbTgtTcdLib_ERP_SUCCESS);
        else
            usbMhdrcTcdRxReqHandle(pTCDData, pRequest);
        }
    return ;
    }

/*******************************************************************************
*
* usbMhdrcTcdReqHandle - handle a request
*
* This routine is to handle a request.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcTcdReqHandle
    (
    pUSB_MHDRC_TCD_DATA      pTCDData,
    pUSB_MHDRC_TCD_REQUEST   pRequest
    )
    {
    pUSB_MHDRC_TCD_PIPE         pTCDPipe = NULL;

    /* Valid parameter */

    if ((pTCDData == NULL) || (pRequest == NULL))
        {
        USB_MHDRC_ERR("Invalid Paramter: pTCDData %p, pRequest %p\n",
                      pTCDData, pRequest, 3, 4, 5, 6);
        return;
        }

    if ((pTCDPipe = pRequest->pTCDPipe) == NULL)
        {
        USB_MHDRC_ERR("Invalid Paramter: pRequest->pTCDPipe is NULL\n",
                      1, 2, 3, 4, 5, 6);
        return;
        }

    if (pTCDPipe->uEpAddress == USB_ENDPOINT_DEFAULT_CONTROL)
        {
        usbMhdrcTcdEp0ReqHandle(pTCDData, pRequest);
        }
    else
        {
        /* rx */

        if (pTCDPipe->uEpDir == USB_DIR_OUT)
            {
            usbMhdrcTcdRxReqHandle(pTCDData, pRequest);
            }
        /* tx */
        else
            {
            usbMhdrcTcdTxReqHandle(pTCDData, pRequest);
            }
        }
    }

/*******************************************************************************
*
* usbMhdrcTcdCoreInterruptHandler - interrupt handler for usb core interrupt
*
* This routine is to handle the usb core interrupt.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcTcdCoreInterruptHandler
    (
    pUSB_MHDRC_TCD_DATA  pTCDData,
    UINT8                uDevctl,
    UINT8                uPower
    )
    {
    pUSBTGT_TCD            pTCD = NULL;
    pUSB_MUSBMHDRC         pMHDRC = NULL;
    UINT8                  uTcdUsbIrqStatus;

    USB_MHDRC_TCD_VAILD_TCDDATA_PARAMETER(pTCDData);

    pMHDRC = pTCDData->pMHDRC;
    pTCD = pTCDData->pTCD;

    SPIN_LOCK_ISR_TAKE(&pMHDRC->spinLock);
    uTcdUsbIrqStatus = pTCDData->uTcdUsbIrqStatus;
    pTCDData->uTcdUsbIrqStatus = 0;
    SPIN_LOCK_ISR_GIVE(&pMHDRC->spinLock);

    /* Handle disconnect interrupt */

    if (uTcdUsbIrqStatus & USB_MHDRC_INTR_DISCONNECT)
        {
        USB_MHDRC_WARN("usbMhdrcTcdCoreInterruptHandler(): "
                       "USB_MHDRC_INTR_DISCONNECT\n",
                       1, 2, 3, 4, 5 ,6);
        usbMhdrcTcdDisconnect(pTCD);
        }

    /* Handle reset interrupt */

    if ((uTcdUsbIrqStatus & USB_MHDRC_INTR_RESET) &&
        (!(uDevctl & USB_MHDRC_DEVCTL_HM)))
        {
        USB_MHDRC_WARN("usbMhdrcTcdCoreInterruptHandler(): "
                       "USB_MHDRC_INTR_RESET\n",
                       1, 2, 3, 4, 5 ,6);
        usbMhdrcTcdReset(pTCD);
        }

    /* Handle resume interrupt */

    if (uTcdUsbIrqStatus & USB_MHDRC_INTR_RESUME)
        {
        USB_MHDRC_WARN("usbMhdrcTcdCoreInterruptHandler(): "
                       "USB_MHDRC_INTR_RESUME\n",
                       1, 2, 3, 4, 5 ,6);
        usbMhdrcTcdResume(pTCD);
        }

    /* Handle suspend interrupt */

    if (uTcdUsbIrqStatus & USB_MHDRC_INTR_SUSPEND)
        {
        USB_MHDRC_WARN("usbMhdrcTcdCoreInterruptHandler(): "
                       "USB_MHDRC_INTR_SUSPEND\n",
                       1, 2, 3, 4, 5 ,6);
        usbMhdrcTcdSuspend(pTCD);
        }

    return;
    }

/*******************************************************************************
*
* usbMhdrcTcdTxReqHandle - handle a TX request
*
* This routine is to handle a TX request.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcTcdTxReqHandle
    (
    pUSB_MHDRC_TCD_DATA      pTCDData,
    pUSB_MHDRC_TCD_REQUEST   pRequest
    )
    {
    int                         uDmaChannel = 0;
    UINT8                       uDmaMode = 0;
    UINT16                      uCsrReg = 0;
    UINT16                      uIndex = 0;
    UINT32                      uTxLen = 0;
    pUSB_MUSBMHDRC              pMHDRC = NULL;
    pUSB_MHDRC_TCD_PIPE         pTCDPipe = NULL;
    pUSB_MHDRC_DMA_DATA         pDMAData = NULL;

    pMHDRC = pTCDData->pMHDRC;
    pDMAData = &(pMHDRC->dmaData);
    pTCDPipe = pRequest->pTCDPipe;
    uIndex = pTCDPipe->uEpAddress;

    uCsrReg = USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_PERI_TXCSR_EP(uIndex));

    if (uCsrReg & USB_MHDRC_PERI_TXCSR_TXPKTRDY)
        {
        USB_MHDRC_DBG("usbMhdrcTcdTxReqHandle - "
                      "the before packet doesn't complete\n",
                      1, 2, 3, 4, 5, 6);
        return;
        }

    if (uCsrReg & USB_MHDRC_PERI_TXCSR_SENDSTALL)
        {
        USB_MHDRC_DBG("usbMhdrcTcdTxReqHandle - "
                      "the pipe is stall\n",
                      1, 2, 3, 4, 5, 6);
        return;
        }

    if (pMHDRC->bDmaEnabled)
        {
        uTxLen = min((pRequest->uXferSize - pRequest->uActLength),
                     USB_MHDRC_HS_DMA_TX_MAX_LEN);
        /*
         * If transfer length is bigger than the USB_MHDRC_TCD_HS_DMA_TX_LIMIT,
         * we will use DMA to transfer data
         * NOTE: this may affect the read speed from host side
         */

        if (uTxLen > USB_MHDRC_TCD_HS_DMA_TX_LIMIT)
            {
            if (pRequest->bDmaActive == TRUE)
                {
                USB_MHDRC_DBG("usbMhdrcTcdTxReqHandle - TX DMA ongoing!\n",
                              1, 2, 3, 4, 5, 6);
                return;
                }

            /* Get a free DMA channel */

            uDmaChannel = pDMAData->pDmaChannelRequest(pDMAData);

            if (USB_MHDRC_DMA_INVALID_CHANNEL != uDmaChannel)
                {
                /* Record the DMA channel */

                pTCDPipe->uDmaChannel = (UINT8)uDmaChannel;

                /*
                 * If the tranfer data size is bigger than the max packet
                 * size, we should choose DMA mode "1"
                 * or else we should choose DMA mode "0"
                 */

                if (pMHDRC->uPlatformType != USB_MHDRC_PLATFORM_CENTAURUS)
                    {
                    uDmaMode = (UINT8)((uTxLen > pTCDPipe->uMaxPacketSize) ? 1 : 0);
                    pRequest->uDmaMode = uDmaMode;
                    pRequest->bDmaActive = TRUE;

                /* Program TXCSR register */

                    if (uDmaMode == 0)
                        {
                        uCsrReg = (UINT16)(uCsrReg & ~(USB_MHDRC_PERI_TXCSR_AUTOSET |
                                                       USB_MHDRC_PERI_TXCSR_DMAEN));
                        USB_MHDRC_REG_WRITE16(pMHDRC,
                                  USB_MHDRC_PERI_TXCSR_EP(uIndex),
                                  uCsrReg | USB_MHDRC_PERI_TXCSR_WZC_BITS);


                        uCsrReg = USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_PERI_TXCSR_EP(uIndex));
                        uCsrReg = (UINT16)(uCsrReg & ~ USB_MHDRC_PERI_TXCSR_DMAMODE);
                        uCsrReg |= (
                                    USB_MHDRC_PERI_TXCSR_DMAEN|
                                    USB_MHDRC_PERI_TXCSR_MODE);
                        }
                    else
                        {
                        uCsrReg |= (USB_MHDRC_PERI_TXCSR_AUTOSET |
                                    USB_MHDRC_PERI_TXCSR_DMAEN   |
                                    USB_MHDRC_PERI_TXCSR_DMAMODE |
                                    USB_MHDRC_PERI_TXCSR_MODE);

                        }

                    uCsrReg = (UINT16)(uCsrReg & ~USB_MHDRC_PERI_TXCSR_UNDERRUN);
                    USB_MHDRC_REG_WRITE16(pMHDRC,
                                  USB_MHDRC_PERI_TXCSR_EP(uIndex),
                                  uCsrReg);
                    }

                USB_MHDRC_DBG("Program TX DMA for %d bytes pRequest %p pTCDPipe %p uDmaChannel %d\n",
                              uTxLen,
                              pRequest,
                              pTCDPipe,
                              uDmaChannel, 5, 6);

                /* Program the DMA and start to transfer */

                pDMAData->pDmaConfigure (pMHDRC,
                                         pRequest,
                                         (ULONG)pRequest->pCurrentBuffer,
                                         uTxLen,
                                         (UINT8)uDmaChannel,
                                         uDmaMode,
                                         (UINT8)uIndex,
                                         (BOOLEAN)(pTCDPipe->uEpDir == USB_DIR_IN),
                                         pTCDPipe->uMaxPacketSize);

                return;
                }
            else
                {
                USB_MHDRC_DBG("usbMhdrcTcdTxReqHandle -"
                              "No valid DMA Channel, use PIO mode keep going\n",
                              1, 2, 3, 4, 5, 6);
                }
            }
        }

    /* Set the DMA channel number to invalid value */

    pTCDPipe->uDmaChannel = pMHDRC->uNumDmaChannels;
    pRequest->bDmaActive = FALSE;

    /* Use PIO mode to handle the TX request */

    uTxLen = min((pRequest->uXferSize - pRequest->uActLength),
                  pTCDPipe->uMaxPacketSize);

    /* Write data to FIFO */

    usbMhdrcFIFOWrite(pMHDRC, pRequest->pCurrentBuffer, (UINT16)uTxLen, (UINT8)uIndex);

    /* Update the currentBuffer pointer and ActLength */

    pRequest->pCurrentBuffer += uTxLen;
    pRequest->uActLength += uTxLen;

    /* Set CSR register to indicate the data already loaded into FIFO */

    uCsrReg |= USB_MHDRC_PERI_TXCSR_TXPKTRDY;
    uCsrReg = (UINT16)(uCsrReg & ~USB_MHDRC_PERI_TXCSR_UNDERRUN);

    USB_MHDRC_REG_WRITE16(pMHDRC,
                          USB_MHDRC_PERI_TXCSR_EP(uIndex),
                          uCsrReg);

    USB_MHDRC_DBG("Program PIO TX data pRequest->uActLength %d uXferSize %d pRequest %p\n",
                  pRequest->uActLength,
                  pRequest->uXferSize,
                  pRequest, 4, 5, 6);
    }

/*******************************************************************************
*
* usbMhdrcTcdRxReqHandle - handle a RX request
*
* This routine is to handle a RX request.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcTcdRxReqHandle
    (
    pUSB_MHDRC_TCD_DATA      pTCDData,
    pUSB_MHDRC_TCD_REQUEST   pRequest
    )
    {
    UINT8                       uDmaMode;
    int                         uDmaChannel = 0;
    UINT16                      uCsrReg = 0;
    UINT16                      uCountReg = 0;
    UINT16                      uIndex = 0;
    UINT32                      uRxLen = 0;
    pUSB_MUSBMHDRC              pMHDRC = NULL;
    pUSB_MHDRC_TCD_PIPE         pTCDPipe = NULL;
    pUSB_MHDRC_DMA_DATA         pDMAData = NULL;

    pMHDRC = pTCDData->pMHDRC;
    pDMAData = &(pMHDRC->dmaData);
    pTCDPipe = pRequest->pTCDPipe;
    uIndex = pTCDPipe->uEpAddress;

    uCsrReg = USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_PERI_RXCSR_EP(uIndex));

    if (uCsrReg & (USB_MHDRC_PERI_RXCSR_SENDSTALL | USB_MHDRC_PERI_RXCSR_SENTSTALL))
        {
        USB_MHDRC_ERR("usbMhdrcTcdRxReqHandle the endpoint is stall\n",
                      1, 2, 3, 4, 5, 6);

        USB_MHDRC_REG_WRITE16(pMHDRC,
                              USB_MHDRC_PERI_RXCSR_EP(uIndex),
                              uCsrReg & ~(USB_MHDRC_PERI_RXCSR_SENDSTALL |
                                          USB_MHDRC_PERI_RXCSR_SENTSTALL));

        return;
        }

    /* Set default value to avoid the request complete in wrong way */

    uRxLen = pTCDPipe->uMaxPacketSize;

    if (uCsrReg & USB_MHDRC_PERI_RXCSR_RXPKTRDY)
        {
        if ((uCsrReg & USB_MHDRC_PERI_RXCSR_DMAEN) &&
            (pRequest->bDmaActive == TRUE))
            {
            USB_MHDRC_DBG("usbMhdrcTcdRxReqHandle - RX DMA Active, uPipeFlag 0x%X!\n",
                           pTCDPipe->uPipeFlag, 2, 3, 4, 5, 6);
            return;
            }

        uCountReg = USB_MHDRC_REG_READ16(pMHDRC,
                                         USB_MHDRC_PERI_RXCOUNT_EP(uIndex));

        if (pRequest->uActLength < pRequest->uXferSize)
            {
            if (pMHDRC->uPlatformType == USB_MHDRC_PLATFORM_CENTAURUS)
                {
                while ((uCountReg < pTCDPipe->uMaxPacketSize) &&
                       ((pRequest->uXferSize - pRequest->uActLength) >= pTCDPipe->uMaxPacketSize) &&
                       (pMHDRC->bDmaEnabled == TRUE))
                    {
                    USB_MHDRC_WARN("\n\n usbMhdrcTcdRxReqHandle 0x%x, 0x%x, 0x%x, 0x%x, 0x%x. \n\n",
                        pRequest->uXferSize, pRequest->uActLength, uCountReg, pTCDPipe->uMaxPacketSize,uCsrReg, 6);

                    uCountReg =0x200;
                    }
                }

            uRxLen = min((pRequest->uXferSize - pRequest->uActLength),
                         uCountReg);

            /*
             * If transfer length is bigger than the USB_MHDRC_TCD_HS_DMA_RX_LIMIT
             * and the DMA mode is enabled, we will use DMA to receive data
             * NOTE: this may affect the write speed from host side
             */

            if ((pMHDRC->bDmaEnabled == TRUE) &&
                (uRxLen > USB_MHDRC_TCD_HS_DMA_RX_LIMIT))
                {
                uDmaChannel = pDMAData->pDmaChannelRequest(pDMAData);
                /*
                 * If we cann't get a valid DMA chanenl,
                 * use PIO mode to transfer data
                 */

                if ((USB_MHDRC_DMA_INVALID_CHANNEL != uDmaChannel) &&
                    (uDmaChannel < pMHDRC->uNumDmaChannels))
                    {
                    pTCDPipe->uDmaChannel = (UINT8)uDmaChannel;

                    if (pMHDRC->uPlatformType != USB_MHDRC_PLATFORM_CENTAURUS)
                        {
                        uCsrReg |= USB_MHDRC_PERI_RXCSR_DMAEN;
                        uCsrReg |= USB_MHDRC_PERI_RXCSR_AUTOCLEAR;

                        USB_MHDRC_REG_WRITE16(pMHDRC,
                                              USB_MHDRC_PERI_RXCSR_EP(uIndex),
                                              uCsrReg);
                        }

                    /*
                     * If the transfer data size is bigger than the max packet
                     * size, we should choose DMA mode "0"
                     * or else we should choose DMA mode "1"
                     */

                    uDmaMode = (UINT8)((uRxLen > pTCDPipe->uMaxPacketSize) ? 0 : 1);
                    pRequest->uDmaMode = uDmaMode;
                    pRequest->bDmaActive = TRUE;
                    USB_MHDRC_DBG ("Program RX DMA for %d bytes pRequest %p pTCDPipe %p uDmaChannel %d uXferSize %d pRequest->pTCDPipe %p\n",
                                   uRxLen,
                                   pRequest,
                                   pTCDPipe,
                                   uDmaChannel, pRequest->uXferSize, pRequest->pTCDPipe);

                    /*
                     * Set to default invalid value to avoid
                     * complete the request wrongly
                     */

                    pRequest->uCurXferLength = pTCDPipe->uMaxPacketSize;

                    /* Program the DMA and start to transfer */

                    pDMAData->pDmaConfigure (pMHDRC,
                                             pRequest,
                                             (ULONG)pRequest->pCurrentBuffer,
                                             uRxLen,
                                             (UINT8)uDmaChannel,
                                             uDmaMode,
                                             (UINT8)uIndex,
                                             (BOOLEAN)(pTCDPipe->uEpDir == USB_DIR_IN),
                                             pTCDPipe->uMaxPacketSize);

                    return;
                    }
                else
                    {
                    USB_MHDRC_DBG("No valid DMA Channel, use PIO mode keep going\n",
                                  1, 2, 3, 4, 5, 6);
                    }
                }
            /* Set the DMA channel number to invalid value */

            pTCDPipe->uDmaChannel = pMHDRC->uNumDmaChannels;
            pRequest->bDmaActive = FALSE;

            /* Use PIO Mode to transfer data */

            usbMhdrcFIFORead(pMHDRC, pRequest->pCurrentBuffer, (UINT16)uRxLen, (UINT8)uIndex);

            /* Update the length and buffer pointer */

            pRequest->pCurrentBuffer += uRxLen;
            pRequest->uActLength += uRxLen;

            /* Clear the RXPKTRDY to indicate data already read from FIFO */

            uCsrReg |= (USB_MHDRC_PERI_RXCSR_WZC_BITS);
            uCsrReg = (UINT16)(uCsrReg & ~USB_MHDRC_PERI_RXCSR_RXPKTRDY);
            USB_MHDRC_REG_WRITE16(pMHDRC,
                                  USB_MHDRC_PERI_RXCSR_EP(uIndex),
                                  uCsrReg);
            }
        }

    /*
     * If we receive enough data or receive a small packet
     * we should complete the request
     */

    if ((pRequest->uActLength == pRequest->uXferSize) ||
        (uRxLen < pTCDPipe->uMaxPacketSize))
        {
        usbMhdrcTcdReqComplete(pRequest, S_usbTgtTcdLib_ERP_SUCCESS);
        }
    }

/*******************************************************************************
*
* usbMhdrcTcdTxEpHandle - handle TX endpoint
*
* This routine is to handle TX endpoint.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcTcdTxEpHandle
    (
    pUSB_MHDRC_TCD_DATA  pTCDData,
    UINT8                uIndex
    )
    {
    UINT16                    uCsrReg = 0;
    pUSB_MUSBMHDRC            pMHDRC = NULL;
    pUSB_MHDRC_TCD_PIPE       pTCDPipe = NULL;
    pUSB_MHDRC_TCD_REQUEST    pRequest= NULL;
    pUSB_MHDRC_TCD_REQUEST    pNextRequest= NULL;

    /* Valid parameter */

    USB_MHDRC_TCD_VAILD_TCDDATA_PARAMETER(pTCDData);

    pMHDRC = pTCDData->pMHDRC;

    if ((uIndex > pMHDRC->uNumEps)||(uIndex == 0))
        {
        USB_MHDRC_ERR("Invalid parameter: uIndex %d, uNumEps = %d\n",
                      uIndex, pMHDRC->uNumEps, 3, 4, 5, 6);
        return;
        }

    pTCDPipe = usbMhdrcTcdGetPipeByEpAddr(pTCDData, uIndex, USB_DIR_IN);
    if (pTCDPipe == NULL)
        {
        USB_MHDRC_ERR("Can't find pTCDPipe by Index %d for USB_DIR_IN\n",
                      uIndex, 2, 3, 4, 5, 6);
        return;
        }

    /*
     * Get the status register for Ep[uIndex] to know the real reason
     * of the interrupt
     */

    uCsrReg = USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_PERI_TXCSR_EP(uIndex));

    if (uCsrReg & USB_MHDRC_PERI_TXCSR_SENTSTALL)
        {
        USB_MHDRC_ERR("USB_MHDRC_PERI_TXCSR_SENTSTALL\n",
                      1, 2, 3, 4, 5, 6);
        uCsrReg |= USB_MHDRC_PERI_TXCSR_WZC_BITS;
        uCsrReg = (UINT16)(uCsrReg & ~USB_MHDRC_PERI_TXCSR_SENTSTALL);
        USB_MHDRC_REG_WRITE16(pMHDRC,
                              USB_MHDRC_PERI_TXCSR_EP(uIndex),
                              uCsrReg);
        return;
        }

    if (uCsrReg & USB_MHDRC_PERI_TXCSR_UNDERRUN)
        {
        USB_MHDRC_VDBG("USB_MHDRC_PERI_TXCSR_UNDERRUN uCsrReg 0x%X\n",
                       uCsrReg, 2, 3, 4, 5, 6);
        uCsrReg |= USB_MHDRC_PERI_TXCSR_WZC_BITS;
        uCsrReg = (UINT16)(uCsrReg & ~(USB_MHDRC_PERI_TXCSR_UNDERRUN|
                                       USB_MHDRC_PERI_TXCSR_TXPKTRDY));
        USB_MHDRC_REG_WRITE16(pMHDRC,
                              USB_MHDRC_PERI_TXCSR_EP(uIndex),
                              uCsrReg);
        }

    OS_WAIT_FOR_EVENT(pTCDPipe->pRequestMutex, WAIT_FOREVER);

    /* Get first request of the pipe */

    pRequest = usbMhdrcTcdFirstReqGet(pTCDPipe);
    if (pRequest != NULL)
        {
        if (pRequest->uXferSize == pRequest->uActLength)
            {
            /* Get next request */

            pNextRequest = usbMhdrcTcdNextReqGet(pRequest);

            /* Complete the current request */

            usbMhdrcTcdReqComplete(pRequest, S_usbTgtTcdLib_ERP_SUCCESS);
            pRequest = pNextRequest;
            if (pRequest == NULL)
                {
                OS_RELEASE_EVENT(pTCDPipe->pRequestMutex);
                return;
                }
            }
        usbMhdrcTcdTxReqHandle(pTCDData, pRequest);
        }
    OS_RELEASE_EVENT(pTCDPipe->pRequestMutex);
    return;
    }

/*******************************************************************************
*
* usbMhdrcTcdRxEpHandle - handle RX endpoint
*
* This routine is to handle the RX endpoint.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcTcdRxEpHandle
    (
    pUSB_MHDRC_TCD_DATA  pTCDData,
    UINT8                uIndex
    )
    {
    UINT16                    uCsrReg;
    pUSB_MUSBMHDRC            pMHDRC = NULL;
    pUSB_MHDRC_TCD_REQUEST    pRequest= NULL;
    pUSB_MHDRC_TCD_PIPE       pTCDPipe = NULL;

    /* Valid parameter */

    USB_MHDRC_TCD_VAILD_TCDDATA_PARAMETER(pTCDData);

    pMHDRC = pTCDData->pMHDRC;

    if ((uIndex > pMHDRC->uNumEps) || (uIndex == 0))
        {
        USB_MHDRC_ERR("Invalid parameter: uIndex %d uNumEps %d\n",
                      uIndex, pMHDRC->uNumEps, 3, 4, 5, 6);
        return;
        }

    /* Get the related pipe */

    pTCDPipe = usbMhdrcTcdGetPipeByEpAddr(pTCDData, uIndex, USB_DIR_OUT);
    if (pTCDPipe == NULL)
        {
        USB_MHDRC_ERR("Can't find pTCDPipe by Index %d for USB_DIR_OUT\n",
                      uIndex, 2, 3, 4, 5, 6);
        return;
        }

    /* Get the first request of the pipe. If no, just return */

    pRequest = usbMhdrcTcdFirstReqGet(pTCDPipe);
    if (pRequest == NULL)
        {
        USB_MHDRC_DBG("There is no request on the pipe %p\n",
                      pTCDPipe, 2, 3, 4, 5, 6);
        return;
        }

    /*
     * Get the status register for Ep[uIndex] to know the real reason
     * of the interrupt
     */

    uCsrReg = USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_PERI_RXCSR_EP(uIndex));

    if (uCsrReg & USB_MHDRC_PERI_RXCSR_SENTSTALL)
        {
        uCsrReg |= (USB_MHDRC_PERI_RXCSR_WZC_BITS);
        uCsrReg = (UINT16)(uCsrReg & ~USB_MHDRC_PERI_RXCSR_SENTSTALL);
        USB_MHDRC_REG_WRITE16(pMHDRC,
                              USB_MHDRC_PERI_RXCSR_EP(uIndex),
                              uCsrReg);
        return;
        }

    if (uCsrReg & USB_MHDRC_PERI_RXCSR_OVERRUN)
        {
        uCsrReg = (UINT16)(uCsrReg & ~USB_MHDRC_PERI_RXCSR_OVERRUN);
        USB_MHDRC_REG_WRITE16(pMHDRC,
                              USB_MHDRC_PERI_RXCSR_EP(uIndex),
                              uCsrReg);
        }

    usbMhdrcTcdRxReqHandle(pTCDData, pRequest);

    return;
    }

/*******************************************************************************
*
* usbMhdrcTcdEp0InterruptHandler - process the control endpoint interrupt
*
* This routine processes the control endpoint interrupt.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcTcdEp0InterruptHandler
    (
    pUSB_MHDRC_TCD_DATA  pTCDData
    )
    {
    UINT16                      uCsr0 = 0;
    UINT16                      uCount0 = 0;
    pUSBTGT_TCD                 pTCD = NULL;
    pUSB_MUSBMHDRC              pMHDRC = NULL;
    pUSB_MHDRC_TCD_PIPE         pTCDPipe = NULL;
    pUSB_MHDRC_TCD_REQUEST      pRequest = NULL;

    /* The caller has already checked parameter so no need to do again */

    pMHDRC = pTCDData->pMHDRC;
    pTCD = pTCDData->pTCD;
    pTCDPipe = (pUSB_MHDRC_TCD_PIPE)(pTCD->controlPipe);

    /* Get CSR0 and COUNT0 register content */

    uCsr0 = USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_PERI_CSR0);
    uCount0 = USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_COUNT0);

    USB_MHDRC_DBG("uCsr0 0x%X uCount0 0x%X ep0Stage %d\n",
                  uCsr0, uCount0, pTCDData->ep0Stage, 4, 5, 6);

    if (uCsr0 & USB_MHDRC_PERI_CSR0_SENTSTALL)
        {
        /*
         * This bit menas a STALL handshake is transmitted
         * ACTION: Clear this bit, update ep0Stage into IDLE
         */

        USB_MHDRC_REG_WRITE16(pMHDRC,
                              USB_MHDRC_PERI_CSR0,
                              (uCsr0 & (~USB_MHDRC_PERI_CSR0_SENTSTALL)));
        pTCDData->ep0Stage = USB_MHDRC_TCD_STAGE_IDLE;
        uCsr0 = USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_PERI_CSR0);
        }

    if (uCsr0 & USB_MHDRC_PERI_CSR0_SETUPEND)
        {
        /*
         * This bit means control transaction ends before the DATAEND bit has
         * been set.
         * ACTION: Update the ep0Stage
         */

        USB_MHDRC_REG_WRITE16(pMHDRC,
                              USB_MHDRC_PERI_CSR0,
                              USB_MHDRC_PERI_CSR0_SERV_SETUPEND);
        switch(pTCDData->ep0Stage)
            {
            case USB_MHDRC_TCD_STAGE_DATA_OUT:
                pTCDData->ep0Stage = USB_MHDRC_TCD_STAGE_IDLE;
                break;
            case USB_MHDRC_TCD_STAGE_DATA_IN:
                pTCDData->ep0Stage = USB_MHDRC_TCD_STAGE_IDLE;
                break;
            default:
                USB_MHDRC_ERR("ep0Stage: %d occur in SETUPEND\n",
                               pTCDData->ep0Stage, 2, 3, 4, 5, 6);
            }
        uCsr0 = USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_PERI_CSR0);
        }

    switch (pTCDData->ep0Stage)
        {
        case USB_MHDRC_TCD_STAGE_DATA_OUT:
            if ((uCsr0 & USB_MHDRC_PERI_CSR0_TXPKTRDY) == 0)
                {
                /*
                 * This bit means the data alrady transfer over
                 * ACTION: Send the remain data of the request if any
                 * if this is the last one, update ep0Stage set DATAEND bit
                 * or else just set TXPKTRDY to set the data out
                 */

                usbMhdrcTcdEp0TxReqHandle(pTCDData,
                                          usbMhdrcTcdFirstReqGet(pTCDPipe));
                }
            break;
        case USB_MHDRC_TCD_STAGE_DATA_IN:
            if (uCsr0 & USB_MHDRC_PERI_CSR0_RXPKTRDY)
                {
                /*
                 * This bit means a data packet has been received
                 * ACTION: Get data from the fifo
                 * If this data is the last one,
                 * or else
                 */

                usbMhdrcTcdEp0RxReqHandle(pTCDData,
                                          usbMhdrcTcdFirstReqGet(pTCDPipe));
                }
            break;
        case USB_MHDRC_TCD_STAGE_STATUS_IN:
            pTCDData->ep0Stage = USB_MHDRC_TCD_STAGE_IDLE;
            if (pTCD->uAddrToSet != 0)
                {
                /* Update the FADD register */

                USB_MHDRC_REG_WRITE8(pMHDRC, USB_MHDRC_FADD, pTCD->uAddrToSet);

                /* Notify the attached function */

                usbMhdrcTcdReqComplete(usbMhdrcTcdFirstReqGet(pTCDPipe),
                                       S_usbTgtTcdLib_ERP_RESTART);
                pTCD->uAddrToSet = 0;
                }
            else if (pTCDData->uTestMode != 0)
                {
                USB_MHDRC_REG_WRITE8(pMHDRC, USB_MHDRC_TESTMODE, pTCDData->uTestMode);
                pTCDData->uTestMode = 0;
                }
            else
                {
                /* Indicate the request is complete */

                usbMhdrcTcdReqComplete(usbMhdrcTcdFirstReqGet(pTCDPipe),
                                       S_usbTgtTcdLib_ERP_RESTART);
                }

            if (uCsr0 & USB_MHDRC_PERI_CSR0_RXPKTRDY)
                {
                /* Receive a packet, goto setup phase */
                goto SETUP_STAGE;
                }
            break;
        case USB_MHDRC_TCD_STAGE_STATUS_OUT:

            pTCDData->ep0Stage = USB_MHDRC_TCD_STAGE_IDLE;

            /* Complet the firest request */

            usbMhdrcTcdReqComplete(usbMhdrcTcdFirstReqGet(pTCDPipe),
                                   S_usbTgtTcdLib_ERP_RESTART);

            if (uCsr0 & USB_MHDRC_PERI_CSR0_RXPKTRDY)
                {
                /* Receive a packet, goto setup phase */
                goto SETUP_STAGE;
                }
            break;
        case USB_MHDRC_TCD_STAGE_IDLE:
            pTCDData->ep0Stage = USB_MHDRC_TCD_STAGE_SETUP;
            /* Fall Through */
        case USB_MHDRC_TCD_STAGE_SETUP:
SETUP_STAGE:
            if (uCsr0 & USB_MHDRC_PERI_CSR0_RXPKTRDY)
                {
                if (uCount0 != 8)
                    {
                    USB_MHDRC_ERR("Setup packet len: %d != 8\n",
                                  uCount0, 2, 3, 4, 5, 6);

                    pRequest = usbMhdrcTcdFirstReqGet(pTCDPipe);

                    USB_MHDRC_REG_WRITE16(pMHDRC,
                                          USB_MHDRC_PERI_CSR0,
                                          USB_MHDRC_PERI_CSR0_SERV_RXPKTRDY);

                    /* Wrong data for Setup packet, callback for new request */

                    if (NULL != pRequest)
                        {
                        pTCDData->ep0Stage = USB_MHDRC_TCD_STAGE_IDLE;
                        usbMhdrcTcdReqComplete(pRequest,
                                               S_usbTgtTcdLib_TCD_FAULT);
                        }

                    break;
                    }
                pRequest = usbMhdrcTcdFirstReqGet(pTCDPipe);

                if (pRequest != NULL)
                    {
                    usbMhdrcFIFORead(pMHDRC,
                                     pRequest->pCurrentBuffer,
                                     8,
                                     0);
                    /* Update the tranfer length and buffer pointer */

                    pRequest->uActLength = 8;

                    /* Start to handle the setup command */

                    usbMhdrcTcdSetupHandle(pTCDData, pTCDPipe, pRequest);
                    }
                }
            break;
        case USB_MHDRC_TCD_STAGE_ACK_WAIT:
            break;
        default:
            USB_MHDRC_DBG("ep0Stage: %d set to IDLE\n",
                          pTCDData->ep0Stage, 2, 3, 4, 5, 6);
            USB_MHDRC_REG_WRITE16(pMHDRC,
                                  USB_MHDRC_PERI_CSR0,
                                  USB_MHDRC_PERI_CSR0_SENDSTALL);
            pTCDData->ep0Stage = USB_MHDRC_TCD_STAGE_IDLE;
            break;
        }
    return;
    }

/*******************************************************************************
*
* usbMhdrcTcdEp0TxReqHandle - handle endpoint[0]'s TX request
*
* This routine is to handle endpoint[0]'s TX request.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcTcdEp0TxReqHandle
    (
    pUSB_MHDRC_TCD_DATA      pTCDData,
    pUSB_MHDRC_TCD_REQUEST   pRequest
    )
    {
    UINT16                      uCsr0 = 0;
    UINT32                      uBufLen = 0;
    pUSB_MUSBMHDRC              pMHDRC = NULL;
    pUSB_MHDRC_TCD_PIPE         pTCDPipe = NULL;

    /* Valid parameter */

    USB_MHDRC_TCD_VAILD_TCDDATA_PARAMETER(pTCDData);

    if (pRequest == NULL)
        {
        USB_MHDRC_ERR("Invalid Parameter: pRequest\n",
                      1, 2, 3, 4, 5, 6);
        return;
        }

    pMHDRC = pTCDData->pMHDRC;
    pTCDPipe = (pRequest->pTCDPipe);

    uBufLen = min(USB_MHDRC_TCD_EP0_MAX_FIFO_SIZE,
            (pRequest->uXferSize - pRequest->uActLength));

    usbMhdrcFIFOWrite(pMHDRC, pRequest->pCurrentBuffer, (UINT16)uBufLen, 0);

    pRequest->pCurrentBuffer += uBufLen;
    pRequest->uActLength += uBufLen;

    /* Check if this packet is the last one */

    if ((uBufLen < USB_MHDRC_TCD_EP0_MAX_FIFO_SIZE) ||
        (pRequest->uActLength == pRequest->uXferSize))
        {
        pTCDData->ep0Stage = USB_MHDRC_TCD_STAGE_STATUS_OUT;
        uCsr0 = (USB_MHDRC_PERI_CSR0_TXPKTRDY |
                 USB_MHDRC_PERI_CSR0_DATAEND);
        }
    else
        {
        uCsr0 = USB_MHDRC_PERI_CSR0_TXPKTRDY;
        }

    /*
     * We need to sync the cache or anything else before we
     * send the data out
     */

    USB_MHDRC_REG_WRITE16(pMHDRC,
                          USB_MHDRC_PERI_CSR0,
                          uCsr0);

    }

/*******************************************************************************
*
* usbMhdrcTcdEp0RxReqHandle - handle endpoint[0]'s RX request
*
* This routine is to handle endpoint[0]'s RX request.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcTcdEp0RxReqHandle
    (
    pUSB_MHDRC_TCD_DATA      pTCDData,
    pUSB_MHDRC_TCD_REQUEST   pRequest
    )
    {
    UINT16                      uCsr0 = 0;
    UINT16                      uCount0 = 0;
    UINT32                      uBufLen = 0;
    pUSB_MUSBMHDRC              pMHDRC = NULL;

    /* Valid parameter */

    USB_MHDRC_TCD_VAILD_TCDDATA_PARAMETER(pTCDData);

    pMHDRC = pTCDData->pMHDRC;

    if (pRequest != NULL)
        {
        uCsr0 = USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_PERI_CSR0);

        if (0 == (uCsr0 & USB_MHDRC_PERI_CSR0_RXPKTRDY))
            {
            USB_MHDRC_WARN("NO bit set of USB_MHDRC_PERI_CSR0_RXPKTRDY, return\n",
                          1, 2, 3, 4, 5, 6);
            return;
            }

        uBufLen = pRequest->uXferSize - pRequest->uActLength;

        /* Get the real FIFO count */

        uCount0 = USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_COUNT0);

        if (uCount0 > uBufLen)
            {
            USB_MHDRC_ERR("Get data %d > request %d\n",
                          1, 2, 3, 4, 5, 6);
            }
        else
            uBufLen = uCount0;


        usbMhdrcFIFORead(pMHDRC, pRequest->pCurrentBuffer, (UINT16)uBufLen, 0);


        pRequest->pCurrentBuffer += uBufLen;
        pRequest->uActLength += uBufLen;

        /* Check if this packet is the last one */

        if ((uBufLen < USB_MHDRC_TCD_EP0_MAX_FIFO_SIZE) ||
            (pRequest->uActLength == pRequest->uXferSize))
            {
            uCsr0 = (USB_MHDRC_PERI_CSR0_SERV_RXPKTRDY |
                    USB_MHDRC_PERI_CSR0_DATAEND);
            pTCDData->ep0Stage = USB_MHDRC_TCD_STAGE_STATUS_OUT;
            }
        else
            {
            uCsr0 = USB_MHDRC_PERI_CSR0_SERV_RXPKTRDY;
            }
        /*
         * we need to sync the cache before we read the data in
         */
        }
    else
        {
        uCsr0 = (USB_MHDRC_PERI_CSR0_SERV_RXPKTRDY |
                   USB_MHDRC_PERI_CSR0_SENDSTALL);

        }

    USB_MHDRC_REG_WRITE16(pMHDRC,
                          USB_MHDRC_PERI_CSR0,
                          uCsr0);
    }

/*******************************************************************************
*
* usbMhdrcTcdEp0ReqHandle - handle endpoint[0]'s request
*
* This routine is to handle endpoint[0]'s request.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcTcdEp0ReqHandle
    (
    pUSB_MHDRC_TCD_DATA      pTCDData,
    pUSB_MHDRC_TCD_REQUEST   pRequest
    )
    {
    USB_MHDRC_DBG("usbMhdrcTcdEp0ReqHandle - ep0Stage = %d, pRequest = %p\n",
                  pTCDData->ep0Stage, pRequest, 3, 4, 5, 6);

    if (pTCDData->ep0Stage == USB_MHDRC_TCD_STAGE_DATA_OUT)
        {
        usbMhdrcTcdEp0TxReqHandle(pTCDData, pRequest);
        }
    else if (pTCDData->ep0Stage == USB_MHDRC_TCD_STAGE_DATA_IN)
        {
        usbMhdrcTcdEp0RxReqHandle(pTCDData, pRequest);
        }

    return;
    }

/*******************************************************************************
*
* usbMhdrcTcdAddressSet - process the set address command
*
* This routine is to process the set address command.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcTcdAddressSet
    (
    pUSB_MHDRC_TCD_DATA pTCDData,
    UINT16              uAddr
    )
    {
    pUSB_MUSBMHDRC pMHDRC = NULL;
    pUSBTGT_TCD    pTCD = NULL;

    pTCD = pTCDData->pTCD;
    pMHDRC = pTCDData->pMHDRC;

    /* Record the address */

    pTCD->uAddrToSet = (UINT8)uAddr;

    /* Update the stage */

    pTCDData->ep0Stage = USB_MHDRC_TCD_STAGE_STATUS_IN;

    /* End the transfer */

    pTCDData->uCsr0 |= USB_MHDRC_PERI_CSR0_DATAEND;
    USB_MHDRC_REG_WRITE16(pMHDRC, USB_MHDRC_PERI_CSR0, pTCDData->uCsr0);
    pTCDData->uCsr0 = 0;
    return ;
    }

/*******************************************************************************
*
* usbMhdrcTcdFeatureClear - process the clear feature command
*
* This routine is to process the clear feature command.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcTcdFeatureClear
    (
    pUSB_MHDRC_TCD_DATA    pTCDData,
    pUSB_MHDRC_TCD_REQUEST pRequest,
    UINT8                  uRecipient,
    UINT16                 uFeature,
    UINT16                 uIndex
    )
    {
    UINT16         uEpAddr = 0;
    UINT16         uCsrReg = 0;
    STATUS         result = ERROR;
    pUSB_MUSBMHDRC pMHDRC = NULL;
    pUSB_MHDRC_TCD_PIPE    pTCDPipe = NULL;
    pUSB_MHDRC_TCD_REQUEST pCurRequest = NULL;

    /*
     * This function handle clear endpoint halt
     * Other will report to the TGT layer to handle.
     */

    pMHDRC = pTCDData->pMHDRC;

    switch (uRecipient)
        {
        case USB_RT_ENDPOINT:

            uEpAddr = (UINT16)(uIndex & 0x0F);

            if ((uEpAddr == 0)||(uEpAddr >= pMHDRC->uNumEps)||
                (uFeature != USB_FSEL_DEV_ENDPOINT_HALT))
                break;

            if (uIndex & USB_ENDPOINT_IN)
                {
                pTCDPipe = usbMhdrcTcdGetPipeByEpAddr(pTCDData, uEpAddr, USB_DIR_IN);
                uCsrReg = USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_PERI_TXCSR_EP(uEpAddr));
                uCsrReg |= (USB_MHDRC_PERI_TXCSR_CLRDATATOG |
                             USB_MHDRC_PERI_TXCSR_WZC_BITS);

                uCsrReg = (UINT16)(uCsrReg & ~(USB_MHDRC_PERI_TXCSR_SENDSTALL |
                                               USB_MHDRC_PERI_TXCSR_SENTSTALL |
                                               USB_MHDRC_PERI_TXCSR_TXPKTRDY));

                USB_MHDRC_REG_WRITE16(pMHDRC, USB_MHDRC_PERI_TXCSR_EP(uEpAddr), uCsrReg);
                }
            else
                {
                pTCDPipe = usbMhdrcTcdGetPipeByEpAddr(pTCDData, uEpAddr, USB_DIR_OUT);
                uCsrReg = USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_PERI_RXCSR_EP(uEpAddr));

                uCsrReg |= (USB_MHDRC_PERI_RXCSR_CLRDATATOG |
                             USB_MHDRC_PERI_RXCSR_WZC_BITS);
                uCsrReg = (UINT16)(uCsrReg & ~(USB_MHDRC_PERI_RXCSR_SENDSTALL |
                                               USB_MHDRC_PERI_RXCSR_SENTSTALL));

                USB_MHDRC_REG_WRITE16(pMHDRC, USB_MHDRC_PERI_RXCSR_EP(uEpAddr), uCsrReg);
                }
            result = OK;

            /* The pipe can be reused, restart to handle the request */

            pCurRequest = usbMhdrcTcdFirstReqGet(pTCDPipe);

            if (pCurRequest != NULL)
                usbMhdrcTcdReqHandle(pTCDData, pCurRequest);
            break;
        case USB_RT_DEVICE:
            /* Fall through */
        case USB_RT_INTERFACE:
            /* Fall through */
        default:
            pTCDData->uCsr0 |= USB_MHDRC_PERI_CSR0_DATAEND;
            USB_MHDRC_REG_WRITE16(pMHDRC, USB_MHDRC_PERI_CSR0, pTCDData->uCsr0);
            pTCDData->uCsr0 = 0;
            pTCDData->ep0Stage = USB_MHDRC_TCD_STAGE_IDLE;
            usbMhdrcTcdReqComplete(pRequest,
                                   S_usbTgtTcdLib_ERP_RESTART);
           return ;
        }

    /* Set DATAEND to indicate no further data is expected for this request */

    pTCDData->uCsr0 |= USB_MHDRC_PERI_CSR0_DATAEND;

    if (result == OK)
        {
        pTCDData->ep0Stage = USB_MHDRC_TCD_STAGE_STATUS_IN;
        }
    else
        {
        pTCDData->uCsr0 |= USB_MHDRC_PERI_CSR0_SENDSTALL;
        pTCDData->ep0Stage = USB_MHDRC_TCD_STAGE_IDLE;
        }

    USB_MHDRC_REG_WRITE16(pMHDRC, USB_MHDRC_PERI_CSR0, pTCDData->uCsr0);

    pTCDData->uCsr0 = 0;

    return;
    }

/*******************************************************************************
*
* usbMhdrcTcdFeatureSet - process the set feature command
*
* This routine is to process the set feature command.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcTcdFeatureSet
    (
    pUSB_MHDRC_TCD_DATA    pTCDData,
    pUSB_MHDRC_TCD_REQUEST pRequest,
    UINT8                  uRecipient,
    UINT16                 uFeature,
    UINT16                 uIndex
    )
    {
    UINT16               uEpAddr = 0;
    UINT16               uCsrReg = 0;
    STATUS               result = ERROR;
    pUSB_MUSBMHDRC       pMHDRC = NULL;

    /*
     * This function handle set endpoint halt and set device test mode
     * Other will report to the TGT layer to handle.
     */

    pMHDRC = pTCDData->pMHDRC;

    switch (uRecipient)
        {
        case USB_RT_ENDPOINT:

            uEpAddr = (UINT16)(uIndex & 0x0F);

            if ((uEpAddr == 0) || (uEpAddr >= pMHDRC->uNumEps) ||
                (uFeature != USB_FSEL_DEV_ENDPOINT_HALT))
                break;

            if (uIndex & USB_ENDPOINT_IN)
                {
                uCsrReg = USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_PERI_TXCSR_EP(uEpAddr));

                if (uCsrReg & USB_MHDRC_PERI_TXCSR_FIFONOEMPTY)
                    uCsrReg |= USB_MHDRC_PERI_TXCSR_FLUSHFIFO;

                uCsrReg |= (USB_MHDRC_PERI_TXCSR_SENDSTALL |
                            USB_MHDRC_PERI_TXCSR_CLRDATATOG|
                            USB_MHDRC_PERI_TXCSR_WZC_BITS);

                USB_MHDRC_REG_WRITE16(pMHDRC, USB_MHDRC_PERI_TXCSR_EP(uEpAddr), uCsrReg);
                }
            else
                {
                uCsrReg = USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_PERI_RXCSR_EP(uEpAddr));

                uCsrReg |= (USB_MHDRC_PERI_RXCSR_SENDSTALL |
                            USB_MHDRC_PERI_RXCSR_FLUSHFIFO |
                            USB_MHDRC_PERI_RXCSR_CLRDATATOG |
                            USB_MHDRC_PERI_RXCSR_WZC_BITS);

                USB_MHDRC_REG_WRITE16(pMHDRC, USB_MHDRC_PERI_RXCSR_EP(uEpAddr), uCsrReg);
                }
            result = OK;
            break;
        case USB_RT_DEVICE:
            switch(uFeature)
                {
                case USB_FSEL_DEV_TEST_MODE:
                    switch (uIndex >> 8)
                        {
                        case USB_TEST_MODE_J_STATE:
                            pTCDData->uTestMode |= USB_MHDRC_TESTMODE_TEST_J;
                            break;
                        case USB_TEST_MODE_K_STATE:
                            pTCDData->uTestMode |= USB_MHDRC_TESTMODE_TEST_K;
                            break;
                        case USB_TEST_MODE_SE0_ACK:
                            pTCDData->uTestMode |= USB_MHDRC_TESTMODE_TEST_SE0_NAK;
                            break;
                        case USB_TEST_MODE_TEST_PACKET:
                            pTCDData->uTestMode |= USB_MHDRC_TESTMODE_TEST_PACKET;
                            break;
                        default:
                            /* we should stall for the unknown feature */
                            break;
                        }
                    result = OK;
                    break;
                case USB_FSEL_DEV_REMOTE_WAKEUP:
                case USB_FSEL_DEV_B_HNP_ENABLE:
                case USB_FSEL_DEV_A_HNP_SUPPORT:
                case USB_FSEL_DEV_A_ALT_HNP_SUPPORT:
                    pTCDData->uCsr0 |= USB_MHDRC_PERI_CSR0_DATAEND;
                    USB_MHDRC_REG_WRITE16(pMHDRC, USB_MHDRC_PERI_CSR0, pTCDData->uCsr0);
                    pTCDData->uCsr0 = 0;
                    pTCDData->ep0Stage = USB_MHDRC_TCD_STAGE_IDLE;
                    usbMhdrcTcdReqComplete(pRequest,
                                           S_usbTgtTcdLib_ERP_RESTART);
                    return ;
                }
        /* Fall through */
        case USB_RT_INTERFACE:
        case USB_RT_OTHER:
        default:
            pTCDData->uCsr0 |= USB_MHDRC_PERI_CSR0_DATAEND;
            USB_MHDRC_REG_WRITE16(pMHDRC, USB_MHDRC_PERI_CSR0, pTCDData->uCsr0);
            pTCDData->uCsr0 = 0;
            pTCDData->ep0Stage = USB_MHDRC_TCD_STAGE_IDLE;
            usbMhdrcTcdReqComplete(pRequest,
                                   S_usbTgtTcdLib_ERP_RESTART);
            return ;
        }

    /* Set DATAEND to indicate no further data is expected for this request */

    pTCDData->uCsr0 |= USB_MHDRC_PERI_CSR0_DATAEND;

    if (result == OK)
        {
        pTCDData->ep0Stage = USB_MHDRC_TCD_STAGE_STATUS_IN;
        }
    else
        {
        pTCDData->uCsr0 |= USB_MHDRC_PERI_CSR0_SENDSTALL;
        pTCDData->ep0Stage = USB_MHDRC_TCD_STAGE_IDLE;
        }

    USB_MHDRC_REG_WRITE16(pMHDRC, USB_MHDRC_PERI_CSR0, pTCDData->uCsr0);
    pTCDData->uCsr0 = 0;
    return ;
    }

/*******************************************************************************
*
* usbMhdrcTcdSetupHandle - handle setup command
*
* This routine is to handle all the setup commands.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcTcdSetupHandle
    (
    pUSB_MHDRC_TCD_DATA    pTCDData,
    pUSB_MHDRC_TCD_PIPE    pTCDPipe,
    pUSB_MHDRC_TCD_REQUEST pRequest
    )
    {
    UINT8               uRecipient = 0;
    UINT16              uValue = 0;
    UINT16              uIndex = 0;
    UINT16              uLen = 0;
    pUSB_SETUP          pSetup = NULL;
    pUSB_MUSBMHDRC      pMHDRC = NULL;


    USB_MHDRC_TCD_VAILD_TCDDATA_PARAMETER(pTCDData);

    if ((pTCDPipe == NULL) ||
        (pRequest == NULL) ||
        (pRequest->pErp == NULL))
        {
        USB_MHDRC_ERR("Invalid Parameter: pTCDPipe %p, pRequest %p\n",
                      pTCDPipe, pRequest, 3, 4, 5, 6);
        return;
        }

    pMHDRC = pTCDData->pMHDRC;
    pSetup = (pUSB_SETUP)(pRequest->pErp->bfrList[0].pBfr);

    USB_MHDRC_DBG("usbMhdrcTcdSetupHandle requestType %x request %x value %x index %x length %d\n",
        pSetup->requestType, pSetup->request, pSetup->value, pSetup->index, pSetup->length, 6);

    /* Set SERV_RXPKTRDY to indicate the command has been read from the FIFO */

    pTCDData->uCsr0 = USB_MHDRC_PERI_CSR0_SERV_RXPKTRDY;

    if (pSetup->length == 0)
        {
        if (pSetup->requestType & USB_RT_DEV_TO_HOST)
            {
            /* Set TXPKTRDY to indicate a packet in the FIFO to be sent */

            pTCDData->uCsr0 |= USB_MHDRC_PERI_CSR0_TXPKTRDY;
            }
        pTCDData->ep0Stage = USB_MHDRC_TCD_STAGE_ACK_WAIT;
        }
    else if (pSetup->requestType & USB_RT_DEV_TO_HOST)
        {
        pTCDData->ep0Stage = USB_MHDRC_TCD_STAGE_DATA_OUT;
        USB_MHDRC_REG_WRITE16(pMHDRC,
                              USB_MHDRC_PERI_CSR0,
                              USB_MHDRC_PERI_CSR0_SERV_RXPKTRDY);
        while (0 !=
                   (USB_MHDRC_REG_READ16(pMHDRC,USB_MHDRC_PERI_CSR0) &
                    USB_MHDRC_PERI_CSR0_RXPKTRDY))
            {
            OS_DELAY_MS(1);
            }
        pTCDData->uCsr0 = 0;
        }
    else
        pTCDData->ep0Stage = USB_MHDRC_TCD_STAGE_DATA_IN;

    uRecipient = (UINT8)(pSetup->requestType & USB_RT_RECIPIENT_MASK);
    uValue = OS_UINT16_LE_TO_CPU(pSetup->value);
    uIndex = OS_UINT16_LE_TO_CPU(pSetup->index);
    uLen = OS_UINT16_LE_TO_CPU(pSetup->length);

    USB_MHDRC_DBG("Receive a setup packet "
                  "request %p, requestType %p, value %p, index %p, length %p\n",
                  pSetup->request,
                  pSetup->requestType,
                  pSetup->value,
                  pSetup->index,
                  pSetup->length, 6);

    switch (pTCDData->ep0Stage)
        {
        case USB_MHDRC_TCD_STAGE_ACK_WAIT:
            /*
             * The following command will handle in this routine
             * SET_FEATURE/CLEAR_FEATURE/SET_ADDRESS/SET_CONFIGURATION/SET_INTERFACE
             * The process is:
             * 1:Decode the command, if unrecognized go to 5, or else go to next step
             * 2:Set SERV_RXPKTPDY to indicate the command has been read from the FIFO
             * 3:Set DATAEND to indicate no further data is expected for this request
             * 4:At the status of the related command, service the EP0 interrupt
             *   to indicate the request is completed, Return.
             * 5:Set SERV_RXPKTPDY and SENDSTALL
             * 6:At the status of the related command, servie the EP0 SENTSTALL
             *   interrupt, Return.
             */

            if ((pSetup->requestType & USB_RT_CATEGORY_MASK) == USB_RT_STANDARD)
                {
                switch (pSetup->request)
                    {
                    case USB_REQ_SET_ADDRESS:
                        usbMhdrcTcdAddressSet(pTCDData, uValue);
                        return;
                    case USB_REQ_CLEAR_FEATURE:
                        usbMhdrcTcdFeatureClear(pTCDData,
                                                       pRequest,
                                                       uRecipient,
                                                       uValue,
                                                       uIndex);
                        return;
                    case USB_REQ_SET_FEATURE:
                        usbMhdrcTcdFeatureSet(pTCDData,
                                                     pRequest,
                                                     uRecipient,
                                                     uValue,
                                                     uIndex);
                        return;
                    case USB_REQ_SET_CONFIGURATION:
                    case USB_REQ_SET_INTERFACE:
                        pTCDData->uCsr0 |= USB_MHDRC_PERI_CSR0_DATAEND;
                        USB_MHDRC_REG_WRITE16(pMHDRC,
                                              USB_MHDRC_PERI_CSR0,
                                              pTCDData->uCsr0);
                        pTCDData->uCsr0 = 0;
                        pTCDData->ep0Stage = USB_MHDRC_TCD_STAGE_IDLE;
                        usbMhdrcTcdReqComplete(pRequest,
                                               S_usbTgtTcdLib_ERP_RESTART);
                        return;
                    default:
                        pTCDData->uCsr0 |= USB_MHDRC_PERI_CSR0_SENDSTALL;
                        pTCDData->ep0Stage = USB_MHDRC_TCD_STAGE_IDLE;
                        break;
                    }
                }
            else
                {
                /* Non-stand request zero length request */

                if (pSetup->length == 0)
                    {
                    pTCDData->uCsr0 |= USB_MHDRC_PERI_CSR0_DATAEND;
                    USB_MHDRC_REG_WRITE16(pMHDRC,
                                          USB_MHDRC_PERI_CSR0,
                                          pTCDData->uCsr0);
                    pTCDData->uCsr0 = 0;
                    pTCDData->ep0Stage = USB_MHDRC_TCD_STAGE_IDLE;
                    usbMhdrcTcdReqComplete(pRequest,
                                           S_usbTgtTcdLib_ERP_RESTART);
                    return;
                    }
                }
            break;
        case USB_MHDRC_TCD_STAGE_DATA_OUT:
            if ((pSetup->requestType & USB_RT_CATEGORY_MASK) == USB_RT_STANDARD)
                {
                /*
                 * The following request will handle in this routine
                 * GET_CONFIGURATION/GET_INTERFACE/GET_DESCRIPTOR/
                 * GET_STATUS/SYNCH_FRAME
                 */
                switch (pSetup->request)
                    {
                    case USB_REQ_GET_STATUS:
                    case USB_REQ_GET_CONFIGURATION:
                    case USB_REQ_GET_INTERFACE:
                    case USB_REQ_GET_DESCRIPTOR:
                    case USB_REQ_GET_SYNCH_FRAME:
                        break;
                    default:
                        pTCDData->uCsr0 |= USB_MHDRC_PERI_CSR0_SENDSTALL;
                        pTCDData->ep0Stage = USB_MHDRC_TCD_STAGE_IDLE;
                        break;
                    }
                }
            break;
        default:
            break;
        }

    USB_MHDRC_REG_WRITE16(pMHDRC, USB_MHDRC_PERI_CSR0, pTCDData->uCsr0);
    pTCDData->uCsr0 = 0;
    usbMhdrcTcdReqComplete(pRequest,
                           S_usbTgtTcdLib_ERP_SUCCESS);
    return;
    }

/*******************************************************************************
*
* usbMhdrcTcdCppiDmaCompletionTx - MHDRC TCD DMA Transmission Completion processes
*
* This routine processes MHDRC TCD CPPI DMA Transmission Completion
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcTcdCppiDmaCompletionTx
    (
    pUSB_MUSBMHDRC   pMHDRC,
    UINT32           txInterr
    )
    {
    pUSB_MHDRC_CPPIDMA_PKT_DESC  pCurrentPd;
    UINT32   index;
    UINT32   uReg32;
    UINT32   uPdAddr = 0;
    UINT16   csr;
    UINT8    epNum;


    pUSB_MHDRC_DMA_DATA       pDMAData = NULL;
    pUSB_MHDRC_TCD_REQUEST    pRequest = NULL;
    pUSB_MHDRC_TCD_DATA       pTCDData = NULL;
    UINT32                    uDmaCount   = 0;


    pDMAData = &(pMHDRC->dmaData);
    pTCDData = (pUSB_MHDRC_TCD_DATA)pMHDRC->pTCDData;

    for (index = 0; txInterr != 0; txInterr >>= 1, index++)
        {
        if (txInterr & 1)
            {
            epNum = (UINT8)(index + 1);

            /* Find the related pRequest */

            pRequest = (pUSB_MHDRC_TCD_REQUEST) pDMAData->dmaChannel[index].pRequest;

            if ((pRequest == NULL) ||
                (pRequest->pTCDPipe == NULL))
                {
                USB_MHDRC_ERR("usbMhdrcTcdCppiDmaCompletionTx(): "
                              "pRequestInChannel NULL\n",
                              1, 2, 3, 4, 5, 6);
                continue;
                }

            for(;;)
                {
                /* Read  */

                uReg32 = USB_MHDRC_QMGR_REG_READ32(pMHDRC,
                              QMGR_QUEUE_CTRL_D(index + USB_MHDRC_CPPI_TXQ_START_NUM + pDMAData->uDmaOffset));

                uPdAddr = uReg32 & QMGR_QUEUE_DESC_PTR_MASK;

                pCurrentPd = (pUSB_MHDRC_CPPIDMA_PKT_DESC)
                              (USB_MHDRC_PHYS_TO_VIRT(uPdAddr));
                if(!pCurrentPd)
                    break;

                /* Extract data from received packet descriptor */

                epNum = pCurrentPd->epNum;
                uDmaCount = pCurrentPd->bufLen;

                semTake(pDMAData->pSynchMutex, WAIT_FOREVER);
                usbMhdrcCppiDmaPDPut(pDMAData->pCppi, pCurrentPd);
                semGive(pDMAData->pSynchMutex);

                /* Wait for data to be completely output FIFO */

                for(;;)
                    {
                    csr = USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_PERI_TXCSR_EP(epNum));
                    if(!(csr & USB_MHDRC_PERI_TXCSR_TXPKTRDY))
                        break;
                    }

                /* free the dma channel */

                pRequest->uCurXferLength = uDmaCount;

                usbMhdrcTcdDmaTxReqHandle(pTCDData, pRequest);

                USB_MHDRC_VDBG("usbMhdrcTcdCppiDmaCompletionTx(): "
                               "Released TCD event 0x%x\n",
                               pTCDData->uTcdTxIrqStatus, 2, 3, 4, 5, 6);
                }
            }
        }
    }

/*******************************************************************************
*
* usbMhdrcTcdCppiDmaCompletionRx - MHDRC TCD DMA Receive Completion processes
*
* This routine processes MHDRC TCD CPPI DMA Receive Completion
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcTcdCppiDmaCompletionRx
    (
    pUSB_MUSBMHDRC   pMHDRC,
    UINT32           rxInterr
    )
    {
    pUSB_MHDRC_CPPIDMA_PKT_DESC  pCurrentPd;

    UINT16 uCsrReg16   = 0;
    UINT32 index;
    UINT32 uReg32;
    UINT8  epNum;

    pUSB_MHDRC_DMA_DATA       pDMAData = NULL;
    pUSB_MHDRC_TCD_REQUEST    pRequest = NULL;
    pUSB_MHDRC_TCD_DATA       pTCDData = NULL;
    UINT32                    uDmaCount   = 0;

    pDMAData = &(pMHDRC->dmaData);
    pTCDData = (pUSB_MHDRC_TCD_DATA)pMHDRC->pTCDData;

    for (index = 0; rxInterr != 0; rxInterr >>= 1, index++)
        {
        if (rxInterr & 1)
            {
            epNum = (UINT8)(index + 1);

            /* Find the related pRequest */

            pRequest = (pUSB_MHDRC_TCD_REQUEST) pDMAData->dmaChannel[index].pRequest;

            if ((pRequest == NULL) ||
                (pRequest->pTCDPipe == NULL))
                {
                USB_MHDRC_ERR("usbMhdrcTcdCppiDmaCompletionRx: "
                               "pRequestInChannel NULL\n",
                               1, 2, 3, 4, 5, 6);
                continue;
                }

            for(;;)
                {

                /* queue_pop */

                uReg32 = USB_MHDRC_QMGR_REG_READ32(pMHDRC,
                     QMGR_QUEUE_CTRL_D(USB_MHDRC_CPPI_RXQ_START_NUM + pDMAData->uDmaOffset + index));

                pCurrentPd = (pUSB_MHDRC_CPPIDMA_PKT_DESC)
                    (USB_MHDRC_PHYS_TO_VIRT(uReg32 & QMGR_QUEUE_DESC_PTR_MASK));

                if(!pCurrentPd)
                    break;

                /* Extract data from received packet descriptor */

                epNum = pCurrentPd->epNum;
                uDmaCount = pCurrentPd->bufLen;

                semTake(pDMAData->pSynchMutex, WAIT_FOREVER);
                usbMhdrcCppiDmaPDPut(pDMAData->pCppi, pCurrentPd);
                semGive(pDMAData->pSynchMutex);

                /* Should free the dma channel set as free status */

                pRequest->uActLength += uDmaCount;

                uCsrReg16 = USB_MHDRC_REG_READ16(pMHDRC,
                                                 USB_MHDRC_PERI_RXCSR_EP(epNum));

                uCsrReg16 = (USB_MHDRC_PERI_RXCSR_WZC_BITS);

                if (pRequest->uActLength == pRequest->uXferSize)
                    uCsrReg16 = (UINT16)(uCsrReg16 & ~USB_MHDRC_PERI_RXCSR_RXPKTRDY);

                USB_MHDRC_REG_WRITE16(pMHDRC,
                                      USB_MHDRC_PERI_RXCSR_EP(epNum),
                                      uCsrReg16);

                pRequest->pCurrentBuffer += uDmaCount;

                if ((pRequest->uActLength == pRequest->uXferSize) ||
                    (pRequest->uCurXferLength < pRequest->pTCDPipe->uMaxPacketSize))
                    {
                    usbMhdrcTcdReqComplete(pRequest, S_usbTgtTcdLib_ERP_SUCCESS);
                    }
                else
                    {
                    usbMhdrcTcdRxReqHandle(pTCDData, pRequest);
                    }
                }
            }
        }
    }

/*******************************************************************************
*
* usbMhdrcTcdCppiDmaInterruptHandler - interrupt handler for internal CPPI DMA
*
* This routine is to handle the internal CPPI DMA's interrupt.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcTcdCppiDmaInterruptHandler
    (
    pUSB_MUSBMHDRC  pMHDRC,
    UINT8           uChannel
    )
    {
    UINT32 queueCompStat0;
    UINT32 queueCompStat1;
    UINT32 usbTxInter;
    UINT32 usbRxInter;

    /* Parameter verification */

    if ((NULL == pMHDRC) || (NULL == pMHDRC->pTCDData))
        {
        /* Debug print */

        USB_MHDRC_ERR("usbMhdrcTcdCppiDmaInterruptHandler(): "
                      "Invalid parameter, pTCDData is NULL\n",
                      1, 2, 3, 4, 5 ,6);

        return;
        }

    /* DMA completion status Read */

    if ((pMHDRC->uRegBase & USB_MHDRC_USBx_CHECK_ADR_MASK) == 0x000)
        {
        /* USB 0 */
        /* This field indicates the queue pending status for queues[95:64] */

        queueCompStat0 = USB_MHDRC_QMGR_REG_READ32(pMHDRC, QMGR_QUEUE_PEND_REG2);

        /* This field indicates the queue pending status for queues[127:96] */

        queueCompStat1 = USB_MHDRC_QMGR_REG_READ32(pMHDRC, QMGR_QUEUE_PEND_REG3);

        /* USB tx completion interrupt for ep1..15  (queue 93 - 107) */

        usbTxInter = ((queueCompStat0 & USB_MHDRC_QUERE_93_95) >> 29) |
                     ((queueCompStat1 & USB_MHDRC_QUEUE_96_107) << 3);

        /* USB rx completion interrupt for ep1..15 (Queue 109 - 123) */

        usbRxInter = ((queueCompStat1 & USB_MHDRC_QUEUE_109_123) >> 13);
        }
    else
        {
        /* USB 1 */
        /* This field indicates the queue pending status for queues[125:127] */

        queueCompStat0 = USB_MHDRC_QMGR_REG_READ32(pMHDRC, QMGR_QUEUE_PEND_REG3);

        /* This field indicates the queue pending status for queues[128155] */

        queueCompStat1 = USB_MHDRC_QMGR_REG_READ32(pMHDRC, QMGR_QUEUE_PEND_REG4);

        /* USB tx completion interrupt for ep1..15  (queue 125 - 139) */

        usbTxInter = ((queueCompStat0 & 0xE0000000) >> 29) |
                     ((queueCompStat1 & 0x00000FFF) << 3);

        /* USB rx completion interrupt for ep1..15 (Queue 141 - 123) */

        usbRxInter = ((queueCompStat1 & 0x07FFE000) >> 13);
        }

    if (usbTxInter)
        {
        usbMhdrcTcdCppiDmaCompletionTx(pMHDRC, usbTxInter);
        }

    if (usbRxInter)
        {
        usbMhdrcTcdCppiDmaCompletionRx(pMHDRC, usbRxInter);
        }

    return;
    }

