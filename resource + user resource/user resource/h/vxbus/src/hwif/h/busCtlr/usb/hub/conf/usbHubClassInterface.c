/* usbHubClassInterface.c - functions used by USB host stack */

/*
 * Copyright (c) 2003-2005, 2008-2015 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2003-2005, 2008-2015 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
Modification history
--------------------
02c,14dec15,whu  Remove the waiting for completion of ClearTT (VXW6-84918)
02b,03feb15,wyy  Reduce waiting time of usbHubClearTT() (VXW6-83997)
02a,06Jun14,wyy  Return after usbHubClearTT totally completed (VXW6-82955)
01z,03may13,wyy  Remove compiler warning (WIND00356717)
01y,04jan13,s_z  Remove compiler warning (WIND00390357)
01x,12nov12,w_x  remove vxbBusRemovalAnnounce() (WIND00387497)
01w,17oct12,w_x  Fix compiler warnings (WIND00370525)
01v,26sep12,w_x  Correct the way to calculate Route String (WIND00378361)
01u,19sep12,w_x  Delay device removal during device mounting reset (WIND00373115)
01t,04sep12,w_x  Fix host controller driver exit issue (WIND00372413)
01s,24aug12,w_x  Fix task schedule issue in single BusM mode (WIND00370558)
01r,19jul12,w_x  Add support for USB 3.0 host (WIND00188662)
01q,13dec11,m_y  Modify according to code check result (WIND00319317)
01p,12aug11,w_x  Fix usbHstResetDevice() when called in BusM task (WIND00244001)
01o,28jul10,ghs  Modify for coding convention
01n,08jul10,m_y  Modify for coding convention
01m,29jun10,w_x  Add more port reset delay and clean hub logging (WIND00216628)
01l,24may10,m_y  Modify OS_ASSERT to if judgement for usb robustness (WIND00183499)
01k,27apr10,j_x  Error debug adaption and hard code replacement (WIND00183874)
01j,27jan10,y_l  Change hub device reset (WIND00151882)
01i,11mar10,j_x  Changed for USB debug (WIND00184542)
01h,13jan10,ghs  vxWorks 6.9 LP64 adapting
01g,17jul09,w_x  Fix crash in reset callback (WIND00172353)
01f,28feb09,w_x  Cancel status change URB in usbHubRemove before destroy it
                 (WIND00156002)
01e,17dec08,w_x  Added mutex to protect hub status data ;
                 Use seperate copy of hub status to avoid potential corruption;
                 Use DMA/cache safe buffer for hub status (WIND00148225)
01d,12dec05,ami  Fix for SPR #114308
01c,15oct04,ami  Refgen Changes
01b,11oct04,ami  Apigen Changes
01a,27jun03,nrv  Changing the code to WRS standards
*/

/*
DESCRIPTION

This module implements the functions to be used by the USB Host Software Stack.
The following functions are described in this module:
                    1.    Function for plug and play.
                    2.    Function for power management.
                    3.    Function for root hub management.
                    4.    Function for selective suspend and resume.
                    5.    Function for checking hub power capability.
                    6.    Function for resetting a device.

INCLUDE FILES: usb2/usbOsal.h, usb2/usbHubCommon.h, usb2/usbHubGlobalVariables.h,
usb2/usbHubUtility.h, usb2/usbHubClassInterface.h

*/

/*
INTERNAL
*******************************************************************************
 * Filename         : HUB_ClassInterface.c
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      : This module implements the functions to be used by the
 *                    USB Host Software Stack. The following functions are
 *                    described in this module:
 *                    1.    Function for plug and play.
 *                    2.    Function for power management.
 *                    3.    Function for root hub management.
 *                    4.    Function for selective suspend and resume.
 *                    5.    Function for checking hub power capability.
 *                    6.    Function for resetting a device.
 *
 *
 ******************************************************************************/



/************************** INCLUDE FILES *************************************/


#include "usb/usbOsal.h"
#include "usbHubCommon.h"
#include "usbHubGlobalVariables.h"
#include "usbHubUtility.h"
#include "usbHubClassInterface.h"
#include "usb/usbd.h"

extern pUSB_HUB_BUS_INFO gpGlobalBus;
extern USBHST_FUNCTION_LIST g_usbHstFunctionList;
/************************ GLOBAL FUNCTIONS DEFINITION *************************/

/***************************************************************************
*
* usbHubAdd - called by USB Host Software for adding a new hub.
*
* This routine is called by USB Host Software for adding a new hub and
* to configure it.
*
* RETURNS:
* USBHST_INVALID_PARAMETER for invalid parameters
* USBHST_FAILURE for failure to add hub
* USBHST_SUCCESS for success
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubAdd
    (
    UINT32 uDeviceHandle,
    UINT8 uInterfaceNumber,
    UINT8 uSpeed,
    void ** pContext
    )
    {
    /* pointer to the parent hub */

    pUSB_HUB_INFO      pParentHub  = NULL;

    /* port where the child hub is connected */

    UINT8          uPortIndex  = 0;

    /* Storage for the results */

    USBHST_STATUS  Result      = USBHST_FAILURE;

    /* The new hub pointer */

    pUSB_HUB_INFO      pHub        = NULL;

    /* The port Information */

    pUSB_HUB_PORT_INFO pPort       = NULL;

    /* Pointer to the device information */

    pUSBD_DEVICE_INFO pDeviceInfo = NULL;

    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
        USB_HUB_WV_DEVICE,
        "Entering usbHubAdd() Function",
        USB_HUB_WV_FILTER);

    /* Debug Message */

    USB_HUB_DBG("usbHubAdd - adding a new hub 0x%X, " \
                "InterfaceNumbere=0x%X, pContext=0x%X \n",
                uDeviceHandle,
                uInterfaceNumber,
                (ULONG)*pContext, 4, 5, 6);

    /*
     * Call HUB_FindParentHubInBuses() to find the parent hub and If this
     * is not found, then return USBHST_INVALID_PARAMETER.
     */

    pParentHub = usbHubFindParentHubInBuses(uDeviceHandle);

    /* if not found return USBHST_INVALID_PARAMETER */

    if (NULL == pParentHub)
        {
        /* Debug Message */

        USB_HUB_ERR("parent hub not found for hub 0x%X\n",
                    uDeviceHandle, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of  if (NULL == pParentHub ) */

    /*
     * Call HUB_FindPortNumber() to find the port number for this device handle.
     * If this is not found, then return USBHST_INVALID_PARAMETER.
     */

    uPortIndex = usbHubFindPortNumber(pParentHub, uDeviceHandle);

    /* if not found return USBHST_INVALID_PARAMETER */

    if (USB_PORT_NUMBER_NOT_FOUND == uPortIndex)
        {
        USB_HUB_ERR("port number not found for hub 0x%X on parent hub 0x%X\n",
                    uDeviceHandle, pParentHub->uDeviceHandle, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of if (PORT_NUMBER_NOT_FOUND == uPortIndex ) */

    /* Extract the port info from parent */

    pPort = pParentHub->pPortList[uPortIndex];

    /* Check if parent hub has a port info structure assigned for the port */

    if (NULL == pPort)
        {
        /* Debug Message */

        USB_HUB_ERR("no device on the parent hub port %d\n",
                    uPortIndex, 2, 2, 4, 5, 6);

        return USBHST_FAILURE;
        }

    /* Check if the port is a already set as a hub device */

    if (NULL != pPort->pHub)
        {
        USB_HUB_WARN("pPort->pHub already set as %p, "
                     "hub handle 0x%X on parent port %d\n",
                     pPort->pHub,
                     pPort->pHub->uDeviceHandle, uPortIndex, 4, 5, 6);

        /*
         * If it is already set, no need to do more,
         * so return USBHST_SUCCESS
         */

        return USBHST_SUCCESS;
        }

    /*
     * Call the HUB_ConfigureHub() to configure the hub. If the function
     * call fails:
     * i.    Return result returned by HUB_ConfigureHub() function call.
     */

    Result = usbHubConfigure(&pHub, uDeviceHandle, pParentHub->pBus, pParentHub);

    /* Check for the result and pHub is NULL or not */

    if ((USBHST_SUCCESS != Result) || (pHub == NULL))
        {
        /* Debug Message */

        USB_HUB_ERR("failed to configure hub 0x%X on parent hub 0x%X result=%d\n",
                    uDeviceHandle, pParentHub->uDeviceHandle, Result, 4, 5, 6);

        return USBHST_FAILURE;
        }/* End of (USBHST_SUCCESS !=Result ) */

#ifdef USB_HUB_CREATE_BUS_INSTANCE_FOR_EACH_HUB

    /*
     * Announce the presence of VXB_USB_HUB_BUS whenever a new hub is
     * configured.
     */

    /* Obtain the device information from the device handle */

    usbdTranslateDeviceHandle (uDeviceHandle, &pDeviceInfo);

    /* Announce the bus presence to the vxBus */

    if (vxbBusAnnounce (pDeviceInfo->pDev, VXB_BUSID_USB_HUB) != OK)
        {
        /* Debug Message */

        USB_HUB_ERR("vxbBusAnnounce failed \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_FAILURE;
        }
#endif

    /* Record our parent port information */

    pHub->pParentPort = pPort;

    /* Attach the HUB_INFO structure to the parent hub. */

    /* Update the hub information (pPort has been validated to be non-NULL) */

    pPort->pHub = pHub;

    /* Store the parent hub pointer into pContext. */

    *pContext = (void *) pParentHub;

    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
        USB_HUB_WV_DEVICE,
        "Exiting usbHubAdd() Function",
        USB_HUB_WV_FILTER);

    /* Return USBHST_SUCCESS. */
    return USBHST_SUCCESS;

} /* End of function HUB_AddHub() */

/***************************************************************************
*
* usbHubRemove - delete a hub.
*
* This routine is called by the USB Host Software Stack for deletion of a hub.
*
* RETURNS: N/A
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL void usbHubRemove
    (
    UINT32 uDeviceHandle,
    void *  pContext
    )
    {
    /* The pointer to the parent hub */
    pUSB_HUB_INFO pParentHub       = NULL;

    /* port number of the device */
    UINT8 uPortIndex          = 0;

    /* port count for the hub  */
    UINT8 uPortCount           = 0;

    /* Storage for the hub structure */
    pUSB_HUB_INFO pHub             = NULL;

    /* The port Information */
    pUSB_HUB_PORT_INFO pPort       = NULL;

    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
        USB_HUB_WV_DEVICE,
        "Entering usbHubRemove() Function",
        USB_HUB_WV_FILTER);

    /* Debug Message */

    USB_HUB_DBG("usbHubRemove - delete a hub 0x%X, pContext=%p\n",
                uDeviceHandle, pContext, 3, 4, 5, 6);

    /* If pContext is NULL then return. */
    if (NULL == pContext)
        {
        /* Debug Message */
        USB_HUB_ERR("parameter invalid \n",
                    1, 2, 3, 4, 5, 6);

        return;

        } /* End of if (NULL==pContext..*/

    /* Get the parent hub pointer from the pContext. */
    pParentHub = (pUSB_HUB_INFO) pContext;

    /* Search for the DeviceID in the parent hub. If failed return. */

    /* get the port number */
    uPortIndex = usbHubFindPortNumber(pParentHub,uDeviceHandle);

    /* Check  if the port is found */
    if (USB_PORT_NUMBER_NOT_FOUND == uPortIndex)
        {
        USB_HUB_ERR("port number not found for hub 0x%X on parent hub 0x%X\n",
                    uDeviceHandle, pParentHub->uDeviceHandle, 3, 4, 5, 6);

        return;

        } /* End of if (PORT_NUM.. */

    /* Check if the port number is within limits */
    if (uPortIndex > (pParentHub->HubDescriptor.bNbrPorts))
        {
        USB_HUB_ERR("port num %d > max %d \n",
                    uPortIndex,
                    pParentHub->HubDescriptor.bNbrPorts,
                    3, 4, 5, 6);

        return;

        } /* End of (uPortIndex > (pParentHub->.... */

    /*
     * Retrieve HUB_INFO structure from
     * parentHub::pPortList[uPortIndex]::pHub.
     */
    pPort = pParentHub->pPortList[uPortIndex];

    /* update the hub information */

    pHub = pPort->pHub;

    /* If the HUB_INFO structure is NULL then return. */
    if (NULL == pHub)
        {
        USB_HUB_ERR("hub information is NULL (not a hub?) \n",
                    1, 2, 3, 4, 5, 6);

        return;

        } /* End of if (NULL== pHub) */


    /*
     * Call the USBHST_CancelURB() function to cancel the Status change
     * interrupt IN request URB.
     */

    /* No need to check URB cancel result */

    (void)usbHstURBCancel(&(pHub->StatusChangeURB));


    /*
     * for all the ports in the pHub::pPortList  which are enabled,
     *  i.    Call the HUB_RemoveDevice() function for
     *        the pHub::pPortList[uPortCount]::uDeviceHandle.
     *  ii.   Call the OS_FREE() function to free the
     *        pHub::pPortList[uPortCount]
     *  iii.  Set the pHub::pPortList[uPortCount] as NULL
     */
    for (uPortCount = 0;
         uPortCount < pHub->HubDescriptor.bNbrPorts;
         uPortCount++)
        {
        /* Retrieve the HUB_PORT_INFO structure */
        pUSB_HUB_PORT_INFO pPort = pHub->pPortList[uPortCount];
        /* Check if the port is enabled */
        if (NULL != pPort)
            {
            /* Debug Message */

            USB_HUB_DBG("usbHubRemove - delete a device on hub 0x%X port 0x%X\n",
                         pHub->uDeviceHandle, uPortCount, 3, 4, 5, 6);

            /* 9/5/2k3:NM: Changed here to centralise the effect*/
            usbHubRemoveDevice(pHub, uPortCount);

            } /* End of If (NULL != pPort) */

        } /* End of for (uPortCount.... */

    /*
     * Call the OS_FREE() function to free all the allocated memory for
     * the hub.
     */

    /* Get the parent port */
    pPort = pParentHub->pPortList[uPortIndex];

    /* Destroy the port status semaphore */

    if (pPort != NULL)
        {
        /* Setting parent port port as not enabled */

        pParentHub->pPortList[uPortIndex] = NULL;

        if (pPort->hPortStatusSem != NULL)
            {
            OS_DESTROY_EVENT(pPort->hPortStatusSem);
            /* Set the handle as NULL */

            pPort->hPortStatusSem = NULL;
            }

        /* Free the memory */
        OS_FREE(pPort);
        }

    /*
     * Set the StatusChangeURB->pContext to NULL to avoid the callback function
     * called after the hub remove
     */

    (&(pHub->StatusChangeURB))->pContext= NULL;

    /* Delete the mutex */
    if (pHub->hubMutex)
        {
        /*
         * Take the mutex before delete it, this should serve as a way
         * to wait all ongoing requests done before proceed to free the
         * hub structures
         */
        /* No need to check semTake return value */
        (void)semTake(pHub->hubMutex, WAIT_FOREVER);

        /* Now delete the mutex */
        semDelete(pHub->hubMutex);
        pHub->hubMutex = NULL;
        }

    if (pHub->pHubStatus)
        {
        OS_FREE(pHub->pHubStatus);      /* Free the hub status */
        pHub->pHubStatus = NULL;
        }

    if (pHub->pCurrentStatus)
        {
        OS_FREE (pHub->pCurrentStatus); /* Free the status bitmap */
        pHub->pCurrentStatus = NULL;
        }

    if (pHub->pCopiedStatus)
        {
        OS_FREE (pHub->pCopiedStatus);  /* Free the status bitmap */
        pHub->pCopiedStatus = NULL;
        }

    if (pHub->StatusChangeURB.pTransferBuffer)
        {
        OS_FREE (pHub->StatusChangeURB.pTransferBuffer);  /* Free URB Buffer */
        pHub->StatusChangeURB.pTransferBuffer = NULL;
        }

    OS_FREE (pHub);                                   /* Free the Hub itself */

    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
        USB_HUB_WV_DEVICE,
        "Exiting usbHubRemove() Function",
        USB_HUB_WV_FILTER);

    return;
    } /* End of function HUB_RemoveHub() */

/***************************************************************************
*
* usbHubSuspend - called by the USB Host Software Stack for suspending a hub.
*
* This routine is called by the USB Host Software Stack for suspending a hub.
*
* RETURNS: N/A
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL void usbHubSuspend
    (
    UINT32 uDeviceHandle,
    void *  pContext
    )
    {
    /* The pointer to the parent hub */
    pUSB_HUB_INFO pParentHub       = NULL;

    /* port number of the device */
    UINT8 uPortIndex          = 0;

    /* port count for the hub  */
    UINT8 uPortCount           = 0;

    /* Storage for the hub structure */
    pUSB_HUB_INFO pHub             = NULL;

    /* The port Information */
    pUSB_HUB_PORT_INFO pPort       = NULL;

    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
        USB_HUB_WV_DEVICE,
        "Entering usbHubSuspend() Function",
        USB_HUB_WV_FILTER);

    /* If pContext is NULL then return. */
    if (NULL == pContext)
        {
        /* Debug Message */
        USB_HUB_ERR("parameter invalid \n",
                    1, 2, 3, 4, 5, 6);

        return;

        } /* End of if (NULL==pContext..*/

    /* Get the parent hub pointer from the pContext. */
    pParentHub = (pUSB_HUB_INFO) pContext;

    /* Search for the DeviceID in the parent hub. If failed return. */

    /* get the port number */
    uPortIndex = usbHubFindPortNumber(pParentHub,uDeviceHandle);

    /* Check  if the port is found */
    if (USB_PORT_NUMBER_NOT_FOUND == uPortIndex)
        {
        /* Debug Message */
        USB_HUB_ERR("port number not found \n",
                    1, 2, 3, 4, 5, 6);

        return;

        } /* End of if (PORT_NUM.. */

    /* Check if the port number is within limits */
    if (uPortIndex > (pParentHub->HubDescriptor.bNbrPorts))
        {
        /* Debug Message */

        USB_HUB_ERR("port num %d > max %d \n",
                    uPortIndex,
                    pParentHub->HubDescriptor.bNbrPorts,
                    3, 4, 5, 6);
        return;

        } /* End of (uPortIndex > (pParentHub->.... */

    /*
     * Retrieve HUB_INFO structure from
     * parentHub::pPortList[uPortIndex]::pHub.
     */
    pPort = pParentHub->pPortList[uPortIndex];

    /* update the hub information */

    pHub = pPort->pHub;


    /* If the HUB_INFO structure is NULL then return. */
    if (NULL == pHub)
        {
        /* Debug Message */
        USB_HUB_ERR("hub information is NULL \n",
                    1, 2, 3, 4, 5, 6);

        return;

        } /* End of if (NULL== pHub) */


    /* Call the USBHST_CancelURB function to cancel the URB for the hub. */

    /* No need to check URB cancel result */

    (void)usbHstURBCancel(&(pHub->StatusChangeURB));

    /*
     * for all the ports in the pHub::pPortList  which are enabled,
     * i.    Call the g_USBHSTFunctionList::USBHST_SuspendDevice() function
     *       for the pHub::pPortList[uPortCount]::DeviceHandle.
     */
    for (uPortCount = 0;
         uPortCount < pHub->HubDescriptor.bNbrPorts;
         uPortCount++)
        {
        /* Retrieve the HUB_PORT_INFO structure */
        pUSB_HUB_PORT_INFO pPort = pHub->pPortList[uPortCount];
        /* Check if the port is enabled */
        if (NULL != pPort)
            {
            /* Debug Message */

            USB_HUB_DBG("usbHubSuspend - suspend a device on hub 0x%X port %d\n",
                         pHub->uDeviceHandle, uPortCount, 3, 4, 5, 6);

            /* Call function to suspend the device */
            g_usbHstFunctionList.UsbdToHubFunctionList.suspendDevice
                (
                pPort->uDeviceHandle
                );

            } /* End of If (NULL != pPort) */

        } /* End of for (uPortCount.... */

    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
        USB_HUB_WV_DEVICE,
        "Exiting usbHubSuspend() Function",
        USB_HUB_WV_FILTER);

    /* Return */
    return;
    } /* End of function HUB_SuspendHub() */

/***************************************************************************
*
* usbHubResume - called by the USB Host Software Stack  for resuming a hub.
*
* This routine is called by the USB Host Software Stack  for resuming a hub.
*
* RETURNS: N/A
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL void usbHubResume
    (
    UINT32 uDeviceHandle,
    void * pContext
    )
    {

    /* The pointer to the parent hub */
    pUSB_HUB_INFO pParentHub       = NULL;

    /* port number of the device */
    UINT8 uPortIndex          = 0;

    /* port count for the hub  */
    UINT8 uPortCount           = 0;

    /* Storage for the hub structure */
    pUSB_HUB_INFO pHub             = NULL;

    /* The port Information */
    pUSB_HUB_PORT_INFO pPort       = NULL;

    /* Result storage */
    USBHST_STATUS Result;

    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
            USB_HUB_WV_DEVICE,
            "Entering usbHubResume() Function",
            USB_HUB_WV_FILTER);

    /* If pContext is NULL then return. */

    if (NULL == pContext)
        {
        /* Debug Message */
        USB_HUB_ERR("parameter invalid \n",
                    1, 2, 3, 4, 5, 6);

        return;

        } /* End of if (NULL==pContext..*/

    /* Get the parent hub pointer from the pContext. */

    pParentHub= (pUSB_HUB_INFO) pContext;

    /* get the port number */
    uPortIndex = usbHubFindPortNumber(pParentHub, uDeviceHandle);

    /* Check  if the port is found */
    if (USB_PORT_NUMBER_NOT_FOUND == uPortIndex)
        {
        /* Debug Message */
        USB_HUB_ERR("port number not found \n",
                    1, 2, 3, 4, 5, 6);

        return;

        } /* End of if (PORT_NUM.. */

    /* Check if the port number is within limits */
    if (uPortIndex > (pParentHub->HubDescriptor.bNbrPorts))
        {
        /* Debug Message */
        USB_HUB_ERR("port num %d > max %d \n",
                    uPortIndex,
                    pParentHub->HubDescriptor.bNbrPorts,
                    3, 4, 5, 6);

        return;

        } /* End of (uPortIndex > (pParentHub->.... */

    /*
     * Retrieve HUB_INFO structure from
     * parentHub::pPortList[uPortIndex]::pHub.
     */

    pPort = pParentHub->pPortList[uPortIndex];

    /* update the hub information */

    pHub = pPort->pHub;

    /* If the HUB_INFO structure is NULL then return. */

    if (NULL == pHub)
        {
        /* Debug Message */
        USB_HUB_ERR("hub information is NULL \n",
                    1, 2, 3, 4, 5, 6);

        return;

        } /* End of if (NULL== pHub) */

    /*
     * Call the HUB_SubmitInterruptRequest() function to submit the URB for the
     * hub.
     */

    Result = usbHubSubmitInterruptRequest(pHub);

    /* If the SubmitInterruptRequest fails then,
     *  i.    Call the HUB_SubmitFeature() to disable the port.
     * ii.    Call the HUB_RemoveDevice() function to
     *        delete the device.
     * iii.   Return.
     */

    if (USBHST_SUCCESS != Result)
        {
        USB_HUB_ERR("failed to submit interrupt request for hub 0x%X\n",
                    pHub->uDeviceHandle, 2, 3, 4, 5, 6);

        /* Disable the port */
        Result = USB_HUB_CLEAR_PORT_FEATURE(pParentHub,uPortIndex,USB_PORT_ENABLE);

        /* 9/5/2k3:NM: Changed here to centralise the effect*/
        usbHubRemoveDevice(pParentHub,uPortIndex);

        /* return */
        return;

        }/* End of if (USBHST_SUCCESS != Result ) */

    /*
     * for all the ports in the pHub::pPortList  which are enabled,
     * i.    Call the g_USBHSTFunctionList::USBHST_ResumeDevice() function
     *       for the pHub::pPortList[uPortCount]::DeviceHandle.
     */
    for (uPortCount = 0;
         uPortCount < pHub->HubDescriptor.bNbrPorts;
         uPortCount++)
        {
        /* Retrieve the HUB_PORT_INFO structure */

        pUSB_HUB_PORT_INFO pPort = pHub->pPortList[uPortCount];

        /* Check if the port is enabled */

        if (NULL != pPort)
            {
            /* Debug Message */

            USB_HUB_DBG("usbHubResume - resume a device on hub 0x%X port %d\n",
                         pHub->uDeviceHandle, uPortCount, 3, 4, 5, 6);

            /* Call function to resume the device */
            g_usbHstFunctionList.UsbdToHubFunctionList.resumeDevice
                (
                pPort->uDeviceHandle
                );

            } /* End of If (NULL != pPort) */

        } /* End of for (uPortCount.... */

    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
        USB_HUB_WV_DEVICE,
        "Exiting usbHubResume() Function",
        USB_HUB_WV_FILTER);

    return;
    } /* End of function HUB_ResumeHub() */

/***************************************************************************
*
* usbHubSelectiveSuspendDevice - selectively suspending a device
*
* Called by the USB Host Software Stack for selectively suspending a device.
* In this process, it selectively suspends  the port of the parent hub where
* the device is connected.
*
* RETURNS: N/A
*
* ERRNO: None
*
* \NOMANUAL
*/
LOCAL USBHST_STATUS usbHubSelectiveSuspendDevice
    (
    UINT32 uDeviceHandle
    )
    {
    /* The pointer to the parent hub */
    pUSB_HUB_INFO pParentHub       = NULL;

    /* port number of the device */
    UINT8 uPortIndex          = 0;

    /* The port Information */
    pUSB_HUB_PORT_INFO pPort       = NULL;

    /* Storage for the result */
    USBHST_STATUS  Result;

    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
            USB_HUB_WV_DEVICE,
            "Entering usbHubSelectiveSuspendDevice() Function",
            USB_HUB_WV_FILTER);

    /*
     * Call HUB_FindParentHubInBuses() to find the parent hub and If this
     * is not found, then return USBHST_INVALID_PARAMETER.
     */

    pParentHub = usbHubFindParentHubInBuses(uDeviceHandle);

    /* if not found return USBHST_INVALID_PARAMETER */

    if (NULL == pParentHub)
        {
        /* Debug Message */
        USB_HUB_ERR("parent hub not found \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of if (NULL == pParentHub ) */

    /*
     * HUB_FindPortNumber() to find the port number for this device handle.
     * If this is not found, then return USBHST_INVALID_PARAMETER.
     */
    uPortIndex = usbHubFindPortNumber(pParentHub, uDeviceHandle);

    /* Check  if the port is found */
    if (USB_PORT_NUMBER_NOT_FOUND == uPortIndex)
        {
        /* Debug Message */
        USB_HUB_ERR("port number not found \n",
                    1, 2, 3, 4, 5, 6);
        return USBHST_INVALID_PARAMETER;

        } /* End of if (PORT_NUM.. */

    /* Check if the port number is within limits */
    if (uPortIndex >= (pParentHub->HubDescriptor.bNbrPorts))
        {
        /* Debug Message */
        USB_HUB_ERR("port num %d > max %d \n",
                    uPortIndex,
                    pParentHub->HubDescriptor.bNbrPorts,
                    3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        } /* End of (uPortIndex > (pParentHub->.... */

    /*
     * Retrieve HUB_INFO structure from
     * parentHub::pPortList[uPortIndex]::pHub.
     */

    pPort = pParentHub->pPortList[uPortIndex];

    if (pPort->pHub != NULL)
        {
        USB_HUB_ERR("The pHub of this port is not NULL\n", 1, 2, 3, 4, 5, 6);
        return USBHST_FAILURE;
        }

    /*
     * If this is SuperSpeed bus, we can only set the link state
     * to U3 which is the same as suspend.
     */

    if (pParentHub->pBus->uBusSpeed == USBHST_SUPER_SPEED)
        {
        Result = USB_HUB_SET_PORT_LINK_STATE(pParentHub,
                                             uPortIndex,
                                             USB3_PORT_PLS_U3);
        }
    else
        {
        /*
         * Call the HUB_SubmitFeature() function to submit a selective
         * suspend to the parent hub on the port number.
     * Note: The actual port number is 1 more than the port index
     */

        Result = USB_HUB_SET_PORT_FEATURE(pParentHub,
                                          uPortIndex,
                                          USB_PORT_SUSPEND);
        }

    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
            USB_HUB_WV_DEVICE,
            "Exiting usbHubSelectiveSuspendDevice() Function",
            USB_HUB_WV_FILTER);

    /* Return the result of the submission.*/
    return Result;

    } /* End of function HUB_SelectiveSuspendDevice() */

/***************************************************************************
*
* usbHubSelectiveResumeDevice - selectively resuming a device.
*
* Called by the USB Host Software Stack for selectively resuming a device.
* In this process it selectively resumes the port of the parent hub where the
* device is connected.
*
*
* RETURNS: USBHST_STATUS
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubSelectiveResumeDevice
    (
    UINT32 uDeviceHandle
    )
    {
    /* The pointer to the parent hub */
    pUSB_HUB_INFO pParentHub       = NULL;

    /* port number of the device */
    UINT8 uPortIndex          = 0;

    /* The port Information */
    pUSB_HUB_PORT_INFO pPort       = NULL;

    /* The Result Storage */
    USBHST_STATUS Result      = USBHST_FAILURE;

    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
            USB_HUB_WV_DEVICE,
            "Entering usbHubSelectiveResumeDevice() Function",
            USB_HUB_WV_FILTER);

    /*
     * Call HUB_FindParentHubInBuses() to find the parent hub and If this
     * is not found, then return USBHST_INVALID_PARAMETER.
     */
    pParentHub = usbHubFindParentHubInBuses(uDeviceHandle);

    /* if not found return USBHST_INVALID_PARAMETER */
    if (NULL == pParentHub)
        {
        /* Debug Message */
        USB_HUB_ERR("parent hub not found \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }/* End of if (NULL == pParentHub ) */

    /* Search for the DeviceID in the parent hub. If failed return. */

    /*
     * HUB_FindPortNumber() to find the port number for this device handle.
     * If this is not found, then return USBHST_INVALID_PARAMETER.
     */
    uPortIndex = usbHubFindPortNumber(pParentHub,uDeviceHandle);

    /* Check  if the port is found */
    if (USB_PORT_NUMBER_NOT_FOUND == uPortIndex)
        {
        /* Debug Message */
        USB_HUB_ERR("port number not found \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        } /* End of if (PORT_NUM.. */

    /* Check if the port number is within limits */
    if (uPortIndex >= (pParentHub->HubDescriptor.bNbrPorts))
        {
        /* Debug Message */
        USB_HUB_ERR("port num %d > max %d \n",
                    uPortIndex,
                    pParentHub->HubDescriptor.bNbrPorts,
                    3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        } /* End of (uPortIndex > (pParentHub->.... */

    /*
     * Retrieve HUB_INFO structure from
     * parentHub::pPortList[uPortIndex]::pHub.
     */
    pPort = pParentHub->pPortList[uPortIndex];

    if (pPort->pHub != NULL)
        {
        USB_HUB_ERR("The pHub of this port is not NULL\n", 1, 2, 3, 4, 5, 6);
        return USBHST_FAILURE;
        }
    /*
     * If this is SuperSpeed bus, we can only set the link state
     * to U0 which is the same as resume.
     */

    if (pParentHub->pBus->uBusSpeed == USBHST_SUPER_SPEED)
        {
        Result = USB_HUB_SET_PORT_LINK_STATE(pParentHub,
                                             uPortIndex,
                                             USB3_PORT_PLS_U0);
        }
    else
        {
        /*
         * Call the USB_HUB_CLEAR_PORT_FEATURE() function to submit a selective
         * resume to the parent hub on the port number.
     * Note: actual port number is 1 more than the port index.
     */
        Result =  USB_HUB_CLEAR_PORT_FEATURE(pParentHub,
                                             uPortIndex,
                                             USB_PORT_SUSPEND);
        }

    /* resume recovery period of 10 ms */
    OS_DELAY_MS(10);

    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
        USB_HUB_WV_DEVICE,
        "Exiting usbHubSelectiveResumeDevice() Function",
        USB_HUB_WV_FILTER);

    /* Return the result of the submission.*/
    return Result;

    } /* End of function usbHubSelectiveResumeDevice() */

/***************************************************************************
*
* usbHubHandleTranslate - translate a hub device handle to pUSB_HUB_INFO
*
* This routine translates a hub device handle to pUSB_HUB_INFO.
*
* RETURNS: pUSB_HUB_INFO for that hub handle
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_HUB_INFO usbHubHandleTranslate
    (
    UINT32 uDeviceHandle
    )
    {
    /* The pointer to the parent hub */

    pUSB_HUB_INFO pParentHub = NULL;

    /* port number of the device */

    UINT8 uParentPortIndex  = 0;

    /* The port Information */

    pUSB_HUB_PORT_INFO pPort = NULL;

    /* The bus List pointer to be used for browsing the list */

    pUSB_HUB_BUS_INFO pBusList = gpGlobalBus;

    while (pBusList != NULL)
        {
        if (pBusList->uDeviceHandle == uDeviceHandle)
            {
            return pBusList->pRootHubInfo;
            }

        pBusList = pBusList->pNextBus;
        }

    /*
     * Call usbHubFindParentHubInBuses() to find the parent hub
     * and If this is not found, then return USBHST_INVALID_PARAMETER.
     */

    pParentHub = usbHubFindParentHubInBuses(uDeviceHandle);

    /* If not found return USBHST_INVALID_PARAMETER */

    if (NULL == pParentHub)
        {
        USB_HUB_ERR("parent hub not found \n",
                    1, 2, 3, 4, 5, 6);

        return NULL;
        }

    /*
     * usbHubFindPortNumber() to find the port number for this device handle.
     * If this is not found, then return USBHST_INVALID_PARAMETER.
     */

    uParentPortIndex = usbHubFindPortNumber(pParentHub, uDeviceHandle);

    /* Check if the port is found */

    if (USB_PORT_NUMBER_NOT_FOUND == uParentPortIndex)
        {
        USB_HUB_ERR("port number not found \n",
                    1, 2, 3, 4, 5, 6);

        return NULL;
        }

    /* Check if the port number is within limits */

    if (uParentPortIndex >= (pParentHub->HubDescriptor.bNbrPorts))
        {
        USB_HUB_ERR("port num %d >= max %d \n",
                    uParentPortIndex,
                    pParentHub->HubDescriptor.bNbrPorts,
                    3, 4, 5, 6);

        return NULL;
        }

    /*
     * Retrieve HUB_INFO structure from
     * parentHub::pPortList[uParentPortIndex]::pHub.
     */

    pPort = pParentHub->pPortList[uParentPortIndex];

    /* Return the hub device info */

    return pPort->pHub;
    }

/*******************************************************************************
*
* usbHubSetUxTimeout - set U1/U2 timeout for the hub port (USB3 only)
*
* This routine is to set U1/U2 timeout for the hub port (USB3 only).
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbHubSetUxTimeout
    (
    UINT32 uDeviceHandle,
    UINT32 uPortIndex,
    UINT32 uUxSelection,
    UINT32 uTimeout
    )
    {
    /* Storage for the result */

    USBHST_STATUS  Result;

    /* The pointer to the this hub */

    pUSB_HUB_INFO pThisHub = NULL;

    if ((uUxSelection != USB3_HUB_FSEL_PORT_U1_TIMEOUT) &&
        (uUxSelection != USB3_HUB_FSEL_PORT_U2_TIMEOUT))
        {
        USB_HUB_ERR("uUxSelection %d invalid\n",
            uDeviceHandle, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }

    pThisHub = usbHubHandleTranslate(uDeviceHandle);

    if (pThisHub == NULL)
        {
        USB_HUB_ERR("usbHubHandleTranslate failed for handle 0x%x\n",
            uDeviceHandle, 2, 3, 4, 5, 6);

        return USBHST_FAILURE;
        }

    /* Check if the port number is within limits */

    if (uPortIndex >= (pThisHub->HubDescriptor.bNbrPorts))
        {
        USB_HUB_ERR("The hub port num %d >= max %d \n",
                    uPortIndex,
                    pThisHub->HubDescriptor.bNbrPorts,
                    3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }

    /* This is only supported by USB3 */

    if (pThisHub->pBus->uBusSpeed == USBHST_SUPER_SPEED)
        {
        Result = usbHubSubmitControlRequest(pThisHub,
                             USB_PORT_TARGET_SET,
                             USB_SET_FEATURE,
                             (UINT16)uUxSelection,
                             (UINT16)((uTimeout << 8) | (uPortIndex + 1)));
        }
    else
        {
        /* This is not supported */

        Result = USBHST_INVALID_REQUEST;
        }

    /* Return the result of the submission.*/

    return Result;
    }

/*******************************************************************************
*
* usbHubSetRemoteWakeupMask - set remote wake up mask for the hub port (USB3 only)
*
* This routine is to set remote wake up mask for the hub port (USB3 only).
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbHubSetRemoteWakeupMask
    (
    UINT32 uDeviceHandle,
    UINT32 uPortIndex,
    UINT32 uMask
    )
    {
    /* Storage for the result */

    USBHST_STATUS  Result;

    /* The pointer to the this hub */

    pUSB_HUB_INFO pThisHub = NULL;

    pThisHub = usbHubHandleTranslate(uDeviceHandle);

    if (pThisHub == NULL)
        {
        USB_HUB_ERR("usbHubHandleTranslate failed for handle 0x%x\n",
            uDeviceHandle, 2, 3, 4, 5, 6);

        return USBHST_FAILURE;
        }

    /* Check if the port number is within limits */

    if (uPortIndex >= (pThisHub->HubDescriptor.bNbrPorts))
        {
        USB_HUB_ERR("The hub port num %d >= max %d \n",
                    uPortIndex,
                    pThisHub->HubDescriptor.bNbrPorts,
                    3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }

    /* This is only supported by USB3 */

    if (pThisHub->pBus->uBusSpeed == USBHST_SUPER_SPEED)
        {
        Result = usbHubSubmitControlRequest(pThisHub,
                             USB_PORT_TARGET_SET,
                             USB_SET_FEATURE,
                             USB3_HUB_FSEL_PORT_REMOTE_WAKE_MASK,
                             (UINT16)(((uMask) << 8) |((uPortIndex) + 1)));

        }
    else
        {
        /* This is not supported */

        Result = USBHST_INVALID_REQUEST;
        }

    /* Return the result of the submission.*/

    return Result;
    }

/*******************************************************************************
*
* usbHubForceLinkPmAccept - issue FORCE_LINKPM_ACCEPT for the hub port (USB3 only)
*
* This routine is to issue FORCE_LINKPM_ACCEPT for the hub port (USB3 only).
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbHubForceLinkPmAccept
    (
    UINT32 uDeviceHandle,
    UINT32 uPortIndex
    )
    {
    /* Storage for the result */

    USBHST_STATUS  Result;

    /* The pointer to the this hub */

    pUSB_HUB_INFO pThisHub = NULL;

    pThisHub = usbHubHandleTranslate(uDeviceHandle);

    if (pThisHub == NULL)
        {
        USB_HUB_ERR("usbHubHandleTranslate failed for handle 0x%x\n",
            uDeviceHandle, 2, 3, 4, 5, 6);

        return USBHST_FAILURE;
        }

    /* Check if the port number is within limits */

    if (uPortIndex >= (pThisHub->HubDescriptor.bNbrPorts))
        {
        USB_HUB_ERR("The hub port num %d >= max %d \n",
                    uPortIndex,
                    pThisHub->HubDescriptor.bNbrPorts,
                    3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }

    /* This is only supported by USB3 */

    if (pThisHub->pBus->uBusSpeed == USBHST_SUPER_SPEED)
        {
        Result = usbHubSubmitControlRequest(pThisHub,
                             USB_PORT_TARGET_SET,
                             USB_SET_FEATURE,
                             USB3_HUB_FSEL_FORCE_LINKPM_ACCEPT,
                             (UINT16)(((uPortIndex) + 1)));
        }
    else
        {
        /* This is not supported */

        Result = USBHST_INVALID_REQUEST;
        }

    /* Return the result of the submission.*/

    return Result;
    }

/*******************************************************************************
*
* usbHubSetPortLinkState - set hub device port link state
*
* This routine is to set hub device port link state of a device.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbHubSetPortLinkState
    (
    UINT32 uDeviceHandle,
    UINT32 uPortIndex,
    UINT32 uLinkState
    )
    {
    /* Storage for the result */

    USBHST_STATUS  Result;

    /* The pointer to the this hub */

    pUSB_HUB_INFO pThisHub = NULL;

    pThisHub = usbHubHandleTranslate(uDeviceHandle);

    if (pThisHub == NULL)
        {
        USB_HUB_ERR("usbHubHandleTranslate failed for handle 0x%x\n",
            uDeviceHandle, 2, 3, 4, 5, 6);

        return USBHST_FAILURE;
        }

    /* Check if the port number is within limits */

    if (uPortIndex >= (pThisHub->HubDescriptor.bNbrPorts))
        {
        USB_HUB_ERR("The hub port num %d >= max %d \n",
                    uPortIndex,
                    pThisHub->HubDescriptor.bNbrPorts,
                    3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }

    /* This is only supported by USB3 */

    if (pThisHub->pBus->uBusSpeed == USBHST_SUPER_SPEED)
        {
        Result = USB_HUB_SET_PORT_LINK_STATE(pThisHub, uPortIndex, uLinkState);
        }
    else
        {
        /* This is not supported */

        Result = USBHST_INVALID_REQUEST;
        }

    /* Return the result of the submission. */

    return Result;
    }

/*******************************************************************************
*
* usbHubSetPortLinkState - get hub device port link state
*
* This routine is to get hub device port link state of a device.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbHubGetPortLinkState
    (
    UINT32 uDeviceHandle,
    UINT32 uPortIndex,
    UINT32 * pLinkState
    )
    {
    /* Storage for the result */

    USBHST_STATUS  Result;

    /* The pointer to the this hub */

    pUSB_HUB_INFO pThisHub = NULL;

    pThisHub = usbHubHandleTranslate(uDeviceHandle);

    if (pThisHub == NULL)
        {
        USB_HUB_ERR("usbHubHandleTranslate failed for handle 0x%x\n",
            uDeviceHandle, 2, 3, 4, 5, 6);

        return USBHST_FAILURE;
        }

    /* Check if the port number is within limits */

    if (uPortIndex >= (pThisHub->HubDescriptor.bNbrPorts))
        {
        USB_HUB_ERR("The hub port num %d >= max %d \n",
                    uPortIndex,
                    pThisHub->HubDescriptor.bNbrPorts,
                    3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }

    /* This is only supported by USB3 */

    if (pThisHub->pBus->uBusSpeed == USBHST_SUPER_SPEED)
        {
        /* To store the result of the port status. */

        pUSB_HUB_PORT_STATUS pPortStatus = NULL;

        /* To store the length of the transfers */

        UINT8 uLength = sizeof(USB_HUB_PORT_STATUS);

        /* Port Status */

        UINT16 wPortStatus;

        /* Allocate memory for the port status */

        pPortStatus = OSS_CALLOC(sizeof(USB_HUB_PORT_STATUS));

        /* Check if memory allocation is successful */

        if (pPortStatus == NULL)
            {
            USB_HUB_ERR("memory allocate failed for port status \n",
                        1, 2, 3, 4, 5, 6);

            return USBHST_INSUFFICIENT_MEMORY;
            }

        /*
         * Call the HUB_GET_PORT_STATUS() function to retrieve
         * the HUB_PORT_STATUS structure. If failed, then return
         * USBHST_FAILURE.
         *
         * Note: actual port number is 1+ the port index
         */

        Result = USB_HUB_GET_PORT_STATUS(pThisHub,
                                         uPortIndex,
                                         (UINT8 *)(pPortStatus),
                                         &uLength);

        if ((USBHST_SUCCESS != Result) ||
            (uLength != sizeof(USB_HUB_PORT_STATUS)))
            {
            USB_HUB_ERR("get port status failed: result=%d, length=0x%X \n",
                        Result, uLength, 3, 4, 5, 6);

            /* Free the memory allocated for the port status */

            OS_FREE(pPortStatus);

            return USBHST_FAILURE;
            }

        wPortStatus = OS_UINT16_LE_TO_CPU(pPortStatus->wPortStatus);

        OSS_FREE(pPortStatus);

        if (pLinkState != NULL)
            *pLinkState = USB3_HUB_STS_PORT_PLS_GET(wPortStatus);

        logMsg("Port %d Link State %d\n",
               uPortIndex,
               USB3_HUB_STS_PORT_PLS_GET(wPortStatus), 3, 4, 5, 6);
        }
    else
        {
        /* This is not supported */

        Result = USBHST_INVALID_REQUEST;
        }

    /* Return the result of the submission.*/

    return Result;
    }

/***************************************************************************
*
* usbHubAddRoot - configures the root hub
*
* This function is called by the USB Host Software Stack  for adding a new root
* hub. In this process, it configures the root hub and also initializes the
* structures to handle  the bus under the root hub.
*
* RETURNS: N/A
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubAddRoot
    (
    UINT32 uDeviceHandle,
    UINT8 uBusHandle,
    UINT8 uSpeed
    )
    {
    /* Bus List pointer */

    pUSB_HUB_BUS_INFO pBus = NULL;

    /* Hub pointer */

    pUSB_HUB_INFO     pRootHub     = NULL;

    /* Results */

    USBHST_STATUS Result   = USBHST_FAILURE;

    /* Pointer to the device information */

    pUSBD_DEVICE_INFO pDeviceInfo = NULL;

    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
        USB_HUB_WV_DEVICE,
        "Entering usbHubAddRoot() Function",
        USB_HUB_WV_FILTER);

    /* Debug Message */

    USB_HUB_DBG("usbHubAddRoot - adding a new root hub 0x%X, " \
                "uBusHandle=0x%X, uSpeed=0x%X \n",
                uDeviceHandle, uBusHandle, uSpeed, 4, 5, 6);
    /*
     * Call the HUB_CreateBusStructure() function to create
     * HUB_BUS_INFO structure. If this fails, then return
     * USBHST_INVALID_PARAMETER.
     */
    pBus = usbHubCreateBusStructure (uBusHandle);

    if (NULL == pBus)
        {
        /* Debug Message */

        USB_HUB_ERR("failed to create bus structure \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Update the HUB_BUS_INFO::uDeviceHandle with the uDeviceHandle.*/

    pBus->uDeviceHandle = uDeviceHandle;

    /*
     * Call the HUB_ConfigureHub() function to configure the hub.
     * If the configuration fails then
     * i.    Call the HUB_DeleteBusStructure() function.
     * ii.   Return the result of the configuration.
     */
    Result = usbHubConfigure(&pRootHub, uDeviceHandle, pBus, NULL);

    /* Check the result */
    if ((USBHST_SUCCESS != Result) || (NULL == pRootHub) )
        {
        /* Debug Message */
        USB_HUB_ERR("failed to configure the root hub, result=0x%d \n",
                    Result, 2, 3, 4, 5, 6);

        /* Delete the Bus structure */
        usbHubDeleteBusStructure(uBusHandle);

        /* return the failure status */

        return USBHST_FAILURE;

        } /* End of if ((USBHST_SUCCESS != Result)|| (NULL == pRootHub) ) */

    /*
     * Announce the presence of VXB_USB_HUB_BUS whenever a new hub is
     * configured.
     */

    /* Obtain the device information from the device handle */

    usbdTranslateDeviceHandle (uDeviceHandle, &pDeviceInfo);

    /* Announce the bus presence to the vxBus */

    if (vxbBusAnnounce (pDeviceInfo->pDev, VXB_BUSID_USB_HUB) != OK)
        {
        /* Debug Message */

        USB_HUB_ERR("vxbBusAnnounce failed \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_FAILURE;
        }

    /*
     * Set the BUS_INFO::uBusSpeed as the uSpeed
     */
    pBus->uBusSpeed = uSpeed;

    /*
     * Set the BUS_INFO::pRootHubInfo as HUB_INFO structure returned by the
     * HUB_ConfigureHub() function.
     */
    pBus->pRootHubInfo = pRootHub;

    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
        USB_HUB_WV_DEVICE,
        "Exiting usbHubAddRoot() Function",
        USB_HUB_WV_FILTER);

    /* Return USBHST_SUCCESS */
    return USBHST_SUCCESS;
    } /* End of function HUB_AddRootHub() */

/***************************************************************************
*
* usbHubRemoveRoot - removes the root hub.
*
* This function is called by the USB Host Software Stack for removing a root
* hub.
*
* RETURNS: N/A
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL void usbHubRemoveRoot
    (
    UINT8 uBusHandle
    )
    {
    /* Bus List pointer */
    pUSB_HUB_BUS_INFO pBusList = gpGlobalBus;

    /* Root hub structure */
    pUSB_HUB_INFO pRootHub = NULL;

    /* Port counter */
    UINT8    uPortCount = 0;

    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
            USB_HUB_WV_DEVICE,
            "Entering usbHubRemoveRoot() Function",
            USB_HUB_WV_FILTER);

    /* Debug Message */

    USB_HUB_WARN("usbHubRemoveRoot - remove a root hub uBusHandle=0x%X\n",
                uBusHandle, 2, 3, 4, 5, 6);
    /*
     * Scan through the gpGlobalBus to find the matching HUB_BUS_INFO structure
     * for the uBusHandle. If not found return.
     */
    while (NULL != pBusList)
        {
        /* Check for duplicate entries */
        if (pBusList->uBusHandle == uBusHandle)
            {
            /* Jump out of the loop */
            break;
            } /* End of if (pBusList->....*/

        /* Move the pointer to the next bus */
        pBusList=pBusList->pNextBus;

        } /* End of While (NULL != pBusList) */

    /* Check if we found the bus */
    if (NULL == pBusList)
        {
        /* Debug Message */

        USB_HUB_ERR("bus not found for root hub, uBusHandle 0x%X\n",
                    uBusHandle, 2, 3, 4, 5, 6);

        /* nope.. we did not find the bus, so we return */
        return;

        } /* End of if (NULL==pBusList) */

    /* Retrieve the HUB_INFO structure from the HUB_BUS_INFO structure.*/
    pRootHub = pBusList->pRootHubInfo;
    if (NULL == pRootHub)
        {
        /* Debug Message */
        USB_HUB_ERR("root hub not found for uBusHandle 0x%X\n",
                    uBusHandle, 2, 3, 4, 5, 6);

        /* Call the HUB_DeleteBusStructure() function to delete bus structure. */
        usbHubDeleteBusStructure(uBusHandle);
        return;
        }

    /*
     * Call the USBHST_CancelURB() function to cancel the Status change
     * interrupt IN request URB.
     */

    /* No need to check URB cancel result */

    (void)usbHstURBCancel(&(pRootHub->StatusChangeURB));


    /*
     * If any of the HUB_PORT_INFO pointer of the HUB_BUS_INFO::pPortList is
     * enabled then call the g_USBHSTFunctionList::HUB_RemoveDevice().
     * Power down the port and free the resources allocated.
     */
    for (uPortCount = 0;
         uPortCount < pRootHub->HubDescriptor.bNbrPorts;
         uPortCount++)
        {
        /* To store the result of the transactions */
        USBHST_STATUS Result;

        /* Retrieve the HUB_PORT_INFO structure */

        pUSB_HUB_PORT_INFO pPort = pRootHub->pPortList[uPortCount];

        /* power off the port */

        Result = USB_HUB_CLEAR_PORT_FEATURE(pRootHub, uPortCount, USB_PORT_POWER);

        /* Check if the port is topology enabled */
        if (NULL != pPort)
            {
            /* Debug Message */

            USB_HUB_WARN("usbHubRemoveRoot - delete a device on root hub port %d\n",
                         uPortCount, 2, 3,4, 5, 6);

            /* let the USB host stack know.. that we have discon the device
             * Physically
             */

            /* 9/5/2k3:NM: Changed here to centralise the effect*/
            usbHubRemoveDevice(pRootHub, uPortCount);

            } /* End of If (NULL != pPort) */

        } /* End of for (uPortCount.... */

    /* Call the HUB_DeleteBusStructure() function to delete bus structure. */
    usbHubDeleteBusStructure(uBusHandle);

    /*
     * set (&(pRootHub->StatusChangeURB))->pContext to NULL to avoid the URB's
     * callback function to be called after the hub resource all released.
     */

    (&(pRootHub->StatusChangeURB))->pContext = NULL;

    /* Delete the mutex */
    if (pRootHub->hubMutex)
        {

        /*
         * Take the mutex before delete it, this should serve as a way
         * to wait all ongoing requests done before proceed to free the
         * hub structures
         */
        /* No need to check semTake return value */
        (void)semTake(pRootHub->hubMutex, WAIT_FOREVER);

        /* Now delete the mutex */
        semDelete(pRootHub->hubMutex);
        pRootHub->hubMutex = NULL;
        }

    /*
     * Call OS_FREE () to free the memory allocated for a root hub
     * HUB_INFO structure.
     */
    if (pRootHub->pHubStatus)
        {
        OS_FREE(pRootHub->pHubStatus);              /* Free the hub status */
        pRootHub->pHubStatus = NULL;
        }

    if (pRootHub->pCurrentStatus)
        {
        OS_FREE (pRootHub->pCurrentStatus);         /* Free the status bitmap */
        pRootHub->pCurrentStatus = NULL;
        }

    if (pRootHub->pCopiedStatus)
        {
        OS_FREE (pRootHub->pCopiedStatus);          /* Free the status bitmap */
        pRootHub->pCopiedStatus = NULL;
        }

    if (pRootHub->StatusChangeURB.pTransferBuffer)
        {
        OS_FREE (pRootHub->StatusChangeURB.pTransferBuffer);/* Free URB Buffer */
        pRootHub->StatusChangeURB.pTransferBuffer = NULL;
        }

    /* Free the Hub itself  */

    OS_FREE (pRootHub);

    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
        USB_HUB_WV_DEVICE,
        "Exiting usbHubRemoveRoot() Function",
        USB_HUB_WV_FILTER);

    /* Return */
    return;

    } /* End of function HUB_RemoveRootHub() */

/***************************************************************************
*
* usbHubCheckPower - check the parent hub can support a power for a device
*
* called by the USB Host Software Stack for checking if the  parent hub can
* support a power for a device.
*
* RETURNS: USBHST_SUCCESS, USBHST_FAILURE if the power cannot be supported.
*
* ERRNO: None
*
* \NOMANUAL
*/
LOCAL USBHST_STATUS usbHubCheckPower
    (
    UINT32 uDeviceHandle,
    UINT8  uPowerRequirement
    )
    {
    /* Storage for the parent hub structure */
    pUSB_HUB_INFO pParentHub             = NULL;

    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
            USB_HUB_WV_DEVICE,
            "Entering usbHubCheckPower() Function",
            USB_HUB_WV_FILTER);

    /*
     * Call HUB_FindParentHubInBuses() to find the parent hub and If this
     * is not found, then return USBHST_INVALID_PARAMETER.
     */
    /* Call HUB_FindParentHubInBuses() to find the parent hub. */

    pParentHub = usbHubFindParentHubInBuses(uDeviceHandle);

    /*
     * If parent hub pointer is not found then search through the bus structures
     * for BUS_INFO::uDeviceHandle  matching for the uDeviceHandle.
     * This is coz the device could be root hub. In that case we wont have a
     * parent hub.
     */
    if (NULL == pParentHub)
        {
        /* start searching thru the HUB_BUS_INFO list */
        pUSB_HUB_BUS_INFO pBusList = gpGlobalBus;
        /* scan thru all the buses available */
        while (NULL != pBusList)
            {
            /* check if this is a root hub */
            if (uDeviceHandle == pBusList->uDeviceHandle)
                {
                /* voila.. a root hub.. */
                /* If uPowerRequirement is less than MAXIMUM_POWER_PER_ROOT_HUB
                 * return USBHST_SUCCESS. Else return USBHST_FAILURE.
                 */
                if (USB_MAXIMUM_POWER_PER_ROOT_HUB >=uPowerRequirement)
                    {
                    /* Debug Message */

                    USB_HUB_VDBG("usbHubCheckPower - root hub power ok\n",
                                 1, 2, 3, 4, 5, 6);
                    /* ok power requirement- return success */
                    return USBHST_SUCCESS;
                    }
                else
                    {
                    /* Debug Message */
                    USB_HUB_ERR("root hub power failed \n",
                                1, 2, 3, 4, 5, 6);

                    /*
                     * root hub is asking for more power than we can provide.
                     * return failure.
                     */
                    return USBHST_FAILURE;
                    }

                } /* End of if (uDeviceHandle == pBusList->uDeviceHandle) */

            /* Move the pointer to the next bus */

            pBusList=pBusList->pNextBus;
            } /* End of while (NULL != pBusList) */

        if (pBusList == NULL)
            {
            /* Debug Message */

            USB_HUB_ERR("root hub not found \n",
                        1, 2, 3, 4, 5, 6);

            /* cannot find the root hub. return USBHST_INVALID_PARAMETER */
            return USBHST_INVALID_PARAMETER;

            }

        }/* End of if (NULL == pParentHub ) */

    /*
     * If this uPowerRequirement less than HUB_INFO::nPowerPerPort
     * then return USBHST_SUCCESS.
     */
    if (pParentHub->uPowerPerPort >= uPowerRequirement)
        {
        /* Debug Message */

        USB_HUB_VDBG("usbHubCheckPower - "
                    "parent hub 0x%X can support"
                    "power for a device 0x%X power requirement %d.\n",
                    pParentHub->uDeviceHandle,
                    uDeviceHandle,
                    uPowerRequirement, 4, 5, 6);

        return USBHST_SUCCESS;
        }

    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
        USB_HUB_WV_DEVICE,
        "Exiting usbHubCheckPower() Function",
        USB_HUB_WV_FILTER);

    /* Debug Message */

    USB_HUB_INFO("usbHubCheckPower - "
                 "parent hub 0x%X can not support "
                 "enough power for a device 0x%X power requirement %d.\n"
                 "Please check if the parent hub is correctly powered!\n"
                 "We will ignore this device for the sake of other devices!\n",
                 pParentHub->uDeviceHandle,
                 uDeviceHandle,
                 uPowerRequirement, 4, 5, 6);

    /* Return USBHST_FAILURE */
    return USBHST_FAILURE;

    } /* End of function HUB_CheckPower() */

/***************************************************************************
*
* usbHubResetDevice - reset the device.
*
* This function is called by the USB Host Software Stack for resetting a device.
*
* RETURNS: N/A
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubResetDevice
    (
    UINT32 uDeviceHandle,
    INT32  timeoutMS
    )
    {
    /* The pointer to the parent hub */
    pUSB_HUB_INFO pParentHub       = NULL;

    /* port number of the device */
    UINT8 uPortIndex          = 0;

    /* The port Information */
    pUSB_HUB_PORT_INFO pPort       = NULL;

    /* reset time out */
    INT32 timeout                  = 0;

    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
            USB_HUB_WV_DEVICE,
            "Entering usbHubResetDevice() Function",
            USB_HUB_WV_FILTER);

    /*
     * Call HUB_FindParentHubInBuses() to find the parent hub and If this is not
     * found, then return USBHST_INVALID_PARAMETER.
     */
    pParentHub = usbHubFindParentHubInBuses(uDeviceHandle);

    /* if not found return USBHST_INVALID_PARAMETER */
    if (NULL == pParentHub)
        {
        /* Debug Message */
        USB_HUB_ERR("parent hub not found \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }/* End of if (NULL == pParentHub ) */

    /*
     * Call HUB_FindPortNumber() to find the port number for this device handle.
     * If this is not found, then return USBHST_INVALID_PARAMETER.
     */
    uPortIndex = usbHubFindPortNumber(pParentHub, uDeviceHandle);

    /* Check  if the port is found */
    if (USB_PORT_NUMBER_NOT_FOUND == uPortIndex)
        {
        /* Debug Message */
        USB_HUB_ERR("port number not found \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        } /* End of if (PORT_NUM.. */

    /* Check if the port number is within limits */
    if (uPortIndex >= (pParentHub->HubDescriptor.bNbrPorts))
        {
        /* Debug Message */
        USB_HUB_ERR("port uPortIndex not correct \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        } /* End of (uPortIndex > (pParentHub->.... */

    /* Retrieve the HUB_PORT_INFO structure.*/

    pPort = pParentHub->pPortList[uPortIndex];

    if (pPort->pHub != NULL)
        {
        USB_HUB_ERR("pHub is not NULL\n", 1, 2, 3, 4, 5, 6);
        return USBHST_INVALID_PARAMETER;
        }

    /*
     * Mark the flag, after reset just call resetDeviceCheck instead of
     * Calling newDevice
     */
    pPort->bResetCheckDevice = TRUE;

    /* Set the HUB_PORT_INFO::StateOfPort as HUB_RESET_PENDING.*/
    pPort->StateOfPort = USB_HUB_RESET_PENDING;

    /* Debug Message */
    USB_HUB_DBG("usbHubResetDevice - waiting for reset completion... \n",
                1, 2, 3, 4, 5, 6);

    /*
     * If this routine is called from the BusM task (typically when called
     * by class drivers in the addDevice() or removeDevice() code), we have
     * to handle the reset and reset change in place (just in this routine)
     * directly, otherwise, it will enter the loop to wait the port status
     * to be changed from USB_HUB_RESET_PENDING to USB_HUB_PORT_CONFIGURED,
     * however since the change of the port status needs the BusM task to
     * run over again (usbHubPortEventHandler() should be called). This will
     * cause a dead lock since the BusM task is waiting for itself.
     *
     * So we just issue the reset directly here then wait the status change
     * and handle the reset change here. This makes reset as a synchronous
     * procedure, which is the expected behaviour.
     */
#ifndef USB_HUB_CREATE_BUSM_TASK_FOR_EACH_BUS
    if (taskIdSelf() == BusManagerThreadID)
#else
    if (taskIdSelf() == pParentHub->pBus->BusManagerThreadID)
#endif
        {
        USB_HUB_WARN("usbHubResetDevice - Resetting in the BusM task\n",
                1, 2, 3, 4, 5, 6);

        /* if the hub is marked for deletion fall thru to step 5 */

        if (USB_MARKED_FOR_DELETION != pParentHub->StateOfHub)
            {
            /*
             * Call usbHubSubmitInterruptRequest to submit an interrupt
             * URB. In this function we will judge if the URB is already
             * submitted or not.
             * If the URB already submitted we will return USBHST_SUCCESS.
             * or else we will submit an interrupt URB and return the submit
             * result.
             */

            /* If failed, delete the hub */
            if (USBHST_SUCCESS != usbHubSubmitInterruptRequest(pParentHub))
                {
                /* Mark the hub for deletion */
                pParentHub->StateOfHub = USB_MARKED_FOR_DELETION;

                USB_HUB_ERR("usbHubResetDevice - submit interrupt URB failed\n",
                            1, 2, 3, 4, 5, 6);

                return USBHST_FAILURE;
                } /* End of if (USBHST_SUCCESS !=Result) */

            }

        /* This actually will issue the Port Reset to the port */

        if (usbHubHandleDeviceConnection(pParentHub, uPortIndex)
            != USBHST_SUCCESS)
            {
            USB_HUB_ERR("usbHubHandleDeviceConnection failed\n",
                1, 2, 3, 4, 5, 6);

            /* Set the HUB_PORT_INFO::StateOfPort as USB_MARKED_FOR_DELETION.*/

            pPort->StateOfPort = USB_MARKED_FOR_DELETION;

            return USBHST_FAILURE;
            }

        /*
         * Wait the Interrupt URB callback to report a
         * Reset Status Change
         */

        while (TRUE != pParentHub->bCurrentStatusChanged)
            {
            OS_DELAY_MS(1);

            if (timeoutMS != WAIT_FOREVER)
                {
                timeout++;

                if (timeout > timeoutMS)
                    {
                    USB_HUB_ERR("usbHubResetDevice - timeout waiting change\n",
                                1, 2, 3, 4, 5, 6);

                    return USBHST_FAILURE;
                    }
                }
            }

        USB_HUB_DBG("usbHubResetDevice - bCurrentStatusChanged\n",
                1, 2, 3, 4, 5, 6);

        /* Reset the status change to FALSE */

        pParentHub->bCurrentStatusChanged = FALSE;

        /* Check if there is port event for this port */

        if (TRUE == USB_HUB_IS_PORT_EVENT(pParentHub->pCurrentStatus,
                                         (uPortIndex + 1)))
            {
            USB_HUB_DBG("usbHubResetDevice - port event detected\n",
                        1, 2, 3, 4, 5, 6);
            /*
             * Handle the port event (in this case it will be the
             * Reset Change Event triggered by the Port Reset)
             */

            usbHubPortEventHandler(pParentHub, uPortIndex);

            return USBHST_SUCCESS;
            }
        else
            {
            USB_HUB_ERR("usbHubResetDevice - port event not detected\n",
                        1, 2, 3, 4, 5, 6);

            return USBHST_FAILURE;
            }
        }

    /* Notify the BusM task to move on to issue the port reset */

#ifndef USB_HUB_CREATE_BUSM_TASK_FOR_EACH_BUS
    usbHubAddEvent(pParentHub);
#endif

    /* Delay 10 ms each time waiting for the complete of reset */
    if (timeoutMS != WAIT_FOREVER)
        timeoutMS /= 10;

    /* Begin to wait for the end state */

    while (1)
        {
        OS_DELAY_MS(10);

        /* Retrieve the HUB_PORT_INFO structure.*/

        pPort = pParentHub->pPortList[uPortIndex];

        /* The port might get removed while waiting for reset complete */

        if (pPort == NULL)
            {
            /* Debug Message */
            USB_HUB_ERR("device is removed while waiting for reset complete!\n",
                        1, 2, 3, 4, 5, 6);

            return USBHST_FAILURE;
            }

        /* If disconnect happens during reset, mark the port to be deleted */

        if (pPort->bDelayedRemove == TRUE)
            {
            /* Mark the port for deletion */

            pPort->StateOfPort = USB_MARKED_FOR_DELETION;

            /* Debug Message */
            USB_HUB_ERR("device removal is delayed while waiting for reset complete!\n",
                        1, 2, 3, 4, 5, 6);

            return USBHST_FAILURE;
            }

        if (USB_HUB_RESET_PENDING != pPort->StateOfPort)
            {
            /*
             * Parent hub got marked for deletion.. implies the reset would fail.
             */
            /* has been deleted */
            if (USB_MARKED_FOR_DELETION == pParentHub->StateOfHub )
                {
                /* Debug Message */
                USB_HUB_ERR("parent hub marked for delete \n",
                            1, 2, 3, 4, 5, 6);

                return USBHST_FAILURE;
                }

            /* reset successful, go to default, address, config state*/
            if (pPort->StateOfPort == USB_HUB_PORT_CONFIGURED)
                {
                /* Debug Message */
                USB_HUB_DBG("usbHubResetDevice - reset ok... \n",
                            1, 2, 3, 4, 5, 6);
                break;
                }
            }

        if (timeoutMS != WAIT_FOREVER)
            {
            timeout++;

            /* Check if timeout */
            if (timeout >= timeoutMS)
                {
                /* Debug Message */
                USB_HUB_ERR("reset device timeout \n",
                            1, 2, 3, 4, 5, 6);

                /* Reset failed, so mark the port as deletion state */
                pPort->StateOfPort = USB_MARKED_FOR_DELETION;

                return USBHST_FAILURE;
                }
            }
        }


    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
        USB_HUB_WV_DEVICE,
        "Exiting usbHubResetDevice() Function",
        USB_HUB_WV_FILTER);

    /* Return USBHST_SUCCESS.*/
    return USBHST_SUCCESS;

    } /* End of function HUB_ResetDevice() */

/***************************************************************************
*
* usbHubClearTT - submits a request for clear TT
*
* This routine submits a request for clear TT.
*
* RETURNS: USBHST_SUCCESS, USBHST_FAILURE if request submission is unsuccessful
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubClearTT
    (
    UINT32 uHubHandle,
    UINT8  uPortNumber,
    UINT16 uValue,
    void * pContext
    )
    {

    /* This is the URB */
    pUSBHST_URB pControlURB             =NULL;

    /* This stores the setup packet */
    pUSBHST_SETUP_PACKET pSetupPacket   =NULL;

    /* This is to store the request result */
    USBHST_STATUS Result                =USBHST_FAILURE;

    /* Port number to which the TT is attached on the hub */
    UINT16 uTTPortNumber                =0;

    /* WindView Instrumentation */
	
    USB_HUB_LOG_EVENT(
            USB_HUB_WV_DEVICE,
            "Entering usbHubClearTT() Function",
            USB_HUB_WV_FILTER);

    /* Allocate memory for the setup packet */

    pSetupPacket = OS_MALLOC(sizeof(USBHST_SETUP_PACKET));

    if (NULL == pSetupPacket)
        {
        USB_HUB_ERR("memory allocate failed for setup packet \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INSUFFICIENT_MEMORY;
        }

    /*Clear the allocated memory */

    OS_MEMSET(pSetupPacket, 0, sizeof(USBHST_SETUP_PACKET));

    /*
     *  Last bit of uPortNumber specifies if HUB is Single/Multiple TT
     *  1) If the last bit Of uPortNumber is Zero (single TT Hub),
     *     uTTPortNumber is set One
     *  2) If the last bit of uPortNumber is One(Multiple TT Hub) ,uTTPortNumber
     *     is uPortNumber with last bit made Zero
     */

    uTTPortNumber = (UINT8)(((uPortNumber & HUB_MASK_MULTIPLE_TT) == 0 )?
                            1 : (uPortNumber & HUB_MASK_MULTIPLE_TT_PORT));

    /*
     * Call USBHST_FILL_SETUP_PACKET() to populate an USBHST_SETUP_PACKET
     * structure by with bmRequest as PORT_TARGET, bRequest as CLEAR_TT_REQUEST,
     * wValue as the uValue, wIndex as uTTPortNumber and wLength as 0.
     *
     */

    USBHST_FILL_SETUP_PACKET(pSetupPacket,         /* pSetup */
                             USB_PORT_TARGET_SET,  /* uRequestType */
                             USB_CLEAR_TT_REQUEST, /* uRequest */
                             uValue,               /* uValue */
                             uTTPortNumber,        /* uIndex */
                             0);                   /* uSize */

    pControlURB = OS_MALLOC(sizeof(USBHST_URB));

    /* Check the result */

    if (NULL == pControlURB)
        {
        USB_HUB_ERR("memory allocate failed for control urb \n",
                    1, 2, 3, 4, 5, 6);

        OS_FREE(pSetupPacket);

        return USBHST_INSUFFICIENT_MEMORY;

        }

     /*clear the memory allocated */

     OS_MEMSET(pControlURB, 0, sizeof(USBHST_URB));

     /*
     * Call USBHST_FILL_CONTROL_URB() to populate a URB structure with
     * pHub::uDeviceHandle, endpoint as the DEFAUL_ENDPOINT, transfer buffer as
     * NULL, transfer length as 0, USBHST_SETUP_PACKET structure,
     * usbHubClearTTRequestCallback  as the callback and the pHub as the pContext.
     */

    USBHST_FILL_CONTROL_URB(   pControlURB,                 /* Urb */
                               uHubHandle,                  /* Device */
                               USB_HUB_DEFAULT_ENDPOINT,    /* EndPointAddress */
                               NULL,                        /* TransferBuffer */
                               0,                           /* TransferLength */
                               USBHST_SHORT_TRANSFER_OK,    /* TransferFlags */
                               pSetupPacket,                /* SetupPacket */
                               usbHubClearTTRequestCallback , /* Callback */
                               pContext,                    /* pContext */
                               USBHST_FAILURE               /* Status */
                               );

    Result = usbHstURBSubmit(pControlURB);

    if (USBHST_SUCCESS != Result)
        {
        USB_HUB_ERR("failed to submit a request for clear TT \n",
                    1, 2, 3, 4, 5, 6);

        /* Free the allocated memory */
        OS_FREE(pSetupPacket);

        OS_FREE(pControlURB);

        }/* End of if (USBHST_SUCCESS !=Result) */

    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
        USB_HUB_WV_DEVICE,
        "Exiting usbHubClearTT() Function",
        USB_HUB_WV_FILTER);

    return Result;
    }/*end ofHUB_ClearTT */

/***************************************************************************
*
* usbHubResetTT - This function submits a request for clear TT
*
* This routine submits a request for clear TT
*
* RETURNS: USBHST_SUCCESS, USBHST_FAILURE if request submission is unsuccessful
*
* ERRNO: None
*
* \NOMANUAL
*/
LOCAL USBHST_STATUS usbHubResetTT
    (
    UINT32 uHubHandle,
    UINT8  uPortNumber,
    void * pContext
    )
    {
    /* This is the URB */
    pUSBHST_URB pControlURB             =NULL;

    /* This stores the setup packet */
    pUSBHST_SETUP_PACKET pSetupPacket   =NULL;

    /* This is to store the request result */
    USBHST_STATUS Result                =USBHST_FAILURE;

    /* Port number to which the TT is attached on the hub */
    UINT16 uTTPortNumber                 =0;

    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
            USB_HUB_WV_DEVICE,
            "Entering usbHubResetTT() Function",
            USB_HUB_WV_FILTER);

    /*Allocate for pSetupPacket */

    pSetupPacket = OS_MALLOC(sizeof(USBHST_SETUP_PACKET));

    /* Check the result */

    if (NULL == pSetupPacket)
        {
        /* Debug Message */
        USB_HUB_ERR("memory allocate failed for setup packet \n",
                    1, 2, 3, 4, 5, 6);

        /* Return USBHST_INSUFFICIENT_MEMORY */
        return USBHST_INSUFFICIENT_MEMORY;

        }/* End of if (NULL== pSetupPacket) */

    /*Clear the allocated memory */

    OS_MEMSET(pSetupPacket, 0, sizeof(USBHST_SETUP_PACKET));

    /*
     *  Last bit of uPortNumber specifies if HUB is Single/Multiple TT
     *  1) If the last bit Of uPortNumber is Zero (single TT Hub),
     *     uTTPortNumber is set One
     *  2) If the last bit of uPortNumber is One(Multiple TT Hub) ,uTTPortNumber
     *     is uPortNumber with last bit made Zero
     */
    uTTPortNumber = (UINT16)(((uPortNumber & HUB_MASK_MULTIPLE_TT) == 0 ) ?
                            1 : (uPortNumber & HUB_MASK_MULTIPLE_TT_PORT));

    /*
     * Call USBHST_FILL_SETUP_PACKET() to populate an USBHST_SETUP_PACKET
     * structure by with bmRequest as PORT_TARGET, bRequest as RESET_TT,
     * wValue as zero, wIndex as uPortIndex and wLength as 0.
     */
    USBHST_FILL_SETUP_PACKET(   pSetupPacket, /* pSetup */
                                USB_PORT_TARGET_SET,  /* uRequestType */
                                USB_RESET_TT ,/* uRequest */
                                0,   /* uValue */
                                uTTPortNumber,  /* uIndex */
                                0);     /* uSize */

    /* Allocate memory for the URB */

    pControlURB = OS_MALLOC(sizeof(USBHST_URB));

    /* Check the result */

    if (NULL == pControlURB)
        {
        /* Debug Message */
        USB_HUB_ERR("memory allocate failed for control urb \n",
                    1, 2, 3, 4, 5, 6);

        /* Free the setup packet */
        OS_FREE(pSetupPacket);
        return USBHST_INSUFFICIENT_MEMORY;

        }/* End of if (NULL== pControlURB) */

    /*clear the memory allocated */

    OS_MEMSET(pControlURB, 0, sizeof(USBHST_URB));

    /*
     * Call USBHST_FILL_CONTROL_URB() to populate a URB structure with
     * pHub::uDeviceHandle, endpoint as the DEFAUL_ENDPOINT, transfer buffer as
     * NULL, transfer length as 0, USBHST_SETUP_PACKET structure,
     * HUB_ClearTTRequestCallback  as the callback and the pHub as the pContext.
     */
    USBHST_FILL_CONTROL_URB(pControlURB,                  /* Urb */
                            uHubHandle,                   /* Device */
                            USB_HUB_DEFAULT_ENDPOINT,     /* EndPointAddress */
                            NULL,                         /* TransferBuffer */
                            0,                            /* TransferLength */
                            USBHST_SHORT_TRANSFER_OK,     /* TransferFlags */
                            pSetupPacket,                 /* SetupPacket */
                            usbHubResetTTRequestCallback , /* Callback */
                            pContext,                     /* pContext */
                            USBHST_FAILURE                /* Status */
                            );
    /* Submit the request to the USBHST */
    Result = usbHstURBSubmit(pControlURB);

    if (USBHST_SUCCESS != Result)
        {
        /* Debug Message */
        USB_HUB_ERR("failed to submit a request for reset TT \n",
                    1, 2, 3, 4, 5, 6);

        /* Free the memory */
        OS_FREE(pSetupPacket);

        OS_FREE(pControlURB);
        }

    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
            USB_HUB_WV_DEVICE,
            "Exiting usbHubResetTT() Function",
            USB_HUB_WV_FILTER);
    return Result;
    }/*end of HUB_ResetTT*/

/**************************** End of File HUB_ClassInterface.c ****************/

