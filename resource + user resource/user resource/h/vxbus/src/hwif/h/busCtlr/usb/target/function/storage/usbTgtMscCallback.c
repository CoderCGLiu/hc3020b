/* usbTgtMscCallback.c - USB Target Mass Storage Class callback routine module */

/*
 * Copyright (c) 2010-2013, 2015 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
Modification History
--------------------
01r,02feb15,lan  Modify for WCID support (VXW6-83565)
01q,06may13,s_z  Remove compiler warning (WIND00356717)
01p,17oct12,s_z  Fix short packet receive issue (WIND00374594) 
01o,18sep12,s_z  Pass CV2.0 testing (WIND00375402)
01n,18may12,s_z  Add support for USB 3.0 target (WIND00326012)
01m,13dec11,m_y  Modify according to code check result (WIND00319317)
01l,16aug11,m_y  remove useless variable
01k,21apr11,s_z  Correct usbTgtMscVendorSpecific to support composite device
                 (WIND00268105)
01j,07apr11,ghs  Destroy all pipes when function driver detached
                 (WIND00265861)
01i,25mar11,m_y  make sure the buffer is empty at emulation stage
01h,24mar11,s_z  Changes for unused routines removed
01g,23mar11,m_y  remove descriptorGet routine
01f,17mar11,m_y  modify code based on the code review
01e,09mar11,s_z  Code clean up 
01d,03mar11,m_y  code clean up 
01c,23feb11,s_z  Add function driver device descriptor support
01b,28dec10,x_f  update and clean up
01a,26sep10,m_y  written
*/

/*
DESCRIPTION

This module defines those routines directly referenced by the USB peripheral
stack; namely, the routines that intialize the USB_TARG_CALLBACK_TABLE data
structure. Additional routines are also provided which are specific to the
mass storage driver.

INCLUDES: usbTgtMscLib.h, usbTgtMscUtil.h, usb/usbTgtMsc.h, usbTgtFunc.h
*/

/* includes */

#include <usbTgtMscLib.h>
#include <usbTgtMscUtil.h>
#include <usb/usbTgtMsc.h>
#include <usbTgtFunc.h>


IMPORT LIST g_usbTgtMscDevList;

IMPORT USR_USBTGT_MSC_CONFIG usrUsbTgtMscConfigTable[];



/* defines */

#ifdef USE_RBC_SUBCLASS
    #define USB_MSC_SUBCLASS     USB_MSC_SUBCLASS_RBC
#elif defined(USE_SCSI_SUBCLASS)
    #define USB_MSC_SUBCLASS     USB_MSC_SUBCLASS_SCSI
#else
    #error USB_MSC_SUBCLASS      undefined
#endif

#define MSC_NUM_ENDPOINTS               2       /* mass storage endpoints */


#define MSC_BULK_IN_ENDPOINT_NUM     0x81    /* BULK IN endpoint */
#define MSC_BULK_OUT_ENDPOINT_NUM    0x1     /* BULK OUT endpoint */

/* string identifiers and indexes for string descriptors */

#define MSC_CONFIG_VALUE             1          /* configuration value */

/* mass storage interface */

#define MSC_NUM_INTERFACES           1          /* 1 number of interfaces */
#define MSC_INTERFACE_NUM            0          /* interface number */
#define MSC_INTERFACE_ALT_SETTING    0          /* alternate setting */

#define MSC_ID_VENDOR                (0x1439)   /* mass storage vendor ID */
                                                /* Use a SanDisk ID so it */
                                                /* works on Windows */

#define MSC_BULK_MAX_PACKETSIZE      0x40       /* bulk max packet size */

#define MSC_HIGH_SPEED_CONTROL_MAX_PACKET_SIZE  0x40 /* Maximum packet size for */
                                                     /* the default control */
                                                     /* endpoint, if device */
                                                     /* is operating at high */
                                                     /* speed */

/* globals */

IMPORT void usbTgtMscLunListUpdate
    (
    pUSB_TGT_MSC_DEV pUsbTgtMscDev
    );

/* locals */

/* descriptor definitions */

LOCAL USB_INTERFACE_DESCR gMscIfDescr =     /* interface descriptor */
    {
    USB_INTERFACE_DESCR_LEN,                /* bLength */
    USB_DESCR_INTERFACE,                    /* bDescriptorType */
    MSC_INTERFACE_NUM,                      /* bInterfaceNumber */
    MSC_INTERFACE_ALT_SETTING,              /* bAlternateSetting */
    MSC_NUM_ENDPOINTS,                      /* bNumEndpoints */
    USB_MSC_CLASS_MASS_STORAGE,             /* bInterfaceClass */
    USB_MSC_SUBCLASS,                       /* bInterfaceSubClass */
    USB2_MSC_PROTOCOL_BOT,                  /* bInterfaceProtocol */
    0                                       /* iInterface */
    };

LOCAL USB_ENDPOINT_DESCR gMscBulkOutEpDescr =/* OUT endpoint descriptor */
    {
    USB_ENDPOINT_DESCR_LEN,                 /* bLength */
    USB_DESCR_ENDPOINT,                     /* bDescriptorType */
    MSC_BULK_OUT_ENDPOINT_NUM,              /* bEndpointAddress */
    USB_ATTR_BULK,                          /* bmAttributes */
    TO_LITTLEW (USB_MAX_HIGH_SPEED_BULK_SIZE), /* max packet size */
    0                                       /* bInterval */
    };

LOCAL USB_ENDPOINT_DESCR gMscSSBulkOutEpDescr =/* OUT endpoint descriptor */
    {
    USB_ENDPOINT_DESCR_LEN,                 /* bLength */
    USB_DESCR_ENDPOINT,                     /* bDescriptorType */
    MSC_BULK_OUT_ENDPOINT_NUM,              /* bEndpointAddress */
    USB_ATTR_BULK,                          /* bmAttributes */
    TO_LITTLEW (USB_MAX_SUPER_SPEED_BULK_SIZE), /* max packet size */
    0                                       /* bInterval */
    };

LOCAL USB_ENDPOINT_COMPANION_DESCR gMscBulkOutEpCompDescr =/* OUT endpoint companion descriptor */
    {
    USB_ENDPOINT_COMP_DESCR_LEN,                 /* bLength */
    USB_DESCR_SS_ENDPOINT_COMPANION,             /* bDescriptorType */
    0x0E,                                        /* bMaxBurst */
    0,                                           /* bmAttributes */
    0                                            /* wBytesPerInterval */
    };

LOCAL USB_ENDPOINT_DESCR gMscBulkInEpDescr = /* IN endpoint descriptor */
    {
    USB_ENDPOINT_DESCR_LEN,                 /* bLength */
    USB_DESCR_ENDPOINT,                     /* bDescriptorType */
    MSC_BULK_IN_ENDPOINT_NUM,               /* bEndpointAddress */
    USB_ATTR_BULK,                          /* bmAttributes */
    TO_LITTLEW (USB_MAX_HIGH_SPEED_BULK_SIZE),/* max packet size */
    0                                       /* bInterval */
    };

LOCAL USB_ENDPOINT_DESCR gMscSSBulkInEpDescr = /* IN endpoint descriptor */
    {
    USB_ENDPOINT_DESCR_LEN,                 /* bLength */
    USB_DESCR_ENDPOINT,                     /* bDescriptorType */
    MSC_BULK_IN_ENDPOINT_NUM,               /* bEndpointAddress */
    USB_ATTR_BULK,                          /* bmAttributes */
    TO_LITTLEW (USB_MAX_SUPER_SPEED_BULK_SIZE),/* max packet size */
    0                                       /* bInterval */
    };

LOCAL USB_ENDPOINT_COMPANION_DESCR gMscBulkInEpCompDescr =/* IN endpoint companion descriptor */
    {
    USB_ENDPOINT_COMP_DESCR_LEN,                 /* bLength */
    USB_DESCR_SS_ENDPOINT_COMPANION,             /* bDescriptorType */
    0x0E,                                        /* bMaxBurst */
    0,                                           /* bmAttributes */
    0                                            /* wBytesPerInterval */
    };

LOCAL USB_ENDPOINT_DESCR gMscOtherSpeedbulkOutEpDescr =/* Other Speed OUT */
                                                      /* endpoint descriptor */
    {
    USB_ENDPOINT_DESCR_LEN,                 /* bLength */
    USB_DESCR_ENDPOINT,                     /* bDescriptorType */
    MSC_BULK_OUT_ENDPOINT_NUM,              /* bEndpointAddress */
    USB_ATTR_BULK,                          /* bmAttributes */
    TO_LITTLEW (MSC_BULK_MAX_PACKETSIZE),   /* max packet size */
    0                                       /* bInterval */
    };

LOCAL USB_ENDPOINT_DESCR gMscOtherSpeedbulkInEpDescr = /* Other Speed IN */
                                                      /* endpoint descriptor */
    {
    USB_ENDPOINT_DESCR_LEN,                 /* bLength */
    USB_DESCR_ENDPOINT,                     /* bDescriptorType */
    MSC_BULK_IN_ENDPOINT_NUM,               /* bEndpointAddress */
    USB_ATTR_BULK,                          /* bmAttributes */
    TO_LITTLEW (MSC_BULK_MAX_PACKETSIZE),   /* max packet size */
    0                                       /* bInterval */
    };

LOCAL pUSB_DESCR_HDR gMscOtherSpeedDescHdr[] =
    {
    (pUSB_DESCR_HDR )&gMscIfDescr,                  /* Interface descriptor */
    (pUSB_DESCR_HDR )&gMscOtherSpeedbulkOutEpDescr, /* Endpoint descriptor */
    (pUSB_DESCR_HDR )&gMscOtherSpeedbulkInEpDescr,  /* Endpoint descriptor */
    NULL                                            /* End of descriptors, must have */
    };

LOCAL pUSB_DESCR_HDR gMscHighSpeedDescHdr[] =
    {
    (pUSB_DESCR_HDR )&gMscIfDescr,        /* Interface descriptor */
    (pUSB_DESCR_HDR )&gMscBulkOutEpDescr, /* Endpoint descriptor */
    (pUSB_DESCR_HDR )&gMscBulkInEpDescr,  /* Endpoint descriptor */
     NULL                                 /* End of descriptors, must have */
     };
LOCAL pUSB_DESCR_HDR gMscSuperSpeedDescHdr[] =
    {
    (pUSB_DESCR_HDR )&gMscIfDescr,        /* Interface descriptor */
    (pUSB_DESCR_HDR )&gMscSSBulkOutEpDescr, /* Endpoint descriptor */
    (pUSB_DESCR_HDR )&gMscBulkOutEpCompDescr, /* Endpoint companion descriptor */    
    (pUSB_DESCR_HDR )&gMscSSBulkInEpDescr,  /* Endpoint descriptor */
    (pUSB_DESCR_HDR )&gMscBulkInEpCompDescr,  /* Endpoint companion descriptor */    
     NULL                                 /* End of descriptors, must have */
     };

/* forward declarations */

LOCAL STATUS usbTgtMscMngmtFunc 
    (
    pVOID            param, 
    USB_TARG_CHANNEL targChannel,
    UINT16           mngmtCode, 
    pVOID            pContext
    );

LOCAL STATUS usbTgtMscFeatureClear 
    (
    pVOID            param, 
    USB_TARG_CHANNEL targChannel,
    UINT8            requestType, 
    UINT16           feature, 
    UINT16           index
    );

LOCAL STATUS usbTgtMscFeatureSet 
    (
    pVOID            param,
    USB_TARG_CHANNEL targChannel,
    UINT8            requestType, 
    UINT16           feature, 
    UINT16           index
    );

LOCAL STATUS usbTgtMscConfigurationSet 
    (
    pVOID            param, 
    USB_TARG_CHANNEL targChannel,
    UINT8            configuration
    );

LOCAL STATUS usbTgtMscInterfaceGet 
    (
    pVOID            param, 
    USB_TARG_CHANNEL targChannel,
    UINT16           interfaceIndex,
    pUINT8           pAlternateSetting
    );

LOCAL STATUS usbTgtMscInterfaceSet 
    (
    pVOID            param, 
    USB_TARG_CHANNEL targChannel,
    UINT16           interfaceIndex, 
    UINT8            alternateSetting
    );

LOCAL STATUS usbTgtMscVendorSpecific 
    (
    pVOID            param, 
    USB_TARG_CHANNEL targChannel,
    UINT8            requestType, 
    UINT8            request, 
    UINT16           value,
    UINT16           index, 
    UINT16           length
    );
LOCAL void usbTgtMscDeletePipes
    (
    pUSB_TGT_MSC_DEV pUsbTgtMscDev
    );
LOCAL STATUS usbTgtMscCreatePipes
    (
    pUSB_TGT_MSC_DEV pUsbTgtMscDev,
    UINT8            uConfig
    );

LOCAL USB_TARG_CALLBACK_TABLE usbTgtMscCallbackTable = /* callback table */
    {
    usbTgtMscMngmtFunc,                  /* mngmtFunc */
    usbTgtMscFeatureClear,               /* featureClear */
    usbTgtMscFeatureSet,                 /* featureSet */
    NULL,                                /* configurationGet */
    usbTgtMscConfigurationSet,           /* configurationSet */
    NULL,                                /* descriptorGet */
    NULL,                                /* descriptorSet */
    usbTgtMscInterfaceGet,               /* interfaceGet */
    usbTgtMscInterfaceSet,               /* interfaceSet */
    NULL,                                /* statusGet */
    NULL,                                /* addressSet */
    NULL,                                /* synchFrameGet */
    usbTgtMscVendorSpecific              /* vendorSpecific */
    };

/*******************************************************************************
*
* usbTgtMscDriverBind - bind the device structure to the function driver
*
* This routine binds the device structure to the function driver.
*
* RETURNS: OK, or ERROR if the bind process failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbTgtMscDriverBind
    (
    pUSB_TGT_MSC_DEV  pUsbTgtMscDev,
    int               index
    )
    {
    USBTGT_FUNC_INFO    usbTgtFuncInfo;
    
    if (NULL == pUsbTgtMscDev)
        {
        USBTGT_MSC_ERR("usbTgtMscDevInit - pUsbTgtMscDev is NULL\n",
                       1, 2, 3, 4, 5, 6);
        return ERROR;
        }
    
    usbTgtFuncInfo.ppFsFuncDescTable = (USB_DESCR_HDR **)gMscOtherSpeedDescHdr; 
    usbTgtFuncInfo.ppHsFuncDescTable = (USB_DESCR_HDR **)gMscHighSpeedDescHdr;  
    usbTgtFuncInfo.ppSsFuncDescTable = (USB_DESCR_HDR **)gMscSuperSpeedDescHdr;
    usbTgtFuncInfo.pFuncCallbackTable = &usbTgtMscCallbackTable; 
    usbTgtFuncInfo.pCallbackParam = (pVOID)pUsbTgtMscDev;
    usbTgtFuncInfo.pFuncSpecific = (pVOID)pUsbTgtMscDev;
    
    usbTgtFuncInfo.pFuncName = usrUsbTgtMscConfigTable[index].funcName;
    usbTgtFuncInfo.pTcdName = usrUsbTgtMscConfigTable[index].tcdName;
    usbTgtFuncInfo.uFuncUnit = usrUsbTgtMscConfigTable[index].funcUnit;
    usbTgtFuncInfo.uTcdUnit = usrUsbTgtMscConfigTable[index].tcdUnit;
    usbTgtFuncInfo.uConfigToBind = usrUsbTgtMscConfigTable[index].configNum;
    usbTgtFuncInfo.pWcidString = NULL;
    usbTgtFuncInfo.pSubWcidString = NULL;
    usbTgtFuncInfo.uWcidVc = NULL;


    /* Register to the TML */
    
    pUsbTgtMscDev->targChannel = usbTgtFuncRegister(&usbTgtFuncInfo);

    if (USBTGT_TARG_CHANNEL_DEAD == pUsbTgtMscDev->targChannel)
        {
        USBTGT_MSC_ERR("Register to the TML fail\n",
                         1, 2, 3, 4, 5, 6);
        return ERROR;
        }       
        
    return OK;
    }

/*******************************************************************************
*
* usbTgtMscDriverUnBind - unbind the device structure to the function driver
*
* This routine unbinds the device structure to the function driver.
*
* RETURNS: OK, or ERROR if the unbind process failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbTgtMscDriverUnBind
    (
    pUSB_TGT_MSC_DEV  pUsbTgtMscDev
    )
    {
    if (NULL == pUsbTgtMscDev)
        {
        USBTGT_MSC_ERR("usbTgtMscDriverUnBind - Invalid parameter\n",
                       1, 2, 3, 4, 5, 6);
        
        return ERROR;
        }

    /* Un-register from TML */

    (void)usbTgtFuncUnRegister(pUsbTgtMscDev->targChannel);
    
    USBTGT_MSC_DBG("usbTgtMscDriverUnBind - un-register from TML\n",
                   1, 2, 3, 4, 5, 6);
    
    return OK;
    }

/*******************************************************************************
*
* usbTgtMscMngmtFunc - invoke the connection management function
*
* This function handles various management related events. <mngmtCode>
* consist of the management event function code that is reported by the
* TargLib layer. <pContext> is the argument sent for the management event to
* be handled.
*
* RETURNS: OK, or ERROR if unable to handle event
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtMscMngmtFunc
    (
    pVOID            param,          /* TCD specific paramter */
    USB_TARG_CHANNEL targChannel,    /* target channel */
    UINT16           mngmtCode,      /* management code */
    pVOID            pContext        /* Context value */
    )
    {
    pUSB_TGT_MSC_DEV  pUsbTgtMscDev;

    if ((pUsbTgtMscDev = (pUSB_TGT_MSC_DEV)param) == NULL)
        {
        USBTGT_MSC_ERR("usbTgtMscMngmtFunc - pUsbTgtMscDev is NULL\n",
                       1, 2, 3, 4, 5, 6);
        return(ERROR);
        }

    switch (mngmtCode)
        {
        case TARG_MNGMT_ATTACH:
            USBTGT_MSC_DBG("usbTgtMscMngmtFunc - TARG_MNGMT_ATTACH\n",
                1, 2, 3, 4, 5, 6);

            pUsbTgtMscDev->uCurConfig = 0;
            pUsbTgtMscDev->uAltSetting = 0;
            pUsbTgtMscDev->bulkOutPipe = NULL;
            pUsbTgtMscDev->bulkInPipe = NULL;
            pUsbTgtMscDev->targChannel = targChannel;
            
            break;

        case TARG_MNGMT_DETACH:
            USBTGT_MSC_DBG("usbTgtMscMngmtFunc - TARG_MNGMT_DETACH\n",
                1, 2, 3, 4, 5, 6);

            usbTgtMscDeletePipes (pUsbTgtMscDev);

            break;

        case TARG_MNGMT_BUS_RESET:
            USBTGT_MSC_DBG("usbTgtMscMngmtFunc - TARG_MNGMT_BUS_RESET\n",
                1, 2, 3, 4, 5, 6);
            pUsbTgtMscDev->uCurConfig = 0;
            pUsbTgtMscDev->uAltSetting = 0;

            usbTgtMscDeletePipes (pUsbTgtMscDev);

            break;

        case TARG_MNGMT_DISCONNECT:
            USBTGT_MSC_DBG("usbTgtMscMngmtFunc - TARG_MNGMT_DISCONNECT\n",
                           1, 2, 3, 4, 5, 6);
            usbTgtMscConfigurationSet (param, targChannel, 0);
            break;
        case TARG_MNGMT_SUSPEND:
            USBTGT_MSC_DBG("usbTgtMscMngmtFunc - TARG_MNGMT_SUSPEND\n",
                           1, 2, 3, 4, 5, 6);
            break;
        case TARG_MNGMT_RESUME:
            USBTGT_MSC_DBG("usbTgtMscMngmtFunc - TARG_MNGMT_RESUME\n",
                           1, 2, 3, 4, 5, 6);
            break;
        default:
            USBTGT_MSC_DBG("usbTgtMscMngmtFunc - mngmtCode 0x%X unsupported\n",
                           mngmtCode, 2, 3, 4, 5, 6);
            return ERROR;
        }

    return OK;
    }

/*******************************************************************************
*
* usbTgtMscFeatureClear - clear the specified feature
*
* This routine implements the clear feature standard device request.
*
* RETURNS: OK, or ERROR if unable to clear the feature
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtMscFeatureClear
    (
    pVOID               param,          /* TCD specific parameter */
    USB_TARG_CHANNEL    targChannel,    /* target channel */
    UINT8               requestType,    /* request type */
    UINT16              feature,        /* feature to clear */
    UINT16              index           /* index */
    )
    {
    pUSB_TGT_MSC_DEV    pUsbTgtMscDev;
    USB_TARG_PIPE       pipeHandle = NULL;
    UINT8               uInterface;
    UINT8               uSuspendOp;

    if ((pUsbTgtMscDev = (pUSB_TGT_MSC_DEV)param) == NULL)
        {
        USBTGT_MSC_ERR("usbTgtMscFeatureClear - pUsbTgtMscDev is NULL\n",
                       1, 2, 3, 4, 5, 6);
        return(ERROR);
        }  

    /* This request must be standard request from the host */

    if (((requestType & USB_RT_DIRECTION_MASK) != USB_RT_HOST_TO_DEV) ||
        ((requestType & USB_RT_CATEGORY_MASK) != USB_RT_STANDARD))
        {
        USBTGT_MSC_ERR("usbTgtMscFeatureClear - non-standard request\n",
                       1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    requestType = (UINT8)(requestType & (~(USB_RT_DIRECTION_MASK | USB_RT_CATEGORY_MASK)));

    if ((requestType == USB_RT_ENDPOINT) && (feature == USB_FSEL_DEV_ENDPOINT_HALT))
        {
        pipeHandle = usbTgtFuncFindPipeHandleByEpIndex(pUsbTgtMscDev->targChannel,
                                                       (UINT8)(index & 0xFF));

        if (pipeHandle == pUsbTgtMscDev->bulkInPipe)
            {
            if (pUsbTgtMscDev->uCmdStatus == USBTGT_MSC_CBW_INVALID)
                {
                usbTgtSetPipeStatus(pUsbTgtMscDev->bulkInPipe, USBTGT_PIPE_STATUS_STALL);
                pUsbTgtMscDev->bulkInPipeState = USB_TGT_MSC_PIPE_STALL;
                }
            else
                {
                pUsbTgtMscDev->bulkInPipeState = USB_TGT_MSC_PIPE_UNSTALL;
                }
            return OK;
            }
        else if (pipeHandle == pUsbTgtMscDev->bulkOutPipe)
            {
            if (pUsbTgtMscDev->uCmdStatus == USBTGT_MSC_CBW_INVALID)
                {
                usbTgtSetPipeStatus(pUsbTgtMscDev->bulkOutPipe, USBTGT_PIPE_STATUS_STALL);
                pUsbTgtMscDev->bulkOutPipeState = USB_TGT_MSC_PIPE_STALL;
                }
            else
                {
                pUsbTgtMscDev->bulkOutPipeState = USB_TGT_MSC_PIPE_UNSTALL;
                
                }
            return OK;
            }
        else
            {
            USBTGT_MSC_ERR("Can not find the pipe from the Ep index 0x%X\n",
                           index, 2, 3, 4, 5, 6);
            return ERROR;
            }
        }
    else if ((requestType == USB_RT_INTERFACE) && 
             (feature == USB_FSEL_DEV_FUNCTION_SUSPEND))
        {
        /* The MSB is the specific suspend options */
        
        uSuspendOp = (UINT8) (index >> 8);
        uInterface = (UINT8) (index & 0xFF);
        
        USBTGT_MSC_DBG ("Clear interface %d suspendop %x\n",
                        uInterface, uSuspendOp, 3, 4, 5, 6);
        
        return OK;
        }
 
    USBTGT_MSC_ERR("usbTgtMscFeatureClear - "
                   "requestType 0x%X, feature 0x%X, index 0x%X\r\n",
                   requestType, feature, index, 4, 5, 6);
    return ERROR;
    }

/*******************************************************************************
*
* usbTgtMscFeatureSet - set the specified feature
*
* This routine implements the set feature standard device request.
*
* RETURNS: OK or ERORR if unable to set the feature.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtMscFeatureSet
    (
    pVOID               param,          /* TCD specific parameter */
    USB_TARG_CHANNEL    targChannel,    /* target channel */
    UINT8               requestType,    /* request type */
    UINT16              feature,        /* feature to set */
    UINT16              index           /* wIndex */
    )
    {
    pUSB_TGT_MSC_DEV    pUsbTgtMscDev;
    USB_TARG_PIPE       pipeHandle = NULL;
    UINT8               uInterface;
    UINT8               uSuspendOp;

    if ((pUsbTgtMscDev = (pUSB_TGT_MSC_DEV)param) == NULL)
        {
        USBTGT_MSC_ERR("usbTgtMscFeatureSet - pUsbTgtMscDev is NULL\n",
                       1, 2, 3, 4, 5, 6);
        return(ERROR);
        }
        
    /* This request must be standard request from the host */

    if (((requestType & USB_RT_DIRECTION_MASK) != USB_RT_HOST_TO_DEV) ||
        ((requestType & USB_RT_CATEGORY_MASK) != USB_RT_STANDARD))
        {
        USBTGT_MSC_ERR("usbTgtMscFeatureSet - non-standard request\n",
                       1, 2, 3, 4, 5, 6);
        return(ERROR);
        }

    requestType =
        (UINT8)(requestType & (~(USB_RT_DIRECTION_MASK | USB_RT_CATEGORY_MASK)));

    if ((requestType == USB_RT_ENDPOINT) && 
        (feature == USB_FSEL_DEV_ENDPOINT_HALT))
        { 
        pipeHandle = usbTgtFuncFindPipeHandleByEpIndex(pUsbTgtMscDev->targChannel,
                                                       (UINT8) (index & 0xFF));
        if (pipeHandle == pUsbTgtMscDev->bulkInPipe)
            {
            pUsbTgtMscDev->bulkInPipeState = USB_TGT_MSC_PIPE_STALL;
            return OK;
            }
        else if (pipeHandle == pUsbTgtMscDev->bulkOutPipe)
            {
            pUsbTgtMscDev->bulkOutPipeState = USB_TGT_MSC_PIPE_STALL;
            return OK;
            }
        else
            {
            USBTGT_MSC_WARN("No find the right pipe for ep %x\n",
                            index, 2, 3, 4, 5, 6);
            
            return ERROR;
            }
        }
    else if ((requestType == USB_RT_INTERFACE) && 
             (feature == USB_FSEL_DEV_FUNCTION_SUSPEND))
        {
        /* The MSB is the specific suspend options */
        
        uSuspendOp = (UINT8) (index >> 8);
        uInterface = (UINT8) (index & 0xFF);
        
        USBTGT_MSC_DBG ("Set interface %d suspendop %x\n",
                        uInterface, uSuspendOp, 3, 4, 5, 6);
        
        return OK;
        }

    USBTGT_MSC_ERR("usbTgtMscFeatureSet - "
                   "requestType 0x%X, feature 0x%X, index 0x%X\r\n",
                   requestType, feature, index, 4, 5, 6);
    return ERROR;
    }

/*******************************************************************************
*
* usbTgtMscConfigurationSet - set the specified configuration
*
* This function is used to set the current configuration to the configuration
* value sent by host. <configuration> consists of the value to set.
*
* RETURNS: OK, or ERROR if unable to set specified configuration
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtMscConfigurationSet
    (
    pVOID               param,          /*  TCD specific parameter */
    USB_TARG_CHANNEL    targChannel,    /* target channel */
    UINT8               configuration   /* configuration value to set */
    )
    {
    USB_MSC_CBW *       pCbw;           /* Command Block Wrapper */
    UINT8 *             pData;
    UINT32              size;
    pUSB_TGT_MSC_LUN    pUsbTgtMscLun;
    pUSB_TGT_MSC_DEV    pUsbTgtMscDev;

    if ((pUsbTgtMscDev = (pUSB_TGT_MSC_DEV)param) == NULL)
        {
        USBTGT_MSC_ERR("usbTgtMscConfigurationSet - pUsbTgtMscDev is NULL\n",
                       1, 2, 3, 4, 5, 6);
        return(ERROR);
        }    
        
    pUsbTgtMscLun = pUsbTgtMscDev->pCurrentLun;

    if (configuration == 0)
        {
        USBTGT_MSC_WARN("usbTgtMscConfigurationSet - "
                        "configuration == 0\n",
                        1, 2, 3, 4, 5, 6);

        pUsbTgtMscDev->uCurConfig = configuration;
        usbTgtMscDeletePipes (pUsbTgtMscDev);
        }
    else
        {
        USBTGT_MSC_VDBG("usbTgtMscConfigurationSet - correct config value %d\n",
                        configuration, 2, 3, 4, 5, 6);
        
        /* Update the Lun list of the current USB target MSC device */
        
        usbTgtMscLunListUpdate(pUsbTgtMscDev);

        /* Reset the buf state to empty */
        
        USBTGT_MSC_BUF_RELEASE(pUsbTgtMscDev);        

       /* if the bulk in pipe is in use, try to clear it */

        if (pUsbTgtMscDev->bulkInErpState == USB_TGT_MSC_ERP_BUSY)
            {
            if (usbTgtCancelErp(pUsbTgtMscDev->bulkInPipe, 
                               &pUsbTgtMscDev->bulkInErp) != OK)
                {
                USBTGT_MSC_ERR("usbTgtMscConfigurationSet - "
                               "usbTgtCancelErp failed for BULK IN\n",
                               1, 2, 3, 4, 5, 6);
                return ERROR;
                }
            pUsbTgtMscDev->bulkInErpState = USB_TGT_MSC_ERP_IDLE;
            }

        /* if the bulk out pipe is in use, try to clear it */

        if (pUsbTgtMscDev->bulkOutErpState == USB_TGT_MSC_ERP_BUSY)
            {
            if (usbTgtCancelErp(pUsbTgtMscDev->bulkOutPipe, 
                               &pUsbTgtMscDev->bulkOutErp) != OK)
                {
                USBTGT_MSC_ERR("usbTgtMscConfigurationSet - "
                               "usbTgtCancelErp failed for BULK OUT\n",
                               1, 2, 3, 4, 5, 6);
                return ERROR;
                }
            pUsbTgtMscDev->bulkOutErpState = USB_TGT_MSC_ERP_IDLE;
            }

        if (usbTgtMscCreatePipes(pUsbTgtMscDev, configuration) != OK)
            {                
            USBTGT_MSC_ERR("Creat bulk in/out pipe failed\n",
                           1, 2, 3, 4, 5, 6);
            return ERROR;
            }
        
        pUsbTgtMscDev->uCurConfig = configuration;

        /*
         * Initialize ERP to listen for data 
         * on reset setup to receive a new CBW 
         */
         
        pCbw    = pUsbTgtMscDev->pCBW;
        memset(pCbw, 0, sizeof(USB_MSC_CBW));
        pCbw->dCBWSignature = OS_UINT32_CPU_TO_LE (USB_MSC_CBW_SIGNATURE);      
        pData   = (UINT8 *)pCbw;
        size    = sizeof(USB_MSC_CBW);

        if (usbTgtMscBulkErpInit(pUsbTgtMscDev, pData, size,
            USB_PID_OUT, usbTgtMscBulkOutErpCallbackCBW) != OK)
            {
            USBTGT_MSC_ERR("usbTgtMscConfigurationSet - "
                           "usbTgtMscBulkErpInit for BULK OUT pipe failed\n",
                           1, 2, 3, 4, 5, 6);

            usbTgtMscDeletePipes(pUsbTgtMscDev);
            return ERROR;
            }
        }

    return OK;
    }

/*******************************************************************************
*
* usbTgtMscInterfaceGet - get the specified interface
*
* This function is used to get the selected alternate setting of the
* specified interface.
*
* RETURNS: OK, or ERROR if unable to return interface setting
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtMscInterfaceGet
    (
    pVOID               param,              /* TCD specific parameter */
    USB_TARG_CHANNEL    targChannel,        /* target channel */
    UINT16              interfaceIndex,     /* interface index */
    pUINT8              pAlternateSetting   /* alternate setting */
    )
    {
    pUSB_TGT_MSC_DEV    pUsbTgtMscDev;

    if ((pUsbTgtMscDev = (pUSB_TGT_MSC_DEV)param) == NULL)
        {
        USBTGT_MSC_ERR("usbTgtMscInterfaceGet - pUsbTgtMscDev is NULL\n",
            1, 2, 3, 4, 5, 6);
        return(ERROR);
        }
       
    /* This is an invalid request if the device is in default state */

    if (pUsbTgtMscDev->uCurConfig == 0)
        {
        USBTGT_MSC_ERR("usbTgtMscConfigurationSet - configuration error\n",
                       1, 2, 3, 4, 5, 6);
        return(ERROR);
        }

    *pAlternateSetting = (UINT8) (pUsbTgtMscDev->uAltSetting & 0xFF);

    return (OK);
    }

/*******************************************************************************
*
* usbTgtMscInterfaceSet - set the specified interface
*
* This function is used to select the alternate setting of the specified
* interface.
*
* RETURNS: OK, or ERROR if unable to set specified interface
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtMscInterfaceSet
    (
    pVOID               param,              /* TCD specific parameter */
    USB_TARG_CHANNEL    targChannel,        /* target channel */
    UINT16              interfaceIndex,     /* interface index */
    UINT8               alternateSetting    /* alternate setting */
    )
    {
    pUSB_TGT_MSC_DEV    pUsbTgtMscDev;
    UINT8               uIfBase;
    
    if ((pUsbTgtMscDev = (pUSB_TGT_MSC_DEV)param) == NULL)
        {
        USBTGT_MSC_ERR("usbTgtMscInterfaceSet - pUsbTgtMscDev is NULL\n",
                       1, 2, 3, 4, 5, 6);
        return ERROR;
        }        
        
    /* This is an invalid request if the device is in default state */

    if (pUsbTgtMscDev->uCurConfig == 0)
        {
        USBTGT_MSC_ERR("usbTgtMscInterfaceSet - configuration error\n",
                       1, 2, 3, 4, 5, 6);
        return ERROR;
        }
    
    /* 
     * This function driver may be part of one compiste device 
     * Get the interface base.
     */
     
    if ( ERROR == usbTgtFuncInfoMemberGet(pUsbTgtMscDev->targChannel,
                                     USBTGT_FUNC_INFO_MEM_TYPE_uIfNumMin,
                                     &uIfBase))
        {
        USBTGT_MSC_ERR("Can not get the interface base number\n",
                        1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    if (alternateSetting == uIfBase)
        {
        
        pUsbTgtMscDev->uAltSetting = alternateSetting;
        return OK;
        }
    else
        {
        USBTGT_MSC_ERR("usbTgtMscInterfaceSet - alternateSetting error\n",
                       1, 2, 3, 4, 5, 6);
        return ERROR;
        }
    }

/*******************************************************************************
*
* usbTgtMscVendorSpecific - invoke the VENDOR_SPECIFIC request
*
* This routine implements the vendor specific standard device request
*
* RETURNS: OK, or ERROR if unable to process vendor-specific request
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtMscVendorSpecific
    (
    pVOID               param,          /* TCD specific parameter */
    USB_TARG_CHANNEL    targChannel,    /* target channel */
    UINT8               requestType,    /* request type */
    UINT8               request,        /* request name */
    UINT16              value,          /* wValue */
    UINT16              index,          /* wIndex */
    UINT16              length          /* wLength */
    )
    {
    STATUS              retVal = ERROR;
    UINT8 *             pData;
    UINT32              size;
    UINT8               maxLun;
    pUSB_TGT_MSC_LUN    pUsbTgtMscLun;
    pUSB_TGT_MSC_DEV    pUsbTgtMscDev;

    if ((pUsbTgtMscDev = (pUSB_TGT_MSC_DEV)param) == NULL)
        {
        USBTGT_MSC_ERR("usbTgtMscVendorSpecific - pUsbTgtMscDev is NULL\n",
                       1, 2, 3, 4, 5, 6);
        return ERROR;
        }    
        
    pUsbTgtMscLun = pUsbTgtMscDev->pCurrentLun;
        
    if (requestType == (USB_RT_HOST_TO_DEV | USB_RT_CLASS | USB_RT_INTERFACE))
        {
        /* mass storage reset */

        if (request == USB_MSC_RESET)
            {
            USBTGT_MSC_VDBG("usbTgtMscVendorSpecific - MSC reset\n",
                            1, 2, 3, 4, 5, 6);

            /* 
             * Do not check the interface index, 
             * since it is asigned by target stack. 
             */
             
            if ((value != 0) ||
                (length != 0))
                {
                USBTGT_MSC_ERR("usbTgtMscVendorSpecific - USB_MSC_RESET error\n",
                               1, 2, 3, 4, 5, 6);
                return ERROR;
                }
            
            /* if the bulk in pipe is in use, try to clear it */

            if (pUsbTgtMscDev->bulkInErpState == USB_TGT_MSC_ERP_BUSY)
                {
                if (usbTgtCancelErp(pUsbTgtMscDev->bulkInPipe, 
                                    &pUsbTgtMscDev->bulkInErp) != OK)
                    {
                    USBTGT_MSC_ERR("usbTgtMscVendorSpecific - "
                                   "usbTgtCancelErp failed for BULK IN\n",
                                   1, 2, 3, 4, 5, 6);
                    return ERROR;
                    }
                pUsbTgtMscDev->bulkInErpState = USB_TGT_MSC_ERP_IDLE;
                }

            /* if the bulk out pipe is in use, try to clear it */

            if (pUsbTgtMscDev->bulkOutErpState== USB_TGT_MSC_ERP_BUSY)
                {
                if (usbTgtCancelErp(pUsbTgtMscDev->bulkOutPipe, 
                                    &pUsbTgtMscDev->bulkOutErp) != OK)
                    {
                    USBTGT_MSC_ERR("usbTgtMscVendorSpecific - "
                                   "usbTgtCancelErp failed for BULK OUT\n",
                                   1, 2, 3, 4, 5, 6);
                    return ERROR;
                    }
                pUsbTgtMscDev->bulkOutErpState = USB_TGT_MSC_ERP_IDLE;
                }

            /* setup to receive a new CBW */

            memset(pUsbTgtMscDev->pCBW, 0, sizeof(USB_MSC_CBW));
            pUsbTgtMscDev->pCBW->dCBWSignature = 
                            OS_UINT32_CPU_TO_LE (USB_MSC_CBW_SIGNATURE);
            pData   = (UINT8 *)pUsbTgtMscDev->pCBW;
            size    = sizeof(USB_MSC_CBW);

            if (usbTgtMscBulkErpInit(pUsbTgtMscDev, 
                                     pData, 
                                     size,
                                     USB_PID_OUT, 
                                     usbTgtMscBulkOutErpCallbackCBW) == OK)
                {
                /* Need check this on more controllers */
                /*  usbTgtControlStatusSend(targChannel, FALSE); */
                
                pUsbTgtMscDev->uCmdStatus = USBTGT_MSC_CBW_NORMAL;
                return OK;
                }
            else
                {
                USBTGT_MSC_ERR("usbTgtMscVendorSpecific - "
                               "setup to receive CBW ERROR\n",
                               1, 2, 3, 4, 5, 6);
                return ERROR;
                }
            }
        else
            {
            USBTGT_MSC_ERR("usbTgtMscVendorSpecific - request error\n",
                           1, 2, 3, 4, 5, 6);
            return ERROR;
            }
        }
    else if (requestType == (USB_RT_DEV_TO_HOST | USB_RT_CLASS | 
                             USB_RT_INTERFACE))
        {
        /* Get Max LUN */

        if (request == USB_MSC_GET_MAX_LUN)
            {
            USBTGT_MSC_DBG("usbTgtMscVendorSpecific - Get Max LUN\n",
                           1, 2, 3, 4, 5, 6);
            
            /* 
             * Do not check the interface index, 
             * since it is asigned by target stack. 
             */
             
            if ((value != 0) || 
                (length != 1))
                {
                USBTGT_MSC_ERR("usbTgtMscVendorSpecific - "
                               "request USB_MSC_GET_MAX_LUN error\n",
                               1, 2, 3, 4, 5, 6);
                return ERROR;
                }
            maxLun = (UINT8) (pUsbTgtMscDev->nLuns - 1); 

            USBTGT_MSC_DBG("usbTgtMscVendorSpecific - maxLun is %d\n",
                           maxLun, 2, 3, 4, 5, 6);
            /*
             * the maxLun is local buffer but the usbTgtControlResponseSend
             * will copy the buffer to the data transfer buffer
             * So no need to worry about using a stack based 
             * variable to hold transfer data
             */
             
            retVal = usbTgtControlResponseSend(pUsbTgtMscDev->targChannel, 
                                               1, 
                                               &maxLun, 
                                               USB_ERP_FLAG_NORMAL);
            }
        else
            {
            USBTGT_MSC_ERR("usbTgtMscVendorSpecific - request is %u\n", request,
                           2, 3, 4, 5, 6);
            return ERROR;
            }
        }
    else
        {
        USBTGT_MSC_ERR("usbTgtMscVendorSpecific - requestType is %u\n", requestType,
                       2, 3, 4, 5, 6);
        return ERROR;
        }

    return(retVal);
    }

/*******************************************************************************
*
* usbTgtMscDeletePipes - delete bulk in/out pipes for the MSC device
*
* This routine deletes bulk in/out pipes for the MSC device.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbTgtMscDeletePipes
    (
    pUSB_TGT_MSC_DEV pUsbTgtMscDev
    )
    {
    /* Valid paramete */

    if (pUsbTgtMscDev == NULL)
        return;

    /* Destroy the bulk in pipe if already created */
    
    if (pUsbTgtMscDev->bulkInPipe != NULL)
        {
        usbTgtDeletePipe (pUsbTgtMscDev->bulkInPipe);
        pUsbTgtMscDev->bulkInPipe = NULL;
        pUsbTgtMscDev->bulkInErpState = USB_TGT_MSC_ERP_IDLE;
        }
    
    /* Destroy the bulk out pipe if already created */
    
    if (pUsbTgtMscDev->bulkOutPipe != NULL)
        {
        usbTgtDeletePipe (pUsbTgtMscDev->bulkOutPipe);
        pUsbTgtMscDev->bulkOutPipe = NULL;
        pUsbTgtMscDev->bulkOutErpState = USB_TGT_MSC_ERP_IDLE;
        }
    }


/*******************************************************************************
*
* usbTgtMscCreatePipes - creat bulk in/out pipes for the MSC device 
*
* This routine creates bulk in/out pipes for the MSC device.
*
* RETURNS: OK, or ERROR if something wrong 
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtMscCreatePipes
    (
    pUSB_TGT_MSC_DEV pUsbTgtMscDev,
    UINT8            uConfig
    )
    {
    pUSB_ENDPOINT_DESCR      pEpDesc = NULL;
    USBTGT_PIPE_CONFIG_PARAM ConfigParam;
    UINT8                    uIfBase;

    if (pUsbTgtMscDev == NULL) 
        return ERROR;
    
    /* 
     * This function driver may be part of one compiste device 
     * Get the interface base.
     */
     
    if ( ERROR == usbTgtFuncInfoMemberGet(pUsbTgtMscDev->targChannel,
                                     USBTGT_FUNC_INFO_MEM_TYPE_uIfNumMin,
                                     &uIfBase))
        {
        USBTGT_MSC_ERR("Can not get the interface base number\n",
                        1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Find the right endpoint descriptor */
    
    pEpDesc = usbTgtFuncEndpointDescFind (pUsbTgtMscDev->targChannel,
                          uIfBase,            /* Only one interface */
                          0,                  /* Altsetting 0 */
                          USB_ATTR_BULK,      /* Bulk */
                          USB_ENDPOINT_IN,    /* IN */
                          0,                  /* Synch Type */
                          0,                  /* Usage */
                          1);                 /* The first one */

    /* Create the pipe for the endpoint */
    
    if (usbTgtCreatePipe (pUsbTgtMscDev->targChannel, 
                          pEpDesc,
                          uConfig, 
                          uIfBase,
                          0,
                          &pUsbTgtMscDev->bulkInPipe) != OK)
        {
        USBTGT_MSC_ERR("Create for bulk pipe failed.\n",
                       1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Set the configuration parameters */
    
    ConfigParam.uMaxErpCount = 1;
    ConfigParam.uMaxErpBufCount = 1;

    /* The max buffer size is 2K */
    
    ConfigParam.uMaxErpBufSize = USBTGT_MSC_MAX_ERP_BUF_SIZE; 
    
    /* Configure the pipe */

    if (ERROR ==  usbTgtConfigPipe(pUsbTgtMscDev->bulkInPipe, 
                                   &ConfigParam))
        {
        USBTGT_MSC_ERR("Configure for interrupt pipe failed.\n",
                       1, 2, 3, 4, 5, 6);

        usbTgtMscDeletePipes(pUsbTgtMscDev);

        return ERROR;
        }
    
    /* Find the right endpoint descriptor */
    
    pEpDesc = usbTgtFuncEndpointDescFind (pUsbTgtMscDev->targChannel,
                          uIfBase,            /* Only one interface */
                          0,                  /* Altsetting 0 */
                          USB_ATTR_BULK,      /* Bulk */
                          USB_ENDPOINT_OUT,   /* OUT */
                          0,                  /* Synch Type */
                          0,                  /* Usage */
                          1);                 /* The first one */

    /* Create the pipe for the endpoint */
    
    if (usbTgtCreatePipe (pUsbTgtMscDev->targChannel, 
                          pEpDesc,
                          uConfig, 
                          uIfBase,
                          0,
                          &pUsbTgtMscDev->bulkOutPipe) != OK)
        {
        USBTGT_MSC_ERR("Create for interrupt pipe failed.\n",
                       1, 2, 3, 4, 5, 6);
        
        usbTgtMscDeletePipes(pUsbTgtMscDev);

        return ERROR;
        }

    /* Set the configuration parameters */
    
    ConfigParam.uMaxErpCount = 1;
    ConfigParam.uMaxErpBufCount = 1;
    
    /* The max buffer size is 2K */
    
    ConfigParam.uMaxErpBufSize = USBTGT_MSC_MAX_ERP_BUF_SIZE; 
    
    /* Configure the pipe */

    if (ERROR ==  usbTgtConfigPipe(pUsbTgtMscDev->bulkOutPipe, 
                                   &ConfigParam))
        {
        USBTGT_MSC_ERR("Configure for interrupt pipe failed.\n",
                       1, 2, 3, 4, 5, 6);

        usbTgtMscDeletePipes(pUsbTgtMscDev);

        return ERROR;
        }
 
    USBTGT_MSC_DBG("Create all the pipes with bulk in %p, bulk out %p\n",
                   pUsbTgtMscDev->bulkInPipe,
                   pUsbTgtMscDev->bulkOutPipe, 
                   3 , 4, 5, 6);
    return OK;
    }

