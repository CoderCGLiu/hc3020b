/* usbXhcd.c - USB xHCI Driver Entry and Exit module */

/*
 * Copyright (c) 2011-2015 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */


/*
Modification history
--------------------
01k,03apr15,j_x  add post reset hook. (US54139)
01j,04Dec14,wyy  Remove compiler warning
01i,10Jul13,wyy  Make usbd layer to uniformly handle ClearTTBuffer request
                 (WIND00424927)
01h,03may13,wyy  Remove compiler warning (WIND00356717)
01g,11mar13,j_x  Clear Power Management build error (WIND00407552)
01f,19oct12,w_x  Simplify usbHcdXhciDeviceConnect() error handling using goto
01e,17oct12,w_x  Fix compiler warnings (WIND00370525)
01d,20sep12,w_x  Use separate default pipe for USB2 and USB3 bus (WIND00377413)
01c,12sep12,w_x  Keep root hub driver for backward compatibility (WIND00375595)
01b,04sep12,w_x  Address review comments and fix some defects (WIND00372413)
01a,09may11,w_x  written
*/

/*
DESCRIPTION

This contains the initialization and uninitialization routines provided by 
the xHCI Host Controller Driver.

INCLUDE FILES:
*/



#include <vxWorks.h>
#include "usbXhcdInterfaces.h"
#if defined (_WRS_CONFIG_PWR_MGMT) && defined (_WRS_ARCH_HAS_DEV_PWR_MGMT)
#include <hwif/pwr/pwrDeviceLib.h>
#endif

/* defines */

/* externs */

/* Routine to add root hub */

IMPORT VOID usbVxbRootHubAdd 
    (
    VXB_DEVICE_ID pDev
    );  

/* Routine to remove root hub */

IMPORT STATUS usbVxbRootHubRemove 
    (
    VXB_DEVICE_ID pDev
    );   
                                               
/* globals */

/* To hold the array of pointers to the HCD maintained data structures */

pUSB_XHCD_DATA * gpXHCDData = NULL;

/* To hold the handle returned by the USBD during HC driver registration */

UINT32  gXHCDHandle = 0;


STATUS usbXhcdInit (void);

STATUS usbXhcdExit (VOID);

VOID vxbUsbXhciRegister (VOID);

STATUS vxbUsbXhciDeregister (VOID);

VOID usbXhcdInstantiate (VOID);

int usbXhcdDisableHC
    (
    int startType 
    );

/* locals */

LOCAL BOOL usbVxbXhciDeviceProbe   
    (
    VXB_DEVICE_ID pDev
    );

LOCAL VOID usbVxbNullFunction 
    (
    VXB_DEVICE_ID pDev
    );

LOCAL VOID usbHcdXhciPlbInit 
    (
    VXB_DEVICE_ID pDev
    );  

LOCAL STATUS usbHcdXhciDeviceInit 
    (
    VXB_DEVICE_ID pDev
    );   

LOCAL VOID usbHcdXhciDeviceConnect 
    (
    VXB_DEVICE_ID pDev
    );

LOCAL STATUS usbHcdXhciDeviceRemove 
    (
    VXB_DEVICE_ID pDev
    ); 

#if defined (_WRS_CONFIG_PWR_MGMT) && defined (_WRS_ARCH_HAS_DEV_PWR_MGMT)
LOCAL STATUS  usbXhcdPwrD0UnInitStateSet (VXB_DEVICE_ID pDev);
VOID usbXhcdPciSave
    (
    VXB_DEVICE_ID pDev
    );
VOID usbXhcdPciRestore      
    (
    VXB_DEVICE_ID pDev
    );
#endif /* (_WRS_CONFIG_PWR_MGMT) && (_WRS_ARCH_HAS_DEV_PWR_MGMT) */

/* PCI VID/PID list that we want to support - 0xFFFF for all */

LOCAL PCI_DEVVEND usbVxbPcidevVendId[1] =
    {
    {0xffff,             /* device ID */
    0xffff}              /* vendor ID */
    };

/* VxBus method for driver unlinking - called when unregister the driver */

LOCAL DRIVER_METHOD usbVxbHcdXhciHCMethods[] =
    {
    DEVMETHOD(vxbDrvUnlink, usbHcdXhciDeviceRemove),
#if defined (_WRS_CONFIG_PWR_MGMT) && defined (_WRS_ARCH_HAS_DEV_PWR_MGMT)
    DEVMETHOD(pwrStateSet,  usbXhcdPwrStateSet),
#endif /* (_WRS_CONFIG_PWR_MGMT) && (_WRS_ARCH_HAS_DEV_PWR_MGMT) */
    DEVMETHOD_END
    };

/* VxBus device init routines for driver - called for PLB based xHCI */

LOCAL struct drvBusFuncs usbVxbHcdXhciPlbDriverFuncs =
    {
    usbHcdXhciPlbInit,          /* init 1 */
    usbVxbNullFunction,         /* init 2 */
    usbHcdXhciDeviceConnect     /* device connect */
    };

/* VxBus device init routines for driver - called for PCI based xHCI */

LOCAL struct drvBusFuncs usbVxbHcdXhciPciDriverFuncs =
    {
    usbVxbNullFunction,         /* init 1 */
    usbVxbNullFunction,         /* init 2 */
    usbHcdXhciDeviceConnect     /* device connect */
    };

/* initialization structure for xHCI Root Hub */

LOCAL struct drvBusFuncs        usbVxbRootHubDriverFuncs =
    {
    usbVxbNullFunction,         /* init 1 */
    usbVxbNullFunction,         /* init 2 */
    usbVxbRootHubAdd            /* device connect */
    };

/* method structure for xHCI Root Hub */

LOCAL struct vxbDeviceMethod    usbVxbRootHubMethods[2] =
    {
    DEVMETHOD(vxbDrvUnlink, usbVxbRootHubRemove),
    DEVMETHOD_END
    };


/* DRIVER_REGISTRATION for xHCI Root hub */

LOCAL DRIVER_REGISTRATION       usbVxbHcdXhciHub =
    {
    NULL,                       /* pNext */
    VXB_DEVID_BUSCTRL,          /* hub driver is bus
                                 * controller
                                 */
    VXB_BUSID_USB_HOST,         /* parent bus ID */
    USB_VXB_VERSIONID,          /* version */
    "vxbUsbXhciHub",            /* driver name */
    &usbVxbRootHubDriverFuncs,  /* struct drvBusFuncs * */
    &usbVxbRootHubMethods[0],   /* struct vxbDeviceMethod */
    NULL,               /* probe routine */
    NULL                /* vxbParams */
    };

/* Default PLB xHCI parameters */

LOCAL VXB_PARAMETERS usbVxbPlbHcdXhciDevParamDefaults[] =
    {
        {"descBigEndian", VXB_PARAM_INT32, {(void *)FALSE}},
        {"regBigEndian", VXB_PARAM_INT32, {(void *)FALSE}},
        {"postResetHook", VXB_PARAM_FUNCPTR, {(void *)NULL}},
        {NULL, VXB_PARAM_END_OF_LIST, {NULL}}
    };

/* Default PCI xHCI parameters */

LOCAL VXB_PARAMETERS usbVxbPciHcdXhciDevParamDefaults[] =
    {
        {"descBigEndian", VXB_PARAM_INT32, {(void *)FALSE}},
        {"regBigEndian", VXB_PARAM_INT32, {(void *)FALSE}},
        {"postResetHook", VXB_PARAM_FUNCPTR, {(void *)NULL}},
        {NULL, VXB_PARAM_END_OF_LIST, {NULL}}
    };

/* DRIVER_REGISTRATION for PLB based xHCI */

LOCAL DRIVER_REGISTRATION usbVxbHcdXhciPlbDev =
    {
    NULL,                                   /* register next driver */
    VXB_DEVID_BUSCTRL,                      /* bus controller */
    VXB_BUSID_PLB,                          /* bus id - PLB Bus Type */
    USB_VXB_VERSIONID,                      /* vxBus version Id */
    "vxbPlbUsbXhci",                        /* drv name */
    &usbVxbHcdXhciPlbDriverFuncs,           /* pDrvBusFuncs */
    &usbVxbHcdXhciHCMethods[0],             /* pMethods */
    NULL,                                   /* probe routine */
    usbVxbPlbHcdXhciDevParamDefaults        /* vxbParams */
    };

/* DRIVER_REGISTRATION for PCI based xHCI */

LOCAL PCI_DRIVER_REGISTRATION   usbVxbHcdXhciPciDev =
    {
        {
        NULL,                                   /* register next driver */
        VXB_DEVID_BUSCTRL,                      /* bus controller */
        VXB_BUSID_PCI,                          /* bus id - PCI Bus Type */
        USB_VXB_VERSIONID,                      /* vxBus version Id */
        "vxbPciUsbXhci",                        /* drv name */
        &usbVxbHcdXhciPciDriverFuncs,           /* pDrvBusFuncs */
        &usbVxbHcdXhciHCMethods [0],            /* pMethods */
        usbVxbXhciDeviceProbe,                  /* probe routine */
        usbVxbPciHcdXhciDevParamDefaults        /* vxbParams */
        },
        NELEMENTS(usbVxbPcidevVendId),          /* idListLen */
        & usbVxbPcidevVendId [0]                /* idList */
    };

/*******************************************************************************
*
* usbXhcdInstantiate - instantiate the USB xHCI Host Controller Driver.
*
* This routine instantiates  the xHCI Host Controller Driver and allows
* the xHCI Controller driver to be included with the vxWorks image and
* not be registered with vxBus.  xHCI devices will remain orphan devices
* until the usbXhciInit() routine is called.  This supports the
* INCLUDE_XHCI behaviour of previous vxWorks releases.
*
* The routine itself does nothing.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

VOID usbXhcdInstantiate (VOID)
    {
    return;
    }

/*******************************************************************************
*
* usbXhcdInit - initialize the xHCI Host Controller Driver
*
* This routine intializes the xHCI Host Controller Driver data structures.
* This routine is executed prior to vxBus device connect to initialize data 
* structures expected by the device initialization.
*
* The USBD must be initialized prior to calling this routine. In this routine
* the book-keeping variables for the xHCI Driver are initialized.
*
* The routine also registers the xHCI Host Controller Driver with USBD.
*
* RETURNS: OK or ERROR, if the initialization fails
*
* ERRNO: N/A
*/

STATUS usbXhcdInit (void)
    {
    /* To hold the status  */
    
    USBHST_STATUS       status = USBHST_FAILURE; 

    /* Structure to hold xHCI driver  informations */
     
    USBHST_HC_DRIVER    xHCDriver;   

    /*
     * Check whether the globals are initialized - This can happen if this
     * function is called more than once.
     */

    if (gpXHCDData != NULL)
        {
        USB_XHCD_ERR("XHCD already initialized\n",
            0, 0, 0, 0, 0, 0);

        return OK;
        }

    /* Initialize the global array */

    gpXHCDData = (pUSB_XHCD_DATA *)
        OSS_CALLOC(sizeof(pUSB_XHCD_DATA) * USB_MAX_HOSTS_PER_HCD);


    /* Check if memory allocation is successful */

    if (gpXHCDData == NULL)
        {
        USB_XHCD_ERR("allocation failed for gpXHCDData\n",
            0, 0, 0, 0, 0, 0);

        return ERROR;
        }

    /* Hook the routine which needs to be called on a reboot */
#if 0
    if (ERROR == rebootHookAdd((REBOOT_HOOK)usbXhcdDisableHC))
        {
        USB_XHCD_ERR(
            "Error hooking usbXhcdDisableHC\n",
            0, 0, 0, 0, 0, 0);

        /* Free the global array */

        OSS_FREE (gpXHCDData);

        gpXHCDData = NULL;

        return ERROR;
        }
#endif
    /* Initialize the members of the data structure */

    OS_MEMSET(&xHCDriver, 0, sizeof(USBHST_HC_DRIVER));

    /* Populate the members of the HC Driver data structure - Start */

    /* Function pointer to control the device characteristics */
    xHCDriver.deviceControl = usbXhcdDeviceControl;
    
    /* Function to retrieve the frame number */

    xHCDriver.getFrameNumber = usbXhcdGetFrameNumber;

    /* Function to change the frame interval */

    xHCDriver.setBitRate = usbXhcdSetBitRate;

    /* Function to check whether bandwidth is available */

    xHCDriver.isBandwidthAvailable = usbXhcdIsBandwidthAvailable;

    /* Function to create a pipe */

    xHCDriver.createPipe = usbXhcdCreatePipe;

    /* Function to modify the default pipe */

    xHCDriver.modifyDefaultPipe = usbXhcdModifyDefaultPipe;

    /* Function to delete the pipe */

    xHCDriver.deletePipe = usbXhcdDeletePipe;

    /* Function to check if the request is pending */

    xHCDriver.isRequestPending = usbXhcdIsRequestPending;

    /* Function to submit an URB */

    xHCDriver.submitURB = usbXhcdSubmitURB;

    /* Function to cancel an URB */

    xHCDriver.cancelURB = usbXhcdCancelURB;

    /* Function to control the pipe characteristics */

    xHCDriver.pipeControl = usbXhcdPipeControl;

    /* Function to submit a clear tt request complete  */

    xHCDriver.clearTTRequestComplete = NULL;

    /* Function to submit a clear tt request complete  */

    xHCDriver.resetTTRequestComplete = NULL;

    /* Populate the members of the HC Driver data structure - End */

    /*
     * Register the HCD with the USBD. We also pass the bus id in this function
     * This to to register xHCI driver with vxBus as a bus type.
     * After the registration is done we get a handle "gXHCDHandle". This
     * handle is used for all subsequent communication of xHCI driver with
     * USBD
     */

    status = usbHstHCDRegister(&xHCDriver,
                             &gXHCDHandle,
                             NULL,
                             VXB_BUSID_USB_HOST
                             );

    /* Check whether the registration is successful */

    if (USBHST_SUCCESS != status)
        {
        USB_XHCD_ERR(
            "Error registering the HCD\n",
            0, 0, 0, 0, 0, 0);

        /* Delete the reboot hook */
        #if 0
        if (ERROR == rebootHookDelete((REBOOT_HOOK)usbXhcdDisableHC))
            {
            USB_MHDRC_WARN("usbXhcdExit - Failure in rebootHookDelete\n",
                         1, 2, 3, 4, 5 ,6);
            /* Continue */
            }
        #endif
        /* Free the global array */

        OSS_FREE (gpXHCDData);

        gpXHCDData = NULL;

        return ERROR;
        }

    return OK;
    }/* End of usbXhcdInit() */

/*******************************************************************************
*
* usbXhcdExit - uninitialize the xHCI Host Controller Driver
*
* This routine uninitializes the xHCI Host Controller Driver and detaches it  
* from the USBD interface layer.
*
* RETURNS: OK, or ERROR if there is an error during HCD uninitialization.
*
* ERRNO: N/A
*/

STATUS usbXhcdExit(VOID)
    {
    /* Check to see if the HCD has been initialized */
    
    if (gpXHCDData == NULL)
        {
        USB_XHCD_ERR(
            "usbXhcdExit - gpXHCDData is NULL, HCD not initialized\n",
            1, 2, 3, 4, 5, 6);
        
        return ERROR;
        }

    /* Call the function to deregister with vxBus */

    if (vxbUsbXhciDeregister() != OK)
        {
        USB_EHCD_ERR(
            "Failed to Deregister xHCI Driver with vxBus\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }
    
    /* Disable the xHCI */
    
    (void) usbXhcdDisableHC(0);

    /* 
     * Deregister the HCD from USBD and check if HCD is deregistered
     * successfully
     */

    if (USBHST_SUCCESS != usbHstHCDDeregister(gXHCDHandle))
        {
        USB_XHCD_ERR(
            "usbXhcdExit - Failure in deregistering the HCD\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Delete the reboot hook */
    #if 0
    if (ERROR == rebootHookDelete((REBOOT_HOOK)usbXhcdDisableHC))
        {
        USB_XHCD_ERR("usbXhcdExit - Failure in rebootHookDelete\n",
                     1, 2, 3, 4, 5 ,6);

        return ERROR;
        }
	#endif
    gXHCDHandle = 0;

    /* Free the memory allocated for the global data structure */

    OSS_FREE(gpXHCDData);

    gpXHCDData = NULL;

    return OK;
    }

/*******************************************************************************
*
* usbXhcdDisableHC - disable the host controller
*
* This routine is called on a warm reboot to disable the host controller by
* the BSP.
*
* RETURNS: Always 0.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

int usbXhcdDisableHC
    (
    int startType
    )
    {
    /* Pointer to the HCD data structure */

    pUSB_XHCD_DATA      pHCDData = NULL;

    /* HCD index */
    
    UINT8               uBusIndex = 0;

    /* Check to see if the HCD has been initialized */

    if (gpXHCDData == NULL)
        {
        return 0;
        }

    /* This loop releases the resources for all the host controllers present */

    for (uBusIndex = 0; uBusIndex < USB_MAX_HOSTS_PER_HCD; uBusIndex++)
        {
        /* Extract the pointer from the global array */

        pHCDData = gpXHCDData[uBusIndex];

        /* Check if the pointer is valid */

        if (pHCDData == NULL)
            continue;

        /* 
         * Clear the Run/Stop bit to stop the Host Controller and 
         * then Reset the Host Controller 
         */
        
        if (usbXhcdHostCtlrReset(pHCDData) != OK)
            {
            USB_XHCD_WARN("usbXhcdDisableHC - HC reset fail\n",
                         1, 2, 3, 4, 5 ,6);
            }

        /* If anything needs to be done after the reset, do it now */

        if (pHCDData->pPostResetHook != NULL)
            {
            pHCDData->pPostResetHook();
            }

        }

    return 0;
    }

/*******************************************************************************
*
* usbHcdXhciDeviceInit - do basic vxBus related initialization
*
* This routine does basic vxBus related initialization. It creates a HC 
* instance data structure and then populates some parameters.
* 
* RETURN : N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbHcdXhciDeviceInit
    (
    VXB_DEVICE_ID       pDev                    /* struct vxbDev * */
    )
    {    
    pUSB_XHCD_DATA      pHCDData = NULL;
    
    UINT32              uCount = 0;
        
    VXB_INST_PARAM_VALUE    paramVal; 

    /* Validate parameters */

    if (pDev == NULL)
        {
        USB_XHCD_ERR("pDev NULL\n",
            1, 2, 3, 4, 5, 6);
        
        return ERROR;
        }

    /* Get the next available unit number */
    
    (void) vxbNextUnitGet (pDev);

    USB_XHCD_DBG("Add xHCI unitNumber %d @%p\n", 
        pDev->unitNumber, pDev, 3, 4, 5, 6);

    /* Allocate the HC instance data */
    
    pHCDData = OSS_CALLOC(sizeof(USB_XHCD_DATA));

    if (pHCDData == NULL)
        {
        USB_XHCD_ERR("pHCDData NULL\n",
            1, 2, 3, 4, 5, 6);
        
        return ERROR;
        }

    for (uCount = 0; uCount < USB_MAX_HOSTS_PER_HCD; uCount++)
        {
        if (gpXHCDData[uCount] == NULL)
            {
            pHCDData->uBusIndex = uCount;
            gpXHCDData[uCount] = pHCDData;
            
            USB_XHCD_DBG("Got uBusIndex %d\n",
                pHCDData->uBusIndex, 2, 3, 4, 5, 6);
            
            break;
            }
        }

    if (uCount == USB_MAX_HOSTS_PER_HCD)
        {
        USB_XHCD_ERR("No more empty entries\n",
            1, 2, 3, 4, 5, 6);

        /* Free the memory allocated */

        OSS_FREE(pHCDData);
        
        return ERROR;        
        }
    
    /* Save the pDev */
    
    pHCDData->pDev = pDev;
    
    /*
     * Map the access registers.  This is needed for subsequent operations.  
     * We look at the pDev->regBaseFlags which are set when the pci bus 
     * device is announced to vxBus. The device announce, based on the 
     * BAR will set VXB_REG_IO, VXB_REG_MEM, VXB_REG_NONE or VXB_REG_SPEC.
     *
     * The following test identifies the BARs to use.
     */

    for (uCount = 0; uCount < VXB_MAXBARS; uCount ++)
        {
        /* 
         * xHCs are also required to implement at least the first two 
         * Base Address Registers (BAR 0 and BAR 1) to enable 64-bit 
         * addressing. These Base Address Registers are used to point 
         * to the start of the host controller's memory-mapped 
         * Input/Output (MMIO) register space.
         */
         
        if (pDev->regBaseFlags[uCount] == VXB_REG_MEM)
            {
            /*
             * Initialize the system I/O memory maps, if supported. The 
             * BIOS should have set the Frame Length Adjustment (FLADJ) 
             * register to a system-specific value.
             */
             
            if (vxbRegMap(pHCDData->pDev, (int)uCount,
                           &pHCDData->pRegAccessHandle) == OK)
                {
                /* Save the register base */

                pHCDData->uCpRegBase = (ULONG)pDev->pRegBase[uCount];

                /* Record the register index */

                pHCDData->uBarIndex = uCount;

                break;
                }
            }
        }

    if (uCount >= VXB_MAXBARS)
        {
        /*
         * No usable BARs found.
         */

        /* Reset the global array */

        gpXHCDData[pHCDData->uBusIndex] = NULL;

        /* Free the memory allocated */

        OSS_FREE(pHCDData);

        USB_XHCD_ERR("No usable BARs found\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * Check if we are required do a post reset operation;
     * This is normally required for some dual role devices
     * whose host module will need to be set to 'host mode'
     * after controller reset.
     */

    if (OK != vxbInstParamByNameGet(pDev,
                                    "postResetHook",
                                    VXB_PARAM_FUNCPTR,
                                    &paramVal))
        {
        /* Unmap the register space */

        (void) vxbRegUnmap (pHCDData->pDev, pHCDData->uBarIndex);

        /* Reset the global array */
        
        gpXHCDData[pHCDData->uBusIndex] = NULL;

        /* Free the memory allocated */

        OSS_FREE(pHCDData);

        USB_XHCD_ERR("usbHcdXhciDeviceInit - Get postResetHook error\n",
                     0, 0, 0, 0, 0, 0);
        }

    pHCDData->pPostResetHook = paramVal.funcVal;
    
    switch (pDev->busID)
        {
        case VXB_BUSID_PLB:
            /*
             * USB is little endian, if CPU is big endian, we need this swap!
             * This is to swap data such as the PortStatus and PortStatusChange
             * to/from CPU endian to set or clear any bit; And when reporting
             * to the USBD (by URB callback), swap into Little Endian so that
             * it conforms to USB requirement.
             */

#if (_BYTE_ORDER == _BIG_ENDIAN)
             pHCDData->pUsbSwap32 = vxbSwap32;
#endif

            /*
             * Normal xHCI data structures are little endian, if the CPU is
             * big endian and it implements these data structures to be little
             * endian, then we need this swap; But if the CPU is big endian
             * and it implements these data structures to be also big endian,
             * then we do not need this swap! In short, if the descriptor 
             * endian format is different with CPU endian format, we need this 
             * swap!
             */

            if (OK != vxbInstParamByNameGet(pDev,
                                   "descBigEndian",
                                   VXB_PARAM_INT32,
                                   &paramVal))
                {
                /* Unmap the register space */

                (void) vxbRegUnmap (pHCDData->pDev, pHCDData->uBarIndex);

                /* Reset the global array */
                
                gpXHCDData[pHCDData->uBusIndex] = NULL;

                /* Free the memory allocated */

                OSS_FREE(pHCDData);

                USB_XHCD_ERR(
                    "usbHcdXhciDeviceInit - Get descBigEndian error\n",
                    0, 0, 0, 0, 0, 0);

                return ERROR;
                }

            USB_XHCD_DBG(
                "xHCI desc endian %s \n",
                (ULONG)((paramVal.int32Val) ?
                "_BIG_ENDIAN" : "_LITTLE_ENDIAN"), 2, 3, 4, 5, 6);

#if (_BYTE_ORDER == _BIG_ENDIAN)

            /* CPU BE, but DESC LE */

            if (paramVal.int32Val == FALSE)
                {
                pHCDData->pDescSwap32 = vxbSwap32;
                pHCDData->pDescSwap64 = vxbSwap64;
                }
#else
            /* CPU LE, but DESC BE */

            if (paramVal.int32Val == TRUE)
                {
                pHCDData->pDescSwap32 = vxbSwap32;
                pHCDData->pDescSwap64 = vxbSwap64;
                }
#endif

            /*
             * Normal xHCI is little endian, if the CPU is big endian and
             * it implements xHCI registers in little endian, then we need
             * this swap. But if the CPU is big endian, and it implements 
             * xHCI registers in big endian, then we do not need this swap!
             * In short, if the register endian format is different with 
             * CPU endian format, we need this swap!
             */

            if (OK != vxbInstParamByNameGet(pDev,
                                   "regBigEndian",
                                   VXB_PARAM_INT32,
                                   &paramVal))
                {
                /* Unmap the register space */

                (void) vxbRegUnmap (pHCDData->pDev, pHCDData->uBarIndex);

                /* Reset the global array */
                
                gpXHCDData[pHCDData->uBusIndex] = NULL;

                /* Free the memory allocated */

                OSS_FREE(pHCDData);

                USB_XHCD_ERR(
                    "usbHcdXhciDeviceInit - Get regBigEndian error\n",
                    0, 0, 0, 0, 0, 0);

                return ERROR;
                }

            USB_XHCD_DBG(
                "xHCI reg endian %s \n",
                (ULONG)((paramVal.int32Val) ?
                "_BIG_ENDIAN" : "_LITTLE_ENDIAN"), 2, 3, 4, 5, 6);

#if (_BYTE_ORDER == _BIG_ENDIAN)

            /* CPU BE, but REG LE */

            if (paramVal.int32Val == FALSE)
                {
                pHCDData->pRegSwap32 = vxbSwap32;
                pHCDData->pRegSwap64 = vxbSwap64;
                }
#else
            /* CPU LE, but REG BE */

            if (paramVal.int32Val == TRUE)
                {
                pHCDData->pRegSwap32 = vxbSwap32;
                pHCDData->pRegSwap64 = vxbSwap64;
                }
#endif

            /* Update the vxBus read-write handle */

            if (pHCDData->pRegSwap32 != NULL)
                {
                pHCDData->pRegAccessHandle = (void *)
                    VXB_HANDLE_SWAP ((ULONG)((pHCDData)->pRegAccessHandle));
                }

            break;

        case VXB_BUSID_PCI:

            /*
             * The underlying bus type is PCI. Query the underlying bus for
             * cpu to physical memory converison functions and populate the
             * required function pointers
             */

            /*
             * populate the function pointer for byte converions. Byte swapping
             * is required only if bus type is PCI and architecture is BE.
             * NOTE: for PLB no byte swapping is required
             */

#if (_BYTE_ORDER == _BIG_ENDIAN)
            /*
             * PCI is little endian, and most USB PCI controller cards use
             * little endian format for these xHCI data structures, so on
             * big endian systems we need this swap! As of this writing, we
             * have not seen any USB PCI card that use big endian data
             * strcutures, so we right now only consider the normal case.
             * If someday we really see such a USB PCI card that use big endian
             * data structures, we may need to use the same mechanism as for
             * the PLB controllers(see above)!
             */

             pHCDData->pDescSwap32 = vxbSwap32;
             pHCDData->pUsbSwap32 = vxbSwap32;
             pHCDData->pDescSwap64 = vxbSwap64;
             pHCDData->pUsbSwap64 = vxbSwap64;
#endif
            break;

        default:
            /* Unmap the register space */

            (void) vxbRegUnmap (pHCDData->pDev, pHCDData->uBarIndex);

            /* Reset the global array */
            
            gpXHCDData[pHCDData->uBusIndex] = NULL;

            /* Free the memory allocated */

            OSS_FREE(pHCDData);

            return ERROR;
        }

    /* Save the driver control */

    pDev->pDrvCtrl = (void *)pHCDData;

    return OK;
    }

/*******************************************************************************
*
* usbHcdXhciDeviceConnect - initializes the xHCI Host Controller instance
*
* This routine intializes the xHCI host controller and activates it to
* handle all USB operations. The function is called by vxBus on a successful
* driver - device match with VXB_DEVICE_ID as parameter. This structure has all
* information about the device. This routine resets all xHCI registers and
* then initializes them with default value. Once all the registers are properly
* initialized, it sets the RUN/STOP bit to activate the controller for USB
* operations
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbHcdXhciDeviceConnect
    (
    VXB_DEVICE_ID   pDev            
    )
    {    
    pUSB_XHCD_DATA  pHCDData = NULL;

    USBHST_STATUS   status = USBHST_FAILURE;

    UCHAR           ThreadName[20];

    /* validate paramters */
    if (pDev == NULL)
        {
        USB_XHCD_ERR("Invalid Parameters \n",
            0, 0, 0, 0, 0, 0);

        return;
        }

    if (usbHcdXhciDeviceInit (pDev) != OK)
        {
        USB_XHCD_ERR("usbHcdXhciDeviceInit failed\n",
            0, 0, 0, 0, 0, 0);

        return;
        }
    
    /* Get the saved HCD data */
    
    pHCDData = pDev->pDrvCtrl;
        
    /*
     * Call the bus specific initialization function to initialize the
     * host controller in the system. This is primarily to get some
     * hardware parameters and make the hardware in quiescent state.
     */
     
    if (usbXhcdHardwareParamInit(pHCDData) != OK)
        {
        USB_XHCD_ERR("init hardware parameter failed\n",
            0, 0, 0, 0, 0, 0);
        goto xhcdHardwareParamInitFailed;
        }
    
    /* Initialize the Host Controller data structure */
    
    if (usbXhcdDataInit(pHCDData) != OK)
        {
        USB_XHCD_ERR("init HCD data failed\n",
            0, 0, 0, 0, 0, 0);
        goto xhcdDataInitFailed;
        }
    
    /* Create a default pipe to register to USBD */
        
    if (usbXhcdDefaultPipeCreate(pHCDData) != OK)
        {
        USB_XHCD_ERR("usbHcdXhciDeviceConnect - create default pipe failed\n",
            0, 0, 0, 0, 0, 0);
        goto xhcdDefaultPipeCreateFailed;        
        }

    /* Start the Host Controller */

    if (usbXhcdHostCtlrStart(pHCDData) != OK)
        {
        USB_XHCD_ERR("usbHcdXhciDeviceConnect - usbXhcdHostCtlrStart fail\n",
            0, 0, 0, 0, 0, 0);

        goto xhcdHstCtlrStartFailed;
        }

    /* Add the Host Controller Instance to the USBD */
    
    status = usbHstCtlrAdd(gXHCDHandle, pDev);

    /* Check if the bus is registered successfully */

    if (USBHST_SUCCESS != status)
        {
        USB_XHCD_ERR("usbHcdXhciDeviceConnect - usbHstCtlrAdd xHCD fail\n",
            0, 0, 0, 0, 0, 0);
        
        goto xhcdHstCtrlAddFailed;
        }
    
    /* Add the Super Speed bus to the USBD */

    status = usbHstBusAdd(gXHCDHandle,
                          pDev,
                          USBHST_SUPER_SPEED,
                          (ULONG)pHCDData->pDefaultUSB3Pipe);

    /* Check if the bus is registered successfully */

    if (USBHST_SUCCESS != status)
        {
        USB_XHCD_ERR("usbHcdXhciDeviceConnect - usbHstBusAdd USBHST_SUPER_SPEED fail\n",
            0, 0, 0, 0, 0, 0);
        
        goto xhcdSuperSpeedBusAddFailed;
        }

    /* Register the High Speed bus with the USBD */

    status = usbHstBusAdd(gXHCDHandle,
                          pDev,
                          USBHST_HIGH_SPEED,
                          (ULONG)pHCDData->pDefaultUSB2Pipe);

    /* Check if the bus is registered successfully */

    if (USBHST_SUCCESS != status)
        {
        USB_XHCD_ERR("usbHcdXhciDeviceConnect - usbHstBusAdd USBHST_HIGH_SPEED fail\n",
            0, 0, 0, 0, 0, 0);
        
        goto xhcdHighSpeedBusAddFailed;
        }

    /* Assign an unique name for the interrupt handler thread */

    snprintf((char*)ThreadName, 20, "xHCD_IH%d", pDev->unitNumber);

    /* Create the interrupt handler thread for handling the interrupts */

    pHCDData->IntHandlerThreadID = (TASK_ID)
        OS_CREATE_THREAD((char *)ThreadName,
                         USB_XHCD_INT_THREAD_PRIORITY,
                         usbXhcdInterruptHandler,
                         (long)pHCDData);

    /* Check whether the thread creation is successful */

    if ((TASK_ID)OS_THREAD_FAILURE == pHCDData->IntHandlerThreadID)

        {
        USB_XHCD_ERR("usbHcdXhciDeviceConnect - "
            "OS_CREATE_THREAD for IntHandlerThreadID fail\n",
            0, 0, 0, 0, 0, 0);
        goto xhcdIsrThreadCreateFailed;
        }

    /* Actually start the hardware */
    
    if (usbXhcdHardwareStart(pHCDData) != OK)
        {
        goto xhcdHardwareStartFailed;
        }
    
    USB_XHCD_DBG("usbHcdXhciDeviceConnect - xHCD HC instance connected!\n",
        0, 0, 0, 0, 0, 0);

    return;

xhcdHardwareStartFailed:
    /* Destory the interrupt handling task */

    OS_DESTROY_THREAD(pHCDData->IntHandlerThreadID);
    
    pHCDData->IntHandlerThreadID = OS_THREAD_FAILURE;

xhcdIsrThreadCreateFailed:
    
    /* Call the function to remove the High Speed bus */

    status = usbHstBusRemove(gXHCDHandle, 
                             pDev,
                             USBHST_HIGH_SPEED,
                             (ULONG)pHCDData->pDefaultUSB2Pipe);

    /* Check if the bus is deregistered successfully */

    if (USBHST_SUCCESS != status)
        {
        USB_XHCD_ERR("usbHstBusRemove USBHST_HIGH_SPEED fail\n",
            0, 0, 0, 0, 0, 0);
        }
    
xhcdHighSpeedBusAddFailed:
    
    /* Call the function to remove the Super Speed bus */

    status = usbHstBusRemove(gXHCDHandle, 
                             pDev,
                             USBHST_SUPER_SPEED,
                             (ULONG)pHCDData->pDefaultUSB3Pipe);

    /* Check if the bus is deregistered successfully */

    if (USBHST_SUCCESS != status)
        {
        USB_XHCD_ERR("usbHstBusRemove USBHST_SUPER_SPEED fail\n",
            0, 0, 0, 0, 0, 0);
        }
    
xhcdSuperSpeedBusAddFailed:

    /* Remove the Host Controller Instance from the USBD */

    status = usbHstCtlrRemove(gXHCDHandle, pDev);

    /* Check if the bus is deregistered successfully */

    if (USBHST_SUCCESS != status)
        {
        USB_XHCD_ERR("usbHstCtlrRemove() failed\n",
            0, 0, 0, 0, 0, 0);
        }
    
xhcdHstCtrlAddFailed:
    
    if (usbXhcdHostCtlrStop(pHCDData) != OK)
        {
        USB_XHCD_WARN("usbXhcdHostCtlrStop() failed\n",
            0, 0, 0, 0, 0, 0);
        }
    
xhcdHstCtlrStartFailed: 
    
    if (usbXhcdDefaultPipeDestroy(pHCDData) != OK)
        {
        USB_XHCD_WARN("usbXhcdDefaultPipeDestroy() failed\n",
            0, 0, 0, 0, 0, 0);
        }
    
xhcdDefaultPipeCreateFailed:
    
    if (usbXhcdDataUnInit(pHCDData) != OK)
        {
        USB_XHCD_WARN("usbXhcdDataUnInit() failed\n",
            0, 0, 0, 0, 0, 0);
        }
        
xhcdDataInitFailed:
    /* No resources allocated in the hardware param init step */
xhcdHardwareParamInitFailed:
    /* Unmap the register space */
    
    (void) vxbRegUnmap (pHCDData->pDev, pHCDData->uBarIndex);
    
    /* Reset the global array */
    
    gpXHCDData[pHCDData->uBusIndex] = NULL;
    
    /* Free the memory allocated */
    
    OSS_FREE(pHCDData);

    /*
     * Set pDev->pDrvCtrl to NULL so usbHcdXhciDeviceRemove()
     * will not do anything when xHCD exits.
     */
     
    pDev->pDrvCtrl = NULL;
    
    return;
    }

/*******************************************************************************
*
* usbHcdXhciDeviceRemove - remove the xHCI Host Controller instance
*
* This routine un-initializes the USB host controller instance. The routine
* is registered with vxBus and called when the driver is de-registered with
* vxBus. The routine will have VXB_DEVICE_ID as its parameter. This structure
* consists of all the information about the device. The routine will
* subsequently de-register the bus from USBD.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbHcdXhciDeviceRemove
    (
    VXB_DEVICE_ID   pDev            /* struct vxbDev */
    )
    {
    /* To hold the pointer to the HCD maintained data structure */
    
    pUSB_XHCD_DATA pHCDData;  
    
    /* To hold the status of the function */
    
    USBHST_STATUS  status;           

    /* validate the paramters */

    if (pDev == NULL)
        {
        USB_XHCD_ERR("usbHcdXhciDeviceRemove - pDev NULL\n",
            0, 0, 0, 0, 0, 0);

        return ERROR;
        }

    /* Get the saved HCD data */
    
    pHCDData = pDev->pDrvCtrl;

    /* Check if the pointer is valid */

    if (NULL == pHCDData)
        {
        USB_XHCD_ERR("usbHcdXhciDeviceRemove - pHCDData NULL\n",
            0, 0, 0, 0, 0, 0);
        
        return ERROR;
        }

    USB_XHCD_WARN("usbHcdXhciDeviceRemove - uBusIndex %d\n",
        pHCDData->uBusIndex, 0, 0, 0, 0, 0);
    
    /* Remove the Super Speed bus */

    status = usbHstBusRemove(gXHCDHandle, 
                             pDev,
                             USBHST_SUPER_SPEED,
                             (ULONG)pHCDData->pDefaultUSB3Pipe);

    /* Check if the bus is deregistered successfully */

    if (USBHST_SUCCESS != status)
        {
        USB_XHCD_ERR("usbHcdXhciDeviceRemove - usbHstBusRemove fail\n",
            0, 0, 0, 0, 0, 0);

        return ERROR;
        }

    /* Remove the High Speed bus */

    status = usbHstBusRemove(gXHCDHandle, 
                             pDev,
                             USBHST_HIGH_SPEED,
                             (ULONG)pHCDData->pDefaultUSB2Pipe);

    /* Check if the bus is deregistered successfully */

    if (USBHST_SUCCESS != status)
        {
        USB_XHCD_ERR("usbHcdXhciDeviceRemove - usbHstBusRemove fail\n",
            0, 0, 0, 0, 0, 0);

        return ERROR;
        }

    /* Remove the Host Controller Instance from the USBD */
    
    status = usbHstCtlrRemove(gXHCDHandle, pDev);
    
    /* Check if the bus is deregistered successfully */
    
    if (USBHST_SUCCESS != status)
        {
        USB_XHCD_ERR("usbHstCtlrRemove xHCD fail\n",
            0, 0, 0, 0, 0, 0);

        return ERROR;
        }

    /* Reset the Host Controller and Disable all the interrupts */

    if (usbXhcdHostCtlrStop(pHCDData) != OK)
        {
        USB_XHCD_ERR("usbHcdXhciDeviceRemove - stop host controller failed\n",
            0, 0, 0, 0, 0, 0);
        }

    /* Destory the interrupt handling task */
    
    if (pHCDData->IntHandlerThreadID != OS_THREAD_FAILURE)
        {
        OS_DESTROY_THREAD(pHCDData->IntHandlerThreadID);
        
        pHCDData->IntHandlerThreadID = OS_THREAD_FAILURE;
        }

    /* Destroy the default pipe */
        
    if (usbXhcdDefaultPipeDestroy(pHCDData) != OK)
        {
        USB_XHCD_ERR("usbHcdXhciDeviceRemove - destroy default pipe failed\n",
            0, 0, 0, 0, 0, 0);
        }

    /* Uninitialize the HCD data */
        
    if (usbXhcdDataUnInit(pHCDData) != OK)
        {
        USB_XHCD_ERR("usbHcdXhciDeviceRemove - uninitialize HCD data failed\n",
            0, 0, 0, 0, 0, 0);
        }

    /* Unmap the register space */
   
    (void) vxbRegUnmap (pHCDData->pDev, pHCDData->uBarIndex);

    /* Reset the array element */
    
    gpXHCDData[pHCDData->uBusIndex] = NULL;

    /* Release the memory allocated for the HCD data structure */

    OSS_FREE(pHCDData);

    /* Set the pDev->pDrvCtrl to NULL */
    
    pDev->pDrvCtrl = NULL;

    USB_XHCD_WARN("usbHcdXhciDeviceRemove - unitNumber %d OK\n",
        pDev->unitNumber, 0, 0, 0, 0, 0);

    return OK;
    }

/*******************************************************************************
* vxbUsbXhciRegister - register the xHCI Host Controller Driver with vxBus
*
* This routine registers the xHCI Host Controller Driver with vxBus.  
* Note that this can be called early in the initialization sequence.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID vxbUsbXhciRegister (VOID)
    {
    /* Register the xHCI driver for PCI bus as underlying bus */

    if (vxbDevRegister ((DRIVER_REGISTRATION *)&usbVxbHcdXhciPciDev) == ERROR)
        {
        USB_XHCD_ERR("vxbUsbXhciRegister - vxbDevRegister with PCI Bus fail\n",
            1, 2, 3, 4, 5, 6);

        return;
        }

    /* Register the xHCI driver for PLB bus as underlying bus */
#if 0
    if (vxbDevRegister ((DRIVER_REGISTRATION *)&usbVxbHcdXhciPlbDev) == ERROR)
        {
        USB_XHCD_ERR("vxbUsbXhciRegister - vxbDevRegister with PLB Bus fail\n",
            1, 2, 3, 4, 5, 6);
        
        return;
        }
#endif
	
    /* Register the xHCI driver for root hub */

    if (vxbDevRegister ((DRIVER_REGISTRATION *) &usbVxbHcdXhciHub) == ERROR)
        {
        USB_XHCD_DBG(
            "Error registering xHCI root hub\n", 1, 2, 3, 4, 5, 6);
        return;
        }

    USB_XHCD_DBG(
        "vxbUsbXhciRegister - xHCI Driver successfully registered with vxBus\n",
        1, 2, 3, 4, 5, 6);
    return;
    }


/*******************************************************************************
*
* vxbUsbXhciDeregister - de-registers xHCI driver with vxBus
*
* This routine de-registers the xHCI Driver with vxBus Module.
*
* RETURNS: OK, or ERROR if not able to de-register with vxBus
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS vxbUsbXhciDeregister (VOID)
    {
    /* De-register the xHCI root hub as bus controller driver */

    USB_XHCD_DBG("de-registering xHCI Root Hub with vxBus\n", 1, 2, 3, 4, 5, 6);

    if (vxbDriverUnregister (&usbVxbHcdXhciHub) == ERROR)
        {
        USB_EHCD_ERR(
            "Error de-registering xHCI Root Hub with vxBus\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* De-register the xHCI driver for PCI bus as underlying bus */

    USB_XHCD_DBG("vxbDriverUnregister - "
        "de-registering xHCI Driver with PCI Bus\n", 1, 2, 3, 4, 5, 6);

    if (vxbDriverUnregister((DRIVER_REGISTRATION *)&usbVxbHcdXhciPciDev) == ERROR)
        {
        USB_XHCD_ERR(
            "vxbUsbXhciDeregister - vxbDriverUnregister with PCI Bus fail\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* De-register the xHCI driver for PLB bus as underlying bus */

    USB_XHCD_DBG("vxbDriverUnregister - "
        "de-registering xHCI Driver with PLB Bus\n", 1, 2, 3, 4, 5, 6);

    if (vxbDriverUnregister((DRIVER_REGISTRATION *)&usbVxbHcdXhciPlbDev) == ERROR)
        {
        USB_XHCD_ERR(
            "vxbDriverUnregister - vxbDriverUnregister with PLB Bus fail\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    return OK;
    }

/*******************************************************************************
*
* usbVxbXhciDeviceProbe - determine whether matching device is xHCI
*
* This routine determines whether the matching device is an xHCI Controller or
* not. For PCI device type, the routine will probe the PCI Configuration Device
* for Bus ID, Device Id and Function ID to determine wheter the device is of
* xHCI type. If a matching device is found, the routine will return TRUE.
*
* RETURNS: TRUE or FALSE
*
* ERRNO: N/A
*
* \NOMANUAL
*/
LOCAL BOOL usbVxbXhciDeviceProbe
    (
    VXB_DEVICE_ID       pDev
    )
    {
    /* Bus id of parent bus */
    
    UINT32  uBusId = 0;      

    /* PCI class code and revision */
    
    UINT32  uPciClassRev = 0;
    /* Validate the paramter */

    if ((pDev == NULL) ||
        (pDev->pParentBus == NULL) ||
        (pDev->pParentBus->pBusType == NULL))
        return FALSE;

    /* Determine the bus type of parent bus */

    uBusId = pDev->pParentBus->pBusType->busID;

    if (uBusId != VXB_BUSID_PCI)
        {
        return FALSE;
        }


    /*
     * Read the config space using access functions
     */

    VXB_PCI_BUS_CFG_READ(pDev, 
        USB_XHCI_PCI_CLASS_REV_OFFSET,
        USB_XHCI_PCI_CLASS_REV_SIZE, 
        uPciClassRev);

    /* Check to see if it is PCI/PCIe based xHCI controller */

    if (USB_XHCI_PCI_CLASS_GET(uPciClassRev) == USB_XHCI_PCI_CLASS_CODE)
        {
        USB_XHCD_DBG(
            "usbVxbXhciDeviceProbe - USB 3.0 xHCI Host Controller Found\n",
            1, 2, 3, 4, 5, 6);
        /* Matching device found */
        return TRUE;
        }

    /* Match not found */

    return FALSE;
    }

/*******************************************************************************
*
* usbVxbNullFunction - dummy routine
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbVxbNullFunction
    (
    VXB_DEVICE_ID       pDev
    )
    {
    /* This is a dummy routine which simply returns */

    return ;
    }

/*******************************************************************************
*
* usbHcdXhciPlbInit - perform the BSP specific Initializaion
*
* For many PLB based controllers, different BSP level initialization is
* requried. This routine does the BSP specific initializaiton in case it is
* required
*
* RETURNS: N/A
*
* ERRNO: N/A
*
*\NOMANUAL
*/

LOCAL VOID usbHcdXhciPlbInit
    (
    VXB_DEVICE_ID pDev
    )
    {
    HCF_DEVICE  * pHcf = NULL;
    void (* pPlbInit1) (void) = NULL;

    /* Get the HCF device from the instance id */

    pHcf = hcfDeviceGet (pDev);

    if(pHcf != NULL)
        (void) devResourceGet (pHcf, "xhciInit", HCF_RES_ADDR,(void *)&(pPlbInit1));

    /*
     * Typically, the BSP may provide a way to set Frame Length Adjustment
     * (FLADJ) register to a system-specific value.
     */
     
    if (pPlbInit1 != NULL)
        {
        (*pPlbInit1)();
        }

    return;
    }

#if defined (_WRS_CONFIG_PWR_MGMT) && defined (_WRS_ARCH_HAS_DEV_PWR_MGMT)
/* 16 bit Power Management Control and Status register bit settings */

#define PCI_PMCSR_PME_ENABLE    0x0100
#define PCI_PMCSR_PME_STATUS    0x8000
#define PCI_PMCSR_PME_D3        0x0003

/*******************************************************************************
* 
* usbXhcdD0PowerStateSet - transition device to D0 state
* 
* This routine instructs the device to enter D0 state. It needs to be called
* before device can be enabled again. It also needs to be called by the generic 
* PCI Power Management code on a warm reboot, in order to retore access to PCI 
* Config registers.
* 
* RETURNS: N/A
* 
* ERRNO: N/A
*/

LOCAL VOID usbXhcdD0PowerStateSet
    (
    VXB_DEVICE_ID   pDev
    )
    {
    UINT16          uStatus;
    pUSB_XHCD_DATA  pHCDData;

    pHCDData = (pUSB_XHCD_DATA)pDev->pDrvCtrl;

    /* Turn off D3 if on, and set to D0 */

    VXB_PCI_BUS_CFG_READ (pDev, pHCDData->pciPmcsr, 2, uStatus);
    
    USB_XHCD_WARN("usbXhcdD0PowerStateSet - uStatus 0x%04x (before) pDev %p\n",
        uStatus, pDev, 3, 4, 5, 6);
    
    if (uStatus & PCI_PMCSR_PME_D3)
        {
        /*
         * Aparently we can write 0 to all off PMCSR to return to D0
         * and it will not affect the uStatus bits.
         */
 
        uStatus = 0;
        
        VXB_PCI_BUS_CFG_WRITE (pDev, pHCDData->pciPmcsr, 2, uStatus);
        }

    /* Give it a bit (100ms) of time to come out of D3 */

    OS_DELAY_MS(100);

    VXB_PCI_BUS_CFG_READ  (pDev, pHCDData->pciPmcsr, 2, uStatus);
    
    USB_XHCD_WARN("usbXhcdD0PowerStateSet - uStatus 0x%04x (after)\n",
        uStatus, 2, 3, 4, 5, 6);
    }

/*******************************************************************************
*
* usbXhcdD3PowerStateSet - transition device to D3 state
*
* This routine instructs the device to enter D3 state. It will clear the
* PME_Status bit first by writing a 1b to bit 15.
*
* RETURNS: N/A
*
* ERRNO: N/A     
*/

LOCAL VOID usbXhcdD3PowerStateSet
    (
    VXB_DEVICE_ID   pDev
    )
    {
    UINT16          uStatus;
    pUSB_XHCD_DATA  pHCDData;

    pHCDData = (pUSB_XHCD_DATA)pDev->pDrvCtrl;

    /*
     * Set D3 mode - This will clear the PME_Status bit if it was set.
     */

    VXB_PCI_BUS_CFG_READ  (pDev, pHCDData->pciPmcsr, 2, uStatus);
    USB_XHCD_WARN("usbXhcdD3PowerStateSet - uStatus 0x%04x (before) pDev %p\n",
        uStatus, pDev, 3, 4, 5, 6);
    uStatus |= PCI_PMCSR_PME_D3;
    VXB_PCI_BUS_CFG_WRITE (pDev, pHCDData->pciPmcsr, 2, uStatus);

    OS_DELAY_MS(100);        /* 100ms as per PCIe Spec */

    VXB_PCI_BUS_CFG_READ  (pDev, pHCDData->pciPmcsr, 2, uStatus);
    USB_XHCD_WARN("usbXhcdD3PowerStateSet - uStatus 0x%04x (after)\n",
        uStatus, 2, 3, 4, 5, 6);
    }

/*******************************************************************************
*
* usbXhcdSuspend - transition device to D3 (suspend) state
*
* This routine clears earlier wakeup event indications in the PCI config space
* and then stops the adapter and stores the PCI config registers in order to
* restore register contents on D0 (resume) requests. An ERROR will be returned
* if the interface cannot be stopped.
*
* RETURNS: OK or ERROR
*
* ERRNO: N/A     
*/

LOCAL STATUS usbXhcdSuspend
    (
    VXB_DEVICE_ID   pDev
    )
    {
    STATUS          uSts;
    UINT16          uStatus;
    pUSB_XHCD_DATA  pHCDData;
    UINT32          uIndex;
    
    pHCDData = (pUSB_XHCD_DATA)pDev->pDrvCtrl;

    /* Clear PCI Config Space PME_Status bit if set - bit is W1C */

    VXB_PCI_BUS_CFG_READ (pDev, pHCDData->pciPmcsr, 2, uStatus);
    VXB_PCI_BUS_CFG_WRITE (pDev, pHCDData->pciPmcsr, 2, uStatus);

    /* Save off the PCI config space */

    usbXhcdPciSave(pDev);

    /* 
     * Step 1: Stop all USB activity by issuing Stop Endpoint Commands
     * for each endpoint in the Running state. This shall cause the xHC
     * to update the respective Endpoint or Stream Context TR Dequeue
     * Pointer and DCS fields. 
     */
     
    for (uIndex = 1; uIndex < (pHCDData->uMaxSlots + 1); uIndex++)
        {
        if (pHCDData->ppDevSlots[uIndex])
            {
            uSts = usbXhcdDeviceSlotStop(pHCDData, 
                                         pHCDData->ppDevSlots[uIndex]);
            if (uSts != OK)
                {
                USB_XHCD_ERR("usbXhcdSuspend - stopping slot %d fail\n",
                    uIndex, 2, 3, 4, 5, 6);
                
                return ERROR;
                }
            }
        }
    
    /* Step 2: Stop the controller by setting Run/Stop (R/S) = '0'. */
    
    uSts = usbXhcdHardwareStop(pHCDData);
    if (uSts != OK)
        {
        USB_XHCD_ERR("usbXhcdSuspend - could not stop xHC %d\n", 
            pDev->unitNumber, 2, 3, 4, 5, 6);
        
        return ERROR;
        }
    
    usbXhcdCmdRingDeqSet(pHCDData);
    
    /* 
     * Step 3: Read the USBCMD, DNCTRL, CRCR, DCBAAP, and CONFIG Operational
     * registers and the IMAN, IMOD, ERSTSZ, ERSTBA, and ERDP Runtime
     * Registers and save their state.
     */
     
    uSts = usbXhcdRegSave(pHCDData);
    if (uSts != OK)
        {
        USB_XHCD_ERR("usbXhcdSuspend - could not save registers for xHC %d\n", 
            pDev->unitNumber, 2, 3, 4, 5, 6);
        
        return ERROR;
        }
    
    /* 
     * Step 4: Set the Controller Save State (CSS) flag in the USBCMD
     * register and wait for the Save State Status (SSS) flag in the
     * USBSTS register to transition to '0'.
     */
     
    uSts = usbXhcdInternalStateSave(pHCDData);
    if (uSts != OK)
        {
        USB_XHCD_ERR("usbXhcdSuspend - could not save insternal state for xHC %d\n", 
            pDev->unitNumber, 2, 3, 4, 5, 6);
        
        return ERROR;
        }

    /* 
     * Step 5: Remove Core Well power.
     *
     * After entering D3, we cannot access memory mapped or IO registers 
     * anymore until D0 mode is restored.
     */

    usbXhcdD3PowerStateSet(pDev);

    return OK;
    }

/*******************************************************************************
*
* usbXhcdResume - restores operation in D0 mode
*
* This routine will restore device operation after it has been in D3. It will
* enable memory and IO accesses by restoring the saved PCI configuration space
* registers required. In addition it will restart adapter operation after 
* saving the new values of PCI config registers. 
*
* RETURNS: OK or ERROR
*
* ERRNO: N/A     
*/

LOCAL STATUS usbXhcdResume
    (
    VXB_DEVICE_ID   pDev
    )
    {
    STATUS          uSts;

    pUSB_XHCD_DATA  pHCDData;

    pHCDData = (pUSB_XHCD_DATA)pDev->pDrvCtrl;

    /* Step 1: Enable Core Well power. */
    
    /* Restore D0 state and re-enable access to PCI Config space */

    usbXhcdD0PowerStateSet(pDev);

    /*
     * Restore PCI config space register contents in reverse order
     * according to PCI power management specified requirements.
     */

    usbXhcdPciRestore(pDev);
    
    /*
     * Save the restored PCI config space as the new space, since
     * this can differ from prior instances, especially since possible
     * events may have been indicated in the PMCSR which is in the PCI
     * config space - and now are cleared.
     * This is to take make sure we have the latest config space data
     * in situations when we want to do a warm reboot.
     */

    usbXhcdPciSave(pDev);

    /* 
     * Step 2: Restore the Operational and Runtime Registers with their
     * previously saved state.
     */
     
    uSts = usbXhcdRegRestore(pHCDData);
    if (uSts != OK)
        {
        USB_XHCD_ERR("usbXhcdSuspend - could not restore registers for xHC %d\n", 
            pDev->unitNumber, 2, 3, 4, 5, 6);
        
        return ERROR;
        }   
    
    usbXhcdCmdRingDeqSet(pHCDData);

    /* 
     * Step 3: Set the Controller Restore State (CRS) flag in the USBCMD 
     * register to '1' and wait for the Restore State Status (RSS) flag 
     * in the USBSTS register to transition to '0'.
     */

    uSts = usbXhcdInternalStateRestore(pHCDData);
    if (uSts != OK)
        {
        USB_XHCD_ERR("usbXhcdSuspend - could not restore internal state for xHC %d\n", 
            pDev->unitNumber, 2, 3, 4, 5, 6);
        
        return ERROR;
        }   

    /* Step 4: Enable the controller by setting Run/Stop (R/S) = '1'. */

    uSts = usbXhcdHardwareStart(pHCDData);
    if (uSts != OK)
        {
        USB_XHCD_ERR("usbXhcdSuspend - could not start for xHC %d\n", 
            pDev->unitNumber, 2, 3, 4, 5, 6);
        
        return ERROR;
        }

    return OK;
    }

/******************************************************************************
*
* usbXhcdPciSave - Save PCI config space registers
*
* This function saves PCI config registers and should be called before entering
* D3 mode. Only a few important registers are saved here that works for the
* 82574L card - HOWEVER - This function will be removed once the shared PCI code
* are done, and the global function will be used. 
*
* RETURNS: N/A
*
* ERRNO: N/A     
*/

VOID usbXhcdPciSave
    (
    VXB_DEVICE_ID pDev
    )
    {
    int i;
    pUSB_XHCD_DATA  pHCDData;

    pHCDData = (pUSB_XHCD_DATA)pDev->pDrvCtrl;

    for (i = 0; i < 16; i++)
        VXB_PCI_BUS_CFG_READ (pDev, i * 4, 4, pHCDData->pciConfig[i]);
    }

/******************************************************************************
*
* usbXhcdPciRestore - Restore PCI config space registers   
*
* This function restore PCI config registers in reverse order so that memory
* and IO access functionality can be restored.   
*
* NOTE - This function will be removed once the shared PCI code
* are done, and the global function will be used.                       
*
* RETURNS: N/A                    
*
* ERRNO: N/A
*/

VOID usbXhcdPciRestore      
    (
    VXB_DEVICE_ID pDev
    )
    {
    int i;
    UINT32 uCurrentVal;
    pUSB_XHCD_DATA  pHCDData;

    pHCDData = (pUSB_XHCD_DATA)pDev->pDrvCtrl;

    for (i = 15; i >= 0; i--)
        {
        VXB_PCI_BUS_CFG_READ (pDev, i * 4, 4, uCurrentVal);
        if (uCurrentVal != pHCDData->pciConfig[i])
            VXB_PCI_BUS_CFG_WRITE (pDev, i * 4, 4, pHCDData->pciConfig[i]);
        }
    
    OS_DELAY_MS(100);       /* 100 ms */
    }

/******************************************************************************
*
* usbXhcdPwrD0UnInitStateSet - Restore device D0 un-initialized state
*
* This routine restores the device context that was in use when the device last
* was in D0 mode. This includes the PMCSR and other PCI Config space registers.
* 
* This routine must be as small as possible and is typically called during a
* warm or fast reboot. It is required to restore the context before sysToMonitor
* exits, so that the device will be usable again when the bootrom executes.
*
* RETURNS: OK
*
* ERRNO: N/A
*/

LOCAL STATUS usbXhcdPwrD0UnInitStateSet
    (
    VXB_DEVICE_ID   pDev
    )
    {
    pUSB_XHCD_DATA  pHCDData;
    STATUS          result;

    if (pDev == NULL)
        return ERROR;

    pHCDData = (pUSB_XHCD_DATA *)pDev->pDrvCtrl;

    result = semTake (pHCDData->hcdMutex, WAIT_FOREVER);

    if (result != OK)
        return (result);
             
    usbXhcdD0PowerStateSet(pDev);
    
    usbXhcdPciRestore(pDev);
    
    OS_DELAY_MS(100);   /* Need to wait at least 100 ms */

    result = semGive (pHCDData->hcdMutex);

    return (result);
    }

/*******************************************************************************
*
* usbXhcdPwrStateSet - dispatch wrapper for usb xhcd pwrDeviceStateSet
*
* This routine will call the appropiate functions to enter D0 or D3
* power states. Our device do not support D1 or D2 and the current hardware
* simply ignore requests to enter those modes. 
*
* RETURNS: OK or ERROR
*
* ERRNO: N/A     
*/
 
STATUS usbXhcdPwrStateSet
    (
    VXB_DEVICE_ID   pDev,
    void *          state
    )
    {
    STATUS          retVal = 0;
    UINT32          uRequestedState = 0;
    UINT32          uMaskedState = 0;
    UINT32          uDeviceState;
    pUSB_XHCD_DATA  pHCDData = NULL;
    UINT32          uD0UnInitState = 0;

    uRequestedState = (UINT32)state;

    USB_XHCD_WARN("usbXhcdPwrStateSet - uRequestedState 0x%08x\n",
           uRequestedState, 2, 3, 4, 5, 6);

    /* Catch special state requests and mask it off */

    uD0UnInitState = uRequestedState & WRS_D0_UNINITIALIZED;
    uMaskedState = uRequestedState & ~(WRS_D0_UNINITIALIZED);

    pHCDData = (pUSB_XHCD_DATA)pDev->pDrvCtrl;

    if ((pDev->pwrDeviceId == NULL) ||
        (vxbPwrDeviceStateSetFunc == NULL))
        {
        return (ERROR);
        }
    
    semTake (pHCDData->hcdMutex, WAIT_FOREVER);

    uDeviceState = PWR_DEVICE_CURRENT_STATE (pDev->pwrDeviceId);

    USB_XHCD_WARN("usbXhcdPwrStateSet - set to D%d (current D%d)\n",
           uMaskedState, uDeviceState, 3, 4, 5, 6);
    
    if (uDeviceState == uMaskedState)
        {
        semGive (pHCDData->hcdMutex);

        USB_XHCD_DBG("usbXhcdPwrStateSet - same state (D%d), return!\n",
               uMaskedState, 2, 3, 4, 5, 6);
        
        return (OK);    /* no work to do */
        }

    /* If requested state is not enabled (allowed in config) */

    if (!PWR_DEVICE_STATE_ENABLED(pDev->pwrDeviceId, uMaskedState))
        {
        semGive (pHCDData->hcdMutex);

        USB_XHCD_WARN("usbXhcdPwrStateSet - state (D%d) not enabled, return!\n",
               uMaskedState, 2, 3, 4, 5, 6);
        
        return (ERROR);
        }

    /* If going up in state number */

    if (uMaskedState > uDeviceState)
        {
        /* Request state is D3 */

        if (uMaskedState == 3)
            {
            /* If the device was previously running */

            if (uDeviceState == 0)
                {
                /* Powering off, stop the device and set to D3 */

                retVal = usbXhcdSuspend(pDev);

                if (retVal != OK)
                    {
                    semGive (pHCDData->hcdMutex);

                    USB_XHCD_WARN("usbXhcdPwrStateSet - usbXhcdSuspend ERROR!\n",
                           1, 2, 3, 4, 5, 6);
                    
                    return (ERROR);
                    }
                }

            /* Set device record state */

            retVal = vxbPwrDeviceStateSetFunc (pDev->pwrDeviceId, uRequestedState);

            }
        else /* Currently only D3 is supported */
            {
            semGive (pHCDData->hcdMutex);
            
            USB_XHCD_WARN("usbXhcdPwrStateSet - not to D3!\n",
                   1, 2, 3, 4, 5, 6);
            
            return (ERROR);
            }

        }

    else  if (uMaskedState == 0)  /* Going down in state number */
        {
        /*
         * If requested state is D0 un-initialized, then only restore
         * device PCI Config registers so that we are able to access
         * the device in the bootrom to configure it again.
         * Restoring D0 un-initialized context is much faster then
         * restoring context and initializing the device for usage.
         * In WARM/FAST reboot cases we do not need to initialize the
         * device for software usage, so use D0 un-initialized state.
         */
         
        if (uD0UnInitState)
            {
            retVal = usbXhcdPwrD0UnInitStateSet(pDev);
            
            USB_XHCD_DBG("usbXhcdPwrStateSet - usbXhcdPwrD0UnInitStateSet return %d!\n",
                   retVal, 2, 3, 4, 5, 6);
            }
        else
            {
            retVal = usbXhcdResume(pDev);
            
            USB_XHCD_DBG("usbXhcdPwrStateSet - usbXhcdResume return %d!\n",
                   retVal, 2, 3, 4, 5, 6);
            }

        if (retVal != OK)
            {
            semGive (pHCDData->hcdMutex);

            USB_XHCD_WARN("usbXhcdPwrStateSet - ERROR return %d!\n",
                   retVal, 2, 3, 4, 5, 6);
            
            return (ERROR);
            }

        /* Set device record state */

        retVal = vxbPwrDeviceStateSetFunc (pDev->pwrDeviceId, uRequestedState);

        }
    else
        {
        USB_XHCD_ERR("usbXhcdPwrStateSet - invalid state D%d!\n",
               uMaskedState, 2, 3, 4, 5, 6);
        retVal = ERROR;
        }

    semGive (pHCDData->hcdMutex);

    USB_XHCD_DBG("usbXhcdPwrStateSet - done returning %d!\n",
           retVal, 2, 3, 4, 5, 6);
    
    return retVal;
    }
#endif /* (_WRS_CONFIG_PWR_MGMT) && (_WRS_ARCH_HAS_DEV_PWR_MGMT) */

