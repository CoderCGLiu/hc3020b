/* usbTgtUtil.c - USB Taget Stack Utility Module */

/*
 * Copyright (c) 2012,2014 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,02Sep14,bbz  clear the warnings of no check of the return value (VXW6-83095)
01a,05mar12,s_z  written
*/

/*
DESCRIPTION

This module defines the internal utlity routines used by the usb target
stack only.

INCLUDES: usbTgtUtil.h

*/

/* includes */

#include "usbTgtUtil.h"

/*******************************************************************************
*
* usbTgtFindPipeByEpIndex - find the pipe pointer by the endpoint index.
*
* This routine finds the pipe <'pUSBTGT_PIPE'> from the function driver's 
* usbTgtPipe list by the endpoint index.
*
* NOTE: the <uEpIndex> includes the endpoint direction and endpoint address
* information, such as the endpoint address of endpoint descripor.
* 
* RETURNS: pointer to the pipe or NULL if no found
*
* ERRNO: N/A
*/

pUSBTGT_PIPE usbTgtFindPipeByEpIndex
    (
    pUSBTGT_FUNC_DRIVER pFuncDriver,
    UINT8               uEpIndex
    )
    {
    pUSBTGT_PIPE pUsbTgtPipe = NULL;
    int          i;
    int          count;

    /* Validate parameters */
    
    if (pFuncDriver == NULL)
        {
        USB_TGT_ERR("Invalid parameter, pFuncDriver is NULL\n",
                     1, 2, 3, 4, 5 ,6);
        
        return (NULL);
        }
    
    OS_WAIT_FOR_EVENT(pFuncDriver->funcMutex, USBTGT_WAIT_TIMEOUT);

    count = lstCount (&pFuncDriver->pipeList);
    
    for (i = 1; i <= count; i ++)
        {
        pUsbTgtPipe = (pUSBTGT_PIPE) lstNth (&pFuncDriver->pipeList, i);

        if ((NULL != pUsbTgtPipe) &&
            (uEpIndex == pUsbTgtPipe->uEpAddr))
           {
           (void)OS_RELEASE_EVENT(pFuncDriver->funcMutex);
           
           USB_TGT_DBG("Find the pUsbTgtPipe 0x%X with the endpoint index 0x%X\n",
                       (ULONG)pUsbTgtPipe, uEpIndex, 3, 4, 5 ,6);

           return pUsbTgtPipe;
           }
        }

    /* Release the event */

    (void)OS_RELEASE_EVENT(pFuncDriver->funcMutex);
    
    USB_TGT_ERR("No find the pUsbTgtPipe who uses the endpoint index 0x%X\n",
                uEpIndex, 2, 3, 4, 5 ,6);

    return NULL;
    }

    
/*******************************************************************************
*
* usbTgtFindFuncByEpIndex - find the function driver by the endpoint index.
*
* This routine finds the function driver which occupys endpoint 
* who has the endpoint index.
* 
* NOTE: the <uEpIndex> includes the endpoint direction and endpoint address
* information.
*
* RETURNS: pointer to the funtion driver or NULL if no found
*
* ERRNO: N/A
*/

pUSBTGT_FUNC_DRIVER usbTgtFindFuncByEpIndex
    (
    pUSBTGT_TCD pTcd,
    UINT8       uEpIndex
    )
    {
    NODE *              pNode = NULL;
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;
    int                 i;
    int                 count;

    /* Validate parameters */

    if (pTcd == NULL)
        {
        USB_TGT_ERR("Invalid parameter, pTcd is NULL\n",
                     1, 2, 3, 4, 5 ,6);
        
        return (NULL);
        }
    
    OS_WAIT_FOR_EVENT(pTcd->tcdMutex, USBTGT_WAIT_TIMEOUT);

    count = lstCount (&pTcd->funcList);
    
    for (i = 1; i <= count; i ++)
        {
        pNode = lstNth (&pTcd->funcList, i);
        pFuncDriver = TCD_LIST_NODE_TO_USBTGT_FUNC(pNode);

         if ((NULL != pFuncDriver) &&
              (pTcd->uCurrentConfig == pFuncDriver->uCurrentConfig) &&
             (NULL != usbTgtFindPipeByEpIndex(pFuncDriver,uEpIndex)))
            {

            (void)OS_RELEASE_EVENT(pTcd->tcdMutex);
            
            USB_TGT_DBG("Find the pFuncDriver 0x%X who uses the endpoint index 0x%X\n",
                       (ULONG)pFuncDriver, uEpIndex, 3, 4, 5 ,6);

            return pFuncDriver;
            
            }
        }

    /* Release the event */

    (void)OS_RELEASE_EVENT(pTcd->tcdMutex);
    
    USB_TGT_ERR("No find the funtion driver who uses the endpoint index 0x%X\n",
                uEpIndex, 2, 3, 4, 5 ,6);

    return NULL;
    }

/*******************************************************************************
*
* usbTgtFindFuncByInterface - find the function driver by the interface number.
*
* This routine finds the function driver who occupys the interface 
* number from the the function driver list of <'pUSBTGT_TCD'>.
*
* RETURNS: pointer to the funtion driver or NULL if no found
*
* ERRNO: N/A
*/

pUSBTGT_FUNC_DRIVER usbTgtFindFuncByInterface
    (
    pUSBTGT_TCD pTcd,
    UINT8       uInterface
    )
    {
    NODE *              pNode = NULL;
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;
    int                 i;
    int                 count;

    /* Validate parameters */

    if (pTcd == NULL)
        {
        USB_TGT_ERR("Invalid parameter, pTcd is NULL\n",
                     1, 2, 3, 4, 5 ,6);
        
        return (NULL);
        }
    
    OS_WAIT_FOR_EVENT(pTcd->tcdMutex, USBTGT_WAIT_TIMEOUT);

    count = lstCount (&pTcd->funcList);
    
    for (i = 1; i <= count; i ++)
        {
        pNode = lstNth (&pTcd->funcList, i);
        pFuncDriver = TCD_LIST_NODE_TO_USBTGT_FUNC(pNode);

         if ((NULL != pFuncDriver) &&
             (uInterface < pFuncDriver->uIfNumMax) &&
             (uInterface >= pFuncDriver->uIfNumMin))
            {

            (void)OS_RELEASE_EVENT(pTcd->tcdMutex);

            USB_TGT_DBG("Find the pFuncDriver 0x%X who occupys the interface num 0x%X\n",
                       (ULONG)pFuncDriver, uInterface, 3, 4, 5 ,6);

            return pFuncDriver;
            
            }
        }

    /* Release the event */

    (void)OS_RELEASE_EVENT(pTcd->tcdMutex);

    USB_TGT_ERR("No find the funtion driver who occupys the interface num 0x%X\n",
                uInterface, 2, 3, 4, 5 ,6);

    return NULL;
    }


