/* usbTgtRndisMsgProcess.c - USB Remote NDIS Message process Module */

/*
 * Copyright (c) 2011-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
Modification history
--------------------
01n,06may13,s_z  Remove compiler warning (WIND00356717)
01m,04jan13,s_z  Remove compiler warning (WIND00390357)
01l,18may12,s_z  Add support for USB 3.0 target (WIND00326012) 
01k,15may12,s_z  Correct debugging display issue (WIND00348017)
01j,13dec11,m_y  Modify according to code check result (WIND00319317)
01i,29sep11,s_z  Add direct callback mode to avoid asynch issue (WIND00306920)
01h,28jul11,s_z  Ignore add unicast addresses to the multicast list issue
01g,12jul11,s_z  Code clean with control request callback update of TML
01f,21apr11,s_z  Correct the misuse of ERP cancel status
01e,11apr11,s_z  Add Open Specifications Documentation description
01d,08apr11,ghs  Fix code coverity issue(WIND00264893)
01c,28mar11,s_z  Correct the support list members definition
01b,09mar11,s_z  Code clean up
01a,04jan11,s_z  written
*/

/*
DESCRIPTION

This file includes the message process routines for the RNDIS function driver.
It includes the process all the supported control message and OIDs process.

This RNDIS function driver follows the [MS-RNDIS]:Remote Network Driver 
Interface Specification (RNDIS) Protocol Specification 1.0, which is one of the
Open Specifications Documentation.

Following is the Intellectual Property Rights Notice for Open Specifications 
Documentation:

\h Intellectual Property Rights Notice for Open Specifications Documentation

\h Technical Documentation. 

Microsoft publishes Open Specifications documentation for protocols, 
file formats, languages, standards as well as overviews of the interaction 
among each of these technologies.

\h Copyrights. 

This documentation is covered by Microsoft copyrights. Regardless of any other 
terms that are contained in the terms of use for the Microsoft website that
hosts this documentation, you may make copies of it in order to develop 
implementations of the technologies described in the Open Specifications 
and may distribute portions of it in your implementations using these 
technologies or your documentation as necessary to properly document the 
implementation. You may also distribute in your implementation, with or without 
modification, any schema, IDL's, or code samples that are included in the 
documentation. This permission also applies to any documents that are 
referenced in the Open Specifications.

\h No Trade Secrets.

Microsoft does not claim any trade secret rights in this documentation.

\h Patents.

Microsoft has patents that may cover your implementations of the technologies 
described in the Open Specifications. Neither this notice nor Microsoft's 
delivery of the documentation grants any licenses under those or any other 
Microsoft patents. However, a given Open Specification may be covered by 
Microsoft's Open Specification Promise (available here: 
http://www.microsoft.com/interop/osp) or the Community Promise 
(available here: http://www.microsoft.com/interop/cp/default.mspx).
If you would prefer a written license, or if the technologies described in 
the Open Specifications are not covered by the Open Specifications Promise 
or Community Promise, as applicable, patent licenses are available by 
contacting iplg@microsoft.com.

\h Trademarks. 

The names of companies and products contained in this 
documentation may be covered by trademarks or similar intellectual 
property rights. This notice does not grant any licenses under those rights.

\h Fictitious Names.

The example companies, organizations, products, 
domain names, e-mail addresses, logos, people, places, and events depicted in 
this documentation are fictitious. No association with any real company, 
organization, product, domain name, email address, logo, person, place, or 
event is intended or should be inferred.

\h Reservation of Rights. 

All other rights are reserved, and this notice does 
not grant any rights other than specifically described above, whether by 
implication, estoppel, or otherwise.

\h Tools. 

The Open Specifications do not require the use of Microsoft 
programming tools or programming environments in order for you to develop an 
implementation. If you have access to Microsoft programming tools and 
environments you are free to take advantage of them. Certain Open 
Specifications are intended for use in conjunction with publicly available 
standard specifications and network programming art, and assumes that the 
reader either is familiar with the aforementioned material or has immediate 
access to it.

INCLUDE FILES:  etherMultiLib.h endLib.h usb/usbCdc.h usb/usbTgt.h 
                usb/usbTgtRndis.h usbTgtNetMediumBinder.h usbTgtFunc.h

*/

/* includes */

#include <etherMultiLib.h>
#include <endLib.h>
#include <usb/usbCdc.h>
#include <usb/usbTgt.h>
#include <usb/usbTgtRndis.h>
#include <usbTgtNetMediumBinder.h>
#include <usbTgtFunc.h>

/* globals */

IMPORT STATUS usbTgtSubmitControlErp
    (
    pUSBTGT_TCD pTcd
    );

IMPORT STATUS usbTgtControlPipeStall
    (
    pUSBTGT_TCD pTcd
    );

IMPORT void usbTgtInitStatusErp
    (
    pUSBTGT_TCD pTcd,
    BOOL        isStatusIn
    );

IMPORT BOOL usrUsbTgtRndisPMEnableGet
    (
    int index
    );

LOCAL void usbTgtRndisBulkOutErpCallback
    (
    void * pErpCallback
    );

LOCAL void usbTgtRndisBulkInErpCallback
    (
    void * pErpCallback
    );


/******************************************************************************
*
* usbTgtRndisIntInErpCallback - be called when the interrupt in ERP terminates
*
* This routine is called when the interrupt in ERP terminates
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/

LOCAL void usbTgtRndisIntInErpCallback
    (
    void * pErpCallback
    )
    {
    pUSB_ERP      pErp = (pUSB_ERP) pErpCallback;
    pUSBTGT_RNDIS pUsbTgtRndis = NULL;

    if ((NULL == pErp) ||
        (NULL == pErp->userPtr))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pErp) ? "pErp" :
                         "pErp->userPtr"), 2, 3, 4, 5, 6);

        return;
        }

    USBTGT_RNDIS_VDBG ("usbTgtRndisIntInErpCallback\n", 1, 2, 3, 4, 5, 6);

    pUsbTgtRndis = (pUSBTGT_RNDIS)pErp->userPtr;

    pUsbTgtRndis->bIntInErpUsed = FALSE;

    return;
    }

/******************************************************************************
*
* usbTgtRndisMsgAvailableNotify - send the response available notification
*
* This routine sends the response available notification
* to the host on the interrupt IN channel.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO:N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtRndisMsgAvailableNotify
    (
    pUSBTGT_RNDIS pUsbTgtRndis
    )
    {
    RESPONSE_AVAILABLE_MSG * pRespAvailablemsg = NULL;
    STATUS                   status;


    if (NULL == pUsbTgtRndis)
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ("pUsbTgtRndis"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    if ((TRUE == pUsbTgtRndis->bIntInErpUsed) ||
        (semTake(pUsbTgtRndis->intInErpMutex, NO_WAIT) != OK))
        {
        USBTGT_RNDIS_WARN("semTake the Interrupt in ownship fails IntInErpMutex 0x%X\n",
                         (ULONG)pUsbTgtRndis->intInErpMutex, 2, 3, 4, 5, 6);

        /* It still using */

        return ERROR;
        }

    /* Set the pipe is using */

    pUsbTgtRndis->bIntInErpUsed = TRUE;

    pRespAvailablemsg = (RESPONSE_AVAILABLE_MSG *)&pUsbTgtRndis->intInBuf[0];

    /* Send response available message via interrupt pipe */

    pRespAvailablemsg->msgType = TO_LITTLEL(USB_CDC_NOTIFY_RESPONSE_AVAILABLE);
    pRespAvailablemsg->reserved = 0;

    /* Init the Erp */

    memset(&pUsbTgtRndis->intInErp, 0, sizeof (USB_ERP));

    pUsbTgtRndis->intInErp.erpLen = sizeof (USB_ERP);
    pUsbTgtRndis->intInErp.userPtr = (pVOID)pUsbTgtRndis;
    pUsbTgtRndis->intInErp.userCallback = usbTgtRndisIntInErpCallback;
    pUsbTgtRndis->intInErp.targCallback = usbTgtErpCallback;
    pUsbTgtRndis->intInErp.bfrCount = 1;
    pUsbTgtRndis->intInErp.bfrList[0].pid = USB_PID_IN;
    pUsbTgtRndis->intInErp.bfrList[0].pBfr = (pUINT8) pRespAvailablemsg;
    pUsbTgtRndis->intInErp.bfrList[0].bfrLen = sizeof(RESPONSE_AVAILABLE_MSG);

    /* Submit the Erp */

    USBTGT_RNDIS_VDBG ("usbTgtRndisMsgAvailableNotify submit the Interrupt Erp \n",
                      1, 2, 3, 4, 5, 6);

    status = usbTgtSubmitErp(pUsbTgtRndis->intInPipeHandle,
                             &pUsbTgtRndis->intInErp);

    /* Check the status */

    if (status != OK)
        {
        USBTGT_RNDIS_ERR("Submit the interrupt in "
                          "Erp with status %d\n",
                          status, 2, 3, 4, 5, 6);

        pUsbTgtRndis->bIntInErpUsed = FALSE;

        semGive(pUsbTgtRndis->intInErpMutex);

        return ERROR;
        }

    semGive(pUsbTgtRndis->intInErpMutex);

    USBTGT_RNDIS_VDBG("usbTgtRndisMsgAvailableNotify submit the interrupt in "
                      "Erp with status %d\n",
                      status, 2, 3, 4, 5, 6);

    return(status);
    }


/******************************************************************************
*
* usbTgtRndisBulkOutErpSubmit - submit the bulk out ERP
*
* This routine submits the bulk out erp to get the data from
* the host data channel. The bulk out erp will be initialized in the
* frist step.
*
* RETURNS: OK/ERROR if there is something wrong
*
* ERRNO:N/A
*
* \NOMANUAL
*/

STATUS usbTgtRndisBulkOutErpSubmit
    (
    pUSBTGT_RNDIS pUsbTgtRndis
    )
    {
    STATUS             status;
    pUSBTGT_BINDER_BUF pBinderBuf = NULL;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pUsbTgtRndis->pBinder) ||
        (NULL == pUsbTgtRndis->bulkOutPipeHandle))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         (NULL == pUsbTgtRndis->pBinder) ? "pBinder" :
                         "bulkOutPipeHandle" ), 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Take the bulk Out Erp ownership */

    if ((TRUE == pUsbTgtRndis->bBulkOutErpUsed) ||
        (pUsbTgtRndis->uBulkOutPipeStatus == USBTGT_PIPE_STATUS_STALL) ||
        (pUsbTgtRndis->rndisState != RNDIS_DATA_INITIALIZED) ||
        (semTake(pUsbTgtRndis->bulkOutErpMutex, NO_WAIT) != OK))
        {
        USBTGT_RNDIS_WARN("semTake the Bulk Out ownship fails\n",
                          1, 2, 3, 4, 5, 6);

        /* The pipe is still using */

        return ERROR;
        }

    /* Get one free binder buffer to receive data from the USB host */

    pBinderBuf = usbTgtNetMediumBinderBufGet(pUsbTgtRndis->pBinder,
                                             USBTGT_BINDER_BUF_FREE_XMIT);

    if (NULL == pBinderBuf)
        {
        semGive(pUsbTgtRndis->bulkOutErpMutex);

        USBTGT_RNDIS_VDBG("No free xmit buffer\n",
                          1, 2, 3, 4, 5, 6);

        /* No free buffer to used to receive the data from the host */

        usbTgtRndisManagementNotify(pUsbTgtRndis,
                              USBTGT_RNDIS_NOTIFY_REQUIRE_DATA_OUT,
                              NULL);
        
        return ERROR;
        }

    /* Init the Erp */

    memset(&pUsbTgtRndis->bulkOutErp, 0, sizeof (USB_ERP));

    pUsbTgtRndis->bulkOutErp.userPtr = (void *)pUsbTgtRndis;

    pUsbTgtRndis->bulkOutErp.erpLen = sizeof (USB_ERP);
    pUsbTgtRndis->bulkOutErp.userCallback = usbTgtRndisBulkOutErpCallback;
    pUsbTgtRndis->bulkOutErp.targCallback = usbTgtErpCallback;
    pUsbTgtRndis->bulkOutErp.bfrCount = 1;
    pUsbTgtRndis->bulkOutErp.bfrList[0].pid = USB_PID_OUT;
    pUsbTgtRndis->bulkOutErp.bfrList[0].pBfr = (pUINT8)pBinderBuf->pBuf;
    pUsbTgtRndis->bulkOutErp.bfrList[0].bfrLen = (UINT32)pBinderBuf->brfLen;
    pUsbTgtRndis->bulkOutErp.pContext = (void *)pBinderBuf;
    pUsbTgtRndis->bBulkOutErpUsed = TRUE;

    USBTGT_RNDIS_VDBG("usbTgtRndisBulkOutErpSubmit pBinderBuf->brfLen %d\n",
                      pBinderBuf->brfLen, 2, 3, 4, 5, 6);

    /* Submit the Erp */

    status = usbTgtSubmitErp(pUsbTgtRndis->bulkOutPipeHandle,
                             &pUsbTgtRndis->bulkOutErp);

    /* Check the status */

    if (status != OK)
        {
        USBTGT_RNDIS_ERR("Submit the Bulk Out "
                          "Erp with status %d\n",
                          status, 2, 3, 4, 5, 6);

        pUsbTgtRndis->bBulkOutErpUsed = FALSE;

        semGive(pUsbTgtRndis->bulkOutErpMutex);

        if (ERROR == usbTgtNetMediumBinderBufPut(pUsbTgtRndis->pBinder,
                                                 pBinderBuf,
                                                 USBTGT_BINDER_BUF_FREE_XMIT))
            {
            USBTGT_RNDIS_ERR ("Put the binder buffer to freeXmitBufList error \n",
                               1, 2, 3, 4, 5, 6);

            }

        return ERROR;
        }

    semGive(pUsbTgtRndis->bulkOutErpMutex);

    USBTGT_RNDIS_VDBG("usbTgtRndisBulkOutErpSubmit submit the bulk out "
                      "Erp with status %d\n",
                      status, 2, 3, 4, 5, 6);
    return(status);
    }

/******************************************************************************
*
* usbTgtRndisBulkOutErpCallback - be called when the bulk out ERP terminates
*
* This routine is called when the bulk out ERP terminates
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL void usbTgtRndisBulkOutErpCallback
    (
    void * pErpCallback
    )
    {
    pUSB_ERP           pErp = (pUSB_ERP) pErpCallback;
    pUSBTGT_RNDIS      pUsbTgtRndis = NULL;
    RNDIS_PACKET_MSG * pRndisDataMsg = NULL;
    pUSBTGT_BINDER_BUF pBinderBuf = NULL;

    if ((NULL == pErp) ||
        (NULL == pErp->userPtr) ||
        (NULL == pErp->pContext))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pErp) ? "pErp" :
                         (NULL == pErp->userPtr) ? "userPtr" :
                         "pContext"), 2, 3, 4, 5, 6);

        return;
        }

    /*
     * To be note, the pErp->userPtr will be used by the function driver
     * device. such as the rndis dev
     */

    pUsbTgtRndis = (pUSBTGT_RNDIS)pErp->userPtr;

    /* Clear the using flag */

    pUsbTgtRndis->bBulkOutErpUsed = FALSE;

    USBTGT_RNDIS_VDBG ("usbTgtRndisBulkOutErpCallback recv 0x%X bytes data\n",
                       pErp->bfrList[0].actLen, 2, 3, 4, 5, 6);

    /* Get the binder buffer */

    pBinderBuf = (pUSBTGT_BINDER_BUF)pErp->pContext;

    if (pErp->result == S_usbTgtTcdLib_ERP_CANCELED)
        {
        USBTGT_RNDIS_VDBG("usbTgtRndisBulkOutErpCallback canceled\n",
                           1, 2, 3, 4, 5, 6);

        if (ERROR == usbTgtNetMediumBinderBufPut(pUsbTgtRndis->pBinder,
                                                 pBinderBuf,
                                                 USBTGT_BINDER_BUF_FREE_XMIT))
            {
            USBTGT_RNDIS_ERR("Put the binder buffer to freeXmitBufList error\n",
                               1, 2, 3, 4, 5, 6);

            }

        return;
        }

    /* Get the Data message */

    pRndisDataMsg = (RNDIS_PACKET_MSG *)pErp->bfrList[0].pBfr;

    /* Check the data packet validation */

    if ((NULL != pRndisDataMsg) &&
        (REMOTE_NDID_PACKET_MSG == FROM_LITTLEL(pRndisDataMsg->msgType)))
        {
        pBinderBuf->actDataLen = FROM_LITTLEL(pRndisDataMsg->dataLen);

        if (ERROR == usbTgtNetMediumBinderBufPut(pUsbTgtRndis->pBinder,
                                                 pBinderBuf,
                                                 USBTGT_BINDER_BUF_XMIT_TO_MEDIUM))
            {
            USBTGT_RNDIS_ERR ("Put the binder buffer to xmitToMediumBufList error\n",
                               1, 2, 3, 4, 5, 6);


            return;
            }

        usbTgtRndisManagementNotify(pUsbTgtRndis,
                                    USBTGT_RNDIS_NOTIFY_XMIT_DATA_TO_MEDIUM,
                                    NULL);

        }
    else
        {

        /* Put this binder buffer to the xmit free list */

        if (ERROR == usbTgtNetMediumBinderBufPut(pUsbTgtRndis->pBinder,
                                                 pBinderBuf,
                                                 USBTGT_BINDER_BUF_FREE_XMIT))
            {
            USBTGT_RNDIS_ERR ("Put the binder buffer to freeXmitBufList error\n",
                               1, 2, 3, 4, 5, 6);

            }
        }

    /* Submit one Bulk out erp asking for more data */

    usbTgtRndisManagementNotify(pUsbTgtRndis,
                                USBTGT_RNDIS_NOTIFY_REQUIRE_DATA_OUT,
                                NULL);

    return;
    }


/******************************************************************************
*
* usbTgtRndisBulkInErpSubmit - submit the bulk in ERP
*
* This routine submits the bulk in ERP to transfer the data to
* the host by the bulk in data channel. The bulk in ERP will be initialized
* in the frist step.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO:N/A
*
* \NOMANUAL
*/

STATUS usbTgtRndisBulkInErpSubmit
    (
    pUSBTGT_RNDIS    pUsbTgtRndis
    )
    {
    STATUS             status;
    pUSBTGT_BINDER_BUF pBinderBuf = NULL;
    pRNDIS_PACKET_MSG  pRndisDataMsg = NULL;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pUsbTgtRndis->bulkInPipeHandle))
        {
        USBTGT_RNDIS_DBG("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         "bulkInPipeHandle"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    pBinderBuf = usbTgtNetMediumBinderBufGet(pUsbTgtRndis->pBinder,
                                             USBTGT_BINDER_BUF_RCV_FROM_MEDIUM);

    if (NULL == pBinderBuf)
        {
        USBTGT_RNDIS_DBG("usbTgtRndisBulkInErpSubmit pBinderBuf is NULL\n",
                         1, 2, 3, 4, 5, 6);

        return OK;
        }

    if ((pUsbTgtRndis->bBulkInErpUsed == TRUE) ||
        (pUsbTgtRndis->uBulkInPipeStatus == USBTGT_PIPE_STATUS_STALL) ||
        (pUsbTgtRndis->rndisState != RNDIS_DATA_INITIALIZED) ||
        (semTake(pUsbTgtRndis->bulkInErpMutex, NO_WAIT) != OK))
        {
        USBTGT_RNDIS_VDBG("semTake the Bulk In ownship fails\n",
                          1, 2, 3, 4, 5, 6);

        /* Release the buffer */

        if (ERROR == usbTgtNetMediumBinderBufPut(pUsbTgtRndis->pBinder,
                                                 pBinderBuf,
                                                 USBTGT_BINDER_BUF_FREE_RCV))
            {
            USBTGT_RNDIS_WARN("Put the binder buffer to freeRcvBufList error\n",
                              1, 2, 3, 4, 5, 6);

            }
        return ERROR;
        }

    pRndisDataMsg = (pRNDIS_PACKET_MSG)pBinderBuf->pBuf;
    pRndisDataMsg->msgType = FROM_LITTLEL(REMOTE_NDID_PACKET_MSG);
    pRndisDataMsg->dataLen = FROM_LITTLEL(pBinderBuf->actDataLen);
    pRndisDataMsg->dataOffset = FROM_LITTLEL(sizeof (RNDIS_PACKET_MSG) -
                                OFFSET(RNDIS_PACKET_MSG, dataOffset));

    pRndisDataMsg->msgLen = FROM_LITTLEL(pBinderBuf->actDataLen +
                                         pBinderBuf->headerLen);

    /* Init the Erp */

    memset(&pUsbTgtRndis->bulkInErp, 0, sizeof (USB_ERP));

    pUsbTgtRndis->bulkInErp.userPtr = (void *)pUsbTgtRndis;

    pUsbTgtRndis->bulkInErp.erpLen = sizeof (USB_ERP);
    pUsbTgtRndis->bulkInErp.userCallback= usbTgtRndisBulkInErpCallback;
    pUsbTgtRndis->bulkInErp.targCallback = usbTgtErpCallback;
    pUsbTgtRndis->bulkInErp.bfrCount = 1;
    pUsbTgtRndis->bulkInErp.bfrList[0].pid = USB_PID_IN;
    pUsbTgtRndis->bulkInErp.bfrList[0].pBfr = (pUINT8)pBinderBuf->pBuf;
    pUsbTgtRndis->bulkInErp.bfrList[0].bfrLen = FROM_LITTLEL(pRndisDataMsg->msgLen);
    pUsbTgtRndis->bulkInErp.pContext = (void *)pBinderBuf;

    USBTGT_RNDIS_VDBG("usbTgtRndisBulkInErpSubmit pBinderBuf->brfLen %d\n",
                      FROM_LITTLEL(pRndisDataMsg->msgLen), 2, 3, 4, 5, 6);

    pUsbTgtRndis->bBulkInErpUsed = TRUE;

    /* Submit the Erp */

    status = usbTgtSubmitErp(pUsbTgtRndis->bulkInPipeHandle,
                             &pUsbTgtRndis->bulkInErp);

    /* Check the status */

    if (status != OK)
        {
        USBTGT_RNDIS_ERR("Submit the Bulk IN Erp with status %d\n",
                          status, 2, 3, 4, 5, 6);

        pUsbTgtRndis->bBulkInErpUsed = FALSE;

        semGive(pUsbTgtRndis->bulkInErpMutex);

        if (ERROR == usbTgtNetMediumBinderBufPut(pUsbTgtRndis->pBinder,
                                                 pBinderBuf,
                                                 USBTGT_BINDER_BUF_FREE_RCV))
            {
            USBTGT_RNDIS_WARN("Put the binder buffer to freeRcvBufList error\n",
                              1, 2, 3, 4, 5, 6);
            }
        USBTGT_RNDIS_ERR("usbTgtSubmitErp error\n",
                         1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    semGive(pUsbTgtRndis->bulkInErpMutex);

    USBTGT_RNDIS_VDBG("usbTgtRndisBulkInErpSubmit submit the bulk in "
                      "Erp with status %d\n",
                      status, 2, 3, 4, 5, 6);

    return(status);
    }

/******************************************************************************
*
* usbTgtRndisBulkInErpCallback - be called when the bulk in ERP terminates
*
* This routine is called when the bulk in ERP terminates
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL void usbTgtRndisBulkInErpCallback
    (
    void * pErpCallback
    )
    {
    pUSB_ERP           pErp = (pUSB_ERP) pErpCallback;
    pUSBTGT_RNDIS      pUsbTgtRndis = NULL;
    pUSBTGT_BINDER_BUF pBinderBuf = NULL;

    if ((NULL == pErp) ||
        (NULL == pErp->userPtr) ||
        (NULL == pErp->pContext))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pErp) ? "pErp" :
                         (NULL == pErp->userPtr) ? "userPtr" :
                         "pContext"), 2, 3, 4, 5, 6);
        return;
        }

    USBTGT_RNDIS_VDBG("usbTgtRndisBulkInErpCallback\n",
                       1, 2, 3, 4, 5, 6);

    pUsbTgtRndis = (pUSBTGT_RNDIS)pErp->userPtr;
    pBinderBuf = (pUSBTGT_BINDER_BUF)pErp->pContext;

    if (ERROR == usbTgtNetMediumBinderBufPut(pUsbTgtRndis->pBinder,
                                             pBinderBuf,
                                             USBTGT_BINDER_BUF_FREE_RCV))
        {
        USBTGT_RNDIS_WARN("Put the binder buffer to freeRcvBufList error\n",
                           1, 2, 3, 4, 5, 6);

        }

    pUsbTgtRndis->bBulkInErpUsed = FALSE;

    if (pErp->result == S_usbTgtTcdLib_ERP_CANCELED)
        {
        /* The Erp has been canceled */

        USBTGT_RNDIS_WARN ("Bulk In transaction canceled\n",
                           1, 2, 3, 4, 5, 6);

        return;
        }

    usbTgtRndisManagementNotify(pUsbTgtRndis,
                                USBTGT_RNDIS_NOTIFY_RCV_DATA_FROM_MEDIUM,
                                NULL);

    return;
    }

/******************************************************************************
*
* usbTgtRndisOidGenSupportedList - handle supported list query message request
*
* This routine handles the query message request for the
* supported list OID.
*
* As a query, the OID_GEN_SUPPORTED_LIST OID specifies an array of OIDs for
* objects that the miniport driver or a NIC supports. Objects include general,
* media-specific, and implementation-specific objects.
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisOidGenSupportedList
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_QUERY_MSG *   pQueryMsg,
    RNDIS_QUERY_CMPLT * pQueryCmplt
    )
    {
    UINT32 * pVal;
    int      i = 0;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pQueryCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         "pQueryCmplt" ),
                          2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * As a query, the OID_GEN_SUPPORTED_LIST OID specifies an array of
     * OIDs for objects that the miniport driver or a NIC supports. Objects
     * include general,media-specific, and implementation-specific objects.
     */

    pVal = (UINT32 *) (((UINT8 *) pQueryCmplt) + sizeof(RNDIS_QUERY_CMPLT));

    /* General operational OIDs*/

    pVal[i++] = TO_LITTLEL(OID_GEN_SUPPORTED_LIST);
    pVal[i++] = TO_LITTLEL(OID_GEN_HARDWARE_STATUS);
    pVal[i++] = TO_LITTLEL(OID_GEN_MEDIA_SUPPORTED);
    pVal[i++] = TO_LITTLEL(OID_GEN_MEDIA_IN_USE);
    pVal[i++] = TO_LITTLEL(OID_GEN_MAXIMUM_FRAME_SIZE);
    pVal[i++] = TO_LITTLEL(OID_GEN_LINK_SPEED);
    pVal[i++] = TO_LITTLEL(OID_GEN_TRANSMIT_BLOCK_SIZE);
    pVal[i++] = TO_LITTLEL(OID_GEN_RECEIVE_BLOCK_SIZE);
    pVal[i++] = TO_LITTLEL(OID_GEN_VENDOR_ID);
    pVal[i++] = TO_LITTLEL(OID_GEN_VENDOR_DESCRIPTION);
    pVal[i++] = TO_LITTLEL(OID_GEN_CURRENT_PACKET_FILTER);
    pVal[i++] = TO_LITTLEL(OID_GEN_MAXIMUM_TOTAL_SIZE);
    pVal[i++] = TO_LITTLEL(OID_GEN_MAC_OPTIONS);
    pVal[i++] = TO_LITTLEL(OID_GEN_MEDIA_CONNECT_STATUS);

    /* General statistic OIDs */

    pVal[i++] = TO_LITTLEL(OID_GEN_XMIT_OK);
    pVal[i++] = TO_LITTLEL(OID_GEN_RCV_OK);
    pVal[i++] = TO_LITTLEL(OID_GEN_XMIT_ERROR);
    pVal[i++] = TO_LITTLEL(OID_GEN_RCV_ERROR);

    /* 802.3 OIDs */

    pVal[i++] = TO_LITTLEL(OID_802_3_PERMANENT_ADDRESS);
    pVal[i++] = TO_LITTLEL(OID_802_3_CURRENT_ADDRESS);
    pVal[i++] = TO_LITTLEL(OID_802_3_MULTICAST_LIST);
    pVal[i++] = TO_LITTLEL(OID_802_3_MAXIMUM_LIST_SIZE);
    pVal[i++] = TO_LITTLEL(OID_802_3_RCV_ERROR_ALIGNMENT);
    pVal[i++] = TO_LITTLEL(OID_802_3_XMIT_ONE_COLLISION);
    pVal[i++] = TO_LITTLEL(OID_802_3_XMIT_MORE_COLLISIONS);

    /* Power management OIDs */

    if (usrUsbTgtRndisPMEnableGet(0))
        {
        pVal[i++] = TO_LITTLEL(OID_PNP_CAPABILITIES);
        pVal[i++] = TO_LITTLEL(OID_PNP_SET_POWER);
        pVal[i++] = TO_LITTLEL(OID_PNP_QUERY_POWER);
        pVal[i++] = TO_LITTLEL(OID_PNP_ADD_WAKE_UP_PATTERN);
        pVal[i++] = TO_LITTLEL(OID_PNP_REMOVE_WAKE_UP_PATTERN);
        pVal[i++] = TO_LITTLEL(OID_PNP_ENABLE_WAKE_UP);
        }

    pQueryCmplt->infoBufLen = i * 4;

    USBTGT_RNDIS_DBG("usbTgtRndisOidGenSupportedList Oid\n",
                     1, 2, 3, 4, 5, 6);

    return OK;
    }

/******************************************************************************
*
* usbTgtRndisOidGenHardwareStatus - handle hardware status query message request
*
* This routine handles the query message request for the
* hardware status OID.
*
* As a query, the OID_GEN_HARDWARE_STATUS OID specifies the current
* hardware status of the underlying NIC.
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisOidGenHardwareStatus
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_QUERY_MSG *   pQueryMsg,
    RNDIS_QUERY_CMPLT * pQueryCmplt
    )
    {
    UINT32 * pVal;
    STATUS   status = ERROR;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pQueryCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         "pQueryCmplt" ),
                          2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * As a query, the OID_GEN_HARDWARE_STATUS OID specifies the current
     * hardware status of the underlying NIC.
     */

    pVal = (UINT32 *) (((UINT8 *) pQueryCmplt) + sizeof(RNDIS_QUERY_CMPLT));

    /* Always ready */

    * pVal = TO_LITTLEL((UINT32)RNDIS_HARDWARE_STATUS_READY);

    pQueryCmplt->infoBufLen = 4;

    USBTGT_RNDIS_DBG("usbTgtRndisOidGenHardwareStatus 0x%X\n",
                     * pVal, 2, 3, 4, 5, 6);


    return status;
    }

/******************************************************************************
*
* usbTgtRndisOidGenMediaSupported - handle meidia supported query message request
*
* This routine handles the query message request for the
* media support OID.
*
* As a query, the OID_GEN_MEDIA_SUPPORTED OID specifies the media types that
* a NIC can support but not necessarily the media types that the NIC
* currently uses.
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisOidGenMediaSupported
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_QUERY_MSG *   pQueryMsg,
    RNDIS_QUERY_CMPLT * pQueryCmplt
    )
    {
    UINT32 * pVal;
    STATUS   status = ERROR;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pQueryCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         "pQueryCmplt" ),
                          2, 3, 4, 5, 6);

        return ERROR;
        }
    /*
     * As a query, the OID_GEN_MEDIA_SUPPORTED OID specifies the media types
     * that a NIC can support but not necessarily the media types that
     * the NIC currently uses.
     */

    pVal = (UINT32 *) (((UINT8 *) pQueryCmplt) + sizeof(RNDIS_QUERY_CMPLT));

    if ((USBTGT_BINDER_MEDIUM_VIRTUAL_END == pUsbTgtRndis->uMediumType) ||
        (USBTGT_BINDER_MEDIUM_END_BRIGGE == pUsbTgtRndis->uMediumType))
        {
        * ((UINT32 *)pVal) = TO_LITTLEL((UINT32)RNDIS_MEDIUM_802_3);

        }

    USBTGT_RNDIS_DBG("usbTgtRndisOidGenMediaSupported 0x%X\n",
                    * pVal, 2, 3, 4, 5, 6);

    pQueryCmplt->infoBufLen = 4;

    return status;
    }

/******************************************************************************
*
* usbTgtRndisOidGenMediaInUse - handle meidia in use query message request
*
* This routine handles the query message request for the
* media in use OID.
*
* As a query, the OID_GEN_MEDIA_IN_USE OID specifies a complete list
* of the media types that the NIC currently uses.
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisOidGenMediaInUse
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_QUERY_MSG *   pQueryMsg,
    RNDIS_QUERY_CMPLT * pQueryCmplt
    )
    {
    UINT32 * pVal;
    STATUS   status = ERROR;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pQueryCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         "pQueryCmplt" ),
                          2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * As a query, the OID_GEN_MEDIA_IN_USE OID specifies a complete list
     * of the media types that the NIC currently uses.
     */

    pVal = (UINT32 *) (((UINT8 *) pQueryCmplt) + sizeof(RNDIS_QUERY_CMPLT));

    if ((USBTGT_BINDER_MEDIUM_VIRTUAL_END== pUsbTgtRndis->uMediumType) ||
        (USBTGT_BINDER_MEDIUM_END_BRIGGE == pUsbTgtRndis->uMediumType))
        {
        * ((UINT32 *)pVal) = TO_LITTLEL((UINT32)RNDIS_MEDIUM_802_3);

        }

    USBTGT_RNDIS_DBG("usbTgtRndisOidGenMediaInUse 0x%X\n",
                    * pVal, 2, 3, 4, 5, 6);

    pQueryCmplt->infoBufLen = 4;

    return status;
    }

/******************************************************************************
*
* usbTgtRndisOidGenMaximumFrameSize - handle max frame size query message request
*
* This routine handles the query message request for the
* max frame size OID.
*
* As a query, the OID_GEN_MAXIMUM_FRAME_SIZE OID specifies the maximum
* network packet size, in bytes, that the NIC supports. This
* specification does not include a header.
*
* In response to this query from requesting transports, the NIC driver should
* indicate the maximum frame size that the transports can send, excluding
* the header. A NIC driver that emulates another medium type for binding to
* a transport must ensure that the maximum frame size for a protocol-supplied
* net packet does not exceed the size limitations for the true network medium.
* The same is true for a NIC driver that supports a NIC that requires
* inserting fields in frames. For example, to determine the maximum transfer
* unit (MTU), transports send this query to a NIC.
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtRndisOidGenMaximumFrameSize
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_QUERY_MSG *   pQueryMsg,
    RNDIS_QUERY_CMPLT * pQueryCmplt
    )
    {
    UINT32 * pVal;
    STATUS   status = ERROR;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pUsbTgtRndis->pBinder) ||
        (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ||
        (NULL == pQueryCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         (NULL == pUsbTgtRndis->pBinder) ? "pBinder" :
                         (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ?
                         "pBinder->mediumIoctl" : "pQueryCmplt"),
                          2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * As a query, the OID_GEN_MAXIMUM_FRAME_SIZE OID specifies the maximum
     * network packet size, in bytes, that the NIC supports. This
     * specification does not include a header.
     */

    pVal = (UINT32 *) (((UINT8 *) pQueryCmplt) + sizeof(RNDIS_QUERY_CMPLT));

    /* Normally return the MTU */

    status = (pUsbTgtRndis->pBinder->mediumIoctl)
                           (pUsbTgtRndis->pBinder->pMediumDev,
                           USBTGT_NET_IOCTL_END_GET_MTU,
                           (void *)pVal);

    pQueryCmplt->infoBufLen = 4;

    USBTGT_RNDIS_DBG("usbTgtRndisOidGenMaximumFrameSize %d\n",
                       * pVal, 2, 3, 4, 5, 6);

    return status;
    }


/******************************************************************************
*
* usbTgtRndisOidGenLinkSpeed - handle max speed query message request
*
* This routine handles the query message request for the
* max speed OID.
*
* As a query, the OID_GEN_LINK_SPEED OID specifies the maximum
* speed of the NIC in kbps.
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtRndisOidGenLinkSpeed
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_QUERY_MSG *   pQueryMsg,
    RNDIS_QUERY_CMPLT * pQueryCmplt
    )
    {
    UINT32 * pVal;
    UINT32   uSpeed = 0;
    STATUS   status = ERROR;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pUsbTgtRndis->pBinder) ||
        (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ||
        (NULL == pQueryCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         (NULL == pUsbTgtRndis->pBinder) ? "pBinder" :
                         (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ?
                         "pBinder->mediumIoctl" : "pQueryCmplt"),
                          2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * As a query, the OID_GEN_LINK_SPEED OID specifies the maximum
     * speed of the NIC in kbps.
     */

    pVal = (UINT32 *) (((UINT8 *) pQueryCmplt) + sizeof(RNDIS_QUERY_CMPLT));

    status = (pUsbTgtRndis->pBinder->mediumIoctl)
                           (pUsbTgtRndis->pBinder->pMediumDev,
                           USBTGT_NET_IOCTL_END_GET_LINK_SPEED,
                           (void *)&uSpeed);

    /* Translate to the RNDIS type */

    * pVal = uSpeed / 100;

    USBTGT_RNDIS_VDBG("usbTgtRndisOidGenLinkSpeed %d\n",
                      * pVal, 2, 3, 4, 5, 6);

    pQueryCmplt->infoBufLen = 4;

    return status;
    }


/******************************************************************************
*
* usbTgtRndisOidGenTransmitBlockSize - handle TX block size query message request
*
* This routine handles the query message request for the
* transfer block size OID.
*
* As a query, the OID_GEN_TRANSMIT_BLOCK_SIZE OID specifies the
* minimum number of bytes that a single net packet occupies in the
* transmit buffer space of the NIC.
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisOidGenTransmitBlockSize
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_QUERY_MSG *   pQueryMsg,
    RNDIS_QUERY_CMPLT * pQueryCmplt
    )
    {
    UINT32 * pVal;
    STATUS   status = ERROR;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pUsbTgtRndis->pBinder) ||
        (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ||
        (NULL == pQueryCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         (NULL == pUsbTgtRndis->pBinder) ? "pBinder" :
                         (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ?
                         "pBinder->mediumIoctl" : "pQueryCmplt"),
                          2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * As a query, the OID_GEN_TRANSMIT_BLOCK_SIZE OID specifies the
     * minimum number of bytes that a single net packet occupies in the
     * transmit buffer space of the NIC.
     */

    pVal = (UINT32 *) (((UINT8 *) pQueryCmplt) + sizeof(RNDIS_QUERY_CMPLT));

    status = (pUsbTgtRndis->pBinder->mediumIoctl)
                           (pUsbTgtRndis->pBinder->pMediumDev,
                           USBTGT_NET_IOCTL_END_GET_MTU,
                           (void *)pVal);

    pQueryCmplt->infoBufLen = 4;

    USBTGT_RNDIS_DBG ("usbTgtRndisOidGenTransmitBlockSize %d\n",
                       * pVal, 2, 3, 4, 5, 6);


    return status;
    }

/******************************************************************************
*
* usbTgtRndisOidGenReceiveBlockSize - handle RX block size query message request
*
* This routine handles the query message request for the
* receive block size  OID.
*
* As a query. the OID_GEN_RECEIVE_BLOCK_SIZE OID specifies the amount
* of storage, in bytes, that a single packet occupies in the receive
* buffer space of the NIC.
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtRndisOidGenReceiveBlockSize
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_QUERY_MSG *   pQueryMsg,
    RNDIS_QUERY_CMPLT * pQueryCmplt
    )
    {
    UINT32 * pVal;
    STATUS   status = ERROR;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pUsbTgtRndis->pBinder) ||
        (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ||
        (NULL == pQueryCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         (NULL == pUsbTgtRndis->pBinder) ? "pBinder" :
                         (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ?
                         "pBinder->mediumIoctl" : "pQueryCmplt"),
                          2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * As a query. the OID_GEN_RECEIVE_BLOCK_SIZE OID specifies the amount
     * of storage, in bytes, that a single packet occupies in the receive
     * buffer space of the NIC.
     */

    pVal = (UINT32 *) (((UINT8 *) pQueryCmplt) + sizeof(RNDIS_QUERY_CMPLT));

    status = (pUsbTgtRndis->pBinder->mediumIoctl)
                           (pUsbTgtRndis->pBinder->pMediumDev,
                           USBTGT_NET_IOCTL_END_GET_MTU,
                           (void *)pVal);

    pQueryCmplt->infoBufLen = 4;

    USBTGT_RNDIS_DBG ("usbTgtRndisOidGenReceiveBlockSize %d\n",
                       * pVal, 2, 3, 4, 5, 6);


    return status;
    }

/******************************************************************************
*
* usbTgtRndisOidGenVendorId - handle vendor ID query message request
*
* This routine handles the query message request for the
* NIC vendor ID OID.
*
* As a query, the OID_GEN_VENDOR_ID OID specifies a three-byte
* IEEE-registered vendor code, followed by a single byte that
* the vendor assigns to identify a particular NIC.
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtRndisOidGenVendorId
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_QUERY_MSG *   pQueryMsg,
    RNDIS_QUERY_CMPLT * pQueryCmplt
    )
    {
    UINT32 * pVal;
    UINT16   uVonderId;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pQueryCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                          "pQueryCmplt"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * As a query, the OID_GEN_VENDOR_ID OID specifies a three-byte
     * IEEE-registered vendor code, followed by a single byte that
     * the vendor assigns to identify a particular NIC.
     */

    pVal = (UINT32 *) (((UINT8 *) pQueryCmplt) + sizeof(RNDIS_QUERY_CMPLT));


    if (OK == usbTgtFuncInfoMemberGet(pUsbTgtRndis->targChannel,
                                      USBTGT_FUNC_INFO_MEM_TYPE_uVendorID,  
                                      &uVonderId))
        {    
        pVal[0] = TO_LITTLEL(uVonderId);
        }
    else
        {
        pVal[0] = 0;
        }

    /* Get the Vendor ID from the media TODO */

    pQueryCmplt->infoBufLen = 4;

    USBTGT_RNDIS_DBG ("usbTgtRndisOidGenVendorId 0x%X\n",
                       * pVal, 2, 3, 4, 5, 6);

    return OK;
    }


/******************************************************************************
*
* usbTgtRndisOidGenVendorDescription - handle vendor description query message request
*
* This routine handles the query message request for the
* NIC vendor descriptor OID.
*
* As a query, the OID_GEN_VENDOR_DESCRIPTION OID points to
* a null-terminated string describing the NIC.
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtRndisOidGenVendorDescription
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_QUERY_MSG *   pQueryMsg,
    RNDIS_QUERY_CMPLT * pQueryCmplt
    )
    {
    char * pVal;
    char *  pMfgStr = NULL;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pQueryCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         "pQueryCmplt"), 2, 3, 4, 5, 6);

        return ERROR;
        }
    /*
     * As a query, the OID_GEN_VENDOR_DESCRIPTION OID points to
     * a null-terminated string describing the NIC.
     */

    pVal = (char *) (((UINT8 *) pQueryCmplt) + sizeof(RNDIS_QUERY_CMPLT));

    if (OK == usbTgtFuncInfoMemberGet(pUsbTgtRndis->targChannel, 
                                       USBTGT_FUNC_INFO_MEM_TYPE_pMfgString,
                                       &pMfgStr))
        {    
        /* Get the Vendor string from the media */

        if (NULL != pMfgStr)
            {
            pQueryCmplt->infoBufLen = (UINT32) (min (strlen(pMfgStr) + 1, 
                         RNDIS_CTRL_MSG_BUF_SIZE - sizeof(RNDIS_QUERY_CMPLT)));
            strncpy(pVal, pMfgStr, pQueryCmplt->infoBufLen);
            }
        else
            {
            pQueryCmplt->infoBufLen = 0;
            }
        }
    else
        {
        pQueryCmplt->infoBufLen = 0;
        }

    USBTGT_RNDIS_VDBG ("usbTgtRndisOidGenVendorDescription length %d\n",
                       pQueryCmplt->infoBufLen, 2, 3, 4, 5, 6);

    return OK;
    }

/******************************************************************************
*
* usbTgtRndisOidGenCurrentPacketFilterQuery - query current packet filter
*
* This routine handles the query message request for the
* types of the net packets OID.
*
* As a query, the OID_GEN_CURRENT_PACKET_FILTER OID reports the
* types of net packets that are in receive indications from a
* miniport driver.
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisOidGenCurrentPacketFilterQuery
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_QUERY_MSG *   pQueryMsg,
    RNDIS_QUERY_CMPLT * pQueryCmplt
    )
    {
    UINT32 * pVal;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pQueryMsg)    ||
        (NULL == pQueryCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         (NULL == pQueryMsg) ? "pQueryMsg" : "pQueryCmplt" ),
                          2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * As a query, the OID_GEN_CURRENT_PACKET_FILTER OID reports the
     * types of net packets that are in receive indications from a
     * miniport driver.
     */

    pVal = (UINT32 *) (((UINT8 *) pQueryCmplt) + sizeof(RNDIS_QUERY_CMPLT));

    pVal[0] = TO_LITTLEL(pUsbTgtRndis->rndisFilterValue);

    USBTGT_RNDIS_DBG("Query OID_GEN_CURRENT_PACKET_FILTER with value 0x%08x\n",
                      pUsbTgtRndis->rndisFilterValue, 2, 3, 4, 5, 6);

    pQueryCmplt->infoBufLen = 4;

    return OK;
    }


/******************************************************************************
*
* usbTgtRndisOidGenCurrentPacketFilterSet - set current packet filter
*
* This routine handles the query message request for the
* types of the net packets OID.
*
* As a set, the OID_GEN_CURRENT_PACKET_FILTER OID specifies the types
* of net packets for which a protocol receives indications from a
* miniport driver.
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisOidGenCurrentPacketFilterSet
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_SET_MSG *     pSetMsg,
    RNDIS_SET_CMPLT *   pSetCmplt
    )
    {
    UINT32 * pVal;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pSetMsg)    ||
        (NULL == pSetCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         (NULL == pSetMsg) ? "pSetMsg" : "pSetCmplt" ),
                          2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * As a set, the OID_GEN_CURRENT_PACKET_FILTER OID specifies the types
     * of net packets for which a protocol receives indications from a
     * miniport driver.
     */

    pVal = (UINT32 *)(((UINT8 *) &pSetMsg->requestId) +
                       FROM_LITTLEL(pSetMsg->infoBufOffset));

    pUsbTgtRndis->rndisFilterValue = FROM_LITTLEL(pVal[0]);

    USBTGT_RNDIS_DBG("Set OID_GEN_CURRENT_PACKET_FILTER with value 0x%08x\n",
                      pUsbTgtRndis->rndisFilterValue, 2, 3, 4, 5, 6);

    if (pUsbTgtRndis->rndisFilterValue != 0)
        {
        pUsbTgtRndis->rndisState = RNDIS_DATA_INITIALIZED;

        /* The RNDIS is RNDIS Data initalized state , Ready the Data channels */

        usbTgtRndisManagementNotify(pUsbTgtRndis,
                                    USBTGT_RNDIS_NOTIFY_DATA_CHANNEL_RESET,
                                    NULL);

        }
    else
        {
        pUsbTgtRndis->rndisState = RNDIS_INITIALIZED;
        }

    pSetCmplt->status = TO_LITTLEL(RNDIS_STATUS_SUCCESS);

    return OK;
    }

/******************************************************************************
*
* usbTgtRndisOidGenMaximumTotalSize - query max total size
*
* This routine handles the query message request for the
* total size the NIC supported OID.
*
* As a query, the OID_GEN_MAXIMUM_TOTAL_SIZE OID specifies the maximum
* total packet length, in bytes, the NIC supports.
* This specification includes the header.
*
* The returned length specifies the largest packet size for the underlying
* medium. Thus, the returned length depends on the particular medium.
* A protocol driver might use this returned length as a gauge to determine
* the maximum size packet that a NIC driver could forward to the protocol
* driver. If the protocol driver pre-allocates buffers, it allocates
* buffers accordingly. The returned length also specifies the largest packet
* a protocol driver can pass to NdisSend or NdisSendPackets.
*
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisOidGenMaximumTotalSize
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_QUERY_MSG *   pQueryMsg,
    RNDIS_QUERY_CMPLT * pQueryCmplt
    )
    {
    UINT32 * pVal;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pQueryCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         "pQueryCmplt" ),
                          2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * As a query, the OID_GEN_MAXIMUM_TOTAL_SIZE OID specifies the maximum
     * total packet length, in bytes, the NIC supports.
     * This specification includes the header.
     */

    pVal = (UINT32 *) (((UINT8 *) pQueryCmplt) + sizeof(RNDIS_QUERY_CMPLT));

    /* Normally return the total packet length */

    if ((pUsbTgtRndis->pBinder) &&
        (pUsbTgtRndis->pBinder->binderBufSize))
        {
        pVal[0] = TO_LITTLEL(pUsbTgtRndis->pBinder->binderBufSize);
        }
    else
        {
        pVal[0] = 0;
        }

    pQueryCmplt->infoBufLen = 4;

    USBTGT_RNDIS_DBG ("usbTgtRndisOidGenMaximumTotalSize %d\n",
                       * pVal, 2, 3, 4, 5, 6);

    return OK;
    }

/******************************************************************************
*
* usbTgtRndisOidGenMediaConnectStatus - query medium connect status
*
* This routine handles the query message request for the
* media connection status OID.
*
* As a query, the OID_GEN_MEDIA_CONNECT_STATUS OID requests
* the connection status of the NIC on the network.
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisOidGenMediaConnectStatus
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_QUERY_MSG *   pQueryMsg,
    RNDIS_QUERY_CMPLT * pQueryCmplt
    )
    {
    UINT32 * pVal;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pQueryCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         "pQueryCmplt" ),
                          2, 3, 4, 5, 6);

        return ERROR;
        }


    /*
     * As a query, the OID_GEN_MEDIA_CONNECT_STATUS OID requests
     * the connection status of the NIC on the network.
     */

    pVal = (UINT32 *) (((UINT8 *) pQueryCmplt) + sizeof(RNDIS_QUERY_CMPLT));

    pVal[0] = TO_LITTLEL(RNDIS_MEDIA_STATE_CONNECTED);

    USBTGT_RNDIS_DBG ("usbTgtRndisOidGenMediaConnectStatus 0x%X\n",
                       *(UINT32 *)pVal, 2, 3, 4, 5, 6);

    pQueryCmplt->infoBufLen = 4;

    return OK;
    }



/******************************************************************************
*
* usbTgtRndisOidGenXmitOk - query Tx OK packet count
*
* This routine handles the query message request for the
* OID_GEN_XMIT_OK OID to get the Tx OK packet count.
*
* As a query, the OID_GEN_XMIT_OK OID specifies the number of
* frames that are transmitted without errors.
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisOidGenXmitOk
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_QUERY_MSG *   pQueryMsg,
    RNDIS_QUERY_CMPLT * pQueryCmplt
    )
    {
    UINT32 * pVal;
    STATUS   status = ERROR;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pUsbTgtRndis->pBinder) ||
        (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ||
        (NULL == pQueryCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         (NULL == pUsbTgtRndis->pBinder) ? "pBinder" :
                         (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ?
                         "pBinder->mediumIoctl" : "pQueryCmplt"),
                          2, 3, 4, 5, 6);

        return ERROR;
        }
    /*
     * As a query, the OID_GEN_XMIT_OK OID specifies the number of
     * frames that are transmitted without errors.
     */

    pVal = (UINT32 *) (((UINT8 *) pQueryCmplt) + sizeof(RNDIS_QUERY_CMPLT));

    status = (pUsbTgtRndis->pBinder->mediumIoctl)
                           (pUsbTgtRndis->pBinder->pMediumDev,
                           USBTGT_NET_IOCTL_END_GET_XMIT_OK,
                           (void *)pVal);

    USBTGT_RNDIS_VDBG ("usbTgtRndisOidGenXmitOk 0x%X\n",
                       *(UINT32 *)pVal, 2, 3, 4, 5, 6);

    pQueryCmplt->infoBufLen = 4;

    return status;
    }

/******************************************************************************
*
* usbTgtRndisOidGenRcvOk - query Rx OK packet count
*
* This routine handles the query message request for the
* OID_GEN_RCV_OK OID to get the Rx OK packet count.
*
* As a query, the OID_GEN_RCV_OK OID specifies the number of frames
* that the NIC receives without errors and indicates to bound protocols.
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisOidGenRcvOk
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_QUERY_MSG *   pQueryMsg,
    RNDIS_QUERY_CMPLT * pQueryCmplt
    )
    {
    UINT32 * pVal;
    STATUS   status = ERROR;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pUsbTgtRndis->pBinder) ||
        (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ||
        (NULL == pQueryCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         (NULL == pUsbTgtRndis->pBinder) ? "pBinder" :
                         (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ?
                         "pBinder->mediumIoctl" : "pQueryCmplt"),
                          2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * As a query, the OID_GEN_RCV_OK OID specifies the number of frames
     * that the NIC receives without errors and indicates to bound protocols.
     */

    pVal = (UINT32 *) (((UINT8 *) pQueryCmplt) + sizeof(RNDIS_QUERY_CMPLT));

    status = (pUsbTgtRndis->pBinder->mediumIoctl)
                           (pUsbTgtRndis->pBinder->pMediumDev,
                           USBTGT_NET_IOCTL_END_GET_RCV_OK,
                           (void *)pVal);

    USBTGT_RNDIS_VDBG ("usbTgtRndisOidGenRcvOk 0x%X\n",
                       *(UINT32 *)pVal, 2, 3, 4, 5, 6);

    pQueryCmplt->infoBufLen = 4;

    return status;
    }

/******************************************************************************
*
* usbTgtRndisOidGenXmitError - query Tx ERROR packet count
*
* This routine handles the query message request for the
* OID_GEN_XMIT_ERROR OID to get the Tx ERROR packet count.
*
* As a query, the OID_GEN_XMIT_ERROR OID specifies the number
* of frames that a NIC fails to transmit.
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisOidGenXmitError
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_QUERY_MSG *   pQueryMsg,
    RNDIS_QUERY_CMPLT * pQueryCmplt
    )
    {
    UINT32 * pVal;
    STATUS   status = ERROR;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pUsbTgtRndis->pBinder) ||
        (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ||
        (NULL == pQueryCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         (NULL == pUsbTgtRndis->pBinder) ? "pBinder" :
                         (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ?
                         "pBinder->mediumIoctl" : "pQueryCmplt"),
                          2, 3, 4, 5, 6);

        return ERROR;
        }
    /*
     * As a query, the OID_GEN_XMIT_ERROR OID specifies the number
     * of frames that a NIC fails to transmit.
     */
    pVal = (UINT32 *) (((UINT8 *) pQueryCmplt) + sizeof(RNDIS_QUERY_CMPLT));

    status = (pUsbTgtRndis->pBinder->mediumIoctl)
                           (pUsbTgtRndis->pBinder->pMediumDev,
                           USBTGT_NET_IOCTL_END_GET_XMIT_ERROR,
                           (void *)pVal);

    USBTGT_RNDIS_VDBG ("usbTgtRndisOidGenXmitError 0x%X\n",
                       *(UINT32 *)pVal, 2, 3, 4, 5, 6);

    pQueryCmplt->infoBufLen = 4;

    return status;
    }

/******************************************************************************
*
* usbTgtRndisOidGenRcvError - query Rx ERROR packet count
*
* This routine handles the query message request for the
* OID_GEN_RCV_ERROR OID to get Rx ERROR packet count.
*
* As a query, the OID_GEN_RCV_ERROR OID specifies the number of
* frames that a NIC receives but does not indicate to the protocols
* due to errors.
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisOidGenRcvError
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_QUERY_MSG *   pQueryMsg,
    RNDIS_QUERY_CMPLT * pQueryCmplt
    )
    {
    UINT32 * pVal;
    STATUS   status = ERROR;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pUsbTgtRndis->pBinder) ||
        (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ||
        (NULL == pQueryCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         (NULL == pUsbTgtRndis->pBinder) ? "pBinder" :
                         (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ?
                         "pBinder->mediumIoctl" : "pQueryCmplt"),
                          2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * As a query, the OID_GEN_RCV_ERROR OID specifies the number of
     * frames that a NIC receives but does not indicate to the protocols
     * due to errors.
     */

    pVal = (UINT32 *) (((UINT8 *) pQueryCmplt) + sizeof(RNDIS_QUERY_CMPLT));

    status = (pUsbTgtRndis->pBinder->mediumIoctl)
                           (pUsbTgtRndis->pBinder->pMediumDev,
                           USBTGT_NET_IOCTL_END_GET_RCV_ERROR,
                           (void *)pVal);

    USBTGT_RNDIS_VDBG ("usbTgtRndisOidGenRcvError 0x%X\n",
                       *(UINT32 *)pVal, 2, 3, 4, 5, 6);

    pQueryCmplt->infoBufLen = 4;

    return status;
    }

/******************************************************************************
*
* usbTgtRndisOidGenRcvNoBuffer - query Rx no buffer packet count
*
* This routine handles the query message request for the
* OID_GEN_RCV_NO_BUFFER OID to get the packet count which droped for no enough
* buffer.
*
* As a query, the OID_GEN_RCV_NO_BUFFER OID specifies the number
* of frames that the NIC cannot receive due to lack of NIC receive
* buffer space. Some NICs do not provide the exact number of missed
* frames; they provide only the number of times at least one frame
* is missed.
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisOidGenRcvNoBuffer
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_QUERY_MSG *   pQueryMsg,
    RNDIS_QUERY_CMPLT * pQueryCmplt
    )
    {
    UINT32 * pVal;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pQueryCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         "pQueryCmplt" ),
                          2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * As a query, the OID_GEN_RCV_NO_BUFFER OID specifies the number
     * of frames that the NIC cannot receive due to lack of NIC receive
     * buffer space. Some NICs do not provide the exact number of missed
     * frames; they provide only the number of times at least one frame
     * is missed.
     */

    pVal = (UINT32 *) (((UINT8 *) pQueryCmplt) + sizeof(RNDIS_QUERY_CMPLT));

    pVal[0] = 0;
    
    USBTGT_RNDIS_VDBG ("usbTgtRndisOidGenRcvNoBuffer 0x%X\n",
                       *(UINT32 *)pVal, 2, 3, 4, 5, 6);

    pQueryCmplt->infoBufLen = 4;

    return OK;
    }

/******************************************************************************
*
* usbTgtRndisOidGenMacOptions - query the MAC options
*
* This routine handles the query message request for the
* OID_GEN_MAC_OPTIONS OID to get the MAC options.
*
* As a query, the OID_GEN_MAC_OPTIONS OID specifies a bitmask that
* defines optional properties of the underlying driver or a NIC.
*
* There is no message set request to set the MAC options, so the user can not
* set the options on the host side.
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtRndisOidGenMacOptions
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_QUERY_MSG *   pQueryMsg,
    RNDIS_QUERY_CMPLT * pQueryCmplt
    )
    {
    UINT32 *      pVal;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pQueryCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         "pQueryCmplt" ),
                          2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * As a query, the OID_GEN_MAC_OPTIONS OID specifies a bitmask that
     * defines optional properties of the underlying driver or a NIC.
     */

    pVal = (UINT32 *) (((UINT8 *) pQueryCmplt) + sizeof(RNDIS_QUERY_CMPLT));

    pVal[0] = TO_LITTLEL((RNDIS_MAC_OPTION_RECEIVE_SERIALIZED
                          | RNDIS_MAC_OPTION_FULL_DUPLEX));

    USBTGT_RNDIS_VDBG ("usbTgtRndisOidGenMacOptions 0x%X\n",
                       *(UINT32 *)pVal, 2, 3, 4, 5, 6);

    pQueryCmplt->infoBufLen = 4;

    return OK;
    }

/******************************************************************************
*
* usbTgtRndisOid8023PermanentAddress - query parmanent address
*
* This routine handles the query message request for the
* OID_802_3_PERMANENT_ADDRESS OID to get parmanent address.
*
* As a query, the OID_802_3_PERMANENT_ADDRESS OID specifies the permanent
* address of the NIC encoded in the hardware.
*
* RETURNS: OK/ERROR there is some thing wrong
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisOid8023PermanentAddress
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_QUERY_MSG *   pQueryMsg,
    RNDIS_QUERY_CMPLT * pQueryCmplt
    )
    {
    UINT8 * pVal;
    UINT8   PerMacAddr[6];
    STATUS  status= ERROR;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pUsbTgtRndis->pBinder) ||
        (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ||
        (NULL == pQueryCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         (NULL == pUsbTgtRndis->pBinder) ? "pBinder" :
                         (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ?
                         "pBinder->mediumIoctl" : "pQueryCmplt"),
                          2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * As a query, the OID_802_3_PERMANENT_ADDRESS OID specifies the permanent
     * address of the NIC encoded in the hardware.
     */

    pVal = (UINT8 *) (((UINT8 *) pQueryCmplt) + sizeof(RNDIS_QUERY_CMPLT));

    status = (pUsbTgtRndis->pBinder->mediumIoctl)
                   (pUsbTgtRndis->pBinder->pMediumDev,
                    USBTGT_NET_IOCTL_END_GET_MAC_ADDR,
                    (char *)(&PerMacAddr[0]));

    if (OK != status)
        {

        USBTGT_RNDIS_ERR("Get permanent address error from the NIC\n",
                         1, 2, 3, 4, 5, 6);

        pQueryCmplt->status = TO_LITTLEL(RNDIS_STATUS_FAILURE);

        return status;
        }

    memcpy(pVal, PerMacAddr, 6);

    pQueryCmplt->infoBufLen = 6;

    USBTGT_RNDIS_DBG("Get permenent address from the NIC %x:%x:%x:%x:%x:%x\n",
                     PerMacAddr[0],
                     PerMacAddr[1],
                     PerMacAddr[2],
                     PerMacAddr[3],
                     PerMacAddr[4],
                     PerMacAddr[5]);


    return OK;
    }

/******************************************************************************
*
* usbTgtRndisOid8023CurrentAddress - query current address
*
* This routine handles the query message request for the
* OID_802_3_CURRENT_ADDRESS OID to get the current address.
*
* As a query, the OID_802_3_CURRENT_ADDRESS OID specifies the current
* address of the NIC encoded in the hardware.
*
* RETURNS: OK/ERROR there is some thing wrong
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisOid8023CurrentAddress
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_QUERY_MSG *   pQueryMsg,
    RNDIS_QUERY_CMPLT * pQueryCmplt
    )
    {
    char * pVal;
    char   CurMacAddr[6];
    STATUS  status = ERROR;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pUsbTgtRndis->pBinder) ||
        (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ||
        (NULL == pQueryCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         (NULL == pUsbTgtRndis->pBinder) ? "pBinder" :
                         (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ?
                         "pBinder->mediumIoctl" : "pQueryCmplt"),
                          2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * As a query, the OID_802_3_CURRENT_ADDRESS OID specifies the current
     * address of the NIC encoded in the hardware.
     */

    pVal = (char *) (((char *) pQueryCmplt) + sizeof(RNDIS_QUERY_CMPLT));

    status = (pUsbTgtRndis->pBinder->mediumIoctl)
                   (pUsbTgtRndis->pBinder->pMediumDev,
                    USBTGT_NET_IOCTL_END_GET_MAC_ADDR,
                    (char *)(&CurMacAddr[0]));

    if (OK != status)
        {

        USBTGT_RNDIS_ERR("Get current address error from the NIC\n",
                         1, 2, 3, 4, 5, 6);

        pQueryCmplt->status = TO_LITTLEL(RNDIS_STATUS_FAILURE);

        return status;
        }

    memcpy(pVal, CurMacAddr, 6);

    pQueryCmplt->infoBufLen = 6;

    USBTGT_RNDIS_DBG("Get current address from the NIC %x:%x:%x:%x:%x:%x\n",
                     CurMacAddr[0],
                     CurMacAddr[1],
                     CurMacAddr[2],
                     CurMacAddr[3],
                     CurMacAddr[4],
                     CurMacAddr[5]);

    return OK;
    }

/******************************************************************************
*
* usbTgtRndisOid8023MulticastListSet - set multicast list
*
* This routine handles the set message request for the
* OID_802_3_MULTICAST_LIST OID to set the multicast list.
*
* As a set, the OID_802_3_MULTICAST_LIST OID specifies the multicast
* address list on the NIC enabled for packet reception. For a set,
* the NIC will set the multicast address lists provied by the NDIS.
*
* RETURNS: OK/ERROR there is some thing wrong
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisOid8023MulticastListSet
    (
    pUSBTGT_RNDIS     pUsbTgtRndis,
    RNDIS_SET_MSG *   pSetMsg,
    RNDIS_SET_CMPLT * pSetCmplt
    )
    {
    char *        pVal;
    UINT16        count = 0;
    UINT32        infoBufLen;
    STATUS        status = ERROR;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pUsbTgtRndis->pBinder) ||
        (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ||
        (NULL == pSetMsg) ||
        (NULL == pSetCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         (NULL == pUsbTgtRndis->pBinder) ? "pBinder" :
                         (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ?
                         "pBinder->mediumIoctl" :
                         (NULL == pSetMsg) ? "pSetMsg" :
                         "pSetCmplt"),
                          2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * As a Set, the OID_802_3_MULTICAST_LIST OID specifies the multicast
     * address list on the NIC enabled for packet reception. For a set,
     * the NIC will set the multicast address lists provied by the NDIS.
     */

    USBTGT_RNDIS_DBG("Set OID_802_3_MULTICAST_LIST with value \n",
                      1, 2, 3, 4, 5, 6);

    infoBufLen = FROM_LITTLEL(pSetMsg->infoBufLen);

    pVal = (char *)(((char *) &pSetMsg->requestId) +
                       FROM_LITTLEL(pSetMsg->infoBufOffset));

    if (infoBufLen % 6)
        {
        USBTGT_RNDIS_ERR("Invalid mulitcast list data\n",
                         1, 2, 3, 4, 5, 6);

        pSetCmplt->status = TO_LITTLEL(RNDIS_STATUS_FAILURE);
        }
    else
        {
        while (count < infoBufLen)
            {

            /* Add the multiple address */

            status = (pUsbTgtRndis->pBinder->mediumIoctl)
                   (pUsbTgtRndis->pBinder->pMediumDev,
                    USBTGT_NET_IOCTL_END_SET_MULTIPLE_ADDR_ADD,
                    (void *)(pVal + count));

            if (OK != status)
                {
                USBTGT_RNDIS_WARN("Set address got from multicast list data fail %d\n",
                                  status, 2, 3, 4, 5, 6);
                UINT8 * pBuf = (UINT8 *)(pVal + count);
                
                if ((pBuf[0] != 0) ||(pBuf[1] != 0) || (pBuf[2] != 0) ||
                    (pBuf[3] != 0) ||(pBuf[4] != 0) || (pBuf[5] != 0))
                    {
                    pSetCmplt->status = TO_LITTLEL(RNDIS_STATUS_FAILURE);
                    }
                
                /* 
                 * Do not break here, till all the address have been added.
                 * Internel testing shows that, the host protocols may
                 * attempt to add unicast addresses to the multicast list.
                 * Such as the [00 00 00 00 00 00], which is invalid address.
                 * Our network stack will avoid this and return EINVAL as the
                 * return value.
                 */
                }
            count = (UINT16) (count + 6);
            
            }
        pSetCmplt->status = TO_LITTLEL(RNDIS_STATUS_SUCCESS);
        }

    USBTGT_RNDIS_DBG("Set OID_802_3_MULTICAST_LIST len 0x%X with status %d \n",
                      infoBufLen,status,3,4,5,6);

    return OK;
    }

/******************************************************************************
*
* usbTgtRndisOid8023MulticastListQuery - query multicast list
*
* This routine handles the query message request for the
* OID_802_3_MULTICAST_LIST OID to get the multicast list value.
*
* As a query, the OID_802_3_MULTICAST_LIST OID specifies the multicast
* address list on the NIC enabled for packet reception. For a query,
* NDIS returns a list containing the union of all bindings' multicast
* address lists.
*
* RETURNS: OK/ERROR there is some thing wrong
*
* ERRNO:N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtRndisOid8023MulticastListQuery
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_QUERY_MSG *   pQueryMsg,
    RNDIS_QUERY_CMPLT * pQueryCmplt
    )
    {
    UINT8 *       pVal;
    MULTI_TABLE * pTable;
    STATUS        status = ERROR;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pUsbTgtRndis->pBinder) ||
        (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ||
        (NULL == pQueryMsg) ||
        (NULL == pQueryCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         (NULL == pUsbTgtRndis->pBinder) ? "pBinder" :
                         (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ?
                         "pBinder->mediumIoctl" :
                         (NULL == pQueryMsg) ? "pQueryMsg" :
                         "pQueryCmplt"),
                          2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * As a query, the OID_802_3_MULTICAST_LIST OID specifies the multicast
     * address list on the NIC enabled for packet reception. For a query,
     * NDIS returns a list containing the union of all bindings' multicast
     * address lists.
     */

    USBTGT_RNDIS_DBG("Query OID_802_3_MULTICAST_LIST with value \n",
                      1, 2, 3, 4, 5, 6);

    pVal = (UINT8 *)(((UINT8 *) pQueryCmplt) + sizeof(RNDIS_QUERY_CMPLT));

    pTable = (MULTI_TABLE *) pVal;

    /* Get the Multicast list */

    status = (pUsbTgtRndis->pBinder->mediumIoctl)
                   (pUsbTgtRndis->pBinder->pMediumDev,
                    USBTGT_NET_IOCTL_END_GET_MULTIPLE_ADDR,
                    (void *)(pTable));

    if (OK != status)
        {

        USBTGT_RNDIS_ERR("Get mulitcast list data error from the NIC\n",
                         1, 2, 3, 4, 5, 6);

        pQueryCmplt->status = TO_LITTLEL(RNDIS_STATUS_FAILURE);

        return status;
        }

    /* Here we got the multi-cast list data */

    bcopy ((char *) pTable->pTable, (char *) (pVal), pTable->len);

    pQueryCmplt->infoBufLen = (UINT32) (pTable->len);

    USBTGT_RNDIS_DBG("Query OID_802_3_MULTICAST_LIST len 0x%X with status %d \n",
                      pQueryCmplt->infoBufLen,status,3,4,5,6);


    return status;
    }

/******************************************************************************
*
* usbTgtRndisOid8023MaximumListSize - query max list size
*
* This routine handles the query message request for the
* OID_802_3_MAXIMUM_LIST_SIZE OID to get the max list size.
*
* As a query, the OID_802_3_MAXIMUM_LIST_SIZE OID specifies maximum number
* of multicast addresses the NIC driver can manage.
*
* RETURNS: OK or ERROR there is some thing wrong
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtRndisOid8023MaximumListSize
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_QUERY_MSG *   pQueryMsg,
    RNDIS_QUERY_CMPLT * pQueryCmplt
    )
    {
    UINT32 *  pVal;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pQueryCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         "pQueryCmplt" ),
                          2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * As a query, the OID_802_3_MAXIMUM_LIST_SIZE OID specifies maximum number
     * of multicast addresses the NIC driver can manage.
     */

    pVal = (UINT32 *) (((UINT8 *) pQueryCmplt) + sizeof(RNDIS_QUERY_CMPLT));

    /*
     * NOTE: This value is related to the NIC, and differnet NIC may different
     * And some of the NIC driver do not have the Ioctl routine to handle this.
     * So, recently, we only use RNDIS_MAX_MCAST_LIST_SZ as default.Marked as
     * TODO if this needed on some NIC driver.
     */

    pVal[0] = TO_LITTLEL((UINT32)(RNDIS_MAX_MCAST_LIST_SZ));
    
    USBTGT_RNDIS_VDBG ("usbTgtRndisOid8023MaximumListSize 0x%X\n",
                       *(UINT32 *)pVal, 2, 3, 4, 5, 6);

    pQueryCmplt->infoBufLen = 4;

    return OK;
    }

/******************************************************************************
*
* usbTgtRndisOid8023RcvErrorAlignment - query the count of alignment error
*
* This routine handles the query message request for the
* OID_802_3_RCV_ERROR_ALIGNMENT OID to get the count of alignment error frames.
*
* As a query, the OID_802_3_RCV_ERROR_ALIGNMENT OID specifies the number of
* frames received with alignment errors.
*
* RETURNS: OK/ERROR there is some thing wrong
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisOid8023RcvErrorAlignment
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_QUERY_MSG *   pQueryMsg,
    RNDIS_QUERY_CMPLT * pQueryCmplt
    )
    {
    UINT32 *      pVal;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pQueryCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         "pQueryCmplt" ),
                          2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * As a query, the OID_802_3_RCV_ERROR_ALIGNMENT OID specifies the number of
     * frames received with alignment errors.
     */

    pVal = (UINT32 *) (((UINT8 *) pQueryCmplt) + sizeof(RNDIS_QUERY_CMPLT));

    /*
     * NOTE: This value is related to the NIC, return 0 to be simple TODO if
     * this needed on some NIC driver.
     */

    pVal[0] = 0;
    
    USBTGT_RNDIS_VDBG ("usbTgtRndisOid8023RcvErrorAlignment 0x%X\n",
                       *(UINT32 *)pVal, 2, 3, 4, 5, 6);

    pQueryCmplt->infoBufLen = 4;

    return OK;
    }

/******************************************************************************
*
* usbTgtRndisOid8023XmitOneCollision - query the frame count after one collision
*
* This routine handles the query message request for the
* OID_802_3_XMIT_ONE_COLLISION OID.
*
* As a query, the OID_802_3_XMIT_ONE_COLLISION OID specifies the number of
* frames successfully transmitted after exactly one collision.
*
* RETURNS: OK/ERROR there is some thing wrong
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisOid8023XmitOneCollision
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_QUERY_MSG *   pQueryMsg,
    RNDIS_QUERY_CMPLT * pQueryCmplt
    )
    {
    UINT32 *      pVal;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pQueryCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         "pQueryCmplt" ),
                          2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * As a query, the OID_802_3_XMIT_ONE_COLLISION OID specifies the number of
     * frames successfully transmitted after exactly one collision.
     */

    pVal = (UINT32 *) (((UINT8 *) pQueryCmplt) + sizeof(RNDIS_QUERY_CMPLT));

    /*
     * NOTE: This value is related to the NIC, return 0 to be simple TODO if
     * this needed on some NIC driver.
     */

    pVal[0] = 0;
    
    USBTGT_RNDIS_VDBG ("usbTgtRndisOid8023XmitOneCollision 0x%X\n",
                       *(UINT32 *)pVal, 2, 3, 4, 5, 6);

    pQueryCmplt->infoBufLen = 4;

    return OK;
    }

/******************************************************************************
*
* usbTgtRndisOid8023XmitMoreCollisions - query frames count under more collision
*
* This routine handles the query message request for the
* OID_802_3_XMIT_MORE_COLLISIONS OID.
*
* As a query, the OID_802_3_XMIT_MORE_COLLISIONS OID specifies the number of
* frames successfully transmitted after more than one collision.
*
* RETURNS: OK/ERROR there is some thing wrong
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtRndisOid8023XmitMoreCollisions
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_QUERY_MSG *   pQueryMsg,
    RNDIS_QUERY_CMPLT * pQueryCmplt
    )
    {
    UINT32 *      pVal;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pQueryCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         "pQueryCmplt" ),
                          2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * As a query, the OID_802_3_XMIT_MORE_COLLISIONS OID sspecifies the number
     * of frames successfully transmitted after more than one collision.
     */

    pVal = (UINT32 *) (((UINT8 *) pQueryCmplt) + sizeof(RNDIS_QUERY_CMPLT));

    /*
     * NOTE: This value is related to the NIC, return 0 to be simple TODO if
     * this needed on some NIC driver.
     */

    pVal[0] = 0;
    
    USBTGT_RNDIS_VDBG ("usbTgtRndisOid8023XmitMoreCollisions 0x%X\n",
                       *(UINT32 *)pVal, 2, 3, 4, 5, 6);

    pQueryCmplt->infoBufLen = 4;

    return OK;
    }


/******************************************************************************
*
* usbTgtRndisOidPnpCapabilities - query power management capabilities
*
* This routine handles the query message request for the
* OID_PNP_CAPABILITIES OID.
*
* As a query, the OID_PNP_CAPABILITIES OID requests a miniport to return
* the wake-up capabilities of its NIC or requests an intermediate driver
* to return the intermediate driver's wake-up capabilities. The wake-up
* capabilities are formatted as an NDIS_PNP_CAPABILITIES.
*
* If the miniport returns NDIS_STATUS_SUCCESS to a query of
* OID_PNP_CAPABILITIES, NDIS considers the miniport to be PM-aware.
* If the miniport returns NDIS_STATUS_NOT_SUPPORTED, NDIS considers the
* miniport to be a legacy miniport that is not PM-aware.
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisOidPnpCapabilities
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_QUERY_MSG *   pQueryMsg,
    RNDIS_QUERY_CMPLT * pQueryCmplt
    )
    {
    RNDIS_PNP_CAPABILITIES * pPnpCap = NULL;
    STATUS                   status = ERROR;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pUsbTgtRndis->pBinder) ||
        (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ||
        (NULL == pQueryCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         (NULL == pUsbTgtRndis->pBinder) ? "pBinder" :
                         (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ?
                         "pBinder->mediumIoctl" : "pQueryCmplt"),
                          2, 3, 4, 5, 6);

        return ERROR;
        }

    pPnpCap = (RNDIS_PNP_CAPABILITIES *) (((UINT8 *) pQueryCmplt) +
                                            sizeof(RNDIS_QUERY_CMPLT));

    /*
     * Quary the enable capabiltiies using the IOCTL to
     * get this capabilities from the medium. The NIC driver may
     * do not support such feature.
     */

    status = (pUsbTgtRndis->pBinder->mediumIoctl)
                           (pUsbTgtRndis->pBinder->pMediumDev,
                            USBTGT_NET_IOCTL_END_PNP_CAPABILITIES_GET,
                           (void *)pPnpCap);

    pQueryCmplt->infoBufLen = sizeof(RNDIS_PNP_CAPABILITIES);

    /*
     * If the status return ERROR, means the Medium do not support
     * Power management.
     */

    return status;
    }

/******************************************************************************
*
* usbTgtRndisOidPnpPowerQuery - query NDIS_DEVICE_POWER_STATE value
*
* This routine handles the query message request for the
* OID_PNP_QUERY_POWER OID.
*
* As a query, the OID_PNP_QUERY_POWER OID requests the miniport to indicate
* whether it can transition its NIC to the low-power state specified in the
* InformationBuffer of an associated NdisRequest query. The following table
* shows the NDIS_DEVICE_POWER_STATE values that indicate low-power state.
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisOidPnpPowerQuery
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_QUERY_MSG *   pQueryMsg,
    RNDIS_QUERY_CMPLT * pQueryCmplt
    )
    {
    RNDIS_DEVICE_POWER_STATE * pDevPowerState = NULL;
    STATUS                     status = ERROR;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pUsbTgtRndis->pBinder) ||
        (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ||
        (NULL == pQueryMsg)    ||
        (NULL == pQueryCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         (NULL == pUsbTgtRndis->pBinder) ? "pBinder" :
                         (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ?
                         "pBinder->mediumIoctl" :
                         (NULL == pQueryMsg) ? "pQueryMsg" :
                         "pQueryCmplt" ),
                          2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * As a query, the OID_PNP_QUERY_POWER OID requests the miniport to
     * indicate whether it can transition its NIC to the low-power state
     * specified in the InformationBuffer of an associated NdisRequest query.
     */

    pDevPowerState = (RNDIS_DEVICE_POWER_STATE *)(((UINT8 *) &pQueryMsg->requestId) +
                                        FROM_LITTLEL(pQueryMsg->infoBufOffset));

    /*
     * Quary the power using the IOCTL to
     * get this status from the medium. The NIC driver may
     * do not support such feature.
     */

    switch(FROM_LITTLEL(* pDevPowerState))
        {
        case RNDIS_DEVICE_STATE_UNSPECIFIED:

            USBTGT_RNDIS_DBG("usbTgtRndisOidPnpQueryPower "
                             "RNDIS_DEVICE_STATE_UNSPECIFIED\n",
                             1,2,3,4,5,6);

            break;
        case RNDIS_DEVICE_STATE_D0:

            USBTGT_RNDIS_DBG("usbTgtRndisOidPnpQueryPower "
                             "RNDIS_DEVICE_STATE_D0\n",
                             1,2,3,4,5,6);

            break;
        case RNDIS_DEVICE_STATE_D1:

            USBTGT_RNDIS_DBG("usbTgtRndisOidPnpQueryPower "
                             "RNDIS_DEVICE_STATE_D1\n",
                             1,2,3,4,5,6);

            break;
        case RNDIS_DEVICE_STATE_D2:

            USBTGT_RNDIS_DBG("usbTgtRndisOidPnpQueryPower "
                             "RNDIS_DEVICE_STATE_D2\n",
                             1,2,3,4,5,6);

            break;
        case RNDIS_DEVICE_STATE_D3:

            USBTGT_RNDIS_DBG("usbTgtRndisOidPnpQueryPower "
                             "RNDIS_DEVICE_STATE_D3\n",
                             1,2,3,4,5,6);

            break;
        default:

            USBTGT_RNDIS_DBG("usbTgtRndisOidPnpQueryPower "
                             "unKnown 0x%X\n",
                             FROM_LITTLEL(* pDevPowerState),2,3,4,5,6);

            break;
        }

    /*
     * Quary the enable capabiltiies using the IOCTL to
     * check the medium if this feature supported or not. The NIC driver may
     * do not support such feature if return ERROR.
     */

    status = (pUsbTgtRndis->pBinder->mediumIoctl)
                           (pUsbTgtRndis->pBinder->pMediumDev,
                           USBTGT_NET_IOCTL_END_PNP_POWER_GET,
                           (void *)(FROM_LITTLEL(* pDevPowerState)));

    pQueryCmplt->infoBufLen = 0;

    return status;
    }

/******************************************************************************
*
* usbTgtRndisOidPnpPowerSet - query device power state which has been set
*
* This routine handles the query message request for the
* OID_PNP_SET_POWER OID.
*
* As a set,the OID_PNP_SET_POWER OID notifies a miniport driver that its NIC
* will be transitioning to the device power state specified in the
* InformationBuffer. The device power state is specified as one of the
* following NDIS_DEVICE_POWER_STATE values:
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisOidPnpPowerSet
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_SET_MSG *     pSetMsg,
    RNDIS_SET_CMPLT *   pSetCmplt
    )
    {
    RNDIS_DEVICE_POWER_STATE * pVal;
    STATUS                     status = ERROR;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pUsbTgtRndis->pBinder) ||
        (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ||
        (NULL == pSetMsg)    ||
        (NULL == pSetCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         (NULL == pUsbTgtRndis->pBinder) ? "pBinder" :
                         (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ?
                         "pBinder->mediumIoctl" :
                         (NULL == pSetMsg) ? "pSetMsg" :
                         "pSetCmplt" ),
                          2, 3, 4, 5, 6);

        return ERROR;
        }

    pSetCmplt->status = TO_LITTLEL(RNDIS_STATUS_SUCCESS);

    pVal = (RNDIS_DEVICE_POWER_STATE *)(((UINT8 *) &pSetMsg->requestId) +
                                        FROM_LITTLEL(pSetMsg->infoBufOffset));

    /*
     * The OID_PNP_SET_POWER OID notifies a miniport driver that its NIC
     * will be transitioning to the device power state specified in the
     * InformationBuffer. The device power state is specified as one of the
     * following NDIS_DEVICE_POWER_STATE values:
     */

    pUsbTgtRndis->powerState = FROM_LITTLEL(*pVal);

    switch(pUsbTgtRndis->powerState)
        {
        case RNDIS_DEVICE_STATE_UNSPECIFIED:

            USBTGT_RNDIS_DBG("usbTgtRndisOidPnpSetPower "
                             "RNDIS_DEVICE_STATE_UNSPECIFIED\n",
                             1,2,3,4,5,6);

            break;
        case RNDIS_DEVICE_STATE_D0:

            USBTGT_RNDIS_DBG("usbTgtRndisOidPnpSetPower "
                             "RNDIS_DEVICE_STATE_D0\n",
                             1,2,3,4,5,6);

            break;
        case RNDIS_DEVICE_STATE_D1:

            USBTGT_RNDIS_DBG("usbTgtRndisOidPnpSetPower "
                             "RNDIS_DEVICE_STATE_D1\n",
                             1,2,3,4,5,6);

            break;
        case RNDIS_DEVICE_STATE_D2:

            USBTGT_RNDIS_DBG("usbTgtRndisOidPnpSetPower "
                             "RNDIS_DEVICE_STATE_D2\n",
                             1,2,3,4,5,6);

            break;
        case RNDIS_DEVICE_STATE_D3:

            USBTGT_RNDIS_DBG("usbTgtRndisOidPnpSetPower "
                             "RNDIS_DEVICE_STATE_D3\n",
                             1,2,3,4,5,6);

            break;
        default:

            USBTGT_RNDIS_DBG("usbTgtRndisOidPnpSetPower "
                             "unknown 0x%X\n",
                             pUsbTgtRndis->powerState,2,3,4,5,6);

            break;
        }

    /*
     * Set the power using the IOCTL to
     * check the medium if this feature supported or not. The NIC driver may
     * do not support such feature if return ERROR.
     */

    status = (pUsbTgtRndis->pBinder->mediumIoctl)
                           (pUsbTgtRndis->pBinder->pMediumDev,
                           USBTGT_NET_IOCTL_END_PNP_POWER_SET,
                           (void *)(pUsbTgtRndis->powerState));


    return status;
    }


/******************************************************************************
*
* usbTgtRndisFindWakeUpPattern - find wake up pattern
*
* This routine handles the query message request for the
* OID_PNP_SET_POWER OID.
*
* As a set,the OID_PNP_SET_POWER OID notifies a miniport driver that its NIC
* will be transitioning to the device power state specified in the
* InformationBuffer. The device power state is specified as one of the
* following NDIS_DEVICE_POWER_STATE values:
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL RNDIS_PM_PATTERN_LIST_ENTRY * usbTgtRndisFindWakeUpPattern
    (
    LIST *                    pList,
    RNDIS_PM_PACKET_PATTERN * pPattern,
    UINT32                    length
    )
    {
    RNDIS_PM_PATTERN_LIST_ENTRY * pListEntry;

    for (pListEntry = (RNDIS_PM_PATTERN_LIST_ENTRY *) lstFirst (pList);
         pListEntry != NULL;
         pListEntry = (RNDIS_PM_PATTERN_LIST_ENTRY *) lstNext ((NODE *) pListEntry))
        {
        if (pListEntry->header.length == length)
            {
            if (memcmp(&pListEntry->pattern,pPattern,length) == 0)
                {
                return (pListEntry);
                }
            }
        }
    return NULL;
    }

/******************************************************************************
*
* usbTgtRndisOidPnpAddWakeUpPattern - add wake up pattern
*
* This routine handles the query message request for the
* OID_PNP_ADD_WAKE_UP_PATTERN OID.
*
* The OID_PNP_ADD_WAKE_UP_PATTERN OID is sent by a protocol driver to a
* miniport driver to specify a wake-up pattern. The wake-up pattern, along
* with its mask, is described by an NDIS_PM_PACKET_PATTERN structure. For more
* information on this structure, see NDIS_PM_PACKET_PATTERN.
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisOidPnpAddWakeUpPattern
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_SET_MSG *     pSetMsg,
    RNDIS_SET_CMPLT *   pSetCmplt
    )
    {
    RNDIS_PM_PACKET_PATTERN * pVal;
    RNDIS_PM_PATTERN_LIST_ENTRY * pPatternEntry;
    UINT32 infoBufLen;
    int    patternLen;
    STATUS status = ERROR;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pUsbTgtRndis->pBinder) ||
        (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ||
        (NULL == pSetMsg)    ||
        (NULL == pSetCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         (NULL == pUsbTgtRndis->pBinder) ? "pBinder" :
                         (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ?
                         "pBinder->mediumIoctl" :
                         (NULL == pSetMsg) ? "pSetMsg" :
                         "pSetCmplt" ),
                          2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * The OID_PNP_ADD_WAKE_UP_PATTERN OID is sent by a protocol driver to a
     * miniport driver to specify a wake-up pattern. The wake-up pattern, along
     * with its mask, is described by an NDIS_PM_PACKET_PATTERN structure. For more
     * information on this structure, see NDIS_PM_PACKET_PATTERN.
     */

    pSetCmplt->status = TO_LITTLEL(RNDIS_STATUS_SUCCESS);

    pVal = (RNDIS_PM_PACKET_PATTERN *)(((UINT8 *) &pSetMsg->requestId) +
                                       FROM_LITTLEL(pSetMsg->infoBufOffset));

    /*
     * Set the power using the IOCTL to
     * check the medium if this feature supported or not. The NIC driver may
     * do not support such feature if return ERROR.
     */

    status = (pUsbTgtRndis->pBinder->mediumIoctl)
                           (pUsbTgtRndis->pBinder->pMediumDev,
                           USBTGT_NET_IOCTL_END_PNP_WAKEUP_PATTERN_ADD,
                           (void *)(pVal));

    if (ERROR == status)
        {
        return ERROR;
        }

    infoBufLen = FROM_LITTLEL(pSetMsg->infoBufLen);

    patternLen = FROM_LITTLEL(pVal->patternSize);

    pPatternEntry = usbTgtRndisFindWakeUpPattern(&pUsbTgtRndis->wakeUpPatternList,
                                                 pVal,
                                                 infoBufLen);

    if (NULL == pPatternEntry)
        {
        /* Allocate memory for pattern and populate it */

        pPatternEntry = (RNDIS_PM_PATTERN_LIST_ENTRY *)
                        malloc(infoBufLen +
                               sizeof(RNDIS_PM_PATTERN_LIST_HEADER));

        if (pPatternEntry == NULL)
            {
            USBTGT_RNDIS_ERR("No enouth memory to calloc "
                             "RNDIS_PM_PATTERN_LIST_HEADER\n",
                             1, 2, 3, 4, 5, 6);
            return ERROR;
            }

        pPatternEntry->header.length = infoBufLen;

        memcpy(&pPatternEntry->pattern, pVal, infoBufLen);

        lstAdd(&pUsbTgtRndis->wakeUpPatternList,(NODE *)pPatternEntry);

        USBTGT_RNDIS_DBG("usbTgtRndisOidPnpAddWakeUpPattern: Pattern add list count = %d\n",
                         lstCount(&pUsbTgtRndis->wakeUpPatternList),
                         2, 3, 4, 5, 6);
        }
    else
        {
        USBTGT_RNDIS_DBG("usbTgtRndisOidPnpAddWakeUpPattern: Pattern exists list count %d\n",
                         lstCount(&pUsbTgtRndis->wakeUpPatternList),
                         2, 3, 4, 5, 6);
        }

    return OK;

    }

/******************************************************************************
*
* usbTgtRndisOidPnpRemoveWakeUpPattern - remove wake up pattern
*
* This routine handles the query message request for the
* OID_PNP_REMOVE_WAKE_UP_PATTERN OID.
*
* The OID_PNP_REMOVE_WAKE_UP_PATTERN OID requests the miniport driver to
* delete a wake-up pattern that it previously received in an
* OID_PNP_ADD_WAKE_UP_PATTERN request. The wake-up pattern, along with
* its mask, is described by an NDIS_PM_PACKET_PATTERN structure.
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisOidPnpRemoveWakeUpPattern
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_SET_MSG *     pSetMsg,
    RNDIS_SET_CMPLT *   pSetCmplt
    )
    {
    RNDIS_PM_PACKET_PATTERN * pVal;
    RNDIS_PM_PATTERN_LIST_ENTRY * pPatternEntry;
    UINT32 infoBufLen;
    int    patternLen;
    STATUS status = ERROR;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pUsbTgtRndis->pBinder) ||
        (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ||
        (NULL == pSetMsg)    ||
        (NULL == pSetCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         (NULL == pUsbTgtRndis->pBinder) ? "pBinder" :
                         (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ?
                         "pBinder->mediumIoctl" :
                         (NULL == pSetMsg) ? "pSetMsg" :
                         "pSetCmplt" ),
                          2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * The OID_PNP_REMOVE_WAKE_UP_PATTERN OID requests the miniport driver to
     * delete a wake-up pattern that it previously received in an
     * OID_PNP_ADD_WAKE_UP_PATTERN request. The wake-up pattern, along with
     * its mask, is described by an NDIS_PM_PACKET_PATTERN structure.
     */

    pSetCmplt->status = TO_LITTLEL(RNDIS_STATUS_SUCCESS);

    pVal = (RNDIS_PM_PACKET_PATTERN *)(((UINT8 *) &pSetMsg->requestId) +
                                       FROM_LITTLEL(pSetMsg->infoBufOffset));

    /*
     * Set the power using the IOCTL to
     * check the medium if this feature supported or not. The NIC driver may
     * do not support such feature if return ERROR.
     */

    status = (pUsbTgtRndis->pBinder->mediumIoctl)
                           (pUsbTgtRndis->pBinder->pMediumDev,
                           USBTGT_NET_IOCTL_END_PNP_WAKEUP_PATTERN_REMOVE,
                           (void *)(pVal));

    if (ERROR == status)
        {
        return ERROR;
        }

    infoBufLen = FROM_LITTLEL(pSetMsg->infoBufLen);

    patternLen = FROM_LITTLEL(pVal->patternSize);

    pPatternEntry = usbTgtRndisFindWakeUpPattern(&pUsbTgtRndis->wakeUpPatternList,
                                                 pVal,
                                                 infoBufLen);

    if (NULL != pPatternEntry)
        {
        lstDelete (&pUsbTgtRndis->wakeUpPatternList,
                   (NODE *) pPatternEntry);

        free(pPatternEntry);

        USBTGT_RNDIS_DBG("usbTgtRndisOidPnpRemoveWakeUpPattern: lstCount %d\n",
                        lstCount(&pUsbTgtRndis->wakeUpPatternList),
                        2, 3, 4, 5, 6);
        }
    else
        {
        USBTGT_RNDIS_DBG("usbTgtRndisOidPnpRemoveWakeUpPattern: lstcount %d\n",
                        lstCount(&pUsbTgtRndis->wakeUpPatternList),
                        2, 3, 4, 5, 6);
        }

    return OK;
    }

/******************************************************************************
*
* usbTgtRndisOidPnpEnableWakeUpSet - set wake up capabilities
*
* This routine handles the set message request for the
* OID_PNP_ENABLE_WAKE_UP OID.
*
* As a set, the OID_PNP_ENABLE_WAKE_UP OID specifies the wake-up
* capabilities that a miniport driver should enable in a NIC.
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisOidPnpEnableWakeUpSet
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_SET_MSG *     pSetMsg,
    RNDIS_SET_CMPLT *   pSetCmplt
    )
    {
    UINT32 * pVal;
    UINT32   wakeUp;
    STATUS   status = ERROR;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pUsbTgtRndis->pBinder) ||
        (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ||
        (NULL == pSetMsg)    ||
        (NULL == pSetCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         (NULL == pUsbTgtRndis->pBinder) ? "pBinder" :
                         (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ?
                         "pBinder->mediumIoctl" :
                         (NULL == pSetMsg) ? "pSetMsg" :
                         "pSetCmplt" ),
                          2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * As a set, the OID_PNP_ENABLE_WAKE_UP OID specifies the wake-up
     * capabilities that a miniport driver should enable in a NIC.
     */

    pSetCmplt->status = TO_LITTLEL(RNDIS_STATUS_SUCCESS);

    pVal = (UINT32 *)(((UINT8 *) &pSetMsg->requestId) +
                      FROM_LITTLEL(pSetMsg->infoBufOffset));

    wakeUp = FROM_LITTLEL(*pVal);

    if (wakeUp & RNDIS_PNP_WAKE_UP_MAGIC_PACKET)
        {
        pUsbTgtRndis->wakeUpnMagicPacketEnable = TRUE;
        }
    else
        {
        pUsbTgtRndis->wakeUpnMagicPacketEnable = FALSE;
        }

    if (wakeUp & RNDIS_PNP_WAKE_UP_PATTERN_MATCH)
        {
        pUsbTgtRndis->wakeUpPatternMatchEnable = TRUE;
        }
    else
        {
        pUsbTgtRndis->wakeUpPatternMatchEnable = FALSE;
        }

    if (wakeUp & RNDIS_PNP_WAKE_UP_LINK_CHANGE)
        {
        pUsbTgtRndis->wakeUpLinkChangeEnable = TRUE;
        }
    else
        {
        pUsbTgtRndis->wakeUpLinkChangeEnable = FALSE;
        }

    /*
     * Set the wakeup enable using the IOCTL to
     * manage the medium if this feature supported. The NIC driver may
     * do not support such feature if return ERROR.
     */

    status = (pUsbTgtRndis->pBinder->mediumIoctl)
                           (pUsbTgtRndis->pBinder->pMediumDev,
                           USBTGT_NET_IOCTL_END_PNP_WAKEUP_ENABLE_SET,
                           (void *)((ULONG)(wakeUp)));

    USBTGT_RNDIS_DBG ("usbTgtRndisOidPnpEnableWakeUpSet: wakeUp = 0x%X status %d\n",
                      wakeUp, status, 3, 4, 5, 6);

    return status;
    }


/******************************************************************************
*
* usbTgtRndisOidPnpEnableWakeUpQuery - query wake up capabilities
*
* This routine handles the query message request for the
* OID_PNP_ENABLE_WAKE_UP OID.
*
* As a query, OID_PNP_ENABLE_WAKE_UP obtains the current wake-up
* capabilities that are enabled for a NIC.
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisOidPnpEnableWakeUpQuery
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    RNDIS_QUERY_MSG *   pQueryMsg,
    RNDIS_QUERY_CMPLT * pQueryCmplt
    )
    {
    UINT32 * pVal;
    STATUS   status = ERROR;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pUsbTgtRndis->pBinder) ||
        (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ||
        (NULL == pQueryMsg)    ||
        (NULL == pQueryCmplt))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         (NULL == pUsbTgtRndis->pBinder) ? "pBinder" :
                         (NULL == pUsbTgtRndis->pBinder->mediumIoctl) ?
                         "pBinder->mediumIoctl" :
                         (NULL == pQueryMsg) ? "pQueryMsg" :
                         "pQueryCmplt" ),
                          2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * As a query, OID_PNP_ENABLE_WAKE_UP obtains the current wake-up
     * capabilities that are enabled for a NIC.
     */

    pQueryCmplt->status = TO_LITTLEL(RNDIS_STATUS_SUCCESS);

    pVal = (UINT32 *)(((UINT8 *) &pQueryMsg->requestId) +
                      FROM_LITTLEL(pQueryMsg->infoBufOffset));

    /*
     * Get the wakeup enable using the IOCTL to
     * manage the medium if this feature supported. The NIC driver may
     * do not support such feature if return ERROR.
     */

    status = (pUsbTgtRndis->pBinder->mediumIoctl)
                          (pUsbTgtRndis->pBinder->pMediumDev,
                           USBTGT_NET_IOCTL_END_PNP_WAKEUP_ENABLE_GET,
                           (void *)(pVal));

    pQueryCmplt->infoBufLen = 4;

    USBTGT_RNDIS_DBG ("usbTgtRndisOidPnpEnableWakeUpQuery: *pVal = 0x%X status %d\n",
                      * pVal, status, 3, 4, 5, 6);

    return status;
    }



/******************************************************************************
*
* usbTgtRndisMsgInitialize - handle the RNDIS intialize message from the host
*
* This routine handles the RNDIS intialize message from the host
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisMsgInitialize
    (
    pUSBTGT_RNDIS          pUsbTgtRndis,
    RNDIS_INITIALIZE_MSG * pMsg
    )
    {
    RNDIS_INITIALIZE_CMPLT * pReply = NULL;
    STATUS                   status = ERROR;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pMsg))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         "pMsg"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    pReply = (RNDIS_INITIALIZE_CMPLT *) &pUsbTgtRndis->replyMsgBuf[0];

    pReply->hdr.msgType = TO_LITTLEL(REMOTE_NDIS_INITIALIZE_CMPLT);
    pReply->hdr.msgLen = TO_LITTLEL(sizeof(RNDIS_INITIALIZE_CMPLT));
    pReply->requestId = pMsg->requestId;
    pReply->status = TO_LITTLEL(RNDIS_STATUS_SUCCESS);
    pReply->majorVersion = TO_LITTLEL(RNDIS_MAJOR_VERSION);
    pReply->minorVersion = TO_LITTLEL(RNDIS_MINOR_VERSION);
    pReply->deviceFlags = TO_LITTLEL(RNDIS_DF_CONNECTIONLESS);
    pReply->medium = TO_LITTLEL(RNDIS_MEDIUM_802_3);
    pReply->maxPktsPerMsg = TO_LITTLEL(1);
    pReply->maxXferSize = pMsg->maxXferSize;

    /* Change 3 -> 0 s_z 1220 */

    pReply->pktAlignmentFactor = TO_LITTLEL(0);
    pReply->AFListOffset = 0;
    pReply->AFListSize = 0;

    pUsbTgtRndis->uReplyMsgLen = sizeof(RNDIS_INITIALIZE_CMPLT);

    USBTGT_RNDIS_DBG("RNDIS intialized with maxXferSize 0x%X\n",
                     pReply->maxXferSize, 2, 3, 4, 5, 6);

    /*
     * From the RNDIS spec 5.5. The function device must return a notification
     * on the communication class interface's interrupt IN endpoint, which is
     * polled by the host whenever the device can return control message.
     */

    status = usbTgtRndisMsgAvailableNotify(pUsbTgtRndis);

    if (status == OK)
        {
        /* RNDIS intialized */

        pUsbTgtRndis->rndisState = RNDIS_INITIALIZED;
        }

    return status;
    }

/******************************************************************************
*
* usbTgtRndisMsgHalt - handle the RNDIS halt message from the host
*
* This routine handles the RNDIS halt message from the host.
*
* From the RNDIS spec chapter 3.2.
* The device must terminate all communication
* immediately after sending this message.The message maybe sent at any time that
* the device is in the RNDIS_INITIALIZED or RNDIS_DATA_INITIALIZED state.
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisMsgHalt
    (
    pUSBTGT_RNDIS    pUsbTgtRndis,
    RNDIS_HALT_MSG * pMsg
    )
    {
    if ((NULL == pUsbTgtRndis) ||
        (NULL == pMsg))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         "pMsg"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * From the RNDIS spec chapter 3.2.
     *
     * The device must terminate all communication immediately
     * after sending this message.Sending this message causes the
     * device to enter the RNDIS_UNINITIALIZED state.
     */


    /* Check if the interrupt in pipe is using or not, if yes, cancel it */

    if (pUsbTgtRndis->bIntInErpUsed)
        {
        if (OK != usbTgtCancelErp(pUsbTgtRndis->intInPipeHandle,
                                     &pUsbTgtRndis->intInErp))
            {
            USBTGT_RNDIS_DBG("Cancel intInErp fail\n", 1, 2, 3, 4, 5, 6);
            }
        }

    /* Check if the Bulk in pipe is using or not, if yes, cancel it */

    if (pUsbTgtRndis->bBulkInErpUsed)
        {
        if (OK != usbTgtCancelErp(pUsbTgtRndis->bulkInPipeHandle,
                                     &pUsbTgtRndis->bulkInErp))
            {
            USBTGT_RNDIS_DBG("Cancel bulkInErp fail\n", 1, 2, 3, 4, 5, 6);
            }
        }

    /* Check if the Bulk Out pipe is using or not, if yes, cancel it */

    if (pUsbTgtRndis->bBulkOutErpUsed)
        {
        if (OK != usbTgtCancelErp(pUsbTgtRndis->bulkOutPipeHandle,
                                     &pUsbTgtRndis->bulkOutErp))
            {
            USBTGT_RNDIS_DBG("Cancel bulkOutErp fail\n", 1, 2, 3, 4, 5, 6);
            }
        }

    /* Go to RNDIS_UNINITIALIZED state */

    pUsbTgtRndis->rndisState = RNDIS_UNINITIALIZED;

    return OK;
    }


/******************************************************************************
*
* usbTgtRndisMsgQuery - handle the RNDIS Query message request from the host
*
* This routine handles the RNDIS Query message request from the host.
*
* From the RNDIS spec chapter 3.3.
* REMOTE_NDIS_QUERY_MSG is sent to a RNDIS device from a host when it needs
* to query the device for its charatcheristisc or statistics information or
* status.
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisMsgQuery
    (
    pUSBTGT_RNDIS     pUsbTgtRndis,
    RNDIS_QUERY_MSG * pQueryMsg
    )
    {
    RNDIS_QUERY_CMPLT * pQueryCmplt = NULL;
    STATUS              status = ERROR;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pQueryMsg))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         "pQueryMsg"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    pQueryCmplt = (RNDIS_QUERY_CMPLT *) &pUsbTgtRndis->replyMsgBuf[0];

    /*
     * From the RNDIS spec chapter 3.3.
     * REMOTE_NDIS_QUERY_MSG is sent to a RNDIS device from a host
     * when it needs to query the device for its charatcheristisc or
     * statistics information or status. The parameters or statistics being
     * queried for is identified by means of an OIDs.
     */

    /* Initialize reply */

    pQueryCmplt->hdr.msgType = TO_LITTLEL(REMOTE_NDIS_QUERY_CMPLT);
    pQueryCmplt->hdr.msgLen = sizeof(RNDIS_QUERY_CMPLT);
    pQueryCmplt->requestId = pQueryMsg->requestId;
    pQueryCmplt->status = RNDIS_STATUS_SUCCESS;
    pQueryCmplt->infoBufOffset = 0;
    pQueryCmplt->infoBufLen = 0;

    switch (FROM_LITTLEL(pQueryMsg->oid))
        {
        case OID_GEN_SUPPORTED_LIST:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMsgQuery with OID_GEN_SUPPORTED_LIST\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisOidGenSupportedList (pUsbTgtRndis,
                                                     pQueryMsg,
                                                     pQueryCmplt);

            break;
            }
        case OID_GEN_HARDWARE_STATUS:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMsgQuery with OID_GEN_HARDWARE_STATUS\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisOidGenHardwareStatus (pUsbTgtRndis,
                                                      pQueryMsg,
                                                      pQueryCmplt);

            break;
            }
        case OID_GEN_MEDIA_SUPPORTED:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMsgQuery with OID_GEN_MEDIA_SUPPORTED\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisOidGenMediaSupported (pUsbTgtRndis,
                                                      pQueryMsg,
                                                      pQueryCmplt);

            break;
            }
        case OID_GEN_MEDIA_IN_USE:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMsgQuery with OID_GEN_MEDIA_IN_USE\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisOidGenMediaInUse (pUsbTgtRndis,
                                                  pQueryMsg,
                                                  pQueryCmplt);

            break;
            }
        case OID_GEN_MAXIMUM_FRAME_SIZE:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMsgQuery with OID_GEN_MAXIMUM_FRAME_SIZE\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisOidGenMaximumFrameSize (pUsbTgtRndis,
                                                        pQueryMsg,
                                                        pQueryCmplt);

            break;
            }
        case OID_GEN_LINK_SPEED:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMsgQuery with OID_GEN_LINK_SPEED\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisOidGenLinkSpeed (pUsbTgtRndis,
                                                 pQueryMsg,
                                                 pQueryCmplt);

            break;
            }
        case OID_GEN_TRANSMIT_BLOCK_SIZE:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMsgQuery with OID_GEN_TRANSMIT_BLOCK_SIZE\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisOidGenTransmitBlockSize (pUsbTgtRndis,
                                                         pQueryMsg,
                                                         pQueryCmplt);

            break;
            }
        case OID_GEN_RECEIVE_BLOCK_SIZE:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMsgQuery with OID_GEN_RECEIVE_BLOCK_SIZE\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisOidGenReceiveBlockSize (pUsbTgtRndis,
                                                        pQueryMsg,
                                                        pQueryCmplt);

            break;
            }
        case OID_GEN_VENDOR_ID:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMsgQuery with OID_GEN_VENDOR_ID\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisOidGenVendorId (pUsbTgtRndis,
                                                pQueryMsg,
                                                pQueryCmplt);

            break;
            }
        case OID_GEN_VENDOR_DESCRIPTION:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMsgQuery with OID_GEN_VENDOR_DESCRIPTION\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisOidGenVendorDescription (pUsbTgtRndis,
                                                         pQueryMsg,
                                                         pQueryCmplt);

            break;
            }
        case OID_GEN_CURRENT_PACKET_FILTER:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMsgQuery with OID_GEN_CURRENT_PACKET_FILTER\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisOidGenCurrentPacketFilterQuery (pUsbTgtRndis,
                                                                pQueryMsg,
                                                                pQueryCmplt);

            break;
            }
        case OID_GEN_MAXIMUM_TOTAL_SIZE:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMsgQuery with OID_GEN_MAXIMUM_TOTAL_SIZE\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisOidGenMaximumTotalSize (pUsbTgtRndis,
                                                        pQueryMsg,
                                                        pQueryCmplt);

            break;
            }
        case OID_GEN_MEDIA_CONNECT_STATUS:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMsgQuery with OID_GEN_MEDIA_CONNECT_STATUS\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisOidGenMediaConnectStatus (pUsbTgtRndis,
                                                          pQueryMsg,
                                                          pQueryCmplt);

            break;
            }
        case OID_GEN_XMIT_OK:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMsgQuery with OID_GEN_XMIT_OK\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisOidGenXmitOk(pUsbTgtRndis,
                                             pQueryMsg,
                                             pQueryCmplt);

            break;
            }
        case OID_GEN_RCV_OK:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMsgQuery with OID_GEN_RCV_OK\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisOidGenRcvOk(pUsbTgtRndis,
                                            pQueryMsg,
                                            pQueryCmplt);

            break;
            }
        case OID_GEN_XMIT_ERROR:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMsgQuery with OID_GEN_XMIT_ERROR\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisOidGenXmitError(pUsbTgtRndis,
                                                pQueryMsg,
                                                pQueryCmplt);

            break;
            }
        case OID_GEN_RCV_ERROR:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMsgQuery with OID_GEN_RCV_ERROR\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisOidGenRcvError(pUsbTgtRndis,
                                               pQueryMsg,
                                               pQueryCmplt);

            break;
            }
        case OID_GEN_RCV_NO_BUFFER:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMsgQuery with OID_GEN_RCV_NO_BUFFER\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisOidGenRcvNoBuffer(pUsbTgtRndis,
                                                  pQueryMsg,
                                                  pQueryCmplt);

            break;
            }
        case OID_GEN_MAC_OPTIONS:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMsgQuery with OID_GEN_MAC_OPTIONS\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisOidGenMacOptions(pUsbTgtRndis,
                                                 pQueryMsg,
                                                 pQueryCmplt);

            break;
            }
        case OID_802_3_MULTICAST_LIST:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMedium8023MsgQuery with "
                              "OID_802_3_MULTICAST_LIST\n",
                              1, 2, 3, 4, 5, 6);

            status = usbTgtRndisOid8023MulticastListQuery(pUsbTgtRndis,
                                                          pQueryMsg,
                                                          pQueryCmplt);

            break;
            }
        case OID_802_3_MAXIMUM_LIST_SIZE:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMedium8023MsgQuery with "
                              "OID_802_3_MAXIMUM_LIST_SIZE\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisOid8023MaximumListSize(pUsbTgtRndis,
                                                       pQueryMsg,
                                                       pQueryCmplt);

            break;
            }
        case OID_802_3_CURRENT_ADDRESS:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMedium8023MsgQuery with "
                             "OID_802_3_CURRENT_ADDRESS\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisOid8023CurrentAddress(pUsbTgtRndis,
                                                      pQueryMsg,
                                                      pQueryCmplt);

            break;
            }
        case OID_802_3_PERMANENT_ADDRESS:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMedium8023MsgQuery with "
                             "OID_802_3_PERMANENT_ADDRESS\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisOid8023PermanentAddress(pUsbTgtRndis,
                                                        pQueryMsg,
                                                        pQueryCmplt);

            break;
            }
        case OID_802_3_RCV_ERROR_ALIGNMENT:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMedium8023MsgQuery with "
                             "OID_802_3_RCV_ERROR_ALIGNMENT\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisOid8023RcvErrorAlignment(pUsbTgtRndis,
                                                         pQueryMsg,
                                                         pQueryCmplt);

            break;
            }
        case OID_802_3_XMIT_ONE_COLLISION:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMedium8023MsgQuery with "
                             "OID_802_3_XMIT_ONE_COLLISION\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisOid8023XmitOneCollision (pUsbTgtRndis,
                                                         pQueryMsg,
                                                         pQueryCmplt);

            break;
            }
        case OID_802_3_XMIT_MORE_COLLISIONS:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMedium8023MsgQuery with "
                             "OID_802_3_XMIT_MORE_COLLISIONS\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisOid8023XmitMoreCollisions (pUsbTgtRndis,
                                                           pQueryMsg,
                                                           pQueryCmplt);

            break;
            }
        case OID_PNP_CAPABILITIES:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMsgQuery with OID_PNP_CAPABILITIES\n",
                             1, 2, 3, 4, 5, 6);

            /* If power management is supported */

            if (usrUsbTgtRndisPMEnableGet(0))
                {
                status = usbTgtRndisOidPnpCapabilities (pUsbTgtRndis,
                                                        pQueryMsg,
                                                        pQueryCmplt);
                }
            break;
            }
        case OID_PNP_QUERY_POWER:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMsgQuery with OID_PNP_QUERY_POWER\n",
                             1, 2, 3, 4, 5, 6);

            /* If power management is supported */

            if (usrUsbTgtRndisPMEnableGet(0))
                {
                status = usbTgtRndisOidPnpPowerQuery (pUsbTgtRndis,
                                                      pQueryMsg,
                                                      pQueryCmplt);
                }
            }
            break;
        case OID_PNP_ENABLE_WAKE_UP:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMsgQuery with OID_PNP_ENABLE_WAKE_UP\n",
                             1, 2, 3, 4, 5, 6);

            /* If power management is supported */

            if (usrUsbTgtRndisPMEnableGet(0))
                {
                status = usbTgtRndisOidPnpEnableWakeUpQuery (pUsbTgtRndis,
                                                             pQueryMsg,
                                                             pQueryCmplt);
                }
            }
            break;
        default:
            USBTGT_RNDIS_DBG("usbTgtRndisMsgQuery with unsupported OID\n",
                             1, 2, 3, 4, 5, 6);

            break;
        }

    if (ERROR == status)
        {
        USBTGT_RNDIS_ERR("usbTgtRndisMsgQuery with unsupported OID 0x%X\n",
                         FROM_LITTLEL(pQueryMsg->oid), 2, 3, 4, 5, 6);

        pQueryCmplt->status = RNDIS_STATUS_NOT_SUPPORTED;
        }

    /* If supported, adjust reply */

    if (pQueryCmplt->status == RNDIS_STATUS_SUCCESS)
        {
        pQueryCmplt->infoBufOffset = 0x10;
        pQueryCmplt->hdr.msgLen += pQueryCmplt->infoBufLen;
        }

    pUsbTgtRndis->uReplyMsgLen = (UINT16)(pQueryCmplt->hdr.msgLen);

    pQueryCmplt->status = TO_LITTLEL(pQueryCmplt->status);
    pQueryCmplt->hdr.msgLen = TO_LITTLEL(pQueryCmplt->hdr.msgLen);
    pQueryCmplt->infoBufOffset = TO_LITTLEL(pQueryCmplt->infoBufOffset);
    pQueryCmplt->infoBufLen = TO_LITTLEL(pQueryCmplt->infoBufLen);

    /* Notify the host the reply is ready */

    return usbTgtRndisMsgAvailableNotify(pUsbTgtRndis);
    }

/******************************************************************************
*
* usbTgtRndisMsgSet - handle the RNDIS set message request from the host
*
* This routine handles the RNDIS set message request from the host.
*
* From the RNDIS spec chapter 3.3.
* REMOTE_NDIS_QUERY_MSG is sent to a RNDIS device from a host when it needs
* to query the device for its charatcheristisc or statistics information or
* status.
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisMsgSet
    (
    pUSBTGT_RNDIS   pUsbTgtRndis,
    RNDIS_SET_MSG * pSetMsg
    )
    {
    RNDIS_SET_CMPLT * pSetCmplt = NULL;
    STATUS            status = ERROR;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pSetMsg))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         "pSetMsg"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    pSetCmplt = (RNDIS_SET_CMPLT *) &pUsbTgtRndis->replyMsgBuf[0];

    /* Initialize reply */

    pSetCmplt->hdr.msgType = TO_LITTLEL(REMOTE_NDIS_SET_CMPLT);
    pSetCmplt->hdr.msgLen = TO_LITTLEL(sizeof(RNDIS_SET_CMPLT));
    pSetCmplt->requestId = pSetMsg->requestId;

    pUsbTgtRndis->uReplyMsgLen = sizeof (RNDIS_SET_CMPLT);

    switch (FROM_LITTLEL(pSetMsg->oid))
        {
        case OID_GEN_CURRENT_PACKET_FILTER:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMsgSet with OID_GEN_CURRENT_PACKET_FILTER\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisOidGenCurrentPacketFilterSet (pUsbTgtRndis,
                                                     pSetMsg,
                                                     pSetCmplt);

            break;
            }
        case OID_802_3_MULTICAST_LIST:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMedium8023MsgSet with "
                             "OID_802_3_MULTICAST_LIST\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisOid8023MulticastListSet(pUsbTgtRndis,
                                                        pSetMsg,
                                                        pSetCmplt);

            break;
            }
        case OID_PNP_SET_POWER:
           {

            USBTGT_RNDIS_DBG("usbTgtRndisMsgSet with OID_PNP_SET_POWER\n",
                             1, 2, 3, 4, 5, 6);

            if (usrUsbTgtRndisPMEnableGet(0))
                {
                status = usbTgtRndisOidPnpPowerSet(pUsbTgtRndis,
                                                   pSetMsg,
                                                   pSetCmplt);
                }
            }

            break;

        case OID_PNP_ADD_WAKE_UP_PATTERN:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMsgSet with OID_PNP_ADD_WAKE_UP_PATTERN\n",
                             1, 2, 3, 4, 5, 6);

            if (usrUsbTgtRndisPMEnableGet(0))
                {
                status = usbTgtRndisOidPnpAddWakeUpPattern(pUsbTgtRndis,
                                                           pSetMsg,
                                                           pSetCmplt);
                }
            }
            break;

        case OID_PNP_REMOVE_WAKE_UP_PATTERN:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMsgSet with OID_PNP_REMOVE_WAKE_UP_PATTERN\n",
                             1, 2, 3, 4, 5, 6);
            if (usrUsbTgtRndisPMEnableGet(0))
                {
                status = usbTgtRndisOidPnpRemoveWakeUpPattern(pUsbTgtRndis,
                                                              pSetMsg,
                                                              pSetCmplt);
                }
            }

            break;

        case OID_PNP_ENABLE_WAKE_UP:
           {

            USBTGT_RNDIS_DBG("usbTgtRndisMsgSet with OID_PNP_ENABLE_WAKE_UP\n",
                             1, 2, 3, 4, 5, 6);

            if (usrUsbTgtRndisPMEnableGet(0))
                {
                status = usbTgtRndisOidPnpEnableWakeUpSet(pUsbTgtRndis,
                                                              pSetMsg,
                                                              pSetCmplt);
                }
            }
            break;
        default:
            USBTGT_RNDIS_DBG("usbTgtRndisMsgQuery with unsupported OID\n",
                             1, 2, 3, 4, 5, 6);

            break;

        }

    if (ERROR == status)
        {
        USBTGT_RNDIS_WARN("usbTgtRndisMsgSet with unsupported OID 0x%08x\n",
                         FROM_LITTLEL(pSetMsg->oid), 2, 3, 4, 5, 6);

        pSetCmplt->status = TO_LITTLEL(RNDIS_STATUS_NOT_SUPPORTED);
        }

    /* Notify the host the reply is ready */

    return usbTgtRndisMsgAvailableNotify(pUsbTgtRndis);
    }



/******************************************************************************
*
* usbTgtRndisMsgReset - handle the RNDIS reset message request from the host
*
* This routine handles the RNDIS reset message request from the host.
*
* From RNDIS Spec 1.0 chapter 3.5
* This message is sent to a Remote NDIS device from a host to reset
* the device and return status. The host may send REMOTE_NDIS_RESET_MSG
* to the device through the control channel at any time that the device
* is in a rndis-initialized or rndis-data-intialized by Remote NDIS. The
* Remote NDIS device will respond to this message by sending a
* REMOTE_NDIS_RESET_CMPLT to the host.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisMsgReset
    (
    pUSBTGT_RNDIS     pUsbTgtRndis,
    RNDIS_RESET_MSG * pResetMsg
    )
    {
    RNDIS_RESET_CMPLT * pResetCmplt = NULL;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pResetMsg))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         "pResetMsg"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    pResetCmplt = (RNDIS_RESET_CMPLT *)&pUsbTgtRndis->replyMsgBuf[0];

    /*
     * From RNDIS Spec 1.0 chapter 3.5
     * This message is sent to a Remote NDIS device from a host to reset
     * the device and return status. The host may send REMOTE_NDIS_RESET_MSG
     * to the device through the control channel at any time that the device
     * is in a rndis-initialized or rndis-data-intialized by Remote NDIS. The
     * Remote NDIS device will respond to this message by sending a
     * REMOTE_NDIS_RESET_CMPLT to the host.
     */

    /* Build the completion message */

    pResetCmplt->hdr.msgType = TO_LITTLEL(REMOTE_NDIS_RESET_CMPLT);
    pResetCmplt->hdr.msgLen = TO_LITTLEL(sizeof(RNDIS_RESET_CMPLT));
    pResetCmplt->status = TO_LITTLEL(RNDIS_STATUS_SUCCESS);
    pResetCmplt->addressingReset = 0;

    /* The replay length */

    pUsbTgtRndis->uReplyMsgLen = sizeof(RNDIS_RESET_CMPLT);

    /*
     * A reset does not affect Remote NDIS message communication with the
     * host. The device typically resets its network-side controller(s)
     * during this process.
     */

    usbTgtRndisManagementNotify(pUsbTgtRndis,
                                USBTGT_RNDIS_NOTIFY_DATA_CHANNEL_RESET,
                                NULL);

    USBTGT_RNDIS_DBG("usbTgtRndisMsgReset\n",
                      1, 2, 3, 4, 5, 6);

    /* Notify the host the reply is ready */

    return usbTgtRndisMsgAvailableNotify(pUsbTgtRndis);
    }


/******************************************************************************
*
* usbTgtRndisMsgKeepAlive - handle the RNDIS keep alive message request
*
* This routine handles the RNDIS keep alive message request from the host.
*
* From RNDIS Spec 1.0 chapter 3.7
* The host sends this message periodically when there has been no other
* control or data traffic from the device to the host for the bus-defined
* KeepAliveTimeoutPeriod.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO:N/A
*
* \NOMANUAL
*/
LOCAL STATUS usbTgtRndisMsgKeepAlive
    (
    pUSBTGT_RNDIS         pUsbTgtRndis,
    RNDIS_KEEPALIVE_MSG * pKeepAliveMsg
    )
    {
    RNDIS_KEEPALIVE_CMPLT * pKeepAliveCmplt = NULL;

    if ((NULL == pUsbTgtRndis) ||
        (NULL == pKeepAliveMsg))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pUsbTgtRndis) ? "pUsbTgtRndis" :
                         "pKeepAliveMsg"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    pKeepAliveCmplt = (RNDIS_KEEPALIVE_CMPLT *)&pUsbTgtRndis->replyMsgBuf[0];

    USBTGT_RNDIS_DBG("usbTgtRndisMsgKeepAlive prepare the replay\n",
                      1, 2, 3, 4, 5, 6);

    /*
     * The host sends this message periodically when there has been no other
     * control or data traffic from the device to the host for the bus-defined
     * KeepAliveTimeoutPeriod.
     */

    pKeepAliveCmplt->hdr.msgType = TO_LITTLEL(REMOTE_NDIS_KEEPALIVE_CMPLT);
    pKeepAliveCmplt->hdr.msgLen = TO_LITTLEL(sizeof(RNDIS_KEEPALIVE_CMPLT));
    pKeepAliveCmplt->requestId = pKeepAliveMsg->requestId;
    pKeepAliveCmplt->status = TO_LITTLEL(RNDIS_STATUS_SUCCESS);

    /* The replay length */

    pUsbTgtRndis->uReplyMsgLen = sizeof(RNDIS_RESET_CMPLT);

    /*
     * NOTE: The device dose not need to preform any specific action if it
     * get this message .
     */

    /* Notify the host the reply is ready */

    return usbTgtRndisMsgAvailableNotify(pUsbTgtRndis);
    }


/******************************************************************************
*
* usbTgtRndisMsgProcessCallback - the callback routine to process the control
* message get from the host.
*
* This routine is the callback routine to process the control message get from
* the host, as the user callback routine <' usbTgtRndisVendorSpecific'>'s
* <'SEND_ENCAPSULATED_COMMAND'>
*
* NOTE: this function driver can be used by different TCD. The control channel
* process will be different according to differnt hardware, which indicated
* by the <'pErp->result'>. The programer should submit
*
* RETURNS: OK, or ERROR if unable to process vendor-specific request
*
* ERRNO: N/A
*
* \NOMANUAL
*/
void usbTgtRndisMsgProcessCallback
    (
    void * pErpCallback
    )
    {
    pUSB_ERP              pErp = (pUSB_ERP) pErpCallback;
    RNDIS_MSG_HDR * pHdr = NULL;
    pUSBTGT_RNDIS         pUsbTgtRndis = NULL;
    STATUS                status = OK;

    /* Parameter validation */

    if ((NULL == pErp) ||
        (NULL == pErp->userPtr))
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         ((NULL == pErp) ? "pErpCallback" :
                         "pErp->userPtr"), 2, 3, 4, 5, 6);

        return;
        }

    /* Get the pTcd pointer */

    pUsbTgtRndis = (pUSBTGT_RNDIS)pErp->userPtr;

    pHdr = (RNDIS_MSG_HDR *) pUsbTgtRndis->requestMsgBuf;

    USBTGT_RNDIS_VDBG ("usbTgtRndisMsgProcess with msgType 0x%X \n",
                       FROM_LITTLEL(pHdr->msgType),
                        2, 3, 4, 5, 6);

    /*
     * This is the controll message, we always expect that,
     * there is only one pecket received.
     */

    if (pErp->bfrList[0].actLen != FROM_LITTLEL( pHdr->msgLen))
        {
        /* Stall default endpoints */

        usbTgtFuncDefaultPipeStall (pUsbTgtRndis->targChannel);

        status = usbTgtFuncControlSetupErpSubmit(pUsbTgtRndis->targChannel);

        USBTGT_RNDIS_ERR("Get request actLen 0x%X msgLen 0x%X,stall default "
                         "pipe and restart setup with %d\n",
                         pErp->bfrList[0].actLen,
                         FROM_LITTLEL( pHdr->msgLen),
                         status, 4, 5, 6);

        return;
        }

    /* Process the request get from the host */

    switch (FROM_LITTLEL(pHdr->msgType))
        {
        case REMOTE_NDIS_INITIALIZE_MSG:

            USBTGT_RNDIS_DBG("Get RNDIS_INITIALIZE_MSG\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisMsgInitialize(pUsbTgtRndis,
                                              (RNDIS_INITIALIZE_MSG *) pHdr);

            break;
        case REMOTE_NDIS_HALT_MSG:

            USBTGT_RNDIS_DBG("Get REMOTE_NDIS_HALT_MSG\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisMsgHalt(pUsbTgtRndis,
                                        (RNDIS_HALT_MSG *) pHdr);

            break;
        case REMOTE_NDIS_QUERY_MSG:

            USBTGT_RNDIS_DBG("Get REMOTE_NDIS_QUERY_MSG\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisMsgQuery(pUsbTgtRndis,
                                         (RNDIS_QUERY_MSG *) pHdr);

            break;
        case REMOTE_NDIS_SET_MSG:

            USBTGT_RNDIS_DBG("Get REMOTE_NDIS_SET_MSG\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisMsgSet(pUsbTgtRndis,
                                       (RNDIS_SET_MSG *) pHdr);

            break;
        case REMOTE_NDIS_RESET_MSG:

            USBTGT_RNDIS_DBG("Get REMOTE_NDIS_RESET_MSG\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisMsgReset(pUsbTgtRndis,
                                         (RNDIS_RESET_MSG *) pHdr);

            break;
        case REMOTE_NDIS_KEEPALIVE_MSG:

            USBTGT_RNDIS_DBG("Get REMOTE_NDIS_KEEPALIVE_MSG\n",
                             1, 2, 3, 4, 5, 6);

            status = usbTgtRndisMsgKeepAlive(pUsbTgtRndis,
                                             (RNDIS_KEEPALIVE_MSG *) pHdr);

            break;
        default:

            USBTGT_RNDIS_DBG("Unsupported RNDIS message 0x%08x\n",
                             FROM_LITTLEL(pHdr->msgType),
                             2, 3, 4, 5, 6);

            break;
        } /* end switch */

    if (status == ERROR)
        {
        /* Stall default endpoints */

        usbTgtFuncDefaultPipeStall (pUsbTgtRndis->targChannel);

        status = usbTgtFuncControlSetupErpSubmit(pUsbTgtRndis->targChannel);

        USBTGT_RNDIS_ERR("Process the control message error, reset setup request\n",
                          1, 2, 3, 4, 5, 6);

        return;
        }
    return;
    }


