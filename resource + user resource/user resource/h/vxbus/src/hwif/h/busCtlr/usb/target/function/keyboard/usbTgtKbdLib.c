/* usbTgtKbdLib.c - USB Target Keyboard Function Library module */

/*
 * Copyright (c) 2011-2013, 2015 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
Modification history
--------------------
01h,02feb15,lan  Modify for WCID support (VXW6-83565)
01g,06may13,s_z  Remove compiler warning (WIND00356717)
01f,06jan13,s_z  Remove compiler warning (WIND00371346)
01e,18may12,s_z  Add support for USB 3.0 target (WIND00326012) 
01d,13dec11,m_y  Modify according to code check result (WIND00319317)
01c,13sep11,ljg  Remove ctrl-z in usbTgtKbdReportTest (WIND00302728)
01b,04aug11,ljg  Add HID Desc and Report Desc and modify usbTgtKbdReportTest
                 (WIND00291023 WIND00291012)
01a,11apr11,ljg  written
*/

/*
DESCRIPTION

This module contains code to exercise the usbTgtLib by emulating a rudimentary
USB keyboard. This module defines those routines directly referenced by the USB
peripheral stack; namely, the routines that intialize the data structure.
Additional routines are also provided which are specific to the target keyboard
function driver.

INCLUDE FILES: usb/usbPlatform.h usb/usbHid.h usb/usbTgt.h usb/usbTgtKbd.h
               usbTgtFunc.h
*/

/* includes */

#include <usb/usbPlatform.h>
#include <usb/usbHid.h>
#include <usb/usbTgt.h>
#include <usb/usbTgtKbd.h>
#include <usbTgtFunc.h>

/* import */

IMPORT USR_USBTGT_KBD_CONFIG usrUsbTgtKbdConfigTable[];

/* The global list to record all the Kbd device */

LIST        gUsbTgtKbdList;
SEM_HANDLE  gUsbTgtKbdListSem;

/* descriptor definitions */

LOCAL UCHAR gUsbTgtKbdReportDescr[USBTGT_KBD_REPORT_DESCRIPTOR_LENGTH]= /* Keyboard Report Descriptor */
    {                         /* From Spec: Device Class Definition for Human Interface Devices (HID) */
    0x05, 0x01,               /* Usage Page (Generic Desktop),                                        */
    0x09, 0x06,               /* Usage (Keyboard),                                                    */
    0xA1, 0x01,               /* Collection (Application),                                            */
    0x05, 0x07,               /* Usage Page (Key Codes);                                              */
    0x19, 0xE0,               /* Usage Minimum (224),                                                 */
    0x29, 0xE7,               /* Usage Maximum (231),                                                 */
    0x15, 0x00,               /* Logical Minimum (0),                                                 */
    0x25, 0x01,               /* Logical Maximum (1),                                                 */
    0x75, 0x01,               /* Report Size (1),                                                     */
    0x95, 0x08,               /* Report Count (8),                                                    */
    0x81, 0x02,               /* Input (Data, Variable, Absolute), ;Modifier byte                     */
    0x95, 0x01,               /* Report Count (1),                                                    */
    0x75, 0x08,               /* Report Size (8),                                                     */
    0x81, 0x01,               /* Input (Constant), ;Reserved byte                                     */
    0x95, 0x05,               /* Report Count (5),                                                    */
    0x75, 0x01,               /* Report Size (1),                                                     */
    0x05, 0x08,               /* Usage Page (Page# for LEDs),                                         */
    0x19, 0x01,               /* Usage Minimum (1),                                                   */
    0x29, 0x05,               /* Usage Maximum (5),                                                   */
    0x91, 0x02,               /* Output (Data, Variable, Absolute), ;LED report                       */
    0x95, 0x01,               /* Report Count (1),                                                    */
    0x75, 0x03,               /* Report Size (3),                                                     */
    0x91, 0x01,               /* Output (Constant), ;LED report padding                               */
    0x95, 0x06,               /* Report Count (6),                                                    */
    0x75, 0x08,               /* Report Size (8),                                                     */
    0x15, 0x00,               /* Logical Minimum (0),                                                 */
    0x25, 0x65,               /* Logical Maximum(101),                                                */
    0x05, 0x07,               /* Usage Page (Key Codes),                                              */
    0x19, 0x00,               /* Usage Minimum (0),                                                   */
    0x29, 0x65,               /* Usage Maximum (101),                                                 */
    0x81, 0x00,               /* Input (Data, Array), ;Key arrays (6 bytes)                           */
    0xc0                      /* End Collection                                                       */
    };

LOCAL USB_INTERFACE_DESCR gUsbTgtKbdIfDescr =          /* Interface Descriptor */
    {
    USB_INTERFACE_DESCR_LEN,                           /* bLength */
    USB_DESCR_INTERFACE,                               /* bDescriptorType */
    USBTGT_KBD_INTERFACE_NUM,                          /* bInterfaceNumber */
    USBTGT_KBD_INTERFACE_ALT_SETTING,                  /* bAlternateSetting */
    USBTGT_KBD_NUM_ENDPOINTS,                          /* bNumEndpoints */
    USB_CLASS_HID,                                     /* bInterfaceClass */
    USB_SUBCLASS_HID_BOOT,                             /* bInterfaceSubClass */
    USB_PROTOCOL_HID_BOOT_KEYBOARD,                    /* bInterfaceProtocol */
    0                                                  /* iInterface */
    };

LOCAL USB_HID_DESCR gUsbTgtKbdHidDescr =               /* HID Descriptor */
    {
    USB_HID_DESCR_LEN,                                 /* bLength */
    USB_DESCR_HID,                                     /* bDescriptorType */
    TO_LITTLEW(USBTGT_KBD_BINARY_CODED_DECIMAL),       /* bcdHID */
    USBTGT_KBD_HARDWARE_TARGET_COUNTRY,                /* bCountryCode */
    USBTGT_KBD_DESCRIPTORS_NUMBER,                     /* bNumDescriptors */
        {
            {
            USB_DESCR_REPORT,                              /* bDescriptorType */
            TO_LITTLEW(USBTGT_KBD_REPORT_DESCRIPTOR_LENGTH)/* wDescriptorLength */
            }
        }
    };

LOCAL USB_ENDPOINT_DESCR gUsbTgtKbdIntInEpDescr =      /* Interrupt In Endpoint Descriptor */
    {
    USB_ENDPOINT_DESCR_LEN,                            /* bLength */  
    USB_DESCR_ENDPOINT,                                /* bDescriptorType */
    USBTGT_KBD_INT_IN_ENDPOINT_NUM,                    /* bEndpointAddress */
    USB_ATTR_INTERRUPT,                                /* bmAttributes */
    TO_LITTLEW(sizeof (HID_KBD_BOOT_REPORT)),          /* maxPacketSize */
    USBTGT_KBD_INTERRUPT_ENDPOINT_INTERVAL             /* bInterval */
    };

LOCAL USB_ENDPOINT_COMPANION_DESCR gUsbTgtKbdIntInCompDescr = /* companion descriptor */
    {
    USB_ENDPOINT_COMP_DESCR_LEN,                 /* bLength */
    USB_DESCR_SS_ENDPOINT_COMPANION,             /* bDescriptorType */
    0x0,                                         /* bMaxBurst */
    0x0,                                         /* bmAttributes */
    0x0                                          /* wBytesPerInterval */
    };

LOCAL pUSB_DESCR_HDR gUsbTgtKbdOtherSpeedDescHdr[] =
    {
    (pUSB_DESCR_HDR )&gUsbTgtKbdIfDescr,               /* Interface descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtKbdHidDescr,              /* HID descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtKbdIntInEpDescr,          /* Endpoint descriptor */
    NULL                                               /*  */
    };

LOCAL pUSB_DESCR_HDR gUsbTgtKbdHighSpeedDescHdr[] =
    {
    (pUSB_DESCR_HDR )&gUsbTgtKbdIfDescr,               /* Interface descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtKbdHidDescr,              /* HID descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtKbdIntInEpDescr,          /* Endpoint descriptor */
    NULL                                               /*  */
    };

LOCAL pUSB_DESCR_HDR gUsbTgtKbdSuperSpeedDescHdr[] =
    {
    (pUSB_DESCR_HDR )&gUsbTgtKbdIfDescr,               /* Interface descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtKbdHidDescr,              /* HID descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtKbdIntInEpDescr,          /* Endpoint descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtKbdIntInCompDescr,        /* Endpoint comp desc */
    NULL                                               /* END */
    };

/* locals */

LOCAL STATUS usbTgtKbdMngmtFunc 
    (
    pVOID               param, 
    USB_TARG_CHANNEL    targChannel,
    UINT16              mngmtCode, 
    pVOID               pContext
    );

LOCAL STATUS usbTgtKbdFeatureClear 
    (
    pVOID               param, 
    USB_TARG_CHANNEL    targChannel,
    UINT8               requestType, 
    UINT16              feature,
    UINT16              index
    );

LOCAL STATUS usbTgtKbdFeatureSet 
    (
    pVOID               param, 
    USB_TARG_CHANNEL    targChannel,
    UINT8               requestType, 
    UINT16              feature, 
    UINT16              index
    );

LOCAL STATUS usbTgtKbdConfigurationSet 
    (
    pVOID               param, 
    USB_TARG_CHANNEL    targChannel,
    UINT8               configuration
    );

LOCAL STATUS usbTgtKbdDescriptorGet
    (
    pVOID            param,
    USB_TARG_CHANNEL targChannel,
    UINT8            requestType, 
    UINT8            descriptorType,
    UINT8            descriptorIndex,
    UINT16           languageId,
    UINT16           length,
    pUINT8           pBfr,
    pUINT16          pActLen
    );

LOCAL STATUS usbTgtKbdInterfaceGet 
    (
    pVOID               param, 
    USB_TARG_CHANNEL    targChannel,
    UINT16              interfaceIndex, 
    pUINT8              pAlternateSetting
    );

LOCAL STATUS usbTgtKbdInterfaceSet 
    (
    pVOID               param, 
    USB_TARG_CHANNEL    targChannel,
    UINT16              interfaceIndex, 
    UINT8               alternateSetting
    );

LOCAL STATUS usbTgtKbdVendorSpecific 
    (
    pVOID               param, 
    USB_TARG_CHANNEL    targChannel,
    UINT8               requestType, 
    UINT8               request, 
    UINT16              value,
    UINT16              index, 
    UINT16              length
    );

LOCAL USB_TARG_CALLBACK_TABLE gUsbTgtKbdCallbackTable =  /* Callback Table */
    {
    usbTgtKbdMngmtFunc,                                  /* usbTgtKbdMngmtFunc */
    usbTgtKbdFeatureClear,                               /* usbTgtKbdFeatureClear */
    usbTgtKbdFeatureSet,                                 /* usbTgtKbdFeatureSet */
    NULL,                                                /* usbTgtKbdConfigurationGet */
    usbTgtKbdConfigurationSet,                           /* usbTgtKbdConfigurationSet */
    usbTgtKbdDescriptorGet,                              /* usbTgtKbdDescriptorGet */
    NULL,                                                /* descriptorSet */
    usbTgtKbdInterfaceGet,                               /* usbTgtKbdInterfaceGet */
    usbTgtKbdInterfaceSet,                               /* usbTgtKbdInterfaceSet */
    NULL,                                                /* statusGet */
    NULL,                                                /* addressSet */
    NULL,                                                /* synchFrameGet */
    usbTgtKbdVendorSpecific                              /* usbTgtKbdVendorSpecific */
    };

LOCAL UCHAR    pUsbKbdBuf[2];

LOCAL BOOL     gUsbTgtKbdInited = FALSE;

#define USBTGT_KBD_TEST

#ifdef USBTGT_KBD_TEST
pUSBTGT_KBD_DEV   pUsbTgtKbdTest = NULL;
#define USBTGT_KBD_TEST_LEN                            30
#define USBTGT_KBD_TEST_ATO0_LEN                       36

/* Key value table spec: USB HID Usage Tables */

#define USBTGT_KBD_TEST_KEYCODE_A                      0x04
#define USBTGT_KBD_TEST_KEYCODE_1                      0x1E
#define USBTGT_KBD_TEST_KEYCODE_0                      0x27
#define USBTGT_KBD_TEST_KEYCODE_MODIFIER_LEFTCTRLDOWN  0x01
#define USBTGT_KBD_TEST_KEYCODE_MODIFIER_LEFTSHIFTDOWN 0x02

/* ASCII key definitions */

#define BS         8                     /* backspace, CTRL-H */
#define DEL        BS                    /* delete key */
#define TAB        9                     /* tab, CTRL-I */
#define CR         0x0A                  /* carriage return, CTRL-M */
#define ESC        27                    /* escape */

/* key value table without shfit down */

LOCAL char pUSKeyMapTableOriginal[] =
    {
      0,    0,    0,    0,  'a',  'b',  'c',  'd',  'e',  'f',
    'g',  'h',  'i',  'j',  'k',  'l',  'm',  'n',  'o',  'p',
    'q',  'r',  's',  't',  'u',  'v',  'w',  'x',  'y',  'z',
    '1',  '2',  '3',  '4',  '5',  '6',  '7',  '8',  '9',  '0',  
     CR,  ESC,  DEL,  TAB,  ' ',  '-',  '=',  '[',  ']', '\\',
      0,  ';', '\'',  '`',  ',',  '.',  '/',    0
    };

/* key value table with shfit down */

LOCAL char pUSKeyMapTableShift[] =
    {
      0,    0,    0,    0,  'A',  'B',  'C',  'D',  'E',  'F',
    'G',  'H',  'I',  'J',  'K',  'L',  'M',  'N',  'O',  'P',
    'Q',  'R',  'S',  'T',  'U',  'V',  'W',  'X',  'Y',  'Z',
    '!',  '@',  '#',  '$',  '%',  '^',  '&',  '*',  '(',  ')',   
     CR,  ESC,  DEL,  TAB,  ' ',  '_',  '+',  '{',  '}',  '|',
    '~',  ':', '\"',  '~',  '<',  '>',  '?',    0
    };
#endif /* USBTGT_KBD_TEST */

/* functions */

/*******************************************************************************
*
* usbTgtKbdDestroy - destroy one pUsbTgtKbd resource
*
* This routine destroys one pUsbTgtKbd resource which alloced by the 
* usbTgtKbdCreate routine.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbTgtKbdDestroy
    (
    pUSBTGT_KBD_DEV   pUsbTgtKbd
    )
    {
    /* Parameter has been checked by the caller */
    /* Delete the Interrupt In Mutex */

    if (OSS_SEM_TAKE (pUsbTgtKbd->intInErpSem, USBTGT_WAIT_TIMEOUT) == OK)
        {
        /* Mutex should be taken before delete it */
        
        OSS_SEM_DESTROY (pUsbTgtKbd->intInErpSem);
        pUsbTgtKbd->intInErpSem = NULL;
        }
    
    USBTGT_KBD_VDBG ("usbTgtKbdDestroy the dev 0x%X successfully.\n",
                     pUsbTgtKbd, 2, 3, 4, 5, 6);
    
    OSS_FREE(pUsbTgtKbd);

    return;
    }

/*******************************************************************************
*
* usbTgtKbdCreate - create one new Kbd device
*
* This routine creates one new Kbd device.
*
* RETURNS: NULL or the pUSBTGT_KBD_DEV
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL pUSBTGT_KBD_DEV usbTgtKbdCreate
    (
    void
    )
    {
    pUSBTGT_KBD_DEV      pUsbTgtKbd = NULL;

    pUsbTgtKbd = (pUSBTGT_KBD_DEV)OSS_CALLOC(sizeof (USBTGT_KBD_DEV));

    if (NULL == pUsbTgtKbd)
        {
        USBTGT_KBD_ERR("No enough memory to melloc for the Kbd resource.\n",
                       1, 2, 3, 4, 5 ,6);

        return NULL;
        }

    /* Create the Interrupt In Erp Mutex */

    if (OSS_SEM_CREATE( 1, 1, &pUsbTgtKbd->intInErpSem) != OK)
        {
        USBTGT_KBD_ERR ("Create intInErpSem failed.\n",
                        1, 2, 3, 4, 5, 6);

        usbTgtKbdDestroy(pUsbTgtKbd);

        return NULL;
        }

    /* Add to the global list */
  
    (void) OSS_SEM_TAKE (gUsbTgtKbdListSem, USBTGT_WAIT_TIMEOUT);

    lstAdd (&gUsbTgtKbdList, &pUsbTgtKbd->kbdNode);

    OSS_SEM_GIVE (gUsbTgtKbdListSem);

    USBTGT_KBD_VDBG ("usbTgtKbdCreate the dev 0x%X.\n", 
                     pUsbTgtKbd, 2, 3, 4, 5, 6);

    return pUsbTgtKbd;
    }

/*******************************************************************************
*
* usbTgtKbdUnInit - uninitialize the Kbd driver and destroy resource
*
* This routine uninitializes the Kbd driver and destroy resource which created
* before.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbTgtKbdUnInit
    (
    void
    )
    {
    pUSBTGT_KBD_DEV    pUsbTgtKbd = NULL;
    STATUS             status     = ERROR;
    int                index;
    int                count;

    /* Get the count of the global Kbd device list */

    (void) OSS_SEM_TAKE (gUsbTgtKbdListSem, USBTGT_WAIT_TIMEOUT);

    count = lstCount(&gUsbTgtKbdList);

    for (index = 1; index <= count; index ++)
        {
        pUsbTgtKbd = (pUSBTGT_KBD_DEV)lstNth(&gUsbTgtKbdList, index);

        if (NULL != pUsbTgtKbd)
            {

            /* Un-register from the TML */
            
            (void)usbTgtFuncUnRegister(pUsbTgtKbd->targChannel);
            
            /* Destroy the kbd data structure */

            usbTgtKbdDestroy(pUsbTgtKbd);

            /* Delete the resource from the global list */

            if (ERROR != lstFind(&gUsbTgtKbdList, &pUsbTgtKbd->kbdNode))
                {
                lstDelete(&gUsbTgtKbdList, &pUsbTgtKbd->kbdNode);
                status = OK;
                }
            else
                {
                USBTGT_KBD_ERR("lstFind failed.\n", 1, 2, 3, 4, 5 ,6);
                status = ERROR;                    
                break;
                }
            }

        else
            USBTGT_KBD_ERR("pUsbTgtKbd is NULL.\n", 1, 2, 3, 4, 5 ,6);
        }

    OSS_SEM_GIVE (gUsbTgtKbdListSem);

    gUsbTgtKbdInited = FALSE;

    return status;
    }

/*******************************************************************************
*
* usbTgtKbdInit - initialize the Kbd struct for devices
*
* This routine initializes the struct for devices.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbTgtKbdInit
    (
    int devCount
    )
    {
    pUSBTGT_KBD_DEV       pUsbTgtKbd  = NULL;
    USBTGT_FUNC_INFO      usbTgtFuncInfo;
    int                   index;
    
    /* This module has been initialized */
    
    if (gUsbTgtKbdInited == TRUE)
        return OK;

    /* Init the global Kbd devcie list */

    if (OSS_SEM_CREATE( 1, 1, &gUsbTgtKbdListSem) != OK)
        {
        USBTGT_KBD_ERR ("Create gUsbTgtKbdListSem failed.\n",
                        1, 2, 3, 4, 5, 6);

        return ERROR;
        }


    lstInit(&gUsbTgtKbdList);

    /* Allocal memory resource */

    for (index = 0; index < devCount; index ++)
        {
        pUsbTgtKbd = usbTgtKbdCreate();

        if (pUsbTgtKbd == NULL)
            {
            USBTGT_KBD_ERR("Create new Kbd device fail.\n", 1, 2, 3, 4, 5 ,6);

            usbTgtKbdUnInit();

            return ERROR;
            }

        usbTgtFuncInfo.ppFsFuncDescTable = 
            (USB_DESCR_HDR **)gUsbTgtKbdOtherSpeedDescHdr;
        usbTgtFuncInfo.ppHsFuncDescTable = 
            (USB_DESCR_HDR **)gUsbTgtKbdHighSpeedDescHdr;
        usbTgtFuncInfo.ppSsFuncDescTable = 
            (USB_DESCR_HDR **)gUsbTgtKbdSuperSpeedDescHdr;
        usbTgtFuncInfo.pFuncCallbackTable =
            (USB_TARG_CALLBACK_TABLE *)&gUsbTgtKbdCallbackTable;

        usbTgtFuncInfo.pCallbackParam = (pVOID)pUsbTgtKbd;
        usbTgtFuncInfo.pFuncSpecific = (pVOID)pUsbTgtKbd;

        usbTgtFuncInfo.pFuncName = usrUsbTgtKbdConfigTable[index].kbdName;
        usbTgtFuncInfo.pTcdName = usrUsbTgtKbdConfigTable[index].tcdName;
        usbTgtFuncInfo.uFuncUnit = usrUsbTgtKbdConfigTable[index].uKbdUnit;
        usbTgtFuncInfo.uTcdUnit = usrUsbTgtKbdConfigTable[index].uTcdUnit;
        usbTgtFuncInfo.uConfigToBind = 
            usrUsbTgtKbdConfigTable[index].uConfigNum;
        usbTgtFuncInfo.pWcidString = NULL;
        usbTgtFuncInfo.pSubWcidString = NULL;
        usbTgtFuncInfo.uWcidVc = 0;

        /* Register to the TML */

        pUsbTgtKbd->targChannel = usbTgtFuncRegister(&usbTgtFuncInfo);

        if (USBTGT_TARG_CHANNEL_DEAD == pUsbTgtKbd->targChannel)
            {
            USBTGT_KBD_ERR("Register to the TML fail\n",
                             1, 2, 3, 4, 5, 6);
            
            (void)usbTgtKbdDestroy (pUsbTgtKbd);
            
            return ERROR;
            }       

        }

#ifdef USBTGT_KBD_TEST
    pUsbTgtKbdTest = pUsbTgtKbd;
#endif 

    gUsbTgtKbdInited = TRUE;

    USBTGT_KBD_DBG("usbTgtKbdInit successfully.\n", 1, 2, 3, 4, 5 ,6);

    return OK;
    }

/*******************************************************************************
*
* usbTgtKbdIntInErpCallback - process the status when interrupt pipe terminate
*
* This callback is invoked when an Endpoint Request Packet terminates. It
* states that no more ERP reports are pending.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbTgtKbdIntInErpCallback
    (
    pVOID               erp                     /* USB_ERP endpoint request packet */
    )
    {
    USB_ERP *           pErp = (USB_ERP *)erp;  /* USB_ERP */
    pUSBTGT_KBD_DEV     pUsbTgtKbd;

    /* check parameter */

    if ((NULL == pErp) || 
        (NULL == pErp->userPtr))
        {
        USBTGT_KBD_ERR("Invalid parameter %s is NULL.\n",
                       (ULONG)((NULL == pErp) ? 
                       "pErp" : (NULL == pErp->userPtr) ? "userPtr" : "NULL"), 
                       2, 3, 4, 5, 6);

        return;
        }

    pUsbTgtKbd = pErp->userPtr;

    pUsbTgtKbd->bIntInErpUsed = FALSE;
    
    return;
    }

/*******************************************************************************
*
* usbTgtKbdIntInErpReport - report data to host by intIn pipe
*
* This function injects the boot report into the interrupt pipe. <pReport> is
* the pointer to the boot report which to be injected. 
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbTgtKbdIntInErpReport
    (
    pUSBTGT_KBD_DEV       pUsbTgtKbd,
    pHID_KBD_BOOT_REPORT  pReport,       /* Boot Report to be injected */
    UINT16                reportLen      /* Length of the boot report */
    )
    {
    /* If the report pipe isn't enabled, return an error. */

    if (NULL == pUsbTgtKbd)
        {
        USBTGT_KBD_ERR("Invalid parameter %s is NULL.\n",
                       "pUsbTgtKbd", 2, 3, 4, 5, 6);

        return ERROR;
        }

    while (pUsbTgtKbd->bIntInErpUsed)
        OSS_THREAD_SLEEP(1);

    pUsbTgtKbd->bIntInErpUsed = TRUE;

    /* Copy the report and set up the transfer. */

    reportLen = (UINT16)min (sizeof (pUsbTgtKbd->intInBfr), reportLen);

    memset (&pUsbTgtKbd->intInErp, 0, sizeof (USB_ERP));
    memcpy (&pUsbTgtKbd->intInBfr, pReport, reportLen);

    pUsbTgtKbd->intInErp.erpLen = sizeof (USB_ERP);
    pUsbTgtKbd->intInErp.userCallback = usbTgtKbdIntInErpCallback;
    pUsbTgtKbd->intInErp.targCallback = usbTgtErpCallback;
    pUsbTgtKbd->intInErp.bfrCount = 1;
    pUsbTgtKbd->intInErp.bfrList [0].pid = USB_PID_IN;
    pUsbTgtKbd->intInErp.bfrList [0].pBfr = (pUINT8) &pUsbTgtKbd->intInBfr;
    pUsbTgtKbd->intInErp.bfrList [0].bfrLen = reportLen;
    pUsbTgtKbd->intInErp.userPtr = pUsbTgtKbd;

    if (usbTgtSubmitErp (pUsbTgtKbd->intInPipeHandle,
                         &pUsbTgtKbd->intInErp) != OK)
        {
        pUsbTgtKbd->bIntInErpUsed = FALSE;
        USBTGT_KBD_ERR("usbTgtSubmitErp error in USB_PID_IN\n",
                       1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    return OK;
    }

/*******************************************************************************
*
* usbTgtKbdFeatureClear - clear the specified feature
*
* This routine implements the clear feature standard device request.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtKbdFeatureClear
    (
    pVOID               param,          /* TCD specific parameter */
    USB_TARG_CHANNEL    targChannel,    /* target channel */
    UINT8               requestType,    /* request type */
    UINT16              feature,        /* feature to clear */
    UINT16              index           /* index */
    )
    {
    pUSBTGT_KBD_DEV     pUsbTgtKbdDev = (pUSBTGT_KBD_DEV) param;
    USB_TARG_PIPE       pipeHandle = NULL;
    UINT8               uInterface;
    UINT8               uSuspendOp;
    
    /* check parameter */

    if (pUsbTgtKbdDev == NULL)
        {
        USBTGT_KBD_ERR("parameter error.\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* This request must be standard request from the host */

    if (((requestType & USB_RT_DIRECTION_MASK) != USB_RT_HOST_TO_DEV) ||
        ((requestType & USB_RT_CATEGORY_MASK) != USB_RT_STANDARD))
        {
        USBTGT_KBD_ERR("unsupport requestType.\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }
  
    requestType = (UINT8) (requestType & ~(USB_RT_DIRECTION_MASK | USB_RT_CATEGORY_MASK));

    if ((requestType == USB_RT_ENDPOINT) &&
        (feature == USB_FSEL_DEV_ENDPOINT_HALT))
        {
        USBTGT_KBD_DBG("Clear ENDPOINT_HALT Feature Ep index 0x%X\n", index,
                       2, 3, 4, 5, 6);

        pipeHandle = usbTgtFuncFindPipeHandleByEpIndex(pUsbTgtKbdDev->targChannel,
                                              (UINT8) (index & 0xFF));

        if (pipeHandle == pUsbTgtKbdDev->intInPipeHandle)
            {
            pUsbTgtKbdDev->uIntInPipeStatus = USBTGT_KBD_PIPE_UNSTALL;
      
            USBTGT_KBD_DBG("Set Feature is called to inform the interrupt in "
                           "pipe 0x%X with EpIndex 0x%X has been unstalled.\n",
                           (ULONG)pipeHandle, index, 3, 4, 5, 6);
            }        
        else
            {
            USBTGT_KBD_ERR("pUsbTgtPipe 0x%X is not Kbd interrupt pipe.\n", 
                           (ULONG)pipeHandle, index, 3, 4, 5, 6);
            return ERROR;
            }
        }
    else if ((requestType == USB_RT_INTERFACE) && 
             (feature == USB_FSEL_DEV_FUNCTION_SUSPEND))
        {
        /* The MSB is the specific suspend options */
        
        uSuspendOp = (UINT8) (index >> 8);
        uInterface = (UINT8) (index & 0xFF);
        
        USBTGT_KBD_DBG ("Clear interface %d suspendop %x\n",
                        uInterface, uSuspendOp, 3, 4, 5, 6);
        
        return OK;
        }

    USBTGT_KBD_VDBG("Clear Feature is called to inform the pipe 0x%X with "
                    "EpIndex 0x%X unstalled. \n", (ULONG)pipeHandle, index,
                    3, 4, 5, 6);
    return OK;
    }
    
/*******************************************************************************
*
* usbTgtKbdFeatureSet - set the specified feature
*
* This routine implements the set feature standard device request.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtKbdFeatureSet
    (
    pVOID               param,          /* TCD specific parameter */
    USB_TARG_CHANNEL    targChannel,    /* target channel */
    UINT8               requestType,    /* request type */
    UINT16              feature,        /* feature to set */
    UINT16              index           /* wIndex */
    )
    {
    pUSBTGT_KBD_DEV     pUsbTgtKbdDev = (pUSBTGT_KBD_DEV) param;
    USB_TARG_PIPE       pipeHandle = NULL;
    UINT8               uInterface;
    UINT8               uSuspendOp;

    /* check parameter */

    if (pUsbTgtKbdDev == NULL)
        {
        USBTGT_KBD_ERR("parameter error.\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }
    
    /* This request must be standard request from the host */

    if (((requestType & USB_RT_DIRECTION_MASK) != USB_RT_HOST_TO_DEV) ||
        ((requestType & USB_RT_CATEGORY_MASK) != USB_RT_STANDARD))
        {
        USBTGT_KBD_ERR("unsupport requestType.\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    requestType = (UINT8) (requestType & (~(USB_RT_DIRECTION_MASK | USB_RT_CATEGORY_MASK)));

    if ((requestType == USB_RT_ENDPOINT) &&
        (feature == USB_FSEL_DEV_ENDPOINT_HALT))
        {
        USBTGT_KBD_DBG("Clear ENDPOINT_HALT Feature Ep index 0x%X\n",
                       index, 2, 3, 4, 5, 6);

        pipeHandle = usbTgtFuncFindPipeHandleByEpIndex(pUsbTgtKbdDev->targChannel,
                                        (UINT8) (index & 0xFF));

        if (pipeHandle == pUsbTgtKbdDev->intInPipeHandle)
            {
            pUsbTgtKbdDev->uIntInPipeStatus = USBTGT_KBD_PIPE_STALL;

            USBTGT_KBD_DBG("Set Feature is called to inform the interrupt in "
                           "pipe 0x%X with EpIndex 0x%X has been stalled.\n",
                           (ULONG)pipeHandle, index, 3, 4, 5, 6);
            }
        else
            {
            USBTGT_KBD_ERR("pUsbTgtPipe 0x%X is not Kbd interrupt in pipe.\n",
                           (ULONG)pipeHandle, index, 3, 4, 5, 6);
            return ERROR;
            }

        }
    else if ((requestType == USB_RT_INTERFACE) && 
             (feature == USB_FSEL_DEV_FUNCTION_SUSPEND))
        {
        /* The MSB is the specific suspend options */
        
        uSuspendOp = (UINT8) (index >> 8);
        uInterface = (UINT8) (index & 0xFF);
        
        USBTGT_KBD_DBG ("Set interface %d suspendop %x\n",
                        uInterface, uSuspendOp, 3, 4, 5, 6);
        
        return OK;
        }

    USBTGT_KBD_DBG("requestType = 0x%X feature = 0x%X.\n", requestType,
                   feature, 3, 4, 5, 6);

    return OK;
    }

/*******************************************************************************
*
* usbTgtKbdInterfaceGet - get the specified interface
*
* This routine is used to get the selected alternate setting of the
* specified interface.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtKbdInterfaceGet
    (
    pVOID               param,              /* TCD specific parameter */
    USB_TARG_CHANNEL    targChannel,        /* target channel */
    UINT16              interfaceIndex,     /* interface index */
    pUINT8              pAlternateSetting   /* alternate setting */
    )
    {
    pUSBTGT_KBD_DEV     pUsbTgtKbdDev = (pUSBTGT_KBD_DEV) param;

    /* check parameter */

    if (pUsbTgtKbdDev == NULL)
        {
        USBTGT_KBD_ERR("parameter error.\n", 1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    if (pUsbTgtKbdDev->uConfigurationValue == 0)
        {
        USBTGT_KBD_ERR("configuration not set.\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    USBTGT_KBD_DBG("usbTgtKbdInterfaceSet(): alternateSetting = %d.\n",  
                   pUsbTgtKbdDev->uAlternateSetting, 2, 3, 4, 5, 6);

    *pAlternateSetting = pUsbTgtKbdDev->uAlternateSetting;

    return OK;
    }

/*******************************************************************************
*
* usbTgtKbdInterfaceSet - set the specified interface
*
* This routine is used to select the alternate setting of he specified
* interface.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtKbdInterfaceSet
    (
    pVOID               param,              /* TCD specific parameter */
    USB_TARG_CHANNEL    targChannel,        /* target channel */
    UINT16              interfaceIndex,     /* interface index */
    UINT8               alternateSetting    /* alternate setting */
    )
    {
    pUSBTGT_KBD_DEV     pUsbTgtKbdDev = (pUSBTGT_KBD_DEV) param;

    /* check parameter */

    if (pUsbTgtKbdDev == NULL)
        {
        USBTGT_KBD_ERR("parameter error\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    if (pUsbTgtKbdDev->uConfigurationValue == 0)
        {
        USBTGT_KBD_ERR("configuration not set\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    USBTGT_KBD_DBG("usbTgtKbdInterfaceSet(): alternateSetting = %d.\n",  
                   alternateSetting, 2, 3, 4, 5, 6);

    pUsbTgtKbdDev->uAlternateSetting = alternateSetting;

    return OK;
    }

/******************************************************************************
*
* usbkbdSetReportCallback - handle the USB_REQ_HID_SET_REPORT request
*
* This function is invoked when USB_REQ_HID_SET_REPORT vendor specific
* request is received from the host. It acts as a dummy.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbTgtKbdSetReportCallback
    (
    pVOID        pErp           /* Pointer to ERP structure */
    )
    {
    /* Do nothing for this time. */
    
    return;
    }
    
/*******************************************************************************
*
* usbTgtKbdVendorSpecific - handle the VENDOR_SPECIFIC(Class-Specific) request
*
* This routine implements the vendor specific standard device request.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtKbdVendorSpecific
    (
    pVOID               param,          /* TCD specific parameter */
    USB_TARG_CHANNEL    targChannel,    /* target channel */
    UINT8               requestType,    /* request type */
    UINT8               request,        /* request name */
    UINT16              value,          /* wValue */
    UINT16              index,          /* wIndex */
    UINT16              length          /* wLength */
    )
    {
    pUSBTGT_KBD_DEV     pUsbTgtKbdDev = (pUSBTGT_KBD_DEV) param;

    /* check parameter */

    if (pUsbTgtKbdDev == NULL ||
        pUsbTgtKbdDev->targChannel != targChannel)
        {
        USBTGT_KBD_ERR("parameter error\n", 1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    USBTGT_KBD_DBG("usbTgtKbdVendorSpecific(): requestType %p request %p "
                   "value %p index %p length %p\n", requestType, request, 
                   value, index, length, 6);

    if (requestType == (USB_RT_HOST_TO_DEV | USB_RT_CLASS | USB_RT_INTERFACE))
        {
        switch (request)
            {
            case USB_REQ_HID_SET_PROTOCOL:
            case USB_REQ_HID_SET_IDLE:

                /* Send control transfer status to the host */

                usbTgtControlStatusSend (targChannel, FALSE);
                break;

            case USB_REQ_HID_SET_REPORT:

                /* Send the request to receive the data from the host */

                usbTgtControlPayloadRcv (targChannel, 1, pUsbKbdBuf,
                                         usbTgtKbdSetReportCallback);
                break;
                
            default:
                USBTGT_KBD_ERR("parameter request 0x%x.\n", request, 
                               2, 3, 4, 5, 6);
                return ERROR;
            }
        }

    return OK;
    }

/*******************************************************************************
*
* usbTgtKbdDestroyAllPipes - abort pending requests and destroy for all pipes
*
* This routine is used to destroy all pipes. Before destroy the pipes, all 
* requests pending for these pipes are also aborted.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtKbdDestroyAllPipes
    (
    pUSBTGT_KBD_DEV   pUsbTgtKbdDev
    )
    {
    /* If the interrupt in pipe is in use, try to clear it */

    if (pUsbTgtKbdDev->bIntInErpUsed == TRUE)
        {
        if (usbTgtCancelErp(pUsbTgtKbdDev->intInPipeHandle,
                           &pUsbTgtKbdDev->intInErp) != OK)
            {
            USBTGT_KBD_ERR("usbTgtCancelErp failed for Interrupt In.\n",
                           1, 2, 3, 4, 5, 6);
            return ERROR;
            }

        pUsbTgtKbdDev->bIntInErpUsed = FALSE;
        }

    /* Destroy the interrupt pipe if already created */

    if (pUsbTgtKbdDev->intInPipeHandle != NULL)
        {
        usbTgtDeletePipe (pUsbTgtKbdDev->intInPipeHandle);
        pUsbTgtKbdDev->intInPipeHandle = NULL;
        pUsbTgtKbdDev->bIntInErpUsed = FALSE;
        }

    return OK;
    }


/*******************************************************************************
*
* usbTgtKbdCreateAllPipes - create all pipes needed by this module
*
* This routine is used to create all pipes needed by this module.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtKbdCreateAllPipes
    (
    pUSBTGT_KBD_DEV   pUsbTgtKbdDev,
    UINT8             uConfig
    )
    {
    pUSB_ENDPOINT_DESCR      pEpDesc = NULL;
    USBTGT_PIPE_CONFIG_PARAM ConfigParam;
    UINT8                    uIfBase;

    /* Validate parameters */

    if (NULL == pUsbTgtKbdDev)
        {
        USBTGT_KBD_ERR("Invalid parameter pUsbTgtKbdDev is NULL\n",
                       1, 2, 3, 4, 5, 6);
        
        return ERROR;
        }
    
    /* 
     * This function driver may be part of one compiste device 
     * Get the interface base.
     */
     
    if ( ERROR == usbTgtFuncInfoMemberGet(pUsbTgtKbdDev->targChannel,
                                     USBTGT_FUNC_INFO_MEM_TYPE_uIfNumMin,
                                     &uIfBase))
        {
        USBTGT_KBD_ERR("Can not get the interface base number\n",
                        1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Find the right endpoint descriptor */
    
    pEpDesc = usbTgtFuncEndpointDescFind (pUsbTgtKbdDev->targChannel,
                          uIfBase,            /* Only one interface */
                          0,                  /* Altsetting 0 */
                          USB_ATTR_INTERRUPT, /* Interrupt */
                          USB_ENDPOINT_IN,    /* IN */
                          0,                  /* Synch Type */
                          0,                  /* Usage */
                          1);                 /* The first one */

    /* Create the pipe for the endpoint */
    
    if (usbTgtCreatePipe (pUsbTgtKbdDev->targChannel, 
                          pEpDesc,
                          uConfig, 
                          uIfBase,
                          0,
                          &pUsbTgtKbdDev->intInPipeHandle) != OK)
        {
        USBTGT_KBD_ERR("Create for interrupt pipe failed.\n",
                       1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Set the configuration parameters */
    
    ConfigParam.uMaxErpCount = 1;
    ConfigParam.uMaxErpBufCount = 1;
    ConfigParam.uMaxErpBufSize = HID_BOOT_REPORT_MAX_LEN;
    
    /* Configure the pipe */

    if (ERROR ==  usbTgtConfigPipe(pUsbTgtKbdDev->intInPipeHandle, 
                                   &ConfigParam))
        {
        USBTGT_KBD_ERR("Configure for interrupt pipe failed.\n",
                       1, 2, 3, 4, 5, 6);

        usbTgtKbdDestroyAllPipes(pUsbTgtKbdDev);

        return ERROR;
        }

    return OK;
    }

/*******************************************************************************
*
* usbTgtKbdConfigurationSet - set the specified configuration
*
* This routine is used to set the current configuration to the configuration
* value sent by host. <configuration> consists of the value to set.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtKbdConfigurationSet
    (
    pVOID                 param,          /* TCD specific parameter */
    USB_TARG_CHANNEL      targChannel,    /* target channel */
    UINT8                 configuration   /* configuration value to set */
    )
    {
    pUSBTGT_KBD_DEV       pUsbTgtKbdDev = (pUSBTGT_KBD_DEV) param;

    /* check parameter */

    if (pUsbTgtKbdDev == NULL ||
        pUsbTgtKbdDev->targChannel != targChannel )
        {
        USBTGT_KBD_ERR("parameter error\n", 1, 2, 3, 4, 5, 6);

        return ERROR ;
        }

    if (configuration == 0)
        {
        USBTGT_KBD_DBG("Reset to the config 0, delete all created pipes.\n",
                       1, 2, 3, 4, 5, 6);

        usbTgtKbdDestroyAllPipes(pUsbTgtKbdDev);
        }
    else 
        {
        /* if KBD was configured as multi-configuration device. The 
         * configuration may not be keyboard config value. 
         */

        USBTGT_KBD_DBG("configuration = 0x%x.", configuration, 2, 3, 4, 5, 6);

        usbTgtKbdDestroyAllPipes(pUsbTgtKbdDev);

        if (usbTgtKbdCreateAllPipes(pUsbTgtKbdDev, configuration) == ERROR)
            {
        usbTgtKbdDestroyAllPipes(pUsbTgtKbdDev);

            return ERROR;
            }                
        }
    pUsbTgtKbdDev->uConfigurationValue = configuration;

    return OK;
    }

/*******************************************************************************
*
* usbTgtKbdDescriptorGet - handle the descriptors get request 
*
* This routine handles the Descriptor Get request get from the target
* management layer. 
*
* NOTE: Most descriptors get request has been handled by the target management
* level. But it is still useful, such as get the USB_DESCR_REPORT descriptors.
*
* NOTE: we used the <'param'> to record the <'pUSBTGT_KBD_DEV'> pointer, and 
* also have the <'targChannel'> parameter even it is not used to be compatible
* with the USB GEN1 target stack.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtKbdDescriptorGet
    (
    pVOID            param,          /* TCD specific parameter */
    USB_TARG_CHANNEL targChannel,    /* target chennel */
    UINT8            requestType,    /* request type */
    UINT8            descriptorType, /* descriptor type */
    UINT8            descriptorIndex,/* descriptor index */
    UINT16           languageId,     /* language id */
    UINT16           length,         /* length of descriptor */
    pUINT8           pBfr,           /* buffer to hold the descriptor */
    pUINT16          pActLen         /* actual length */
    )
    {
    /* Determine type of descriptor being requested. */

    switch(descriptorType)
        {
        case USB_DESCR_REPORT:
            bcopy((const char *)&gUsbTgtKbdReportDescr, (char *)pBfr,
                  USBTGT_KBD_REPORT_DESCRIPTOR_LENGTH);
            
            *pActLen=USBTGT_KBD_REPORT_DESCRIPTOR_LENGTH;
            
            USBTGT_KBD_DBG("Get USB_DESCR_REPORT descriptor.\n",
                           1, 2, 3, 4, 5, 6);
            break;

        default:

            /* Other descriptorTypes have been hold by USBTGT stack */

            USBTGT_KBD_ERR("Unknown descriptorType 0x%x.\n", descriptorType, 
                           2, 3, 4, 5, 6);
            return ERROR;
        }

    return OK;
    }

/*******************************************************************************
*
* usbTgtKbdMngmtFunc - invoke the connection management function
*
* This function handles various management related events. <mngmtCode> consist 
* of the management event function code that is reported by the TargLib layer. 
* <pContext> is the argument sent for the management event to be handled.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtKbdMngmtFunc
    (
    pVOID                   param,                  /* TCD specific parameter */
    USB_TARG_CHANNEL        targChannel,            /* target channel */
    UINT16                  mngmtCode,              /* management code */
    pVOID                   pContext                /* context value */
    )
    {
    pUSBTGT_KBD_DEV         pUsbTgtKbdDev = (pUSBTGT_KBD_DEV) param;

    /* check parameter */

    if (pUsbTgtKbdDev == NULL)
        {
        USBTGT_KBD_ERR("parameter error.\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    switch (mngmtCode)
        {
        case TARG_MNGMT_ATTACH:
            USBTGT_KBD_DBG("usbTgtKbdMngmtFunc(): TARG_MNGMT_ATTACH targChannel"
                           " = %p\n", targChannel, 2, 3, 4, 5, 6);

            pUsbTgtKbdDev->targChannel = targChannel;
            pUsbTgtKbdDev->uConfigurationValue = 0;
            pUsbTgtKbdDev->uAlternateSetting = 0;
            pUsbTgtKbdDev->intInPipeHandle = NULL;

            break;

        case TARG_MNGMT_DETACH:
            USBTGT_KBD_DBG("usbTgtKbdMngmtFunc(): TARG_MNGMT_DETACH\n",
                           1, 2, 3, 4, 5, 6);

            /* Call the function to delete the interrupt pipe created */

            if (pUsbTgtKbdDev->intInPipeHandle != NULL)
                {
                usbTgtDeletePipe(pUsbTgtKbdDev->intInPipeHandle);
                pUsbTgtKbdDev->intInPipeHandle = NULL;
                pUsbTgtKbdDev->bIntInErpUsed = FALSE;
                }

            pUsbTgtKbdDev->targChannel = 0;
            pUsbTgtKbdDev->uDeviceFeature = 0;
            
            break;

        case TARG_MNGMT_BUS_RESET:
            USBTGT_KBD_DBG("usbTgtKbdMngmtFunc(): TARG_MNGMT_BUS_RESET\n",
                           1, 2, 3, 4, 5, 6);

            pUsbTgtKbdDev->uConfigurationValue = 0;
            pUsbTgtKbdDev->uAlternateSetting = 0;
            pUsbTgtKbdDev->bIntInErpUsed = FALSE;

            /* Call the function to delete the created interrupt pipe */

            if (pUsbTgtKbdDev->intInPipeHandle != NULL)
                {
                usbTgtDeletePipe(pUsbTgtKbdDev->intInPipeHandle);
                pUsbTgtKbdDev->intInPipeHandle = NULL;
                pUsbTgtKbdDev->bIntInErpUsed = FALSE;
                }

            break;

        case TARG_MNGMT_DISCONNECT:
            USBTGT_KBD_DBG("usbTgtKbdMngmtFunc(): TARG_MNGMT_DISCONNECT\n",
                           1, 2, 3, 4, 5, 6);

            usbTgtKbdConfigurationSet (param, targChannel, 0);

            break;

        case TARG_MNGMT_SUSPEND:
            USBTGT_KBD_DBG("usbTgtKbdMngmtFunc(): TARG_MNGMT_SUSPEND\n",
                           1, 2, 3, 4, 5, 6);

            /* when device change to suspend state, must not output */

            break;
            
        case TARG_MNGMT_RESUME:
            USBTGT_KBD_DBG("usbTgtKbdMngmtFunc(): TARG_MNGMT_RESUME\n",
                           1, 2, 3, 4, 5, 6);
            break;

        default:
            USBTGT_KBD_ERR("usbTgtKbdMngmtFunc(): parameter mngmtCode=0x%x.", 
                           mngmtCode, 2, 3, 4, 5, 6);
            return ERROR;
        }

    return OK;
    }

#ifdef USBTGT_KBD_TEST

/*******************************************************************************
*
* usbTgtKbdReportTest - keyboard report test
*
* This function injects a keyboard report into keyboard emulator.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbTgtKbdReportTest
    (
    char * pKeyReport
    )
    {
    pUSBTGT_KBD_DEV   pUsbTgtKbd;
    HID_KBD_BOOT_REPORT report;
    char keyReport[USBTGT_KBD_TEST_LEN];
    UINT32 keyLen = 0;
    UINT32 uKeyMapTableOriginalSize = 0;
    UINT32 uKeyMapTableShiftSize = 0;
    UINT32 usageId = 0;
    UINT32 i;

    pUsbTgtKbd = pUsbTgtKbdTest;

    if (pKeyReport == NULL)
        {
        /* If the user does not give any parameter, the report will be
         * from 'a' to 'z' and '1' to '9' and '0' followed by a ctrl-z.
         * There are total 26 alphabes and 10 numbers will be reported.
         * Key value tablespec: USB HID Usage Tables.
         */
        
        for (i = 0; i < USBTGT_KBD_TEST_ATO0_LEN; i ++)
            {
            memset (&report, 0, sizeof (report));
            report.scanCodes[0] = (UINT8) (USBTGT_KBD_TEST_KEYCODE_A + i);
            if (usbTgtKbdIntInErpReport (pUsbTgtKbd, &report, 
                                         sizeof (report)) != OK)
                USBTGT_KBD_ERR ("usbTgtKbdIntInErpReport() return ERROR.\n",
                                1, 2, 3, 4, 5, 6);
    
            memset (&report, 0, sizeof (report));
    
            if (usbTgtKbdIntInErpReport (pUsbTgtKbd, &report, 
                                         sizeof (report)) != OK)
                USBTGT_KBD_ERR ("usbTgtKbdIntInErpReport() return ERROR.\n",
                                1, 2, 3, 4, 5, 6);
            }
            
        return;
        }

    keyLen = (UINT32) (strlen(pKeyReport));
    if (keyLen > USBTGT_KBD_TEST_LEN) 
        {
        USBTGT_KBD_ERR("usbTgtKbdReportTest: too many parameters. Maxsize"
                       " is %d.", USBTGT_KBD_TEST_LEN, 2, 3, 4, 5, 6);
        return;
        }

    strncpy(keyReport, pKeyReport, (USBTGT_KBD_TEST_LEN - 1));
    
    uKeyMapTableOriginalSize = sizeof(pUSKeyMapTableOriginal);
    uKeyMapTableShiftSize = sizeof(pUSKeyMapTableShift);
    
    /* Key value table spec: USB HID Usage Tables */

    for (i = 0; i < keyLen; i ++)
        {
        memset (&report, 0, sizeof (report));

        for (usageId = 0; usageId < uKeyMapTableOriginalSize; usageId ++)
            {
            if (keyReport[i] == pUSKeyMapTableOriginal[usageId])
                {
                report.scanCodes[0] = (UINT8)(usageId);
                break;
                }                                  
            }
        
        if (usageId == uKeyMapTableOriginalSize)
            {
            for (usageId = 0; usageId < uKeyMapTableShiftSize; usageId ++)
                {
                if (keyReport[i] == pUSKeyMapTableShift[usageId])
                    break;
                }
                
            if (usageId == uKeyMapTableShiftSize)
                {
                USBTGT_KBD_ERR("Don't support this char %s.\n", keyReport[i], 
                               2, 3, 4, 5, 6);
                continue;
                }
            
            report.modifiers = USBTGT_KBD_TEST_KEYCODE_MODIFIER_LEFTSHIFTDOWN;
            if (usbTgtKbdIntInErpReport (pUsbTgtKbd, &report, 
                                         sizeof (report)) != OK)
                USBTGT_KBD_ERR ("usbTgtKbdIntInErpReport() return ERROR.\n",
                                1, 2, 3, 4, 5, 6);
            
            memset (&report, 0, sizeof (report));
            report.modifiers = USBTGT_KBD_TEST_KEYCODE_MODIFIER_LEFTSHIFTDOWN;
            report.scanCodes[0] = (UINT8)usageId;
            }

        if (usbTgtKbdIntInErpReport (pUsbTgtKbd, &report, 
                                     sizeof (report)) != OK)
            USBTGT_KBD_ERR ("usbTgtKbdIntInErpReport() return ERROR.\n",
                            1, 2, 3, 4, 5, 6);

        memset (&report, 0, sizeof (report));

        if (usbTgtKbdIntInErpReport (pUsbTgtKbd, &report, 
                                     sizeof (report)) != OK)
            USBTGT_KBD_ERR ("usbTgtKbdIntInErpReport() return ERROR.\n",
                            1, 2, 3, 4, 5, 6);
        }

    return;
    }

#endif /* #ifdef USBTGT_KBD_TEST */
