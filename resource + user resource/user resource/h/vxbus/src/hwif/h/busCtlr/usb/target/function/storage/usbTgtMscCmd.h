/* usbTgtMscCmd.h - Definitions of USB Target MSC command process module */

/*
 * Copyright (c) 2010, 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification History
--------------------
01e,13dec11,m_y  Modify according to code check result (WIND00319317)
01d,11apr11,m_y  modify read/write routine to support bigger transfer length
01c,25mar11,m_y  modify usbTgtMscStdInquiryDataInit
01b,17mar11,m_y  modify code based on the code review
01a,28dec10,x_f  written
*/

#ifndef __INCusbTgtMscCmdh
#define __INCusbTgtMscCmdh

#ifdef    __cplusplus
extern "C" {
#endif

#include <usbTgtMscDataStructure.h>

#define RBC_MODE_PARAMETER_PAGE    0x06 /* RBC Mode Page */

#ifdef USE_RBC_SUBCLASS
#   define PERIPHERAL_DEVICE_TYPE  0xe  /* Simplified direct-access device (RBC) */
#elif defined(USE_SCSI_SUBCLASS)
#   define PERIPHERAL_DEVICE_TYPE  0x0  /* Direct-access device */
#else
#   error PERIPHERAL_DEVICE_TYPE undefined
#endif

#define PERIPHERAL_DEVICE_QUAL          0x0     /* device qualifier */

#define MIN_STD_INQUIRY_LGTH            36      /* inquiry length */
#define VPD_SUPPORTED_PAGE_CODE         0x0     /* VPD page code */
#define VPD_UNIT_SERIAL_NUM_PAGE_CODE   0x80    /* VPD serial page number */
#define VPD_DEVICE_ID_PAGE_CODE         0x83    /* VPD device id page code */
#define VENDOR_SPECIFIC_23_LENGTH       12      /* vendor specific length */
#define DEVICE_ID	        'V','x','W','o','r','k','s',':'	/* device id */

/* SCSI Sense codes */

#define SCSI_SENSE_NO_SENSE         0x00  /* no specific sense key info */
                                          /* no error */		
#define SCSI_SENSE_RECOVERED_ERROR  0x01  /* last command completed with */
                                          /* some recovery action by UFI */
#define SCSI_SENSE_NOT_READY        0x02  /* UFI device can't be accessed */	
#define SCSI_SENSE_MEDIUM_ERROR     0x03  /* flaw in the mediaum */	  
#define SCSI_SENSE_HARDWARE_ERROR   0x04  /* non-recoverable hardware failure */	
#define SCSI_SENSE_ILLEGAL_REQUEST  0x05  /* illegal paramter in command */	
#define SCSI_SENSE_UNIT_ATTENTION   0x06  /* removeable media may have been */
                                          /* changed or UFI device reset */
#define SCSI_SENSE_DATA_PROTECT     0x07  /* write protected media */ 
#define SCSI_SENSE_BLANK_CHECK      0x08  /* device encountered a blank */
                                          /* medium while reading or non-blank*/
                                          /* medium while writing */ 
#define SCSI_SENSE_UNIQUE           0x09  /* vendor specific conditions */
#define SCSI_SENSE_COPY_ABORTED     0x0A  /* copy aborted */
#define SCSI_SENSE_ABORTED_COMMAND  0x0B  /* UFI device aborted the command */
#define SCSI_SENSE_EQUAL            0x0C  
#define SCSI_SENSE_VOL_OVERFLOW     0x0D  /* buffer overflow */
#define SCSI_SENSE_MISCOMPARE       0x0E  /* data mismatch occured */
#define SCSI_SENSE_RESERVED         0x0F  /* reserved */

/* Additional Sense codes (ASC) */

#define SCSI_ADSENSE_NO_SENSE           0x00	/* no sense key- no error */
#define SCSI_ADSENSE_LUN_NOT_READY      0x04	/* logical drive not ready */
#define SCSI_ADSENSE_ILLEGAL_COMMAND    0x20	/* illegal command */
#define SCSI_ADSENSE_ILLEGAL_BLOCK      0x21	/* block address out of range */
#define SCSI_ADSENSE_INVALID_PARAMETER  0x26	/* paramter value invalid */
#define SCSI_ADSENSE_INVALID_LUN        0x25	/* logical unit not supported */
#define SCSI_ADSENSE_INVALID_CDB        0x24	/* invalid command packet */
#define SCSI_ADSENSE_MUSIC_AREA         0xA0	/* additional sense code */
#define SCSI_ADSENSE_DATA_AREA          0xA1	/* additional sense code */
#define SCSI_ADSENSE_VOLUME_OVERFLOW    0xA7	/* volume overflow */

#define SCSI_ADSENSE_NO_MEDIA_IN_DEVICE 0x3A	/* media not present */
#define SCSI_ADSENSE_FORMAT_ERROR       0x31	/* format command failed */
#define SCSI_ADSENSE_CMDSEQ_ERROR       0x2C	/* command sequence error */
#define SCSI_ADSENSE_MEDIUM_CHANGED     0x28	/* media changed */
#define SCSI_ADSENSE_BUS_RESET          0x29	/* bus reset detected */
#define SCSI_ADWRITE_PROTECT            0x27	/* write protected */
#define SCSI_ADSENSE_TRACK_ERROR        0x14	/* recorded track not found */
#define SCSI_ADSENSE_SAVE_ERROR         0x39	/* saving parameters not */
                                                /* supported */
#define SCSI_ADSENSE_SEEK_ERROR         0x15	/* seek error */
#define SCSI_ADSENSE_REC_DATA_NOECC     0x17	/* recoverd data with retries */
#define SCSI_ADSENSE_REC_DATA_ECC       0x18	/* recovered data with ecc */ 
#define SCSI_PARAMETER_LIST_LENGTH_ERROR 0x1A	/* paramter list length error */

/* SCSI Additional sense code qualifier (ASCQ) */

#define SCSI_SENSEQ_FORMAT_IN_PROGRESS              0x04 /* format in progress*/	
                                                         /* LUN not supported */  
#define SCSI_SENSEQ_INIT_COMMAND_REQUIRED           0x02 /* initialization */
                                                         /* required */
#define SCSI_SENSEQ_MANUAL_INTERVENTION_REQUIRED    0x03 /*manual intervention*/
#define SCSI_SENSEQ_BECOMING_READY                  0x01 /* becoming ready */
#define SCSI_SENSEQ_FILEMARK_DETECTED               0x01 /*file mark detected*/
#define SCSI_SENSEQ_SETMARK_DETECTED                0x03 /* set mark detected*/
#define SCSI_SENSEQ_END_OF_MEDIA_DETECTED           0x02 /* end of media */
#define SCSI_SENSEQ_BEGINNING_OF_MEDIA_DETECTED     0x04 /* start of media */
#define SCSI_SENSEQ_ILLEGAL_POWER_CONDITION_REQUEST 0x05 /* bad power */
                                                         /* conditions */
/* typedefs */

/* command process routine */

STATUS usbTgtMscLunRead
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    ); 
STATUS usbTgtRbcRead
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    );

STATUS usbTgtRbcCapacityRead
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun,
    UINT8 **         ppData,
    UINT32 *         pSize
    );

STATUS usbTgtRbcStartStop
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    );

STATUS usbTgtRbcPreventAllowRemoval
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    );

STATUS usbTgtRbcVerify
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    );

STATUS usbTgtRbcWrite
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    );

STATUS usbTgtRbcInquiry
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun,
    UINT8 **         ppData,
    UINT32 *         pSize
    );

STATUS usbTgtRbcModeSelect
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun,
    UINT8 **         ppData,
    UINT32 *         pSize
    );

STATUS usbTgtRbcModeSense
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun,
    UINT8 **         ppData,
    UINT32 *         pSize
    );

STATUS usbTgtRbcModeSelect10
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun,
    UINT8 **         ppData,
    UINT32 *         pSize
    );

STATUS usbTgtRbcModeSense10
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun,
    UINT8 **         ppData,
    UINT32 *         pSize
    );

STATUS usbTgtRbcTestUnitReady
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    );

STATUS usbTgtRbcBufferWrite
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun,
    UINT8 **         ppData,
    UINT32 *         pSize
    );

STATUS usbTgtRbcFormat
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    );

STATUS usbTgtRbcPersistentReserveIn
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun,
    UINT8 **         ppData,
    UINT32 *         pSize
    );

STATUS usbTgtRbcPersistentReserveOut
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun,
    UINT8 **         ppData,
    UINT32 *         pSize
    );

STATUS usbTgtRbcRelease
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    );

STATUS usbTgtRbcRequestSense
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun,
    UINT8 **         ppData,
    UINT32 *         pSize
    );

STATUS usbTgtRbcReserve
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    );

STATUS usbTgtRbcCacheSync
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    );

void usbTgtRbcVendorSpecificDataInit
    (
    UINT8 *          vendorSpecificData
    );

STATUS usbTgtRbcVendorSpecific
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun,
    UINT8 **         ppData,
    UINT32 *         pSize
    );

void usbTgtMscSenseDataInit
    (
    SENSE_DATA*      pSenseData
    );

void usbTgtMscSenseDataForm
    (
    SENSE_DATA*      pSenseData,
    UINT8            senseKey,
    UINT8            asc,
    UINT8            ascq
    );

void usbTgtMscCapacityDataForm
    (
    CAPACITY_DATA*   pCapacityData,
    unsigned         blockSize,
    sector_t         nBlocks
    );

void usbTgtMscVpdSupportedPageInit
    (
    VPD_SUPPORTED_PAGE * pVpdSupportedPage
    );

void usbTgtMscVpdDevIdPageInit
    (
    VPD_DEVICE_ID_PAGE * pVpdDevIdPage
    );

void usbTgtMscVpdSerialNumPageInit
    (
    VPD_UNIT_SERIAL_NUM_PAGE * pVpdSerialNumPage,
    UINT8                      index
    );

void usbTgtMscCmdDataInit
    (
    COMMAND_DATA *   pCmdData
    );

void usbTgtMscDeviceParamListInit
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    );

void usbTgtMscDeviceParamListMaskInit
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    );

void usbTgtMscDeviceParamList10Init
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    );

void usbTgtMscDeviceParamListMask10Init
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    );

void usbTgtMscStdInquiryDataInit
    (
    STD_INQUIRY_DATA * pStdInquiryData,
    pUSB_TGT_MSC_LUN   pUsbTgtMscLun
    );

#ifdef    __cplusplus
}
#endif

#endif /* __INCusbTgtMscCmdh */
