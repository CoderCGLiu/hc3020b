/* usbEhcdHal.h - Utility Functions for EHCI */

/*
 * Copyright (c) 2002-2003, 2005-2011, 2015 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2002-2003, 2005-2011, 2015 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
modification history
--------------------
01r,05jul15,j_x  add spinlock to context of USBINTR register operation (VXW6-84654)
01q,16sep11,s_z  Remove unnecessary process to configure the EHCI register
                 (WIND00293308)
01p,28may10,w_x  Add some port line state definitions
01o,13jan10,ghs  vxWorks 6.9 LP64 adapting
01n,04feb09,w_x  Added support for FSL quirky EHCI with various register and
                 descriptor endian format (such as MPC5121E)
01m,16jul08,w_x  Added non-standard root hub TT support (WIND00127895)
01l,06aug07,jrp  Change register access methods
01k,30mar07,sup  Change the macros for read and write for one additional
                 parameter flag
01j,07oct06,ami  Changes for USB-vxBus porting
01i,22apr05,pdg  Corrected the error on reading HCCPARAMS::ADDRCAP field
01h,28mar05,pdg  non-PCI changes
01g,22mar05,mta  Complete 64-bit support added (SPR #104950)
01f,18feb05,pdg  Added macro for clearing CF bt of CONFIGFLAG register
01e,28Jul03,gpd  Incorporated the changes identified during integration testing
01d,23Jul03,gpd  Incorporated the changes identified during testing on MIPS
01c,03Jul03,gpd  Changed the interrupt status mask
01b,26jun03,gpd  changing the code to WRS standards.
01a,25apr02,ram  written.
*/

/*
DESCRIPTION
This contains the constants and macros used for
accessing the EHCI Host Controller registers.
*/

/*
INTERNAL
 *******************************************************************************
 * Filename         : usbEhcdHal.h
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      :  This contains the constants and macros used for
 *                     accessing the EHCI Host Controller registers.
 *
 *
 ******************************************************************************/
#ifndef __INCusbEhcdHalh

#define __INCusbEhcdHalh

#ifdef    __cplusplus
extern "C" {
#endif

#include <hwif/vxbus/vxBus.h>
#include <../src/hwif/h/vxbus/vxbAccess.h>

/*
 * Array of Spinlocks for the EHCI controllers.  The array will be
 * allocated in the usbEhcdInit once the maxEhciCount is known.
 */

extern spinlockIsr_t spinLockIsrEhcd[USB_MAX_EHCI_COUNT]; //zj

/* Value of the Class Code register for EHCI Host Controller */
#define USB_EHCD_CLASS_CODE   0x000C0320

/* Offsets of the Host Controller capability registers - Start */
#define USB_EHCD_CAPLENGTH         0x00  /* Offset of the CAPLENGTH register */
#define USB_EHCD_HCIVERSION        0x02  /* Offset of the HCIVERSION register */
#define USB_EHCD_HCSPARAMS         0x04  /* Offset of the HCSPARAMS register */
#define USB_EHCD_HCCPARAMS         0x08   /* Offset of the HCCPARAMS register */
#define USB_EHCD_HCSP_PORTROUTE    0x0C  /* Offset of the HCSP_PORTROUTE register */
/* Offsets of the Host Controller capability registers - End */

/* Offsets of the Host Controller operational registers - Start */
#define USB_EHCD_USBCMD            0x00 /* Offset of the USBCMD register */
#define USB_EHCD_USBSTS            0x04 /* Offset of the USBSTS register */
#define USB_EHCD_USBINTR           0x08 /* Offset of the USBINTR register */
#define USB_EHCD_FRINDEX           0x0C  /* Offset of the FRINDEX register */
#define USB_EHCD_CTRLDSSEGMENT     0x10 /* Offset of the CTRLDSSEGMENT register */
#define USB_EHCD_PERIODICLISTBASE  0x14 /* Offset of the PERIODICLISTBASE register*/
#define USB_EHCD_ASYNCLISTADDR     0x18 /* Offset of the ASYNCLISTADDR register */
#define USB_EHCD_CONFIGFLAG        0x40 /* Offset of the CONFIGFLAG register */

/* Offset of the PORTSC register*/
#define USB_EHCD_PORTSC(PORT_INDEX)  (0x44 + ((PORT_INDEX) * 4))

/* Offsets of the Host Controller operational registers - End */

/* Bit positions of USBCMD register - Start */

/* Interrupt threshold level bitposition */
#define USB_EHCD_USBCMD_INT_THRESHOLD                       16

/* Asynch schedule park mode enable bitposition */
#define USB_EHCD_USBCMD_ASYNCH_SCHEDULE_PARK_MODE_ENABLE    11

/* Asynch schedule park mode count bitposition */
#define USB_EHCD_USBCMD_ASYNCH_SCHEDULE_PARK_MODE_COUNT     8

/* Light Host Controller reset bitposition */
#define USB_EHCD_USBCMD_LIGHT_HC_RESET                      7

/* Interrupt On Async Advance Doorbell bitposition */
#define USB_EHCD_USBCMD_INTERRUPT_ON_ASYNC_ADVANCE_DOORBELL 6

/* Asynchronous Schedule Enable bitposition */
#define USB_EHCD_USBCMD_ASYNCHRONOUS_SCHEDULE_ENABLE        5

/* Periodic Schedule Enable bitposition */
#define USB_EHCD_USBCMD_PERIODIC_SCHEDULE_ENABLE            4

/* Frame list size bitposition */
#define USB_EHCD_USBCMD_FRAME_LIST_SIZE                     2

/* Host Controller Reset bitposition */
#define USB_EHCD_USBCMD_HC_RESET                            1

/* Run/Stop bitposition */
#define USB_EHCD_USBCMD_RS                                  0

/* Bit positions of USBCMD register - End */

/* Mask values of the HCSPARAMS register - Start */

/* Mask value of the N_PORTS field */
#define USB_EHCD_HCSPARAMS_N_PORTS_MASK                     0x0F

/* Mask value of the PPC field */
#define USB_EHCD_HCSPARAMS_PPC_MASK                         0x10

#define USB_EHCD_HCCPARAMS_ADDRCAP_MASK                     0x01
/* Mask values of the HCSPARAMS register - End */

/* Bit positions of HCSPARAMS register - Start */

/* Bitposition of PPC field */
#define USB_EHCD_HCSPARAMS_PPC                              4

/* Bit positions of HCSPARAMS register - End */

/* Bit positions of USBSTS register - Start */

/* Asyncronous Schedule Status bitposition */
#define USB_EHCD_USBSTS_ASYNCHRONOUS_SCHEDULE_STATUS        15
/* Periodic Schedule Status bitposition */
#define USB_EHCD_USBSTS_PERIODIC_SCHEDULE_STATUS            14
/* Reclamation bitposition */
#define USB_EHCD_USBSTS_RECLAMATION                         13
/* HCHalted bitposition */
#define USB_EHCD_USBSTS_HCHALTED                            12
/* Interrupt On Async Advance bitposition */
#define USB_EHCD_USBSTS_INTERRUPT_ON_ASYNC_ADVANCE          5
/* Host System Error bitposition */
#define USB_EHCD_USBSTS_HOST_SYSTEM_ERROR                   4

/* Frame list Rollover bitposition */
#define USB_EHCD_USBSTS_FRAME_LIST_ROLLOVER                 3

/* Port Change Detect field bitposition */
#define USB_EHCD_USBSTS_PORT_CHANGE_DETECT                  2
/* USB Error Interrupt field bitposition */
#define USB_EHCD_USBSTS_USBERRINT                           1
/* USB Interrupt field bitposition */
#define USB_EHCD_USBSTS_USBINT                              0
/* Bit positions of USBSTS register - End */

/* Bit positions of the USBINTR register - Start */
/* Interrupt on Aync Advance Enable bitposition */
#define USB_EHCD_USBINTR_INTERRUPT_ON_ASYNC_ADVANCE_ENABLE   5
/* Interrupt on Aync Advance Enable bitposition */
#define USB_EHCD_USBINTR_HOST_SYSTEM_ERROR_ENABLE            4
/* Frame List Rollover Enable bitposition */
#define USB_EHCD_USBINTR_FRAME_LIST_ROLLOVER_ENABLE          3
/* Port Change Interrupt Enable bitposition */
#define USB_EHCD_USBINTR_PORT_CHANGE_INTERRUPT_ENABLE        2
/* USB Error Interrupt Enable bitposition */
#define USB_EHCD_USBINTR_USB_ERROR_INTERRUPT_ENABLE          1
/* USB Interrupt Enable bitposition */
#define USB_EHCD_USBINTR_USB_INT_ENABLE                      0
/* Bit positions of the USBINTR register - End */

/* Frame Index bitposition of FRINDEX register */
#define USB_EHCD_FRINDEX_FRAME_INDEX                         0
/* Bit position of the Base Address field of the PERIODICLISTBASE register */
#define USB_EHCD_PERIODICLISTBASE_BASE_ADDRESS               12
/* Bit position of the Link Pointer field of the ASYNCHLISTADDR register */
#define USB_EHCD_ASYNCHLISTADDR_LINK_PTR_LOW                 5

/* Bit position of the Configure Flag field of the CONFIGFLAG register */
#define USB_EHCD_CONFIGFLAG_CF                               0

/* Bit positions of PORTSC register - Start */
#define USB_EHCD_PORTSC_PSPD                      26  /* PSPD bitposition */
#define USB_EHCD_PORTSC_WKOC_E                    22  /* WKOC_E bitposition */
#define USB_EHCD_PORTSC_WKDSCNNT_E                21  /* WKDSCNNT_E bitposition */
#define USB_EHCD_PORTSC_WKCNNT_E                  20 /* WKCNNT_E bitposition */
#define USB_EHCD_PORTSC_PORT_TEST_CONTROL         16 /* Port Test Control
                                                    bitposition */
#define USB_EHCD_PORTSC_PORT_INDICATOR_CONTROL    14  /* Port Indicator Control
                                                     bitposition */
#define USB_EHCD_PORTSC_PORT_OWNER                13  /* Port Owner bitposition */
#define USB_EHCD_PORTSC_PORT_POWER                12 /* Port Power bitposition */
#define USB_EHCD_PORTSC_LINE_STATUS               10 /* Line status bitposition */
#define USB_EHCD_PORTSC_PORT_RESET                8  /* Port Reset bitposition */
#define USB_EHCD_PORTSC_SUSPEND                   7  /* Suspend bitposition */
#define USB_EHCD_PORTSC_FORCE_PORT_RESUME         6  /* Force Port Resume
                                                 bitposition */
#define USB_EHCD_PORTSC_OVER_CURRENT_CHANGE       5  /* Over-current change
                                                    bitposition */
#define USB_EHCD_PORTSC_OVER_CURRENT_ACTIVE       4  /* Over-current Active
                                                    bitposition */
#define USB_EHCD_PORTSC_PORT_ENABLE_DISABLE_CHANGE  3 /* Port Enable/Disable
                                                   change bitposition */
#define USB_EHCD_PORTSC_PORT_ENABLED_DISABLED      2 /* Port Enabled/Disabled
                                                    bitposition */
#define USB_EHCD_PORTSC_CONNECT_STATUS_CHANGE      1 /* Connect Status Change
                                                    bitposition */
#define USB_EHCD_PORTSC_CURRENT_CONNECT_STATUS     0   /* Current Connect Status
                                                      field bitposition */
/* Bit positions of PORTSC register - End */

/* Mask values for USBCMD register - Start */
#define USB_EHCD_USBCMD_INT_THRESHOLD_MASK        0x00FF0000 /* Interrupt threshold
                                                            mask */

/* Asynch Schedule Park Mode Enable mask */
#define USB_EHCD_USBCMD_ASYNCH_SCHEDULE_PARK_MODE_EN_MASK    0x00000800
/* Asynch Schedule Park Mode Count mask */
#define USB_EHCD_USBCMD_ASYNCH_SCHEDULE_PARK_MODE_COUNT_MASK 0x00000300
/* Light HC Reset mask */
#define USB_EHCD_USBCMD_LIGHT_HC_RESET_MASK                  0x00000080
/* Interrupt on Asynch Advance doorbell mask */
#define USB_EHCD_USBCMD_ASYNC_ADVANCE_DOORBELL_MASK          0x00000040
/* Asynch Schedule Enable mask */
#define USB_EHCD_USBCMD_ASYNCH_SCHEDULE_ENABLE_MASK          0x00000020
/* Periodic Schedule Enable mask */
#define USB_EHCD_USBCMD_PERIODIC_SCHEDULE_ENABLE_MASK        0x00000010
/* Frame list size mask */
#define USB_EHCD_USBCMD_FRAME_LIST_SIZE_MASK                 0x0000000C
/* HCRESET mask */
#define USB_EHCD_USBCMD_HCRESET_MASK                         0x00000002
/* RS mask */
#define USB_EHCD_USBCMD_RS_MASK                              0x00000001
/* Mask values for USBCMD register - End */

/* Mask values for USBSTS register - Start */
/* Asynch Schedule Status mask */
#define USB_EHCD_USBSTS_ASYCH_SCHEDULE_STATUS_MASK           0x00008000
/* Periodic Schedule Status mask */
#define USB_EHCD_USBSTS_PERIODIC_SCHEDULE_STATUS_MASK        0x00004000
/* Reclamation mask */
#define USB_EHCD_USBSTS_RECLAMATION_MASK                     0x00002000
/* HCHalted mask */
#define USB_EHCD_USBSTS_HCHALTED_MASK                        0x00001000
/* Interrupt On Asynch Advance mask */
#define USB_EHCD_USBSTS_INTERRUPT_ON_ASYNC_ADVANCE_MASK      0x00000020
/* Host System Error mask */
#define USB_EHCD_USBSTS_HOST_SYSTEM_ERROR_MASK               0x00000010
/* Frame list Rollover mask */
#define USB_EHCD_USBSTS_FRAME_LIST_ROLLOVER_MASK             0x00000008
/* Port Change detect mask */
#define USB_EHCD_USBSTS_PORT_CHANGE_DETECT_MASK              0x00000004
/* USBERRINT mask */
#define USB_EHCD_USBSTS_USBERRINT_MASK                       0x00000002
/* USBINT mask */
#define USB_EHCD_USBSTS_USBINT_MASK                          0x00000001
/* Mask values for USBSTS register - End */

/* Mask values for USBINTR register - Start */
/* Asynch Advance Enable mask */
#define USB_EHCD_USBINTR_INTERRUPT_ON_ASYNC_ADVANCE_MASK     0x00000020
/* Host System Error Enable mask */
#define USB_EHCD_USBINTR_HOST_SYSTEM_ERROR_ENABLE_MASK       0x00000010
/* Host Frame List Rollover Enable mask */
#define USB_EHCD_USBINTR_FRAME_LIST_ROLLOVER_ENABLE_MASK     0x00000008
/* Port change Enable mask */
#define USB_EHCD_USBINTR_PORT_CHANGE_INTERRUPT_ENABLE_MASK   0x00000004
/* USB Error Enable mask */
#define USB_EHCD_USBINTR_USB_ERROR_INTERRUPT_ENABLE_MASK     0x00000002
/* USB Interrupt mask */
#define USB_EHCD_USBINTR_USB_INT_ENABLE_MASK                 0x00000001
/* Mask values for USBINTR register - End */

/* Mask values for FRINDEX register - Start changed "Vijay" */
#define USB_EHCD_FRINDEX_FRAME_INDEX_MASK                    0x00001FFF
/* Mask values for FRINDEX register - End */

/* Mask values for PERIODICLISTBASE register - Start */
#define USB_EHCD_PERIODICLISTBASE_BASE_ADDRESS_MASK          0xFFFFF000
/* Mask values for PERIODICLISTBASE register - End */

/* Mask values for ASYNCHLISTADDR register - Start */
/* Link pointer low mask */
#define USB_EHCD_ASYNCHLISTADDR_LINK_PTR_LOW_MASK            0xFFFFFFE0
/* Mask values for ASYNCHLISTADDR register - End */

/* Mask values for CONFIGFLAG register - Start */
/* Configure Flag mask */
#define USB_EHCD_CONFIGFLAG_CF_MASK                          0x00000001
/* Mask values for CONFIGFLAG register - End */

/* Mask values for PORTSC register - Start */
#define USB_EHCD_PORTSC_PSPD_MASK                            0x0C000000
/* WKOC_E mask */
#define USB_EHCD_PORTSC_WKOC_E_MASK                          0x00400000
/* WKDSCNNT_E mask */
#define USB_EHCD_PORTSC_WKDSCNNT_E_MASK                      0x00200000
/* WKCNNT_E mask */
#define USB_EHCD_PORTSC_WKCNNT_E_MASK                        0x00100000
/* Port Test Control mask */
#define USB_EHCD_PORTSC_PORT_TEST_CONTROL_MASK               0x000F0000
/* Port Indicator Control mask */
#define USB_EHCD_PORTSC_PORT_INDICATOR_CONTROL_MASK          0x0000C000
/* Port Owner mask */
#define USB_EHCD_PORTSC_PORT_OWNER_MASK                      0x00002000
/* Port Power mask */
#define USB_EHCD_PORTSC_PORT_POWER_MASK                      0x00001000
/* Line Status mask */
#define USB_EHCD_PORTSC_LINE_STATUS_MASK                     0x00000C00
/* Port Reset mask */
#define USB_EHCD_PORTSC_PORT_RESET_MASK                      0x00000100
/* Suspend mask */
#define USB_EHCD_PORTSC_SUSPEND_MASK                         0x00000080
/* Force port resume mask */
#define USB_EHCD_PORTSC_FORCE_PORT_RESUME_MASK               0x00000040
/* Overcurrent change mask */
#define USB_EHCD_PORTSC_OVER_CURRENT_CHANGE_MASK             0x00000020
/* Overcurrent active mask */
#define USB_EHCD_PORTSC_OVER_CURRENT_ACTIVE_MASK             0x00000010
/* Port enable/ disable change mask */
#define USB_EHCD_PORTSC_PORT_ENABLE_DISABLE_CHANGE_MASK      0x00000008
/* Port enabled/ disabled mask */
#define USB_EHCD_PORTSC_PORT_ENABLED_DISABLED_MASK           0x00000004
/* Port connect status change mask */
#define USB_EHCD_PORTSC_CONNECT_STATUS_CHANGE_MASK           0x00000002
/* Port current connect status mask */
#define USB_EHCD_PORTSC_CURRENT_CONNECT_STATUS_MASK          0x00000001

/* 00b SE0 Not Low-speed device, perform EHCI reset */
#define USB_EHCD_PORTSC_LINE_STATUS_SE0                    0x00

/* 01b K-state Low-speed device, release ownership of port */
#define USB_EHCD_PORTSC_LINE_STATUS_K                      0x01

/* 10b J-state Not Low-speed device, perform EHCI reset */
#define USB_EHCD_PORTSC_LINE_STATUS_J                      0x02

/* Mask values for PORTSC register - End */

/* Value to be written to the interrupt enable register */
/* Enable the Host System Error interrupt */
#define USB_EHCD_INTERRUPT_MASK                              0x00000010
/* Mask value of the USBSTS register for reading the interrupt status */
#define USB_EHCD_USBSTS_INTERRUPT_MASK                       0x0000003F

/*******************************************************************************
 * Macro Name       : USB_EHCD_READ_REG
 * Description      : This macro is used to read the contents of the register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 *                    OFFSET    IN Offset of the register
 * Return Type      : UINT32
 *                        - Returns the dword read from the address.
 ******************************************************************************/

#define USB_EHCD_READ_REG(PEHCDDATA,                                        \
                      OFFSET)                                               \
                                                                            \
    vxbRead32 ((PEHCDDATA)->pRegAccessHandle,                               \
    (VOID *)((PEHCDDATA)->regBase  + (ULONG)(OFFSET) + (PEHCDDATA)->capLenOffset))

/*******************************************************************************
 * Macro Name       : USB_EHCD_WRITE_REG
 * Description      : This macro is used to write to a register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 *                    OFFSET    IN Offset of the register
 *                    VALUE     IN Value to be written to the register.
 * Return Type      : None.
 ******************************************************************************/

#define USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       OFFSET,                                              \
                       VALUE)                                               \
    vxbWrite32 ((PEHCDDATA)->pRegAccessHandle,                              \
    (VOID *)((PEHCDDATA)->regBase  + (ULONG)(OFFSET) + (PEHCDDATA)->capLenOffset),   \
            VALUE)



/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD
 * Description      : This macro is used to get the fields of a register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 *                    REGNAME   IN Name of the register
 *                    FIELD     IN Field of the register
 * Return Type      : Value in the specified field of the register.
 ******************************************************************************/
#define USB_EHCD_GET_FIELD(PEHCDDATA,                       \
                       REGNAME,                             \
                       FIELD)                               \
                                                            \
    USB_EHCD_GET_FIELD_##REGNAME##__##FIELD (PEHCDDATA)

/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_BIT
 * Description      : This macro is used to set the fields of a register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 *                    REGNAME   IN Name of the register
 *                    FIELD     IN Field of the register
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_BIT(PEHCDDATA,                         \
                     REGNAME,                               \
                     FIELD)                                 \
                                                            \
    USB_EHCD_SET_BIT_##REGNAME##__##FIELD (PEHCDDATA)

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT
 * Description      : This macro is used to clear the fields of a register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 *                    REGNAME   IN Name of the register
 *                    FIELD     IN Field of the register
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT(PEHCDDATA,                         \
                     REGNAME,                               \
                     FIELD)                                 \
                                                            \
    USB_EHCD_CLR_BIT_##REGNAME##__##FIELD (PEHCDDATA)

/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_FIELD
 * Description      : This macro is used to set a particular value in the
 *                    specified field of a register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 *                    REGNAME   IN Name of the register
 *                    FIELD     IN Field of the register
 *                    VALUE     IN Value to be set in the specified field.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_FIELD(PEHCDDATA,                       \
                       REGNAME,                             \
                       FIELD,                               \
                       VALUE)                               \
                                                            \
    USB_EHCD_SET_FIELD_##REGNAME##__##FIELD (PEHCDDATA,     \
                                          VALUE)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_PORT
 * Description      : This macro is used to get the fields of a port register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register
 *                    FIELD      IN Field of the register
 * Return Type      : Value in the specified field of the register.
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_PORT(PEHCDDATA,                      \
                            PORT_INDEX,                         \
                            FIELD)                              \
                                                                \
    USB_EHCD_GET_FIELD_PORTSC__##FIELD (PEHCDDATA,              \
                                     PORT_INDEX)

/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_BIT_PORT
 * Description      : This macro is used to set the fields of a port register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register
 *                    FIELD      IN Field of the register
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_BIT_PORT(PEHCDDATA,                        \
                          PORT_INDEX,                           \
                          FIELD)                                \
                                                                \
    USB_EHCD_SET_BIT_PORTSC__##FIELD (PEHCDDATA,                \
                                   PORT_INDEX)

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_PORT
 * Description      : This macro is used to clear the fields of a port register.
 * Parameters       : PEHCDDATA     IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX    IN Index of the port register
 *                    FIELD         IN Field of the register
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_PORT(PEHCDDATA,                        \
                          PORT_INDEX,                           \
                          FIELD)                                \
                                                                \
    USB_EHCD_CLR_BIT_PORTSC__##FIELD (PEHCDDATA,                \
                                   PORT_INDEX)

/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_FIELD_PORT
 * Description      : This macro is used to set a particular value in the
 *                    specified field of a port register.
 * Parameters       : PEHCDDATA     IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX    IN Index of the port register
 *                    FIELD         IN Field of the register
 *                    VALUE         IN Value to be set in the specified field.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_FIELD_PORT(PEHCDDATA,                      \
                            PORT_INDEX,                         \
                            FIELD,                              \
                            VALUE)                              \
                                                                \
    USB_EHCD_SET_FIELD_PORTSC__##FIELD (PEHCDDATA,              \
                                     PORT_INDEX,                \
                                     VALUE)

/*******************************************************************************
 * Macro Name       : USB_EHCD_READ_CAPLENGTH
 * Description      : This macro is used to read the contents of the CAPLENGTH
 *                    register
 * Parameters       : PEHCDDATA  IN  Pointer to the USB_EHCD_DATA structure.
 * Return Type      : UINT8
 *                        - Returns the contents of the CAPLENGTH register.
 ******************************************************************************/

#define USB_EHCD_READ_CAPLENGTH(PEHCDDATA)                      \
    vxbRead32 ((PEHCDDATA)->pRegAccessHandle,                   \
        (VOID *)((PEHCDDATA)->regBase + USB_EHCD_CAPLENGTH))

/*******************************************************************************
 * Macro Name       : USB_EHCD_READ_HCSPARAMS
 * Description      : This macro is used to read the contents of the HCSPARAMS
 *                    register
 * Parameters       : PEHCDDATA  IN  Pointer to the USB_EHCD_DATA structure.
 * Return Type      : UINT32
 *                        - Returns the contents of the HCSPARAMS register.
 ******************************************************************************/

#define USB_EHCD_READ_HCSPARAMS(PEHCDDATA)                      \
    vxbRead32 ((PEHCDDATA)->pRegAccessHandle,                   \
        (VOID *)((PEHCDDATA)->regBase + USB_EHCD_HCSPARAMS))

/*
 *******************************************************************************
 * Macro Name       : USB_EHCD_READ_HCCPARAMS
 * Description      : This macro is used to read the contents of the HCCPARAMS
 *                    register
 * Parameters       : PEHCDDATA  IN  Pointer to the USB_EHCD_DATA structure.
 * Return Type      : UINT32
 *                        - Returns the contents of the HCCPARAMS register.
 *****************************************************************************
 */

#define USB_EHCD_READ_HCCPARAMS(PEHCDDATA)                      \
    vxbRead32 ((PEHCDDATA)->pRegAccessHandle,                   \
        (VOID *)((PEHCDDATA)->regBase + USB_EHCD_HCCPARAMS))



/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_HCSPARAMS__N_PORTS
 * Description      : This macro is used to read the value of N_PORTS of the
 *                    HCSPARAMS register.
 * Parameters       : PEHCDDATA  IN  Pointer to the USB_EHCD_DATA structure.
 * Return Type      : UINT32
 *                        - Returns the contents of the N_PORTS field of
 *                          HCSPARAMS register.
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_HCSPARAMS__N_PORTS(PEHCDDATA)                    \
                                                                            \
    (USB_EHCD_READ_HCSPARAMS(PEHCDDATA) & USB_EHCD_HCSPARAMS_N_PORTS_MASK)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_HCSPARAMS__PPC
 * Description      : This macro is used to read the value of PPC of the
 *                    HCSPARAMS register.
 * Parameters       : PEHCDDATA  IN  Pointer to the USB_EHCD_DATA structure.
 * Return Type      : UINT32
 *                        - Returns the contents of the PPC field of
 *                          HCSPARAMS register.
 ******************************************************************************/

#define USB_EHCD_GET_FIELD_HCSPARAMS__PPC(PEHCDDATA)                          \
                                                                              \
    ((USB_EHCD_READ_HCSPARAMS(PEHCDDATA) &                                    \
        USB_EHCD_HCSPARAMS_PPC_MASK) >> USB_EHCD_HCSPARAMS_PPC)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_HCCPARAMS__ADDRCAP
 * Description      : This macro is used to read the value of ADDRCAP of the
 *                    HCCPARAMS register.
 * Parameters       : PEHCDDATA  IN  Pointer to the USB_EHCD_DATA structure.
 * Return Type      : UINT32
 *                        - Returns the contents of the ADDRCAP field of
 *                          HCCPARAMS register.
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_HCCPARAMS__ADDRCAP(PEHCDDATA)                    \
                                                                            \
    (USB_EHCD_READ_HCCPARAMS(PEHCDDATA) & USB_EHCD_HCCPARAMS_ADDRCAP_MASK)

/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_FIELD_USBCMD__INT_THRESHOLD_CONTROL
 * Description      : This macro is used to set the INT_THRESHOLD_CONTROL field
 *                    of the USBCMD register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 *                    VALUE     IN Value to be set in the field
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_FIELD_USBCMD__INT_THRESHOLD_CONTROL(PEHCDDATA,     \
                                                     VALUE)             \
                                                                        \
{                                                                       \
                                                                        \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                   \
                       USB_EHCD_USBCMD,                                 \
                       (USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_USBCMD) &  \
                       ~(USB_EHCD_USBCMD_INT_THRESHOLD_MASK)) |         \
                       (VALUE << USB_EHCD_USBCMD_INT_THRESHOLD));       \
}

/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_BIT_USBCMD__ASYNCH_SCHEDULE_PARK_MODE_ENABLE
 * Description      : This macro is used to set the
 *                    ASYNCH_SCHEDULE_PARK_MODE_ENABLE field of the
 *                    USBCMD register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_BIT_USBCMD__ASYNCH_SCHEDULE_PARK_MODE_ENABLE(PEHCDDATA)\
                                                                            \
{                                                                           \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBCMD,                                     \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_USBCMD) |       \
                       USB_EHCD_USBCMD_ASYNCH_SCHEDULE_PARK_MODE_EN_MASK);  \
}

/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_FIELD_USBCMD__ASYCH_SCHEDULE_PARK_MODE_COUNT
 * Description      : This macro is used to set the
 *                    ASYCH_SCHEDULE_PARK_MODE_COUNT field
 *                    of the USBCMD register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 *                    VALUE     IN Value to be set in the field
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_FIELD_USBCMD__ASYCH_SCHEDULE_PARK_MODE_COUNT(PEHCDDATA,\
                                                              VALUE)        \
                                                                            \
{                                                                           \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
               USB_EHCD_USBCMD,                                             \
               (USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_USBCMD) &              \
               ~(USB_EHCD_USBCMD_ASYNCH_SCHEDULE_PARK_MODE_COUNT_MASK)) |   \
               (VALUE << USB_EHCD_USBCMD_ASYNCH_SCHEDULE_PARK_MODE_COUNT)); \
}
/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_BIT_USBCMD__LIGHT_HC_RESET
 * Description      : This macro is used to set the
 *                    LIGHT_HCRESET field of the
 *                    USBCMD register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_BIT_USBCMD__LIGHT_HC_RESET(PEHCDDATA)                  \
                                                                            \
{                                                                           \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBCMD,                                     \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_USBCMD) |       \
                       USB_EHCD_USBCMD_LIGHT_HC_RESET_MASK);                \
}

/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_BIT_USBCMD__INT_ON_ASYNC_ADVANCE_DOORBELL
 * Description      : This macro is used to set the
 *                    INT_ON_ASYNCH_ADVANCE_DOORBELL field of the
 *                    USBINTR register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_BIT_USBINTR_INT_ON_ASYNC_ADVANCE_DOORBELL(PEHCDDATA)   \
                                                                            \
{                                                                           \
        SPIN_LOCK_ISR_TAKE(&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);         \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBINTR,                                    \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_USBINTR) |      \
                       USB_EHCD_USBSTS_INTERRUPT_ON_ASYNC_ADVANCE_MASK);    \
        SPIN_LOCK_ISR_GIVE (&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);        \
}
/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_BIT_USBCMD_ASYNC_ADVANCE_DOORBELL
 * Description      : This macro is used to set the
 *                    INT_ON_ASYNCH_ADVANCE_DOORBELL field of the
 *                    USBCMD register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_BIT_USBCMD_INT_ON_ASYNC_ADVANCE_DOORBELL(PEHCDDATA)    \
                                                                            \
{                                                                           \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBCMD,                                     \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_USBCMD) |       \
                       USB_EHCD_USBCMD_ASYNC_ADVANCE_DOORBELL_MASK);        \
}
/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_BIT_USBINTR_INT_ON_FRAME_LIST_ROLLOVER
 * Description      : This macro is used to set the
 *                    INT_ON_FRAME_LIST_ROLLOVER field of the
 *                    USBINT register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_BIT_USBINTR_INT_ON_FRAME_LIST_ROLLOVER(PEHCDDATA)      \
                                                                            \
{                                                                           \
        SPIN_LOCK_ISR_TAKE(&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);         \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBINTR,                                    \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_USBINTR) |      \
                       USB_EHCD_USBSTS_FRAME_LIST_ROLLOVER_MASK);           \
        SPIN_LOCK_ISR_GIVE (&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);        \
}
/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_USBINTR_INT_ON_FRAME_LIST_ROLLOVER
 * Description      : This macro is used to clear the
 *                    INT_ON_FRAME_LIST_ROLLOVER field of the
 *                    USBINT register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_USBINTR_INT_ON_FRAME_LIST_ROLLOVER(PEHCDDATA)      \
                                                                            \
{                                                                           \
        SPIN_LOCK_ISR_TAKE(&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);         \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBINTR,                                    \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_USBINTR) &      \
                       ~(USB_EHCD_USBSTS_FRAME_LIST_ROLLOVER_MASK));        \
        SPIN_LOCK_ISR_GIVE (&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);        \
}

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_USBCMD__INT_ON_ASYNC_ADVANCE_DOORBELL
 * Description      : This macro is used to clear the
 *                    INT_ON_ASYNCH_ADVANCE_DOORBELL field of the
 *                    USBINTR register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_USBINTR_INT_ON_ASYNC_ADVANCE_DOORBELL(PEHCDDATA)   \
                                                                            \
{                                                                           \
        SPIN_LOCK_ISR_TAKE(&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);         \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBINTR,                                    \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_USBINTR) &      \
                       ~(USB_EHCD_USBSTS_INTERRUPT_ON_ASYNC_ADVANCE_MASK)); \
        SPIN_LOCK_ISR_GIVE (&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);        \
}
/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_CLR_USBCMD_ASYNC_ADVANCE_DOORBELL
 * Description      : This macro is used to clear the
 *                    INT_ON_ASYNCH_ADVANCE_DOORBELL field of the
 *                    USBCMD register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_USBCMD_INT_ON_ASYNC_ADVANCE_DOORBELL(PEHCDDATA)    \
                                                                            \
{                                                                           \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBCMD,                                     \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_USBCMD) &       \
                       ~(USB_EHCD_USBCMD_ASYNC_ADVANCE_DOORBELL_MASK));     \
}
/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_BIT_USBCMD__ASYNCH_SCHEDULE_ENABLE
 * Description      : This macro is used to set the
 *                    ASYNCH_SCHEDULE_ENABLE field of the
 *                    USBCMD register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_BIT_USBCMD__ASYNCH_SCHEDULE_ENABLE(PEHCDDATA)          \
                                                                            \
{                                                                           \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBCMD,                                     \
                       (USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_USBCMD) |      \
                       USB_EHCD_USBCMD_ASYNCH_SCHEDULE_ENABLE_MASK));       \
}

/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_BIT_USBCMD__PERIODIC_SCHEDULE_ENABLE
 * Description      : This macro is used to set the
 *                    PERIODIC_SCHEDULE_ENABLE field of the
 *                    USBCMD register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_BIT_USBCMD__PERIODIC_SCHEDULE_ENABLE(PEHCDDATA)        \
                                                                            \
{                                                                           \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBCMD,                                     \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_USBCMD) |       \
                       USB_EHCD_USBCMD_PERIODIC_SCHEDULE_ENABLE_MASK);      \
}

/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_FIELD_USBCMD__FRAME_LIST_SIZE
 * Description      : This macro is used to set the
 *                    FRAME_LIST_SIZE field
 *                    of the USBCMD register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 *                    VALUE     IN Value to be set in the field
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_FIELD_USBCMD__FRAME_LIST_SIZE(PEHCDDATA,               \
                                               VALUE)                       \
                                                                            \
{                                                                           \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBCMD,                                     \
                       (USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_USBCMD)&       \
                       ~(USB_EHCD_USBCMD_FRAME_LIST_SIZE_MASK)) |           \
                       (VALUE << USB_EHCD_USBCMD_FRAME_LIST_SIZE));         \
}

/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_BIT_USBCMD__HCRESET
 * Description      : This macro is used to set the HCRESET field of the
 *                    USBCMD register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_BIT_USBCMD__HCRESET(PEHCDDATA)                         \
                                                                            \
{                                                                           \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBCMD,                                     \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_USBCMD) |       \
                       USB_EHCD_USBCMD_HCRESET_MASK);                       \
}

/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_BIT_USBCMD__RS
 * Description      : This macro is used to set the RS field of the
 *                    USBCMD register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_BIT_USBCMD__RS(PEHCDDATA)                              \
                                                                            \
{                                                                           \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBCMD,                                     \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_USBCMD) |       \
                       USB_EHCD_USBCMD_RS_MASK);                            \
}

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_USBCMD__ASYNCH_SCHEDULE_PARK_MODE_ENABLE
 * Description      : This macro is used to clear the
 *                    ASYNCH_SCHEDULE_PARK_MODE_ENABLE field of the
 *                    USBCMD register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_USBCMD__ASYNCH_SCHEDULE_PARK_MODE_ENABLE(PEHCDDATA)\
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBCMD,                                     \
                       (USB_EHCD_READ_REG(PEHCDDATA,                        \
                                     USB_EHCD_USBCMD) &                     \
                       (~USB_EHCD_USBCMD_ASYNCH_SCHEDULE_PARK_MODE_EN_MASK))

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_FIELD_USBCMD__ASYCH_SCHEDULE_PARK_MODE_COUNT
 * Description      : This macro is used to clear the
 *                    ASYCH_SCHEDULE_PARK_MODE_COUNT field
 *                    of the USBCMD register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 *                    VALUE     IN Value to be set in the field
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_FIELD_USBCMD__ASYCH_SCHEDULE_PARK_MODE_COUNT(PEHCDDATA,\
                                                              VALUE)        \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBCMD,                                     \
                       (USB_EHCD_READ_REG(PEHCDDATA,                        \
                                     USB_EHCD_USBCMD) &                     \
                       (~USB_EHCD_USBCMD_ASYNCH_SCHEDULE_PARK_MODE_COUNT_MASK)))

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_USBCMD__LIGHT_HC_RESET
 * Description      : This macro is used to clear the
 *                    LIGHT_HCRESET field of the
 *                    USBCMD register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_USBCMD__LIGHT_HC_RESET(PEHCDDATA)                  \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBCMD,                                     \
                       (USB_EHCD_READ_REG(PEHCDDATA,                        \
                                      USB_EHCD_USBCMD) &                    \
                       ~(USB_EHCD_USBCMD_LIGHT_HCRESET_MASK)))

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_USBCMD__INT_ON_ASYNC_ADVANCE_DOORBELL
 * Description      : This macro is used to clear the
 *                    INT_ON_ASYNC_ADVANCE_DOORBELL field of the
 *                    USBCMD register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_USBCMD__INT_ON_ASYNC_ADVANCE_DOORBELL(PEHCDDATA)   \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBCMD,                                     \
                       (USB_EHCD_READ_REG(PEHCDDATA,                        \
                                     USB_EHCD_USBCMD) &                     \
                       (~USB_EHCD_USBCMD_ASYNC_ADVANCE_DOORBELL_MASK)))

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_USBCMD__ASYNCH_SCHEDULE_ENABLE
 * Description      : This macro is used to clear the
 *                    ASYNCH_SCHEDULE_ENABLE field of the
 *                    USBCMD register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_USBCMD__ASYNCH_SCHEDULE_ENABLE(PEHCDDATA)          \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBCMD,                                     \
                       (USB_EHCD_READ_REG(PEHCDDATA,                        \
                                     USB_EHCD_USBCMD) &                     \
                       (~USB_EHCD_USBCMD_ASYNCH_SCHEDULE_ENABLE_MASK)))

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_USBCMD__PERIODIC_SCHEDULE_ENABLE
 * Description      : This macro is used to clear the
 *                    PERIODIC_SCHEDULE_ENABLE field of the
 *                    USBCMD register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_USBCMD__PERIODIC_SCHEDULE_ENABLE(PEHCDDATA)        \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBCMD,                                     \
                       (USB_EHCD_READ_REG(PEHCDDATA,                        \
                                     USB_EHCD_USBCMD) &                     \
                       (~USB_EHCD_USBCMD_PERIODIC_SCHEDULE_ENABLE_MASK)))

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_USBCMD__HCRESET
 * Description      : This macro is used to clear the HCRESET field of the
 *                    USBCMD register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_USBCMD__HCRESET(PEHCDDATA)                         \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBCMD,                                     \
                       (USB_EHCD_READ_REG(PEHCDDATA,                        \
                                     USB_EHCD_USBCMD) &                     \
                       (~USB_EHCD_USBCMD_HCRESET_MASK)))

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_USBCMD__RS
 * Description      : This macro is used to clear the RS field of the
 *                    USBCMD register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_USBCMD__RS(PEHCDDATA)                              \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBCMD,                                     \
                       (USB_EHCD_READ_REG(PEHCDDATA,                        \
                                      USB_EHCD_USBCMD) &                    \
                       ~(USB_EHCD_USBCMD_RS_MASK)))

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_USBCMD__INT_THRESHOLD_CONTROL
 * Description      : This macro is used to get the INT_THRESHOLD_CONTROL field
 *                    of the USBCMD register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : Value of INT_THRESHOLD_CONTROL field.
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_USBCMD__INT_THRESHOLD_CONTROL(PEHCDDATA)         \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                      USB_EHCD_USBCMD) & USB_EHCD_USBCMD_INT_THRESHOLD_MASK)\
        >> USB_EHCD_USBCMD_INT_THRESHOLD)


/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_USBCMD__ASYNCH_SCHEDULE_PARK_MODE_ENABLE
 * Description      : This macro is used to get the
 *                    ASYNCH_SCHEDULE_PARK_MODE_ENABLE field of the
 *                    USBCMD register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : Value of ASYNCH_SCHEDULE_PARK_MODE_ENABLE
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_USBCMD__ASYNCH_SCHEDULE_PARK_MODE_ENABLE(PEHCDDATA)\
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_USBCMD) &                                  \
                        USB_EHCD_USBCMD_ASYNCH_SCHEDULE_PARK_MODE_EN_MASK)  \
        >> USB_EHCD_USBCMD_ASYNCH_SCHEDULE_PARK_MODE_ENABLE)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_USBCMD__ASYCH_SCHEDULE_PARK_MODE_COUNT
 * Description      : This macro is used to get the
 *                    ASYCH_SCHEDULE_PARK_MODE_COUNT field
 *                    of the USBCMD register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : Value of ASYCH_SCHEDULE_PARK_MODE_COUNT
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_USBCMD__ASYCH_SCHEDULE_PARK_MODE_COUNT(PEHCDDATA)\
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_USBCMD) &                                  \
                        USB_EHCD_USBCMD_ASYNCH_SCHEDULE_PARK_MODE_COUNT_MASK)\
        >> USB_EHCD_USBCMD_ASYNCH_SCHEDULE_PARK_MODE_COUNT)


/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_USBCMD__LIGHT_HCRESET
 * Description      : This macro is used to get the
 *                    LIGHT_HCRESET field of the
 *                    USBCMD register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : Value of LIGHT_HC_RESET field.
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_USBCMD__LIGHT_HCRESET(PEHCDDATA)             \
                                                                        \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                  \
                        USB_EHCD_USBCMD) &                              \
                        USB_EHCD_USBCMD_LIGHT_HC_RESET_MASK)            \
        >> USB_EHCD_USBCMD_LIGHT_HC_RESET)


/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_USBCMD__INT_ON_ASYNC_ADVANCE_DOORBELL
 * Description      : This macro is used to get the
 *                    INT_ON_ASYNC_ADVANCE_DOORBELL field of the
 *                    USBCMD register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : Value of INT_ON_ASYNC_ADVANCE_DOORBELL field.
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_USBCMD__INT_ON_ASYNC_ADVANCE_DOORBELL(PEHCDDATA) \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_USBCMD) &                                  \
                        USB_EHCD_USBCMD_ASYNC_ADVANCE_DOORBELL_MASK)        \
        >> USB_EHCD_USBCMD_INTERRUPT_ON_ASYNC_ADVANCE_DOORBELL)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_USBCMD__ASYNCH_SCHEDULE_ENABLE
 * Description      : This macro is used to get the
 *                    ASYNCH_SCHEDULE_ENABLE field of the
 *                    USBCMD register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : Value of ASYNCH_SCHEDULE_ENABLE field.
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_USBCMD__ASYNCH_SCHEDULE_ENABLE(PEHCDDATA)        \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_USBCMD) &                                  \
                        USB_EHCD_USBCMD_ASYNCH_SCHEDULE_ENABLE_MASK)        \
        >> USB_EHCD_USBCMD_ASYNCHRONOUS_SCHEDULE_ENABLE)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_USBCMD__PERIODIC_SCHEDULE_ENABLE
 * Description      : This macro is used to get the
 *                    PERIODIC_SCHEDULE_ENABLE field of the
 *                    USBCMD register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : Value of PERIODIC_SCHEDULE_ENABLE field.
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_USBCMD__PERIODIC_SCHEDULE_ENABLE(PEHCDDATA)      \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_USBCMD) &                                  \
                        USB_EHCD_USBCMD_PERIODIC_SCHEDULE_ENABLE_MASK)      \
        >> USB_EHCD_USBCMD_PERIODIC_SCHEDULE_ENABLE)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_USBCMD__FRAME_LIST_SIZE
 * Description      : This macro is used to get the
 *                    FRAME_LIST_SIZE field
 *                    of the USBCMD register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : Value of FRAME_LIST_SIZE field.
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_USBCMD__FRAME_LIST_SIZE(PEHCDDATA)               \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_USBCMD) &                                  \
                        USB_EHCD_USBCMD_FRAME_LIST_SIZE_MASK)               \
        >> USB_EHCD_USBCMD_FRAME_LIST_SIZE)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_USBCMD__HCRESET
 * Description      : This macro is used to get the HCRESET field of the
 *                    USBCMD register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : Value of HCRESET field.
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_USBCMD__HCRESET(PEHCDDATA)                       \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_USBCMD) &                                  \
                        USB_EHCD_USBCMD_HCRESET_MASK)                       \
        >> USB_EHCD_USBCMD_HC_RESET)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_USBCMD__RS
 * Description      : This macro is used to get the RS field of the
 *                    USBCMD register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : Value of RS field.
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_USBCMD__RS(PEHCDDATA)                            \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_USBCMD) &                                  \
                        USB_EHCD_USBCMD_RS_MASK)                            \
        >> USB_EHCD_USBCMD_RS)

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_USBSTS__INTERRUPT_ON_ASYNC_ADVANCE
 * Description      : This macro is used to clear the
 *                    INTERRUPT_ON_ASYNC_ADVANCE field of the
 *                    USBSTS register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_USBSTS__INTERRUPT_ON_ASYNC_ADVANCE(PEHCDDATA)      \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBSTS,                                     \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_USBSTS) |       \
                       USB_EHCD_USBSTS_INTERRUPT_ON_ASYNC_ADVANCE_MASK)

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_USBSTS__HOST_SYSTEM_ERROR
 * Description      : This macro is used to clear the
 *                    HOST_SYSTEM_ERROR field of the
 *                    USBSTS register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_USBSTS__HOST_SYSTEM_ERROR(PEHCDDATA)               \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBSTS,                                     \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_USBSTS) |       \
                       USB_EHCD_USBSTS_HOST_SYSTEM_ERROR_MASK)

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_USBSTS__FRAME_LIST_ROLLOVER
 * Description      : This macro is used to clear the
 *                    FRAME_LIST_ROLLOVER field of the
 *                    USBSTS register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_USBSTS__FRAME_LIST_ROLLOVER(PEHCDDATA)             \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBSTS,                                     \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_USBSTS) |       \
                       USB_EHCD_USBSTS_FRAME_LIST_ROLLOVER_MASK)


/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_USBSTS__FRAME_LIST_ROLLOVER
 * Description      : This macro is used to clear the
 *                    FRAME_LIST_ROLLOVER field of the
 *                    USBSTS register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_USBSTS__FRAME_LIST_ROLLOVER(PEHCDDATA)             \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBSTS,                                     \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_USBSTS) |       \
                       USB_EHCD_USBSTS_FRAME_LIST_ROLLOVER_MASK)

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_USBSTS__PORT_CHANGE_DETECT
 * Description      : This macro is used to clear the
 *                    FRAME_LIST_ROLLOVER field of the
 *                    USBSTS register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_USBSTS__PORT_CHANGE_DETECT(PEHCDDATA)              \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBSTS,                                     \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_USBSTS) |       \
                       USB_EHCD_USBSTS_PORT_CHANGE_DETECT_MASK)

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_USBSTS__USBERRINT
 * Description      : This macro is used to clear the
 *                    USBERRINT field of the
 *                    USBSTS register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_USBSTS__USBERRINT(PEHCDDATA)                       \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBSTS,                                     \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_USBSTS) |       \
                       USB_EHCD_USBSTS_USBERRINT_MASK)

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_USBSTS__USBINT
 * Description      : This macro is used to clear the
 *                    USBINT field of the
 *                    USBSTS register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_USBSTS__USBINT(PEHCDDATA)                          \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBSTS,                                     \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_USBSTS) |       \
                       USB_EHCD_USBSTS_USBINT_MASK)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_USBSTS__ASYCHRONOUS_SCHEDULE_STATUS
 * Description      : This macro is used to get the
 *                    ASYCHRONOUS_SCHEDULE_STATUS field
 *                    of the USBSTS register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : Value of ASYCHRONOUS_SCHEDULE_STATUS
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_USBSTS__ASYCHRONOUS_SCHEDULE_STATUS(PEHCDDATA)   \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_USBSTS) &                                  \
                        USB_EHCD_USBSTS_ASYCH_SCHEDULE_STATUS_MASK)         \
        >> USB_EHCD_USBSTS_ASYNCHRONOUS_SCHEDULE_STATUS)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_USBSTS__PERIODIC_SCHEDULE_STATUS
 * Description      : This macro is used to get the
 *                    PERIODIC_SCHEDULE_STATUS field
 *                    of the USBSTS register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : Value of PERIODIC_SCHEDULE_STATUS
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_USBSTS__PERIODIC_SCHEDULE_STATUS(PEHCDDATA)      \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_USBSTS) &                                  \
                        USB_EHCD_USBSTS_PERIODIC_SCHEDULE_STATUS_MASK)      \
        >> USB_EHCD_USBSTS_PERIODIC_SCHEDULE_STATUS)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_USBSTS__RECLAMATION
 * Description      : This macro is used to get the
 *                    RECLAMATION field
 *                    of the USBSTS register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : Value of RECLAMATION
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_USBSTS__RECLAMATION(PEHCDDATA)                   \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_USBSTS) &                                  \
                        USB_EHCD_USBSTS_RECLAMATION_MASK)                   \
        >> USB_EHCD_USBSTS_RECLAMATION)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_USBSTS__HCHALTED
 * Description      : This macro is used to get the
 *                    HCHALTED field
 *                    of the USBSTS register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : Value of HCHALTED
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_USBSTS__HCHALTED(PEHCDDATA)                      \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_USBSTS) &                                  \
                        USB_EHCD_USBSTS_HCHALTED_MASK)                      \
        >> USB_EHCD_USBSTS_HCHALTED)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_USBSTS__INTERRUPT_ON_ASYNC_ADVANCE
 * Description      : This macro is used to get the
 *                    INTERRUPT_ON_ASYNC_ADVANCE field
 *                    of the USBSTS register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : Value of INTERRUPT_ON_ASYNC_ADVANCE
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_USBSTS__INTERRUPT_ON_ASYNC_ADVANCE(PEHCDDATA)    \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_USBSTS) &                                  \
                        USB_EHCD_USBSTS_INTERRUPT_ON_ASYNC_ADVANCE_MASK)    \
        >> USB_EHCD_USBSTS_INTERRUPT_ON_ASYNC_ADVANCE)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_USBSTS__HOST_SYSTEM_ERROR
 * Description      : This macro is used to get the
 *                    HOST_SYSTEM_ERROR field
 *                    of the USBSTS register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : Value of HOST_SYSTEM_ERROR
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_USBSTS__HOST_SYSTEM_ERROR(PEHCDDATA)             \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_USBSTS) &                                  \
                        USB_EHCD_USBSTS_HOST_SYSTEM_ERROR_MASK)             \
        >> USB_EHCD_USBSTS_HOST_SYSTEM_ERROR)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_USBSTS__FRAME_LIST_ROLLOVER
 * Description      : This macro is used to get the
 *                    FRAME_LIST_ROLLOVER field
 *                    of the USBSTS register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : Value of FRAME_LIST_ROLLOVER
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_USBSTS__FRAME_LIST_ROLLOVER(PEHCDDATA)           \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_USBSTS) &                                  \
                        USB_EHCD_USBSTS_FRAME_LIST_ROLLOVER_MASK)           \
        >> USB_EHCD_USBSTS_FRAME_LIST_ROLLOVER)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_USBSTS__PORT_CHANGE_DETECT
 * Description      : This macro is used to get the
 *                    PORT_CHANGE_DETECT field
 *                    of the USBSTS register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : Value of PORT_CHANGE_DETECT
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_USBSTS__PORT_CHANGE_DETECT(PEHCDDATA)            \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_USBSTS) &                                  \
                        USB_EHCD_USBSTS_PORT_CHANGE_DETECT_MASK)            \
        >> USB_EHCD_USBSTS_PORT_CHANGE_DETECT)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_USBSTS__USBERRINT
 * Description      : This macro is used to get the
 *                    USBERRINT field of the USBSTS register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : Value of USBERRINT
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_USBSTS__USBERRINT(PEHCDDATA)                     \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_USBSTS) &                                  \
                        USB_EHCD_USBSTS_USBERRINT_MASK)                     \
        >> USB_EHCD_USBSTS_USBERRINT)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_USBSTS__USBINT
 * Description      : This macro is used to get the
 *                    USBINT field of the USBSTS register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : Value of USBINT
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_USBSTS__USBINT(PEHCDDATA)                        \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_USBSTS) &                                  \
                        USB_EHCD_USBSTS_USBINT_MASK)                        \
        >> USB_EHCD_USBSTS_USBINT)

/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_BIT_USBINTR__INTERRUPT_ON_ASYNC_ADVANCE_ENABLE
 * Description      : This macro is used to set the
 *                    INTERRUPT_ON_ASYNC_ADVANCE_ENABLE field of the
 *                    USBINTR register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_BIT_USBINTR__INTERRUPT_ON_ASYNC_ADVANCE_ENABLE(PEHCDDATA)\
                                                                            \
{                                                                           \
        SPIN_LOCK_ISR_TAKE(&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);         \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBINTR,                                    \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_USBINTR) |      \
                       USB_EHCD_USBINTR_INTERRUPT_ON_ASYNC_ADVANCE_MASK);   \
        SPIN_LOCK_ISR_GIVE (&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);        \
}

/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_BIT_USBINTR__HOST_SYSTEM_ERROR_ENABLE
 * Description      : This macro is used to set the
 *                    HOST_SYSTEM_ERROR_ENABLE field of the
 *                    USBINTR register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_BIT_USBINTR__HOST_SYSTEM_ERROR_ENABLE(PEHCDDATA)       \
                                                                            \
{                                                                           \
        SPIN_LOCK_ISR_TAKE(&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);         \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBINTR,                                    \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_USBINTR) |      \
                       USB_EHCD_USBINTR_HOST_SYSTEM_ERROR_ENABLE_MASK);     \
        SPIN_LOCK_ISR_GIVE (&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);        \
}

/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_BIT_USBINTR__FRAME_LIST_ROLLOVER_ENABLE
 * Description      : This macro is used to set the
 *                    FRAME_LIST_ROLLOVER_ENABLE field of the
 *                    USBINTR register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_BIT_USBINTR__FRAME_LIST_ROLLOVER_ENABLE(PEHCDDATA)     \
                                                                            \
{                                                                           \
        SPIN_LOCK_ISR_TAKE(&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);         \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBINTR,                                    \
                       (USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_USBINTR) |     \
                       USB_EHCD_USBINTR_FRAME_LIST_ROLLOVER_ENABLE_MASK));  \
        SPIN_LOCK_ISR_GIVE (&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);        \
}

/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_BIT_USBINTR__PORT_CHANGE_INTERRUPT_ENABLE
 * Description      : This macro is used to set the
 *                    PORT_CHANGE_INTERRUPT_ENABLE field of the
 *                    USBINTR register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_BIT_USBINTR__PORT_CHANGE_INTERRUPT_ENABLE(PEHCDDATA)   \
                                                                            \
{                                                                           \
        SPIN_LOCK_ISR_TAKE(&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);         \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBINTR,                                    \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_USBINTR) |      \
                       USB_EHCD_USBINTR_PORT_CHANGE_INTERRUPT_ENABLE_MASK); \
        SPIN_LOCK_ISR_GIVE (&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);        \
}
/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_BIT_USBINTR__USB_ERROR_INTERRUPT_ENABLE
 * Description      : This macro is used to set the
 *                    USB_ERROR_INTERRUPT_ENABLE field of the
 *                    USBINTR register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_BIT_USBINTR__USB_ERROR_INTERRUPT_ENABLE(PEHCDDATA)     \
                                                                            \
{                                                                           \
        SPIN_LOCK_ISR_TAKE(&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);         \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBINTR,                                    \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_USBINTR) |      \
                       USB_EHCD_USBINTR_USB_ERROR_INTERRUPT_ENABLE_MASK);   \
        SPIN_LOCK_ISR_GIVE (&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);        \
}

/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_BIT_USBINTR__USB_INT_ENABLE
 * Description      : This macro is used to set the
 *                    USB_INT_ENABLE field of the
 *                    USBINTR register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_BIT_USBINTR__USB_INT_ENABLE(PEHCDDATA)                 \
                                                                            \
{                                                                           \
        SPIN_LOCK_ISR_TAKE(&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);         \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBINTR,                                    \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_USBINTR) |      \
                       USB_EHCD_USBINTR_USB_INT_ENABLE_MASK);               \
        SPIN_LOCK_ISR_GIVE (&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);        \
}
/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_USBINTR__INTERRUPT_ON_ASYNC_ADVANCE
 * Description      : This macro is used to clear the
 *                    INTERRUPT_ON_ASYNC_ADVANCE field of the
 *                    USBINTR register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_USBINTR__INTERRUPT_ON_ASYNC_ADVANCE(PEHCDDATA)     \
        SPIN_LOCK_ISR_TAKE(&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);         \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBCMD,                                     \
                       (USB_EHCD_READ_REG(PEHCDDATA,                        \
                                     USB_EHCD_USBINTR) &                    \
                       (~USB_EHCD_USBINTR_INTERRUPT_ON_ASYNC_ADVANCE_MASK));\
        SPIN_LOCK_ISR_GIVE (&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);    
/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_USBINTR__HOST_SYSTEM_ERROR_ENABLE
 * Description      : This macro is used to clear the
 *                    HOST_SYSTEM_ERROR_ENABLE field of the
 *                    USBINTR register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_USBINTR__HOST_SYSTEM_ERROR_ENABLE(PEHCDDATA)       \
        SPIN_LOCK_ISR_TAKE(&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);         \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBCMD,                                     \
                       (USB_EHCD_READ_REG(PEHCDDATA,                        \
                                     USB_EHCD_USBINTR) &                    \
                       (~USB_EHCD_USBINTR_HOST_SYSTEM_ERROR_ENABLE_MASK));  \
        SPIN_LOCK_ISR_GIVE (&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_USBINTR__FRAME_LIST_ROLLOVER_ENABLE
 * Description      : This macro is used to clear the
 *                    FRAME_LIST_ROLLOVER_ENABLE field of the
 *                    USBINTR register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_USBINTR__FRAME_LIST_ROLLOVER_ENABLE(PEHCDDATA)     \
        SPIN_LOCK_ISR_TAKE(&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);         \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBCMD,                                     \
                       (USB_EHCD_READ_REG(PEHCDDATA,                        \
                                     USB_EHCD_USBINTR) &                    \
                       (~USB_EHCD_USBINTR_FRAME_LIST_ROLLOVER_ENABLE_MASK));\
        SPIN_LOCK_ISR_GIVE (&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_USBINTR__PORT_CHANGE_INTERRUPT_ENABLE
 * Description      : This macro is used to clear the
 *                    PORT_CHANGE_INTERRUPT_ENABLE field of the
 *                    USBINTR register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_USBINTR__PORT_CHANGE_INTERRUPT_ENABLE(PEHCDDATA)     \
        SPIN_LOCK_ISR_TAKE(&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);           \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                         \
                       USB_EHCD_USBCMD,                                       \
                       (USB_EHCD_READ_REG(PEHCDDATA,                          \
                                     USB_EHCD_USBINTR) &                      \
                       (~USB_EHCD_USBINTR_PORT_CHANGE_INTERRUPT_ENABLE_MASK));\
        SPIN_LOCK_ISR_GIVE (&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_USBINTR__USB_ERROR_INTERRUPT_ENABLE
 * Description      : This macro is used to clear the
 *                    USB_ERROR_INTERRUPT_ENABLE field of the
 *                    USBINTR register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_USBINTR__USB_ERROR_INTERRUPT_ENABLE(PEHCDDATA)     \
        SPIN_LOCK_ISR_TAKE(&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);         \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBCMD,                                     \
                       (USB_EHCD_READ_REG(PEHCDDATA,                        \
                                     USB_EHCD_USBINTR) &                    \
                       (~USB_EHCD_USBINTR_USB_ERROR_INTERRUPT_ENABLE_MASK));\
        SPIN_LOCK_ISR_GIVE (&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_USBINTR__USB_INT_ENABLE
 * Description      : This macro is used to clear the
 *                    USB_INT_ENABLE field of the
 *                    USBINTR register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_USBINTR__USB_INT_ENABLE(PEHCDDATA)                 \
        SPIN_LOCK_ISR_TAKE(&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);         \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_USBCMD,                                     \
                       (USB_EHCD_READ_REG(PEHCDDATA,                        \
                                     USB_EHCD_USBINTR) &                    \
                       (~USB_EHCD_USBINTR_USB_INT_ENABLE_MASK));            \
        SPIN_LOCK_ISR_GIVE (&spinLockIsrEhcd[PEHCDDATA->uBusIndex]);

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_USBINTR__INTERRUPT_ON_ASYNC_ADVANCE_ENABLE
 * Description      : This macro is used to get the
 *                    INTERRUPT_ON_ASYNC_ADVANCE_ENABLE field
 *                    of the USBINTR register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : Value of INTERRUPT_ON_ASYNC_ADVANCE_ENABLE
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_USBSTS__INTERRUPT_ON_ASYNC_ADVANCE_ENABLE(PEHCDDATA)\
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_USBSTS) &                                  \
                        USB_EHCD_USBINTR_INTERRUPT_ON_ASYNC_ADVANCE_MASK)   \
        >> USB_EHCD_USBINTR_INTERRUPT_ON_ASYNC_ADVANCE_ENABLE)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_USBINTR__HOST_SYSTEM_ERROR_ENABLE
 * Description      : This macro is used to get the
 *                    HOST_SYSTEM_ERROR_ENABLE field
 *                    of the USBINTR register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : Value of HOST_SYSTEM_ERROR_ENABLE
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_USBSTS__HOST_SYSTEM_ERROR_ENABLE(PEHCDDATA)      \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_USBSTS) &                                  \
                        USB_EHCD_USBINTR_HOST_SYSTEM_ERROR_ENABLE_MASK)     \
        >> USB_EHCD_USBINTR_HOST_SYSTEM_ERROR_ENABLE)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_USBINTR__FRAME_LIST_ROLLOVER_ENABLE
 * Description      : This macro is used to get the
 *                    FRAME_LIST_ROLLOVER_ENABLE field
 *                    of the USBINTR register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : Value of FRAME_LIST_ROLLOVER_ENABLE
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_USBSTS__FRAME_LIST_ROLLOVER_ENABLE(PEHCDDATA)    \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_USBSTS) &                                  \
                        USB_EHCD_USBINTR_FRAME_LIST_ROLLOVER_ENABLE_MASK)   \
        >> USB_EHCD_USBINTR_FRAME_LIST_ROLLOVER_ENABLE)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_USBINTR__PORT_CHANGE_INTERRUPT_ENABLE
 * Description      : This macro is used to get the
 *                    PORT_CHANGE_INTERRUPT_ENABLE field
 *                    of the USBINTR register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : Value of PORT_CHANGE_INTERRUPT_ENABLE
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_USBSTS__PORT_CHANGE_INTERRUPT_ENABLE(PEHCDDATA)  \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_USBSTS) &                                  \
                        USB_EHCD_USBINTR_PORT_CHANGE_INTERRUPT_ENABLE_MASK) \
        >> USB_EHCD_USBINTR_PORT_CHANGE_INTERRUPT_ENABLE)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_USBINTR__USB_ERROR_INTERRUPT_ENABLE
 * Description      : This macro is used to get the
 *                    USB_ERROR_INTERRUPT_ENABLE field
 *                    of the USBINTR register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : Value of USB_ERROR_INTERRUPT_ENABLE
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_USBSTS__USB_ERROR_INTERRUPT_ENABLE(PEHCDDATA)    \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_USBSTS) &                                  \
                        USB_EHCD_USBINTR_USB_ERROR_INTERRUPT_ENABLE_MASK)   \
        >> USB_EHCD_USBINTR_USB_ERROR_INTERRUPT_ENABLE)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_USBINTR__USB_INT_ENABLE
 * Description      : This macro is used to get the
 *                    USB_INT_ENABLE field
 *                    of the USBINTR register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : Value of USB_INT_ENABLE
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_USBSTS__USB_INT_ENABLE(PEHCDDATA)                \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_USBSTS) &                                  \
                        USB_EHCD_USBINTR_USB_INT_ENABLE_MASK)               \
        >> USB_EHCD_USBINTR_USB_INT_ENABLE)

/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_FIELD_FRINDEX__FRAME_INDEX
 * Description      : This macro is used to set the
 *                    FRAME_INDEX field
 *                    of the FRINDEX register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 *                    VALUE     IN Value to be set in the field
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_FIELD_FRINDEX__FRAME_INDEX(PEHCDDATA,                  \
                                            VALUE)                          \
                                                                            \
{                                                                           \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_FRINDEX,                                    \
                       (USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_FRINDEX) &     \
                       ~(USB_EHCD_FRINDEX_FRAME_INDEX_MASK)) |              \
                       (VALUE << USB_EHCD_FRINDEX_FRAME_INDEX));            \
}

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_FRINDEX__FRAME_INDEX
 * Description      : This macro is used to get the
 *                    FRAME_INDEX field
 *                    of the FRINDEX register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : Value of FRAME_INDEX
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_FRINDEX__FRAME_INDEX(PEHCDDATA)                  \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_FRINDEX) &                                 \
                        USB_EHCD_FRINDEX_FRAME_INDEX_MASK)                  \
        >> USB_EHCD_FRINDEX_FRAME_INDEX)

/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_FIELD_PERIODICLISTBASE__BASE_ADDRESS
 * Description      : This macro is used to set the
 *                    BASE_ADDRESS field
 *                    of the PERIODICLISTBASE register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 *                    VALUE     IN Value to be set in the field
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_FIELD_PERIODICLISTBASE__BASE_ADDRESS(PEHCDDATA,           \
                                                      VALUE)                   \
                                                                               \
{                                                                              \
    USB_EHCD_WRITE_REG(PEHCDDATA,                                              \
                   USB_EHCD_PERIODICLISTBASE,                                  \
                   (USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_PERIODICLISTBASE) &   \
                   ~(USB_EHCD_PERIODICLISTBASE_BASE_ADDRESS_MASK)));           \
                                                                               \
    /* Workaround for FSL EHCI - Write 0x0000 to the PERIODICLISTBASE */       \
    /* two times in succession before setting PERIODICLISTBASE        */       \
    USB_EHCD_WRITE_REG(PEHCDDATA,                                              \
                   USB_EHCD_PERIODICLISTBASE,                                  \
                   (USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_PERIODICLISTBASE) &   \
                   ~(USB_EHCD_PERIODICLISTBASE_BASE_ADDRESS_MASK)));           \
                                                                               \
    USB_EHCD_WRITE_REG(PEHCDDATA,                                              \
                   USB_EHCD_PERIODICLISTBASE,                                  \
                   USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_PERIODICLISTBASE) |    \
                   (VALUE << USB_EHCD_PERIODICLISTBASE_BASE_ADDRESS));         \
}

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_PERIODICLISTBASE__BASE_ADDRESS
 * Description      : This macro is used to get the
 *                    BASE_ADDRESS field
 *                    of the PERIODICLISTBASE register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : Value of BASE_ADDRESS
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_PERIODICLISTBASE__BASE_ADDRESS(PEHCDDATA)        \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_PERIODICLISTBASE) &                        \
                        USB_EHCD_PERIODICLISTBASE_BASE_ADDRESS_MASK)        \
        >> USB_EHCD_PERIODICLISTBASE_BASE_ADDRESS)

/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_FIELD_ASYNCLISTADDR__LINK_PTR_LOW
 * Description      : This macro is used to set the
 *                    LINK_PTR_LOW field
 *                    of the ASYNCLISTADDR register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 *                    VALUE     IN Value to be set in the field
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_FIELD_ASYNCLISTADDR__LINK_PTR_LOW(PEHCDDATA,           \
                                                   VALUE)                   \
                                                                            \
{                                                                           \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                   USB_EHCD_ASYNCLISTADDR,                                  \
                   (USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_ASYNCLISTADDR) &   \
                   ~(USB_EHCD_ASYNCHLISTADDR_LINK_PTR_LOW_MASK)) |          \
                   (VALUE << USB_EHCD_ASYNCHLISTADDR_LINK_PTR_LOW));        \
}

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_ASYNCLISTADDR__LINK_PTR_LOW
 * Description      : This macro is used to get the
 *                    LINK_PTR_LOW field
 *                    of the ASYNCLISTADDR register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : Value of LINK_PTR_LOW
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_ASYNCLISTADDR__LINK_PTR_LOW(PEHCDDATA)           \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_ASYNCLISTADDR) &                           \
                        USB_EHCD_ASYNCHLISTADDR_LINK_PTR_LOW_MASK)          \
        >> USB_EHCD_ASYNCHLISTADDR_LINK_PTR_LOW)

/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_BIT_CONFIGFLAG__CF
 * Description      : This macro is used to set the
 *                    CF field of the CONFIGFLAG register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_BIT_CONFIGFLAG__CF(PEHCDDATA)                          \
                                                                            \
{                                                                           \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_CONFIGFLAG,                                 \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_CONFIGFLAG) |   \
                       USB_EHCD_CONFIGFLAG_CF_MASK);                        \
}

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_CONFIGFLAG__CF
 * Description      : This macro is used to clear the
 *                    CF field of the CONFIGFLAG register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_CONFIGFLAG__CF(PEHCDDATA)                          \
                                                                            \
{                                                                           \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_CONFIGFLAG,                                 \
                       (USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_CONFIGFLAG) &  \
                       ~(USB_EHCD_CONFIGFLAG_CF_MASK)));                    \
}
/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_CONFIGFLAG__CF
 * Description      : This macro is used to get the
 *                    CF field of the CONFIGFLAG register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 * Return Type      : Value of CF
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_CONFIGFLAG__CF(PEHCDDATA)                        \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_CONFIGFLAG) &                              \
                        USB_EHCD_CONFIGFLAG_CF_MASK)                        \
        >> USB_EHCD_CONFIGFLAG_CF)

/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_BIT_PORTSC__WKOC_E
 * Description      : This macro is used to set the
 *                    WKOC_E field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_BIT_PORTSC__WKOC_E(PEHCDDATA,                          \
                                    PORT_INDEX)                             \
{                                                                           \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_PORTSC(PORT_INDEX),                         \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_PORTSC(PORT_INDEX)) | \
                       (USB_EHCD_PORTSC_WKOC_E_MASK));                      \
}

/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_BIT_PORTSC__WKDSCNNT_E
 * Description      : This macro is used to set the
 *                    WKDSCNNT_E field of the PORTSC register.
 * Parameters       : PEHCDDATA IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_BIT_PORTSC__WKDSCNNT_E(PEHCDDATA,                      \
                                        PORT_INDEX)                         \
{                                                                           \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_PORTSC,                                     \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_PORTSC(PORT_INDEX)) | \
                       (USB_EHCD_PORTSC_WKDSCNNT_E_MASK));                  \
}

/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_BIT_PORTSC__WKCNNT_E
 * Description      : This macro is used to set the
 *                    WKCNNT_E field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_BIT_PORTSC__WKCNNT_E(PEHCDDATA,                        \
                                      PORT_INDEX)                           \
{                                                                           \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_PORTSC(PORT_INDEX),                         \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_PORTSC(PORT_INDEX)) | \
                       (USB_EHCD_PORTSC_WKCNNT_E_MASK));                    \
}

/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_FIELD_PORTSC__PORT_TEST_CONTROL
 * Description      : This macro is used to set the
 *                    PORT_TEST_CONTROL field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register
 *                    VALUE      IN Value to be set in the PORT_TEST_CONTROL
 *                                  field.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_FIELD_PORTSC__PORT_TEST_CONTROL(PEHCDDATA,             \
                                                 PORT_INDEX,                \
                                                 VALUE)                     \
{                                                                           \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
               USB_EHCD_PORTSC(PORT_INDEX),                                 \
               (USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_PORTSC(PORT_INDEX)) &  \
               ~(USB_EHCD_PORTSC_PORT_TEST_CONTROL_MASK)) |                 \
               (VALUE << USB_EHCD_PORTSC_PORT_TEST_CONTROL));               \
}

/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_FIELD_PORTSC__PORT_INDICATOR_CONTROL
 * Description      : This macro is used to set the
 *                    PORT_INDICATOR_CONTROL field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register
 *                    VALUE      IN Value to be set in the PORT_INDICATOR_CONTROL
 *                                  field.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_FIELD_PORTSC__PORT_INDICATOR_CONTROL(PEHCDDATA,        \
                                                      PORT_INDEX,           \
                                                      VALUE)                \
{                                                                           \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
               USB_EHCD_PORTSC(PORT_INDEX),                                 \
               (USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_PORTSC(PORT_INDEX)) &  \
               ~(USB_EHCD_PORTSC_PORT_INDICATOR_CONTROL_MASK)) |            \
               (VALUE << USB_EHCD_PORTSC_PORT_INDICATOR_CONTROL));          \
}
/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_BIT_PORTSC__PORT_OWNER
 * Description      : This macro is used to set the
 *                    PORT_OWNER field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_BIT_PORTSC__PORT_OWNER(PEHCDDATA,                      \
                                        PORT_INDEX)                         \
                                                                            \
{                                                                           \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_PORTSC(PORT_INDEX),                         \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_PORTSC(PORT_INDEX)) |   \
                       USB_EHCD_PORTSC_PORT_OWNER_MASK);                    \
}

/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_BIT_PORTSC__PORT_POWER
 * Description      : This macro is used to set the
 *                    PORT_POWER field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_BIT_PORTSC__PORT_POWER(PEHCDDATA,                      \
                                        PORT_INDEX)                         \
                                                                            \
{                                                                           \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_PORTSC(PORT_INDEX),                         \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_PORTSC(PORT_INDEX)) |   \
                       USB_EHCD_PORTSC_PORT_POWER_MASK);                    \
}
/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_BIT_PORTSC__PORT_RESET
 * Description      : This macro is used to set the
 *                    PORT_RESET field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_BIT_PORTSC__PORT_RESET(PEHCDDATA,                      \
                                        PORT_INDEX)                         \
                                                                            \
{                                                                           \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_PORTSC(PORT_INDEX),                         \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_PORTSC(PORT_INDEX)) |   \
                       USB_EHCD_PORTSC_PORT_RESET_MASK);                    \
}
/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_BIT_PORTSC__SUSPEND
 * Description      : This macro is used to set the
 *                    SUSPEND field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_BIT_PORTSC__SUSPEND(PEHCDDATA,                         \
                                     PORT_INDEX)                            \
                                                                            \
{                                                                           \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_PORTSC(PORT_INDEX),                         \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_PORTSC(PORT_INDEX)) |   \
                       USB_EHCD_PORTSC_SUSPEND_MASK);                       \
}

/*******************************************************************************
 * Macro Name       : USB_EHCD_SET_BIT_PORTSC__FORCE_PORT_RESUME
 * Description      : This macro is used to set the
 *                    FORCE_PORT_RESUME field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_SET_BIT_PORTSC__FORCE_PORT_RESUME(PEHCDDATA,               \
                                               PORT_INDEX)                  \
                                                                            \
{                                                                           \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_PORTSC(PORT_INDEX),                         \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_PORTSC(PORT_INDEX)) |   \
                       USB_EHCD_PORTSC_FORCE_PORT_RESUME_MASK);             \
}
/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_PORTSC__WKOC_E
 * Description      : This macro is used to clear the
 *                    WKOC_E field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_PORTSC__WKOC_E(PEHCDDATA,                          \
                                    PORT_INDEX)                             \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_PORTSC(PORT_INDEX),                         \
                       (USB_EHCD_READ_REG(PEHCDDATA,                        \
                                     USB_EHCD_PORTSC(PORT_INDEX)) &         \
                       ~(USB_EHCD_PORTSC_WKOC_E_MASK)))

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_PORTSC__WKDSCNNT_E
 * Description      : This macro is used to clear the
 *                    WKDSCNNT_E field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_PORTSC__WKDSCNNT_E(PEHCDDATA,                      \
                                        PORT_INDEX)                         \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_PORTSC(PORT_INDEX),                         \
                       (USB_EHCD_READ_REG(PEHCDDATA,                        \
                                     USB_EHCD_PORTSC(PORT_INDEX)) &         \
                       ~(USB_EHCD_PORTSC_WKDSCNNT_E_MASK)))

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_PORTSC__WKCNNT_E
 * Description      : This macro is used to clear the
 *                    WKCNNT_E field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_PORTSC__WKCNNT_E(PEHCDDATA,                        \
                                      PORT_INDEX)                           \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_PORTSC(PORT_INDEX),                         \
                       (USB_EHCD_READ_REG(PEHCDDATA,                        \
                                     USB_EHCD_PORTSC(PORT_INDEX)) &         \
                       ~(USB_EHCD_PORTSC_WKCNNT_E_MASK)))

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_PORTSC__PORT_TEST_CONTROL
 * Description      : This macro is used to clear the
 *                    PORT_TEST_CONTROL field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_PORTSC__PORT_TEST_CONTROL(PEHCDDATA,               \
                                               PORT_INDEX)                  \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_PORTSC(PORT_INDEX),                         \
                       (USB_EHCD_READ_REG(PEHCDDATA,                        \
                                     USB_EHCD_PORTSC(PORT_INDEX)) &         \
                       ~(USB_EHCD_PORTSC_PORT_TEST_CONTROL_MASK)))

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_PORTSC__PORT_INDICATOR_CONTROL
 * Description      : This macro is used to clear the
 *                    PORT_INDICATOR_CONTROL field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_PORTSC__PORT_INDICATOR_CONTROL(PEHCDDATA,          \
                                                    PORT_INDEX)             \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_PORTSC(PORT_INDEX),                         \
                       (USB_EHCD_READ_REG(PEHCDDATA,                        \
                                     USB_EHCD_PORTSC(PORT_INDEX)) &         \
                       ~(USB_EHCD_PORTSC_PORT_INDICATOR_CONTROL_MASK)))
/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_PORTSC__PORT_OWNER
 * Description      : This macro is used to clear the
 *                    PORT_OWNER field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_PORTSC__PORT_OWNER(PEHCDDATA,                      \
                                        PORT_INDEX)                         \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_PORTSC(PORT_INDEX),                         \
                       (USB_EHCD_READ_REG(PEHCDDATA,                        \
                                     USB_EHCD_PORTSC(PORT_INDEX)) &         \
                       ~(USB_EHCD_PORTSC_PORT_OWNER_MASK)))

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_PORTSC__PORT_POWER
 * Description      : This macro is used to clear the
 *                    PORT_POWER field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_PORTSC__PORT_POWER(PEHCDDATA,                      \
                                        PORT_INDEX)                         \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_PORTSC(PORT_INDEX),                         \
                       (USB_EHCD_READ_REG(PEHCDDATA,                        \
                                     USB_EHCD_PORTSC(PORT_INDEX)) &         \
                       ~(USB_EHCD_PORTSC_PORT_POWER_MASK)))

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_PORTSC__PORT_RESET
 * Description      : This macro is used to clear the
 *                    PORT_RESET field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_PORTSC__PORT_RESET(PEHCDDATA,                      \
                                        PORT_INDEX)                         \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_PORTSC(PORT_INDEX),                         \
                       (USB_EHCD_READ_REG(PEHCDDATA,                        \
                                     USB_EHCD_PORTSC(PORT_INDEX)) &         \
                       ~(USB_EHCD_PORTSC_PORT_RESET_MASK)))

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_PORTSC__FORCE_PORT_RESUME
 * Description      : This macro is used to clear the
 *                    FORCE_PORT_RESUME field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_PORTSC__FORCE_PORT_RESUME(PEHCDDATA,               \
                                               PORT_INDEX)                  \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_PORTSC(PORT_INDEX),                         \
                       (USB_EHCD_READ_REG(PEHCDDATA,                        \
                                     USB_EHCD_PORTSC(PORT_INDEX)) &         \
                       ~(USB_EHCD_PORTSC_FORCE_PORT_RESUME_MASK)))

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_PORTSC__OVER_CURRENT_CHANGE
 * Description      : This macro is used to clear the
 *                    OVER_CURRENT_CHANGE field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_PORTSC__OVER_CURRENT_CHANGE(PEHCDDATA,             \
                                                 PORT_INDEX)                \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_PORTSC(PORT_INDEX),                         \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_PORTSC(PORT_INDEX)) |   \
                       USB_EHCD_PORTSC_OVER_CURRENT_CHANGE_MASK)

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_PORTSC__PORT_ENABLE_DISABLE_CHANGE
 * Description      : This macro is used to clear the
 *                    PORT_ENABLE_DISABLE_CHANGE field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_PORTSC__PORT_ENABLE_DISABLE_CHANGE(PEHCDDATA,      \
                                                        PORT_INDEX)         \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_PORTSC(PORT_INDEX),                         \
                       USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_PORTSC(PORT_INDEX)) |   \
                       USB_EHCD_PORTSC_PORT_ENABLE_DISABLE_CHANGE_MASK)

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_PORTSC__PORT_ENABLED_DISABLED
 * Description      : This macro is used to clear the
 *                    PORT_ENABLED_DISABLED field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_PORTSC__PORT_ENABLED_DISABLED(PEHCDDATA,           \
                                                   PORT_INDEX)              \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_PORTSC(PORT_INDEX),                         \
                       (USB_EHCD_READ_REG(PEHCDDATA,                        \
                                     USB_EHCD_PORTSC(PORT_INDEX)) &         \
                       ~(USB_EHCD_PORTSC_PORT_ENABLED_DISABLED_MASK)))

/*******************************************************************************
 * Macro Name       : USB_EHCD_CLR_BIT_PORTSC__CONNECT_STATUS_CHANGE
 * Description      : This macro is used to clear the
 *                    CONNECT_STATUS_CHANGE field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register.
 * Return Type      : None.
 ******************************************************************************/
#define USB_EHCD_CLR_BIT_PORTSC__CONNECT_STATUS_CHANGE(PEHCDDATA,           \
                                                   PORT_INDEX)              \
                                                                            \
        USB_EHCD_WRITE_REG(PEHCDDATA,                                       \
                       USB_EHCD_PORTSC(PORT_INDEX),                         \
                       (USB_EHCD_READ_REG(PEHCDDATA,USB_EHCD_PORTSC(PORT_INDEX)) |   \
                       USB_EHCD_PORTSC_CONNECT_STATUS_CHANGE_MASK))
/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_PORTSC__WKOC_E
 * Description      : This macro is used to get the
 *                    WKOC_E field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register.
 * Return Type      : Value of WKOC_E
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_PORTSC__WKOC_E(PEHCDDATA,                        \
                                      PORT_INDEX)                           \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_PORTSC(PORT_INDEX)) &                      \
                        USB_EHCD_PORTSC_WKOC_E_MASK)                        \
        >> USB_EHCD_PORTSC_WKOC_E)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_PORTSC__WKDSCNNT_E
 * Description      : This macro is used to get the
 *                    WKDSCNNT_E field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register.
 * Return Type      : Value of WKDSCNNT_E
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_PORTSC__WKDSCNNT_E(PEHCDDATA,                    \
                                          PORT_INDEX)                       \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_PORTSC(PORT_INDEX)) &                      \
                        USB_EHCD_PORTSC_WKDSCNNT_E_MASK)                    \
        >> USB_EHCD_PORTSC_WKDSCNNT_E)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_PORTSC__WKCNNT_E
 * Description      : This macro is used to get the
 *                    WKCNNT_E field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register.
 * Return Type      : Value of WKCNNT_E
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_PORTSC__WKCNNT_E(PEHCDDATA,                      \
                                        PORT_INDEX)                         \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_PORTSC(PORT_INDEX)) &                      \
                        USB_EHCD_PORTSC_WKCNNT_E_MASK)                      \
        >> USB_EHCD_PORTSC_WKCNNT_E)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_PORTSC__PORT_TEST_CONTROL
 * Description      : This macro is used to get the
 *                    PORT_TEST_CONTROL field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register.
 * Return Type      : Value of PORT_TEST_CONTROL
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_PORTSC__PORT_TEST_CONTROL(PEHCDDATA,             \
                                                 PORT_INDEX)                \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_PORTSC(PORT_INDEX)) &                      \
                        USB_EHCD_PORTSC_PORT_TEST_CONTROL_MASK)             \
        >> USB_EHCD_PORTSC_PORT_TEST_CONTROL)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_PORTSC__PORT_INDICATOR_CONTROL
 * Description      : This macro is used to get the
 *                    PORT_INDICATOR_CONTROL field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register.
 * Return Type      : Value of PORT_INDICATOR_CONTROL
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_PORTSC__PORT_INDICATOR_CONTROL(PEHCDDATA,        \
                                                      PORT_INDEX)           \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_PORTSC(PORT_INDEX)) &                      \
                        USB_EHCD_PORTSC_PORT_INDICATOR_CONTROL_MASK)        \
        >> USB_EHCD_PORTSC_PORT_INDICATOR_CONTROL)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_PORTSC__PORT_OWNER
 * Description      : This macro is used to get the
 *                    PORT_OWNER field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register.
 * Return Type      : Value of PORT_OWNER
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_PORTSC__PORT_OWNER(PEHCDDATA,                    \
                                          PORT_INDEX)                       \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_PORTSC(PORT_INDEX)) &                      \
                        USB_EHCD_PORTSC_PORT_OWNER_MASK)                    \
        >> USB_EHCD_PORTSC_PORT_OWNER)
/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_PORTSC__PORT_POWER
 * Description      : This macro is used to get the
 *                    PORT_POWER field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register.
 * Return Type      : Value of PORT_POWER
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_PORTSC__PORT_POWER(PEHCDDATA,                    \
                                          PORT_INDEX)                       \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_PORTSC(PORT_INDEX)) &                      \
                        USB_EHCD_PORTSC_PORT_POWER_MASK)                    \
        >> USB_EHCD_PORTSC_PORT_POWER)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_PORTSC__LINE_STATUS
 * Description      : This macro is used to get the
 *                    LINE_STATUS field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register.
 * Return Type      : Value of LINE_STATUS
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_PORTSC__LINE_STATUS(PEHCDDATA,                   \
                                           PORT_INDEX)                      \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_PORTSC(PORT_INDEX)) &                      \
                        USB_EHCD_PORTSC_LINE_STATUS_MASK)                   \
        >> USB_EHCD_PORTSC_LINE_STATUS)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_PORTSC__PSPD
 * Description      : This macro is used to get the
 *                    PSPD field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register.
 * Return Type      : Value of PSPD
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_PORTSC__PSPD(PEHCDDATA,                          \
                                           PORT_INDEX)                      \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_PORTSC(PORT_INDEX)) &                      \
                        USB_EHCD_PORTSC_PSPD_MASK)                          \
        >> USB_EHCD_PORTSC_PSPD)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_PORTSC__PORT_RESET
 * Description      : This macro is used to get the
 *                    PORT_RESET field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register.
 * Return Type      : Value of PORT_RESET
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_PORTSC__PORT_RESET(PEHCDDATA,                    \
                                          PORT_INDEX)                       \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_PORTSC(PORT_INDEX)) &                      \
                        USB_EHCD_PORTSC_PORT_RESET_MASK)                    \
        >> USB_EHCD_PORTSC_PORT_RESET)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_PORTSC__SUSPEND
 * Description      : This macro is used to get the
 *                    SUSPEND field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register.
 * Return Type      : Value of SUSPEND
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_PORTSC__SUSPEND(PEHCDDATA,                       \
                                       PORT_INDEX)                          \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_PORTSC(PORT_INDEX)) &                      \
                        USB_EHCD_PORTSC_SUSPEND_MASK)                       \
        >> USB_EHCD_PORTSC_SUSPEND)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_PORTSC__FORCE_PORT_RESUME
 * Description      : This macro is used to get the
 *                    FORCE_PORT_RESUME field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register.
 * Return Type      : Value of FORCE_PORT_RESUME
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_PORTSC__FORCE_PORT_RESUME(PEHCDDATA,             \
                                                 PORT_INDEX)                \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_PORTSC(PORT_INDEX)) &                      \
                        USB_EHCD_PORTSC_FORCE_PORT_RESUME_MASK)             \
        >> USB_EHCD_PORTSC_FORCE_PORT_RESUME)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_PORTSC__OVER_CURRENT_CHANGE
 * Description      : This macro is used to get the
 *                    OVER_CURRENT_CHANGE field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register.
 * Return Type      : Value of OVER_CURRENT_CHANGE
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_PORTSC__OVER_CURRENT_CHANGE(PEHCDDATA,           \
                                                   PORT_INDEX)              \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_PORTSC(PORT_INDEX)) &                      \
                        USB_EHCD_PORTSC_OVER_CURRENT_CHANGE_MASK)           \
        >> USB_EHCD_PORTSC_OVER_CURRENT_CHANGE)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_PORTSC__OVER_CURRENT_ACTIVE
 * Description      : This macro is used to get the
 *                    OVER_CURRENT_ACTIVE field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register.
 * Return Type      : Value of OVER_CURRENT_ACTIVE
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_PORTSC__OVER_CURRENT_ACTIVE(PEHCDDATA,           \
                                                   PORT_INDEX)              \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_PORTSC(PORT_INDEX)) &                      \
                        USB_EHCD_PORTSC_OVER_CURRENT_ACTIVE_MASK)           \
        >> USB_EHCD_PORTSC_OVER_CURRENT_ACTIVE)
/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_PORTSC__PORT_ENABLE_CHANGE
 * Description      : This macro is used to get the
 *                    PORT_ENABLE_CHANGE field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register.
 * Return Type      : Value of PORT_ENABLE_CHANGE
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_PORTSC__PORT_ENABLE_CHANGE(PEHCDDATA,            \
                                                  PORT_INDEX)               \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_PORTSC(PORT_INDEX)) &                      \
                        USB_EHCD_PORTSC_PORT_ENABLE_DISABLE_CHANGE_MASK)    \
        >> USB_EHCD_PORTSC_PORT_ENABLE_DISABLE_CHANGE)
/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_PORTSC__PORT_ENABLED_DISABLED
 * Description      : This macro is used to get the
 *                    PORT_ENABLED_DISABLED field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register.
 * Return Type      : Value of PORT_ENABLED_DISABLED
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_PORTSC__PORT_ENABLED_DISABLED(PEHCDDATA,         \
                                                     PORT_INDEX)            \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_PORTSC(PORT_INDEX)) &                      \
                        USB_EHCD_PORTSC_PORT_ENABLED_DISABLED_MASK)         \
        >> USB_EHCD_PORTSC_PORT_ENABLED_DISABLED)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_PORTSC__CONNECT_STATUS_CHANGE
 * Description      : This macro is used to get the
 *                    CONNECT_STATUS_CHANGE field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register.
 * Return Type      : Value of CONNECT_STATUS_CHANGE
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_PORTSC__CONNECT_STATUS_CHANGE(PEHCDDATA,         \
                                                     PORT_INDEX)            \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_PORTSC(PORT_INDEX)) &                      \
                        USB_EHCD_PORTSC_CONNECT_STATUS_CHANGE_MASK)         \
        >> USB_EHCD_PORTSC_CONNECT_STATUS_CHANGE)

/*******************************************************************************
 * Macro Name       : USB_EHCD_GET_FIELD_PORTSC__CURRENT_CONNECT_STATUS
 * Description      : This macro is used to get the
 *                    CURRENT_CONNECT_STATUS field of the PORTSC register.
 * Parameters       : PEHCDDATA  IN Pointer to the USB_EHCD_DATA structure.
 *                    PORT_INDEX IN Index of the port register.
 * Return Type      : Value of CURRENT_CONNECT_STATUS
 ******************************************************************************/
#define USB_EHCD_GET_FIELD_PORTSC__CURRENT_CONNECT_STATUS(PEHCDDATA,        \
                                                      PORT_INDEX)           \
                                                                            \
        ((USB_EHCD_READ_REG(PEHCDDATA,                                      \
                        USB_EHCD_PORTSC(PORT_INDEX)) &                      \
                        USB_EHCD_PORTSC_CURRENT_CONNECT_STATUS_MASK)        \
        >> USB_EHCD_PORTSC_CURRENT_CONNECT_STATUS)


#ifdef    __cplusplus
}
#endif

#endif /* End of __INCusbEhcdHalh */
/*************************** End of file usbEhcdHal.h**************************/

