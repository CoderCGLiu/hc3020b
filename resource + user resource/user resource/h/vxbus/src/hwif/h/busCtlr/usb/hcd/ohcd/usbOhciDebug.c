/* usbOhciDebug.c - USB OHCI Debug Routines */

/*
 * Copyright (c) 2004-2006, 2008, 2011, 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2004-2006, 2008, 2011, 2013 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
modification history
--------------------
01q,03may13,wyy  Remove compiler warning (WIND00356717)
01p,21apr13,wyy  Cut the relationship among busIndex of HCDData, unitNumber of
                 vxBus Device, and HC count (such as g_EHCDControllerCount or
                 g_UhcdHostControllerCount) to dynamically release or announce
                 a HCD driver (WIND00362065)
01o,04jan13,s_z  Remove compiler warning (WIND00390357)
01n,13dec11,m_y  Modify according to code check result (WIND00319317)
01m,08aug11,w_x  Fix usbOhcdShow helpers endian issues (WIND00286517)
01l,22jul10,m_y  Modify host controller index compare
01k,02jul10,m_y  Modify for coding convention
01j,17jun10,m_y  Add usbOhciDumpMemTable routine to dump memTable list.
01i,18may10,m_y  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
01h,13jan10,ghs  vxWorks 6.9 LP64 adapting
01g,11jul08,w_x, Fix usbOhciDebug.c can not build issue (WIND00127532)
01f,11jul08,w_x  Fix OHCI power on/off ports inconsistent code issue (WIND00120108)
01e,08oct06,ami  Changes for USB-vxBus changes
01d,28mar05,pdg  non-PCI changes
01c,11oct04,ami  Apigen Changes
01b,27jun03,nld Changing the code to WRS standards
01a,17mar02,ssh Initial Version
*/

/*
DESCRIPTION

This file contains functions for display the USB OHCI registers, memory,
endpoint desriptor, transfer descriptor etc. This interfaces exposed from
this file can used to debug the OHCI driver.

INCLUDE FILES: usbOhci.h, usbOhciRegisterInfo.h, usbOhciTransferManagement.h
*/

/*
 INTERNAL
 ******************************************************************************
 * Filename         : OHCI_Debug.c
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 * Author           : Sadashivan M (sadashivan.manickam@wipro.com)
 *
 * Revision history :
 *
 * Version  Date        Author          Description                     ID
 * -------  ----------  --------------  ------------------------------  --------
 * 0.9      17/03/2002  Sadashivan      Initial Version                 -
 *
 * Description      : This file contains functions for display the OHCI
 *                    registers, memory, endpoint desriptor, transfer descriptor
 *                    etc. This interfaces exposed from this file can used to
 *                    debug the OHCI driver.
 *
 * FIX ME           : None.
 *
 * TO DO            : None.
 *
 ******************************************************************************/

/* includes */

#include <usb/usbOsal.h>
#include <usb/usbHst.h>
#include <usbOhci.h>
#include <usbOhciRegisterInfo.h>
#include <usbOhciTransferManagement.h>
#include <usbOhciUtil.h>
/* forward declarations */

BOOLEAN usbOhcdRegShow(UINT32 uHostControllerIndex);
VOID usbOhciDumpMemory(ULONG uAddress, UINT32 uLength, UINT32 uWidth);
VOID usbOhciDumpPipe(pUSB_OHCD_DATA pHCDData,
                     pUSB_OHCD_PIPE pHCDPipe);
VOID usbOhciDumpPeriodicPipeList(UINT8 uHostControllerIndex);
VOID usbOhciDumpGeneralTransferDescriptor(pUSB_OHCD_DATA pHCDData,
                                          PVOID pGeneralTransferDescriptor);
VOID usbOhciDumpIsoTransferDescriptor(pUSB_OHCD_DATA pHCDData,
                                      PVOID pGeneralTransferDescriptor);
VOID usbOhciDumpPendingTransfers(pUSB_OHCD_PIPE pHCDPipe);
VOID usbOhciDumpCancelRequest(pUSB_OHCD_PIPE pHCDPipe);
VOID usbOhciDumpMemTable(UINT32 uHostControllerIndex);
STATUS usbOhcdShow(UINT32 uHostControllerIndex);

#ifdef USB_OHCD_DRIVER_TEST
VOID usbOhciInitializeModuleTestingFunctions(PUSBHST_HC_DRIVER_TEST pHCDriverTestEntryPoints);
#endif


/****************** MODULE SPECIFIC FUNCTIONS DECLARATION *********************/

/* functions */
#ifdef USB_OHCD_DRIVER_TEST
LOCAL
VOID usbOhciPowerOnPorts(UINT32 uHostControllerIndex);

LOCAL
BOOLEAN usbOhciResetPort(UINT32 uHostControllerIndex, UINT8 uPortNumber);

LOCAL
BOOLEAN usbOhciWaitForConnection(UINT32 uHostControllerIndex, UINT8 uPortNumber);

LOCAL
UINT8 usbOhciGetDeviceSpeed(UINT32 uHostControllerIndex, UINT8 uPortNumber);
#endif
/************************ GLOBAL FUNCTIONS DEFINITION *************************/

#define USB_OHCD_DEBUG
#ifdef  USB_OHCD_DEBUG

/***************************************************************************
*
* usbOhcdRegShow - dump registers contents.
*
* This function is used to dump the contents of the USB OHCI Host Controller
* Registers.
*
* PARAMETERS: <uHostControllerIndex (IN)> - Specifies the OHCI Host Controller
* index.
*
* RETURNS: TRUE if the host controller index specified is valid for the USB
* OHCI controllers detected on the system, otherwise FALSE.
*
* ERRNO:
*   None.
*/

BOOLEAN usbOhcdRegShow
    (
    UINT32 uHostControllerIndex
    )

    {
    /*
     * To hold the number of OHCI Contorller Registers (excluding the
     * root hub port status registers.
     */

    const UINT32 uNumberOfRegisters = 21;

    /* To hold the name of the OHCI Controller Registers */

    UCHAR pRegisterName[][10] =
        {
        "HC_REV   ",
        "HC_CON   ",
        "HC_CMDST ",
        "HC_INTST ",
        "HC_INTEN ",
        "HC_INTDI ",
        "HC_HCCA  ",
        "HC_PCED  ",
        "HC_CHED  ",
        "HC_CCED  ",
        "HC_BHED  ",
        "HC_BCED  ",
        "HC_DONEH ",
        "HC_FMINT ",
        "HC_FMREM ",
        "HC_FMNUM ",
        "HC_PERST ",
        "HC_LSTHR ",
        "HC_RHDESA",
        "HC_RHDESB",
        "HC_RHSTA ",
        "HC_RHPS",
        };

    /* To hold the offset of the OHCI Controller Registers */

    UINT32 uRegisterOffset[] =
        {
        USB_OHCI_REVISION_REGISTER_OFFSET,
        USB_OHCI_CONTROL_REGISTER_OFFSET,
        USB_OHCI_COMMAND_STATUS_REGISTER_OFFSET,
        USB_OHCI_INTERRUPT_STATUS_REGISTER_OFFSET,
        USB_OHCI_INTERRUPT_ENABLE_REGISTER_OFFSET,
        USB_OHCI_INTERRUPT_DISABLE_REGISTER_OFFSET,
        USB_OHCI_HCCA_REGISTER_OFFSET,
        USB_OHCI_PERIOD_CURRENT_ED_REGISTER_OFFSET,
        USB_OHCI_CONTROL_HEAD_ED_REGISTER_OFFSET,
        USB_OHCI_CONTROL_CURRENT_ED_REGISTER_OFFSET,
        USB_OHCI_BULK_HEAD_ED_REGISTER_OFFSET,
        USB_OHCI_BULK_CURRENT_ED_REGISTER_OFFSET,
        USB_OHCI_DONE_HEAD_REGISTER_OFFSET,
        USB_OHCI_FM_INTERVAL_REGISTER_OFFSET,
        USB_OHCI_FM_REMAINING_REGISTER_OFFSET,
        USB_OHCI_FM_NUMBER_REGISTER_OFFSET,
        USB_OHCI_PERIODIC_START_REGISTER_OFFSET,
        USB_OHCI_LC_THRESHOLD_REGISTER_OFFSET,
        USB_OHCI_RH_DESCRIPTOR_A_REGISTER_OFFSET,
        USB_OHCI_RH_DESCRIPTOR_B_REGISTER_OFFSET,
        USB_OHCI_RH_STATUS_REGISTER_OFFSET,
        };

    /* To hold the loop index */

    UINT32  uIndex = 0;

    /* To hold the port number */

    UINT16  uPortNumber = 1;

    /* To hold the downstream ports on the root hub */

    UINT16  uNumberOfDownStreamPorts = 0;

    /* To hold the offset of the root hub port status register */

    UINT8   uRootHubPortStatusRegisterOffset = 0;

    /* To hold the value read from the registers */

    UINT32  uRegisterValue = 0;


    /* Check whether the OHCI Bus Number is valid */

    if (uHostControllerIndex >= USB_MAX_OHCI_COUNT ||
        g_OHCDData[uHostControllerIndex].pDev == NULL)
        {
        /* Invalid OHCI Bus Number */

        return FALSE;
        }

    /* Display new lines */

    printf ("\n\n");

    /* Display the contents of the OHCI Controller registers */

    for (uIndex = 0; uIndex < uNumberOfRegisters; uIndex++)
        {
        /* Display the contents of the register */

        printf ("%s : 0x%08X   \n",
                pRegisterName[uIndex],
                USB_OHCI_REG_READ (g_OHCDData[uHostControllerIndex].pDev,
                                   uRegisterOffset[uIndex]));
        }

    /* Obtain the number of downstream ports on the root hub */

    uNumberOfDownStreamPorts =
        g_OHCDData[uHostControllerIndex].uNumberOfDownStreamPorts;

    /* Display the contents of the root hub port status register */

    for (uPortNumber = 1;
         uPortNumber <= uNumberOfDownStreamPorts;
         uPortNumber++)
        {
        /* Obtain the offset of the root hub port status register */

        uRootHubPortStatusRegisterOffset =
            (UINT8)USB_OHCI_RH_PORT_STATUS_REGISTER_OFFSET(uPortNumber);

        /* Read the contents of the port status register */

        uRegisterValue =
            USB_OHCI_REG_READ(g_OHCDData[uHostControllerIndex].pDev,
                              uRootHubPortStatusRegisterOffset);

        /* Display the contents of the register */

        printf("%s%d  : 0x%08X   \n",
               pRegisterName[uNumberOfRegisters],
               uPortNumber,
               uRegisterValue);

        /* Increment the index for formatting */

        uIndex++;
        }

    /* Display new lines */

    printf("\n\n");

    return TRUE;

    } /* End of function usbOhcdRegShow () */

/***************************************************************************
*
* usbOhciDumpMemory - dump memory contents
*
* This function is used to dump the contents of the specified memory location.
*
* PARAMETERS: <uAddress (IN)> - Specifies the address of memory location.
*
* <uLength (IN)> - Specifies the length of memory to be dumped.
*
* <uWidth (IN)> - Specifies the width of each entry in bytes. For example,
* if this value is 1, the data will be displayed in bytes. If the value is 4,
* the data will be displayed in DWORDS (4 bytes).
*
* RETURNS: N/A
*
* ERRNO:
*   None.
*/

VOID usbOhciDumpMemory
    (
    ULONG uAddress,
    UINT32 uLength,
    UINT32 uWidth
    )

    {
    /* To hold the loop index */

    UINT32  uIndex = 0;

    /* To hold the pointer to the memory location */

    PUCHAR  pBuffer = (PUCHAR) uAddress;

    /* Display the contents of the memory location */

    for (uIndex = 0; uIndex < uLength / uWidth; uIndex++)
        {
        /* Display the memory address */

        if ((uIndex % (8 / uWidth)) == 0)
            {
            printf("\n0x%lX: ", uAddress + uIndex * uWidth);
            }

        /* Display the contents of the memory */

        switch (uWidth)
            {
            case 1: /* Display in bytes */
                printf("0x%02X ", ((PUCHAR) pBuffer)[uIndex]);
                break;
            case 4:
                printf("0x%08X ", ((PUINT32) pBuffer)[uIndex]);
                break;
            default:
                printf("0x%X ", ((PUCHAR) pBuffer)[uIndex]);
                break;
            };

        } /* End of for (uIndex = 0; ...) */

    return;

    } /* End of function usbOhciDumpMemory () */

/***************************************************************************
*
* usbOhciDumpPipe - dump endpoint descriptor contents
*
* This function is used to dump the contents of the endpoint descriptor.
*
* PARAMETERS: <pEndpointDescriptor (IN)> - Pointer to the endpoint descriptor to
* be dumped.
*
* RETURNS: N/A
*
* ERRNO:
*   None.
*/

VOID usbOhciDumpPipe
    (
    pUSB_OHCD_DATA pHCDData,
    pUSB_OHCD_PIPE pHCDPipe
    )

    {
    /* To hold the pointer to the OHCI general transfer descriptor */

    PUSB_OHCI_TD pOhciGeneralTransferDescriptor = NULL;

    UINT32       uBufferAddr = 0x0;

    /* Check whether the parameter is valid */

    if (pHCDPipe == NULL)
        {
        return;
        }

    /* Initialize the pointer to the OHCI endpoint descriptor */

    printf("Start Dump ED %p\n\n", pHCDPipe);

    /* Display the control information */

    printf("uControlInformation          : 0x%08X\n",
        USB_OHCD_SWAP_DATA(pHCDData->uBusIndex, pHCDPipe->uControlInformation));

    /* Display the pointer to the tail of the transfer descriptor list */

    printf("uTDQueueTailPointer          : 0x%08X\n",
        USB_OHCD_SWAP_DATA(pHCDData->uBusIndex, pHCDPipe->uTDQueueTailPointer));

    /* Display the pointer to the head of the transfer descriptor list */

    printf("uTDQueueHeadPointer          : 0x%08X\n",
        USB_OHCD_SWAP_DATA(pHCDData->uBusIndex, pHCDPipe->uTDQueueHeadPointer));

    /* Display the pointer to the next endpoint descriptor list */

    printf("uNextEDPointer               : 0x%08X\n",
        USB_OHCD_SWAP_DATA(pHCDData->uBusIndex, pHCDPipe->uNextEDPointer));

    /* Initialize the pointer to the transfer descriptor */

    /* Convert into CPU endian */

    uBufferAddr = USB_OHCD_SWAP_DATA(pHCDData->uBusIndex,
                              pHCDPipe->uTDQueueHeadPointer);

    /* Get the actual bus address in CPU endian */

    uBufferAddr &= (~USB_OHCI_TD_ALIGN_MASK);

    pOhciGeneralTransferDescriptor = (PUSB_OHCI_TD)
        usbOhciPhyToVirt(pHCDData, uBufferAddr, USB_OHCI_MEM_MAP_TD);

    /*
     * NOTE: The TDQueueHead field in the OHCI Endpoint descriptor also
     *       contains the 'ToggleCarry (bit 1)' and 'Halt (bit 0)' bit.
     *
     *       Hence, while using the TDQueueHead pointer, mask the least
     *       significant 4 bits.
     */

    /* Display the transfer descriptors for the endpoint */

    if (pHCDPipe->uEndpointType != USBHST_ISOCHRONOUS_TRANSFER)
        {
        while (pOhciGeneralTransferDescriptor != NULL)
            {
            /* Call the function to display the transfer descriptor */

            usbOhciDumpGeneralTransferDescriptor (pHCDData,
                (PVOID) pOhciGeneralTransferDescriptor);

            /* Obtain the pointer to the actual transfer descriptor */

            uBufferAddr = pOhciGeneralTransferDescriptor->uNextTDPointer;

            /* Get the bus address in CPU endian */

            uBufferAddr = USB_OHCD_SWAP_DATA(pHCDData->uBusIndex,uBufferAddr);

            pOhciGeneralTransferDescriptor = (PUSB_OHCI_TD)
                usbOhciPhyToVirt(pHCDData, uBufferAddr ,USB_OHCI_MEM_MAP_TD);
            }
        }
    else
        {
        while (pOhciGeneralTransferDescriptor != NULL)
            {
            /* Call the function to display the transfer descriptor */

            usbOhciDumpIsoTransferDescriptor (pHCDData,
                (PVOID) pOhciGeneralTransferDescriptor);

            /* Obtain the pointer to the actual transfer descriptor */

            uBufferAddr = pOhciGeneralTransferDescriptor->uNextTDPointer;

            /* Get the bus address in CPU endian */

            uBufferAddr = USB_OHCD_SWAP_DATA(pHCDData->uBusIndex,uBufferAddr);

            pOhciGeneralTransferDescriptor = (PUSB_OHCI_TD)
                usbOhciPhyToVirt(pHCDData, uBufferAddr ,USB_OHCI_MEM_MAP_TD);
            }

        }
    printf("\n***** End of ED Dump *****\n");
    return;
    } /* End of function usbOhciDumpPipe () */

/***************************************************************************
*
* usbOhciDumpPeriodicPipeList - dump periodic pipe list
*
* This function is used to dump the contents of the periodic pipe list.
*
* PARAMETERS: <uHostControllerIndex (IN)> - Specifies the host controller index
*
* RETURNS: N/A
*
* ERRNO:
*   None.
*/

VOID usbOhciDumpPeriodicPipeList
    (
    UINT8  uHostControllerIndex
    )

    {
    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA          pHCDData = NULL;

    /* To hold the pointer to the base address of the periodic list */

    PUINT32                 pInterruptTable = NULL;

    UINT32                  uBufferAddr = 0x0;


    /* To hold the loop index */

    UINT32 uIndex = 0;

    /* Check whether the OHCI Bus Number is valid */


    if (uHostControllerIndex >= USB_MAX_OHCI_COUNT ||
        g_OHCDData[uHostControllerIndex].pDev == NULL)
        {
        /* Invalid OHCI Bus Number */

        return;
        }

    /* Obtain the pointer to the OHCI host controller information */

    pHCDData = &g_OHCDData[uHostControllerIndex];

    /*
     * The following sentence want to get the address of
     * pHCDData->pHcca->uHccaInterruptTable
     * But in DIAB compiler, we will get warning as
     * "dangerous to take address of member of packed or swapped structure"
     * The "uHccaInterruptTable" must be the first of the structure
     * So we will get the HCCA address to instead.
     */

    pInterruptTable = (PUINT32)(pHCDData->pHcca);


    /* Display the periodic endpoint list */

    for (uIndex = 0; uIndex < USB_OHCI_MAXIMUM_POLLING_INTERVAL; uIndex++)
        {
        /* To hold the pointer to the current endpoint */

        pUSB_OHCD_PIPE pHCDPipe = NULL;

        /* To hold the number of endpoints to be displayed per line */

        UINT8   uCount = 0;

        uBufferAddr = pInterruptTable[uIndex];

        /* Get the bus address in CPU endian */

        uBufferAddr = USB_OHCD_SWAP_DATA(uHostControllerIndex, uBufferAddr);

        pHCDPipe = (pUSB_OHCD_PIPE)
            usbOhciPhyToVirt(pHCDData,
                             uBufferAddr,
                             USB_OHCI_MEM_MAP_ED);

        /* Display the bandwidth reserved */

        if (pHCDPipe != NULL)
            {
            printf("\n%02d [%04d]: ", uIndex,
                   pHCDPipe->uBandwidthAvailable);
            }
        else
            {
            printf("\n%02d [0000]: \n", uIndex);
            }

        /* Display the tree of endpoints */

        while (uBufferAddr != 0)
            {
            /* Format the display */

            if (((uCount % 3) == 0) && (uCount != 0))
                {
                printf("\n");
                }

            pHCDPipe = (pUSB_OHCD_PIPE)
                usbOhciPhyToVirt(pHCDData,
                                 uBufferAddr,
                                 USB_OHCI_MEM_MAP_ED);

            if (pHCDPipe == NULL)
                {
                USB_OHCD_ERR("Invalid virtual address \n", 1, 2, 3, 4, 5, 6);
                return;
                }

            /*
             * Display the address and polling interval for the
             * endpoint
             */

            printf("%p (%04d), ", pHCDPipe,
                   pHCDPipe->uPollingInterval);

            /* Traverse to the next endpoint in the list */

            uBufferAddr = pHCDPipe->uNextEDPointer;

            /* Get the bus address in CPU endian */

            uBufferAddr = USB_OHCD_SWAP_DATA(uHostControllerIndex, uBufferAddr);

            /* Increment the count */

            uCount++;
            }

        /* Format the display */
        if ((uCount % 3) != 0)
            {
            printf("\n");
            }
        }

    return;

    } /* End of function usbOhciDumpPeriodicPipeList () */

/***************************************************************************
*
* usbOhciDumpGeneralTransferDescriptor - dump general transfer descriptor
*
* This function is used to dump the contents of the general transfer
* descriptor.
*
* PARAMETERS: <pGeneralTransferDescriptor (IN)> - Pointer to the general
* descriptor
*
* RETURNS: N/A
*
* ERRNO:
*   None.
*
*/

VOID usbOhciDumpGeneralTransferDescriptor
    (
    pUSB_OHCD_DATA pHCDData,
    PVOID          pGeneralTransferDescriptor
    )
    {
    /* To hold the pointer to the temporary OHCI general transfer descriptor */

    PUSB_OHCI_TD pTempTD = NULL;

    UINT32       uIndex = 0;

    /* Check whether the parameter is valid */

    if (pGeneralTransferDescriptor == NULL)
        {
        return;
        }

    /* Initialize the pointer to the OHCI general transfer descriptor */

    pTempTD = (PUSB_OHCI_TD) pGeneralTransferDescriptor;

    printf("\nStart Dump TD %p\n", pTempTD);

    /* Display the endpoint descriptor */

    printf("pHCDPipe                     : %p\n", pTempTD->pHCDPipe);

    /* Display the control information */

    printf("uControlInformation          : 0x%08X\n",
        USB_OHCD_SWAP_DATA(pHCDData->uBusIndex, pTempTD->uControlInformation));

    /* Display the current buffer pointer for this transfer descriptor */

    printf("uCurrentBufferPointer        : 0x%08X\n",
        USB_OHCD_SWAP_DATA(pHCDData->uBusIndex, pTempTD->uCurrentBufferPointer));

    /* Display the pointer to the next transfer descriptor */

    printf("uNextTDPointer               : 0x%08X\n",
        USB_OHCD_SWAP_DATA(pHCDData->uBusIndex, pTempTD->uNextTDPointer));

    /* Display the end of buffer pointer for this transfer descriptor */

    printf("uBufferEndPointer            : 0x%08X\n",
        USB_OHCD_SWAP_DATA(pHCDData->uBusIndex, pTempTD->uBufferEndPointer));

    /* Display the offset and status for this transfer descriptor */

    for (uIndex = 0; uIndex < 4; uIndex++)
        {
        printf("uOffsetAndStatus[%d]          : 0x%08X\n", uIndex,
            USB_OHCD_SWAP_DATA(pHCDData->uBusIndex, pTempTD->uOffsetAndStatus[uIndex]));
        }

    printf("@@@@@ End of TD Dump @@@@@\n");
    return;

    } /* End of function usbOhciDumpGeneralTransferDescriptor () */

/***************************************************************************
*
* usbOhciDumpIsoTransferDescriptor - dump isochronous transfer descriptor
*
* This function is used to dump the contents of the isochronous transfer
* descriptor.
*
* PARAMETERS: <pGeneralTransferDescriptor (IN)> - Pointer to the general
* descriptor
*
* RETURNS: N/A
*
* ERRNO:
*   None.
*
*/

VOID usbOhciDumpIsoTransferDescriptor
    (
    pUSB_OHCD_DATA pHCDData,
    PVOID          pGeneralTransferDescriptor
    )

    {
    /* To hold the pointer to the temporary OHCI general transfer descriptor */

    PUSB_OHCI_TD pTempTD = NULL;
    UINT32       uIndex = 0;

    /* Check whether the parameter is valid */

    if (pGeneralTransferDescriptor == NULL)
        {
        return;
        }

    /* Initialize the pointer to the OHCI general transfer descriptor */

    pTempTD = (PUSB_OHCI_TD) pGeneralTransferDescriptor;

    printf("\nStart Dump TD %p\n", pTempTD);

    /* Display the endpoint descriptor */

    printf("pHCDPipe                     : %p\n", pTempTD->pHCDPipe);

    /* Display the control information */

    printf("uControlInformation          : 0x%08X\n",
        USB_OHCD_SWAP_DATA(pHCDData->uBusIndex, pTempTD->uControlInformation));

    /* Display the current buffer pointer for this transfer descriptor */

    printf("uCurrentBufferPointer        : 0x%08X\n",
        USB_OHCD_SWAP_DATA(pHCDData->uBusIndex, pTempTD->uCurrentBufferPointer));

    /* Display the pointer to the next transfer descriptor */

    printf("uNextTDPointer               : 0x%08X\n",
        USB_OHCD_SWAP_DATA(pHCDData->uBusIndex, pTempTD->uNextTDPointer));

    /* Display the end of buffer pointer for this transfer descriptor */

    printf("uBufferEndPointer            : 0x%08X\n",
        USB_OHCD_SWAP_DATA(pHCDData->uBusIndex, pTempTD->uBufferEndPointer));

    /* Display the offset and status [0] for this transfer descriptor */

    /* Display the offset and status for this transfer descriptor */

    for (uIndex = 0; uIndex < 4; uIndex++)
        {
        printf("uOffsetAndStatus[%d]          : 0x%08X\n", uIndex,
            USB_OHCD_SWAP_DATA(pHCDData->uBusIndex, pTempTD->uOffsetAndStatus[uIndex]));
        }

    printf("@@@@@ End of TD Dump @@@@@\n");

    return;
    } /* End of function usbOhciDumpGeneralTransferDescriptor () */

/***************************************************************************
*
* usbOhciDumpPendingTransfers - dump pending transfers
*
* This function is used to dump the pending transfers for the endpoint
*
* PARAMETERS: <pEndpointDescriptor (IN)> - Pointer to the endpoint descriptor
* to be dumped
*
* RETURNS: N/A
*
* ERRNO:
*   None.
*/

VOID usbOhciDumpPendingTransfers
    (
    pUSB_OHCD_PIPE pHCDPipe
    )

    {
    /* To hold the pointer to the current URB list element */

    pUSB_OHCD_REQUEST_INFO  pCurRequest = NULL;

    NODE *pNode = NULL;

    /* Check whether the parameter is valid */

    if (pHCDPipe == NULL)
        {
        return;
        }

    /* Get the first element of the request list */

    pNode = lstFirst(&(pHCDPipe->requestList));

    while (pNode != NULL)
        {
        pCurRequest = LIST_NODE_TO_USB_OHCD_REQUEST_INFO(pNode);
        if (pCurRequest != NULL)
            {
            /* Display the pointer to the URB */

            printf("[%p]->pUrb                         : %p\n",
                   pCurRequest, (pCurRequest->pUrb));

            /*
             * Display the pointer to the first transfer descriptor corresponding
             * to the URB.
             */

            printf("[%p->pTDListHead                  : %p\n",
                pCurRequest, (pCurRequest->pTDListHead));

            /*
             * Display the pointer to the first transfer descriptor corresponding
             * to the URB.
             */

            printf("[%p]->pTDListTail                  : %p\n",
                pCurRequest, (pCurRequest->pTDListTail));
            }

        pNode = lstNext(pNode);
        }

    return;
    } /* End of function usbOhciDumpPendingTransfers () */

/***************************************************************************
*
* usbOhciDumpCancelRequest - dump cancel requests
*
* This function is used to dump the cancel requests for the endpoint
*
* PARAMETERS: <pHCDPipe (IN)> - Pointer to the endpoint descriptor
* to be dumped
*
* RETURNS: N/A
*
* ERRNO:
*   None.
*/

VOID usbOhciDumpCancelRequest
    (
    pUSB_OHCD_PIPE pHCDPipe
    )

    {
    /* To hold the pointer to the current URB list element */

    pUSB_OHCD_REQUEST_INFO  pCurRequest = NULL;

    NODE *pNode = NULL;

    /* Check whether the parameter is valid */

    if (pHCDPipe == NULL)
        {
        return;
        }

    /* Get the first element of the request list */

    pNode = lstFirst(&(pHCDPipe->cancelRequestList));

    while (pNode != NULL)
        {
        pCurRequest = CANCEL_NODE_TO_USB_OHCD_REQUEST_INFO(pNode);

        if (pCurRequest != NULL)
            {
            /* Display the pointer to the URB */

            printf("[%p]->pUrb                         : %p\n",
                   pCurRequest,(pCurRequest->pUrb));

            /*
             * Display the pointer to the first transfer descriptor corresponding
             * to the URB.
             */

            printf("[%p]->pTDListHead                  : %p\n",
                   pCurRequest, (pCurRequest->pTDListHead));

            /*
             * Display the pointer to the first transfer descriptor corresponding
             * to the URB.
             */

            printf("[%p]->pTDListTail                  : %p\n",
                   pCurRequest, (pCurRequest->pTDListTail));
            }

        pNode = lstNext(pNode);
        }

    return;
    } /* End of function usbOhciDumpPendingTransfers () */

/*******************************************************************************
*
* usbOhcdShow - show the OHCI controller registers and data structures
*
* This routine shows the OHCI controller registers and data structures.
*
* <uHostControllerIndex> - the OHCI host controller index
*
* RETURNS: OK, or ERROR if error happens.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbOhcdShow
    (
    UINT32         uHostControllerIndex
    )
    {
    pUSB_OHCD_DATA                pHCDData = NULL;
    pUSB_OHCD_PIPE                pHCDPipe = NULL;
    PUINT32                       pInterruptTable = NULL;
    UINT32                        uBufferAddr = 0x0;
    UINT8                         uIndex = 0x0;
    UINT32                        uPeriodicCount = 0x0;


    if (uHostControllerIndex >= USB_MAX_OHCI_COUNT ||
        g_OHCDData[uHostControllerIndex].pDev == NULL)
        {
        printf("Index %d is invalid\n", uHostControllerIndex);

        return ERROR;
        }

    pHCDData = &g_OHCDData[uHostControllerIndex];

    if (pHCDData->pDev == NULL)
        {
        printf("usbOhcdShow - pHCDData is NULL\n");

        return ERROR;
        }

    /* Show all registers */

    printf("Dump register:\n");

    usbOhcdRegShow(uHostControllerIndex);

    /* Show control endpoint descriptor */


    OS_WAIT_FOR_EVENT(pHCDData->pipeListAccessEvent, WAIT_FOREVER);

    uBufferAddr = USB_OHCI_REG_READ (pHCDData->pDev,
                                     USB_OHCI_CONTROL_HEAD_ED_REGISTER_OFFSET);

    pHCDPipe = (pUSB_OHCD_PIPE)
          usbOhciPhyToVirt(pHCDData, uBufferAddr, USB_OHCI_MEM_MAP_ED);

    if (pHCDPipe != NULL)
        {
        printf("Control endpoint descriptor list:\n\n");
        }
    else
        {
        printf("Control endpoint descriptor list is NULL\n\n");
        }

    while (pHCDPipe != NULL)
        {
        usbOhciDumpPipe(pHCDData, pHCDPipe);
        printf("\n");
        pHCDPipe = pHCDPipe->pHCDNextPipe;
        }

    /* Show bulk endpoint descriptor */

    uBufferAddr = USB_OHCI_REG_READ (pHCDData->pDev,
                   USB_OHCI_BULK_HEAD_ED_REGISTER_OFFSET);

    pHCDPipe = (pUSB_OHCD_PIPE)
          usbOhciPhyToVirt(pHCDData, uBufferAddr, USB_OHCI_MEM_MAP_ED);

    if (pHCDPipe != NULL)
        {
        printf("\nBulk endpoint descriptor list:\n\n");
        }
    else
        {
        printf("\nBulk endpoint descriptor list is NULL\n\n");
        }

    while (pHCDPipe != NULL)
        {
        usbOhciDumpPipe(pHCDData, pHCDPipe);
        printf("\n");
        pHCDPipe = pHCDPipe->pHCDNextPipe;
        }

    /*
     * The following sentence want to get the address of
     * pHCDData->pHcca->uHccaInterruptTable
     * But in DIAB compiler, we will get warning as
     * "dangerous to take address of member of packed or swapped structure"
     * The "uHccaInterruptTable" must be the first of the structure
     * So we will get the HCCA address to instead.
     */

    pInterruptTable = (PUINT32)(pHCDData->pHcca);


    /*
     * Search for the endpoint descriptor in the list of periodic pipes
     * for the specified polling interval
     */

    for (uIndex = 0; uIndex < 32; uIndex++)
        {
        /*
         * Obtain the pointer to the head of the periodic list for
         * the specified polling interval.
         */

        uBufferAddr = pInterruptTable[uIndex];

        /* Get the bus address in CPU endian */

        uBufferAddr = USB_OHCD_SWAP_DATA(uHostControllerIndex, uBufferAddr);

        pHCDPipe = (pUSB_OHCD_PIPE)
            usbOhciPhyToVirt(pHCDData,
                             uBufferAddr,
                             USB_OHCI_MEM_MAP_ED);

        while (pHCDPipe != NULL)
            {
            uPeriodicCount++;

            usbOhciDumpPipe(pHCDData, pHCDPipe);

            printf("\n");

            pHCDPipe = pHCDPipe->pHCDNextPipe;
            }

        }

    printf("\nPeriodic endpoint descriptor list is %d\n\n",
           uPeriodicCount);

    OS_RELEASE_EVENT(pHCDData->pipeListAccessEvent);
    return OK;
    }


/*******************************************************************************
*
* usbOhciDumpMemTable - dump memMap Table
*
* This routine dumps memMap Table list.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbOhciDumpMemTable
    (
    UINT32         uHostControllerIndex
    )
    {
    pUSB_OHCD_DATA                pHCDData = NULL;
    NODE                          *pNode = NULL;
    pUSB_OHCI_MEM_MAP_TABLE       pMemMap = NULL;
    pUSB_OHCD_PIPE                pHCDPipe = NULL;
    PUSB_OHCI_TD                  pTD = NULL;

    if (uHostControllerIndex >= USB_MAX_OHCI_COUNT ||
        g_OHCDData[uHostControllerIndex].pDev == NULL)
        {
        printf("Index %d is invalid\n", uHostControllerIndex);

        return;
        }

    pHCDData = &g_OHCDData[uHostControllerIndex];

    OS_WAIT_FOR_EVENT(pHCDData->memMapTableEvent, WAIT_FOREVER);

    printf("ED memTab count %d, TD memTab count %d\n",
           lstCount(&(pHCDData->edMemMapTableList)),
           lstCount(&(pHCDData->tdMemMapTableList)));

    printf("Start Dump TD memTab list\n");

    pNode = lstFirst((LIST *)(&(pHCDData->tdMemMapTableList)));

    while (pNode != NULL)
        {
        pMemMap = MEM_NODE_TO_USB_OHCI_MEM_MAP_TABLE(pNode);

        if (pMemMap != NULL)
            {
            printf("pMemMap %p virAddr %p busAddr %lx\n",
                    pMemMap, pMemMap->pVirtAddr,
                   (ULONG)pMemMap->uBusAddr);
            }
        pNode = lstNext(pNode);
        }

    printf("Start Dump ED memTab list\n");

    pNode = lstFirst((LIST *)(&(pHCDData->edMemMapTableList)));

    while (pNode != NULL)
        {
        pMemMap = MEM_NODE_TO_USB_OHCI_MEM_MAP_TABLE(pNode);

        if (pMemMap != NULL)
            {
            printf("pMemMap %p virAddr %p busAddr %lx\n",
                   pMemMap, pMemMap->pVirtAddr,
                  (ULONG)pMemMap->uBusAddr);
            }

        pNode = lstNext(pNode);
        }

    OS_RELEASE_EVENT(pHCDData->memMapTableEvent);

    /* dump Default Pipe's free TD list */

    pHCDPipe = pHCDData->pDefaultPipe;

    pTD =  pHCDPipe->pFreeTDHead;
    while (pTD != NULL)
        {
        printf("pDefault pipe %p free TD %p pTD->pMemMap %p\n",
               pHCDPipe, pTD, pTD->pMemMap);

        pTD = pTD->pHCDNextTransferDescriptor;
        }

    return;
    }

#endif
#ifdef USB_OHCD_DRIVER_TEST
/***************************************************************************
* usbOhciInitializeModuleTestingFunctions - obtaines entry points
*
* Function to obtain the entry points used for HCD module testing.
*
* RETURNS: N/A
*
* ERRNO: none
*/

VOID usbOhciInitializeModuleTestingFunctions
    (
    PUSBHST_HC_DRIVER_TEST pHCDriverTestEntryPoints /* Ptr to HCD module entry points */
    )
    {
    /* Check whether the parameter is valid */

    if (NULL == pHCDriverTestEntryPoints)
        {
        return;
        }

    /* Clear the OHCI host controller driver entry functions information */

    OS_MEMSET(pHCDriverTestEntryPoints, 0, sizeof(USBHST_HC_DRIVER_TEST));

    /*
     * Populate the function pointer to power on the ports on the host
     * controller
     */

    pHCDriverTestEntryPoints->HCD_PowerOnPorts =
        usbOhciPowerOnPorts;

    /* Function to reset the port on the host controller */

    pHCDriverTestEntryPoints->HCD_ResetPort =
        usbOhciResetPort;

    /* Function to wait for a device connection on the host controller */

    pHCDriverTestEntryPoints->HCD_WaitForConnection =
        usbOhciWaitForConnection;

    /*
     * Function to check the speed of the device connected to the host
     * controller
     */
    pHCDriverTestEntryPoints->HCD_GetDeviceSpeed =
        usbOhciGetDeviceSpeed;

    return;

    } /* End of function usbOhciInitializeModuleTestingFunctions() */

/***************************************************************************
*
* usbOhciPowerOnPorts - power on the downstream ports
*
* This function is used to power on the downstream ports of the USB OHCI
* host controller.
*
* PARAMETERS: <uHostControllerIndex (IN)> - Specifies the host controller index
*
* RETURNS: N/A
*
* ERRNO:
*   None
*
*\NOMANUAL
*/

LOCAL VOID usbOhciPowerOnPorts
    (
    UINT32   uHostControllerIndex
    )

    {

    /* To hold the value read from the registers */

    UINT32  uRegisterValue = 0;

    /* To hold the loop index */

    UINT32  uIndex = 0;

    /* To hold the offset of the port status register */

    UINT32  uPortStatusRegisterOffset = 0;

    /* To hold the number of downstream ports */

    UINT32  uNumberOfDownstreamPorts = 0;

    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA   pHCDData = NULL;

    /* Obtain the pointer to the host controller information */

    pHCDData = &g_OHCDData[uHostControllerIndex];

    /* Obtain the number of ports supported by the host controller (BEGIN) */

    /* Read the contents of the root hub descriptor A register */

    uNumberOfDownstreamPorts = USB_OHCI_REG_READ(pHCDData->pDev,
        USB_OHCI_RH_DESCRIPTOR_A_REGISTER_OFFSET);

    /* Mask the other fields of the root hub description register */
    uNumberOfDownstreamPorts = uNumberOfDownstreamPorts & 0x000000FF;

    /* Obtain the number of ports supported by the host controller (END) */

    /* Enable all the downstream ports with device attached then power up these ports */
    for (uIndex = 1; uIndex <= uNumberOfDownstreamPorts; uIndex++)
        {
        uPortStatusRegisterOffset =
        USB_OHCI_RH_PORT_STATUS_REGISTER_OFFSET(uIndex);

        /* Read the contents of the port status register */

        uRegisterValue = USB_OHCI_REG_READ(pHCDData->pDev,
                               uPortStatusRegisterOffset);

        /* Check if there is device attached on this port */

        if ((uRegisterValue & USB_OHCI_RH_PORT_STATUS_CCS) != 0)
            {

            /* NOTE:  Only when the port is attached with device can we enable the port */

            /* Set Port Enable */
            USB_OHCI_REG_WRITE(pHCDData->pDev,
                    uPortStatusRegisterOffset,
                    USB_OHCI_RH_PORT_STATUS_PES);
            }
         }

    /*
     * NOTE: If the root hub supports ganged power on the down stream ports,
     *       power on all the downstream ports. Else power on the specified
     *       port.
     */

    /* Read the contents of the root hub descriptor A register */

    uRegisterValue =
        USB_OHCI_REG_READ(pHCDData->pDev,
                          USB_OHCI_RH_DESCRIPTOR_A_REGISTER_OFFSET);

    /*
     * Check whether the root hub supports  power switching
     */

    if ((uRegisterValue & USB_OHCI_RH_DESCRIPTOR_A_REGISTER__NPS_MASK) != 0)
        {

        /*
         *  If the root hub always power on ports when HC is powered on, we can do nothing
         *  to power on the ports.
         */

        /* Return from the function */
        return;
        }

    /*
     * Check whether the root hub supports ganged power on the down
     * stream ports (global power mode).
     */

    if ((uRegisterValue & USB_OHCI_RH_DESCRIPTOR_A_REGISTER__PSM_MASK) == 0)
        {
        /*
         * NOTE: Write 1 to the Local Power Status Change (LPSC) bit
         *       to set the port power state.
         */

        /*
         * Root hub supports ganged power. Power on all the down
         * stream ports
         */
        USB_OHCI_REG_WRITE(pHCDData->pDev,
                           USB_OHCI_RH_STATUS_REGISTER_OFFSET,
                           USB_OHCI_RH_STATUS_LPSC);

        /* Return from the function */
        return;
        }

    /* Obtain the number of ports supported by the host controller (BEGIN) */

    /* uNumberOfDownstreamPorts is already obtaind above */

    /* Obtain the number of ports supported by the host controller (END) */

    /* Power on all the downstream ports */

    for (uIndex = 1; uIndex <= uNumberOfDownstreamPorts; uIndex++)
        {
        /* Obtain the base address of the port status register */
        uPortStatusRegisterOffset =
            USB_OHCI_RH_PORT_STATUS_REGISTER_OFFSET(uIndex);

        /*
        * NOTE: For HcRhPortStatus registers, read and write to them have
        * different meanings;
        * Also when writing '0' to these bits has no effects; thus we can
        * not use a read-modify-write procedue to set specifi functional
        * bits. So we just need to set the desired bits in order to
        * perform some actions.
        */

        /* Set the port power */
        USB_OHCI_REG_WRITE(pHCDData->pDev,
                           uPortStatusRegisterOffset,
                           USB_OHCI_RH_PORT_STATUS_PPS);
        }

    return;

    } /* End of function usbOhciPowerOnPorts () */

/***************************************************************************
* usbOhciResetPort - function to reset the port of the OHCI host controller
*
* This function is used to reset the port of the OHCI host controller.
*
* Resets the OHCI host controller ports
*
* RETURNS: FALSE, TRUE if the reset operation was successful.
*
* ERRNO:  N/A
*   None
*
* \NOMANUAL
*/

LOCAL BOOLEAN usbOhciResetPort
    (
    UINT32   uHostControllerIndex,
    UINT8    uPortNumber
    )

    {
    /* To hold the offset of the port status register */

    UINT32  uPortStatusRegisterOffset = 0;

    /* To hold the value read from the registers */

    UINT32  uRegisterValue = 0;

    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA   pHCDData = NULL;


    /* Check whether the OHCI host controller index is valid */

    if (uHostControllerIndex >= USB_MAX_OHCI_COUNT ||
        g_OHCDData[uHostControllerIndex].pDev == NULL)
        {
        return FALSE;
        }

    /* Obtain the pointer to the host controller information */

    pHCDData = &g_OHCDData[uHostControllerIndex];


    /* Obtain the base address of the port status register */

    uPortStatusRegisterOffset = USB_OHCI_RH_PORT_STATUS_REGISTER_OFFSET(uPortNumber);

    /* Read the contents of the port status register */

    uRegisterValue = USB_OHCI_REG_READ(pHCDData->pDev,
                                       uPortStatusRegisterOffset);


    /* Issue a port reset */

    USB_OHCI_REG_WRITE(pHCDData->pDev,
                       uPortStatusRegisterOffset,
                       uRegisterValue | 0x00000010);

    /*
     * Wait for 100 milli seconds for the reset to complete.
     *
     * NOTE: As per the specification, the reset completion time is
     *       10 ms to 20 ms. However, additional delay is provided to
     *       handle some devices which take extra time.
     */

    OS_DELAY_MS(100);

    /* Wait for reset completion */

    do
        {
        /* Read the contents of the port status register */

        uRegisterValue = USB_OHCI_REG_READ(pHCDData->pDev,
                                           uPortStatusRegisterOffset);
        }
    while (0x00100000 != (uRegisterValue & 0x00100000));

    /* Clear the reset status change */

    USB_OHCI_REG_WRITE(pHCDData->pDev,
                       uPortStatusRegisterOffset,
                       0x00100000);

    /* Read the contents of the port status register */

    uRegisterValue = USB_OHCI_REG_READ(pHCDData->pDev,
                                       uPortStatusRegisterOffset);



    /* Check whether the device is enabled */

    if (0x00000003 == (uRegisterValue & 0x00000003))
        {
        return TRUE;
        }

    /* Failed to reset the port */

    return FALSE;

    } /* End of function usbOhciResetPort() */

/***************************************************************************
*
* usbOhciWaitForConnection - wait for a device connection on a port
*
* This function is used to wait for a device connection on a port. This
* function also issues a port reset and enables the port on which the device
* is connected.
*
* PARAMETERS: <uHostControllerIndex (IN)> - Specifies the host controller on
* which the device should be connected.
*
* <uPortNumber (IN)> - Specifies the port on which the device will be connected.
*
* RETURNS: TRUE if the device was connected, otherwise FALSE.
*
* ERRNO:
*   None
*
*\NOMANUAL
*/

LOCAL BOOLEAN usbOhciWaitForConnection
	(
	UINT32   uHostControllerIndex,
	UINT8    uPortNumber
	)

	{
    /* To hold the offset of the port status register */

    UINT32  uPortStatusRegisterOffset = 0;

    /* To hold the value read from the registers */

    UINT32  uRegisterValue = 0;

    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA   pHCDData = NULL;


    /* Check whether the OHCI host controller index is valid */

    if(uHostControllerIndex >= USB_MAX_OHCI_COUNT ||
       g_OHCDData[uHostControllerIndex].pDev == NULL)
        {
        return FALSE;
        }

    /* Obtain the pointer to the host controller information */

    pHCDData = &g_OHCDData[uHostControllerIndex];


    /* Obtain the base address of the port status register */

    uPortStatusRegisterOffset =
        USB_OHCI_RH_PORT_STATUS_REGISTER_OFFSET(uPortNumber);

    /* Wait for connect status change */

    do
        {
        /* Read the contents of the port status register */

        uRegisterValue =
            USB_OHCI_REG_READ(pHCDData->pDev,
                                          uPortStatusRegisterOffset);

        }
    while ((uRegisterValue & 0x00010000) != 0x00010000);

    /* Clear the connection status change */

    USB_OHCI_REG_WRITE(pHCDData->pDev,
                       uPortStatusRegisterOffset,
                       0x00010000);

    /* Read the contents of the port status register */

    uRegisterValue =
        USB_OHCI_REG_READ(pHCDData->pDev,
                          uPortStatusRegisterOffset);


    /* Issue a port reset */

    USB_OHCI_REG_WRITE(pHCDData->pDev,
                       uPortStatusRegisterOffset,
                       uRegisterValue | 0x00000010);

    /* Wait for 100 milli seconds for the reset to complete */

    OS_DELAY_MS(100);

    /* Wait for reset completion */

    do
        {
        /* Read the contents of the port status register */

        uRegisterValue =
            USB_OHCI_REG_READ(pHCDData->pDev,
                                          uPortStatusRegisterOffset);

        }
    while ((uRegisterValue & 0x00100000) != 0x00100000);

    /* Clear the reset status change */

    USB_OHCI_REG_WRITE(pHCDData->pDev,
                       uPortStatusRegisterOffset,
                       0x00100000);

    /* Read the contents of the port status register */

    uRegisterValue =
        USB_OHCI_REG_READ(pHCDData->pDev,
                                  uPortStatusRegisterOffset);


    /* Check whether the device is enabled */

    if ((uRegisterValue & 0x00000003) == 0x00000003)
        {
        return TRUE;
        }

    return FALSE;

    } /* End of function usbOhciWaitForConnection () */

/***************************************************************************
* usbOhciGetDeviceSpeed - obtain the speed of the device
*
* Function to obtain the speed of the device connected to the root hub.
*
* RETURNS: USBHST_LOW_SPEED,USBHST_FULL_SPEED if a full speed device is
*          connected.
*
* ERRNO:
*   None
*
* \NOMANUAL
*/
LOCAL UINT8 usbOhciGetDeviceSpeed
    (
    UINT32   uHostControllerIndex,
    UINT8    uPortNumber
    )

    {
    /* To hold the offset of the port status register */

    UINT32  uPortStatusRegisterOffset = 0;

    /* To hold the value read from the registers */

    UINT32  uRegisterValue = 0;

    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA   pHCDData = NULL;

    /* Check whether the OHCI host controller index is valid */

    if (uHostControllerIndex >= USB_MAX_OHCI_COUNT ||
        g_OHCDData[uHostControllerIndex].pDev == NULL)
        {
        return USBHST_LOW_SPEED;
        }

    /* Obtain the pointer to the host controller information */

    pHCDData = &g_OHCDData[uHostControllerIndex];

    /* Obtain the base address of the port status register */

    uPortStatusRegisterOffset = USB_OHCI_RH_PORT_STATUS_REGISTER_OFFSET(uPortNumber);

    /* Read the contents of the port status register */

    uRegisterValue = USB_OHCI_REG_READ(pHCDData->pDev,
                                       uPortStatusRegisterOffset);


    /* Check whether the device connnected is a low speed device */

    if (0x00000200 == (uRegisterValue & 0x00000200))
        {
        /* Device connected is a low speed device */
        return USBHST_LOW_SPEED;
        }

    /* Device connected is a full speed device */
    return USBHST_FULL_SPEED;

    } /* End of function usbOhciGetDeviceSpeed () */

#endif

/* End of File usbOhciDebug.c */
