/* usbMhdrcHcdDebug.c - Mentor Graphics HCD debug routines */

/*
 * Copyright (c) 2009-2011, 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01g,03may13,wyy  Remove compiler warning (WIND00356717)
01f,21apr13,wyy  Cut the relationship among busIndex of HCDData, unitNumber of
                 vxBus Device, and HC count (such as g_EHCDControllerCount or
                 g_UhcdHostControllerCount) to dynamically release or announce
                 a HCD driver (WIND00362065)
01e,13dec11,m_y  Modify according to code check result (WIND00319317)
01d,08apr11,w_x  Clean up MUSBMHDRC platform definitions
01c,13sep10,ghs  Fix defect found by code review of merge (WIND00232740)
01b,16aug10,w_x  VxWorks 64 bit audit and warning removal
01a,11nov09,s_z  written
*/

/*
DESCRIPTION

This file contains routines for display the Mentor Graphics HCD registers,
memory, HCD data structures resource, pipe structure resouce,request resouces, etc.
This interfaces exposed from this file can used to debug the MHCD driver.

INCLUDE FILES: usb/usb.h, usb/usbd.h, usb/usbOsal.h, usb/usbHst.h, usb/usbPhyUlpi.h
               usbMhdrc.h, usbMhdrcHcd.h
SEE ALSO:

None
*/

/* includes */

#include <usb/usb.h>
#include <usb/usbd.h>
#include <usb/usbOsal.h>
#include <usb/usbHst.h>
#include <usb/usbPhyUlpi.h>
#include <usbMhdrc.h>
#include <usbMhdrcHcd.h>

/* typedefs */

/* defines */

#define USB_MHCD_SHOW

#ifdef USB_MHCD_SHOW

#define USB_MHCD_SHOW_LEVEL_0   0
#define USB_MHCD_SHOW_LEVEL_1   1
#define USB_MHCD_SHOW_LEVEL_2   2

/***************************************************************************
*
* usbMhdrcHcdRequestShow - show request info detail
*
* This routine desplay the request current values.
*
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcHcdRequestShow
    (
    pUSB_MHCD_REQUEST_INFO pRequestInfo
    )
    {
    if (!pRequestInfo)
        {
        printf("pRequestInfo NULL\n");
        return;
        }
    else
        {
        printf("\n");
        printf(" pRequestInfo  %p\n", pRequestInfo);
        printf("  pRequestInfo->requestNode Address %p\n", &(pRequestInfo->requestNode));
        printf("  pRequestInfo->pUrb %p\n", pRequestInfo->pUrb);
        printf("  pRequestInfo->pHCDPipe %p\n", pRequestInfo->pHCDPipe);
        printf("  pRequestInfo->pHCDData %p\n", pRequestInfo->pHCDData);
        printf("  pRequestInfo->uIsoUrbCurrentPacketIndex 0x%X\n",
               pRequestInfo->uIsoUrbCurrentPacketIndex);
        printf("  pRequestInfo->uIsoUrbTotalPacketCount 0x%X\n",
               pRequestInfo->uIsoUrbTotalPacketCount);
        printf("  pRequestInfo->uActLength 0x%X\n", pRequestInfo->uActLength);
        printf("  pRequestInfo->uXferSize 0x%X\n", pRequestInfo->uXferSize);
        printf("  pRequestInfo->pCurrentBuffer %p\n",
               pRequestInfo->pCurrentBuffer);
        printf("  pRequestInfo->uTransactionLength 0x%X\n",
               pRequestInfo->uTransactionLength);
        printf("  pRequestInfo->uFrameNumberLast 0x%X\n",
               pRequestInfo->uFrameNumberLast);
        printf("  pRequestInfo->uFrameNumberNext 0x%X\n",
               pRequestInfo->uFrameNumberNext);
        printf("  pRequestInfo->uIsoPipeUrbIndex 0x%X\n",
               pRequestInfo->uIsoPipeUrbIndex);
        printf("  pRequestInfo->uTransferFlag 0x%X\n",
               pRequestInfo->uTransferFlag);
        printf("  pRequestInfo->uStage 0x%X\n", pRequestInfo->uStage);
        printf("  pRequestInfo->uRetried 0x%X\n", pRequestInfo->uRetried);
        printf("  pRequestInfo->submitState 0x%X\n", pRequestInfo->submitState);

        return;
        }
    }


/***************************************************************************
*
* usbMhdrcHcdRequestListShow - show request lists
*
* This routine desplay the request lists current values.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcHcdRequestListShow
    (
    LIST * pList,
    UINT8  uShowLevel
    )
    {
    NODE * pNode = NULL;
    int    i = 0;
    int    count = 0;

    if (!pList)
        {
        printf("List NULL\n");
        return;
        }

    count = lstCount(pList);
    printf("  RequestInfo Lists %p count 0x%X\n", pList, count);

    for (i = 1; i <= count; i ++)
        {
        pNode = lstNth (pList, i);
        if (pNode != NULL)
            {
            printf("\n");
            printf(" pRequestInfo List Iterm [%d]  %p\n", i - 1 ,pNode);
            if (uShowLevel > USB_MHCD_SHOW_LEVEL_1)
                {
                usbMhdrcHcdRequestShow ((pUSB_MHCD_REQUEST_INFO)pNode);
                }
            }
        }

    return;
    }


/***************************************************************************
*
* usbMhdrcHcdPipeShow - dump pipe resource
*
* This routine desplay the pipe resource current values.
*
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcHcdPipeShow
    (
    pUSB_MHCD_PIPE  pHCDPipe,
    UINT8           uShowLevel
    )
    {
    pUSBD_DEVICE_INFO pDeviceInfo = NULL; /* Ptr to hold translated dev info */

    if (!pHCDPipe)
        {
        printf("pHCDPipe NULL\n");
        return;
        }
    else
        {
        printf("pHCDPipe %p\n", pHCDPipe);
        printf("\n");
        printf(" pHCDPipe->pipeListNode Address %p\n", &(pHCDPipe->pipeListNode));
        printf(" pHCDPipe->pPipeSynchMutex %p\n",
                pHCDPipe->pPipeSynchMutex);
        printf(" pHCDPipe->uHandle 0x%X\n",pHCDPipe->uHandle);
        printf(" pHCDPipe->uIsoPipeUrbCount 0x%lX\n",
                (ULONG)pHCDPipe->uIsoPipeUrbCount);
        printf(" pHCDPipe->uIsoPipeCurrentUrbIndex 0x%X\n",
                (UINT32)pHCDPipe->uIsoPipeCurrentUrbIndex);
        printf(" pHCDPipe->uMaximumPacketSize 0x%X\n",
                pHCDPipe->uMaximumPacketSize);
        printf(" pHCDPipe->uMultiTransPerMicroFrame 0x%X\n",
                pHCDPipe->uMultiTransPerMicroFrame);
        printf(" pHCDPipe->uHubInfo 0x%X\n", pHCDPipe->uHubInfo);
        printf(" pHCDPipe->uEndpointAddress 0x%X\n", pHCDPipe->uEndpointAddress);
        printf(" pHCDPipe->uDeviceAddress 0x%X\n", pHCDPipe->uDeviceAddress);
        printf(" pHCDPipe->uSpeed 0x%X\n", pHCDPipe->uSpeed);
        printf(" pHCDPipe->uEndpointType 0x%X\n", pHCDPipe->uEndpointType);
        printf(" pHCDPipe->uEndpointDir 0x%X\n", pHCDPipe->uEndpointDir);
        printf(" pHCDPipe->uDataToggle 0x%X\n", pHCDPipe->uDataToggle);
        printf(" pHCDPipe->uDmaChannel 0x%X\n", pHCDPipe->uDmaChannel);
        printf(" pHCDPipe->uPipeFlag 0x%X\n", pHCDPipe->uPipeFlag);
        printf(" pHCDPipe->bIsHalted 0x%X\n", pHCDPipe->bIsHalted);
        printf(" pHCDPipe->bInterval 0x%X\n", pHCDPipe->bInterval);
        printf(" pHCDPipe->uEndPointEngineAsigned 0x%X\n",
                pHCDPipe->uEndPointEngineAsigned);
        printf(" pHCDPipe->uListIndex 0x%X\n", pHCDPipe->uListIndex);
        printf(" pHCDPipe->uBandwidth 0x%lX\n", pHCDPipe->uBandwidth);
        printf(" pHCDPipe->uUFrameMaskValue 0x%X\n", pHCDPipe->uUFrameMaskValue);
        printf(" pHCDPipe->uBusIndex 0x%X\n", pHCDPipe->uBusIndex);
        usbdTranslateDeviceHandle(pHCDPipe->uHandle, &pDeviceInfo);
        printf(" pHCDPipe->pDeviceInfo %p\n", pDeviceInfo);
        printf(" pHCDPipe->RequestInfoList %p\n", &pHCDPipe->RequestInfoList);

        /* Show the request list on the pipe */

        usbMhdrcHcdRequestListShow(&pHCDPipe->RequestInfoList, uShowLevel);

        return;
        }
    }

/***************************************************************************
*
* usbMhdrcHcdPipetListShow - show pipe lists
*
* This routine desplay the pipe lists current values.
*
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcHcdPipetListShow
    (
    LIST * pList,
    UINT8  uShowLevel
    )
    {
    NODE * pNode = NULL;
    int    i = 0;
    int    count = 0;

    if (!pList)
        {
        printf("List NULL\n");
        return;
        }

    count = lstCount(pList);

    printf("Pipe Lists %p count 0x%X \n",pList,count);

    for (i = 1; i <= count; i++)
        {

        pNode = lstNth (pList,i);
        if (pNode != NULL)
            {
            printf("\n");
            printf(" pHCDPipe List Iterm [%d]  %p\n", i - 1 ,pNode);
            usbMhdrcHcdPipeShow((pUSB_MHCD_PIPE)(pNode), uShowLevel);
            }
        }

    return;
    }

/***************************************************************************
*
* usbMhdrcHcdDeviceInfoShow - show device info resource
*
* This routine desplay the device info resource current values.
*
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcHcdDeviceInfoShow
    (
    pUSBD_DEVICE_INFO pDeviceInfo
    )
    {
    if (!pDeviceInfo)
        {
        printf("pDeviceInfo NULL\n");
        return;
        }
    else
        {
        printf("pDeviceInfo %p\n",pDeviceInfo);
        printf("\n");
        printf(" pDeviceInfo->hDevice 0x%X\n", pDeviceInfo->hDevice);
        printf(" pDeviceInfo->uDeviceSpeed 0x%X\n", pDeviceInfo->uDeviceSpeed);
        printf(" pDeviceInfo->uDeviceAddress 0x%X\n", pDeviceInfo->uDeviceAddress);
        printf(" pDeviceInfo->uBusIndex 0x%X\n", pDeviceInfo->uBusIndex);
        printf(" pDeviceInfo->uHighSpeedHubInfo 0x%X\n",
                 pDeviceInfo->uHighSpeedHubInfo);
        printf(" pDeviceInfo->uParentPortIndex 0x%X\n",
                 pDeviceInfo->uParentPortIndex);
        printf(" pDeviceInfo->uParentTier 0x%X\n", pDeviceInfo->uParentTier);
        printf(" pDeviceInfo->hDefaultPipe 0x%lX\n", pDeviceInfo->hDefaultPipe);
        printf(" pDeviceInfo->uBCDUSB 0x%X\n", pDeviceInfo->uBCDUSB);
        printf(" pDeviceInfo->uDeviceClass 0x%X\n", pDeviceInfo->uDeviceClass);
        printf(" pDeviceInfo->uDeviceSubClass 0x%X\n", pDeviceInfo->uDeviceSubClass);

        printf(" pDeviceInfo->uDeviceProtocol 0x%X\n", pDeviceInfo->uDeviceProtocol);
        printf(" pDeviceInfo->uVendorID 0x%X\n", pDeviceInfo->uVendorID);
        printf(" pDeviceInfo->uDeviceID 0x%X\n", pDeviceInfo->uDeviceID);
        printf(" pDeviceInfo->uBCDDevice 0x%X\n", pDeviceInfo->uBCDDevice);
        printf(" pDeviceInfo->pDriver %p\n", pDeviceInfo->pDriver);

        printf(" pDeviceInfo->pDriverData %p\n", pDeviceInfo->pDriverData);
        printf(" pDeviceInfo->uInterfaceNumber 0x%X\n",
                 pDeviceInfo->uInterfaceNumber);
        printf(" pDeviceInfo->uInterfaceCount 0x%X\n",
                 pDeviceInfo->uInterfaceCount);
        printf(" pDeviceInfo->pCurrentConfiguration %p\n",
                 pDeviceInfo->pCurrentConfiguration);
        printf(" pDeviceInfo->uNumConfigurations 0x%X\n",
                 pDeviceInfo->uNumConfigurations);
        printf(" pDeviceInfo->uConfigCRCValue 0x%X\n", pDeviceInfo->uConfigCRCValue);
        printf(" pDeviceInfo->pInterfacesInfo %p\n", pDeviceInfo->pInterfacesInfo);
        printf(" pDeviceInfo->pNextUSBDeviceInfo %p\n",
                 pDeviceInfo->pNextUSBDeviceInfo);
        printf(" pDeviceInfo->pDev %p\n", pDeviceInfo->pDev);
        printf(" pDeviceInfo->bAnnounced 0x%X\n", pDeviceInfo->bAnnounced);
        printf(" pDeviceInfo->bIsComposite 0x%X\n", pDeviceInfo->bIsComposite);

        return;
        }

   }

/***************************************************************************
*
* usbMhdrcHcdDriverDataShow - show pUSBHST_DEVICE_DRIVER resource
*
* This routine desplay the pUSBHST_DEVICE_DRIVER resource current values.
*
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcHcdDriverDataShow
    (
    pUSBHST_DEVICE_DRIVER pDriverData
    )
    {
    if (!pDriverData)
        {
        printf("pDriverData NULL\n");
        return;
        }
    else
        {
        printf("pDriverData %p\n",pDriverData);
        printf("\n");
        printf(" pDriverData->vxbDriverInfo pointer %p\n",
                 &pDriverData->vxbDriverInfo);

        /* Show the information in vxbDriverInfo */

        printf("  pDriverData->vxbDriverInfo.pNext %p\n",
                  pDriverData->vxbDriverInfo.pNext);
        printf("  pDriverData->vxbDriverInfo.devID 0x%X\n",
                  pDriverData->vxbDriverInfo.devID);
        printf("  pDriverData->vxbDriverInfo.busID 0x%X\n",
                  pDriverData->vxbDriverInfo.busID);
        printf("  pDriverData->vxbDriverInfo.vxbVersion 0x%X\n",
                  pDriverData->vxbDriverInfo.vxbVersion);
        printf("  pDriverData->vxbDriverInfo.drvName %s\n",
                  pDriverData->vxbDriverInfo.drvName);
        printf("  pDriverData->vxbDriverInfo.pDrvBusFuncs %p\n",
                  pDriverData->vxbDriverInfo.pDrvBusFuncs);
        printf("  pDriverData->vxbDriverInfo.pMethods %p\n",
                  pDriverData->vxbDriverInfo.pMethods);
        printf("  pDriverData->vxbDriverInfo.devProbe %p\n",
                  pDriverData->vxbDriverInfo.devProbe);
        printf("  pDriverData->vxbDriverInfo.pParamDefaults %p\n",
                  pDriverData->vxbDriverInfo.pParamDefaults);
        printf(" pDriverData->bFlagVendorSpecific 0x%X\n",
                 pDriverData->bFlagVendorSpecific);
        printf(" pDriverData->uVendorIDorClass 0x%X\n",
                 pDriverData->uVendorIDorClass);
        printf(" pDriverData->uProductIDorSubClass 0x%X\n",
                 pDriverData->uProductIDorSubClass);
        printf(" pDriverData->uBCDUSBorProtocol 0x%X\n",
                 pDriverData->uBCDUSBorProtocol);
        printf(" pDriverData->pDeviceList %p\n",pDriverData->pDeviceList);
        printf(" pDriverData->addDevice %p\n",pDriverData->addDevice);
        printf(" pDriverData->removeDevice%p\n",pDriverData->removeDevice);
        printf(" pDriverData->suspendDevice %p\n",pDriverData->suspendDevice);
        printf(" pDriverData->resumeDevice %p\n",pDriverData->resumeDevice);

        return;
        }

    }

/***************************************************************************
*
* usbMhdrcHcdRegisterWrite - write value to the common registers
*
* This routine writes the value to the common registers.
*
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcHcdRegisterWrite
    (
    UINT8   uBusIndex, /* Bus Index */
    UINT32  uOffset,   /* Register offset */
    UINT32  uValue,    /* value to be write */
    UINT8   uBytes     /* 1  byte, 2 wolds , 4 dwords*/
    )
    {
    pUSB_MHCD_DATA  pHCDData = NULL;
    pUSB_MUSBMHDRC  pMHDRC;

    if (USB_MAX_MHCI_COUNT < uBusIndex)
        {
        printf ("Parameter uBusIndex should between 0 ~ %d\n",
               USB_MAX_MHCI_COUNT);

        return;
        }

    pHCDData = gpMHCDData[uBusIndex] ;

    if (!pHCDData || !pHCDData->pMHDRC)
        {
        return;
        }

    /* Get the common hardware data structure */

    pMHDRC = pHCDData->pMHDRC;

    switch (uBytes)
        {
        case 1:
            USB_MHDRC_REG_WRITE8 (pMHDRC,
                                 uOffset,
                                 (UINT8)(uValue & 0xff));
            printf(" Write Common Register offset 0x%X Value 0x%X\n", uOffset,
                   USB_MHDRC_REG_READ8 (pMHDRC,
                                       uOffset));
            break;
        case 2:
            USB_MHDRC_REG_WRITE16 (pMHDRC,
                                  uOffset,
                                  (UINT16)(uValue & 0xffff));

            printf(" Write Common Register offset 0x%X Value 0x%X\n", uOffset,
                    USB_MHDRC_REG_READ16 (pMHDRC,
                                         uOffset));
            break;
        case 4:
            USB_MHDRC_REG_WRITE32 (pMHDRC,
                                  uOffset,
                                  uValue );
            printf(" Write Common Register offset 0x%X Value 0x%X\n", uOffset,
                     USB_MHDRC_REG_READ32 (pMHDRC,
                                          uOffset));
            break;
        default:
            printf ("Invalid uBytes 1 - byte, 2 - wolds , 4 - dwords\n");
            break;
        }

    return;
    }

/***************************************************************************
*
* usbMhdrcHcdRegisterRead - read value to the common registers
*
* This routine reads the value from the common registers.
*
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcHcdRegisterRead
    (
    UINT8   uBusIndex, /* Bus Index */
    UINT32  uOffset,   /* Register offset */
    UINT8   uBytes     /* 1  byte, 2 wolds , 4 dwords*/
    )
    {
    pUSB_MHCD_DATA  pHCDData = NULL;
    pUSB_MUSBMHDRC  pMHDRC;

    if (USB_MAX_MHCI_COUNT < uBusIndex)
        {
        printf ("Parameter uBusIndex should between 0 ~ %d\n",
               USB_MAX_MHCI_COUNT);

        return;
        }

    pHCDData = gpMHCDData[uBusIndex];

    if (!pHCDData || !pHCDData->pMHDRC)
        {
        return;
        }

    /* Get the common hardware data structure */

    pMHDRC = pHCDData->pMHDRC;

    switch (uBytes)
        {
        case 1:
            printf(" Read Common Register offset 0x%X Value 0x%X\n", uOffset,
            USB_MHDRC_REG_READ8 (pMHDRC,
                                uOffset));
            break;
        case 2:
             printf(" Read Common Register offset 0x%X Value 0x%X\n", uOffset,
                  USB_MHDRC_REG_READ16 (pMHDRC,
                                       uOffset));
            break;
        case 4:
            printf(" Read Common Register offset 0x%X Value 0x%X\n", uOffset,
                    USB_MHDRC_REG_READ32 (pMHDRC,
                                         uOffset));
            break;
        default:
            printf ("Invalid uBytes 1 - byte, 2 - wolds , 4 - dwords\n");
            break;
        }

    return;
    }

/***************************************************************************
*
* usbMhdrcHcdRegisterReadP - read private registers
*
* This routine reads the current value from private registers of the platform.
*
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcHcdRegisterReadP
    (
    UINT8   uBusIndex, /* Bus Index */
    UINT32  uOffset    /* Register offset */
    )
    {
    pUSB_MHCD_DATA  pHCDData = NULL;

    if (USB_MAX_MHCI_COUNT < uBusIndex)
        {
        printf ("Parameter uBusIndex should between 0 ~ %d\n",
               USB_MAX_MHCI_COUNT);

        return;
        }

    pHCDData = gpMHCDData[uBusIndex];

    if (!pHCDData || !pHCDData->pMHDRC)
        {
        return;
        }

    printf(" Read Private Register offset 0x%X Value 0x%X\n", uOffset,
             USB_MHDRC_DAVINCI_PRIVATE_REG_READ32 (pHCDData->pMHDRC,
                                          uOffset));
    }

/***************************************************************************
*
* usbMhdrcHcdRegisterWriteP - write provate registers
*
* This routine writes the current value to private registers of the platform.
*
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcHcdRegisterWriteP
    (
    UINT8    uBusIndex, /* Bus Index */
    UINT32   uOffset,   /* Register offset */
    UINT32   uValue     /* Value to be write */
    )
    {
    pUSB_MHCD_DATA  pHCDData = NULL;

    if (USB_MAX_MHCI_COUNT < uBusIndex)
        {
        printf ("Parameter uBusIndex should between 0 ~ %d\n",
               USB_MAX_MHCI_COUNT);

        return;
        }

    pHCDData = gpMHCDData[uBusIndex];

    if (!pHCDData || !pHCDData->pMHDRC)
        {
        return;
        }

    USB_MHDRC_DAVINCI_PRIVATE_REG_WRITE32 (pHCDData->pMHDRC,
                                  uOffset,
                                  uValue );

    printf(" Write Private Register offset 0x%X Value 0x%X\n", uOffset,
             USB_MHDRC_DAVINCI_PRIVATE_REG_READ32 (pHCDData->pMHDRC,
                                          uOffset));

    }

/*******************************************************************************
*
* usbMhdrcHcdDevCtlRegShow - show DEVCTL register
*
* This routine desplay the DEVCTL register current values.
*
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcHcdCriticalRegShow
    (
    UINT8   uBusIndex /* Bus Index */
    )
    {
    pUSB_MHCD_DATA  pHCDData;
    pUSB_MUSBMHDRC  pMHDRC;
    UINT8           uReg8;
    UINT8           uVbus;

    if ((gpMHCDData == NULL) ||
        (USB_MAX_MHCI_COUNT < uBusIndex))
        {
        printf ("Parameter uBusIndex should between 0 ~ %d\n",
               USB_MAX_MHCI_COUNT);

        return;
        }

    pHCDData = gpMHCDData[uBusIndex];

    if (!pHCDData || !pHCDData->pMHDRC)
        {
        return;
        }

    /* Get the common hardware data structure */

    pMHDRC = pHCDData->pMHDRC;

    uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_DEVCTL);

    printf("DEVCTL 0ffset 0x%X Value 0x%X\n", USB_MHDRC_DEVCTL, uReg8);

    if (uReg8 & USB_MHDRC_DEVCTL_BDEVICE)
        {
        printf("B-device\n");
        }
    else
        {
        printf("A-device\n");
        }

    if (uReg8 & USB_MHDRC_DEVCTL_FSDEV)
        {
        printf("FSDEV\n");
        }
    else
        {
        printf("non-FSDEV\n");
        }

    if (uReg8 & USB_MHDRC_DEVCTL_LSDEV)
        {
        printf("LSDEV\n");
        }
    else
        {
        printf("non-LSDEV\n");
        }

    uVbus = (UINT8)((uReg8 & USB_MHDRC_DEVCTL_VBUS) >> USB_MHDRC_DEVCTL_S_VBUS);

    if (uVbus == 0)
        {
        printf("VBUS Below Session End\n");
        }
    else if (uVbus == 1)
        {
        printf("VBUS Above Session End, below AValid\n");
        }
    else if (uVbus == 2)
        {
        printf("VBUS Above AValid, below VBusValid\n");
        }
    else /* if (uVbus == 3) */
        {
        printf("VBUS Above VBusValid\n");
        }

    if (uReg8 & USB_MHDRC_DEVCTL_HM)
        {
        printf("USB controller is acting as a Host.\n");
        }
    else
        {
        printf("USB controller is acting as a Device.\n");
        }

    if (uReg8 & USB_MHDRC_DEVCTL_HOSTREQ)
        {
        printf("HOSTREQ is set to initiate the Host Negotiation"
               "when Suspend mode is entered\n");
        }
    else
        {
        printf("HOSTREQ is not set\n");
        }

    if (uReg8 & USB_MHDRC_DEVCTL_SESSION)
        {
        printf("SESSION active\n");
        }
    else
        {
        printf("SESSION not active\n");
        }

    uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_POWER);

    printf("POWER 0ffset 0x%X Value 0x%X\n", USB_MHDRC_POWER, uReg8);

    if (uReg8 & USB_MHDRC_POWER_ENSUSPM)
        {
        printf("ENSUSPM is set\n");
        }
    else
        {
        printf("ENSUSPM is cleared\n");
        }

    if (uReg8 & USB_MHDRC_POWER_SUSPENDM)
        {
        printf("SUSPENDM is set\n");
        }
    else
        {
        printf("SUSPENDM is cleared\n");
        }

    if (uReg8 & USB_MHDRC_POWER_RESUME)
        {
        printf("RESUME is set\n");
        }
    else
        {
        printf("RESUME is cleared\n");
        }

    if (uReg8 & USB_MHDRC_POWER_RESET)
        {
        printf("RESET is set\n");
        }
    else
        {
        printf("RESET is cleared\n");
        }

    if (uReg8 & USB_MHDRC_POWER_HSMODE)
        {
        printf("HSMODE is set\n");
        }
    else
        {
        printf("HSMODE is cleared\n");
        }

    if (uReg8 & USB_MHDRC_POWER_HSEN)
        {
        printf("HSEN is set\n");
        }
    else
        {
        printf("HSEN is cleared\n");
        }

    if (uReg8 & USB_MHDRC_POWER_SOFTCONN)
        {
        printf("SOFTCONN is set\n");
        }
    else
        {
        printf("SOFTCONN is cleared\n");
        }

    uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_INTRUSB);

    printf("INTRUSB 0ffset 0x%X Value 0x%X\n", USB_MHDRC_INTRUSB, uReg8);

    if (uReg8 & USB_MHDRC_INTR_DISCONNECT)
        {
        printf("USB_MHDRC_INTR_DISCONNECT is set\n");
        }

    if (uReg8 & USB_MHDRC_INTR_CONNECT)
        {
        printf("USB_MHDRC_INTR_CONNECT is set\n");
        }

    if (uReg8 & USB_MHDRC_INTR_SESSREQ)
        {
        printf("USB_MHDRC_INTR_SESSREQ is set\n");
        }

    uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_INTRUSBE);

    printf("INTRUSBE 0ffset 0x%X Value 0x%X\n", USB_MHDRC_INTRUSBE, uReg8);

    printf("\n");
    }

void usbMhdrcHcdClearSoftConnect
    (
    UINT8   uBusIndex /* Bus Index */
    )
    {
    pUSB_MHCD_DATA  pHCDData;
    pUSB_MUSBMHDRC  pMHDRC;
    UINT8           uReg8;

    if ((gpMHCDData == NULL) ||
        (USB_MAX_MHCI_COUNT < uBusIndex))
        {
        printf ("Parameter uBusIndex should between 0 ~ %d\n",
               USB_MAX_MHCI_COUNT);

        return;
        }

    pHCDData = gpMHCDData[uBusIndex];

    if (!pHCDData || !pHCDData->pMHDRC)
        {
        return;
        }

    /* Get the common hardware data structure */

    pMHDRC = pHCDData->pMHDRC;

    USB_MHDRC_WARN("Clr the SOFTCONN bit\n", 1, 2, 3, 4, 5 ,6);

    /* Read the POWER register */

    uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_POWER);

    uReg8 = (UINT8)(uReg8 & ~USB_MHDRC_POWER_SOFTCONN);

    /* Clear the SOFTCONN bit */

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                          USB_MHDRC_POWER,
                          uReg8);
    }

void usbMhdrcHcdCtlrReset
    (
    UINT8 uBusIndex
    )
    {
    pUSB_MHCD_DATA  pHCDData;
    pUSB_MUSBMHDRC  pMHDRC;
    UINT32          uReg32;
    UINT32          uCount;

    if ((gpMHCDData == NULL) ||
        (USB_MAX_MHCI_COUNT < uBusIndex))
        {
        printf ("Parameter uBusIndex should between 0 ~ %d\n",
               USB_MAX_MHCI_COUNT);

        return;
        }

    pHCDData = gpMHCDData[uBusIndex];

    if (!pHCDData || !pHCDData->pMHDRC)
        {
        return;
        }

    /* Get the common hardware data structure */

    pMHDRC = pHCDData->pMHDRC;

    USB_MHDRC_REG_WRITE32 (pMHDRC,
                          USB_MHDRC_OTG_SYSCONFIG,
                          OTG_SYSCONFIG_SOFTRST);

    uCount = 0;

    uReg32 = USB_MHDRC_REG_READ32 (pMHDRC, USB_MHDRC_OTG_SYSSTATUS);

    while (!(uReg32 & OTG_SYSSTATUS_RESETDONE) && (uCount++ < 100))
        {
        uReg32 = USB_MHDRC_REG_READ32 (pMHDRC, USB_MHDRC_OTG_SYSSTATUS);
        }

    if (uCount == 100)
        {
        printf("Reset timeout\n");
        }

    return;
    }
void usbMhdrcHcdSetSoftConnect
    (
    UINT8   uBusIndex /* Bus Index */
    )
    {
    pUSB_MHCD_DATA  pHCDData;
    pUSB_MUSBMHDRC  pMHDRC;
    UINT8           uReg8;

    if ((gpMHCDData == NULL) ||
        (USB_MAX_MHCI_COUNT < uBusIndex))
        {
        printf ("Parameter uBusIndex should between 0 ~ %d\n",
               USB_MAX_MHCI_COUNT);

        return;
        }

    pHCDData = gpMHCDData[uBusIndex];

    if (!pHCDData || !pHCDData->pMHDRC)
        {
        return;
        }

    /* Get the common hardware data structure */

    pMHDRC = pHCDData->pMHDRC;

    USB_MHDRC_WARN("Set the SOFTCONN bit\n", 1, 2, 3, 4, 5 ,6);

    /* Read the POWER register */

    uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_POWER);

    uReg8 |= (USB_MHDRC_POWER_SOFTCONN);

    /* Clear the SOFTCONN bit */

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                          USB_MHDRC_POWER,
                          uReg8);
    }

void usbMhdrcHcdPortReset
    (
    UINT8   uBusIndex /* Bus Index */
    )
    {
    pUSB_MHCD_DATA  pHCDData;
    pUSB_MUSBMHDRC  pMHDRC;
    UINT8           uReg8;

    if ((gpMHCDData == NULL) ||
        (USB_MAX_MHCI_COUNT < uBusIndex))
        {
        printf ("Parameter uBusIndex should between 0 ~ %d\n",
               USB_MAX_MHCI_COUNT);

        return;
        }

    pHCDData = gpMHCDData[uBusIndex];

    if (!pHCDData || !pHCDData->pMHDRC)
        {
        return;
        }

    /* Get the common hardware data structure */

    pMHDRC = pHCDData->pMHDRC;

    USB_MHDRC_WARN("Set the SOFTCONN bit\n", 1, 2, 3, 4, 5 ,6);

    /* Read the POWER register */

    uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_POWER);

    /* Set the RESET bit */

    uReg8 |= (USB_MHDRC_POWER_RESET);


    USB_MHDRC_REG_WRITE8 (pMHDRC,
                          USB_MHDRC_POWER,
                          uReg8);

    OS_DELAY_MS(50);

    /* Read the POWER register */

    uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_POWER);

    /* Clear the RESET bit */

    uReg8 = (UINT8)(uReg8 & ~USB_MHDRC_POWER_RESET);

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                          USB_MHDRC_POWER,
                          uReg8);
    }

void usbMhdrcHcdDevCtlSession
    (
    UINT8   uBusIndex,
    BOOL    bEn
    )
    {
    pUSB_MHCD_DATA  pHCDData;
    pUSB_MUSBMHDRC  pMHDRC;
    UINT8           uReg8;

    if ((gpMHCDData == NULL) ||
        (USB_MAX_MHCI_COUNT < uBusIndex))
        {
        printf ("Parameter uBusIndex should between 0 ~ %d\n",
               USB_MAX_MHCI_COUNT);

        return;
        }

    pHCDData = gpMHCDData[uBusIndex];

    if (!pHCDData || !pHCDData->pMHDRC)
        {
        return;
        }

    /* Get the common hardware data structure */

    pMHDRC = pHCDData->pMHDRC;

    /* Read the DEVCTL register */

    uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_DEVCTL);

    if (bEn == TRUE)
        {
        USB_MHDRC_WARN("Set the SESSION bit\n", 1, 2, 3, 4, 5 ,6);

        /* Set the SESSION bit */

        USB_MHDRC_REG_WRITE8 (pMHDRC,
                              USB_MHDRC_DEVCTL,
                              uReg8 | USB_MHDRC_DEVCTL_SESSION);
        }
    else
        {
        USB_MHDRC_WARN("Clr the SESSION bit\n", 1, 2, 3, 4, 5 ,6);

        /* Clear the SESSION bit */

        USB_MHDRC_REG_WRITE8 (pMHDRC,
                              USB_MHDRC_DEVCTL,
                              (UINT8)(uReg8 & ~USB_MHDRC_DEVCTL_SESSION));
        }

    /* Read the POWER register */

    uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_POWER);

    if (uReg8 & USB_MHDRC_POWER_SOFTCONN)
        {
        USB_MHDRC_WARN("Clr the SOFTCONN bit\n", 1, 2, 3, 4, 5 ,6);

        /* Clear the SOFTCONN bit */

        USB_MHDRC_REG_WRITE8 (pMHDRC,
                              USB_MHDRC_POWER,
                              (UINT8)(uReg8 & ~USB_MHDRC_POWER_SOFTCONN));
        }

    }



void usbMhdrcHcdUlpiSession
    (
    UINT8   uBusIndex
    )
    {
    pUSB_MHCD_DATA  pHCDData;
    pUSB_MUSBMHDRC  pMHDRC;
    UINT8           uReg8;

    if ((gpMHCDData == NULL) ||
        (USB_MAX_MHCI_COUNT < uBusIndex))
        {
        printf ("Parameter uBusIndex should between 0 ~ %d\n",
               USB_MAX_MHCI_COUNT);

        return;
        }

    pHCDData = gpMHCDData[uBusIndex];

    if (!pHCDData || !pHCDData->pMHDRC)
        {
        return;
        }

    /* Get the common hardware data structure */

    pMHDRC = pHCDData->pMHDRC;

    usbMhdrcUlpiWrite(pMHDRC, USB_ULPI_FUNCTION_CTL  + USB_ULPI_SET_ADJ,
                      ULPI_FUNC_CTRL_RESET);

    /* Make sure the ID pull-up is set */

    usbMhdrcUlpiWrite(pMHDRC, USB_ULPI_OTG_CTL + USB_ULPI_SET_ADJ,
                      ULPI_OTG_CTL_ID_PULL_UP);

    if (usbMhdrcUlpiRead(pMHDRC, USB_ULPI_INTR_STATUS, &uReg8) != OK)
        {
        /* Read the DEVCTL register */

        uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_DEVCTL);

        USB_MHDRC_WARN("DEVCTL USBOTG_ID_STATE %s\n",
                   (uReg8 & USB_MHDRC_DEVCTL_BDEVICE) ? "B-device" : "A-device",
                   2, 3, 4, 5, 6);
        }
    else
        {
        USB_MHDRC_WARN("ULPI USBOTG_ID_STATE %s\n",
                   (uReg8 & ULPI_INTR_IdGnd) ? "B-device" : "A-device",
                   2, 3, 4, 5, 6);
        }
    }

void usbMhdrcHcdDrvVbusSession
    (
    UINT8   uBusIndex,
    BOOL    bEn
    )
    {
    pUSB_MHCD_DATA  pHCDData;
    pUSB_MUSBMHDRC  pMHDRC;
    UINT8           uReg8;
    UINT32          count = 0;

    if ((gpMHCDData == NULL) ||
        (USB_MAX_MHCI_COUNT < uBusIndex))
        {
        printf ("Parameter uBusIndex should between 0 ~ %d\n",
               USB_MAX_MHCI_COUNT);

        return;
        }

    pHCDData = gpMHCDData[uBusIndex];

    if (!pHCDData || !pHCDData->pMHDRC)
        {
        return;
        }

    /* Get the common hardware data structure */

    pMHDRC = pHCDData->pMHDRC;

    if (bEn == TRUE)
        {
        USB_MHDRC_WARN("Set ULPI_OTG_CTL_DRV_VBUS\n", 1, 2, 3, 4, 5 ,6);

        usbMhdrcUlpiWrite(pMHDRC, USB_ULPI_OTG_CTL + USB_ULPI_SET_ADJ,
                             ULPI_OTG_CTL_DRV_VBUS);
        }
    else
        {
        USB_MHDRC_WARN("Clr ULPI_OTG_CTL_DRV_VBUS\n", 1, 2, 3, 4, 5 ,6);

        usbMhdrcUlpiWrite(pMHDRC, USB_ULPI_OTG_CTL + USB_ULPI_CLR_ADJ,
                             ULPI_OTG_CTL_DRV_VBUS);

        while (usbMhdrcUlpiRead(pMHDRC, USB_ULPI_INTR_STATUS, &uReg8) == OK)
            {
            if (uReg8 & ULPI_INTR_SessEnd)
                break;

            if (count++ > 10)
                break;
            }
        }

    /* Read the POWER register */

    uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_POWER);

    if (uReg8 & USB_MHDRC_POWER_SOFTCONN)
        {
        USB_MHDRC_WARN("Clr the SOFTCONN bit\n", 1, 2, 3, 4, 5 ,6);

        /* Clear the SOFTCONN bit */

        USB_MHDRC_REG_WRITE8 (pMHDRC,
                              USB_MHDRC_POWER,
                              (UINT8)(uReg8 & ~USB_MHDRC_POWER_SOFTCONN));
        }

    }

/*******************************************************************************
*
* usbMhdrcHcdForceHostMode - force the controller to be in Host Mode
*
* This routine forces the controller to be in Host Mode.
*
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcHcdForceHostMode
    (
    UINT8   uBusIndex /* Bus Index */
    )
    {
    pUSB_MHCD_DATA  pHCDData;
    pUSB_MUSBMHDRC  pMHDRC;

    if ((gpMHCDData == NULL) ||
        (USB_MAX_MHCI_COUNT < uBusIndex))
        {
        printf ("Parameter uBusIndex should between 0 ~ %d\n",
               USB_MAX_MHCI_COUNT);

        return;
        }

    pHCDData = gpMHCDData[uBusIndex];

    if (!pHCDData || !pHCDData->pMHDRC)
        {
        return;
        }

    /* Get the common hardware data structure */

    pMHDRC = pHCDData->pMHDRC;

    /* Force it to HOST mode */

    USB_MHDRC_REG_WRITE8 (pMHDRC, USB_MHDRC_TESTMODE, (1 << 7));

    return;
    }
/***************************************************************************
*
* usbMhdrcHcdRegShow - show common registers
*
* This routine desplay the common registers current values.
*
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcHcdRegShow
    (
    UINT8   uBusIndex /* Bus Index */
    )
    {
    pUSB_MHCD_DATA  pHCDData = NULL;
    UINT8           uDmaChannel;
    pUSB_MUSBMHDRC  pMHDRC;

    if (USB_MAX_MHCI_COUNT < uBusIndex)
        {
        printf ("Parameter uBusIndex should between 0 ~ %d\n",
               USB_MAX_MHCI_COUNT);

        return;
        }

    pHCDData = gpMHCDData[uBusIndex];

    if (!pHCDData || !pHCDData->pMHDRC)
        {
        return;
        }

    /* Get the common hardware data structure */

    pMHDRC = pHCDData->pMHDRC;

    printf("\n");

    /*
     * Some registers will cleared after reading, be careful if you want to
     * read such registers. Here we only read some registers which my concerned.
     */

    /* Get the Common USB register of Menter Graphics USB value */

    printf(" \nCommon USB Registers \n\n");

    printf(" FADDR     0ffset 0x%X Value 0x%X\n",USB_MHDRC_FADD,
                                   USB_MHDRC_REG_READ8 (pMHDRC,
                                    USB_MHDRC_FADD));
    printf(" POWER     0ffset 0x%X Value 0x%X\n",USB_MHDRC_POWER,
                                   USB_MHDRC_REG_READ8 (pMHDRC,
                                    USB_MHDRC_POWER));
    printf(" INTRTX    0ffset 0x%X Value 0x%X\n",USB_MHDRC_INTRTX,
                                    USB_MHDRC_REG_READ16 (pMHDRC,
                                    USB_MHDRC_INTRTX));
    printf(" INTRRX    0ffset 0x%X Value 0x%X\n",USB_MHDRC_INTRRX,
                                    USB_MHDRC_REG_READ16 (pMHDRC,
                                    USB_MHDRC_INTRRX));
    printf(" INTRTXE   0ffset 0x%X Value 0x%X\n",USB_MHDRC_INTRTXE,
                                    USB_MHDRC_REG_READ16 (pMHDRC,
                                    USB_MHDRC_INTRTXE));

    printf(" INTRRXE   0ffset 0x%X Value 0x%X\n",USB_MHDRC_INTRRXE,
                                    USB_MHDRC_REG_READ16 (pMHDRC,
                                    USB_MHDRC_INTRRXE));
    printf(" INTRUSB   0ffset 0x%X Value 0x%X\n",USB_MHDRC_INTRUSB,
                                    USB_MHDRC_REG_READ8 (pMHDRC,
                                    USB_MHDRC_INTRUSB));

    printf(" INTRUSBE  0ffset 0x%X Value 0x%X\n",USB_MHDRC_INTRUSBE,
                                    USB_MHDRC_REG_READ8 (pMHDRC,
                                    USB_MHDRC_INTRUSBE));
    printf(" FRAME     0ffset 0x%X Value 0x%X\n",USB_MHDRC_FRAME,
                                    USB_MHDRC_REG_READ16 (pMHDRC,
                                    USB_MHDRC_FRAME));

    printf(" INDEX     0ffset 0x%X Value 0x%X\n",USB_MHDRC_INDEX,
                                    USB_MHDRC_REG_READ8 (pMHDRC,
                                    USB_MHDRC_INDEX));
    printf(" TESTMODE  0ffset 0x%X Value 0x%X\n",USB_MHDRC_TESTMODE,
                                    USB_MHDRC_REG_READ8 (pMHDRC,
                                    USB_MHDRC_TESTMODE));

    printf(" \nDo Not Dump Indexed Registers \n\n");

    printf(" \nDo Not Dump FIFOn Registers \n\n");

    printf(" DEVCTL 0ffset 0x%X Value 0x%X\n",USB_MHDRC_DEVCTL,
                                    USB_MHDRC_REG_READ8 (pMHDRC,
                                    USB_MHDRC_DEVCTL));

    printf(" \nDynamic FIFO Registers \n\n");

    for (uDmaChannel = 1;
         uDmaChannel < pMHDRC->uNumDmaChannels;
         uDmaChannel ++)
        {
        USB_MHDRC_REG_WRITE8 (pMHDRC,
                            USB_MHDRC_INDEX,
                            uDmaChannel);

        printf(" TXFIFOSZ   (EP %d)  0ffset 0x%X Value 0x%X\n",uDmaChannel,
                                        USB_MHDRC_TXFIFOSZ,
                                        USB_MHDRC_REG_READ8 (pMHDRC,
                                        USB_MHDRC_TXFIFOSZ));
        printf(" RXFIFOSZ   (EP %d)  0ffset 0x%X Value 0x%X\n",uDmaChannel,
                                        USB_MHDRC_RXFIFOSZ,
                                        USB_MHDRC_REG_READ8 (pMHDRC,
                                        USB_MHDRC_RXFIFOSZ));
        printf(" TXFIFOADDR (EP %d)  0ffset 0x%X Value 0x%X\n",uDmaChannel,
                                        USB_MHDRC_TXFIFOADDR,
                                        USB_MHDRC_REG_READ16 (pMHDRC,
                                        USB_MHDRC_TXFIFOADDR));
        printf(" RXFIFOADDR (EP %d)  0ffset 0x%X Value 0x%X\n",uDmaChannel,
                                        USB_MHDRC_RXFIFOADDR,
                                        USB_MHDRC_REG_READ16 (pMHDRC,
                                        USB_MHDRC_RXFIFOADDR));

        USB_MHDRC_REG_WRITE8 (pMHDRC,
                             USB_MHDRC_INDEX,
                             0);

        }

    /* Omap3evm platform also has such registers */

    if (pMHDRC->uPlatformType == USB_MHDRC_PLATFORM_OMAP35xx)
        {
        printf(" \n ULPI Registers \n\n");

        printf(" ULPIVBUSCONTROL  0ffset 0x%X Value 0x%X\n",USB_MHDRC_ULPIVBUSCONTROL,
                                        USB_MHDRC_REG_READ8 (pMHDRC,
                                        USB_MHDRC_ULPIVBUSCONTROL));
        printf(" ULPIUTMICONTROL  0ffset 0x%X Value 0x%X\n",USB_MHDRC_ULPIUTMICONTROL,
                                        USB_MHDRC_REG_READ8 (pMHDRC,
                                        USB_MHDRC_ULPIUTMICONTROL));
        printf(" ULPIINTMASK      0ffset 0x%X Value 0x%X\n",USB_MHDRC_ULPIINTMASK,
                                        USB_MHDRC_REG_READ8 (pMHDRC,
                                        USB_MHDRC_ULPIINTMASK));
        printf(" ULPIINTSRC       0ffset 0x%X Value 0x%X\n",USB_MHDRC_ULPIINTSRC,
                                        USB_MHDRC_REG_READ8 (pMHDRC,
                                        USB_MHDRC_ULPIINTSRC));
        printf(" ULPIREGDATA      0ffset 0x%X Value 0x%X\n",USB_MHDRC_ULPIREGDATA,
                                        USB_MHDRC_REG_READ8 (pMHDRC,
                                        USB_MHDRC_ULPIREGDATA));
        printf(" ULPIREGADDR      0ffset 0x%X Value 0x%X\n",USB_MHDRC_ULPIREGADDR,
                                        USB_MHDRC_REG_READ8 (pMHDRC,
                                        USB_MHDRC_ULPIREGADDR));
        printf(" ULPIREGCONTROL   0ffset 0x%X Value 0x%X\n",USB_MHDRC_ULPIREGCONTROL,
                                        USB_MHDRC_REG_READ8 (pMHDRC,
                                        USB_MHDRC_ULPIREGCONTROL));
        printf(" ULPIRAWDATAL     0ffset 0x%X Value 0x%X\n",USB_MHDRC_ULPIRAWDATAL,
                                        USB_MHDRC_REG_READ8 (pMHDRC,
                                        USB_MHDRC_ULPIRAWDATAL));
        }

    printf(" \nFunction Registers for each Channel \n\n");

    for (uDmaChannel = 0;
         uDmaChannel < pMHDRC->uNumDmaChannels;
         uDmaChannel ++)
        {
        printf(" TXFUNCADDR (EP %d)  0ffset 0x%X Value 0x%X\n",uDmaChannel,
                                        USB_MHDRC_TXFUNCADDR_EP(uDmaChannel),
                                        USB_MHDRC_REG_READ8 (pMHDRC,
                                        USB_MHDRC_TXFUNCADDR_EP(uDmaChannel)));

        printf(" TXHUBADDR  (EP %d)  0ffset 0x%X Value 0x%X\n",uDmaChannel,
                                        USB_MHDRC_TXHUBADDR_EP(uDmaChannel),
                                        USB_MHDRC_REG_READ8 (pMHDRC,
                                        USB_MHDRC_TXHUBADDR_EP(uDmaChannel)));
        printf(" TXHUBPORT  (EP %d)  0ffset 0x%X Value 0x%X\n",uDmaChannel,
                                        USB_MHDRC_TXHUBPORT_EP(uDmaChannel),
                                        USB_MHDRC_REG_READ8 (pMHDRC,
                                        USB_MHDRC_TXHUBPORT_EP(uDmaChannel)));

        printf(" RXFUNCADDR (EP %d)  0ffset 0x%X Value 0x%X\n",uDmaChannel,
                                        USB_MHDRC_RXFUNCADDR_EP(uDmaChannel),
                                        USB_MHDRC_REG_READ8 (pMHDRC,
                                        USB_MHDRC_RXFUNCADDR_EP(uDmaChannel)));

        printf(" RXHUBADDR  (EP %d)  0ffset 0x%X Value 0x%X\n",uDmaChannel,
                                        USB_MHDRC_RXHUBADDR_EP(uDmaChannel),
                                        USB_MHDRC_REG_READ8 (pMHDRC,
                                        USB_MHDRC_RXHUBADDR_EP(uDmaChannel)));
        printf(" RXHUBPORT  (EP %d)  0ffset 0x%X Value 0x%X\n",uDmaChannel,
                                        USB_MHDRC_RXHUBPORT_EP(uDmaChannel),
                                        USB_MHDRC_REG_READ8 (pMHDRC,
                                        USB_MHDRC_RXHUBPORT_EP(uDmaChannel)));

        }

    printf(" \nControl Registers for each Channel \n\n");

    /* Ep 0 */

    printf(" CSR0_EP0       (EP %d)  0ffset 0x%X Value 0x%X\n",0,
                                    USB_MHDRC_HOST_CSR0,
                                    USB_MHDRC_REG_READ16 (pMHDRC,
                                    USB_MHDRC_HOST_CSR0));
    printf(" COUNT0_EP0     (EP %d)  0ffset 0x%X Value 0x%X\n",0,
                                    USB_MHDRC_COUNT0,
                                    USB_MHDRC_REG_READ16 (pMHDRC,
                                    USB_MHDRC_COUNT0));
    printf(" TYPE0_EP0      (EP %d)  0ffset 0x%X Value 0x%X\n",0,
                                    USB_MHDRC_HOST_TYPE0,
                                    USB_MHDRC_REG_READ8 (pMHDRC,
                                    USB_MHDRC_HOST_TYPE0));
    printf(" NAKLIMIT0_EP0  (EP %d)  0ffset 0x%X Value 0x%X\n",0,
                                    USB_MHDRC_HOST_NAKLIMIT0,
                                    USB_MHDRC_REG_READ8 (pMHDRC,
                                    USB_MHDRC_HOST_NAKLIMIT0));
    printf(" CONFIGDATA_EP0 (EP %d)  0ffset 0x%X Value 0x%X\n",0,
                                    USB_MHDRC_CONFIGDATA,
                                    USB_MHDRC_REG_READ8 (pMHDRC,
                                    USB_MHDRC_CONFIGDATA));

    for (uDmaChannel = 1;
         uDmaChannel < pMHDRC->uNumDmaChannels;
         uDmaChannel ++)
        {

        printf(" TXMAXP     (EP %d)  0ffset 0x%X Value 0x%X\n",uDmaChannel,
                                        USB_MHDRC_HOST_TXMAXP_EP(uDmaChannel),
                                        USB_MHDRC_REG_READ16 (pMHDRC,
                                        USB_MHDRC_HOST_TXMAXP_EP(uDmaChannel)));

        printf(" TXCSR      (EP %d)  0ffset 0x%X Value 0x%X\n",uDmaChannel,
                                        USB_MHDRC_HOST_TXCSR_EP(uDmaChannel),
                                        USB_MHDRC_REG_READ16 (pMHDRC,
                                        USB_MHDRC_HOST_TXCSR_EP(uDmaChannel)));
        printf(" RXMAXP     (EP %d)  0ffset 0x%X Value 0x%X\n",uDmaChannel,
                                        USB_MHDRC_HOST_RXMAXP_EP(uDmaChannel),
                                        USB_MHDRC_REG_READ16 (pMHDRC,
                                        USB_MHDRC_HOST_RXMAXP_EP(uDmaChannel)));

        printf(" RXCSR      (EP %d)  0ffset 0x%X Value 0x%X\n",uDmaChannel,
                                        USB_MHDRC_HOST_RXCSR_EP(uDmaChannel),
                                        USB_MHDRC_REG_READ16 (pMHDRC,
                                        USB_MHDRC_HOST_RXCSR_EP(uDmaChannel)));

        printf(" RXCOUNT    (EP %d)  0ffset 0x%X Value 0x%X\n",uDmaChannel,
                                        USB_MHDRC_HOST_RXCOUNT_EP(uDmaChannel),
                                        USB_MHDRC_REG_READ16 (pMHDRC,
                                        USB_MHDRC_HOST_RXCOUNT_EP(uDmaChannel)));
        printf(" TXTYPE     (EP %d)  0ffset 0x%X Value 0x%X\n",uDmaChannel,
                                        USB_MHDRC_HOST_TXTYPE_EP(uDmaChannel),
                                        USB_MHDRC_REG_READ8 (pMHDRC,
                                        USB_MHDRC_HOST_TXTYPE_EP(uDmaChannel)));
        printf(" TXINTERVAL (EP %d)  0ffset 0x%X Value 0x%X\n",uDmaChannel,
                                        USB_MHDRC_HOST_TXINTERVAL_EP(uDmaChannel),
                                        USB_MHDRC_REG_READ8 (pMHDRC,
                                        USB_MHDRC_HOST_TXINTERVAL_EP(uDmaChannel)));

        printf(" RXTYPE     (EP %d)  0ffset 0x%X Value 0x%X\n",uDmaChannel,
                                        USB_MHDRC_HOST_RXTYPE_EP(uDmaChannel),
                                        USB_MHDRC_REG_READ8 (pMHDRC,
                                        USB_MHDRC_HOST_TXTYPE_EP(uDmaChannel)));
        printf(" RXINTERVAL (EP %d)  0ffset 0x%X Value 0x%X\n",uDmaChannel,
                                        USB_MHDRC_HOST_RXINTERVAL_EP(uDmaChannel),
                                        USB_MHDRC_REG_READ8 (pMHDRC,
                                        USB_MHDRC_HOST_TXINTERVAL_EP(uDmaChannel)));
        }


    if (pMHDRC->uPlatformType == USB_MHDRC_PLATFORM_OMAP35xx)
        {
        printf(" \nDMA Registers \n\n");

        for (uDmaChannel = 0;
             uDmaChannel < USB_MHDRC_INVENTRA_DMA_CHANNEL_NUM;
             uDmaChannel ++)
            {
            printf(" DMA_CTNL_CH  (%d)   0ffset 0x%X Value 0x%X\n",uDmaChannel + 1,
                                            USB_MHDRC_HS_DMA_CTNL_CH(uDmaChannel),
                                            USB_MHDRC_REG_READ32 (pMHDRC,
                                            USB_MHDRC_HS_DMA_CTNL_CH(uDmaChannel)));

            printf(" DMA_ADDR_CH  (%d)   0ffset 0x%X Value 0x%X\n",uDmaChannel + 1,
                                            USB_MHDRC_HS_DMA_ADDR_CH(uDmaChannel),
                                            USB_MHDRC_REG_READ32 (pMHDRC,
                                            USB_MHDRC_HS_DMA_ADDR_CH(uDmaChannel)));
            printf(" DMA_COUNT_CH (%d)  0ffset 0x%X Value 0x%X\n",uDmaChannel + 1,
                                            USB_MHDRC_HS_DMA_COUNT_CH(uDmaChannel),
                                            USB_MHDRC_REG_READ32 (pMHDRC,
                                            USB_MHDRC_HS_DMA_COUNT_CH(uDmaChannel)));
            }
        printf(" \nOTG Registers \n\n");
        printf(" OTG_REVISION   0ffset 0x%X Value 0x%X\n",
                                        USB_MHDRC_OTG_REVISION,
                                        USB_MHDRC_REG_READ32 (pMHDRC,
                                        USB_MHDRC_OTG_REVISION));
        printf(" OTG_SYSCONFIG  0ffset 0x%X Value 0x%X\n",
                                        USB_MHDRC_OTG_SYSCONFIG,
                                        USB_MHDRC_REG_READ32 (pMHDRC,
                                        USB_MHDRC_OTG_SYSCONFIG));
        printf(" OTG_SYSSTATUS  0ffset 0x%X Value 0x%X\n",
                                    USB_MHDRC_OTG_SYSSTATUS,
                                    USB_MHDRC_REG_READ32 (pMHDRC,
                                    USB_MHDRC_OTG_SYSSTATUS));
        printf(" OTG_INTERFSEL  0ffset 0x%X Value 0x%X\n",
                                    USB_MHDRC_OTG_INTERFSEL,
                                    USB_MHDRC_REG_READ32 (pMHDRC,
                                    USB_MHDRC_OTG_INTERFSEL));
        printf(" OTG_FORCESTDBY 0ffset 0x%X Value 0x%X\n",
                                    USB_MHDRC_OTG_FORCESTDBY,
                                    USB_MHDRC_REG_READ32 (pMHDRC,
                                    USB_MHDRC_OTG_FORCESTDBY));

        }

    return;
    }

/***************************************************************************
*
* usbMhdrcHcdDataShow - show MHCD data structure
*
* This routine displays the request lists current values.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcHcdDataShow
    (
    UINT8 uShowLevel
    )
    {
    pUSB_MHCD_DATA      pHCDData = NULL;
    int                 i = 0;
    int                 j = 0;
    pUSB_MUSBMHDRC      pMHDRC;

    printf("usbMhdrcHcdDataShow (UINT8 uShowLevel); Usage:\n \n");

    printf("uShowLevel 0 : Show all the MHCD Data Struchture \n");

    printf("uShowLevel 1 : Show all the MHCD Data Struchture \n");
    printf("             : Show all the Transaction Pipes on MHCD \n\n");

    printf("uShowLevel 2 : Show all the MHCD Data Struchture \n");
    printf("             : Show all the Transaction Pipes on MHCD \n");
    printf("             : Show all the Request Info in the Pipes List\n");

    for (i = 0; i < USB_MAX_MHCI_COUNT; i ++)
        {
        pHCDData = gpMHCDData[i];

        if (!pHCDData || !pHCDData->pMHDRC)
            {
            continue;
            }

        /* Get the common hardware data structure */

        pMHDRC = pHCDData->pMHDRC;

        printf("\n");
        printf("pMHDRC[%d] %p\n",i,pMHDRC);

        printf("pHCDData[%d]->uBusIndex 0x%X\n",i,pHCDData->uBusIndex);

        printf("pHCDData[%d]->uFreeEpChannelBitMap 0x%X\n",
               i,(UINT32)pHCDData->uFreeEpChannelBitMap);

        printf("pMHDRC[%d]->uTxInterruptStatus 0x%X\n",
               i,(UINT32)pMHDRC->uTxIrqStatus);
        printf("pMHDRC[%d]->uRxInterruptStatus 0x%X\n",
               i,(UINT32)pMHDRC->uRxIrqStatus);
        printf("pMHDRC[%d]->uUsbInterruptStatus 0x%X\n",
               i,(UINT32)pMHDRC->uUsbIrqStatus);

        printf("pMHDRC[%d]->uHcdTxIrqStatus 0x%X\n",
               i,(UINT32)pHCDData->uHcdTxIrqStatus);
        printf("pMHDRC[%d]->uHcdRxIrqStatus 0x%X\n",
               i,(UINT32)pHCDData->uHcdRxIrqStatus);
        printf("pMHDRC[%d]->uHcdUsbIrqStatus 0x%X\n",
               i,(UINT32)pHCDData->uHcdUsbIrqStatus);

        for (j = 0; j < USB_MHDRC_ENDPOINT_MAX; j++)
            {
            printf("pHCDData[%d]->pRequestInChannel[%d] %p\n",
                   i,j,pHCDData->pRequestInChannel[j]);
            }

        for (j = 0; j < USB_MHDRC_ENDPOINT_MAX; j++)
            {
            printf("pMHDRC[%d]->uTxFifoSize[%d] 0x%X\n",
                   i,j,(UINT32)pMHDRC->uTxFifoSize[j]);
            printf("pMHDRC[%d]->uRxFifoSize[%d] 0x%X\n",
                   i,j,(UINT32)pMHDRC->uRxFifoSize[j]);
            }

        printf("pMHDRC[%d]->uFrameNumberRecord 0x%X\n",
               i,pMHDRC->uFrameNumberRecord);
        printf("pMHDRC[%d]->uFrameNumChangeFlag 0x%X\n",
               i,pMHDRC->uFrameNumChangeFlag);
        printf("pMHDRC[%d]->regBase 0x%lX\n",i,pMHDRC->uRegBase);
        printf("pMHDRC[%d]->uRootHubNumPorts 0x%X\n",
               i,pMHDRC->uRootHubNumPorts);

        printf("pMHDRC[%d]->uNumDmaChannels 0x%X\n",
               i,pMHDRC->uNumDmaChannels);

        printf("pMHDRC[%d]->uUseExternalPower 0x%X\n",
               i,pMHDRC->uUseExtPower);

        printf("pMHDRC[%d]->uPlatformType 0x%X\n",
               i,pMHDRC->uPlatformType);
        printf("pMHDRC[%d]->uDMAType 0x%X\n",
               i,pMHDRC->uDmaType);

        printf("pMHDRC[%d]->bDmaEnabled 0x%X\n",
               i,pMHDRC->bDmaEnabled);
        printf("pMHDRC[%d]->uVbusError 0x%X\n",
               i,pMHDRC->uVbusError);

        printf("pMHDRC[%d]->pDev %p\n",i,pMHDRC->pDev);

        printf("pHCDData[%d]->pHcdSynchMutex %p\n",i,pHCDData->pHcdSynchMutex);
        printf("pHCDData[%d]->pDefaultPipe %p\n",i,pHCDData->pDefaultPipe);


        printf("pMHDRC[%d]->isrMagic 0x%X\n",i,pHCDData->pMHDRC->isrMagic);
        printf("pHCDData[%d]->isrEvent %p\n",i,pHCDData->isrEvent);

        printf("pHCDData[%d]->IntHandlerThreadID 0x%lX\n",
               i,pHCDData->IntHandlerThreadID);
        printf("pHCDData[%d]->TransferThreadMsgID %p\n",
               i,pHCDData->TransferThreadMsgID);
        printf("pHCDData[%d]->TransferThreadID 0x%lX\n",
               i,pHCDData->TransferThreadID);

        printf("pMHDRC[%d] Root Hub data\n", i);

        printf("pHCDData[%d]->RHData Address %p\n",i,&(pHCDData->RHData));
        printf("pHCDData->pPortStatus 0x%X\n",pHCDData->RHData.pPortStatus[0]);
        for (j = 0; j < USB_MHDRC_RH_DOWNSTREAM_PORT; j++)
            {
            printf("  pRHData->pRootHubPortStatus[%d].uRHPortStatus 0x%X\n",
                      j,pHCDData->RHData.pRootHubPortStatus[j].uRHPortStatus);
            printf("  pRHData->pRootHubPortStatus[%d].uRHPortChangeStatus 0x%X\n",
            j,pHCDData->RHData.pRootHubPortStatus[j].uRHPortChangeStatus);
            printf("  pRHData->HubStatus[%d] 0x%X\n",j,pHCDData->RHData.HubStatus[j]);
            }
        for (j = 0; j < USB_HUB_STATUS_SIZE; j++)
            {
            printf("  pRHData->HubStatus[%d] 0x%X\n",j,pHCDData->RHData.HubStatus[j]);
            }

        printf("  pRHData->uNumDownstreamPorts 0x%X\n",
                  pHCDData->RHData.uNumDownstreamPorts);
        printf("  pRHData->pHubInterruptData 0x%X\n",
                  pHCDData->RHData.pHubInterruptData[0]);

        printf("  pRHData->uSizeInterruptData 0x%X\n",
                  pHCDData->RHData.uSizeInterruptData);
        printf("  pRHData->pControlPipe %p\n",pHCDData->RHData.pControlPipe);
        printf("  pRHData->pInterruptPipe %p\n",pHCDData->RHData.pInterruptPipe);
        printf("  pRHData->pPendingInterruptRequest %p\n",
                  pHCDData->RHData.pPendingInterruptRequest);
        printf("  pRHData->bRemoteWakeupEnabled 0x%X\n",
                  pHCDData->RHData.bRemoteWakeupEnabled);
        printf("  pRHData->bInterruptEndpointHalted 0x%X\n",
                  pHCDData->RHData.bInterruptEndpointHalted);
        printf("  pRHData->uDeviceAddress 0x%X\n",pHCDData->RHData.uDeviceAddress);
        printf("  pRHData->uConfigValue 0x%X\n",pHCDData->RHData.uConfigValue);

        printf("pMHDRC[%d] Platform data\n", i);

        printf("pMHDRC[%d]->PlatformData Address %p\n",
                i,&(pMHDRC->PlatformData));

        printf("pMHDRC[%d]->pRegAccessHandle %p\n",
                i,pMHDRC->pRegAccessHandle);
        printf("pMHDRC[%d]->pUsbHwInit %p\n",i,pMHDRC->pUsbHwInit);
        printf("pMHDRC[%d]->pDescSwap %p\n",i,pMHDRC->pDescSwap);
        printf("pMHDRC[%d]->pUsbSwap %p\n",i,pMHDRC->pUsbSwap);
        printf("pMHDRC[%d]->pRegSwap %p\n",i,pMHDRC->pRegSwap);

        printf("pMHDRC[%d]->pBusToCpu %p\n",i,pMHDRC->pBusToCpu);
        printf("pMHDRC[%d]->pCpuToBus %p\n",i,pMHDRC->pCpuToBus);
        printf("pMHDRC[%d]->pPostResetHook %p\n",i,pMHDRC->pPostResetHook);

        printf("pMHDRC[%d]->PeriodicPipeList %p\n",
               i,&pHCDData->PeriodicPipeList);

        if (uShowLevel)
            {
            usbMhdrcHcdPipetListShow (&(pHCDData->PeriodicPipeList), uShowLevel);
            }

        printf("pHCDData[%d]->NonPeriodicPipeList %p\n",
               i, &pHCDData->NonPeriodicPipeList);

        if (uShowLevel)
            {
            usbMhdrcHcdPipetListShow (&(pHCDData->NonPeriodicPipeList), uShowLevel);
            }
        }

    return;
    }


#endif /* USB_MHCD_SHOW */
