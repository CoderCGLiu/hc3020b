/* usbTgtMscLib.c - USB Target Mass Storage Class library module */

/*
 * Copyright (c) 2010-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification History
--------------------
01o,06may13,s_z  Remove compiler warning (WIND00356717)
01n,04jan13,s_z  Remove compiler warning (WIND00390357)
01m,25oct12,s_z  MHDRC TCD pass CV testing (WIND00385197)
01l,17oct12,s_z  Fix short packet receive issue (WIND00374594)
01k,18may12,s_z  Add support for USB 3.0 target (WIND00326012)
01j,12aug11,m_y  modify read/write process (WIND00288793)
01i,23may11,m_y  relax the limit of the inquiry command (WIND00277225)
01h,11apr11,m_y  modify read/write routine to support bigger transfer length
01g,08apr11,ghs  Fix code coverity issue(WIND00264893)
01f,25mar11,m_y  release buffer when callback coming
01e,17mar11,m_y  remove usb_tgt_msc_lunf structure
01d,17mar11,m_y  modify code based on the code review
01c,09mar11,s_z  Code clean up
01b,03mar11,m_y  Code clean up and modify to fix WIND00254432
01a,28dec10,x_f  written
*/

/*
DESCRIPTION

This module defines the USB_ERP callback routines directly used by the
USB 2.0 mass storage driver. These callback routines invoke the
routines defined in the file usbTgtMscCmd.c.

INCLUDES: usbTgtMscLib.h, usbTgtMscUtil.h, usbTgtMscCmd.h
*/

/* includes */

#include <usbTgtMscLib.h>
#include <usbTgtMscUtil.h>
#include <usbTgtMscCmd.h>

/* defines */

/* globals */

/* forward declarations */

/*******************************************************************************
*
* usbTgtMscCswSend - send the csw with user set status 
*
* This routine sends csw with user set status.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbTgtMscCswSend
    (
    pUSB_TGT_MSC_DEV pUsbTgtMscDev,
    UINT8            uStatus
    )
    {
    pUSB_MSC_CSW     pCsw;

    if ((pUsbTgtMscDev == NULL) || (uStatus > USB_CSW_PHASE_ERROR))
        {
        USBTGT_MSC_ERR("Invalid Parameter\n", 1, 2, 3, 4, 5, 6);
        return;
        }
 
    pCsw = pUsbTgtMscDev->pCSW;
    
    /* 
     * set CSW Status
     * USB_CSW_STATUS_PASS(0x00)
     * USB_CSW_STATUS_FAIL(0x01)
     * USB_CSW_PHASE_ERROR(0x02)
     */
    
    pCsw->bCSWStatus = uStatus;
    
    /* init CSW ERP on Bulk-in pipe and submit */
    
    if (usbTgtMscBulkErpInit(pUsbTgtMscDev, 
                            (UINT8 *)pCsw, 
                            sizeof(USB_MSC_CSW),
                            USB_PID_IN, 
                            usbTgtMscBulkInErpCallbackCSW) != OK)
        {
        USBTGT_MSC_ERR("usbTgtMscCswSend - "
                       "usbTgtMscBulkErpInit error\n", 
                       1, 2, 3, 4, 5, 6);
        }
    return;
    }
    

/*******************************************************************************
*
* usbTgtMscBulkOutErpCallbackCBW - process the CBW on bulk-out pipe
*
* This routine processes the the CBW (Command Block Wrapper) which is received
* on the bulk out pipe.
*
* RETURNS: N/A
*
* ERRNO:
*  none
*/

void usbTgtMscBulkOutErpCallbackCBW
    (
    pVOID    erp        /* USB_ERP endpoint request packet */
    )
    {
    STATUS           rbcRetVal  = ERROR;   /* return value */
    BOOL             rbcDataIn  = FALSE;   /* flag for data in */
    BOOL             rbcDataOut = FALSE;   /* flag for data out */
    UINT8 *          pData      = NULL;    /* data buffer */
    UINT8 **         ppData     = &pData;
    UINT32           dSize      = 0;       /* size of data */
    BOOL             validCBW   = FALSE;   /* flag for CBW */
    USB_MSC_CBW *    pCbw       = NULL;    /* Command Block Wrapper */
    USB_MSC_CSW *    pCsw       = NULL;    /* Command Block Status */
    UINT32           cbwDataXferLgth = 0;  /* transfer length */
    UINT8            cbwDataDir;           /* direction */
    ERP_CALLBACK     erpCallback   = NULL; /* ERP_CALLBACK pointer */
    pUSB_ERP         pUsbErp       = NULL;
    pUSB_TGT_MSC_LUN pUsbTgtMscLun = NULL;
    pUSB_TGT_MSC_DEV pUsbTgtMscDev = NULL;

    pUsbErp = (pUSB_ERP)erp;

    if ((pUsbErp == NULL) || (pUsbErp->userPtr == NULL))
        {
        USBTGT_MSC_ERR("usbTgtMscBulkOutErpCallbackCBW - Invalid erp\n",
                       1, 2, 3, 4, 5, 6);
        return;
        }

    pUsbTgtMscDev =  (pUSB_TGT_MSC_DEV)pUsbErp->userPtr;
    pUsbTgtMscDev->bulkOutErpState = USB_TGT_MSC_ERP_IDLE;

    /* if we had a reset condition, then we are no longer configured */

    if (pUsbTgtMscDev->uCurConfig == 0)
        {
        USBTGT_MSC_ERR("usbTgtMscBulkOutErpCallbackCBW - uCurConfig is 0\n",
                       1, 2, 3, 4, 5, 6);
        return;
        }

    /* verify whether the data is good */

    if ((pUsbErp->result != S_usbTgtTcdLib_ERP_SUCCESS) &&
        (pUsbErp->result != S_usbTgtTcdLib_ERP_RESTART) &&
        (pUsbErp->result != OK))
        {
        USBTGT_MSC_ERR("usbTgtMscBulkOutErpCallbackCBW - pUsbErp->result %x\n",
                       pUsbErp->result, 2, 3, 4, 5, 6);
        return;
        }

    if (pUsbErp->bfrList[0].actLen == sizeof(USB_MSC_CBW))
        {
        pCbw = (USB_MSC_CBW *)(pUsbErp->bfrList[0].pBfr);

        if (pCbw->dCBWSignature == OS_UINT32_CPU_TO_LE(USB_MSC_CBW_SIGNATURE))
            {
            if (pCbw->bCBWLUN >= pUsbTgtMscDev->nLuns)
                {
                USBTGT_MSC_ERR("pCbw->bCBWLUN is %d\n",
                               pCbw->bCBWLUN, 2, 3, 4, 5, 6);
                return;
                }
            if ((pCbw->bCBWCBLength >= 1) &&
                (pCbw->bCBWCBLength <= MAX_MSC_CMD_LEN))
                validCBW = TRUE;
            }
    else
        {
            /* Wait for recover */
            
            pUsbTgtMscDev->uCmdStatus = USBTGT_MSC_CBW_INVALID; 

            goto EXIT_ERROR;
            }
        }
    else
        {
        /* Wait for recover */
        
        pUsbTgtMscDev->uCmdStatus = USBTGT_MSC_CBW_INVALID; 

        goto EXIT_ERROR;
        }

    pUsbTgtMscLun = usbTgtMscLunGetByIndex(pUsbTgtMscDev, pCbw->bCBWLUN);
    if (pUsbTgtMscLun == NULL)
        {
        USBTGT_MSC_ERR("Can't find the LUN structure of %d \n",
                       pCbw->bCBWLUN, 2, 3, 4, 5, 6);
        return;
        }

    /* Assign the currentLun */

    pUsbTgtMscDev->pCurrentLun = pUsbTgtMscLun;
    pUsbTgtMscDev->uActLen = 0;

    if (validCBW == TRUE)
        {
        /*
         * init CSW, set status to "command good",
         * set CSW tag to CBW tag
         */

        pCsw = pUsbTgtMscDev->pCSW;

        memset(pCsw, 0, sizeof(USB_MSC_CSW));
        pCsw->dCSWSignature = OS_UINT32_CPU_TO_LE (USB_MSC_CSW_SIGNATURE);
        pCsw->bCSWStatus = USB_CSW_STATUS_PASS;
        pCsw->dCSWTag = pCbw->dCBWTag;
        pCsw->dCSWDataResidue = OS_UINT32_CPU_TO_LE(0x0);

        /* Record the command */

        pUsbTgtMscDev->cmdLen = (UINT32)pCbw->bCBWCBLength;
        memcpy(pUsbTgtMscDev->cmd, pCbw->CBWCB, pUsbTgtMscDev->cmdLen);

        /* get data transfer lgth, and correct endian */

        cbwDataXferLgth = OS_UINT32_LE_TO_CPU(pCbw->dCBWDataTransferLength);

        /* set data direction flag from CBW */

        if (((pCbw->bmCBWFlags & USB_CBW_DIR_IN) >> 7) == 0x1)
            cbwDataDir = USB_PID_IN;
        else
            cbwDataDir = USB_PID_OUT;

        switch(pUsbTgtMscDev->cmd[0])
            {
            /* RBC commands */

            case RBC_CMD_FORMAT:
                if (pCbw->bCBWCBLength == RBC_CMD_SIZE6)
                    {
                    rbcRetVal = usbTgtRbcFormat(pUsbTgtMscLun);
                    erpCallback = usbTgtMscBulkInErpCallbackCSW;
                    }
                else
                    {
                    USBTGT_MSC_ERR("RBC_CMD_FORMAT EXIT_ERROR\n",
                        1, 2, 3, 4, 5, 6);
                    goto EXIT_ERROR;
                    }
                break;

            case RBC_CMD_READ10:
                if (pCbw->bCBWCBLength == RBC_CMD_SIZE10)
                    {
                    if (OK != usbTgtRbcRead(pUsbTgtMscLun))
                        usbTgtMscCswSend(pUsbTgtMscDev, USB_CSW_STATUS_FAIL);
                    return;
                    }
                else
                    {
                    USBTGT_MSC_ERR("RBC_CMD_READ10 EXIT_ERROR\n",
                        1, 2, 3, 4, 5, 6);
                    goto EXIT_ERROR;
                    }
                break;

            case RBC_CMD_READ12:
                if (pCbw->bCBWCBLength == RBC_CMD_SIZE12)
                    {
                    if (OK != usbTgtRbcRead(pUsbTgtMscLun))
                        usbTgtMscCswSend(pUsbTgtMscDev, USB_CSW_STATUS_FAIL);
                    return;
                    }
                else
                    {
                    USBTGT_MSC_ERR("RBC_CMD_READ12 EXIT_ERROR\n",
                        1, 2, 3, 4, 5, 6);
                    goto EXIT_ERROR;
                    }
                break;

            case RBC_CMD_READCAPACITY:
                if (pCbw->bCBWCBLength == RBC_CMD_SIZE10)
                    {
                    rbcRetVal = usbTgtRbcCapacityRead(pUsbTgtMscLun,
                                                 ppData, &dSize);
                    erpCallback = usbTgtMscBulkInErpCallbackData;
                    rbcDataIn = TRUE;
                    }
                else
                    {
                    USBTGT_MSC_ERR("RBC_CMD_READCAPACITY EXIT_ERROR\n",
                        1, 2, 3, 4, 5, 6);
                    goto EXIT_ERROR;
                    }
                break;

            case RBC_CMD_STARTSTOPUNIT:
                if (pCbw->bCBWCBLength == RBC_CMD_SIZE6)
                    {
                    rbcRetVal = usbTgtRbcStartStop(pUsbTgtMscLun);
                    erpCallback = usbTgtMscBulkInErpCallbackCSW;
                    }
                else
                    {
                    USBTGT_MSC_ERR("RBC_CMD_STARTSTOPUNIT EXIT_ERROR\n",
                        1, 2, 3, 4, 5, 6);
                    goto EXIT_ERROR;
                    }
                break;

            case RBC_CMD_SYNCCACHE:
                if (pCbw->bCBWCBLength == RBC_CMD_SIZE10)
                    {
                    rbcRetVal = usbTgtRbcCacheSync(pUsbTgtMscLun);
                    erpCallback = usbTgtMscBulkInErpCallbackCSW;
                    }
                else
                    {
                    USBTGT_MSC_ERR("RBC_CMD_SYNCCACHE EXIT_ERROR\n",
                        1, 2, 3, 4, 5, 6);
                    goto EXIT_ERROR;
                    }
                break;

            case RBC_CMD_VERIFY10:
                if (pCbw->bCBWCBLength == RBC_CMD_SIZE10)
                    {
                    rbcRetVal = usbTgtRbcVerify(pUsbTgtMscLun);
                    erpCallback = usbTgtMscBulkInErpCallbackCSW;
                    }
                else
                    {
                    USBTGT_MSC_ERR("RBC_CMD_VERIFY10 EXIT_ERROR\n",
                        1, 2, 3, 4, 5, 6);
                    goto EXIT_ERROR;
                    }
                break;

            case RBC_CMD_WRITE10:
                if (pCbw->bCBWCBLength == RBC_CMD_SIZE10)
                    {
                    if (OK != usbTgtRbcWrite(pUsbTgtMscLun))
                        usbTgtMscCswSend(pUsbTgtMscDev, USB_CSW_STATUS_FAIL);
                    return;
                    }
                else
                    {
                    USBTGT_MSC_ERR("RBC_CMD_WRITE10 EXIT_ERROR\n",
                        1, 2, 3, 4, 5, 6);
                    goto EXIT_ERROR;
                    }
                break;

            case RBC_CMD_WRITE12:
                if (pCbw->bCBWCBLength == RBC_CMD_SIZE12)
                    {
                    if (OK != usbTgtRbcWrite(pUsbTgtMscLun))
                        usbTgtMscCswSend(pUsbTgtMscDev, USB_CSW_STATUS_FAIL);
                    return;
                    }
                else
                    {
                    USBTGT_MSC_ERR("RBC_CMD_WRITE12 EXIT_ERROR\n",
                        1, 2, 3, 4, 5, 6);
                    goto EXIT_ERROR;
                    }
                break;

            /* SPC-2 commands */

            case RBC_CMD_INQUIRY:
                /* Remove this if judgement to suit with windows 7 */

                /* if (pCbw->bCBWCBLength == RBC_CMD_SIZE6)*/
                    {
                    rbcRetVal = usbTgtRbcInquiry(pUsbTgtMscLun, ppData, &dSize);
                    erpCallback = usbTgtMscBulkInErpCallbackData;
                    rbcDataIn = TRUE;
                    }
                /*
                else
                    {
                    USBTGT_MSC_ERR("RBC_CMD_INQUIRY EXIT_ERROR\n",
                        1, 2, 3, 4, 5, 6);
                    goto EXIT_ERROR;
                    }
                */
                break;

            case RBC_CMD_MODESELECT6:
                if (pCbw->bCBWCBLength == RBC_CMD_SIZE6)
                    {
                    rbcRetVal = usbTgtRbcModeSelect(pUsbTgtMscLun,
                                               ppData, &dSize);
                    erpCallback = usbTgtMscBulkOutErpCallbackData;
                    rbcDataOut = TRUE;
                    }
                else
                    {
                    USBTGT_MSC_ERR("RBC_CMD_MODESELECT6 EXIT_ERROR\n",
                        1, 2, 3, 4, 5, 6);
                    goto EXIT_ERROR;
                    }
                break;

            case RBC_CMD_MODESENSE6:
                if (pCbw->bCBWCBLength == RBC_CMD_SIZE6)
                    {
                    rbcRetVal = usbTgtRbcModeSense(pUsbTgtMscLun,
                                               ppData, &dSize);
                    erpCallback = usbTgtMscBulkInErpCallbackData;
                    rbcDataIn = TRUE;
                    }
                else
                    {
                    USBTGT_MSC_ERR("RBC_CMD_MODESENSE6 EXIT_ERROR\n",
                        1, 2, 3, 4, 5, 6);
                    goto EXIT_ERROR;
                    }
                break;

            case RBC_CMD_PERSISTANTRESERVIN:
                if (pCbw->bCBWCBLength == RBC_CMD_SIZE10)
                    {
                    rbcRetVal = usbTgtRbcPersistentReserveIn(pUsbTgtMscLun,
                                               ppData, &dSize);
                    erpCallback = usbTgtMscBulkInErpCallbackData;
                    rbcDataIn = TRUE;
                    }
                else
                    {
                    USBTGT_MSC_ERR("RBC_CMD_PERSISTANTRESERVIN EXIT_ERROR\n",
                        1, 2, 3, 4, 5, 6);
                    goto EXIT_ERROR;
                    }
                break;

            case RBC_CMD_PERSISTANTRESERVOUT:
                if (pCbw->bCBWCBLength == RBC_CMD_SIZE10)
                    {
                    rbcRetVal = usbTgtRbcPersistentReserveOut(pUsbTgtMscLun,
                                               ppData, &dSize);
                    erpCallback = usbTgtMscBulkOutErpCallbackData;
                    rbcDataOut = TRUE;
                    }
                else
                    {
                    USBTGT_MSC_ERR("RBC_CMD_PERSISTANTRESERVOUT EXIT_ERROR\n",
                        1, 2, 3, 4, 5, 6);
                    goto EXIT_ERROR;
                    }
                break;

            case RBC_CMD_PRVENTALLOWMEDIUMREMOVAL:
               if (pCbw->bCBWCBLength == RBC_CMD_SIZE6)
                    {
                    rbcRetVal = usbTgtRbcPreventAllowRemoval(pUsbTgtMscLun);
                    erpCallback = usbTgtMscBulkInErpCallbackCSW;
                    }
                else
                    {
                    USBTGT_MSC_ERR("RBC_CMD_PRVENTALLOWMEDIUMREMOVAL EXIT_ERROR\n",
                        1, 2, 3, 4, 5, 6);
                    goto EXIT_ERROR;
                    }
                break;

            case RBC_CMD_RELEASE6:
                if (pCbw->bCBWCBLength == RBC_CMD_SIZE6)
                    {
                    rbcRetVal = usbTgtRbcRelease(pUsbTgtMscLun);
                    erpCallback = usbTgtMscBulkInErpCallbackCSW;
                    }
                else
                    {
                    USBTGT_MSC_ERR("RBC_CMD_RELEASE6 EXIT_ERROR\n",
                        1, 2, 3, 4, 5, 6);
                    goto EXIT_ERROR;
                    }
                break;

            case RBC_CMD_REQUESTSENSE:
                    {
                    rbcRetVal = usbTgtRbcRequestSense(pUsbTgtMscLun,
                                                       ppData, &dSize);
                    erpCallback = usbTgtMscBulkInErpCallbackData;
                    rbcDataIn = TRUE;
                    }
                break;

            case RBC_CMD_RESERVE6:
                if (pCbw->bCBWCBLength == RBC_CMD_SIZE6)
                    {
                    rbcRetVal = usbTgtRbcReserve(pUsbTgtMscLun);
                    erpCallback = usbTgtMscBulkInErpCallbackCSW;
                    }
                else
                    {
                    USBTGT_MSC_ERR("RBC_CMD_RESERVE6 EXIT_ERROR\n",
                        1, 2, 3, 4, 5, 6);
                    goto EXIT_ERROR;
                    }
                break;

            case RBC_CMD_TESTUNITREADY:
                if (pCbw->bCBWCBLength == RBC_CMD_SIZE6)
                    {
                    rbcRetVal = usbTgtRbcTestUnitReady(pUsbTgtMscLun);
                    erpCallback = usbTgtMscBulkInErpCallbackCSW;
                    }
                else
                    {
                    USBTGT_MSC_ERR("RBC_CMD_TESTUNITREADY EXIT_ERROR\n",
                        1, 2, 3, 4, 5, 6);
                    goto EXIT_ERROR;
                    }
                break;

            case RBC_CMD_WRITEBUFFER:
                if (pCbw->bCBWCBLength == RBC_CMD_SIZE10)
                    {
                    rbcRetVal = usbTgtRbcBufferWrite(pUsbTgtMscLun,
                                                      ppData, &dSize);
                    erpCallback = usbTgtMscBulkOutErpCallbackData;
                    rbcDataOut = TRUE;
                    }
                else
                    {
                    USBTGT_MSC_ERR("RBC_CMD_WRITEBUFFER EXIT_ERROR\n",
                        1, 2, 3, 4, 5, 6);
                    goto EXIT_ERROR;
                    }
                break;

            case RBC_CMD_VENDORSPECIFIC_23:
                if (pCbw->bCBWCBLength == RBC_CMD_SIZE10)
                    {
                    rbcRetVal = usbTgtRbcVendorSpecific(pUsbTgtMscLun,
                                                   ppData, &dSize);
                    erpCallback = usbTgtMscBulkInErpCallbackData;
                    rbcDataIn = TRUE;
                    }
                else
                    {
                    USBTGT_MSC_ERR("RBC_CMD_VENDORSPECIFIC_23 EXIT_ERROR\n",
                        1, 2, 3, 4, 5, 6);
                    goto EXIT_ERROR;
                    }
                break;

            case RBC_CMD_MODESENSE10 :
                if (pCbw->bCBWCBLength == RBC_CMD_SIZE10)
                    {
                    rbcRetVal = usbTgtRbcModeSense10(pUsbTgtMscLun,
                                               ppData, &dSize);
                    erpCallback = usbTgtMscBulkInErpCallbackData;
                    rbcDataIn = TRUE;
                    }
                else
                    {
                    USBTGT_MSC_ERR("RBC_CMD_MODESENSE10 EXIT_ERROR\n",
                                   1, 2, 3, 4, 5, 6);
                    goto EXIT_ERROR;
                    }
                break;

            case RBC_CMD_MODESELECT10:
                if (pCbw->bCBWCBLength == RBC_CMD_SIZE10)
                    {
                    rbcRetVal = usbTgtRbcModeSelect10(pUsbTgtMscLun,
                                                      ppData,
                                                      &dSize);
                    erpCallback = usbTgtMscBulkOutErpCallbackData;
                    rbcDataOut = TRUE;
                    }
                else
                    {
                    USBTGT_MSC_ERR("RBC_CMD_MODESELECT10 EXIT_ERROR\n",
                                   1, 2, 3, 4, 5, 6);
                    goto EXIT_ERROR;
                    }
                break;

            default:
                USBTGT_MSC_ERR("not valid RBC command EXIT_ERROR\n",
                    1, 2, 3, 4, 5, 6);
                goto EXIT_ERROR;
            }

        if (rbcRetVal == OK)
            {
            if (rbcDataIn == TRUE)
                {
                if (cbwDataXferLgth > 0)
                    {
                    if (cbwDataDir == USB_PID_IN)
                        {
                        if (cbwDataXferLgth == dSize)
                            {
                            pCsw->dCSWDataResidue = OS_UINT32_CPU_TO_LE(0x0);

                            /*
                             * init Data-In ERP on Bulk-In pipe and submit
                             * to TCD
                             */

                            if (usbTgtMscBulkErpInit(pUsbTgtMscDev, pData, dSize,
                                USB_PID_IN, erpCallback) != OK)
                                {
                                USBTGT_MSC_ERR("usbTgtMscBulkOutErpCallbackCBW - "
                                               "usbTgtMscBulkErpInit error\n",
                                               1, 2, 3, 4, 5, 6);
                                goto EXIT_ERROR;
                                }
                            }
                        else if (cbwDataXferLgth > dSize)
                            {
                            pCsw->dCSWDataResidue =
                                OS_UINT32_CPU_TO_LE((cbwDataXferLgth - dSize));

                            /* set stall flag on Bulk-In Endpoint */

                            pUsbTgtMscDev->bulkInPipeState = USB_TGT_MSC_PIPE_STALL;

                            /*
                             * init Data In ERP on Bulk-In pipe and submit
                             * to TCD
                             */

                            if (usbTgtMscBulkErpInit(pUsbTgtMscDev, pData, dSize,
                                USB_PID_IN, erpCallback) != OK)
                                {
                                USBTGT_MSC_ERR("usbTgtMscBulkOutErpCallbackCBW - "
                                               "usbTgtMscBulkErpInit error\n",
                                               1, 2, 3, 4, 5, 6);
                                goto EXIT_ERROR;
                                }
                            }
                        else
                            {
                            /* set CSW Status 0x2 (phase error) */

                            pCsw->bCSWStatus = USB_CSW_PHASE_ERROR;
                            pData = (UINT8 *)pCsw;
                            dSize = sizeof(USB_MSC_CSW);
                            erpCallback = usbTgtMscBulkInErpCallbackCSW;

                            /* init CSW ERP on Bulk-in pipe and submit */

                            if (usbTgtMscBulkErpInit(pUsbTgtMscDev, pData, dSize,
                                USB_PID_IN, erpCallback) != OK)
                                {
                                USBTGT_MSC_ERR("usbTgtMscBulkOutErpCallbackCBW - "
                                               "usbTgtMscBulkErpInit error\n",
                                               1, 2, 3, 4, 5, 6);
                                goto EXIT_ERROR;
                                }
                            }
                        }
                    else
                        {
                        /* set CSW Status 0x2 (phase error) */

                        pCsw->bCSWStatus = USB_CSW_PHASE_ERROR;
                        pData = (UINT8 *)pCsw;
                        dSize = sizeof(USB_MSC_CSW);
                        erpCallback = usbTgtMscBulkInErpCallbackCSW;

                        /* init CSW ERP on Bulk-in pipe and submit */

                        if (usbTgtMscBulkErpInit(pUsbTgtMscDev, pData, dSize,
                            USB_PID_IN, erpCallback) != OK)
                            {
                            USBTGT_MSC_ERR("usbTgtMscBulkOutErpCallbackCBW - "
                                    "usbTgtMscBulkErpInit error\n",
                                    1, 2, 3, 4, 5, 6);
                            goto EXIT_ERROR;
                            }
                        }
                    }
                else
                    {
                    /* set CSW Status 0x2 (phase error) */

                    pCsw->bCSWStatus = USB_CSW_PHASE_ERROR;
                    pData = (UINT8 *)pCsw;
                    dSize = sizeof(USB_MSC_CSW);
                    erpCallback = usbTgtMscBulkInErpCallbackCSW;

                    /* init CSW ERP on Bulk-in pipe and submit */

                    if (usbTgtMscBulkErpInit(pUsbTgtMscDev, pData, dSize,
                        USB_PID_IN, erpCallback) != OK)
                        {
                        USBTGT_MSC_ERR("usbTgtMscBulkOutErpCallbackCBW - "
                                       "usbTgtMscBulkErpInit error\n",
                                       1, 2, 3, 4, 5, 6);
                        goto EXIT_ERROR;
                        }
                    }
                }
            else if (rbcDataOut == TRUE)
                {
                if (cbwDataXferLgth > 0)
                    {
                    if (cbwDataDir == USB_PID_OUT)
                        {
                        if (cbwDataXferLgth == dSize)
                            {
                            pCsw->dCSWDataResidue = OS_UINT32_CPU_TO_LE(0x0);

                            /* init Bulk-Out ERP and submit to TCD */

                            if (usbTgtMscBulkErpInit(pUsbTgtMscDev, pData, dSize,
                                USB_PID_OUT, erpCallback) != OK)
                                {
                                USBTGT_MSC_ERR("usbTgtMscBulkOutErpCallbackCBW - "
                                               "usbTgtMscBulkErpInit error\n",
                                               1, 2, 3, 4, 5, 6);
                                goto EXIT_ERROR;
                                }
                            }
                        else if (cbwDataXferLgth > dSize)
                            {
                            pCsw->dCSWDataResidue =
                                OS_UINT32_CPU_TO_LE((cbwDataXferLgth - dSize));

                            /* set stall flag on Bulk-Out Endpoint */

                            pUsbTgtMscDev->bulkOutPipeState = USB_TGT_MSC_PIPE_STALL;

                            /* init Data Out ERP on Bulk-Out Pipe and submit */

                            if (usbTgtMscBulkErpInit(pUsbTgtMscDev, pData, dSize,
                                USB_PID_OUT, erpCallback) != OK)
                                {
                                USBTGT_MSC_ERR("usbTgtMscBulkOutErpCallbackCBW - "
                                               "usbTgtMscBulkErpInit error\n",
                                               1, 2, 3, 4, 5, 6);
                                goto EXIT_ERROR;
                                }
                            }
                        else
                            {
                            /* set CSW Status 0x2 (phase error) */

                            pCsw->bCSWStatus = USB_CSW_PHASE_ERROR;
                            pData = (UINT8 *)pCsw;
                            dSize = sizeof(USB_MSC_CSW);
                            erpCallback = usbTgtMscBulkInErpCallbackCSW;

                            /* init CSW ERP on Bulk-in pipe and submit */

                            if (usbTgtMscBulkErpInit(pUsbTgtMscDev, pData, dSize,
                                USB_PID_IN, erpCallback) != OK)
                                {
                                USBTGT_MSC_ERR("usbTgtMscBulkOutErpCallbackCBW - "
                                               "usbTgtMscBulkErpInit error\n",
                                               1, 2, 3, 4, 5, 6);
                                goto EXIT_ERROR;
                                }
                            }
                        }
                    else
                        {
                        /* set CSW Status 0x2 (phase error) */

                        pCsw->bCSWStatus = USB_CSW_PHASE_ERROR;
                        pData = (UINT8 *)pCsw;
                        dSize = sizeof(USB_MSC_CSW);
                        erpCallback = usbTgtMscBulkInErpCallbackCSW;

                        /* init CSW ERP on Bulk-in pipe and submit */

                        if (usbTgtMscBulkErpInit(pUsbTgtMscDev, pData, dSize,
                            USB_PID_IN, erpCallback) != OK)
                            {
                            USBTGT_MSC_ERR("usbTgtMscBulkOutErpCallbackCBW - "
                                           "usbTgtMscBulkErpInit error\n",
                                           1, 2, 3, 4, 5, 6);
                            goto EXIT_ERROR;
                            }
                        }
                    }
                else
                    {
                    /* set CSW Status 0x2 (phase error) */

                    pCsw->bCSWStatus = USB_CSW_PHASE_ERROR;
                    pData = (UINT8 *)pCsw;
                    dSize = sizeof(USB_MSC_CSW);
                    erpCallback = usbTgtMscBulkInErpCallbackCSW;

                    /* init CSW ERP on Bulk-in pipe and submit */

                    if (usbTgtMscBulkErpInit(pUsbTgtMscDev, pData, dSize,
                        USB_PID_IN, erpCallback) != OK)
                        {
                        USBTGT_MSC_ERR("usbTgtMscBulkOutErpCallbackCBW - "
                                       "usbTgtMscBulkErpInit error\n",
                                       1, 2, 3, 4, 5, 6);
                        goto EXIT_ERROR;
                        }
                    }
                }
            else
                {
                if (cbwDataXferLgth == 0)
                    {
                    /* init CSW ERP on Bulk-in pipe submit ERP */

                    pCsw->dCSWDataResidue = OS_UINT32_CPU_TO_LE(0x0);
                    pData = (UINT8 *)pCsw;
                    dSize = sizeof(USB_MSC_CSW);
                    erpCallback = usbTgtMscBulkInErpCallbackCSW;

                    /* init CSW ERP on Bulk-in pipe and submit */

                    if (usbTgtMscBulkErpInit(pUsbTgtMscDev, pData, dSize,
                        USB_PID_IN, erpCallback) != OK)
                        {
                        USBTGT_MSC_ERR("usbTgtMscBulkOutErpCallbackCBW - "
                                       "usbTgtMscBulkErpInit error\n",
                                       1, 2, 3, 4, 5, 6);
                        goto EXIT_ERROR;
                        }
                    }
                else
                    {
                    /* set CSW residue size, dSize = 0 */

                    pCsw->dCSWDataResidue =
                        OS_UINT32_CPU_TO_LE(cbwDataXferLgth);

                    if (cbwDataDir == USB_PID_IN)
                        {
                        /* set stall flag on Bulk-In Pipe */

                        pUsbTgtMscDev->bulkInPipeState = USB_TGT_MSC_PIPE_STALL;

                        /*
                         * init Data In ERP on Bulk-In pipe and submit
                         * to TCD
                         */

                        if (usbTgtMscBulkErpInit(pUsbTgtMscDev, pData, dSize,
                            USB_PID_IN, erpCallback) != OK)
                            {
                            USBTGT_MSC_ERR("usbTgtMscBulkOutErpCallbackCBW - "
                                           "usbTgtMscBulkErpInit error\n",
                                           1, 2, 3, 4, 5, 6);
                            goto EXIT_ERROR;
                            }
                        }
                    else
                        {
                        /* set stall flag on Bulk-In Pipe */

                        pUsbTgtMscDev->bulkOutPipeState = USB_TGT_MSC_PIPE_STALL;

                        /* init Data In ERP on Bulk-In pipe and submit to TCD */

                        if (usbTgtMscBulkErpInit(pUsbTgtMscDev, pData, dSize,
                            USB_PID_OUT, erpCallback) != OK)
                            {
                            USBTGT_MSC_ERR("usbTgtMscBulkOutErpCallbackCBW - "
                                           "usbTgtMscBulkErpInit error\n",
                                           1, 2, 3, 4, 5, 6);
                            goto EXIT_ERROR;
                            }
                        }
                    }
                }
            }
        else
            {
            usbTgtMscCswSend(pUsbTgtMscDev, USB_CSW_STATUS_FAIL);
            }
        }
    else
        {
        /* Wait for recover */
        
        pUsbTgtMscDev->uCmdStatus = USBTGT_MSC_CBW_INVALID; 
        
        USBTGT_MSC_ERR("usbTgtMscBulkOutErpCallbackCBW - "
                       "not valid CBW... EXIT_ERROR\n",
                       1, 2, 3, 4, 5, 6);
        /* We should stall the BULK IN pipe */
        
        goto EXIT_ERROR;
        }

    return;

EXIT_ERROR:

    return;
    }

/*******************************************************************************
*
* usbTgtMscBulkInErpCallbackCSW - send the CSW on bulk-in pipe
*
* This routine sends the CSW (Command Status Wrapper) back to the host following
* execution of the CBW.
*
* RETURNS: N/A
*
* ERRNO: none
*/

void usbTgtMscBulkInErpCallbackCSW
    (
    pVOID    erp        /* USB_ERP endpoint request packet */
    )
    {
    UINT8          * pData;            /* data buffer */
    UINT32           dSize;            /* size */
    pUSB_ERP         pUsbErp = NULL;
    pUSB_TGT_MSC_LUN pUsbTgtMscLun = NULL;
    pUSB_TGT_MSC_DEV pUsbTgtMscDev = NULL;
    USB_MSC_CBW    * pCbw  = NULL;

    pUsbErp = (pUSB_ERP)erp;

    if ((pUsbErp == NULL) || (pUsbErp->userPtr == NULL))
        {
        USBTGT_MSC_ERR("usbTgtMscBulkOutErpCallbackCBW - Invalid erp\n",
                        1, 2, 3, 4, 5, 6);
        return;
        }

    pUsbTgtMscDev = (pUSB_TGT_MSC_DEV)pUsbErp->userPtr;
    pUsbTgtMscLun = pUsbTgtMscDev->pCurrentLun;
    pUsbTgtMscDev->bulkInErpState = USB_TGT_MSC_ERP_IDLE;

    if (pUsbTgtMscDev->uCurConfig == 0)
        {
        USBTGT_MSC_WARN("usbTgtMscBulkOutErpCallbackCBW - uCurConfig is 0%d\n",
            1, 2, 3, 4, 5, 6);
        return;
        }


    if ((pUsbErp->result != S_usbTgtTcdLib_ERP_SUCCESS) &&
        (pUsbErp->result != S_usbTgtTcdLib_ERP_RESTART) &&
        (pUsbErp->result != OK))
        {
        USBTGT_MSC_ERR("usbTgtMscBulkInErpCallbackCSW ERROR bulk in callback %p\n",
            pUsbErp->result, 2, 3, 4, 5, 6);
        return;
        }

    pCbw = pUsbTgtMscDev->pCBW;
    memset(pCbw, 0, sizeof(USB_MSC_CBW));
    pCbw->dCBWSignature = OS_UINT32_CPU_TO_LE (USB_MSC_CBW_SIGNATURE);
    pCbw->bCBWLUN = (UINT8) (pUsbTgtMscLun->lunIndex);
    pData = (UINT8 *)pCbw;
    dSize = sizeof (USB_MSC_CBW);

    /* init bulk-out ERP w/usbTgtMscBulkOutErpCallbackCBW() callback */

    if (usbTgtMscBulkErpInit(pUsbTgtMscDev, pData, dSize,
        USB_PID_OUT, usbTgtMscBulkOutErpCallbackCBW) != OK)
        {
        USBTGT_MSC_ERR("usbTgtMscBulkErpInit ERROR bulk in callback\n",
                       1, 2, 3, 4, 5, 6);
        usbTgtSetPipeStatus(pUsbTgtMscDev->bulkOutPipe, USBTGT_PIPE_STATUS_UNSTALL);
        pUsbTgtMscDev->bulkOutPipeState = USB_TGT_MSC_PIPE_UNSTALL;
        }



    return;
    }

/*******************************************************************************
*
* usbTgtMscBulkInErpCallbackData - process end of data phase on bulk-in pipe
*
* This routine is invoked following a data IN phase to the host.
*
* RETURNS: N/A
*
* ERRNO:
*  none
*/

void usbTgtMscBulkInErpCallbackData
    (
    pVOID        erp        /* USB_ERP endpoint request packet */
    )
    {
    USB_MSC_CBW * pCbw;         /* USB_MSC_CBW    */
    USB_MSC_CSW * pCsw;         /* USB_MSC_CSW    */
    UINT8         opCode;       /* operation code */
    UINT8 *       pData;        /* pointer to buffer */
    UINT32        dSize;        /* size */

    pUSB_ERP         pUsbErp = NULL;
    pUSB_TGT_MSC_LUN pUsbTgtMscLun = NULL;
    pUSB_TGT_MSC_DEV pUsbTgtMscDev = NULL;

    pUsbErp = (pUSB_ERP)erp;

    if ((pUsbErp == NULL) || (pUsbErp->userPtr == NULL))
        {
        USBTGT_MSC_ERR("usbTgtMscBulkInErpCallbackData - Invalid erp\n",
                        1, 2, 3, 4, 5, 6);
        return;
        }

    pUsbTgtMscDev = (pUSB_TGT_MSC_DEV)pUsbErp->userPtr;
    pUsbTgtMscLun = pUsbTgtMscDev->pCurrentLun;
    pUsbTgtMscDev->bulkInErpState = USB_TGT_MSC_ERP_IDLE;
    USBTGT_MSC_BUF_RELEASE(pUsbTgtMscDev);

    /* if a reset ocurred, we are no longer configured */

    if (pUsbTgtMscDev->uCurConfig == 0)
        {
        USBTGT_MSC_ERR("usbTgtMscBulkInErpCallbackData - uCurConfig is 0\n",
                       1, 2, 3, 4, 5, 6);
        return;
        }

    if ((pUsbErp->result != S_usbTgtTcdLib_ERP_SUCCESS) &&
        (pUsbErp->result != S_usbTgtTcdLib_ERP_RESTART) &&
        (pUsbErp->result != OK))
        {
        USBTGT_MSC_ERR("usbTgtMscBulkInErpCallbackData %p\n",
                       pUsbErp->result, 2, 3, 4, 5, 6);
        return;
        }

    pCsw = pUsbTgtMscDev->pCSW;
    pCbw = pUsbTgtMscDev->pCBW;
    opCode = pCbw->CBWCB[0];

    switch(opCode)
        {
        /* place any user specific code here */

        case RBC_CMD_FORMAT:
            break;
        case RBC_CMD_READ10:
        case RBC_CMD_READ12:
            /* If there is still some data to read */
            
            if (pUsbTgtMscLun->mediaRemoved == TRUE)
                 {
                 pCsw->bCSWStatus = USB_CSW_STATUS_FAIL;
                 }
            else
                {
                if (pUsbTgtMscDev->uTotalLen > pUsbTgtMscDev->uActLen)
                    {
                    if (ERROR == usbTgtMscLunRead(pUsbTgtMscLun))
                        {
                        USBTGT_MSC_ERR("usbTgtMscLunRead fail\n",
                                       1, 2, 3, 4, 5, 6);
                        }
                    return;
                    }
                }
            break;
        case RBC_CMD_READCAPACITY:
           if (pUsbTgtMscLun->mediaRemoved == TRUE)
                {
                pCsw->bCSWStatus = USB_CSW_STATUS_FAIL;
                }
           break;
        case RBC_CMD_STARTSTOPUNIT:
        case RBC_CMD_SYNCCACHE:
        case RBC_CMD_VERIFY10:
        case RBC_CMD_WRITE10:
        case RBC_CMD_INQUIRY:
        case RBC_CMD_MODESELECT6:
        case RBC_CMD_MODESENSE6:
        case RBC_CMD_PERSISTANTRESERVIN:
        case RBC_CMD_PERSISTANTRESERVOUT:
        case RBC_CMD_PRVENTALLOWMEDIUMREMOVAL:
        case RBC_CMD_RELEASE6:
        case RBC_CMD_REQUESTSENSE:
        case RBC_CMD_RESERVE6:
        case RBC_CMD_TESTUNITREADY:
        case RBC_CMD_WRITEBUFFER:
        case RBC_CMD_MODESENSE10:
        case RBC_CMD_MODESELECT10:
        default:
        break;
        }

    /* init CSW ERP on Bulk-in pipe and submit */

    pData = (UINT8 *)pCsw;
    dSize = sizeof(USB_MSC_CSW);

    /* init CSW ERP on Bulk-in pipe and submit */

    if (usbTgtMscBulkErpInit(pUsbTgtMscDev, pData, dSize,
        USB_PID_IN, usbTgtMscBulkInErpCallbackCSW) != OK)
        {
        USBTGT_MSC_ERR("usbTgtMscBulkInErpCallbackData - "
                       "submit bulk in CSW fail\n",
                       1, 2, 3, 4, 5, 6);
        usbTgtSetPipeStatus(pUsbTgtMscDev->bulkInPipe, USBTGT_PIPE_STATUS_STALL);
        pUsbTgtMscDev->bulkInPipeState = USB_TGT_MSC_PIPE_STALL;
        }

    return;
    }

/*******************************************************************************
*
* usbTgtMscBulkOutErpCallbackData - process end of data phase on bulk-out pipe
*
* This routine is invoked following a data OUT phase from the host.
*
* RETURNS: N/A
*
* ERRNO:
*  none
*/
void usbTgtMscBulkOutErpCallbackData
    (
    pVOID    erp        /* USB_ERP endpoint request packet */
    )
    {
    USB_MSC_CBW *    pCbw;           /* USB_MSC_CBW */
    USB_MSC_CSW *    pCsw;           /* USB_MSC_CSW */
    UINT8            opCode;         /* operation code */
    UINT8 *          pData;          /* pointer to buffer */
    UINT32           dSize;          /* size */
    UINT32           uTotalLen;
    UINT32           uRemainLen;
    UINT32           uWriteLen;
    UINT32           uStartBlk;    
    UINT16           uTransferBlks;
    STATUS           retVal;
    UINT8 *          pBuf;
    UINT8 *          pWriteBuf;
    pUSB_ERP         pUsbErp = NULL;
    pUSB_TGT_MSC_LUN pUsbTgtMscLun = NULL;
    pUSB_TGT_MSC_DEV pUsbTgtMscDev = NULL;


    pUsbErp = (pUSB_ERP)erp;

    if ((pUsbErp == NULL) || (pUsbErp->userPtr == NULL))
        {
        USBTGT_MSC_ERR("usbTgtMscBulkOutErpCallbackData - Invalid erp\n",
                        1, 2, 3, 4, 5, 6);
        return;
        }

    pUsbTgtMscDev = (pUSB_TGT_MSC_DEV)pUsbErp->userPtr;
    pUsbTgtMscLun = pUsbTgtMscDev->pCurrentLun;
    pUsbTgtMscDev->bulkOutErpState = USB_TGT_MSC_ERP_IDLE;
    USBTGT_MSC_BUF_RELEASE(pUsbTgtMscDev);

    /* if we had a reset condition, then we are no longer configured */

    if (pUsbTgtMscDev->uCurConfig == 0)
        {
        USBTGT_MSC_ERR("usbTgtMscBulkOutErpCallbackData - uCurConfig is 0%d\n",
                       1, 2, 3, 4, 5, 6);
        return;
        }

    /* verify that the data is good */

    if ((pUsbErp->result != S_usbTgtTcdLib_ERP_SUCCESS) &&
        (pUsbErp->result != S_usbTgtTcdLib_ERP_RESTART) &&
        (pUsbErp->result != OK))
        {
        USBTGT_MSC_ERR("usbTgtMscBulkOutErpCallbackData - "
                       "pUsbErp->result error\n", 1, 2, 3, 4, 5, 6);
        return;
        }

    pCsw = pUsbTgtMscDev->pCSW;
    pCbw = pUsbTgtMscDev->pCBW;
    opCode = pCbw->CBWCB[0];

    switch(opCode)
        {
        case RBC_CMD_WRITE10:
        case RBC_CMD_WRITE12:
            {
            /* If this LUN is readonly, we should return fail status */
            
            if (pUsbTgtMscLun->bReadOnly == TRUE)
                {
                pCsw->bCSWStatus = USB_CSW_STATUS_FAIL;
                }
            else
                {
                /* 
                 * This callback means write data is coming, 
                 * we should update to the real device
                 */
                 
                uWriteLen = pUsbErp->bfrList[0].bfrLen;
                pWriteBuf = pUsbErp->bfrList[0].pBfr;

                /* Update the start block number */ 
                
                uStartBlk = (pUsbTgtMscDev->cmd[2] << 24) | 
                            (pUsbTgtMscDev->cmd[3] << 16) | 
                            (pUsbTgtMscDev->cmd[4] << 8)  | 
                             pUsbTgtMscDev->cmd[5];
                
                uTransferBlks = (UINT16) (pUsbTgtMscDev->uActLen / 
                                pUsbTgtMscLun->perBlockSize);
            
                uStartBlk += uTransferBlks;
            
                /* Popolate the bio for write operation */
                
                pUsbTgtMscLun->bio.bio_dev     = pUsbTgtMscLun->devXbdHandle;
                pUsbTgtMscLun->bio.bio_blkno   = uStartBlk;
                pUsbTgtMscLun->bio.bio_bcount  = uWriteLen;
                pUsbTgtMscLun->bio.bio_data    = pWriteBuf;
                pUsbTgtMscLun->bio.bio_error   = 0;
                pUsbTgtMscLun->bio.bio_flags   = BIO_WRITE;
                pUsbTgtMscLun->bio.bio_caller1 = (void *)&pUsbTgtMscLun->bioDoneSem;
                pUsbTgtMscLun->bio.bio_chain   = NULL;
                pUsbTgtMscLun->bio.bio_done    = usbTgtMscDevBioDone;
                
                /* perform block write operation */
                
                retVal = xbdStrategy (pUsbTgtMscLun->devXbdHandle, &pUsbTgtMscLun->bio);
                
                if (retVal == ERROR)
                    {                        
                    usbTgtSetPipeStatus(pUsbTgtMscDev->bulkOutPipe, USBTGT_PIPE_STATUS_STALL);
                    pUsbTgtMscDev->bulkOutPipeState = USB_TGT_MSC_PIPE_STALL;
                    USBTGT_MSC_ERR("bioBlkRW returned ERROR\n", 1, 2, 3, 4, 5, 6); 
                    return ;
                    }
                
                if (semTake (&pUsbTgtMscLun->bioDoneSem, WAIT_FOREVER) == ERROR)
                    {
                    USBTGT_MSC_ERR("semTake bioDoneSem error\n", 1, 2, 3, 4, 5, 6); 
                    return ;
                    }

                /* Update the actual length and csw's dataresidue field */
                
                pUsbTgtMscDev->uActLen += uWriteLen;
                
                /* 
                 * If there is still some data to transfer we need submit a 
                 * bulk out erp to get the write data back and wait for the
                 * callback or else send a csw to indicate it is complete
                 */

                if (pUsbTgtMscDev->uTotalLen > pUsbTgtMscDev->uActLen)
                    {
                    uRemainLen = pUsbTgtMscDev->uTotalLen - pUsbTgtMscDev->uActLen;
                    pUsbTgtMscDev->pCSW->dCSWDataResidue = 
                        OS_UINT32_CPU_TO_LE(uRemainLen);
                    
                    pBuf = usbTgtMscEmptyBufGet(pUsbTgtMscDev);
                    
                    /* init Bulk-Out ERP and submit to TCD */
            
                    if (usbTgtMscBulkErpInit(pUsbTgtMscDev, 
                                             pBuf, 
                                             min(uRemainLen, MSC_DATA_BUF_LEN),
                                             USB_PID_OUT, 
                                             usbTgtMscBulkOutErpCallbackData) != OK)
                        {
                        USBTGT_MSC_ERR("usbTgtMscBulkOutErpCallbackCBW - "
                                       "usbTgtMscBulkErpInit error\n", 
                                       1, 2, 3, 4, 5, 6);
                        USBTGT_MSC_BUF_RELEASE(pUsbTgtMscDev);
                        return;
                        }
                    return ;
                    }
                else
                    {
                    /*
                     * If we finish the data transfer, update CSW
                     * according to the transfer status 
                     */
                     
                    UINT16 uNumBlks;
                    uNumBlks = (UINT16)((pUsbTgtMscDev->cmd[7] << 8) | 
                                         pUsbTgtMscDev->cmd[8]);
                    uTotalLen = uNumBlks * pUsbTgtMscLun->perBlockSize;

                    /* 
                     * If the total length in the cmd field is bigger than the
                     * CBWTransferLength the CSW status should be fail
                     * Or else update the CSW's data residue field 
                     */
                     
                    if (uTotalLen > pUsbTgtMscDev->uTotalLen)
                        {
                        USBTGT_MSC_DBG("Finish write but CBWtransLen %x < cmdTransLen %x\n",
                                     pUsbTgtMscDev->uTotalLen, 
                                     uTotalLen,
                                     3, 4, 5, 6);
                        pCsw->bCSWStatus = USB_CSW_STATUS_FAIL;
                        }
                    else
                        {
                        pUsbTgtMscDev->pCSW->dCSWDataResidue = 
                        OS_UINT32_CPU_TO_LE((pUsbTgtMscDev->uTotalLen - uTotalLen));
                        }
                    }
                }
            }
            break;
        case RBC_CMD_FORMAT:
        case RBC_CMD_READ10:
        case RBC_CMD_READCAPACITY:
        case RBC_CMD_STARTSTOPUNIT:
        case RBC_CMD_SYNCCACHE:
        case RBC_CMD_VERIFY10:
        case RBC_CMD_INQUIRY:
        case RBC_CMD_MODESELECT6:
        case RBC_CMD_MODESENSE6:
        case RBC_CMD_PERSISTANTRESERVIN:
        case RBC_CMD_PERSISTANTRESERVOUT:
        case RBC_CMD_PRVENTALLOWMEDIUMREMOVAL:
        case RBC_CMD_RELEASE6:
        case RBC_CMD_REQUESTSENSE:
        case RBC_CMD_RESERVE6:
        case RBC_CMD_TESTUNITREADY:
        case RBC_CMD_WRITEBUFFER:
        default:
        break;
        }

    /* init CSW ERP on Bulk-in pipe and submit */

    pData = (UINT8 *)pCsw;
    dSize = sizeof(USB_MSC_CSW);

    /* init CSW ERP on Bulk-in pipe and submit */

    if (usbTgtMscBulkErpInit(pUsbTgtMscDev, pData, dSize,
        USB_PID_IN, usbTgtMscBulkInErpCallbackCSW) != OK)
        {
        USBTGT_MSC_ERR("usbTgtMscBulkOutErpCallbackData - "
                       "init CSW ERP ERROR, stalling BULK IN\n",
                        1, 2, 3, 4, 5, 6);
        usbTgtSetPipeStatus(pUsbTgtMscDev->bulkInPipe, USBTGT_PIPE_STATUS_STALL);
        pUsbTgtMscDev->bulkInPipeState = USB_TGT_MSC_PIPE_STALL;
        }
    return;
    }
