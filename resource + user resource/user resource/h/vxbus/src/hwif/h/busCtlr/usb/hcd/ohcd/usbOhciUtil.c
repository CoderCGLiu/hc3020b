/* usbOhciUtil.c - Util module of USB OHCD */

/*
 * Copyright (c) 2010, 2013-2014 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01i,25Feb14,wyy  Enable vxbDmaBuf alignment checking (VXW6-70065)
01h,03may13,wyy  Remove compiler warning (WIND00356717)
01g,24apr13,wyy  Clean warning of code coverity
01f,17aug10,m_y  Use routine usbOhciModifyTDList to modify the TD list
                 modify routine usbOchiCheckRequestPending and rollback the
                 "01d" change (WIND00228503)
01e,05aug10,m_y  Modify the return value of usbOhciPhyToVirt (WIND00226314)
01d,16jul10,m_y  Set pHCDPipe to (-1) after free to avoid re-access
                 the content (WIND00223843)
01c,02jul10,m_y  Modify routine usbOhciCleanDisabledPipe and
                 usbOchiCheckRequestPending to avoid frequently plug in/out
                 GEN1 cbi crash issue (WIND00221377)
01b,17jun10,m_y  Replace the routine usbOhciFreeTDList to
                 usbOhciFreeRequestTDList (WIND00216859)
01a,18may10,m_y  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
*/

/*

DESCRIPTION

This file defines the utility routines of the USB OHCI driver.

INCLUDE FILES: usbOhciTransferManagement.h,  usbOhciUtil.h
*/

/* includes */
#include <vxWorks.h>
#include "usbOhciTransferManagement.h"
#include "usbOhciUtil.h"
#include "usbOhciRegisterInfo.h"
/* externs */

extern USBHST_STATUS usbOhciUnSetupPipe
    (
    pUSB_OHCD_DATA    pHCDData,
    pUSB_OHCD_PIPE    pHCDPipe
    );

/*******************************************************************************
*
* usbOhciPhyToVirt - translate the bus address to virtual address
*
* This routine is used to translate the bus address to virtual address.
*
* RETURNS: virtual address of the bus address
*
* ERRNO: N/A
*
* \NOMANUAL
*/

PVOID usbOhciPhyToVirt
    (
    pUSB_OHCD_DATA       pHCDData,
    bus_addr_t           busAddr,
    UINT8                uFlag
    )
    {
    NODE *                      pNode = NULL;
    pUSB_OHCI_MEM_MAP_TABLE     pMemMap = NULL;

    if ((uFlag != USB_OHCI_MEM_MAP_TD) && (uFlag != USB_OHCI_MEM_MAP_ED))
        {
        USB_OHCD_ERR("usbOhciPhyToVirt - uFlag incorrect uFlag = %d\n",
                     uFlag, 2, 3, 4, 5, 6);

        return NULL;
        }

    /* FIXME: Normally bus address will not be zero, but it still can be! */

    if (busAddr == 0)
        {
        USB_OHCD_VDBG("usbOhciPhyToVirt - busAddr is NULL\n",
                      1, 2, 3, 4, 5, 6);
        return NULL;
        }

    OS_WAIT_FOR_EVENT(pHCDData->memMapTableEvent, WAIT_FOREVER);

    if (uFlag == USB_OHCI_MEM_MAP_TD)
        {
        pNode = lstFirst((LIST *)(&(pHCDData->tdMemMapTableList)));
        }
    else
        {
        pNode = lstFirst((LIST *)(&(pHCDData->edMemMapTableList)));
        }

    while (pNode != NULL)
        {
        pMemMap = MEM_NODE_TO_USB_OHCI_MEM_MAP_TABLE(pNode);

        if (pMemMap != NULL)
            {
            if (pMemMap->uBusAddr == busAddr)
                {
                OS_RELEASE_EVENT(pHCDData->memMapTableEvent);

                return pMemMap->pVirtAddr;
                }
            }

        pNode = lstNext(pNode);
        }

    OS_RELEASE_EVENT(pHCDData->memMapTableEvent);

    USB_OHCD_ERR("No matching virtual address for busAddr 0x%x, uFlag = %d\n",
                 busAddr, uFlag, 3, 4, 5, 6);
    /*
     * If we can't find the address in the memMapList we will return NULL
     * to indicate error.
     */

    return (pVOID)(ULONG)(NULL);
    }

/*******************************************************************************
*
* usbOhciCreateRequestInfo - create a request info of the pipe
*
* This routine is used to create a request info of the pipe.
*
* RETURNS: Pointer to the request info structure
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_OHCD_REQUEST_INFO usbOhciCreateRequestInfo
   (
    pUSB_OHCD_DATA      pHCDData,
    pUSB_OHCD_PIPE      pHCDPipe
    )
    {
    /* Request information */

    pUSB_OHCD_REQUEST_INFO  pRequest = NULL;

    /* Get the request info */

    pRequest = (pUSB_OHCD_REQUEST_INFO)
               OS_MALLOC(sizeof(USB_OHCD_REQUEST_INFO));

    if (pRequest == NULL)
         {
         USB_OHCD_ERR("usbOhciCreateRequestInfo - "
                      "allocate pRequest fail\n",
                      1, 2, 3, 4, 5, 6);
         return NULL;
         }

    /* Clear the request info */

    OS_MEMSET(pRequest, 0, sizeof(USB_OHCD_REQUEST_INFO));

    /* Create Data DMA MAP ID */

    pRequest->usrDataDmaMapId = vxbDmaBufMapCreate (pHCDData->pDev,
                                                    pHCDPipe->usrBuffDmaTagId,
                                                    0,
                                                    NULL);

    if (pRequest->usrDataDmaMapId == NULL)
        {
        USB_OHCD_ERR("usbOhciCreateRequestInfo - "
                     "allocate usrDataDmaMapId fail\n",
                     1, 2, 3, 4, 5, 6);

        /* Delete the request resources */

        usbOhciDeleteRequestInfo(pHCDData, pHCDPipe, pRequest);

        return NULL;
        }

    /* Control pipe needs special treatment for the Setup */

    if (pHCDPipe->uEndpointType == USBHST_CONTROL_TRANSFER)
        {
        /* Create control Setup DMA MAP ID */

        pRequest->ctrlSetupDmaMapId = vxbDmaBufMapCreate (pHCDData->pDev,
                                                          pHCDPipe->ctrlSetupDmaTagId,
                                                          0,
                                                          NULL);

        if (pRequest->ctrlSetupDmaMapId == NULL)
            {
            USB_OHCD_ERR("usbOhciCreateRequestInfo - "
                         "allocate ctrlSetupDmaMapId fail\n",
                          1, 2, 3, 4, 5, 6);

            /* Delete the request resources */

            usbOhciDeleteRequestInfo(pHCDData, pHCDPipe, pRequest);

            return NULL;
            }
        }

    /* Link the new request into the free request queue */

    ADD_REQEUEST_INTO_FREE_LIST(pHCDPipe, pRequest);

    return pRequest;
    }

/*******************************************************************************
*
* usbOhciDeleteRequestInfo - delete a request info of the pipe
*
* This routine is used to delete a request info of the pipe. It will return
* the trasnfer descriptors to the HCD free list and destroy the DMA map
* associated with this request info structures.
*
* <pHCDData> - Pointer to the HCD data
* <pHCDPipe> - Pointer to the pipe
* <pRequest> - Pointer to the request info structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the OHCD. Assumption is that valid parameters are passed by the OHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbOhciDeleteRequestInfo
    (
    pUSB_OHCD_DATA                  pHCDData,
    pUSB_OHCD_PIPE                  pHCDPipe,
    pUSB_OHCD_REQUEST_INFO          pRequest
    )
    {
    /* The Caller routine should verify the parameters valid */

    if (pRequest != NULL)
        {
        /* Destroy the maps creatred */

        if (pRequest->ctrlSetupDmaMapId != NULL)
            {
            vxbDmaBufMapDestroy (pHCDPipe->ctrlSetupDmaTagId,
                                 pRequest->ctrlSetupDmaMapId);

            pRequest->ctrlSetupDmaMapId = NULL;
            }

        if (pRequest->usrDataDmaMapId != NULL)
            {
            vxbDmaBufMapDestroy (pHCDPipe->usrBuffDmaTagId,
                                 pRequest->usrDataDmaMapId);

            pRequest->usrDataDmaMapId = NULL;
            }

        /* Free the request */

        OS_FREE(pRequest);
        pRequest = NULL;
        }
    return;
    }

/*******************************************************************************
*
* usbOhciReserveRequestInfo - reserve a request info of the pipe for a URB
*
* This routine is used to reserve a request info structure of the pipe for a
* URB. It is commonly called when submitting a URB. A request info is considered
* to have been reserved when its associated with a URB (the URB pointer of this
* request info structure being non-NULL).
*
* <pHCDData> - Pointer to the HCD data
* <pHCDPipe> - Pointer to the pipe
* <pUrb> - Pointer to the URB
*
* WARNING : No parameter validation is done as this routine is used internally
* by the OHCD. Assumption is that valid parameters are passed by the OHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: the pointer to the reserved request info structure,
*   or NULL if no more free request on this pipe.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_OHCD_REQUEST_INFO usbOhciReserveRequestInfo
    (
    pUSB_OHCD_DATA                 pHCDData,
    pUSB_OHCD_PIPE                 pHCDPipe,
    pUSBHST_URB                    pUrb
    )
    {
    NODE                    *pNode = NULL;
    pUSB_OHCD_REQUEST_INFO  pRequest = NULL;

    /* We can not allow bigger than the specified size */

    if (pUrb->uTransferLength > pHCDPipe->uMaxTransferSize)
        {
        USB_OHCD_ERR("Invalid parameter:"
                     "pUrb->uTransferLength = %d,pHCDPipe->uMaxTransferSize = %d\n",
                     pUrb->uTransferLength, pHCDPipe->uMaxTransferSize,
                     3, 4, 5, 6);

        return NULL;
        }

    /* Check if there is a free request info */

    if (0 == lstCount(&(pHCDPipe->freeRequestList)))
        {
        /* Create a new request */

        pRequest = usbOhciCreateRequestInfo(pHCDData, pHCDPipe);

        if (pRequest == NULL)
            {
            USB_OHCD_ERR("usbOhciReserveRequestInfo -"
                         "uTransferLength (%d) uMaxTransferSize (%d) \n",
                         pUrb->uTransferLength,
                         pHCDPipe->uMaxTransferSize,
                         3, 4, 5, 6);

            return NULL;
            }
        }
    /* Get the first node of the free list */

    pNode = lstFirst(&(pHCDPipe->freeRequestList));

    /* Transfer it to usb_ohcd_request_info structure */

    pRequest = LIST_NODE_TO_USB_OHCD_REQUEST_INFO(pNode);

    /*
     * Delete this request from the free list
     * Add this request into ED list
     */

    MOVE_REQUEST_FROM_FREE_TO_PIPE_LIST(pHCDPipe, pRequest);

    /* Associate the request info with the URB */

    pRequest->pUrb = pUrb;

    pRequest->uRequestLength = pUrb->uTransferLength;

    /* Associate the URB with the request */

    pUrb->pHcdSpecific = (void *)pRequest;

    return pRequest;
    }

/*******************************************************************************
*
* usbOhciReleaseRequestInfo - release a request info of the pipe
*
* This routine is used to release a request info structure of the pipe.
* It is commonly called when returnning a URB to the user.
*
* <pHCDData> - Pointer to the HCD data
* <pHCDPipe> - Pointer to the pipe
* <pRequest> - Pointer to the request info structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the OHCD. Assumption is that valid parameters are passed by the OHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbOhciReleaseRequestInfo
    (
    pUSB_OHCD_DATA                 pHCDData,
    pUSB_OHCD_PIPE                 pHCDPipe,
    pUSB_OHCD_REQUEST_INFO         pRequest
    )
    {
    if (pRequest != NULL)
        {
        /*
         * If the request still in the cancel list just remove it
         */

        REMOVE_REQEUEST_FROM_CANCEL_LIST(pHCDPipe, pRequest);

        /* Set the pUrb as NULL to indicate it is free */

        pRequest->pUrb = NULL;

        /*
         * Delete this request from the ED list
         * Add this request into free list
         */

        MOVE_REQUEST_FROM_PIPE_TO_FREE_LIST(pHCDPipe, pRequest);
        }

    return;
    }

/*******************************************************************************
*
* usbOhciCreateTDList - Create a TD list
*
* This routine creates a TD list have 'uNumOfTD' TDs
*
* RETURNS: TRUR, or FALSE is failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOLEAN usbOhciCreateTDList
    (
    pUSB_OHCD_DATA                         pHCDData,
    pUSB_OHCD_PIPE                         pHCDPipe,
    pUSB_OHCD_REQUEST_INFO                 pRequest,
    PUSB_OHCI_TD                           * ppDataHead,
    PUSB_OHCI_TD                           * ppDataTail,
    UINT32                                 uNumOfTD
    )
    {
    PUSB_OHCI_TD pCurTD = NULL;
    PUSB_OHCI_TD pTempTD = NULL;
    UINT32       uIndex = 0x0;

    /* Check the validity of the parameters */

    if ((NULL == ppDataHead) ||
        (NULL == ppDataTail) ||
        (0 == uNumOfTD))
        {
        USB_OHCD_ERR("Invalid  Parameters: "
                     "ppDataHead = %p, ppDataTail = %p, uNumOfTD = %d\n",
                     ppDataHead, ppDataTail, uNumOfTD, 4, 5, 6);
        return FALSE;
        }

    for (uIndex = 0; uIndex < uNumOfTD; uIndex++)
        {
        pCurTD = usbOhciGetFreeTD(pHCDData, pHCDPipe);

        if (NULL == pCurTD)
            {
            USB_OHCD_ERR("usbOhciCreateTDList - "
                         "usbOhciGetFreeTD fail \n",
                         1, 2, 3, 4, 5, 6);
            pCurTD = *ppDataHead;
            while (NULL != pCurTD)
                {
                pTempTD = pCurTD->pHCDNextTransferDescriptor;
                usbOhciAddToFreeTDList(pHCDData, pHCDPipe, pCurTD);
                pCurTD = pTempTD;
                }
            *ppDataHead = NULL;
            *ppDataTail = NULL;
            return FALSE;
            }

        pCurTD->pRequestInfo = pRequest;

        if (uIndex == 0)
            {
            *ppDataHead = pCurTD;
            *ppDataTail = pCurTD;
            }
        else
            {
            (*ppDataTail)->pHCDNextTransferDescriptor = pCurTD;
            (*ppDataTail)->uNextTDPointer =
                USB_OHCD_SWAP_DATA(pHCDData->uBusIndex,
                                   USB_OHCI_DESC_LO32(pCurTD));
            *ppDataTail = pCurTD;
            }
        }

    return TRUE;
    }

/*******************************************************************************
*
* usbOhciCreateTD - creat a TD structure
*
* This routine creates a TD structure.
*
* RETURNS: Pointer of the TD structure
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL PUSB_OHCI_TD usbOhciCreateTD
    (
    pUSB_OHCD_DATA  pHCDData
    )
    {
    /* To hold the pointer to the QTD */

    PUSB_OHCI_TD   pTD = NULL;

    /* Result of the vxbDmaBufMapLoad */

    STATUS          vxbSts = ERROR;

    pUSB_OHCI_MEM_MAP_TABLE  pTDMemMap = NULL;

    /* The DMA MAP ID */

    VXB_DMA_MAP_ID  dmaMapId = NULL;

    /* Create memory and DMA map for the QTD */

    pTD = vxbDmaBufMemAlloc (pHCDData->pDev,
                             pHCDData->tdDmaTagId,
                             NULL,
                             0,
                             &dmaMapId);

    /* Check if allocation is successful */

    if (NULL == pTD)
        {
        USB_OHCD_ERR("creating pTD fail\n",
                     1, 2, 3, 4, 5, 6);
        return NULL;
        }

    /* Zero the memory allocated */

    bzero ((char *)pTD, USB_OHCI_MAX_TD_SIZE);

    /* Save our MAP ID */

    pTD->dmaMapId = dmaMapId;

    /*
     * Load the DMA MAP so that we get the correct BUS address
     * to program the hardware
     */

    vxbSts = vxbDmaBufMapLoad (pHCDData->pDev,
                               pHCDData->tdDmaTagId,
                               pTD->dmaMapId,
                               pTD,
                               USB_OHCI_MAX_TD_SIZE,
                               0);

    if (vxbSts != OK)
        {
        USB_OHCD_ERR("loading map fail\n",
                     1, 2, 3, 4, 5, 6);

        /* Free the memory allocated */

        (void) vxbDmaBufMemFree(pHCDData->tdDmaTagId, pTD, pTD->dmaMapId);

        return NULL;
        }

    if (NULL == (pTDMemMap = OS_MALLOC(sizeof(USB_OHCI_MEM_MAP_TABLE))))
        {
        USB_OHCD_ERR("create memMap fail\n",
                     1, 2, 3, 4, 5, 6);
        (void) vxbDmaBufMemFree(pHCDData->tdDmaTagId, pTD, pTD->dmaMapId);
        return NULL;
        }
    else
        {
        OS_WAIT_FOR_EVENT(pHCDData->memMapTableEvent, WAIT_FOREVER);
        OS_MEMSET(pTDMemMap, 0, sizeof(USB_OHCI_MEM_MAP_TABLE));
        pTDMemMap->uBusAddr = USB_OHCI_DESC_LO32(pTD);
        pTDMemMap->pVirtAddr = (PVOID)(pTD);
        lstAdd((LIST *)(&(pHCDData->tdMemMapTableList)),
               (NODE *)(&(pTDMemMap->memNode)));
        pTD->pMemMap = pTDMemMap;
        OS_RELEASE_EVENT(pHCDData->memMapTableEvent);
        }

    return pTD;
    }

/*******************************************************************************
*
* usbOhciDestroyTD - Delete a TD structure
*
* This routine deletes a TD structure.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbOhciDestroyTD
    (
    pUSB_OHCD_DATA  pHCDData,
    PUSB_OHCI_TD    pTD
    )
    {
    pUSB_OHCI_MEM_MAP_TABLE  pTDMemMap = NULL;
    pUSB_OHCI_MEM_MAP_TABLE  pTempMemMap = NULL;
    NODE                     *pNode = NULL;

    if ((NULL == pTD) ||(NULL == pHCDData))
        {
        USB_OHCD_ERR("usbOhciDestroyTD - vxbDmaBufMapUnload fail\n",
                     0, 0, 0, 0, 0, 0);
        return ERROR;
        }

    pTDMemMap = pTD->pMemMap;

    if (pTDMemMap != NULL)
        {
        OS_WAIT_FOR_EVENT(pHCDData->memMapTableEvent, WAIT_FOREVER);
        pNode = lstFirst((LIST *)(&(pHCDData->tdMemMapTableList)));

        while (pNode != NULL)
            {
            pTempMemMap = MEM_NODE_TO_USB_OHCI_MEM_MAP_TABLE(pNode);

            if (pTempMemMap == pTDMemMap)
                {
                lstDelete((LIST *)(&(pHCDData->tdMemMapTableList)),
                          (NODE *)(&(pTDMemMap->memNode)));
                break;
                }
            pNode =  lstNext(pNode);
            }
        OS_RELEASE_EVENT(pHCDData->memMapTableEvent);
        OS_FREE(pTDMemMap);
        pTD->pMemMap= NULL;
        }

    /* Unload the DMA MAP */

    if (vxbDmaBufMapUnload (pHCDData->tdDmaTagId, pTD->dmaMapId) != OK)
        {
        USB_OHCD_ERR("usbOhciDestroyTD - vxbDmaBufMapUnload fail\n",
                     0, 0, 0, 0, 0, 0);

        return ERROR;
        }

    return vxbDmaBufMemFree(pHCDData->tdDmaTagId, pTD, pTD->dmaMapId);
    }

/*******************************************************************************
*
* usbOhciGetFreeTD - get a free TD from pipe's free TD list
*
* This routine gets a free TD from pipe's free TD list.
*
* RETURNS: Pointer to the free TD structure.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

PUSB_OHCI_TD usbOhciGetFreeTD
    (
    pUSB_OHCD_DATA  pHCDData,
    pUSB_OHCD_PIPE  pHCDPipe
    )
    {
    /* Pointer to the TD */

    PUSB_OHCI_TD pTD = NULL;

    /*
     * Check if there is any trasnfer descriptor in the free list and
     * extract the trasnfer descriptor.Update the free list. This is done
     * acquiring and releasing an event.
     */

    /* There are trasnfer descriptors in the free list */

    if (NULL != pHCDPipe->pFreeTDHead)
        {
        /* Get the head trasnfer descriptor */

        pTD = pHCDPipe->pFreeTDHead;

        /* The head itself is returned, update head ptr to the next */

        pHCDPipe->pFreeTDHead = pTD->pHCDNextTransferDescriptor;

        /* If this is the last trasnfer descriptor in the list, update the tail */

        if (pTD == pHCDPipe->pFreeTDTail)
            {
            pHCDPipe->pFreeTDTail = NULL;
            pHCDPipe->pFreeTDHead = NULL;
            }

        /* Break from the free list */

        pTD->pHCDNextTransferDescriptor = NULL;

        }
    else
        {
        /* Create a new TD */

        pTD = usbOhciCreateTD(pHCDData);

        if (NULL == pTD)
            {
            USB_OHCD_ERR("usbOhciGetFreeTD - "
                         "usbOhciCreateTD fail\n",
                         1, 2, 3, 4, 5, 6);
            return NULL;
            }
        }

    /* Update the TD's pEndpointDescriptor field */

    pTD->pHCDPipe = pHCDPipe;

    /* Return the pointer of TD */

    return pTD;
    }

/*******************************************************************************
*
* usbOhciAddToFreeTDList - add TD into pipe's free TD list
*
* This routine adds a TD into pipe's free TD list.
*
* RETURNS: TRUE, or FALSE if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOLEAN usbOhciAddToFreeTDList
    (
    pUSB_OHCD_DATA    pHCDData,
    pUSB_OHCD_PIPE    pHCDPipe,
    PUSB_OHCI_TD      pTD
    )
    {
    /* Save the original DMA MAP ID */

    VXB_DMA_MAP_ID          dmaMapId = NULL;
    pUSB_OHCI_MEM_MAP_TABLE pTDMemMap = NULL;
    PUSB_OHCI_TD            pFreeTD = NULL;

    if ((NULL == pHCDData) || (NULL == pHCDPipe) || (NULL == pTD))
        {
        USB_OHCD_ERR("Invalid parameter pHCDData %p,pHCDPipe %p,pTD %p\n",
                     pHCDData, pHCDPipe, pTD, 4, 5, 6);
        return FALSE;
        }


    /* Check if this TD exist in the free TD list or not */

    pFreeTD = pHCDPipe->pFreeTDHead;

    while (pFreeTD != NULL)
        {
        /* If this TD exist in the free TD list just return */

        if (pFreeTD == pTD)
            {
            USB_OHCD_WARN("usbOhciAddToFreeTDList: the TD already in free list\n",
                          1, 2, 3, 4, 5, 6);
            return TRUE;
            }

        pFreeTD = pFreeTD->pHCDNextTransferDescriptor;
        }

    /* Get original DMA MAP ID */

    dmaMapId = pTD->dmaMapId;

    /* Get the memMap */

    pTDMemMap = pTD->pMemMap;

    /* Reinitialize the memory */

    OS_MEMSET(pTD, 0, USB_OHCI_MAX_TD_SIZE);

    /* Copy the DMA MAP ID back */

    pTD->dmaMapId = dmaMapId;

    /* Copy the mmeMap back */

    pTD->pMemMap = pTDMemMap;

    /* Update the next trasnfer descriptor of the TD */

    pTD->pHCDNextTransferDescriptor = pHCDPipe->pFreeTDHead;

    /* Make this TD as the head of the free list */

    pHCDPipe->pFreeTDHead = pTD;

    /* If tail pointer is NULL, this TD becomes the tail pointer */

    if (NULL == pHCDPipe->pFreeTDTail)
        {
        pHCDPipe->pFreeTDTail = pTD;
        pHCDPipe->pFreeTDTail->pHCDNextTransferDescriptor = NULL;
        }

    return TRUE;
    }

/*******************************************************************************
*
* usbOhciFreeRequestTDList - add all TDs of the request into free list.
*
* This routine adds all TDs of the request into free list.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbOhciFreeRequestTDList
    (
    pUSB_OHCD_DATA         pHCDData,
    pUSB_OHCD_PIPE         pHCDPipe,
    pUSB_OHCD_REQUEST_INFO pRequest
    )
    {
    PUSB_OHCI_TD pCurTD = NULL;
    PUSB_OHCI_TD pTempTD = NULL;

    /* Parameters valid check */

    if ((NULL == pHCDData) ||
        (NULL == pHCDPipe) ||
        (NULL == pRequest))
        {
        USB_OHCD_ERR("usbOhciFreeRequestTDList invalid parameter"
                     "pHCDData %p, pHCDPipe %p, pRequest %p\n",
                     (ULONG)pHCDData,
                     (ULONG)pHCDPipe,
                     (ULONG)pRequest,
                     4, 5, 6);
        return;
        }

    pCurTD = pRequest->pTDListHead;
    pTempTD = pRequest->pTDListTail;

    /*
     * Make sure that the "pTDListTail->pHCDNextTransferDescriptor" equal to
     * NULL. The "pTDListTail->pHCDNextTransferDescriptor" is always
     * dummyTD, user must disconnect the link before enter into this routine.
     */

    if (pTempTD != NULL)
        {
        if (pTempTD->pHCDNextTransferDescriptor != NULL)
            {
            USB_OHCD_ERR("usbOhciFreeRequestTDList - pRequest %p "
                         "pTDListTail->pHCDNextTransferDescriptor %p != NULL",
                         (ULONG)pRequest,
                         (ULONG)pTempTD->pHCDNextTransferDescriptor,
                         3, 4, 5, 6);
            pTempTD->pHCDNextTransferDescriptor = NULL;
            }
        }

    while (NULL != pCurTD)
        {
        pTempTD = pCurTD->pHCDNextTransferDescriptor;
        usbOhciAddToFreeTDList(pHCDData, pHCDPipe, pCurTD);
        pCurTD = pTempTD;
        }

   /* Set the request's pTDListHead and pTDListTail to NULL */

   pRequest->pTDListHead = NULL;
   pRequest->pTDListTail = NULL;

   return;
   }

/*******************************************************************************
*
* usbOhciDestroyTDList - Delete a list of TD
*
* This routine deletes a list of TD.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbOhciDestroyTDList
    (
    pUSB_OHCD_DATA pHCDData,
    PUSB_OHCI_TD   pTDListHead
    )
    {
    PUSB_OHCI_TD pCurTD = NULL;
    PUSB_OHCI_TD pTempTD = NULL;

    if ((NULL == pHCDData) || (NULL == pTDListHead))
        {
        USB_OHCD_ERR("Invalid parameter pHCDData %p, pTDListHead %p\n",
                     pHCDData, pTDListHead, 3, 4, 5, 6);
        return;
        }

    pCurTD = pTDListHead;
    while (NULL != pCurTD)
        {
        pTempTD = pCurTD->pHCDNextTransferDescriptor;

        /* Delete TD */
        usbOhciDestroyTD(pHCDData, pCurTD);
        pCurTD = pTempTD;
        }
    return;
    }

/*******************************************************************************
*
* usbOhciCreatePipeStructure - Create a USB_OHCD_PIPE structure
*
* This routine creates a USB_OHCD_PIPE structure.
*
* RETURNS: Pointer of the USB_OHCD_PIPE structure.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL pUSB_OHCD_PIPE usbOhciCreatePipeStructure
    (
    pUSB_OHCD_DATA    pHCDData
    )
    {
    pUSB_OHCD_PIPE                pHCDPipe = NULL;
    VXB_DMA_MAP_ID                dmaMapId = NULL;
    STATUS                        vxbSts;
    pUSB_OHCI_MEM_MAP_TABLE       pEDMemMap = NULL;

    pHCDPipe = vxbDmaBufMemAlloc (pHCDData->pDev,
                                  pHCDData->edDmaTagId,
                                  NULL,
                                  0,
                                  &dmaMapId);

    /* Check if allocation is successful */

    if (pHCDPipe  == NULL)
        {
        USB_OHCD_ERR("usbOhciCreatePipeStructure - "
                     "creating pHCDPipe fail\n",
                     1, 2, 3, 4, 5, 6);
        return NULL;
        }

    /* Zero the memory allocated */

    bzero ((char *)pHCDPipe, USB_OHCI_MAX_ED_SIZE);

    /* Save our MAP ID */

    pHCDPipe->dmaMapId = dmaMapId;

    /*
     * Load the DMA MAP so that we get the correct BUS address
     * to program the hardware
     */

    vxbSts = vxbDmaBufMapLoad (pHCDData->pDev,
                               pHCDData->edDmaTagId,
                               pHCDPipe->dmaMapId,
                               pHCDPipe,
                               USB_OHCI_MAX_ED_SIZE,
                               0);

    if (vxbSts != OK)
        {
        USB_OHCD_ERR("usbOhciCreatePipeStructure - "
                     "loading map fail\n",
                     1, 2, 3, 4, 5, 6);

        /* Free the memory allocated */

        (void) vxbDmaBufMemFree(pHCDData->edDmaTagId, pHCDPipe, pHCDPipe->dmaMapId);

        return NULL;
        }

    if (NULL == (pEDMemMap = OS_MALLOC(sizeof(USB_OHCI_MEM_MAP_TABLE))))
        {
        USB_OHCD_ERR("usbOhciCreatePipeStructure - create memMap fail\n",
                     1, 2, 3, 4, 5, 6);
        (void) vxbDmaBufMemFree(pHCDData->edDmaTagId, pHCDPipe, pHCDPipe->dmaMapId);
        return NULL;
        }
    else
        {
        OS_WAIT_FOR_EVENT(pHCDData->memMapTableEvent, WAIT_FOREVER);
        OS_MEMSET(pEDMemMap, 0, sizeof(USB_OHCI_MEM_MAP_TABLE));
        pEDMemMap->uBusAddr = USB_OHCI_DESC_LO32(pHCDPipe);
        pEDMemMap->pVirtAddr = (PVOID)(pHCDPipe);
        lstAdd((LIST *)(&(pHCDData->edMemMapTableList)),
               (NODE *)&(pEDMemMap->memNode));
        pHCDPipe->pMemMap = pEDMemMap;
        OS_RELEASE_EVENT(pHCDData->memMapTableEvent);
        }

    /*
     * Init the request list of the ED
     * requestList is used to record the requests being transferred
     * freeRequestList is used to record the free requests
     * cancelRequestList is used to record the requests to be canceld
     */

    lstInit(&(pHCDPipe->requestList));
    lstInit(&(pHCDPipe->freeRequestList));
    lstInit(&(pHCDPipe->cancelRequestList));

    return pHCDPipe;
    }

/*******************************************************************************
*
* usbOhciFormEmptyPipe - Get a empty USB_OHCD_PIPE structure
*
* This routine gets a empty USB_OHCD_PIPE structure.
*
* RETURNS: Pointer of the USB_OHCD_PIPE structure
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_OHCD_PIPE usbOhciFormEmptyPipe
    (
    pUSB_OHCD_DATA  pHCDData
    )
    {
    pUSB_OHCD_PIPE  pHCDPipe = NULL;
    pHCDPipe = usbOhciCreatePipeStructure(pHCDData);

    if (pHCDPipe != NULL)
        {
        /* Set pipe's state */

        pHCDPipe->uCurPipeState = USB_OHCD_PIPE_STATE_NORMAL;
        pHCDPipe->uNextPipeState = USB_OHCD_PIPE_STATE_NORMAL;
        pHCDPipe->PipeSynchEventID = OS_CREATE_EVENT(OS_EVENT_SIGNALED);
        if (pHCDPipe->PipeSynchEventID == NULL)
            {
            USB_OHCD_ERR("usbOhciFormEmptyPipe - create PipeSynchEventID fail\n",
                         1, 2, 3, 4, 5, 6);
            usbOhciDestroyPipe(pHCDData, pHCDPipe);
            return NULL;
            }
        }
    return pHCDPipe;
    }

/*******************************************************************************
*
* usbOhciDestroyED - Delete a ED structure
*
* This routine delete a ED structure.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbOhciDestroyPipe
    (
    pUSB_OHCD_DATA  pHCDData,
    pUSB_OHCD_PIPE  pHCDPipe
    )
    {
    pUSB_OHCI_MEM_MAP_TABLE  pEDMemMap = NULL;
    pUSB_OHCI_MEM_MAP_TABLE  pTempMemMap = NULL;
    NODE                     *pNode = NULL;

    pEDMemMap = pHCDPipe->pMemMap;

    if (pEDMemMap != NULL)
        {
        OS_WAIT_FOR_EVENT(pHCDData->memMapTableEvent, WAIT_FOREVER);
        pNode = lstFirst((LIST *)(&(pHCDData->edMemMapTableList)));

        while (pNode != NULL)
            {
            pTempMemMap = MEM_NODE_TO_USB_OHCI_MEM_MAP_TABLE(pNode);

            if (pTempMemMap == pEDMemMap)
                {
                lstDelete((LIST *)(&(pHCDData->edMemMapTableList)),
                          (NODE *)(&(pEDMemMap->memNode)));
                break;
                }
            pNode =  lstNext(pNode);
            }
        OS_RELEASE_EVENT(pHCDData->memMapTableEvent);
        OS_FREE(pEDMemMap);
        pHCDPipe->pMemMap= NULL;
        }

    /* Unload the DMA MAP */

    if (vxbDmaBufMapUnload (pHCDData->edDmaTagId, pHCDPipe->dmaMapId) != OK)
        {
        USB_OHCD_ERR("usbOhciDestroyED - vxbDmaBufMapUnload fail\n",
                     0, 0, 0, 0, 0, 0);

        return ERROR;
        }

    return vxbDmaBufMemFree(pHCDData->edDmaTagId, pHCDPipe, pHCDPipe->dmaMapId);
    }

/*******************************************************************************
*
* usbOhciCleanUpPipe - Clean up USB_OHCD_PIPE resource
*
* This routine cleans up USB_OHCD_PIPE's resource.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/
VOID usbOhciCleanUpPipe
    (
    pUSB_OHCD_DATA  pHCDData,
    pUSB_OHCD_PIPE  pHCDPipe
    )
    {
    if ((NULL == pHCDData) || (NULL == pHCDPipe))
        {
        USB_OHCD_ERR("Invlalid parameter pHCDData %p,pHCDPipe %p \n",
                     pHCDData, pHCDPipe, 0, 0, 0, 0);
        return;
        }

    /*
     * Add dummy TD into free list, usbOhciUnSetupPipe
     * will free all the TD in the free list.
     */

    if (pHCDPipe->pHCDTDTail != NULL)
        {
        usbOhciAddToFreeTDList(pHCDData, pHCDPipe, pHCDPipe->pHCDTDTail);
        pHCDPipe->pHCDTDTail = NULL;
        }

    usbOhciUnSetupPipe(pHCDData, pHCDPipe);

    if (pHCDPipe->PipeSynchEventID != NULL)
        {
        OS_DESTROY_EVENT(pHCDPipe->PipeSynchEventID);
        pHCDPipe->PipeSynchEventID = NULL;
        }

    usbOhciDestroyPipe(pHCDData, pHCDPipe);
    return;
    }

/*******************************************************************************
*
* usbOhciPipeRmAllRequests - add all requests of the pipe into cancel list
*
* This routine adds all requests of the pipe into cancel list.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbOhciPipeRmAllRequests
    (
    pUSB_OHCD_PIPE pHCDPipe
    )
    {
    pUSB_OHCD_REQUEST_INFO  pRequest = NULL;
    NODE                    *pNode =NULL;

    pNode = lstFirst(&(pHCDPipe->requestList));
    while (pNode != NULL)
        {
        pRequest = LIST_NODE_TO_USB_OHCD_REQUEST_INFO(pNode);
		ADD_REQEUEST_INTO_CANCEL_LIST(pHCDPipe, pRequest);
        pNode = lstNext(pNode);
        }
    return;
    }

/*******************************************************************************
*
* usbOhciPipeReturnAll - Call all request's callback function
*
* This routine is used to call all request's callback function.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbOhciPipeReturnAll
    (
    pUSB_OHCD_PIPE pHCDPipe
    )
    {
    pUSB_OHCD_REQUEST_INFO          pRequest = NULL;
    NODE *                          pNode  = NULL;

    pNode = lstFirst(&(pHCDPipe->requestList));

    while (pNode != NULL)
        {
        pRequest = LIST_NODE_TO_USB_OHCD_REQUEST_INFO(pNode);

        /*
         * If there is any real data transfer, we should do
         * vxbDmaBufSync and unload the DMA MAP.
         */

        if (pRequest->uRequestLength != 0)
            {
            /* Unload the Data DMA MAP */

            vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
                pRequest->usrDataDmaMapId);
            }

        /* Control transfer should also deal with the Setup buffer */

        if (pHCDPipe->uEndpointType == USBHST_CONTROL_TRANSFER)
            {
            /* Unload the control Setup DMA MAP */

            vxbDmaBufMapUnload (pHCDPipe->ctrlSetupDmaTagId,
                pRequest->ctrlSetupDmaMapId);
            }

        /*
         * Update Urb's status as USBHST_TRANSFER_CANCELLED
         * and call it's callback routine
         */

        USB_OHCD_CALL_URB_CALLBACK(pRequest->pUrb, USBHST_TRANSFER_CANCELLED);

       /* Update pRequest->pUrb to NULL to indicate it can't be accessed */

        pRequest->pUrb = NULL;

        pNode = lstNext(pNode);
        }
    }

/***************************************************************************
*
* usbOhciModifyTDList - modify transfer descriptor list
*
* This function modifies the transfer descriptor list. If an URB is to
* be deleted, the transfer descriptor list should modified in order to remove
* the transfer descriptors associated with the URB to be deleted.
*
* PARAMETERS:
*
* <uHostControllerIndex> (IN) - Index of the host controller.
* <pEndpointDescriptor (IN)> - Pointer to the endpoint descriptor.
*
* <pPreviousURBListNode (IN)> - Pointer to the previous URB corresponding to the
* URB to be deleted.
*
* <pURBToBeCancelled (IN)> - Pointer to the URB to be deleted.
*
* RETURNS: N/A
*
* ERRNO:
*   None.
*/

LOCAL VOID usbOhciModifyTDList
    (
    UINT32                          uHostControllerIndex,
    pUSB_OHCD_PIPE                  pHCDPipe,
    pUSB_OHCD_REQUEST_INFO          pPrevRequest,
    pUSB_OHCD_REQUEST_INFO          pRequestToBeCancelled
    )
    {
    /* To hold the pointer to the temporary transfer descriptor */

    PUSB_OHCI_TD          pTempTD = NULL;

    /*
     * To hold the pointer the dummy transfer descriptor. The dummy
     * transfer descriptor will always be present at the end of the
     * transfer descriptor list.
     */

    PUSB_OHCI_TD           pDummyTD = NULL;
    pUSB_OHCD_REQUEST_INFO pNextRequest = NULL;
    NODE                   *pNode = NULL;
    UINT32                 uTDHead = 0x0;
    UINT32                 uBufferAddr = 0x0;

    /*
     * NOTE: There is always a dummy transfer descriptor node at the end
     *       of the list. This node cannot be deleted.
     */

    pNode = &(pRequestToBeCancelled->listNode);

    /* Get the next node of the cancel request */

    pNode = lstNext(pNode);

    /* Get the next request of the cancel request */

    if (pNode != NULL)
        pNextRequest = LIST_NODE_TO_USB_OHCD_REQUEST_INFO(pNode);

    /* Check whether the previous element of the URB list is valid */

    if (pPrevRequest == NULL)
        {
        /*
         * Check whether the next element in the URB list following the
         * URB to be deleted is valid.
         */

        if (pNextRequest != NULL)
            {
            /*
             * If the pPrevRequest is NULL and the pNextRequest is not NULL,
             * the request to be deleted is the first request of the URB list.
             * We need update the TDQueueHead pointers use the nexPequest's
             * head TD
             *
             * NOTE: The TDQueueHead also contains the information about the
             *       'ToggleCarry' and 'Halted' bit. Do not update these.
             */

            /*
             * Obtain the pointer to the current head of the transfers
             * descriptor list.
             */

            uTDHead =
                USB_OHCD_SWAP_DATA
                (uHostControllerIndex, (pHCDPipe->uTDQueueHeadPointer));

            /*
             * Clear the contents of the TDQueueHead except for the
             * 'ToggleCarry' and 'Halted' bit
             */

            uTDHead &= USB_OHCI_TD_ALIGN_MASK;

            /* Update the next transfer descriptor head pointer */

            uTDHead |= USB_OHCI_DESC_LO32(pNextRequest->pTDListHead);

            /* Update the pointer to the TDQueueHead */

            pHCDPipe->uTDQueueHeadPointer =
                             USB_OHCD_SWAP_DATA(uHostControllerIndex, uTDHead);
            }
        else
            {
            /*
             * If the pPrevRequest is NULL and the
             * pNextRequest is NULL, the node to be deleted is
             * the first node and also the last node of the URB list.
             */

            /*
             * Obtain the pointer to the last transfer descriptor for this
             * URB.
             */

            pTempTD = pRequestToBeCancelled->pTDListTail;

            /*
             * Obtain the pointer to the transfer descriptor following
             * the last transfer descriptor for the URB to be deleted.
             */

            pDummyTD = pTempTD->pHCDNextTransferDescriptor;

            /*
             * NOTE: The transfer descriptor obtained in the above step
             *       corresponds to the dummy transfer descriptor. Hence,
             *       the next pointer to the dummy transfer descriptor
             *       should be NULL.
             *
             *       If not, it is an error.
             */

            /*
             * Check whether the next pointer is NULL. If not it is an
             * error.
             */

            if (pDummyTD->uNextTDPointer != 0)
                {
                /* Debug message */
                USB_OHCD_ERR("Invalid dummy transfer descriptor pointer \n",
                             1, 2, 3, 4, 5, 6);

                /*
                 * Set the next transfer descriptor to NULL.
                 *
                 * NOTE: This is not a fix, it is a recovery mechanism.
                 */

                pDummyTD->uNextTDPointer = 0;

                /* Update the HCD's next pointer */

                pDummyTD->pHCDNextTransferDescriptor = NULL;
                }

            /*
             * Update the uTDQueueHeadPointer and uTDQueueHeadTail pointers.
             *
             * NOTE: The uTDQueueHeadPointer also contains the information about the
             *       'ToggleCarry' and 'Halted' bit. Do not update these.
             */

            uBufferAddr = USB_OHCI_DESC_LO32(pDummyTD);

            if (pHCDPipe->uEndpointType != USBHST_ISOCHRONOUS_TRANSFER)
                {
                /*
                 * Obtain the pointer to the current head of the transfers
                 * descriptor list.
                 */

                uTDHead = USB_OHCD_SWAP_DATA(uHostControllerIndex,
                                            (pHCDPipe->uTDQueueHeadPointer));

                /*
                 * Clear the contents of the TDQueueHead except for the
                 * 'ToggleCarry' and 'Halted' bit
                 */

                uTDHead &=  USB_OHCI_TD_ALIGN_MASK;

                pHCDPipe->uTDQueueHeadPointer =
                    USB_OHCD_SWAP_DATA(uHostControllerIndex,
                                       (uBufferAddr | uTDHead));
                }
            else
                {
                pHCDPipe->uTDQueueHeadPointer =
                    USB_OHCD_SWAP_DATA(uHostControllerIndex, uBufferAddr);
                }

            /* Update the TDQueueTail pointer for the endpoint */

            pHCDPipe->uTDQueueTailPointer =
                USB_OHCD_SWAP_DATA(uHostControllerIndex, uBufferAddr);

            /* Update the HCD's tail pointer maintained for the endpoint */

            pHCDPipe->pHCDTDTail = pDummyTD;
            }
        }
    else /* Else for if (pPrevRequest == NULL) */
        {
        /*
         * Check whether the next element in the URB list following the
         * URB to be deleted is valid.
         */

        if (pNextRequest != NULL)
            {
            /*
             * If the pPrevRequest is not NULL and the pNextRequest is not null,
             * the request to be deleted is in the middle of the URB list.
             */

            /*
             * Obtain the pointer to the last transfer descriptor corresponding
             * to the URB previous to the URB to be cancelled.
             */

            pTempTD = pPrevRequest->pTDListTail;

            /*
             * Update the transfer descriptor pointers.
             *
             * NOTE: Each TD has a head and tail pointers. Assume that there
             *       are three TDs, TD1, TD2 and TD3.
             *
             *       Tail pointer of TD1 will point to the head pointer of
             *       TD2. Similarly, Tail pointer of TD2 will point to the
             *       head pointer of TD3.
             *
             *       In order to remove TD2, the tail pointer of TD1 should
             *       be updated to point to the head pointer of TD3.
             */

            uBufferAddr = USB_OHCI_DESC_LO32(pNextRequest->pTDListHead);

            pTempTD->uNextTDPointer =
              USB_OHCD_SWAP_DATA(uHostControllerIndex, uBufferAddr);

            /* Update the HCD's next pointer */

            pTempTD->pHCDNextTransferDescriptor = pNextRequest->pTDListHead;
            }
        else
            {
            /*
             * If the pPrevRequest is not NULL and the pNextRequest is NULL,
             * the request to be deleted is the last request in the URB list.
             */

            /* Obtain the pointer of the tail TD for this request */

            pTempTD = pRequestToBeCancelled->pTDListTail;


            /*
             * Obtain the pointer to the transfer descriptor following
             * the last transfer descriptor for the URB to be deleted.
             *
             * NOTE: Since the URB to be deleted is the last URB is the
             *       list of URBs, the transfer descriptor following the
             *       last transfer descriptor for the URB corresponds to
             *       the dummy transfer descriptor.
             */

            /* Obtain the pointer to the dummy transfer descriptor */

            pDummyTD = pTempTD->pHCDNextTransferDescriptor;

            /*
             * NOTE: The transfer descriptor obtained in the above step
             *       corresponds to the dummy transfer descriptor. Hence,
             *       the next pointer to the dummy transfer descriptor
             *       should be NULL.
             *
             *       If not, it is an error.
             */

            /*
             * Check whether the next pointer is NULL. If not it is an
             * error.
             */
            if (pDummyTD->uNextTDPointer != 0)
                {
                /* Invalid dummy transfer descriptor */

                USB_OHCD_ERR("Invalid dummy transfer descriptor pointer \n",
                             1, 2, 3, 4, 5, 6);

                /*
                 * Set the next transfer descriptor to NULL.
                 *
                 * NOTE: This is not a fix, it is a recovery mechanism.
                 */
                pDummyTD->uNextTDPointer = 0;

                /* Update the HCD's next pointer */
                pDummyTD->pHCDNextTransferDescriptor = NULL;
                }

            /*
             * Update the next transfer descriptor pointer of the previous
             * URB in the URB list
             */

            /*
             * Obtain the pointer to the last transfer descriptor corresponding
             * to the previous URB in the URB list.
             */

            pTempTD = pPrevRequest->pTDListTail;

            /*
             * Update the next transfer descriptor pointer of the last transfer
             * descriptor corresponding to the previous URB in the URB list.
             */

            pTempTD->uNextTDPointer =
                USB_OHCD_SWAP_DATA (uHostControllerIndex,
                   USB_OHCI_DESC_LO32 (pDummyTD));

            /* Update the HCD's next pointer */

            pTempTD->pHCDNextTransferDescriptor = pDummyTD;
            }
        }

    return;
    } /* End of function usbOhciModifyTDList () */

/*******************************************************************************
*
* usbOchiCheckRequestPending - Check if there is any request is pending
*
* This routine check if there is any request is pending
*
* RETURNS: TRUE if the request's TDs is still under transfer, or FALSE
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL BOOLEAN usbOchiCheckRequestPending
    (
    pUSB_OHCD_DATA                pHCDData,
    pUSB_OHCD_PIPE                pHCDPipe,
    pUSB_OHCD_REQUEST_INFO        pRequest
    )
    {
    PUSB_OHCI_TD                  pHeadTD = NULL;
    PUSB_OHCI_TD                  pTempTD = NULL;
    UINT32                        uBufferAddr = 0x0;

    /* Get current physical address of the head TD */

    /* Convert into CPU endian */

    uBufferAddr = USB_OHCD_SWAP_DATA(pHCDData->uBusIndex,
                                     pHCDPipe->uTDQueueHeadPointer);

    /* Get the actual bus address in CPU endian */

    uBufferAddr &= (~USB_OHCI_TD_ALIGN_MASK);

    /* Convert to the virtual address */

    pHeadTD = (PUSB_OHCI_TD) usbOhciPhyToVirt(pHCDData,
                                              uBufferAddr,
                                              USB_OHCI_MEM_MAP_TD);
    if (pHeadTD != NULL)
        {
        if (pHeadTD->pRequestInfo == pRequest)
            {
            /* If the current TD belongs to the request */

            if (pHeadTD != pRequest->pTDListHead)
                {
                /* Get the prev TD of the pHeadTD */

                pTempTD = pRequest->pTDListHead;

                while ( pTempTD != NULL )
                    {
                    if (pTempTD->pHCDNextTransferDescriptor == pHeadTD)
                        break;

                    pTempTD = pTempTD->pHCDNextTransferDescriptor;
                    }

                /* Judge all the TD before the pHeadTD is completed */

                if (pTempTD != pHCDPipe->pLastCompletedTD)
                    {
                    USB_OHCD_DBG("There are still some TD in transfer "
                                 "curTD %p,prevTD %p,pTDListHead %p,completeTD %p\n",
                                 pHeadTD,
                                 pTempTD,
                                 pRequest->pTDListHead,
                                 pHCDPipe->pLastCompletedTD, 5, 6);

                    return TRUE;
                    }
                }
            }
        else
            {
            /* If the pipe's pLastCompletedTD != NULL
             * and it equal to the pRequest->pTDListTail, it means the hardware
             * already handle all the TD of the request but waiting to process
             * we should wait the TD to be processed.
             */

            if (pHCDPipe->pLastCompletedTD != NULL)
                {
                if (pHCDPipe->pLastCompletedTD == pRequest->pTDListTail)
                    {
                    USB_OHCD_DBG("Waiting the last TD %p to be processed\n",
                                 pRequest->pTDListTail, 2, 3, 4, 5, 6);
                    return TRUE;
                    }
                }
            }
        }
    return FALSE;
    }


/*******************************************************************************
*
* usbOhciUnlinkRequestFromPipe - Unlink the request from the pipe
*
* This routine unlinke the request from the pipe.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbOhciUnlinkRequestFromPipe
    (
    pUSB_OHCD_DATA                pHCDData,
    pUSB_OHCD_PIPE                pHCDPipe,
    pUSB_OHCD_REQUEST_INFO        pRequest
    )
    {
    pUSB_OHCD_REQUEST_INFO        pPrevRequest = NULL;
    PUSB_OHCI_TD                  pHeadTD = NULL;
    PUSB_OHCI_TD                  pTempTD = NULL;
    NODE                          *pNode  = NULL;
    UINT32                        uBufferAddr = 0x0;
    UINT32                        uBusIndex = 0x0;
    UINT32                        uBufferTransferLength = 0x0;
    UINT32                        uMaxPakSize = 0x0;

    if ((NULL == pHCDData)||
        (NULL == pHCDPipe)||
        (NULL == pRequest))
        {
        USB_OHCD_ERR("Invlalid parameter pHCDData %p,pHCDPipe %p,pRequest %p \n",
                     pHCDData, pHCDPipe, pRequest, 0, 0, 0);
        return ERROR;
        }

    uBusIndex = pHCDData->uBusIndex;

    /* Get the list node of the request to be canceld */

    pNode = &(pRequest->listNode);

    /* Get the prev node of the cancel request */

    pNode = lstPrevious(pNode);

    /* Get prev requst of the request to be cancelled */

    if (pNode != NULL)
        pPrevRequest = LIST_NODE_TO_USB_OHCD_REQUEST_INFO(pNode);

    /* Get current physical address of the head TD */

    /* Convert into CPU endian */

    uBufferAddr = USB_OHCD_SWAP_DATA(uBusIndex, pHCDPipe->uTDQueueHeadPointer);

    /* Get the actual bus address in cPU endian */

    uBufferAddr &= (~USB_OHCI_TD_ALIGN_MASK);

    /* Convert to the virtual address */

    pHeadTD = (PUSB_OHCI_TD)usbOhciPhyToVirt(pHCDData,
                                             uBufferAddr,
                                             USB_OHCI_MEM_MAP_TD);

    if (pHeadTD == NULL)
        {
        USB_OHCD_ERR("Invalid virtual address \n", 1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    if (pHCDPipe->uEndpointType != USBHST_ISOCHRONOUS_TRANSFER)
        {
        if (pHeadTD->pRequestInfo == pRequest)
            {
            if (pHeadTD->uCurrentBufferPointer !=
                USB_OHCD_SWAP_DATA(uBusIndex, pHeadTD->uStartAddressPointer))
                {
                uBufferTransferLength =
                  USB_OHCD_SWAP_DATA(uBusIndex, pHeadTD->uCurrentBufferPointer) -
                  pHeadTD->uStartAddressPointer;

                uMaxPakSize =
                 OHCI_GET_ENDPOINT_MAXIMUM_PACKET_SIZE_FROM_ED(
                                          uBusIndex, pHCDPipe->uControlInformation);
                if (uBufferTransferLength % (2 * uMaxPakSize) != 0)
                    {
                    /* Update the toggle value */
                    if (0 ==
                       OHCI_GET_TOGGLE_CARRY_FROM_ED(uBusIndex, pHCDPipe->uTDQueueHeadPointer))
                        OHCI_SET_ENDPOINT_TOGGLE(uBusIndex, pHCDPipe);
                    else
                        OHCI_RESET_ENDPOINT_TOGGLE(uBusIndex, pHCDPipe);
                    }
                }
            }
        }

    usbOhciModifyTDList (uBusIndex,
                         pHCDPipe,
                         pPrevRequest,
                         pRequest);

    /*
     * Obtain the pointer to the last transfer descriptor corresponding
     * to the transfer.
     */

    pTempTD = (PUSB_OHCI_TD)pRequest->pTDListTail;

    /* Terminate the list of transfers corresponding to the transfer */

    pTempTD->pHCDNextTransferDescriptor = NULL;

    /* Release all the TDs of the request */

    usbOhciFreeRequestTDList(pHCDData,
                             pHCDPipe,
                             pRequest);

    /* Delete the memory allocated for the URB list element */

    usbOhciReleaseRequestInfo(pHCDData, pHCDPipe, pRequest);
    return OK;
    }

/*******************************************************************************
*
* usbOhciUnlinkPipe - Unlink pipe from the schedule list
*
* This routine unlinkes pipe from the schedule list.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/
LOCAL VOID usbOhciUnlinkPipe
    (
    pUSB_OHCD_DATA pHCDData,
    pUSB_OHCD_PIPE pHCDPipe
    )
    {
    pUSB_OHCD_PIPE pHeadPipe = NULL;
    pUSB_OHCD_PIPE pPrevPipe = NULL;
    pUSB_OHCD_PIPE pTempPipe = NULL;
    pUSB_OHCD_PIPE pCurPipe = NULL;
    PUINT32        pInterrupTable = NULL;
    UINT32         uBufferAddr = 0x0;
    UINT32         uBandwidthAvailable = 0x0;
    UINT32         uBandwidthAllocatedForEndpoint = 0x0;
    UINT8          uPollingInterval = 0x0;
    UINT8          uDeviceSpeed = 0x0;
    UINT8          uEndpointDirection = 0x0;
    UINT32         uBusIndex = 0x0;
    UINT32         uIndex = 0x0;

    if ((NULL == pHCDData) ||(NULL == pHCDPipe))
        {
        USB_OHCD_ERR("Invlalid parameter pHCDData %p,pHCDPipe %p\n",
                     pHCDData, pHCDPipe, 0, 0, 0, 0);
        return;
        }

    uBusIndex = pHCDData->uBusIndex;

    /*
     * This routine is called by delete pipe.
     * we should update ED List
     */

    if (pHCDPipe->uEndpointType == USBHST_CONTROL_TRANSFER)
        {
        /* Get the controle Head ED */

        uBufferAddr =
            USB_OHCI_REG_READ(pHCDData->pDev,
                              USB_OHCI_CONTROL_HEAD_ED_REGISTER_OFFSET);

        pHeadPipe = (pUSB_OHCD_PIPE)
                   usbOhciPhyToVirt(pHCDData,
                                    uBufferAddr,
                                    USB_OHCI_MEM_MAP_ED);

        if (pHeadPipe != NULL)
            {
            /* Get PrevED of this ED */

            if (pHeadPipe == pHCDPipe)
                {
                pPrevPipe = NULL;
                }
            else
                {
                pTempPipe = pHeadPipe;
                while ((pTempPipe != NULL) && (pTempPipe != pHCDPipe))
                    {
                    pPrevPipe = pTempPipe;
                    pTempPipe = pTempPipe->pHCDNextPipe;
                    }
                }

            /* Update ED List */

            if (pPrevPipe != NULL)
                {
                pPrevPipe->uNextEDPointer = pHCDPipe->uNextEDPointer;
                pPrevPipe->pHCDNextPipe = pHCDPipe->pHCDNextPipe;
                }

            if (pHCDPipe == pHCDData->pControlPipeListTail)
                {
                pHCDData->pControlPipeListTail = pPrevPipe;
                }

            /* If ED == HeadED, update the HeadED register */

            if (pHeadPipe == pHCDPipe)
                {
                uBufferAddr = USB_OHCI_DESC_LO32(pHCDPipe->pHCDNextPipe);
                USB_OHCI_REG_WRITE(pHCDData->pDev,
                                   USB_OHCI_CONTROL_HEAD_ED_REGISTER_OFFSET,
                                   uBufferAddr);
                }

            /* Get current ED from the hardware */

            uBufferAddr =
                 USB_OHCI_REG_READ (pHCDData->pDev,
                                    USB_OHCI_CONTROL_CURRENT_ED_REGISTER_OFFSET);

            pCurPipe = (pUSB_OHCD_PIPE)
                       usbOhciPhyToVirt(pHCDData,
                                        uBufferAddr,
                                        USB_OHCI_MEM_MAP_ED);

            if (pCurPipe == pHCDPipe)
                {
                /*
                 * Update the HcControlCurrentED register with the
                 * next element in the list of control pipes.
                 */

                uBufferAddr = USB_OHCI_DESC_LO32(pHCDPipe->pHCDNextPipe);

                USB_OHCI_REG_WRITE(pHCDData->pDev,
                                   USB_OHCI_CONTROL_CURRENT_ED_REGISTER_OFFSET,
                                   uBufferAddr);
                }

            }

        }
    else if (pHCDPipe->uEndpointType == USBHST_BULK_TRANSFER)
        {
        uBufferAddr = USB_OHCI_REG_READ(pHCDData->pDev,
                              USB_OHCI_BULK_HEAD_ED_REGISTER_OFFSET);

        pHeadPipe = (pUSB_OHCD_PIPE)
                  usbOhciPhyToVirt(pHCDData,
                                   uBufferAddr,
                                   USB_OHCI_MEM_MAP_ED);

        if (pHeadPipe != NULL)
            {

            /* Get PrevED of this ED */

            if (pHeadPipe == pHCDPipe)
                {
                pPrevPipe = NULL;
                }
            else
                {
                pTempPipe = pHeadPipe;
                while ((pTempPipe != NULL)&&(pTempPipe != pHCDPipe))
                    {
                    pPrevPipe = pTempPipe;
                    pTempPipe = pTempPipe->pHCDNextPipe;
                    }
                }

            /* Update ED List */

            if (pPrevPipe != NULL)
                {
                pPrevPipe->uNextEDPointer = pHCDPipe->uNextEDPointer;
                pPrevPipe->pHCDNextPipe = pHCDPipe->pHCDNextPipe;
                }

            if (pHCDPipe == pHCDData->pBulkPipeListTail)
                {
                pHCDData->pBulkPipeListTail = pPrevPipe;
                }

            if (pHCDPipe == pHeadPipe)
                {
                uBufferAddr = USB_OHCI_DESC_LO32(pHCDPipe->pHCDNextPipe);
                USB_OHCI_REG_WRITE(pHCDData->pDev,
                                   USB_OHCI_BULK_HEAD_ED_REGISTER_OFFSET,
                                   uBufferAddr);
                }

            uBufferAddr = USB_OHCI_REG_READ (pHCDData->pDev,
                                   USB_OHCI_BULK_CURRENT_ED_REGISTER_OFFSET);

            pCurPipe = (pUSB_OHCD_PIPE)
                      usbOhciPhyToVirt(pHCDData,
                                       uBufferAddr,
                                       USB_OHCI_MEM_MAP_ED);;

            if (pCurPipe == pHCDPipe)
                {
                uBufferAddr = USB_OHCI_DESC_LO32(pHCDPipe->pHCDNextPipe);

                USB_OHCI_REG_WRITE (pHCDData->pDev,
                                    USB_OHCI_BULK_CURRENT_ED_REGISTER_OFFSET,
                                    uBufferAddr);
                }
            }
        }
    else
        {
        uPollingInterval = pHCDPipe->uPollingInterval;
        uBandwidthAllocatedForEndpoint =
            (UINT32)OHCI_GET_ED_MPS(uBusIndex,
                                    pHCDPipe->uControlInformation);
        uDeviceSpeed =
            (UINT8)OHCI_GET_DEVICE_SPEED_FROM_ED(uBusIndex,
                                          pHCDPipe->uControlInformation);
        uEndpointDirection =
            (UINT8)OHCI_GET_ENDPOINT_DIRECTION_FROM_ED(uBusIndex,
                                                pHCDPipe->uControlInformation);
        /*
         * Compute the bandwidth allocated for the endpoint. This
         * bandwidth should be released.
         */
        uBandwidthAllocatedForEndpoint =
            USB_OHCI_COMPUTE_BANDWIDTH_FOR_INTERRUPT_ENDPOINT(
                                    uDeviceSpeed,
                                    uEndpointDirection,
                                    uBandwidthAllocatedForEndpoint);
        /*
         * The following sentence want to get the address of
         * pHCDData->pHcca->uHccaInterruptTable
         * But in DIAB compiler, we will get warning as
         * "dangerous to take address of member of packed or swapped structure"
         * The "uHccaInterruptTable" must be the first of the structure
         * So we will get the HCCA address to instead.
         */

        pInterrupTable = (PUINT32)(pHCDData->pHcca);

        for (uIndex = 0; uIndex < USB_OHCI_MAXIMUM_POLLING_INTERVAL; uIndex++)
            {
            uBufferAddr = pInterrupTable[uIndex];

            /* Get the bus address in CPU endian */

            uBufferAddr = USB_OHCD_SWAP_DATA(uBusIndex, uBufferAddr);

            pTempPipe = (pUSB_OHCD_PIPE)
                       usbOhciPhyToVirt(pHCDData,
                                        uBufferAddr,
                                        USB_OHCI_MEM_MAP_ED);
            if (pTempPipe == NULL)
                {
                continue;
                }

            uBandwidthAvailable = pTempPipe->uBandwidthAvailable;

            if (pTempPipe == pHCDPipe)
                {
                pInterrupTable[uIndex] = pHCDPipe->uNextEDPointer;

                /*
                 * Obtain the pointer to the head of the periodic list for
                 * the specified polling interval.
                 */

                uBufferAddr = pInterrupTable[uIndex];

                /* Get the bus address in CPU endian */

                uBufferAddr = USB_OHCD_SWAP_DATA(uBusIndex, uBufferAddr);

                pTempPipe =  (pUSB_OHCD_PIPE)
                            usbOhciPhyToVirt(pHCDData,
                                             uBufferAddr,
                                             USB_OHCI_MEM_MAP_ED);;
                if (pTempPipe != NULL)
                    {
                    pTempPipe->uBandwidthAvailable =
                        uBandwidthAvailable + uBandwidthAllocatedForEndpoint;
                    }
                continue;
                }

            while ((pTempPipe != NULL) &&
                  (pTempPipe->pHCDNextPipe != NULL))
                {
                if (pTempPipe->pHCDNextPipe == pHCDPipe)
                    {
                    pTempPipe->uNextEDPointer = pHCDPipe->uNextEDPointer;
                    pTempPipe->pHCDNextPipe = pHCDPipe->pHCDNextPipe;
                    uBufferAddr = pInterrupTable[uIndex];

                    /* Get the bus address in CPU endian */

                    uBufferAddr = USB_OHCD_SWAP_DATA(uBusIndex, uBufferAddr);

                    pHeadPipe = (pUSB_OHCD_PIPE)
                              usbOhciPhyToVirt(pHCDData,
                                               uBufferAddr,
                                               USB_OHCI_MEM_MAP_ED);;
                    if (pHeadPipe == NULL)
                        {
                        continue;
                        }

                    pHeadPipe->uBandwidthAvailable =
                        uBandwidthAvailable + uBandwidthAllocatedForEndpoint;

                    break;
                    }

                pTempPipe = pTempPipe->pHCDNextPipe;
                }
            }
        }
    return;
    }

/*******************************************************************************
*
* usbOhciCleanDisabledPipe - Clean pipe in disable list
*
* This routine cleans pipe in disable list.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbOhciCleanDisabledPipe
    (
    pUSB_OHCD_DATA pHCDData
    )
    {
    pUSB_OHCD_PIPE                pHCDPipe = NULL;
    pUSB_OHCD_REQUEST_INFO        pRequest = NULL;
    NODE                          *pEDNode = NULL;
    NODE                          *pNextEDNode = NULL;
    NODE                          *pReqeuestNode = NULL;
    NODE                          *pNextReqeuestNode = NULL;

    if (0 == lstCount(&(pHCDData->disableEDList)))
        {
        USB_OHCD_VDBG("Disable ED list is null \n",
                       1, 2, 3, 4, 5, 6);
        return;
        }

    /*
     * Wait for the 'endpoint list access event' before updating the
     * list. This event is important to prevent simultaneous updation of
     * the endpoint descriptor list.
     */

    OS_WAIT_FOR_EVENT(pHCDData->pipeListAccessEvent, WAIT_FOREVER);


    pEDNode = lstFirst(&(pHCDData->disableEDList));

    while (pEDNode != NULL)
        {
        pHCDPipe = DISABLE_NODE_TO_USB_OHCI_ENDPOINT_DESCRIPTOR(pEDNode);

        pNextEDNode = lstNext(pEDNode);

        USB_OHCD_DBG("usbOhciCleanDisabledPipe - disable pipe %p,"
                     " curState %d, nextState %d \n",
                     (ULONG)pHCDPipe,
                     pHCDPipe->uCurPipeState,
                     pHCDPipe->uNextPipeState,
                     4, 5, 6);

        if (pHCDPipe->uCurPipeState == USB_OHCD_PIPE_STATE_DELETE)
            {
            usbOhciUnlinkPipe(pHCDData, pHCDPipe);

            /* Enable the transfer list */

            USB_OHCI_ENABLE_TRANSFER_LIST(pHCDData, pHCDPipe);
            }

        OS_WAIT_FOR_EVENT(pHCDPipe->PipeSynchEventID, WAIT_FOREVER);

        pReqeuestNode = lstFirst(&(pHCDPipe->cancelRequestList));

        while (pReqeuestNode != NULL)
            {
        	pRequest = CANCEL_NODE_TO_USB_OHCD_REQUEST_INFO(pReqeuestNode);

            pNextReqeuestNode = lstNext(pReqeuestNode);

            /*
             * If no request is pending, unlink the request from
             * the ED, update ED's request list
             */

            if (pRequest != NULL)
                {
                if (FALSE == usbOchiCheckRequestPending(pHCDData,
                                                        pHCDPipe,
                                                        pRequest))
                    {
                    usbOhciUnlinkRequestFromPipe(pHCDData,
                                                 pHCDPipe,
                                                 pRequest);

                    }
                }
            pReqeuestNode = pNextReqeuestNode;
            }

        OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

        /*
         * Check if there is any request on the ED, If exist,
         * loop to the next ED. If no request is on the ED,
         * we should delte the ED from the disable list
         * and do some clean action
         */

       USB_OHCD_VDBG("pHCDPipe %p cancel request count = %d\n",
                      pHCDPipe, lstCount(&(pHCDPipe->cancelRequestList)),
                      3, 4, 5, 6);

        if (lstCount(&(pHCDPipe->cancelRequestList)) == 0)
            {

            /*
             * If we intend to delete this pipe remove it from the disable list
             * then do clean up action.
             */

            if (pHCDPipe->uCurPipeState == USB_OHCD_PIPE_STATE_DELETE)
                {
                USB_OHCI_REMOVE_PIPE_FROM_DISABLE_LIST(pHCDData, pHCDPipe);

                /*
                 * This routine is called by delete pipe.
                 * Remove the ED from disalbe list
                 * Release all the ED resources
                 */

                usbOhciCleanUpPipe(pHCDData, pHCDPipe);

                pHCDPipe = NULL;
                }

            /* The pipe complete a cancel or reset action */

            if (pHCDPipe != NULL)
                {
                /*
                 * if uNextPipeState == USB_OHCD_PIPE_STATE_NORMAL
                 * remove the pipe from disable list clean the skip bit.
                 * Or else there is still another clean request of this pipe.
                 * We will keep this pipe in the disable list and update
                 * it's state, in next loop the clean request will be treated.
                 */

                if (pHCDPipe->uNextPipeState == USB_OHCD_PIPE_STATE_NORMAL)
                    {
                    USB_OHCI_REMOVE_PIPE_FROM_DISABLE_LIST(pHCDData, pHCDPipe);

                    /*
                     * Set uCurPipeState to USB_OHCD_PIPE_STATE_NORMAL to
                     * indicate the pipe is complete the clean request
                     */

                    pHCDPipe->uCurPipeState = USB_OHCD_PIPE_STATE_NORMAL;

                    /*
                     * We only need to canncel some TD from the ED
                     * So we should let this ED can be re-schedule
                     * Clear the SKIP bit for the endpoint
                     */

                    OHCI_CLEAR_ED_SKIP_BIT(pHCDData->uBusIndex,
                                           pHCDPipe->uControlInformation);
                    }
                else
                    {
                    USB_OHCD_DBG("usbOhciCleanDisabledPipe - "
                                 "Pipe %p nextState %d != NORMAL "
                                 "will clean in next loop\n",
                                 (ULONG)pHCDPipe,
                                 pHCDPipe->uNextPipeState,
                                 3, 4, 5, 6);

                    /*
                     * We still have another clean request of this pipe.
                     * Update uCurPipeState and uNextPipeState let next
                     * loop to deal with the clean request of this pipe
                     */

                    pHCDPipe->uCurPipeState = pHCDPipe->uNextPipeState;
                    pHCDPipe->uNextPipeState = USB_OHCD_PIPE_STATE_NORMAL;
                    }
                }

            }

        /* Loop to the next ED */

        pEDNode = pNextEDNode;
        } /* End of while (NULL != pEndpointDescriptor) */

    USB_OHCD_VDBG("disable ed count = %d\n",
                  lstCount(&pHCDData->disableEDList), 2, 3, 4, 5, 6);

    /*
     * Release the 'endpoint list access event' before updating the
     * list. This event is important to prevent simultaneous updation of
     * the endpoint descriptor list.
     */

    OS_RELEASE_EVENT(pHCDData->pipeListAccessEvent);
    return;
    }

