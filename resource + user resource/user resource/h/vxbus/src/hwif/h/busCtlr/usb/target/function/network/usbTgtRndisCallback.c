/* usbTgtRndisCallback.c - USB Remote NDIS Callback Module */

/*
 * Copyright (c) 2011-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
Modification history
--------------------
01g,06may13,s_z  Remove complier warning (WIND00356717)
01f,18may12,s_z  Add support for USB 3.0 target (WIND00326012)
01e,11apr11,s_z  Add Open Specifications Documentation description
01d,28mar11,s_z  Call usbTgtRndisAllPipesDelete when got reset event
01c,22mar11,s_z  Code clean up based on the code review
01b,09mar11,s_z  Code clean up
01a,04jan11,s_z  written
*/

/*
DESCRIPTION

This module is the USB remote NDIS callback module used for control channel.

This RNDIS function driver follows the [MS-RNDIS]:Remote Network Driver 
Interface Specification (RNDIS) Protocol Specification 1.0, which is one of the
Open Specifications Documentation.

Following is the Intellectual Property Rights Notice for Open Specifications 
Documentation:

\h Intellectual Property Rights Notice for Open Specifications Documentation

\h Technical Documentation. 

Microsoft publishes Open Specifications documentation for protocols, 
file formats, languages, standards as well as overviews of the interaction 
among each of these technologies.

\h Copyrights. 

This documentation is covered by Microsoft copyrights. Regardless of any other 
terms that are contained in the terms of use for the Microsoft website that
hosts this documentation, you may make copies of it in order to develop 
implementations of the technologies described in the Open Specifications 
and may distribute portions of it in your implementations using these 
technologies or your documentation as necessary to properly document the 
implementation. You may also distribute in your implementation, with or without 
modification, any schema, IDL's, or code samples that are included in the 
documentation. This permission also applies to any documents that are 
referenced in the Open Specifications.

\h No Trade Secrets.

Microsoft does not claim any trade secret rights in this documentation.

\h Patents.

Microsoft has patents that may cover your implementations of the technologies 
described in the Open Specifications. Neither this notice nor Microsoft's 
delivery of the documentation grants any licenses under those or any other 
Microsoft patents. However, a given Open Specification may be covered by 
Microsoft's Open Specification Promise (available here: 
http://www.microsoft.com/interop/osp) or the Community Promise 
(available here: http://www.microsoft.com/interop/cp/default.mspx).
If you would prefer a written license, or if the technologies described in 
the Open Specifications are not covered by the Open Specifications Promise 
or Community Promise, as applicable, patent licenses are available by 
contacting iplg@microsoft.com.

\h Trademarks. 

The names of companies and products contained in this 
documentation may be covered by trademarks or similar intellectual 
property rights. This notice does not grant any licenses under those rights.

\h Fictitious Names.

The example companies, organizations, products, 
domain names, e-mail addresses, logos, people, places, and events depicted in 
this documentation are fictitious. No association with any real company, 
organization, product, domain name, email address, logo, person, place, or 
event is intended or should be inferred.

\h Reservation of Rights. 

All other rights are reserved, and this notice does 
not grant any rights other than specifically described above, whether by 
implication, estoppel, or otherwise.

\h Tools. 

The Open Specifications do not require the use of Microsoft 
programming tools or programming environments in order for you to develop an 
implementation. If you have access to Microsoft programming tools and 
environments you are free to take advantage of them. Certain Open 
Specifications are intended for use in conjunction with publicly available 
standard specifications and network programming art, and assumes that the 
reader either is familiar with the aforementioned material or has immediate 
access to it.

This module provides one <'USB_TARG_CALLBACK_TABLE'> pointer to the RNDIS 
initializationmodule which used to process all the requests and hardware events
from the TCD controller.

This file provice the following routines in the table.

/cs
USB_TARG_CALLBACK_TABLE gUsbTgtRndisCallbackTable =
    {
    usbTgtRndisMngmtFunc,                    /@ mngmtFunc @/
    usbTgtRndisFeatureClear,                 /@ featureClear @/
    usbTgtRndisFeatureSet,                   /@ featureSet @/
    NULL,                                    /@ configurationGet @/
    usbTgtRndisConfigSet,                    /@ configurationSet @/
    usbTgtRndisDescriptorGet,                /@ descriptorGet @/
    NULL,                                    /@ descriptorSet @/
    usbTgtRndisInterfaceGet,                 /@ interfaceGet @/
    usbTgtRndisInterfaceSet,                 /@ interfaceSet @/
    NULL,                                    /@ statusGet @/
    usbTgtRndisAddressSet,                   /@ addressSet @/
    NULL,                                    /@ synchFrameGet @/
    usbTgtRndisVendorSpecific,               /@ vendorSpecific @/
    };
/ce

INCLUDE FILES:  usb/usbCdc.h usb/usbTgt.h usb/usbTgtRndis.h 
                usbTgtFunc.h


*/

/* includes */

#include <usb/usbCdc.h>
#include <usb/usbTgt.h>
#include <usb/usbTgtRndis.h>
#include <usbTgtFunc.h>

/* externs */

IMPORT void usbTgtRndisMsgProcessCallback
    (
    void * pErpCallback
    );

IMPORT void usrUsbTgtRndisAttachCallback
    (
    void
    );

IMPORT void usrUsbTgtRndisDetachCallback
    (
    void
    );
IMPORT int usrUsbTgtNetworkDataPoolSizeGet
    (
    void
    );

/* forward declarations */

LOCAL STATUS usbTgtRndisMngmtFunc
    (
    pVOID            param,       
    USB_TARG_CHANNEL targChannel, 
    UINT16           mngmtCode,   
    pVOID            pContext     
    );

LOCAL STATUS usbTgtRndisFeatureClear
    (
    pVOID            param,
    USB_TARG_CHANNEL targChannel,
    UINT8            requestType,
    UINT16           feature,
    UINT16           index
    );

LOCAL STATUS usbTgtRndisFeatureSet
    (
    pVOID            param,       
    USB_TARG_CHANNEL targChannel, 
    UINT8            requestType, 
    UINT16           feature,     
    UINT16           index        
    );

LOCAL STATUS usbTgtRndisConfigSet
    (
    pVOID            param,        
    USB_TARG_CHANNEL targChannel,  
    UINT8            configuration 
    );

LOCAL STATUS usbTgtRndisDescriptorGet
    (
    pVOID            param,          
    USB_TARG_CHANNEL targChannel,    
    UINT8            requestType,    
    UINT8            descriptorType, 
    UINT8            descriptorIndex,
    UINT16           languageId,     
    UINT16           length,         
    pUINT8           pBfr,           
    pUINT16          pActLen         
    );

LOCAL STATUS usbTgtRndisInterfaceGet
    (
    pVOID            param,            
    USB_TARG_CHANNEL targChannel,      
    UINT16           interfaceIndex,   
    pUINT8           pAlternateSetting 
    );

LOCAL STATUS usbTgtRndisInterfaceSet
    (
    pVOID            param,           
    USB_TARG_CHANNEL targChannel,     
    UINT16           interfaceIndex,  
    UINT8            alternateSetting 
    );

LOCAL STATUS usbTgtRndisVendorSpecific
    (
    pVOID               param,          
    USB_TARG_CHANNEL    targChannel,    
    UINT8               requestType,    
    UINT8               request,        
    UINT16              value,          
    UINT16              index,          
    UINT16              length          
    );

/* globals */

USB_TARG_CALLBACK_TABLE gUsbTgtRndisCallbackTable =
    {
    usbTgtRndisMngmtFunc,                    /* mngmtFunc */
    usbTgtRndisFeatureClear,                 /* featureClear */
    usbTgtRndisFeatureSet,                   /* featureSet */
    NULL,                                    /* configurationGet */
    usbTgtRndisConfigSet,                    /* configurationSet */
    usbTgtRndisDescriptorGet,                /* descriptorGet */
    NULL,                                    /* descriptorSet */
    usbTgtRndisInterfaceGet,                 /* interfaceGet */
    usbTgtRndisInterfaceSet,                 /* interfaceSet */
    NULL,                                    /* statusGet */
    NULL,                                    /* addressSet */
    NULL,                                    /* synchFrameGet */
    usbTgtRndisVendorSpecific,               /* vendorSpecific */
    };


/*******************************************************************************
*
* usbTgtRndisAllPipesDelete - delete all the pipes which used by the RNDIS dev
*
* This routine deletes all the pipes which used by the RNDIS dev.
*
* NOTE: This routine is only used as one sub-routine by <'usbTgtRndisConfigSet'>
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtRndisAllPipesDelete
    (
    pUSBTGT_RNDIS pUsbTgtRndis
    )
    {

    /* Parameter validation */
    
    if (NULL == pUsbTgtRndis)
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         "pUsbTgtRndis", 2, 3, 4, 5, 6);

        return ERROR;
        }

    USBTGT_RNDIS_DBG("usbTgtRndisUnConfig(): deletes all the pipes of RNDIS dev\n",
                     1, 2, 3, 4, 5, 6);

    /* Delete the interrup pipe */

    if (pUsbTgtRndis->intInPipeHandle != NULL)
        {
        usbTgtDeletePipe(pUsbTgtRndis->intInPipeHandle);
        pUsbTgtRndis->intInPipeHandle = NULL;
        }

    /* Delete the bulk in pipe */

    if (pUsbTgtRndis->bulkInPipeHandle != NULL)
        {
        usbTgtDeletePipe(pUsbTgtRndis->bulkInPipeHandle);
        pUsbTgtRndis->bulkInPipeHandle = NULL;
        }

    /* Delete the bulk out pipe */

    if (pUsbTgtRndis->bulkOutPipeHandle != NULL)
        {
        usbTgtDeletePipe(pUsbTgtRndis->bulkOutPipeHandle);
        pUsbTgtRndis->bulkOutPipeHandle = NULL;
        }

    return OK;
    }


/*******************************************************************************
*
* usbTgtRndisAllPipesCreate - create all the pipes of RNDIS dev to enable the 
* RNDIS function.
*
* This routine creates all the pipes of RNDIS dev to enable the 
* RNDIS function.
*
* NOTE:before create the pipe, this routine will get the right endpoint 
* descriptor from the TML.Since when the function driver registered to the TML
* by the routine <'usbTgtFuncAdd'>. It only claims that, the function driver 
* need several endpoints defined by the endpoint descriptor. And the TML has 
* the responsiblity to asign a usable hardware endpoint from the TCD hardware.
* And the asigned endpoint may have different endpoint index with the original
* endpoint index which claimed. So, to get the right endpoint descriptor before
* used by the function driver such as creating a pipe for an endpoint, 
* 
* NOTE: This routine is only used as one sub-routine by <'usbTgtRndisConfigSet'>
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtRndisAllPipesCreate
    (
    pUSBTGT_RNDIS       pUsbTgtRndis,
    UINT8               uConfig
    )
    {
    pUSB_ENDPOINT_DESCR pEpDesc = NULL;
    USBTGT_PIPE_CONFIG_PARAM ConfigParam;
    UINT8               uIfBase;
    
    if (NULL == pUsbTgtRndis)
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         "pUsbTgtRndis", 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* 
     * This function driver may be part of one compiste device 
     * Get the interface base.
     */
     
    if ( ERROR == usbTgtFuncInfoMemberGet(pUsbTgtRndis->targChannel,
                                     USBTGT_FUNC_INFO_MEM_TYPE_uIfNumMin,
                                     &uIfBase))
        {
        USBTGT_RNDIS_ERR("Can not get the interface base number\n",
                         1, 2, 3, 4, 5, 6);

        return ERROR;
        }
    
    /* Find the right endpoint descriptor */
    
    pEpDesc = usbTgtFuncEndpointDescFind (pUsbTgtRndis->targChannel,
                          uIfBase,            /* Comm interface */
                          0,                  /* Altsetting 0 */
                          USB_ATTR_INTERRUPT, /* Interrupt */
                          USB_ENDPOINT_IN,    /* IN */
                          0,                  /* Synch Type */
                          0,                  /* Usage */
                          1);                 /* The first one */

    /* Create the pipe for the endpoint */
    
    if (usbTgtCreatePipe (pUsbTgtRndis->targChannel, 
                          pEpDesc,
                          uConfig, 
                          uIfBase,
                          0,
                          &pUsbTgtRndis->intInPipeHandle) != OK)
        {
        USBTGT_RNDIS_ERR("Create for interrupt pipe failed.\n",
                          1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Set the configuration parameters */
    
    ConfigParam.uMaxErpCount = 1;
    ConfigParam.uMaxErpBufCount = 1;

    /* The notification buffer size is 8 */
    
    ConfigParam.uMaxErpBufSize = 8;
    
    /* Configure the pipe */

    if (ERROR ==  usbTgtConfigPipe(pUsbTgtRndis->intInPipeHandle, 
                                   &ConfigParam))
        {
        USBTGT_RNDIS_ERR("Configure for interrupt pipe failed.\n",
                       1, 2, 3, 4, 5, 6);

        usbTgtRndisAllPipesDelete(pUsbTgtRndis);

        return ERROR;
        }

    /* Find the right endpoint descriptor */
    
    pEpDesc = usbTgtFuncEndpointDescFind (pUsbTgtRndis->targChannel,
                          (UINT8) (uIfBase + 1), /* Data interface */
                          0,                     /* Altsetting 0 */
                          USB_ATTR_BULK,         /* BULK */
                          USB_ENDPOINT_IN,       /* IN */
                          0,                     /* Synch Type */
                          0,                     /* Usage */
                          1);                    /* The first one */

    /* Create the pipe for the endpoint */
    
    if (usbTgtCreatePipe (pUsbTgtRndis->targChannel, 
                          pEpDesc,
                          uConfig, 
                          uIfBase,
                          0,
                          &pUsbTgtRndis->bulkInPipeHandle) != OK)
        {
        USBTGT_RNDIS_ERR("Create for interrupt pipe failed.\n",
                          1, 2, 3, 4, 5, 6);

        usbTgtRndisAllPipesDelete(pUsbTgtRndis);

        return ERROR;
        }

    /* Set the configuration parameters */
    
    ConfigParam.uMaxErpCount = 1;
    ConfigParam.uMaxErpBufCount = 1;

    /* The max buffer size is data pool size + sizeof(RNDIS_PACKET_MSG) */
    
    ConfigParam.uMaxErpBufSize = (UINT32) (usrUsbTgtNetworkDataPoolSizeGet() + 
                                           sizeof (RNDIS_PACKET_MSG));
    
    /* Configure the pipe */

    if (ERROR ==  usbTgtConfigPipe(pUsbTgtRndis->bulkInPipeHandle, 
                                   &ConfigParam))
        {
        USBTGT_RNDIS_ERR("Configure for interrupt pipe failed.\n",
                          1, 2, 3, 4, 5, 6);

        usbTgtRndisAllPipesDelete(pUsbTgtRndis);

        return ERROR;
        }

    /* Find the right endpoint descriptor */
    
    pEpDesc = usbTgtFuncEndpointDescFind (pUsbTgtRndis->targChannel,
                          (UINT8) (uIfBase + 1), /* Data interface */
                          0,                     /* Altsetting 0 */
                          USB_ATTR_BULK,         /* BULK */
                          USB_ENDPOINT_OUT,      /* OUT */
                          0,                     /* Synch Type */
                          0,                     /* Usage */
                          1);                    /* The first one */

    /* Create the pipe for the endpoint */
    
    if (usbTgtCreatePipe (pUsbTgtRndis->targChannel, 
                          pEpDesc,
                          uConfig, 
                          uIfBase,
                          0,
                          &pUsbTgtRndis->bulkOutPipeHandle) != OK)
        {
        USBTGT_RNDIS_ERR("Create for interrupt pipe failed.\n",
                          1, 2, 3, 4, 5, 6);

        usbTgtRndisAllPipesDelete(pUsbTgtRndis);

        return ERROR;
        }

    /* Set the configuration parameters */
    
    ConfigParam.uMaxErpCount = 1;
    ConfigParam.uMaxErpBufCount = 1;

    /* The max buffer size is data pool size + sizeof(RNDIS_PACKET_MSG) */
    
    ConfigParam.uMaxErpBufSize = (UINT32) (usrUsbTgtNetworkDataPoolSizeGet() + 
                                           sizeof (RNDIS_PACKET_MSG));
    
    /* Configure the pipe */

    if (ERROR ==  usbTgtConfigPipe(pUsbTgtRndis->bulkOutPipeHandle, 
                                   &ConfigParam))
        {
        USBTGT_RNDIS_ERR("Configure for interrupt pipe failed.\n",
                          1, 2, 3, 4, 5, 6);

        usbTgtRndisAllPipesDelete(pUsbTgtRndis);

        return ERROR;
        }

    USBTGT_RNDIS_DBG("Create all the pipes with interrupt %p, bulk in %p, "
                     " bulk out %p\n",
                     pUsbTgtRndis->intInPipeHandle,
                     pUsbTgtRndis->bulkInPipeHandle, 
                     pUsbTgtRndis->bulkOutPipeHandle, 4, 5, 6);
    return OK;
    }

/*******************************************************************************
*
* usbTgtRndisMngmtFunc - handle the management events 
*
* This routine handles the management events of the rndis function device.Such 
* as Reset/Disconnect etc events which get from the target management level.
*
* NOTE: we used the <'param'> to record the <'pUSBTGT_RNDIS'> pointer, and 
* also have the <'targChannel'> parameter even no used to be compatible with
* the USB GEN1 target stack.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtRndisMngmtFunc
    (
    pVOID            param,       /* TCD Specic parameter */
    USB_TARG_CHANNEL targChannel, /* Target channel */
    UINT16           mngmtCode,   /* Management code */
    pVOID            pContext     /* Context value */
    )
    {
    pUSBTGT_RNDIS       pUsbTgtRndis = (pUSBTGT_RNDIS)param;
    
    /* pUSBTGT_DEVICE_INFO pDeviceInfo = NULL; */

    if (NULL == pUsbTgtRndis)
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         "pUsbTgtRndis", 2, 3, 4, 5, 6);

        return ERROR;
        }
    
    switch (mngmtCode)
        {
        case TARG_MNGMT_ATTACH:
        case USBTGT_NOTIFY_ATTACH_EVENT:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMngmtFunc TARG_MNGMT_ATTACH\n",
                             1, 2, 3, 4, 5 ,6);
            
            pUsbTgtRndis->targChannel = targChannel;
            
            /* 
             * Call the user interface to notify the function driver 
             * has been attached to the TCD 
             */
             
            usrUsbTgtRndisAttachCallback();
            }            
            break;
        case TARG_MNGMT_DETACH:
        case USBTGT_NOTIFY_DETACH_EVENT:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMngmtFunc TARG_MNGMT_DETACH\n",
                              1, 2, 3, 4, 5 ,6);
            
            /* Delete all the pipes expect Ep 0*/
            
            usbTgtRndisAllPipesDelete(pUsbTgtRndis);            

            /* 
             * Call the user interface to notify the function driver 
             * has been detached from the TCD 
             */

            usrUsbTgtRndisDetachCallback();
            }
            break;
        case TARG_MNGMT_BUS_RESET:
        case USBTGT_NOTIFY_RESET_EVENT:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMngmtFunc TARG_MNGMT_BUS_RESET\n",
                              1, 2, 3, 4, 5 ,6);

            /* Copy the operating speed of the device */

            pUsbTgtRndis->uSpeed = (UINT32)(((ULONG)pContext) & 0xFFFFFFFF);

            /* Reset the Device to the default status */

            pUsbTgtRndis->uConfigurationValue = 0;
            pUsbTgtRndis->uAltSetting = 0;
            pUsbTgtRndis->rndisState = RNDIS_UNINITIALIZED;

            /* Delete all the pipes expect Ep 0*/
            
            usbTgtRndisAllPipesDelete(pUsbTgtRndis);            
            }
            break;
        case TARG_MNGMT_DISCONNECT:
        case USBTGT_NOTIFY_DISCONNECT_EVENT:
            {
            USBTGT_RNDIS_DBG("usbTgtRndisMngmtFunc TARG_MNGMT_DISCONNECT\n",
                              1, 2, 3, 4, 5 ,6);
            
            pUsbTgtRndis->uConfigurationValue = 0;
            pUsbTgtRndis->uAltSetting = 0;
            pUsbTgtRndis->rndisState = RNDIS_UNINITIALIZED;

            /* Delete all the pipes expect Ep 0*/
            
            usbTgtRndisAllPipesDelete(pUsbTgtRndis);
            
            }            
            break;
        case TARG_MNGMT_SUSPEND:
        case USBTGT_NOTIFY_SUSPEND_EVENT:

            USBTGT_RNDIS_DBG("usbTgtRndisMngmtFunc TARG_MNGMT_SUSPEND\n",
                          1, 2, 3, 4, 5 ,6);

            break;
        case TARG_MNGMT_RESUME:
        case USBTGT_NOTIFY_RESUME_EVENT:

            USBTGT_RNDIS_DBG("usbTgtRndisMngmtFunc TARG_MNGMT_RESUME\n",
                          1, 2, 3, 4, 5 ,6);

            break;
        default:

            USBTGT_RNDIS_DBG("usbTgtRndisMngmtFunc Unknown\n",
                          1, 2, 3, 4, 5 ,6);

            break;
        }
    
    USBTGT_RNDIS_DBG("usbTgtRndisMngmtFunc Done.\n",
                     1, 2, 3, 4, 5 ,6);
    return OK;
    }


/*******************************************************************************
*
* usbTgtRndisFeatureClear - handle the feature clear request
*
* This routine handles the Feature Clear request get from the target
* management level.This routine will only mark the pipe status with 
* <'USBTGT_PIPE_STATUS_UNSTALL'>
*
* NOTE: we used the <'param'> to record the <'pUSBTGT_RNDIS'> pointer, and 
* also have the <'targChannel'> parameter even no used to be compatible with
* the USB GEN1 target stack.
*
* TODO: Some testing need to do, when the pipe still using when feature clear
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtRndisFeatureClear
    (
    pVOID            param,
    USB_TARG_CHANNEL targChannel,
    UINT8            requestType,
    UINT16           feature,
    UINT16           index
    )
    {
    pUSBTGT_RNDIS       pUsbTgtRndis = (pUSBTGT_RNDIS)param;
    USB_TARG_PIPE pipeHandle = NULL;
    UINT8         uInterface;
    UINT8         uSuspendOp;
    
    /* Validate parameters */

    if (NULL == pUsbTgtRndis)
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         "pUsbTgtRndis", 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* this request must be standard request from the host */

    if (((requestType & USB_RT_DIRECTION_MASK) != USB_RT_HOST_TO_DEV) ||
        ((requestType & USB_RT_CATEGORY_MASK) != USB_RT_STANDARD))
        {
        USBTGT_RNDIS_ERR("Clear Feature should be a standard from the host \n",
                         1, 2, 3, 4, 5, 6);
        
        return ERROR;
        }

    requestType = (UINT8) (requestType & (~(USB_RT_DIRECTION_MASK | USB_RT_CATEGORY_MASK)));

    if ((requestType == USB_RT_ENDPOINT) &&
        (feature == USB_FSEL_DEV_ENDPOINT_HALT))
        {
        USBTGT_RNDIS_DBG("Clear ENDPOINT_HALT Feature Ep index 0x%X\n",
                         index, 2, 3, 4, 5, 6);

        pipeHandle = usbTgtFuncFindPipeHandleByEpIndex(pUsbTgtRndis->targChannel,
                                                       (UINT8)(index));

        /* Update the status for the right pipe */
        
        if (pipeHandle == pUsbTgtRndis->intInPipeHandle)
            {
            pUsbTgtRndis->uIntInPipeStatus = USBTGT_PIPE_STATUS_UNSTALL;
            }
        else if (pipeHandle == pUsbTgtRndis->bulkOutPipeHandle)
            {
            pUsbTgtRndis->uBulkOutPipeStatus = USBTGT_PIPE_STATUS_UNSTALL;
            }
        else if (pipeHandle == pUsbTgtRndis->bulkInPipeHandle)
            {
            pUsbTgtRndis->uBulkInPipeStatus = USBTGT_PIPE_STATUS_UNSTALL;
            }
        else
            {
            USBTGT_RNDIS_WARN("No find the right pipe for ep %x\n",
                              index, 2, 3, 4, 5, 6);
            
            return ERROR;
            }
        }
    else if ((requestType == USB_RT_INTERFACE) && 
             (feature == USB_FSEL_DEV_FUNCTION_SUSPEND))
        {
        /* The MSB is the specific suspend options */
        
        uSuspendOp = (UINT8) (index >> 8);
        uInterface = (UINT8) (index & 0xFF);
        
        USBTGT_RNDIS_DBG ("Clear interface %d suspendop %x\n",
                           uInterface, uSuspendOp, 3, 4, 5, 6);
        
        return OK;
        }

    USBTGT_RNDIS_VDBG("Clear Feature for EpIndex 0x%X for pipe 0x%X\n",
                      index, pipeHandle, 3, 4, 5, 6);

    return OK;
    }


/*******************************************************************************
*
* usbTgtRndisFeatureSet - handle the feature set request
*
* This routine handles the Feature Set request get from the target
* management level.
* This routine will only mark the pipe status with <'USBTGT_PIPE_STATUS_STALL'>
*
* NOTE: we used the <'param'> to record the <'pUSBTGT_RNDIS'> pointer, and 
* also have the <'targChannel'> parameter even no used to be compatible with
* the USB GEN1 target stack.
*
* TODO: Some testing need to do, when the pipe still using when feature set
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtRndisFeatureSet
    (
    pVOID            param,       /* TCD specific parameter */
    USB_TARG_CHANNEL targChannel, /* target channel */
    UINT8            requestType, /* request type */
    UINT16           feature,     /* feature to set */
    UINT16           index        /* wIndex */
    )
    {
    pUSBTGT_RNDIS pUsbTgtRndis = (pUSBTGT_RNDIS)param;
    USB_TARG_PIPE pipeHandle = NULL;
    UINT8         uInterface;
    UINT8         uSuspendOp;

    /* Validate parameters */
    
    if (NULL == pUsbTgtRndis) 
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         "pUsbTgtRndis", 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* this request must be standard request from the host */

    if (((requestType & USB_RT_DIRECTION_MASK) != USB_RT_HOST_TO_DEV) ||
        ((requestType & USB_RT_CATEGORY_MASK) != USB_RT_STANDARD))
        {
        USBTGT_RNDIS_ERR("Clear Feature should be a standard from the host \n",
                         1, 2, 3, 4, 5, 6);
        
        return ERROR;
        }

    requestType = (UINT8) (requestType & (~(USB_RT_DIRECTION_MASK | USB_RT_CATEGORY_MASK)));

    if ((requestType == USB_RT_ENDPOINT) &&
        (feature == USB_FSEL_DEV_ENDPOINT_HALT))
        {
        USBTGT_RNDIS_DBG("Clear ENDPOINT_HALT Feature Ep index 0x%X\n",
                         index, 2, 3, 4, 5, 6);

        pipeHandle = usbTgtFuncFindPipeHandleByEpIndex(pUsbTgtRndis->targChannel,
                                                       (UINT8)(index));

        /* Update the status for the right pipe */

        if (pipeHandle == pUsbTgtRndis->intInPipeHandle)
            {
            pUsbTgtRndis->uIntInPipeStatus = USBTGT_PIPE_STATUS_STALL;
            }
        else if (pipeHandle == pUsbTgtRndis->bulkOutPipeHandle)
            {
            pUsbTgtRndis->uBulkOutPipeStatus = USBTGT_PIPE_STATUS_STALL;
            }
        else if (pipeHandle == pUsbTgtRndis->bulkInPipeHandle)
            {
            pUsbTgtRndis->uBulkInPipeStatus = USBTGT_PIPE_STATUS_STALL;
            }
        else
            {
            USBTGT_RNDIS_WARN("No find the right pipe for ep %x\n",
                              index, 2, 3, 4, 5, 6);
            
            return ERROR;
            }
        }
    else if ((requestType == USB_RT_INTERFACE) && 
             (feature == USB_FSEL_DEV_FUNCTION_SUSPEND))
        {
        /* The MSB is the specific suspend options */
        
        uSuspendOp = (UINT8) (index >> 8);
        uInterface = (UINT8) (index & 0xFF);
        
        USBTGT_RNDIS_DBG ("Set interface %d suspendop %x\n",
                          uInterface, uSuspendOp, 3, 4, 5, 6);
        
        return OK;
        }

    USBTGT_RNDIS_VDBG("Set Feature for EpIndex 0x%X for pipe 0x%X\n",
                      index, pipeHandle, 3, 4, 5, 6);

    return OK;
    }

/*******************************************************************************
*
* usbTgtRndisConfigSet - handle the configuration set request 
*
* This routine handles the Config Set request get from the target
* management level. This routine is called to let the function driver to know
* the configuration has been set.And the function driver can create its pipes
* to enable the fucntion driver for using.
*
* NOTE: we used the <'param'> to record the <'pUSBTGT_RNDIS'> pointer, and 
* also have the <'targChannel'> parameter even no used to be compatible with
* the USB GEN1 target stack.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtRndisConfigSet
    (
    pVOID            param,        /* TCD specific parameter */
    USB_TARG_CHANNEL targChannel,  /* target channel */
    UINT8            configuration /* configuration value to set */
    )
    {
    pUSBTGT_RNDIS       pUsbTgtRndis = (pUSBTGT_RNDIS)param;
    STATUS              status = ERROR;
    
    if (NULL == pUsbTgtRndis)
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         "pUsbTgtRndis", 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Set current configuration global static variable */

    USBTGT_RNDIS_DBG("Set to the config %d\n",
                     configuration, 2, 3, 4, 5, 6);

    /*
     * <'uConfigToBind'> is the user's configuration, see
     * <'USBTGT_FUNC_DRIVER'>. And the developer need add the function 
     * driver to the TML with this parameter. The TML will attach this
     * function driver to the right configuration if there is enough resource
     * to support this function driver.
     * 
     * When this routine be called, means that, this functiond driver has
     * been attached to the right configuration. So, we do not check the 
     * configuration number here.
     */
     
    if (0 == configuration)
        {
        USBTGT_RNDIS_DBG("ReSet to the config 0, delete all the pipes created\n",
                         1, 2, 3, 4, 5, 6);

        status = usbTgtRndisAllPipesDelete(pUsbTgtRndis);
        
        }
    else
        {
        /* 
         * The target management has check the configur 
         * value is valid. So do not check it here.
         */
         
        USBTGT_RNDIS_DBG("Create all the required pipes to support the RNDIS "
                         "function\n",
                         1, 2, 3, 4, 5, 6);
        
        status = usbTgtRndisAllPipesCreate(pUsbTgtRndis, configuration);        
        }
    
    /* Record the cofiguration to be set */

    /* TODO : Add the retry using the task notifier */

    if (OK == status)
        {
        pUsbTgtRndis->uConfigurationValue = configuration;
        }
        
    return(status);
    }

/*******************************************************************************
*
* usbTgtRndisDescriptorGet - handle the descriptors Get request 
*
* This routine handles the Descriptor Get request get from the target
* management level. 
*
* NOTE: Most descriptors get request has been handled by the target management
* level.But it is still usful, such as get the string descriptors.
*
* NOTE: we used the <'param'> to record the <'pUSBTGT_RNDIS'> pointer, and 
* also have the <'targChannel'> parameter even no used to be compatible with
* the USB GEN1 target stack.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtRndisDescriptorGet
    (
    pVOID            param,          /*  TCD specific parameter */
    USB_TARG_CHANNEL targChannel,    /* target chennel */
    UINT8            requestType,    /* request type */
    UINT8            descriptorType, /* descriptor type */
    UINT8            descriptorIndex,/* descriptor index */
    UINT16           languageId,     /* language id */
    UINT16           length,         /* length of descriptor */
    pUINT8           pBfr,           /* buffer to hold the descriptor */
    pUINT16          pActLen         /* actual length */
    )
    {
    pUSBTGT_RNDIS       pUsbTgtRndis = (pUSBTGT_RNDIS)param;

    if (NULL == pUsbTgtRndis)
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         "pUsbTgtRndis", 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* this request must be standard request from the host */

    if (((requestType & USB_RT_DIRECTION_MASK) != USB_RT_DEV_TO_HOST) ||
        ((requestType & USB_RT_CATEGORY_MASK) != USB_RT_STANDARD) ||
        ((requestType & USB_RT_RECIPIENT_MASK) != USB_RT_DEVICE))
        {
        USBTGT_RNDIS_ERR("Get descriptor request should be standard and from host\n",
                         1, 2, 3, 4, 5, 6);
        
        return ERROR;
        }
    
    /* Determine type of descriptor being requested. */

    switch (descriptorType)
        {
        case USB_DESCR_STRING:
            
            USBTGT_RNDIS_DBG("Get USB_DESCR_STRING descriptors index %d\n",
                              descriptorIndex, 2, 3, 4, 5, 6);

            /*
             * If request for an interface string descriptor
             * copy the descriptor to the buffer.
             */
            
            break;
            
        case USB_DESCR_INTERFACE_POWER: 
            
            USBTGT_RNDIS_DBG("Get USB_DESCR_INTERFACE_POWER descriptors\n",
                            1, 2, 3, 4, 5, 6);
            
            return ERROR;
        default:
            return ERROR;
        }

    return OK;
    }

/*******************************************************************************
*
* usbTgtRndisInterfaceGet - handle the interface get request 
*
* This routine handles the interface Get request get from the target
* management level. 
*
* NOTE: TODO If move to the TML, all the function driver will be happy.It only
* here for using on GEN1 target stack. no using on GEN2.
*
* NOTE: we used the <'param'> to record the <'pUSBTGT_RNDIS'> pointer, and 
* also have the <'targChannel'> parameter even no used to be compatible with
* the USB GEN1 target stack.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtRndisInterfaceGet
    (
    pVOID            param,            /* TCD specific parameter */
    USB_TARG_CHANNEL targChannel,      /* Target channel */
    UINT16           interfaceIndex,   /* Interface index */
    pUINT8           pAlternateSetting /* Alternate setting */
    )
    {
    pUSBTGT_RNDIS       pUsbTgtRndis = (pUSBTGT_RNDIS)param;
    
    /* pUSBTGT_DEVICE_INFO pDeviceInfo = NULL; */

    if (NULL == pUsbTgtRndis)
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         "pUsbTgtRndis", 2, 3, 4, 5, 6);

        return ERROR;
        }

    if (pUsbTgtRndis->uConfigurationValue == 0)
        {
        USBTGT_RNDIS_ERR("Interface Get is not valid in default or configure state\n",
                         1, 2, 3, 4, 5, 6);
        
        return ERROR;
        }
    
    USBTGT_RNDIS_DBG("Interface get 0x%x\n",
                     pUsbTgtRndis->uAltSetting, 2, 3, 4, 5, 6);
    
    *pAlternateSetting = pUsbTgtRndis->uAltSetting;

    return OK;
    }

/*******************************************************************************
*
* usbTgtRndisInterfaceSet - handle the interface set request
*
* This routine handles the interface Set request get from the target
* management level. 
*
* NOTE: TODO If move to the TML, all the function driver will be happy.It only
* here for using on GEN1 target stack. no using on GEN2.
*
* NOTE: we used the <'param'> to record the <'pUSBTGT_RNDIS'> pointer, and 
* also have the <'targChannel'> parameter even no used to be compatible with
* the USB GEN1 target stack.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtRndisInterfaceSet
    (
    pVOID            param,           /* TCD specific parameter */
    USB_TARG_CHANNEL targChannel,     /* target channel */
    UINT16           interfaceIndex,  /* interface index */
    UINT8            alternateSetting /* alternate setting */
    )
    {
    pUSBTGT_RNDIS       pUsbTgtRndis = (pUSBTGT_RNDIS)param;
    
    /* pUSBTGT_DEVICE_INFO pDeviceInfo = NULL; */

    if (NULL == pUsbTgtRndis)
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         "pUsbTgtRndis", 2, 3, 4, 5, 6);

        return ERROR;
        }


    if (pUsbTgtRndis->uConfigurationValue == 0)
        {
        USBTGT_RNDIS_ERR("Interface Get is not valid in default or configure state\n",
                         1, 2, 3, 4, 5, 6);
        
        return ERROR;
        }
    
    USBTGT_RNDIS_DBG("Interface set  = 0x%x\n",
                     alternateSetting, 2, 3, 4, 5, 6);

    pUsbTgtRndis->uAltSetting = alternateSetting;
    
    return OK;
    }

/******************************************************************************
*
* usbTgtRndisVendorSpecific - handle vendor/class requests
*
* This routine is called when any vendor/class specific requests comes from host.
*
* NOTE: we used the <'param'> to record the <'pUSBTGT_RNDIS'> pointer, and 
* also have the <'targChannel'> parameter even no used to be compatible with
* the USB GEN1 target stack.
*
* RETURNS: OK, or ERROR if unable to process vendor-specific request
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtRndisVendorSpecific
    (
    pVOID               param,          /* TCD specific parameter */
    USB_TARG_CHANNEL    targChannel,    /* target channel */
    UINT8               requestType,    /* characteristics of request */
    UINT8               request,        /* request type */
    UINT16              value,          /* wValue */
    UINT16              index,          /* wIndex */
    UINT16              length          /* length to transfer */
    )
    {
    STATUS              status = ERROR;
    pUSBTGT_RNDIS       pUsbTgtRndis = (pUSBTGT_RNDIS)param;
    
    /* pUSBTGT_DEVICE_INFO pDeviceInfo = NULL; */

    if (NULL == pUsbTgtRndis)
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         "pUsbTgtRndis", 2, 3, 4, 5, 6);

        return ERROR;
        }

    if (requestType ==(USB_RT_HOST_TO_DEV | USB_RT_CLASS | USB_RT_INTERFACE))
        {
        if (request == USB_CDC_SEND_ENCAPSULATED_COMMAND)
            {
            /*
             * Get the RNDIS message payload.  Payload will be processed by the
             * callback.
             * Normally 256 length is enough.
             */

            if (length > RNDIS_CTRL_MSG_BUF_SIZE)
                {
                USBTGT_RNDIS_ERR("There is no enough buffer to receive the request"
                                 " data\n", 1, 2, 3, 4, 5, 6);

                /* TODO */
                }
            
            USBTGT_RNDIS_DBG("usbTgtRndisVendorSpecific "
                             "USB_CDC_SEND_ENCAPSULATED_COMMAND length 0x%X\n",
                             length, 2, 3, 4, 5, 6);

            /* I hate this parameter */
            
            status = usbTgtControlPayloadRcv(pUsbTgtRndis->targChannel, 
                                             length,
                                             (UINT8 *)&pUsbTgtRndis->requestMsgBuf[0],
                                             usbTgtRndisMsgProcessCallback);
                                      
            }
        }
    else if (requestType ==(USB_RT_DEV_TO_HOST | USB_RT_CLASS | USB_RT_INTERFACE))
        {
        if (request == USB_CDC_GET_ENCAPSULATED_RESPONSE)
            {
            /* Return encapsulated response */
            
            USBTGT_RNDIS_DBG("usbTgtRndisVendorSpecific "
                             "USB_CDC_GET_ENCAPSULATED_RESPONSE length %x\n",
                              pUsbTgtRndis->uReplyMsgLen, 2, 3, 4, 5, 6);
            
            status = usbTgtControlResponseSend(pUsbTgtRndis->targChannel, 
                                               pUsbTgtRndis->uReplyMsgLen, 
                                               pUsbTgtRndis->replyMsgBuf,
                                               USB_ERP_FLAG_NORMAL);
            
            if (status != OK)
                {
                USBTGT_RNDIS_ERR("GET_ENCAPSULATED_RESPONSE send fail\n",
                                 1, 2, 3, 4, 5, 6);
                
                return(status);
                }
            }
        }
    else
        {
        USBTGT_RNDIS_ERR("Unsupported Request type 0x%02x, req 0x%02x\n",
                         requestType, request, 3, 4, 5, 6);
        }
        
    return (status);
    }


