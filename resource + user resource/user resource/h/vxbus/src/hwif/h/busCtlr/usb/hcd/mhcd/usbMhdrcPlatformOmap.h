/* usbMhdrcPlatformOmap.h - MUSBMHDRC OMAP specific definitions */

/*
 * Copyright (c) 2009-2011, 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01e,21apr13,wyy  Make the preprocessor look for usbMhdrc.h in 
                 the current directory first
01d,23mar11,w_x  Address more code review comments
01c,09mar11,w_x  Code clean up for make man
01b,13mar10,s_z  Add Inventra DMA supported on OMAP3EVM
01a,13jul09,j_x  initial version
*/

/*
DESCRIPTION

This file defines the OMAP special register offsets and macros for the MHCI
Host Controller.

INCLUDE FILES: usbMhdrc.h
*/

#ifndef __INCusbMhdrcPlatformOmaph
#define __INCusbMhdrcPlatformOmaph

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "usbMhdrc.h" 

/* defines */

/* ULPI Register */

/* ULPI register of the MHCI controller for OMAP3530 */

#define USB_MHDRC_ULPIVBUSCONTROL        0x70
#define USB_MHDRC_ULPIUTMICONTROL        0x71
#define USB_MHDRC_ULPIINTMASK            0x72
#define USB_MHDRC_ULPIINTSRC             0x73
#define USB_MHDRC_ULPIREGDATA            0x74
#define USB_MHDRC_ULPIREGADDR            0x75
#define USB_MHDRC_ULPIREGCONTROL         0x76
#define USB_MHDRC_ULPIRAWDATAL           0x77

/* Bit Map for ULPIVBUSCONTROL register */

#define USB_MHDRC_ULPIVBUSCONTROL_USEEXTVBUSIND         (0x1 << 1)
#define USB_MHDRC_ULPIVBUSCONTROL_USEEXTVBUS            (0x1 << 0)

/* Bit Map for ULPIUTMICONTROL register */

#define USB_MHDRC_ULPIUTMICONTROL_DISBALEUTMI           (0x1 << 0)

/* Bit Map for ULPIREGCONTROL register */

#define USB_MHDRC_ULPIREGCONTROL_ULPIRDNWR              (0x1 << 2)
#define USB_MHDRC_ULPIREGCONTROL_ULPIREGCMPLT           (0x1 << 1)
#define USB_MHDRC_ULPIREGCONTROL_ULPIREGREQ             (0x1 << 0)

/* Define ISP1504 Phy Register Address */

#define USB_PHY_ISP1504_VID_LOW                         0x00
#define USB_PHY_ISP1504_VID_HIGH                        0x01
#define USB_PHY_ISP1504_PID_LOW                         0x02
#define USB_PHY_ISP1504_PID_HIGH                        0x03
#define USB_PHY_ISP1504_FUNCTURE_CTL                    0x04
#define USB_PHY_ISP1504_INTERFACE_CTL                   0x07
#define USB_PHY_ISP1504_OTG_CTL                         0x0A
#define USB_PHY_ISP1504_SETBITS_OFFSET                  0x01
#define USB_PHY_ISP1504_CLEARBITS_OFFSET                0x02

/* 1504 OTG Control Register bits */

#define ISP1504_OTG_CTL_USE_EXT_VBUS_IND     (1 << 7)     /* Use ext. Vbus indicator */
#define ISP1504_OTG_CTL_DRV_VBUS_EXT         (1 << 6)     /* Drive Vbus external */
#define ISP1504_OTG_CTL_DRV_VBUS             (1 << 5)     /* Drive Vbus */
#define ISP1504_OTG_CTL_CHRG_VBUS            (1 << 4)     /* Charge Vbus */
#define ISP1504_OTG_CTL_DISCHRG_VBUS         (1 << 3)     /* Discharge Vbus */
#define ISP1504_OTG_CTL_DM_PULL_DOWN         (1 << 2)     /* enable DM Pull Down */
#define ISP1504_OTG_CTL_DP_PULL_DOWN         (1 << 1)     /* enable DP Pull Down */
#define ISP1504_OTG_CTL_ID_PULL_UP           (1 << 0)     /* enable ID Pull Up */

/* Request Packet Count Pegisters */

#define USB_MHDRC_RQPKTCOUNT_EP(n)           (0x300 + (0x4 * (n)))

/* OTG Related Registers */

#define USB_MHDRC_OTG_REVISION            0x400
#define USB_MHDRC_OTG_SYSCONFIG           0x404
#define USB_MHDRC_OTG_SYSSTATUS           0x408
#define USB_MHDRC_OTG_INTERFSEL           0x40C
#define USB_MHDRC_OTG_FORCESTDBY          0x414

/* OTG_REVISION bits */

#define OTG_REVISION_MSK                 0xFF

/* OTG_SYSCONFIG bits */

#define OTG_SYSCONFIG_MIDLEMODE_SHIFT    12
#define OTG_SYSCONFIG_MIDLEMODE_MSK      (3 << OTG_SYSCONFIG_MIDLEMODE_SHIFT)
#define OTG_SYSCONFIG_FORCESTDBY         (0 << OTG_SYSCONFIG_MIDLEMODE_SHIFT)
#define OTG_SYSCONFIG_NOSTDBY            (1 << OTG_SYSCONFIG_MIDLEMODE_SHIFT)
#define OTG_SYSCONFIG_SMARTSTDBY         (2 << OTG_SYSCONFIG_MIDLEMODE_SHIFT)
#define OTG_SYSCONFIG_SIDLEMODE_SHIFT    3
#define OTG_SYSCONFIG_SIDLEMODE_MSK      (3 << OTG_SYSCONFIG_SIDLEMODE_SHIFT)
#define OTG_SYSCONFIG_FORCEIDLE          (0 << OTG_SYSCONFIG_SIDLEMODE_SHIFT)
#define OTG_SYSCONFIG_NOIDLE             (1 << OTG_SYSCONFIG_SIDLEMODE_SHIFT)
#define OTG_SYSCONFIG_SMARTIDLE          (2 << OTG_SYSCONFIG_SIDLEMODE_SHIFT)
#define OTG_SYSCONFIG_ENABLEWAKEUP       (1 << 2)
#define OTG_SYSCONFIG_SOFTRST            (1 << 1)
#define OTG_SYSCONFIG_AUTOIDLE           (1 << 0)

/* OTG_SYSSTATUS bits */

#define OTG_SYSSTATUS_RESETDONE          (1 << 0)

/* OTG_INTERFSEL bits */

#define OTG_INTERFSEL_PHYSEL_SHIFT        0
#define OTG_INTERFSEL_PHYSEL_MSK         (3 << OTG_INTERFSEL_PHYSEL_SHIFT)
#define OTG_INTERFSEL_UTMI_8BIT           (0 << OTG_INTERFSEL_PHYSEL_SHIFT)
#define OTG_INTERFSEL_ULPI_12PIN          (1 << OTG_INTERFSEL_PHYSEL_SHIFT)
#define OTG_INTERFSEL_ULPI_8PIN           (2 << OTG_INTERFSEL_PHYSEL_SHIFT)

/* OTG_FORCESTDBY bits */

#define OTG_FORCESTDBY_ENABLEFORCE        (1 << 0)

#define USB_MHDRC_INVENTRA_DMA_CHANNEL_NUM (0x8)

#define USB_MHDRC_USE_OMAP_ULPI_INTERFACE  
#define USB_MHDRC_USE_OMAP_EXTENDED_OTG_REGS    

#ifdef __cplusplus
}
#endif

#endif /* __INCusbMhdrcPlatformOmaph */


