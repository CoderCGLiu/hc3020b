/* usbXhcdEvent.c - USB XHCI Driver Event Handling Routines */

/*
 * Copyright (c) 2011-2014 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01g,04Dec14,wyy  Clean compiler warning
01f,03may13,wyy  Remove compiler warning (WIND00356717)
01e,19oct12,w_x  Add polling mode support
01d,17oct12,w_x  Fix compiler warnings (WIND00370525)
01c,04sep12,w_x  Address review comments and fix some defects
                 (WIND00370637, WIND00372765)
01b,26aug12,w_x  Properly sync data transfer buffer (WIND00372215)
01a,16may11,w_x  written
*/

#include "usbXhcdInterfaces.h"

#ifdef USB_DEBUG_ENABLE
IMPORT VOID usbXhcdRingShowInternal
       (
       pUSB_XHCD_DATA      pHCDData,
       pUSB_XHCD_RING      pRing
       );
#endif

/*******************************************************************************
*
* usbXhcdHandleCommandCompleteEvent - handle one Command Completion Event TRB
*
* This routine is to handle one Command Completion Event TRB.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbXhcdHandleCommandCompleteEvent
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCI_TRB       pEventTRB
    )
    {
    /* Short cut to command segment */

    pUSB_XHCD_SEGMENT   pCmdSeg;

    /* Command TRB that this Event TRB completes for */

    pUSB_XHCI_TRB       pCmdTRB;

    /* Pointer to the related Command structure */

    pUSB_XHCD_CMD       pHCDCmd;

    /* Bus address for the Command TRB */

    ULONG               uCmdTRB;

    /* Command TRB Type */

    UINT8               uCmdTrbType;

    /* Command Completion Code */

    UINT8               uCmdCompCode;

    /* Slot ID */

    UINT8               uSlotId;

    /* Command TRB Index (also Command structure array index) */

    UINT32              uIndex;

    /*
     * Get the Command Segment - we have only one Command Ring
     * with one single Segment, so locating the Command TRB is
     * just an offset math!
     */

    pCmdSeg = pHCDData->pCmdRing->pHeadSeg;

    /* Get the Command TRB Bus Address */

    uCmdTRB = (ULONG)usbXhcdGetTrbData64(pHCDData, pEventTRB);

    if (uCmdTRB == 0)
        {
        USB_XHCD_WARN("usbXhcdHandleTransferEvent - "
            "pEventTRB %p uCmdTRB NULL\n",
            pEventTRB, 2, 3, 4, 5, 6);
        return;
        }

    /*
     * Get the Command TRB CPU Virtual Address.
     *
     * Note that we cast uCmdTRB and pCmdSeg->uHeadTRB into
     * pUSB_XHCI_TRB then perform a pointer substract to get
     * the Command TRB index, which saves a divding than if
     * we use them as UINT64 directly.
     */

    uIndex = (UINT32)((pUSB_XHCI_TRB)uCmdTRB -
              (pUSB_XHCI_TRB)((ULONG)pCmdSeg->uHeadTRB));

    pCmdTRB = &pCmdSeg->pTRBs[uIndex];

    pHCDCmd = &pHCDData->pCMDs[uIndex];

    /* Get the Command TRB Type */

    uCmdTrbType = (UINT8)USB_XHCI_TRB_CTRL_TYPE_GET(
                         USB_XHCD_SWAP_DESC_DATA32(pHCDData, pCmdTRB->uCtrl));

    /* Get the Command Completion Code */

    uCmdCompCode = (UINT8)USB_XHCI_TRB_INFO_COMP_CODE_GET(
                          USB_XHCD_SWAP_DESC_DATA32(pHCDData, pEventTRB->uInfo));

    /* Get the Slot ID */

    uSlotId = (UINT8)USB_XHCI_TRB_CTRL_SLOT_ID_GET(
                     USB_XHCD_SWAP_DESC_DATA32(pHCDData, pEventTRB->uCtrl));

    pHCDCmd->uCompCode = uCmdCompCode;
    pHCDCmd->uSlotId = uSlotId;

    USB_XHCD_WARN("usbXhcdHandleCommandCompleteEvent - "
        "Event TRB @%p[CPU] completes %s CMD TRB @%p[CPU] for Slot ID %d - "
        "Completion Code %d (%s)\n",
        pEventTRB,
        usbXhcdTrbName(uCmdTrbType),
        pCmdTRB,
        uSlotId,
        uCmdCompCode,
        usbXhcdCmdCompCodeName(uCmdCompCode));

    usbXhcdCmdComplete(pHCDData, pHCDCmd);
    }

/*******************************************************************************
*
* usbXhcdHandlePortStatusChangeEvent - handle one Port Status Change Event TRB
*
* This routine is to handle one Port Status Change Event TRB.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbXhcdHandlePortStatusChangeEvent
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCI_TRB       pEventTRB
    )
    {
    /* Command Completion Code */

    UINT8               uCmdCompCode;

    /* Port ID that generated this Port Status Change Event */

    UINT8               uPortId;

    /* PORTSC register value */

    UINT32              uPortSc;

    /* Pointer to Port info */

    pUSB_XHCD_ROOT_PORT_INFO pPortInfo;

    /* Pointer to Root Hub Data */

    pUSB_XHCD_ROOT_HUB_DATA  pRootHub;

    /* Get the Command Completion Code */

    uCmdCompCode = (UINT8)USB_XHCI_TRB_INFO_COMP_CODE_GET(
                          USB_XHCD_SWAP_DESC_DATA32(pHCDData, pEventTRB->uInfo));

    /* Get Port Number of the Root Hub Port that generated this event */

    uPortId = (UINT8)USB_XHCI_TRB_DATALO_PORT_ID_GET(
                     USB_XHCD_SWAP_DESC_DATA32(pHCDData, pEventTRB->uDataLo));

    /* Read the PORTSC register */

    uPortSc = USB_XHCD_READ_OP_REG32(pHCDData,
                    USB_XHCI_PORTSC((uPortId - 1)));

    pPortInfo = &pHCDData->pRootPorts[uPortId - 1];

    pRootHub = pPortInfo->pRootHub;

    USB_XHCD_WARN("usbXhcdHandlePortStatusChangeEvent - "
         "Status Changed - USB%d Port %d %s at %s in %s [uPortSc=0x%08x]\n",
         pRootHub->uRevMajor,
         pPortInfo->uPortNumber,
         (uPortSc & USB_XHCI_PORTSC_CCS) ? "Connected" : "Disconnected",
         gUsbXhcdSpeedStr[USB_XHCI_PORTSC_PS_GET(uPortSc)],
         gUsbXhcdLinkStateStr[USB_XHCI_PORTSC_PLS_GET(uPortSc)],
         uPortSc);

    (void) usbXhcdRootHubCopyInterruptData(pHCDData,
                                    pRootHub,
                                    (1 << pPortInfo->uPortNumber));
    }

/*******************************************************************************
*
* usbXhcdHandleTransferEvent - handle one Transfer Event TRB
*
* This routine is to handle one Transfer Event TRB.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbXhcdHandleTransferEvent
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCI_TRB       pEventTRB
    )
    {
    /* Bus address for the Transfer TRB */

    ULONG                   uTrbBusAddr = 0;

    /* Command Completion Code */

    UINT8                   uXferCompCode = 0;

    /* The device that generated the event */

    pUSB_XHCD_DEVICE        pHCDDevice = NULL;

    /* Pointer to the HCD maintained pipe */

    pUSB_XHCD_PIPE          pHCDPipe = NULL;

    /* Pointer to the Completed Request */

    pUSB_XHCD_REQUEST_INFO  pRequest = NULL;

    /* Pointer to the Completed URB */

    pUSBHST_URB             pURB = NULL;

    /* Slot ID that generated this event */

    UINT8                   uSlotId = 0;

    /* Endpoint ID that generated this event */

    UINT8                   uEpId = 0;

    /*
     * Transfer Length (either residual number of bytes
     * not transferred for Normal Transfer Event TRB or
     * actual transfered length for Event Data TRB)
     */

    UINT32                  uLength = 0;

    /* Actual Transfer Length */

    UINT32                  uActLength = 0;

    /* Request Transfer Length */

    UINT32                  uReqLength = 0;

    /* TRB Control field */

    UINT32                  uCtrl = 0;

    /* TRB Info field */

    UINT32                  uInfo = 0;

    /* Get the TRB Bus Address */

    uTrbBusAddr = (ULONG)usbXhcdGetTrbData64(pHCDData, pEventTRB);

    if (uTrbBusAddr == 0)
        {
        USB_XHCD_WARN("usbXhcdHandleTransferEvent - "
            "pEventTRB uTrbBusAddr NULL\n",
            pEventTRB, 2, 3, 4, 5, 6);
        return;
        }

    /* Get the TRB Ctrl field in CPU endian */

    uCtrl = USB_XHCD_SWAP_DESC_DATA32(pHCDData, pEventTRB->uCtrl);

    /* Get the TRB Info field in CPU endian */

    uInfo = USB_XHCD_SWAP_DESC_DATA32(pHCDData, pEventTRB->uInfo);

    /* Get the Transfer Completion Code */

    uXferCompCode = (UINT8)USB_XHCI_TRB_INFO_COMP_CODE_GET(uInfo);

    /* Get the Slot ID that generated this event */

    uSlotId = (UINT8)USB_XHCI_TRB_CTRL_SLOT_ID_GET(uCtrl);

    /* Get the Endpoint ID that generated this event */

    uEpId = (UINT8)USB_XHCI_TRB_CTRL_EP_ID_GET(uCtrl);

    /* Find the device */

    pHCDDevice = pHCDData->ppDevSlots[uSlotId];

    if (pHCDDevice == NULL)
        {
        USB_XHCD_ERR("usbXhcdHandleTransferEvent - "
            "Device slot for Slot ID %d NULL\n",
            uSlotId, 2, 3, 4, 5, 6);

        return;
        }

    /* Find the pipe */

    pHCDPipe = pHCDDevice->pDevPipes[uEpId];

    if ((pHCDPipe == NULL) ||
        (pHCDPipe->uValidMaggic != USB_XHCD_MAGIC_ALIVE))
        {
        USB_XHCD_ERR("usbXhcdHandleTransferEvent - "
            "Device pipe for Slot ID %d EP ID %d invalid\n",
            uSlotId, uEpId, 3, 4, 5, 6);

        return;
        }

    /* Exclusively access the pipe request list */

    (void) OS_WAIT_FOR_EVENT(pHCDPipe->pipeMutex, OS_WAIT_INFINITE);

#if _WRS_CONFIG_USB_XHCD_TRANS_ERR_RETRIES

    /* Soft retrying for transaction error */

    if (uXferCompCode == USB_XHCI_COMP_TRANS_ERR)
        {
        USB_XHCD_WARN("usbXhcdHandleTransferEvent - "
            "USB Transaction Error on Slot ID %d EP ID %d!\n",
            uSlotId, uEpId, pHCDPipe->uNumTransErrors, 4, 5, 6);

        pHCDPipe->uNumTransErrors++;

        if (pHCDPipe->uNumTransErrors < _WRS_CONFIG_USB_XHCD_TRANS_ERR_RETRIES)
            {
            if (usbXhcdQueueResetEndpointCmd(pHCDData,
                                             uSlotId,
                                             uEpId,
                                             1,
                                             NO_WAIT) == OK)
                {
                /* Release the exclusive access */

                OS_RELEASE_EVENT(pHCDPipe->pipeMutex);

                return;
                }
            }
        }

    pHCDPipe->uNumTransErrors = 0;

#endif  /* _WRS_CONFIG_USB_XHCD_TRANS_ERR_RETRIES */

#ifdef USB_DEBUG_ENABLE
    if ((uXferCompCode != USB_XHCI_COMP_SUCCESS) &&
        (uXferCompCode != USB_XHCI_COMP_SHORT_PACKET))
        USB_XHCD_WARN("usbXhcdHandleTransferEvent - "
            "Slot ID %d EP ID %d %s\n",
            uSlotId, uEpId, usbXhcdCmdCompCodeName(uXferCompCode), 4, 5, 6);
#endif


    /* Check if we are using Event Data mechanism */

    if (USB_XHCI_TRB_CTRL_ED_GET(uCtrl))
        {
        /* Get the Transfer Length */

        uLength = USB_XHCI_TRB_INFO_ACT_LEN_GET(uInfo);

        /* Get the request directly from the Event Data */

        pRequest = (pUSB_XHCD_REQUEST_INFO)uTrbBusAddr;

        if (pRequest != NULL)
            {
            pURB = pRequest->pURB;
            pHCDPipe = pRequest->pHCDPipe;
            uReqLength = pRequest->uReqLen;

            /*
             * Get the actually transfered length.
             *
             * If the Event Data flag is '1' TRB Transfer Length
             * field is set to the value of the Event Data Transfer
             * Length Accumulator (EDTLA).
             */
            uActLength = uLength;
            }

        USB_XHCD_DBG("usbXhcdHandleTransferEvent - Event Data Transfer Event - "
            "pHCDPipe %p pRequest %p pURB %p uReqLength %d uActLength %d %s\n",
            pHCDPipe,
            pRequest,
            pURB,
            uReqLength,
            uActLength,
            usbXhcdCmdCompCodeName(uXferCompCode));
        }
    else
        {
        /* Get the Transfer Length */

        uLength = USB_XHCI_TRB_INFO_XFER_LEN_GET(uInfo);

        /* We have to find the request by comparing */

        pRequest = usbXhcdPipeRequestFind(pHCDData, uSlotId, uEpId, uTrbBusAddr);
        if (pRequest != NULL)
            {
            pURB = pRequest->pURB;
            pHCDPipe = pRequest->pHCDPipe;
            uReqLength = pRequest->uReqLen;
            /*
             * Get the actually transfered length.
             *
             * If the Event Data flag is '0', TRB Transfer Length
             * field shall reflect the residual number of bytes
             * not transferred. So we need to do a subtraction
             * to get the actually transfered length.
             */
            uActLength = pURB->uTransferLength - uLength;
            }

        USB_XHCD_DBG("usbXhcdHandleTransferEvent - Normal Transfer Event - "
            "pHCDPipe %p pRequest %p pURB %p uReqLength %d uActLength %d %s\n",
            pHCDPipe,
            pRequest,
            pURB,
            uReqLength,
            uActLength,
            usbXhcdCmdCompCodeName(uXferCompCode));
        }

    /* If we didn't find a valid URB, return */

    if (pURB == NULL)
        {
        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDPipe->pipeMutex);

        return ;
        }

    pURB->uTransferLength = uActLength;

    switch (uXferCompCode)
        {
        case USB_XHCI_COMP_SHORT_PACKET:
        case USB_XHCI_COMP_SUCCESS:
            {
            pURB->nStatus = USBHST_SUCCESS;
            }
            break;
        case USB_XHCI_COMP_BABBLE_DETECTED_ERR:

#ifdef USB_DEBUG_ENABLE
            usbXhcdRingShowInternal(pHCDData, pRequest->pRing);
#endif /* USB_DEBUG_ENABLE */
        /* Fall through */
        case USB_XHCI_COMP_STALL_ERR:
            {
            pURB->nStatus = USBHST_STALL_ERROR;
            pURB->uTransferLength = 0;
            pHCDPipe->uErrStreamId = (UINT16)pURB->uStreamId;
            }
            break;
        default:
            {
            pURB->nStatus = USBHST_FAILURE;
            pURB->uTransferLength = 0;
            }
            break;
        }

    if (pURB->nStatus != USBHST_SUCCESS)
        {
        UINT8 uEpState;

        uEpState = usbXhcdPipeStateGet(pHCDData, pHCDPipe);

        if (uEpState == USB_XHCI_EP_STATE_HALTED)
            {
            pURB->nStatus = USBHST_STALL_ERROR;

            USB_XHCD_WARN("usbXhcdHandleTransferEvent - STALL!! "
                "uTrbBusAddr @0x%08x[BUS] URB @%p callback @%p with Status %d Length %d\n",
                uTrbBusAddr,
                pURB,
                pURB->pfCallback,
                pURB->nStatus,
                pURB->uTransferLength, 6);
            }
        else
            {
            USB_XHCD_WARN("usbXhcdHandleTransferEvent - NOT STALL!! "
                "uTrbBusAddr @0x%08x[BUS] URB @%p callback @%p with Status %d Length %d uEpState %d\n",
                uTrbBusAddr,
                pURB,
                pURB->pfCallback,
                pURB->nStatus,
                pURB->uTransferLength, uEpState);
            }
        }

    USB_XHCD_DBG("usbXhcdHandleTransferEvent - "
        "Calling URB @%p callback @%p with Status %d Length %d\n",
        pURB,
        pURB->pfCallback,
        pURB->nStatus,
        pURB->uTransferLength, 5, 6);

    if (pRequest->uReqLen != 0)
        {
        /*
         * <copy from bounce buffer and> DMA cache operations.
         *
         * Even though the xHCI can access full 64-bit address
         * space, we still may need to use bounce buffer since
         * we have specified VXB_DMABUF_MAPLOAD_CACHEALIGNCHECK
         * and VXB_DMABUF_CACHEALIGNCHECK for the DMA map/tags.
         *
         * We vxbDmaBufSync() only for IN transfers.
         */

        if (USBHST_URB_DATA_IN(pURB))
            {          
            (void) vxbDmaBufSync(pHCDData->pDev,
                          pHCDPipe->dataDmaTagId,
                          pRequest->dataDmaMapId,
                          _VXB_DMABUFSYNC_DMA_POSTREAD);

            /*
             * On some platforms, control transfer needs some delay
             * before the data can be accessed. We generize this assuming
             * control transfers are not performance critical thus
             * this delay is acceptable. This seems to be a workaround
             * for now but we may look into again.
             */

            if (pURB->uTransferType == USBHST_CONTROL_TRANSFER)
                OS_DELAY_MS(5);
            }

        (void) vxbDmaBufMapUnload(pHCDPipe->dataDmaTagId,
                           pRequest->dataDmaMapId);
        }

    usbXhcdReleaseRequestInfo(pHCDData, pHCDPipe, pRequest);

    /* Release the exclusive access */

    OS_RELEASE_EVENT(pHCDPipe->pipeMutex);

    /* Call the URB callback */

    if (pURB->pfCallback)
        pURB->pfCallback(pURB);
    }

/*******************************************************************************
*
* usbXhcdHandleEventTRB - handle one Event TRB of the Event Ring
*
* This routine is to handle one Event TRB of the Event Ring.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbXhcdHandleEventTRB
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_RING      pEvtRing,
    pUSB_XHCD_SEGMENT   pDeqSeg,
    pUSB_XHCI_TRB       pDeqTRB
    )
    {
    /* Get the Event TRB Type */

    UINT8 uTrbType = (UINT8)USB_XHCI_TRB_CTRL_TYPE_GET(
                            USB_XHCD_SWAP_DESC_DATA32(pHCDData, pDeqTRB->uCtrl));

    USB_XHCD_VDBG("usbXhcdHandleEventTRB - "
        "Interrupter %d Deq TRB CPU ADDR @%p with Type %d is %s TRB\n",
        pEvtRing->uIntr, pDeqTRB, uTrbType, usbXhcdTrbName(uTrbType), 5, 6);

    switch (uTrbType)
        {
        case USB_XHCI_TRB_XFER_EVENT:
            {
            USB_XHCD_VDBG("usbXhcdHandleEventTRB - "
                "USB_XHCI_TRB_XFER_EVENT\n",
                1, 2, 3, 4, 5, 6);
            usbXhcdHandleTransferEvent(pHCDData, pDeqTRB);
            }
            break;
        case USB_XHCI_TRB_CMD_COMPLETE_EVENT:
            {
            USB_XHCD_DBG("usbXhcdHandleEventTRB - "
                "USB_XHCI_TRB_CMD_COMPLETE_EVENT\n",
                1, 2, 3, 4, 5, 6);
            usbXhcdHandleCommandCompleteEvent(pHCDData, pDeqTRB);
            }
            break;
        case USB_XHCI_TRB_PORTSTS_CHANGE_EVENT:
            {
            USB_XHCD_WARN("usbXhcdHandleEventTRB - "
                "USB_XHCI_TRB_PORTSTS_CHANGE_EVENT\n",
                1, 2, 3, 4, 5, 6);
            usbXhcdHandlePortStatusChangeEvent(pHCDData, pDeqTRB);
            }
            break;
        case USB_XHCI_TRB_BW_REQ_EVENT:
            {
            USB_XHCD_WARN("usbXhcdHandleEventTRB - "
                "USB_XHCI_TRB_BW_REQ_EVENT\n",
                1, 2, 3, 4, 5, 6);
            }
            break;
        case USB_XHCI_TRB_DB_EVENT:
            {
            USB_XHCD_WARN("usbXhcdHandleEventTRB - "
                "USB_XHCI_TRB_DB_EVENT\n",
                1, 2, 3, 4, 5, 6);
            }
            break;
        case USB_XHCI_TRB_HC_EVENT:
            {
            USB_XHCD_WARN("usbXhcdHandleEventTRB - "
                "USB_XHCI_TRB_HC_EVENT\n",
                1, 2, 3, 4, 5, 6);
            }
            break;
        case USB_XHCI_TRB_DEV_NOTIFY_EVENT:
            {
            USB_XHCD_WARN("usbXhcdHandleEventTRB - "
                "USB_XHCI_TRB_DEV_NOTIFY_EVENT\n",
                1, 2, 3, 4, 5, 6);
            }
            break;
        case USB_XHCI_TRB_MFIDX_WRAP_EVENT:
            {
            USB_XHCD_WARN("usbXhcdHandleEventTRB - "
                "USB_XHCI_TRB_MFIDX_WRAP_EVENT\n",
                1, 2, 3, 4, 5, 6);
            }
            break;
        default:
            {
            USB_XHCD_WARN("usbXhcdHandleEventTRB - "
                "Uknown event %d\n",
                uTrbType, 2, 3, 4, 5, 6);
            }
            break;
        }
    }

/*******************************************************************************
*
* usbXhcdHandleEventInterrupt - handle events for one interrupter
*
* This routine is to handle events for one interrupter.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbXhcdHandleEventInterrupt
    (
    pUSB_XHCD_DATA      pHCDData,
    UINT32              uIntrIndex
    )
    {
    pUSB_XHCD_RING      pEvtRing = NULL;
    pUSB_XHCI_TRB       pDeqTRB = NULL;
    pUSB_XHCI_TRB       pFirstDeqTRB = NULL;
    UINT64              uDeqPtr;
    UINT64              uERDP;
    UINT32              uCtrl;

    if ((uIntrIndex >= pHCDData->uNumEvtRings) ||
        ((pEvtRing = pHCDData->ppEvtRings[uIntrIndex]) == NULL))
        {
        USB_XHCD_ERR("usbXhcdHandleEventInterrupt - invalid parameter,"
            "uIntrIndex %d pEvtRing %p uNumEvtRings %d\n",
            uIntrIndex, pEvtRing, pHCDData->uNumEvtRings, 4, 5, 6);

        return;
        }
    /* Read the current ERDP */

    uERDP = USB_XHCD_READ_RT_REG64(pHCDData, USB_XHCI_ERDP(uIntrIndex));

    USB_XHCD_VDBG("usbXhcdHandleEventInterrupt - Interrupter %d ERDP %p\n",
        uIntrIndex, uERDP, 3, 4, 5, 6);

    /* Get the First Dequeue TRB */

    pDeqTRB = pFirstDeqTRB = pEvtRing->pDeqTRB;

    /* Get the TRB Control in CPU endian */

    uCtrl = USB_XHCD_SWAP_DESC_DATA32(pHCDData, pDeqTRB->uCtrl);

    /* The TRB Cycle State should match Event Ring Consumer Cycle State */

    while (USB_XHCI_TRB_CTRL_CYCLE_BIT_GET(uCtrl) == pEvtRing->uCycle)
        {
        usbXhcdHandleEventTRB(pHCDData, pEvtRing, pEvtRing->pDeqSeg, pDeqTRB);

        /* Advance to the next valid Event TRB */

        usbXhcdDeqPtrAdvance(pHCDData, pEvtRing);

        /* Get the current Dequeue TRB */

        pDeqTRB = pEvtRing->pDeqTRB;

        /* Get the TRB Control in CPU endian */

        uCtrl = USB_XHCD_SWAP_DESC_DATA32(pHCDData, pDeqTRB->uCtrl);
        }

    /*
     * If the above loop has advanced the software Dequeue Pointer,
     * then let's advance the hardware Dequeue Pointer here!
     */

    if (pFirstDeqTRB != pEvtRing->pDeqTRB)
        {
        /* Get the new Dequeue Pointer Bus Address */

        uDeqPtr = usbXhcdTrbBusAddr(pHCDData,
                                    pEvtRing->pDeqSeg, pEvtRing->pDeqTRB);

        /* We need a valid Dequeue Pointer to update */

        if (uDeqPtr != 0)
            {
            USB_XHCD_VDBG("usbXhcdHandleEventInterrupt - "
                "Interrupter %d Next Dequeue TRB @%p[CPU] @%p[BUS]\n",
                uIntrIndex, pEvtRing->pDeqTRB, uDeqPtr, 4, 5, 6);

            /* Clear the ERDP field but keep the DESI and EHB bits */

            uERDP &= (USB_XHCI_ERDP_MASK);

            /*
             * Clear the EHB bit.
             *
             * Event Handler Busy (EHB) - RW1C. Default = '0'. This flag
             * shall be set to '1' when the IP bit is set to '1' and cleared
             * to '0' by software when the Dequeue Pointer register is written.
             */

            uERDP |= (USB_XHCI_ERDP_EHB);

            /* Composite the actual value to write to ERDP register */

            uERDP |= (uDeqPtr & (~USB_XHCI_ERDP_MASK));
            }
        }

    /* Write the ERDP register */

    USB_XHCD_WRITE_RT_REG64(pHCDData, USB_XHCI_ERDP(uIntrIndex), uERDP);
    }


/*******************************************************************************
*
* usbXhcdISR - interrupt service routine for the XHCI HCD Driver
*
* This routine is the ISR for the XHCI HCD Driver.
*
* SMP NOTE:
*
* This ISR does not make any callouts except to ISR safe control functions
* (logMsg, semGive etc). It manipulates local controller registers and
* then releases a semaphore for a high priority task to perform the processing.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbXhcdISR
    (
    pVOID pParam
    )
    {
    pUSB_XHCD_RING  pEventRing = pParam;
    pUSB_XHCD_DATA  pHCDData = NULL;
    UINT32          uReg32;
    UINT32          uSts;
    UINT32          uIdx;

    /* Validity check */

    if ((pEventRing == NULL) ||
        ((pHCDData = pEventRing->pHCDData) == NULL))
        {
        USB_XHCD_VDBG("usbXhcdISR - invalid parameter pEventRing %p pHCDData %p\n",
            pEventRing, pHCDData, 3, 4, 5, 6);

        return;
        }

    /* Take care of shared interrupt cases */

    if (pHCDData->uIsrMagic != USB_XHCD_MAGIC_ALIVE)
        {
        USB_XHCD_WARN("usbXhcdISR - not my dish\n",
            1, 2, 3, 4, 5, 6);
        return;
        }
    /* Read the USBSTS register */
    uSts = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_USBSTS);

    /*
     * Write the USBSTS register to clear the generated interrupts.
     * Software that uses EINT shall clear it prior to clearing any
     * IP flags.
     */

    USB_XHCD_WRITE_OP_REG32(pHCDData, USB_XHCI_USBSTS, uSts);

    /*
     * Check if there are Event Interrupts (EINT), if so, we should
     * clear the IP bit in the IMAN reigister here. We can not delay
     * this to the interupt handler task, doing so would get flood
     * of interrupts and causing system reboot.
     */

    if (uSts & USB_XHCI_USBSTS_EINT)
        {
        uIdx = pEventRing->uIntr;

        USB_XHCD_VDBG("usbXhcdISR - Interrupter %d has events\n",
            uIdx, 2, 3, 4, 5, 6);

        /*
         * 4.17.5 Interrupt Blocking:
         *
         * If MSI or MSI-X interrupts are enabled, IP shall be cleared
         * automatically when the PCI Dword write generated by the
         * Interrupt assertion is complete.
         *
         * If PCI Pin Interrupts are enabled then, IP shall be cleared
         * by software.
         */
#ifndef _WRS_CONFIG_USB_XHCD_POLLING_MODE
        if (pHCDData->uIntrType == USB_XHCD_INTR_TYPE_INTX)
#endif /* _WRS_CONFIG_USB_XHCD_POLLING_MODE */
            {
            /* Read the IMAN register */

            uReg32 = USB_XHCD_READ_RT_REG32(pHCDData, USB_XHCI_IMAN(uIdx));

            if (uReg32 & USB_XHCI_IMAN_IP)
                {
                /* Clear the IP (RW1C) but keep IE (RW) enabled */

                USB_XHCD_WRITE_RT_REG32(pHCDData,
                    USB_XHCI_IMAN(uIdx),
                    (uReg32 | USB_XHCI_IMAN_IP | USB_XHCI_IMAN_IE));
                }
            }

        /* Record that this interrupter has events */

        (void) vxAtomicInc(&(pEventRing->uNumEvents));
        }

    /* All events should be handled */

    if (uSts != 0)
        {
        SPIN_LOCK_ISR_TAKE(&pHCDData->spinLockIsr);

        pHCDData->uUSBSTS = uSts;

        SPIN_LOCK_ISR_GIVE(&pHCDData->spinLockIsr);

        USB_XHCD_VDBG("usbXhcdISR - USBSTS = 0x%X\n",
            uSts, 2, 3, 4, 5, 6);

        OS_RELEASE_EVENT(pHCDData->InterruptEvent);
        }
    }

/*******************************************************************************
*
* usbXhcdInterruptHandler - handle XHCI interrupts
*
* This is the task entry function which handles the XHCI interrupts.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbXhcdInterruptHandler
    (
    pUSB_XHCD_DATA pHCDData
    )
    {
    pUSB_XHCD_RING  pEventRing;
    UINT32          uSts;
    UINT32          uIdx;
    _Vx_ticks_t     timeout;      /* timeout in ticks */

    if ((pHCDData == NULL) ||
        (pHCDData->pDev == NULL) ||
        (pHCDData->InterruptEvent == NULL))
        {
        USB_XHCD_ERR("usbXhcdInterruptHandler - invalid parameters\n",
            1, 2, 3, 4, 5, 6);
        return;
        }

/*
 * When in interrupt mode, we need to wait until interrupts fire;
 * Otherwise, we will wait some time and then call the ISR to poll
 * for interrupt. In polling mode, we will still use INTx mode (but
 * we do not connect the ISR).
 */

#ifndef _WRS_CONFIG_USB_XHCD_POLLING_MODE
    timeout = WAIT_FOREVER;
#else
    timeout = _WRS_CONFIG_USB_XHCD_POLLING_INTERVAL;
#endif /* _WRS_CONFIG_USB_XHCD_POLLING_MODE */

    while (TRUE)
        {
        if (OS_WAIT_FOR_EVENT(pHCDData->InterruptEvent, timeout) != OK)
            {
#ifndef _WRS_CONFIG_USB_XHCD_POLLING_MODE
            USB_XHCD_WARN("usbXhcdInterruptHandler - OS_WAIT_FOR_EVENT error\n",
                1, 2, 3, 4, 5, 6);
            (void) OS_RESCHEDULE_THREAD();

#else /* _WRS_CONFIG_USB_XHCD_POLLING_MODE */

            /* Poll for interrupt events */

            usbXhcdISR((pVOID)pHCDData->ppEvtRings[0]);

#endif /* _WRS_CONFIG_USB_XHCD_POLLING_MODE */

            continue;
            }

        /* Save the USBSTS value as local copy */

        SPIN_LOCK_ISR_TAKE(&pHCDData->spinLockIsr);

        uSts = pHCDData->uUSBSTS;

        SPIN_LOCK_ISR_GIVE(&pHCDData->spinLockIsr);

        /* Scan through all possible event rings for interrupters */

        for (uIdx = 0; uIdx < pHCDData->uNumEvtRings; uIdx++)
            {
            /* Get the event ring */

            pEventRing = pHCDData->ppEvtRings[uIdx];

            /* If some events posted, handle them here! */

            if (vxAtomicGet(&(pEventRing->uNumEvents)) > 0)
                {
                USB_XHCD_VDBG("usbXhcdInterruptHandler - "
                              "Interrupter %d has events\n",
                              uIdx, 2, 3, 4, 5, 6);

                (void) vxAtomicDec(&(pEventRing->uNumEvents));

                /* Handle the interrupter events */

                usbXhcdHandleEventInterrupt(pHCDData, uIdx);
                }
            }
        }
    }

