/* usbTgtMscCmd.c - USB Target Mass Storage Class command process module */

/*
 * Copyright (c) 2010-2013, 2016 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification History
--------------------
01p,14mar16,dee  fix static analysis issues   CHECKED_RETURN
01o,06may13,s_z  Remove compiler warning (WIND00356717)
01n,21nov12,s_z  Add workaround to pass MSC CV case 10 (WIND00389964)
01m,25oct12,s_z  MHDRC TCD pass CV testing (WIND00385197) 
01l,17oct12,s_z  Fix short packet receive issue (WIND00374594)
01k,10apr12,s_z  Add media change notification (WIND00328309)
01j,10oct11,m_y  remove unused variable 
01i,12aug11,m_y  modify read/write process (WIND00288793)
01h,13jun11,jws  set mediaReady = FALSE when mediaRemoved = TRUE 
                 with loEj command from host. 
01g,11apr11,m_y  modify read/write routine to support bigger transfer length
01f,08apr11,ghs  Fix code coverity issue(WIND00264893)
01e,25mar11,m_y  correct mode sense data and usbTgtMscStdInquiryDataInit
01d,17mar11,m_y  remove usb_tgt_msc_lunf structure
01c,17mar11,m_y  modify code based on the code review
01b,03mar11,m_y  code clean up
01a,28dec10,x_f  written
*/

/*
DESCRIPTION

This module implements for USB target mass storage class.
These routines are invoked by the USB 2.0 mass storage driver based on
the contents of the USB CBW (command block wrapper).

INCLUDES: usbTgtMscCmd.h, usbTgtMscUtil.h, usbTgtMscLib
*/


/* includes */

#include <usbTgtMscCmd.h>
#include <usbTgtMscUtil.h>
#include <usbTgtMscLib.h>

/* defines */

#define USB_TGT_MSC_VENDOR_NAME  "WRS     "
#define USB_TGT_MSC_PRODUCT_NAME "USB Mass Storage"
#define USB_TGT_MSC_PRODUCT_REV  "1.00"

/* locals */

/*******************************************************************************
*
* usbTgtMscLunRead - read data from the lun
*
* This routine reads data from the lun.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbTgtMscLunRead
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun 
    )
    {
    pUSB_TGT_MSC_DEV pUsbTgtMscDev;
    UINT32           uReadLen;
    UINT32           uTotalLen;
    UINT32           uStartBlk;    
    UINT16           uTransferBlks;
    UINT16           uReadBlks;
    STATUS           retVal;
    UINT8 *          pBuf;

    if (pUsbTgtMscLun == NULL)
        {
        USBTGT_MSC_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);
        return ERROR;
        }
    
    pUsbTgtMscDev = pUsbTgtMscLun->pUsbTgtMscDev;
    
    /* Get start Blk number */

    uStartBlk = (UINT32)((pUsbTgtMscDev->cmd[2] << 24) | 
                         (pUsbTgtMscDev->cmd[3] << 16) | 
                         (pUsbTgtMscDev->cmd[4] << 8)  | 
                          pUsbTgtMscDev->cmd[5]);
  
    /* Get total transfer length */

    uTotalLen = OS_UINT32_LE_TO_CPU(pUsbTgtMscDev->pCBW->dCBWDataTransferLength);
    pUsbTgtMscDev->uTotalLen = uTotalLen;
    
    /* Get the Blk number already transfered */
   
    uTransferBlks = (UINT16) (pUsbTgtMscDev->uActLen / pUsbTgtMscLun->perBlockSize);

    /* Update the startBlk and uReadBlks */

    uStartBlk += uTransferBlks;

    /* Get the read blks for this transfer */

    uReadLen = min ((uTotalLen - pUsbTgtMscDev->uActLen), MSC_DATA_BUF_LEN);

    /* If not data to transfer return error let the caller send CSW */

    if (uReadLen == 0)
        {
        USBTGT_MSC_ERR("Read length is zero\n", 1, 2, 3, 4, 5, 6);
        return ERROR;
        }
    
    uReadBlks = (UINT16) (uReadLen / pUsbTgtMscLun->perBlockSize);
    
    USBTGT_MSC_DBG("usbTgtMscLunRead - lunIndex %d startBlk %d uReadBlks %d\n", 
                   pUsbTgtMscLun->lunIndex, uStartBlk, uReadBlks, uTotalLen, 5, 6);

    /* Get empty buffer */
    
    pBuf = usbTgtMscEmptyBufGet(pUsbTgtMscDev);
    
    if (pBuf == NULL)
        {
        USBTGT_MSC_ERR("pBuf is NULL\n", 1, 2, 3, 4, 5, 6);
        return ERROR;
        } 
    
    /* Fill BIO prepare read */

    pUsbTgtMscLun->bio.bio_dev     = pUsbTgtMscLun->devXbdHandle;
    pUsbTgtMscLun->bio.bio_blkno   = uStartBlk;
    pUsbTgtMscLun->bio.bio_bcount  = uReadLen;
    pUsbTgtMscLun->bio.bio_data    = (pVOID)pBuf;
    pUsbTgtMscLun->bio.bio_error   = 0;
    pUsbTgtMscLun->bio.bio_flags   = BIO_READ;
    pUsbTgtMscLun->bio.bio_done    = usbTgtMscDevBioDone;
    pUsbTgtMscLun->bio.bio_caller1 = (void *) &pUsbTgtMscLun->bioDoneSem;
    pUsbTgtMscLun->bio.bio_chain   = NULL;

    /* perform blk read */
    
    retVal = xbdStrategy (pUsbTgtMscLun->devXbdHandle, &pUsbTgtMscLun->bio);
    
    if (retVal == ERROR)
        {
        USBTGT_MSC_ERR("usbTgtMscLunRead - bioBlkRW returned ERROR\n",
                       1, 2, 3, 4, 5, 6); 
        USBTGT_MSC_BUF_RELEASE(pUsbTgtMscDev);
        return ERROR;
        }
    
    if (semTake (&pUsbTgtMscLun->bioDoneSem, WAIT_FOREVER) == ERROR)
        {
        USBTGT_MSC_ERR("usbTgtMscLunRead - semTake bioDoneSem error\n", 
                       1, 2, 3, 4, 5, 6); 
        USBTGT_MSC_BUF_RELEASE(pUsbTgtMscDev);
        return ERROR;
        }
    
   /* Update actual length */

   pUsbTgtMscDev->uActLen += uReadLen;

   /* Update the CSW's dataresidue field */

   pUsbTgtMscDev->pCSW->dCSWDataResidue = 
                       OS_UINT32_CPU_TO_LE((uTotalLen - pUsbTgtMscDev->uActLen));
   
    /*
     * init Data-In ERP on Bulk-In pipe and submit
     * to TCD
     */
    
    if (usbTgtMscBulkErpInit(pUsbTgtMscDev, 
                             pUsbTgtMscDev->pBuf, 
                             uReadLen,
                             USB_PID_IN, 
                             usbTgtMscBulkInErpCallbackData) != OK)
        {
        USBTGT_MSC_ERR("usbTgtMscLunRead - "
                       "usbTgtMscBulkErpInit error\n", 
                       1, 2, 3, 4, 5, 6);
        USBTGT_MSC_BUF_RELEASE(pUsbTgtMscDev);
        return ERROR;
        } 
    
    return OK;
    }

/*******************************************************************************
*
* usbTgtRbcRead - read data from the RBC device
*
* This routine reads data from the RBC block I/O device.
*
* RETURNS: OK, or ERROR
*
* ERRNO: N/A
*
*/

STATUS usbTgtRbcRead
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    )
    {
    STATUS retVal;
    pUSB_TGT_MSC_DEV pUsbTgtMscDev;
    UINT32 uNumBlks;
    UINT32 uReadLen;

    if (pUsbTgtMscLun == NULL)
        {
        USBTGT_MSC_ERR("usbTgtRbcRead - pUsbTgtMscLun is NULL\n",
            1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    pUsbTgtMscDev = pUsbTgtMscLun->pUsbTgtMscDev;

    if (pUsbTgtMscLun->mediaRemoved == TRUE)
        {
        pUsbTgtMscDev->pCSW->bCSWStatus = USB_CSW_STATUS_FAIL;
        usbTgtSetPipeStatus(pUsbTgtMscDev->bulkInPipe,
                            USBTGT_PIPE_STATUS_STALL);
        pUsbTgtMscDev->bulkInPipeState = USB_TGT_MSC_PIPE_STALL;
        usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                               SCSI_SENSE_NOT_READY,
                               SCSI_ADSENSE_LUN_NOT_READY,
                               0x0);
        return ERROR;
        }
    

    if (pUsbTgtMscDev->cmd[0] == RBC_CMD_READ12)
        {
        uNumBlks = (UINT32 )((pUsbTgtMscDev->cmd[6] << 24) |
                             (pUsbTgtMscDev->cmd[7] << 16) |
                             (pUsbTgtMscDev->cmd[8] << 8) |
                             (pUsbTgtMscDev->cmd[9]));
        
        uReadLen =  (uNumBlks * pUsbTgtMscLun->perBlockSize);
        } 
    else
        {
        /* RBC_CMD_READ10 */
        uNumBlks = (UINT32) (((pUsbTgtMscDev->cmd[7] << 8))  | 
                            ((pUsbTgtMscDev->cmd[8])));
        
        uReadLen = uNumBlks * pUsbTgtMscLun->perBlockSize;
        }

    if (uReadLen != 
        OS_UINT32_LE_TO_CPU(pUsbTgtMscDev->pCBW->dCBWDataTransferLength))
        {
        USBTGT_MSC_ERR("CBW Transfer length %x, ReadLen from command %x\n",
            OS_UINT32_LE_TO_CPU(pUsbTgtMscDev->pCBW->dCBWDataTransferLength),
            uReadLen, 3, 4, 5, 6);
        pUsbTgtMscDev->pCSW->bCSWStatus = USB_CSW_STATUS_FAIL;
        usbTgtSetPipeStatus(pUsbTgtMscDev->bulkInPipe,
                            USBTGT_PIPE_STATUS_STALL);
        pUsbTgtMscDev->bulkInPipeState = USB_TGT_MSC_PIPE_STALL;
        return ERROR;
        }

    usbTgtSetPipeStatus(pUsbTgtMscDev->bulkOutPipe,
                        USBTGT_PIPE_STATUS_MSC_CASE_10);

    retVal = usbTgtMscLunRead(pUsbTgtMscLun);

    /* set sense data to no sense */
    
    if (retVal == OK)
        usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData, 
                               SCSI_SENSE_NO_SENSE,
                               SCSI_ADSENSE_NO_SENSE, 
                               0x0);
    else
        {
        usbTgtSetPipeStatus(pUsbTgtMscLun->pUsbTgtMscDev->bulkInPipe, 
                            USBTGT_PIPE_STATUS_STALL);
        pUsbTgtMscLun->pUsbTgtMscDev->bulkInPipeState = USB_TGT_MSC_PIPE_STALL;
        usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData, 
                               SCSI_SENSE_NOT_READY,
                               SCSI_ADSENSE_LUN_NOT_READY, 
                               0x0);
        }

    return retVal;
    }

/*******************************************************************************
*
* usbTgtRbcCapacityRead - read the capacity of the RBC device
*
* This routine reads the capacity of the RBC block I/O device.
*
* RETURNS: OK or ERROR
*
* ERRNO: N/A
*/

STATUS usbTgtRbcCapacityRead
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun,
    UINT8 **         ppData,        /* point to capacity data */
    UINT32 *         pSize          /* size of capacity */
    )
    {
    if (pUsbTgtMscLun == NULL)
        {
        USBTGT_MSC_ERR("usbTgtRbcCapacityRead - pUsbTgtMscLun is NULL\n",
                       1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    *ppData = (UINT8 *)pUsbTgtMscLun->pCapacityData;
    *pSize  = sizeof(CAPACITY_DATA);

    if (pUsbTgtMscLun->mediaRemoved == FALSE)
        {
        /* set sense data to no sense */

        usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                               SCSI_SENSE_NO_SENSE,
                               SCSI_ADSENSE_NO_SENSE,
                               0x0);
        }
    else
        {
        /*
         * set sense key to NOT READY (02h), and an ASC of LOGICAL UNIT
         * NOT READY (04h). set ASCQ to cause not reportable (0x0)
         */

        usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                               SCSI_SENSE_NOT_READY,
                               SCSI_ADSENSE_LUN_NOT_READY,
                               0x0);
        }

    return OK;
    }

/*******************************************************************************
*
* usbTgtRbcStartStop - start or stop the RBC device
*
* This routine starts or stops the RBC block I/O device.
*
* RETURNS: OK, or ERROR
*
* ERRNO: N/A
*/

STATUS usbTgtRbcStartStop
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    )
    {
    UINT8 pwrConditions; /* power conditions */
    UINT8 loEj;  /* load eject */
    UINT8 start;  /* enable or disable media */
                  /* access operations */
    pUSB_TGT_MSC_DEV pUsbTgtMscDev;

    if (pUsbTgtMscLun == NULL)
        {
        USBTGT_MSC_ERR("usbTgtRbcCapacityRead - pUsbTgtMscLun is NULL\n",
            1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    pUsbTgtMscDev = pUsbTgtMscLun->pUsbTgtMscDev;
    pwrConditions = (UINT8)(pUsbTgtMscDev->cmd[4] & 0xf0); /* power conditions */
    loEj  = (UINT8)((pUsbTgtMscDev->cmd[4] & 0x2) >> 1);  /* load eject */
    start = (UINT8)(pUsbTgtMscDev->cmd[4] & 0x1);  /* enable or disable media */
                                                   /* access operations */
    if (pwrConditions != 0)
        {
        switch (pwrConditions)
            {
            case 1:
                /* Place device in Active condition */

                break;

            case 2:
                /* Place device in Idle condition */

                break;

            case 3:
                /* Place device in Standby condition */

                break;

            case 5:
                /* Place device in Sleep condition */

                break;

            case 7:
               /* optional control */

            default:
                break;
            }

        /*
         * Save power conditions:
         * NOTE: The device shall terminate any command received that requires
         * more power consumption than allowed by the START STOP UNIT command's
         * most recent power condition setting. Status shall be ERROR, the sense
         * key to ILLEGAL REQUEST (05h), and the ASC/ASCQ to LOW POWER CONDITION
         * ACTIVE (5Eh/00h). It is not an error to request a device be placed
         * into the same power consumption level in which it currently resides.
         */

        pUsbTgtMscLun->pwrConditions = pwrConditions;
        }
    else
        {
        /*
         * If a removable medium device, in either PREVENT state 01b or 11b,
         * receives a START STOP UNIT command with the POWER CONDITIONS field
         * set to the Sleep state (5), this routine will return ERROR with
         * the sense key set to ILLEGAL REQUEST (05h), and the ASC/ASCQ to
         * ILLEGAL POWER CONDITION REQUEST (2Ch/05h).
         */

        if (pUsbTgtMscLun->pwrConditions == 0x5)
            {
            if (pUsbTgtMscLun->mediaPrevent == 0x1 ||
                pUsbTgtMscLun->mediaPrevent == 0x3)
                {
                usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                                       SCSI_SENSE_ILLEGAL_REQUEST,
                                       SCSI_ADSENSE_CMDSEQ_ERROR,
                                       SCSI_SENSEQ_ILLEGAL_POWER_CONDITION_REQUEST);
                USBTGT_MSC_ERR("usbTgtRbcStartStop - mediaPrevent\n",
                    1, 2, 3, 4, 5, 6);
                return ERROR;
                }
            }

        /* either load or eject the media */

        if (loEj == 1)
            {
            if (start == 1)
                {
                /* load medium: set media removed flag to loaded */

                pUsbTgtMscLun->mediaRemoved = FALSE;
                }
            else
                {
                /* unload medium: set media removed flag to ejected */

                pUsbTgtMscLun->mediaRemoved = TRUE;
                pUsbTgtMscLun->mediaReady = FALSE;		/* added for Windows's "eject" operarion */
                }
            }
        else
            {
            /* get media ready for data transfer */

            if (start == 1)

                /* media ready for data transfers */

                pUsbTgtMscLun->mediaReady = TRUE;
            else

                /* media inaccessible for data transfers */

                pUsbTgtMscLun->mediaReady = FALSE;
            }
        }

    /* set sense data to no sense */

    usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                           SCSI_SENSE_NO_SENSE,
                           SCSI_ADSENSE_NO_SENSE,
                           0);

    return OK;
    }

/*******************************************************************************
*
* usbTgtRbcPreventAllowRemoval - prevent or allow the removal of the RBC device
*
* This routine prevents or allows the removal of the RBC block I/O device.
*
* RETURNS: OK, or ERROR
*
* ERRNO: N/A
*/

STATUS usbTgtRbcPreventAllowRemoval
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    )
    {
    UINT8 prevent;
    pUSB_TGT_MSC_DEV pUsbTgtMscDev;

    if (pUsbTgtMscLun == NULL)
        {
        USBTGT_MSC_ERR("usbTgtRbcPreventAllowRemoval - pUsbTgtMscLun is NULL\n",
            1, 2, 3, 4, 5, 6);
        return ERROR;
        }
    pUsbTgtMscDev = pUsbTgtMscLun->pUsbTgtMscDev;
    prevent = (UINT8)(pUsbTgtMscDev->cmd[4] & 0x3);   /* prevent media removal flag */

    switch (prevent)
        {
        case 0:
           /*
            * Medium removal shall be allowed from both the data transport
            * element and the attached medium changer (if any)
            */

            break;

        case 1:
            /*
             * Medium removal shall be prohibited from the data transport
             * element but allowed from the attached medium changer (if any)
             */

            break;

        case 2:
            /*
             * Medium removal shall be allowed for the data transport element
             * but prohibited for the attached medium changer.
             */

            break;

        case 3:
            /*
             * Medium removal shall be prohibited for both the data transport
             * element and the attached medium changer.
             */

            break;

        default:
            USBTGT_MSC_ERR("usbTgtRbcPreventAllowRemoval - prevent error\n",
                1, 2, 3, 4, 5, 6);

            /*
             * set the sense key to ILLEGAL REQUEST (0x5) and set the ASC to
             * INVALID COMMAND OPERATION CODE (0x20)
             */

            usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                                   SCSI_SENSE_ILLEGAL_REQUEST,
                                   SCSI_ADSENSE_ILLEGAL_COMMAND,
                                   0);
            return ERROR;
        }

    /* save prevent/allow flag for use in usbTgtRbcStartStop() cmd */

    pUsbTgtMscLun->mediaPrevent = prevent;

    /* set sense data to no sense */

    usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                           SCSI_SENSE_NO_SENSE,
                           SCSI_ADSENSE_NO_SENSE,
                           0);

    return OK;
    }

/*******************************************************************************
*
* usbTgtRbcVerify - verify the last data written to the RBC device
*
* This routine verifies the last data written to the RBC block I/O device.
*
* RETURNS: OK or ERROR.
*
* ERRNO: N/A
*/

STATUS usbTgtRbcVerify
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    )
    {
    if (pUsbTgtMscLun == NULL)
        {
        USBTGT_MSC_ERR("usbTgtRbcVerify - pUsbTgtMscLun is NULL\n",
            1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    /*
     * Assume last WRITE command was good.
     * set sense key to NO SENSE (0x0),
     * set additional sense code to NO ADDITIONAL SENSE INFORMATION
     */

    usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                           SCSI_SENSE_NO_SENSE,
                           SCSI_ADSENSE_NO_SENSE,
                           0);

    return OK;
    }

/*******************************************************************************
*
* usbTgtRbcWrite - write to the RBC device
*
* This routine writes to the RBC block I/O device.
*
* RETURNS: OK or ERROR
*
* ERRNO: N/A
*/

STATUS usbTgtRbcWrite
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    )
    {
    UINT32    startLBA = 0;    /* logical block address */
    UINT32    uTotalLen = 0;   /* total length to write */
    UINT32    uWriteLen;
    UINT8 *   pBuf;
    pUSB_TGT_MSC_DEV pUsbTgtMscDev;

    if (pUsbTgtMscLun == NULL)
        {
        USBTGT_MSC_ERR("usbTgtRbcWrite - pUsbTgtMscLun is NULL\n",
                       1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    pUsbTgtMscDev = pUsbTgtMscLun->pUsbTgtMscDev;

    /* save starting LBA from arg[2] - arg[5]> save transfer length */

    startLBA = (pUsbTgtMscDev->cmd[2] << 24) |
               (pUsbTgtMscDev->cmd[3] << 16) |
               (pUsbTgtMscDev->cmd[4] << 8)  |
                pUsbTgtMscDev->cmd[5];

    uTotalLen = OS_UINT32_LE_TO_CPU(pUsbTgtMscDev->pCBW->dCBWDataTransferLength);
    pUsbTgtMscDev->uTotalLen = uTotalLen;
    uWriteLen = min(uTotalLen, MSC_DATA_BUF_LEN);

    
    if (uWriteLen == 0)
        {
        USBTGT_MSC_ERR("usbTgtRbcWrite - write len is zero\n",
                       1, 2, 3, 4, 5, 6);
        return ERROR;
        }
    
       
    USBTGT_MSC_DBG("usbTgtRbcWrite - lunIndex %d startLBA %d "
                   "uTotalLen %d uWriteLen %d\n", 
                   pUsbTgtMscLun->lunIndex, startLBA, 
                   uTotalLen, uWriteLen, 5, 6);
    
    pBuf = usbTgtMscEmptyBufGet(pUsbTgtMscDev);

    if (pBuf == NULL)
        {
        USBTGT_MSC_ERR("usbTgtRbcWrite - pUsbTgtMscBuf is NULL\n",
                       1, 2, 3, 4, 5, 6);
        return ERROR;
        }
    
    /* Submit a bulk out erp to get the write data back */

    if (usbTgtMscBulkErpInit(pUsbTgtMscDev, 
                             pBuf, 
                             uWriteLen,
                             USB_PID_OUT, 
                             usbTgtMscBulkOutErpCallbackData) != OK)
        {
        USBTGT_MSC_ERR("usbTgtMscBulkOutErpCallbackCBW - "
                       "usbTgtMscBulkErpInit error\n", 
                       1, 2, 3, 4, 5, 6);
        USBTGT_MSC_BUF_RELEASE(pUsbTgtMscDev);
        return ERROR;
        }

    /* set sense data to no sense */

    if (pUsbTgtMscLun->bReadOnly == TRUE)
        {
        usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                               SCSI_SENSE_DATA_PROTECT,
                               SCSI_ADWRITE_PROTECT,
                               0x0);
        }
    else
        {
        usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                               SCSI_SENSE_NO_SENSE,
                               SCSI_ADSENSE_NO_SENSE,
                               0x0);
        }
    return OK;
    }

/*******************************************************************************
*
* usbTgtRbcInquiry - retrieve inquiry data from the RBC device
*
* This routine retrieves inquiry data from the RBC block I/O device.
*
* RETURNS: OK or ERROR
*
* ERRNO: N/A
*/

STATUS usbTgtRbcInquiry
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun,
    UINT8 **         ppData,        /* location of inquiry data on device */
    UINT32 *         pSize          /* size of inquiry data on device */
    )
    {
    UINT8 evpd  ; /* enable vital product data bit */
    UINT8 cmdDt ; /* command support data bit */
    UINT8 pageCode ;    /* page code */
    UINT8 allocLgth;    /* number of bytes of inquiry */
    UINT8 * pBuf;
    pUSB_TGT_MSC_DEV pUsbTgtMscDev;
    if (pUsbTgtMscLun == NULL)
        {
        USBTGT_MSC_ERR("usbTgtRbcWrite12 - pUsbTgtMscLun is NULL\n",
            1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    pUsbTgtMscDev = pUsbTgtMscLun->pUsbTgtMscDev;

    evpd      = (UINT8)(pUsbTgtMscDev->cmd[1] & 0x1); /* enable vital product data bit */
    cmdDt     = (UINT8)(pUsbTgtMscDev->cmd[1] & 0x2); /* command support data bit */
    pageCode  = pUsbTgtMscDev->cmd[2];                /* page code */
    allocLgth = pUsbTgtMscDev->cmd[4];                /* number of bytes of inquiry */

    pBuf = usbTgtMscEmptyBufGet(pUsbTgtMscDev);
    if (pBuf == NULL)
        {
        USBTGT_MSC_ERR("usbTgtRbcInquiry - pUsbTgtMscBuf is NULL\n",
                       1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    *ppData = NULL;
    *pSize = 0;

    if (evpd == 1)
        {
        /* return vital product data */

        switch(pageCode)
            {
            case VPD_SUPPORTED_PAGE_CODE:
                /* return supported vital product data page codes: */

                *ppData = (UINT8 *)pUsbTgtMscDev->pBuf;
                usbTgtMscVpdSupportedPageInit((VPD_SUPPORTED_PAGE *)pUsbTgtMscDev->pBuf);
                *pSize  = sizeof(VPD_SUPPORTED_PAGE);
                break;

            case VPD_UNIT_SERIAL_NUM_PAGE_CODE:
                /* transmit vpdSerialNumPage */

                *ppData = (UINT8 *)pUsbTgtMscDev->pBuf;
                usbTgtMscVpdSerialNumPageInit((VPD_UNIT_SERIAL_NUM_PAGE *)pUsbTgtMscDev->pBuf,
                    (UINT8)pUsbTgtMscLun->lunIndex);
                *pSize  = sizeof(VPD_UNIT_SERIAL_NUM_PAGE);
                break;

            case VPD_DEVICE_ID_PAGE_CODE:
                /* transmit vpdDevIdPage */

                *ppData = (UINT8 *)pUsbTgtMscDev->pBuf;
                usbTgtMscVpdDevIdPageInit((VPD_DEVICE_ID_PAGE *)pUsbTgtMscDev->pBuf);
                *pSize  = sizeof(VPD_DEVICE_ID_PAGE);
                break;

            default:
                USBTGT_MSC_ERR("usbTgtRbcInquiry - pageCode error\n",
                               1, 2, 3, 4, 5, 6);

                /* Release the buffer */

                USBTGT_MSC_BUF_RELEASE(pUsbTgtMscDev);

                /*
                 * return ERROR status; set the sense key set to
                 * ILLEGAL REQUEST and set the additional sense code
                 * of INVALID FIELD IN CDB.
                 */

                usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                                       SCSI_SENSE_ILLEGAL_REQUEST,
                                       SCSI_ADSENSE_INVALID_CDB,
                                       0);
                return ERROR;
            }
        }
    else if (cmdDt == 1)
        {
        /*
         * return command support data. This implementation is optional;
         * therefore, the data returned will contain the peripheral qualifier,
         * device type byte, and 001b entered in the SUPPORT field.
         */

        /* transmit cmdData */

        *ppData = (UINT8 *)pUsbTgtMscDev->pBuf;
        usbTgtMscCmdDataInit((COMMAND_DATA *)pUsbTgtMscDev->pBuf);
        *pSize  = sizeof(COMMAND_DATA);
        }
    else if (pageCode == 0)
        {
        /* return standard inquiry data must be at least 36 bytes */

        *ppData = (UINT8 *)pUsbTgtMscDev->pBuf;
        usbTgtMscStdInquiryDataInit((STD_INQUIRY_DATA *)pUsbTgtMscDev->pBuf, pUsbTgtMscLun);

        *pSize  = sizeof(STD_INQUIRY_DATA);
        }
    else
        {
        USBTGT_MSC_ERR("usbTgtRbcInquiry - evpd error\n",
                       1, 2, 3, 4, 5, 6);

        /* Release the buffer */

        USBTGT_MSC_BUF_RELEASE(pUsbTgtMscDev);

        /*
         * return ERROR status with the sense key set to
         * ILLEGAL REQUEST and an additional sense code of
         * INVALID FIELD IN CDB.
         */

        usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                               SCSI_SENSE_ILLEGAL_REQUEST,
                               SCSI_ADSENSE_INVALID_CDB,
                               0);
        return ERROR;
        }

    /* return only what was requested */

    if (allocLgth < *pSize)
        *pSize = allocLgth;

    usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                           SCSI_SENSE_NO_SENSE,
                           SCSI_ADSENSE_NO_SENSE,
                           0x0);
    return OK;
    }

/*******************************************************************************
*
* usbTgtRbcModeSelect - select the mode parameter page of the RBC device
*
* This routine selects the mode parameter page of the RBC block I/O device.
* For non-removable medium devices the SAVE PAGES (SP) bit shall be set to one.
* This indicates that the device shall perform the specified MODE SELECT
* operation and shall save, to a non-volatile vendor-specific location, all the
* changeable pages, including any sent with the command. Application clients
* should issue MODE SENSE(6) prior to each MODE SELECT(6) to determine supported
* pages, page lengths, and other parameters.
*
* RETURNS: OK or ERROR
*
* ERRNO: N/A
*/

STATUS usbTgtRbcModeSelect
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun,
    UINT8 **         ppData,        /* location of mode parameter data on device */
    UINT32 *         pSize          /* size of mode parameter data on device */
    )
    {
    UINT8 pgfmt;  /* page format */
    UINT8 savePgs; /* save pages */
    UINT8 paramLstLgth;     /* parameter list length */
    pUSB_TGT_MSC_DEV pUsbTgtMscDev;

    if (pUsbTgtMscLun == NULL)
        {
        USBTGT_MSC_ERR("usbTgtRbcModeSelect - pUsbTgtMscLun is NULL\n",
            1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    pUsbTgtMscDev = pUsbTgtMscLun->pUsbTgtMscDev;
    pgfmt         = (UINT8)((pUsbTgtMscDev->cmd[1] & 0x10 ) >> 4);  /* page format */
    savePgs       = (UINT8)(pUsbTgtMscDev->cmd[1] & 0x1); /* save pages */
    paramLstLgth  = pUsbTgtMscDev->cmd[4];     /* parameter list length */

    /*
     * The removable media bit (RMB) in the stdInquiry data
     * page is one; therefore, support for the save pages bit
     * is optional
     */

    if (pgfmt == 1 && savePgs == 0)
        {
        *ppData = (UINT8 *)&pUsbTgtMscLun->deviceParamList;
        *pSize = sizeof(MODE_PARAMETER_LIST);

        /*
         * return ERROR if paramLstLgth truncates any of the mode
         * parameter list (header and mode page). Set sense key to
         * ILLEGAL REQUEST, and ASC to PARAMETER LIST LENGTH ERROR.
         */

        if (paramLstLgth < *pSize)
            {
            USBTGT_MSC_ERR("usbTgtRbcModeSelect - paramLstLgth < *pSize\n",
                1, 2, 3, 4, 5, 6);

            usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                                   SCSI_SENSE_ILLEGAL_REQUEST,
                                   SCSI_PARAMETER_LIST_LENGTH_ERROR,
                                   0);
            return ERROR;
            }
        }
    else
        {
        USBTGT_MSC_ERR("usbTgtRbcModeSelect error\n",
            1, 2, 3, 4, 5, 6);
        /*
         * set sense key to ILLEGAL REQUEST (05h) and an ASC of INVALID FIELD
         * IN CDB (24h).
         */

        usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                               SCSI_SENSE_ILLEGAL_REQUEST,
                               SCSI_ADSENSE_INVALID_CDB,
                               0);
        return ERROR;
        }

    usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                           SCSI_SENSE_NO_SENSE,
                           SCSI_ADSENSE_NO_SENSE,
                           0);

    return OK;
    }

/*******************************************************************************
*
* usbTgtRbcModeSense - retrieve sense data from the RBC device
*
* This routine retrieves sense data from the RBC block I/O device.
*
* RETURNS: OK or ERROR
*
* ERRNO: N/A
*/

STATUS usbTgtRbcModeSense
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun,
    UINT8 **         ppData,        /* location mode parameter data on device */
    UINT32 *         pSize          /* size of mode parameter data on device */
    )
    {
    UINT8 pgCtrl; /* page control feild */
    UINT8 pgCode;    /* page code */
    UINT8 allocLgth;        /* allocation length */
    pUSB_TGT_MSC_DEV pUsbTgtMscDev;

    if (pUsbTgtMscLun == NULL)
        {
        USBTGT_MSC_ERR("usbTgtRbcModeSense - pUsbTgtMscLun is NULL\n",
            1, 2, 3, 4, 5, 6);
        return ERROR;
        }
    pUsbTgtMscDev = pUsbTgtMscLun->pUsbTgtMscDev;
    pgCtrl     = (UINT8)((pUsbTgtMscDev->cmd[2] & 0xc0) >> 6); /* page control feild */
    pgCode     = (UINT8)(pUsbTgtMscDev->cmd[2] & 0x3f);    /* page code */
    allocLgth  = pUsbTgtMscDev->cmd[4];        /* allocation length */

    *ppData = NULL;
    *pSize = 0;

    if (pgCode == RBC_MODE_PARAMETER_PAGE)
        {
        switch (pgCtrl)
            {
            case 0: /* return current values */
            case 2: /* return default values */
                *ppData = (UINT8 *)&pUsbTgtMscLun->deviceParamList;
                *pSize = sizeof(MODE_PARAMETER_LIST);
                break;

            case 1:
                /* return changeable values */

                /*
                 * return a mask where the bit fields of the mode parameters
                 * that are changeable shall are one; otherwise, zero.
                 */

                *ppData = (UINT8 *)&pUsbTgtMscLun->deviceParamListMask;
                *pSize = sizeof(MODE_PARAMETER_LIST);
                break;

            case 3:
                USBTGT_MSC_ERR("usbTgtRbcModeSense - pgCtrl error\n",
                    1, 2, 3, 4, 5, 6);

                /* saved values are not implemented */
                /*
                 * termine with ERROR status,
                 * set sense key set to ILLEGAL REQUEST
                 * set additional sense code set to SAVING PARAMETERS
                 *
                 * NOT SUPPORTED.
                 */

                usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                                       SCSI_SENSE_ILLEGAL_REQUEST,
                                       SCSI_ADSENSE_SAVE_ERROR,
                                       0);
                return ERROR;

            default:
                USBTGT_MSC_ERR("usbTgtRbcModeSense - pgCtrl is %d\n",
                               1, 2, 3, 4, 5, 6);

                /* error */
                /*
                 * terminate with ERROR status,
                 * set sense key set to ILLEGAL REQUEST
                 * set additional sense code set to INVALID FIELD IN CDB.
                 */

                usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                                       SCSI_SENSE_ILLEGAL_REQUEST,
                                       SCSI_ADSENSE_INVALID_CDB,
                                       0);
                return ERROR;
            }
        }
    else
        {
        /*
         * If not an RBC device, return just the header with no page info
         * update the data length according to the transfer context
         */

        pUsbTgtMscLun->deviceParamList.header.dataLen =
                           sizeof(MODE_PARAMETER_HEADER) - 1;

        *ppData = (UINT8 *)&pUsbTgtMscLun->deviceParamList.header;
        *pSize = sizeof(MODE_PARAMETER_HEADER);
        }

    /* return only what was requested */

    if (allocLgth < *pSize)
        *pSize = allocLgth;

    /* set sense data */

    usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                           SCSI_SENSE_NO_SENSE,
                           SCSI_ADSENSE_NO_SENSE,
                           0);

    return OK;
    }

/*******************************************************************************
*
* usbTgtRbcModeSelect10 - select the mode parameter page of the RBC device
*
* This routine selects the mode parameter page of the RBC block I/O device.
* For non-removable medium devices the SAVE PAGES (SP) bit shall be set to one.
* This indicates that the device shall perform the specified MODE SELECT
* operation and shall save, to a non-volatile vendor-specific location, all the
* changeable pages, including any sent with the command. Application clients
* should issue MODE SENSE(10) prior to each MODE SELECT(10) to determine supported
* pages, page lengths, and other parameters.
*
* RETURNS: OK or ERROR
*
* ERRNO: N/A
*/

STATUS usbTgtRbcModeSelect10
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun,
    UINT8 **         ppData,        /* location of mode parameter data on device */
    UINT32 *         pSize          /* size of mode parameter data on device */
    )
    {
    UINT8 pgfmt;        /* page format */
    UINT8 savePgs;      /* save pages */
    UINT16 paramLstLgth;/* parameter list length */
    pUSB_TGT_MSC_DEV pUsbTgtMscDev;

    if (pUsbTgtMscLun == NULL)
        {
        USBTGT_MSC_ERR("usbTgtRbcModeSelect10 - pUsbTgtMscLun is NULL\n",
            1, 2, 3, 4, 5, 6);
        return ERROR;
        }
    pUsbTgtMscDev = pUsbTgtMscLun->pUsbTgtMscDev;
    pgfmt = (UINT8)((pUsbTgtMscDev->cmd[1] & 0x10) >> 4);/* page format */
    savePgs = (UINT8)(pUsbTgtMscDev->cmd[1] & 0x1);      /* save pages */
    paramLstLgth  = (UINT16)((pUsbTgtMscDev->cmd[7] << 8) |
                              pUsbTgtMscDev->cmd[8]);/* parameter list length */

    /*
     * The removable media bit (RMB) in the stdInquiry data
     * page is one; therefore, support for the save pages bit
     * is optional
     */

    if (pgfmt == 1 && savePgs == 0)
        {
        *ppData = (UINT8 *)&pUsbTgtMscLun->deviceParamList10;
        *pSize = sizeof(MODE_PARAMETER_LIST10);

        /*
         * return ERROR if paramLstLgth truncates any of the mode
         * parameter list (header and mode page). Set sense key to
         * ILLEGAL REQUEST, and ASC to PARAMETER LIST LENGTH ERROR.
         */

        if (paramLstLgth < *pSize)
            {
            USBTGT_MSC_ERR("usbTgtRbcModeSelect10 - paramLstLgth < *pSize\n",
                1, 2, 3, 4, 5, 6);

            usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                                   SCSI_SENSE_ILLEGAL_REQUEST,
                                   SCSI_PARAMETER_LIST_LENGTH_ERROR,
                                   0);
            return ERROR;
            }
        }
    else
        {
        USBTGT_MSC_ERR("usbTgtRbcModeSelect10 error\n",
            1, 2, 3, 4, 5, 6);

        /*
         * set sense key to ILLEGAL REQUEST (05h) and an ASC of INVALID FIELD
         * IN CDB (24h).
         */

        usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                               SCSI_SENSE_ILLEGAL_REQUEST,
                               SCSI_ADSENSE_INVALID_CDB,
                               0);
        return ERROR;
        }

    usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                           SCSI_SENSE_NO_SENSE,
                           SCSI_ADSENSE_NO_SENSE,
                           0);

    return OK;
    };

/*******************************************************************************
*
* usbTgtRbcModeSense10 - request for mode sense 10 command
*
* This routine retrieves sense data from the RBC block I/O device.
*
* RETURNS: OK or ERROR
*
* ERRNO: N/A
*/

STATUS usbTgtRbcModeSense10
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun,
    UINT8 **         ppData,        /* location mode parameter data on device */
    UINT32 *         pSize          /* size of mode parameter data on device */
    )
    {
    UINT8 pgCtrl;      /* page control feild */
    UINT8 pgCode;      /* page code */
    UINT16 allocLgth;  /* allocation length */
    pUSB_TGT_MSC_DEV pUsbTgtMscDev;

    if (pUsbTgtMscLun == NULL)
        {
        USBTGT_MSC_ERR("usbTgtRbcModeSelect10 - pUsbTgtMscLun is NULL\n",
            1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    pUsbTgtMscDev = pUsbTgtMscLun->pUsbTgtMscDev;
    pgCtrl = (UINT8)((pUsbTgtMscDev->cmd[2] & 0xc0) >> 6);/* page control feild */
    pgCode = (UINT8)(pUsbTgtMscDev->cmd[2] & 0x3f);      /* page code */
    allocLgth = (UINT16)((pUsbTgtMscDev->cmd[7]<< 8 ) |
                          pUsbTgtMscDev->cmd[8]);/* allocation length */

    *ppData = NULL;
    *pSize = 0;

    if (pgCode == RBC_MODE_PARAMETER_PAGE)
        {
        switch (pgCtrl)
            {
            case 0: /* return current values */
            case 2: /* return default values */
                *ppData = (UINT8 *)&pUsbTgtMscLun->deviceParamList10;
                *pSize = sizeof(MODE_PARAMETER_LIST10);
                break;

            case 1:
                /* return changeable values */

                /*
                 * return a mask where the bit fields of the mode parameters
                 * that are changeable shall are one; otherwise, zero.
                 */

                *ppData = (UINT8 *)&pUsbTgtMscLun->deviceParamListMask10;
                *pSize = sizeof(MODE_PARAMETER_LIST10);
                break;

            case 3:
                USBTGT_MSC_ERR("usbTgtRbcModeSense10 - pgCtrl error\n",
                    1, 2, 3, 4, 5, 6);

                /* saved values are not implemented */
                /*
                 * termine with ERROR status,
                 * set sense key set to ILLEGAL REQUEST
                 * set additional sense code set to SAVING PARAMETERS
                 *
                 * NOT SUPPORTED.
                 */

                usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                                       SCSI_SENSE_ILLEGAL_REQUEST,
                                       SCSI_ADSENSE_SAVE_ERROR,
                                       0);
                return ERROR;

            default:
                USBTGT_MSC_ERR("usbTgtRbcModeSense10 - pgCtrl is %d\n",
                    1, 2, 3, 4, 5, 6);

                /* error */
                /*
                 * terminate with ERROR status,
                 * set sense key set to ILLEGAL REQUEST
                 * set additional sense code set to INVALID FIELD IN CDB.
                 */

                usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                                       SCSI_SENSE_ILLEGAL_REQUEST,
                                       SCSI_ADSENSE_INVALID_CDB,
                                       0);
                return ERROR;
            }
        }
    else
        {
        /* If not an RBC device, return just the header with no page info */

        *ppData = (UINT8 *)&pUsbTgtMscLun->deviceParamList10.header;
        *pSize = sizeof(MODE_PARAMETER_HEADER10);
        }

    /* return only what was requested */

    if (allocLgth < *pSize)
        *pSize = allocLgth;

    /* set sense data */

    usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                           SCSI_SENSE_NO_SENSE,
                           SCSI_ADSENSE_NO_SENSE,
                           0);

    return OK;
    }

/*******************************************************************************
*
* usbTgtRbcTestUnitReady - test if the RBC device is ready
*
* This routine tests whether the RBC block I/O device is ready.
*
* RETURNS: OK or ERROR
*
* ERRNO: N/A
*/

STATUS usbTgtRbcTestUnitReady
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    )
    {
    if (pUsbTgtMscLun == NULL)
        {
        USBTGT_MSC_ERR("usbTgtRbcTestUnitReady - pUsbTgtMscLun is NULL\n",
            1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    USBTGT_MSC_DBG("usbTgtRbcTestUnitReady - lunIndex %d, mediaReady %d\n",
        pUsbTgtMscLun->lunIndex, pUsbTgtMscLun->mediaReady, 3, 4, 5, 6);

    if (pUsbTgtMscLun->mediaReady == TRUE)
        {
        /* 
         * Set sense code as SCSI_ADSENSE_MEDIUM_CHANGED to notify the 
         * host that the removeable media may have been changed.
         */
         
        if (pUsbTgtMscLun->mediaChanged == TRUE)
            {
            pUsbTgtMscLun->mediaChanged = FALSE;
            
            usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                       SCSI_SENSE_UNIT_ATTENTION,
                       SCSI_ADSENSE_MEDIUM_CHANGED,
                       0);
            return ERROR;
            }
        else
            {
            /*
             * Set sense key to NO SENSE (0x0) and set additional sense code
             * to NO ADDITIONAL SENSE INFORMATION
             */
             
            usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                                   SCSI_SENSE_NO_SENSE,
                                   SCSI_ADSENSE_NO_SENSE,
                                   0);
            
            return OK;
            }        
        }
    else
        {
        usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                               SCSI_SENSE_NOT_READY,
                               SCSI_ADSENSE_NO_MEDIA_IN_DEVICE,
                               0);
        return ERROR;
        }
    }

/*******************************************************************************
*
* usbTgtRbcBufferWrite - write micro-code to the RBC device
*
* This routine writes micro-code to the RBC block I/O device.
*
* RETURNS: OK or ERROR
*
* ERRNO: N/A
*/

STATUS usbTgtRbcBufferWrite
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun,
    UINT8 **         ppData,        /* micro-code location on device */
    UINT32 *         pSize          /* size of micro-code location on device */
    )
    {
    UINT32 offset;          /* offset */
    UINT32 paramListLgth;   /* paramter list length */
    UINT8  mode;            /* mode */
    UINT8 *  pBuf;
    pUSB_TGT_MSC_DEV pUsbTgtMscDev;

    if (pUsbTgtMscLun == NULL)
        {
        USBTGT_MSC_ERR("usbTgtRbcBufferWrite - pUsbTgtMscLun is NULL\n",
            1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    pUsbTgtMscDev = pUsbTgtMscLun->pUsbTgtMscDev;
    pBuf = usbTgtMscEmptyBufGet(pUsbTgtMscDev);

    if (pBuf == NULL)
        {
        USBTGT_MSC_ERR("usbTgtRbcBufferWrite - pUsbTgtMscBuf is NULL\n",
                       1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    mode = (UINT8)(pUsbTgtMscDev->cmd[1] & 0x7);
    offset = (pUsbTgtMscDev->cmd[3] << 16) |
             (pUsbTgtMscDev->cmd[4] << 8) |
              pUsbTgtMscDev->cmd[5];

    paramListLgth = (pUsbTgtMscDev->cmd[6] << 16) |
                    (pUsbTgtMscDev->cmd[7] << 8) |
                     pUsbTgtMscDev->cmd[8];

    switch(mode)
        {
        case 5:
            /*
             * vendor-specific Microcode or control information is
             * transferred to the device and saved saved to
             * non-volatile memory
             */

            break;

        case 7:
            /*
             * vendor-specific microcode or control information is
             * transferred to the device with two or more WRITE BUFFER commands.
             */

            break;

        default:
            USBTGT_MSC_ERR("usbTgtRbcBufferWrite - mode is %d\n",
                mode, 2, 3, 4, 5, 6);

            /* other modes optional for RBC devices and not implemented */

            /*
             * set sense key to ILLEGAL REQUEST (05h) and an ASC of COMMAND
             * SEQUENCE ERROR (2Ch)
             */
            USBTGT_MSC_BUF_RELEASE(pUsbTgtMscDev);

            usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                                   SCSI_SENSE_ILLEGAL_REQUEST,
                                   SCSI_ADSENSE_CMDSEQ_ERROR,
                                   0x0);
            return ERROR;
        }

    /*
     * The PARAMETER LIST LENGTH field specifies the maximum number of bytes
     * that sent by the host to be stored in the specified buffer beginning
     * at the buffer offset. If the BUFFER OFFSET and PARAMETER LIST LENGTH
     * fields specify a transfer in excess of the buffer capacity, return
     * ERROR and set the sense key to ILLEGAL REQUEST with an additional
     * sense code of INVALID FIELD IN CDB.
     */

    if (paramListLgth + offset > (pUsbTgtMscLun->nBlocks * pUsbTgtMscLun->perBlockSize))
        {
        USBTGT_MSC_ERR("usbTgtRbcBufferWrite - PARAMETER LIST LENGTH field is %d\n",
            paramListLgth + offset, 2, 3, 4, 5, 6);

        USBTGT_MSC_BUF_RELEASE(pUsbTgtMscDev);

        usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                               SCSI_SENSE_ILLEGAL_REQUEST,
                               SCSI_ADSENSE_INVALID_CDB,
                               0x0);
        return ERROR;
        }

    usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                           SCSI_SENSE_NO_SENSE,
                           SCSI_ADSENSE_NO_SENSE,
                           0x0);

    *ppData = (UINT8 *)pUsbTgtMscDev->pBuf;
    *pSize = MSC_DATA_BUF_LEN;

    return OK;
    }

/* optional routines */

/*******************************************************************************
*
* usbTgtRbcFormat - format the RBC device
*
* This routine formats the RBC block I/O device.
*
* RETURNS: ERROR to indicate the command is unsupported or anything wrong
*
* ERRNO: N/A
*/

STATUS usbTgtRbcFormat
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    )
    {
    if (pUsbTgtMscLun == NULL)
        {
        USBTGT_MSC_ERR("usbTgtRbcBufferWrite - pUsbTgtMscLun is NULL\n",
                       1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    USBTGT_MSC_WARN("usbTgtRbcFormat - not supported\n",
                    1, 2, 3, 4, 5, 6);

    /*
     * This routine should not be called because the mode sense data indicates
     * the unit cannot be formatted. If this routine is called, return a status
     * of ERROR, a sense key of MEDIA ERROR (03h), an ASC/ASCQ
     * of FORMAT COMMAND FAILED (31h /01h).
     */

    usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                           SCSI_SENSE_MEDIUM_ERROR,
                           SCSI_ADSENSE_FORMAT_ERROR,
                           0x1);
    return ERROR;
    }

/*******************************************************************************
*
* usbTgtRbcPersistentReserveIn - send reserve data to the host
*
* This routine requests reserve data to be sent to the initiator.
*
* RETURNS: ERROR to indicate the command is unsupported or anything wrong
*
* ERRNO: N/A
*/

STATUS usbTgtRbcPersistentReserveIn
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun,
    UINT8 **         ppData,        /* location of reserve data on device */
    UINT32 *         pSize          /* size of reserve data */
    )
    {
    *ppData = NULL;
    *pSize  = 0;

    if (pUsbTgtMscLun == NULL)
        {
        USBTGT_MSC_ERR("usbTgtRbcPersistentReserveIn - pUsbTgtMscLun is NULL\n",
                       1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    USBTGT_MSC_WARN("usbTgtRbcPersistentReserveIn - not supported\n",
                    1, 2, 3, 4, 5, 6);

    /*
     * This routine is optional and isn't implemented. If the routine is called,
     * return a status of ERROR, a sense key of ILLEGAL REQUEST (05h),
     * and an ASC/ASCQ of INVALID COMMAND OPERATION CODE (20h).
     */

    usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                           SCSI_SENSE_ILLEGAL_REQUEST,
                           SCSI_ADSENSE_ILLEGAL_COMMAND,
                           0x0);

    return ERROR;
    }

/*******************************************************************************
*
* usbTgtRbcPersistentReserveOut - reserve resources on the RBC device
*
* This routine reserves resources on the RBC block I/O device.
*
* RETURNS: ERROR to indicate the command is unsupported or anything wrong
*
* ERRNO: N/A
*/

STATUS usbTgtRbcPersistentReserveOut
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun,
    UINT8 **         ppData,        /* location of reserve data on device */
    UINT32 *         pSize          /* size of reserve data */
    )
    {
    if (pUsbTgtMscLun == NULL)
        {
        USBTGT_MSC_ERR("usbTgtRbcPersistentReserveOut - pUsbTgtMscLun is NULL\n",
            1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    USBTGT_MSC_WARN("usbTgtRbcPersistentReserveOut - not supported\n",
        1, 2, 3, 4, 5, 6);

    *ppData = NULL;
    *pSize = 0;

    /*
     * This routine is optional and is not implemented. If this routine is
     * called, return a status of ERROR, a sense key of ILLEGAL REQUEST (05h),
     * and an ASC/ASCQ of INVALID COMMAND OPERATION CODE (20h).
     */

    usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                           SCSI_SENSE_ILLEGAL_REQUEST,
                           SCSI_ADSENSE_ILLEGAL_COMMAND,
                           0x0);

    return ERROR;
    }

/*******************************************************************************
*
* usbTgtRbcRelease - release a resource on the RBC device
*
* This routine releases a resource on the RBC block I/O device.
*
* RETURNS: ERROR to indicate the command is unsupported or anything wrong
*
* ERRNO: N/A
*/

STATUS usbTgtRbcRelease
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    )
    {
    if (pUsbTgtMscLun == NULL)
        {
        USBTGT_MSC_ERR("usbTgtRbcRelease - pUsbTgtMscLun is NULL\n",
                       1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    USBTGT_MSC_WARN("usbTgtRbcRelease - not supported\n",
                    1, 2, 3, 4, 5, 6);

    /*
     * This routine is optional and is not implemented. If this routine is
     * called, return a status of ERROR, a sense key of ILLEGAL REQUEST (05h),
     * and an ASC/ASCQ of INVALID COMMAND OPERATION CODE (20h).
     */

    usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                           SCSI_SENSE_ILLEGAL_REQUEST,
                           SCSI_ADSENSE_ILLEGAL_COMMAND,
                           0x0);

    return ERROR;
    }

/*******************************************************************************
*
* usbTgtRbcRequestSense - request sense data from the RBC device
*
* This routine requests sense data from the RBC block I/O device.
*
* RETURNS: OK or ERROR
*
* ERRNO: N/A
*/

STATUS usbTgtRbcRequestSense
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun,
    UINT8 **         ppData,        /* location of sense data on device */
    UINT32 *         pSize          /* size of sense data */
    )
    {
    UINT8            allocLgth;
    pUSB_TGT_MSC_DEV pUsbTgtMscDev;

    if (pUsbTgtMscLun == NULL)
        {
        USBTGT_MSC_ERR("usbTgtRbcRequestSense - pUsbTgtMscLun is NULL\n",
            1, 2, 3, 4, 5, 6);
        return ERROR;
        }
    pUsbTgtMscDev = pUsbTgtMscLun->pUsbTgtMscDev;

    allocLgth = pUsbTgtMscDev->cmd[4];    /* allocation length */

    *ppData = (UINT8 *)pUsbTgtMscLun->pSenseData;
    *pSize  = sizeof(SENSE_DATA);

    if (allocLgth < *pSize)
        *pSize = allocLgth;

    return OK;
    }

/*******************************************************************************
*
* usbTgtRbcReserve - reserve a resource on the RBC device
*
* This routine reserves a resource on the RBC block I/O device.
*
* RETURNS: ERROR to indicate the command is unsupported or anything wrong
*
* ERRNO: N/A
*/

STATUS usbTgtRbcReserve
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    )
    {
    if (pUsbTgtMscLun == NULL)
        {
        USBTGT_MSC_ERR("usbTgtRbcReserve - pUsbTgtMscLun is NULL\n",
                       1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    USBTGT_MSC_WARN("usbTgtRbcRequestSense - not supported\n",
        1, 2, 3, 4, 5, 6);

    /*
     * set sense key to ILLEGAL REQUEST 0x5 and
     * set additional sense code to INVALID COMMAND OPERATION CODE 0x20
     */

    usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                           SCSI_SENSE_ILLEGAL_REQUEST,
                           SCSI_ADSENSE_ILLEGAL_COMMAND,
                           0);
    return ERROR;
    }

/*******************************************************************************
*
* usbTgtRbcCacheSync - synchronize the cache of the RBC device
*
* This routine synchronizes the cache of the RBC block I/O device.
*
* RETURNS: OK or ERROR
*
* ERRNO: N/A
*/

STATUS usbTgtRbcCacheSync
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    )
    {
    if (pUsbTgtMscLun == NULL)
        {
        USBTGT_MSC_ERR("usbTgtRbcCacheSync - pUsbTgtMscLun is NULL\n",
                      1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    usbTgtMscSenseDataForm(pUsbTgtMscLun->pSenseData,
                           SCSI_SENSE_NO_SENSE,
                           SCSI_ADSENSE_NO_SENSE,
                           0);
    return OK;
    }

/*******************************************************************************
*
* usbTgtRbcVendorSpecificDataInit - initialize vendor specific data
*
* This routine initializes vendor specific data.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbTgtRbcVendorSpecificDataInit
    (
    UINT8 * vendorSpecificData
    )
    {
    if (vendorSpecificData == NULL)
        {
        USBTGT_MSC_ERR("usbTgtRbcVendorSpecificDataInit - vendorSpecificData is NULL\n",
            1, 2, 3, 4, 5, 6);
        return;
        }

    memset(vendorSpecificData, 0, VENDOR_SPECIFIC_23_LENGTH);
    vendorSpecificData[3] = 0x08;
    vendorSpecificData[4] = 0x02;
    vendorSpecificData[5] = 0x54;
    vendorSpecificData[6] = 0x29;
    vendorSpecificData[7] = 0x7f;
    vendorSpecificData[10] = 0x02;

    return;
    }

/*******************************************************************************
*
* usbTgtRbcVendorSpecific - vendor specific call
*
* This routine is a vendor specific call.
*
* RETURNS: OK, or ERROR is something error
*
* ERRNO: N/A
*/

STATUS usbTgtRbcVendorSpecific
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun,
    UINT8 **         ppData,        /* location of sense data on device */
    UINT32 *         pSize          /* size of sense data */
    )
    {
    UINT8 * pBuf;
    pUSB_TGT_MSC_DEV pUsbTgtMscDev;

    if (pUsbTgtMscLun == NULL)
        {
        USBTGT_MSC_ERR("usbTgtRbcVendorSpecific - pUsbTgtMscLun is NULL\n",
                       1, 2, 3, 4, 5, 6);
        return ERROR;
        }
    pUsbTgtMscDev = pUsbTgtMscLun->pUsbTgtMscDev;

    pBuf = usbTgtMscEmptyBufGet(pUsbTgtMscDev);

    *ppData = pBuf;
    usbTgtRbcVendorSpecificDataInit(pBuf);
    *pSize = VENDOR_SPECIFIC_23_LENGTH;

    return OK;
    }

/*******************************************************************************
*
* usbTgtMscSenseDataInit - initialize the sense data
*
* This routine initializes the sense data.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbTgtMscSenseDataInit
    (
    SENSE_DATA*  pSenseData
    )
    {
    if (pSenseData == NULL)
        {
        USBTGT_MSC_ERR("usbTgtRbcVendorSpecific - pSenseData is NULL\n",
                       1, 2, 3, 4, 5, 6);
        return;
        }

    memset(pSenseData, 0, sizeof(SENSE_DATA));
    pSenseData->responseCode = 0x70;
    pSenseData->additionalSenseLgth = 0xA; /* additionalSenseLgth always 0xA */
    }

/*******************************************************************************
*
* usbTgtMscSenseDataForm - form sense data
*
* This routine forms sense data.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbTgtMscSenseDataForm
    (
    SENSE_DATA*  pSenseData,
    UINT8        senseKey,      /* the SCSI sense key */
    UINT8        asc,           /* additional sense code */
    UINT8        ascq           /* additional sense code qualifier */
    )
    {
    if (pSenseData == NULL)
        {
        USBTGT_MSC_ERR("usbTgtMscSenseDataForm - pSenseData is NULL\n",
                       1, 2, 3, 4, 5, 6);
        return;
        }
    pSenseData->senseKey = senseKey;
    pSenseData->asc = asc;
    pSenseData->ascq = ascq;
    return;
    }

/*******************************************************************************
*
* usbTgtMscCapacityDataForm - form capacity data
*
* This routine forms capacity data.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbTgtMscCapacityDataForm
    (
    CAPACITY_DATA*  pCapacityData,
    unsigned        blockSize,
    sector_t        nBlocks
    )
    {
    if (pCapacityData == NULL)
        {
        USBTGT_MSC_ERR("usbTgtMscCapacityDataForm - pCapacityData is NULL\n",
            1, 2, 3, 4, 5, 6);
        return;
        }
    pCapacityData->lba_3 = (UINT8) (((nBlocks - 1) & 0xff000000) >> 24);
    pCapacityData->lba_2 = (UINT8) (((nBlocks - 1) & 0x00ff0000) >> 16);
    pCapacityData->lba_1 = (UINT8) (((nBlocks - 1) & 0x0000ff00) >> 8);
    pCapacityData->lba_0 = (UINT8) (((nBlocks - 1) & 0x000000ff));

    pCapacityData->blockLen_3 = (UINT8) ((blockSize & 0xff000000) >> 24);
    pCapacityData->blockLen_2 = (UINT8) ((blockSize & 0x00ff0000) >> 16);
    pCapacityData->blockLen_1 = (UINT8) ((blockSize & 0x0000ff00) >> 8);
    pCapacityData->blockLen_0 = (UINT8) ((blockSize & 0x000000ff));

    return;
    }

/*******************************************************************************
*
* usbTgtMscVpdSupportedPageInit - initialize vpd supported page
*
* This routine initializes vpd supported page.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbTgtMscVpdSupportedPageInit
    (
    VPD_SUPPORTED_PAGE * pVpdSupportedPage
    )
    {
    if (pVpdSupportedPage == NULL)
        {
        USBTGT_MSC_ERR("usbTgtMscVpdSupportedPageInit - pVpdSupportedPage is NULL\n",
            1, 2, 3, 4, 5, 6);
        return;
        }

    memset(pVpdSupportedPage, 0, sizeof(VPD_SUPPORTED_PAGE));
    pVpdSupportedPage->devQualType = ((PERIPHERAL_DEVICE_QUAL & 0x7) << 5) |
                                      PERIPHERAL_DEVICE_TYPE;
    pVpdSupportedPage->pageLgth = 0x3;
    pVpdSupportedPage->page_0 = VPD_SUPPORTED_PAGE_CODE;
    pVpdSupportedPage->page_80 = VPD_UNIT_SERIAL_NUM_PAGE_CODE;
    pVpdSupportedPage->page_83 = VPD_DEVICE_ID_PAGE_CODE;

    return;
    }

/*******************************************************************************
*
* usbTgtMscVpdDevIdPageInit - initialize vpd device ID page
*
* This routine initializes vpd device ID page.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbTgtMscVpdDevIdPageInit
    (
    VPD_DEVICE_ID_PAGE * pVpdDevIdPage
    )
    {
    char c[] = {DEVICE_ID};

    if (pVpdDevIdPage == NULL)
        {
        USBTGT_MSC_ERR("usbTgtMscVpdSupportedPageInit - pVpdDevIdPage is NULL\n",
            1, 2, 3, 4, 5, 6);
        return;
        }

    memset(pVpdDevIdPage, 0, sizeof(VPD_DEVICE_ID_PAGE));
    pVpdDevIdPage->devQualType = ((PERIPHERAL_DEVICE_QUAL & 0x7) << 5) |
                                  PERIPHERAL_DEVICE_TYPE;
    pVpdDevIdPage->pageCode = VPD_DEVICE_ID_PAGE_CODE;
    pVpdDevIdPage->pageLgth = 0x1;

    pVpdDevIdPage->devIdDescr.codeSet = 0x2;
    pVpdDevIdPage->devIdDescr.assocIdType = 0x1;
    pVpdDevIdPage->devIdDescr.idLgth = DEVICE_ID_LGTH;
    memcpy(&pVpdDevIdPage->devIdDescr.devId, c, DEVICE_ID_LGTH);

    return;
    }

/*******************************************************************************
*
* usbTgtMscVpdSerialNumPageInit - initialize vpd serial num page
*
* This routine initializes vpd serial num page.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbTgtMscVpdSerialNumPageInit
    (
    VPD_UNIT_SERIAL_NUM_PAGE * pVpdSerialNumPage,
    UINT8                      index
    )
    {
    int i;

    if (pVpdSerialNumPage == NULL)
        {
        USBTGT_MSC_ERR("usbTgtMscVpdSerialNumPageInit - pVpdSerialNumPage is NULL\n",
            1, 2, 3, 4, 5, 6);
        return;
        }

    memset(pVpdSerialNumPage, 0, sizeof(VPD_UNIT_SERIAL_NUM_PAGE));
    pVpdSerialNumPage->devQualType = ((PERIPHERAL_DEVICE_QUAL & 0x7) << 5) |
                                  PERIPHERAL_DEVICE_TYPE;
    pVpdSerialNumPage->pageCode = VPD_UNIT_SERIAL_NUM_PAGE_CODE;
    pVpdSerialNumPage->pageLgth = 24;

    for (i = 0; i < 24; i += 2)
        {
        pVpdSerialNumPage->serialNum[i] = (UINT8)( '0' + index);
        pVpdSerialNumPage->serialNum[i + 1] = index;
        }

    return;
    }

/*******************************************************************************
*
* usbTgtMscVpdSerialNumPageInit - initialize command data
*
* This routine initializes command data.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbTgtMscCmdDataInit
    (
    COMMAND_DATA * pCmdData
    )
    {
    if (pCmdData == NULL)
        {
        USBTGT_MSC_ERR("usbTgtMscCmdDataInit - pCmdData is NULL\n",
            1, 2, 3, 4, 5, 6);
        return;
        }

    memset(pCmdData, 0, sizeof(COMMAND_DATA));
    pCmdData->devQualType = ((PERIPHERAL_DEVICE_QUAL & 0x7) << 5) |
                             PERIPHERAL_DEVICE_TYPE;
    pCmdData->support = 0x1;
    pCmdData->cdbSize = 0x2;

    return;
    }

/*******************************************************************************
*
* usbTgtMscDeviceParamListInit - initialize device parameter list for LUN
*
* This routine initializes device parameter list for LUN.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbTgtMscDeviceParamListInit
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    )
    {
    if (pUsbTgtMscLun == NULL)
        {
        USBTGT_MSC_ERR("usbTgtMscDeviceParamListInit - pUsbTgtMscLun is NULL\n",
                       1, 2, 3, 4, 5, 6);
        return;
        }

    memset(&pUsbTgtMscLun->deviceParamList, 0, sizeof(MODE_PARAMETER_LIST));
    pUsbTgtMscLun->deviceParamList.header.dataLen = sizeof(MODE_PARAMETER_LIST) - 1;
    pUsbTgtMscLun->deviceParamList.header.deviceParameter =
        (UINT8) ((pUsbTgtMscLun->bReadOnly == TRUE) ? 0x80: 0x00);
    pUsbTgtMscLun->deviceParamList.params.pageCode = 0x80 | RBC_MODE_PARAMETER_PAGE;
    pUsbTgtMscLun->deviceParamList.params.pageLth = 0x08;
    pUsbTgtMscLun->deviceParamList.params.writeCacheDisable = 0x1;

    pUsbTgtMscLun->deviceParamList.params.logicalBlockSize[0] = (UINT8) ((pUsbTgtMscLun->perBlockSize & 0xff00) >> 8);
    pUsbTgtMscLun->deviceParamList.params.logicalBlockSize[1] = (UINT8) (pUsbTgtMscLun->perBlockSize & 0xff);

    pUsbTgtMscLun->deviceParamList.params.numberOfLogicalBlocks[1] = (UINT8) ((pUsbTgtMscLun->nBlocks & 0xff000000) >> 24);
    pUsbTgtMscLun->deviceParamList.params.numberOfLogicalBlocks[2] = (UINT8) ((pUsbTgtMscLun->nBlocks & 0x00ff0000) >> 16);
    pUsbTgtMscLun->deviceParamList.params.numberOfLogicalBlocks[3] = (UINT8) ((pUsbTgtMscLun->nBlocks & 0x0000ff00) >> 8);
    pUsbTgtMscLun->deviceParamList.params.numberOfLogicalBlocks[4] = (UINT8) ((pUsbTgtMscLun->nBlocks & 0x000000ff));

    return;
    }

/*******************************************************************************
*
* usbTgtMscDeviceParamListMaskInit - initialize device parameter list mask for LUN
*
* This routine initializes device parameter list mask for LUN.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbTgtMscDeviceParamListMaskInit
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    )
    {
    if (pUsbTgtMscLun == NULL)
        {
        USBTGT_MSC_ERR("usbTgtMscDeviceParamListMaskInit - pUsbTgtMscLun is NULL\n",
                       1, 2, 3, 4, 5, 6);
        return;
        }

    memset(&pUsbTgtMscLun->deviceParamListMask, 0, sizeof(MODE_PARAMETER_LIST));
    pUsbTgtMscLun->deviceParamList.header.deviceParameter =
        (UINT8)((pUsbTgtMscLun->bReadOnly == TRUE) ? 0x80: 0x00);
    pUsbTgtMscLun->deviceParamList.header.dataLen = sizeof(MODE_PARAMETER_LIST) - 1;
    pUsbTgtMscLun->deviceParamList.params.pageCode = 0x80 | RBC_MODE_PARAMETER_PAGE;

    return;
    }

/*******************************************************************************
*
* usbTgtMscDeviceParamListMaskInit - initialize device parameter list10 for LUN
*
* This routine initializes device parameter list10 for LUN.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbTgtMscDeviceParamList10Init
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    )
    {
    if (pUsbTgtMscLun == NULL)
        {
        USBTGT_MSC_ERR("usbTgtMscDeviceParamList10Init - pUsbTgtMscLun is NULL\n",
                       1, 2, 3, 4, 5, 6);
        return;
        }

    memset(&pUsbTgtMscLun->deviceParamList10, 0, sizeof(MODE_PARAMETER_LIST10));
    pUsbTgtMscLun->deviceParamList10.header.dataLenMSB = (((sizeof(MODE_PARAMETER_LIST10) - 2) & 0xFF00) >> 8);
    pUsbTgtMscLun->deviceParamList10.header.dataLenLSB = ((sizeof(MODE_PARAMETER_LIST10) - 2) & 0x00FF);
    pUsbTgtMscLun->deviceParamList10.header.deviceParameter =
        (UINT8) ((pUsbTgtMscLun->bReadOnly == TRUE) ? 0x80: 0x00);
    pUsbTgtMscLun->deviceParamList10.params.pageCode = 0x80 | RBC_MODE_PARAMETER_PAGE;
    pUsbTgtMscLun->deviceParamList10.params.pageLth = 0x08;
    pUsbTgtMscLun->deviceParamList10.params.writeCacheDisable = 0x1;

    pUsbTgtMscLun->deviceParamList10.params.logicalBlockSize[0] = (UINT8) ((pUsbTgtMscLun->perBlockSize & 0xff00) >> 8);
    pUsbTgtMscLun->deviceParamList10.params.logicalBlockSize[1] = (UINT8) (pUsbTgtMscLun->perBlockSize & 0xff);

    pUsbTgtMscLun->deviceParamList10.params.numberOfLogicalBlocks[1] = (UINT8) ((pUsbTgtMscLun->nBlocks & 0xff000000) >> 24);
    pUsbTgtMscLun->deviceParamList10.params.numberOfLogicalBlocks[2] = (UINT8) ((pUsbTgtMscLun->nBlocks & 0x00ff0000) >> 16);
    pUsbTgtMscLun->deviceParamList10.params.numberOfLogicalBlocks[3] = (UINT8) ((pUsbTgtMscLun->nBlocks & 0x0000ff00) >> 8);
    pUsbTgtMscLun->deviceParamList10.params.numberOfLogicalBlocks[4] = (UINT8) ((pUsbTgtMscLun->nBlocks & 0x000000ff));

    return;
    }

/*******************************************************************************
*
* usbTgtMscDeviceParamListMaskInit - initialize device parameter list10 mask for LUN
*
* This routine initializes device parameter list10 mask for LUN.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbTgtMscDeviceParamListMask10Init
    (
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    )
    {
    if (pUsbTgtMscLun == NULL)
        {
        USBTGT_MSC_ERR("usbTgtMscDeviceParamListMask10Init - pUsbTgtMscLun is NULL\n",
                       1, 2, 3, 4, 5, 6);
        return;
        }

    memset(&pUsbTgtMscLun->deviceParamListMask10, 0, sizeof(MODE_PARAMETER_LIST10));
    pUsbTgtMscLun->deviceParamList10.header.dataLenMSB = (((sizeof(MODE_PARAMETER_LIST10) - 2) & 0xFF00) >> 8);
    pUsbTgtMscLun->deviceParamList10.header.dataLenLSB = ((sizeof(MODE_PARAMETER_LIST10) - 2) & 0x00FF);
    pUsbTgtMscLun->deviceParamList10.header.deviceParameter =
        (UINT8)((pUsbTgtMscLun->bReadOnly == TRUE) ? 0x80: 0x00);

    pUsbTgtMscLun->deviceParamList10.params.pageCode = 0x80 | RBC_MODE_PARAMETER_PAGE;

    return;
    }

/*******************************************************************************
*
* usbTgtMscStdInquiryDataInit - initialize standard inquiry data for LUN
*
* This routine initializes standard inquiry data for LUN.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbTgtMscStdInquiryDataInit
    (
    STD_INQUIRY_DATA * pStdInquiryData,
    pUSB_TGT_MSC_LUN   pUsbTgtMscLun
    )
    {
    if ((pStdInquiryData == NULL) || (pUsbTgtMscLun == NULL))
        {
        USBTGT_MSC_ERR("usbTgtMscStdInquiryDataInit - Invalid parameter\n",
                       1, 2, 3, 4, 5, 6);
        return;
        }

    memset(pStdInquiryData, 0, sizeof(STD_INQUIRY_DATA));

    /* Set media type */

    pStdInquiryData->devQualType = ((PERIPHERAL_DEVICE_QUAL & 0x7) << 5) |
                                  PERIPHERAL_DEVICE_TYPE;

    /* Set media removable attribute */

    if (pUsbTgtMscLun->bRemovable == TRUE)
        pStdInquiryData->RemovableMedia = 0x80;
    else
        pStdInquiryData->RemovableMedia = 0x00;

    pStdInquiryData->byte3 = 0x2;

    /* This field shall specify the length in bytes of the parameters */

    pStdInquiryData->additionalLgth = sizeof(STD_INQUIRY_DATA)- 4;

    memcpy(pStdInquiryData->vendorId, USB_TGT_MSC_VENDOR_NAME, sizeof(pStdInquiryData->vendorId));
    memcpy(pStdInquiryData->productId, USB_TGT_MSC_PRODUCT_NAME, sizeof(pStdInquiryData->productId));
    memcpy(pStdInquiryData->productRevLevel, USB_TGT_MSC_PRODUCT_REV, sizeof(pStdInquiryData->productRevLevel));

    pStdInquiryData->versionDescriptor[0] = 0x023C;
    pStdInquiryData->versionDescriptor[1] = 0x0260;

    return;
    }

