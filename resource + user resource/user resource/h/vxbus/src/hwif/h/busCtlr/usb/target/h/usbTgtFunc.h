/* usbTgtFunc.h - Definitions for USB Target Function Driver Common Module */

/*
 * Copyright (c) 2011, 2012 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
Modification history
--------------------
01b,04Sep12,s_z  clean up 
01a,17oct11,s_z  created
*/

/*
DESCRIPTION

This file includes the defination and routines of the USB GEN2 target stack
management modules.


*/

#ifndef __INCusbTgtFunch
#define __INCusbTgtFunch

#ifdef  __cplusplus
extern "C" {
#endif

/* includes */

#include <vxWorks.h>
#include <semLib.h>
#include <lstLib.h>
#include <vwModNum.h>
#include <hwif/vxbus/vxBus.h>
#include <usb/usb.h>
#include <usb/usbTgt.h>
#include <usb/usbOtg.h>
#include <vsbConfig.h>


/* defines */

/* typedefs */

typedef enum usbtgt_func_info_mem_type
    {
    USBTGT_FUNC_INFO_MEM_TYPE_uIfNumMin  = (0),
    USBTGT_FUNC_INFO_MEM_TYPE_uVendorID  = (1),
    USBTGT_FUNC_INFO_MEM_TYPE_pMfgString = (2)
    }USBTGT_FUNC_INFO_MEM_TYPE;

/* Early declaration */

IMPORT pUSB_INTERFACE_DESCR usbTgtFuncInterfaceDescFind
    (    
    USB_TARG_CHANNEL     targChannel,    
    UINT8                uInterfaceNum, 
    UINT8                uAltSetting    
    );
IMPORT pUSB_ENDPOINT_DESCR usbTgtFuncEndpointDescFind
    (    
    USB_TARG_CHANNEL targChannel,    
    UINT8            uInterfaceNum,  
    UINT8            uAltSetting,    
    UINT8            uEpType,        
    UINT8            uEpDir,         
    UINT8            uSyncType,      
    UINT8            uUsageType,     
    UINT8            uNth            
    );
IMPORT STATUS usbTgtFuncAltsetPipesDelete
    (
    USB_TARG_CHANNEL    targChannel,
    UINT8               uInterface,
    UINT8               uAltSetting
    );
IMPORT STATUS usbTgtFuncAltsetPipesCreate
    (
    USB_TARG_CHANNEL    targChannel,
    UINT8               uInterface,
    UINT8               uAltSetting
    );

IMPORT STATUS usbTgtFuncAllPipesCreate
    (
    USB_TARG_CHANNEL    targChannel
    );
IMPORT STATUS usbTgtFuncAllPipesDelete
    (
    USB_TARG_CHANNEL    targChannel
    );
IMPORT STATUS usbTgtFuncInfoMemberGet
    (
    USB_TARG_CHANNEL    targChannel,
    UINT16              uMemberType,
    void *              pContext
    );
IMPORT STATUS usbTgtFuncInfoMemberSet
    (
    USB_TARG_CHANNEL    targChannel,
    UINT16              uMemberType,
    void *              pContext
    );
IMPORT USB_TARG_PIPE usbTgtFuncFindPipeHandleByEpAttr
    (
    USB_TARG_CHANNEL    targChannel,
    UINT8               uInterface,
    UINT8               uAltSetting,
    UINT8               uEpType,      
    UINT8               uEpDir,       
    UINT8               uNth
    );
IMPORT STATUS usbTgtFuncDefaultPipeStall
    (
    USB_TARG_CHANNEL    targChannel
    );
IMPORT USB_TARG_PIPE usbTgtFuncFindPipeHandleByEpIndex
    (
    USB_TARG_CHANNEL    targChannel,
    UINT8               uEpIndex
    );
IMPORT STATUS usbTgtErpBufListDelete
    (
    LIST * pList
    );
IMPORT STATUS usbTgtErpBufListCreate
    (
    LIST *     pList,        /* List pointer */
    UINT16     uBfrCount,    /* Count of the Erp buffer to be created */
    UINT32     uHeaderLen,   /* Indicates the header length of the buffer */
    UINT32     uPayloadLen,  /* Indicates the payload length of buffer */
    UINT32     uTailLen      /* Indicates the tail length of the buffer */
    );
IMPORT STATUS usbTgtErpListDelete
    (
    LIST * pList
    );
IMPORT STATUS usbTgtErpListCreate
    (
    LIST *     pList,        /* List head */
    UINT16     uErpCount,    /* Count of the Erp List */
    UINT16     uBfrCount,    /* Count of the Erp buffer to be created */
    UINT32     uHeaderLen,   /* Indicates the header length of the buffer */
    UINT32     uPayloadLen,  /* Indicates the payload length of buffer */
    UINT32     uTailLen      /* Indicates the tail length of the buffer */
    );
#ifdef  __cplusplus
}
#endif

#endif  /* __INCusbTgtFunch */



