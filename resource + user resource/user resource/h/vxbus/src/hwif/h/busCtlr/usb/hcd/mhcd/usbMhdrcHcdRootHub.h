/* usbMhdrcHcdRootHub.h - root hub emulation entries for MHCD */

/*
 * Copyright (c) 2009 - 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */


/*
modification history
--------------------
01e,09mar11,w_x  Code clean up for make man
01d,16aug10,w_x  VxWorks 64 bit audit and warning removal
01c,03jun10,s_z  Coding convention
01b,07nov09,s_z  Rewrite according to the structure redesign
01a,05nov09,s_z  write
*/

/*
DESCRIPTION

This file contains the function declarations for root hub emulation.
*/

#ifndef __INCusbMhdrcHcdRootHubh
#define __INCusbMhdrcHcdRootHubh

#ifdef    __cplusplus
extern "C" {
#endif

/* includes*/

#define USB_MHCD_RH_REQUEST_TYPE            (0x60) /* Request type mask */
#define USB_MHCD_RH_STANDARD_REQUEST        (0x0)  /* Standard request identifier */
#define USB_MHCD_RH_CLASS_SPECIFIC_REQUEST  (0x20) /* Class specific request */
#define USB_MHCD_RH_RECIPIENT_MASK          (0x1F) /*
                                                    * Mask value for indicating
                                                    * the recipient
                                                    */

#define USB_MHCD_RH_GET_CONFIG_RETURN_SIZE  (0x1)
#define USB_MHCD_RH_DEVICE_DESC_SIZE        (0x12) /* Size of device descriptor */
#define USB_MHCD_RH_CONFIG_DESC_SIZE        (0x19) /* Size of config descriptor */
#define USB_MHCD_RH_GET_STATUS_RETURN_SIZE  (0x02) /*Size of GetStatus request data */
#define USB_MHCD_RH_DESCRIPTOR_BITPOSITION  (0x08) /*
                                                    * Descriptor type bit
                                                    * position in setup packet
                                                    * for GetDesc request
                                                    */

#define USB_MHCI_RH_PORT_FEATURE_CONNECTION      (0x00)
#define USB_MHCI_RH_PORT_FEATURE_ENABLE          (0x01)
#define USB_MHCI_RH_PORT_FEATURE_SUSPEND         (0x02)
#define USB_MHCI_RH_PORT_FEATURE_OVER_CURRENT    (0x03)
#define USB_MHCI_RH_PORT_FEATURE_RESET           (0x04)
#define USB_MHCI_RH_PORT_FEATURE_POWER           (0x08)
#define USB_MHCI_RH_PORT_FEATURE_LOWSPEED        (0x09)
#define USB_MHCI_RH_PORT_FEATURE_HIGHSPEED       (0x0A) /* 10 */
#define USB_MHCI_RH_PORT_FEATURE_C_CONNECTION    (0x10) /* 16 */
#define USB_MHCI_RH_PORT_FEATURE_C_ENABLE        (0x11) /* 17 */
#define USB_MHCI_RH_PORT_FEATURE_C_SUSPEND       (0x12) /* 18 */
#define USB_MHCI_RH_PORT_FEATURE_C_OVER_CURRENT  (0x13) /* 19 */
#define USB_MHCI_RH_PORT_FEATURE_C_RESET         (0x14) /* 20 */
#define USB_MHCI_RH_PORT_FEATURE_TEST            (0x15) /* 21 */
#define USB_MHCI_RH_PORT_FEATURE_INDICATOR       (0x16) /* 22 */

#ifndef USB_HUB_DESCRIPTOR
#define USB_HUB_DESCRIPTOR                 ((UINT8)0x29)
#endif


/* imports */

IMPORT USBHST_STATUS usbMhdrcHcdRootHubCreatePipe
    (
    pUSB_MHCD_DATA pHCDData,
    UINT8          uDeviceAddress,
    UINT8          uDeviceSpeed,
    UCHAR *        pEndpointDescriptor,
    ULONG *        puPipeHandle
    );

IMPORT USBHST_STATUS usbMhdrcHcdRootHubDeletePipe
    (
    pUSB_MHCD_DATA pHCDData,
    ULONG          uPipeHandle
    );

IMPORT USBHST_STATUS usbMhdrcHcdRootHubSubmitURB
    (
    pUSB_MHCD_DATA pHCDData,
    ULONG          uPipeHandle,
    pUSBHST_URB    pURB
    );

IMPORT USBHST_STATUS usbMhdrcHcdRootHubCancelURB
    (
    pUSB_MHCD_DATA pHCDData,
    ULONG          uPipeHandle,
    pUSBHST_URB    pURB
    );

IMPORT BOOLEAN usbMhdrcHcdCopyRootHubInterruptData
    (
    pUSB_MHCD_DATA pHCDData,
    UINT32         uStatusChange
    );

IMPORT USBHST_STATUS usbMhdrcHcdRootHubSetPortFeature
    (
    pUSB_MHCD_DATA pHCDData,           /* Pointer to HCD block           */
    pUSBHST_URB    pURB                /* Pointer to User Request Block  */
    );

IMPORT USBHST_STATUS usbMhdrcHcdRootHubClearPortFeature
    (
    pUSB_MHCD_DATA pHCDData,           /* Pointer to HCD block           */
    pUSBHST_URB    pURB                /* Pointer to User Request Block  */
    );

IMPORT USBHST_STATUS usbMhdrcHcdRootHubGetHubDescriptor
    (
    pUSB_MHCD_DATA pHCDData,           /* Pointer to HCD block           */
    pUSBHST_URB    pURB                /* Pointer to User Request Block  */
    );

IMPORT USBHST_STATUS usbMhdrcHcdRootHubGetPortStatus
    (
    pUSB_MHCD_DATA pHCDData,           /* Pointer to HCD block           */
    pUSBHST_URB    pURB                /* Pointer to User Request Block  */
    );

IMPORT USBHST_STATUS usbMhdrcHcdRootHubProcessInterruptRequest
    (
    pUSB_MHCD_DATA pHCDData,    /* Pointer to HCD block           */
    pUSBHST_URB    pURB         /* Pointer to User Request Block  */
    );

IMPORT USBHST_STATUS usbMhdrcHcdRootHubProcessStandardRequest
    (
    pUSB_MHCD_DATA pHCDData,    /* Pointer to HCD block           */
    pUSBHST_URB    pURB         /* Pointer to User Request Block  */
    );

IMPORT USBHST_STATUS usbMhdrcHcdRootHubProcessClassSpecificRequest
    (
    pUSB_MHCD_DATA pHCDData,    /* Pointer to HCD block           */
    pUSBHST_URB    pURB         /* Pointer to User Request Block  */
    );

IMPORT USBHST_STATUS usbMhdrcHcdRootHubProcessControlRequest
    (
    pUSB_MHCD_DATA pHCDData,           /* Pointer to HCD block           */
    pUSBHST_URB    pURB                /* Pointer to User Request Block  */
    );


#ifdef __cplusplus
}
#endif

#endif /* __INCusbMhdrcHcdRootHubh */

