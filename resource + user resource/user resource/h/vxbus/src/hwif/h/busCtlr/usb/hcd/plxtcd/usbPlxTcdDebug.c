/*  usbPlxTcdDebug.c  - USB PLX TCD Debug routines */

/*
 * Copyright (c) 2012 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,23nov12,s_z  Remove un-used information (WIND00390391)
01a,18may12,s_z  Writen
*/

/*
DESCRIPTION

This file provides the debug routines for USB PLX Target Controller 
Driver.

INCLUDE FILES: usbPlxTcd.h
*/

#include <usbPlxTcd.h>

/* etxern */

extern LIST  gUsbPlxTcdList;

/*******************************************************************************
*
* usbPlxTcdRegShow - show the registers value of PLX TCD 
*
* This routine shows the registers value of PLX TCD.
* <uIndex>  is based from 0.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbPlxTcdRegShow
    (
    UINT8 uIndex
    )
    {
    pUSB_PLX_TCD_DATA pTCDData = NULL;
    pUSB_PLX_CTRL     pPlxCtrl = NULL;
    VXB_DEVICE_ID     pDev = NULL;
    NODE *            pNode = NULL;
    UINT32            uReg32;
    int               i;
    
    pNode = lstNth(&gUsbPlxTcdList, uIndex + 1);

    if (NULL == pNode) 
        {
        printf("No find the PLX TCD instance by index %d\n", uIndex);
        return ERROR;
        }

    pTCDData = USB_PLX_TCD_DATA_CONV_FROM_NODE(pNode);

    if ((NULL == pTCDData) ||
        (NULL == pTCDData->pPlxCtrl) ||
        (NULL == pTCDData->pPlxCtrl->pDev))
        {
        printf("Invalid PLX TCD instance by index %d\n", uIndex);
        return ERROR;
        }    

    pPlxCtrl = pTCDData->pPlxCtrl;
    pDev = pTCDData->pPlxCtrl->pDev;

    /* PCI Configurure Registers */

    printf("\nPCI Configurure Registers:\n\n");

    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_DEVINIT_REG);

    for (i = 0; i <= 0x44; i = i + 4)
        {
        VXB_PCI_BUS_CFG_READ(pDev, i, 4, uReg32);
        printf("PCI Reg %02X    = 0x%08X\n", i, uReg32);
        }

    /* Main Control Registers */

    printf("\nMain Control Registers:\n\n");

    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_DEVINIT_REG);
    printf("DEVINIT    = 0x%08X\n", uReg32);
   
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_EECTL_REG);
    printf("EECTL      = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_EECLKFREQ_REG);
    printf("EECLKFREQ  = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_PCICTL_REG);
    printf("PCICTL     = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_PCIIRQENB0_REG);
    printf("PCIIRQENB0 = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_PCIIRQENB1_REG);
    printf("PCIIRQENB1 = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_CPUIRQENB0_REG);
    printf("CPUIRQENB0 = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_CPUIRQENB1_REG);
    printf("CPUIRQENB1 = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_USBIRQENB1_REG);
    printf("USBIRQENB1 = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_IRQSTAT0_REG);
    printf("IRQSTAT0   = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_IRQSTAT1_REG);
    printf("IRQSTAT1   = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_IDXADDR_REG);
    printf("IDXADDR    = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_IDXDATA_REG);
    printf("IDXDATA    = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_FIFOCTL_REG);
    printf("FIFOCTL    = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_MEMADDR_REG);
    printf("MEMADDR    = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_MEMDATA0_REG);
    printf("MEMDATA0   = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_MEMDATA1_REG);
    printf("MEMDATA1   = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_GPIOCTL_REG);
    printf("GPIOCTL    = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_GPIOSTAT_REG);
    printf("GPIOSTAT   = 0x%08X\n", uReg32);

    /* USB Control Registers */

    printf("\nUSB Control Registers:\n\n");
    
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_STDRSP_REG);
    printf("STDRSP     = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_PRODVENDID_REG);
    printf("PRODVENDID = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_RELNUM_REG);
    printf("RELNUM     = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_USBCTL_REG);
    printf("USBCTL     = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_USBSTAT_REG);
    printf("USBSTAT    = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_XCVRDIAG_REG);    
    printf("XCVRDIAG   = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_SETUP0123_REG);
    printf("SETUP0123  = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_SETUP4567_REG);
    printf("SETUP4567  = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_OURADDR_REG);
    printf("OURADDR    = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_OURCONFIG_REG);
    printf("OURCONFIG  = 0x%08X\n", uReg32);

    printf("\nPCI Control Registers:\n\n");
    
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_PCIMSTCTL_REG);
    printf("PCIMSTCTL  = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_PCIMSTADDR_REG);
    printf("PCIMSTADDR = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_PCIMSTDATA_REG);
    printf("PCIMSTDATA = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_PCIMSTSTAT_REG);
    printf("PCIMSTSTAT = 0x%08X\n", uReg32);


    /* DMA  Registers */

    printf("\nDMA Control Registers:\n\n");

    for (i = 0; i < 4; i++)
        {
        uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_DMACTL_OFFSET(i + 1));
        printf("DMACTL   [%c] = 0x%08X\n", 'A' + i, uReg32);
        uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_DMASTAT_OFFSET(i + 1));
        printf("DMASTAT  [%c] = 0x%08X\n", 'A' + i, uReg32);
        uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_DMACOUNT_OFFSET(i + 1));
        printf("DMACOUNT [%c] = 0x%08X\n", 'A' + i, uReg32);
        uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_DMAADDR_OFFSET(i + 1));
        printf("DMAADDR  [%c] = 0x%08X\n", 'A' + i, uReg32);
        uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_DMADESC_OFFSET(i + 1));
        printf("DMADESC  [%c] = 0x%08X\n\n", 'A' + i, uReg32);
        }

    /* Dedicated Endpoint Registers */

    printf("\nDedicated Endpoint Registers:\n\n");

    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, 
                                USB_PLX_DEP_CFGOUT_REG_BASE +
                                USB_PLX_DEP_CFG_OFFSET);
    printf("CFGOUT_DEP_CFG = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, 
                                USB_PLX_DEP_CFGOUT_REG_BASE +
                                USB_PLX_DEP_RSP_OFFSET);
    printf("CFGOUT_DEP_RSP = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, 
                                USB_PLX_DEP_CFGIN_REG_BASE +
                                USB_PLX_DEP_CFG_OFFSET);
    printf("CFGIN_DEP_CFG  = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, 
                                USB_PLX_DEP_CFGIN_REG_BASE +
                                USB_PLX_DEP_RSP_OFFSET);
    printf("CFGIN_DEP_RSP  = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, 
                                USB_PLX_DEP_PCIOUT_REG_BASE +
                                USB_PLX_DEP_CFG_OFFSET);
    printf("PCIOUT_DEP_CFG = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, 
                                USB_PLX_DEP_PCIOUT_REG_BASE +
                                USB_PLX_DEP_RSP_OFFSET);
    printf("PCIOUT_DEP_RSP = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, 
                                USB_PLX_DEP_PCIIN_REG_BASE +
                                USB_PLX_DEP_CFG_OFFSET);
    printf("PCIIN_DEP_CFG  = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, 
                                USB_PLX_DEP_PCIIN_REG_BASE +
                                USB_PLX_DEP_RSP_OFFSET);
    printf("PCIIN_DEP_RSP  = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, 
                                USB_PLX_DEP_STATIN_REG_BASE +
                                USB_PLX_DEP_CFG_OFFSET);
    printf("STATIN_DEP_CFG = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, 
                                USB_PLX_DEP_STATIN_REG_BASE +
                                USB_PLX_DEP_RSP_OFFSET);
    printf("STATIN_DEP_RSP = 0x%08X\n", uReg32);


    /* Configurable Endpoint / FIFO Registers */

    printf("\nConfigurable Endpoint / FIFO Registers:\n\n");

    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_EP_CFG_OFFSET(0));
    printf("EP_CFG    [0] = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_EP_RSP_OFFSET(0));
    printf("EP_RSP    [0] = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_EP_IRQENB_OFFSET(0));
    printf("EP_IRQENB [0] = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_EP_STAT_OFFSET(0));
    printf("EP_STAT   [0] = 0x%08X\n", uReg32);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_EP_AVAIL_OFFSET(0));
    printf("EP_AVAIL  [0] = 0x%08X\n", uReg32);

    for (i = 0; i < 6; i ++)
        {
        uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_EP_CFG_OFFSET(i + 1));
        printf("EP_CFG    [%c] = 0x%08X\n", 'A' + i, uReg32);
        uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_EP_RSP_OFFSET(i + 1));
        printf("EP_RSP    [%c] = 0x%08X\n", 'A' + i, uReg32);
        uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_EP_IRQENB_OFFSET(i + 1));
        printf("EP_IRQENB [%c] = 0x%08X\n", 'A' + i, uReg32);
        uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_EP_STAT_OFFSET(i + 1));
        printf("EP_STAT   [%c] = 0x%08X\n", 'A' + i, uReg32);
        uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_EP_AVAIL_OFFSET(i + 1));
        printf("EP_AVAIL  [%c] = 0x%08X\n", 'A' + i, uReg32);
        }

    /* Indexed Registers */

    printf("\nIndexed Registers:\n\n");

    USB_PLX_REG_WRITE32(pPlxCtrl, USB_PLX_IDXADDR_REG, 
                        USB_PLX_DIAG_IDX);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_IDXDATA_REG);
    printf("DIAG           = 0x%08X\n", uReg32);
    USB_PLX_REG_WRITE32(pPlxCtrl, USB_PLX_IDXADDR_REG, 
                        USB_PLX_PKTLEN_IDX);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_IDXDATA_REG);
    printf("PKTLEN         = 0x%08X\n", uReg32);
    USB_PLX_REG_WRITE32(pPlxCtrl, USB_PLX_IDXADDR_REG, 
                        USB_PLX_FRAME_IDX);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_IDXDATA_REG);
    printf("FRAME          = 0x%08X\n", uReg32);
    USB_PLX_REG_WRITE32(pPlxCtrl, USB_PLX_IDXADDR_REG, 
                        USB_PLX_CHIPREV_IDX);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_IDXDATA_REG);
    printf("CHIPREV        = 0x%08X\n", uReg32);   
    USB_PLX_REG_WRITE32(pPlxCtrl, USB_PLX_IDXADDR_REG, 
                        USB_PLX_HS_MAXPOWER_IDX);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_IDXDATA_REG);
    printf("HS_MAXPOWER    = 0x%08X\n", uReg32);
    USB_PLX_REG_WRITE32(pPlxCtrl, USB_PLX_IDXADDR_REG, 
                        USB_PLX_FS_MAXPOWER_IDX);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_IDXDATA_REG);
    printf("FS_MAXPOWER    = 0x%08X\n", uReg32);
    USB_PLX_REG_WRITE32(pPlxCtrl, USB_PLX_IDXADDR_REG, 
                        USB_PLX_HS_INTPOLL_RATE_IDX);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_IDXDATA_REG);
    printf("HS_INTPOLL     = 0x%08X\n", uReg32);
    USB_PLX_REG_WRITE32(pPlxCtrl, USB_PLX_IDXADDR_REG, 
                        USB_PLX_FS_INTPOLL_RATE_IDX);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_IDXDATA_REG);
    printf("FS_INTPOLL     = 0x%08X\n", uReg32);
    USB_PLX_REG_WRITE32(pPlxCtrl, USB_PLX_IDXADDR_REG, 
                        USB_PLX_HS_NAK_RATE_IDX);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_IDXDATA_REG);
    printf("HS_NAK         = 0x%08X\n", uReg32);
    USB_PLX_REG_WRITE32(pPlxCtrl, USB_PLX_IDXADDR_REG,
                        USB_PLX_SCRATCH_IDX);
    uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_IDXDATA_REG);
    printf("SCRATCH        = 0x%08X\n", uReg32);

    for (i = 1; i <= 4; i ++)
        {    
        USB_PLX_REG_WRITE32(pPlxCtrl, USB_PLX_IDXADDR_REG, 
                            USB_PLX_EP_X_HS_MAXPKT_IDX(i));
        uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_IDXDATA_REG);
        printf("EP_%c_HS_MAXPKT = 0x%08X\n",'A' + i - 1, uReg32);
        USB_PLX_REG_WRITE32(pPlxCtrl, USB_PLX_IDXADDR_REG, 
                            USB_PLX_EP_X_FS_MAXPKT_IDX(i));
        uReg32 = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_IDXDATA_REG);
        printf("EP_%c_FS_MAXPKT = 0x%08X\n",'A' + i - 1, uReg32);
        }    
    
    return OK;
    }



/*******************************************************************************
*
* usbPlxTcdRequestShow - show the usb_plx_tcd_pipe structure's information
*
* This routine shows the usb_plx_tcd_pipe structure's information
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbPlxTcdRequestShow
    (
    pUSB_PLX_TCD_REQUEST pRequest
    )
    {
    if (NULL == pRequest)
        {
        return ERROR;
        }
    printf(" |\n");    
    printf(" | - pRequest           = %p\n", pRequest);
    printf(" |  - reqNode         = %p\n", &pRequest->reqNode);
    printf(" |  - pTCDPipe        = %p\n", pRequest->pTCDPipe);
    printf(" |  - pErp            = %p\n", pRequest->pErp);
    printf(" |  - usrDataDmaMapId = %p\n", pRequest->usrDataDmaMapId);
    printf(" |  - pCurrentBuffer  = %p\n", pRequest->pCurrentBuffer);
    printf(" |  - uXferSize       = 0x%08X\n", pRequest->uXferSize);
    printf(" |  - uActLength      = 0x%08X\n", pRequest->uActLength);    
    printf(" |  - uCurXferLength  = 0x%08X\n", pRequest->uCurXferLength);
    printf(" |  - uErpBufIndex    = 0x%08X\n", pRequest->uErpBufIndex);
    printf(" |  - uErpBufCount    = 0x%08X\n", pRequest->uErpBufCount);
   /* printf("  dmaQueue        = 0x%08X\n", pRequest->dmaQueue); */
    printf(" |  - uPid            = 0x%08X\n", pRequest->uPid);
    printf(" |  - uDmaMode        = 0x%08X\n", pRequest->uDmaMode);
    printf(" |  - bDmaActive      = 0x%08X\n", pRequest->bDmaActive);
    
    return OK;
    }



/*******************************************************************************
*
* usbPlxTcdPipeShow - show the usb_plx_tcd_pipe structure's information
*
* This routine shows the usb_plx_tcd_pipe structure's information
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbPlxTcdPipeShow
    (
    pUSB_PLX_TCD_PIPE pTcdPipe
    )
    {
    pUSB_PLX_TCD_REQUEST pRequest = NULL;
    NODE *               pNode = NULL;

    if (NULL == pTcdPipe)
        {
        return ERROR;
        }
    printf("|\n");    
    printf("| - pTcdPipe            = %p\n", pTcdPipe);    
    printf("|  - pipeNode            = %p\n", &pTcdPipe->pipeNode);
    printf("|  - pPipeSyncMutex      = %p\n", pTcdPipe->pPipeSyncMutex);
    printf("|  - pUsbTgtPipe         = %p\n", pTcdPipe->pUsbTgtPipe);
    printf("|  - pipeDmaTagId        = %p\n", pTcdPipe->pipeDmaTagId);
    printf("|  - pipeDmaMapId        = %p\n", pTcdPipe->pipeDmaMapId);
    printf("|  - pTCDData            = %p\n", pTcdPipe->pTCDData);    
    printf("|  - uIsoPipeErpCount    = 0x%08X\n", pTcdPipe->uIsoPipeErpCount);
    printf("|  - uIsoPipeCurErpIndex = 0x%08X\n", pTcdPipe->uIsoPipeCurErpIndex);
    printf("|  - uMaxPacketSize      = 0x%08X\n", pTcdPipe->uMaxPacketSize);
    printf("|  - uMult               = 0x%08X\n", pTcdPipe->uMult);
    printf("|  - uEpAddr             = 0x%08X\n", pTcdPipe->uEpAddr);
    printf("|  - uEpDir              = 0x%08X\n", pTcdPipe->uEpDir);
    printf("|  - uEpType             = 0x%08X\n", pTcdPipe->uEpType);
    printf("|  - uUsage              = 0x%08X\n", pTcdPipe->uUsage);
    printf("|  - uSynchType          = 0x%08X\n", pTcdPipe->uSynchType);
    printf("|  - uInterval           = 0x%08X\n", pTcdPipe->uInterval);
    printf("|  - uDataToggle         = 0x%08X\n", pTcdPipe->uDataToggle);
    printf("|  - uConfigValue        = 0x%08X\n", pTcdPipe->uConfigValue);
    printf("|  - uInterface          = 0x%08X\n", pTcdPipe->uInterface);
    printf("|  - uAltSetting         = 0x%08X\n", pTcdPipe->uAltSetting);
    printf("|  - uPhyEpAddr          = 0x%08X\n", pTcdPipe->uPhyEpAddr);
    printf("|  - uDmaChannel         = 0x%08X\n", pTcdPipe->uDmaChannel);
    printf("|  - uPipeFlag           = 0x%08X\n", pTcdPipe->uPipeFlag);
    printf("|  - status              = 0x%08X\n", pTcdPipe->status);

    printf("|  - pendingReqList      = %p\n\n", &pTcdPipe->pendingReqList);
    pNode = lstFirst (&pTcdPipe->pendingReqList);

    while (NULL != pNode)
        {
        pRequest = USB_PLX_TCD_REQUEST_CONV_FROM_NODE(pNode);

        if (NULL != pRequest)
            {
            usbPlxTcdRequestShow(pRequest);
            }
        pNode = lstNext (pNode);
        }
    
    return OK;
    }


/*******************************************************************************
*
* usbPlxTcdDataShow - show the usb_plx_tcd_data structure's information
*
* This routine shows the usb_plx_tcd_data structure's information
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbPlxTcdDataShow
    (
    UINT8 uIndex
    )
    {
    pUSB_PLX_TCD_DATA pTCDData = NULL;
    pUSB_PLX_CTRL     pPlxCtrl = NULL;
    NODE *            pNode = NULL;
    pUSB_PLX_TCD_PIPE pTcdPipe = NULL;

    pNode = lstNth(&gUsbPlxTcdList, uIndex + 1);

    if (NULL == pNode) 
        {
        printf("No find the PLX TCD instance by index %d\n", uIndex);
        return ERROR;
        }

    pTCDData = USB_PLX_TCD_DATA_CONV_FROM_NODE(pNode);

    if ((NULL == pTCDData) ||
        (NULL == pTCDData->pPlxCtrl))
        {
        printf("Invalid PLX TCD instance by index %d\n", uIndex);
        return ERROR;
        }    

    pPlxCtrl = pTCDData->pPlxCtrl;
    
    printf("\npTCDData             = %p\n", pTCDData);
    printf(" - pTcd             = %p\n", pTCDData->pTcd);
    printf(" - tcdNode          = %p\n", &pTCDData->tcdNode);
    printf(" - ep0Stage         = 0x%08X\n", pTCDData->ep0Stage);
    printf(" - pPlxCtrl         = %p\n", pTCDData->pPlxCtrl);
    printf(" - tcdSyncMutex     = %p\n", pTCDData->tcdSyncMutex);
    printf(" - isrEventId       = %p\n", pTCDData->isrEventId);    
    printf(" - isrHandlerThread = 0x%08X\n", pTCDData->isrHandlerThread);
    printf(" - uIrqStat0        = 0x%08X\n", pTCDData->uIrqStat0);
    printf(" - uIrqStat1        = 0x%08X\n", pTCDData->uIrqStat1);
    printf(" - uMaxPhyEps       = 0x%08X\n", pTCDData->uMaxPhyEps);
    printf(" - uMaxLagicalEps   = 0x%08X\n", pTCDData->uMaxLagicalEps);
    printf(" - inEpUseFlag      = 0x%08X\n", pTCDData->inEpUseFlag);
    printf(" - outEpUseFlag     = 0x%08X\n", pTCDData->outEpUseFlag);
    printf(" - uTestMode        = 0x%08X\n", pTCDData->uTestMode);
    printf(" - isDeviceSuspend  = 0x%08X\n", pTCDData->isDeviceSuspend);
    printf(" - isRemoteWakeup   = 0x%08X\n", pTCDData->isRemoteWakeup);
    printf(" - isSelfPower      = 0x%08X\n", pTCDData->isSelfPower);
    printf(" - isSoftConnected  = 0x%08X\n", pTCDData->isSoftConnected);

    printf(" - tcdPipeList      = %p\n\n", &pTCDData->tcdPipeList);
    pNode = lstFirst (&pTCDData->tcdPipeList);

    while (NULL != pNode)
        {
        pTcdPipe = USB_PLX_TCD_PIPE_CONV_FROM_NODE(pNode);

        if (NULL != pTcdPipe)
            {
            usbPlxTcdPipeShow(pTcdPipe);
            }
        pNode = lstNext (pNode);
        }

    return OK;
    
    }



