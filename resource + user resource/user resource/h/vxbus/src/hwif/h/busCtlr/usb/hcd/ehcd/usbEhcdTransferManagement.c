/* usbEhcdTransferManagement.c - transfer management functions of the EHCD */

/*
 * Copyright (c) 2002-2011, 2013-2015 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2002-2011, 2013-2015 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
modification history
--------------------
03a,09sep15,j_x  Add parameters for EHCI polling and interrupt threshold (VXW6-84172) 
02z,30Jul15,hma  Fix the memrory leak when calling usbExit (VXW6-84619)
02y,25Feb14,wyy  Enable vxbDmaBuf alignment checking (VXW6-70065)
02x,01Aug13,wyy  Remove END debugging errors on detach (WIND00427931)
02w,10Jul13,wyy  Make usbd layer to uniformly handle ClearTTBuffer request
                 (WIND00424927)
02v,03may13,wyy  Remove compiler warning (WIND00356717)
02u,21apr13,wyy  Cut the relationship among busIndex of HCDData, unitNumber of 
                 vxBus Device, and HC count (such as g_EHCDControllerCount or 
                 g_UhcdHostControllerCount) to dynamically release or announce
                 a HCD driver (WIND00362065)
02t,13mar13,s_z  Add USB_EHCD_WRITE_BUFFER_DRAIN to make sure data updated be 
                 seen by the controller (WIND00381556)
02s,04jan13,s_z  Remove compiler warning (WIND00390357)
02r,28dec11,ljg  Save ping state while submit and cancel URB (WIND00315226)
02q,16sep11,s_z  Add usbEhcdAsynchScheduleEnable and usbEhcdAsynchScheduleDisable
                 routines to avoid dead loop to process the asynchnorous schedule
                 (WIND00293308)
02p,07jan11,ghs  Clean up compile warnings (WIND00247082)
02o,02sep10,ghs  Remove complie warning for LP64
02n,17aug10,ghs  Remove compile warning (WIND00228305)
02m,30jul10,w_x  Correct URB cancel handling (WIND00223154)
02l,22jul10,m_y  Modify host controller index compare
02k,27may10,w_x  Avoid dead loop waiting for ASYNCH_SCHEDULE_ENABLE to be
                 set or cleared (WIND00214252)
                 Fix ASYNCH pipe deletion crash (WIND00214253)
02j,18may10,w_x  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
02i,09mar10,j_x  Changed for USB debug (WIND00184542)
02h,13jan10,ghs  vxWorks 6.9 LP64 adapting
02g,27sep09,y_l  Fix bug: missing semaphore release operation (WIND00183805)
02f,17sep09,y_l  Code Coverity CID(3925): NULL pointer check
                 Code Coverity CID(5242): make more readable code (WIND00182326)
02e,07sep09,ghs  Change QH RL field to max at control/bulk transfer.
                 (WIND00091032)
02d,15jul09,ghs  Fix for WIND00171264, remove align defines
02c,09apr09,w_x  Remove tabs and coding convention changes
02b,09feb09,w_x  Added "intEachTD" as a config paramter to make WIND00084918
                 fix built into library and configurable (with minor clean up)
02a,04feb09,w_x  Added support for FSL quirky EHCI with various register and
                 descriptor endian format (such as MPC5121E)
01z,20nov08,w_x  Convert pEndpointDesc->wMaxPacketSize into CPU endian format
                 in usbEhcdCreatePipe to a local variable (WIND00144630)
01y,23jul08,w_x  Compiler warnning fix for WIND00128597
01x,16jul08,w_x  Added non-standard root hub TT support (WIND00127895)
01w,07jul08,w_x  Code coverity changes (WIND00126885)
01v,20nov07,ami  NEC fix integrated under conditional macros (CQ:WIND00084918)
01u,27aug07,pdg  Enabled asynchronous schedule only if there is an outstanding
                 transfer(WIND00102592)
01t,07oct06,ami  Changes for USB-vxBus porting
01s,28mar05,pdg  non-PCI changes
01r,22mar05,mta  64-bit support added (SPR #104950)
01q,02mar05,ami  SPR #106383 (Max EHCI Host Controller Issue)
01p,02feb05,pdg  Fix for multiple device connection/disconnection
01o,19jan05,pdg  Fix for SPR #104949(USB STACK 2.1 PID 2.1 ISN'T CORRECTLY
                 SUPPORTING ON BOARD VIA VT8237 EHCI HOST)
01n,10dec04,pdg  corrected IP code error - short packet detection
01m,03dec04,ami  Merged IP Changes
01l,26oct04,ami  Severity Changes for Debug Messages
01k,15oct04,ami  Apigen Changes
01j,16aug04,pdg  Fix for print-stop-reprint
01i,03aug04,pdg  Fixed coverity errors
01h,03aug04,ami  Warning Messages Removed
01g,05jul04,???  TD is accessed even after cancelling. Fix for this is
                 provided here.
01f,08Sep03,nrv  Changed g_HostControllerCount to g_EHCDControllerCount
01e,28Jul03,gpd  Incorporated the changes after integration testing.
01d,23Jul03,gpd  Incorporated the changes after testing on MIPS.
01c,03Jul03,gpd  changed the search algorithm on a delete pipe
                 cancelling URBs should use the asynch on advance and frame
                 list rollover interrupts.
01b,26jun03,gpd  changing the code to WRS standards.
01a,25apr02,ram  written.
*/

/*
DESCRIPTION

This module defines the interfaces which are registered with the USBD during
EHCI Host Controller Driver initialization.

INCLUDE FILES: usbhst.h, usbEhcdDataStructures.h, usbEhcdInterfaces.h,
usbEhcdUtil.h, usbEhcdConfig.h, usbEhcdHal.h, usbEhcdRHEmulation.h,
usbEhcdDebug.h

SEE ALSO: N/A
*/

/*
INTERNAL
 *******************************************************************************
 * Filename         : EHCD_TransferManagement.c
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 * Description      :  This contains the functions which handle the transfer
 *                     management requests from the USBD.
 *
 *
 ******************************************************************************/

/* includes */

#include "usb/usbOsal.h"
#include "usb/usbHst.h"
#include "usbEhcdDataStructures.h"
#include "usbEhcdInterfaces.h"
#include "usbEhcdUtil.h"
#include "usbEhcdConfig.h"
#include "usbEhcdHal.h"
#include "usbEhcdRhEmulation.h"
#include "usb/usbHcdInstr.h"
spinlockIsr_t spinLockIsrEhcd[USB_MAX_EHCI_COUNT];//zj
/* globals */

LOCAL USBHST_STATUS usbEhcdQueueQTDs
    (
    pUSB_EHCD_DATA          pHCDData,
    pUSB_EHCD_PIPE          pHCDPipe,
    pUSB_EHCD_REQUEST_INFO  pRequest,
    pUSBHST_URB             pURB
    );

LOCAL USBHST_STATUS usbEhcdSubmitControlURB
    (
    pUSB_EHCD_DATA          pHCDData,
    pUSB_EHCD_PIPE          pHCDPipe,
    pUSB_EHCD_REQUEST_INFO  pRequest,
    pUSBHST_URB             pURB
    );

LOCAL USBHST_STATUS usbEhcdSubmitBulkIntrURB
    (
    pUSB_EHCD_DATA          pHCDData,
    pUSB_EHCD_PIPE          pHCDPipe,
    pUSB_EHCD_REQUEST_INFO  pRequest,
    pUSBHST_URB             pURB
    );

LOCAL USBHST_STATUS usbEhcdSubmitIsochURB
    (
    pUSB_EHCD_DATA          pHCDData,
    pUSB_EHCD_PIPE          pHCDPipe,
    pUSB_EHCD_REQUEST_INFO  pRequest,
    pUSBHST_URB             pURB
    );

/*******************************************************************************
*
* usbEhcdUnSetupPipe - destroy any allocated transfer resources for a pipe
*
* This routine is used to destroy any allocated transfer resource for a pipe.
* The transfer resources, including request info structures associated DMA
* tag and DMA map are destroyed.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: USBHST_SUCCESS
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbEhcdUnSetupPipe
    (
    pUSB_EHCD_DATA pHCDData,
    pUSB_EHCD_PIPE pHCDPipe
    )
    {
    /* Request information */

    pUSB_EHCD_REQUEST_INFO  pRequest;

    /* Temp request information */

    pUSB_EHCD_REQUEST_INFO  pTempRequest;

    /* Destroy the created request info structures */

    for (pRequest = pHCDPipe->pFreeRequestQueueHead; /* Start from the head */
         pRequest != NULL;  /* Make sure it is not NULL */
         pRequest = pTempRequest) /* Goto the next request when complete */
        {
        /* Get the next request info */

        pTempRequest = pRequest->pNext;

        /* Delete the request resources */

        usbEhcdDeleteRequestInfo(pHCDData, pHCDPipe, pRequest);
        }

    /* Set the head and tal as NULL */

    pHCDPipe->pFreeRequestQueueHead = NULL;

    pHCDPipe->pFreeRequestQueueTail = NULL;

    /* Destroy all the free TDs */

    usbEhcdDestroyAllTDs(pHCDData, pHCDPipe);

    /* Destroy the user Data DMA TAG */

    if (pHCDPipe->usrBuffDmaTagId != NULL)
        {
        USB_EHCD_DBG("usbEhcdUnSetupPipe - free usrBuffDmaTagId\n",
            0, 0, 0, 0, 0, 0);

        (void) vxbDmaBufTagDestroy(pHCDPipe->usrBuffDmaTagId);

        pHCDPipe->usrBuffDmaTagId = NULL;
        }

    /* Destroy the control Setup DMA TAG */

    if (pHCDPipe->ctrlSetupDmaTagId != NULL)
        {
        USB_EHCD_DBG("usbEhcdUnSetupPipe - free ctrlSetupDmaTagId\n",
            0, 0, 0, 0, 0, 0);

        (void) vxbDmaBufTagDestroy(pHCDPipe->ctrlSetupDmaTagId);

        pHCDPipe->ctrlSetupDmaTagId = NULL;
        }

    return USBHST_SUCCESS;
    }

/*******************************************************************************
*
* usbEhcdSetupPipe - allocate transfer resources for a pipe
*
* This routine is used to allocate transfer resources for a pipe.
* The transfer resources, including request info structures and associated DMA
* tag and DMA map are created.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: USBHST_SUCCESS when all resources allocation is done
*          USBHST_INVALID_PARAMETER when any paramter is invalid
*          USBHST_INVALID_REQUEST when there is any active transfer on the pipe
*          USBHST_INSUFFICIENT_MEMORY when any resource can not be allocated
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbEhcdSetupPipe
    (
    pUSB_EHCD_DATA              pHCDData,
    pUSB_EHCD_PIPE              pHCDPipe,
    pUSB_TRANSFER_SETUP_INFO    pSetupInfo
    )
    {
    /* Request information */

    pUSB_EHCD_REQUEST_INFO  pRequest;

    /* Transfer request index */

    UINT32                  uReqIndex;

    /* Check if the parameters are valid */

    if ((pHCDData == NULL) ||
        (pHCDPipe == NULL) ||
        (pSetupInfo == NULL) ||
        (pSetupInfo->uMaxNumReqests == 0)) /* Must have some transfer */
        {
        USB_EHCD_ERR("usbEhcdSetupPipe - invalid parameter %s is NULL\n",
                     ((pHCDData == NULL) ? "pHCDData" :
                      (pHCDPipe == NULL) ? "pHCDPipe":
                      (pSetupInfo == NULL)? "pSetupInfo":
                      "pSetupInfo->uMaxNumReqests"), 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }

    /*
     * The pipe setup must be performed when there is no active transfers
     */

    if (pHCDPipe->pRequestQueueHead != NULL)
        {
        USB_EHCD_ERR("usbEhcdSetupPipe - pRequestQueueHead non NULL\n"
            "There are still active transfers in progress\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_REQUEST;
        }

    /* Remove any previous setting */

    usbEhcdUnSetupPipe(pHCDData, pHCDPipe);

    /* Copy transfer setup info */

    pHCDPipe->uMaxTransferSize = pSetupInfo->uMaxTransferSize;

    pHCDPipe->uMaxNumReqests = pSetupInfo->uMaxNumReqests;

    pHCDPipe->uFlags = pSetupInfo->uFlags;

    /* Create tag for mapping Data buffer */

    pHCDPipe->usrBuffDmaTagId = vxbDmaBufTagCreate (pHCDData->pDev,
        pHCDData->ehciParentTag,        /* parent */
        1,                              /* alignment */
        0,                              /* boundary */
        pHCDData->usrDmaTagLowAddr,     /* lowaddr */
        VXB_SPACE_MAXADDR,              /* highaddr */
        NULL,                           /* filter */
        NULL,                           /* filterarg */
        pHCDPipe->uMaxTransferSize,     /* max size */
        1,                              /* nSegments */
        pHCDPipe->uMaxTransferSize,     /* max seg size */
        pHCDData->usrDmaMapFlags,       /* flags */
        NULL,                           /* lockfunc */
        NULL,                           /* lockarg */
        NULL);                          /* ppDmaTag */

    if (pHCDPipe->usrBuffDmaTagId == NULL)
        {
        USB_EHCD_ERR("usbEhcdSetupPipe - allocate usrBuffDmaTagId fail\n",
            0, 0, 0, 0, 0, 0);

        usbEhcdUnSetupPipe(pHCDData, pHCDPipe);

        return USBHST_INSUFFICIENT_MEMORY;
        }

    /* Control pipe needs special treatment for the Setup */

    if (pHCDPipe->uEndpointType == USBHST_CONTROL_TRANSFER)
        {
        /*
         * Create tag for mapping Setup buffer.
         * We create the TAG with 32 bytes instead of
         * 8 bytes (Setup packet size)
         */

        pHCDPipe->ctrlSetupDmaTagId = vxbDmaBufTagCreate (pHCDData->pDev,
            pHCDData->ehciParentTag,        /* parent */
            1,                              /* alignment */
            0,                              /* boundary */
            pHCDData->usrDmaTagLowAddr,     /* lowaddr */
            VXB_SPACE_MAXADDR,              /* highaddr */
            NULL,                           /* filter */
            NULL,                           /* filterarg */
            32,                             /* max size */
            1,                              /* nSegments */
            32,                             /* max seg size */
            pHCDData->usrDmaMapFlags,       /* flags */
            NULL,                           /* lockfunc */
            NULL,                           /* lockarg */
            NULL);                          /* ppDmaTag */

        if (pHCDPipe->ctrlSetupDmaTagId == NULL)
            {
            USB_EHCD_ERR("usbEhcdSetupPipe - allocate "
                "ctrlSetupDmaTagId fail\n",
                0, 0, 0, 0, 0, 0);

            usbEhcdUnSetupPipe(pHCDData, pHCDPipe);

            return USBHST_INSUFFICIENT_MEMORY;
            }
        }

    /* Create the requested number of request info structures */

    for (uReqIndex = 0; uReqIndex < pHCDPipe->uMaxNumReqests; uReqIndex++)
        {
        /* Create the request info */

        pRequest = usbEhcdCreateRequestInfo(pHCDData, pHCDPipe);

        if (pRequest == NULL)
            {
            USB_EHCD_ERR("usbEhcdSetupPipe - allocate pRequest fail\n",
                0, 0, 0, 0, 0, 0);

            usbEhcdUnSetupPipe(pHCDData, pHCDPipe);

            return USBHST_INSUFFICIENT_MEMORY;
            }
        }

    return USBHST_SUCCESS;
    }

/*******************************************************************************
*
* usbEhcdSetupControlPipe - allocate transfer resources for a control pipe
*
* This routine is used to allocate transfer resources for a cotnrol pipe.
* The transfer resources, including request info structures and associated DMA
* tag and DMA map are created. This is done by calling usbEhcdSetupPipe with
* maxiumn trasnfer request size for control pipe.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: USBHST_SUCCESS when all resources allocation is done
*          USBHST_FAILURE if the pipe specified is not a control pipe
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbEhcdSetupControlPipe
    (
    pUSB_EHCD_DATA pHCDData,
    pUSB_EHCD_PIPE pHCDPipe
    )
    {
    USBHST_STATUS hstSts = USBHST_FAILURE;

    USB_TRANSFER_SETUP_INFO pipeSetupInfo;

    pUSB_TRANSFER_SETUP_INFO pSetupInfo = &pipeSetupInfo;

    /* Setup a default transfer limitations */

    OS_MEMSET(&pipeSetupInfo, 0, sizeof(USB_TRANSFER_SETUP_INFO));

    /*
     * Set up the transfer characteristics such as
     * creating proper DMA TAG and DMA MAPs
     */

    switch (pHCDPipe->uEndpointType)
        {
        case USBHST_CONTROL_TRANSFER:
            {
            /* Control transfer will have a UINT16 limit in the Data phase */

            pSetupInfo->uMaxTransferSize = USB_EHCD_CTRL_MAX_DATA_SIZE;

            /* We allow only 1 control request for one pipe at a time */

            pSetupInfo->uMaxNumReqests = 1;

            /* No special flags */

            pSetupInfo->uFlags = 0;

            hstSts = usbEhcdSetupPipe(pHCDData, pHCDPipe, pSetupInfo);
            }
            break;
         default:
            break;
        }
    return hstSts;
    }

/*******************************************************************************
*
* usbEhcdPipeRemoveAll - remove all active request info strcutures for a pipe
*
* This routine is used to remove all request info strcutures for a pipe.
* This is done by putting all these active request info structures onto the
* proper request cancel list, which will be reclaimed when the current HC
* schedule. This is to avoid any pontential race with the HC, in that the HC
* can cache any part of the request transfer descriptors while the HCD is
* trying to modifying the transfer descriptors.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbEhcdPipeRemoveAll
    (
    pUSB_EHCD_DATA          pHCDData,
    pUSB_EHCD_PIPE          pHCDPipe
    )
    {
    /* To hold the pointer to the request information */

    pUSB_EHCD_REQUEST_INFO pRequest;

    /* To hold the pointer to the next request information */

    pUSB_EHCD_REQUEST_INFO pNextRequest;

    /* Start from the head of the active request list */

    pRequest = pHCDPipe->pRequestQueueHead;

    while (pRequest != NULL)
        {

        /*
         * Save the next active reuqest on the active request
         * list of this pipe so that we can safely move to the
         * next request once this request has been processed.
         */

        pNextRequest = pRequest->pNext;

        if ((USBHST_CONTROL_TRANSFER == pHCDPipe->uEndpointType) ||
            (USBHST_BULK_TRANSFER == pHCDPipe->uEndpointType))
            {
            /* Add to the asynch request removal list */

            USB_EHCD_ADD_TO_ASYNCH_REQUEST_REMOVAL_LIST(pHCDData, pRequest);
            }
        else
            {
            /* Add the request to the periodic request removal list */

            USB_EHCD_ADD_TO_PERIODIC_REQUEST_REMOVAL_LIST(pHCDData, pRequest);
            }

        /*
         * If this request is already the tail of the pipe's
         * active request list, then we can break out.
         */

        if (pRequest == pHCDPipe->pRequestQueueTail)
            break;

        /* Go to the next request */

        pRequest = pNextRequest;
        }
    }

/*******************************************************************************
*
* usbEhcdPipeReturnAll - return the URB for all active request of a pipe
*
* This routine is used to return the URB for all active request of a pipe
* to the user code (class drivers) by calling the associated URB callback.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbEhcdPipeReturnAll
    (
    pUSB_EHCD_PIPE          pHCDPipe
    )
    {
    /* To hold the pointer to the request information */

    pUSB_EHCD_REQUEST_INFO pRequest;

    /* To hold the pointer to the next request information */

    pUSB_EHCD_REQUEST_INFO pNextRequest;

    /* Start from the head of the active request list */

    pRequest = pHCDPipe->pRequestQueueHead;

    while (pRequest != NULL)
        {

        /*
         * Save the next active reuqest on the active request
         * list of this pipe so that we can safely move to the
         * next request once this request has been processed.
         */

        pNextRequest = pRequest->pNext;

        /*
         * If there is any real data transfer, we should do
         * vxbDmaBufSync and unload the DMA MAP.
         */

        if (pRequest->uRequestLength != 0)
            {
            /* Unload the Data DMA MAP */

            vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
                pRequest->usrDataDmaMapId);
            }

        /* Control transfer should also deal with the Setup buffer */

        if (pHCDPipe->uEndpointType == USBHST_CONTROL_TRANSFER)
            {
            /* Unload the control Setup DMA MAP */

            vxbDmaBufMapUnload (pHCDPipe->ctrlSetupDmaTagId,
                pRequest->ctrlSetupDmaMapId);
            }

        /* Call the callback if any */

        if (pRequest->pUrb && pRequest->pUrb->pfCallback)
            {
            USB_EHCD_DBG("usbEhcdCancelURB - calling callback %p\n",
                pRequest->pUrb->pfCallback, 0, 0, 0, 0, 0);

            /* Decouple with the request info */

            pRequest->pUrb->pHcdSpecific = NULL;

             /* Update the URB status to cancelled */

            pRequest->pUrb->nStatus = USBHST_TRANSFER_CANCELLED;

            /* Call the callback */

            (pRequest->pUrb->pfCallback)(pRequest->pUrb);

            /* Set the request's active URB as NULL indicating it is canceled */

            pRequest->pUrb = NULL;
            }

        /*
         * If this request is already the tail of the pipe's
         * active request list, then we can break out.
         */

        if (pRequest == pHCDPipe->pRequestQueueTail)
            break;

        /* Go to the next request */

        pRequest = pNextRequest;
        }
    }

/*******************************************************************************
*
* usbEhcdCreatePipe - create a pipe specific to an endpoint
*
* This routine creates the host controller driver specific data structure,
* creates the queue head if it is a non-isochronous pipe and populates the
* <puPipeHandle> parameter with the pointer to the data structure created for
* the pipe. <uBusIndex> is the index of the host controller. <uDeviceAddress>
* is the address of the device holding the endpoint. <uDeviceSpeed> is the
* speed of the device holding the endpoint. <pEndpointDescriptor> is the
* pointer to the endpoint descriptor. <uHighSpeedHubInfo> is the high speed
* hub information. <puPipeHandle> is the pointer to the handle to the pipe.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the pipe was created successfully.
*   USBHST_INVALID_PARAMETER - Returned if the parameters are not valid.
*   USBHST_INSUFFICIENT_MEMORY - Returned if the memory allocation
*                                for the pipe failed.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbEhcdCreatePipe
    (
    UINT8   uBusIndex,          /* Host controller index      */
    UINT8   uDeviceAddress,     /* USB device address         */
    UINT8   uDeviceSpeed,       /* USB device speed           */
    UCHAR  *pEndpointDescriptor,/* Endpoint descriptor        */
    UINT16  uHighSpeedHubInfo,  /* High speed hub information */
    ULONG  *puPipeHandle        /* Pointer to the pipe handle */
    )
    {
    /* To hold the status of the request */

    USBHST_STATUS Status = USBHST_FAILURE;

    /* Pointer to the Endpoint Descriptor */

    pUSBHST_ENDPOINT_DESCRIPTOR pEndpointDesc = NULL;

    /* To hold the pointer to the EHCD maintained pipe data structure */

    pUSB_EHCD_PIPE pHCDPipe = NULL;

    /* Bandwidth required for this pipe */

    UINT32 uBandwidth = 0;

    /* Pointer to the HCD specific data structure */

    pUSB_EHCD_DATA  pHCDData = NULL;

    /* Microframe mask */

    UINT32 uUFrameMask = 0;

    /* To hold the index of the list */

    UINT32 uListIndex = 0;

    /* To hold the index into the microframes */

    UINT32 uUFrameIndex = 0;

    /* To hold the control end point */

    UINT32 uControlEndPoint;

    /* To hold the pointer to the Queue Head */

    pUSB_EHCD_QH pQH = NULL;

    /* To hold the endpoint wMaxPacketSize in CPU endian format */

    UINT16 wMaxPacketSizeCpu = 0;

    /* To hold the endpoint type */

    UINT8 uEndpointType = 0;

    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT(
        USB_EHCI_WV_TRANSFER,
        "usbEhcdCreatePipe() starts",
        USB_EHCD_WV_FILTER);

    /* Check the validity of the parameters */

    if ((USB_MAX_EHCI_COUNT <= uBusIndex) ||
        (USB_EHCD_MAX_DEVICE_ADDRESS < uDeviceAddress) ||
        (NULL == pEndpointDescriptor) ||
        (NULL == puPipeHandle))
        {
        USB_EHCD_ERR("usbEhcdCreatePipe - parameters are not valid\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the global data structure */

    pHCDData = g_pEHCDData[uBusIndex];

    /* Return if the global pointer is not valid */

    if (NULL == pHCDData)
        {
        USB_EHCD_ERR("usbEhcdCreatePipe - pHCDData is NULL\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

   /*
    * With embedded TT, the QH hub address field for direct attached
    * devices should be 0; However, the high byte of uHighSpeedHubInfo
    * is set to the address of the root hub by the usbd, which is
    * typically 1; so if we enable embdded TT support, and when the
    * QH is for the direct attached devices (whose hub address is set to
    * the root hub address), we simply mask the high byte of
    * uHighSpeedHubInfo to make the hub address field of the QH set to 0;
    */

    if ((pHCDData->hasEmbeddedTT == TRUE) &&
        (((uHighSpeedHubInfo & USB_EHCD_PARENT_HUB_ADDRESS_MASK) >> 8)
        == pHCDData->RHData.uDeviceAddress))
        {
        uHighSpeedHubInfo =
            (UINT16)(uHighSpeedHubInfo & (~USB_EHCD_PARENT_HUB_ADDRESS_MASK));
        /*
         * Check if we are required to workaround the USB errata #14
         * on some old MPC834x targets ; The errata is described as :
         * Port number in the Queue head is 0 to N-1. It should be 1 to
         * N according to EHCI spec. This bug only affects the host mode.
         *
         * The workaround for this is :
         * Use the port number 0 to N - 1 instead of 1 to N.
         */

        if (pHCDData->fixupPortNumber == TRUE)
            {
            uHighSpeedHubInfo = (UINT16)(uHighSpeedHubInfo - 1);
            }
        }

    /* Check if the request is for the Root hub and route it */

    if (uDeviceAddress == pHCDData->RHData.uDeviceAddress)
        {
        Status = usbEhcdRhCreatePipe(pHCDData,
                                     uDeviceAddress,
                                     uDeviceSpeed,
                                     pEndpointDescriptor,
                                     puPipeHandle);
        return Status;
        }

    /* Retrieve the endpoint descriptor */

    pEndpointDesc = (pUSBHST_ENDPOINT_DESCRIPTOR)pEndpointDescriptor;

    /* Swap the maximum packet size to get it in CPU endian format */

    wMaxPacketSizeCpu = OS_UINT16_LE_TO_CPU(pEndpointDesc->wMaxPacketSize);

    /* Get the endpoint type */

    uEndpointType = (UINT8)(pEndpointDesc->bmAttributes &
                            USB_EHCD_ENDPOINT_TYPE_MASK);

    /* Check the validity of the endpoint type */

    if ((USBHST_ISOCHRONOUS_TRANSFER != uEndpointType) &&
        (USBHST_INTERRUPT_TRANSFER != uEndpointType) &&
        (USBHST_CONTROL_TRANSFER != uEndpointType) &&
        (USBHST_BULK_TRANSFER != uEndpointType))
        {
        USB_EHCD_ERR("usbEhcdCreatePipe - endpoint type %d is not valid\n",
            uEndpointType, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Check if the endpoint type is isochronous or interrupt */

    if ((USBHST_ISOCHRONOUS_TRANSFER == uEndpointType) ||
        (USBHST_INTERRUPT_TRANSFER == uEndpointType))
        {
        /* To hold the maximum packet size for the endpoint */

        UINT32 uMaxPacketSize = 0;

        /* To hold the status of the bandwidth availability */

        BOOLEAN bIsBwAvailable = FALSE;

        /*
         * If the device speed is high speed, the payload is
         * equal the maxpacketsize * number of packets that can
         * be sent in a microframe.
         * The bits 0 to 10 give the maximum packet size. The bits 11 and 12
         * give the (number of packets in a microframe - 1).
         */

        if (USBHST_HIGH_SPEED == uDeviceSpeed)
            {
            uMaxPacketSize =
            (UINT32)(((wMaxPacketSizeCpu) &
             USB_EHCD_ENDPOINT_MAX_PACKET_SIZE_MASK) *
            ((((wMaxPacketSizeCpu) &
               USB_EHCD_ENDPOINT_NUMBER_OF_TRANSACTIONS_MASK) >> 11) + 1));
            }
        /*
         * If it is low or full speed, the maximum packet size is the same
         * as that retrieved from the endpoint descriptor
         */

        else
            {
            uMaxPacketSize = wMaxPacketSizeCpu;
            }

        /*
         * Calculate the bandwidth which is required for this pipe
         * Pass speed as high speed always EHCD is always high speed
         */

        uBandwidth = (UINT32)usbEhcdCalculateBusTime(USBHST_HIGH_SPEED,
                (UINT32)((pEndpointDesc->bEndpointAddress) & USB_EHCD_DIR_IN),
                (UINT32)(uEndpointType),
                uMaxPacketSize);

        /* Check if this bandwidth can be accomodated */

        bIsBwAvailable = usbEhcdCheckBandwidth(pHCDData,
                                               uBandwidth,
                                               uDeviceSpeed,
                                               pEndpointDesc,
                                               &uListIndex,
                                               &uUFrameMask);

        /* If bandwidth is not available, return an error */

        if (FALSE == bIsBwAvailable)
            {
            USB_EHCD_ERR("usbEhcdCreatePipe - bandwidth is not available\n",
                0, 0, 0, 0, 0, 0);

            return USBHST_INSUFFICIENT_BANDWIDTH;
            }
        }

    /* Allocate memory for the USB_EHCD_PIPE data structure */

    pHCDPipe = (pUSB_EHCD_PIPE)OS_MALLOC(sizeof(USB_EHCD_PIPE));

    /* Check if memory allocation is successful */

    if (NULL == pHCDPipe)
        {
        USB_EHCD_ERR("usbEhcdCreatePipe - Memory not allocated for EHCD pipe\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_MEMORY_NOT_ALLOCATED;
        }

    /* Initialize the pipe data structure */

    USB_EHCD_PIPE_INITIALIZE(pHCDPipe);

    /* Create the event used for synchronization of requests for this pipe */

    pHCDPipe->PipeSynchEventID = OS_CREATE_EVENT(OS_EVENT_SIGNALED);

    /* Check if the event is created successfully */

    if (NULL == pHCDPipe->PipeSynchEventID)
        {
        USB_EHCD_ERR(
            "usbEhcdCreatePipe - PipeSynchEventID not created\n",
            0, 0, 0, 0, 0, 0);

        OS_FREE(pHCDPipe);

        return USBHST_INSUFFICIENT_RESOURCE;
        }

    /* Update the pointer to the pipe handle */

    *puPipeHandle = (ULONG)pHCDPipe;

    /* Update the endpoint speed */

    pHCDPipe->uSpeed = uDeviceSpeed;

    /* Update the device address which holds the endpoint */

    pHCDPipe->uAddress = uDeviceAddress;

    /* Update the high speed hub information */

    pHCDPipe->uHubInfo = uHighSpeedHubInfo;

    /* Update the type of endpoint to be created */

    pHCDPipe->uEndpointType = uEndpointType;

    /* Update the direction of the endpoint */

    pHCDPipe->uEndpointDir =
        ((pEndpointDesc->bEndpointAddress & USB_EHCD_DIR_IN)
          >> USB_EHCD_DIR_BIT_POSITION) & 0x1;

    /* Update Endpoint address */

    pHCDPipe->uEndpointAddress = pEndpointDesc->bEndpointAddress;

    /*
     * Store the maximum packet size - for high speed includes the number
     * of transactions in a microframe also
     */

    pHCDPipe->uMaximumPacketSize = wMaxPacketSizeCpu;

    /* If it is an interrupt or isochronous endpoint, update the bandwidth */

    if ((USBHST_ISOCHRONOUS_TRANSFER == pHCDPipe->uEndpointType) ||
        (USBHST_INTERRUPT_TRANSFER == pHCDPipe->uEndpointType))
        {
        /* To hold the status of the bandwidth reservation */

        BOOLEAN bStatus = FALSE;

        /* Update the bandwidth occupied by the endpoint */

        pHCDPipe->uBandwidth = uBandwidth;

        /* Initialize the last index */

        pHCDPipe->uLastIndex = USB_EHCD_NO_LIST;

        /* Copy the list index */

        pHCDPipe->uListIndex = (UINT16)uListIndex;

        /* Copy the mask value */

        pHCDPipe->uUFrameMaskValue = (UINT8)uUFrameMask;

        /* Update the tree node bandwidth if it is an interrupt endpoint */

        if (USBHST_INTERRUPT_TRANSFER == pHCDPipe->uEndpointType)
            {
            /* Copy the list index */

            pHCDPipe->uListIndex = (UINT16)uListIndex;

            /* Exclusively access the bandwidth resource */

            OS_WAIT_FOR_EVENT(pHCDData->BandwidthEventID,OS_WAIT_INFINITE);

            /* This loop will update the tree bandwidth for every microframe */

            for (uUFrameIndex = 0;
                USB_EHCD_MAX_MICROFRAME_NUMBER > uUFrameIndex;
                uUFrameIndex++)
                {

                /* Check if bandwidth is to be updated for this
                 * microframe and update the bandwidth
                 */

                if (0 != ((uUFrameMask >> uUFrameIndex) & 0x01))
                    {
                    pHCDData->TreeListData[uListIndex].uBandwidth[uUFrameIndex]
                    += uBandwidth;
                    }
                }

            /* Release the bandwidth exclusive access */

            OS_RELEASE_EVENT(pHCDData->BandwidthEventID);

            /* Update the overall bandwidth */

            bStatus = usbEhcdUpdateBandwidth(pHCDData);

            /*
             * If the bandwidth reservation is not successful,
             * release the bandwidth
             */

            if (FALSE == bStatus)
                {
                USB_EHCD_ERR("usbEhcdCreatePipe - Bandwidth not available\n",
                    0, 0, 0, 0, 0, 0);

                /* Exclusively access the bandwidth resource */

                OS_WAIT_FOR_EVENT(pHCDData->BandwidthEventID,OS_WAIT_INFINITE);

                /*
                 * This loop will update the tree bandwidth
                 * for every microframe
                 */

                for (uUFrameIndex = 0;
                     USB_EHCD_MAX_MICROFRAME_NUMBER > uUFrameIndex;
                     uUFrameIndex++)
                    {
                    /*
                     * Check if bandwidth is to be updated for this
                     * microframe and update the bandwidth
                     */

                    if (0 != ((uUFrameMask >> uUFrameIndex) & 0x01))
                        {
                        pHCDData->TreeListData[uListIndex].
                        uBandwidth[uUFrameIndex] -= uBandwidth;
                        }
                    }

                /* Release the bandwidth exclusive access */

                OS_RELEASE_EVENT(pHCDData->BandwidthEventID);

                /*
                 * Update the bandwidth.
                 * Nothing can be done with the return value. So not
                 * checking for return status.
                 */

                (void) usbEhcdUpdateBandwidth(pHCDData);

                /* Destroy the pipe synch event */

                OS_DESTROY_EVENT(pHCDPipe->PipeSynchEventID);

                pHCDPipe->PipeSynchEventID = NULL;

                /* Free memory allocated for the USB_EHCD_PIPE */

                OS_FREE(pHCDPipe);

                return USBHST_INSUFFICIENT_BANDWIDTH;
                }
            }
        /* End of if (USBHST_INTERRUPT_TRANSFER == pHCDPipe->uEndpointType) */

        /* This is for the isochronous endpoint */

        else
            {
            /* To hold the index into the frame list */

            UINT32  uFrameIndex = 0;

            /* Temporary uUFrameIndex for loop */

            UINT32  uUFrameIndexTemp;

            /* Exclusively access the bandwidth resource */

            OS_WAIT_FOR_EVENT(pHCDData->BandwidthEventID,OS_WAIT_INFINITE);

            /*
             * This loop updates the bandwidth in every frame
             * Note that the list index is the first index into the frame
             * list
             */

            for (uFrameIndex = uListIndex;
                 USB_EHCD_MAX_FRAMELIST_ENTIRES > uFrameIndex;
                 uFrameIndex++)
                {
                /*
                 * This loop will update the frame list bandwidth
                 * for every microframe
                 */

                for (uUFrameIndex = 0;
                    USB_EHCD_MAX_MICROFRAME_NUMBER > uUFrameIndex;
                    uUFrameIndex++)
                    {
                    /*
                     * Check if bandwidth is to be updated for this
                     * microframe and update the bandwidth
                     */

                    if (0 != ((uUFrameMask >> uUFrameIndex) & 0x01))
                        {
                        pHCDData->FrameListData[uFrameIndex].
                        uBandwidth[uUFrameIndex] += uBandwidth;

                        pHCDData->FrameBandwidth[uFrameIndex][uUFrameIndex] +=
                        uBandwidth;
                        }

                    /* Check if bandwidth has exceeded the limits */

                    if (USB_EHCD_EIGHTY_PERCENT_BW <
                        ((pHCDData->FrameBandwidth[uFrameIndex][uUFrameIndex])))
                        {
                        USB_EHCD_ERR("EHCD_CreatePipe - "
                            "Bandwidth has exceeded the limits\n",
                            0, 0, 0, 0, 0, 0);

                        /* Release the bandwidth allocated in all
                         * the other microframes in this frame
                         */

                        uUFrameIndexTemp = uUFrameIndex;

                        for (uUFrameIndex = 0; (uUFrameIndex <= uUFrameIndexTemp);
                            uUFrameIndex++)
                            {
                            /*
                             * If bandwidth has been reserved,
                             * unreserve the bandwidth reserved.
                             */

                            if (0 != ((uUFrameMask >> uUFrameIndex) & 0x01))
                                {
                                pHCDData->FrameListData[uFrameIndex].uBandwidth[uUFrameIndex] -=
                                    uBandwidth;

                                pHCDData->FrameBandwidth[uFrameIndex][uUFrameIndex] -=
                                    uBandwidth;
                                }
                            }

                        /*
                         * Release the bandwidth allocated in
                         * the remaining frames
                         */

                        /*
                         * If the bandwidth is being reserved for the
                         * first time, then this condition will fail
                         * indicating that the bandwidth has already been
                         * released
                         */

                        if (uListIndex < uFrameIndex)
                            {
                            /*
                             * As this frame's bandwidth has already
                             * been released, start releasing from
                             * the previous frame index
                             */

                            uFrameIndex--;

                            for (;(uFrameIndex >= uListIndex) &&
                                (USB_EHCD_MAX_FRAMELIST_ENTIRES > uFrameIndex);
                                uFrameIndex --)
                                {
                                /*
                                 * Release the bandwidth in all the microframes
                                 * in the frame
                                 */

                                for (uUFrameIndex = 0;
                                    USB_EHCD_MAX_MICROFRAME_NUMBER > uUFrameIndex;
                                    uUFrameIndex++)
                                    {
                                    pHCDData->FrameListData[uFrameIndex].
                                    uBandwidth[uUFrameIndex] -= uBandwidth;

                                    pHCDData->FrameBandwidth[uFrameIndex][uUFrameIndex] -=
                                    uBandwidth;
                                    }
                                }
                            }

                        /* Destroy the pipe synch event */

                        OS_DESTROY_EVENT(pHCDPipe->PipeSynchEventID);

                        pHCDPipe->PipeSynchEventID = NULL;

                        /*
                         * Free the memory allocated for the
                         * EHCD_PIPE structure.
                         */

                        OS_FREE(pHCDPipe);

                        /*
                         * Release the exclusive access of the
                         * bandwidth resource
                         */

                        OS_RELEASE_EVENT(pHCDData->BandwidthEventID);

                        /* Return an error from the function */

                        return USBHST_INSUFFICIENT_BANDWIDTH;
                        }
                    }
                }

            /* Release the bandwidth exclusive access */

            OS_RELEASE_EVENT(pHCDData->BandwidthEventID);

            /* Copy the step value and the mask values */

            pHCDPipe->uUFrameMaskValue = (UINT8)uUFrameMask;

            /*
             * If the pipe creation is called in context of the URB completion
             * processing, then we should delay the active pipe list modification
             * until the URB completion processing is done in the end of routine
             * usbEhcdProcessTransferCompletion.
             */

            if ((TASK_ID)taskIdCurrent == pHCDData->IntHandlerThreadID)
                {
                /* Add to the delayed pipe addition list */

                USB_EHCD_ADD_TO_DELAYED_PIPE_ADDITION_LIST(pHCDData, pHCDPipe);
                }
            else
                {
                /*
                 * Add this pipe in the list of isochronous pipe maintained
                 * in HCD data. The proper locking is done in the macro.
                 */

                USB_EHCD_ADD_TO_ISOCH_PIPE_LIST(pHCDData, pHCDPipe);
                }

            return USBHST_SUCCESS;
            }
        } /* End of if ((USBHST_ISOCHRONOUS_TRANSFER ==... */

    /* The following code will be executed for non-isochronous endpoints */

    /* Create an empty QH */

    pQH = usbEhcdFormEmptyQH(pHCDData);

    /* Check if QH is valid */

    if (NULL == pQH)
        {
        USB_EHCD_ERR("usbEhcdCreatePipe - QH not created\n",
            0, 0, 0, 0, 0, 0);

        /* Destroy the pipe synch event */

        OS_DESTROY_EVENT(pHCDPipe->PipeSynchEventID);

        pHCDPipe->PipeSynchEventID = NULL;

        /*
         * Free the memory allocated for the
         * EHCD_PIPE structure.
         */

        OS_FREE(pHCDPipe);

        return USBHST_FAILURE;
        }

    /* Do some default setup for control transfer */

    if (pHCDPipe->uEndpointType == USBHST_CONTROL_TRANSFER)
        {
        if (usbEhcdSetupControlPipe(pHCDData, pHCDPipe) !=
            USBHST_SUCCESS)
            {
            USB_EHCD_ERR("usbEhcdCreatePipe - usbEhcdSetupControlPipe fail\n",
                0, 0, 0, 0, 0, 0);

            /* Destroy the pipe synch event */

            OS_DESTROY_EVENT(pHCDPipe->PipeSynchEventID);

            pHCDPipe->PipeSynchEventID = NULL;

            /* Destroy the QH */

            usbEhcdDestroyQH(pHCDData, pHCDPipe->pQH);

            /*
             * Free the memory allocated for the pipe structure.
             */

            OS_FREE(pHCDPipe);

            return USBHST_FAILURE;
            }
        }

    /* Copy the QH pointer in USB_EHCD_PIPE data structure */

    pHCDPipe->pQH = pQH;

    /* Copy the USB_EHCD_PIPE pointer in the QH */

    pQH->pHCDPipe = pHCDPipe;

    /* Populate the fields of the QH - start */

    /* Populate the device address */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          QH,
                          pQH->uEndPointCharacteristics,
                          uDeviceAddress,
                          ENDPOINT_CHARACTERISTICS_DEVICE_ADDRESS);

    /* Populate the endpoint number */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          QH,
                          pQH->uEndPointCharacteristics,
                          ((pEndpointDesc->bEndpointAddress) &
                            USB_EHCD_ENDPOINT_NUMBER_MASK),
                          ENDPOINT_CHARACTERISTICS_ENDPT_NUMBER);

    /* Populate the device speed */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          QH,
                          pQH->uEndPointCharacteristics,
                          uDeviceSpeed,
                          ENDPOINT_CHARACTERISTICS_ENDPT_SPEED);

    /*
     * A control transfer uses different stages of transfer(atleast 2).
     * So if it is a control transfer, use the data toggle field
     * from the TD
     */

    if (USBHST_CONTROL_TRANSFER == pHCDPipe->uEndpointType)
        {
        /* pQH->dword1.dtc = USB_EHCD_DTC_RETRIEVE_FROM_QTD; */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              QH,
                              pQH->uEndPointCharacteristics,
                              USB_EHCD_DTC_RETRIEVE_FROM_QTD,
                              ENDPOINT_CHARACTERISTICS_DTC);
        }
    /*
     * For bulk and interrupt, use the data toggle field from QH
     * Note that this function is not called for isochronous
     */

    else
        {
        /* pQH->dword1.dtc = USB_EHCD_DTC_PRESERVE_IN_QH; */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              QH,
                              pQH->uEndPointCharacteristics,
                              USB_EHCD_DTC_PRESERVE_IN_QH,
                              ENDPOINT_CHARACTERISTICS_DTC);
        }

    /* Update the maximum packet size */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          QH,
                          pQH->uEndPointCharacteristics,
                          ((wMaxPacketSizeCpu) &
                            USB_EHCD_ENDPOINT_MAX_PACKET_SIZE_MASK),
                          ENDPOINT_CHARACTERISTICS_MAXIMUM_PACKET_LENGTH);

    /*
     * This field is to be set if it is not a high speed device and the
     * endpoint is a control endpoint
     */

    uControlEndPoint = (UINT32)(((USBHST_CONTROL_TRANSFER ==
                                pHCDPipe->uEndpointType) &&
                               (USBHST_HIGH_SPEED != uDeviceSpeed)) ? 1 : 0);

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          QH,
                          pQH->uEndPointCharacteristics,
                          uControlEndPoint,
                          ENDPOINT_CHARACTERISTICS_CONTROL_ENDPOINT_FLAG);

    /*
     * Default set this bit to 1 (1 transaction per microframe)
     * For periodic high speed enpoints, mult field is calculated
     * accordingly
     */

    /* pQH->dword2.mult = 1; */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          QH,
                          pQH->uEndPointCapabilities,
                          1,
                          ENDPOINT_CAPABILITIES_MULT);

    /*
     * If the pipe is to be created for a full/low speed device
     * then it should support split transfers. So
     * update the hub address to which the full/low speed device
     * is attached and the port number to which it is attached
     */

    if (uDeviceSpeed != USBHST_HIGH_SPEED)
        {
        USB_EHCD_SET_BITFIELD(uBusIndex,
                              QH,
                              pQH->uEndPointCapabilities,
                              ((uHighSpeedHubInfo &
                                USB_EHCD_PARENT_HUB_ADDRESS_MASK) >> 8),
                              ENDPOINT_CAPABILITIES_HUB_ADDR);

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              QH,
                              pQH->uEndPointCapabilities,
                              (uHighSpeedHubInfo &
                               USB_EHCD_HUB_PORT_NUMBER_MASK),
                              ENDPOINT_CAPABILITIES_PORT_NUMBER);

        /*
         * If it is an interrupt transfer,
         * update the start and complete split masks
         */

        if (0 != uUFrameMask)
            {
            /* To hold temporarily the mask value */

            UINT8 uTempMask = (UINT8)uUFrameMask;

            /* To hold the mask value */

            UINT8 uSMask = 0x01;

            /* To hold the shift count */

            UINT8 uShiftCount = 0;

            /* This loop extracts only the start split mask */

            while (0 != uTempMask)
                {
                /* Update the start split mask value */

                if (0 != (uTempMask & 0x01))
                    {
                    uSMask = (UINT8)(uSMask << uShiftCount);

                    break;
                    }
                else
                    {
                    uTempMask >>= 1;
                    }

                uShiftCount++;
                }

            /* Update the start split mask */

            USB_EHCD_SET_BITFIELD(uBusIndex,
                                  QH,
                                  pQH->uEndPointCapabilities,
                                  uSMask,
                                  ENDPOINT_CAPABILITIES_UFRAME_S);

            /* Update the complete split mask */

            USB_EHCD_SET_BITFIELD(uBusIndex,
                                  QH,
                                  pQH->uEndPointCapabilities,
                                  (uUFrameMask & ~uSMask),
                                  ENDPOINT_CAPABILITIES_UFRAME_C);

            }

        }

    /* Device is a high speed device */

    else
        {
        /* If it is a control or bulk pipe, populate the maximum NAK rate */

        if ((USBHST_CONTROL_TRANSFER == pHCDPipe->uEndpointType) ||
            (USBHST_BULK_TRANSFER == pHCDPipe->uEndpointType))
            {
            USB_EHCD_SET_BITFIELD(uBusIndex,
                                  QH,
                                  pQH->uEndPointCharacteristics,
                                  USB_EHCD_MAX_NAK_RATE,
                                  ENDPOINT_CHARACTERISTICS_RL);
            }

        /*
         * It is a high speed interrupt transfer, update the
         * mult field(the maximum number of transactions in a microframe
         */

        else
            {
            /*
             * If the endpoint descriptor's mult field is 0, the number of
             * transactions in a microframe is 1. Because of this,
             * 1 is added to the number of transactions field.
             */

            USB_EHCD_SET_BITFIELD(uBusIndex,
                                  QH,
                                  pQH->uEndPointCapabilities,
                                  ((((wMaxPacketSizeCpu) &
                                   USB_EHCD_ENDPOINT_NUMBER_OF_TRANSACTIONS_MASK)
                                   >> 11)+ 1),
                                  ENDPOINT_CAPABILITIES_MULT);

            /* Update the mask value in the QH */

            USB_EHCD_SET_BITFIELD(uBusIndex,
                                 QH,
                                 pQH->uEndPointCapabilities,
                                 uUFrameMask,
                                 ENDPOINT_CAPABILITIES_UFRAME_S  );

            }/* End of else */
        }/* End of else */

    /* Populate the fields of the QH - End */

    /* Exclusively access the list */

    OS_WAIT_FOR_EVENT(pHCDData->RequestSynchEventID, OS_WAIT_INFINITE);

    /* Link the Queue head in the list - start */

    /* If it is the interrupt endpoint, link it to the periodic schedule */

    if (USBHST_INTERRUPT_TRANSFER == pHCDPipe->uEndpointType)
        {
        USB_EHCD_ADD_TO_HC_INTERRUPT_SCHEDULE(pHCDData, uListIndex, pQH);
        }

    /*
     * If it is a control or bulk endpoint,
     * link it to the asynchronous schedule.
     */

    else
        {
        USB_EHCD_ADD_TO_HC_ASYNCH_SCHEDULE(pHCDData, pQH);
        }

    /* Release the exclusive list access */

    OS_RELEASE_EVENT(pHCDData->RequestSynchEventID);

    /*
     * If the pipe creation is called in the context of the URB completion
     * processing, then we should delay the active pipe list modification
     * until the URB completion processing is done in the end of routine
     * usbEhcdProcessTransferCompletion.
     */

    if ((TASK_ID)taskIdCurrent == pHCDData->IntHandlerThreadID)
        {
        /* Add to the delayed pipe addition list */

        USB_EHCD_ADD_TO_DELAYED_PIPE_ADDITION_LIST(pHCDData, pHCDPipe);
        }
    else
        {
        if (USBHST_INTERRUPT_TRANSFER == pHCDPipe->uEndpointType)
            {
            /* Proper locking is done in the macro */

            USB_EHCD_ADD_TO_INTER_PIPE_LIST(pHCDData, pHCDPipe);
            }
        else
            {
            /* Proper locking is done in the macro */

            USB_EHCD_ADD_TO_ASYNCH_PIPE_LIST(pHCDData, pHCDPipe);
            }
        }

    /* Link the Queue head in the list - End */

    return USBHST_SUCCESS;
    } /* End of function usbEhcdCreatePipe() */

/*******************************************************************************
*
* usbEhcdDeletePipe - delete a pipe created already
*
* This routine is used to delete a pipe specific to an endpoint.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the pipe was deleted successfully
*   USBHST_INVALID_PARAMETER - Returned if the parameters are not valid.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbEhcdDeletePipe
    (
    UINT8   uBusIndex,   /* Index of the host controller     */
    ULONG   uPipeHandle  /* Handle of the pipe to be deleted */
    )
    {
    /* To hold the status of the request */

    USBHST_STATUS   Status = USBHST_FAILURE;

    /* To hold the pointer to the EHCD maintained pipe data structure */

    pUSB_EHCD_PIPE  pHCDPipe = NULL;

    /* Pointer to the HCD specific data structure */

    pUSB_EHCD_DATA  pHCDData = NULL;

    /* Temporary queue head pointer */

    UINT32          pQhPtrLow32;

    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT(
        USB_EHCI_WV_TRANSFER,
        "usbEhcdDeletePipe() starts",
        USB_EHCD_WV_FILTER);

    /* Check the validity of the parameters */

    if ((USB_MAX_EHCI_COUNT <= uBusIndex) ||
        (0 == uPipeHandle))
        {
        USB_EHCD_WARN("usbEhcdDeletePipe - parameters are not valid\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the global data structure */

    pHCDData = g_pEHCDData[uBusIndex];

    /* Return if the global pointer is not valid */

    if (NULL == pHCDData)
        {
        USB_EHCD_ERR("usbEhcdDeletePipe - pHCDData not valid\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the USB_EHCD_PIPE data structure */

    pHCDPipe = (pUSB_EHCD_PIPE)uPipeHandle;

    /* Check if the request is for the Root hub and route it */

    if ((pHCDData->RHData.pInterruptPipe == pHCDPipe) ||
        (pHCDData->RHData.pControlPipe == pHCDPipe))
        {
        Status = usbEhcdRHDeletePipe(pHCDData, uPipeHandle);

        return Status;
        }

    /* Check if the endpoint type is valid */

    if ((USBHST_CONTROL_TRANSFER != pHCDPipe->uEndpointType) &&
        (USBHST_BULK_TRANSFER != pHCDPipe->uEndpointType) &&
        (USBHST_INTERRUPT_TRANSFER != pHCDPipe->uEndpointType) &&
        (USBHST_ISOCHRONOUS_TRANSFER != pHCDPipe->uEndpointType))
        {
        USB_EHCD_ERR("usbEhcdDeletePipe - Invalid endpoint type\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Check if it is a non-periodic endpoint */

    if ((USBHST_CONTROL_TRANSFER == pHCDPipe->uEndpointType) ||
        (USBHST_BULK_TRANSFER == pHCDPipe->uEndpointType))
        {
        /* To hold the pointer to the previous pipe data structure */

        pUSB_EHCD_PIPE pPrevPipe = NULL;

        /* To hold the pointer to the next pipe data structure */

        pUSB_EHCD_PIPE pNextPipe = pHCDPipe->pNext;

        /*
         * This is ASYNCH pipe thus it should be in a cirular
         * ASYNCH pipe list, so the next pipe will be non-NULL
         * and the QH should be valid.
         */

        if ((pNextPipe == NULL) || (pNextPipe->pQH == NULL))
            {
            USB_EHCD_ERR("usbEhcdDeletePipe - invalid next pipe %p QH %p\n",
                pNextPipe,
                (pNextPipe == NULL) ? NULL : pNextPipe->pQH, 0, 0, 0, 0);

            return USBHST_INVALID_PARAMETER;
            }

        /*
         * We can not use USB_EHCD_REMOVE_ASYNCH_PIPE() directly here becasue
         * we need to find the previous pipe of the removing pipe to remove
         * the pipe from hardware schedule properly.
         */

        /*
         * If the pipe removal is done in the context of URB completion
         * processing task, we should delay the pipe list modification
         * until the URB completion processing is done. Otherwise, we can
         * take the ActivePipeListSynchEventID and remove the pipe.
         */

        if ((TASK_ID)taskIdCurrent == pHCDData->IntHandlerThreadID)
            {
            USB_EHCD_DBG(
                "usbEhcdDeletePipe - happened in IntHandlerThreadID context\n",
                0, 0, 0, 0, 0, 0);

            /* Search the asychronous list and update the next pointers */

            /*
             * If the pipe deletion is called in the URB completion processing
             * task context (through the URB callback), then the lock which
             * protects the active pipe lists (ActivePipeListSynchEventID) has
             * already been taken, thus we are safe to iterate the active asynch
             * pipe list to find the previous pipe of this pipe (to be removed).
             *
             * However, we should not alter the asynch pipe list in this
             * condition, becasue we need to keep the integrity of the active
             * pipe lists while in the URB completion processing, so that
             * no dereferring to removed pipe condition happen (to overcome
             * potential crash of the HCD).
             */

            for (pPrevPipe = pHCDData->pDefaultPipe;
                 (pPrevPipe->pNext != pHCDPipe) &&
                 (pHCDData->pDefaultPipe != pPrevPipe->pNext);
                 pPrevPipe = pPrevPipe->pNext);

            /*
             * The loop above can end in two conditions:
             *
             * (pPrevPipe->pNext == pHCDPipe)
             *
             * --> found a previous pipe
             *
             * or
             *
             * (pHCDData->pDefaultPipe == pPrevPipe->pNext)
             *
             * --> didn't find a previous pipe
             *
             * So, if the loop above comes out in the 2nd case,
             * we are passed an invalid pipe handle, thus return!
             */

            if (pHCDData->pDefaultPipe == pPrevPipe->pNext)
                {
                USB_EHCD_ERR("usbEhcdDeletePipe - didn't find a pPrevPipe 1\n",
                    0, 0, 0, 0, 0, 0);

                return USBHST_INVALID_PARAMETER;
                }

            /* Do early pipe cancel */

            usbEhcdPipeReturnAll(pHCDPipe);
            }
        else
            {
            /*
             * If we are not in the URB completion processing task context,
             * then we have to take the active pipe protection lock
             * (ActivePipeListSynchEventID) to make sure no intermediate
             * modification is done while we are iterating the list.
             */

            /* Exclusively access the list */

            OS_WAIT_FOR_EVENT(pHCDData->ActivePipeListSynchEventID,
                OS_WAIT_INFINITE);

            /* Search the asychronous list and update the next pointers */

            for (pPrevPipe = pHCDData->pDefaultPipe;
                 (pHCDPipe != pPrevPipe->pNext) &&
                 (pHCDData->pDefaultPipe != pPrevPipe->pNext);
                 pPrevPipe = pPrevPipe->pNext);

            /*
             * Deletion of the default pipe is not through this function. So
             * the pHCDPipe can never be the same as pHCDData->pDefaultPipe
             */

            /*
             * The loop above can end in two conditions:
             *
             * (pPrevPipe->pNext == pHCDPipe)
             *
             * --> found a previous pipe
             *
             * or
             *
             * (pHCDData->pDefaultPipe == pPrevPipe->pNext)
             *
             * --> didn't find a previous pipe
             *
             * So, if the loop above comes out in the 2nd case,
             * we are passed an invalid pipe handle, thus return!
             */

            if (pHCDData->pDefaultPipe == pPrevPipe->pNext)
                {
                USB_EHCD_ERR("usbEhcdDeletePipe - didn't find a pPrevPipe 2\n",
                    0, 0, 0, 0, 0, 0);

                /* Release the exclusive access */

                OS_RELEASE_EVENT(pHCDData->ActivePipeListSynchEventID);

                return USBHST_INVALID_PARAMETER;
                }

            /* Update the HCD maintained link pointers */

            pPrevPipe->pNext = pHCDPipe->pNext;

            /* If this pipe forms the tail, update the tail element */

            if (pHCDData->pAsynchTailPipe == pHCDPipe)
                {
                pHCDData->pAsynchTailPipe = pPrevPipe;
                }

            /* Break from the active pipe list */

            pHCDPipe->pNext = NULL;

            /* Release the exclusive access */

            OS_RELEASE_EVENT(pHCDData->ActivePipeListSynchEventID);

            /* Do early pipe cancel */

            usbEhcdPipeReturnAll(pHCDPipe);
            }

        /*
         * A task can submit an asynchronous request while a pipe is being
         * deleted. An asynchronous submit request enables the asynch schedule.
         * So, it is important to protect the following code
         * with the asych schedule mutex.
         */

        semTake(pHCDData->AsychQueueMutex, WAIT_FOREVER);

        /*
         * Don't clear the ASYNCH_SCHEDULE_ENABLE when HC is halted
         * to avoid the dead loop waiting for the bit to be cleared.
         */

        if (USB_EHCD_GET_FIELD(pHCDData, USBSTS, HCHALTED) == 0)
            {

            USB_EHCD_DBG("usbEhcdDeletePipe - Clr ASYNCH_SCHEDULE_ENABLE\n",
                0, 0, 0, 0, 0, 0);

            /* Disable the asynchronous schedule */

            usbEhcdAsynchScheduleDisable(pHCDData);
            }

        /*
         * Update the HC maintained next pointers. This has to be done after
         * disabling the list. If the host controller is accessing the nextTD
         * element and when the software also updates it, it will lead to errors.
         * This is a problem only with asynchronous QHs
         */

        pQhPtrLow32 = USB_EHCD_DESC_LO32(pNextPipe->pQH);

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              QH,
                              pPrevPipe->pQH->uQueueHeadHorizontalLinkPointer,
                              (pQhPtrLow32 >> 5),
                              HORIZONTAL_LINK_POINTER);

        pQhPtrLow32 = USB_EHCD_DESC_LO32(pHCDPipe->pQH);

        /*
         * If the host controller is accessing currently the removed QH,
         * update the host controller's current accessible QH
         */

        if (USB_EHCD_GET_FIELD(pHCDData,
                       ASYNCLISTADDR,
                       LINK_PTR_LOW) ==
            (pQhPtrLow32 >> USB_EHCD_ASYNCHLISTADDR_LINK_PTR_LOW))
            {
            pQhPtrLow32 = USB_EHCD_DESC_LO32(pNextPipe->pQH);

            USB_EHCD_SET_FIELD(pHCDData,
                       ASYNCLISTADDR,
                       LINK_PTR_LOW,
                       (pQhPtrLow32 >> USB_EHCD_ASYNCHLISTADDR_LINK_PTR_LOW));
            }

        USB_EHCD_VDBG("usbEhcdDeletePipe - Set ASYNCH_SCHEDULE_ENABLE\n",
            0, 0, 0, 0, 0, 0);

        /* Enable the asynchronous schedule */

        usbEhcdAsynchScheduleEnable(pHCDData);
        semGive(pHCDData->AsychQueueMutex);

        /*
         * Add to the delayed removal pipe list - from here we will not
         * use any of the pipe resources for any operation, other than
         * the clean up code to delete the resources used by the pipe.
         */

        USB_EHCD_ADD_TO_DELAYED_PIPE_REMOVAL_LIST(pHCDData, pHCDPipe);

        if (FALSE == usrUsbEhciPollingEnabled())
            {
            /* Enable the aynch advance interrupt */

            USB_EHCD_SET_BIT_USBINTR_INT_ON_ASYNC_ADVANCE_DOORBELL(pHCDData);

            }

        /*
         * Enable an interrupt to be generated
         * when the Asynchronous list is advanced.
         */

        USB_EHCD_SET_BIT_USBCMD_INT_ON_ASYNC_ADVANCE_DOORBELL(pHCDData);

        /*
         * Don't disable the asynchronous schedule here, we will do it
         * when the INT_ON_ASYNC_ADVANCE_DOORBELL interrupt is triggered
         * and the pipe clean up routine is called. Because the
         * ASYNC_ADVANCE_DOORBELL interrupt will not be triggered if the
         * ASYNC schedule is disabled.
         */

        }
    /* It is a periodic endpoint, */

    else
        {
        /* To hold the bandwidth reservation status */

        BOOLEAN bStatus = FALSE;

        /* To hold the microframe index */

        UINT8 uUFrameIndex = 0;

        /* Check if it is an interrupt endpoint */

        if (USBHST_INTERRUPT_TRANSFER == pHCDPipe->uEndpointType)
            {
            /* Pointer to the tail of the list */

            pUSB_EHCD_QH pTempQH = NULL;

            /* Pointer to the tail of the list */

            pUSB_EHCD_QH pTailQH = NULL;

            /* Exclusively access the list */

            OS_WAIT_FOR_EVENT(pHCDData->RequestSynchEventID, OS_WAIT_INFINITE);

            /* Copy the first element of the list */

            pTempQH = (pUSB_EHCD_QH)
                    pHCDData->TreeListData[pHCDPipe->uListIndex].pHeadPointer;

            pTailQH = (pUSB_EHCD_QH)
                    pHCDData->TreeListData[pHCDPipe->uListIndex].pTailPointer;

            /* This loop searches for the QH which needs to be removed */

            while ((pTempQH != pTailQH) && (NULL != pTempQH))
                {
                /*
                 * If the next element is the same QH
                 * which is being removed, break from the loop.
                 */

                if (pTempQH->pNext == pHCDPipe->pQH)
                    break;

                /* Update the temp pointer */

                pTempQH = pTempQH->pNext;
                }

            /*
             * Check if the loop has exited as the QH is not found
             * Note that 'pTempQH' points to the previous QH always
             */

            if ((pTempQH == pTailQH) || (NULL == pTempQH))
                {
                USB_EHCD_ERR("usbEhcdDeletePipe - QH is not found\n",
                    0, 0, 0, 0, 0, 0);

                /* Release the resource access */

                OS_RELEASE_EVENT(pHCDData->RequestSynchEventID);

                return USBHST_FAILURE;
                }

            /* Update the next pointers */

            pTempQH->pNext = pHCDPipe->pQH->pNext;

            /* If the pointer is the tail, update the tail pointer */

            if (pTailQH == pHCDPipe->pQH)
                {
                pHCDData->TreeListData[pHCDPipe->uListIndex].pTailPointer =
                    pTempQH;
                }

            /* Copy the t bits */

            USB_EHCD_SET_BITFIELD(uBusIndex,
                    QH,
                    pTempQH->uQueueHeadHorizontalLinkPointer,
                    USB_EHCD_GET_BITFIELD(uBusIndex,
                               QH,
                               pHCDPipe->pQH->uQueueHeadHorizontalLinkPointer,
                               HORIZONTAL_LINK_POINTER_T),
                    HORIZONTAL_LINK_POINTER_T);

            /*
             * If the QH's next pointer is valid, update the previous head's
             * next pointer
             */

            if (NULL != pHCDPipe->pQH->pNext)
                {
                /* Get the low 32 bit of the BUS address of the next QH */

                pQhPtrLow32 = USB_EHCD_DESC_LO32(pHCDPipe->pQH->pNext);

                USB_EHCD_SET_BITFIELD(uBusIndex,
                                   QH,
                                   pTempQH->uQueueHeadHorizontalLinkPointer,
                                   (pQhPtrLow32 >> 5),
                                   HORIZONTAL_LINK_POINTER);
                }
            /*
             * The next pointer is not valid, update the next link pointer
             * to point to an invalid link address
             */

            else
                {
                USB_EHCD_SET_BITFIELD(uBusIndex,
                                      QH,
                                      pTempQH->uQueueHeadHorizontalLinkPointer,
                                      0,
                                      HORIZONTAL_LINK_POINTER);
                }

            /* Release the resource access */

            OS_RELEASE_EVENT(pHCDData->RequestSynchEventID);

            /* Exclusively access the bandwidth resource */

            OS_WAIT_FOR_EVENT(pHCDData->BandwidthEventID, OS_WAIT_INFINITE);

            /*
             * This loop will update the tree node bandwidth
             * for every microframe
             */

            for (uUFrameIndex = 0;
                 USB_EHCD_MAX_MICROFRAME_NUMBER > uUFrameIndex;
                 uUFrameIndex++)
                {
                /*
                 * Check if bandwidth is to be updated for this
                 * microframe and update the bandwidth
                 */

                if (0 != ((pHCDPipe->uUFrameMaskValue >> uUFrameIndex) & 0x01))
                    {
                    pHCDData->TreeListData[pHCDPipe->uListIndex].uBandwidth[uUFrameIndex]
                        -= (UINT32)pHCDPipe->uBandwidth;
                    }
                }

            /* Release the bandwidth exclusive access */

            OS_RELEASE_EVENT(pHCDData->BandwidthEventID);

            /* Update the bandwidth */

            bStatus = usbEhcdUpdateBandwidth(pHCDData);

            /* Check if bandwidth updation is successful */

            if (FALSE == bStatus)
                {
                USB_EHCD_ERR("usbEhcdDeletePipe - "
                    "upate bandwidth fail for interrupt pipe %p 1\n",
                    pHCDPipe, 2, 3, 4, 5 ,6);
                /*
                 * We have to continue, only may loose some bandwidth
                 * if it really fails
                 */
                }

            /* Do early pipe cancel */

            usbEhcdPipeReturnAll(pHCDPipe);

            /* Remove pipe from inter list maintained in the HCD data */

            /*
             * If the pipe removal is done in the context of URB completion
             * processing task, we should delay the pipe list modification
             * until the URB completion processing is done. Otherwise, we can
             * take the ActivePipeListSynchEventID and remove the pipe.
             */

            if ((TASK_ID)taskIdCurrent == pHCDData->IntHandlerThreadID)
                {
                /* Add to the delayed removal pipe list */

                USB_EHCD_ADD_TO_DELAYED_PIPE_REMOVAL_LIST(pHCDData, pHCDPipe);
                }
            else
                {
                /* Exclusively access the list */

                OS_WAIT_FOR_EVENT(pHCDData->ActivePipeListSynchEventID,
                    OS_WAIT_INFINITE);

                /* Remove from the active inter pipe list */

                USB_EHCD_REMOVE_INTER_PIPE(pHCDData, pHCDPipe);

                /* Release the exclusive access */

                OS_RELEASE_EVENT(pHCDData->ActivePipeListSynchEventID);

                /* Add to the reclamation list */

                USB_EHCD_ADD_TO_PERIODIC_RECLAMATION_LIST(pHCDData, pHCDPipe);
                }
            }/* End of if ((USBHST_INTERRUPT_TRANSFER ==  */

        /* Isochronous endpoint removal */

        else
            {
            /* To hold the pointer to the request information */

            pUSB_EHCD_REQUEST_INFO pRequestInfo = NULL;

            /* To hold the frame index */

            UINT16 uFrameIndex = 0;

            /* Exclusively access the pipe request list */

            OS_WAIT_FOR_EVENT(pHCDPipe->PipeSynchEventID, OS_WAIT_INFINITE);

            /* Extract the first request element */

            for ((pRequestInfo = pHCDPipe->pRequestQueueHead);
                 (NULL != pRequestInfo);
                 (pRequestInfo = pRequestInfo->pNext))
                 {
                 /* Unlink all the isoch TDs associated with this request */

                 USB_EHCD_UNLINK_ISOCH_REQ(pHCDData,
                                           pRequestInfo,
                                           pHCDPipe->uSpeed);
                 }

            /* Release the exclusive access */

            OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

            /* Exclusively access the bandwidth resource */

            OS_WAIT_FOR_EVENT(pHCDData->BandwidthEventID, OS_WAIT_INFINITE);

            /*
             * This loop will remove the bandwidth
             * allocated in each of the frames
             */

            for (uFrameIndex = pHCDPipe->uListIndex;
                 USB_EHCD_MAX_FRAMELIST_ENTIRES > uFrameIndex;
                 uFrameIndex++)
                {
                /*
                 * This loop will update the frame list bandwidth
                 * for every microframe
                 */

                for (uUFrameIndex = 0;
                     USB_EHCD_MAX_MICROFRAME_NUMBER > uUFrameIndex;
                     uUFrameIndex++)
                    {
                    /*
                     * Check if bandwidth is to be updated for this
                     * microframe and update the bandwidth
                     */

                    if (0 != ((pHCDPipe->uUFrameMaskValue >> uUFrameIndex) & 0x01))
                        {
                        pHCDData->FrameListData[uFrameIndex].uBandwidth[uUFrameIndex]
                            -= (UINT32)pHCDPipe->uBandwidth;

                        pHCDData->FrameBandwidth[uFrameIndex][uUFrameIndex]
                            -= (UINT32)pHCDPipe->uBandwidth;
                        }
                    }
                }

            /* Release the bandwidth exclusive access */

            OS_RELEASE_EVENT(pHCDData->BandwidthEventID);

            /* Update the bandwidth */

            bStatus = usbEhcdUpdateBandwidth(pHCDData);

            /* Check if bandwidth updation is successful */

            if (FALSE == bStatus)
                {
                USB_EHCD_ERR("usbEhcdDeletePipe - "
                    "upate bandwidth fail for interrupt pipe %p 2\n",
                    pHCDPipe, 2, 3, 4, 5, 6);
                /*
                 * We have to continue, only may loose some bandwidth
                 * if it really fails
                 */
                }

            /* Do early pipe cancel */

            usbEhcdPipeReturnAll(pHCDPipe);

            /* Remove pipe from isoch list maintained in the HCD data */

            /*
             * If the pipe removal is done in the context of URB completion
             * processing task, we should delay the pipe list modification
             * until the URB completion processing is done. Otherwise, we can
             * take the ActivePipeListSynchEventID and remove the pipe.
             */

            if ((TASK_ID)taskIdCurrent == pHCDData->IntHandlerThreadID)
                {
                /* Add to the delayed removal pipe list */

                USB_EHCD_ADD_TO_DELAYED_PIPE_REMOVAL_LIST(pHCDData, pHCDPipe);
                }
            else
                {
                /* Exclusively access the list */

                OS_WAIT_FOR_EVENT(pHCDData->ActivePipeListSynchEventID,
                    OS_WAIT_INFINITE);

                /* Remove from the active isoc pipe list */

                USB_EHCD_REMOVE_ISOCH_PIPE(pHCDData, pHCDPipe);

                /* Release the exclusive access */

                OS_RELEASE_EVENT(pHCDData->ActivePipeListSynchEventID);

                /* Add to the reclamation list */

                USB_EHCD_ADD_TO_PERIODIC_RECLAMATION_LIST(pHCDData, pHCDPipe);
                }
            }/* End of isochronous endpoint removal request handling */

        if (FALSE == usrUsbEhciPollingEnabled())
            {
            /* Enable the interrupt to be generated on a frame list rollover */

            USB_EHCD_SET_BIT_USBINTR_INT_ON_FRAME_LIST_ROLLOVER(pHCDData);
            }
        else
            {
            /*
             * Give sufficient delay for the frame list to rollover. This is
             * the worst case condition where the frame list rolls over in
             * less than 1024 ms
             */

            OS_DELAY_MS(USB_EHCD_MAX_FRAMELIST_ENTIRES);
            }
        }/* End of else */

    /* Return the success status */

    return USBHST_SUCCESS;
    }/* End of function usbEhcdDeletePipe() */

/*******************************************************************************
*
* usbEhcdModifyDefaultPipe - modify the default pipe characteristics.
*
* This routine is used to modify the properties (device speed and maximum
* packet size) of the default pipe (address 0, endpoint 0). <uBusIndex>
* specifies the host controller bus index. <uDefaultPipeHandle> holds the
* pipe handle of the default pipe. <uDeviceSpeed> is the speed of the
* device which needs to be modified. <uMaxPacketSize> is the maximum packet
* size of the default pipe which needs to be modified. <uHighSpeedHubInfo>
* specifies the nearest high speed hub and the port number information. This
* information will be used to handle a split transfer to the full / low speed
* device. The high byte will hold the high speed hub address. The low byte
* will hold the port number to which the USB 1.1 device is connected.
*
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the deafult pipe properties were modified
*       successfully.
*   USBHST_INVALID_PARAMETER - Returned if the parameters are not valid.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbEhcdModifyDefaultPipe
    (
    UINT8   uBusIndex,          /* Host controller bus index               */
    ULONG   uDefaultPipeHandle, /* Handle to the default pipe              */
    UINT8   uDeviceSpeed,       /* Speed of the device in default state    */
    UINT8   uMaxPacketSize,     /* Maximum packet size of the default pipe */
    UINT16   uHighSpeedHubInfo  /* High speed hub info for USB 1.1 device  */
    )
    {
    /* To hold the pointer to the EHCD maintained pipe data structure */

    pUSB_EHCD_PIPE pHCDPipe = NULL;

    /* Pointer to the HCD specific data structure */

    pUSB_EHCD_DATA  pHCDData = NULL;

    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT(
        USB_EHCI_WV_TRANSFER,
        "usbEhcdModifyDefaultPipe() starts",
        USB_EHCD_WV_FILTER);

    /* Check the validity of the parameters */

    if ((USB_MAX_EHCI_COUNT <= uBusIndex) ||
        (0 == uDefaultPipeHandle) ||
        (USBHST_HIGH_SPEED != uDeviceSpeed &&
         USBHST_FULL_SPEED != uDeviceSpeed &&
         USBHST_LOW_SPEED != uDeviceSpeed))
        {
        USB_EHCD_ERR("usbEhcdModifyDefaultPipe - parameters not valid\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the global data structure */

    pHCDData = g_pEHCDData[uBusIndex];

    /* Assert if the global pointer is not valid */

    /* Assert if the global pointer is not valid */

    if (NULL == pHCDData)
        {
        USB_EHCD_ERR("usbEhcdSubmitURB - pHCDData is not valid\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Check if this request is for the Root hub */

    if (0 == pHCDData->RHData.uDeviceAddress)
        {
        return USBHST_SUCCESS;
        }

    /* WARNING : This function will change on split support */

    /*
     * With embedded TT, the QH hub address field for direct attached devices
     * should be 0; However, the high byte of uHighSpeedHubInfo is set to the
     * address of the root hub by the usbd, which is typically 1; so if we
     * enable embdded TT support, and when the QH is for the direct attached
     * devices(whose hub address is set to the root hub address), we simply
     * mask the high byte of uHighSpeedHubInfo to make the hub address field
     * of the QH set to 0;
     */

    if ((pHCDData->hasEmbeddedTT == TRUE) &&
        (((uHighSpeedHubInfo & USB_EHCD_PARENT_HUB_ADDRESS_MASK)>> 8)
        == pHCDData->RHData.uDeviceAddress))
        {
        uHighSpeedHubInfo =
            (UINT16)(uHighSpeedHubInfo & (~USB_EHCD_PARENT_HUB_ADDRESS_MASK));
        /*
         * Check if we are required to workaround the USB errata #14
         * on some old MPC834x targets ; The errata is described as :
         * Port number in the Queue head is 0 to N-1. It should be 1 to
         * N according to EHCI spec. This bug only affects the host mode.
         *
         * The workaround for this is :
         * Use the port number 0 to N -1 instead of 1 to N.
         */

        if (pHCDData->fixupPortNumber == TRUE)
            {
            uHighSpeedHubInfo = (UINT16)(uHighSpeedHubInfo - 1);
            }
        }

    /* Extract the default pipe handle */

    pHCDPipe = (pUSB_EHCD_PIPE)uDefaultPipeHandle;

    /* Modify the default pipe contents */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          QH,
                          pHCDPipe->pQH->uEndPointCharacteristics,
                          uDeviceSpeed,
                          ENDPOINT_CHARACTERISTICS_ENDPT_SPEED );

    /* pHCDPipe->pQH->dword2.mult = 1; */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                         QH,
                         pHCDPipe->pQH->uEndPointCapabilities,
                         1,
                         ENDPOINT_CAPABILITIES_MULT  );

    /* Update the hub address and port number for the USB 1.1 device */

    if (uDeviceSpeed != USBHST_HIGH_SPEED)
        {
        USB_EHCD_SET_BITFIELD(uBusIndex,
                              QH,
                              pHCDPipe->pQH->uEndPointCapabilities,
                              ((uHighSpeedHubInfo &
                                    USB_EHCD_PARENT_HUB_ADDRESS_MASK)>> 8),
                              ENDPOINT_CAPABILITIES_HUB_ADDR );

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              QH,
                              pHCDPipe->pQH->uEndPointCapabilities,
                              (uHighSpeedHubInfo &
                                        USB_EHCD_HUB_PORT_NUMBER_MASK),
                               ENDPOINT_CAPABILITIES_PORT_NUMBER);
        }
    else
        {

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              QH,
                              pHCDPipe->pQH->uEndPointCapabilities,
                              0,
                              ENDPOINT_CAPABILITIES_HUB_ADDR );

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              QH,
                              pHCDPipe->pQH->uEndPointCapabilities,
                              0,
                              ENDPOINT_CAPABILITIES_PORT_NUMBER);
        }

    /*
     * This field is to be set if it is not a high speed device and the
     * endpoint is a control endpoint
     */

    if (USBHST_HIGH_SPEED != uDeviceSpeed)
        {
        USB_EHCD_SET_BITFIELD(uBusIndex,
                              QH,
                              pHCDPipe->pQH->uEndPointCharacteristics,
                              1 ,
                              ENDPOINT_CHARACTERISTICS_CONTROL_ENDPOINT_FLAG);

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              QH,
                              pHCDPipe->pQH->uEndPointCharacteristics,
                              0,
                              ENDPOINT_CHARACTERISTICS_RL);
        }
    else
        {
        USB_EHCD_SET_BITFIELD(uBusIndex,
                              QH,
                              pHCDPipe->pQH->uEndPointCharacteristics,
                              USB_EHCD_MAX_NAK_RATE,
                              ENDPOINT_CHARACTERISTICS_RL);

        }

    /* Modify the maximum packet size */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          QH,
                          pHCDPipe->pQH->uEndPointCharacteristics,
                          uMaxPacketSize,
                          ENDPOINT_CHARACTERISTICS_MAXIMUM_PACKET_LENGTH);

    return USBHST_SUCCESS;
    }/* End of usbEhcdModifyDefaultPipe() */

/*******************************************************************************
*
* usbEhcdIsBandwidthAvailable - check the bandwidth availability
*
* This routine is used to check whether there is enough bandwidth to support
* the new configuration or an alternate interface setting of an interface.
*
* <uBusIndex> specifies the host controller bus index.
* <uDeviceAddress> specifies the device address. <uDeviceSpeed> is the speed
* of the device which needs to be modified. <pCurrentDescriptor> is the pointer
* to the current configuration or interface descriptor. If the pNewDescriptor
* corresponds to a USB configuration descriptor, this parameter is ignored
* (i.e. this parameter can be NULL). <pNewDescriptor> is the pointer to the new
* configuration or interface descriptor.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the new configuration or
*       new alternate interface can be supported (ALWAYS)
*   USBHST_INVALID_PARAMETER - Returned if the parameters are
*       not valid.
*   USBHST_INSUFFICIENT_BANDWIDTH - Returned if bandwidth is
*       not available.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbEhcdIsBandwidthAvailable
    (
    UINT8   uBusIndex,            /* Host controller bus index */
    UINT8   uDeviceAddress,       /* Handle to the device addr */
    UINT8   uDeviceSpeed,         /* Speed of the device in default state */
    UCHAR * pCurrentDescriptor,   /* Ptr to current configuration */
    UCHAR * pNewDescriptor        /* Ptr to new configuration */
    )
    {
    /* To hold the pointer to the descriptor header */

    pUSBHST_DESCRIPTOR_HEADER   pDescHeader = NULL;

    /* To hold the status of the request */

    USBHST_STATUS   Status = USBHST_FAILURE;

    /* To hold the bandwidth availability status */

    BOOLEAN bIsBwAvailable = FALSE;

    /* Pointer to the HCD specific data structure */

    pUSB_EHCD_DATA  pHCDData = NULL;

    /* Array to hold the total bandwidth for each Frame */

    PUINT32 FrameBW;

    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT(
        USB_EHCI_WV_TRANSFER,
        "usbEhcdIsBandwidthAvailable() starts",
        USB_EHCD_WV_FILTER);

    /* Check the validity of the parameters */

    if (USB_MAX_EHCI_COUNT <= uBusIndex ||
        0 == uDeviceAddress ||
        NULL == pNewDescriptor)
        {
        USB_EHCD_ERR("Parameters are not valid\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the global data structure */

    pHCDData = g_pEHCDData[uBusIndex];

    /* Assert if the global pointer is not valid */

    if (NULL == pHCDData)
        {
        USB_EHCD_ERR("usbEhcdSubmitURB - pHCDData is not valid\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* If this request is for the Root hub, return success */

    if (uDeviceAddress == pHCDData->RHData.uDeviceAddress)
        {
        return USBHST_SUCCESS;
        }

    FrameBW = OS_MALLOC(sizeof(UINT32) * USB_EHCD_MAX_FRAMELIST_ENTIRES);

    if (NULL == FrameBW)
        {
        USB_EHCD_ERR("usbEhcdIsBandwidthAvailable - "
            "Memory not allocated for the bandwidth\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_MEMORY_NOT_ALLOCATED;
        }

    /* Initialize the element of array to zero */

    OS_MEMSET(FrameBW, 0, sizeof(UINT32) * USB_EHCD_MAX_FRAMELIST_ENTIRES);

    /* Extract the descriptor header of the new descriptor */

    pDescHeader = (pUSBHST_DESCRIPTOR_HEADER)pNewDescriptor;

    /* Check if it is an interface descriptor */

    if (USBHST_INTERFACE_DESC == pDescHeader->uDescriptorType)
        {
        /* To hold the pointer to the current interface descriptor */

        pUSBHST_INTERFACE_DESCRIPTOR pCurrentInterfaceDesc = NULL;

        /* To hold the pointer to the new interface descriptor */

        pUSBHST_INTERFACE_DESCRIPTOR pNewInterfaceDesc = NULL;

        /* Get the current interface descriptor */

        pCurrentInterfaceDesc =
        (pUSBHST_INTERFACE_DESCRIPTOR)pCurrentDescriptor;

        /* Get the new interface descriptor */

        pNewInterfaceDesc =
        (pUSBHST_INTERFACE_DESCRIPTOR)pNewDescriptor;

        /*
         * Call the function to subtract the bandwidth used
         * by current interface
         */

        Status = usbEhcdSubBandwidth(pHCDData,
                    uDeviceAddress,
                    uDeviceSpeed,
                    pCurrentInterfaceDesc,
                    FrameBW);

        if (USBHST_INSUFFICIENT_MEMORY == Status)
            {
            USB_EHCD_ERR("Memory is not available 1\n",
                0, 0, 0, 0, 0, 0);

            OS_FREE(FrameBW);

            return USBHST_INSUFFICIENT_MEMORY;
            }


        /* Check the bandwidth for new interface */

        bIsBwAvailable =  usbEhcdAddBandwidth(uDeviceSpeed,
                                pNewInterfaceDesc,
                                FrameBW);

        if (FALSE == bIsBwAvailable)
            {
            USB_EHCD_ERR("Bandwidth is not available 2\n",
                0, 0, 0, 0, 0, 0);

            OS_FREE(FrameBW);

            return USBHST_INSUFFICIENT_BANDWIDTH;
            }

        }  /* End of interface descriptor */

    /* Check if it is a configuration descriptor */

    else if (USBHST_CONFIG_DESC == pDescHeader->uDescriptorType)
        {
        /* To hold the pointer to the current configuration descriptor */

        pUSBHST_CONFIG_DESCRIPTOR pCurrentConfigDesc = NULL;

        /* To hold the pointer to the new configuration descriptor */

        pUSBHST_CONFIG_DESCRIPTOR pNewConfigDesc = NULL;

        /* To hold the pointer to the interface descriptor */

        pUSBHST_INTERFACE_DESCRIPTOR pInterfaceDesc = NULL;

        /* To hold the total no of interface present in the configuration */

        UINT8 uInterfaceCount = 0, uCount;

        /* Get the current configuration descriptor */

        pCurrentConfigDesc =
        (pUSBHST_CONFIG_DESCRIPTOR)pCurrentDescriptor;

        /* Get the new configuration descriptor */

        pNewConfigDesc =
        (pUSBHST_CONFIG_DESCRIPTOR)pNewDescriptor;


        /* Subtract bandwidth used by this device */

        Status = usbEhcdSubDeviceBandwidth(pHCDData, uDeviceAddress, FrameBW);

        if (USBHST_INSUFFICIENT_MEMORY == Status)
            {
            USB_EHCD_ERR("Memory is not available 3\n",
                0, 0, 0, 0, 0, 0);

            OS_FREE(FrameBW);

            return USBHST_INSUFFICIENT_MEMORY;
            }

        /* Get the total no of endpoint for this interface */

        uInterfaceCount = pNewConfigDesc->bNumInterfaces;

        pInterfaceDesc = (pUSBHST_INTERFACE_DESCRIPTOR)
            ((UINT8 *)(pNewConfigDesc) + pNewConfigDesc->bLength);

        for (uCount=0; uCount < uInterfaceCount; uCount++)
            {

            while (USBHST_INTERFACE_DESC != pInterfaceDesc->bDescriptorType)
                {
                pInterfaceDesc
                = (pUSBHST_INTERFACE_DESCRIPTOR)((UINT8 *)(pInterfaceDesc)
                                                  + pInterfaceDesc->bLength);
                }

            if (0 == pInterfaceDesc->bAlternateSetting)
                {

                /* Check the bandwidth for this interface */

                bIsBwAvailable =  usbEhcdAddBandwidth(uDeviceSpeed,
                                        pInterfaceDesc,
                                        FrameBW);

                if (FALSE == bIsBwAvailable)
                    {
                    USB_EHCD_ERR("Bandwidth is not available 4\n",
                        0, 0, 0, 0, 0, 0);

                    OS_FREE(FrameBW);

                    return USBHST_INSUFFICIENT_BANDWIDTH;
                    }
                }

            pInterfaceDesc
                = (pUSBHST_INTERFACE_DESCRIPTOR)((UINT8 *)(pInterfaceDesc)
                                              + pInterfaceDesc->bLength);

            } /* End of for () */

        }  /* End of configuration descriptor */

    /*
     * If it is not an interface or configuration descriptor,
     * then it is an error
     */

    else
        {
        USB_EHCD_ERR("Descriptor type is not valid\n",
            0, 0, 0, 0, 0, 0);

        OS_FREE(FrameBW);

        return USBHST_INVALID_PARAMETER;
        }

    OS_FREE(FrameBW);

    /* Return success */

    return USBHST_SUCCESS;
    }/* End of usbEhcdIsBandwidthAvailable() */


/*******************************************************************************
*
* usbEhcdResetPipe - reset a pipe created already
*
* This routine is used to reset a pipe specific to an endpoint, which cancels
* all the pending requests on the pipe and force the data toggle to return
* DATA0 in the next transfer.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the pipe was reset successfully
*   USBHST_INVALID_PARAMETER - Returned if the parameters are not valid.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbEhcdResetPipe
    (
    pUSB_EHCD_DATA  pHCDData,
    pUSB_EHCD_PIPE  pHCDPipe
    )
    {
    /* Temporary queue head pointer */

    UINT32          pQhPtrLow32;

    UINT32          uBusIndex = pHCDData->uBusIndex;

    /* Check if the request is for the Root hub and route it */

    if ((pHCDData->RHData.pInterruptPipe == pHCDPipe) ||
        (pHCDData->RHData.pControlPipe == pHCDPipe))
        {
        USB_EHCD_ERR("usbEhcdResetPipe - reset root hub pipe\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_SUCCESS;
        }

    /* Check if the endpoint type is valid */

    if (USBHST_ISOCHRONOUS_TRANSFER == pHCDPipe->uEndpointType)
        {
        USB_EHCD_ERR("usbEhcdResetPipe - uEndpointType %d not valid\n",
            pHCDPipe->uEndpointType, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Check if it is a non-periodic endpoint */

    if ((USBHST_CONTROL_TRANSFER == pHCDPipe->uEndpointType) ||
        (USBHST_BULK_TRANSFER == pHCDPipe->uEndpointType))
        {
        /* To hold the pointer to the next pipe data structure */

        pUSB_EHCD_PIPE pNextPipe = pHCDPipe->pNext;

        /*
         * This is ASYNCH pipe thus it should be in a cirular
         * ASYNCH pipe list, so the next pipe will be non-NULL
         * and the QH should be valid.
         */

        if ((pNextPipe == NULL) || (pNextPipe->pQH == NULL))
            {
            USB_EHCD_ERR(
                "usbEhcdResetPipe - invalid next pipe %p QH %p\n",
                pNextPipe,
                (pNextPipe == NULL) ? NULL : pNextPipe->pQH, 0, 0, 0, 0);

            return USBHST_INVALID_PARAMETER;
            }

        /* Do early pipe cancel */

        usbEhcdPipeReturnAll(pHCDPipe);

        /* Now move all the requests to the cancel list */

        usbEhcdPipeRemoveAll(pHCDData, pHCDPipe);

        /* Set the QTD head as NULL */

        pHCDPipe->pQH->pQTD = NULL;

        /*
         * A task can submit an asynchronous request while a pipe is being
         * deleted. An asynchronous submit request enables the asynch schedule.
         * So, it is important to protect the following code
         * with the asych schedule mutex.
         */

        semTake(pHCDData->AsychQueueMutex, WAIT_FOREVER);

        USB_EHCD_VDBG("usbEhcdResetPipe - Clr ASYNCH_SCHEDULE_ENABLE\n",
            0, 0, 0, 0, 0, 0);

        /* Disable the asynchronous schedule */

        usbEhcdAsynchScheduleDisable(pHCDData);
        /* Get low 32 bits of the current QH BUS address */

        pQhPtrLow32 = USB_EHCD_DESC_LO32(pHCDPipe->pQH);

        /*
         * If the host controller is accessing currently the removed QH,
         * update the host controller's current accessible QH
         */

        if (USB_EHCD_GET_FIELD(pHCDData,
                       ASYNCLISTADDR,
                       LINK_PTR_LOW) ==
            (pQhPtrLow32 >> USB_EHCD_ASYNCHLISTADDR_LINK_PTR_LOW))
            {
            pQhPtrLow32 = USB_EHCD_DESC_LO32(pNextPipe->pQH);

            USB_EHCD_SET_FIELD(pHCDData,
                       ASYNCLISTADDR,
                       LINK_PTR_LOW,
                       (pQhPtrLow32 >> USB_EHCD_ASYNCHLISTADDR_LINK_PTR_LOW));
            }

        USB_EHCD_VDBG("usbEhcdResetPipe - Set ASYNCH_SCHEDULE_ENABLE\n",
            0, 0, 0, 0, 0, 0);

        /* Enable the asynchronous schedule */

        usbEhcdAsynchScheduleEnable(pHCDData);

        semGive(pHCDData->AsychQueueMutex);

        if (FALSE == usrUsbEhciPollingEnabled())
            {
            /* Enable the aynch advance interrupt */

            USB_EHCD_SET_BIT_USBINTR_INT_ON_ASYNC_ADVANCE_DOORBELL(pHCDData);
            }

        /*
         * Enable an interrupt to be generated
         * when the Asynchronous list is advanced.
         */

        USB_EHCD_SET_BIT_USBCMD_INT_ON_ASYNC_ADVANCE_DOORBELL(pHCDData);

        /*
         * Don't disable the asynchronous schedule here, we will do it
         * when the INT_ON_ASYNC_ADVANCE_DOORBELL interrupt is triggered
         * and the pipe clean up routine is called. Because the
         * ASYNC_ADVANCE_DOORBELL interrupt will not be triggered if the
         * ASYNC schedule is disabled.
         */

        }
    /* It is a periodic endpoint, */

    else
        {
        /* Do early pipe cancel */

        usbEhcdPipeReturnAll(pHCDPipe);

        /* Now move all the requests to the cancel list */

        usbEhcdPipeRemoveAll(pHCDData, pHCDPipe);

        /* Set the QTD head as NULL */

        pHCDPipe->pQH->pQTD = NULL;

        if (FALSE == usrUsbEhciPollingEnabled())
            {
            /* Enable the interrupt to be generated on a frame list rollover */

            USB_EHCD_SET_BIT_USBINTR_INT_ON_FRAME_LIST_ROLLOVER(pHCDData);
            }
        }/* End of else */

    /* Indicate that there are no elements in the list */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                         QH,
                         pHCDPipe->pQH->uNextQtdPointer,
                         USB_EHCD_INVALID_LINK,
                         NEXTQTD_POINTER_T) ;

    /* Update the HC's next pointer */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                         QH,
                         pHCDPipe->pQH->uNextQtdPointer,
                         (ULONG)NULL,
                         NEXTQTD_POINTER) ;

    /* Initialize all the other fields */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                         QH,
                         pHCDPipe->pQH->uCurrentQtdPointer,
                         0,
                         CURRENTQTD_POINTER);

    pHCDPipe->pQH->uAlternateNextQtdPointer = 0;

    /* Must reset the data toggle value */

    pHCDPipe->pQH->uTransferInfo = 0;

    pHCDPipe->pQH->uBufferPagePointerList[0] = 0;
    pHCDPipe->pQH->uBufferPagePointerList[1] = 0;
    pHCDPipe->pQH->uBufferPagePointerList[2] = 0;
    pHCDPipe->pQH->uBufferPagePointerList[3] = 0;
    pHCDPipe->pQH->uBufferPagePointerList[4] = 0;

    pHCDPipe->pQH->uExtBufferPointerPageList[0] = 0;
    pHCDPipe->pQH->uExtBufferPointerPageList[1] = 0;
    pHCDPipe->pQH->uExtBufferPointerPageList[2] = 0;
    pHCDPipe->pQH->uExtBufferPointerPageList[3] = 0;
    pHCDPipe->pQH->uExtBufferPointerPageList[4] = 0;

    /* Return the success status */

    return USBHST_SUCCESS;
    }/* End of function usbEhcdResetPipe() */

/*******************************************************************************
*
* usbEhcdPipeControl - control the pipe characteristics
*
* This routine is used to control the pipe characteristics.
*
* <uBusIndex> specifies the host controller bus index.
* <uPipeHandle> holds the pipe handle.
* <pPipeCtrl> is the pointer to the USBHST_PIPE_CONTROL_INFO holding
*             the request details.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the URB is submitted successfully.
*   USBHST_INVALID_PARAMETER - Returned if the parameters are not valid.
*   USBHST_INVALID_REQUEST - Returned if the request is not valid
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbEhcdPipeControl
    (
    UINT8  uBusIndex,                   /* Index of the host controller */
    ULONG  uPipeHandle,                 /* Pipe handle */
    USBHST_PIPE_CONTROL_INFO * pPipeCtrl /* Pointer to the pipe control info */
    )
    {
    /* Pointer to the HCD maintained pipe */

    pUSB_EHCD_PIPE pHCDPipe = NULL;

    /* Pointer to the HCD specific data structure */

    pUSB_EHCD_DATA  pHCDData = NULL;

    /* The control result */

    USBHST_STATUS   hstSts = USBHST_FAILURE;

    /* Check the validity of the parameters */

    if ((USB_MAX_EHCI_COUNT <= uBusIndex) ||
        (0 == uPipeHandle) ||
        (NULL == pPipeCtrl))
        {
        USB_EHCD_WARN("usbEhcdPipeControl - parameters are not valid\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the global data structure */

    pHCDData = g_pEHCDData[uBusIndex];

    /* Return if the global pointer is not valid */

    if (NULL == pHCDData)
        {
        USB_EHCD_ERR("usbEhcdPipeControl - pHCDData is NULL\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the USB_EHCD_PIPE data structure */

    pHCDPipe = (pUSB_EHCD_PIPE)uPipeHandle;

    /* Do the pipe control based on the control code selection */

    switch (pPipeCtrl->uControlCode)
        {
        case USBHST_CMD_PIPE_RESET:
            {
            /*
             * Do pipe reset so that the pipe will be in DATA0 state
             * in next transfer
             */

            hstSts = usbEhcdResetPipe(pHCDData, pHCDPipe);
            }

            break;

        case USBHST_CMD_TRANSFER_SETUP:
            {
            pUSB_TRANSFER_SETUP_INFO pSetupInfo;

            /* Root hub doesn't need any real DMA transfer */

            if ((pHCDData->RHData.pInterruptPipe == pHCDPipe) ||
                (pHCDData->RHData.pControlPipe == pHCDPipe))
                {
                USB_EHCD_DBG("usbEhcdPipeControl - setup root hub pipe\n",
                    0, 0, 0, 0, 0, 0);

                return USBHST_SUCCESS;
                }

            /* For non-root hub device the setup should really happen */

            pSetupInfo = (pUSB_TRANSFER_SETUP_INFO)
                          pPipeCtrl->uControlParam;

            if (pSetupInfo == NULL)
                {
                USB_EHCD_ERR("usbEhcdPipeControl - "
                    "USBHST_CMD_TRANSFER_SETUP pSetupInfo NULL\n",
                    0, 0, 0, 0, 0, 0);

                return USBHST_INVALID_PARAMETER;
                }

            USB_EHCD_DBG(
                "usbEhcdPipeControl - USBHST_CMD_TRANSFER_SETUP for EP %p:\n"
                "uMaxTransferSize %p uMaxNumReqests %d uFlags %p\n",
                pHCDPipe->uEndpointAddress,
                pSetupInfo->uMaxTransferSize,
                pSetupInfo->uMaxNumReqests,
                pSetupInfo->uFlags, 0, 0);


            /* Exclusively access the pipe request list */

            OS_WAIT_FOR_EVENT(pHCDPipe->PipeSynchEventID, OS_WAIT_INFINITE);

            /*
             * Set up the transfer characteristics such as
             * creating proper DMA TAG and DMA MAPs
             */

            hstSts = usbEhcdSetupPipe(pHCDData, pHCDPipe, pSetupInfo);

            /* Release the exclusive access */

            OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);
            }

            break;

        default :

            break;
        }

    return hstSts;
    }

/*******************************************************************************
*
* usbEhcdQueueQTDs - link the new QTDs onto the QH transfer queue
*
* This routine is used for common part of the URB submitting process for
* non-isoch URB which requires linking the new QTDs onto the QH transfer queue.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: USBHST_SUCCESS
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbEhcdQueueQTDs
    (
    pUSB_EHCD_DATA          pHCDData,
    pUSB_EHCD_PIPE          pHCDPipe,
    pUSB_EHCD_REQUEST_INFO  pRequest,
    pUSBHST_URB             pURB
    )
    {
    /* To hold the HCD bus index */

    UINT8                   uBusIndex;

    /* QTD for this control pipe */

    pUSB_EHCD_QTD           pQTD;

    /* Temporay pointer to hold the QTD next pointer */

    UINT32                  uNextQTDPointer;

    /* Get the HCD bus index */

    uBusIndex = (UINT8)pHCDData->uBusIndex;

    /*
     * Here we check if it is a control or bulk transfer request.
     * if it is a control or bulk transfer request, increment the
     * outstanding asynchronous request count and enable the
     * asynchronous schedule
     */

    if (USBHST_INTERRUPT_TRANSFER != pHCDPipe->uEndpointType)
        {
        semTake (pHCDData->AsychQueueMutex, WAIT_FOREVER);

        pHCDData->noOfAsynchTransfers++;

        USB_EHCD_VDBG("usbEhcdQueueQTDs - Set ASYNCH_SCHEDULE_ENABLE\n",
            0, 0, 0, 0, 0, 0);

        /* Enable the asynchronous schedule */

        USB_EHCD_SET_BIT(pHCDData,
             USBCMD,
             ASYNCH_SCHEDULE_ENABLE);

        semGive (pHCDData->AsychQueueMutex);
        }

    /* Check if there are TDs linked to the QH */

    if (NULL == pHCDPipe->pQH->pQTD)
        {
        /* Indicate that this is not the last element in the list */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              QH,
                              pHCDPipe->pQH->uNextQtdPointer,
                              USB_EHCD_INVALID_LINK,
                              NEXTQTD_POINTER_T);

        /* Add the head of the request as the first TD in the QH */

        pHCDPipe->pQH->pQTD = (pUSB_EHCD_QTD)pRequest->pHead;

        /* Link the TD to the QH if its not halted */

        if (!pHCDPipe->bIsHalted)
            {

            /* Initialize all the other fields */

            USB_EHCD_SET_BITFIELD(uBusIndex,
                                  QH,
                                  pHCDPipe->pQH->uCurrentQtdPointer,
                                  0,
                                  CURRENTQTD_POINTER);

            pHCDPipe->pQH->uAlternateNextQtdPointer =
                       USB_EHCD_SWAP_DESC_DATA(uBusIndex,
                                          USB_EHCD_INVALID_LINK);

            /* Do not modify the toggle value */
            /* Do not modify the ping state for High-speed bulk and control endpoints */

            if ((USBHST_HIGH_SPEED == pHCDPipe->uSpeed) &&
                ((USBHST_BULK_TRANSFER == pHCDPipe->uEndpointType) ||
                 (USBHST_CONTROL_TRANSFER == pHCDPipe->uEndpointType)))
                {
                pHCDPipe->pQH->uTransferInfo = pHCDPipe->pQH->uTransferInfo &
                    USB_EHCD_SWAP_DESC_DATA(uBusIndex,
                                            USB_EHCD_QH_TRANSFERINFO_DT_MASK |
                                            USB_EHCD_QH_TRANSFERINFO_PINGSTATE_MASK);
                }
            else
                {
                pHCDPipe->pQH->uTransferInfo = pHCDPipe->pQH->uTransferInfo &
                    USB_EHCD_SWAP_DESC_DATA(uBusIndex,
                                            USB_EHCD_QH_TRANSFERINFO_DT_MASK);
                }
 
            pHCDPipe->pQH->uBufferPagePointerList[0] = 0;
            pHCDPipe->pQH->uBufferPagePointerList[1] = 0;
            pHCDPipe->pQH->uBufferPagePointerList[2] = 0;
            pHCDPipe->pQH->uBufferPagePointerList[3] = 0;
            pHCDPipe->pQH->uBufferPagePointerList[4] = 0;

            pHCDPipe->pQH->uExtBufferPointerPageList[0] = 0;
            pHCDPipe->pQH->uExtBufferPointerPageList[1] = 0;
            pHCDPipe->pQH->uExtBufferPointerPageList[2] = 0;
            pHCDPipe->pQH->uExtBufferPointerPageList[3] = 0;
            pHCDPipe->pQH->uExtBufferPointerPageList[4] = 0;

            pQTD = (pUSB_EHCD_QTD) pRequest->pHead;

            /* Get the lower 32 bit of the BUS address */

            uNextQTDPointer = USB_EHCD_DESC_LO32(pQTD);

            /* Update the HC's next pointer */

            USB_EHCD_SET_BITFIELD(uBusIndex,
                                  QH,
                                  pHCDPipe->pQH->uNextQtdPointer,
                                  (uNextQTDPointer >> 5),
                                  NEXTQTD_POINTER);

            /* Indicate that this is not the last element in the list */

            USB_EHCD_SET_BITFIELD(uBusIndex,
                                  QH,
                                  pHCDPipe->pQH->uNextQtdPointer,
                                  USB_EHCD_VALID_LINK,
                                  NEXTQTD_POINTER_T);
            }
        }

    /* If there are TDs already added to the QH */

    else
        {
        /* Scan for the last TD linked to the QH */

        for (pQTD = pHCDPipe->pQH->pQTD;
            pQTD->pNext != NULL;
            pQTD = pQTD->pNext);

        /*
         * Update the head of the request to be the
         * next element of the last TD
         */

        pQTD->pNext = (pUSB_EHCD_QTD)pRequest->pHead;

        /* Get the lower 32 bit of the BUS address */

        uNextQTDPointer = USB_EHCD_DESC_LO32(pQTD->pNext);

        /* Update the HC's next pointer */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              QTD,
                              pQTD->uNextQTDPointer,
                              (uNextQTDPointer >> 5),
                              NEXTQTD_POINTER);

        /*
         * Indicate that the last element which was already linked is
         * not the last element
         */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              QTD,
                              pQTD->uNextQTDPointer,
                              USB_EHCD_VALID_LINK,
                              NEXTQTD_POINTER_T);

        /*
         * If TD in qH is not active && next qtd ptr is invalid,
         * modify qh's qtd ptr ( Do not link if QH is halted )
         */

        if (!pHCDPipe->bIsHalted)
            {
            if ((USB_EHCD_INVALID_LINK ==
                          USB_EHCD_GET_BITFIELD(uBusIndex,
                                                QH,
                                                pHCDPipe->pQH->uNextQtdPointer,
                                                NEXTQTD_POINTER_T)) &&
                (USB_EHCD_QTD_STATUS_ACTIVE !=
                          (USB_EHCD_GET_BITFIELD(
                                          uBusIndex,
                                          QH,
                                          pHCDPipe->pQH->uTransferInfo,
                                          TRANSFERINFO_STATUS) &
                                          USB_EHCD_QTD_STATUS_ACTIVE)))
                {
                USB_EHCD_SET_BITFIELD(uBusIndex,
                                QH,
                                pHCDPipe->pQH->uNextQtdPointer,
                                (USB_EHCD_GET_BITFIELD(uBusIndex,
                                                      QTD,
                                                      pQTD->uNextQTDPointer,
                                                      NEXTQTD_POINTER)),
                                NEXTQTD_POINTER);

                USB_EHCD_SET_BITFIELD(uBusIndex,
                                      QH,
                                      pHCDPipe->pQH->uNextQtdPointer,
                                      USB_EHCD_VALID_LINK,
                                      NEXTQTD_POINTER_T);
                }
            }
        }

    return USBHST_SUCCESS;
    }

/*******************************************************************************
*
* usbEhcdSubmitControlURB - submit a new URB to the control pipe
*
* This routine is used to submit a new URB to the control pipe. It is done by
* loading a request info with the proper QTDs and then call the common queue
* link routine to put the QTDs onto the hardware schedule.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: USBHST_INVALID_PARAMETER if any of the parameter is invalid
*          USBHST_FAILURE if any resouce can not be requested
*          USBHST_SUCCESS if the URB is successfully loaded onto the hardware
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbEhcdSubmitControlURB
    (
    pUSB_EHCD_DATA          pHCDData,
    pUSB_EHCD_PIPE          pHCDPipe,
    pUSB_EHCD_REQUEST_INFO  pRequest,
    pUSBHST_URB             pURB
    )
    {
    /* To hold the HCD bus index */

    UINT8                   uBusIndex;

    /* To hold the number of bytes to be transferred */

    UINT32                  uNumBytes;

    /* To hold the PID value */

    UINT8                   uPID;

    /* To hold the status of the function call */

    BOOLEAN                 bStatus;

    /* To hold the status of the function call */

    STATUS                  vxbStatus;

    /* Pointer to the setup packet */

    pUSBHST_SETUP_PACKET    pSetup;

    /* Setup QTD for this control pipe */

    pUSB_EHCD_QTD           pSetupQTD;

    /* Status QTD for this control pipe */

    pUSB_EHCD_QTD           pStatusQTD;

    /* QTD for this control pipe */

    pUSB_EHCD_QTD           pQTD;

    /* QTD Next Link pointer */

    UINT32                  uNextQTDPointer;

    /* Get the HCD bus index */

    uBusIndex = (UINT8)pHCDData->uBusIndex;

    /*
     * Control transfer must have the Setup packet
     * which is kept in pTransferSpecificData
     */

    if (pURB->pTransferSpecificData == NULL)
        {
        USB_EHCD_ERR(
            "usbEhcdSubmitControlURB - pTransferSpecificData NULL\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /*
     * Control transfer Data phase is limited in a UINT16 size,
     * if greater that that, it should be an invalid request.
     */

    if (pURB->uTransferLength > USB_EHCD_CTRL_MAX_DATA_SIZE)
        {
        USB_EHCD_ERR(
            "usbEhcdSubmitControlURB - uTransferLength %d is larger than %d\n",
            pURB->uTransferLength,
            USB_EHCD_CTRL_MAX_DATA_SIZE, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Get a QTD as the Setup QTD */

    pSetupQTD = usbEhcdFormEmptyQTD(pHCDData, pHCDPipe);

    if (pSetupQTD == NULL)
        {
        USB_EHCD_ERR("usbEhcdSubmitControlURB - pSetupQTD NULL\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Get a QTD as the Status QTD */

    pStatusQTD = usbEhcdFormEmptyQTD(pHCDData, pHCDPipe);

    if (pStatusQTD == NULL)
        {
        USB_EHCD_ERR("usbEhcdSubmitControlURB - pStatusQTD NULL\n",
            0, 0, 0, 0, 0, 0);

        /* Return the QTD back to the free list */

        usbEhcdAddToFreeQTDList(pHCDData, pHCDPipe, pSetupQTD);

        return USBHST_INVALID_PARAMETER;
        }

    /* Retrieve the setup packet information */

    pSetup = (pUSBHST_SETUP_PACKET)(pURB->pTransferSpecificData);

    /* Load the DMA MAP for Control Setup */

    vxbStatus = vxbDmaBufMapLoad (pHCDData->pDev,
        pHCDPipe->ctrlSetupDmaTagId,
        pRequest->ctrlSetupDmaMapId,
        pURB->pTransferSpecificData,
        sizeof(USBHST_SETUP_PACKET),
        VXB_DMABUF_MAPLOAD_CACHEALIGNCHECK);

    if (vxbStatus != OK)
        {
        USB_EHCD_ERR(
            "usbEhcdSubmitControlURB - load ctrlSetupDmaMapId fail\n",
            0, 0, 0, 0, 0, 0);

        /* Return the QTD back to the free list */

        usbEhcdAddToFreeQTDList(pHCDData, pHCDPipe, pSetupQTD);

        /* Return the QTD back to the free list */

        usbEhcdAddToFreeQTDList(pHCDData, pHCDPipe, pStatusQTD);

        return USBHST_FAILURE;
        }

    /* <copy to bounce buffer and> DMA cache flush */

    vxbStatus = vxbDmaBufSync(pHCDData->pDev,
        pHCDPipe->ctrlSetupDmaTagId,
        pRequest->ctrlSetupDmaMapId,
        _VXB_DMABUFSYNC_DMA_PREWRITE);

    if (vxbStatus != OK)
        {
        USB_EHCD_ERR("usbEhcdSubmitControlURB - sync ctrlSetupDmaMapId fail\n",
            0, 0, 0, 0, 0, 0);

        /* Unload the control Setup DMA MAP */

        vxbDmaBufMapUnload (pHCDPipe->ctrlSetupDmaTagId,
            pRequest->ctrlSetupDmaMapId);

        /* Return the QTD back to the free list */

        usbEhcdAddToFreeQTDList(pHCDData, pHCDPipe, pSetupQTD);

        /* Return the QTD back to the free list */

        usbEhcdAddToFreeQTDList(pHCDData, pHCDPipe, pStatusQTD);

        return USBHST_FAILURE;
        }

    /* Load the DMA MAP for Control Data */

    if (pURB->uTransferLength != 0)
        {
        /* Load the DMA MAP */

        vxbStatus = vxbDmaBufMapLoad (pHCDData->pDev,
            pHCDPipe->usrBuffDmaTagId,
            pRequest->usrDataDmaMapId,
            pURB->pTransferBuffer,
            pURB->uTransferLength,
            VXB_DMABUF_MAPLOAD_CACHEALIGNCHECK);

        if (vxbStatus != OK)
            {
            USB_EHCD_ERR("usbEhcdSubmitControlURB - load usrDataDmaMapId fail\n",
                0, 0, 0, 0, 0, 0);

            /* Unload the control Setup DMA MAP */

            vxbDmaBufMapUnload (pHCDPipe->ctrlSetupDmaTagId,
                pRequest->ctrlSetupDmaMapId);

            /* Return the QTD back to the free list */

            usbEhcdAddToFreeQTDList(pHCDData, pHCDPipe, pSetupQTD);

            /* Return the QTD back to the free list */

            usbEhcdAddToFreeQTDList(pHCDData, pHCDPipe, pStatusQTD);

            return USBHST_FAILURE;
            }

        /* Perform vxbDmaBufSync operation according to Data direction */

        if (pSetup->bmRequestType & USB_EHCD_DIR_IN)
            {
            /* Save the Data direction */

            pRequest->uDataDir = USB_EHCD_DIR_IN;

            /* DMA cache invalidate before data receive */

            vxbStatus = vxbDmaBufSync(pHCDData->pDev,
                pHCDPipe->usrBuffDmaTagId,
                pRequest->usrDataDmaMapId,
                _VXB_DMABUFSYNC_DMA_PREREAD);
            }
        else
            {
            /* Save the Data direction */

            pRequest->uDataDir = USB_EHCD_DIR_OUT;

            /* <copy to bounce buffer and> DMA cache flush */

            vxbStatus = vxbDmaBufSync(pHCDData->pDev,
                pHCDPipe->usrBuffDmaTagId,
                pRequest->usrDataDmaMapId,
                _VXB_DMABUFSYNC_DMA_PREWRITE);
            }

        if (vxbStatus != OK)
            {
            USB_EHCD_ERR("usbEhcdSubmitControlURB - "
                "vxbDmaBufSync usrDataDmaMapId fail \n",
                0, 0, 0, 0, 0, 0);

            /* Unload the control Setup DMA MAP */

            vxbDmaBufMapUnload (pHCDPipe->ctrlSetupDmaTagId,
                pRequest->ctrlSetupDmaMapId);

            /* Unload the control Data DMA MAP */

            vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
                pRequest->usrDataDmaMapId);

            /* Return the QTD back to the free list */

            usbEhcdAddToFreeQTDList(pHCDData, pHCDPipe, pSetupQTD);

            /* Return the QTD back to the free list */

            usbEhcdAddToFreeQTDList(pHCDData, pHCDPipe, pStatusQTD);

            return USBHST_FAILURE;
            }
        }

    /* Update the buffer pointer fields of the QTD */

    uNumBytes = usbEhcdFillQTDBuffer(uBusIndex,
                                     pSetupQTD,
                                     pRequest->ctrlSetupDmaMapId->fragList[0].frag,
                                     sizeof(USBHST_SETUP_PACKET),
                                     pHCDPipe->uMaximumPacketSize);

    /* Check if the buffer pointer fields are updated */

    if (0 == uNumBytes)
        {
        USB_EHCD_ERR("usbEhcdSubmitControlURB - pSetupQTD fill fail\n",
            0, 0, 0, 0, 0, 0);

        /* Unload the control Setup DMA MAP */

        vxbDmaBufMapUnload (pHCDPipe->ctrlSetupDmaTagId,
            pRequest->ctrlSetupDmaMapId);

        /* Unload the control Data DMA MAP */

        vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
            pRequest->usrDataDmaMapId);

        /* Return the QTD back to the free list */

        usbEhcdAddToFreeQTDList(pHCDData, pHCDPipe, pSetupQTD);

        /* Return the QTD back to the free list */

        usbEhcdAddToFreeQTDList(pHCDData, pHCDPipe, pStatusQTD);

        return USBHST_FAILURE;
        }

    /* Update the data toggle bit */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          QTD,
                          pSetupQTD->uTransferInfo,
                          0,
                          TOKEN_DT);

    /* Indicate that it is a SETUP token */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          QTD,
                          pSetupQTD->uTransferInfo,
                          USB_EHCD_SETUP_PID,
                          TOKEN_PID_CODE);

    /* Copy the TD to the head of the request */

    pRequest->pHead = (VOID *)pSetupQTD;

    /* Update the buffer pointer fields of the TD */

    (void) usbEhcdFillQTDBuffer(uBusIndex, pStatusQTD, 
                         (bus_addr_t)(ULONG)NULL, 0,
                         pHCDPipe->uMaximumPacketSize);

    /* Update the data toggle field */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          QTD,
                          pStatusQTD->uTransferInfo,
                          1U,
                          TOKEN_DT);

    /*
     * Indicate that an interrupt is to be
     * generated on completion of the status stage
     */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          QTD,
                          pStatusQTD->uTransferInfo,
                          1U,
                          TOKEN_IOC);


    /* Indicate that there are no elements after the status stage */

    pStatusQTD->pNext = NULL;

    /*
     * Based on the direction of data transfer, update the status
     * stage direction. The status stage would be in a direction
     * opposite to the data stage direction.
     */

    uPID = (UINT8)((0 != (pSetup->bmRequestType & USB_EHCD_DIR_IN))?
                                USB_EHCD_OUT_PID : USB_EHCD_IN_PID);

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          QTD,
                          pStatusQTD->uTransferInfo,
                          uPID,
                          TOKEN_PID_CODE);

    /* Make this as the tail */

    pRequest->pTail = (VOID *)pStatusQTD;

    /* Create TDs for the DATA TRANSACTION */

    if (pURB->uTransferLength != 0)
        {
        /* Pointer to the head of the data TD list */

        pUSB_EHCD_QTD pDataHead = NULL;

        /* Pointer to the tail of the data TD list */

        pUSB_EHCD_QTD pDataTail = NULL;

        /* To hold the PID value */

        uPID = (UINT8)((0 != (pSetup->bmRequestType & USB_EHCD_DIR_IN))?
                     USB_EHCD_IN_PID : USB_EHCD_OUT_PID);

        /* Create the TD chain for the data stage */

        bStatus = usbEhcdCreateQTDs(pHCDData,
                                    pHCDPipe,
                                    pRequest,
                                    &pDataHead,
                                    &pDataTail,
                                    pRequest->usrDataDmaMapId->fragList[0].frag,
                                    pURB->uTransferLength,
                                    pHCDPipe->uMaximumPacketSize,
                                    1,
                                    uPID);

        /* Check if the TDs are not created successfully */

        if (FALSE == bStatus)
            {
            USB_EHCD_ERR("usbEhcdSubmitURB - Data TDs not created\n",
                0, 0, 0, 0, 0, 0);

            /* Unload the control Setup DMA MAP */

            vxbDmaBufMapUnload (pHCDPipe->ctrlSetupDmaTagId,
                pRequest->ctrlSetupDmaMapId);

            /* Unload the control Data DMA MAP */

            vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
                pRequest->usrDataDmaMapId);

            /* Return the QTD back to the free list */

            usbEhcdAddToFreeQTDList(pHCDData, pHCDPipe, pSetupQTD);

            /* Return the QTD back to the free list */

            usbEhcdAddToFreeQTDList(pHCDData, pHCDPipe, pStatusQTD);

            return USBHST_FAILURE;
            }

        /* Store the head of the Request list */

        pQTD = (pUSB_EHCD_QTD)pRequest->pHead;

        /* Update the next element of the head of the request */

        pQTD->pNext = pDataHead;

        /* Get the lower 32 bit of the BUS address */

        uNextQTDPointer = USB_EHCD_DESC_LO32(pDataHead);

        /* Update the HC's link pointers */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              QTD,
                              pQTD->uNextQTDPointer,
                              (uNextQTDPointer >> 5),
                              NEXTQTD_POINTER);

        /*
         * Indicate that the head element of
         * the request is not the last element in the list
         */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              QTD,
                              pQTD->uNextQTDPointer,
                              0,
                              NEXTQTD_POINTER_T);

        /* Update the HCD maintained next pointer */

        pDataTail->pNext = (pUSB_EHCD_QTD)pRequest->pTail;

        /* Get the lower 32 bit of the BUS address */

        uNextQTDPointer = USB_EHCD_DESC_LO32(pDataTail->pNext);

        /* Update the HC maintained next pointer */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              QTD,
                              pDataTail->uNextQTDPointer,
                              (uNextQTDPointer >> 5),
                              NEXTQTD_POINTER);

        /*
         * Indicate that the last data TD is not the last element in
         * the list as the status stage would follow
         */

        /* pDataTail->dword0.t = 0; */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              QTD,
                              pDataTail->uNextQTDPointer,
                              0,
                              NEXTQTD_POINTER_T);
        }/* End of if(0 != pURB->uTransferLength) */

    /* The control transfer has no data stage */

    else
        {
        /* Store the head of the Request list */

        pQTD = (pUSB_EHCD_QTD)pRequest->pHead;

        /*
         * Update the next element of the head
         * to point to the tail element of the request.
         */

        pQTD->pNext = (pUSB_EHCD_QTD)pRequest->pTail;

        /* Get the lower 32 bit of the BUS address */

        uNextQTDPointer = USB_EHCD_DESC_LO32(pQTD->pNext);

        /* Update the HC maintained next pointer */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              QTD,
                              pQTD->uNextQTDPointer,
                              (uNextQTDPointer >> 5),
                              NEXTQTD_POINTER);

        /* Indicate that head is not the last element in the queue */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              QTD,
                              pQTD->uNextQTDPointer,
                              0,
                              NEXTQTD_POINTER_T);
        }

    /* Queue these QTDs onto the QH */

    return usbEhcdQueueQTDs(pHCDData,
                pHCDPipe, pRequest, pURB);
    }

/*******************************************************************************
*
* usbEhcdSubmitControlURB - submit a new URB to the bulk or interrupt pipe
*
* This routine is used to submit a new URB to the bulk or interrupt pipe.
* It is done by loading a request info with the proper QTDs and then
* call the common queue link routine to put the QTDs onto the hardware
* schedule.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: USBHST_FAILURE if any resouce can not be requested
*          USBHST_SUCCESS if the URB is successfully loaded onto the hardware
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbEhcdSubmitBulkIntrURB
    (
    pUSB_EHCD_DATA          pHCDData,
    pUSB_EHCD_PIPE          pHCDPipe,
    pUSB_EHCD_REQUEST_INFO  pRequest,
    pUSBHST_URB             pURB
    )
    {
    /* To hold the HCD bus index */

    UINT8                   uBusIndex;

    /* To hold the PID value */

    UINT8                   uPID;

    /* To hold the status of the function call */

    BOOLEAN                 bStatus;

    /* To hold the status of the function call */

    STATUS                  vxbStatus;

    /* QTD for this pipe */

    pUSB_EHCD_QTD           pQTD;


    /* Get the HCD bus index */

    uBusIndex = (UINT8)pHCDData->uBusIndex;

    /* Load the DMA MAP */

    vxbStatus = vxbDmaBufMapLoad (pHCDData->pDev,
        pHCDPipe->usrBuffDmaTagId,
        pRequest->usrDataDmaMapId,
        pURB->pTransferBuffer,
        pURB->uTransferLength,
        VXB_DMABUF_MAPLOAD_CACHEALIGNCHECK);

    if (vxbStatus != OK)
        {
        USB_EHCD_ERR("usbEhcdSubmitBulkIntrURB - load usrDataDmaMapId fail\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_FAILURE;
        }

    /* Perform vxbDmaBufSync operation according to Data direction */

    if (pHCDPipe->uEndpointDir)
        {
        /* Save the Data direction */

        pRequest->uDataDir = USB_EHCD_DIR_IN;

        /* DMA cache invalidate before data receive */

        vxbStatus = vxbDmaBufSync(pHCDData->pDev,
            pHCDPipe->usrBuffDmaTagId,
            pRequest->usrDataDmaMapId,
            _VXB_DMABUFSYNC_DMA_PREREAD);
        }
    else
        {
        /* Save the Data direction */

        pRequest->uDataDir = USB_EHCD_DIR_OUT;

        /* <copy to bounce buffer and> DMA cache flush */

        vxbStatus = vxbDmaBufSync(pHCDData->pDev,
            pHCDPipe->usrBuffDmaTagId,
            pRequest->usrDataDmaMapId,
            _VXB_DMABUFSYNC_DMA_PREWRITE);
        }

    if (vxbStatus != OK)
        {
        USB_EHCD_ERR("usbEhcdSubmitControlURB - "
            "vxbDmaBufSync usrDataDmaMapId fail \n",
            0, 0, 0, 0, 0, 0);

        /* Unload the control Data DMA MAP */

        vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
            pRequest->usrDataDmaMapId);

        return USBHST_FAILURE;
        }

    /* To hold the PID value */

    uPID = (UINT8)((0 != pHCDPipe->uEndpointDir)?
                 USB_EHCD_IN_PID : USB_EHCD_OUT_PID);

    /* Create the TDs required for the transfer */

    bStatus = usbEhcdCreateQTDs(pHCDData,
                                pHCDPipe,
                                pRequest,
                                (pUSB_EHCD_QTD *)&pRequest->pHead,
                                (pUSB_EHCD_QTD *)&pRequest->pTail,
                                pRequest->usrDataDmaMapId->fragList[0].frag,
                                pRequest->usrDataDmaMapId->fragList[0].fragLen,
                                pHCDPipe->uMaximumPacketSize,
                                0,
                                uPID);

    /* Check if the TDs are created successfully */

    if (FALSE == bStatus)
        {
        USB_EHCD_ERR("usbEhcdSubmitURB - Data TDs not created\n",
            0, 0, 0, 0, 0, 0);

        /* Unload the control Data DMA MAP */

        vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
            pRequest->usrDataDmaMapId);

        return USBHST_FAILURE;
        }

    if (pHCDData->intEachTD)
        {
        /*
         * The below code addresses defect CQ:WIND00084918. It was
         * observerd that for NEC 720101  EHCI Controller sometimes the
         * Next qTD pointer in the Transfer Descriptor(qTD) of the EHCI
         * Data Structure was not getting updated by the host controller
         * hardware. This resulted in a transfer error.
         *
         * In the below code, we are providing a work-around (very similar
         * to the workaround provided by Linux Code for the same hardware).
         *
         * Description of Workaround Provided
         * ----------------------------------
         * As the workaround, for every TD processes by the host controller,
         * an interrupt was generated, subsequent to which the software
         * (HCD) used to update the Next qTD pointer manually.
         *
         * In the below code we update every TD to generate an interrupt on
         * every TD Completion.
         *
         * Note: The operations may be a little slow because here we
         * generate interrupt on every TD completion.
         */

        if (pRequest->pHead != pRequest->pTail)
            {
            pUSB_EHCD_QTD    pTempTDHead = NULL;

            /* Store the head temporarily */

            pTempTDHead = (pUSB_EHCD_QTD)pRequest->pHead;

            /* Interrupt on completion of head request */

            USB_EHCD_SET_BITFIELD(uBusIndex, QTD,
                              pTempTDHead->uTransferInfo,
                              1,
                              TOKEN_IOC);
            }
        }

    /* Copy the pointer to the tail of the request */

    pQTD = (pUSB_EHCD_QTD)pRequest->pTail;

    /* Interrupt on completion of tail request */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          QTD,
                          pQTD->uTransferInfo,
                          1,
                          TOKEN_IOC);

    /* Do some post setup */

    return usbEhcdQueueQTDs(pHCDData,
                pHCDPipe, pRequest, pURB);
    }

/*******************************************************************************
*
* usbEhcdSubmitControlURB - submit a new URB to the isochronous pipe
*
* This routine is used to submit a new URB to the isochronous pipe.
* It is done by loading a request info with the proper isochronous TDs and
* link these isochronous TDs onto hardware schedule.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: USBHST_FAILURE if any resouce can not be requested
*          USBHST_SUCCESS if the URB is successfully loaded onto the hardware
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbEhcdSubmitIsochURB
    (
    pUSB_EHCD_DATA          pHCDData,
    pUSB_EHCD_PIPE          pHCDPipe,
    pUSB_EHCD_REQUEST_INFO  pRequest,
    pUSBHST_URB             pURB
    )
    {
    /* To hold the status of the function call */

    BOOLEAN                 bStatus;

    /* To hold the status of the function call */

    STATUS                  vxbStatus;

    /* Load the DMA MAP */

    vxbStatus = vxbDmaBufMapLoad (pHCDData->pDev,
        pHCDPipe->usrBuffDmaTagId,
        pRequest->usrDataDmaMapId,
        pURB->pTransferBuffer,
        pURB->uTransferLength,
        VXB_DMABUF_MAPLOAD_CACHEALIGNCHECK);

    if (vxbStatus != OK)
        {
        USB_EHCD_ERR("usbEhcdSubmitIsochURB - load usrDataDmaMapId fail\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_FAILURE;
        }

    /* Perform vxbDmaBufSync operation according to Data direction */

    if (pHCDPipe->uEndpointDir)
        {
        /* Save the Data direction */

        pRequest->uDataDir = USB_EHCD_DIR_IN;

        /* DMA cache invalidate before data receive */

        vxbStatus = vxbDmaBufSync(pHCDData->pDev,
            pHCDPipe->usrBuffDmaTagId,
            pRequest->usrDataDmaMapId,
            _VXB_DMABUFSYNC_DMA_PREREAD);
        }
    else
        {
        /* Save the Data direction */

        pRequest->uDataDir = USB_EHCD_DIR_OUT;

        /* <copy to bounce buffer and> DMA cache flush */

        vxbStatus = vxbDmaBufSync(pHCDData->pDev,
            pHCDPipe->usrBuffDmaTagId,
            pRequest->usrDataDmaMapId,
            _VXB_DMABUFSYNC_DMA_PREWRITE);
        }

    if (vxbStatus != OK)
        {
        USB_EHCD_ERR("usbEhcdSubmitIsochURB - "
            "vxbDmaBufSync usrDataDmaMapId fail \n",
            0, 0, 0, 0, 0, 0);

        /* Unload the control Data DMA MAP */

        vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
            pRequest->usrDataDmaMapId);

        return USBHST_FAILURE;
        }


    /* If it is a high speed transfer request, create ITDs and link them */

    if (USBHST_HIGH_SPEED == pHCDPipe->uSpeed)
        {
        /* Create isochronous TDs and link them */

        bStatus = usbEhcdGenerateITDs(pHCDData,
                                      pHCDPipe,
                                      pRequest,
                                      (pUSB_EHCD_ITD *)&pRequest->pHead,
                                      (pUSB_EHCD_ITD *)&pRequest->pTail,
                                      pRequest->usrDataDmaMapId->fragList[0].frag,
                                      pURB->uNumberOfPackets,
                                      (pUSBHST_ISO_PACKET_DESC)
                                      pURB->pTransferSpecificData);

        /* Check if the TDs are created successfully */

        if (FALSE == bStatus)
            {
            USB_EHCD_ERR("usbEhcdSubmitIsochURB - Error in creating ITDs\n",
                0, 0, 0, 0, 0, 0);

            /* Unload the control Data DMA MAP */

            vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
                pRequest->usrDataDmaMapId);

            return USBHST_FAILURE;
            }
        }
    else
        {
        /* Create SITD isochronous TDs and link them */

        bStatus = usbEhcdGenerateSITDs(pHCDData,
                                       pHCDPipe,
                                       pRequest,
                                       (pUSB_EHCD_SITD *)&pRequest->pHead,
                                       (pUSB_EHCD_SITD *)&pRequest->pTail,
                                       pRequest->usrDataDmaMapId->fragList[0].frag,
                                       pURB->uNumberOfPackets,
                                       (pUSBHST_ISO_PACKET_DESC)
                                       pURB->pTransferSpecificData);

        /* Check if the TDs are created successfully */

        if (FALSE == bStatus)
            {
            USB_EHCD_ERR("usbEhcdSubmitIsochURB - Error in creating SITDs\n",
                0, 0, 0, 0, 0, 0);

            /* Unload the control Data DMA MAP */

            vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
                pRequest->usrDataDmaMapId);

            return USBHST_FAILURE;
            }
        }

    /* If it is a high speed transfer request, link ITDs */

    if (USBHST_HIGH_SPEED == pHCDPipe->uSpeed)
        {

        /* Call the function to link the isochronous TDs */

        usbEhcdLinkITDs(pHCDData, pHCDPipe, (UINT32)pHCDPipe->uStartIndex,
                        (pUSB_EHCD_ITD)pRequest->pHead,
                        (pUSB_EHCD_ITD)pRequest->pTail);

        }
    else
        {
        /* Call the function to link the Split isochronous TDs */

        usbEhcdLinkSITDs(pHCDData, pHCDPipe, (UINT32)pHCDPipe->uStartIndex,
                         (pUSB_EHCD_SITD)pRequest->pHead,
                         (pUSB_EHCD_SITD)pRequest->pTail);

        }

    return USBHST_SUCCESS;
    }

/*******************************************************************************
*
* usbEhcdSubmitURB - submit a request to a pipe.
*
* This routine is used to submit a request to the pipe. <uBusIndex> specifies
* the host controller bus index. <uPipeHandle> holds the pipe handle. <pURB>
* is the pointer to the URB holding the request details. This is done by
* checking if the parameters are valid or request can be added, then calling
* the proper sub routines to do the actual transfer.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the URB is submitted successfully.
*   USBHST_INVALID_PARAMETER - Returned if the parameters are not valid.
*   USBHST_BAD_START_OF_FRAME - Returned if the start frame is invalid.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbEhcdSubmitURB
    (
    UINT8           uBusIndex,          /* Index of the host controller */
    ULONG           uPipeHandle,        /* Pipe handle */
    pUSBHST_URB     pURB                /* Pointer to the URB */
    )
    {
    /* To hold the pointer to the data structure */

    pUSB_EHCD_DATA          pHCDData;

    /* Pointer to the HCD maintained pipe */

    pUSB_EHCD_PIPE          pHCDPipe;

    /* To hold the request information */

    pUSB_EHCD_REQUEST_INFO  pRequest;

    /* To hold the status of the request */

    USBHST_STATUS           Status;

    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT(
        USB_EHCI_WV_TRANSFER,
        "usbEhcdSubmitURB() starts",
        USB_EHCD_WV_FILTER);

    /* Check the validity of the parameters */

    if ((USB_MAX_EHCI_COUNT <= uBusIndex) ||
        (0 == uPipeHandle) ||
        (NULL == pURB))
        {
        USB_EHCD_WARN("usbEhcdSubmitURB - parameters are not valid\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the global data structure */

    pHCDData = g_pEHCDData[uBusIndex];

    /* Assert if the global pointer is not valid */

    if (NULL == pHCDData)
        {
        USB_EHCD_ERR("usbEhcdSubmitURB - pHCDData is not valid\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the USB_EHCD_PIPE data structure */

    pHCDPipe = (pUSB_EHCD_PIPE)uPipeHandle;

    /* Check if the request is for the Root hub and route it */

    if ((pHCDData->RHData.pInterruptPipe == pHCDPipe) ||
        (pHCDData->RHData.pControlPipe == pHCDPipe) ||
        ((pHCDData->pDefaultPipe == pHCDPipe) &&
         (0 == pHCDData->RHData.uDeviceAddress)))
        {
        Status = usbEhcdRHSubmitURB(pHCDData,
                                    uPipeHandle,
                                    pURB);

        return Status;
        }

    /* Check if the endpoint type is valid */

    if ((USBHST_CONTROL_TRANSFER != pHCDPipe->uEndpointType) &&
        (USBHST_BULK_TRANSFER != pHCDPipe->uEndpointType) &&
        (USBHST_INTERRUPT_TRANSFER != pHCDPipe->uEndpointType) &&
        (USBHST_ISOCHRONOUS_TRANSFER != pHCDPipe->uEndpointType))
        {
        USB_EHCD_ERR("usbEhcdSubmitURB - Invalid endpoint type %d\n",
            pHCDPipe->uEndpointType, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Non-Isoc should have QH */

    if (USBHST_ISOCHRONOUS_TRANSFER != pHCDPipe->uEndpointType)
        {
        if (NULL == pHCDPipe->pQH)
            {
            USB_EHCD_ERR("usbEhcdSubmitURB - pHCDPipe->pQH is not valid\n",
                0, 0, 0, 0, 0, 0);

            return USBHST_INVALID_PARAMETER;
            }
        }

    /* Isoc should get a valid start frame */

    else
        {
        /* To hold the current frame index */

        UINT32 uFrameNumber = 0;

        /* Get the current frame index */

        uFrameNumber = USB_EHCD_GET_FIELD(pHCDData,
                                          FRINDEX,
                                          FRAME_INDEX);

        /* Shift FrameIndex to get no. of SOF */

        uFrameNumber >>= 3;

        /* Check for Frame index and get it */

        if (USBHST_START_ISOCHRONOUS_TRANSFER_ASAP !=
            (pURB->uTransferFlags & USBHST_START_ISOCHRONOUS_TRANSFER_ASAP))
            {
            uFrameNumber = pURB->uStartFrame;
            }
        else
            {
            if (USB_EHCD_NO_LIST == pHCDPipe->uLastIndex)
                {
                uFrameNumber += USB_EHCD_FRAME_OFFSET;
                uFrameNumber &= 0x3FF;
                }

            else if (pHCDPipe->uLastIndex >
                (UINT16)(uFrameNumber+ USB_EHCD_FRAME_OFFSET))
                {

                uFrameNumber = (UINT32)pHCDPipe->uLastIndex;
                }
            else if ((pHCDPipe->uLastIndex >= (UINT16) uFrameNumber) ||
                     (USB_EHCD_NO_LIST == pHCDPipe->uLastIndex))
                {
                uFrameNumber += USB_EHCD_FRAME_OFFSET;
                uFrameNumber &= 0x3FF;
                }
            else if ((pHCDPipe->uStartIndex > pHCDPipe->uLastIndex) &&
                     ((int)uFrameNumber > pHCDPipe->uLastIndex))
                {
                uFrameNumber = (UINT32)pHCDPipe->uLastIndex;
                }
            else if ((int)uFrameNumber > pHCDPipe->uLastIndex)
                {
                uFrameNumber += USB_EHCD_FRAME_OFFSET;
                uFrameNumber &= 0x3FF;
                }
            else
                {
                USB_EHCD_ERR("usbEhcdSubmitURB - Invalid frame index\n",
                    0, 0, 0, 0, 0, 0);

                return USBHST_BAD_START_OF_FRAME;
                }
            }
        /*
         * Save the starting frame numer which will be used
         * for actual submitting
         */

        pHCDPipe->uStartIndex = (INT16)uFrameNumber;
        }

    /* Exclusively access the pipe request list */

    OS_WAIT_FOR_EVENT(pHCDPipe->PipeSynchEventID,OS_WAIT_INFINITE);

    /* Get a free request info structure from the pipe */

    pRequest = usbEhcdReserveRequestInfo(pHCDData, pHCDPipe, pURB);

    if (pRequest == NULL)
        {
        USB_EHCD_ERR("usbEhcdSubmitURB - no free pRequest found\n",
            0, 0, 0, 0, 0, 0);

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

        return USBHST_INVALID_REQUEST;
        }

    /* Check if it is a control transfer request */

    if (USBHST_CONTROL_TRANSFER == pHCDPipe->uEndpointType)
        {
        Status = usbEhcdSubmitControlURB(pHCDData,
                    pHCDPipe, pRequest, pURB);
        }
    else if ((USBHST_BULK_TRANSFER == pHCDPipe->uEndpointType) ||
             (USBHST_INTERRUPT_TRANSFER == pHCDPipe->uEndpointType))
        {
        Status = usbEhcdSubmitBulkIntrURB(pHCDData,
                    pHCDPipe, pRequest, pURB);
        }
    else
        {
        Status = usbEhcdSubmitIsochURB(pHCDData,
                    pHCDPipe, pRequest, pURB);
        }

    /* Check if there is any failure */

    if (Status != USBHST_SUCCESS)
        {
        USB_EHCD_ERR("usbEhcdSubmitURB - URB submit fail (%d)\n",
            Status, 0, 0, 0, 0, 0);

        /* Return the request info to the pipe */

        usbEhcdReleaseRequestInfo(pHCDData, pHCDPipe, pRequest);
        }
    else
        {
        /* 
         * Drain the write buffer 
         */
        
        USB_EHCD_WRITE_BUFFER_DRAIN();
        }

    /* Release the exclusive access */

    OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

    return Status;
    }/* End of function usbEhcdSubmitURB() */

/*******************************************************************************
*
* usbEhcdCancelURB - cancel a request to a pipe
*
* This routine is used to cancel a request to the pipe. <uBusIndex> specifies
* the host controller bus index. <uPipeHandle> holds the pipe handle. <pURB>
* pointer to the URB holding the request details.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the URB is cancelled successfully.
*   USBHST_INVALID_PARAMETER - Returned if the parameters are not valid.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbEhcdCancelURB
    (
    UINT8       uBusIndex,   /* Index of the host controller */
    ULONG       uPipeHandle, /* Pipe handle */
    pUSBHST_URB pURB         /* Pointer to the URB */
    )
    {
    /* To hold the pointer to the data structure */

    pUSB_EHCD_DATA          pHCDData = NULL;

    /* The filed value get from a descriptor */

    UINT32                  uFieldValue;

    /* Pointer to the HCD maintained pipe */

    pUSB_EHCD_PIPE          pHCDPipe = NULL;

    /* To hold the request information */

    pUSB_EHCD_REQUEST_INFO  pRequest = NULL;

    /* Temporary pointer */

    UINT32                  pQHTemp;

    /* To hold the status of the request */

    USBHST_STATUS           Status = USBHST_FAILURE;

    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT(
        USB_EHCI_WV_TRANSFER,
        "usbEhcdCancelURB() starts",
        USB_EHCD_WV_FILTER);

    /* Check the validity of the parameters */

    if ((USB_MAX_EHCI_COUNT <= uBusIndex) ||
        (0 == uPipeHandle) ||
        (NULL == pURB))
        {
        USB_EHCD_ERR("usbEhcdCancelURB - parameters are not valid\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the global data structure */

    pHCDData = g_pEHCDData[uBusIndex];

    /* Assert if the global pointer is not valid */

    if (NULL == pHCDData)
        {
        USB_EHCD_ERR("usbEhcdCancelURB - pHCDData not valid\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the EHCD_PIPE data structure */

    pHCDPipe = (pUSB_EHCD_PIPE)uPipeHandle;

    /* Check if the request is for the Root hub and route it */

    if (pHCDData->RHData.pInterruptPipe == pHCDPipe)
        {
        Status = usbEhcdRHCancelURB(pHCDData,
                                    uPipeHandle,
                                    pURB);

        return Status;
        }

    /* Check if the endpoint type is valid */

    if ((USBHST_CONTROL_TRANSFER != pHCDPipe->uEndpointType) &&
        (USBHST_BULK_TRANSFER != pHCDPipe->uEndpointType) &&
        (USBHST_INTERRUPT_TRANSFER != pHCDPipe->uEndpointType) &&
        (USBHST_ISOCHRONOUS_TRANSFER != pHCDPipe->uEndpointType))
        {
        USB_EHCD_ERR("usbEhcdCancelURB - Invalid endpoint type\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /*
     * It is of the assumption that the USBD always gives a valid pipe handle.
     * This also includes not giving a cancel URB request while the pipe
     * is being deleted.
     */

    /* Exclusively access the pipe request list */

    OS_WAIT_FOR_EVENT(pHCDPipe->PipeSynchEventID,OS_WAIT_INFINITE);

    /* Get the HCD specific information of this URB */

    pRequest = (pUSB_EHCD_REQUEST_INFO)pURB->pHcdSpecific;

    /* If the request is not found, return an error */

    if (NULL == pRequest)
        {
        USB_EHCD_ERR("usbEhcdCancelURB - Request is not found\n",
            0, 0, 0, 0, 0, 0);

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

        return USBHST_INVALID_REQUEST;
        }

    if (USBHST_ISOCHRONOUS_TRANSFER == pHCDPipe->uEndpointType)
        {
        /* Unlink all the isoch TDs associated with this request */

        USB_EHCD_UNLINK_ISOCH_REQ(pHCDData,
                                  pRequest, pHCDPipe->uSpeed);

        }
    else
        {
        /* Pointer to the tail of QTD */

        pUSB_EHCD_QTD   pQTDTail;

        /* Pointer to the head of QTD */

        pUSB_EHCD_QTD   pQTDHead;

        pQTDHead = (pUSB_EHCD_QTD)pRequest->pHead;
        pQTDTail = (pUSB_EHCD_QTD)pRequest->pTail;

        if (pRequest->pHead == pHCDPipe->pQH->pQTD)
            {
            pHCDPipe->pQH->pQTD = pQTDTail->pNext;

            USB_EHCD_DBG("usbEhcdCancelURB - "
                "1st request, pHCDPipe->pQH->pQTD = %p\n",
                 pHCDPipe->pQH->pQTD, 2, 3, 4, 5, 6);

            if (pHCDPipe->pQH->pQTD == NULL)
                {
                /* Indicate that there are no elements in the list */

                USB_EHCD_SET_BITFIELD(uBusIndex,
                                     QH,
                                     pHCDPipe->pQH->uNextQtdPointer,
                                     USB_EHCD_INVALID_LINK,
                                     NEXTQTD_POINTER_T) ;

                /* Update the HC's next pointer */

                USB_EHCD_SET_BITFIELD(uBusIndex,
                                     QH,
                                     pHCDPipe->pQH->uNextQtdPointer,
                                     (ULONG)NULL,
                                     NEXTQTD_POINTER) ;

                /* Initialize all the other fields */

                USB_EHCD_SET_BITFIELD(uBusIndex,
                                     QH,
                                     pHCDPipe->pQH->uCurrentQtdPointer,
                                     0,
                                     CURRENTQTD_POINTER);

                pHCDPipe->pQH->uAlternateNextQtdPointer = 0;

                /* Do not modify the toggle value */
                /* Do not modify the ping state for High-speed bulk and control endpoints */
                
                if ((USBHST_HIGH_SPEED == pHCDPipe->uSpeed) &&
                    ((USBHST_BULK_TRANSFER == pHCDPipe->uEndpointType) ||
                     (USBHST_CONTROL_TRANSFER == pHCDPipe->uEndpointType)))
                    {
                    pHCDPipe->pQH->uTransferInfo = pHCDPipe->pQH->uTransferInfo &
                        USB_EHCD_SWAP_DESC_DATA(uBusIndex,
                                                USB_EHCD_QH_TRANSFERINFO_DT_MASK |
                                                USB_EHCD_QH_TRANSFERINFO_PINGSTATE_MASK);
                    }
                else
                    {
                    pHCDPipe->pQH->uTransferInfo = pHCDPipe->pQH->uTransferInfo &
                        USB_EHCD_SWAP_DESC_DATA(uBusIndex,
                                                USB_EHCD_QH_TRANSFERINFO_DT_MASK);
                    }

                pHCDPipe->pQH->uBufferPagePointerList[0] = 0;
                pHCDPipe->pQH->uBufferPagePointerList[1] = 0;
                pHCDPipe->pQH->uBufferPagePointerList[2] = 0;
                pHCDPipe->pQH->uBufferPagePointerList[3] = 0;
                pHCDPipe->pQH->uBufferPagePointerList[4] = 0;

                pHCDPipe->pQH->uExtBufferPointerPageList[0] = 0;
                pHCDPipe->pQH->uExtBufferPointerPageList[1] = 0;
                pHCDPipe->pQH->uExtBufferPointerPageList[2] = 0;
                pHCDPipe->pQH->uExtBufferPointerPageList[3] = 0;
                pHCDPipe->pQH->uExtBufferPointerPageList[4] = 0;
                }
            else
                {
                /* First invalidate the link */

                USB_EHCD_SET_BITFIELD(uBusIndex,
                                    QH,pHCDPipe->pQH->uNextQtdPointer,
                                    USB_EHCD_INVALID_LINK,
                                    NEXTQTD_POINTER_T) ;

                /* Initialize all the fields */

                USB_EHCD_SET_BITFIELD(uBusIndex,
                                     QH,
                                     pHCDPipe->pQH->uCurrentQtdPointer,
                                     0,
                                     CURRENTQTD_POINTER);

                pHCDPipe->pQH->uAlternateNextQtdPointer = 0;
                pHCDPipe->pQH->uTransferInfo = 0;

                pHCDPipe->pQH->uBufferPagePointerList[0] = 0;
                pHCDPipe->pQH->uBufferPagePointerList[1] = 0;
                pHCDPipe->pQH->uBufferPagePointerList[2] = 0;
                pHCDPipe->pQH->uBufferPagePointerList[3] = 0;
                pHCDPipe->pQH->uBufferPagePointerList[4] = 0;

                /* Get the lower 32 bit of the BUS address */

                pQHTemp = USB_EHCD_DESC_LO32(pHCDPipe->pQH->pQTD);

                /* Update the HC's next pointer */

                USB_EHCD_SET_BITFIELD(uBusIndex,
                                     QH,
                                     pHCDPipe->pQH->uNextQtdPointer,
                                    (pQHTemp >> 5),
                                    NEXTQTD_POINTER) ;

                /* Indicate that this is a valid element in the list */

                USB_EHCD_SET_BITFIELD(uBusIndex,
                                    QH,pHCDPipe->pQH->uNextQtdPointer,
                                    USB_EHCD_VALID_LINK,
                                    NEXTQTD_POINTER_T) ;
                }
            }
        else
            {
            /* Pointer to the tail QTD of the request being removed */

            pUSB_EHCD_QTD pHCDQTD = NULL;

            /* Retrieve the TD previous to the TD being removed */

            for (pHCDQTD = pHCDPipe->pQH->pQTD;
                 (NULL != pHCDQTD) &&
                 (pHCDQTD->pNext != pRequest->pHead);
                 pHCDQTD = pHCDQTD->pNext);

            if (pHCDQTD == NULL)
                {
                USB_EHCD_DBG("usbEhcdCancelURB - "
                             "non-1st request but no prev QTD found\n",
                             1, 2, 3, 4, 5, 6);

                /* Release the resource access */

                OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

                return USBHST_INVALID_REQUEST;
                }

            pHCDQTD->pNext = pQTDTail->pNext;

            USB_EHCD_DBG("usbEhcdCancelURB - non-1st request pHCDQTD->pNext [%p]\n",
                         pHCDQTD->pNext, 2, 3, 4, 5, 6);

            /* Invalidate the next QTD first */

            USB_EHCD_SET_BITFIELD(uBusIndex,
                            QTD,
                            pHCDQTD->uNextQTDPointer,
                            USB_EHCD_INVALID_LINK,
                            NEXTQTD_POINTER_T);


            uFieldValue = (UINT32)USB_EHCD_GET_BITFIELD(uBusIndex,
                            QTD,
                            pQTDTail->uNextQTDPointer,
                            NEXTQTD_POINTER);

            /* Update HC maintained next pointers */

            USB_EHCD_SET_BITFIELD(uBusIndex,
                            QTD,
                            pHCDQTD->uNextQTDPointer,
                            uFieldValue,
                            NEXTQTD_POINTER);

            /* Update the t bits */

            uFieldValue = (UINT32)USB_EHCD_GET_BITFIELD(uBusIndex,
                            QTD,
                            pQTDTail->uNextQTDPointer,
                            NEXTQTD_POINTER_T);

            USB_EHCD_SET_BITFIELD(uBusIndex,
                            QTD,
                            pHCDQTD->uNextQTDPointer,
                            uFieldValue,
                            NEXTQTD_POINTER_T);


            }
        }

    /*
     * If there is any real data transfer, we should do
     * vxbDmaBufSync and unload the DMA MAP.
     */

    if (pRequest->uRequestLength != 0)
        {
        /* Unload the Data DMA MAP */

        vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
            pRequest->usrDataDmaMapId);
        }

    /* Control transfer should also deal with the Setup buffer */

    if (pHCDPipe->uEndpointType == USBHST_CONTROL_TRANSFER)
        {
        /* Unload the control Setup DMA MAP */

        vxbDmaBufMapUnload (pHCDPipe->ctrlSetupDmaTagId,
            pRequest->ctrlSetupDmaMapId);
        }

    /*
     * Clear the URB HCD specific information
     */

    pURB->pHcdSpecific = NULL;


    /* Update the URB status to cancelled */

    pURB->nStatus = USBHST_TRANSFER_CANCELLED;

    /* Set the request's active URB as NULL indicating it is canceled */

    pRequest->pUrb = NULL;

    /* Release the exclusive access */

    OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

    /*
     * Check if it is an asynchronous request and add it to
     * the asynchrnous request removal list
     */

    if ((USBHST_CONTROL_TRANSFER == pHCDPipe->uEndpointType) ||
        (USBHST_BULK_TRANSFER == pHCDPipe->uEndpointType))
        {
        /* Add to the asynch request removal list */

        USB_EHCD_ADD_TO_ASYNCH_REQUEST_REMOVAL_LIST(pHCDData, pRequest);

        if (FALSE == usrUsbEhciPollingEnabled())
            {
            /* Enable the aynch advance interrupt */

            USB_EHCD_SET_BIT_USBINTR_INT_ON_ASYNC_ADVANCE_DOORBELL(pHCDData);
            }

        /*
         * Set the interrupt to be generated on an advance of
         * asynchronous schedule
         */

        USB_EHCD_SET_BIT_USBCMD_INT_ON_ASYNC_ADVANCE_DOORBELL(pHCDData);

        }
    /*
     * Check if it is a periodic request and add it to the periodic
     * request removal list
     */

    else /* if ((USBHST_INTERRUPT_TRANSFER == pHCDPipe->uEndpointType) ||
             (USBHST_ISOCHRONOUS_TRANSFER == pHCDPipe->uEndpointType)) */
        {
        /* Add the request to the periodic request removal list */

        USB_EHCD_ADD_TO_PERIODIC_REQUEST_REMOVAL_LIST(pHCDData, pRequest);

        if (FALSE == usrUsbEhciPollingEnabled())
            {
            /* Enable the frame list rollover interrupt */

            USB_EHCD_SET_BIT_USBINTR_INT_ON_FRAME_LIST_ROLLOVER(pHCDData);
            }
        else
            {
            /*
             * Give sufficient delay for the frame list to rollover.
             * This is the worst case condition where the frame list rolls
             * over in less than 1024 ms
             */

            OS_DELAY_MS(USB_EHCD_MAX_FRAMELIST_ENTIRES);
            }
        }

    /*
     * If the request which is being cancelled is an asynchronous transfer
     * request, and if there are no more outstanding asynchronous transfers,
     * then the asynchronous list can be disabled.
     */

    if ((USBHST_CONTROL_TRANSFER == pHCDPipe->uEndpointType) ||
        (USBHST_BULK_TRANSFER == pHCDPipe->uEndpointType))
        {
        semTake (pHCDData->AsychQueueMutex, WAIT_FOREVER);

        pHCDData->noOfAsynchTransfers--;

        if (pHCDData->noOfAsynchTransfers == 0)
            {
            /*
             * There are chances that at this point, another
             * task tries to delete an asynchronous pipe and has
             * enabled the asynch advance doorbell interrupt.
             * An asynch advance doorbell interrupt will not be
             * generated if the schedule is disabled. So check
             * if there are any elements in the reclamation list.
             * If only there are no elements in the reclamation
             * list, disable the asynchronous schedule.
             */

            OS_WAIT_FOR_EVENT(pHCDData->ReclamationListSynchEventID,
                              OS_WAIT_INFINITE);

            if (pHCDData->pAsynchReclamationListHead == NULL)
                {
                USB_EHCD_VDBG("usbEhcdCancelURB - Clr ASYNCH_SCHEDULE_ENABLE\n",
                    0, 0, 0, 0, 0, 0);

                USB_EHCD_CLR_BIT(pHCDData,
                                 USBCMD,
                                 ASYNCH_SCHEDULE_ENABLE);
                }

            OS_RELEASE_EVENT(pHCDData->ReclamationListSynchEventID);
            }

        semGive (pHCDData->AsychQueueMutex);
        }

    /*
     * Now it is time to call the URB callback and we MUST call it here.
     *
     * We can not delay the callback, becasue the URB itself belongs to
     * the upper layer software such as the class drivers, there will be
     * chances that just after the URB cancel is returns, the URB is
     * destroyed by the user code, thus any delayed access to the URB
     * is considered unsafe.
     *
     * However, we MUST delay the removal of the associated HCD specific
     * transfer descriptors and related request info structure until
     * the hardware has advanced the schedule, otherwise, if we remove
     * and destroy these hardware accessble memory strcutures, the hardware
     * may get to access some invalid address thus causing unexpected
     * behaviour. Once the hardware schedule is advanced, the interrupts
     * such as ASYNC_ADVANCE_DOORBELL or FRAME_LIST_ROLLOVER will be
     * triggered, thus the interrupt handlers for these will clean these
     * request info structures and return the transfer descriptors properly,
     * but at that time the URB callback will not be called (we have done
     * here).
     */

    /* Call the callback function if it is registered */

    if (NULL != pURB->pfCallback)
        {
        USB_EHCD_DBG("usbEhcdCancelURB - calling callback %p\n",
            pURB->pfCallback, 0, 0, 0, 0, 0);

        (pURB->pfCallback)(pURB);
        }

    return USBHST_SUCCESS;
    }/* End of usbEhcdCancelURB() */


/*******************************************************************************
*
* usbEhcdIsRequestPending - checks if a request is pending for a pipe.
*
* This function is used to check whether any request is pending on a pipe.
* <uBusIndex> Specifies the host controller bus index. <uPipeHandle> holds the
* pipe handle.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if a request is pending for the pipe.
*   USBHST_FAILURE - Returned if the request is not pending for the pipe.
*   USBHST_INVALID_PARAMETER - Returned if the parameters are not valid.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbEhcdIsRequestPending
    (
    UINT8  uBusIndex, /* Index of the host controller */
    ULONG  uPipeHandle  /* Pipe handle */
    )
    {
    /* Pointer to the HCD maintained pipe */

    pUSB_EHCD_PIPE pHCDPipe = NULL;

    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT(
        USB_EHCI_WV_TRANSFER,
        "usbEhcdIsRequestPending() starts",
        USB_EHCD_WV_FILTER);

    /* Check the validity of the parameters */

    if ((USB_MAX_EHCI_COUNT <= uBusIndex) ||
        (0 == uPipeHandle))
        {
        USB_EHCD_ERR("usbEhcdIsRequestPending - parameters are not valid\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the USB_EHCD_PIPE data structure */

    pHCDPipe = (pUSB_EHCD_PIPE)uPipeHandle;

    /* Check if any request is pending for the endpoint */

    if (NULL == pHCDPipe->pRequestQueueHead)
        {
        return USBHST_FAILURE;
        }

    /* Return success indicating that a request is pending for the pipe */

    return USBHST_SUCCESS;
    }/* End of function usbEhcdIsRequestPending() */


/*******************************************************************************
*
* usbEhcdGetFrameNumber - get the current frame number of the bus.
*
* This routine is used to get the current frame number of the host controller.
* <uBusIndex> Specifies the host controller bus index. <puFrameNumber> is a
* pointer to a variable to hold the current frame number.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the frame number was obtained successfully.
*   USBHST_INVALID_PARAMETER - Returned if the parameters are not valid.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbEhcdGetFrameNumber
    (
    UINT8   uBusIndex,    /* Index of the host controller */
    UINT16 *puFrameNumber /* Pointer to the variable to hold the frame number */
    )
    {
    /* To hold the pointer to the data structure */

    pUSB_EHCD_DATA pHCDData = NULL;

    /* The current frame number */

    UINT32 uFrameNumber = 0;

    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT(
        USB_EHCI_WV_TRANSFER,
        "usbEhcdGetFrameNumber() starts",
        USB_EHCD_WV_FILTER);

    /* Check the validity of the parameters */

    if ((USB_MAX_EHCI_COUNT <= uBusIndex) ||
        (NULL == puFrameNumber))
        {
        USB_EHCD_ERR("usbEhcdGetFrameNumber - parameters are not valid\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the global data structure */

    pHCDData = g_pEHCDData[uBusIndex];

    /* Assert if the global pointer is not valid */

    if (NULL == pHCDData)
        {
        USB_EHCD_ERR("usbEhcdGetFrameNumber - pHCDData is not valid\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /*
     * Read the frame register contents and copy it to
     * the OUT parameter.
     * Note : This copies only 14 bits
     */

    uFrameNumber = USB_EHCD_GET_FIELD(pHCDData,
                                      FRINDEX,
                                      FRAME_INDEX);

    *puFrameNumber = (UINT16)(uFrameNumber >> 3);

    return USBHST_SUCCESS;
    } /* End of function usbEhcdGetFrameNumber() */

/*******************************************************************************
*
* usbEhcdSetBitRate - modify the frame width
*
* This routine is used to modify the frame width of the host controller.
* <uBusIndex> specifies the host controller bus index. <bIncrement> is a flag
* to specify whether the frame number should be incremented or decremented.
* If TRUE, the frame number will be incremented, otherwise, decremented.
* <puCurrentFrameWidth> is a pointer to hold the current frame width(after
* modification)
*
* RETURNS:
*   USBHST_FAILURE - Returned if the functionality is not supported.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbEhcdSetBitRate
    (
    UINT8   uBusIndex,          /* Index of the host controller       */
    BOOL    bIncrement,         /* Flag for increment or decrement    */
    UINT32 *puCurrentFrameWidth /* Pointer to the current frame width */
    )
    {
    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT(
        USB_EHCI_WV_TRANSFER,
        "usbEhcdSetBitRate() starts",
        USB_EHCD_WV_FILTER);

    /*
     * Eventhough changing the bittime is possible, but it
     * needs to halt the controller, this is not supported
     */

    /* As this functionality is not supported, always return a failure. */

    return USBHST_FAILURE;
    }/* End of function usbEhcdSetBitRate() */


