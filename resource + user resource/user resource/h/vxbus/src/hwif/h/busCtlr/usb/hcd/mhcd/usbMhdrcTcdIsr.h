/* usbMhdrcTcdIsr.h -  USB Mentor Graphics TCD Interrupt Handler module */

/*
 * Copyright (c) 2010, 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,05jan13,ljg  add CPPI DMA support for MHDRC TCD (WIND00398723)
01a,17may10,s_z  created
*/

/*
DESCRIPTION

This file defines the interrupt handler for the USB Mentor Graphice Target 
Controller Driver.

*/

#ifndef __INCusbMhdrcTcdIsrh
#define __INCusbMhdrcTcdIsrh

#ifdef  __cplusplus
extern "C" {
#endif

/* declartion */

void usbMhdrcTcdInterruptHandler
    (
    pUSBTGT_TCD pTCD
    );
void usbMhdrcTcdHsDmaInterruptHandler
    (
    pUSB_MUSBMHDRC  pMHDRC,
    int             uChannel
    );

void usbMhdrcTcdCppiDmaInterruptHandler
    (
    pUSB_MUSBMHDRC  pMHDRC,
    UINT8           uChannel
    );

void usbMhdrcTcdEp0ReqHandle
    (
    pUSB_MHDRC_TCD_DATA      pTCDData,
    pUSB_MHDRC_TCD_REQUEST   pRequest
    );
void usbMhdrcTcdReqHandle
    (
    pUSB_MHDRC_TCD_DATA      pTCDData,
    pUSB_MHDRC_TCD_REQUEST   pRequest
    );
#endif /* __INCusbMhdrcTcdIsrh */
