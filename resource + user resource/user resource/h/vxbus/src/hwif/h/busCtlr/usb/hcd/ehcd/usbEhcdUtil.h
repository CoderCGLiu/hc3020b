/* usbEhcdUtil.h - Utility Functions for EHCI */

/*
 * Copyright (c) 2003-2005, 2007, 2009-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2003-2005, 2007, 2009-2013 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
Modification history
--------------------
01t,16feb13,s_z  Check the request before adding to removal list (WIND00395057)
01s,10jul12,ljg  Update nStatus of pUrb after isochronous transfer (WIND00353767)
01r,27jun12,ljg  Update uTransferLength after isochronous transfer (WIND00353767)
01q,13dec11,m_y  Modify according to code check result (WIND00319317)
01p,16sep11,s_z  Add usbEhcdAsynchScheduleEnable and usbEhcdAsynchScheduleDisable
                 routines to avoid dead loop to process the asynchnorous schedule
                 (WIND00293308)
01o,03aug10,m_y  Modify level of some debug message
01n,27may10,w_x  Avoid dead loop waiting for ASYNCH_SCHEDULE_ENABLE to be
                 set or cleared (WIND00214252)
01m,18may10,w_x  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
01l,13jan10,ghs  vxWorks 6.9 LP64 adapting
01k,15jul09,ghs  Fix for WIND00171264, remove align defines
01j,09apr09,w_x  Remove tabs and coding convention changes
01i,05feb07,ami  Defect WIND00086670: In-Tokens for Interrupt Endpoint not
                 getting schedulled properly with polling interval of less
                 then 1 ms
01h,28mar05,pdg  non-PCI changes
01g,22mar05,mta  64-bit support added (SPR #104950)
01f,02feb05,pdg  Fix for multiple device connection/disconnection
01e,03aug04,ami  Warning Messages Removed
01d,23Jul03,gpd  Incorporated the changes identified during testing on MIPS
01c,03Jul03,gpd  fixed the bug with asynchronous schedule updation.
                 Added separate reclamation lists for asynchronous and periodic
                 Added reclamation lists for asynch and periodic request
                 cancellation.
01b,26jun03,psp  changing the code to WRS standards.
01a,25apr03,ram  written.
*/

/*
DESCRIPTION
This contains the prototypes of the utility functions
which are used by the EHCI Driver.
*/

/*
INTERNAL
 *******************************************************************************
 * Filename         : usbEhcdUtil.h
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 ******************************************************************************/



#ifndef __INCusbEhcdUtilh
#define __INCusbEhcdUtilh

#ifdef    __cplusplus
extern "C" {
#endif


/* defines */

/* Max retry times to set or clear the ASYNCH_SCHEDULE_ENABLE bit of USBCMD reg */

#define USB_EHCD_CONFIG_ASYNCH_SCHEDULE_MAX_RETRY  100

/* Macro to Initialize USB_USB_EHCD_PIPE */

#define USB_EHCD_PIPE_INITIALIZE(pEHCDPipe)                                    \
    {                                                                          \
    OS_MEMSET(pEHCDPipe, 0, sizeof(USB_EHCD_PIPE));                            \
    pEHCDPipe->PipeDeletedFlag = FALSE;                                        \
    }

/* Macro to Add QH to asynchronous schedule */

#define USB_EHCD_ADD_TO_HC_ASYNCH_SCHEDULE(pHCDData,                           \
                                           pQH)                                \
    {                                                                          \
    /* To hold the field returned by GET_FIELD macro. */                       \
                                                                               \
    UINT32 getField;                                                           \
                                                                               \
    /* Temporary pointer to hold the queue head */                             \
                                                                               \
    UINT32 pQhPtrLow32;                                                        \
                                                                               \
    /* Take ASYNC mutex to set/clr ASYNCH_SCHEDULE_ENABLE bit */               \
                                                                               \
    semTake (pHCDData->AsychQueueMutex, WAIT_FOREVER);                         \
                                                                               \
    getField = (UINT32)USB_EHCD_GET_BITFIELD(pHCDData->uBusIndex,              \
               QH,                                                             \
               pHCDData->pAsynchTailPipe->pQH->uQueueHeadHorizontalLinkPointer,\
               HORIZONTAL_LINK_POINTER);                                       \
                                                                               \
    USB_EHCD_SET_BITFIELD(pHCDData->uBusIndex,                                 \
                QH, pQH->uQueueHeadHorizontalLinkPointer,                      \
                getField, HORIZONTAL_LINK_POINTER);                            \
                                                                               \
                                                                               \
    USB_EHCD_VDBG("ADD_TO_HC_ASYNCH_SCHEDULE - Clr ASYNCH_SCHEDULE_ENABLE\n",  \
                  0, 0, 0, 0, 0, 0);                                           \
    /*                                                                         \
     * Don't clear the ASYNCH_SCHEDULE_ENABLE when HC is halted                \
     * to avoid the dead loop waiting for the bit to be cleared.               \
     */                                                                        \
                                                                               \
    if (USB_EHCD_GET_FIELD(pHCDData, USBSTS, HCHALTED) == 0)                   \
        {                                                                      \
        /* Disable the asynchronous schedule */                                \
                                                                               \
        usbEhcdAsynchScheduleDisable(pHCDData);                                \
                                                                               \
        }                                                                      \
                                                                               \
    pQhPtrLow32 = USB_EHCD_DESC_LO32(pQH);                                     \
                                                                               \
    /* Update the tail's next pointer */                                       \
                                                                               \
    USB_EHCD_SET_BITFIELD(pHCDData->uBusIndex,                                 \
               QH,                                                             \
               pHCDData->pAsynchTailPipe->pQH->uQueueHeadHorizontalLinkPointer,\
               (pQhPtrLow32 >> 5),                                             \
               HORIZONTAL_LINK_POINTER);                                       \
                                                                               \
                                                                               \
    USB_EHCD_VDBG("ADD_TO_HC_ASYNCH_SCHEDULE - Set ASYNCH_SCHEDULE_ENABLE\n",  \
                  0, 0, 0, 0, 0, 0);                                           \
                                                                               \
    /* Enable the asynchronous schedule */                                     \
                                                                               \
    usbEhcdAsynchScheduleEnable(pHCDData);                                     \
                                                                               \
    /* Release ASYNC mutex */                                                  \
                                                                               \
    semGive(pHCDData->AsychQueueMutex);                                        \
    }

/* Macro to Add QH to Interrupt Schedule */

#define USB_EHCD_ADD_TO_HC_INTERRUPT_SCHEDULE(pHCDData,                        \
                                              uListIndex,                      \
                                              pQH)                             \
    {                                                                          \
    /* To hold the field returned by GET_FIELD macro. */                       \
                                                                               \
    UINT32    getField;                                                        \
                                                                               \
    /* Pointer to the tail of the list */                                      \
                                                                               \
    pUSB_EHCD_QH pTailQH = NULL;                                               \
                                                                               \
    /* Temporary pointer to hold the queue head  */                            \
                                                                               \
    UINT32       pQhPtrLow32;                                                  \
                                                                               \
    /* Copy the tail of the list */                                            \
                                                                               \
    pTailQH = (pUSB_EHCD_QH)pHCDData->TreeListData[uListIndex].pTailPointer;   \
                                                                               \
                                                                               \
    /* Copy the next data structure type from the tail element */              \
                                                                               \
    getField = (UINT32)USB_EHCD_GET_BITFIELD(pHCDData->uBusIndex,              \
                                     QH,                                       \
                                     pTailQH->uQueueHeadHorizontalLinkPointer, \
                                     HORIZONTAL_LINK_POINTER_TYPE);            \
                                                                               \
    USB_EHCD_SET_BITFIELD(pHCDData->uBusIndex,                                 \
        QH,pQH->uQueueHeadHorizontalLinkPointer,                               \
        getField, HORIZONTAL_LINK_POINTER_TYPE );                              \
                                                                               \
    /*                                                                         \
     * Copy tail element's 't' type which indicates whether the next           \
     * data structure is a valid data structure                                \
     */                                                                        \
    /* pQH->dword0.t = pTailQH->dword0.t; */                                   \
                                                                               \
    getField = (UINT32)USB_EHCD_GET_BITFIELD(pHCDData->uBusIndex,              \
                        QH,                                                    \
                        pTailQH->uQueueHeadHorizontalLinkPointer,              \
                        HORIZONTAL_LINK_POINTER_T);                            \
                                                                               \
    USB_EHCD_SET_BITFIELD(pHCDData->uBusIndex,                                 \
                        QH,pQH->uQueueHeadHorizontalLinkPointer,               \
                        getField,                                              \
                        HORIZONTAL_LINK_POINTER_T);                            \
                                                                               \
    /* The QH's next pointer should point to the tail's next element */        \
    /* pQH->dword0.queue_head_horizontal_link_pointer =                        \
                        pTailQH->dword0.queue_head_horizontal_link_pointer; */ \
                                                                               \
    getField = (UINT32)USB_EHCD_GET_BITFIELD(pHCDData->uBusIndex,              \
                        QH,                                                    \
                        pTailQH->uQueueHeadHorizontalLinkPointer,              \
                        HORIZONTAL_LINK_POINTER);                              \
                                                                               \
    USB_EHCD_SET_BITFIELD(pHCDData->uBusIndex,                                 \
                        QH,pQH->uQueueHeadHorizontalLinkPointer,               \
                        getField,                                              \
                        HORIZONTAL_LINK_POINTER);                              \
                                                                               \
                                                                               \
    /* Update the next pointer of the QH */                                    \
                                                                               \
    pQH->pNext = pTailQH->pNext;                                               \
                                                                               \
    /* Update the tail's next pointer */                                       \
                                                                               \
    pTailQH->pNext = pQH;                                                      \
                                                                               \
    /* Update the tail pointer */                                              \
                                                                               \
    pHCDData->TreeListData[uListIndex].pTailPointer = (pVOID)pQH;              \
                                                                               \
    /* Get the low 32 bit of the BUS address */                                \
                                                                               \
    pQhPtrLow32 = USB_EHCD_DESC_LO32(pQH);                                     \
                                                                               \
    /* Update the next element of the tail pointer */                          \
    /* pTailQH->dword0.queue_head_horizontal_link_pointer =                    \
                                         (unsigned) (pQH) >> 5;  */            \
    USB_EHCD_SET_BITFIELD(pHCDData->uBusIndex,                                 \
                        QH,pTailQH->uQueueHeadHorizontalLinkPointer,           \
                        (pQhPtrLow32 >> 5),                                    \
                        HORIZONTAL_LINK_POINTER);                              \
                                                                               \
    /* make the link valid - this has effect only for list index 0 */          \
                                                                               \
    USB_EHCD_SET_BITFIELD(pHCDData->uBusIndex,                                 \
                        QH,pTailQH->uQueueHeadHorizontalLinkPointer,           \
                        USB_EHCD_VALID_LINK,                                   \
                        HORIZONTAL_LINK_POINTER_T);                            \
                                                                               \
    }

/* Macro to add pointer to USB_EHCD_PIPE in Asynch Reclamation List */

#define  USB_EHCD_ADD_TO_ASYNCH_RECLAMATION_LIST(pHCDData,                     \
                                                pHCDPipe)                      \
    {                                                                          \
    /* Exclusively access the list */                                          \
    OS_WAIT_FOR_EVENT(pHCDData->ReclamationListSynchEventID, OS_WAIT_INFINITE);\
                                                                               \
    /* Add to the list */                                                      \
    pHCDPipe->pNext = pHCDData->pAsynchReclamationListHead;                    \
                                                                               \
    /* Update the asynch reclamation list head */                              \
    pHCDData->pAsynchReclamationListHead = pHCDPipe;                           \
                                                                               \
    /* Release the exclusive access */                                         \
    OS_RELEASE_EVENT(pHCDData->ReclamationListSynchEventID);                   \
    }

/* Macro to add pointer to USB_EHCD_PIPE in Periodic Reclamation List */

#define  USB_EHCD_ADD_TO_PERIODIC_RECLAMATION_LIST(pHCDData,                   \
                                                   pHCDPipe)                   \
    {                                                                          \
    /* Exclusively access the list */                                          \
    OS_WAIT_FOR_EVENT(pHCDData->ReclamationListSynchEventID, OS_WAIT_INFINITE);\
                                                                               \
    /* Add to the list */                                                      \
    pHCDPipe->pNext = pHCDData->pPeriodicReclamationListHead;                  \
                                                                               \
    /* Update the periodic reclamation list head */                            \
    pHCDData->pPeriodicReclamationListHead = pHCDPipe;                         \
                                                                               \
    /* Release the exclusive access */                                         \
    OS_RELEASE_EVENT(pHCDData->ReclamationListSynchEventID);                   \
    }

/* Macro to add pointer to USB_EHCD_PIPE in Delayed pipe addition List */

#define  USB_EHCD_ADD_TO_DELAYED_PIPE_ADDITION_LIST(pHCDData,                  \
                                                   pHCDPipe)                   \
    {                                                                          \
    /* Exclusively access the list */                                          \
    OS_WAIT_FOR_EVENT(pHCDData->ReclamationListSynchEventID, OS_WAIT_INFINITE);\
                                                                               \
    /* Add to the list */                                                      \
    pHCDPipe->pAltNext = pHCDData->pDelayedPipeAdditionList;                   \
                                                                               \
    /* Update the delayed pipe addition list head */                           \
    pHCDData->pDelayedPipeAdditionList = pHCDPipe;                             \
                                                                               \
    /* Release the exclusive access */                                         \
    OS_RELEASE_EVENT(pHCDData->ReclamationListSynchEventID);                   \
    }

/* Macro to add pointer to USB_EHCD_PIPE in Delayed pipe removal List */

#define  USB_EHCD_ADD_TO_DELAYED_PIPE_REMOVAL_LIST(pHCDData,                   \
                                                   pHCDPipe)                   \
    {                                                                          \
    /* Exclusively access the list */                                          \
    OS_WAIT_FOR_EVENT(pHCDData->ReclamationListSynchEventID, OS_WAIT_INFINITE);\
                                                                               \
    /* Add to the list */                                                      \
    pHCDPipe->pAltNext = pHCDData->pDelayedPipeRemovalList;                    \
                                                                               \
    /* Update the delayed pipe removal list head */                            \
    pHCDData->pDelayedPipeRemovalList = pHCDPipe;                              \
                                                                               \
    /* Release the exclusive access */                                         \
    OS_RELEASE_EVENT(pHCDData->ReclamationListSynchEventID);                   \
    }

/* Macro to add pointer to USB_EHCD_PIPE in Isoch pipe List */

#define  USB_EHCD_ADD_TO_ISOCH_PIPE_LIST(pHCDData,                             \
                                                pHCDPipe)                      \
    {                                                                          \
    /* Exclusively access the list */                                          \
    OS_WAIT_FOR_EVENT(pHCDData->ActivePipeListSynchEventID, OS_WAIT_INFINITE); \
                                                                               \
    /* Add to the list */                                                      \
    pHCDPipe->pNext = pHCDData->pIsochPipeList;                                \
                                                                               \
    /* Update the isoch reclamation list head */                               \
    pHCDData->pIsochPipeList = pHCDPipe;                                       \
                                                                               \
    /* Release the exclusive access */                                         \
    OS_RELEASE_EVENT(pHCDData->ActivePipeListSynchEventID);                   \
    }

/* Macro to add pointer to USB_EHCD_PIPE in Isoch pipe List */

#define  USB_EHCD_REMOVE_ISOCH_PIPE(pHCDData,                     \
                                                pHCDPipe)         \
    {                                                             \
    pUSB_EHCD_PIPE pTempPipe;                                     \
                                                                  \
    if (pHCDPipe == pHCDData->pIsochPipeList)                     \
        {                                                         \
        /* If it is the hesd element,                             \
         * update the head of list to next pointer */             \
         pHCDData->pIsochPipeList = pHCDPipe->pNext;              \
        }                                                         \
    else                                                          \
        {                                                         \
        /* Search for the pipe in the list */                     \
        for (pTempPipe = pHCDData->pIsochPipeList;                \
             (NULL != pTempPipe) &&                               \
             (pHCDPipe != pTempPipe->pNext);                      \
              pTempPipe = pTempPipe->pNext);                      \
                                                                  \
        /* If Pipe not found, Assert an error */                  \
        if (NULL != pTempPipe)                                    \
            pTempPipe->pNext = pHCDPipe->pNext;                   \
        }                                                         \
    }

/* Macro to add pointer to USB_EHCD_PIPE in Inter pipe List */

#define  USB_EHCD_ADD_TO_INTER_PIPE_LIST(pHCDData,                             \
                                                pHCDPipe)                      \
    {                                                                          \
    /* Exclusively access the list */                                          \
    OS_WAIT_FOR_EVENT(pHCDData->ActivePipeListSynchEventID, OS_WAIT_INFINITE); \
                                                                               \
    /* Add to the list */                                                      \
    pHCDPipe->pNext = pHCDData->pInterPipeList;                                \
                                                                               \
    /* Update the inter pipe list head */                                      \
    pHCDData->pInterPipeList = pHCDPipe;                                       \
                                                                               \
    /* Release the exclusive access */                                         \
    OS_RELEASE_EVENT(pHCDData->ActivePipeListSynchEventID);                    \
    }

/* Macro to add pointer to USB_EHCD_PIPE in Isoch pipe List */

#define  USB_EHCD_REMOVE_INTER_PIPE(pHCDData,                     \
                                                pHCDPipe)         \
    {                                                             \
    pUSB_EHCD_PIPE pTempPipe;                                     \
                                                                  \
    if (pHCDPipe == pHCDData->pInterPipeList)                     \
        {                                                         \
        /* If it is the hesd element,                             \
         * update the head of list to next pointer */             \
         pHCDData->pInterPipeList = pHCDPipe->pNext;              \
        }                                                         \
    else                                                          \
        {                                                         \
        /* Search for the pipe in the list */                     \
        for (pTempPipe = pHCDData->pInterPipeList;                \
             (NULL != pTempPipe) &&                               \
             (pHCDPipe != pTempPipe->pNext);                      \
              pTempPipe = pTempPipe->pNext);                      \
                                                                  \
        /* If Pipe not found, Assert an error */                  \
        if (NULL != pTempPipe)                                    \
            pTempPipe->pNext = pHCDPipe->pNext;                   \
        }                                                         \
    }


/* Macro to add pointer to USB_EHCD_PIPE in Inter pipe List */

#define USB_EHCD_ADD_TO_ASYNCH_PIPE_LIST(pHCDData,                              \
                                           pHCDPipe)                           \
    {                                                                          \
    /* Exclusively access the list */                                          \
    OS_WAIT_FOR_EVENT(pHCDData->ActivePipeListSynchEventID, OS_WAIT_INFINITE); \
                                                                               \
    /* This USB_EHCD_PIPE pointer's next element is the tail's next element */ \
                                                                               \
    pHCDPipe->pNext = pHCDData->pAsynchTailPipe->pNext;                        \
                                                                               \
    /* The tail pointer's next points to this QH's USB_EHCD_PIPE pointer */    \
                                                                               \
    pHCDData->pAsynchTailPipe->pNext = pHCDPipe;                               \
                                                                               \
    /* Make this as the tail element */                                        \
                                                                               \
    pHCDData->pAsynchTailPipe = pHCDPipe;                                      \
                                                                               \
    /* Release the exclusive access */                                         \
    OS_RELEASE_EVENT(pHCDData->ActivePipeListSynchEventID);                    \
    }

/* Macro to add pointer to USB_EHCD_PIPE in Isoch pipe List */

#define  USB_EHCD_REMOVE_ASYNCH_PIPE(pHCDData,                      \
                                    pHCDPipe)                       \
{                                                                   \
pUSB_EHCD_PIPE          pTempPipe;                                  \
                                                                    \
/* Search the asychronous list and update the next pointers */      \
                                                                    \
for (pTempPipe = pHCDData->pDefaultPipe;                            \
     (pHCDPipe != pTempPipe->pNext) &&                              \
     (pHCDData->pDefaultPipe != pTempPipe->pNext);                  \
     pTempPipe = pTempPipe->pNext);                                 \
                                                                    \
/*                                                                  \
 * Deletion of the default pipe is not through this function. So    \
 * the pHCDPipe can never be the same as pHCDData->pDefaultPipe.    \
 * Thus the pHCDTempPipe should always be valid but just to         \
 * watch out.                                                       \
 */                                                                 \
                                                                    \
if (pTempPipe != NULL)                                              \
    {                                                               \
    /* Update the HCD maintained link pointers */                   \
                                                                    \
    pTempPipe->pNext = pHCDPipe->pNext;                             \
                                                                    \
    /* If this QH forms the tail, update the tail element */        \
                                                                    \
    if (pHCDData->pAsynchTailPipe == pHCDPipe)                      \
        {                                                           \
        pHCDData->pAsynchTailPipe = pTempPipe;                      \
        }                                                           \
    }                                                               \
}

/* Macro to add request to the list of asynchronous requests to be removed */

#define USB_EHCD_ADD_TO_ASYNCH_REQUEST_REMOVAL_LIST(pHCDData,                  \
                                                    pRequestInfo)              \
    {                                                                          \
    pUSB_EHCD_REQUEST_INFO pRequestChecker;                                    \
                                                                               \
    /* Exclusively access the list */                                          \
    OS_WAIT_FOR_EVENT(pHCDData->ReclamationListSynchEventID, OS_WAIT_INFINITE);\
                                                                               \
    /* Check if pRequest has already been added */                             \
    for (pRequestChecker = pHCDData->pHeadAsynchCancelList;                    \
         pRequestChecker != NULL; pRequestChecker = pRequestChecker->pAltNext) \
         {                                                                     \
         if (pRequestChecker == pRequestInfo)                                  \
             break;                                                            \
         }                                                                     \
                                                                               \
    if (pRequestChecker == NULL)                                               \
        {                                                                      \
        /* Add to the list */                                                  \
        pRequestInfo->pAltNext = pHCDData->pHeadAsynchCancelList;              \
                                                                               \
        /* Update the asynch cancel request list */                            \
        pHCDData->pHeadAsynchCancelList = pRequestInfo;                        \
        }                                                                      \
                                                                               \
    /* Release the exclusive access */                                         \
    OS_RELEASE_EVENT(pHCDData->ReclamationListSynchEventID);                   \
    }

/* Macro to add request to the list of periodic requests to be removed */
#define  USB_EHCD_ADD_TO_PERIODIC_REQUEST_REMOVAL_LIST(pHCDData,               \
                                                       pRequestInfo)           \
    {                                                                          \
    pUSB_EHCD_REQUEST_INFO pRequestChecker;                                    \
    /* Exclusively access the list */                                          \
    OS_WAIT_FOR_EVENT(pHCDData->ReclamationListSynchEventID, OS_WAIT_INFINITE);\
                                                                               \
    /* Check if pRequest has already been added */                             \
    for (pRequestChecker = pHCDData->pHeadPeriodicCancelList;                  \
         pRequestChecker != NULL; pRequestChecker = pRequestChecker->pAltNext) \
         {                                                                     \
         if (pRequestChecker == pRequestInfo)                                  \
             break;                                                            \
         }                                                                     \
                                                                               \
    if (pRequestChecker == NULL)                                               \
        {                                                                      \
        /* Add to the periodic list */                                         \
        pRequestInfo->pAltNext = pHCDData->pHeadPeriodicCancelList;            \
                                                                               \
        /* Update the periodic cancel request list */                          \
        pHCDData->pHeadPeriodicCancelList = pRequestInfo;                      \
        }                                                                      \
                                                                               \
                                                                               \
    /* Release the exclusive access */                                         \
    OS_RELEASE_EVENT(pHCDData->ReclamationListSynchEventID);                   \
    }

/* Macro to add the request to requests list maintained for the endpoint. */
#define  USB_EHCD_ADD_TO_ENDPOINT_LIST(pHCDData,                               \
                                   pRequest)                                   \
    {                                                                          \
                                                                               \
    /*                                                                         \
     * If there is no element in the list,                                     \
     * make the head and tail point to the request                             \
     */                                                                        \
    if (NULL == pRequest->pHCDPipe->pRequestQueueHead)                         \
        {                                                                      \
        pRequest->pHCDPipe->pRequestQueueHead = pRequest;                      \
        pRequest->pHCDPipe->pRequestQueueTail = pRequest;                      \
        }                                                                      \
    else                                                                       \
        {                                                                      \
        /* Update the next pointer of the tail element */                      \
        pRequest->pHCDPipe->pRequestQueueTail->pNext = pRequest;               \
        /* Update the tail element to point to the request */                  \
        pRequest->pHCDPipe->pRequestQueueTail = pRequest;                      \
        }                                                                      \
    }

/* This macro is used to to delete the request from request list maintained
 * for the endpoint */
#define  USB_EHCD_UNLINK_ISOCH_REQ(pEHCDData, pRequestInfo, uSpeed)      \
    {                                                                    \
    /* If it is a full speed isochronous endpoint,                       \
     * update the SITD's link pointers */                                \
                                                                         \
    if (USBHST_FULL_SPEED == uSpeed)                                     \
        {                                                                \
                                                                         \
        /* Pointer to the SITD data structure */                         \
                                                                         \
        pUSB_EHCD_SITD pSITD = NULL;                                     \
                                                                         \
        /* Unlinking all SITDs from hardware frame list. */              \
                                                                         \
        /* Extract the head of the request */                            \
        pSITD = (pUSB_EHCD_SITD)pRequestInfo->pHead;                     \
                                                                         \
        /* Remove all the SITDs from request list. */                    \
        while (NULL != pSITD)                                            \
            {                                                            \
            usbEhcdUnLinkSITD(pEHCDData, pSITD);                         \
            pSITD = pSITD->pVerticalNext;                                \
            }                                                            \
                                                                         \
        } /* End of Full speed isochronous endpoint removal handling */  \
                                                                         \
    /* It is a high speed endpoint, update the ITD's link pointers */    \
    else                                                                 \
        {                                                                \
        /* Pointer to the ITD data structure */                          \
                                                                         \
        pUSB_EHCD_ITD pITD = NULL;                                       \
                                                                         \
        /* Unlinking all ITDs from hardware frame list. */               \
                                                                         \
        /* Extract the head of the request */                            \
        pITD = (pUSB_EHCD_ITD)pRequestInfo->pHead;                       \
                                                                         \
        /* Remove all the SITDs from request list. */                    \
        while (NULL != pITD)                                             \
            {                                                            \
            usbEhcdUnLinkITD(pEHCDData, pITD);                           \
            pITD = pITD->pVerticalNext;                                  \
            }                                                            \
                                                                         \
        }/* End of high speed isochronous endpoint removal handling */   \
    }


/* This macro is used to calculate the bandwidth occupied in the frame */
#define USB_EHCD_CALCULATE_FRAME_BANDWIDTH(pEHCDData, uFrameIndex, uCalBandwidth)  \
    {                                                                              \
    /* To hold the index into the microframe array */                              \
    UINT32 uMicroFrameCount = 0;                                                   \
                                                                                   \
    /* This loop adds up the bandwidth in every microframe */                      \
    for (uMicroFrameCount = 0;                                                     \
         uMicroFrameCount < USB_EHCD_MAX_MICROFRAME_NUMBER;                        \
         uMicroFrameCount++)                                                       \
        {                                                                          \
        uCalBandwidth +=                                                           \
             pEHCDData->FrameBandwidth[uFrameIndex][uMicroFrameCount];             \
        }                                                                          \
    }

/*This macro is used to calculate the bandwidth occupied in the microframes
 specified by the mask. */
#define USB_EHCD_CALCULATE_BW_MICROFRAMES(pHCDData,                            \
                                      uFrameMask,                              \
                                      uFrameIndex,                             \
                                      uCalBandwidth)                           \
    {                                                                          \
    /* To hold the index into the microframes in a frame */                    \
    UINT8 uIndex = 0;                                                          \
                                                                               \
    /*                                                                         \
     * If this microframe is to be acounted                                    \
     * for bandwidth calculation, calculate                                    \
     */                                                                        \
    for (uIndex = 0; USB_EHCD_MAX_MICROFRAME_NUMBER > uIndex; uIndex++)        \
        {                                                                      \
        /* Check if this microframe's bandwidth is to be calculated */         \
        if (0 != ((uFrameMask >> uIndex) & 0x01))                              \
            {                                                                  \
            uCalBandwidth += pHCDData->FrameBandwidth[uFrameIndex][uIndex];    \
            }                                                                  \
        }                                                                      \
    }

/*  This macro is used to calculate the traversal count */
 #define USB_EHCD_CALCULATE_TRAVERSAL_COUNT(uPollInterval, uTraversalCount)    \
    {                                                                          \
    /*                                                                         \
     * If the pollinterval is between 32(not including 32) and 16              \
     * the list needs to be traversed once                                     \
     */                                                                        \
    if (16 <= uPollInterval)                                                   \
        {                                                                      \
        uTraversalCount = 1;                                                   \
        }                                                                      \
    /* Pollinterval lies between 8 and 16(excluded) */                         \
    else if (8 <= uPollInterval)                                               \
        {                                                                      \
        uTraversalCount = 2;                                                   \
        }                                                                      \
    /* Pollinterval lies between 4 and 8(excluded) */                          \
    else if (4 <= uPollInterval)                                               \
        {                                                                      \
        uTraversalCount = 3;                                                   \
        }                                                                      \
    /* Pollinterval lies between 2 and 4(excluded) */                          \
    else if (2 <= uPollInterval)                                               \
        {                                                                      \
        uTraversalCount = 4;                                                   \
        }                                                                      \
    /* Polling interval is 1 */                                                \
    else                                                                       \
        {                                                                      \
        uTraversalCount = 5;                                                   \
        }                                                                      \
    }

/*  This macro is used to calculate the traversal count */
 #define USB_EHCD_CALCULATE_LEAF_START_INDEX(uPollInterval, uTreeListCount)    \
    {                                                                          \
    /*                                                                         \
     * If the pollinterval is between 32(not including 32) and 16              \
     * the list needs to be traversed once                                     \
     */                                                                        \
    if (16 <= uPollInterval)                                                   \
        {                                                                      \
        uTraversalCount = 1;                                                   \
        }                                                                      \
    /* Pollinterval lies between 8 and 16(excluded) */                         \
    else if (8 <= uPollInterval)                                               \
        {                                                                      \
        uTraversalCount = 2;                                                   \
        }                                                                      \
    /* Pollinterval lies between 4 and 8(excluded) */                          \
    else if (4 <= uPollInterval)                                               \
        {                                                                      \
        uTraversalCount = 3;                                                   \
        }                                                                      \
    /* Pollinterval lies between 2 and 4(excluded) */                          \
    else if (2 <= uPollInterval)                                               \
        {                                                                      \
        uTraversalCount = 4;                                                   \
        }                                                                      \
    /* Polling interval is 1 */                                                \
    else                                                                       \
        {                                                                      \
        uTraversalCount = 5;                                                   \
        }                                                                      \
    }

#define USB_EHCD_GET_POLL_INTERVAL(uPollInterval)                              \
    {                                                                          \
    /* Calculate the polling interval in terms of frames */                    \
                                                                               \
    if (0 != (uPollInterval % USB_EHCD_MAX_MICROFRAME_NUMBER))                 \
        {                                                                      \
        uPollInterval = 1;                                                     \
        }                                                                      \
    else                                                                       \
        {                                                                      \
        uPollInterval /= USB_EHCD_MAX_MICROFRAME_NUMBER;                       \
        }                                                                      \
                                                                               \
    /* If the bandwidth is more than what is supported,                        \
     * update the polling interval to the supported polling interval           \
     */                                                                        \
                                                                               \
    if (USB_EHCD_MAX_USB11_INTERRUPT_POLL_INTERVAL <                           \
        uPollInterval)                                                         \
        {                                                                      \
        uPollInterval = USB_EHCD_MAX_USB11_INTERRUPT_POLL_INTERVAL;            \
        }                                                                      \
    }

#define USB_EHCD_GET_ISOCH_BANDWIDTH(pHCDData, IsochBW)                        \
    {                                                                          \
    UINT32 uFrameIndex, uUFrameIndex;                                          \
    for (uFrameIndex = 0;                                                      \
         USB_EHCD_MAX_FRAMELIST_ENTIRES > uFrameIndex;                            \
         uFrameIndex++)                                                        \
        {                                                                      \
        for (uUFrameIndex = 0;                                                 \
             USB_EHCD_MAX_MICROFRAME_NUMBER > uUFrameIndex;                    \
             uUFrameIndex++)                                                   \
             {                                                                 \
             IsochBW[uFrameIndex][uUFrameIndex]                                \
             = pHCDData->FrameListData[uFrameIndex].                           \
                         uBandwidth[uUFrameIndex];                             \
             }                                                                 \
        }                                                                      \
    }

#define USB_EHCD_GET_INTERRUPT_BANDWIDTH(pHCDData, InterruptBW)                \
    {                                                                          \
    UINT32 uFrameIndex, uUFrameIndex;                                          \
    for (uFrameIndex = 0;                                                      \
         USB_EHCD_MAX_TREE_NODES > uFrameIndex;                                \
         uFrameIndex++)                                                        \
        {                                                                      \
        for (uUFrameIndex = 0;                                                 \
             USB_EHCD_MAX_MICROFRAME_NUMBER > uUFrameIndex;                    \
             uUFrameIndex++)                                                   \
             {                                                                 \
             InterruptBW[uFrameIndex][uUFrameIndex]                            \
             = pHCDData->TreeListData[uFrameIndex].                            \
                         uBandwidth[uUFrameIndex];                             \
             }                                                                 \
         }                                                                     \
    }


#define USB_EHCD_GET_FRAME_BANDWIDTH(pHCDData, IsochBW, InterruptBW, pTotalFrameBW) \
    {                                                                          \
    UINT32 uFrameIndex, uUFrameIndex, uBandwidth, uTreeListIndex;              \
    /* This loop calculates bandwidth for each frame */                        \
                                                                               \
    for (uFrameIndex =0;                                                       \
         USB_EHCD_MAX_FRAMELIST_ENTIRES > uFrameIndex;                            \
         uFrameIndex++)                                                        \
        {                                                                      \
        /* This loop will calculates bandwidth for                             \
           each microframes in a frame */                                      \
                                                                               \
        for (uUFrameIndex = 0;                                                 \
             USB_EHCD_MAX_MICROFRAME_NUMBER > uUFrameIndex;                    \
             uUFrameIndex++)                                                   \
            {                                                                  \
                                                                               \
            /* Copy the bandwidth occupied                                     \
             * in the frame by isochronous endpoints                           \
             */                                                                \
            uBandwidth = IsochBW[uFrameIndex][uUFrameIndex];                   \
                                                                               \
            /* Hold the index of the list into the periodic tree */            \
                                                                               \
            uTreeListIndex                                                     \
            = pHCDData->FrameListData[uFrameIndex].uNextListIndex;             \
                                                                               \
            /* This loop calculates the bandwidth for                          \
               tree list data in the microframe                                \
            */                                                                 \
                                                                               \
            for (; ((int)uTreeListIndex != USB_EHCD_NO_LIST);                  \
                   uTreeListIndex                                              \
                   = pHCDData->TreeListData[uTreeListIndex].                   \
                                            uNextListIndex)                    \
                                                                               \
                {                                                              \
                uBandwidth +=                                                  \
                    InterruptBW[uTreeListIndex][uUFrameIndex];                 \
                }                                                              \
                                                                               \
            pTotalFrameBW[uFrameIndex] += uBandwidth;                          \
            } /* End of loop for each microframe */                            \
        } /* End of loop for each frame */                                     \
                                                                               \
    }



/* function declarations */

IMPORT USBHST_STATUS usbEhcdUnSetupPipe
    (
    pUSB_EHCD_DATA pHCDData,
    pUSB_EHCD_PIPE pHCDPipe
    );

IMPORT USBHST_STATUS usbEhcdSetupPipe
    (
    pUSB_EHCD_DATA pHCDData,
    pUSB_EHCD_PIPE pHCDPipe,
    pUSB_TRANSFER_SETUP_INFO pSetupInfo
    );

IMPORT USBHST_STATUS usbEhcdSetupControlPipe
    (
    pUSB_EHCD_DATA pHCDData,
    pUSB_EHCD_PIPE pHCDPipe
    );

IMPORT USBHST_STATUS usbEhcdResetPipe
    (
    pUSB_EHCD_DATA  pHCDData,
    pUSB_EHCD_PIPE  pHCDPipe
    );

IMPORT VOID usbEhcdDeleteRequestInfo
    (
    pUSB_EHCD_DATA          pHCDData,
    pUSB_EHCD_PIPE          pHCDPipe,
    pUSB_EHCD_REQUEST_INFO  pRequest
    );

IMPORT pUSB_EHCD_REQUEST_INFO usbEhcdCreateRequestInfo
    (
    pUSB_EHCD_DATA  pHCDData,
    pUSB_EHCD_PIPE  pHCDPipe
    );

IMPORT pUSB_EHCD_REQUEST_INFO usbEhcdReserveRequestInfo
    (
    pUSB_EHCD_DATA  pHCDData,
    pUSB_EHCD_PIPE  pHCDPipe,
    pUSBHST_URB     pUrb
    );

IMPORT VOID usbEhcdReleaseRequestInfo
    (
    pUSB_EHCD_DATA  pHCDData,
    pUSB_EHCD_PIPE  pHCDPipe,
    pUSB_EHCD_REQUEST_INFO  pRequest
    );

IMPORT BOOLEAN usbEhcdIsRequestInfoUsed
    (
    pUSB_EHCD_DATA  pHCDData,
    pUSB_EHCD_PIPE  pHCDPipe,
    pUSB_EHCD_REQUEST_INFO  pRequest
    );

IMPORT VOID usbEhcdDestroyAllTDs
    (
    pUSB_EHCD_DATA  pHCDData,
    pUSB_EHCD_PIPE  pHCDPipe
    );

IMPORT pUSB_EHCD_QH usbEhcdFormEmptyQH
    (
    pUSB_EHCD_DATA  pHCDData
    );

IMPORT BOOLEAN usbEhcdAddToFreeQHList
    (
    pUSB_EHCD_DATA  pHCDData,
    pUSB_EHCD_QH    pQH
    );

IMPORT BOOLEAN usbEhcdAddToFreeQTDList
    (
    pUSB_EHCD_DATA  pHCDData,
    pUSB_EHCD_PIPE  pHCDPipe,
    pUSB_EHCD_QTD   pQTD
    );

IMPORT BOOLEAN usbEhcdAddToFreeITDList
    (
    pUSB_EHCD_DATA  pHCDData,
    pUSB_EHCD_PIPE  pHCDPipe,
    pUSB_EHCD_ITD   pITD
    );

IMPORT BOOLEAN usbEhcdAddToFreeSITDList
    (
    pUSB_EHCD_DATA  pHCDData,
    pUSB_EHCD_PIPE  pHCDPipe,
    pUSB_EHCD_SITD  pSITD
    );

IMPORT pUSB_EHCD_QTD usbEhcdGetFreeQTD
    (
    pUSB_EHCD_DATA  pHCDData,
    pUSB_EHCD_PIPE  pHCDPipe
    );

IMPORT pUSB_EHCD_ITD usbEhcdGetFreeITD
    (
    pUSB_EHCD_DATA  pHCDData,
    pUSB_EHCD_PIPE  pHCDPipe
    );

IMPORT pUSB_EHCD_SITD usbEhcdGetFreeSITD
    (
    pUSB_EHCD_DATA  pHCDData,
    pUSB_EHCD_PIPE  pHCDPipe
    );

IMPORT VOID usbEhcdInitQTD
    (
    pUSB_EHCD_DATA  pHCDData,
    pUSB_EHCD_PIPE  pHCDPipe,
    pUSB_EHCD_QTD   pQTD
    );

IMPORT VOID usbEhcdInitITD
    (
    pUSB_EHCD_DATA  pHCDData,
    pUSB_EHCD_PIPE  pHCDPipe,
    pUSB_EHCD_ITD   pITD
    );

IMPORT VOID usbEhcdInitSITD
    (
    pUSB_EHCD_DATA  pHCDData,
    pUSB_EHCD_PIPE  pHCDPipe,
    pUSB_EHCD_SITD  pSITD
    );

IMPORT STATUS usbEhcdDestroyQH
    (
    pUSB_EHCD_DATA  pHCDData,
    pUSB_EHCD_QH    pQH
    );

IMPORT STATUS usbEhcdDestroyQTD
    (
    pUSB_EHCD_DATA   pHCDData,
    pUSB_EHCD_QTD    pQTD
    );

IMPORT STATUS usbEhcdDestroyITD
    (
    pUSB_EHCD_DATA   pHCDData,
    pUSB_EHCD_ITD    pITD
    );

IMPORT STATUS usbEhcdDestroySITD
    (
    pUSB_EHCD_DATA    pHCDData,
    pUSB_EHCD_SITD    pSITD
    );

IMPORT pUSB_EHCD_QTD usbEhcdFormEmptyQTD
    (
    pUSB_EHCD_DATA  pHCDData,
    pUSB_EHCD_PIPE  pHCDPipe
    );

IMPORT pUSB_EHCD_ITD usbEhcdFormEmptyITD
    (
    pUSB_EHCD_DATA  pHCDData,
    pUSB_EHCD_PIPE  pHCDPipe
    );

IMPORT pUSB_EHCD_SITD usbEhcdFormEmptySITD
    (
    pUSB_EHCD_DATA  pHCDData,
    pUSB_EHCD_PIPE  pHCDPipe
    );

IMPORT pUSB_EHCD_QH usbEhcdCreateQH
    (
    pUSB_EHCD_DATA  pHCDData
    );

IMPORT pUSB_EHCD_QTD usbEhcdCreateQTD
    (
    pUSB_EHCD_DATA  pHCDData
    );

IMPORT pUSB_EHCD_ITD usbEhcdCreateITD
    (
    pUSB_EHCD_DATA  pHCDData
    );

IMPORT pUSB_EHCD_SITD usbEhcdCreateSITD
    (
    pUSB_EHCD_DATA  pHCDData
    );

IMPORT VOID usbEhcdUpdateQTDData
    (
    UINT8    index,
    pUSB_EHCD_QTD pHead,
    pUSB_EHCD_QTD pTail,
    pUSBHST_URB pUrb
    );

IMPORT VOID usbEhcdUpdateITDData
    (
    UINT8    index,           /* index of the host controller */
    UINT8    uMicroFrameMask, /* Micro frame mask to be tested */
    pUSB_EHCD_ITD pHead,      /* Pointer to the head QTD */
    pUSB_EHCD_ITD pTail,      /* Pointer to the tail QTD */
    void *        pVoid       /* Pointer for extend */
    );

IMPORT VOID usbEhcdUpdateSITDData
    (
    UINT8    index,
    pUSB_EHCD_SITD pHead,   /* Pointer to the head QTD */
    pUSB_EHCD_SITD pTail,   /* Pointer to the tail QTD */
    void *         pVoid    /* Pointer for extend */
    );

IMPORT VOID usbEhcdUnLinkSITD
    (
    pUSB_EHCD_DATA  pHCDData, /* Pointer to the EHCD_DATA structure */
    pUSB_EHCD_SITD   pSITD     /* Pointer to the tail SITD */
    );

IMPORT VOID usbEhcdUnLinkITD
    (
    pUSB_EHCD_DATA  pHCDData, /* Pointer to the EHCD_DATA structure */
    pUSB_EHCD_ITD   pITD     /* Pointer to the tail SITD */
    );

IMPORT BOOLEAN usbEhcdCopyRHInterruptData
    (
    pUSB_EHCD_DATA  pHCDData,
    UINT32          uStatusChange
    );

IMPORT UINT32 usbEhcdFillQTDBuffer
    (
    UINT8               index,
    pUSB_EHCD_QTD       pQTD,
    bus_addr_t          pBuffer,
    bus_size_t          uSize,
    UINT32              uMaxPacketSize
    );

IMPORT BOOLEAN usbEhcdCreateQTDs
    (
    pUSB_EHCD_DATA  pHCDData,
    pUSB_EHCD_PIPE  pHCDPipe,
    pUSB_EHCD_REQUEST_INFO  pRequest,
    pUSB_EHCD_QTD *ppDataHead,
    pUSB_EHCD_QTD *ppDataTail,
    bus_addr_t    pTransferBuffer,
    bus_size_t    uTransferLength,
    UINT32     uMaximumPacketSize,
    UINT8      uToggle,
    UINT8      uPID
    );

IMPORT VOID usbEhcdLinkITDs
    (
    pUSB_EHCD_DATA  pHCDData,
    pUSB_EHCD_PIPE  pHCDPipe,
    UINT32          uFrameNumber,
    pUSB_EHCD_ITD   pHead,
    pUSB_EHCD_ITD   pTail
    );

IMPORT VOID usbEhcdLinkSITDs
    (
    pUSB_EHCD_DATA  pHCDData,
    pUSB_EHCD_PIPE  pHCDPipe,
    UINT32          uFrameNumber,
    pUSB_EHCD_SITD   pHead,
    pUSB_EHCD_SITD   pTail
    );

IMPORT BOOLEAN usbEhcdGenerateITDs
    (
    pUSB_EHCD_DATA  pHCDData,
    pUSB_EHCD_PIPE  pHCDPipe,
    pUSB_EHCD_REQUEST_INFO  pRequest,
    pUSB_EHCD_ITD   *ppHead,
    pUSB_EHCD_ITD   *ppTail,
    bus_addr_t      pBuffer,
    UINT32          uPktCnt,
    pUSBHST_ISO_PACKET_DESC pIsocPktDesc
    );

IMPORT BOOLEAN usbEhcdGenerateSITDs
    (
    pUSB_EHCD_DATA  pHCDData,
    pUSB_EHCD_PIPE  pHCDPipe,
    pUSB_EHCD_REQUEST_INFO  pRequest,
    pUSB_EHCD_SITD  *ppDataHead,
    pUSB_EHCD_SITD  *ppDataTail,
    bus_addr_t      pTransferBuffer,
    UINT32          uPktCnt,
    pUSBHST_ISO_PACKET_DESC pIsocPktDesc
    );

IMPORT INT32 usbEhcdCalculateBusTime
    (
    UINT32 uSpeed,
    UINT32 uDirection,
    UINT32 uPipeType,
    UINT32 uDataByteCount
    );

IMPORT BOOLEAN usbEhcdCheckBandwidth
    (
    pUSB_EHCD_DATA  pHCDData,
    ULONG       uBandwidth,
    UINT32        uSpeed,
    pUSBHST_ENDPOINT_DESCRIPTOR        pEndpointDesc,
    UINT32 *    puListIndex,
    UINT32 *    puMicroFrameMask
    );

IMPORT USBHST_STATUS usbEhcdSubBandwidth
    (
    pUSB_EHCD_DATA  pHCDData,
    UINT32      uDeviceAddress,
    UINT32      uDeviceSpeed,
    pUSBHST_INTERFACE_DESCRIPTOR pInterfaceDesc,
    UINT32  *   pTotalFrameBW
    );

IMPORT USBHST_STATUS usbEhcdSubDeviceBandwidth
    (
    pUSB_EHCD_DATA  pHCDData,
    UINT32      uDeviceAddress,
    UINT32  *   pTotalFrameBW
    );

IMPORT BOOLEAN usbEhcdAddBandwidth
    (
    UINT32      uDeviceSpeed,
    pUSBHST_INTERFACE_DESCRIPTOR pInterfaceDesc,
    UINT32  *   pTotalFrameBW
    );

IMPORT BOOLEAN usbEhcdUpdateBandwidth
    (
    pUSB_EHCD_DATA pHCDData
    );

IMPORT STATUS usbEhcdAsynchScheduleEnable
    (
    pUSB_EHCD_DATA pHCDData
    );

IMPORT STATUS usbEhcdAsynchScheduleDisable
    (
    pUSB_EHCD_DATA pHCDData
    );

/* To hold the array of pointers to the HCD maintained data structures */

extern pUSB_EHCD_DATA *g_pEHCDData;

/* Number of host controllers present in the system */

extern UINT32  g_EHCDControllerCount;

#ifdef    __cplusplus
}
#endif

#endif /* End of __INCusbEhcdUtilh */
/************************* End of file usbEhcdUtil.h***************************/



