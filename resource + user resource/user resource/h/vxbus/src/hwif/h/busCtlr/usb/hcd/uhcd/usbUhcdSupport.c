/* usbUhcdSupport.c - USB UHCD HCD register access routines */

/*
 * Copyright (c) 2003-2010 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2003-2010 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
Modification history
--------------------
01s,06jul10,m_y  Modify for coding convention
01r,18may10,w_x  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
01q,09mar10,j_x  Changed for USB debug (WIND00184542)
01p,08jan10,y_l  Fixed usbUhcdDelay (taking whole frames only) WIND00190835
01o,13jan10,ghs  vxWorks 6.9 LP64 adapting
01n,04jun08,w_x  Removed usbUhcdJobQueueAdd routine. (WIND00121282 fix)
01m,05sep07,jrp  APIGEN updates
01l,09aug07,jrp  Moving usbUhcdJobQueueAdd to this file
01k,27mar07,sup  Remove the read function added in previous checkin
01j,22mar07,sup  Added a function to read 16 bit register
01i,08oct06,ami  Changes for USB-vxBus changes
01h,28mar05,pdg  non-PCI changes
01g,15oct04,ami  Refgen Changes
01f,11oct04,ami  Apigen Changes
01e,07oct04,mta  Review comment changes
01d,05oct04,mta  SPR100704- Removal of floating point math
01c,28jul03,mat  Endian related changes
01b,26jun03,mat  changing the code to WRS standards.
01a,25apr03,ram  written.
*/

/*
DESCRIPTION

This file contains the fucntions which would be used to access
various register/sub-fields of the UHCD.

INCLUDE FILES:  usb/usbOsal.h, usb/usbHst.h, usbUhci.h,
usbUhcdScheduleQueue.h, usbUhcdSupport.h,
usbUhcdCommon.h, usbUhcdScheduleQueue.h
*/


/*
INTERNAL
 *******************************************************************************
 * Filename         : usbUhcdSupport.c
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      :  This file contains the fucntions which would
 *                     be used to access various register/sub-fields of the UHCD.
 *
 *
 ******************************************************************************/

/* includes */

#include "usb/usbOsal.h"
#include "usb/usbHst.h"
#include "usbUhci.h"
#include "usbUhcdScheduleQueue.h"
#include "usbUhcdSupport.h"
#include "usbUhcdCommon.h"
#include "usbUhcdScheduleQueue.h"

/* global */

/* forward declarations */

/*******************************************************************************
*
* usbUhcdWriteReg - write a value to the UHCD's register.
*
* This routine writes a value to the UHCD's register.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbUhcdWriteReg
    (
    pUSB_UHCD_DATA pHCDData,
    UINT8          usbUhcdRegBaseOffset,
    UINT16         valueToWrite
    )
    {
    /* To hold the value of the register */

    UINT16 sReg= 0;

    /* Read the word from the register at the offset mentioned */

    sReg = USB_UHCD_READ_WORD (pHCDData->pDev, usbUhcdRegBaseOffset);

    /* Update the value which is to be written */

    sReg = sReg | valueToWrite;

    /* Write the value to the register */

    USB_UHCD_WRITE_WORD(pHCDData->pDev, usbUhcdRegBaseOffset, sReg);

    return;

    }/* End of usbUhcdWriteReg */


/***************************************************************************
*
* usbUhcdReadReg - read a value from the UHCD's register.
*
* This routine reads a value from the UHCD's register.
*
* RETURNS: sReadValue
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/

INT16 usbUhcdReadReg
    (
    pUSB_UHCD_DATA pHCDData,
    UINT8          usbUhcdRegBaseOffset
    )
    {
    /* To hold the value of the register */

    UINT16 sReadValue= 0;

    /* Read the word from the register at the offset mentioned */

    sReadValue = USB_UHCD_READ_WORD( pHCDData->pDev, usbUhcdRegBaseOffset);

    return (INT16)sReadValue;

    }/* End of usbUhcdReadReg */


/***************************************************************************
*
* usbUhcdRegxSetBit - sets a bit of the UHCD's register
*
* This routine sets a bit of the UHCD's register.
*
* RETURNS: N/A
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/

void usbUhcdRegxSetBit
    (
    pUSB_UHCD_DATA pHCDData,
    UINT8          usbUhcdRegBaseOffset,
    UINT8          setBit
    )
    {

    /* To hold the value of the register */

    UINT16 regx = 0;

    /* Read the word from the register at the offset mentioned */

    regx = USB_UHCD_READ_WORD (pHCDData->pDev, usbUhcdRegBaseOffset);

    /* Update the value which is to be written */

    regx = (UINT16)(regx | ( 1 << setBit));

    /* Write the value to the register */

    USB_UHCD_WRITE_WORD (pHCDData->pDev, usbUhcdRegBaseOffset, regx);

    }/* End of usbUhcdRegxSetBit() */

/***************************************************************************
*
* usbUhcdRegxClearBit - clears a bit of the UHCD's register
*
* This routine clears a bit of the UHCD's register.
*
* RETURNS: N/A
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/

void usbUhcdRegxClearBit
    (
    pUSB_UHCD_DATA pHCDData,
    UINT8          usbUhcdRegBaseOffset,
    UINT8          clearBit
    )
    {
    /* To hold the value of the register */

    UINT16 regx = 0;

    /* Read the word from the register at the offset mentioned */

    regx = USB_UHCD_READ_WORD ( pHCDData->pDev, usbUhcdRegBaseOffset);

    /* Update the value which is to be written */

    regx = (UINT16)(regx & ~( 1 << clearBit ));

    /* Write the value to the register */

    USB_UHCD_WRITE_WORD (pHCDData->pDev, usbUhcdRegBaseOffset, regx);

    }/* End of usbUhcdRegxClearBit() */

/***************************************************************************
*
* usbUhcdPortSetBit - sets a bit of the UHCD's port register
*
* This function is used to set a bit of the UHCD's port regsiter
*
* RETURNS: N/A
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/

void usbUhcdPortSetBit
    (
    pUSB_UHCD_DATA pHCDData,
    UINT8          usbUhcdRegBaseOffset,
    UINT8          setBit
    )
    {
    /* To hold the value of the register */

    UINT16 regx = 0;

    /* Read the word from the register at the offset mentioned */

    regx = USB_UHCD_READ_WORD ( pHCDData->pDev, usbUhcdRegBaseOffset);

    /* Update the value which is to be written */

    regx = (UINT16)((regx & 0xFFF5) | ( 1 << setBit ));

    /* Write the value to the register */

    USB_UHCD_WRITE_WORD (pHCDData->pDev, usbUhcdRegBaseOffset, regx);

    }/* End of usbUhcdPortSetBit() */


/***************************************************************************
*
* usbUhcdPortClearBit - clears a bit of the UHCD's port register
*
* This routine sets a bit of the UHCD's port register.
*
* RETURNS: N/A
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/
void usbUhcdPortClearBit
    (
    pUSB_UHCD_DATA pHCDData,
    UINT8          usbUhcdRegBaseOffset,
    UINT8          clearBit
    )
    {

    /* To hold the value of the register */

    UINT16 regx = 0;

    /* Read the word from the register at the offset mentioned */

    regx = USB_UHCD_READ_WORD ( pHCDData->pDev, usbUhcdRegBaseOffset);

    /* Mask the specific fields so that clearing is not a problem */

    regx = (UINT16)(regx & 0xFFF5);

    /* Bits 1 and 3 can be cleared by writing a 1 */
    if ( clearBit == 1 || clearBit == 3)
        {
        regx = (UINT16)(regx | (1 << clearBit));
        }

    /* The remaining bits will be cleared by writing 0 */

    else
        {
        regx = (UINT16)(regx & ~( 1 << clearBit ));
        }

    /* Write the value to the register */

    USB_UHCD_WRITE_WORD( pHCDData->pDev, usbUhcdRegBaseOffset, regx);

    }/* End of usbUhcdPortClearBit() */

/***************************************************************************
*
* usbUhcdIsBitSet - check whether a bit is set of the UHCD's register
*
* This routine checks whether a bit is set of the UHCD's register.
*
* RETURNS: TRUE, FALSE if bit is not set
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/

BOOLEAN usbUhcdIsBitSet
    (
    pUSB_UHCD_DATA pHCDData,
    UINT8          usbUhcdRegBaseOffset,
    UINT8          whichBit
    )
    {

    /* To hold the value of the register */

    UINT16 regx = 0;


    /* Read the word from the register at the offset mentioned */

    regx = USB_UHCD_READ_WORD( pHCDData->pDev, usbUhcdRegBaseOffset);

    /* Return the value of the bit in the specified bit position */

    return (BOOLEAN)(((regx >> whichBit) & 0x01) == 1);

    }/* End of usbUhcdIsBitSet() */


/***************************************************************************
*
* usbUhcdIsBitReset - check whether a bit is reset of the UHCD's register
*
* This routine checks whether a bit is reset of the UHCD's register.
*
* RETURNS: TRUE, FALSE if bit is not reset
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/

BOOLEAN usbUhcdIsBitReset
    (
    pUSB_UHCD_DATA pHCDData,
    UINT8          usbUhcdRegBaseOffset,
    UINT8          whichBit
    )
    {

    /* To hold the value of the register */

    UINT16 regx = 0;

    /* Read the word from the register at the offset mentioned */

    regx = USB_UHCD_READ_WORD( pHCDData->pDev, usbUhcdRegBaseOffset);

    /* Return the value of the bit in the specified bit position */

    return (BOOLEAN)(((regx >> whichBit) & 0x01) == 0);

    }/* End of usbUhcdIsBitReset) */


/***************************************************************************
*
* usbUhcdGetBit - get the bit value of the of the UHCD's register
*
* This routine gets the bit value of the of the UHCD's register.
*
* RETURNS: UINT8, the value of the register
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/

UINT8 usbUhcdGetBit
    (
    pUSB_UHCD_DATA pHCDData,
    UINT8          usbUhcdRegBaseOffset,
    UINT8          whichBit
    )
    {

    /* To hold the value of the register */

    UINT16 regx = 0;

    /* Read the word from the register at the offset mentioned */

    regx = USB_UHCD_READ_WORD( pHCDData->pDev, usbUhcdRegBaseOffset);

    /* Return the value of the bit in the specified bit position */

    return (UINT8)((regx >> whichBit) & 0x01);

    }/* End of usbUhcdGetBit() */


/***************************************************************************
*
* usbUhcdHcReset - resets the HC
*
* This routine resets the HC.
*
* RETURNS: N/A
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/

void usbUhcdHcReset
    (
    pUSB_UHCD_DATA pHCDData
    )
    {

    /* To hold the wait count */

    UINT32 wait_count = 0;

    /* Set bit 1 in USBCMD register to reset the HC */

    usbUhcdRegxSetBit (pHCDData, USB_UHCD_USBCMD, 1);

    /* Wait for reset to complete */

    /* About 64 bit times after HCReset goes to 0, the connect and low-speed
     * detect will take place and bits 0 and 8 of the PORTSC will change
     * accordingly.
     */

    for (wait_count = 0;
         wait_count < USB_UHCD_NUM_OF_RETRIES &&
         usbUhcdIsBitSet (pHCDData, USB_UHCD_USBCMD, 1); wait_count++);

    /* Check if reset was not successful */
    if (wait_count == USB_UHCD_NUM_OF_RETRIES )
        {
        USB_UHCD_ERR("%s[L%d]: unable to reset UHCI host controller \n",
                      __FILE__, __LINE__, 3, 4, 5, 6);

        return;
        }

    }/* End of usbUhcdHcReset() */


/***************************************************************************
*
* usbUhciDelay - resets the HC
*
* Sends a reset signal to the Host Controller
*
* RETURNS: N/A
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/

void usbUhciDelay
    (
    pUSB_UHCD_DATA pHCDData,
    UINT16         uDelay
    )
    {
    /* To hold the frame number */
    UINT16  uFrameNumber, uCurrentFrameNumber;

    /* Compute the current delay */
    UINT16  uCurrentDelay = 0;

    /*
     * Force Function to wait at least 1 ms more since we have to guarantee
     * at least 1 whole frame for 1 ms. If we are at the end of a frame,
     * We will wait less than the given time.
     */
    uDelay ++ ;

    /* Read the SOF before giving a reset */
    uFrameNumber =
        (UINT16)(USB_UHCD_READ_WORD( pHCDData->pDev, USB_UHCD_FRNUM) & 0x3FF);

    /* Initialise Current frmae number to frame number */
    uCurrentFrameNumber =  uFrameNumber;

    /* Wait till the delay is reached */
    while (uCurrentDelay < uDelay)
        {
        /* Read the current frame number */
        uCurrentFrameNumber =
            (UINT16)(USB_UHCD_READ_WORD( pHCDData->pDev, USB_UHCD_FRNUM) & 0x3FF);

        /* Compute the current delay */
        if (uCurrentFrameNumber >= uFrameNumber)
            {
            uCurrentDelay = (UINT16)(uCurrentFrameNumber - uFrameNumber);
            }
        else
            {
            uCurrentDelay = (UINT16)(1023 - uFrameNumber + uCurrentFrameNumber);
            }
        }

    return;

}

/***************************************************************************
*
* usbUhciLog2 - Function to return log to the base 2
*
* Returns the log value to the base 2
*
* RETURNS: UINT32 , log value to the base 2
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/

UINT32 usbUhciLog2
    (
    UINT32     value
    )
    {
    int i=0;
    UINT32 tempElement = value;

    for (i=0; i<32; i++)
        {
        if (tempElement==0)
            break;
        tempElement = (tempElement>>1);
        }

    if(i)
        return (UINT32)(i -1) ;
    else
        return 0;

    }

/********************* End of File usbUhcdSupport.c ***************************/


