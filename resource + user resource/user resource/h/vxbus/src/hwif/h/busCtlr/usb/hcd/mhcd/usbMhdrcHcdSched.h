/* usbMhdrcScheduleProcess.h - schedule process of Mentor Graphics HCD */

/*
 * Copyright (c) 2009 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01d,16aug10,w_x  VxWorks 64 bit audit and warning removal
01c,13mar10,s_z  Add Inventra DMA supported on OMAP3EVM
01b,25feb10,s_z  Rename request list in USB_MHCD_DATA structure(WIND00201709)
01a,27oct09,s_z  written
*/

/*
DESCRIPTION

This file contains the function declarations for process schedule.
*/

#ifndef __INCusbMhdrcScheduleProcessh
#define __INCusbMhdrcScheduleProcessh

#ifdef __cplusplus
extern "C" {
#endif


/* imports */

IMPORT void usbMhdrcHcdEp0InterruptHandle
    (
    pUSB_MHCD_DATA pHCDData
    );

IMPORT void usbMhdrcHcdTxInterruptHandle
    (
    pUSB_MHCD_DATA pHCDData,
    UINT8          uEndpointNumber
    );

IMPORT void usbMhdrcHcdRxInterruptHandle
    (
    pUSB_MHCD_DATA pHCDData,
    UINT8          uEndpointNumber
    );

IMPORT void usbMhdrcHcdProcessScheduleHandler
    (
    pUSB_MHCD_DATA pHCDData
    );

IMPORT void usbMhdrcHcdTransferManagementTask
    (
    pUSB_MHCD_DATA pHCDData
    );

IMPORT void usbMhdrcHcdTransferTaskFeed
    (
    pUSB_MHCD_DATA          pHCDData,
    pUSB_MHCD_REQUEST_INFO  pRequestInfo,
    UINT32                  uCmdCode,
    USBHST_STATUS           uCompleteStatus
    );

IMPORT void usbMhdrcHcdRequestTransferDone
    (
    pUSB_MHCD_DATA         pHCDData,
    pUSB_MHCD_REQUEST_INFO pRequestInfo,
    USBHST_STATUS          uCompleteStatus
    );

IMPORT void usbMhdrcHcdRequestCompleteProcess
    (
    pUSB_MHCD_DATA         pHCDData,
    pUSB_MHCD_REQUEST_INFO pRequestInfo,
    USBHST_STATUS          uCompleteStatus
    );

/* Macro to get the data pid toggle */

#define usbMhdrcHcdGetDataToggle(pHCDPipe)      \
    ((pHCDPipe->uDataToggle == 1) ? 1 : 0)

/* Macro to set the data pid toggle, tick it */

#define usbMhdrcHcdTickDataToggle(pHCDPipe)     \
    (pHCDPipe->uDataToggle = (UINT8)        \
     ((pHCDPipe->uDataToggle == 1) ? 0 : 1))\


#ifdef __cplusplus
}
#endif

#endif

