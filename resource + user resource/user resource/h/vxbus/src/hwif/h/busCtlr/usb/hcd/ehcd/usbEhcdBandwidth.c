/* usbEhcdBandwidth.c - contains the bandwidth functions of EHCD */

/*
 * Copyright (c) 2002-2006, 2009-2011, 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2002-2006, 2009-2011, 2013 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
modification history
--------------------
01l,09may13,wyy  Adjust endpoint count before get the first EP DESP in function
                 "usbEhcdAddBandwidth" (WIND00414101)
01k,17aug11,ghs  Check pHCDPipe valid before using pQH node (WIND00286527)
01j,18may10,w_x  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
01i,09mar10,j_x  Changed for USB debug (WIND00184542)
01h,13jan10,ghs  vxWorks 6.9 LP64 adapting
01g,15jul09,ghs  Fix for WIND00171264, remove align defines
01f,18jul06,sup  Fix for SPR#108289
01e,28mar05,pdg  non-PCI changes
01d,22mar05,mta  64-bit Support Added (SPR #104950)
01c,03dec04,ami  Merged IP Changes
01b,26jun03,gpd  changing the code to WRS standards.
01a,25apr02,ram  written.
*/

/*
DESCRIPTION

This module defines the bandwidth related functions for the
EHCI Host Controller Driver.

INCLUDE FILES: usbOsal.h, usbOsalDebug.h, usbHst.h, usbEhcdDataStructures.h,
 usbEhcdUtil.h, usbEhcdDebug.h,

SEE ALSO:
<USB specification, revision 2.0>
<EHCI specification, revision 1.0>

*/

/*
 INTERNAL
 *******************************************************************************
 *
 * Filename         : usbEhcdBandwidth.c
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 * Description      :  This contains the functions which handle the bandwidth
 * related requests by the EHCI Driver.
 *
 *
 ******************************************************************************/

/* includes */

#include "usb/usbOsal.h"
#include "usb/usbOsalDebug.h"
#include "usb/usbHst.h"
#include "usbEhcdDataStructures.h"
#include "usbEhcdUtil.h"

/*******************************************************************************
*
* usbEhcdSubBandwidth - subtract the bw utilised by each endpoint
*
* This routine is used to subtract the bw utilised by each endpoint
* and returns the updated bandwidth array.
*
* <pHCDData> - Pointer to the EHCD_DATA structure.
* <uDeviceAddress> - Device Address.
* <uSpeed> - Device Speed Device Speed of the transfer
* <pInterfaceDesc>  - Pointer to the interface desc.
* <pFrameBW> - Array of bandwidth list
*
* RETURNS: USBHST_SUCCESS - Returned if success.
*          USBHST_INSUFFICIENT_MEMORY - Returned if the memory
*                                       allocation for failed.
*
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbEhcdSubBandwidth
    (
    pUSB_EHCD_DATA  pHCDData,       /* Pointer to the EHCD_DATA structure */
    UINT32          uDeviceAddress, /* Device Address */
    UINT32          uSpeed,         /* Device Speed of the transfer */
    pUSBHST_INTERFACE_DESCRIPTOR pInterfaceDesc, /* Pointer to the interface desc */
    UINT32  *       pTotalFrameBW   /* Array of bandwidth list */
    )
    {
    /* To hold the micro frame index */

    UINT32 uUFrameIndex = 0;

    /* To hold the frame index in the frame list */

    UINT32 uFrameIndex = 0;

    /* To hold the supported poll interval */

    UINT32 uPollInterval = 0;

    /* To hold the end point descriptor */

    pUSBHST_ENDPOINT_DESCRIPTOR pEPDesc = NULL;

    /* Structure to hold the bandwidth details */

    struct _bandwidth_
        {
        UINT32 InterruptBW[USB_EHCD_MAX_TREE_NODES][USB_EHCD_MAX_MICROFRAME_NUMBER];
        UINT32 IsochBW[USB_EHCD_MAX_FRAMELIST_ENTIRES][USB_EHCD_MAX_MICROFRAME_NUMBER];
        } *pBandwidth;

    /* To hold the count */

    UINT32 uCount;

    /* To hold the total nos of endpoint */

    UINT32 uEPCount;

    /* Allocate memory for bandwidth pointer for interrupt and isoch */

    pBandwidth = (struct _bandwidth_ *)OS_MALLOC(sizeof(struct _bandwidth_));

    if (NULL == pBandwidth)
        {
        USB_EHCD_ERR("usbEhcdSubBandwidth -"
            " Memory not allocated for the bandwidth\n", 0, 0, 0, 0, 0, 0);

        return USBHST_MEMORY_NOT_ALLOCATED;
        }

    /* Initialize the element of array to zero */

    OS_MEMSET(pBandwidth, 0, sizeof(struct _bandwidth_));

    /* Exclusively access the bandwidth resource */

    OS_WAIT_FOR_EVENT(pHCDData->BandwidthEventID, OS_WAIT_INFINITE);

    /* Get the bandwidth used for isochronous in frame list */

    USB_EHCD_GET_ISOCH_BANDWIDTH(pHCDData, pBandwidth->IsochBW);

    /* Get the bandwidth used for isochronous in frame list */

    USB_EHCD_GET_INTERRUPT_BANDWIDTH(pHCDData, pBandwidth->InterruptBW);

    /* Release the bandwidth exclusive access */

    OS_RELEASE_EVENT(pHCDData->BandwidthEventID);

    /* Get the total no of endpoint for this interface */

    uEPCount = pInterfaceDesc->bNumEndpoints;

    pEPDesc = (pUSBHST_ENDPOINT_DESCRIPTOR)
        ((UINT8 *)(pInterfaceDesc) + pInterfaceDesc->bLength);

    while (USBHST_ENDPOINT_DESC != pEPDesc->bDescriptorType)
        {
        pEPDesc = (pUSBHST_ENDPOINT_DESCRIPTOR)
            ((UINT8 *)(pEPDesc) + pEPDesc->bLength);
        }

    for (uCount = 0; uCount < uEPCount; uCount++)
        {

        /* Get the polling interval of the endpoint */

        uPollInterval = pEPDesc->bInterval;

        if (USBHST_INTERRUPT_TRANSFER ==
            (pEPDesc->bmAttributes & USB_EHCD_ENDPOINT_TYPE_MASK))
            {
            UINT32 uTempVariable = 0;
            UINT32 uTreeListIndex = 0;
            UINT32 uTreeNodeCount;

            if (USBHST_HIGH_SPEED == uSpeed)
                {
                /*
                 * Calculate the updated polling interval. This is in
                 * units of microframes 2^n
                 */

                uPollInterval = (UINT32)(1 << (uPollInterval - 1));

                USB_EHCD_GET_POLL_INTERVAL(uPollInterval);
                }
            else if (USB_EHCD_MAX_USB11_INTERRUPT_POLL_INTERVAL < uPollInterval)
                {
                uPollInterval = USB_EHCD_MAX_USB11_INTERRUPT_POLL_INTERVAL;
                }

            /* Get the starting node in the tree for this interval */

            uTempVariable = (UINT8)uPollInterval;

            uTreeListIndex = 1;

            while (uTempVariable >>= 1)
                {
                uTreeListIndex = (UINT32)(uTreeListIndex << 1);
                }

            uTreeListIndex--;

            for (uTreeNodeCount = uTreeListIndex;
                 (uPollInterval + uTreeListIndex) > uTreeNodeCount;
                 uTreeNodeCount++)
                {
                BOOLEAN bNodeFound = FALSE;

                pUSB_EHCD_QH pQH =
                    pHCDData->TreeListData[uTreeNodeCount].pHeadPointer;

                if (NULL == pQH->pNext)
                    {
                    continue;
                    }

                do
                    {
                    /*
                     * Get the first active QH pointer in first iteration
                     * then get next pointer
                     */

                    pQH = pQH->pNext;

                    if (pQH->pHCDPipe == NULL)
                        break;

                    if ((uDeviceAddress == pQH->pHCDPipe->uAddress) &&
                        (pQH->pHCDPipe->uEndpointAddress ==
                         pEPDesc->bEndpointAddress))
                        {
                        for (uUFrameIndex = 0;
                              USB_EHCD_MAX_MICROFRAME_NUMBER > uUFrameIndex;
                              uUFrameIndex++)
                            {
                            /*
                             * Check if bandwidth is to be updated for this
                             * microframe and update the bandwidth
                             */

                            if (0 !=
                                ((pQH->pHCDPipe->uUFrameMaskValue >> uUFrameIndex) & 0x01))
                                {
                                pBandwidth->InterruptBW[uTreeNodeCount][uUFrameIndex]
                                    -= (UINT32)pQH->pHCDPipe->uBandwidth;
                                }
                            }

                        bNodeFound = TRUE;

                        break;
                        }
                    }while (pQH !=
                        pHCDData->TreeListData[uTreeNodeCount].pTailPointer);

                if (bNodeFound)
                    {
                    break;
                    }
                }
            }
        else if (USBHST_ISOCHRONOUS_TRANSFER ==
                (pEPDesc->bmAttributes & USB_EHCD_ENDPOINT_TYPE_MASK))
            {
            /* To hold the isochronous pipe */

            pUSB_EHCD_PIPE pHCDPipe = NULL;

            /* Get pointer of the isochronous pipe list */

            for (pHCDPipe = pHCDData->pIsochPipeList;
                 NULL != pHCDPipe;
                 pHCDPipe = pHCDPipe->pNext)
                {
                if ((uDeviceAddress == pHCDPipe->uAddress) &&
                   (pHCDPipe->uEndpointAddress == pEPDesc->bEndpointAddress))
                    {
                    /*
                     * This loop will remove the bandwidth
                     * allocated in each of the frames
                     */

                    for (uFrameIndex = pHCDPipe->uListIndex;
                         USB_EHCD_MAX_FRAMELIST_ENTIRES > uFrameIndex;
                         uFrameIndex++)
                        {
                        /*
                         * This loop will update the frame list bandwidth
                         * for every microframe
                         */

                        for (uUFrameIndex = 0;
                             USB_EHCD_MAX_MICROFRAME_NUMBER > uUFrameIndex;
                             uUFrameIndex++)
                            {
                            /*
                             * Check if bandwidth is to be updated for this
                             * microframe and update the bandwidth
                             */

                            if (0 !=
                               ((pHCDPipe->uUFrameMaskValue >> uUFrameIndex) & 0x01))
                                {
                                pBandwidth->IsochBW[uFrameIndex][uUFrameIndex] -=
                                    (UINT32)pHCDPipe->uBandwidth;
                                }
                            } /* End of loop for each microframe */
                        } /* End of loop for each frame */
                        break; /* Pipe found */
                    } /* End of if */
                } /* End of loop for pipe search */
            }/* end of else if */
        pEPDesc = (pUSBHST_ENDPOINT_DESCRIPTOR)
                ((UINT8 *)(pEPDesc) + pEPDesc->bLength);
        } /* end of loop for each endpoint */

    /* Get the bandwidth per frame in the frame list */

    USB_EHCD_GET_FRAME_BANDWIDTH (pHCDData,
        pBandwidth->IsochBW,
        pBandwidth->InterruptBW,
        pTotalFrameBW);

    /* free the memory allocated */

    OS_FREE(pBandwidth);

    return USBHST_SUCCESS;

    }  /* End of function */


/*******************************************************************************
*
* usbEhcdSubDeviceBandwidth - subtract the bw utilised by each device
*
* This routine is used to subtract the bw utilised by each endpoint
* and returns the updated bandwidth array
*
* <pHCDData> - Pointer to the EHCD_DATA structure.
* <uDeviceAddress> - Device Address.
* <pInterfaceDesc>  - Pointer to the interface desc.
* <pFrameBW> - Array of bandwidth list
*
* RETURNS: USBHST_SUCCESS - Returned if success.
*          USBHST_INSUFFICIENT_MEMORY - Returned if the memory
*                                       allocation for failed.
*
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbEhcdSubDeviceBandwidth
    (
    pUSB_EHCD_DATA  pHCDData,       /* Pointer to the EHCD_DATA structure */
    UINT32          uDeviceAddress, /* Device Address */
    UINT32  *       pTotalFrameBW   /* Array of bandwidth list */
    )
    {
    /* To hold the micro frame index */

    UINT32 uUFrameIndex = 0;

    /* To hold the frame index in the frame list */

    UINT32 uFrameIndex = 0;

    /* To hold the tree node index in the frame list */

    UINT32 uTreeNodeCount;

    /* To hold the isochronous pipe */

    pUSB_EHCD_PIPE pHCDPipe = NULL;

    /* structure to hold the bandwidth details */

    struct _bandwidth_
        {
        UINT32 InterruptBW[USB_EHCD_MAX_TREE_NODES][USB_EHCD_MAX_MICROFRAME_NUMBER];
        UINT32 IsochBW[USB_EHCD_MAX_FRAMELIST_ENTIRES][USB_EHCD_MAX_MICROFRAME_NUMBER];
        } *pBandwidth;

    /* Allocate memory for bandwidth pointer for interrupt and isoch */

    pBandwidth = (struct _bandwidth_ *)OS_MALLOC(sizeof(struct _bandwidth_));

    if (NULL == pBandwidth)
        {
        USB_EHCD_ERR(
            "usbEhcdSubDeviceBandwidth "
            "Memory not allocated for the bandwidth\n", 0, 0, 0, 0, 0, 0);

        return USBHST_MEMORY_NOT_ALLOCATED;
        }

    /* Initialize the element of array to zero */

    OS_MEMSET(pBandwidth, 0, sizeof(struct _bandwidth_));

    /* Exclusively access the bandwidth resource */

    OS_WAIT_FOR_EVENT(pHCDData->BandwidthEventID,OS_WAIT_INFINITE);

    /* Get the bandwidth used for isochronous in frame list */

    USB_EHCD_GET_ISOCH_BANDWIDTH(pHCDData, pBandwidth->IsochBW);

    /* Get the bandwidth used for isochronous in frame list */

    USB_EHCD_GET_INTERRUPT_BANDWIDTH(pHCDData, pBandwidth->InterruptBW);

    /* Release the bandwidth exclusive access */

    OS_RELEASE_EVENT(pHCDData->BandwidthEventID);

    /* Subtract bandwidth for each tree node */

    for (uTreeNodeCount =0;
         USB_EHCD_MAX_TREE_NODES > uTreeNodeCount;
         uTreeNodeCount++)
        {
        /* Initialize the pQH */

        pUSB_EHCD_QH pQH = pHCDData->TreeListData[uTreeNodeCount].pHeadPointer;

        /* If there is no QH attached here, only dummy QH present */

        if ((pHCDData->TreeListData[uTreeNodeCount].pTailPointer == (pVOID)pQH)
            || (NULL == pQH->pNext))
            {
            continue;
            }

        do
            {
            /*
             * Get the first active QH pointer in first iteration
             * then get next pointer
             */

            pQH = pQH->pNext;

            if (uDeviceAddress == pQH->pHCDPipe->uAddress)
                {
                for (uUFrameIndex = 0;
                    USB_EHCD_MAX_MICROFRAME_NUMBER > uUFrameIndex;
                    uUFrameIndex++)
                    {
                    /*
                     * Check if bandwidth is to be updated for this
                     * microframe and update the bandwidth
                     */

                    if (0 != ((pQH->pHCDPipe->uUFrameMaskValue >> uUFrameIndex) & 0x01))
                        {
                        pBandwidth->InterruptBW[uTreeNodeCount][uUFrameIndex] -=
                            (UINT32)pQH->pHCDPipe->uBandwidth;
                        }
                    } /* End of for() */
                } /* End of if (uDevice...) */
            } while (pQH != pHCDData->TreeListData[uTreeNodeCount].pTailPointer);
        }

    /* Get pointer of the isochronous pipe list */

    for (pHCDPipe = pHCDData->pIsochPipeList;
         NULL != pHCDPipe;
         pHCDPipe = pHCDPipe->pNext)
        {
        if (uDeviceAddress == pHCDPipe->uAddress)
            {
            /*
             * This loop will remove the bandwidth
             * allocated in each of the frames
             */

            for (uFrameIndex = pHCDPipe->uListIndex;
                 USB_EHCD_MAX_FRAMELIST_ENTIRES > uFrameIndex;
                 uFrameIndex++)
                {
                /*
                 * This loop will update the frame list bandwidth
                 * for every microframe
                 */

                for (uUFrameIndex = 0;
                     USB_EHCD_MAX_MICROFRAME_NUMBER > uUFrameIndex;
                     uUFrameIndex++)
                    {
                    /*
                     * Check if bandwidth is to be updated for this
                     * microframe and update the bandwidth
                     */

                    if (0 !=
                        ((pHCDPipe->uUFrameMaskValue >> uUFrameIndex) & 0x01))
                        {
                        pBandwidth->IsochBW[uFrameIndex][uUFrameIndex] -=
                            (UINT32)pHCDPipe->uBandwidth;
                        }
                    } /* End of loop for each microframe */
                } /* End of loop for each frame */
            } /* End of if */
        } /* End of loop for pipe search */

    /* Get the bandwidth per frame in the frame list */

    USB_EHCD_GET_FRAME_BANDWIDTH(pHCDData,
        pBandwidth->IsochBW,
        pBandwidth->InterruptBW,
        pTotalFrameBW);

    /* free the memory allocated */

    OS_FREE(pBandwidth);

    return USBHST_SUCCESS;
    }  /* End of function */


/*******************************************************************************
*
* usbEhcdAddBandwidth - check and allocate the bandwidth availability
*
* This routine is used to check whether bandwidth is available and returns the
* updated bandwidth array
*
* <uDeviceSpeed> - Device Speed of the transfer.
* <pInterfaceDesc>  - Pointer to the interface desc.
* <pFrameBW> - Array of bandwidth list
*
* RETURNS: TRUE if the bandwidth can be accomodated.
*          FALSE if the bandwidth cannot be accomodated.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOLEAN usbEhcdAddBandwidth
    (
    UINT32      uDeviceSpeed,     /* Device Speed of the transfer */
    pUSBHST_INTERFACE_DESCRIPTOR pInterfaceDesc, /* Pointer to the interface desc */
    UINT32  *   pTotalFrameBW     /* Array of bandwidth list */
    )
    {
    /* To hold the maximum packet size for the endpoint */

    UINT32 uMaxPacketSize = 0;

    /* To hold the no. of micro frame in a frame */

    UINT32 uMicroFrameCount = 0;

    /* To hold the frame index inthe frame list */

    UINT32 uFrameIndex = 0, uFrameListIndex = 0;

    /* To hold the end point descriptor */

    pUSBHST_ENDPOINT_DESCRIPTOR pEPDesc = NULL;

    /* To hold the count and end point count */

    UINT32 uCount, uEPCount;

    /* To hold the supported poll interval */

    UINT32 uPollInterval = 0;

    /* To hold the bandwidth */

    UINT32 uBandwidth = 0;

    /*
     * If the device speed is high speed, the payload is
     * equal the maxpacketsize * number of packets that can
     * be sent in a microframe.
     * The bits 0 to 10 give the maximum packet size. The bits 11 and 12
     * give the (number of packets in a microframe - 1).
     */

    uEPCount = pInterfaceDesc->bNumEndpoints;

    pEPDesc = (pUSBHST_ENDPOINT_DESCRIPTOR)
        ((UINT8 *)(pInterfaceDesc) + pInterfaceDesc->bLength);

    uCount = uEPCount;

    while (USBHST_ENDPOINT_DESC != pEPDesc->bDescriptorType && uCount--)
        {
        pEPDesc = (pUSBHST_ENDPOINT_DESCRIPTOR)
            ((UINT8 *)(pEPDesc) + pEPDesc->bLength);
        }

    for (uCount = 0; uCount < uEPCount; uCount++)
        {
        /* Get the polling interval of the endpoint */

        uPollInterval = pEPDesc->bInterval;

        if (USBHST_HIGH_SPEED == uDeviceSpeed)
            {
            uMaxPacketSize =
                (UINT32)((OS_UINT16_LE_TO_CPU(pEPDesc->wMaxPacketSize) &
                USB_EHCD_ENDPOINT_MAX_PACKET_SIZE_MASK) *
                (((OS_UINT16_LE_TO_CPU(pEPDesc->wMaxPacketSize) &
                USB_EHCD_ENDPOINT_NUMBER_OF_TRANSACTIONS_MASK) >> 11) + 1));

            /* Get the polling interval */

            uPollInterval = (UINT32)(1 << (uPollInterval - 1));

            /* Get the no. of micro frame to be scheduled in a frame */

            if (0 == (uPollInterval % USB_EHCD_MAX_MICROFRAME_NUMBER))
                {
                /* The endpoint is to be polled one microframe in the frame */

                uMicroFrameCount = 1;
                }
            else
                {
                uMicroFrameCount = USB_EHCD_MAX_MICROFRAME_NUMBER /
                    (uPollInterval % USB_EHCD_MAX_MICROFRAME_NUMBER);
                }
            }

        /*
         * If it is low or full speed, the maximum packet size is the same
         * as that retrieved from the endpoint descriptor
         */

        else
            {
            uMaxPacketSize = OS_UINT16_LE_TO_CPU(pEPDesc->wMaxPacketSize);

            /* If it is an Out end point */

            if (0 == (pEPDesc->bEndpointAddress & USB_EHCD_DIR_IN))
                {
                if (USBHST_INTERRUPT_TRANSFER ==
                    (pEPDesc->bmAttributes & USB_EHCD_ENDPOINT_TYPE_MASK))
                    {
                    /*
                     * Only one start split and 3 complete split
                     * will be scheduled in one frame
                     */

                    uMicroFrameCount = 4;
                    }
                else
                    {
                    /* Only one start split will be scheduled in one frame */

                    uMicroFrameCount = 1;
                    }
                }
            else
                /* If it is an In end point */
                {
                /*
                 * Only one start split and 3 complete split
                 * will be scheduled in one frame
                 */

                uMicroFrameCount = 4;
                }
            }

        /* Calculate the bandwidth which is required for this pipe */

        uBandwidth = (UINT32)usbEhcdCalculateBusTime(USBHST_HIGH_SPEED,
                (UINT32)((pEPDesc->bEndpointAddress) & USB_EHCD_DIR_IN),
                (UINT32)((pEPDesc->bmAttributes) & USB_EHCD_ENDPOINT_TYPE_MASK),
                uMaxPacketSize);

        /* Bandwidth required for each frame */

        uBandwidth *= uMicroFrameCount;

        if (USBHST_ISOCHRONOUS_TRANSFER ==
            (pEPDesc->bmAttributes & USB_EHCD_ENDPOINT_TYPE_MASK))
            {
            /* To hold the max bandwidth utilised */

            UINT32 uMaxBandwidth = 0;

            for (uFrameIndex = 0;
                 USB_EHCD_MAX_FRAMELIST_ENTIRES > uFrameIndex;
                 uFrameIndex++)
                {

                /* Add the current BW required in each frame*/

                pTotalFrameBW[uFrameIndex] += uBandwidth;

                /* Get the maximum bandwidth used in a frame */

                if (pTotalFrameBW[uFrameIndex] > uMaxBandwidth)
                    {
                    uMaxBandwidth = pTotalFrameBW[uFrameIndex];
                    }

                } /* End of for () */

            /* If there is no bandwidth available, return an error */

            if ((USB_EHCD_EIGHTY_PERCENT_BW * USB_EHCD_MAX_MICROFRAME_NUMBER) <
                 uMaxBandwidth)
                {
                USB_EHCD_ERR("usbEhcdCheckBandwidth -"
                    " Bandwidth not available\n", 0, 0, 0, 0, 0, 0);

                return FALSE;
                }
            }
        else if(USBHST_INTERRUPT_TRANSFER ==
             (pEPDesc->bmAttributes & USB_EHCD_ENDPOINT_TYPE_MASK))
            {
            /* Initialize with the min bandwidth */

            UINT32 uMinBandwidth = pTotalFrameBW[0];

            if (USBHST_HIGH_SPEED == uDeviceSpeed)
                {
                /* Calculate the polling interval in terms of frames */

                if (0 != (uPollInterval % USB_EHCD_MAX_MICROFRAME_NUMBER))
                    {
                    uPollInterval = 1;
                    }
                else
                    {
                    uPollInterval /= USB_EHCD_MAX_MICROFRAME_NUMBER;
                    }
                }

            /*
             * If the pollling intervalis greater than supported
             * then copy the max only
             */

            if (USB_EHCD_MAX_USB11_INTERRUPT_POLL_INTERVAL < uPollInterval)
                {
                uPollInterval = USB_EHCD_MAX_USB11_INTERRUPT_POLL_INTERVAL;
                }

            for (uFrameIndex = 0; uPollInterval > uFrameIndex; uFrameIndex++)
                {
                /* Get the minimum bandwidth used in a frame */

                if (pTotalFrameBW[uFrameIndex] < uMinBandwidth)
                    {
                    uMinBandwidth = pTotalFrameBW[uFrameIndex];
                    uFrameListIndex = uFrameIndex;
                    }
                }

            for (uFrameIndex = uFrameListIndex;
                 USB_EHCD_MAX_FRAMELIST_ENTIRES > uFrameIndex;
                 uFrameIndex+=uPollInterval)
                {
                pTotalFrameBW[uFrameIndex] += uBandwidth;

                /* If there is no bandwidth available, return an error */

                if ((USB_EHCD_EIGHTY_PERCENT_BW * USB_EHCD_MAX_MICROFRAME_NUMBER) <
                    pTotalFrameBW[uFrameIndex])
                    {
                    USB_EHCD_ERR(
                        "usbEhcdAddBandwidth -"
                        " Bandwidth not available\n", 0, 0, 0, 0, 0, 0);

                    return FALSE;
                    }
                }
            }/* end of Interrupt */
        pEPDesc = (pUSBHST_ENDPOINT_DESCRIPTOR)
            ((UINT8 *)(pEPDesc) + pEPDesc->bLength);
        } /* end of loop for each endpoint */

    return TRUE;
    }

