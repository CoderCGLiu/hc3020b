/* usbOhciUtil.h - utility routine interfaces for OHCD */

/*
 * Copyright (c) 2010 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01d,18aug10,m_y  Add declaration of usbOhciModifyTDList
01c,02jul10,m_y  Add macro USB_OHCD_CALL_URB_CALLBACK (WIND00221377)
01b,23jun10,m_y  Replace the routine usbOhciFreeTDList to
                 usbOhciFreeRequestTDList (WIND00216859)
01a,18may10,m_y  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
*/

/*
DESCRIPTION

This file contains the utility routine interfaces for transfer management.
*/



#ifndef __INCusbOhciUtilh
#define __INCusbOhciUtilh

#ifdef  __cplusplus
extern "C" {
#endif

#define USB_OHCI_ED_DESC_ALIGN           (16)
#define USB_OHCI_MAX_ED_SIZE             sizeof(USB_OHCD_PIPE)

#define USB_OHCI_TD_DESC_ALIGN           (32)
#define USB_OHCI_MAX_TD_SIZE             sizeof(USB_OHCI_TD)

#define USB_OHCI_HCCA_ALIGN              (256)
#define USB_OHCI_MAX_HCCA_SIZE           sizeof(USB_OHCI_HCCA)

#define USB_OHCI_DESC_BOUNDARY_LIMIT     (4096)

#define USB_OHCI_ISR_THREAD_PRI          (100)

/* Fill TD */

#define usbOhciFillTD(uBusIndex, pTd, uBufferAddr, uLength)                    \
    {                                                                          \
    pTd->uCurrentBufferPointer = USB_OHCD_SWAP_DATA(uBusIndex, uBufferAddr);   \
    if (pTd->pHCDNextTransferDescriptor != NULL)                               \
        pTd->uNextTDPointer = USB_OHCD_SWAP_DATA(uBusIndex,                    \
                    USB_OHCI_DESC_LO32(pTd->pHCDNextTransferDescriptor));      \
    else                                                                       \
        pTd->uNextTDPointer = 0;                                               \
    if (uLength > 0)                                                           \
        pTd->uBufferEndPointer = USB_OHCD_SWAP_DATA                            \
                                   (uBusIndex ,(uBufferAddr + uLength - 1));   \
    else                                                                       \
        pTd->uBufferEndPointer  = pTd->uCurrentBufferPointer;                  \
    pTd->uStartAddressPointer = uBufferAddr;                                   \
    }

/* Set CLF bit in the HcCommandStatus register */

#define USB_OHCI_SET_CLF(pHCDData)                                           \
    {                                                                        \
    UINT32 uTemp;                                                            \
    uTemp = USB_OHCI_REG_READ(pHCDData->pDev,                                \
                              USB_OHCI_COMMAND_STATUS_REGISTER_OFFSET);      \
    uTemp |= USB_OHCI_COMMAND_STATUS_CLF;                                    \
    USB_OHCI_REG_WRITE(pHCDData->pDev,                                       \
                       USB_OHCI_COMMAND_STATUS_REGISTER_OFFSET,              \
                       uTemp);                                               \
    }

/* Enable CLE in the HcControl register */

#define USB_OHCI_ENABLE_CONTROL_LIST(pHCDData)                               \
    {                                                                        \
    UINT32 uTemp;                                                            \
    uTemp = USB_OHCI_REG_READ(pHCDData->pDev,                                \
                              USB_OHCI_CONTROL_REGISTER_OFFSET);             \
    uTemp |= USB_OHCI_CONTROL_CLE;                                           \
    USB_OHCI_REG_WRITE(pHCDData->pDev,                                       \
                       USB_OHCI_CONTROL_REGISTER_OFFSET,                     \
                       uTemp);                                               \
    }

/* Disable CLE in the HcControl register  */

#define USB_OHCI_DISABLE_CONTROL_LIST(pHCDData)                              \
    {                                                                        \
    UINT32 uTemp;                                                            \
    uTemp = USB_OHCI_REG_READ(pHCDData->pDev,                                \
                              USB_OHCI_CONTROL_REGISTER_OFFSET);             \
    uTemp &= ~USB_OHCI_CONTROL_CLE;                                          \
    USB_OHCI_REG_WRITE(pHCDData->pDev,                                       \
                       USB_OHCI_CONTROL_REGISTER_OFFSET,                     \
                       uTemp);                                               \
    }

/* Set BLF bit in the HcCommandStatus register */

#define USB_OHCI_SET_BLF(pHCDData)                                           \
    {                                                                        \
    UINT32 uTemp;                                                            \
    uTemp = USB_OHCI_REG_READ(pHCDData->pDev,                                \
                              USB_OHCI_COMMAND_STATUS_REGISTER_OFFSET);      \
    uTemp |= USB_OHCI_COMMAND_STATUS_BLF;                                    \
    USB_OHCI_REG_WRITE(pHCDData->pDev,                                       \
                       USB_OHCI_COMMAND_STATUS_REGISTER_OFFSET,              \
                       uTemp);                                               \
    }

/* Enable BLE in the HcControl register */

#define USB_OHCI_ENABLE_BULK_LIST(pHCDData)                                  \
    {                                                                        \
    UINT32 uTemp;                                                            \
    uTemp = USB_OHCI_REG_READ(pHCDData->pDev,                                \
                              USB_OHCI_CONTROL_REGISTER_OFFSET);             \
    uTemp |= USB_OHCI_CONTROL_BLE;                                           \
    USB_OHCI_REG_WRITE(pHCDData->pDev,                                       \
                       USB_OHCI_CONTROL_REGISTER_OFFSET,                     \
                       uTemp);                                               \
    }

/* Disable BLE in the HcControl register  */

#define USB_OHCI_DISABLE_BULK_LIST(pHCDData)                                 \
    {                                                                        \
    UINT32 uTemp;                                                            \
    uTemp = USB_OHCI_REG_READ(pHCDData->pDev,                                \
                              USB_OHCI_CONTROL_REGISTER_OFFSET);             \
    uTemp &= ~USB_OHCI_CONTROL_BLE;                                          \
    USB_OHCI_REG_WRITE(pHCDData->pDev,                                       \
                       USB_OHCI_CONTROL_REGISTER_OFFSET,                     \
                       uTemp);                                               \
    }


/* Enable PLE in the HcControl register */

#define USB_OHCI_ENABLE_PERIODIC_LIST(pHCDData)                              \
    {                                                                        \
    UINT32 uTemp;                                                            \
    uTemp = USB_OHCI_REG_READ(pHCDData->pDev,                                \
                              USB_OHCI_CONTROL_REGISTER_OFFSET);             \
    uTemp |= USB_OHCI_CONTROL_PLE;                                           \
    USB_OHCI_REG_WRITE(pHCDData->pDev,                                       \
                       USB_OHCI_CONTROL_REGISTER_OFFSET,                     \
                       uTemp);                                               \
    }


/* Disable PLE in the HcControl register */

#define USB_OHCI_DISABLE_PERIODIC_LIST(pHCDData)                             \
    {                                                                        \
    UINT32 uTemp;                                                            \
    uTemp = USB_OHCI_REG_READ(pHCDData->pDev,                                \
                              USB_OHCI_CONTROL_REGISTER_OFFSET);             \
    uTemp &= ~USB_OHCI_CONTROL_PLE;                                          \
    USB_OHCI_REG_WRITE(pHCDData->pDev,                                       \
                       USB_OHCI_CONTROL_REGISTER_OFFSET,                     \
                       uTemp);                                               \
    }

#define USB_OHCI_ADD_PIPE_INTO_DISABLE_LIST(pHCDData, pHCDPipe)                 \
    {                                                                           \
    if (ERROR == lstFind(&(pHCDData->disableEDList), &(pHCDPipe->disableNode))) \
        lstAdd(&(pHCDData->disableEDList), &(pHCDPipe->disableNode));           \
    }

#define USB_OHCI_REMOVE_PIPE_FROM_DISABLE_LIST(pHCDData, pHCDPipe)              \
    {                                                                           \
    if (ERROR != lstFind(&(pHCDData->disableEDList), &(pHCDPipe->disableNode))) \
        {                                                                       \
        USB_OHCD_DBG("remove pipe %p from disable list\n",                      \
                     (ULONG)pHCDPipe, 2, 3, 4, 5, 6);                           \
        lstDelete(&(pHCDData->disableEDList), &(pHCDPipe->disableNode));        \
        }                                                                       \
    }

#define USB_OHCI_DISABLE_TRANSFER_LIST(pHCDData, pHCDPipe)                   \
    {                                                                        \
    if (pHCDPipe->uEndpointType == USBHST_CONTROL_TRANSFER)                  \
        {                                                                    \
        USB_OHCI_DISABLE_CONTROL_LIST(pHCDData);                             \
        }                                                                    \
    else if (pHCDPipe->uEndpointType == USBHST_BULK_TRANSFER)                \
        {                                                                    \
        USB_OHCI_DISABLE_BULK_LIST(pHCDData);                                \
        }                                                                    \
    else                                                                     \
        {                                                                    \
        USB_OHCI_DISABLE_PERIODIC_LIST(pHCDData);                            \
        }                                                                    \
    }

#define USB_OHCI_ENABLE_TRANSFER_LIST(pHCDData, pHCDPipe)                    \
    {                                                                        \
    if (pHCDPipe->uEndpointType == USBHST_CONTROL_TRANSFER)                  \
        {                                                                    \
        USB_OHCI_ENABLE_CONTROL_LIST(pHCDData);                              \
        }                                                                    \
    else if (pHCDPipe->uEndpointType == USBHST_BULK_TRANSFER)                \
        {                                                                    \
        USB_OHCI_ENABLE_BULK_LIST(pHCDData);                                 \
        }                                                                    \
    else                                                                     \
        {                                                                    \
        USB_OHCI_ENABLE_PERIODIC_LIST(pHCDData);                             \
        }                                                                    \
    }

#define ADD_REQEUEST_INTO_FREE_LIST(pHCDPipe, pRequest)                       \
    {                                                                         \
    if (ERROR == lstFind(&(pHCDPipe->freeRequestList), &(pRequest->listNode)))\
        {                                                                     \
        lstAdd(&(pHCDPipe->freeRequestList), &(pRequest->listNode));          \
        }                                                                     \
    }

#define ADD_REQEUEST_INTO_CANCEL_LIST(pHCDPipe, pRequest)                        \
    {                                                                            \
    if(ERROR == lstFind(&(pHCDPipe->cancelRequestList), &(pRequest->cancelNode)))\
        {                                                                        \
        lstAdd(&(pHCDPipe->cancelRequestList), &(pRequest->cancelNode));         \
        }                                                                        \
    }

#define REMOVE_REQEUEST_FROM_FREE_LIST(pHCDPipe, pRequest)                     \
    {                                                                          \
    if (ERROR != lstFind(&(pHCDPipe->freeRequestList), &(pRequest->listNode))) \
        {                                                                      \
        lstDelete(&(pHCDPipe->freeRequestList), &(pRequest->listNode));        \
        }                                                                      \
    }

#define REMOVE_REQEUEST_FROM_CANCEL_LIST(pHCDPipe, pRequest)                      \
    {                                                                             \
    if (ERROR != lstFind(&(pHCDPipe->cancelRequestList), &(pRequest->cancelNode)))\
        {                                                                         \
        lstDelete(&(pHCDPipe->cancelRequestList), &(pRequest->cancelNode));       \
        }                                                                         \
    }

#define MOVE_REQUEST_FROM_FREE_TO_PIPE_LIST(pHCDPipe, pRequest)               \
    {                                                                         \
    if (ERROR != lstFind(&(pHCDPipe->freeRequestList), &(pRequest->listNode)))\
        {                                                                     \
        lstDelete(&(pHCDPipe->freeRequestList), &(pRequest->listNode));       \
        lstAdd(&(pHCDPipe->requestList), &(pRequest->listNode));              \
        }                                                                     \
    }

#define MOVE_REQUEST_FROM_PIPE_TO_FREE_LIST(pHCDPipe, pRequest)              \
    {                                                                        \
    if (ERROR != lstFind(&(pHCDPipe->requestList), &(pRequest->listNode)))   \
        {                                                                    \
        lstDelete(&(pHCDPipe->requestList), &(pRequest->listNode));          \
        lstAdd(&(pHCDPipe->freeRequestList), &(pRequest->listNode));         \
        }                                                                    \
    }

/* Macro to update the URB's nStatus and call it's callBack function */

#define USB_OHCD_CALL_URB_CALLBACK(pUrb, nUrbStatus)                       \
    {                                                                      \
    if (pUrb != NULL)                                                      \
        {                                                                  \
        if (pUrb->pHcdSpecific != NULL)                                    \
            {                                                              \
            pUrb->nStatus = nUrbStatus;                                    \
            pUrb->pHcdSpecific = NULL;                                     \
            if (pUrb->pfCallback != NULL)                                  \
                pUrb->pfCallback(pUrb);                                    \
            }                                                              \
        }                                                                  \
    }

#define USB_OHCI_MEM_MAP_TD 0
#define USB_OHCI_MEM_MAP_ED 1

IMPORT PVOID usbOhciPhyToVirt
    (
    pUSB_OHCD_DATA  pHCDData,
    bus_addr_t      busAddr,
    UINT8           uFlag
    );

IMPORT pUSB_OHCD_REQUEST_INFO usbOhciCreateRequestInfo
    (
    pUSB_OHCD_DATA  pHCDData,
    pUSB_OHCD_PIPE  pHCDPipe
    );

IMPORT VOID usbOhciDeleteRequestInfo
    (
    pUSB_OHCD_DATA                  pHCDData,
    pUSB_OHCD_PIPE                  pHCDPipe,
    pUSB_OHCD_REQUEST_INFO          pRequest
    );

IMPORT pUSB_OHCD_REQUEST_INFO usbOhciReserveRequestInfo
    (
    pUSB_OHCD_DATA  pHCDData,
    pUSB_OHCD_PIPE  pHCDPipe,
    pUSBHST_URB     pUrb
    );

IMPORT VOID usbOhciReleaseRequestInfo
    (
    pUSB_OHCD_DATA                 pHCDData,
    pUSB_OHCD_PIPE                 pHCDPipe,
    pUSB_OHCD_REQUEST_INFO         pRequest
    );

IMPORT BOOLEAN usbOhciCreateTDList
    (
    pUSB_OHCD_DATA                 pHCDData,
    pUSB_OHCD_PIPE                 pHCDPipe,
    pUSB_OHCD_REQUEST_INFO         pRequest,
    PUSB_OHCI_TD *                 ppDataHead,
    PUSB_OHCI_TD *                 ppDataTail,
    UINT32                         uNumOfTD
    );

LOCAL PUSB_OHCI_TD usbOhciCreateTD
    (
    pUSB_OHCD_DATA  pHCDData
    );

IMPORT PUSB_OHCI_TD usbOhciGetFreeTD
    (
    pUSB_OHCD_DATA  pHCDData,
    pUSB_OHCD_PIPE  pHCDPipe
    );

IMPORT BOOLEAN usbOhciAddToFreeTDList
    (
    pUSB_OHCD_DATA  pHCDData,
    pUSB_OHCD_PIPE  pHCDPipe,
    PUSB_OHCI_TD    pTD
    );

IMPORT VOID usbOhciFreeRequestTDList
    (
    pUSB_OHCD_DATA         pHCDData,
    pUSB_OHCD_PIPE         pHCDPipe,
    pUSB_OHCD_REQUEST_INFO pRequest
    );

IMPORT VOID usbOhciDestroyTDList
    (
    pUSB_OHCD_DATA pHCDData,
    PUSB_OHCI_TD   pTDListHead
    );

LOCAL pUSB_OHCD_PIPE usbOhciCreatePipeStructure
    (
    pUSB_OHCD_DATA pHCDData
    );

IMPORT pUSB_OHCD_PIPE usbOhciFormEmptyPipe
    (
    pUSB_OHCD_DATA pHCDData
    );

LOCAL STATUS usbOhciDestroyPipe
    (
    pUSB_OHCD_DATA  pHCDData,
    pUSB_OHCD_PIPE  pHCDPipe
    );

IMPORT VOID usbOhciCleanUpPipe
    (
    pUSB_OHCD_DATA  pHCDData,
    pUSB_OHCD_PIPE  pHCDPipe
    );
IMPORT VOID usbOhciPipeReturnAll
    (
    pUSB_OHCD_PIPE pHCDPipe
    );

LOCAL VOID usbOhciModifyTDList
    (
    UINT32                          uHostControllerIndex,
    pUSB_OHCD_PIPE                  pHCDPipe,
    pUSB_OHCD_REQUEST_INFO          pPrevRequest,
    pUSB_OHCD_REQUEST_INFO          pRequestToBeCancelled
    );

IMPORT VOID usbOhciPipeRmAllRequests
    (
    pUSB_OHCD_PIPE pHCDPipe
    );

IMPORT VOID usbOhciCleanDisabledPipe
    (
    pUSB_OHCD_DATA pHCDData
    );

#ifdef  __cplusplus
}
#endif

#endif /* __INCusbOhciUtilh */

/* End of File usbOhciUtil.h */
