/* usbMhdrcTcd.c - USB Mentor Graphics TCD Entry and Exit points */

/*
 * Copyright (c) 2010-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01u,03may13,wyy  Remove compiler warning (WIND00356717)
01t,05jan13,ljg  Add CPPI DMA support for MHDRC TCD (WIND00398723)
01s,15oct12,s_z  Add USBTGT_TCD_MAX_CONFIG_COUNT (WIND00382057)
01r,04sep12,s_z  Use usbTgtTcdRegister/usbTgtTcdUnRegister API
01q,22aug12,ljg  Fix coverity issue "CHECKED_RETURN' (WIND00371808)
01p,15sep11,m_y  Using one structure to describe HSDMA and CPPIDMA
01o,21jun11,jws  Modified to support target mode for TI centaurus chip
01n,13jun11,jws  Comment out invoking of rebootHookDelete()
01m,13dec11,m_y  Modify according to code check result (WIND00319317)
01l,21apr11,w_x  Check DMA enabled flag when enable/disable TCD DMA
01k,08apr11,w_x  Use USB_MHDRC_TASK_NAME_MAX for Coverity (WIND00264893)
01j,30mar11,s_z  Changed based on the redefinition of struct usbtgt_device_info
01i,28mar11,s_z  Destroy the TCD resource in usbMhdrcTcdDataUnInit
                 Fix dynomic attach/detach issue
01h,28mar11,w_x  Correct uNumEps usage (WIND00262862)
01g,22mar11,s_z  Changes for unused routines removed
                 Modify usbMhdrcTcdDisable() to do soft disconnect
01f,23mar11,m_y  code clean up based on the review result
01e,15mar11,w_x  move BSP specific init to platform init (WIND00258032)
01d,09mar11,s_z  Code clean up
01c,18feb11,m_y  modify device feature's default value
01b,20oct10,m_y  write
01a,18may10,s_z  created
*/

/*
DESCRIPTION

This file provides the entry and exit points for USB Mentor Graphics
Target Controller Driver.

INCLUDE FILES: usb/usb.h, usb/ossLib.h, usb/usbOsal.h, usb/usbOsalDebug.h,
               usb/usbPlatform.h, hwif/vxbus/hwConf.h,
               hwif/vxbus/vxbPciLib.h, spinLockLib.h, string.h,
               rebootLib.h, hookLib.h, usbMhdrcTcd.h,
               usbMhdrcTcdIsr.h, usbMhdrcPlatform.h, usbMhdrcTcdDebug.c,
               usbMhdrcDma.h
*/

/* includes */

#include <usb/usb.h>
#include <usb/ossLib.h>
#include <usb/usbOsal.h>
#include <usb/usbOsalDebug.h>
#include <usb/usbPlatform.h>
#include <hwif/vxbus/hwConf.h>
#include <hwif/vxbus/vxbPciLib.h>
#include <spinLockLib.h>
#include <string.h>
#include <rebootLib.h>
#include <hookLib.h>
#include "usbMhdrcTcd.h"
#include "usbMhdrcTcdIsr.h"
#include "usbMhdrcPlatform.h"
#include "usbMhdrcTcdDebug.c"
#include "usbMhdrcDma.h"

/* externs */

extern UINT16 gUsbMhdrcTcdVid[];
extern UINT16 gUsbMhdrcTcdPid[];
extern UINT16 gUsbMhdrcTcdBcdDevice[];
extern char * gUsbMhdrcTcdMfgString[];
extern char * gUsbMhdrcTcdProdString[];
extern char * gUsbMhdrcTcdSerialString[];
extern char * gUsbMhdrcTcdName[];



extern USBTGT_TCD_FUNCS g_UsbMhdrcTcdDriver;

/* The MHDRC driver */

IMPORT USBTGT_TCD_FUNCS g_UsbMhdrcTcdDriver;


/* extern */

IMPORT    STATUS usbMhdrcTcdSoftConnect
    (
    pUSBTGT_TCD pTCD,
    BOOL        isConnectUp
    );

/* locals */

LOCAL BOOL  g_UsbMhdrcTcdRegistered = FALSE;
LOCAL LIST  g_UsbMhdrcTcdList;

LOCAL VOID usbVxbMhdrcTcdNullFunction
    (
    VXB_DEVICE_ID       pDev
    );
LOCAL void usbVxbMhdrcTcdPciInit
    (
    VXB_DEVICE_ID       pDev
    );
LOCAL VOID usbVxbMhdrcTcdPlbInit
    (
    VXB_DEVICE_ID       pDev
    );
LOCAL void usbVxbMhdrcTcdConnect
    (
    VXB_DEVICE_ID       pDev
    );
LOCAL STATUS usbVxbMhdrcTcdRemove
    (
    VXB_DEVICE_ID       pDev
    );
LOCAL BOOL usbVxbMhdrcTcdPciDeviceProbe
    (
    VXB_DEVICE_ID       pDev
    );
LOCAL STATUS usbMhdrcTcdDataInit
    (
    pUSB_MHDRC_TCD_DATA   pTCDData
    );
LOCAL VOID usbMhdrcTcdDataUnInit
    (
    pUSB_MHDRC_TCD_DATA   pTCDData
    );
LOCAL int usbMhdrcTcdDisable
    (
    int startType
    );
LOCAL STATUS usbMhdrcTcdInstanceAdd
    (
    pUSB_MUSBMHDRC      pMHDRC
    );
LOCAL STATUS usbMhdrcTcdInstanceRemove
    (
    pUSB_MUSBMHDRC      pMHDRC
    );
LOCAL STATUS usbMhdrcTcdInstanceEnable
    (
    pUSB_MUSBMHDRC      pMHDRC
    );
LOCAL STATUS usbMhdrcTcdInstanceDisable
    (
    pUSB_MUSBMHDRC      pMHDRC
    );

LOCAL PCI_DEVVEND usbVxbPciMhdrcTcdDevVendId[1] =
   {
       {
        0xffff, /* Device ID */
        0xffff  /* Vendor ID */
       }
   };

/* Structure to store the driver functions for vxBus */

LOCAL struct drvBusFuncs usbVxbPlbMhdrcTcdDriverFuncs =
    {
    usbVxbMhdrcTcdPlbInit,         /* init 1 */
    usbVxbMhdrcTcdNullFunction,    /* init 2 */
    usbVxbMhdrcTcdConnect          /* Dev Connect */
    };

/* Structure to store the driver functions for vxBus */

LOCAL struct drvBusFuncs usbVxbPciMhdrcTcdDriverFuncs =
    {
    usbVxbMhdrcTcdPciInit,         /* init 1 */
    usbVxbMhdrcTcdNullFunction,    /* init 2 */
    usbVxbMhdrcTcdConnect          /* Dev Connect */
    };

/* Instance methods */

LOCAL DRIVER_METHOD usbVxbMhdrcTcdDrvMethods[2] =
    {
    DEVMETHOD (vxbDrvUnlink, usbVxbMhdrcTcdRemove),
    DEVMETHOD_END
    };

/* DRIVER_REGISTRATION for the MHDRC device residing on PLB */

LOCAL DRIVER_REGISTRATION usbVxbPlbMhdrcTcdDevRegistration =
    {
    NULL,
    VXB_DEVID_BUSCTRL,                             /* bus controller */
    VXB_BUSID_PLB,                                 /* bus id - PLB Bus Type */
    VXB_VER_4_0_0,                                 /* vxBus version Id */
    "vxbPlbUsbMhdrcTcd",                           /* drv name */
    &usbVxbPlbMhdrcTcdDriverFuncs,                 /* pDrvBusFuncs */
    &usbVxbMhdrcTcdDrvMethods[0],                  /* pMethods */
    NULL,                                          /* probe routine */
    NULL                                           /* vxbParams */
    };

/*
 * DRIVER_REGISTRATION for MHDRC device residing on PCI Bus
 * Most of MHDRC are use PLB bus, the PCI interface is here
 * for futher using.
 */

LOCAL PCI_DRIVER_REGISTRATION usbVxbPciMhdrcTcdDevRegistration =
    {
        {
        NULL,
        VXB_DEVID_BUSCTRL,                         /* bus controller */
        VXB_BUSID_PCI,                             /* bus id - PCI Bus Type */
        VXB_VER_4_0_0,                             /* vxBus version Id */
        "vxbPciUsbMhdrcTcd",                       /* drv name */
        &usbVxbPciMhdrcTcdDriverFuncs,             /* pDrvBusFuncs */
        &usbVxbMhdrcTcdDrvMethods [0],             /* pMethods */
        usbVxbMhdrcTcdPciDeviceProbe,              /* probe routine */
        NULL                                       /* vxbParams */
        },
    NELEMENTS(usbVxbPciMhdrcTcdDevVendId),         /* idListLen */
    &usbVxbPciMhdrcTcdDevVendId [0]                /* idList */
    };

/*******************************************************************************
*
* LOCAL VOID usbVxbMhdrcTcdNullFunction - dummy routine
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbVxbMhdrcTcdNullFunction
    (
    VXB_DEVICE_ID       pDev
    )
    {
    /* This is a dummy routine which simply returns */

    return ;
    }

/*******************************************************************************
*
* usbVxbMhdrcTcdPciInit - legacy PCI support implementation
*
* This routine implements the legacy support for MHDRC Controller.
*
* RETURN : N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbVxbMhdrcTcdPciInit
    (
    VXB_DEVICE_ID       pDev                    /* struct vxbDev * */
    )
    {
    /*
     * This is a dummy routine. No such kind of device found and be
     * supported currently. If needed, fill up this routine.
     */

    return;
    }

/*******************************************************************************
*
* usbVxbMhdrcTcdPlbInit - do the BSP specific Initializaion
*
* This routine does the BSP specific initializaiton incase it is required.
* For many PLB based controllers, different BSP level initialization is
* requried.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
*\NOMANUAL
*/

LOCAL VOID usbVxbMhdrcTcdPlbInit
    (
    VXB_DEVICE_ID pDev
    )
    {
    /*
     * The BSP specific initializaton will be done by the platform
     * initalizaton hook in usbMhdrcPlatformHardwareInit(), which
     * will make the boot process quicker since it is called in the
     * VxBus tDevConn task where communication with the PHY using
     * slow link such as I2C is more proper.
     */

    return;
    }

/*****************************************************************************
*
* usbVxbMhdrcTcdConnect - vxBus instConnect handler
*
* This routine implements the VxBus instConnect handler for the
* device instance.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL void usbVxbMhdrcTcdConnect
    (
    VXB_DEVICE_ID pDev
    )
    {
    pUSB_MUSBMHDRC      pMHDRC = NULL;

    /* Validate paramters */

    if (NULL == pDev)
        {
        USB_MHDRC_ERR("Ivalid parameter, pDev is NULL\n",
                      1, 2, 3, 4, 5 ,6);
        return;
        }


    /* Allocate memory for the USB_MUSBMHDRC structure */

    pMHDRC = (pUSB_MUSBMHDRC)OSS_CALLOC(sizeof(USB_MUSBMHDRC));

    if (NULL == pMHDRC)
        {
        USB_MHDRC_ERR("Malloc pMHDRC failed\n", 1, 2, 3, 4, 5 ,6);
        return;
        }

    /* Set the VxBus Device instance */

    pMHDRC->pDev = pDev;

    /* Save the common hardware information as driver control */

    pDev->pDrvCtrl = pMHDRC;

    /* Get a unit number for pDev */

    vxbNextUnitGet (pDev);

    /* Initialize the common hardware data structure */

    if (usbMhdrcPlatformHardwareInit(pMHDRC) != OK)
        {
        USB_MHDRC_ERR("usbMhdrcPlatformHardwareInit failed\n",
                      1, 2, 3, 4, 5, 6);
        OS_FREE(pMHDRC);
        pDev->pDrvCtrl = NULL;
        return;
        }

    /* Add a target controller instance */

    if (usbMhdrcTcdInstanceAdd(pMHDRC) != OK)
        {
        USB_MHDRC_ERR("usbMhdrcTcdInstanceAdd failed\n",
                      1, 2, 3, 4, 5, 6);
        usbMhdrcPlatformHardwareUnInit(pMHDRC);
        OS_FREE(pMHDRC);
        pDev->pDrvCtrl = NULL;
        return;
        }

    /* Enable the target controller instance */

    if (usbMhdrcTcdInstanceEnable(pMHDRC) != OK)
        {
        USB_MHDRC_ERR("usbMhdrcTcdInstanceEnable failed\n",
                      1, 2, 3, 4, 5, 6);
        usbMhdrcTcdInstanceRemove(pMHDRC);
        usbMhdrcPlatformHardwareUnInit(pMHDRC);
        OS_FREE(pMHDRC);
        pDev->pDrvCtrl = NULL;
        return;
        }

    pMHDRC->isrMagic = USB_MHDRC_MAGIC_ALIVE;

    /* Enable hardware interrupt */

    usbMhdrcHardwareIntrEnable(pMHDRC);

    return;
    }

/*******************************************************************************
*
* usbVxbMhdrcTcdRemove - remove the MHDRC target controller
*
* This routine removes the MHDRC target controller.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbVxbMhdrcTcdRemove
    (
    VXB_DEVICE_ID       pDev
    )
    {
    pUSB_MUSBMHDRC      pMHDRC = NULL;

    if ((NULL == pDev) ||
        (NULL == pDev->pDrvCtrl))
        {
        USB_MHDRC_ERR("Invalid Parameters, pDev or pDev->pDrvCtrl NULL\n",
                      1, 2, 3, 4, 5 ,6);
        return ERROR;
        }

    pMHDRC = (pUSB_MUSBMHDRC)(pDev->pDrvCtrl);

    /* Disable the interrupt */

    usbMhdrcHardwareIntrDisable(pMHDRC);

    /* We can not respond to interrupts */

    pMHDRC->isrMagic = USB_MHDRC_MAGIC_DEAD;

    /* Uninitialize the hardware */

    usbMhdrcPlatformHardwareUnInit(pMHDRC);

    /* Disable the target controller instance */

    usbMhdrcTcdInstanceDisable(pMHDRC);

    /* Remove the target controller instance */

    usbMhdrcTcdInstanceRemove(pMHDRC);

    /* Release the resource of the pMHDRC */

    OS_FREE(pMHDRC);

    return OK;
    }

/*******************************************************************************
*
* usbVxbMhdrcTcdPciDeviceProbe - determine whether matching device is MHDRC
*
* This routine determines whether the matching device is a MHDRC Controller or
* not. For PCI device type, the routine will probe the PCI Configuration Device
* for Bus ID, Device Id and Function ID to determine whether the device is MHDRC
* controller.
*
* RETURNS: always FALSE
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL BOOL usbVxbMhdrcTcdPciDeviceProbe
    (
    VXB_DEVICE_ID       pDev
    )
    {
    UINT32              busId = 0; /* Bus id of parent bus */

    /* Validate the paramter */

    if ((NULL == pDev) ||
        (NULL == pDev->pParentBus) ||
        (NULL == pDev->pParentBus->pBusType))
        return FALSE;

    /* Determine the bus type of parent bus */

    busId = pDev->pParentBus->pBusType->busID;

    if (busId != VXB_BUSID_PCI)
        {
        return FALSE;
        }

    /*
     * If the PCI device use the MHDRC, it has the VID/PID or some other
     * feature to mark it is. So use it and full in the follow and return
     * TRUE
     */

    return FALSE;
    }

/*******************************************************************************
*
* usbVxbMhdrcTcdRegister - register the MHDRC with vxBus
*
* This routine registers the MHDRC Controller Driver driver with vxBus.
* Note that this can be called early in the initialization sequence.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbVxbMhdrcTcdRegister(void)
    {
    /* Do not registered multiple times */

    if (g_UsbMhdrcTcdRegistered != FALSE)
        {
        USB_MHDRC_ERR("The MHDRC TCD driver already registered\n",
                      1, 2, 3, 4, 5, 6);
        return;
        }
    /* Init the global list */

    lstInit(&g_UsbMhdrcTcdList);

    /* Register the MHDRC driver for PCI bus as underlying bus */

    if (vxbDevRegister ((DRIVER_REGISTRATION *)
        &usbVxbPciMhdrcTcdDevRegistration)
        == ERROR)
        {
        USB_MHDRC_ERR("Registering MHDRC Driver over PCI Bus\n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }

    /* Register the MHDRC driver for PLB bus as underlying bus */

    if (vxbDevRegister ((DRIVER_REGISTRATION *)
        &usbVxbPlbMhdrcTcdDevRegistration)
        == ERROR)
        {
        USB_MHDRC_ERR("Registering MHDRC Driver over PLB Bus\n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }

    g_UsbMhdrcTcdRegistered  = TRUE;

    /* Set the interfaces for OCD to use as a hook */

    gpUsbMhdrcTcdLoad = usbMhdrcTcdInstanceAdd;
    gpUsbMhdrcTcdUnLoad = usbMhdrcTcdInstanceRemove;
    gpUsbMhdrcTcdEnable = usbMhdrcTcdInstanceEnable;
    gpUsbMhdrcTcdDisable = usbMhdrcTcdInstanceDisable;

    return;
    }

/*******************************************************************************
*
* usbVxbMhdrcTcdDeregister - deregister the MHDRC with vxBus
*
* This routine deregisters the MHDRC with vxBus.
*
* RETURNS: OK, or ERROR if unable to deregister with vxBus
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbVxbMhdrcTcdDeregister (void)
    {
    if (g_UsbMhdrcTcdRegistered != TRUE)
        {
        USB_MHDRC_ERR("The MHDRC TCD driver already deregistered\n",
                      1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    /* Deregister the MHDRC driver for PLB bus as underlying bus */

    if (vxbDriverUnregister ((DRIVER_REGISTRATION *)
         &usbVxbPlbMhdrcTcdDevRegistration) == ERROR)
        {
        USB_MHDRC_ERR("Deregistering MHDRC Driver over PLB Bus\n",
                      1, 2, 3, 4, 5 ,6);
        return ERROR;
        }

    /* Deregisterr the MHDRC driver for PCI bus as underlying bus */

    if (vxbDriverUnregister ((DRIVER_REGISTRATION *)
         &usbVxbPciMhdrcTcdDevRegistration) == ERROR)
        {
        USB_MHDRC_ERR("Deregistering MHDRC Driver over PCI Bus\n",
                      1, 2, 3, 4, 5 ,6);
        return ERROR;
        }

    g_UsbMhdrcTcdRegistered = FALSE;
    return OK;
    }

/*******************************************************************************
*
* usbMhdrcTcdDataInit - initialize usb_mhdrc_tcd_data structure.
*
* This routine initializes the usb_mhdrc_tcd_data structure.
*
* RETURNS: OK, or ERROR if usb_mhdrc_tcd_data initialization is unsuccessful
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbMhdrcTcdDataInit
    (
    pUSB_MHDRC_TCD_DATA   pTCDData
    )
    {
    int                   i;
    char                  ThreadName[USB_MHDRC_TASK_NAME_MAX];
    pUSBTGT_TCD           pTCD = NULL;
    USBTGT_DEVICE_INFO *  pDeviceInfo = NULL;
    pUSB_MUSBMHDRC        pMHDRC = NULL;
    USBTGT_TCD_INFO       usbTgtTcdInfo;
    VXB_DEVICE_ID         pDev = NULL;

    if ((NULL == pTCDData) ||
        (NULL == pTCDData->pMHDRC) ||
        (NULL == pTCDData->pMHDRC->pDev))
        return ERROR;

    pMHDRC = pTCDData->pMHDRC;
    pDev = pTCDData->pMHDRC->pDev;

    if (ERROR == rebootHookAdd((FUNCPTR)usbMhdrcTcdDisable))
        {
        USB_MHDRC_ERR("Error in hooking the routine usbMhdrcTcdDisable\n",
                     1, 2, 3, 4, 5 ,6);
        return ERROR;
        }

    /* Init pipe list */

    lstInit(&(pTCDData->pipeList));

    /* Init the isrEvent */

    pTCDData->isrSemId = OS_CREATE_EVENT(OS_EVENT_SIGNALED);
    if (NULL == pTCDData->isrSemId)
        {
        USB_MHDRC_ERR("Create isrEvent fail\n", 1, 2, 3, 4, 5, 6);
        return ERROR;
        }
    /* Init the pTcdSyncMutex */

    pTCDData->tcdSyncMutex = semMCreate(SEM_Q_PRIORITY |
                                         SEM_INVERSION_SAFE |
                                         SEM_DELETE_SAFE);
    if (NULL == pTCDData->tcdSyncMutex)
        {
        USB_MHDRC_ERR("Create pTcdSyncMutex fail\n", 1, 2, 3, 4, 5, 6);
        usbMhdrcTcdDataUnInit(pTCDData);
        return ERROR;
        }

    /* Update the TCD create information */

    usbTgtTcdInfo.pDev = pDev;
    usbTgtTcdInfo.pTcdFuncs = &g_UsbMhdrcTcdDriver;
    usbTgtTcdInfo.pTcdSpecific = (pVOID)pTCDData;
    usbTgtTcdInfo.uVendorID = gUsbMhdrcTcdVid[pDev->unitNumber];
    usbTgtTcdInfo.uProductID = gUsbMhdrcTcdPid[pDev->unitNumber];
    usbTgtTcdInfo.uBcdDevice = gUsbMhdrcTcdBcdDevice[pDev->unitNumber];
    usbTgtTcdInfo.pMfgString = gUsbMhdrcTcdMfgString[pDev->unitNumber];
    usbTgtTcdInfo.pProdString = gUsbMhdrcTcdProdString[pDev->unitNumber];
    usbTgtTcdInfo.pSerialString = gUsbMhdrcTcdSerialString[pDev->unitNumber];
    usbTgtTcdInfo.pTcdName = gUsbMhdrcTcdName[pDev->unitNumber];
    usbTgtTcdInfo.uTcdUnit = (UINT8)pDev->unitNumber;
    usbTgtTcdInfo.uConfigCount = USBTGT_TCD_DEFAULT_CONFIG_COUNT;

    /* Init the device information */

    pDeviceInfo = &(usbTgtTcdInfo.DeviceInfo);

    /* Support self power don't support the remote wakeup */

    pDeviceInfo->uDeviceFeature = USB_DEV_STS_LOCAL_POWER;

    /* Support control transfer and enabled */

    pDeviceInfo->txEpCaps[0].bUsable = TRUE;
    pDeviceInfo->txEpCaps[0].uMaxPktSize = USB_MHDRC_MAXPSIZE_ENDPT_0;
    pDeviceInfo->txEpCaps[0].uXferTypeBitMap = (0x1 << USB_ATTR_CONTROL);

    pDeviceInfo->rxEpCaps[0].bUsable = FALSE;
    pDeviceInfo->rxEpCaps[0].uMaxPktSize = 0;
    pDeviceInfo->rxEpCaps[0].uXferTypeBitMap = 0;

    /* Support other transfer and enabled */

    for (i = 1; i < pMHDRC->uNumEps; i++)
        {
        /* The max packet size is 512 and support all transfer types */

        pDeviceInfo->txEpCaps[i].bUsable = TRUE;
        pDeviceInfo->txEpCaps[i].bRxTxShared = FALSE;
        pDeviceInfo->txEpCaps[i].uMaxPktSize = USB_MHDRC_MAXPSIZE_512;
        pDeviceInfo->txEpCaps[i].uXferTypeBitMap = (0x1 << USB_ATTR_CONTROL) |
                                                   (0x1 << USB_ATTR_ISOCH) |
                                                   (0x1 << USB_ATTR_BULK) |
                                                   (0x1 << USB_ATTR_INTERRUPT);

        pDeviceInfo->rxEpCaps[i].bUsable = TRUE;
        pDeviceInfo->rxEpCaps[i].bRxTxShared = FALSE;
        pDeviceInfo->rxEpCaps[i].uMaxPktSize = USB_MHDRC_MAXPSIZE_512;
        pDeviceInfo->rxEpCaps[i].uXferTypeBitMap = (0x1 << USB_ATTR_CONTROL) |
                                                   (0x1 << USB_ATTR_ISOCH) |
                                                   (0x1 << USB_ATTR_BULK) |
                                                   (0x1 << USB_ATTR_INTERRUPT);
        }

    /* Use the information register to TML */

    pTCDData->pTCD = usbTgtTcdRegister(&usbTgtTcdInfo);

    if (NULL == pTCDData->pTCD)
        {
        USB_MHDRC_ERR("Register the TCD to TML fail\n",
                      1, 2, 3, 4, 5 ,6);

        usbMhdrcTcdDataUnInit(pTCDData);

        return ERROR;
        }

    pTCD = pTCDData->pTCD;

     /* Assign an unique name for the interrupt handler thread */

    snprintf((char*)ThreadName,
        USB_MHDRC_TASK_NAME_MAX, "MTCD_IH%d", pTCD->uTcdUnit);

    /* Create the interrupt handler thread for handling the interrupts */

    pTCDData->intHandlerThread =
             OS_CREATE_THREAD((char *)ThreadName,
                               USB_MHDRC_TCD_INT_THREAD_PRIORITY,
                               usbMhdrcTcdInterruptHandler,
                               pTCDData->pTCD);

    /* Check whether the thread creation is successful */

    if (OS_THREAD_FAILURE == pTCDData->intHandlerThread)
        {
        USB_MHDRC_ERR("Interrupt handler thread is not created\n",
                     1, 2, 3, 4, 5 ,6);

        usbMhdrcTcdDataUnInit(pTCDData);
        return ERROR;
        }

    pTCDData->ep0Stage = USB_MHDRC_TCD_STAGE_IDLE;

    return OK;
    }

/*******************************************************************************
*
* usbMhdrcTcdDataUnInit - un-initialize usb_mhdrc_tcd_data structure.
*
* This routine un-initializes the usb_mhdrc_tcd_data structure.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbMhdrcTcdDataUnInit
    (
    pUSB_MHDRC_TCD_DATA   pTCDData
    )
    {
    /* I want to unit all the subelement of the pTCDData */

    pUSBTGT_TCD   pTCD = NULL;

    if (pTCDData == NULL)
        return;

    if (ERROR == rebootHookDelete(usbMhdrcTcdDisable))
        {
        USB_MHDRC_ERR("Failure in rebootHookDelete. \n",
                      1, 2, 3, 4, 5 ,6);
        }

    if ((pTCDData->intHandlerThread != 0) &&
        (pTCDData->intHandlerThread != OS_THREAD_FAILURE))
        {
        OS_DESTROY_THREAD(pTCDData->intHandlerThread);
        pTCDData->intHandlerThread = OS_THREAD_FAILURE;
        }

    if (pTCDData->isrSemId != NULL)
        {
        OS_DESTROY_EVENT(pTCDData->isrSemId);
        pTCDData->isrSemId = NULL;
        }

    if (pTCDData->tcdSyncMutex != NULL)
        {
        OS_DESTROY_EVENT(pTCDData->tcdSyncMutex);
        pTCDData->tcdSyncMutex = NULL;
        }

    if ((pTCD = pTCDData->pTCD) != NULL)
        {
        /* Remove pTCD from TML */

        /* Un-Register pTCD from TML */

        (void)usbTgtTcdUnRegister(pTCD);

        pTCDData->pTCD = NULL;
        }

    return ;
    }

/*******************************************************************************
*
* usbMhdrcTcdDisable - reboot hook to disable the host controller
*
* This routine is called on a warm reboot to disable the target controller by
* the BSP.
*
* RETURNS: 0, always
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL int usbMhdrcTcdDisable
    (
    int startType
    )
    {
    NODE *              pNode;
    pUSB_MHDRC_TCD_DATA pTCDData;

    pNode = lstFirst(&g_UsbMhdrcTcdList);

    while (pNode != NULL)
        {
        pTCDData = TCD_NODE_TO_USB_MHDRC_TCD_DATA(pNode);
        if (pTCDData != NULL)
            {
            usbMhdrcTcdSoftConnect(pTCDData->pTCD, FALSE);
            }
        pNode = lstNext(pNode);
        }
    return 0;
    }

/*******************************************************************************
*
* usbMhdrcTcdInstanceAdd - add a target controller instance
*
* This routine is to add a target controller instance.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbMhdrcTcdInstanceAdd
    (
    pUSB_MUSBMHDRC      pMHDRC
    )
    {
    pUSB_MHDRC_TCD_DATA pTCDData = NULL;

    pTCDData = (pUSB_MHDRC_TCD_DATA)OSS_CALLOC(sizeof(USB_MHDRC_TCD_DATA));

    /* Check if memory allocation is successful */

    if (NULL == pTCDData)
        {
        USB_MHDRC_ERR("Memory not allocated for USB_MTCD_DATA structure\n",
                     1, 2, 3, 4, 5 ,6);
        return ERROR;
        }

    /* Add the TCD to global list */

    lstAdd(&g_UsbMhdrcTcdList, &pTCDData->tcdNode);

    /* Save the common hardware information structure */

    pTCDData->pMHDRC = pMHDRC;

    /* Save the TCD information structure */

    pMHDRC->pTCDData = pTCDData;

    /* Initialize the Host Controller data structure */

    if (OK != usbMhdrcTcdDataInit(pTCDData))
        {
        USB_MHDRC_ERR("usbMhdrcTcdDataInit failed\n",
                      1, 2, 3, 4, 5 ,6);

        /* Uninitialize the hardware */

        pTCDData->pMHDRC = NULL;

        /* Remove the tcd from the global list */

        if (ERROR != lstFind(&g_UsbMhdrcTcdList, &pTCDData->tcdNode))
            lstDelete(&g_UsbMhdrcTcdList, &pTCDData->tcdNode);
        OS_FREE(pTCDData);
        return ERROR;
        }
    return OK;
    }

/*******************************************************************************
*
* usbMhdrcTcdInstanceRemove - remove a target controller instance
*
* This routine is to remove a target controller instance.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbMhdrcTcdInstanceRemove
    (
    pUSB_MUSBMHDRC      pMHDRC
    )
    {
    pUSB_MHDRC_TCD_DATA pTCDData = NULL;

    if ((NULL == pMHDRC) ||
        (NULL == pMHDRC->pTCDData))
        {
        USB_MHDRC_ERR("Invalid Parameters, pMHDRC or pMHDRC->pTCDData NULL\n",
                      1, 2, 3, 4, 5 ,6);
        return ERROR;
        }

    pTCDData = (pUSB_MHDRC_TCD_DATA)(pMHDRC->pTCDData);

    /* Release the resource of the pTCDData */

    usbMhdrcTcdDataUnInit(pTCDData);

    if (ERROR != lstFind(&g_UsbMhdrcTcdList, &pTCDData->tcdNode))
        lstDelete(&g_UsbMhdrcTcdList, &pTCDData->tcdNode);

    OS_FREE(pTCDData);
    return OK;
    }

/*******************************************************************************
*
* usbMhdrcTcdInstanceEnable - enable a target controller instance
*
* This routine is to enable a target controller instance.
*
* RETURNS: always OK
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbMhdrcTcdInstanceEnable
    (
    pUSB_MUSBMHDRC      pMHDRC
    )
    {
    /* Peform hardware specific mode change action */

    usbMhdrcPlatformModeChange (pMHDRC, USBOTG_MODE_PERIPHERAL);

    /* Enable the DMA */

    if (pMHDRC->bDmaEnabled == TRUE)
        {
        switch (pMHDRC->uPlatformType)
            {
            case USB_MHDRC_PLATFORM_OMAP35xx:

                /* Install the DMA callback routine */

                usbMhdrcDmaCallbackSet(&(pMHDRC->dmaData),
                                        (pUSB_MHDRC_DMA_CALLBACK_FUNC) usbMhdrcTcdHsDmaInterruptHandler);

                /* Enable the DMA interrupt */

                usbMhdrcDmaIntrEnable(pMHDRC);

                break;

            case USB_MHDRC_PLATFORM_CENTAURUS:

                /* Install the DMA callback routine */

                usbMhdrcDmaCallbackSet(&(pMHDRC->dmaData),
                                       (pUSB_MHDRC_DMA_CALLBACK_FUNC) usbMhdrcTcdCppiDmaInterruptHandler);

                /* Enable the DMA interrupt */

                usbMhdrcDmaIntrEnable(pMHDRC);

                break;

            default:
               break;
            }
        }
    return OK;
    }

/*******************************************************************************
*
* usbMhdrcTcdInstanceDisable - disable a target controller instance
*
* This routine is to disable target a host controller instance.
*
* RETURNS: always OK
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbMhdrcTcdInstanceDisable
    (
    pUSB_MUSBMHDRC      pMHDRC
    )
    {
    pUSB_MHDRC_TCD_DATA pTCDData = pMHDRC->pTCDData;

    usbMhdrcTcdSoftConnect(pTCDData->pTCD, FALSE);

    if (pMHDRC->bDmaEnabled == TRUE)
        {
        /* Disable the dma interrupt routine */

        usbMhdrcDmaIntrDisable(pMHDRC);

        /* Set dma callback routine as NULL */

        usbMhdrcDmaCallbackSet(&(pMHDRC->dmaData),
                                 NULL);
        }

    return OK;
    }

