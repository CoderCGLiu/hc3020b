/* usbMhdrcDavinci.c - platform Davanci configuration for Mentor Graphics HCD  */

/*
 * Copyright (c) 2010, 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01e,09mar11,w_x  Code clean up for make man
01d,13sep10,ghs  Fix defect found by code review of merge (WIND00232740)
01c,16aug10,w_x  VxWorks 64 bit audit and warning removal
01b,03jun10,s_z  Debug macro changed
01a,11mar10,s_z  written
*/

/*
DESCRIPTION

This file provides the Davanci platform related hardware configure routines
for the MHCD. It also proviedes the setup routine to configure Dm355 and
Dm6446 platforms.

INCLUDE FILES: usb/usb.h, usb/usbOsal.h, usb/usbHst.h, usb/usbd.h,
               usbMhdrc.h, usbMhdrcHcd.h usbMhdrcPlatform.h
*/

/* includes */

#include <usb/usb.h>
#include <usb/usbOsal.h>
#include <usb/usbHst.h>
#include <usb/usbd.h>
#include <usbMhdrc.h>
#include <usbMhdrcHcd.h>
#include <usbMhdrcPlatform.h>

/*******************************************************************************
*
* usbMhdrcDavinciIsr - interrupt handler for handling MHCI controller interrupts
*
* This routine is registered as the interrupt handler for handling MHCI
* controller interrupts.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
*
* \NOMANUAL
*/

LOCAL void usbMhdrcDavinciIsr
    (
    void * pMhdrc
    )
    {
    pUSB_MUSBMHDRC pMHDRC = (pUSB_MUSBMHDRC)pMhdrc;
    UINT16         uIntTx = 0;
    UINT16         uIntRx = 0;
    UINT8          uIntUsb = 0;
    UINT32         uInterruptStatus = 0;

    /* Check the validity of the parameter */

    if ((NULL == pMHDRC) ||
        (NULL == pMHDRC->pDev))
        {
        USB_MHDRC_ERR("usbMhdrcDavinciIsr(): "
                     "Invalid parameter, %s is NULL\n",
                     ((NULL == pMHDRC) ? "pMHDRC" :
                     "pMHDRC->pDev"),
                     2, 3, 4, 5 ,6);

        return;
        }

    if (pMHDRC->isrMagic != USB_MHDRC_MAGIC_ALIVE)
        {
        USB_MHDRC_ERR("usbMhdrcDavinciIsr(): "
                     "ISR not active, maybe shared?\n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }

    SPIN_LOCK_ISR_TAKE(&pMHDRC->spinLock);

    /* Read the contents of the HC Interrupt Status register */

    uInterruptStatus = USB_MHDRC_DAVINCI_PRIVATE_REG_READ32(pMHDRC,
                                                   USB_MHDRC_INTMASKED);

    /* Clear the interrupt bit and EOI to permit next interrupt come in */

    USB_MHDRC_DAVINCI_PRIVATE_REG_WRITE32(pMHDRC,
                                 USB_MHDRC_INTCLR,
                                 uInterruptStatus);

    uIntRx =
        (UINT16)((uInterruptStatus & USB_MHDRC_RXINT_MASK) >> USB_MHDRC_RXINT_SHIFT);
    uIntTx =
        (UINT16)((uInterruptStatus & USB_MHDRC_TXINT_MASK) >> USB_MHDRC_TXINT_SHIFT);
    uIntUsb =
        (UINT8)((uInterruptStatus & USB_MHDRC_USBINT_MASK) >> USB_MHDRC_USBINT_SHIFT);

    USB_MHDRC_VDBG("usbMhdrcDavinciIsr(): IRQ usb%04x tx%04x rx%04x,reg%x\n",
                  uIntUsb,
                  uIntTx,
                  uIntRx,
                  uInterruptStatus, 5, 6);


    if (uIntTx)
        {
        pMHDRC->uTxIrqStatus |= uIntTx;
        }

    if (uIntRx)
        {
        pMHDRC->uRxIrqStatus |= uIntRx;
        }

    if (uIntUsb)
        {
        pMHDRC->uUsbIrqStatus |= uIntUsb;
        }

   SPIN_LOCK_ISR_GIVE(&pMHDRC->spinLock);

    /* Signal the ISR event */

    if (uIntTx | uIntRx | uIntUsb)
        {
        USB_MHDRC_VDBG("usbMhdrcDavinciIsr(): "
                      "Interrupt Registers uIntTx %p uIntRx %p, uIntUsb %p\n",
                       uIntTx, uIntRx, uIntUsb, 4, 5 ,6);

        USB_MHDRC_HCD_REPORT_EVENT(pMHDRC, (pUSB_MHCD_DATA)pMHDRC->pHCDData);
        }

    return;
    }

/*******************************************************************************
*
* usbMhdrcDavinciIsrProcessDone - disable USB MHCI host controller interrupt
*
* This routine is used as callback to disable MHCI host controller interrupt.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcDavinciIsrProcessDone
    (
    void * pMhdrc
    )
    {
    pUSB_MUSBMHDRC  pMHDRC = (pUSB_MUSBMHDRC)pMhdrc;

    /* Check the validity of the parameter */

    if (NULL == pMHDRC)
        {
        USB_MHDRC_ERR("usbMhdrcDavinciIsrProcessDone(): "
                     "Invalid parameter, pMHDRC is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }

    /*
     * Clear EOIR to acknowledge the completion of the USB core interrupt
     */

    USB_MHDRC_DAVINCI_PRIVATE_REG_WRITE32 (pMHDRC,
                                  USB_MHDRC_EOI,
                                  0);

    }

/*******************************************************************************
*
* usbMhdrcDavinciInterruptEnable - enable MHCI USB host controller interrupt
*
* This routine is used as callback to enable MHCI USB host controller interrupt.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcDavinciInterruptEnable
    (
    void * pMhdrc
    )
    {
    pUSB_MUSBMHDRC pMHDRC = (pUSB_MUSBMHDRC)pMhdrc;
    UINT32         uRegisterValue;
    UINT32         uTmp;
    UINT32         uOld;

    /* Check the validity of the parameter */

    if (NULL == pMHDRC)
        {
        USB_MHDRC_ERR("usbMhdrcDavinciInterruptEnable(): "
                     "Invalid parameter, pMHDRC is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }

    /* Set USB interrupt in INTRTXE register */

    USB_MHDRC_REG_WRITE16 (pMHDRC,
                          USB_MHDRC_INTRTXE,
                          USB_MHDRC_INTMSKCLR_TX_MASK);

    /* Set USB interrupt in INTRRXE register */

    USB_MHDRC_REG_WRITE16 (pMHDRC,
                          USB_MHDRC_INTRRXE,
                          USB_MHDRC_INTMSKCLR_RX_MASK);

    /* Set USB interrupt in INTRUSBE register, except SOF interrupt */

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                         USB_MHDRC_INTRUSBE,
                         USB_MHDRC_INTMSKCLR_SOF_MASK);

    /* Set USB TX interrupt in INTMSKSET register */

    uTmp = USB_MHDRC_TXINT_MASK;

    USB_MHDRC_DAVINCI_PRIVATE_REG_WRITE32 (pMHDRC,
                                  USB_MHDRC_INTMSKSET,
                                  uTmp);

    /* Set USB RX interrupt in INTMSKSET register */

    uOld = uTmp;

    uTmp = USB_MHDRC_RXINT_MASK;

    USB_MHDRC_DAVINCI_PRIVATE_REG_WRITE32 (pMHDRC,
                                  USB_MHDRC_INTMSKSET,
                                  uTmp);

    /* Set USB TX, RX and Core USB interrupts in INTMSKSET register */

    uTmp |= uOld;

    uRegisterValue = ~USB_MHDRC_INTR_SOF;

    uTmp |= ((uRegisterValue & USB_MHDRC_INTMSKCLR_USBINT_MASK)
             << USB_MHDRC_USBINT_SHIFT);

    USB_MHDRC_DAVINCI_PRIVATE_REG_WRITE32 (pMHDRC,
                                  USB_MHDRC_INTMSKSET,
                                  uTmp);

    /* Disable USB2.0 Test Modes */

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                         USB_MHDRC_TESTMODE,
                         0x00);

    /* Put into basic highspeed mode, set SESSION and HOSTREQ bit */

    USB_MHDRC_REG_WRITE8 (pMHDRC, USB_MHDRC_POWER,
                         USB_MHDRC_POWER_ENSUSPM | USB_MHDRC_POWER_HSEN);

    /* Enable Session */

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                         USB_MHDRC_DEVCTL,
                         USB_MHDRC_DEVCTL_SESSION);

    USB_MHDRC_DBG("usbMhdrcDavinciInterruptEnable(): "
                 "Enable Done\n",
                 1, 2, 3, 4, 5 ,6);

    return;
    }


/*******************************************************************************
*
* usbMhdrcDavinciInterruptDisable - disable USB MHCI host controller interrupt
*
* This routine is used as callback to disable MHCI host controller interrupt.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcDavinciInterruptDisable
    (
    void * pMhdrc
    )
    {
    pUSB_MUSBMHDRC  pMHDRC = (pUSB_MUSBMHDRC)pMhdrc;
    UINT16 uReg16;
    UINT8  uReg8;

    if (NULL == pMHDRC)
        {
        USB_MHDRC_ERR("usbMhdrcDavinciInterruptDisable(): "
                     "Invalid parameter, pMHDRC is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }

    /* Reset the Host Controller */

    USB_MHDRC_DAVINCI_PRIVATE_REG_WRITE32 (pMHDRC,
                                  USB_MHDRC_CONTROL,
                                  USB_MHDRC_CTRLR_RESET);

    /* Disable CPPI DMA */

    USB_MHDRC_DAVINCI_PRIVATE_REG_WRITE32 (pMHDRC,
                                  USB_MHDRC_TCPPIC,
                                  0);

    USB_MHDRC_DAVINCI_PRIVATE_REG_WRITE32 (pMHDRC,
                                  USB_MHDRC_RCPPIC,
                                  0);

    /* Mask USB Tx interrupt in INTRTXE */

    USB_MHDRC_REG_WRITE16 (pMHDRC,
                          USB_MHDRC_INTRTXE,
                          0);

    /* Mask USB Rx interrupt in INTRRXE */

    USB_MHDRC_REG_WRITE16 (pMHDRC,
                          USB_MHDRC_INTRRXE,
                          0);

    /* Mask USB core interrupt in USB_MHDRC_INTRUSBE */

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                         USB_MHDRC_INTRUSBE,
                         0);

    USB_MHDRC_DAVINCI_PRIVATE_REG_WRITE32 (pMHDRC,
                                  USB_MHDRC_INTMSKCLR,
                                  USB_MHDRC_USBINT_MASK |
                                  USB_MHDRC_TXINT_MASK  |
                                  USB_MHDRC_RXINT_MASK);

    USB_MHDRC_DAVINCI_PRIVATE_REG_WRITE32 (pMHDRC,
                                  USB_MHDRC_EOI,
                                  0);

    /* Read the register to clean the 3 registers */

    uReg16 = USB_MHDRC_REG_READ16 (pMHDRC,
                         USB_MHDRC_INTRTX);


    uReg16 =  USB_MHDRC_REG_READ16 (pMHDRC,
                         USB_MHDRC_INTRRX);

    uReg8 =  USB_MHDRC_REG_READ8 (pMHDRC,
                         USB_MHDRC_INTRUSB);

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                         USB_MHDRC_POWER,
                         0 );

    /* Disable the session */

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                         USB_MHDRC_DEVCTL,
                         0 );

    USB_MHDRC_DBG("usbMhdrcDavinciInterruptDisable(): "
                 "Disable Done\n",
                 1, 2, 3, 4, 5 ,6);

    return ;
    }


/*******************************************************************************
*
* usbMhdrcDavinciSetup - configure davinci platform
*
* This routine configures davinci platform information.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcDavinciSetup
    (
    void * pMhdrc,
    void * pPlatformInfo
    )
    {
    pUSB_MUSBMHDRC  pMHDRC = (pUSB_MUSBMHDRC)pMhdrc;
    pUSB_MHDRC_PLATFORM_DATA pPlatformData =
                  (pUSB_MHDRC_PLATFORM_DATA)pPlatformInfo;

    if (NULL == pMHDRC || NULL == pPlatformData)
        {
        USB_MHDRC_ERR("usbMhdrcDavinciSetup(): "
                     "Invalid parameter, pMHDRC is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }

    /* Fill up the platform configure information */

    pPlatformData->uComRegOffset = USB_MHDRC_DAVINCI_COMMON_REG_OFFSET;
    pPlatformData->pUsbCoreIsr = usbMhdrcDavinciIsr;
    pPlatformData->pUsbIsrDone = usbMhdrcDavinciIsrProcessDone;
    pPlatformData->pHwInterruptEnable = usbMhdrcDavinciInterruptEnable;
    pPlatformData->pHwInterruptDisable = usbMhdrcDavinciInterruptDisable;

    return;
    }

