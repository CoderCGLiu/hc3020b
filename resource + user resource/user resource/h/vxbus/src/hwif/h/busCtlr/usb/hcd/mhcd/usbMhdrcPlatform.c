/* usbMhdrcPlatform.c - platform configuration which uses Mentor Graphics HCD */

/*
 * Copyright (c) 2010-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01n,03may13,wyy  Remove compiler warning (WIND00356717)
01m,31jan13,ljg  add usb support for ti816x EVM
01l,04jan13,s_z  Remove compiler warning (WIND00390357)
01k,17feb12,s_z  Fix 15+ devices pending issue by correcting endpoint channels
                 usage (WIND00333840)
01j,21jun11,jws  Added to call CPPI DMA setup function
01i,13jun11,jws  Added define for CENTAURUS. added include <hwif/vxbus/hwConf.h>
01h,08apr11,w_x  Clean up MUSBMHDRC platform definitions and Coverity
                 report for STRAY_SEMICOLON (WIND00264893)
01g,28mar11,w_x  Correct uNumEps usage (WIND00262862)
01f,23mar11,w_x  Address more code review comments
01e,15mar11,w_x  Move BSP specific init to platform init (WIND00258032)
01d,09mar11,w_x  Address code review comments
01c,16aug10,w_x  VxWorks 64 bit audit and warning removal
01b,03jun10,s_z  Debug macro changed, Add more debug message
01a,13mar10,s_z  written
*/

/*
DESCRIPTION

This file provides the platform configuration routine to use MHCD.

Mentor Graphics HCD (MHCD) has been used on many platforms. Different platforms
may use different versions of the same IP. They are different on the common
registers offset, IP related registers offset, DMA related registers offset
and DMA engine types (thus the handling of the DMA engine transfer setup), etc.
This module provides an easier way for the user to configure such platform
specific parameters.

Since the MHCD has a high dependency on the platform, we use this file to
configure the MHCD parameters according to different platform types.

INCLUDE FILES: usb/usb.h, usb/usbOsal.h, usb/usbHst.h, usb/usbd.h,
               usb/usbPhyUlpi.h, usbMhdrc.h, usbMhdrcPlatform.h,
               usbMhdrcHsDma.h, usbMhdrcCppiDma.h, usbMhdrcHcd.h, usbMhdrcTcd.h
*/

/* includes */

#include <usb/usb.h>
#include <usb/usbOsal.h>
#include <usb/usbHst.h>
#include <usb/usbd.h>
#include <usb/usbPhyUlpi.h>
#include "usbMhdrc.h"
#include "usbMhdrcPlatform.h"
#include "usbMhdrcHsDma.h"
#include "usbMhdrcCppiDma.h"
#include "usbMhdrcHcd.h"
#include "usbMhdrcTcd.h"

/* forward declare */

IMPORT    STATUS usbMhdrcPlatformDmaSetup
    (
    pUSB_MUSBMHDRC  pMHDRC
    );

#if defined (USB_MHDRC_DAVINCI)
#include <usbMhdrcPlatformDavinci.c>
#endif

#if  defined (USB_MHDRC_OMAP)
#include <usbMhdrcPlatformOmap.c>
#endif

#if  defined (USB_MHDRC_CENTAURUS)
#include <usbMhdrcPlatformCentaurus.c>
#endif

usbMhdrcHcdTcdInterfacePrototype gpUsbMhdrcHcdLoad = NULL;
usbMhdrcHcdTcdInterfacePrototype gpUsbMhdrcHcdUnLoad = NULL;
usbMhdrcHcdTcdInterfacePrototype gpUsbMhdrcTcdLoad = NULL;
usbMhdrcHcdTcdInterfacePrototype gpUsbMhdrcTcdUnLoad = NULL;
usbMhdrcHcdTcdInterfacePrototype gpUsbMhdrcHcdEnable = NULL;
usbMhdrcHcdTcdInterfacePrototype gpUsbMhdrcHcdDisable = NULL;
usbMhdrcHcdTcdInterfacePrototype gpUsbMhdrcTcdEnable = NULL;
usbMhdrcHcdTcdInterfacePrototype gpUsbMhdrcTcdDisable = NULL;


/*******************************************************************************
*
* usbMhdrcHcdFIFOInit - initialize the FIFO size and address for HCD
*
* This routine initializes the FIFO size and address, which will simplify
* the operation of the FIFO usage.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcHcdFIFOInit
    (
    pUSB_MUSBMHDRC  pMHDRC
    )
    {
    UINT8  uEpIndex = 0;
    UINT32 uAddressOffset = 0;

    if (NULL == pMHDRC)
        {
        USB_MHDRC_ERR("usbMhdrcHcdFIFOInit(): Invalid parameter, pMHDRC is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }


    USB_MHDRC_WARN("usbMhdrcHcdFIFOInit()\n", 1, 2, 3, 4, 5 ,6);

    /* Selet the EP Index */

    /* EP 0 , 64 Bytes */
    /* EP 1 , 128 Bytes */
    /* EP 2 , 256 Bytes */
    /* EP 3 , 512 Bytes */
    /* EP 4 , Double 512 Bytes */

    USB_MHDRC_VDBG("usbMhdrcHcdFIFOInit(): "
                  "Init the first 4 EPs [128,256,512, D512]\n",
                  1, 2, 3, 4, 5 ,6);

    for (uEpIndex = 0; uEpIndex <= 4; uEpIndex ++)
        {
        /* Change to Epx FIFO control register */

        USB_MHDRC_REG_WRITE8 (pMHDRC,
                             USB_MHDRC_INDEX,
                             uEpIndex);
        /* RX FIFO size */

        if (4 == uEpIndex)
            {
            USB_MHDRC_REG_WRITE8 (pMHDRC,
                                 USB_MHDRC_RXFIFOSZ,
                                 USB_MHDRC_FIFOSZ_DPB |
                                 (USB_MHDRC_FIFOSZ_SZ_64 + uEpIndex - 1 ));

            }
        else
            {
            USB_MHDRC_REG_WRITE8 (pMHDRC,
                                 USB_MHDRC_RXFIFOSZ,
                                 (USB_MHDRC_FIFOSZ_SZ_64 + uEpIndex));
            }

        USB_MHDRC_REG_WRITE16 (pMHDRC,
                             USB_MHDRC_RXFIFOADDR,
                             uAddressOffset);

        uAddressOffset += (0x1 << (USB_MHDRC_FIFOSZ_SZ_64 + uEpIndex ));

        /* For EP0 share the 64 Bytes */

        if (0 == uEpIndex)
            {
            uAddressOffset -= (0x1 << (USB_MHDRC_FIFOSZ_SZ_64));
            }

        /* TX FIFO size */

        if (4 == uEpIndex)
            {
            USB_MHDRC_REG_WRITE8 (pMHDRC,
                                 USB_MHDRC_TXFIFOSZ,
                                 USB_MHDRC_FIFOSZ_DPB |
                                 (USB_MHDRC_FIFOSZ_SZ_64 + uEpIndex - 1 ));
            }
        else
            {
            USB_MHDRC_REG_WRITE8 (pMHDRC,
                                 USB_MHDRC_TXFIFOSZ,
                                 (USB_MHDRC_FIFOSZ_SZ_64 + uEpIndex));
            }

        USB_MHDRC_REG_WRITE16 (pMHDRC,
                             USB_MHDRC_TXFIFOADDR,
                             uAddressOffset);

        uAddressOffset += (0x1 << (USB_MHDRC_FIFOSZ_SZ_64 + uEpIndex ));
        }

    /*
     * We have more than 4 Endpoints(expect Ep 0) to use,
     * such as in Omap3530 Evm
     */

    for (uEpIndex = 5; uEpIndex < pMHDRC->uNumEps; uEpIndex ++)
        {

        USB_MHDRC_VDBG("usbMhdrcHcdFIFOInit(): "
                       "Init the other EPs if it has, the size is 512\n",
                       1, 2, 3, 4, 5 ,6);

        /* Change to Epx FIFO control register */

        USB_MHDRC_REG_WRITE8 (pMHDRC,
                             USB_MHDRC_INDEX,
                             uEpIndex);

        USB_MHDRC_REG_WRITE8 (pMHDRC,
                             USB_MHDRC_RXFIFOSZ,
                             USB_MHDRC_FIFOSZ_SZ_512);

        USB_MHDRC_REG_WRITE16 (pMHDRC,
                             USB_MHDRC_RXFIFOADDR,
                             uAddressOffset);

        uAddressOffset += (0x1 << USB_MHDRC_FIFOSZ_SZ_512);

        USB_MHDRC_REG_WRITE8 (pMHDRC,
                             USB_MHDRC_TXFIFOSZ,
                             USB_MHDRC_FIFOSZ_SZ_512);
        USB_MHDRC_REG_WRITE16 (pMHDRC,
                             USB_MHDRC_TXFIFOADDR,
                             uAddressOffset);
        uAddressOffset += (0x1 << USB_MHDRC_FIFOSZ_SZ_512);
        }

    /* Restore the fifo size */

    for (uEpIndex = 0; uEpIndex < pMHDRC->uNumEps; uEpIndex ++)
        {
        USB_MHDRC_REG_WRITE8 (pMHDRC,
                             USB_MHDRC_INDEX,
                             uEpIndex);
        pMHDRC->uRxFifoSize[uEpIndex] = USB_MHDRC_REG_READ8 (pMHDRC,
                                          USB_MHDRC_RXFIFOSZ);

        pMHDRC->uTxFifoSize[uEpIndex] = USB_MHDRC_REG_READ8 (pMHDRC,
                                          USB_MHDRC_TXFIFOSZ);
        }

    /* Rewrite the index registers */

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                         USB_MHDRC_INDEX,
                         0);

    return;
    }


/*******************************************************************************
*
* usbMhdrcTcdFIFOInit - initialize the FIFO size and address for TCD
*
* This routine initializes the dynamic fifo size.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcTcdFIFOInit
    (
    pUSB_MUSBMHDRC  pMHDRC
    )
    {
    UINT8  uEpIndex = 0;
    UINT32 uAddressOffset = 0;
    UINT32 uSize = 0;

    /* Caller valid the parameter */

    USB_MHDRC_DBG("usbMhdrcTcdFIFOInit()\n", 1, 2, 3, 4, 5 ,6);

    /* Set endpoints fifo size as 512Byte except endpoint0 */

    for (uEpIndex = 0; uEpIndex < pMHDRC->uNumEps; uEpIndex ++)
        {
        /* Set index register */

        USB_MHDRC_REG_WRITE8 (pMHDRC, USB_MHDRC_INDEX, uEpIndex);

        /* RX FIFO size */

        if (0 != uEpIndex)
            {
            USB_MHDRC_REG_WRITE8 (pMHDRC,
                                  USB_MHDRC_RXFIFOSZ,
                                  USB_MHDRC_FIFOSZ_SZ_512);
            uAddressOffset = 8 + (uSize/8);
            uSize += USB_MHDRC_MAXPSIZE_512;
            }
        else
            {
            USB_MHDRC_REG_WRITE8 (pMHDRC,
                                  USB_MHDRC_RXFIFOSZ,
                                  USB_MHDRC_FIFOSZ_SZ_64);
            uAddressOffset = 0;
            }

        USB_MHDRC_REG_WRITE16 (pMHDRC,
                               USB_MHDRC_RXFIFOADDR,
                               uAddressOffset);

        /* TX FIFO size */

        if (0 != uEpIndex)
            {
            USB_MHDRC_REG_WRITE8 (pMHDRC,
                                  USB_MHDRC_TXFIFOSZ,
                                  USB_MHDRC_FIFOSZ_SZ_512);
            uAddressOffset = 8 + (uSize/8);
            uSize += USB_MHDRC_MAXPSIZE_512;
            }
        else
            {
            USB_MHDRC_REG_WRITE8 (pMHDRC,
                                  USB_MHDRC_TXFIFOSZ,
                                  USB_MHDRC_FIFOSZ_SZ_64);

            /* For Ep0, the TX/RX fifo share the 64-byte */

            uAddressOffset = 0;
            }

        USB_MHDRC_REG_WRITE16 (pMHDRC,
                               USB_MHDRC_TXFIFOADDR,
                               uAddressOffset);
        }

    /* Restore the fifo size */

    for (uEpIndex = 0; uEpIndex < pMHDRC->uNumEps; uEpIndex++)
        {
        USB_MHDRC_REG_WRITE8 (pMHDRC, USB_MHDRC_INDEX, uEpIndex);
        pMHDRC->uRxFifoSize[uEpIndex] =
           USB_MHDRC_REG_READ8 (pMHDRC,USB_MHDRC_RXFIFOSZ);
        pMHDRC->uTxFifoSize[uEpIndex] =
           USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_TXFIFOSZ);
        }

    /* Rewrite the index registers */

    USB_MHDRC_REG_WRITE8 (pMHDRC, USB_MHDRC_INDEX, 0);
    }

/*******************************************************************************
* usbMhdrcFIFOWrite - write endpoint data buffer to the MHDRC FIFO
*
* This routines writes endpoint data buffer to the MHDRC FIFO.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
*/

void usbMhdrcFIFOWrite
    (
    pUSB_MUSBMHDRC pMHDRC,          /* Pointer to common data structure */
    UINT8 *        pSourceBuffer,   /* Pointer to the source buffer */
    UINT16         wCount,          /* Count of byte to write */
    UINT8          uEndpointNumber  /* Endpoint number */
    )
    {
    /* To hold the value read from the registers */

    UINT8 * pTemp = pSourceBuffer;

    /* Parameter verification */

    if ((NULL == pMHDRC) ||
        (NULL == pSourceBuffer))
        {
        /* Debug print */

        USB_MHDRC_ERR("usbMhdrcFIFOWrite(): "
                     "Invalid parameter, %s is NULL\n",
                     ((NULL == pMHDRC) ? "pMHDRC" :
                     "pSourceBuffer"),
                     2, 3, 4, 5 ,6);

        return;
        }

    /* Obtain the pointer to the host controller information */

    while (wCount >= 4)
        {
        USB_MHDRC_REG_WRITE32 (pMHDRC,
                              USB_MHDRC_FIFO_EP(uEndpointNumber),
                              OS_UINT32_CPU_TO_LE(*((UINT32 *)pTemp)));

        pTemp += 4;
        wCount = (UINT16)(wCount - 4);
        }

    if (wCount >= 2)
        {
        USB_MHDRC_REG_WRITE16 (pMHDRC,
                              USB_MHDRC_FIFO_EP(uEndpointNumber),
                              OS_UINT16_CPU_TO_LE(*(UINT16 *)pTemp));
        pTemp += 2;
        wCount = (UINT16)(wCount - 2);
        }

    if (wCount == 1)
        {
        USB_MHDRC_REG_WRITE8 (pMHDRC,
                             USB_MHDRC_FIFO_EP(uEndpointNumber),
                             (*(UINT8 *)pTemp));
        }
    return;
    }


/*******************************************************************************
*
* usbMhdrcFIFORead - read endpoint data buffer from the MHDRC FIFO
*
* This routine reads endpoint data buffer from the MHDRC FIFO��
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbMhdrcFIFORead
    (
    pUSB_MUSBMHDRC pMHDRC,          /* Pointer to common data structure */
    UINT8 *        pDisBuffer,      /* Pointer to the distance buffer */
    UINT16         wCount,          /* Count of byte to read */
    UINT8          uEndpointNumber  /* Endpoint number */
    )
    {
    /* To hold the value read from the registers */

    UINT8 * pTemp = pDisBuffer;

    /* Parameter verification */

    if ((NULL == pMHDRC) ||
        (NULL == pDisBuffer))
        {
        /* Debug print */

        USB_MHDRC_ERR("usbMhdrcFIFORead(): "
                     "Invalid parameter, %s is NULL\n",
                     ((NULL == pMHDRC) ? "pMHDRC" :
                     "pDisBuffer"),
                     2, 3, 4, 5 ,6);

        return;
        }

    while (wCount >= 4)
        {
         *((UINT32 *)pTemp) = OS_UINT32_LE_TO_CPU(USB_MHDRC_REG_READ32 (pMHDRC,
                                 USB_MHDRC_FIFO_EP(uEndpointNumber)));
        pTemp += 4;
        wCount = (UINT16)(wCount - 4);
        }

    if (wCount >= 2)
        {
        *((UINT16 *)pTemp) = OS_UINT16_LE_TO_CPU(USB_MHDRC_REG_READ16 (pMHDRC,
                                    USB_MHDRC_FIFO_EP(uEndpointNumber)));
        pTemp += 2;
        wCount = (UINT16)(wCount - 2);
        }

    if (wCount == 1)
        {
        *((UINT8 *)pTemp) = USB_MHDRC_REG_READ8 (pMHDRC,
                                     USB_MHDRC_FIFO_EP(uEndpointNumber));
        }

    return;
    }

/*******************************************************************************
*
* usbMhdrcPlatformSetup - configure platform based on platform type
*
* This routine configures the platform according to the platform type.
*
* RETURNS: OK, or ERROR if failed to configure the platform.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbMhdrcPlatformSetup
    (
    pUSB_MUSBMHDRC  pMHDRC
    )
    {
    /* Check the validity of the parameter */

    if (NULL == pMHDRC)
        {
        USB_MHDRC_ERR("usbMhdrcPlatformSetup(): "
                     "Invalid parameter, pMHDRC is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return ERROR;
        }

    switch (pMHDRC->uPlatformType)
        {
        case USB_MHDRC_PLATFORM_OMAP35xx:
#if  defined (USB_MHDRC_OMAP)
            usbMhdrcOmap3evmSetup(pMHDRC, &pMHDRC->PlatformData);
#else
            USB_MHDRC_ERR ("usbMhdrcPlatformSetup(): USB_MHDRC_OMAP not defined\n",
                           1, 2, 3, 4, 5, 6);
            return ERROR;
#endif
            break;
        case USB_MHDRC_PLATFORM_DAVINIC:
#if defined (USB_MHDRC_DAVINCI)
            usbMhdrcDavinciSetup(pMHDRC, &pMHDRC->PlatformData);
#else
            USB_MHDRC_ERR ("usbMhdrcPlatformSetup(): USB_MHDRC_DAVINCI not defined\n",
                    1, 2, 3, 4, 5, 6);
            return ERROR;
#endif
            break;
        case USB_MHDRC_PLATFORM_OMAPL13x:
            break;
        case USB_MHDRC_PLATFORM_CENTAURUS:
            /* fall through */

        case USB_MHDRC_PLATFORM_CENTAURUS_TI816X:
#if defined (USB_MHDRC_CENTAURUS)
            usbMhdrcCentaurusSetup(pMHDRC, &pMHDRC->PlatformData);
#else
            USB_MHDRC_ERR ("usbMhdrcPlatformSetup(): USB_MHDRC_CENTAURUS not defined\n",
                    1, 2, 3, 4, 5, 6);
            return ERROR;
#endif
            break;
        default:
            USB_MHDRC_ERR("usbMhdrcPlatformSetup(): "
                         "Unknown platform %d\n",
                         pMHDRC->uPlatformType, 2, 3, 4, 5 ,6);
            return ERROR;
        }

    if ((NULL == pMHDRC->PlatformData.pUsbCoreIsr) ||
        (NULL == pMHDRC->PlatformData.pHwInterruptEnable) ||
        (NULL == pMHDRC->PlatformData.pHwInterruptDisable))
        {
        USB_MHDRC_ERR ("usbMhdrcPlatformSetup(): Platform information Error!!!\n",
                       1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    return OK;
    }

/*******************************************************************************
*
* usbMhdrcReportHostInterrupts - report MHDRC USB Host interrupts
*
* This routine reports MHDRC USB Host interrupts.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbMhdrcReportHostInterrupts
    (
    pUSB_MUSBMHDRC      pMHDRC
    )
    {
    USB_MHDRC_HCD_REPORT_EVENT(pMHDRC, (pUSB_MHCD_DATA)pMHDRC->pHCDData);
    }

/*******************************************************************************
*
* usbMhdrcReportTargetInterrupts - report MHDRC USB Target interrupts
*
* This routine reports MHDRC USB Target interrupts.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbMhdrcReportTargetInterrupts
    (
    pUSB_MUSBMHDRC      pMHDRC
    )
    {
    USB_MHDRC_TCD_REPORT_EVENT(pMHDRC, (pUSB_MHDRC_TCD_DATA)pMHDRC->pTCDData);
    }

/*******************************************************************************
*
* usbMhdrcHardwareIntrEnable - enable MHDRC USB interrupt
*
* This routine enables MHDRC USB interrupt.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcHardwareIntrEnable
    (
    pUSB_MUSBMHDRC pMHDRC
    )
    {
    if ((NULL == pMHDRC) ||
        (NULL == pMHDRC->PlatformData.pHwInterruptEnable))
        {
        USB_MHDRC_VDBG("usbMhdrcHardwareInterruptEnable(): "
                      "Invalid parameter, %s is NULL\n",
                      ((NULL == pMHDRC) ? "pMHDRC" :
                      "pMHDRC->PlatformData.pHwInterruptEnable"),
                       2, 3, 4, 5 ,6);

        return;
        }

    /* Enable the interrupt hardware */

    (pMHDRC->PlatformData.pHwInterruptEnable)((void *)pMHDRC);

    /* Enable the interrupts for the CPU */

    if (vxbIntEnable (pMHDRC->pDev,
                      0,
                      (pMHDRC->PlatformData.pUsbCoreIsr),
                      (void *)pMHDRC) == ERROR)
        {
        USB_MHDRC_ERR("Error enabling core intrrupts\n", 1, 2, 3, 4, 5 ,6);
        }

    return;
    }

/*******************************************************************************
*
* usbMhdrcHardwareIntrDisable - disable MHDRC USB interrupt
*
* This routine disables MHDRC USB interrupt.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcHardwareIntrDisable
    (
    pUSB_MUSBMHDRC pMHDRC
    )
    {
    if ((NULL == pMHDRC) ||
        (NULL == pMHDRC->PlatformData.pHwInterruptDisable))
        {
        USB_MHDRC_VDBG("usbMhdrcHardwareInterruptDisable(): "
                      "Invalid parameter, %s is NULL\n",
                      ((NULL == pMHDRC) ? "pMHDRC" :
                      "pMHDRC->PlatformData.pHwInterruptDisable"),
                       2, 3, 4, 5 ,6);

        return;
        }

    /* Enable the interrupts for the CPU */

    if (vxbIntDisable (pMHDRC->pDev,
                      0,
                      (pMHDRC->PlatformData.pUsbCoreIsr),
                      (void *)pMHDRC) == ERROR)
        {
        USB_MHDRC_ERR("Error disabling core intrrupts\n", 1, 2, 3, 4, 5 ,6);

        /* Do not return, but continue to disable the interrupt hardware */
        }

    (pMHDRC->PlatformData.pHwInterruptDisable)((void *)pMHDRC);

    return ;
    }

/*******************************************************************************
*
* usbMhdrcPlatformHardwareInit - perform the platform specific initialization
*
* This routine performs the platform specific initialization.
*
* RETURNS: OK, or ERROR if failed to initialize
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbMhdrcPlatformHardwareInit
    (
    pUSB_MUSBMHDRC          pMHDRC
    )
    {
    HCF_DEVICE *            pHcf;
    UINT32                  resourceVal;
    UINT8                   i;
    VXB_DEVICE_ID           pDev;
    UINT8                   uDevCtl;

    /*
     * The pMHDRC should have been allocated by the caller
     * and the pMHDRC->pDev should have been set
     */

    if ((pMHDRC == NULL) || (pMHDRC->pDev == NULL))
        {
        USB_MHDRC_ERR("USB_MUSBMHDRC not allocated\n",
                  1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Get the VxBus Device instance */

    pDev = pMHDRC->pDev;

    for (i = 0; i < VXB_MAXBARS; i++)
        {
        if (pDev->regBaseFlags[i] != VXB_REG_NONE)
            {
            if(vxbRegMap (pDev, i, &pMHDRC->pRegAccessHandle) == ERROR)
                {
                USB_MHDRC_ERR("vxbRegMap Fail\n",
                          1, 2, 3, 4, 5, 6);

                return ERROR;
                }

            /* Save the register base just to keep things a little simpler */

            pMHDRC->uRegBase = (ULONG)pDev->pRegBase[i];

            USB_MHDRC_DBG("USB_MUSBMHDRC regbase %p\n",
                      pMHDRC->uRegBase, 2, 3, 4, 5, 6);

            break;
            }
        }

    if (i >= VXB_MAXBARS)
        {
        USB_MHDRC_ERR ("Unable to locate usable BAR \n",
                      1, 2, 3, 4, 5 ,6);

        return ERROR;
        }

    /* Initialize the spinlock */

    SPIN_LOCK_ISR_INIT(&pMHDRC->spinLock, 0);

    /* Populate the required resources and parameters */

    switch (pDev->busID)
        {
        case VXB_BUSID_PLB:

            /*
             * The underlying bus type is PLB. Query the hwconf file for
             * address conversion functions and populate the required
             * function pointers
             */

            pHcf = hcfDeviceGet (pDev);

            if (NULL == pHcf)
                {
                USB_MHDRC_ERR ("hcfDeviceGet returns NULL\n",
                              1, 2, 3, 4, 5 ,6);

                return ERROR;
                }

             if (ERROR != devResourceGet (pHcf, "cpuToBus", HCF_RES_ADDR,
                            (void *)&(pMHDRC->pCpuToBus)))
                {
                USB_MHDRC_VDBG ("hcfDeviceGet returns cpuToBus %p\n",
                                pMHDRC->pCpuToBus, 2, 3, 4, 5 ,6);
                }

             if (ERROR != devResourceGet (pHcf, "busToCpu", HCF_RES_ADDR,
                            (void *)&(pMHDRC->pBusToCpu)))
                {
                USB_MHDRC_VDBG ("hcfDeviceGet returns cpuToBus %p\n",
                                pMHDRC->pBusToCpu, 2, 3, 4, 5 ,6);
                }

             if (ERROR != devResourceGet (pHcf, "rootHubNumPorts", HCF_RES_INT,
                            (void *)(&resourceVal)))
                {
                pMHDRC->uRootHubNumPorts = (UINT8)resourceVal;
                }
             else
                {
                /* Normally we have only 1 root port for this IP */

                pMHDRC->uRootHubNumPorts = 1;
                }

             if (ERROR != devResourceGet (pHcf, "numDmaChannels", HCF_RES_INT,
                            (void *)(&resourceVal)))
                {
                pMHDRC->uNumDmaChannels = (UINT8)resourceVal;
                }
             else
                {
                /*
                 * If the user does not specify this resource,
                 * then it indicates no DMA channels are available.
                 */

                pMHDRC->uNumDmaChannels = 0;
                }

             if (ERROR != devResourceGet (pHcf, "numEndpoints", HCF_RES_INT,
                            (void *)(&resourceVal)))
                {
                pMHDRC->uNumEps = (UINT8)resourceVal;
                }
             else
                {
                pMHDRC->uNumEps = (UINT8)USB_MHDRC_ENDPOINT_MAX;
                }

             if (ERROR != devResourceGet (pHcf, "usbHwInit", HCF_RES_ADDR,
                            (void *)&(pMHDRC->pUsbHwInit)))
                {
                USB_MHDRC_VDBG ("hcfDeviceGet returns pUsbHwInit %p\n",
                                pMHDRC->pUsbHwInit, 2, 3, 4, 5 ,6);
                }

             if (ERROR != devResourceGet (pHcf, "usbPhyUlpiRead", HCF_RES_ADDR,
                            (void *)&(pMHDRC->pPhyUlpiRegRead)))
                {
                USB_MHDRC_VDBG ("hcfDeviceGet returns pPhyUlpiRegRead %p\n",
                                pMHDRC->pPhyUlpiRegRead, 2, 3, 4, 5 ,6);
                }

             if (ERROR != devResourceGet (pHcf, "usbPhyUlpiWrite", HCF_RES_ADDR,
                            (void *)&(pMHDRC->pPhyUlpiRegWrite)))
                {
                USB_MHDRC_VDBG ("hcfDeviceGet returns pPhyUlpiRegWrite %p\n",
                                pMHDRC->pPhyUlpiRegWrite, 2, 3, 4, 5 ,6);
                }

             if (ERROR != devResourceGet (pHcf, "usbPhyIdPinStateGet", HCF_RES_ADDR,
                            (void *)&(pMHDRC->pPhyIdPinStateGet)))
                {
                USB_MHDRC_VDBG ("hcfDeviceGet returns pPhyIdPinStateGet %p\n",
                                pMHDRC->pPhyIdPinStateGet, 2, 3, 4, 5 ,6);
                }

             if (ERROR != devResourceGet (pHcf, "usbPhyVbusPinStateGet", HCF_RES_ADDR,
                            (void *)&(pMHDRC->pPhyVbusPinStateGet)))
                {
                USB_MHDRC_VDBG ("hcfDeviceGet returns pPhyVbusPinStateGet %p\n",
                                pMHDRC->pPhyVbusPinStateGet, 2, 3, 4, 5 ,6);
                }

             if (ERROR != devResourceGet (pHcf, "platformType", HCF_RES_INT,
                    (void *)(&resourceVal)))
                {
                pMHDRC->uPlatformType = (UINT8)resourceVal;
                }

            /* If the platform gives such resouce, get it */

            if (ERROR != devResourceGet (pHcf, "useExternalPower", HCF_RES_INT,
                           (void *)(&resourceVal)))
                {
                pMHDRC->uUseExtPower = (UINT8)resourceVal;
                }

            if (ERROR != devResourceGet (pHcf, "dmaType", HCF_RES_INT,
                           (void *)(&resourceVal)))
                {
                pMHDRC->uDmaType = (UINT8)resourceVal;
                }

            if (ERROR != devResourceGet (pHcf, "dmaEnabled", HCF_RES_INT,
                           (void *)(&resourceVal)))
                {
                pMHDRC->bDmaEnabled = resourceVal;
                }

            if (ERROR != devResourceGet (pHcf, "usbssRegBase", HCF_RES_INT,
                           (void *)(&resourceVal)))
                {
                pMHDRC->uUsbssRegBase = resourceVal;
                }

            /*
             * If the BSP specific initialization is required,
             * do it now before any other hardware access.
             */

            if (pMHDRC->pUsbHwInit != NULL)
                pMHDRC->pUsbHwInit();
            break;

        case VXB_BUSID_PCI:
            /* Currently no PCI variants, fall through! */
        default:

            USB_MHDRC_ERR("busID %p invalid\n ",
                        pDev->busID, 2, 3, 4, 5, 6);

            return ERROR;
        }

    /* Setup the platform data */

    if (usbMhdrcPlatformSetup ((void *)pMHDRC) == ERROR)
        {
        USB_MHDRC_ERR("usbMhdrcPlatformSetup fail\n",
                      1, 2, 3, 4, 5 ,6);
        return ERROR;
        }

    /* Disable the interrupt */

    usbMhdrcHardwareIntrDisable(pMHDRC);

    /* Register the interrupt handler for the IRQ */

    if (ERROR == (vxbIntConnect (pDev,
                                 0,
                                 (pMHDRC->PlatformData.pUsbCoreIsr),
                                 (void *)(pMHDRC))))
        {
        USB_MHDRC_ERR("Error hooking the core ISR %p for DEV %p\n",
                      pMHDRC->PlatformData.pUsbCoreIsr, pDev, 3, 4, 5 ,6);
        return ERROR;
        }

    usbMhdrcPlatformDmaSetup(pMHDRC);

    usbMhdrcUlpiWrite(pMHDRC, USB_ULPI_FUNCTION_CTL  + USB_ULPI_SET_ADJ,
                  ULPI_FUNC_CTRL_RESET);

    /* Make sure the ID pull-up is set */

    usbMhdrcUlpiWrite(pMHDRC, USB_ULPI_OTG_CTL + USB_ULPI_SET_ADJ,
                      ULPI_OTG_CTL_ID_PULL_UP);

    uDevCtl = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_DEVCTL);

    if (uDevCtl & USB_MHDRC_DEVCTL_HM)
        {
        /* Init the FIFO for host mode */

        usbMhdrcHcdFIFOInit(pMHDRC);
        }
    else
        {
        /* Init the FIFO for target mode */

        usbMhdrcTcdFIFOInit(pMHDRC);
        }

    return OK;
    }

/*******************************************************************************
*
* usbMhdrcPlatformHardwareUnInit - perform the platform specific uninitialization
*
* This routine performs the platform specific uninitialization.
*
* RETURNS: OK, or ERROR if failed to uninitialize
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbMhdrcPlatformHardwareUnInit
    (
    pUSB_MUSBMHDRC          pMHDRC
    )
    {
    /* Check the validity of the parameter */

    if ((pMHDRC == NULL) || (pMHDRC->pDev == NULL))
        {
        USB_MHDRC_ERR("Invalid parameter, pMHDRC NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return ERROR;
        }

    /* Disable the interrupts */

    if (vxbIntDisable (pMHDRC->pDev,
                       0,
                       (pMHDRC->PlatformData.pUsbCoreIsr),
                       (void *)(pMHDRC)) == ERROR)
        {
        USB_MHDRC_ERR("Failure in vxbIntDisable\n",
                     1, 2, 3, 4, 5 ,6);

        return ERROR;
        }

    if (vxbIntDisconnect (pMHDRC->pDev,
                      0,
                      (pMHDRC->PlatformData.pUsbCoreIsr),
                      (void *)(pMHDRC)) != OK)
        {
        USB_MHDRC_ERR("Failure in vxbIntDisconnect\n",
                     1, 2, 3, 4, 5 ,6);

        return ERROR;
        }

    if (usbMhdrcPlatformHsDmaUnSetup(pMHDRC) != OK)
        {
        USB_MHDRC_ERR("Failure in usbMhdrcPlatformHsDmaUnSetup\n",
                     1, 2, 3, 4, 5 ,6);

        return ERROR;
        }

    return OK;
    }


/*******************************************************************************
*
* usbMhdrcUlpiRead - read the ULPI register
*
* This routine is to read the ULPI register. Strict users should check the
* return value to make sure the ULPI access is sucessfull.
*
* RETURNS: OK when the ULPI access is sucessfull, ERROR when failed.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbMhdrcUlpiRead
    (
    pUSB_MUSBMHDRC  pMHDRC,
    UINT8           uOffset,
    UINT8 *         pData
    )
    {
    if (pMHDRC->pPhyUlpiRegRead != NULL)
        {
        USB_MHDRC_VDBG("usbMhdrcUlpiRead - Calling BSP pPhyUlpiRegRead\n",
            1, 2, 3, 4, 5 ,6);

        return pMHDRC->pPhyUlpiRegRead(uOffset, pData);
        }

    switch (pMHDRC->uPlatformType)
        {
        case USB_MHDRC_PLATFORM_OMAP35xx:
#if  defined (USB_MHDRC_OMAP)
            return usbMhdrcOmap3UlpiRead(pMHDRC, uOffset, pData);
#else
            printf ("usbMhdrcUlpiRead(): USB_MHDRC_OMAP not defined\n");
            return ERROR;
#endif
        default:
            break;
        }
    return OK;
    }

/*******************************************************************************
*
* usbMhdrcUlpiWrite - write the ULPI register
*
* This routine is to read the ULPI register. Strict users should check the
* return value to make sure the ULPI access is sucessfull.
*
* RETURNS: OK when the ULPI access is sucessfull, ERROR when failed.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbMhdrcUlpiWrite
    (
    pUSB_MUSBMHDRC  pMHDRC,
    UINT32          uOffset,
    UINT8           uData
    )
    {
    if (pMHDRC->pPhyUlpiRegWrite != NULL)
        {
        USB_MHDRC_VDBG("usbMhdrcUlpiWrite - Calling BSP pPhyUlpiRegWrite\n",
            1, 2, 3, 4, 5 ,6);

        return pMHDRC->pPhyUlpiRegWrite((UINT8)uOffset, uData);
        }

    switch (pMHDRC->uPlatformType)
        {
        case USB_MHDRC_PLATFORM_OMAP35xx:
#if  defined (USB_MHDRC_OMAP)
            return usbMhdrcOmap3UlpiWrite(pMHDRC, uOffset, uData);
#else
            printf ("usbMhdrcUlpiWrite(): USB_MHDRC_OMAP not defined\n");
            return ERROR;
#endif
        default:
            break;
        }

    return OK;
    }


/*******************************************************************************
*
* usbMhdrcPlatformDmaSetup - configure platform DMA based on platform type
*
* This routine configures the platform DMA according to the platform type.
*
* RETURNS: OK, or ERROR if failed to configure the platform DMA.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbMhdrcPlatformDmaSetup
    (
    pUSB_MUSBMHDRC  pMHDRC
    )
    {
#if defined (USB_MHDRC_DMA_ENABLE)
    if (pMHDRC->bDmaEnabled)
        {
        switch (pMHDRC->uPlatformType)
            {
            case USB_MHDRC_PLATFORM_OMAP35xx:
#if  defined (USB_MHDRC_OMAP)
            if (usbMhdrcPlatformHsDmaSetup((void *)pMHDRC) == ERROR)
                {
                USB_MHDRC_ERR("usbMhdrcPlatformHsDmaSetup fail\n",
                              1, 2, 3, 4, 5 ,6);
                return ERROR;
                }
#else
            printf ("usbMhdrcPlatformDmaSetup(): USB_MHDRC_OMAP not defined\n");
            return ERROR;
#endif
            break;
          case USB_MHDRC_PLATFORM_CENTAURUS:
               /* fall through */

          case USB_MHDRC_PLATFORM_CENTAURUS_TI816X:

#if  defined (USB_MHDRC_CENTAURUS)
           if (usbMhdrcPlatformCppiDmaSetup((void *)pMHDRC) == ERROR)
                {
                USB_MHDRC_ERR("usbMhdrcPlatformCppiDmaSetup fail\n",
                              1, 2, 3, 4, 5 ,6);
                return ERROR;
                }
#else
            printf ("usbMhdrcPlatformDmaSetup(): USB_MHDRC_CENTAURUS not defined\n");
            return ERROR;
#endif
            break;

          case USB_MHDRC_PLATFORM_DAVINIC:
          case USB_MHDRC_PLATFORM_OMAPL13x:
          default:
              return ERROR;
         }
       }
#endif /* end of USB_MHDRC_DMA_ENABLE */
    return OK;
    }

/*******************************************************************************
*
* usbMhdrcPlatformModeChange - perform the platform specific mode change action
*
* This routine performs the platform specific mode change action.
*
* RETURNS: OK, or ERROR if fail
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbMhdrcPlatformModeChange
    (
    pUSB_MUSBMHDRC  pMHDRC,
    USBOTG_MODE     uMode
    )
    {
    if (NULL == pMHDRC)
        {
        USB_MHDRC_ERR("usbMhdrcPlatformModeChange(): Invalid parameter: pMHDRC\n",
                      1, 2, 3, 4, 5 ,6);
        return ERROR;
        }

    USB_MHDRC_DBG("usbMhdrcPlatformModeChange pMHDRC %p, uMode %d\n",
                  pMHDRC, uMode, 3, 4, 5, 6);

    switch (pMHDRC->uPlatformType)
        {
        case USB_MHDRC_PLATFORM_CENTAURUS:
            /* fall through */

        case USB_MHDRC_PLATFORM_CENTAURUS_TI816X:

            usbMhdrcCentaurusModeChange(pMHDRC, uMode);
            break;
        default:
            break;
        }

    if (uMode == USBOTG_MODE_HOST)
        usbMhdrcHcdFIFOInit(pMHDRC);
    else
        usbMhdrcTcdFIFOInit(pMHDRC);

    return OK;
    }

