/* usbUhcdScheduleQWaitForSignal.c - USB UHCD HCD ISR support routines */

/*
 * Copyright (c) 2003-2005, 2007-2010, 2012, 2014 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2003-2005, 2007-2010, 2012, 2014 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
Modification history
--------------------
02b,16jul14,wyy  Unify APIs of USB message to support error report (VXW6-16596)
02a,05nov12,ljg  Remove member halted from USB_UHCD_QH (WIND00386978)
01z,25jun10,w_x  Remove _VXB_DMABUFSYNC_DMA_POSTWRITE for write (WIND00218556)
01y,18may10,w_x  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
01x,10mar10,j_x  Changed for USB debug (WIND00184542)
01w,13jan10,ghs  vxWorks 6.9 LP64 adapting
01v,17sep09,y_l  Code Coverity CID(6312, 6313, 6314): NULL pointer check
                 (WIND00182326)
01u,20jun09,w_x  Correct modification time for WIND00156002
01t,21apr09,w_x  Correct data toggle updating code
01s,20mar09,w_x  Added support for UHCD_POLLING_MODE
01r,20mar09,w_x  Change the non-isoc request queue management (WIND00156002)
01q,04jun08,w_x  Removed job queue mechanism; changed URB callback after
                 request deleted from request list.(WIND00121282 fix)
01p,30aug07,adh  Defect WIND00089969 fix
01o,19May07,jrp  Replaced taskSpawns with Job Queue.
01n,17nov05,ami  Queueing of the request handled properly (Fix for SPR #114478)
01m,20apr05,pdg  Fix for Iomega device not working
01l,03apr05,ami  Correction of Non-Pci changes
01k,29mar05,pdg  Corrected non-PCI changes mistakes
01j,28mar05,pdg  non-PCI changes
01i,14mar05,ami  SPR #106153 (Modification for handling data toggle on a stall
                 condition)
01h,02mar05,ami  SPR #106383 (Max UHCI Host Controller Issue)
01g,31jan05,pdg  Modifying comment for short packet handling
01f,28jan05,pdg  Short packet handling modified
01e,18jan05,ami  Short Packet on Control Transfer handled (SPR #104979 Fix)
01d,15oct04,ami  Refgen Changes
01c,28jul03,mat  Endian Related Changes.
01b,26jun03,mat  changing the code to WRS standards.
01a,25apr03,ram  written.
*/

/*
DESCRIPTION

This file contains the handlers that would be invoked by the
ISR when relevent interrupts occur.

INCLUDE FILES:  usb2/usbOsal.h, usb2/usbHst.h, usb2/usbUhcdCommon.h,
usb2/usbUhcdScheduleQueue.h, usb2/usbUhcdSupport.h,
usb2/usbUhcdScheduleQSupport.h, usb2/usbUhci.h

*/


/*
 INTERNAL
 ******************************************************************************
 * Filename         : usbUhcdScheduleQWaitForSignal.c
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      :  This file contains the handlers that would be
 *                     invoked by the ISR when relevent interrupts occur.
 *
 *
 ******************************************************************************/

/* includes */

#include <usb/usbOsal.h>
#include <usb/usbHst.h>
#include "usbUhcdCommon.h"
#include "usbUhcdScheduleQueue.h"
#include "usbUhcdSupport.h"
#include "usbUhcdScheduleQSupport.h"
#include "usbUhci.h"

/* forward declarations */

void usbUhcdCleartdOnSysError
    (
    pUSB_UHCD_DATA pHCDData
    );

void usbUhcdCompletedTdHandler
    (
    ULONG ptrHCDData
    );

/* externs */

/* Pointer to the global array of HOst controller structures.*/

extern pUSB_UHCD_DATA * g_pUHCDData;

/* Prototype of the the function to reset the UHCD Host Controller */

extern void usbUhcdHcReset
    (
    pUSB_UHCD_DATA pHCDData
    );


/*******************************************************************************
*
* usbUhcdCompletedTdHandler - handle the completed TDs
*
* This routine is spawned as a task and is used to handle the completed TDs
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbUhcdCompletedTdHandler
    (
    ULONG ptrHCDData
    )
    {
    pUSB_UHCD_DATA  pHCDData = (pUSB_UHCD_DATA)ptrHCDData;

    UINT16          uIntStatus;

    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT(
        USB_UHCI_WV_EVENT_HANDLER,
        "usbUhcdCompletedTdHandler() starts",
        USB_UHCD_WV_FILTER);
    /*
     * This is an infinite loop performing the following operations
     * 1. Waits on the signalling from the ISR indicating that the transfer
     *    is completed.
     * 2. Call the function to process the completed TDs.
     * 3. Enable the interrupts
     * 4. The steps 1 to 4 are repeated again.
     */

    while (1)
        {
        #ifndef UHCD_POLLING_MODE

        /* Wait infinitely for a semaphore to be signalled from ISR */

        if (OS_WAIT_FOR_EVENT (pHCDData->tdCompleteSem, WAIT_FOREVER)
            == ERROR)
            {
            USB_UHCD_ERR("OS_WAIT_FOR_EVENT return ERROR\n",
                0, 0, 0, 0, 0, 0);

            return;
            }

        #else /* UHCD_POLLING_MODE */

        uIntStatus = USB_UHCD_READ_WORD (pHCDData->pDev, USB_UHCD_USBSTS);

        /* Check if any interrupt status bits are set - only for bits [0:5] */

        if (uIntStatus & USB_UHCD_USBSTS_MASK)
            {
            /* Update the interrupt status register into the HCD data structure */

            pHCDData->uIntStatus |= uIntStatus;

            /* Clears the interrupt status bits by writing '1' to it. */

            USB_UHCD_WRITE_WORD(pHCDData->pDev, USB_UHCD_USBSTS, uIntStatus);
            }
        else
            {
            /* Reschedule to give other tasks a chance to run */

            OS_RESCHEDULE_THREAD();

            continue;
            }

        #endif /* UHCD_POLLING_MODE */

        /* Take the spin lock */

        SPIN_LOCK_ISR_TAKE (&pHCDData->spinLockIsr);

        /* Save the interrupt status as local variable */

        uIntStatus = pHCDData->uIntStatus;

        /* Clear the interrupt status */

        pHCDData->uIntStatus = 0;

        /* Release the spin lock */

        SPIN_LOCK_ISR_GIVE (&pHCDData->spinLockIsr);

        /* Check if there is an error interrupt */

        if (uIntStatus & USB_UHCD_TRANSACTION_ERR_IOC)
            {
            /* Handle the transfer completion */

            usbUhcdProcessTransferCompletion(pHCDData);
            }

        if (uIntStatus & USB_UHCD_HC_HALTED)
            {
            /*
             * The Host Conrtoller sets this bit to 1,
             * after it has stopped executing as
             * a result of the RUN/STOP bit being set to 0,
             * either by s/w or by the HC.
             */

            USB_UHCD_ERR("**** HC HALTED ERROR *****\n", 0, 0, 0, 0, 0, 0);

            if (uIntStatus & USB_UHCD_HST_SYSTEM_ERR)
                {
                /*
                 * Bit 3 of USBSTS register indicates a Host System Error
                 * (ie. the Host Controller has detected a serious error
                 * during the host system access)
                 */

                USB_UHCD_ERR("**** SYSTEM ERROR ***** \n",
                    1, 2, 3, 4, 5, 6);

                /* Set the RUN/STOP bit in the command register */

                usbUhcdRegxSetBit(pHCDData, USB_UHCD_USBCMD, 0);

                /*
                 * Remove all the data structures created for the
                 * associative host controller.
                 * After removing all the stuff, host controller
                 * reset is applied.
                 */

                usbUhcdCleartdOnSysError(pHCDData);

                /* Call the function to reset the UHCI Host Controller */

                usbUhcdHcReset (pHCDData);
				
                /* Post a message to report error */

                usbMsgPost(USBMSG_HCD_UHCD_ERROR,
                           pHCDData->pDev,
                           (void *)((unsigned long)pHCDData->uBusIndex));
                }

            if (uIntStatus & USB_UHCD_HC_PROCESS_ERR)
                {
                /*
                 * Bit 4 of USBSTS register indicates a Host Controller
                 * Process Error (ie. the Host Controller has detected
                 * a fatal error)
                 */

                USB_UHCD_ERR("**** HC PROCESS ERROR ***** \n",
                    1, 2, 3, 4, 5, 6);

                /* Set the RUN/STOP bit in the command register */

                usbUhcdRegxSetBit(pHCDData, USB_UHCD_USBCMD, 0);

                /*
                 * Remove all the data structures created for the
                 * associative host controller. After removing all
                 * the stuff, make the RUN?STOP bit 1 to perform
                 * scheduling again.
                 */

                usbUhcdCleartdOnSysError(pHCDData);

                }
            }
        }/* End of while () */

    }/* End of usbUhcdCompletedTdHandler() */


/*******************************************************************************
*
* usbUhcdProcessDelayedPipeList - process any delayed pipe list modification
*
* This routine process any delayed pipe list modification.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbUhcdProcessDelayedPipeList
    (
    pUSB_UHCD_DATA  pHCDData
    )
    {
    pUSB_UHCD_PIPE  pHCDPipe;

    /*
     * If there is any delayed pipe addition happened during the URB completion
     * processing, we will add them to the proper active pipe list here.
     */

    /* Exclusively access the reclamation list */

    OS_WAIT_FOR_EVENT(pHCDData->ReclamationListSynchEventID, OS_WAIT_INFINITE);

    pHCDPipe = pHCDData->pDelayedPipeAdditionList;

    while (pHCDPipe != NULL)
        {
        /* Add the pipe into the HCD maintained pipe list */

        if (pHCDPipe->uEndpointType != USBHST_ISOCHRONOUS_TRANSFER)
            {
            USB_UHCD_ADD_TO_NON_ISOCH_PIPE_LIST(pHCDData, pHCDPipe);
            }
        else
            {
            USB_UHCD_ADD_TO_ISOCH_PIPE_LIST(pHCDData, pHCDPipe);
            }

        /* Move the head pointer to the next of delayed pipe addition */

        pHCDData->pDelayedPipeAdditionList = pHCDPipe->pAltNext;

        /* Go to the next pipe that was delayed */

        pHCDPipe = pHCDData->pDelayedPipeAdditionList;
        }

    /* Release the exclusive access */

    OS_RELEASE_EVENT(pHCDData->ReclamationListSynchEventID);

    /*
     * Modification to the active pipe lists should be protected by
     * ActivePipeListSynchEventID.
     */

    /* Exclusively access the reclamation list */

    OS_WAIT_FOR_EVENT(pHCDData->ActivePipeListSynchEventID, OS_WAIT_INFINITE);

    /*
     * If there is any delayed pipe removal happened during the URB completion
     * processing, we will add them to the proper reclamnation pipe list here.
     */

    pHCDPipe = pHCDData->pDelayedPipeRemovalList;

    while (pHCDPipe != NULL)
        {
        /* Remove the pipe from the original active pipe list */

        if (pHCDPipe->uEndpointType != USBHST_ISOCHRONOUS_TRANSFER)
            {
            USB_UHCD_REMOVE_NON_ISOCH_PIPE(pHCDData, pHCDPipe);
            }
        else
            {
            USB_UHCD_REMOVE_ISOCH_PIPE(pHCDData, pHCDPipe);
            }

        /* Move the head pointer to the next of delayed pipe addition */

        pHCDData->pDelayedPipeRemovalList = pHCDPipe->pAltNext;

        /* Delete the resources associated with the pipe */

        usbUhcdDeletePipeContext(pHCDData, pHCDPipe);

        /* Go to the next pipe that was delayed */

        pHCDPipe = pHCDData->pDelayedPipeRemovalList;
        }

    /* Release the exclusive access */

    OS_RELEASE_EVENT(pHCDData->ActivePipeListSynchEventID);
    }

/*******************************************************************************
*
* usbUhcdProcessNonIsochRequestCompletion - process non-isoch request completion
*
* This routine is to process non-isoch request completion.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbUhcdProcessNonIsochRequestCompletion
    (
    pUSB_UHCD_DATA          pHCDData,
    pUSB_UHCD_PIPE          pHCDPipe,
    pUSB_UHCD_REQUEST_INFO  pRequest
    )
    {
    /* Pointer to the URB */

    pUSBHST_URB     pUrb;

    /* Index of the host controller */

    UINT32          uBusIndex;

    /* Pointer to the temporary TD */

    pUSB_UHCD_TD    pTempTD = NULL;

    /* To hold the pointer to the TD which is presently serviced by HCD */

    USB_UHCD_TD *                       pCurrentTD = NULL;

    USB_UHCD_TD *                       pLastTD = NULL;

    USB_UHCD_TD *                       pHeadTD = NULL;

    USB_UHCD_TD *                       pTailTD = NULL;

    /* To hold the pointer to the temporary TD */

    USB_UHCD_TD *                       pToggleTD = NULL;

    /* To hold the flag indicating that the request is halted */

    UINT8                               bReqThatHalted = 0;

    /* To hold the flag indicating that a short packet is received*/

    UINT8                               bReqWithSp = 0;

    /* To hold the last data toggle value received */

    UINT8                               uLastToggle = 0;

    UINT32                              uActlen = 0;

    UINT32                              uMaxLen = 0;

    UINT32                              uTotalLen =0;

    /* Exclusively access the pipe request list */

    OS_WAIT_FOR_EVENT(pHCDPipe->PipeSynchEventID, OS_WAIT_INFINITE);

    /* Get the tail of the TD list */

    pTailTD = (pUSB_UHCD_TD)pRequest->pTail;

    /* Get the head of the TD list */

    pHeadTD = (pUSB_UHCD_TD)pRequest->pHead;

    /* Check to make sure the request is valid */

    if ((NULL == pHeadTD) ||
        (NULL == pTailTD) ||
        (NULL == pHCDPipe->pQH) ||
        (NULL == pRequest->pUrb) ||
        (NULL == pRequest->pUrb->pHcdSpecific)) /* canceled */
        {
        USB_UHCD_WARN(
               "NonIsochRequestCompletion got an invalid request!\n"
               "pTDHead %p pTDTail %p pQH %p pUrb %p\n",
               pHeadTD, pTailTD, pHCDPipe->pQH, pRequest->pUrb, 0, 0);

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

        return;
        }

    /* Retrieve the bus index */

    uBusIndex = pHCDData->uBusIndex;

    /* Get the URB pointer of this request */

    pUrb = pRequest->pUrb;

    /*
     * If the head is still active, do not traverse the list of TDs.
     * Goto the next request instead
     */

    if ((USB_UHCD_SWAP_DATA(uBusIndex, pHeadTD->dWord1Td) &
        USBUHCD_TDCS_STS_ACTIVE) != 0)
        {
        USB_UHCD_VDBG("NonIsochRequestCompletion - "
            "request is not yet serviced\n",
            1, 2, 3, 4, 5, 6);

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

        return;
        }

    bReqThatHalted = 0;

    bReqWithSp = 0;

    uTotalLen = 0;

    pLastTD = NULL;

    /*
     * Check if the current request concluded with a Short packet &
     * TD has been processed
     */

    pCurrentTD = pRequest->pHead;

    while (pCurrentTD)
        {
        if ((USB_UHCD_SWAP_DATA(uBusIndex, pCurrentTD->dWord1Td) &
             USBUHCD_TDCS_STS_ACTIVE) == 0)
            {
            uActlen = USBUHCD_TDCS_ACTLEN(
                USB_UHCD_SWAP_DATA(uBusIndex, pCurrentTD->dWord1Td));

            uMaxLen = USBUHCD_TDTOK_MAXLEN(
                USB_UHCD_SWAP_DATA(uBusIndex, pCurrentTD->dWord2Td));

            /* Update the total length transfered */

            if (uActlen != USBUHCD_TDCS_ACTLEN_ZERO)
                {
                if ((pRequest->pHCDPipe->uEndpointType ==
                     USBHST_CONTROL_TRANSFER) &&
                    ((pCurrentTD == pRequest->pTail) ||
                     (pCurrentTD == pRequest->pHead)))
                    {
                    /* Ignore length for setup and status packet */

                    uTotalLen += 0;
                    }
                else
                    {
                    uTotalLen += uActlen + 1 ;
                    }
                }

            if (uActlen != uMaxLen)
                {
                USB_UHCD_VDBG("NonIsochRequestCompletion - "
                    "current request is short & TD is processed,"
                    "uActlen = %d uMaxLen = %d \n",
                    uActlen, uMaxLen, 3, 4, 5, 6);

                /*
                 * If short packet transfer occured,
                 * the remaining TDs should not have been reached
                 */

                pLastTD = pCurrentTD;

                bReqWithSp = 1;

                break;
                }

            /* Determine if pipe has halted */

            if (USB_UHCD_SWAP_DATA(uBusIndex, pCurrentTD->dWord1Td)
                & USBUHCD_TDCS_STS_STALLED)
                {
                USB_UHCD_VDBG("NonIsochRequestCompletion - endpoint halted! "
                    "endPointNum=0x%X endPointType=0x%X, deviceNum=0x%X \n",
                    pHCDPipe->uEndpointAddress,
                    pHCDPipe->uEndpointType,
                    pHCDPipe->uAddress, 4, 5, 6);

                pLastTD = pCurrentTD;

                bReqThatHalted = 1;

                break;
                }
            }

        /* If we reached the tail of the request, then stop scanning */

        if (pCurrentTD == pRequest->pTail)
            break;

        /* Go to the next TD of this request */

        pCurrentTD = pCurrentTD->hNextTd;
        }  /* end of for loop */

    /* Check if there is a short packet on control in pipe */

    /* Reschedule only for control in pipe */

    if ((pHCDPipe->uEndpointType == USBHST_CONTROL_TRANSFER) && bReqWithSp &&
        ((USB_UHCD_SWAP_DATA(uBusIndex, pLastTD->dWord2Td) &
          USBUHCD_TDTOK_PID_MASK) == USBUHCD_TDTOK_PID_IN))
        {
        /*
         * Check if the tail is still active
         * Here it cannot be checked against the ACTLENGTH != 0x7FF.
         * The host controller updates the ACTLENGTH to 0x7FF even on a NAK.
         * Check against the ACTIVE bit to find out whether the tail is
         * still active.
         */

        if ((USB_UHCD_SWAP_DATA(uBusIndex, pRequest->pTail->dWord1Td)
            & USBUHCD_TDCS_STS_ACTIVE) != 0)
            {
            /*
             * If the control pipe is stalled,
             * retry for the whole transfer. This has been seen
             * when several hubs and devices are in the tree
             * and huge transfers are in progress, sometimes some
             * hub may return STALL for the hub status request,
             * thus causing the whole tree under the hub get detached
             * even though there is no detach operation.
             *
             * Retry the whole control request has been seen to
             * workaround this.
             */

            if ((USB_UHCD_SWAP_DATA(uBusIndex, pCurrentTD->dWord1Td)
                & USBUHCD_TDCS_STS_STALLED) && pRequest->uRetryCount)
                {
                USB_UHCD_VDBG("NonIsochRequestCompletion - "
                              "retry for control pipe " \
                              "of hDevice=0x%X, uTransferLength=%d, " \
                              "uActlen=%d, retryCount=%d \n",
                              pUrb->hDevice,
                              pUrb->uTransferLength,
                              uActlen,
                              pRequest->uRetryCount, 5, 6);

                pRequest->uRetryCount--;

                pCurrentTD = pRequest->pHead;

                /*
                 * Loop to restore the status and control fields
                 * to re-activate the request TDs until the
                 * latest STALLed TD is reached
                 */

                while (pCurrentTD)
                    {
                    /* Clear the STATUS and ACTLEN fields */

                    pCurrentTD->dWord1Td = pCurrentTD->dWord1Td &
                        USB_UHCD_SWAP_DATA(uBusIndex,
                        (~(USBUHCD_TDCS_STS_MASK | USBUHCD_TDCS_ACTLEN_MASK)));

                    /* Set the ACTIVE bit and allow 3 hardware retries */

                    pCurrentTD->dWord1Td = pCurrentTD->dWord1Td |
                        USB_UHCD_SWAP_DATA(uBusIndex,
                        (USBUHCD_TDCS_STS_ACTIVE | USBUHCD_TDCS_ERRCTR_3ERR));

                    /* If we reach the latest STALLed TD, go out */

                    if (pCurrentTD == pLastTD)
                        break;

                    /* If we reach the tail of the request, go out */

                    if (pCurrentTD == pRequest->pTail)
                        break;

                    /* Otherwise we go to the next TD */

                    pCurrentTD = pCurrentTD->hNextTd;
                    }

                /* Stop the HC */

                USB_UHCD_HALT_HC(pHCDData);

                /* Put this request on schedule again */

                USB_UHCD_QH_LINK_TD_ELEMENT_ACTIVE(uBusIndex,
                                     pHCDPipe->pQH,
                                     pRequest->pHead);

                /* Restart the HC */

                USB_UHCD_RESTART_HC(pHCDData);

                /* Release the exclusive access */

                OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

                return;
                }

            /* OUT token not sent, schedule OUT token */

            if (pHCDPipe->bOutTokenScheduled == FALSE)
                {
                USB_UHCD_VDBG("NonIsochRequestCompletion - "
                              "schedule OUT token for control " \
                              "pipe of hDevice=0x%X, uTransferLength=%d " \
                              "uActlen=0x%X, uTotalLen=%d \n",
                              pUrb->hDevice,
                              pUrb->uTransferLength,
                              uActlen, uTotalLen, 5, 6);

                /* Stop the HC */

                USB_UHCD_HALT_HC(pHCDData);

                /* Put the last TD on the schedule */

                USB_UHCD_QH_LINK_TD_ELEMENT_ACTIVE(uBusIndex,
                                     pHCDPipe->pQH,
                                     pRequest->pTail);

                pHCDPipe->bOutTokenScheduled = TRUE;

                /* Restart the HC */

                USB_UHCD_RESTART_HC(pHCDData);

                /* Release the exclusive access */

                OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

                return;
                }
            }
        }

    /*
     * Check if the entire request has been processed (or)
     * if pipe is halted (or)
     * if a bulk/interrupt req that ended with a Short Packet.
     */

    if (((USB_UHCD_SWAP_DATA(uBusIndex, pTailTD->dWord1Td) &
        USBUHCD_TDCS_STS_ACTIVE ) == 0 ) ||
        bReqThatHalted ||
        bReqWithSp)
        {
        USB_UHCD_VDBG("NonIsochRequestCompletion - "
            "pipe is to be deleted or processed or halted \n",
            1, 2, 3, 4, 5, 6);

        /* If the request did not end in a short packet */

        if (!bReqWithSp)
            {
            USB_UHCD_VDBG("NonIsochRequestCompletion - "
                "request didn't end in short packet \n",
                1, 2, 3, 4, 5, 6);

            /* Fill the status */

            usbUhcdFillNonisoStatus (
                USB_UHCD_SWAP_DATA(uBusIndex, pTailTD->dWord1Td),
                                    &(pUrb->nStatus),
                                    pHCDPipe->bMarkedForDeletion,
                                    bReqThatHalted);

            }
        else /* Normal completion */
            {
            USB_UHCD_VDBG("NonIsochRequestCompletion - normal completion \n",
                1, 2, 3, 4, 5, 6);

            /* Fill the status */

             usbUhcdFillNonisoStatus (
                USB_UHCD_SWAP_DATA(uBusIndex, pCurrentTD->dWord1Td),
                                    &(pUrb->nStatus),
                                    pHCDPipe->bMarkedForDeletion,
                                    bReqThatHalted);
            }

        /* Get the actual transfer length */

        pUrb->uTransferLength = uTotalLen;

        if (pHCDPipe->bOutTokenScheduled == TRUE)
            {
            pHCDPipe->bOutTokenScheduled = FALSE;
            }

        /* If the pipe is halted, manipulate qHead's qTD ptr */

        if (pUrb->nStatus == USBHST_STALL_ERROR || bReqWithSp)
            {
            USB_UHCD_VDBG("NonIsochRequestCompletion - "
                          "manipulate qHead's qTD for pipe=0x%X " \
                          "on device=0x%X, ep=0x%X, len=%d \n",
                          pHCDPipe,
                          pUrb->hDevice,
                          pUrb->uEndPointAddress,
                          pUrb->uTransferLength, 5, 6);

            /* Stop the HC */

            USB_UHCD_HALT_HC(pHCDData);

            /*
             * Before updating the link pointers, stop the HC from
             * accessing the pointers. This is done by setting the bit
             * saying that this is the last TD in the queue
             */

            USB_UHCD_QH_UNLINK_TD_ELEMENT (uBusIndex, pHCDPipe->pQH);

            /* Restart the HC */

            USB_UHCD_RESTART_HC(pHCDData);

            /* Update the data toggle field - Start */

            /* Fetch the last data toggle used */

            if (pLastTD)
               uLastToggle = pLastTD->uToggle;
            else if (pCurrentTD)
               uLastToggle = pCurrentTD->uToggle;

            /*
             * Toggle the data value only if the endpoint is not
             * halted. If it is halted, reset the data toggle value
             * to zero.
             */

            /* Check if pipe is halted pipe for interrupt or bulk */
            
            if (pUrb->nStatus == USBHST_STALL_ERROR &&
                (pHCDPipe->uEndpointType == USBHST_INTERRUPT_TRANSFER ||
                pHCDPipe->uEndpointType == USBHST_BULK_TRANSFER))
                {
                USB_UHCD_WARN("NonIsochRequestCompletion - pipe is halted \n",
                              1, 2, 3, 4, 5, 6);
            
                uLastToggle = 0;
                }
            else
                {
                uLastToggle = (UINT8)((uLastToggle == 0) ? 1 : 0);
                }

            /* Check if pipe is interrupt or bulk */

            if (pHCDPipe->uEndpointType == USBHST_INTERRUPT_TRANSFER ||
                pHCDPipe->uEndpointType == USBHST_BULK_TRANSFER)
                {
                USB_UHCD_REQUEST_INFO * pTempRequest = NULL;

                USB_UHCD_WARN("NonIsochRequestCompletion - "
                              "fix up toggle for bulk or interrupt pipe \n",
                              1, 2, 3, 4, 5, 6);

                /* Fix data toggle for other TDs */

                pTempRequest = pRequest->pNext;

                while (pTempRequest)
                    {
                    pToggleTD = pTempRequest->pHead;

                    while (pToggleTD)
                        {
                        if (uLastToggle == 1)
                            {
                            USB_UHCD_TD_SET_TOGGLE_BIT (uBusIndex,
                                                        pToggleTD);
                            }
                        else
                            {
                            USB_UHCD_TD_CLR_TOGGLE_BIT (uBusIndex,
                                                        pToggleTD);
                            }

                        pToggleTD->uToggle = uLastToggle;

                        uLastToggle = (UINT8)((uLastToggle == 0) ? 1 : 0);

                        if (pToggleTD == pTempRequest->pTail)
                            break;

                        pToggleTD = pToggleTD->hNextTd;
                        }

                    pTempRequest = pTempRequest->pNext;
                    }
                }

            /* Update the new toggle value for the request */

            pHCDPipe->pQH->uToggle = uLastToggle;

            /* Update the data toggle field - End */
            }

        /* Stop the HC */

        USB_UHCD_HALT_HC(pHCDData);

        /* Update the Terminate bit of the earlier TD/QH. */

        /*
         * Check if it is the head element of the pipe
         * This must be TRUE because it is not possible for
         * the request to be completed out of order!
         */

        if (pHCDPipe->pRequestQueueHead == pRequest)
            {
            /*
             * If it is the only element,
             * update the head and tail pointers to NULL.
             */

            if (pHCDPipe->pRequestQueueTail == pRequest)
                {
                pHCDPipe->pRequestQueueHead = NULL;

                pHCDPipe->pRequestQueueTail = NULL;

                /* Update the head of the TD link to be NULL */

                pHCDPipe->pQH->pTD = NULL;

                /* Indicate that there are no elements in the list */

                USB_UHCD_QH_UNLINK_TD_ELEMENT(uBusIndex, pHCDPipe->pQH);

                }
            else
                {
                /* Update the head of the request */

                pHCDPipe->pRequestQueueHead = pRequest->pNext;

                /* Update the head of the TD list for the endpoint */

                pHCDPipe->pQH->pTD = pRequest->pNext->pHead;

                }
            }
        else
            {
            USB_UHCD_WARN("NonIsochRequestCompletion - "
                          "request completedout of order\n",
                          1, 2, 3, 4, 5, 6);

            }

        /*
         * Check if there are any more TDs to be processed
         * by the HC following the current request.
         *
         * This has to be checked against both HW and SW :
         *
         * (T bit in pRequest->tail->dWord0Td is cleared)
         * and
         * (pRequest->pEndpointNext not NULL)
         */

        /*
         * If the QH is not activated but there are TDs
         * pending on this QH, let's warm it up
         */

        if ((USB_UHCD_SWAP_DATA(uBusIndex, pHCDPipe->pQH->dWord1Qh) &
            USBUHCD_LINK_TERMINATE) && (pHCDPipe->pQH->pTD != NULL))
            {
            /* Update the HC QH's element link pointer */

            USB_UHCD_QH_LINK_TD_ELEMENT_ACTIVE(uBusIndex,
                                               pHCDPipe->pQH,
                                               pHCDPipe->pQH->pTD);
            }

        /* Restart the HC */

        USB_UHCD_RESTART_HC(pHCDData);
        }
    else /* The request is not yet serviced */
        {
        USB_UHCD_VDBG("NonIsochRequestCompletion - "
                      "request is not yet serviced \n",
                      1, 2, 3, 4, 5, 6);

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

        return;
        }/* End of else */

    /*
     * If there is any real data transfer, we should do
     * vxbDmaBufSync and unload the DMA MAP.
     */

    if (pRequest->uRequestLength != 0)
        {
        /* Check if the data transfer is IN direction */

        if (pRequest->uDataDir == USB_UHCD_DIR_IN)
            {
            /* DMA cache invalidate and <copy from bounce buffer> */

            vxbDmaBufSync(pHCDData->pDev,
                pHCDPipe->usrBuffDmaTagId,
                pRequest->usrDataDmaMapId,
                _VXB_DMABUFSYNC_DMA_POSTREAD);
            }
        else
            {
#if 0 /* Don't use _VXB_DMABUFSYNC_DMA_POSTWRITE for now */
            /* DMA cache invalidate and <copy from bounce buffer> */

            vxbDmaBufSync(pHCDData->pDev,
                pHCDPipe->usrBuffDmaTagId,
                pRequest->usrDataDmaMapId,
                _VXB_DMABUFSYNC_DMA_POSTWRITE);
#endif
            }

        /* Unload the Data DMA MAP */

        vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
            pRequest->usrDataDmaMapId);
        }

    /* Control transfer should also deal with the Setup buffer */

    if (pHCDPipe->uEndpointType == USBHST_CONTROL_TRANSFER)
        {
        /* Setup transaction is always DMA OUT */

#if 0 /* Don't use _VXB_DMABUFSYNC_DMA_POSTWRITE for now */

        /* DMA cache invalidate and <copy from bounce buffer> */

        vxbDmaBufSync(pHCDData->pDev,
                pHCDPipe->ctrlSetupDmaTagId,
                pRequest->ctrlSetupDmaMapId,
                _VXB_DMABUFSYNC_DMA_POSTWRITE);
#endif
        /* Unload the control Setup DMA MAP */

        vxbDmaBufMapUnload (pHCDPipe->ctrlSetupDmaTagId,
            pRequest->ctrlSetupDmaMapId);
        }

    /*
     * Return the request info to the pipe which also
     * release the URB from the request, so that the
     * URB callback can submit the URB again just in
     * the callback and it can find free request info
     * to be used.
     *
     * Note: This has to be done before calling the
     * URB callback.
     */

    /* Store the address of the head in the local */

    pHeadTD = (pUSB_UHCD_TD)(pRequest->pHead);

    /* Release all the TDs associated with this Request */

    while (pHeadTD != NULL)
        {
        /* Store the next pointer temporarily */

        pTempTD = pHeadTD->hNextTd;

        /* Return the TD back to the free list */

        usbUhcdAddToFreeTDList(pHCDData, pHCDPipe, pHeadTD);

        /* When reach the tail TD of the request, break */

        if (pHeadTD == (pUSB_UHCD_TD)pRequest->pTail)
            break;

        /* Go to the next TD */

        pHeadTD = pTempTD;
        }

    /* Release the request info */

    usbUhcdReleaseRequestInfo(pHCDData, pHCDPipe, pRequest);

    /* Release the exclusive access */

    OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

    /*
     * Check if the buffer pointer is valid and the return
     * status is success
     */

    /* Call the callback function if it is registered */

    if (NULL != pUrb->pfCallback)
        {
        (pUrb->pfCallback)(pUrb);
        }
    }

/*******************************************************************************
*
* usbUhcdProcessIsochRequestCompletion - process isoch request completion
*
* This routine is to process isoch request completion.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbUhcdProcessIsochRequestCompletion
    (
    pUSB_UHCD_DATA          pHCDData,
    pUSB_UHCD_PIPE          pHCDPipe,
    pUSB_UHCD_REQUEST_INFO  pRequest
    )
    {
    /* Pointer to the URB */

    pUSBHST_URB     pUrb;

    /* Index of the host controller */

    UINT32          uBusIndex;

    /* Pointer to the tail of TD */

    pUSB_UHCD_TD    pTDTail;

    /* Pointer to the head of TD */

    pUSB_UHCD_TD    pTDHead;

    /* Pointer to the temporary TD */

    pUSB_UHCD_TD    pTempTD;

    /* Retrieve the bus index */

    uBusIndex = pHCDData->uBusIndex;

    /* Exclusively access the pipe request list */

    OS_WAIT_FOR_EVENT(pHCDPipe->PipeSynchEventID, OS_WAIT_INFINITE);

    /* Get the URB pointer of this request */

    pUrb = pRequest->pUrb;

    /* Get the tail of the TD list */

    pTDTail = (pUSB_UHCD_TD)pRequest->pTail;

    /* Get the head of the TD list */

    pTDHead = (pUSB_UHCD_TD)pRequest->pHead;

    /* Make sure the reuqest is valid */

    if ((NULL == pTDHead) ||
        (NULL == pTDTail) ||
        (NULL == pUrb) ||
        (NULL == pUrb->pTransferSpecificData))
        {
        USB_UHCD_ERR("IsocRequestCompletion got an invalid request!\n"
               "pTDHead %p pTDTail %p pUrb %p pTransferSpecificData %p\n",
               pTDHead, pTDTail, pUrb,
               (pUrb != NULL) ? pUrb->pTransferSpecificData : NULL, 5, 6);

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

        return;
        }

    /* Isochronous transfer completion */

    /* Check if the transfer is complete */

    if ((USB_UHCD_SWAP_DATA(uBusIndex, pTDTail->dWord1Td)
        & USBUHCD_TDCS_STS_ACTIVE) == 0)
        {
        USB_UHCD_VDBG("IsochRequestCompletion - transfer complete\n",
            1, 2, 3, 4, 5, 6);

        /* Fill up status of the transfer */

        usbUhcdFillIsoStatus(uBusIndex, USBHST_SUCCESS, pUrb, pRequest);

        /* Fill up transfer length */

        usbUhcdGetIsoTransferLength (uBusIndex, pRequest);

        /* Stop the HC */

        USB_UHCD_HALT_HC(pHCDData);

        /* Unlink the isochrnous TDs and free them */

        usbUhcdUnlinkItds (pHCDData, pRequest->pHead, FALSE);

        /* Restart the HC */

        USB_UHCD_RESTART_HC(pHCDData);
        }

    /* If the transfer is not complete */

    else
        {
        USB_UHCD_VDBG("IsochRequestCompletion - transfer not complete\n",
            1, 2, 3, 4, 5, 6);

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

        return;
        }

    /* Check if it is a high speed data transfer request */

    /* Whole of the request is completed */

    /*
     * If there is any real data transfer, we should do
     * vxbDmaBufSync and unload the DMA MAP.
     */

    if (pRequest->uRequestLength != 0)
        {
        /* Check if the data transfer is IN direction */

        if (pRequest->uDataDir == USB_UHCD_DIR_IN)
            {
            /* DMA cache invalidate and <copy from bounce buffer> */

            vxbDmaBufSync(pHCDData->pDev,
                pHCDPipe->usrBuffDmaTagId,
                pRequest->usrDataDmaMapId,
                _VXB_DMABUFSYNC_DMA_POSTREAD);
            }
        else
            {
#if 0 /* Don't use _VXB_DMABUFSYNC_DMA_POSTWRITE for now */

            /* DMA cache invalidate and <copy from bounce buffer> */

            vxbDmaBufSync(pHCDData->pDev,
                pHCDPipe->usrBuffDmaTagId,
                pRequest->usrDataDmaMapId,
                _VXB_DMABUFSYNC_DMA_POSTWRITE);
#endif
            }

        /* Unload the Data DMA MAP */

        vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
            pRequest->usrDataDmaMapId);
        }

    /*
     * Return the request info to the pipe which also
     * release the URB from the request, so that the
     * URB callback can submit the URB again just in
     * the callback and it can find free request info
     * to be used.
     *
     * Note: This has to be done before calling the
     * URB callback.
     */

    /* Remove all the TDs from request list. */

    while (pTDHead != NULL)
        {
        /* Save the next pointer in the request*/

        pTempTD = pTDHead->vNextTd;

        /* Return the TD to the free TD list */

        usbUhcdAddToFreeTDList(pHCDData, pHCDPipe, pTDHead);

        /* When reach the tail of the request, break */

        if (pTDHead == pTDTail)
            break;

        /* Go to the next TD of this request */

        pTDHead = pTempTD;
        }

    /* Release the request info */

    usbUhcdReleaseRequestInfo(pHCDData, pHCDPipe, pRequest);

    /* Release the exclusive access */

    OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

    /* Call the callback function if it is registered */

    if (NULL != pUrb->pfCallback)
        {
        (pUrb->pfCallback)(pUrb);
        }

    return;
    }

/*******************************************************************************
*
* usbUhcdProcessPipeCompletion - process a pipe for request completion
*
* This routine is to process a pipe for request completion.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbUhcdProcessPipeCompletion
    (
    pUSB_UHCD_DATA          pHCDData,
    pUSB_UHCD_PIPE          pHCDPipe
    )
    {
    /* To hold the pointer to the request information */

    pUSB_UHCD_REQUEST_INFO pRequest;

    /* To hold the pointer to the next request information */

    pUSB_UHCD_REQUEST_INFO pNextRequest;

    /* To hold the pointer to the tail request information */

    pUSB_UHCD_REQUEST_INFO pTailRequest;

    /*
     * Theoretically we should take the pipe lock <PipeSynchEventID> here
     * so that the integrity of active request list is maintained,
     * but the code is implemented that this is not necessary.
     *
     * 1. Once entered this loop, we saved the end of the current active
     * request list, which will be the last request to be scanned in this
     * loop.
     *
     * 2. If a new URB is submitted while the URB is called during the
     * processing, it is put to the end of the active request list. But
     * we will not scan the new request in this loop, becasue it can't be
     * in completion as it is just submitted. If for any reason it is really
     * completed while this loop is still under course, it will be scanned
     * in the next run to scan this pipe.
     *
     * 3. If any URB of this pipe is canceled during calling any of the URB
     * callback, the URB cancel code has been implemented that the callback of
     * the URB being canceled is called just in the URB cancel routine, namely
     * usbUhcdCancelURB, but the request info and its associated transfer
     * descriptors are not unlinked in that routine, instead, the request will
     * be put onto the proper request cancel list (but the <pNext> link pointer
     * is not modified, it just use another link pointer <pListRequest> to
     * link this canceled request onto the cancel request list). Thus, the
     * active request list is still not modified, so it is safe to scan through
     * this active list. The URB cancel routine will flag that the request has
     * been canceled, thus when this routine reached the canceled request, it
     * doesn't process it and ignore it. With this scheme, the active requst
     * list is kept integrity from being altered by new URB submitting and
     * URB cancelling while the pipe scan is under progress.
     *
     * 4. When scanning one request on the active request list, and find it has
     * completed, the request will be returned to the free request list of this
     * pipe (and it will be unlinked from the active request list of this pipe).
     * However, this still doesn't affect the integrity of the active request
     * list of this pipe. Because, for one thing, the request completion happens
     * sequentially for one pipe, so the altering of the active list happens
     * from the head of the list, that is to say, we don't need to refer to the
     * request any more after the request is scanned; for another thing, the
     * altering of the active list happens just in the context of this task,
     * sequentially, there is no asynchronous modification to the list, thus
     * we are safe.
     *
     * 5. When returnning a completed request (unlinking from the active list
     * and link it to the free request list of this pipe), we will take the
     * pipe lock <PipeSynchEventID> for a short period of time, this is to
     * protect the pipe free request list for integrity, becasue at that time
     * another task may try to take a free request to submit a new URB thus
     * althering of the pipe free request list should be protected.
     *
     * This is done to reduce lock contention. Previously, the UHCD is written
     * that each URB will be on both the pipe request list and the HCD global
     * request list, so when an interrupt indicating any USB completion occur,
     * the URB completion processing takes a HCD global lock, which is widely
     * used across the driver to protect almost any list access. Once the global
     * lock is taken, the other tasks can not take that lock, even the protected
     * resources are not closely related. For example, when this routine is
     * processing URB completion for one pipe, the lock is taken, if at the same
     * time any other pipe of any device wants to submit a new URB, it has to
     * wait the global lock to be released. Now we changed the lock to be per pipe
     * lock, and don't put the request onto the HCD global list but only on the
     * pipe active request list, based on the fact that the requests are
     * essentially submitted to the pipe, thus the request doesn't need to go
     * onto any global request list. Doing so we only need to take the pipe lock.
     * If another pipe wants to submit requests while we are proncessing this
     * pipe, it can just do the regular submitting process (it only needs to take
     * the pipe lock so that concurrent access to the free request list is safely
     * protected).
     */

    /* Save the tail of the active request list */

    pTailRequest = pHCDPipe->pRequestQueueTail;

    /* Start from the head of the active request list */

    pRequest = pHCDPipe->pRequestQueueHead;

    while (pRequest != NULL)
        {
        /*
         * Save the next active request on the active request
         * list of this pipe so that we can safely move to the
         * next request once this request has been processed.
         *
         * Note: This should be done before calling the actual
         * request completion handling. Becasue it is possible
         * that the current request is complete and the URB
         * callback is called, and the URB callback can do
         * anyting such as submit a new URB or cancel any existing
         * active URB, which will modify the active request list
         * of the pipe. If the moving to the pRequest->pNext is
         * done after the URB completion handling, then the pNext
         * may have been changed thus may delay some request
         * completion processing.
         */

        pNextRequest = pRequest->pNext;

        /* Check the request for completion */

        if (pHCDPipe->uEndpointType != USBHST_ISOCHRONOUS_TRANSFER)
            {
            usbUhcdProcessNonIsochRequestCompletion(pHCDData,
                pHCDPipe, pRequest);
            }
        else
            {
            usbUhcdProcessIsochRequestCompletion(pHCDData,
                pHCDPipe, pRequest);
            }

        /*
         * If this request is already the tail of the pipe's
         * active request list, then we can break out.
         */

        if (pRequest == pTailRequest)
            break;

        /* Go to the next request */

        pRequest = pNextRequest;
        }

    return;
    }

/*******************************************************************************
*
* usbUhcdProcessTransferCompletion - handle the data transfer interrupt
*
* This routine is used to handle the data transfer completion interrupt by
* scan through the pipe lists maintained by the HCD.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbUhcdProcessTransferCompletion
    (
    pUSB_UHCD_DATA          pHCDData
    )
    {
    /* Pointer to the HCDPipe */

    pUSB_UHCD_PIPE          pHCDPipe = NULL;

    /* Pointer to the next HCDPipe */

    pUSB_UHCD_PIPE          pNextHCDPipe = NULL;

    /* Exclusively access the active pipe list */

    OS_WAIT_FOR_EVENT(pHCDData->ActivePipeListSynchEventID, OS_WAIT_INFINITE);

    /*
     * It is implemented that during the period the ActivePipeListSynchEventID
     * is taken for processing transfer completion events, there is no
     * modification done to these pipe lists. If during the time of processing
     * completion there is any pipe creation or deletion, it is delayed until
     * the URB completion processing is done.
     */

    /* Scan through the ISOC pipe list */

    pHCDPipe = pHCDData->pIsochPipeList;

    /*
     * Work when the pipe is valid
     */

    while (pHCDPipe != NULL)
        {

        /* Save the next pipe */

        pNextHCDPipe = pHCDPipe->pNext;

        /* Scan the pipe */

        usbUhcdProcessPipeCompletion(pHCDData, pHCDPipe);

        /* Go to the next pipe */

        pHCDPipe = pNextHCDPipe;
        }

    /* Scan through the INTER pipe list */

    pHCDPipe = pHCDData->pNonIsochPipeList;

    /*
     * Work when the pipe is valid
     */

    while (pHCDPipe != NULL)
        {
        /* Save the next pipe */

        pNextHCDPipe = pHCDPipe->pNext;

        /* Scan the pipe */

        usbUhcdProcessPipeCompletion(pHCDData, pHCDPipe);

        /* Go to the next pipe */

        pHCDPipe = pNextHCDPipe;
        }

    /* Release the synchronization event */

    OS_RELEASE_EVENT(pHCDData->ActivePipeListSynchEventID);

    /*
     * Process any delayed pipe list modification that could have
     * happened during the time the ActivePipeListSynchEventID
     * was taken to process URB completion.
     */

    usbUhcdProcessDelayedPipeList(pHCDData);
    }/* End of usbUhcdProcessTransferCompletion() */

/*******************************************************************************
*
* usbUhcdCleartdOnSysError - handle the Host Controller Process error
*
* This routine handles the Host Controller Process error.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbUhcdCleartdOnSysError
    (
    pUSB_UHCD_DATA pHCDData
    )
    {
    /*
     * Consider the following scenario that the
     * requests which have been serviced before this interrupt
     * has occured, but their respective callback has not been called.
     *
     * For such requests, callback function needs to be called.
     * This is accomplished by calling the usbUhcdProcessTransferCompletion()
     * function.
     */

    usbUhcdProcessTransferCompletion(pHCDData);

    /*
     * Now for the remaining the requests, that has not been yet serviced by
     * the host controller, needs to be cancelled, and the memory allocated
     * for these ones needs to be freed.
     */

    return;
    }


/********************** End of File usbUhcdScheduleQwaitForSignal.c ***********/

