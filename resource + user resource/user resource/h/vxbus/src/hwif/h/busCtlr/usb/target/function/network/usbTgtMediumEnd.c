/* usbTgtMediumEnd.c - The END Medium Agent Module for USB Target Function Driver */

/*
 * Copyright (c) 2011-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
Modification history
--------------------
01j,04jan13,s_z  Remove compiler warning (WIND00390357)
01i,15may12,s_z  Correct coverity warning  
01h,15may12,s_z  Correct debugging display issue (WIND00348017)
01g,29sep11,s_z  More times retry to get a free buffer (WIND00306920)
01f,08apr11,ghs  Fix code coverity issue(WIND00264893)
01e,28mar11,s_z  Add more commands support in usbTgtMediumEndIoctl rotuine
01d,22mar11,s_z  Code clean up based on the code review
01c,09mar11,s_z  Code clean up
01b,23feb11,s_z  Return error when no buffer for using
01a,04jan11,s_z  written
*/

/*
DESCRIPTION

This file includes the END medium agent mamagement module for USB target function
driver.

This module is above the Medium binder and bellow the Virtual END device or the
real END device. It supports both of the two kinds of using.

INCLUDE FILES:  vxWorks.h etherMultiLib.h endLib.h usb/usbOsal.h
                usbTgtVrtlEnd.h usbTgtMediumEnd.h usbTgtNetMediumBinder.h

*/

/* includes */

#include <vxWorks.h>
#include <etherMultiLib.h>
#include <endLib.h>
#include <usb/usbOsal.h>
#include <usbTgtVrtlEnd.h>
#include <usbTgtMediumEnd.h>
#include <usbTgtNetMediumBinder.h>

/* external */

IMPORT int ipAttach
    (
    int    unit,     /* Unit number  */
    char * name      /* Device name (e.g. "ln", "ei", etc.). */
    );

IMPORT int ipDetach
    (
    int    unit,     /* Unit number  */
    char * name      /* Device name (e.g. "ln", "ei", etc.). */
    );

/* locals */

LOCAL LIST gUsbTgtMediumEndList;
LOCAL BOOL gUsbTgtMediumEndListInited = FALSE;
LOCAL OS_EVENT_ID gUsbTgtMediumListLock = OS_INVALID_EVENT_ID;


/*******************************************************************************
*
* usbTgtMediumEndFind - find the END medium device pointer
* <'pUSBTGT_MEDIUM_END'> from the global list by the <'pEND_OBJ'> item.
*
* This routine finds and returns the END medium device pointer
* <'pUSBTGT_MEDIUM_END'> from the global list by the <'pEND_OBJ'> item.
*
* RETURNS: NULL, or the pUSBTGT_MEDIUM_END pointer if found in the list
*
* ERRNO: N/A
*
* \NOMANUAL
*/
LOCAL pUSBTGT_MEDIUM_END usbTgtMediumEndFind
    (
    END_OBJ * pEndObj
    )
    {
    pUSBTGT_MEDIUM_END pMediumEnd = NULL;
    int                index;
    int                count;

    OS_WAIT_FOR_EVENT(gUsbTgtMediumListLock, WAIT_FOREVER);

    count = lstCount (&gUsbTgtMediumEndList);

    for (index = 1; index <= count; index ++)
        {
        pMediumEnd = (pUSBTGT_MEDIUM_END)lstNth(&gUsbTgtMediumEndList, index);

        if ((NULL != pMediumEnd) &&
            (pEndObj == pMediumEnd->pEndObj))
            {

            /* Find it */

            OS_RELEASE_EVENT(gUsbTgtMediumListLock);
            return pMediumEnd;
            }
        }

    OS_RELEASE_EVENT(gUsbTgtMediumListLock);

    return NULL;
    }

/*******************************************************************************
*
* usbTgtMediumEndIoctl - the ioctl interface to the END medium
*
* This routine is the ioctl interface to the END medium, which registered to
* the binder, and used to control/access the END device.
*
* RETURNS: A command specific response, usually OK, ERROR or EINVAL.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL int usbTgtMediumEndIoctl
    (
    void *  pMediumDev,
    int     funcCode,
    char *  arg
    )
    {
    pUSBTGT_MEDIUM_END pMediumEnd = (pUSBTGT_MEDIUM_END)pMediumDev;
    END_OBJ *          pEndObj = NULL;
    int                error = OK;

    /* Validate parameters */

    if ((NULL == pMediumEnd) ||
        (NULL == (pEndObj = pMediumEnd->pEndObj)) ||
        (NULL == pEndObj->pFuncTable) ||
        (NULL == pEndObj->pFuncTable->ioctl))
        {
        USBTGT_END_ERR("Invalid parameter %s is NULL\n",
                       ((NULL == pMediumDev) ? "pMediumDev" :
                        (NULL == pEndObj) ?
                        "pMediumEnd->pEndObj" :
                        (NULL == pEndObj->pFuncTable) ?
                        "pFuncTable" : "pFuncTable->ioctl"), 2, 3, 4, 5, 6);

        return EINVAL;
        }

    switch (funcCode)
        {
        case USBTGT_NET_IOCTL_END_SET_MULTIPLE_ADDR_ADD:
           {
            /* Add the Multi address */

            error = (pEndObj->pFuncTable->ioctl)(pEndObj,
                                                 EIOCMULTIADD,
                                                 arg);

            USBTGT_END_VDBG("Add the Multi address to END NIC return %d\n",
                            error, 2, 3, 4, 5, 6);
            }
            break;
        case USBTGT_NET_IOCTL_END_SET_MULTIPLE_ADDR_DEL:
           {
            /* Delete the Multi address */

            error = (pEndObj->pFuncTable->ioctl)(pEndObj,
                                                 EIOCMULTIDEL,
                                                 arg);

            USBTGT_END_VDBG("Add the Multi address to END NIC return %d\n",
                            error, 2, 3, 4, 5, 6);
            }
            break;
        case USBTGT_NET_IOCTL_END_GET_MULTIPLE_ADDR:
            {
            /* Get the Multi address */

            error = (pEndObj->pFuncTable->ioctl)(pEndObj,
                                      EIOCMULTIGET,
                                      arg);

            USBTGT_END_VDBG("Get the Multi address from END NIC return %d\n",
                            error, 2, 3, 4, 5, 6);
            }
            break;
        case USBTGT_NET_IOCTL_END_GET_MAC_ADDR:
            {
            /* Get the MAC address */

            if (USBTGT_BINDER_MEDIUM_VIRTUAL_END == pMediumEnd->uMediumType)
                {
                error = (pEndObj->pFuncTable->ioctl)(pEndObj,
                                                     UIOCGREMOTEADDR,
                                                     arg);
                }
            else
                {
                error = (pEndObj->pFuncTable->ioctl)(pEndObj,
                                                     EIOCGADDR,
                                                     arg);
                }

            USBTGT_END_VDBG("Get the MAC address from END NIC return %d\n",
                            error, 2, 3, 4, 5, 6);
            }
            break;
        case USBTGT_NET_IOCTL_END_SET_MAC_ADDR:
            {
            /* Set the MAC address */

            if (USBTGT_BINDER_MEDIUM_VIRTUAL_END == pMediumEnd->uMediumType)
                {
                error = (pEndObj->pFuncTable->ioctl)(pEndObj,
                                                     UIOCSREMOTEADDR,
                                                     arg);
                }
            else
                {
                error = (pEndObj->pFuncTable->ioctl)(pEndObj,
                                                     EIOCSADDR,
                                                     arg);
                }

            USBTGT_END_VDBG("Get the MAC address from END NIC return %d\n",
                            error, 2, 3, 4, 5, 6);
            }
            break;
        case USBTGT_NET_IOCTL_END_GET_MTU:
            {
            /*
             * Press the RNDIS's OID_GEN_TRANSMIT_BLOCK_SIZE and
             * OID_GEN_RECEIVE_BLOCK_SIZE OIDs
             * will return the MTU
             * OID_GEN_MAXIMUM_FRAME_SIZE will require the MTU
             * too
             */

            error = (pEndObj->pFuncTable->ioctl)(pEndObj,
                                                 EIOCGIFMTU,
                                                 arg);

            USBTGT_RNDIS_DBG("USBTGT_NET_IOCTL_END_GET_MTU 0x%X with return %d\n",
                            (UINT32)(ETHERMTU),
                             error, 3, 4, 5, 6);
            }

            break;
        case USBTGT_NET_IOCTL_END_SET_MTU:
            {
            /*
             * Press the RNDIS's OID_GEN_TRANSMIT_BLOCK_SIZE and
             * OID_GEN_RECEIVE_BLOCK_SIZE OIDs
             * will set the MTU
             */

            error = (pEndObj->pFuncTable->ioctl)(pEndObj,
                                                 EIOCSIFMTU,
                                                 arg);

            USBTGT_RNDIS_DBG("USBTGT_NET_IOCTL_END_GET_MTU 0x%X with return %d\n",
                            (UINT32)(ETHERMTU),
                             error, 3, 4, 5, 6);
            }
            break;
        case USBTGT_NET_IOCTL_END_GET_XMIT_OK:
            {
            * (UINT32 *)arg =  TO_LITTLEL(pEndObj->mib2Tbl.ifOutUcastPkts +
                                          pEndObj->mib2Tbl.ifOutNUcastPkts);

            USBTGT_END_VDBG("Get %d XMIT OK pkts of the END NIC \n",
                            *(UINT32 *)arg, 2, 3, 4, 5, 6);
            }
            break;
        case USBTGT_NET_IOCTL_END_GET_RCV_OK:
            {
            * (UINT32 *)arg =  TO_LITTLEL(pEndObj->mib2Tbl.ifInUcastPkts +
                                          pEndObj->mib2Tbl.ifInNUcastPkts);

            USBTGT_END_VDBG("Get %d RCV OK pkts of the END NIC\n",
                            *(UINT32 *)arg, 2, 3, 4, 5, 6);
            }
            break;
        case USBTGT_NET_IOCTL_END_GET_XMIT_ERROR:
            {

            * (UINT32 *) arg =  TO_LITTLEL(pEndObj->mib2Tbl.ifOutErrors);

            USBTGT_END_VDBG("Get %d XMIT ERROR pkts of the END NIC\n",
                            *(UINT32 *)arg, 2, 3, 4, 5, 6);

            }
            break;
        case USBTGT_NET_IOCTL_END_GET_RCV_ERROR:
            {
            * (UINT32 *) arg =  TO_LITTLEL(pEndObj->mib2Tbl.ifInErrors);

            USBTGT_END_VDBG("Get %d RCV ERROR pkts of the END NIC\n",
                            *(UINT32 *)arg, 2, 3, 4, 5, 6);
            }
            break;
        case USBTGT_NET_IOCTL_END_GET_LINK_SPEED:
            {
            * (UINT32 *) arg =  TO_LITTLEL(pEndObj->mib2Tbl.ifSpeed);

            USBTGT_END_VDBG("Get the link speed from the END NIC with 0x%X\n",
                            *(UINT32 *)arg, 2, 3, 4, 5, 6);
            }
            break;
        case USBTGT_NET_IOCTL_END_PNP_WAKEUP_ENABLE_SET:
            {
            USBTGT_END_VDBG("Set PNP_WAKEUP_ENABLE of the END NIC with 0x%X\n",
                            *(UINT32 *)arg, 2, 3, 4, 5, 6);
            }
            break;
        case USBTGT_NET_IOCTL_END_PNP_WAKEUP_ENABLE_GET:
            {
            USBTGT_END_VDBG("Get PNP_WAKEUP_ENABLE of the END NIC with \n",
                            1, 2, 3, 4, 5, 6);
            }
            break;
        case USBTGT_NET_IOCTL_END_PNP_CAPABILITIES_GET:
            {
            USBTGT_END_VDBG("Get PNP_WAKEUP_ENABLE of the END NIC with \n",
                            1, 2, 3, 4, 5, 6);
            }
            break;
        case USBTGT_NET_IOCTL_END_PNP_POWER_GET:
            {
            USBTGT_END_VDBG("Get USBTGT_NET_IOCTL_END_PNP_POWER_GET of the END NIC with \n",
                            1, 2, 3, 4, 5, 6);
            }
            break;
        case USBTGT_NET_IOCTL_END_PNP_POWER_SET:
            {
            USBTGT_END_VDBG("Get USBTGT_NET_IOCTL_END_PNP_POWER_SET of the END NIC with \n",
                            1, 2, 3, 4, 5, 6);
            }
            break;
        case USBTGT_NET_IOCTL_END_PNP_WAKEUP_PATTERN_ADD:
            {
            USBTGT_END_VDBG("Get USBTGT_NET_IOCTL_END_PNP_WAKEUP_PATTERN_ADD of the END NIC with \n",
                            1, 2, 3, 4, 5, 6);
            }
            break;
        case USBTGT_NET_IOCTL_END_PNP_WAKEUP_PATTERN_REMOVE:
            {
            USBTGT_END_VDBG("Get USBTGT_NET_IOCTL_END_PNP_WAKEUP_PATTERN_REMOVE of the END NIC with \n",
                            1, 2, 3, 4, 5, 6);
            }
            break;
        default:
            {
            error = EINVAL;
            }
            break;
        }

    return error;
    }

/*******************************************************************************
*
* usbTgtMediumEndDataToUsb - send data to the USB low level which come from
* the medium END device
*
* This routine is used to send data to the USB low level which come from
* the medium END device. It may comes from the virtual END device (network stack).
* Or comes from the real END device (the bound END hardware).
*
* When the medium is the virtual END device.It used as the send sub routine of
* the virtual END driver. It should be regisered to the virtual END driver
* before using.
*
* When the medium is the real END hardware (bound END device). It will replease
* the <'pEND->receiveRtn'>. So when one data received by the hardware, this
* routine will be called to send the data the to USB level. And the hareware
* may think it upload the data to the network stack.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtMediumEndDataToUsb
    (
    END_OBJ * pEndObj,
    M_BLK_ID  pMblk
    )
    {
    pUSBTGT_MEDIUM_END pMediumEnd = NULL;
    pUSBTGT_BINDER_BUF pBinderBuf = NULL;
    pUSBTGT_BINDER     pBinder = NULL;
    UINT16             uRetry   = 1000;

    /* Validate parameters */

    if ((NULL == pEndObj) ||
        (NULL == pMblk))
        {
        USBTGT_END_ERR("Invalid parameter %s is NULL\n",
                       ((NULL == pEndObj) ? "pEndObj" :
                        "pMblk"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Find the media end from the global list with the EndObj item */

    pMediumEnd = usbTgtMediumEndFind(pEndObj);

    if ((NULL == pMediumEnd) ||
        (NULL == pMediumEnd->pBinder))
        {
        USBTGT_END_ERR("No find the Media END, drop all the data\n",
                       1, 2, 3, 4, 5, 6);

        netMblkClChainFree (pMblk);

        return ERROR;
        }

    pBinder = (pUSBTGT_BINDER)pMediumEnd->pBinder;

    /* Get one free binder buffer to store the received END data */

    pBinderBuf = usbTgtNetMediumBinderBufGet(pBinder,
                                             USBTGT_BINDER_BUF_FREE_RCV);

    /* If there is no buffer for using, retry */

    while ((NULL == pBinderBuf) && (uRetry --))
        {
        taskDelay(NO_WAIT);

        pBinderBuf = usbTgtNetMediumBinderBufGet(pBinder,
                                                 USBTGT_BINDER_BUF_FREE_RCV);
        }

    /* After retried 3 times, still no buffer for using */

    if (NULL == pBinderBuf)
        {
        netMblkClChainFree (pMblk);

        USBTGT_END_VDBG("usbTgtMediumEndDataToUsb No buffer \n",
                         1, 2, 3, 4, 5, 6);

        /* Could you please faster :) */

        if (pBinder->dataToTransport)
            {
            (pBinder->dataToTransport)(pBinder->pTransportDev);
            }

        return ERROR;
        }

    /* Copy the data to be transfer */

    pBinderBuf->actDataLen = (UINT32)netMblkToBufCopy (pMblk,
                              (char *)(pBinderBuf->pBuf + pBinderBuf->headerLen),
                              NULL);

    /* Finished the copy, free it */

    netMblkClChainFree (pMblk);

    if (ERROR == usbTgtNetMediumBinderBufPut(pBinder,
                                             pBinderBuf,
                                             USBTGT_BINDER_BUF_RCV_FROM_MEDIUM))
        {
        USBTGT_END_ERR("Put the binder buffer to rcvFromMediumBufList error\n",
                       1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * If the medium is the virtula END, account the pecket count
     * else, do noting, since the END hardware interface has done that
     */

    if (USBTGT_BINDER_MEDIUM_VIRTUAL_END == pBinder->uMediumType)
        {
        endM2Packet (pEndObj, pMblk, M2_PACKET_OUT);
        }

    USBTGT_END_VDBG ("usbTgtMediumEndDataToUsb %d bytes from END interface\n",
                      pBinderBuf->actDataLen, 2, 3, 4, 5, 6);

    /* Call the binder low level notification sub routine */

    if (pBinder->dataToTransport)
        {
        (pBinder->dataToTransport)(pBinder->pTransportDev);
        }

    return OK;
    }

/*******************************************************************************
*
* usbTgtMediumUsbDataToEnd - receive the data from the USB low level, and
* transfer to the END medium.
*
* This routine is used to receive the data from the USB low level, and
* transfer to the END medium.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbTgtMediumUsbDataToEnd
    (
    void * pMediumDev
    )
    {
    pUSBTGT_MEDIUM_END pMediumEnd = (pUSBTGT_MEDIUM_END)pMediumDev;
    END_OBJ *          pEndObj = NULL;
    M_BLK_ID           pMblk;
    pUSBTGT_BINDER     pBinder = NULL;
    pUSBTGT_BINDER_BUF pBinderBuf = NULL;

    /* Validate parameters */

    if ((NULL == pMediumEnd) ||
        (NULL == pMediumEnd->pEndObj) ||
        (NULL == pMediumEnd->pBinder))
        {
        USBTGT_END_ERR("Invalid parameter %s is NULL\n",
                       ((NULL == pMediumEnd) ? "pMediumDev" :
                        (NULL == pMediumEnd->pEndObj) ? "pEndObj" :
                        "pBinder"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    pEndObj = pMediumEnd->pEndObj;

    pBinder = (pUSBTGT_BINDER)pMediumEnd->pBinder;

    /* Prepare the data and send to the End device */

    pBinderBuf = usbTgtNetMediumBinderBufGet(pBinder,
                                             USBTGT_BINDER_BUF_XMIT_TO_MEDIUM);

    if (NULL == pBinderBuf)
        {
        USBTGT_END_ERR("No data on the xmit list\n",
                         1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    pMblk = endPoolTupleGet (pEndObj->pNetPool);

    if (NULL == pMblk)
        {
        USBTGT_END_ERR("No enough mblk to send the data\n",
                       1, 2, 3, 4, 5, 6);

        if (ERROR == usbTgtNetMediumBinderBufPut(pBinder,
                                                 pBinderBuf,
                                                 USBTGT_BINDER_BUF_FREE_XMIT))
            {
            USBTGT_END_WARN("Put the binder buffer back to freeXmitBufList error\n",
                             1, 2, 3, 4, 5, 6);

            }
        return ERROR;
        }

    memcpy(pMblk->mBlkHdr.mData,
           pBinderBuf->pBuf + pBinderBuf->headerLen,
           pBinderBuf->actDataLen);

    if (ERROR == usbTgtNetMediumBinderBufPut(pBinder,
                                             pBinderBuf,
                                             USBTGT_BINDER_BUF_FREE_XMIT))
        {
        /*
         * If we release this binder buffer error,
         * still do not block the xmit to medium action
         */

        USBTGT_END_WARN("Put the binder buffer back to freeXmitBufList error\n",
                        1, 2, 3, 4, 5, 6);
        }

    pBinderBuf->actDataLen = ( pBinderBuf->actDataLen < 60) ? 60 :
                                    pBinderBuf->actDataLen;

    pMblk->mBlkHdr.mLen = pBinderBuf->actDataLen;

    pMblk->mBlkHdr.mFlags |= M_PKTHDR;

    pMblk->mBlkPktHdr.len = (long)pBinderBuf->actDataLen;

    /* Call the interface of the END dev layer's */

    USBTGT_END_VDBG ("usbTgtVxbVrtlEndDevDataRcv %d bytes to the Virtual END\n",
                     pBinderBuf->actDataLen, 2, 3, 4, 5, 6);

    /*
     * If the medium is the virtula END, account the pecket count
     * else, do noting, since the END hardware interface has done that
     */

    if (USBTGT_BINDER_MEDIUM_VIRTUAL_END == pBinder->uMediumType)
        {
        endM2Packet (pEndObj, pMblk, M2_PACKET_IN);
        }

    if (NULL != pMediumEnd->dataToMediumRtn)
        {
        netJobAdd(pMediumEnd->dataToMediumRtn,
                  pEndObj,
                  pMblk,
                  NULL,
                  NULL,
                  NULL);
        }

    return OK;
    }

/*******************************************************************************
*
* usbTgtMediumEndDetach - unbind the medium device to the RNDIS
*
* This routine unbinds the medium device to the rndis
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbTgtMediumEndDetach
    (
    void * pMediumDev
    )
    {
    pUSBTGT_MEDIUM_END pMediumEnd = (pUSBTGT_MEDIUM_END)pMediumDev;
    int                retVal = ERROR;

    /* Validate parameters */

    if ((NULL == pMediumEnd) ||
        (NULL == pMediumEnd->pEndObj))
        {
        USBTGT_END_ERR("Invalid parameter %s is NULL\n",
                       ((NULL == pMediumEnd) ? "pMediaDev" :
                       "pMediumEnd->pEndObj"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    switch (pMediumEnd->uMediumType)
        {
        case USBTGT_BINDER_MEDIUM_END_BRIGGE:
            {
            if ((NULL != pMediumEnd->pEndName))
                {
                pMediumEnd->pEndObj->receiveRtn = pMediumEnd->oldReceiveRtn;

                if (ERROR == ipAttach(pMediumEnd->endUnit, pMediumEnd->pEndName))
                    {
                    USBTGT_END_ERR("Attach the END device %s%d to network error\n",
                                   pMediumEnd->pEndName,
                                   pMediumEnd->endUnit, 3, 4, 5, 6);

                    /* Notify the task to unBind the End media */

                    return ERROR;
                    }
                }
            }
            break;
        case USBTGT_BINDER_MEDIUM_VIRTUAL_END:
            {
            if (NULL != pMediumEnd->pEndObj->pFuncTable)
                {
                /* It will awalys return OK */

                retVal = (pMediumEnd->pEndObj->pFuncTable->ioctl)
                                          (pMediumEnd->pEndObj,
                                           UIOCSSUBSENDRTN,
                                           (caddr_t)NULL);
                }

            if (OK != retVal)
                {
                USBTGT_END_ERR("Reset the sub send routine to NULL fail\n",
                               1, 2, 3, 4, 5, 6);

                return ERROR;
                }
            }
            break;
        default:
            {
            USBTGT_END_ERR("Unknown medium type\n",
                           1, 2, 3, 4, 5, 6);
            return ERROR;
            }
            break;
        }


    OS_WAIT_FOR_EVENT(gUsbTgtMediumListLock, WAIT_FOREVER);

    if (ERROR != lstFind (&gUsbTgtMediumEndList,
                          &pMediumEnd->mediumEndNode))
        {
        lstDelete(&gUsbTgtMediumEndList,
                  &pMediumEnd->mediumEndNode);
        }

    OS_RELEASE_EVENT(gUsbTgtMediumListLock);

    OS_FREE (pMediumEnd);

    return OK;
    }

/*******************************************************************************
*
* usbTgtMediumEndAttach - bind the medium device to the RNDIS
*
* This routine binds the medium device to the rndis
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

 STATUS usbTgtMediumEndAttach
    (
    pUSBTGT_BINDER pBinder,       /* The medium binder pointer */
    char *         pMediumName,   /* Medium name which need to bind */
    int            mediumUnit,    /* Medium unit which need to bind */
    UINT8          uMediumType,   /* Medium type */
    void **        ppMediumDev    /* Store the the medium dev pointer */
    )
    {
    pUSBTGT_MEDIUM_END   pMediumEnd = NULL;
    END_OBJ *            pEndObj = NULL;

    /* Validate parameters */

    if ((NULL == pMediumName) ||
        (NULL == ppMediumDev) ||
        (NULL == pBinder))
        {
        USBTGT_END_ERR("Invalid parameter %s\n",
                       ((NULL == pMediumName) ? "pMediumName is NULL" :
                        (NULL == ppMediumDev) ? "ppMediumDev is NULL" :
                        "pBinder is NULL"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Init the medium end list */

    if (FALSE == gUsbTgtMediumEndListInited)
        {

        gUsbTgtMediumListLock = OS_CREATE_EVENT(OS_EVENT_SIGNALED);

        /* Check if the device driver list lock is invalid */

        if (OS_INVALID_EVENT_ID == gUsbTgtMediumListLock)
            {
            USB_TGT_ERR ("No enough resource to create usb target list lock\n",
                         1, 2, 3, 4, 5, 6);

            return ERROR;
            }

        lstInit(&gUsbTgtMediumEndList);

        gUsbTgtMediumEndListInited = TRUE;
        }

    pEndObj = endFindByName(pMediumName, mediumUnit);

    if (NULL == pEndObj)
        {
        /* The device is not exist */

        USBTGT_END_ERR("Can not find the END device %s%d\n",
                       pMediumName,mediumUnit,3,4,5,6);

        return ERROR;
        }

    /* Find it, detach it form the network stack */

    if (ERROR == ipDetach(mediumUnit,pMediumName))
        {
        USBTGT_END_ERR("Detach the END device %s%d from network error\n",
                       pMediumName,mediumUnit,3,4,5,6);

        return ERROR;
        }

    /* Allocal the medium end data structure */

    pMediumEnd = (pUSBTGT_MEDIUM_END)OSS_CALLOC(sizeof (USBTGT_MEDIUM_END));

    if (NULL == pMediumEnd)
        {
        USBTGT_END_ERR("Malloc memory for Media END struct fail\n",
                       1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Init the Medium dev data */

    pMediumEnd->pEndObj = pEndObj;
    pMediumEnd->pEndName = pMediumName;
    pMediumEnd->endUnit = mediumUnit;
    pMediumEnd->pBinder = pBinder;
    pMediumEnd->uMediumType = uMediumType;

    pBinder->pMediumDev = (void *)pMediumEnd;

    /* Inint the medium data receive routine */

    pBinder->dataToMedium = usbTgtMediumUsbDataToEnd;
    pBinder->mediumIoctl = usbTgtMediumEndIoctl;

    /* Add the medium to the global list */

    OS_WAIT_FOR_EVENT(gUsbTgtMediumListLock, WAIT_FOREVER);

    lstAdd (&gUsbTgtMediumEndList, &pMediumEnd->mediumEndNode);

    OS_RELEASE_EVENT(gUsbTgtMediumListLock);


    switch (uMediumType)
        {
        case USBTGT_BINDER_MEDIUM_VIRTUAL_END:
            {
            /*
             * 1. Set the send sub routine
             *    NOTE: This Ioctl only available on the USB virtual END driver
             * 2. Set the medium receive routine, which can be called when
             *    data received by the low level dev.
             */

            if (NULL != pEndObj->pFuncTable)
                {
                pEndObj->pFuncTable->ioctl(pEndObj,
                                           UIOCSSUBSENDRTN,
                                           (caddr_t)usbTgtMediumEndDataToUsb);

                pMediumEnd->dataToMediumRtn = pEndObj->receiveRtn;
                }

            /* Need attach this END again */

            if (ERROR == ipAttach(mediumUnit,pMediumName))
                {
                USBTGT_END_ERR("Attach the END device %s%d to the network error\n",
                               pMediumName,mediumUnit,3,4,5,6);

                /* Unbind the Media to free the resource */

                (void)usbTgtMediumEndDetach(pMediumEnd);

                return ERROR;
                }

            if (pEndObj->pFuncTable)
                {
                (pEndObj->pFuncTable->start)(pEndObj);
                }

            USBTGT_END_DBG("Attach the END device %s%d to the USB TCD\n",
                            pMediumName,mediumUnit,3,4,5,6);
            }
            break;
        case USBTGT_BINDER_MEDIUM_END_BRIGGE:
            {

            /*
             * 1. Change the receive routine of the end device
             * So when the Hw end received the data, it will send the
             * data to the usb by calling <'usbTgtMediumEndDataToUsb'>
             * 2. Set the data to medium routine, when the data recieved
             * from the USB, it will call <'usbTgtMediumUsbDataToEnd'> and
             * then call <'send'> routine to send the data out of the END.
             */

            pMediumEnd->oldReceiveRtn = pEndObj->receiveRtn;

            if (pEndObj->pFuncTable)
                {
                pEndObj->receiveRtn = usbTgtMediumEndDataToUsb;
                pMediumEnd->dataToMediumRtn = pEndObj->pFuncTable->send;
                }

            }
            break;
        default:
           {

            /* Should be not be here, since it has been checked above */

            USBTGT_END_ERR("Unknown medium type %d\n",
                           uMediumType, 2, 3, 4, 5, 6);

            OS_WAIT_FOR_EVENT(gUsbTgtMediumListLock, WAIT_FOREVER);

            lstAdd (&gUsbTgtMediumEndList, &pMediumEnd->mediumEndNode);

            OS_RELEASE_EVENT(gUsbTgtMediumListLock);

            OS_FREE (pMediumEnd);

            * ppMediumDev = NULL;

            return ERROR;
            }
            break;
        }

    * ppMediumDev = (void *)pMediumEnd;

    USBTGT_END_VDBG ("usbTgtMediumEndAttach pMediumName %s unit %d type %x\n",
                     pMediumName,
                     mediumUnit,
                     uMediumType,
                     4, 5, 6);


    return OK;
    }

