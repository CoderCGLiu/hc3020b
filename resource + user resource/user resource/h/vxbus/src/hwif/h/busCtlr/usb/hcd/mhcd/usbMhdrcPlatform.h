/* usbMhdrcPlatform.h - MUSBMHDRC platform definition */

/*
 * Copyright (c) 2010, 2011, 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */


/*
modification history
--------------------
01i,21apr13,wyy  Make the preprocessor look for some header files in
                 the current directory first
01h,31jan13,ljg  add usb support for ti816x EVM
01g,13jun11,jws  Added define for CENTAURUS
01f,13dec11,m_y  Modify according to code check result (WIND00319317)
01e,08apr11,w_x  Clean up MUSBMHDRC platform definitions
01d,09mar11,w_x  Code clean up for make man
01c,16aug10,w_x  VxWorks 64 bit audit and warning removal
01b,03jun10,s_z  Coding convention
01a,13mar10,s_z  initial version
*/

/*
DESCRIPTION

This file contains the platform specific definitions and interfaces for the
MUSBMHDRC controller.

INCLUDE FILES: vxWorks.h usb/usbHcdInstr.h usb/usbOtg.h cacheLib.h 
               hwif/vxbus/vxBus.h
*/

#ifndef __INCusbMhdrcPlatformh
#define __INCusbMhdrcPlatformh

/* includes */

#include <vxWorks.h>
#include <usb/usbHcdInstr.h>
#include <usb/usbOtg.h>
#include <cacheLib.h>
#include <hwif/vxbus/vxBus.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Platform definations */

#undef USB_MHDRC_VSB_BUILD

#ifdef USB_MHDRC_VSB_BUILD

#   undef USB_MHDRC_DAVINCI

#   ifdef USB_MHDRC_DAVINCI
#       undef USB_MHDRC_OMAP
#   else
#       define USB_MHDRC_OMAP
#   endif

#else

#   undef USB_MHDRC_OMAP
#   undef USB_MHDRC_DAVINCI
#   undef USB_MHDRC_CENTAURUS

#   define USB_MHDRC_OMAP
#   define USB_MHDRC_DAVINCI
#   define USB_MHDRC_CENTAURUS

#endif

/* DAVINCI related defines */

#   ifdef USB_MHDRC_DAVINCI
#       define USB_MHDRC_DAVINCI_DM355
#       undef USB_MHDRC_DAVINCI_DM6446
#   endif

/* OMAP related defines */

#   ifdef USB_MHDRC_OMAP
#       define  USB_MHDRC_OMAP_3530
#       undef  USB_MHDRC_OMAP_L137
#       undef  USB_MHDRC_OMAP_L138
#   endif

#ifdef USB_MHDRC_OMAP
#       include "usbMhdrcPlatformOmap.h" 
#endif

#ifdef USB_MHDRC_DAVINCI
#       include "usbMhdrcPlatformDavinci.h" 
#endif

#ifdef USB_MHDRC_CENTAURUS
#       include "usbMhdrcPlatformCentaurus.h"
#endif

#define USB_MHDRC_PLATFORM_OMAP35xx         0x0
#define USB_MHDRC_PLATFORM_DAVINIC          0x1
#define USB_MHDRC_PLATFORM_OMAPL13x         0x2
#define USB_MHDRC_PLATFORM_CENTAURUS        0x3
#define USB_MHDRC_PLATFORM_CENTAURUS_TI816X 0x4


/* DMA defination */

#define USB_MHDRC_DMA_ENABLE

/* Use the following macros to select how to detect ID and VBUS */

#undef USB_MHDRC_USE_DEVCTL_ID_DETECT    
#define USB_MHDRC_USE_DEVCTL_VBUS_DETECT    

STATUS usbMhdrcUlpiRead
    (
    pUSB_MUSBMHDRC  pMHDRC,
    UINT8           uOffset,
    UINT8 *         pData
    );

STATUS usbMhdrcUlpiWrite
    (
    pUSB_MUSBMHDRC  pMHDRC,
    UINT32          uOffset,
    UINT8           uData
    );

STATUS usbMhdrcPlatformHcdDmaUnSetup
    (
    pUSB_MUSBMHDRC  pMHDRC
    );

STATUS usbMhdrcPlatformHcdDmaSetup
    (
    pUSB_MUSBMHDRC  pMHDRC
    );

STATUS usbMhdrcPlatformSetup
    (
    pUSB_MUSBMHDRC  pMHDRC
    );

STATUS usbMhdrcPlatformHardwareInit
    (
    pUSB_MUSBMHDRC  pMHDRC
    );

STATUS usbMhdrcPlatformHardwareUnInit
    (
    pUSB_MUSBMHDRC  pMHDRC
    );

STATUS usbMhdrcPlatformSetup
    (
    pUSB_MUSBMHDRC  pMHDRC
    );

void usbMhdrcHardwareIntrEnable
    (
    pUSB_MUSBMHDRC  pMHDRC
    );

void usbMhdrcHardwareIntrDisable
    (
    pUSB_MUSBMHDRC  pMHDRC
    );

void usbMhdrcFIFOWrite
    (
    pUSB_MUSBMHDRC pMHDRC,
    UINT8 *        pSourceBuffer,
    UINT16         wCount,
    UINT8          uEndpointNumber
    );

void usbMhdrcFIFORead
    (
    pUSB_MUSBMHDRC  pMHDRC,
    UINT8 *         pDisBuffer,
    UINT16          wCount,
    UINT8           uEndpointNumber
    );

void usbMhdrcReportHostInterrupts
    (
    pUSB_MUSBMHDRC  pMHDRC
    );

STATUS usbMhdrcPlatformModeChange
    (
    pUSB_MUSBMHDRC  pMHDRC,
    USBOTG_MODE     uMode
    );
#ifdef __cplusplus
}
#endif

#endif /* __INCusbMhdrcPlatformh */
