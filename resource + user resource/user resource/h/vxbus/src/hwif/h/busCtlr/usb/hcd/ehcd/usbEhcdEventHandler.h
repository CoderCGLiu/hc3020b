/* usbEhcdEventHandler.h - Utility Functions for EHCI */

/*
 * Copyright 2003, 2009-2010 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2003, 2009-2010 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/


/*
Modification history
--------------------
01c,13jan10,ghs  vxWorks 6.9 LP64 adapting
01b,17mar09,s_z  Header file change for usbEhcdISR changed when deal
                 with(WIND00152849) and add the modification history
                 iterm of this file
01a,25apr03,ram  written.
*/

/*
DESCRIPTION
This contains interrupt routines which handle the EHCI
interrupts.
*/

/*
INTERNAL
 *******************************************************************************
 * Filename         : usbEhcdEventHandler.h
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 *
 * Description      :  This contains interrupt routines which handle the EHCI
 *                     interrupts.
 *
 *
 ******************************************************************************/
#ifndef __INCehcdEventHandlerh
#define __INCehcdEventHandlerh

#ifdef  __cplusplus
extern "C" {
#endif

extern VOID usbEhcdISR
    (
    pVOID pHCDDev
    );

extern VOID usbEhcdInterruptHandler
    (
    pUSB_EHCD_DATA pEHCDData
    );

#ifdef  __cplusplus
}
#endif

#endif /* End of __INCehcdEventHandlerh */
/************************** End of file usbEhcdISR.h*****************************/

