/* usbHubUtility.c - Utility functions to provide the functionality of HUB class driver */

/*
 * Copyright (c) 2003-2014 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2003-2014 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
Modification history
--------------------
03a,27Jun14,wyy  Support RMH in Intel Chipset 89xx (VXW6-82982)
02z,06Jun14,wyy  Return after usbHubClearTT totally completed (VXW6-82955)
02y,18jul13,wyy  Fix a wrong pointer in debugging message (WIND00427039)
02x,03may13,wyy  Remove compiler warning (WIND00356717)
02w,14may13,ghs  Make a delay after all ports are powered on in hub (WIND00417261)
02v,24apr13,wyy  Clean warnings of code coverity
02u,04jan13,s_z  Remove compiler warning (WIND00390357)
02t,01nov12,j_x  Update bus state in usbHubRemoveDevice() (WIND00386640)
02s,31oct12,j_x  Remove USB_MARK_FOR_DELETE_PORT in usbHubRemoveDevice() (WIND00386068)
02r,17oct12,w_x  Fix compiler warnings (WIND00370525)
02q,17oct12,w_x  Reset Super Speed port by Warm Reset (WIND00374209)
02p,21sep12,w_x  Clear reset flag if disconnect occurs during reset(WIND00375677)
02o,14sep12,w_x  Post event to Single BusM when reset is delayed (WIND00375677)
02n,24aug12,w_x  Fix task schedule issue in single BusM mode (WIND00370558)
02m,19jul12,w_x  Add support for USB 3.0 host (WIND00188662)
02l,13dec11,m_y  Modify according to code check result (WIND00319317)
02k,24jun11,ghs  Change hub configure check points (WIND00268645)
02j,02sep10,ghs  Use OS_THREAD_FAILURE to check taskSpawn failure (WIND00229830)
02i,08jul10,m_y  Modify for coding convention
02h,05jun10,ghs  Fix conflict symbol defines(WIND00221550)
02g,29jun10,w_x  Add more port reset delay and clean hub logging (WIND00216628)
02f,24may10,m_y  Modify URB time out value for usb robustness (WIND00183499)
02e,18may10,w_x  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
02d,27apr10,j_x  Error debug adaption and hard code replacement (WIND00183874)
02c,07mar10,y_l  Call OS_LOCKED_INCREMENT() to increment uNumberOfHubEvents
                 (WIND00187007)
02b,12mar10,j_x  Changed for USB debug (WIND00184542)
02a,18jan10,m_y  Return failed status when submit urb failed at reset
                 port (WIND00196403)
01z,08jan10,y_l  Add hub current status buffer changed flag (WIND00191906)
01y,07jan10,y_l  Remove stall handling in the hub driver (WIND00191018)
01x,10nov09,w_x  Prevent endless enable change loop for quirky hubs that set
                 both reset and enable change at reset complete (WIND00189783)
01w,13jan10,ghs  vxWorks 6.9 LP64 adapting
01v,16oct09,ghs  Move OS_DELAY_MS for t6 in usb spec 7.1.7.3(WIND00186053)
01u,30sep09,w_x  Retry the status change URB when failed and clear stall
                 condition if the endpoint stalled (WIND00183403)
01t,17sep09,y_l  Code Coverity CID(4908): NULL pointer check (WIND00182326)
01s,17jul09,w_x  Fix crash in reset callback (WIND00172353)
01r,01jul09,w_x  Optimize usbHubPowerOnPort for realtime compliant(WIND00165419)
01q,11mar09,w_x  Clear the hub status buffer after OS_MALLOC (WIND00156002)
01p,17dec08,w_x  Added mutex to protect hub status data ;
                 Use seperate copy of hub status to avoid potential corruption;
                 Use DMA/cache safe buffer for hub status (WIND00148225)
01o,11dec08,s_z  Change the length request of hub descriptor to support some
                 quirky hub such as Belkin F5U707(WIND00126686)
01n,03jul08,w_x  Fix pHub->pBus NULL dereference (WIND00126885)
01m,10aug07,p_g  defect fix WIND00038567 - handled for a hub which sends
                 0 length of status packet
01l,08oct06,ami  Changes for USB-vxBus changes
01k,18jul06,???  fix for SPR#108289
01j,06jan06,ami  Check for multiple bus-powered hub incorporated (SPR #115386)
01i,18aug05,ami  Fix for enumerating hubs with bcd version 0x101
01h,28mar05,pdg  Allocation of hardware accessed memory changed
01g,23nov04,h_k  fixed over writing read-only-data. (SPR #104386)
01f,15oct04,ami  Refgen Changes
01e,11oct04,ami  Apigen Changes
01d,07oct04,pdg  SPR #100800 fix
01c,03aug04,mta  coverity error fixes
01b,03aug04,ami  Warning Messages Removed
01a,27jun03,nrv  Changing the code to WRS standards
*/

/*
DESCRIPTION

This module provides the utility functions required for the functioning
of the USB Hub Class Driver.

INCLUDE FILES:  usb2/usbOsal.h, usb2/usbHst.h, usb2/usbHubUtility.h,
usb2/usbHubGlobalVariables.h, usb2/usbHubBusManager.h,
usb2/usbHubEventHandler.h, usb2/usbd.h, hwif/vxbus/vxBus.h

*/

/*
INTERNAL
 *******************************************************************************
 * Filename         : usbHubUtility.c
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      : This module provides the utility functions required for
 *                    the functioning of the USB Hub Class Driver.
 *
 *
 ******************************************************************************/



/************************** INCLUDE FILES *************************************/


#include "usb/usbOsal.h"
#include "usb/usbHst.h"
#include "usbHubUtility.h"
#include "usbHubGlobalVariables.h"
#include "usbHubBusManager.h"
#include "usbHubEventHandler.h"

IMPORT int usrUsbWaitTimeOutValueGet(void);
IMPORT USBHST_STATUS usbHstHubTTUpdate
    (
    UINT32 uDeviceHandle,
    UINT32 uNumPorts,
    UINT32 uMTT,
    UINT32 uTTT
    );

/****************** MODULE SPECIFIC FUNCTIONS DECLARATION *********************/


/*
 * This function is used as a call back for the Reset. On failure, reset is
 * retried for a maximum number of tries. If the maximum number of retries
 * expires, the port is disabled.
 */

LOCAL USBHST_STATUS usbHubResetCallback (pUSBHST_URB pResetURB);

/*
 * This function is a general purpose call back function that can be used for
 * issuing a blocking request.
 */

LOCAL USBHST_STATUS usbHubBlockingCallback (pUSBHST_URB pUrb);

/*
 * This function is the call back for the status change interrupt IN pipe
 * completion.
 */

LOCAL USBHST_STATUS usbHubInterruptRequestCallback (pUSBHST_URB pURB);

/*
 * This Function is used to validate the Standard Descriptors
 */

LOCAL USBHST_STATUS usbHubValidateDescriptor (UINT8 *  pDescriptor,
                               UINT16 uUsbDeviceVersion,UINT8 uDescriptorType);

/*
 * This function is the call back function for the completion of a
 * Clear TT request to a High Speed Hub.
 */

LOCAL USBHST_STATUS usbHubClearTTRequestCallback (pUSBHST_URB pURB);

/*
 * This function is the call back function for the completion of a
 * Reset TT request to a High Speed Hub.
 */

LOCAL USBHST_STATUS usbHubResetTTRequestCallback (pUSBHST_URB pURB);


/************************ GLOBAL FUNCTIONS DEFINITION *************************/



/***************************************************************************
*
* usbHubFindPortNumber - finds the port number of a device .
*
* This routine finds the port number of a device.
*
* RETURNS: port number, PORT_NUMBER_NOT_FOUND if the match was not
*          found/invalid params.
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL UINT8 usbHubFindPortNumber
    (
    pUSB_HUB_INFO pHub,
    UINT32        uDeviceHandle
    )
    {
    /* Counter for the port number */

    UINT8 uPortCount = 0;

    /* If pHub is NULL then return PORT_NUMBER_NOT_FOUND */

    if (NULL == pHub)
        {
        /* Debug Message */
        USB_HUB_ERR("invalid parameter \n",
                    1, 2, 3, 4, 5, 6);

        return USB_PORT_NUMBER_NOT_FOUND;

        }/* End of if (NULL==pHub) */


    /*
     * For all ports in the pHub::pPortList that are enabled
     * i.    Retrieve the HUB_PORT_INFO from pHub::pPortList[ port count ].
     * ii.   If the HUB_PORT_INFO::uDeviceHandle is equal to uDeviceHandle then
     *         return the portcount.
     */

    for (uPortCount = 0;
         uPortCount < pHub->HubDescriptor.bNbrPorts;
         uPortCount++)
        {
        /* Retrieve the HUB_PORT_INFO structure */

        pUSB_HUB_PORT_INFO pPort = pHub->pPortList[uPortCount];

        /* Check if the port is enabled */

        if (NULL != pPort)
            {
            /* Check for uDeviceHandle */

            if (uDeviceHandle == pPort->uDeviceHandle)
                {
                /* Debug Message */

                USB_HUB_VDBG("usbHubFindPortNumber - found port %d\n",
                            uPortCount, 2, 3, 4, 5, 6);

                return uPortCount;
                }/* End of if (uDeviceHandle... */
            } /* End of If (NULL != pPort) */
        } /* End of for (uPortCount.... */

    /* Return PORT_NUMBER_NOT_FOUND.*/
    return USB_PORT_NUMBER_NOT_FOUND;

    }/* End of HUB_FindPortNumber() */


/***************************************************************************
*
* usbHubFindParentHubInBuses - searches for a parent hub.
*
* This routine searches for a parent hub.
*
* RETURNS: pointer to the PHUB_INFO, NULL if the match was not found/invalid
* params.
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL pUSB_HUB_INFO usbHubFindParentHubInBuses
    (
    UINT32 uDeviceHandle
    )
    {
    /* The bus List pointer to be used for browsing the list */

    pUSB_HUB_BUS_INFO pBusList = gpGlobalBus;

    /*
     * For every bus in the gpGlobalBus do:
     *   i.     Call HUB_FindParentHub () function to find the matching hub.
     *          If this returns a non NULL value, return the value.
     */

    while (NULL != pBusList)
        {
        /* Find matching hub */

        pUSB_HUB_INFO pHub =
            usbHubFindParentHub(uDeviceHandle, pBusList->pRootHubInfo);

        /* if found return the value */

        if (NULL != pHub)
            {
            /* Debug Message */

            USB_HUB_VDBG("usbHubFindParentHubInBuses - "
                         "found the matching hub 0x%X (pHub addr %p)\n",
                         pHub->uDeviceHandle, pHub, 3, 4, 5, 6);

            return pHub;

            }/* End of if (NULL!=pHub) */

        /* move the bus list pointer to the next bus pointer */

        pBusList = pBusList->pNextBus;

        }/* End of While (NULL != pBusList) */

    return NULL;

    } /* End of HUB_FindParentHubInBuses() */

/***************************************************************************
*
* usbHubFindParentHub - recursively searches for the parent hub.
*
* This routine recursively searches for the parent hub of the specified  device.
*
* RETURNS: pointer to the PHUB_INFO, NULL if the match was not found/invalid
* params.
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL pUSB_HUB_INFO usbHubFindParentHub
    (
    UINT32        uDeviceHandle ,
    pUSB_HUB_INFO pHub
    )
    {
    /* Counter for the port number */

    UINT8 uPortCount = 0;

    /* If pHub is NULL then return NULL*/

    if (NULL == pHub)
        {
        return NULL;
        }/* End of (NULL == pHub) */


    /*
     * For all ports in the pHub::pPortList that are not NULL
     * i.     Retrieve the HUB_PORT_INFO from pHub::pPortList[ port count]
     * ii.    If uDeviceHandle is same as HUB_PORT_INFO::uDeviceHandle  then
     *        return pHub
     */
    for (uPortCount = 0;
         uPortCount < pHub->HubDescriptor.bNbrPorts;
         uPortCount++)
        {
        /* Retrieve the HUB_PORT_INFO structure */

        pUSB_HUB_PORT_INFO pPort = pHub->pPortList[uPortCount];

        /* Check if the port is enabled */

        if (NULL != pPort)
            {
            /* Check for uDeviceHandle */

            if (uDeviceHandle == pPort->uDeviceHandle)
                {
                return pHub;
                }/* End of if (uDeviceHandle... */
            } /* End of If (NULL != pPort) */
        } /* End of for (uPortCount.... */


    /*
     * For all ports in the pHub::pPortList that are not NULL
     * i.    Retrieve the HUB_PORT_INFO from pHub::pPortList[ port count]
     * ii.     If HUB_PORT_INFO::pHub is not NULL then:
     * iii.    Call HUB_FindParentHub () function to find the matching hub. If
     *         this
     * returns a non NULL value, return the value.
     */

    for (uPortCount = 0;
         uPortCount < pHub->HubDescriptor.bNbrPorts;
         uPortCount++)
        {
        /* Retrieve the HUB_PORT_INFO structure */

        pUSB_HUB_PORT_INFO pPort = pHub->pPortList[uPortCount];

        /* Check if the port is enabled */

        if (NULL != pPort)
            {
            /* Check for Hub device */

            if (NULL != pPort->pHub)
                {
                /* storage for storing the result */

                pUSB_HUB_INFO pResultHub ;

                /* Call for matching hub in  this sub tree. */

                pResultHub = usbHubFindParentHub(uDeviceHandle, pPort->pHub);

                /* Check if the hub was found */

                if (NULL != pResultHub)
                    {
                    return pResultHub;

                    }/* End of if (NULL != pResultHub ) */
                }/* End of if (NULL != pPort->pHub) */
            } /* End of If (NULL != pPort) */
        } /* End of for (uPortCount.... */

    /* Return NULL*/
    return NULL;

    }/* End of HUB_FindParentHub() */

/***************************************************************************
*
* usbHubCreateBusStructure - Creates a bus structure .
*
* Creates a bus structure and connects the same to the global Bus list.
* This also launches a thread to handle the events occurring on the Bus.
*
* RETURNS: HUB_BUS_INFO, NULL .
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL pUSB_HUB_BUS_INFO usbHubCreateBusStructure
    (
    UINT8 uBusHandle
    )
    {
    char busManagerName [USB_BUSM_TASK_NAME_BASE_LEN];
    pUSB_HUB_BUS_INFO pBusList = gpGlobalBus;        /* Bus List pointer */

    /* Set Bus Manager Thread Base Name */

    strncpy ((char *) busManagerName, USB_BUSM_TASK_NAME_BASE, USB_BUSM_TASK_NAME_BASE_LEN);

    /* Set the tag as the bus handle */

    busManagerName [USB_BUSM_TASK_NAME_BASE_LEN - 2] = (char)('A' +(uBusHandle%26));

    /*
     * Browse the HUB_BUS_INFO list and check if gpGlobalBus exists. If it
     * exists, return NULL.
     */

    while (NULL != pBusList)
        {
        /* Check for duplicate entries */
        if (pBusList->uBusHandle == uBusHandle)
            {
            /* Debug Message */
            USB_HUB_WARN("usbHubCreateBusStructure - "
                          "bus strcutre already exists for bus handle 0x%X\n",
                         uBusHandle, 2, 3, 4, 5, 6);
            return NULL;

            } /* End of if (pBusList->....*/

        /* move to the next bus */
        pBusList = pBusList->pNextBus;

        } /* End of While (NULL != pBusList) */

    /*
     * Call OS_MALLOC() to allocate memory for HUB_BUS_INFO structure.
     * If failed, return NULL
     */

    pBusList = OS_MALLOC(sizeof(USB_HUB_BUS_INFO));

    /* Check the result */

    if (NULL == pBusList)
        {
        /* Debug Message */
        USB_HUB_ERR("failed to allocate bus information for bus handle 0x%X\n",
                    uBusHandle, 2, 3, 4, 5, 6);

        return NULL;

        }/* End of if (NULL== pBusList) */

    /* Clear the allocated structure.*/

    OS_MEMSET(pBusList, 0, sizeof(USB_HUB_BUS_INFO));


    /* set HUB_BUS_INFO::uBusHandle as uBusHandle*/

    pBusList->uBusHandle = uBusHandle;

    /* set HUB_BUS_INFO::pNextBus as gpGlobalBus*/

    pBusList->pNextBus = gpGlobalBus;

    /* Create the binary semaphore for device reset on this bus */

    pBusList->hDeviceResetDoneSem = OS_CREATE_EVENT(OS_EVENT_NON_SIGNALED);

    if (NULL == pBusList->hDeviceResetDoneSem)
        {
        /* Debug Message */

        USB_HUB_ERR("failed to create hDeviceResetDoneSem for bus handle 0x%X\n",
                    uBusHandle, 2, 3, 4, 5, 6);

        OS_FREE(pBusList);

        return NULL;

        }

#ifdef USB_HUB_CREATE_BUSM_TASK_FOR_EACH_BUS

    /*
     * Call the OS_CREATE_THREAD() function to create a thread (the function as
     * HUB_BusManager() will be spawned as a thread with HUB_BUS_INFO as the
     * parameter) If the thread creation fails:
     * i.    Call the OS_FREE() function to free the memory allocated to the
     *       HUB_BUS_INFO structure.
     * ii.   Destroy the binary semaphore for device reset
     * iii.   Return NULL
     * Set the HUB_BUS_INFO::BusManagerThreadID as the thread id returned by
     * the OS_CREATE_THREAD() function.
     */

    pBusList->BusManagerThreadID = OS_CREATE_THREAD((char *) busManagerName,
                                                    USB_HUB_THREAD_PRIORITY,
                                                    usbHubThread,
                                                    (long)pBusList);

    /* Check for the success  of thread creation */

    if (OS_THREAD_FAILURE == pBusList->BusManagerThreadID)
        {

        /* Destroy the binary semaphore */

        OS_DESTROY_EVENT(pBusList->hDeviceResetDoneSem);

        /* Set to NULL */

        pBusList->hDeviceResetDoneSem = NULL;

        /* Free the memory allocated */

        OS_FREE(pBusList);

        /* Debug Message */

        USB_HUB_ERR("thread(%s %c) created failed for bus manager \n",
                    USB_BUSM_TASK_NAME_BASE, (char)('A' +(uBusHandle%26)),
                    3, 4, 5, 6);

        /*return failure */
        return NULL;

        }/* End of if (NULL==pBusList->BusMan... */
#endif

   /* Set  gpGlobalBus  as HUB_BUS_INFO*/

   gpGlobalBus = pBusList;

   /* Return HUB_BUS_INFO structure.*/
   return pBusList;

   }/* End of HUB_CreateBusStructure() */


/***************************************************************************
*
* usbHubDeleteBusStructure - deletes the Bus Structure.
*
* This routine deletes the Bus Structure and kills any threadassociated with it.
*
* RETURNS: None
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL void usbHubDeleteBusStructure
    (
    UINT8 uBusHandle
    )
    {
    /* Bus List pointer */

    pUSB_HUB_BUS_INFO pBusList = gpGlobalBus;

    /* Previous bus pointer -to be used for unlinking */

    pUSB_HUB_BUS_INFO pPrevBusList = gpGlobalBus;

    /*
     * Browse the gpGlobalBus list and check if uBusHandle exists. If it does
     * not exists, return.
     */

    while (NULL != pBusList)
        {
        /* Check for correct entry */

        if (pBusList->uBusHandle == uBusHandle)
            {
            /* Jump out of the loop */
            break;

            } /* End of if (pBusList->....*/

        /* Set the previous bus list pointer as the current one */

        pPrevBusList = pBusList;

        /* Go to the next bus list */

        pBusList = pBusList->pNextBus;

        } /* End of While (NULL != pBusList) */

    /* Check if we found the bus */

    if (NULL == pBusList)
        {
        /* Debug Message */

        USB_HUB_ERR("bus handle 0x%X not found \n",
                    uBusHandle, 2, 3, 4, 5, 6);

        /* nope.. we did nto find the bus, so we return */
        return;

        } /* End of if (NULL==pBusList) */

    /* Unlink the HUB_BUS_INFO from the gpGlobalBus list.(BEGIN) */

    /* Check if the previous bus list is the same as the global bus list */

    if (( pPrevBusList == gpGlobalBus) && (pPrevBusList == pBusList))
        {
        /* This is the first bus */

        /* Set the global pointer to this */
        gpGlobalBus = pBusList->pNextBus;
        }
    else
        {
        /* This is not the first bus */

        /* Reset the pointer to point to the next bus */

        pPrevBusList->pNextBus = pBusList->pNextBus;

        } /* End of if (pPrevBusList ==.. */

    /* Unlink the HUB_BUS_INFO from the gpGlobalBus list.(END) */

#ifdef USB_HUB_CREATE_BUSM_TASK_FOR_EACH_BUS


    /*
     * Call OS_DESTROY_THREAD() to kill the thread with thread ID as
     * HUB_BUS_INFO:: BusManagerThreadID
     */

    OS_DESTROY_THREAD(pBusList->BusManagerThreadID);

    pBusList->BusManagerThreadID = OS_THREAD_FAILURE;
#endif

    /* Destroy the binary semaphore */

    OS_DESTROY_EVENT(pBusList->hDeviceResetDoneSem);

    /* Set to NULL */

    pBusList->hDeviceResetDoneSem = NULL;

    /* Call OS_FREE() function to free the memory allocated for HUB_BUS_INFO*/

    OS_FREE(pBusList);

    return;
    }/* End of Hub_DeleteBusStructure() */

/***************************************************************************
*
* usbHubRemoveDevice - calls the remove device of the Host stack.
*
* This routine calls the remove device of the Host stack.This also frees the
* memory as required.
*
* RETURNS: USBHST_SUCCESS, USBHST_INVALID_PARAMETER if there are invalid params
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubRemoveDevice
    (
    pUSB_HUB_INFO pHub,
    UINT8         uPortIndex
    )
    {
    /* To Store the port information */

    pUSB_HUB_PORT_INFO pPort = NULL;

    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
        USB_HUB_WV_DEVICE,
        "Entering usbHubRemoveDevice() Function",
        USB_HUB_WV_FILTER);

    /* Verify the Parameters */

    if (NULL == pHub)
        {
        /* Debug Message */
        USB_HUB_ERR("invalid parameter \n",
                    1, 2, 3, 4, 5, 6);

        /* Return invalid parameter */
        return USBHST_INVALID_PARAMETER;

        } /* End of if (NULL == pHub) */

    /* Verify the Parameters */

    if (uPortIndex >= pHub->HubDescriptor.bNbrPorts)
        {
        /* Debug Message */

        USB_HUB_ERR("portIndex %d >= max %d \n",
                    uPortIndex,
                    pHub->HubDescriptor.bNbrPorts,
                    3, 4, 5, 6);

        /* Return invalid parameter */
        return USBHST_INVALID_PARAMETER;

        } /* End of if (uPortIndex >= pHub->HubDescriptor.bNbrPorts) */

    /* Copy the pPort value */

    pPort = pHub->pPortList[uPortIndex];

    /*
     * If the port is enabled then Call the
     * g_USBHSTFunctionList::USBHST_RemoveDevice() function with
     * HUB_PORT_INFO::uDeviceHandle.
     */

    if (NULL != pPort)
        {
        /* Debug Message */

        USB_HUB_DBG("usbHubRemoveDevice - delete hub 0x%X port %d\n",
                    pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

        /*
         * If the port is disconnected during reset, set the bus
         * resetting flag to FALSE and the reset check count to
         * zero so that other new connections would not go through
         * the false reset check process.
         */

        if ((pPort->StateOfPort == USB_HUB_RESET_PENDING) ||
            (pPort->StateOfPort == USB_HUB_RESET_COMPLETION_PENDING))
            {
            USB_HUB_WARN("usbHubRemoveDevice - Set bDeviceBeingConfigured to FALSE\n",
                        pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

            pHub->pBus->bDeviceBeingConfigured = FALSE;
            pHub->pBus->uResetCheckCount = 0;
            }

        if ((NULL != pHub->pBus) &
            (USB_HUB_PORT_CONFIGURED > pPort->StateOfPort) &
            (USB_HUB_RESET_PENDING < pPort->StateOfPort))
            {
            /* Set the bus state as no device is being configured */

            pHub->pBus->bDeviceBeingConfigured = FALSE;
            }

        /* Check if this port is a hub device or not */

        if (NULL == pPort->pHub)
            {
            /* Debug Message */

            USB_HUB_DBG("usbHubRemoveDevice - "
                        "delete normal device on hub 0x%X port %d\n",
                        pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

            /* disable the port */
            pHub->pPortList[uPortIndex] = NULL;

            /* Call remove device */
            if (0 != pPort->uDeviceHandle)
                {
                g_usbHstFunctionList.UsbdToHubFunctionList.removeDevice(pPort->uDeviceHandle);
                }/* End of if (0 != pPort->uDeviceHandle) */

            /* Destroy the port status semaphore */

            OS_DESTROY_EVENT(pPort->hPortStatusSem);

            /* Set the handle as NULL */

            pPort->hPortStatusSem = NULL;

            /* Free the memory */
            OS_FREE(pPort);
            }
        else
            /* an Hub device */
            {
            /* Debug Message */

            USB_HUB_DBG("usbHubRemoveDevice - "
                         "delete hub device on hub 0x%X port %d\n",
                         pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

            /* Call remove device */
            g_usbHstFunctionList.UsbdToHubFunctionList.removeDevice(pPort->uDeviceHandle);

            /*
             * pPort was be freed in the above call, so do not refer to
             * pPort elements thereafter!
             */
            } /* End of if (NULL == pPort->pHub) */
        }/* End of if (NULL != pPort ) */

    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
        USB_HUB_WV_DEVICE,
        "Exiting usbHubRemoveDevice() Function",
        USB_HUB_WV_FILTER);

    return USBHST_SUCCESS;
    } /* End of HUB_RemoveDevice() */

/***************************************************************************
*
* usbHubPortDebounceHandler - used for handling de-bounce condition.
*
* This routine handles the de-bounce condition.
*
* RETURNS: USBHST_SUCCESS, USBHST_FAILURE,
* USBHST_INVALID_PARAMETER if there are invalid params
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubPortDebounceHandler
    (
    pUSB_HUB_INFO pHub,
    UINT8         uPortIndex
    )
    {
    /* Storage for the port informaiton */

    pUSB_HUB_PORT_INFO pPort = NULL;

    /* To store the port status */

    pUSB_HUB_PORT_STATUS pPortStatus = NULL;

    /* To store the length of the  status */

    UINT8 uLength = sizeof(USB_HUB_PORT_STATUS) ;

    /* To store the result of the requests */

    USBHST_STATUS  Result;

    /* To store the max tier possible  */

    UINT8 uMaxTier =0;

    /* If the pHub or pHub->pBus is NULL then return USBHST_INVALID_PARAMETER */

    if ((NULL == pHub) || (NULL == pHub->pBus))
        {
        /* Debug Message */
        USB_HUB_ERR("invalid parameters \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of if (NULL ==pHub) */

    /*
     * If the uPortIndex greater than pHub::HubDescriptor::bNbrPorts then
     * return USBHST_INVALID_PARAMETER.
     * Note: the actual port number is the port index + 1
     */

    if (uPortIndex >= pHub->HubDescriptor.bNbrPorts)
        {
        /* Debug Message */
        USB_HUB_ERR("portIndex %d >= max %d \n",
                    uPortIndex,
                    pHub->HubDescriptor.bNbrPorts,
                    3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of if (uPortIndex >= pHub->HubDescriptor.bNbrPorts) */

    /*
     * Retrieve the HUB_PORT_INFO structure from pHub::pPortList for
     * the uPortIndex.
     */

    pPort = pHub->pPortList[uPortIndex];

    if (NULL == pPort)
        {
        /* Debug Message */

        USB_HUB_ERR("pPort is null for port %d\n",
                    uPortIndex, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }

    /*
     * If PORT_INFO::StateOfPort is not HUB_DEBOUNCE_PENDING then
     * return USBHST_INVALID_PARAMETER.
     */
    if (USB_HUB_DEBOUNCE_PENDING != pPort->StateOfPort)
        {
        /* Debug Message */

        USB_HUB_ERR("debounce not pending for port %d\n",
                    uPortIndex, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of if (HUB_DEBOUNCE... */

    /* Wait for debounce period to expire */

    OS_DELAY_MS(100);

    /* Allocate memory for the port status */

    pPortStatus = OS_MALLOC(sizeof(USB_HUB_PORT_STATUS));

    /* Check if memory allocation is successful */

    if (pPortStatus == NULL)
        {
        /* Debug Message */
        USB_HUB_ERR("port status memory allocate failed for hub 0x%X port %d\n",
                    pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

        return USBHST_INSUFFICIENT_MEMORY;
        }

    /* Initialize the memory allocated */

    OS_MEMSET(pPortStatus, 0, sizeof(USB_HUB_PORT_STATUS));

    /*
     * Call HUB_GET_PORT_STATUS() to get the HUB_PORT_STATUS structure.
     * If this fails then return result.
     */

    Result = USB_HUB_GET_PORT_STATUS(pHub,
                                     uPortIndex,
                                     (PUINT8)(pPortStatus),
                                     &uLength);

    if (USBHST_SUCCESS != Result)
        {
        /* Debug Message */

        USB_HUB_ERR("failed to get port status for hub 0x%X port %d, result=%d\n",
                    pHub->uDeviceHandle, uPortIndex, Result, 4, 5, 6);

        /* Release memory allocated for port status */

        OS_FREE(pPortStatus);

        return Result;

        }/* End of if (USBHST_SUCCESS... */

   /* swap the members of the structure */

    pPortStatus->wPortChange  = OS_UINT16_LE_TO_CPU(pPortStatus->wPortChange);
    pPortStatus->wPortStatus  = OS_UINT16_LE_TO_CPU(pPortStatus->wPortStatus);

    /*
     * If the HUB_PORT_STATUS:;wPortStatus is DISCONNECTED then
     * i.    Set the HUB_PORT_INFO::StateOfPort as MARKED_FOR_DELETION
     * ii.   Return USBHST_SUCCESS
     */

    if (0 ==( pPortStatus->wPortStatus & USB_PORT_CONNECTION_VALUE))
        {
        /* Debug Message */

        USB_HUB_DBG("usbHubPortDebounceHandler - "
                    "no device connected on hub 0x%X port %d\n",
                    pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

        /* Mark the port for deletion */
        USB_MARK_FOR_DELETE_PORT(pHub, pPort);

        /* Release memory allocated for port status */

        OS_FREE(pPortStatus);

        return  USBHST_SUCCESS;

        } /* End of if (0==... */

    /* Release memory allocated for port status */

    OS_FREE(pPortStatus);

    /*
     * If the device is connected to a High speed bus,
     * The max tiers allowed is as per 2.00 spec
     * else
     * The max tiers allowed is as per 1.1 spec
     */
    if (USBHST_HIGH_SPEED != pHub->pBus->uBusSpeed)
        {
        uMaxTier = HUB_MAX_ALLOWED_DEPTH_USB1_1 ;
        }
    else
        {
        uMaxTier = HUB_MAX_ALLOWED_DEPTH_USB2_0;
        }


    /*
     * if pHub::uCurrentTier is HUB_MAX_ALLOWED_DEPTH then
     * i.    Set the HUB_PORT_INFO::StateOfPort as MARKED_FOR_DELETION
     * ii.   Return USBHST_SUCCESS
     */

    if (uMaxTier == pHub->uCurrentTier)
        {
        /* Debug Message */

        USB_HUB_WARN("usbHubPortDebounceHandler - "
                    "current tier %d for hub 0x%X exceeded max %d\n",
                    pHub->uCurrentTier,
                    pHub->uDeviceHandle, uMaxTier, 4, 5, 6);

        /* Mark the port for deletion */
        USB_MARK_FOR_DELETE_PORT(pHub,pPort);

        return  USBHST_SUCCESS;

        }/* End of if (HUB_MAX_ALLO... */


    /* Update the StateOfPort as HUB_RESET_PENDING.*/

    pPort->StateOfPort = USB_HUB_RESET_PENDING;

    return  USBHST_SUCCESS;
    } /* End of HUB_PortDebounceHandler() */


/***************************************************************************
*
* usbHubConfigure - Configures a given hub.
*
*
* This routine configures a given hub.
*
* RETURNS: USBHST_SUCCESS, USBHST_FAILURE,
* USBHST_INSUFFICIENT_MEMORY if the  memory allocation failed.
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubConfigure
    (
    pUSB_HUB_INFO    * pHub,
    UINT32             uDeviceHandle,
    pUSB_HUB_BUS_INFO  pBus,
    pUSB_HUB_INFO      pParentHub
    )
    {
    /* To store the return value */

    USBHST_STATUS Result = USBHST_FAILURE;

    /* Buffer to store incoming Device descriptor */

    UINT8  *      pDeviceDescriptor= NULL;

    /* Buffer to store incoming Configuration descriptor */

    UINT8  *     pConfigDescriptor= NULL;

    /* Pointer to parse Configuration descriptor */

    UINT8  *     pParseDesc = NULL ;

    /* Pointer to the hub descriptor */

    UINT8 *    pHubDescriptor = NULL;

    /* Buffer for the status change interrupt IN bit map data */

    UINT8  *     pStatusChangeData = NULL;

    /* Data Store for the status change interrupt IN bit map data */

    UINT8  *     pStatusData = NULL;

    /* Data Store for the status change interrupt IN bit map data */

    UINT8  *     pCurrentStatusData = NULL;

    /* Hub Structure for this hub */

    pUSB_HUB_INFO pNewHub    = NULL;

    /* Length of descriptor to be stored here */

    UINT32         uLength   = 0;

    /* Number of ports */

    UINT8         uNumPorts  = 0;

    /* Counter for the port number */

    UINT8         uPortCount = 0;

    /* Structure to parse the device descriptor */

    pUSBHST_DEVICE_DESCRIPTOR pParseDeviceDesc = NULL;

    /* Structure to parse the configuration descriptor */

    pUSBHST_CONFIG_DESCRIPTOR pParseConfigDesc = NULL;

    /* Structure to parse the interface descriptor */

    pUSBHST_INTERFACE_DESCRIPTOR pParseInterfaceDesc = NULL;

    /* Structure to parse the interface descriptor */

    pUSBHST_INTERFACE_DESCRIPTOR pParseAlternateInterfaceDesc = NULL;

    /* Structure to parse the endpoint descriptor */

    pUSBHST_ENDPOINT_DESCRIPTOR pParseEndpointDesc = NULL;

    /* Variable to store the sequence of descriptors */

    UINT32 uDescriptorSequence = 0;

    /* Count for number of descriptors which follow config desc */

    UINT8  uCount = 0;

    /* Parse Count for configuration Descriptor */

    UINT8    parseCount = 3;

    /* parsed length of desc to be updated here */

    UINT16 uLengthParsed = 0;

    /* length of desc to be stored here */

    UINT16 uDescLength =0 ;

    UINT16 * pStatusOfHub = NULL;

    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
            USB_HUB_WV_DEVICE,
            "Entering usbHubconfigure() Function",
            USB_HUB_WV_FILTER);

    if ((pHub == NULL) || (pBus == NULL))
        {
        USB_HUB_ERR("Invalid Parameter pHub %p, pBus %p\n",
                    pHub, pBus, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Set the output as NULL until successfully created the new hub */

    *pHub = NULL;

    /*
     * Call OS_MALLOC() to allocate Buffer of 18 bytes. If call failed,
     * return USBHST_INSUFFICIENT_MEMORY.
     */

    pDeviceDescriptor = (UINT8 *) OS_MALLOC(18 * sizeof(UINT8));

    /* Check for call failure */

    if (NULL == pDeviceDescriptor)
        {
        /* Debug Message */
        USB_HUB_ERR("memory allocate failed for device descriptor \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INSUFFICIENT_MEMORY;

        }/* End of if (NULL == pDeviceDescriptor ) */


    /* Clear the allocated buffer.*/
    OS_MEMSET(pDeviceDescriptor, 0, sizeof(UINT8)*18);

    uLength = HUB_LENGTH_DEVICE_DESC;

    /*
    * Call USBHST_GetDescriptor() for uDeviceHandle,
    * DEVICE_DESCRIPTOR and for 18 bytes. If call fails,
    * i.    Call OS_FREE() to free the Buffer.
    * ii.    Return result.
    */
    Result= usbHstGetDescriptor ( uDeviceHandle, /* Device Handle */
                                  USBHST_DEVICE_DESC, /* Desc type */
                                  0, /* descriptor index */
                                  0, /* language ID */
                                  &uLength, /* Size of the descriptor */
                                  /* buffer to copy to */
                                  (UINT8 *)pDeviceDescriptor );

    if (( USBHST_SUCCESS != Result) || (HUB_LENGTH_DEVICE_DESC != uLength ))
        {
        /* Debug Message */

        USB_HUB_ERR("failed to get device descriptor: " \
                    "result=%d, length=%d \n",
                    Result, uLength, 3, 4, 5, 6);

        /* Free the memory allocated */
        OS_FREE (pDeviceDescriptor);

        return USBHST_FAILURE;

        }/* End of if ( USBHST_SUCCESS !=Result) */

    /* Parse the Device Descriptor buffer */
    pParseDeviceDesc = (pUSBHST_DEVICE_DESCRIPTOR) pDeviceDescriptor;

    /* Validate the device descriptor */

    Result = usbHubValidateDescriptor(pDeviceDescriptor,
                                      pParseDeviceDesc->bcdUSB,
                                      USBHST_DEVICE_DESC );

    if(USBHST_SUCCESS != Result)
        {
        /* Debug Message */
        USB_HUB_ERR("invalid device descriptor: " \
                    "result=%d \n",
                    Result, 2, 3, 4, 5, 6);

        /* Free the memory allocated */

        OS_FREE (pDeviceDescriptor);

        return USBHST_INVALID_PARAMETER;
        }


    /*
     * Call OS_MALLOC() to allocate Buffer of 8 bytes. If call failed,
     * return USBHST_INSUFFICIENT_MEMORY.
     */
    pConfigDescriptor = (UINT8 * ) OS_MALLOC(8*sizeof(UINT8));

    /* Set the desc length as 8 */
    uLength = HUB_LENGTH_CONFIG_DESC;

    /* Check for call failure */
    if (NULL == pConfigDescriptor)
        {
        /* Debug Message */
        USB_HUB_ERR("memory allocate failed for config descriptor \n",
                    1, 2, 3, 4, 5, 6);

        OS_FREE (pDeviceDescriptor);
        return USBHST_INSUFFICIENT_MEMORY;

        }/* End of if (NULL == pConfigDescriptor ) */

    /* Clear the allocated buffer.*/

    OS_MEMSET(pConfigDescriptor, 0, sizeof(UINT8)*8);

    /*
     * Call USBHST_GetDescriptor() for uDeviceHandle,
     * CONFIGURATION_DESCRIPTOR and for 8 bytes. If call fails,
     * i.    Call OS_FREE() to free the Buffer.
     * ii.    Return result.
     */
    Result= usbHstGetDescriptor(    uDeviceHandle, /* Device Handle */
                                     USBHST_CONFIG_DESC, /* Desc type */
                                     /* descriptor index */
                                     USB_DEF_ACTIVE_HUB_CONFIG_INDEX,
                                     0 , /* language ID */
                                     &uLength, /* Size of the descriptor */
                                     /* buffer to copy to */
                                     (UINT8 *)pConfigDescriptor );
    if (( USBHST_SUCCESS != Result)||(HUB_LENGTH_CONFIG_DESC != uLength ))
        {
        /* Debug Message */
        USB_HUB_ERR("failed to get config descriptor: " \
                    "result=%d, length=%d \n",
                    Result, uLength, 3, 4, 5, 6);

        /* Free the memory allocated */

        OS_FREE (pDeviceDescriptor);

        OS_FREE (pConfigDescriptor);

        return USBHST_FAILURE;

        }/* End of if ( USBHST_SUCCESS !=Result) */

    /*
     * Parse the configuration descriptor and find the complete length of the
     * descriptor.
     */

    uLength = (UINT32)USB_HUB_CONFIG_wTotalLength(pConfigDescriptor);

    /* Call OS_FREE to free the Buffer.*/

    OS_FREE(pConfigDescriptor);

    /*
     * Call OS_MALLOC() to allocate configuration buffer for the complete length
     * of the descriptor. If call failed, return USBHST_INSUFFICIENT_MEMORY.
     */

    pConfigDescriptor = (UINT8 *) OS_MALLOC(uLength*sizeof(UINT8));

    /* Check for call failure */

    if (NULL == pConfigDescriptor)
        {
        /* Debug Message */
        USB_HUB_ERR("memory allocate failed for complete " \
                    "config descriptor \n",
                    1, 2, 3, 4, 5, 6);

        OS_FREE (pDeviceDescriptor);

        return USBHST_INSUFFICIENT_MEMORY;

        }/* End of if (NULL == pConfigDescriptor ) */

    /* Clear the allocated buffer */

    OS_MEMSET(pConfigDescriptor, 0, sizeof(UINT8)*uLength);

    /*
     * Call HUB_GetDescriptor() for uDeviceHandle, CONFIGURATION_DESCRIPTOR and
     * for complete length. If call fails,
     * i.    Call OS_FREE() to free the configuration buffer.
     * ii.    Return USBHST_FAILURE.
     */
    Result= usbHstGetDescriptor(    uDeviceHandle, /* Device Handle */
                                     USBHST_CONFIG_DESC, /* Desc type */
                                     /* descriptor index */
                                     USB_DEF_ACTIVE_HUB_CONFIG_INDEX,
                                     0 , /* language ID */
                                     &uLength , /* Size of the descriptor */
                                     /* buffer to copy to */
                                     (UINT8 *)pConfigDescriptor );

    /* check  the  result and lenth of descriptor*/
    if (( USBHST_SUCCESS != Result)||
        (uLength != (UINT32)USB_HUB_CONFIG_wTotalLength(pConfigDescriptor)) )
        {
        /* Debug Message */
        USB_HUB_ERR("failed to get complete config descriptor: " \
                    "result=%d, length=%d \n",
                    Result, uLength, 3, 4, 5, 6);

        /* Free the memory allocated */
        OS_FREE (pDeviceDescriptor);

        /* Free the memory allocated */
        OS_FREE (pConfigDescriptor);

        return USBHST_FAILURE;

        }/* End of if ( USBHST_SUCCESS !=Result) */

     /* pointer for parsing the desc */

     pParseDesc = pConfigDescriptor;

    /*
     * In this loop we parse the config desc and also keep track of the sequence
     * in which the desriptors arrive
     * 1) If Default Interface Descriptor then update uDescriptorSequence as
     *    value one left shifted by (Four * ucount)
     * 2) If Alternate Interface Descriptor then update uDescriptorSequence as
     *    value Four left shifted by (Four * ucount)
     * 3) If End Point Descriptor then update uDescriptorSequence as
     *    value two left shifted by (Four * ucount)
     */

     /* for Multiple TT hub the bDeviceProtocol is 2 hence the ucount should be
      * compared to 5
      */
     if (pParseDeviceDesc->bDeviceProtocol == HUB_PROTOCOL_MULTIPLE_TT)
         {
         parseCount = 5;
         }

     /* while loop to parse the configuration descriptor */

     while(uCount < parseCount)
        {

        /* this switch  selects the descriptor type  */
        switch(pParseDesc[1])
            {
            /* this is a interface descriptor */
            case USBHST_INTERFACE_DESC :

                /*validate the endpoint descriptor */
                Result = usbHubValidateDescriptor
                            (
                            pParseDesc,
                            pParseDeviceDesc->bcdUSB,
                            USBHST_INTERFACE_DESC
                            );

                if (USBHST_SUCCESS != Result)
                    {

                    /* Debug Message */
                    USB_HUB_ERR("invalid interface descriptor: " \
                                "result=%d \n",
                                Result, 2, 3, 4, 5, 6);

                    /* Free the memory allocated */
                    OS_FREE (pDeviceDescriptor);

                    /* Free the memory allocated */
                    OS_FREE (pConfigDescriptor);

                    return USBHST_INVALID_PARAMETER ;

                    }/* end of if(USBHST_SUCCESS != Result) */

                /* this is valid interface descriptor */

                /* this is default interface descriptor */
                if (pParseDesc[3] == 0)
                    {
                    pParseInterfaceDesc = (pUSBHST_INTERFACE_DESCRIPTOR)pParseDesc;
                    uDescriptorSequence |= USB_HUB_INTERFACE_DEFAULT_DESC_SEQ;
                    }/* end of if(pParseDesc[3]......)*/

                /* this is Alternate interface descriptor */
                else
                    {
                    pParseAlternateInterfaceDesc = (pUSBHST_INTERFACE_DESCRIPTOR)pParseDesc;
                    uDescriptorSequence |= USB_HUB_INTERFACE_ALTERNATE_DESC_SEQ;
                    } /* end of else */

                uCount++;

                break;

            /* this is a endpoint descriptor */
            case USBHST_ENDPOINT_DESC :

                /*validate the endpoint descriptor */
                Result = usbHubValidateDescriptor
                            (
                            pParseDesc,
                            pParseDeviceDesc->bcdUSB,
                            USBHST_ENDPOINT_DESC
                            );

                if (USBHST_SUCCESS != Result)
                    {

                    /* Debug Message */
                    USB_HUB_ERR("invalid endpoint descriptor: " \
                                "result=%d \n",
                                Result, 2, 3, 4, 5, 6);

                    /* Free the memory allocated */
                    OS_FREE (pDeviceDescriptor);

                    /* Free the memory allocated */
                    OS_FREE (pConfigDescriptor);

                    return USBHST_INVALID_PARAMETER ;

                    }/* end of if(USBHST_SUCCESS != Result) */

                /* This is a valid endpoint descriptor */
                pParseEndpointDesc = (pUSBHST_ENDPOINT_DESCRIPTOR) pParseDesc;
                if (uDescriptorSequence & USB_HUB_ENDPOINT_DESC_SEQ)
                    uDescriptorSequence  |= USB_HUB_ENDPOINT_DESC_SEQ << 8;
                else
                    uDescriptorSequence  |= USB_HUB_ENDPOINT_DESC_SEQ;

                uCount++;

                break;

            /* this is a configuration descriptor */
            case USBHST_CONFIG_DESC :
                /* validate the configuration descriptor */
                Result = usbHubValidateDescriptor
                            (
                            pParseDesc,
                            pParseDeviceDesc->bcdUSB,
                            USBHST_CONFIG_DESC
                            );

                if (USBHST_SUCCESS != Result)
                    {

                    /* Debug Message */
                    USB_HUB_ERR("invalid config descriptor: " \
                                "result=%d \n",
                                Result, 2, 3, 4, 5, 6);

                    /* Free the memory allocated */
                    OS_FREE (pDeviceDescriptor);

                    /* Free the memory allocated */
                    OS_FREE (pConfigDescriptor);

                    return USBHST_INVALID_PARAMETER ;

                    }/* end of if(USBHST_SUCCESS != Result) */

                /* This is a valid configuration descriptor */
                pParseConfigDesc = (pUSBHST_CONFIG_DESCRIPTOR)pParseDesc;

                uCount++;

                break;

            default: /* unknown descriptor type */

                /* Debug Message */
                USB_HUB_DBG("unknown descriptor type %d\n",
                            pParseDesc[1], 2, 3, 4, 5, 6);

                /* ignore it */
            }/*end of switch (pParseDesc[1]) */

        uDescLength = pParseDesc[0];

        /* check descriptor length, zero lengh can cause a dead loop */

        if (uDescLength == 0)
            {
            USB_HUB_ERR("Invalid descriptor length\n",
                        1, 2, 3, 4, 5, 6);

            break;
            }

        /* check are  we jumping off the  descriptor  */

        if ((pParseConfigDesc != NULL ) && (uDescLength + uLengthParsed
            >= USB_HUB_CONFIG_wTotalLength((UINT8 *)pParseConfigDesc)))
            {
            break;
            }/* end of if ( uDescLength + ....)*/

        /* update the length parsed */

        uLengthParsed = (UINT16)(uLengthParsed + uDescLength);

        /*
         * move the pointer to the next desc by adding the length of
         * descriptor to the pointer
         */

        pParseDesc =(UINT8 *) (((ULONG)pParseDesc)+(UINT32)uDescLength);
    }/* end of  while(uCount < 3)    */

    /* Validate if the  descriptor have arrived in correct sequence */

    if(1 != (USB_VALIDATE_DESCRIPTOR_SEQUENCE(uDescriptorSequence)
                & (uCount <= parseCount)))
        {
        USB_HUB_ERR("missing necessary descriptor\n",
                    1, 2, 3, 4, 5, 6);

        /* Free the memory allocated */

        OS_FREE (pDeviceDescriptor);

        /* Free the memory allocated */

        OS_FREE (pConfigDescriptor);

        return USBHST_INVALID_PARAMETER ;
        }/* end of if(1!= ( USB_VALIDATE_DESCRIPTOR_SEQUENCE(uDescriptorSequence).....)*/

    /*
     * Call USBHST_SetConfiguration() with configuration value and device
     * handle. if failed:
     * i.    Call OS_FREE() to free the configuration buffer.
     * ii.   Return result.
     */

    Result = usbHstSetConfiguration (uDeviceHandle,
                                     USB_DEF_ACTIVE_HUB_CONFIG_INDEX);

    /* Check the result */

    if (USBHST_SUCCESS != Result)
        {
        /* Debug Message */

        USB_HUB_ERR("failed to set configuration, result=%d \n",
                    Result, 2, 3, 4, 5, 6);

        /* Free the memory allocated */

        OS_FREE (pDeviceDescriptor);

        /* Free the memory allocated */
        OS_FREE (pConfigDescriptor);

        return Result;

        } /* End of (USBHST_SUCCESS !=Result ) */

    /*
     * Set the Hub Descriptor length as 11
     * For some quirky hubs(such as Belkin F5U707), no matter how long the Hub
     * Descriptor you required, they will just return 7 bytes. In the original
     * code, we ask for 6 bytes, and the quirky hub will return 7 bytes.Then,
     * the host will treat it to be a mistake, and stop to configure the hub
     * and return with 'Error'.That is why the hub can not be used.
     * From the USB3.0 spec, to be compatible with the software written for 1.0
     * compliant devices,we request for 11 bytes,but the returned data should be
     * at least 6 bytes,because the first 6 bytes contain important information
     * we need to configure the hub.
     */


    uLength = 11;

    /* Allocate memory for the hub descriptor */

    pHubDescriptor = OS_MALLOC (uLength);

    /* Check if memory allocation is successful */

    if (pHubDescriptor == NULL)
        {

        /* Debug Message */
        USB_HUB_ERR("memory allocate failed for hub descriptor \n",
                    1, 2, 3, 4, 5, 6);

        /* Free the memory allocated */
        OS_FREE (pDeviceDescriptor);

        /* Free the memory allocated */
        OS_FREE (pConfigDescriptor);

        return USBHST_INSUFFICIENT_MEMORY;

        }

    /* Initialize all the fields */

    OS_MEMSET(pHubDescriptor, 0, uLength);


    /*
     * Call HUB_GetHubDescriptor() for uDeviceHandle,
     * HUB_DESCRIPTOR and for 8 bytes. If call fails,
     * i.    Call OS_FREE() to free the configuration buffer.
     * ii.   Return USBHST_FAILURE.
     */
    Result = usbHubGetDescriptor (uDeviceHandle,         /* Device Handle */
                                 (UINT8 *)pHubDescriptor,/* Buffer to copy to */
                                 &uLength);              /* Size of the descriptor */

    /*
     * Check the result
     * if the return result not ok, or the length of data < 6 or > 8
     * we think this is a very bad thing
     */

    if ( (USBHST_SUCCESS != Result) || (uLength < 6) || (uLength > 11) )
        {
        /* Debug Message */
        USB_HUB_ERR("failed to get hub descriptor: " \
                    "result=%d, length=%d \n",
                    Result, uLength, 3, 4, 5, 6);

        /* Free the memory allocated */
        OS_FREE (pDeviceDescriptor);

        /* Free the memory allocated */
        OS_FREE (pConfigDescriptor);

        /* Free memory allocated for hub descriptor */

        OS_FREE(pHubDescriptor);

        return USBHST_FAILURE;

        } /* End of (USBHST_SUCCESS !=Result ) */


    /* Allocate memory for hub status */
    pStatusOfHub = OS_MALLOC(2);

    /* Check if memory allocation is successful */
    if (pStatusOfHub == NULL)
        {

        /* Debug Message */
        USB_HUB_ERR("memory allocate failed for hub status \n",
                    1, 2, 3, 4, 5, 6);

        /* Free the memory allocated */
        OS_FREE (pDeviceDescriptor);

        /* Free the memory allocated */
        OS_FREE (pConfigDescriptor);

        /* Free memory allocated for hub descriptor */
        OS_FREE (pHubDescriptor);

        return USBHST_INSUFFICIENT_MEMORY;

        }

    /*
     * Get the Power requirement of the hub
     * - Do a get status to the hub.
     * If the current status returns that the hub is
     * self powered, it can supply 500 mA per port
     * if it is bus powered, it can supply 100mA per port
     */

    Result = usbHstGetStatus(uDeviceHandle, /* Device Handle */
                             USBHST_RECIPIENT_DEVICE, /* uRecipient */
                             0,  /* Index */
                             (UCHAR *) pStatusOfHub);  /* Buffer */

    /* Check the result */

    if (USBHST_SUCCESS != Result)
        {
        /* Debug Message */
        USB_HUB_ERR("failed to get hub status: " \
                    "result=%d \n",
                    Result, 2, 3, 4, 5, 6);

        /* Free the memory allocated */
        OS_FREE (pDeviceDescriptor);

        /* Free the memory allocated */
        OS_FREE (pConfigDescriptor);

        /* Free memory allocated for hub descriptor */
        OS_FREE (pHubDescriptor);

        /* Free memory allocated for hub status */
        OS_FREE (pStatusOfHub);

        return Result;

        } /* End of (USBHST_SUCCESS !=Result ) */

    /* swap */

    *pStatusOfHub = OS_UINT16_LE_TO_CPU(*pStatusOfHub);

    /* check whether the hub is operating in bus powered or self powered mode */

    if (!(*pStatusOfHub & USB_HUB_POWER_CHECK))
        {

        /* hub operating in bus powered mode */

        /* check whether the parent hub is a root hub or not */

        if (pParentHub != NULL)
            {
            /* As per USB Specs -
             * "another bus powered hub cannot be connected to bus powered hub"
             *
             * check is the parent hub is bus-powered or self-powered.
             * This can be done by comparing the  parent_hub::uPowerPerPort with
             * values USB_HUB_PPORT_POWER_SELF_POWERED(250) or
             * USB_HUB_PPORT_POWER_BUS_POWERED (50)
             */

            if (pParentHub->uPowerPerPort == USB_HUB_PPORT_POWER_BUS_POWERED)
                {
                /* Free the memory allocated */
                OS_FREE (pDeviceDescriptor);

                /* Free the memory allocated */
                OS_FREE (pConfigDescriptor);

                /* Free memory allocated for hub descriptor */
                OS_FREE (pHubDescriptor);

                /* Free memory allocated for hub status */
                OS_FREE (pStatusOfHub);

                /*
                 * cannot configure the new hub cause the parent hub is bus powered
                 * and the new hub is also bus powered.
                 */
                return USBHST_FAILURE;
                }
            }
        }
    /* Parse the number of ports from the hub descriptor.*/

    uNumPorts = USB_HUB_DESC_bNbrPorts(pHubDescriptor);

    /*
     * Call OS_MALLOC() to allocate HUB_BYTE_GRANURALITY(number of ports + 1)
     * bytes of status change data buffer. If failed,
     * i.    Call OS_FREE() to free the configuration buffer.
     * ii.    Return USBHST_INSUFFICIENT_MEMORY.
     */

    uLength = (UINT32)USB_HUB_BYTE_GRANULARITY(uNumPorts + 1);

    /*
     * An internal hub with 6 ports (RMH) involved in Intel Communications
     * Chipset 89xx Series may report 32 bytes status data by Interrupt pipe.
     * To support this chip (usually working with Sandy_bridge BSP), USB stack
     * provides a workaround -- Setting 32 bytes transfer length for status
     * data. Otherwise, transfer of that pipe will report Babble error.
     * The RMH reports Vendor ID = 0x8087 and Product ID = 0x0020.
     */

    if (pParseDeviceDesc->idVendor == 0x8087 &&
        pParseDeviceDesc->idProduct == 0x0020)
        {
        uLength = 32;
        }

    /* allocate the memory */

    pStatusChangeData = OS_MALLOC(uLength);

    /* Check the result */

    if (NULL == pStatusChangeData)
        {
        /* Debug Message */

        USB_HUB_ERR("memory allocate failed for status change " \
                    "interrupt IN bit map data \n",
                    1, 2, 3, 4, 5, 6);

        /* Free the memory allocated */

        OS_FREE (pDeviceDescriptor);

       /* Free the memory allocated */

        OS_FREE (pConfigDescriptor);

        /* Free memory allocated for hub descriptor */

        OS_FREE (pHubDescriptor);

        /* Free memory allocated for hub status */

        OS_FREE (pStatusOfHub);

        return USBHST_INSUFFICIENT_MEMORY;

        } /* End of (USBHST_SUCCESS !=Result ) */

    /* allocate the memory */

    pStatusData = OS_MALLOC(uLength);

    /* Check the result */

    if (NULL == pStatusData)
        {
        /* Debug Message */

        USB_HUB_ERR("memory allocate failed for status " \
                    "interrupt IN bit map data \n",
                    1, 2, 3, 4, 5, 6);

        /* Free the memory allocated */

        OS_FREE (pDeviceDescriptor);

        OS_FREE (pConfigDescriptor);

        /* Free the memory allocated */

        OS_FREE (pStatusChangeData);

        /* Free memory allocated for hub descriptor */

        OS_FREE (pHubDescriptor);

        /* Free memory allocated for hub status */

        OS_FREE (pStatusOfHub);

        return USBHST_INSUFFICIENT_MEMORY;

        } /* End of (USBHST_SUCCESS !=Result ) */

    /* allocate the memory */

    pCurrentStatusData = OS_MALLOC(uLength);

    /* Check the result */

    if (NULL == pCurrentStatusData)
        {
        /* Debug Message */

        USB_HUB_ERR("memory allocate failed for current status " \
                    "interrupt IN bit map data \n",
                    1, 2, 3, 4, 5, 6);

        /* Free the memory allocated */

        OS_FREE (pDeviceDescriptor);

        OS_FREE (pConfigDescriptor);

        /* Free the memory allocated */

        OS_FREE (pStatusChangeData);

        /* Free the memory allocated */

        OS_FREE (pStatusData);

        /* Free memory allocated for hub descriptor */

        OS_FREE (pHubDescriptor);

        /* Free memory allocated for hub status */

        OS_FREE (pStatusOfHub);

        return USBHST_INSUFFICIENT_MEMORY;

        } /* End of (USBHST_SUCCESS !=Result ) */


    /* Clear the allocated buffer for status change data. */

    OS_MEMSET(pStatusChangeData, 0, uLength);

    /* Clear the allocated buffer for the status data. */

    OS_MEMSET(pStatusData, 0, uLength);

    /* Clear the allocated buffer for the status data. */

    OS_MEMSET(pCurrentStatusData, 0, uLength);

    /*
     * Call OS_MALLOC() to allocate memory for HUB_INFO as
     * sizeof(USB_HUB_INFO)+ ((number of ports-1)*sizeof(PHUB_PORT_INFO)) bytes.
     * If Failed,
     * i.    Call OS_FREE() to free the configuration buffer.
     * ii.    Call OS_FREE() to free the status change buffer.
     * iii.    Return USBHST_INSUFFICIENT_MEMORY.
     */

    /*
     * This is an over allocation of the HUB_INFO structure.
     * this allows us to access pPortList[n] count for all the ports.
     * [<------------->y][y][y][y]...
     * [<------------->y] is the normal size of HUB_INFO with pPortList[1]
     * If there are two ports then it should be pPortList[2]. or:
     * [<------------->yy]
     * and so on.. we shud be still be able to access pPortList[uPortIndex].
     * hence we allocate
     * [<------------->y] bytes + (number of ports-1) * sizeof(y)
     * thus for 5 ports we get automatically get
     * [<------------->y]followed by[y][y][y][y] in one single memory chunk.
     */

    pNewHub = OS_MALLOC(sizeof(*pNewHub)+
                       ((uNumPorts-1U)* sizeof(pUSB_HUB_PORT_INFO)));

    /* Check the result */

    if (NULL == pNewHub)
        {
        USB_HUB_ERR("memory allocate failed for new hub information \n",
                    1, 2, 3, 4, 5, 6);

        /* Free the memory allocated */

        OS_FREE (pDeviceDescriptor);

        /* Free the memory allocated */

        OS_FREE (pConfigDescriptor);

        /* Free the memory allocated */

        OS_FREE (pStatusChangeData);

        /* Free the memory allocated */

        OS_FREE (pStatusData);

        /* Free the memory allocated */

        OS_FREE (pCurrentStatusData);

        /* Free memory allocated for hub descriptor */

        OS_FREE (pHubDescriptor);

        /* Free memory allocated for hub status */

        OS_FREE (pStatusOfHub);

        return USBHST_INSUFFICIENT_MEMORY;

        } /* End of (USBHST_SUCCESS !=Result ) */

    /* Clear the allocated structure.*/

    OS_MEMSET(pNewHub, 0,
              sizeof(*pNewHub)+((uNumPorts-1U)* sizeof(pUSB_HUB_PORT_INFO)));

    /* Here we find the TT Organization  */

    /* this switch checks the bDeviceProtocol field in device descriptor */
    switch(pParseDeviceDesc->bDeviceProtocol)
        {
        case 0:
            /* this hub does not support TT */

            pNewHub->uHubTTInfo = 0;
            break;

        case 1:
            /* this is a single TT Hub */
            if(pParseInterfaceDesc->bInterfaceProtocol == 0)
                {
                pNewHub->uHubTTInfo = 1;
                }/* end of if(pParseInterfaceDesc ....*/

            /* invalid parameters in descriptor */
            else
                {
                /* Debug Message */
                USB_HUB_ERR("invalid parameter in descriptor \n",
                            1, 2, 3, 4, 5, 6);

                /* Free the memory allocated */

                OS_FREE (pDeviceDescriptor);

                /* Free the memory allocated */

                OS_FREE (pConfigDescriptor);

                /* Free the memory allocated */

                OS_FREE (pStatusChangeData);

                /* Free the memory allocated */

                OS_FREE (pStatusData);

                /* Free the memory allocated */

                OS_FREE (pCurrentStatusData);

                /* Free the memory allocated */

                OS_FREE (pNewHub);

                /* Free memory allocated for hub descriptor */

                OS_FREE (pHubDescriptor);

                /* Free memory allocated for hub status */

                OS_FREE (pStatusOfHub);

                return USBHST_INVALID_PARAMETER ;
                }/* end of else  */
            break;

         case 2:
            /* check if we have recieved default i/f and alternate i/f descriptor*/
            if (uDescriptorSequence == 0x24210)
                {
                /* this is a multiple TT hub */
                if(pParseAlternateInterfaceDesc->bInterfaceProtocol ==
                   HUB_PROTOCOL_MULTIPLE_TT)
                    {

                    pNewHub->uHubTTInfo = 2;

                    }  /*  if != 2 then .....*/

                }/* end of if(uDescriptorSequence == 0x24210) */

            /* invalid descriptor */
            else
                {
                /* Debug Message */
                USB_HUB_ERR("invalid parameter in descriptor \n",
                            1, 2, 3, 4, 5, 6);

                /* Free the memory allocated */

                OS_FREE (pDeviceDescriptor);

                /* Free the memory allocated */

                OS_FREE (pConfigDescriptor);

                /* Free the memory allocated */

                OS_FREE (pStatusChangeData);

                /* Free the memory allocated */

                OS_FREE (pStatusData);

                /* Free the memory allocated */

                OS_FREE (pCurrentStatusData);

                /* Free the memory allocated */

                OS_FREE (pNewHub);

                /* Free memory allocated for hub descriptor */

                OS_FREE (pHubDescriptor);

                /* Free memory allocated for hub status */

                OS_FREE (pStatusOfHub);

                return USBHST_INVALID_PARAMETER ;
                } /* end of else */
            break ;
         case 3:
            /* this hub does not support TT */

            pNewHub->uHubTTInfo = 0;
            break;
         default:
                /* Debug Message */
                USB_HUB_ERR("invalid parameter in descriptor \n",
                            1, 2, 3, 4, 5, 6);

                /* Free the memory allocated */

                OS_FREE (pDeviceDescriptor);

                /* Free the memory allocated */

                OS_FREE (pConfigDescriptor);

                /* Free the memory allocated */

                OS_FREE (pStatusChangeData);

                /* Free the memory allocated */

                OS_FREE (pStatusData);

                /* Free the memory allocated */

                OS_FREE (pCurrentStatusData);

                /* Free the memory allocated */

                OS_FREE (pNewHub);

                /* Free memory allocated for hub descriptor */

                OS_FREE (pHubDescriptor);

                /* Free memory allocated for hub status */

                OS_FREE (pStatusOfHub);

            return USBHST_INVALID_PARAMETER;
            }/* end of switch(pParseDeviceDesc.......)*/

    /* check if Hub is a Multiple TT hub */
    if (pNewHub->uHubTTInfo == 2)
        {
        Result = usbHstSetInterface (uDeviceHandle,
                                     0, /* interface number */
                                     1); /* alternate interface Setting*/

        /* Check the result */
        if (USBHST_SUCCESS != Result)
            {

            /* Debug Message */
            USB_HUB_ERR("failed to set interface: " \
                        "result=%d \n",
                        Result, 2, 3, 4, 5, 6);

            /* Free the memory allocated */

            OS_FREE (pDeviceDescriptor);

            /* Free the memory allocated */

            OS_FREE (pConfigDescriptor);

            /* Free the memory allocated */

            OS_FREE (pStatusChangeData);

            /* Free the memory allocated */

            OS_FREE (pStatusData);

            /* Free the memory allocated */

            OS_FREE (pCurrentStatusData);

            /* Free the memory allocated */

            OS_FREE (pNewHub);

            /* Free memory allocated for hub descriptor */

            OS_FREE (pHubDescriptor);

            /* Free memory allocated for hub status */

            OS_FREE (pStatusOfHub);

            return Result;

            } /* End of (USBHST_SUCCESS !=Result ) */

        }/*end of if(pNewHub->uHubTTInfo ...*/

    /* Allocated hub status buffer to avoid use transfer buffer on stack */

    pNewHub->pHubStatus = OS_MALLOC(sizeof(USB_HUB_STATUS));

    if (!pNewHub->pHubStatus)
        {
        /* Debug Message */
        USB_HUB_ERR("memory allocate failed for new hub status \n",
                    1, 2, 3, 4, 5, 6);

        /* Free the memory allocated */

        OS_FREE (pDeviceDescriptor);

        /* Free the memory allocated */

        OS_FREE (pConfigDescriptor);

        /* Free the memory allocated */

        OS_FREE (pStatusChangeData);

        /* Free the memory allocated */

        OS_FREE (pStatusData);

        /* Free the memory allocated */

        OS_FREE (pCurrentStatusData);

        /* Free the memory allocated */

        OS_FREE (pNewHub);

        /* Free memory allocated for hub descriptor */

        OS_FREE (pHubDescriptor);

        /* Free memory allocated for hub status */

        OS_FREE (pStatusOfHub);

        return USBHST_INSUFFICIENT_MEMORY;
        }

    /* Clear the hub status buffer */

    OS_MEMSET(pNewHub->pHubStatus, 0x00, sizeof(USB_HUB_STATUS));

    /*
     * Populate the HUB_INFO structure, HUB_INFO::HubDescriptor structure by
     * taking the necessary values including parsing the hub descriptor and
     * the configuration descriptor.
     */

    /* Create the status mutex */
    pNewHub->hubMutex = semMCreate (SEM_Q_PRIORITY |
                                SEM_INVERSION_SAFE | SEM_DELETE_SAFE);

    if (pNewHub->hubMutex == NULL)
        {
        /* Debug Message */
        USB_HUB_ERR("mutex create failed for new hub status \n",
                    1, 2, 3, 4, 5, 6);

        /* Free the memory allocated */

        OS_FREE (pDeviceDescriptor);

        /* Free the memory allocated */

        OS_FREE (pConfigDescriptor);

        /* Free the memory allocated */

        OS_FREE (pStatusChangeData);

        /* Free the memory allocated */

        OS_FREE (pStatusData);

        /* Free the memory allocated */

        OS_FREE (pCurrentStatusData);

        /* Free the memory allocated */

        OS_FREE (pNewHub->pHubStatus);

        /* Free the memory allocated */

        OS_FREE (pNewHub);

        /* Free memory allocated for hub descriptor */

        OS_FREE (pHubDescriptor);

        /* Free memory allocated for hub status */

        OS_FREE (pStatusOfHub);

        return USBHST_INSUFFICIENT_RESOURCE;
        }

    /* the device handle */

    pNewHub->uDeviceHandle = uDeviceHandle;

    /* The pointer to the status change bit map */

    pNewHub->pCopiedStatus = pStatusData;

    /* The pointer to the status change bit map */

    pNewHub->pCurrentStatus = pCurrentStatusData;

    /* status change is default set to false */

    pNewHub->bCurrentStatusChanged = FALSE;

    /* store the parent bus */

    pNewHub->pBus = pBus;

    /*
     * how much power can this device provide to each port?
     * if this is a self powered config, then we can support 500 mA.
     * if this is a bus powered, we can support a max of 100 mA.
     */
    pNewHub->uPowerPerPort =(UINT8)(
            (((UINT8)(*pStatusOfHub&0xFF))&(0x01))? /* Self powered?*/
            USB_HUB_PPORT_POWER_SELF_POWERED : USB_HUB_PPORT_POWER_BUS_POWERED);
                                          /* multiply by 2 to get mA */
    /* The interrupt endpoint for the Hub */
    pNewHub->uInterruptEndpointNumber = pParseEndpointDesc->bEndpointAddress;

    /*
     * check if the parent of the given hub is NULL or not
     * NULL parent implies root hub. set the tier as 1.
     */
    if (NULL == pParentHub)
        {
        /* root hub, the tier is 1 */

        pNewHub->uCurrentTier = 1;
        }
    else
        {
        /* The current Tier is 1 more than the parent hub */

        pNewHub->uCurrentTier = (UINT8)(pParentHub->uCurrentTier + 1);

        }/* End of if (NULL == pParentHub) */

    /* Set the Hub state as default no state */
    pNewHub->StateOfHub = USB_HUB_NO_STATE;

    /* Save the status data length */
    pNewHub->uTotalStatusLength = uLength;

    /* Set the actual status length to be 0 */
    pNewHub->uActualStatusLength = 0;

    /* Init the Hub descriptor params */

    /* number of ports */

    pNewHub->HubDescriptor.bNbrPorts =
                                USB_HUB_DESC_bNbrPorts(pHubDescriptor);
    /* Hub characteristics */

    pNewHub->HubDescriptor.wHubCharacteristics =
                                (UINT16)USB_HUB_DESC_wHubCharacteristics(pHubDescriptor);

    /* Time for the power to stabilize on power on */

    pNewHub->HubDescriptor.bPwrOn2PwrGood =
                                USB_HUB_DESC_bPwrOn2PwrGood(pHubDescriptor);

    /* hub electronics power requirements */

    pNewHub->HubDescriptor.bHubContrCurrent =
                                USB_HUB_DESC_bHubContrCurrent(pHubDescriptor);

    /* The 1st external hub gets HubDepth of 0, root hub depth gets 0xFF */

    usbHubSetHubDepth(uDeviceHandle, (UINT16)(pNewHub->uCurrentTier - 2));

    /* Update Hub TT information to let the host controller aware */

    usbHstHubTTUpdate(uDeviceHandle,
                  pNewHub->HubDescriptor.bNbrPorts,
                  (pNewHub->uHubTTInfo == 2) ? 1 : 0,
                  USB_HUB_TT_TT(pNewHub->HubDescriptor.wHubCharacteristics));

    /* Free the memory allocated for hub descriptor */

    OS_FREE(pHubDescriptor);

    /* Debug Message */

    USB_HUB_DBG("usbHubConfigure - new hub 0x%X information:\n"
                "uPowerPerPort=%d, uInterruptEpNumber=%d, "
                "uCurrentTier=%d, StateOfHub=%d\n",
                pNewHub->uDeviceHandle,
                pNewHub->uPowerPerPort,
                pNewHub->uInterruptEndpointNumber,
                pNewHub->uCurrentTier,
                pNewHub->StateOfHub, 6);

    USB_HUB_DBG("usbHubConfigure - new hub descriptor information:\n"
                "wHubCharacteristics=%d,"
                "bPwrOn2PwrGood=%d, bHubContrCurrent=%d\n",
                pNewHub->HubDescriptor.bNbrPorts,
                pNewHub->HubDescriptor.wHubCharacteristics,
                pNewHub->HubDescriptor.bPwrOn2PwrGood,
                pNewHub->HubDescriptor.bHubContrCurrent, 5, 6);

    /* POWER ON PORTS (BEGIN) */

    /* for all ports that are enabled,
     * i. Call HUB_PowerOnPort() with pHub and port count to re enable the
     *    port. If the call fails, then free memory and return failure result.
     */
    for (uPortCount = 0;
         uPortCount < pNewHub->HubDescriptor.bNbrPorts;
         uPortCount++)
        {
        /* power on the port */
        Result = usbHubPowerOnPort(pNewHub, uPortCount);
        if (USBHST_SUCCESS != Result)
            {
            /* Debug Message */
            USB_HUB_ERR("power on port %d failed \n",
                        uPortCount, 2, 3, 4, 5, 6);

            /* Call OS_FREE() to free the configuration buffer.*/

            OS_FREE (pDeviceDescriptor);

            OS_FREE (pConfigDescriptor);

            /* Free the memory allocated */

            OS_FREE (pStatusChangeData);

            /* Free the memory allocated */

            OS_FREE (pStatusData);

            /* Free the memory allocated */

            OS_FREE (pCurrentStatusData);

            /* Free the memory allocated */

            OS_FREE (pNewHub->pHubStatus);

            /* Free the memory allocated */

            semDelete(pNewHub->hubMutex);

            /* Free the memory allocated */

            OS_FREE(pNewHub);

            /* Reset the return value */

            *pHub = NULL;

            /* return failure result */

            return Result;

            }/* End of if (USBHST_SUCCESS !=Result) */
        else
            {
            /* Debug Message */

            USB_HUB_DBG("usbHubPowerOnPort - waiting %d ms for power on hub port\n",
                        (pNewHub->HubDescriptor.bPwrOn2PwrGood * 2), 2, 3, 4, 5, 6);
            /*
             * There is a minimal time for the system to stabilize on
             * setting a port power. This time is provided in the hub
             * descriptor. we have to wait for this time period.
             */

            /*
             * Note on bPwrOn2PwrGood:
             * Time (in 2 ms intervals) from the time the power-on
             * sequence begins on a port until power is good on that
             * port. The USB System Software uses this value to
             * determine how long to wait before accessing a
             * powered-on port.
             */

            OSS_THREAD_SLEEP ((UINT32)(pNewHub->HubDescriptor.bPwrOn2PwrGood * 2)) ;
            } /* End of if (USBHST_SUCCESS==Result) */
        } /* End of for (uPortCount.... */

        {
        USB_TRANSFER_SETUP_INFO         xferSetup;

        /* Set up the trasnfer requirment */

        OS_MEMSET(&xferSetup, 0, sizeof(USB_TRANSFER_SETUP_INFO));

        xferSetup.uMaxTransferSize = max(32, uLength);
        xferSetup.uMaxNumReqests = 1;
        xferSetup.uFlags = 0;

        if ((Result = usbHstPipePrepare(uDeviceHandle,
                        pNewHub->uInterruptEndpointNumber,
                        &xferSetup)) != USBHST_SUCCESS)
            {
            USB_HUB_ERR("usbHstPipePrepare status change ep 0x%02X failed \n",
                        pNewHub->uInterruptEndpointNumber, 2, 3, 4, 5, 6);

            /* Call OS_FREE() to free the configuration buffer.*/

            OS_FREE (pDeviceDescriptor);

            OS_FREE(pConfigDescriptor);

            /* Free the memory allocated */

            OS_FREE (pStatusChangeData);

            /* Free the memory allocated */

            OS_FREE (pStatusData);

            /* Free the memory allocated */

            OS_FREE (pCurrentStatusData);

            /* Free the memory allocated */

            OS_FREE (pNewHub->pHubStatus);

            /* Free the memory allocated */

            semDelete(pNewHub->hubMutex);

            /* Free the memory allocated */

            OS_FREE(pNewHub);

            /* Reset the return value */

            *pHub = NULL;

            /* return failure result */
            return Result;

            }
        }

    /* POWER ON PORTS (END) */

    /*
     * Call the USBHST_FILL_INT_URB() to populate the
     * HUB_INFO::StatusChangeURB.
     * Set the HUB_INFO::StatusChangeURB::pfCallBack as
     * HUB_InterruptRequestCallback function.
     */

    USBHST_FILL_INTERRUPT_URB(
                          (&(pNewHub->StatusChangeURB)),      /* pointer to URB     */
                          uDeviceHandle,                      /* Device Handle      */
                          pNewHub->uInterruptEndpointNumber,  /* EP no: */
                          pStatusChangeData,                  /* transfer buffer    */
                          uLength,                            /* uTransferBufferLen */
                          USBHST_SHORT_TRANSFER_OK,           /* TransferFlags      */
                          usbHubInterruptRequestCallback,     /* pfCallback       */
                          pNewHub,                            /* pContext           */
                          USBHST_FAILURE                      /* uStatus            */
                          );

    /* Call OS_FREE() to free the  buffer.*/

    OS_FREE (pDeviceDescriptor);

    OS_FREE(pConfigDescriptor);

    /* Free memory allocated for hub status */

    OS_FREE (pStatusOfHub);

    /* Set pHub as HUB_INFO structure.*/

    *pHub = pNewHub;

    /*
     * Set the URB Submitted flag as true - This is being done here coz the
     * callback can be called when the SubmitURB is called.
     */

    if (pNewHub->hubMutex) /* No need to check semTake return value */
        (void)semTake(pNewHub->hubMutex, WAIT_FOREVER);

    pNewHub->bURBSubmitted = TRUE;

    if (pNewHub->hubMutex)
        semGive(pNewHub->hubMutex);

    /* Clear the error counter */

    pNewHub->uNumErrors = 0;

    /*
     * Call USBHSTSubmitURB() to submit the URB. If this fails
     * i.    Call OS_FREE() to free the status change buffer.
     * ii.   Return USBHST_INSUFFICIENT_MEMORY.
     */
    Result = usbHstURBSubmit(&(pNewHub->StatusChangeURB));

    /* Check the result */
    if (USBHST_SUCCESS != Result)
        {
        /* Debug Message */
        USB_HUB_ERR("failed to submit interrupt urb, result=%d \n",
                    Result, 2, 3, 4, 5, 6);

        /* Free the memory allocated */

        OS_FREE (pStatusChangeData);

        /* Free the memory allocated */

        OS_FREE (pStatusData);

        /* Free the memory allocated */

        OS_FREE (pCurrentStatusData);

        /* Free the memory allcoated */

        OS_FREE (pNewHub->pHubStatus);

        /* Take the mutex before delte it */
        /* No need to check semTake return value */
        (void)semTake(pNewHub->hubMutex, WAIT_FOREVER);

        /* Free the memory allcoated */

        semDelete (pNewHub->hubMutex);

        /* Free the memory allocated */

        OS_FREE(pNewHub);

        /* Reset the return value */

        *pHub = NULL;

        return USBHST_FAILURE;

        }/* End of if ( USBHST_SUCCESS != Result) */

    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
        USB_HUB_WV_DEVICE,
        "Exiting usbHubconfigure() Function",
        USB_HUB_WV_FILTER);

    return USBHST_SUCCESS;
    }/* End of HUB_ConfigureHub() */

/***************************************************************************
*
* usbHubHandleDeviceConnection - Handles the device connection.
*
* This routine handles the device connection.
*
* RETURNS: USBHST_SUCCESS, USBHST_INVALID_PARAMETERS If there are errors in
* parameters passed.
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubHandleDeviceConnection
    (
    pUSB_HUB_INFO pHub,
    UINT8         uPortIndex
    )
    {
    /* Storage for the port information */

    pUSB_HUB_PORT_INFO pPort = NULL;

    /* Storage for the result of the requests */

    USBHST_STATUS Result;

    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
        USB_HUB_WV_DEVICE,
        "Entering usbHubHandleDeviceConnection() Function",
        USB_HUB_WV_FILTER);

    /* If pHub or pHub->pBus is NULL return USBHST_INVALID_PARAMETER */

    if ((NULL == pHub) || (NULL == pHub->pBus))
        {
        /* Debug Message */
        USB_HUB_ERR("invalid parameters \n",
                    1, 2, 3, 4, 5, 6);


        return USBHST_INVALID_PARAMETER;

        }/* End of if (NULL ==pHub) */

    /*
     * If the uPortIndex greater than pHub::HubDescriptor::bNbrPorts then
     * return USBHST_INVALID_PARAMETER.
     * Note: the actual port number is the port index + 1
     */
    if (uPortIndex >= pHub->HubDescriptor.bNbrPorts)
        {
        /* Debug Message */
        USB_HUB_ERR("port index %d >= max %d \n",
                    uPortIndex,
                    pHub->HubDescriptor.bNbrPorts,
                    3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of if (uPortIndex >= pHub->HubDescriptor.bNbrPorts) */

    /*
     * Retrieve the HUB_PORT_INFO structure from pHub::pPortList for
     * the uPortIndex.
     */

    pPort = pHub->pPortList[uPortIndex];

    if (NULL == pPort)
        {
        USB_HUB_ERR("pPort is null\n", 1, 2, 3, 4, 5, 6);
        return USBHST_INVALID_PARAMETER;
        }

    /* Update the HUB_PORT_INFO::StateOfPort as HUB_RESET_PENDING.*/

    pPort->StateOfPort = USB_HUB_RESET_PENDING;

    /*
     * If pHub:pBus::bDeviceBeingConfigured is TRUE, then return
     *  USBHST_SUCCESS.
     */

    if (TRUE == pHub->pBus->bDeviceBeingConfigured)
        {
        /*
         * The pHub->pBus->bDeviceBeingConfigured is set to FALSE
         * in the reset URB callback routine if there is any failure;
         * Otherwise, if the reset URB is successful, then the
         * pHub->pBus->bDeviceBeingConfigured is only set to FALSE
         * in the reset change handler.
         *
         * This creates one extreme case, where the reset URB was
         * successful, so the URB callback doesn't set the
         * pHub->pBus->bDeviceBeingConfigured to FALSE, but the
         * device doesn't report a reset change thereafter due to
         * any device specific reasons; In this case, the reset change
         * handler can not be called, pHub->pBus->bDeviceBeingConfigured
         * can not be reset to FALSE; Finally, a dead loop condition
         * happens: when new device is attached, it wants to call the
         * reset routine below, but pHub->pBus->bDeviceBeingConfigured
         * is still TRUE, and there is no other code to reset this
         * flag to FALSE, thus the code causes a dead loop!
         *
         * To cope with this extreme case, the following code is to
         * do a counting for this case, we allow a maxium of
         * USB_HUB_MAX_RESET_CHECK_COUNT retries to check if this
         * pHub->pBus->bDeviceBeingConfigured are set to FALSE or not;
         * When this counting is exceeded, we force reset the flag to
         * FALSE!
         */

        /* Counting the retries for this check */

        pHub->pBus->uResetCheckCount++;

        USB_HUB_DBG("usbHubHandleDeviceConnection - "
                    "hub 0x%X port %d on bus %p uResetCheckCount %d\n",
                    pHub->uDeviceHandle,
                    uPortIndex,
                    pHub->pBus,
                    pHub->pBus->uResetCheckCount, 5, 6);

        if (pHub->pBus->uResetCheckCount > USB_HUB_MAX_RESET_CHECK_COUNT)
            {
            /* Debug Message */

            USB_HUB_ERR("Port %d reset failed, exceed max reset count %d\n",
                        uPortIndex, pHub->pBus->uResetCheckCount, 3, 4, 5, 6);

            pHub->pBus->bDeviceBeingConfigured = FALSE;
            }
        else
            {
            /*
             * If there is port being reset, we must wait until the
             * reset change happen.
             */

            OS_DELAY_MS(10);

            /*
             * When we are working in Single BusM task mode, we will
             * need "event" to trigger the Single BusM task to handle
             * hub related operations. The "pending reset" operation
             * is "delayed" until ongoing reset complete, but since we
             * have no hardware to report this event, we have to add
             * this hub to the event list explictly. One would ask why
             * in Multiple BusM mode it was working. The answer is that
             * in that mode, the BusM tasks were actually "polling",
             * thus they would finally poll to know "reset is pending".
             */

#ifndef USB_HUB_CREATE_BUSM_TASK_FOR_EACH_BUS
            usbHubAddEvent(pHub);
#endif
            return USBHST_SUCCESS;
            }

        }/* End of if (TRUE... */

    /*
     * Call HUB_ResetPort() with pHub, uPortIndex. If the call fails,
     * return the result.
     */

    Result = usbHubResetPort(pHub, uPortIndex);
    if (USBHST_SUCCESS != Result)
        {
        /* Debug Message */

        USB_HUB_ERR("Port %d reset failed, result=%d \n",
                    uPortIndex, Result, 3, 4, 5, 6);

        return Result;

        } /* End of if (USBHST_SUCCESS... */

    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
        USB_HUB_WV_DEVICE,
        "Exiting usbHubHandleDeviceConnection() Function",
        USB_HUB_WV_FILTER);

    /* Return USBHST_SUCCESS.*/
    return USBHST_SUCCESS;

    } /* End of HUB_HandleDeviceConnection() */


/***************************************************************************
*
* usbHubRetryDeviceConfiguration - called to retry a device configuration.
*
* This routine is called to retry a device configuration. If the configuration
* retries have expired, then the port is disabled.
*
* RETURNS: USBHST_SUCCESS, USBHST_FAILURE, USBHST_INVALID_PARAMETERS if
* parameters are wrong.
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubRetryDeviceConfiguration
    (
    pUSB_HUB_INFO pHub,
    UINT8         uPortIndex
    )
    {
    /* Storage for the port information */

    pUSB_HUB_PORT_INFO pPort = NULL;

    /* Storage for the result of requests */

    USBHST_STATUS Result;

    /* Storage for the retries */

    UINT8 uNumOfRetries = 0;

    /* If pHub or pHub->pBus is NULL return USBHST_INVALID_PARAMETER */

    if ((NULL == pHub) || (NULL == pHub->pBus))
        {
        /* Debug Message */
        USB_HUB_ERR("invalid parameters \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of if (NULL ==pHub) */

    /*
     * If the uPortIndex greater than pHub::HubDescriptor::bNbrPorts then
     * return USBHST_INVALID_PARAMETER.
     * Note: the actual port number is the port index + 1
     */

    if (uPortIndex >= pHub->HubDescriptor.bNbrPorts)
        {
        /* Debug Message */
        USB_HUB_ERR("port index %d >= max %d \n",
                    uPortIndex,
                    pHub->HubDescriptor.bNbrPorts,
                    3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of if (uPortIndex >= pHub->HubDescriptor.bNbrPorts) */

    /*
     * Retrieve the HUB_PORT_INFO structure from pHub. If failed, return
     * USBHST_FAILURE.
     */

    pPort = pHub->pPortList[uPortIndex];
    if (NULL == pPort)
        {
        /* Debug Message */

        USB_HUB_WARN("usbHubRetryDeviceConfiguration - "
                     "pPort information for hub 0x%X port %d is NULL\n",
                     pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

        /* Return the success as there is nothing to be done. */
        return USBHST_SUCCESS;

        }/* End of if (NULL == pPort) */

    /* Store pHub::pBus::uNumberOfConfigRetries */
    pHub->pBus->uNumberOfConfigRetries--;
    uNumOfRetries = pHub->pBus->uNumberOfConfigRetries;

    /*
     * If pHub::pBus::uNumberOfConfigRetries is zero then:
     * i.    Update HUB_PORT_INFO::StateOfPort as MARKED_FOR_DELETION.
     * ii.   Return  USBHST_SUCCESS
     */

    if (0 == pHub->pBus->uNumberOfConfigRetries)
        {
        /* Debug Message */

        USB_HUB_WARN("usbHubRetryDeviceConfiguration - "
                     "hub 0x%X port %d exceed the max retry counts \n",
                     pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);


        /* Mark the port for deletion */
        USB_MARK_FOR_DELETE_PORT(pHub, pPort);

        return USBHST_SUCCESS;
        }

    /*
     * Call the HUB_HandleDeviceConnection() to retry the handling of the
     * connection to the port. If the call fails return the result.
     */
    Result = usbHubHandleDeviceConnection(pHub, uPortIndex);
    if (USBHST_SUCCESS != Result)
        {
        /* Debug Message */
        USB_HUB_ERR("failed to handle device connection "
                    "for hub 0x%X port %d, result=%d\n",
                    pHub->uDeviceHandle, uPortIndex, Result, 4, 5, 6);

        return Result;

        }/* End of USBHST_SUCCESS !=Result) */

    /* Store back the pHub::pBus::uNumberOfConfigRetries */

    pHub->pBus->uNumberOfConfigRetries = uNumOfRetries;

    /* Return USBHST_SUCCESS.*/
    return USBHST_SUCCESS;

    } /* End of HUB_RetryDeviceConfiguration () */



/***************************************************************************
*
* usbHubPowerOnPort - re-enable a port that has caused a serious error.
*
* This routine re-enables a port that has caused a serious error.
*
* RETURNS: USBHST_SUCCESS, USBHST_FAILURE, USBHST_INVALID_PARAMETERS if
* parameters are wrong.
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubPowerOnPort
    (
    pUSB_HUB_INFO pHub,
    UINT8         uPortIndex
    )
    {
    /* Storage for the port information */

    pUSB_HUB_PORT_INFO pPort=NULL;

    /* Storage for the result of requests */

    USBHST_STATUS Result;

    /* If pHub is NULL return USBHST_INVALID_PARAMETER*/

    if (NULL == pHub)
        {
        /* Debug Message */
        USB_HUB_ERR("invalid parameter \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of if (NULL ==pHub) */

    /*
     * If the uPortIndex greater than pHub::HubDescriptor::bNbrPorts then
     * return USBHST_INVALID_PARAMETER.
     * Note: the actual port number is the port index + 1
     */

    if (uPortIndex >= pHub->HubDescriptor.bNbrPorts)
        {
        /* Debug Message */
        USB_HUB_ERR("port index %d >= max %d \n",
                    uPortIndex,
                    pHub->HubDescriptor.bNbrPorts,
                    3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of if (uPortIndex >= pHub->HubDescriptor.bNbrPorts) */

    /*
     * Retrieve the HUB_PORT_INFO structure from pHub. If failed, return
     * USBHST_FAILURE.
     */

    pPort = pHub->pPortList[uPortIndex];

    /* Check if port is NULL */

    if (NULL != pPort)
        {
        /*
         * if the port is not null then we call remove device to remove the
         * device
         */
        /*
         * If HUB_PORT_INFO::pHub is NULL then
         * i.    Call g_USBHSTFunctionList::USBST_RemoveDevice() with
         *       HUB_PORT_INFO::uDeviceHandle
         * ii.    Set pHub:pPortList[uPortIndex] as NULL
         * iii.  Call OS_FREE() to free memory occupied by hub_PORT_INFO.
         * 9/5/2k3:NM: Changed here to centralise the effect
         */
        usbHubRemoveDevice(pHub,uPortIndex);

        }/* End of if (NULL != pPort ) */

    /*
     * Call HUB_SET_PORT_FEATURE() to set the PORT_POWER feature to power on
     * the port for pHub and uPortIndex. If request success then,
     * i.    Store the Get the current frame number as old time.
     * ii.   Loop until HUB_TIME_DIFF() between the  old time and current time
     *       obtained by USBHST_GetFrameNumber() is greater than
     *       pHub::HubDescriptor::bPwrOn2PwrGood.
     * Note: The actual port number is 1 more than the port index
     */
    Result = USB_HUB_SET_PORT_FEATURE(pHub, uPortIndex, USB_PORT_POWER);

    /* Return result of the SubmitFeature.*/
    return Result;

    } /* End of HUB_PowerOnPort() */


/***************************************************************************
*
* usbHubResetPort - reset a Port.
*
* This function is used to reset a Port. This function ensures that the reset is
* issued to only one device per bus.
*
* RETURNS: USBHST_SUCCESS, USBHST_FAILURE, USBHST_INVALID_PARAMETERS if
* parameters are wrong.
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubResetPort
    (
    pUSB_HUB_INFO pHub,
    UINT8         uPortIndex
    )
    {
    /* Storage for the port information */

    pUSB_HUB_PORT_INFO pPort = NULL;

    /* Storage for the result of requests */

    USBHST_STATUS Result;

    /* The urb to be used for the reset */

    pUSBHST_URB pURB = NULL;

    /* The setup packet being used */

    pUSBHST_SETUP_PACKET pSetupPacket = NULL;

    /* The Feature Selector for SetPortFeature() */

    UINT16 uFeatureSelector = 0;

    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
        USB_HUB_WV_DEVICE,
        "Entering usbHubResetPort() Function",
        USB_HUB_WV_FILTER);

    /* If pHub or pHub->pBus is NULL return USBHST_INVALID_PARAMETER */

    if ((NULL == pHub) || (NULL == pHub->pBus))
        {
        /* Debug Message */
        USB_HUB_ERR("invalid parameters \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of if (NULL ==pHub) */

    /*
     * If the uPortIndex greater than pHub::HubDescriptor::bNbrPorts then
     * return USBHST_INVALID_PARAMETER.
     * Note: the actual port number is the port index + 1
     */

    if (uPortIndex >= pHub->HubDescriptor.bNbrPorts)
        {
        /* Debug Message */
        USB_HUB_ERR("port index %d >= max %d \n",
                    uPortIndex,
                    pHub->HubDescriptor.bNbrPorts,
                    3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of if (uPortIndex >= pHub->HubDescriptor.bNbrPorts) */

    /*
     * Retrieve the HUB_PORT_INFO structure from pHub. If failed, return
     * USBHST_FAILURE.
     */

    pPort = pHub->pPortList[uPortIndex];
    if (NULL == pPort)
        {
        /* Debug Message */
        USB_HUB_ERR("port %d information is null \n",
                    uPortIndex, 2, 3, 4, 5, 6);

        return USBHST_FAILURE;

        }/* End of if (NULL == pPort) */

    /*
     * If pHub::pBus::bDeviceBeingConfigured  is TRUE then return
     * USBHST_FAILURE.
     */

    if (TRUE == pHub->pBus->bDeviceBeingConfigured)
        {
        /* Debug Message */

        USB_HUB_ERR("other device being configured \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_FAILURE;

        }/* End of if (TRUE == ... */

    /* Debug Message */

    USB_HUB_DBG("usbHubResetPort - reset hub 0x%X port %d start\n",
                pPort->pParentHub->uDeviceHandle,
                pPort->uPortIndex, 3, 4, 5, 6);

    /* Set HUB_PORT_INFO::StateOfPort as HUB_RESET_COMPLETION_PENDING */

    pPort->StateOfPort = USB_HUB_RESET_COMPLETION_PENDING;

    /* Get the pointer to port reset URB for this bus */

    pURB = &(pHub->pBus->deviceResetURB);

    /* Clear the allocated structure.*/

    OS_MEMSET(pURB, 0, sizeof(USBHST_URB));

    /* Get the pointer to port reset setup packet for this bus */

    pSetupPacket = &(pHub->pBus->deviceResetSetup);

    /* Clear the allocated buffer.*/

    OS_MEMSET(pSetupPacket, 0, sizeof(USBHST_SETUP_PACKET));

    /* Set the pHub::pBus::uResetCheckCount as 0 */

    pHub->pBus->uResetCheckCount = 0;


    /* Set the pHub::pBus::bDeviceBeingConfigured as TRUE */

    pHub->pBus->bDeviceBeingConfigured = TRUE;

    /* Set the pHub::pBus::uNumberOfConfigRetries as HUB_CONFIG_RETRY_MAX */

    pHub->pBus->uNumberOfConfigRetries = USB_HUB_CONFIG_RETRY_MAX;

    /*
     * For Super Speed buses, we do a Warm Reset, which will also have
     * the effect of Hot Reset (USB_PORT_RESET).
     */

    if (pHub->pBus->uBusSpeed == USB_SPEED_SUPER)
        {
        uFeatureSelector = USB3_HUB_FSEL_BH_PORT_RESET;
        }
    else
        {
        uFeatureSelector = USB_PORT_RESET;
        }

    /*
     * Call USBHST_FILL_SETUP_PACKET() to populate an USBHST_SETUP_PACKET
     * structure by with bmRequest as PORT_TARGET, bRequest as SET_FEATURE,
     * wValue as the PORT_RESET, wIndex as uPortIndex and wLength as 0.
     * Note: the port number is the port index + 1
     */

    USBHST_FILL_SETUP_PACKET(   pSetupPacket,         /* pSetup */
                                USB_PORT_TARGET_SET,  /* uRequestType */
                                USB_SET_FEATURE,      /* uRequest */
                                uFeatureSelector,     /* uValue */
                                uPortIndex + 1,       /* uIndex */
                                0);                   /* uSize */

    /*
     * Call USBHST_FILL_CONTROL_URB() to populate a URB structure with
     * pHub::uDeviceHandle, endpoint as the DEFAUL_ENDPOINT, transfer buffer as
     * NULL, transfer length as 0, USBHST_SETUP_PACKET structure,
     * HUB_ResetCallback as the callback and the pHub as the pContext.
     */

    USBHST_FILL_CONTROL_URB(   pURB,                      /* Urb */
                               pHub->uDeviceHandle,       /* Device */
                               USB_HUB_DEFAULT_ENDPOINT,  /* EndPointAddress */
                               NULL,                      /* TransferBuffer */
                               0,                         /* TransferLength */
                               USBHST_SHORT_TRANSFER_OK,  /* TransferFlags */
                               pSetupPacket,              /* SetupPacket */
                               usbHubResetCallback,       /* Callback */
                               pPort,                     /* pContext */
                               USBHST_FAILURE             /* Status */
                               );

    /*
     * Call usbHstURBSubmit() function with the URB structure. If this call
     * fails then
     * i.   Set pHub::pBus::bDeviceBeingConfigured  as FALSE
     * ii.  Set HUB_PORT_INFO::StateOfPort as MARKED_FOR_DELETION
     */
    Result = usbHstURBSubmit(pURB);

    if (USBHST_SUCCESS != Result)
        {
        /* Debug Message */

        USB_HUB_ERR("failed to submit reset urb for hub 0x%X port %d,"
                    "result = %d\n",
                    pPort->pParentHub->uDeviceHandle,
                    pPort->uPortIndex, Result, 4, 5, 6);

        /* Reset the changes that we had made */

        pHub->pBus->bDeviceBeingConfigured = FALSE;

        /* Mark the port for deletion */

        pPort->StateOfPort = USB_MARKED_FOR_DELETION;

        /* Return the submit result if failed */

        return Result;
        }

    /*
     * Wait until the reset URB to complete, but reset URB
     * complete doesn't necessarily mean a reset fully successful!
     * The pHub->pBus->bDeviceBeingConfigured is set to FALSE
     * in the reset URB callback if there is any failure
     * of this URB, but if the URB is successful, then the
     * pHub->pBus->bDeviceBeingConfigured will be set to FALSE in
     * the reset change handler routine.
     */

    usbHstWaitUrbComplete(pURB,
                          pHub->pBus->hDeviceResetDoneSem,
                          usrUsbWaitTimeOutValueGet());

    Result = pURB->nStatus;

    /*
     * 7.1.7.5 Reset Signaling
     *
     * A hub signals reset to a downstream port by
     * driving an extended SE0 at the port. After the
     * reset is removed, the device will be in the
     * Default state (refer to Section 9.1).
     *
     * The reset signaling can be generated on any Hub
     * or Host Controller port by request from the USB
     * System Software. The reset signaling must be
     * driven for a minimum of 10ms (TDRST). After the
     * reset, the hub port will transition to the
     * Enabled state (refer to Section 11.5).
     */

    OS_DELAY_MS(10);

    USB_HUB_DBG("usbHubResetPort - reset hub 0x%X port %d end\n",
                pPort->pParentHub->uDeviceHandle,
                pPort->uPortIndex, 3, 4, 5, 6);

    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
        USB_HUB_WV_DEVICE,
        "Exiting usbHubResetPort() Function",
        USB_HUB_WV_FILTER);

    /* Return result of the reset status */

    return Result;

    }/* End of HUB_ResetPort() */

/***************************************************************************
*
* usbHubSubmitControlRequest - submit a blocking feature request to the hub.
*
* This routine submits a blocking feature request to the hub.
*
* RETURNS: USBHST_SUCCESS, USBHST_FAILURE, USBHST_INVALID_PARAMETERS if
* parameters are wrong.
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubSubmitControlRequest
    (
    pUSB_HUB_INFO   pHub,
    UINT8           bmRequest,
    UINT8           bRequest,
    UINT16          wValue,
    UINT16          wIndex
    )
    {
    /* This is to store the event Identifier */

    OS_EVENT_ID EventId;

    /* This is the URB */

    USBHST_URB ControlURB;

    /* This stores the setup packet */

    pUSBHST_SETUP_PACKET pSetupPacket  = NULL;

    /* This is to store the request result */

    USBHST_STATUS Result;

    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
        USB_HUB_WV_TRANSFER,
        "Entering usbHubSubmitControlRequest() Function",
        USB_HUB_WV_FILTER);

    /* If pHub is NULL then return USBHST_INVALID_PARAMETER*/

    if (NULL == pHub)
        {
        /* Debug Message */
        USB_HUB_ERR("invalid parameter \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of (NULL == pHub */

    /*
     * Call OS_CREATE_EVENT() to create an OS_EVENT_ID event.
     * If this fails return USBHST_FAILURE.
     */

    EventId = OS_CREATE_EVENT(OS_EVENT_NON_SIGNALED);

    if (OS_INVALID_EVENT_ID == EventId)
        {
        /* Debug Message */
        USB_HUB_ERR("event create failed \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_FAILURE;

        }/* End of (NULL == EventId) */

    /* Allocate memory for the setup packet */

    pSetupPacket = OS_MALLOC(sizeof(USBHST_SETUP_PACKET));

    /* Check if memory is allocated */

    if (pSetupPacket == NULL)
        {
        /* Debug Message */
        USB_HUB_ERR("memory allocate failed for setup packet \n",
                    1, 2, 3, 4, 5, 6);

        OS_DESTROY_EVENT(EventId);

        return USBHST_INSUFFICIENT_MEMORY;
        }

    /* Initialize the memory allocated */

    OS_MEMSET(pSetupPacket, 0, sizeof(USBHST_SETUP_PACKET));

    /*
     * Call USBHST_FILL_USBHST_SETUP_PACKET() to populate an USBHST_SETUP_PACKET structure
     * by with bmRequest, bRequest, wValue, wIndex and wLength as 0.
     */

    USBHST_FILL_SETUP_PACKET(   (pSetupPacket), /* pSetup */
                                bmRequest,      /* uRequestType */
                                bRequest,       /* uRequest */
                                wValue,         /* uValue */
                                wIndex,         /* uIndex */
                                0);             /* uSize */


    /*
     * Call USBHST_FILL_CONTROL_URB() to populate a URB structure with
     * pHub::uDeviceHandle, endpoint as the DEFAUL_ENDPOINT, transfer buffer as
     * NULL, transfer length as 0, USBHST_SETUP_PACKET structure, HUB_BlockingCallback
     * as the callback and the OS_EVENT_ID as the pContext.
     */

    USBHST_FILL_CONTROL_URB(   (&ControlURB),             /* Urb */
                               pHub->uDeviceHandle,       /* Device */
                               USB_HUB_DEFAULT_ENDPOINT,  /* EndPointAddress */
                               NULL,                      /* TransferBuffer */
                               0,                         /* TransferLength */
                               USBHST_SHORT_TRANSFER_OK,  /* TransferFlags */
                               pSetupPacket,              /* SetupPacket */
                               usbHubBlockingCallback,    /* Callback */
                               &EventId,                  /* pContext */
                               USBHST_FAILURE             /* Status */
                               );

    /*
     * Call USBHST_SubmitURB() function with the URB structure. If this call
     * fails then
     * i.    Call OS_DESTROY_EVENT() to destroy the OS_EVENT_ID.
     * ii.    Return the result of the USBHST_SubmitURB() call.
     */

    Result = usbHstURBSubmit(&ControlURB);

    if (USBHST_SUCCESS != Result)
        {
        /* Debug Message */
        USB_HUB_ERR("failed to submit control urb for hub, " \
                    "result=%d \n",
                    Result, 2, 3, 4, 5, 6);

        OS_DESTROY_EVENT(EventId);

        /* Release the memory allocated for the setup packet */

        OS_FREE(pSetupPacket);

        return Result;

        }/* End of (USBHST_SUCCESS !=Result */

    /* Call usbHstWaitUrbComplete to wait the urb complete */

    usbHstWaitUrbComplete(&ControlURB,
                          EventId,
                          usrUsbWaitTimeOutValueGet());

    /*
     * Set "ControlURB.pContext" to NULL to prevent the callback function to
     * be called after the EventId be destroied.
     */

    ControlURB.pContext = NULL;

    /* Call OS_DESTROY_EVENT() to destroy the OS_EVENT_ID.*/

    OS_DESTROY_EVENT(EventId);

    /* store the result */

    Result = ControlURB.nStatus;

    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
        USB_HUB_WV_TRANSFER,
        "Exiting usbHubSubmitControlRequest() Function",
        USB_HUB_WV_FILTER);

    /* Release the memory allocated for the setup packet */

    OS_FREE(pSetupPacket);

    /* Return Result.*/

    return Result;

    } /* End of HUB_SubmitControlRequest() */


/***************************************************************************
*
* usbHubGetStatus - submit a blocking get status request to the hub.
*
* This routine submits a blocking get status request to the hub.
*
* RETURNS: USBHST_SUCCESS, USBHST_FAILURE, USBHST_INVALID_PARAMETERS if
* parameters are wrong.
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubGetStatus
    (
    pUSB_HUB_INFO  pHub,
    UINT8          wIndex,
    UINT8          bmRequest,
    UINT8  *       pBuffer,
    UINT8  *       pBufferLength
    )
    {
    /* This is to store the event Identifier */

    OS_EVENT_ID EventId;

    /* This is the URB */

    USBHST_URB ControlURB;

    /* This stores the setup packet */

    pUSBHST_SETUP_PACKET pSetupPacket;

    /* This is to store the request result */

    USBHST_STATUS Result;

    /* This is to store uIndex of setup packet */

    UINT16 uIndex;

    /* This is to store uSize of setup packet */

    UINT16 uSize;


    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
        USB_HUB_WV_TRANSFER,
        "Entering usbHubGetStatus() Function",
        USB_HUB_WV_FILTER);

    /* If pHub is NULL then return USBHST_INVALID_PARAMETER*/

    if (NULL == pHub)
        {
        USB_HUB_ERR("invalid parameter \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of (NULL == pHub */

     /* if the Length of the buffer is not 4 bytes.. GetStatus violation */

    if ( 4 != *pBufferLength)
        {
        /* Debug Message */

        USB_HUB_ERR("hub status length %d is not 4 \n",
                    *pBufferLength, 2, 3, 4, 5, 6);

        /* Return USBHST_INVALID_PARAMETER */
        return USBHST_INVALID_PARAMETER;

        } /* End of if ( 4 != *pBufferLength) */

    /*
     * Call OS_CREATE_EVENT() to create an OS_EVENT_ID event. If this fails
     * return USBHST_FAILURE.
     */

    EventId = OS_CREATE_EVENT(OS_EVENT_NON_SIGNALED);
    if (NULL == EventId)
        {
        /* Debug Message */
        USB_HUB_ERR("failed to create event \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_FAILURE;

        }/* End of (NULL == EventId) */

    /* Allocate memory for the setup packet */

    pSetupPacket = OS_MALLOC(sizeof(USBHST_SETUP_PACKET));

    /* Check if memory is allocated */

    if (pSetupPacket == NULL)
        {
        /* Debug Message */
        USB_HUB_ERR("memory allocate failed for setup packet \n",
                    1, 2, 3, 4, 5, 6);

        OS_DESTROY_EVENT(EventId);

        return USBHST_INSUFFICIENT_MEMORY;
        }

    /* Initialize the memory allocated */

    OS_MEMSET(pSetupPacket, 0, sizeof(USBHST_SETUP_PACKET));

    /*
     * Call USBHST_FILL_SETUP_PACKET() to populate an SETUP_PACKET
     * structure by with bmRequest as bmRequest, bRequest as GET_STATUS,
     * wValue as the uDescriptorType, wIndex as wIndex and
     * wLength as BufferLength.
     */

    uIndex = (UINT16) wIndex;

    uSize = (UINT16) *pBufferLength;

    USBHST_FILL_SETUP_PACKET(  (pSetupPacket),      /* pSetup */
                                bmRequest,          /* uRequestType */
                                USB_GET_STATUS,     /* uRequest */
                                0,                  /* uValue */
                                uIndex,             /* uIndex */
                                uSize);             /* uSize */

    /*
     * Call USBHST_FILL_CONTROL_URB() to populate a URB structure with
     * uDeviceHandle, endpoint as the DEFAUL_ENDPOINT, transfer buffer as
     * pBuffer, transfer length as BufferLength, USBHST_SETUP_PACKET structure,
     * HUB_BlockingCallback as the callback and the OS_EVENT_ID as the pContext.
     */

    USBHST_FILL_CONTROL_URB(   (&ControlURB),             /* Urb */
                               pHub->uDeviceHandle,       /* Device */
                               USB_HUB_DEFAULT_ENDPOINT,  /* EndPointAddress */
                               pBuffer,                   /* TransferBuffer */
                               *pBufferLength,            /* TransferLength */
                               USBHST_SHORT_TRANSFER_OK,  /* TransferFlags */
                               pSetupPacket,              /* SetupPacket */
                               usbHubBlockingCallback,    /* Callback */
                               &EventId,                  /* pContext */
                               USBHST_FAILURE             /* Status */
                               );

    /*
     * Call USBHST_SubmitURB() function with the URB structure. If this call
     * fails then
     * i.    Call OS_DESTROY_EVENT() to destroy the OS_EVENT_ID.
     * ii.    Return the result of the USBHST_SubmitURB() call.
     */

    Result = usbHstURBSubmit(&ControlURB);
    if (USBHST_SUCCESS != Result)
        {
        /* Debug Message */

        USB_HUB_ERR("submit control request failed, result=%d \n",
                    Result, 2, 3, 4, 5, 6);

        OS_DESTROY_EVENT(EventId);

        /* Release the memory allocated for the setup packet */

        OS_FREE(pSetupPacket);

        return Result;

        }/* End of (USBHST_SUCCESS !=Result */

    /* Call OS_WAIT_FOR_EVENT() function to wait for the OS_EVENT_ID event.*/

    if ( OK != OS_WAIT_FOR_EVENT(EventId, USBHST_URB_TIMEOUT))
        {

        USB_HUB_WARN("usbHubGetStatus - wait event timeout \n",
                     1, 2, 3, 4, 5, 6);

        /* Cancel the URB */

        Result = usbHstURBCancel(&ControlURB);

        /* Wait the URB callback to be called from cancel */

        if (USBHST_SUCCESS == Result)
            {
            if (OK != OS_WAIT_FOR_EVENT(EventId, USBHST_URB_TIMEOUT))
                {
                /* Debug Message */
                USB_HUB_WARN("usbHubGetStatus - wait urb callback timeout \n",
                             1, 2, 3, 4, 5, 6);
                }
            }

        /* Set the status as canceled */

        ControlURB.nStatus = USBHST_TRANSFER_CANCELLED;
        }


    /*
     * Set "ControlURB.pContext" to NULL to prevent the URB's callback
     * function to be called after the EventId be destroied.
     */

    ControlURB.pContext = NULL;

    /* Call OS_DESTROY_EVENT() to destroy the OS_EVENT_ID.*/

    OS_DESTROY_EVENT(EventId);

    /* If the return result is successful,then convert the data */
    if ( (USBHST_SUCCESS == ControlURB.nStatus) &&
         ( 4 == ControlURB.uTransferLength))
        {
        /* Store the result */
        Result = ControlURB.nStatus;

        } /* End of if (USBHST_SUCCESS ==ControlURB.nStatus) */
    else
        {
        /* Copy the transfer length as return to the caller */
        *pBufferLength = (UINT8)ControlURB.uTransferLength;

        /* The request returned non 4 byte data! failed */
        Result = USBHST_FAILURE;

        } /* End of else of if (USBHST_SUCCESS ==ControlURB.nStatus) */


    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
        USB_HUB_WV_TRANSFER,
        "Exiting usbHubGetStatus() Function",
        USB_HUB_WV_FILTER);

    /* Release the memory allocated for the setup packet */

    OS_FREE(pSetupPacket);

    /* Return Result.*/
    return Result;

    } /* End of HUB_GetStatus() */

/*******************************************************************************
*
* usbHubGetPortStatus - submit a get port status request to the hub
*
* This routine submits a get port status request to the hub.
*
* RETURNS: USBHST_SUCCESS, USBHST_FAILURE, USBHST_INVALID_PARAMETERS if
* parameters are wrong.
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubGetPortStatus
    (
    pUSB_HUB_INFO  pHub,
    UINT8          wIndex,
    UINT8          bmRequest,
    UINT8 *        pBuffer,
    UINT8 *        pBufferLength
    )
    {
    /* Storage for the port information */

    pUSB_HUB_PORT_INFO      pPort = NULL;

    /* Storage for the point index */

    UINT8                   uPortIndex;

    /* This stores the setup packet */

    pUSBHST_SETUP_PACKET pSetupPacket;

    /* This is to store the request result */

    USBHST_STATUS Result;

    /* This is to store uIndex of setup packet */

    UINT16 uIndex;

	/* This is to store uSize of setup packet */

	UINT16 uSize;

    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
        USB_HUB_WV_TRANSFER,
        "Entering usbHubGetPortStatus() Function",
        USB_HUB_WV_FILTER);

    /* If pHub or pBuffer is NULL then return USBHST_INVALID_PARAMETER */

    if ((NULL == pHub) || (NULL == pBuffer))
        {
        USB_HUB_ERR("invalid parameters \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of (NULL == pHub */

    /* if the Length of the buffer is not 4 bytes.. GetStatus violation */

    if ( HUB_SIZE_PORT_STATUS != *pBufferLength)
        {
        /* Debug Message */
        USB_HUB_ERR("hub status length %d is not 4 \n",
                    *pBufferLength, 2, 3, 4, 5, 6);

        /* Return USBHST_INVALID_PARAMETER */

        return USBHST_INVALID_PARAMETER;
        } /* End of if ( 4 != *pBufferLength) */

    /* Get the real port index */

    uPortIndex = (UINT8)(wIndex - 1);

    /*
     * If the uPortIndex greater than pHub::HubDescriptor::bNbrPorts then
     * return USBHST_INVALID_PARAMETER.
     * Note: the actual port number is the port index + 1
     */

    if (uPortIndex >= pHub->HubDescriptor.bNbrPorts)
        {
        /* Debug Message */

        USB_HUB_ERR("port index %d >= max %d \n",
                    uPortIndex,
                    pHub->HubDescriptor.bNbrPorts,
                    3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of if (uPortIndex >= pHub->HubDescriptor.bNbrPorts) */

    /*
     * Retrieve the HUB_PORT_INFO structure from pHub::pPortList
     * for the uPortIndex.
     */

    pPort = pHub->pPortList[uPortIndex];

    if ((NULL == pPort) ||
        (pPort->hPortStatusSem == NULL) ||
        (pPort->pParentHub == NULL))
        {
        /* Debug Message */

        USB_HUB_VDBG("usbHubGetPortStatus - get hub 0x%X port %d status\n",
                     pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);
        /*
         * If the port structure has not been allocated yet,
         * call the common usbHubGetStatus to get the port
         * status.
         */

        return usbHubGetStatus(pHub, wIndex, bmRequest, pBuffer, pBufferLength);
        }

    /* Get the pointer to the port status setup packet */

    pSetupPacket = &(pPort->portStatusSetup);

    /* Initialize the memory allocated */

    OS_MEMSET(pSetupPacket, 0, sizeof(USBHST_SETUP_PACKET));

    /*
     * Call USBHST_FILL_SETUP_PACKET() to populate an SETUP_PACKET
     * structure by with bmRequest as bmRequest, bRequest as GET_STATUS,
     * wValue as the uDescriptorType, wIndex as wIndex and
     * wLength as BufferLength.
     */

    uIndex = (UINT16) wIndex;

    uSize = (UINT16) *pBufferLength;

    USBHST_FILL_SETUP_PACKET(  (pSetupPacket),      /* pSetup */
                                bmRequest,          /* uRequestType */
                                USB_GET_STATUS,     /* uRequest */
                                0,                  /* uValue */
                                uIndex,             /* uIndex */
                                uSize);             /* uSize */

    /*
     * Call USBHST_FILL_CONTROL_URB() to populate a URB structure with
     * uDeviceHandle, endpoint as the DEFAUL_ENDPOINT, transfer buffer as
     * pBuffer, transfer length as BufferLength, USBHST_SETUP_PACKET structure,
     * HUB_BlockingCallback as the callback and the OS_EVENT_ID as the pContext.
     */

    USBHST_FILL_CONTROL_URB(   &(pPort->portStatusURB),   /* Urb */
                               pHub->uDeviceHandle,       /* Device */
                               USB_HUB_DEFAULT_ENDPOINT,  /* EndPointAddress */
                               pBuffer,                   /* TransferBuffer */
                               *pBufferLength,            /* TransferLength */
                               USBHST_SHORT_TRANSFER_OK,  /* TransferFlags */
                               pSetupPacket,              /* SetupPacket */
                               usbHubBlockingCallback,    /* Callback */
                               &pPort->hPortStatusSem,    /* pContext */
                               USBHST_FAILURE             /* Status */
                               );

    /*
     * Call usbHstURBSubmit() function with the URB structure. If this call
     * fails then
     * ii.    Return the result of the usbHstURBSubmit() call.
     */

    Result = usbHstURBSubmit(&(pPort->portStatusURB));

    if (USBHST_SUCCESS != Result)
        {
        /* Debug Message */

        USB_HUB_ERR("failed to submit urb for hub 0x%X port %d status, result %d\n",
                    pHub->uDeviceHandle, uPortIndex, Result, 4, 5, 6);

        return Result;

        }/* End of (USBHST_SUCCESS !=Result */

    /* Call OS_WAIT_FOR_EVENT() function to wait for the PortStatusSem */

    if (OK != OS_WAIT_FOR_EVENT(pPort->hPortStatusSem, USBHST_URB_TIMEOUT))
        {
        USB_HUB_WARN("usbHubGetPortStatus - wait event timeout \n",
                     1, 2, 3, 4, 5, 6);

        /* Cancel the URB */

        Result = usbHstURBCancel(&(pPort->portStatusURB));

        /* Wait the URB callback to be called from cancel */

        if (USBHST_SUCCESS == Result)
            {
            if (OK != OS_WAIT_FOR_EVENT(pPort->hPortStatusSem, USBHST_URB_TIMEOUT))
                {
                /* Debug Message */

                USB_HUB_WARN("usbHubGetPortStatus - wait urb callback timeout\n",
                             1, 2, 3, 4, 5, 6);
                }
            }
        /* Set the status as canceled */

        pPort->portStatusURB.nStatus = USBHST_TRANSFER_CANCELLED;

        }

    /* Copy the transfer length as return to the caller */

    *pBufferLength = (UINT8)pPort->portStatusURB.uTransferLength;

    /* If the return result is successful, then convert the data */

    if ((USBHST_SUCCESS == pPort->portStatusURB.nStatus) &&
        (4 == pPort->portStatusURB.uTransferLength))
        {
        /* Store the result */

        Result = USBHST_SUCCESS;

        }
    else
        {

        /* The request returned non 4 byte data! failed */

        Result = USBHST_FAILURE;

        }

    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
        USB_HUB_WV_TRANSFER,
        "Exiting usbHubGetPortStatus() Function",
        USB_HUB_WV_FILTER);

    /* Debug Message */

    USB_HUB_VDBG("usbHubGetPortStatus - "
                 "hub 0x%X port %d uTransferLength=%d\n",
                 pHub->uDeviceHandle,
                 uPortIndex,
                 *pBufferLength, 4, 5, 6);

    /* Return Result.*/

    return Result;
    } /* End of HUB_GetStatus() */

/***************************************************************************
*
* usbHubGetDescriptor - retrieve the Hub descriptor.
*
* This routine retrieves the Hub descriptor.
*
* RETURNS: USBHST_SUCCESS, USBHST_FAILURE, USBHST_INVALID_PARAMETERS if
* parameters are wrong.
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubGetDescriptor
    (
    UINT32   uDeviceHandle,
    UINT8  * pBuffer,
    UINT32 * pBufferLength
    )
    {
    /* This is to store the event Identifier */

    OS_EVENT_ID EventId;

    /* This is the URB */

    USBHST_URB ControlURB;

    /* This stores the setup packet */

    pUSBHST_SETUP_PACKET pSetupPacket = NULL;

    /* This is to store the request result */

    USBHST_STATUS Result;

    /* To store device info */

    pUSBD_DEVICE_INFO           pDeviceInfo = NULL;

    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
        USB_HUB_WV_TRANSFER,
        "Entering usbHubGetDescriptor() Function",
        USB_HUB_WV_FILTER);

    usbdTranslateDeviceHandle (uDeviceHandle, &pDeviceInfo);

    if (pDeviceInfo == NULL)
        {
        /* Debug Message */
        USB_HUB_ERR("failed to get device info\n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_FAILURE;
        }

    /*
     * Call OS_CREATE_EVENT() to create an OS_EVENT_ID event. If this fails
     * return USBHST_FAILURE.
     */

    EventId = OS_CREATE_EVENT(OS_EVENT_NON_SIGNALED);
    if (NULL == EventId)
        {
        /* Debug Message */
        USB_HUB_ERR("failed to create event \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_FAILURE;

        }/* End of (NULL == EventId) */

    /* Allocate memory for the setup packet */

    pSetupPacket = OS_MALLOC(sizeof(USBHST_SETUP_PACKET));

    /* Check if memory is allocated */

    if (pSetupPacket == NULL)
        {
        /* Debug Message */
        USB_HUB_ERR("memory allocate failed for setup packet \n",
                    1, 2, 3, 4, 5, 6);

        OS_DESTROY_EVENT(EventId);

        return USBHST_INSUFFICIENT_MEMORY;

        }

    /* Initialize the memory allocated */

    OS_MEMSET(pSetupPacket, 0, sizeof(USBHST_SETUP_PACKET));

    /*
     * Call USBHST_FILL_SETUP_PACKET() to populate an SETUP_PACKET
     * structure by with bmRequest as bmRequest, bRequest as GET_STATUS,
     * wValue as the uDescriptorType, wIndex as wIndex and
     * wLength as BufferLength.
     */
    if (pDeviceInfo->uDeviceSpeed != USBHST_SUPER_SPEED)
        {
        *pBufferLength = 8;
    USBHST_FILL_SETUP_PACKET(  (pSetupPacket),          /* pSetup */
                                USB_GET_HUB_DESCRIPTOR, /* uRequestType */
                                USB_GET_DESCRIPTOR,     /* uRequest */
                                (USB_HUB_DESCRIPTOR<<8) | 0x00,     /* uValue */
                                0,                  /* uIndex */
                                *pBufferLength);    /* uSize */
        }
    else
        {
        USBHST_FILL_SETUP_PACKET(  (pSetupPacket),          /* pSetup */
                                    USB_GET_HUB_DESCRIPTOR, /* uRequestType */
                                    USB_GET_DESCRIPTOR,     /* uRequest */
                                    (USB_DESCR_USB3_HUB << 8) | 0x00,     /* uValue */
                                    0,                  /* uIndex */
                                    *pBufferLength);    /* uSize */
        }
    /*
     * Call USBHST_FILL_CONTROL_URB() to populate a URB structure with
     * uDeviceHandle, endpoint as the DEFAUL_ENDPOINT, transfer buffer as
     * pBuffer, transfer length as BufferLength, USBHST_SETUP_PACKET structure,
     * HUB_BlockingCallback as the callback and the OS_EVENT_ID as the pContext.
     */

    USBHST_FILL_CONTROL_URB(   (&ControlURB),         /* Urb */
                               uDeviceHandle,   /* Device */
                               USB_HUB_DEFAULT_ENDPOINT,  /* EndPointAddress */
                               pBuffer,               /* TransferBuffer */
                               *pBufferLength,        /* TransferLength */
                               USBHST_SHORT_TRANSFER_OK,/* TransferFlags */
                               pSetupPacket,          /* SetupPacket */
                               usbHubBlockingCallback,  /* Callback */
                               &EventId,              /* pContext */
                               USBHST_FAILURE         /* Status */
                               );
    /*
     * Call USBHST_SubmitURB() function with the URB structure. If this call
     * fails then
     * i.    Call OS_DESTROY_EVENT() to destroy the OS_EVENT_ID.
     * ii.    Return the result of the USBHST_SubmitURB() call.
     */

    Result = usbHstURBSubmit(&ControlURB);

    if (USBHST_SUCCESS !=Result)
        {
        /* Debug Message */
        USB_HUB_ERR("failed to submit urb, result=%d \n",
                    Result, 2, 3, 4, 5, 6);

        OS_DESTROY_EVENT(EventId);

        /* Release the memory allocated for the setup packet */

        OS_FREE(pSetupPacket);

        return Result;

        }/* End of (USBHST_SUCCESS !=Result */

    /* Call usbHstWaitUrbComplete to wait the URB complete */

    usbHstWaitUrbComplete(&ControlURB,
                          EventId,
                          usrUsbWaitTimeOutValueGet());

    /*
     * Set ControlURB.pContext to NULL to prevent the callback function
     * to be called after the EventId is destroied.
     */

    ControlURB.pContext = NULL;

    /* Call OS_DESTROY_EVENT() to destroy the OS_EVENT_ID.*/

    OS_DESTROY_EVENT(EventId);

     /* If the return result is successful,then convert the data */

    if (USBHST_SUCCESS == ControlURB.nStatus)
        {
        /* Convert the data in buffer to proper Endianess */
        OS_BUFFER_LE_TO_CPU(pBuffer, *pBufferLength);

        } /* End of if (USBHST_SUCCESS ==ControlURB.nStatus) */

    /* Store the result */

    Result = ControlURB.nStatus;

    /* Release the memory allocated for the setup packet */

    OS_FREE(pSetupPacket);

    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
            USB_HUB_WV_TRANSFER,
            "Exiting usbHubGetDescriptor() Function",
            USB_HUB_WV_FILTER);

    /* Return Result.*/
    return Result;

    } /* End of usbHubGetDescriptor() */

/***************************************************************************
*
* usbHubSetHubDepth - issue the Set Hub Depth hub class specific request
*
* This routine is to issue the Set Hub Depth hub class specific request.
*
* RETURNS: USBHST_SUCCESS, USBHST_FAILURE, USBHST_INVALID_PARAMETERS if
* parameters are wrong.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubSetHubDepth
    (
    UINT32   uDeviceHandle,
    UINT16   uHubDepth
    )
    {
    /* This is to store the event Identifier */

    OS_EVENT_ID EventId;

    /* This is the URB */

    USBHST_URB ControlURB;

    /* This stores the setup packet */

    pUSBHST_SETUP_PACKET pSetupPacket = NULL;

    /* This is to store the request result */

    USBHST_STATUS Result;

    /* To store device info */

    pUSBD_DEVICE_INFO           pDeviceInfo = NULL;

    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
        USB_HUB_WV_TRANSFER,
        "Entering usbHubSetHubDepth() Function",
        USB_HUB_WV_FILTER);

    usbdTranslateDeviceHandle (uDeviceHandle, &pDeviceInfo);

    if (pDeviceInfo == NULL)
        {
        /* Debug Message */
        USB_HUB_ERR("failed to get device info\n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_FAILURE;
        }

    /* Only SuperSpeed Hub supports this command */

    if (pDeviceInfo->uDeviceSpeed != USBHST_SUPER_SPEED)
        {
        /* Debug Message */
        USB_HUB_WARN("Device not USBHST_SUPER_SPEED, no need to set Hub Depth\n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_FAILURE;
        }

    USB_HUB_DBG("USBHST_SUPER_SPEED Hub 0x%x uHubDepth %d\n",
           uDeviceHandle, uHubDepth, 3, 4, 5, 6);

    /*
     * Call OS_CREATE_EVENT() to create an OS_EVENT_ID event. If this fails
     * return USBHST_FAILURE.
     */

    EventId = OS_CREATE_EVENT(OS_EVENT_NON_SIGNALED);
    if (NULL == EventId)
        {
        /* Debug Message */
        USB_HUB_ERR("failed to create event \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_FAILURE;

        }/* End of (NULL == EventId) */

    /* Allocate memory for the setup packet */

    pSetupPacket = OS_MALLOC(sizeof(USBHST_SETUP_PACKET));

    /* Check if memory is allocated */

    if (pSetupPacket == NULL)
        {
        /* Debug Message */
        USB_HUB_ERR("memory allocate failed for setup packet \n",
                    1, 2, 3, 4, 5, 6);

        OS_DESTROY_EVENT(EventId);

        return USBHST_INSUFFICIENT_MEMORY;
        }

    /* Initialize the memory allocated */

    OS_MEMSET(pSetupPacket, 0, sizeof(USBHST_SETUP_PACKET));

    /*
     * Call USBHST_FILL_SETUP_PACKET() to populate an SETUP_PACKET
     * structure by with bmRequest as bmRequest, bRequest as GET_STATUS,
     * wValue as the uDescriptorType, wIndex as wIndex and
     * wLength as BufferLength.
     */

    uHubDepth = OS_UINT16_CPU_TO_LE(uHubDepth);

    USBHST_FILL_SETUP_PACKET((pSetupPacket),          /* pSetup */
                              USB_HUB_TARGET_SET,     /* uRequestType */
                              USB3_HUB_REQ_SET_HUB_DEPTH, /* uRequest */
                              uHubDepth,              /* uValue */
                              0,                      /* uIndex */
                              0);                     /* uSize */

    /*
     * Call USBHST_FILL_CONTROL_URB() to populate a URB structure with
     * uDeviceHandle, endpoint as the DEFAUL_ENDPOINT, transfer buffer as
     * pBuffer, transfer length as BufferLength, USBHST_SETUP_PACKET structure,
     * HUB_BlockingCallback as the callback and the OS_EVENT_ID as the pContext.
     */

    USBHST_FILL_CONTROL_URB((&ControlURB),    /* Urb */
                             uDeviceHandle,   /* Device */
                             USB_HUB_DEFAULT_ENDPOINT,  /* EndPointAddress */
                             NULL,                  /* TransferBuffer */
                             0,                     /* TransferLength */
                             USBHST_SHORT_TRANSFER_OK,/* TransferFlags */
                             pSetupPacket,          /* SetupPacket */
                             usbHubBlockingCallback,/* Callback */
                             &EventId,              /* pContext */
                             USBHST_FAILURE         /* Status */
                             );
    /*
     * Call USBHST_SubmitURB() function with the URB structure. If this call
     * fails then
     * i.    Call OS_DESTROY_EVENT() to destroy the OS_EVENT_ID.
     * ii.    Return the result of the USBHST_SubmitURB() call.
     */

    Result = usbHstURBSubmit(&ControlURB);

    if (USBHST_SUCCESS != Result)
        {
        /* Debug Message */
        USB_HUB_ERR("failed to submit urb, result=%d \n",
                    Result, 2, 3, 4, 5, 6);

        OS_DESTROY_EVENT(EventId);

        /* Release the memory allocated for the setup packet */

        OS_FREE(pSetupPacket);

        return Result;

        }/* End of (USBHST_SUCCESS !=Result */

    /* Call usbHstWaitUrbComplete to wait the URB complete */

    usbHstWaitUrbComplete(&ControlURB,
                          EventId,
                          usrUsbWaitTimeOutValueGet());

    /*
     * Set ControlURB.pContext to NULL to prevent the callback function
     * to be called after the EventId is destroied.
     */

    ControlURB.pContext = NULL;

    /* Call OS_DESTROY_EVENT() to destroy the OS_EVENT_ID.*/

    OS_DESTROY_EVENT(EventId);

    /* Store the result */

    Result = ControlURB.nStatus;

    /* Release the memory allocated for the setup packet */

    OS_FREE(pSetupPacket);

    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
            USB_HUB_WV_TRANSFER,
            "Exiting usbHubGetDescriptor() Function",
            USB_HUB_WV_FILTER);

    /* Return Result.*/
    return Result;
    } /* End of HUB_GetHubDescriptor() */

/*******************************************************************************
*
* usbHubSubmitInterruptRequest - submitting a status change Interrupt IN request
*
* This routine submits a status change Interrupt IN request.
*
* RETURNS: USBHST_SUCCESS, USBHST_FAILURE, USBHST_INVALID_PARAMETERS if
* parameters are wrong.
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubSubmitInterruptRequest
    (
    pUSB_HUB_INFO pHub
    )
    {
    /* pointer to store the URB pointer */

    pUSBHST_URB pURB;

    /* to store the result of the request */

    USBHST_STATUS Result;

    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
        USB_HUB_WV_TRANSFER,
        "Entering usbHubSubmitInterruptRequest() Function",
        USB_HUB_WV_FILTER);

    /* If pHub is NULL return USBHST_INVALID_PARAMETER.*/

    if (NULL == pHub)
        {
        /* Debug Message */
        USB_HUB_ERR("invalid parameter \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Retrieve the URB structure from the pHub*/

    pURB = (&(pHub->StatusChangeURB));

    /*
     * If URB::uInterruptEndpointNumber is invalid endpoint number
     * then return USBHST_FAILURE
     */

    if ( (pURB->uEndPointAddress != pHub->uInterruptEndpointNumber) ||
         (0 == pHub->uInterruptEndpointNumber) )
        {
        /* Debug Message */

        USB_HUB_ERR("invalid endpoint number, uEndPointAddress=%d, " \
                    "uInterruptEndpointNumber=%d \n",
                    pURB->uEndPointAddress,
                    pHub->uInterruptEndpointNumber,
                    3, 4, 5, 6);

        return USBHST_FAILURE;

        } /* End of if ((pURB->uEndpoint.... */

    /* If URB::pTransferBuffer is NULL then return USBHST_FAILURE */
    if (NULL == pURB->pTransferBuffer)
        {
        /* Debug Message */
        USB_HUB_ERR("pTransferBuffer is NULL \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_FAILURE;

        } /* End of if (NULL == pURB->pTransfer.. */

    if (pHub->hubMutex) /* No need to check semTake return value */
        (void)semTake(pHub->hubMutex, WAIT_FOREVER);

    /*
     * If the <pHub->bURBSubmitted == TRUE> this means there is
     * an interrupt URB is already submitted.
     * Just return "USBHST_SUCCESS" to indicate the success.
     */

    if (pHub->bURBSubmitted == TRUE)
        {
        if (pHub->hubMutex)
            semGive(pHub->hubMutex);
        return USBHST_SUCCESS;
        }

    /* Set the URB Submitted flag as true */

    pHub->bURBSubmitted = TRUE;

    if (pHub->hubMutex)
        semGive(pHub->hubMutex);

    /* Restore the length */

    pURB->uTransferLength = pHub->uTotalStatusLength;

    /* Call USBHST_SubmitRequest() for submitting the URB*/

    Result = usbHstURBSubmit(pURB);

    /* If the submition failed, mark it as not subitted */

    if (USBHST_SUCCESS != Result)
        {
        /* Set the URB Submitted flag as FALSE */
        USB_HUB_ERR("failed to submit interrupt urb, result=%d \n",
                    Result, 2, 3, 4, 5, 6);

        if (pHub->hubMutex) /* No need to check semTake return value */
            (void)semTake(pHub->hubMutex, WAIT_FOREVER);

        pHub->bURBSubmitted = FALSE;

        if (pHub->hubMutex)
            semGive(pHub->hubMutex);

        }

    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
        USB_HUB_WV_TRANSFER,
        "Exiting usbHubSubmitInterruptRequest() Function",
        USB_HUB_WV_FILTER);

    /* Return the result of the USBHST_SubmitRequest()*/
    return Result;

    } /* End of usbHubSubmitInterruptRequest() */



/******************* MODULE SPECIFIC FUNCTIONS DEFINITION *********************/


/***************************************************************************
*
* usbHubResetCallback - used as a call back for the Reset
*
* This function is used as a call back for the Reset. On failure, reset is
* retried for a maximum number of tries. If the maximum number of retries
* expires, the port is disabled.
*
* RETURNS: USBHST_SUCCESS, USBHST_INVALID_PARAMETERS if  parameters are wrong.
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubResetCallback
    (
    pUSBHST_URB pResetURB
    )
    {
    /* Storage of the port information */

    pUSB_HUB_PORT_INFO pPort = NULL;

    /* Storage for the request results */

    USBHST_STATUS Result;

    /* Pointer to the hub where the reset is taking place */

    pUSB_HUB_INFO pHub = NULL;

    /* If pURB or any of its context is NULL return USBHST_INVALID_PARAMETER */

    if ((NULL == pResetURB) ||
        (NULL == pResetURB->pTransferSpecificData) ||
        (NULL == pResetURB->pContext))
        {
        /* Debug Message */
        USB_HUB_ERR("invalid parameters \n",
                    1, 2, 3, 4, 5, 6);
        return USBHST_INVALID_PARAMETER;

        }/* End of if (NULL == pURB ) */

    /* Retrieve the Hub port */

    pPort = (pUSB_HUB_PORT_INFO) pResetURB->pContext;

    /* Get the pointer to the parent hub */

    pHub = pPort->pParentHub;

    /* If pHub or pHub->pBus is NULL return USBHST_INVALID_PARAMETER */

    if ((NULL == pHub) || (NULL == pHub->pBus))
        {
        /* Debug Message */
        USB_HUB_ERR("parent hub not found \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_FAILURE;

        }/* End of if (NULL == pHub ) */

    /*
     * If the hub was marked for deletion then there is
     * no sense in doing other operations
     */

    if (USB_MARKED_FOR_DELETION == pHub->StateOfHub)
        {
        /* Mark the port for deletion */

        pPort->StateOfPort = USB_MARKED_FOR_DELETION;

        /* Set the bus state as no device is being configured */

        pHub->pBus->bDeviceBeingConfigured = FALSE;

        /* Release the reset done semaphore */

        semGive(pHub->pBus->hDeviceResetDoneSem);

        /* return success */

        return USBHST_SUCCESS;
        }

    /* Debug Message */

    USB_HUB_DBG("usbHubResetCallback - reset hub 0x%X port %d done, status %d\n",
                pHub->uDeviceHandle,
                pPort->uPortIndex,
                pResetURB->nStatus, 4, 5, 6);
    /*
     * If pURB::uStatus has failed then
     * i.     Retrieve the HUB_INFO structure from the pURB::pContext,
     *        if failed go to step 6.
     * ii.    If pURB::uStatus is DEVICE_NOT_RESPONDING then
     *        a.    Update the HUB_INFO::StateOfHub as MARKED_FOR_DELETION.
     *        b.    Update the HUB_INFO::pBus::bDeviceBeingConfigured as FALSE.
     *        c.    Return USBHST_SUCCESS.
     * iii.   Decrement HUB_INFO::pBus::uNumberOfConfigRetries
     * iv.     If the HUB_PORT_INFO::StateOfPort is not
     *        HUB_RESET_COMPLETION_PENDING then go to step 6.
     * v.    If HUB_INFO::pBus::uNumberOfConfigRetries is non-zero then call
     *        the usbHstURBSubmit() function to submit port reset with pURB
     *        and the usbHubResetCallback as the call back function. If this
     *        call succeeds, return  USBHST_SUCCESS. Else
     *        a.    Update the HUB_INFO::StateOfHub as MARKED_FOR_DELETION.
     *        b.    Update the HUB_INFO::pBus::bDeviceBeingConfigured as FALSE.
     *        c.    Return  USBHST_SUCCESS.
     * vi.    Update the HUB_PORT_INFO::StateOfPort as MARKED_FOR_DELETION.
     */

    if (USBHST_SUCCESS != pResetURB->nStatus)
        {
        /* Debug Message */

        USB_HUB_WARN("usbHubResetCallback - reset hub 0x%X port %d failed, status %d\n",
                     pHub->uDeviceHandle,
                     pPort->uPortIndex,
                     pResetURB->nStatus, 4, 5, 6);

        /*
         *  i. Retrieve the HUB_INFO structure from the pURB::pContext,
         *    if failed go to step 6. This has been done above!
         */

        /* ii. If pURB::uStatus is DEVICE_NOT_RESPONDING_ERROR then */

        if (USBHST_DEVICE_NOT_RESPONDING_ERROR == pResetURB->nStatus)
            {
            /* Debug Message */

            USB_HUB_DBG("usbHubResetCallback - "
                        "reset hub 0x%X port %d failed, status %d, "
                        "device not responding\n",
                        pHub->uDeviceHandle,
                        pPort->uPortIndex,
                        pResetURB->nStatus, 4, 5, 6);

            /* Mark the port for deletion */

            pPort->StateOfPort = USB_MARKED_FOR_DELETION;

            /* Set the bus state as no device is being configured */

            pHub->pBus->bDeviceBeingConfigured = FALSE;

            /* Release the reset done semaphore */

            semGive(pHub->pBus->hDeviceResetDoneSem);

            return USBHST_SUCCESS;
            }/* End of if (USBHST_DEVICE_NOT_RESPONDING .. */

        /* Debug Message */

        USB_HUB_DBG("usbHubResetCallback - "
                    "reset hub 0x%X port %d failed, status %d, "
                    "retrying config\n",
                    pHub->uDeviceHandle,
                    pPort->uPortIndex,
                    pResetURB->nStatus, 4, 5, 6);

        /* iii.   Decrement HUB_INFO::pBus::uNumberOfConfigRetries */

        pHub->pBus->uNumberOfConfigRetries--;

        /*
         * v. If the HUB_PORT_INFO::StateOfPort is not
         *    HUB_RESET_COMPLETION_PENDING then go to step 6.
         */

        if (USB_HUB_RESET_COMPLETION_PENDING == pPort->StateOfPort)
            {
            /*
             * vi.If HUB_INFO::pBus::uNumberOfConfigRetries is non-zero
             * then call the usbHstURBSubmit() function to submit port
             * reset with pURB and the usbHubResetCallback as the callback
             * function.
             * If this call succeeds, return  USBHST_SUCCESS. Else
             *    a.    Update the HUB_INFO::StateOfHub as
             *          MARKED_FOR_DELETION.
             *    b.    Set pHub::pBus::bDeviceBeingConfigured as FALSE
             *    c.    Return  USBHST_SUCCESS.
             * vii.Update the HUB_PORT_INFO::StateOfPort as
             *      MARKED_FOR_DELETION.
             */

            if (0 != pHub->pBus->uNumberOfConfigRetries)
                {
                Result = usbHstURBSubmit(pResetURB);

                if (USBHST_SUCCESS != Result)
                    {
                    /* Debug Message */

                    USB_HUB_ERR ("reset hub 0x%X port %d failed,"
                                 "retrying config submit urb failed\n",
                                 pHub->uDeviceHandle,
                                 pPort->uPortIndex,
                                 3, 4, 5, 6);


                    /* Mark the port for deletion */

                    pPort->StateOfPort = USB_MARKED_FOR_DELETION;

                    /* Set the bus state as no device is being configured */

                    pHub->pBus->bDeviceBeingConfigured = FALSE;

                    /* Release the reset done semaphore */

                    semGive(pHub->pBus->hDeviceResetDoneSem);
                    }

                /* Only here we do not release the reset done semaphore */

                return USBHST_SUCCESS;
                } /* End of if (0!=... */

            /* Debug Message */

            USB_HUB_ERR ("reset hub 0x%X port %d failed,"
                         "config retry out\n",
                         pHub->uDeviceHandle,
                         pPort->uPortIndex,
                         3, 4, 5, 6);


            /* Mark the port for deletion */

            pPort->StateOfPort = USB_MARKED_FOR_DELETION;

            /* Set the bus state as no device is being configured */

            pHub->pBus->bDeviceBeingConfigured = FALSE;

            /* Release the reset done semaphore */

            semGive(pHub->pBus->hDeviceResetDoneSem);
            }
        }
    else
        {
        /* Update the HUB_PORT_INFO::StateOfPort as HUB_RESET_COMPLETED.*/

        pPort->StateOfPort = USB_HUB_RESET_COMPLETED;

        /*
         * Note: We do not set pHub->pBus->bDeviceBeingConfigured
         * to FALSE here, we have to wait until the device reports
         * the reset change event and then set the flag as FALSE
         * in the reset change handler. Otherwise, it is observed
         * some hubs will behave strangely, because no other reset
         * can be issued before the reset change is reported.
         */

        semGive(pHub->pBus->hDeviceResetDoneSem);
        }/* End of if (USBHST_SUCCESS != pResetURB->uStatus) */

    /* return USBHST_SUCCESS */

    return USBHST_SUCCESS;

    } /* End of usbHubResetCallback() */

/***************************************************************************
*
* usbHubBlockingCallback - callback when a blocking request is issued
*
* This routine is a general purpose call back function that can be used for
* issuing a blocking request.
*
* RETURNS: USBHST_SUCCESS.
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubBlockingCallback
    (
    pUSBHST_URB pUrb
    )
    {
    OS_EVENT_ID * EventId;

    if ((NULL == pUrb) || (NULL == pUrb->pContext))
        {
        USB_HUB_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);
        return USBHST_INVALID_PARAMETER;
        }

    /* Storage for the event id. */

    EventId  = (OS_EVENT_ID * )pUrb->pContext;

    /* Call OS_RELEASE_EVENT () with pURB::pContext*/

    OS_RELEASE_EVENT(*EventId);

    return USBHST_SUCCESS;

    }/* End of HUB_BlockingCallback() */


/***************************************************************************
*
* usbHubInterruptRequestCallback - call back for the status change interrupt.
*
* This routine is a call back for the status change interrupt IN  pipe
* completion.
*
* RETURNS: USBHST_SUCCESS, USBHST_FAILURE, USBHST_INVALID_PARAMETER if
* parameters are wrong.
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubInterruptRequestCallback
    (
    pUSBHST_URB pURB
    )
    {
    /* Storage for the pHub */

    pUSB_HUB_INFO pHub = NULL;

    /* To store the status of the submit request */

    USBHST_STATUS    status = USBHST_FAILURE;

    if ((NULL == pURB) || (NULL == pURB->pContext))
        {
        /* Debug Message */

        USB_HUB_ERR("Invalid Parameter\n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }

    /*
     * Retrieve the HUB_INFO structure from pURB::pContext.
     * if this fails,return USBHST_FAILURE
     */

    pHub = (pUSB_HUB_INFO) pURB->pContext;

    /* If pHub or pHub->pBus is NULL return USBHST_INVALID_PARAMETER */

    if (NULL == pHub->pBus)
        {
        /* Debug Message */
        USB_HUB_ERR("failed to get hub information \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_FAILURE;

        }/* End of if (NULL == pHub ) */

    /*
     * If URB::uInterruptEndpointNumber is invalid endpoint number
     * then return USBHST_FAILURE
     */

    if ( (pURB->uEndPointAddress != pHub->uInterruptEndpointNumber) ||
         (0 == pHub->uInterruptEndpointNumber) )
        {
        /* Debug Message */
        USB_HUB_ERR("invalid endpoint number, uEndPointAddress=%d, " \
                    "uInterruptEndpointNumber=%d \n",
                    pURB->uEndPointAddress,
                    pHub->uInterruptEndpointNumber,
                    3, 4, 5, 6);

        return USBHST_FAILURE;

        } /* End of if ((pURB->uEndpoint.... */

   /*
    * Check if there is any corruption in the URB, valid HUB interrupt request
    * should have non NULL transfer buffer.
    */
    if (pURB->pTransferBuffer == NULL)
        {
        /* Debug Message */
        USB_HUB_ERR("invalid transfer buffer \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_FAILURE;
        }

    /* Take the mutex before update the flag */

    if (pHub->hubMutex) /* No need to check semTake return value */
        (void)semTake(pHub->hubMutex, WAIT_FOREVER);


    /* Set the URB Submitted flag as false */

    pHub->bURBSubmitted = FALSE;

    /* Release the mutex */

    if (pHub->hubMutex)
        semGive(pHub->hubMutex);

    if (USBHST_SUCCESS != pURB->nStatus)
        {
        pHub->uNumErrors++;

        USB_HUB_ERR("uDeviceHandle=0x%X, uCurrentTier=%d, " \
                    "error num=%d, error code=%d \n",
                    pHub->uDeviceHandle,
                    pHub->uCurrentTier,
                    pHub->uNumErrors,
                    pURB->nStatus, 5, 6);
        /*
         * If the error happened consecutively for more than USB_HUB_ERROR_COUNT_MAX,
         * times the hub may have become not working. Right now, we mark it
         * to be deleted; We may end up reseting the hub itself to try to
         * make it back to life again! (May need to pretend a connection change!)
         */

        if (pHub->uNumErrors > USB_HUB_ERROR_COUNT_MAX)
            {
            USB_HUB_ERR("number of errors %d > max %d \n",
                        pHub->uNumErrors,
                        USB_HUB_ERROR_COUNT_MAX,
                        3, 4, 5, 6);

            /* Mark the hub for deletion */
            pHub->StateOfHub = USB_MARKED_FOR_DELETION;

            /* return */
            return pURB->nStatus;

            }

        if (USBHST_STALL_ERROR == pURB->nStatus)
            {
            /*
             * It is not due to the transfer that stalls, It is due to the fact
             * that the UHCI sets every td to stalled for every transfer that is
             * not yet completed, when a device gets unplugged (like a hub).
             * So no need to clear the endpoint feature when the EP is halt
             */

            USB_HUB_ERR("stall error \n",
                        1, 2, 3, 4, 5, 6);

            }
        }


    /*
     * If the hub reports a zero length data,
     * submit the request again.
     */

    if ((pURB->uTransferLength == 0) || (USBHST_SUCCESS != pURB->nStatus))
        {
        /*
         * If a hub submits a zero length of data instead of NAK, then allocating
         * USB_HUB_BYTE_GRANULARITY(numberOfPorts) + 1) bytes to the data buffer
         * before resubmitting the request to the hub.
         */
        pURB->uTransferLength = pHub->uTotalStatusLength;

        /* Call HUB_SubmitInterruptRequest() with the pHub  */

        status = usbHubSubmitInterruptRequest(pHub);

        /* If failed, delete the hub */

        if (status != USBHST_SUCCESS)
            {
            /* Debug Message */

            USB_HUB_ERR("submit urb failed, status= %d \n",
                        status, 2, 3, 4, 5, 6);

            /* Take the mutex before access the status buffer */

            if (pHub->hubMutex) /* No need to check semTake return value */
                (void)semTake(pHub->hubMutex, WAIT_FOREVER);

            /* Clear the status buffer to indicate no more events */

            memset(pHub->pCurrentStatus, 0, pHub->uTotalStatusLength);

            /* reset the status change to FALSE */

            pHub->bCurrentStatusChanged = FALSE;

            /* Mark the hub for deletion */

            pHub->StateOfHub = USB_MARKED_FOR_DELETION;

            /* Release the mutex */

            if (pHub->hubMutex)
                semGive(pHub->hubMutex);


            } /* End of if (USBHST_SUCCESS !=Result) */

        return status;
        }

    /* Clear the error counter when a successful transfer returned */

    pHub->uNumErrors = 0;

    /*
     * Call HUB_IS_HUBEVENT() with pURB::pTransferBuffer to check for a hub
     * event. If this returns TRUE then:
     * i.    Increment pHub::pBus::uNumberOfHubEvents
     */

    if (TRUE == USB_HUB_IS_EVENT(pURB->pTransferBuffer))
        {
        /* Debug Message */

        USB_HUB_DBG("usbHubInterruptRequestCallback - add a hub event\n",
                     1, 2, 3, 4, 5, 6);

        /*
         * Call OS_LOCKED_INCREMENT() to increment pHub::pBus::uNumberOfHubEvents.
         */

        OS_LOCKED_INCREMENT(pHub->pBus->uNumberOfHubEvents);
        }

    /* Convert the data recieved from the LE to the CPU format */

    OS_BUFFER_LE_TO_CPU(pURB->pTransferBuffer, pURB->uTransferLength);

    /* Take the mutex befor access the status buffer */

    if (pHub->hubMutex) /* No need to check semTake return value */
        (void)semTake(pHub->hubMutex, WAIT_FOREVER);

    /* Update the actual status length */

    pHub->uActualStatusLength = pURB->uTransferLength;

    /*
     * Copy the data pURB::pTransferBuffer to HUB_INFO::pStatus for
     * pURB::nLengthOfTransfer bytes.
     */

    OS_MEMCPY (pHub->pCurrentStatus, pURB->pTransferBuffer, pURB->uTransferLength);

    /* set the status change to TURE */

    pHub->bCurrentStatusChanged = TRUE;

    /* Release the mutex */

    if (pHub->hubMutex)
        semGive(pHub->hubMutex);

#ifndef USB_HUB_CREATE_BUSM_TASK_FOR_EACH_BUS

    usbHubAddEvent(pHub);

#endif
    /* Return  USBHST_SUCCESS */
    return USBHST_SUCCESS;

    }/* End of HUB_InterruptRequestCallback() */


/***************************************************************************
*
* usbHubFindNearestParent - recursively search for the parent hub
*
* This routine recursively searches for the parent hub.
*
* RETURNS: pointer to the PHUB_INFO, NULL.
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL pUSB_HUB_INFO usbHubFindNearestParent
    (
    UINT32        uDeviceHandle,
    pUSB_HUB_INFO pHub,
    UINT8         * pPortNumber
    )
    {
    /* Counter for the port number */

    UINT8 uPortCount = 0;

    /* If pHub is NULL then return NULL*/

    if (NULL == pHub)
        {
        /* Debug Message */
        USB_HUB_ERR("invalid parameter \n",
                    1, 2, 3, 4, 5, 6);
        return NULL;

        }/* End of (NULL == pHub) */

     /* Check for the pPortNumber */

    if (NULL == pPortNumber)
        {
        /* Debug Message */
        USB_HUB_ERR("invalid parameter \n",
                    1, 2, 3, 4, 5, 6);

        /* return NULL */
        return NULL;

        }/* End of (NULL == pPortNumber) */


    /*
     * For all ports in the pHub::pPortList that are not NULL
     * i.     Retrieve the HUB_PORT_INFO from pHub::pPortList[ port count]
     * ii.    If uDeviceHandle is same as HUB_PORT_INFO::uDeviceHandle  then
     *        return pHub
     */

    for (uPortCount = 0;
         uPortCount < pHub->HubDescriptor.bNbrPorts;
         uPortCount++)
        {
        /* Retrieve the HUB_PORT_INFO structure */

        pUSB_HUB_PORT_INFO pPort = pHub->pPortList[uPortCount];

        /* Check if the port is enabled */

        if (NULL != pPort)
            {
            /* Check for uDeviceHandle */

            if ( uDeviceHandle == pPort->uDeviceHandle )
                {
                /* Debug Message */

                USB_HUB_VDBG("usbHubFindNearestParent - "
                            "found hub information (parent hub 0x%X port %d)\n",
                            pHub->uDeviceHandle, uPortCount, 3, 4, 5, 6);

                if(pHub->uHubTTInfo > 0)
                    {
                    /* Set the port number as the port index  */
                    *pPortNumber = uPortCount;
                    }
                else
                    {
                    *pPortNumber = USB_PORT_NUMBER_NOT_FOUND;
                    }
                return pHub;

                }/* End of if (uDeviceHandle... */

            } /* End of If (NULL != pPort) */

        } /* End of for (uPortCount.... */


    /*
     * For all ports in the pHub::pPortList that are not NULL
     * i.    Retrieve the HUB_PORT_INFO from pHub::pPortList[ port count]
     * ii.     If HUB_PORT_INFO::pHub is not NULL then:
     * iii.    Call HUB_FindParentHub () function to find the matching hub. If
     *         this
     * returns a non NULL value, return the value.
     */

    for (uPortCount = 0;
         uPortCount < pHub->HubDescriptor.bNbrPorts;
         uPortCount++)
        {
        /* Retrieve the HUB_PORT_INFO structure */

        pUSB_HUB_PORT_INFO pPort = pHub->pPortList[uPortCount];

        /* Check if the port is enabled */

        if (NULL != pPort)
            {
            /* Check for Hub device */

            if ( NULL != pPort->pHub)
                {
                /* storage for storing the result */

                pUSB_HUB_INFO pResultHub ;

                /* Call for matching hub in  this sub tree. */

                pResultHub = usbHubFindNearestParent(uDeviceHandle,pPort->pHub,pPortNumber);

                /* Check if the hub was found */

                if (NULL != pResultHub)
                    {
                    /* the last bit of pPortNumber is the flag */
                    if(*pPortNumber == USB_PORT_NUMBER_NOT_FOUND )
                        {
                        if(pHub->uHubTTInfo > 0)
                            {
                            pResultHub = pHub;
                            /* Enter the port number and set the flag */
                            *pPortNumber = uPortCount;
                            }
                        }
                    return pResultHub;
                    }/* End of if (NULL != pResultHub ) */

                }/* End of if (NULL != pPort->pHub) */

            } /* End of If (NULL != pPort) */

        } /* End of for (uPortCount.... */

    /* Return NULL*/
    return NULL;

    }/* End of usbHubFindNearestParent() */


/***************************************************************************
*
* usbHubValidateDescriptor - validates the standard descriptors.
*
* This routine validates the standard descriptors.
*
* RETURNS: Status.
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubValidateDescriptor
    (
    UINT8  * pDescriptor,
    UINT16 uUsbDeviceVersion,
    UINT8  uDescriptorType
    )
    {

    return USBHST_SUCCESS;

#if 0

    /* To store the return value */

    USBHST_STATUS Status;

    /* swap bytes */

    uUsbDeviceVersion = OS_UINT16_LE_TO_CPU(uUsbDeviceVersion);

    /* Check if pDescriptor is Invalid*/

    if(NULL == pDescriptor)
        {

        /* Debug Message */
        USB_HUB_ERR("invalid parameter \n",
                    1, 2, 3, 4, 5, 6);

        /*return the Error Status*/
        return USBHST_INVALID_PARAMETER ;

        }

    /* Check the USB Version of the device */

    /* This is a 2.0 device */

    if (uUsbDeviceVersion == USB_HUB_VERSION_0200)
        {
        /* Switch to select the descriptor type */

        switch(uDescriptorType)
            {

            /* This is a device descriptor */

            case USBHST_DEVICE_DESC :
                /*Validate the fields of device descriptor */

                Status = (INT8)((0 == ((pDescriptor[1] == USBHST_DEVICE_DESC) &&
                                 (pDescriptor[4] == USBHST_HUB_CLASS) &&
                                 (pDescriptor[6] < HUB_VALID_RANGE_PROTOCOL) &&
                                 (pDescriptor[7] <= HUB_MAX_PACKET_SIZE) &&
                                 (pDescriptor[17] == HUB_NUMBER_CONFIG ))) ?
                                USBHST_INVALID_PARAMETER : USBHST_SUCCESS);
                break;
            case USBHST_CONFIG_DESC :
                /*Validate the fields of config descriptor */
                Status = (INT8)((0 == ((pDescriptor[1] == USBHST_CONFIG_DESC) &&
                                  (pDescriptor[4] == HUB_NUMBER_INTERFACE))) ?
                                 USBHST_INVALID_PARAMETER : USBHST_SUCCESS);
                break;
            case USBHST_INTERFACE_DESC :
                /*Validate the fields of interface descriptor */
                Status = (INT8)((0 == ((pDescriptor[1] == USBHST_INTERFACE_DESC) &&
                                  (pDescriptor[2] == HUB_INTERFACE_NUMBER) &&
                                  (pDescriptor[3] < HUB_VALID_RANGE_ALTERNATE) &&
                                  (pDescriptor[4] == HUB_NUMBER_ENDPOINT) &&
                                  (pDescriptor[5] == USBHST_HUB_CLASS) &&
                                  (pDescriptor[6] == HUB_SUB_CLASS_CODE) &&
                                  (pDescriptor[7] < HUB_VALID_RANGE_PROTOCOL))) ?
                                 USBHST_INVALID_PARAMETER : USBHST_SUCCESS);
                break;
            case USBHST_ENDPOINT_DESC :
                /*Validate the fields of endpoint descriptor */
                Status = (INT8)((0 == ((pDescriptor[1] == USBHST_ENDPOINT_DESC) &&
                                  (pDescriptor[3] == USBHST_INTERRUPT_TRANSFER))) ?
                                 USBHST_INVALID_PARAMETER : USBHST_SUCCESS);
                break;
            default :

                /* Debug Message */
                USB_HUB_ERR("invalid descriptor type \n",
                            1, 2, 3, 4, 5, 6);

                /*return the Error Status*/
                Status = USBHST_INVALID_PARAMETER ;
            }/*end of switch(uDescriptor...)*/


        }/* end of if(uUsbDeviceVersion ..) */

    else if ((uUsbDeviceVersion == USB_HUB_VERSION_0110)||(uUsbDeviceVersion ==
        USB_HUB_VERSION_0100) || (uUsbDeviceVersion == USB_HUB_VERSION_0101))

        {

        /* Switch to select the descriptor type */
        switch(uDescriptorType)
            {

            /* Validate the fields of device descriptor */
            case USBHST_DEVICE_DESC :
                Status = (INT8)((0 == ((pDescriptor[4] == USBHST_HUB_CLASS) &&
                                 (pDescriptor[5] == HUB_SUB_CLASS_CODE))) ?
                                USBHST_INVALID_PARAMETER : USBHST_SUCCESS );
                break;

            /* Validate the fields of config descriptor */
            case USBHST_CONFIG_DESC :
                Status = USBHST_SUCCESS;
                break;
            /* Validate the fields of interface descriptor */
            case USBHST_INTERFACE_DESC :
                Status = (INT8)((0 == ((pDescriptor[4] == HUB_NUMBER_ENDPOINT) &&
                                  (pDescriptor[5] == USBHST_HUB_CLASS) &&
                                  (pDescriptor[6] == HUB_SUB_CLASS_CODE) &&
                                  (pDescriptor[7] == HUB_PROTOCOL_CODE))) ?
                                 USBHST_INVALID_PARAMETER : USBHST_SUCCESS);
                    break;
            /* Validate the fields of Endpoint descriptor */
            case USBHST_ENDPOINT_DESC :
                 Status = (INT8)((0 == ((pDescriptor[3] == USBHST_INTERRUPT_TRANSFER))) ?
                                  USBHST_INVALID_PARAMETER : USBHST_SUCCESS);
                 break;
            default :

                /* Debug Message */
                USB_HUB_ERR("invalid descriptor type \n",
                            1, 2, 3, 4, 5, 6);

                /* return the Error Status */
                Status = USBHST_INVALID_PARAMETER ;

            }/*endof switch(uDescriptor... )*/

        }/*end of else if (uUsbDeviceVersion ..)*/
    else
        {
        /* Debug Message */
        USB_HUB_ERR("invalid usb version, version=%d \n",
                    uUsbDeviceVersion, 2, 3, 4, 5, 6);

        /* Return the Error Status */
        Status = USBHST_INVALID_PARAMETER ;

        }/* end of else*/

    return Status;

#endif
    } /*end of HUB_ValidateDescriptor */


/***************************************************************************
*
* usbHubClearTTRequestCallback - call back function.
*
* This routine is a call back function for the completion of a  Clear TT
* request to a High Speed Hub for the completion of a  Clear TT request to
* a High Speed Hub.
*
* RETURNS: USBHST_SUCCESS if successful
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubClearTTRequestCallback
    (
    pUSBHST_URB pURB
    )
    {

    /* Storage for the request results */

    USBHST_STATUS Result = USBHST_SUCCESS;


    /* If pURB is NULL return USBHST_INVALID_PARAMETER.*/

    if (NULL == pURB )
        {
        USB_HUB_ERR("invalid parameter \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of if (NULL == pURB ) */

    /* Free allocated memory  */

    OS_FREE(pURB->pTransferSpecificData);

    /* Free allocated memory  */

    OS_FREE(pURB);

    return Result;

    }/*end of HUB_ClearTTRequestCallback */


/***************************************************************************
*
* usbHubResetTTRequestCallback - call back function.
*
* This routine is a call back function for the completion of a reset TT request
* to a High Speed Hub.
*
* RETURNS: USBHST_SUCCESS if successful
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubResetTTRequestCallback
    (
    pUSBHST_URB pURB
    )
    {

    /* Storage for the request results */
    USBHST_STATUS Result;

    /* If pURB is NULL return USBHST_INVALID_PARAMETER.*/
    if (NULL == pURB )
        {
        /* Debug Message */
        USB_HUB_ERR("invalid parameter \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of if (NULL == pURB ) */

    /*call ResetTT complete()*/

    Result =
    g_usbHstFunctionList.UsbdToHubFunctionList.resetTTComplete(pURB->nStatus,
                                                               pURB->hDevice,
                                                               pURB->pContext);

    if (USBHST_SUCCESS != Result)
        {
        /* Debug Message */
        USB_HUB_ERR("failed to reset TT, result=%d \n",
                    Result, 2, 3, 4, 5, 6);
        }

    /* free the memory allocated*/

    OS_FREE(pURB->pTransferSpecificData);

    /* free the memory allocated */

    OS_FREE(pURB);

    return Result;
    }/*end of HUB_ResetTTRequestCallback*/


/**************************** End of File HUB_Utility.c ***********************/


