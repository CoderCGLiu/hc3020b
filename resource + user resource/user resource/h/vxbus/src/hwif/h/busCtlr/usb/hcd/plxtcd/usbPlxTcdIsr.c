/* usbPlxTcdIsr.c - USB PLX TCD interrupt handler module */

/*
 * Copyright (c) 2012-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01g,03may13,wyy  Remove compiler warning (WIND00356717)
01f,21nov12,s_z  Add Reset exit workaround based Errata Issue_4
                 Reset squeuence number after endpoint halt (WIND00389964)
01e,23oct12,s_z  Add U1/U2 rejection workaround based Errata (WIND00382685)
01d,17oct12,s_z  Fix short packet receive issue (WIND00374594)
01c,10oct12,s_z  Avoid warm reset after resume (WIND00382688)
01b,18sep12,s_z  Pass CV2.0 testing (WIND00375402)
01a,18may12,s_z  Writen
*/

/*
DESCRIPTION

This file provides the interrupt handling routines for USB PLX Target
Controller Driver.

INCLUDE FILES: usbPlxTcd.h, usbPlxTcdIsr.h, usbPlxTcdUtil.h, usbPlxTcdDma.h

*/

/* includes */

#include <usbPlxTcd.h>
#include <usbPlxTcdIsr.h>
#include <usbPlxTcdUtil.h>
#include <usbPlxTcdDma.h>

#ifdef USB_PLX_SHOW_ENABLE
#include <usbPlxTcdDebug.c>
#endif

extern UINT8 usrUsbPlxTcdIsrTaskFlagGet
    (
    UINT8  uIndex /* The contorller instance index */
    );

/*******************************************************************************
*
* usbPlxTcdSuspendEventHandler - handle the suspend event of PLX TCD controller
*
* This routine is to handle the suspend event of PLX TCD controller.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbPlxTcdSuspendEventHandler
    (
    pUSBTGT_TCD pTcd
    )
    {
    pUSB_PLX_TCD_DATA pTCDData = NULL;
    pUSB_PLX_CTRL     pPlxCtrl = NULL;

    /* Validate parameters */

    if ((NULL == pTcd) ||
        (NULL == (pTCDData = (pUSB_PLX_TCD_DATA)pTcd->pTcdSpecific)) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTcd) ? "pTcd" :
                     (NULL == pTCDData) ? "pTcdSpecific" :
                     "pTCDData->pPlxCtrl"),
                     2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Update the device suspend status as TRUE */

    pTCDData->isDeviceSuspend = TRUE;

    USB_PLX_VDBG("usbPlxTcdSuspendEventHandler(): notify "
                 "suspend event\n",1, 2, 3, 4, 5, 6);

    /* Do not clear the USBSTAT_SUSPEND bit */

    /* Set root port wake up enable bit */

    USB_PLX_REG_WRITE32 (pPlxCtrl,
                         USB_PLX_USBCTL_REG,
                         USB_PLX_REG_READ32 (pPlxCtrl,
                         USB_PLX_USBCTL_REG) |
                         USB_PLX_USBCTL_REG_RPWE);

    /* Notify the suspend event */

    usbTgtTcdEventNotify(pTcd, USBTGT_NOTIFY_SUSPEND_EVENT);

    return OK;
    }

/*******************************************************************************
*
* usbPlxTcdResumeEventHandler - handle the resume event of PLX TCD controller
*
* This routine is to handle the resume event of PLX TCD controller.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbPlxTcdResumeEventHandler
    (
    pUSBTGT_TCD pTcd
    )
    {
    pUSB_PLX_TCD_DATA pTCDData = NULL;
    pUSB_PLX_CTRL     pPlxCtrl = NULL;

    /* Validate parameters */

    if ((NULL == pTcd) ||
        (NULL == (pTCDData = (pUSB_PLX_TCD_DATA)pTcd->pTcdSpecific)) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTcd) ? "pTcd" :
                     (NULL == pTCDData) ? "pTcdSpecific" :
                     "pTCDData->pPlxCtrl"),
                     2, 3, 4, 5, 6);

        return ERROR;
        }

    pTCDData->isDeviceSuspend = FALSE;

    USB_PLX_VDBG("usbPlxTcdDisconnectEventHandler(): notify "
                 "resume event\n",1, 2, 3, 4, 5, 6);

    /* Notify the resume event */

    usbTgtTcdEventNotify(pTcd, USBTGT_NOTIFY_RESUME_EVENT);

    return OK;
    }


/*******************************************************************************
*
* usbPlxTcdDisconnectEventHandler - handle the disconnect event of PLX TCD
*
* This routine is to handle the disconnect event of PLX TCD controller.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbPlxTcdDisconnectEventHandler
    (
    pUSBTGT_TCD pTcd
    )
    {
    pUSB_PLX_TCD_DATA pTCDData = NULL;
    pUSB_PLX_CTRL     pPlxCtrl = NULL;
    UINT32            uDevInit;

    /* Validate parameters */

    if ((NULL == pTcd) ||
        (NULL == (pTCDData = (pUSB_PLX_TCD_DATA)pTcd->pTcdSpecific)) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTcd) ? "pTcd" :
                     (NULL == pTCDData) ? "pTcdSpecific" :
                     "pTCDData->pPlxCtrl"),
                     2, 3, 4, 5, 6);

        return ERROR;
        }

    pTCDData->isSoftConnected = FALSE;

    /* Read the DEVINIT regsiter */

    uDevInit = USB_PLX_REG_READ32 (pPlxCtrl, USB_PLX_DEVINIT_REG);

    /* Set bit 4 of DEVINIT register to flush the FIFO */

    uDevInit |= USB_PLX_DEVINIT_FIFO_RESET | USB_PLX_DEVINIT_USB_RESET;

    USB_PLX_REG_WRITE32 (pPlxCtrl, USB_PLX_DEVINIT_REG, uDevInit);


    USB_PLX_VDBG("usbPlxTcdDisconnectEventHandler(): notify TML "
                 "disconnect event\n",1, 2, 3, 4, 5, 6);

    /* Notify the disconnect event */

    usbTgtTcdEventNotify(pTcd, USBTGT_NOTIFY_DISCONNECT_EVENT);

    return OK;
    }


/*******************************************************************************
*
* usbPlxTcdResetEventHandler - handle the reset event of the PLX TCD controller
*
* This routine is to handle the reset event of the PLX TCD controller.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbPlxTcdResetEventHandler
    (
    pUSBTGT_TCD pTcd
    )
    {
    pUSB_PLX_TCD_DATA pTCDData = NULL;
    pUSB_PLX_CTRL     pPlxCtrl = NULL;
    UINT32            uUsbStat;

    /* Validate parameters */

    if ((NULL == pTcd) ||
        (NULL == (pTCDData = (pUSB_PLX_TCD_DATA)pTcd->pTcdSpecific)) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTcd) ? "pTcd" :
                     (NULL == pTCDData) ? "pTcdSpecific" :
                     "pTCDData->pPlxCtrl"),
                     2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * Read the USB STATUS register and determine the speed
     * According to the internal testing, directlly read the
     * USB_PLX228X_USBSTAT_REG status immediately after root port reset,
     * this register returns 0.
     *
     * Waiting 200ms for the hardware registers synch up.
     *
     */

    taskDelay (OS_CONVERT_MILLISECONDS_TO_WAIT_VALUE(200));

    /* Get the hardware speed */

    pTcd->uSpeed = usbPlxTcdBusSpeedGet(pPlxCtrl);

    /* If the bus speed is ivalid, give a falk super speed */

    if (pTcd->uSpeed == 0xFF)
        {
        /* Check if the hardware suspend */

        uUsbStat = USB_PLX_REG_READ32 (pPlxCtrl,
                                      USB_PLX_USBSTAT_REG);

        if (uUsbStat & USB_PLX_USBSTAT_SUSPEND)
            {
            USB_PLX_REG_WRITE32 (pPlxCtrl,
                                 USB_PLX_USBSTAT_REG,
                                 USB_PLX_USBSTAT_SUSPEND);
            }

        pTcd->uSpeed = USB_SPEED_SUPER;
        }

    /* Update pTCD's device specific paramter */

    pTcd->uAddrToSet = 0;
    pTcd->uDeviceAddress = 0;

    /* Support self power don't support the remote wakeup */

    pTcd->DeviceInfo.uDeviceFeature = USB_DEV_STS_LOCAL_POWER;

    /* Set device status to default value */

    pTCDData->isDeviceSuspend = FALSE;
    pTCDData->isSoftConnected =  TRUE;
    pTCDData->ep0Stage = USB_TCD_EP0_STAGE_IDLE;

    /* Notify the reset event */

    usbTgtTcdEventNotify(pTcd, USBTGT_NOTIFY_RESET_EVENT);

    return OK;
    }

/*******************************************************************************
*
* usbPlxTcdEp0TxIrqHandler - process the control endpoint transaction interrupt
*
* This routine processes the control endpoint transaction interrupt.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbPlxTcdEp0TxIrqHandler
    (
    pUSB_PLX_TCD_DATA    pTCDData
    )
    {
    pUSBTGT_TCD          pTcd = NULL;
    pUSB_PLX_CTRL        pPlxCtrl = NULL;
    pUSB_PLX_TCD_PIPE    pTCDPipe = NULL;
    pUSB_PLX_TCD_REQUEST pRequest = NULL;

    /* Parameter verification */

    if ((NULL == pTCDData) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)) ||
        (NULL == (pTcd = pTCDData->pTcd)) ||
        (NULL == (pTCDPipe = (pUSB_PLX_TCD_PIPE)(pTcd->controlPipe))))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTCDData) ? "pTCDData" :
                     (NULL == pTCDData->pPlxCtrl) ? "pPlxCtrl" :
                     (NULL == pTcd) ? "pTcd" :
                     "pTCDPipe"), 2, 3, 4, 5, 6);
        return;
        }

    /* Get the first request */

    pRequest = usbPlxTcdFirstPendingReqGet(pTCDPipe);

    if ((NULL == pRequest) ||
        (USB_PID_IN != pRequest->uPid))
        {
        /* There is no request pending here */

        USB_PLX_DBG("usbPlxTcdEp0TxIrqHandler(): uEpRsp 0x%X uEpStat 0x%X "
                    "uEpAlail 0x%X ep0stats %d pRequest invalid\n",
                     pPlxCtrl->epReg[0].uEpRsp,
                     pPlxCtrl->epReg[0].uEpStat,
                     pPlxCtrl->epReg[0].uEpAvail,
                     pTCDData->ep0Stage, 5, 6);

        return;
        }

    /* Normally, the TX interrupt only happend for DATA/STATUS IN stage */

    if (pTCDData->ep0Stage == USB_TCD_EP0_STAGE_DATA_IN)
        {
        /* Update the transfer data lenght */

        pRequest->uActLength += pRequest->uCurXferLength;
        pRequest->pCurrentBuffer = pRequest->pCurrentBuffer +
                                   pRequest->uCurXferLength;

        if ((pRequest->uCurXferLength < pTCDPipe->uMaxPacketSize) ||
            (pRequest->uXferSize == pRequest->uActLength))
            {
            /* Complete the request */

            pTCDData->ep0Stage = USB_TCD_EP0_STAGE_SETUP;

            USB_PLX_DBG ("usbPlxTcdEp0TxIrqHandler callback %x\n",
                          pRequest->uActLength,2,3,4,5,6);

            (void) usbPlxTcdRequestComplete (pRequest,
                                             S_usbTgtTcdLib_ERP_RESTART);

            /* Enter the status stage */

            USB_PLX_REG_WRITE32 (pPlxCtrl,
                                 USB_PLX_EP_STAT_OFFSET(0),
                                 USB_PLX_REG_READ32 (pPlxCtrl,
                                 USB_PLX_EP_STAT_OFFSET(0)));

            USB_PLX_REG_WRITE32 (pPlxCtrl,
                                 USB_PLX_EP_RSP_OFFSET(0),
                                 USB_PLX_EP_RSP_CSPH);

            USB_PLX_DBG ("usbPlxTcdEp0TxIrqHandler after callback RSP %x STAT %x\n",
                    USB_PLX_REG_READ32 (pPlxCtrl,
                                 USB_PLX_EP_RSP_OFFSET(0)),
                    USB_PLX_REG_READ32 (pPlxCtrl,
                                 USB_PLX_EP_STAT_OFFSET(0)),3,4,5,6);

            }
        else
            {
            USB_PLX_VDBG("usbPlxTcdEp0TxIrqHandler():uXferSize 0x%X ack 0x%X\n",
                          pRequest->uXferSize, pRequest->uActLength,
                          3, 4, 5, 6);

            pRequest->uCurXferLength = min (pTCDPipe->uMaxPacketSize,
                       pRequest->uXferSize - pRequest->uActLength);

            USB_PLX_VDBG ("usbPlxTcdEp0TxIrqHandler():Write next packet %x %x \n",
                           1, 2, 3, 4, 5, 6);

            /*
             * NOTE: Within the code clean,
             * remove the FIFO fulsh for EP0
             * which is set 0x200 for EP_STAT (0)
             */

            usbPlxTcdFIFOWrite(pPlxCtrl,
                               pRequest->pCurrentBuffer,
                               pRequest->uCurXferLength,
                               0);
            }
        }
    else if (pTCDData->ep0Stage == USB_TCD_EP0_STAGE_STATUS_IN)
        {
        /* In the status IN stage, callback */

        pTCDData->ep0Stage = USB_TCD_EP0_STAGE_SETUP;

        (void) usbPlxTcdRequestComplete (pRequest,
                                         S_usbTgtTcdLib_ERP_SUCCESS);

        }
    return;
    }

/*******************************************************************************
*
* usbPlxTcdEp0RxIrqHandler - process the control endpoint receive interrupt
*
* This routine processes the control endpoint receive interrupt.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbPlxTcdEp0RxIrqHandler
    (
    pUSB_PLX_TCD_DATA  pTCDData
    )
    {
    pUSBTGT_TCD          pTcd = NULL;
    pUSB_PLX_CTRL        pPlxCtrl = NULL;
    pUSB_PLX_TCD_PIPE    pTCDPipe = NULL;
    pUSB_PLX_TCD_REQUEST pRequest = NULL;

    /* Parameter verification */

    if ((NULL == pTCDData) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)) ||
        (NULL == (pTcd = pTCDData->pTcd)) ||
        (NULL == (pTCDPipe = (pUSB_PLX_TCD_PIPE)(pTcd->controlPipe))))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTCDData) ? "pTCDData" :
                     (NULL == pTCDData->pPlxCtrl) ? "pPlxCtrl" :
                     (NULL == pTcd) ? "pTcd" :
                     "pTCDPipe"), 2, 3, 4, 5, 6);

        return;
        }

    /* Get the first request */

    pRequest = usbPlxTcdFirstPendingReqGet(pTCDPipe);

    if (NULL == pRequest)
        {
        /* There is no request pending here */

        USB_PLX_VDBG("usbPlxTcdEp0RxIrqHandler(): uEpRsp 0x%X uEpStat 0x%X "
                     "uEpAlail 0x%X ep0stats %d with No pRequest\n",
                     pPlxCtrl->epReg[0].uEpRsp,
                     pPlxCtrl->epReg[0].uEpStat,
                     pPlxCtrl->epReg[0].uEpAvail,
                     pTCDData->ep0Stage, 5, 6);

        /* Update the status */

        if (pTCDData->ep0Stage == USB_TCD_EP0_STAGE_DATA_OUT)
            {
            pTCDData->ep0Stage = USB_TCD_EP0_STAGE_DATA_OUT_PEND;
            }
        else if (pTCDData->ep0Stage == USB_TCD_EP0_STAGE_STATUS_OUT)
            {
            pTCDData->ep0Stage = USB_TCD_EP0_STAGE_STATUS_OUT_PEND;
            }

        return;
        }

    /*
     * Normally, the RX interrupt only happend for DATA/STATUS OUT
     * stage
     */

    if (pTCDData->ep0Stage == USB_TCD_EP0_STAGE_DATA_OUT)
        {
        pRequest->uCurXferLength = usbPlxTcdFIFORead(pPlxCtrl,
                          pRequest->pCurrentBuffer,
                          pRequest->uXferSize - pRequest->uActLength,
                          0);

        /* Update the request act data length */

        pRequest->uActLength += pRequest->uCurXferLength;
        pRequest->pCurrentBuffer = pRequest->pCurrentBuffer +
                                  pRequest->uCurXferLength;

        if ((pRequest->uCurXferLength < pTCDPipe->uMaxPacketSize) ||
            (pRequest->uActLength == pRequest->uXferSize))
            {
            /* The end of the data packet, callback */

            pTCDData->ep0Stage = USB_TCD_EP0_STAGE_SETUP;

            (void) usbPlxTcdRequestComplete (pRequest,
                                             S_usbTgtTcdLib_ERP_RESTART);

            USB_PLX_VDBG ("usbPlxTcdEp0RxIrqHandler uEpStat %x\n",
                          pPlxCtrl->epReg[0].uEpStat, 2, 3, 4, 5, 6);

            /* Clean the endpoint status */

            USB_PLX_REG_WRITE32 (pPlxCtrl,
                                 USB_PLX_EP_STAT_OFFSET(0),
                                 pPlxCtrl->epReg[0].uEpStat);

            /* Move in the status stage */
            USB_PLX_REG_WRITE32 (pPlxCtrl,
                 USB_PLX_EP_RSP_OFFSET(0),
                 USB_PLX_EP_RSP_CSPH);

            }
        else
            {
            /* Simply clean the status and wait for another packet */

            USB_PLX_REG_WRITE32 (pPlxCtrl,
                                 USB_PLX_EP_STAT_OFFSET(0),
                                 pPlxCtrl->epReg[0].uEpStat);
            }
        }
    else if (pTCDData->ep0Stage == USB_TCD_EP0_STAGE_STATUS_OUT)
        {
        /* Status OUT stage, callback */

        pTCDData->ep0Stage = USB_TCD_EP0_STAGE_SETUP;

        (void) usbPlxTcdRequestComplete (pRequest,
                                         S_usbTgtTcdLib_ERP_SUCCESS);

        }
    else if (pTCDData->ep0Stage == USB_TCD_EP0_STAGE_DATA_IN)
        {
        /* The host try to stop, callback */

        pTCDData->ep0Stage = USB_TCD_EP0_STAGE_SETUP;

        (void) usbPlxTcdRequestComplete (pRequest,
                                         S_usbTgtTcdLib_ERP_RESTART);
        }
    else if (pTCDData->ep0Stage == USB_TCD_EP0_STAGE_STATUS_IN)
        {
        /* It should not happen, callback */

        pTCDData->ep0Stage = USB_TCD_EP0_STAGE_SETUP;

        (void) usbPlxTcdRequestComplete (pRequest,
                                         S_usbTgtTcdLib_ERP_RESTART);
        }

    return;
    }


/*******************************************************************************
*
* usbPlxTcdEp0SetupIrqHandler - process the control endpoint setup interrupt
*
* This routine processes the control endpoint setup interrupt.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbPlxTcdEp0SetupIrqHandler
    (
    pUSB_PLX_TCD_DATA  pTCDData
    )
    {
    pUSBTGT_TCD          pTcd = NULL;
    pUSB_PLX_CTRL        pPlxCtrl = NULL;
    pUSB_PLX_TCD_PIPE    pTCDPipe = NULL;
    pUSB_PLX_TCD_REQUEST pRequest = NULL;
    USB_SETUP *          pSetup = NULL;
    UINT32               uSetup0123;
    UINT32               uSetup4567;
    UINT8                uAddr = USB_PLX_OURADDR_REG_FI;

    /* Parameter verification */

    if ((NULL == pTCDData) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)) ||
        (NULL == (pTcd = pTCDData->pTcd)) ||
        (NULL == (pTCDPipe = (pUSB_PLX_TCD_PIPE)(pTcd->controlPipe))))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTCDData) ? "pTCDData" :
                     (NULL == pTCDData->pPlxCtrl) ? "pPlxCtrl" :
                     (NULL == pTcd) ? "pTcd" :
                     "pTCDPipe"), 2, 3, 4, 5, 6);
        return;
        }

    /* Get the first request */

    pRequest = usbPlxTcdFirstPendingReqGet(pTCDPipe);

    if (NULL == pRequest)
        {
        /* There is no request pending here */

        USB_PLX_ERR("usbPlxTcdEp0SetupIrqHandler(): uEpRsp 0x%X uEpStat 0x%X "
                    "uEpAlail 0x%X ep0stats %d with NO pRequest\n",
                     pPlxCtrl->epReg[0].uEpRsp,
                     pPlxCtrl->epReg[0].uEpStat,
                     pPlxCtrl->epReg[0].uEpAvail,
                     pTCDData->ep0Stage, 5, 6);

        pTCDData->ep0Stage = USB_TCD_EP0_STAGE_SETUP_PEND;

        return;
        }

    if ((pTCDData->ep0Stage != USB_TCD_EP0_STAGE_SETUP) &&
        (pTCDData->ep0Stage != USB_TCD_EP0_STAGE_IDLE))
        {
        /* Not in the setup stage, callback */

        pTCDData->ep0Stage = USB_TCD_EP0_STAGE_SETUP;

        (void) usbPlxTcdRequestComplete (pRequest,
                                         S_usbTgtTcdLib_ERP_RESTART);

        /* Re-load the setup packet request*/

        pRequest = usbPlxTcdFirstPendingReqGet(pTCDPipe);

        if (NULL == pRequest)
            {
            /* There is no request pending here */

            USB_PLX_ERR("usbPlxTcdEp0SetupIrqHandler():Re-load the setup\n"
                        "request fail ep0Stage %d\n",
                        pTCDData->ep0Stage, 2, 3, 4, 5, 6);

            return;
            }
        }

    /* Clear the stall bit if get one setup packet */

    if (USB_PLX_EP_RSP_STALL & USB_PLX_REG_READ32 (pPlxCtrl,
                             USB_PLX_EP_RSP_OFFSET(0)))
        {
        /* Clear the stall bit */

        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_EP_RSP_OFFSET(0),
                             USB_PLX_EP_RSP_CLEAR(USB_PLX_EP_RSP_STALL));
        }

    pSetup = (USB_SETUP *)pRequest->pCurrentBuffer;

    uSetup0123 = USB_PLX_REG_READ32(pPlxCtrl,
                                    USB_PLX_SETUP0123_REG);

    *(UINT32 *)pRequest->pCurrentBuffer =
                 OS_UINT32_LE_TO_CPU(uSetup0123);

    /* Update the lenght and get other setup packet data */

    pRequest->uActLength += 4;
    pRequest->pCurrentBuffer += pRequest->uActLength;

    uSetup4567 = USB_PLX_REG_READ32(pPlxCtrl,
                                    USB_PLX_SETUP4567_REG);

    *(UINT32 *)pRequest->pCurrentBuffer =
                 OS_UINT32_LE_TO_CPU(uSetup4567 );

    /* Update the data length */

    pRequest->uActLength += 4;
    pRequest->pCurrentBuffer += pRequest->uActLength;

    USB_PLX_VDBG("usbPlxTcdEp0SetupIrqHandler(): Setup PKT "
                  "[0x%02X 0x%02X 0x%04X 0x%04X 0x%04X]\n",
                  pSetup->requestType,
                  pSetup->request,
                  pSetup->value,
                  pSetup->index,
                  pSetup->length, 6);

    /* Call the workaround before DATA stage of the first control READ */

    usbPlxTcdLgoUxWorkaround(pPlxCtrl);

    /* Update the Ep0 status */

    if (pSetup->length)
        {
        if (pSetup->requestType & USB_RT_DEV_TO_HOST)
            {
            /* Data needed from the device to the Host */

            pTCDData->ep0Stage = USB_TCD_EP0_STAGE_DATA_IN;
            }
        else
            {
            /* Data will issued from the host to the device */

            pTCDData->ep0Stage = USB_TCD_EP0_STAGE_DATA_OUT;
            }

        /* Complete the request */

        (void) usbPlxTcdRequestComplete (pRequest,
                                         S_usbTgtTcdLib_ERP_SUCCESS);

        if (pTCDData->ep0Stage == USB_TCD_EP0_STAGE_SETUP)
            {
            USB_PLX_REG_WRITE32 (pPlxCtrl,
                                 USB_PLX_EP_RSP_OFFSET(0),
                                 USB_PLX_EP_RSP_CSPH);
            }

        }
    else
        {
        /* No data stage, waiting the status in stage */

        pTCDData->ep0Stage = USB_TCD_EP0_STAGE_SETUP;

        if ((USB_REQ_SET_ADDRESS == pSetup->request) &&
            ((pSetup->requestType & USB_RT_CATEGORY_MASK) == USB_RT_STANDARD))
            {
            uAddr = (UINT8)FROM_LITTLEW (pSetup->value);
            }

        /* Complete the request */

        (void) usbPlxTcdRequestComplete (pRequest,
                                         S_usbTgtTcdLib_ERP_RESTART);

        /* Clear the status */

        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_EP_STAT_OFFSET(0),
                             pPlxCtrl->epReg[0].uEpStat);

        /* Set address ? */

        if (USB_PLX_OURADDR_REG_FI != uAddr)
            {
            USB_PLX_REG_WRITE32 (pPlxCtrl,
                                 USB_PLX_OURADDR_REG,
                                  uAddr);
            }

        /* Move to the status stage */

        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_EP_RSP_OFFSET(0),
                             USB_PLX_EP_RSP_CSPH);
        }

    USB_PLX_VDBG("usbPlxTcdEp0SetupIrqHandler Done ep0Stage %d\n",
                 pTCDData->ep0Stage, 2, 3, 4, 5, 6);

    return;
    }


/*******************************************************************************
*
* usbPlxTcdEp0InterruptHandler - process the control endpoint interrupts
*
* This routine processes the control endpoint interrupts according to the
* setup interrupt and endpoint 0 interrupt status.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbPlxTcdEp0InterruptHandler
    (
    pUSB_PLX_TCD_DATA  pTCDData,
    UINT32             uIrqStat0
    )
    {
    pUSBTGT_TCD          pTcd = NULL;
    pUSB_PLX_CTRL        pPlxCtrl = NULL;
    pUSB_PLX_TCD_PIPE    pTCDPipe = NULL;
    BOOL                 bProcessRx = FALSE;
    BOOL                 bProcessTx = FALSE;

    /* Parameter verification */

    if ((NULL == pTCDData) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)) ||
        (NULL == (pTcd = pTCDData->pTcd)) ||
        (NULL == (pTCDPipe = (pUSB_PLX_TCD_PIPE)(pTcd->controlPipe))))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTCDData) ? "pTCDData" :
                     (NULL == pTCDData->pPlxCtrl) ? "pPlxCtrl" :
                     (NULL == pTcd) ? "pTcd" :
                     "pTCDPipe"), 2, 3, 4, 5, 6);
        return;
        }

    /*
     * This indicate the transaction should switch to another stage
     *
     * 1. setup packet received
     * 2. DATA IN OK, switch to STATUS OUT
     * 3. DATA OUT OK, switch to STATUS OUT
     */

    /* Only check the setup interrupt and EP0 interrupt */

    uIrqStat0 &= USB_PLX_IRQENB0_SETUP | USB_PLX_IRQENB0_EP(0);

    USB_PLX_DBG ("usbPlxTcdEp0InterruptHandler uEpRsp %x uEpStat %x "
                  "uEpAvail %X uIrqStat0 %x ep0Stage %x\n",
                  pPlxCtrl->epReg[0].uEpRsp,
                  pPlxCtrl->epReg[0].uEpStat,
                  pPlxCtrl->epReg[0].uEpAvail,
                  uIrqStat0, pTCDData->ep0Stage, 6);

    /* Only get the SETUP interrupt */

    if (USB_PLX_IRQENB0_SETUP == uIrqStat0)
        {
        usbPlxTcdEp0SetupIrqHandler(pTCDData);

        /* Also have data in the buffer ? */

        if (USB_PLX_EP_STAT_DPR & pPlxCtrl->epReg[0].uEpStat)
            {
            /* Data Received */

            USB_PLX_VDBG ("usbPlxTcdEp0InterruptHandler(): Data Received \n",
                          1,2,3,4,5,6);

            usbPlxTcdEp0RxIrqHandler(pTCDData);
            }
        }
    else if (USB_PLX_IRQENB0_EP(0) == uIrqStat0)
        {
        /* Get the endpoint transaction interrupt */

        if ((USB_PLX_EP_STAT_DPT | USB_PLX_EP_STAT_SPT ) &
             pPlxCtrl->epReg[0].uEpStat)
            {
            /* Data Transferd */

            USB_PLX_VDBG ("usbPlxTcdEp0InterruptHandler(): Data Transferd \n",
                          1,2,3,4,5,6);

            usbPlxTcdEp0TxIrqHandler(pTCDData);
            }

       if (USB_PLX_EP_STAT_DPR & pPlxCtrl->epReg[0].uEpStat)
            {
            /* Data Received */

            USB_PLX_VDBG ("usbPlxTcdEp0InterruptHandler(): Data Received \n",
                          1,2,3,4,5,6);

            usbPlxTcdEp0RxIrqHandler(pTCDData);
            }
        }
    else if (uIrqStat0 == (USB_PLX_IRQENB0_SETUP | USB_PLX_IRQENB0_EP(0)))
        {
        /* Next step we will do */


        if ((USB_PLX_EP_STAT_DPR & pPlxCtrl->epReg[0].uEpStat) ||
            (USB_PLX_EP_RSP_NAKOUT & pPlxCtrl->epReg[0].uEpRsp) ||
            (USB_PLX_EP_STAT_OUTACKSNT & pPlxCtrl->epReg[0].uEpStat))
            {
            bProcessRx = TRUE;
            }

        if ((USB_PLX_EP_STAT_DPT & pPlxCtrl->epReg[0].uEpStat)||
            (USB_PLX_EP_STAT_INACKRCV & pPlxCtrl->epReg[0].uEpStat))
            {
            bProcessTx = TRUE;
            }

        usbPlxTcdEp0SetupIrqHandler(pTCDData);

        if (bProcessRx)
            {
            /* Data Received */

            USB_PLX_VDBG ("usbPlxTcdEp0InterruptHandler(): Data Received \n",
                          1,2,3,4,5,6);

            usbPlxTcdEp0RxIrqHandler(pTCDData);
            }

        if (bProcessTx)
            {
            /* Data Transferd */

            USB_PLX_VDBG ("usbPlxTcdEp0InterruptHandler(): Data Transferd \n",
                          1,2,3,4,5,6);

            usbPlxTcdEp0TxIrqHandler(pTCDData);
            }
        }

    USB_PLX_VDBG ("usbPlxTcdEp0InterruptHandler done \n", 1,2,3,4,5,6);

    return;
    }

/*******************************************************************************
*
* usbPlxTcdTxDmaIrqHandler - process the TX DMA interrupt
*
* This routine processes the TX DMA interrupt.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbPlxTcdTxDmaIrqHandler
    (
    pUSB_PLX_TCD_DATA pTCDData,
    pUSB_PLX_TCD_PIPE pTCDPipe
    )
    {
    pUSB_PLX_CTRL        pPlxCtrl = NULL;
    pUSB_PLX_TCD_REQUEST pRequest = NULL;
    UINT32               uEpAvail;
    UINT32               uEpStat;
    UINT8                uEpIndex;

    /* Parameter verification */

    if ((NULL == pTCDData) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)) ||
        (NULL == pTCDPipe))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTCDData) ? "pTCDData" :
                     (NULL == pTCDData->pPlxCtrl) ? "pPlxCtrl" :
                     "pTCDPipe"), 2, 3, 4, 5, 6);
        return;
        }

    /* Get the first request */

    pRequest = usbPlxTcdFirstPendingReqGet (pTCDPipe);

    if (NULL == pRequest)
        {
        USB_PLX_WARN("usbPlxTcdTxDmaIrqHandler(): No Request pending\n",
                     1, 2, 3, 4, 5, 6);

        return;
        }

    uEpIndex = pTCDPipe->uPhyEpAddr;

    pRequest->bDmaActive = FALSE;

    /*
     * NOTE: Polling will be needed to check
     * if all the data has been transfered to the USB host
     * MARKed Ad TODO
     */

    uEpStat = USB_PLX_REG_READ32 (pPlxCtrl,
                                  USB_PLX_EP_STAT_OFFSET(uEpIndex));

    uEpAvail = USB_PLX_REG_READ32 (pPlxCtrl,
                                   USB_PLX_EP_AVAIL_OFFSET(uEpIndex));

    USB_PLX_VDBG("usbPlxTcdTxDmaIrqHandler(): uEpStat %x uEpAvail %x DMAADDR %x C %x\n",
                  uEpStat, uEpAvail,
                  USB_PLX_REG_READ32 (pPlxCtrl,
                             USB_PLX_DMAADDR_OFFSET(pTCDPipe->uDmaChannel)),
                  USB_PLX_REG_READ32 (pPlxCtrl,
                             USB_PLX_DMACOUNT_OFFSET(pTCDPipe->uDmaChannel)),
                  5, 6);

    /*
     * Update the transfer data lenght
     * NOTE: There is possible that, some data still in the FIFO and
     * have not tranfer to the HOST yet, but this will not affect start
     * another DMA transaction from the user buffer to the FIFO.
     * The hardware will issue the data from the FIFO to the USB HOST.
     */

    pRequest->uActLength += pRequest->uCurXferLength;
    pRequest->pCurrentBuffer = pRequest->pCurrentBuffer +
                               pRequest->uCurXferLength;

    OS_WAIT_FOR_EVENT(pTCDPipe->pPipeSyncMutex, WAIT_FOREVER);

    pTCDPipe->status &= ~USBTGT_PIPE_STATUS_PROCESSING;

    OS_RELEASE_EVENT(pTCDPipe->pPipeSyncMutex);

    /* TODO : ZLP needed ?????*/

    if ((pRequest->uCurXferLength < pTCDPipe->uMaxPacketSize) ||
        (pRequest->uXferSize == pRequest->uActLength))
        {
        /* Complete the request */

        USB_PLX_VDBG ("usbPlxTcdTxDmaIrqHandler callback uActLength %x uXferSize %x\n",
                      pRequest->uActLength,pRequest->uXferSize,3,4,5,6);

        (void) usbPlxTcdRequestComplete (pRequest,
                                         S_usbTgtTcdLib_ERP_SUCCESS);
        }

    /* Get the first pending request after complete */

    pRequest = usbPlxTcdFirstPendingReqGet (pTCDPipe);

    if (NULL != pRequest)
        {
        (void)usbPlxTcdPipeProcessStart (pTCDData, pTCDPipe);
        }

    return;
    }


/*******************************************************************************
*
* usbPlxTcdTxEpIrqHandler - process the general endpoint transfer interrupt
*
* This routine processes the general endpoint transfer interrupt, not control
* endpoint.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbPlxTcdTxEpIrqHandler
    (
    pUSB_PLX_TCD_DATA pTCDData,
    pUSB_PLX_TCD_PIPE pTCDPipe
    )
    {
    pUSB_PLX_CTRL        pPlxCtrl = NULL;
    pUSB_PLX_TCD_REQUEST pRequest = NULL;
    UINT8                uEpIndex;
    UINT32               uEpStat;
    UINT32               uEpAvail;

    /* Parameter verification */

    if ((NULL == pTCDData) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)) ||
        (NULL == pTCDPipe))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTCDData) ? "pTCDData" :
                     (NULL == pTCDData->pPlxCtrl) ? "pPlxCtrl" :
                     "pTCDPipe"), 2, 3, 4, 5, 6);
        return;
        }

    /* Get the first request */

    pRequest = usbPlxTcdFirstPendingReqGet (pTCDPipe);

    if (NULL == pRequest)
        {
        USB_PLX_VDBG("usbPlxTcdTxEpIrqHandler(): No Request pending\n",
                     1, 2, 3, 4, 5, 6);

        return;
        }

    uEpIndex = pTCDPipe->uPhyEpAddr;

    uEpStat = USB_PLX_REG_READ32 (pPlxCtrl,
                                  USB_PLX_EP_STAT_OFFSET(uEpIndex));

    uEpAvail = USB_PLX_REG_READ32 (pPlxCtrl,
                                   USB_PLX_EP_AVAIL_OFFSET(uEpIndex));

    USB_PLX_VDBG("usbPlxTcdTxEpIrqHandler(): uEpStat %x uEpAvail %x Ep %x [%x]\n",
                 uEpStat, uEpAvail, uEpIndex, pPlxCtrl->epReg[uEpIndex].uEpStat, 5, 6);

    /* This is DATA IN endpoint, check the DPT */

    if (pPlxCtrl->epReg[uEpIndex].uEpStat & USB_PLX_EP_STAT_DPT)
        {
        /* Update the transfer data lenght */

        pRequest->uActLength += pRequest->uCurXferLength;
        pRequest->pCurrentBuffer = pRequest->pCurrentBuffer +
                                   pRequest->uCurXferLength;

        /* TODO : ZLP needed ?????*/

        if ((pRequest->uCurXferLength < pTCDPipe->uMaxPacketSize) ||
            (pRequest->uXferSize == pRequest->uActLength))
            {
            /* Complete the request */

            USB_PLX_VDBG ("usbPlxTcdTxEpIrqHandler(): callback "
                          "[Cur %x Max %x Act %x Xfer %x ]\n",
                          pRequest->uCurXferLength,
                          pTCDPipe->uMaxPacketSize,
                          pRequest->uActLength,
                          pRequest->uXferSize, 5, 6);

            (void) usbPlxTcdRequestComplete (pRequest,
                                             S_usbTgtTcdLib_ERP_SUCCESS);

            /* If there is another request pending, issue it */

            pRequest = usbPlxTcdFirstPendingReqGet (pTCDPipe);

            if (NULL != pRequest)
                {
                (void)usbPlxTcdPipeProcessStart(pTCDData, pTCDPipe);
                }
            }
        else
            {
            /* Issue more date to transfer */

            pRequest->uCurXferLength = min (pTCDPipe->uMaxPacketSize,
                       pRequest->uXferSize - pRequest->uActLength);

            USB_PLX_VDBG ("usbPlxTcdTxEpIrqHandler(): Issue more data %x\n",
                          pRequest->uCurXferLength, 2, 3, 4, 5, 6);

            usbPlxTcdFIFOWrite(pPlxCtrl,
                               pRequest->pCurrentBuffer,
                               pRequest->uCurXferLength,
                               uEpIndex);
            }
        }

    return;
    }

/*******************************************************************************
*
* usbPlxTcdRxDmaIrqHandler - process the RX DMA interrupt
*
* This routine processes the RX DMA interrupt.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbPlxTcdRxDmaIrqHandler
    (
    pUSB_PLX_TCD_DATA pTCDData,
    pUSB_PLX_TCD_PIPE pTCDPipe
    )
    {
    pUSB_PLX_CTRL        pPlxCtrl = NULL;
    pUSB_PLX_TCD_REQUEST pRequest = NULL;
    UINT32               uEpAvail;
    UINT32               uEpStat;
    UINT32               uCount;
    UINT8                uEpIndex;

    /* Parameter verification */

    if ((NULL == pTCDData) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)) ||
        (NULL == pTCDPipe))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTCDData) ? "pTCDData" :
                     (NULL == pTCDData->pPlxCtrl) ? "pPlxCtrl" :
                     "pTCDPipe"), 2, 3, 4, 5, 6);
        return;
        }

    /* Get the first request */

    pRequest = usbPlxTcdFirstPendingReqGet (pTCDPipe);

    /* Get the endpoint index which asigned */

    uEpIndex = pTCDPipe->uPhyEpAddr;

    if (NULL == pRequest)
        {
        USB_PLX_VDBG("usbPlxTcdRxDmaIrqHandler(): No Request pending\n",
                     1, 2, 3, 4, 5, 6);

        /* TODO: protect */

        pTCDPipe->status |= USBTGT_PIPE_STATUS_DATA_PENDING;

        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_EP_IRQENB_OFFSET(uEpIndex),
                             USB_PLX_EP_IRQENB_DPR);

        USB_PLX_REG_BIT_SET(pPlxCtrl,
                            USB_PLX_PCIIRQENB0_REG,
                            USB_PLX_XIRQENB0_EP(uEpIndex));
        return;
        }

    /* In the DMA process handler routine, DMA is not actioved */

    pRequest->bDmaActive = FALSE;

    /* Check the count which transfered by DMA */

    uCount = USB_PLX_REG_READ32 (pPlxCtrl,
                                 USB_PLX_DMACOUNT_OFFSET(uEpIndex));

    uCount &= USB_PLX_DMACOUNT_BC;

    uEpAvail = USB_PLX_REG_READ32 (pPlxCtrl,
                                   USB_PLX_EP_AVAIL_OFFSET(uEpIndex));


    USB_PLX_DBG ("usbPlxTcdRxDmaIrqHandler():Need %x left %x uEpAvail %x\n",
                  pRequest->uCurXferLength, uCount, uEpAvail, 4, 5, 6);

    /* Get the length of data transfered */

    if (pRequest->uCurXferLength < uCount)
        {
        return;
        }

    pRequest->uCurXferLength = pRequest->uCurXferLength - uCount;

    /* Update the actual length */

    pRequest->uActLength += pRequest->uCurXferLength;
    pRequest->pCurrentBuffer = pRequest->pCurrentBuffer +
                              pRequest->uCurXferLength;

    if (uEpAvail)
        {
        pRequest->uCurXferLength = usbPlxTcdFIFORead(pPlxCtrl,
                                             pRequest->pCurrentBuffer,
                                             uEpAvail,
                                             uEpIndex);
        pRequest->uActLength += pRequest->uCurXferLength;
        pRequest->pCurrentBuffer = pRequest->pCurrentBuffer +
                                  pRequest->uCurXferLength;
        }

    uEpStat = USB_PLX_REG_READ32 (pPlxCtrl,
                             USB_PLX_EP_STAT_OFFSET(uEpIndex));

    /* The end of the packet? */

    if ((pRequest->uCurXferLength % pTCDPipe->uMaxPacketSize) ||
        (pRequest->uActLength == pRequest->uXferSize))
        {
        USB_PLX_DBG ("usbPlxTcdRxDmaIrqHandler():Callback uActLength %x Xfer %x\n",
                      pRequest->uActLength, pRequest->uXferSize,
                      3, 4, 5, 6);

        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_EP_STAT_OFFSET(uEpIndex),
                             uEpStat);

        (void) usbPlxTcdRequestComplete (pRequest,
                                         S_usbTgtTcdLib_ERP_SUCCESS);

        if (pTCDData->isAlaysShortPkt == FALSE)
            {
            /* Directly RX DMA, re-schedule and return */

            return;
            }
        }

    /* Get the endpoint status register */

    uEpStat = USB_PLX_REG_READ32 (pPlxCtrl,
                             USB_PLX_EP_STAT_OFFSET(uEpIndex));

    /* Check if there are still data in the FIFO */

    uEpAvail = USB_PLX_REG_READ32 (pPlxCtrl,
                                   USB_PLX_EP_AVAIL_OFFSET(uEpIndex));

    USB_PLX_DBG ("usbPlxTcdRxDmaIrqHandler uEpAvail %x STAT %x\n",
                  uEpAvail, uEpStat, 3, 4, 5, 6);

    /* Check if there is another short packet in the FIFO */

    if ((0 == (uEpAvail % pTCDPipe->uMaxPacketSize)) &&
        (0 == (uEpStat & USB_PLX_EP_STAT_NAKOUT)))
        {
        /* No there is no short packet in the FIFO */

        /* Is there any data ? */

        if (0 == uEpAvail)
            {
            /* No data in the FIFO, Enable data receive interrupt and return */

            USB_PLX_REG_WRITE32 (pPlxCtrl,
                                 USB_PLX_EP_IRQENB_OFFSET(pTCDPipe->uPhyEpAddr),
                                 USB_PLX_EP_IRQENB_DPR);

            USB_PLX_REG_BIT_SET(pPlxCtrl,
                                USB_PLX_PCIIRQENB0_REG,
                                USB_PLX_XIRQENB0_EP(uEpIndex));

            USB_PLX_REG_WRITE32 (pPlxCtrl,
                     USB_PLX_EP_STAT_OFFSET(uEpIndex),
                     uEpStat );
            return;
            }
        else
            {
            /* Still some data pending in the FIFO to be read */

            if (NULL != (pRequest = usbPlxTcdFirstPendingReqGet (pTCDPipe)))
                {
                pRequest->uCurXferLength = min (uEpAvail,
                                   pRequest->uXferSize - pRequest->uActLength);;

                USB_PLX_DBG ("usbPlxTcdRxDmaIrqHandler(): Restart DMA "
                               "uEpAvail %x STAT %x\n",
                               uEpAvail, uEpStat, 3, 4, 5, 6);

                (void)usbPlxTcdDmaStart (pTCDData, pTCDPipe, pRequest);

                return;
                }
            else
                {
                /* No request pending */

                pTCDPipe->status |= USBTGT_PIPE_STATUS_DATA_PENDING;
                USB_PLX_REG_WRITE32 (pPlxCtrl,
                                     USB_PLX_EP_IRQENB_OFFSET(pTCDPipe->uPhyEpAddr),
                                     USB_PLX_EP_IRQENB_DPR);

                USB_PLX_REG_BIT_SET(pPlxCtrl,
                                    USB_PLX_PCIIRQENB0_REG,
                                    USB_PLX_XIRQENB0_EP(uEpIndex));

                USB_PLX_REG_WRITE32 (pPlxCtrl,
                         USB_PLX_EP_STAT_OFFSET(uEpIndex),
                         uEpStat );
                return;
                }
            }

        }
    else
        {
        /* Short packet in the FIFO */

        if ((uEpStat & USB_PLX_EP_STAT_NAKOUT) &&
            (0 == uEpAvail) &&
            (NULL != (pRequest = usbPlxTcdFirstPendingReqGet (pTCDPipe))))
            {
            /* Get one ZLP */

            USB_PLX_REG_WRITE32 (pPlxCtrl,
                                 USB_PLX_EP_IRQENB_OFFSET(pTCDPipe->uPhyEpAddr),
                                 USB_PLX_EP_IRQENB_DPR);

            USB_PLX_REG_BIT_SET(pPlxCtrl,
                                USB_PLX_PCIIRQENB0_REG,
                                USB_PLX_XIRQENB0_EP(uEpIndex));

            USB_PLX_REG_WRITE32 (pPlxCtrl,
                                 USB_PLX_EP_STAT_OFFSET(uEpIndex),
                                 uEpStat);

            USB_PLX_DBG ("usbPlxTcdRxDmaIrqHandler():ZLP\n", 1,2,3,4,5,6);

            (void) usbPlxTcdRequestComplete (pRequest,
                                         S_usbTgtTcdLib_ERP_SUCCESS);

            return;
            }

        /* Not the ZLP */

        if (NULL != (pRequest = usbPlxTcdFirstPendingReqGet (pTCDPipe)))
            {
            while ((NULL != pRequest) &&
                  (0 != uEpAvail))
                {
                uEpAvail = min (uEpAvail,
                                pRequest->uXferSize - pRequest->uActLength);
                /* Using the PIO mode to get the data */

                pRequest->uCurXferLength = usbPlxTcdFIFORead(pPlxCtrl,
                                                     pRequest->pCurrentBuffer,
                                                     uEpAvail,
                                                     uEpIndex);
                /* Re-check the FIFO */

                uEpAvail = USB_PLX_REG_READ32 (pPlxCtrl,
                                   USB_PLX_EP_AVAIL_OFFSET(uEpIndex));
                uEpStat = USB_PLX_REG_READ32 (pPlxCtrl,
                                         USB_PLX_EP_STAT_OFFSET(uEpIndex));

                USB_PLX_DBG ("usbPlxTcdRxDmaIrqHandler pio avail %x stata %x\n",
                              uEpAvail, uEpStat, 3,4,5,6);

                /* Update the act length */

                pRequest->uActLength += pRequest->uCurXferLength;
                pRequest->pCurrentBuffer = pRequest->pCurrentBuffer +
                                              pRequest->uCurXferLength;

                if ((pRequest->uCurXferLength % pTCDPipe->uMaxPacketSize) ||
                    (pRequest->uActLength == pRequest->uXferSize))
                    {
                    USB_PLX_DBG ("usbPlxTcdRxDmaIrqHandler() Callback Act %x "
                                  "Xfer %x uEpAvail %x\n",
                                  pRequest->uActLength, pRequest->uXferSize,
                                  uEpAvail, 4, 5, 6);

                    if (0 == uEpAvail)
                        {

                        USB_PLX_REG_WRITE32 (pPlxCtrl,
                                 USB_PLX_EP_IRQENB_OFFSET(pTCDPipe->uPhyEpAddr),
                                 USB_PLX_EP_IRQENB_DPR);

                        USB_PLX_REG_BIT_SET(pPlxCtrl,
                                            USB_PLX_PCIIRQENB0_REG,
                                            USB_PLX_XIRQENB0_EP(uEpIndex));

                        USB_PLX_REG_WRITE32 (pPlxCtrl,
                                             USB_PLX_EP_STAT_OFFSET(uEpIndex),
                                             uEpStat);
                        }

                    (void) usbPlxTcdRequestComplete (pRequest,
                                                     S_usbTgtTcdLib_ERP_SUCCESS);
                    }

                /* Re-get the first pending request */

                pRequest = usbPlxTcdFirstPendingReqGet (pTCDPipe);
                }

            }
        else
            {
            /* No request pending */

            pTCDPipe->status |= USBTGT_PIPE_STATUS_DATA_PENDING;
            USB_PLX_REG_WRITE32 (pPlxCtrl,
                                 USB_PLX_EP_IRQENB_OFFSET(pTCDPipe->uPhyEpAddr),
                                 USB_PLX_EP_IRQENB_DPR);

            USB_PLX_REG_BIT_SET(pPlxCtrl,
                                USB_PLX_PCIIRQENB0_REG,
                                USB_PLX_XIRQENB0_EP(uEpIndex));

            USB_PLX_REG_WRITE32 (pPlxCtrl,
                     USB_PLX_EP_STAT_OFFSET(uEpIndex),
                     uEpStat );

            return;
            }
        }

    USB_PLX_VDBG ("usbPlxTcdRxDmaIrqHandler():Done\n",
                  1, 2, 3, 4, 5, 6);

    return;
    }

/*******************************************************************************
*
* usbPlxTcdRxEpIrqHandler - process the general endpoint receive interrupt
*
* This routine processes the general endpoint receive interrupt, not control
* endpoint.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbPlxTcdRxEpIrqHandler
    (
    pUSB_PLX_TCD_DATA pTCDData,
    pUSB_PLX_TCD_PIPE pTCDPipe
    )
    {
    pUSB_PLX_CTRL        pPlxCtrl = NULL;
    pUSB_PLX_TCD_REQUEST pRequest = NULL;
    UINT32               uEpAvail;
    UINT32               uEpStat;
    UINT8                uEpIndex;

    /* Parameter verification */

    if ((NULL == pTCDData) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)) ||
        (NULL == pTCDPipe))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTCDData) ? "pTCDData" :
                     (NULL == pTCDData->pPlxCtrl) ? "pPlxCtrl" :
                     "pTCDPipe"), 2, 3, 4, 5, 6);
        return;
        }

    /* Get the first request */

    pRequest = usbPlxTcdFirstPendingReqGet (pTCDPipe);

    if (NULL == pRequest)
        {
        USB_PLX_VDBG("usbPlxTcdRxEpIrqHandler(): No Request pending \n",
                     1, 2, 3, 4, 5, 6);

        if (pTCDPipe->status & USBTGT_PIPE_STATUS_MSC_CASE_10)
            {
            USB_PLX_REG_WRITE32 (pPlxCtrl,
                                  USB_PLX_EP_RSP_OFFSET(2),
                                  USB_PLX_EP_RSP_STALL << 8);
            USB_PLX_REG_WRITE32 (pPlxCtrl,
                                  USB_PLX_EP_STAT_OFFSET(2),
                                  USB_PLX_EP_STAT_FIFO_FLUSH);

            USB_PLX_REG_WRITE32 (pPlxCtrl,
                                  USB_PLX_EP_RSP_OFFSET(1),
                                  USB_PLX_EP_RSP_STALL << 8);

            pTCDPipe->status &=  ~USBTGT_PIPE_STATUS_MSC_CASE_10;
            }

        /* TODO: protect */

        pTCDPipe->status |= USBTGT_PIPE_STATUS_DATA_PENDING;

        return;
        }

    /*
     * If the DMA for this pipe is enabled, and we just got another RX interrupt
     * Before the DMA finished. return;
     */

    if (pRequest->bDmaActive == TRUE)
        {
        USB_PLX_DBG("usbPlxTcdRxEpIrqHandler(): DMA for this request is pending\n",
                     1, 2, 3, 4, 5, 6);
        return;
        }

    uEpIndex = pTCDPipe->uPhyEpAddr;

    USB_PLX_DBG ("usbPlxTcdRxEpIrqHandler(): Porcess interrupt for %x\n",
                 uEpIndex, 2, 3, 4, 5, 6);
    /*
     * When a rx interrupt issued.
     * 1. disable the Ep irq
     * 2. read the endpoint status register
     * 3. read the endpoint avail register
     * 4. check if a short packet received
     */

RX_MORE_DATA:

    /* Read the endpoint status and the available data */

    uEpStat = USB_PLX_REG_READ32 (pPlxCtrl,
                                  USB_PLX_EP_STAT_OFFSET(uEpIndex));

    uEpAvail = USB_PLX_REG_READ32 (pPlxCtrl,
                                   USB_PLX_EP_AVAIL_OFFSET(uEpIndex));

    USB_PLX_DBG("usbPlxTcdRxEpIrqHandler(): uEpStat %x uEpAvail %x\n",
                uEpStat, uEpAvail, 3, 4, 5, 6);

    /*
     * There are more date than the max packet,
     * check if we get one short packet
     */

    if (uEpAvail > pTCDPipe->uMaxPacketSize)
        {
        uEpStat = USB_PLX_REG_READ32 (pPlxCtrl,
                                      USB_PLX_EP_STAT_OFFSET(uEpIndex));

        /*
         * One short packet received from the host,
         * indicate the end of the transaction.
         */

        if (uEpStat & USB_PLX_EP_STAT_NAKOUT)
            {
            /* Re-get the data length to avoid the delay for register update */

            uEpAvail = USB_PLX_REG_READ32 (pPlxCtrl,
                                           USB_PLX_EP_AVAIL_OFFSET(uEpIndex));
            }
        }

    /* Short packeted */

    if ((uEpStat & USB_PLX_EP_STAT_NAKOUT) ||
        (uEpAvail % pTCDPipe->uMaxPacketSize))
        {
        pRequest->bShortPkt = TRUE;
        }
    else
        {
        pRequest->bShortPkt = FALSE;

        /* Do not receive the short packet, but no data in the FIFO */

        if (uEpAvail == 0)
            {
            USB_PLX_VDBG ("Do not receive the short packet, but no data in the FIFO\n",
                    1,2,3,4,5,6);

            USB_PLX_REG_WRITE32 (pPlxCtrl,
                                 USB_PLX_EP_IRQENB_OFFSET(uEpIndex),
                                 USB_PLX_EP_IRQENB_DPR);

            USB_PLX_REG_BIT_SET(pPlxCtrl,
                                USB_PLX_PCIIRQENB0_REG,
                                USB_PLX_XIRQENB0_EP(uEpIndex));
            return;
            }
        }

    USB_PLX_VDBG ("usbPlxTcdRxEpIrqHandler(): uEpAvail %x uEpStat %x [%d %d %d]\n",
                   uEpAvail, uEpStat,
                   pTCDPipe->uDmaChannel,
                   pRequest->bShortPkt,
                   pRequest->bDmaActive, 6);

    uEpAvail = min (uEpAvail, pRequest->uXferSize - pRequest->uActLength);

   if ((pTCDPipe->uDmaChannel != USB_PLX_DMA_CH_IVALID) &&
       (pRequest->bShortPkt == FALSE) &&
       (pRequest->bDmaActive == FALSE) &&
       (0 == (pRequest->uXferSize % pTCDPipe->uMaxPacketSize)))
        {
        pRequest->uCurXferLength = uEpAvail;

        /* Do not enable the DMA for short pacekt */

        USB_PLX_DBG("usbPlxTcdRxEpIrqHandler():Restart DMA uEpStat %x uEpAvail %x\n",
                      uEpStat, uEpAvail, 3, 4, 5, 6);

        if (OK == usbPlxTcdDmaStart (pTCDData, pTCDPipe, pRequest))
            {
            /* If DMA start, return */

            return;
            }
        }

    /* Using the PIO mode to get the data */

    pRequest->uCurXferLength = usbPlxTcdFIFORead(pPlxCtrl,
                                                 pRequest->pCurrentBuffer,
                                                 uEpAvail,
                                                 uEpIndex);
    /* Clean the endpoint status regiser */

    USB_PLX_REG_WRITE32 (pPlxCtrl,
                         USB_PLX_EP_STAT_OFFSET(uEpIndex),
                         uEpStat);

    /* Update the act length */

    pRequest->uActLength += pRequest->uCurXferLength;

    if (pRequest->uActLength == 31)
        USB_PLX_VDBG ("usbPlxTcdRxEpIrqHandler [%x %x %x %x]\n",
               *(UINT32 *)&pRequest->pCurrentBuffer[15],
               *(UINT32 *)&pRequest->pCurrentBuffer[19],
               *(UINT32 *)&pRequest->pCurrentBuffer[23],
               *(UINT32 *)&pRequest->pCurrentBuffer[27], 5,6
               );

    pRequest->pCurrentBuffer = pRequest->pCurrentBuffer +
                                  pRequest->uCurXferLength;

    /* Check if this request should be done */

    if ((pRequest->bShortPkt) ||
        (pRequest->uCurXferLength == 0) ||
        (pRequest->uCurXferLength % pTCDPipe->uMaxPacketSize) ||
        (pRequest->uActLength == pRequest->uXferSize))
        {
        USB_PLX_DBG ("usbPlxTcdRxEpIrqHandler callback [bShort %x Cur %x Max %x Act %x Xfer %x ]\n",
                      pRequest->bShortPkt, pRequest->uCurXferLength,
                      pTCDPipe->uMaxPacketSize, pRequest->uActLength,
                      pRequest->uXferSize, 6);

        (void) usbPlxTcdRequestComplete (pRequest,
                                         S_usbTgtTcdLib_ERP_SUCCESS);
        }
    else
        {
        USB_PLX_VDBG ("usbPlxTcdRxEpIrqHandler NO [bShort %x Cur %x Max %x "
                      "Act %x Xfer %x ]\n",
                      pRequest->bShortPkt, pRequest->uCurXferLength,
                      pTCDPipe->uMaxPacketSize, pRequest->uActLength,
                      pRequest->uXferSize, 6);
        }

    /* Re-read the status regiser and data available regiser */

    uEpAvail = USB_PLX_REG_READ32 (pPlxCtrl,
                                   USB_PLX_EP_AVAIL_OFFSET(uEpIndex));

    uEpStat = USB_PLX_REG_READ32 (pPlxCtrl,
                                   USB_PLX_EP_STAT_OFFSET(uEpIndex));

    USB_PLX_VDBG ("usbPlxTcdRxEpIrqHandler():Re-Load Reg uEpAvail %x uEpStat %x\n",
                  uEpAvail, uEpStat, 3, 4, 5, 6);

    if (uEpAvail)
        {
        pRequest = usbPlxTcdFirstPendingReqGet (pTCDPipe);

        if (NULL == pRequest)
            {
            /* No request pending */

            pTCDPipe->status |= USBTGT_PIPE_STATUS_DATA_PENDING;
            USB_PLX_REG_WRITE32 (pPlxCtrl,
                                 USB_PLX_EP_IRQENB_OFFSET(uEpIndex),
                                 USB_PLX_EP_IRQENB_DPR );
            USB_PLX_REG_BIT_SET(pPlxCtrl,
                                USB_PLX_PCIIRQENB0_REG,
                                USB_PLX_XIRQENB0_EP(uEpIndex));
            }
        else
            {
            /* Reload the data from the FIFO */

            goto RX_MORE_DATA;
            }
        }
    else
        {
        /* No data pending in the FIFO, restart */

        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_EP_IRQENB_OFFSET(uEpIndex),
                             USB_PLX_EP_IRQENB_DPR );
        USB_PLX_REG_BIT_SET(pPlxCtrl,
                            USB_PLX_PCIIRQENB0_REG,
                            USB_PLX_XIRQENB0_EP(uEpIndex));
        }

    USB_PLX_DBG ("usbPlxTcdRxEpIrqHandler():Done\n",
                  1, 2, 3, 4, 5, 6);
    return;
    }

/*******************************************************************************
*
* usbPlxTcdEpIrqHandler - process the general endpoint interrupts
*
* This routine processes the general endpoint interrupts.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbPlxTcdEpIrqHandler
    (
    pUSB_PLX_TCD_DATA pTCDData,
    UINT8             uEpIndex
    )
    {
    pUSB_PLX_CTRL        pPlxCtrl = NULL;
    pUSB_PLX_TCD_PIPE    pTCDPipe = NULL;

    /* Parameter verification */

    if ((NULL == pTCDData) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)) ||
        (USB_PLX_ENDPT_0 == uEpIndex))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTCDData) ? "pTCDData" :
                     (NULL == pTCDData->pPlxCtrl) ? "pPlxCtrl" :
                     "uEpIndex"), 2, 3, 4, 5, 6);
        return;
        }

    /* Find the right pipe */

    pTCDPipe = usbPlxTcdPipeFind (pTCDData, uEpIndex);

    if (NULL == pTCDPipe)
        {
        USB_PLX_WARN("Invalid parameter %s is NULL\n",
                    ("pTCDPipe"), 2, 3, 4, 5, 6);
        return;
        }

    if (pTCDPipe->uEpDir == USB_ENDPOINT_IN)
        {
        usbPlxTcdTxEpIrqHandler (pTCDData, pTCDPipe);
        }
    else
        {
        usbPlxTcdRxEpIrqHandler (pTCDData, pTCDPipe);
        }

    return;
    }


/*******************************************************************************
*
* usbPlxTcdDmaIrqHandler - process the control endpoint setup interrupt
*
* This routine processes the control endpoint setup interrupt.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbPlxTcdDmaIrqHandler
    (
    pUSB_PLX_TCD_DATA pTCDData,
    UINT8             uEpIndex
    )
    {
    pUSB_PLX_CTRL        pPlxCtrl = NULL;
    pUSB_PLX_TCD_PIPE    pTCDPipe = NULL;

    /* Parameter verification */

    if ((NULL == pTCDData) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)) ||
        (USB_PLX_ENDPT_0 == uEpIndex))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTCDData) ? "pTCDData" :
                     (NULL == pTCDData->pPlxCtrl) ? "pPlxCtrl" :
                     "uEpIndex"), 2, 3, 4, 5, 6);
        return;
        }

    USB_PLX_VDBG ("usbPlxTcdDmaIrqHandler in DMASTAT %x of channel %x\n",
                   USB_PLX_REG_READ32 (pPlxCtrl,
                                       USB_PLX_DMASTAT_OFFSET (uEpIndex)),
                   uEpIndex, 3, 4, 5, 6);

    /* Find the right pipe */

    pTCDPipe = usbPlxTcdPipeFind (pTCDData, uEpIndex);

    if (NULL == pTCDPipe)
        {
        USB_PLX_WARN("Invalid parameter %s is NULL\n",
                    ("pTCDPipe"), 2, 3, 4, 5, 6);
        return;
        }

    /* Stop the DMA */

    if (pTCDPipe->uEpDir == USB_ENDPOINT_IN)
        {
        usbPlxTcdDmaStop(pTCDData, pTCDPipe);
        usbPlxTcdTxDmaIrqHandler (pTCDData, pTCDPipe);
        }
    else
        {
        usbPlxTcdDmaStop(pTCDData, pTCDPipe);
        usbPlxTcdRxDmaIrqHandler (pTCDData, pTCDPipe);
        }
    return;
    }


/*******************************************************************************
*
* usbPlxTcdInterruptHandler - handler for handling USB PLX TCD interrupts
*
* This routine is used to handle the USB USB PLX TCD interrupts
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbPlxTcdInterruptHandler
    (
    void * pTCDDATA
    )
    {
    pUSB_PLX_TCD_DATA   pTCDData = NULL;
    pUSB_PLX_CTRL       pPlxCtrl = NULL;
    pUSBTGT_TCD         pTcd = NULL;
    pUSB_PLX_IRQ_BOX    pIrqBox = NULL;
    UINT32              uIrqStat0;
    UINT32              uIrqStat1;
    UINT32              uUsbCtl = 0;
    UINT32              uEp0Stat = 0;
    UINT32              uEp0Rsp = 0;
    UINT32              uEp0Avail = 0;
    UINT8               uEpIndex;

    /* Parameter verification */

    if ((NULL == (pTCDData = (pUSB_PLX_TCD_DATA)pTCDDATA)) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)) ||
        (NULL == (pTcd = pTCDData->pTcd)))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTCDData) ? "pTCDData" :
                     (NULL == pTCDData->pPlxCtrl) ? "pPlxCtrl" :
                     "pTcd"), 2, 3, 4, 5, 6);
        return;
        }

    while (TRUE)
        {
        OS_WAIT_FOR_EVENT(pTCDData->isrEventId, WAIT_FOREVER);
IRQ_PROCESS:

        /* Handle the interrupt here */

        SPIN_LOCK_ISR_TAKE(&pTCDData->pPlxCtrl->spinLock);
        uIrqStat0 = 0;
        uIrqStat1 = 0;

        pIrqBox = (pUSB_PLX_IRQ_BOX)lstFirst(&pTCDData->irqBoxPendList);
        if (NULL != pIrqBox)
            {
            uIrqStat0 = pIrqBox->uIrqStat0;
            uIrqStat1 = pIrqBox->uIrqStat1;
            uEp0Stat  =  pIrqBox->uEp0Stat ;
            uEp0Rsp   = pIrqBox->uEp0Rsp  ;
            uEp0Avail =   pIrqBox->uEp0Avail  ;
            pIrqBox->uIrqStat0 = 0;
            pIrqBox->uIrqStat1 = 0;
            lstDelete(&pTCDData->irqBoxPendList, &pIrqBox->irqNode);
            lstAdd(&pTCDData->irqBoxFreeList, &pIrqBox->irqNode);
            }

        SPIN_LOCK_ISR_GIVE(&pTCDData->pPlxCtrl->spinLock);

        /* If the flag is not 0, will ignore all irq */

        if (usrUsbPlxTcdIsrTaskFlagGet(pTcd->uTcdUnit))
            {
            if (lstCount(&pTCDData->irqBoxPendList))
                {
                goto IRQ_PROCESS;
                }
            else
                {
                continue;
                }
            }

        USB_PLX_VDBG("usbPlxTcdInterruptHandler(): uIrqStat0 0x%X uIrqStat1 0x%X\n",
                     uIrqStat0, uIrqStat1, 3, 4, 5, 6);

        /* Handle all the interrupts ... */

        /*
         * Root-Port Reset event
         * Root-Port Reset is recognized only when the VBUS input pin is
         * high, and the USBCTL register USB Detect Enable bit is set.
         */

        if (uIrqStat1 & USB_PLX_IRQENB1_RPRESET)
            {
            USB_PLX_VDBG("usbPlxTcdInterruptHandler(): handle the root-port reset\n",
                         1, 2, 3, 4, 5, 6);

            (void)usbPlxTcdResetEventHandler(pTcd);
            }

        /*
         * Disconnect event
         * IRQENB1_VBUS indicate the VBUS changed
         * VBUSPIN status should be low in USBCTL register
         */

        if (uIrqStat1 & USB_PLX_IRQENB1_VBUS)
            {
            uUsbCtl = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_USBCTL_REG);

            if (uUsbCtl & USB_PLX_USBCTL_REG_VBUSPIN)
                {
                USB_PLX_VDBG("usbPlxTcdInterruptHandler(): device connected\n",
                             1, 2, 3, 4, 5, 6);
                /*
                 * Hardware connection interrupt.
                 *
                 * In USB3.0, the host will try to do termination training
                 * to determining the hardware speed with the device. If the
                 * USB3.0 termination passed, There will no reset signal
                 * to the USB device, and issue the first reqest Address Set
                 * request directly to the USB device.
                 *
                 * So, the USB target stack should prepare the setup ERP
                 * early to get the potential request.
                 *
                 * Call usbPlxTcdResetEventHandler() routine to
                 * determine the hardware speed and prepare the ERP with
                 * a fake reset interrupt.
                 */

                (void)usbPlxTcdResetEventHandler(pTcd);
                }
            else
                {
                USB_PLX_VDBG("usbPlxTcdInterruptHandler(): device disconnected\n",
                             1, 2, 3, 4, 5, 6);

                (void)usbPlxTcdDisconnectEventHandler(pTcd);
                }
            }

        /* Suspend event */

        if (uIrqStat1 & USB_PLX_IRQENB1_SUSREQCHG)
            {
            USB_PLX_VDBG("usbPlxTcdInterruptHandler(): handle the Suspend event\n",
                         1, 2, 3, 4, 5, 6);

            (void)usbPlxTcdSuspendEventHandler(pTcd);
            }

        /* Resume event */

        if (uIrqStat1 & USB_PLX_IRQENB1_RESM)
            {
            USB_PLX_VDBG("usbPlxTcdInterruptHandler(): handle the Resume\n",
                         1, 2, 3, 4, 5, 6);

            (void)usbPlxTcdResumeEventHandler(pTcd);
            }

        /* Ep0 setup packet received */

        if ((uIrqStat0 & USB_PLX_IRQENB0_EP(0)) ||
            (uIrqStat0 & USB_PLX_IRQENB0_SETUP))
            {
            /* 3380/3382 no need it */

            if (((pPlxCtrl->uDeviceId !=  USB_PLX_DEVICE_ID_3380) ||
                 (pPlxCtrl->uDeviceId !=  USB_PLX_DEVICE_ID_3382)) &&
                (pTCDData->ep0Stage == USB_TCD_EP0_STAGE_DATA_OUT))
                {
                usbPlxTcdEpRegRead(pPlxCtrl, 0);
                }

            pPlxCtrl->epReg[0].uEpRsp = uEp0Rsp;
            pPlxCtrl->epReg[0].uEpStat= uEp0Stat;
            pPlxCtrl->epReg[0].uEpAvail= uEp0Avail;

            usbPlxTcdEp0InterruptHandler(pTCDData, uIrqStat0);
            }

        for (uEpIndex = USB_PLX_ENDPT_A;
             uEpIndex <= USB_PLX_ENDPT_F; uEpIndex ++)
            {
            if (uIrqStat0 & USB_PLX_IRQENB0_EP(uEpIndex))
                {
                USB_PLX_VDBG("usbPlxTcdInterruptHandler(): uEpIndex 0x%X\n",
                              uEpIndex, 2, 3, 4, 5, 6);

                usbPlxTcdEpIrqHandler (pTCDData, uEpIndex);
                }
            }

        for (uEpIndex = USB_PLX_ENDPT_A;
             uEpIndex <= USB_PLX_ENDPT_D; uEpIndex ++)
            {
            if (uIrqStat1 & USB_PLX_IRQENB1_DMA(uEpIndex))
                {
                USB_PLX_VDBG("usbPlxTcdInterruptHandler(): uEpIndex 0x%X\n",
                              uEpIndex, 2, 3, 4, 5, 6);

                usbPlxTcdDmaIrqHandler (pTCDData, uEpIndex);
                }
            }

        /* More interrupt requests to be handle */

        if (lstCount(&pTCDData->irqBoxPendList))
            {
            goto IRQ_PROCESS;
            }
        }
    }


/*******************************************************************************
*
* usbPlxTcdIsr - interrupt handler for handling PLX TCD interrupts
*
* This routine is registered as the interrupt handler for handling PLX TCD
* interrupts.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbPlxTcdIsr
    (
    void * pPLXCTRL
    )
    {
    pUSB_PLX_CTRL     pPlxCtrl = (pUSB_PLX_CTRL)pPLXCTRL;
    pUSB_PLX_TCD_DATA pTCDData = NULL;
    pUSB_PLX_IRQ_BOX  pIrqBox = NULL;
    UINT8             uEpIndex;
    UINT32            uEp0Stat = 0;
    UINT32            uEp0Rsp = 0;
    UINT32            uEp0Avail = 0;

    /* Check the validity of the parameter */

    if ((NULL == pPlxCtrl) ||
        (NULL == pPlxCtrl->pDev))
        {
        USB_PLX_ERR("usbPlxTcdIsr():Invalid parameter, %s is NULL\n",
                    ((NULL == pPlxCtrl) ? "pPlxCtrl" :
                    "pPlxCtrl->pDev"),
                    2, 3, 4, 5 ,6);

        return;
        }

    if (pPlxCtrl->uMagicCode != USB_PLX_MAGIC_ALIVE)
        {
        USB_PLX_ERR("usbPlxTcdIsr():ISR not active, maybe shared?\n",
                    1, 2, 3, 4, 5 ,6);

        return;
        }

    /* Disable the USB Interrupts. clear bit 31 of PCIIRQENB1 register */

    USB_PLX_PCI_IRQ_DISABLE(pPlxCtrl);

    /* Read IRQSTAT0 register */

    pPlxCtrl->uIrqStat0 = pPlxCtrl->uIrqMask0 &
                          USB_PLX_REG_READ32 (pPlxCtrl, USB_PLX_IRQSTAT0_REG);

    /* Clear the irq status 0 */

    USB_PLX_REG_WRITE32 (pPlxCtrl,
                         USB_PLX_IRQSTAT0_REG,
                         pPlxCtrl->uIrqStat0);

    /* Mask the unwanted interrupts */

    /* Clear the intertupt status register */

    if (pPlxCtrl->uIrqStat0 & USB_PLX_XIRQENB0_SETUP)
        {
        usbPlxTcdEpRegRead(pPlxCtrl, 0);
        uEp0Stat = pPlxCtrl->epReg[0].uEpStat;
        uEp0Rsp = pPlxCtrl->epReg[0].uEpRsp;
        uEp0Avail = pPlxCtrl->epReg[0].uEpAvail;
        }

    if (pPlxCtrl->uIrqStat0 & USB_PLX_XIRQENB0_EP(0))
        {
        usbPlxTcdEpRegRead(pPlxCtrl, 0);
        uEp0Stat = pPlxCtrl->epReg[0].uEpStat;
        uEp0Rsp = pPlxCtrl->epReg[0].uEpRsp;
        uEp0Avail = pPlxCtrl->epReg[0].uEpAvail;
        if (pPlxCtrl->epReg[0].uEpStat & USB_PLX_EP_STAT_DPT)
            {
            /* Check if it is time to set address */

            /* Only clear the TX interrupt status */

            USB_PLX_REG_WRITE32(pPlxCtrl,
                                USB_PLX_EP_STAT_OFFSET(0),
                                USB_PLX_EP_STAT_DPT);
            }
        if ((pPlxCtrl->epReg[0].uEpStat & USB_PLX_EP_STAT_DPR)/* &&
            (pPlxCtrl->uIrqStat0 & USB_PLX_XIRQENB0_SETUP) */)
            {
            USB_PLX_REG_WRITE32(pPlxCtrl,
                                USB_PLX_EP_STAT_OFFSET(0),
                                USB_PLX_EP_STAT_DPR);
            }
        }

    for (uEpIndex = USB_PLX_ENDPT_A; uEpIndex <= USB_PLX_ENDPT_F; uEpIndex ++)
        {
        if (pPlxCtrl->uIrqStat0 & USB_PLX_XIRQENB0_EP(uEpIndex))
            {
             pPlxCtrl->epReg[uEpIndex].uEpCfg  = USB_PLX_REG_READ32 (pPlxCtrl,
                                         USB_PLX_EP_CFG_OFFSET(uEpIndex));

             pPlxCtrl->epReg[uEpIndex].uEpStat = USB_PLX_REG_READ32 (pPlxCtrl,
                                         USB_PLX_EP_STAT_OFFSET(uEpIndex));

             if (0 == (pPlxCtrl->epReg[uEpIndex].uEpCfg & USB_ENDPOINT_IN))
                {
                /* Disable the interrupt */

                USB_PLX_REG_WRITE32 (pPlxCtrl,
                                     USB_PLX_EP_STAT_OFFSET(uEpIndex),
                                     (USB_PLX_EP_STAT_DPR |
                                     USB_PLX_EP_STAT_DOPT |
                                    /* USB_PLX_EP_STAT_OUTACKSNT | */
                                    /* USB_PLX_EP_STAT_NAKOUT | */
                                     USB_PLX_EP_STAT_SPOD /*|*/
                                    /* USB_PLX_EP_STAT_SPT */));

                USB_PLX_REG_BIT_CLR(pPlxCtrl,
                                    USB_PLX_PCIIRQENB0_REG,
                                    USB_PLX_XIRQENB0_EP(uEpIndex));
                }

            else
                {
                 USB_PLX_REG_WRITE32 (pPlxCtrl,
                     USB_PLX_EP_STAT_OFFSET(uEpIndex),
                     pPlxCtrl->epReg[uEpIndex].uEpStat);
                }

            }
        }

    /* Read IRQSTAT1 register */

    pPlxCtrl->uIrqStat1 = USB_PLX_REG_READ32 (pPlxCtrl,
                          USB_PLX_IRQSTAT1_REG) &
                          pPlxCtrl->uIrqMask1;

    for (uEpIndex = USB_PLX_ENDPT_A;
         uEpIndex <= USB_PLX_ENDPT_D; uEpIndex ++)
        {
        if (pPlxCtrl->uIrqStat1 & USB_PLX_IRQENB1_DMA(uEpIndex))
            {
            /* Disable the DMA */

            USB_PLX_REG_WRITE32(pPlxCtrl,
                                USB_PLX_DMACTL_OFFSET (uEpIndex),
                                USB_PLX_REG_READ32 (pPlxCtrl,
                                USB_PLX_DMACTL_OFFSET (uEpIndex)) &
                                ~(USB_PLX_DMACTL_EN));

            /* Clear the DMA status */

            USB_PLX_REG_WRITE32(pPlxCtrl,
                                USB_PLX_DMASTAT_OFFSET (uEpIndex),
                                USB_PLX_REG_READ32 (pPlxCtrl,
                                         USB_PLX_DMASTAT_OFFSET (uEpIndex)) |
                                USB_PLX_DMASTAT_TD_INT);
            }
        }

    /* Clear the intertupt status register */

    USB_PLX_REG_WRITE32 (pPlxCtrl,
                         USB_PLX_IRQSTAT1_REG,
                         pPlxCtrl->uIrqStat1 &
                         ~ (USB_PLX_IRQENB1_SUSP));

    /* Signal the ISR event */

    if ((0 != pPlxCtrl->uIrqStat0) ||
        (0 != pPlxCtrl->uIrqStat1))
        {
        USB_PLX_VDBG("usbPlxTcdIsr():Interrupt status uIrqStat0 0x%X "
                     "uIrqStat1 0x%X \n",
                     pPlxCtrl->uIrqStat0, pPlxCtrl->uIrqStat1, 3, 4, 5 ,6);

        if (NULL != pPlxCtrl->pTCDData)
            {
            SPIN_LOCK_ISR_TAKE(&pPlxCtrl->spinLock);
            pTCDData = (pUSB_PLX_TCD_DATA) pPlxCtrl->pTCDData;

            pIrqBox = (pUSB_PLX_IRQ_BOX)lstFirst(&pTCDData->irqBoxFreeList);
            if (NULL != pIrqBox)
                {
                pIrqBox->uIrqStat0 = (UINT32)pPlxCtrl->uIrqStat0;
                pIrqBox->uIrqStat1 = (UINT32)pPlxCtrl->uIrqStat1;
                pIrqBox->uEp0Stat = uEp0Stat;
                pIrqBox->uEp0Rsp = uEp0Rsp;
                pIrqBox->uEp0Avail = uEp0Avail;
                lstDelete(&pTCDData->irqBoxFreeList, &pIrqBox->irqNode);
                lstAdd(&pTCDData->irqBoxPendList, &pIrqBox->irqNode);
                }

            SPIN_LOCK_ISR_GIVE(&pPlxCtrl->spinLock);
            OS_RELEASE_EVENT(pTCDData->isrEventId);
            }
        }

    /* PCI IRQ enable */

    USB_PLX_PCI_IRQ_ENABLE(pPlxCtrl);

    return;
    }

#ifdef USB_PLX_TCD_IRQ_POLLING_MODE

/*******************************************************************************
*
* usbPlxTcdPollingThread - check for interrupts periodically
*
* This routine is the task entry for PLX TCD polling task, which
* periodically checks for interrupts.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbPlxTcdPollingThread
    (
    void * pPLXCTRL
    )
    {
    while (1)
        {
        /* Call the ISR function */

        USB_PLX_VDBG("usbPlxTcdPollingThread():Polling the IRQs\n",
                     1,2, 3, 4, 5 ,6);

        usbPlxTcdIsr(pPLXCTRL);

        taskDelay(20);
        }
    }
#endif /* USB_PLX_TCD_IRQ_POLLING_MODE */
