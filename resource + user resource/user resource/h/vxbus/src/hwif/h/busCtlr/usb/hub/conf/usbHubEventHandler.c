/* usbHubEventHandler.c - Functions for handling HUB events */

/*
 * Copyright (c) 2003-2004, 2006, 2008, 2010, 2013-2014 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2003-2004, 2006, 2008, 2010, 2013-2014 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
Modification history
--------------------
01k,15jun14,j_x  Modified the type for uNumberOfHubEvents (VXW6-80778)
01j,14may13,ghs  Make a delay after all ports are powered on in hub (WIND00417261)
01i,29jun10,w_x  Add more port reset delay and clean hub logging (WIND00216628)
01h,24may10,m_y  Modify routine usbHubEventHandler for usb robustness (WIND00183499)
01g,27apr10,j_x  Error debug adaption and hard code replacement (WIND00183874)
01f,11mar10,j_x  Changed for USB debug (WIND00184542)
01e,13jan10,ghs  vxWorks 6.9 LP64 adaptingd
01d,17dec08,w_x  Use seperate copy of hub status to avoid potential corruption;
                 Use DMA/cache safe buffer for hub status (WIND00148225)
01c,11Dec06,jrp  (with ami) Defect 61612
01b,15oct04,ami  Refgen Changes
01a,27jun03,nrv  Changing the code to WRS standards
*/

/*
DESCRIPTION

This provides the functions for handling the hub events. This is used in
conjunction with the Port Event Handling Module and Bus Manager Module.

INCLUDE FILES:  usb2/usbOsal.h, usb2/usbHst.h, usb2/usbHubCommon.h,
usb2/usbHubUtility.h, usb2/usbHubEventHandler.h

*/

/*
INTERNAL
 *******************************************************************************
 * Filename         : HUB_HubEventHandler.c
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      : This provides the functions for handling the hub events.
 *                    This is used in conjunction with the Port Event Handling
 *                    Module and Bus Manager Module.
 *
 *
 ******************************************************************************/



/************************** INCLUDE FILES *************************************/

#include "usb/usbOsal.h"
#include "usb/usbHst.h"
#include "usbHubCommon.h"
#include "usbHubUtility.h"
#include "usbHubEventHandler.h"


/************************** DEFINES********************************************/

/* Defines the bit mask of  hub status */
#define HUB_MASK_STATUS                   0xFE
#define HUB_OVER_CURRENT_COOL_DOWN_TIME  (500)

/************************ GLOBAL FUNCTIONS DEFINITION *************************/

/***************************************************************************
*
* usbHubEventHandler - Handle the hub event.
*
* This is the function to handle the hub event. This has  the maximum priority
* over any other event. Each Bus structure maintains a variable whose count is
* incremented every time a hub event is detected and decremented every time a
* hub event is handled. Only when this counter is 0 are other events handled.
*
* RETURNS: N/A
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubEventHandler
    (
    pUSB_HUB_INFO pHub
    )
    {
    /* To store the length of the buffer */
    UINT8         uLength = sizeof(USB_HUB_STATUS);

    /* To store the result */
    USBHST_STATUS Result;

    /* Counter for the port numbers */
    UINT8         uPortCount = 0;

    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
            USB_HUB_WV_EVENT_HANDLER,
            "Entering usbHubEventHandler() Function",
            USB_HUB_WV_FILTER);

    /* If the pHub is NULL then return  USBHST_INVALID_PARAMETER */
    if ((NULL == pHub) || (pHub->pHubStatus == NULL))
        {
        /* Debug Message */
        USB_HUB_ERR("invalid parameters \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }/* End of if (NULL == pHub) */

    /*
     * Call OS_LOCKED_DECREMENT() to decrement pHub::pBus::uNumberOfHubEvents.
     * Move the decrement of the pHub->pBus->uNumberOfHubEvents from
     * the end of the function to here.
     * When we run into any error condition, the uNumberOfHubEvents also should
     * be decreased to keep consistent.
     */

    if ((vxAtomicGet(&(pHub->pBus->uNumberOfHubEvents))) > 0)
        {
        OS_LOCKED_DECREMENT(pHub->pBus->uNumberOfHubEvents);
        }

    /* Clean up the bit of hub status */

    pHub->pCopiedStatus[0] = (UINT8)(pHub->pCopiedStatus[0] & (0xFE));


    /*
     * Call HUB_GET_HUB_STATUS for getting the HUB_STATUS structure.
     * i. If the call fails then set the pHub::StateOfHub
     *    as MARKED_FOR_DELETION.
     * ii.Return the result.
     */

    Result = USB_HUB_GET_STATUS(pHub, (UINT8 *)(pHub->pHubStatus), &uLength);

    /* Check if the call failed. */

    if ((USBHST_SUCCESS != Result) || (sizeof(USB_HUB_STATUS) != uLength))
        {
        /* Debug Message */
        USB_HUB_ERR("failed to get hub status \n",
                    1, 2, 3, 4, 5, 6);
        return USBHST_FAILURE;
        }/* End of if (USBHST_SUCCESS != Result) */


   /* check if there is a Over Current Status Change */

   if ((OS_UINT16_LE_TO_CPU(pHub->pHubStatus->wHubChange) &
        USB_C_HUB_OVER_CURRENT_VALUE) != 0)
       {

       /*
        * Over current may result by a pulse
        * We delay a little time to let it down
        */

       OS_DELAY_MS(HUB_OVER_CURRENT_COOL_DOWN_TIME);

       USB_HUB_DBG("usbHubEventHandler - clear hub 0x%X USB_HUB_OVER_CURRENT\n",
                   pHub->uDeviceHandle, 2, 3, 4, 5, 6);

       /* issue a clear hub feature request to clear the over current bit */

       USB_HUB_CLEAR_HUB_FEATURE (pHub, Result, USB_HUB_OVER_CURRENT);

       if (USBHST_SUCCESS != Result)
            {
            /* Debug Message */
            USB_HUB_ERR("failed to clear hub feature \n",
                        1, 2, 3, 4, 5, 6);

            return Result;
            }/* End of if (USBHST_SUCCESS !=Result) */
        }

   /* check if there is a local power status change */

   if ((OS_UINT16_LE_TO_CPU(pHub->pHubStatus->wHubChange) &
        USB_C_HUB_LOCAL_POWER_VALUE) != 0)
        {
        USB_HUB_DBG("usbHubEventHandler - clear hub 0x%X USB_HUB_LOCAL_POWER\n",
                   pHub->uDeviceHandle, 2, 3, 4, 5, 6);

        /* issue a clear hub feature request to clear the hub local power status
         * change bit
         */
        USB_HUB_CLEAR_HUB_FEATURE (pHub, Result, USB_HUB_LOCAL_POWER);

        if (USBHST_SUCCESS != Result)
            {
            /* Debug Message */
            USB_HUB_ERR("failed to clear hub feature \n",
                        1, 2, 3, 4, 5, 6);

            return Result;
            }/* End of if (USBHST_SUCCESS !=Result) */
        }

    /*
     * check the hub status for Over Current and LPS. If the bits are set
     * power on the ports
     */

    if (pHub->pHubStatus->wHubStatus != 0)
        {
        /* for all ports that are enabled,
         * i. Call HUB_PowerOnPort() with pHub and port count to re enable the
         *    port. If the call fails, then return USBHST_FAILURE.
         */

        for (uPortCount = 0; uPortCount < pHub->HubDescriptor.bNbrPorts;
             uPortCount++)
            {
            USB_HUB_DBG("usbHubEventHandler - power on hub %p port %d\n",
                       pHub->uDeviceHandle, uPortCount, 3, 4, 5, 6);

            Result = usbHubPowerOnPort(pHub,uPortCount);

            if (USBHST_SUCCESS !=Result)
                {
                /* Debug Message */
                USB_HUB_ERR("failed to power on port%d \n",
                            uPortCount, 2, 3, 4, 5, 6);

                return Result;
                }/* End of if (USBHST_SUCCESS !=Result) */
            else
                {
                /* Debug Message */

                USB_HUB_DBG("usbHubPowerOnPort - waiting %d ms for power on hub port\n",
                            (pHub->HubDescriptor.bPwrOn2PwrGood * 2), 2, 3, 4, 5, 6);
                /*
                 * There is a minimal time for the system to stabilize on
                 * setting a port power. This time is provided in the hub
                 * descriptor. we have to wait for this time period.
                 */

                /*
                 * Note on bPwrOn2PwrGood:
                 * Time (in 2 ms intervals) from the time the power-on
                 * sequence begins on a port until power is good on that
                 * port. The USB System Software uses this value to
                 * determine how long to wait before accessing a
                 * powered-on port.
                 */

                OSS_THREAD_SLEEP ((UINT32)(pHub->HubDescriptor.bPwrOn2PwrGood * 2)) ;
                } /* End of if (USBHST_SUCCESS==Result) */
            } /* End of for (uPortCount.... */
        }

    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
            USB_HUB_WV_EVENT_HANDLER,
            "Exiting usbHubEventHandler() Function",
            USB_HUB_WV_FILTER);

    /* Return USBHST_SUCCESS*/

    return USBHST_SUCCESS;

    }/* End of HUB_HubEventHandler ()*/


/**************************** End of File HUB_HubEventHandler.c ***************/


