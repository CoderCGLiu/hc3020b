/* usbMhdrcDma.h - Common DMA procession of Mentor Graphics */

/*
 * Copyright (c) 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,09sep11,m_y  written
*/

/*
DESCRIPTION

This file provides the common DMA routines for Mentor Graphics,
These routines are common to both HSDMA and CPPIDMA.

*/


#ifndef __INCusbMhdrcDmah
#define __INCusbMhdrcDmah

#include "usbMhdrc.h"

#ifdef __cplusplus
extern "C" {
#endif

void usbMhdrcDmaIntrEnable
    (
    pUSB_MUSBMHDRC pMHDRC
    );
void usbMhdrcDmaIntrDisable
    (
    pUSB_MUSBMHDRC pMHDRC
    );
void usbMhdrcDmaCallbackSet
    (
    pUSB_MHDRC_DMA_DATA          pDMAData,
    pUSB_MHDRC_DMA_CALLBACK_FUNC pFunc
    );

#ifdef __cplusplus
}
#endif

#endif

