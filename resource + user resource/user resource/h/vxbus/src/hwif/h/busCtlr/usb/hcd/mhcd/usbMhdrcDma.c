/* usbMhdrcDma.c - Common DMA procession for Mentor Graphics */

/*
 * Copyright (c) 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,15sep11,m_y  written
*/

/*
DESCRIPTION

This file provides the common DMA routines for Mentor Graphics,
These routines are common to both HSDMA and CPPIDMA.

INCLUDE FILES: usb/usbOsal.h, usb/usbHst.h, usb/usb.h, usbMhdrc.h, 
               usbMhdrcDma.h, usbMhdrcPlatform.h, usbMhdrcHsDma.h,
               usbMhdrcCppiDma.h
*/

/* includes */

#include <usb/usbOsal.h>
#include <usb/usbHst.h>
#include <usb/usb.h>
#include "usbMhdrc.h"
#include "usbMhdrcDma.h"
#include "usbMhdrcPlatform.h"
#include "usbMhdrcHsDma.h"
#include "usbMhdrcCppiDma.h"

/* globals */

/*******************************************************************************
*
* usbMhdrcDmaIntrEnable - enable MHDRC USB DMA interrupt
*
* This routine enables MHDRC USB DMA interrupt.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcDmaIntrEnable
    (
    pUSB_MUSBMHDRC pMHDRC
    )
    {
    pUSB_MHDRC_DMA_DATA   pDMAData = NULL;

    if (NULL == pMHDRC)  
        {
        USB_MHDRC_VDBG("usbMhdrcDmaIntrEnable(): Invalid parameter\n",
                       1, 2, 3, 4, 5 ,6);

        return;
        }
    
    pDMAData = &pMHDRC->dmaData;
    
    switch (pDMAData->uDMAType)
        {
        case USB_MHDRC_HS_DMA:
            usbMhdrcHsDmaIntrEnable(pMHDRC);
            break;
        case USB_MHDRC_CPPI_DMA:
            usbMhdrcCppiDmaIntrEnable(pMHDRC);
            break;
        default:
            USB_MHDRC_ERR("usbMhdrcDmaIntrEnable - invalid dma Type %d\n",
                          pDMAData->uDMAType, 2, 3, 4, 5, 6);
        }
    return;
    }

/*******************************************************************************
*
* usbMhdrcDmaIntrDisable - disable MHDRC USB DMA interrupt
*
* This routine disables MHDRC USB DMA interrupt.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcDmaIntrDisable
    (
    pUSB_MUSBMHDRC pMHDRC
    )
    {
    pUSB_MHDRC_DMA_DATA   pDMAData = NULL;

    if (NULL == pMHDRC)
        {
        USB_MHDRC_VDBG("usbMhdrcDmaIntrDisable(): Invalid parameter\n",
                       1, 2, 3, 4, 5 ,6);

        return;
        }

    pDMAData = &(pMHDRC->dmaData);
    
    switch (pDMAData->uDMAType)
        {
        case USB_MHDRC_HS_DMA:
            usbMhdrcHsDmaIntrDisable(pMHDRC);
            break;
        case USB_MHDRC_CPPI_DMA:
            usbMhdrcCppiDmaIntrDisable(pMHDRC);
            break;
        default:
            USB_MHDRC_ERR("usbMhdrcDmaIntrDisable - invalid dma Type %d\n",
                          pDMAData->uDMAType, 2, 3, 4, 5, 6);
        }
    return;
    }

/*******************************************************************************
*
* usbMhdrcDmaCallbackSet - set the DMA callback routine
*
* This routine is to set the DMA callback routine which is used in the DMA 
* interrupt handler.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcDmaCallbackSet
    (
    pUSB_MHDRC_DMA_DATA          pDMAData,
    pUSB_MHDRC_DMA_CALLBACK_FUNC pFunc
    )
    {
    if (pDMAData != NULL)
        pDMAData->pDmaCallback = pFunc;
    return;
    }

