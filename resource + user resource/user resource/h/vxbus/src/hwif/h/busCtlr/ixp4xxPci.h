/* ixp4xxPci.h - IXP4XX VxBus PCI bus controller driver */

/*
 * Copyright (c) 2006-2008, 2010 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01i,26feb10,h_k  added a cacheable Bounce Partition.
                 removed ixHwMemPool.
01h,30may08,h_k  removed unused fields.
01g,04jul07,wap  Convert to new register access API
01f,20apr07,tor  method update
01e,03apr07,d_c  Method update on behalf of Radzy
01d,06feb07,dtr  UPdate for new PCI config library.
01c,23jul06,wap  Fix project build
01b,22jul06,wap  Remove some BSP dependencies
01a,14apr06,wap  written
*/

#ifndef __INCixp4xxPcih
#define __INCixp4xxPcih

#ifdef __cplusplus
extern "C" {
#endif

#include <hwif/util/vxbDmaBufLib.h>

#define IXPCI_NAME "ixPci"

typedef atomicVal_t SPIN_LOCK;
typedef SPIN_LOCK * SPIN_LOCK_ID;

typedef struct ixpci_drv_ctrl 
    {
    VXB_DEVICE_ID		ixPciDev;
    void *			ixBar;
    void *			ixHandle;
    void			*ixBouncePool;
    PART_ID			ixBouncePart;
    PART_ID			ixBounceCachePart;
    size_t			ixBouncePoolSize;
    size_t			ixBounceCachePoolSize;
    FUNCPTR			ixPciIntAckFunc;
    FUNCPTR			ixPciIntGetFunc;
    struct vxbPciConfig *pPciConfig;
    struct vxbPciInt *pIntInfo;
    int         pciMaxBus;	/* Max number of sub-busses */
    UINT32      autoConfig;

    } IXPCI_DRV_CTRL;

IMPORT STATUS vxbPciBusTypeInit (VXB_DEVICE_ID);

/* Default size of the bounce pool and the cacheable bounce pool is 2MB each */

#ifndef IXPCI_BOUNCE_POOL_SIZE
#define IXPCI_BOUNCE_POOL_SIZE		0x200000
#endif

#ifndef	IXPCI_BOUNCE_CACHE_POOL_SIZE
#define IXPCI_BOUNCE_CACHE_POOL_SIZE	IXPCI_BOUNCE_POOL_SIZE
#endif

/*
 * In order to support any start address of the contiguous memory space,
 * used by the bounce pool and the cacheable bounce pool, within four 16MB
 * DMA windows, which are combined to form a single 64MB window,
 * the max size of the two pools is currently set 48MB.
 */

#define IXPCI_BOUNCE_MAX_TOTAL_POOL_SIZE	0x03000000

/* max size of the bounce pool is 48MB */

#define IXPCI_BOUNCE_MAX_POOL_SIZE \
	IXPCI_BOUNCE_MAX_TOTAL_POOL_SIZE

/*
 * The PCI bridge only has a 64MB DMA window, starting at address 0.
 * If a PHYS_LIMIT defines the upper bound of the window: buffers below
 * this limit can be DMA'ed directly, but buffers above it must be
 * bounce-buffered.
 */

#define IXPCI_PHYS_LIMIT	0x4000000

/* PCI configuration registers for the IXP4xx controller */

#define IXPCI_CFG_DIDVID	0x00	/* Device/vendor ID */
#define IXPCI_CFG_SRCR		0x04	/* status/control register */
#define IXPCI_CFG_CCRID		0x08	/* class code/revision ID */
#define IXPCI_CFG_BHLC		0x0C	/* BIST/hdr/lat/cache line */
#define IXPCI_CFG_BAR0		0x10	/* AHB address bank 0 */
#define IXPCI_CFG_BAR1		0x14	/* AHB address bank 1 */
#define IXPCI_CFG_BAR2		0x18	/* AHB address bank 2 */
#define IXPCI_CFG_BAR3		0x1C	/* AHB address bank 3 */
#define IXPCI_CFG_BAR4		0x20	/* CSR addressing */
#define IXPCI_CFG_BAR5		0x24	/* IO addressing */
#define IXPCI_CFG_SIDSVID	0x2C	/* subsys/subven ID */
#define IXPCI_CFG_LATINT	0x30	/* Maxlat/mingnt/intpin/intline */
#define IXPCI_CFG_RTOTTO	0x40	/* retry timeout, trdy timeout */

/* Location in IXP4xx address space where PCI devices are mapped. */

#define IXPCI_PCI_BASE                (0x48000000)


#define IXPCI_INTA_PIN 11
#define IXPCI_INTB_PIN 10
#define IXPCI_INTC_PIN 9
#define IXPCI_INTD_PIN 8

#define IXPCI_INTA_INTERRUPT_NUM 28
#define IXPCI_INTB_INTERRUPT_NUM 27
#define IXPCI_INTC_INTERRUPT_NUM 26
#define IXPCI_INTD_INTERRUPT_NUM 25

#define IXPCI_MAX_DEV 4

#define IXPCI_PHYS_TO_BUS (x)	\
	(((x) & 0x00FFFFFF) | IXP425_PCI_BAR_0_DEFAULT)

#ifndef BIT
#define BIT(x) (1<<(x))
#endif

/* Mask definitions*/
#define IXP425_PCI_TOP_WORD_OF_LONG_MASK        0xffff0000
#define IXP425_PCI_TOP_BYTE_OF_LONG_MASK        0xff000000
#define IXP425_PCI_BOTTOM_WORD_OF_LONG_MASK     0x0000ffff
#define IXP425_PCI_BOTTOM_TRIBYTES_OF_LONG_MASK 0x00ffffff
#define IXP425_PCI_BOTTOM_NIBBLE_OF_LONG_MASK   0x0000000f
#define IXP425_PCI_MAX_UINT32                   0xffffffff

#define IXP425_PCI_BAR_QUERY                    0xffffffff


#define IXPCI_PCIADDR_MASK 0x00ffffff

/*Configuration command bit definitions*/
#define IXPCI_CFG_CMD_IOAE BIT(0) 
#define IXPCI_CFG_CMD_MAE  BIT(1)
#define IXPCI_CFG_CMD_BME  BIT(2)
#define IXPCI_CFG_CMD_MWIE BIT(4)
#define IXPCI_CFG_CMD_SER  BIT(8)
#define IXPCI_CFG_CMD_FBBE BIT(9)
#define IXPCI_CFG_CMD_MDPE BIT(24)
#define IXPCI_CFG_CMD_STA  BIT(27)
#define IXPCI_CFG_CMD_RTA  BIT(28)
#define IXPCI_CFG_CMD_RMA  BIT(29)
#define IXPCI_CFG_CMD_SSE  BIT(30)
#define IXPCI_CFG_CMD_DPE  BIT(31)

/*Register addressing definitions for PCI controller configuration
  and status registers*/

#define IXPCI_CSR_BASE (0xC0000000)

#define IXPCI_NP_AD_OFFSET       (0x00)
#define IXPCI_NP_CBE_OFFSET      (0x04)
#define IXPCI_NP_WDATA_OFFSET    (0x08)
#define IXPCI_NP_RDATA_OFFSET    (0x0C)
#define IXPCI_CRP_OFFSET         (0x10)
#define IXPCI_CRP_WDATA_OFFSET   (0x14)
#define IXPCI_CRP_RDATA_OFFSET   (0x18)
#define IXPCI_CSR_OFFSET         (0x1C)
#define IXPCI_ISR_OFFSET         (0x20)
#define IXPCI_INTEN_OFFSET       (0x24)
#define IXPCI_DMACTRL_OFFSET     (0x28)
#define IXPCI_AHBMEMBASE_OFFSET  (0x2C)
#define IXPCI_AHBIOBASE_OFFSET   (0x30)
#define IXPCI_PCIMEMBASE_OFFSET  (0x34)
#define IXPCI_AHBDOORBELL_OFFSET (0x38)
#define IXPCI_PCIDOORBELL_OFFSET (0x3C)
#define IXPCI_ATPDMA0_AHBADDR    (0x40)
#define IXPCI_ATPDMA0_PCIADDR    (0x44)
#define IXPCI_ATPDMA0_LENADDR    (0x48)
#define IXPCI_ATPDMA1_AHBADDR    (0x4C)
#define IXPCI_ATPDMA1_PCIADDR    (0x50)
#define IXPCI_ATPDMA1_LENADDR    (0x54)
#define IXPCI_PTADMA0_AHBADDR    (0x58)
#define IXPCI_PTADMA0_PCIADDR    (0x5C)
#define IXPCI_PTADMA0_LENADDR    (0x60)
#define IXPCI_PTADMA1_AHBADDR    (0x64)
#define IXPCI_PTADMA1_PCIADDR    (0x68)
#define IXPCI_PTADMA1_LENADDR    (0x6C)

/*Non prefetch registers bit definitions*/

#define IXPCI_NP_CMD_INTACK      (0x0)
#define IXPCI_NP_CMD_SPECIAL     (0x1)
#define IXPCI_NP_CMD_IOREAD      (0x2)
#define IXPCI_NP_CMD_IOWRITE     (0x3)
#define IXPCI_NP_CMD_MEMREAD     (0x6)
#define IXPCI_NP_CMD_MEMWRITE    (0x7)
#define IXPCI_NP_CMD_CONFIGREAD  (0xa)
#define IXPCI_NP_CMD_CONFIGWRITE (0xb)

#define IXPCI_NP_CBE_BESL  (4)
#define IXPCI_NP_AD_FUNCSL (8)

/*Configuration Port register bit definitions*/
#define IXPCI_CRP_WRITE BIT(16)


/*CSR Register bit definitions*/
#define IXPCI_CSR_HOST  BIT(0)
#define IXPCI_CSR_ARBEN BIT(1)
#define IXPCI_CSR_ADS   BIT(2)
#define IXPCI_CSR_PDS   BIT(3)
#define IXPCI_CSR_ABE   BIT(4)
#define IXPCI_CSR_DBT   BIT(5)
#define IXPCI_CSR_ASE   BIT(8)
#define IXPCI_CSR_IC    BIT(15)

/*ISR (Interrupt status) Register bit definitions*/
#define IXPCI_ISR_PSE   BIT(0)
#define IXPCI_ISR_PFE   BIT(1)
#define IXPCI_ISR_PPE   BIT(2)
#define IXPCI_ISR_AHBE  BIT(3)
#define IXPCI_ISR_APDC  BIT(4)
#define IXPCI_ISR_PADC  BIT(5)
#define IXPCI_ISR_ADB   BIT(6)
#define IXPCI_ISR_PDB   BIT(7)

/*INTEN (Interrupt Enable) Register bit definitions*/
#define IXPCI_INTEN_PSE   BIT(0)
#define IXPCI_INTEN_PFE   BIT(1)
#define IXPCI_INTEN_PPE   BIT(2)
#define IXPCI_INTEN_AHBE  BIT(3)
#define IXPCI_INTEN_APDC  BIT(4)
#define IXPCI_INTEN_PADC  BIT(5)
#define IXPCI_INTEN_ADB   BIT(6)
#define IXPCI_INTEN_PDB   BIT(7)

/*DMACTRL DMA Control and status Register*/
#define IXPCI_DMACTRL_APDCEN  BIT(0)
#define IXPCI_DMACTRL_APDC0   BIT(4)
#define IXPCI_DMACTRL_APDE0   BIT(5)
#define IXPCI_DMACTRL_APDC1   BIT(6)
#define IXPCI_DMACTRL_APDE1   BIT(7)
#define IXPCI_DMACTRL_PADCEN  BIT(8)
#define IXPCI_DMACTRL_PADC0   BIT(12)
#define IXPCI_DMACTRL_PADE0   BIT(13)
#define IXPCI_DMACTRL_PADC1   BIT(14)
#define IXPCI_DMACTRL_PADE1   BIT(15)

/*DMA length registers bit definitions - these are common to all
 four DMA length registers*/
#define IXPCI_DMA_LEN_BE BIT(28)
#define IXPCI_DMA_LEN_EN BIT(31)

#define IXP425_PCI_DMA_MAX_LEN 0xffff

/* number of IRQs mapped on PCI interrupt */

#define IXPCI_IRQ_LINES		4

/*define the GPIO pins used for the 4 PCI interrupts*/
#define IXP425_GPIO_PIN_11 11
#define IXP425_GPIO_PIN_10 10
#define IXP425_GPIO_PIN_9  9
#define IXP425_GPIO_PIN_8  8

#define IXPCI_INT_LVL0		28
#define IXPCI_INT_LVL1		27
#define IXPCI_INT_LVL2		26
#define IXPCI_INT_LVL3		25

/*Size of DMA request pools*/
#define NUM_REQ_POOL 10

#define IX_BAR(p)   ((IXPCI_DRV_CTRL *)(p)->pDrvCtrl)->ixBar
#define IX_HANDLE(p)   ((IXPCI_DRV_CTRL *)(p)->pDrvCtrl)->ixHandle

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (IX_HANDLE(pDev), (UINT32 *)((char *)IX_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (IX_HANDLE(pDev),                             \
        (UINT32 *)((char *)IX_BAR(pDev) + addr), data)

#define CSR_SETBIT_4(pDev, offset, val)		\
	CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)		\
	CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

/* externals */

IMPORT STATUS (*_func_ixPciDmaMapIpcomLoad)(VXB_DEVICE_ID,
                                            VXB_DMA_TAG_ID,
                                            VXB_DMA_MAP_ID,
                                            struct Ipcom_pkt_struct *,
                                            int);

IMPORT STATUS ixPciDmaFragFill
    (
    VXB_DMA_TAG_ID      dmaTagID,       /* DMA Tag ID */
    VXB_DMA_MAP_ID      map,            /* DMA Map ID */
    void *              buf,            /* virtual address of the buffer */
    bus_size_t          bufLen,         /* buffer length */
    int                 flags,          /* flags */
    int *               pNSegs,         /* pointer to the segment number */
    void **             ppBounceBufCur  /* ptr to the cur bounce buf adr */
    );

#ifdef __cplusplus
}
#endif

#endif /* __INCixp4xxPcih */
