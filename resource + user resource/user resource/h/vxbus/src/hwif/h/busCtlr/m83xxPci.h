/* m83xxPci.h - Freescale MPC83xx PCI bridge setup header file */

/* 
* Copyright (c) 2010 Wind River Systems, Inc. 
*
* The right to copy, distribute, modify or otherwise make use
* of this software may be licensed only pursuant to the terms
* of an applicable Wind River license agreement.
*/

/*
modification history
--------------------
01a,04dec10,y_y  created
*/

#ifndef __INCm83xxPcih
#define __INCm83xxPcih

#ifdef __cplusplus
    extern "C" {
#endif /* __cplusplus */

#define M83XXPCI_LAWBAR0                0x60
#define M83XXPCI_LAWAR0                 0x64
#define M83XXPCI_LAWBAR1                0x68
#define M83XXPCI_LAWAR1                 0x6C
#define M83XXPCI0_REG_BASE              0x8000
#define M83XXPCI_LAWAR_ENABLE           0x80000000
#define M83XXPCI_PREFETCHABLE           0x20000000

/* LAWAR SIZE Settings */

#define  M83XXPCI_LAWAR_SIZE_4KB        0x0000000B
#define  M83XXPCI_LAWAR_SIZE_8KB        0x0000000C
#define  M83XXPCI_LAWAR_SIZE_16KB       0x0000000D
#define  M83XXPCI_LAWAR_SIZE_32KB       0x0000000E
#define  M83XXPCI_LAWAR_SIZE_64KB       0x0000000F
#define  M83XXPCI_LAWAR_SIZE_128KB      0x00000010
#define  M83XXPCI_LAWAR_SIZE_256KB      0x00000011
#define  M83XXPCI_LAWAR_SIZE_512KB      0x00000012
#define  M83XXPCI_LAWAR_SIZE_1MB        0x00000013
#define  M83XXPCI_LAWAR_SIZE_2MB        0x00000014
#define  M83XXPCI_LAWAR_SIZE_4MB        0x00000015
#define  M83XXPCI_LAWAR_SIZE_8MB        0x00000016
#define  M83XXPCI_LAWAR_SIZE_16MB       0x00000017
#define  M83XXPCI_LAWAR_SIZE_32MB       0x00000018
#define  M83XXPCI_LAWAR_SIZE_64MB       0x00000019
#define  M83XXPCI_LAWAR_SIZE_128MB      0x0000001A
#define  M83XXPCI_LAWAR_SIZE_256MB      0x0000001B
#define  M83XXPCI_LAWAR_SIZE_512MB      0x0000001C
#define  M83XXPCI_LAWAR_SIZE_1GB        0x0000001D
#define  M83XXPCI_LAWAR_SIZE_2GB        0x0000001E

#ifndef PCI_MSTR_MEM_BUS
#define PCI_MSTR_MEM_BUS                0x40000000 /* align on 512 MB */
#endif

#define COMMAND_REGISTER_OFFSET         0x4
#define COMMAND_REGISTER_WIDTH          0x2
#define BRIDGE_BAR0_OFFSET              0x10
#define BRIDGE_BAR0_WIDTH               0x4

/* Control and Status registers */ 

#define M83XXPCI_ESR_REG                0x500 /* Error Status Register */
#define M83XXPCI_GCR_REG                0x520 /* General Control Register */
#define M83XXPCI_ECR_REG                0x524 /* Error Control Register */
#define M83XXPCI_GSR_REG                0x528 /* General Status Register */

/* configuration space reg and int ack */

#define M83XXPCI_CONF_ADDR              0x300
#define M83XXPCI_CONF_DATA              0x304

/* Outbound translation and base address registers */

#define M83XXPCI_OB_TRANS_ADRS_REG0     0x400
#define M83XXPCI_OB_BASE_ADRS_REG0      0x408
#define M83XXPCI_OB_TRANS_ADRS_REG1     0x418
#define M83XXPCI_OB_BASE_ADRS_REG1      0x420
#define M83XXPCI_OB_TRANS_ADRS_REG2     0x430
#define M83XXPCI_OB_BASE_ADRS_REG2      0x438
#define M83XXPCI_OB_TRANS_ADRS_REG3     0x448
#define M83XXPCI_OB_BASE_ADRS_REG3      0x450
#define M83XXPCI_OB_TRANS_ADRS_REG4     0x460
#define M83XXPCI_OB_BASE_ADRS_REG4      0x468

/* Outbound attributes register definitions */

#define M83XXPCI_OB_ATTR_REG0           0x410
#define M83XXPCI_OB_ATTR_REG1           0x428
#define M83XXPCI_OB_ATTR_REG2           0x440
#define M83XXPCI_OB_ATTR_REG3           0x458
#define M83XXPCI_OB_ATTR_REG4           0x470

/* Outbound Comparison mask register defines */

#define M83XXPCI_OB_WINDOW_ENABLE_BIT   0x80000000
#define M83XXPCI_OB_ATTR_IO_BIT         0x40000000
#define M83XXPCI_OB_ATTR_SE_BIT         0x20000000
#define M83XXPCI_OB_ATTR_WS_4K          0x000FFFFF
#define M83XXPCI_OB_ATTR_WS_8K          0x000FFFFE
#define M83XXPCI_OB_ATTR_WS_16K         0x000FFFFC
#define M83XXPCI_OB_ATTR_WS_32K         0x000FFFF8
#define M83XXPCI_OB_ATTR_WS_64K         0x000FFFF0
#define M83XXPCI_OB_ATTR_WS_128K        0x000FFFE0
#define M83XXPCI_OB_ATTR_WS_256K        0x000FFFC0
#define M83XXPCI_OB_ATTR_WS_512K        0x000FFF80
#define M83XXPCI_OB_ATTR_WS_1M          0x000FFF00
#define M83XXPCI_OB_ATTR_WS_2M          0x000FFE00
#define M83XXPCI_OB_ATTR_WS_4M          0x000FFC00
#define M83XXPCI_OB_ATTR_WS_8M          0x000FF800
#define M83XXPCI_OB_ATTR_WS_16M         0x000FF000
#define M83XXPCI_OB_ATTR_WS_32M         0x000FE000
#define M83XXPCI_OB_ATTR_WS_64M         0x000FC000
#define M83XXPCI_OB_ATTR_WS_128M        0x000F8000
#define M83XXPCI_OB_ATTR_WS_256M        0x000F0000
#define M83XXPCI_OB_ATTR_WS_512M        0x000E0000
#define M83XXPCI_OB_ATTR_WS_1G          0x000C0000
#define M83XXPCI_OB_ATTR_WS_2G          0x00080000
#define M83XXPCI_OB_ATTR_WS_4G          0x00000000

/* Inbound translation and base address registers */

#define M83XXPCI_IB_TRANS_ADRS_REG0     0x568
#define M83XXPCI_IB_BASE_ADRS_REG0      0x570
#define M83XXPCI_IB_ATTR_REG0           0x578
#define M83XXPCI_IB_TRANS_ADRS_REG1     0x550
#define M83XXPCI_IB_BASE_ADRS_REG1      0x558
#define M83XXPCI_IB_ATTR_REG1           0x560
#define M83XXPCI_IB_TRANS_ADRS_REG2     0x538
#define M83XXPCI_IB_BASE_ADRS_REG2      0x540
#define M83XXPCI_IB_ATTR_REG2           0x548 

/* Inbound Window Attribute register defines */

#define M83XXPCI_IB_ATTR_IWS_4K         0x0000000B
#define M83XXPCI_IB_ATTR_IWS_8K         0x0000000C
#define M83XXPCI_IB_ATTR_IWS_16K        0x0000000D
#define M83XXPCI_IB_ATTR_IWS_32K        0x0000000E
#define M83XXPCI_IB_ATTR_IWS_64K        0x0000000F
#define M83XXPCI_IB_ATTR_IWS_128K       0x00000010
#define M83XXPCI_IB_ATTR_IWS_256K       0x00000011
#define M83XXPCI_IB_ATTR_IWS_512K       0x00000012
#define M83XXPCI_IB_ATTR_IWS_1M         0x00000013
#define M83XXPCI_IB_ATTR_IWS_2M         0x00000014
#define M83XXPCI_IB_ATTR_IWS_4M         0x00000015
#define M83XXPCI_IB_ATTR_IWS_8M         0x00000016
#define M83XXPCI_IB_ATTR_IWS_16M        0x00000017
#define M83XXPCI_IB_ATTR_IWS_32M        0x00000018
#define M83XXPCI_IB_ATTR_IWS_64M        0x00000019
#define M83XXPCI_IB_ATTR_IWS_128M       0x0000001A
#define M83XXPCI_IB_ATTR_IWS_256M       0x0000001B
#define M83XXPCI_IB_ATTR_IWS_512M       0x0000001C
#define M83XXPCI_IB_ATTR_IWS_1G         0x0000001D
#define M83XXPCI_IB_ATTR_IWS_2G         0x0000001E
#define M83XXPCI_IB_WINDOW_ENABLE_BIT   0x80000000
#define M83XXPCI_IB_ATTR_PREFETCHABLE   0x20000000
#define M83XXPCI_IB_ATTR_RTT_READ_SNOOP         0x00050000
#define M83XXPCI_IB_ATTR_RTT_WRITE_SNOOP        0x00005000
#define M83XXPCI_IB_ATTR_RTT_READ_NO_SNOOP      0x00040000
#define M83XXPCI_IB_ATTR_RTT_WRITE_NO_SNOOP     0x00004000

/* Encoding direction defines */

#define M83XXPCI_IN_BOUND               0
#define M83XXPCI_OUT_BOUND              1
#define PCI_SNOOP_ENABLE                0x40000000
#define PCI_PREFETCHABLE                0x20000000

/* PCI error Registers */

/* Comand status register defines */

#define BUS_MASTER_ENABLE_BIT           0x4
#define MEMORY_SPACE_ACCESS_ENABLE_BIT  0x2

/* PCI Host Bridge setup File */

#define MAX_NUM_VECTORS 4

 /* DrvCtrl */
 
typedef struct m83xxPciDrvCtrl
    {
    VXB_DEVICE_ID   pInst;
    void *      handle;
    UINT32      magicNumber1;
    UINT32      magicNumber2;
    int         pciMaxBus;    /* Max number of sub-busses */
    
    void *      mem32Addr;
    UINT32      mem32Size;
    void *      memIo32Addr;
    UINT32      memIo32Size;
    void *      io32Addr;
    UINT32      io32Size;

    void *      pimmrBase;
    void *      mstrMemBus;
    void *      lclMemAddr;
    UINT32      lclMemMapSize;

    UINT32      tgtIf;
    UINT32      owAttrMem;
    UINT32      owAttrMemIo;
    UINT32      owAttrIo;
    UINT32      iwAttr;
    UINT32      singleLawBar;    
    UINT32      singleLawBarSize;    
    UINT32      pciExpressHost;
    UINT32      autoConfig;
    UINT32*     lawbar;         /* LAWBAR address  */
    UINT32*     lawar;          /* LAWAR  address*/
    UINT32      lawarAttr;      /* LAWAR  attributes */
    BOOL        initDone;

    struct      vxbPciConfig *pPciConfig;
    struct      vxbPciInt *pIntInfo;
    struct      hcfDevice * pHcf;
    } M83XXPCI_DRV_CTRL;

/* defines */

#define M83XXPCI_REG_READ32(offset, result) \
        result = m83xxPciRegRead((UINT32 *)((UINT32)pInst->pRegBase[0] + offset)) 

#define M83XXPCI_REG_WRITE32(offset, val) \
        m83xxPciRegWrite((UINT32 *)((UINT32)pInst->pRegBase[0] + offset), (UINT32)val)

#ifdef __cplusplus
    }
#endif /* __cplusplus */

#endif /* __INCm83xxPcih */
