/* usbXhcdStructures.h - USB XHCI Driver Data Structure Definitions */


/*
 * Copyright (c) 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,09may11,w_x  written
*/

#ifndef __INCusbXhcdStructuresh
#define __INCusbXhcdStructuresh

#ifdef __cplusplus
extern "C" {
#endif

#include <vxWorks.h>
#include <hwif/vxbus/vxBus.h>
#include <hwif/util/vxbDmaBufLib.h>

/*
 * This file defines the interface data structures used to communicate control,
 * status and data between HCD (software) and the xHCI (hardware). The data
 * structure definitions in this file support a 32-bit or 64-bit memory buffer
 * address space.
 */

#define USB_XHCD_LO32(uData64) \
    ((UINT32)(uData64))
#define USB_XHCD_HI32(uData64) \
    ((UINT32)(((uData64) >> 16) >> 16))
#define USB_XHCD_FORM_UINT64(lo32, hi32) \
    (UINT64)((UINT64)(lo32) + (UINT64)(((UINT64)(hi32)) << 32))
    
/*
 * Max Size and alignment requirements of the various xHCI data structures.
 *
 * Note that software shall ensure that no interface data structure with a
 * Max Size less than or equal to 64KB spans a 64KB boundary, and that no
 * interface data structure with a Max Size less than or equal to PAGESIZE
 * spans a PAGESIZE boundary.
 *
 * See Table 49:
 * Data Structure Max Size, Boundary, and Alignment Requirement Summary
 */

/* Use the most common page size and minimum supported by xHCI */

#define USB_XHCI_DEFAULT_PAGESIZE       4096

/* Device Context Base Address Array max size and alignment restriction */

#define USB_XHCI_DCBAA_MAX_SIZE         2048
#define USB_XHCI_DCBAA_ALIGNMENT        64

/* Device Context max size and alignment restriction */

#define USB_XHCI_DEV_CTX_MAX_SIZE32     1024
#define USB_XHCI_DEV_CTX_MAX_SIZE64     2048
#define USB_XHCI_DEV_CTX_ALIGNMENT      64

/* 
 * We allow one more context entry to be allocated for 
 * Device Context so that it is the same size as the Input 
 * Context. This is to easy the managment of Context memory.
 */

#define USB_XHCI_CTX_ALIGNMENT          64
#define USB_XHCI_CTX_MAX_SIZE32         (1024 + 32)
#define USB_XHCI_CTX_MAX_SIZE64         (2048 + 64)

/* Slot Context max size and alignment restriction */

#define USB_XHCI_SLOT_CTX_MAX_SIZE      64
#define USB_XHCI_SLOT_CTX_ALIGNMENT     64

/* Endpoint Context max size and alignment restriction */

#define USB_XHCI_EP_CTX_MAX_SIZE        64
#define USB_XHCI_EP_CTX_ALIGNMENT       64

/* Stream Context max size and alignment restriction */

#define USB_XHCI_STREAM_CTX_MAX_SIZE    16
#define USB_XHCI_STREAM_CTX_ALIGNMENT   16

/* Steam Array (Linear) max size and alignment restriction */

#define USB_XHCI_LSCA_MAX_SIZE    0x100000 /* 1MB */
#define USB_XHCI_LSCA_ALIGNMENT   16
#define USB_XHCI_LSCA_MAX_CTX     65536

/* Steam Array (Pri/Sec) max size and alignment restriction */

#define USB_XHCI_PSCA_MAX_SIZE    4096 /* 4KB */
#define USB_XHCI_PSCA_ALIGNMENT   16
#define USB_XHCI_PSCA_MAX_CTX     256 

/*
 * Transfer or Event Ring segments and Command Ring segment
 * max size and alignment restriction. Note that Transfer or
 * Event Ring segments alignment can be 16, but Command Ring
 * segment alignment should be at least 64, so we use 64 as
 * the common size for all ring types.
 */

#define USB_XHCI_RING_MAX_SIZE          (64 * 1024) /* 64KB */
#define USB_XHCI_RING_ALIGNMENT         64

/* Event Ring Segment Table max size and alignment restriction */

#define USB_XHCI_ERST_MAX_SIZE          (512 * 1024) /* 512KB */
#define USB_XHCI_ERST_ALIGNMENT         64
#define USB_XHCI_ERST_MAX_ENTRIES       (64) 

/* Scratchpad Buffer Array max size and alignment restriction */

#define USB_XHCI_SPBA_MAX_SIZE          248
#define USB_XHCI_SPBA_ALIGNMENT         64

/* Scratchpad Buffers max size and alignment restriction */

#define USB_XHCI_SPB_MAX_SIZE          4096
#define USB_XHCI_SPB_ALIGNMENT         USB_XHCI_DEFAULT_PAGESIZE

/*
 * Boundary which data structure shall not span.
 *
 * Steam Array (Linear) and Event Ring Segment Table has no bounday limits;
 *
 * Device Context Base Address Array, Device Context, Slot Context,
 * Endpoint Context, Stream Context, Steam Array (Pri/Sec),
 * Scratchpad Buffer Array, and Scratchpad Buffers structures all have
 * PAGESIZE bounardy limits;
 *
 * Transfer or Event Ring segments and Command Ring segment have 64KB
 * boundary limits.
 */

#define USB_XHCI_COMMON_BOUNDARY_LIMIT  USB_XHCI_DEFAULT_PAGESIZE
#define USB_XHCI_SEG_BOUNDARY_LIMIT    (64 * 1024) /* 64 KB */

/*
 * Device Context Base Address Array related definitions.
 *
 * The Device Context Base Address Array entry associated with
 * each allocated Device Slot shall contain a 64-bit pointer
 * to the base of the associated Device Context.
 *
 * The Device Context Base Address Array shall be indexed by the
 * Device Slot ID, aligned to a 64 byte boundary, physically
 * contiguous, contain MaxSlotsEn + 1 entries. The maximum size
 * of the Device Context Base Array is 256 64-bit entries,
 * or 2K Bytes.
 *
 * If the Max Scratchpad Buffers field of the HSCPARAMS2 register
 * is > '0', then the first entry (entry_0) in the DCBAA shall
 * contain a pointer to the Scratchpad Buffer Array. If the Max
 * Scratchpad Buffers field of the HSCPARAMS2 register is = '0',
 * then the first entry (entry_0) in the DCBAA is reserved and
 * shall be cleared to '0' by software.
 */

#define USB_XHCI_MAX_DEV_SLOTS          256
#define USB_XHCI_DCBAA_ENTRY_MASK       (~0x3FULL) 
#define USB_XHCI_DCBAA_SCRATCHPAD_BUFF_ARRAY_IDX    0

/* TRB Transfer Length MAX (Valid values are 0 to 64K) */

#define USB_XHCI_TRB_MAX_XFER_SIZE      (64 * 1024)
#define USB_XHCI_TRB_MAX_XFER_SHIFT     (17)
#define USB_XHCI_TRB_MAX_XFER_MASK      (0x1FFFF)

/*
 * 6.2.1 Device Context
 *
 * The Device Context data structure consists of 32 entries.
 * The first entry (entry_0) is the Slot Context data structure
 * and the remaining entries are Endpoint Context data structures.
 *
 * The Device Context data structure is used in the xHCI as Output
 * by the xHC to report device configuration and state information
 * to system software.
 *
 * The Device Context Index (DCI) is used to reference the respective
 * element of the Device Context data structure.
 *
 * The xHC uses 32 byte Device Context data structures if the Context
 * Size (CSZ) field in the HCCPARAMS register = '0'. If the Context
 * Size (CSZ) field = '1' then the Device Context data structures
 * consume 64 bytes each.
 */

/*
 * 4.5.1 Device Context Index
 *
 * The term Device Context Index (DCI) is used throughout this document
 * to reference an individual context data structure in the Device Context. 
 * The range of DCI values is 0 to 31.
 *
 * The DCI of the Slot Context is 0.
 *
 * For Device Context Indices 1-31, the following rules apply:
 *
 * 1) For Isoch, Interrupt, or Bulk type endpoints the DCI is calculated
 *    from the Endpoint Number and Direction with the following formula;
 *
 * DCI = (Endpoint Number * 2) + Direction,
 *
 * where Direction = '0' for OUT endpoints and '1' for IN endpoints.
 *
 * 2) For Control type endpoints:
 *
 * DCI = (Endpoint Number * 2) + 1.
 */

#define USB_XHCI_SLOT_CTX_DCI 0
#define USB_XHCI_CTRL_EP_CTX_DCI(uEpAddr) \
    ((((uEpAddr) & 0xF) * 2) + 1)
#define USB_XHCI_NON_CTRL_EP_CTX_DCI(uEpAddr) \
    ((((uEpAddr) & 0xF) * 2) + (((uEpAddr) >> 7) & 0x1))
    
/*
 * 6.2.5  Input Context
 *
 * The Input Context data structure specifies the endpoints and the
 * operations to be performed on those endpoints by the Address Device,
 * Configure Endpoint, and Evaluate Context Commands.
 *
 * The Input Context is pointed to by an Input Context Pointer field
 * of a Address Device, Configure Endpoint, and Evaluate Context Command TRBs.
 * The Input Context is an array of up to 33 context data structure entries.
 *
 * The first entry (offset 000h) of the Input Context shall be the Input
 * Control Context data structure. The remaining entries shall be organized
 * identically to the Device Context data structures.
 *
 * If the Add Context flag is set for an entry in the Input Context, then
 * the entry shall be initialized appropriately by software. All other
 * entries of the Input Context are ignored by the xHC. The Add Context
 * and Drop Context flag indices are calculated identically to the Device
 * Context Index (DCI) for the Device Context portion of the Input Context.
 * e.g. EP context 1 OUT maps to D2 and A2, and so on, up to EP 15 IN
 * mapping to D31 and A31.
 *
 * Note: The xHC uses 32 bytes Input Control Context data structures if the
 * Context Size (CSZ) field in the HCCPARAMS register = '0'. If the Context
 * Size (CSZ) field = '1' then the Input Control Context data structures
 * consume 64 bytes each. The offsets shall be 040h for the Slot Context,
 * 080h for EP Context 0, and so on.
 */

#define USB_XHCI_DEV_CTX_TYPE       0x1
#define USB_XHCI_INPUT_CTX_TYPE     0x2

/*
 * 6.2.2 Slot Context Data Structure
 *
 * The Slot Context data structure defines information that applies
 * to a device as a whole.
 *
 * uSlotInfo0 - Route String, Speed, Multi-TT (MTT), Hub, Context Entries;
 * uSlotInfo1 - Max Exit Latency, Root Hub Port Number, Number of Ports;
 * uSlotInfo2 - TT Hub Slot ID, TT Port Number, TT Think Time (TTT),
 *              Interrupter Target;
 * uSlotInfo3 - USB Device Address,Slot State
 */

typedef struct usbXhciSlotCtx
    {
    UINT32 uSlotInfo0; /* See comments above */
    UINT32 uSlotInfo1; /* See comments above */
    UINT32 uSlotInfo2; /* See comments above */
    UINT32 uSlotInfo3; /* See comments above */
    UINT32 uRsvdO[4];  /* If CSZ = 1 then there are 32 more padding bytes */
    }USB_XHCI_SLOT_CTX, *pUSB_XHCI_SLOT_CTX;

/* usbXhciSlotCtx::uSlotInfo0 bit fields - Start */

/*
 * Route String. This field is used by hubs to route packets to the correct
 * downstream port. The format of the Route String is defined in section 8.9
 * the USB3 specification.
 * As Input, this field shall be set for all USB devices, irrespective of
 * their speed, to indicate their location in the USB topology
 */

#define USB_XHCI_SLOT_CTX_ROUTE_STRING_OFFSET   0
#define USB_XHCI_SLOT_CTX_ROUTE_STRING_MASK     (0xFFFFF)
#define USB_XHCI_SLOT_CTX_ROUTE_STRING_GET(uSlotInfo0) \
    ((uSlotInfo0) & 0xFFFFF)
#define USB_XHCI_SLOT_CTX_ROUTE_STRING(uRouteStr) \
    ((uRouteStr) & 0xFFFFF)

/*
 * Speed. This field indicates the speed of the device. Refer to the PORTSC
 * Port Speed field in Table 35 for the definition of the valid values.
 */

#define USB_XHCI_SLOT_CTX_SPEED_OFFSET   20
#define USB_XHCI_SLOT_CTX_SPEED_MASK     (0xF << 20)
#define USB_XHCI_SLOT_CTX_SPEED_GET(uSlotInfo0) \
    (((uSlotInfo0) >> 20) & 0xF)
#define USB_XHCI_SLOT_CTX_SPEED(uSpeed) \
    (((uSpeed) & 0xF) << 20)

/*
 * Multi-TT (MTT). This flag is set to '1' by software if this is a
 * High-speed hub (Speed = '3' and Hub = '1') that supports Multiple TTs,
 * or if this is a Low-/Full-speed device (Speed = '1' or '2', and Hub = '0')
 * and connected to the xHC through a parentb High-speed hub that supports
 * Multiple TTs, or '0' if not.
 */

#define USB_XHCI_SLOT_CTX_MTT_OFFSET   25
#define USB_XHCI_SLOT_CTX_MTT_MASK   (0x1 << 25)
#define USB_XHCI_SLOT_CTX_MTT_GET(uSlotInfo0) \
    (((uSlotInfo0) >> 25) & 0x1)
#define USB_XHCI_SLOT_CTX_MTT(uMTT) \
    ((((uMTT)) & 0x1 ) << 25)

/*
 * Hub. This flag is set to '1' by software if this device is a USB hub,
 * or '0' if it is a USB function.
 */

#define USB_XHCI_SLOT_CTX_HUB_OFFSET   26
#define USB_XHCI_SLOT_CTX_HUB_MASK   (0x1 << 26)
#define USB_XHCI_SLOT_CTX_HUB_GET(uSlotInfo0) \
    (((uSlotInfo0) >> 26) & 0x1)
#define USB_XHCI_SLOT_CTX_HUB(uHub) \
    (((uHub) & 0x1) << 26)

/*
 * Context Entries. This field identifies the index of the last valid
 * Endpoint Context within this Device Context structure. The value of '0'
 * is Reserved and is not a valid entry for this field. Valid entries
 * for this field shall be in the range of 1-31. This field indicates
 * the size of the Device Context structure. For example, if CSZ = 1 then
 * ((Context Entries + 1) * 32 bytes) = Total bytes for this structure.
 */

#define USB_XHCI_SLOT_CTX_ENTRIES_OFFSET   27
#define USB_XHCI_SLOT_CTX_ENTRIES_MASK   (0x1F << 27)
#define USB_XHCI_SLOT_CTX_ENTRIES_GET(uSlotInfo0) \
    (((uSlotInfo0) >> 27) & 0x1F)
#define USB_XHCI_SLOT_CTX_ENTRIES(uEntries) \
    (((uEntries) & 0x1F) << 27)

/* usbXhciSlotCtx::uSlotInfo0 bit fields - End */

/* usbXhciSlotCtx::uSlotInfo1 bit fields - Start */

/*
 * Max Exit Latency. The Maximum Exit Latency is in microseconds, and
 * indicates the worst case time it takes to wake up all the links in
 * the path to the device, given the current USB link level power
 * management settings.
 */

#define USB_XHCI_SLOT_CTX_MAX_EXIT_LATENCY_OFFSET   0
#define USB_XHCI_SLOT_CTX_MAX_EXIT_LATENCY_MASK   0xFFFF
#define USB_XHCI_SLOT_CTX_MAX_EXIT_LATENCY_GET(uSlotInfo1) \
    (((uSlotInfo1) >> 0) & 0xFFFF)
#define USB_XHCI_SLOT_CTX_MAX_EXIT_LATENCY(uLatency) \
    (((uLatency) & 0xFFFF) << 0)

/*
 * Root Hub Port Number. This field identifies the Root Hub Port Number
 * used to access the USB device.
 *
 * Note: Ports are numbered from 1 to MaxPorts.
 */

#define USB_XHCI_SLOT_CTX_RH_PORT_OFFSET   16
#define USB_XHCI_SLOT_CTX_RH_PORT_MASK   (0xFF << 16)
#define USB_XHCI_SLOT_CTX_RH_PORT_GET(uSlotInfo1) \
    (((uSlotInfo1) >> 16) & 0xFF)
#define USB_XHCI_SLOT_CTX_RH_PORT(uRhPort) \
    (((uRhPort) & 0xFF) << 16)

/*
 * Number of Ports. If this device is a hub (Hub = '1'), then this field
 * is set by software to identify the number of downstream facing ports
 * supported by the hub. Refer to the bNbrPorts field description in
 * the Hub Descriptor (Table 11-13) of the USB2 spec.
 */

#define USB_XHCI_SLOT_CTX_NUM_PORTS_OFFSET   24
#define USB_XHCI_SLOT_CTX_NUM_PORTS_MASK   (0xFF << 24)
#define USB_XHCI_SLOT_CTX_NUM_PORTS_GET(uSlotInfo1) \
    (((uSlotInfo1) >> 24) & 0xFF)
#define USB_XHCI_SLOT_CTX_NUM_PORTS(uNumPorts) \
    (((uNumPorts) & 0xFF) << 24)

/* usbXhciSlotCtx::uSlotInfo1 bit fields - End */

/* usbXhciSlotCtx::uSlotInfo2 bit fields - Start */

/*
 * TT Hub Slot ID. If this device is Low-/Full-speed and connected through
 * a High-speed hub, then this field shall contain the Slot ID of the parent
 * High-speed hub. If this device is attached to a Root Hub port or it is
 * not Low-/Full-speed then this field shall be '0'.
 */

#define USB_XHCI_SLOT_CTX_TT_HUB_SLOT_ID_OFFSET   0
#define USB_XHCI_SLOT_CTX_TT_HUB_SLOT_ID_MASK   (0xFF)
#define USB_XHCI_SLOT_CTX_TT_HUB_SLOT_ID_GET(uSlotInfo2) \
    (((uSlotInfo2) >> 0) & 0xFF)
#define USB_XHCI_SLOT_CTX_TT_HUB_SLOT_ID(uTtHubSlotId) \
    (((uTtHubSlotId) & 0xFF) << 0)

/*
 * TT Port Number. If this device is Low-/Full-speed and connected through
 * a High-speed hub, then this field contains the number of the downstream
 * facing port of the parent High-speed hub. If this device is attached to
 * a Root Hub port or it is not Low-/Full-speed then this field shall be '0'.
 */

#define USB_XHCI_SLOT_CTX_TT_PORT_OFFSET   8
#define USB_XHCI_SLOT_CTX_TT_PORT_MASK   (0xFF << 8)
#define USB_XHCI_SLOT_CTX_TT_PORT_GET(uSlotInfo2) \
    (((uSlotInfo2) >> 8) & 0xFF)
#define USB_XHCI_SLOT_CTX_TT_PORT(uTtPort) \
    (((uTtPort) & 0xFF) << 8)

/*
 * TT Think Time (TTTT). If this is a High-speed hub (Hub = '1' and Speed =
 * High-Speed device attached ('3')), then this field shall be set by
 * software to identify the time the TT of the hub requires to proceed
 * to the next full-/low-speed transaction.
 *     Value         Think Time
 *       0           TT requires at most 8 FS bit times of inter-transaction
 *                   gap on a full-/low-speed downstream bus.
 *       1           TT requires at most 16 FS bit times.
 *       2           TT requires at most 24 FS bit times.
 *       3           TT requires at most 32 FS bit times.
 * Refer to the TT Think Time sub-field of the wHubCharacteristics field
 * description in the Hub Descriptor (Table 11-13) and section 11.18.2 of
 * the USB2 spec for more information on TT Think Time.
 */

#define USB_XHCI_SLOT_CTX_TT_TT_OFFSET   16
#define USB_XHCI_SLOT_CTX_TT_TT_MASK   (0x3 << 16)
#define USB_XHCI_SLOT_CTX_TT_TT_GET(uSlotInfo2) \
    (((uSlotInfo2) >> 16) & 0x3)
#define USB_XHCI_SLOT_CTX_TT_TT(uTTTT) \
    (((uTTTT) & 0x3) << 16)

/*
 * Interrupter Target. This field defines the index of the Interrupter
 * that will receive Bandwidth Request Events and Device Notification Events
 * generated by this slot, or when a Ring Underrun or Ring Overrun condition
 * is reported (refer to section 4.10.3.1). Valid values are between 0 and
 * MaxIntrs-1.
 */

#define USB_XHCI_SLOT_CTX_INTR_TARGET_OFFSET   22
#define USB_XHCI_SLOT_CTX_INTR_TARGET_MASK   (0x3FF << 22)
#define USB_XHCI_SLOT_CTX_INTR_TARGET_GET(uSlotInfo2) \
    (((uSlotInfo2) >> 22) & 0x3FF)
#define USB_XHCI_SLOT_CTX_INTR_TARGET(uIntrTarget) \
    (((uIntrTarget) & 0x3FF) << 22)

/* usbXhciSlotCtx::uSlotInfo2 bit fields - End */

/* usbXhciSlotCtx::uSlotInfo3 bit fields - Start */

/*
 * USB Device Address. This field identifies the address assigned to the
 * USB device by the xHC, and is set upon the successful completion of a
 * Set Address Command. Refer to the USB2 spec for a more detailed description.
 * As Output, this field is invalid if the Slot State = Disabled or Default.
 * As Input, software shall initialize the field to '0'.
 */

#define USB_XHCI_SLOT_CTX_DEV_ADDR_OFFSET   0
#define USB_XHCI_SLOT_CTX_DEV_ADDR_MASK  0xFF
#define USB_XHCI_SLOT_CTX_DEV_ADDR_GET(uSlotInfo3) \
    (((uSlotInfo3) >> 0) & 0xFF)
#define USB_XHCI_SLOT_CTX_DEV_ADDR(uDevAddr) \
    (((uDevAddr) & 0xFF) << 0)

/*
 * Slot State. This field is updated by the xHC when a Device Slot
 * transitions from one state to another.
 *      Value           Slot State
 *        0             Disabled/Enabled
 *        1             Default
 *        2             Addressed
 *        3             Configured
 *       31-4           Reserved
 * As Output, since software initializes all fields of the Device Context
 * data structure to '0', this field shall initially indicate the Disabled
 * state.
 * As Input, software shall initialize the field to '0'.
 */

#define USB_XHCI_SLOT_CTX_SLOT_STATE_OFFSET   27
#define USB_XHCI_SLOT_CTX_SLOT_STATE_MASK  (0x1F << 27)
#define USB_XHCI_SLOT_CTX_SLOT_STATE_GET(uSlotInfo3) \
    (((uSlotInfo3) >> 27) & 0x1F)
#define USB_XHCI_SLOT_CTX_SLOT_STATE(uSlotState) \
    (((uSlotState) & 0x1F) << 27)

/* usbXhciSlotCtx::uSlotInfo3 bit fields - End */

/*
 * 6.2.3 Endpoint Context Data Structure
 *
 * The Endpoint Context data structure defines information that applies to
 * a specific endpoint.
 *
 * Note:  Unless otherwise stated: As Input, all fields of the Endpoint
 * Context shall be initialized to the appropriate value by software before
 * issuing a command. As Output, the xHC shall update each field to reflect
 * the current value that it is using
 *
 * uEpInfo0 - Endpoint State (EP State), Mult, Max Primary Streams (MaxPStreams),
 *            Linear Stream Array (LSA), Interval
 * uEpInfo1 - Force Event (FE), Error Count (CErr), Endpoint Type (EP Type),
 *            Host Initiate Disable (HID), Max Burst Size, Max Packet Size
 * uEpDeqPtr - Dequeue Cycle State (DCS), TR Dequeue Pointer
 * uEpInfo2 - Average TRB Length, Max Endpoint Service Time Interval Payload
 *           (Max ESIT Payload)
 */

typedef struct usbXhciEpCtx
    {
    UINT32 uEpInfo0; /* See comments above */
    UINT32 uEpInfo1; /* See comments above */
    UINT32 uEpDeqLo; /* See comments above */
    UINT32 uEpDeqHi; /* See comments above */
    UINT32 uEpInfo2; /* See comments above */
    UINT32 uRsvdO[3];/* If CSZ = 1 then there are 32 more padding bytes */
    }USB_XHCI_EP_CTX, *pUSB_XHCI_EP_CTX;


/* usbXhciEpCtx::uEpInfo0 bit fields - Start */

/*
 * Endpoint State (EP State). The Endpoint State identifies the current
 * operational state of the endpoint.
 *     Value          Definition
 *       0            Disabled        The endpoint is not operational
 *       1            Running         The endpoint is operational, either
 *                                    waiting for a doorbell ring or
 *                                    processing TDs.
 *       2            Halted          The endpoint is halted due to a Halt
 *                                    condition detected on the USB. SW shall
 *                                    issue Reset Endpoint Command to recover
 *                                    from the Halt condition and transition
 *                                    to the Stopped state. SW may manipulate
 *                                    the Transfer Ring.
 *       3            Stopped         The endpoint is not running due to a
 *                                    Stop Endpoint Command or recovering from
 *                                    a Halt condition. SW may manipulate the
 *                                    Transfer Ring.
 *       4            Error           The endpoint is not running due to a
 *                                    TRB Error. SW may manipulate the Transfer
 *                                    Ring.
 *      5-7           Reserved
 * As Output, a Running to Halted transition is forced by the xHC if a STALL
 * condition is detected on the endpoint. A Running to Error transition is
 * forced by the xHC if a TRB Error condition is detected.
 * As Input, this field is initialized to '0' by software.
 */

#define USB_XHCI_EP_CTX_EP_STATE_OFFSET   0
#define USB_XHCI_EP_CTX_EP_STATE_MASK   0x7
#define USB_XHCI_EP_CTX_EP_STATE_GET(uEpInfo0) \
    (((uEpInfo0) >> 0) & 0x7)
#define USB_XHCI_EP_CTX_EP_STATE(uEpState) \
    (((uEpState) & 0x7) << 0)
    
#define USB_XHCI_EP_STATE_DISABLED      0
#define USB_XHCI_EP_STATE_RUNNING       1
#define USB_XHCI_EP_STATE_HALTED        2
#define USB_XHCI_EP_STATE_STOPPED       3
#define USB_XHCI_EP_STATE_ERROR         4

/*
 * Mult. This field indicates the maximum number of bursts within an
 * Interval that this endpoint supports, where the valid range of values is
 * '0' to '2', where '0' = 1 burst, '1' = 2 bursts, etc.
 * This field shall be '0' for all endpoint types except for SS Isochronous.
 */

#define USB_XHCI_EP_CTX_MULT_OFFSET   8
#define USB_XHCI_EP_CTX_MULT_MASK   (0x3 << 8)
#define USB_XHCI_EP_CTX_MULT_GET(uEpInfo0) \
    (((uEpInfo0) >> 8) & 0x3)
#define USB_XHCI_EP_CTX_MULT(uMult) \
    (((uMult) & 0x3) << 8)
    
/*
 * Max Primary Streams (MaxPStreams). This field identifies the maximum
 * number of Primary Stream IDs this endpoint supports. Valid values are
 * defined below. If the value of this field is '0', then the TR Dequeue
 * Pointer field shall point to a Transfer Ring. If this field is > '0'
 * then the TR Dequeue Pointer field shall point to a Primary Stream
 * Context Array.
 *
 * A value of '0' indicates that Streams are not supported by this endpoint
 * and the Endpoint Context TR Dequeue Pointer field references a Transfer Ring.
 * A value of '1' to '15' indicates that the Primary Stream ID Width is
 * MaxPstreams+1 and the Primary Stream Array contains 2^(MaxPStreams+1)
 * entries.
 *
 * For SS Bulk endpoints, the range of valid values for this field is
 * defined by the MaxPSASize field in the HCCPARAMS register.
 *
 * This field shall be '0' for all SS Control, Isoch, and Interrupt endpoints,
 * and for all non-SS endpoints.
 */

#define USB_XHCI_EP_CTX_MAX_PRI_STREAMS_OFFSET   10
#define USB_XHCI_EP_CTX_MAX_PRI_STREAMS_MASK   (0x1F << 10)
#define USB_XHCI_EP_CTX_MAX_PRI_STREAMS_GET(uEpInfo0) \
    (((uEpInfo0) >> 10) & 0x1F)
#define USB_XHCI_EP_CTX_MAX_PRI_STREAMS(uMaxPriStreams) \
    (((uMaxPriStreams) & 0x1F) << 10)

/*
 * Linear Stream Array (LSA). This field identifies how a Stream ID shall
 * be interpreted. Setting this bit to a value of '1' shall disable
 * Secondary Stream Arrays and a Stream ID shall be interpreted as a linear
 * index into the Primary Stream Array, where valid values for
 * MaxPStreams are '1' to '15'.
 *
 * A value of '0' shall enable Secondary Stream Arrays, where the low order
 * (MaxPStreams+1) bits of a Stream ID shall be interpreted as a linear
 * index into the Primary Stream Array, where valid values for MaxPStreams
 * are '1' to '7'. And the high order bits of a Stream ID shall be
 * interpreted as a linear index into the Secondary Stream Array.
 *
 * If MaxPStreams = '0', this field RsvdZ.
 */

#define USB_XHCI_EP_CTX_LSA_OFFSET   15
#define USB_XHCI_EP_CTX_LSA_MASK   (0x1 << 15)
#define USB_XHCI_EP_CTX_LSA_GET(uEpInfo0) \
    (((uEpInfo0) >> 15) & 0x1)
#define USB_XHCI_EP_CTX_LSA(uLSA) \
    (((uLSA) & 0x1) << 15)

/*
 * Interval. The period between consecutive requests to a USB endpoint
 * to send or receive data. Expressed in 125us increments. The period is
 * calculated as 125us * (2^Interval); e.g., an Interval value of 0 means
 * a period of 125us (2^0 = 1), a value of 1 means a period of 250us, etc.
 * The legal range of values is 3 to 10 for Full- or Low-speed Interrupt
 * endpoints (1 to 256 ms.) and 0 to 15 for all other endpoint types
 * (125us to 4096ms).
 */

#define USB_XHCI_EP_CTX_INTERVAL_OFFSET   16
#define USB_XHCI_EP_CTX_INTERVAL_MASK   (0xFF << 16)
#define USB_XHCI_EP_CTX_INTERVAL_GET(uEpInfo0) \
    (((uEpInfo0) >> 16) & 0xFF)
#define USB_XHCI_EP_CTX_INTERVAL(uInternal) \
    (((uInternal) & 0xFF) << 16)

/* usbXhciEpCtx::uEpInfo0 bit fields - End */

/* usbXhciEpCtx::uEpInfo1 bit fields - Start */

/*
 * Force Event (FE). This bit determines whether Transfer Events will be
 * generated for all Transfer TRBs processed for this endpoint. Setting
 * this bit to a value of '1' will force a Transfer Event to be generated
 * for all Transfer TRBs, irrespective of the state of the IOC and ISP flags.
 * A value of '0' will enable normal operation Event Generation by the IOC
 * and ISP flags.
 *
 * Note: This flag shall only be used for debugging and shall not be set
 * for normal runtime operation of the endpoint.
 */

#define USB_XHCI_EP_CTX_FORCE_EVENT_OFFSET   0
#define USB_XHCI_EP_CTX_FORCE_EVENT_MASK   0x1
#define USB_XHCI_EP_CTX_FORCE_EVENT_GET(uEpInfo1) \
    (((uEpInfo1) >> 0) & 0x1)
#define USB_XHCI_EP_CTX_FORCE_EVENT(uForceEvent) \
    (((uForceEvent) & 0x1) << 0)

/*
 * Error Count (CErr). This field defines a 2-bit down count, which identifies
 * the number of consecutive USB Bus Errors allowed while executing a TD.
 * If this field is programmed with a non-zero value when the Endpoint Context
 * is initialized, the xHC loads this value into an internal Bus Error
 * Counter before executing a USB transaction and decrements it if the
 * transaction fails. If the Bus Error Counter counts from '1' to '0', the xHC
 * ceases execution of the TRB, sets the endpoint to the Halted state, and
 * generates a USB Transaction Error Event for the TRB that caused the internal
 * Bus Error Counter to decrement to '0'. If system software programs this
 * field to '0', the xHC shall not count errors for TRBs on the Endpoint's
 * Transfer Ring and there shall be no limit on the number of TRB retries.
 */

#define USB_XHCI_EP_CTX_CERR_OFFSET   1
#define USB_XHCI_EP_CTX_CERR_MASK   (0x3 << 1)
#define USB_XHCI_EP_CTX_CERR_GET(uEpInfo1) \
    (((uEpInfo1) >> 1) & 0x3)
#define USB_XHCI_EP_CTX_CERR(uCerr) \
    (((uCerr) & 0x3) << 1)
#define USB_XHCI_EP_CTX_CERR_MAX    3

/*
 * Endpoint Type (EP Type). This field identifies whether an Endpoint
 * Context is Valid, and if so, what type of endpoint the context defines.
 *
 *     Value     Endpoint Type      Direction
 *       0        Not Valid           N/A
 *       1        Isoch               Out
 *       2        Bulk                Out
 *       3        Interrupt           Out
 *       4        Control             Bidirectional
 *       5        Isoch               In
 *       6        Bulk                In
 *       7        Interrupt           In
 */

#define USB_XHCI_EP_CTX_EP_TYPE_OFFSET   3
#define USB_XHCI_EP_CTX_EP_TYPE_MASK   (0x7 << 3)
#define USB_XHCI_EP_CTX_EP_TYPE_GET(uEpInfo1) \
    (((uEpInfo1) >> 3) & 0x7)
#define USB_XHCI_EP_CTX_EP_TYPE(uType)        \
    (((uType) & 0x7) << 3)

#define USB_XHCI_ISOC_OUT_EP    1
#define USB_XHCI_BULK_OUT_EP    2
#define USB_XHCI_INT_OUT_EP     3
#define USB_XHCI_CTRL_EP        4
#define USB_XHCI_ISOC_IN_EP     5
#define USB_XHCI_BULK_IN_EP     6
#define USB_XHCI_INT_IN_EP      7

/*
 * Host Initiate Disable (HID). This field affects Stream enabled endpoints,
 * allowing the Host Initiated Stream selection feature to be disabled for
 * the endpoint. Setting this bit to a value of '1' shall disable the Host
 * Initiated Stream selection feature. A value of '0' will enable normal
 * Stream operation.
 */

#define USB_XHCI_EP_CTX_HID_OFFSET   7
#define USB_XHCI_EP_CTX_HID_MASK   (0x1 << 7)
#define USB_XHCI_EP_CTX_HID_GET(uEpInfo1) \
    (((uEpInfo1) >> 7) & 0x1)
#define USB_XHCI_EP_CTX_HID(uHID) \
    (((uHID) & 0x1) << 7)

/*
 * Max Burst Size. This field indicates to the xHC the maximum number
 * of consecutive USB transactions that should be executed per scheduling
 * opportunity. This is a "zero-based" value, where 0 to 15 represents
 * burst sizes of 1 to 16, respectively.
 */

#define USB_XHCI_EP_CTX_MAX_BURST_SIZE_OFFSET   8
#define USB_XHCI_EP_CTX_MAX_BURST_SIZE_MASK   (0xFF << 8)
#define USB_XHCI_EP_CTX_MAX_BURST_SIZE_GET(uEpInfo1) \
    (((uEpInfo1) >> 8) & 0xFF)
#define USB_XHCI_EP_CTX_MAX_BURST_SIZE(uMaxBurstSize) \
    (((uMaxBurstSize) & 0xFF) << 8)

/*
 * Max Packet Size. This field indicates the maximum packet size in
 * bytes that this endpoint is capable of sending or receiving when
 * configured.
 */

#define USB_XHCI_EP_CTX_MAX_PACKET_SIZE_OFFSET   16
#define USB_XHCI_EP_CTX_MAX_PACKET_SIZE_MASK   (0xFFFF << 16)
#define USB_XHCI_EP_CTX_MAX_PACKET_SIZE_GET(uEpInfo1) \
    (((uEpInfo1) >> 16) & 0xFFFF)
#define USB_XHCI_EP_CTX_MAX_PACKET_SIZE(uMaxPacketSize) \
    (((uMaxPacketSize) & 0xFFFF) << 16)

/* usbXhciEpCtx::uEpInfo1 bit fields - End */

/* usbXhciEpCtx::uEpDeqPtr bit fields - Start */

/*
 * Dequeue Cycle State (DCS). This bit identifies the value of the xHC
 * Consumer Cycle State (CCS) flag for the TRB referenced by the TR
 * Dequeue Pointer. This field shall be '0' if MaxPStreams > '0'.
 */

#define USB_XHCI_EP_CTX_DCS_OFFSET   0
#define USB_XHCI_EP_CTX_DCS_MASK   0x1
#define USB_XHCI_EP_CTX_DCS_GET(uEpDeqPtr) \
    (((uEpDeqPtr) >> 0) & 0x1)
#define USB_XHCI_EP_CTX_DCS(uDCS) \
    (((uDCS) & 0x1) << 0)

/*
 * TR Dequeue Pointer. As Input, this field represents the high order bits
 * of the 64-bit base address of a Transfer Ring or a Stream Context Array
 * associated with this endpoint. If MaxPStreams = '0' then this field shall
 * point to a Transfer Ring. If MaxPStreams > '0' then this field shall point
 * to a Stream Context Array.
 *
 * As Output, if MaxPStreams = '0' this field shall be used by the xHC to
 * store the value of the Dequeue Pointer when the endpoint enters the
 * Halted or Stopped states, and the value of the this field shall be
 * undefined when the endpoint is not in the Halted or Stopped states. if
 * MaxPStreams > '0' then this field shall point to a Stream Context Array.
 *
 * The memory structure referenced by this physical memory pointer shall
 * be aligned to a 16-byte boundary.
 */

#define USB_XHCI_EP_CTX_TR_DEQ_PTR_OFFSET   0 /* We use 0 instead of 4 */
#define USB_XHCI_EP_CTX_TR_DEQ_PTR_MASK   (~0xF)
#define USB_XHCI_EP_CTX_TR_DEQ_PTR_GET(uEpDeqPtr) \
        ((uEpDeqPtr) & (~0xF))
#define USB_XHCI_EP_CTX_TR_DEQ_PTR(uEpDeqPtr) \
        ((uEpDeqPtr) & (~0xF))

/* usbXhciEpCtx::uEpDeqPtr bit fields - End */

/* usbXhciEpCtx::uEpInfo2 bit fields - Start */

/*
 * Average TRB Length. This field represents the average Length of the TRBs
 * executed by this endpoint. The xHC shall use this parameter to calculate
 * system bus bandwidth requirements.
 */

#define USB_XHCI_EP_CTX_AVRG_TRB_LEN_OFFSET   0
#define USB_XHCI_EP_CTX_AVRG_TRB_LEN_MASK   0xFFFF
#define USB_XHCI_EP_CTX_AVRG_TRB_LEN_GET(uEpInfo2) \
    (((uEpInfo2) >> 0) & 0xFFFF)
#define USB_XHCI_EP_CTX_AVRG_TRB_LEN(uAvrgTrbLen) \
    (((uAvrgTrbLen) & 0xFFFF) << 0)

/* 
 * Reasonable initial values of Average TRB Length for Control endpoints 
 * would be 8B, Interrupt endpoints 1KB, and Bulk and Isoch endpoints 3KB.
 */
 
#define USB_XHCI_EP_CTX_AVRG_TRB_LEN_CTRL   (8)
#define USB_XHCI_EP_CTX_AVRG_TRB_LEN_INTR   (1024)
#define USB_XHCI_EP_CTX_AVRG_TRB_LEN_BULK   (3 * 1024)
#define USB_XHCI_EP_CTX_AVRG_TRB_LEN_ISOC   (3 * 1024)

/*
 * Max Endpoint Service Time Interval Payload (Max ESIT Payload).
 * This field represents the total number of bytes this endpoint will
 * transfer during an ESIT. This field is only valid for periodic endpoints.
 * For periodic endpoints, this value is used by the xHC to reserve the
 * bus time in the pipe schedule.
 */

#define USB_XHCI_EP_CTX_MAX_ESIT_PAYLOAD_OFFSET   16
#define USB_XHCI_EP_CTX_MAX_ESIT_PAYLOAD_MASK   (0xFFFF << 16)
#define USB_XHCI_EP_CTX_MAX_ESIT_PAYLOAD_GET(uEpInfo2) \
    (((uEpInfo2) >> 16) & 0xFFFF)
#define USB_XHCI_EP_CTX_MAX_ESIT_PAYLOAD(uMaxEsitPayload) \
    (((uMaxEsitPayload) & 0xFFFF) << 16)

/* usbXhciEpCtx::uEpInfo2 bit fields - End */

/*
 * 6.2.4 Stream Context Array
 *
 * The xHCI supports hierarchal Stream Context Arrays. A Stream Context Array
 * contains Stream Context data structures. Entries are addressed by a Stream
 * ID. The 0th entry of a Stream Context Array is reserved and does not
 * reference a Transfer Ring or another Stream Context Array.
 */

/*
 * 6.2.4.1 Stream Context Data Structure
 *
 * The Stream Context data structure defines information that applies to
 * a specific Stream associated with an endpoint.
 *
 * Note: Unless otherwise stated: As Input, all fields of the Stream Context
 * shall be initialized to the appropriate value by software before issuing
 * a command. As Output, the xHC shall update each field to reflect the
 * current value that it is using.
 *
 * Note: The Context Size (CSZ) field in the HCCPARAMS register does not
 * apply to Stream Context data structures, they are always 16 bytes in size.
 *
 * uStreamDeqPtr - Dequeue Cycle State (DCS), Stream Context Type (SCT),
 *                 TR Dequeue Pointer
 */

typedef struct usbXhciStreamCtx
    {
    UINT64 uStreamDeqPtr; /* See comments above */
    UINT32 uRsvdO[2];
    }USB_XHCI_STREAM_CTX, *pUSB_XHCI_STREAM_CTX;

/* usbXhciStreamCtx::uStreamDeqPtr bit fields - Start */

/*
 * Dequeue Cycle State (DCS). This bit identifies the value of the xHC
 * Consumer Cycle State (CCS) flag for the TRB referenced by the TR
 * Dequeue Pointer.
 */

#define USB_XHCI_STREAM_CTX_DCS_OFFSET   0
#define USB_XHCI_STREAM_CTX_DCS_MASK   0x1
#define USB_XHCI_STREAM_CTX_DCS_GET(uStreamDeqPtr) \
    (((uStreamDeqPtr) >> 0) & 0x1)
#define USB_XHCI_STREAM_CTX_DCS(uDCS) \
    (((uDCS) & 0x1) << 0)

/*
 * Stream Context Type (SCT). This field identifies whether the Stream
 * Context is a member of a Primary or Secondary Stream Context Array,
 * if the TR Dequeue Pointer field references a Transfer Ring or a Stream
 * Context Array, and if a Stream Context Array is referenced, the size
 * of the array.
 *     Value      Stream Array Type     Dequeue Ptr   Secondary Stream Array Size
 *        0        Secondary             Transfer Ring     N/A
 *        1        Primary               Transfer Ring     N/A
 *        2        Primary               SSA              8
 *        3        Primary               SSA              16
 *        4        Primary               SSA              32
 *        5        Primary               SSA              64
 *        6        Primary               SSA              128
 *        7        Primary               SSA              256
 */

#define USB_XHCI_STREAM_CTX_SCT_OFFSET   1
#define USB_XHCI_STREAM_CTX_SCT_MASK   (0x7 << 1)
#define USB_XHCI_STREAM_CTX_SCT_GET(uStreamDeqPtr) \
    (((uStreamDeqPtr) >> 1) & 0x7)
#define USB_XHCI_STREAM_CTX_SCT(uSCT) \
    (((uSCT) & 0x7) << 1)

#define USB_XHCI_SCT_SECONDARY          0
#define USB_XHCI_SCT_PRIMARY            1
#define USB_XHCI_SCT_PRIMARY_SSA8       2
#define USB_XHCI_SCT_PRIMARY_SSA16      3
#define USB_XHCI_SCT_PRIMARY_SSA32      4
#define USB_XHCI_SCT_PRIMARY_SSA64      5
#define USB_XHCI_SCT_PRIMARY_SSA128     6
#define USB_XHCI_SCT_PRIMARY_SSA256     7


/*
 * TR Dequeue Pointer. This field represents the high order bits of the
 * 64-bit base address of the TRB ring or Stream Context Array associated
 * with this Stream.
 *
 * The memory structure referenced by this physical memory pointer shall
 * be aligned to a 16-byte boundary. This field is initialized by software
 * and shall be overwritten by the xHC to save the value of the Dequeue
 * Pointer when the endpoint enters the Halted or Stopped states. The
 * value of the this field shall be undefined when the endpoint is not in
 * the Halted or Stopped states.
 */

#define USB_XHCI_STREAM_CTX_TR_DEQ_PTR_OFFSET   0 /* We use 0 instead of 4 */
#define USB_XHCI_STREAM_CTX_TR_DEQ_PTR_MASK   (~0xFULL)
#define USB_XHCI_STREAM_CTX_TR_DEQ_PTR_GET(uStreamDeqPtr) \
        (((uStreamDeqPtr)) & (~0xFULL))
#define USB_XHCI_STREAM_CTX_TR_DEQ_PTR(uTrDeqPtr) \
        ((uTrDeqPtr) & (~0xFULL))

/* usbXhciStreamCtx::uStreamDeqPtr bit fields - End */

/*
 * 6.2.5.1 Input Control Context
 *
 * The Input Control Context data structure defines which Device Context
 * data structures are affected by a command and the operations to be
 * performed on those contexts.
 *
 * uDropFlags - Drop Context flags (D2 - D31)
 *
 * uAddFlags - Add Context flags (A0 - A31)
 */

typedef struct usbXhciInputCtrlCtx
    {
    UINT32 uDropFlags;
    UINT32 uAddFlags;
    UINT32 uRsvdZ[6];
    }USB_XHCI_INPUT_CTRL_CTX, *pUSB_XHCI_INPUT_CTRL_CTX;

/* usbXhciInputCtrlCtx::uDropFlags bit fields - Start */

/*
 * Drop Context flags (D2 - D31). These single bit fields identify which
 * Device Context data structures should be disabled by command. If set
 * to '1', the respective Endpoint Context shall be disabled. If cleared
 * to '0', the Endpoint Context is ignored.
 */

#define    USB_XHCI_DROP_EP(x)    (0x1 << (x))

/* usbXhciInputCtrlCtx::uDropFlags bit fields - End */

/* usbXhciInputCtrlCtx::uAddFlags bit fields - Start */

/*
 * Add Context flags (A0 - A31). These single bit fields identify which
 * Device Context data structures shall be evaluated and/or enabled by
 * a command. If set to '1', the respective Context shall be evaluated.
 * If cleared to '0', the Context is ignored.
 */

#define    USB_XHCI_ADD_EP(x)    (0x1 << (x))

/* usbXhciInputCtrlCtx::uAddFlags bit fields - End */

/*
 * 6.2.6 Port Bandwidth Context
 *
 * The Port Bandwidth Context data structure is used to provide system
 * software with the percentage of periodic bandwidth available on each
 * Root Hub Port, at the Speed indicated by the Device Speed field of
 * the Get Port Bandwidth Command. Software allocates the Context data
 * structure and the xHC updates it during the execution of a Get Port
 * Bandwidth Command.
 */

/*
 * Generic Port Bandwidth Context data structure. System sizes this data
 * structure as function of the number of Root Hub ports supported by the xHC
 * (i.e. MaxPorts). Software shall round up the size of the buffer to the
 * nearest Dword boundary.
 */

typedef struct usbXhciPortBandwidthCtx
    {
    UINT8 *     pPortBandwidth;
    bus_addr_t  uDmaBusAddr;
    UINT32      uMaxPorts;
    }USB_XHCI_PORT_BANDWIDTH_CTX,*pUSB_XHCI_PORT_BANDWIDTH_CTX;

/*
 * Port Bandwidth. Percentage of Total Available Bandwidth available on Port.
 *
 * Note: The range of valid values depends on the value of the Dev Speed
 * field in the Get Port Bandwidth Command. 0 to 80% for HS, and 0 to 90%
 * for SS and FS. Refer to section 4.14.2 for more information.
 *
 * Note: The Port fields of the Port Bandwidth Context shall report decimal
 * percentage values in hex, i.e. 0Ah = 10%, 50h = 80%, etc.
 */

#define USB_XHCI_PORT_BANDWIDTH(pPortBandwidthCtx, uPort) \
    ((pPortBandwidthCtx)->pPortBandwidth[(uPort)])


/*
 * 6.4 Transfer Request Block (TRB)
 *
 * The Transfer Request Block is the basic building block upon which all xHC
 * USB transfers are constructed. All Transfer Request Blocks shall be aligned
 * on a 16-byte boundary. TRBs are used for all transactions performed by an
 * xHC, which includes commands sent to the host controller, events generated
 * by the host controller, and transactions associated with USB endpoints.
 */

/*
 * 6.4.1 Transfer TRBs
 *
 * A Transfer TRBs shall be found on a Transfer Ring. A Work Item on a
 * Transfer Ring is called a Transfer Descriptor (TD) and is comprised of one
 * or more Transfer TRB data structures.
 *
 * Note: If a zero-length transfer is specified, the Data Buffer Pointer field
 * is ignored by the xHC, irrespective of the state of the IDT flag.
 *
 * Note: Data buffers referenced by Transfer TRBs shall not span 64KB
 * boundaries. If a physical data buffer spans a 64KB boundary, software
 * shall chain multiple TRBs to describe the buffer.
 */

/*
 * 6.4.1.1 Normal TRB
 *
 * A Normal TRB is used in several ways; exclusively on Bulk and Interrupt
 * Transfer Rings for normal and Scatter/Gather operations, to define
 * additional data buffers for Scatter/Gather operations on Isoch Transfer
 * Rings, and to define the Data stage information for Control Transfer Rings.
 *
 * uData - Data Buffer Pointer Hi and Lo, 64-bit address of the TRB data
 *         area for this transaction or 8 bytes of immediate data;
 * uInfo - TRB Transfer Length, TD Size, Interrupter Target;
 * uCtrl - Cycle bit (C), Evaluate Next TRB (ENT), Interrupt-on Short Packet
 *         (ISP), No Snoop (NS), Chain bit (CH), Interrupt On Completion (IOC),
 *         Immediate Data (IDT), TRB Type.
 */

typedef struct usbXhciTRB
    {
    UINT32 uDataLo; /* See comments above */
    UINT32 uDataHi; /* See comments above */
    UINT32 uInfo;   /* See comments above */
    UINT32 uCtrl;   /* See comments above */
    }USB_XHCI_TRB, *pUSB_XHCI_TRB;

/* usbXhciTRB::uData bit fields - Start */

/*
 * Data Buffer Pointer Hi and Lo. These fields represent the 64-bit
 * address of the TRB data area for this transaction or 8 bytes of
 * immediate data. The Immediate Data (IDT) control flag selects this
 * option for each Normal TRB.
 *
 * The memory structure referenced by this physical memory pointer is
 * allowed to begin on a byte address boundary. However, user may find
 * other alignments, such as 64-byte or 128-byte alignments, to be more
 * efficient and provide better performance.
 */

#define USB_XHCI_TRB_DATA64_TRB_PTR_OFFSET (0)
#define USB_XHCI_TRB_DATA64_TRB_PTR_MASK (~0xFULL)
#define USB_XHCI_TRB_DATA64_TRB_PTR_GET(uData64) \
        ((uData64) & (~0xFULL))
#define USB_XHCI_TRB_DATA64_TRB_PTR(uData64) \
        ((uData64) & (~0xFULL))

/* Note: This is only used for Control Transfer Setup Stage TRB */

#define USB_XHCI_TRB_DATALO_bmRequestType_OFFSET 0
#define USB_XHCI_TRB_DATALO_bmRequestType_MASK (0xFF)
#define USB_XHCI_TRB_DATALO_bmRequestType_GET(uDataLo) \
        (((uDataLo) >> 0) & 0xFF)
#define USB_XHCI_TRB_DATALO_bmRequestType(uValue) \
        (((uValue) & 0xFF) << 0)

/* Note: This is only used for Control Transfer Setup Stage TRB */

#define USB_XHCI_TRB_DATALO_bRequest_OFFSET 8
#define USB_XHCI_TRB_DATALO_bRequest_MASK ((0xFFULL) << 8)
#define USB_XHCI_TRB_DATALO_bRequest_GET(uDataLo) \
        (((uDataLo) >> 8) & (0xFF))
#define USB_XHCI_TRB_DATALO_bRequest(uValue) \
        (((uValue) & (0xFF)) << 8)

/* Note: This is only used for Control Transfer Setup Stage TRB */

#define USB_XHCI_TRB_DATALO_wValue_OFFSET 16
#define USB_XHCI_TRB_DATALO_wValue_MASK ((0xFFFF) << 16)
#define USB_XHCI_TRB_DATALO_wValue_GET(uDataLo) \
        (((uDataLo) >> 16) & (0xFFFF))
#define USB_XHCI_TRB_DATALO_wValue(uValue) \
        (((uValue) & (0xFFFF)) << 16)

/* Note: This is only used for Control Transfer Setup Stage TRB */

#define USB_XHCI_TRB_DATAHI_wIndex_OFFSET 0
#define USB_XHCI_TRB_DATAHI_wIndex_MASK (0xFFFF)
#define USB_XHCI_TRB_DATAHI_wIndex_GET(uDataHi) \
        (((uDataHi) >> 0) & (0xFFFF))
#define USB_XHCI_TRB_DATAHI_wIndex(uValue) \
        (((uValue) & (0xFFFF)) << 0)

/* Note: This is only used for Control Transfer Setup Stage TRB */

#define USB_XHCI_TRB_DATAHI_wLength_OFFSET 16
#define USB_XHCI_TRB_DATAHI_wLength_MASK ((0xFFFF) << 16)
#define USB_XHCI_TRB_DATAHI_wLength_GET(uDataHi) \
        (((uDataHi) >> 16) & (0xFFFF))
#define USB_XHCI_TRB_DATAHI_wLength(uValue) \
        (((uValue) & (0xFFFF)) << 16)

/*
 * Port ID. The ID of the Root Hub Port that generated this event.
 *
 * Note: This is only used for Port Status Change Event TRB.
 */

#define USB_XHCI_TRB_DATALO_PORT_ID_OFFSET 24
#define USB_XHCI_TRB_DATALO_PORT_ID_MASK (0xFF << 24)
#define USB_XHCI_TRB_DATALO_PORT_ID_GET(uDataLo) \
        (((uDataLo) >> 24) & 0xFF)

/*
 * DB Reason. This field contains the value written to the DB Target field
 * of the associated Doorbell.
 *
 * Note: This is only used for Doorbell Event TRB.
 */

#define USB_XHCI_TRB_DATALO_DB_REASON_OFFSET 0
#define USB_XHCI_TRB_DATALO_DB_REASON_MASK 0x1F
#define USB_XHCI_TRB_DATALO_DB_REASON_GET(uDataLo) \
        (((uDataLo) >> 0) & 0x1F)

/*
 * Notification Type. This field reports the value of the Notification Type
 * field of the received USB Device Notification Transaction Packet.
 *
 * Note: This is only used for Device Notification Event TRB.
 */

#define USB_XHCI_TRB_DATALO_NOTIFY_TYPE_OFFSET 4
#define USB_XHCI_TRB_DATALO_NOTIFY_TYPE_MASK (0xF << 4)
#define USB_XHCI_TRB_DATALO_NOTIFY_TYPE_GET(uDataLo) \
        (((uDataLo) >> 4) & 0xF)

/*
 * Device Notification Data. This field reports the value of bytes 05h
 * through 0Bh of the received USB Device Notification Transaction Packet
 * (DNTP), i.e. Device Notification Event (DNE) TRB byte 01h = DNTP byte 05h,
 * ..., DNE TRB byte 07h = DNTP byte 0Bh.
 *
 * Note: This is only used for Device Notification Event TRB.
 */

#define USB_XHCI_TRB_DATA64_NOTIFY_DATA_OFFSET 8
#define USB_XHCI_TRB_DATA64_NOTIFY_DATA_MASK (~0xFFULL)
#define USB_XHCI_TRB_DATA64_NOTIFY_DATA_GET(uData64) \
        ((uData64) >> 8)

/*
 * Dequeue Cycle State (DCS). This bit identifies the value of the xHC
 * Consumer Cycle State (CCS) flag for the TRB referenced by the TR
 * Dequeue Pointer.
 *
 * Note: This is only used for Set TR Dequeue Pointer Command TRB.
 */

#define USB_XHCI_TRB_DATALO_DCS_OFFSET   0
#define USB_XHCI_TRB_DATALO_DCS_MASK   0x1
#define USB_XHCI_TRB_DATALO_DCS_GET(uDataLo) \
    (((uDataLo) >> 0) & 0x1)
#define USB_XHCI_TRB_DATALO_DCS(uValue) \
    (((uValue) & 0x1) << 0)

/*
 * Stream Context Type (SCT). If the Stream ID field is non-zero, this field
 * identifies the type of the Stream Context, otherwise this field shall be '0'.
 *
 * Note: This is only used for Set TR Dequeue Pointer Command TRB.
 */

#define USB_XHCI_TRB_DATALO_SCT_OFFSET   1
#define USB_XHCI_TRB_DATALO_SCT_MASK   (0x7 << 1)
#define USB_XHCI_TRB_DATALO_SCT_GET(uDataLo) \
    (((uDataLo) >> 1) & 0x7)
#define USB_XHCI_TRB_DATALO_SCT(uValue) \
    (((uValue) & 0x7) << 1)

/*
 * Packet Type (Type). This field identifies the packet type. Refer to
 * section 8.3.1.2 in the USB3 specification for valid values.
 *
 * Note: This is only used for Force Header Command TRB.
 */

#define USB_XHCI_TRB_DATALO_HDR_PKT_TYPE_OFFSET   0
#define USB_XHCI_TRB_DATALO_HDR_PKT_TYPE_MASK   0x1F
#define USB_XHCI_TRB_DATALO_HDR_PKT_TYPE_GET(uDataLo) \
    (((uDataLo) >> 0) & 0x1F)
#define USB_XHCI_TRB_DATALO_HDR_PKT_TYPE(uValue) \
    (((uValue) & 0x1F) << 0)

/* usbXhciTRB::uData bit fields - End */

/* usbXhciTRB::uInfo bit fields - Start */

/*
 * TRB Transfer Length. For an OUT, this field defines the number of data
 * bytes the xHC shall send during the execution of this TRB. If the value
 * of this field is '0' when the xHC fetches this TRB, the xHC shall execute
 * a zero-length transaction and retires the TD.
 *
 * Note: If a zero-length transfer is specified, the Data Buffer Pointer
 * field is ignored by the xHC, irrespective of the state of the IDT flag.
 *
 * For an IN, the value of the field identifies the size of the data buffer
 * referenced by the Data Buffer Pointer, i.e. the number of bytes the host
 * expects the endpoint to deliver. Valid values are 0 to 64K.
 */

#define USB_XHCI_TRB_INFO_XFER_LEN_OFFSET 0
#define USB_XHCI_TRB_INFO_XFER_LEN_MASK 0x1FFFF
#define USB_XHCI_TRB_INFO_XFER_LEN_GET(uInfo) \
        (((uInfo) >> 0) & 0x1FFFF)
#define USB_XHCI_TRB_INFO_XFER_LEN(uValue) \
        (((uValue) & 0x1FFFF) << 0)

/* 
 * The Transfer Event TRB has also a "TRB Transfer Length" field.
 * If the Event Data flag is '0' the legal range of values is 0 to 10000h. 
 * If the Event Data flag is '1' this field is set to the value of the
 * Event Data Transfer Length Accumulator (EDTLA).
 */
#define USB_XHCI_TRB_INFO_ACT_LEN_OFFSET 0
#define USB_XHCI_TRB_INFO_ACT_LEN_MASK 0xFFFFFF
#define USB_XHCI_TRB_INFO_ACT_LEN_GET(uInfo) \
        (((uInfo) >> 0) & 0xFFFFFF)
#define USB_XHCI_TRB_INFO_ACT_LEN(uValue) \
        (((uValue) & 0xFFFFFF) << 0)

/*
 * TD Size. This field provides an indicator of the number of bytes
 * remaining in the TD.
 */

#define USB_XHCI_TRB_INFO_TD_SIZE_OFFSET 17
#define USB_XHCI_TRB_INFO_TD_SIZE_MASK (0x1F << 17)
#define USB_XHCI_TRB_INFO_TD_SIZE_GET(uInfo) \
        (((uInfo) >> 17) & 0x1F)
#define USB_XHCI_TRB_INFO_TD_SIZE(uValue) \
        (((uValue) & 0x1F) << 17)

/*
 * Interrupter Target. This field defines the index of the Interrupter
 * that will receive events generated by this TRB. Valid values are
 * between 0 and MaxIntrs-1.
 */

#define USB_XHCI_TRB_INFO_INTR_TARGET_OFFSET 22
#define USB_XHCI_TRB_INFO_INTR_TARGET_MASK (0x3FF << 22)
#define USB_XHCI_TRB_INFO_INTR_TARGET_GET(uInfo) \
        (((uInfo) >> 22) & 0x3FF)
#define USB_XHCI_TRB_INFO_INTR_TARGET(uValue) \
        (((uValue) & 0x3FF) << 22)

/*
 * Completion Code. This field encodes the completion status that can
 * be identified by a TRB.
 *
 * Note: This is only used for (all) Event TRBs.
 */

#define USB_XHCI_TRB_INFO_COMP_CODE_OFFSET 24
#define USB_XHCI_TRB_INFO_COMP_CODE_MASK (0xFF << 24)
#define USB_XHCI_TRB_INFO_COMP_CODE_GET(uInfo) \
        (((uInfo) >> 24) & 0xFF)

/*
 * TRB Transfer Length. This field shall reflect the residual number of bytes
 * not transferred.
 *
 * For an OUT, this field shall indicate the value of the Length field of the
 * Transfer TRB, minus the data bytes that were successfully transmitted. A
 * successful OUT transfer shall return a Length of '0'.
 *
 * For an IN, this field shall indicate the value of the Length field of the
 * Transfer TRB, minus the data bytes that were successfully received. If the
 * device terminates the receive transfer with a short packet, then this field
 * shall indicate the difference between the expected transfer size (defined
 * by the Transfer TRB) and the actual number of bytes received. If the receive
 * transfer completed with an error, then this field shall indicated the
 * difference between the expected transfer size and the number of bytes
 * successfully received.
 *
 * If the Event Data flag is '0', the legal range of values is 0 to 10000h.
 * If the Event Data flag is '1', this field is set to the value of the Event
 * Data Transfer Length Accumulator (EDTLA).
 *
 * Note: This is only used for Transfer Event TRB.
 */

#define USB_XHCI_TRB_INFO_RESIDUAL_LEN_OFFSET 0
#define USB_XHCI_TRB_INFO_RESIDUAL_LEN_MASK 0xFFFFFF
#define USB_XHCI_TRB_INFO_RESIDUAL_LEN_GET(uInfo) \
        (((uInfo) >> 0) & 0xFFFFFF)

/*
 * Stream ID. If Streams are enabled for this endpoint, this field identifies
 * the Stream Context that will receive the new TR Dequeue Pointer.
 *
 * Note: This is only used for Set TR Dequeue Pointer Command TRB.
 */

#define USB_XHCI_TRB_INFO_STREAM_ID_OFFSET 16
#define USB_XHCI_TRB_INFO_STREAM_ID_MASK (0xFFFF << 16)
#define USB_XHCI_TRB_INFO_STREAM_ID_GET(uInfo) \
        (((uInfo) >> 16) & 0xFFFF)
#define USB_XHCI_TRB_INFO_STREAM_ID(uValue) \
        (((uValue) & 0xFFFF) << 16)

/* usbXhciTRB::uInfo bit fields - End */

/* usbXhciTRB::uCtrl bit fields - Start */

/*
 * Cycle bit (C). This bit is used to mark the Enqueue Pointer of the
 * Transfer ring.
 */

#define USB_XHCI_TRB_CTRL_CYCLE_BIT_OFFSET 0
#define USB_XHCI_TRB_CTRL_CYCLE_BIT_MASK (0x1 << 0)
#define USB_XHCI_TRB_CTRL_CYCLE_BIT_GET(uCtrl) \
            (((uCtrl) >> 0) & 0x1)
#define USB_XHCI_TRB_CTRL_CYCLE_BIT(uValue) \
            (((uValue) & 0x1) << 0)

/*
 * Evaluate Next TRB (ENT). If this flag is '1' the xHC shall fetch and
 * evaluate the next TRB before saving the endpoint state.
 */

#define USB_XHCI_TRB_CTRL_ENT_OFFSET 1
#define USB_XHCI_TRB_CTRL_ENT_MASK (0x1 << 1)
#define USB_XHCI_TRB_CTRL_ENT_GET(uCtrl) \
            (((uCtrl) >> 1) & 0x1)
#define USB_XHCI_TRB_CTRL_ENT(uValue) \
            (((uValue) & 0x1) << 1)


/*
 * Interrupt-on Short Packet (ISP). If this flag is '1' and a Short Packet
 * is encountered for this TRB (i.e., less than the amount specified in TRB
 * Transfer Length), then a Transfer Event TRB shall be generated with its
 * Completion Code set to Short Packet. The TRB Transfer Length field in the
 * Transfer Event TRB shall reflect the residual number of bytes not
 * transferred into the associated data buffer. In either case, when a
 * Short Packet is encountered, the TRB shall be retired without error and
 * the xHC shall advance to the next Transfer Descriptor (TD).
 *
 * Note that if the ISP and IOC flags are both '1' and a Short Packet is
 * detected, then only one Transfer Event TRB shall be queued to the Event
 * Ring.
 */

#define USB_XHCI_TRB_CTRL_ISP_OFFSET 2
#define USB_XHCI_TRB_CTRL_ISP_MASK (0x1 << 2)
#define USB_XHCI_TRB_CTRL_ISP_GET(uCtrl) \
            (((uCtrl) >> 2) & 0x1)
#define USB_XHCI_TRB_CTRL_ISP(uValue) \
            (((uValue) & 0x1) << 2)

/*
 * No Snoop (NS). When set to '1', the xHC is permitted to set the No Snoop
 * bit in the Requester Attributes of the PCIe transactions it initiates
 * if the PCIe configuration Enable No Snoop flag is also set. When cleared
 * to '0', the xHC is not permitted to set PCIe packet No Snoop Requester
 * Attribute.
 *
 * NOTE: If software sets this bit, then it is responsible for maintaining
 * cache consistency.
 */

#define USB_XHCI_TRB_CTRL_NO_SNOOP_OFFSET 3
#define USB_XHCI_TRB_CTRL_NO_SNOOP_MASK (0x1 << 3)
#define USB_XHCI_TRB_CTRL_NO_SNOOP_GET(uCtrl) \
            (((uCtrl) >> 3) & 0x1)
#define USB_XHCI_TRB_CTRL_NO_SNOOP(uValue) \
            (((uValue) & 0x1) << 3)

/*
 * Chain bit (CH). Set to '1' by software to associate this TRB with the
 * next TRB on the Ring. A Transfer Descriptor (TD) is defined as one or
 * more TRBs. The Chain bit is used to identify the TRBs that comprise a TD.
 * The Chain bit is always '0' in the last TRB of a TD.
 */

#define USB_XHCI_TRB_CTRL_CHAIN_BIT_OFFSET 4
#define USB_XHCI_TRB_CTRL_CHAIN_BIT_MASK (0x1 << 4)
#define USB_XHCI_TRB_CTRL_CHAIN_BIT_GET(uCtrl) \
            (((uCtrl) >> 4) & 0x1)
#define USB_XHCI_TRB_CTRL_CHAIN_BIT(uValue) \
            (((uValue) & 0x1) << 4)

/*
 * Interrupt On Completion (IOC). If this bit is set to '1', it specifies
 * that when this TRB completes, the Host Controller shall notify the system
 * of the completion by placing an Event TRB on the Event ring and asserting
 * an interrupt to the host at the next interrupt threshold.
 */

#define USB_XHCI_TRB_CTRL_IOC_OFFSET 5
#define USB_XHCI_TRB_CTRL_IOC_MASK (0x1 << 5)
#define USB_XHCI_TRB_CTRL_IOC_GET(uCtrl) \
            (((uCtrl) >> 5) & 0x1)
#define USB_XHCI_TRB_CTRL_IOC(uValue) \
            (((uValue) & 0x1) << 5)

/*
 * Immediate Data (IDT). If this bit is set to '1', it specifies that the
 * Data Buffer Pointer field of this TRB contains data, not a pointer,
 * and the Length field shall contain a value between '0' and '8' to indicate
 * the number of valid bytes from offset 0 in the TRB that should be used
 * as data.
 *
 * Note: If the IDT flag is set in one Transfer TRB of a TD, then it shall
 * be the only Transfer TRB of the TD. An Event Data TRB may be included in
 * the TD. Failure to follow this rule may result in undefined xHC operation.
 */

#define USB_XHCI_TRB_CTRL_IDT_OFFSET 6
#define USB_XHCI_TRB_CTRL_IDT_MASK (0x1 << 6)
#define USB_XHCI_TRB_CTRL_IDT_GET(uCtrl) \
            (((uCtrl) >> 6) & 0x1)
#define USB_XHCI_TRB_CTRL_IDT(uValue) \
            (((uValue) & 0x1) << 6)

/*
 * TRB Type. This shall be set to Normal TRB type. Refer to Table 131 for
 * the definition of the valid Transfer TRB type IDs.
 */

#define USB_XHCI_TRB_CTRL_TYPE_OFFSET 10
#define USB_XHCI_TRB_CTRL_TYPE_MASK (0x3F << 10)
#define USB_XHCI_TRB_CTRL_TYPE_GET(uCtrl) \
            (((uCtrl) >> 10) & 0x3F)
#define USB_XHCI_TRB_CTRL_TYPE(uValue) \
            (((uValue) & 0x3F) << 10)

/*
 * Direction (DIR). This is used only for Control Transfer Data Stage TRB
 * and Status Stage TRB.
 *
 * Note: The Direction (DIR) flag in the Status Stage TRB indicates the
 * direction of the control transfer acknowledgement. For USB2 devices,
 * DIR directly determines the PID that shall be used for the associated
 * USB2 transaction. For USB3 devices, a Status TP that is defined which
 * is used for the status stage of all SuperSpeed (SS) control transfers.
 *
 * Note: The Direction (DIR) flag in the Data Stage TRB defines the transfer
 * direction for all TRBs in the Data Stage TD. For USB2 devices, DIR
 * directly determines the PID that shall be used for the Data Stage
 * transaction. For USB3 devices, if DIR = OUT a DP is generated with write
 * data, if DIR = IN an ACK TP is generated to request read data from the
 * device.
 */

#define USB_XHCI_TRB_CTRL_DIR_OFFSET 16
#define USB_XHCI_TRB_CTRL_DIR_MASK (0x1 << 16)
#define USB_XHCI_TRB_CTRL_DIR_GET(uCtrl) \
            (((uCtrl) >> 16) & 0x1)
#define USB_XHCI_TRB_CTRL_DIR(uValue) \
            (((uValue) & 0x1) << 16)
            
#define USB_XHCI_SETUP_TRB_DIR_IN   (1)
#define USB_XHCI_SETUP_TRB_DIR_OUT  (0)

/*
 * Transfer Type (TRT). This field indicates the type and direction of 
 * the control transfer.
 *
 * Value    Definition
 *  0       No Data Stage
 *  1       Reserved
 *  2       OUT Data Stage
 *  3       IN Data Stage
 */

#define USB_XHCI_TRB_CTRL_TRT_OFFSET 16
#define USB_XHCI_TRB_CTRL_TRT_MASK (0x3 << 16)
#define USB_XHCI_TRB_CTRL_TRT_GET(uCtrl) \
            (((uCtrl) >> 16) & 0x3)
#define USB_XHCI_TRB_CTRL_TRT(uValue) \
            (((uValue) & 0x3) << 16)

#define USB_XHCI_SETUP_TRB_TRT_NO_DATA  0
#define USB_XHCI_SETUP_TRB_TRT_OUT_DATA 2
#define USB_XHCI_SETUP_TRB_TRT_IN_DATA  3

/*
 * Transfer Burst Count (TBC). This field indentifies number of 
 * bursts - 1 that shall be required to move this Isoch TD. All 
 * bursts except the last shall transfer Max Burst Size packets. 
 * The last burst shall transfer TLBPC + 1 packets.
 */
 
#define USB_XHCI_TRB_CTRL_TBC_OFFSET 7
#define USB_XHCI_TRB_CTRL_TBC_MASK (0x3 << 7)
#define USB_XHCI_TRB_CTRL_TBC_GET(uCtrl) \
                (((uCtrl) >> 7) & 0x3)
#define USB_XHCI_TRB_CTRL_TBC(uValue) \
                (((uValue) & 0x3) << 7)

/*
 * Transfer Last Burst Packet Count (TLBPC). This field indentifies
 * number of packets - 1 that shall be in the last burst of this 
 * Isoch TD, e.g. '0' = 1 packet, '1' = 2 packets, etc.
 */

#define USB_XHCI_TRB_CTRL_TLBPC_OFFSET 16
#define USB_XHCI_TRB_CTRL_TLBPC_MASK (0xF << 16)
#define USB_XHCI_TRB_CTRL_TLBPC_GET(uCtrl) \
            (((uCtrl) >> 16) & 0xF)
#define USB_XHCI_TRB_CTRL_TLBPC(uValue) \
            (((uValue) & 0xF) << 16)

/*
 * Frame ID. The value in this field identifies the target 1ms frame that
 * the Interval associated with this Isochronous Transfer Descriptor will
 * start on. Bits [13:3] of the Microframe Index field of the MFINDEX
 * register may be used to determine the current periodic frame. This field is
 * ignored by the xHC if the Start Isoch ASAP flag is set ('1').
 *
 * Note: This is only used for Isoch TRB.
 */

#define USB_XHCI_TRB_CTRL_FRAME_ID_OFFSET 20
#define USB_XHCI_TRB_CTRL_FRAME_ID_MASK (0x7FF << 20)
#define USB_XHCI_TRB_CTRL_FRAME_ID_GET(uCtrl) \
            (((uCtrl) >> 20) & 0x7FF)
#define USB_XHCI_TRB_CTRL_FRAME_ID(uValue) \
            (((uValue) & 0x7FF) << 20)

/*
 * Start Isoch ASAP (SIA). If this flag is set ('1'), the Frame ID is
 * ignored and the Isoch TD is scheduled as soon as possible. If this flag
 * is not set ('0'), the Frame ID is valid and the Isoch TD is scheduled
 * the next time there is a match between the Frame ID and the Frame Index
 * portion (bits 13:3) of the Microframe Index (MFINDEX) register.
 *
 * Note: This is only used for Isoch TRB.
 */

#define USB_XHCI_TRB_CTRL_SIA_OFFSET 31
#define USB_XHCI_TRB_CTRL_SIA_MASK (0x1 << 31)
#define USB_XHCI_TRB_CTRL_SIA_GET(uCtrl) \
            (((uCtrl) >> 31) & 0x1)
#define USB_XHCI_TRB_CTRL_SIA(uValue) \
            (((uValue) & 0x1) << 31)

/*
 * Event Data (ED). When set to '1', the event was generated by an Event Data
 * TRB and the Parameter Component (TRB Pointer field) contains a 64-bit
 * value provided by the Event Data TRB. If cleared to '0', the Parameter
 * Component (TRB Pointer field) contains a pointer to the TRB that generated
 * this event.
 *
 * Note: This is only used for Transfer Event TRB.
 */

#define USB_XHCI_TRB_CTRL_ED_OFFSET 2
#define USB_XHCI_TRB_CTRL_ED_MASK (0x1 << 2)
#define USB_XHCI_TRB_CTRL_ED_GET(uCtrl) \
            (((uCtrl) >> 2) & 0x1)

/*
 * Endpoint ID. The ID of the Endpoint that generated the event. This value
 * is used as an index in the Device Context to select the Endpoint Context
 * associated with this event.
 *
 * Note: This is only used for Transfer Event TRB.
 */

#define USB_XHCI_TRB_CTRL_EP_ID_OFFSET 16
#define USB_XHCI_TRB_CTRL_EP_ID_MASK (0x1F << 16)
#define USB_XHCI_TRB_CTRL_EP_ID_GET(uCtrl) \
            (((uCtrl) >> 16) & 0x1F)
#define USB_XHCI_TRB_CTRL_EP_ID(uCtrl) \
            (((uCtrl) & 0x1F) << 16)

/*
 * Slot ID. The ID of the Device Slot that generated the event. This is value
 * is used as an index in the Device Context Base Address Array to select the
 * Device Context of the source device.
 *
 * Note: This is only used for Transfer Event TRB, Command Completion Event TRB,
 * Bandwidth Request Event TRB, Doorbell Event TRB, Device Notification Event TRB.
 */

#define USB_XHCI_TRB_CTRL_SLOT_ID_OFFSET 24
#define USB_XHCI_TRB_CTRL_SLOT_ID_MASK ((0xFF << 24))
#define USB_XHCI_TRB_CTRL_SLOT_ID_GET(uCtrl) \
            (((uCtrl) >> 24) & 0xFF)
#define USB_XHCI_TRB_CTRL_SLOT_ID(uSlotId) \
            (((uSlotId) & 0xFF) << 24)

/*
 * VF ID. The ID of the Virtual Function that generated the event. Note that
 * this field is valid only if Virtual Functions are enabled. If they are
 * not enabled this field shall be cleared to '0'.
 *
 * Note: This is only used for Command Completion Event TRB, Doorbell Event TRB.
 */

#define USB_XHCI_TRB_CTRL_VF_ID_OFFSET 16
#define USB_XHCI_TRB_CTRL_VF_ID_MASK (0xFF << 16)
#define USB_XHCI_TRB_CTRL_VF_ID_GET(uCtrl) \
            (((uCtrl) >> 16) & 0xFF)
#define USB_XHCI_TRB_CTRL_VF_ID(uValue) \
            (((uValue) & 0xFF) << 16)

/*
 * Block Set Address Request (BSR). When this flag is set to '0' the Address
 * Device Command shall generate a USB SET_ADDRESS request to the device.
 * When this flag is set to ��1�� the Address Device Command shall not
 * generate a USB SET_ADDRESS request.
 *
 * Note: This is only used for Address Device Command TRB.
 */

#define USB_XHCI_TRB_CTRL_BSR_OFFSET 9
#define USB_XHCI_TRB_CTRL_BSR_MASK (0x1 << 9)
#define USB_XHCI_TRB_CTRL_BSR_GET(uCtrl) \
            (((uCtrl) >> 9) & 0x1)
#define USB_XHCI_TRB_CTRL_BSR(uValue) \
            (((uValue) & 0x1) << 9)


/*
 * Deconfigure (DC). Set to '1' by software to "deconfigure" the Device Slot.
 * If the DC flag = '1', the Input Context Pointer field is ignored by the xHC.
 *
 * Note: This is only used for Configure Endpoint Command TRB.
 */

#define USB_XHCI_TRB_CTRL_DC_OFFSET 9
#define USB_XHCI_TRB_CTRL_DC_MASK (0x1 << 9)
#define USB_XHCI_TRB_CTRL_DC_GET(uCtrl) \
            (((uCtrl) >> 9) & 0x1)
#define USB_XHCI_TRB_CTRL_DC(uValue) \
            (((uValue) & 0x1) << 9)

/*
 * Transfer State Preserve (TSP). Set to '1' by software if the Reset
 * operation does not affect the current transfer state of the endpoint.
 * Cleared to '0' by software if the Reset operation resets the current
 * transfer state of the endpoint, i.e. The Data Toggle of a USB2 device
 * or the Sequence Number of a USB3 device is cleared to '0'.
 *
 * Note: This is only used for Reset Endpoint Command TRB.
 */

#define USB_XHCI_TRB_CTRL_TSP_OFFSET 9
#define USB_XHCI_TRB_CTRL_TSP_MASK (0x1 << 9)
#define USB_XHCI_TRB_CTRL_TSP_GET(uCtrl) \
            (((uCtrl) >> 9) & 0x1)
#define USB_XHCI_TRB_CTRL_TSP(uValue) \
            (((uValue) & 0x1) << 9)

/*
 * Suspend (SP). When '1' this bit indicates that the Stop Endpoint Command
 * is being issued to stop activity on an endpoint that is about to be
 * suspended, and the endpoint shall be stopped for at least 10 ms. The xHC
 * may use this information to power manage the endpoint hardware resources.
 *
 * Note: This is only used for Stop Endpoint Command TRB.
 */

#define USB_XHCI_TRB_CTRL_SP_OFFSET 23
#define USB_XHCI_TRB_CTRL_SP_MASK (0x1 << 23)
#define USB_XHCI_TRB_CTRL_SP_GET(uCtrl) \
            (((uCtrl) >> 23) & 0x1)
#define USB_XHCI_TRB_CTRL_SP(uValue) \
            (((uValue) & 0x1) << 23)

/*
 * Best Effort Latency Tolerance Value. The Best Effort Latency Tolerance
 * (BELT) value provided by software. This value shall be formatted as defined
 * in the section of the USB3 Specification describing Device Notification
 * (DEV_NOTIFICATION) Transaction Packet (TP).
 *
 * Note: This is only used for Set Latency Tolerance Value (LTV) Command TRB
 * (Optional Normative).
 */

#define USB_XHCI_TRB_CTRL_BELT_OFFSET 23
#define USB_XHCI_TRB_CTRL_BELT_MASK (0x1 << 23)
#define USB_XHCI_TRB_CTRL_BELT_GET(uCtrl) \
            (((uCtrl) >> 23) & 0x1)
#define USB_XHCI_TRB_CTRL_BELT(uValue) \
            (((uValue) & 0x1) << 23)

/*
 * Dev Speed. The bus speed of interest.
 *
 * Note: The Undefined and Reserved Speeds are invalid values for this field.
 *
 * Note: This is only used for Get Port Bandwidth Command TRB.
 */

#define USB_XHCI_TRB_CTRL_DEV_SPEED_OFFSET 16
#define USB_XHCI_TRB_CTRL_DEV_SPEED_MASK (0xF << 16)
#define USB_XHCI_TRB_CTRL_DEV_SPEED_GET(uCtrl) \
            (((uCtrl) & USB_XHCI_TRB_CTRL_DEV_SPEED_MASK) >> \
             USB_XHCI_TRB_CTRL_DEV_SPEED_OFFSET)
#define USB_XHCI_TRB_CTRL_DEV_SPEED(uValue) \
            (((uValue) << USB_XHCI_TRB_CTRL_DEV_SPEED_OFFSET) & \
             USB_XHCI_TRB_CTRL_DEV_SPEED_MASK)


/*
 * Root Hub Port Number. This field identifies the number of the Root Hub
 * Port that the header packet shall be issued to.
 *
 * Note: This is only used for Force Header Command TRB.
 */

#define USB_XHCI_TRB_CTRL_RH_PORT_OFFSET 24
#define USB_XHCI_TRB_CTRL_RH_PORT_MASK (0xFF << 24)
#define USB_XHCI_TRB_CTRL_RH_PORT_GET(uCtrl) \
            (((uCtrl) >> 24) & 0xFF)
#define USB_XHCI_TRB_CTRL_RH_PORT(uValue) \
            (((uValue) & 0xFF) << 24)

/*
 * Toggle Cycle (TC). When set to '1', the xHC shall toggle its interpretation
 * of the Cycle bit. When cleared to '0', the xHC shall continue to the next
 * segment using its current interpretation of the Cycle bit.
 *
 * Note: This is only used for Link TRB.
 */

#define USB_XHCI_TRB_CTRL_TC_OFFSET 1
#define USB_XHCI_TRB_CTRL_TC_MASK (0x1 << 1)
#define USB_XHCI_TRB_CTRL_TC_GET(uCtrl) \
    (((uCtrl) >> 1) & 0x1)
#define USB_XHCI_TRB_CTRL_TC(uValue)    \
    (((uValue) & 0x1) << 1)

/* usbXhciTRB::uCtrl bit fields - End */

/*
 * 6.4.6 TRB Types
 *
 * TRB Types fall into three categories; Command, Event, or Transfer.
 * These categories relate to the TRB Ring that specific TRB(s) may appear on.
 */

/* TRB type 0 is reserved */

/* Normal bulk, interrupt, isoch scatter/gather, and control data stage */

#define USB_XHCI_TRB_NORMAL           1 /* TR */

/* Setup stage for control transfers */

#define USB_XHCI_TRB_SETUP            2 /* TR */

/* Data stage for control transfers */

#define USB_XHCI_TRB_DATA             3 /* TR */

/* Status stage for control transfers */

#define USB_XHCI_TRB_STATUS           4 /* TR */

/* Isoch transfers */

#define USB_XHCI_TRB_ISOC             5 /* TR */

/* TRB for linking ring segments */

#define USB_XHCI_TRB_LINK             6 /* TR or CR */

/* Event Data TRB */

#define USB_XHCI_TRB_EVENT_DATA       7 /* TR */

/* Transfer Ring No-op (not for the command ring) */

#define USB_XHCI_TRB_TR_NOOP          8 /* TR */

/* Enable Slot Command */

#define USB_XHCI_TRB_ENABLE_SLOT      9 /* CR */

/* Disable Slot Command */

#define USB_XHCI_TRB_DISABLE_SLOT     10 /* CR */

/* Address Device Command */

#define USB_XHCI_TRB_ADDR_DEV         11 /* CR */

/* Configure Endpoint Command */

#define USB_XHCI_TRB_CONFIG_EP        12 /* CR */

/* Evaluate Context Command */

#define USB_XHCI_TRB_EVAL_CONTEXT     13 /* CR */

/* Reset Endpoint Command */

#define USB_XHCI_TRB_RESET_EP         14 /* CR */

/* Stop Transfer Ring Command */

#define USB_XHCI_TRB_STOP_RING        15 /* CR */

/* Set Transfer Ring Dequeue Pointer Command */

#define USB_XHCI_TRB_SET_DEQ          16 /* CR */

/* Reset Device Command */

#define USB_XHCI_TRB_RESET_DEV        17 /* CR */

/* Force Event Command (Optional, used with virtualization only) */

#define USB_XHCI_TRB_FORCE_EVENT      18 /* CR */

/* Negotiate Bandwidth Command (Optional) */

#define USB_XHCI_TRB_NEG_BANDWIDTH    19 /* CR */

/* Set Latency Tolerance Value Command (Optional) */

#define USB_XHCI_TRB_SET_LTV          20 /* CR */

/* Get port bandwidth Command */

#define USB_XHCI_TRB_GET_PORT_BW      21 /* CR */

/* Force Header Command - generate a TP or LMP */

#define USB_XHCI_TRB_FORCE_HEADER     22 /* CR */

/* No-op Command - not for transfer rings */

#define USB_XHCI_TRB_CMD_NOOP         23 /* CR */

/* TRB types 24-31 are reserved */

/* Transfer Event */

#define USB_XHCI_TRB_XFER_EVENT       32 /* ER */

/* Command Completion Event */

#define USB_XHCI_TRB_CMD_COMPLETE_EVENT        33 /* ER */

/* Port Status Change Event */

#define USB_XHCI_TRB_PORTSTS_CHANGE_EVENT      34 /* ER */

/* Bandwidth Request Event (Optional) */

#define USB_XHCI_TRB_BW_REQ_EVENT              35 /* ER */

/* Doorbell Event (Optional, used with virtualization only) */

#define USB_XHCI_TRB_DB_EVENT                  36 /* ER */

/* Host Controller Event - report xHC state changes and Error conditions */

#define USB_XHCI_TRB_HC_EVENT                  37 /* ER */

/* Device Notification Event - received USB Device (DEV_NOTIFICATION) TP */

#define USB_XHCI_TRB_DEV_NOTIFY_EVENT          38 /* ER */

/* MFINDEX Wrap Event - MFINDEX register wrap from 0x3FFFh to 0 */

#define USB_XHCI_TRB_MFIDX_WRAP_EVENT          39 /* ER */

/* TRB types 40-47 are reserved, 48-63 are vendor-defined */

/*
 * 6.4.5 TRB Completion Codes
 *
 * The following TRB Completion Status codes will be asserted by the Host
 * Controller during status update if the associated error condition is
 * detected.
 */

/*
 * Invalid - Indicates that the Completion Code field has not been updated
 * by the TRB consumer.
 */

#define USB_XHCI_COMP_INVALID                    0

/* Success - Indicates successful completion of the TRB operation. */

#define USB_XHCI_COMP_SUCCESS                    1

/*
 * Data Buffer Error - Indicates that the Host Controller is unable to keep
 * up with the reception of incoming data (overrun) or is unable to supply
 * data fast enough during transmission (underrun).
 */

#define USB_XHCI_COMP_DATA_BUFF_ERR              2

/*
 * Babble Detected Error - Asserted when "babbling" is detected during the
 * transaction generated by this TRB.
 */

#define USB_XHCI_COMP_BABBLE_DETECTED_ERR        3

/*
 * USB Transaction Error - Asserted in the case where the host did not receive
 * a valid response from the device (Timeout, CRC, Bad PID, unexpected NYET,
 * ERDY, etc.).
 */

#define USB_XHCI_COMP_TRANS_ERR                  4

/*
 * TRB Error - Asserted when a TRB parameter error condition (e.g., out of
 * range or invalid parameter) is detected in a TRB.
 */

#define USB_XHCI_COMP_TRB_PARAM_ERR              5

/*
 * Stall Error - Asserted when a Stall condition (e.g., a Stall PID received
 * from a device) is detected for a TRB. This code also indicates that the
 * USB device has an error that prevents it from completing a command issued
 * through a Control endpoint. Refer to section 8.5.3.1 of the USB2 spec for
 * more information.
 */

#define USB_XHCI_COMP_STALL_ERR                  6

/*
 * Resource Error - Asserted by a Configure Endpoint Command if there are not
 * adequate xHC resources available to enable the requested set of endpoints.
 */

#define USB_XHCI_COMP_RESOURCE_ERR               7

/*
 * Bandwidth Error - Asserted by a Configure Endpoint Command if periodic
 * endpoints are declared and the xHC is not able to allocate the required
 * Bandwidth.
 */

#define USB_XHCI_COMP_BW_ERR                     8

/*
 * No Slots Available Error - Asserted if adding one more device would
 * result in the host controller to exceed the maximum Number of Device Slots
 * (MaxSlots) for this implementation.
 */

#define USB_XHCI_COMP_NO_SLOTS_ERR               9

/*
 * Invalid Stream Type Error - Asserted if a invalid Stream Context Type (SCT)
 * value is detected.
 */

#define USB_XHCI_COMP_INVALID_STREAM_TYPE_ERR   10

/*
 * Slot Not Enabled Error - Asserted if a command is issued to a Device Slot
 * that is in the Disabled state. The Slot ID is reported.
 */

#define USB_XHCI_COMP_SLOT_NOT_ENABLED_ERR      11

/*
 * Endpoint Not Enabled Error - Asserted if a doorbell is rung for an endpoint
 * that is in the Disabled state. The Slot ID and error Endpoint ID are reported.
 */

#define USB_XHCI_COMP_EP_NOT_ENABLED_ERR        12

/*
 * Short Packet - Asserted if the number of bytes received was less than the TD
 * Transfer Size.
 */

#define USB_XHCI_COMP_SHORT_PACKET              13

/*
 * Ring Underrun - Asserted in a Transfer Event TRB if the Transfer Ring is
 * empty when an enabled Isoch endpoint is scheduled to transmit data.
 *
 * Note that the Transfer Event TRB Pointer field is not valid when this
 * condition is indicated and should be ignored by software.
 */

#define USB_XHCI_COMP_TR_UNDERRUN               14

/*
 * Ring Overrun - Asserted in a Transfer Event TRB if the Transfer Ring is
 * empty when an enabled Isoch endpoint is scheduled to receive data.
 *
 * Note that the Transfer Event TRB Pointer field is not valid when this
 * condition is indicated and should be ignored by software.
 */

#define USB_XHCI_COMP_TR_OVERRUN                15

/*
 * Virtual Function Event Ring Full Error - Asserted by a Force Event command
 * if the target VF's Event Ring is full.
 *
 * Note that the Transfer Event TRB Pointer field is not valid when this
 * error is indicated and should be ignored by software.
 */

#define USB_XHCI_COMP_VF_FULL_ERR               16

/* Parameter Error - Asserted by a command if a Context parameter is invalid. */

#define USB_XHCI_COMP_PARAM_ERR                 17

/*
 * Bandwidth Overrun Error - Asserted during an Isoch transfer if the TD
 * exceeds the bandwidth allocated to the endpoint.
 */

#define USB_XHCI_COMP_BW_OVERRUN_ERR            18

/*
 * Context State Error - Asserted if a command is issued to transition from
 * an illegal context state.
 */

#define USB_XHCI_COMP_CTX_STATE_ERR             19

/*
 * No Ping Response Error - Asserted if the xHC was unable to complete a
 * periodic data transfer associated within the ESIT, because it did not
 * receive a PING_RESPONSE in time.
 */

#define USB_XHCI_COMP_NO_PING_RESPONSE_ERR      20

/*
 * Event Ring is full - Asserted if the Event Ring is full, the xHC is unable
 * to post an Event to the ring. This error is reported in a Host Controller
 * Event TRB.
 */

#define USB_XHCI_COMP_ER_FULL                   21

/* Completion Code 22 is reserved */

/*
 * Missed Service Error - Asserted if the xHC was unable to service a
 * Isochronous endpoint within the Interval time.
 */

#define USB_XHCI_COMP_MISSED_SERVICE_ERR        23

/*
 * Command Ring Stopped - Asserted in a Command Completion Event due to a
 * Command Stop (CS) operation. The Command TRB Pointer is invalid for this
 * completion and shall be cleared to '0'.
 */

#define USB_XHCI_COMP_CR_STOPPED                24

/*
 * Command Aborted (and Stopped) - Asserted in a Command Completion Event of an
 * aborted command if the command was terminated by a Command Abort (CA)
 * operation.
 */

#define USB_XHCI_COMP_CR_ABORTED                25

/*
 * Stopped - Asserted in a Transfer Event if the transfer was terminated by a
 * Stop Endpoint Command.
 */

#define USB_XHCI_COMP_TRANSFER_STOPPED          26

/*
 * Stopped - Length Invalid - Asserted in a Transfer Event if the transfer
 * was terminated by a Stop Endpoint Command and the Transfer Event Length
 * field is invalid.
 */

#define USB_XHCI_COMP_TRANSFER_STOPPED_INVALID_LEN    27

/*
 * Control Abort Error - Asserted by the Debug Capability to indicate a
 * Control pipe abort operation by a Debug Host. This error utilizes a
 * Transfer Event TRB. The Transfer Event TRB Pointer and Transfer Length
 * fields are invalid.
 */

#define USB_XHCI_COMP_DBG_CTRL_ABORT_ERR        28

/* Completion Code 29 and 30 are reserved */

/*
 * Isoch Buffer Overrun - Asserted if the an Isoch TD on an IN endpoint is
 * less than the Max ESIT Payload in size and the device attempts to send
 * more data than it can hold.
 */

#define USB_XHCI_COMP_ISOCH_BUFF_OVERRUN        31

/*
 * Event Lost Error - Asserted if the xHC internal event overrun condition.
 * If the condition is due to TD related events, then the endpoint shall be
 * halted. The conditions that generate this error are xHC implementation
 * specific.
 */

#define USB_XHCI_COMP_EVENT_LOST_ERR            32

/*
 * Undefined Error - May be reported by an event when other error codes do
 * not apply. The conditions that assert this condition code are xHC
 * implementation specific.
 */

#define USB_XHCI_COMP_UNDEFINED_ERR             33

/* Invalid Stream ID Error - Asserted if an invalid Stream ID is received. */

#define USB_XHCI_COMP_INVALID_STREAM_ID_ERR     34

/*
 * Secondary Bandwidth Error - Asserted by a Configure Endpoint Command if
 * periodic endpoints are declared and the xHC is not able to allocate the
 * required Bandwidth due to a Secondary Bandwidth Domain.
 */

#define USB_XHCI_COMP_2ND_BW_ERR                35

/*
 * Split Transaction Error - Asserted if an error is detected on a USB2
 * protocol endpoint for a split transaction.
 */

#define    USB_XHCI_COMP_SPLIT_TRANS_ERR        36

/* Completion Code 37-191 are reserved */

/*
 * Completion Code 192-223 are Vendor Defined Errors, Asserted by a vendor
 * to indicate an error condition has occurred. Refer to vendor documentation
 * to identify specific error condition(s).
 *
 * If software does not recognize the code, it shall interpret this range
 * of vendor defined values as a Undefined Error condition.
 */

#define    USB_XHCI_COMP_VENDOR_ERR_LOW            192
#define    USB_XHCI_COMP_VENDOR_ERR_HIGH           223

/*
 * Completion Code 224-255 are Vendor Defined Info, Asserted by a vendor
 * for informational purposes. Refer to vendor documentation to identify
 * specific information reported. If software does not recognize the code,
 * it shall interpret this range of vendor defined values as a Success
 * condition code.
 */

#define    USB_XHCI_COMP_VENDOR_INFO_LOW           224
#define    USB_XHCI_COMP_VENDOR_INFO_HIGH          255

/*
 * 6.5 Event Ring Segment Table
 *
 * The Event Ring Segment Table is used to define multi-segment Event Rings
 * and to enable runtime expansion and shrinking of the Event Ring. The
 * location of the Event Ring Segment Table is defined by the Event Ring
 * Segment Table Base Address Register (5.5.2.3.2). The size of the Event
 * Ring Segment Table is defined by the Event Ring Segment Table Base Size
 * Register (5.5.2.3.1).
 */

/*
 * Event Ring Segment Table Entry
 *
 * This section defines the properties of a single Event Ring Segment Table
 * element. Refer to section 4.9.4 for more information.
 *
 * uRingSegBase - Ring Segment Base Address Hi and Lo; These fields represent
 *                the high order bits of the 64-bit base address of the Event
 *                Ring Segment. The memory structure referenced by this physical
 *                memory pointer shall begin on a 16-byte address boundary.
 *
 * uRingSegSize - Ring Segment Size. This field defines the number of TRBs
 *                supported by the ring segment, Valid values for this field
 *                are 16 to FFFFh, i.e. an Event Ring segment shall contain
 *                at least 16 entries.
 */

typedef struct usbXhciERSTEntry
    {
    UINT64 uRingSegBase; /* 64-bit base address of the Event Ring Segment */
    UINT32 uRingSegSize; /* Ring Segment Size - # of TRBs supported by segment */
    UINT32 uRsvdZ;       /* Reserved - should be zero */
    }USB_XHCI_ERST_ENTRY, *pUSB_XHCI_ERST_ENTRY;

/*
 * 6.6 Scratchpad Buffer Array
 *
 * The Scratchpad Buffer Array is used to define the locations of statically
 * allocated memory pages that are available for the private use of the xHC.
 *
 * The location of the Scratchpad Buffer Array is defined by entry 0 of the
 * Device Context Base Address Array (6.1).
 *
 * The size of the Scratchpad Buffer Array is defined by the Max Scratchpad
 * Buffers field in the HCSPARAMS2 Register (5.3.4).
 */

/*
 * Each entry contains the "Scratchpad Buffer Base Address", aligned on
 * PAGESIZE boundary.
 */

typedef UINT64 USB_XHCI_SCRATCHPAD_BUFF_ENTRY;

/*
 * Scratchpad Buffer Base Address - Default = '0'. This field contains bits
 * 63 to PSZ of a pointer to a Scratchpad Buffer.
 *
 * The actual number of bits used for the Scratchpad Buffer Base Address
 * field depends on the value of the PAGESIZE register. If PAGESIZE = 4K
 * then bits 31-12 of the Scratchpad Buffer Base Address field are valid,
 * if PAGESIZE = 8K then bits 31-13 of the Scratchpad Buffer Base Address
 * field are valid, and so on. Valid values for PSZ are 12 to 20.
 */


#ifdef __cplusplus
}
#endif
#endif  /* __INCusbXhcdStructuresh */

