/* usbMhdrcPlatformOmap.c - OMAP platform configuration which uses Mentor Graphics HCD */

/*
 * Copyright (c) 2010, 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01j,03may13,wyy  Remove compiler warning (WIND00356717)
01i,16sep11,m_y  Move ulpi read/write routine here.
01h,28mar11,s_z  Fix dynomic attach/detach issue
01g,28mar11,w_x  Correct uNumEps and spinlock on interrupt report (WIND00262862)
01f,23mar11,w_x  Address more code review comments
01e,09mar11,w_x  Code clean up for make man
01d,13sep10,ghs  Fix defect found by code review of merge (WIND00232740)
01c,16aug10,w_x  VxWorks 64 bit audit and warning removal
01b,03jun10,s_z  Debug macro changed, Add more debug message
01a,11mar10,s_z  written
*/

/*
DESCRIPTION

This file provides the OMAP platform related hardware configure routines
which will be used to configure the MHCD. And also proviedes the fixup routine
to configure Omap3evm platform.

INCLUDE FILES: usb/usb.h, usb/usbOsal.h, usb/usbHst.h, usb/usbd.h,
               usbMhdrc.h, usbMhdrcPlatform.h usbMhdrcHcd.h usbMhdrcHcdDebug.h

SEE ALSO:

None
*/

/* includes */

#include <usb/usb.h>
#include <usb/usbOsal.h>
#include <usb/usbHst.h>
#include <usb/usbd.h>
#include "usbMhdrc.h"
#include "usbMhdrcPlatform.h"
#include "usbMhdrcHcd.h"
#include "usbMhdrcHcdDebug.h"

/*******************************************************************************
*
* usbMhdrcOmapIsr - interrupt handler for handling MHCI controller interrupts
*
* This routine is registered as the interrupt handler for handling MHCI
* controller interrupts.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcOmapIsr
    (
    void * pMhdrc
    )
    {
    pUSB_MUSBMHDRC pMHDRC = (pUSB_MUSBMHDRC)pMhdrc;
    UINT16         uIntTx   = 0;
    UINT16         uIntRx   = 0;
    UINT8          uIntUsb  = 0;

    /* Check the validity of the parameter */

    if ((NULL == pMHDRC) ||
        (NULL == pMHDRC->pDev))
        {
        USB_MHDRC_ERR("usbMhdrcOmapIsr(): "
                     "Invalid parameter, %s is NULL\n",
                     ((NULL == pMHDRC) ? "pMHDRC" :
                     "pMHDRC->pDev"),
                     2, 3, 4, 5 ,6);

        return;
        }

    if (pMHDRC->isrMagic != USB_MHDRC_MAGIC_ALIVE)
        {
        USB_MHDRC_ERR("usbMhdrcOmapIsr(): "
                     "ISR not active, maybe shared?\n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }

    uIntTx = USB_MHDRC_REG_READ16 (pMHDRC, USB_MHDRC_INTRTX) &
             USB_MHDRC_REG_READ16 (pMHDRC, USB_MHDRC_INTRTXE);
    uIntRx = USB_MHDRC_REG_READ16 (pMHDRC, USB_MHDRC_INTRRX) &
             USB_MHDRC_REG_READ16 (pMHDRC, USB_MHDRC_INTRRXE);
    uIntUsb = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_INTRUSB) &
              USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_INTRUSBE);

    pMHDRC->uTxIrqStatus = uIntTx;

    pMHDRC->uRxIrqStatus = uIntRx;

    pMHDRC->uUsbIrqStatus = uIntUsb;

    /* Signal the ISR event */

    if (uIntTx | uIntRx | uIntUsb)
        {
        USB_MHDRC_VDBG("usbMhdrcOmapIsr(): "
                      "Interrupt Registers uIntTx %p uIntRx %p, uIntUsb %p\n",
                       uIntTx, uIntRx, uIntUsb, 4, 5 ,6);
        if (pMHDRC->pHCDData != NULL)
            USB_MHDRC_HCD_REPORT_EVENT(pMHDRC, (pUSB_MHCD_DATA)pMHDRC->pHCDData);
        if (pMHDRC->pTCDData != NULL)
            USB_MHDRC_TCD_REPORT_EVENT(pMHDRC, (pUSB_MHDRC_TCD_DATA)pMHDRC->pTCDData);
        }

    return;
    }


/*******************************************************************************
*
* usbMhdrcOmapExternalPowerConfigure - configure external power supply for omap
*
* This routine configure the external power supply for the omap platform.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcOmapExternalPowerConfigure
    (
    void * pMhdrc,
    BOOL   uEnable
    )
    {
    pUSB_MUSBMHDRC  pMHDRC = (pUSB_MUSBMHDRC)pMhdrc;

    if (NULL == pMHDRC)
        {
        USB_MHDRC_ERR("usbMhdrcOmapExternalPowerConfigure(): "
                     "Invalid parameter, pMHDRC is NULL\n",
                      1, 2, 3, 4, 5 ,6);

        return;
        }

    USB_MHDRC_DBG("usbMhdrcOmapExternalPowerConfigure(): "
                 "uEnable %s Power\n",
                 ((uEnable) ? "External" : "Internal"), 2, 3, 4, 5 ,6);

    if (uEnable)
        {
        USB_MHDRC_REG_WRITE8(pMHDRC,
                            USB_MHDRC_ULPIVBUSCONTROL,
                            USB_MHDRC_ULPIVBUSCONTROL_USEEXTVBUS |
                            USB_MHDRC_REG_READ8(pMHDRC,
                            USB_MHDRC_ULPIVBUSCONTROL));
        }
    else
        {
        USB_MHDRC_REG_WRITE8(pMHDRC,
                            USB_MHDRC_ULPIVBUSCONTROL,
                            (UINT8)(~USB_MHDRC_ULPIVBUSCONTROL_USEEXTVBUS &
                            USB_MHDRC_REG_READ8(pMHDRC,
                            USB_MHDRC_ULPIVBUSCONTROL)));
        }

    return;
    }

/*******************************************************************************
*
* usbMhdrcOmapInterruptEnable - enable MHCI USB host controller interrupt
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcOmapInterruptEnable
    (
    void * pMhdrc
    )
    {
    pUSB_MUSBMHDRC  pMHDRC = (pUSB_MUSBMHDRC)pMhdrc;
    UINT16          uTxIntMask = 0;
    UINT16          uRxIntMask = 0;

    if (NULL == pMHDRC)
        {
        USB_MHDRC_ERR("usbMhdrcOmapInterruptEnable(): "
                     "Invalid parameter, pMHDRC is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }

    /*
     * Get the endpoint interrupt enable mask according to
     * the number of endpoints
     */

    uTxIntMask = (UINT16)(((1 << pMHDRC->uNumEps) - 1) & USB_MHDRC_INTMSKCLR_TX_MASK);
    uRxIntMask = (UINT16)(((1 << pMHDRC->uNumEps) - 1) & USB_MHDRC_INTMSKCLR_RX_MASK);

    /* Set USB interrupt in INTRTXE register */

    USB_MHDRC_REG_WRITE16 (pMHDRC,
                          USB_MHDRC_INTRTXE,
                          uTxIntMask);

    /* Set USB interrupt in INTRRXE register */

    USB_MHDRC_REG_WRITE16 (pMHDRC,
                          USB_MHDRC_INTRRXE,
                          uRxIntMask);

    /* Set USB interrupt in INTRUSBE register, except SOF interrupt */

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                         USB_MHDRC_INTRUSBE,
                         USB_MHDRC_INTMSKCLR_SOF_MASK);

    /* Disable USB2.0 Test Modes */

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                         USB_MHDRC_TESTMODE,
                         0x00);

    /* Put into basic highspeed mode, set SESSION and HOSTREQ bit */

    USB_MHDRC_REG_WRITE8 (pMHDRC, USB_MHDRC_POWER,
                          USB_MHDRC_REG_READ8(pMHDRC, USB_MHDRC_POWER) |
                          USB_MHDRC_POWER_HSEN);

    USB_MHDRC_DBG("usbMhdrcOmapInterruptEnable(): "
                 "Enable Done\n",
                 1, 2, 3, 4, 5 ,6);

    return;
    }


/*******************************************************************************
*
* usbMhdrcPlatformOmap3InterruptDisable - disable MHCI host controller interrupt
*
* This routine disables MHCI host controller interrupt.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcOmapInterruptDisable
    (
    void * pMhdrc
    )
    {
    pUSB_MUSBMHDRC pMHDRC = (pUSB_MUSBMHDRC)pMhdrc;
    UINT16         uReg16;
    UINT8          uReg8;

    if (NULL == pMHDRC)
        {
        USB_MHDRC_ERR("usbMhdrcOmapInterruptDisable(): "
                     "Invalid parameter, pMHDRC is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }

    /* Mask USB Tx interrupt in INTRTXE */

    USB_MHDRC_REG_WRITE16 (pMHDRC,
                          USB_MHDRC_INTRTXE,
                          0);

    /* Mask USB Rx interrupt in INTRRXE */

    USB_MHDRC_REG_WRITE16 (pMHDRC,
                          USB_MHDRC_INTRRXE,
                          0);

    /* Mask USB core interrupt in USB_MHDRC_INTRUSBE */

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                         USB_MHDRC_INTRUSBE,
                         0);

    /* Read the register to clean the 3 registers */

    uReg16 = USB_MHDRC_REG_READ16 (pMHDRC,
                         USB_MHDRC_INTRTX);

    uReg16 = USB_MHDRC_REG_READ16 (pMHDRC,
                         USB_MHDRC_INTRRX);

    uReg8 = USB_MHDRC_REG_READ8 (pMHDRC,
                         USB_MHDRC_INTRUSB);

    usbMhdrcOmapExternalPowerConfigure(pMHDRC, pMHDRC->uUseExtPower);

    USB_MHDRC_DBG("usbMhdrcOmapInterruptDisable(): "
                 "Disable Done\n",
                 1, 2, 3, 4, 5 ,6);

    return ;
    }

/*******************************************************************************
*
* usbMhdrcOmapReset - reset the hardware in OMAP specific way
*
* This routine resets the hardware in OMAP specific way.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcOmapReset
    (
    pUSB_MUSBMHDRC  pMHDRC
    )
    {
#ifdef USB_MHDRC_USE_OMAP_EXTENDED_OTG_REGS
    UINT32 uReg32;
    UINT32 uCount;

    USB_MHDRC_REG_WRITE32 (pMHDRC,
                          USB_MHDRC_OTG_SYSCONFIG,
                          OTG_SYSCONFIG_SOFTRST);

    uCount = 0;

    uReg32 = USB_MHDRC_REG_READ32 (pMHDRC, USB_MHDRC_OTG_SYSSTATUS);

    while (!(uReg32 & OTG_SYSSTATUS_RESETDONE) &&
        (uCount++ < USB_MHDRC_REG_ACCESS_WAIT_COUNT))
        {
        uReg32 = USB_MHDRC_REG_READ32 (pMHDRC, USB_MHDRC_OTG_SYSSTATUS);
        }

    if (uCount >= USB_MHDRC_REG_ACCESS_WAIT_COUNT)
        {
        USB_MHDRC_WARN("usbMhdrcOmapReset(): SOFTRST timeout\n",
                     1, 2, 3, 4, 5 ,6);
        }

    /* Resume from any possible suspend */

    uReg32 = USB_MHDRC_REG_READ32 (pMHDRC, USB_MHDRC_OTG_FORCESTDBY);
    uReg32 &= ~OTG_FORCESTDBY_ENABLEFORCE; /* Disable MSTANDBY to go high */
    USB_MHDRC_REG_WRITE32 (pMHDRC, USB_MHDRC_OTG_FORCESTDBY, uReg32);


    uReg32 = USB_MHDRC_REG_READ32 (pMHDRC, USB_MHDRC_OTG_SYSCONFIG);
    uReg32 &= ~OTG_SYSCONFIG_SOFTRST; /* Clear Software reset bit */
    uReg32 &= ~OTG_SYSCONFIG_ENABLEWAKEUP; /* Wakeup is disabled */
    uReg32 &= ~OTG_SYSCONFIG_MIDLEMODE_MSK;
    uReg32 &= ~OTG_SYSCONFIG_SIDLEMODE_MSK;
    uReg32 |= OTG_SYSCONFIG_AUTOIDLE; /* When no activity on OCP, clock is cut off */
    uReg32 |= OTG_SYSCONFIG_SMARTIDLE; /* Smart-Idle mode is enabled */
    uReg32 |= OTG_SYSCONFIG_SMARTSTDBY; /* Smart standby mode is enabled */
    USB_MHDRC_REG_WRITE32 (pMHDRC, USB_MHDRC_OTG_SYSCONFIG, uReg32);


    uReg32 = USB_MHDRC_REG_READ32 (pMHDRC, USB_MHDRC_OTG_INTERFSEL);
    uReg32 &= ~(OTG_INTERFSEL_PHYSEL_MSK);
    uReg32 |= OTG_INTERFSEL_ULPI_12PIN;
    USB_MHDRC_REG_WRITE32 (pMHDRC, USB_MHDRC_OTG_INTERFSEL, uReg32);

#endif /* USB_MHDRC_USE_OMAP_EXTENDED_OTG_REGS */
    }

/*******************************************************************************
*
* usbMhdrcOmap3evmSetup - configure omap3 platform information
*
* This routine configures omap3evm platform information.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcOmap3evmSetup
    (
    void * pMhdrc,
    void * pPlatformInfo
    )
    {
    pUSB_MUSBMHDRC  pMHDRC = (pUSB_MUSBMHDRC)pMhdrc;
    pUSB_MHDRC_PLATFORM_DATA pPlatformData =
                  (pUSB_MHDRC_PLATFORM_DATA)pPlatformInfo;

    if (NULL == pMHDRC)
        {
        USB_MHDRC_ERR("usbMhdrcOmap3evmSetup(): "
                      "Invalid parameter, pMHDRC is NULL\n",
                      1, 2, 3, 4, 5 ,6);

        return;
        }

    /* Fill up the platform configure information */

    pPlatformData->uComRegOffset = 0;

    if (pMHDRC->pISR)
        pPlatformData->pUsbCoreIsr = pMHDRC->pISR;
    else
        pPlatformData->pUsbCoreIsr = usbMhdrcOmapIsr;

    pPlatformData->pHwInterruptEnable = usbMhdrcOmapInterruptEnable;
    pPlatformData->pHwInterruptDisable = usbMhdrcOmapInterruptDisable;
    pPlatformData->pPowerConfigure = usbMhdrcOmapExternalPowerConfigure;

    usbMhdrcOmapReset(pMHDRC);

    return;
    }

/*******************************************************************************
*
* usbMhdrcOmap3UlpiRead - read the ULPI register
*
* This routine is to read the ULPI register. Strict users should check the
* return value to make sure the ULPI access is successful.
*
* RETURNS: OK when the ULPI access is successful, ERROR when failed.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbMhdrcOmap3UlpiRead
    (
    pUSB_MUSBMHDRC  pMHDRC,
    UINT8           uOffset,
    UINT8 *         pData
    )
    {
    int     i = 0;
    UINT8   uReg8;

    /* We can not access the ULPI if we are in suspend mode */

    uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_POWER);

    if ((uReg8 & USB_MHDRC_POWER_SUSPENDM) || (pMHDRC->bIsResetActive))
        {
        USB_MHDRC_WARN("usbMhdrcUlpiRead - in SUSPEND mode\n",
            1, 2, 3, 4, 5 ,6);

        if (pMHDRC->bIsResetActive)
            {
            USB_MHDRC_WARN("usbMhdrcUlpiRead - being RESET, clear the flag\n",
                1, 2, 3, 4, 5 ,6);

            OS_DELAY_MS(50);

            pMHDRC->bIsResetActive = FALSE;
            }

        return ERROR;
        }

    /* If in reset, delay about 50ms to let the link stable */

    if (uReg8 & USB_MHDRC_POWER_RESET)
        {
        USB_MHDRC_WARN("usbMhdrcUlpiRead - in RESET mode\n",
            1, 2, 3, 4, 5 ,6);

        uReg8 = (UINT8)(uReg8 & ~USB_MHDRC_POWER_RESET);

        OS_DELAY_MS(50);

        USB_MHDRC_REG_WRITE8(pMHDRC, USB_MHDRC_POWER, uReg8);
        }

    /* Initiate a ULPI READ access */

    USB_MHDRC_REG_WRITE8(pMHDRC, USB_MHDRC_ULPIREGADDR, uOffset);

    USB_MHDRC_REG_WRITE8(pMHDRC, USB_MHDRC_ULPIREGCONTROL,
                        USB_MHDRC_ULPIREGCONTROL_ULPIRDNWR |
                        USB_MHDRC_ULPIREGCONTROL_ULPIREGREQ);

    i = 0;

    /* Wait until the access is reported as completed */

    while (!(USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_ULPIREGCONTROL)
                & USB_MHDRC_ULPIREGCONTROL_ULPIREGCMPLT))
        {
        i++;

        if (i == USB_MHDRC_REG_ACCESS_WAIT_COUNT)
            {
            USB_MHDRC_ERR("ULPI read timed out bIsResetActive %d\n",
                pMHDRC->bIsResetActive , 2, 3, 4, 5, 6);

            *pData = 0;

            /* Clear the completed indication */

            USB_MHDRC_REG_WRITE8(pMHDRC, USB_MHDRC_ULPIREGCONTROL, 0);

            return ERROR;
            }

        /* Give other task a chance to run! */

        OS_DELAY_MS(1);
        }

    *pData = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_ULPIREGDATA);

    /* Clear the completed indication */

    USB_MHDRC_REG_WRITE8(pMHDRC, USB_MHDRC_ULPIREGCONTROL, 0);
    return OK;
    }

/*******************************************************************************
*
* usbMhdrcUlpiWrite - write the ULPI register
*
* This routine is to read the ULPI register. Strict users should check the
* return value to make sure the ULPI access is successful.
*
* RETURNS: OK when the ULPI access is successful, ERROR when failed.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbMhdrcOmap3UlpiWrite
    (
    pUSB_MUSBMHDRC  pMHDRC,
    UINT32          uOffset,
    UINT8           uData
    )
    {
    int     i = 0;
    UINT8   uReg8;

    /* We can not access the ULPI if we are in suspend mode */

    uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_POWER);

    if ((uReg8 & USB_MHDRC_POWER_SUSPENDM) || (pMHDRC->bIsResetActive))
        {
        USB_MHDRC_WARN("usbMhdrcUlpiWrite - in SUSPEND mode\n",
            1, 2, 3, 4, 5 ,6);

        if (pMHDRC->bIsResetActive)
            {
            USB_MHDRC_WARN("usbMhdrcUlpiWrite - being RESET, clear the flag\n",
                1, 2, 3, 4, 5 ,6);

            OS_DELAY_MS(50);

            pMHDRC->bIsResetActive = FALSE;
            }

        return ERROR;
        }

    /* If in reset, delay about 50ms to let the link stable */

    if (uReg8 & USB_MHDRC_POWER_RESET)
        {
        USB_MHDRC_WARN("usbMhdrcUlpiWrite - in RESET mode\n",
            1, 2, 3, 4, 5 ,6);

        uReg8 = (UINT8)(uReg8 & ~USB_MHDRC_POWER_RESET);

        OS_DELAY_MS(50);

        USB_MHDRC_REG_WRITE8(pMHDRC, USB_MHDRC_POWER, uReg8);
        }

    /* Initiate a ULPI WRITE access */

    USB_MHDRC_REG_WRITE8(pMHDRC, USB_MHDRC_ULPIREGADDR, (UINT8)uOffset);
    USB_MHDRC_REG_WRITE8(pMHDRC, USB_MHDRC_ULPIREGDATA, uData);
    USB_MHDRC_REG_WRITE8(pMHDRC, USB_MHDRC_ULPIREGCONTROL,
                        USB_MHDRC_ULPIREGCONTROL_ULPIREGREQ);

    i = 0;

    /* Wait until the access is reported as completed */

    while (!(USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_ULPIREGCONTROL)
                & USB_MHDRC_ULPIREGCONTROL_ULPIREGCMPLT))
        {
        i++;

        if (i == USB_MHDRC_REG_ACCESS_WAIT_COUNT)
            {
            USB_MHDRC_ERR("ULPI write timed out bIsResetActive %d\n",
                pMHDRC->bIsResetActive , 2, 3, 4, 5, 6);

            /* Clear the completed indication */

            USB_MHDRC_REG_WRITE8(pMHDRC, USB_MHDRC_ULPIREGCONTROL, 0);

            return ERROR;
            }

        /* Give other task a chance to run! */

        OS_DELAY_MS(1);
        }

    /* Clear the completed indication */

    USB_MHDRC_REG_WRITE8(pMHDRC, USB_MHDRC_ULPIREGCONTROL, 0);

    return OK;
    }

