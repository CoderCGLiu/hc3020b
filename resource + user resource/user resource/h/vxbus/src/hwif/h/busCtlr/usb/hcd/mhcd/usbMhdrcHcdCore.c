/* usbMhdrcHcdCore.c - Transfer Management of USB MHCI */

/*
 * Copyright 2009-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01r,03may13,wyy  Remove compiler warning (WIND00356717)
01p,21apr13,wyy  Cut the relationship among busIndex of HCDData, unitNumber of
                 vxBus Device, and HC count (such as g_EHCDControllerCount or
                 g_UhcdHostControllerCount) to dynamically release or announce
                 a HCD driver (WIND00362065)
01o,22aug12,ljg  Fix coverity issue "CHECKED_RETURN' (WIND00371808)
01n,09mar11,w_x  Code clean up for make man
01m,07jan11,w_x  Fix port hung up when plug/unplug hub (WIND00226926)
01l,13sep10,ghs  Fix defect found by code review of merge (WIND00232740)
01k,16aug10,w_x  VxWorks 64 bit audit and warning removal
01j,03jun10,s_z  Debug macro changed, Add more debug message,
                 update usbMhdrcHcdIsRequestPending
01i,13mar10,s_z  Add Inventra DMA supported on OMAP3EVM
01h,25feb10,s_z  Rename request list in USB_MHCD_DATA structure(WIND00201709)
01g,27jan10,s_z  Fix cancel urb issue, call back directly if the device is not
                 existed,and code cleaning
01f,17dec09,s_z  Fix Interrupt In issue with hub and add ISO transaction support
01e,01dec09,s_z  Use fixed FIFO size
01d,30nov09,s_z  Fix delete pipe issue
01c,10nov09,s_z  Add usbMhdrcHcdFreeEndPointEngineAsign to asign free endpoint pool
                 for the created pipe, and initialze the FIFO size.
01b,05nov09,s_z  Rewrite according to the structure redesign,change all the APIs
                 according to EHCI mechanism.
01a,13jul09,j_x  initial version
*/

/*
DESCRIPTION

This file defines the transfer management of the USB MHCD (Mentor Graphics USB
Host Controller Driver).

INCLUDE FILES: usb/usbOsal.h, usb/usbHst.h, usb.h, usbMhdrc.h, usbMhdrcIsr.h,
               usbMhdrcRootHubEmulation.h, usbMhdrcScheduleProcess.h,
               usbMhdrcTransferManagement.h usbMhdrcDebug.h
*/

/* includes */

#include <usb/usbOsal.h>
#include <usb/usbHst.h>
#include <usb.h>
#include <usbMhdrc.h>
#include <usbMhdrcHcd.h>
#include <usbMhdrcHcdIsr.h>
#include <usbMhdrcHcdRootHub.h>
#include <usbMhdrcHcdSched.h>
#include <usbMhdrcHcdCore.h>
#include <usbMhdrcHcdDebug.h>

/*******************************************************************************
*
* usbMhdrcHcdGetFrameNumber - get the current frame number of the bus
*
* This routine is used to get the current frame number of the host controller.
*
* <uBusIndex> specifies the host controller bus index.
* <puFrameNumber> is a pointer to a variable to hold the current frame number.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the frame number was obtained successfully.
*   USBHST_INVALID_PARAMETER - Returned if the parameters are not valid.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbMhdrcHcdGetFrameNumber
    (
    UINT8    uBusIndex,    /* Index of the host controller */
    UINT16 * puFrameNumber /* Pointer to the variable to hold the frame number */
    )
    {
    pUSB_MHCD_DATA  pHCDData = NULL; /* Pointer to the data structure */
    pUSB_MUSBMHDRC  pMHDRC;

    /* Check the validity of the parameters */

    if ((USB_MAX_MHCI_COUNT < uBusIndex) ||
        (NULL == puFrameNumber))
        {
        USB_MHDRC_ERR("usbMhdrcHcdGetFrameNumber(): "
                     "Invalid parameter, %s \n",
                     ((USB_MAX_MHCI_COUNT < uBusIndex) ?
                     "uBusIndex too big" :
                     "puFrameNumber is NULL"),
                      2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the global data structure */

    pHCDData = gpMHCDData[uBusIndex];

    /* Return if the global pointer is not valid */

    if ((NULL == pHCDData) || (pHCDData->pMHDRC == NULL))
        {
        USB_MHDRC_ERR("usbMhdrcHcdGetFrameNumber(): "
                     "pHCDData is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Get the common hardware data structure */

    pMHDRC = pHCDData->pMHDRC;

    /*
     * The frame number registe will not change until one usb device plugged in
     * and session start.Which will make the HCD initialization fail.
     *
     * Before the HCD initialization finished, frame number record will be
     * increased by software.
     */

    if (pMHDRC->uFrameNumChangeFlag == 0)
        {
        pMHDRC->uFrameNumberRecord =  (UINT16)((pMHDRC->uFrameNumberRecord + 10)
                                        % USB_MHDRC_HCD_FRAME_NUM_MAX);
        *puFrameNumber = pMHDRC->uFrameNumberRecord ;
        }
    else
        {
        *puFrameNumber = (UINT16)(USB_MHDRC_REG_READ16 (pMHDRC,
                                        USB_MHDRC_FRAME) %
                                        USB_MHDRC_HCD_FRAME_NUM_MAX);

        /*
         * If the current frame numer is still the same as the
         * last time value, it might be the frame number is not
         * advancing any more, so we change it to software simulated
         * frame numbering. Once the SOF interrupt is triggering,
         * then it will switch us into hardware frame numbering
         * by setting pMHDRC->uFrameNumChangeFlag back to 1.
         */

        if (*puFrameNumber == pMHDRC->uFrameNumberRecord)
            pMHDRC->uFrameNumChangeFlag = 0;

        pMHDRC->uFrameNumberRecord = *puFrameNumber;

        }

    return USBHST_SUCCESS;
    }


/*******************************************************************************
*
* usbMhdrcHcdSetBitRate - modify the frame width
*
* This routine is used to modify the frame width of the host controller.
*
* <uBusIndex> specifies the host controller bus index. <bIncrement> is a flag
* to specify whether the frame number should be incremented or decremented.
* If TRUE, the frame number will be incremented, otherwise, decremented.
* <puCurrentFrameWidth> is a pointer to hold the current frame width(after
* modification)
*
* RETURNS: USBHST_FAILURE - returned since the functionality is not
* supported.
*
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbMhdrcHcdSetBitRate
    (
    UINT8    uBusIndex,          /* Index of the host controller       */
    BOOL     bIncrement,         /* Flag for increment or decrement    */
    UINT32 * puCurrentFrameWidth /* Pointer to the current frame width */
    )
    {
    return USBHST_SUCCESS;
    }


/*******************************************************************************
*
* usbMhdrcHcdIsBandwidthAvailable - check the bandwidth availability
*
* This routine is used to check whether there is enough bandwidth to support
* the new configuration or an alternate interface setting of an interface.
*
* <uBusIndex> specifies the host controller bus index.
* <uDeviceAddress> specifies the device address. <uDeviceSpeed> is the speed
* of the device which needs to be modified. <pCurrentDescriptor> is the pointer
* to the current configuration or interface descriptor. If the pNewDescriptor
* corresponds to a USB configuration descriptor, this parameter is ignored
* (i.e. this parameter can be NULL). <pNewDescriptor> is the pointer to the new
* configuration or interface descriptor.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the new configuration or
*       new alternate interface can be supported (ALWAYS)
*   USBHST_INVALID_PARAMETER - Returned if the parameters are
*       not valid.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS  usbMhdrcHcdIsBandwidthAvailable
    (
    UINT8   uBusIndex,            /* Host controller bus index */
    UINT8   uDeviceAddress,       /* Handle to the device addr */
    UINT8   uDeviceSpeed,         /* Speed of the device in default state */
    UCHAR * pCurrentDescriptor,   /* Ptr to current configuration */
    UCHAR * pNewDescriptor        /* Ptr to new configuration */
    )
    {
    /* Return success */

    return USBHST_SUCCESS;
    }


/*******************************************************************************
*
* usbMhdrcHcdEndpointEngineAsign - asign endpoint to pipe
*
* This routine is used to asign endpoint to pipe.
*
* RETURNS: OK or ERROR
*   OK - Asigned endpoint to pipe successfully.
*   ERROR - Returned if the parameters are not valid or assigned failed.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS  usbMhdrcHcdEndpointEngineAsign
    (
    pUSB_MHCD_DATA pHCDData, /* Pointer to HCD Data */
    pUSB_MHCD_PIPE pHCDPipe  /* Pointer to HCD pipe */
    )
    {

    /* Parameter verification */

    if ((NULL == pHCDData) ||
        (NULL == pHCDPipe))
        {
        /* Debug print */

        USB_MHDRC_ERR("usbMhdrcHcdEndpointEngineAsign(): "
                     "Invalid parameter, %s is NULL\n",
                     ((NULL == pHCDData) ? "pHCDData" :
                     "pHCDPipe"),
                     2, 3, 4, 5 ,6);

        return ERROR;
        }

    /* All the Control pipe asigned to EP0 */

    if (pHCDPipe->uEndpointType == USB_ATTR_CONTROL)
        {
        pHCDPipe->uEndPointEngineAsigned = USB_MHCI_EP_0;
        return OK;
        }
    else if (pHCDPipe->uEndpointType == USB_ATTR_ISOCH)
        {
        pHCDPipe->uEndPointEngineAsigned = USB_MHCI_EP_4;
        return OK;
        }
    else if (pHCDPipe->uEndpointType == USB_ATTR_BULK)
        {
        pHCDPipe->uEndPointEngineAsigned = USB_MHCI_EP_3;
        return OK;
        }

    if ((pHCDPipe->uMaximumPacketSize <= USB_MHCI_MAX_PACKET_SIZE_128) &&
        ((pHCDPipe->uMaximumPacketSize == 1)))
        {
        pHCDPipe->uEndPointEngineAsigned = USB_MHCI_EP_1;
        }
    else if (pHCDPipe->uMaximumPacketSize <= USB_MHCI_MAX_PACKET_SIZE_256)
        {
        pHCDPipe->uEndPointEngineAsigned = USB_MHCI_EP_2;
        }
    else if (pHCDPipe->uMaximumPacketSize <= USB_MHCI_MAX_PACKET_SIZE_512)
        {
        pHCDPipe->uEndPointEngineAsigned = USB_MHCI_EP_3;
        }
    else
        {
        USB_MHDRC_ERR("usbMhdrcHcdEndpointEngineAsign(): "
                     "No bigger space \n",
                     1, 2, 3, 4, 5 ,6);

        return ERROR;
        }

    USB_MHDRC_DBG("usbMhdrcHcdEndpointEngineAsign(): EP %p assigned to ch %d \n",
                 pHCDPipe->uEndpointAddress,
                 pHCDPipe->uEndPointEngineAsigned,
                 3, 4, 5, 6);

    /* Return success */

    return OK;
    }


/*******************************************************************************
*
* usbMhdrcHcdModifyDefaultPipe - modify the default pipe characteristics
*
* This routine is used to modify the properties (device speed and maximum
* packet size) of the default pipe (address 0, endpoint 0). <uBusIndex>
* specifies the host controller bus index. <uDefaultPipeHandle> holds the
* pipe handle of the default pipe. <uDeviceSpeed> is the speed of the
* device which needs to be modified. <uMaxPacketSize> is the maximum packet
* size of the default pipe which needs to be modified. <uHighSpeedHubInfo>
* specifies the nearest high speed hub and the port number information. This
* information will be used to handle a split transfer to the full / low speed
* device. The high byte will hold the high speed hub address. The low byte
* will hold the port number to which the USB 1.1 device is connected.
*
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the deafult pipe properties were modified
*       successfully.
*   USBHST_INVALID_PARAMETER - Returned if the parameters are not valid.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbMhdrcHcdModifyDefaultPipe
    (
    UINT8   uBusIndex,          /* Host controller bus index               */
    ULONG   uDefaultPipeHandle, /* Handle to the default pipe              */
    UINT8   uDeviceSpeed,       /* Speed of the device in default state    */
    UINT8   uMaxPacketSize,     /* Maximum packet size of the default pipe */
    UINT16  uHighSpeedHubInfo   /* High speed hub info for USB 1.1 device  */
    )
    {
    pUSB_MHCD_PIPE         pHCDPipe = NULL;
    pUSB_MHCD_DATA         pHCDData = NULL;
    pUSB_MHCD_REQUEST_INFO pRequestInfo = NULL;
    NODE *                 pRequestNode = NULL;

    /* Check the validity of the parameters */

    /* NOTE: the speed need translate to the MhdrcHcd speed */

    if ((USB_MAX_MHCI_COUNT < uBusIndex) ||
        (0 == uDefaultPipeHandle) ||
        (USBHST_HIGH_SPEED != uDeviceSpeed &&
         USBHST_FULL_SPEED != uDeviceSpeed &&
         USBHST_LOW_SPEED != uDeviceSpeed))
        {
        USB_MHDRC_ERR("usbMhdrcHcdModifyDefaultPipe(): "
                     "Invalid parameter, %s \n",
                     ((USB_MAX_MHCI_COUNT < uBusIndex) ?
                     "USB_MAX_MHCI_COUNT < uBusIndex" :
                     (0 == uDefaultPipeHandle) ?
                     "uDefaultPipeHandle is 0" :
                     "uDeviceSpeed invalid"),
                     2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the global data structure */

    pHCDData = gpMHCDData[uBusIndex];

    /* Assert if the global pointer is not valid */

    if (NULL == pHCDData)
        {
        USB_MHDRC_ERR("usbMhdrcHcdModifyDefaultPipe(): "
                     "pHCDData is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Check if this request is for the Root hub */

    if (0 == pHCDData->RHData.uDeviceAddress)
        {
        return USBHST_SUCCESS;
        }

    /* Extract the default pipe handle */

    pHCDPipe = (pUSB_MHCD_PIPE)uDefaultPipeHandle;

    semTake(pHCDPipe->pPipeSynchMutex,WAIT_FOREVER);

    if ((pHCDPipe->uSpeed != uDeviceSpeed) ||
        (pHCDPipe->uMaximumPacketSize != uMaxPacketSize) ||
        (pHCDPipe->uHubInfo != uHighSpeedHubInfo))
        {
        pRequestNode = lstFirst(&pHCDPipe->RequestInfoList);

        while (pRequestNode != NULL)
            {
            pRequestInfo = (pUSB_MHCD_REQUEST_INFO)(pRequestNode);

            if ((NULL != pRequestNode) &&
                (pRequestInfo->pHCDPipe == pHCDPipe))
                {
                 USB_MHDRC_VDBG("usbMhdrcHcdModifyDefaultPipe(): "
                               "Still have pRequestInfo %p in defalut pipe\n",
                               pRequestInfo,
                               2, 3, 4, 5 ,6);

                /* Stop the transaction and Call back the urb directly */

                usbMhdrcHcdRequestTransferDone(pHCDData,
                                           pRequestInfo,
                                           USBHST_TRANSFER_CANCELLED);

                }
            pRequestNode = lstNext(pRequestNode);
            }
        }

    pHCDPipe->uPipeFlag = USB_MHCD_PIPE_FLAG_OPEN;

    pHCDPipe->uMaximumPacketSize = uMaxPacketSize;

    /* Update the speed */

    pHCDPipe->uSpeed             = uDeviceSpeed;
    pHCDPipe->uHubInfo           = uHighSpeedHubInfo;
    pHCDPipe->bInterval          = 0;
    pHCDPipe->uEndpointAddress   = 0;
    pHCDPipe->uEndpointType      = USB_ATTR_CONTROL;

    /* Release the exclusive list access */

    USB_MHDRC_VDBG("usbMhdrcHcdModifyDefaultPipe(): To speed %p, hubInfo %p MaxPktSize %p\n",
                  pHCDPipe->uSpeed,
                  pHCDPipe->uHubInfo,
                  pHCDPipe->uMaximumPacketSize, 4, 5 ,6);


    semGive(pHCDPipe->pPipeSynchMutex);

    return USBHST_SUCCESS;
    }


/*******************************************************************************
*
* usbMhdrcHcdCreatePipe - create a pipe specific to an endpoint.
*
* This routine creates the host controller driver specific data structure.
*
* <uBusIndex> is the index of the host controller.
* <uDeviceAddress> is the address of the device holding the endpoint.
* <uDeviceSpeed> is the speed of the device holding the endpoint.
* <pEndpointDescriptor> is the pointer to the endpoint descriptor.
* <uHighSpeedHubInfo> is the high speed hub information.
* <puPipeHandle> is the pointer to the handle to the pipe.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the pipe was created successfully.
*   USBHST_INVALID_PARAMETER - Returned if the parameters are not valid.
*   USBHST_INSUFFICIENT_MEMORY - Returned if the memory allocation
*                                for the pipe failed.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbMhdrcHcdCreatePipe
    (
    UINT8    uBusIndex,          /* Host controller index      */
    UINT8    uDeviceAddress,     /* USB device address         */
    UINT8    uDeviceSpeed,       /* USB device speed           */
    UCHAR *  pEndpointDescriptor,/* Endpoint descriptor        */
    UINT16   uHighSpeedHubInfo,  /* High speed hub information */
    ULONG *  puPipeHandle        /* Pointer to the pipe handle */
    )
    {
    pUSBHST_ENDPOINT_DESCRIPTOR pEndpointDesc = NULL;
    pUSB_MHCD_PIPE              pHCDPipe = NULL;
    pUSB_MHCD_DATA              pHCDData = NULL;
    USBHST_STATUS               Status = USBHST_FAILURE;
    UINT8                       uEndpointType;

    /* Parameters verification */

    if ((USB_MAX_MHCI_COUNT < uBusIndex) ||
        (USB_MHDRC_HCD_MAX_DEVICE_ADDRESS < uDeviceAddress) ||
        (NULL == pEndpointDescriptor) ||
        (NULL == puPipeHandle))
        {
        USB_MHDRC_ERR("usbMhdrcHcdCreatePipe(): "
                     "Invalid parameter, %s \n",
                     ((USB_MAX_MHCI_COUNT < uBusIndex) ?
                     "USB_MAX_MHCI_COUNT < uBusIndex" :
                     (USB_MHDRC_HCD_MAX_DEVICE_ADDRESS < uDeviceAddress) ?
                     "uDeviceAddress > 127" :
                     (NULL == pEndpointDescriptor) ?
                     "pEndpointDescriptor is NULL" :
                     "puPipeHandle is NULL"),
                     2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the global data structure */

    pHCDData = gpMHCDData[uBusIndex];

    /* Return if the global pointer is not valid */

    if (NULL == pHCDData)
        {
        USB_MHDRC_ERR("usbMhdrcHcdCreatePipe(): "
                     "pHCDData is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Check if the request is for the Root hub and route it */

    if (uDeviceAddress == pHCDData->RHData.uDeviceAddress)
        {
        Status = usbMhdrcHcdRootHubCreatePipe(pHCDData,
                                          uDeviceAddress,
                                          uDeviceSpeed,
                                          pEndpointDescriptor,
                                          puPipeHandle);
        return Status;
        }

    /* Retrieve the endpoint descriptor */

    pEndpointDesc = (pUSBHST_ENDPOINT_DESCRIPTOR)pEndpointDescriptor;

    uEndpointType =
        (UINT8)((pEndpointDesc->bmAttributes & USB_ATTR_EPTYPE_MASK));

    /* Check the validity of the endpoint type */

    if ((USBHST_ISOCHRONOUS_TRANSFER != uEndpointType) &&
        (USBHST_INTERRUPT_TRANSFER != uEndpointType) &&
        (USBHST_CONTROL_TRANSFER != uEndpointType) &&
        (USBHST_BULK_TRANSFER != uEndpointType))
        {
        USB_MHDRC_ERR("usbMhdrcHcdCreatePipe(): "
                     "Endpoint types is not valid\n",
                     1, 2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Allocate memory for the USB_EHCD_PIPE data structure */

    pHCDPipe = (pUSB_MHCD_PIPE)OS_MALLOC(sizeof(USB_MHCD_PIPE));

    /* Check if memory allocation is successful */

    if (NULL == pHCDPipe)
        {
        USB_MHDRC_ERR("usbMhdrcHcdCreatePipe(): "
                     "Memory not allocated for MHCD pipe\n",
                     1, 2, 3, 4, 5 ,6);

        return USBHST_MEMORY_NOT_ALLOCATED;
        }

    OS_MEMSET(pHCDPipe, 0, sizeof(USB_MHCD_PIPE));

    pHCDPipe->pPipeSynchMutex = semMCreate (USB_MHDRC_HCD_MUTEX_CREATION_OPTS);

    if (NULL == pHCDPipe->pPipeSynchMutex)
        {
        USB_MHDRC_ERR("usbMhdrcHcdCreatePipe(): Basic Mutex Creation failed\n",
                     1, 2, 3, 4, 5 ,6);

        OS_FREE(pHCDPipe);

        return USBHST_FAILURE;
        }

    /* Update the pointer to the pipe handle */

    *puPipeHandle = (ULONG)pHCDPipe;

    pHCDPipe->uPipeFlag = USB_MHCD_PIPE_FLAG_OPEN;

    /* Recorde the Bus Index */

    pHCDPipe->uBusIndex = uBusIndex;

    /* Update the endpoint speed */

    pHCDPipe->uSpeed = uDeviceSpeed;

    /* Update the device address which holds the endpoint */

    pHCDPipe->uDeviceAddress = uDeviceAddress;

    pHCDPipe->uHandle = (pHCDPipe->uBusIndex << 8) | pHCDPipe->uDeviceAddress;

    /* Update the type of endpoint to be created */

    pHCDPipe->uEndpointType = uEndpointType;

    /* Update the direction of the endpoint */

    pHCDPipe->uEndpointDir = (UINT8)
    ((pEndpointDesc->bEndpointAddress & USB_ENDPOINT_IN) ? USB_DIR_IN : USB_DIR_OUT);

    /* Update Endpoint address */

    pHCDPipe->uEndpointAddress = pEndpointDesc->bEndpointAddress;

    /* Init the requestinfo list */

    lstInit(&(pHCDPipe->RequestInfoList));

    /*
     * [Quote]
     * Interval for polling endpoint for data transfers.
     * Expressed in frames or microframes depending on the
     * device operating speed (i.e., either 1 millisecond or
     * 125 us units).
     *
     * For full-/high-speed isochronous endpoints, this value
     * must be in the range from 1 to 16. The bInterval value
     * is used as the exponent for value.
     *
     * For full-/low-speed interrupt endpoints, the value of
     * this field may be from 1 to 255.
     *
     * For high-speed interrupt endpoints, the bInterval value
     * is used as the exponent for a value. This value
     * must be from 1 to 16.
     *
     * For high-speed bulk/control OUT endpoints, the
     * bInterval must specify the maximum NAK rate of the
     * endpoint. A value of 0 indicates the endpoint never
     * NAKs. Other values indicate at most 1 NAK each
     * bInterval number of microframes. This value must be
     * in the range from 0 to 255.
     * [Quote]
     */

     if (pHCDPipe->uEndpointType == USB_ATTR_INTERRUPT)
        {
        if (pHCDPipe->uSpeed != USBHST_HIGH_SPEED)
            {
            pHCDPipe->bInterval = (UINT8)((pEndpointDesc->bInterval == 0) ?
                                    1 : pEndpointDesc->bInterval);
            }
        else
            {
            pHCDPipe->bInterval = (UINT8)((pEndpointDesc->bInterval > 16) ?
                                    16 : pEndpointDesc->bInterval);
            }
        }

   /* Here for ASYNC endpoint, we set the interval 0 */

   if ((pHCDPipe->uEndpointType == USB_ATTR_BULK) ||
       (pHCDPipe->uEndpointType == USB_ATTR_CONTROL))
        {
        pHCDPipe->bInterval = 0;
        }

    /*
     * Store the maximum packet size - for high speed includes the number
     * of transactions in a microframe also
     */

    pHCDPipe->uMaximumPacketSize =
                   OS_UINT16_LE_TO_CPU(pEndpointDesc->wMaxPacketSize);
    pHCDPipe->uDataToggle = 0;

    /* Update the high speed hub information */

    pHCDPipe->uHubInfo = uHighSpeedHubInfo;

    usbMhdrcHcdEndpointEngineAsign(pHCDData, pHCDPipe);

    semTake(pHCDData->pHcdSynchMutex, WAIT_FOREVER);

    /* Add the pipe to the pipe list */

    switch(pHCDPipe->uEndpointType)
        {
        case USB_ATTR_CONTROL:
        case USB_ATTR_BULK:
            lstAdd(&(pHCDData->NonPeriodicPipeList), &(pHCDPipe->pipeListNode));
            break;
        case USB_ATTR_INTERRUPT:
        case USB_ATTR_ISOCH:
            lstAdd(&(pHCDData->PeriodicPipeList), &(pHCDPipe->pipeListNode));
            break;
        default:
            break;
        }
    semGive(pHCDData->pHcdSynchMutex);

    USB_MHDRC_VDBG("usbMhdrcHcdCreatePipe(): Created Dev %d, Ep %x \n",
                  pHCDPipe->uDeviceAddress,
                  pHCDPipe->uEndpointAddress,
                  3, 4, 5 ,6);

    return USBHST_SUCCESS;
    }


/*******************************************************************************
*
* usbMhdrcHcdDeletePipe - delete a pipe created already
*
* This routine is used to delete a pipe specific to an endpoint.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the pipe was deleted successfully
*   USBHST_INVALID_PARAMETER - Returned if the parameters are
*                              not valid.
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbMhdrcHcdDeletePipe
    (
    UINT8   uBusIndex,   /* Index of the host controller     */
    ULONG   uPipeHandle  /* Handle of the pipe to be deleted */
    )
    {
    /* To hold the pointer to the data structure */

    pUSB_MHCD_DATA         pHCDData     = NULL;
    pUSB_MHCD_PIPE         pHCDPipe     = NULL;
    pUSB_MHCD_REQUEST_INFO pRequestInfo = NULL;
    NODE *                 pRequestNode = NULL;
    USBHST_STATUS          Status       = USBHST_FAILURE;

    /* Check the validity of the parameters */

    if ((USB_MAX_MHCI_COUNT < uBusIndex) ||
        (0 == uPipeHandle) )
        {
        USB_MHDRC_ERR("usbMhdrcHcdDeletePipe(): "
                     "Invalid parameter, %s \n",
                     ((USB_MAX_MHCI_COUNT < uBusIndex) ?
                     "USB_MAX_MHCI_COUNT < uBusIndex" :
                     "uPipeHandle is NULL"),
                     2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the global data structure */

    pHCDData = gpMHCDData[uBusIndex];

    /* Assert if the global pointer is not valid */

    if (NULL == pHCDData)
        {
        USB_MHDRC_ERR("usbMhdrcHcdDeletePipe(): "
                     "pHCDData is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the USB_MHCD_PIPE data structure */

    pHCDPipe = (pUSB_MHCD_PIPE)uPipeHandle;

    USB_MHDRC_VDBG("usbMhdrcHcdDeletePipe(): "
                  "pHCDPipe %p \n", pHCDPipe, 2,3,4,5,6);


    /* Check if the request is for the Root hub and route it */

    if ((pHCDData->RHData.pInterruptPipe == pHCDPipe) ||
        (pHCDData->RHData.pControlPipe == pHCDPipe))
        {
        Status = usbMhdrcHcdRootHubDeletePipe(pHCDData,
                                          uPipeHandle);

        return Status;
        }

    if (pHCDPipe->pPipeSynchMutex == NULL)
        {
        USB_MHDRC_ERR("usbMhdrcHcdDeletePipe(): "
                     "pHCDPipe->pPipeSynchMutex is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }


    semTake(pHCDPipe->pPipeSynchMutex, WAIT_FOREVER);

    if (pHCDPipe == pHCDData->pDefaultPipe)
        {
        pHCDPipe->uPipeFlag |= USB_MHCD_PIPE_FLAG_MODIFY_DEFAULT_PIPE;
        }

    /*
     * Mark the pipe as to be deleted
     * So, when there are some request have not scheduled,
     * which will not be scheduled
     */

    pHCDPipe->uPipeFlag |= USB_MHCD_PIPE_FLAG_DELETE;

    pRequestNode = lstFirst(&pHCDPipe->RequestInfoList);

    while (pRequestNode != NULL)
        {
        pRequestInfo = (pUSB_MHCD_REQUEST_INFO)(pRequestNode);

        if ((NULL != pRequestNode) &&
            (pRequestInfo->pHCDPipe == pHCDPipe))
            {
            USB_MHDRC_VDBG("usbMhdrcHcdDeletePipe(): "
                          "Find pRequestInfo %p On the pHCDPipe %p, cancel it\n",
                          pRequestInfo,
                          pHCDPipe,
                          3,4,5,6);

            /* Stop the transaction and Call back the urb directly */

            usbMhdrcHcdRequestTransferDone(pHCDData,
                                       pRequestInfo,
                                       USBHST_TRANSFER_CANCELLED);


            }
        pRequestNode = lstNext(pRequestNode);
        }

    semGive(pHCDPipe->pPipeSynchMutex);

    semTake(pHCDData->pHcdSynchMutex, WAIT_FOREVER);

    /*
     * If there is no request belong to the pipe, we can delete it here,
     * or, delete it in the complite routine
     */

    if (lstCount(&pHCDPipe->RequestInfoList) == 0)
        {
        switch(pHCDPipe->uEndpointType)
            {
            case USB_ATTR_CONTROL:
            case USB_ATTR_BULK:
                 if(ERROR != lstFind(&(pHCDData->NonPeriodicPipeList),
                                      &(pHCDPipe->pipeListNode)))
                      {
                      lstDelete(&(pHCDData->NonPeriodicPipeList),
                                &(pHCDPipe->pipeListNode));
                      }
                break;
            case USB_ATTR_INTERRUPT:
            case USB_ATTR_ISOCH:
                 if(ERROR != lstFind(&(pHCDData->PeriodicPipeList),
                                      &(pHCDPipe->pipeListNode)))
                      {
                      lstDelete(&(pHCDData->PeriodicPipeList),
                                &(pHCDPipe->pipeListNode));
                      }
                break;
            default:
                break;
            }

        USB_MHDRC_VDBG("usbMhdrcHcdDeletePipe(): "
                     "No pRequestInfoOn the pHCDPipe %p, delete it and free\n",
                     pHCDPipe,
                     2,3,4,5,6);

        /* Delete the request info Mutex */

        if (pHCDPipe != pHCDData->pDefaultPipe)
            {
            /* Mutex should be taken before delete it */

           if (OK == semTake(pHCDPipe->pPipeSynchMutex, WAIT_FOREVER))
                {
                semDelete (pHCDPipe->pPipeSynchMutex);
                pHCDPipe->pPipeSynchMutex = NULL;
                OSS_FREE(pHCDPipe);
                pHCDPipe = NULL;
                }
            }
        else
            {
            pHCDPipe->uPipeFlag = (UINT8)(pHCDPipe->uPipeFlag & ~USB_MHCD_PIPE_FLAG_DELETE);
            if (pHCDPipe == pHCDData->pDefaultPipe)
                {
                pHCDPipe->uPipeFlag = (UINT8)(pHCDPipe->uPipeFlag & ~USB_MHCD_PIPE_FLAG_MODIFY_DEFAULT_PIPE);
                }
            }
        }
    else
        {

        /*
         * When we find request on the pipe, we first call back it
         * But still not remove, send MsgQ to remove it
         */

        USB_MHDRC_VDBG("usbMhdrcHcdDeletePipe(): "
                      "Has pRequestInfo On the pHCDPipe %p, Send MsgQ to remove\n",
                      pHCDPipe,
                      2,3,4,5,6);

        pRequestNode = lstFirst(&(pHCDPipe->RequestInfoList));
        if (pRequestNode != NULL)
            {
            usbMhdrcHcdTransferTaskFeed(pHCDData,
                                    (pUSB_MHCD_REQUEST_INFO)(pRequestNode),
                                    USB_MHCD_TRANSFER_CMD_COMPLITE_PROCESS,
                                    USBHST_TRANSFER_CANCELLED);

            }
        }

    semGive(pHCDData->pHcdSynchMutex);

    return USBHST_SUCCESS;

    }


/*******************************************************************************
*
* usbMhdrcHcdSubmitURB - submit a request to a pipe
*
* This routine is used to submit a request to the pipe. <uBusIndex> specifies
* the host controller bus index. <uPipeHandle> holds the pipe handle. <pURB>
* is the pointer to the URB holding the request details.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the URB is submitted successfully.
*   USBHST_INVALID_PARAMETER - Returned if the parameters are not valid.
*   USBHST_INSUFFICIENT_BANDWIDTH - Returned if memory is insufficient for the
*       request.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbMhdrcHcdSubmitURB
    (
    UINT8        uBusIndex,   /* Index of the host controller */
    ULONG        uPipeHandle, /* Pipe handle */
    pUSBHST_URB  pURB         /* Pointer to the URB */
    )
    {
    pUSB_MHCD_DATA          pHCDData = NULL;
    pUSB_MHCD_PIPE          pHCDPipe = NULL;
    pUSB_MHCD_REQUEST_INFO  pRequestInfo = NULL;
    pUSBHST_ISO_PACKET_DESC pIsoPacketDesc = NULL;
    USBHST_STATUS           Status = USBHST_FAILURE;

    /* Check the validity of the parameters */

    if ((USB_MAX_MHCI_COUNT < uBusIndex) ||
        (0 == uPipeHandle) ||
        (NULL == pURB))
        {
        USB_MHDRC_ERR("usbMhdrcHcdSubmitURB(): "
                     "Invalid parameter, %s \n",
                     ((USB_MAX_MHCI_COUNT < uBusIndex) ?
                     "USB_MAX_MHCI_COUNT < uBusIndex" :
                     (0 == uPipeHandle) ?
                     "uPipeHandle is 0" :
                     "pURB is NULL"),
                     2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the global data structure */

    pHCDData = gpMHCDData[uBusIndex];

    /* Return if the global pointer is not valid */

    if (NULL == pHCDData)
        {
        USB_MHDRC_ERR("usbMhdrcHcdSubmitURB(): pHCDData is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the  data structure */

    pHCDPipe = (pUSB_MHCD_PIPE)uPipeHandle;

    /* Check if the request is for the Root hub and route it */

    if ((pHCDData->RHData.pInterruptPipe == pHCDPipe) ||
        (pHCDData->RHData.pControlPipe == pHCDPipe) ||
        ((pHCDData->pDefaultPipe == pHCDPipe) &&
         (0 == pHCDData->RHData.uDeviceAddress)))
         {
         Status = usbMhdrcHcdRootHubSubmitURB(pHCDData,
                                          uPipeHandle,
                                          pURB);
         return Status;
         }

   /* Exclusively access the list */

   if (pHCDPipe->uPipeFlag & USB_MHCD_PIPE_FLAG_DELETE)
       {
       USB_MHDRC_ERR("usbMhdrcHcdSubmitURB(): "
                    "ERROR, the pipe will be deleted or HC removed\n",
                    1, 2, 3, 4, 5 ,6);

       return USBHST_FAILURE;
       }

    /*
     * If there is no device on the root hub
     */

    if (0 == (pHCDData->RHData.pRootHubPortStatus[0].uRHPortStatus &
        USB_HUB_STS_PORT_CONNECTION))
        {
        USB_MHDRC_ERR("usbMhdrcHcdSubmitURB(): "
                     "ERROR, no device exist on the root hub\n",
                     1, 2, 3, 4, 5 ,6);

        return USBHST_FAILURE;
        }

    /* Allocate the request information structure */

    pRequestInfo = (pUSB_MHCD_REQUEST_INFO)
                        OS_MALLOC(sizeof(USB_MHCD_REQUEST_INFO));

    /* Check if memory allocation is successful */

    if (NULL == pRequestInfo)
        {
        USB_MHDRC_ERR("usbMhdrcHcdSubmitURB(): "
                     "Memory not allocated for the request info\n",
                     1, 2, 3, 4, 5 ,6);

        return USBHST_FAILURE;
        }

    /* Initialize the fields of the data structure */

    OS_MEMSET(pRequestInfo, 0, sizeof(USB_MHCD_REQUEST_INFO));

    semTake(pHCDPipe->pPipeSynchMutex, WAIT_FOREVER);

    /* Copy the URB pointer */

    pRequestInfo->pUrb = pURB;

    /* Store the USB_MHCD_PIPE pointer */

    pRequestInfo->pHCDPipe = pHCDPipe;

    /* Store the pUSB_MHCD_DATA pointer */

    pRequestInfo->pHCDData = pHCDData;

    /* Store the URB's total packet count*/

    pRequestInfo->uIsoUrbTotalPacketCount = pURB->uNumberOfPackets;
    pRequestInfo->uIsoUrbCurrentPacketIndex = 0;

    if (pHCDData->pDefaultPipe == pHCDPipe)
        {
        pHCDPipe->uEndpointType = USB_ATTR_CONTROL;
        }

    if (pHCDPipe->uEndpointType == USB_ATTR_CONTROL)
        {
        pRequestInfo->uStage = USB_MHCD_REQUEST_STAGE_SETUP;
        }

    if ((pHCDPipe->uEndpointType == USB_ATTR_ISOCH))
        {
        pRequestInfo->uIsoPipeUrbIndex = pHCDPipe->uIsoPipeUrbCount;
        pHCDPipe->uIsoPipeUrbCount =
            (UINT16)((pHCDPipe->uIsoPipeUrbCount + 1) % 0xFFFF);

        pIsoPacketDesc = (pUSBHST_ISO_PACKET_DESC)(pURB->pTransferSpecificData);
        }

    pRequestInfo->uXferSize = pRequestInfo->pUrb->uTransferLength;

    pRequestInfo->uActLength = 0;

    if (pHCDPipe->uEndpointDir == USB_DIR_IN)
        {
        /* Invalidate the cache so that next access is from RAM */

        cacheInvalidate(DATA_CACHE,
                        (void *)(pURB->pTransferBuffer),
                        pURB->uTransferLength);
        }
    else
        {
        if (pHCDPipe->uEndpointType != USB_ATTR_CONTROL)
            {
            /* Flush cache */

            cacheFlush(DATA_CACHE,
                       (void *)(pURB->pTransferBuffer),
                       pURB->uTransferLength);
            }
        }

    /* Update the frame number */

    usbMhdrcHcdGetFrameNumber(pHCDData->uBusIndex, &pRequestInfo->uFrameNumberLast);

    pRequestInfo->uFrameNumberNext = pRequestInfo->uFrameNumberLast;

    USB_MHDRC_VDBG("usbMhdrcHcdSubmitURB(): "
                  "pUrb %p uXferSize %p for pHCDPipe %p device %d Ep %p\n",
                  pRequestInfo->pUrb,
                  pRequestInfo->uXferSize,
                  pHCDPipe,
                  pHCDPipe->uDeviceAddress,
                  pHCDPipe->uEndpointAddress,6);


    /* Add the requeset into the pipe request list */

    lstAdd(&(pRequestInfo->pHCDPipe->RequestInfoList),
           &(pRequestInfo->requestNode));

    semGive(pHCDPipe->pPipeSynchMutex);

    /* Schedule all the requesets */

    usbMhdrcHcdTransferTaskFeed(pHCDData,
                NULL,
                USB_MHCD_TRANSFER_CMD_SHEDULE_PROCESS,
                0);

    return USBHST_SUCCESS;
    }


/*******************************************************************************
*
* usbMhdrcHcdCancelURB - cancel a request to a pipe.
*
* This routine is used to cancel a request to the pipe. <uBusIndex> specifies
* the host controller bus index. <uPipeHandle> holds the pipe handle. <pURB>
* pointer to the URB holding the request details.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the URB is cancelled successfully.
*   USBHST_INVALID_PARAMETER - Returned if the parameters are
*       not valid.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbMhdrcHcdCancelURB
    (
    UINT8       uBusIndex,   /* Index of the host controller */
    ULONG       uPipeHandle, /* Pipe handle */
    pUSBHST_URB pURB         /* Pointer to the URB */
    )
    {
    pUSB_MHCD_DATA         pHCDData     = NULL;
    pUSB_MHCD_PIPE         pHCDPipe     = NULL;
    pUSB_MHCD_REQUEST_INFO pRequestInfo = NULL;
    NODE *                 pRequestNode = NULL;
    USBHST_STATUS          Status       = USBHST_FAILURE;

    /* Check the validity of the parameters */

     if ((USB_MAX_MHCI_COUNT < uBusIndex) ||
        (0 == uPipeHandle) ||
        (NULL == pURB))
        {
        USB_MHDRC_ERR("usbMhdrcHcdCancelURB(): "
                     "Invalid parameter, %s \n",
                     ((USB_MAX_MHCI_COUNT < uBusIndex) ?
                     "USB_MAX_MHCI_COUNT < uBusIndex" :
                     (0 == uPipeHandle) ?
                     "uPipeHandle is 0" :
                     "pURB is NULL"),
                     2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the global data structure */

    pHCDData = gpMHCDData[uBusIndex];

    /* Assert if the global pointer is not valid */

    if (NULL == pHCDData)
        {
        USB_MHDRC_ERR("usbMhdrcHcdCancelURB(): pHCDData is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the USB_MHCD_PIPE data structure */

    pHCDPipe = (pUSB_MHCD_PIPE)uPipeHandle;

    /* Check if the request is for the Root hub and route it */

    if (pHCDData->RHData.pInterruptPipe == pHCDPipe)
        {
        Status = usbMhdrcHcdRootHubCancelURB(pHCDData,
                                         uPipeHandle,
                                         pURB);
        return Status;
        }

    /* Exclusively access the list */

    semTake(pHCDPipe->pPipeSynchMutex, WAIT_FOREVER);

    /* Find this urb from the waiting list */

    pRequestNode = lstFirst(&pHCDPipe->RequestInfoList);

    while (pRequestNode != NULL)
        {
        pRequestInfo = (pUSB_MHCD_REQUEST_INFO)(pRequestNode);

        if ((NULL != pRequestNode) &&
            (pRequestInfo->pHCDPipe == pHCDPipe) &&
            (pRequestInfo->pUrb == pURB))
            {
            semGive(pHCDPipe->pPipeSynchMutex);

            USB_MHDRC_VDBG("usbMhdrcHcdCancelURB(): Find the Request can call back\n",
                          1, 2, 3, 4, 5 ,6);

            /* Stop the transaction and Call back the urb directly */

            usbMhdrcHcdRequestTransferDone(pHCDData,
                                       pRequestInfo,
                                       USBHST_TRANSFER_CANCELLED);

            return USBHST_SUCCESS;
            }
        pRequestNode = lstNext(pRequestNode);
        }

    /* Not in the request list ? call back directly */

    pURB->nStatus = USBHST_TRANSFER_CANCELLED;

    semGive(pHCDPipe->pPipeSynchMutex);

    USB_MHDRC_VDBG("usbMhdrcHcdCancelURB(): No Find the Request? "
                  "Should not happen, call back\n",
                  1, 2, 3, 4, 5 ,6);

    if (pURB->pfCallback)
       (pURB->pfCallback)(pURB);

    /* Free resource */

    OSS_FREE(pRequestInfo);

    return USBHST_SUCCESS;
    }


/*******************************************************************************
*
* usbMhdrcHcdIsRequestPending - check if a request is pending for a pipe.
*
* This routine is used to check whether any request is pending on a pipe.
* <uBusIndex> Specifies the host controller bus index. <uPipeHandle> holds the
* pipe handle.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if a request is pending for the pipe.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbMhdrcHcdIsRequestPending
    (
    UINT8    uBusIndex,   /* Index of the host controller */
    ULONG    uPipeHandle  /* Pipe handle */
    )
    {
    pUSB_MHCD_DATA          pHCDData = NULL;
    pUSB_MHCD_PIPE          pHCDPipe = NULL;
    USBHST_STATUS           Status = USBHST_FAILURE;

    /* Check the validity of the parameters */

    if ((USB_MAX_MHCI_COUNT < uBusIndex) ||
        (0 == uPipeHandle))
        {
        USB_MHDRC_ERR("usbMhdrcHcdIsRequestPending(): "
                     "Invalid parameter, %s \n",
                     ((USB_MAX_MHCI_COUNT < uBusIndex) ?
                     "USB_MAX_MHCI_COUNT < uBusIndex" :
                     "uPipeHandle is 0"),
                     2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the global data structure */

    pHCDData = gpMHCDData[uBusIndex];

    /* Return if the global pointer is not valid */

    if (NULL == pHCDData)
        {
        USB_MHDRC_ERR("usbMhdrcHcdIsRequestPending(): pHCDData is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the  data structure */

    pHCDPipe = (pUSB_MHCD_PIPE)uPipeHandle;

    semTake(pHCDPipe->pPipeSynchMutex, WAIT_FOREVER);

    /*
     * If there are any URBs pending on the pipe then return USBHST_SUCCESS
     * to indicate this situation
     */

    if (lstCount(&pHCDPipe->RequestInfoList) > 0)
        {
        USB_MHDRC_VDBG("usbMhdrcHcdIsRequestPending(): Request Pending\n",
             1, 2, 3, 4, 5 ,6);

        Status = USBHST_SUCCESS;
        }
    else
        {
        USB_MHDRC_VDBG("usbMhdrcHcdIsRequestPending(): No Request Pending\n",
             1, 2, 3, 4, 5 ,6);

        Status = USBHST_FAILURE;
        }

    semGive(pHCDPipe->pPipeSynchMutex);

    return Status;
    }


/*******************************************************************************
*
* usbMhdrcHcdClearTTRequestComplete - To indicate a ClearTTBuffer completion.
*
* This routine is called by the USBD to indicate a completion of the
* ClearTTBuffer request. <uRelativeBusIndex> is the relative bus index of the
* host controller <pContext> is the context which comes from the USBD.
* <status> is the status of the ClearTT request completion
*
* RETURNS:
*   USBHST_SUCCESS - If the request is completed successfully.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbMhdrcHcdClearTTRequestComplete
    (
    UINT8         uRelativeBusIndex,  /* bus index of host controller */
    void *        pContext,           /* usbd context value           */
    USBHST_STATUS nStatus             /* status of request completion */
    )
    {
    return USBHST_SUCCESS;
    }


/*******************************************************************************
*
* usbMhdrcHcdResetTTRequestComplete - To indicate a ResetTT completion.
*
* This routine is called by the USBD to indicate a completion of the
* ResetTT request. <uRelativeBusIndex> is the relative bus index of the host
* controller. <pContext> is the context which comes from the USBD. <status>
* is the Status of the reset TT request completion
*
* RETURNS:
*   USBHST_SUCCESS - If the request is completed successfully.
*
* ERRNO: N/A.
*
* \NOMANUAL
*/

USBHST_STATUS   usbMhdrcHcdResetTTRequestComplete
    (
    UINT8         uRelativeBusIndex,  /* bus index of host controller */
    void *        pContext,           /* usbd context value           */
    USBHST_STATUS nStatus             /* status of request completion */
    )
    {
    return USBHST_SUCCESS;
    }


