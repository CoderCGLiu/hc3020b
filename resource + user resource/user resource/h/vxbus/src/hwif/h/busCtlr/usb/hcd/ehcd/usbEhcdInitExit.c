/* usbEhcdInitExit.c - USB EHCI HCD initialization routine */

/*
 * Copyright (c) 2003-2011, 2013, 2015-2016 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2003-2011, 2013, 2015 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
Modification history
--------------------
05b,24jan16,hma  fix for Processing to detach USB devices is slow (VXW6-85151)
04a,09sep15,j_x  Add parameters for EHCI polling and interrupt threshold (VXW6-84172)
03z,30Jul15,hma  Fix the memrory leak when calling usbExit (VXW6-84619)
03y,05jul15,j_x  add spinlock to context of USBINTR register operation (VXW6-84654)
03x,15nov13,ljg  Change type of phyBaseAddr to be HCF_RES_INT(WIND00441048)
03w,03sep13,wyy  Add macro OS_BUSY_WAIT_US and OS_BUSY_WAIT_MS to busy wait
                 when rebooting (WIND00432482)
03v,10Jul13,wyy  Make usbd layer to uniformly handle ClearTTBuffer request
                 (WIND00424927)
03u,03may13,wyy  Remove compiler warning (WIND00356717)
03t,21apr13,wyy  Cut the relationship among busIndex of HCDData, unitNumber of
                 vxBus Device, and HC count (such as g_EHCDControllerCount or
                 g_UhcdHostControllerCount) to dynamically release or announce
                 a HCD driver (WIND00362065)
03s,31jan13,ljg  Add usb support for imx6 (WIND00366039)
03r,06jan13,ljg  Modify root hub port status pointer (WIND00364050)
03q,04jan13,s_z  Remove compiler warning (WIND00390357)
03p,28dec11,ljg  Stop controller before reset it by software (WIND00318360)
03o,13dec11,m_y  Modify according to code check result (WIND00319317)
03n,12may11,j_x  Change the power on settle down time (WIND00269013)
03m,07jan11,ghs  Clean up compile warnings (WIND00247082)
03l,14dec10,ghs  Change the usage of the reboot hook APIs to specific
                 rebootHookAdd/rebootHookDelete (WIND00240804)
03k,06dec10,ghs  Add special handling for first QH when destroy interrupt tree
                 in usbEhcdDestroyInterruptTree (WIND00240466)
03j,06dec10,ghs  Add taskDelay after write CF flag to HCD (WIND00242690)
03i,22nov10,ghs  Code Coverity CID(9694): UNUSED_VALUE (WIND00242477)
03h,14sep10,ghs  Delete reboot hook when HCD usbEhcdInit failed
03g,02sep10,ghs  Use OS_THREAD_FAILURE to check taskSpawn failure (WIND00229830)
03f,01sep10,j_x  Remove hook from reboot hook table when EHCD exit (WIND00229326)
03e,23aug10,m_y  Update prototype for EHCI/OHCI/UHCI init routine
                 (WIND00229662)
03d,23aug10,m_y  Fix usbEhcdExit fail issue (WIND00229420)
03c,15jul10,w_x  Fix usbEhcdDestroyInterruptTree() QH free issue (WIND00223657)
03b,21jun10,ghs  Fix for polling mode(WIND00218930)
03a,27may10,w_x  Handle over ownership when has companion chip (WIND00212469)
                 Correct fix for WIND00193546 (WIND00214683)
02z,18may10,w_x  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
02y,09Apr10,y_l  Fix rdb8315 reboot error (WIND00193546)
02x,09mar10,j_x  Changed for USB debug (WIND00184542)
02w,13jan10,ghs  vxWorks 6.9 LP64 adapting
02v,17sep09,y_l  Code Coverity CID(4787): Function return status check
                 (WIND00182326)
02u,11sep09,ghs  Redefine max hcd as global(WIND00152418)
02t,02sep09,w_x  Fix detecting device connection when start up (WIND00178962)
02s,15jul09,ghs  Fix for WIND00171264, remove align defines
02r,07jul09,s_z  Respin for the USB HCD re-initialization vxBus issue and
                 Correct memory address when remove interrupt tree;
02q,13feb09,w_x  Fix USB HCD re-initialization vxBus issue and change
                 vxbDrvUnlink methods to have STATUS return type (WIND00152849)
02p,09feb09,w_x  Added "intEachTD" as a config paramter to make WIND00084918
                 fix built into library and configurable
02o,04feb09,w_x  Added support for FSL quirky EHCI with various register and
                 descriptor endian format (such as MPC5121E)
02n,20oct08,w_x  Attach usbEhcdDisableHC as reboot hook (WIND00139231)
02m,10sep08,w_x  Removed tabs to meet alignment and corrected the way
                 to concatenate task name with busIndex
02l,16jul08,w_x  Added non-standard root hub TT support (WIND00127895)
02k,18jun08,h_k  removed pAccess.
02j,21feb08,x_s  Add operation after controller reset(WIND00127383)
02i,03jun08,j_x  Convert vxBus API busCfgRead/Wirte to vxbPciDevCfgRead/Write
02h,12oct07,ami  Fix for defect CQ:WIND00106601
02g,20sep07,tor  VXB_VERSION_3
02f,10sep07,ami  changes for supporting PLB-based controllers
02e,27aug07,pdg  Enabled asynchronous schedule only if there is an outstanding
                 transfer(WIND00102592)
02d,30aug07,jrp  Apigen updates
02c,23aug07,jrp  WIND00101202
02b,06aug07,jrp  Changing register access methods
02a,25jul07,jrp  WIND00099137 fixing init sequence
01z,17jul07,jrp  Adding Instantiation routine
01y,11jul07,jrp  Remove stdout announcements
01x,13jun07,tor  remove VIRT_ADDR
01w,29May07,jrp  WIND00095133 Register access method initialization
01v,29Mar07,tor  clean up unlink method
01u,13Feb07,jrp  SMP Conversion
01t,21mar07,sup  Corrected error in leagcy support in usbHcdEhciDeviceInit
01s,07oct06,ami  Changes for USB-vxBus porting
01r,11apr05,ami  Changes made in Warm Reboot Function
01q,04apr05,mta  Fix for Detach ehci issue
01p,28mar05,pdg  non-PCI changes
01o,22mar05,mta  64-bit support added (SPR #104950)
01n,02mar05,ami  SPR #106373 Fix (Max EHCI Host Controller Issue)
01m,25feb05,mta  SPR 106276
01l,18feb05,pdg  Fix for SPR #106434(From usbTool, when 'attach ohci' is
                 called after 'detach ehci', ohci does not detect the devices)
01k,03feb05,mta  Fix for hcd detach leading to a hang issue
01j,08dec04,pdg  Corrected memory leak-USBIP merge error
01i,03dec04,ami  Merged IP Changes
01h,26oct04,ami  Severity Changes for Debug Messages
01g,15oct04,ami  Apigen Changes
01f,08Sep03,nrv  Changed g_HostControllerCount to g_EHCDControllerCount
01e,23Jul03,gpd  Incorporated the changes after testing for MIPS
01d,12Jul03,gpd  Driver handle can be zero. The check has been removed.
01c,05Jul03,gpd  if no host controller is found, the function should return error.
01b,26jun03,psp  changing the code to WRS standards.
01a,25apr03,ram  written.
*/

/*
DESCRIPTION

This contains the initialization and uninitialization functions
provided by the EHCI Host Controller Driver.

INCLUDE FILES:  usb2/usbOsal.h, usb2/BusAbstractionLayer.h, usb2/usbEhcdConfig.h,
usb2/usbHst.h, usb2/usbEhcdDataStructures.h, usb2/usbEhcdInterfaces.h,
usb2/usbEhcdHal.h, usb2/usbEhcdUtil.h, usb2/usbEhcdEventHandler.h,
usb2/usbHcdInstr.h, spinLockLib.h
*/

/*
INTERNAL
 *******************************************************************************
 * Filename         : usbEhcdInitExit.c
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 *
 * Description      :  This contains the initialization and uninitialization
 *                     functions provided by the EHCI Host Controller Driver.
 *
 *
 ******************************************************************************/

#include <usb/usbOsal.h>
#include <usb/usbHst.h>
#include "usbEhcdConfig.h"
#include "usbEhcdDataStructures.h"
#include "usbEhcdInterfaces.h"
#include "usbEhcdHal.h"
#include "usbEhcdUtil.h"
#include "usbEhcdEventHandler.h"
#include "usb/usbHcdInstr.h"
#include <hwif/util/vxbParamSys.h>
#include <hwif/util/vxbDmaLib.h>
#include <hwif/vxbus/vxBus.h>
#include <hwif/vxbus/hwConf.h>
#include <hwif/vxbus/vxbPciLib.h>
#include <spinLockLib.h>
#include <string.h>
#include <vxbTimerLib.h>
#include <rebootLib.h>
#include <hookLib.h>


/* defines */

#define USB_EHCD_DMA_MEMORY_SIZE        0x40000

#define EHCI_HCCPARAMS                  0x08    /* HCCPARAMS register */
                                                /* offset */

#define EHCI_HCCPARAMS_EECP_MASK        0x0000FF00  /* extended capabilities
                                                     * field position mask value
                                                     */

#define EHCI_HCCPARAMS_EECP_MIN         0x40       /*
                                                    * extended capabilities
                                                    * minimum value
                                                    */

#define EHCI_BIOS_OWNED                 0x010000   /* the EHCI ownership is */
                                                   /* with BIOS */
                                                   /* bit 16 in USBLEGSUP set */
#define EHCI_OS_OWNED                   0x01000000 /* the EHCI ownership is */
                                                   /* with OS bit 24 in */
                                                   /* USBLEGSUP set */

#define NUM_RETRIES                     1000       /* number of retries to */
                                                   /* obtain  ownership */

#define NUM_RETRIES_USBCMD_RS           3          /* number of retries to */
                                                   /* obtain  RS of USBCMD */

#define REFLECT_OWNERSHIP_CHANGE        1     /* wait for bits in USBLEGSUP  */
                                              /* to be set/reset according   */
                                              /* to ownership change */
#define EHCI_USBLEGSUP_OS_OFFSET        3     /* offset in the USBLEGSUP
                                               * register to access the
                                               * HC OS Owned Semaphore bit
                                               */
#define USB_EHCI_BASEC          0x000C0000    /* class code */
#define USB_EHCI_SCC            0x00000300    /* sub class code */
#define USB_EHCI_PI             0x00000020    /* programming interface */

#define USB_EHCI_PCI_NAME       "vxbPciUsbEhci" /* Name to register to PCI bus*/
#define USB_EHCI_PLB_NAME       "vxbPlbUsbEhci" /* Name to register to PLB bus*/
#define USB_EHCI_HUB_NAME       "vxbUsbEhciHub" /* Name to register to Hub bus*/

/******************************  Global Variables  ****************************/

/* globals */

/* To hold the array of pointers to the HCD maintained data structures */

pUSB_EHCD_DATA *g_pEHCDData = NULL;

/* To hold the handle returned by the USBD during HC driver registration */

UINT32  g_EHCDHandle = 0;

/* Number of host controllers present in the system */

UINT32  g_EHCDControllerCount = 0;

/* Event used for synchronising the access of the free lists */

OS_EVENT_ID g_ListAccessEvent = NULL;


IMPORT VOID usbVxbRootHubAdd (VXB_DEVICE_ID pDev);    /* func to add root */
                                                               /* hub */

IMPORT STATUS usbVxbRootHubRemove (VXB_DEVICE_ID pDev);   /* function to remove */
                                                                    /* root hub */
IMPORT USBHST_STATUS usbEhcdUnSetupPipe (pUSB_EHCD_DATA, pUSB_EHCD_PIPE );

/* forward declarations */

void usbEhcdInstantiate (void);

STATUS usbEhcdInit (void);

STATUS usbEhcdExit (void);

void vxbUsbEhciRegister (void);

/* Function to be attached as reboot hook */

int usbEhcdDisableHC(int startType);

/* Function to destroy the periodic interrupt tree */

VOID usbEhcdDestroyInterruptTree(pUSB_EHCD_DATA    pHCDData);

/* Function to uninitialize the data structure */

LOCAL VOID usbEhcdDataUninitialize(pUSB_EHCD_DATA pHCDData);

/* Function to create the interrupt tree */

BOOLEAN usbEhcdCreateInterruptTree(pUSB_EHCD_DATA pHCDData);

LOCAL USB_EHCD_BUS_STATUS usbEhcdHostBusInitialize(pUSB_EHCD_DATA    *ppHCDData,
                                                   VXB_DEVICE_ID      pDev,
                                                   UINT32    uBusIndex);

LOCAL VOID usbEhcdHostBusUninitialize(pUSB_EHCD_DATA pHCDData);

LOCAL BOOL usbVxbHcdEhciDeviceProbe   (VXB_DEVICE_ID pDev);

LOCAL VOID usbVxbNullFunction (VXB_DEVICE_ID pDev);

LOCAL VOID usbHcdEhciPlbInit (VXB_DEVICE_ID pDev);  /*
                                                     * function to do the
                                                     * BSP specific
                                                     * initialization incase
                                                     * of PLB based Controller
                                                     */

LOCAL VOID usbHcdEhciDeviceInit (VXB_DEVICE_ID pDev);   /* function to
                                                         * handle legacy the
                                                         * support
                                                         */

LOCAL VOID usbHcdEhciDeviceConnect (VXB_DEVICE_ID pDev);/* function to
                                                         * intialize the
                                                         * EHCI device
                                                         */
LOCAL STATUS usbHcdEhciDeviceRemove (VXB_DEVICE_ID pDev); /* funtion to
                                                         * uninitialize the
                                                         * EHCI device
                                                         */

/* function to deregister the EHCI driver with vxBus */

LOCAL STATUS vxbUsbEhciDeregister (void);

/* locals */

/* To store the Polling thread ID */

LOCAL OS_THREAD_ID    ehciPollingTh[USB_MAX_EHCI_COUNT] ;

LOCAL PCI_DEVVEND       usbVxbPcidevVendId[1] =
    {
    {0xffff,             /* device ID */
    0xffff}              /* vendor ID */
    };

LOCAL DRIVER_METHOD     usbVxbHcdEhciHCMethods[2] =
    {
    DEVMETHOD(vxbDrvUnlink, usbHcdEhciDeviceRemove),
    { 0, NULL }
    };

LOCAL struct drvBusFuncs usbVxbHcdEhciPlbDriverFuncs =
    {
    usbHcdEhciPlbInit,                          /* init 1 */
    usbVxbNullFunction,                         /* init 2 */
    usbHcdEhciDeviceConnect                     /* device connect */
    };

LOCAL struct drvBusFuncs usbVxbHcdEhciPciDriverFuncs =
    {
    usbVxbNullFunction,                           /* init 1 */
    usbHcdEhciDeviceInit,                         /* init 2 */
    usbHcdEhciDeviceConnect                       /* device connect */
    };


/* initialization structure for EHCI Root Hub */

LOCAL struct drvBusFuncs        usbVxbRootHubDriverFuncs =
    {
    usbVxbNullFunction,         /* init 1 */
    usbVxbNullFunction,         /* init 2 */
    usbVxbRootHubAdd            /* device connect */
    };

/* method structure for EHCI Root Hub */

LOCAL struct vxbDeviceMethod    usbVxbRootHubMethods[2] =
    {
    DEVMETHOD(vxbDrvUnlink, usbVxbRootHubRemove),
    DEVMETHOD_END
    };


/* DRIVER_REGISTRATION for EHCI Root hub */

LOCAL DRIVER_REGISTRATION       usbVxbHcdEhciHub =
    {
    NULL,                       /* pNext */
    VXB_DEVID_BUSCTRL,          /* hub driver is bus
                                 * controller
                                 */
    VXB_BUSID_USB_HOST_EHCI,    /* parent bus ID */
    USB_VXB_VERSIONID,          /* version */
    USB_EHCI_HUB_NAME,          /* driver name */
    &usbVxbRootHubDriverFuncs,  /* struct drvBusFuncs * */
    &usbVxbRootHubMethods[0],   /* struct vxbDeviceMethod */
    NULL,               /* probe routine */
    NULL                /* vxbParams */
    };

/* Default PLB EHCI parameters */

LOCAL VXB_PARAMETERS usbVxbPlbHcdEhciDevParamDefaults[] =
    {
       {"hasEmbeddedTT", VXB_PARAM_INT32, {(void *)FALSE}},
       {"fixupPortNumber", VXB_PARAM_INT32, {(void *)FALSE}},
       {"postResetHook", VXB_PARAM_FUNCPTR, {(void *)NULL}},
       {"descBigEndian", VXB_PARAM_INT32, {(void *)FALSE}},
       {"regBigEndian", VXB_PARAM_INT32, {(void *)FALSE}},
       {"intEachTD", VXB_PARAM_INT32, {(void *)FALSE}},
       {"hasCompanion", VXB_PARAM_INT32, {(void *)TRUE}},
       {"platformType", VXB_PARAM_INT32, {(void *)FALSE}},
       {"phyBaseAddr", VXB_PARAM_POINTER, {(void *)FALSE}},
       {NULL, VXB_PARAM_END_OF_LIST, {NULL}}
    };

LOCAL DRIVER_REGISTRATION       usbVxbPlbHcdEhciDevRegistration =
    {
    NULL,                                   /* register next driver */

    VXB_DEVID_BUSCTRL,                      /* bus controller */
    VXB_BUSID_PLB,                          /* bus id - PLB Bus Type */
    USB_VXB_VERSIONID,                      /* vxBus version Id */
    USB_EHCI_PLB_NAME,                      /* drv name */
    &usbVxbHcdEhciPlbDriverFuncs,           /* pDrvBusFuncs */
    &usbVxbHcdEhciHCMethods[0],             /* pMethods */
    NULL,                                   /* probe routine */
    usbVxbPlbHcdEhciDevParamDefaults        /* vxbParams */
    };

/* Default PCI EHCI parameters */

LOCAL VXB_PARAMETERS usbVxbPciHcdEhciDevParamDefaults[] =
    {
       {"hasEmbeddedTT", VXB_PARAM_INT32, {(void *)FALSE}},
       {"fixupPortNumber", VXB_PARAM_INT32, {(void *)FALSE}},
       {"postResetHook", VXB_PARAM_FUNCPTR, {(void *)NULL}},
       {"descBigEndian", VXB_PARAM_INT32, {(void *)FALSE}},
       {"regBigEndian", VXB_PARAM_INT32, {(void *)FALSE}},
       {"intEachTD", VXB_PARAM_INT32, {(void *)FALSE}},
       {"hasCompanion", VXB_PARAM_INT32, {(void *)TRUE}},
       {NULL, VXB_PARAM_END_OF_LIST, {NULL}}
    };

LOCAL PCI_DRIVER_REGISTRATION   usbVxbPciHcdEhciDevRegistration =
    {
        {
        NULL,                                   /* register next driver */
        VXB_DEVID_BUSCTRL,                      /* bus controller */
        VXB_BUSID_PCI,                          /* bus id - PCI Bus Type */
        USB_VXB_VERSIONID,                      /* vxBus version Id */
        USB_EHCI_PCI_NAME,                      /* drv name */
        &usbVxbHcdEhciPciDriverFuncs,           /* pDrvBusFuncs */
        &usbVxbHcdEhciHCMethods [0],            /* pMethods */
        usbVxbHcdEhciDeviceProbe,               /* probe routine */
        usbVxbPciHcdEhciDevParamDefaults        /* vxbParams */
        },
        NELEMENTS(usbVxbPcidevVendId),          /* idListLen */
        & usbVxbPcidevVendId [0]                /* idList */
    };

/*******************************************************************************
*
* usbEhcdDestroyInterruptTree - destroys the periodic tree created
*
* This routine destroys the periodic tree created for the interrupt endpoints.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbEhcdDestroyInterruptTree
    (
    pUSB_EHCD_DATA    pHCDData
    )
    {
    /* To hold the index into the frame list */

    UINT32 uCount = 0;

    /* To hold the Queue Head pointer */

    pUSB_EHCD_QH pQH = NULL;

    /* Temporary Queue Head */

    pUSB_EHCD_QH pTempQH = NULL;

    /* Check the validity of the parameter */

    if (NULL == pHCDData)
        {
        USB_EHCD_ERR("usbEhcdDestroyInterruptTree - invalid parameter\n",
            0, 0, 0, 0, 0, 0);

        return;
        }

    /* This loop removes all the elements of the tree */

    for (uCount = 0; USB_EHCD_MAX_TREE_NODES > uCount; uCount++)
        {
        pUSB_EHCD_LIST_DATA pTreeListData = &pHCDData->TreeListData[uCount];

        /* This loop searches for the QHs in the tree and frees them */

        pQH = pTreeListData->pHeadPointer;

        if (uCount == 0)
            {
            while (pQH != NULL)
                {
                /* Retrieve the pointer to HCD specific data */

                pTempQH = pQH->pNext;

                /* Destroy the QH */

                usbEhcdDestroyQH(pHCDData, pQH);

                pQH = pTempQH;
                }

            continue;
            }

        for ( ; (pTreeListData->pHeadPointer != pTreeListData->pTailPointer) &&
                (NULL != pQH);
                pQH = pTempQH)
            {
            /* Retrieve the pointer to HCD specific data */

            pTempQH = pQH->pNext;

            /*
             * The uNextListIndex points to the tree node in a higher tier;
             * We should break out when we reach the next head QH; The next
             * head QH will be destroyed on the run to that tree node.
             */

            if (pQH &&
                (pQH != pHCDData->TreeListData
                        [pTreeListData->uNextListIndex].pHeadPointer))
                {
                USB_EHCD_DBG("Destroy QH %p on count %d\n",
                             pQH, uCount, 3, 4, 5, 6);

                /* Destroy the QH */

                usbEhcdDestroyQH(pHCDData, pQH);
                }
            else
                {
                USB_EHCD_DBG("QH %p can not be freed on count %d\n",
                             pQH, uCount, 3, 4, 5, 6);
                break;
                }
            }

        /* Check if QH is not NULL and if it is the same as the tail element */

        if ((pQH != NULL) && (pQH == pTreeListData->pTailPointer))
            {
            USB_EHCD_DBG("Destroy QH %p on count %d and it is tail\n",
                         pQH, uCount,3,4,5,6);


            /* Destroy the QH */

            usbEhcdDestroyQH(pHCDData, pQH);
            }

        }

    /* Set the array to empty */

    OS_MEMSET(pHCDData->TreeListData,
              0,
              USB_EHCD_MAX_TREE_NODES * sizeof(USB_EHCD_LIST_DATA));

    return;
    }/* End of usbEhcdDestroyInterruptTree() */

/*******************************************************************************
*
* usbEhcdCreateInterruptTree - creates the periodic tree
*
* This function creates the periodic tree created for the interrupt endpoints.
*
* RETURNS: TRUE, or FALSE if periodic tree is not created.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOLEAN usbEhcdCreateInterruptTree
    (
    pUSB_EHCD_DATA    pHCDData
    )
    {
    /* To hold the index into the nodes of the periodic tree */

    UINT32 uCount = 0;

    /* Pointer to the Queue Head data structure */

    pUSB_EHCD_QH pQH = NULL;

    /* Temporary variable */

    UINT32 uTempVariable = 0;

    /* Temporary count variable */

    UINT32 uTempCount = 0;

    UINT32 uBusIndex = pHCDData->uBusIndex;

    /*
     * Static array which helps in arriving
     * at the next indices of the leaf nodes
     */

    static UCHAR Balance[USB_EHCD_MAX_LEAF_NODES] ={
                                                0x0,
                                                0x08,
                                                0x04,
                                                0x0C,
                                                0x02,
                                                0x0A,
                                                0x06,
                                                0x0E,
                                                0x01,
                                                0x09,
                                                0x05,
                                                0x0D,
                                                0x03,
                                                0x0B,
                                                0x07,
                                                0x0F
                                              };

    /*
     * This loop will create the first 31 empty nodes of the periodic tree
     * and initialize the link pointers.
     */

    for (uCount = 0; (USB_EHCD_MAX_TREE_NODES / 2) > uCount; uCount++)
        {
        /* Create an empty QH */

        pQH = usbEhcdFormEmptyQH(pHCDData);

        /* Check if QH pointer is valid */

        if (NULL == pQH)
            {
            /* Release memory allocated */

            usbEhcdDestroyInterruptTree(pHCDData) ;

            return FALSE;
            }

        /* Update the node pointer */

        pHCDData->TreeListData[uCount].pHeadPointer = (pVOID) pQH;

        /* Update the tail pointer in the list to be the same as the node */

        pHCDData->TreeListData[uCount].pTailPointer = (pVOID) pQH;

        /* The S-mask field should be non-zero for periodic endpoints */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              QH,
                              pQH->uEndPointCapabilities,
                              1,
                              ENDPOINT_CAPABILITIES_UFRAME_S);

        /* As this is only a dummy QH, set the halted bit */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              QH,
                              pQH->uTransferInfo,
                              USB_EHCD_QTD_STATUS_HALTED,
                              TRANSFERINFO_STATUS);

        /* Initialize the bandwidth reserved fields */

        for (uTempCount = 0;
             USB_EHCD_MAX_MICROFRAME_NUMBER > uTempCount;
             uTempCount++)
            {
            pHCDData->TreeListData[uCount].uBandwidth[uTempCount] = 0;
            }

        /*
         * If it is not the root of the node, update the
         * next pointer index and the next pointer
         */

        if (0 < uCount)
            {
            /* LOW 32 bit for BUS address of the next QH */

            UINT32 pQHNextTemp;

            /* Update the next pointer in the list */

            pQH->pNext =
                (pVOID) pHCDData->TreeListData[(uCount - 1) / 2].pHeadPointer;

            /* Update the next list index */

            pHCDData->TreeListData[uCount].uNextListIndex = (uCount - 1) / 2;

            /*
             * Get the BUS address of the next QH. With the logic here,
             * we are sure the pQH->pNext has been created in the previous
             * loop, thus it will be non-NULL and we can use its DMA MAP to
             * get its BUS address.
             *
             * Note that all EHCI transfer data structures are allocated in
             * DMA32 partition in 64 bit systems, which means they are in the
             * first 4GB of the 64 bit memory space. So the PHYS address of
             * these transfer data structures can be casted to UINT32;
             *
             * However, the DMA MAP 'frag' should be the BUS address of the
             * corresponding PHYS address, which theoretically can be out of
             * the first 4GB memory (consider this case, for PHYS address
             * 0x1000,0000, a PCI bus controller may see it at 0x1,1000,0000)
             * in a 64 bit system.
             *
             * The link pointers in the transfer data structures are defined
             * as 32 bit in EHCI (address bits [31:0]), the most significant
             * address bits [63:32] has been set in CTRLDSSEGMENT register.
             * EHCI hardware will concat the CTRLDSSEGMENT register and the
             * 32 bit link pointer in the transfer data structures to locate
             * the other transfer data structures in the 64 bit BUS memory
             * space (from a BUS controller point of view, not from CPU point
             * of view).
             */

            /* Get the lower 32 bit of the BUS address */

            pQHNextTemp = USB_EHCD_DESC_LO32(pQH->pNext);

            USB_EHCD_SET_BITFIELD(uBusIndex,
                                  QH,
                                  pQH->uQueueHeadHorizontalLinkPointer,
                                  (pQHNextTemp >> 5),
                                  HORIZONTAL_LINK_POINTER );

            /* Indicate that this is not the last element in the list */

            USB_EHCD_SET_BITFIELD(uBusIndex,
                                  QH,
                                  pQH->uQueueHeadHorizontalLinkPointer,
                                  USB_EHCD_VALID_LINK,
                                  HORIZONTAL_LINK_POINTER_T);

            /* Update the type of the data structure to be a QH */

            USB_EHCD_SET_BITFIELD(uBusIndex,
                                  QH,
                                  pQH->uQueueHeadHorizontalLinkPointer,
                                  USB_EHCD_TYPE_QH,
                                  HORIZONTAL_LINK_POINTER_TYPE );

            }
        else
            {
            /* Update the next list index */

            pHCDData->TreeListData[uCount].uNextListIndex = (UINT32)USB_EHCD_NO_LIST;

            /*
             * Set the terminate bit indicating that there are no more
             * elements in the list.
             */

            USB_EHCD_SET_BITFIELD(uBusIndex,
                                QH,
                                pQH->uQueueHeadHorizontalLinkPointer,
                                USB_EHCD_INVALID_LINK,
                                HORIZONTAL_LINK_POINTER_T);

            /* Update the type of the data structure to be a QH */

            USB_EHCD_SET_BITFIELD(uBusIndex,
                                  QH,
                                  pQH->uQueueHeadHorizontalLinkPointer,
                                  USB_EHCD_TYPE_QH,
                                  HORIZONTAL_LINK_POINTER_TYPE);
            pQH->pNext = NULL;
            }
        }/* End of for () */

    /*
     * This loop will update the link pointers of the leaf nodes
     * of the interrupt tree as per the static data available
     */

    for (; USB_EHCD_MAX_TREE_NODES > uCount; uCount++, uTempCount++)
        {
        /* To hold the index into the microframe bandwidth array */

        UINT uFrameIndex = 0;

        /* LOW 32 bit for BUS address of the next QH */

        UINT32 uQHBusAddrLow32;

        /* Create an empty QH */

        pQH = usbEhcdFormEmptyQH(pHCDData);

        /* Check if QH pointer is valid */

        if (NULL == pQH)
            {
            /* Release memory allocated */

            usbEhcdDestroyInterruptTree(pHCDData) ;

            return FALSE;
            }

        /* Update the node pointer */

        pHCDData->TreeListData[uCount].pHeadPointer = (pVOID) pQH;

        /* Update the tail pointer in the list to be the same as the node */

        pHCDData->TreeListData[uCount].pTailPointer = (pVOID) pQH;

        /* The S-mask field should be non-zero for periodic endpoints */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              QH,
                              pQH->uEndPointCapabilities,
                              1,
                              ENDPOINT_CAPABILITIES_UFRAME_S);


        /* As this is only a dummy QH, set the halted bit */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              QH,
                              pQH->uTransferInfo,
                              USB_EHCD_QTD_STATUS_HALTED,
                              TRANSFERINFO_STATUS);

        /* Initialize the bandwidth reserved fields */

        for (uFrameIndex = 0; USB_EHCD_MAX_MICROFRAME_NUMBER > uFrameIndex;
             uFrameIndex++)
            {
            pHCDData->TreeListData[uCount].uBandwidth[uFrameIndex] = 0;
            }

        /* Update the index of the next node in the list */

        uTempVariable = (UINT32)(Balance[uTempCount & 0x0F] +
                        ((USB_EHCD_MAX_LEAF_NODES / 2) - 1));

        /* Update the next list index */

        pHCDData->TreeListData[uCount].uNextListIndex = uTempVariable;

        /* Update the QH's Next pointer */

        pQH->pNext = (pVOID) pHCDData->TreeListData[uTempVariable].pHeadPointer;

        /*
         * See the discussion above. We are sure pQH->pNext is non-NULL
         * and qhMapId is valid.
         */

        /* Get the lower 32 bit of the BUS address */

        uQHBusAddrLow32 = USB_EHCD_DESC_LO32(pQH->pNext);

        /* Update the link pointer */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              QH,
                              pQH->uQueueHeadHorizontalLinkPointer,
                              (uQHBusAddrLow32 >> 5),
                              HORIZONTAL_LINK_POINTER );

        /* Indicate that this is not the last element in the list */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              QH,
                              pQH->uQueueHeadHorizontalLinkPointer,
                              USB_EHCD_VALID_LINK,
                              HORIZONTAL_LINK_POINTER_T);

        /* Update the type of the data structure to be a QH */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              QH,
                              pQH->uQueueHeadHorizontalLinkPointer,
                              USB_EHCD_TYPE_QH,
                              HORIZONTAL_LINK_POINTER_TYPE );
        }

    /* Return TRUE */

    return TRUE;
    }/* End of usbEhcdCreateInterruptTree() */

/*******************************************************************************
*
* usbEhcdPollingISR - check for interrupts periodically
*
* This routine is the task entry for EHCD polling task, which periodically
* checks for interrupts.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbEhcdPollingISR(pVOID pHCDDev)
    {
    while (1)
        {
        /* Call the ISR function */

        usbEhcdISR(pHCDDev);

        taskDelay(10);
        }
    }

/*******************************************************************************
*
* usbEhcdHostBusInitialize - perform the host bus specific initialization
*
* This function performs the host bus specific initialization of the
* EHCI Host Controller.
*
* RETURNS:
*
* USB_EHCD_HC_NOT_PRESENT if the host controller is not present.
*
* USB_EHCD_HCBUS_NOT_INITIALIZED if the host bus specific initialisation
* is unsuccessful.
*
* USB_EHCD_HCBUS_INITIALIZED if the host bus specific initialisation
* is successful.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL USB_EHCD_BUS_STATUS usbEhcdHostBusInitialize
    (
    pUSB_EHCD_DATA            * ppHCDData,   /* struct _USB_EHCD_DATA */
    VXB_DEVICE_ID               pDeviceInfo, /* struct vxbDev */
    UINT32                      uBusIndex    /* the index of USB_EHCD_DATA */
    )
    {
    UINT32                      uCount = 0;
    VXB_INST_PARAM_VALUE        paramVal; /* To hold the parameter */
    pUSB_EHCD_DATA              pHCDData;
    HCF_DEVICE *                pHcf;
    UINT32                      resourceVal32;
    uintptr_t                   resourceValPtr;

    /* Check the validity of the parameter */

    if (NULL == ppHCDData || pDeviceInfo == NULL)
        {
        USB_EHCD_ERR("usbEhcdHostBusInitialize - parameters are not valid\n",
            0, 0, 0, 0, 0, 0);

        return USB_EHCD_HCBUS_NOT_INITIALIZED;
        }

    /*
     * Set the *ppHCDData to NULL, only when this routine returns
     * we set it to the memory allocated.This is to watch out for
     * intermediate failure where *ppHCDData will need to be NULL
     * before this routine returns with failure.
     */

    *ppHCDData = NULL;

    /* Allocate memory for the EHCD_DATA structure */

    pHCDData = (pUSB_EHCD_DATA) OS_MALLOC(sizeof(USB_EHCD_DATA));

    /* Check if memory allocation is successful */

    if (NULL == pHCDData)
        {
        USB_EHCD_ERR("usbEhcdHostBusInitialize - "
            "USB_EHCD_DATA allocation failed\n",
            0, 0, 0, 0, 0, 0);

        return USB_EHCD_HCBUS_NOT_INITIALIZED;
        }

    /* Initialize all the elements of the structure */

    OS_MEMSET(pHCDData, 0, sizeof(USB_EHCD_DATA));

    /* Populate pUSB_EHCD_DATA :: pDev */

    pHCDData->pDev = pDeviceInfo;

    /*
     * Clear the pDev->pDrvCtrl only just before this routine returns
     * we set it to the memory allocated. This is to watch out for
     * intermediate failure where pDev->pDrvCtrl will need to be NULL
     * before this routine returns with failure.
     */

    pHCDData->pDev->pDrvCtrl = NULL;

    pHCDData->uBusIndex = uBusIndex;

    /*
     * Map the access registers.  This is needed for subsequent
     * operations.  We look at the pDev->regBaseFlags which are set when the
     * pci bus device is announced to vxBus.  The device announce, based on
     * the BAR will set VXB_REG_IO, VXB_REG_MEM, VXB_REG_NONE or VXB_REG_SPEC.
     *
     * It is actually suspected that it will always be VXB_REG_MEM, but at this
     * time no specification has been identified that guarantees this.
     *
     * The following test identifies the BARs to use.
     */

    for (uCount = 0; uCount < VXB_MAXBARS; uCount++)
        {
        if ((pHCDData)->pDev->regBaseFlags[uCount] != VXB_REG_NONE)
            {
            if (ERROR == vxbRegMap (pHCDData->pDev, (int)uCount,
                              &pHCDData->pRegAccessHandle))
                {
                USB_EHCD_ERR("usbEhcdHostBusInitialize - "
                    "vxbRegMap failed\n",
                    0, 0, 0, 0, 0, 0);

                return USB_EHCD_HCBUS_NOT_INITIALIZED;
                }


            /* Save the register base just to keep things a little simpler */

            pHCDData->regBase = (ULONG)pHCDData->pDev->pRegBase[uCount];

            /* Record the register index */

            pHCDData->regIndex = uCount;

            break;
            }
        }

    if (uCount >= VXB_MAXBARS)
        {
        /*
         * No usable BARs found.
         */

        /* Free the memory allocated */

        OS_FREE(pHCDData);

        USB_EHCD_ERR("usbEhcdHostBusInitialize - No usable BARs found\n",
            0, 0, 0, 0, 0, 0);

        return USB_EHCD_HCBUS_NOT_INITIALIZED;
        }

    /* Populate the requried function pointers and parameters */

    /* Check if we are required to support embedded TT */

    if ( OK != vxbInstParamByNameGet(pDeviceInfo,
                           "hasEmbeddedTT",
                           VXB_PARAM_INT32,
                           &paramVal) )
        {
        /* Unmap the register space */

        vxbRegUnmap (pHCDData->pDev, pHCDData->regIndex);

        /* Free the memory allocated */

        OS_FREE(pHCDData);

        USB_EHCD_ERR(
            "usbEhcdHostBusInitialize - vxbInstParamByNameGet"
            " hasEmbeddedTT error\n", 0, 0, 0, 0, 0, 0);

        return USB_EHCD_HCBUS_NOT_INITIALIZED;
        }

    pHCDData->hasEmbeddedTT = (BOOLEAN)paramVal.int32Val;

    /*
     * Check if we are required to workaround the USB errata #14
     * on some old MPC834x targets ; The errata is described as :
     * Port number in the Queue head is 0 to N-1. It should be 1 to
     * N according to EHCI spec. This bug only affects the host mode.
     */

    if ( OK != vxbInstParamByNameGet(pDeviceInfo,
                           "fixupPortNumber",
                           VXB_PARAM_INT32,
                           &paramVal) )
        {

        /* Unmap the register space */

        vxbRegUnmap (pHCDData->pDev, pHCDData->regIndex);

        /* Free the memory allocated */

        OS_FREE(pHCDData);

        USB_EHCD_ERR(
            "usbEhcdHostBusInitialize - vxbInstParamByNameGet"
            " fixupPortNumber error\n", 0, 0, 0, 0, 0, 0);

        return USB_EHCD_HCBUS_NOT_INITIALIZED;
        }

    pHCDData->fixupPortNumber = (BOOLEAN)paramVal.int32Val;

    /*
     * Set "intEachTD" to TRUE to address WIND00084918. It was
     * observerd that sometimes the Next qTD pointer in the Transfer
     * Descriptor(qTD) of the EHCI Data Structure was not getting unpdated
     * by the host controller hardware. This resulted in a transfer error.
     * We are providing a work-around.
     *
     * Description of Workaround Provided
     * ----------------------------------
     * As the workaround, for every TD processes by the host controller,
     * an interrupt was generated, subsequent to which the software (HCD)
     * used to update the Next qTD pointer manually.
     *
     * If "intEachTD" is set to TRUE, we update the next QTD pointer
     * manully on an error. This flag is set as FALSE by default.
     */

    if ( OK != vxbInstParamByNameGet(pDeviceInfo,
                           "intEachTD",
                           VXB_PARAM_INT32,
                           &paramVal) )
        {
        /* Unmap the register space */

        vxbRegUnmap (pHCDData->pDev, pHCDData->regIndex);

        /* Free the memory allocated */

        OS_FREE(pHCDData);

        USB_EHCD_ERR(
            "usbEhcdHostBusInitialize - vxbInstParamByNameGet"
            " intEachTD error\n", 0, 0, 0, 0, 0, 0);

        return USB_EHCD_HCBUS_NOT_INITIALIZED;
        }

    pHCDData->intEachTD = (BOOLEAN)paramVal.int32Val;

    /*
     * If there is Embedded TT, then the EHCI should have no companion,
     * so there is no need to get from the parameter, just set it directly.
     *
     * This controls the port handle over situation.
     */

    if (pHCDData->hasEmbeddedTT == TRUE)
        {
        pHCDData->hasCompanion = FALSE;
        }
    else
        {
        /* Check if there are companion host controllers */

        if ( OK != vxbInstParamByNameGet(pDeviceInfo,
                               "hasCompanion",
                               VXB_PARAM_INT32,
                               &paramVal) )
            {
            /* Unmap the register space */

            vxbRegUnmap (pHCDData->pDev, pHCDData->regIndex);

            /* Free the memory allocated */

            OS_FREE(pHCDData);

            USB_EHCD_ERR(
                "usbEhcdHostBusInitialize - vxbInstParamByNameGet"
                " hasCompanion error\n", 0, 0, 0, 0, 0, 0);

            return USB_EHCD_HCBUS_NOT_INITIALIZED;
            }

        pHCDData->hasCompanion = (BOOLEAN)paramVal.int32Val;
        }

    /*
     * Check if we are required do a post reset operation;
     * This is normally required for some dual role devices
     * whose host module will need to be set to 'host mode'
     * after controller reset; Some Freescale dual role controllers
     * have this feature.
     */

    if (OK != vxbInstParamByNameGet(pDeviceInfo,
                           "postResetHook",
                           VXB_PARAM_FUNCPTR,
                           &paramVal))
        {
        /* Unmap the register space */

        vxbRegUnmap (pHCDData->pDev, pHCDData->regIndex);

        /* Free the memory allocated */

        OS_FREE(pHCDData);

        USB_EHCD_ERR(
            "usbEhcdHostBusInitialize - vxbInstParamByNameGet"
            " postResetHook error\n", 0, 0, 0, 0, 0, 0);

        return USB_EHCD_HCBUS_NOT_INITIALIZED;
        }

    pHCDData->pPostResetHook = paramVal.funcVal;

    switch (pDeviceInfo->busID)
        {
        case VXB_BUSID_PLB:

            /*
             * USB is little endian, if CPU is big endian, we need this swap!
             * This is to swap data such as the PortStatus and PortStatusChange
             * to/from CPU endian to set or clear any bit; And when reporting
             * to the USBD (by URB callback), swap into Little Endian so that
             * it conforms to USB requirement.
             */

#if (_BYTE_ORDER == _BIG_ENDIAN)
             (pHCDData)->pUsbSwap = vxbSwap32;
#endif

            /*
             * Normal EHCI data structures are little endian, if the CPU is
             * big endian and it implements these data structures to be little
             * endian, then we need this swap; But if the CPU is big endian
             * and it implements these data structures to be also big endian,
             * then we do not need this swap (which is the case for all
             * MPC5121E SOCs)! In short, if the descriptor endian format is
             * different with CPU endian format, we need this swap!
             */

            if (OK != vxbInstParamByNameGet(pDeviceInfo,
                                   "descBigEndian",
                                   VXB_PARAM_INT32,
                                   &paramVal) )
                {
                /* Unmap the register space */

                vxbRegUnmap (pHCDData->pDev, pHCDData->regIndex);

                /* Free the memory allocated */

                OS_FREE(pHCDData);

                USB_EHCD_ERR(
                    "usbEhcdHostBusInitialize - vxbInstParamByNameGet"
                    " descBigEndian error\n", 0, 0, 0, 0, 0, 0);

                return USB_EHCD_HCBUS_NOT_INITIALIZED;
                }

            USB_EHCD_DBG(
                "EHCI desc endian %s \n",
                (ULONG)((paramVal.int32Val) ?
                "_BIG_ENDIAN" : "_LITTLE_ENDIAN"), 2, 3, 4, 5, 6);

#if (_BYTE_ORDER == _BIG_ENDIAN)
            /* CPU BE, but DESC LE */

            if (paramVal.int32Val == FALSE)
                {
                pHCDData->pDescSwap = vxbSwap32;
                }
#else
            /* CPU LE, but DESC BE */

            if (paramVal.int32Val == TRUE)
                {
                pHCDData->pDescSwap = vxbSwap32;
                }
#endif

            /*
             * Normal EHCI is little endian, if the CPU is big endian and
             * it implements EHCI registers in little endian, then we need
             * this swap (as the case for MPC5121E silicon REV1.5); But if
             * the CPU is big endian, and it implements EHCI registers in
             * big endian, then we do not need this swap! In short, if the
             * register endian format is different with CPU endian format,
             * we need this swap!
             */

            if ( OK != vxbInstParamByNameGet(pDeviceInfo,
                                   "regBigEndian",
                                   VXB_PARAM_INT32,
                                   &paramVal) )
                {
                /* Unmap the register space */

                vxbRegUnmap (pHCDData->pDev, pHCDData->regIndex);

                /* Free the memory allocated */

                OS_FREE(pHCDData);

                USB_EHCD_ERR(
                    "usbEhcdHostBusInitialize - vxbInstParamByNameGet"
                    " regBigEndian error\n", 0, 0, 0, 0, 0, 0);

                return USB_EHCD_HCBUS_NOT_INITIALIZED;
                }

            USB_EHCD_DBG(
                "EHCI reg endian %s \n",
                (ULONG)((paramVal.int32Val) ?
                "_BIG_ENDIAN" : "_LITTLE_ENDIAN"), 2, 3, 4, 5, 6);

#if (_BYTE_ORDER == _BIG_ENDIAN)
            /* CPU BE, but REG LE */

            if (paramVal.int32Val == FALSE)
                {
                pHCDData->pRegSwap = vxbSwap32;
                }
#else
            /* CPU LE, but REG BE */

            if (paramVal.int32Val == TRUE)
                {
                pHCDData->pRegSwap = vxbSwap32;
                }
#endif

            /* Update the vxBus read-write handle */

            if (pHCDData->pRegSwap != NULL)
                {
                pHCDData->pRegAccessHandle = (void *)
                    VXB_HANDLE_SWAP ((ULONG)((pHCDData)->pRegAccessHandle));
                }

            pHcf = hcfDeviceGet (pDeviceInfo);

            if (NULL != pHcf)
               {
               if (ERROR != devResourceGet (pHcf, "platformType", HCF_RES_INT,
                                             (void *)(&resourceVal32)))
                   {
                   pHCDData->uPlatformType = resourceVal32;
                   }  

                if (ERROR != devResourceGet (pHcf, "phyBaseAddr", HCF_RES_INT,
                                             (void *)(&resourceValPtr)))
                   {
                   pHCDData->phyBaseAddr = (void *) resourceValPtr;
                   }  
                }

            break;

        case VXB_BUSID_PCI:

            /*
             * populate the function pointer for byte converions. Byte swapping
             * is required only if bus type is PCI and architecture is BE.
             * NOTE: for PLB no byte swapping is required
             */

#if (_BYTE_ORDER == _BIG_ENDIAN)
            /*
             * PCI is little endian, and most USB PCI controller cards use
             * little endian format for these EHCI data structures, so on
             * big endian systems we need this swap! As of this writing, we
             * have not seen any USB PCI card that use big endian data
             * strcutures, so we right now only consider the normal case.
             * If someday we really see such a USB PCI card that use big endian
             * data structures, we may need to use the same mechanism as for
             * the PLB controllers(see above)!
             */

             pHCDData->pDescSwap = vxbSwap32;
             pHCDData->pUsbSwap = vxbSwap32;
#endif
            break;

        default:
            /* Unmap the register space */

            vxbRegUnmap (pHCDData->pDev, pHCDData->regIndex);

            /* Free the memory allocated */

            OS_FREE(pHCDData);

            return USB_EHCD_HCBUS_NOT_INITIALIZED;
        }

    /*
     * Move it here to let the VXB_HANDLE_SWAP code
     * take place when register access needs swap!
     *
     * Note that USB_EHCD_READ_CAPLENGTH has been changed to
     * use vxbRead32 to cope with register endian issue.
     */

    pHCDData->capLenOffset =  USB_EHCD_READ_CAPLENGTH(pHCDData);

    pHCDData->capLenOffset &= 0x000000FF;

    /* Read the HCCPARAMS register to identify the size of data structure */

    pHCDData->addressMode64 = (UINT8)USB_EHCD_GET_FIELD(pHCDData,
                                                 HCCPARAMS,
                                                 ADDRCAP);

    if (pHCDData->addressMode64)
        pHCDData->descAlignment = USB_EHCD_64BIT_HC_DATA_STRUCTURE_ALIGNMENT;
    else
        pHCDData->descAlignment = USB_EHCD_HC_DATA_STRUCTURE_ALIGNMENT;

    /*
     * If the HC can address 64 bit memory space,
     * then we do not have any addressing limit for user buffers,
     * thus set it to VXB_SPACE_MAXADDR.
     *
     * The user buffer highAddr will be all VXB_SPACE_MAXADDR.
     *
     * Aslo the user data DMA MAP will be created with
     * VXB_DMABUF_MAPCREATE_NOBOUNCE_BUF
     */

    if (pHCDData->addressMode64)
        pHCDData->usrDmaTagLowAddr = VXB_SPACE_MAXADDR;
    else
        pHCDData->usrDmaTagLowAddr = VXB_SPACE_MAXADDR_32BIT;

    if (pHCDData->addressMode64)
        pHCDData->usrDmaMapFlags = VXB_DMABUF_CACHEALIGNCHECK;/* |
            VXB_DMABUF_MAPCREATE_NOBOUNCE_BUF;*/
    else
        pHCDData->usrDmaMapFlags = VXB_DMABUF_CACHEALIGNCHECK;

    /*
     * Initialize the global array with the element. This should be done here
     * because the register access macros and other modules need to refer to
     * the g_pEHCDData with the bus index.
     */

    g_pEHCDData[pHCDData->uBusIndex] = pHCDData;


    /* Clear the Run/Stop bit to stop the Host Controller */

    USB_EHCD_CLR_BIT(pHCDData,
                 USBCMD,
                 RS);

    /* Reset the Host Controller */

    USB_EHCD_SET_BIT(pHCDData,
                 USBCMD,
                 HCRESET);


    /* This loop waits for sometime for the reset to be complete */

    for (uCount = 0;
         uCount < USB_EHCD_MAX_DELAY_INTERVAL;
         uCount++)
        {
        /* Check if the reset is successful */

        if (0 == USB_EHCD_GET_FIELD(pHCDData,
                               USBCMD,
                               HCRESET))
            {
            USB_EHCD_VDBG("Host Controller reset successful\n",
                0, 0, 0, 0, 0, 0);

            break;
            }

        /* Wait for sometime */

        OS_DELAY_MS(1);

        }/* End of for () */

    /*
     * Check if the delay has expired
     * This means that the reset is not successful.
     */

    if (USB_EHCD_MAX_DELAY_INTERVAL == uCount)
        {
        USB_EHCD_ERR("usbEhcdHostBusInitialize - Error during HC reset\n",
            0, 0, 0, 0, 0, 0);

        /* Reset the global pointer to NULL */

        g_pEHCDData[pHCDData->uBusIndex] = NULL;

        /* Unmap the register space */

        vxbRegUnmap (pHCDData->pDev, pHCDData->regIndex);

        /* Free the memory allocated */

        OS_FREE(pHCDData);

        return USB_EHCD_HCBUS_NOT_INITIALIZED;
        }

    /* If anything needs to be done after the reset, do it now */

    if (pHCDData->pPostResetHook != NULL)
        {
        USB_EHCD_DBG("Calling Host Controller PostResetHook\n",
            0, 0, 0, 0, 0, 0);

        pHCDData->pPostResetHook();
        }

    /*
     * Set the pDev->pDrvCtrl to the memory allocated. This should
     * be done here because the ISR or the polling task will need
     * to refer to pDev->pDrvCtrl.
     */

    pHCDData->pDev->pDrvCtrl = (pVOID) pHCDData;

    if (FALSE == usrUsbEhciPollingEnabled())
        {

        /* Register the interrupt handler for the IRQ */

		#if 0
        if ((vxbIntConnect (pDeviceInfo,
                            0,
                            usbEhcdISR,
                            (VOID *)(pHCDData->pDev))) == ERROR)
            {
            USB_EHCD_ERR("usbEhcdHostBusInitialize - Error hooking the ISR\n",
                0, 0, 0, 0, 0, 0);

            /* Reset the global pointer to NULL */

            g_pEHCDData[pHCDData->uBusIndex] = NULL;

            /* Clear the pDev->pDrvCtrl so that VxBus is consistent */

            pHCDData->pDev->pDrvCtrl = NULL;

            /* Unmap the register space */

            vxbRegUnmap (pHCDData->pDev, pHCDData->regIndex);

            /* Free the memory allocated */

            OS_FREE(pHCDData);

            return USB_EHCD_HCBUS_NOT_INITIALIZED;
            }
		#endif
		int irq_val =0;
		vxbPciBusCfgRead (pHCDData->pDev, 0x3c, 1, &irq_val);
		printk("@@@@@@@@@@irq val %d\n",irq_val);
//		if(uBusIndex == 0)
//			irq_val = 48;
//		else if(uBusIndex == 1)
//			irq_val = 50;
		    int_install_handler("ehci",irq_val, 0,usbEhcdISR,   (VOID *)(pHCDData->pDev));
		    int_enable_pic(irq_val);

        }
    else
        {

        ehciPollingTh[uBusIndex] = OS_CREATE_THREAD("PollingISR",
                              37,
                              usbEhcdPollingISR,
                              (long)(pHCDData->pDev));

        if (ehciPollingTh[uBusIndex] == OS_THREAD_FAILURE)
            {
            USB_EHCD_ERR(
                "usbEhcdHostBusInitialize - Error creating the polling task\n",
                0, 0, 0, 0, 0, 0);

            /* Reset the global pointer to NULL */

            g_pEHCDData[pHCDData->uBusIndex] = NULL;

            /* Clear the pDev->pDrvCtrl so that VxBus is consistent */

            pHCDData->pDev->pDrvCtrl = NULL;

            /* Unmap the register space */

            vxbRegUnmap (pHCDData->pDev, pHCDData->regIndex);

            /* Free the memory allocated */

            OS_FREE(pHCDData);
            }

        } /* End of USB_EHCD_ENABLE_POLLING */

    /* Set the *ppHCDData to the memory allocated */

    *ppHCDData = pHCDData;

    /*
     * Return the status that the host controller
     * bus is initialized successfully.
     */

    return USB_EHCD_HCBUS_INITIALIZED;
    }/* End of function usbEhcdHostBusInitialize() */

/*******************************************************************************
*
* usbEhcdHostBusUninitialize - perform the host bus specific uninitialization
*
* This routine perform the host bus specific uninitialization.
*
* RETURNS: N/A.
*
* ERRNO:N/A
*
* \NOMANUAL
*/

LOCAL VOID usbEhcdHostBusUninitialize
    (
    pUSB_EHCD_DATA pHCDData
    )
    {
    /* Check the validity of the parameter */

    if (NULL == pHCDData)
        {
        USB_EHCD_ERR(
            "usbEhcdHostBusUninitialize - invalid parameter\n",
            0, 0, 0, 0, 0, 0);

        return;
        }

    if (FALSE == usrUsbEhciPollingEnabled())
        {
        /* Un-register the interrupt handler for the IRQ */

        /* Disable the interrupts */

        if (vxbIntDisable (pHCDData->pDev,
                           0,
                           usbEhcdISR,
                           (VOID *)pHCDData->pDev)== ERROR)
            {
            USB_EHCD_ERR("Failure in deregistering the bus\n",
                0, 0, 0, 0, 0, 0);

            /* WindView Instrumentation */

            USB_HCD_LOG_EVENT (USB_EHCI_WV_INIT_EXIT,
                               "usbEhcdExit() exits - bus deregistration failed",
                                USB_EHCD_WV_FILTER);

            return;
            }

        vxbIntDisconnect (pHCDData->pDev, 0, usbEhcdISR, (VOID *)(pHCDData->pDev));
        
        }
    else
        {
        /* Destroy the polling thread */

        if (ehciPollingTh[pHCDData->uBusIndex] != OS_THREAD_FAILURE
            && ehciPollingTh[pHCDData->uBusIndex] != 0)
            {
            OS_DESTROY_THREAD (ehciPollingTh[pHCDData->uBusIndex]);

            ehciPollingTh[pHCDData->uBusIndex] = OS_THREAD_FAILURE;
            }
        }

    vxbRegUnmap (pHCDData->pDev, pHCDData->regIndex);

    /* Free the memory allocated */

    OS_FREE(pHCDData);

    return;
    }/* End of function usbEhcdHostBusUninitialize() */

/*******************************************************************************
*
* usbEhcdDataInitialize - initialize EHCD_DATA structure
*
* This routine initializes the EHCD_DATA structure.
*
* RETURNS: TURE, or FALSE if EHCD_DATA initialization is unsuccessful.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL BOOLEAN usbEhcdDataInitialize
    (
    pUSB_EHCD_DATA pHCDData
    )
    {
    /* To hold the base of the frame list */

    pUSB_EHCD_PERIODIC_FRAME_ELEMENT pFrameList = NULL;

    /* To hold the status of a function call */

    BOOLEAN bIsSuccess = FALSE;

    /* To hold the index into the periodic frame list */

    UINT32 uCount = 0;

    /* Temporary variable */

    UINT32 uTempVariable = 0;

    /* Index of the host controller */

    UINT32          uBusIndex = 0;

    /* Short cut for VXB_DEVICE_ID */

    VXB_DEVICE_ID   pDev;

    /* Status of the vxbDmaBufLib call */

    STATUS          sts;

    /* Our DMA MAP ID */

    VXB_DMA_MAP_ID  dmaMapId;

    /* LOW 32 bit of the BUS address */

    UINT32          uBusAddrLow32;

    /* LOW 32 bit of the BUS address for the default pipe */

    UINT32          uAsyncListAddrLow32;

    /* Interrupt interval */

    UINT32          uInterrupInterval;

    /* Check the validity of the parameter */

    if (NULL == pHCDData)
        {
        USB_EHCD_ERR(
            "usbEhcdDataInitialize - Parameter is not valid\n",
            0, 0, 0, 0, 0, 0);

        return FALSE;
        }

    /* Extract the index of the host controller */

    uBusIndex = pHCDData->uBusIndex;

    /* Get short cut to our VXB_DEVICE_ID */

    pDev = pHCDData->pDev;

    /* Read the number of ports supported by the EHCI Host Controller */

    pHCDData->RHData.uNumDownstreamPorts = (UINT8)USB_EHCD_GET_FIELD(pHCDData,
                                                          HCSPARAMS,
                                                          N_PORTS);

    /* Check if the number of ports is valid */

    if (0 == pHCDData->RHData.uNumDownstreamPorts)
        {
        USB_EHCD_ERR(
            "usbEhcdDataInitialize - uNumDownstreamPorts is 0, reject!\n",
            0, 0, 0, 0, 0, 0);

        return FALSE;
        }

    /* Get the size of the port status data buffer */

    uTempVariable = (UINT32)(pHCDData->RHData.uNumDownstreamPorts
                                 * USB_EHCD_RH_PORT_STATUS_SIZE);

    /* Allocate memory for the Root hub port status */

    pHCDData->RHData.pPortStatus = (UCHAR *) OS_MALLOC(uTempVariable);

    /* Check if memory allocation is successful */

    if (NULL == pHCDData->RHData.pPortStatus)
        {
        USB_EHCD_ERR(
            "usbEhcdDataInitialize - memory not allocated for port status\n",
            0, 0, 0, 0, 0, 0);

        usbEhcdDataUninitialize(pHCDData);

        return FALSE;
        }

    /* Initialize the fields of the port status register */

    OS_MEMSET(pHCDData->RHData.pPortStatus, 0, uTempVariable);

    /*
     * The interrupt transfer data is of the following format
     * D0 - Holds the status change information of the hub
     * D1 - Holds the status change information of the Port 1
     *
     * Dn - holds the status change information of the Port n
     * So if the number of downstream ports is N, the size of interrupt
     * transfer data would be N + 1 bits.
     */

    uTempVariable = (UINT32)((pHCDData->RHData.uNumDownstreamPorts + 1)/ 8);
    if (0 != ((pHCDData->RHData.uNumDownstreamPorts + 1) % 8))
        {
        uTempVariable = uTempVariable + 1;
        }

    /* Allocate memory for the interrupt transfer data */

    pHCDData->RHData.pHubInterruptData = OS_MALLOC(uTempVariable);

    /* Check if memory allocation is successful */

    if (NULL == pHCDData->RHData.pHubInterruptData)
        {
        USB_EHCD_ERR(
            "usbEhcdDataInitialize - "
            "memory not allocated for interrupt transfer data \n",
            0, 0, 0, 0, 0, 0);

        usbEhcdDataUninitialize(pHCDData);

        return FALSE;
        }

    /* Initialize the interrupt data */

    OS_MEMSET(pHCDData->RHData.pHubInterruptData, 0, uTempVariable);

    /* Copy the size of the interrupt data */

    pHCDData->RHData.uSizeInterruptData = uTempVariable;

    /* Create the mutex used for protecting asynchronous queue access */

    pHCDData->AsychQueueMutex = semMCreate(SEM_Q_PRIORITY | SEM_DELETE_SAFE |
                                           SEM_INVERSION_SAFE );

    if (pHCDData->AsychQueueMutex == NULL)
        {
        USB_EHCD_ERR(
            "usbEhcdDataInitialize - asynch mutex not created\n",
            0, 0, 0, 0, 0, 0);

        usbEhcdDataUninitialize(pHCDData);

        return FALSE;
        }

    /* Create the event which is used for signalling the thread on interrupt */

    pHCDData->InterruptEvent = OS_CREATE_EVENT(OS_EVENT_NON_SIGNALED);

    /* Check if the event is created successfully */

    if (NULL == pHCDData->InterruptEvent)
        {
        USB_EHCD_ERR(
            "usbEhcdDataInitialize - signal event not created\n",
            0, 0, 0, 0, 0, 0);

        usbEhcdDataUninitialize(pHCDData);

        return FALSE;
        }

    /* Create the event used for synchronisation of the requests */

    pHCDData->RequestSynchEventID = OS_CREATE_EVENT(OS_EVENT_SIGNALED);

    /* Check if the event is created successfully */

    if (NULL == pHCDData->RequestSynchEventID)
        {
        USB_EHCD_ERR(
            "usbEhcdDataInitialize - request synch event not created\n",
            0, 0, 0, 0, 0, 0);

        usbEhcdDataUninitialize(pHCDData);

        return FALSE;
        }

    /* Create the event used for synchronisation of the reclamation list */

    pHCDData->ReclamationListSynchEventID = OS_CREATE_EVENT(OS_EVENT_SIGNALED);

    /* Check if the event is created successfully */

    if (NULL == pHCDData->ReclamationListSynchEventID)
        {
        USB_EHCD_ERR(
            "usbEhcdDataInitialize - reclamation synch event is not created\n",
            0, 0, 0, 0, 0, 0);

        usbEhcdDataUninitialize(pHCDData);

        return FALSE;
        }

    /* Create the event used for synchronisation of the active pipe list */

    pHCDData->ActivePipeListSynchEventID = OS_CREATE_EVENT(OS_EVENT_SIGNALED);

    /* Check if the event is created successfully */

    if (NULL == pHCDData->ActivePipeListSynchEventID)
        {
        USB_EHCD_ERR(
            "usbEhcdDataInitialize - active pipe synch event is not created\n",
            0, 0, 0, 0, 0, 0);

        usbEhcdDataUninitialize(pHCDData);

        return FALSE;
        }

    /* Create the event for bandwidth reservation */

    pHCDData->BandwidthEventID = OS_CREATE_EVENT(OS_EVENT_SIGNALED);

    /* Check if the event is created successfully */

    if (NULL == pHCDData->BandwidthEventID)
        {
        USB_EHCD_ERR(
            "usbEhcdDataInitialize - bandwidth event is not created\n",
            0, 0, 0, 0, 0, 0);

        usbEhcdDataUninitialize(pHCDData);

        return FALSE;
        }

    USB_EHCD_DBG(
        "USB_EHCD_MAX_QH_SIZE %d\n"
        "USB_EHCD_MAX_QTD_SIZE %d\n"
        "USB_EHCD_MAX_ITD_SIZE %d\n"
        "USB_EHCD_MAX_SITD_SIZE %d\n",
        USB_EHCD_MAX_QH_SIZE,
        USB_EHCD_MAX_QTD_SIZE,
        USB_EHCD_MAX_ITD_SIZE,
        USB_EHCD_MAX_SITD_SIZE, 0, 0);

    /* Get a reference to our parent tag */

    pHCDData->ehciParentTag = vxbDmaBufTagParentGet (pDev, 0);

    /* Create tag for mapping QH */

    pHCDData->qhDmaTagId = vxbDmaBufTagCreate (pDev,
        pHCDData->ehciParentTag,        /* parent */
        pHCDData->descAlignment,        /* alignment */
        USB_EHCD_DESC_BOUNDARY_LIMIT,   /* boundary */
        VXB_SPACE_MAXADDR_32BIT,        /* lowaddr */
        VXB_SPACE_MAXADDR,              /* highaddr */
        NULL,                           /* filter */
        NULL,                           /* filterarg */
        USB_EHCD_MAX_QH_SIZE,           /* max size */
        1,                              /* nSegments */
        USB_EHCD_MAX_QH_SIZE,           /* max seg size */
        VXB_DMABUF_NOCACHE,             /* flags */
        NULL,                           /* lockfunc */
        NULL,                           /* lockarg */
        NULL);                          /* ppDmaTag */

    if (pHCDData->qhDmaTagId == NULL)
        {
        USB_EHCD_ERR(
            "usbEhcdDataInitialize - creating qhDmaTagId fail\n",
            0, 0, 0, 0, 0, 0);

        usbEhcdDataUninitialize(pHCDData);

        return FALSE;
        }

    /* Create tag for mapping qTD */

    pHCDData->qtdDmaTagId = vxbDmaBufTagCreate (pDev,
        pHCDData->ehciParentTag,        /* parent */
        pHCDData->descAlignment,        /* alignment */
        USB_EHCD_DESC_BOUNDARY_LIMIT,   /* boundary */
        VXB_SPACE_MAXADDR_32BIT,        /* lowaddr */
        VXB_SPACE_MAXADDR,              /* highaddr */
        NULL,                           /* filter */
        NULL,                           /* filterarg */
        USB_EHCD_MAX_QTD_SIZE,          /* max size */
        1,                              /* nSegments */
        USB_EHCD_MAX_QTD_SIZE,          /* max seg size */
        VXB_DMABUF_NOCACHE,             /* flags */
        NULL,                           /* lockfunc */
        NULL,                           /* lockarg */
        NULL);                          /* ppDmaTag */

    if (pHCDData->qtdDmaTagId == NULL)
        {
        USB_EHCD_ERR(
            "usbEhcdDataInitialize - creating qtdDmaTagId fail\n",
            0, 0, 0, 0, 0, 0);

        usbEhcdDataUninitialize(pHCDData);

        return FALSE;
        }

    /* Create tag for mapping iTD */

    pHCDData->itdDmaTagId = vxbDmaBufTagCreate (pDev,
        pHCDData->ehciParentTag,        /* parent */
        pHCDData->descAlignment,        /* alignment */
        USB_EHCD_DESC_BOUNDARY_LIMIT,   /* boundary */
        VXB_SPACE_MAXADDR_32BIT,        /* lowaddr */
        VXB_SPACE_MAXADDR,              /* highaddr */
        NULL,                           /* filter */
        NULL,                           /* filterarg */
        USB_EHCD_MAX_ITD_SIZE,          /* max size */
        1,                              /* nSegments */
        USB_EHCD_MAX_ITD_SIZE,          /* max seg size */
        VXB_DMABUF_NOCACHE,             /* flags */
        NULL,                           /* lockfunc */
        NULL,                           /* lockarg */
        NULL);                          /* ppDmaTag */

    if (pHCDData->itdDmaTagId == NULL)
        {
        USB_EHCD_ERR(
            "usbEhcdDataInitialize - creating itdDmaTagId fail\n",
            0, 0, 0, 0, 0, 0);

        usbEhcdDataUninitialize(pHCDData);

        return FALSE;
        }

    /* Create tag for mapping siTD */

    pHCDData->sitdDmaTagId = vxbDmaBufTagCreate (pDev,
        pHCDData->ehciParentTag,        /* parent */
        pHCDData->descAlignment,        /* alignment */
        USB_EHCD_DESC_BOUNDARY_LIMIT,   /* boundary */
        VXB_SPACE_MAXADDR_32BIT,        /* lowaddr */
        VXB_SPACE_MAXADDR,              /* highaddr */
        NULL,                           /* filter */
        NULL,                           /* filterarg */
        USB_EHCD_MAX_SITD_SIZE,         /* max size */
        1,                              /* nSegments */
        USB_EHCD_MAX_SITD_SIZE,         /* max seg size */
        VXB_DMABUF_NOCACHE,             /* flags */
        NULL,                           /* lockfunc */
        NULL,                           /* lockarg */
        NULL);                          /* ppDmaTag */

    if (pHCDData->sitdDmaTagId == NULL)
        {
        USB_EHCD_ERR(
            "usbEhcdDataInitialize - creating sitdDmaTagId fail\n",
            0, 0, 0, 0, 0, 0);

        usbEhcdDataUninitialize(pHCDData);

        return FALSE;
        }

    /* Create tag for mapping periodic frame list */

    pHCDData->frameListDmaTagId = vxbDmaBufTagCreate (pDev,
        pHCDData->ehciParentTag,        /* parent */
        USB_EHCD_FRAMELIST_ALIGNMENT,   /* alignment */
        USB_EHCD_DESC_BOUNDARY_LIMIT,   /* boundary */
        VXB_SPACE_MAXADDR_32BIT,        /* lowaddr */
        VXB_SPACE_MAXADDR,              /* highaddr */
        NULL,                           /* filter */
        NULL,                           /* filterarg */
        USB_EHCD_MAX_FRAMELIST_SIZE,    /* max size */
        1,                              /* nSegments */
        USB_EHCD_MAX_FRAMELIST_SIZE,    /* max seg size */
        VXB_DMABUF_NOCACHE,             /* flags */
        NULL,                           /* lockfunc */
        NULL,                           /* lockarg */
        NULL);                          /* ppDmaTag */

    if (pHCDData->frameListDmaTagId == NULL)
        {
        USB_EHCD_ERR(
            "usbEhcdDataInitialize - creating frameListDmaTagId fail\n",
            0, 0, 0, 0, 0, 0);

        usbEhcdDataUninitialize(pHCDData);

        return FALSE;
        }

    /* Create the default pipe */

    pHCDData->pDefaultPipe = (pUSB_EHCD_PIPE)OS_MALLOC(sizeof(USB_EHCD_PIPE));

    /* Check if default pipe is created successfully */

    if (NULL == pHCDData->pDefaultPipe)
        {
        USB_EHCD_ERR(
            "usbEhcdDataInitialize - "
            "memory not allocated for the default pipe \n",
            0, 0, 0, 0, 0, 0);

        usbEhcdDataUninitialize(pHCDData);

        return FALSE;
        }

    /* Initialize the USB_EHCD_PIPE data structure */

    USB_EHCD_PIPE_INITIALIZE(pHCDData->pDefaultPipe);

    /* Create the event used for synchronisation of requests for this pipe */

    pHCDData->pDefaultPipe->PipeSynchEventID = OS_CREATE_EVENT(OS_EVENT_SIGNALED);

    /* Check if the event is created successfully */

    if (NULL == pHCDData->pDefaultPipe->PipeSynchEventID)
        {
        USB_EHCD_ERR(
            "usbEhcdDataInitialize - pDefaultPipe->PipeSynchEventID fail\n",
            0, 0, 0, 0, 0, 0);

        usbEhcdDataUninitialize(pHCDData);

        return FALSE;
        }

    /* Do some default setup */

    if (usbEhcdSetupControlPipe(pHCDData, pHCDData->pDefaultPipe)
        != USBHST_SUCCESS)
        {
        USB_EHCD_ERR(
            "usbEhcdDataInitialize - usbEhcdSetupControlPipe fail\n",
            0, 0, 0, 0, 0, 0);

        usbEhcdDataUninitialize(pHCDData);

        return FALSE;
        }

    /* Allocate and setup a QH for the default endpoint (START)  */

    /* Get an aligned QH */

    pHCDData->pDefaultPipe->pQH = usbEhcdFormEmptyQH(pHCDData);

    /* Check if a valid QH is retrieved */

    if (NULL == pHCDData->pDefaultPipe->pQH)
        {
        USB_EHCD_ERR("usbEhcdDataInitialize - invalid QH \n",
            0, 0, 0, 0, 0, 0);

        usbEhcdDataUninitialize(pHCDData);

        return FALSE;
        }

    /* Store this pointer as the tail of the asynchronous list */

    pHCDData->pAsynchTailPipe = pHCDData->pDefaultPipe;

    /* As it is a circular list, the next pointer also points to itself */

    pHCDData->pDefaultPipe->pNext = pHCDData->pDefaultPipe;

    /* Store the USB_EHCD_PIPE pointer in the QH */

    pHCDData->pDefaultPipe->pQH->pHCDPipe = pHCDData->pDefaultPipe;

    /* Get the lower 32 bit of the BUS address */

    uAsyncListAddrLow32 = USB_EHCD_DESC_LO32(pHCDData->pDefaultPipe->pQH);

    /* Update the next link pointer to point to itself */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                    QH,
                    pHCDData->pDefaultPipe->pQH->uQueueHeadHorizontalLinkPointer,
                    (uAsyncListAddrLow32 >> 5),
                    HORIZONTAL_LINK_POINTER);

    /* Make the next QH as a valid element */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                   QH,
                   pHCDData->pDefaultPipe->pQH->uQueueHeadHorizontalLinkPointer,
                   USB_EHCD_VALID_LINK,
                   HORIZONTAL_LINK_POINTER_T);

    /* Address of the default endpoint is 0 */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                    QH,
                    pHCDData->pDefaultPipe->pQH->uEndPointCharacteristics,
                    0,
                    ENDPOINT_CHARACTERISTICS_DEVICE_ADDRESS);

    /* Endpoint number */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                    QH,pHCDData->pDefaultPipe->pQH->uEndPointCharacteristics,
                    0,
                    ENDPOINT_CHARACTERISTICS_ENDPT_NUMBER);


    /* Indicate the speed as high speed */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                    QH,
                    pHCDData->pDefaultPipe->pQH->uEndPointCharacteristics,
                    USBHST_HIGH_SPEED,
                    ENDPOINT_CHARACTERISTICS_ENDPT_SPEED );

    /* Retrieve the data toggle bit from the QTD */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          QH,
                          pHCDData->pDefaultPipe->pQH->uEndPointCharacteristics,
                          USB_EHCD_DTC_RETRIEVE_FROM_QTD,
                          ENDPOINT_CHARACTERISTICS_DTC);

    /* Set this QH as the head of the reclamation list */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          QH,
                          pHCDData->pDefaultPipe->pQH->uEndPointCharacteristics,
                          1,
                          ENDPOINT_CHARACTERISTICS_HEAD_RECLAMATION_LIST);

    /* Initialize the maximum packet size of the default pipe */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          QH,
                          pHCDData->pDefaultPipe->pQH->uEndPointCharacteristics,
                          USB_EHCD_DEFAULT_MAX_PACKET_SIZE,
                          ENDPOINT_CHARACTERISTICS_MAXIMUM_PACKET_LENGTH );

    /* Update the NAK count Reload field */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                         QH,
                         pHCDData->pDefaultPipe->pQH->uEndPointCharacteristics,
                         USB_EHCD_MAX_NAK_RATE,
                         ENDPOINT_CHARACTERISTICS_RL);

    /* Allocate and setup a QH for the default endpoint (END)  */

    /* Create map for periodic frame list */

    pFrameList = pHCDData->pFrameList = vxbDmaBufMemAlloc (pDev,
        pHCDData->frameListDmaTagId,
        NULL,
        0,
        &pHCDData->frameListDmaMapId);

    if (pFrameList == NULL)
        {
        USB_EHCD_ERR(
            "usbEhcdDataInitialize - creating pFrameList fail\n",
            0, 0, 0, 0, 0, 0);

        usbEhcdDataUninitialize(pHCDData);

        return FALSE;
        }

    /* Zero the memory allocated */

    bzero ((char *)pHCDData->pFrameList, USB_EHCD_MAX_FRAMELIST_SIZE);

    /*
     * Load the DMA MAP so that we get the correct BUS address
     * to program the hardware
     */

    sts = vxbDmaBufMapLoad (pDev,
        pHCDData->frameListDmaTagId,
        pHCDData->frameListDmaMapId,
        pHCDData->pFrameList,
        USB_EHCD_MAX_FRAMELIST_SIZE,
        0);

    if (sts != OK)
        {
        USB_EHCD_ERR(
            "usbEhcdDataInitialize - loading map frameListDmaMapId fail\n",
            0, 0, 0, 0, 0, 0);

        usbEhcdDataUninitialize(pHCDData);

        return FALSE;
        }

    /* Create the static interrupt tree */

    bIsSuccess = usbEhcdCreateInterruptTree(pHCDData);

    /* Check if the interrupt tree creation is successful */

    if (TRUE != bIsSuccess)
        {
        usbEhcdDataUninitialize(pHCDData);

        return FALSE;
        }

    /* Establish the links between the frame list and the interrupt tree */

    for (uCount = 0; USB_EHCD_MAX_FRAMELIST_ENTIRES > uCount ; uCount++)
        {

        /* Pointer to the Queue Head data structure */

        pUSB_EHCD_QH pQH;

        /* Temporary index variable */

        UINT32 uTempIndex;

        /*
         * Store the index of the next element in the list
         * in a temporary variable.
         */

        uTempIndex = (uCount % USB_EHCD_MAX_LEAF_NODES) +
                        USB_EHCD_FIRST_LEAF_NODE_INDEX;

        /* Update the index of the next list */

        pHCDData->FrameListData[uCount].uNextListIndex = uTempIndex;

        /*
         * Cast to QH pointer which is created and saved in
         * usbEhcdCreateInterruptTree above
         */

        pQH = (pUSB_EHCD_QH)pHCDData->TreeListData[uTempIndex].pHeadPointer;

        /* Get the lower 32 bit of the BUS address */

        uBusAddrLow32 = USB_EHCD_DESC_LO32(pQH);

        /* Update the link pointer */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              FRAME_LIST,
                              (*(pFrameList + uCount)),
                              (uBusAddrLow32 >> 5),
                              POINTER);

        /* Indicate that the link pointer points to a QH */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              FRAME_LIST,
                              (*(pFrameList + uCount)),
                              USB_EHCD_TYPE_QH,
                              POINTER_TYPE);


        USB_EHCD_SET_BITFIELD(uBusIndex,
                              FRAME_LIST,
                              (*(pFrameList + uCount)),
                              USB_EHCD_VALID_LINK,
                              POINTER_VALID_ENTRY);

        /*
         * Update the head and tail pointers to NULL
         * These are initialized to NULL as there is no node
         * element which is created statically. These will be populated when
         * isochronous or split isochronous TDs are added.
         */

        pHCDData->FrameListData[uCount].pHeadPointer = NULL;
        pHCDData->FrameListData[uCount].pTailPointer = NULL;

        /* Update the bandwidth occupied field */

        for (uTempIndex = 0; USB_EHCD_MAX_MICROFRAME_NUMBER > uTempIndex;
             uTempIndex++)
            {
            pHCDData->FrameListData[uCount].uBandwidth[uTempIndex] = 0;
            }
        }/* End of for (uCount = 0;...) */

    /* Get the DMA MAP ID */

    dmaMapId = pHCDData->frameListDmaMapId;

    /* Get the lower 32 bit of the BUS address */

    uBusAddrLow32 = USB_EHCD_BUS_ADDR_LO32(dmaMapId->fragList[0].frag);

    /* Write the base address of the frame list in the periodicListbase */

    USB_EHCD_SET_FIELD(pHCDData,
                   PERIODICLISTBASE,
                   BASE_ADDRESS,
                   (uBusAddrLow32 >> USB_EHCD_PERIODICLISTBASE_BASE_ADDRESS));

    /*
     * Update the QH in the ASYNCLISTADDR
     * add the default pipe to the asynchronous schedule
     *
     * Note: This is moved before setting the RS and CF bits so that when
     * the controller is actually running it doesn't see invalid address.
     * (although when the RS/CF bits are set the ASYNC schedule is not
     * enabled yet thus we haven't see any failure before.)
     */

    USB_EHCD_SET_FIELD(pHCDData,
                ASYNCLISTADDR,
                LINK_PTR_LOW,
                (uAsyncListAddrLow32 >> USB_EHCD_ASYNCHLISTADDR_LINK_PTR_LOW));

    /* Enable the periodic schedule */

    USB_EHCD_SET_BIT(pHCDData,
                 USBCMD,
                 PERIODIC_SCHEDULE_ENABLE);

    /* Set the interrupt threshold to generate interrupt after 125 us by default */

    uInterrupInterval = usrUsbEhciInterruptIntervalGet();
    
    USB_EHCD_SET_FIELD(pHCDData,
                       USBCMD,
                       INT_THRESHOLD_CONTROL,
                       uInterrupInterval);

    /* Set the Run/Stop bit to start the Host Controller */

    USB_EHCD_SET_BIT(pHCDData,
                 USBCMD,
                 RS);

    /*
     * Set the CONFIGURE_FLAG bit to default route all the ports
     * to the EHCI Host Controller
     */

    USB_EHCD_SET_BIT(pHCDData,
                 CONFIGFLAG,
                 CF);

    return TRUE;
    }
    /* End of function usbEhcdDataInitialize() */


/*******************************************************************************
*
* usbEhcdDataUninitialize - perform the EHCD_DATA structure uninitialization
*
* This routine performs the EHCD_DATA structure uninitialization.
*
* RETURNS: N/A.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbEhcdDataUninitialize
    (
    pUSB_EHCD_DATA pHCDData
    )
    {
    /* Check the validity of the parameter */

    if (NULL == pHCDData)
        {
        USB_EHCD_ERR("usbEhcdDataUninitialize - invalid parameter\n",
            0, 0, 0, 0, 0, 0);

        return;
        }

    /* Clear the Run/Stop bit to stop the Host Controller */

    USB_EHCD_CLR_BIT(pHCDData,
                 USBCMD,
                 RS);

    /*
     * Clear the CONFIGURE_FLAG bit to default route all the ports
     * to the companion Host Controller
     */

    USB_EHCD_CLR_BIT(pHCDData,
                 CONFIGFLAG,
                 CF);

    /* Disable the periodic list */

    USB_EHCD_CLR_BIT(pHCDData,
                 USBCMD,
                 PERIODIC_SCHEDULE_ENABLE);

    /*
     * Disable the asynchronous list
     * Periodic list is disabled during EHCD
     * data structure uninitialization.
     */

    USB_EHCD_CLR_BIT (pHCDData, USBCMD, ASYNCH_SCHEDULE_ENABLE);


    /* Destroy the thread created for handling the interrupts */

    if (pHCDData->IntHandlerThreadID != OS_THREAD_FAILURE
        && pHCDData->IntHandlerThreadID != 0)
        {
        OS_DESTROY_THREAD(pHCDData->IntHandlerThreadID);

        pHCDData->IntHandlerThreadID = OS_THREAD_FAILURE;
        }

    /* Destroy the signalling event */

    if (NULL != pHCDData->InterruptEvent)
        {
        OS_DESTROY_EVENT(pHCDData->InterruptEvent);

        pHCDData->InterruptEvent = NULL;
        }

    /* Destroy the requests synchronization event */

    if (NULL != pHCDData->RequestSynchEventID)
        {
        OS_DESTROY_EVENT(pHCDData->RequestSynchEventID);

        pHCDData->RequestSynchEventID = NULL;
        }

    /* Destroy the reclamation list synchronisation event */

    if (NULL != pHCDData->ReclamationListSynchEventID)
        {
        OS_DESTROY_EVENT(pHCDData->ReclamationListSynchEventID);

        pHCDData->ReclamationListSynchEventID = NULL;
        }

    /* Destroy the active pipe list synchronisation event */

    if (NULL != pHCDData->ActivePipeListSynchEventID)
        {
        OS_DESTROY_EVENT(pHCDData->ActivePipeListSynchEventID);

        pHCDData->ActivePipeListSynchEventID = NULL;
        }

    /* Destroy the bandwidth reservation event */

    if (NULL != pHCDData->BandwidthEventID)
        {
        OS_DESTROY_EVENT(pHCDData->BandwidthEventID);

        pHCDData->BandwidthEventID = NULL;
        }

    /* delete the mutex created for protecting asynchronous queue access */

    if (NULL != pHCDData->AsychQueueMutex)
        {
        semDelete(pHCDData->AsychQueueMutex);

        pHCDData->AsychQueueMutex = NULL;
        }

    /* Free memory allocated for the port status */

    if (NULL != pHCDData->RHData.pPortStatus)
        {
        OS_FREE(pHCDData->RHData.pPortStatus);

        pHCDData->RHData.pPortStatus = NULL;
        }

    /* Free memory allocated for the interrupt data */

    if (NULL != pHCDData->RHData.pHubInterruptData)
        {
        OS_FREE(pHCDData->RHData.pHubInterruptData);

        pHCDData->RHData.pHubInterruptData = NULL;
        }

    /* Free memory allocated for the default pipe */

    if (NULL != pHCDData->pDefaultPipe)
        {
        /* Free the memory allocated for the TDs */
        (void) usbEhcdUnSetupPipe(pHCDData, pHCDData->pDefaultPipe);
        
        /* Free the memory allocated for the QH */

        if (NULL != pHCDData->pDefaultPipe->pQH)
            {
            usbEhcdDestroyQH(pHCDData, pHCDData->pDefaultPipe->pQH);

            pHCDData->pDefaultPipe->pQH = NULL;
            }

        /* Destroy the pipe sync event */

        if (NULL != pHCDData->pDefaultPipe->PipeSynchEventID)
            {
            OS_DESTROY_EVENT(pHCDData->pDefaultPipe->PipeSynchEventID);

            pHCDData->pDefaultPipe->PipeSynchEventID = NULL;
            }

        OS_FREE(pHCDData->pDefaultPipe);

        pHCDData->pDefaultPipe = NULL;
        }

    /* Destroy the interrupt tree */

    usbEhcdDestroyInterruptTree(pHCDData);

    /* Free memory allocated for the frame list */

    if (NULL != pHCDData->pFrameList)
        {
        /*
         * Free the memory allocated for the Periodic Frame List.
         *
         * If pHCDData->pFrameList is non-NULL, then we are sure
         * pHCDData->frameListDmaTagId and pHCDData->frameListDmaMapId
         * have already been created, thus no need to check them for NULL.
         */

        (void) vxbDmaBufMemFree(pHCDData->frameListDmaTagId,
            pHCDData->pFrameList,
            pHCDData->frameListDmaMapId);
        }

    /* Destroy the DMA TAGs */

    if (pHCDData->frameListDmaTagId)
        {
        (void) vxbDmaBufTagDestroy(pHCDData->frameListDmaTagId);

        pHCDData->frameListDmaTagId = NULL;
        }

    if (pHCDData->qhDmaTagId)
        {
        (void) vxbDmaBufTagDestroy(pHCDData->qhDmaTagId);

        pHCDData->qhDmaTagId = NULL;
        }

    if (pHCDData->qtdDmaTagId)
        {
        (void) vxbDmaBufTagDestroy(pHCDData->qtdDmaTagId);

        pHCDData->qtdDmaTagId = NULL;
        }

    if (pHCDData->itdDmaTagId)
        {
        (void) vxbDmaBufTagDestroy(pHCDData->itdDmaTagId);

        pHCDData->itdDmaTagId = NULL;
        }

    if (pHCDData->sitdDmaTagId)
        {
        (void) vxbDmaBufTagDestroy(pHCDData->sitdDmaTagId);

        pHCDData->sitdDmaTagId = NULL;
        }

    return;
    }/* End of function usbEhcdDataUninitialize() */

/*******************************************************************************
*
* usbEhcdInstantiate - instantiate the USB EHCI Host Controller Driver.
*
* This routine instantiates  the EHCI Host Controller Driver and allows
* the EHCI Controller driver to be included with the vxWorks image and
* not be registered with vxBus.  EHCI devices will remain orphan devices
* until the usbEhciInit() routine is called.  This supports the
* INCLUDE_EHCI behaviour of previous vxWorks releases.
*
* The routine itself does nothing.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbEhcdInstantiate (void)
    {
    return;
    }

/*******************************************************************************
*
* usbEhcdInit - initialize the EHCI Host Controller Driver
*
* This routine intializes the EHCI Host Controller Driver data structures.
* This routine is executed prior to vxBus device connect to initialize
* data structures expected by the device initialization.
*
* The USBD must be initialized prior to calling this routine.
* In this routine the book-keeping variables for the EHCI Driver are
* initialized.
*
* The function also registers the EHCI Host controller Drive with USBD
*
* RETURNS: OK or ERROR, if the initialization fails
*
* ERRNO: N/A
*/

STATUS usbEhcdInit (void)
    {

    USBHST_STATUS               status = USBHST_FAILURE; /* To hold the */
                                                         /* status  */
    USBHST_HC_DRIVER            g_pEHCDriverInfo;        /* structure to hold
                                                          * EHCI driver
                                                          * informations
                                                          */
    /*
     * g_EHCDHandle is actually the HCD index in
     * the global HCD driver list, which is not
     * necessarily to be zero, so do not check
     * g_EHCDHandle against zero for validity.
     */

    /*
     * Check whether the globals are initialized - This can happen if this
     * function is called more than once.
     */

    if (g_pEHCDData != NULL)
        {
        USB_EHCD_ERR(
            "usbEhcdInit - EHCD already initialized\n", 0, 0, 0, 0, 0, 0);

        /* WindView Instrumentation */

        USB_HCD_LOG_EVENT(
                    USB_EHCI_WV_INIT_EXIT,
                    "usbEhcdInit() exits - EHCD already initialized",
                    USB_EHCD_WV_FILTER);

        return ERROR;
        }

    /* Initialize the global array */

    g_pEHCDData = (pUSB_EHCD_DATA *)OS_MALLOC
                            (sizeof(pUSB_EHCD_DATA) * USB_MAX_EHCI_COUNT);


    /* Check if memory allocation is successful */

    if (g_pEHCDData == NULL)
        {
        USB_EHCD_ERR("usbEhcdInit - allocation failed for g_pEHCDData\n",
            0, 0, 0, 0, 0, 0);

        /* WindView Instrumentation */

        USB_HCD_LOG_EVENT(
                    USB_EHCI_WV_INIT_EXIT,
                    "usbEhcdInit() exits - allocation failed for g_pEHCDData",
                    USB_EHCD_WV_FILTER);

        return ERROR;
        }

    /* Reset the global array */

    OS_MEMSET (g_pEHCDData, 0, sizeof(pUSB_EHCD_DATA) * USB_MAX_EHCI_COUNT);

    /* Create the event used for synchronising the free list accesses */

    g_ListAccessEvent = OS_CREATE_EVENT(OS_EVENT_SIGNALED);

    /* Check if the event is created successfully */

     if (NULL == g_ListAccessEvent)
        {
         USB_EHCD_ERR(
            "usbEhcdInit - Error in creating the list event\n",
            0, 0, 0, 0, 0, 0);

        /* WindView Instrumentation */

        USB_HCD_LOG_EVENT(USB_EHCI_WV_INIT_EXIT,
                    "usbEhcdInit() exits - Creating list access event failed",
                    USB_EHCD_WV_FILTER);

        /* Free the global array */

        OS_FREE (g_pEHCDData);

        g_pEHCDData = NULL;

        return ERROR;
        }

    /* Hook the routine which needs to be called on a reboot */
#if 0 //zj
    if (ERROR == rebootHookAdd(usbEhcdDisableHC))
        {
        USB_EHCD_ERR(
            "Error in hooking the ehcd routine to disable HC\n",
            0, 0, 0, 0, 0, 0);

        /* Destroy the event */

        OS_DESTROY_EVENT(g_ListAccessEvent);

        g_ListAccessEvent = NULL;

        /* free the global array */

        OS_FREE (g_pEHCDData);

        g_pEHCDData = NULL;

        return ERROR;
        }
#endif
    /* Initialize the members of the data structure */

    OS_MEMSET(&g_pEHCDriverInfo, 0, sizeof(USBHST_HC_DRIVER));

    /* Populate the members of the HC Driver data structure - start */
    /* Function to retrieve the frame number */

    g_pEHCDriverInfo.getFrameNumber = usbEhcdGetFrameNumber;

    /* Function to change the frame interval */

    g_pEHCDriverInfo.setBitRate = usbEhcdSetBitRate;

    /* Function to check whether bandwidth is available */

    g_pEHCDriverInfo.isBandwidthAvailable = usbEhcdIsBandwidthAvailable;

    /* Function to create a pipe */

    g_pEHCDriverInfo.createPipe = usbEhcdCreatePipe;

    /* Function to modify the default pipe */

    g_pEHCDriverInfo.modifyDefaultPipe = usbEhcdModifyDefaultPipe;

    /* Function to delete the pipe */

    g_pEHCDriverInfo.deletePipe = usbEhcdDeletePipe;

    /* Function to check if the request is pending */

    g_pEHCDriverInfo.isRequestPending = usbEhcdIsRequestPending;

    /* Function to submit an URB */

    g_pEHCDriverInfo.submitURB = usbEhcdSubmitURB;

    /* Function to cancel an URB */

    g_pEHCDriverInfo.cancelURB = usbEhcdCancelURB;

    /* Function to control the pipe characteristics */

    g_pEHCDriverInfo.pipeControl = usbEhcdPipeControl;

    /* Function to submit a clear tt request complete  */

    g_pEHCDriverInfo.clearTTRequestComplete = NULL;

    /* Function to submit a clear tt request complete  */

    g_pEHCDriverInfo.resetTTRequestComplete = NULL;

    /* Populate the members of the HC Driver data structure - End */

    /*
     * Register the HCD with the USBD. We also pass the bus id in this function
     * This to to register EHCI driver with vxBus as a bus type.
     * After the registration is done we get a handle "g_EHCDHandle". This
     * handle is used for all subsequent communication of EHCI driver with
     * USBD
     */

    status = usbHstHCDRegister(&g_pEHCDriverInfo,
                             &g_EHCDHandle,
                             NULL,
                             VXB_BUSID_USB_HOST_EHCI
                             );

    /* Check whether the registration is successful */

    if (USBHST_SUCCESS != status)
        {
        USB_EHCD_ERR(
            "usbEhcdInit - Error in registering the HCD \n",
            0, 0, 0, 0, 0, 0);

        /* Destroy the event */

        OS_DESTROY_EVENT(g_ListAccessEvent);

        g_ListAccessEvent = NULL;

        /* free the global array */

        OS_FREE (g_pEHCDData);

        g_pEHCDData = NULL;

#if 0 //zj
        (void) rebootHookDelete(usbEhcdDisableHC);
#endif
        /* WindView Instrumentation */

        USB_HCD_LOG_EVENT (USB_EHCI_WV_INIT_EXIT,
                    "usbEhcdInit() exits - EHCD registration failed",
                    USB_EHCD_WV_FILTER);

        return ERROR;
        }

    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT (USB_EHCI_WV_INIT_EXIT,
                       "usbEhcdInit() exits - EHCD successfully registered",
                       USB_EHCD_WV_FILTER);
    return OK;
    }/* End of function usbEhcdInit() */

/*******************************************************************************
*
* usbEhcdExit - uninitialize the EHCI Host Controller
*
* This routine uninitializes the EHCI Host Controller Driver and detaches
* it from the USBD interface layer.
*
* RETURNS: OK, or ERROR if there is an error during HCD uninitialization.
*
* ERRNO: N/A
*/

STATUS usbEhcdExit(void)
    {
    USBHST_STATUS Status = USBHST_FAILURE;

    /* To hold the status of the function call */

    /* validate the global */

    /*
     * g_EHCDHandle is actually the HCD index in
     * the global HCD driver list, which is not
     * necessarily to be zero, so do not check
     * g_EHCDHandle against zero for validity.
     */

	/* Call the function to deregister with vxBus */

    if (vxbUsbEhciDeregister() == ERROR)
        {
        USB_EHCD_ERR(
            "Failed to Deregister EHCI Driver with vxBus\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Deregister the HCD from USBD */

    Status = usbHstHCDDeregister(g_EHCDHandle);

    /* Check if HCD is deregistered successfully */

    if (USBHST_SUCCESS != Status)
        {
        USB_EHCD_ERR(
            "Failure in deregistering the HCD\n",
            0, 0, 0, 0, 0, 0);

        /* WindView Instrumentation */

        USB_HCD_LOG_EVENT(
                    USB_EHCI_WV_INIT_EXIT,
                    "usbEhcdExit() exits - EHCD deregistration failed",
                    USB_EHCD_WV_FILTER);

        return ERROR;
        }

    /* Delete the hook function from the table */
#if 0 //zj
    if (ERROR == rebootHookDelete(usbEhcdDisableHC))
        return ERROR;
#endif
    g_EHCDHandle = 0;

    /* Destroy the event used for synchronisation of the free list */

    OS_DESTROY_EVENT(g_ListAccessEvent);

    g_ListAccessEvent = NULL;

    /* Free the memory allocated for the global data structure */

    OS_FREE(g_pEHCDData);

    g_pEHCDData = NULL;

    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT(USB_EHCI_WV_INIT_EXIT,
        "usbEhcdExit() exits successfully",
        USB_EHCD_WV_FILTER);

    return OK;
    }

/*******************************************************************************
*
* usbEhcdDisableHC - disable the host controller
*
* This routine is called on a warm reboot to disable the host controller by
* the BSP.
*
* RETURNS: 0, always
*
* ERRNO: N/A
*
* \NOMANUAL
*/

int usbEhcdDisableHC
    (
    int startType
    )
    {
    /* Pointer to the HCD data structure */

    pUSB_EHCD_DATA      pHCDData = NULL;
    UINT8               index = 0;
    UINT8               count = 0;

    if ((0 == g_EHCDControllerCount) ||
        (NULL == g_ListAccessEvent) ||
        (g_pEHCDData == NULL))
        {
        return 0;
        }

    /* This loop releases the resources for all the host controllers present */

    for(index = 0; index < USB_MAX_EHCI_COUNT; index++)
        {
        /* Extract the pointer from the global array */

        pHCDData = g_pEHCDData[index];

        /* Check if the pointer is valid */

        if (pHCDData == NULL)
            continue;

        /* Stop Host Controller */

        USB_EHCD_CLR_BIT(pHCDData, USBCMD, RS);

        /* Controller must halt within 16 micro-frames after clears RS bit */

        for (count = 0; count < NUM_RETRIES_USBCMD_RS; count++)
            {
            if (USB_EHCD_GET_FIELD(pHCDData, USBSTS, HCHALTED) != 0)
                break;

            OS_BUSY_WAIT_MS(1);

            if (count == (NUM_RETRIES_USBCMD_RS - 1))
                {
                USB_EHCD_ERR("Failed to stop the HCD\n", 0, 0, 0, 0, 0, 0);
                }
            }

        /* Reset the Host Controller */

        USB_EHCD_SET_BIT(pHCDData, USBCMD, HCRESET);

        if (pHCDData->pPostResetHook != NULL)
            {
            UINT32  uTempCount;

            /* This loop waits for sometime for the reset to be complete */

            for (uTempCount = 0;
                 uTempCount < USB_EHCD_MAX_DELAY_INTERVAL;
                 uTempCount++)
                {
                /* Check if the reset is successful */

                if (0 == USB_EHCD_GET_FIELD(pHCDData, USBCMD, HCRESET))
                    {
                    USB_EHCD_DBG("Host Controller reset successful \n",
                                 1, 2, 3, 4, 5, 6);
                    break;
                    }

                /* Wait for sometime */

                OS_BUSY_WAIT_MS(1);
                }

            USB_EHCD_DBG("Calling Host Controller PostResetHook\n",
                0, 0, 0, 0, 0, 0);

            pHCDData->pPostResetHook();
            }

        if (FALSE == usrUsbEhciPollingEnabled())
            {
            /* Un-register the interrupt handler for the IRQ */

            /* Disable the interrupts */

            if (vxbIntDisable (pHCDData->pDev,
                               0,
                               usbEhcdISR,
                               (VOID *)pHCDData->pDev)== ERROR)
                {
                USB_EHCD_ERR("Failure in deregistering the bus\n",
                    0, 0, 0, 0, 0, 0);

                /* WindView Instrumentation */

                USB_HCD_LOG_EVENT (USB_EHCI_WV_INIT_EXIT,
                                   "usbEhcdExit() exits - bus deregistration failed",
                                    USB_EHCD_WV_FILTER);

                return 0;
                }

            vxbIntDisconnect (pHCDData->pDev, 0, usbEhcdISR, (VOID *)(pHCDData->pDev));
            }
        else
            {
            /* Destroy the polling thread */

            if (ehciPollingTh[pHCDData->uBusIndex] != OS_THREAD_FAILURE
                && ehciPollingTh[pHCDData->uBusIndex] != 0)
                {
                OS_DESTROY_THREAD (ehciPollingTh[pHCDData->uBusIndex]);

                ehciPollingTh[pHCDData->uBusIndex] = OS_THREAD_FAILURE;
                }
            }
        }/* End of for () */

    return 0;
    }


/*******************************************************************************
*
* usbHcdEhciDeviceInit - legacy support implementation
*
* This function implements the legacy support for EHCI Controller. This
* function handles the handoff of USB EHCI Controller from BIOS to System
* Software
*
* RETURN : N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbHcdEhciDeviceInit
    (
    VXB_DEVICE_ID       pDev                    /* struct vxbDev * */
    )
    {
    UINT32              hccParamsValue = 0;     /* store the legacy */
                                                /* information */
    int                 count = 0;

    UINT32            * pHandler;


    /* validate parameters */

    if (pDev == NULL)
        {
        USB_EHCD_ERR("usbHcdEhciDeviceInit pDev NULL\n",
            1, 2, 3, 4, 5, 6);

        return;
        }

    /* Retrieve the contents of the HCCPARAMS register */

    USB_EHCD_DBG("usbHcdEhciDeviceInit for pDev %p and unit %d\n",
        pDev,
        pDev->unitNumber, 3, 4, 5 ,6);

    if (vxbRegMap (pDev, 0, (void **)&pHandler) == ERROR)
        {
        USB_EHCD_ERR("usbHcdEhciDeviceInit vxbRegMap error\n",
            1, 2, 3, 4, 5, 6);

        return;
        }

    hccParamsValue = vxbRead32 ((pVOID)pHandler,
                    (UINT32 *)((UINT8 *)pDev->pRegBase [0] + EHCI_HCCPARAMS));

    /*
     * The bits 15 to 8 indicate whether the host controller supports
     * extended capabilities
     */
 //zj
    hccParamsValue = (hccParamsValue & EHCI_HCCPARAMS_EECP_MASK) >> 8;
printk("%s %d %p hccParamsValue 0x%x\n",__FUNCTION__,__LINE__,pDev->pRegBase [0] ,hccParamsValue);
#if 0
    /*
     * If the value read is greater than EHCI_HCCPARAMS_EECP_MIN,
     * then offset to the pci configuration space, set the bit which
     * indicates OS ownership of the host controller.
     */

    if (hccParamsValue >= EHCI_HCCPARAMS_EECP_MIN)
        {

        UINT32      pciConfigInfo = 0;      /* to read/write in PCI */
                                            /* configuration space */
        UINT8       pciWriteForOwnership = 0x01;

        /*
         * To obtain ownership set the HC OS OWNED SEMAPHORE to 1, ownership is
         * obtained, after this bit is set, only when this bit reads 1 and HC
         * BIOS OWNED SEMAPHORE bit reads 0. The time one should wait for this
         * bit to be changed is not mentioned in the specifications. Therefore
         * wait for 1 millisec and then again retry for ownership if its not
         * obtained
         */

        VXB_PCI_BUS_CFG_WRITE (pDev,
                               (hccParamsValue + EHCI_USBLEGSUP_OS_OFFSET),
                               1,
                               pciWriteForOwnership);

        while (count < NUM_RETRIES)
            {

            VXB_PCI_BUS_CFG_READ (pDev, hccParamsValue, 4, pciConfigInfo);


            if (!(pciConfigInfo & EHCI_BIOS_OWNED)&&
                (pciConfigInfo & EHCI_OS_OWNED))
                    break;

            count++;

            /* Give a delay if 1 msec (can not use taskDelay here) */

            OS_BUSY_WAIT_MS(1);
            }

        if (count == NUM_RETRIES)
            {
            /* Just return, pDev can not be freed here */

            return;
            }
        }
#endif
    return;
    }

/*******************************************************************************
*
* usbHcdEhciDeviceConnect - initializes the EHCI Host Controller Device
*
* This routine intializes the EHCI host controller and activates it to
* handle all USB operations. The function is called by vxBus on a successful
* driver - device match with VXB_DEVICE_ID as parameter. This structure has all
* information about the device. This routine resets all EHCI registers and
* then initializes them with default value. Once all the registers are properly
* initialized, it sets the RUN/STOP bit to activate the controller for USB
* operations
*
* RETURN : N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbHcdEhciDeviceConnect
    (
    VXB_DEVICE_ID               pDev            /* struct vxbDev */
    )
    {

    STATUS                      busInitStatus = FALSE;
                                               /* To hold the return value of */
                                               /* bus initialization function */
                                               /* call */

    pUSB_EHCD_DATA              pHCDData = NULL;
                                               /* To hold the pointer to the */
                                               /* HCD maintained data */
                                               /* structure */

    USBHST_STATUS               status = USBHST_FAILURE;
                                                /* to hold status info */

    UINT32                      uTempCount = 0; /* counter */

    BOOLEAN                     bInitStatus = FALSE;
                                                /* to host status */

    /* To hold the name of the thread */

    UCHAR ThreadName[20];

    UINT32 index;


    /* validate paramters */

    if (pDev == NULL)
        {
        USB_EHCD_ERR(
            "usbHcdEhciDeviceConnect - Invalid Parameters \n",
            0, 0, 0, 0, 0, 0);

        /* WindView Instrumentation */

        USB_HCD_LOG_EVENT(
                    USB_EHCI_WV_INIT_EXIT,
                    "usbHcdEhciDeviceConnect() exits - Invalid Paramters",
                    USB_EHCD_WV_FILTER);

        ossStatus (USBHST_INVALID_PARAMETER);

        return;
        }

    /*
     * determine whether the usage counter has exceeded maximum number of
     * host controllers
     */

    OS_WAIT_FOR_EVENT (g_ListAccessEvent, WAIT_FOREVER);

    if (g_EHCDControllerCount == USB_MAX_EHCI_COUNT)
        {
        USB_EHCD_ERR(
            "usbHcdEhciDeviceConnect - Out of resources \n",
            0, 0, 0, 0, 0, 0);

        /* WindView Instrumentation */

        USB_HCD_LOG_EVENT(
                    USB_EHCI_WV_INIT_EXIT,
                    "usbHcdEhciDeviceConnect() exits - Out of Resource",
                    USB_EHCD_WV_FILTER);

        status = USBHST_INSUFFICIENT_RESOURCE;

        OS_RELEASE_EVENT (g_ListAccessEvent);

        goto ERROR_HANDLER;
        }

    /* Get bus index */

    for (index = 0; index < USB_MAX_EHCI_COUNT; index ++)
        {
        if (g_pEHCDData[index] == NULL)
            break;
        }

    if (index == USB_MAX_EHCI_COUNT)
        {
        USB_EHCD_ERR("usbHcdEhciDeviceConnect - g_pEHCDData is full\n",
            0, 0, 0, 0, 0, 0);

        /* WindView Instrumentation */

        USB_HCD_LOG_EVENT(
                    USB_EHCI_WV_INIT_EXIT,
                    "usbHcdEhciDeviceConnect() exits - g_pEHCDData is full",
                    USB_EHCD_WV_FILTER);

        status = USBHST_INSUFFICIENT_RESOURCE;

        OS_RELEASE_EVENT (g_ListAccessEvent);

        goto ERROR_HANDLER;
        }

    /* If this EHCI device is a PCI device, Unit Number shall be assigned. */
    if (0 == strncmp(pDev->pName, USB_EHCI_PCI_NAME, sizeof(USB_EHCI_PCI_NAME)))
        {
        vxbNextUnitGet(pDev);
        }


    /*
     * Initialize the ISR Spinlock
     */

    SPIN_LOCK_ISR_INIT(&spinLockIsrEhcd[index], 0);

    OS_RELEASE_EVENT (g_ListAccessEvent);


    /*
     * Call the bus specific initialization function to initialize the
     * host controllers in the system. This primarily is to get some
     * hardware parameters and make the hardware in quiescent state.
     */

    busInitStatus = usbEhcdHostBusInitialize(&pHCDData, pDev, index);

    if (USB_EHCD_HCBUS_INITIALIZED != busInitStatus)
        {
        USB_EHCD_ERR(
            "usbHcdEhciDeviceConnect - No other HC is present\n",
            0, 0, 0, 0, 0, 0);

        /* Reset the global array */

        g_pEHCDData[index] = NULL;

        status = USBHST_FAILURE;

        goto ERROR_HANDLER;
        }

    /* Initialize the Host Controller data structure */

    bInitStatus = usbEhcdDataInitialize(pHCDData);

    /* Check if the EHCD_DATA structure initialization is successful */

    if (TRUE != bInitStatus)
        {
        USB_EHCD_ERR(
            "usbHcdEhciDeviceConnect - "
            "EHCD_DATA initialization is unsuccessful\n",
            0, 0, 0, 0, 0, 0);

        /* Reset the global array */

        g_pEHCDData[index] = NULL;

        /* Call the uninitialization function of the HC bus */

        usbEhcdHostBusUninitialize(pHCDData);

        /* WindView Instrumentation */

        USB_HCD_LOG_EVENT (USB_EHCI_WV_INIT_EXIT,
            "usbHcdEhciDeviceConnect() exits - "
            "Controller Data Initialisation failed",
            USB_EHCD_WV_FILTER);

        status = USBHST_FAILURE;

        goto ERROR_HANDLER;
        }

    /* Power on all the root hub ports */

    /*
     * The EHC_PPC field indicates whether the EHCI Host Controller supports
     * per port power switching.
     * Check whether the port supports per port power switching.
     */

    /*
     * Port Power Control (PPC). This field indicates whether the host
     * controller implementation includes port power control. A one in
     * this bit indicates the ports have port power switches. A zero in
     * this bit indicates the port do not have port power switches. The
     * value of this field affects the functionality of the Port Power
     * field in each port status and control register (see Section 2.3.8).
     */

    /*
     * Port Power (PP). R/W or RO. The function of this bit depends on
     * the value of the Port Power Control (PPC) field in the HCSPARAMS
     * register. The behavior is as follows:
     *  PPC PP   Operation
     *  0b  1b   RO.Host controller does not have port power control switches.
     *           Each port is hard-wired to power.
     *  1b 1b/0b R/W.Host controller has port power control switches. This bit
     *           represents the current setting of the switch (0 = off, 1 = on).
     *           When power is not available on a port (i.e. PP equals a 0),
     *           the port is nonfunctional and will not report attaches,
     *           detaches, etc.
     * When an over-current condition is detected on a powered port and PPC
     * is a one, the PP bit in each affected port may be transitioned by
     * the host controller from a 1 to 0 (removing power from the port).
     */

    if (1 == USB_EHCD_GET_FIELD(pHCDData,
                                HCSPARAMS,
                                PPC))
        {
        /* This loop will power on all the ports */

        for (uTempCount = 0;
             uTempCount < pHCDData->RHData.uNumDownstreamPorts;
             uTempCount++)
            {
            USB_EHCD_DBG("Set PORT_POWER for PORT %d\n",
                uTempCount, 2, 3, 4, 5, 6);

            /* Set the PP field to power on the port */

            USB_EHCD_SET_BIT_PORT(pHCDData,
                                  uTempCount,
                                  PORT_POWER);

            /* Wait for 200 ms for the power to settle down */

            OS_DELAY_MS(200);

            }/* End of for() */
        }

    /*
     * If a device is connected while the target boots up, there
     * is no connect status change and no root hub status change
     * interrupt which is generated
     */

    for (uTempCount = 0;
         uTempCount < pHCDData->RHData.uNumDownstreamPorts;
         uTempCount++)
        {
        if (USB_EHCD_GET_FIELD_PORT(pHCDData,
                                    uTempCount,
                                    CURRENT_CONNECT_STATUS) != 0)
            {
            /* To hold the status of the port */

            UINT32 uPortStatus = 0;

            /* Copy the value in the port status register */

            OS_MEMCPY(&uPortStatus,
                      (pHCDData->RHData.pPortStatus +
                       uTempCount * USB_EHCD_RH_PORT_STATUS_SIZE),
                      USB_EHCD_RH_PORT_STATUS_SIZE);

            /* Swap the data from LE format */

            uPortStatus =
              USB_EHCD_SWAP_USB_DATA(pHCDData->uBusIndex, uPortStatus);

            /* Update the connect status change */

            uPortStatus |= USB_EHCD_RH_PORT_CONNECT_CHANGE;

            /* Swap the data to LE format data */

            uPortStatus =
              USB_EHCD_SWAP_USB_DATA(pHCDData->uBusIndex, uPortStatus);

            /* Copy the status back to the port status */

            OS_MEMCPY((pHCDData->RHData.pPortStatus +
                       uTempCount * USB_EHCD_RH_PORT_STATUS_SIZE),
                      &uPortStatus,
                      USB_EHCD_RH_PORT_STATUS_SIZE);

            /*
             * Call the function to populate the interrupt
             * status data
             */

            usbEhcdCopyRHInterruptData(
                     pHCDData,
                     (UINT32)(USB_EHCD_RH_MASK_VALUE << uTempCount));

            USB_EHCD_DBG("usbHcdEhciDeviceConnect - "
                         "A device already connected on PORT %d\r\n",
                         uTempCount, 2, 3, 4, 5, 6);

            }
        }

    /*
     * Enable the interrupts which are necessary for the EHCI
     * Host Controller. Other interrupts can be enabled only after
     * Root hub configuration.
     */

    if (FALSE == usrUsbEhciPollingEnabled())
        {

        USB_EHCD_WRITE_REG(pHCDData,
                       USB_EHCD_USBINTR,
                       USB_EHCD_INTERRUPT_MASK);

        /* Enable the interrupts */

        if (vxbIntEnable (pDev,
                          0,
                          usbEhcdISR,
                          (VOID *)(pHCDData->pDev)) == ERROR)
            {
            USB_EHCD_ERR(
                "usbHcdEhciDeviceConnect - Error enabling intrrupts\n",
                0, 0, 0, 0, 0, 0);

            /* Reset the global array */

            g_pEHCDData[index] = NULL;

            /* Stop the Host Controller */

            USB_EHCD_CLR_BIT(pHCDData,
                             USBCMD,
                             RS);

            /* Call the uninitialization function of the HCD data */

            usbEhcdDataUninitialize(pHCDData);

            /* Call the uninitialization function of the HC bus */

            usbEhcdHostBusUninitialize(pHCDData);

            /* WindView Instrumentation */

            USB_HCD_LOG_EVENT (USB_EHCI_WV_INIT_EXIT,
                        "usbHcdEhciDeviceConnect() exits - int enable failed",
                        USB_EHCD_WV_FILTER);

            status = USBHST_FAILURE;

            goto ERROR_HANDLER;
            }
        
        }
    
    /* Take the mutex */

    OS_WAIT_FOR_EVENT (g_ListAccessEvent, WAIT_FOREVER);

    /* Incremnet the global counter */

    g_EHCDControllerCount++;

    /*
     * Do pre-initialization of these registers.  This is to prevent deadlocks
     * when these regiters are accessed in spinLock regions.  These registers
     * can be read with impunity.
     */

    USB_EHCD_READ_REG(pHCDData, USB_EHCD_USBINTR);

    USB_EHCD_READ_REG(pHCDData, USB_EHCD_USBSTS);

    /* We can now respond to interrupts */

    pHCDData->isrMagic = USB_EHCD_MAGIC_ALIVE;

    OS_RELEASE_EVENT (g_ListAccessEvent);

    /* Register the bus with the USBD */

    status = usbHstBusRegister (g_EHCDHandle,
                                USBHST_HIGH_SPEED,
                                (ULONG)pHCDData->pDefaultPipe,
                                pDev);

    /* Check if the bus is registered successfully */

    if (USBHST_SUCCESS != status)
        {
        USB_EHCD_ERR("usbHcdEhciDeviceConnect - Error in registering the bus \n",
            0, 0, 0, 0, 0, 0);

        /* Decrement the global counter */

        g_EHCDControllerCount--;

        /* Reset the array element */

        g_pEHCDData[index] = NULL;

        /* Disable intrrupts */

        if (FALSE == usrUsbEhciPollingEnabled())
            {
            vxbIntDisable (pDev, 0, usbEhcdISR, (VOID *)(pHCDData->pDev));
            }

        /* Stop the Host Controller */

        USB_EHCD_CLR_BIT(pHCDData,
                     USBCMD,
                     RS);

        /* Call the uninitialization function of the HCD data */

        usbEhcdDataUninitialize(pHCDData);

        /* Call the uninitialization function of the HC bus */

        usbEhcdHostBusUninitialize(pHCDData);

        /* WindView Instrumentation */

        USB_HCD_LOG_EVENT (USB_EHCI_WV_INIT_EXIT,
                    "usbHcdEhciDeviceConnect() exits - Bus registration failed",
                    USB_EHCD_WV_FILTER);

        status = USBHST_FAILURE;

        goto ERROR_HANDLER;
        }

    /* Assign an unique name for the interrupt handler thread */

    snprintf((char*)ThreadName, 20, "EHCD_IH%d", pHCDData->uBusIndex);

    /* Create the interrupt handler thread for handling the interrupts */

    pHCDData->IntHandlerThreadID = OS_CREATE_THREAD((char *)ThreadName,
                                                    USB_EHCD_INT_THREAD_PRIORITY,
                                                    usbEhcdInterruptHandler,
                                                    (long)pHCDData);

    /* Check whether the thread creation is successful */

    if (OS_THREAD_FAILURE == pHCDData->IntHandlerThreadID)
        {
        USB_EHCD_ERR("usbEhcdDataInitialize - "
            "interrupt handler thread is not created\n",
            0, 0, 0, 0, 0, 0);

        /* Decrement the global counter */

        g_EHCDControllerCount--;

        /* Reset the array element */

        g_pEHCDData[index] = NULL;

        /* Disable intrrupts */

        if (FALSE == usrUsbEhciPollingEnabled())
            {
            vxbIntDisable (pDev, 0, usbEhcdISR, (VOID *)(pHCDData->pDev));
            }

        /* Stop the Host Controller */

        USB_EHCD_CLR_BIT(pHCDData,
                     USBCMD,
                     RS);

        /* call the function to de-register the bus */

        status = usbHstBusDeregister(g_EHCDHandle, index,
                                     (ULONG)pHCDData->pDefaultPipe);

        /* Check if the bus is deregistered successfully */

        if (USBHST_SUCCESS != status)
            {
            USB_EHCD_ERR("Failure in deregistering the bus\n",
                0, 0, 0, 0, 0, 0);

            /* WindView Instrumentation */

            USB_HCD_LOG_EVENT (USB_EHCI_WV_INIT_EXIT,
                        "usbEhcdExit() exits - bus deregistration failed",
                        USB_EHCD_WV_FILTER);

            }

        /* Call the uninitialization function of the HCD data */

        usbEhcdDataUninitialize(pHCDData);

        /* Call the uninitialization function of the HC bus */

        usbEhcdHostBusUninitialize(pHCDData);

        /* WindView Instrumentation */

        USB_HCD_LOG_EVENT (USB_EHCI_WV_INIT_EXIT,
                    "usbHcdEhciDeviceConnect() exits - "
                    "create IntHandlerThreadID failed",
                    USB_EHCD_WV_FILTER);

        status = USBHST_FAILURE;

        goto ERROR_HANDLER;
        }

    return;

ERROR_HANDLER:

    /* Set the pDrvCtrl as NULL */

    pDev->pDrvCtrl = NULL;

    ossStatus (status);

    return;
    }

/*******************************************************************************
*
* usbHcdEhciDeviceRemove - removes the EHCI Host Controller device
*
* This routine un-initializes the USB host controller device. The function
* is registered with vxBus and called when the driver is de-registered with
* vxBus. The function will have VXB_DEVICE_ID as its parameter. This structure
* consists of all the information about the device. The function will
* subsequently de-register the bus from USBD.
*
* <pDev> - vxBus Device Id.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbHcdEhciDeviceRemove
    (
    VXB_DEVICE_ID               pDev            /* struct vxbDev */
    )
    {
    pUSB_EHCD_DATA pHCDData = NULL;  /* To hold the pointer to the HCD */
                                     /* maintained data structure */
    USBHST_STATUS  status;           /* To hold the status of the function */
    UINT32         uBusIndex;        /* index of pUSB_EHCD_DATA */
    int            i = 0;

    /* validate the paramters */

    if (pDev == NULL)
        {
        USB_EHCD_DBG("usbHcdEhciDeviceRemove - pDev is NULL\n",
                     0, 0, 0, 0, 0, 0);
        return ERROR;
        }

    /*
     * pDev->pDrvCtrl is NULL means we don't need to clean the pHCDData
     * we should return OK to indicate the device can be removed
     */

    if (pDev->pDrvCtrl == NULL)
        {
        USB_EHCD_DBG("usbHcdEhciDeviceRemove - pDev->pDrvCtrl is NULL\n",
                     1, 2, 3, 4, 5, 6);
        return OK;
        }

    /* Extract the pointer from the global array */

    pHCDData = (pUSB_EHCD_DATA)pDev->pDrvCtrl;

    /* Wait for all delay/async pipe deleted */
    while ((NULL != pHCDData->pAsynchReclamationListHead)\
        ||(NULL != pHCDData->pDelayedPipeAdditionList)\
        ||(NULL != pHCDData->pPeriodicReclamationListHead)\
        ||(NULL != pHCDData->pDelayedPipeRemovalList))
        {
        if (i++ > USB_EHCD_MAX_DELAY_INTERVAL)
            break;
        taskDelay(2);
        }    

    OS_WAIT_FOR_EVENT (g_ListAccessEvent, WAIT_FOREVER);

    /* Get bus index */
    uBusIndex = pHCDData->uBusIndex;

    /* Clear the Run/Stop bit to stop the Host Controller */

    USB_EHCD_CLR_BIT (pHCDData, USBCMD, RS);

    /*
     * Reset the CONFIGURE_FLAG bit to default route all the ports
     * to the companion Host Controllers.
     */

    USB_EHCD_CLR_BIT (pHCDData, CONFIGFLAG, CF);


    /* Disable the periodic list */

    USB_EHCD_CLR_BIT(pHCDData,
                     USBCMD,
                     PERIODIC_SCHEDULE_ENABLE);

    /*
     * Disable the asynchronous list
     * Periodic list is disabled during EHCD
     * data structure uninitialization.
     */

    USB_EHCD_CLR_BIT (pHCDData, USBCMD, ASYNCH_SCHEDULE_ENABLE);


    /* Disable all the interrupts */

    USB_EHCD_WRITE_REG(pHCDData,
                   USB_EHCD_USBINTR,
                   0);

    /* We can not respond to interrupts */

    pHCDData->isrMagic = USB_EHCD_MAGIC_DEAD;

    /* call the function to de-register the bus */

    status = usbHstBusDeregister(g_EHCDHandle,
                                 uBusIndex,
                                 (ULONG)pHCDData->pDefaultPipe);

    /* Check if the bus is deregistered successfully */

    if (USBHST_SUCCESS != status)
        {
        USB_EHCD_ERR("usbHcdEhciDeviceRemove - "
            "Failure in deregistering the bus\n",
            0, 0, 0, 0, 0, 0);

        /* WindView Instrumentation */

        USB_HCD_LOG_EVENT (USB_EHCI_WV_INIT_EXIT,
            "usbHcdEhciDeviceRemove() exits - bus deregistration failed",
            USB_EHCD_WV_FILTER);

        OS_RELEASE_EVENT (g_ListAccessEvent);

        return ERROR;
        }

    /* Call the function to uninitialize the EHCD data structure */

    usbEhcdDataUninitialize(pHCDData);

    /* Call the function to perform the host bus specific uninitialization
     * This function will un-hook the ISR from the interrupt line
     */

    usbEhcdHostBusUninitialize(pHCDData);

    g_pEHCDData[uBusIndex] = NULL;

    /* Decrement the global counter by 1 */

    g_EHCDControllerCount--;

    OS_RELEASE_EVENT (g_ListAccessEvent);

    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT(
                USB_EHCI_WV_INIT_EXIT,
                "usbHcdEhciDeviceRemove() exits successfully",
                USB_EHCD_WV_FILTER);

    return OK;
    }

/*******************************************************************************
* vxbUsbEhciRegister - register the EHCI Controller with vxBus
*
* This routine registers the EHCI host controller Driver and EHCI Root-hub
* driver with vxBus.  Note that this can be called early in the initialization
* sequence.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void vxbUsbEhciRegister (void)
    {
    /* Register the EHCI driver for PCI bus as underlying bus */

    if (vxbDevRegister ((DRIVER_REGISTRATION *)
        &usbVxbPciHcdEhciDevRegistration)
        == ERROR)
        {
        USB_EHCD_ERR(
            "Error registering EHCI Driver over PCI Bus\n", 1, 2, 3, 4, 5, 6);

        return;
        }

    /* Register the EHCI driver for PLB bus as underlying bus */

    if (vxbDevRegister ((DRIVER_REGISTRATION *)
        &usbVxbPlbHcdEhciDevRegistration)
        == ERROR)
        {
        USB_EHCD_ERR(
            "Error registering EHCI Driver over PLB Bus\n", 1, 2, 3, 4, 5, 6);
        return;
        }

    /* Register the EHCI driver for root hub */

    if (vxbDevRegister ((DRIVER_REGISTRATION *)
        &usbVxbHcdEhciHub)
        == ERROR)
        {
        USB_EHCD_ERR(
            "Error registering EHCI root hub\n", 1, 2, 3, 4, 5, 6);
        return;
        }


    USB_EHCD_DBG(
        "EHCI Driver successfully registered with vxBus \n", 1, 2, 3, 4, 5, 6);


    return;
    }


/*******************************************************************************
*
* vxbUsbEhciDeregister - de-registers EHCI driver with vxBus
*
* This routine de-registers the EHCI Driver with vxBus Module. The routine
* first de-registers the EHCI Root hub. This is followed by deregistration of
* EHCI Controller for PCI and PLB bus types
*
* RETURNS: OK, or ERROR if not able to de-register with vxBus
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS vxbUsbEhciDeregister (void)
    {

    /* De-register the EHCI root hub as bus controller driver */

    USB_EHCD_DBG("de-registering EHCI Root Hub with vxBus\n", 1, 2, 3, 4, 5, 6);

    if (vxbDriverUnregister (&usbVxbHcdEhciHub) == ERROR)
        {
        USB_EHCD_ERR(
            "Error de-registering EHCI Root Hub with vxBus\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* De-register the EHCI driver for PCI bus as underlying bus */

    USB_EHCD_DBG("de-registering EHCI Driver over PCI Bus\n", 1, 2, 3, 4, 5, 6);

    if (vxbDriverUnregister ((DRIVER_REGISTRATION *)
        &usbVxbPciHcdEhciDevRegistration)
        == ERROR)
        {
        USB_EHCD_ERR(
            "Error de-registering EHCI Driver over PCI Bus\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* De-register the EHCI driver for PLB bus as underlying bus */

    USB_EHCD_DBG("de-registering EHCI Driver over PLB Bus\n", 1, 2, 3, 4, 5, 6);

    if (vxbDriverUnregister (&usbVxbPlbHcdEhciDevRegistration) == ERROR)
        {
        USB_EHCD_ERR(
            "Error de-registering EHCI Driver over PLB Bus\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    return OK;
    }

/*******************************************************************************
*
* usbVxbHcdEhciDeviceProbe - determine whether mathcing device is EHCI
*
* This routine determines whether the matching device is an EHCI Controller or
* not. For PCI device type, the routine will probe the PCI Configuration Device
* for Bus ID, Device Id and Function ID to determine wheter the device is of
* EHCI type. If a matching device is found, the routine will return TRUE.
*
* RETURNS: TRUE or FALSE
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL BOOL usbVxbHcdEhciDeviceProbe
    (
    VXB_DEVICE_ID       pDev
    )

    {
    UINT32              busId = 0;      /* bus id of parent bus */

    UINT32              pciConfigInfo = 0;/* buffer for holding pci configuration
                                           * space information
                                           */

    /* Validate the paramter */

    if ((pDev == NULL) ||
        (pDev->pParentBus == NULL) ||
        (pDev->pParentBus->pBusType == NULL))
        return FALSE;

    /* Determine the bus type of parent bus */

    busId = pDev->pParentBus->pBusType->busID;

    if (busId != VXB_BUSID_PCI)
        {
        return FALSE;
        }

    /*
     * Read the config space using access functions
     */

    VXB_PCI_BUS_CFG_READ (pDev, 0x8, 4, pciConfigInfo);

    /* Right shift by one byte of get the correct value */

    pciConfigInfo >>= 8;

    /*
     * To determine whether the device is the EHCI host controller the
     * bytes read from configuration space should be 0C0320h.
     *
     * 23:16 is Base Class Code should be 0Ch (serial bus controller
     * indication)
     *
     * 15:8 is Sub Class Code should be 03h (universal serial bus
     * controller indication)
     *
     * 7:0 is Programming interface should be 20h indicating USB 2.0 Host
     * Controller that conforms to this specification
     */

    if (((pciConfigInfo  & 0x00FF0000) == USB_EHCI_BASEC) &&
        ((pciConfigInfo  & 0x0000FF00) == USB_EHCI_SCC) &&
        ((pciConfigInfo  & 0x000000FF) == USB_EHCI_PI))
        {

        /* Matching device found */

        return TRUE;
        }

    /* Match not found */

    return FALSE;
    }

/*******************************************************************************
*
* usbVxbNullFunction - dummy routine
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbVxbNullFunction
    (
    VXB_DEVICE_ID       pDev
    )
    {
    /* this is a dummy routine which simply returns */

    return ;
    }

/*******************************************************************************
*
* usbHcdEhciPlbInit - perform the BSP specific Initializaion
*
* For many PLB based controllers, different BSP level initialization is
* requried. This function does the BSP specific initializaiton in case it is
* required
*
* RETURNS: N/A
*
* ERRNO: N/A
*
*\NOMANUAL
*/

LOCAL VOID usbHcdEhciPlbInit
    (
    VXB_DEVICE_ID pDev
    )
    {
    HCF_DEVICE    * pHcf = NULL;
    void          (* pPlbInit1) (void) = NULL;

    /* Get the HCF device from the instance id */

    pHcf = hcfDeviceGet (pDev);

    if (NULL == pHcf || OK != devResourceGet (pHcf, "ehciInit", HCF_RES_ADDR,(void *)&(pPlbInit1)))
       {
       USB_EHCD_WARN("devResourceGet ehciInit fail\n", 1, 2, 3, 4, 5, 6);
       }

    if (pPlbInit1 != NULL)
        {
        (*pPlbInit1)();
        }

    return;
    }

#define USB_EHCD_SHOW
#ifdef USB_EHCD_SHOW

/* foward declarations */

VOID usbEhcdRegShow
    (
    UINT32         uIndex
    );

STATUS usbEhcdShow
    (
    UINT32         uIndex
    );

/*******************************************************************************
*
* usbEhcdSITDShow - show the SITD data structure
*
* This routine shows the SITD data structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbEhcdSITDShow
    (
    pUSB_EHCD_DATA          pHCDData,
    pUSB_EHCD_PIPE          pHCDPipe,
    pUSB_EHCD_REQUEST_INFO  pRequest,
    pUSB_EHCD_SITD          pSITD
    )
    {
    UINT32 uBusIndex = pHCDData->uBusIndex;

    printf("\nSITD %p:\n"
           "uNextLinkPointer                %08x\n"
           "uEndPointCharacteristics        %08x\n"
           "uMicroFrameScheduleControl      %08x\n"
           "uTransferState                  %08x\n"
           "uBufferPointerList[0]           %08x\n"
           "uBufferPointerList[1]           %08x\n"
           "uBackPointer                    %08x\n"
           "uExtBufferPointerPageList[0]    %08x\n"
           "uExtBufferPointerPageList[1]    %08x\n",
            pSITD,
            USB_EHCD_SWAP_DESC_DATA(uBusIndex,
                pSITD->uNextLinkPointer),
            USB_EHCD_SWAP_DESC_DATA(uBusIndex,
                pSITD->uEndPointCharacteristics),
            USB_EHCD_SWAP_DESC_DATA(uBusIndex,
                pSITD->uMicroFrameScheduleControl),
            USB_EHCD_SWAP_DESC_DATA(uBusIndex,
                pSITD->uTransferState),
            USB_EHCD_SWAP_DESC_DATA(uBusIndex,
                pSITD->uBufferPointerList[0]),
            USB_EHCD_SWAP_DESC_DATA(uBusIndex,
                pSITD->uBufferPointerList[1]),
            USB_EHCD_SWAP_DESC_DATA(uBusIndex,
                pSITD->uBackPointer),
            USB_EHCD_SWAP_DESC_DATA(uBusIndex,
                pSITD->uExtBufferPointerPageList[0]),
            USB_EHCD_SWAP_DESC_DATA(uBusIndex,
                pSITD->uExtBufferPointerPageList[1])
            );

    printf("\n========================================\n");

    }

/*******************************************************************************
*
* usbEhcdITDShow - show the ITD data structure
*
* This routine shows the ITD data structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbEhcdITDShow
    (
    pUSB_EHCD_DATA          pHCDData,
    pUSB_EHCD_PIPE          pHCDPipe,
    pUSB_EHCD_REQUEST_INFO  pRequest,
    pUSB_EHCD_ITD           pITD
    )
    {
    UINT32 uBusIndex = pHCDData->uBusIndex;
    UINT32 uIndex;

    printf("\nSITD %p:\n",pITD);

    printf("uNextLinkPointer                    %08x\n",
            USB_EHCD_SWAP_DESC_DATA(uBusIndex,
                pITD->uNextLinkPointer));

    for (uIndex = 0; uIndex < 8; uIndex++)
        {
    printf("uTransactionStatusControlList[%d]   %08x\n",
        uIndex,
        USB_EHCD_SWAP_DESC_DATA(uBusIndex,
            pITD->uTransactionStatusControlList[uIndex]));
        }

    for (uIndex = 0; uIndex < 7; uIndex++)
        {
    printf("uBufferPointerList[%d]              %08x\n",
        uIndex,
        USB_EHCD_SWAP_DESC_DATA(uBusIndex,
            pITD->uBufferPointerList[uIndex]));
        }

    for (uIndex = 0; uIndex < 7; uIndex++)
        {
    printf("uExtBufferPointerPageList[%d]       %08x\n",
        uIndex,
        USB_EHCD_SWAP_DESC_DATA(uBusIndex,
            pITD->uExtBufferPointerPageList[uIndex]));
        }

    printf("\n========================================\n");

    }

/*******************************************************************************
*
* usbEhcdQTDShow - show the QTD data structure
*
* This routine shows the QTD data structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbEhcdQTDShow
    (
    pUSB_EHCD_DATA          pHCDData,
    pUSB_EHCD_PIPE          pHCDPipe,
    pUSB_EHCD_REQUEST_INFO  pRequest,
    pUSB_EHCD_QTD           pQTD
    )
    {
    UINT32 uBusIndex = pHCDData->uBusIndex;
    UINT32 uIndex;

    printf("\nQTD %p:\n", pQTD);

    printf("uNextQTDPointer                    %08x\n",
            USB_EHCD_SWAP_DESC_DATA(uBusIndex,
                pQTD->uNextQTDPointer));

    printf("uAlternateNextQTDPointer           %08x\n",
            USB_EHCD_SWAP_DESC_DATA(uBusIndex,
                pQTD->uAlternateNextQTDPointer));

    printf("uTransferInfo                      %08x\n",
            USB_EHCD_SWAP_DESC_DATA(uBusIndex,
                pQTD->uTransferInfo));

    for (uIndex = 0; uIndex < 5; uIndex++)
        {
    printf("uBufferPagePointerList[%d]          %08x\n",
        uIndex,
        USB_EHCD_SWAP_DESC_DATA(uBusIndex,
            pQTD->uBufferPagePointerList[uIndex]));
        }

    for (uIndex = 0; uIndex < 5; uIndex++)
        {
    printf("uExtBufferPointerPageList[%d]       %08x\n",
        uIndex,
        USB_EHCD_SWAP_DESC_DATA(uBusIndex,
            pQTD->uExtBufferPointerPageList[uIndex]));
        }

    printf("\n========================================\n");

    }

/*******************************************************************************
*
* usbEhcdQHShow - show the QH data structure
*
* This routine shows the QH data structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbEhcdQHShow
    (
    pUSB_EHCD_DATA          pHCDData,
    pUSB_EHCD_PIPE          pHCDPipe,
    pUSB_EHCD_QH            pQH
    )
    {
    UINT32 uBusIndex = pHCDData->uBusIndex;
    UINT32 uIndex;

    printf("\nQH %p ==> QTD %p\n", pQH, pQH->pQTD);

    printf("uQueueHeadHorizontalLinkPointer    %08x\n",
        USB_EHCD_SWAP_DESC_DATA(uBusIndex,
                pQH->uQueueHeadHorizontalLinkPointer));

    printf("uEndPointCharacteristics           %08x\n",
        USB_EHCD_SWAP_DESC_DATA(uBusIndex,
                pQH->uEndPointCharacteristics));

    printf("uEndPointCapabilities              %08x\n",
        USB_EHCD_SWAP_DESC_DATA(uBusIndex,
                pQH->uEndPointCapabilities));

    printf("uCurrentQtdPointer                 %08x\n",
        USB_EHCD_SWAP_DESC_DATA(uBusIndex,
                pQH->uCurrentQtdPointer));

    printf("uNextQtdPointer                    %08x\n",
        USB_EHCD_SWAP_DESC_DATA(uBusIndex,
                pQH->uNextQtdPointer));

    printf("uAlternateNextQtdPointer           %08x\n",
        USB_EHCD_SWAP_DESC_DATA(uBusIndex,
                pQH->uAlternateNextQtdPointer));

    printf("uTransferInfo                      %08x\n",
        USB_EHCD_SWAP_DESC_DATA(uBusIndex,
                pQH->uTransferInfo));

    for (uIndex = 0; uIndex < 5; uIndex++)
        {
    printf("uBufferPagePointerList[%d]          %08x\n",
        uIndex,
        USB_EHCD_SWAP_DESC_DATA(uBusIndex,
            pQH->uBufferPagePointerList[uIndex]));
        }

    for (uIndex = 0; uIndex < 5; uIndex++)
        {
    printf("uExtBufferPointerPageList[%d]       %08x\n",
        uIndex,
        USB_EHCD_SWAP_DESC_DATA(uBusIndex,
            pQH->uExtBufferPointerPageList[uIndex]));
        }

    printf("\n========================================\n");

    }

/*******************************************************************************
*
* usbEhcdIsochRequestShow - show the isoch request info data structure
*
* This routine shows the isoch request info data structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbEhcdIsochRequestShow
    (
    pUSB_EHCD_DATA          pHCDData,
    pUSB_EHCD_PIPE          pHCDPipe,
    pUSB_EHCD_REQUEST_INFO  pRequest
    )
    {
    if (USBHST_HIGH_SPEED == pHCDPipe->uSpeed)
        {
        /* Pointer to the ITD */

        pUSB_EHCD_ITD   pITD = pRequest->pHead;

        while (pITD != NULL)
            {
            usbEhcdITDShow(pHCDData, pHCDPipe, pRequest, pITD);

            pITD = pITD->pNext;
            }
        }
    else
        {
        /* Pointer to the SITD */

        pUSB_EHCD_SITD   pSITD = pRequest->pHead;

        while (pSITD != NULL)
            {
            usbEhcdSITDShow(pHCDData, pHCDPipe, pRequest, pSITD);

            pSITD = pSITD->pNext;
            }
        }

    }

/*******************************************************************************
*
* usbEhcdNonIsochRequestShow - show the isoch request info data structure
*
* This routine shows the isoch request info data structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbEhcdNonIsochRequestShow
    (
    pUSB_EHCD_DATA          pHCDData,
    pUSB_EHCD_PIPE          pHCDPipe,
    pUSB_EHCD_REQUEST_INFO  pRequest
    )
    {
    /* Pointer to the QTD */

    pUSB_EHCD_QTD   pQTD = pRequest->pHead;

    while (pQTD != NULL)
        {
        usbEhcdQTDShow(pHCDData, pHCDPipe, pRequest, pQTD);

        pQTD = pQTD->pNext;
        }
    }

/*******************************************************************************
*
* usbEhcdPipeShow - show the pipe data structure
*
* This routine shows the pipe data structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbEhcdPipeShow
    (
    pUSB_EHCD_DATA          pHCDData,
    pUSB_EHCD_PIPE          pHCDPipe
    )
    {
    /* To hold the pointer to the request information */

    pUSB_EHCD_REQUEST_INFO pRequest;

    /* To hold the pointer to the next request information */

    pUSB_EHCD_REQUEST_INFO pNextRequest;

    /* Indicate the tail request of the active list is reached */

    BOOLEAN                bTail = FALSE;

    printf("\npHCDPipe %p settings:\n"
        "uAddress           %u\n"
        "uEndpointAddress   %u\n"
        "uEndpointType      %u\n"
        "uEndpointDir       %u\n"
        "uMaximumPacketSize %u\n"
        "uMaxTransferSize   %lu\n"
        "uMaxNumReqests     %lu\n"
        "bIsHalted          %u\n",
        pHCDPipe,
        pHCDPipe->uAddress,
        pHCDPipe->uEndpointAddress,
        pHCDPipe->uEndpointType,
        pHCDPipe->uEndpointDir,
        pHCDPipe->uMaximumPacketSize,
        pHCDPipe->uMaxTransferSize,
        pHCDPipe->uMaxNumReqests,
        pHCDPipe->bIsHalted);

    printf("\n========================================\n");

    if (pHCDPipe->pQH != NULL)
        {
        usbEhcdQHShow(pHCDData, pHCDPipe, pHCDPipe->pQH);
        }

    /* Start from the head of the active request list */

    pRequest = pHCDPipe->pRequestQueueHead;

    while (pRequest != NULL)
        {
        /*
         * Save the next active reuqest on the active request
         * list of this pipe so that we can safely move to the
         * next request once this request has been processed.
         */

        pNextRequest = pRequest->pNext;

        /*
         * If this request is already the tail of the pipe's
         * active request list, then we can break out once
         * this request is processed.
         */

        if (pRequest == pHCDPipe->pRequestQueueTail)
            bTail = TRUE;

        /* Check the request for completion */

        if (pHCDPipe->uEndpointType != USBHST_ISOCHRONOUS_TRANSFER)
            {
            printf("Scan Non Isoc Request %p\n",pRequest);

            usbEhcdNonIsochRequestShow(pHCDData, pHCDPipe, pRequest);
            }
        else
            {
            printf("Scan Isoc Request     %p\n",pRequest);

            usbEhcdIsochRequestShow(pHCDData, pHCDPipe, pRequest);
            }

        /*
         * If this request is already the tail of the pipe's
         * active request list, then we can break out.
         */

        if (bTail == TRUE)
            break;

        /* Go to the next request */

        pRequest = pNextRequest;
        }

    return OK;
    }

/*******************************************************************************
*
* usbEhcdPipeListShow - show the list of pipe structures
*
* This routine shows the list of pipe structures
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: Number of pipes shown on this list
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL UINT32 usbEhcdPipeListShow
    (
    pUSB_EHCD_DATA          pHCDData,
    pUSB_EHCD_PIPE          pHCDPipe,
    char *                  strDesc,
    BOOLEAN                 bUseAltNext
    )
    {
    UINT32                  uPipeCount;
    pUSB_EHCD_PIPE          pSavedHCDPipe;

    /* Reset to 0 */

    uPipeCount = 0;

    /* Save the head */

    pSavedHCDPipe = pHCDPipe;

    printf("\n==============================\n");

    printf("\n%s pipe list %p\n", strDesc, pHCDPipe);

    /*
     * Work when the pipe is valid
     */

    while (pHCDPipe != NULL)
        {
        /* Increase the pipe count scanned */

        uPipeCount++;

        /* Scan the pipe */

        usbEhcdPipeShow(pHCDData, pHCDPipe);

        if (bUseAltNext == TRUE)
            {
            /* Go to the next pipe */

            pHCDPipe = pHCDPipe->pAltNext;

            }
        else
            {
            /* Go to the next pipe */

            pHCDPipe = pHCDPipe->pNext;

            if (((pSavedHCDPipe->uEndpointType == USBHST_CONTROL_TRANSFER) ||
                 (pSavedHCDPipe->uEndpointType == USBHST_BULK_TRANSFER)) &&
                 (pSavedHCDPipe == pHCDPipe))
                {
                break;
                }
            }
        }

    printf("\n%s pipe count %d\n", strDesc, uPipeCount);

    printf("\n##############################\n");

    /* return the count */

    return uPipeCount;
    }

/*******************************************************************************
*
* usbEhcdRegShow - show the EHCI controller registers
*
* This routine shows the EHCI controller registers.
*
* <uIndex> - the EHCI host controller index
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbEhcdRegShow
    (
    UINT32         uIndex
    )
    {
    /* Pointer to the HCDData */

    pUSB_EHCD_DATA pHCDData;

    /* The register value */

    UINT32 uReg32;

    /* The port index */

    UINT32 uPortIndex;

    /* Check the index to be in correct range */

    if (uIndex >= USB_MAX_EHCI_COUNT)
        {
        printf("Index %d too big\n",uIndex);

        return;
        }

    /* Check if there is a HC here */

    pHCDData = g_pEHCDData[uIndex];

    if (pHCDData == NULL)
        {
        printf("Index %d has no HC\n",uIndex);

        return;
        }

    printf("\nThe EHCI registers are:\n\n");

    printf("BASEADDR------------%lx\n",
        pHCDData->regBase);
    printf("CAPLENADDR----------%lx\n",
        pHCDData->regBase + USB_EHCD_CAPLENGTH );
    printf("USBCMDADDR----------%lx\n",
        (pHCDData->regBase + pHCDData->capLenOffset));

    uReg32 = USB_EHCD_READ_CAPLENGTH(pHCDData);
    printf("CAPLENGTH-----------%08x\n",uReg32);

    uReg32 = USB_EHCD_READ_HCSPARAMS(pHCDData);
    printf("HCSPARAMS-----------%08x\n",uReg32);

    uReg32 = USB_EHCD_READ_HCCPARAMS(pHCDData);
    printf("HCCPARAMS-----------%08x\n",uReg32);

    uReg32 = USB_EHCD_READ_REG(pHCDData, USB_EHCD_USBCMD);
    printf("USBCMD--------------%08x\n",uReg32);

    uReg32 = USB_EHCD_READ_REG(pHCDData, USB_EHCD_USBSTS);
    printf("USBSTS--------------%08x\n",uReg32);

    uReg32 = USB_EHCD_READ_REG(pHCDData, USB_EHCD_USBINTR);
    printf("USBINTR-------------%08x\n",uReg32);

    uReg32 = USB_EHCD_READ_REG(pHCDData, USB_EHCD_FRINDEX);
    printf("FRINDEX-------------%08x\n",uReg32);

    uReg32 = USB_EHCD_READ_REG(pHCDData, USB_EHCD_CTRLDSSEGMENT);
    printf("CTRLDSSEGMENT-------%08x\n",uReg32);

    uReg32 = USB_EHCD_READ_REG(pHCDData, USB_EHCD_PERIODICLISTBASE);
    printf("PERIODICLISTBASE----%08x\n",uReg32);

    uReg32 = USB_EHCD_READ_REG(pHCDData, USB_EHCD_ASYNCLISTADDR);
    printf("ASYNCLISTADDR-------%08x\n",uReg32);

    uReg32 = USB_EHCD_READ_REG(pHCDData, USB_EHCD_CONFIGFLAG);
    printf("CONFIGFLAG----------%08x\n",uReg32);

    /* Show all ports */

    for (uPortIndex = 0;
         uPortIndex < pHCDData->RHData.uNumDownstreamPorts;
         uPortIndex++)
        {
        uReg32 = USB_EHCD_READ_REG(pHCDData, USB_EHCD_PORTSC(uPortIndex));
        printf("PORTSC%d-------------%08x\n",uPortIndex, uReg32);
        }

    printf("This EHCI addressMode64 = %d\n", pHCDData->addressMode64);

    printf("\n");
    }

/*******************************************************************************
*
* usbEhcdRegShow - show the EHCI controller registers and data structures
*
* This routine shows the EHCI controller registers and data structures.
*
* <uIndex> - the EHCI host controller index
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbEhcdShow
    (
    UINT32         uIndex
    )
    {
    /* Pointer to the HCDData */

    pUSB_EHCD_DATA pHCDData;

    /* Total scanned pipe count */

    UINT32         uTotalPipeCount = 0;

    /* Check the index to be in correct range */

    if (uIndex >= USB_MAX_EHCI_COUNT)
        {
        printf("Index %d too big\n",uIndex);

        return ERROR;
        }

    /* Check if there is a HC here */

    pHCDData = g_pEHCDData[uIndex];

    if (pHCDData == NULL)
        {
        printf("Index %d has no HC\n",uIndex);

        return ERROR;
        }

    /* Show all regisrters */

    usbEhcdRegShow(uIndex);

    /* Exclusively access the active pipe list */

    OS_WAIT_FOR_EVENT(pHCDData->ActivePipeListSynchEventID, OS_WAIT_INFINITE);

    /*
     * It is implemented that during the period the ActivePipeListSynchEventID
     * is taken, there is no modification done to these pipe lists.
     * If during the time of processing there is any pipe creation or deletion,
     * it is delayed until the processing is done.
     */

    /* Scan through the ISOC pipe list */

    uTotalPipeCount += usbEhcdPipeListShow(pHCDData,
                            pHCDData->pIsochPipeList,
                            "ISOC",FALSE);


    /* Scan through the INTER pipe list */

    uTotalPipeCount += usbEhcdPipeListShow(pHCDData,
                            pHCDData->pInterPipeList,
                            "INTER",FALSE);


    /*
     * Scan through the ASYNC pipe list.
     * pDefaultPipe and pAsynchTailPipe form a circular ASYNC pipe list.
     */

    uTotalPipeCount += usbEhcdPipeListShow(pHCDData,
                            pHCDData->pDefaultPipe,
                            "ASYNC",FALSE);

    /* Release the synchronization event */

    OS_RELEASE_EVENT(pHCDData->ActivePipeListSynchEventID);

    /* Exclusively access the reclamation list */

    OS_WAIT_FOR_EVENT(pHCDData->ReclamationListSynchEventID, OS_WAIT_INFINITE);

    /* Scan through the PeriodicReclamation pipe list */

    uTotalPipeCount += usbEhcdPipeListShow(pHCDData,
                            pHCDData->pPeriodicReclamationListHead,
                            "PeriodicReclamation",FALSE);

    /* Scan through the AsynchReclamation pipe list */

    uTotalPipeCount += usbEhcdPipeListShow(pHCDData,
                            pHCDData->pAsynchReclamationListHead,
                            "AsynchReclamation",FALSE);

    /* Scan through the DelayedPipeAddition pipe list */

    uTotalPipeCount += usbEhcdPipeListShow(pHCDData,
                            pHCDData->pDelayedPipeAdditionList,
                            "DelayedPipeAddition",TRUE);

    /* Scan through the DelayedPipeRemoval pipe list */

    uTotalPipeCount += usbEhcdPipeListShow(pHCDData,
                            pHCDData->pDelayedPipeRemovalList,
                            "DelayedPipeRemoval",TRUE);

    printf("\nTotal %d pipes for this HC\n", uTotalPipeCount);

    /* Release the synchronization event */

    OS_RELEASE_EVENT(pHCDData->ReclamationListSynchEventID);

    return OK;
    }/* End of usbEhcdShow() */

#endif /* USB_EHCD_SHOW */

