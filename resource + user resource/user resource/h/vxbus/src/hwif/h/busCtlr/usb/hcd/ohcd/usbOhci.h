/* usbOhci.h - OHCI host controller driver interface definition */

/*
 * Copyright (c) 2003-2010 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2003-2010 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
modification history
--------------------
01o,23aug10,m_y  Update prototype for EHCI/OHCI/UHCI init routine
                 (WIND00229662)
01n,18may10,m_y  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
01m,13jan10,ghs  vxWorks 6.9 LP64 adapting
01l,11Apr08,s_z  Redefine USB_OHCD_SWAP_BUFDATA for warning massage
01k,26sep07,ami  Fix CQ:WIND00102715 (check added for address conversion macro)
01j,24aug07,ami  Non-PCI USB Support Added
01i,07aug07,jrp  Removing reference to usbVxbRegAccess.h
01h,30mar07,sup  changes for the read/write routine to additional parameter
                 flag
01g,07oct06,ami  Changes for USB-vxBus porting
01f,28mar05,pdg  non-pci changes
01e,25feb05,mta  SPR 106276
01d,18aug04,hch  remove the DMA_MALLOC and DMA_FREE macro
01c,16jul04,hch  adding OHCI PCI class definition
01b,26jun03,nld  Changing the code to WRS standards
01a,17mar03,ssh  Initial Version
*/

/*
DESCRIPTION

This file contains the constants, data structures and functions exposed by the
OHCI host controller driver.
*/

/*
INTERNAL
 *******************************************************************************
 * Filename         : OHCI.h
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      : This file contains the constants, data structures and
 *                    functions exposed by the OHCI host controller driver.
 *
 *
 ******************************************************************************/

/* includes */

#include <usb/usbHcdInstr.h>
#include <cacheLib.h>
#include <../src/hwif/h/vxbus/vxbAccess.h>
#include <hwif/vxbus/vxBus.h>

#ifndef __INCusbOhcih
#define __INCusbOhcih

#ifdef  __cplusplus
extern "C" {
#endif

/* defines */
#define USB_OHCI_INTERRUPT_MASK                                 0x80000042
#define USB_OHCI_DEVICE_DEFAULT_STATE                           0x00
#define USB_OHCI_DEVICE_CONFIGURED_STATE                        0x02
/* To hold the speed of the device */

/* To hold the value for the full speed devices */

#define USB_OHCI_DEVICE_FULL_SPEED          0x00

/* To hold the value for the low speed devices */

#define USB_OHCI_DEVICE_LOW_SPEED           0x01

/* Macro used for swapping the 32 bit values */

#define USB_OHCD_SWAP_DATA(INDEX,VALUE)                                        \
    ((((&g_OHCDData[INDEX])->pDataSwap) == NULL) ? ((UINT32)(ULONG)(VALUE)):  \
        ((*((&g_OHCDData[INDEX])->pDataSwap))((UINT32)(ULONG)(VALUE))))

#define USB_OHCI_BUS_ADDR_LO32(VALUE)    \
                    ((UINT32)((UINT64)(VALUE)))

#define USB_OHCI_BUS_ADDR_HI32(VALUE)    \
                    ((UINT32)((UINT64)(VALUE) >> 32))

#define USB_OHCI_DESC_LO32(DESC)                                             \
    ((DESC != NULL)?                                                         \
    (USB_OHCI_BUS_ADDR_LO32((DESC)->dmaMapId->fragList[0].frag)): 0)         \

#define USB_OHCI_DESC_HI32(DESC)                                             \
        ((DESC != NULL)?                                                     \
        (USB_OHCI_BUS_ADDR_HI32((DESC)->dmaMapId->fragList[0].frag)): 0)     \


/* function declarations */

/*******************************************************************************
 * Function Name  : usbOhcdInit
 * Description    : Function to initialise the OHCI Host Controller Driver.
 *                  This function detects the number of OHCI Controllers
 *                  present on the system and initializes all the OHCI
 *                  controllers.
 * Parameters     : None.
 * Return Type    : Returns OK if the OHCI Host Controllers are initialized
 *                  successfully. Else returns ERROR.
 ******************************************************************************/

STATUS usbOhcdInit (void);

/*******************************************************************************
 * Function Name  : usbOhciExit
 * Description    : Function to uninitialise the OHCI Host Controller Driver.
 * Parameters     : None.
 * Return Type    : Returns TRUE if all the OHCI Controllers are reset and
 *                  the cleanup is successful. Else returns FALSE.
 ******************************************************************************/

STATUS usbOhcdExit (void);

#ifdef  __cplusplus
}
#endif

#endif  /* __INCusbOhcih */

/* End of file usbOhci.h */
