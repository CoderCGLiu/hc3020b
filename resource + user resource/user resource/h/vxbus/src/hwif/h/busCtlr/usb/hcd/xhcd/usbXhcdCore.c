/* usbXhcdUtil.c - USB XHCI Driver Core Routines */

/*
 * Copyright (c) 2011-2014 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01n,04Dec14,wyy  Clean compiler warning
01m,10Jul13,wyy  Make usbd layer to uniformly handle ClearTTBuffer request
                 (WIND00424927)
01l,03may13,wyy  Remove compiler warning (WIND00356717)
01k,17oct12,w_x  Fix compiler warnings (WIND00370525)
01j,16oct12,j_x  Add device speed parameter to usbXhcdDeviceGet() and
                 usbXhcdHubDeviceUpdate() (WIND00381845)
01i,15otc12,w_x  Fix some headset device mount issue (evaluate EP) (WIND00381857)
01h,09oct12,j_x  Release pipeMutex before usbXhcdQueueStopEndpointCmd (WIND00379228)
01g,26sep12,j_x  Return all active requests in pipe deletion function (WIND00378761)
01f,21sep12,w_x  Release request structure for SetAddress() (WIND00371201)
01e,20sep12,w_x  Use separate default pipe for USB2 and USB3 bus (WIND00377413)
01d,12sep12,j_x  Change the include file (WIND00375618)
01c,04sep12,w_x  Address review comments and fix some defects (WIND00370637)
01b,26aug12,w_x  Properly sync data transfer buffer (WIND00372215)
01a,16may11,w_x  written
*/

#include "usbXhcdUtil.h"

/*******************************************************************************
*
* usbXhcdGetFrameNumber - get the current frame number of the bus
*
* This routine is used to get the current frame number of the host controller.
* <uBusIndex> specifies the host controller bus index. <puFrameNumber> is a
* pointer to a variable to hold the current frame number.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the frame number was obtained successfully.
*   USBHST_INVALID_PARAMETER - Returned if the parameters are not valid.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbXhcdGetFrameNumber
    (
    UINT8   uBusIndex,    /* Index of the host controller */
    UINT16 *puFrameNumber /* Pointer to the variable to hold the frame number */
    )
    {
    /* To hold the pointer to the data structure */

    pUSB_XHCD_DATA          pHCDData;

    /* The value returned in the MFINDEX register */

    UINT32                  uReg32;

    /* Extract the global data structure */

    pHCDData = gpXHCDData[uBusIndex];

    /* Assert if the global pointer is not valid */

    if (NULL == pHCDData)
        {
        USB_XHCD_ERR("usbXhcdGetFrameNumber - pHCDData NULL\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Read the MFINDEX register */

    uReg32 = USB_XHCD_READ_RT_REG32(pHCDData, USB_XHCI_MFINDEX);

    /* Get the 1ms frame time */

    *puFrameNumber = (UINT16)USB_XHCI_MFINDEX_FRAME_GET(uReg32);

    return USBHST_SUCCESS;
    }

/*******************************************************************************
*
* usbXhcdSetBitRate - modify the frame width
*
* This routine is used to modify the frame width of the host controller.
* <uBusIndex> specifies the host controller bus index. <bIncrement> is a flag
* to specify whether the frame number should be incremented or decremented.
* If TRUE, the frame number will be incremented, otherwise, decremented.
* <puCurrentFrameWidth> is a pointer to hold the current frame width(after
* modification)
*
* RETURNS: USBHST_SUCCESS - Returned as the functionality is not supported.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbXhcdSetBitRate
    (
    UINT8   uBusIndex,          /* Index of the host controller       */
    BOOL    bIncrement,         /* Flag for increment or decrement    */
    UINT32 *puCurrentFrameWidth /* Pointer to the current frame width */
    )
    {
    return USBHST_SUCCESS;
    }

/*******************************************************************************
*
* usbXhcdIsBandwidthAvailable - check the bandwidth availability
*
* This routine is used to check whether there is enough bandwidth to support
* the new configuration or an alternate interface setting of an interface.
*
* <uBusIndex> specifies the host controller bus index.
*
* <uDeviceAddress> specifies the device address.
*
* <uDeviceSpeed> is the speed of the device which needs to be modified.
*
* <pCurrentDescriptor> is the pointer to the current configuration or
* interface descriptor. If the pNewDescriptor corresponds to a USB
* configuration descriptor, this parameter is ignored (i.e. this parameter
* can be NULL).
*
* <pNewDescriptor> is the pointer to the new configuration or interface
* descriptor.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the new configuration or
*       new alternate interface can be supported
*   USBHST_INVALID_PARAMETER - Returned if the parameters are
*       not valid.
*   USBHST_INSUFFICIENT_RESOURCE - Returned if the resources are
*       not available.
*   USBHST_INSUFFICIENT_BANDWIDTH - Returned if bandwidth is
*       not available.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbXhcdIsBandwidthAvailable
    (
    UINT8   uBusIndex,            /* Host controller bus index */
    UINT8   uDeviceAddress,       /* Handle to the device addr */
    UINT8   uDeviceSpeed,         /* Speed of the device in default state */
    UCHAR * pCurrentDescriptor,   /* Ptr to current configuration */
    UCHAR * pNewDescriptor        /* Ptr to new configuration */
    )
    {
    /* Pointer to the descriptor header */

    pUSBHST_DESCRIPTOR_HEADER   pDescHeader = NULL;

    /* Pointer to the HCD specific data structure */

    pUSB_XHCD_DATA              pHCDData = NULL;

    /* Pointer to the Device structure */

    pUSB_XHCD_DEVICE            pHCDDevice = NULL;

    /* Pointer to the Pipe structure */

    pUSB_XHCD_PIPE              pHCDPipe = NULL;

    /* The status of the request */

    USBHST_STATUS               Status = USBHST_FAILURE;

    /* Device Endpoint Context Index */

    UINT8                       uIdx2 = 0;

    /* Input Control Context */

    pUSB_XHCI_INPUT_CTRL_CTX    pInputCtrlCtx;

    /* Input Slot Context */

    pUSB_XHCI_SLOT_CTX          pInputSlotCtx;

    /* Check the validity of the parameters */

    if ((0 == uDeviceAddress) ||
        (NULL == pNewDescriptor))
        {
        USB_XHCD_ERR("Parameters are not valid\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the global data structure */

    pHCDData = gpXHCDData[uBusIndex];

    /* Assert if the global pointer is not valid */

    if (NULL == pHCDData)
        {
        USB_XHCD_ERR("usbXhcdIsBandwidthAvailable - pHCDData NULL\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* If this request is for the Root hub, return success */

    if ((uDeviceAddress == pHCDData->USB2RH.uDeviceAddress) ||
        (uDeviceAddress == pHCDData->USB3RH.uDeviceAddress))
        {
        USB_XHCD_WARN("usbXhcdIsBandwidthAvailable - "
                      "Root hub device does not need to check bandwidth\n",
                      1, 2, 3, 4, 5 ,6);

        return USBHST_SUCCESS;
        }

    /*
     * Upon here, we should already have a device structure
     * created for that device during the initial steps!
     */

    pHCDDevice = usbXhcdDeviceGet(pHCDData, uDeviceAddress, uDeviceSpeed);

    if (pHCDDevice == NULL)
        {
        USB_XHCD_ERR("usbXhcdIsBandwidthAvailable - "
                     "usbXhcdDeviceGet for dev %d fail\n",
                     uDeviceAddress, 2, 3, 4, 5 ,6);

        return USBHST_INSUFFICIENT_RESOURCE;
        }

    /* Extract the descriptor header of the new descriptor */

    pDescHeader = (pUSBHST_DESCRIPTOR_HEADER)pNewDescriptor;

    /* Check if it is an interface descriptor */

    if (pDescHeader->uDescriptorType == USBHST_INTERFACE_DESC)
        {
        USB_XHCD_DBG("usbXhcdIsBandwidthAvailable - USBHST_INTERFACE_DESC\n",
                      1, 2, 3, 4, 5 ,6);
        /*
         * In the case of setting an alternate interface setting,
         * it may be setting an existing anternate setting which
         * is already covered in the current configuration, or it
         * may be changing to another alternate setting. We need
         * to make sure the unused endpoints finally get dropped.
         */

        if (usbXhcdInterfaceScan(pHCDData,
                                 pHCDDevice,
                                 (pUSBHST_INTERFACE_DESCRIPTOR)pNewDescriptor) != OK)
            {
            USB_XHCD_ERR("usbXhcdIsBandwidthAvailable - USBHST_INTERFACE_DESC - "
                         "usbXhcdInterfaceScan failed\n",
                          1, 2, 3, 4, 5 ,6);

            return USBHST_INSUFFICIENT_RESOURCE;
            }
        }
    else if (pDescHeader->uDescriptorType == USBHST_CONFIG_DESC)
        {
        /* Pointer to the new configuration descriptor */

        pUSBHST_CONFIG_DESCRIPTOR pNewConfigDesc = NULL;

        /* Pointer to the interface descriptor */

        pUSBHST_INTERFACE_DESCRIPTOR pIfDesc = NULL;

        /* Total # of interfaces present in the configuration */

        UINT8 bNumInterfaces = 0;

        /* Inferface index */

        UINT8 uIfIndex = 0;

        /* Configuration data length */

        UINT16 wTotalLength = 0;

        /* Current scanned config buffer size */

        UINT16 uCurrentSize = 0;

        /* Interface Array */

        UINT8 uInterfaces[USB_XHCI_MAX_INTERFACES] = {0};

        USB_XHCD_DBG("usbXhcdIsBandwidthAvailable - USBHST_CONFIG_DESC\n",
                      1, 2, 3, 4, 5 ,6);

        /*
         * In case of trying to set another cofiguration,
         * the current configuration may have interfaces
         * with endpoints that are not used in the new
         * configuration, so we have to drop these endpoints.
         *
         * Initially mark all existing pipes to be deleted;
         * If the following scan found a same endpoint in
         * use, we will mark it to be modified; othwise, it
         * will be left as "to be deleted" and then finally
         * get dropped. Note that EP ID (DCI) starts from 1
         * which represents the Default Control Endpoint, but
         * we should always keep it, so we try to drop from
         * DCI 2.
         */

        for (uIdx2 = 2; uIdx2 < USB_XHCI_MAX_DEV_PIPES; uIdx2++)
            {
            if (pHCDDevice->pDevPipes[uIdx2] == NULL)
                continue;

            pHCDDevice->pDevPipes[uIdx2]->uEpCtxState =
                USB_XHCD_EP_CTX_STATE_DEL;
            }

        /* Get the new configuration descriptor */

        pNewConfigDesc = (pUSBHST_CONFIG_DESCRIPTOR)pNewDescriptor;

        /* Get the total config descriptor length */

        wTotalLength = OS_UINT16_LE_TO_CPU(pNewConfigDesc->wTotalLength);

        /* Get the total no of endpoint for this interface */

        bNumInterfaces = pNewConfigDesc->bNumInterfaces;

        /* Start from the first interface */

        pIfDesc = (pUSBHST_INTERFACE_DESCRIPTOR)
            ((UINT8 *)(pNewConfigDesc) + pNewConfigDesc->bLength);

        /* Add to the scanned size */

        uCurrentSize = (UINT16)(uCurrentSize + pNewConfigDesc->bLength);

        /* for (uIfIndex = 0; uIfIndex < bNumInterfaces; uIfIndex++) */

        while (uCurrentSize < wTotalLength)
            {
            /* Go to the next Interface descriptor */

            while (pIfDesc->bDescriptorType != USBHST_INTERFACE_DESC)
                {
                pIfDesc = (pUSBHST_INTERFACE_DESCRIPTOR)
                    ((UINT8 *)(pIfDesc) + pIfDesc->bLength);

                /* If the buffer is incorrect, break out! */

                if (pIfDesc->bLength == 0)
                    {
                    pIfDesc = NULL;

                    break;
                    }

                /* Add to the scanned size */

                uCurrentSize = (UINT16)(uCurrentSize + pIfDesc->bLength);

                /* If the scanned size exceeds total size, break out! */

                if (uCurrentSize >= wTotalLength)
                    {
                    pIfDesc = NULL;

                    break;
                    }
                }

            if (pIfDesc == NULL)
                break;

            /* Get the interface number */

            uIfIndex = pIfDesc->bInterfaceNumber;

            /*
             * Check if this is the 1st Interface descriptor with endpoints.
             *
             * Some classes, for example the USB audio class, will have some
             * interfaces without endpoints, these interfaces will not comsume
             * any bandwidth and will not have any pipe created for them.
             */

            if ((uInterfaces[uIfIndex] == 0) &&
                (pIfDesc->bNumEndpoints != 0))
                {
                /* Set this interface number as already scanned */

                uInterfaces[uIfIndex] = pIfDesc->bNumEndpoints;

                if (usbXhcdInterfaceScan(pHCDData,
                                         pHCDDevice,
                                         pIfDesc) != OK)
                    {
                    USB_XHCD_ERR("usbXhcdIsBandwidthAvailable - USBHST_CONFIG_DESC - "
                                 "usbXhcdInterfaceScan for interface %d failed\n",
                                  pIfDesc->bInterfaceNumber, 2, 3, 4, 5 ,6);

                    return USBHST_INSUFFICIENT_RESOURCE;
                    }
                }

            /* Go to the next Interface Descriptor */

            pIfDesc = (pUSBHST_INTERFACE_DESCRIPTOR)
                ((UINT8 *)(pIfDesc) + pIfDesc->bLength);

            /* Add to the scanned size */

            uCurrentSize = (UINT16)(uCurrentSize + pIfDesc->bLength);
            }

        /* Save the number of interfaces in the current configuration */

        pHCDDevice->uNumInterfaces = bNumInterfaces;
        }
    else
        {
        USB_XHCD_ERR("Descriptor type 0x%x is not valid\n",
            pDescHeader->uDescriptorType, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /*
     * The Add Context flag A1 and Drop Context flags D0 and D1
     * of the Input Control Context (in the Input Context) shall
     * be cleared to '0'. A0 shall be set to '1'.
     */

    pHCDDevice->uAddFlags |= USB_XHCI_ADD_EP(0);
    pHCDDevice->uAddFlags &= ~(USB_XHCI_ADD_EP(1));
    pHCDDevice->uDropFlags &= ~(USB_XHCI_DROP_EP(1) | USB_XHCI_DROP_EP(0));

    /*
     * Now its time to update the Endpoint Contexts with new or
     * changed values. Endpoint 0 Context does not apply to the
     * Configure Endpoint Command and shall be ignored by the xHC,
     * so we start from EP ID 2.
     */

    for (uIdx2 = 2; uIdx2 < USB_XHCI_MAX_DEV_PIPES; uIdx2++)
        {
        if (pHCDDevice->pDevPipes[uIdx2] == NULL)
            continue;

        pHCDPipe = pHCDDevice->pDevPipes[uIdx2];

        USB_XHCD_WARN("usbXhcdIsBandwidthAvailable - Check EP CTX %d - "
                      "dev %d EP ID %d ep addr 0x%02x ep ctx state %d\n",
                      uIdx2,
                      pHCDDevice->uDevAddr,
                      pHCDPipe->uEpId,
                      pHCDPipe->uEpAddr,
                      pHCDPipe->uEpCtxState, 6);

        /* If this pipe is to be dropped, set corresponding "Drop" bit */

        if (pHCDPipe->uEpCtxState == USB_XHCD_EP_CTX_STATE_DEL)
            {
            pUSB_XHCI_EP_CTX    pEpCtx = NULL;

            USB_XHCD_WARN("usbXhcdIsBandwidthAvailable - DEL EP CTX %d - "
                          "dev %d EP ID %d ep addr 0x%02x ep ctx state %d\n",
                          uIdx2,
                          pHCDDevice->uDevAddr,
                          pHCDPipe->uEpId,
                          pHCDPipe->uEpAddr,
                          pHCDPipe->uEpCtxState, 6);


            pEpCtx = USB_XHCI_EP_CTX_CAST(pHCDData,
                                          pHCDDevice->pIntputCtx,
                                          pHCDPipe->uEpId);

            pEpCtx->uEpInfo0 = 0;

            pEpCtx->uEpInfo1 = 0;

            pEpCtx->uEpDeqHi = 0;

            pEpCtx->uEpDeqLo = 0;

            pEpCtx->uEpInfo2 = 0;

            /* Drop this Endpoint */

            pHCDDevice->uDropFlags |= USB_XHCI_DROP_EP(pHCDPipe->uEpId);
            pHCDDevice->uAddFlags &= ~(USB_XHCI_ADD_EP(pHCDPipe->uEpId));

            if (usbXhcdPipeDestroy(pHCDData, pHCDPipe) != OK)
                {
                USB_XHCD_WARN("usbXhcdIsBandwidthAvailable - destroy pipe fail\n",
                    0, 0, 0, 0, 0, 0);
                }

            pHCDDevice->pDevPipes[uIdx2] = NULL;

            continue;
            }

        /*
         * Save "Context Entries", which identifies the index of
         * the last valid Endpoint Context within the Device
         * Context structure.
         */

        pHCDDevice->uContextEntries = uIdx2;

        /* If it is not to be deleted, we need to update its context */

        if ((pHCDPipe->uEpCtxState == USB_XHCD_EP_CTX_STATE_NEW) ||
            (pHCDPipe->uEpCtxState == USB_XHCD_EP_CTX_STATE_MOD))
            {
            USB_XHCD_WARN("usbXhcdIsBandwidthAvailable - Commit %s EP CTX %d - "
                          "dev %d EP ID %d ep addr 0x%02x ep ctx state %d\n",
                          (pHCDPipe->uEpCtxState == USB_XHCD_EP_CTX_STATE_NEW)?
                          "NEW" : "MOD",
                          uIdx2,
                          pHCDDevice->uDevAddr,
                          pHCDPipe->uEpId,
                          pHCDPipe->uEpAddr,
                          pHCDPipe->uEpCtxState);

            (void) usbXhcdEpCtxCommit(pHCDData, pHCDDevice, pHCDPipe);
            }
        else
            {
            USB_XHCD_WARN("usbXhcdIsBandwidthAvailable - KEEP OLD EP CTX %d - "
                          "dev %d EP ID %d ep addr 0x%02x ep ctx state %d\n",
                          uIdx2,
                          pHCDDevice->uDevAddr,
                          pHCDPipe->uEpId,
                          pHCDPipe->uEpAddr,
                          pHCDPipe->uEpCtxState, 6);

            /*
             * Keep this Endpoint by not setting the Drop flag and also
             * clearing the Add flag.
             *
             * The xHCI spec indicates that for a unchanged Endpoint which
             * has been enabled before, we can just keep both the Add and
             * Drop flag bits cleared. xHC behavior is undefined if the Drop
             * flag is '0', the Add flag is '1' and the Output Endpoint
             * Context is not in the Disabled state (i.e. software is trying
             * to add an endpoint without dropping its current resources).
             * For example, the NEC uPD720200 chip may trigger a "TRB Error"
             * for the Configure Endpoint Command if we have an OLD Endpoint
             * but not clearing the Add flag.
             */

            pHCDDevice->uAddFlags &= ~(USB_XHCI_ADD_EP(pHCDPipe->uEpId));
            }
        }

    /* Update Input Control Context with new "Add" and "Drop" flags */

    pInputCtrlCtx = USB_XHCI_INPUT_CTRL_CTX_CAST(pHCDData,
                        pHCDDevice->pIntputCtx);

    pInputCtrlCtx->uAddFlags = USB_XHCD_SWAP_DESC_DATA32(pHCDData,
                               pHCDDevice->uAddFlags);
    pInputCtrlCtx->uDropFlags = USB_XHCD_SWAP_DESC_DATA32(pHCDData,
                               pHCDDevice->uDropFlags);

    /* Update Input Slot Context with new "Context Entries" */

    pInputSlotCtx = USB_XHCI_SLOT_CTX_CAST(pHCDData, pHCDDevice->pIntputCtx);
    pInputSlotCtx->uSlotInfo0 &= USB_XHCD_SWAP_DESC_DATA32(pHCDData,
                                (~(USB_XHCI_SLOT_CTX_ENTRIES_MASK |
                                   USB_XHCI_SLOT_CTX_HUB_MASK)));
    pInputSlotCtx->uSlotInfo0 |= USB_XHCD_SWAP_DESC_DATA32(pHCDData,
        (USB_XHCI_SLOT_CTX_ENTRIES(pHCDDevice->uContextEntries)));

    USB_XHCD_WARN("usbXhcdIsBandwidthAvailable - "
                  "Configure Endpoint for dev %d "
                  "uAddFlags 0x%08x uDropFlags 0x%08x uContextEntries %d\n",
                  pHCDDevice->uDevAddr,
                  pInputCtrlCtx->uAddFlags,
                  pInputCtrlCtx->uDropFlags,
                  pHCDDevice->uContextEntries, 5, 6);

    if (usbXhcdQueueConfigureEndpointCmd(pHCDData,
        pHCDDevice->pIntputCtx->uCtxBusAddr,
        pHCDDevice->uSlotId,
        0,
        USB_XHCD_DEFAULT_CMD_WAIT_TIME) != OK)
        {
        USB_XHCD_WARN("USBHST_INSUFFICIENT_BANDWIDTH - config\n",
            1, 2, 3, 4, 5, 6);

        Status = USBHST_INSUFFICIENT_BANDWIDTH;
        }
    else
        {
        Status = USBHST_SUCCESS;

        pHCDDevice->bIsConfiged = TRUE;
        }

    /* Reset the flags */

    pHCDDevice->uAddFlags = 0;
    pHCDDevice->uDropFlags = 0;

    return Status;
    }

/*******************************************************************************
*
* usbXhcdIsRequestPending - checks if a request is pending for a pipe
*
* This function is used to check whether any request is pending on a pipe.
* <uBusIndex> specifies the host controller bus index. <uPipeHandle> holds the
* pipe handle.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if a request is pending for the pipe.
*   USBHST_FAILURE - Returned if the request is not pending for the pipe.
*   USBHST_INVALID_PARAMETER - Returned if the parameters are not valid.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbXhcdIsRequestPending
    (
    UINT8  uBusIndex,   /* Index of the host controller */
    ULONG  uPipeHandle  /* Pipe handle */
    )
    {
    /* To hold the pointer to the data structure */

    pUSB_XHCD_DATA          pHCDData;
    pUSB_XHCD_PIPE          pHCDPipe;
    USBHST_STATUS           hstSts;

    /* Check the validity of the parameters */

    if (uPipeHandle == 0)
        {
        USB_XHCD_ERR("usbXhcdIsRequestPending - "
            "uBusIndex %d uPipeHandle %p\n",
            uBusIndex, uPipeHandle, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the global data structure */

    pHCDData = gpXHCDData[uBusIndex];

    /* Assert if the global pointer is not valid */

    if (NULL == pHCDData)
        {
        USB_XHCD_ERR("usbXhcdIsRequestPending - pHCDData NULL\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Cast to pipe structure */

    pHCDPipe = (pUSB_XHCD_PIPE)uPipeHandle;

    /* Exclusively access the pipe request list */

    (void) OS_WAIT_FOR_EVENT(pHCDPipe->pipeMutex, OS_WAIT_INFINITE);

    if (lstCount(&pHCDPipe->activeReqList) > 0)
        hstSts = USBHST_SUCCESS;
    else
        hstSts = USBHST_FAILURE;

    /* Release the exclusive access */

    OS_RELEASE_EVENT(pHCDPipe->pipeMutex);

    return hstSts;
    }

/*******************************************************************************
*
* usbXhcdCreatePipe - create a pipe specific to an endpoint
*
* This routine creates the host controller driver specific pipe data structure,
* <puPipeHandle> returns the pointer to the data structure created for the
* pipe. <uBusIndex> is the index of the host controller. <uDeviceAddress>
* is the address of the device holding the endpoint. <uDeviceSpeed> is the
* speed of the device holding the endpoint. <pEndpointDescriptor> is the
* pointer to the endpoint descriptor. <uHighSpeedHubInfo> is the high speed
* hub information.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the pipe was created successfully.
*   USBHST_INVALID_PARAMETER - Returned if the parameters are not valid.
*   USBHST_INSUFFICIENT_MEMORY - Returned if the memory allocation
*                                for the pipe failed.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbXhcdCreatePipe
    (
    UINT8   uBusIndex,          /* Host controller index      */
    UINT8   uDeviceAddress,     /* USB device address         */
    UINT8   uDeviceSpeed,       /* USB device speed           */
    UCHAR  *pEndpointDescriptor,/* Endpoint descriptor        */
    UINT16  uHighSpeedHubInfo,  /* High speed hub information */
    ULONG  *puPipeHandle        /* Pointer to the pipe handle */
    )
    {
    pUSB_XHCD_PIPE              pHCDPipe = NULL;
    pUSB_XHCD_DATA              pHCDData = NULL;
    pUSB_XHCD_DEVICE            pHCDDevice = NULL;
    USBHST_STATUS               Status = USBHST_FAILURE;
    pUSBHST_ENDPOINT_DESCRIPTOR pEpDesc = NULL;
    UINT8                       uEpXferType = 0;
    UINT8                       uEpId = 0;

    /* Parameters verification */

    if ((NULL == pEndpointDescriptor) ||
        (NULL == puPipeHandle))
        {
        USB_XHCD_ERR("usbXhcdCreatePipe - uBusIndex %d "
            "pEndpointDescriptor %p puPipeHandle %p\n",
            uBusIndex,
            pEndpointDescriptor,
            puPipeHandle, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the global data structure */

    pHCDData = gpXHCDData[uBusIndex];

    /* Return if the global pointer is not valid */

    if (NULL == pHCDData)
        {
        USB_XHCD_ERR("usbXhcdCreatePipe - pHCDData NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Check if the request is for the Root hub and route to it */

    if ((pHCDData->pDefaultUSB3Pipe != NULL) &&
        (pHCDData->pDefaultUSB2Pipe != NULL) &&
        ((uDeviceAddress == pHCDData->USB2RH.uDeviceAddress) ||
         (uDeviceAddress == pHCDData->USB3RH.uDeviceAddress)))
        {
        Status = usbXhcdRootHubCreatePipe(pHCDData,
                                          uDeviceAddress,
                                          uDeviceSpeed,
                                          pEndpointDescriptor,
                                          puPipeHandle);

        return Status;
        }

    /*
     * Try to create a working pipe for a Default state
     * device is not allowed, that should still be using
     * the HCD default pipe for initial communications!
     */

    if (uDeviceAddress == 0)
        {
        USB_XHCD_ERR("usbXhcdCreatePipe - "
                     "Device address is still 0, are you crazy?\n",
                     1, 2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /*
     * Upon here, we should already have a device structure
     * created for that device during the initial steps!
     */

    pHCDDevice = usbXhcdDeviceGet(pHCDData, uDeviceAddress, uDeviceSpeed);

    if (pHCDDevice == NULL)
        {
        USB_XHCD_ERR("usbXhcdCreatePipe - usbXhcdDeviceGet for dev %d fail\n",
                     uDeviceAddress, 2, 3, 4, 5 ,6);

        return USBHST_INSUFFICIENT_RESOURCE;
        }

    pEpDesc = (pUSBHST_ENDPOINT_DESCRIPTOR)pEndpointDescriptor;

    if (pEpDesc->bEndpointAddress != 0)
        {
        /*
         * Retrieve non-default pipe which was already created
         * during Configure Endpoint (in usbXhcdIsBandwidthAvailable)
         */

        uEpXferType = (UINT8)(pEpDesc->bmAttributes & USB_ATTR_EPTYPE_MASK);

        if (uEpXferType == USBHST_CONTROL_TRANSFER)
           {
           uEpId = (UINT8)USB_XHCI_CTRL_EP_CTX_DCI(pEpDesc->bEndpointAddress);
           }
        else
           {
           uEpId = (UINT8)USB_XHCI_NON_CTRL_EP_CTX_DCI(pEpDesc->bEndpointAddress);
           }

        if (pHCDDevice->pDevPipes[uEpId] == NULL)
            {
            USB_XHCD_ERR("usbXhcdPipeCreate - pipe not created for EP ID %d\n",
                          uEpId, 2, 3, 4, 5 ,6);

            return USBHST_FAILURE;
            }

        /* Get the new pipe */

        pHCDPipe = pHCDDevice->pDevPipes[uEpId];

        USB_XHCD_DBG("usbXhcdPipeCreate - Retrieved already created pipe %p\n",
                      pHCDPipe, 2, 3, 4, 5 ,6);
        }
    else
        {
        /*
         * Create the new default control pipe which is not created
         * during Configure Endpoint (in usbXhcdIsBandwidthAvailable)
         */

        pHCDPipe = usbXhcdPipeCreate(pHCDData, pHCDDevice, pEpDesc);

        if (pHCDPipe == NULL)
            {
            USB_XHCD_ERR("usbXhcdPipeCreate - create default control pipe failed\n",
                          1, 2, 3, 4, 5 ,6);

            return USBHST_INSUFFICIENT_MEMORY;
            }

        USB_XHCD_DBG("usbXhcdPipeCreate - Created default control pipe %p\n",
                      pHCDPipe, 2, 3, 4, 5 ,6);
        }

    pHCDPipe->uHubInfo = uHighSpeedHubInfo;

    /*
     * For the default control pipe, initially it was using
     * HCD default pipe to communicate, but now we have created
     * a new pipe for it, it is time to switch from the HCD
     * default pipe to the new pipe for latter communications!
     */

    if (pHCDPipe->uEpAddr == 0)
        {
        UINT64 uNewDeqPtr;
        STATUS uSts;

        if (usbXhcdSetupControlPipe(pHCDData, pHCDPipe) != OK)
            {
            USB_XHCD_ERR("usbXhcdCreatePipe - "
                         "usbXhcdSetupControlPipe for dev 0x%x fail\n",
                         uDeviceAddress, 2, 3, 4, 5 ,6);

            return USBHST_FAILURE;
            }

        USB_XHCD_DBG("usbXhcdCreatePipe - "
                     "Switch from default pipe to new pipe %p for dev 0x%x\n",
                     pHCDPipe, uDeviceAddress, 3, 4, 5 ,6);

        uSts = usbXhcdQueueStopEndpointCmd(pHCDData,
                                           pHCDDevice->uSlotId,
                                           pHCDPipe->uEpId,
                                           0,
                                           USB_XHCD_DEFAULT_CMD_WAIT_TIME);
        if (uSts != OK)
            {
            USB_XHCD_ERR("usbXhcdCreatePipe - "
                         "Stop default control pipe for dev 0x%x fail\n",
                         uDeviceAddress, 2, 3, 4, 5 ,6);

            return USBHST_FAILURE;
            }

        uNewDeqPtr = pHCDPipe->pEpRing->pDeqSeg->uHeadTRB;

        uSts = usbXhcdQueueSetTRDeqPtrCmd(pHCDData,
                                          uNewDeqPtr,
                                          pHCDDevice->uSlotId,
                                          pHCDPipe->uEpId,
                                          0,
                                          pHCDPipe->pEpRing->uCycle,
                                          0,
                                          USB_XHCD_DEFAULT_CMD_WAIT_TIME);

        if (uSts != OK)
            {
            USB_XHCD_ERR("usbXhcdCreatePipe - "
                         "could not set new Deq Ptr for ctrl EP for dev 0x%x\n",
                         uDeviceAddress, 2, 3, 4, 5 ,6);

            return USBHST_FAILURE;
            }

        /*
         * Override with new default control pipe. It was initially
         * set to point to the HCD default control pipe.
         */

        pHCDDevice->pDevPipes[pHCDPipe->uEpId] = pHCDPipe;
        }

    /* Return the newly created pipe */

    *puPipeHandle = (ULONG)pHCDPipe;

    return USBHST_SUCCESS;
    }

/*******************************************************************************
*
* usbXhcdDeletePipe - delete a pipe created already
*
* This routine is used to delete a pipe specific to an endpoint.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the pipe was deleted successfully
*   USBHST_INVALID_PARAMETER - Returned if the parameters are not valid.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbXhcdDeletePipe
    (
    UINT8   uBusIndex,   /* Index of the host controller     */
    ULONG   uPipeHandle  /* Handle of the pipe to be deleted */
    )
    {
    /* Pointer to the data structure */

    pUSB_XHCD_DATA          pHCDData;

    /* Pointer to the HCD maintained pipe */

    pUSB_XHCD_PIPE          pHCDPipe;

    /* Request node */

    NODE * pReqNode = NULL;

    /* Request information */

    pUSB_XHCD_REQUEST_INFO  pRequest = NULL;

    /* Pointer to the URB */

    pUSBHST_URB pURB;

    /* Check the validity of the parameters */

    if (0 == uPipeHandle)
        {
        USB_XHCD_ERR("usbXhcdDeletePipe - uBusIndex %d "
            "puPipeHandle %p\n",
            uBusIndex, 2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the global data structure */

    pHCDData = gpXHCDData[uBusIndex];

    /* Return if the global pointer is not valid */

    if (NULL == pHCDData)
        {
        USB_XHCD_ERR("usbXhcdDeletePipe - pHCDData NULL\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the USB_XHCD_PIPE data structure */

    pHCDPipe = (pUSB_XHCD_PIPE)uPipeHandle;

    /* Do not need to control root hub pipe */

    if (pHCDPipe->uDevAddr == USB_ROOT_HUB_ADDRESS)
        {
        USB_XHCD_DBG("usbXhcdDeletePipe - delete Root Hub Pipe!\n",
            0, 0, 0, 0, 0, 0);

        return usbXhcdRootHubDeletePipe(pHCDData, uPipeHandle);
        }

    /*
     * For normal pipes, we do not need to destroy the pipe here.
     *
     * This is because of the following reasons:
     *
     * 1) The usbXhcdIsBandwidthAvailable() is called during set
     * configuration or set interface alternate setting, during which
     * we will scan the interfaces; if a pipe is not created, we will
     * create it; if an alreayd created pipe is not used in the new
     * configuration or new interface alternate setting, we will then
     * destroy the pipe. However, the USBD will try to call this HCD
     * interface to descrory pipes during that process, which is then
     * redundant. The USBD will then try to create the pipe, which is
     * also redundant since usbXhcdIsBandwidthAvailable() has done that
     * already (we will not create the pipe again in usbXhcdCreatePipe).
     *
     * 2) When the device is to be removed, the pipes will be destroyed.
     * The USBD tries to call this HCD interface for each created pipe.
     * However, for xHCD, the USBD will follow up with a call to the device
     * control interface with USBHST_CMD_RELEASE_DEVICE, that is a perfect
     * time to delete all device resources, including the pipes. We do that
     * at that step, so in this interface, we just need to pretent we have
     * done the pipe deletion.
     */

    if (pHCDPipe->uValidMaggic != USB_XHCD_MAGIC_ALIVE)
        {
        return USBHST_SUCCESS;
        }

    /* Return all active requests */

    (void) OS_WAIT_FOR_EVENT(pHCDPipe->pipeMutex, OS_WAIT_INFINITE);

    while ((pReqNode = lstGet(&pHCDPipe->activeReqList)) != NULL)
        {
        /* Cast to the request info */

        pRequest = USB_XHCD_EP_NODE_TO_REQ_INFO(pReqNode);

        /* Save the URB pointer */

        pURB = pRequest->pURB;

        usbXhcdReleaseRequestInfo(pHCDData, pHCDPipe, pRequest);

        if ((pURB != NULL) && (pURB->pfCallback != NULL))
            {
            pURB->pfCallback(pURB);
            }
        }

    OS_RELEASE_EVENT(pHCDPipe->pipeMutex);

    return USBHST_SUCCESS;
    }

/*******************************************************************************
*
* usbXhcdModifyDefaultPipe - modify the default pipe characteristics
*
* This routine is used to modify the properties (device speed and maximum
* packet size) of the default pipe (address 0, endpoint 0). <uBusIndex>
* specifies the host controller bus index. <uDefaultPipeHandle> holds the
* pipe handle of the default pipe. <uDeviceSpeed> is the speed of the
* device which needs to be modified. <uMaxPacketSize> is the maximum packet
* size of the default pipe which needs to be modified. <uHighSpeedHubInfo>
* specifies the nearest high speed hub and the port number information. This
* information will be used to handle a split transfer to the full / low speed
* device. The high byte will hold the high speed hub address. The low byte
* will hold the port number to which the USB 1.1 device is connected.
*
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the deafult pipe properties were modified
*       successfully.
*   USBHST_INVALID_PARAMETER - Returned if the parameters are not valid.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbXhcdModifyDefaultPipe
    (
    UINT8   uBusIndex,          /* Host controller bus index               */
    ULONG   uDefaultPipeHandle, /* Handle to the default pipe              */
    UINT8   uDeviceSpeed,       /* Speed of the device in default state    */
    UINT8   uMaxPacketSize,     /* Maximum packet size of the default pipe */
    UINT16  uHighSpeedHubInfo   /* High speed hub info for USB 1.1 device  */
    )
    {
    pUSB_XHCD_DATA              pHCDData;
    pUSB_XHCD_DEVICE            pHCDDevice;
    pUSB_XHCD_PIPE              pHCDPipe;
    pUSB_XHCI_INPUT_CTRL_CTX    pInputCtrlCtx;
    pUSB_XHCI_EP_CTX            pCtrlEpCtx;

    /* Extract the global data structure */

    pHCDData = gpXHCDData[uBusIndex];

    /* Assert if the global pointer is not valid */

    if (NULL == pHCDData)
        {
        USB_XHCD_ERR("usbXhcdModifyDefaultPipe - pHCDData NULL\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Cast to pipe structure */

    pHCDPipe = (pUSB_XHCD_PIPE)uDefaultPipeHandle;

    pHCDPipe->uDevSpeed = uDeviceSpeed;

    pHCDPipe->uMaxPacketSize = uMaxPacketSize;

    pHCDPipe->uHubInfo = uHighSpeedHubInfo;

    USB_XHCD_WARN("usbXhcdModifyDefaultPipe - "
        "device %d pipe %p speed %d max packet size %d HS hub info 0x%04x\n",
        pHCDPipe->uDevAddr,
        uDefaultPipeHandle,
        uDeviceSpeed,
        uMaxPacketSize,
        uHighSpeedHubInfo, 0);

    /*
     * SuperSpeed devices are setup during the Address Device
     * phase in usbXhcdDeviceSlotInit() so we do not need to
     * update them. Root hub devices also do not need to update.
     */

    if ((uDeviceSpeed == USBHST_SUPER_SPEED) ||
        (pHCDPipe->uDevAddr == 0))
        {
        USB_XHCD_WARN("usbXhcdModifyDefaultPipe - no need to update context\n",
            1, 2, 3, 4, 5, 6);

        return USBHST_SUCCESS;
        }
    /*
     * Upon here, we should already have a device structure
     * created for that device during the initial steps!
     */

    pHCDDevice = usbXhcdDeviceGet(pHCDData, pHCDPipe->uDevAddr, uDeviceSpeed);

    if (pHCDDevice == NULL)
        {
        USB_XHCD_ERR("usbXhcdModifyDefaultPipe - "
                     "usbXhcdDeviceGet for dev %d fail\n",
                     pHCDPipe->uDevAddr, 2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    pInputCtrlCtx = USB_XHCI_INPUT_CTRL_CTX_CAST(pHCDData,
                        pHCDDevice->pIntputCtx);

    /*
     * We only update the Endpoint 0 (corresponding to A1).
     * Some xHCI controllers will not actually update the
     * output contexts if we also specify A0 to be evaluated.
     */

    pHCDDevice->uAddFlags = USB_XHCI_ADD_EP(1);

    pInputCtrlCtx->uAddFlags = USB_XHCD_SWAP_DESC_DATA32(pHCDData,
                               pHCDDevice->uAddFlags);
    /*
     * All Drop Context flags of the Input Control Context shall
     * be cleared to '0'.
     */

    pInputCtrlCtx->uDropFlags = 0;

    pCtrlEpCtx = USB_XHCI_EP_CTX_CAST(pHCDData,
                                      pHCDDevice->pIntputCtx,
                                      USB_XHCI_CTRL_EP_CTX_DCI(0));

    pCtrlEpCtx->uEpInfo1 = USB_XHCD_SWAP_DESC_DATA32(pHCDData,
                           (USB_XHCI_EP_CTX_EP_TYPE(USB_XHCI_CTRL_EP) |
                           USB_XHCI_EP_CTX_MAX_PACKET_SIZE(pHCDPipe->uMaxPacketSize) |
                           USB_XHCI_EP_CTX_MAX_BURST_SIZE(0) |
                           USB_XHCI_EP_CTX_CERR(3)));

    /* Try to update the endpoint context */

    if (usbXhcdQueueEvaluateContextCmd(pHCDData,
                                       pHCDDevice->pIntputCtx->uCtxBusAddr,
                                       pHCDDevice->uSlotId,
                                       USB_XHCD_DEFAULT_CMD_WAIT_TIME) != OK)
        {
        USB_XHCD_WARN("usbXhcdModifyDefaultPipe - Evaluate Context fail\n",
            1, 2, 3, 4, 5, 6);
        }

    /* Reset the flags */

    pHCDDevice->uAddFlags = 0;
    pHCDDevice->uDropFlags = 0;

    return USBHST_SUCCESS;
    }

/*******************************************************************************
*
* usbXhcdSubmitControlURB - submit a Control request to a pipe
*
* This routine is used to submit a Control request to the pipe.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the URB is submitted successfully.
*   USBHST_INVALID_PARAMETER - Returned if the parameters are not valid.
*   USBHST_BAD_START_OF_FRAME - Returned if the start frame is invalid.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbXhcdSubmitControlURB
    (
    pUSB_XHCD_DATA          pHCDData,
    pUSB_XHCD_PIPE          pHCDPipe,
    pUSB_XHCD_REQUEST_INFO  pRequest,
    pUSBHST_URB             pURB
    )
    {
    pUSBHST_SETUP_PACKET    pSetup;
    pUSB_XHCD_SEGMENT       pSEG;
    pUSB_XHCI_TRB           pTRB;
    UINT64                  uXferBuffer;
    UINT32                  uXferSize;
    UINT32                  uDataLo;
    UINT32                  uDataHi;
    UINT32                  uInfo;
    UINT32                  uCtrl;
    UINT32                  uNumTRBs;
    UINT8                   uTRT;
    UINT8                   uDIR;

    pSetup = pURB->pTransferSpecificData;

    USB_XHCD_DBG("usbXhcdSubmitControlURB - Submit Control URB @%p - "
        "bmRequestType 0x%02x bRequest 0x%02x wValue 0x%02x "
        "wIndex 0x%02x wLength 0x%02x\n",
        pURB,
        pSetup->bmRequestType,
        pSetup->bRequest,
        pSetup->wValue,
        pSetup->wIndex,
        pSetup->wLength);

    pRequest->pRing = pHCDPipe->pEpRing;

    /*
     * We should have been assigned an address through the
     * Address Device Command. xHCI will reject our control
     * request for SetAddress(). So we just fake this request.
     */

    if ((pSetup->bRequest == USB_REQ_SET_ADDRESS) &&
        ((pSetup->bmRequestType & USB_RT_CATEGORY_MASK) == USB_RT_STANDARD))
        {
        USB_XHCD_DBG("usbXhcdSubmitControlURB - "
            "xHCI does not need software to set device address! Ignored!\n",
            1, 2, 3, 4, 5, 6);

        pURB->nStatus = USBHST_SUCCESS;

        /* Release the request so that it is avaible for new requests */

        usbXhcdReleaseRequestInfo(pHCDData, pHCDPipe, pRequest);

        if (pURB->pfCallback)
            pURB->pfCallback(pURB);

        return USBHST_SUCCESS;
        }
    else if ((pSetup->bRequest == USB_REQ_SET_CONFIGURATION) &&
             ((pSetup->bmRequestType & USB_RT_CATEGORY_MASK) == USB_RT_STANDARD))
        {
        USB_XHCD_DBG("usbXhcdSubmitControlURB - "
            "Trying to set configuration %d!\n",
            pSetup->wValue, 2, 3, 4, 5, 6);

        /*
         * SetConfiguration() with a non-zero Configuratin Value
         * must wait the usbXhcdIsBandwidthAvailable() is called
         * to reserve bandwidth by "Configure Endpoint Command".
         * We may want to check to make sure, but since we are
         * sure our USBD is nice to call isBandwidthAvailable()
         * before issuing SetConfiguration() with non-zero values,
         * we can just allow it to proceed anyway.
         *
         * However, if a Configuratin Value of zero is issued, we
         * have to make sure a "Configure Endpoint Command" with
         * DC bit set is issued.
         */

        /*
         * TODO: Check if Configuratin Value is zero and then
         * "Configure Endpoint Command" with DC bit set.
         */
        }

    uXferBuffer = pRequest->uDataBusAddr;
    uXferSize = pRequest->uReqLen;

    /*
     * We need one DATA TRB if there is data transfer,
     * otherwise we need just 2 TRBs for a control transfer.
     */

    if (pSetup->wLength != 0)
        uNumTRBs = 3;
    else
        uNumTRBs = 2;

    /* Check if there is room on the transfer ring */

    if (usbXhcdHasRoomOnRing(pHCDData, pHCDPipe->pEpRing, uNumTRBs) != TRUE)
        {
        USB_XHCD_ERR("usbXhcdSubmitControlURB - usbXhcdHasRoomOnRing FALSE!\n",
            1, 2, 3, 4, 5, 6);

        return USBHST_INSUFFICIENT_RESOURCE;
        }

    /* Setup Transfer Type (TRT) and Direction (DIR) */

    if (pSetup->wLength == 0)
        {
        uTRT = USB_XHCI_SETUP_TRB_TRT_NO_DATA;

        uDIR = USB_XHCI_SETUP_TRB_DIR_OUT;
        }

    if (pSetup->bmRequestType & USB_ENDPOINT_IN)
        {
        if (pSetup->wLength > 0)
            {
            uTRT = USB_XHCI_SETUP_TRB_TRT_IN_DATA;
            uDIR = USB_XHCI_SETUP_TRB_DIR_IN;
            }
        }
    else
        {
        if (pSetup->wLength > 0)
            {
            uTRT = USB_XHCI_SETUP_TRB_TRT_OUT_DATA;
            uDIR = USB_XHCI_SETUP_TRB_DIR_OUT;
            }
        }

    /* See Figure 79: Setup Stage TRB */

    uDataLo = (USB_XHCI_TRB_DATALO_bmRequestType(pSetup->bmRequestType) |
               USB_XHCI_TRB_DATALO_bRequest(pSetup->bRequest) |
               USB_XHCI_TRB_DATALO_wValue(OS_UINT16_CPU_TO_LE(pSetup->wValue)));

    uDataHi = (USB_XHCI_TRB_DATAHI_wIndex(OS_UINT16_CPU_TO_LE(pSetup->wIndex)) |
               USB_XHCI_TRB_DATAHI_wLength(OS_UINT16_CPU_TO_LE(pSetup->wLength)));

    uInfo = (USB_XHCI_TRB_INFO_XFER_LEN(8) |
             USB_XHCI_TRB_INFO_INTR_TARGET(0));

    uCtrl = (USB_XHCI_TRB_CTRL_CYCLE_BIT(pHCDPipe->pEpRing->uCycle) |
             USB_XHCI_TRB_CTRL_IOC(0) |
             USB_XHCI_TRB_CTRL_IDT(1) |
             USB_XHCI_TRB_CTRL_TYPE(USB_XHCI_TRB_SETUP) |
             USB_XHCI_TRB_CTRL_TRT(uTRT));

    pSEG = pHCDPipe->pEpRing->pEnqSeg;

    pTRB = usbXhcdQueueTRB(pHCDData, pHCDPipe->pEpRing,
                           uDataLo, uDataHi, uInfo, uCtrl);

    pRequest->pHeadSeg = pSEG;
    pRequest->pHeadTRB = pTRB;
    pRequest->uHeadTRB = usbXhcdTrbBusAddr(pHCDData, pSEG, pTRB);

    if (pSetup->wLength > 0)
        {
        /* See Figure 80: Data Stage TRB */

        uDataLo = USB_XHCD_LO32(uXferBuffer);

        uDataHi = USB_XHCD_HI32(uXferBuffer);;

        uInfo = (USB_XHCI_TRB_INFO_XFER_LEN(uXferSize) |
                 USB_XHCI_TRB_INFO_INTR_TARGET(0));

        uCtrl = (USB_XHCI_TRB_CTRL_CYCLE_BIT(pHCDPipe->pEpRing->uCycle) |
                 USB_XHCI_TRB_CTRL_CHAIN_BIT(0) |
                 USB_XHCI_TRB_CTRL_IOC(0) |
                 USB_XHCI_TRB_CTRL_ISP(1) |
                 USB_XHCI_TRB_CTRL_TYPE(USB_XHCI_TRB_DATA) |
                 USB_XHCI_TRB_CTRL_DIR(uDIR));

        usbXhcdQueueTRB(pHCDData, pHCDPipe->pEpRing,
                        uDataLo, uDataHi, uInfo, uCtrl);

        }

    uDIR = (UINT8)((uDIR == USB_XHCI_SETUP_TRB_DIR_IN)?
            USB_XHCI_SETUP_TRB_DIR_OUT : USB_XHCI_SETUP_TRB_DIR_IN);

    /* See Figure 81: Status Stage TRB */

    uDataLo = 0;

    uDataHi = 0;

    uInfo = USB_XHCI_TRB_INFO_INTR_TARGET(0);

    uCtrl = (USB_XHCI_TRB_CTRL_CYCLE_BIT(pHCDPipe->pEpRing->uCycle) |
             USB_XHCI_TRB_CTRL_CHAIN_BIT(0) |
             USB_XHCI_TRB_CTRL_IOC(1) |
             USB_XHCI_TRB_CTRL_TYPE(USB_XHCI_TRB_STATUS) |
             USB_XHCI_TRB_CTRL_DIR(uDIR));

    pSEG = pHCDPipe->pEpRing->pEnqSeg;

    pTRB = usbXhcdQueueTRB(pHCDData, pHCDPipe->pEpRing,
                           uDataLo, uDataHi, uInfo, uCtrl);

    pRequest->pTailSeg = pSEG;
    pRequest->pTailTRB = pTRB;
    pRequest->uTailTRB = usbXhcdTrbBusAddr(pHCDData, pSEG, pTRB);

    /* Notify the endpoint ready */

    usbXhcdNotifyEndpointReady(pHCDData,
                               pHCDPipe->uSlotId,
                               pHCDPipe->uEpId,
                               0);
    return USBHST_SUCCESS;
    }

/*******************************************************************************
*
* usbXhcdSubmitBulkIntrURB - submit a Bulk request to a pipe
*
* This routine is used to submit a Bulk request to the pipe.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the URB is submitted successfully.
*   USBHST_INVALID_PARAMETER - Returned if the parameters are not valid.
*   USBHST_BAD_START_OF_FRAME - Returned if the start frame is invalid.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbXhcdSubmitBulkIntrURB
    (
    pUSB_XHCD_DATA          pHCDData,
    pUSB_XHCD_PIPE          pHCDPipe,
    pUSB_XHCD_REQUEST_INFO  pRequest,
    pUSBHST_URB             pURB
    )
    {
    UINT64                  uXferBuffer;
    UINT32                  uXferSize;
    UINT32                  uRemainSize;
    UINT32                  uDataLo;
    UINT32                  uDataHi;
    UINT32                  uInfo;
    UINT32                  uCtrl;
    UINT32                  uNumTRBs;
    UINT8                   uIOC;
    UINT8                   uISP;
    UINT8                   uENT;
    UINT8                   uChain;
    pUSB_XHCD_SEGMENT       pSEG;
    pUSB_XHCI_TRB           pTRB;
    pUSB_XHCD_RING          pEpRing;

    uXferBuffer = pRequest->uDataBusAddr;
    uRemainSize = pRequest->uReqLen;
    uXferSize = 0;

    uNumTRBs = (uRemainSize / USB_XHCI_TRB_MAX_XFER_SIZE);

    if (uRemainSize % USB_XHCI_TRB_MAX_XFER_SIZE)
        uNumTRBs++;

    if (pURB->uStreamId == 0)
        {
        pEpRing = pHCDPipe->pEpRing;
        }
    else
        {
        if (pURB->uStreamId >= pHCDPipe->streamInfo.uNumStreams)
            {
            USB_XHCD_ERR("usbXhcdSubmitBulkIntrURB - "
                "Requested uStreamId %d bigger than supported uNumStreams %d\n",
                pURB->uStreamId,
                pHCDPipe->streamInfo.uNumStreams, 3, 4, 5, 6);

            return USBHST_INVALID_PARAMETER;
            }

        USB_XHCD_DBG("usbXhcdSubmitBulkIntrURB - "
            "uDevAddr %d uEpAddr 0x%02x uEpId %d pRequest %p pURB %p uTransferLength %d\n",
            pHCDPipe->uDevAddr,
            pHCDPipe->uEpAddr,
            pHCDPipe->uEpId,
            pRequest, pURB, pURB->uTransferLength);

        pEpRing = pHCDPipe->streamInfo.ppStreamRings[pURB->uStreamId];
        }

    if (pEpRing == NULL)
        {
        USB_XHCD_ERR("usbXhcdSubmitBulkIntrURB - NO Transfer Ring! "
            "uDevAddr %d uEpAddr 0x%02x uEpId %d uStreamId %d\n",
            pHCDPipe->uDevAddr,
            pHCDPipe->uEpAddr,
            pHCDPipe->uEpId,
            pURB->uStreamId, 5, 6);

        return ERROR;
        }

    pRequest->pRing = pEpRing;

    /* Check if there is room on the transfer ring */

    if (usbXhcdHasRoomOnRing(pHCDData, pEpRing, uNumTRBs) != TRUE)
        {
        USB_XHCD_ERR("usbXhcdSubmitBulkIntrURB - usbXhcdHasRoomOnRing FALSE!\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    USB_XHCD_VDBG("usbXhcdSubmitBulkIntrURB - "
        "pHCDPipe %p pRequest %p pURB %p dev 0x%x ep 0x%x len %d\n",
        pHCDPipe,
        pRequest,
        pURB,
        pURB->hDevice,
        pURB->uEndPointAddress,
        pURB->uTransferLength);

    /*
     * On an IN endpoint, if the device class allows a device to supply
     * less data than the host has provided buffer space for, software
     * has two options in forming a TD.
     *
     * 1) Set the Interrupt-on Short Packet (ISP) flag in all TRBs of a TD,
     * and set the IOC flag in the last TRB. This action shall cause the
     * xHC to generate a Transfer Event if a Short Packet condition is
     * detected while executing any TRB in the TD, or generate a Transfer
     * Event if the device completely fills the buffer.
     *
     * To determine the number of bytes actually transferred, software
     * shall add the TRB Transfer Length fields of all TRBs up to and
     * including the TRB that generated the Transfer Event, and subtract
     * the Transfer Event TRB Transfer Length field.
     *
     * 2) Terminate the TD with an Event Data TRB that has its IOC flag
     * set, and not set the ISP or IOC flag in any Transfer TRB of the TD.
     * This action shall cause the xHC to generate an Event Data Transfer
     * Event if a Short Packet condition is detected while executing any
     * TRB in the TD or if the device completely fills the buffer.
     *
     * The TRB Transfer Length field of the Event Data Transfer Event
     * identifies the number of bytes actually transferred, from the
     * beginning of the TD or since the last Event Data Transfer Event.
     * The TRB Transfer Length field of the Event Data Transfer Event
     * may define up to a 16,777,215 (16MB - 1) byte transfer.
     *
     * If Event Data TRBs are defined within a TD, then the IOC or ISP
     * flags shall not be set in any Transfer TRB of a TD. i.e. the use
     * of Event Data Transfer Events and normal Transfer Events to report
     * a TD completion are mutually exclusive.
     */

    /*
     * Initially set uIOC to 0 and uChain to 1, for both Event Data
     * TRB mechanism and Transfer TRB mechanism. If it is normal
     * Transfer TRB mechanism, uIOC will be set to 1 and uChain will
     * be set to 0 for the last TRB.
     */

    uIOC = 0;

    uChain = 1;

    uENT = 0;

#ifdef _WRS_CONFIG_USB_XHCD_USE_EVENT_DATA_TRB
    /* Event Data TRB mechanism does not need ISP to be set! */
    uISP = 0;
#else
    /* Normal Transfer TRB mechanism needs ISP to be set! */
    uISP = 1;
#endif /* _WRS_CONFIG_USB_XHCD_USE_EVENT_DATA_TRB */

    do
        {
        /* A TRB can transfer no more than USB_XHCI_TRB_MAX_XFER_SIZE */

        if (uRemainSize >= USB_XHCI_TRB_MAX_XFER_SIZE)
            uXferSize = USB_XHCI_TRB_MAX_XFER_SIZE;
        else
            uXferSize = uRemainSize;

        /* Get the remaining size */

        uRemainSize -= uXferSize;

        /* Check if this is the last Transfer TRB in this TD */

        if (uRemainSize == 0)
            {
#ifdef _WRS_CONFIG_USB_XHCD_USE_EVENT_DATA_TRB
            /*
             * If Event Data TRB mechanism is used, Data TRBs do not
             * need IOC/ISP bits to be set (even it is the last TRB
             * of a TD). But the Chain bit needs to be set even if
             * it is the last TRB of a TD, since a Event Data TRB is
             * being chained to form a complete TD.
             *
             * The uIOC, uISP, and uChain variables are already set
             * to proper values (uIOC = uISP = 0, uChain = 1) just
             * before this do{...}while loop, so we have no need to
             * set them again here.
             */

            /*
             * System software shall set the ENT flag in the last
             * Transfer TRB before a terminating Event Data TRB in a
             * TD on a Stream (Bulk), normal Bulk, Interrupt, or
             * Control Transfer Ring. This action ensures the timely
             * execution of an Event Data TRB if the Transfer Ring
             * is flow controlled.
             */

             uENT = 1;

#else /* _WRS_CONFIG_USB_XHCD_USE_EVENT_DATA_TRB */
            /*
             * If we use normal Transfer TRB mechanism, the last TRB
             * in a TD should have its IOC/ISP bits set, and Chain
             * bit cleared.
             *
             * The uISP variable has been set to 1 before this do loop,
             * so we only need to set the uIOC and uChain variables
             * here since they are using the inversion values.
             */

            uIOC = 1;
            uChain = 0;
#endif /* _WRS_CONFIG_USB_XHCD_USE_EVENT_DATA_TRB */
            }

        /* See Figure 78: Normal TRB */

        uDataLo = USB_XHCD_LO32(uXferBuffer);
        uDataHi = USB_XHCD_HI32(uXferBuffer);;
        uXferBuffer += uXferSize;

        /*
         * Software shall specify the same Interrupter Target value
         * in all Transfer TRBs of a TD.
         */

        uInfo = (USB_XHCI_TRB_INFO_XFER_LEN(uXferSize) |
                 USB_XHCI_TRB_INFO_INTR_TARGET(0));

        uCtrl = (USB_XHCI_TRB_CTRL_CYCLE_BIT(pEpRing->uCycle) |
                 USB_XHCI_TRB_CTRL_CHAIN_BIT(uChain) |
                 USB_XHCI_TRB_CTRL_ENT(uENT) |
                 USB_XHCI_TRB_CTRL_IOC(uIOC) |
                 USB_XHCI_TRB_CTRL_ISP(uISP) |
                 USB_XHCI_TRB_CTRL_TYPE(USB_XHCI_TRB_NORMAL));

        pSEG = pEpRing->pEnqSeg;
        pTRB = usbXhcdQueueTRB(pHCDData, pEpRing,
                               uDataLo, uDataHi, uInfo, uCtrl);


        /*
         * If we are NOT using Event Data TRB mechanism, we have to
         * record the first Segment and TRB information for the sake
         * of doing BUS address to CPU Virtual address translation
         * to find the proper request when it is completed!
         *
         * Even we are using the Event Data TRB mechanism, if any Data
         * TRB got error (such as a STALL), we will still get a normal
         * Transfer Event TRB reporting the erroring Data TRB, and we
         * need to fall back to the searching method as for the normal
         * Transfer TRB mechanism to find the erroring request.
         *
         * This seems to be a waste of CPU time but we have to do it!
         */

        if (pRequest->pHeadSeg == NULL)
            {
            pRequest->pHeadSeg = pSEG;
            pRequest->pHeadTRB = pTRB;
            pRequest->uHeadTRB = usbXhcdTrbBusAddr(pHCDData, pSEG, pTRB);
            }

        USB_XHCD_DBG("usbXhcdSubmitBulkIntrURB - "
            "pRequest %p pTRB %p uRemainSize %d uXferSize %d uIOC %d uChain %d\n",
            pRequest, pTRB, uRemainSize, uXferSize, uIOC, uChain);
        }while (uRemainSize > 0);

    /*
     * Record the last Segment and TRB information.
     */

    pRequest->pTailSeg = pSEG;
    pRequest->pTailTRB = pTRB;
    pRequest->uTailTRB = usbXhcdTrbBusAddr(pHCDData, pSEG, pTRB);

#ifdef _WRS_CONFIG_USB_XHCD_USE_EVENT_DATA_TRB
    /*
     * If we are using Event Data TRB mechanism, its time to Chain a
     * Event Data TRB, specifying the "pRequest" as the Event Data,
     * when the TD completes, we will get the "Event Data Transfer
     * Event TRB" notifying us (in its Event Data) which "pRequest"
     * is done and get the result information from the "Event Data
     * Transfer Event TRB"! We have benefited from this mechanism:
     *
     * 1) We do not need to search for which request has done, the
     * "Event Data" in the "Event Data Transfer Event TRB" tells us
     * directly our "pRequest", which is a CPU Virtual address that
     * we can use directly. Otherwise, using the normal Transfer TRB
     * mechanisim, we must do "Bus Address to CPU Address conversion"
     * in order to find our "pRequest" from the "Transfer Event TRB".
     *
     * 2) We do not need to sum up the transfer length fields, the
     * "Event Data Transfer Event TRB" tells us directly! We also
     * get the Completion Code from "Event Data Transfer Event TRB".
     */

    /* See Figure 107: Event Data TRB */

    uXferBuffer = (UINT64)pRequest;

    uDataLo = USB_XHCD_LO32(uXferBuffer);

    uDataHi = USB_XHCD_HI32(uXferBuffer);

    uInfo = (USB_XHCI_TRB_INFO_INTR_TARGET(0));

    uCtrl = (USB_XHCI_TRB_CTRL_CYCLE_BIT(pEpRing->uCycle) |
             USB_XHCI_TRB_CTRL_IOC(1) |
             USB_XHCI_TRB_CTRL_TYPE(USB_XHCI_TRB_EVENT_DATA));

    pTRB = usbXhcdQueueTRB(pHCDData, pEpRing,
                           uDataLo, uDataHi, uInfo, uCtrl);
#endif /* _WRS_CONFIG_USB_XHCD_USE_EVENT_DATA_TRB */

    /* Notify the endpoint ready */

    usbXhcdNotifyEndpointReady(pHCDData,
                               pHCDPipe->uSlotId,
                               pHCDPipe->uEpId,
                               pURB->uStreamId);

    return USBHST_SUCCESS;
    }

/*******************************************************************************
*
* usbXhcdSubmitIsochronousURB - submit a Isochronous request to a pipe
*
* This routine is used to submit a Isochronous request to the pipe.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the URB is submitted successfully.
*   USBHST_INVALID_PARAMETER - Returned if the parameters are not valid.
*   USBHST_BAD_START_OF_FRAME - Returned if the start frame is invalid.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbXhcdSubmitIsochronousURB
    (
    pUSB_XHCD_DATA          pHCDData,
    pUSB_XHCD_PIPE          pHCDPipe,
    pUSB_XHCD_REQUEST_INFO  pRequest,
    pUSBHST_URB             pURB
    )
    {
    UINT64                  uXferBuffer;
    UINT32                  uXferSize;
    UINT64                  uXferBufferBase;
    UINT32                  uXferSizeTotal;
    UINT32                  uRemainSize;
    UINT32                  uDataLo;
    UINT32                  uDataHi;
    UINT32                  uInfo;
    UINT32                  uCtrl;
    UINT32                  uNumTRBs;
    UINT32                  uNumPackets;
    UINT32                  uPacketIndex;
    UINT8                   uIOC;
    UINT8                   uISP;
    UINT8                   uENT;
    UINT8                   uChain;
    UINT8                   uASAP;
    BOOL                    bFirstTRB;
    pUSB_XHCD_SEGMENT       pSEG;
    pUSB_XHCI_TRB           pTRB;
    pUSB_XHCD_RING          pEpRing;
    pUSBHST_ISO_PACKET_DESC pIsoPacket;

    pEpRing = pHCDPipe->pEpRing;

    if (pEpRing == NULL)
        {
        USB_XHCD_ERR("usbXhcdSubmitIsochronousURB - NO Transfer Ring! "
            "uDevAddr %d uEpAddr 0x%02x uEpId %d\n",
            pHCDPipe->uDevAddr,
            pHCDPipe->uEpAddr,
            pHCDPipe->uEpId, 4, 5, 6);

        return ERROR;
        }

    pRequest->pRing = pEpRing;

    pIsoPacket = pURB->pTransferSpecificData;
    uNumPackets = pURB->uNumberOfPackets;
    uXferBufferBase = pRequest->uDataBusAddr;
    uXferSizeTotal = pRequest->uReqLen;

    if (pURB->uTransferFlags & USBHST_START_ISOCHRONOUS_TRANSFER_ASAP)
        uASAP = 1;
    else
        uASAP = 0;

    uNumTRBs = (uXferSizeTotal / USB_XHCI_TRB_MAX_XFER_SIZE);

    if (uXferSizeTotal % USB_XHCI_TRB_MAX_XFER_SIZE)
        uNumTRBs++;

    /* Check if there is room on the transfer ring */

    if (usbXhcdHasRoomOnRing(pHCDData, pEpRing, uNumTRBs) != TRUE)
        {
        USB_XHCD_ERR("usbXhcdSubmitIsochronousURB - usbXhcdHasRoomOnRing FALSE!\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    USB_XHCD_VDBG("usbXhcdSubmitIsochronousURB - "
        "pHCDPipe %p pRequest %p pURB %p dev 0x%x ep 0x%x len %d\n",
        pHCDPipe,
        pRequest,
        pURB,
        pURB->hDevice,
        pURB->uEndPointAddress,
        pURB->uTransferLength);

    for (uPacketIndex = 0; uPacketIndex < uNumPackets; uPacketIndex ++)
        {
        /* Transfer Descriptor Packet Count */

        UINT32 uTDPC;

        /* Isoch TRB Transfer Burst Count (TBC) */

        UINT32 uTBC;

        /* Transfer Last Burst Packet Count (TLBPC) */

        UINT32 uTLBPC;

        /* Residue Number of Packets not scheduled yet */

        UINT32 uResiduePackets;

        /* Frame ID of the packet */

        UINT32 uFrameID;

        uXferBuffer = uXferBufferBase + pIsoPacket->uOffset;
        uRemainSize = pIsoPacket->uLength;
        uXferSize = 0;
        bFirstTRB = TRUE;

        /*
         * The Transfer Descriptor Packet Count (TDPC) is the number
         * of packets required to move all the data defined by a TD.
         * Note that a partial or a zero-length packet increments
         * this count by 1.
         */

        uTDPC = (uRemainSize / pHCDPipe->uMaxPacketSize);
        if (uRemainSize % pHCDPipe->uMaxPacketSize)
            uTDPC++;

        /*
         * TBC = ROUNDUP ( TDPC / Max Burst Size ) - 1
         *
         * Note that the 'uMaxBusrtSize' is zero based,
         * so we need to add 1 for actual count.
         */

        uTBC = (uTDPC / (pHCDPipe->uMaxBusrtSize + 1));

        uResiduePackets = (uTDPC % (pHCDPipe->uMaxBusrtSize + 1));

        if (uResiduePackets != 0)
            {
            uTLBPC = uResiduePackets - 1;
            uTBC++;
            }
        else
            {
            uTLBPC = pHCDPipe->uMaxBusrtSize;
            }

        /* TBC is zero based, thus we substract 1 */

        uTBC--;

        uFrameID = pURB->uStartFrame +
            ((uPacketIndex * (1 << pHCDPipe->uInterval)) >> 3);

        USB_XHCD_VDBG("usbXhcdSubmitIsochronousURB - "
            "uCycle %d uPacketIndex %d uLength %d uTBC %d uTDPC %d uFrameID 0x%x\n",
            pEpRing->uCycle,
            uPacketIndex,
            uRemainSize,
            uTBC,
            uTDPC,
            uFrameID);
        /*
         * On an IN endpoint, if the device class allows a device to supply
         * less data than the host has provided buffer space for, software
         * has two options in forming a TD.
         *
         * 1) Set the Interrupt-on Short Packet (ISP) flag in all TRBs of a TD,
         * and set the IOC flag in the last TRB. This action shall cause the
         * xHC to generate a Transfer Event if a Short Packet condition is
         * detected while executing any TRB in the TD, or generate a Transfer
         * Event if the device completely fills the buffer.
         *
         * To determine the number of bytes actually transferred, software
         * shall add the TRB Transfer Length fields of all TRBs up to and
         * including the TRB that generated the Transfer Event, and subtract
         * the Transfer Event TRB Transfer Length field.
         *
         * 2) Terminate the TD with an Event Data TRB that has its IOC flag
         * set, and not set the ISP or IOC flag in any Transfer TRB of the TD.
         * This action shall cause the xHC to generate an Event Data Transfer
         * Event if a Short Packet condition is detected while executing any
         * TRB in the TD or if the device completely fills the buffer.
         *
         * The TRB Transfer Length field of the Event Data Transfer Event
         * identifies the number of bytes actually transferred, from the
         * beginning of the TD or since the last Event Data Transfer Event.
         * The TRB Transfer Length field of the Event Data Transfer Event
         * may define up to a 16,777,215 (16MB - 1) byte transfer.
         *
         * If Event Data TRBs are defined within a TD, then the IOC or ISP
         * flags shall not be set in any Transfer TRB of a TD. i.e. the use
         * of Event Data Transfer Events and normal Transfer Events to report
         * a TD completion are mutually exclusive.
         */

        /*
         * Initially set uIOC to 0 and uChain to 1, for both Event Data
         * TRB mechanism and Transfer TRB mechanism. If it is normal
         * Transfer TRB mechanism, uIOC will be set to 1 and uChain will
         * be set to 0 for the last TRB.
         */

        uIOC = 0;

        uChain = 1;

        uENT = 0;

#ifdef _WRS_CONFIG_USB_XHCD_USE_EVENT_DATA_TRB
        /* Event Data TRB mechanism does not need ISP to be set! */
        uISP = 0;
#else
        /* Normal Transfer TRB mechanism needs ISP to be set! */
        uISP = 1;
#endif /* _WRS_CONFIG_USB_XHCD_USE_EVENT_DATA_TRB */

        do
            {
            /* A TRB can transfer no more than USB_XHCI_TRB_MAX_XFER_SIZE */

            if (uRemainSize >= USB_XHCI_TRB_MAX_XFER_SIZE)
                uXferSize = USB_XHCI_TRB_MAX_XFER_SIZE;
            else
                uXferSize = uRemainSize;

            /* Get the remaining size */

            uRemainSize -= uXferSize;

            /* Check if this is the last Transfer TRB in this TD */

            if (uRemainSize == 0)
                {
#ifdef _WRS_CONFIG_USB_XHCD_USE_EVENT_DATA_TRB
                /*
                 * If Event Data TRB mechanism is used, Data TRBs do not
                 * need IOC/ISP bits to be set (even it is the last TRB
                 * of a TD). But the Chain bit needs to be set even if
                 * it is the last TRB of a TD, since a Event Data TRB is
                 * being chained to form a complete TD.
                 *
                 * The uIOC, uISP, and uChain variables are already set
                 * to proper values (uIOC = uISP = 0, uChain = 1) just
                 * before this do{...}while loop, so we have no need to
                 * set them again here.
                 */

                /*
                 * System software shall set the ENT flag in the last
                 * Transfer TRB before a terminating Event Data TRB in a
                 * TD on a Stream (Bulk), normal Bulk, Interrupt, or
                 * Control Transfer Ring. This action ensures the timely
                 * execution of an Event Data TRB if the Transfer Ring
                 * is flow controlled.
                 */

                 uENT = 1;

#else /* _WRS_CONFIG_USB_XHCD_USE_EVENT_DATA_TRB */
                /*
                 * If we use normal Transfer TRB mechanism, the last TRB
                 * in a TD should have its IOC/ISP bits set, and Chain
                 * bit cleared.
                 *
                 * The uISP variable has been set to 1 before this do loop,
                 * so we only need to set the uIOC and uChain variables
                 * here since they are using the inversion values.
                 */

                uIOC = 1;
                uChain = 0;
#endif /* _WRS_CONFIG_USB_XHCD_USE_EVENT_DATA_TRB */
                }

            /* See Figure 78: Normal TRB */

            uDataLo = USB_XHCD_LO32(uXferBuffer);
            uDataHi = USB_XHCD_HI32(uXferBuffer);;
            uXferBuffer += uXferSize;

            /*
             * Software shall specify the same Interrupter Target value
             * in all Transfer TRBs of a TD.
             */

            uInfo = (USB_XHCI_TRB_INFO_XFER_LEN(uXferSize) |
                     USB_XHCI_TRB_INFO_INTR_TARGET(0));

            uCtrl = (USB_XHCI_TRB_CTRL_CYCLE_BIT(pEpRing->uCycle) |
                           USB_XHCI_TRB_CTRL_CHAIN_BIT(uChain) |
                           USB_XHCI_TRB_CTRL_ENT(uENT) |
                           USB_XHCI_TRB_CTRL_IOC(uIOC) |
                           USB_XHCI_TRB_CTRL_ISP(uISP));

            if (bFirstTRB == TRUE)
                {
                bFirstTRB = FALSE;

                uCtrl |= (USB_XHCI_TRB_CTRL_TYPE(USB_XHCI_TRB_ISOC) |
                    USB_XHCI_TRB_CTRL_FRAME_ID(uFrameID) |
                    USB_XHCI_TRB_CTRL_SIA(uASAP) |
                    USB_XHCI_TRB_CTRL_TLBPC(uTLBPC) |
                    USB_XHCI_TRB_CTRL_TBC(uTBC));

                }
            else
                {
                uCtrl |= (USB_XHCI_TRB_CTRL_TYPE(USB_XHCI_TRB_NORMAL));
                }

            pSEG = pEpRing->pEnqSeg;
            pTRB = usbXhcdQueueTRB(pHCDData, pEpRing,
                                   uDataLo, uDataHi, uInfo, uCtrl);


            /*
             * If we are NOT using Event Data TRB mechanism, we have to
             * record the first Segment and TRB information for the sake
             * of doing BUS address to CPU Virtual address translation
             * to find the proper request when it is completed!
             *
             * Even we are using the Event Data TRB mechanism, if any Data
             * TRB got error (such as a STALL), we will still get a normal
             * Transfer Event TRB reporting the erroring Data TRB, and we
             * need to fall back to the searching method as for the normal
             * Transfer TRB mechanism to find the erroring request.
             *
             * This seems to be a waste of CPU time but we have to do it!
             */

            if (pRequest->pHeadSeg == NULL)
                {
                pRequest->pHeadSeg = pSEG;
                pRequest->pHeadTRB = pTRB;
                pRequest->uHeadTRB = usbXhcdTrbBusAddr(pHCDData, pSEG, pTRB);
                }

            USB_XHCD_DBG("usbXhcdSubmitBulkIntrURB - "
                "pRequest %p pTRB %p uRemainSize %d uXferSize %d uIOC %d uChain %d\n",
                pRequest, pTRB, uRemainSize, uXferSize, uIOC, uChain);
            }while (uRemainSize > 0);

        /*
         * Record the last Segment and TRB information.
         */

        pRequest->pTailSeg = pSEG;
        pRequest->pTailTRB = pTRB;
        pRequest->uTailTRB = usbXhcdTrbBusAddr(pHCDData, pSEG, pTRB);

#ifdef _WRS_CONFIG_USB_XHCD_USE_EVENT_DATA_TRB
        /*
         * If we are using Event Data TRB mechanism, its time to Chain a
         * Event Data TRB, specifying the "pRequest" as the Event Data,
         * when the TD completes, we will get the "Event Data Transfer
         * Event TRB" notifying us (in its Event Data) which "pRequest"
         * is done and get the result information from the "Event Data
         * Transfer Event TRB"! We have benefited from this mechanism:
         *
         * 1) We do not need to search for which request has done, the
         * "Event Data" in the "Event Data Transfer Event TRB" tells us
         * directly our "pRequest", which is a CPU Virtual address that
         * we can use directly. Otherwise, using the normal Transfer TRB
         * mechanisim, we must do "Bus Address to CPU Address conversion"
         * in order to find our "pRequest" from the "Transfer Event TRB".
         *
         * 2) We do not need to sum up the transfer length fields, the
         * "Event Data Transfer Event TRB" tells us directly! We also
         * get the Completion Code from "Event Data Transfer Event TRB".
         */

        /* See Figure 107: Event Data TRB */

        uXferBuffer = (UINT64)pRequest;

        uDataLo = USB_XHCD_LO32(uXferBuffer);

        uDataHi = USB_XHCD_HI32(uXferBuffer);

        uInfo = (USB_XHCI_TRB_INFO_INTR_TARGET(0));

        uCtrl = (USB_XHCI_TRB_CTRL_CYCLE_BIT(pEpRing->uCycle) |
                 USB_XHCI_TRB_CTRL_IOC(1) |
                 USB_XHCI_TRB_CTRL_TYPE(USB_XHCI_TRB_EVENT_DATA));

        pTRB = usbXhcdQueueTRB(pHCDData, pEpRing,
                               uDataLo, uDataHi, uInfo, uCtrl);
#endif /* _WRS_CONFIG_USB_XHCD_USE_EVENT_DATA_TRB */

        /* Go to the next packet */

        pIsoPacket++;
        }

    /* Notify the endpoint ready */

    usbXhcdNotifyEndpointReady(pHCDData,
                               pHCDPipe->uSlotId,
                               pHCDPipe->uEpId,
                               0);

    return USBHST_SUCCESS;
    }

/*******************************************************************************
*
* usbXhcdSubmitURB - submit a request to a pipe
*
* This routine is used to submit a request to the pipe. <uBusIndex> specifies
* the host controller bus index. <uPipeHandle> holds the pipe handle. <pURB>
* is the pointer to the URB holding the request details. This is done by
* checking if the parameters are valid or request can be added, then calling
* the proper sub routines to do the actual transfer.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the URB is submitted successfully.
*   USBHST_INVALID_PARAMETER - Returned if the parameters are not valid.
*   USBHST_BAD_START_OF_FRAME - Returned if the start frame is invalid.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbXhcdSubmitURB
    (
    UINT8           uBusIndex,          /* Index of the host controller */
    ULONG           uPipeHandle,        /* Pipe handle */
    pUSBHST_URB     pURB                /* Pointer to the URB */
    )
    {
    /* Pointer to the data structure */

    pUSB_XHCD_DATA          pHCDData;

    /* Pointer to the HCD maintained pipe */

    pUSB_XHCD_PIPE          pHCDPipe;

    /* Pointer to the Request information */

    pUSB_XHCD_REQUEST_INFO  pRequest;

    /* Status of the request */

    USBHST_STATUS           Status = USBHST_SUCCESS;

    /* vxBus call status */

    STATUS                  vxbStatus;

    /* Check the validity of the parameters */

    if ((0 == uPipeHandle) ||
        (NULL == pURB))
        {
        USB_XHCD_ERR("usbXhcdSubmitURB - uBusIndex %d "
            "puPipeHandle %p\n",
            uBusIndex, uPipeHandle, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the global data structure */

    pHCDData = gpXHCDData[uBusIndex];

    /* Assert if the global pointer is not valid */

    if (NULL == pHCDData)
        {
        USB_XHCD_ERR("usbXhcdSubmitURB - pHCDData NULL\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the USB_XHCD_PIPE data structure */

    pHCDPipe = (pUSB_XHCD_PIPE)uPipeHandle;

    /* Check if the request is for the Root hub and route it */

    if ((pHCDPipe->bRootHubPipe == TRUE) ||
        (((pHCDData->pDefaultUSB3Pipe == pHCDPipe) ||
          (pHCDData->pDefaultUSB2Pipe == pHCDPipe)) &&
         ((0 == pHCDData->USB2RH.uDeviceAddress) ||
          (0 == pHCDData->USB3RH.uDeviceAddress))))
        {
        Status = usbXhcdRootHubSubmitURB(pHCDData,
                                         uPipeHandle,
                                         pURB);

        return Status;
        }

    if (pHCDPipe->uValidMaggic != USB_XHCD_MAGIC_ALIVE)
        {
        USB_XHCD_ERR("usbXhcdSubmitURB - pipe invalid\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    USB_XHCD_VDBG("usbXhcdSubmitURB - "
                  "pHCDPipe %p pURB %p dev 0x%x ep 0x%x len %d\n",
                  pHCDPipe,
                  pURB,
                  pURB->hDevice,
                  pURB->uEndPointAddress,
                  pURB->uTransferLength, 0);

#if defined (_WRS_CONFIG_PWR_MGMT) && defined (_WRS_ARCH_HAS_DEV_PWR_MGMT)

    /* If device is OFF */

    if (PWR_DEVICE_UTILIZATION_SET(pHCDData->pDev->pwrDeviceId) != OK)
        usbXhcdPwrStateSet (pHCDData->pDev, 0);   /* turn device on */

#endif /* (_WRS_CONFIG_PWR_MGMT) && (_WRS_ARCH_HAS_DEV_PWR_MGMT) */

    /* Exclusively access the pipe request list */

    (void) OS_WAIT_FOR_EVENT(pHCDPipe->pipeMutex, OS_WAIT_INFINITE);

    /* Get a free request info structure from the pipe */

    pRequest = usbXhcdReserveRequestInfo(pHCDData, pHCDPipe, pURB);

    if (pRequest == NULL)
        {
        USB_XHCD_ERR("usbXhcdSubmitURB - no free pRequest found\n",
            0, 0, 0, 0, 0, 0);

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDPipe->pipeMutex);

        return USBHST_INVALID_REQUEST;
        }

    /* If there is buffer to be used for transfer, setup for DMA */

    if (pRequest->uReqLen != 0)
        {
        /* Load the DMA MAP */

        vxbStatus = vxbDmaBufMapLoad (pHCDData->pDev,
                                      pHCDPipe->dataDmaTagId,
                                      pRequest->dataDmaMapId,
                                      pURB->pTransferBuffer,
                                      pURB->uTransferLength,
                                      VXB_DMABUF_MAPLOAD_CACHEALIGNCHECK);

        if (vxbStatus == OK)
            {
            /* <copy to bounce buffer and> DMA cache operations */

            if (USBHST_URB_DATA_IN(pURB))
                {
                vxbStatus = vxbDmaBufSync(pHCDData->pDev,
                              pHCDPipe->dataDmaTagId,
                              pRequest->dataDmaMapId,
                              _VXB_DMABUFSYNC_DMA_PREREAD);
                }
            else
                {
                vxbStatus = vxbDmaBufSync(pHCDData->pDev,
                              pHCDPipe->dataDmaTagId,
                              pRequest->dataDmaMapId,
                              _VXB_DMABUFSYNC_DMA_PREWRITE);
                }
            }


        if (vxbStatus != OK)
            {
            /*
             * If not properly setup for DMA operation,
             * update the status and do not try to set it up!
             */

            Status = USBHST_INSUFFICIENT_RESOURCE;
            }
        else
            {
            /*
             * Otherwise, save the DMA buffer Bus Address
             * which can be used to setup DMA directly.
             */

            pRequest->uDataBusAddr = pRequest->dataDmaMapId->fragList[0].frag;
            }
        }

    /*
     * If the buffer is ready for transfer setup, then do it.
     */

    if (Status == USBHST_SUCCESS)
        {
        switch (pHCDPipe->uEpXferType)
            {
            case USBHST_CONTROL_TRANSFER:
                Status = usbXhcdSubmitControlURB(pHCDData,
                                                 pHCDPipe, pRequest, pURB);
                break;
            case USBHST_BULK_TRANSFER:
                /* Fall through */
            case USBHST_INTERRUPT_TRANSFER:
                Status = usbXhcdSubmitBulkIntrURB(pHCDData,
                                                  pHCDPipe, pRequest, pURB);
                break;
            case USBHST_ISOCHRONOUS_TRANSFER:
                Status = usbXhcdSubmitIsochronousURB(pHCDData,
                                                     pHCDPipe, pRequest, pURB);
                break;
            default:
                Status = USBHST_INVALID_REQUEST;
                break;
            }
        }

    /* Check if there is any failure */

    if (Status != USBHST_SUCCESS)
        {
        USB_EHCD_ERR("usbXhcdSubmitURB - URB submit fail (%d)\n",
            Status, 0, 0, 0, 0, 0);

        /* If the buffer is loaded, unload it */

        if (pRequest->uReqLen != 0)
            {
            (void) vxbDmaBufMapUnload(pHCDPipe->dataDmaTagId, pRequest->dataDmaMapId);
            }

        /* Return the request info to the pipe */

        usbXhcdReleaseRequestInfo(pHCDData, pHCDPipe, pRequest);
        }

    /* Release the exclusive access */

    OS_RELEASE_EVENT(pHCDPipe->pipeMutex);

    return Status;
    }

/*******************************************************************************
*
* usbXhcdCancelURB - cancel a request to a pipe
*
* This routine is used to cancel a request to the pipe. <uBusIndex> specifies
* the host controller bus index. <uPipeHandle> holds the pipe handle. <pURB>
* pointer to the URB holding the request details.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the URB is cancelled successfully.
*   USBHST_INVALID_PARAMETER - Returned if the parameters are not valid.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbXhcdCancelURB
    (
    UINT8       uBusIndex,   /* Index of the host controller */
    ULONG       uPipeHandle, /* Pipe handle */
    pUSBHST_URB pURB         /* Pointer to the URB */
    )
    {
    /* Pointer to the data structure */

    pUSB_XHCD_DATA          pHCDData;

    /* Pointer to the HCD maintained pipe */

    pUSB_XHCD_PIPE          pHCDPipe;

    /* Pointer to the Request information */

    pUSB_XHCD_REQUEST_INFO  pRequest;

    /* Status of the request */

    USBHST_STATUS           Status = USBHST_SUCCESS;

    /* Status returned */

    STATUS                  uSts = OK;

    /* Transfer Ring */

    pUSB_XHCD_RING          pEpRing;

    /* Dequeue pointer */

    UINT64                  uNewDeqPtr;

    /* Stream Context Type */

    UINT32                  uSCT = 0;

    /* Endpoint State */

    UINT8                   uEpState = 0;

    /* Check the validity of the parameters */

    if ((0 == uPipeHandle) ||
        (NULL == pURB))
        {
        USB_XHCD_ERR("usbXhcdCancelURB - uBusIndex %d "
            "puPipeHandle %p\n",
            uBusIndex, pURB, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the global data structure */

    pHCDData = gpXHCDData[uBusIndex];

    /* Assert if the global pointer is not valid */

    if (NULL == pHCDData)
        {
        USB_XHCD_ERR("usbXhcdCancelURB - pHCDData NULL\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the USB_XHCD_PIPE data structure */

    pHCDPipe = (pUSB_XHCD_PIPE)uPipeHandle;

    /* Check if the request is for the Root hub and route it */

    if ((pHCDPipe->bRootHubPipe == TRUE) ||
        (((pHCDData->pDefaultUSB3Pipe == pHCDPipe) ||
          (pHCDData->pDefaultUSB3Pipe == pHCDPipe)) &&
         ((0 == pHCDData->USB2RH.uDeviceAddress) ||
          (0 == pHCDData->USB3RH.uDeviceAddress))))
        {
        Status = usbXhcdRootHubCancelURB(pHCDData,
                                         uPipeHandle,
                                         pURB);

        return Status;
        }

    if (pHCDPipe->uValidMaggic != USB_XHCD_MAGIC_ALIVE)
        {
        USB_XHCD_ERR("usbXhcdCancelURB - pipe invalid\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Exclusively access the pipe request list */

    (void) OS_WAIT_FOR_EVENT(pHCDPipe->pipeMutex, OS_WAIT_INFINITE);

    /* Get the HCD specific information of this URB */

    pRequest = (pUSB_XHCD_REQUEST_INFO)pURB->pHcdSpecific;

    /* If the request is not found, return an error */

    if (NULL == pRequest)
        {
        USB_EHCD_ERR("usbXhcdCancelURB - Request is not found\n",
            0, 0, 0, 0, 0, 0);

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDPipe->pipeMutex);

        return USBHST_INVALID_REQUEST;
        }

    usbXhcdReleaseRequestInfo(pHCDData, pHCDPipe, pRequest);

    /* Release the exclusive access */

    OS_RELEASE_EVENT(pHCDPipe->pipeMutex);

    /*
     * Aborting a Transfer - If, because of a timeout or other reason,
     * software issued the Stop Endpoint Command to abort the current TD.
     * Then the Set TR Dequeue Pointer Command may be used to force the
     * xHC to dump any internal state that it has for the ring and restart
     * activity at the new Transfer Ring location specified by the Set TR
     * Dequeue Pointer Command.
     */

    uEpState = usbXhcdPipeStateGet(pHCDData, pHCDPipe);

    /* Only when the endpoint is running can we stop it */

    if (uEpState == USB_XHCI_EP_STATE_RUNNING)
        {
        /*
         * Issue Stop Endpoint command with SP bit cleared
         */

        uSts = usbXhcdQueueStopEndpointCmd(pHCDData,
                                           pHCDPipe->uSlotId,
                                           pHCDPipe->uEpId,
                                           0,
                                           USB_XHCD_DEFAULT_CMD_WAIT_TIME);
        uEpState = usbXhcdPipeStateGet(pHCDData, pHCDPipe);
        if ((uSts != OK) ||
            (uEpState != USB_XHCI_EP_STATE_STOPPED))
            {
            USB_XHCD_WARN("usbXhcdCancelURB - "
                         "Stopping pipe for dev %d ep 0x%x fail!\n",
                         pHCDPipe->uDevAddr, pHCDPipe->uEpAddr, 3, 4, 5 ,6);

            return USBHST_FAILURE;
            }
        }
    else if (uEpState == USB_XHCI_EP_STATE_HALTED)
        {
        /*
         * Issue Reset Endpoint command with TSP bit cleared,
         * this will cause the DATA Toggle or Sequence Number
         * to be reset in xHCI.
         */

        uSts = usbXhcdQueueResetEndpointCmd(pHCDData,
                                           pHCDPipe->uSlotId,
                                           pHCDPipe->uEpId,
                                           0,
                                           USB_XHCD_DEFAULT_CMD_WAIT_TIME);
        if (uSts != OK)
            {
            USB_XHCD_WARN("usbXhcdResetPipe - "
                         "Reseting pipe for dev %d ep 0x%x fail!\n",
                         pHCDPipe->uDevAddr, pHCDPipe->uEpAddr, 3, 4, 5 ,6);

            return USBHST_FAILURE;
            }

        }

    if (pURB->uStreamId == 0)
        {
        pEpRing = pHCDPipe->pEpRing;
        }
    else
        {
        if (pURB->uStreamId >= pHCDPipe->streamInfo.uNumStreams)
            {
            USB_XHCD_ERR("usbXhcdCancelURB - "
                "Requested uStreamId %d bigger than supported uNumStreams %d\n",
                pURB->uStreamId,
                pHCDPipe->streamInfo.uNumStreams, 3, 4, 5, 6);

            return USBHST_INVALID_PARAMETER;
            }

        USB_XHCD_DBG("usbXhcdCancelURB - "
            "uDevAddr %d uEpAddr 0x%02x uEpId %d pRequest %p"
            "pURB %p uTransferLength %d\n",
            pHCDPipe->uDevAddr,
            pHCDPipe->uEpAddr,
            pHCDPipe->uEpId,
            pRequest, pURB, pURB->uTransferLength);

        pEpRing = pHCDPipe->streamInfo.ppStreamRings[pURB->uStreamId];

        uSCT = USB_XHCI_SCT_PRIMARY;
        }

    (void) usbXhcdRingReset(pHCDData, pEpRing);

    uNewDeqPtr = pEpRing->pDeqSeg->uHeadTRB;

    uSts = usbXhcdQueueSetTRDeqPtrCmd(pHCDData,
                                  uNewDeqPtr,
                                  pHCDPipe->uSlotId,
                                  pHCDPipe->uEpId,
                                  pURB->uStreamId,
                                  pEpRing->uCycle,
                                  uSCT,
                                  USB_XHCD_DEFAULT_CMD_WAIT_TIME);

    if (uSts != OK)
        {
        USB_XHCD_WARN("usbXhcdQueueSetTRDeqPtrCmd uSlotId %d uEpId %d"
                      "uStreamId %d uCycle %d uSCT %d failed\n",
                                      pHCDPipe->uSlotId,
                                      pHCDPipe->uEpId,
                                      pURB->uStreamId,
                                      pEpRing->uCycle,
                                      uSCT, 6);
        Status = USBHST_FAILURE;
        }

    if ((pURB != NULL) && (pURB->pfCallback != NULL))
        {
        pURB->pfCallback(pURB);
        }

    return Status;
    }

/*******************************************************************************
*
* usbXhcdResetPipe - reset a pipe created already
*
* This routine is used to reset a pipe specific to an endpoint, which cancels
* all the pending requests on the pipe and force the data toggle to return
* DATA0 in the next transfer.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the pipe was reset successfully
*   USBHST_INVALID_PARAMETER - Returned if the parameters are not valid.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbXhcdResetPipe
    (
    pUSB_XHCD_DATA  pHCDData,
    pUSB_XHCD_PIPE  pHCDPipe
    )
    {
    /* Request information */

    pUSB_XHCD_REQUEST_INFO  pRequest = NULL;

    /* Request node */

    NODE * pReqNode = NULL;

    /* Status returned */

    STATUS  uSts = OK;

    /* Endpoint State */

    UINT8   uEpState = USB_XHCI_EP_STATE_DISABLED;

    /* New Dequeue pointer of the Transfer Ring */

    UINT64 uNewDeqPtr;

    /* Pointer to the URB */

    pUSBHST_URB pURB;

    /* Transfer ring of the pipe */

    pUSB_XHCD_RING pEpRing;

    USB_XHCD_WARN("usbXhcdResetPipe - "
                 "Trying to reset pipe for dev %d ep 0x%x!\n",
                 pHCDPipe->uDevAddr, pHCDPipe->uEpAddr, 3, 4, 5 ,6);

    uEpState = usbXhcdPipeStateGet(pHCDData, pHCDPipe);

    if ((uEpState == USB_XHCI_EP_STATE_RUNNING) ||
        (uEpState == USB_XHCI_EP_STATE_DISABLED))
        {
        USB_XHCD_WARN("usbXhcdResetPipe - "
                     "Pipe for dev %d ep 0x%x disabled or still running %d!\n",
                     pHCDPipe->uDevAddr, pHCDPipe->uEpAddr, uEpState, 4, 5 ,6);
        return USBHST_SUCCESS;
        }

    /* Exclusively access the pipe request list */

    (void) OS_WAIT_FOR_EVENT(pHCDPipe->pipeMutex, OS_WAIT_INFINITE);

    /* Return all active requests */

    while ((pReqNode = lstGet(&pHCDPipe->activeReqList)) != NULL)
        {
        /* Cast to the request info */

        pRequest = USB_XHCD_EP_NODE_TO_REQ_INFO(pReqNode);

        /* Save the URB pointer */

        pURB = pRequest->pURB;

        usbXhcdReleaseRequestInfo(pHCDData, pHCDPipe, pRequest);

        if ((pURB != NULL) && (pURB->pfCallback != NULL))
            {
            pURB->pfCallback(pURB);
            }
        }

    USB_XHCD_WARN("usbXhcdResetPipe - "
                 "Reseting pipe for dev %d ep 0x%x!\n",
                 pHCDPipe->uDevAddr, pHCDPipe->uEpAddr, 3, 4, 5 ,6);

    /*
     * Issue Reset Endpoint command with TSP bit cleared,
     * this will cause the DATA Toggle or Sequence Number
     * to be reset in xHCI.
     */

    uSts = usbXhcdQueueResetEndpointCmd(pHCDData,
                                       pHCDPipe->uSlotId,
                                       pHCDPipe->uEpId,
                                       0,
                                       USB_XHCD_DEFAULT_CMD_WAIT_TIME);
    if (uSts != OK)
        {
        USB_XHCD_WARN("usbXhcdResetPipe - "
                     "Reseting pipe for dev %d ep 0x%x fail!\n",
                     pHCDPipe->uDevAddr, pHCDPipe->uEpAddr, 3, 4, 5 ,6);

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDPipe->pipeMutex);

        return USBHST_FAILURE;
        }

    if (pHCDPipe->uErrStreamId == 0)
        {
        pEpRing = pHCDPipe->pEpRing;
        }
    else
        {
        if (pHCDPipe->uErrStreamId >= pHCDPipe->streamInfo.uNumStreams)
            {
            USB_XHCD_ERR("usbXhcdResetPipe - "
                "uErrStreamId %d bigger than supported uNumStreams %d\n",
                pHCDPipe->uErrStreamId,
                pHCDPipe->streamInfo.uNumStreams, 3, 4, 5, 6);

            /* Release the exclusive access */

            OS_RELEASE_EVENT(pHCDPipe->pipeMutex);

            return USBHST_INVALID_PARAMETER;
            }

        USB_XHCD_DBG("usbXhcdResetPipe - "
            "uDevAddr %d uEpAddr 0x%02x uEpId %d uStreamId %d\n",
            pHCDPipe->uDevAddr,
            pHCDPipe->uEpAddr,
            pHCDPipe->uEpId,
            pHCDPipe->uErrStreamId, 5, 6);

        pEpRing = pHCDPipe->streamInfo.ppStreamRings[pHCDPipe->uErrStreamId];
        }

    (void) usbXhcdRingReset(pHCDData, pEpRing);

    uNewDeqPtr = pHCDPipe->pEpRing->pDeqSeg->uHeadTRB;

    uSts = usbXhcdQueueSetTRDeqPtrCmd(pHCDData,
                                  uNewDeqPtr,
                                  pHCDPipe->uSlotId,
                                  pHCDPipe->uEpId,
                                  pHCDPipe->uErrStreamId,
                                  pEpRing->uCycle,
                                  0,
                                  USB_XHCD_DEFAULT_CMD_WAIT_TIME);

    if (uSts != OK)
        {
        USB_XHCD_ERR("usbXhcdCreatePipe - "
                     "could not set new Deq Ptr for control EP for dev 0x%x\n",
                     pHCDPipe->uEpAddr, 2, 3, 4, 5 ,6);

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDPipe->pipeMutex);

        return USBHST_FAILURE;
        }

    /* Release the exclusive access */

    OS_RELEASE_EVENT(pHCDPipe->pipeMutex);

    USB_XHCD_WARN("usbXhcdResetPipe - "
                 "Reset pipe completed for dev %d ep 0x%x uNewDeqPtr @%p uCycle %d!\n",
                 pHCDPipe->uDevAddr, pHCDPipe->uEpAddr,
                 uNewDeqPtr, pEpRing->uCycle, 5 ,6);

    return USBHST_SUCCESS;
    }

/*******************************************************************************
*
* usbXhcdPipeControl - control the pipe characteristics
*
* This routine is used to control the pipe characteristics.
*
* <uBusIndex> specifies the host controller bus index.
*
* <uPipeHandle> holds the pipe handle.
*
* <pPipeCtrl> is the pointer to the USBHST_PIPE_CONTROL_INFO holding
*             the request details.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the pipe control is done successfully.
*   USBHST_INVALID_PARAMETER - Returned if the parameters are not valid.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbXhcdPipeControl
    (
    UINT8  uBusIndex,                   /* Index of the host controller */
    ULONG  uPipeHandle,                 /* Pipe handle */
    USBHST_PIPE_CONTROL_INFO * pPipeCtrl /* Pointer to the pipe control info */
    )
    {
    /* Pointer to the HCD maintained pipe */

    pUSB_XHCD_PIPE pHCDPipe = NULL;

    /* Pointer to the HCD specific data structure */

    pUSB_XHCD_DATA  pHCDData = NULL;

    /* The control result */

    USBHST_STATUS   hstSts = USBHST_FAILURE;

    /* Check the validity of the parameters */

    if ((0 == uPipeHandle) ||
        (NULL == pPipeCtrl))
        {
        USB_XHCD_ERR("usbXhcdPipeControl - parameters are not valid\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the global data structure */

    pHCDData = gpXHCDData[uBusIndex];

    /* Return if the global pointer is not valid */

    if (NULL == pHCDData)
        {
        USB_XHCD_ERR("usbXhcdPipeControl - pHCDData is NULL\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the USB_XHCD_PIPE data structure */

    pHCDPipe = (pUSB_XHCD_PIPE)uPipeHandle;

    /* Do not need to control root hub pipe */

    if (pHCDPipe->uDevAddr == USB_ROOT_HUB_ADDRESS)
        {
        USB_XHCD_WARN("usbXhcdPipeControl - Do not control Root Hub Pipe!\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_SUCCESS;
        }

    /* Do the pipe control based on the control code selection */

    switch (pPipeCtrl->uControlCode)
        {
        case USBHST_CMD_PIPE_RESET:
            {
            /* Exclusively access the pipe request list */

            (void) OS_WAIT_FOR_EVENT(pHCDPipe->pipeMutex, OS_WAIT_INFINITE);

            /*
             * Do pipe reset so that the pipe will be in DATA0 state
             * in next transfer
             */

            hstSts = usbXhcdResetPipe(pHCDData, pHCDPipe);

            /* Release the exclusive access */

            OS_RELEASE_EVENT(pHCDPipe->pipeMutex);
            }

            break;

        case USBHST_CMD_TRANSFER_SETUP:
            {
            pUSB_TRANSFER_SETUP_INFO pSetupInfo;

            /* For non-root hub device the setup should really happen */

            pSetupInfo = (pUSB_TRANSFER_SETUP_INFO)pPipeCtrl->uControlParam;

            if (pSetupInfo == NULL)
                {
                USB_XHCD_ERR("usbXhcdPipeControl - "
                    "USBHST_CMD_TRANSFER_SETUP pSetupInfo NULL\n",
                    0, 0, 0, 0, 0, 0);

                return USBHST_INVALID_PARAMETER;
                }

            USB_XHCD_DBG(
                "usbXhcdPipeControl - USBHST_CMD_TRANSFER_SETUP EP 0x%02x - "
                "uMaxTransferSize %p uMaxNumReqests %d uFlags %p\n",
                pHCDPipe->uEpAddr,
                pSetupInfo->uMaxTransferSize,
                pSetupInfo->uMaxNumReqests,
                pSetupInfo->uFlags, 0, 0);

            /* Exclusively access the pipe request list */

            (void) OS_WAIT_FOR_EVENT(pHCDPipe->pipeMutex, OS_WAIT_INFINITE);

            /*
             * Set up the transfer characteristics such as
             * creating proper DMA TAG and DMA MAPs
             */

            if (usbXhcdSetupPipe(pHCDData, pHCDPipe, pSetupInfo) == OK)
                hstSts = USBHST_SUCCESS;

            /* Release the exclusive access */

            OS_RELEASE_EVENT(pHCDPipe->pipeMutex);
            }

            break;

        default :

            break;
        }

    return hstSts;
    }

/*******************************************************************************
*
* usbXhcdDeviceControl - control the device characteristics
*
* This routine is used to control the device characteristics.
*
* <uBusIndex> specifies the host controller bus index.
* <pDeviceCtrl> is the pointer to the USBHST_DEVICE_CONTROL_INFO holding
*             the request details.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the control operation is done successfully.
*   USBHST_INVALID_PARAMETER - Returned if the parameters are not valid.
*   USBHST_INVALID_REQUEST - Returned if the request is not valid
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbXhcdDeviceControl
    (
    UINT8                        uBusIndex,  /* Index of host controller */
    USBHST_DEVICE_CONTROL_INFO * pDeviceCtrl /* Pointer to Device control info */
    )
    {
    /* To hold the pointer to the data structure */

    pUSB_XHCD_DATA          pHCDData;

    /* To hold the control status */

    USBHST_STATUS           Status = USBHST_FAILURE;

    /* Check the validity of the parameters */

    if (pDeviceCtrl == NULL)
        {
        USB_XHCD_ERR("usbXhcdDeviceControl - "
            "uBusIndex %d pDeviceCtrl %p\n",
            uBusIndex, pDeviceCtrl, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the global data structure */

    pHCDData = gpXHCDData[uBusIndex];

    /* Assert if the global pointer is not valid */

    if (NULL == pHCDData)
        {
        USB_XHCD_ERR("usbXhcdDeviceControl - pHCDData NULL\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    if (pDeviceCtrl->uDeviceAddress == USB_ROOT_HUB_ADDRESS)
        {
        USB_XHCD_WARN("usbXhcdDeviceControl - Do not control Root Hub 1!\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_REQUEST;
        }

    if (pDeviceCtrl->uDeviceSpeed == USBHST_SUPER_SPEED)
        {
        if (pHCDData->USB3RH.uDeviceAddress == 0)
            {
            USB_XHCD_WARN("usbXhcdDeviceControl - Do not control Root Hub 2!\n",
                0, 0, 0, 0, 0, 0);

            return USBHST_INVALID_REQUEST;
            }
        }
    else
        {
        if (pHCDData->USB2RH.uDeviceAddress == 0)
            {
            USB_XHCD_WARN("usbXhcdDeviceControl - Do not control Root Hub 3!\n",
                0, 0, 0, 0, 0, 0);

            return USBHST_INVALID_REQUEST;
            }
        }

#if defined (_WRS_CONFIG_PWR_MGMT) && defined (_WRS_ARCH_HAS_DEV_PWR_MGMT)

        /* If device is OFF */

        if (PWR_DEVICE_UTILIZATION_SET(pHCDData->pDev->pwrDeviceId) != OK)
            usbXhcdPwrStateSet (pHCDData->pDev, 0);   /* turn device on */

#endif /* (_WRS_CONFIG_PWR_MGMT) && (_WRS_ARCH_HAS_DEV_PWR_MGMT) */

    switch (pDeviceCtrl->uControlCode)
        {
        case USBHST_CMD_RESERVE_DEVICE:
            {
            /* Initialize the device slot structure */

            if (usbXhcdDeviceSlotInit(pHCDData,
                                      pDeviceCtrl->uRootHubPort,
                                      pDeviceCtrl->uRouteString,
                                      pDeviceCtrl->uTTHubAddress,
                                      pDeviceCtrl->uTTHubPort,
                                      pDeviceCtrl->uDeviceSpeed,
                                      &pDeviceCtrl->uDeviceAddress) != OK)
                {
                USB_XHCD_ERR("usbXhcdDeviceControl - "
                    "usbXhcdDeviceSlotInit fail\n",
                    0, 0, 0, 0, 0, 0);

                break;
                }

            USB_XHCD_DBG("usbXhcdDeviceControl - USBHST_CMD_RESERVE_DEVICE - "
                "Got Device Slot ID %d and Device Address %d\n",
                pHCDData->uNewSlotId,
                pDeviceCtrl->uDeviceAddress, 0, 0, 0, 0);

            pDeviceCtrl->nStatus = USBHST_SUCCESS;

            Status = USBHST_SUCCESS;
            }
            break;
        case USBHST_CMD_RELEASE_DEVICE:
            {
            pUSB_XHCD_DEVICE pHCDDevice;
            UINT32 uIndex;

            USB_XHCD_WARN("usbXhcdDeviceControl - USBHST_CMD_RELEASE_DEVICE - "
                "Releasing Device %d\n",
                pDeviceCtrl->uDeviceAddress, 2, 3, 4, 5, 6);

            /*
             * Upon here, we should already have a device structure
             * created for that device during the initial steps!
             */

            pHCDDevice = usbXhcdDeviceGet(pHCDData,
                                          pDeviceCtrl->uDeviceAddress,
                                          pDeviceCtrl->uDeviceSpeed);

            if (pHCDDevice == NULL)
                {
                USB_XHCD_ERR("usbXhcdDeviceControl - "
                             "usbXhcdDeviceGet for dev %d fail\n",
                             pDeviceCtrl->uDeviceAddress, 2, 3, 4, 5 ,6);

                return USBHST_INVALID_PARAMETER;
                }

            /* De-Configure the device slot */

            if ((pHCDDevice->bIsConfiged == TRUE) &&
                (usbXhcdQueueConfigureEndpointCmd(pHCDData,
                 pHCDDevice->pIntputCtx->uCtxBusAddr,
                 pHCDDevice->uSlotId,
                 1,
                 USB_XHCD_DEFAULT_CMD_WAIT_TIME) != OK))
                {
                USB_XHCD_WARN("usbXhcdDeviceControl - "
                    "usbXhcdQueueConfigureEndpointCmd - DC fail\n",
                    1, 2, 3, 4, 5, 6);
                Status = USBHST_FAILURE;
                }
            else
                {
                Status = USBHST_SUCCESS;
                }

            /* Disable the Slot */

            if (usbXhcdQueueDisableSlotCmd(pHCDData,
                pHCDDevice->uSlotId,
                USB_XHCD_DEFAULT_CMD_WAIT_TIME) != OK)
                {
                USB_XHCD_WARN("usbXhcdDeviceControl - "
                    "usbXhcdQueueDisableSlotCmd failed\n",
                    1, 2, 3, 4, 5, 6);
                Status = USBHST_FAILURE;
                }
            else
                {
                Status = USBHST_SUCCESS;
                }

            /* Save the Slot ID for latter use */

            uIndex = pHCDDevice->uSlotId;

            /* Destroy the device structure */

            if (usbXhcdDeviceDestroy(pHCDData, pHCDDevice) != OK)
                {
                USB_XHCD_WARN("usbXhcdDeviceControl - "
                    "usbXhcdDeviceDestroy failed\n",
                    1, 2, 3, 4, 5, 6);
                }

            pHCDData->ppDevSlots[uIndex] = NULL;
            pHCDData->pDCBAA[uIndex] = 0ULL;

            pDeviceCtrl->nStatus = Status;
            }
            break;
        case USBHST_CMD_HS_HUB_TT_MODE:
            {
            USB_XHCD_WARN("usbXhcdDeviceControl - USBHST_CMD_HS_HUB_TT_MODE - "
                "Hub Device %d is updated with uTTHubNumPorts (%d) MTT (%d) TTT (%d)\n",
                pDeviceCtrl->uDeviceAddress,
                pDeviceCtrl->uTTHubNumPorts,
                pDeviceCtrl->uMTT,
                pDeviceCtrl->uTTT, 0, 0);

            if (usbXhcdHubDeviceUpdate(pHCDData,
                                   pDeviceCtrl->uDeviceAddress,
                                   pDeviceCtrl->uTTHubNumPorts,
                                   pDeviceCtrl->uMTT,
                                   pDeviceCtrl->uTTT,
                                   pDeviceCtrl->uDeviceSpeed) != OK)
                {
                USB_XHCD_ERR("usbXhcdDeviceControl - "
                    "usbXhcdHubDeviceUpdate fail\n",
                    0, 0, 0, 0, 0, 0);

                break;
                }

            pDeviceCtrl->nStatus = USBHST_SUCCESS;

            Status = USBHST_SUCCESS;
            }
        default: break;
        }

    return Status;
    }


