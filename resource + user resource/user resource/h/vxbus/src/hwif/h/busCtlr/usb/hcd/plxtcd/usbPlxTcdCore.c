/* usbPlxTcdCore.c - USB PLX TCD core module */

/*
 * Copyright (c) 2012-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01f,03may13,wyy  Remove compiler warning (WIND00356717)
01e,21nov12,s_z  Add Reset exit workaround based Errata Issue_4
                 Reset squeuence number after endpoint halt (WIND00389964)
01d,23oct12,s_z  Add U1/U2 rejection workaround based Errata (WIND00382685)
01c,10oct12,s_z  Correct feature clear issue (WIND00382688)
01b,18sep12,s_z  Pass CV2.0 testing (WIND00375402)
01a,18may12,s_z  Writen
*/

/*
DESCRIPTION

This file provides core functions for the USB PLX Target Controller
Driver. All the routines will be filled to the USBTGT_TCD_FUNCS
structure and register to the TML.

INCLUDE FILES: usbPlxTcd.h, usbPlxTcd.h
*/

/* includes */


#include <usbPlxTcd.h>
#include <usbPlxTcdUtil.h>

/* Function declartion */

LOCAL STATUS usbPlxTcdCreatePipe
    (
    pUSBTGT_TCD         pTcd,
    pUSB_ENDPOINT_DESCR pEndpointDesc,
    UINT16              uConfigurationValue,
    UINT16              uInterface,
    UINT16              uAltSetting,
    pUSB_TARG_PIPE      pPipeHandle
    );
LOCAL STATUS usbPlxTcdDeletePipe
    (
    pUSBTGT_TCD         pTcd,
    USB_TARG_PIPE       pipeHandle
    );
LOCAL STATUS usbPlxTcdSubmitErp
    (
    pUSBTGT_TCD         pTcd,
    USB_TARG_PIPE       pipeHandle,
    pUSB_ERP            pErp
    );
LOCAL STATUS usbPlxTcdCancelErp
    (
    pUSBTGT_TCD         pTcd,
    USB_TARG_PIPE       pipeHandle,
    pUSB_ERP            pErp
    );
LOCAL STATUS usbPlxTcdPipeStatusSet
    (
    pUSBTGT_TCD         pTcd,
    USB_TARG_PIPE       pipeHandle,
    UINT16              uStatus
    );
LOCAL STATUS usbPlxTcdPipeStatusGet
    (
    pUSBTGT_TCD         pTcd,
    USB_TARG_PIPE       pipeHandle,
    UINT16 *            pStatus
    );
LOCAL STATUS usbPlxTcdRemoteWakeUp
    (
    pUSBTGT_TCD pTcd
    );
LOCAL STATUS usbPlxTcdGetFrameNum
    (
    pUSBTGT_TCD pTcd,
    UINT16 *    pFrameNum
    );
LOCAL STATUS usbPlxTcdSoftConnect
    (
    pUSBTGT_TCD pTcd,
    BOOL        isConnectUp
    );
LOCAL int usbPlxTcdIoctl
    (
    pUSBTGT_TCD pTcd,
    int         cmd,
    void *      pContext
    );

/* glolabs */

USBTGT_TCD_FUNCS gUsbPlxTcdDriver =
    {
    usbPlxTcdCreatePipe,    /* pTcdCreatePipe */
    usbPlxTcdDeletePipe,    /* pTcdDeletePipe */
    usbPlxTcdSubmitErp,     /* pTcdSubmitErp */
    usbPlxTcdCancelErp,     /* pTcdCancelErp */
    usbPlxTcdPipeStatusSet, /* pTcdPipeStatusSet */
    usbPlxTcdPipeStatusGet, /* pTcdPipeStatusGet */
    usbPlxTcdRemoteWakeUp,  /* pTcdWakeUp */
    usbPlxTcdGetFrameNum,   /* pTcdGetFrameNum */
    usbPlxTcdSoftConnect,   /* pTcdSoftConnect */
    usbPlxTcdIoctl          /* pTcdIoctl */
    };

/*******************************************************************************
*
* usbPlxTcdCreatePipe - create a pipe specific to an endpoint
*
* This routine creates a pipe specific to an endpoint. By defalut, if this
* endpoint support DMA, get one DMA channel.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbPlxTcdCreatePipe
    (
    pUSBTGT_TCD         pTcd,               /* TCD data pointer */
    pUSB_ENDPOINT_DESCR pEndpointDesc,      /* USB_ENDPOINT_DESCR */
    UINT16              uConfigurationValue,/* configuration value */
    UINT16              uInterface,         /* Number of the interface index */
    UINT16              uAltSetting,        /* Alternate Setting */
    pUSB_TARG_PIPE      pPipeHandle         /* Pointer to pipe handle */
    )
    {
    pUSB_PLX_CTRL       pPlxCtrl = NULL;
    pUSB_PLX_TCD_DATA   pTCDData = NULL;
    pUSB_PLX_TCD_PIPE   pTCDPipe = NULL;
    pUSBTGT_PIPE        pUsbTgtPipe = NULL;
    pUSB_ENDPOINT_COMPANION_DESCR pEpComp = NULL;

    /* Validate parameters */

    if ((NULL == pTcd) ||
        (NULL == (pTCDData = (pUSB_PLX_TCD_DATA)pTcd->pTcdSpecific)) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)) ||
        (NULL == pEndpointDesc) ||
        (NULL == pPipeHandle))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTcd) ? "pTcd" :
                     (NULL == pTCDData) ? "pTcdSpecific" :
                     (NULL == pPlxCtrl) ? "pTCDData->pPlxCtrl" :
                     (NULL == pEndpointDesc) ? "pEndpointDesc" :
                     "pPipeHandle" ),
                    2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Alloc the USB_PLX_TCD_PIPE data structure */

    pTCDPipe = OSS_CALLOC(sizeof(USB_PLX_TCD_PIPE));

    if (pTCDPipe == NULL)
        {
        USB_PLX_ERR("Malloc pTCDPipe fail\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Init requests list, TODO, USE DMA buffer ?? */

    lstInit(&(pTCDPipe->pendingReqList));
    lstInit(&(pTCDPipe->freeReqList));

    /* Create the mutex to protect all the resource of the pipe structure */

    pTCDPipe->pPipeSyncMutex = semMCreate(SEM_Q_PRIORITY |
                                          SEM_INVERSION_SAFE |
                                          SEM_DELETE_SAFE);

    if (pTCDPipe->pPipeSyncMutex ==  OS_INVALID_EVENT_ID)
        {
        USB_PLX_ERR("Create pTCDPipe->pPipeSyncMutex fail\n",
                    1, 2, 3, 4, 5, 6);

        OSS_FREE(pTCDPipe);

        return ERROR;
        }

    pTCDPipe->pUsbTgtPipe = pUsbTgtPipe;
    pTCDPipe->pTCDData = pTCDData;

    pTCDPipe->uMaxPacketSize = (UINT16)(USB_MAX_PACKET_SIZE_MASK &
                      OS_UINT16_LE_TO_CPU(pEndpointDesc->maxPacketSize));

    /* Hard code ~~ USB3 host will define this */

    /*
     * If the pipe is for the USB 3.0 control pipe
     * According to the USB 3.0 Spec, the maxPacketSize0 = 9
     * indicates that this is USB 3.0 device and the
     * max packet size should be 512 bytes.
     */

    if ((0x09 == pTCDPipe->uMaxPacketSize) &&
        (USB_ATTR_CONTROL == pTCDPipe->uEpType))
        {
        pTCDPipe->uMaxPacketSize = USB_MAX_HIGH_SPEED_BULK_SIZE; /* 512 */
        }

    USB_PLX_DBG ("usbPlxTcdCreatePipe pTCDPipe->uMaxPacketSize %d\n",
                  pTCDPipe->uMaxPacketSize, 2, 3, 4, 5, 6);

    /*
     * It used for record the max burst of bulk endpoint
     * or number of transaction of ISO endpoint
     */

    pTCDPipe->pEndpointDesc = pEndpointDesc;

    /* Get the endpoint address */

    pTCDPipe->uEpAddr = pEndpointDesc->endpointAddress;
    pTCDPipe->uEpDir = (UINT8)(pEndpointDesc->endpointAddress & USB_ENDPOINT_DIR_MASK);
    pTCDPipe->uEpType = (UINT8)(pEndpointDesc->attributes & USB_ATTR_EPTYPE_MASK);

    /* The Usage type bit 5:4 */

    pTCDPipe->uUsage = (UINT8)(USB_ATTR_EP_USAGE_TYPE_MASK &
                (pEndpointDesc->attributes >> USB_ATTR_EP_USAGE_TYPE_OFFSET));

    /* The synch type bit 3:2 */

    pTCDPipe->uSynchType = (UINT8)(USB_ATTR_EP_SYNCH_TYPE_MASK &
                (pEndpointDesc->attributes >> USB_ATTR_EP_SYNCH_TYPE_OFFSET));

    pTCDPipe->uDmaChannel = USB_PLX_DMA_CH_IVALID;


    /* Get the companion descriptor for this endpoint */

    pEpComp = (pUSB_ENDPOINT_COMPANION_DESCR) ((UINT8 *)pEndpointDesc +
                                                pEndpointDesc->length);

    /* Is this endpoint has one valid companion descriptor ? */

    if ((NULL != pEpComp) &&
        (USB_ENDPOINT_COMP_DESCR_LEN == pEpComp->bLength) &&
        (USB_DESCR_SS_ENDPOINT_COMPANION == pEpComp->bDescriptorType) &&
        (USB_SPEED_SUPER == pTcd->uSpeed))
        {
        /*
         * Get the max number of packets the endpoint
         * can send or receive as part of burst. Valid
         * valueds are zero based from 0 - 15.
         */

        pTCDPipe->uMaxBurst = pEpComp->bMaxBurst;

        /* Periodic endpoint */

        if ((pTCDPipe->uEpType == USB_ATTR_INTERRUPT) ||
            (pTCDPipe->uEpType == USB_ATTR_ISOCH))
            {
            /* bmAttributes
             *
             * For periodic endpoint.
             * A zero based value deternines the max
             * packet number within a service interval.
             * 0 - 2 is valid.
             */

            pTCDPipe->uMult = pEpComp->bmAttributes;

            /*
             * wBytesPerInterval
             *
             * This value is only used for periodic endpoint.
             * The total bytes this endpoint will transfer every
             * service interval.
             * For ISO endpoint this value is used to reserve the
             * bus time in the schedule, required for the frame data
             * payload per 125us.
             */

            pTCDPipe->uBytesPerInterval = pEpComp->wBytesPerInterval;
            }

        /* Bulk endpoint */

        if (pTCDPipe->uEpType == USB_ATTR_BULK)
            {
            /* bmAttributes
             *
             * For bulk endpoint.
             * This value deternines the max streams number
             * this endpoint supports.
             * 0 - 16 are valid.
             *
             * 0      : this endpoint do not define streams.
             * 1 - 16 : the number of streams supported
             *          equals 2 ^ uMaxStreams.
             */

            pTCDPipe->uMaxStreams = pEpComp->bmAttributes;
            }
        }
    else
        {
        /*
         * Do not have a valid companion endpoint descriptor
         * or the hardware is no super speed.
         */

        if ((pTCDPipe->uEpType == USB_ATTR_INTERRUPT) ||
            (pTCDPipe->uEpType == USB_ATTR_ISOCH))
            {
            pTCDPipe->uMult =
                    (UINT8)((OS_UINT16_LE_TO_CPU(pEndpointDesc->maxPacketSize) &
                                     USB_NUMBER_OF_TRANSACTIONS_MASK) >>
                                     USB_NUMBER_OF_TRANSACTIONS_BITPOSITION);
            }
        }

    /* Record the configuration value */

    pTCDPipe->uConfigValue = (UINT8)uConfigurationValue;
    pTCDPipe->uInterface = (UINT8)uInterface;
    pTCDPipe->uAltSetting = (UINT8)uAltSetting;
    pTCDPipe->status = USBTGT_PIPE_STATUS_UNSTALL;

    /* TODO: Create DMA buffer tag over DMA buffer lib */

    /* TODO: Create requests for this pipe */

    /* Program the related Endpoint let them prepare for transfer */

     if (ERROR == usbPlxTcdPipePhyEpAssign(pTCDData, pTCDPipe))
        {
        semDelete(pTCDPipe->pPipeSyncMutex);

        OS_FREE(pTCDPipe);

        return ERROR;
        }

    pTCDPipe->uDmaChannel = pTCDPipe->uPhyEpAddr;

    /* Add the pipe into the tcd pipe list */

    OS_WAIT_FOR_EVENT(pTCDData->tcdSyncMutex, WAIT_FOREVER);

    if (ERROR == lstFind(&(pTCDData->tcdPipeList), &(pTCDPipe->pipeNode)))
        lstAdd(&(pTCDData->tcdPipeList), &(pTCDPipe->pipeNode));

    OS_RELEASE_EVENT(pTCDData->tcdSyncMutex);

    /* Set the TCD pipe handle */

    * pPipeHandle = pTCDPipe;

    USB_PLX_DBG("usbPlxTcdCreatePipe(): Created pipe %p for "
                "EpAddr 0x%X Dir 0x%X Type 0x%X max %x\n",
                pTCDPipe, pTCDPipe->uEpAddr, pTCDPipe->uEpDir,
                pTCDPipe->uEpType, pTCDPipe->uMaxPacketSize ,6);

    return OK;
    }

/*******************************************************************************
*
* usbPlxTcdDeletePipe - delete a pipe created already
*
* This routine deletes a pipe specific to an endpoint.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbPlxTcdDeletePipe
    (
    pUSBTGT_TCD       pTcd,       /* TCD data pointer */
    USB_TARG_PIPE     pipeHandle  /* Pointer to pipe handle */
    )
    {
    pUSB_PLX_CTRL     pPlxCtrl = NULL;
    pUSB_PLX_TCD_DATA pTCDData = NULL;
    pUSB_PLX_TCD_PIPE pTCDPipe = NULL;

    /* Validate parameters */

    if ((NULL == pTcd) ||
        (NULL == (pTCDData = (pUSB_PLX_TCD_DATA)pTcd->pTcdSpecific)) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)) ||
        (NULL == (pTCDPipe = (pUSB_PLX_TCD_PIPE)pipeHandle)))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTcd) ? "pTcd" :
                     (NULL == pTCDData) ? "pTcdSpecific" :
                     (NULL == pPlxCtrl) ? "pTCDData->pPlxCtrl" :
                     "pTCDPipe"),
                    2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Another task is tring to delete it */

    if (0 != (pTCDPipe->uPipeFlag & USBTGT_PIPE_STATUS_DELETING))
        {
        USB_PLX_ERR("This pipe already in process of delete\n",
                     1, 2, 3, 4, 5, 6);

        return OK;
        }

    /*
     * Take the pipeSyncMutex to make sure the request list cann't be
     * accessed by other task
     * NOTE: In usbPlxTcdPipeFlush this mutex will be taken to remove
     * request list
     */

    OS_WAIT_FOR_EVENT(pTCDPipe->pPipeSyncMutex, WAIT_FOREVER);

    USB_PLX_DBG("usbPlxTcdDeletePipe(): Delete pipe %p for Ep %x \n",
                 pTCDPipe, pTCDPipe->uEpAddr, 3, 4, 5, 6);

    /* Update pipeflag to indicate the pipe to be deleted */

    pTCDPipe->uPipeFlag |= USBTGT_PIPE_STATUS_DELETING;

    /* Flush the pipe to cancel all the requests */

    USB_PLX_VDBG("usbPlxTcdDeletePipe(): Flush pipe and release Ep\n",
                 1, 2, 3, 4, 5, 6);

    (void)usbPlxTcdPipeFlush(pTCDPipe, S_usbTgtTcdLib_ERP_CANCELED);

    /* Release the hardware endpoint */

    (void)usbPlxTcdPipePhyEpRelease(pTCDData, pTCDPipe);

    /* Remove the pipe form the pipelist */

    USB_PLX_VDBG("usbPlxTcdDeletePipe(): Delete the pipe from the pipe list\n",
                 1, 2, 3, 4, 5, 6);

    /* Delete the pipe from the list */

    OS_WAIT_FOR_EVENT(pTCDData->tcdSyncMutex, WAIT_FOREVER);

    if (ERROR != lstFind(&(pTCDData->tcdPipeList), &(pTCDPipe->pipeNode)))
        lstDelete(&(pTCDData->tcdPipeList), &(pTCDPipe->pipeNode));

    OS_RELEASE_EVENT(pTCDData->tcdSyncMutex);

    USB_PLX_VDBG("usbPlxTcdDeletePipe(): Free the resource\n",
                 1, 2, 3, 4, 5, 6);

    /* Delete the mutex */

    semDelete(pTCDPipe->pPipeSyncMutex);
    pTCDPipe->pPipeSyncMutex = NULL;

    /* Release the pipe resource */

    OS_FREE(pTCDPipe);

    return OK;
    }

/*******************************************************************************
*
* usbPlxTcdSubmitErp - submit an ERP request to a pipe
*
* This routine is used to submit an ERP request to the pipe.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbPlxTcdSubmitErp
    (
    pUSBTGT_TCD          pTcd,       /* TCD data pointer */
    USB_TARG_PIPE        pipeHandle, /* Pointer to pipe handle */
    pUSB_ERP             pErp        /* ERP pointer */
    )
    {
    pUSB_PLX_TCD_DATA    pTCDData = NULL;
    pUSB_PLX_TCD_PIPE    pTCDPipe = NULL;
    pUSB_PLX_TCD_REQUEST pRequest = NULL;
    NODE *               pNode    = NULL;
    UINT32               uXferLen = 0;
    int i;

    /* Validate parameters */

    if ((NULL == pTcd) ||
        (NULL == (pTCDData = (pUSB_PLX_TCD_DATA)pTcd->pTcdSpecific)) ||
        (NULL == (pTCDPipe = (pUSB_PLX_TCD_PIPE)pipeHandle)) ||
        (NULL == pErp))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTcd) ? "pTcd" :
                     (NULL == pTCDData) ? "pTcdSpecific" :
                     (NULL == pTCDPipe) ? "pTCDPipe" :
                     "pErp"),
                    2, 3, 4, 5, 6);

        return ERROR;
        }

    USB_PLX_VDBG("usbPlxTcdSubmitErp() pErp %p pTCDPipe %p, uEpAddr 0x%X\n",
                 pErp, pTCDPipe, pTCDPipe->uEpAddr, 4, 5, 6);

    /* If this pipe will be deleted ? */

    if (0 != (pTCDPipe->uPipeFlag & USBTGT_PIPE_STATUS_DELETING))
        {
        USB_PLX_ERR("Submit Erp to delete pipe %p, dir %d, ep %d\n",
                    pTCDPipe, pTCDPipe->uEpDir, pTCDPipe->uEpAddr, 4, 5, 6);

        return ERROR;
        }

    /* Check if all the buffer for this erp are right */

    for (i = 0; i < pErp->bfrCount; i++)
        {
        if ((pErp->bfrList[i].bfrLen != 0) &&
            (pErp->bfrList[i].pBfr == NULL))
            {
            USB_PLX_ERR("Invalid Paramter: pBfr[%d] is NULL but bfrLen %d\n",
                        i, pErp->bfrList[i].bfrLen, 3, 4, 5, 6);

            return ERROR;
            }
        uXferLen += pErp->bfrList[i].bfrLen;
        }

    /*
     * TODO: Single transfer mode or scatter/gather mode
     * Only use scatter/gather DMA for TX endpoint
     */

    if ((pTCDData->uDmaType == USB_PLX_SG_DMA) &&
        (pTCDPipe->uEpType != USB_ATTR_CONTROL) &&
        (pTCDPipe->uEpDir == USB_ENDPOINT_IN) &&
        (uXferLen > USB_PLX_DMA_DOOR_KEEPER))
        {
        pRequest = usbPlxTcdRequestReserve(pTCDPipe);

        if (NULL == pRequest)
            {
            USB_PLX_ERR("usbPlxRequestReserve pRequest fail\n",
                        1, 2, 3, 4, 5, 6);

            return ERROR;
            }

        /* Check the scater/gather dma descripter buffer */

        if ((pRequest->uErpBufCount < pErp->bfrCount) &&
            (NULL != pRequest->pScatDesc))
            {
            OSS_FREE (pRequest->pScatDesc);
            pRequest->pScatDesc = NULL;
            pRequest->uErpBufCount = 0;
            }

        if (NULL == pRequest->pScatDesc)
            {
            pRequest->pScatDesc = (USB_PLX_SCAT_DMA_DESC *)OSS_CALLOC
                                (pErp->bfrCount * sizeof (USB_PLX_SCAT_DMA_DESC));

            if (NULL == pRequest->pScatDesc)
                {
                (void)usbPlxTcdRequestRelease(pTCDPipe, pRequest);

                return ERROR;
                }
            pRequest->uErpBufCount = pErp->bfrCount;
            }

        for (i = 0; i < pErp->bfrCount; i++)
            {
            /* The transaction count */

            pRequest->pScatDesc[i].uDmaConfig = USB_PLX_DMADESC_VALID |
                                                pErp->bfrList[i].bfrLen;

            /* The transaction dir */

            pRequest->pScatDesc[i].uDmaConfig |= USB_PLX_DMADESC_DIR;

            /* The caller has prepare the right buffer address */

            pRequest->pScatDesc[i].uStartAddr = (UINT32)((ULONG)pErp->bfrList[i].pBfr);

            cacheFlush(DATA_CACHE,
                       (void *)(pErp->bfrList[i].pBfr),
                       pErp->bfrList[i].bfrLen);

            if (i < pErp->bfrCount - 1)
                {
                pRequest->pScatDesc[i].uNextDesc =
                                      (UINT32)((ULONG)(&pRequest->pScatDesc[i + 1]));
                }
            else
                {
                /* The last one */

                pRequest->pScatDesc[i].uNextDesc = 0;
                pRequest->pScatDesc[i].uDmaConfig |= USB_PLX_DMADESC_END_CHAIN |
                                                    USB_PLX_DMADESC_DONE_INT_EN;

                /* We have a short packet as the end */

                if ((pRequest->uXferSize + pErp->bfrList[i].bfrLen) %
                     pTCDPipe->uMaxPacketSize)
                    {
                    pRequest->pScatDesc[i].uDmaConfig |= USB_PLX_DMADESC_FIFO_VALID;
                    }
                }

            pRequest->uXferSize += pErp->bfrList[i].bfrLen;
            }

        pRequest->pErp = pErp;
        pRequest->uActLength = 0;
        pRequest->pCurrentBuffer = NULL;
        pRequest->bDmaActive = FALSE;

        USB_PLX_DBG("usbPlxTcdSubmitErp():Reserve Request %p for pErp %p "
                    "uXferSize %x pErp->bfrCount %x\n",
                    pRequest, pErp, pRequest->uXferSize,
                    pErp->bfrCount, 5, 6);

        /*
         * Add request into the pipe list
         */

        OS_WAIT_FOR_EVENT(pTCDPipe->pPipeSyncMutex, WAIT_FOREVER);

        /* Add the request into pending list */

        if (ERROR == lstFind(&(pTCDPipe->pendingReqList), &(pRequest->reqNode)))
            {
            pRequest->pTCDPipe = pTCDPipe;
            lstAdd(&(pTCDPipe->pendingReqList), &(pRequest->reqNode));
            }
        OS_RELEASE_EVENT(pTCDPipe->pPipeSyncMutex);

        }
    else
        {
        USB_PLX_DBG("usbPlxTcdSubmitErp() Prepare requests\n",
                    1, 2, 3, 4, 5, 6);

        for (i = 0; i < pErp->bfrCount; i++)
            {
            pRequest = usbPlxTcdRequestReserve(pTCDPipe);

            if (pRequest == NULL)
                {
                USB_PLX_ERR("usbPlxRequestReserve pRequest fail\n",
                            1, 2, 3, 4, 5, 6);

                /* Release requests for this ERP */

                pNode = lstFirst (&pTCDPipe->pendingReqList);

                while (NULL != pNode)
                    {
                    pRequest = USB_PLX_TCD_REQUEST_CONV_FROM_NODE(pNode);

                    /* Only release requests assigned for this ERP */

                    if ((NULL != pRequest) &&
                        (pRequest->pErp == pErp))
                        {
                        (void)usbPlxTcdRequestComplete(pRequest,
                                                 S_usbTgtTcdLib_GENERAL_FAULT);
                        }
                    pNode = lstNext(pNode);
                    }
                return ERROR;
                }

            pRequest->pErp = pErp;
            pRequest->uXferSize = pErp->bfrList[i].bfrLen;
            pRequest->uActLength = 0;
            pRequest->pCurrentBuffer = pErp->bfrList[i].pBfr;
            pRequest->uErpBufIndex = (UINT16)i;
            pRequest->uErpBufCount = pErp->bfrCount;
            pRequest->bDmaActive = FALSE;

            if (pTCDPipe->uEpDir)
                {
                cacheInvalidate(DATA_CACHE,
                    pRequest->pCurrentBuffer,
                    pRequest->uErpBufCount);
                }
            else
                {
                cacheFlush(DATA_CACHE,
                        pRequest->pCurrentBuffer,
                        pRequest->uErpBufCount);
                }

            /* If the function driver forget set pid valude */

            if (pErp->bfrList[i].pid == 0)
                {
                if (pTCDPipe->uEpType != USB_ATTR_CONTROL)
                    {
                    pRequest->uPid = (UINT16)(pTCDPipe->uEpDir ? USB_PID_IN :
                                        USB_PID_OUT);
                    }
                }
            else
                {
                pRequest->uPid = pErp->bfrList[i].pid;
                }

             USB_PLX_VDBG("usbPlxTcdSubmitErp():Reserve Request %p for pErp %p "
                          "uXferSize %x\n",
                          pRequest, pErp, pRequest->uXferSize,
                          4, 5, 6);

            /*
             * Add request into the pipe list
             */

            OS_WAIT_FOR_EVENT(pTCDPipe->pPipeSyncMutex, WAIT_FOREVER);

            /* Add the request into pending list */

            if (ERROR == lstFind(&(pTCDPipe->pendingReqList), &(pRequest->reqNode)))
                {
                pRequest->pTCDPipe = pTCDPipe;
                lstAdd(&(pTCDPipe->pendingReqList), &(pRequest->reqNode));
                }
            OS_RELEASE_EVENT(pTCDPipe->pPipeSyncMutex);
            }
        }

    /* Record the last request */

    pErp->tcdPtr = (pVOID)pRequest;

    USB_PLX_VDBG("usbPlxTcdSubmitErp():Schedule for the submitted ERP\n",
                 1, 2, 3, 4, 5, 6);

    /* Start the request schedule */

    (void)usbPlxTcdSchedule(pTCDData);

    return OK;
    }

/*******************************************************************************
*
* usbPlxTcdCancelErp - cancel the request which already submitted to pipe
*
* This routine is to cancel the request which already submitted to pipe.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbPlxTcdCancelErp
    (
    pUSBTGT_TCD          pTcd,       /* TCD data pointer */
    USB_TARG_PIPE        pipeHandle, /* Pointer to pipe handle */
    pUSB_ERP             pErp        /* ERP pointer */
    )
    {
    pUSB_PLX_TCD_DATA    pTCDData = NULL;
    pUSB_PLX_TCD_PIPE    pTCDPipe = NULL;
    pUSB_PLX_TCD_REQUEST pRequest = NULL;
    NODE *               pNode    = NULL;

    /* Validate parameters */

    if ((NULL == pTcd) ||
        (NULL == (pTCDData = (pUSB_PLX_TCD_DATA)pTcd->pTcdSpecific)) ||
        (NULL == (pTCDPipe = (pUSB_PLX_TCD_PIPE)pipeHandle)) ||
        (NULL == pErp))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTcd) ? "pTcd" :
                     (NULL == pTCDData) ? "pTcdSpecific" :
                     (NULL == pTCDPipe) ? "pTCDPipe" :
                     "pErp"),
                    2, 3, 4, 5, 6);

        return ERROR;
        }

    OS_WAIT_FOR_EVENT(pTCDPipe->pPipeSyncMutex, WAIT_FOREVER);

    if (pErp->tcdPtr == NULL)
        {
        USB_PLX_ERR("Erp doesn't have a corresponding request\n",
                     1, 2, 3, 4, 5, 6);

        OS_RELEASE_EVENT(pTCDPipe->pPipeSyncMutex);

        return ERROR;
        }

    /* Find the request */

    pNode = lstFirst(&pTCDPipe->pendingReqList);

    while (pNode != NULL)
        {
        pRequest = USB_PLX_TCD_REQUEST_CONV_FROM_NODE(pNode);
        if ((pRequest != NULL) &&
            (pRequest->pErp == pErp))
            {
            /*
             * Complete the request with cancel status.
             * NOTE: the Erp's callback routine may re-submit this erp
             * and will re-take the "pPipeSyncMutex".
             */

            usbPlxTcdRequestComplete(pRequest, S_usbTgtTcdLib_ERP_CANCELED);
            }
        pNode = lstNext(pNode);
        }

    OS_RELEASE_EVENT(pTCDPipe->pPipeSyncMutex);

    USB_PLX_DBG("usbPlxTcdCancelErp():cancel all requests submitted for ERP %p\n",
                 pErp, 2, 3, 4, 5, 6);

    return OK;
    }

/*******************************************************************************
*
* usbPlxTcdPipeStatusSet - set pipe status
*
* This routine is to set the pipe's status.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbPlxTcdPipeStatusSet
    (
    pUSBTGT_TCD          pTcd,       /* TCD data pointer */
    USB_TARG_PIPE        pipeHandle, /* Pointer to pipe handle */
    UINT16               uStatus     /* Status to set */
    )
    {
    pUSB_PLX_TCD_DATA    pTCDData = NULL;
    pUSB_PLX_CTRL        pPlxCtrl = NULL;
    pUSB_PLX_TCD_PIPE    pTCDPipe = NULL;
    UINT32               uEpResp  = 0;

    /* Validate parameters */

    if ((NULL == pTcd) ||
        (NULL == (pTCDData = (pUSB_PLX_TCD_DATA)pTcd->pTcdSpecific)) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)) ||
        (NULL == (pTCDPipe = (pUSB_PLX_TCD_PIPE)pipeHandle)))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTcd) ? "pTcd" :
                     (NULL == pTCDData) ? "pTcdSpecific" :
                     (NULL == pPlxCtrl) ? "pPlxCtrl" :
                     "pTCDPipe"),
                    2, 3, 4, 5, 6);

        return ERROR;
        }

    USB_PLX_VDBG("usbPlxTcdPipeStatusSet(): status %d\n",
                uStatus,2,3,4,5,6);

    /*
     * Check the real physical endpoint
     * #1
     *    if the pipe do not be assigned to a physical endpoint, set the
     *    pipe status to do hardware stall or unstall when scheduled.
     *    This is used for virtual endpoints
     *
     * #2.
     *    if the pipe has been assigned to a physical endpoint, set the
     *    pipe status according to the hardware.
     */

    uEpResp = USB_PLX_REG_READ32 (pPlxCtrl,
                           USB_PLX_EP_RSP_OFFSET(pTCDPipe->uPhyEpAddr));

    if (uStatus == USBTGT_PIPE_STATUS_STALL)
        {
        if (USB_PLX_EP_RSP_STALL != (uEpResp & USB_PLX_EP_RSP_STALL))
            {
            /* Set to stall the endpoint */

            USB_PLX_REG_WRITE32 (pPlxCtrl,
                USB_PLX_EP_RSP_OFFSET(pTCDPipe->uPhyEpAddr),
                USB_PLX_EP_RSP_SET(USB_PLX_EP_RSP_STALL));

            }

        pTCDPipe->status &= ~USBTGT_PIPE_STATUS_UNSTALL;
        pTCDPipe->status |= USBTGT_PIPE_STATUS_STALL;
        }
    else if (uStatus == USBTGT_PIPE_STATUS_UNSTALL)
        {
        if (USB_PLX_EP_RSP_STALL == (uEpResp & USB_PLX_EP_RSP_STALL))
            {
            /* Clear the endpoint halt */

            USB_PLX_REG_WRITE32 (pPlxCtrl,
                USB_PLX_EP_RSP_OFFSET(pTCDPipe->uPhyEpAddr),
                USB_PLX_EP_RSP_CLEAR(USB_PLX_EP_RSP_STALL));
            }

        pTCDPipe->status &= ~USBTGT_PIPE_STATUS_STALL;
        pTCDPipe->status |= USBTGT_PIPE_STATUS_UNSTALL;
        }
    else
        {
        pTCDPipe->status |= uStatus;
        }

    USB_PLX_VDBG("usbPlxTcdPipeStatusSet(): Set status %d for pipe %p\n",
                 uStatus, pipeHandle, 3, 4, 5, 6);

    return OK;
    }

/*******************************************************************************
*
* usbPlxTcdPipeStatusGet - get pipe status
*
* This routine is to get the pipe's status.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbPlxTcdPipeStatusGet
    (
    pUSBTGT_TCD          pTcd,       /* TCD data pointer */
    USB_TARG_PIPE        pipeHandle, /* Pointer to pipe handle */
    UINT16 *             pStatus     /* Buffer to hold the pipe status */
    )
    {
    pUSB_PLX_TCD_DATA    pTCDData = NULL;
    pUSB_PLX_CTRL        pPlxCtrl = NULL;
    pUSB_PLX_TCD_PIPE    pTCDPipe = NULL;
    UINT32               uEpResp  = 0;

    /* Validate parameters */

    if ((NULL == pTcd) ||
        (NULL == (pTCDData = (pUSB_PLX_TCD_DATA)pTcd->pTcdSpecific)) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)) ||
        (NULL == (pTCDPipe = (pUSB_PLX_TCD_PIPE)pipeHandle)))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTcd) ? "pTcd" :
                     (NULL == pTCDData) ? "pTcdSpecific" :
                     (NULL == pPlxCtrl) ? "pPlxCtrl" :
                     "pTCDPipe"),
                    2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * Check the real physical endpoint
     * #1
     *    if the pipe do not be assigned to a physical endpoint, set the
     *    pipe status to do hardware stall or unstall when scheduled.
     *    This is used for virtual endpoints
     *
     * #2.
     *    if the pipe has been assigned to a physical endpoint, set the
     *    pipe status according to the hardware.
     */

    uEpResp = USB_PLX_REG_READ32 (pPlxCtrl,
                USB_PLX_EP_RSP_OFFSET(pTCDPipe->uPhyEpAddr));

    if (USB_PLX_EP_RSP_STALL != (uEpResp & USB_PLX_EP_RSP_STALL))
        {
        *pStatus = USB_ENDPOINT_STS_UNHALT;
        }
    else
        {
        *pStatus = USB_ENDPOINT_STS_HALT;
        }

    USB_PLX_VDBG("usbPlxTcdPipeStatusGet(): Get status %d for pipe %p\n",
                 *pStatus, pipeHandle, 3, 4, 5, 6);

    return OK;
    }

/*******************************************************************************
*
* usbPlxTcdRemoteWakeUp - issue remote wake up of the target controller
*
* This routine is used to issue remote wake up of the target controller.
* NOTE: This routine is used for Device-Remote wakeup, not the function
* wakeup.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbPlxTcdRemoteWakeUp
    (
    pUSBTGT_TCD       pTcd       /* TCD data pointer */
    )
    {
    pUSB_PLX_TCD_DATA pTCDData = NULL;
    pUSB_PLX_CTRL     pPlxCtrl = NULL;
    UINT32            uUsbStat;
    UINT32            uUsbCtl;

    /* Validate parameters */

    if ((NULL == pTcd) ||
        (NULL == (pTCDData = (pUSB_PLX_TCD_DATA)pTcd->pTcdSpecific)) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTcd) ? "pTcd" :
                     (NULL == pTCDData) ? "pTcdSpecific" :
                     "pTCDData->pPlxCtrl"),
                     2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Check if the remote wake up is enabled or not */

    uUsbCtl = USB_PLX_REG_READ32(pPlxCtrl, USB_PLX_USBCTL_REG);

    if (0 == (uUsbCtl & USB_PLX_USBCTL_REG_DRWUE))
        {
        USB_PLX_ERR("usbPlxTcdRemoteWakeUp(): Device remote wakeup disabled\n",
                     1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    if (pTCDData->isDeviceSuspend)
        {
        /* Read the contents of the USBSTAT register */

        uUsbStat = USB_PLX_REG_READ32(pPlxCtrl,
                                      USB_PLX_USBSTAT_REG);

        uUsbStat |= USB_PLX_USBSTAT_REG_GENRES;

        /*
         * Write to initiates a Resume sqquence to the host if Device-Remote
         * Wakeup is enabled.
         * And this bit will cleared by the hardware itself.
         */

        USB_PLX_REG_WRITE32(pPlxCtrl,
                            USB_PLX_USBSTAT_REG,
                            uUsbStat);

        USB_PLX_DBG("usbPlxTcdWakeUp(): Device remote wakeup issued\n",
                     1, 2, 3, 4, 5, 6);
        }
    else
        {
        USB_PLX_WARN("usbPlxTcdWakeUp(): Device already not suspend\n",
                      1, 2, 3, 4, 5, 6);
        }

    return OK;
    }

/*******************************************************************************
*
* usbPlxTcdGetFrameNum - get the frame number of the target controller
*
* This routine gets the frame number of the target controller.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbPlxTcdGetFrameNum
    (
    pUSBTGT_TCD       pTcd,       /* TCD data pointer */
    UINT16 *          pFrameNum   /* Buffer pointer of frame number */
    )
    {
    pUSB_PLX_TCD_DATA pTCDData = NULL;
    pUSB_PLX_CTRL     pPlxCtrl = NULL;

    /* Validate parameters */

    if ((NULL == pTcd) ||
        (NULL == (pTCDData = (pUSB_PLX_TCD_DATA)pTcd->pTcdSpecific)) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)) ||
        (NULL == pFrameNum))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTcd) ? "pTcd" :
                     (NULL == pTCDData) ? "pTcdSpecific" :
                     (NULL == pPlxCtrl) ? "pTCDData->pPlxCtrl" :
                      "pFrameNum"),
                     2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * The frame number(count) register is an index register.
     * Write IDXADDR register with FRAME_IDX ,then read the IDXADDR
     */

    USB_PLX_REG_WRITE32(pPlxCtrl,
                        USB_PLX_IDXADDR_REG,
                        USB_PLX_FRAME_IDX);

    /* The frame number is valid of bit 10:0 */

    * pFrameNum = (UINT16)(USB_PLX_REG_READ32(pPlxCtrl,
                           USB_PLX_IDXDATA_REG) &
                           USB_PLX_FRAME_MASK);

    /* Record the current frame number */

    pPlxCtrl->uFrameNumRecord  = * pFrameNum;

    USB_PLX_DBG("usbPlxTcdGetFrameNum() is 0x%X\n",
                pPlxCtrl->uFrameNumRecord,
                2, 3, 4, 5, 6);

    return OK;
    }

/*******************************************************************************
*
* usbPlxTcdSoftConnect - do soft connect/disconnect of the controller
*
* This routine is to do soft connect/disconnect of the target controller.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbPlxTcdSoftConnect
    (
    pUSBTGT_TCD       pTcd,       /* TCD data pointer */
    BOOL              isConnectUp /* Connect or disconnect */
    )
    {
    pUSB_PLX_TCD_DATA pTCDData = NULL;
    pUSB_PLX_CTRL     pPlxCtrl = NULL;
    UINT32            uUsbCtl;

    /* Validate parameters */

    if ((NULL == pTcd) ||
        (NULL == (pTCDData = (pUSB_PLX_TCD_DATA)pTcd->pTcdSpecific)) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTcd) ? "pTcd" :
                     (NULL == pTCDData) ? "pTcdSpecific" :
                     "pTCDData->pPlxCtrl"),
                    2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Get the USBCTL register */

    uUsbCtl = USB_PLX_REG_READ32 (pPlxCtrl, USB_PLX_USBCTL_REG);

    if (isConnectUp)
        {
        /* Soft connect the pin */

        uUsbCtl |= USB_PLX_USBCTL_REG_USBDE;

        USB_PLX_REG_WRITE32 (pPlxCtrl, USB_PLX_USBCTL_REG, uUsbCtl);

        usbPlxTcdEnableDataEpsAsZero(pPlxCtrl);
        pTCDData->isSoftConnected = TRUE;
        }
    else
        {
        /* Soft disconnect the pin */

        uUsbCtl &= ~USB_PLX_USBCTL_REG_USBDE;

        USB_PLX_REG_WRITE32 (pPlxCtrl, USB_PLX_USBCTL_REG, uUsbCtl);

        pTCDData->isSoftConnected = FALSE;
        }

    USB_PLX_VDBG("usbPlxTcdSoftConnect - %s\n",
                (isConnectUp ? "CONNECT" : "DISCONNECT"),
                2, 3, 4, 5, 6);

    return OK;
    }

/*******************************************************************************
*
* usbPlxTcdFeatureSet - process set feature request command for controller
*
* This routine is used to process set feature request command for controller.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbPlxTcdFeatureSet
    (
    pUSB_PLX_TCD_DATA      pTCDData,
    USBTGT_FEATURE_PARAM * pFeatureParam
    )
    {
    pUSB_PLX_TCD_PIPE pTCDPipe = NULL;
    pUSB_PLX_CTRL     pPlxCtrl;
    UINT32            uReg32;
    UINT8             uIndex;

    /* Validate parameters */

    if ((NULL == pTCDData ) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                     ((NULL == pTCDData) ? "pTcdSpecific" :
                     "pTCDData->pPlxCtrl"),
                    2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Set the endpoint feature */

    switch (pFeatureParam->uRequestType & USB_RT_RECIPIENT_MASK)
        {
        case USB_RT_DEVICE:
            {
            switch(pFeatureParam->uFeature)
                {
                case USB_FSEL_DEV_REMOTE_WAKEUP:
                    {
                    pTCDData->isRemoteWakeup = TRUE;

                    USB_PLX_REG_WRITE32(pPlxCtrl,
                                        USB_PLX_USBCTL_REG,
                                        USB_PLX_REG_READ32(pPlxCtrl,
                                        USB_PLX_USBCTL_REG) |
                                        USB_PLX_USBCTL_REG_DRWUE);
                    }
                    break;
                case USB_FSEL_DEV_U1_ENABLE:
                    {
                    pTCDData->isU1Enabled = TRUE;

                    USB_PLX_REG_WRITE32(pPlxCtrl,
                                        USB_PLX_USBCTL2_REG,
                                        USB_PLX_REG_READ32(pPlxCtrl,
                                        USB_PLX_USBCTL2_REG) |
                                        USB_PLX_USBCTL2_U1EN);
                    }
                    break;
                case USB_FSEL_DEV_U2_ENABLE:
                    {
                    pTCDData->isU2Enabled = TRUE;

                    USB_PLX_REG_WRITE32(pPlxCtrl,
                                        USB_PLX_USBCTL2_REG,
                                        USB_PLX_REG_READ32(pPlxCtrl,
                                        USB_PLX_USBCTL2_REG) |
                                        USB_PLX_USBCTL2_U2EN);
                    }
                    break;
                case USB_FSEL_DEV_LTM_ENABLE:
                    {
                    pTCDData->isLTMEnabled = TRUE;

                    USB_PLX_REG_WRITE32(pPlxCtrl,
                                        USB_PLX_USBCTL2_REG,
                                        USB_PLX_REG_READ32(pPlxCtrl,
                                        USB_PLX_USBCTL2_REG) |
                                        USB_PLX_USBCTL2_LTMEN);
                    }
                    break;
                default:
                    break;
                }
            }
            break;
        case USB_RT_INTERFACE:
            {

            }
            break;
        case USB_RT_ENDPOINT:
            {
            if ((pFeatureParam->uFeature == USB_FSEL_DEV_ENDPOINT_HALT))
                {
                for (uIndex = USB_PLX_ENDPT_A;
                     uIndex <= USB_PLX_ENDPT_F; uIndex ++)
                    {
                    uReg32 = USB_PLX_REG_READ32(pPlxCtrl,
                                    USB_PLX_EP_CFG_OFFSET(uIndex)) &
                                    (USB_PLX_EP_CFG_NUMBER |
                                     USB_PLX_EP_CFG_DIRECTION );

                    if (pFeatureParam->uIndex == (UINT16)uReg32)
                        {
                        USB_PLX_REG_WRITE32(pPlxCtrl,
                           USB_PLX_EP_RSP_OFFSET(uIndex),
                           USB_PLX_EP_RSP_SET(USB_PLX_EP_RSP_STALL));

                        /* Update the pipe status */

                        pTCDPipe = usbPlxTcdPipeFind (pTCDData, uIndex);
                        if(pTCDPipe == NULL)
                            {
                            USB_PLX_ERR("Didn't find pipe\n",
                                        1, 2, 3, 4, 5, 6);

                            return ERROR;
                            }

                        pTCDPipe->status &= ~USBTGT_PIPE_STATUS_UNSTALL;
                        pTCDPipe->status |= USBTGT_PIPE_STATUS_STALL;

                        usbPlxTcdPipeSequNumReset(pPlxCtrl, pTCDPipe);
                        }
                    }
                }
            }
            break;
        }

     return OK;
     }


/*******************************************************************************
*
* usbPlxTcdFeatureClear - process clear feature request command for controller
*
* This routine is used to process clear feature request command for controller.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbPlxTcdFeatureClear
    (
    pUSB_PLX_TCD_DATA      pTCDData,
    USBTGT_FEATURE_PARAM * pFeatureParam
    )
    {
    pUSB_PLX_TCD_PIPE pTCDPipe = NULL;
    pUSB_PLX_CTRL     pPlxCtrl;
    UINT32            uReg32;
    UINT8             uIndex;

    /* Validate parameters */

    if ((NULL == pTCDData ) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTCDData) ? "pTcdSpecific" :
                     "pTCDData->pPlxCtrl"),
                    2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Set the endpoint feature */

    switch (pFeatureParam->uRequestType & USB_RT_RECIPIENT_MASK)
        {
        case USB_RT_DEVICE:
            {
           switch(pFeatureParam->uFeature)
                {
                /* Test mode valid only in high speed */

                case USB_FSEL_DEV_TEST_MODE:
                    if (usbPlxTcdBusSpeedGet(pPlxCtrl) == USB_SPEED_HIGH)
                        {
                        /* The test mode select */

                        USB_PLX_REG_WRITE32(pPlxCtrl,
                                USB_PLX_XCVRDIAG_REG,
                                (pFeatureParam->uIndex >> 8) << 24);
                        }
                    break;
                case USB_FSEL_DEV_REMOTE_WAKEUP:
                    {
                    pTCDData->isRemoteWakeup = FALSE;

                    USB_PLX_REG_WRITE32(pPlxCtrl,
                                        USB_PLX_USBCTL_REG,
                                        USB_PLX_REG_READ32(pPlxCtrl,
                                        USB_PLX_USBCTL_REG) &
                                        ~ USB_PLX_USBCTL_REG_DRWUE);
                    }
                    break;
                case USB_FSEL_DEV_U1_ENABLE:
                    {
                    pTCDData->isU1Enabled = FALSE;

                    USB_PLX_REG_WRITE32(pPlxCtrl,
                                        USB_PLX_USBCTL2_REG,
                                        USB_PLX_REG_READ32(pPlxCtrl,
                                        USB_PLX_USBCTL2_REG) &
                                        ~ USB_PLX_USBCTL2_U1EN);
                    }
                    break;
                case USB_FSEL_DEV_U2_ENABLE:
                    {
                    pTCDData->isU2Enabled = FALSE;

                    USB_PLX_REG_WRITE32(pPlxCtrl,
                                        USB_PLX_USBCTL2_REG,
                                        USB_PLX_REG_READ32(pPlxCtrl,
                                        USB_PLX_USBCTL2_REG) &
                                        ~ USB_PLX_USBCTL2_U2EN);
                    }
                    break;
                case USB_FSEL_DEV_LTM_ENABLE:
                    {
                    pTCDData->isLTMEnabled = FALSE;

                    /* Unstall and clear data toggle */

                    USB_PLX_REG_WRITE32(pPlxCtrl,
                                        USB_PLX_USBCTL2_REG,
                                        USB_PLX_REG_READ32(pPlxCtrl,
                                        USB_PLX_USBCTL2_REG) &
                                        ~ USB_PLX_USBCTL2_LTMEN);
                    }
                    break;
                default:
                    break;
                }
            }
            break;
        case USB_RT_INTERFACE:
            {
            }
            break;
        case USB_RT_ENDPOINT:
            {
            if ((pFeatureParam->uFeature == USB_FSEL_DEV_ENDPOINT_HALT))
                {
                for (uIndex = USB_PLX_ENDPT_A;
                     uIndex <= USB_PLX_ENDPT_F; uIndex ++)
                    {
                    uReg32 = USB_PLX_REG_READ32(pPlxCtrl,
                                    USB_PLX_EP_CFG_OFFSET(uIndex)) &
                                    (USB_PLX_EP_CFG_NUMBER |
                                     USB_PLX_EP_CFG_DIRECTION );

                    if (pFeatureParam->uIndex == (UINT16)uReg32)
                        {
                        /*
                         * According to the spec of USB3380, clear the
                         * STALL bit will clear the data toggle bit
                         * automatically.
                         */
                        USB_PLX_REG_WRITE32(pPlxCtrl,
                           USB_PLX_EP_RSP_OFFSET(uIndex),
                           USB_PLX_EP_RSP_CLEAR(USB_PLX_EP_RSP_STALL));

                        /* Update the pipe status */

                        pTCDPipe = usbPlxTcdPipeFind (pTCDData, uIndex);
                        if(pTCDPipe == NULL)
                            {
                            USB_PLX_ERR("Didn't find pipe\n",1, 2, 3, 4, 5, 6);

                            return ERROR;
                            }

                        pTCDPipe->status &= ~USBTGT_PIPE_STATUS_STALL;
                        pTCDPipe->status |= USBTGT_PIPE_STATUS_UNSTALL;

                        usbPlxTcdPipeSequNumReset(pPlxCtrl, pTCDPipe);
                        }
                    }
                }
            }
            break;
        }

     return OK;
     }

/*******************************************************************************
*
* usbPlxTcdIoctl - Ioctl interface of the TCD
*
* This routine is the Ioctl interface of the TCD.
*
* RETURNS: values based on the cmd
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL int usbPlxTcdIoctl
    (
    pUSBTGT_TCD pTcd,
    int         cmd,
    void *      pContext
    )
    {
    pUSB_PLX_TCD_DATA pTCDData = NULL;
    pUSB_PLX_CTRL     pPlxCtrl = NULL;
    UINT32            uData32;
    UINT16            uData16;
    UINT8             uData8;
    int               reV = 0;

    /* Validate parameters */

    if ((NULL == pTcd) ||
        (NULL == (pTCDData = (pUSB_PLX_TCD_DATA)pTcd->pTcdSpecific)) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTcd) ? "pTcd" :
                     (NULL == pTCDData) ? "pTcdSpecific" :
                     "pTCDData->pPlxCtrl"),
                    2, 3, 4, 5, 6);

        return ERROR;
        }

    switch (cmd)
        {
        case USBTGT_TCD_IOCTL_CMD_SPEED_UPDATE:
            {
            pTcd->uSpeed = usbPlxTcdBusSpeedGet(pPlxCtrl);

            /* If the bus speed is ivalid, give a falk super speed */

            if (pTcd->uSpeed == 0xFF)
                {
                /* Check if the hardware suspend */

                uData32 = USB_PLX_REG_READ32 (pPlxCtrl,
                                              USB_PLX_USBSTAT_REG);

                if (uData32 & USB_PLX_USBSTAT_SUSPEND)
                    {
                    USB_PLX_REG_WRITE32 (pPlxCtrl,
                                         USB_PLX_USBSTAT_REG,
                                         USB_PLX_USBSTAT_SUSPEND);
                    }

                pTcd->uSpeed = USB_SPEED_SUPER;
                }
            }
            break;
        case USBTGT_TCD_IOCTL_CMD_SET_SEL:
            {
            /* Process the Set SEL request */

            uData8 = * (UINT8 *)pContext;        /* Offset 0 */
            uData16 = *((UINT16 *)pContext + 1); /* Offset 2 */
            uData32 = (uData16 << 8) | uData8;

            USB_PLX_REG_WRITE32(pPlxCtrl, USB_PLX_SS_SEL, uData32);

            uData8 = *((UINT8 *)pContext + 1);   /* Offset 1 */
            uData16 = *((UINT16 *)pContext + 2); /* Offset 4 */
            uData32 = (uData16 << 8) | uData8;

            USB_PLX_REG_WRITE32(pPlxCtrl, USB_PLX_SS_DEL, uData32);
            }
            break;
        case USBTGT_TCD_IOCTL_CMD_SET_ISOCH_DELAY:
            {
            /* Process the Set ISO Delay request */

            uData16 = * (UINT16 *)pContext;

            USB_PLX_REG_WRITE32(pPlxCtrl, USB_PLX_SS_ISODELAY, uData16);
            }
            break;
        case USBTGT_TCD_IOCTL_CMD_SET_FEATURE:
            {
            reV = (int)usbPlxTcdFeatureSet(pTCDData,
                                           (USBTGT_FEATURE_PARAM *)pContext);
            }
            break;
        case USBTGT_TCD_IOCTL_CMD_CLEAR_FEATURE:
            {
            reV = (int)usbPlxTcdFeatureClear(pTCDData,
                                             (USBTGT_FEATURE_PARAM *)pContext);
            }
            break;

        /* case Function resume/suspend */

        default:
            break;
        }

    return reV;
    }

