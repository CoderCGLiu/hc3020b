/* usbMhdrcCppiDma.h - CPPI 4.1 DMA procession of MHDRC USB Controller */

/*
 * Copyright (c) 2011, 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01c,05jan13,ljg  Add CPPI DMA support for MHDRC TCD (WIND00398723)
01b,22jun11,jws  ported to new MHDRC stack
01a,13jun11,ita  initial version
*/

/*
DESCRIPTION

This file defines the CENTAURUS special register offsets and macros for the MHDRC
Internal DMA (CPPI 4.1 DMA) Controller.
*/


#ifndef __INCusbMhdrcCppDmah
#define __INCusbMhdrcCppDmah

#ifdef __cplusplus
extern "C" {
#endif

#ifndef    USB_MHDRC_DMA_INT_THREAD_PRIORITY 
#define USB_MHDRC_DMA_INT_THREAD_PRIORITY (49)
#endif

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))


#define USB_MHDRC_CPPIDMA_NUM_QUEUE_MGR            1    /* 4  max */
#define USB_MHDRC_CPPIDMA_NUM_DMA_BLOCK            1    /* 64 max */

#define USB_MHDRC_CPPIDMA_MAX_MEM_RGN              16

#define USB_MHDRC_CPPIDMA_PKT_TYPE_USB             (0x5 << 26)
#define USB_MHDRC_CPPIDMA_RETURN_LINKED            (0x0 << 15)

#define USB_MHDRC_CPPIDMA_NUM_CH                   15
#define USB_MHDRC_CPPIDMA_DESC_SIZE_SHIFT          6
#define USB_MHDRC_CPPIDMA_DESC_ALIGN               (1 << USB_MHDRC_CPPIDMA_DESC_SIZE_SHIFT)
#define USB_MHDRC_CPPIDMA_CH_NUM_PD                64    /* 4K bulk data at full speed */
#define USB_MHDRC_CPPIDMA_MAX_PD                   (USB_MHDRC_CPPIDMA_CH_NUM_PD * USB_MHDRC_CPPIDMA_NUM_CH)

#define USB_MHDRC_CPPIDMA_SRC_TAG_PORT_NUM_SHIFT   27
#define USB_MHDRC_CPPIDMA_SRC_TAG_PORT_NUM_MASK    (0x1f << USB_MHDRC_CPPIDMA_SRC_TAG_PORT_NUM_SHIFT)

#define USB_MHDRC_CPPIDMA_DESC_TYPE_SHIFT          27
#define USB_MHDRC_CPPIDMA_DESC_TYPE_MASK           (0x1f << USB_MHDRC_CPPIDMA_DESC_TYPE_SHIFT)
#define USB_MHDRC_CPPIDMA_DESC_TYPE_HOST           ((UINT32)(0x10 << USB_MHDRC_CPPIDMA_DESC_TYPE_SHIFT))

#define USB_MHDRC_CPPIDMA_RETURN_QMGR_SHIFT        12
#define USB_MHDRC_CPPIDMA_RETURN_QMGR_MASK         (3 << USB_MHDRC_CPPIDMA_RETURN_QMGR_SHIFT)
#define USB_MHDRC_CPPIDMA_RETURN_QNUM_SHIFT        0
#define USB_MHDRC_CPPIDMA_RETURN_QNUM_MASK         (0xfff << USB_MHDRC_CPPIDMA_RETURN_QNUM_SHIFT)

#define USB_MHDRC_CPPIDMA_PKT_INTR_FLAG            ((UINT32)(1 << 31))

/* USBSS registers               BaseAddress:0x0000_0000 size:0x1000 */

#define USB_MHDRC_USBSS_REVISION                    0x0000           /* REVREG          */
#define USB_MHDRC_USBSS_SYSCONFIG                   0x0010           /* SYSCONFIG       */
#define USB_MHDRC_USBSS_EOI                         0x0020           /* EOI             */
#define USB_MHDRC_USBSS_IRQ_STATUS_RAW              0x0024           /* IRQSTATRAW      */
#define USB_MHDRC_USBSS_IRQ_STATUS                  0x0028           /* IRQSTAT         */
#define USB_MHDRC_USBSS_IRQ_ENABLE_SET              0x002C           /* IRQENABLER      */
#define USB_MHDRC_USBSS_IRQ_ENABLE_CLR              0x0030           /* IRQCLEARR       */
#define USB_MHDRC_USBSS_IRQ_DMA_THRESHOLD_TX0       0x0100           /* IRQDMATHOLDTX00 */
#define USB_MHDRC_USBSS_IRQ_DMA_THRESHOLD_TX1       0x0104           /* IRQDMATHOLDTX01 */
#define USB_MHDRC_USBSS_IRQ_DMA_THRESHOLD_TX2       0x0108           /* IRQDMATHOLDTX02 */
#define USB_MHDRC_USBSS_IRQ_DMA_THRESHOLD_TX3       0x010C           /* IRQDMATHOLDTX03 */

#define USB_MHDRC_USBSS_IRQ_DMA_THRESHOLD_RX0       0x0110           /* IRQDMATHOLDRX00 */
#define USB_MHDRC_USBSS_IRQ_DMA_THRESHOLD_RX1       0x0114           /* IRQDMATHOLDRX01 */
#define USB_MHDRC_USBSS_IRQ_DMA_THRESHOLD_RX2       0x0118           /* IRQDMATHOLDRX02 */
#define USB_MHDRC_USBSS_IRQ_DMA_THRESHOLD_RX3       0x011C           /* IRQDMATHOLDRX03 */

#define USB_MHDRC_USBSS_IRQ_DMA_THRESHOLD_TX11      0x0120           /* IRQDMATHOLDTX10 */
#define USB_MHDRC_USBSS_IRQ_DMA_THRESHOLD_TX12      0x0124           /* IRQDMATHOLDTX11 */
#define USB_MHDRC_USBSS_IRQ_DMA_THRESHOLD_TX13      0x0128           /* IRQDMATHOLDTX12 */
#define USB_MHDRC_USBSS_IRQ_DMA_THRESHOLD_TX14      0x012C           /* IRQDMATHOLDTX13 */

#define USB_MHDRC_USBSS_IRQ_DMA_THRESHOLD_RX11      0x0130           /* IRQDMATHOLDRX10 */
#define USB_MHDRC_USBSS_IRQ_DMA_THRESHOLD_RX12      0x0134           /* IRQDMATHOLDRX11 */
#define USB_MHDRC_USBSS_IRQ_DMA_THRESHOLD_RX13      0x0138           /* IRQDMATHOLDRX12 */
#define USB_MHDRC_USBSS_IRQ_DMA_THRESHOLD_RX14      0x013C           /* IRQDMATHOLDRX13 */
#define USB_MHDRC_USBSS_IRQ_DMA_ENABLE0             0x0140           /* IRQDMAENABLE0   */
#define USB_MHDRC_USBSS_IRQ_DMA_ENABLE1             0x0144           /* IRQDMAENABLE1   */
#define USB_MHDRC_USBSS_IRQ_FRAME_THRESHOLD_TX00    0x0200           /* IRQFRAMETHOLDTX00 */
#define USB_MHDRC_USBSS_IRQ_FRAME_THRESHOLD_TX01    0x0204           /* IRQFRAMETHOLDTX01 */
#define USB_MHDRC_USBSS_IRQ_FRAME_THRESHOLD_TX02    0x0208           /* IRQFRAMETHOLDTX02 */
#define USB_MHDRC_USBSS_IRQ_FRAME_THRESHOLD_TX03    0x020C           /* IRQFRAMETHOLDTX03 */
#define USB_MHDRC_USBSS_IRQ_FRAME_THRESHOLD_RX00    0x0210           /* IRQFRAMETHOLDRX00 */
#define USB_MHDRC_USBSS_IRQ_FRAME_THRESHOLD_RX01    0x0214           /* IRQFRAMETHOLDRX01 */
#define USB_MHDRC_USBSS_IRQ_FRAME_THRESHOLD_RX02    0x0218           /* IRQFRAMETHOLDRX02 */
#define USB_MHDRC_USBSS_IRQ_FRAME_THRESHOLD_RX03    0x021C           /* IRQFRAMETHOLDRX03 */
#define USB_MHDRC_USBSS_IRQ_FRAME_THRESHOLD_TX10    0x0220           /* IRQFRAMETHOLDTX10 */
#define USB_MHDRC_USBSS_IRQ_FRAME_THRESHOLD_TX11    0x0224           /* IRQFRAMETHOLDTX11 */
#define USB_MHDRC_USBSS_IRQ_FRAME_THRESHOLD_TX12    0x0228           /* IRQFRAMETHOLDTX12 */
#define USB_MHDRC_USBSS_IRQ_FRAME_THRESHOLD_TX13    0x022C           /* IRQFRAMETHOLDTX13 */
#define USB_MHDRC_USBSS_IRQ_FRAME_THRESHOLD_RX10    0x0230           /* IRQFRAMETHOLDRX10 */
#define USB_MHDRC_USBSS_IRQ_FRAME_THRESHOLD_RX11    0x0234           /* IRQFRAMETHOLDRX11 */
#define USB_MHDRC_USBSS_IRQ_FRAME_THRESHOLD_RX12    0x0238           /* IRQFRAMETHOLDRX12 */
#define USB_MHDRC_USBSS_IRQ_FRAME_THRESHOLD_RX13    0x023C           /* IRQFRAMETHOLDRX13 */

#define USB_MHDRC_USBSS_IRQ_FRAME_ENABLE_0          0x0240           /* IRQFRAMEENABLE0 */
#define USB_MHDRC_USBSS_IRQ_FRAME_ENABLE_1          0x0244           /* IRQFRAMEENABLE1 */


/* CPPI DMA controller registers BaseAddress:0x0000_2000 size:0x1000 */

#define DMA_REVISION                                0x0000           /* DMAREVID    */
#define DMA_TEADOWN_FREE_DESCRIPTOR_QUEUE_CTRL      0x0004           /* TDFDQ       */

/* IRQSTAT register bits */

#define USB_MHDRC_USBSS_IRQSTAT_TX_PKT_CMP_0       (0x00000100)
#define USB_MHDRC_USBSS_IRQSTAT_RX_PKT_CMP_0       (0x00000200)
#define USB_MHDRC_USBSS_IRQSTAT_TX_PKT_CMP_1       (0x00000400)
#define USB_MHDRC_USBSS_IRQSTAT_RX_PKT_CMP_1       (0x00000800)

/* Teardown Free Descriptor Queue Control Register bits */

#define DMA_TD_DESC_QMGR_SHIFT                12
#define DMA_TD_DESC_QMGR_MASK                 (3 << DMA_TD_DESC_QMGR_SHIFT)
#define DMA_TD_DESC_QNUM_SHIFT                0
#define DMA_TD_DESC_QNUM_MASK                 (0xfff << DMA_TD_DESC_QNUM_SHIFT)

#define DMA_EMULATION_CTRL                   0x0008         /* DMAEMU      */
#define DMA_MEM1_BASE_ADDRESS                0x0010         /* DMAMEM1BA   */
#define DMA_MEM1_MASK_ADDRESS                0x0014         /* DMAMEM1MASK */

/* TXGCRn   */ /* Channel_0 -- Channel_14 */

#define DMA_TX_CHANNEL_GLOBAL_CONFIG(x)        (0x0800 + (0x0020 * (x))) 

/* Tx Channel N Global Configuration Register bits */

#define DMA_CH_TX_ENABLE_SHIFT               31
#define DMA_CH_TX_ENABLE_MASK                (1 << DMA_CH_TX_ENABLE_SHIFT)
#define DMA_CH_TX_TEARDOWN_SHIFT             30
#define DMA_CH_TX_TEARDOWN_MASK              (1 << DMA_CH_TX_TEARDOWN_SHIFT)
#define DMA_CH_TX_DEFAULT_QMGR_SHIFT         12
#define DMA_CH_TX_DEFAULT_QMGR_MASK          (3 << DMA_CH_TX_DEFAULT_QMGR_SHIFT)
#define DMA_CH_TX_DEFAULT_QNUM_SHIFT         0
#define DMA_CH_TX_DEFAULT_QNUM_MASK          (0xfff << DMA_CH_TX_DEFAULT_QNUM_SHIFT)

/* RXGCRn   */ /* Channel_0 -- Channel_14 */

#define DMA_RX_CHANNEL_GLOBAL_CONFIG(x)      (0x0808 + (0x0020 * (x)))  

/* Rx Channel N Global Configuration Register bits */

#define DMA_CH_RX_ENABLE_SHIFT               31
#define DMA_CH_RX_ENABLE_MASK                (1 << DMA_CH_RX_ENABLE_SHIFT)
#define DMA_CH_RX_TEARDOWN_SHIFT             30
#define DMA_CH_RX_TEARDOWN_MASK              (1 << DMA_CH_RX_TEARDOWN_SHIFT)
#define DMA_CH_RX_ERROR_HANDLING_SHIFT       24
#define DMA_CH_RX_ERROR_HANDLING_MASK        (1 << DMA_CH_RX_ERROR_HANDLING_SHIFT)
#define DMA_CH_RX_ERROR_HANDLING_BIT_ON      (1 << DMA_CH_RX_ERROR_HANDLING_SHIFT)
#define DMA_CH_RX_SOP_OFFSET_SHIFT           16
#define DMA_CH_RX_SOP_OFFSET_MASK            (0xff << DMA_CH_RX_SOP_OFFSET_SHIFT)
#define DMA_CH_RX_DEFAULT_DESC_TYPE_SHIFT    14
#define DMA_CH_RX_DEFAULT_DESC_TYPE_HOST     0x00004000
#define DMA_CH_RX_DEFAULT_DESC_EMBED         0
#define DMA_CH_RX_DEFAULT_DESC_HOST          1
#define DMA_CH_RX_DEFAULT_DESC_MONO          2
#define DMA_CH_RX_DEFAULT_RQ_QMGR_SHIFT      12
#define DMA_CH_RX_DEFAULT_RQ_QMGR_MASK       (3 << DMA_CH_RX_DEFAULT_RQ_QMGR_SHIFT)
#define DMA_CH_RX_DEFAULT_RQ_QNUM_SHIFT      0
#define DMA_CH_RX_DEFAULT_RQ_QNUM_MASK       (0xfff << DMA_CH_RX_DEFAULT_RQ_QNUM_SHIFT)

/* RXHPCRAn */ /* Channel_0 -- Channel_14 */

#define DMA_RX_CHANNEL_HOST_PACKET_CONFIG_A(x)        (0x080C + (0x0020 * (x))) 

/* RXHPCRBn */ /* Channel_0 -- Channel_14 */

#define DMA_RX_CHANNEL_HOST_PACKET_CONFIG_B(x)        (0x0810 + (0x0020 * (x))) 

/* CPPI DMA scheduler registers  BaseAddress:0x0000_3000 size:0x1000 */

#define DMA_SCHEDULER_CTRL                    0x0000             /* DMA_SCHED_CTRL */

  /* DMA Scheduler Control Register bits */

#define DMA_SCHED_ENABLE_SHIFT            31
#define DMA_SCHED_ENABLE_MASK            (1 << DMA_SCHED_ENABLE_SHIFT)
#define DMA_SCHED_LAST_ENTRY_SHIFT        0
#define DMA_SCHED_LAST_ENTRY_MASK        (0xff << DMA_SCHED_LAST_ENTRY_SHIFT)

/* WORDn  */  /* WORD0 -- WORD63 */

#define DMA_SCHEDULER_TABLE_WORD(x)        (0x0800 + (0x004 * (x))) 


/* Queue Manager registers       BaseAddress:0x0000_4000 size:0x4000 */

#define QMGR_REVISION                        0x0000             /* QMGRREVID */
#define QMGR_RESET                           0x0004             /* QMGRRST   */
#define QMGR_QUEUE_DIVERSION                 0x0008             /* DIVERSION */

#define QMGR_FDBSC0                          0x0020             /* FDBSC0    */
#define QMGR_FDBSC1                          0x0024             /* FDBSC1    */
#define QMGR_FDBSC2                          0x0028             /* FDBSC2    */
#define QMGR_FDBSC3                          0x002C             /* FDBSC3    */
#define QMGR_FDBSC4                          0x0030             /* FDBSC4    */
#define QMGR_FDBSC5                          0x0034             /* FDBSC5    */
#define QMGR_FDBSC6                          0x0038             /* FDBSC6    */
#define QMGR_FDBSC7                          0x003C             /* FDBSC7    */

#define QMGR_LINK_RAM0_BASE_ADDR             0x0080             /* LRAM0BASE */
#define QMGR_LINK_RAM0_SIZE                  0x0084             /* LRAM0SIZE */
#define QMGR_LINK_RAM1_BASE_ADDR             0x0088             /* LRAM1BASE */
#define QMGR_QUEUE_PEND_REG0                 0x0090             /* PEND0     */
#define QMGR_QUEUE_PEND_REG1                 0x0094             /* PEND1     */
#define QMGR_QUEUE_PEND_REG2                 0x0098             /* PEND2     */
#define QMGR_QUEUE_PEND_REG3                 0x009C             /* PEND3     */
#define QMGR_QUEUE_PEND_REG4                 0x00A0             /* PEND3     */

#define USB_MHDRC_QUERE_93_95               (0xE0000000)
#define USB_MHDRC_QUEUE_96_107              (0x000007FF)
#define USB_MHDRC_QUEUE_109_123             (0x07FFE000)

/* QMEMRBASEr */  /* x=0 -- x=15 */

#define QMGR_MEM_REGION_BASE_ADDR(x)        (0x1000 + (16*(x))) 

/* QMEMRCTRLr */  /* x=0 -- x=15 */

#define QMGR_MEM_REGION_CTRL(x)             (0x1004 + (16*(x)))  

  /* Memory Region R Control Register bits */

#define QMGR_MEM_RGN_INDEX_SHIFT            16
#define QMGR_MEM_RGN_INDEX_MASK             (0x3fff << QMGR_MEM_RGN_INDEX_SHIFT)
#define QMGR_MEM_RGN_DESC_SIZE_SHIFT        8
#define QMGR_MEM_RGN_DESC_SIZE_MASK         (0xf << QMGR_MEM_RGN_DESC_SIZE_SHIFT)
#define QMGR_MEM_RGN_SIZE_SHIFT             0
#define QMGR_MEM_RGN_SIZE_MASK              (7 << QMGR_MEM_RGN_SIZE_SHIFT)

/* CTRLDn  */  /* x=0 -- x=155 */

#define QMGR_QUEUE_CTRL_D(x)                (0x200C + (16*(x)))  

  /* Queue N Register D bits */

#define QMGR_QUEUE_DESC_PTR_SHIFT           5
#define QMGR_QUEUE_DESC_PTR_MASK            ((UINT32)(0x7ffffff << QMGR_QUEUE_DESC_PTR_SHIFT))
#define QMGR_QUEUE_DESC_SIZE_SHIFT          0
#define QMGR_QUEUE_DESC_SIZE_MASK           (0x1f << QMGR_QUEUE_DESC_SIZE_SHIFT)

#define QMGR_QUEUE_STATUS_A(x)              (0x3000 + (16*(x)))  /* QSTATAn */  /* x=0 -- x=155 */
#define QMGR_QUEUE_STATUS_B(x)              (0x3004 + (16*(x)))  /* QSTATBn */  /* x=0 -- x=155 */
#define QMGR_QUEUE_STATUS_C(x)              (0x3008 + (16*(x)))  /* QSTATCn */  /* x=0 -- x=155 */

/* TXCSR in Peripheral and Host mode */

#define USB_MHDRC_TXCSR_AUTOSET             0x8000
#define USB_MHDRC_TXCSR_DMAENAB             0x1000
#define USB_MHDRC_TXCSR_FRCDATATOG          0x0800
#define USB_MHDRC_TXCSR_DMAMODE             0x0400
#define USB_MHDRC_TXCSR_CLRDATATOG          0x0040
#define USB_MHDRC_TXCSR_FLUSHFIFO           0x0008
#define USB_MHDRC_TXCSR_FIFONOTEMPTY        0x0002
#define USB_MHDRC_TXCSR_TXPKTRDY            0x0001

/* TXCSR in Peripheral mode */

#define USB_MHDRC_TXCSR_P_ISO               0x4000
#define USB_MHDRC_TXCSR_P_INCOMPTX          0x0080
#define USB_MHDRC_TXCSR_P_SENTSTALL         0x0020
#define USB_MHDRC_TXCSR_P_SENDSTALL         0x0010
#define USB_MHDRC_TXCSR_P_UNDERRUN          0x0004

/* TXCSR in Host mode */

#define USB_MHDRC_TXCSR_H_WR_DATATOGGLE     0x0200
#define USB_MHDRC_TXCSR_H_DATATOGGLE        0x0100
#define USB_MHDRC_TXCSR_H_NAKTIMEOUT        0x0080
#define USB_MHDRC_TXCSR_H_RXSTALL           0x0020
#define USB_MHDRC_TXCSR_H_ERROR             0x0004

/* TXCSR bits to avoid zeroing (write zero clears, write 1 ignored) */

#define USB_MHDRC_TXCSR_P_WZC_BITS    \
    (USB_MHDRC_TXCSR_P_INCOMPTX | USB_MHDRC_TXCSR_P_SENTSTALL \
    | USB_MHDRC_TXCSR_P_UNDERRUN | USB_MHDRC_TXCSR_FIFONOTEMPTY)
#define USB_MHDRC_TXCSR_H_WZC_BITS    \
    (USB_MHDRC_TXCSR_H_NAKTIMEOUT | USB_MHDRC_TXCSR_H_RXSTALL \
    | USB_MHDRC_TXCSR_H_ERROR | USB_MHDRC_TXCSR_FIFONOTEMPTY)

/* DMA Base Regster Offset */

#define    USB_MHDRC_CPPIDMA_REG_OFFSET     0x2000
#define    USB_MHDRC_DMASCHED_REG_OFFSET    0x3000
#define    USB_MHDRC_QMGR_REG_OFFSET        0x4000

/* Queue-endpoint assignments : Tx Completion Queue EP1..15 93..107 */

#define USB_MHDRC_CPPI_TXQ_START_NUM    93  /* usb0 93  usb1 125*/

/* Queue-endpoint assignments : Rx Completion Queue EP1..15  109..123*/

#define USB_MHDRC_CPPI_RXQ_START_NUM    109 /* usb0 109  usb1 141 */

/* Queue-endpoint assignments : Rx Completion Queue EP1..15  109..123*/

#define USB_MHDRC_CPPI_START_NUM_RANGE  32

/* Max RX transfer size : 128 * 1024 */

#define USB_MHDRC_CPPI_MAX_RX_SIZE      (0x200000)

/* Max TX transfer size: 4*1024*1024 -1 */

#define USB_MHDRC_CPPI_MAX_TX_SIZE      (0x3FFFFF)

#define USB_MHDRC_LINK_RAM_SIZE         (0x3FFF)

/* USBSS */

#define    USB_MHDRC_USBx_CHECK_ADR_MASK      (0x00000fff)

#define USB_MHDRC_USBSS_REG_WRITE32(pMHDRC, OFFSET, VALUE)                      \
    vxbWrite32 (((pUSB_MUSBMHDRC)(pMHDRC))->pRegAccessHandle,                   \
        (void *)(((pUSB_MUSBMHDRC)(pMHDRC))->uUsbssRegBase + OFFSET),           \
        (UINT32)(VALUE))

#define USB_MHDRC_USBSS_REG_READ32(pMHDRC, OFFSET)                              \
    vxbRead32 (((pUSB_MUSBMHDRC)(pMHDRC))->pRegAccessHandle,                    \
        (void *)(((pUSB_MUSBMHDRC)(pMHDRC))->uUsbssRegBase+ OFFSET))

/* CPPIDMA */

#define USB_MHDRC_CPPIDMA_REG_WRITE32(pMHDRC, OFFSET, VALUE)                    \
    vxbWrite32 (((pUSB_MUSBMHDRC)(pMHDRC))->pRegAccessHandle,                   \
        (void *)(((pUSB_MUSBMHDRC)(pMHDRC))->uUsbssRegBase +                    \
                   USB_MHDRC_CPPIDMA_REG_OFFSET + OFFSET),(UINT32)(VALUE))

#define USB_MHDRC_CPPIDMA_REG_READ32(pMHDRC, OFFSET)                            \
    vxbRead32 (((pUSB_MUSBMHDRC)(pMHDRC))->pRegAccessHandle,                    \
        (void *)(((pUSB_MUSBMHDRC)(pMHDRC))->uUsbssRegBase +                    \
                   USB_MHDRC_CPPIDMA_REG_OFFSET + OFFSET))

/* DMA SCHEDULER */

#define USB_MHDRC_DMASCHED_REG_WRITE32(pMHDRC, OFFSET, VALUE)                   \
    vxbWrite32 (((pUSB_MUSBMHDRC)(pMHDRC))->pRegAccessHandle,                   \
        (void *)(((pUSB_MUSBMHDRC)(pMHDRC))->uUsbssRegBase +                    \
                   USB_MHDRC_DMASCHED_REG_OFFSET + OFFSET), (UINT32)(VALUE))
 
#define USB_MHDRC_DMASCHED_REG_READ32(pMHDRC, OFFSET)                           \
    vxbRead32 (((pUSB_MUSBMHDRC)(pMHDRC))->pRegAccessHandle,                    \
        (void *)(((pUSB_MUSBMHDRC)(pMHDRC))->uUsbssRegBase +                    \
                   USB_MHDRC_DMASCHED_REG_OFFSET + OFFSET))                        

/* QUEUE MANAGER */

#define USB_MHDRC_QMGR_REG_WRITE32(pMHDRC, OFFSET, VALUE)                       \
    vxbWrite32 (((pUSB_MUSBMHDRC)(pMHDRC))->pRegAccessHandle,                   \
        (void *)(((pUSB_MUSBMHDRC)(pMHDRC))->uUsbssRegBase +                    \
                   USB_MHDRC_QMGR_REG_OFFSET + OFFSET), (UINT32)(VALUE))

#define USB_MHDRC_QMGR_REG_READ32(pMHDRC, OFFSET)                               \
    vxbRead32 (((pUSB_MUSBMHDRC)(pMHDRC))->pRegAccessHandle,                    \
        (void *)(((pUSB_MUSBMHDRC)(pMHDRC))->uUsbssRegBase +                    \
                   USB_MHDRC_QMGR_REG_OFFSET + OFFSET))

/*
 * USB MHDRC CPPI 4.1 Teardown Descriptor
 */
 
typedef struct usb_mhdrc_cppidma_teardown_desc
    {
    UINT32 teardownInfo;                /* Teardown Information */
    UINT32 reservedPad[7];              /* reserved padding     */
    }USB_MHDRC_CPPIDMA_TEARDOWN_DESC, *pUSB_MHDRC_CPPIDMA_TEARDOWN_DESC;

typedef struct usb_mhdrc_dma_teardown
    {
    size_t   rgnSize;                   /* Region size     */
    void     *addr;                     /* Region address  */
    UINT8    memRgn;                    /* Region num      */
    UINT16   queMgr;                    /* Que manager num */
    UINT16   queNum;                    /* Que num         */
    }USB_MHDRC_DMA_TEARDOWN, *pUSB_MHDRC_DMA_TEARDOWN;

/*
 * USB Packet Descriptor
 */
 
typedef struct usb_mhdrc_cppidma_pkt_desc 
    {
    /* Hardware desc */
    
    UINT32    pktInfoWd0;    /* Packet Information Word 0 */
    UINT32    pktInfoWd1;    /* Packet Information Word 1 (Tag)*/
    UINT32    ptkInfoWd2;    /* Packet and Buffer Information Word 2 */
    UINT32    bufLen;        /* Buffer Infomation Word 0(Buffer Length) */
    UINT32    bufPtr;        /* Buffer Infomation Word 1(Buffer Pointer) */
    UINT32    nextDescPrt;   /* Linking Infomation(Next Descriptor Pointer) */
    UINT32    orgBufLen;     /* Original Buffer Infomation Word 0( Original Buffer Length) */
    UINT32    orgBufPtr;     /* Original Buffer Infomation Word 1( Original Buffer Pointer) */
    
    /* Hardware desc end */
    
    struct usb_mhdrc_cppidma_pkt_desc *  pNextPD;   /* Next PD Pointer     */
    UINT32                               dmaAddr;   /* DMA Set Address     */
    UINT8                                chNum;     /* DMA Channel Num     */
    UINT8                                epNum;     /* EndPoint Num        */
    UINT8                                eop;       /* end of point        */
    }USB_MHDRC_CPPIDMA_PKT_DESC, *pUSB_MHDRC_CPPIDMA_PKT_DESC;


/*
 * struct usb_mhdrc_cppidma - CPPI 4.1 DMA Control
 */
 
typedef struct usb_mhdrc_cppidma
    {
    USB_MHDRC_DMA_TEARDOWN      tearDownRegion;
    void *                      pLinkRamRegion;
    pUSB_MHDRC_CPPIDMA_PKT_DESC pPdPoolTop;    /* Free PD pool Top */
    void *                      pPdMem;        /* PD memory pointer */
    UINT8                       pdMemRegion;   /* PD memory region num */
    UINT16                      teardownQNum;  /* Teardown completion queue num */
    }USB_MHDRC_CPPIDMA, *pUSB_MHDRC_CPPIDMA;

IMPORT void usbMhdrcCppiDmaIntrDisable
    (
    pUSB_MUSBMHDRC pMHDRC
    );
IMPORT void usbMhdrcCppiDmaIntrEnable
    (
    pUSB_MUSBMHDRC pMHDRC
    );
/* declare */

STATUS usbMhdrcPlatformCppiDmaSetup
    (
    pUSB_MUSBMHDRC  pMHDRC
    );

STATUS usbMhdrcPlatformCppiDmaUnSetup
    (
    pUSB_MUSBMHDRC  pMHDRC
    );
pUSB_MHDRC_CPPIDMA_PKT_DESC usbMhdrcCppiDmaPDGet
    (
    pUSB_MHDRC_CPPIDMA pCppi
    );
void usbMhdrcCppiDmaPDPut
    (
    pUSB_MHDRC_CPPIDMA           pCppi, 
    pUSB_MHDRC_CPPIDMA_PKT_DESC  pFreePD
    );
#ifdef __cplusplus
}
#endif

#endif

