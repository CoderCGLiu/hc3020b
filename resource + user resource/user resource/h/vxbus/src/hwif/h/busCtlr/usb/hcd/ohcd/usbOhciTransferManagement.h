/* usbOhciTransferManagement.h - OHCI host controller transgfer management */

/*
 * Copyright (c) 2003-2008, 2010, 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2003-2010, 2013 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
modification history
--------------------
01q,03nov13,wyy  remove __attribute__((packed)) for all sharing memory 
                 (WIND00439756)
01p,02jul10,m_y  Replace PipeDeletedFlag by uCurPipeState and uNextPipeState in
                 structrure USB_OHCD_PIPE (WIND00221377)
01o,18may10,m_y  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
01n,13jan10,ghs  vxWorks 6.9 LP64 adapting
01m,13aug08,w_x  Corrected some macros which write intermediate value to memory
                 shared by CPU and HC (WIND00130232)
01l,16jul08,w_x  Speaker sound jump issue fix merged (WIND00119020)
01k,04sep07,ami  new variables added in usbOhciInformation
01j,30aug07,ami  Fix for defect WIND00102455
01i,06aug07,jrp  Changing Register access methods
01h,07oct06,ami  Changes for USB-vxBus porting
01g,29mar05,pdg  non-PCI changes
01f,02mar05,ami  Structure members removed (SPR #106373)
01e,03feb05,ami  The check for endpoint halt macro handled properly
                            (OHCI_IS_ENDPOINT_HALTED) for big-endian targets
01d,05oct04,mta  SPR100704- Removal of floating point math
01c,16aug04,pdg  Fix for print-stop-reprint
01b,26jun03,nld  Changing the code to WRS standards
01a,17mar03,ssh  Initial Version
*/

/*
DESCRIPTION

This file contains the data structure declarations for transfer management.
*/

/*
INTERNAL
 *******************************************************************************
 * Filename         : OHCI_TransferManagement.h
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      : This file contains the data structure declarations for
 *                    transfer management.
 *
 *
 ******************************************************************************/

#ifndef __INCusbOhciTransferManagementh
#define __INCusbOhciTransferManagementh

#include <hwif/util/vxbDmaBufLib.h>
#include <lstLib.h>
#include <usb/usbHst.h>
#ifdef  __cplusplus
extern "C" {
#endif

/* defines */

/********************** MODULE SPECIFIC MACRO DEFINITION **********************/

/* To hold the status of the OHCI transfer */
#define USB_OHCI_TRANSFER_STATUS                            UCHAR


/* To hold the USB Descriptor Types (BEGIN) */

/* To hold the value used to identify the USB Device Descriptor */

#define USB_OHCI_DEVICE_DESCRIPTOR_TYPE                     0x01

/* To hold the value used to identify the USB Configuration Descriptor */

#define USB_OHCI_CONFIGURATION_DESCRIPTOR_TYPE              0x02

/* To hold the value used to identify the USB String Descriptor */

#define USB_OHCI_STRING_DESCRIPTOR_TYPE                     0x03

/* To hold the value used to identify the USB Interface Descriptor */

#define USB_OHCI_INTERFACE_DESCRIPTOR_TYPE                  0x04

/* To hold the value used to identify the USB Endpoint Descriptor */

#define USB_OHCI_ENDPOINT_DESCRIPTOR_TYPE                   0x05

/* To hold the USB Descriptor Types (END) */

/* To hold the USB Transfer Types (BEGIN) */

/* Value to specific an USB Control Transfer */

#define USB_OHCI_CONTROL_TRANSFER                           0x00

/* Value to specific an USB Isochronous Transfer */

#define USB_OHCI_ISOCHRONOUS_TRANSFER                       0x01

/* Value to specific an USB Bulk Transfer */

#define USB_OHCI_BULK_TRANSFER                              0x02

/* Value to specific an USB Interrupt Transfer */

#define USB_OHCI_INTERRUPT_TRANSFER                         0x03

/* To hold the USB Transfer Types (END) */

/* Condition codes for transfer completion (BEGIN) */

/* Value to specify 'No Error' condition */

#define USB_OHCI_COMPLETION_CODE_NO_ERROR                   0x00

/* Value to specify 'Device Not Responding' condition */

#define USB_OHCI_COMPLETION_CODE_DEVICE_NOT_RESPONDING      0x05

/* Value to specify 'Not Accessed' condition */

#define USB_OHCI_COMPLETION_CODE_NOT_ACCESSED               0x0F

/* Condition codes for transfer completion (END) */

/********************* MODULE SPECIFIC MACRO DECLARATION **********************/

/* Macro definitions used to populate the endpoint descriptor (BEGIN) */


/*
 * To hold the endpoint direction as defined in the bEndpointAddress field of
 * the USB endpoint descriptor.
 */

#define USB_OUT_ENDPOINT            0x00    /* OUT Endpoint */
#define USB_IN_ENDPOINT             0x80    /* IN Endpoint */

/*
 * To hold the control transfer direction as specified in the bmRequestType
 * field of the SETUP packet.
 */

#define USB_CONTROL_OUT_TRANSFER    0x00    /* Control OUT Transfer */
#define USB_CONTROL_IN_TRANSFER     0x80    /* Control IN Transfer */

/* To hold the SETUP packet size */

#define USB_SETUP_PACKET_LENGTH     0x08

/* To hold the endpoint direction as define in the OHCI Specification */

#define OHCI_BIDIRECTIONAL_ENDPOINT 0x00    /*
                                             * Bidirectional endpoint. Transfer
                                             * direction will be obtained from
                                             * the transfer descriptor (TD).
                                             */

#define OHCI_OUT_ENDPOINT           0x01    /* OUT Endpoint */
#define OHCI_IN_ENDPOINT            0x02    /* IN Endpoint */

/* To hold the format of the transfer descriptor (TD) */

#define OHCI_GENERAL_TD_FORMAT      0x00    /* Control, Bulk or Interrupt TD */
#define OHCI_ISOCHRONOUS_TD_FORMAT  0x01    /* Isochronous TD */

/* Macro definitions used to populate the endpoint descriptor (END) */

/* To hold the data toggle value for the general transfer descriptor */

#define USB_OHCI_TD_DATA_TOGGLE0    0x00000000  /* Data toggle 0 */
#define USB_OHCI_TD_DATA_TOGGLE1    0x01000000  /* Data toggle 1 */

#define USB_OHCD_MAGIC_ALIVE        0xbeefbeef
#define USB_OHCD_MAGIC_DEAD         0xdeadbeef

/*
 * To hold the mask value used to obtain the device address from the URB
 * device handle parameter
 */

#define USB_OHCI_DEVICE_ADDRESS_MASK                                0xFF

/* To hold the value for an invalid pipe handle */

#define USB_OHCI_INVALID_PIPE_HANDLE                                0

/* To hold the value of pipe handle created for the root hub */

#define USB_OHCI_ROOT_HUB_PIPE_HANDLE                               (-1)

/* To hold the memory alignment mask for the OHCI transfer descriptor */

#define USB_OHCI_TD_ALIGN_MASK                                      (0x1F)

/*
 * To hold the maximum transfer length that can be submitted with a single
 * General Transfer Descriptor (TD).
 */

#define USB_OHCI_TD_MAXIMUM_TRANSFER_LENGTH                         (0x1000)

/*
 * To hold the software delay required in scheduling a single TD for
 * a isochronous transfer. Currently fixed at 2 frames per TD.
 */

#define USB_OHCI_ISOCHRONOUS_SCHEDULING_SOFTWARE_DELAY_PER_TD       (4)

/*
 * To hold the number of packets that can be supported by a single
 * isochronous transfer descriptor. Each OHCI isochronous transfer
 * descriptor can send / receive up to 8 packets.
 */

#define USB_OHCI_NUMBER_OF_ISOCHRONOUS_PACKETS_PER_TD               8

/* Maximum size ctrl DATA Transaction */

#define USB_OHCD_CTRL_MAX_DATA_SIZE                                (1 << 16)

/**************** MACROS TO PARSE THE USB ENDPOINT DESCRIPTOR *****************/

/*******************************************************************************
 * MACRO NAME    : USB_GET_ENDPOINT_NUMBER
 * DESCRIPTION   : Macro to obtain the endpoint number from the bEndpointAddress
 *                 field of the USB endpoint descriptor.
 * PARAMETERS    : bEndpointAddress IN  Endpoint address as specified in the
 *                                      USB endpoint descriptor.
 * RETURN TYPE   : None
 ******************************************************************************/
#define USB_GET_ENDPOINT_NUMBER(bEndpointAddress) (bEndpointAddress & 0x0F)

/*******************************************************************************
 * MACRO NAME    : USB_GET_ENDPOINT_DIRECTION
 * DESCRIPTION   : Macro to obtain the endpoint direction from the
 *                 bEndpointAddress field of the USB endpoint descriptor.
 * PARAMETERS    : bEndpointAddress IN  Endpoint address as specified in the
 *                                      USB endpoint descriptor.
 * RETURN TYPE   : None
 ******************************************************************************/
#define USB_GET_ENDPOINT_DIRECTION(bEndpointAddress) (bEndpointAddress & 0x80)

/*******************************************************************************
 * MACRO NAME    : USB_GET_ENDPOINT_TRANSFER_TYPE
 * DESCRIPTION   : Macro to obtain the transfer type from the bmAttributes
 *                 field of the USB endpoint descriptor.
 * PARAMETERS    : bmAttributes IN  Endpoint attributes as specified in the
 *                                  USB endpoint descriptor.
 * RETURN TYPE   : None
 ******************************************************************************/
#define USB_GET_ENDPOINT_TRANSFER_TYPE(bmAttributes) (bmAttributes & 0x03)

/*******************************************************************************
 * MACRO NAME    : USB_GET_CONTROL_TRANSFER_DIRECTION
 * DESCRIPTION   : Macro to obtain the direction of the control transfer.
 * PARAMETERS    : bmRequestType    IN  bmRequestType field of the SETUP
 *                                      packet.
 * RETURN TYPE   : None
 ******************************************************************************/
#define USB_GET_CONTROL_TRANSFER_DIRECTION(bmRequestType)                      \
                (bmRequestType & USB_CONTROL_IN_TRANSFER)


/**************** MACROS TO HANDLE THE OHCI ENDPOINT DESCRIPTOR ***************/

/*******************************************************************************
 * MACRO NAME    : OHCI_POPULATE_ED_CONTROL_INFO
 * DESCRIPTION   : Macro to populate the control information of the OHCI
 *                 endpoint descriptor.
 *                 for the endpoint.
 * PARAMETERS    : index                IN  index of the host controller
 *                 uControlInformation  OUT Control information for the
 *                                          OHCI Endpoint Descriptor.
 *                 uFunctionAddress     IN  Device Address.
 *                 uEndpointNumber      IN  Endpoint number.
 *                 uDirection           IN  Endpoint direction.
 *                 uSpeed               IN  Device speed.
 *                 uFormat              IN  Transfer descriptor format.
 *                 uMaximumPacketSize   IN  Maximum packet size for the
 *                                          endpoint.
 * RETURN TYPE   : None
 ******************************************************************************/

#define OHCI_POPULATE_ED_CONTROL_INFO(index,                                   \
                                      uControlInformation,                     \
                                      uFunctionAddress,                        \
                                      uEndpointNumber,                         \
                                      uDirection,                              \
                                      uSpeed,                                  \
                                      uFormat,                                 \
                                      uMaximumPacketSize)                      \
                                                                               \
        {                                                                      \
        /* Initialize the endpoint descriptor control information */           \
        (uControlInformation) = 0;                                             \
                                                                               \
        /* Populate the device address */                                      \
        (uControlInformation) |= (uFunctionAddress);                           \
                                                                               \
        /* Populate the endpoint number */                                     \
        (uControlInformation) |= (uEndpointNumber << 7);                       \
                                                                               \
        /* Populate the endpoint direction */                                  \
        (uControlInformation) |= (uDirection << 11);                           \
                                                                               \
        /* Populate the device speed */                                        \
        (uControlInformation) |= (uSpeed << 13);                               \
                                                                               \
        /* Populate the transfer descriptor format */                          \
        (uControlInformation) |= (uFormat << 15);                              \
                                                                               \
        /* Populate the maximum packet size for the endpoint */                \
        (uControlInformation) |= (uMaximumPacketSize << 16);                   \
                                                                               \
        uControlInformation =                                                  \
            USB_OHCD_SWAP_DATA(index, uControlInformation);                    \
        }


/*******************************************************************************
 * MACRO NAME    : OHCI_SET_ED_SKIP_BIT
 * DESCRIPTION   : Macro to set the SKIP bit in the control information of the
 *                 OHCI endpoint descriptor.
 * PARAMETERS    : index                IN  index of the host controller
 *                 uControlInformation  OUT Control information for the
 *                                          OHCI Endpoint Descriptor.
 * RETURN TYPE   : None
 ******************************************************************************/
#define OHCI_SET_ED_SKIP_BIT(index, uControlInformation)                      \
    {                                                                         \
    uControlInformation |= USB_OHCD_SWAP_DATA(index,0x00004000);              \
    }

/*******************************************************************************
 * MACRO NAME    : OHCI_CLEAR_ED_SKIP_BIT
 * DESCRIPTION   : Macro to clear the SKIP bit in the control information of
 *                 the OHCI endpoint descriptor.
 * PARAMETERS    : index                IN  index of the host controller
 *                 uControlInformation  OUT Control information for the
 *                                          OHCI Endpoint Descriptor.
 * RETURN TYPE   : None
 ******************************************************************************/
#define OHCI_CLEAR_ED_SKIP_BIT(index, uControlInformation)                     \
    {                                                                          \
    uControlInformation =                                                      \
       (uControlInformation & USB_OHCD_SWAP_DATA(index,(~0x00004000)));        \
    }

/*******************************************************************************
 * MACRO NAME    : OHCI_GET_SKIP_BIT
 * DESCRIPTION   : Macro to get the value of the SKIP bit in the control
 *                 information of the OHCI endpoint descriptor.
 * PARAMETERS    : index                IN  index of the host controller
 *                 uControlInformation  IN Control information for the
 *                                          OHCI Endpoint Descriptor.
 * RETURN TYPE   : UINT32
 ******************************************************************************/
#define OHCI_GET_SKIP_BIT(index, uControlInformation)                          \
         ((USB_OHCD_SWAP_DATA(index, uControlInformation) & 0x00004000) >> 14)

/*******************************************************************************
 * MACRO NAME    : OHCI_GET_ED_MPS
 * DESCRIPTION   : Macro to obtain the maximum packet size of the endpoint from
 *                 the control information of the OHCI endpoint descriptor.
 * PARAMETERS    : index                IN  index of the host controller
 *                 uControlInformation  OUT Control information for the
 *                                          OHCI Endpoint Descriptor.
 * RETURN TYPE   : None
 ******************************************************************************/
#define OHCI_GET_ED_MPS(index, uControlInformation)                            \
            ((USB_OHCD_SWAP_DATA(index, uControlInformation) &                 \
                                    0x07FF0000) >> 16);

/*******************************************************************************
 * MACRO NAME    : OHCI_MODIFY_ED_SPEED_AND_MPS
 * DESCRIPTION   : Macro to modify the speed and the maximum packet size or
 *                 the endpoint descriptor.
 *
 * PARAMETERS    : index                IN  index of the host controller
 *                 uControlInformation  IN  Control information for the
 *                                          OHCI Endpoint Descriptor.
 * RETURN TYPE   : None
 ******************************************************************************/
#define OHCI_MODIFY_ED_SPEED_AND_MPS(index,                                    \
                                     uControlInformation,                      \
                                     uSpeed,                                   \
                                     uMaximumPacketSize)                       \
                                                                               \
        {                                                                      \
        /* To hold intermediate value */                                       \
        UINT32 uTempInfo;                                                      \
                                                                               \
        uTempInfo = USB_OHCD_SWAP_DATA(index, uControlInformation);            \
                                                                               \
        /* Clear the device speed field */                                     \
        (uTempInfo) &= (~0x00002000);                                          \
                                                                               \
        /* Populate the device speed */                                        \
        (uTempInfo) |= (uSpeed << 13);                                         \
                                                                               \
        /* Clear the maximum packet size field for the endpoint */             \
        (uTempInfo) &= (~0x07FF0000);                                          \
                                                                               \
        /* Populate the maximum packet size for the endpoint */                \
        (uTempInfo) |= (uMaximumPacketSize << 16);                             \
                                                                               \
        uControlInformation = USB_OHCD_SWAP_DATA(index, uTempInfo);            \
        }

/*******************************************************************************
 * MACRO NAME    : OHCI_IS_CONTROL_ENDPOINT
 * DESCRIPTION   : Macro to identify whether the OHCI endpoint descriptor
 *                 corresponds to an control endpoint.
 *
 *                 If the ENDPOINT_DESCRIPTOR::FORMAT bit is not set and
 *                 the ENDPOINT_DESCRIPTOR::DIRECTION bit is GET DIRECTION
 *                 FROM TD, it means a control endpoint.
 *
 * PARAMETERS    : index                IN  index of the host controller
 *                 uControlInformation  IN  Control information for the
 *                                          OHCI Endpoint Descriptor.
 * RETURN TYPE   : None
 ******************************************************************************/
#define OHCI_IS_CONTROL_ENDPOINT(index, uControlInformation)                 \
            ((0 == ((USB_OHCD_SWAP_DATA(index, uControlInformation))         \
                        & 0x00008000))                                       \
             &&                                                              \
             (0 == ((USB_OHCD_SWAP_DATA(index, uControlInformation))         \
                        & 0x00001800)))

/*******************************************************************************
 * MACRO NAME    : OHCI_IS_BULK_OR_INTERRUPT_ENDPOINT
 * DESCRIPTION   : Macro to identify whether the OHCI endpoint descriptor
 *                 corresponds to a bulk or interrupt endpoint.
 * PARAMETERS    : index                IN  index of the host controller
 *                 uControlInformation  IN  Control information for the
 *                                          OHCI Endpoint Descriptor.
 * RETURN TYPE   : None
 ******************************************************************************/
#define OHCI_IS_BULK_OR_INTERRUPT_ENDPOINT(index, uControlInformation)         \
            ((0 == ((USB_OHCD_SWAP_DATA(index, uControlInformation))           \
                        & 0x00008000))                                         \
             &&                                                                \
        ((0x00000800 == ((USB_OHCD_SWAP_DATA(index, uControlInformation))      \
                                 & 0x00000800))                                \
              ||                                                               \
        (0x00001000 == ((USB_OHCD_SWAP_DATA(index, uControlInformation))       \
                                 & 0x00001000))))


/*******************************************************************************
 * MACRO NAME    : OHCI_IS_ISOCHRONOUS_ENDPOINT
 * DESCRIPTION   : Macro to identify whether the OHCI endpoint descriptor
 *                 corresponds to an isochronous endpoint.
 * PARAMETERS    : index                IN  index of the host controller
 *                 uControlInformation  IN  Control information for the
 *                                          OHCI Endpoint Descriptor.
 * RETURN TYPE   : None
 ******************************************************************************/
#define OHCI_IS_ISOCHRONOUS_ENDPOINT(index, uControlInformation)               \
            ((USB_OHCD_SWAP_DATA(index, uControlInformation)) & 0x00008000)


/*******************************************************************************
 * MACRO NAME    : OHCI_IS_ENDPOINT_HALTED
 * DESCRIPTION   : Macro to check whether the endpoint is halted.
 * PARAMETERS    : index                IN  index of the host controller
 *                 pEndpointDescriptor  IN  Pointer to the endpoint descriptor.
 * RETURN TYPE   : None
 ******************************************************************************/

#define OHCI_IS_ENDPOINT_HALTED(index, pEndpointDescriptor)                    \
            ((0 !=                                                             \
             ((USB_OHCD_SWAP_DATA(index,                                       \
               (pEndpointDescriptor->uTDQueueHeadPointer)))\
                   & 0x01)) ? TRUE : FALSE)


/*******************************************************************************
 * MACRO NAME    : OHCI_CLEAR_ENDPOINT_HALT
 * DESCRIPTION   : Macro to clear the endpoint halt condition.
 * PARAMETERS    : index                IN  index of the host controller
 *                 pEndpointDescriptor  IN  Pointer to the endpoint descriptor.
 * RETURN TYPE   : None
 ******************************************************************************/
#define OHCI_CLEAR_ENDPOINT_HALT(index, pEndpointDescriptor)                  \
            pEndpointDescriptor->uTDQueueHeadPointer =                        \
             ((pEndpointDescriptor->uTDQueueHeadPointer)                      \
                     & (~(USB_OHCD_SWAP_DATA(index, 0x01))))


/*******************************************************************************
 * MACRO NAME    : OHCI_GET_DEVICE_SPEED_FROM_ED
 * DESCRIPTION   : Macro to obtain the speed of the device from the control
 *                 information of the OHCI endpoint descriptor.
 * PARAMETERS    : index                IN  index of the host controller
 *                 uControlInformation  IN  Control information for the
 *                                          OHCI Endpoint Descriptor.
 * RETURN TYPE   : None
 ******************************************************************************/
#define OHCI_GET_DEVICE_SPEED_FROM_ED(index, uControlInformation)              \
        (((USB_OHCD_SWAP_DATA(index, uControlInformation)) & 0x00002000) >> 13)

 /*******************************************************************************
 * MACRO NAME    : OHCI_GET_DEVICE_ADDRESS_FROM_ED
 * DESCRIPTION   : Macro to obtain the USB device address from the control
 *                 information of the OHCI endpoint descriptor.
 * PARAMETERS    : index                IN  index of the host controller
 *                 uControlInformation  IN  Control information for the
 *                                          OHCI Endpoint Descriptor.
 * RETURN TYPE   : None
 ******************************************************************************/
#define OHCI_GET_DEVICE_ADDRESS_FROM_ED(index, uControlInformation)            \
        ((USB_OHCD_SWAP_DATA(index, uControlInformation)) & 0x0000007F)

/*******************************************************************************
 * MACRO NAME    : OHCI_GET_ENDPOINT_NUMBER_FROM_ED
 * DESCRIPTION   : Macro to obtain the endpoint number from the control
 *                 information of the OHCI endpoint descriptor.
 * PARAMETERS    : index                IN  index of the host controller
 *                 uControlInformation  IN  Control information for the
 *                                          OHCI Endpoint Descriptor.
 * RETURN TYPE   : None
 ******************************************************************************/
#define OHCI_GET_ENDPOINT_NUMBER_FROM_ED(index, uControlInformation)           \
        (((USB_OHCD_SWAP_DATA(index, uControlInformation)) & 0x00000780) >> 7)

/*******************************************************************************
 * MACRO NAME    : OHCI_GET_ENDPOINT_DIRECTION_FROM_ED
 * DESCRIPTION   : Macro to obtain the endpoint direction from the control
 *                 information of the OHCI endpoint descriptor.
 * PARAMETERS    : index                IN  index of the host controller
 *                 uControlInformation  IN  Control information for the
 *                                          OHCI Endpoint Descriptor.
 * RETURN TYPE   : None
 ******************************************************************************/
#define OHCI_GET_ENDPOINT_DIRECTION_FROM_ED(index, uControlInformation)        \
        (((USB_OHCD_SWAP_DATA(index, uControlInformation)) & 0x00001800) >> 11)

/*******************************************************************************
 * MACRO NAME    : OHCI_GET_ENDPOINT_MAXIMUM_PACKET_SIZE_FROM_ED
 * DESCRIPTION   : Macro to obtain the endpoint maximum packet size from the
 *                 control information of the OHCI endpoint descriptor.
 * PARAMETERS    : index                IN  index of the host controller
 *                 uControlInformation  IN  Control information for the
 *                                          OHCI Endpoint Descriptor.
 * RETURN TYPE   : None
 ******************************************************************************/
#define OHCI_GET_ENDPOINT_MAXIMUM_PACKET_SIZE_FROM_ED(index,uControlInformation)     \
        (((USB_OHCD_SWAP_DATA(index, uControlInformation)) & 0x07FF0000) >> 16)


/*******************************************************************************
 * MACRO NAME    : OHCI_GET_TOGGLE_CARRY_FROM_ED
 * DESCRIPTION   : Macro to obtain the toggle carry
 *                 information of the OHCI endpoint descriptor.
 * PARAMETERS    : index    IN  index of the host controller
 *                 uHeadTD  IN  TD which forms the head of the TD list
 * RETURN TYPE   : None
 ******************************************************************************/

#define OHCI_GET_TOGGLE_CARRY_FROM_ED(index, uHeadTD)                          \
        ((USB_OHCD_SWAP_DATA(index, uHeadTD) & 0x02) >> 1)

/*******************************************************************************
 * MACRO NAME    : OHCI_SET_ENDPOINT_TOGGLE
 * DESCRIPTION   : Macro to set the toggle field.
 * PARAMETERS    : index                IN  index of the host controller
 *                 pEndpointDescriptor  IN  Pointer to the OHCI ED.
 * RETURN TYPE   : None
 ******************************************************************************/

#define OHCI_SET_ENDPOINT_TOGGLE(index, pEndpointDescriptor)                   \
                pEndpointDescriptor->uTDQueueHeadPointer =                     \
                ((pEndpointDescriptor->uTDQueueHeadPointer)                    \
                     | (USB_OHCD_SWAP_DATA(index, 0x02)))

/*******************************************************************************
 * MACRO NAME    : OHCI_RESET_ENDPOINT_TOGGLE
 * DESCRIPTION   : Macro to reset the toggle field.
 * PARAMETERS    : index                IN  index of the host controller
 *                 pEndpointDescriptor  IN  Pointer to the OHCI ED.
 * RETURN TYPE   : None
 ******************************************************************************/
#define OHCI_RESET_ENDPOINT_TOGGLE(index, pEndpointDescriptor)                 \
                pEndpointDescriptor->uTDQueueHeadPointer =                     \
                 ((pEndpointDescriptor->uTDQueueHeadPointer)                   \
                     & (USB_OHCD_SWAP_DATA(index, (ULONG)(~0x02))))

/************ MACROS TO HANDLE THE OHCI GENERAL TRANSFER DESCRIPTOR ***********/

/*******************************************************************************
 * MACRO NAME    : USB_OHCI_SET_TD_BUFFER_ROUND
 * DESCRIPTION   : Macro to set the buffer rounding field of the transfer
 *                 descriptor. This macro is meaningful only for general
 *                 transfer descriptor.
 * PARAMETERS    : index                IN  index of the host controller
 *                 uControlInformation  IN  Control information for the
 *                                          OHCI Transfer Descriptor.
 * RETURN TYPE   : None
 ******************************************************************************/
#define USB_OHCI_SET_TD_BUFFER_ROUND(index, uControlInformation)               \
    {                                                                          \
    uControlInformation |= USB_OHCD_SWAP_DATA(index, 0x00040000);              \
    }


/*******************************************************************************
 * MACRO NAME    : USB_OHCI_SET_TD_PID_OUT
 * DESCRIPTION   : Macro to set PID field of the TD to OUT.
 * PARAMETERS    : index                IN  index of the host controller
 *                 uControlInformation  IN  Control information for the OHCI
 *                                          Transfer Descriptor.
 * RETURN TYPE   : None
 ******************************************************************************/
#define USB_OHCI_SET_TD_PID_OUT(index, uControlInformation)                    \
    {                                                                          \
    uControlInformation |= USB_OHCD_SWAP_DATA(index, 0x00080000);              \
    }


/*******************************************************************************
 * MACRO NAME    : USB_OHCI_SET_TD_PID_IN
 * DESCRIPTION   : Macro to set PID field of the TD to IN.
 * PARAMETERS    : index                IN  index of the host controller
 *                 uControlInformation  IN  Control information for the OHCI
 *                                          Transfer Descriptor.
 * RETURN TYPE   : None
 ******************************************************************************/
#define USB_OHCI_SET_TD_PID_IN(index, uControlInformation)                     \
    {                                                                          \
    uControlInformation |= USB_OHCD_SWAP_DATA(index, 0x00100000);              \
    }

/*******************************************************************************
 * MACRO NAME    : USB_OHCI_SET_TD_DATA_TOGGLE
 * DESCRIPTION   : Macro to set data toggle for the TD.
 * PARAMETERS    : index                IN  index of the host controller
 *                 uControlInformation  IN  Control information for the OHCI
 *                                          Transfer Descriptor.
 *                 uDataToggleValue     IN  Data toggle value to be set.
 * RETURN TYPE   : None
 ******************************************************************************/

#define USB_OHCI_SET_TD_DATA_TOGGLE(index, uControlInformation, uDataToggleValue)  \
    {                                                                              \
    uControlInformation |= USB_OHCD_SWAP_DATA(index, (0x02000000 | uDataToggleValue)); \
    }

/*******************************************************************************
 * MACRO NAME    : USB_OHCI_CLEAR_TD_COMPLETION_INTERRUPT
 * DESCRIPTION   : Macro to clear the transfer completion interrupt for the
 *                 transfer descriptor. This macro can be used for both
 *                 general transfer descriptor and isochronous transfer
 *                 descriptor.
 * PARAMETERS    : index                IN  index of the host controller
 *                 uControlInformation  IN  Control information for the
 *                                          OHCI Transfer Descriptor.
 * RETURN TYPE   : None
 ******************************************************************************/

#define USB_OHCI_CLEAR_TD_COMPLETION_INTERRUPT(index, uControlInformation)     \
    {                                                                          \
    uControlInformation |= USB_OHCD_SWAP_DATA(index, 0x00E00000);              \
    }

/*******************************************************************************
 * MACRO NAME    : USB_OHCI_IS_GENERAL_TRANSFER_SUCCESSFUL
 * DESCRIPTION   : Macro to check whether the general transfer descriptor
 *                 completed successfully.
 * PARAMETERS    : index                IN  index of the host controller
 *                 uControlInformation  IN  Control information for the
 *                                          OHCI General Transfer Descriptor.
 * RETURN TYPE   : None
 ******************************************************************************/
#define USB_OHCI_IS_GENERAL_TRANSFER_SUCCESSFUL(index, uControlInformation)    \
        ((0x00000000 == ((USB_OHCD_SWAP_DATA(index, uControlInformation))      \
                            & 0xF0000000)) ? TRUE : FALSE)

/*******************************************************************************
 * MACRO NAME    : USB_OHCI_IS_ISOCHRONOUS_TRANSFER_SUCCESSFUL
 * DESCRIPTION   : Macro to check whether the isochronous transfer descriptor
 *                 completed successfully.
 * PARAMETERS    : index                IN  index of the host controller
 *                 uControlInformation  IN  Control information for the
 *                                          OHCI Isochronous Transfer
 *                                          Descriptor.
 * RETURN TYPE   : None
 ******************************************************************************/
#define USB_OHCI_IS_ISOCHRONOUS_TRANSFER_SUCCESSFUL(index, uControlInformation)\
        ((0x00000000 == ((USB_OHCD_SWAP_DATA(index, uControlInformation))      \
                            & 0xF0000000)) ? TRUE : FALSE)

/*******************************************************************************
 * MACRO NAME    : USB_OHCI_GET_TRANSFER_COMPLETION_CODE
 * DESCRIPTION   : Macro to obtain the completion code for the transfer.
 * PARAMETERS    : index                IN  index of the host controller
 *                 uControlInformation  IN  Control information for the
 *                                          OHCI General Transfer Descriptor.
 * RETURN TYPE   : None
 ******************************************************************************/
#define USB_OHCI_GET_TRANSFER_COMPLETION_CODE(index, uControlInformation)      \
   (((USB_OHCD_SWAP_DATA(index, uControlInformation)) & 0xF0000000) >> 28)

/*******************************************************************************
 * MACRO NAME    : USB_OHCI_GET_ISO_TD_FRAME_NUM
 * DESCRIPTION   : Macro to get frame number from the ISO TD.
 * PARAMETERS    : index                IN  index of the host controller
 *                 uControlInformation  IN  Control information for the
 *                                          OHCI Isochronous Transfer Descriptor.
 * RETURN TYPE   : None
 ******************************************************************************/
#define USB_OHCI_GET_ISO_TD_FRAME_NUM(uIndex,uControlInformation)\
          (USB_OHCD_SWAP_DATA(uIndex, uControlInformation) & 0x0000FFFF)

/*******************************************************************************
 * MACRO NAME    : USB_OHCI_GET_ISO_TD_PACKET
 * DESCRIPTION   : Macro to get the number of the packets from the ISO TD.
 * PARAMETERS    : index                IN  index of the host controller
 *                 uControlInformation  IN  Control information for the
 *                                          OHCI Isochronous Transfer Descriptor.
 * RETURN TYPE   : None
 ******************************************************************************/

#define USB_OHCI_GET_ISO_TD_PACKET(uIndex,uControlInformation)                 \
        ((USB_OHCD_SWAP_DATA(uIndex, uControlInformation) & 0x0F000000) >> 24);

/********************** MACROS FOR BANDWIDTH CALCULATION **********************/

/* To hold the maximum polling interval supported by the OHCI host controller */
#define USB_OHCI_MAXIMUM_POLLING_INTERVAL                   32

/* To hold the polling interval for isochronous endpoint (fixed to 1 ms) */
#define USB_OHCI_ISOCHRONOUS_ENDPOINT_POLLING_INTERVAL      1

/*
 * To hold the host controller and system dependent delay due to gaining access
 * to memory or other implementation dependencies.
 */
#define OHCI_HOST_DELAY                             0

/* To hold the delay an hub takes to enable low speed transactions */
#define OHCI_HUB_LS_SETUP_DELAY                     0

/*******************************************************************************
 * MACRO NAME    : USB_OHCI_COMPUTE_BANDWIDTH_FOR_INTERRUPT_ENDPOINT
 * DESCRIPTION   : Macro to compute the bandwidth required for an interrupt
 *                 endpoint of the specified maximum packet size.
 *
 *                 NOTE: THE FOLLOWING BANDWIDTH FORMULAS ARE DIRECTLY TAKEN
 *                       FROM THE USB 1.1 SPECIFICATION.
 *
 * PARAMETERS    : SPEED                IN  Speed of the device.
 *                 DIRECTION            IN  Direction of the endpoint.
 *                 MAXIMUM_PACKET_SIZE  IN  Maximum packet size of the
 * RETURN TYPE   : Returns the bandwidth required for the specified interrupt
 *                 endpoint.
 ******************************************************************************/
#define USB_OHCI_COMPUTE_BANDWIDTH_FOR_INTERRUPT_ENDPOINT(                  \
            SPEED,                                                          \
            DIRECTION,                                                      \
            MAXIMUM_PACKET_SIZE)                                            \
        ((USBHST_FULL_SPEED == (SPEED))?                                    \
            (9107 +                                                         \
             (84 * (4 + (MAXIMUM_PACKET_SIZE) * 2)) +                       \
             OHCI_HOST_DELAY):                                              \
            ((OHCI_IN_ENDPOINT == (DIRECTION))?                             \
                (64060 +                                                    \
                 (2 * OHCI_HUB_LS_SETUP_DELAY) +                            \
                 (677 * (4 + (MAXIMUM_PACKET_SIZE) * 2)) +                  \
                 OHCI_HOST_DELAY):                                          \
                (64107 +                                                    \
                 (2 * OHCI_HUB_LS_SETUP_DELAY) +                            \
                 (667 * (4 + (MAXIMUM_PACKET_SIZE) * 2)) +                  \
                 OHCI_HOST_DELAY)))

/*******************************************************************************
 * MACRO NAME    : USB_OHCI_COMPUTE_BANDWIDTH_FOR_ISOCHRONOUS_ENDPOINT
 * DESCRIPTION   : Macro to compute the bandwidth required for an isochronous
 *                 endpoint of the specified maximum packet size.
 *
 *                 NOTE: THE FOLLOWING BANDWIDTH FORMULAS ARE DIRECTLY TAKEN
 *                       FROM THE USB 1.1 SPECIFICATION.
 *
 * PARAMETERS    : SPEED                IN  Speed of the device.
 *                 DIRECTION            IN  Direction of the endpoint.
 *                 MAXIMUM_PACKET_SIZE  IN  Maximum packet size of the
 *                                          isochronous endpoint.
 * RETURN TYPE   : Returns the bandwidth required for the specified isochronous
 *                 endpoint.
 ******************************************************************************/
#define USB_OHCI_COMPUTE_BANDWIDTH_FOR_ISOCHRONOUS_ENDPOINT(                \
            SPEED,                                                          \
            DIRECTION,                                                      \
                    MAXIMUM_PACKET_SIZE)                                    \
        ((OHCI_IN_ENDPOINT == (DIRECTION))?                                  \
            (7268 +                                                         \
             (84 * (4 + (MAXIMUM_PACKET_SIZE) * 2)) +                       \
             OHCI_HOST_DELAY):                                              \
            (6265 +                                                         \
             (84 * (4 + (MAXIMUM_PACKET_SIZE) * 2)) +                       \
             OHCI_HOST_DELAY))

/* typedefs */

/****************** MODULE SPECIFIC DATA STRUCTURE DECLARATION ****************/

/* Structure to hold the list of URB's pending for the endpoint */
struct usbOhciTransferDescriptor;

/* Structure to do address translate */
struct usbOhciMemMapTable;

#define USB_OHCD_DIR_IN     (0x80)
#define USB_OHCD_DIR_OUT    (0x00)

typedef struct _USB_OHCD_REQUEST_INFO
    {
    pUSBHST_URB                         pUrb;
                /* To hold the pointer to the URB */
    struct usbOhciTransferDescriptor    *pTDListHead;
                /* To hold the pointer to head of transfer descriptor */
                /* list corresponding to the transfer */
    struct usbOhciTransferDescriptor    *pTDListTail;
                /* To hold the pointer to tail of transfer descriptor */
                /* list corresponding to the transfer */
    NODE                                 listNode;

    NODE                                 cancelNode;

    UINT32                               uDataDir;
    UINT32                               uRequestLength;

                /* To hold the pointer to next element in URB list */
    VXB_DMA_MAP_ID ctrlSetupDmaMapId;   /* DMA MAP ID used for control setup */
    VXB_DMA_MAP_ID usrDataDmaMapId;     /* DMA MAP ID used for user data */
    } USB_OHCD_REQUEST_INFO, *pUSB_OHCD_REQUEST_INFO;

/* Structure to hold the Endpoint Descriptor Information */

#define LIST_NODE_TO_USB_OHCD_REQUEST_INFO(pNode)        \
     ((USB_OHCD_REQUEST_INFO *) ((char *) (pNode) -      \
     OFFSET(USB_OHCD_REQUEST_INFO, listNode)))


#define CANCEL_NODE_TO_USB_OHCD_REQUEST_INFO(pNode)       \
     ((USB_OHCD_REQUEST_INFO *) ((char *) (pNode) -       \
       OFFSET(USB_OHCD_REQUEST_INFO, cancelNode)))

/*
 * USB_OHCD_PIPE_STATE_NORMAL : pipe is in normal state
 * USB_OHCD_PIPE_STATE_CANCEL : pipe is added into disable list for cancel
 *                              some request
 * USB_OHCD_PIPE_STATE_RESET  : pipe is added into disable list for reset
 * USB_OHCD_PIPE_STATE_DELETE : pipe is added into disable list for delete
 *
 * If the pipe is in normal state, we can update pipe's current state to
 * (cancel/rest/delete) state if we have the related clean request.
 *
 * If the pipe is in cancel state,
 * when we receive a reset request, update the pipe's current state to reset.
 * when we receive a delete request, set pipe's next state to delete.
 *
 * If the pipe is in reset state,
 * when we receive a cancel request, keep the pipe's current/next state.
 * when we receive a delete request, set pipe's next state to delete.
 *
 * If the pipe is in delete state,
 * when we receive a cancel/reset request, keep the pipe's current/next state.
 */

typedef enum _USB_OHCD_PIPE_STATE
    {
    USB_OHCD_PIPE_STATE_NORMAL = 0,
    USB_OHCD_PIPE_STATE_CANCEL,
    USB_OHCD_PIPE_STATE_RESET,
    USB_OHCD_PIPE_STATE_DELETE
    }USB_OHCD_PIEP_STATE;

typedef struct _USB_OHCD_PIPE
    {
    UINT32                              uControlInformation;
    UINT32                              uTDQueueTailPointer;
    UINT32                              uTDQueueHeadPointer;
    UINT32                              uNextEDPointer;

    LIST                                requestList;
    LIST                                freeRequestList;
    LIST                                cancelRequestList;

    struct usbOhciTransferDescriptor    *pFreeTDHead; /* To hold the head of the Free TD list */


    struct usbOhciTransferDescriptor    *pFreeTDTail; /* To hold the tail of the Free TD list */

    VOID *  pHCDTDTail;/* To hold the pointer to the tail of the TD list */
                       /* To hold the actual pointer to the ED */

    PVOID                               pLastCompletedTD;
                /* To hold the pointer to last transfer desriptor completed */
                /* for this endpoint */

    struct _USB_OHCD_PIPE               *pHCDNextPipe; /* Pointer to the next pipe structure */

    struct usbOhciMemMapTable           *pMemMap;

    NODE    disableNode;

    OS_EVENT_ID PipeSynchEventID; /* To synchronise the request lists
                                   * for the endpoint.
                                   */


    VXB_DMA_TAG_ID ctrlSetupDmaTagId; /* DMA TAG ID used for control setup */
    VXB_DMA_TAG_ID usrBuffDmaTagId; /* DMA TAG ID used for user data */
    VXB_DMA_MAP_ID dmaMapId;   /* Our DMA MAP ID */

    /* Software members */
    size_t   uMaxTransferSize; /* Maxium possible transfer size of this pipe */

    size_t   uMaxNumReqests;   /* Maxium possible transfer requests at a time */

    UINT32   uFlags;  /* Any possible flags */
    UINT32   uBandwidthAvailable;
    USB_OHCD_PIEP_STATE uCurPipeState; /* Current state of the pipe */
    USB_OHCD_PIEP_STATE uNextPipeState; /* Next state of the pipe */
    UINT8    uPollingInterval;
    UINT8    uEndpointType;

   } USB_OHCD_PIPE;

#define DISABLE_NODE_TO_USB_OHCI_ENDPOINT_DESCRIPTOR(pNode)       \
     ((USB_OHCD_PIPE *) ((char *) (pNode) -                       \
                            OFFSET(USB_OHCD_PIPE, disableNode)))


typedef struct _USB_OHCD_PIPE *  pUSB_OHCD_PIPE;

typedef struct usbOhciTransferDescriptor
    {
    UINT32                              uControlInformation;
    UINT32                              uCurrentBufferPointer;
    UINT32                              uNextTDPointer;
    UINT32                              uBufferEndPointer;
    UINT32                              uOffsetAndStatus[4];

    /* Software members */

    pUSB_OHCD_PIPE                      pHCDPipe;
    pUSB_OHCD_REQUEST_INFO              pRequestInfo;  /* Pointer to the request info */

    /* To hold pointer to the endpoint descriptor. This pointer */
    /* will be used to handle the completion of transfers */
    struct usbOhciTransferDescriptor *pHCDNextTransferDescriptor;
    /* To hold the pointer to the next Transfer Descriptor. This */
    /* pointer will be used to handle the transfer completion. An */
    /* additional pointer is required because of the following */
    /* reason: */
    /*      a)  On completion of a transfer, the host controller */
    /*          moves the transfer to the done head list. */
    /*      b)  In the done head list, the transfers list is in */
    /*          reverse and has to be modified before processing */

    struct usbOhciMemMapTable             *pMemMap;

    UINT32 uTDActualLength;       /* Actual length of data transferred */
    UINT32 uStartAddressPointer;  /* Copy of the current buffer pointer */

    VXB_DMA_MAP_ID                      dmaMapId;   /* Our DMA MAP ID */
    } USB_OHCI_TD,*PUSB_OHCI_TD;


typedef struct usbOhciHcca
    {
    /* This array must be the first in the HCCA structure */

    UINT32  uHccaInterruptTable[32];    /* To hold the pointer to the */
                            /* interrupt list. It consists of 32 pointers to */
                            /* the interrupt endpoint descriptors (EDs) */
    UINT16  uHccaFrameNumber;           /* To hold the current frame number */
                            /* which will be used by the Host Controller */
    UINT16  uHccaPad1;                  /* To be used by the Host Controller */
    UINT32  uHccaDoneHead;              /* To hold the pointer to Done Head */
    UINT32  uReserved[29];              /* Reserved for future use */
    } USB_OHCI_HCCA;

typedef struct usbOhciHcca *        PUSB_OHCI_HCCA;

typedef struct usbOhciMemMapTable
    {
    NODE       memNode;
    bus_addr_t uBusAddr;
    PVOID      pVirtAddr;
    } USB_OHCI_MEM_MAP_TABLE, *pUSB_OHCI_MEM_MAP_TABLE;

#define MEM_NODE_TO_USB_OHCI_MEM_MAP_TABLE(pNode)              \
     ((USB_OHCI_MEM_MAP_TABLE *) ((char *) (pNode) -           \
                            OFFSET(USB_OHCI_MEM_MAP_TABLE, memNode)))



/* Structure to hold the OHCI Controller Information. If there are multiple
 * OHCI Controllers on a system, for each such OHCI Controller a variables of
 * this structure should be created.
 */
typedef struct _USB_OHCD_DATA
    {
    UINT32  uBusIndex;
    UINT32  isrMagic;
    pUSB_OHCD_PIPE   pDefaultPipe;
                /* To hold the pointer to the default to control endpoint */
                /* descriptor. This pointer remains as long as the host */
                /* controller driver is loaded. For every new device */
                /* connection, this pointer will be used for initial */
                /* communication with the device. This pointer should be */
                /* aligned to 16 bytes boundary. In order to acheive this, */
                /* memory as big as {sizeof (ENDPOINT_DESCRIPTOR) + 16} bytes */
                /* is allocated. The memory is then aligned to 16 bytes */
                /* boundary and then programmed into the OHCI Controller */
                /* registers. */
    pUSB_OHCD_PIPE   pControlPipeListTail;
                /* To hold the pointer to tail of list of control endpoints */
    pUSB_OHCD_PIPE   pBulkPipeListTail;
                /* To hold the pointer to tail of the list of bulk endpoints */
    PUSB_OHCI_HCCA  pHcca;
                /* Pointer to the OHCI Host Controller Communication Area. */
                /* This pointer should be aligned to 256 bytes boundary. In */
                /* order to achieve this, memory as big as {sizeof (OHCI_HCCA)*/
                /* + 256} bytes is allocated. The memory is then aligned to */
                /* 256 bytes boundary and then programmed into the OHCI */
                /* Controller registers. */

    LIST         disableEDList;
    LIST         edMemMapTableList;
    LIST         tdMemMapTableList;
    OS_EVENT_ID  memMapTableEvent;
    OS_EVENT_ID  ohciCommonMutex;
    /* Data members to support root hub emulation (BEGIN) */

    pUSBHST_URB                     pRootHubInterruptRequest;
                /* To hold the URB pending interrupt request (status change */
                /* request) for the root hub. */
    OS_EVENT_ID                     pipeListAccessEvent;
                /* To hold the event to be used for modifying the endpoint */
                /* list. It is logical to have separate events for control, */
                /* bulk, interrupt and isochronous list. This will be lead */
                /* to storing four events. However, this event is needed */
                /* only for adding, modifying or deleting an endpoint */
                /* descriptor. Since these operations occur rarely, only one */
                /* event is used for all the list. This event is used in the */
                /* following cases: */
                /*      a)  Modify the properties of the default control pipe */
                /*          (address 0, endpoint 0). */
                /*      b)  To add a new pipe to the list of pipes. */
                /*      c)  To delete a pipe from the list of pipes. */
                /*      d)  For queuing a pipe marked for deletion. Queuing */
                /*          the pipe in the list pointed to by */
                /*          pDisabledEndpointListHead. */
                /*      e)  For queuing a pipe in which transfers are marked */
                /*          for cancellation. */
    UINT32  uMaximumBandwidthAvailable;
                /* To hold the maximum periodic bandwidth available on this */
                /* OHCI host controller. */

    OS_THREAD_ID    isrThreadId;    /* To hold the Thread ID of ISR function */

#ifndef USB_OHCI_POLLING_MODE
    OS_EVENT_ID     isrEvent;   /* To hold the ISR event. This event will be */
                                /* signalled when an interrupt is pending. */
#endif /* End of #ifndef USB_OHCI_POLLING_MODE */

    UINT8   uNumberOfDownStreamPorts;
                /* To hold the number of downstream ports supported on the */
                /* OHCI Controller */
    UINT8   uRootHubState;  /* To hold the USB device state of the root hub */
    UINT8   uRootHubAddress;/* To hold the USB address assigned to root hub */
    UINT8   bCurrentConfigurationValue; /* To hold the current configuration */
                                    /* value of the root hub */
    BOOLEAN bRemoteWakeupEnabled;   /* To hold the flag indicating whether */
                                    /* the remote wake up feature is */
                                    /* enabled or not */

    BOOLEAN bHostControllerInitialized; /* Flag to indicate whether this host */
                                        /* controller is initialized */

    /* Data members to support root hub emulation (END) */
    UINT8 bLastFrameRollover;    /* To keep track of whether there
                                  * has frame number rollover event for
                                  * the last schdule
                                  */
    /* to store the vxBus related information */

    VXB_DEVICE_ID   pDev;

    VOID *          pRegAccessHandle;   /* Handle for the register
                                         * access methods
                                         */
    ULONG           regBase;           /* pDev->pRegBase[i] */
    int             regIndex;
    int             usrDmaMapFlags;  /* User buffer flags */
    VXB_DMA_TAG_ID  ohciParentTag; /* Our parent TAG ID */
    VXB_DMA_TAG_ID  edDmaTagId;        /* DMA TAG ID used for the QH */
    VXB_DMA_TAG_ID  tdDmaTagId;        /* DMA TAG ID used for the general TD */
    VXB_DMA_TAG_ID  hccaDmaTagId;
    VXB_DMA_MAP_ID  hccaDmaMapId;
    UINT32          (*pDataSwap)(UINT32 data); /* function pointer
                                                * for byte conversions
                                                */
    } USB_OHCD_DATA;

typedef struct _USB_OHCD_DATA *     pUSB_OHCD_DATA;

extern USB_OHCD_DATA * g_OHCDData ; //zj
/********** DATA STRUCTURES FOR PARSING THE USB STANDARD DESCRIPTORS **********/

/* Structure of the USB Generic descriptor */

typedef struct usbOhciGenericDescriptor
    {
    UINT8   bLength;            /* To hold the length of the descriptor */
    UINT8   bDescriptorType;    /* To hold the type of the descriptor */
    UINT8   bDescriptorData[1]; /* To hold additional fields of descriptor */
    } __attribute__((__packed__)) USB_OHCI_GENERIC_DESCRIPTOR;

typedef struct usbOhciGenericDescriptor *   PUSB_OHCI_GENERIC_DESCRIPTOR;

/* Structure of the USB configuration descriptor */

typedef struct usbOhciConfigurationDescriptor
    {
    UINT8   bLength;    /* To hold the length of configuration descriptor */
    UINT8   bDescriptorType;    /* To hold the type of the descriptor */
    UINT16  wTotalLength;/* To hold total length of configuration descriptor */
    UINT8   bNumInterfaces; /* To hold number of interfaces in configuration */
    UINT8   bConfigurationValue;/* To hold configuration value to enable */
                                /* this configuration */
    UINT8   iConfiguration; /* To hold the index of the string descriptor */
                            /* describing this configuration */
    UINT8   bmAttributes;   /* To hold the attributes of this configuration */
    UINT8   MaxPower;   /* To hold the maximum power consumption of the */
                        /* device in this configuration */
    USB_OHCI_GENERIC_DESCRIPTOR     GenericDescriptor[1];
                /* To hold the descriptors following the configuration */
                /* descriptor. Normally, an interface descriptor follows */
                /* the configuration descriptor. However, class specific */
                /* descriptors can also follow the configuration descriptor. */
    } __attribute__((__packed__)) USB_OHCI_CONFIGURATION_DESCRIPTOR;

typedef struct usbOhciConfigurationDescriptor *
                    PUSB_OHCI_CONFIGURATION_DESCRIPTOR;

/* Structure of the USB interface descriptor */

typedef struct usbOhciInterfaceDescriptor
    {
    UINT8   bLength;    /* To hold the length of the interface descriptor */
    UINT8   bDescriptorType;    /* To hold the type of the descriptor */
    UINT8   bInterfaceNumber;   /* To hold the interface number */
    UINT8   bAlternateSetting;  /* To hold the alternate setting number */
    UINT8   bNumEndpoints;      /* To hold the number of endpoints */
    UINT8   bInterfaceClass;    /* To hold the interface class code */
    UINT8   bInterfaceSubClass; /* To hold the interface sub class code */
    UINT8   bInterfaceProtocol; /* To hold the interface protocol code */
    UINT8   iInterface;         /* To hold the index of the string */
                                /* descriptor describing this interface */
    USB_OHCI_GENERIC_DESCRIPTOR     GenericDescriptor[1];
            /* To hold the descriptors following the interface descriptor. */
            /* Normally, an endpoint descriptor follows the interface */
            /* descriptor. However, class specific descriptors can also */
            /* follow the interface descriptor. */
    } __attribute__((packed)) USB_OHCI_INTERFACE_DESCRIPTOR;

typedef struct usbOhciInterfaceDescriptor *     PUSB_OHCI_INTERFACE_DESCRIPTOR;

/* Structure of the USB endpoint descriptor */

typedef struct usbEndpointDescriptor
    {
    UINT8   bLength;    /* To hold the length of the endpoint descriptor */
    UINT8   bDescriptorType;    /* To hold the type of the descriptor */
    UINT8   bEndpointAddress;   /* To hold the endpoint address (direction */
                                /* and endpoint number) */
    UINT8   bmAttributes;   /* To hold endpoint attributes (transfer type) */
    UINT16  wMaxPacketSize; /* To hold maximum packet size for the endpoint */
    UINT8   bInterval;  /* To hold the polling interface for the endpoint */
    } __attribute__((packed)) USB_ENDPOINT_DESCRIPTOR;

typedef struct usbEndpointDescriptor *      PUSB_ENDPOINT_DESCRIPTOR;

#ifdef  __cplusplus
}
#endif

#endif /* __INCusbOhciTransferManagementh */

/* End of File usbOhciTransferManagement.h */
