/* usbUhcdInitialization.c - USB UHCI HCD initialization routine */

/*
 * Copyright (c) 2003-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2003-2013 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
Modification history
--------------------
03h,03may13,wyy  Remove compiler warning (WIND00356717)
03g,21apr13,wyy  Cut the relationship among busIndex of HCDData, unitNumber of
                 vxBus Device, and HC count (such as g_EHCDControllerCount or
                 g_UhcdHostControllerCount) to dynamically release or announce
                 a HCD driver (WIND00362065)
03f,04jan13,s_z  Remove compiler warning (WIND00390357)
03e,05nov12,ljg  Remove member halted from USB_UHCD_QH (WIND00386978)
03d,13dec11,m_y  Modify according to code check result (WIND00319317)
03c,01aug11,ghs  Remove delete hook when UHCD initialize failed (WIND00255117)
03b,07jan11,ghs  Clean up compile warnings (WIND00247082)
03a,14dec10,ghs  Change the usage of the reboot hook APIs to specific
                 rebootHookAdd/rebootHookDelete (WIND00240804)
02z,22nov10,ghs  Code Coverity CID(9695): UNUSED_VALUE (WIND00242477)
02y,14sep10,ghs  Delete reboot hook when HCD init failed
02x,06sep10,j_x  Extern usbUhcdDisableHC function (WIND00205038)
02w,02sep10,ghs  Use OS_THREAD_FAILURE to check taskSpawn failure (WIND00229830)
02v,01sep10,j_x  Remove hook from reboot hook table when UHCD exit (WIND00229326)
02u,23aug10,m_y  Update prototype for EHCI/OHCI/UHCI init routine
                 (WIND00229662)
02t,23aug10,m_y  Fix usbUhcdExit fail issue (WIND00229642)
02s,28jul10,ghs  Fix uchd exit issue(WIND00224419)
02r,22jul10,m_y  Modify host controller index compare
02q,06jul10,m_y  Set all the resources to NULL after release (WIND00183499)
02p,25jun10,w_x  Correct debug logging macro
02o,31may10,w_x  Change active pipe lock as mutex fixing dead lock(WIND00214890)
02n,18may10,w_x  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
02m,09mar10,j_x  Changed for USB debug (WIND00184542)
02l,13jan10,ghs  vxWorks 6.9 LP64 adapting
02k,17jul09,ghs  Add USB_VXB_VERSIONID for usb (WIND00172037)
02j,20mar09,w_x  Added support for UHCD_POLLING_MODE (WIND00160880);
                 Move calls to register R/W out from usbHcdUhciDeviceInit;
                 Added uhcdRegShow;
02i,15feb09,w_x  Re-study the code with clean up (no functinality change);
                 Fix vxBus related HCD re-init issue (WIND00152849);
                 Move spinLockIsrUhci[] into USB_UHCD_DATA as spinLockIsr;
02h,14aug08,w_x  Remove unnecessary port manage task and clean up root hub
                 emulation code (WIND00130272)
02g,01aug08,j_x  vxBus USB add dataSwap method for PLB devices
02f,18jun08,h_k  removed pAccess.
02e,04jun08,w_x  Removed UHCD Job queue functions;
                 changed usbUhcdSemCritical from binary semaphore to mutex;
                 (WIND00121282 fix)
02d,03jun08,j_x  Convert vxBus API busCfgRead/Wirte to vxbPciDevCfgRead/Write
02c,20sep07,tor  VXB_VERSION_3
02b,06sep07,p_g  Added code for Non-PCI host controller over vxBus
02a,05sep07,jrp  APIGEN updates
01z,23aug07,jrp  WIND00101202 vxb Command line prototypes.
01y,29may07,jrp  Added UHCD Job queue functions (vxBus version)
01x,07aug07,jrp  Register access method change
01w,25jul07,jrp  WIND00099137 - changing initialization order
01v,17jul07,jrp  Adding Instantiation routine
01u,11jul07,jrp  Remove stdout announcments
01t,29jun07,ami  Defect fix on UHCI legacy support (SPR # WIND00089389)
01s,13jun07,tor  remove VIRT_ADDR
01r,22May07,jrp  Preinitialized Register Access methods
01q,29Mar07,tor  clean up unlink method
01p,27mar07,jrp  SMP Conversion
01o,08oct06,ami  Changes for USB-vxBus changes
01n,11apr05,ami  Function for warm reboot added
01m,28mar05,pdg  non-PCI changes
01l,02mar05,ami  SPR #106383 (Max UHCI Host Controller Issue)
01k,25feb05,mta  SPR 106276
01j,25feb05,ami  Check in usbUhciExit routine (SPR #106559)
01i,09feb05,mta  SPR 106159: Detach UHCI gives error
01h,15oct04,ami  Apigen Changes
01g,17aug04,pdg  Fix for removing the dependency on OHCI while building the
                 project
01f,03aug04,mta  coverity error fixes
01e,08Sep03,nrv  Changed g_HostControllerCount to g_UHCDControllerCount
01d,28jul03,mat  Endian Related Changes
01c,10jul03,mat  Bus Abstraction Related changes
01b,26jun03,mat  changing the code to WRS standards.
01a,25apr03,ram  written.
*/

/*
DESCRIPTION

This library defines the entry and exit points for UHCI USB Host Controller
Driver. The file initializes the USB host controller Driver. It also
exposed routines to initializes the UHCI Controllers.

usbHcdUhciDeviceInit () routine implements the legacy support. It handles the
hand-off of USB UHCI Controllers from BIOS to system software.

usbHcdUhciDeviceConnect () routine initializes the USB UHCI Host Controller and
makes it operational to handle USB operations.

The implementation of this library follows the UHCI Specification Rev 1.1

INCLUDE FILES:  usb2/usbOsal.h, usb2/usbHst.h, usb2/usbUhci.h, usb2/usbUhcdSupport.h,
usb2/usbUhcdCommon.h, usb2/usbUhcdScheduleQueue.h, usb2/usbUhcdScheduleQSupport.h,
rebootLib.h

*/


/*
INTERNAL
 *******************************************************************************
 * Filename         : usbUhcdInitialization.c
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      :  This file contains entry and exit points for the
 *                     UHCI driver.
 *
 *
 ******************************************************************************/

/* includes */
#include <usb/usbOsal.h>
#include <usb/usbHst.h>
#include <hwif/vxbus/vxBus.h>
#include <hwif/vxbus/vxbPciLib.h>
#include <hwif/vxbus/hwConf.h>
#include <vxbTimerLib.h>
#include "usbUhci.h"
#include "usbUhcdSupport.h"
#include "usbUhcdCommon.h"
#include "usbUhcdScheduleQueue.h"
#include "usbUhcdScheduleQSupport.h"
#include <rebootLib.h>
#include <spinLockLib.h>
#include <usb/ossLib.h>
#include <hookLib.h>

/* defines */

#define USB_UHCI_BASEC          0x000C0000      /* BASEC value for serial bus */
#define USB_UHCI_SCC            0x00000300      /* SCC value for USB */
#define USB_UHCI_PI             0x00            /* no specific pgm i/f defined */
#define PCI_UHCI_LEGREG_OFFSET  0xC0            /* UHCI Legacy register offset */
#define PCI_UHCI_LEGREG_VALUE   0x2000          /* UHCI Legacy register value */
#define UHCI_CMD_CF_RS_MASK     0xBE            /* USB command register mask */
#define PCI_UHCI_LEGACY_MASK    0x00BF          /* Legacy support register bits mask */
#define USB_INT_STS_REG_MASK    0xFF            /* Interrupt status register bits mask */

#define USB_UHCI_PLB_NAME "vxbPlbUsbUhci"  /*Name to register to PLB bus*/
#define USB_UHCI_PCI_NAME "vxbPciUsbUhci"  /*Name to register to PCI bus*/
#define USB_UHCI_HUB_NAME "vxbUsbUhciHub"  /*Name to register to HUB bus*/

/* global */

/*
 * To hold the array of pointers to the
 * HCD maintained data structures
 */

pUSB_UHCD_DATA *        g_pUHCDData = NULL;

void usbUhcdIsr (INT32 pHCDData);
void usbUhcdCheckPort (INT32 pHCDData);
void usbUhcdManagePort (INT32 pHCDData);
void usbUhcdCompletedTdHandler (pUSB_UHCD_DATA  pHCDData);
BOOLEAN usbUhcdCreateFrameList (pUSB_UHCD_DATA  pHCDData);

/* function to register the UHCI driver with the vxBus */

void vxbUsbUhciRegister (void);

/* function to register the UHCI driver with the vxBus */

STATUS vxbUsbUhciDeregister (void);

/* extern */

IMPORT void usbUhcdHcReset(pUSB_UHCD_DATA pHCDData);

IMPORT VOID usbVxbRootHubAdd (VXB_DEVICE_ID pDev);      /* function to add */
                                                        /* root hub */

IMPORT STATUS usbVxbRootHubRemove (VXB_DEVICE_ID pDev); /* function to remove */
                                                        /* root hub */

int usbUhcdDisableHC (int startType);

/* locals */

LOCAL UINT32 hHcDriver; /* handle to driver */

LOCAL STATUS usbHcdUhciDeviceRemove (VXB_DEVICE_ID pDev); /* function to remove
                                                           * UHCI Device
                                                           */

LOCAL VOID usbHcdUhciDeviceInit (VXB_DEVICE_ID pDev);   /* function to
                                                         * handle legacy the
                                                         * support
                                                         */

LOCAL VOID usbHcdUhciDeviceConnect (VXB_DEVICE_ID pDev);/* function to
                                                         * intialize the
                                                         * UHCI device
                                                         */

LOCAL BOOL usbVxbHcdUhciDeviceProbe   (VXB_DEVICE_ID pDev);

LOCAL VOID usbVxbUhciNullFunction (VXB_DEVICE_ID pDev);

/* Flag indicating that the Host Controller is initialised */

LOCAL BOOLEAN           alreadyInited = FALSE;

/* Number of host controllers present in the system */

UINT32            g_UhcdHostControllerCount = 0;

/* Event used for synchronising the access of the free lists */

LOCAL OS_EVENT_ID       g_UhcdEvent = NULL;

LOCAL PCI_DEVVEND       usbVxbPcidevVendId[1] =
    {
        {
        0xffff,             /* device ID */
        0xffff              /* vendor ID */
        }
    };

LOCAL DRIVER_METHOD     usbVxbHcdUhciHCMethods[2] =
    {
    DEVMETHOD(vxbDrvUnlink, usbHcdUhciDeviceRemove),
    DEVMETHOD_END
    };

LOCAL struct drvBusFuncs usbVxbHcdUhciDriverFuncs =
    {
    usbVxbUhciNullFunction,         /* init 1 */
    usbHcdUhciDeviceInit,           /* init 2 */
    usbHcdUhciDeviceConnect         /* device connect */
    };


/* initialization structure for UHCI Root Hub */

LOCAL struct drvBusFuncs usbVxbRootHubDriverFuncs =
    {
    usbVxbUhciNullFunction,         /* init 1 */
    usbVxbUhciNullFunction,         /* init 2 */
    usbVxbRootHubAdd                /* device connect */
    };

/* method structure for UHCI Root Hub */

LOCAL struct vxbDeviceMethod    usbVxbRootHubMethods[2] =
    {
    DEVMETHOD(vxbDrvUnlink, usbVxbRootHubRemove),
    DEVMETHOD_END
    };


/* DRIVER_REGISTRATION for UHCI Root hub */

LOCAL DRIVER_REGISTRATION       usbVxbHcdUhciHub =
    {
    NULL,                           /* pNext */
    VXB_DEVID_BUSCTRL,              /* hub driver is bus
                                     * controller
                                     */
    VXB_BUSID_USB_HOST_UHCI,        /* parent bus ID */
    USB_VXB_VERSIONID,              /* version */
    USB_UHCI_HUB_NAME,              /* driver name */
    &usbVxbRootHubDriverFuncs,      /* struct drvBusFuncs * */
    &usbVxbRootHubMethods[0],       /* struct vxbDeviceMethod */
    NULL,                           /* probe routine */
    NULL                            /* vxbParams */
    };

LOCAL DRIVER_REGISTRATION       usbVxbPlbHcdUhciDevRegistration =
    {

    NULL,                           /* register next driver */

    VXB_DEVID_BUSCTRL,              /* bus controller */
    VXB_BUSID_PLB,                  /* bus id - PCI Bus Type */
    USB_VXB_VERSIONID,              /* vxBus version Id */
    USB_UHCI_PLB_NAME,              /* drv name */
    &usbVxbHcdUhciDriverFuncs,      /* pDrvBusFuncs */
    &usbVxbHcdUhciHCMethods[0],     /* pMethods */
    NULL,                           /* probe routine */
    NULL                            /* vxbParams */
    };

LOCAL PCI_DRIVER_REGISTRATION   usbVxbPciHcdUhciDevRegistration =
    {
        {

        NULL,                       /* register next driver */

        VXB_DEVID_BUSCTRL,          /* bus controller */
        VXB_BUSID_PCI,              /* bus id - PCI Bus Type */
        USB_VXB_VERSIONID,          /* vxBus version Id */
        USB_UHCI_PCI_NAME,          /* drv name */
        &usbVxbHcdUhciDriverFuncs,  /* pDrvBusFuncs */
        &usbVxbHcdUhciHCMethods[0], /* pMethods */
        usbVxbHcdUhciDeviceProbe,   /* probe routine */
        NULL                        /* vxbParams */
        },
    NELEMENTS(usbVxbPcidevVendId),  /* idListLen */
    &usbVxbPcidevVendId [0]         /* idList */
    };


void usbUhcdInstantiate (void);
STATUS usbUhcdInit (void);

/*******************************************************************************
*
* usbUhcdInstantiate - instantiate the USB UHCI Host Controller Driver.
*
* This routine instantiates the UHCI Host Controller Driver and allows
* the UHCI Controller driver to be included with the vxWorks image and
* not be registered with vxBus.  UHCI devices will remain orphan devices
* until the usbUhcdInit() routine is called.  This supports the
* INCLUDE_UHCI behaviour of previous vxWorks releases.
*
* The routine itself does nothing.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbUhcdInstantiate (void)
    {
    return;
    }

/*******************************************************************************
*
* usbUhcdInit - initialise the USB UHCI Host Controller Driver.
*
* This routine initializes internal data structues in the UHCI Host Controller
* Driver. This routine is typically called prior to the vxBus invocation of
* the device connect.
*
* This routine registers the UHCI HCD with the USBD Layer.
*
* RETURNS: OK or
*          ERROR - if the UHCD Host Controller initialization fails
*
* ERRNO: N/A
*/

STATUS usbUhcdInit
    (
    void
    )
    {
    USBHST_HC_DRIVER    hcDriver;           /* structure for registering driver */

    UINT8               uCount = 0;         /* counter */

    /* Check if this function has been already called before */

    if (alreadyInited == TRUE)
        {
        USB_UHCD_WARN("usbUhcdInit() has already been called \n",
                      1, 2, 3, 4, 5, 6);

        /* WindView Instrumentation */

        USB_HCD_LOG_EVENT(
            USB_UHCI_WV_INIT_EXIT,
            "usbUhcdInit() exits : Error - already called once",
            USB_UHCD_WV_FILTER);

        return OK;
        }

     /* Allocate memory for the global */

    g_pUHCDData = (pUSB_UHCD_DATA *)OS_MALLOC(USB_MAX_UHCI_COUNT *
                                          sizeof(pUSB_UHCD_DATA));

    /* Check if memory allocation is successful */

    if (g_pUHCDData == NULL)
        {
        USB_UHCD_ERR("usbUhcdInit() memory allocate failed for UHCD data\n",
                     1, 2, 3, 4, 5, 6);

        /* WindView Instrumentation */

        USB_HCD_LOG_EVENT(
            USB_UHCI_WV_INIT_EXIT,
            "usbUhciInit() exits : Memory allocation failure",
            USB_UHCD_WV_FILTER);

        return ERROR;
        }

     /* Hook the function that should be called on reboot */

     if (ERROR == rebootHookAdd(usbUhcdDisableHC))
        {
        USB_UHCD_ERR("usbUhciInit(): failed to hook the reboot routine \n",
                     1, 2, 3, 4, 5, 6);

        /* Release memory allocated for the global HCD data array */

        OS_FREE(g_pUHCDData);

        g_pUHCDData = NULL;

        return ERROR;
        }

    /* Initialize the global array */

    for (uCount = 0; uCount < USB_MAX_UHCI_COUNT; uCount++)
        {
        g_pUHCDData[uCount] = NULL;
        }

    /* register the UHCD with USBD */

    /* Initialize the members of the data structure */

    OS_MEMSET (&hcDriver, 0, sizeof (USBHST_HC_DRIVER));

    /* Initialize members of structure */

    hcDriver.getFrameNumber = usbUhcdGetFrameNumber;
    hcDriver.setBitRate = usbUhcdSetBitRate;
    hcDriver.isBandwidthAvailable = usbUhcdIsBandwidthAvailable;
    hcDriver.pipeControl = usbUhcdPipeControl;
    hcDriver.createPipe = usbUhcdCreatePipe;
    hcDriver.modifyDefaultPipe = usbUhcdModifyDefaultPipe;
    hcDriver.deletePipe = usbUhcdDeletePipe;
    hcDriver.isRequestPending = usbUhcdIsRequestPending;
    hcDriver.submitURB = usbUhcdSubmitUrb;
    hcDriver.cancelURB = usbUhcdCancelUrb;

    /*
     * Register the HCD with the USBD. We also pass the bus id in this function
     * This is to register UHCI driver with vxBus as a bus type.
     *
     * After the registration is done we get a handle "hHcDriver". This
     * handle is used for all subsequent communication of UHCI driver with
     * USBD.
     *
     * In this function we also pass the vxBus Bus ID for the bus controler
     * driver.
     */

    if (USBHST_SUCCESS != usbHstHCDRegister (&hcDriver, &hHcDriver, NULL,
                                             VXB_BUSID_USB_HOST_UHCI))
        {
        USB_UHCD_ERR("usbUhciInit(): failed to register UHCD with USBD \n",
                     1, 2, 3, 4, 5, 6);

        /* WindView Instrumentation */

        USB_HCD_LOG_EVENT(
            USB_UHCI_WV_INIT_EXIT,
            "usbUhciInit() exits - UHCD registration failed",
            USB_UHCD_WV_FILTER);

        /* Release memory allocated for the global HCD data array */

        OS_FREE(g_pUHCDData);

        g_pUHCDData = NULL;

        if(OK != rebootHookDelete(usbUhcdDisableHC))
            {
            USB_UHCD_ERR("RebootHookDelete failed.\n", 1, 2, 3, 4, 5, 6);
            }

        return ERROR;
        }

    /* Create global event g_UhcdEvent for global variables access */

    g_UhcdEvent = OS_CREATE_EVENT(OS_EVENT_SIGNALED);

    if (g_UhcdEvent == NULL)
        {
        USB_UHCD_ERR("usbUhciInit - failed to create UHCD global event \n",
                     1, 2, 3, 4, 5, 6);

        /* de-register with USBD */

        if (USBHST_SUCCESS != usbHstHCDDeregister(hHcDriver))
           {
           USB_UHCD_ERR("usbHstHCDDeregister fail\n", 1, 2, 3, 4, 5, 6);
           }

        /* Release memory allocated for the global HCD data array */

        OS_FREE(g_pUHCDData);

        g_pUHCDData = NULL;

        if (OK != rebootHookDelete(usbUhcdDisableHC))
            {
            USB_UHCD_ERR("RebootHookDelete failed.\n", 1, 2, 3, 4, 5, 6);
            }

        return ERROR;
        }

    /* reset the global counter */

    g_UhcdHostControllerCount = 0;

    alreadyInited = TRUE;

    return OK;
    }

/*******************************************************************************
*
* usbHcdUhciDeviceInit - legacy support implementation
*
* This function implements the legacy support for UHCI Controller. This
* function handles the handoff of USB UHCI Controller from BIOS to System
* Software. In this function, RS and CF bits of the USB COMMAND REGISTER are
* cleared to stop host controller and to signal BIOS that OS has control. All
* pending interrupts are cleared by writing 0xFF to the Interrupt Status
* Register. If any bits in the lower byte is set (SMI Support),
* a value of 0x2000 is written to the legacy support register to clear SMI
* enable bit (bit4) and to route USB interrupt to PIRQD (bit13).
*
* RETURN : N/A
*
* ERRNO: none
*/

LOCAL VOID usbHcdUhciDeviceInit
    (
    struct vxbDev     * pDev                    /* struct vxbDev * */
    )
    {
    INT32               i;
    pUSB_UHCD_DATA          pHCDData = NULL;

    /* validate parameters */

    if (pDev == NULL)
        {
        USB_UHCD_ERR("usbHcdUhciDeviceInit(): invalid parameter for pDev \n",
                     1, 2, 3, 4, 5, 6);
        return;
        }

    /*
     * Allocate the pDrvCtrl structure.  This is done here now so we can
     * get the register access routines earlier than we normally do.
     *
     * We partially initializae pHCDData here and finish the job in
     * the usbHcdUhciDeviceConnect() routine.
     *
     * Allocate memory for the USB_UHCD_DATA structure
     */

    pHCDData = (pUSB_UHCD_DATA)OS_MALLOC(sizeof(USB_UHCD_DATA));

    /* Check if memory allocation is successful */

    if (NULL == pHCDData)
        {
        USB_UHCD_ERR("usbHcdUhciDeviceInit():"
                     "memory allocate failed for UHCD data \n",
                     1, 2, 3, 4, 5, 6);

        return;
        }

    /* Initialize all the elements of the structure */

    OS_MEMSET(pHCDData, 0, sizeof(USB_UHCD_DATA));

    pDev->pDrvCtrl = (pVOID)pHCDData;

    /*
     * Map the access registers.  This is needed for subsequent
     * operations. We look at the pDev->regBaseFlags which are set when the
     * pci bus device is announced to vxBus.  The device announce, based on
     * the BAR will set VXB_REG_IO, VXB_REG_MEM, VXB_REG_NONE or VXB_REG_SPEC.
     *
     * The following test identifies the BARs to use.
     */

    for (i = 0; i < VXB_MAXBARS; i++)
        {
        if (pDev->regBaseFlags[i] != VXB_REG_NONE)
            {
            if(ERROR == vxbRegMap (pDev, i, &pHCDData->pRegAccessHandle))
                {
                USB_UHCD_ERR("usbHcdUhciDeviceInit():"
                             "vxbRegMap fail\n",
                             1, 2, 3, 4, 5, 6);
                return;
                }

            /* Save the register base just to keep things a little simpler */

            pHCDData->regBase = (ULONG)pDev->pRegBase[i];

            /* Record the register index */

            pHCDData->regIndex = i;

            break ;
            }
        }

    if (i >= VXB_MAXBARS)
        {
        /*
         * No usable BARs found.
         */

        pDev->pDrvCtrl = NULL;

        /*
         * With a pDrvCtrl of null, the device Connect routine will
         * signal an error.
         */

        OS_FREE(pHCDData);

        USB_UHCD_ERR("usbHcdUhciDeviceInit(): no usable BARs for UHCI register "
                     "access mapping \n",
                     1, 2, 3, 4, 5, 6);

        return;
        }

    return;
    }

LOCAL BOOLEAN usbHcdUhciDataUnInitialize
    (
    pUSB_UHCD_DATA          pHCDData
    )
    {
    /* Destroy the DMA TAGs */

    if (pHCDData->frameListDmaTagId)
        {
        (void) vxbDmaBufTagDestroy(pHCDData->frameListDmaTagId);

        pHCDData->frameListDmaTagId = NULL;
        }

    if (pHCDData->qhDmaTagId)
        {
        (void) vxbDmaBufTagDestroy(pHCDData->qhDmaTagId);

        pHCDData->qhDmaTagId = NULL;
        }

    if (pHCDData->tdDmaTagId)
        {
        (void) vxbDmaBufTagDestroy(pHCDData->tdDmaTagId);

        pHCDData->tdDmaTagId = NULL;
        }

    return TRUE;
    }

LOCAL BOOLEAN usbHcdUhciDataInitialize
    (
    struct vxbDev *     pDev,
    pUSB_UHCD_DATA          pHCDData
    )
    {
    USB_UHCD_DBG(
        "USB_UHCD_MAX_QH_SIZE %d\n"
        "USB_UHCD_MAX_TD_SIZE %d\n",
        USB_UHCD_MAX_QH_SIZE,
        USB_UHCD_MAX_TD_SIZE,
        0, 0, 0, 0);

    /* UHCI can only address 32bit address space range */

    pHCDData->usrDmaTagLowAddr = VXB_SPACE_MAXADDR_32BIT;

    /* We need to watch out for user buffer non-aligned on cache-line */

    pHCDData->usrDmaMapFlags = VXB_DMABUF_CACHEALIGNCHECK;

    /* QH and TD need to be aligned properly */

    pHCDData->descAlignment = USB_UHCD_HC_DATA_STRUCTURE_ALIGNMENT;

    /* Get a reference to our parent tag */

    pHCDData->uhciParentTag = vxbDmaBufTagParentGet (pDev, 0);

    /* Create tag for mapping QH */

    pHCDData->qhDmaTagId = vxbDmaBufTagCreate (pDev,
        pHCDData->uhciParentTag,        /* parent */
        pHCDData->descAlignment,        /* alignment */
        USB_UHCD_DESC_BOUNDARY_LIMIT,   /* boundary */
        VXB_SPACE_MAXADDR_32BIT,        /* lowaddr */
        VXB_SPACE_MAXADDR,              /* highaddr */
        NULL,                           /* filter */
        NULL,                           /* filterarg */
        USB_UHCD_MAX_QH_SIZE,           /* max size */
        1,                              /* nSegments */
        USB_UHCD_MAX_QH_SIZE,           /* max seg size */
        VXB_DMABUF_NOCACHE,             /* flags */
        NULL,                           /* lockfunc */
        NULL,                           /* lockarg */
        NULL);                          /* ppDmaTag */

    if (pHCDData->qhDmaTagId == NULL)
        {
        USB_UHCD_ERR(
            "usbHcdUhciDataInitialize() - creating qhDmaTagId fail\n",
            0, 0, 0, 0, 0, 0);

        usbHcdUhciDataUnInitialize(pHCDData);

        return FALSE;
        }

    /* Create tag for mapping TD */

    pHCDData->tdDmaTagId = vxbDmaBufTagCreate (pDev,
        pHCDData->uhciParentTag,        /* parent */
        pHCDData->descAlignment,        /* alignment */
        USB_UHCD_DESC_BOUNDARY_LIMIT,   /* boundary */
        VXB_SPACE_MAXADDR_32BIT,        /* lowaddr */
        VXB_SPACE_MAXADDR,              /* highaddr */
        NULL,                           /* filter */
        NULL,                           /* filterarg */
        USB_UHCD_MAX_TD_SIZE,           /* max size */
        1,                              /* nSegments */
        USB_UHCD_MAX_TD_SIZE,           /* max seg size */
        VXB_DMABUF_NOCACHE,             /* flags */
        NULL,                           /* lockfunc */
        NULL,                           /* lockarg */
        NULL);                          /* ppDmaTag */

    if (pHCDData->tdDmaTagId == NULL)
        {
        USB_UHCD_ERR(
            "usbHcdUhciDataInitialize() - creating qtdDmaTagId fail\n",
            0, 0, 0, 0, 0, 0);

        usbHcdUhciDataUnInitialize(pHCDData);

        return FALSE;
        }

    /* Create tag for mapping periodic frame list */

    pHCDData->frameListDmaTagId = vxbDmaBufTagCreate (pDev,
        pHCDData->uhciParentTag,        /* parent */
        USB_UHCD_FRAMELIST_ALIGNMENT,   /* alignment */
        USB_UHCD_DESC_BOUNDARY_LIMIT,   /* boundary */
        VXB_SPACE_MAXADDR_32BIT,        /* lowaddr */
        VXB_SPACE_MAXADDR,              /* highaddr */
        NULL,                           /* filter */
        NULL,                           /* filterarg */
        USB_UHCD_MAX_FRAMELIST_SIZE,    /* max size */
        1,                              /* nSegments */
        USB_UHCD_MAX_FRAMELIST_SIZE,    /* max seg size */
        VXB_DMABUF_NOCACHE,             /* flags */
        NULL,                           /* lockfunc */
        NULL,                           /* lockarg */
        NULL);                          /* ppDmaTag */

    if (pHCDData->frameListDmaTagId == NULL)
        {
        USB_UHCD_ERR(
            "usbHcdUhciDataInitialize() - creating frameListDmaTagId fail\n",
            0, 0, 0, 0, 0, 0);

        usbHcdUhciDataUnInitialize(pHCDData);

        return FALSE;
        }

    return TRUE;
    }

/*******************************************************************************
*
* usbHcdUhciDeviceConnect - initialize the UHCI Host Controller Device
*
* This routine intializes the UHCI host controller device and activates it to
* handle all USB operations. The routine is called by vxBus on a successful
* driver - device match with VXB_DEVICE_ID as parameter. This structure has all
* information about the device. This routine resets all UHCI registers and
* then initializes them with default value.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL VOID usbHcdUhciDeviceConnect
    (
    struct vxbDev *     pDev                    /* struct vxbDev * */
    )
    {
    pUSB_UHCD_DATA      pHCDData = NULL;
    STATUS              busStatus = ERROR;      /* error flag */
    UINT16              regx = 0;               /* for register access */
    USBHST_STATUS       status = USBHST_FAILURE;
    HCF_DEVICE *        pHcf = NULL;
    char                taskName[20];
    UINT8               i;
    UINT8               index;

    /* Validate the paramters */

    if ((pDev == NULL) || (pDev->pDrvCtrl == NULL))
        {
        USB_UHCD_DBG("usbHcdUhciDeviceConnect - "
            "pDev %p or pDev->pDrvCtrl %p NULL\n",
            pDev, pDev ? pDev->pDrvCtrl : 0, 0, 0, 0, 0);

        return;
        }

    pHCDData = (pUSB_UHCD_DATA) pDev->pDrvCtrl;

    /* Check g_pUHCDData is available */
    for (index = 0; index < USB_MAX_UHCI_COUNT; index++)
        {
        if(g_pUHCDData[index] == NULL)
            break;
        }

    /* We allow only fixed number of UHCI host controllers */

    if (g_UhcdHostControllerCount >= USB_MAX_UHCI_COUNT ||
        index >= USB_MAX_UHCI_COUNT )
        {
        USB_UHCD_ERR("usbHcdUhciDeviceConnect - reach MAX (%d) UHCI\n",
                     USB_MAX_UHCI_COUNT, 2, 3, 4, 5, 6);

        /* Unmap the register space */

        vxbRegUnmap (pDev, pHCDData->regIndex);

        /*
         * With a pDrvCtrl of null, the device Connect routine will
         * signal an error.
         */

        OS_FREE(pHCDData);

        /*
         * No usable BARs found.
         */

        pDev->pDrvCtrl = NULL;

        return;
        }

    OS_WAIT_FOR_EVENT (g_UhcdEvent, WAIT_FOREVER);

    /* Initialize the spinlock for this instance */

    SPIN_LOCK_ISR_INIT(&(pHCDData->spinLockIsr), 0);

    /* Update the unit number with g_UHCDControllerCount */

    /* If this UHCI device is a PCI device, unitNumber shall be assigned. */
    if (0 == strncmp (pDev->pName, USB_UHCI_PCI_NAME, sizeof(USB_UHCI_PCI_NAME)))
        {
        vxbNextUnitGet(pDev);
        }

    /* Update the Bus Index in the Stucture */

    pHCDData->uBusIndex = index;

    /* Populate the pUSB_UHCD_DATA :: pDev */

    pHCDData->pDev = pDev;

    /* Store in global array */

    g_pUHCDData[index] = pHCDData;

    g_UhcdHostControllerCount++;

    /* Populate the required function pointers */

    switch (pDev->busID)
        {
        case VXB_BUSID_PLB:

            /*
             * The underlying bus type is PLB. Query the hwconf file for
             * cpu to physical memory converison functions and populate the
             * required function pointers
             */

            /* get the HCF device from the instance id */

            pHcf = hcfDeviceGet (pDev);

            /*
             * populate the function pointers for Cpu to Physical Memory and
             * vice versa conversions
             */

            if (NULL == pHcf || OK != devResourceGet (pHcf, "dataSwap", 
                HCF_RES_ADDR, (void *)&pHCDData->pDataSwap))
                {
                USB_UHCD_WARN("devResourceGet dataSwap fail\n", 1, 2, 3, 4, 5, 6);
                }

            /* update the vxBus read-write handle */

            if (pHCDData->pDataSwap != NULL)
                pHCDData->pRegAccessHandle = (void *)
                VXB_HANDLE_SWAP ((ULONG)(pHCDData->pRegAccessHandle));

            break;

        case VXB_BUSID_PCI:

            /*
             * populate the function pointer for byte conversion. Byte swapping
             * is required only if bus type is PCI and architecture is BE.
             * NOTE: for PLB no byte swapping is required
             */

#if (_BYTE_ORDER == _BIG_ENDIAN)
             pHCDData->pDataSwap = vxbSwap32;
#endif
            break;

        default:

            /*
             * Never try to free pHCDData here, because
             * UHCI_ERROR_HANDLER will refer to it
             */

            /*
             * The g_UhcdEvent should be given only after the
             * pHCDData access is done;
             */

            goto UHCI_ERROR_HANDLER;
        }

    /* Create the vxbDmaBufLib related data elements */

    if (usbHcdUhciDataInitialize(pDev, pHCDData) != TRUE)
        {
        USB_UHCD_ERR("usbHcdUhciDeviceConnect - "
            "usbHcdUhciDataInitialize fail\n",
            1, 2, 3, 4, 5, 6);
        goto UHCI_ERROR_HANDLER;
        }

    /*
     * Register R/W should start after the pRegAccessHandle is updated.
     * So move the code below from usbHcdUhciDeviceInit here.
     */

    /*
     * Disable PIRQ and SMI and RWC bits to kick off from BIOS
     */

    regx = 0x8F00;

    VXB_PCI_BUS_CFG_WRITE (pDev,
                           PCI_UHCI_LEGREG_OFFSET,
                           2,
                           regx);

    /* Reset the host controller */

    USB_UHCD_WRITE_WORD (pDev, USB_UHCD_USBCMD, (1 << USB_UHCD_USBCMD_HCRESET));

    /* Wait about 10ms as per UHCI spec */

    for (i = 0; i < 10; i++)
        {
        vxbDelay();
        }

    /* Set USBCMD and USBINTR to 0 to stop it, no interrupts and DMA */

    USB_UHCD_WRITE_WORD (pDev, USB_UHCD_USBCMD, 0);

    USB_UHCD_WRITE_WORD (pDev, USB_UHCD_USBINTR, 0);

    /*
     * Wait until host controller halts (total time elapsed is less than
     * one millisecond) vxbDelay waits one millisecond in a busy loop
     */

    vxbDelay();

    /*
     * Write 0xFF to Interrupt Status Register to clear all pending
     * interrupts
     */

    USB_UHCD_WRITE_WORD (pDev, USB_UHCD_USBSTS, USB_INT_STS_REG_MASK);

    USB_UHCD_WRITE_WORD (pDev, USB_UHCD_PORT1, 0);

    USB_UHCD_WRITE_WORD (pDev, USB_UHCD_PORT2, 0);

    /*
     * We force to kick off from BIOS, no matter what
     * the previous state it was. So, there is no need
     * to read the UHCI Legacy Support Register here.
     */

    /*
     * The PCI Legacy Support register is located at the offset
     * PCI_UHCI_LEGREG_OFFSET (0xC0 - 0xC1) in the PCI Configuration Space.
     * The USBSMIEN bit of UHCI Legacy Support register should be set to 0
     * and the USBPIRQDEN of UHCI Legacy Support register should be set to 1
     * The UHCI Legacy Support Register is a 16-bit register, so the
     * transaction feild of vxbPciDevCfgWrite should be set to 2.
     */

    /* Write 0x2000 to the legacy support register */

    regx = PCI_UHCI_LEGREG_VALUE;

    VXB_PCI_BUS_CFG_WRITE (pDev,
                           PCI_UHCI_LEGREG_OFFSET,
                           2,
                           regx);

    /* Clear the save port status for port 1 */

    pHCDData->savedPortStatus[0] = 0;

    /* Clear the save port status for port 2 */

    pHCDData->savedPortStatus[1] = 0;

    /* Clear the forceReconnect for port 1 */

    pHCDData->forceReconnect[0] = 0;

    /* Clear the forceReconnect for port 2 */

    pHCDData->forceReconnect[1] = 0;

    /*
     * Create the mutex to protect bandwidth updates ;
     */

    pHCDData->BandwidthMutexEvent = semMCreate(SEM_Q_PRIORITY |
                                              SEM_INVERSION_SAFE |
                                              SEM_DELETE_SAFE);
    if (pHCDData->BandwidthMutexEvent == NULL)
        {
        USB_UHCD_ERR("usbHcdUhciDeviceConnect - "
                     "unable to create BandwidthMutexEvent mutex \n",
                     1, 2, 3, 4, 5, 6);

        /* WindView Instrumentation */

        USB_HCD_LOG_EVENT(
            USB_UHCI_WV_INIT_EXIT,
            "usbHcdUhciDeviceConnect() exits - "
            "Creating BandwidthMutexEvent mutex failed",
            USB_UHCD_WV_FILTER);

        goto UHCI_ERROR_HANDLER;
        }

    /* Create the event used for synchronisation of the reclamation list */

    pHCDData->ReclamationListSynchEventID = OS_CREATE_EVENT(OS_EVENT_SIGNALED);

    /* Check if the event is created successfully */

    if (NULL == pHCDData->ReclamationListSynchEventID)
        {
        USB_UHCD_ERR(
            "usbHcdUhciDeviceConnect - reclamation synch event is not created\n",
            0, 0, 0, 0, 0, 0);

        goto UHCI_ERROR_HANDLER;
        }

    /* Create the event used for synchronisation of the active pipe list */

    pHCDData->ActivePipeListSynchEventID =
        semMCreate (SEM_Q_PRIORITY|SEM_INVERSION_SAFE|SEM_DELETE_SAFE);

    /* Check if the event is created successfully */

    if (NULL == pHCDData->ActivePipeListSynchEventID)
        {
        USB_UHCD_ERR(
            "usbHcdUhciDeviceConnect - active pipe synch event is not created\n",
            0, 0, 0, 0, 0, 0);

        goto UHCI_ERROR_HANDLER;
        }

    /*
     * The UHCI controller requires a frame list which is accessed
     * by the Host Controller in every frame to service the requests
     * meant for the frame.
     * Call the function to create the frame list required by the
     * Host Controller.
     */

    if (!usbUhcdCreateFrameList(pHCDData))
        {
        USB_UHCD_ERR("usbHcdUhciDeviceConnect - "
                     "unable to create frame list \n",
                     1, 2, 3, 4, 5, 6);

        /* WindView Instrumentation */

        USB_HCD_LOG_EVENT(
            USB_UHCI_WV_INIT_EXIT,
            "usbHcdUhciDeviceConnect() exits - Creating frame list failed",
            USB_UHCD_WV_FILTER);

        goto UHCI_ERROR_HANDLER;
        }

    /* create the default pipe */

    pHCDData->pDefaultPipe = usbUhcdFormEmptyPipe();

    if (pHCDData->pDefaultPipe ==  NULL)
        {
        USB_UHCD_ERR("usbHcdUhciDeviceConnect - unable to create default pipe\n",
                     1, 2, 3, 4, 5, 6);

        /* WindView Instrumentation */

        USB_HCD_LOG_EVENT(
                USB_UHCI_WV_INIT_EXIT,
                "usbHcdUhciDeviceConnect() exits - Creating default pipe failed",
                USB_UHCD_WV_FILTER);

       /*
        * The g_UhcdEvent should be given only after the
        * pHCDData access is done;
        */

        goto UHCI_ERROR_HANDLER;
        }

    /* Link the default pipe the the Control Queue Head */

    pHCDData->pDefaultPipe->pQH = pHCDData->pControlQH;
    pHCDData->pDefaultPipe->uEndpointType = USBHST_CONTROL_TRANSFER;
    pHCDData->pDefaultPipe->pNext = NULL;
    pHCDData->pDefaultPipe->pQH->pPipe = pHCDData->pDefaultPipe;

    /* Create the event used for synchronization of requests for this pipe */

    pHCDData->pDefaultPipe->PipeSynchEventID =
        OS_CREATE_EVENT(OS_EVENT_SIGNALED);

    /* Check if the event is created successfully */

    if (NULL == pHCDData->pDefaultPipe->PipeSynchEventID)
        {
        USB_UHCD_ERR(
            "usbHcdUhciDeviceConnect - PipeSynchEventID not created\n",
            0, 0, 0, 0, 0, 0);

        goto UHCI_ERROR_HANDLER;
        }

    /* Setup the default pipe */

    if (usbUhcdSetupControlPipe(pHCDData, pHCDData->pDefaultPipe) !=
        USBHST_SUCCESS)
        {
        USB_UHCD_ERR("usbUhcdCreatePipe - usbUhcdSetupControlPipe fail\n",
            0, 0, 0, 0, 0, 0);

        goto UHCI_ERROR_HANDLER;
        }

    /* Add the default pipe to the non-isoch pipe list */

    pHCDData->pNonIsochPipeList = pHCDData->pDefaultPipe;

#ifndef  UHCD_POLLING_MODE
    /*
     * Create the semaphore used to cleanup the resources
     * allocated after completion of a transfer request.
     */

    pHCDData->tdCompleteSem = OS_CREATE_EVENT (SEM_EMPTY);

    if (pHCDData->tdCompleteSem == NULL)
        {
        USB_UHCD_ERR("usbHcdUhciDeviceConnect(): unable to create semaphore \n",
                     1, 2, 3, 4, 5, 6);

        /* WindView Instrumentation */

        USB_HCD_LOG_EVENT(
            USB_UHCI_WV_INIT_EXIT,
            "usbHcdUhciDeviceConnect() exits - "
            "Creating semaphore for cleanup failed",
            USB_UHCD_WV_FILTER);

        /*
         * The g_UhcdEvent should be given only after the
         * pHCDData access is done;
         */

        goto UHCI_ERROR_HANDLER;
        }

#endif /* UHCD_POLLING_MODE */

    /* Spawn the thread used for handling transfer completion */

    snprintf(taskName, 20, "uhcdClrTd%d", pHCDData->uBusIndex);

    pHCDData->IntHandlerThreadID = OS_CREATE_THREAD (taskName,
                                          90,
                                          (FUNCPTR) usbUhcdCompletedTdHandler,
                                          (long)pHCDData);

    if (pHCDData->IntHandlerThreadID == OS_THREAD_FAILURE)
        {
        USB_UHCD_ERR("usbHcdUhciDeviceConnect(): unable to create thread %s\n",
                     taskName, 2, 3, 4, 5, 6);

        /* WindView Instrumentation */

        USB_HCD_LOG_EVENT(
            USB_UHCI_WV_INIT_EXIT,
            "usbHcdUhciDeviceConnect() exits - "
            "Creating transfer completion thread failed",
            USB_UHCD_WV_FILTER);

        /*
         * The g_UhcdEvent should be given only after the
         * pHCDData access is done;
         */

        goto UHCI_ERROR_HANDLER;
        }

    /* Write the lower 32 bit of the frame list BUS address */

    /* Write the base address of the frame list in the FLBASEADD register */

    USB_UHCD_DWORD_OUT (pHCDData->pDev, USB_UHCD_FLBASEADD,
        USB_UHCD_BUS_ADDR_LO32(pHCDData->frameListDmaMapId->fragList[0].frag));

    /*
     * Bit 7 of the USBCMD register indicates the Maximum packet size used for
     * full speed bandwidth reclamation at the end of the frame.
     *
     * This value is used by the Host Controller to determine whether it
     * should initiate any transaction based on the time remaining in the frame.
     *
     * A value of 0 indicates a maximum packet size of 32 bytes.
     * This bit set to 1 indicates that the maximum packet size is 64 bytes.
     */

    usbUhcdRegxSetBit (pHCDData, USB_UHCD_USBCMD, USB_UHCD_USBCMD_MAXP64);

    /*
     * Bit 6 of the USBCMD register is provided as a semaphore service
     * for the software to access the Host Controller.
     * This has no effect on the hardware.
     */

    usbUhcdRegxSetBit (pHCDData ,USB_UHCD_USBCMD, USB_UHCD_USBCMD_CF);

    /*
     * Bit 0 of the USBCMD register enables the Host Controller to be
     * operational or non-operational. Setting this bit to 1 makes the
     * Host Controller operational and 0 vice-versa.
     */

    usbUhcdRegxSetBit (pHCDData, USB_UHCD_USBCMD, USB_UHCD_USBCMD_RS);


    /* Clear the Port Enable change bit */

    usbUhcdPortClearBit(pHCDData, USB_UHCD_PORT1, USB_UHCD_PORTSC_PEC);

    /* Clear the Port Enable change bit */

    usbUhcdPortClearBit(pHCDData, USB_UHCD_PORT2, USB_UHCD_PORTSC_PEC);

    /*
     * Write 0x3F (RWC bits) to Interrupt Status Register to
     * clear all pending interrupts
     */

    USB_UHCD_WRITE_WORD (pDev, USB_UHCD_USBSTS, USB_INT_STS_REG_MASK);

    /* Register the Bus with USBD */

    status = usbHstBusRegister(hHcDriver,
                               USBHST_FULL_SPEED,
                               (ULONG)pHCDData->pDefaultPipe,
                               pDev);

    if (status != USBHST_SUCCESS)
        {
        USB_UHCD_ERR("usbHcdUhciDeviceConnect - usbHstBusRegister failed\n",
                     1, 2, 3, 4, 5, 6);

        /* WindView Instrumentation */

        USB_HCD_LOG_EVENT(
            USB_UHCI_WV_INIT_EXIT,
            "usbHcdUhciDeviceConnect() exits - Unable to register with USBD",
            USB_UHCD_WV_FILTER);

       /*
        * The g_UhcdEvent should be given only after the
        * pHCDData access is done;
        */

        goto UHCI_ERROR_HANDLER;
        }

    /*
     * Hook the ISR function of the UHCD Host Controller Driver
     * This enables the ISR function to be called when an interrupt
     * is received for this IRQ.
     */

#ifndef UHCD_POLLING_MODE

    busStatus = vxbIntConnect (pHCDData->pDev, 0, usbUhcdIsr, pHCDData);

    /* Check whether the interrupt handler was registered successfully */

    if (busStatus == ERROR)
        {
        USB_UHCD_ERR("usbHcdUhciDeviceConnect - unable to hook interrupt\n",
                     1, 2, 3, 4, 5, 6);

        /* WindView Instrumentation */

        USB_HCD_LOG_EVENT(
            USB_UHCI_WV_INIT_EXIT,
            "usbHcdUhciDeviceConnect() exits - Unable to hook interrupt",
            USB_UHCD_WV_FILTER);

       /*
        * The g_UhcdEvent should be given only after the
        * pHCDData access is done;
        */

        goto UHCI_ERROR_HANDLER;
        }

    /* enable the interrupts */

    busStatus = vxbIntEnable (pHCDData->pDev, 0, usbUhcdIsr, pHCDData);

    /* Check whether the interrupt handler was registered successfully */

    if (busStatus == ERROR)
        {
        USB_UHCD_ERR("usbHcdUhciDeviceConnect - unable to hook interrupt \n",
                     1, 2, 3, 4, 5, 6);

        /* WindView Instrumentation */

        USB_HCD_LOG_EVENT(
            USB_UHCI_WV_INIT_EXIT,
            "usbHcdUhciDeviceConnect() exits - Unable to hook interrupt",
            USB_UHCD_WV_FILTER);

        /* deregister the bus */

        status = usbHstBusDeregister (hHcDriver,
                                      index,
                                      (ULONG)pHCDData->pDefaultPipe);
       if (status != USBHST_SUCCESS)
           {
           USB_UHCD_ERR("usbHstDeregister fail\n", 1, 2, 3, 4, 5, 6);
           }

       /*
        * The g_UhcdEvent should be given only after the
        * pHCDData access is done;
        */

        /* Disconnect the interrupt line */

        vxbIntDisconnect (pHCDData->pDev, 0, usbUhcdIsr, pHCDData);

        goto UHCI_ERROR_HANDLER;
        }
#endif /* UHCD_POLLING_MODE */

    /* Finally enable all the interrupts */

    regx = USB_UHCD_READ_WORD (pHCDData->pDev, USB_UHCD_USBINTR);

    /* Write the value to the register */

    USB_UHCD_WRITE_WORD (pHCDData->pDev,
                         USB_UHCD_USBINTR,
                         regx | USB_UHCD_USBINTR_ALL);

    /* Set this to allow access to ISR because we are now alive */

    pHCDData->magic = USB_UHCD_MAGIC_ALIVE;

    OS_RELEASE_EVENT (g_UhcdEvent);

    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT(
        USB_UHCI_WV_INIT_EXIT,
        "usbHcdUhciDeviceConnect() exits successfully",
        USB_UHCD_WV_FILTER);

    return;

UHCI_ERROR_HANDLER:

    /* This validity check is not that necessary, but just make sure */

    if (pHCDData != NULL)
        {
        if (pHCDData->IntHandlerThreadID != OS_THREAD_FAILURE
            && pHCDData->IntHandlerThreadID != 0)
            {
            OS_DESTROY_THREAD (pHCDData->IntHandlerThreadID);
            pHCDData->IntHandlerThreadID = OS_THREAD_FAILURE;
            }

        /* delete the frame list */

        if (pHCDData->pFrameList != NULL)
            usbUhcdDeleteFrameList (pHCDData);

        if (pHCDData->tdCompleteSem != NULL)
            {
            OS_DESTROY_EVENT (pHCDData->tdCompleteSem);
            pHCDData->tdCompleteSem = NULL;
            }

        /* Delete the mutex which is used to protect list access */

        if (pHCDData->BandwidthMutexEvent != NULL)
            {
            semTake(pHCDData->BandwidthMutexEvent, WAIT_FOREVER);

            semDelete(pHCDData->BandwidthMutexEvent);

            pHCDData->BandwidthMutexEvent = NULL;
            }

        /* Destrouy the default pipe resources */

        if (pHCDData->pDefaultPipe)
            {
            if (pHCDData->pDefaultPipe->PipeSynchEventID)
                {
                /* Destroy the pipe synch event */

                OS_DESTROY_EVENT(pHCDData->pDefaultPipe->PipeSynchEventID);

                pHCDData->pDefaultPipe->PipeSynchEventID = NULL;
                }

            /* Unsetup the default pipe */

            usbUhcdUnSetupPipe(pHCDData, pHCDData->pDefaultPipe);
            }

        /* Destroy the reclamation list synchronisation event */

        if (NULL != pHCDData->ReclamationListSynchEventID)
            {
            OS_DESTROY_EVENT(pHCDData->ReclamationListSynchEventID);

            pHCDData->ReclamationListSynchEventID = NULL;
            }

        /* Destroy the active pipe list synchronisation event */

        if (NULL != pHCDData->ActivePipeListSynchEventID)
            {
            semDelete(pHCDData->ActivePipeListSynchEventID);

            pHCDData->ActivePipeListSynchEventID = NULL;
            }

        /* Destroy the resources allocated for DMA buffer management */

        usbHcdUhciDataUnInitialize(pHCDData);

        /* Unmap the register space */

        vxbRegUnmap (pDev, pHCDData->regIndex);

        /* reset the struct vxbDev :: pDrvCtrl */

        pDev->pDrvCtrl = NULL;

        /* Reset the in global array */

        g_pUHCDData [index] = NULL ;

        /* decrement the global counter */

        g_UhcdHostControllerCount--;

        /* free the allocated memory */

        OS_FREE (pHCDData);
        }

    /* The pHCDData access is done, release the event taken */

    OS_RELEASE_EVENT (g_UhcdEvent);

    ossStatus (USBHST_FAILURE);

    return;
    }

STATUS usbUhcdExit ( void );

/*******************************************************************************
*
* usbUhcdExit - uninitialize the USB UHCI Host Controller Driver.
*
* This routine uninitializes the USB UHCD Host Controller Driver and detaches
* it from the usbd interface layer.
*
* RETURNS: OK, ERROR if the UHCD Host Controller uninitializaton fails
*
*
* ERRNO:N/A
*/

STATUS usbUhcdExit ( void )
    {

    /* Check whether the UHCI Controller is Initialized or not */

    if (alreadyInited == FALSE)
        {
        USB_UHCD_ERR("usbUhcdExit - host controller not initialized \n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Call the function to deregister with vxBus */

    if (vxbUsbUhciDeregister() == ERROR)
        {
        USB_UHCD_ERR("usbUhcdExit - failed to deregister UHCD with vxBus\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Deregister the HC driver */

    if (USBHST_FAILURE == usbHstHCDDeregister(hHcDriver))
        {
        USB_UHCD_ERR("usbUhcdExit - failed to deregister UHCD with USBD\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Delete the hook function from the table */

    if (ERROR == rebootHookDelete(usbUhcdDisableHC))
        return ERROR;

    /* Set the global handle to 0 */

    hHcDriver = 0;

    /* Reset the global counter */

    g_UhcdHostControllerCount = 0;

    /* Free the global allocated variable */

    OS_FREE (g_pUHCDData);

    g_pUHCDData = NULL;

    OS_DESTROY_EVENT(g_UhcdEvent);

    g_UhcdEvent = NULL;

    /* Set alreadyInited flag to FALSE */

    alreadyInited = FALSE;

    return OK;
    }

/*******************************************************************************
*
* usbHcdUhciDeviceRemove - removes the UHCI Host Controller device
*
* This function un-initializes the USB host controller device. The function
* is registered with vxBus and called when the driver is de-registered with
* vxBus. The function will have VXB_DEVICE_ID as its parameter. This structure
* will consists of all the information about the device. The function will
* subsequently de-register the bus from USBD also.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL STATUS usbHcdUhciDeviceRemove
    (
    struct vxbDev *                 pDev            /* struct vxbDev * */
    )
    {
    /* To hold the pointer to the HCD maintained data structure */

    pUSB_UHCD_DATA                  pHCDData = NULL;

    USBHST_STATUS                   status = USBHST_FAILURE;

    UINT32                          regx;

    UINT8                           busIndex;

    /* Validate the paramters */

    if (pDev == NULL)
        {
        USB_UHCD_ERR("usbHcdUhciDeviceRemove - invalid parameters\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }
    /*
     * pDev->pDrvCtrl is NULL means we don't need to clean the pHCDData
     * we should return OK to indicate the device can be removed
     */

    if (pDev->pDrvCtrl == NULL)
        {
        USB_UHCD_DBG("usbHcdUhciDeviceRemove - pDev->pDrvCtrl is NULL\n",
                     1, 2, 3, 4, 5, 6);
        return OK;
        }

    /* obtain the pointer to USB_UHCD_DATA */

    pHCDData = pDev->pDrvCtrl;

    /* Get bus index */
    busIndex = pHCDData->uBusIndex;
    /*
     * Set this to disallow ISR because we are now dead;
     * This is important because sometims the PCI interrupt
     * is shared, and there maybe buggy vxbIntDisconnect
     * that does not fully discoonect the ISR, so if that
     * happens, we may get invalid pointer access in the ISR.
     */

    pHCDData->magic = USB_UHCD_MAGIC_DEAD;

    /*
     * Bit 0 of the USBCMD register enables the Host Controller to be
     * operational or non-operational. Setting this bit to 0 makes the
     * Host Controller non-operational.
     */

    usbUhcdRegxClearBit (pHCDData, USB_UHCD_USBCMD, USB_UHCD_USBCMD_RS);

    usbUhcdRegxClearBit (pHCDData, USB_UHCD_USBCMD, USB_UHCD_USBCMD_CF);

    /*
     * Disable any interrupts on the IRQ line
     * This is done to hook the ISR
     */

    regx = USB_UHCD_READ_WORD (pHCDData->pDev, USB_UHCD_USBINTR);

    /* Clear bits to Disable interrupts */

    regx &= (~(USB_UHCD_USBINTR_MASK));

    /* Write the value to the register. This will clear the register */

    USB_UHCD_WRITE_WORD (pHCDData->pDev, USB_UHCD_USBINTR, (UINT16)regx);

    /* Deregister the host controller from the bus */

    status = usbHstBusDeregister (hHcDriver, busIndex,
                                  (ULONG)pHCDData->pDefaultPipe);

    if (status != USBHST_SUCCESS)
        {
        USB_UHCD_ERR("usbHcdUhciDeviceRemove - failed to deregister UHCI bus\n",
                     1, 2, 3, 4, 5, 6);
        return ERROR;
        }

#ifndef UHCD_POLLING_MODE
    /* Disable the interrupts */

    (void) vxbIntDisable (pDev, 0, usbUhcdIsr, pHCDData);

    /* Disconnect the interrupt line */

    (void) vxbIntDisconnect (pDev, 0, usbUhcdIsr, pHCDData);
#endif /* UHCD_POLLING_MODE */

    /* Call the function to reset the UHCI Host Controller */

    usbUhcdHcReset (pHCDData);

    if (pHCDData->BandwidthMutexEvent != NULL)
        semTake(pHCDData->BandwidthMutexEvent, WAIT_FOREVER);

    /* Destroy the threads */

    if (pHCDData->IntHandlerThreadID != OS_THREAD_FAILURE
        && pHCDData->IntHandlerThreadID != 0)
        {
        OS_DESTROY_THREAD (pHCDData->IntHandlerThreadID);
        pHCDData->IntHandlerThreadID = OS_THREAD_FAILURE;
        }

    if (pHCDData->tdCompleteSem != NULL)
        {
        OS_DESTROY_EVENT (pHCDData->tdCompleteSem);
        pHCDData->tdCompleteSem = NULL;
        }

    if (pHCDData->BandwidthMutexEvent != NULL)
        {
        OS_DESTROY_EVENT (pHCDData->BandwidthMutexEvent);
        pHCDData->BandwidthMutexEvent = NULL;
        }

    /* Destrouy the default pipe resources */

    if (pHCDData->pDefaultPipe)
        {
        if (pHCDData->pDefaultPipe->PipeSynchEventID)
            {
            /* Destroy the pipe synch event */

            OS_DESTROY_EVENT(pHCDData->pDefaultPipe->PipeSynchEventID);

            pHCDData->pDefaultPipe->PipeSynchEventID = NULL;
            }

        /* Unsetup the default pipe */

        usbUhcdUnSetupPipe(pHCDData, pHCDData->pDefaultPipe);
        }

    /* Destroy the reclamation list synchronisation event */

    if (NULL != pHCDData->ReclamationListSynchEventID)
        {
        OS_DESTROY_EVENT(pHCDData->ReclamationListSynchEventID);

        pHCDData->ReclamationListSynchEventID = NULL;
        }

    /* Destroy the active pipe list synchronisation event */

    if (NULL != pHCDData->ActivePipeListSynchEventID)
        {
        semDelete(pHCDData->ActivePipeListSynchEventID);

        pHCDData->ActivePipeListSynchEventID = NULL;
        }

    /* delete the frame list */

    usbUhcdDeleteFrameList(pHCDData);

    /* Destroy the resources allocated for DMA buffer management */

    usbHcdUhciDataUnInitialize(pHCDData);

    /* Unmap the register space */

    vxbRegUnmap (pDev, pHCDData->regIndex);

    /* remove the driver reference from pDev */

    pDev->pDrvCtrl = NULL;

    OS_FREE(pHCDData);

    OS_WAIT_FOR_EVENT (g_UhcdEvent, WAIT_FOREVER);

    /* remove the referece from the global array */

    g_pUHCDData [busIndex] = NULL;

    /* decrement the global counter */

    g_UhcdHostControllerCount--;

    OS_RELEASE_EVENT (g_UhcdEvent);

    return OK;
    }

/*******************************************************************************
*
* usbUhcdDisableHC - called on a reboot to disable the host controller
*
* This routine is called on a warm reboot to disable the host controller by
* the BSP.
*
* RETURNS: 0, always
*
* ERRNO: N/A
*
* \NOMANUAL
*/

int usbUhcdDisableHC
    (
    int startType
    )
    {
    pUSB_UHCD_DATA  pHCDData;           /* Pointer to the HCD data structure */
    UINT8           index = 0;          /* counter */

    /* Validate the global pointer for HCD Data Structure */

    if (g_pUHCDData == NULL)
        return 0;

    while(index < USB_MAX_UHCI_COUNT)
        {
        pHCDData = g_pUHCDData [index];

        if (pHCDData != NULL)
            {

            /* Reset the host controller */

            usbUhcdHcReset (pHCDData);

#ifndef UHCD_POLLING_MODE

            /* Disable the interrupt */

            (void) vxbIntDisable (pHCDData->pDev, 0,
                                  usbUhcdIsr, (VOID *)pHCDData);

            /* Unregister the ISR */

            (void) vxbIntDisconnect (pHCDData->pDev, 0,
                                     usbUhcdIsr, (VOID *)pHCDData);

#endif /* UHCD_POLLING_MODE */
            }
         index++;
        }
    return 0;
    }

/*******************************************************************************
*
* vxbUsbUhciRegister - register the USB UHCI Host Controller Driver with vxBus
*
* This routine registers the UHCI Host Controller Driver with vxBus and can
* be called from either the target initialization code (bootup) or during
* runtime.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void vxbUsbUhciRegister (void)
    {
    /*
     * Register the UHCI driver for PCI bus as underlying bus.
     */

    if (vxbDevRegister ((DRIVER_REGISTRATION *)
        &usbVxbPciHcdUhciDevRegistration)
        == ERROR)
        {
        USB_UHCD_ERR("vxbUsbUhciRegister - "
            "failed to registering UHCI driver over PCI bus \n",
            1, 2, 3, 4, 5, 6);
        return;
        }

    if (vxbDevRegister ((DRIVER_REGISTRATION *)
        &usbVxbPlbHcdUhciDevRegistration)
        == ERROR)
        {
        USB_UHCD_ERR("vxbUsbUhciRegister - "
            "failed to registering UHCI driver over PLB bus \n",
            1, 2, 3, 4, 5, 6);

        return;
        }

    if (vxbDevRegister ((DRIVER_REGISTRATION *)
        &usbVxbHcdUhciHub)
        == ERROR)
        {
        USB_UHCD_ERR("vxbUsbUhciRegister - "
            "failed to registering UHCI root hub with vxBus \n",
            1, 2, 3, 4, 5, 6);

        return;
        }

    return;
    }

/*******************************************************************************
*
* vxbUsbUhciDeregister - de-register UHCI driver with vxBus
*
* This routine de-registers the UHCI Driver with vxBus Module. The routine
* first de-registers the UHCI Root hub. This is followed by deregistration of
* UHCI Controller for PCI and PLB bus types
*
* RETURNS: OK, or ERROR if not able to de-register with vxBus
*
* ERRNO: N/A
*/

STATUS vxbUsbUhciDeregister (void)
    {

    /* de-register the UHCI root hub as bus controller driver */

    if (vxbDriverUnregister (&usbVxbHcdUhciHub) == ERROR)
        {
        USB_UHCD_ERR("vxbUsbUhciDeregister - "
            "failed to de-registering UHCI root hub with vxBus \n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }


    /* de-register the UHCI driver for PCI bus as underlying bus */

    if (vxbDriverUnregister ((DRIVER_REGISTRATION *)
                              &usbVxbPciHcdUhciDevRegistration)
        == ERROR)
        {
        USB_UHCD_ERR("vxbUsbUhciDeregister - "
            "failed to de-registering UHCI driver with PCI bus \n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* de-register the UHCI driver for PLB bus as underlying bus */

    if (vxbDriverUnregister (&usbVxbPlbHcdUhciDevRegistration) == ERROR)
        {
        USB_UHCD_ERR("vxbUsbUhciDeregister - "
            "failed to de-registering UHCI driver with PLB bus \n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    return OK;
    }

/*******************************************************************************
*
* usbVxbHcdUhciDeviceProbe - determine whether mathcing device is UHCI
*
* This routine determines whether the matching device is an UHCI Controller or
* not. For PCI device type, the routine will probe the PCI Configuration Device
* for Bus ID, Device Id and Function ID to determine wheter the device is of
* UHCI type. If a matching device is found, the routine will return TRUE.
*
* RETURNS: TRUE or FALSE
*
* ERRNO: none
*/

LOCAL BOOL usbVxbHcdUhciDeviceProbe
    (
    VXB_DEVICE_ID       pDev
    )
    {
    UINT32              busId = 0;      /* bus id of parent bus */

    UINT32              pciConfigInfo = 0;/* buffer for holding pci configuration
                                           * space information
                                           */

    /* Validate the paramter */

    if ((pDev == NULL) || (pDev->pParentBus == NULL)
        || (pDev->pParentBus->pBusType == NULL))
        return FALSE;


    /* Determine the bus type of parent bus */

    busId = pDev->pParentBus->pBusType->busID;

    switch (busId)
        {
        case VXB_BUSID_PCI :

            /*
             * PCI Type Device
             * read the config space using access functions
             */

            VXB_PCI_BUS_CFG_READ (pDev, 0x08, 4, pciConfigInfo);


            /* right shift by one byte of get the correct value */

            pciConfigInfo >>= 8;

            /*
             * To determine whether the device is UHCI host controller,
             * the bytes read from configuration space should be 0C0300h.
             *
             * 23:16 is Base Class Code should be 0Ch (serial bus controller
             * indication)
             *
             * 15:8 is Sub Class Code should be 03h (universal serial bus
             * controller indication)
             *
             * 7:0 is Programming interface should be 00h indicating no
             * specific register level implementation is defined
             */

            if (((pciConfigInfo & 0x00FF0000) == USB_UHCI_BASEC) &&
                ((pciConfigInfo & 0x0000FF00) == USB_UHCI_SCC) &&
                ((pciConfigInfo & 0x000000FF) == USB_UHCI_PI))

                /* matching device found */

                return TRUE;

            break;

        /* This routine is only intended for PCI devices */

        default :

            return FALSE;
        }

    return FALSE;
    }

/*******************************************************************************
*
* usbVxbUhciNullFunction - dummy routine
*
* RETURNS: N/A
*
* ERRNO: none
*/

LOCAL VOID usbVxbUhciNullFunction
    (
    VXB_DEVICE_ID       pDev
    )
    {
    /* This is a dummy routine which simply returns */

    return ;
    }

#define USB_UHCD_SHOW
#ifdef USB_UHCD_SHOW

/* foward declarations */

VOID usbUhcdRegShow
    (
    int index
    );

STATUS usbUhcdShow
    (
    UINT32         uIndex
    );

/*******************************************************************************
*
* usbUhcdTDShow - show the TD data structure
*
* This routine shows the TD data structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbUhcdTDShow
    (
    pUSB_UHCD_DATA          pHCDData,
    pUSB_UHCD_PIPE          pHCDPipe,
    pUSB_UHCD_REQUEST_INFO  pRequest,
    pUSB_UHCD_TD            pTD
    )
    {
    UINT32 uBusIndex = pHCDData->uBusIndex;

    printf("\nTD %p:\n", pTD);

    printf("dWord0Td            %08x\n",
            USB_UHCD_SWAP_DATA(uBusIndex,
                pTD->dWord0Td));

    printf("dWord1Td            %08x\n",
            USB_UHCD_SWAP_DATA(uBusIndex,
                pTD->dWord1Td));

    printf("dWord2Td            %08x\n",
            USB_UHCD_SWAP_DATA(uBusIndex,
                pTD->dWord2Td));

    printf("dWord3Td            %08x\n",
            USB_UHCD_SWAP_DATA(uBusIndex,
                pTD->dWord3Td));

    printf("\n========================================\n");

    }

/*******************************************************************************
*
* usbUhcdQHShow - show the QH data structure
*
* This routine shows the QH data structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbUhcdQHShow
    (
    pUSB_UHCD_DATA          pHCDData,
    pUSB_UHCD_PIPE          pHCDPipe,
    pUSB_UHCD_QH            pQH
    )
    {
    UINT32 uBusIndex = pHCDData->uBusIndex;

    printf("\nQH %p ==> TD %p, toggle %d\n",
        pQH,
        pQH->pTD,
        pQH->uToggle);

    printf("dWord0Qh            %08x\n",
        USB_UHCD_SWAP_DATA(uBusIndex,
                pQH->dWord0Qh));

    printf("dWord1Qh            %08x\n",
        USB_UHCD_SWAP_DATA(uBusIndex,
                pQH->dWord1Qh));

    printf("\n========================================\n");

    }

/*******************************************************************************
*
* usbUhcdRequestShow - show the isoch request info data structure
*
* This routine shows the isoch request info data structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbUhcdRequestShow
    (
    pUSB_UHCD_DATA          pHCDData,
    pUSB_UHCD_PIPE          pHCDPipe,
    pUSB_UHCD_REQUEST_INFO  pRequest
    )
    {
    /* Pointer to the TD */

    pUSB_UHCD_TD   pTD = pRequest->pHead;

    while (pTD != NULL)
        {
        usbUhcdTDShow(pHCDData, pHCDPipe, pRequest, pTD);

        /* Get to the next TD for same URB */

        if (pHCDPipe->uEndpointType != USBHST_ISOCHRONOUS_TRANSFER)
            pTD = pTD->hNextTd;
        else
            pTD = pTD->vNextTd;
        }
    }

/*******************************************************************************
*
* usbUhcdPipeShow - show the pipe data structure
*
* This routine shows the pipe data structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbUhcdPipeShow
    (
    pUSB_UHCD_DATA          pHCDData,
    pUSB_UHCD_PIPE          pHCDPipe
    )
    {
    /* To hold the pointer to the request information */

    pUSB_UHCD_REQUEST_INFO pRequest;

    /* To hold the pointer to the next request information */

    pUSB_UHCD_REQUEST_INFO pNextRequest;

    /* Indicate the tail request of the active list is reached */

    BOOLEAN                bTail = FALSE;

    printf("\npHCDPipe %p settings:\n"
        "uAddress           0x%x\n"
        "uEndpointAddress   0x%x\n"
        "uEndpointType      0x%x\n"
        "uEndpointDir       0x%x\n"
        "uMaximumPacketSize 0x%x\n"
        "uMaxTransferSize   0x%lx\n"
        "uMaxNumReqests     0x%lx\n",
        pHCDPipe,
        pHCDPipe->uAddress,
        pHCDPipe->uEndpointAddress,
        pHCDPipe->uEndpointType,
        pHCDPipe->uEndpointDir,
        pHCDPipe->uMaximumPacketSize,
        pHCDPipe->uMaxTransferSize,
        pHCDPipe->uMaxNumReqests);

    printf("\n========================================\n");

    if (pHCDPipe->pQH != NULL)
        {
        usbUhcdQHShow(pHCDData, pHCDPipe, pHCDPipe->pQH);
        }

    /* Start from the head of the active request list */

    pRequest = pHCDPipe->pRequestQueueHead;

    while (pRequest != NULL)
        {
        /*
         * Save the next active reuqest on the active request
         * list of this pipe so that we can safely move to the
         * next request once this request has been processed.
         */

        pNextRequest = pRequest->pNext;

        /*
         * If this request is already the tail of the pipe's
         * active request list, then we can break out once
         * this request is processed.
         */

        if (pRequest == pHCDPipe->pRequestQueueTail)
            bTail = TRUE;

        /* Check the request for completion */

        if (pHCDPipe->uEndpointType != USBHST_ISOCHRONOUS_TRANSFER)
            {
            printf("Scan Non Isoc Request %p\n",pRequest);

            usbUhcdRequestShow(pHCDData, pHCDPipe, pRequest);
            }
        else
            {
            printf("Scan Isoc Request     %p\n",pRequest);

            usbUhcdRequestShow(pHCDData, pHCDPipe, pRequest);
            }

        /*
         * If this request is already the tail of the pipe's
         * active request list, then we can break out.
         */

        if (bTail == TRUE)
            break;

        /* Go to the next request */

        pRequest = pNextRequest;
        }

    }

/*******************************************************************************
*
* usbUhcdPipeListShow - show the list of pipe structures
*
* This routine shows the list of pipe structures
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: Number of pipes shown on this list
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL UINT32 usbUhcdPipeListShow
    (
    pUSB_UHCD_DATA          pHCDData,
    pUSB_UHCD_PIPE          pHCDPipe,
    char *                  strDesc,
    BOOLEAN                 bUseAltNext
    )
    {
    UINT32                  uPipeCount;
    pUSB_UHCD_PIPE          pSavedHCDPipe;

    /* Reset to 0 */

    uPipeCount = 0;

    /* Save the head */

    pSavedHCDPipe = pHCDPipe;

    printf("\n==============================\n");

    printf("\n%s pipe list %p\n", strDesc, pHCDPipe);

    /*
     * Work when the pipe is valid
     */

    while (pHCDPipe != NULL)
        {
        /* Increase the pipe count scanned */

        uPipeCount++;

        /* Scan the pipe */

        usbUhcdPipeShow(pHCDData, pHCDPipe);

        if (bUseAltNext == TRUE)
            {
            /* Go to the next pipe */

            pHCDPipe = pHCDPipe->pAltNext;

            }
        else
            {
            /* Go to the next pipe */

            pHCDPipe = pHCDPipe->pNext;

            if (((pSavedHCDPipe->uEndpointType == USBHST_CONTROL_TRANSFER) ||
                 (pSavedHCDPipe->uEndpointType == USBHST_BULK_TRANSFER)) &&
                 (pSavedHCDPipe == pHCDPipe))
                {
                break;
                }
            }
        }

    printf("\n%s pipe count %d\n", strDesc, uPipeCount);

    printf("\n##############################\n");

    /* return the count */

    return uPipeCount;
    }

/*******************************************************************************
*
* usbUhcdRegShow - show the UHCI controller registers
*
* This routine shows the UHCI controller registers.
*
* <uIndex> - the UHCI host controller index
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbUhcdRegShow
    (
    int index
    )
    {
    pUSB_UHCD_DATA  pHCDData;
    UINT16          reg16;
    UINT32          reg32;

    if (index >= USB_MAX_UHCI_COUNT)
        return;

    pHCDData = g_pUHCDData[index];

    if (!pHCDData)
        return;

    reg16 = USB_UHCD_READ_WORD (pHCDData->pDev, USB_UHCD_USBCMD);
    printf("USB_UHCD_USBCMD     0x%08x\n", reg16);

    reg16 = USB_UHCD_READ_WORD (pHCDData->pDev, USB_UHCD_USBSTS);
    printf("USB_UHCD_USBSTS     0x%08x\n", reg16);

    reg16 = USB_UHCD_READ_WORD (pHCDData->pDev, USB_UHCD_USBINTR);
    printf("USB_UHCD_USBINTR    0x%08x\n", reg16);

    reg16 = USB_UHCD_READ_WORD (pHCDData->pDev, USB_UHCD_FRNUM);
    printf("USB_UHCD_FRNUM      0x%08x\n", reg16);

    reg32 = USB_UHCD_DWORD_IN (pHCDData->pDev, USB_UHCD_FLBASEADD);
    printf("USB_UHCD_FLBASEADD  0x%08x\n", reg32);

    reg16 = USB_UHCD_READ_BYTE (pHCDData->pDev, USB_UHCD_SOF_MODIFY);
    printf("USB_UHCD_SOF_MODIFY 0x%08x\n", reg16);

    reg16 = USB_UHCD_READ_WORD (pHCDData->pDev, USB_UHCD_PORT1);
    printf("USB_UHCD_PORT1      0x%08x\n", reg16);

    reg16 = USB_UHCD_READ_WORD (pHCDData->pDev, USB_UHCD_PORT2);
    printf("USB_UHCD_PORT2      0x%08x\n", reg16);

    }


/*******************************************************************************
*
* usbUhcdRegShow - show the UHCI controller registers and data structures
*
* This routine shows the UHCI controller registers and data structures.
*
* <uIndex> - the UHCI host controller index
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbUhcdShow
    (
    UINT32         uIndex
    )
    {
    /* Pointer to the HCDData */

    pUSB_UHCD_DATA pHCDData;

    /* Total scanned pipe count */

    UINT32         uTotalPipeCount = 0;

    /* Check the index to be in correct range */

    if (uIndex >= USB_MAX_UHCI_COUNT)
        {
        printf("Index %d too big\n",uIndex);

        return ERROR;
        }

    /* Check if there is a HC here */

    pHCDData = g_pUHCDData[uIndex];

    if (pHCDData == NULL)
        {
        printf("Index %d has no HC\n",uIndex);

        return ERROR;
        }

    /* Show all regisrters */

    usbUhcdRegShow(uIndex);

    /* Exclusively access the active pipe list */

    OS_WAIT_FOR_EVENT(pHCDData->ActivePipeListSynchEventID, OS_WAIT_INFINITE);

    /*
     * It is implemented that during the period the ActivePipeListSynchEventID
     * is taken, there is no modification done to these pipe lists.
     * If during the time of processing there is any pipe creation or deletion,
     * it is delayed until the processing is done.
     */

    /* Scan through the ISOC pipe list */

    uTotalPipeCount += usbUhcdPipeListShow(pHCDData,
                            pHCDData->pIsochPipeList,
                            "ISOC",FALSE);


    /* Scan through the NOM-ISOCH pipe list */

    uTotalPipeCount += usbUhcdPipeListShow(pHCDData,
                            pHCDData->pNonIsochPipeList,
                            "NON-ISOC",FALSE);

    /* Release the synchronization event */

    OS_RELEASE_EVENT(pHCDData->ActivePipeListSynchEventID);

    /* Exclusively access the reclamation list */

    OS_WAIT_FOR_EVENT(pHCDData->ReclamationListSynchEventID, OS_WAIT_INFINITE);

    /* Scan through the DelayedPipeAddition pipe list */

    uTotalPipeCount += usbUhcdPipeListShow(pHCDData,
                            pHCDData->pDelayedPipeAdditionList,
                            "DelayedPipeAddition",TRUE);

    /* Scan through the DelayedPipeRemoval pipe list */

    uTotalPipeCount += usbUhcdPipeListShow(pHCDData,
                            pHCDData->pDelayedPipeRemovalList,
                            "DelayedPipeRemoval",TRUE);

    printf("\nTotal %d pipes for this HC\n", uTotalPipeCount);

    /* Release the synchronization event */

    OS_RELEASE_EVENT(pHCDData->ReclamationListSynchEventID);

    return OK;
    }/* End of usbUhcdShow() */

#endif /* USB_UHCD_SHOW */

/************* End of usbUhcdInitialization.c *********************************/



