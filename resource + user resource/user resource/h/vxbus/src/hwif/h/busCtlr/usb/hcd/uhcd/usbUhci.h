/* usbUhci.h - USB UHCD host controller header */

/*
 * Copyright (c) 2003-2008, 2008, 2010 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2003-2008, 2008, 2010 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
Modification history
--------------------
01n,18may10,w_x  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
01m,10mar10,j_x  Changed for USB debug (WIND00184542)
01l,13jan10,ghs  vxWorks 6.9 LP64 adapting
01k,11Apr08,s_z  Redefine USB_OHCD_SWAP_BUFDATA for warning massage
01j,26sep07,ami  Fix CQ:WIND00102715 (check added in address conversion
                 macros)
01i,06sep07,p_g  Added code for Non-PCI host controller over vxBus
01h,07oct06,ami  Changes for USB-vxBus porting
01g,28mar05,pdg  non-PCI changes
01f,25feb05,mta  SPR106276
01e,18aug04,hch  remove the DMA_MALLOC and DMA_FREE macro
01d,17aug04,pdg  Fix for removing the dependency on OHCI while building the
                 project
01c,16jul04,hch  add UHCI PCI class definition
01b,26jun03,amn  changing the code to WRS standards.
01a,25apr03,ram  written.
*/

/*
DESCRIPTION

This file contains the constants of the USB UHCD host controller
*/

/*
INTERNAL
 ***************************************************************************
 * Filename         : usbUhci.h
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 * Description      :  This file contains the constants of the UHCD host
 *                     controller
 *
 */

#ifndef __INCusbUhcih
#define __INCusbUhcih

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "usb/usbOsal.h"
#include "usb/usbHcdInstr.h" /* WindView Instrumentation */
#include <cacheLib.h>

/* defines */

/* UHCI PCI class definition */

#define UHCI_CLASS              0x0c        /* BASEC value for serial bus */
#define UHCI_SUBCLASS           0x03        /* SCC value for USB */
#define UHCI_PGMIF              0x00        /* no specific pgm i/f defined */

/* Macro used for swapping the 32 bit values */

#define USB_UHCD_SWAP_DATA(INDEX,VALUE)                                 \
    (((g_pUHCDData[INDEX]->pDataSwap) == NULL) ? ((UINT32)(VALUE)) :    \
    (*(g_pUHCDData[INDEX]->pDataSwap))((UINT32)(VALUE)))

/* Macro used for swapping the contents of buffers */

#define USB_UHCD_SWAP_BUFDATA(INDEX,BUFFER,SIZE)

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCusbUhcih */



