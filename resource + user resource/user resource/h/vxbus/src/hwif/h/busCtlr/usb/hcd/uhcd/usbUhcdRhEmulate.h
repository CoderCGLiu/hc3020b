/* usbUhcdRhEmulate.h - header for USB UHCD HCD Roothub Emulation */

/*
 * Copyright (c) 2003, 2010 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2003, 2010 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
Modification history
--------------------
01d,18may10,w_x  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
01c,13jan10,ghs  vxWorks 6.9 LP64 adapting
01b,26jun03,mat  changing the code to WRS standards.
01a,25apr03,ram  written.
*/

/*
DESCRIPTION

This file contains the data structures used by the Roothub Emulation module
*/
/*
INTERNAL
 *******************************************************************************
 * Filename         : usbUhcdRhEmulate.h
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      :  This file contains the data structures used by the
 *                     Roothub Emulation module
 *
 *
 */

#ifndef __INCusbUhcdRhEmulateh
#define __INCusbUhcdRhEmulateh

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "usb/usbOsal.h"

/* defines */

/* Configuration value of the root hub device */

#define USB_UHCD_CONFIG_VALUE                 1

/* Definition of success */

#define USB_UHCD_DEF_SUCCESSDEF_SUCCESS       0

/* Value of the device descriptor */

#define USB_UHCD_DESCRIPTOR                   1

/* Value of the configuration descriptor */

#define USB_UHCD_CONFIGURATION_DESCRIPTOR     2

/* Feature Selector values used for the Root hub - Start */

/* PORT_ENABLE feature value */

#define USB_UHCD_PORT_ENABLE                  1

/* PORT_SUSPEND feature value */

#define USB_UHCD_PORT_SUSPEND                 2

/* PORT_RESET feature value */

#define USB_UHCD_PORT_RESET                   4

/* PORT_POWER feature value */

#define USB_UHCD_PORT_POWER                   8

/* PORT_LOW_SPEED feature value */
#define USB_UHCD_PORT_LOW_SPEED               9

/* C_PORT_CONNECTION feature value */

#define USB_UHCD_C_PORT_CONNECTION            16

/* C_PORT_ENABLE feature value */

#define USB_UHCD_C_PORT_ENABLE                17

/* C_PORT_SUSPEND feature value */

#define USB_UHCD_C_PORT_SUSPEND               18

/* C_PORT_OVERCURRENT feature value */

#define USB_UHCD_C_PORT_OVERCURRENT           19

/* C_PORT_RESET feature value */

#define USB_UHCD_C_PORT_RESET                 20

/* C_PORT_TEST feature value */

#define USB_UHCD_PORT_TEST                    21

/* C_PORT_INDICATOR feature value */

#define USB_UHCD_PORT_INDICATOR               22

/* Feature Selector values used for the Root hub - End */

/* Value indicating a pass condition */

#define USB_UHCD_PASS                         1

/* Value indicating a failure condition */

#define USB_UHCD_FAIL                         0

#define USB_UHCD_PORT_1                       1
#define USB_UHCD_PORT_2                       2

#define USB_UHCD_BYTE_TX_DATA_STAGE           4

#ifdef INCLUDED_FROM_RH_EMULATE

/* Bus signal line logical level */
#define USB_UHCD_D_NEGATIVE_SIGNAL            0x00
#define USB_UHCD_D_POSITIVE_SIGNAL            0x01

/* Device Descriptor value for the Root hub */

UCHAR usbUhcdGdDeviceDescriptor[] =
    {
    0x12,      /* bLength */
    0x01,      /* Device Descriptor type */
    0x10,
    0x01,      /* bcdUSB - USB 1.1 */
    0x09,      /* Hub DeviceClass */
    0x00,      /* bDeviceSubClass */
    0x00,      /* bDeviceProtocol */
    0x08,      /* Max packet size is 8 */
    0x00,
    0x00,      /* idVendor */
    0x00,
    0x00,      /* idProduct */
    0x00,
    0x00,      /* bcdDevice */
    0x00,      /* iManufacturer */
    0x00,      /* iProduct */
    0x00,      /* iSerialNumber */
    0x01       /* 1 configuration */
    };

/*
 * Descriptor structure returned on a request for
 * Configuration descriptor for the Root hub.
 * This includes the Configuration descriptor, interface descriptor and
 * the endpoint descriptor
 */

UCHAR usbUhcdGdConfigDescriptor[] =
    {

    /* Configuration descriptor */

    0x09,        /* bLength */
    0x02,        /*
                  * Configuration descriptor
                  * type
                  */
    0x19, 0x00,  /* wTotalLength */
    0x01,        /* 1 interface */
    USB_UHCD_CONFIG_VALUE,  /* bConfigurationValue */
    0x00,        /* iConfiguration */
    0xE0,        /* bmAttributes */
    0x00,        /* bMaxPower */

    /* Interface Descriptor */

    0x09,        /* bLength */
    0x04,        /*
                  * Interface Descriptor
                  * type
                  */
    0x00,        /* bInterfaceNumber */
    0x00,        /* bAlternateSetting */
    0x01,        /* bNumEndpoints */
    0x09,        /* bInterfaceClass */
    0x00,        /* bInterfaceSubClass */
    0x00,        /* bInterfaceProtocol */
    0x00,        /* iInterface */

    /* Endpoint Descriptor */

    0x07,        /* bLength */
    0x05,        /*
                  * Endpoint Descriptor
                  * Type
                  */
    0x81,        /* bEndpointAddress */
    0x03,        /* bmAttributes */
    0x08,
    0x00,        /* wMaxPacketSize */
    0x01         /* bInterval - 1 ms */
    };

/* This stucture is added to be send as argument for OS_CREATE_TASK */
typedef struct HCDDataURB
{
    /* pointer to the particular Host controller stucture */
    pUSB_UHCD_DATA  pHCDData;

    /* pointer to  the URB */
    USBHST_URB *purb;

}HCD_DATA_URB, *PHCD_DATA_URB;

#endif /* End of INCLUDED_FROM_RH_EMULATE */

/*******************************************************************************
 * Function Name    : usbUhcdCancelRhRequest
 * Description      : This function is used to cancel a request submitted for
 *                    an endpoint.
 * Parameters       : uPipeHandle        IN  Handle to the pipe.
 *                    pUrb               IN  Pointer to the URB which needs to
 *                                           be cancelled.
 * Return Type      : USBHST_SUCCESS - Returned if the URB is cancelled
 *                                     successfully.
 *                    USBHST_INVALID_PARAMETER - Returned if the parameters are
 *                                               not valid.
 ******************************************************************************/
extern USBHST_STATUS usbUhcdCancelRhRequest(pUSB_UHCD_DATA pHCDData,
                                            USBHST_URB *pUrb ,
                                            ULONG  uPipeHandle);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __INCusbUhcdRhEmulateh */


