/* usbPlxTcd.h - Definitions for PLX controller */

/*
 * Copyright (c) 2012-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
Modification history
--------------------
01c,03may13,wyy  Remove compiler warning (WIND00356717)
01b,21nov12,s_z  Add Reset exit workaround based Errata Issue_4
                 Reset squeuence number after endpoint halt (WIND00389964)
01a,18may12,s_z  Writen
*/

/*
DESCRIPTION

Defines constants related to the PLX NetChip NET 2280 USB device (target) IC.
*/

#ifndef __INCusbPlxTcdh
#define __INCusbPlxTcdh

/* includes */

#include <usb/usbTgt.h>
#include <hwif/util/vxbDmaBufLib.h>
#include "usbPlx228x.h"
#include "usbPlx338x.h"


#ifdef    __cplusplus
extern "C" {
#endif

#define USB_PLX_MAGIC_ID         (0x17CC0000)
#define USB_PLX_MAGIC_ALIVE      (USB_PLX_MAGIC_ID | 0x2280)
#define USB_PLX_MAGIC_DEAD       (USB_PLX_MAGIC_ID | 0xDEAD)


#define USB_PLX_DMA_CH_IVALID    (0xFF)
#define USB_PLX_SINGLE_DMA       (0x1)
#define USB_PLX_SG_DMA           (0x2)
#define USB_PLX_SG_DMA_MAX_SIZE  (0x10)
#define USB_PLX_MEM_BOUNDARY_SIZE (0x4000) /* 64K */

#define USB_PLX_DMA_DOOR_KEEPER  (511)

#define USB_PLX_MAX_IRQ_BOX       (0x10)
#undef USB_PLX_SHOW_ENABLE

/*
 * Virtual Endpoint Support
 * PLX NET 2282 add the virtual endpoint support.
 * While PLX USB2380/3380 not.
 */

typedef struct usb_plx_ctrl_ep_reg
    {
    UINT32 uEpCfg;
    UINT32 uEpRsp;
    UINT32 uEpIrqEnb;
    UINT32 uEpStat;
    UINT32 uEpAvail;
    UINT32 uEpMaxPktSize;
    }USB_PLX_CTRL_EP_REGS, *pUSB_PLX_CTRL_EP_REGS;


typedef struct usb_plx_irq_box
    {
    NODE   irqNode;
    UINT32 uIrqStat0;
    UINT32 uIrqStat1;
    UINT32 uEp0Stat;
    UINT32 uEp0Rsp;
    UINT32 uEp0Avail;
    }USB_PLX_IRQ_BOX,*pUSB_PLX_IRQ_BOX;

typedef struct usb_plx_ctrl
    {
    VXB_DEVICE_ID pDev;       /* struct vxbDev */
    spinlockIsr_t spinLock;   /* Spinlock for the device */

    /* Interrupt status */

    atomicVal_t uIrqStat0;
    atomicVal_t uIrqStat1;

    UINT32  uIrqMask0;
    UINT32  uIrqMask1;

    /* Magic value for shared interrupts */

    UINT32  uMagicCode;

    USB_PLX_CTRL_EP_REGS epReg[USB_PLX_MAX_EP_COUNT];

   /* struct usb_plx_dma_data dmaData; */

    void *  pTCDData;
    void *  pOCDData;
    void *  pHCDData;
    void *  pExtData;

    ULONG   uRegBase[USB_PLX_MAX_REGBASE_COUNT];
    UINT8   uTxFifoSize[USB_ENDPOINT_MAX_COUNT];
    UINT8   uRxFifoSize[USB_ENDPOINT_MAX_COUNT];

    UINT16  uFrameNumRecord;
    UINT16  uVendorId;      /* Pci vendor ID */
    UINT16  uDeviceId;      /* Pci Device ID */
    UINT8   uRevision;      /* Revision number */
    UINT8   uMaxEpNum;

    /* Register related definition */

    /* Handle for the register access methods */

    void *  pRegAccessHandle;

    /* Platform specific initialization */

    void    (*pUsbHwInit) (void);

    /*
     * Function pointer to hold the function doing
     * byte conversions for HC data structure
     */

    UINT32  (*pDescSwap)(UINT32 data);

    /*
     * Function pointer to hold the function doing
     * byte conversions for USB endian
     */

    UINT32  (*pUsbSwap)(UINT32 data);

    /*
     * Function pointer to hold the function doing
     * byte conversions for HC register endian
     */

    UINT32  (*pRegSwap)(UINT32 data);

    /*
      * Function pointer to hold the function doing
      * byte conversions for Bus address to CPU address
      */

    pVOID   (*pBusToCpu)(ULONG addr);

    /*
      * Function pointer to hold the function doing
      * byte conversions for Bus address to CPU address
      */

    ULONG  (*pCpuToBus)(pVOID pAddr);

    VOID  (*pISR)(void * pPlxCtrl);

    /*
     * Function pointer to hold the function doing
     * post reset operation
     */

    FUNCPTR     pPostResetHook;

    /* Is there any connection established */

    BOOL        bIsConnected;
    BOOL        bIsResetActive;
    }USB_PLX_CTRL, * pUSB_PLX_CTRL;

/* TCD instance data structure */

typedef struct usb_plx_tcd_data
    {
    pUSBTGT_TCD       pTcd;     /* TCD pointer */
    NODE              tcdNode;  /* TCD NODE */
    USB_TCD_EP0_STAGE ep0Stage; /* EP0 stage */


    pUSB_PLX_CTRL     pPlxCtrl;
    OS_EVENT_ID       tcdSyncMutex;      /* Mutex to protect the common */
    OS_EVENT_ID       isrEventId;        /* ISR semphore ID */
    OS_THREAD_ID      isrHandlerThread ; /* Interrupt Handler Thread ID */


    atomicVal_t       uIrqStat0;
    atomicVal_t       uIrqStat1;

    LIST irqBoxFreeList;
    LIST irqBoxPendList;

    VXB_DMA_TAG_ID    plxParentTag;    /* The TCD parent TAG ID */


    UINT8             uMaxPhyEps;
    UINT8             uMaxLagicalEps;

    LIST              tcdPipeList;     /* The list of usb_plx_tcd_pipe */
    UINT16            inEpUseFlag;     /* Endpoint Index being used */
    UINT16            outEpUseFlag;    /* Endpoint Index being used */

    UINT8             uTestMode;       /* Current test mode*/
    UINT8             uDmaType;
    BOOL              isAlaysShortPkt;
    BOOL              isDeviceSuspend; /* If the device is suspended */
    BOOL              isRemoteWakeup;  /* If the remote wakeup is enabled */
    BOOL              isSelfPower;     /* If the device is selfpower */
    BOOL              isSoftConnected; /* If the device is soft connected */
    BOOL              isU1Enabled;
    BOOL              isU2Enabled;
    BOOL              isLTMEnabled;
    } USB_PLX_TCD_DATA, *pUSB_PLX_TCD_DATA;

/* TCD pipe data structure */

typedef struct usb_plx_tcd_pipe
    {
    NODE   pipeNode;            /* Node to attach to the */
    SEM_ID pPipeSyncMutex;      /* Protect the request list */
    pUSBTGT_PIPE pUsbTgtPipe;
    VXB_DMA_TAG_ID    pipeDmaTagId;        /* DMA TAG ID used for user data */
    VXB_DMA_MAP_ID    pipeDmaMapId;        /* DMA MAP ID used for user data */
    pUSB_PLX_TCD_DATA pTCDData;
    pUSB_ENDPOINT_DESCR pEndpointDesc;

    LIST   pendingReqList;      /* Request list waiting to handle */
    LIST   freeReqList;         /* Free request list */
    UINT16 uIsoPipeErpCount;    /* Use for ISO transfer */
    UINT16 uIsoPipeCurErpIndex; /* Use for ISO transfer */

    UINT16 uMaxPacketSize;     /* Max pkt size */
    UINT16 uBytesPerInterval;  /* Total bytes tranfer per interval */
    UINT8  uEpAddr;            /* Logical address */
    UINT8  uEpDir;             /* Endpoint dir 0x80 IN; 0x00 OUT */
    UINT8  uEpType;            /* Type of endpoint */
    UINT8  uUsage;             /* Usage for interrupt and iso ep */
    UINT8  uSynchType;         /* Synchronization type */
    UINT8  uMaxBurst;          /* Max burst count */
    UINT8  uMaxStreams;        /* Max streams */
    UINT8  uMult;              /* Multiple packets per interval */
    UINT8  uInterval;          /* Interval */
    UINT8  uDataToggle;        /* For some controller, need record this */
    UINT8  uConfigValue;       /* Configuration number */
    UINT8  uInterface;         /* Interface number */
    UINT8  uAltSetting;        /* Altsetting number */
    UINT8  uPhyEpAddr;         /* Physical endpoint address */
    UINT8  uDmaChannel;        /* Dma channel of the endpoint */
    UINT8  uPipeFlag;          /* Current pipe state */
    USBTGT_PIPE_STATUS  status;/* The pipe status */

    }USB_PLX_TCD_PIPE,*pUSB_PLX_TCD_PIPE;

/* TCD request data structure */

typedef struct usb_plx_scat_dma_desc
    {
    /* Offset 0: - uDmaConfig
     *   Bit 31      : Valid
     *   Bit 30      : Direction
     *   Bit 29      : Done Interrupt Enable
     *   Bit 28      : End of Chain
     *   Bit 27      : DMA Scatter/Gather FIFO Validate
     *   Bit 26 - 25 : DMA ISO Extra Transactio Opportunity
     *   Bit 24      : DMA OUT Continue
     *   Bit 23 - 0  : DMA Byte Count
     */

    UINT32 uDmaConfig;

    /* Offset 4: PCI Starting Address */

    UINT32 uStartAddr;

    /*
     * Offset 8: Next Descriptor Address
     * The address should be 32 bit and the last 4 bit should be 0
     */

    UINT32 uNextDesc;

    /*
     * Offset C
     *   Bit 31 - 16 : Reserved
     *   Bit 15 - 0  : User Defined
     */

    UINT32 uSpecificData;
    }_WRS_PACK_ALIGN(4) USB_PLX_SCAT_DMA_DESC, *pUSB_PLX_SCAT_DMA_DESC;

typedef struct usb_plx_sing_dma_desc
    {
    UINT32  uDmaCtrl;
    UINT32  uDmaCount;
    UINT32  uDmaAddr;
    }USB_PLX_SING_DMA_DESC, *pUSB_PLX_SING_DMA_DESC;;


typedef struct usb_plx_dma_queue
    {
    void *  pRequest;
    struct usb_plx_scat_dma_desc * pScatDma;
    }USB_PLX_DMA_QUEUE, *pUSB_PLX_DMA_QUEUE;


typedef struct usb_plx_tcd_request
    {
    NODE              reqNode;         /* Request node */
    pUSB_PLX_TCD_PIPE pTCDPipe;        /* Pointer to the pTCDPipe */
    pUSB_ERP          pErp;            /* Pointer to the pErp */
    VXB_DMA_MAP_ID    usrDataDmaMapId; /* DMA MAP ID used for user data */
    VXB_DMA_TAG_ID    erpBufDmaTagId;  /* DMA TAG ID for erpBuf */
    VXB_DMA_MAP_ID    erpBufDmaMapId;  /* DMA MAP ID for erpBuf */
    void *            pErpBuf;         /* erpBuf */
    UINT8 *           pCurrentBuffer;  /* Data buffer address */
    UINT32            uXferSize;       /* Bytes size we expect to transfer */
    UINT32            uActLength;      /* Actual length of the transfer */
    UINT32            uCurXferLength;  /* Current transfer length */
    UINT16            uErpBufIndex;    /* Index of the bufList */
    UINT16            uErpBufCount;    /* Count of the bufList */
    struct usb_plx_scat_dma_desc * pScatDesc;
    UINT16            uPid;
    UINT8             uDmaMode;        /* Current DMA Mode */
    BOOL              bDmaActive;
    BOOL              bShortPkt;
    }USB_PLX_TCD_REQUEST, * pUSB_PLX_TCD_REQUEST;


#define USB_PLX_TCD_DATA_CONV_FROM_NODE(pNode) \
        USB_MEMBER_TO_OBJECT(pNode, USB_PLX_TCD_DATA, tcdNode)

#define USB_PLX_TCD_PIPE_CONV_FROM_NODE(pNode) \
        USB_MEMBER_TO_OBJECT(pNode, USB_PLX_TCD_PIPE, pipeNode)

#define USB_PLX_TCD_REQUEST_CONV_FROM_NODE(pNode) \
        USB_MEMBER_TO_OBJECT(pNode, USB_PLX_TCD_REQUEST, reqNode)


#define USB_PLX_REG_READ32(pPlxCtrl, OFFSET)                                    \
        vxbRead32 (((pUSB_PLX_CTRL)(pPlxCtrl))->pRegAccessHandle,              \
            (void *)(((pUSB_PLX_CTRL)(pPlxCtrl))->uRegBase[0] + OFFSET))
#define USB_PLX_REG_WRITE32(pPlxCtrl, OFFSET, VALUE)                            \
        vxbWrite32 (((pUSB_PLX_CTRL)(pPlxCtrl))->pRegAccessHandle,             \
            (void *)(((pUSB_PLX_CTRL)(pPlxCtrl))->uRegBase[0] + OFFSET), (UINT32)(VALUE))

#define USB_PLX_8051_READ32(pPlxCtrl, OFFSET)                                   \
        vxbRead32 (((pUSB_PLX_CTRL)(pPlxCtrl))->pRegAccessHandle,              \
            (void *)(((pUSB_PLX_CTRL)(pPlxCtrl))->uRegBase[1] + OFFSET))

#define USB_PLX_8051_WRITE32(pPlxCtrl, OFFSET, VALUE)                           \
        vxbWrite32 (((pUSB_PLX_CTRL)(pPlxCtrl))->pRegAccessHandle,             \
            (void *)(((pUSB_PLX_CTRL)(pPlxCtrl))->uRegBase[1] + OFFSET), (UINT32)(VALUE))

#define USB_PLX_FIFO_READ32(pPlxCtrl, OFFSET)                                   \
        vxbRead32 (((pUSB_PLX_CTRL)(pPlxCtrl))->pRegAccessHandle,              \
            (void *)(((pUSB_PLX_CTRL)(pPlxCtrl))->uRegBase[2] + OFFSET))

#define USB_PLX_FIFO_WRITE32(pPlxCtrl, OFFSET, VALUE)                           \
        vxbWrite32 (((pUSB_PLX_CTRL)(pPlxCtrl))->pRegAccessHandle,             \
            (void *)(((pUSB_PLX_CTRL)(pPlxCtrl))->uRegBase[2] + OFFSET), (UINT32)(VALUE))


#define USB_PLX_REG_BIT_SET(pPlxCtrl, REGADDR, BITVALUE)                        \
        USB_PLX_REG_WRITE32(pPlxCtrl,                                           \
                            REGADDR,                                            \
                            (USB_PLX_REG_READ32(pPlxCtrl, REGADDR) |            \
                            BITVALUE))

#define USB_PLX_REG_BIT_CLR(pPlxCtrl, REGADDR, BITVALUE)                        \
        USB_PLX_REG_WRITE32(pPlxCtrl,                                           \
                            REGADDR,                                            \
                            (USB_PLX_REG_READ32(pPlxCtrl, REGADDR) &            \
                            (~BITVALUE)))

#define USB_PLX_PCI_IRQ_ENABLE(pPlxCtrl)                                        \
        USB_PLX_REG_BIT_SET(pPlxCtrl,                                           \
                            USB_PLX_PCIIRQENB1_REG,                             \
                            USB_PLX_XIRQENB1_INTEN)


#define USB_PLX_PCI_IRQ_DISABLE(pPlxCtrl)                                       \
        USB_PLX_REG_BIT_CLR(pPlxCtrl,                                           \
                            USB_PLX_PCIIRQENB1_REG,                             \
                            USB_PLX_XIRQENB1_INTEN)

#define USB_PLX_PCI_IRQ_TYPE_SET(pPlxCtrl, IRQREG, IRQTYPE)                     \
        USB_PLX_REG_BIT_SET(pPlxCtrl, IRQREG, IRQTYPE)

#define USB_PLX_PCI_IRQ_TYPE_CLR(pPlxCtrl, IRQREG, IRQTYPE)                     \
        USB_PLX_REG_BIT_CLR(pPlxCtrl, IRQREG, IRQTYPE)


typedef STATUS (* usbPlxTcdInterfacePrototype)
    (
    pUSB_PLX_CTRL pPlxCtrl
    );

#ifdef __cplusplus
}
#endif

#endif /* __INCusbPlxTcdh */
