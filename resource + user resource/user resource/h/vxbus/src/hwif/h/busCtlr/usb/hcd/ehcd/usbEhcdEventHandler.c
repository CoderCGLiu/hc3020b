/* usbEhcdEventHandler.c - USB EHCI HCD interrupt handler */

/*
 * Copyright (c) 2003-2015 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2003-2015 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
Modification history
--------------------
02w,09sep15,j_x  Add parameters for EHCI polling and interrupt threshold (VXW6-84172)
02v,05jul15,j_x  enable _VXB_DMABUFSYNC_DMA_POSTWRITE for write (VXW6-84648)
02u,16jul14,wyy  Unify APIs of USB message to support error report (VXW6-16596)
02t,10Jul13,wyy  Make usbd layer to uniformly handle ClearTTBuffer request
                 (WIND00424927)
02s,03may13,wyy  Remove compiler warning (WIND00356717)
02r,21apr13,wyy  Cut the relationship among busIndex of HCDData, unitNumber of
                 vxBus Device, and HC count (such as g_EHCDControllerCount or
                 g_UhcdHostControllerCount) to dynamically release or announce
                 a HCD driver (WIND00362065)
02q,31jan13,ljg  Add usb support for imx6 (WIND00366039)
02p,27jul12,ljg  Avoid PipeSynchEventID be releaseed twice (WIND00365763)
02o,10jul12,ljg  Update nStatus of pUrb after isochronous transfer (WIND00353767)
02n,27jun12,ljg  Update uTransferLength after isochronous transfer (WIND00353767)
02m,13dec11,m_y  Modify according to code check result (WIND00319317)
02l,24nov10,ghs  Use same parameter taken by spin lock (WIND00240615)
02k,30jul10,w_x  Correct URB cancel handling (WIND00223154)
02j,25jun10,w_x  Remove _VXB_DMABUFSYNC_DMA_POSTWRITE for write (WIND00218556)
02i,21jun10,w_x  Move request reclamation before pipe reclamation (WIND00218756)
02h,21jun10,ghs  Fix for polling mode(WIND00218930)
02g,28may10,w_x  Fix ISOCH pipe locking when completion/cancel (WIND00214650)
                 Fix ASYNCH pipe deletion crash (WIND00214253)
02f,18may10,w_x  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
02e,21mar10,w_x  Avoid taking up 100% CPU time by USB task (WIND00201956)
02d,09mar10,j_x  Changed for USB debug (WIND00184542)
02c,13jan10,ghs  vxWorks 6.9 LP64 adapting
02b,17sep09,y_l  Code Coverity CID(748,749): NULL pointer check (WIND00182326)
02a,15jul09,ghs  Fix for WIND00171264, remove align defines
01z,07jul09,s_z  Respin for the USB HCD re-initialization vxBus issue and
                 add parameter verification in usbEhcdISR(WIND00152849)
01y,15apr09,w_x  Check for USB_EHCD_MAGIC_ALIVE for shared interrupts
01x,09apr09,w_x  Call URB callback when cancel URB or delete pipe (WIND00160730)
01w,09feb09,w_x  Added "intEachTD" as a config paramter to make WIND00084918
                 fix built into library and configurable
01v,04feb09,w_x  Added support for FSL quirky EHCI with various register and
                 descriptor endian format (such as MPC5121E)
01u,07jul08,w_x  Code coverity changes (WIND00126885)
01t,20nov07,ami  NEC fix integrated under conditional macros (CQ:WIND00084918)
01s,10sep07,ami  Changes for PLB-Based controller Support
01r,05sep07,jrp  APIGEN Fixups
01q,27aug07,pdg  Enabled asynchronous schedule only if there is an outstanding
                 transfer(WIND00102592)
01p,22May07,jrp  WIND00095133 register method initialization
01o,23mar07,jrp  SMP Conversion
01n,07oct06,ami  Changes for USB-vxBus porting
01m,28mar05,pdg  non-PCI changes
01l,22mar05,mta  64-bit support added (SPR #104950)
01k,19jan05,pdg  Fix for SPR #104949(USB STACK 2.1 PID 2.1 ISN'T CORRECTLY
                 SUPPORTING ON BOARD VIA VT8237 EHCI HOST)
01j,08dec04,pdg  Removed redundant code-USBIP merge error
01i,03dec04,ami  intLock Changes
01h,03dec04,ami  Merged IP Changes
01g,26oct04,ami  Severity changes for debug messages
01f,25oct04,pdg  SPR #102820 partial fix
01e,28Jul03,gpd  Updated changes identified during integration testing.
01d,23Jul03,gpd  Incorporated changes after testing on MIPS
01c,03jul03,gpd  Included the changes for separate reclamation lists for
                 asynchronous and periodic lists.
                 Updated the usbEhcdProcessTransferCompletion() function to
                 handle a transfer less than the request transfer size.
                 Support for ClearTTBuffer request.
01b,26jun03,psp  changing the code to WRS standards.
01a,25apr03,ram  written.
*/

/*
DESCRIPTION

This contains interrupt routines which handle the EHCI interrupts.

INCLUDE FILES: usb2/usbOsal.h, usb2/usbHst.h, usb2/usbEhcdDataStructures.h,
usb2/usbEhcdUtil.h, usb2/BusAbstractionLayer.h, usb2/usbEhcdEventHandler.h,
usb2/usbEhcdHal.h, usb2/usbEhcdRhEmulation.h,intLib.h
*/

/*
INTERNAL
 ******************************************************************************
 * Filename         : usbEhcdEventHandler.c
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      :  USB EHCI HCD interrupt handler
 *
 *
 ******************************************************************************/


/* includes */
#include <intLib.h>
#include <usb/usbOsal.h>
#include <usb/usbHst.h>
#include "usbEhcdDataStructures.h"
#include "usbEhcdUtil.h"
#include "usbEhcdEventHandler.h"
#include "usbEhcdHal.h"
#include "usbEhcdRhEmulation.h"
#include <usb/usbHcdInstr.h>
#include <spinLockLib.h>
#include <vxAtomicLib.h>
#include <cacheLib.h>

/* globals */

extern UINT32                           g_EHCDHandle;
extern pUSB_EHCD_DATA *                 g_pEHCDData;

#define USE_SPINLOCKS

#ifdef USE_SPINLOCKS
/*
 * This is actually a dynamically allocated array of size USB_MAX_EHCI_COUNT.
 */
extern spinlockIsr_t spinLockIsrEhcd[];
#endif

/* locals */

LOCAL VOID usbEhcdPortChangeHandler
    (
    pUSB_EHCD_DATA pHCDData
    );

LOCAL VOID usbEhcdProcessTransferCompletion
    (
    pUSB_EHCD_DATA pHCDData
    );

LOCAL VOID usbEhcdCleanupAsynchPipes
    (
    pUSB_EHCD_DATA pHCDData
    );

LOCAL VOID usbEhcdCleanupPeriodicPipes
    (
    pUSB_EHCD_DATA pHCDData
    );

/*******************************************************************************
*
* usbEhcdISR - interrupt service routine for the EHCI Driver.
*
* This routine is the ISR for the EHCI Driver.
*
* SMP Note.  This ISR does not make any callouts except to ISR safe control
* functions (logMsg, semGive etc).  It manipulates local controller registers
* and then releases a semaphore for a high priority task to perform the
* processing.
*
* RETURNS : N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbEhcdISR
    (
    pVOID pHCDDev
    )
    {
    /* To store the fields in interrupt status register */

    UINT32 uInterruptStatus = 0;

    /* To store the fields in interrupt enable register */

    UINT32 uInterruptMask = 0;

    pUSB_EHCD_DATA pHCDData = NULL;
//  printk("@@@@@@@@@@%s %d@@@@@@@@@@@@@@@\n",__FUNCTION__,__LINE__);
    struct vxbDev * pDev = (struct vxbDev *)pHCDDev;

    /* Check the validity of the parameter */

    if ((NULL == pDev) || (pDev->pDrvCtrl == NULL))
        {
        USB_EHCD_ERR("usbEhcdISR - Parameter is not valid\n",
            0, 0, 0, 0, 0, 0);
        return;
        }

    pHCDData = (pUSB_EHCD_DATA)pDev->pDrvCtrl;

    if (pHCDData->isrMagic != USB_EHCD_MAGIC_ALIVE)

        {
        USB_EHCD_ERR("usbEhcdISR - ISR not active, maybe shared?\n",
            0, 0, 0, 0, 0, 0);
        return;
        }

    /*
     * Take the ISR spinlock.  NOTE:  It may be possible to eliminate this
     * spinlock if atomic operations are used in usbEhcdEventHandler.
     */

#ifdef USE_SPINLOCKS
    SPIN_LOCK_ISR_TAKE(&spinLockIsrEhcd[pHCDData->uBusIndex]);
#endif

    if (FALSE == usrUsbEhciPollingEnabled())
        {
        /* Disable the EHCI Interrupts */

        /* Get the contents uf usb interrupt Enable register */

        uInterruptMask = USB_EHCD_READ_REG(pHCDData,
                                           USB_EHCD_USBINTR);

        /*
         * Disable the interrupts by setting all bits to zero in
         * Interrupt Enable register
         */

        USB_EHCD_WRITE_REG(pHCDData,
                          USB_EHCD_USBINTR,
                          0);
        }

    /*
     * If any change is detected, signal the event
     * so that the thread can handle the interrupt
     */

    if (0 != (USB_EHCD_READ_REG(pHCDData,
                    USB_EHCD_USBSTS) & USB_EHCD_USBSTS_INTERRUPT_MASK))

       {
       /* Update the interrupt status value */

       uInterruptStatus = USB_EHCD_READ_REG(pHCDData,
                                            USB_EHCD_USBSTS) &
                                            USB_EHCD_USBSTS_INTERRUPT_MASK;

       /* Clear the interrupt status */

       USB_EHCD_WRITE_REG(pHCDData,
                          USB_EHCD_USBSTS,
                          uInterruptStatus);

    if (FALSE == usrUsbEhciPollingEnabled())
        {

        /* Mask all not enabled interrupts */

        uInterruptStatus = (uInterruptMask & uInterruptStatus);


        /*
         * If the periodic reclamation list head or the periodic
         * cancellation list head is NULL, clear the frame list
         * rollover interrupt
         */

        if ((pHCDData->pPeriodicReclamationListHead == NULL) &&
            (pHCDData->pHeadPeriodicCancelList == NULL))
            {
            uInterruptStatus &=
                (~USB_EHCD_USBINTR_FRAME_LIST_ROLLOVER_ENABLE_MASK);
            }

        }
    
        /* Proceed if occured intr is enabled */
        if (uInterruptStatus)
            {
#ifdef USE_SPINLOCKS
            /* Copy interrupt status to HCD Data strucrture */

            pHCDData->uInterruptStatus |= uInterruptStatus;
#else
            vxAtomicOr((atomic_t *)&pHCDData->uInterruptStatus,
                                               uInterruptStatus);
#endif
             }
        }

    if (FALSE == usrUsbEhciPollingEnabled())
        {
        /* Enable the EHCI interrupts */

        /* Enable the interrupts by writing to the Interrupt Enable register */

        USB_EHCD_WRITE_REG(pHCDData,
                          USB_EHCD_USBINTR,
                          uInterruptMask);
        }

#ifdef USE_SPINLOCKS
    SPIN_LOCK_ISR_GIVE (&spinLockIsrEhcd[pHCDData->uBusIndex]);
#endif

    if (uInterruptStatus)
        {
        /* Send the message to the high priority thread */

        OS_RELEASE_EVENT(pHCDData->InterruptEvent);
        }
    return;
    }/* End of function usbEhcdISR() */

/*******************************************************************************
*
* usbEhcdPortChangeHandler - handle the port change interrupt
*
* This routine handles a port change.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbEhcdPortChangeHandler
    (
    pUSB_EHCD_DATA pHCDData
    )
    {
    /* To hold the index into the ports */

    UINT32 uIndex = 0;

    /* To hold the status change of the hub */

    UINT32 uStatusChange = 0;

    /* To hold the Port Status Change information */

    UINT32 uPortSCInfo = 0;
    UINT32 temp;

    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT(
        USB_EHCI_WV_EVENT_HANDLER,
        "usbEhcdPortChangeHandler() starts",
        USB_EHCD_WV_FILTER);

    /* Check the validity of the parameter */

    if (NULL == pHCDData)
        {
        USB_EHCD_ERR(
            "usbEhcdPortChangeHandler - Parameter not valid\n",
            0, 0, 0, 0, 0, 0);

        return;
        }

    /* Assert of the request synchronization event is not valid */

    if (NULL == pHCDData->RequestSynchEventID)
        {
        USB_EHCD_ERR(
            "usbEhcdPortChangeHandler - RequestSynchEventID not valid\n",
            0, 0, 0, 0, 0, 0);

        return;
        }

    /*
     * This loop checks whether there is any change
     * in any of the downstream ports
     */

    for (uIndex = 0; uIndex < pHCDData->RHData.uNumDownstreamPorts; uIndex++)
        {

        /* Copy the data in the Root Hub's buffer */

        OS_MEMCPY(&uPortSCInfo,
                  (pHCDData->RHData.pPortStatus +
                   (USB_EHCD_RH_PORT_STATUS_SIZE * uIndex)),
                  USB_EHCD_RH_PORT_STATUS_SIZE);

        /* Read the PORTSC contents */

        temp = USB_EHCD_READ_REG(pHCDData,
                                 USB_EHCD_PORTSC(uIndex));

        /*
         * USBPHY_CTRL.ENHOSTDISCONDETECT of imx6
         * For host mode, enables high-speed disconnect detector. This signal allows the override of
         * enabling the detection that is normally done in the UTMI controller. The UTMI controller
         * enables this circuit whenever the host sends a start-of-frame packet.
         * SW shall set this bit when it found the high-speed device is connected, suggested during bus
         * reset, after found high-speed device in USB_PORTSC1.PSPD).
         * SW shall make sure this bit is not set at the end of resume, otherwise a wrong disconnect
         * status may be detected. Suggest clear it after set USB_PORTSC1.SUSP, set it again after
         * resume is ended(USB_PORTSC1.FPR==0).
         */

        if (pHCDData->uPlatformType == USB_EHCD_PLATFORM_IMX6)
            {
            if (temp & USB_EHCD_PORTSC_CURRENT_CONNECT_STATUS_MASK)
                {
                if ((temp & USB_EHCD_PORTSC_PSPD_MASK) == 0x08000000)  /* High speed.   USB_PORTSC1.PSPD */
                    {
                    /* USBPHY_CTRL_SET USBPHY_CTRL.ENHOSTDISCONDETECT */

                    *((volatile UINT32 *)((uintptr_t)pHCDData->phyBaseAddr + 0x34)) = 0x2;
                    }
                }
            else
                {
                /* USBPHY_CTRL_CLR USBPHY_CTRL.ENHOSTDISCONDETECT */

                *((volatile UINT32 *)((uintptr_t)pHCDData->phyBaseAddr + 0x38)) = 0x2;
                }
            }

        uPortSCInfo |= temp;

        /* Check if there is any change */

        if (0 != (uPortSCInfo &
                  (USB_EHCD_PORTSC_OVER_CURRENT_CHANGE_MASK |
                   USB_EHCD_PORTSC_CONNECT_STATUS_CHANGE_MASK |
                   USB_EHCD_PORTSC_PORT_ENABLE_DISABLE_CHANGE_MASK)))
            {
            /* Update the status change data */

            uStatusChange |= (USB_EHCD_RH_MASK_VALUE << uIndex);
            }
        }/* End of for () */

    /*
     * If there is a resume detected on the port, which is driven from the
     * device, update the status change bit
     */

    if ((0 != (uPortSCInfo & USB_EHCD_PORTSC_FORCE_PORT_RESUME_MASK)) &&
        (0 == (uPortSCInfo & USB_EHCD_RH_PORT_SUSPEND_CHANGE)))
        {
        /* Update the status change data */

        uStatusChange |= (USB_EHCD_RH_MASK_VALUE << uIndex);

        /*
         * Update the suspend status change bit in
         * the port status value stored in the Root hub's data structure.
         */

        uPortSCInfo |= USB_EHCD_RH_PORT_SUSPEND_CHANGE;
        }

    /* If there is no change return */

    if ((0 == uStatusChange) ||
        (NULL == pHCDData->RHData.pHubInterruptData))
        {
        USB_EHCD_DBG(
            "usbEhcdPortChangeHandler - No change report for hub status"
            " (uStatusChange %p, pHubInterruptData %p)\n",
            uStatusChange,
            pHCDData->RHData.pHubInterruptData, 0, 0, 0, 0);

        return;
        }

    /*
     * Call the function to copy the interrupt status to the
     * request buffer if a request is pending or to the Root hub interrupt
     * transfer data buffer if a request is not pending
     */

    /* Nothing can be done when the return value is false */

    usbEhcdCopyRHInterruptData(pHCDData, uStatusChange);

    return;
    }/* End of usbEhcdPortChangeHandler() */

/*******************************************************************************
*
* usbEhcdProcessDelayedPipeList - process any delayed pipe list modification
*
* This routine process any delayed pipe list modification.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbEhcdProcessDelayedPipeList
    (
    pUSB_EHCD_DATA pHCDData
    )
    {
    pUSB_EHCD_PIPE          pHCDPipe;

    /*
     * If there is any delayed pipe addition happened during the URB completion
     * processing, we will add them to the proper active pipe list here.
     */

    /* Exclusively access the reclamation list */

    OS_WAIT_FOR_EVENT(pHCDData->ReclamationListSynchEventID, OS_WAIT_INFINITE);

    pHCDPipe = pHCDData->pDelayedPipeAdditionList;

    while (pHCDPipe != NULL)
        {
        if (pHCDPipe->uEndpointType == USBHST_ISOCHRONOUS_TRANSFER)
            {
            /*
             * Add this pipe in the list of isochronous pipe maintained
             * in HCD data. The proper locking is done in the macro.
             */

            USB_EHCD_ADD_TO_ISOCH_PIPE_LIST(pHCDData, pHCDPipe);
            }
        else if (pHCDPipe->uEndpointType == USBHST_INTERRUPT_TRANSFER)
            {
            /* Proper locking is done in the macro */

            USB_EHCD_ADD_TO_INTER_PIPE_LIST(pHCDData, pHCDPipe);
            }
        else
            {
            /* Proper locking is done in the macro */

            USB_EHCD_ADD_TO_ASYNCH_PIPE_LIST(pHCDData, pHCDPipe);
            }

        /* Move the head pointer to the next of delayed pipe addition */

        pHCDData->pDelayedPipeAdditionList = pHCDPipe->pAltNext;

        /* Go to the next pipe that was delayed */

        pHCDPipe = pHCDData->pDelayedPipeAdditionList;
        }

    /* Release the exclusive access */

    OS_RELEASE_EVENT(pHCDData->ReclamationListSynchEventID);

    /*
     * Modification to the active pipe lists should be protected by
     * ActivePipeListSynchEventID.
     */

    /* Exclusively access the reclamation list */

    OS_WAIT_FOR_EVENT(pHCDData->ActivePipeListSynchEventID, OS_WAIT_INFINITE);

    /*
     * If there is any delayed pipe removal happened during the URB completion
     * processing, we will add them to the proper reclamnation pipe list here.
     */

    pHCDPipe = pHCDData->pDelayedPipeRemovalList;

    while (pHCDPipe != NULL)
        {
        /* Remove the pipe from the original active pipe list */

        if ((pHCDPipe->uEndpointType == USBHST_CONTROL_TRANSFER) ||
            (pHCDPipe->uEndpointType == USBHST_BULK_TRANSFER))
            {
            /*
             * Remove from the active asynch pipe list - the asynch
             * pipe list is a circle list, so if the pipe is on the
             * list then there must be 'pNext', otherwise the pipe
             * is already tear down from the active pipe list.
             */

            if (pHCDPipe->pNext != NULL)
                {
                USB_EHCD_REMOVE_ASYNCH_PIPE(pHCDData, pHCDPipe);
                }
            }
        else if (pHCDPipe->uEndpointType == USBHST_ISOCHRONOUS_TRANSFER)
            {
            /* Remove from the active isoc pipe list */

            USB_EHCD_REMOVE_ISOCH_PIPE(pHCDData, pHCDPipe);
            }
        else if (pHCDPipe->uEndpointType == USBHST_INTERRUPT_TRANSFER)
            {
            /* Remove from the active inter pipe list */

            USB_EHCD_REMOVE_INTER_PIPE(pHCDData, pHCDPipe);
            }

        /* Add to the reclamation list */

        if ((pHCDPipe->uEndpointType == USBHST_CONTROL_TRANSFER) ||
            (pHCDPipe->uEndpointType == USBHST_BULK_TRANSFER))
            {
            USB_EHCD_ADD_TO_ASYNCH_RECLAMATION_LIST(pHCDData, pHCDPipe);

            if (FALSE == usrUsbEhciPollingEnabled())
                {
                /* Enable the aynch advance interrupt */

                USB_EHCD_SET_BIT_USBINTR_INT_ON_ASYNC_ADVANCE_DOORBELL(pHCDData);
                }

            /*
             * Set the interrupt to be generated on an advance of
             * asynchronous schedule
             */

            USB_EHCD_SET_BIT_USBCMD_INT_ON_ASYNC_ADVANCE_DOORBELL(pHCDData);
            }
        else
            {
            USB_EHCD_ADD_TO_PERIODIC_RECLAMATION_LIST(pHCDData, pHCDPipe);

            if (FALSE == usrUsbEhciPollingEnabled())
                {
                /* Enable the frame list rollover interrupt */

                USB_EHCD_SET_BIT_USBINTR_INT_ON_FRAME_LIST_ROLLOVER(pHCDData);
                }
            }

        /* Move the head pointer to the next of delayed pipe addition */

        pHCDData->pDelayedPipeRemovalList = pHCDPipe->pAltNext;

        /* Go to the next pipe that was delayed */

        pHCDPipe = pHCDData->pDelayedPipeRemovalList;
        }

    /* Release the exclusive access */

    OS_RELEASE_EVENT(pHCDData->ActivePipeListSynchEventID);

    /* Take ASYNC mutex to set/clr ASYNCH_SCHEDULE_ENABLE bit */

    semTake (pHCDData->AsychQueueMutex, WAIT_FOREVER);

    /* Wait for the event */

    OS_WAIT_FOR_EVENT(pHCDData->ReclamationListSynchEventID,
                      OS_WAIT_INFINITE);

    if (pHCDData->pAsynchReclamationListHead != NULL)
        {
        USB_EHCD_DBG("usbEhcdProcessDelayedPipeList - "
                     "Set ASYNCH_SCHEDULE_ENABLE\n",
                     0, 0, 0, 0, 0, 0);

        USB_EHCD_SET_BIT(pHCDData,
                         USBCMD,
                         ASYNCH_SCHEDULE_ENABLE);
        }

    /* Release the event */

    OS_RELEASE_EVENT(pHCDData->ReclamationListSynchEventID);

    /* Release ASYNC mutex */

    semGive(pHCDData->AsychQueueMutex);
    }

/*******************************************************************************
*
* usbEhcdProcessNonIsochRequestCompletion - process non-isoch request completion
*
* This routine is to process non-isoch request completion.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbEhcdProcessNonIsochRequestCompletion
    (
    pUSB_EHCD_DATA          pHCDData,
    pUSB_EHCD_PIPE          pHCDPipe,
    pUSB_EHCD_REQUEST_INFO  pRequest
    )
    {
    /* Pointer to the tail of QTD */

    pUSB_EHCD_QTD   pQTDTail;

    /* Pointer to the head of QTD */

    pUSB_EHCD_QTD   pQTDHead;

    /* Pointer to the URB */

    pUSBHST_URB     pUrb;

    /* Index of the host controller */

    UINT32          uBusIndex;

    /* Pointer to the temporary QTD */

    pUSB_EHCD_QTD   pTempQTD = NULL;

    /* Flag to indicate whether the request is completed */

    BOOLEAN         bRequestCompleted = FALSE;

    /* Exclusively access the pipe request list */

    OS_WAIT_FOR_EVENT(pHCDPipe->PipeSynchEventID, OS_WAIT_INFINITE);

    /* Get the tail of the TD list */

    pQTDTail = (pUSB_EHCD_QTD)pRequest->pTail;

    /* Get the head of the TD list */

    pQTDHead = (pUSB_EHCD_QTD)pRequest->pHead;

    /* Check to make sure the request is valid */

    if ((NULL == pQTDHead) ||
        (NULL == pQTDTail) ||
        (NULL == pHCDPipe->pQH) ||
        (NULL == pRequest->pUrb) ||
        (NULL == pRequest->pUrb->pHcdSpecific)) /* canceled */
        {
        USB_EHCD_WARN(
               "NonIsochRequestCompletion got an invalid request!\n"
               "pQTDHead %p pQTDTail %p pQH %p pUrb %p\n",
               pQTDHead, pQTDTail, pHCDPipe->pQH, pRequest->pUrb, 0, 0);

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

        return;
        }

    /* Retrieve the bus index */

    uBusIndex = pHCDData->uBusIndex;

    /* Get the URB pointer of this request */

    pUrb = pRequest->pUrb;

    /*
     * If the tail of the request is completed, the whole request is
     * completed
     */

    if (0 == (USB_EHCD_GET_BITFIELD(uBusIndex,
                                    QTD,
                                    pQTDTail->uTransferInfo,
                                    TOKEN_STATUS)&
              USB_EHCD_QTD_STATUS_ACTIVE))
        {
        /* Update the request completed flag */

        bRequestCompleted = TRUE;

        /*
         * Update the temporary pointer with the value of the tail.
         * This is required to update the status of the URB.
         */

        pTempQTD = pQTDTail;
        }
    else
        {
        /*
         * Check if the head is completed. If the head is completed,
         * there are 3 cases to be handled.
         *
         * 1. The request is not handled
         * 2. The request has resulted in a transfer size which is
         *    is less than the expected size
         * 3. The transfer has resulted in an error.
         */

        if (0 == (USB_EHCD_GET_BITFIELD(uBusIndex,
                                        QTD,
                                        pQTDHead->uTransferInfo,
                                        TOKEN_STATUS)&
                  USB_EHCD_QTD_STATUS_ACTIVE))
            {
            /* To hold the temporary pointer of the QTD */

            pUSB_EHCD_QTD pQTDTempHead = NULL;

            /*
             * Copy the head onto the temporary pointer. This is
             * done to avoid changing the actual head pointer which
             * is used further down the code
             */

            pQTDTempHead = pQTDHead;

            if (pHCDData->intEachTD)
                {
                /*
                 * The below code addresses defect CQ:WIND00084918.
                 * It was observerd that sometimes the Next qTD
                 * pointer in the Transfer Descriptor(qTD) of the
                 * EHCI Data Structure was not getting unpdated
                 * by the host controller hardware. This resulted
                 * in a transfer error. In the below code, we are
                 * providing a work-around (very similar to the
                 * workaround provided by Linux Code for the same
                 * hardware).
                 *
                 * Description of Workaround Provided
                 * ----------------------------------
                 * As the workaround, for every TD processes by the
                 * host controller, an interrupt was generated,
                 * subsequent to which the software (HCD)
                 * used to update the Next qTD pointer manually.
                 *
                 * In the below code we update the next QTD pointer
                 * manully on an error.
                 */

                if (pHCDPipe->pQH->uCurrentQtdPointer ==
                    pHCDPipe->pQH->uNextQtdPointer)
                    {
                    /* Manually update the QH next QTD pointer */

                    pHCDPipe->pQH->uNextQtdPointer =
                                      pQTDTempHead->uNextQTDPointer;

                    /* Release the exclusive access */

                    OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

                    /* Proceed to the next request in the request list */

                    return;
                    }
                }

            /*
             * Check if any of the requests have completed with
             * halted bit set or with a size which is less
             * than that expected.
             */

            for (; pQTDTempHead != pQTDTail; pQTDTempHead = pTempQTD)
                {
                /* Store the next pointer temporarily */

                pTempQTD = (pUSB_EHCD_QTD)pQTDTempHead->pNext;

                /* Check if the QTD is completed */

                if (0 == (USB_EHCD_GET_BITFIELD(uBusIndex,
                                      QTD,
                                      pQTDTempHead->uTransferInfo,
                                      TOKEN_STATUS) &
                                      USB_EHCD_QTD_STATUS_ACTIVE))

                    {
                    /*
                     * Check if the TD has transferred data
                     * less than expected (this can happen only
                     * for a non-control transfer request) or
                     * if the request has halted. The request is
                     * completed if this condition is true.
                     */

                    if ((USB_EHCD_QTD_STATUS_HALTED ==
                        (USB_EHCD_GET_BITFIELD(uBusIndex,
                                   QTD,
                                   pQTDTempHead->uTransferInfo,
                                   TOKEN_STATUS)&
                                   USB_EHCD_QTD_STATUS_HALTED)) ||
                        ((USBHST_CONTROL_TRANSFER !=
                          pHCDPipe->uEndpointType) &&
                         (0 != USB_EHCD_GET_BITFIELD(uBusIndex,
                                    QTD,
                                    pQTDTempHead->uTransferInfo,
                                    TOKEN_TOTAL_BYTES_TO_TRANSFER))))
                        {
                        bRequestCompleted = TRUE;

                        /*
                         * Update the temporary pointer so that it
                         * can be used for updating the status of
                         * the URB
                         */

                        pTempQTD = pQTDTempHead;

                        break;
                        }
                    }
                /* The request is not completed */
                else
                    {
                    bRequestCompleted = FALSE;

                    break;
                    }
                }/* End of for () */
            }/* End of if () */
        }/* End of else */

    /*
     * If the request is not completed, goto the next element
     * in the HCD request list.
     */

    if (FALSE == bRequestCompleted)
        {
        USB_EHCD_VDBG("NonIsochRequestCompletion - "
                      "pRequest %p on dev %p ep %p Not completed\n",
                      pRequest,
                      pHCDPipe->uAddress,
                      pHCDPipe->uEndpointAddress, 4, 5, 6);

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

        return;
        }

    /*
     * The request is completed at this point. Check if the
     * completed request is an asynchronous transfer request.
     * If it is so, decrement the outstanding asynchronous
     * transfer count. If there are no more outstanding
     * asynchronous transfer requests, disable the asynchronous
     * schedule.
     */

    if (USBHST_INTERRUPT_TRANSFER !=
        pRequest->pHCDPipe->uEndpointType)
        {
        semTake (pHCDData->AsychQueueMutex, WAIT_FOREVER);

        pHCDData->noOfAsynchTransfers--;

        if (pHCDData->noOfAsynchTransfers == 0)
            {

             /*
              * There are chances that at this point, another
              * task tries to delete an asynchronous pipe and has
              * enabled the asynch advance doorbell interrupt.
              * An asynch advance doorbell interrupt will not be
              * generated if the schedule is disabled. So check
              * if there are any elements in the reclamation list.
              * If only there are no elements in the reclamation
              * list, disable the asynchronous schedule.
              */

            /* Wait for the event */

            OS_WAIT_FOR_EVENT(pHCDData->ReclamationListSynchEventID,
                              OS_WAIT_INFINITE);

            if (pHCDData->pAsynchReclamationListHead == NULL)
                {
                USB_EHCD_VDBG("NonIsochRequestCompletion - "
                             "Clr ASYNCH_SCHEDULE_ENABLE\n",
                             0, 0, 0, 0, 0, 0);

                USB_EHCD_CLR_BIT(pHCDData,
                                 USBCMD,
                                 ASYNCH_SCHEDULE_ENABLE);
                }

            /* Release the event */

            OS_RELEASE_EVENT(pHCDData->ReclamationListSynchEventID);
            }

        semGive (pHCDData->AsychQueueMutex);

        }
    /*
     * Update the non isochronous status and the number of
     * bytes transferred for the request
     */

    usbEhcdUpdateQTDData((UINT8)uBusIndex,
                                        pQTDHead,
                                        pQTDTail,
                                        pUrb);

    /* Check if the completed request is the first request */

    if (pQTDHead == pHCDPipe->pQH->pQTD)
        {
        pHCDPipe->pQH->pQTD = pQTDTail->pNext;

        USB_EHCD_VDBG("NonIsochRequestCompletion - "
               "1st request completed, pHCDPipe->pQH->pQTD = %p\n",
               pHCDPipe->pQH->pQTD, 2, 3, 4, 5, 6);

        if (pHCDPipe->pQH->pQTD == NULL)
            {
            /* Indicate that there are no elements in the list */

            USB_EHCD_SET_BITFIELD(uBusIndex,
                                  QH,
                                  pHCDPipe->pQH->uNextQtdPointer,
                                  USB_EHCD_INVALID_LINK,
                                  NEXTQTD_POINTER_T);

            /* Update the HC's next pointer */

            USB_EHCD_SET_BITFIELD(uBusIndex,
                                  QH,
                                  pHCDPipe->pQH->uNextQtdPointer,
                                  0,
                                  NEXTQTD_POINTER) ;
            }
        else
            {
            /*
              * If the pipe's halted bit is set, clear the halt
              * condition on the pipe
              */

             if (pHCDPipe->bIsHalted)
                 {
                 /*
                  * Indicate that this is an invalid element
                  * in the list
                  */

                  USB_EHCD_SET_BITFIELD(uBusIndex,
                                     QH,
                                     pHCDPipe->pQH->uNextQtdPointer,
                                     USB_EHCD_INVALID_LINK,
                                     NEXTQTD_POINTER_T);

                 }
             else
                 /* The endpoint's halt bit is not set */
                 {
                 /* The low 32 bit of the QTD BUS address */

                 UINT32 uQTDLow32;

                 /* Get the lower 32 bit of the BUS address */

                 uQTDLow32 = USB_EHCD_DESC_LO32(pHCDPipe->pQH->pQTD);

                 /*
                  * If this request completes with a short packet,
                  * where the last TD of this request is not accessed
                  * by the host controller, then this will succeed.
                  * We should manually go and update the next link
                  * pointers in this case.
                  */

                 if ((pHCDPipe->pQH->uCurrentQtdPointer !=
                      USB_EHCD_SWAP_DESC_DATA(uBusIndex, uQTDLow32)) &&
                      (USB_EHCD_QTD_STATUS_ACTIVE ==
                      (USB_EHCD_GET_BITFIELD(uBusIndex,
                                  QTD,
                                  pHCDPipe->pQH->pQTD->uTransferInfo,
                                  TOKEN_STATUS) &
                                  USB_EHCD_QTD_STATUS_ACTIVE)))
                     {

                     /* Update the HC's next pointer */

                     USB_EHCD_SET_BITFIELD(uBusIndex,
                             QH,
                             pHCDPipe->pQH->uNextQtdPointer,
                             (uQTDLow32 >> 5),
                             NEXTQTD_POINTER);

                     /*
                      * Indicate that this is an invalid element
                      * in the list
                      */

                     USB_EHCD_SET_BITFIELD(uBusIndex,
                             QH,
                             pHCDPipe->pQH->uNextQtdPointer,
                             USB_EHCD_VALID_LINK,
                             NEXTQTD_POINTER_T);
                     }
                 }
            }

        /*
         * If HCD is halted, clear the halt
         * condition on the pipe
         */

        if (pUrb->nStatus == USBHST_STALL_ERROR)
            {
            /*
             * Clear the halt condition and
             * reset data toggle for the pipe
             */

            pHCDPipe->pQH->uTransferInfo = 0;
            }
        }
    else
        {

        /* Retrieve the TD previous to the TD being removed */

        for (pTempQTD = pHCDPipe->pQH->pQTD;
             (NULL != pTempQTD) &&
             (pTempQTD->pNext != pRequest->pHead);
             pTempQTD = pTempQTD->pNext);

        USB_EHCD_DBG("NonIsochRequestCompletion - "
               "non-1st request completed, pTempQTD = %p\n",
               pTempQTD, 2, 3, 4, 5, 6);

        /*
         * If the temporary TD pointer is NULL,it is an error;
         * because if pRequest is not the head of the completed
         * request for this pipe, then there must be some
         * incompleted request right before this pRequest,
         * thus there must be TD right before the first TD
         * of this pRequest; So theoritically pPreRequestInfo
         * will never be NULL once the code runs to here;
         * However, for code coverity issue, we check pTempQTD
         * against NULL before refer to it. if that should
         * really happens, we continue.
         */

        if (NULL == pTempQTD)
            {
            USB_EHCD_WARN("Can not find a previous QTD for a \n"
                   "request (%p) which is not the head of the pipe (%p)!\n",
                   pRequest, pHCDPipe, 3, 4, 5, 6);

            /* Release the exclusive access */

            OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

            return;
            }

        /* Update the next pointers */

        if (pRequest->pNext == NULL)
            pTempQTD->pNext = NULL;
        else
            pTempQTD->pNext = pRequest->pNext->pHead;

        /*
         * If this element is the last element, indicate that
         * the next element is invalid
         */

        if (pTempQTD->pNext == NULL)
            {
            USB_EHCD_SET_BITFIELD(uBusIndex,
                      QTD,
                      pTempQTD->uNextQTDPointer,
                      USB_EHCD_INVALID_LINK,
                      NEXTQTD_POINTER_T);

            }
        else
            {
            /* The low 32 bit of the QTD BUS address */

            UINT32 uQTDLow32;

            /* Get the lower 32 bit of the BUS address */

            uQTDLow32 = USB_EHCD_DESC_LO32(pTempQTD->pNext);

            /* Update the HC's next pointer */

            USB_EHCD_SET_BITFIELD(uBusIndex,
                      QTD,
                      pTempQTD->uNextQTDPointer,
                      (uQTDLow32 >> 5),
                      NEXTQTD_POINTER) ;

            /* Indicate that this link is a valid link */

            USB_EHCD_SET_BITFIELD(uBusIndex,
                      QTD,
                      pTempQTD->uNextQTDPointer,
                      USB_EHCD_VALID_LINK,
                      NEXTQTD_POINTER_T);
            }

        /*
         * If the pipe's halted bit is set,
         * set the bit which disables host controller
         * access on this endpoint.
         */

        if (pHCDPipe->bIsHalted)
            {
            USB_EHCD_SET_BITFIELD(uBusIndex,
                                  QH,
                                  pHCDPipe->pQH->uNextQtdPointer,
                                  USB_EHCD_INVALID_LINK,
                                  NEXTQTD_POINTER_T);

            }

        /*
         * If HCD is halted, clear the halt
         * condition on the pipe
         */

        if (pUrb->nStatus == USBHST_STALL_ERROR)
            {
            /*
             * Clear the halt condition and
             * reset data toggle for the pipe
             */

            pHCDPipe->pQH->uTransferInfo = 0;

            }
        }

    /*
     * If there is any real data transfer, we should do
     * vxbDmaBufSync and unload the DMA MAP.
     */

    if (pRequest->uRequestLength != 0)
        {
        /* Check if the data transfer is IN direction */

        if (pRequest->uDataDir == USB_EHCD_DIR_IN)
            {
            /* DMA cache invalidate and <copy from bounce buffer> */

            vxbDmaBufSync(pHCDData->pDev,
                pHCDPipe->usrBuffDmaTagId,
                pRequest->usrDataDmaMapId,
                _VXB_DMABUFSYNC_DMA_POSTREAD);
            }
        else
            {

            /* DMA cache invalidate and <copy from bounce buffer> */

            vxbDmaBufSync(pHCDData->pDev,
                pHCDPipe->usrBuffDmaTagId,
                pRequest->usrDataDmaMapId,
                _VXB_DMABUFSYNC_DMA_POSTWRITE);
            }

        /* Unload the Data DMA MAP */

        vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
            pRequest->usrDataDmaMapId);
        }

    /* Control transfer should also deal with the Setup buffer */

    if (pHCDPipe->uEndpointType == USBHST_CONTROL_TRANSFER)
        {
        /* Setup transaction is always DMA OUT */


        /* DMA cache invalidate and <copy from bounce buffer> */

        vxbDmaBufSync(pHCDData->pDev,
                pHCDPipe->ctrlSetupDmaTagId,
                pRequest->ctrlSetupDmaMapId,
                _VXB_DMABUFSYNC_DMA_POSTWRITE);

        /* Unload the control Setup DMA MAP */

        vxbDmaBufMapUnload (pHCDPipe->ctrlSetupDmaTagId,
            pRequest->ctrlSetupDmaMapId);
        }

    /*
     * Return the request info to the pipe which also
     * release the URB from the request, so that the
     * URB callback can submit the URB again just in
     * the callback and it can find free request info
     * to be used.
     *
     * Note: This has to be done before calling the
     * URB callback.
     */

    /* Store the address of the head in the local */

    pQTDHead = (pUSB_EHCD_QTD)(pRequest->pHead);

    /* Release all the TDs associated with this Request */

    while (pQTDHead != NULL)
        {
        /* Store the next pointer temporarily */

        pTempQTD = pQTDHead->pNext;

        /* Return the QTD back to the free list */

        usbEhcdAddToFreeQTDList(pHCDData, pHCDPipe, pQTDHead);

        /* When reach the tail QTD of the request, break */

        if (pQTDHead == (pUSB_EHCD_QTD)pRequest->pTail)
            break;

        /* Go to the next QTD */

        pQTDHead = pTempQTD;
        }

    /* Release the request info */

    usbEhcdReleaseRequestInfo(pHCDData, pHCDPipe, pRequest);

    /* Release the exclusive access */

    OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

    /*
     * Check if the buffer pointer is valid and the return
     * status is success
     */

    /* Call the callback function if it is registered */

    if (NULL != pUrb->pfCallback)
        {
        /*
         * If the status is Time Out change to STALL for USBD to
         * understand the error
         */

        (pUrb->pfCallback)(pUrb);
        }
    }

/*******************************************************************************
*
* usbEhcdProcessIsochRequestCompletion - process isoch request completion
*
* This routine is to process isoch request completion.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbEhcdProcessIsochRequestCompletion
    (
    pUSB_EHCD_DATA          pHCDData,
    pUSB_EHCD_PIPE          pHCDPipe,
    pUSB_EHCD_REQUEST_INFO  pRequest
    )
    {
    /* Pointer to the URB */

    pUSBHST_URB     pUrb;

    /* Index of the host controller */

    UINT32          uBusIndex;

    /* Retrieve the bus index */

    uBusIndex = pHCDData->uBusIndex;

    /* Isochronous transfer completion */

    /* Check if it is a high speed data transfer request */

    if (USBHST_HIGH_SPEED == pHCDPipe->uSpeed)
        {
        /* Pointer to the tail of ITD */

        pUSB_EHCD_ITD pITDTail;

        /* Pointer to the head of ITD */

        pUSB_EHCD_ITD pITDHead;

        /* Pointer to the temporary ITD */

        pUSB_EHCD_ITD pTempITD;

        /* Micro frame mask value */

        UINT8 uMicroFrameMask = 0;

        /* Microframe Index */

        UINT8 uMicroFrameIndex=0;

        /* Status bit */

        UINT8 uTailStatusBit = 0;

        /* Exclusively access the pipe request list */

        OS_WAIT_FOR_EVENT(pHCDPipe->PipeSynchEventID, OS_WAIT_INFINITE);

        /* Get the URB pointer of this request */

        pUrb = pRequest->pUrb;

        /* Get the tail of the TD list */

        pITDTail = (pUSB_EHCD_ITD)pRequest->pTail;

        /* Get the head of the TD list */

        pITDHead = (pUSB_EHCD_ITD)pRequest->pHead;

        /* Make sure the reuqest is valid */

        if ((NULL == pITDHead) ||
            (NULL == pITDTail) ||
            (NULL == pUrb) ||
            (NULL == pUrb->pTransferSpecificData))
            {
            USB_EHCD_ERR("IsocRequestCompletion HS got an invalid request!\n"
                   "pITDHead %p pITDTail %p pUrb %p pTransferSpecificData %p\n",
                   pITDHead, pITDTail, pUrb,
                   (pUrb != NULL) ? pUrb->pTransferSpecificData : NULL, 5, 6);

            /* Release the exclusive access */

            OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

            return ;
            }

        /* Get the micro frame mask value */

        uMicroFrameMask = pHCDPipe->uUFrameMaskValue;

        /*
         * Check the status bit for the last uFrames
         * to verify the transfer complletion
         */

        for (uMicroFrameIndex=0;
            USB_EHCD_MAX_MICROFRAME_NUMBER > uMicroFrameIndex;
            uMicroFrameIndex++)
            {
            if ((uMicroFrameMask << uMicroFrameIndex) & 0x80)
                {
                /* Get the status bit of Tail ITD */

                uTailStatusBit = (UINT8)(USB_EHCD_GET_BITFIELD(uBusIndex,
                                    ITD,
                                    pITDTail->uTransactionStatusControlList[
                                    (USB_EHCD_MAX_MICROFRAME_NUMBER
                                    -  1 - uMicroFrameIndex)],
                                    TRANSACTION_STATUS) &
                                    USB_EHCD_ITD_STATUS_ACTIVE);
                break;
                }
            }/* End of for () */

        /* Whole of the request is completed */

        if (0 == uTailStatusBit)
            {

            /* Update the number of bytes transferred and status*/

            usbEhcdUpdateITDData((UINT8)uBusIndex,
                uMicroFrameMask,
                pITDHead,
                pITDTail,
                pUrb);

            /*
             * If there is any real data transfer, we should do
             * vxbDmaBufSync and unload the DMA MAP.
             */

            if (pRequest->uRequestLength != 0)
                {
                /* Check if the data transfer is IN direction */

                if (pRequest->uDataDir == USB_EHCD_DIR_IN)
                    {
                    /* DMA cache invalidate and <copy from bounce buffer> */

                    vxbDmaBufSync(pHCDData->pDev,
                        pHCDPipe->usrBuffDmaTagId,
                        pRequest->usrDataDmaMapId,
                        _VXB_DMABUFSYNC_DMA_POSTREAD);
                    }
                else
                    {
                    /* DMA cache invalidate and <copy from bounce buffer> */

                    vxbDmaBufSync(pHCDData->pDev,
                        pHCDPipe->usrBuffDmaTagId,
                        pRequest->usrDataDmaMapId,
                        _VXB_DMABUFSYNC_DMA_POSTWRITE);
                    }

                /* Unload the Data DMA MAP */

                vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
                    pRequest->usrDataDmaMapId);
                }

            /*
             * Return the request info to the pipe which also
             * release the URB from the request, so that the
             * URB callback can submit the URB again just in
             * the callback and it can find free request info
             * to be used.
             *
             * Note: This has to be done before calling the
             * URB callback.
             */

            /* Remove all the ITDs from request list. */

            while (pITDHead != NULL)
                {
                /* Save the next pointer in the request*/

                pTempITD = pITDHead->pVerticalNext;

                /* Unlink the ITD */

                usbEhcdUnLinkITD(pHCDData, pITDHead);

                /* Return the ITD to the free ITD list */

                usbEhcdAddToFreeITDList(pHCDData, pHCDPipe, pITDHead);

                /* When reach the tail of the request, break */

                if (pITDHead == pITDTail)
                    break;

                /* Go to the next ITD of this request */

                pITDHead = pTempITD;
                }

            /*
             * Check if this is the last ITD in the pipe
             * reset the last frame index
             */

            if (pHCDPipe->uLastIndex == pITDTail->uFrameListIndex + 1)
                {
                pHCDPipe->uLastIndex = USB_EHCD_NO_LIST;
                }

            /* Release the request info */

            usbEhcdReleaseRequestInfo(pHCDData, pHCDPipe, pRequest);

            /* Release the exclusive access */

            OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

            /* Call the callback function if it is registered */

            if (NULL != pUrb->pfCallback)
                {
                (pUrb->pfCallback)(pUrb);
                }

            return;
            }
        }
    else
        {

        /* Pointer to the tail of SITD */

        pUSB_EHCD_SITD pSITDTail;

        /* Pointer to the head of SITD */

        pUSB_EHCD_SITD pSITDHead;

        /* Pointer to the temporary SITD */

        pUSB_EHCD_SITD pTempSITD;

        /* Exclusively access the pipe request list */

        OS_WAIT_FOR_EVENT(pHCDPipe->PipeSynchEventID, OS_WAIT_INFINITE);

        /* Get the URB pointer of this request */

        pUrb = pRequest->pUrb;

        /* Get the tail of the TD list */

        pSITDTail = (pUSB_EHCD_SITD)pRequest->pTail;

        /* Get the head of the TD list */

        pSITDHead = (pUSB_EHCD_SITD)pRequest->pHead;

        /* Make sure the reuqest is valid */

        if ((NULL == pSITDHead) ||
            (NULL == pSITDTail) ||
            (NULL == pUrb) ||
            (NULL == pUrb->pTransferSpecificData))
            {
            USB_EHCD_ERR("IsocRequestCompletion FS got an invalid request!\n"
                   "pSITDHead %p pSITDTail %p pUrb %p pTransferSpecificData %p\n",
                   pSITDHead, pSITDTail, pUrb,
                   (pUrb != NULL) ? pUrb->pTransferSpecificData : NULL, 5, 6);

            /* Release the exclusive access */

            OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

            return;
            }

        /*
         * Check the status bit for the last Frame
         * to verify the transfer complletion
         */

        if (0 == (USB_EHCD_GET_BITFIELD(uBusIndex,
                                        SITD,
                                        pSITDTail->uTransferState,
                                        TRANSFER_STATE_STATUS) &
                                        USB_EHCD_SITD_STATUS_ACTIVE))
            {

            /* Update the number of bytes transferred and status */

            usbEhcdUpdateSITDData((UINT8)uBusIndex,
                        pSITDHead,
                        pSITDTail,
                        pUrb);

            /*
             * If there is any real data transfer, we should do
             * vxbDmaBufSync and unload the DMA MAP.
             */

            if (pRequest->uRequestLength != 0)
                {
                /* Check if the data transfer is IN direction */

                if (pRequest->uDataDir == USB_EHCD_DIR_IN)
                    {
                    /* DMA cache invalidate and <copy from bounce buffer> */

                    vxbDmaBufSync(pHCDData->pDev,
                        pHCDPipe->usrBuffDmaTagId,
                        pRequest->usrDataDmaMapId,
                        _VXB_DMABUFSYNC_DMA_POSTREAD);
                    }
                else
                    {
                    /* DMA cache invalidate and <copy from bounce buffer> */

                    vxbDmaBufSync(pHCDData->pDev,
                        pHCDPipe->usrBuffDmaTagId,
                        pRequest->usrDataDmaMapId,
                        _VXB_DMABUFSYNC_DMA_POSTWRITE);
                    }

                /* Unload the Data DMA MAP */

                vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
                    pRequest->usrDataDmaMapId);
                }

            /*
             * Return the request info to the pipe which also
             * release the URB from the request, so that the
             * URB callback can submit the URB again just in
             * the callback and it can find free request info
             * to be used.
             *
             * Note: This has to be done before calling the
             * URB callback.
             */

            /* Remove all the SITDs from request list. */

            while (pSITDHead != NULL)
                {
                /* Save the next SITD pointer */

                pTempSITD = pSITDHead->pVerticalNext;

                /* Unlink the ITD */

                usbEhcdUnLinkSITD(pHCDData, pSITDHead);

                /* Return the ITD to the free SITD list */

                usbEhcdAddToFreeSITDList(pHCDData, pHCDPipe, pSITDHead);

                /* When reach the tail of the request, break */

                if (pSITDHead == pSITDTail)
                    break;

                /* Go to the next SITD of this request */

                pSITDHead = pTempSITD;
                }

            /*
             * Check if this is the last SITD in the pipe
             * reset the last frame index
             */

            if (pHCDPipe->uLastIndex == pSITDTail->uFrameListIndex + 1)
                {
                pHCDPipe->uLastIndex = USB_EHCD_NO_LIST;
                }

            /* Release the request info */

            usbEhcdReleaseRequestInfo(pHCDData, pHCDPipe, pRequest);

            /* Release the exclusive access */

            OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

            /* Call the callback function if it is registered */

            if (NULL != pUrb->pfCallback)
                {
                (pUrb->pfCallback)(pUrb);
                }

            return;
            }
        } /* End of else for full speed */

    /* Release the exclusive access */

    OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);
    return;
    }

/*******************************************************************************
*
* usbEhcdProcessPipeCompletion - process a pipe for request completion
*
* This routine is to process a pipe for request completion.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbEhcdProcessPipeCompletion
    (
    pUSB_EHCD_DATA          pHCDData,
    pUSB_EHCD_PIPE          pHCDPipe
    )
    {
    /* To hold the pointer to the request information */

    pUSB_EHCD_REQUEST_INFO pRequest;

    /* To hold the pointer to the next request information */

    pUSB_EHCD_REQUEST_INFO pNextRequest;

    /* To hold the pointer to the tail request information */

    pUSB_EHCD_REQUEST_INFO pTailRequest;

    /*
     * Theoretically we should take the pipe lock <PipeSynchEventID> here
     * so that the integrity of active request list is maintained,
     * but the code is implemented that this is not necessary.
     *
     * 1. Once entered this loop, we saved the end of the current active
     * request list, which will be the last request to be scanned in this
     * loop.
     *
     * 2. If a new URB is submitted while the URB is called during the
     * processing, it is put to the end of the active request list. But
     * we will not scan the new request in this loop, becasue it can't be
     * in completion as it is just submitted. If for any reason it is really
     * completed while this loop is still under course, it will be scanned
     * in the next run to scan this pipe.
     *
     * 3. If any URB of this pipe is canceled during calling any of the URB
     * callback, the URB cancel code has been implemented that the callback of
     * the URB being canceled is called just in the URB cancel routine, namely
     * usbEhcdCancelURB, but the request info and its associated transfer
     * descriptors are not unlinked in that routine, instead, the request will
     * be put onto the proper request cancel list (but the <pNext> link pointer
     * is not modified, it just use another link pointer <pListRequest> to
     * link this canceled request onto the cancel request list). Thus, the
     * active request list is still not modified, so it is safe to scan through
     * this active list. The URB cancel routine will flag that the request has
     * been canceled, thus when this routine reached the canceled request, it
     * doesn't process it and ignore it. With this scheme, the active requst
     * list is kept integrity from being altered by new URB submitting and
     * URB cancelling while the pipe scan is under progress.
     *
     * 4. When scanning one request on the active request list, and find it has
     * completed, the request will be returned to the free request list of this
     * pipe (and it will be unlinked from the active request list of this pipe).
     * However, this still doesn't affect the integrity of the active request
     * list of this pipe. Because, for one thing, the request completion happens
     * sequentially for one pipe, so the altering of the active list happens
     * from the head of the list, that is to say, we don't need to refer to the
     * request any more after the request is scanned; for another thing, the
     * altering of the active list happens just in the context of this task,
     * sequentially, there is no asynchronous modification to the list, thus
     * we are safe.
     *
     * 5. When returnning a completed request (unlinking from the active list
     * and link it to the free request list of this pipe), we will take the
     * pipe lock <PipeSynchEventID> for a short period of time, this is to
     * protect the pipe free request list for integrity, becasue at that time
     * another task may try to take a free request to submit a new URB thus
     * althering of the pipe free request list should be protected.
     *
     * This is done to reduce lock contention. Previously, the EHCD is written
     * that each URB will be on both the pipe request list and the HCD global
     * request list, so when an interrupt indicating any USB completion occur,
     * the URB completion processing takes a HCD global lock, which is widely
     * used across the driver to protect almost any list access. Once the global
     * lock is taken, the other tasks can not take that lock, even the protected
     * resources are not closely related. For example, when this routine is
     * processing URB completion for one pipe, the lock is taken, if at the same
     * time any other pipe of any device wants to submit a new URB, it has to
     * wait the global lock to be released. Now we changed the lock to be per pipe
     * lock, and don't put the request onto the HCD global list but only on the
     * pipe active request list, based on the fact that the requests are
     * essentially submitted to the pipe, thus the request doesn't need to go
     * onto any global request list. Doing so we only need to take the pipe lock.
     * If another pipe wants to submit requests while we are proncessing this
     * pipe, it can just do the regular submitting process (it only needs to take
     * the pipe lock so that concurrent access to the free request list is safely
     * protected).
     */

    /* Save the tail of the active request list */

    pTailRequest = pHCDPipe->pRequestQueueTail;

    /* Start from the head of the active request list */

    pRequest = pHCDPipe->pRequestQueueHead;

    while (pRequest != NULL)
        {
        /*
         * Save the next active request on the active request
         * list of this pipe so that we can safely move to the
         * next request once this request has been processed.
         *
         * Note: This should be done before calling the actual
         * request completion handling. Becasue it is possible
         * that the current request is complete and the URB
         * callback is called, and the URB callback can do
         * anyting such as submit a new URB or cancel any existing
         * active URB, which will modify the active request list
         * of the pipe. If the moving to the pRequest->pNext is
         * done after the URB completion handling, then the pNext
         * may have been changed thus may delay some request
         * completion processing.
         */

        pNextRequest = pRequest->pNext;

        /* Check the request for completion */

        if (pHCDPipe->uEndpointType != USBHST_ISOCHRONOUS_TRANSFER)
            {
            usbEhcdProcessNonIsochRequestCompletion(pHCDData,
                pHCDPipe, pRequest);
            }
        else
            {
            usbEhcdProcessIsochRequestCompletion(pHCDData,
                pHCDPipe, pRequest);
            }

        /*
         * If this request is already the tail of the pipe's
         * active request list, then we can break out.
         */

        if (pRequest == pTailRequest)
            break;

        /* Go to the next request */

        pRequest = pNextRequest;
        }

    return;
    }

/*******************************************************************************
*
* usbEhcdProcessTransferCompletion - handle the data transfer interrupt
*
* This routine is used to handle the data transfer completion interrupt by
* scan through the pipe lists maintained by the HCD.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbEhcdProcessTransferCompletion
    (
    pUSB_EHCD_DATA          pHCDData
    )
    {
    /* Pointer to the HCDPipe */

    pUSB_EHCD_PIPE pHCDPipe = NULL;

    /* Pointer to the next HCDPipe */

    pUSB_EHCD_PIPE pNextHCDPipe = NULL;

    /* Exclusively access the active pipe list */

    OS_WAIT_FOR_EVENT(pHCDData->ActivePipeListSynchEventID, OS_WAIT_INFINITE);

    /*
     * It is implemented that during the period the ActivePipeListSynchEventID
     * is taken for processing transfer completion events, there is no
     * modification done to these pipe lists. If during the time of processing
     * completion there is any pipe creation or deletion, it is delayed until
     * the URB completion processing is done.
     */

    /* Scan through the ISOC pipe list */

    pHCDPipe = pHCDData->pIsochPipeList;

    /*
     * Work when the pipe is valid
     */

    while (pHCDPipe != NULL)
        {

        /* Save the next pipe */

        pNextHCDPipe = pHCDPipe->pNext;

        /* Scan the pipe */

        usbEhcdProcessPipeCompletion(pHCDData, pHCDPipe);

        /* Go to the next pipe */

        pHCDPipe = pNextHCDPipe;
        }

    /* Scan through the INTER pipe list */

    pHCDPipe = pHCDData->pInterPipeList;

    /*
     * Work when the pipe is valid
     */

    while (pHCDPipe != NULL)
        {
        /* Save the next pipe */

        pNextHCDPipe = pHCDPipe->pNext;

        /* Scan the pipe */

        usbEhcdProcessPipeCompletion(pHCDData, pHCDPipe);

        /* Go to the next pipe */

        pHCDPipe = pNextHCDPipe;
        }

    /*
     * Scan through the ASYNC pipe list.
     * pDefaultPipe and pAsynchTailPipe form a circular ASYNC pipe list.
     */

    pHCDPipe = pHCDData->pDefaultPipe;

    /*
     * Work when the pipe is valid - should be always valid
     * thus it will break when it reaches the default pipe again
     */

    while (pHCDPipe != NULL)
        {
        /* Save the next pipe */

        pNextHCDPipe = pHCDPipe->pNext;

        /* Scan the pipe */

        usbEhcdProcessPipeCompletion(pHCDData, pHCDPipe);

        /* Go to the next pipe */

        pHCDPipe = pNextHCDPipe;

        /*
         * Break out when reached the default pipe
         * because it is a circular list
         */

        if (pHCDPipe == pHCDData->pDefaultPipe)
            break;
        }

    /* Release the synchronization event */

    OS_RELEASE_EVENT(pHCDData->ActivePipeListSynchEventID);

    /*
     * Process any delayed pipe list modification that could have
     * happened during the time the ActivePipeListSynchEventID
     * was taken to process URB completion.
     */

    usbEhcdProcessDelayedPipeList(pHCDData);
    }/* End of usbEhcdProcessTransferCompletion() */

/*******************************************************************************
*
* usbEhcdProcessNonIsochRequestCancel - process non-isoch request cancel
*
* This routine is to process non-isoch request cancel.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbEhcdProcessNonIsochRequestCancel
    (
    pUSB_EHCD_DATA          pHCDData,
    pUSB_EHCD_PIPE          pHCDPipe,
    pUSB_EHCD_REQUEST_INFO  pRequest
    )
    {
    /* Pointer to the tail of QTD */

    pUSB_EHCD_QTD   pQTDTail;

    /* Pointer to the head of QTD */

    pUSB_EHCD_QTD   pQTDHead;

    /* Index of the host controller */

    UINT32          uBusIndex;

    /* Pointer to the temporary QTD */

    pUSB_EHCD_QTD   pTempQTD = NULL;

    /* Exclusively access the pipe request list */

    OS_WAIT_FOR_EVENT(pHCDPipe->PipeSynchEventID, OS_WAIT_INFINITE);

    /* Get the tail of the TD list */

    pQTDTail = (pUSB_EHCD_QTD)pRequest->pTail;

    /* Get the head of the TD list */

    pQTDHead = (pUSB_EHCD_QTD)pRequest->pHead;

    /* Check to make sure the request is valid */

    if ((NULL == pQTDHead) ||
        (NULL == pQTDTail) ||
        (NULL == pHCDPipe->pQH))
        {
        USB_EHCD_ERR("NonIsocRequestCancel got an invalid request!\n"
               "pQTDHead %p pQTDTail %p pQH %p pUrb %p\n",
               pQTDHead, pQTDTail, pHCDPipe->pQH, pRequest->pUrb, 5, 6);

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

        return;
        }

    /* Retrieve the bus index */

    uBusIndex = pHCDData->uBusIndex;

    /*
     * Return the request info to the pipe which also
     * release the URB from the request, so that the
     * class driver can submit the URB again just in
     * the callback and it can find free request info
     * to be used.
     */

    /* Store the address of the head in the local */

    pQTDHead = (pUSB_EHCD_QTD)(pRequest->pHead);

    /* Release all the TDs associated with this Request */

    while (pQTDHead != NULL)
        {
        /* Store the next pointer temporarily */

        pTempQTD = pQTDHead->pNext;

        /* Return the QTD back to the free list */

        usbEhcdAddToFreeQTDList(pHCDData, pHCDPipe, pQTDHead);

        /* When reach the tail QTD of the request, break */

        if (pQTDHead == (pUSB_EHCD_QTD)pRequest->pTail)
            break;

        /* Go to the next QTD */

        pQTDHead = pTempQTD;
        }

    /* Release the request info */

    usbEhcdReleaseRequestInfo(pHCDData, pHCDPipe, pRequest);

    /* Release the exclusive access */

    OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

    /*
     * We should not call the URB callback here, as the URB callback
     * should have been called once the request is canceled or the pipe
     * is deleted. See the comments in the end of usbEhcdCancelURB
     * for more details.
     */

    return;
    }

/*******************************************************************************
*
* usbEhcdProcessIsochRequestCancel - process isoch request cancel
*
* This routine is to process isoch request cancel.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbEhcdProcessIsochRequestCancel
    (
    pUSB_EHCD_DATA          pHCDData,
    pUSB_EHCD_PIPE          pHCDPipe,
    pUSB_EHCD_REQUEST_INFO  pRequest
    )
    {
    /* Index of the host controller */

    UINT32          uBusIndex;

    /* Retrieve the bus index */

    uBusIndex = pHCDData->uBusIndex;

    /* Isochronous transfer completion */

    /* Check if it is a high speed data transfer request */

    if (USBHST_HIGH_SPEED == pHCDPipe->uSpeed)
        {
        /* Pointer to the tail of ITD */

        pUSB_EHCD_ITD pITDTail;

        /* Pointer to the head of ITD */

        pUSB_EHCD_ITD pITDHead;

        /* Pointer to the temporary ITD */

        pUSB_EHCD_ITD pTempITD;

        /* Exclusively access the pipe request list */

        OS_WAIT_FOR_EVENT(pHCDPipe->PipeSynchEventID, OS_WAIT_INFINITE);

        /* Get the tail of the TD list */

        pITDTail = (pUSB_EHCD_ITD)pRequest->pTail;

        /* Get the head of the TD list */

        pITDHead = (pUSB_EHCD_ITD)pRequest->pHead;

        /* Make sure the reuqest is valid */

        if ((NULL == pITDHead) ||
            (NULL == pITDTail))
            {
            USB_EHCD_WARN("IsochRequestCancel HS got an invalid request!\n"
                   "pITDHead %p pITDTail %p\n",
                   pITDHead, pITDTail, 3, 4, 5, 6);

            /* Release the exclusive access */

            OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

            return;
            }

        /*
         * Return the request info to the pipe which also
         * release the URB from the request, so that the
         * URB callback can submit the URB again just in
         * the callback and it can find free request info
         * to be used.
         */

        /* Remove all the ITDs from request list. */

        while (pITDHead != NULL)
            {
            /* Save the next pointer in the request*/

            pTempITD = pITDHead->pVerticalNext;

            /* Unlink the ITD */

            usbEhcdUnLinkITD(pHCDData, pITDHead);

            /* Return the ITD to the free ITD list */

            usbEhcdAddToFreeITDList(pHCDData, pHCDPipe, pITDHead);

            /* When reach the tail of the request, break */

            if (pITDHead == pITDTail)
                break;

            /* Go to the next ITD of this request */

            pITDHead = pTempITD;
            }

        /*
         * Check if this is the last ITD in the pipe
         * reset the last frame index
         */

        if (pHCDPipe->uLastIndex == pITDTail->uFrameListIndex + 1)
            {
            pHCDPipe->uLastIndex = USB_EHCD_NO_LIST;
            }

        /* Release the request info */

        usbEhcdReleaseRequestInfo(pHCDData, pHCDPipe, pRequest);

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

        /*
         * We should not call the URB callback here, as the URB callback
         * should have been called once the request is canceled or the pipe
         * is deleted. See the comments in the end of usbEhcdCancelURB
         * for more details.
         */
        }
    else /* Full speed */
        {

        /* Pointer to the tail of SITD */

        pUSB_EHCD_SITD pSITDTail;

        /* Pointer to the head of SITD */

        pUSB_EHCD_SITD pSITDHead;

        /* Pointer to the temporary SITD */

        pUSB_EHCD_SITD pTempSITD;

        /* Exclusively access the pipe request list */

        OS_WAIT_FOR_EVENT(pHCDPipe->PipeSynchEventID, OS_WAIT_INFINITE);

        /* Get the tail of the TD list */

        pSITDTail = (pUSB_EHCD_SITD)pRequest->pTail;

        /* Get the head of the TD list */

        pSITDHead = (pUSB_EHCD_SITD)pRequest->pHead;

        /* Make sure the reuqest is valid */

        if ((NULL == pSITDHead) ||
            (NULL == pSITDTail))
            {
            USB_EHCD_WARN("IsochRequestCancel FS got an invalid request!\n"
                   "pSITDHead %p pSITDTail\n",
                   pSITDHead, pSITDTail, 3, 4, 5, 6);

            /* Release the exclusive access */

            OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

            return;
            }

        /*
         * Return the request info to the pipe which also
         * release the URB from the request, so that the
         * URB callback can submit the URB again just in
         * the callback and it can find free request info
         * to be used.
         */

        /* Remove all the SITDs from request list. */

        while (pSITDHead != NULL)
            {
            /* Save the next SITD pointer */

            pTempSITD = pSITDHead->pVerticalNext;

            /* Unlink the ITD */

            usbEhcdUnLinkSITD(pHCDData, pSITDHead);

            /* Return the ITD to the free SITD list */

            usbEhcdAddToFreeSITDList(pHCDData, pHCDPipe, pSITDHead);

            /* When reach the tail of the request, break */

            if (pSITDHead == pSITDTail)
                break;

            /* Go to the next SITD of this request */

            pSITDHead = pTempSITD;
            }

        /*
         * Check if this is the last SITD in the pipe
         * reset the last frame index
         */

        if (pHCDPipe->uLastIndex == pSITDTail->uFrameListIndex + 1)
            {
            pHCDPipe->uLastIndex = USB_EHCD_NO_LIST;
            }

        /* Release the request info */

        usbEhcdReleaseRequestInfo(pHCDData, pHCDPipe, pRequest);

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

        /*
         * We should not call the URB callback here, as the URB callback
         * should have been called once the request is canceled or the pipe
         * is deleted. See the comments in the end of usbEhcdCancelURB
         * for more details.
         */
        } /* End of else for full speed */

    return;
    }

/*******************************************************************************
*
* usbEhcdProcessPipeCompletion - process a pipe for request cancel
*
* This routine is to process a pipe for request cancel.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbEhcdProcessPipeCancelAll
    (
    pUSB_EHCD_DATA          pHCDData,
    pUSB_EHCD_PIPE          pHCDPipe
    )
    {
    /* To hold the pointer to the request information */

    pUSB_EHCD_REQUEST_INFO pRequest;

    /* To hold the pointer to the next request information */

    pUSB_EHCD_REQUEST_INFO pNextRequest;

    /* To hold the pointer to the tail request information */

    pUSB_EHCD_REQUEST_INFO pTailRequest;

    /* Save the tail of the active request list */

    pTailRequest = pHCDPipe->pRequestQueueTail;

    /* Start from the head of the active request list */

    pRequest = pHCDPipe->pRequestQueueHead;

    while (pRequest != NULL)
        {

        /*
         * Save the next active reuqest on the active request
         * list of this pipe so that we can safely move to the
         * next request once this request has been processed.
         *
         * Note: This should be done before calling the actual
         * request completion handling. Becasue it is possible
         * that the current request is complete and the URB
         * callback is called, and the URB callback can do
         * anyting such as submit a new URB or cancel any existing
         * active URB, which will modify the active request list
         * of the pipe. If the moving to the pRequest->pNext is
         * done after the URB completion handling, then the pNext
         * may have been changed thus may delay some request
         * completion processing.
         */

        pNextRequest = pRequest->pNext;

        /* Check the request for cancel */

        if (pHCDPipe->uEndpointType != USBHST_ISOCHRONOUS_TRANSFER)
            {
            usbEhcdProcessNonIsochRequestCancel(pHCDData,
                pHCDPipe, pRequest);
            }
        else
            {
            usbEhcdProcessIsochRequestCancel(pHCDData,
                pHCDPipe, pRequest);
            }

        /*
         * If this request is already the tail of the pipe's
         * active request list, then we can break out.
         */

        if (pRequest == pTailRequest)
            break;

        /* Go to the next request */

        pRequest = pNextRequest;
        }

    return;
    }

/*******************************************************************************
*
* usbEhcdCleanupAsynchPipes - clean up the asynchronous pipes
*
* This is used to perform the cleanup functionality of the asynchronous pipes.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbEhcdCleanupAsynchPipes
    (
    pUSB_EHCD_DATA pHCDData
    )
    {
    /* To hold the pointer to the pipe information */

    pUSB_EHCD_PIPE pHCDPipe = NULL;

    /* To hold the request information */

    pUSB_EHCD_REQUEST_INFO pRequest = NULL;

    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT(
        USB_EHCI_WV_EVENT_HANDLER,
        "usbEhcdCleanupAsynchPipes() starts",
        USB_EHCD_WV_FILTER);

    /* Check the validity of the parameters */

    if (NULL == pHCDData)
        {
        USB_EHCD_ERR("usbEhcdCleanupAsynchPipes - parameter not valid\n",
            0, 0, 0, 0, 0, 0);

        return;
        }

    /* Exclusively access the reclamation list */

    OS_WAIT_FOR_EVENT(pHCDData->ReclamationListSynchEventID, OS_WAIT_INFINITE);

    /*
     * Search the list which contains the requests which need to be removed
     * and release the memory allocated for all of them.
     */

    for (pRequest = pHCDData->pHeadAsynchCancelList;
         NULL != pRequest;
         pRequest = pHCDData->pHeadAsynchCancelList)
        {
        /* Update the head of the asynch request reclamation list */

        pHCDData->pHeadAsynchCancelList = pRequest->pAltNext;

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDData->ReclamationListSynchEventID);

        /* Save the pipe of this request */

        pHCDPipe = pRequest->pHCDPipe;

        USB_EHCD_DBG("Cleanup ASYNC dev %p ep %p pRequest = %p\n",
                     pHCDPipe->uAddress,
                     pHCDPipe->uEndpointAddress,
                     pRequest, 4, 5, 6);

        /* Cancel this request */

        usbEhcdProcessNonIsochRequestCancel(pHCDData, pHCDPipe, pRequest);

        /* Exclusively access the reclamation list */

        OS_WAIT_FOR_EVENT(pHCDData->ReclamationListSynchEventID,
                          OS_WAIT_INFINITE);
        }

#if 0 /* Keep for debug usage */
        usbEhcdPipeListShow(pHCDData,
                                    pHCDData->pAsynchReclamationListHead,
                                    "AsynchReclamation",FALSE);
#endif

    /* Search the list and remove the element */

    for (pHCDPipe = pHCDData->pAsynchReclamationListHead;
         NULL != pHCDPipe;
         pHCDPipe = pHCDData->pAsynchReclamationListHead)
        {

        /* Update the head of the reclamation list */

        pHCDData->pAsynchReclamationListHead = pHCDPipe->pNext;

        /* Release the reclamationsychronisation event */

        OS_RELEASE_EVENT(pHCDData->ReclamationListSynchEventID);

        USB_EHCD_DBG("Reclaim pHCDPipe dev %p ep %p \n",
                     pHCDPipe->uAddress,
                     pHCDPipe->uEndpointAddress,
                     3, 4, 5, 6);

        /* Cancel all requests on this pipe */

        usbEhcdProcessPipeCancelAll(pHCDData, pHCDPipe);

        USB_EHCD_DBG("usbEhcdCleanupAsynchPipes - Reclaim pHCDPipe %p \n",
            pHCDPipe, 2, 3, 4, 5, 6);

        /* Release the QH */

        if (NULL != pHCDPipe->pQH)
            {
            /* Destroy the QH */

            usbEhcdDestroyQH(pHCDData, pHCDPipe->pQH);
            }
        else
            {
            USB_EHCD_WARN("usbEhcdCleanupAsynchPipes - pHCDPipe %p QH NULL\n",
                pHCDPipe, 2, 3, 4, 5, 6);
            }

        /* Un-setup the pipe */

        usbEhcdUnSetupPipe(pHCDData, pHCDPipe);

        /* Destroy the pipe synch event */

        OS_DESTROY_EVENT(pHCDPipe->PipeSynchEventID);

        pHCDPipe->PipeSynchEventID = NULL;

        /* Release the memory allocated for the pipe */

        OS_FREE(pHCDPipe);

        /* Exclusively access the reclamation list */

        OS_WAIT_FOR_EVENT(pHCDData->ReclamationListSynchEventID,
                          OS_WAIT_INFINITE);
        }

    /*
     * Check if some request pending in Reclamation list or cancel list,
     * if not disable the intr
     */

    if ((NULL == pHCDData->pAsynchReclamationListHead) &&
        (NULL == pHCDData->pHeadAsynchCancelList))
        {
        /* Disable Async advance intr */

        USB_EHCD_CLR_BIT_USBINTR_INT_ON_ASYNC_ADVANCE_DOORBELL(pHCDData);
        }

    /* Release the exclusive access */

    OS_RELEASE_EVENT(pHCDData->ReclamationListSynchEventID);

    /* If we have no more ASYNC requests then we disable the ASYNC shcedule */

    semTake (pHCDData->AsychQueueMutex, WAIT_FOREVER);

    if (pHCDData->noOfAsynchTransfers == 0)
        {
        USB_EHCD_DBG("usbEhcdCleanupAsynchPipes - "
            "Clr ASYNCH_SCHEDULE_ENABLE\n", 0, 0, 0, 0, 0, 0);

        USB_EHCD_CLR_BIT(pHCDData,
                         USBCMD,
                         ASYNCH_SCHEDULE_ENABLE);
        }

    semGive (pHCDData->AsychQueueMutex);

    /*
     * Process any delayed pipe list modification that could have
     * happened during the time taken to process URB cancelation
     * or pipe deletion.
     */

    usbEhcdProcessDelayedPipeList(pHCDData);

    return;
    }/* End of usbEhcdCleanupAsynchPipes() */

/*******************************************************************************
*
* usbEhcdCleanupPeriodicPipes - clean up the periodic pipes
*
* This is used to perform the cleanup functionality of the periodic pipes.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbEhcdCleanupPeriodicPipes
    (
    pUSB_EHCD_DATA pHCDData
    )
    {
    /* To hold the pointer to the pipe information */

    pUSB_EHCD_PIPE pHCDPipe = NULL;

    /* To hold the request information */

    pUSB_EHCD_REQUEST_INFO pRequest = NULL;

    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT(
        USB_EHCI_WV_EVENT_HANDLER,
        "usbEhcdCleanupPeriodicPipes() starts",
        USB_EHCD_WV_FILTER);

    /* Check the validity of the parameters */

    if (NULL == pHCDData)
        {
        USB_EHCD_ERR(
            "usbEhcdCleanupPeriodicPipes - parameter not valid\n",
            0, 0, 0, 0, 0, 0);

        return;
        }

    /* Exclusively access the reclamation list */

    OS_WAIT_FOR_EVENT(pHCDData->ReclamationListSynchEventID, OS_WAIT_INFINITE);

    /*
     * Search the list which contains the requests which need to be removed
     * and release the memory allocated for all of them.
     */

    for (pRequest = pHCDData->pHeadPeriodicCancelList;
         NULL != pRequest;
         pRequest = pHCDData->pHeadPeriodicCancelList)
        {

        /* Update the head of the asynch request reclamation list */

        pHCDData->pHeadPeriodicCancelList = pRequest->pAltNext;

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDData->ReclamationListSynchEventID);

        /* Save the pipe of this request */

        pHCDPipe = pRequest->pHCDPipe;

        USB_EHCD_DBG("Cleanup PERIODIC dev 0x%X ep 0x%X pRequest = %p\n",
                      pHCDPipe->uAddress,
                      pHCDPipe->uEndpointAddress,
                      pRequest, 4, 5, 6);


        /* Cancel this request */

        if (pHCDPipe->uEndpointType == USBHST_ISOCHRONOUS_TRANSFER)
            {
            usbEhcdProcessIsochRequestCancel(pHCDData, pHCDPipe, pRequest);
            }
        else
            {
            usbEhcdProcessNonIsochRequestCancel(pHCDData, pHCDPipe, pRequest);
            }

        /* Exclusively access the reclamation list */

        OS_WAIT_FOR_EVENT(pHCDData->ReclamationListSynchEventID,
                          OS_WAIT_INFINITE);
        }

#if 0 /* Keep for debug usage */
    usbEhcdPipeListShow(pHCDData,
                                pHCDData->pPeriodicReclamationListHead,
                                "PeriodicReclamation",FALSE);
#endif

    /* Search the list and remove the element */

    for (pHCDPipe = pHCDData->pPeriodicReclamationListHead;
         NULL != pHCDPipe;
         pHCDPipe = pHCDData->pPeriodicReclamationListHead)
        {

        /* Update the head of the reclamation list */

        pHCDData->pPeriodicReclamationListHead = pHCDPipe->pNext;

        /* Release the reclamationsychronisation event */

        OS_RELEASE_EVENT(pHCDData->ReclamationListSynchEventID);

        /* Cancel all requests of this pipe */

        usbEhcdProcessPipeCancelAll(pHCDData, pHCDPipe);

        USB_EHCD_DBG("usbEhcdCleanupPeriodicPipes - Reclaim pHCDPipe %p \n",
            pHCDPipe, 2, 3, 4, 5, 6);

        /* Release the QH */

        if (NULL != pHCDPipe->pQH)
            {
            /* Destroy the QH */

            usbEhcdDestroyQH(pHCDData, pHCDPipe->pQH);
            }

        /* Un-setup the pipe */

        usbEhcdUnSetupPipe(pHCDData, pHCDPipe);

        /* Destroy the pipe synch event */

        OS_DESTROY_EVENT(pHCDPipe->PipeSynchEventID);

        pHCDPipe->PipeSynchEventID = NULL;

        /* Release the memory allocated for the pipe */

        OS_FREE(pHCDPipe);

        /* Exclusively access the reclamation list */

        OS_WAIT_FOR_EVENT(pHCDData->ReclamationListSynchEventID,
                          OS_WAIT_INFINITE);
        }

    /*
     * Check if some request pending in Reclamation list or cancel list,
     * if not disable the intr
     */

    if ((NULL == pHCDData->pPeriodicReclamationListHead) &&
        (NULL == pHCDData->pHeadPeriodicCancelList))
        {
        USB_EHCD_CLR_BIT_USBINTR_INT_ON_FRAME_LIST_ROLLOVER(pHCDData);
        }

    /* Release the exclusive access */

    OS_RELEASE_EVENT(pHCDData->ReclamationListSynchEventID);

    /*
     * Process any delayed pipe list modification that could have
     * happened during the time taken to process URB cancelation
     * or pipe deletion.
     */

    usbEhcdProcessDelayedPipeList(pHCDData);

    return;
    }/* End of usbEhcdCleanupPeriodicPipes() */

/*******************************************************************************
*
* usbEhcdHostSystemErrorHandler - handle the host system error interrupt
*
* This is used to handle the host system error interrupt.
*
* RETURNS: N/A.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbEhcdHostSystemErrorHandler
    (
    pUSB_EHCD_DATA pHCDData
    )
    {
    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT(
        USB_EHCI_WV_EVENT_HANDLER,
        "usbEhcdHostSystemErrorHandler() starts",
        USB_EHCD_WV_FILTER);

    /* Check the validity of the parameters */

    if (NULL == pHCDData)
        {
        USB_EHCD_ERR("usbEhcdHostSystemErrorHandler - parameter not valid\n",
            0, 0, 0, 0, 0, 0);
        return;
        }

    /*
     * When a host system error occurs, the host controller clears
     * the Run/Stop bit to halt the controller.
     * The host system error can be catastrophic
     * (mastor abort) or non-catastrophic (parity error).
     * In the case of catastrophic error, nothing can be done.
     * In the case of non-catastrophic error, the host controller can
     * proceed with the further operation. Here only the
     * non-catastrophic error is handled(The Run/Stop bit is set to run
     * the host controller). There is no indication from
     * the host controller whether this interrupt has occured due to
     * catastrophic or non-catastrophic error. So, there is no check
     * done here before setting the Run/Stop bit.
     */

    USB_EHCD_SET_BIT(pHCDData,
                     USBCMD,
                     RS);
    
    /* Post a message to report error */

    usbMsgPost(USBMSG_HCD_EHCD_ERROR,
               pHCDData->pDev,
               (void *)((unsigned long)pHCDData->uBusIndex));

    return;
    }/* End of usbEhcdHostSystemErrorHandler() */

/*******************************************************************************
*
* usbEhcdInterruptHandler - handle EHCI interrupts
*
* This is the task entry function which handles the EHCI interrupts.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbEhcdInterruptHandler
    (
    pUSB_EHCD_DATA pHCDData
    )
    {
    /* To hold the status of the interrupt */

    UINT32 uInterruptStatus = 0;

    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT(
        USB_EHCI_WV_EVENT_HANDLER,
        "usbEhcdInterruptHandler() starts",
        USB_EHCD_WV_FILTER);

    /* Check the validity of the parameter */

    if (NULL == pHCDData)
        {
        USB_EHCD_ERR("usbEhcdInterruptHandler - Parameter not valid\n",
            0, 0, 0, 0, 0, 0);

        return;
        }

    /* The InterruptEvent should be valid upon here */

    if (pHCDData->InterruptEvent == NULL)
        {
        USB_EHCD_ERR( "usbEhcdInterruptHandler - InterruptEvent not valid\n",
            0, 0, 0, 0, 0, 0);

        return;
        }

    /*
     * This is an infinite loop which does the following
     * 1. Waits on the event to be signalled from ISR
     * 2. Handles the interrupt which has occured.
     * 3. Step 1 and 2 are repeated until the thread dies, that is during
     *    Host Controller unregistration.
     */

    while (TRUE)
        {

        /* Wait on the signalling of the event */

        if (OS_WAIT_FOR_EVENT(pHCDData->InterruptEvent, OS_WAIT_INFINITE) != OK)
            {
            USB_EHCD_ERR("FATAL : usbEhcdInterruptHandler - "
                    "Wait for InterruptEvent got ERROR! Exiting!!\n",
                    0, 0, 0, 0, 0, 0);
            break;
            }

        USB_EHCD_VDBG("usbEhcdInterruptHandler - Received an interrupt event\n",
            0, 0, 0, 0, 0, 0);

        /* Read the contents of the interrupt status */

        uInterruptStatus = pHCDData->uInterruptStatus;

        /* If there are no interrupts observed, continue */

        if (0 == (pHCDData->uInterruptStatus & USB_EHCD_USBSTS_INTERRUPT_MASK))
            {
            /* Do not take up the CPU too much */

            OS_RESCHEDULE_THREAD();

            continue;
            }

        /* Check the type of interrupt and handle the interrupt */

        if (0 != (pHCDData->uInterruptStatus &
                USB_EHCD_USBSTS_PORT_CHANGE_DETECT_MASK))
            {
            USB_EHCD_DBG(
                "usbEhcdInterruptHandler - "
                "Port Status Change detected on EHCI %d\n",
                pHCDData->uBusIndex, 0, 0, 0, 0, 0);

#ifdef USE_SPINLOCKS
            /*
             * Lock the interrupts with a spinlock
             */

            SPIN_LOCK_ISR_TAKE
                (&spinLockIsrEhcd[pHCDData->uBusIndex]);

            /* Clear corresponding bit in the interrupt status variable */

            pHCDData->uInterruptStatus &=
                    (~USB_EHCD_USBSTS_PORT_CHANGE_DETECT_MASK);

            /* Unlock the interrupts */

            SPIN_LOCK_ISR_GIVE
                 (&spinLockIsrEhcd[pHCDData->uBusIndex]);
#else
            vxAtomicNand((atomic_t *)&pHCDData->uInterruptStatus,
                            USB_EHCD_USBSTS_PORT_CHANGE_DETECT_MASK);
#endif
            /* Call the function to handle the port change */

            usbEhcdPortChangeHandler(pHCDData);
            }

        /* Check for normal USB interrupts such as request completion */

        if (0 != (pHCDData->uInterruptStatus & USB_EHCD_USBSTS_USBINT_MASK))
            {
            USB_EHCD_VDBG("usbEhcdInterruptHandler - USB transfer completion\n",
                0, 0, 0, 0, 0, 0);

            /* Lock the interrupts */

#ifdef USE_SPINLOCKS
             SPIN_LOCK_ISR_TAKE (&spinLockIsrEhcd[pHCDData->uBusIndex]);

            /* Clear corresponding bit in the interrupt status variable */

            pHCDData->uInterruptStatus &= (~USB_EHCD_USBSTS_USBINT_MASK);

            /* Unlock the interrupts */

             SPIN_LOCK_ISR_GIVE (&spinLockIsrEhcd[pHCDData->uBusIndex]);
#else
            vxAtomicNand((atomic_t *)&pHCDData->uInterruptStatus,
                            USB_EHCD_USBSTS_USBINT_MASK);
#endif
            /* Call the function to handle the transfer completion */

            usbEhcdProcessTransferCompletion(pHCDData);
            }

        /* Check for error interrupts */

        if (0 != (pHCDData->uInterruptStatus & USB_EHCD_USBSTS_USBERRINT_MASK))
            {
            USB_EHCD_DBG("usbEhcdInterruptHandler - USB transfer error\n",
                0, 0, 0, 0, 0, 0);
#ifdef USE_SPINLOCKS
            /* lock the interrupts */

            SPIN_LOCK_ISR_TAKE
                 (&spinLockIsrEhcd[pHCDData->uBusIndex]);

            /* Clear corresponding bit in the interrupt status variable */

            pHCDData->uInterruptStatus &= (~USB_EHCD_USBSTS_USBERRINT_MASK);

            /* Unlock the interrupts */

            SPIN_LOCK_ISR_GIVE
                 (&spinLockIsrEhcd[pHCDData->uBusIndex]);
#else
            vxAtomicNand((atomic_t *)&pHCDData->uInterruptStatus,
                            USB_EHCD_USBSTS_USBERRINT_MASK);
#endif
            /* Call the function to handle the transfer completion */

            usbEhcdProcessTransferCompletion(pHCDData);
            }

        /* If there are any interrupts pending, handle them. Else continue */

        if (0 == pHCDData->uInterruptStatus)
             continue;

        /* Check for interrupts for ASYNC pipe clean up */

        if (0 != (pHCDData->uInterruptStatus &
                  USB_EHCD_USBSTS_INTERRUPT_ON_ASYNC_ADVANCE_MASK))
            {
            USB_EHCD_DBG("Cleaning up the asynch pipes....\n",
                0, 0, 0, 0, 0, 0);

#ifdef USE_SPINLOCKS
            /* Lock the interrupts */

            SPIN_LOCK_ISR_TAKE
                        (&spinLockIsrEhcd[pHCDData->uBusIndex]);

            /* Clear corresponding bit in the interrupt status variable */

            pHCDData->uInterruptStatus &=
                    (~USB_EHCD_USBSTS_INTERRUPT_ON_ASYNC_ADVANCE_MASK);

            /* Unlock the interrupts */

            SPIN_LOCK_ISR_GIVE
                (&spinLockIsrEhcd[pHCDData->uBusIndex]);
#else
            vxAtomicNand((atomic_t *)&pHCDData->uInterruptStatus,
                            USB_EHCD_USBSTS_INTERRUPT_ON_ASYNC_ADVANCE_MASK);
#endif
            /* Call the function to handle the cleanup function for pipe */

            usbEhcdCleanupAsynchPipes(pHCDData);
            }

        /* Check for interrupts for ISOC pipe clean up */

        if (0 != (pHCDData->uInterruptStatus &
                  USB_EHCD_USBSTS_FRAME_LIST_ROLLOVER_MASK))
            {
            USB_EHCD_DBG("Cleaning up the periodic pipes....\n",
                0, 0, 0, 0, 0, 0);

#ifdef USE_SPINLOCKS
            /* Lock the interrupts */

            SPIN_LOCK_ISR_TAKE (&spinLockIsrEhcd[pHCDData->uBusIndex]);

            /* Clear corresponding bit in the interrupt status variable */

            pHCDData->uInterruptStatus &=
                            (~USB_EHCD_USBSTS_FRAME_LIST_ROLLOVER_MASK);

            /* Unlock the interrupts */

            SPIN_LOCK_ISR_GIVE (&spinLockIsrEhcd[pHCDData->uBusIndex]);
#else
            vxAtomicNand((atomic_t *)&pHCDData->uInterruptStatus,
                          USB_EHCD_USBSTS_FRAME_LIST_ROLLOVER_MASK);
#endif
            /* Call the function to handle the cleanup function for pipe */

            usbEhcdCleanupPeriodicPipes(pHCDData);
            }

        /* Check for system error interrupt */

        if (0 != (pHCDData->uInterruptStatus &
                  USB_EHCD_USBSTS_HOST_SYSTEM_ERROR_MASK))
            {
            USB_EHCD_ERR("Host system error....\n", 0, 0, 0, 0, 0, 0);
#ifdef USE_SPINLOCKS
            /* Lock the interrupts */

            SPIN_LOCK_ISR_TAKE
                        (&spinLockIsrEhcd[pHCDData->uBusIndex]);

            /* Clear corresponding bit in the interrupt status variable */

            pHCDData->uInterruptStatus &=
                                (~USB_EHCD_USBSTS_HOST_SYSTEM_ERROR_MASK);

            /* Unlock the interrupts */

            SPIN_LOCK_ISR_GIVE
                        (&spinLockIsrEhcd[pHCDData->uBusIndex]);
#else
            vxAtomicNand((atomic_t *)&pHCDData->uInterruptStatus,
                            USB_EHCD_USBSTS_HOST_SYSTEM_ERROR_MASK);
#endif

            /* Call the function to handle the host system error */

            usbEhcdHostSystemErrorHandler(pHCDData);
            }
        }/* End of while () */

    return;
    }/* End of function usbEhcdInterruptHandler() */


/*********************** End of file EHCD_EventHandler.c***********************/

