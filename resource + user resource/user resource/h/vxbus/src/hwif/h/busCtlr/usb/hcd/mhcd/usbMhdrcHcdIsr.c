/* usbMhdrcHcdIsr.c - USB MHCD Interrupt Handler */

/*
 * Copyright (c) 2009-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01r,03may13,wyy  Remove compiler warning (WIND00356717)
01q,05jan13,ljg  Add CPPI DMA support for MHDRC TCD (WIND00398723)
01p,17feb12,s_z  Fix 15+ devices pending issue by correcting endpoint channels
                 usage (WIND00333840)
01o,15sep11,m_y  using one structure to describe HSDMA and CPPIDMA
01n,21jun11,jws  added include <usbMhdrcCppiDma.h> and function relevant CPPI DMA
01m,28mar11,w_x  Save local copy for interrupt status (WIND00262862)
01l,23mar11,w_x  Rename USB_MHDRC_HCD_VBUSERROR_RECOVERY_MAX
01k,08mar11,w_x  Fix cases when connect/disconnect recorded at the same time
01j,07jan11,ghs  Clean up compile warnings (WIND00247082)
01i,14dec10,ghs  Drive session bit regularly (WIND00244972)
01h,16aug10,w_x  VxWorks 64 bit audit and warning removal
01g,03jun10,s_z  Debug macro changed, Add more debug message
                 polling isr routine avoid missing interrupt (WIND00214719)
01f,16apr10,s_z  Support self-recovery when power low
01e,13mar10,s_z  Add Inventra DMA supported on OMAP3EVM
01d,02feb10,s_z  Code cleaning
01c,17dec09,s_z  Fix Interrupt In issue with hub and add ISO transaction support
01b,04nov09,s_z  Rewrite for data structure change
01a,13jul09,j_x  initial version
*/

/*
DESCRIPTION
This file defines the interrupt handler for the USB MHCD (Mentor Graphics Host
Controller Driver).

INCLUDE FILES: usb/usb.h usb/usbOsal.h, usbMhdrc.h, usbMhdrcIsr.h,
               usbMhdrcRootHub.h, usbMhdrcHcdCore.h, usbMhdrcHcdSched.h,
               usbMhdrcHsDma.h, usbMhdrcCppiDma.h, usbMhdrcHcdDebug.h
*/

/* includes */

#include <usb/usb.h>
#include <usb/usbOsal.h>
#include "usbMhdrc.h"
#include "usbMhdrcHcdIsr.h"
#include "usbMhdrcHcdRootHub.h"
#include "usbMhdrcHcdCore.h"
#include "usbMhdrcHcdSched.h"
#include "usbMhdrcHsDma.h"
#include "usbMhdrcCppiDma.h"
#include "usbMhdrcHcdDebug.h"

/*******************************************************************************
*
* usbMhdrcHcdRootHubPortDisconnect - process root hub port disconnect
*
* This function processes the connect of the root hub.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
*/

void usbMhdrcHcdRootHubPortDisconnect
    (
    pUSB_MHCD_DATA pHCDData
    )
    {

    /* Parameter verification */

    if (NULL == pHCDData )
        {
        /* Debug print */

        USB_MHDRC_ERR("usbMhdrcHcdRootHubPortDisconnect(): "
                     "Invalid parameter, pHCDData is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }

    /* Update the Root hub port status and status change information */

    semTake(pHCDData->pHcdSynchMutex, WAIT_FOREVER);

    pHCDData->RHData.pRootHubPortStatus[0].uRHPortStatus =
        (UINT16)(pHCDData->RHData.pRootHubPortStatus[0].uRHPortStatus &
        (UINT16)(~(USB_HUB_STS_PORT_CONNECTION
                  | USB_HUB_STS_PORT_ENABLE
                  | USB_HUB_STS_PORT_LOW_SPEED
                  | USB_HUB_STS_PORT_HIGH_SPEED
                  | USB_HUB_STS_PORT_STATUS_TEST)));

    pHCDData->RHData.pRootHubPortStatus[0].uRHPortChangeStatus |=
        USB_HUB_C_PORT_CONNECTION;

    semGive(pHCDData->pHcdSynchMutex);

    if (pHCDData->pMHDRC->uFrameNumChangeFlag)
        {
        pHCDData->pMHDRC->uFrameNumChangeFlag = 0;
        }

    usbMhdrcHcdCopyRootHubInterruptData(pHCDData, (1 << 1));

    USB_MHDRC_WARN("usbMhdrcHcdRootHubPortDisconnect(): "
                 "Disconnect changed \n",
                 1, 2, 3, 4, 5 ,6);

    return;
    }


/*******************************************************************************
*
* usbMhdrcHcdRootHubPortConnect - process root hub port connect
*
* This function processes the connect of the root hub.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
*/

void usbMhdrcHcdRootHubPortConnect
    (
    pUSB_MHCD_DATA pHCDData
    )
    {
    pUSB_MUSBMHDRC  pMHDRC;
    UINT8 uReg8;

    /* Parameter verification */

    if (NULL == pHCDData)
        {
        /* Debug print */

        USB_MHDRC_ERR("usbMhdrcHcdRootHubPortConnect(): "
                     "Invalid parameter, pHCDData is NULL \n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }

    /* Update the Root hub port status and status change information */

    semTake(pHCDData->pHcdSynchMutex, WAIT_FOREVER);

    if (!(pHCDData->RHData.pRootHubPortStatus[0].uRHPortStatus &
        USB_HUB_STS_PORT_CONNECTION))
        pHCDData->RHData.pRootHubPortStatus[0].uRHPortChangeStatus |=
             USB_HUB_C_PORT_CONNECTION;

    pHCDData->RHData.pRootHubPortStatus[0].uRHPortStatus |=
            USB_HUB_STS_PORT_CONNECTION;

    semGive(pHCDData->pHcdSynchMutex);
    usbMhdrcHcdCopyRootHubInterruptData(pHCDData, (1 << 1));

    pMHDRC = pHCDData->pMHDRC;

    uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_POWER);

    /* If in reset, delay about 50ms to let the link stable */

    if (uReg8 & USB_MHDRC_POWER_RESET)
        {
        USB_MHDRC_WARN("usbMhdrcHcdRootHubPortConnect - in RESET mode\n",
            1, 2, 3, 4, 5 ,6);

        uReg8 = (UINT8)(uReg8 & ~USB_MHDRC_POWER_RESET);

        OS_DELAY_MS(50);

        USB_MHDRC_REG_WRITE8(pMHDRC, USB_MHDRC_POWER, uReg8);
        }

    USB_MHDRC_WARN("usbMhdrcHcdRootHubPortConnect(): "
                 "Connect changed \n",
                 1, 2, 3, 4, 5 ,6);

    return;
    }


/*******************************************************************************
*
* usbMhdrcHcdUsbCoreInterruptHandler - handle MHCI core usb interrupts
*
* This routine processes MHCI core usb interrupts, such as connection,
* disconnection and vbus error, etc.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcHcdUsbCoreInterruptHandler
    (
    pUSB_MUSBMHDRC pMHDRC,
    UINT8          uDevctl,
    UINT8          uPower,
    UINT8          uHcdUsbIrqStatus
    )
    {
    pUSB_MHCD_DATA pHCDData;

    /* Parameter verification */

    if ((NULL == pMHDRC) ||
        (NULL == pMHDRC->pDev) ||
        (NULL == pMHDRC->pHCDData))
        {
        /* Debug print */

        USB_MHDRC_ERR("usbMhdrcHcdUsbCoreInterruptHandler(): "
                     "Invalid parameter, %s is NULL \n",
                     ((NULL == pMHDRC) ? "pMHDRC" :
                     "pMHDRC->pDev"),
                     2, 3, 4, 5 ,6);

        return;
        }

    pHCDData = pMHDRC->pHCDData;

    /* resume interrupt process */

    if (uHcdUsbIrqStatus & USB_MHDRC_INTR_RESUME)
        {
        if (uDevctl & USB_MHDRC_DEVCTL_HM)
            {
            uPower =
                (UINT8)
                (uPower & ~(USB_MHDRC_POWER_SUSPENDM | USB_MHDRC_POWER_ENSUSPM));

            USB_MHDRC_REG_WRITE8 (pMHDRC,
                                USB_MHDRC_POWER,
                                uPower | USB_MHDRC_POWER_RESUME);
            }

        USB_MHDRC_WARN("usbMhdrcHcdUsbCoreInterruptHandler(): "
                     "USB_MHDRC_INTR_RESUME - uHcdUsbIrqStatus 0x%X\n",
                     uHcdUsbIrqStatus, 2, 3, 4, 5 ,6);
        }

    /* SESSREQ detected interrupt */

    if (uHcdUsbIrqStatus & USB_MHDRC_INTR_SESSREQ)
        {
        USB_MHDRC_REG_WRITE8 (pMHDRC,
                             USB_MHDRC_DEVCTL,
                             uDevctl | USB_MHDRC_DEVCTL_SESSION);

        USB_MHDRC_WARN("usbMhdrcHcdUsbCoreInterruptHandler(): "
                     "USB_MHDRC_INTR_SESSREQ\n",
                     1, 2, 3, 4, 5 ,6);
        }

    /* Vbus error */

    if (uHcdUsbIrqStatus & USB_MHDRC_INTR_VBUSERROR)
        {
        USB_MHDRC_WARN("usbMhdrcHcdUsbCoreInterruptHandler(): "
                     "USB_MHDRC_INTR_VBUSERROR\n",
                     1, 2, 3, 4, 5 ,6);
        USB_MHDRC_REG_WRITE8 (pMHDRC,
                             USB_MHDRC_DEVCTL,
                             uDevctl | USB_MHDRC_DEVCTL_SESSION);

        /*
         * This interrupt happends in two case
         *
         * Case 1.
         * Power ship just at the beginning when devices plugged in.
         * This is acceptable, retry several times to see if it can recover.
         *
         * Case 2.
         * On some board such as Omap3530evm Rev D, the power supply is
         * limitted of 100mA. So exteral powered hub will be needed to support
         * bind of devices. But if you plugging out the power supply of the
         * external hub, the host controller will enter into suspend mode.
         * When this happended, the host controller will stop seccion and the
         * frame numbe register will stoped, and all the devices can not used.
         *
         * Since all the device can not used, resume from the devices is
         * impossible. The best way is remove all the devices, and wait for
         * the power recovery. So in this case, if you plug the power to the hub
         * again, all the devices can be detected too.
         *
         */

        pMHDRC->uVbusError ++;
        if (pMHDRC->uVbusError >= USB_MHDRC_VBUSERROR_RECOVERY_MAX)
            {
            pMHDRC->uVbusError = 0;

            /* If you do not want the host recovery itself, #if 0 */

            USB_MHDRC_WARN("Power Low! Please check the power and wait for "
                          "10s for recovery\n",
                          1,2,3,4,5,6);

            usbMhdrcHcdRootHubPortDisconnect(pMHDRC->pHCDData);

            usbMhdrcHcdTransferTaskFeed(pMHDRC->pHCDData,
                   NULL,
                   USB_MHCD_TRANSFER_CMD_SHEDULE_PROCESS,
                   0);
            }

        }

    /* Sof interrupt */

    if (uHcdUsbIrqStatus & USB_MHDRC_INTR_SOF)
        {
        USB_MHDRC_VDBG("usbMhdrcHcdUsbCoreInterruptHandler(): "
                      "USB_MHDRC_INTR_SOF\n",
                       1, 2, 3, 4, 5 ,6);

         if (pMHDRC->uFrameNumChangeFlag == 0)
            {
            pMHDRC->uFrameNumChangeFlag = 1;
            }
#if 0
         usbMhdrcHcdTransferTaskFeed(pMHDRC,
                NULL,
                USB_MHCD_TRANSFER_CMD_SHEDULE_PROCESS,
                0);
#endif
        }

    /*
     * If connect and disconnect recorded at the same time,
     * handle disconnect first! This is to workaround the problem
     * while doing long time OTG/HNP testing when such situations
     * happen!
     */

    if ((uHcdUsbIrqStatus & USB_MHDRC_INTR_CONNECT) &&
        (uHcdUsbIrqStatus & USB_MHDRC_INTR_DISCONNECT) &&
        (pMHDRC->bIsConnected == TRUE))
        {
        UINT8 uReg8;

        /* Disconnect interrupt */

        USB_MHDRC_WARN("usbMhdrcHcdUsbCoreInterruptHandler(): "
                     "USB_MHDRC_INTR_DISCONNECT - uHcdUsbIrqStatus 0x%X\n",
                     uHcdUsbIrqStatus, 2, 3, 4, 5 ,6);

        usbMhdrcHcdRootHubPortDisconnect(pMHDRC->pHCDData);

        usbMhdrcHcdTransferTaskFeed(pMHDRC->pHCDData,
                NULL,
                USB_MHCD_TRANSFER_CMD_SHEDULE_PROCESS,
                0);

        uHcdUsbIrqStatus = (UINT8)(uHcdUsbIrqStatus & ~USB_MHDRC_INTR_DISCONNECT);

        /* Start a RESET here which will be ended in the connection handling */

        uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_POWER);
        uReg8 |= USB_MHDRC_POWER_RESET;
        USB_MHDRC_REG_WRITE8(pMHDRC, USB_MHDRC_POWER, uReg8);
        }

    /* Connection interrupt */

    if (uHcdUsbIrqStatus & USB_MHDRC_INTR_CONNECT)
        {
        USB_MHDRC_WARN("usbMhdrcHcdUsbCoreInterruptHandler(): "
                     "USB_MHDRC_INTR_CONNECT - uHcdUsbIrqStatus 0x%X\n",
                     uHcdUsbIrqStatus, 2, 3, 4, 5 ,6);

        usbMhdrcHcdRootHubPortConnect(pMHDRC->pHCDData);
        }

    /* Disconnect interrupt */

    if (uHcdUsbIrqStatus & USB_MHDRC_INTR_DISCONNECT)
        {
        USB_MHDRC_WARN("usbMhdrcHcdUsbCoreInterruptHandler(): "
                     "USB_MHDRC_INTR_DISCONNECT - uHcdUsbIrqStatus 0x%X\n",
                     uHcdUsbIrqStatus, 2, 3, 4, 5 ,6);

        usbMhdrcHcdRootHubPortDisconnect(pMHDRC->pHCDData);

        usbMhdrcHcdTransferTaskFeed(pMHDRC->pHCDData,
                NULL,
                USB_MHCD_TRANSFER_CMD_SHEDULE_PROCESS,
                0);
        }

    /* Babble error */

    if ((uHcdUsbIrqStatus & USB_MHDRC_INTR_BABBLE) &&
        (uDevctl & USB_MHDRC_DEVCTL_HM))
        {
        USB_MHDRC_WARN("usbMhdrcHcdUsbCoreInterruptHandler(): "
                     "USB_MHDRC_INTR_BABBLE uDevctl %p uPower %p\n",
                     uDevctl, uPower, 3, 4, 5 ,6);

        }

    /* Suspend */

    if (uHcdUsbIrqStatus & USB_MHDRC_INTR_SUSPEND)
        {
        USB_MHDRC_WARN("usbMhdrcHcdUsbCoreInterruptHandler(): "
                     "USB_MHDRC_INTR_SUSPEND - uHcdUsbIrqStatus 0x%X\n",
                     uHcdUsbIrqStatus, 2, 3, 4, 5 ,6);
        }

    return;
    }


/*******************************************************************************
*
* usbMhdrcHcdTransactionInterruptHandler - handle endpoints tranaction interrupts
*
* This routine deals with all the tranaction interrupt all the endpoints, include
* Ep 0.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void  usbMhdrcHcdTransactionInterruptHandler
    (
    pUSB_MUSBMHDRC pMHDRC,
    UINT16         uHcdRxIrqStatus,
    UINT16         uHcdTxIrqStatus
    )
    {
    UINT8 i;
    pUSB_MHCD_DATA pHCDData;

    /* Parameter verification */

    if ((NULL == pMHDRC) || (NULL == pMHDRC->pHCDData))
        {
        /* Debug print */

        USB_MHDRC_ERR("usbMhdrcTransactionInterruptHandle(): "
                     "Invalid parameter, pMHDRC or pMHDRC->pHCDData is NULL\n",
                      1, 2, 3, 4, 5 ,6);

        return;
        }

    pHCDData = pMHDRC->pHCDData;

    /* Handle endpoint 0 first */

    if (uHcdTxIrqStatus & 1)
        {
        USB_MHDRC_VDBG("usbMhdrcTransactionInterruptHandle(): "
                      "Enpoint 0 interrupt\n",
                      1, 2, 3, 4, 5 ,6);

        usbMhdrcHcdEp0InterruptHandle(pMHDRC->pHCDData);
        }

    /* Deal with other Tx endpoints */

    for (i = 1; i <= pMHDRC->uNumEps; i ++)
        {
        if (uHcdTxIrqStatus & (0x1 << i))
            {
            USB_MHDRC_VDBG("usbMhdrcTransactionInterruptHandle(): "
                          "Tx Enpoint %p interrupt\n",
                           i, 2, 3, 4, 5 ,6);

            usbMhdrcHcdTxInterruptHandle(pMHDRC->pHCDData, i);
            }
        }

     /* Deal with other Rx endpoints */

    for (i = 1; i <= pMHDRC->uNumEps; i ++)
        {
        if (uHcdRxIrqStatus & (0x1 << i))
            {
            USB_MHDRC_VDBG("usbMhdrcTransactionInterruptHandle(): "
                          "Rx Enpoint %p interrupt\n",
                          i, 2, 3, 4, 5 ,6);

            usbMhdrcHcdRxInterruptHandle(pMHDRC->pHCDData, i);
            }
        }

    return;
    }


/*******************************************************************************
*
* usbMhdrcHcdInterruptProcessHandler - thread for handling USB MHCI interrupts
*
* This function is spawn as thread for handling USB MHCI controller interrupts.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcHcdInterruptProcessHandler
    (
    pUSB_MHCD_DATA pHCDData
    )
    {
    volatile UINT8 uDevctl = 0;
    volatile UINT8 uPower  = 0;
    pUSB_MUSBMHDRC pMHDRC;
    pUSB_MHDRC_DMA_DATA pDMAData;
    volatile UINT16  uHcdTxIrqStatus;
    volatile UINT16  uHcdRxIrqStatus;
    volatile UINT8   uHcdUsbIrqStatus;

    /* Parameter verification */

    if ((NULL == pHCDData) || (NULL == pHCDData->pMHDRC))
        {
        USB_MHDRC_ERR("usbMhdrcHcdInterruptProcessHandler(): "
                     "Invalid parameter, pHCDData or pMHDRC NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }

    /* Get the common hardware data structure */

    pMHDRC = pHCDData->pMHDRC;
    pDMAData = &(pMHDRC->dmaData);

    /* Call the MHCI interrupt infinitely */

    while (1)
        {
        /* Wait for the ISR event */

        if (OS_WAIT_FOR_EVENT(pHCDData->isrEvent,
            OS_CONVERT_MILLISECONDS_TO_WAIT_VALUE(pHCDData->isrPollingRateMs)) != OK)
            {
            /*
             * We found the interrupt missing issue on Omap3530. Davinic may have
             * no such problem, since it uses the shadow Interrupt Status and
             * Control registers.
             *
             * The scenario is:
             * On Omap3530, when 2 channals are both used as Interrupt IN
             * transaction, and start the 1st then the 2nd. In the bus, both
             * IN token for different devices are seen. But if the 2nd
             * channel get the data, it may not trigger the interrupt
             * event, even the channel's interrupt status has been set in
             * INTRRX register.
             *
             * The workaround is polling the usb interrupt registers.By calling
             * the ISR routine directly which forces to read and update the
             * interrupt status, and if the status is updated, the isrEvent
             * is released, so the next wait for that event will get returned
             * immediately(so we use the continue statement below).
             */

            uDevctl = USB_MHDRC_REG_READ8(pMHDRC,
                                     USB_MHDRC_DEVCTL);

            /* Set the SESSION bit */

            USB_MHDRC_REG_WRITE8 (pMHDRC,
                                  USB_MHDRC_DEVCTL,
                                  uDevctl | USB_MHDRC_DEVCTL_SESSION);

            pMHDRC->PlatformData.pUsbCoreIsr(pMHDRC);

            continue;
            }

        /* Save a local copy of interrupt status for further processing */

        SPIN_LOCK_ISR_TAKE(&pMHDRC->spinLock);

        uHcdUsbIrqStatus = pHCDData->uHcdUsbIrqStatus;
        uHcdTxIrqStatus = pHCDData->uHcdTxIrqStatus;
        uHcdRxIrqStatus = pHCDData->uHcdRxIrqStatus;

        pHCDData->uHcdUsbIrqStatus = 0;
        pHCDData->uHcdTxIrqStatus = 0;
        pHCDData->uHcdRxIrqStatus = 0;

        SPIN_LOCK_ISR_GIVE (&pMHDRC->spinLock);

        /* Call the interrupt handler */

        if (uHcdTxIrqStatus ||
            uHcdRxIrqStatus ||
            uHcdUsbIrqStatus ||
            pDMAData->uIrqStatus)
            {
            uDevctl = USB_MHDRC_REG_READ8(pMHDRC,
                                     USB_MHDRC_DEVCTL);

            uPower = USB_MHDRC_REG_READ8(pMHDRC,
                                     USB_MHDRC_POWER);

            USB_MHDRC_VDBG("usbMhdrcProcessInterrupts(): IRQ tx %p rx %p usb %p \n",
                          uHcdTxIrqStatus,
                          uHcdRxIrqStatus,
                          uHcdUsbIrqStatus,
                          4,5,6);

            /* Handle usb core irqs */

            if (uHcdUsbIrqStatus)
                {
                usbMhdrcHcdUsbCoreInterruptHandler(pMHDRC, uDevctl, uPower, uHcdUsbIrqStatus);
                }

            /* Handle all the endpoint tranaction irqs */

            if ((uHcdRxIrqStatus) ||
                (uHcdTxIrqStatus))
                {
                usbMhdrcHcdTransactionInterruptHandler(pMHDRC, uHcdRxIrqStatus, uHcdTxIrqStatus);
                }

            /* Handle DMA irqs */

            if ((pMHDRC->bDmaEnabled) &&
                (pDMAData->uIrqStatus) &&
                (pDMAData->pDmaIrqHandler))
                {
                (pDMAData->pDmaIrqHandler)(pMHDRC);
                }

            }

        /*
         * Clear EOIR to acknowledge the completion of the USB core interrupt
         * for some platfrom
         */

        if (pMHDRC->PlatformData.pUsbIsrDone)
            {
            (pMHDRC->PlatformData.pUsbIsrDone)((void *)pMHDRC);
            }

        }
    }

/*******************************************************************************
*
* usbMhdrcHcdHsDmaInterruptHandler - handle MHCI DMA interrupts
*
* This routine processes MHCI DMA interrupts.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcHcdHsDmaInterruptHandler
    (
    pUSB_MUSBMHDRC  pMHDRC,
    UINT8           uChannel
    )
    {
    pUSB_MHCD_REQUEST_INFO      pRequestInfo = NULL;
    pUSB_MHCD_DATA              pHCDData = NULL;
    pUSB_MHDRC_DMA_DATA         pDMAData = NULL;
    UINT32 uDmaCtl     = 0;
    ULONG  uDmaAddress = 0;
    UINT32 uDmaCount   = 0;
    UINT16 uCsrReg16   = 0;
    UINT8  uEpIndex    = 0;


    /* Parameter verification */

    if ((NULL == pMHDRC) || (NULL == pMHDRC->pHCDData))
        {
        /* Debug print */

        USB_MHDRC_ERR("usbMhdrcHcdHsDmaInterruptHandler(): "
                     "Invalid parameter, pHCDData is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }

    pHCDData = (pUSB_MHCD_DATA)pMHDRC->pHCDData;
    pDMAData = &(pMHDRC->dmaData);
    pDMAData->pDmaChannelRelease(pDMAData, uChannel);

    uDmaCtl = USB_MHDRC_REG_READ32(pMHDRC,
                                         USB_MHDRC_HS_DMA_CTNL_CH(uChannel));

    uDmaAddress = USB_MHDRC_REG_READ32(pMHDRC,
                                         USB_MHDRC_HS_DMA_ADDR_CH(uChannel));

    if (uDmaCtl & USB_MHDRC_HS_DMA_CTNL_ERR)
        {
        USB_MHDRC_ERR("usbMhdrcHcdHsDmaInterruptHandler(): DMA ERR\n",
                     1,2,3,4,5,6);
        }
    else
        {

        /* Find the related pRequestInfo */

         pRequestInfo = (pUSB_MHCD_REQUEST_INFO)
                          pDMAData->dmaChannel[uChannel].pRequest;

        if ((pRequestInfo == NULL) ||
            (pRequestInfo->pHCDPipe == NULL))
            {
             USB_MHDRC_ERR("usbMhdrcHcdHsDmaInterruptHandler(): "
                          "pRequestInChannel NULL\n",
                          pRequestInfo,2,3,4,5,6);
            }
        else
            {
            uDmaCount = (UINT32)(uDmaAddress -
                                 (ULONG)pRequestInfo->pCurrentBuffer);
            uEpIndex  = pRequestInfo->pHCDPipe->uEndPointEngineAsigned;

            if (pRequestInfo->pHCDPipe->uEndpointDir == USB_DIR_IN) /* In Ep */
                {
                pRequestInfo->uActLength += uDmaCount;

                uCsrReg16 = USB_MHDRC_REG_READ16(pMHDRC,
                            USB_MHDRC_HOST_RXCSR_EP(uEpIndex));

                USB_MHDRC_REG_WRITE16(pMHDRC,
                                 USB_MHDRC_HOST_RXCSR_EP(uEpIndex),
                                 (UINT16)(uCsrReg16 & ~ USB_MHDRC_HOST_RXCSR_RXPKTRDY));

                /* Complited the transaction */

                if ((pRequestInfo->uXferSize <= pRequestInfo->uActLength) ||
                    ((uDmaCount < pRequestInfo->pHCDPipe->uMaximumPacketSize) &&
                     (pRequestInfo->pHCDPipe->uEndpointType != USB_ATTR_ISOCH)))
                    {
                    /* Stop the transaction and Call back the urb directly */

                    usbMhdrcHcdRequestTransferDone(pHCDData,
                                                   pRequestInfo,
                                                   USBHST_SUCCESS);

                    }
                else
                    {
                    usbMhdrcHcdTransferTaskFeed(pHCDData,
                            pRequestInfo,
                            USB_MHCD_TRANSFER_CMD_CHANNEL_START_PROCESS,
                            0);
                    }
                }
            else /* Out Ep */
                {
                pRequestInfo->uTransactionLength = uDmaCount;
                uCsrReg16 = USB_MHDRC_REG_READ16(pMHDRC,
                            USB_MHDRC_HOST_TXCSR_EP(uEpIndex));

                USB_MHDRC_REG_WRITE16(pMHDRC,
                                 USB_MHDRC_HOST_TXCSR_EP(uEpIndex),
                                 uCsrReg16 | USB_MHDRC_HOST_TXCSR_TXPKTRDY);
                }
            }
        }

    return;
    }

/*******************************************************************************
*
* usbMhdrcHcdCppiDmaCompletionTx - MHDRC HCD DMA Transmission Completion processes
*
* This routine processes MHDRC HCD CPPI DMA Transmission Completion
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcHcdCppiDmaCompletionTx
    (
    pUSB_MUSBMHDRC   pMHDRC,
    UINT32           txInterr
    )
    {
    pUSB_MHCD_DATA          pHCDData = NULL;
    pUSB_MHDRC_DMA_DATA     pDMAData = NULL;
    pUSB_MHCD_REQUEST_INFO  pRequest = NULL;
    pUSB_MHDRC_CPPIDMA_PKT_DESC  pCurrentPd;
    UINT32   index;
    UINT32   uDmaCount   = 0;
    UINT32   uReg32;
    UINT32   uPdAddr = 0;
    UINT16   csr;
    UINT8    epNum;

    pDMAData = &(pMHDRC->dmaData);
    pHCDData = (pUSB_MHCD_DATA)pMHDRC->pHCDData;

    for (index = 0; txInterr != 0; txInterr >>= 1, index++)
        {
        if (txInterr & 1)
            {
            epNum = (UINT8)(index + 1);

            /* Find the related pRequest */

            pRequest = (pUSB_MHCD_REQUEST_INFO) pDMAData->dmaChannel[index].pRequest;

            if ((pRequest == NULL) ||
                (pRequest->pHCDPipe == NULL))
                {
                USB_MHDRC_ERR("usbMhdrcHdcCppiDmaCompletionTx(): "
                              "pRequestInChannel NULL\n",
                              1, 2, 3, 4, 5, 6);
                continue;
                }

            for(;;)
                {
                /* Read  */

                uReg32 = USB_MHDRC_QMGR_REG_READ32(pMHDRC,
                              QMGR_QUEUE_CTRL_D(index + USB_MHDRC_CPPI_TXQ_START_NUM + pDMAData->uDmaOffset));

                uPdAddr = uReg32 & QMGR_QUEUE_DESC_PTR_MASK;

                pCurrentPd = (pUSB_MHDRC_CPPIDMA_PKT_DESC)
                              (USB_MHDRC_PHYS_TO_VIRT(uPdAddr));
                if(!pCurrentPd)
                    break;

                /* Extract data from received packet descriptor */

                epNum = pCurrentPd->epNum;
                uDmaCount = pCurrentPd->bufLen;

                semTake(pDMAData->pSynchMutex, WAIT_FOREVER);
                usbMhdrcCppiDmaPDPut(pDMAData->pCppi, pCurrentPd);
                semGive(pDMAData->pSynchMutex);

                /* Wait for data to be completely output FIFO */

                for(;;)
                    {
                    csr = USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_HOST_TXCSR_EP(epNum));
                    if(!(csr & USB_MHDRC_HOST_TXCSR_TXPKTRDY))
                        break;
                    }

                /* free the dma channel */

                pRequest->uTransactionLength = uDmaCount;

                usbMhdrcHcdTxInterruptHandle(pMHDRC->pHCDData, epNum);
                USB_MHDRC_VDBG("usbMhdrcHdcCppiDmaCompletionTx(): "
                               "Released HCD event 0x%x\n",
                               pHCDData->uHcdTxIrqStatus, 2, 3, 4, 5, 6);
                }
            }
        }
    }

/*******************************************************************************
*
* usbMhdrcHcdCppiDmaCompletionRx - MHDRC HCD DMA Receive Completion processes
*
* This routine processes MHDRC HCD CPPI DMA Receive Completion
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcHcdCppiDmaCompletionRx
    (
    pUSB_MUSBMHDRC   pMHDRC,
    UINT32           rxInterr
    )
    {
    pUSB_MHDRC_DMA_DATA          pDMAData = NULL;
    pUSB_MHCD_REQUEST_INFO       pRequest = NULL;
    pUSB_MHDRC_CPPIDMA_PKT_DESC  pCurrentPd;

    UINT32 uDmaCount   = 0;
    UINT16 uCsrReg16   = 0;
    UINT32 index;
    UINT32 uReg32;
    UINT8  epNum;

    pDMAData = &(pMHDRC->dmaData);

    for (index = 0; rxInterr != 0; rxInterr >>= 1, index++)
        {
        if (rxInterr & 1)
            {
            epNum = (UINT8)(index + 1);

            /* Find the related pRequest */

            pRequest = (pUSB_MHCD_REQUEST_INFO) pDMAData->dmaChannel[index].pRequest;

            if ((pRequest == NULL) ||
                (pRequest->pHCDPipe == NULL))
                {
                USB_MHDRC_ERR("usbMhdrcHcdCppiDmaCompletionRx: "
                               "pRequestInChannel NULL\n",
                               1, 2, 3, 4, 5, 6);
                continue;
                }

            for(;;)
                {

                /* queue_pop */

                uReg32 = USB_MHDRC_QMGR_REG_READ32(pMHDRC,
                     QMGR_QUEUE_CTRL_D(USB_MHDRC_CPPI_RXQ_START_NUM + pDMAData->uDmaOffset + index));

                pCurrentPd = (pUSB_MHDRC_CPPIDMA_PKT_DESC)
                    (USB_MHDRC_PHYS_TO_VIRT(uReg32 & QMGR_QUEUE_DESC_PTR_MASK));
                if(!pCurrentPd)
                    break;

                /* Extract data from received packet descriptor */

                epNum = pCurrentPd->epNum;
                uDmaCount = pCurrentPd->bufLen;

                semTake(pDMAData->pSynchMutex, WAIT_FOREVER);
                usbMhdrcCppiDmaPDPut(pDMAData->pCppi, pCurrentPd);
                semGive(pDMAData->pSynchMutex);

                /* Should free the dma channel set as free status */

                pRequest->uActLength += uDmaCount;

                uCsrReg16 = USB_MHDRC_REG_READ16(pMHDRC,
                                                 USB_MHDRC_HOST_RXCSR_EP(epNum));

                USB_MHDRC_REG_WRITE16(pMHDRC,
                                      USB_MHDRC_HOST_RXCSR_EP(epNum),
                                      (UINT16)(uCsrReg16 & ~ USB_MHDRC_HOST_RXCSR_RXPKTRDY));

                /* Complited the transaction */

                if ((pRequest->uXferSize <= pRequest->uActLength) ||
                    ((uDmaCount < pRequest->pHCDPipe->uMaximumPacketSize) &&
                    (pRequest->pHCDPipe->uEndpointType != USB_ATTR_ISOCH)))
                    {
                    /* Stop the transaction and Call back the urb directly */

                    usbMhdrcHcdRequestTransferDone((pUSB_MHCD_DATA)pMHDRC->pHCDData,
                                                    pRequest,
                                                    USBHST_SUCCESS);

                    USB_MHDRC_VDBG("usbMhdrcHdcCppiDmaCompletionRx(): "
                                       "invoked HcdRequestTransferDone\n",
                                        1, 2, 3, 4, 5, 6);

                    }
                else
                    {
                    USB_MHDRC_VDBG("usbMhdrcHdcCppiDmaCompletionRx(): "
                                   "invoked HcdTransferTaskFeed\n",
                                   1, 2, 3, 4, 5, 6);
                    usbMhdrcHcdTransferTaskFeed((pUSB_MHCD_DATA)pMHDRC->pHCDData,
                                                pRequest,
                                                USB_MHCD_TRANSFER_CMD_CHANNEL_START_PROCESS,
                                                0);
                    }

                }
            }
        }
    }

/*******************************************************************************
*
* usbMhdrcHcdCppiDmaInterruptHandler - handle MHDRC CPPI DMA interrupts
*
* This routine processes MHDRC CPPI DMA interrupts.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcHcdCppiDmaInterruptHandler
    (
    pUSB_MUSBMHDRC  pMHDRC,
    UINT8           uChannel
    )
    {
    UINT32 queueCompStat0;
    UINT32 queueCompStat1;
    UINT32 usbTxInter;
    UINT32 usbRxInter;

    /* Parameter verification */

    if ((NULL == pMHDRC) || (NULL == pMHDRC->pHCDData))
        {
        /* Debug print */

        USB_MHDRC_ERR("usbMhdrcHcdCppiDmaInterruptHandler(): "
                      "Invalid parameter, pHCDData is NULL\n",
                      1, 2, 3, 4, 5 ,6);

        return;
        }

    /* DMA completion status Read */

    if ((pMHDRC->uRegBase & USB_MHDRC_USBx_CHECK_ADR_MASK) == 0x000)
        {
        /* USB 0 */
        /* This field indicates the queue pending status for queues[95:64] */

        queueCompStat0 = USB_MHDRC_QMGR_REG_READ32(pMHDRC, QMGR_QUEUE_PEND_REG2);

        /* This field indicates the queue pending status for queues[127:96] */

        queueCompStat1 = USB_MHDRC_QMGR_REG_READ32(pMHDRC, QMGR_QUEUE_PEND_REG3);

        /* USB tx completion interrupt for ep1..15  (queue 93 - 107) */

        usbTxInter = ((queueCompStat0 & USB_MHDRC_QUERE_93_95) >> 29) |
                     ((queueCompStat1 & USB_MHDRC_QUEUE_96_107) << 3);

        /* USB rx completion interrupt for ep1..15 (Queue 109 - 123) */

        usbRxInter = ((queueCompStat1 & USB_MHDRC_QUEUE_109_123) >> 13);
        }
    else
        {
        /* USB 1 */
        /* This field indicates the queue pending status for queues[125:127] */

        queueCompStat0 = USB_MHDRC_QMGR_REG_READ32(pMHDRC, QMGR_QUEUE_PEND_REG3);

        /* This field indicates the queue pending status for queues[128155] */

        queueCompStat1 = USB_MHDRC_QMGR_REG_READ32(pMHDRC, QMGR_QUEUE_PEND_REG4);

        /* USB tx completion interrupt for ep1..15  (queue 125 - 139) */

        usbTxInter = ((queueCompStat0 & 0xE0000000) >> 29) |
                     ((queueCompStat1 & 0x00000FFF) << 3);

        /* USB rx completion interrupt for ep1..15 (Queue 141 - 123) */

        usbRxInter = ((queueCompStat1 & 0x07FFE000) >> 13);
        }

    if (usbTxInter)
        {
        usbMhdrcHcdCppiDmaCompletionTx(pMHDRC, usbTxInter);
        }

    if (usbRxInter)
        {
        usbMhdrcHcdCppiDmaCompletionRx(pMHDRC, usbRxInter);
        }

    return;
    }

