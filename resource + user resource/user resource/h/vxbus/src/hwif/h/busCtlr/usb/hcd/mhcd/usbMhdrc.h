/* usbMhdrc.h - USB MHDRC Common Header File */

/*
 * Copyright (c) 2010 - 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01l,03may13,wyy  Remove compiler warning (WIND00356717)
01k,17feb12,s_z  Fix 15+ devices pending issue by adding uLastPausedEp item
                 in structure usb_musbmhdrc (WIND00333840)
01j,15sep11,m_y  Using one structure to describe HSDMA and CPPIDMA
01i,22jun11,jws  Added "uUsbssBaseReg" elements to put USBSS register base address
01h,16may11,w_x  Record DMA channel number in USB_MHDRC_DMA_CHANNEL (WIND00276544)
01g,12apr11,m_y  Modify USB_MHDRC_DMA_DATA structure (WIND00265911)
01f,08apr11,w_x  Use USB_MHDRC_TASK_NAME_MAX for Coverity (WIND00264893)
01e,28mar11,w_x  Correct RX/TX interrupt mask (WIND00262862)
01d,23mar11,w_x  Add some definitions to avoid hard code
01c,20oct10,m_y  Modify structure USB_MUSBMHDRC
01b,17jun10,s_z  Add registers defines
01a,30may10,w_x  Written
*/

#ifndef __INCusbMhdrch
#define __INCusbMhdrch

#include <vxWorks.h>
#include <vxBusLib.h>
#include <spinLockLib.h>
#include <cacheLib.h>
#include <usb/usbPlatform.h>
#include <usb/usbOsal.h>

#ifdef __cplusplus
    extern "C" {
#endif

#define MHDRC_MALLOC(size)                    malloc((size))
#define MHDRC_FREE(pBfr)                      free ((char *) (pBfr))

#define USB_MHDRC_PHYS_TO_VIRT(adrs) CACHE_DRV_PHYS_TO_VIRT(&cacheDmaFuncs, adrs)
#define USB_MHDRC_VIRT_TO_PHYS(adrs) CACHE_DRV_VIRT_TO_PHYS(&cacheDmaFuncs, adrs)

#define USB_MHDRC_MAGIC_ALIVE                 (0x35303530)
#define USB_MHDRC_MAGIC_DEAD                  (0xAAAADEAD)

#define USB_MHDRC_TASK_NAME_MAX               (20)

/* Endpoint Index */

#define USB_MHDRC_ENDPT(n)                 (n)  /* Endpoint index n  */

/* number of endpoints */

#define USB_MHDRC_NUM_ENDPOINTS_RX         (0x10)
#define USB_MHDRC_NUM_ENDPOINTS_TX         (0x10)
#define USB_MHDRC_ENDPOINT_MAX             (16)

/* TODO : make the number of endpoints as config options */

#define USB_MHDRC_NUM_ENDPOINTS           (USB_MHDRC_NUM_ENDPOINTS_RX + \
                                           USB_MHDRC_NUM_ENDPOINTS_TX)

#define USB_MHDRC_MAXPSIZE_ENDPT_0        (0x40)

/* Bitmap for IN and OUT Endpoints Supported */

#define USB_MHDRC_TCD_OUT_ENDPOINT_BITMAP (0x0000FFFF)  /* bitmap for Out endpoint */
#define USB_MHDRC_TCD_IN_ENDPOINT_BITMAP  (0xFFFF0000)  /* bitmap for In endpoint */


/* Registers of Hardware */

/* physical address offsets of MHDRC common registers */

#define USB_MHDRC_OFFSET                 (0)

/* Common USB registers */

#define USB_MHDRC_FADD                   (USB_MHDRC_OFFSET + 0x00)
#define USB_MHDRC_POWER                  (USB_MHDRC_OFFSET + 0x01)
#define USB_MHDRC_INTRTX                 (USB_MHDRC_OFFSET + 0x02)
#define USB_MHDRC_INTRRX                 (USB_MHDRC_OFFSET + 0x04)
#define USB_MHDRC_INTRTXE                (USB_MHDRC_OFFSET + 0x06)
#define USB_MHDRC_INTRRXE                (USB_MHDRC_OFFSET + 0x08)
#define USB_MHDRC_INTRUSB                (USB_MHDRC_OFFSET + 0x0A)
#define USB_MHDRC_INTRUSBE               (USB_MHDRC_OFFSET + 0x0B)
#define USB_MHDRC_FRAME                  (USB_MHDRC_OFFSET + 0x0C)
#define USB_MHDRC_INDEX                  (USB_MHDRC_OFFSET + 0x0E)
#define USB_MHDRC_TESTMODE               (USB_MHDRC_OFFSET + 0x0F)

/* Index related registers */

#define USB_MHDRC_INDEXED_TXMAXP            (USB_MHDRC_OFFSET + 0x10)
#define USB_MHDRC_INDEXED_HOST_TXCSR        (USB_MHDRC_OFFSET + 0x12)
#define USB_MHDRC_INDEXED_RXMAXP            (USB_MHDRC_OFFSET + 0x14)
#define USB_MHDRC_INDEXED_HOST_RXCSR        (USB_MHDRC_OFFSET + 0x16)
#define USB_MHDRC_INDEXED_COUNT0_RXCOUNT    (USB_MHDRC_OFFSET + 0x18)
#define USB_MHDRC_INDEXED_HOST_TYPE0        (USB_MHDRC_OFFSET + 0x1A)
#define USB_MHDRC_INDEXED_HOST_TXTYPE       (USB_MHDRC_OFFSET + 0x1A)
#define USB_MHDRC_INDEXED_HOST_NAKLIMIT0    (USB_MHDRC_OFFSET + 0x1B)
#define USB_MHDRC_INDEXED_HOST_TXINTERVAL   (USB_MHDRC_OFFSET + 0x1B)
#define USB_MHDRC_INDEXED_HOST_RXTYPE       (USB_MHDRC_OFFSET + 0x1C)
#define USB_MHDRC_INDEXED_HOST_RXINTERVAL   (USB_MHDRC_OFFSET + 0x1D)

/* CONFIGDATA Register */

#define USB_MHDRC_INDEXED_CONFIGDATA        (USB_MHDRC_OFFSET + 0x1F)

/* FIFOn */

/* TX and RX FIFO Register for Epx */

#define USB_MHDRC_FIFO_EP(x)             (USB_MHDRC_OFFSET + 0x20 + ((x) << 2))

/* OTG Device Control Register */

#define USB_MHDRC_DEVCTL                 (USB_MHDRC_OFFSET + 0x60)

/* Dynamic FIFO Control */

#define USB_MHDRC_TXFIFOSZ               (USB_MHDRC_OFFSET + 0x62)
#define USB_MHDRC_RXFIFOSZ               (USB_MHDRC_OFFSET + 0x63)
#define USB_MHDRC_TXFIFOADDR             (USB_MHDRC_OFFSET + 0x64)
#define USB_MHDRC_RXFIFOADDR             (USB_MHDRC_OFFSET + 0x66)

/* Target Epx Control Registers, Valid Only in Host Mode */

#define USB_MHDRC_TXFUNCADDR_EP(x)       (USB_MHDRC_OFFSET + 0x80 + ((x) << 3))
#define USB_MHDRC_TXHUBADDR_EP(x)        (USB_MHDRC_OFFSET + 0x82 + ((x) << 3))
#define USB_MHDRC_TXHUBPORT_EP(x)        (USB_MHDRC_OFFSET + 0x83 + ((x) << 3))
#define USB_MHDRC_RXFUNCADDR_EP(x)       (USB_MHDRC_OFFSET + 0x84 + ((x) << 3))
#define USB_MHDRC_RXHUBADDR_EP(x)        (USB_MHDRC_OFFSET + 0x86 + ((x) << 3))
#define USB_MHDRC_RXHUBPORT_EP(x)        (USB_MHDRC_OFFSET + 0x87 + ((x) << 3))

/* Control and status register for Endpoint 0 Peripheral Mode */

#define USB_MHDRC_PERI_CSR0              (USB_MHDRC_OFFSET + 0x102)
#define USB_MHDRC_HOST_CSR0              (USB_MHDRC_OFFSET + 0x102)
#define USB_MHDRC_COUNT0                 (USB_MHDRC_OFFSET + 0x108)
#define USB_MHDRC_HOST_TYPE0             (USB_MHDRC_OFFSET + 0x10A)
#define USB_MHDRC_HOST_NAKLIMIT0         (USB_MHDRC_OFFSET + 0x10B)
#define USB_MHDRC_CONFIGDATA             (USB_MHDRC_OFFSET + 0x10F)

/* Control and Status Register for Epx  1<= x <= 15 */

#define USB_MHDRC_PERI_TXMAXP_EP(x)      (USB_MHDRC_OFFSET + 0x110 + ((x - 1) << 4))
#define USB_MHDRC_PERI_TXCSR_EP(x)       (USB_MHDRC_OFFSET + 0x112 + ((x - 1) << 4))
#define USB_MHDRC_PERI_RXMAXP_EP(x)      (USB_MHDRC_OFFSET + 0x114 + ((x - 1) << 4))
#define USB_MHDRC_PERI_RXCSR_EP(x)       (USB_MHDRC_OFFSET + 0x116 + ((x - 1) << 4))
#define USB_MHDRC_PERI_RXCOUNT_EP(x)     (USB_MHDRC_OFFSET + 0x118 + ((x - 1) << 4))

#define USB_MHDRC_HOST_TXMAXP_EP(x)      (USB_MHDRC_OFFSET + 0x110 + ((x - 1) << 4))
#define USB_MHDRC_HOST_TXCSR_EP(x)       (USB_MHDRC_OFFSET + 0x112 + ((x - 1) << 4))
#define USB_MHDRC_HOST_RXMAXP_EP(x)      (USB_MHDRC_OFFSET + 0x114 + ((x - 1) << 4))
#define USB_MHDRC_HOST_RXCSR_EP(x)       (USB_MHDRC_OFFSET + 0x116 + ((x - 1) << 4))
#define USB_MHDRC_HOST_RXCOUNT_EP(x)     (USB_MHDRC_OFFSET + 0x118 + ((x - 1) << 4))
#define USB_MHDRC_HOST_TXTYPE_EP(x)      (USB_MHDRC_OFFSET + 0x11A + ((x - 1) << 4))
#define USB_MHDRC_HOST_TXINTERVAL_EP(x)  (USB_MHDRC_OFFSET + 0x11B + ((x - 1) << 4))
#define USB_MHDRC_HOST_RXTYPE_EP(x)      (USB_MHDRC_OFFSET + 0x11C + ((x - 1) << 4))
#define USB_MHDRC_HOST_RXINTERVAL_EP(x)  (USB_MHDRC_OFFSET + 0x11D + ((x - 1) << 4))

/* To hold the USB Transfer Types (BEGIN) */

#define USB_MHDRC_CONTROL_TRANSFER        (0x00)
#define USB_MHDRC_ISOCHRONOUS_TRANSFER    (0x01)
#define USB_MHDRC_BULK_TRANSFER           (0x02)
#define USB_MHDRC_INTERRUPT_TRANSFER      (0x03)

/* To hold the USB Transfer Types (END) */

#define USB_MHDRC_INTMSKCLR_USBINT_MASK   (0xff)
#define USB_MHDRC_INTMSKCLR_TX_MASK       (0xffff)    /* Total 16 with EP0 */
#define USB_MHDRC_INTMSKCLR_RX_MASK       (0xfffe)    /* Total 15 without EP0 */
#define USB_MHDRC_INTMSKCLR_SOF_MASK      (0xf7)

#define USB_MHDRC_USBINT_SHIFT                  16
#define USB_MHDRC_TXINT_SHIFT                   0
#define USB_MHDRC_RXINT_SHIFT                   8

#define USB_MHDRC_USBINT_MASK \
        (USB_MHDRC_INTMSKCLR_USBINT_MASK << USB_MHDRC_USBINT_SHIFT)
#define USB_MHDRC_TXINT_MASK \
        (USB_MHDRC_INTMSKCLR_TX_MASK << USB_MHDRC_TXINT_SHIFT)
#define USB_MHDRC_RXINT_MASK \
        (USB_MHDRC_INTMSKCLR_RX_MASK << USB_MHDRC_RXINT_SHIFT)

/* Value to set the RESET field of the CTRLR register */

#define USB_MHDRC_CTRLR_RESET                            0x00000001

/* Value to set the UINT field of the CTRLR register */

#define USB_MHDRC_CTRLR_UINT                             0x00000008

/* Bit's of  INTRTX */

#define USB_MHDRC_INTRTX_ACTIVE_EP(n)    (0x1 << (n))

/* Bit's of  POWER */

#define USB_MHDRC_POWER_ISOUPDATE         (0x80)
#define USB_MHDRC_POWER_SOFTCONN          (0x40)
#define USB_MHDRC_POWER_HSEN              (0x20)
#define USB_MHDRC_POWER_HSMODE            (0x10)
#define USB_MHDRC_POWER_RESET             (0x08)
#define USB_MHDRC_POWER_RESUME            (0x04)
#define USB_MHDRC_POWER_SUSPENDM          (0x02)
#define USB_MHDRC_POWER_ENSUSPM           (0x01)

/* Bit's of INTRUSB */

#define USB_MHDRC_INTR_VBUSERROR          (0x80)
#define USB_MHDRC_INTR_SESSREQ            (0x40)
#define USB_MHDRC_INTR_DISCONNECT         (0x20)
#define USB_MHDRC_INTR_CONNECT            (0x10)
#define USB_MHDRC_INTR_SOF                (0x08)
#define USB_MHDRC_INTR_BABBLE             (0x04)
#define USB_MHDRC_INTR_RESET              (0x04)
#define USB_MHDRC_INTR_RESUME             (0x02)
#define USB_MHDRC_INTR_SUSPEND            (0x01)
#define USB_MHDRC_INTR_MASK_NO_SOF        (0xF7)

/* Bit's of TESTMODE */

#define USB_MHDRC_TESTMODE_FORCE_HOST     (0x80)
#define USB_MHDRC_TESTMODE_FORCE_ACCESS   (0x40)
#define USB_MHDRC_TESTMODE_FORCE_FS       (0x20)
#define USB_MHDRC_TESTMODE_FORCE_HS       (0x10)
#define USB_MHDRC_TESTMODE_TEST_PACKET    (0x08)
#define USB_MHDRC_TESTMODE_TEST_K         (0x04)
#define USB_MHDRC_TESTMODE_TEST_J         (0x02)
#define USB_MHDRC_TESTMODE_TEST_SE0_NAK   (0x01)


/* Bit's of CSR0 of Peripheral */

#define USB_MHDRC_PERI_CSR0_FLUSHFIFO     (0x0100)
#define USB_MHDRC_PERI_CSR0_SERV_SETUPEND (0x0080)
#define USB_MHDRC_PERI_CSR0_SERV_RXPKTRDY (0x0040)
#define USB_MHDRC_PERI_CSR0_SENDSTALL     (0x0020)
#define USB_MHDRC_PERI_CSR0_SETUPEND      (0x0010)
#define USB_MHDRC_PERI_CSR0_DATAEND       (0x0008)
#define USB_MHDRC_PERI_CSR0_SENTSTALL     (0x0004)
#define USB_MHDRC_PERI_CSR0_TXPKTRDY      (0x0002)
#define USB_MHDRC_PERI_CSR0_RXPKTRDY      (0x0001)


/* Bit's of PERI_TXCSR */

#define USB_MHDRC_PERI_TXCSR_AUTOSET      (0x8000)
#define USB_MHDRC_PERI_TXCSR_ISO          (0x4000)
#define USB_MHDRC_PERI_TXCSR_MODE         (0x2000)
#define USB_MHDRC_PERI_TXCSR_DMAEN        (0x1000)
#define USB_MHDRC_PERI_TXCSR_FRCDATATOG   (0x0800)
#define USB_MHDRC_PERI_TXCSR_DMAMODE      (0x0400)
#define USB_MHDRC_PERI_TXCSR_INCOMPTX     (0x0080)
#define USB_MHDRC_PERI_TXCSR_CLRDATATOG   (0x0040)
#define USB_MHDRC_PERI_TXCSR_SENTSTALL    (0x0020)
#define USB_MHDRC_PERI_TXCSR_SENDSTALL    (0x0010)
#define USB_MHDRC_PERI_TXCSR_FLUSHFIFO    (0x0008)
#define USB_MHDRC_PERI_TXCSR_UNDERRUN     (0x0004)
#define USB_MHDRC_PERI_TXCSR_FIFONOEMPTY  (0x0002)
#define USB_MHDRC_PERI_TXCSR_TXPKTRDY     (0x0001)

#define USB_MHDRC_HOST_TXCSR_WZC_BITS                                     \
                                       (USB_MHDRC_HOST_TXCSR_NAKTIMEOUT  |\
                                        USB_MHDRC_HOST_TXCSR_RXSTALL     |\
                                        USB_MHDRC_HOST_TXCSR_ERROR       |\
                                        USB_MHDRC_HOST_TXCSR_FIFONOEMPTY)
#define USB_MHDRC_PERI_TXCSR_WZC_BITS                                     \
                                 (USB_MHDRC_PERI_TXCSR_SENTSTALL         |\
                                  USB_MHDRC_PERI_TXCSR_UNDERRUN          |\
                                  USB_MHDRC_PERI_TXCSR_FIFONOEMPTY)

/* Bit's of  PERI_RXCSR */

#define USB_MHDRC_PERI_RXCSR_AUTOCLEAR    (0x8000)
#define USB_MHDRC_PERI_RXCSR_ISO          (0x4000)
#define USB_MHDRC_PERI_RXCSR_DMAEN        (0x2000)
#define USB_MHDRC_PERI_RXCSR_DISNYET      (0x1000)
#define USB_MHDRC_PERI_RXCSR_DMAMODE      (0x0800)
#define USB_MHDRC_PERI_RXCSR_INCOMPRX     (0x0100)
#define USB_MHDRC_PERI_RXCSR_CLRDATATOG   (0x0080)
#define USB_MHDRC_PERI_RXCSR_SENTSTALL    (0x0040)
#define USB_MHDRC_PERI_RXCSR_SENDSTALL    (0x0020)
#define USB_MHDRC_PERI_RXCSR_FLUSHFIFO    (0x0010)
#define USB_MHDRC_PERI_RXCSR_DATAERROR    (0x0008)
#define USB_MHDRC_PERI_RXCSR_OVERRUN      (0x0004)
#define USB_MHDRC_PERI_RXCSR_FIFOFULL     (0x0002)
#define USB_MHDRC_PERI_RXCSR_RXPKTRDY     (0x0001)

/* Bit's of CSR0 */

#define USB_MHDRC_HOST_CSR0_DISPING         0x0800
#define USB_MHDRC_HOST_CSR0_DATATOGWREN     0x0400
#define USB_MHDRC_HOST_CSR0_DATATOG         0x0200
#define USB_MHDRC_HOST_CSR0_FLUSHFIFO       0x0100
#define USB_MHDRC_HOST_CSR0_NAKTIMEOUT      0x0080
#define USB_MHDRC_HOST_CSR0_STATUSPKT       0x0040
#define USB_MHDRC_HOST_CSR0_REQPKT          0x0020
#define USB_MHDRC_HOST_CSR0_ERROR           0x0010
#define USB_MHDRC_HOST_CSR0_SETUPPKT        0x0008
#define USB_MHDRC_HOST_CSR0_RXSTALL         0x0004
#define USB_MHDRC_HOST_CSR0_TXPKTRDY        0x0002
#define USB_MHDRC_HOST_CSR0_RXPKTRDY        0x0001

/* Bit's of HOST_TXCSR */

#define USB_MHDRC_HOST_TXCSR_AUTOSET     0x8000
#define USB_MHDRC_HOST_TXCSR_MODE        0x2000
#define USB_MHDRC_HOST_TXCSR_DMAEN       0x1000
#define USB_MHDRC_HOST_TXCSR_FRCDATATOG  0x0800
#define USB_MHDRC_HOST_TXCSR_DMAMODE     0x0400
#define USB_MHDRC_HOST_TXCSR_DATATOGWREN 0x0200
#define USB_MHDRC_HOST_TXCSR_DATATOG     0x0100
#define USB_MHDRC_HOST_TXCSR_NAKTIMEOUT  0x0080
#define USB_MHDRC_HOST_TXCSR_CLRDATATOG  0x0040
#define USB_MHDRC_HOST_TXCSR_RXSTALL     0x0020
#define USB_MHDRC_HOST_TXCSR_SETUPPKT    0x0010
#define USB_MHDRC_HOST_TXCSR_FLUSHFIFO   0x0008
#define USB_MHDRC_HOST_TXCSR_ERROR       0x0004
#define USB_MHDRC_HOST_TXCSR_FIFONOEMPTY 0x0002
#define USB_MHDRC_HOST_TXCSR_TXPKTRDY    0x0001

/* Bit's of  HOST_RXCSR */

#define USB_MHDRC_HOST_RXCSR_AUTOCLEAR   0x8000
#define USB_MHDRC_HOST_RXCSR_AUTOREQ     0x4000
#define USB_MHDRC_HOST_RXCSR_DMAEN       0x2000
#define USB_MHDRC_HOST_RXCSR_DISNYET     0x1000
#define USB_MHDRC_HOST_RXCSR_DMAMODE     0x0800
#define USB_MHDRC_HOST_RXCSR_DATATOGWREN 0x0400
#define USB_MHDRC_HOST_RXCSR_DATATOG     0x0200
#define USB_MHDRC_HOST_RXCSR_CLRDATATOG  0x0080
#define USB_MHDRC_HOST_RXCSR_RXSTALL     0x0040
#define USB_MHDRC_HOST_RXCSR_REQPKT      0x0020
#define USB_MHDRC_HOST_RXCSR_FLUSHFIFO   0x0010
#define USB_MHDRC_HOST_RXCSR_NAKTIMEOUT  0x0008
#define USB_MHDRC_HOST_RXCSR_ERROR       0x0004
#define USB_MHDRC_HOST_RXCSR_FIFOFULL    0x0002
#define USB_MHDRC_HOST_RXCSR_RXPKTRDY    0x0001

#define USB_MHDRC_HOST_RXCSR_WZC_BITS                                   \
                                       (USB_MHDRC_HOST_RXCSR_RXSTALL   |\
                                       USB_MHDRC_HOST_RXCSR_ERROR      |\
                                       USB_MHDRC_HOST_RXCSR_NAKTIMEOUT |\
                                       USB_MHDRC_HOST_RXCSR_RXPKTRDY)

#define USB_MHDRC_PERI_RXCSR_WZC_BITS                                   \
                                    (USB_MHDRC_PERI_RXCSR_SENTSTALL    |\
                                     USB_MHDRC_PERI_RXCSR_OVERRUN      |\
                                     USB_MHDRC_PERI_RXCSR_RXPKTRDY)



/* Bit's of  HOST_RX_TXTYPE */

#define USB_MHDRC_HOST_TYPE_SPEED_OFFSET                 (0x06)
#define USB_MHDRC_HOST_TYPE_PORT_OFFSET                  (0x04)
#define USB_MHDRC_HOST_TYPE_ENDPOINT_OFFSET              (0x00)

#define USB_MHDRC_HOST_TYPE_SPEED_MASK                   (0xC0)
#define USB_MHDRC_HOST_TYPE_SPEED_HIGH                   (0x40)
#define USB_MHDRC_HOST_TYPE_SPEED_FULL                   (0x80)
#define USB_MHDRC_HOST_TYPE_SPEED_LOW                    (0xC0)

#define USB_MHDRC_HOST_TYPE_PORT_MASK                    (0x30)
#define USB_MHDRC_HOST_TYPE_PORT_CONTROL                 (0x00)
#define USB_MHDRC_HOST_TYPE_PORT_ISO                     (0x10)
#define USB_MHDRC_HOST_TYPE_PORT_BULK                    (0x20)
#define USB_MHDRC_HOST_TYPE_PORT_INTERRUPT               (0x30)

#define USB_MHDRC_HOST_TYPE_ENDPOINT_MASK                (0x0F)

/* Bit's of  DEVCTL */

#define USB_MHDRC_DEVCTL_BDEVICE          (0x80)
#define USB_MHDRC_DEVCTL_FSDEV            (0x40)
#define USB_MHDRC_DEVCTL_LSDEV            (0x20)
#define USB_MHDRC_DEVCTL_VBUS             (0x18)
#define USB_MHDRC_DEVCTL_S_VBUS           (0x03)
#define USB_MHDRC_DEVCTL_HM               (0x04)
#define USB_MHDRC_DEVCTL_HOSTREQ          (0x02)
#define USB_MHDRC_DEVCTL_SESSION          (0x01)
#define USB_MHDRC_DEVCTL_VBUS_SESS_END        0
#define USB_MHDRC_DEVCTL_VBUS_A_VALID         1
#define USB_MHDRC_DEVCTL_VBUS_B_VALID         2
#define USB_MHDRC_DEVCTL_VBUS_VALID           3

/* Bit's of FIFOSZ */

#define USB_MHDRC_FIFOSZ_DPB              (0x10)
#define USB_MHDRC_FIFOSZ_SZ_MASK          (0x0F)

#define USB_MHDRC_FIFOSZ_SZ_8             (0x0)
#define USB_MHDRC_FIFOSZ_SZ_16            (0x1)
#define USB_MHDRC_FIFOSZ_SZ_32            (0x2)
#define USB_MHDRC_FIFOSZ_SZ_64            (0x3)
#define USB_MHDRC_FIFOSZ_SZ_128           (0x4)
#define USB_MHDRC_FIFOSZ_SZ_256           (0x5)
#define USB_MHDRC_FIFOSZ_SZ_512           (0x6)
#define USB_MHDRC_FIFOSZ_SZ_1024          (0x7)

#define USB_MHDRC_MAXPSIZE_8                (0x0008)   /* Max Packet Size - 8 */
#define USB_MHDRC_MAXPSIZE_16               (0x0010)   /* Max Packet Size - 16 */
#define USB_MHDRC_MAXPSIZE_32               (0x0020)   /* Max Packet Size - 32 */
#define USB_MHDRC_MAXPSIZE_64               (0x0040)   /* Max Packet Size - 64 */
#define USB_MHDRC_MAXPSIZE_128              (0x0080)   /* Max Packet Size - 128 */
#define USB_MHDRC_MAXPSIZE_256              (0x0100)   /* Max Packet Size - 256 */
#define USB_MHDRC_MAXPSIZE_512              (0x0200)   /* Max Packet Size - 512 */

#define USB_MHDRC_MAX_PACK_SIZE_MASK        (0x07FF) /* max packet size mask */

#define USB_MHDRC_FRAME_MASK                (0x00007FF) /* FRAME mask */

#define USB_MHDRC_RH_DOWNSTREAM_PORT        (1)

#define USB_MHDRC_REG_ACCESS_WAIT_COUNT     (1000)
#define USB_MHDRC_VBUSERROR_RECOVERY_MAX    (5)

typedef struct usbTcdMhdrcPlatformDataStruct
    {
    ULONG   uComRegOffset;                          /* Common registers offset */
    void  (*pUsbCoreIsr)(void * pMHDRC);          /* Usb core Isr */
    void  (*pUsbIsrDone)(void * pMHDRC);          /* Isr process done */
    void  (*pHwInterruptEnable)(void * pMHDRC);   /* Platform interrupt enable */
    void  (*pHwInterruptDisable) (void * pMHDRC); /* Platform Interrupt disable */
    void  (*pPowerConfigure)(void * pMHDRC,BOOL uEnable); /* Power supply configure */
    }USB_MHDRC_PLATFORM_DATA,*pUSB_MHDRC_PLATFORM_DATA;

/* Common Attributes for MUSBMHDRC USB HS OTG Controller */

struct usb_musbmhdrc;

#define USB_MHDRC_DMA_INVALID_CHANNEL (0xFF)
#define USB_MHDRC_MAX_DMA_CH          (16)

typedef enum usb_dma_status
    {
    USB_MHDRC_DMA_CHANNEL_FREE = 0,
    USB_MHDRC_DMA_CHANNEL_BUSY,
    USB_MHDRC_DMA_CHANNEL_ERR
    }USB_DMA_STATUS;

typedef enum usb_dma_type
    {
    USB_MHDRC_HS_DMA = 0,
    USB_MHDRC_CPPI_DMA,
    }USB_DMA_TYPE;

typedef void (*pUSB_MHDRC_DMA_CALLBACK_FUNC)
    (
    struct usb_musbmhdrc * pMHDRC,
    int                    uChannel
    );

typedef struct usb_mhdrc_dma_channel
    {
    void *  pRequest;
    ULONG   uAddr;
    UINT32  uTotalLen;
    UINT32  uActLen;
    UINT8   uStatus;
    UINT8   uEpAddress;
    UINT8   uMode;
    BOOLEAN bTX;
    UINT16  uMaxPacketSize;
    UINT8   uChannel;
    }USB_MHDRC_DMA_CHANNEL, *pUSB_MHDRC_DMA_CHANNEL;

typedef struct usb_mhdrc_dma_data
    {
    OS_EVENT_ID  pSynchMutex;
    OS_EVENT_ID  isrEvent;
    OS_THREAD_ID irqThread;
    void *       pCppi;       /* CPPI DMA Private data */
    UINT32       uDmaOffset;
    UINT32       isrMagic;
    UINT16       uFreeDmaChannelBitMap;
    UINT8        uIrqStatus;
    UINT8        uDMAType;
    UINT8        uMaxDmaChannel;
    BOOL         bEnable;
    USB_MHDRC_DMA_CHANNEL dmaChannel[USB_MHDRC_MAX_DMA_CH];
    int (* pDmaChannelRequest)(struct usb_mhdrc_dma_data * pDMAData);
    int (* pDmaChannelRelease)(struct usb_mhdrc_dma_data * pDMAData,
                               int                         uChannel);
    STATUS (* pDmaConfigure)(struct usb_musbmhdrc *  pMHDRC,
                             void *                  pRequest,
                             ULONG                   uAddr,
                             UINT32                  uLength,
                             UINT8                   uChannel,
                             UINT8                   uMode,
                             UINT8                   uEp,
                             BOOLEAN                 bTX,
                             UINT16                  uMaxPacketSize);
    STATUS (* pDmaCancel)(struct usb_musbmhdrc *    pMHDRC,
                          UINT8                     uChannel);
    void (*pDmaIrqHandler)(struct usb_musbmhdrc *   pMHDRC);
    void (*pDmaIsr)(struct usb_musbmhdrc *   pMHDRC);
    pUSB_MHDRC_DMA_CALLBACK_FUNC pDmaCallback;
    } USB_MHDRC_DMA_DATA, *pUSB_MHDRC_DMA_DATA;

typedef struct usb_musbmhdrc
    {
    VXB_DEVICE_ID pDev;       /* struct vxbDev */
    spinlockIsr_t spinLock;   /* Spinlock for the device */

    /* Interrupt status */

    UINT16  uTxIrqStatus;
    UINT16  uRxIrqStatus;
    UINT8   uUsbIrqStatus;
    UINT32  uVbusError;

    UINT16  uFrameNumberRecord;
    UINT8   uFrameNumChangeFlag;

    UINT8   uTxFifoSize[USB_MHDRC_NUM_ENDPOINTS_TX];
    UINT8   uRxFifoSize[USB_MHDRC_NUM_ENDPOINTS_RX];

    USB_MHDRC_PLATFORM_DATA PlatformData;
    USB_MHDRC_DMA_DATA      dmaData;

    pVOID   pHCDData;
    pVOID   pTCDData;
    pVOID   pOCDData;

    ULONG   uRegBase;         /* Register base address */
    ULONG   uUsbssRegBase;    /* Register base address for USBSS */
    UINT8   uRootHubNumPorts; /* Number of usb port of root hub */
    UINT8   uUseExtPower;     /* Use external power or not */
    UINT8   uPlatformType;    /* Platform Type */
    UINT8   uDmaType;         /* Dma Type */
    UINT8   uNumDmaChannels;  /* Number of DMA channels */
    UINT8   uNumEps;          /* Number of endpoints */
    UINT8   uLastPausedEp;    /* The last endpoint channel paused */
    BOOL    bDmaEnabled;      /* Enable Dma or not */

    /* Phy mode can be define paltform specific
     *
     * Omap3530
     *
     * #define USB2_TARGET_PHY_ULPI_INTERNAL 0x00
     * #define USB2_TARGET_PHY_ULPI_EXTERNAL 0x01
     *
     * fsl_ads5121e
     *
     * #define FSL_USB_DR_PHY_ULPI         0x0001
     * #define FSL_USB_DR_PHY_UTMI8        0x0002
     * #define FSL_USB_DR_PHY_UTMI16       0x0003
     * #define FSL_USB_DR_PHY_SERIAL       0x0004
     */

    UINT8       phyMode;

    /* Magic value for shared interrupts */

    UINT32      isrMagic;

    /* Register related definition */

    /* Handle for the register access methods */

    void *  pRegAccessHandle;

    /* Platform specific initialization */

    void    (*pUsbHwInit) (void);

    /*
     * Function pointer to hold the function doing
     * byte conversions for HC data structure
     */

    UINT32  (*pDescSwap)(UINT32 data);

    /*
     * Function pointer to hold the function doing
     * byte conversions for USB endian
     */

    UINT32  (*pUsbSwap)(UINT32 data);

    /*
     * Function pointer to hold the function doing
     * byte conversions for HC register endian
     */

    UINT32  (*pRegSwap)(UINT32 data);

    /*
      * Function pointer to hold the function doing
      * byte conversions for Bus address to CPU address
      */

    pVOID   (*pBusToCpu)(ULONG addr);

    /*
      * Function pointer to hold the function doing
      * byte conversions for Bus address to CPU address
      */

    ULONG  (*pCpuToBus)(pVOID pAddr);


    /*
     * Function pointer to hold the function doing
     * to read ULPI PHY registers
     */

    STATUS  (*pPhyUlpiRegRead)(UINT8 uOffset, UINT8 * pVal);

    /*
     * Function pointer to hold the function doing
     * to write ULPI PHY registers
     */

    STATUS  (*pPhyUlpiRegWrite)(UINT8 uOffset, UINT8 uVal);

    /*
     * Function pointer to hold the function to
     * get ID pin state.
     *
     * If this returns FALSE, then there is A-plug inserted
     * and the device should be in A-device.
     *
     * If this function is present, then it suppresses built-in
     * methods to get ID pin state.
     */

    BOOL  (*pPhyIdPinStateGet)(void);

    /*
     * Function pointer to hold the function to
     * get VBUS pin state.
     *
     * If this returns TRUE, then the port is attached to an ,
     * external Host and the Host has powered on its VBUS to us.
     *
     * If this function is present, then it suppresses built-in
     * methods to get VBUS pin state if the device is B-device.
     */

    BOOL  (*pPhyVbusPinStateGet)(void);


    VOID  (*pISR)(void * pMHDRC);

    /*
     * Function pointer to hold the function doing
     * post reset operation
     */

    FUNCPTR     pPostResetHook;

    /* Is there any connection established */

    BOOL        bIsConnected;

    BOOL        bIsResetActive;
    }USB_MUSBMHDRC, * pUSB_MUSBMHDRC;

/* Definition for register access */

#define USB_MHDRC_REG_WRITE32(pMHDRC, OFFSET, VALUE)                          \
        vxbWrite32 (((pUSB_MUSBMHDRC)(pMHDRC))->pRegAccessHandle,             \
            (void *)(((pUSB_MUSBMHDRC)(pMHDRC))->uRegBase +                   \
            ((pUSB_MUSBMHDRC)(pMHDRC))->PlatformData.uComRegOffset + OFFSET), \
            (UINT32)(VALUE))

#define USB_MHDRC_REG_READ32(pMHDRC, OFFSET)                                   \
        vxbRead32 (((pUSB_MUSBMHDRC)(pMHDRC))->pRegAccessHandle,               \
            (void *)(((pUSB_MUSBMHDRC)(pMHDRC))->uRegBase +                    \
            ((pUSB_MUSBMHDRC)(pMHDRC))->PlatformData.uComRegOffset + OFFSET))

#define USB_MHDRC_REG_WRITE16(pMHDRC, OFFSET, VALUE)                           \
        vxbWrite16 (((pUSB_MUSBMHDRC)(pMHDRC))->pRegAccessHandle,              \
            (void *)(((pUSB_MUSBMHDRC)(pMHDRC))->uRegBase +                    \
            ((pUSB_MUSBMHDRC)(pMHDRC))->PlatformData.uComRegOffset + OFFSET),  \
            (UINT16)(VALUE))

#define USB_MHDRC_REG_READ16(pMHDRC, OFFSET)                                   \
        vxbRead16 (((pUSB_MUSBMHDRC)(pMHDRC))->pRegAccessHandle,               \
            (void *)(((pUSB_MUSBMHDRC)(pMHDRC))->uRegBase +                    \
            ((pUSB_MUSBMHDRC)(pMHDRC))->PlatformData.uComRegOffset + OFFSET))

#define USB_MHDRC_REG_WRITE8(pMHDRC, OFFSET, VALUE)                            \
        vxbWrite8 (((pUSB_MUSBMHDRC)(pMHDRC))->pRegAccessHandle,               \
            (void *)(((pUSB_MUSBMHDRC)(pMHDRC))->uRegBase +                    \
            ((pUSB_MUSBMHDRC)(pMHDRC))->PlatformData.uComRegOffset + OFFSET),  \
            (UINT8)(VALUE))

#define USB_MHDRC_REG_READ8(pMHDRC, OFFSET)                                    \
        vxbRead8 (((pUSB_MUSBMHDRC)(pMHDRC))->pRegAccessHandle,                \
        (void *)(((pUSB_MUSBMHDRC)(pMHDRC))->uRegBase +                        \
            ((pUSB_MUSBMHDRC)(pMHDRC))->PlatformData.uComRegOffset + OFFSET))

#define USB_MHDRC_FIND_FIRST_SET_BIT(uValue)     \
       (ffsLsb(uValue)-1)

typedef STATUS (*usbMhdrcHcdTcdInterfacePrototype)
    (
    pUSB_MUSBMHDRC      pMHDRC
    );

extern usbMhdrcHcdTcdInterfacePrototype gpUsbMhdrcHcdLoad;
extern usbMhdrcHcdTcdInterfacePrototype gpUsbMhdrcHcdUnLoad;
extern usbMhdrcHcdTcdInterfacePrototype gpUsbMhdrcTcdLoad;
extern usbMhdrcHcdTcdInterfacePrototype gpUsbMhdrcTcdUnLoad;
extern usbMhdrcHcdTcdInterfacePrototype gpUsbMhdrcHcdEnable;
extern usbMhdrcHcdTcdInterfacePrototype gpUsbMhdrcHcdDisable;
extern usbMhdrcHcdTcdInterfacePrototype gpUsbMhdrcTcdEnable;
extern usbMhdrcHcdTcdInterfacePrototype gpUsbMhdrcTcdDisable;

#ifdef __cplusplus
}
#endif

#endif /* __INCusbMhdrch */

