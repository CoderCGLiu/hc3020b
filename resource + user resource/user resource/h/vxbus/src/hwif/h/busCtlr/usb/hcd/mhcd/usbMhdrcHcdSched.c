/* usbMhdrcHcdSched.c - schedule process of Mentor Graphics HCD  */

/*
 * Copyright (c) 2009-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
02e,03may13,wyy  Remove compiler warning (WIND00356717)
02d,06may13,ljg  Do not issue PING tokens in the data and status phases of a 
                 high-speed control transfer (WIND00412889)
02c,20feb13,j_x  Do not update hub address/port registers for AM389X (WIND00404335)
02b,31jan13,ljg  add usb support for ti816x EVM
02a,25jul12,ljg  Clear Coverity issues for semXXXX functions (WIND00357471)
01z,18jun12,ljg  Correct the range of frame number (WIND00332337)
01y,17feb12,s_z  Fix 15+ devices pending issue by optimizing the channel usage and
                 fixing MTT hub issue (WIND00333840)
01x,15sep11,m_y  Using one structure to describe HSDMA and CPPIDMA
01w,22jun11,jws  Added include <usbMhdrcCppiDma.h> and modified to call CPPI DMA routine
01v,12apr11,m_y  Modify USB_MHDRC_DMA_DATA structure (WIND00265911)
01u,08apr11,w_x  Clear Coverity FORWARD_NULL for ISOCH Xfer (WIND00264893)
01t,09mar11,w_x  Code clean up for make man
01s,07jan11,w_x  Fix exception when disconnecting device (WIND00248652)
01r,13sep10,ghs  Fix defect found by code review of merge (WIND00232740)
01q,16aug10,w_x  VxWorks 64 bit audit and warning removal
01p,03jun10,s_z  Debug macro changed, Add more debug message
01o,16apr10,s_z  Correct interval abusing
01n,13mar10,s_z  Add Inventra DMA supported on OMAP3EVM
01m,25feb10,s_z  Rename request list in USB_MHCD_DATA structure(WIND00201709)
01l,02feb10,s_z  Remove all the un-scheduled reqeset in the list if no devices
                 existed on the root hub,and code cleaning
01k,22jan10,s_z  Adjust the checking interval of all interrupt pipes
01j,21jan10,s_z  Fix plugging issue and Interrupt transaction issue, add channel
                 available checking routine.
01i,14jan10,s_z  Adjust the schedule rule
01h,17dec09,s_z  Fix Interrupt In issue with hub and add ISO transaction support
01g,12nov09,s_z  Fix second plugging issue on the root hub
01f,26nov09,s_z  Fix Bulk out transaction issue and only move request from
                 waiting list when needed
01e,24nov09,s_z  Add the support for low speed devices and correct status phase
                 transaction issue.
01d,15nov09,s_z  Change the usbMhdrcHcdEp0InterruptHandle,usbMhdrcHcdTxInterruptHandle
                 and usbMhdrcHcdRxInterruptHandle routines
o1c,13nov09,s_z  Add request list management routines
01b,12nov09,s_z  Add Management task to manage the schedule process
01a,11nov09,s_z  written
*/

/*
DESCRIPTION

This file provides the requests management and schedules the transaction process.

INCLUDE FILES: usb/usbOsal.h, usb/usbHst.h, usb/usbd.h, usb/usb.h,
               usbMhdrc.h, usbMhdrcHcd.h, usbMhdrcHcdCore.h,
               usbMhdrcHcdSched.h, usbMhdrcHcdDebug.h, usbMhdrcHsDma.h,
               usbMhdrcCppiDma.h

SEE ALSO:

None
*/

/* includes */

#include <usb/usbOsal.h>
#include <usb/usbHst.h>
#include <usb/usbd.h>
#include <usb/usb.h>
#include "usbMhdrc.h"
#include "usbMhdrcHcd.h"
#include "usbMhdrcHcdCore.h"
#include "usbMhdrcHcdSched.h"
#include "usbMhdrcHcdDebug.h"
#include "usbMhdrcHsDma.h"
#include "usbMhdrcCppiDma.h"

/* Add the debug file, which will be built in if defined USB_MHCD_SHOW */

#include <usbMhdrcHcdDebug.c>


/* forward declarations */

LOCAL void usbMhdrcHcdNewRequestProcessStart
    (
    pUSB_MHCD_DATA         pHCDData,
    pUSB_MHCD_REQUEST_INFO pRequestInfo
    );

LOCAL void usbMhdrcHcdControlRequestPorcess
    (
    pUSB_MHCD_DATA         pHCDData,    /* Pointer to HCD Data     */
    pUSB_MHCD_REQUEST_INFO pRequestInfo /* Pointer to request info */
    );

/*******************************************************************************
*
* usbMhdrcHcdTransferTaskFeed - send message to usbMhdrcHcdTransferManagementTask task
*
* This routine is used to send transfer message to usbMhdrcHcdTransferManagementTask
* task.
*
* RETURNS: N/A.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcHcdTransferTaskFeed
    (
    pUSB_MHCD_DATA          pHCDData,       /* Pointer to HCD Data */
    pUSB_MHCD_REQUEST_INFO  pRequestInfo,   /* Pointer to request info */
    UINT32                  uCmdCode,       /* Cmd code */
    USBHST_STATUS           uCompleteStatus /* Urb complete Status */
    )
    {
    USB_MHCD_TRANSFER_TASK_INFO transferTaskInfo;

    if ((pHCDData == NULL) ||
        (pHCDData->TransferThreadMsgID == NULL)||
        (0 == uCmdCode))
        {
        USB_MHDRC_ERR("usbMhdrcHcdTransferTaskFeed(): "
                     "MsgQueue doesn't exist\n",
                     1, 2, 3, 4, 5 ,6);

        return ;
        }

    transferTaskInfo.pHCDData = pHCDData;
    transferTaskInfo.pRequestInfo = pRequestInfo;
    transferTaskInfo.uCmdCode = uCmdCode;
    transferTaskInfo.uCompleteStatus = uCompleteStatus;

    if (OK != msgQSend (pHCDData->TransferThreadMsgID,
                         (char*) &transferTaskInfo,
                         sizeof(USB_MHCD_TRANSFER_TASK_INFO),
                         WAIT_FOREVER,
                         MSG_PRI_NORMAL))
        {
        USB_MHDRC_ERR("usbMhdrcHcdTransferTaskFeed(): "
                     "Message Send Failed\n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }
    return ;
    }

/***************************************************************************
*
* usbMhdrcHcdRequestTransferPause - pause the transaction of the request
*
* This routine pauses the transaction of the request
*
* RETURNS: N/A.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcHcdRequestTransferPause
    (
    pUSB_MHCD_DATA         pHCDData,       /* Pointer to HCD Data */
    pUSB_MHCD_REQUEST_INFO pRequestInfo    /* Pointer to request info */
    )
    {
    pUSB_MHCD_PIPE          pHCDPipe = NULL;
    pUSB_MUSBMHDRC          pMHDRC;

    if((NULL == pHCDData) ||
       (NULL == pRequestInfo) ||
       (NULL == pRequestInfo->pUrb) ||
       (NULL == pRequestInfo->pHCDPipe))
       {
       USB_MHDRC_ERR("usbMhdrcHcdRequestTransferPause(): "
                     "Invalid parameter, %s is NULL\n",
                     ((NULL == pHCDData) ? "pHCDData" :
                     (NULL == pRequestInfo) ? "pRequestInfo" :
                     (NULL == pRequestInfo->pUrb) ? "pRequestInfo->pUrb" :
                     "pRequestInfo->pHCDPipe"),
                     2, 3, 4, 5 ,6);

       return;
       }


    /* Get the common hardware data structure */

    pMHDRC = pHCDData->pMHDRC;

    /* Get the pipe pointer */

    pHCDPipe = pRequestInfo->pHCDPipe;

    /* Stop the request if needed */

    if (pRequestInfo ==
        pHCDData->pRequestInChannel[pHCDPipe->uEndPointEngineAsigned])
        {

        USB_MHDRC_VDBG("usbMhdrcHcdRequestTransferPause(): "
                      "Pause the transaction of pRequestInfo %p, dev %p Ep %p on CH %p\n",
                      pRequestInfo,
                      pHCDPipe->uDeviceAddress,
                      pHCDPipe->uEndpointAddress,
                      pHCDPipe->uEndPointEngineAsigned, 5 ,6);

        /* Stop the transaction */

        if (pHCDPipe->uEndpointDir == USB_DIR_IN) /* In */
            {
            USB_MHDRC_REG_WRITE16 (pMHDRC,
                 USB_MHDRC_HOST_RXCSR_EP(pHCDPipe->uEndPointEngineAsigned),
                 USB_MHDRC_HOST_RXCSR_FLUSHFIFO);
            USB_MHDRC_REG_WRITE16 (pMHDRC,
                USB_MHDRC_HOST_RXCSR_EP(pHCDPipe->uEndPointEngineAsigned),
                0);

            }
        else /* Out */
            {
            if (pHCDPipe->uEndpointAddress == 0)
                {
                USB_MHDRC_REG_WRITE16 (pMHDRC,
                     USB_MHDRC_HOST_CSR0,
                     USB_MHDRC_HOST_CSR0_FLUSHFIFO);
                USB_MHDRC_REG_WRITE16 (pMHDRC,
                     USB_MHDRC_HOST_CSR0,
                     0 );
                }
            else
                {
                USB_MHDRC_REG_WRITE16 (pMHDRC,
                     USB_MHDRC_HOST_TXCSR_EP(pHCDPipe->uEndPointEngineAsigned),
                     USB_MHDRC_HOST_TXCSR_FLUSHFIFO );
                USB_MHDRC_REG_WRITE16 (pMHDRC,
                     USB_MHDRC_HOST_TXCSR_EP(pHCDPipe->uEndPointEngineAsigned),
                     0 );
                }
            }

        }

    return;

    }

/***************************************************************************
*
* usbMhdrcHcdRequestStopAndCallBack - do the request call back and ready for delete
*
* This routine does the request call back and ready for delete
*
* RETURNS: N/A.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcHcdRequestStopAndCallBack
    (
    pUSB_MHCD_DATA         pHCDData,       /* Pointer to HCD Data */
    pUSB_MHCD_REQUEST_INFO pRequestInfo,   /* Pointer to request info */
    USBHST_STATUS          uCompleteStatus /* Urb complete Status */
    )
    {
    pUSB_MHCD_PIPE          pHCDPipe = NULL;

    if((NULL == pHCDData) ||
       (NULL == pRequestInfo) ||
       (NULL == pRequestInfo->pUrb) ||
       (NULL == pRequestInfo->pHCDPipe))
       {
       USB_MHDRC_ERR("usbMhdrcHcdRequestStopAndCallBack(): "
                     "Invalid parameter, %s is NULL\n",
                     ((NULL == pHCDData) ? "pHCDData" :
                     (NULL == pRequestInfo) ? "pRequestInfo" :
                     (NULL == pRequestInfo->pUrb) ? "pRequestInfo->pUrb" :
                     "pRequestInfo->pHCDPipe"),
                     2, 3, 4, 5 ,6);

       return;
       }

    /* Get the pipe pointer */

    pHCDPipe = pRequestInfo->pHCDPipe;

    /* Stop the request if needed */

    usbMhdrcHcdRequestTransferPause(pHCDData,pRequestInfo);

    /* Get the pipe pointer */

    if ((0 == (pRequestInfo->uTransferFlag & USB_MHCD_REQUEST_TO_REMOVE)) &&
        (ERROR != lstFind(&(pHCDPipe->RequestInfoList),
                         &(pRequestInfo->requestNode))))
        {
        semTake(pRequestInfo->pHCDPipe->pPipeSynchMutex,WAIT_FOREVER);
        pRequestInfo->pUrb->nStatus = uCompleteStatus;
        pRequestInfo->uTransferFlag |= USB_MHCD_REQUEST_TO_REMOVE;
        semGive(pRequestInfo->pHCDPipe->pPipeSynchMutex);

        /*
         * Now we still put the schedule here, as it's better
         * than the behind base my test result
         */

        if (pHCDPipe->uEndpointType == USB_ATTR_ISOCH)
            {
            pHCDPipe->uIsoPipeCurrentUrbIndex =
                (UINT16)((pHCDPipe->uIsoPipeCurrentUrbIndex + 1) % 0xFFFF);
            }

        if (pRequestInfo->uActLength <= pRequestInfo->pUrb->uTransferLength)
            {
            pRequestInfo->pUrb->uTransferLength = pRequestInfo->uActLength;
            }

        USB_MHDRC_VDBG("usbMhdrcHcdRequestStopAndCallBack(): "
                      "Callback before remove from pipe list\n",
                       1, 2,3,4, 5 ,6);

        if (pRequestInfo->pUrb->pfCallback)
            {
            (pRequestInfo->pUrb->pfCallback)(pRequestInfo->pUrb);
            }
        }

    return;
    }

/***************************************************************************
*
* usbMhdrcHcdRequestNeedSchedule - check if the request can be scheduled
*
* This routine checks if the request can be scheduled
*
* RETURNS: TRUE, or FALSE
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL BOOL usbMhdrcHcdRequestNeedSchedule
    (
    pUSB_MHCD_DATA         pHCDData,    /* Pointer to HCD Data */
    pUSB_MHCD_REQUEST_INFO pRequestInfo /* Pointer to request info */
    )
    {
    pUSB_MHCD_PIPE    pHCDPipe = NULL;
    UINT16            uCurrentFrameNumber = 0;
    UINT8             uCheckingInterval;

    /* Parameter varification */

    if ((NULL == pHCDData) ||
        (NULL == pRequestInfo) ||
        (NULL == pRequestInfo->pHCDPipe) ||
        (NULL == pRequestInfo->pUrb))
        {
        USB_MHDRC_ERR("usbMhdrcHcdRequestNeedSchedule(): "
                     "Invalid parameter, %s is NULL\n",
                     ((NULL == pHCDData) ? "pHCDData" :
                     (NULL == pRequestInfo) ? "pRequestInfo" :
                     (NULL == pRequestInfo->pHCDPipe) ? "pRequestInfo->pHCDPipe" :
                     "pRequestInfo->pUrb"),
                     2, 3, 4, 5 ,6);

        return FALSE;
        }

    /* Get the need scheduled request's pipe*/

    pHCDPipe = pRequestInfo->pHCDPipe;
    usbMhdrcHcdGetFrameNumber(pHCDData->uBusIndex, &uCurrentFrameNumber);

    /* No need be scheduled */

    if ((pRequestInfo->uTransferFlag & USB_MHCD_REQUEST_TRACTION_FINISH) ||
        (pRequestInfo->uTransferFlag & USB_MHCD_REQUEST_TO_REMOVE) ||
        (pRequestInfo->uStage == USB_MHCD_REQUEST_STAGE_DONE))
        {
        /* It is not error, means it no need been scheduled this time */

        USB_MHDRC_VDBG("usbMhdrcHcdRequestNeedSchedule(): "
                      "Transaction finished, no need been scheduled \n",
                       1, 2,3,4, 5 ,6);

        return FALSE;
        }

      /* For the control pipe, if the EP0 channel is available do it */

      if (pHCDPipe->uEndpointType == USB_ATTR_CONTROL)
         {
         if (NULL == pHCDData->pRequestInChannel[0])
            {
            return TRUE;
            }
         }

      /*
       * Update the Iso request frame number,
       * make sure it can be schedule
       */

      if (pHCDPipe->uEndpointType == USB_ATTR_ISOCH)
        {
        pRequestInfo->uFrameNumberLast = uCurrentFrameNumber;
        pRequestInfo->uFrameNumberNext =
            (UINT16)((pRequestInfo->uFrameNumberLast + pHCDPipe->bInterval) %
                                     USB_MHDRC_HCD_FRAME_NUM_MAX);
        if (pHCDPipe->uIsoPipeCurrentUrbIndex < pRequestInfo->uIsoPipeUrbIndex)
            {

            /* ISO transaction, need been scheduled one by one and in line */

            USB_MHDRC_VDBG("usbMhdrcHcdRequestNeedSchedule(): "
                          "ISO transaction, it is not the time to schedule this request\n",
                          1, 2, 3, 4, 5 ,6);

            return FALSE;
            }
        }

    /* Read the current frame number */

    /* 0 <= next <= current */

    if (pRequestInfo->uFrameNumberNext <= uCurrentFrameNumber )
        {
        semTake(pRequestInfo->pHCDPipe->pPipeSynchMutex,WAIT_FOREVER);
        pHCDPipe->uDmaChannel = pHCDPipe->uEndPointEngineAsigned;

        pRequestInfo->uTransferFlag |= USB_MHCD_REQUEST_STILL_TRANSFERING;
        pHCDPipe->uPipeFlag |= USB_MHCD_PIPE_FLAG_SCHEDULED;

        /* Set Pipe flags and update the frame number */

        pRequestInfo->uFrameNumberLast = uCurrentFrameNumber;

        /*
         * For high speed interrupt endpoint and full/high speed iso endpoint,
         * the interval means
         * (0x1 << (pHCDPipe->bInterval - 1)) micro frame, or
         * (0x1 << (pHCDPipe->bInterval - 4)) frame.
         *
         * So init the uCheckingInterval = 4
         */

        uCheckingInterval = 4;

        switch (pHCDPipe->uEndpointType)
            {
            case USB_ATTR_INTERRUPT:

                if ((pHCDPipe->uSpeed == USBHST_HIGH_SPEED) &&
                    (pHCDPipe->bInterval >= uCheckingInterval))
                    {
                    pRequestInfo->uFrameNumberNext =
                        (UINT16)((pRequestInfo->uFrameNumberLast +
                        (0x1 << (pHCDPipe->bInterval - uCheckingInterval))) %
                        USB_MHDRC_HCD_FRAME_NUM_MAX);

                    }
                else
                    {
                    pRequestInfo->uFrameNumberNext =
                        (UINT16)((pRequestInfo->uFrameNumberLast +
                                  pHCDPipe->bInterval) %
                                  USB_MHDRC_HCD_FRAME_NUM_MAX);
                    }
                break;
            case USB_ATTR_ISOCH:
                if (pHCDPipe->bInterval >= uCheckingInterval)
                    {
                    pRequestInfo->uFrameNumberNext =
                        (UINT16)((pRequestInfo->uFrameNumberLast +
                        (0x1 << (pHCDPipe->bInterval - uCheckingInterval))) %
                        USB_MHDRC_HCD_FRAME_NUM_MAX);
                    }
                break;
            case USB_ATTR_BULK:
                if (pHCDPipe->bInterval >= uCheckingInterval)
                    {
                    pRequestInfo->uFrameNumberNext =
                        (UINT16)((pRequestInfo->uFrameNumberLast +
                        (0x1 << (pHCDPipe->bInterval - uCheckingInterval))) %
                        USB_MHDRC_HCD_FRAME_NUM_MAX);
                    }
                else
                    {
                    pRequestInfo->uFrameNumberNext = pRequestInfo->uFrameNumberLast;
                    }
                break;
            default:
                pRequestInfo->uFrameNumberNext = pRequestInfo->uFrameNumberLast;
                break;
            }

        semGive(pRequestInfo->pHCDPipe->pPipeSynchMutex);

        USB_MHDRC_VDBG("usbMhdrcHcdRequestNeedSchedule(): "
                       "To be scheduled \n",
                       1, 2,3,4, 5 ,6);

        return TRUE;
        }

    /* Next >= 0x6FF, 0x0 <= uCurrentFrameNumber */

    if (0x6FF <= pRequestInfo->uFrameNumberNext)
        {
        semTake(pRequestInfo->pHCDPipe->pPipeSynchMutex,WAIT_FOREVER);
        pHCDPipe->uDmaChannel = pHCDPipe->uEndPointEngineAsigned;

        pRequestInfo->uTransferFlag |= USB_MHCD_REQUEST_STILL_TRANSFERING;
        pHCDPipe->uPipeFlag |= USB_MHCD_PIPE_FLAG_SCHEDULED;

        /* Set Pipe flags and update the frame number */

        pRequestInfo->uFrameNumberLast = uCurrentFrameNumber;

        /*
         * For high speed interrupt endpoint and full/high speed iso endpoint,
         * the interval means
         * (0x1 << (pHCDPipe->bInterval - 1)) micro frame, or
         * (0x1 << (pHCDPipe->bInterval - 4)) frame.
         *
         * So init the uCheckingInterval = 4
         */

        uCheckingInterval = 4;

        switch (pHCDPipe->uEndpointType)
            {
            case USB_ATTR_INTERRUPT:
                if ((pHCDPipe->uSpeed == USBHST_HIGH_SPEED) &&
                    (pHCDPipe->bInterval >= uCheckingInterval))
                    {
                    pRequestInfo->uFrameNumberNext =
                        (UINT16)((pRequestInfo->uFrameNumberLast +
                        (0x1 << (pHCDPipe->bInterval - uCheckingInterval))) %
                        USB_MHDRC_HCD_FRAME_NUM_MAX);
                    }
                else
                    {
                    pRequestInfo->uFrameNumberNext =
                        (UINT16)((pRequestInfo->uFrameNumberLast +
                        pHCDPipe->bInterval) % USB_MHDRC_HCD_FRAME_NUM_MAX);
                    }
                break;
            case USB_ATTR_ISOCH:
                if (pHCDPipe->bInterval >= uCheckingInterval)
                    {
                    pRequestInfo->uFrameNumberNext =
                        (UINT16)((pRequestInfo->uFrameNumberLast +
                        (0x1 << (pHCDPipe->bInterval - uCheckingInterval))) %
                        USB_MHDRC_HCD_FRAME_NUM_MAX);
                    }
                break;
            case USB_ATTR_BULK:
                if (pHCDPipe->bInterval >= uCheckingInterval)
                    {
                    pRequestInfo->uFrameNumberNext =
                        (UINT16)((pRequestInfo->uFrameNumberLast +
                        (0x1 << (pHCDPipe->bInterval - uCheckingInterval))) %
                        USB_MHDRC_HCD_FRAME_NUM_MAX);
                    }
                else
                    {
                    pRequestInfo->uFrameNumberNext =
                        pRequestInfo->uFrameNumberLast;
                    }
                break;
            default:
                pRequestInfo->uFrameNumberNext = pRequestInfo->uFrameNumberLast;
                break;
            }

        semGive(pRequestInfo->pHCDPipe->pPipeSynchMutex);

        USB_MHDRC_VDBG("usbMhdrcHcdRequestNeedSchedule(): "
                      "To be scheduled \n",
                       1, 2,3,4, 5 ,6);

        return TRUE;
        }
    else
        {
        USB_MHDRC_ERR("usbMhdrcHcdRequestNeedSchedule(): Frame number is not correct."
                      "uCurrentFrameNumber = 0x%x, uFrameNumberNext = 0x%x.\n",
                      uCurrentFrameNumber, pRequestInfo->uFrameNumberNext, 3, 4, 5 ,6);

        return FALSE;
        }

    }

/***************************************************************************
*
* usbMhdrcHcdChannelAvailable - check if the endpoint channel is available
*
* This routine check if the endpoint channel is available. If it free, return
* TRUE directly, if it used by another request, check if it can be pause
*
* RETURNS: TRUE if needed or FALSE if not
*
* ERRNO: N/A
*
*
* \NOMANUAL
*/

LOCAL BOOL usbMhdrcHcdChannelAvailable
    (
    pUSB_MHCD_DATA         pHCDData,    /* Pointer to HCD Data */
    UINT8                  uChannel     /* Endpoint channel */
    )
    {
    pUSB_MHCD_REQUEST_INFO pRequestInfo = NULL;
    pUSB_MHCD_PIPE         pHCDPipe = NULL;
    UINT16                 uCurrentFrameNumber = 0;
    UINT8                  uCheckingInterval = 0;
    BOOL                   bIsTimeToReschdule = FALSE;

    /* Parameter varification */

    if (NULL == pHCDData)
        {
        USB_MHDRC_ERR("usbMhdrcHcdChannelAvailable(): "
                     "Invalid parameter, pHCDData is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return FALSE;
        }

    /* Channel hase been paused */

    if ((pHCDData->uFreeEpChannelBitMap & (0x1 << uChannel)) ||
        (NULL == pHCDData->pRequestInChannel[uChannel]))
        {
        USB_MHDRC_VDBG("usbMhdrcHcdChannelAvailable(): "
                      "Channel is not been used, available\n",
                      1, 2, 3, 4, 5 ,6);

        return TRUE;
        }

    pRequestInfo = pHCDData->pRequestInChannel[uChannel];
    pHCDPipe = pRequestInfo->pHCDPipe;

    /*
     * Ok, Only check Interrupt transaction recently.
     * be careful when used on other transaction type
     *
     * Here is also one workaround to deal with the hub
     * interrupt pipe. But this will afect multiple hubs
     * using.
     *
     * For bulk and other pipe, do not stop the original transaction
     */

    if (pHCDPipe->uEndpointType != USB_ATTR_INTERRUPT)
        {
        USB_MHDRC_VDBG("usbMhdrcHcdChannelAvailable(): "
                      "Un-Interrupt channel, no pause\n",
                      1, 2, 3, 4, 5 ,6);

        return FALSE;
        }
    /* Get the need scheduled request's pipe*/

    usbMhdrcHcdGetFrameNumber(pHCDData->uBusIndex,&uCurrentFrameNumber);

    /* Check if we need stop the corrent transaction temporarily */

    if (uCurrentFrameNumber < pRequestInfo->uFrameNumberLast)
        {
        uCheckingInterval =
            (UINT8)(0x7FF - pRequestInfo->uFrameNumberLast + uCurrentFrameNumber);
        }
    else
        {
        uCheckingInterval =
            (UINT8)(uCurrentFrameNumber - pRequestInfo->uFrameNumberLast);
        }

   /* 3 frames will enough to switch */

   if (uCheckingInterval > USB_MHDRC_HCD_TRANSFER_INTERVAL_TO_SWITCH)
        {
        bIsTimeToReschdule = TRUE;
        }

    if (bIsTimeToReschdule)
        {
        usbMhdrcHcdRequestTransferPause (pHCDData,pRequestInfo);

        semTake(pRequestInfo->pHCDPipe->pPipeSynchMutex,WAIT_FOREVER);

        /* Update the frame number */

        pRequestInfo->uFrameNumberLast = uCurrentFrameNumber;

        /* Clean the flag of the occupied request */

        pHCDPipe->uPipeFlag =
            (UINT8)(pHCDPipe->uPipeFlag & ~USB_MHCD_PIPE_FLAG_SCHEDULED);
        pRequestInfo->uTransferFlag =
            (UINT8)(pRequestInfo->uTransferFlag &
                ~USB_MHCD_REQUEST_STILL_TRANSFERING);
        semGive(pRequestInfo->pHCDPipe->pPipeSynchMutex);

        semTake(pHCDData->pHcdSynchMutex, WAIT_FOREVER);
        pHCDData->pRequestInChannel[uChannel] = NULL;
        pHCDData->uFreeEpChannelBitMap |= (UINT16)(0x1 << uChannel);
        semGive(pHCDData->pHcdSynchMutex);

        USB_MHDRC_DBG("usbMhdrcHcdChannelAvailable(): "
                      "Channel %p paused, can be used by other request now\n",
                      uChannel, 2, 3, 4, 5 ,6);

        return TRUE;
        }

    USB_MHDRC_VDBG("usbMhdrcHcdChannelAvailable(): "
                  "Channel %p still used by pRequestInfo %p \n",
                  uChannel,
                  pRequestInfo, 3, 4, 5 ,6);

   return FALSE;

   }


/***************************************************************************
*
* usbMhdrcHcdPipeChannelReasign - resign the pipe channel for transaction
*
* This routine check if the pipe need reasign the channel for transaction.
* This routine will full use of the channels on the platform, whose endpoint
* channels are limitted.
*
* RETURNS: TRUE if reasigned or FALSE if not
*
* ERRNO: N/A
*
*
* \NOMANUAL
*/

LOCAL BOOL usbMhdrcHcdPipeChannelReasign
    (
    pUSB_MHCD_DATA         pHCDData,    /* Pointer to HCD Data */
    pUSB_MHCD_PIPE         pHCDPipe     /* Endpoint channel */
    )
    {
    UINT16 uCheckingChannels;
    UINT8  uChannleIndex;
    UINT32 uMaxFifoSize;
    pUSB_MUSBMHDRC  pMHDRC;

    /* Parameter varification */

    if ((NULL == pHCDData) ||
        (NULL == pHCDData->pMHDRC) ||
        (NULL == pHCDPipe))
        {
        USB_MHDRC_ERR("usbMhdrcHcdPipeChannelReasign(): "
                     "Invalid parameter, %s is NULL\n",
                     ((NULL == pHCDData) ? "pHCDData" :
                     "pHCDPipe"),
                     2, 3, 4, 5 ,6);

        return FALSE;
        }

    pMHDRC = pHCDData->pMHDRC;

    uChannleIndex = pHCDPipe->uEndPointEngineAsigned;

    /*
     * This routine is used to find one available channel for the pipe.
     * 1. The the Pipe's original Asigned channel is available?
     *  - if yes, return
     * 2. or not, the pipe is used by itself?
     *  - if yes, return
     * 3. or, the pipe is used by others
     *  - is there any free channel
     *  - if yes, find one
     *  - if not, find one channel which can pause now.
     */

    if (pHCDData->pRequestInChannel[uChannleIndex] == NULL)
        {
        USB_MHDRC_VDBG("usbMhdrcHcdPipeChannelReasign(): "
                      "Chanel %p free, no need reasign\n",
                      uChannleIndex, 2, 3, 4, 5 ,6);

        return TRUE;
        }

    if ((pHCDData->pRequestInChannel[uChannleIndex]) &&
        (pHCDData->pRequestInChannel[uChannleIndex]->pHCDPipe == pHCDPipe))
        {
        /* If the channel has been occupied by this pipe, do not reroute it */

        USB_MHDRC_VDBG("usbMhdrcHcdPipeChannelReasign(): "
                      "Chanel %p used by the pipe itself \n",
                       uChannleIndex, 2, 3, 4, 5 ,6);

         return FALSE;
        }
    else
        {
        /* The original channel is using by other pipe*/

        uCheckingChannels =
            (UINT16)((pHCDPipe->uEndpointType == USB_ATTR_INTERRUPT) ?
                        USB_MHDRC_HCD_INTERRUPT_CHANNEL_MASK :
                        USB_MHDRC_HCD_BULK_CHANNEL_MASK);

        /* Find one unused channel */

        for (uChannleIndex = 1; uChannleIndex < pMHDRC->uNumEps;
             uChannleIndex++)
            {
             /*
              * Refer to 1.1.4.32: the fifo size is 0x1 << (m+3) or
              * 0x1 << (m+4) if use double buffer. But the Max packet size
              * still 0x1 << (m+3)
              * Meanwhile, the last 4 bit (0xF) is valid
              */

            uMaxFifoSize =
                (UINT32)(0x1 << ((pMHDRC->uRxFifoSize[uChannleIndex] & 0xF) + 3));

            if ((pHCDData->pRequestInChannel[uChannleIndex] == NULL) &&
                (uCheckingChannels & (0x1 << uChannleIndex)) &&
                (uMaxFifoSize >= pHCDPipe->uMaximumPacketSize))
                {
                 semTake(pHCDPipe->pPipeSynchMutex,WAIT_FOREVER);
                 pHCDPipe->uEndPointEngineAsigned = uChannleIndex;
                 semGive(pHCDPipe->pPipeSynchMutex);

                USB_MHDRC_VDBG("usbMhdrcHcdPipeChannelReasign(): "
                              "Channel %d is free, reasign it to pipe %p \n",
                              uChannleIndex,
                              pHCDPipe, 3, 4, 5 ,6);

                 return TRUE;
                }
            }

        /* All the channel has been used by other pipes, find one can used */
        if (pMHDRC->uLastPausedEp == pMHDRC->uNumEps - 1)
           {
           pMHDRC->uLastPausedEp = 0;
           }

        for (uChannleIndex = (UINT8)(1 + pMHDRC->uLastPausedEp);
             uChannleIndex < pMHDRC->uNumEps;
             uChannleIndex++)
            {
            /*
             * Refer to 1.1.4.32: the fifo size is 0x1 << (m+3) or
             * 0x1 << (m+4) if use double buffer. But the Max packet size
             * still 0x1 << (m+3)
             * Meanwhile, the last 4 bit (0xF) is valid
             */

            uMaxFifoSize =
                (UINT32)(0x1 << ((pMHDRC->uRxFifoSize[uChannleIndex] & 0xF) + 3));

            if ((uCheckingChannels & (0x1 << uChannleIndex)) &&
                (uMaxFifoSize >= pHCDPipe->uMaximumPacketSize))
                {
                if (usbMhdrcHcdChannelAvailable(pHCDData, uChannleIndex))
                    {
                     /* Record the last paused Ep */

                     pMHDRC->uLastPausedEp = uChannleIndex;

                     semTake(pHCDPipe->pPipeSynchMutex,WAIT_FOREVER);
                     pHCDPipe->uEndPointEngineAsigned = uChannleIndex;
                     semGive(pHCDPipe->pPipeSynchMutex);
                     USB_MHDRC_VDBG("usbMhdrcHcdPipeChannelReasign(): "
                                   "No free channel, but Chanel %p can paused, "
                                   "reasign it to pipe %p \n",
                                   uChannleIndex,
                                   pHCDPipe, 3, 4, 5 ,6);
                     return TRUE;
                    }
                }
            }

        }

     pMHDRC->uLastPausedEp = 0;

     USB_MHDRC_VDBG("usbMhdrcHcdPipeChannelReasign(): "
                   "Find no available free channel this time for %p,"
                   "return for next try\n",
                   pHCDPipe, 2, 3, 4, 5 ,6);

    return FALSE;

   }



/***************************************************************************
*
* usbMhdrcHcdPipeNeedSchedule - check if the request can be scheduled
*
* This routine checks if the request can be scheduled
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcHcdPipeScheduleHandle
    (
    pUSB_MHCD_DATA    pHCDData,    /* Pointer to HCD Data */
    pUSB_MHCD_PIPE    pHCDPipe
    )
    {
    pUSB_MHCD_REQUEST_INFO pRequestInfo = NULL;
    NODE *                 pNode = NULL;
    UINT8                  uChannel;

    /* Parameter varification */

    if ((NULL == pHCDData) ||
        (NULL == pHCDPipe))
        {
        /* Debug print */

        USB_MHDRC_ERR("usbMhdrcHcdPipeScheduleHandle(): "
                     "Invalid parameter,%s is NULL\n",
                     ((NULL == pHCDData) ? "pHCDData" :
                     "pHCDPipe"),
                     2, 3, 4, 5 ,6);

        return ;
        }

    /* If there is no devices on the root hub, delete all the pipes */

    /*
     * Since the Endpoint engine is limitted, if one interrupt
     * request occupy one Endpoint, and the device have no data
     * to transfer, the host will always receive NAK. But the
     * Host controller have no way to tell the software that device
     * NAKing.It may be sucked.
     *
     * 1. stop the Endpoint
     * 2. schedule another request
     *
     */

    /*
     * Now, we want to schedule this pipe.
     * 1. Check this pipe has been already scheduled or not
     * 2. Check the request of this pipe is still transfering or not
     * 3. Find one available channel to schedule this pipe
     *
     */

    if ((pHCDPipe->uPipeFlag & USB_MHCD_PIPE_FLAG_DELETE) ||
        (pHCDPipe->uPipeFlag & USB_MHCD_PIPE_FLAG_SCHEDULED))
        {

        if (NULL == pHCDData->pRequestInChannel[pHCDPipe->uEndPointEngineAsigned])
            {
            USB_MHDRC_DBG("usbMhdrcHcdPipeScheduleHandle(): 1 "
                          "pHCDPipe %p scheduled or to be delete,flag %p\n",
                          pHCDPipe,
                          pHCDPipe->uPipeFlag, 3, 4, 5 ,6);

            pHCDPipe->uPipeFlag =
                (UINT8)(pHCDPipe->uPipeFlag & ~USB_MHCD_PIPE_FLAG_SCHEDULED);
            }
        else
            {
            USB_MHDRC_VDBG("usbMhdrcHcdPipeScheduleHandle(): 2 "
                          "pHCDPipe %p scheduled or to be delete,flag %p ep %x type %x\n",
                          pHCDPipe,
                          pHCDPipe->uPipeFlag,
                          pHCDPipe->uEndpointAddress,
                          pHCDPipe->uEndpointType, 5 ,6);
            }

        return;
        }

    if (pHCDPipe->uEndpointType == USB_ATTR_CONTROL)
        {
        /*
         * For control pipe, we schedule one by one, if the channel 0
         * is using, do not schedule it.
         */

        if (pHCDData->pRequestInChannel[0])
            {
            USB_MHDRC_VDBG("usbMhdrcHcdPipeScheduleHandle(): "
                          "Control channel and still transfering \n",
                          1, 2, 3, 4, 5 ,6);

            return;
            }
        }

    /* No need be scheduled */

    /* Check the first Request */

    semTake(pHCDPipe->pPipeSynchMutex, WAIT_FOREVER);
    pNode = lstFirst(&(pHCDPipe->RequestInfoList));
    semGive (pHCDPipe->pPipeSynchMutex);

    if (pNode != NULL)
        {
        pRequestInfo = (pUSB_MHCD_REQUEST_INFO)(pNode);
        if (usbMhdrcHcdRequestNeedSchedule(pHCDData, pRequestInfo))
            {
            /*
             * This entry will mark the related channel to busy,
             * and start deal with the request
             */

            /* For other kind of pipe, try to find one free channel to schedule */

            if (usbMhdrcHcdPipeChannelReasign(pHCDData,pHCDPipe) == FALSE)
                {
                USB_MHDRC_DBG("usbMhdrcHcdPipeScheduleHandle reasign fail\n",
                            1,2,3,4,5,6);

                return;
                }

            uChannel = pHCDPipe->uEndPointEngineAsigned;

            semTake(pHCDData->pHcdSynchMutex, WAIT_FOREVER);

            pHCDData->uFreeEpChannelBitMap =
                (UINT16)(pHCDData->uFreeEpChannelBitMap & ~(0x1 << uChannel));
            pHCDData->pRequestInChannel[uChannel] = pRequestInfo;
            semGive (pHCDData->pHcdSynchMutex);

            USB_MHDRC_VDBG("usbMhdrcHcdPipeScheduleHandle(): "
                          "Reasign pRequestInfo %p dev %p Ep %p to CH %p\n",
                           pRequestInfo,
                           pHCDPipe->uDeviceAddress,
                           pHCDPipe->uEndpointAddress,
                           uChannel,
                           5 ,6);

            usbMhdrcHcdNewRequestProcessStart(pHCDData,
                                          pRequestInfo);
            }
        else
            {
            USB_MHDRC_VDBG("usbMhdrcHcdPipeScheduleHandle(): "
                          "pipe %p with dev %p Ep %p unscheduled\n",
                          pHCDPipe,
                          pHCDPipe->uDeviceAddress,
                          pHCDPipe->uEndpointAddress,4,5,6);
            }
        }

     return;
    }

/***************************************************************************
*
* usbMhdrcHcdRequestTransferDone - finish the request transfaction of the Hcd
*
* This routine cleans the request channel of the Hcd, which allowed other
* request to use the channel, and finish the transaction .
*
* RETURNS: N/A.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcHcdRequestTransferDone
    (
    pUSB_MHCD_DATA         pHCDData,
    pUSB_MHCD_REQUEST_INFO pRequestInfo,
    USBHST_STATUS          uCompleteStatus
    )
    {
    pUSB_MHCD_PIPE pHCDPipe = NULL;
    UINT8          uChannel;


    if((NULL == pHCDData) ||
       (NULL == pRequestInfo) ||
       (NULL == pRequestInfo->pHCDPipe)||
       (NULL == pRequestInfo->pUrb))
       {
       USB_MHDRC_ERR("usbMhdrcHcdRequestTransferDone(): "
                     "Invalid parameter, %s is NULL\n",
                     ((NULL == pHCDData) ? "pHCDData" :
                     (NULL == pRequestInfo) ? "pRequestInfo" :
                     (NULL == pRequestInfo->pHCDPipe) ? "pRequestInfo->pHCDPipe" :
                     "pRequestInfo->pUrb"),
                     2, 3, 4, 5 ,6);

       return;
       }

    /* Get the endpoint channel used by the request*/
    pHCDPipe = pRequestInfo->pHCDPipe;
    uChannel = pHCDPipe->uEndPointEngineAsigned;

    /* Clean up the channel to allow other request to use */
    /* TODO: Check more about this s_z */

    if (pRequestInfo == pHCDData->pRequestInChannel[uChannel])
        {
        semTake(pHCDData->pHcdSynchMutex, WAIT_FOREVER);
        pHCDData->pRequestInChannel[uChannel] = NULL;
        pHCDData->uFreeEpChannelBitMap |= (UINT16)(0x1 << uChannel);
        semGive(pHCDData->pHcdSynchMutex);

        USB_MHDRC_VDBG("usbMhdrcHcdRequestTransferDone(): "
                     "Free pRequestInfo %p dev %p Ep %p on CH %p [%x]\n",
                     pRequestInfo,
                     pHCDPipe->uDeviceAddress,
                     pHCDPipe->uEndpointAddress,
                     uChannel,
                     pRequestInfo->pUrb->pTransferBuffer[0],6);

        }

     /* Exclusively access the request list */

    semTake(pHCDPipe->pPipeSynchMutex, WAIT_FOREVER);
    pHCDPipe->uPipeFlag =
        (UINT8)(pHCDPipe->uPipeFlag & ~USB_MHCD_PIPE_FLAG_SCHEDULED);
    if (0 == (pRequestInfo->uTransferFlag & USB_MHCD_REQUEST_TRACTION_FINISH))
        {
        /* If tranfer finished or not successfully */

        if ((pRequestInfo->uActLength >= pRequestInfo->uXferSize)||
            (USBHST_SUCCESS != uCompleteStatus) ||
            (USB_MHCD_REQUEST_STAGE_DONE == pRequestInfo->uStage))
            {
            pRequestInfo->uTransferFlag |= USB_MHCD_REQUEST_TRACTION_FINISH;
            pRequestInfo->uTransferFlag =
                (UINT8)(pRequestInfo->uTransferFlag &
                    ~USB_MHCD_REQUEST_STILL_TRANSFERING);
            }
        }
    semGive(pHCDPipe->pPipeSynchMutex);

    if (pRequestInfo->uTransferFlag & USB_MHCD_REQUEST_TRACTION_FINISH)
        {
        usbMhdrcHcdRequestStopAndCallBack(pHCDData,
                                      pRequestInfo,
                                      uCompleteStatus);
        }

    /* Call back has been called, send MsgQ to remove it from the request list */

    USB_MHDRC_VDBG("usbMhdrcHcdRequestTransferDone(): "
                  "Call back done, to removed\n",
                  1, 2, 3, 4, 5 ,6);

    usbMhdrcHcdTransferTaskFeed(pHCDData,
                           pRequestInfo,
                           USB_MHCD_TRANSFER_CMD_COMPLITE_PROCESS,
                           uCompleteStatus);

    return;
    }

/***************************************************************************
*
* usbMhdrcHcdRequestCompleteProcess - handle the complete process to the request
*
* This routine does complete process of the request.
*
* RETURNS: N/A.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcHcdRequestCompleteProcess
    (
    pUSB_MHCD_DATA         pHCDData,       /* Pointer to HCD Data */
    pUSB_MHCD_REQUEST_INFO pRequestInfo,   /* Pointer to request info */
    USBHST_STATUS          uCompleteStatus
    )
    {
    pUSB_MHCD_PIPE          pHCDPipe = NULL;
    NODE *                  pNode = NULL;
    UINT8                   uChannel;

    if((NULL == pHCDData) ||
       (NULL == pRequestInfo) ||
       (NULL == pRequestInfo->pHCDPipe)||
       (NULL == pRequestInfo->pUrb))
       {
       USB_MHDRC_ERR("usbMhdrcHcdRequestCompleteProcess(): "
                     "Invalid parameter, %s is NULL\n",
                     ((NULL == pHCDData) ? "pHCDData" :
                     (NULL == pRequestInfo) ? "pRequestInfo" :
                     (NULL == pRequestInfo->pHCDPipe) ? "pRequestInfo->pHCDPipe" :
                     "pRequestInfo->pUrb"),
                     2, 3, 4, 5 ,6);

       return;
       }

    pHCDPipe = pRequestInfo->pHCDPipe;
    uChannel = pHCDPipe->uEndPointEngineAsigned;

    /* To be remove */

    if (OK != semTake(pHCDPipe->pPipeSynchMutex, WAIT_FOREVER))
        {
        USB_MHDRC_ERR("usbMhdrcHcdRequestCompleteProcess(): "
                      "semTake pHCDPipe->pPipeSynchMutex failed.\n",
                      1, 2, 3, 4, 5, 6);
        return;
        }

    if ((pRequestInfo->uTransferFlag & USB_MHCD_REQUEST_TO_REMOVE) &&
        (ERROR != lstFind(&(pHCDPipe->RequestInfoList),
                         &(pRequestInfo->requestNode))))
        {
        if (pHCDPipe->uEndpointType == USB_ATTR_ISOCH)
            {
            pHCDPipe->uIsoPipeCurrentUrbIndex =
                (UINT16)((pHCDPipe->uIsoPipeCurrentUrbIndex + 1) % 0xFFFF);
            }

        USB_MHDRC_VDBG("usbMhdrcHcdRequestCompleteProcess(): "
                      "Removed from request list and Free pRequestInfo %p\n",
                      pRequestInfo, 2, 3, 4, 5 ,6);

        /* Delete request From related list */

        lstDelete(&(pHCDPipe->RequestInfoList),
                  &(pRequestInfo->requestNode));

        OS_FREE(pRequestInfo);
        pRequestInfo = NULL;

        }
    (void)semGive(pHCDPipe->pPipeSynchMutex);

    /*
     * If there is no request belong to the pipe, we can delete it here,
     */

    if (pHCDPipe->uPipeFlag & USB_MHCD_PIPE_FLAG_DELETE)
        {
        if (lstCount(&(pHCDPipe->RequestInfoList)) == 0)
            {
            if (OK != semTake(pHCDData->pHcdSynchMutex, WAIT_FOREVER))
                {
                USB_MHDRC_ERR("usbMhdrcHcdRequestCompleteProcess(): "
                              "semTake pHCDData->pHcdSynchMutex failed.\n",
                              1, 2, 3, 4, 5 ,6);
                return;
                };

            switch(pHCDPipe->uEndpointType)
                {
                case USB_ATTR_CONTROL:
                case USB_ATTR_BULK:
                     if(ERROR != lstFind(&(pHCDData->NonPeriodicPipeList),
                                          &(pHCDPipe->pipeListNode)))
                          {
                          lstDelete(&(pHCDData->NonPeriodicPipeList),
                                    &(pHCDPipe->pipeListNode));
                          }
                    break;
                case USB_ATTR_INTERRUPT:
                case USB_ATTR_ISOCH:
                     if(ERROR != lstFind(&(pHCDData->PeriodicPipeList),
                                          &(pHCDPipe->pipeListNode)))
                          {
                          lstDelete(&(pHCDData->PeriodicPipeList),
                                    &(pHCDPipe->pipeListNode));
                          }
                    break;
                default:
                    break;
                }

            /* Delete the request info Mutex */

            if (pHCDPipe != pHCDData->pDefaultPipe)
                {
                /* Mutex should be taken before delete it */

                if (OK == semTake(pHCDPipe->pPipeSynchMutex, WAIT_FOREVER))
                    {
                    (void)semDelete (pHCDPipe->pPipeSynchMutex);
                    pHCDPipe->pPipeSynchMutex = NULL;

                    USB_MHDRC_VDBG("usbMhdrcHcdRequestCompleteProcess(): "
                                  "No request on the to be deleted Pipe %p, free it\n",
                                  pHCDPipe, 2, 3, 4, 5 ,6);

                    OSS_FREE(pHCDPipe);
                    pHCDPipe = NULL;
                    }
                }
            else
                {
                pHCDPipe->uPipeFlag = (UINT8)(pHCDPipe->uPipeFlag & ~USB_MHCD_PIPE_FLAG_DELETE);
                if (pHCDPipe == pHCDData->pDefaultPipe)
                    {
                    pHCDPipe->uPipeFlag = (UINT8)(pHCDPipe->uPipeFlag & ~USB_MHCD_PIPE_FLAG_MODIFY_DEFAULT_PIPE);
                    }
                }
            (void)semGive(pHCDData->pHcdSynchMutex);
            }
        else
            {
            pNode = lstFirst(&(pHCDPipe->RequestInfoList));
            if (pNode != NULL)
                {
                usbMhdrcHcdRequestTransferDone(pHCDData,
                                           (pUSB_MHCD_REQUEST_INFO)pNode,
                                           USBHST_TRANSFER_CANCELLED);
                }
            }
        }

    /* Reschedule , to deal with next transaction */

    USB_MHDRC_VDBG("usbMhdrcHcdRequestCompleteProcess(): "
                  "Reschedule\n",
                  1, 2, 3, 4, 5 ,6);

    usbMhdrcHcdTransferTaskFeed(pHCDData,
                NULL,
                USB_MHCD_TRANSFER_CMD_SHEDULE_PROCESS,
                0);


    return ;
    }


/*******************************************************************************
*
* usbMhdrcHcdEp0InterruptHandle - Handle Ep0 endpoint interrupt
*
* RETURNS: N/A
*
* ERRNO: N/A
*
*/

void usbMhdrcHcdEp0InterruptHandle
    (
    pUSB_MHCD_DATA pHCDData  /* Pointer to HCD Data */
    )
    {
    pUSB_MHCD_REQUEST_INFO pRequestInfo = NULL;
    pUSB_MHCD_PIPE         pHCDPipe = NULL;
    pUSBHST_SETUP_PACKET   pSetup = NULL;
    UINT16                 wCsrVal = 0;
    UINT16                 wCount = 0;
    pUSB_MUSBMHDRC         pMHDRC;

    /* Paramenter verification */

    if ((NULL == pHCDData) || (NULL == pHCDData->pMHDRC))
        {
        /* Debug print */

        USB_MHDRC_ERR("usbMhdrcHcdEp0InterruptHandle(): "
                     "Invalid parameter pHCDData is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }

    pMHDRC = pHCDData->pMHDRC;

    pRequestInfo = pHCDData->pRequestInChannel[0];

    if ((NULL == pRequestInfo) ||
        (NULL == pRequestInfo->pHCDPipe))
        {
        USB_MHDRC_DBG("usbMhdrcHcdEp0InterruptHandle(): "
                     "Invalid parameter, %s is NULL\n",
                     ((NULL == pRequestInfo) ? "pRequestInfo" :
                     "pRequestInfo->pHCDPipe"),
                     2, 3, 4, 5 ,6);

        return;
        }

    pHCDPipe = pRequestInfo->pHCDPipe;

    /* Obtain the pointer to the host controller information */

    wCsrVal = USB_MHDRC_REG_READ16 (pMHDRC,
                                  USB_MHDRC_HOST_CSR0);

    wCount = USB_MHDRC_REG_READ16 (pMHDRC,
                                 USB_MHDRC_COUNT0);

    /* Prepare status */

    if (wCsrVal & USB_MHDRC_HOST_CSR0_RXSTALL)
        {
        USB_MHDRC_DBG("usbMhdrcHcdEp0InterruptHandle(): EP0 RXSTALL\n",
                     1, 2, 3, 4, 5 ,6);

        /* Flush FIFO and clean the error bit */

        USB_MHDRC_REG_WRITE16 (pMHDRC,
                              USB_MHDRC_HOST_CSR0,
                              USB_MHDRC_HOST_CSR0_FLUSHFIFO);

        /* Stop the transaction and Call back the urb directly */

         usbMhdrcHcdRequestTransferDone(pHCDData,
                                    pRequestInfo,
                                    USBHST_STALL_ERROR);

        return;
        }
    else if (wCsrVal & USB_MHDRC_HOST_CSR0_ERROR)
        {
        USB_MHDRC_DBG("usbMhdrcHcdEp0InterruptHandle(): EP0 ERROR bit set \n",
                     1, 2, 3, 4, 5 ,6);

        /* Flush FIFO and clean the error bit */

        USB_MHDRC_REG_WRITE16 (pMHDRC,
                              USB_MHDRC_HOST_CSR0,
                              USB_MHDRC_HOST_CSR0_FLUSHFIFO);

        /* Stop the transaction and Call back the urb directly */

        usbMhdrcHcdRequestTransferDone(pHCDData,
                                   pRequestInfo,
                                   USBHST_DEVICE_NOT_RESPONDING_ERROR);
        return;
        }
    else if (wCsrVal & USB_MHDRC_HOST_CSR0_NAKTIMEOUT)
        {
        USB_MHDRC_DBG("usbMhdrcHcdEp0InterruptHandle(): EP0 NAKTIMEOUT bit set \n",
                     1, 2, 3, 4, 5 ,6);

        /* Flush FIFO and clean the error bit */

        USB_MHDRC_REG_WRITE16 (pMHDRC,
                              USB_MHDRC_HOST_CSR0,
                              USB_MHDRC_HOST_CSR0_FLUSHFIFO);

        /* Stop the transaction and Call back the urb directly */

        usbMhdrcHcdRequestTransferDone(pHCDData,
                                   pRequestInfo,
                                   USBHST_TIMEOUT);

         return;
        }

    pSetup = (pUSBHST_SETUP_PACKET)
          pRequestInfo->pUrb->pTransferSpecificData;

    switch (pRequestInfo->uStage)
        {
        case USB_MHCD_REQUEST_STAGE_NON_CONTROL:

            /* Stop the transaction and Call back the urb directly */

            USB_MHDRC_ERR("usbMhdrcHcdEp0InterruptHandle(): "
                         "STAGE_NON_CONTROL, should not happend\n",
                         1, 2, 3, 4, 5 ,6);

            usbMhdrcHcdRequestTransferDone(pHCDData,
                                       pRequestInfo,
                                       USBHST_FAILURE);
            break;
        case USB_MHCD_REQUEST_STAGE_SETUP:
            pHCDPipe->uDataToggle = 1;

            /*
             * Here we do not change to LE, since we do not want to
             * know the existly length
             */

            if (pSetup->wLength)
                {
                pRequestInfo->uStage =
                    USB_MHCD_REQUEST_STAGE_DATA;
                }
            else
                {
                pRequestInfo->uStage =
                    USB_MHCD_REQUEST_STAGE_STATUS;
                }

            USB_MHDRC_VDBG("usbMhdrcHcdEp0InterruptHandle(): "
                          "SETUP stage done\n",
                          1, 2, 3, 4, 5 ,6);

            usbMhdrcHcdTransferTaskFeed(pHCDData,
                                    pRequestInfo,
                                    USB_MHCD_TRANSFER_CMD_CHANNEL_START_PROCESS,
                                    0);
            break;
        case USB_MHCD_REQUEST_STAGE_DATA:

             if (pSetup->bmRequestType & USB_RT_DEV_TO_HOST)
                 {

                 /* IN data status */

                 usbMhdrcFIFORead (pMHDRC,
                                (UINT8 *)(pRequestInfo->pUrb->pTransferBuffer +
                                         pRequestInfo->uActLength),
                                 wCount,
                                 0);

                 /* Flush the FIFO */

                 USB_MHDRC_REG_WRITE16 (pMHDRC,
                         USB_MHDRC_HOST_CSR0,
                         USB_MHDRC_HOST_CSR0_FLUSHFIFO);

                 semTake(pHCDPipe->pPipeSynchMutex, WAIT_FOREVER);

                 /* Update uAcdLenght */

                 pRequestInfo->uActLength += wCount;

                 if ((pRequestInfo->uXferSize <= pRequestInfo->uActLength) ||
                     (wCount < pHCDPipe->uMaximumPacketSize))
                    {
                    pHCDPipe->uDataToggle = 1;
                    pRequestInfo->uStage = USB_MHCD_REQUEST_STAGE_STATUS;
                    }
                else
                    {
                    /* Tick the data toggle */

                    usbMhdrcHcdTickDataToggle(pHCDPipe);

                    }

               semGive(pHCDPipe->pPipeSynchMutex);

                }
             else
                {
                 semTake(pHCDPipe->pPipeSynchMutex, WAIT_FOREVER);

                 /* Update uAcdLenght */

                 pRequestInfo->uActLength += pRequestInfo->uTransactionLength;

                 if ((pRequestInfo->uXferSize <= pRequestInfo->uActLength))
                     {
                     pHCDPipe->uDataToggle = 1;
                     pRequestInfo->uStage = USB_MHCD_REQUEST_STAGE_STATUS;
                     }
                 else
                     {
                     /* Tick the data toggle */

                     usbMhdrcHcdTickDataToggle(pHCDPipe);
                     }
                semGive(pHCDPipe->pPipeSynchMutex);

                }

            /*
             * For OUT tranfer, there is no command need transfer
             * big packet data
             */

            USB_MHDRC_VDBG("usbMhdrcHcdEp0InterruptHandle(): "
                          "DATA stage happen, go on for more data of status stage\n",
                          1, 2, 3, 4, 5 ,6);

            usbMhdrcHcdTransferTaskFeed(pHCDData,
                                    pRequestInfo,
                                    USB_MHCD_TRANSFER_CMD_CHANNEL_START_PROCESS,
                                    0);
            break;
        case USB_MHCD_REQUEST_STAGE_STATUS:

            /* This should be impossible */

            pRequestInfo->uStage = USB_MHCD_REQUEST_STAGE_DONE;

            USB_MHDRC_VDBG("usbMhdrcHcdEp0InterruptHandle(): "
                          "STATUS stage done, complete the request\n",
                          1, 2, 3, 4, 5 ,6);

            /* Stop the transaction and Call back the urb directly */

            usbMhdrcHcdRequestTransferDone(pHCDData,
                                       pRequestInfo,
                                       USBHST_SUCCESS);
            break;
        default:
            break;
        }

    return;
    }


/*******************************************************************************
*
* usbMhdrcHcdTxInterruptHandle - Handle default endpoint interrupt
*
* RETURNS: N/A
*
* ERRNO: N/A
*
*/

void usbMhdrcHcdTxInterruptHandle
    (
    pUSB_MHCD_DATA pHCDData,       /* Pointer to HCD Data */
    UINT8          uEndpointNumber /* Endpoint number */
    )
    {
    pUSB_MHCD_REQUEST_INFO  pRequestInfo = NULL;
    pUSB_MHCD_PIPE          pHCDPipe = NULL;
    UINT16                  wCsrVal = 0;
    pUSBHST_ISO_PACKET_DESC pIsoPacketDesc = NULL;
    pUSB_MUSBMHDRC          pMHDRC;

    /* Paramenter verification */

    if (NULL == pHCDData)
        {
        /* Debug print */

        USB_MHDRC_ERR("usbMhdrcHcdTxInterruptHandle(): "
                     "Invalid parameter, pHCDData is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }

    pRequestInfo = pHCDData->pRequestInChannel[uEndpointNumber];

    if ((NULL == pRequestInfo) ||
        (NULL == pRequestInfo->pHCDPipe))
        {
        USB_MHDRC_DBG("usbMhdrcHcdTxInterruptHandle(): "
                     "Invalid parameter, %s is NULL\n",
                     ((NULL == pRequestInfo) ? "pRequestInfo" :
                     "pRequestInfo->pHCDPipe"),
                     2, 3, 4, 5 ,6);

        return;
        }


    /* Get the common hardware data structure */

    pMHDRC = pHCDData->pMHDRC;

    pHCDPipe = pRequestInfo->pHCDPipe;

    /* Get the CSR register value */

    wCsrVal = USB_MHDRC_REG_READ16 (pMHDRC,
                         USB_MHDRC_HOST_TXCSR_EP(uEndpointNumber));

    /* Check the status */

    if (wCsrVal & USB_MHDRC_HOST_TXCSR_RXSTALL)
        {
        USB_MHDRC_DBG("usbMhdrcHcdTxInterruptHandle(): Endpoint %d RXSTALL bit set \n",
                     uEndpointNumber, 2, 3, 4, 5, 6);

        /* Flush FIFO and clean the error bit */

        USB_MHDRC_REG_WRITE16 (pMHDRC,
                              USB_MHDRC_HOST_TXCSR_EP(uEndpointNumber),
                              USB_MHDRC_HOST_TXCSR_FLUSHFIFO);

        /* Stop the transaction and Call back the urb directly */

        usbMhdrcHcdRequestTransferDone(pHCDData,
                                   pRequestInfo,
                                   USBHST_STALL_ERROR);


        return;
        }
    else if (wCsrVal & USB_MHDRC_HOST_TXCSR_ERROR)
        {
        USB_MHDRC_DBG("usbMhdrcHcdTxInterruptHandle(): Endpoint %d ERROR bit set \n",
                      uEndpointNumber, 2, 3, 4, 5, 6);

        /* Flush FIFO and clean the error bit */

        USB_MHDRC_REG_WRITE16 (pMHDRC,
                              USB_MHDRC_HOST_TXCSR_EP(uEndpointNumber),
                              USB_MHDRC_HOST_TXCSR_FLUSHFIFO);

        /* Stop the transaction and Call back the urb directly */

        usbMhdrcHcdRequestTransferDone(pHCDData,
                                   pRequestInfo,
                                   USBHST_DEVICE_NOT_RESPONDING_ERROR);

        return;
        }
    else if (wCsrVal & USB_MHDRC_HOST_TXCSR_NAKTIMEOUT)
        {
        USB_MHDRC_DBG("usbMhdrcHcdTxInterruptHandle(): Endpoint %d NAKTIMEOUT bit set\n",
                     uEndpointNumber, 2, 3, 4, 5, 6);

        /* Flush FIFO and clean the error bit */

        USB_MHDRC_REG_WRITE16 (pMHDRC,
                              USB_MHDRC_HOST_TXCSR_EP(uEndpointNumber),
                              USB_MHDRC_HOST_TXCSR_FLUSHFIFO);

        /* Stop the transaction and Call back the urb directly */

        usbMhdrcHcdRequestTransferDone(pHCDData,
                                   pRequestInfo,
                                   USBHST_TIMEOUT);
        return;
        }

    if ((wCsrVal & USB_MHDRC_HOST_TXCSR_FIFONOEMPTY) == 0)
        {
         /* For OUT tranfer, there is no command need transfer big packet data */

         semTake(pRequestInfo->pHCDPipe->pPipeSynchMutex,WAIT_FOREVER);

        /* Tick the data toggle */

        usbMhdrcHcdTickDataToggle(pHCDPipe);

        pRequestInfo->uActLength += pRequestInfo->uTransactionLength;
        semGive(pRequestInfo->pHCDPipe->pPipeSynchMutex);

        if (pRequestInfo->uActLength >= pRequestInfo->uXferSize)
            {

            USB_MHDRC_VDBG("usbMhdrcHcdTxInterruptHandle(): Request %p Tx Done on Ep %p\n",
                          pRequestInfo, uEndpointNumber, 3, 4, 5, 6);

            /* Stop the transaction and Call back the urb directly */

            usbMhdrcHcdRequestTransferDone(pHCDData,
                                       pRequestInfo,
                                       USBHST_SUCCESS);

            return;
            }
        else
            {
            if (pHCDPipe->uEndpointType == USB_ATTR_ISOCH)
                {
                pIsoPacketDesc = (pUSBHST_ISO_PACKET_DESC)
                                 (pRequestInfo->pUrb->pTransferSpecificData);
                pIsoPacketDesc += pRequestInfo->uIsoUrbCurrentPacketIndex;

                if (pRequestInfo->uActLength >= (pIsoPacketDesc->uLength +
                                                pIsoPacketDesc->uOffset))
                    {
                    pRequestInfo->uIsoUrbCurrentPacketIndex++;
                    }
                }
            }

        USB_MHDRC_VDBG("usbMhdrcHcdTxInterruptHandle(): "
                      "Request %p Still has data Tx on Ep %p\n",
                      pRequestInfo, uEndpointNumber, 3, 4, 5, 6);

        usbMhdrcHcdTransferTaskFeed(pHCDData,
                                pRequestInfo,
                                USB_MHCD_TRANSFER_CMD_CHANNEL_START_PROCESS,
                                USBHST_SUCCESS);

        }

    return;
    }


/*******************************************************************************
*
* usbMhdrcHcdRxInterruptHandle - Handle default endpoint interrupt
*
* RETURNS: N/A
*
* ERRNO: N/A
*
*/

void usbMhdrcHcdRxInterruptHandle
    (
    pUSB_MHCD_DATA pHCDData,       /* Pointer to HCD Data */
    UINT8          uEndpointNumber /* Endpoint nubmer     */
    )
    {
    pUSB_MHCD_REQUEST_INFO pRequestInfo = NULL;
    pUSB_MHCD_PIPE         pHCDPipe = NULL;
    UINT16                 wCsrVal = 0;
    UINT16                 wCount = 0;
    pUSB_MUSBMHDRC         pMHDRC;
    pUSB_MHDRC_DMA_DATA    pDMAData;

    /* Check whether the MHCI host controller index is valid */

    if ((NULL == pHCDData) || (NULL == pHCDData->pMHDRC))
        {
        /* Debug print */

        USB_MHDRC_ERR("usbMhdrcHcdRxInterruptHandle(): "
                     "Invalid parameter, pHCDData is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }

    pMHDRC = pHCDData->pMHDRC;
    pDMAData = &(pMHDRC->dmaData);

    pRequestInfo = pHCDData->pRequestInChannel[uEndpointNumber];

    if ((NULL == pRequestInfo) ||
        (NULL == pRequestInfo->pHCDPipe))
        {
        USB_MHDRC_DBG("usbMhdrcHcdRxInterruptHandle(): "
                     "Invalid parameter, %s is NULL\n",
                     ((NULL == pRequestInfo) ? "pRequestInfo" :
                     "pRequestInfo->pHCDPipe"),
                     2, 3, 4, 5 ,6);

        return;
        }

    pHCDPipe = pRequestInfo->pHCDPipe;

    /* Get the CSR register value */

    wCsrVal = USB_MHDRC_REG_READ16 (pMHDRC,
                   USB_MHDRC_HOST_RXCSR_EP(uEndpointNumber));

    if (wCsrVal & USB_MHDRC_HOST_RXCSR_RXSTALL)
        {
        /* Stop the transaction and Call back the urb directly */

        USB_MHDRC_DBG("usbMhdrcHcdRxInterruptHandle(): Endpoint %d RXSTALL bit set\n",
                     uEndpointNumber, 2, 3, 4, 5, 6);

        /* Flush FIFO and clean the error bit */

        USB_MHDRC_REG_WRITE16 (pMHDRC,
                              USB_MHDRC_HOST_RXCSR_EP(uEndpointNumber),
                              USB_MHDRC_HOST_RXCSR_FLUSHFIFO);

        usbMhdrcHcdRequestTransferDone(pHCDData,
                                   pRequestInfo,
                                   USBHST_STALL_ERROR);

        return;
        }
    else if (wCsrVal & USB_MHDRC_HOST_RXCSR_ERROR)
        {
        USB_MHDRC_DBG("usbMhdrcHcdRxInterruptHandle(): Endpoint %d ERROR bit set\n",
                     uEndpointNumber, 2, 3, 4, 5, 6);

        /* Flush FIFO and clean the error bit */

        USB_MHDRC_REG_WRITE16 (pMHDRC,
                              USB_MHDRC_HOST_RXCSR_EP(uEndpointNumber),
                              USB_MHDRC_HOST_RXCSR_FLUSHFIFO);

        /* Stop the transaction and Call back the urb directly */

        usbMhdrcHcdRequestTransferDone(pHCDData,
                                   pRequestInfo,
                                   USBHST_DEVICE_NOT_RESPONDING_ERROR);
        return;
        }

    else if (wCsrVal & USB_MHDRC_HOST_RXCSR_NAKTIMEOUT)
        {
        USB_MHDRC_DBG("usbMhdrcHcdRxInterruptHandle(): Endpoint %d NAKTIMEOUT bit set\n",
                     uEndpointNumber, 2, 3, 4, 5, 6);

        /* Flush FIFO and clean the error bit */

        USB_MHDRC_REG_WRITE16 (pMHDRC,
                              USB_MHDRC_HOST_RXCSR_EP(uEndpointNumber),
                              USB_MHDRC_HOST_RXCSR_FLUSHFIFO);

        /* Stop the transaction and Call back the urb directly */

        usbMhdrcHcdRequestTransferDone(pHCDData,
                                   pRequestInfo,
                                   USBHST_TIMEOUT);

        return;
        }

    if (wCsrVal & USB_MHDRC_HOST_RXCSR_RXPKTRDY)
        {
        UINT8 uDmaChannel = 0;

        /* For OUT tranfer, there is no command need transfer big packet data */

        semTake(pRequestInfo->pHCDPipe->pPipeSynchMutex,WAIT_FOREVER);

        /* Tick the data toggle */

        usbMhdrcHcdTickDataToggle(pHCDPipe);

        semGive(pRequestInfo->pHCDPipe->pPipeSynchMutex);

        wCount = USB_MHDRC_REG_READ16 (pMHDRC,
             USB_MHDRC_HOST_RXCOUNT_EP(uEndpointNumber));

        if ((pMHDRC->bDmaEnabled) &&
            (pDMAData->pDmaChannelRequest) &&
            (pDMAData->pDmaConfigure) &&
            (wCount >= 0x40))
            {
            /* Get a free DMA channel */

            uDmaChannel = (UINT8)pDMAData->pDmaChannelRequest(pDMAData);

            if (uDmaChannel != USB_MHDRC_DMA_INVALID_CHANNEL)
                {
                /* OK, receive one packet */

               if ((pMHDRC->uPlatformType != USB_MHDRC_PLATFORM_CENTAURUS) &&
                   (pMHDRC->uPlatformType != USB_MHDRC_PLATFORM_CENTAURUS_TI816X))
                   {
                   wCsrVal = (UINT16)(wCsrVal & ~USB_MHDRC_HOST_RXCSR_REQPKT);

                   wCsrVal |= USB_MHDRC_HOST_RXCSR_DMAEN |
                              USB_MHDRC_HOST_RXCSR_AUTOCLEAR;

                   USB_MHDRC_REG_WRITE16 (pMHDRC,
                                          USB_MHDRC_HOST_RXCSR_EP(uEndpointNumber),
                                          wCsrVal);
                   }

                pRequestInfo->pCurrentBuffer = pRequestInfo->pUrb->pTransferBuffer +
                                                pRequestInfo->uActLength;

                (pDMAData->pDmaConfigure)(pMHDRC,
                                          pRequestInfo,
                                          (ULONG)(pRequestInfo->pCurrentBuffer),
                                          wCount,
                                          uDmaChannel,
                                          0,
                                          pHCDPipe->uEndPointEngineAsigned,
                                          (BOOLEAN)(pHCDPipe->uEndpointDir == USB_DIR_OUT),
                                          pHCDPipe->uMaximumPacketSize);
                return;
                }
            }


        /*
         * If we get here, means
         * 1. The wCount <= 0x40, no need use DMA
         * 2. or, the DMA is not enabled
         * 3. or, all the DMA channels are used
         * 4. or, there is something wrong on the DMA
         * we will use directly access FIFO
         */

        usbMhdrcFIFORead (pMHDRC,
                        (UINT8 *)(pRequestInfo->pUrb->pTransferBuffer +
                                 pRequestInfo->uActLength),
                         wCount,
                         uEndpointNumber);

        /* Update uAcdLenght */

        pRequestInfo->uActLength += wCount;

        /* Flush the FIFO */

        USB_MHDRC_REG_WRITE16 (pMHDRC,
                              USB_MHDRC_HOST_RXCSR_EP(uEndpointNumber),
                              USB_MHDRC_HOST_RXCSR_FLUSHFIFO);

        USB_MHDRC_REG_WRITE8 (pMHDRC,
                             USB_MHDRC_HOST_RXINTERVAL_EP(uEndpointNumber),
                             0);

        USB_MHDRC_VDBG("usbMhdrcHcdRxInterruptHandle(): "
                      "uXferSize %p uActLength %p wCount %p uMaximumPacketSize %p\n",
                      pRequestInfo->uXferSize,
                      pRequestInfo->uActLength,
                      wCount,
                      pHCDPipe->uMaximumPacketSize, 5 ,6);

        /* For OUT tranfer, there is no command need transfer big packet data */

        if ((pRequestInfo->uXferSize <= pRequestInfo->uActLength) ||
            ((wCount < pHCDPipe->uMaximumPacketSize) &&
             (pHCDPipe->uEndpointType != USB_ATTR_ISOCH)))
            {
            semTake(pHCDPipe->pPipeSynchMutex, WAIT_FOREVER);
            pRequestInfo->uTransferFlag |= USB_MHCD_REQUEST_TRACTION_FINISH;
            semGive(pHCDPipe->pPipeSynchMutex);

            /* Stop the transaction and Call back the urb directly */

            usbMhdrcHcdRequestTransferDone(pHCDData,
                                       pRequestInfo,
                                       USBHST_SUCCESS);

            return;
            }

        usbMhdrcHcdTransferTaskFeed(pHCDData,
                                pRequestInfo,
                                USB_MHCD_TRANSFER_CMD_CHANNEL_START_PROCESS,
                                0);


        }
    return;
    }



/*******************************************************************************
*
* usbMhdrcHcdControlPipeSetupPhaseStart - start the setup phase transfer
*
* This routine starts the setup phase transfer of the control pipe.
*
* RETURNS: N/A.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcHcdControlPipeSetupPhaseStart
    (
    pUSB_MHCD_DATA         pHCDData,    /* Pointer to HCD Data     */
    pUSB_MHCD_REQUEST_INFO pRequestInfo /* Pointer to request info */
    )
    {
    /* For Control pipe, we do not use DMA */

    pUSBHST_URB          pURB = NULL;
    pUSBHST_SETUP_PACKET pSetup = NULL;
    pUSB_MHCD_PIPE       pHCDPipe = NULL;
    UINT16               uHostCsr0Reg16 = 0;
    UINT8                uHubAddr = 0x0;
    UINT8                uPortAddr = 0x0;
    UINT8                uTypeReg8 = 0;
    pUSB_MUSBMHDRC       pMHDRC;

    /* Parameter verification*/

    if ((NULL == pHCDData) ||
        (NULL == pHCDData->pMHDRC) ||
        (NULL == pRequestInfo) ||
        (NULL == pRequestInfo->pHCDPipe) ||
        (NULL == pRequestInfo->pUrb) ||
        (NULL == pRequestInfo->pUrb->pTransferSpecificData))
        {
        USB_MHDRC_ERR("usbMhdrcHcdControlPipeSetupPhaseStart(): "
                     "Invalid parameter, %s is NULL\n",
                     ((NULL == pHCDData) ? "pHCDData" :
                     (NULL == pRequestInfo) ? "pRequestInfo" :
                     (NULL == pRequestInfo->pHCDPipe) ? "pRequestInfo->pHCDPipe" :
                     (NULL == pRequestInfo->pUrb) ? "pRequestInfo->pUrb" :
                     "pRequestInfo->pUrb->pTransferSpecificData"),
                     2, 3, 4, 5 ,6);

        return;
        }

    pMHDRC = pHCDData->pMHDRC;

    pHCDPipe = pRequestInfo->pHCDPipe;

    pURB = pRequestInfo->pUrb;

    pSetup = (pUSBHST_SETUP_PACKET)pURB->pTransferSpecificData;
    OS_BUFFER_CPU_TO_LE(pSetup, sizeof(USBHST_SETUP_PACKET));

    uHostCsr0Reg16 = USB_MHDRC_REG_READ16(pMHDRC,
                                         USB_MHDRC_HOST_CSR0);

    /* Flush fifo */

    uHostCsr0Reg16 |= USB_MHDRC_HOST_CSR0_FLUSHFIFO;
    USB_MHDRC_REG_WRITE16 (pMHDRC,
                          USB_MHDRC_HOST_CSR0,
                          uHostCsr0Reg16);
    USB_MHDRC_REG_WRITE16 (pMHDRC,
                          USB_MHDRC_HOST_CSR0,
                          uHostCsr0Reg16);

    /* Fill the setup packet to the FIFO */

    usbMhdrcFIFOWrite(pMHDRC,
                     (UINT8 *)pSetup,
                     USB_SETUP_PACKET_SIZE,
                     0);

    /*
     * Make sure the Data toggle is correct
     * Seems not need check that. ignore!
     */

    /* Update the device address, hub address and hubport */

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                         USB_MHDRC_TXFUNCADDR_EP(0),
                         pHCDPipe->uDeviceAddress);

    if (pMHDRC->uPlatformType == USB_MHDRC_PLATFORM_CENTAURUS_TI816X)
        {
        /* TI816X doesn't support USB hub (due to multipoint not supported in hardware). */

        USB_MHDRC_REG_WRITE8 (pMHDRC,
                              USB_MHDRC_FADD,
                              pHCDPipe->uDeviceAddress);
        }


    /*
     * Add the MTT information 0x80(HUB_MASK_MULTIPLE_TT) in uHbuInfo
     * Add the MTT information with hub address (mask with 0x7F)
     */

    uHubAddr = (UINT8)(((pHCDPipe->uHubInfo >> 8) & 0x7F) | (0x80 & pHCDPipe->uHubInfo));
    uPortAddr = (UINT8)((pHCDPipe->uHubInfo) & 0x7f);

    /* TODO: It seems that we need record the hub TT*/

    switch (pHCDPipe->uSpeed)
        {
        case USBHST_HIGH_SPEED:
           uTypeReg8 |= USB_MHDRC_HOST_TYPE_SPEED_HIGH;
           break;
        case USBHST_FULL_SPEED:
           uTypeReg8 |= USB_MHDRC_HOST_TYPE_SPEED_FULL;
           break;
        case USBHST_LOW_SPEED:
           uTypeReg8 |= USB_MHDRC_HOST_TYPE_SPEED_LOW;
           break;
        default:
            break;
        }
    /* Update the device speed */

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                         USB_MHDRC_HOST_TYPE0,
                         uTypeReg8);

    /* Update the hub address and port */

    if (pMHDRC->uPlatformType != USB_MHDRC_PLATFORM_CENTAURUS_TI816X)
        {
        USB_MHDRC_REG_WRITE8 (pMHDRC,
                             USB_MHDRC_TXHUBADDR_EP(0),
                             uHubAddr);

        USB_MHDRC_REG_WRITE8 (pMHDRC,
                             USB_MHDRC_TXHUBPORT_EP(0),
                             uPortAddr);
        }

    /* Start Setup pack transfer */

    uHostCsr0Reg16 = USB_MHDRC_HOST_CSR0_SETUPPKT | USB_MHDRC_HOST_CSR0_TXPKTRDY;

    USB_MHDRC_REG_WRITE16 (pMHDRC,
                          USB_MHDRC_HOST_CSR0,
                          uHostCsr0Reg16);

    USB_MHDRC_VDBG("usbMhdrcHcdControlPipeSetupPhaseStart(): "
                  "Triggered\n",
                  1, 2, 3, 4, 5 ,6);

    return;
    }


/***************************************************************************
*
* usbMhdrcHcdControlPipeINDataPhaseStart - start the IN data phase transfer
*
* This routine starts the IN data phase transfer of the control pipe.
*
* RETURNS: N/A.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcHcdControlPipeINDataPhaseStart
    (
    pUSB_MHCD_DATA         pHCDData,    /* Pointer to HCD Data     */
    pUSB_MHCD_REQUEST_INFO pRequestInfo /* Pointer to request info */
    )
    {
    /* For Control pipe, we do not use DMA */

    pUSB_MHCD_PIPE       pHCDPipe = NULL;
    UINT16               uHostCsr0Reg16 = 0;
    UINT8                uHubAddr = 0x0;
    UINT8                uHubPort = 0x0;
    UINT8                uTypeReg8 = 0;
    pUSB_MUSBMHDRC       pMHDRC;

    /* Parameter verification*/

    if ((NULL == pHCDData) ||
        (NULL == pRequestInfo) ||
        (NULL == pRequestInfo->pHCDPipe) ||
        (NULL == pRequestInfo->pUrb) ||
        (NULL == pRequestInfo->pUrb->pTransferBuffer))
        {
        USB_MHDRC_ERR("usbMhdrcHcdControlPipeINDataPhaseStart(): "
                     "Invalid parameter, %s is NULL\n",
                     ((NULL == pHCDData) ? "pHCDData" :
                     (NULL == pRequestInfo) ? "pRequestInfo" :
                     (NULL == pRequestInfo->pHCDPipe) ? "pRequestInfo->pHCDPipe" :
                     (NULL == pRequestInfo->pUrb) ? "pRequestInfo->pUrb" :
                     "pRequestInfo->pUrb->pTransferBuffer"),
                     2, 3, 4, 5 ,6);

        return;
        }


    /* Get the common hardware data structure */

    pMHDRC = pHCDData->pMHDRC;

    /* For In data phase, we only need set REQPKT of HOST_CSR0 */

    pHCDPipe = pRequestInfo->pHCDPipe;

    uHostCsr0Reg16 = USB_MHDRC_REG_READ16(pMHDRC,
                                         USB_MHDRC_HOST_CSR0);

    /* Flush fifo */

    uHostCsr0Reg16 |= USB_MHDRC_HOST_CSR0_FLUSHFIFO;
    USB_MHDRC_REG_WRITE16 (pMHDRC,
                          USB_MHDRC_HOST_CSR0,
                          uHostCsr0Reg16);

    uHostCsr0Reg16 = 0;

    /*
     * Make sure the Data toggle is correct
     * Seems not need check that.ignor?
     */

    if (usbMhdrcHcdGetDataToggle(pHCDPipe))
        {
        uHostCsr0Reg16 = USB_MHDRC_HOST_CSR0_DATATOGWREN |
                         USB_MHDRC_HOST_CSR0_DATATOG;
        }
    else
        {
        uHostCsr0Reg16 = USB_MHDRC_HOST_CSR0_DATATOGWREN;
        }

    USB_MHDRC_REG_WRITE16 (pMHDRC,
                          USB_MHDRC_HOST_CSR0,
                          uHostCsr0Reg16);

    uHostCsr0Reg16 = 0;

    /* Update the device address, hub address and hubport */

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                         USB_MHDRC_RXFUNCADDR_EP(0),
                         pHCDPipe->uDeviceAddress);

    switch (pHCDPipe->uSpeed)
        {
        case USBHST_HIGH_SPEED:
           uTypeReg8 |= USB_MHDRC_HOST_TYPE_SPEED_HIGH;
           break;
        case USBHST_FULL_SPEED:
           uTypeReg8 |= USB_MHDRC_HOST_TYPE_SPEED_FULL;
           break;
        case USBHST_LOW_SPEED:
           uTypeReg8 |= USB_MHDRC_HOST_TYPE_SPEED_LOW;
           break;
        default:
            break;
        }

    /* Update the device speed */

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                         USB_MHDRC_HOST_TYPE0,
                         uTypeReg8);

    uHubAddr = (UINT8)((pHCDPipe->uHubInfo >> 8) & 0x7f);
    uHubPort = (UINT8)((pHCDPipe->uHubInfo) & 0x7f);

    /* TODO: It seems that we need record the hub TT*/

    if (pMHDRC->uPlatformType != USB_MHDRC_PLATFORM_CENTAURUS_TI816X)
        {
        USB_MHDRC_REG_WRITE8 (pMHDRC,
                             USB_MHDRC_RXHUBADDR_EP(0),
                             uHubAddr);

        USB_MHDRC_REG_WRITE8 (pMHDRC,
                             USB_MHDRC_RXHUBPORT_EP(0),
                             uHubPort);
        }
    
    /* 
     * Start In Data pack transfer.
     * Do not issue PING tokens in the data and status phases of a high-speed 
     * control transfer.
     */

    uHostCsr0Reg16 |= USB_MHDRC_HOST_CSR0_REQPKT | USB_MHDRC_HOST_CSR0_DISPING;

    USB_MHDRC_REG_WRITE16 (pMHDRC,
                          USB_MHDRC_HOST_CSR0,
                          uHostCsr0Reg16);

    USB_MHDRC_VDBG("usbMhdrcHcdControlPipeINDataPhaseStart(): "
                  "Triggered\n",
                  1, 2, 3, 4, 5 ,6);

    return;
    }


/***************************************************************************
*
* usbMhdrcHcdControlPipeOUTDataPhaseStart - start the OUT data phase transfer
*
* This routine starts the OUT data phase transfer of the control pipe.
*
* RETURNS: N/A.
*
* ERRNO: N/A
*
*
* \NOMANUAL
*/

LOCAL void usbMhdrcHcdControlPipeOUTDataPhaseStart
    (
    pUSB_MHCD_DATA         pHCDData,         /* Pointer to HCD Data     */
    pUSB_MHCD_REQUEST_INFO pRequestInfo,     /* Pointer to request info */
    UINT16                 uLengthToTransfer /* Length to transfer */
    )
    {
    /* For Control pipe, we do not use DMA */

    pUSB_MHCD_PIPE       pHCDPipe = NULL;
    UINT16               uHostCsr0Reg16 = 0;
    UINT8                uHubAddr = 0x0;
    UINT8                uHubPort = 0x0;
    UINT8                uTypeReg8 = 0;
    pUSB_MUSBMHDRC       pMHDRC;

    /* Parameter verification*/

    if ((NULL == pHCDData) ||
        (NULL == pHCDData->pMHDRC) ||
        (NULL == pRequestInfo) ||
        (NULL == pRequestInfo->pHCDPipe) ||
        (NULL == pRequestInfo->pUrb) ||
        (NULL == pRequestInfo->pUrb->pTransferBuffer))
        {
        USB_MHDRC_ERR("usbMhdrcHcdControlPipeOUTDataPhaseStart(): "
                     "Invalid parameter, %s is NULL\n",
                     ((NULL == pHCDData) ? "pHCDData" :
                     (NULL == pRequestInfo) ? "pRequestInfo" :
                     (NULL == pRequestInfo->pHCDPipe) ? "pRequestInfo->pHCDPipe" :
                     (NULL == pRequestInfo->pUrb) ? "pRequestInfo->pUrb" :
                     "pRequestInfo->pUrb->pTransferBuffer"),
                     2, 3, 4, 5 ,6);

        return;
        }

    pMHDRC = pHCDData->pMHDRC;

    /* For In data phase, we only need set REQPKT of HOST_CSR0 */

    pHCDPipe = pRequestInfo->pHCDPipe;

    uHostCsr0Reg16 = USB_MHDRC_REG_READ16(pMHDRC,
                            USB_MHDRC_HOST_CSR0);

    /* Flush fifo */

    uHostCsr0Reg16 |= USB_MHDRC_HOST_CSR0_FLUSHFIFO;

    USB_MHDRC_REG_WRITE16 (pMHDRC,
                          USB_MHDRC_HOST_CSR0,
                          uHostCsr0Reg16);

    /* File the data packet to the FIFO */

    usbMhdrcFIFOWrite(pMHDRC,
                     (UINT8 *)pRequestInfo->pUrb->pTransferBuffer +
                     pRequestInfo->uActLength,
                     uLengthToTransfer,
                     0);

    pRequestInfo->uTransactionLength = uLengthToTransfer;

    /* Init the CSR0 */

    uHostCsr0Reg16 = 0;

    /*
     * Make sure the Data toggle is correct
     * Seems not need check that.ignor?
     */

    if (usbMhdrcHcdGetDataToggle(pHCDPipe))
        {
        uHostCsr0Reg16 = USB_MHDRC_HOST_CSR0_DATATOGWREN |
                         USB_MHDRC_HOST_CSR0_DATATOG;
        }
    else
        {
        uHostCsr0Reg16 = USB_MHDRC_HOST_CSR0_DATATOGWREN;
        }
    switch (pHCDPipe->uSpeed)
        {
        case USBHST_HIGH_SPEED:
           uTypeReg8 |= USB_MHDRC_HOST_TYPE_SPEED_HIGH;
           break;
        case USBHST_FULL_SPEED:
           uTypeReg8 |= USB_MHDRC_HOST_TYPE_SPEED_FULL;
           break;
        case USBHST_LOW_SPEED:
           uTypeReg8 |= USB_MHDRC_HOST_TYPE_SPEED_LOW;
           break;
        default:
            break;
        }

    /* Update the device speed */

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                         USB_MHDRC_HOST_TYPE0,
                         uTypeReg8);

    /* Update the device address, hub address and hubport */

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                         USB_MHDRC_TXFUNCADDR_EP(0),
                         pHCDPipe->uDeviceAddress);
    /*
     * Add the MTT information 0x80(HUB_MASK_MULTIPLE_TT) in uHbuInfo
     * Add the MTT information with hub address (mask with 0x7F)
     */

    uHubAddr = (UINT8)(((pHCDPipe->uHubInfo >> 8) & 0x7F) | (0x80 & pHCDPipe->uHubInfo));
    uHubPort = (UINT8)((pHCDPipe->uHubInfo) & 0x7f);

    /* TODO: It seems that we need record the hub TT*/

    if (pMHDRC->uPlatformType != USB_MHDRC_PLATFORM_CENTAURUS_TI816X)
        {
        USB_MHDRC_REG_WRITE8 (pMHDRC,
                             USB_MHDRC_TXHUBADDR_EP(0),
                             uHubAddr);

        USB_MHDRC_REG_WRITE8 (pMHDRC,
                             USB_MHDRC_TXHUBPORT_EP(0),
                             uHubPort);
        }
    
    /* 
     * Start Out Data pack transfer.
     * Do not issue PING tokens in the data and status phases of a high-speed 
     * control transfer.
     */

    uHostCsr0Reg16 |= USB_MHDRC_HOST_CSR0_TXPKTRDY | USB_MHDRC_HOST_CSR0_DISPING;

    USB_MHDRC_REG_WRITE16 (pMHDRC,
                          USB_MHDRC_HOST_CSR0,
                          uHostCsr0Reg16);

    USB_MHDRC_VDBG("usbMhdrcHcdControlPipeOUTDataPhaseStart(): "
                  "Triggered\n",
                  1, 2, 3, 4, 5 ,6);

    return;

    }


/*******************************************************************************
*
* usbMhdrcHcdControlPipeStatusPhaseStart - start the status phase transfer
*
* This routine starts the status data phase transfer of the control pipe.
*
* RETURNS: N/A.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcHcdControlPipeStatusPhaseStart
    (
    pUSB_MHCD_DATA         pHCDData,     /* Pointer to HCD Data     */
    pUSB_MHCD_REQUEST_INFO pRequestInfo, /* Pointer to request info */
    UINT8                  uStatusIN     /* Status In request or not */
    )
    {
    /* For Control pipe, we do not use DMA */

    pUSB_MHCD_PIPE       pHCDPipe = NULL;
    UINT16               uHostCsr0Reg16 = 0;
    UINT8                uHubAddr = 0x0;
    UINT8                uHubPort = 0x0;
    UINT8                uTypeReg8 = 0;
    pUSB_MUSBMHDRC       pMHDRC;

    /* Parameter verification*/

    if ((NULL == pHCDData) ||
        (NULL == pRequestInfo) ||
        (NULL == pRequestInfo->pHCDPipe) ||
        (NULL == pRequestInfo->pUrb))
        {
        USB_MHDRC_ERR("usbMhdrcHcdControlPipeStatusPhaseStart(): "
                     "Invalid parameter, %s is NULL\n",
                     ((NULL == pHCDData) ? "pHCDData" :
                     (NULL == pRequestInfo) ? "pRequestInfo" :
                     (NULL == pRequestInfo->pHCDPipe) ? "pRequestInfo->pHCDPipe" :
                     "pRequestInfo->pUrb"),
                     2, 3, 4, 5 ,6);

        return;
        }


    /* Get the common hardware data structure */

    pMHDRC = pHCDData->pMHDRC;

    /* For In data phase, we only need set REQPKT of HOST_CSR0 */

    pHCDPipe = pRequestInfo->pHCDPipe;

    /* Flush fifo */

    uHostCsr0Reg16 = USB_MHDRC_HOST_CSR0_FLUSHFIFO;
    USB_MHDRC_REG_WRITE16 (pMHDRC,
                         USB_MHDRC_HOST_CSR0,
                         uHostCsr0Reg16);

    /* Init the CSR0 */

    uHostCsr0Reg16 = 0;

    /*
     * Make sure the Data toggle is correct
     * Seems not need check that.ignor?
     */

    /* If status OUT,DATA1 */

    if (usbMhdrcHcdGetDataToggle(pRequestInfo->pHCDPipe))
        {
        uHostCsr0Reg16 = USB_MHDRC_HOST_CSR0_DATATOGWREN |
                         USB_MHDRC_HOST_CSR0_DATATOG ;
        }
    else
        {
        uHostCsr0Reg16 = USB_MHDRC_HOST_CSR0_DATATOGWREN;
        }

    switch (pHCDPipe->uSpeed)
        {
        case USBHST_HIGH_SPEED:
           uTypeReg8 |= USB_MHDRC_HOST_TYPE_SPEED_HIGH;
           break;
        case USBHST_FULL_SPEED:
           uTypeReg8 |= USB_MHDRC_HOST_TYPE_SPEED_FULL;
           break;
        case USBHST_LOW_SPEED:
           uTypeReg8 |= USB_MHDRC_HOST_TYPE_SPEED_LOW;
           break;
        default:
            break;
        }

    /* Update the device speed */

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                         USB_MHDRC_HOST_TYPE0,
                         uTypeReg8);

    uHubAddr = (UINT8)((pHCDPipe->uHubInfo >> 8) & 0x7f);
    uHubPort = (UINT8)((pHCDPipe->uHubInfo) & 0x7f);

    /* Start Setup pack transfer */

    if (uStatusIN)
        {
        uHostCsr0Reg16 |=  USB_MHDRC_HOST_CSR0_REQPKT ;

        /* Update the device address, hub address and hubport */

        USB_MHDRC_REG_WRITE8 (pMHDRC,
                            USB_MHDRC_RXFUNCADDR_EP(0),
                            pHCDPipe->uDeviceAddress);

        /* TODO: It seems that we need record the hub TT*/

        if (pMHDRC->uPlatformType != USB_MHDRC_PLATFORM_CENTAURUS_TI816X)
            {
            USB_MHDRC_REG_WRITE8 (pMHDRC,
                                 USB_MHDRC_RXHUBADDR_EP(0),
                                 uHubAddr);

            USB_MHDRC_REG_WRITE8 (pMHDRC,
                                 USB_MHDRC_RXHUBPORT_EP(0),
                                 uHubPort);
            }

        }
    else
        {
        /* Update the device address, hub address and hubport */

        USB_MHDRC_REG_WRITE8 (pMHDRC,
                             USB_MHDRC_TXFUNCADDR_EP(0),
                             pHCDPipe->uDeviceAddress);

        /* TODO: It seems that we need record the hub TT*/

        if (pMHDRC->uPlatformType != USB_MHDRC_PLATFORM_CENTAURUS_TI816X)
            {
            USB_MHDRC_REG_WRITE8 (pMHDRC,
                                 USB_MHDRC_TXHUBADDR_EP(0),
                                 uHubAddr);

            USB_MHDRC_REG_WRITE8 (pMHDRC,
                                 USB_MHDRC_TXHUBPORT_EP(0),
                                 uHubPort);
            }

        uHostCsr0Reg16 |= USB_MHDRC_HOST_CSR0_TXPKTRDY;
        }

    /* 
     * Start status phase transfer.
     * Do not issue PING tokens in the data and status phases of a high-speed 
     * control transfer.
     */

    uHostCsr0Reg16 |= USB_MHDRC_HOST_CSR0_STATUSPKT | USB_MHDRC_HOST_CSR0_DISPING;

    USB_MHDRC_REG_WRITE16 (pMHDRC,
                      USB_MHDRC_HOST_CSR0,
                      uHostCsr0Reg16);

    USB_MHDRC_VDBG("usbMhdrcHcdControlPipeStatusPhaseStart(): "
                  "Triggered\n",
                  1, 2, 3, 4, 5 ,6);

    return;

    }


/***************************************************************************
*
* usbMhdrcHcdControlRequestPorcess - process the control request transfer
*
* This routine processes the control request transfer.
*
*
* RETURNS: N/A
*
* ERRNO: N/A
*
*
* \NOMANUAL
*/

LOCAL void usbMhdrcHcdControlRequestPorcess
    (
    pUSB_MHCD_DATA         pHCDData,    /* Pointer to HCD Data     */
    pUSB_MHCD_REQUEST_INFO pRequestInfo /* Pointer to request info */
    )
    {
    pUSBHST_SETUP_PACKET pSetup = NULL;
    UINT16  uLengthToTransfer = 0;

    /* Parameter verification*/

    if ((NULL == pHCDData) ||
        (NULL == pRequestInfo) ||
        (NULL == pRequestInfo->pHCDPipe) ||
        (NULL == pRequestInfo->pUrb) ||
        (NULL == pRequestInfo->pUrb->pTransferSpecificData))
        {
        USB_MHDRC_ERR("usbMhdrcHcdControlRequestPorcess(): "
                     "Invalid parameter, %s is NULL\n",
                     ((NULL == pHCDData) ? "pHCDData" :
                     (NULL == pRequestInfo) ? "pRequestInfo" :
                     (NULL == pRequestInfo->pHCDPipe) ? "pRequestInfo->pHCDPipe" :
                     "pRequestInfo->pUrb"),
                     2, 3, 4, 5 ,6);

        return;
        }

    pSetup = (pUSBHST_SETUP_PACKET)pRequestInfo->pUrb->pTransferSpecificData;
    switch (pRequestInfo->uStage)
        {
        case USB_MHCD_REQUEST_STAGE_NON_CONTROL:
            USB_MHDRC_ERR("usbMhdrcHcdControlRequestPorcess(): Wrong Stage, should not happen\n",
                          1, 2, 3, 4, 5 ,6);

            break;
        case USB_MHCD_REQUEST_STAGE_SETUP:
            /* Setup phase */

            USB_MHDRC_VDBG("usbMhdrcHcdControlRequestPorcess():"
                          "Call Device %p Setup phase \n",
                          pRequestInfo->pHCDPipe->uDeviceAddress,
                          2, 3, 4, 5 ,6);

            usbMhdrcHcdControlPipeSetupPhaseStart(pHCDData, pRequestInfo);
            break;
        case USB_MHCD_REQUEST_STAGE_DATA:

            USB_MHDRC_VDBG("usbMhdrcHcdControlRequestPorcess():"
                          "Call Device %p Data phase \n",
                          pRequestInfo->pHCDPipe->uDeviceAddress,
                          2, 3, 4, 5 ,6);

            /* Transfer size */

            uLengthToTransfer =
                (UINT16)(pRequestInfo->uXferSize - pRequestInfo->uActLength);

            if (uLengthToTransfer > pRequestInfo->pHCDPipe->uMaximumPacketSize)
                {
                uLengthToTransfer = pRequestInfo->pHCDPipe->uMaximumPacketSize;
                }
            if (pSetup->bmRequestType & USB_RT_DEV_TO_HOST)
                {

                usbMhdrcHcdControlPipeINDataPhaseStart(pHCDData,
                                                   pRequestInfo);
                }
            else
                {
                usbMhdrcHcdControlPipeOUTDataPhaseStart(pHCDData,
                                                    pRequestInfo,
                                                    uLengthToTransfer);
                }
            break;
        case USB_MHCD_REQUEST_STAGE_STATUS:

            /*
             * Status phase, according to bmRequestType to determine IN or OUT
             * 1. IN status phase
             *    - Zero data request
             *    - Write reqest, followed by OUT data
             * 2. OUT status phase
             *    - Read request, followed by IN data
             */

            USB_MHDRC_VDBG("usbMhdrcHcdControlRequestPorcess():"
                          "Call Device %p Stauts phase \n",
                          pRequestInfo->pHCDPipe->uDeviceAddress,
                          2, 3, 4, 5 ,6);

            if ((pSetup->wLength) &&
                (pSetup->bmRequestType & USB_RT_DEV_TO_HOST))
                {
                usbMhdrcHcdControlPipeStatusPhaseStart(pHCDData,
                                                   pRequestInfo,
                                                   USB_ENDPOINT_OUT);
                }
            else
                {
                usbMhdrcHcdControlPipeStatusPhaseStart(pHCDData,
                                                   pRequestInfo,
                                                   USB_ENDPOINT_IN);
                }
             break;
          default:
            break;
        }
    return;
    }


/***************************************************************************
*
* usbMhdrcHcdRxRequestPorcessStart - process the receive request transfer
*
* This routine processes the control request transfer.
*
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcHcdRxRequestPorcessStart
    (
    pUSB_MHCD_DATA         pHCDData,    /* Pointer to HCD Data     */
    pUSB_MHCD_REQUEST_INFO pRequestInfo /* Pointer to request info */
    )
    {
    pUSB_MHCD_PIPE pHCDPipe = NULL;
    UINT16         uHostCsrReg16 = 0;
    UINT8          uChannel = 0;
    UINT8          uHubAddr = 0;
    UINT8          uHubPort = 0;
    UINT8          uTypeReg8 = 0;
    pUSB_MUSBMHDRC pMHDRC;

    /* Parameter verification*/

    if ((NULL == pHCDData) ||
        (NULL == pRequestInfo) ||
        (NULL == pRequestInfo->pHCDPipe) ||
        (NULL == pRequestInfo->pUrb))
        {
        USB_MHDRC_ERR("usbMhdrcHcdRxRequestPorcessStart(): "
                     "Invalid parameter, %s is NULL\n",
                     ((NULL == pHCDData) ? "pHCDData" :
                     (NULL == pRequestInfo) ? "pRequestInfo" :
                     (NULL == pRequestInfo->pHCDPipe) ? "pRequestInfo->pHCDPipe" :
                     "pRequestInfo->pUrb"),
                     2, 3, 4, 5 ,6);

        return;
        }

    /* Get the common hardware data structure */

    pMHDRC = pHCDData->pMHDRC;

    pHCDPipe = pRequestInfo->pHCDPipe;
    uChannel = pHCDPipe->uEndPointEngineAsigned;

    /*
     * Make sure the Data toggle is correct
     * Seems not need check that.ignor?
     */

    if (usbMhdrcHcdGetDataToggle(pHCDPipe))
        {
        uHostCsrReg16 = USB_MHDRC_HOST_RXCSR_DATATOGWREN |
                        USB_MHDRC_HOST_RXCSR_DATATOG;
        }
    else
        {
        uHostCsrReg16 = USB_MHDRC_HOST_RXCSR_DATATOGWREN;
        }


    /* Update the device address, hub address and hubport */

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                         USB_MHDRC_RXFUNCADDR_EP(uChannel),
                         pHCDPipe->uDeviceAddress);

    /*
     * Add the MTT information 0x80(HUB_MASK_MULTIPLE_TT) in uHbuInfo
     * Add the MTT information with hub address (mask with 0x7F)
     */

    uHubAddr = (UINT8)(((pHCDPipe->uHubInfo >> 8) & 0x7F) | (0x80 & pHCDPipe->uHubInfo));
    uHubPort = (UINT8)((pHCDPipe->uHubInfo) & 0x7f);

    /* TODO: It seems that we need record the hub TT */

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                         USB_MHDRC_RXHUBADDR_EP(uChannel),
                         uHubAddr);

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                         USB_MHDRC_RXHUBPORT_EP(uChannel),
                         uHubPort);

    /* HOST RXMAXP */

    USB_MHDRC_REG_WRITE16 (pMHDRC,
                          USB_MHDRC_HOST_RXMAXP_EP(uChannel),
                          pHCDPipe->uMaximumPacketSize);

    /* HOST RXTYPE */

    switch (pHCDPipe->uSpeed)
        {
        case USBHST_HIGH_SPEED:
           uTypeReg8 |= USB_MHDRC_HOST_TYPE_SPEED_HIGH;
           break;
        case USBHST_FULL_SPEED:
           uTypeReg8 |= USB_MHDRC_HOST_TYPE_SPEED_FULL;
           break;
        case USBHST_LOW_SPEED:
           uTypeReg8 |= USB_MHDRC_HOST_TYPE_SPEED_LOW;
           break;
        default:
            break;
        }

    switch (pHCDPipe->uEndpointType)
        {
        case USB_ATTR_CONTROL:
           uTypeReg8 |= USB_MHDRC_HOST_TYPE_PORT_CONTROL;
           break;
        case USB_ATTR_ISOCH:
           uTypeReg8 |= USB_MHDRC_HOST_TYPE_PORT_ISO;
           break;
        case USB_ATTR_BULK:
           uTypeReg8 |= USB_MHDRC_HOST_TYPE_PORT_BULK;
           break;
       case USB_ATTR_INTERRUPT:
           uTypeReg8 |= USB_MHDRC_HOST_TYPE_PORT_INTERRUPT;
           break;
        default:
            break;
        }
    uTypeReg8 = (UINT8)(uTypeReg8 | (pHCDPipe->uEndpointAddress &
                 USB_MHDRC_HOST_TYPE_ENDPOINT_MASK));

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                         USB_MHDRC_HOST_RXTYPE_EP(uChannel),
                         uTypeReg8);

    /* HOST RXINTERVAL */

    /*
     * For interrupt we need set the bInterval in this register.
     * It will re-start the transaction when the device NAKed.
     * And the software can not recognize the NAK from any registers,
     * it will block the schedule for other interrupt transaction, if the
     * interrupt pipe is more than the endpoint.
     *
     * So, do not use the function of this register, schedule this by software.
     *
     */

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                         USB_MHDRC_HOST_RXINTERVAL_EP(uChannel),
                         0);

    /* Start pack transfer request */
    uHostCsrReg16 |= USB_MHDRC_HOST_RXCSR_FLUSHFIFO;


    USB_MHDRC_REG_WRITE16 (pMHDRC,
                          USB_MHDRC_HOST_RXCSR_EP(uChannel),
                          uHostCsrReg16);
    USB_MHDRC_REG_WRITE16 (pMHDRC,
                          USB_MHDRC_HOST_RXCSR_EP(uChannel),
                          uHostCsrReg16);

    uHostCsrReg16 |= USB_MHDRC_HOST_RXCSR_REQPKT;


    USB_MHDRC_REG_WRITE16 (pMHDRC,
                          USB_MHDRC_HOST_RXCSR_EP(uChannel),
                          uHostCsrReg16);
    uHostCsrReg16 =  USB_MHDRC_REG_READ16 (pMHDRC,
                          USB_MHDRC_HOST_RXCSR_EP(uChannel));

    USB_MHDRC_VDBG("usbMhdrcHcdRxRequestPorcessStart(): "
                  "pRequestInfo %p dev %p Ep %p to CH %p RXCSR 0x%X LEN %d\n",
                   pRequestInfo,
                   pHCDPipe->uDeviceAddress,
                   pHCDPipe->uEndpointAddress,
                   uChannel,
                   uHostCsrReg16,pRequestInfo->pUrb->uTransferLength);

    return;

    }


/***************************************************************************
*
* usbMhdrcHcdTxRequestPorcessStart - process the Tx request transfer
*
* This routine processes the control request transfer.
*
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcHcdTxRequestPorcessStart
    (
    pUSB_MHCD_DATA         pHCDData,    /* Pointer to HCD Data     */
    pUSB_MHCD_REQUEST_INFO pRequestInfo /* Pointer to request info */
    )
    {
    pUSB_MHCD_PIPE pHCDPipe = NULL;
    UINT16         uHostCsrReg16 = 0;
    pUSBHST_ISO_PACKET_DESC pIsoPacketDesc = NULL;
    UINT8          uChannel = 0;
    UINT8          uHubAddr = 0;
    UINT8          uHubPort = 0;
    UINT8          uTypeReg8 = 0;
    UINT32         uLengthToTransfer = 0;
    int            uDmaChannel = 0;
    pUSB_MUSBMHDRC pMHDRC;
    pUSB_MHDRC_DMA_DATA pDMAData;

    /* Parameter verification*/

    if ((NULL == pHCDData) ||
        (NULL == pHCDData->pMHDRC) ||
        (NULL == pRequestInfo) ||
        (NULL == pRequestInfo->pHCDPipe) ||
        (NULL == pRequestInfo->pUrb))
        {
        USB_MHDRC_ERR("usbMhdrcHcdTxRequestPorcessStart(): "
                     "Invalid parameter, %s is NULL\n",
                     ((NULL == pHCDData) ? "pHCDData" :
                     (NULL == pRequestInfo) ? "pRequestInfo" :
                     (NULL == pRequestInfo->pHCDPipe) ? "pRequestInfo->pHCDPipe" :
                     "pRequestInfo->pUrb"),
                     2, 3, 4, 5 ,6);

        return;
        }

    pMHDRC = pHCDData->pMHDRC;
    pDMAData = &(pMHDRC->dmaData);
    pHCDPipe = pRequestInfo->pHCDPipe;
    uChannel = pHCDPipe->uEndPointEngineAsigned;

    /*
     * For ISOCH pipe, the URB must have valid ISOCH packets in the
     * pUrb->pTransferSpecificData. Check this condition, if it is
     * invalid, then there is no need to do any thing with this.
     */

    /* Get the first ISO packet descriptor */

    pIsoPacketDesc = (pUSBHST_ISO_PACKET_DESC)
                      pRequestInfo->pUrb->pTransferSpecificData;

    if ((pHCDPipe->uEndpointType == USB_ATTR_ISOCH) &&
        (NULL == pIsoPacketDesc))
        {
        USB_MHDRC_ERR("usbMhdrcHcdTxRequestPorcessStart(): "
                      "pIsoPacketDesc is NULL\n",
                      1, 2, 3, 4, 5 ,6);
        return;
        }

    /*
     * Here remaind me that, I need design the FIFO size.
     * I'd like to structure the FIFO according the priority,
     * which will contribute to the schedule task.
     *
     * Right now, I will just tast what the default assign about
     * the FIFO size.
     */

    /*
     * Make sure the Data toggle is correct
     * Seems not need check that.ignor?
     */

    uHostCsrReg16 = USB_MHDRC_HOST_TXCSR_FLUSHFIFO;
    USB_MHDRC_REG_WRITE16 (pMHDRC,
                         USB_MHDRC_HOST_TXCSR_EP(uChannel),
                         USB_MHDRC_HOST_TXCSR_FLUSHFIFO);


    if (usbMhdrcHcdGetDataToggle(pHCDPipe))
        {

        uHostCsrReg16 = USB_MHDRC_HOST_TXCSR_DATATOGWREN |
                         USB_MHDRC_HOST_TXCSR_DATATOG;
        }
    else
        {
        uHostCsrReg16 = USB_MHDRC_HOST_TXCSR_DATATOGWREN;
        }

    /* Update the device address, hub address and hubport */

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                         USB_MHDRC_TXFUNCADDR_EP(uChannel),
                         pHCDPipe->uDeviceAddress);

    /*
     * Add the MTT information 0x80(HUB_MASK_MULTIPLE_TT) in uHbuInfo
     * Add the MTT information with hub address (mask with 0x7F)
     */

    uHubAddr = (UINT8)(((pHCDPipe->uHubInfo >> 8) & 0x7F) | (0x80 & pHCDPipe->uHubInfo));
    uHubPort = (UINT8)((pHCDPipe->uHubInfo) & 0x7f);

    /* TODO: It seems that we need record the hub TT */

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                         USB_MHDRC_TXHUBADDR_EP(uChannel),
                         uHubAddr);

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                         USB_MHDRC_TXHUBPORT_EP(uChannel),
                         uHubPort);

    /* HOST TXMAXP */

    USB_MHDRC_REG_WRITE16 (pMHDRC,
                         USB_MHDRC_HOST_TXMAXP_EP(uChannel),
                         pHCDPipe->uMaximumPacketSize);


    /* HOST TXTYPE */

    switch (pHCDPipe->uSpeed)
        {
        case USBHST_HIGH_SPEED:
           uTypeReg8 |= USB_MHDRC_HOST_TYPE_SPEED_HIGH;
           break;
        case USBHST_FULL_SPEED:
           uTypeReg8 |= USB_MHDRC_HOST_TYPE_SPEED_FULL;
           break;
        case USBHST_LOW_SPEED:
           uTypeReg8 |= USB_MHDRC_HOST_TYPE_SPEED_LOW;
           break;
        default:
            break;
        }

    switch (pHCDPipe->uEndpointType)
        {
        case USB_ATTR_CONTROL:
           uTypeReg8 |= USB_MHDRC_HOST_TYPE_PORT_CONTROL;
           break;
        case USB_ATTR_ISOCH:
           uTypeReg8 |= USB_MHDRC_HOST_TYPE_PORT_ISO;
           break;
        case USB_ATTR_BULK:
           uTypeReg8 |= USB_MHDRC_HOST_TYPE_PORT_BULK;
           break;
       case USB_ATTR_INTERRUPT:
           uTypeReg8 |= USB_MHDRC_HOST_TYPE_PORT_INTERRUPT;
           break;
        default:
            break;
        }

    uTypeReg8 = (UINT8)(uTypeReg8 | (pHCDPipe->uEndpointAddress &
                 USB_MHDRC_HOST_TYPE_ENDPOINT_MASK));

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                         USB_MHDRC_HOST_TXTYPE_EP(uChannel),
                         uTypeReg8);

    /* HOST TXINTERVAL */

    /*
     * For interrupt we need set the bInterval in this register.
     * It will re-start the transaction when the device NAKed.
     * And the software can not recognize the NAK from any registers,
     * it will block the schedule for other interrupt transaction, if the
     * interrupt pipe is more than the endpoint.
     *
     * So, do not use the function of this register, schedule this by software.
     *
     */

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                         USB_MHDRC_HOST_TXINTERVAL_EP(uChannel),
                         0);

    /* Setup the locations the DMA engines use  */

    /* The data buffer address */

    if (pHCDPipe->uEndpointType == USB_ATTR_ISOCH)
        {
        /* Get the current ISO packet descriptor*/

        pIsoPacketDesc += pRequestInfo->uIsoUrbCurrentPacketIndex;

        pRequestInfo->pCurrentBuffer = (UINT8 *)(pRequestInfo->pUrb->pTransferBuffer +
                                   pRequestInfo->uActLength);
        }
    else
        {
        pRequestInfo->pCurrentBuffer = (UINT8 *)(pRequestInfo->pUrb->pTransferBuffer +
                                   pRequestInfo->uActLength);
        }

    /* The data lenght */

    if (pHCDPipe->uEndpointType == USB_ATTR_ISOCH)
        {
        uLengthToTransfer = (pIsoPacketDesc->uLength + pIsoPacketDesc->uOffset)
                            - pRequestInfo->uActLength;
        }
    else
        {
        uLengthToTransfer = pRequestInfo->uXferSize - pRequestInfo->uActLength;
        }

    if (uLengthToTransfer >= pHCDPipe->uMaximumPacketSize)
        {
        uLengthToTransfer = pHCDPipe->uMaximumPacketSize;
        }

    /* Use Dma */

    if ((uLengthToTransfer >= 0x40) &&
       (pMHDRC->bDmaEnabled) &&
       (pDMAData->pDmaChannelRequest) &&
       (pDMAData->pDmaConfigure))
        {
        /* Get a free DMA channel */

        uDmaChannel = (pDMAData->pDmaChannelRequest)(pDMAData);

        if (uDmaChannel != USB_MHDRC_DMA_INVALID_CHANNEL)
            {

            if ((pMHDRC->uPlatformType != USB_MHDRC_PLATFORM_CENTAURUS) &&
                (pMHDRC->uPlatformType != USB_MHDRC_PLATFORM_CENTAURUS_TI816X))
                {
                /* Here use DMA Mode 0 ,Mode 1 need more test */

                uHostCsrReg16 =
                            (UINT16)(uHostCsrReg16 &
                            ~( USB_MHDRC_HOST_TXCSR_AUTOSET |
                               USB_MHDRC_HOST_TXCSR_DMAMODE|
                               USB_MHDRC_HOST_TXCSR_MODE));

                uHostCsrReg16 |= USB_MHDRC_HOST_TXCSR_DMAEN;

                USB_MHDRC_REG_WRITE16 (pMHDRC,
                                       USB_MHDRC_HOST_TXCSR_EP(uChannel),
                                       uHostCsrReg16);
                }

            (pDMAData->pDmaConfigure)(pMHDRC,
                                      pRequestInfo,
                                      (ULONG)(pRequestInfo->pCurrentBuffer),
                                      uLengthToTransfer,
                                      (UINT8)uDmaChannel,
                                      0,
                                      pHCDPipe->uEndPointEngineAsigned,
                                      (BOOLEAN)(pHCDPipe->uEndpointDir == USB_DIR_OUT),
                                      pHCDPipe->uMaximumPacketSize);

            USB_MHDRC_VDBG("usbMhdrcHcdTxRequestPorcessStart(): "
                          "DMA mode pRequestInfo %p dev %p Ep %p to CH %p Dma_Ch %p\n",
                           pRequestInfo,
                           pHCDPipe->uDeviceAddress,
                           pHCDPipe->uEndpointAddress,
                           uChannel,
                           uDmaChannel ,6);

            return;
            }

        }

    /*
     * If we get here, means
     * 1. The uLengthToTransfer <= 0x40, no need use DMA
     * 2. or, the DMA is not enabled
     * 3. or, all the DMA channels are used
     * 4. or, there is something wrong on the DMA
     * we will use directly access FIFO
     */

    usbMhdrcFIFOWrite(pMHDRC,
                     pRequestInfo->pCurrentBuffer,
                     (UINT16)uLengthToTransfer,
                     uChannel);

    pRequestInfo->uTransactionLength = uLengthToTransfer;

    /* Start pack transfer request */

    uHostCsrReg16 |= USB_MHDRC_HOST_TXCSR_TXPKTRDY;
    USB_MHDRC_REG_WRITE16 (pMHDRC,
                         USB_MHDRC_HOST_TXCSR_EP(uChannel),
                         uHostCsrReg16);

    USB_MHDRC_VDBG("usbMhdrcHcdTxRequestPorcessStart(): "
                  "PIP mode pRequestInfo %p dev %p Ep %p to CH %p\n",
                   pRequestInfo,
                   pHCDPipe->uDeviceAddress,
                   pHCDPipe->uEndpointAddress,
                   uChannel,
                   5 ,6);

    return;

    }


/***************************************************************************
*
* usbMhdrcHcdNewRequestProcessStart - start process the channel
*
* This routine starts process the channel,such as transfering, receiving, etc.
*
*
* RETURNS: N/A
*
* ERRNO: N/A
*
*
* \NOMANUAL
*/

LOCAL void usbMhdrcHcdNewRequestProcessStart
    (
    pUSB_MHCD_DATA         pHCDData,     /* Pointer to HCD Data     */
    pUSB_MHCD_REQUEST_INFO pRequestInfo  /* Pointer to request info */
    )
    {

    pUSB_MHCD_PIPE          pHCDPipe  = NULL;
    UINT8                  uChannel;

    /* Parameter varification */

    if ((NULL == pHCDData) ||
        (NULL == pRequestInfo)||
        (NULL == pRequestInfo->pHCDPipe) ||
        (NULL == pRequestInfo->pUrb))
        {
        USB_MHDRC_ERR("usbMhdrcHcdNewRequestProcessStart(): "
                     "Invalid parameter, %s is NULL\n",
                     ((NULL == pHCDData) ? "pHCDData" :
                     (NULL == pRequestInfo) ? "pRequestInfo" :
                     (NULL == pRequestInfo->pHCDPipe) ? "pRequestInfo->pHCDPipe" :
                      "pRequestInfo->pUrb"),
                     2, 3, 4, 5 ,6);

        return;
        }

    /* Find one request in the pipe queue, if no request available, return */

    pHCDPipe = pRequestInfo->pHCDPipe;
    uChannel = pHCDPipe->uEndPointEngineAsigned;

    switch (pHCDPipe->uEndpointType)
        {
        case USB_ATTR_CONTROL:
            usbMhdrcHcdControlRequestPorcess(pHCDData,
                                         pRequestInfo);
            break;
        case USB_ATTR_BULK:
        case USB_ATTR_INTERRUPT:
        case USB_ATTR_ISOCH:
            if (pHCDPipe->uEndpointDir == USB_DIR_IN)
                {
                usbMhdrcHcdRxRequestPorcessStart(pHCDData,
                                             pRequestInfo);

                }
            else
                {
                usbMhdrcHcdTxRequestPorcessStart(pHCDData,
                                             pRequestInfo);
                }

            break;
        default:
            USB_MHDRC_ERR("usbMhdrcHcdNewRequestProcessStart(): "
                          "invalid Transaction type %d\n",
                          pHCDPipe->uEndpointType, 2, 3, 4, 5 ,6);

            break;
        }

    return;
    }


/***************************************************************************
*
* usbMhdrcHcdProcessScheduleHandler - schedule the transfer of the hcd
*
* This routine schedules the transfer of the hcd, parapers the related
* information before transfer the requeset.
*
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcHcdProcessScheduleHandler
    (
    pUSB_MHCD_DATA pHCDData /* Pointer to HCD Data */
    )
    {
    pUSB_MHCD_PIPE pHCDPipe = NULL;
    NODE *         pNode = NULL;


    if (NULL == pHCDData)
        {
        USB_MHDRC_ERR("usbMhdrcHcdProcessScheduleHandler(): "
                     "Invalid parameter, pHCDData is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }

    /*
     * Periodic request have higher PRI than the Non-periodic request
     * So we start to schedule the periodic request first
     */

    pNode = lstFirst(&(pHCDData->PeriodicPipeList));
    while (pNode != NULL)
        {
        pHCDPipe = (pUSB_MHCD_PIPE)(pNode);

        if(NULL != pHCDPipe)
            {
            usbMhdrcHcdPipeScheduleHandle(pHCDData, pHCDPipe);
            }
        pNode = lstNext(pNode);
        }

    /*
     * Periodic request have higher PRI than the Non-periodic request
     * So we start to schedule the periodic request first
     */

    pNode = lstFirst(&(pHCDData->NonPeriodicPipeList));
    while (pNode != NULL)
        {
        pHCDPipe = (pUSB_MHCD_PIPE)(pNode);

        if(NULL != pHCDPipe)
            {
            usbMhdrcHcdPipeScheduleHandle(pHCDData, pHCDPipe);
            }
        pNode = lstNext(pNode);
        }

    /* Checking the default pipe */

    usbMhdrcHcdPipeScheduleHandle(pHCDData, pHCDData->pDefaultPipe);

    return;
    }


/***************************************************************************
*
* usbMhdrcHcdTransferManagementTask - handle transfer task
*
* This is the thread which handles the MHCD transfer .
*
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcHcdTransferManagementTask
    (
    pUSB_MHCD_DATA pHCDData /* Pointer to HCD Data */
    )
    {
    USB_MHCD_TRANSFER_TASK_INFO transferTaskInfo;
    ssize_t                     nBytes = 0;

    /* Check the validity of the parameter */

    if (NULL == pHCDData)
        {
        USB_MHDRC_ERR("usbMhdrcHcdTransferManagementTask(): "
                     "Invalid parameter, pHCDData is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }

    while (TRUE)
        {
        if (pHCDData->TransferThreadMsgID == NULL)
            {
            USB_MHDRC_ERR("usbMhdrcHcdTransferManagementTask(): "
                         "Invalid parameter, pHCDData->TransferThreadMsgID is NULL\n",
                         1, 2, 3, 4, 5 ,6);

            break;
            }

        /* Reset the transfer task information */

        OS_MEMSET(&transferTaskInfo, 0 ,
                  sizeof(USB_MHCD_TRANSFER_TASK_INFO));

        /* Wait on the signalling of the event */

        nBytes = msgQReceive (pHCDData->TransferThreadMsgID,
                              (char*) & transferTaskInfo,
                              sizeof(USB_MHCD_TRANSFER_TASK_INFO),
                              OS_CONVERT_MILLISECONDS_TO_WAIT_VALUE(USB_MHDRC_HCD_SCHEDULE_FREQ_MS));

        /*
         * Mentor Graphics USB can not schedule the transaction by hardware,
         * as EHCI does. Software needs take the schedule process.
         */

        if ((NULL == transferTaskInfo.pHCDData) ||
            (0 == transferTaskInfo.uCmdCode) ||
            (nBytes != sizeof(USB_MHCD_TRANSFER_TASK_INFO)))
            {
            /* If there is long time we did not get the MsgQ, schedule it */

            if (S_objLib_OBJ_TIMEOUT == errno)
                {
                usbMhdrcHcdProcessScheduleHandler(pHCDData);
                }

            continue;
            }
        if (USB_MHCD_TRANSFER_CMD_CHANNEL_START_PROCESS ==
            transferTaskInfo.uCmdCode )
            {
            usbMhdrcHcdNewRequestProcessStart(transferTaskInfo.pHCDData,
                                          transferTaskInfo.pRequestInfo);
            }
        else if (USB_MHCD_TRANSFER_CMD_COMPLITE_PROCESS ==
                 transferTaskInfo.uCmdCode)
            {
            usbMhdrcHcdRequestCompleteProcess(transferTaskInfo.pHCDData,
                                          transferTaskInfo.pRequestInfo,
                                          transferTaskInfo.uCompleteStatus);
            }
        else if (USB_MHCD_TRANSFER_CMD_SHEDULE_PROCESS ==
                transferTaskInfo.uCmdCode)
            {
            usbMhdrcHcdProcessScheduleHandler(transferTaskInfo.pHCDData);
            }
        }
    return;
    }


