/* usbMhdrcHcdRootHub.c - USB MHCI Root hub Emulation */

/*
 * Copyright (c) 2009 - 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */


/*
modification history
--------------------
01j,03may13,wyy  Remove compiler warning (WIND00356717)
01i,09mar11,w_x  Code clean up for make man
01h,11jan11,ghs  Return FALSE if URB is NULL when copy interrupt status data
                 (WIND00237602)
01g,07jan11,ghs  Clean up compile warnings (WIND00247082)
01f,13sep10,ghs  Fix defect found by code review of merge (WIND00232740)
01e,16aug10,w_x  VxWorks 64 bit audit and warning removal
01d,03jun10,s_z  Debug macro changed, Add more debug message
01c,02feb10,s_z  Code cleaning
01b,05nov09,s_z  Rewrite according to the structure redesign
01a,13jul09,j_x  initial version
*/

/*
DESCRIPTION

This file defines the functions for implementing root hub emulation

INCLUDE FILES: usb/usbOsal.h, usbMhdrc.h, usbMhdrcIsr.h, usbMhdrcRootHubEmulation.h,
               usbMhdrcTransferManagement.h, usbHst.h, usbMhdrcDebug.h
*/

/* includes */

#include <usb/usb.h>
#include <usb/usbOsal.h>
#include <usb/usbHst.h>
#include <usbMhdrc.h>
#include <usbMhdrcHcd.h>
#include <usbMhdrcHcdRootHub.h>
#include <usbMhdrcHcdCore.h>
#include <usbMhdrcHcdDebug.h>

/* locals */

/* Device Descriptor value for the Root hub */

UCHAR gMhdrcHcdRHDeviceDescriptor[] = {
                                  0x12,        /* bLength */
                                  0x01,        /* Device Descriptor type */
                                  0x00, 0x02,  /* bcdUSB - USB 2.0 */
                                  0x09,        /* Hub DeviceClass */
                                  0x00,        /* bDeviceSubClass */
                                  0x01,        /* bDeviceProtocol High Speed single TT support*/
                                  0x40,        /* Max packet size is 64 */
                                  0x00, 0x00,  /* idVendor */
                                  0x00, 0x00,  /* idProduct */
                                  0x00, 0x00,  /* bcdDevice */
                                  0x00,        /* iManufacturer */
                                  0x00,        /* iProduct */
                                  0x00,        /* iSerialNumber */
                                  0x01         /* 1 configuration */
                                  };

/*
 * Descriptor structure returned on a request for
 * Configuration descriptor for the Root hub.
 * This includes the Configuration descriptor, interface descriptor and
 * the endpoint descriptor
 */

UCHAR gMhdrcHcdRHConfigDescriptor[] = {
                                  /* Configuration descriptor */
                                  0x09,        /* bLength */
                                  0x02,        /* Configuration descriptor */
                                  0x19, 0x00,  /* wTotalLength */
                                  0x01,        /* 1 interface */
                                  0x01,        /* bConfigurationValue */
                                  0x00,        /* iConfiguration */
                                  0xE0,        /* bmAttributes */
                                  0x00,        /* bMaxPower */

                                  /* Interface Descriptor */
                                  0x09,        /* bLength */
                                  0x04,        /* Interface Descriptor */
                                  0x00,        /* bInterfaceNumber */
                                  0x00,        /* bAlternateSetting */
                                  0x01,        /* bNumEndpoints */
                                  0x09,        /* bInterfaceClass */
                                  0x00,        /* bInterfaceSubClass */
                                  0x00,        /* bInterfaceProtocol */
                                  0x00,        /* iInterface */

                                  /* Endpoint Descriptor */
                                  0x07,        /* bLength */
                                  0x05,        /* Endpoint Descriptor */
                                  0x81,         /* bEndpointAddress */
                                  0x03,         /* bmAttributes */
                                  0x08,   0x00, /* wMaxPacketSize */
                                  0x0C          /* bInterval - 255 ms*/
                                  };


/*******************************************************************************
*
* usbMhdrcHcdCopyRootHubInterruptData - copy the interrupt status data.
*
* This function is used to copy the interrupt status data
* to the request buffer if a request is pending or to
* the interrupt status buffer if a request is not pending.
*
* <pHCDData> - Pointer to the USB_MHCD_DATA structure.
* <uStatusChange > - Status change data.
*
* RETURNS: TRUE, or FALSE if the data is not successfully copied.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOLEAN usbMhdrcHcdCopyRootHubInterruptData
    (
    pUSB_MHCD_DATA pHCDData,       /* Pointer to the HCD structure */
    UINT32         uStatusChange   /* Status change data */
    )
    {
    pUSB_MHCD_REQUEST_INFO pRequestInfo = NULL; /* Pointer to request info */
    UINT32                 uInterruptData = 0;  /* Interrupt data */
    UINT32                 uBusIndex = 0;       /* Index of the host controller */

    /* Check the validity of the parameters */

    if ((NULL == pHCDData) ||
        (0 == uStatusChange))
        {
        USB_MHDRC_ERR("usbMhdrcHcdCopyRootHubInterruptData(): "
                     "Invalid parameter, %s \n",
                     ((NULL == pHCDData) ? "pHCDData is NULL" :
                     "uStatusChange is 0"), 2, 3, 4, 5 ,6);

        return FALSE;
        }

    /* Extract the index of the host controller */

    uBusIndex = pHCDData->uBusIndex;

    /*
     * If there is any request which is pending for the root hub
     * interrupt endpoint, populate the URB.
     * This has to be done by exclusively accessing the request list.
     */

    /* Copy the status information which is stored already */

    OS_MEMCPY(&uInterruptData,
              pHCDData->RHData.pHubInterruptData,
              pHCDData->RHData.uSizeInterruptData);

    /* Swap the data to CPU endian format */

    uStatusChange = USB_MHCD_SWAP_USB_DATA(uBusIndex,
                                           uStatusChange);

    uInterruptData |= uStatusChange;

    semTake(pHCDData->pHcdSynchMutex, WAIT_FOREVER);

    /* Copy the data to the interrupt data buffer */

    OS_MEMCPY(pHCDData->RHData.pHubInterruptData,
               &uInterruptData,
               pHCDData->RHData.uSizeInterruptData);

    semGive(pHCDData->pHcdSynchMutex);

    /*
     * This condition will pass when devices are kept connected
     * when the system is booted
     */

    if (NULL == pHCDData->RHData.pInterruptPipe)
        {
        return TRUE;
        }

    /* Check if this is the head of the list */

     pRequestInfo = pHCDData->RHData.pPendingInterruptRequest;

    /*
     * If there is no request pending, copy the data
     * to the interrupt data buffer
     */

    if (NULL != pRequestInfo)
        {
        USB_MHDRC_DBG ("usbMhdrcHcdCopyRootHubInterruptData(): "
                      "Calling callback :uStatusChange = %p\n",
                      uStatusChange, 2,3,4,5,6);

        if (NULL == pRequestInfo->pUrb ||
             NULL == pRequestInfo->pUrb->pTransferBuffer)
             return FALSE;

        /* Populate the data buffer */

        OS_MEMCPY(pRequestInfo->pUrb->pTransferBuffer,
                  pHCDData->RHData.pHubInterruptData,
                  pHCDData->RHData.uSizeInterruptData);

        OS_MEMSET(pHCDData->RHData.pHubInterruptData,
                  0,
                  pHCDData->RHData.uSizeInterruptData);

        /* Update the status */

        pRequestInfo->pUrb->nStatus = USBHST_SUCCESS;

        /* Update the length */

        pRequestInfo->pUrb->uTransferLength =
                        pHCDData->RHData.uSizeInterruptData;

        /*
         * If a callback function is registered, call the callback
         * function.
         */
        if (NULL != pRequestInfo->pUrb->pfCallback)
            {
            pRequestInfo->pUrb->pfCallback(pRequestInfo->pUrb);
            }
        OS_FREE (pRequestInfo);
        pRequestInfo = NULL;
        pHCDData->RHData.pPendingInterruptRequest = NULL;

        }
        /* End of else */
    return TRUE;
    }


/*******************************************************************************
*
* usbMhdrcHcdRootHubSetPortFeature - set the features of the port
*
* This routine sets the features of the port.
*
* RETURNS:
*  USBHST_SUCCESS if the URB is submitted successfully.
*  USBHST_INVALID_PARAMETER if the parameters are not valid.
*
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbMhdrcHcdRootHubSetPortFeature
    (
    pUSB_MHCD_DATA pHCDData,           /* Pointer to HCD block           */
    pUSBHST_URB    pURB                /* Pointer to User Request Block  */
    )
    {

    USBHST_STATUS        Status = USBHST_SUCCESS; /* Request status */
    pUSBHST_SETUP_PACKET pSetup = NULL;           /* Pointer to the setup packet */
    UINT32               uBusIndex = 0;
    UINT8                uPower = 0;
    UINT8                uDevctl = 0;
    UINT8                uSpeed = USBHST_FULL_SPEED;
    UINT16               wSpeedMask = 0;
    pUSB_MUSBMHDRC       pMHDRC;
    UINT32               count;

    /* Check the validity of the parameters */

    if ((NULL == pHCDData) ||
        (NULL == pURB) ||
        (NULL == pURB->pTransferSpecificData))
        {
        USB_MHDRC_ERR("usbMhdrcHcdRootHubSetPortFeature(): "
                     "Invalid parameters, %s is NULL\n",
                     ((NULL == pHCDData) ? "pHCDData" :
                     (NULL == pURB) ? "pURB" :
                     "pURB->pTransferSpecificData"),
                     2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Get the common hardware data structure */

    pMHDRC = pHCDData->pMHDRC;

    /* Extract the index of the host controller */

    uBusIndex = pHCDData->uBusIndex;

    /* Extract the setup packet */

    pSetup = (pUSBHST_SETUP_PACKET)pURB->pTransferSpecificData;
    OS_BUFFER_CPU_TO_LE(pSetup, sizeof(USBHST_SETUP_PACKET));

    /* If the device isn't connect we will return directly */

    if (pSetup->wIndex >
                pHCDData->RHData.uNumDownstreamPorts)
        {
        USB_MHDRC_ERR("usbMhdrcHcdRootHubSetPortFeature(): "
                     "Invalid port index\n",
                     1, 2, 3, 4, 5 ,6);

        pURB->nStatus = USBHST_INVALID_PARAMETER;
        return USBHST_INVALID_PARAMETER;
        }

    switch (pSetup->wValue)
        {
        case USB_HUB_PORT_FEATURE_SUSPEND:

            USB_MHDRC_DBG("usbMhdrcHcdRootHubSetPortFeature(): "
                          "USB_HUB_PORT_FEATURE_SUSPEND\n",
                          1, 2, 3, 4, 5, 6);

            /* Suspend */

            uPower = USB_MHDRC_REG_READ8(pMHDRC,
                                    USB_MHDRC_POWER);
            uPower = (UINT8)(uPower & ~USB_MHDRC_POWER_RESUME);
            USB_MHDRC_REG_WRITE8(pMHDRC,
                                USB_MHDRC_POWER,
                                uPower | USB_MHDRC_POWER_SUSPENDM);

            count = 1000;
            uPower = USB_MHDRC_REG_READ8(pMHDRC, USB_MHDRC_POWER);
            while (uPower & USB_MHDRC_POWER_SUSPENDM)
                {
                uPower = USB_MHDRC_REG_READ8(pMHDRC, USB_MHDRC_POWER);
                if (count-- < 1)
                    break;
                }

            USB_MHDRC_DBG("usbMhdrcHcdRootHubSetPortFeature(): "
                          "USB_HUB_PORT_FEATURE_SUSPEND uPower = 0x%X\n",
                          uPower|USB_MHDRC_POWER_SUSPENDM, 2, 3, 4, 5, 6);

            /*
             * Update the port status and port change status
             * Note,in get port status, we need swap the uHHPortStatus
             * and uRHPortChangeStatus
             */

            pHCDData->RHData.pRootHubPortStatus[pSetup->wIndex - 1].uRHPortStatus |=
                                  USB_HUB_STS_PORT_SUSPEND;

            pHCDData->RHData.pRootHubPortStatus[pSetup->wIndex - 1].uRHPortChangeStatus |=
                                  USB_HUB_C_PORT_SUSPEND;

            usbMhdrcHcdCopyRootHubInterruptData(pHCDData,
                 (0x2 << (pSetup->wIndex - 1)));

            pURB->uTransferLength = 0;
            pURB->nStatus = USBHST_SUCCESS;

            break;

        case USB_HUB_PORT_FEATURE_RESET:

            USB_MHDRC_DBG("usbMhdrcHcdRootHubSetPortFeature(): "
                          "USB_HUB_PORT_FEATURE_RESET\n",
                          1, 2, 3, 4, 5, 6);

            uPower = USB_MHDRC_REG_READ8(pMHDRC,
                                        USB_MHDRC_POWER);

            uPower = (UINT8)(uPower & 0xf1); /* Have the ENSUSPEND bit set */

            USB_MHDRC_REG_WRITE8(pMHDRC,
                                USB_MHDRC_POWER,
                                uPower | USB_MHDRC_POWER_RESET);
            OS_DELAY_MS(20);

            USB_MHDRC_REG_WRITE8(pMHDRC,
                            USB_MHDRC_POWER,
                            (UINT8)(uPower & ~USB_MHDRC_POWER_RESET));

            uDevctl = USB_MHDRC_REG_READ8(pMHDRC,
                                         USB_MHDRC_DEVCTL);

            uPower = USB_MHDRC_REG_READ8(pMHDRC,
                                        USB_MHDRC_POWER);

            /* high vs full speed is just a guess until after reset */

            if (uDevctl & USB_MHDRC_DEVCTL_LSDEV)
                {
                uSpeed = USBHST_LOW_SPEED;
                }
            if ((uDevctl & USB_MHDRC_DEVCTL_FSDEV) &&
                (uPower & (USB_MHDRC_POWER_HSMODE)))
                {
                uSpeed = USBHST_HIGH_SPEED;
                }

            switch (uSpeed)
                {
                case USBHST_LOW_SPEED:
                    wSpeedMask = USB_HUB_STS_PORT_LOW_SPEED;
                    break;
                case USBHST_HIGH_SPEED:
                    wSpeedMask = USB_HUB_STS_PORT_HIGH_SPEED;
                    break;
                }

            pHCDData->RHData.pRootHubPortStatus[pSetup->wIndex - 1].uRHPortStatus =
                (UINT16)(pHCDData->RHData.pRootHubPortStatus[pSetup->wIndex - 1].uRHPortStatus &
                  (~(USB_HUB_STS_PORT_LOW_SPEED |
                    USB_HUB_STS_PORT_HIGH_SPEED)));

            pHCDData->RHData.pRootHubPortStatus[pSetup->wIndex - 1].uRHPortStatus |=
                wSpeedMask;

            pHCDData->RHData.pRootHubPortStatus[pSetup->wIndex - 1].uRHPortStatus |=
                                  USB_HUB_STS_PORT_RESET |
                                  USB_HUB_STS_PORT_ENABLE;

            pHCDData->RHData.pRootHubPortStatus[pSetup->wIndex - 1].uRHPortChangeStatus |=
                                  USB_HUB_C_PORT_RESET;

            usbMhdrcHcdCopyRootHubInterruptData(pHCDData,
                 (0x2 << (pSetup->wIndex - 1)));

            pURB->uTransferLength = 0;
            pURB->nStatus = USBHST_SUCCESS;

            break;

        case USB_HUB_PORT_FEATURE_POWER:
            USB_MHDRC_DBG("usbMhdrcHcdRootHubSetPortFeature(): "
                          "USB_HUB_PORT_FEATURE_POWER\n",
                          1, 2, 3, 4, 5, 6);

            /*
             * REVISIT: doing it this way seems like an
             * unclean fit for the hub and bus models,
             * especially without deactivation...
             */

            pHCDData->RHData.pRootHubPortStatus[pSetup->wIndex - 1].uRHPortStatus |=
                              USB_HUB_STS_PORT_POWER;

            pURB->uTransferLength = 0;
            pURB->nStatus = USBHST_SUCCESS;

            break;

        case USB_HUB_PORT_FEATURE_ENABLE:
            USB_MHDRC_DBG("usbMhdrcHcdRootHubSetPortFeature(): "
                          "USB_HUB_PORT_FEATURE_ENABLE\n",
                          1, 2, 3, 4, 5, 6);

            pHCDData->RHData.pRootHubPortStatus[pSetup->wIndex - 1].uRHPortStatus |=
                              USB_HUB_STS_PORT_ENABLE;

            pURB->uTransferLength = 0;
            pURB->nStatus = USBHST_SUCCESS;

           break;

       default:
           pURB->nStatus = USBHST_INVALID_REQUEST;
           break;
        }

    return Status;
    }


/*******************************************************************************
*
* usbMhdrcHcdRootHubClearPortFeature - clear a feature of the port
*
* This routine clears a feature of the port.
*
* RETURNS:
*  USBHST_SUCCESS - if the URB is submitted successfully.
*  USBHST_INVALID_PARAMETER- if the parameters are not valid.
*
* ERRNO:
*   None.
*/

USBHST_STATUS usbMhdrcHcdRootHubClearPortFeature
    (
    pUSB_MHCD_DATA pHCDData,           /* Pointer to HCD block           */
    pUSBHST_URB    pURB                /* Pointer to User Request Block  */
    )
    {
    USBHST_STATUS        Status = USBHST_SUCCESS;
    pUSBHST_SETUP_PACKET pSetup = NULL;
    UINT32               uBusIndex = 0;
    UINT8                uPower = 0;
    pUSB_MUSBMHDRC       pMHDRC;

    /* Check the validity of the parameters */

    if ((NULL == pHCDData) ||
        (NULL == pURB) ||
        (NULL == pURB->pTransferSpecificData))
        {
        USB_MHDRC_ERR("usbMhdrcHcdRootHubClearPortFeature(): "
                     "Invalid parameters, %s is NULL\n",
                     ((NULL == pHCDData) ? "pHCDData" :
                     (NULL == pURB) ? "pURB" :
                     "pURB->pTransferSpecificData"),
                     2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Get the common hardware data structure */

    pMHDRC = pHCDData->pMHDRC;

    /* Extract the index of the host controller */

    uBusIndex = pHCDData->uBusIndex;

    /* Extract the setup packet */

    pSetup = (pUSBHST_SETUP_PACKET)pURB->pTransferSpecificData;
    OS_BUFFER_CPU_TO_LE(pSetup, sizeof(USBHST_SETUP_PACKET));

    /* Check whether the members are valid */

    if (pSetup->wIndex >
                pHCDData->RHData.uNumDownstreamPorts)
        {
        USB_MHDRC_ERR("usbMhdrcHcdRootHubClearPortFeature(): "
                     "Invalid port index\n",
                     1, 2, 3, 4, 5 ,6);

        pURB->nStatus = USBHST_INVALID_PARAMETER;
        return USBHST_INVALID_PARAMETER;
        }

    /* Handle the request based on the feature to be cleared */

    switch(pSetup->wValue)
        {
        case USB_HUB_PORT_FEATURE_CONNECTION:
        case USB_HUB_PORT_FEATURE_OVER_CURRENT:
        case USB_HUB_PORT_FEATURE_POWER:
        case USB_HUB_PORT_FEATURE_INDICATOR:
            pURB->uTransferLength = 0;
            pURB->nStatus = USBHST_SUCCESS;
            break;

        case USB_HUB_PORT_FEATURE_ENABLE:
            pURB->uTransferLength = 0;
            pURB->nStatus = USBHST_SUCCESS;
            break;

        case USB_HUB_PORT_FEATURE_SUSPEND:

            USB_MHDRC_DBG("usbMhdrcHcdRootHubClearPortFeature(): "
                          "USB_HUB_PORT_FEATURE_SUSPEND\n",
                          1, 2, 3, 4, 5, 6);

            uPower = USB_MHDRC_REG_READ8(pMHDRC,
                                        USB_MHDRC_POWER);

            uPower =
                (UINT8)(uPower &
                        ~(USB_MHDRC_POWER_SUSPENDM | USB_MHDRC_POWER_RESUME));

            USB_MHDRC_REG_WRITE8(pMHDRC,
                                USB_MHDRC_POWER,
                                uPower | USB_MHDRC_POWER_RESUME);

            OS_DELAY_MS(10);

            USB_MHDRC_REG_WRITE8(pMHDRC,
                                USB_MHDRC_POWER,
                                uPower);

            usbMhdrcHcdCopyRootHubInterruptData(pHCDData,
                 (0x2 << (pSetup->wIndex - 1)));

            pURB->uTransferLength = 0;
            pURB->nStatus = USBHST_SUCCESS;
            break;

        case USB_HUB_PORT_FEATURE_RESET:

            USB_MHDRC_DBG("usbMhdrcHcdRootHubClearPortFeature(): "
                          "USB_HUB_PORT_FEATURE_RESET\n",
                          1, 2, 3, 4, 5, 6);
            uPower = USB_MHDRC_REG_READ8(pMHDRC,
                                        USB_MHDRC_POWER);

            /* root port reset stopped */

            USB_MHDRC_REG_WRITE8(pMHDRC,
                                USB_MHDRC_POWER,
                                (UINT8)(uPower & ~USB_MHDRC_POWER_RESET));

            /* check for high-speed and set in root device if so */

            uPower = USB_MHDRC_REG_READ8(pMHDRC,
                                        USB_MHDRC_POWER);

            USB_MHDRC_VDBG("usbMhdrcProcessRootHubPortReset(): "
                           "clear power reset bit \n",
                          1, 2, 3, 4, 5 ,6);

            pURB->uTransferLength = 0;
            pURB->nStatus = USBHST_SUCCESS;
            break;

        /* acknowledge changes: */

        case USB_HUB_PORT_FEATURE_C_CONNECTION:
            pHCDData->RHData.pRootHubPortStatus[pSetup->wIndex - 1].uRHPortChangeStatus =
                (UINT16)(pHCDData->RHData.pRootHubPortStatus[pSetup->wIndex - 1].uRHPortChangeStatus &
                         ~USB_HUB_C_PORT_CONNECTION);

            pURB->uTransferLength = 0;
            pURB->nStatus = USBHST_SUCCESS;
            break;

        case USB_HUB_PORT_FEATURE_C_ENABLE:
            pURB->uTransferLength = 0;
            pURB->nStatus = USBHST_SUCCESS;
            break;

        case USB_HUB_PORT_FEATURE_C_SUSPEND:

            USB_MHDRC_DBG("usbMhdrcHcdRootHubClearPortFeature(): "
                          "USB_HUB_PORT_FEATURE_C_SUSPEND\n",
                          1, 2, 3, 4, 5, 6);
            pHCDData->RHData.pRootHubPortStatus[pSetup->wIndex - 1].uRHPortChangeStatus =
                (UINT16)(pHCDData->RHData.pRootHubPortStatus[pSetup->wIndex - 1].uRHPortChangeStatus &
                          (~USB_HUB_C_PORT_SUSPEND));

            usbMhdrcHcdCopyRootHubInterruptData(pHCDData,
                 (0x2 << (pSetup->wIndex - 1)));

            pURB->uTransferLength = 0;
            pURB->nStatus = USBHST_SUCCESS;
            break;

        case USB_HUB_PORT_FEATURE_C_RESET:

            USB_MHDRC_DBG("usbMhdrcHcdRootHubClearPortFeature(): "
                          "USB_HUB_PORT_FEATURE_C_RESET\n",
                          1, 2, 3, 4, 5, 6);
            pHCDData->RHData.pRootHubPortStatus[pSetup->wIndex - 1].uRHPortChangeStatus =
                (UINT16)(pHCDData->RHData.pRootHubPortStatus[pSetup->wIndex - 1].uRHPortChangeStatus &
                          ~USB_HUB_C_PORT_RESET);

            pURB->uTransferLength = 0;
            pURB->nStatus = USBHST_SUCCESS;
            break;

        case USB_HUB_PORT_FEATURE_C_OVER_CURRENT:
            pURB->uTransferLength = 0;
            pURB->nStatus = USBHST_SUCCESS;
            break;

        default:
            USB_MHDRC_ERR("usbMhdrcProcessRootHubClassSpecificRequest(): "
                         "clear feature 0x%02x on port=%d unknown \n",
                         pSetup->wValue,
                         2, 3, 4, 5 ,6);
            pURB->nStatus = USBHST_INVALID_REQUEST;
            break;
        }/* End of switch () */

    return Status;
    }


/*******************************************************************************
*
* usbMhdrcHcdRootHubGetHubDescriptor - get the hub descriptor
*
* This routine gets the hub descriptor.
*
* RETURNS:
*  USBHST_SUCCESS - if the URB is submitted successfully.
*  USBHST_INVALID_PARAMETER - if the parameters are not valid.
*  USBHST_MEMORY_NOT_ALLOCATED - if memory allocate fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbMhdrcHcdRootHubGetHubDescriptor
    (
    pUSB_MHCD_DATA pHCDData,           /* Pointer to HCD block           */
    pUSBHST_URB    pURB                /* Pointer to User Request Block  */
    )
    {
    USBHST_STATUS        Status = USBHST_SUCCESS;
    pUSBHST_SETUP_PACKET pSetup = NULL;
    UINT32               uPortBytes = 0;
    UINT32               uIndex = 0;
    UCHAR *              pBuffer = NULL;

     /* Check the validity of the parameters */

    if ((NULL == pHCDData) ||
        (NULL == pURB) ||
        (NULL == pURB->pTransferSpecificData) ||
        (NULL == pURB->pTransferBuffer))
        {
        USB_MHDRC_ERR("usbMhdrcHcdRootHubGetHubDescriptor(): "
                     "Invalid parameters, %s is NULL\n",
                     ((NULL == pHCDData) ? "pHCDData" :
                     (NULL == pURB) ? "pURB" :
                     (NULL == pURB->pTransferSpecificData) ?
                     "pURB->pTransferSpecificData" :
                     "pURB->pTransferBuffer"),
                     2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the setup packet */

    pSetup = (pUSBHST_SETUP_PACKET)pURB->pTransferSpecificData;
    OS_BUFFER_CPU_TO_LE(pSetup, sizeof(USBHST_SETUP_PACKET));

    /*
     * Check whether the descriptor type and decriptor index is valid.
     *
     * NOTE: For the root hub, only one hub descriptor is supported.
     *       Further, the descriptor type and descriptor index should
     *       be zero.
     */

    if(((USB_HUB_DESCRIPTOR << 8) | 0x00) != pSetup->wValue ||
       (0 != pSetup->wIndex))
        {
        USB_MHDRC_ERR("usbMhdrcHcdRootHubGetHubDescriptor(): "
                     "Invalid parameters\n",
                     1, 2, 3, 4, 5 ,6);

        pURB->nStatus = USBHST_INVALID_PARAMETER;
        return USBHST_INVALID_PARAMETER;
        }

    /*
     * Determine the number of bytes that the descriptor would occupy
     * ie. the num of bytes req to accomodate info about the ports
     */

    uPortBytes = (pHCDData->RHData.uNumDownstreamPorts) / 8;
    if(0 != pHCDData->RHData.uNumDownstreamPorts % 8)
        {
        uPortBytes++;
        }

    /* Allocate memory for the buffer */

    pBuffer = (UCHAR *)OS_MALLOC(7 + (uPortBytes * 2));

    /* Check if memory allocation is successful */

    if(NULL == pBuffer)
        {
        USB_MHDRC_ERR("usbMhdrcHcdRootHubGetHubDescriptor(): "
                     "Memory not allocated\n",
                     1, 2, 3, 4, 5 ,6);

        pURB->nStatus = USBHST_MEMORY_NOT_ALLOCATED;
        return USBHST_MEMORY_NOT_ALLOCATED;
        }

    /* Population of the values of hub descriptor - Start */

    /* Length of the descriptor */

    pBuffer[0] = (UINT8)(7 + (uPortBytes * 2));

    /* Hub Descriptor type */

    pBuffer[1] = 0x29;

    /* Number of downstream ports */

    pBuffer[2] = pHCDData->RHData.uNumDownstreamPorts;

    /*
     * The following 2 bytes give the hub characteristics
     * The root hub has individual port overcurrent indication
     */

    pBuffer[3] = 0x08;
    pBuffer[4] = 0;

    /* The power On to Power Good Time for the Root hub is 1 * 2ms */
    pBuffer[5] = 1;

    /* There are no specific maximim current requirements */

    pBuffer[6] = 0;

    /* The last few bytes of the descriptor is based on the number of ports */

    for(uIndex = 0;uIndex < uPortBytes ; uIndex++)
        {
        /* Indicates whether the hub is removable */

        pBuffer[7+uIndex] = 0;

        /* Port power control mask should be 1 for all the ports */

        pBuffer[7+uPortBytes+uIndex] = 0xff;
        }

    /* Population of the values of hub descriptor - End */

    /* Update the length */

    if (pURB->uTransferLength >= 7 + (uPortBytes * 2))
        {
        pURB->uTransferLength = 7 + (uPortBytes * 2);
        }

    /* Copy the data */

    OS_MEMCPY(pURB->pTransferBuffer, pBuffer, pURB->uTransferLength);

    /* Update the status */

    pURB->nStatus = USBHST_SUCCESS;

    /* Free memory allocated for the buffer */

    OS_FREE(pBuffer);

    return Status;
    }


/*******************************************************************************
*
* usbMhdrcHcdRootHubGetPortStatus - get the status of the port
*
* This routine gets the status of the port.
*
* RETURNS:
*  USBHST_SUCCESS - if the URB is submitted successfully.
*  USBHST_INVALID_PARAMETER - if the parameters are not valid.
*
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbMhdrcHcdRootHubGetPortStatus
    (
    pUSB_MHCD_DATA pHCDData,           /* Pointer to HCD block           */
    pUSBHST_URB    pURB                /* Pointer to User Request Block  */
    )
    {

    USBHST_STATUS Status = USBHST_SUCCESS;
    pUSBHST_SETUP_PACKET pSetup = NULL;
    UINT16 wPortStatus = 0;
    UINT16 wPortChangeStatus = 0;
    UINT32 uPortStatusAvailable = 0;

    /* Parameter verification */

    if ((NULL == pHCDData) ||
        (NULL == pURB) ||
        (NULL == pURB->pTransferBuffer) ||
        (NULL == pURB->pTransferSpecificData))
        {
        USB_MHDRC_ERR("usbMhdrcHcdRootHubGetPortStatus(): "
                     "Invalid parameter, %s is NULL\n",
                     ((NULL == pHCDData) ? "pHCDData" :
                     (NULL == pURB) ? "pURB" :
                     (NULL == pURB->pTransferBuffer) ? "pURB->pTransferBuffer" :
                     "pURB->pTransferSpecificData"),
                     2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    pSetup = (pUSBHST_SETUP_PACKET)pURB->pTransferSpecificData;
    OS_BUFFER_CPU_TO_LE(pSetup, sizeof(USBHST_SETUP_PACKET));

    if (pSetup->wIndex >
                pHCDData->RHData.uNumDownstreamPorts)
        {
        USB_MHDRC_ERR("usbMhdrcHcdRootHubGetPortStatus(): "
                     "Invalid port index\n",
                     1, 2, 3, 4, 5 ,6);

        pURB->nStatus = USBHST_INVALID_PARAMETER;
        return USBHST_INVALID_PARAMETER;
        }

    /*
     * The hub's port status contains 4 bytes of information, out of which
     * the hub's port status is reported in the byte offsets 0 to 1
     * and the hub's port status change in the byte offsets 2 to 3
     * Need swap.
     */

    wPortStatus = OS_UINT16_LE_TO_CPU(
        pHCDData->RHData.pRootHubPortStatus[pSetup->wIndex - 1].uRHPortStatus);

    wPortChangeStatus = OS_UINT16_LE_TO_CPU(
        pHCDData->RHData.pRootHubPortStatus[pSetup->wIndex - 1].uRHPortChangeStatus);

    uPortStatusAvailable = (wPortChangeStatus << 16) |  wPortStatus;


    OS_MEMCPY(pURB->pTransferBuffer, &uPortStatusAvailable, 4);

    pURB->nStatus = USBHST_SUCCESS;

    return Status;
    }


/*******************************************************************************
*
* usbMhdrcHcdRootHubProcessInterruptRequest - process a interrupt transfer request
*
* This routine processes a interrupt transfer request.
*
* RETURNS:
* USBHST_SUCCESS if the URB is submitted successfully.
* USBHST_INVALID_PARAMETER if the parameters are not valid.
* USBHST_INSUFFICIENT_BANDWIDTH if memory is insufficient for the request.
*
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbMhdrcHcdRootHubProcessInterruptRequest
    (
    pUSB_MHCD_DATA pHCDData,    /* Pointer to HCD block           */
    pUSBHST_URB    pURB         /* Pointer to User Request Block  */
    )
    {
    pUSB_MHCD_REQUEST_INFO pRequest       = NULL; /* Pointer to request info */
    USBHST_STATUS          Status         = USBHST_SUCCESS;
    UINT32                 uInterruptData = 0;

    /* Check the validity of the parameters */

    if ((NULL == pHCDData) ||
        (NULL == pURB) ||
        (NULL == pURB->pTransferBuffer))
        {
        USB_MHDRC_ERR("usbMhdrcHcdRootHubProcessInterruptRequest(): "
                     "Invalid parameters, %s is NULL\n",
                     ((NULL == pHCDData) ? "pHCDData" :
                     (NULL == pURB) ? "pURB" :
                     "pURB->pTransferBuffer"),
                     2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /*
     * Check if the Root hub is configured
     * to accept any interrupt transfer request.
     */

    if ((0 == pHCDData->RHData.uConfigValue) ||
        (NULL == pHCDData->RHData.pInterruptPipe) ||
        (USB_MHCD_PIPE_FLAG_DELETE == pHCDData->RHData.pInterruptPipe->uPipeFlag))
        {
        USB_MHDRC_ERR("usbMhdrcHcdRootHubProcessInterruptRequest(): "
                     "Invalid request - device not configured\n",
                     1, 2, 3, 4, 5 ,6);

        return USBHST_INVALID_REQUEST;
        }

    /* Assert if the structure members are invalid */

    OS_ASSERT(NULL != pHCDData->RHData.pHubInterruptData);

    /* Copy the existing interrupt data  */

    OS_MEMCPY(&uInterruptData,
              pHCDData->RHData.pHubInterruptData,
              pHCDData->RHData.uSizeInterruptData);

    /* If interrupt data is available, copy the data directly to URB buffer */

    if (0 != uInterruptData)
        {
        /* Exclusively access the request resource */

        semTake(pHCDData->pHcdSynchMutex, WAIT_FOREVER);

        OS_MEMCPY(pURB->pTransferBuffer,
                  &uInterruptData,
                  pHCDData->RHData.uSizeInterruptData);

        /* Initialize the interrupt data */

        OS_MEMSET(pHCDData->RHData.pHubInterruptData,
                  0,
                  pHCDData->RHData.uSizeInterruptData);

        /* Release the exclusive access */

        semGive(pHCDData->pHcdSynchMutex);

        /* Update the length */

        pURB->uTransferLength = pHCDData->RHData.uSizeInterruptData;

        /* Update the status of URB */

        pURB->nStatus = Status;

        /*
         * If a callback function is registered, call the callback
         * function.
         */

        if (pURB->pfCallback)
            {
            pURB->pfCallback(pURB);
            }
        }
    else
        {
        /* Allocate memory for the request data structure */

        pRequest = (pUSB_MHCD_REQUEST_INFO)OS_MALLOC(sizeof(USB_MHCD_REQUEST_INFO));

        /* Check if memory allocation is successful */

        if (NULL == pRequest)
            {
            USB_MHDRC_ERR("usbMhdrcHcdRootHubProcessInterruptRequest(): "
                         "memory not allocated\n", 1, 2, 3, 4, 5 ,6);

            return USBHST_INSUFFICIENT_MEMORY;
            }

        /* Initialize the request structure */

        OS_MEMSET(pRequest, 0, sizeof(USB_MHCD_REQUEST_INFO));

        semTake(pHCDData->pHcdSynchMutex, WAIT_FOREVER);

        /* Copy the USB_EHCD_PIPE pointer */

        pRequest->pHCDPipe = pHCDData->RHData.pInterruptPipe;

        /* Store the URB pointer in the request data structure */

        pRequest->pUrb = pURB;

        /* Add to the request list */

        /* Record the pending request, will return when ready */
        if (pHCDData->RHData.pPendingInterruptRequest == NULL)
            {
            pHCDData->RHData.pPendingInterruptRequest = pRequest;
            }
        else
            {
            USB_MHDRC_ERR("usbMhdrcHcdRootHubProcessInterruptRequest "
                          "How that happen? \n", 1, 2, 3, 4, 5 ,6);
            }

        /* Release the exclusive access */

        semGive(pHCDData->pHcdSynchMutex);
        }

    return Status;
    }


/*******************************************************************************
*
* usbMhdrcHcdRootHubProcessStandardRequest -  process a standard transfer request
*
* This routine processes a standard transfer request.
*
* RETURNS:
*  USBHST_SUCCESS if the URB is submitted successfully.
*  USBHST_INVALID_PARAMETER if the parameters are not valid.
*  USBHST_INSUFFICIENT_BANDWIDTH if memory is insufficient for the request.
*
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbMhdrcHcdRootHubProcessStandardRequest
    (
    pUSB_MHCD_DATA pHCDData,    /* Pointer to HCD block           */
    pUSBHST_URB    pURB         /* Pointer to User Request Block  */
    )
    {
    /* To hold the return status of the function */

    USBHST_STATUS Status = USBHST_SUCCESS;

    /* Pointer to the setup packet */

    pUSBHST_SETUP_PACKET pSetup = NULL;

    /* Check the validity of the parameters */

    if ((NULL == pHCDData) ||
        (NULL == pURB) ||
        (NULL == pURB->pTransferSpecificData))
        {
        USB_MHDRC_ERR("usbMhdrcHcdRootHubProcessStandardRequest(): "
                     "Invalid parameters, %s is NULL\n",
                     ((NULL == pHCDData) ? "pHCDData" :
                     (NULL == pURB) ? "pURB" :
                     "pURB->pTransferSpecificData"),
                     2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }
    /* Extract the setup packet from the URB */

    pSetup = (pUSBHST_SETUP_PACKET)pURB->pTransferSpecificData;
    OS_BUFFER_CPU_TO_LE(pSetup, sizeof(USBHST_SETUP_PACKET));

    /* Handle the standard request */

    switch(pSetup->bRequest)
        {
        case USBHST_REQ_CLEAR_FEATURE:/* Clear feature request */
            {
            /* Based on the recipient, handle the request */

            switch (pSetup->bmRequestType & USB_MHCD_RH_RECIPIENT_MASK)
                {
                case USBHST_RECIPIENT_DEVICE: /* Device recipient */
                    {
                    /* Check the feature selector */

                    if (USBHST_FEATURE_DEVICE_REMOTE_WAKEUP == pSetup->wValue)
                        {
                        /* Disable the device remote wakeup feature */

                        pHCDData->RHData.bRemoteWakeupEnabled = FALSE;
                        }
                        /* Update the URB status */

                        pURB->nStatus = USBHST_SUCCESS;
                        break;
                    }
                case USBHST_RECIPIENT_INTERFACE: /* Interface recipient */
                case USBHST_RECIPIENT_ENDPOINT:  /* Endpoint recipient */
                    {
                    /* Update the URB status */

                    pURB->nStatus = USBHST_SUCCESS;
                    break;
                    }
                default:
                    {
                    /* Invalid recipient value */

                    pURB->nStatus = USBHST_INVALID_REQUEST;
                    break;
                    }
                }
            break;
            }
        case USBHST_REQ_GET_CONFIGURATION:/* Get Configuration request */
            {
            /*
             * Update the URB transfer buffer with the current configuration
             * value for the root hub
             */

            pURB->pTransferBuffer[0] = pHCDData->RHData.uConfigValue;

            /* Update the URB transfer length */

            pURB->uTransferLength = USB_MHCD_RH_GET_CONFIG_RETURN_SIZE;

            /* Update the URB status */

            pURB->nStatus = USBHST_SUCCESS;
            break;
            }
        case USBHST_REQ_GET_DESCRIPTOR:/* Get Descriptor request */
            {
            /* Check the descriptor type */

            switch (pSetup->wValue >> USB_MHCD_RH_DESCRIPTOR_BITPOSITION)
                {
                case USBHST_DEVICE_DESC:
                    {
                    /* Check the length of descriptor requested */

                    if (pSetup->wLength >= USB_MHCD_RH_DEVICE_DESC_SIZE)
                        {
                        /* Update the URB transfer length */

                        pURB->uTransferLength = USB_MHCD_RH_DEVICE_DESC_SIZE;
                        }
                    else
                        {
                        /* Update the URB transfer length */

                        pURB->uTransferLength = pSetup->wLength;
                        }

                    /* Copy the descriptor to the URB transfer buffer */

                    OS_MEMCPY(pURB->pTransferBuffer,
                              gMhdrcHcdRHDeviceDescriptor,
                              pURB->uTransferLength);

                    /* Update the URB status */

                    pURB->nStatus = USBHST_SUCCESS;

                    break;
                    }
                case USBHST_CONFIG_DESC:
                    {
                    /* Check the length of descriptor requested */

                    if (pSetup->wLength >= USB_MHCD_RH_CONFIG_DESC_SIZE)
                        {
                        /* Update the URB transfer length */

                        pURB->uTransferLength = USB_MHCD_RH_CONFIG_DESC_SIZE;
                        }
                    else
                        {
                        /* Update the URB transfer length */

                        pURB->uTransferLength = pSetup->wLength;
                        }

                    /* Copy the descriptor to the URB transfer buffer */

                    OS_MEMCPY(pURB->pTransferBuffer,
                              gMhdrcHcdRHConfigDescriptor,
                              pURB->uTransferLength);

                    /* Update the URB status */

                    pURB->nStatus = USBHST_SUCCESS;
                    break;
                    }
                default:
                    {
                    /* Invalid descriptor type */

                    pURB->nStatus = USBHST_INVALID_REQUEST;
                    break;
                    }
                }
            break;
            }
        case USBHST_REQ_GET_STATUS:/* Get Status request */
            {
            /* Based on the recipient value, handle the request */

            switch (pSetup->bmRequestType & USB_MHCD_RH_RECIPIENT_MASK)
                {
                case USBHST_RECIPIENT_DEVICE:/* Device recipient */
                    {
                    /* Clear the URB transfer buffer */

                    OS_MEMSET(pURB->pTransferBuffer,0,USB_MHCD_RH_GET_STATUS_RETURN_SIZE);

                    /* Update the device status - Self powered */

                    pURB->pTransferBuffer[0] = 0x01;

                    /* If remote wakeup is enabled, update the status */

                    if (TRUE == pHCDData->RHData.bRemoteWakeupEnabled)
                        {
                        /* Remote wakeup is enabled */

                        pURB->pTransferBuffer[0] |= 0x02;
                        }

                    /* Update the URB transfer length */

                    pURB->uTransferLength = USB_MHCD_RH_GET_STATUS_RETURN_SIZE;

                    /* Update the URB status */

                    pURB->nStatus = USBHST_SUCCESS;

                    break;
                    }
                case USBHST_RECIPIENT_INTERFACE: /* Interface recipient */
                case USBHST_RECIPIENT_ENDPOINT:  /* Endpoint recipient */
                    {
                    /* Update the URB transfer buffer */

                    OS_MEMSET(pURB->pTransferBuffer,
                              0,
                              USB_MHCD_RH_GET_STATUS_RETURN_SIZE);

                    /* Update the URB transfer length */

                    pURB->uTransferLength = USB_MHCD_RH_GET_STATUS_RETURN_SIZE;

                    /* Update the URB status */

                    pURB->nStatus = USBHST_SUCCESS;

                    break;
                    }
                default :
                    {
                    /* Invalid recipient value */

                    pURB->nStatus = USBHST_INVALID_REQUEST;
                    break;
                    }
                }
            break;
            }
        case USBHST_REQ_SET_ADDRESS:/* Set Address request */
            {
            /* Check whether the address is valid */

            if (0 == pSetup->wValue)
                {
                /* Address is not valid */

                pURB->nStatus = USBHST_INVALID_REQUEST;

                break;
                }

            /* Update the RH address */

            pHCDData->RHData.uDeviceAddress = (UINT8)pSetup->wValue;

            /* Update the URB status */

            pURB->nStatus = USBHST_SUCCESS;

            break;
            }
        case USBHST_REQ_SET_CONFIGURATION:/* Set Configuration request */
            {
            /* Check whether the configuration value is valid */

            if ((0 != pSetup->wValue) && (1 != pSetup->wValue))
                {
                /* Invalid configuration value. Update the URB status */

                pURB->nStatus = USBHST_INVALID_REQUEST;

                break;
                }

            /* Update the current configuration value for the root hub */

            pHCDData->RHData.uConfigValue = (UINT8)pSetup->wValue;

            /* Update the URB status */

            pURB->nStatus = USBHST_SUCCESS;

            break;
            }
        case USBHST_REQ_SET_FEATURE:/* Set feature request */
            {
            /* Based on the recipient, handle the request */

            switch (pSetup->bmRequestType & USB_MHCD_RH_RECIPIENT_MASK)
                {
                case USBHST_RECIPIENT_DEVICE: /* Device recipient */
                    {
                    /* Check the feature selector */

                    if (USBHST_FEATURE_DEVICE_REMOTE_WAKEUP == pSetup->wValue)
                        {
                        /* Disable the device remote wakeup feature */

                        pHCDData->RHData.bRemoteWakeupEnabled = TRUE;
                        }

                    /* Update the URB status */

                    pURB->nStatus = USBHST_SUCCESS;
                    break;
                    }
                case USBHST_RECIPIENT_INTERFACE: /* Interface recipient */
                case USBHST_RECIPIENT_ENDPOINT:  /* Endpoint recipient */
                    {

                    /* Update the URB status */

                    pURB->nStatus = USBHST_SUCCESS;
                    break;
                    }
                default:
                    {
                    /* Invalid recipient value */

                    pURB->nStatus = USBHST_INVALID_REQUEST;
                    break;
                    }
                }
            break;
            }
        default :/* Invalid request */
            {
            pURB->nStatus = USBHST_INVALID_REQUEST;
            break;
            }
        }/* End of switch () */

    /* If a callback function is registered, call the callback function */

    if (NULL != pURB->pfCallback)
        {
        (pURB->pfCallback)(pURB);
        }

    return Status;

    }


/*******************************************************************************
*
* usbMhdrcHcdRootHubProcessClassSpecificRequest - process a class specific request
*
* This routine processes a class specific request.
*
* RETURNS:
*  USBHST_SUCCESS if the URB is submitted successfully.
*  USBHST_INVALID_PARAMETER if the parameters are not valid.
*  USBHST_INSUFFICIENT_BANDWIDTH if memory is insufficient for the request.
*
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbMhdrcHcdRootHubProcessClassSpecificRequest
    (
    pUSB_MHCD_DATA pHCDData,    /* Pointer to HCD block           */
    pUSBHST_URB    pURB         /* Pointer to User Request Block  */
    )
    {
    USBHST_STATUS        Status = USBHST_SUCCESS;
    pUSBHST_SETUP_PACKET pSetup = NULL;

    /* Check the validity of the parameters */

    if ((NULL == pHCDData) ||
        (NULL == pURB) ||
        (NULL == pURB->pTransferSpecificData))
        {
        USB_MHDRC_ERR("usbMhdrcHcdRootHubProcessClassSpecificRequest(): "
                     "Invalid parameters, %s is NULL\n",
                     ((NULL == pHCDData) ? "pHCDData" :
                     (NULL == pURB) ? "pURB" :
                     "pURB->pTransferSpecificData"),
                     2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /*
     * Check if the Root hub is configured
     * to accept any class specific request
     */

    if (0 == pHCDData->RHData.uConfigValue)
        {
        USB_MHDRC_ERR("usbMhdrcHcdRootHubProcessClassSpecificRequest(): "
                     "Invalid request - device not configured\n",
                     1, 2, 3, 4, 5 ,6);

        return USBHST_INVALID_REQUEST;
        }

    /* Extract the setup packet from the URB */

    pSetup = (pUSBHST_SETUP_PACKET)pURB->pTransferSpecificData;
    OS_BUFFER_CPU_TO_LE(pSetup, sizeof(USBHST_SETUP_PACKET));

    /* Handle the class specific request */

    switch(pSetup->bRequest)
        {
        case USBHST_REQ_CLEAR_FEATURE:/* Clear feature request */
            {
            /* Based on the recipient value, handle the request */

            switch (pSetup->bmRequestType & USB_MHCD_RH_RECIPIENT_MASK)
                {
                case USBHST_RECIPIENT_DEVICE:
                    {
                    /* None of the hub class features are supported */

                    pURB->nStatus = USBHST_INVALID_REQUEST;
                    break;
                    }
                case USBHST_RECIPIENT_OTHER: /* Clear Port Feature request */
                    {
                    /*
                     * Call the function which handles
                     * the clear port feature request
                     */

                    Status = usbMhdrcHcdRootHubClearPortFeature(pHCDData, pURB);
                    break;
                    }
                default :
                    {
                    pURB->nStatus = USBHST_INVALID_REQUEST;
                    break;
                    }

                }
            break;
            }
        case USBHST_REQ_GET_DESCRIPTOR:/* Hub Get Descriptor request */
            {
            /*
             * Call the function to handle the
             * Root hub Get descripor request.
             */

            Status = usbMhdrcHcdRootHubGetHubDescriptor(pHCDData, pURB);
            break;
            }
        case USBHST_REQ_GET_STATUS:/* Get status request */
            {
            /* Based on the recipient value, handle the request */

            switch (pSetup->bmRequestType & USB_MHCD_RH_RECIPIENT_MASK)
                {
                case USBHST_RECIPIENT_DEVICE:
                    {
                    /*
                     * The hub status cannot be retrieved. So send the status
                     * always as zero
                     */

                    OS_MEMSET(pURB->pTransferBuffer, 0, USB_HUB_STATUS_SIZE);

                    /* Update the URB status */

                    pURB->nStatus = USBHST_SUCCESS;
                    break;
                    }
                case USBHST_RECIPIENT_OTHER: /* GetPortStatus request */
                    {
                    /*
                     * Call the function which handles
                     * the Get Port Status request
                     */

                    Status = usbMhdrcHcdRootHubGetPortStatus(pHCDData, pURB);
                    break;
                    }
                default :
                    {
                    pURB->nStatus = USBHST_INVALID_REQUEST;
                    break;
                    }
                }
            break;
            }
        case USBHST_REQ_SET_FEATURE:/* Set Feature request */
            {
            /* Based on the recipient value, handle the request */

            switch (pSetup->bmRequestType & USB_MHCD_RH_RECIPIENT_MASK)
                {
                case USBHST_RECIPIENT_DEVICE:
                    {
                    /*
                     * None of the sethubfeature requests are supported
                     * in the Root hub
                     */

                    /* Update the URB status */

                    pURB->nStatus = USBHST_INVALID_REQUEST;

                    break;
                    }
                case USBHST_RECIPIENT_OTHER: /* GetPortStatus request */
                    {
                    /*
                     * Call the function which handles
                     * the Get Port Status request
                     */

                    Status = usbMhdrcHcdRootHubSetPortFeature(pHCDData, pURB);
                    break;
                    }
                default :
                    {
                    /* Invalid request */

                    pURB->nStatus = USBHST_INVALID_REQUEST;
                    break;
                    }
                }
            break;
            }
        default:
            {
            /* Invalid request */

            USB_MHDRC_DBG("usbMhdrcHcdRootHubProcessClassSpecificRequest(): "
                         "USBHST_INVALID_REQUEST :\n"
                         "bRequest %p wValue %p wIndex %p wLength %p\n",
                         pSetup->bmRequestType,
                         pSetup->bRequest,
                         pSetup->wValue,
                         pSetup->wIndex,
                         pSetup->wLength, 6);

            pURB->nStatus = USBHST_INVALID_REQUEST;
            }
        }/* End of switch */

    /* If a callback function is registered, call the callback function */

    if (NULL != pURB->pfCallback)
        {
        (pURB->pfCallback)(pURB);
        }
    return Status;
    }


/*******************************************************************************
*
* usbMhdrcHcdRootHubProcessControlRequest -  process a control transfer request
*
* This routine processes a control transfer request.
*
* RETURNS:
*  USBHST_SUCCESS if the URB is submitted successfully.
*  USBHST_INVALID_PARAMETER if the parameters are not valid.
*  USBHST_INSUFFICIENT_BANDWIDTH if memory is insufficient for the request.
*
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbMhdrcHcdRootHubProcessControlRequest
    (
    pUSB_MHCD_DATA pHCDData,           /* Pointer to HCD block           */
    pUSBHST_URB    pURB                /* Pointer to User Request Block  */
    )
    {
    /* Status of the request */

    USBHST_STATUS Status = USBHST_FAILURE;

    /* Pointer to the setup packet */

    pUSBHST_SETUP_PACKET pSetup = NULL;

    /* Check the validity of the parameters */

    if ((NULL == pHCDData) ||
        (NULL == pURB) ||
        (NULL == pURB->pTransferSpecificData))
        {
        USB_MHDRC_ERR("usbMhdrcHcdRootHubProcessControlRequest(): "
                     "Invalid parameters, %s is NULL\n",
                     ((NULL == pHCDData) ? "pHCDData" :
                     (NULL == pURB) ? "pURB" :
                     "pURB->pTransferSpecificData"),
                     2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the setup packet from the URB */

    pSetup = (pUSBHST_SETUP_PACKET)pURB->pTransferSpecificData;

    OS_BUFFER_CPU_TO_LE(pSetup, sizeof(USBHST_SETUP_PACKET));

    /* Check if it is a standard request */

    if (USB_MHCD_RH_STANDARD_REQUEST ==
                        (USB_MHCD_RH_REQUEST_TYPE & pSetup->bmRequestType))
        {
        Status = usbMhdrcHcdRootHubProcessStandardRequest(pHCDData, pURB);
        }

    /* Check if it is a class specific request */

    else if (USB_MHCD_RH_CLASS_SPECIFIC_REQUEST ==
                    (USB_MHCD_RH_REQUEST_TYPE & pSetup->bmRequestType))
        {
        Status = usbMhdrcHcdRootHubProcessClassSpecificRequest(pHCDData, pURB);
        }
    else
        {
        USB_MHDRC_VDBG("usbMhdrcHcdRootHubProcessControlRequest(): "
                      "Invalid request\n", 1, 2, 3, 4, 5 ,6);

        Status = USBHST_INVALID_PARAMETER;
        }

    return Status;
    }


/*******************************************************************************
*
* usbMhdrcHcdRootHubCreatePipe - creat a roothubs pipe specific to an endpoint
*
* This routine creates a pipe specific to an endpoint.
*
* RETURNS:
*  USBHST_SUCCESS - if the pipe was created successfully.
*  USBHST_INVALID_PARAMETER if the parameters are not valid.
*  USBHST_INSUFFICIENT_MEMORY if the memory allocation for the pipe failed.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbMhdrcHcdRootHubCreatePipe
    (
    pUSB_MHCD_DATA pHCDData,            /* Pointer to HCD block           */
    UINT8          uDeviceAddress,      /* Device Address                 */
    UINT8          uDeviceSpeed,        /* Device Speed                   */
    UCHAR *        pEndpointDescriptor, /* Pointer to EndPoint Descriptor */
    ULONG *        puPipeHandle         /* Pointer to pipe handle         */
    )
    {
    USBHST_STATUS               Status = USBHST_FAILURE;
    pUSBHST_ENDPOINT_DESCRIPTOR pEndpointDesc = NULL;
    UINT8                       uPowerReg       = 0;

    /*
     * Check the validity of the parameters
     * The root hub should be USBHST_HIGH_SPEED
     */

    if ((NULL == pHCDData) ||
        (USB_MHDRC_HCD_MAX_DEVICE_ADDRESS < uDeviceAddress) ||
        (NULL == pEndpointDescriptor) ||
        (NULL == puPipeHandle) ||
        (USBHST_HIGH_SPEED != uDeviceSpeed))
        {

        USB_MHDRC_ERR("usbMhdrcHcdRootHubCreatePipe(): "
                     "Invalid parameters, %s \n",
                     ((NULL == pHCDData) ? "pHCDData is NULL" :
                     (USB_MHDRC_HCD_MAX_DEVICE_ADDRESS < uDeviceAddress) ?
                     "uDeviceAddress > 127" :
                     (NULL == pEndpointDescriptor) ?
                     "pEndpointDescriptor is NULL" :
                     "puPipeHandle is NULL"),
                     2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the endpoint descriptor */

    pEndpointDesc = (pUSBHST_ENDPOINT_DESCRIPTOR)pEndpointDescriptor;

    switch(pEndpointDesc->bmAttributes & USB_ATTR_EPTYPE_MASK)
        {
        case USBHST_CONTROL_TRANSFER:/* Control endpoint */
            {

            if (0 != (pEndpointDesc->bEndpointAddress))
                {
                return USBHST_INVALID_REQUEST;
                }
            else
                {
                pUSB_MUSBMHDRC       pMHDRC;

                /* Allocate memory for the control endpoint */

                pHCDData->RHData.pControlPipe =
                          (pUSB_MHCD_PIPE)OS_MALLOC(sizeof(USB_MHCD_PIPE));

                /* Check if memory allocation is successful */

                if (NULL == pHCDData->RHData.pControlPipe)
                    {
                    USB_MHDRC_ERR("usbMhdrcHcdRootHubCreatePipe (): "
                                 "Memory not allocated for control pipe\n",
                                 1, 2, 3, 4, 5 ,6);

                    return USBHST_INSUFFICIENT_MEMORY;
                    }

                /* Get the common hardware data structure */

                pMHDRC = pHCDData->pMHDRC;

                /* Initialize the memory allocated */

                OS_MEMSET(pHCDData->RHData.pControlPipe, 0, sizeof(USB_MHCD_PIPE));
                pHCDData->RHData.pControlPipe->uPipeFlag = USB_MHCD_PIPE_FLAG_OPEN;

                /* Record the parameters of the endpoint */

                pHCDData->RHData.pControlPipe->uEndpointAddress =
                                                pEndpointDesc->bEndpointAddress;
                pHCDData->RHData.pControlPipe->uDeviceAddress = uDeviceAddress;

                /* The pipe speed */

                uPowerReg =  USB_MHDRC_REG_READ8(pMHDRC,
                                              USB_MHDRC_POWER);

                if (uPowerReg & USB_MHDRC_POWER_HSEN)
                    {
                    pHCDData->RHData.pControlPipe->uSpeed = USBHST_HIGH_SPEED;
                    }
                else
                    {
                    pHCDData->RHData.pControlPipe->uSpeed = USBHST_FULL_SPEED;
                    }

                pHCDData->RHData.pControlPipe->uEndpointType =
                                              USBHST_CONTROL_TRANSFER;
                pHCDData->RHData.pControlPipe->uDataToggle = 0;

                *(puPipeHandle) = (ULONG)pHCDData->RHData.pControlPipe;

                Status = USBHST_SUCCESS;
                }
            break;
            }
        case USBHST_INTERRUPT_TRANSFER:
            {
            /* Allocate memory for the interrupt endpoint */

            pHCDData->RHData.pInterruptPipe =
                            (pUSB_MHCD_PIPE)OS_MALLOC(sizeof(USB_MHCD_PIPE));

            if (NULL == pHCDData->RHData.pInterruptPipe)
                {
                USB_MHDRC_ERR("usbMhdrcHcdRootHubCreatePipe(): "
                             "Memory not allocated for interrupt pipe\n",
                             1, 2, 3, 4, 5 ,6);

                return USBHST_INSUFFICIENT_MEMORY;
                }

            /* Initialize the memory allocated */

            OS_MEMSET(pHCDData->RHData.pInterruptPipe, 0, sizeof(USB_MHCD_PIPE));
            pHCDData->RHData.pInterruptPipe->uPipeFlag = USB_MHCD_PIPE_FLAG_OPEN;

            /* Record the parameters of the endpoint */

            pHCDData->RHData.pInterruptPipe->uEndpointAddress =
                                             pEndpointDesc->bEndpointAddress;
            pHCDData->RHData.pInterruptPipe->uDeviceAddress = uDeviceAddress;
            pHCDData->RHData.pInterruptPipe->uSpeed = USBHST_HIGH_SPEED;
            pHCDData->RHData.pInterruptPipe->uEndpointType =
                                                    USBHST_INTERRUPT_TRANSFER;
            pHCDData->RHData.pInterruptPipe->uDataToggle = 0;

            /* Update the pipe handle information */

            *(puPipeHandle) = (ULONG)pHCDData->RHData.pInterruptPipe;

            Status = USBHST_SUCCESS;

            break;
            }
        default:
            {
            Status = USBHST_INVALID_REQUEST;
            }
        }

    return Status;
    }


/*******************************************************************************
*
* usbMhdrcHcdRootHubDeletePipe - delete a roothubs pipe specific to an endpoint
*
* This routine deletes a pipe specific to an endpoint.
*
* RETURNS:
*  USBHST_SUCCESS if the pipe was deleted successfully.
*  USBHST_INVALID_PARAMETER if the parameters are not valid.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbMhdrcHcdRootHubDeletePipe
    (
    pUSB_MHCD_DATA pHCDData,           /* Pointer to HCD block   */
    ULONG          uPipeHandle         /* Pipe Handle Identifier */
    )
    {
     /* To hold the pointer to the pipe */

    pUSB_MHCD_PIPE  pHCDPipe = NULL;

    /* Check the validity of the parameters */

    if ((NULL == pHCDData) ||
        (0 == uPipeHandle))
        {
        USB_MHDRC_ERR("usbMhdrcHcdRootHubDeletePipe(): "
                     "Invalid parameter, %s \n",
                     ((NULL == pHCDData) ?
                     "pHCDData is NULL" :
                     "uPipeHandle is 0"),
                     2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the USB_MHCD_PIPE pointer */

    pHCDPipe = (pUSB_MHCD_PIPE)uPipeHandle;

    /* Check if it is a control pipe delete request */

    if (pHCDPipe == pHCDData->RHData.pControlPipe)
        {
        OS_FREE(pHCDData->RHData.pControlPipe);
        pHCDData->RHData.pControlPipe = NULL;
        }

    /* Check if it is an interrupt pipe delete request */

    else if (pHCDPipe == pHCDData->RHData.pInterruptPipe)
        {
        /* There may need some special action with the interrupt pipe */

        OS_FREE(pHCDData->RHData.pInterruptPipe);
        pHCDData->RHData.pInterruptPipe = NULL;
        }
    else
        {
        USB_MHDRC_ERR("usbMhdrcHcdRootHubDeletePipe(): "
                     "Invalid pipe handle\n",
                     1, 2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    return USBHST_SUCCESS;

    }


/*******************************************************************************
*
* usbMcdRootHubSubmitURB - submit a request to an endpoint.
*
* This routine submits a request to an endpoint.
*
* RETURNS:
*  USBHST_SUCCESS if the URB is submitted successfully.
*  USBHST_INVALID_PARAMETER if the parameters are not valid.
*  USBHST_INSUFFICIENT_BANDWIDTH if memory is insufficient for the request.
*
* ERRNO: N/A.
*
* \NOMANUAL
*/

USBHST_STATUS usbMhdrcHcdRootHubSubmitURB
    (
    pUSB_MHCD_DATA pHCDData,           /* Pointer to HCD block          */
    ULONG          uPipeHandle,        /* Pipe Handle Identifier        */
    pUSBHST_URB    pURB                /* Pointer to User Request Block */
    )
    {
    /* Status of the request */

    USBHST_STATUS Status = USBHST_FAILURE;

    /* Check if the parameters are valid */

    if ((NULL == pHCDData) ||
        (0 == uPipeHandle) ||
        (NULL == pURB))
        {
        USB_MHDRC_ERR("usbMcdRootHubSubmitURB(): "
                     "Invalid parameter, %s \n",
                     ((NULL == pHCDData) ?
                     "pHCDData is NULL" :
                     (0 == uPipeHandle) ?
                     "uPipeHandle is 0" :
                     "pURB is NULL"),
                     2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Check if it is a control request */

    if (uPipeHandle == (ULONG)(pHCDData->RHData.pControlPipe) ||
        ((uPipeHandle == (ULONG)pHCDData->pDefaultPipe) &&
        (0 == pHCDData->RHData.uDeviceAddress)))
        {
        Status = usbMhdrcHcdRootHubProcessControlRequest(pHCDData, pURB);
        }

    /* Check if it is an interrupt request */

    else if (uPipeHandle == (ULONG)(pHCDData->RHData.pInterruptPipe))
        {
        Status = usbMhdrcHcdRootHubProcessInterruptRequest(pHCDData, pURB);
        }
    else
        {
        USB_MHDRC_ERR("usbMcdRootHubSubmitURB(): "
                     "Invalid pipe handle\n", 1, 2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    return Status;
    }


/*******************************************************************************
*
* usbMhdrcHcdRootHubCancelURB - cancel a control transfer request
*
* This routine cancels a control transfer request.
*
* RETURNS:
*  USBHST_SUCCESS if the URB is submitted successfully.
*  USBHST_INVALID_PARAMETER if the parameters are not valid.
*  USBHST_INSUFFICIENT_BANDWIDTH if memory is insufficient for the request.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbMhdrcHcdRootHubCancelURB
    (
    pUSB_MHCD_DATA pHCDData,           /* Pointer to HCD block          */
    ULONG          uPipeHandle,        /* Pipe Handle Identifier        */
    pUSBHST_URB    pURB                /* Pointer to User Request Block */
    )
    {

    /* Check the validity of the parameters */

    if ((NULL == pHCDData) ||
        (0 == uPipeHandle) ||
        (NULL == pURB))
        {
        USB_MHDRC_ERR("usbMhdrcHcdRootHubCancelURB(): "
                     "Invalid parameter, %s \n",
                     ((NULL == pHCDData) ?
                     "pHCDData is NULL" :
                     (0 == uPipeHandle) ?
                     "uPipeHandle is 0" :
                     "pURB is NULL"),
                     2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }
    /*
     * If cancel the urb of the root hub, just return the urb callback,
     * there is no pending on hardware.
     */

    semTake(pHCDData->pHcdSynchMutex, WAIT_FOREVER);

   if (pHCDData->RHData.pPendingInterruptRequest != NULL)
        {
        if (pHCDData->RHData.pPendingInterruptRequest->pUrb == pURB)
            {
            pHCDData->RHData.pPendingInterruptRequest = NULL;
            }
        }

    semGive(pHCDData->pHcdSynchMutex);

    /* update the URB status to cancelled */

    pURB->nStatus = USBHST_TRANSFER_CANCELLED;

    /* Call the callback function if it is registered */

    if (NULL != pURB->pfCallback)
        {
        (pURB->pfCallback)(pURB);
        }

    /* Return a success status */

    return USBHST_SUCCESS;
    }

