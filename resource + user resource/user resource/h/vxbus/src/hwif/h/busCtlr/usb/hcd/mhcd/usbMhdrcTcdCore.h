/* usbMhdrcTcdCore.h - USB Mentor Graphics TCD core module */

/*
 * Copyright (c) 2010, 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,23mar11,m_y  code clean up based on the review result
01a,21oct10,m_y  write
*/

/*
DESCRIPTION

This file contains the prototypes of core functions for the USB Mentor Graphics
Target Controller Driver.

*/

#ifndef __INCusbMhdrcTcdCoreh
#define __INCusbMhdrcTcdCoreh

#ifdef  __cplusplus
extern "C" {
#endif

/* declartion */

STATUS usbMhdrcTcdSuspend
    (
    pUSBTGT_TCD pTCD
    );

STATUS usbMhdrcTcdResume
    (
    pUSBTGT_TCD pTCD
    );

STATUS usbMhdrcTcdWakeUp
    (
    pUSBTGT_TCD pTCD
    );

STATUS usbMhdrcTcdDisconnect
    (
    pUSBTGT_TCD pTCD
    );
STATUS usbMhdrcTcdReset
    (
    pUSBTGT_TCD pTCD
    );
STATUS usbMhdrcTcdSoftConnect
    (
    pUSBTGT_TCD pTCD,
    BOOL        isConnectUp
    );
#endif /* __INCusbMhdrcTcdCoreh */
