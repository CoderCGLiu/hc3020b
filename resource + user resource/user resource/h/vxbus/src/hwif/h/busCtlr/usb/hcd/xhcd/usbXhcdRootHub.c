/* usbXhcdRootHub.c - USB xHCD Root Hub Emulation */

/*
 * Copyright (c) 2011-2014 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01h,04Dec14,wyy  Clean compiler warning
01g,03may13,wyy  Remove compiler warning (WIND00356717)
01f,19oct12,w_x  Unify some USB3 hub macro names
01e,17oct12,w_x  Fix compiler warnings (WIND00370525)
01d,17oct12,w_x  Support for Warm Reset (WIND00374209)
01c,07sep12,j_x  add BOS support (WIND00374277)
01b,04sep12,w_x  Address review comments
01a,29may11,w_x  written
*/

#include "usbXhcdInterfaces.h"

/* locals */

/*
 * BOS descriptor for Root Hub. Details refer CHAP 9.6.2 of
 * USB 3.0 Specification
 */

UINT8 gXhcdRHBosDescriptor[] = {
                                  /* BOS descriptor */
                                  0x05,        /* bLength */
                                  0X0F,        /* BOS descriptor */
                                  0x0F, 0x00,  /* wTotalLength */
                                  0x01,        /* number of device cap */

                                  /* Device Capability Descriptor */
                                  0x0A,        /* bLength */
                                  0x10,        /* device cap descriptor */
                                  0x03,        /* supper speed usb */
                                  0x00,        /* bmAttributes */
                                  0x08, 0x00,  /* wSpeedsSupported */
                                  0x03,        /* bFunctionalitySupported */
                                  0x00,        /* bU1DevExitLat */
                                  0x00, 0x00   /* wU2DevExitLat */
                               };

/*
 * Descriptor structure returned on a request for Configuration descriptor
 * for the Root hub. This includes the Configuration descriptor, interface
 * descriptor and the endpoint descriptor
 */

UINT8 gXhcdRHConfigDescriptor[] = {
                                  /* Configuration descriptor */
                                  0x09,        /* bLength */
                                  0x02,        /* Configuration descriptor */
                                  0x19, 0x00,  /* wTotalLength */
                                  0x01,        /* 1 interface */
                                  0x01,        /* bConfigurationValue */
                                  0x00,        /* iConfiguration */
                                  0xE0,        /* bmAttributes */
                                  0x00,        /* bMaxPower */

                                  /* Interface Descriptor */
                                  0x09,        /* bLength */
                                  0x04,        /* Interface Descriptor */
                                  0x00,        /* bInterfaceNumber */
                                  0x00,        /* bAlternateSetting */
                                  0x01,        /* bNumEndpoints */
                                  0x09,        /* bInterfaceClass */
                                  0x00,        /* bInterfaceSubClass */
                                  0x00,        /* bInterfaceProtocol */
                                  0x00,        /* iInterface */

                                  /* Endpoint Descriptor */
                                  0x07,        /* bLength */
                                  0x05,        /* Endpoint Descriptor */
                                  0x81,         /* bEndpointAddress */
                                  0x03,         /* bmAttributes */
                                  0x08,   0x00, /* wMaxPacketSize */
                                  0x0C          /* bInterval - 255 ms */
                                  };

#define USB_XHCD_RH_GET_CONFIG_RETURN_SIZE  (0x01)
#define USB_XHCD_RH_CONFIG_VALUE            (0x01)
#define USB_XHCD_RH_DEVICE_DESC_SIZE        (0x12)
#define USB_XHCD_RH_BOS_DESC_SIZE           (0x0F)
#define USB_XHCD_RH_CONFIG_DESC_SIZE        (0x19)
#define USB_XHCD_RH_GET_STATUS_RETURN_SIZE  (0x02)
#define USB_XHCD_RH_DESCRIPTOR_BITPOSITION  (0x08)
#define USB_XHCD_DEVICE_SELF_POWERED            (0x0001)
#define USB_XHCD_DEVICE_REMOTE_WAKEUP_ENABLED   (0x0002)

/*******************************************************************************
*
* usbXhcdRootHubCopyInterruptData - copy the interrupt status data.
*
* This function is used to copy the interrupt status data
* to the request buffer if a request is pending or to
* the interrupt status buffer if a request is not pending.
*
* <pHCDData> - Pointer to the USB_XHCD_DATA structure.
* <uStatusChange > - Status change data.
*
* RETURNS: TRUE, or FALSE if the data is not successfully copied.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOLEAN usbXhcdRootHubCopyInterruptData
    (
    pUSB_XHCD_DATA           pHCDData,      /* Pointer to the HCD structure */
    pUSB_XHCD_ROOT_HUB_DATA  pRootHub,      /* Pointer to the Root Hub */
    UINT32                   uStatusChange  /* Status change data */
    )
    {
    pUSBHST_URB            pURB = NULL;         /* Pointer to request info */
    UINT32                 uInterruptData = 0;  /* Interrupt data */

    /* Check the validity of the parameters */

    if ((NULL == pHCDData) || (0 == uStatusChange))
        {
        USB_XHCD_ERR("usbXhcdRootHubCopyInterruptData - "
                     "Invalid parameter, pHCDData %p uStatusChange 0x%X\n",
                     pHCDData, uStatusChange, 3, 4, 5 ,6);

        return FALSE;
        }

    (void) semTake(pHCDData->hcdMutex, WAIT_FOREVER);

    /*
     * If there is any request which is pending for the root hub
     * interrupt endpoint, populate the URB.
     * This has to be done by exclusively accessing the request list.
     */

    /* Copy the status information which is stored already */

    OS_MEMCPY(&uInterruptData,
              pRootHub->pHubInterruptData,
              pRootHub->uHubInterruptDataSize);

    /* Swap the data to CPU endian format */

    uStatusChange = USB_XHCD_SWAP_USB_DATA32(pHCDData, uStatusChange);

    uInterruptData |= uStatusChange;

    /* Copy the data to the interrupt data buffer */

    OS_MEMCPY(pRootHub->pHubInterruptData,
              &uInterruptData,
              pRootHub->uHubInterruptDataSize);


    /*
     * This condition will pass when devices are kept connected
     * when the system is booted
     */

    if ((pRootHub->pInterruptPipe == NULL) ||
        ((pURB = pRootHub->pPendingInterruptURB) == NULL) ||
        (pURB->pfCallback == NULL))
        {
        USB_XHCD_DBG("usbXhcdRootHubCopyInterruptData - "
                      "No pending Request for uStatusChange = 0x%08x\n",
                      uStatusChange, 2, 3, 4, 5, 6);

        (void) semGive(pHCDData->hcdMutex);

        return TRUE;
        }

    USB_XHCD_VDBG ("usbXhcdRootHubCopyInterruptData - "
                  "Calling callback uStatusChange = 0x%08x\n",
                  uStatusChange, 2, 3, 4, 5, 6);

    /* Populate the data buffer */

    OS_MEMCPY(pURB->pTransferBuffer,
              pRootHub->pHubInterruptData,
              pRootHub->uHubInterruptDataSize);

    OS_MEMSET(pRootHub->pHubInterruptData,
              0,
              pRootHub->uHubInterruptDataSize);

    /* Update the status */

    pURB->nStatus = USBHST_SUCCESS;

    /* Update the length */

    pURB->uTransferLength = pRootHub->uHubInterruptDataSize;

    pRootHub->pPendingInterruptURB = NULL;

    (void) semGive(pHCDData->hcdMutex);

    /* Call the callback function.*/

    pURB->pfCallback(pURB);

    return TRUE;
    }


/*******************************************************************************
*
* usbXhcdRootHubSetPortFeature - set the features of the port
*
* This routine sets the features of the port.
*
* RETURNS:
*  USBHST_SUCCESS if the URB is submitted successfully.
*  USBHST_INVALID_PARAMETER if the parameters are not valid.
*
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbXhcdRootHubSetPortFeature
    (
    pUSB_XHCD_DATA           pHCDData, /* Pointer to HCD block           */
    pUSB_XHCD_ROOT_HUB_DATA  pRootHub, /* Pointer to the Root Hub        */
    pUSBHST_URB              pURB      /* Pointer to User Request Block  */
    )
    {
    USBHST_STATUS        nStatus = USBHST_SUCCESS; /* Request status */
    pUSBHST_SETUP_PACKET pSetup = NULL; /* Pointer to the setup packet */
    UINT32               uPortIdx;
    UINT32               uPortSc;
    pUSB_XHCD_ROOT_PORT_INFO pPortInfo; /* Pointer to Port info */

    /* Word size value field for specific req */

    UINT16 wValue;

    /* Word size index field for specific req */

    UINT16 wIndex;

    /* Check the validity of the parameters */

    if ((NULL == pHCDData) ||
        (NULL == pURB) ||
        (NULL == pURB->pTransferSpecificData))
        {
        USB_XHCD_ERR("usbXhcdRootHubSetPortFeature - Invalid parameters,"
                    "pHCDData %p pURB %p pTransferSpecificData %p\n",
                     pHCDData,
                     pURB,
                     (pURB ? pURB->pTransferSpecificData : NULL), 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the setup packet */

    pSetup = (pUSBHST_SETUP_PACKET)pURB->pTransferSpecificData;

    /* Swap the 16 bit values */

    wValue = OS_UINT16_LE_TO_CPU(pSetup->wValue);
    wIndex = OS_UINT16_LE_TO_CPU(pSetup->wIndex);

    /* Get the relative port number in the low byte of wIndex */

    uPortIdx = (wIndex & 0xFF);

    /* If the device isn't connect we will return directly */

    if ((uPortIdx > pRootHub->uNumPorts) || (uPortIdx == 0))
        {
        USB_XHCD_ERR("usbXhcdRootHubSetPortFeature - Invalid port index %d\n",
                     uPortIdx, 2, 3, 4, 5 ,6);

        pURB->nStatus = USBHST_INVALID_PARAMETER;

        return USBHST_INVALID_PARAMETER;
        }

    USB_XHCD_VDBG("usbXhcdRootHubSetPortFeature - wIndex %d wValue 0x%x\n",
                 wIndex, wValue, 3, 4, 5 ,6);

    /* Get the port index of the root hub */

   uPortIdx = pRootHub->uPortOffset + (uPortIdx - 1);

    pPortInfo = &pHCDData->pRootPorts[uPortIdx];

    /* Get current port status */

    uPortSc = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_PORTSC(uPortIdx));

    switch (wValue)
        {
        case USB3_HUB_FSEL_PORT_LINK_STATE:
            {
            if (pRootHub->uRevMajor == USB_XHCI_SUPPORTED_PROT_USB3)
                {
                /*
                 * When the feature selector is PORT_LINK_STATE,
                 * the most significant byte (bits 15..8) of the
                 * wIndex field specifies the U state the host
                 * software wants to put the link connected to
                 * the port into.
                 */

                UINT32 uPLS = (wIndex >> 8);

                USB_XHCD_WARN("USB3_HUB_FSEL_PORT_LINK_STATE "
                              "set PLS to %d\n",
                              uPLS, 2, 3, 4, 5, 6);

                uPortSc = USB_XHCI_PORTSC_KEEP(uPortSc);

                /* Clear the PLS field */

                uPortSc &= ~(USB_XHCI_PORTSC_PLS_MASK);

                /*
                 * The Port Link State Write Strobe (LWS) shall also
                 * be set to '1' to write this field.
                 */

                uPortSc |= USB_XHCI_PORTSC_LWS;

                uPortSc |= USB_XHCI_PORTSC_PLS(uPLS);

                USB_XHCD_WRITE_OP_REG32(pHCDData,
                    USB_XHCI_PORTSC(uPortIdx), uPortSc);
                }
            else
                {
                USB_XHCD_ERR("USB3_HUB_FSEL_PORT_LINK_STATE "
                             "not valid for USB2 protocol ports\n",
                             1, 2, 3, 4, 5, 6);
                nStatus = USBHST_INVALID_REQUEST;
                }
            }
            break;
        case USB3_HUB_FSEL_PORT_U1_TIMEOUT:
            {
            if (pRootHub->uRevMajor == USB_XHCI_SUPPORTED_PROT_USB3)
                {
                /*
                 * When the feature selector is PORT_U1_TIMEOUT, the most
                 * significant byte (bits 15..8) of the wIndex field specifies
                 * the Timeout value for the U1 inactivity timer.
                 */

                UINT32 uTimeout = (wIndex >> 8);
                UINT32 uPORTPMSC = USB_XHCD_READ_OP_REG32(pHCDData,
                                            USB_XHCI_PORTPMSC(uPortIdx));

                uPORTPMSC &= ~(USB_XHCI_PORTPMSC_U1_TIMEOUT_MASK);
                uPORTPMSC |= USB_XHCI_PORTPMSC_U1_TIMEOUT(uTimeout);

                USB_XHCD_WRITE_OP_REG32(pHCDData,
                    USB_XHCI_PORTPMSC(uPortIdx), uPORTPMSC);
                }
            else
                {
                USB_XHCD_ERR("USB3_HUB_FSEL_PORT_U1_TIMEOUT "
                             "not valid for USB2 protocol ports\n",
                             1, 2, 3, 4, 5, 6);
                nStatus = USBHST_INVALID_REQUEST;
                }
            }
            break;
        case USB3_HUB_FSEL_PORT_U2_TIMEOUT:
            {
            if (pRootHub->uRevMajor == USB_XHCI_SUPPORTED_PROT_USB3)
                {
                /*
                 * When the feature selector is PORT_U2_TIMEOUT, the most
                 * significant byte (bits 15..8) of the wIndex field specifies
                 * the Timeout value for the U2 inactivity timer.
                 */

                UINT32 uTimeout = (wIndex >> 8);
                UINT32 uPORTPMSC = USB_XHCD_READ_OP_REG32(pHCDData,
                                            USB_XHCI_PORTPMSC(uPortIdx));

                uPORTPMSC &= ~(USB_XHCI_PORTPMSC_U2_TIMEOUT_MASK);
                uPORTPMSC |= USB_XHCI_PORTPMSC_U2_TIMEOUT(uTimeout);

                USB_XHCD_WRITE_OP_REG32(pHCDData,
                    USB_XHCI_PORTPMSC(uPortIdx), uPORTPMSC);
                }
            else
                {
                USB_XHCD_ERR("USB3_HUB_FSEL_PORT_U2_TIMEOUT "
                             "not valid for USB2 protocol ports\n",
                             1, 2, 3, 4, 5, 6);
                nStatus = USBHST_INVALID_REQUEST;
                }
            }
            break;
        case USB3_HUB_FSEL_PORT_REMOTE_WAKE_MASK:
            {

            }
            break;
        case USB3_HUB_FSEL_BH_PORT_RESET:
            {
            USB_XHCD_WARN("usbXhcdRootHubSetPortFeature - "
                          "USB3_HUB_FSEL_BH_PORT_RESET\n",
                          1, 2, 3, 4, 5, 6);

            usbXhcdPortReset(pHCDData, uPortIdx, TRUE);
            }
            break;
        case USB3_HUB_FSEL_FORCE_LINKPM_ACCEPT:
            {

            }
            break;
        case USB_HUB_PORT_FEATURE_SUSPEND:
            {
            USB_XHCD_DBG("usbXhcdRootHubSetPortFeature - "
                          "USB_HUB_PORT_FEATURE_SUSPEND\n",
                          1, 2, 3, 4, 5, 6);
            }
            break;
        case USB_HUB_PORT_FEATURE_RESET:
            {
            USB_XHCD_WARN("usbXhcdRootHubSetPortFeature - "
                          "USB_HUB_PORT_FEATURE_RESET\n",
                          1, 2, 3, 4, 5, 6);

            usbXhcdPortReset(pHCDData, uPortIdx, FALSE);
            }
            break;
         case USB_HUB_PORT_FEATURE_POWER:
            {
            USB_XHCD_DBG("usbXhcdRootHubSetPortFeature - "
                          "USB_HUB_PORT_FEATURE_POWER\n",
                          1, 2, 3, 4, 5, 6);

            uPortSc = USB_XHCI_PORTSC_KEEP(uPortSc);
            uPortSc |= USB_XHCI_PORTSC_PP;

            USB_XHCD_WRITE_OP_REG32(pHCDData,
                USB_XHCI_PORTSC(uPortIdx), uPortSc);
            }
            break;
         case USB_HUB_PORT_FEATURE_ENABLE:
            {
            USB_XHCD_DBG("usbXhcdRootHubSetPortFeature - "
                          "USB_HUB_PORT_FEATURE_ENABLE\n",
                          1, 2, 3, 4, 5, 6);
            }
            break;
         default:
            {
            USB_XHCD_DBG("usbXhcdRootHubSetPortFeature - "
                          "Unknown wValue\n",
                          wValue, 2, 3, 4, 5, 6);

            nStatus = USBHST_INVALID_REQUEST;
            }
            break;
        }

    pURB->nStatus = nStatus;

    return nStatus;
    }

/*******************************************************************************
*
* usbXhcdRootHubClearPortFeature - clear a feature of the port
*
* This routine clears a feature of the port.
*
* RETURNS:
*  USBHST_SUCCESS - if the URB is submitted successfully.
*  USBHST_INVALID_PARAMETER- if the parameters are not valid.
*
* ERRNO:
*   None.
*/

USBHST_STATUS usbXhcdRootHubClearPortFeature
    (
    pUSB_XHCD_DATA           pHCDData, /* Pointer to HCD block           */
    pUSB_XHCD_ROOT_HUB_DATA  pRootHub, /* Pointer to the Root Hub        */
    pUSBHST_URB              pURB      /* Pointer to User Request Block  */
    )
    {
    USBHST_STATUS        Status = USBHST_SUCCESS;
    pUSBHST_SETUP_PACKET pSetup = NULL;
    UINT32               uPortIdx;
    UINT32               uPortSc;
    /* Word size value field for specific req */

    UINT16 wValue;

    /* Word size index field for specific req */

    UINT16 wIndex;

    /* Check the validity of the parameters */

    if ((NULL == pHCDData) ||
        (NULL == pURB) ||
        (NULL == pURB->pTransferSpecificData))
        {
        USB_XHCD_ERR("usbXhcdRootHubClearPortFeature - Invalid parameters,"
                    "pHCDData %p pURB %p pTransferSpecificData %p\n",
                     pHCDData,
                     pURB,
                     (pURB ? pURB->pTransferSpecificData : NULL), 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the setup packet */

    pSetup = (pUSBHST_SETUP_PACKET)pURB->pTransferSpecificData;

    /* Swap the 16 bit values */

    wValue = OS_UINT16_LE_TO_CPU(pSetup->wValue);
    wIndex = OS_UINT16_LE_TO_CPU(pSetup->wIndex);

    /* Get the relative port number in the low byte of wIndex */

    uPortIdx = (wIndex & 0xFF);

    /* Check whether the members are valid */

    if ((uPortIdx > pRootHub->uNumPorts) || (uPortIdx == 0))
        {
        USB_XHCD_ERR("usbXhcdRootHubClearPortFeature - Invalid port index %d\n",
                     uPortIdx, 2, 3, 4, 5 ,6);

        pURB->nStatus = USBHST_INVALID_PARAMETER;

        return USBHST_INVALID_PARAMETER;
        }

    USB_XHCD_VDBG("usbXhcdRootHubClearPortFeature - wIndex %d wValue 0x%x\n",
                 wIndex, wValue, 3, 4, 5 ,6);

    /* Get the port index of the root hub */

   uPortIdx = pRootHub->uPortOffset + (uPortIdx - 1);

    /* Get the port status */

    uPortSc = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_PORTSC(uPortIdx));

    /* Handle the request based on the feature to be cleared */

    switch (wValue)
        {
        case USB_HUB_PORT_FEATURE_CONNECTION:
        case USB_HUB_PORT_FEATURE_OVER_CURRENT:
        case USB_HUB_PORT_FEATURE_POWER:
        case USB_HUB_PORT_FEATURE_INDICATOR:
            USB_XHCD_WARN("usbXhcdRootHubClearPortFeature - "
                          "%d\n",
                          wValue, 2, 3, 4, 5, 6);
            pURB->uTransferLength = 0;
            pURB->nStatus = USBHST_SUCCESS;
            break;

        case USB_HUB_PORT_FEATURE_ENABLE:
            USB_XHCD_WARN("usbXhcdRootHubClearPortFeature - "
                          "USB_HUB_PORT_FEATURE_ENABLE\n",
                          1, 2, 3, 4, 5, 6);
            pURB->uTransferLength = 0;
            pURB->nStatus = USBHST_SUCCESS;
            break;

        case USB_HUB_PORT_FEATURE_SUSPEND:

            USB_XHCD_WARN("usbXhcdRootHubClearPortFeature - "
                          "USB_HUB_PORT_FEATURE_SUSPEND\n",
                          1, 2, 3, 4, 5, 6);


            break;

        case USB_HUB_PORT_FEATURE_RESET:

            USB_XHCD_WARN("usbXhcdRootHubClearPortFeature - "
                          "USB_HUB_PORT_FEATURE_RESET\n",
                          1, 2, 3, 4, 5, 6);

            uPortSc = USB_XHCI_PORTSC_KEEP(uPortSc);

            if (pRootHub->uRevMajor == USB_XHCI_SUPPORTED_PROT_USB3)
                {
                uPortSc &= ~(USB_XHCI_PORTSC_PR | USB_XHCI_PORTSC_WPR);
                }
            else
                {
                uPortSc &= ~(USB_XHCI_PORTSC_PR);
                }

            OS_DELAY_MS(50);

            USB_XHCD_WRITE_OP_REG32(pHCDData,
                USB_XHCI_PORTSC(uPortIdx), uPortSc);

            pURB->nStatus = USBHST_SUCCESS;

            break;

        /* acknowledge changes: */

        case USB_HUB_PORT_FEATURE_C_CONNECTION:
            USB_XHCD_WARN("usbXhcdRootHubClearPortFeature - "
                          "USB_HUB_PORT_FEATURE_C_CONNECTION\n",
                          1, 2, 3, 4, 5, 6);

            uPortSc = USB_XHCI_PORTSC_KEEP(uPortSc);
            uPortSc |= USB_XHCI_PORTSC_CSC;

            USB_XHCD_WRITE_OP_REG32(pHCDData,
                USB_XHCI_PORTSC(uPortIdx), uPortSc);

            pURB->nStatus = USBHST_SUCCESS;

            break;

        case USB_HUB_PORT_FEATURE_C_ENABLE:
            USB_XHCD_DBG("usbXhcdRootHubClearPortFeature - "
                           "USB_HUB_PORT_FEATURE_C_ENABLE\n",
                           1, 2, 3, 4, 5, 6);

            break;

        case USB_HUB_PORT_FEATURE_C_SUSPEND:

            USB_XHCD_DBG("usbXhcdRootHubClearPortFeature - "
                           "USB_HUB_PORT_FEATURE_C_SUSPEND\n",
                           1, 2, 3, 4, 5, 6);

            break;

        case USB_HUB_PORT_FEATURE_C_RESET:

            USB_XHCD_WARN("usbXhcdRootHubClearPortFeature - "
                          "USB_HUB_PORT_FEATURE_C_RESET\n",
                          1, 2, 3, 4, 5, 6);

            uPortSc = USB_XHCI_PORTSC_KEEP(uPortSc);

            if (pRootHub->uRevMajor == USB_XHCI_SUPPORTED_PROT_USB3)
                {
                uPortSc |= (USB_XHCI_PORTSC_PRC | USB_XHCI_PORTSC_WRC);
                }
            else
                {
                uPortSc |= (USB_XHCI_PORTSC_PRC);
                }

            USB_XHCD_WRITE_OP_REG32(pHCDData,
                USB_XHCI_PORTSC(uPortIdx), uPortSc);

            pURB->nStatus = USBHST_SUCCESS;
            break;

        case USB_HUB_PORT_FEATURE_C_OVER_CURRENT:
            USB_XHCD_DBG("usbXhcdRootHubClearPortFeature - "
                           "USB_HUB_PORT_FEATURE_C_OVER_CURRENT\n",
                           1, 2, 3, 4, 5, 6);
            break;
        case USB3_HUB_FSEL_C_BH_PORT_RESET:
            USB_XHCD_WARN("usbXhcdRootHubClearPortFeature - "
                          "USB3_HUB_FSEL_C_BH_PORT_RESET\n",
                          1, 2, 3, 4, 5, 6);

            uPortSc = USB_XHCI_PORTSC_KEEP(uPortSc);
            uPortSc |= USB_XHCI_PORTSC_WRC;

            USB_XHCD_WRITE_OP_REG32(pHCDData,
                USB_XHCI_PORTSC(uPortIdx), uPortSc);

            pURB->nStatus = USBHST_SUCCESS;
            break;
        case USB3_HUB_FSEL_C_PORT_CONFIG_ERROR:
            USB_XHCD_WARN("usbXhcdRootHubClearPortFeature - "
                          "USB3_HUB_FSEL_C_PORT_CONFIG_ERROR\n",
                          1, 2, 3, 4, 5, 6);

            uPortSc = USB_XHCI_PORTSC_KEEP(uPortSc);
            uPortSc |= USB_XHCI_PORTSC_CEC;

            USB_XHCD_WRITE_OP_REG32(pHCDData,
                USB_XHCI_PORTSC(uPortIdx), uPortSc);

            pURB->nStatus = USBHST_SUCCESS;
            break;
        case USB3_HUB_FSEL_C_PORT_LINK_STATE:
            USB_XHCD_WARN("usbXhcdRootHubClearPortFeature - "
                          "USB3_HUB_FSEL_C_PORT_LINK_STATE\n",
                          1, 2, 3, 4, 5, 6);

            uPortSc = USB_XHCI_PORTSC_KEEP(uPortSc);
            uPortSc |= USB_XHCI_PORTSC_PLC;

            USB_XHCD_WRITE_OP_REG32(pHCDData,
                USB_XHCI_PORTSC(uPortIdx), uPortSc);

            pURB->nStatus = USBHST_SUCCESS;
            break;
        default:
            USB_XHCD_ERR("usbXhcdRootHubClearPortFeature - "
                         "clear feature 0x%02x on port=%d unknown \n",
                         pSetup->wValue,
                         2, 3, 4, 5 ,6);
            break;
        }/* End of switch () */

    return Status;
    }

/*******************************************************************************
*
* usbXhcdRootHubGetHubDescriptor - get the hub descriptor
*
* This routine gets the hub descriptor.
*
* RETURNS:
*  USBHST_SUCCESS - if the URB is submitted successfully.
*  USBHST_INVALID_PARAMETER - if the parameters are not valid.
*  USBHST_MEMORY_NOT_ALLOCATED - if memory allocate fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbXhcdRootHubGetHubDescriptor
    (
    pUSB_XHCD_DATA           pHCDData, /* Pointer to HCD block           */
    pUSB_XHCD_ROOT_HUB_DATA  pRootHub, /* Pointer to the Root Hub        */
    pUSBHST_URB              pURB      /* Pointer to User Request Block  */
    )
    {
    USBHST_STATUS        Status = USBHST_SUCCESS;
    pUSBHST_SETUP_PACKET pSetup = NULL;
    UINT8                uVariableLen = 0;
    UINT32               uIndex = 0;
    UINT8 *              pBuffer = NULL;
    /* Word size value field for specific req */

    UINT16 wValue;

    /* Word size index field for specific req */

    UINT16 wIndex;

    /* Number of bytes in this descriptor */

    UINT8 bDescLength;

     /* Check the validity of the parameters */

    if ((NULL == pHCDData) ||
        (NULL == pURB) ||
        (NULL == pURB->pTransferSpecificData) ||
        (NULL == pURB->pTransferBuffer))
        {
        USB_XHCD_ERR("usbXhcdRootHubGetHubDescriptor - Invalid parameters,"
                    "pHCDData %p pURB %p "
                    "pTransferSpecificData %p pTransferBuffer %p\n",
                     pHCDData,
                     pURB,
                     (pURB ? pURB->pTransferSpecificData : NULL),
                     (pURB ? pURB->pTransferBuffer : NULL), 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the setup packet */

    pSetup = (pUSBHST_SETUP_PACKET)pURB->pTransferSpecificData;

    /* Swap the 16 bit values */

    wValue = OS_UINT16_LE_TO_CPU(pSetup->wValue);
    wIndex = OS_UINT16_LE_TO_CPU(pSetup->wIndex);

    /*
     * Check whether the descriptor type and decriptor index is valid.
     *
     * NOTE: For the root hub, only one hub descriptor is supported.
     *       Further, the descriptor type and descriptor index should
     *       be zero.
     */

    if ((0 != wIndex) ||
        ((pRootHub->uRevMajor == USB_XHCI_SUPPORTED_PROT_USB3) &&
         (((USB_DESCR_USB3_HUB << 8) | 0x00) != wValue)) ||
        ((pRootHub->uRevMajor == USB_XHCI_SUPPORTED_PROT_USB2) &&
         (((USB_DESCR_HUB << 8) | 0x00) != wValue)))
        {
        USB_XHCD_ERR("usbXhcdRootHubGetHubDescriptor - Invalid parameters,"
                    "wValue 0x%x wIndex 0x%x\n",
                     pSetup->wValue, pSetup->wIndex, 3, 4, 5 ,6);

        pURB->nStatus = USBHST_INVALID_PARAMETER;

        return USBHST_INVALID_PARAMETER;
        }

    USB_XHCD_VDBG("usbXhcdRootHubGetHubDescriptor - wIndex 0x%x wValue 0x%x\n",
                 wIndex, wValue, 3, 4, 5 ,6);

    /*
     * Determine the number of bytes that the descriptor would occupy
     * ie. the num of bytes reqired to accomodate info about the ports
     */

    if (pRootHub->uRevMajor == USB_XHCI_SUPPORTED_PROT_USB3)
        {
        bDescLength = 12;
        }
    else
        {
        uVariableLen = (pRootHub->uNumPorts) / 8;
        if (0 != pRootHub->uNumPorts % 8)
            {
            uVariableLen++;
            }

        bDescLength = (UINT8)(7 + (uVariableLen * 2));
        }

    /* Allocate memory for the buffer */

    pBuffer = (UCHAR *)OS_MALLOC(bDescLength);

    /* Check if memory allocation is successful */

    if (NULL == pBuffer)
        {
        USB_XHCD_ERR("usbXhcdRootHubGetHubDescriptor - "
                     "allocate pBuffer fail\n",
                     1, 2, 3, 4, 5 ,6);

        pURB->nStatus = USBHST_MEMORY_NOT_ALLOCATED;

        return USBHST_MEMORY_NOT_ALLOCATED;
        }

    /* Population of the values of hub descriptor - Start */

    /* bDescLength - Length of the descriptor */

    pBuffer[0] = bDescLength;

    /* bDescriptorType - Hub Descriptor type */

    if (pRootHub->uRevMajor == USB_XHCI_SUPPORTED_PROT_USB3)
        {
        pBuffer[1] = USB_DESCR_HUB;
        }
    else
        {
        pBuffer[1] = USB_DESCR_USB3_HUB;
        }

    /* bNbrPorts - Number of downstream ports */

    pBuffer[2] = pRootHub->uNumPorts;

    /*
     * wHubCharacteristics - The following 2 bytes
     * give the hub characteristics. The root hub
     * has individual port overcurrent indication.
     */

    pBuffer[3] = 0x08;
    pBuffer[4] = 0;

    /*
     * bPwrOn2PwrGood - The Power On to Power Good Time
     * for the Root hub is 1 * 2ms
     */

    pBuffer[5] = 1;

    /*
     * bHubContrCurrent - There are no specific maximim
     * current requirements.
     */

    pBuffer[6] = 0;

    if (pRootHub->uRevMajor == USB_XHCI_SUPPORTED_PROT_USB3)
        {
        UINT16  uRemovable;
        UINT8   uPortOffset;
        UINT8   uPortIdx;
        UINT32  uPortSc;

        /* bHubHdrDecLat - Hub Packet Header Decode Latency */

        pBuffer[7] = 0;

        /*
         * wHubDelay - This field defines the average delay
         * in nanoseconds a hub introduces while forwarding
         * packets in either direction.
         */
        pBuffer[8] = 1;
        pBuffer[9] = 0;

        /*
         * DeviceRemovable - Indicates if a port has a removable
         * device attached.
         */

        /* Initialize to 0 */

        uRemovable = 0;

        /* Get Port Offset Base */

        uPortOffset = pRootHub->uPortOffset;

        for (uIndex = 0; uIndex < pRootHub->uNumPorts; uIndex++)
            {
            /* Get the port index of the root hub */

            uPortIdx = (UINT8)(uPortOffset + uIndex);

            /* Get the port status */

            uPortSc = USB_XHCD_READ_OP_REG32(pHCDData,
                        USB_XHCI_PORTSC(uPortIdx));

            /* Check Device Removablek (DR) */

            if (uPortSc & USB_XHCI_PORTSC_DR)
                uRemovable |= (UINT16)(1 << (uIndex + 1));
            }

        /* Update the accumulated value */

        pBuffer[10] = (UINT8)(uRemovable & 0xFF);
        pBuffer[11] = (UINT8)((uRemovable >> 8) & 0xFF);
        }
    else
        {
        /*
         * The last few bytes of the descriptor is based on the
         * number of ports.
         */

        for (uIndex = 0; uIndex < uVariableLen ; uIndex++)
            {
            /* Indicates whether the port is connected removable */

            pBuffer[7 + uIndex] = 0;

            /* Port power control mask should be 1 for all the ports */

            pBuffer[7 + uVariableLen + uIndex] = 0xff;
            }
        }

    /* Population of the values of hub descriptor - End */

    /* Update the length */

    if (pURB->uTransferLength >= bDescLength)
        {
        pURB->uTransferLength = bDescLength;
        }

    /* Copy the data */

    OS_MEMCPY(pURB->pTransferBuffer, pBuffer, pURB->uTransferLength);

    /* Update the status */

    pURB->nStatus = USBHST_SUCCESS;

    /* Free memory allocated for the buffer */

    OS_FREE(pBuffer);

    return Status;
    }

/*******************************************************************************
*
* usbXhcdRootHubGetPortStatus - get the status of the port
*
* This routine gets the status of the port.
*
* RETURNS:
*  USBHST_SUCCESS - if the URB is submitted successfully.
*  USBHST_INVALID_PARAMETER - if the parameters are not valid.
*
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbXhcdRootHubGetPortStatus
    (
    pUSB_XHCD_DATA           pHCDData, /* Pointer to HCD block           */
    pUSB_XHCD_ROOT_HUB_DATA  pRootHub, /* Pointer to the Root Hub        */
    pUSBHST_URB              pURB      /* Pointer to User Request Block  */
    )
    {
    USBHST_STATUS Status = USBHST_SUCCESS;
    pUSBHST_SETUP_PACKET pSetup = NULL;
    UINT32  uPortStatusAndChange = 0;
    UINT32  uPortSc;
    UINT32  uPortIdx;

    pUSB_XHCD_ROOT_PORT_INFO pPortInfo; /* Pointer to Port info */

    /* Word size index field for specific req */

    UINT16 wIndex;

    /* Parameter verification */

    if ((NULL == pHCDData) ||
        (NULL == pURB) ||
        (NULL == pURB->pTransferBuffer) ||
        (NULL == pURB->pTransferSpecificData))
        {
        USB_XHCD_ERR("usbXhcdRootHubGetPortStatus - Invalid parameters,"
                    "pHCDData %p pURB %p "
                    "pTransferSpecificData %p pTransferBuffer %p\n",
                     pHCDData,
                     pURB,
                     (pURB ? pURB->pTransferSpecificData : NULL),
                     (pURB ? pURB->pTransferBuffer : NULL), 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    pSetup = (pUSBHST_SETUP_PACKET)pURB->pTransferSpecificData;

    wIndex = OS_UINT16_LE_TO_CPU(pSetup->wIndex);

    if (wIndex > pRootHub->uNumPorts)
        {
        USB_XHCD_ERR("usbXhcdRootHubGetPortStatus - Invalid port index %d\n",
                     wIndex, 2, 3, 4, 5 ,6);

        pURB->nStatus = USBHST_INVALID_PARAMETER;

        return USBHST_INVALID_PARAMETER;
        }

    /* Get the port index of the root hub */

   uPortIdx = pRootHub->uPortOffset + (wIndex - 1);

    /* Get the port status */

    uPortSc = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_PORTSC(uPortIdx));

    /* Get the previous port status information */

    pPortInfo = &pHCDData->pRootPorts[uPortIdx];

    /* Check if we are USB3 root hub */

    if (pRootHub->uRevMajor == USB_XHCI_SUPPORTED_PROT_USB3)
        {
        /*
         * Following is the interpretation of the 16 bits
         * of Port Status Change for USB3 protocol ports.
         *
         * Bit 8-15 Reserved
         * Bit 7 - Port Config Error (C_PORT_CONFIG_ERROR)
         * Bit 6 - Port Link State Change (C_PORT_LINK_STATE)
         * Bit 5 - BH Reset Change (C_BH_PORT_RESET)
         * Bit 4 - Reset Change (C_PORT_RESET)
         * Bit 3 - Over-Current Indicator Change (C_PORT_OVER_CURRENT)
         * Bit 1-2 Reserved
         * Bit 0 - Connect Status Change (C_PORT_CONNECTION)
         */

        /* Connect Status Change (CSC) */

        if (uPortSc & USB_XHCI_PORTSC_CSC)
            uPortStatusAndChange |= USB_HUB_C_PORT_CONNECTION;

        if ((!(pPortInfo->uPortSc & USB_XHCI_PORTSC_CCS) &&
            (uPortSc & USB_XHCI_PORTSC_CCS)) ||
            ((pPortInfo->uPortSc & USB_XHCI_PORTSC_CCS) &&
             !(uPortSc & USB_XHCI_PORTSC_CCS)))
            uPortStatusAndChange |= USB_HUB_C_PORT_CONNECTION;

        /* Over-current Change (OCC) */

        if (uPortSc & USB_XHCI_PORTSC_OCC)
            uPortStatusAndChange |= USB_HUB_C_PORT_OVER_CURRENT;

        /* Port Reset Change (PRC) */

        if (uPortSc & USB_XHCI_PORTSC_PRC)
            uPortStatusAndChange |= USB_HUB_C_PORT_RESET;

        /* Warm Port Reset Change (WRC) */

        if (uPortSc & USB_XHCI_PORTSC_WRC)
            uPortStatusAndChange |= USB3_HUB_C_BH_PORT_RESET;

        /* Port Link State Change (PLC) */

        if (uPortSc & USB_XHCI_PORTSC_PLC)
            uPortStatusAndChange |= USB3_HUB_C_PORT_LINK_STATE;

        /* Port Config Error Change (CEC) */

        if (uPortSc & USB_XHCI_PORTSC_CEC)
            uPortStatusAndChange |= USB3_HUB_C_PORT_CONFIG_ERROR;

        uPortStatusAndChange = (uPortStatusAndChange << 16);

        /* Update the port status bytes - Start */
        /*
         * Following is the interpretation of the 16 bits of port status
         * Bit 13-15 Reserved
         * Bit 10-12 Negotiated speed of the SuperSpeed Device Attached
         *           to this port (PORT_SPEED) -> different than USB2
         *           (10 PORT_HIGH_SPEED, 11 PORT_TEST, 12 PORT_INDICATOR)
         * Bit 9 - Port Power (PORT_POWER) -> different than USB2 (PORT_LOW_SPEED)
         * Bit 5-8 Port Link State (PORT_LINK_STATE) -> different than USB2
         *         (5-7 Reserved, 8 PORT_POWER)
         * Bit 4 - Reset (PORT_RESET) - same as USB2
         * Bit 3 - Over-current (PORT_OVER_CURRENT) - same as USB2
         * Bit 2 - Reserved - different than USB2 (PORT_SUSPEND)
         * Bit 1 - Port Enabled/Disabled - same as USB2
         * Bit 0 - Current Connect Status (PORT_CONNECTION) - same as USB2
         */

        /* Current Connect Status (CCS) */

        if (uPortSc & USB_XHCI_PORTSC_CCS)
            uPortStatusAndChange |= USB_HUB_STS_PORT_CONNECTION;

        /* Port Enabled/Disabled (PED) */

        if (uPortSc & USB_XHCI_PORTSC_PED)
            uPortStatusAndChange |= USB_HUB_STS_PORT_ENABLE;

        /* Over-current Active (OCA) */

        if (uPortSc & USB_XHCI_PORTSC_OCA)
            uPortStatusAndChange |= USB_HUB_STS_PORT_OVER_CURRENT;

        /* Port Reset (PR) (Hot Reset) */

        if (uPortSc & USB_XHCI_PORTSC_PR)
            uPortStatusAndChange |= USB_HUB_STS_PORT_RESET;

        /* Port Link State (PLS) */

        uPortStatusAndChange |= USB3_HUB_STS_PORT_PLS(
                                USB_XHCI_PORTSC_PLS_GET(uPortSc));

        /* Port Power (PP) */

        if (uPortSc & USB_XHCI_PORTSC_PP)
            uPortStatusAndChange |= USB3_HUB_STS_PORT_POWER;

        /* Port Speed (Port Speed) */

        uPortStatusAndChange |= USB3_HUB_STS_PORT_SPEED(USB3_PORT_SPEED_5Gbps);
        }
    else
        {
        /*
         * Following is the interpretation of the 16 bits
         * of Port Status Change for USB2 protocol ports.
         *
         * Bit 5-15 Reserved
         * Bit 4 - Reset change
         * Bit 3 - Overcurrent indicator Change
         * Bit 2 - Suspend Change
         * Bit 1 - Port Enable/Disable change
         * Bit 0 - Connect status change
         */

        /* Connect Status Change (CSC) */

        if (uPortSc & USB_XHCI_PORTSC_CSC)
            uPortStatusAndChange |= USB_HUB_C_PORT_CONNECTION;

        if ((!(pPortInfo->uPortSc & USB_XHCI_PORTSC_CCS) &&
            (uPortSc & USB_XHCI_PORTSC_CCS)) ||
            ((pPortInfo->uPortSc & USB_XHCI_PORTSC_CCS) &&
             !(uPortSc & USB_XHCI_PORTSC_CCS)))
            uPortStatusAndChange |= USB_HUB_C_PORT_CONNECTION;

        /* Port Enabled/Disabled Change (PEC) */

        if (uPortSc & USB_XHCI_PORTSC_PEC)
            uPortStatusAndChange |= USB_HUB_C_PORT_ENABLE;

        /* Port Link State Change (PLC) */

        if (uPortSc & USB_XHCI_PORTSC_PLC)
            uPortStatusAndChange |= USB_HUB_C_PORT_SUSPEND;

        /* Over-current Active (OCA) */

        if (uPortSc & USB_XHCI_PORTSC_OCA)
            uPortStatusAndChange |= USB_HUB_C_PORT_OVER_CURRENT;

        /* Port Reset Change (PRC) */

        if (uPortSc & USB_XHCI_PORTSC_PRC)
            uPortStatusAndChange |= USB_HUB_C_PORT_RESET;

        uPortStatusAndChange = (uPortStatusAndChange << 16);

        /*
         * Following is the interpretation of the 16 bits of port status
         * Bit 13-15 Reserved
         * Bit 12- Port indicator displays software controlled colour
         * Bit 11- Port is in Port Test Mode
         * Bit 10- High speed device is attached
         * Bit 9 - Low speed device is attached
         * Bit 8 - Port is powered on
         * Bit 5-7 Reserved
         * Bit 4 - Reset signalling asserted
         * Bit 3 - An overcurrent condition exists on the port
         * Bit 2 - Port is suspended
         * Bit 1 - Port is enabled
         * Bit 0 - Device is present on the port
         */

        /* Current Connect Status (CCS) */

        if (uPortSc & USB_XHCI_PORTSC_CCS)
            uPortStatusAndChange |= USB_HUB_STS_PORT_CONNECTION;

        /* Port Enabled/Disabled (PED) */

        if (uPortSc & USB_XHCI_PORTSC_PED)
            uPortStatusAndChange |= USB_HUB_STS_PORT_ENABLE;

        /* Port Link State (PLS) */

        if (USB_XHCI_PORTSC_PLS_GET(uPortSc) == USB_LINK_STATE_U3)
            uPortStatusAndChange |= USB_HUB_STS_PORT_SUSPEND;

        /* Over-current Active (OCA) */

        if (uPortSc & USB_XHCI_PORTSC_OCA)
            uPortStatusAndChange |= USB_HUB_STS_PORT_OVER_CURRENT;

        /* Port Reset (PR) */

        if (uPortSc & USB_XHCI_PORTSC_PR)
            uPortStatusAndChange |= USB_HUB_STS_PORT_RESET;

        /* Port Power (PP) */

        if (uPortSc & USB_XHCI_PORTSC_PP)
            uPortStatusAndChange |= USB_HUB_STS_PORT_POWER;

        /* Port Speed (Port Speed) */

        if (USB_XHCI_PORTSC_PS_GET(uPortSc) == USB_XHCI_PORTSC_PS_HS)
            {
            uPortStatusAndChange |= USB_HUB_STS_PORT_HIGH_SPEED;
            }
        else if (USB_XHCI_PORTSC_PS_GET(uPortSc) == USB_XHCI_PORTSC_PS_LS)
            {
            uPortStatusAndChange |= USB_HUB_STS_PORT_LOW_SPEED;
            }

        /* Port Link State (PLS) */

        if (USB_XHCI_PORTSC_PLS_GET(uPortSc) == USB_LINK_STATE_TestMode)
            uPortStatusAndChange |= USB_HUB_STS_PORT_STATUS_TEST;

        /*
         * Port Indicator Control (PIC): Writing to these bits
         * has no effect if the Port Indicators (PIND) bit in
         * the HCCPARAMS register is a '0'.
         *
         * Port Indicators (PIND). This bit indicates whether
         * the xHC root hub ports support port indicator control.
         */

        if (USB_XHCI_PIND(pHCDData->uHCCPARAMS))
            uPortStatusAndChange |= USB_HUB_STS_PORT_INDICATOR;
        }

    pPortInfo->uPortSc = uPortSc;

    uPortStatusAndChange = USB_XHCD_SWAP_USB_DATA32(pHCDData,
                                                    uPortStatusAndChange);

    OS_MEMCPY(pURB->pTransferBuffer, &uPortStatusAndChange, 4);

    pURB->nStatus = USBHST_SUCCESS;

    USB_XHCD_VDBG("usbXhcdRootHubGetPortStatus - "
        "wIndex %d uPortSc 0x%08x uPortStatusAndChange 0x%08x\n",
        wIndex, uPortSc, uPortStatusAndChange, 4, 5 ,6);

    return Status;
    }


/*******************************************************************************
*
* usbXhcdRootHubInterruptRequest - process a interrupt transfer request
*
* This routine processes a interrupt transfer request.
*
* RETURNS:
* USBHST_SUCCESS if the URB is submitted successfully.
* USBHST_INVALID_PARAMETER if the parameters are not valid.
* USBHST_INSUFFICIENT_BANDWIDTH if memory is insufficient for the request.
*
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbXhcdRootHubInterruptRequest
    (
    pUSB_XHCD_DATA           pHCDData, /* Pointer to HCD block           */
    pUSB_XHCD_ROOT_HUB_DATA  pRootHub, /* Pointer to the Root Hub        */
    pUSBHST_URB              pURB      /* Pointer to User Request Block  */
    )
    {
    USBHST_STATUS  Status = USBHST_SUCCESS;
    UINT32  uInterruptData = 0;

    /* Check the validity of the parameters */

    if ((NULL == pHCDData) ||
        (NULL == pURB) ||
        (NULL == pURB->pTransferBuffer))
        {
        USB_XHCD_ERR("usbXhcdRootHubInterruptRequest - Invalid parameters,"
                     "pHCDData %p pURB %p pTransferBuffer %p\n",
                     pHCDData,
                     pURB,
                     (pURB ? pURB->pTransferBuffer : NULL), 4, 5 ,6);


        return USBHST_INVALID_PARAMETER;
        }

    /*
     * Check if the Root hub is configured
     * to accept any interrupt transfer request.
     */

    if ((0 == pRootHub->uConfigValue) ||
        (NULL == pRootHub->pInterruptPipe) ||
        (NULL == pRootHub->pHubInterruptData))
        {
        USB_XHCD_ERR("usbXhcdRootHubInterruptRequest - "
                     "Invalid request - device not configured"
                     "uConfigValue %d pInterruptPipe %p pHubInterruptData %p\n",
                     pRootHub->uConfigValue,
                     pRootHub->pInterruptPipe,
                     pRootHub->pHubInterruptData, 4, 5 ,6);

        return USBHST_INVALID_REQUEST;
        }

    /* Exclusively access the request resource */

    (void) semTake(pHCDData->hcdMutex, WAIT_FOREVER);

    /* Copy the existing interrupt data  */

    OS_MEMCPY(&uInterruptData,
              pRootHub->pHubInterruptData,
              pRootHub->uHubInterruptDataSize);

    USB_XHCD_VDBG("usbXhcdRootHubInterruptRequest - uInterruptData 0x%08x\n",
        uInterruptData, 2, 3, 4, 5 ,6);

    /* If interrupt data is available, copy the data directly to URB buffer */

    if (0 != uInterruptData)
        {
        OS_MEMCPY(pURB->pTransferBuffer,
                  &uInterruptData,
                  pRootHub->uHubInterruptDataSize);

        /* Initialize the interrupt data */

        OS_MEMSET(pRootHub->pHubInterruptData,
                  0,
                  pRootHub->uHubInterruptDataSize);

        /* Update the length */

        pURB->uTransferLength = pRootHub->uHubInterruptDataSize;

        /* Update the status of URB */

        pURB->nStatus = Status;

        /* Release the exclusive access */

        (void) semGive(pHCDData->hcdMutex);

        /*
         * If a callback function is registered, call the callback
         * function.
         */

        if (pURB->pfCallback)
            {
            pURB->pfCallback(pURB);
            }
        }
    else
        {
        USB_XHCD_VDBG("usbXhcdRootHubInterruptRequest - "
                      "Add as pending URB!\n", 1, 2, 3, 4, 5 ,6);

        /* Add to the request list */

        /* Record the pending request, will return when ready */

        if (pRootHub->pPendingInterruptURB == NULL)
            {
            pRootHub->pPendingInterruptURB = pURB;
            }
        else
            {
            USB_XHCD_ERR("usbXhcdRootHubInterruptRequest "
                          "How that happen? \n", 1, 2, 3, 4, 5 ,6);
            }

        /* Release the exclusive access */

        (void) semGive(pHCDData->hcdMutex);
        }

    return Status;
    }


/*******************************************************************************
*
* usbXhcdRootHubStandardRequest - process a standard transfer request
*
* This routine processes a standard transfer request.
*
* RETURNS:
*  USBHST_SUCCESS if the URB is submitted successfully.
*  USBHST_INVALID_PARAMETER if the parameters are not valid.
*  USBHST_INSUFFICIENT_BANDWIDTH if memory is insufficient for the request.
*
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbXhcdRootHubStandardRequest
    (
    pUSB_XHCD_DATA           pHCDData, /* Pointer to HCD block           */
    pUSB_XHCD_ROOT_HUB_DATA  pRootHub, /* Pointer to the Root Hub        */
    pUSBHST_URB              pURB      /* Pointer to User Request Block  */
    )
    {
    /* To hold the return status of the function */

    USBHST_STATUS Status = USBHST_SUCCESS;

    /* Pointer to the setup packet */

    pUSBHST_SETUP_PACKET pSetup = NULL;

    /* Bitmap for request characteristics */

    UINT8  bmRequestType;

    /* Specific request */

    UINT8  bRequest;

    /* Word size value field for specific req */

    UINT16 wValue;

    /* Word size index field for specific req */

    UINT16 wIndex;

    /* Num of bytes to transfer if data stage */

    UINT16 wLength;

    /* Check the validity of the parameters */

    if ((NULL == pHCDData) ||
        (NULL == pURB) ||
        (NULL == pURB->pTransferSpecificData))
        {
        USB_XHCD_ERR("usbXhcdRootHubStandardRequest - Invalid parameters,"
                     "pHCDData %p pURB %p pTransferSpecificData %p\n",
                     pHCDData,
                     pURB,
                     (pURB ? pURB->pTransferSpecificData : NULL), 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the setup packet from the URB */

    pSetup = (pUSBHST_SETUP_PACKET)pURB->pTransferSpecificData;

    /* Swap the 16 bit values */

    wValue = OS_UINT16_LE_TO_CPU(pSetup->wValue);
    wIndex = OS_UINT16_LE_TO_CPU(pSetup->wIndex);
    wLength = OS_UINT16_LE_TO_CPU(pSetup->wLength);

    /* Get the 8 bit balues */

    bmRequestType = pSetup->bmRequestType;
    bRequest = pSetup->bRequest;

    USB_XHCD_VDBG("usbXhcdRootHubStandardRequest - bRequest 0x%X\n",
        pSetup->bRequest, 2, 3, 4, 5 ,6);

    /* Handle the standard request */

    switch (bRequest)
        {
        case USBHST_REQ_CLEAR_FEATURE:/* Clear feature request */
            {
            /* Based on the recipient, handle the request */

            switch (bmRequestType & USBHST_RECIPIENT_MASK)
                {
                case USBHST_RECIPIENT_DEVICE: /* Device recipient */
                    {
                    /* Check the feature selector */

                    if (USBHST_FEATURE_DEVICE_REMOTE_WAKEUP == wValue)
                        {
                        /* Disable the device remote wakeup feature */

                        pRootHub->bRemoteWakeupEnabled = FALSE;
                        }

                    /* Update the URB status */

                    pURB->nStatus = USBHST_SUCCESS;
                    break;
                    }
                case USBHST_RECIPIENT_INTERFACE: /* Interface recipient */
                case USBHST_RECIPIENT_ENDPOINT:  /* Endpoint recipient */
                    {
                    /* Update the URB status */

                    pURB->nStatus = USBHST_SUCCESS;
                    break;
                    }
                default:
                    {
                    /* Invalid recipient value */

                    pURB->nStatus = USBHST_INVALID_REQUEST;
                    break;
                    }
                }
            break;
            }
        case USBHST_REQ_GET_CONFIGURATION:/* Get Configuration request */
            {
            /*
             * Update the URB transfer buffer with the current configuration
             * value for the root hub
             */

            pURB->pTransferBuffer[0] = pRootHub->uConfigValue;

            /* Update the URB transfer length */

            pURB->uTransferLength = USB_XHCD_RH_GET_CONFIG_RETURN_SIZE;

            /* Update the URB status */

            pURB->nStatus = USBHST_SUCCESS;
            break;
            }
        case USBHST_REQ_GET_DESCRIPTOR:/* Get Descriptor request */
            {
            /* Check the descriptor type */

            switch (wValue >> USB_XHCD_RH_DESCRIPTOR_BITPOSITION)
                {
                case USBHST_DEVICE_DESC:
                    {
                    /* Device Descriptor value for the Root hub */

                    UINT8 gXhcdRHDeviceDescriptor[] =
                        {
                        0x12,        /* bLength */
                        0x01,        /* Device Descriptor type */
                        0x00, 0x02,  /* bcdUSB - USB 2.0 */
                        0x09,        /* Hub DeviceClass */
                        0x00,        /* bDeviceSubClass */
                        0x01,        /* bDeviceProtocol */
                        0x40,        /* Max packet size is 64 */
                        0x00, 0x00,  /* idVendor */
                        0x00, 0x00,  /* idProduct */
                        0x00, 0x00,  /* bcdDevice */
                        0x00,        /* iManufacturer */
                        0x00,        /* iProduct */
                        0x00,        /* iSerialNumber */
                        0x01         /* 1 configuration */
                        };

                    pUSB_DEVICE_DESCR pDeviceDescr =
                        (pUSB_DEVICE_DESCR)gXhcdRHDeviceDescriptor;

                    if (pRootHub->uRevMajor == USB_XHCI_SUPPORTED_PROT_USB3)
                        {
                        pDeviceDescr->bcdUsb = OS_UINT16_CPU_TO_LE(0x0300);
                        pDeviceDescr->deviceProtocol = 0x3;
                        pDeviceDescr->maxPacketSize0 = 0x9;
                        }
                    else
                        {
                        pDeviceDescr->bcdUsb = OS_UINT16_CPU_TO_LE(0x0200);
                        pDeviceDescr->deviceProtocol = 0x1;
                        pDeviceDescr->maxPacketSize0 = 0x40;
                        }

                    /* Check the length of descriptor requested */

                    if (wLength >= USB_XHCD_RH_DEVICE_DESC_SIZE)
                        {
                        /* Update the URB transfer length */

                        pURB->uTransferLength = USB_XHCD_RH_DEVICE_DESC_SIZE;
                        }
                    else
                        {
                        /* Update the URB transfer length */

                        pURB->uTransferLength = wLength;
                        }

                    /* Copy the descriptor to the URB transfer buffer */

                    OS_MEMCPY(pURB->pTransferBuffer,
                              gXhcdRHDeviceDescriptor,
                              pURB->uTransferLength);

                    /* Update the URB status */

                    pURB->nStatus = USBHST_SUCCESS;

                    USB_XHCD_VDBG("usbXhcdRootHubStandardRequest - "
                        "USBHST_DEVICE_DESC - length %d\n",
                        pURB->uTransferLength, 2, 3, 4, 5 ,6);

                    break;
                    }
                case USBHST_BOS_DESC:
                    {
                    /* Check the length of descriptor requested */

                    if (wLength >= USB_XHCD_RH_BOS_DESC_SIZE)
                        {
                        /* Update the URB transfer length */

                        pURB->uTransferLength = USB_XHCD_RH_BOS_DESC_SIZE;
                        }
                    else
                        {
                        /* Update the URB transfer length */

                        pURB->uTransferLength = wLength;
                        }

                    /* Copy the descriptor to the URB transfer buffer */

                    OS_MEMCPY(pURB->pTransferBuffer,
                              gXhcdRHBosDescriptor,
                              pURB->uTransferLength);

                    /* Update the URB status */

                    pURB->nStatus = USBHST_SUCCESS;

                    USB_XHCD_VDBG("usbXhcdRootHubStandardRequest - "
                        "USBHST_BOS_DESC - length %d\n",
                        pURB->uTransferLength, 2, 3, 4, 5 ,6);
                    break;
                    }
                case USBHST_CONFIG_DESC:
                    {
                    /* Check the length of descriptor requested */

                    if (wLength >= USB_XHCD_RH_CONFIG_DESC_SIZE)
                        {
                        /* Update the URB transfer length */

                        pURB->uTransferLength = USB_XHCD_RH_CONFIG_DESC_SIZE;
                        }
                    else
                        {
                        /* Update the URB transfer length */

                        pURB->uTransferLength = wLength;
                        }

                    /* Copy the descriptor to the URB transfer buffer */

                    OS_MEMCPY(pURB->pTransferBuffer,
                              gXhcdRHConfigDescriptor,
                              pURB->uTransferLength);

                    /* Update the URB status */

                    pURB->nStatus = USBHST_SUCCESS;

                    USB_XHCD_VDBG("usbXhcdRootHubStandardRequest - "
                        "USBHST_CONFIG_DESC - length %d\n",
                        pURB->uTransferLength, 2, 3, 4, 5 ,6);
                    break;
                    }
                default:
                    {
                    /* Invalid descriptor type */

                    pURB->nStatus = USBHST_INVALID_REQUEST;
                    break;
                    }
                }
            break;
            }
        case USBHST_REQ_GET_STATUS:/* Get Status request */
            {
            /* Based on the recipient value, handle the request */

            switch (bmRequestType & USBHST_RECIPIENT_MASK)
                {
                case USBHST_RECIPIENT_DEVICE:/* Device recipient */
                    {
                    /* Clear the URB transfer buffer */

                    OS_MEMSET(pURB->pTransferBuffer,
                              0, USB_XHCD_RH_GET_STATUS_RETURN_SIZE);

                    /* Update the device status - Self powered */

                    pURB->pTransferBuffer[0] =
                        USB_XHCD_DEVICE_SELF_POWERED;

                    /* If remote wakeup is enabled, update the status */

                    if (TRUE == pRootHub->bRemoteWakeupEnabled)
                        {
                        /* Remote wakeup is enabled */

                        pURB->pTransferBuffer[0] |=
                            USB_XHCD_DEVICE_REMOTE_WAKEUP_ENABLED;
                        }

                    /* Update the URB transfer length */

                    pURB->uTransferLength = USB_XHCD_RH_GET_STATUS_RETURN_SIZE;

                    /* Update the URB status */

                    pURB->nStatus = USBHST_SUCCESS;

                    break;
                    }
                case USBHST_RECIPIENT_INTERFACE: /* Interface recipient */
                case USBHST_RECIPIENT_ENDPOINT:  /* Endpoint recipient */
                    {
                    /* Update the URB transfer buffer */

                    OS_MEMSET(pURB->pTransferBuffer,
                              0,
                              USB_XHCD_RH_GET_STATUS_RETURN_SIZE);

                    /* Update the URB transfer length */

                    pURB->uTransferLength = USB_XHCD_RH_GET_STATUS_RETURN_SIZE;

                    /* Update the URB status */

                    pURB->nStatus = USBHST_SUCCESS;

                    break;
                    }
                default :
                    {
                    /* Invalid recipient value */

                    pURB->nStatus = USBHST_INVALID_REQUEST;
                    break;
                    }
                }
            break;
            }
        case USBHST_REQ_SET_ADDRESS:/* Set Address request */
            {
            /* Check whether the address is valid */

            if (0 == wValue)
                {
                /* Address is not valid */

                pURB->nStatus = USBHST_INVALID_REQUEST;

                break;
                }

            /* Update the RH address */

            pRootHub->uDeviceAddress = (UINT8)wValue;

            /* Update the URB status */

            pURB->nStatus = USBHST_SUCCESS;

            USB_XHCD_VDBG("usbXhcdRootHubStandardRequest - "
                "USBHST_REQ_SET_ADDRESS - address %d\n",
                pSetup->wValue, 2, 3, 4, 5 ,6);

            break;
            }
        case USBHST_REQ_SET_CONFIGURATION:/* Set Configuration request */
            {
            /* Check whether the configuration value is valid */

            if ((0 != wValue) && (USB_XHCD_RH_CONFIG_VALUE != wValue))
                {
                /* Invalid configuration value. Update the URB status */

                pURB->nStatus = USBHST_INVALID_REQUEST;

                break;
                }

            /* Update the current configuration value for the root hub */

            pRootHub->uConfigValue = (UINT8)wValue;

            /* Update the URB status */

            pURB->nStatus = USBHST_SUCCESS;

            USB_XHCD_VDBG("usbXhcdRootHubStandardRequest - "
                "USBHST_REQ_SET_CONFIGURATION - config %d\n",
                pSetup->wValue, 2, 3, 4, 5 ,6);

            break;
            }
        case USBHST_REQ_SET_FEATURE:/* Set feature request */
            {
            /* Based on the recipient, handle the request */

            switch (bmRequestType & USBHST_RECIPIENT_MASK)
                {
                case USBHST_RECIPIENT_DEVICE: /* Device recipient */
                    {
                    /* Check the feature selector */

                    if (USBHST_FEATURE_DEVICE_REMOTE_WAKEUP == wValue)
                        {
                        /* Disable the device remote wakeup feature */

                        pRootHub->bRemoteWakeupEnabled = TRUE;
                        }

                    /* Update the URB status */

                    pURB->nStatus = USBHST_SUCCESS;
                    break;
                    }
                case USBHST_RECIPIENT_INTERFACE: /* Interface recipient */
                case USBHST_RECIPIENT_ENDPOINT:  /* Endpoint recipient */
                    {

                    /* Update the URB status */

                    pURB->nStatus = USBHST_SUCCESS;
                    break;
                    }
                default:
                    {
                    /* Invalid recipient value */

                    pURB->nStatus = USBHST_INVALID_REQUEST;
                    break;
                    }
                }
            break;
            }
        default :/* Invalid request */
            {
            pURB->nStatus = USBHST_INVALID_REQUEST;
            break;
            }
        }/* End of switch () */

    /* If a callback function is registered, call the callback function */

    if (NULL != pURB->pfCallback)
        {
        (pURB->pfCallback)(pURB);
        }

    return Status;
    }

/*******************************************************************************
*
* usbXhcdRootHubClassSpecificRequest - process a class specific request
*
* This routine processes a class specific request.
*
* RETURNS:
*  USBHST_SUCCESS if the URB is submitted successfully.
*  USBHST_INVALID_PARAMETER if the parameters are not valid.
*  USBHST_INSUFFICIENT_BANDWIDTH if memory is insufficient for the request.
*
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbXhcdRootHubClassSpecificRequest
    (
    pUSB_XHCD_DATA           pHCDData,    /* Pointer to HCD block           */
    pUSB_XHCD_ROOT_HUB_DATA  pRootHub,    /* Pointer to the Root Hub        */
    pUSBHST_URB              pURB         /* Pointer to User Request Block  */
    )
    {
    USBHST_STATUS        Status = USBHST_SUCCESS;
    pUSBHST_SETUP_PACKET pSetup = NULL;

    /* Check the validity of the parameters */

    if ((NULL == pHCDData) ||
        (NULL == pURB) ||
        (NULL == pURB->pTransferSpecificData))
        {
        USB_XHCD_ERR("usbXhcdRootHubClassSpecificRequest - Invalid parameters,"
                     "pHCDData %p pURB %p pTransferSpecificData %p\n",
                     pHCDData,
                     pURB,
                     (pURB ? pURB->pTransferSpecificData : NULL), 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /*
     * Check if the Root hub is configured
     * to accept any class specific request
     */

    if (0 == pRootHub->uConfigValue)
        {
        USB_XHCD_ERR("usbXhcdRootHubClassSpecificRequest - "
                     "Invalid request - device not configured\n",
                     1, 2, 3, 4, 5 ,6);

        return USBHST_INVALID_REQUEST;
        }

    /* Extract the setup packet from the URB */

    pSetup = (pUSBHST_SETUP_PACKET)pURB->pTransferSpecificData;

    USB_XHCD_VDBG("usbXhcdRootHubClassSpecificRequest - bRequest 0x%X\n",
        pSetup->bRequest, 2, 3, 4, 5 ,6);

    /* Handle the class specific request */

    switch (pSetup->bRequest)
        {
        case USBHST_REQ_CLEAR_FEATURE:/* Clear feature request */
            {
            /* Based on the recipient value, handle the request */

            switch (pSetup->bmRequestType & USBHST_RECIPIENT_MASK)
                {
                case USBHST_RECIPIENT_DEVICE:
                    {
                    /* None of the hub class features are supported */

                    pURB->nStatus = USBHST_INVALID_REQUEST;
                    break;
                    }
                case USBHST_RECIPIENT_OTHER: /* Clear Port Feature request */
                    {
                    USB_XHCD_VDBG("usbXhcdRootHubClassSpecificRequest - "
                        "Clear Port Feature\n", 1, 2, 3, 4, 5 ,6);
                    /*
                     * Call the function which handles
                     * the clear port feature request
                     */

                    Status = usbXhcdRootHubClearPortFeature(pHCDData,
                                                            pRootHub,
                                                            pURB);
                    break;
                    }
                default :
                    {
                    pURB->nStatus = USBHST_INVALID_REQUEST;
                    break;
                    }

                }
            break;
            }
        case USBHST_REQ_GET_DESCRIPTOR:/* Hub Get Descriptor request */
            {
            USB_XHCD_VDBG("usbXhcdRootHubClassSpecificRequest - "
                "Get Hub Descriptor\n", 1, 2, 3, 4, 5 ,6);
            /*
             * Call the function to handle the
             * Root hub Get descripor request.
             */

            Status = usbXhcdRootHubGetHubDescriptor(pHCDData,
                                                    pRootHub,
                                                    pURB);
            break;
            }
        case USBHST_REQ_GET_STATUS:/* Get status request */
            {
            /* Based on the recipient value, handle the request */

            switch (pSetup->bmRequestType & USBHST_RECIPIENT_MASK)
                {
                case USBHST_RECIPIENT_DEVICE:
                    {
                    /*
                     * The hub status cannot be retrieved. So send the status
                     * always as zero
                     */

                    OS_MEMSET(pURB->pTransferBuffer, 0, USB_HUB_STATUS_SIZE);

                    /* Update the URB status */

                    pURB->nStatus = USBHST_SUCCESS;
                    break;
                    }
                case USBHST_RECIPIENT_OTHER: /* GetPortStatus request */
                    {
                    USB_XHCD_VDBG("usbXhcdRootHubClassSpecificRequest - "
                        "Get Port Status\n", 1, 2, 3, 4, 5 ,6);
                    /*
                     * Call the function which handles
                     * the Get Port Status request
                     */

                    Status = usbXhcdRootHubGetPortStatus(pHCDData,
                                                         pRootHub,
                                                         pURB);
                    break;
                    }
                default :
                    {
                    pURB->nStatus = USBHST_INVALID_REQUEST;
                    break;
                    }
                }
            break;
            }
        case USBHST_REQ_SET_FEATURE:/* Set Feature request */
            {
            /* Based on the recipient value, handle the request */

            switch (pSetup->bmRequestType & USBHST_RECIPIENT_MASK)
                {
                case USBHST_RECIPIENT_DEVICE:
                    {
                    /*
                     * None of the sethubfeature requests are supported
                     * in the Root hub
                     */

                    /* Update the URB status */

                    pURB->nStatus = USBHST_INVALID_REQUEST;

                    break;
                    }
                case USBHST_RECIPIENT_OTHER: /* SetPortFeature request */
                    {
                    USB_XHCD_VDBG("usbXhcdRootHubClassSpecificRequest - "
                        "Set Port Feature\n", 1, 2, 3, 4, 5 ,6);
                    /*
                     * Call the function which handles
                     * the Get Port Feature request
                     */

                    Status = usbXhcdRootHubSetPortFeature(pHCDData,
                                                          pRootHub,
                                                          pURB);
                    break;
                    }
                default :
                    {
                    /* Invalid request */

                    pURB->nStatus = USBHST_INVALID_REQUEST;
                    break;
                    }
                }
            break;
            }
        case USB3_HUB_REQ_SET_HUB_DEPTH:
            {
            USB_XHCD_VDBG("usbXhcdRootHubClassSpecificRequest - "
                "USB3_HUB_REQ_SET_HUB_DEPTH 0x%02x\n",
                pSetup->wValue, 2, 3, 4, 5 ,6);

            /* Update the URB status */

            pURB->nStatus = USBHST_SUCCESS;
            break;
            }
        default:
            {
            /* Invalid request */

            USB_XHCD_DBG("usbXhcdRootHubClassSpecificRequest - "
                         "USBHST_INVALID_REQUEST :\n"
                         "bRequest %p wValue %p wIndex %p wLength %p\n",
                         pSetup->bmRequestType,
                         pSetup->bRequest,
                         pSetup->wValue,
                         pSetup->wIndex,
                         pSetup->wLength, 6);

            pURB->nStatus = USBHST_INVALID_REQUEST;
            }
        }/* End of switch */

    /* If a callback function is registered, call the callback function */

    if (NULL != pURB->pfCallback)
        {
        (pURB->pfCallback)(pURB);
        }

    return Status;
    }

/*******************************************************************************
*
* usbXhcdRootHubControlRequest -  process a control transfer request
*
* This routine processes a control transfer request.
*
* RETURNS:
*  USBHST_SUCCESS if the URB is submitted successfully.
*  USBHST_INVALID_PARAMETER if the parameters are not valid.
*  USBHST_INSUFFICIENT_BANDWIDTH if memory is insufficient for the request.
*
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbXhcdRootHubControlRequest
    (
    pUSB_XHCD_DATA           pHCDData, /* Pointer to HCD block           */
    pUSB_XHCD_ROOT_HUB_DATA  pRootHub, /* Pointer to the Root Hub        */
    pUSBHST_URB              pURB      /* Pointer to User Request Block  */
    )
    {
    /* Status of the request */

    USBHST_STATUS Status = USBHST_FAILURE;

    /* Pointer to the setup packet */

    pUSBHST_SETUP_PACKET pSetup = NULL;

    /* Requst Category */

    UINT8 uRequstCategory;

    /* Check the validity of the parameters */

    if ((NULL == pHCDData) ||
        (NULL == pURB) ||
        (NULL == pURB->pTransferSpecificData))
        {
        USB_XHCD_ERR("usbXhcdRootHubControlRequest - Invalid parameters,"
                     "pHCDData %p pURB %p pTransferSpecificData %p\n",
                     pHCDData,
                     pURB,
                     (pURB ? pURB->pTransferSpecificData : NULL), 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the setup packet from the URB */

    pSetup = (pUSBHST_SETUP_PACKET)pURB->pTransferSpecificData;

    /* Get Requst Category */

    uRequstCategory = (UINT8)(USB_RT_CATEGORY_MASK & pSetup->bmRequestType);

    /* Check if it is a standard request */

    if (USB_RT_STANDARD == uRequstCategory)
        {
        Status = usbXhcdRootHubStandardRequest(pHCDData, pRootHub, pURB);
        }

    /* Check if it is a class specific request */

    else if (USB_RT_CLASS == uRequstCategory)
        {
        Status = usbXhcdRootHubClassSpecificRequest(pHCDData, pRootHub, pURB);
        }
    else
        {
        USB_XHCD_VDBG("usbXhcdRootHubControlRequest - "
            "Invalid Requst Category 0x%x\n",
            uRequstCategory, 2, 3, 4, 5 ,6);

        Status = USBHST_INVALID_PARAMETER;
        }

    return Status;
    }

/*******************************************************************************
*
* usbXhcdRootHubCreatePipe - creat a pipe specific to a root hub endpoint
*
* This routine creates pipe specific to a root hub endpoint.
*
* RETURNS:
*  USBHST_SUCCESS - if the pipe was created successfully.
*  USBHST_INVALID_PARAMETER if the parameters are not valid.
*  USBHST_INSUFFICIENT_MEMORY if the memory allocation for the pipe failed.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbXhcdRootHubCreatePipe
    (
    pUSB_XHCD_DATA pHCDData,            /* Pointer to HCD block           */
    UINT8          uDeviceAddress,      /* Device Address                 */
    UINT8          uDeviceSpeed,        /* Device Speed                   */
    UCHAR *        pEndpointDescriptor, /* Pointer to EndPoint Descriptor */
    ULONG *        puPipeHandle         /* Pointer to pipe handle         */
    )
    {
    pUSBHST_ENDPOINT_DESCRIPTOR pEndpointDesc;

    /* Pointer to Root Hub Data */

    pUSB_XHCD_ROOT_HUB_DATA  pRootHub;

    /* Pointer to Pipe */

    pUSB_XHCD_PIPE  pPipe;

    /* Endpoint Type */

    UINT8 uEpType;

    /*
     * Check the validity of the parameters
     * The root hub should be USBHST_HIGH_SPEED
     */

    if ((NULL == pHCDData) ||
        (NULL == pEndpointDescriptor) ||
        (NULL == puPipeHandle))
        {
        USB_XHCD_ERR("usbXhcdRootHubCreatePipe - Invalid parameters,"
                     "pHCDData %p pEndpointDescriptor %p puPipeHandle %p\n",
                     pHCDData, pEndpointDescriptor, puPipeHandle, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the endpoint descriptor */

    pEndpointDesc = (pUSBHST_ENDPOINT_DESCRIPTOR)pEndpointDescriptor;

    /* Get the endpoint type */

    uEpType = (UINT8)(pEndpointDesc->bmAttributes & USB_ATTR_EPTYPE_MASK);

    /* Validate the endpoint type */

    if ((uEpType != USBHST_CONTROL_TRANSFER) &&
        (uEpType != USBHST_INTERRUPT_TRANSFER))
        {
        USB_XHCD_ERR("usbXhcdRootHubCreatePipe - Invalid parameters,"
                     "uEpType 0x%x\n", uEpType, 2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Choose the Root Hub based on the device speed */

    if (uDeviceSpeed == USBHST_SUPER_SPEED)
        pRootHub = &pHCDData->USB3RH;
    else
        pRootHub = &pHCDData->USB2RH;

    /* Allocate memory for the control endpoint */

    pPipe = (pUSB_XHCD_PIPE)OSS_CALLOC(sizeof(USB_XHCD_PIPE));

    /* Check if memory allocation is successful */

    if (NULL == pPipe)
        {
        USB_XHCD_ERR("usbXhcdRootHubCreatePipe - "
                     "allocate pipe fail\n",
                     1, 2, 3, 4, 5 ,6);

        return USBHST_INSUFFICIENT_MEMORY;
        }

    /* Copy the endpoint address */

    pPipe->uEpAddr = pEndpointDesc->bEndpointAddress;

    /* Copy the address */

    pPipe->uDevAddr = uDeviceAddress;

    /* Update the speed */

    pPipe->uDevSpeed = uDeviceSpeed;

    /* Set this as root hub pipe */

    pPipe->bRootHubPipe = TRUE;

    /* Update the endpoint type */

    pPipe->uEpXferType = uEpType;

    /* Set to the control/interrupt pipe */

    if (uEpType == USBHST_CONTROL_TRANSFER)
         {
         pRootHub->pControlPipe = pPipe;
         }
     else /* USBHST_INTERRUPT_TRANSFER */
         {
         pRootHub->pInterruptPipe = pPipe;
         }

    /* Return it to the caller */

    *(puPipeHandle) = (ULONG)pPipe;

    return USBHST_SUCCESS;
    }

/*******************************************************************************
*
* usbXhcdRootHubDeletePipe - delete a pipe specific to a root hub endpoint
*
* This routine deletes a pipe specific to a root hub endpoint.
*
* RETURNS:
*  USBHST_SUCCESS if the pipe was deleted successfully.
*  USBHST_INVALID_PARAMETER if the parameters are not valid.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbXhcdRootHubDeletePipe
    (
    pUSB_XHCD_DATA pHCDData,           /* Pointer to HCD block   */
    ULONG          uPipeHandle         /* Pipe Handle Identifier */
    )
    {
    /* To hold the pointer to the pipe */

    pUSB_XHCD_PIPE  pHCDPipe = NULL;

    /* Check the validity of the parameters */

    if ((NULL == pHCDData) ||
        (0 == uPipeHandle))
        {
        USB_XHCD_ERR("usbXhcdRootHubDeletePipe - Invalid parameters,"
                     "pHCDData %p puPipeHandle %p\n",
                     pHCDData, uPipeHandle, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the USB_XHCD_PIPE pointer */

    pHCDPipe = (pUSB_XHCD_PIPE)uPipeHandle;

    /* Check if it is a USB2 control pipe delete request */

    if (pHCDPipe == pHCDData->USB2RH.pControlPipe)
        {
        pHCDData->USB2RH.pControlPipe = NULL;
        }
    /* Check if it is an USB2 interrupt pipe delete request */

    else if (pHCDPipe == pHCDData->USB2RH.pInterruptPipe)
        {
        pHCDData->USB2RH.pInterruptPipe = NULL;
        }
    /* Check if it is a USB3 control pipe delete request */

    else if (pHCDPipe == pHCDData->USB3RH.pControlPipe)
        {
        pHCDData->USB3RH.pControlPipe = NULL;
        }
    /* Check if it is an USB3 interrupt pipe delete request */

    else if (pHCDPipe == pHCDData->USB3RH.pInterruptPipe)
        {
        pHCDData->USB3RH.pInterruptPipe = NULL;
        }
    else
        {
        USB_XHCD_ERR("usbXhcdRootHubDeletePipe - Invalid pipe handle %p\n",
                     uPipeHandle, 2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Free the allocated memory */

    OSS_FREE(pHCDPipe);

    return USBHST_SUCCESS;
    }

/*******************************************************************************
*
* usbXhcdRootHubSubmitURB - submit a request to an endpoint
*
* This routine submits a request to an endpoint.
*
* RETURNS:
*  USBHST_SUCCESS if the URB is submitted successfully.
*  USBHST_INVALID_PARAMETER if the parameters are not valid.
*  USBHST_INSUFFICIENT_BANDWIDTH if memory is insufficient for the request.
*
* ERRNO: N/A.
*
* \NOMANUAL
*/

USBHST_STATUS usbXhcdRootHubSubmitURB
    (
    pUSB_XHCD_DATA pHCDData,           /* Pointer to HCD block          */
    ULONG          uPipeHandle,        /* Pipe Handle Identifier        */
    pUSBHST_URB    pURB                /* Pointer to User Request Block */
    )
    {
    /* Status of the request */

    USBHST_STATUS Status = USBHST_FAILURE;

    /* To hold the pointer to the pipe */

    pUSB_XHCD_PIPE  pHCDPipe = NULL;

    /* Pointer to Root Hub Data */

    pUSB_XHCD_ROOT_HUB_DATA  pRootHub;

    /* Check the validity of the parameters */

    if ((NULL == pHCDData) ||
        (0 == uPipeHandle) ||
        (NULL == pURB))
        {
        USB_XHCD_ERR("usbXhcdRootHubSubmitURB - Invalid parameter,"
                     "pHCDData %p uPipeHandle %p pURB %p\n",
                     pHCDData, uPipeHandle, pURB, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the USB_XHCD_PIPE pointer */

    pHCDPipe = (pUSB_XHCD_PIPE)uPipeHandle;

    /* Choose the Root Hub based on the device speed */

    if (pHCDPipe->uDevSpeed == USBHST_SUPER_SPEED)
        pRootHub = &pHCDData->USB3RH;
    else
        pRootHub = &pHCDData->USB2RH;

    /* Check if it is a control request */

    if (pHCDPipe->uEpXferType == USBHST_CONTROL_TRANSFER)
        {
        Status = usbXhcdRootHubControlRequest(pHCDData, pRootHub, pURB);
        }
    /* Check if it is an interrupt request */

    else if (pHCDPipe->uEpXferType == USBHST_INTERRUPT_TRANSFER)
        {
        Status = usbXhcdRootHubInterruptRequest(pHCDData, pRootHub, pURB);
        }
    else
        {
        USB_XHCD_ERR("usbXhcdRootHubSubmitURB - Invalid pipe handle %p\n",
                     uPipeHandle, 2, 3, 4, 5 ,6);

        return USBHST_INVALID_PARAMETER;
        }

    return Status;
    }


/*******************************************************************************
*
* usbXhcdRootHubCancelURB - cancel a control transfer request
*
* This routine cancels a control transfer request.
*
* RETURNS:
*  USBHST_SUCCESS if the URB is submitted successfully.
*  USBHST_INVALID_PARAMETER if the parameters are not valid.
*  USBHST_INSUFFICIENT_BANDWIDTH if memory is insufficient for the request.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbXhcdRootHubCancelURB
    (
    pUSB_XHCD_DATA pHCDData,           /* Pointer to HCD block          */
    ULONG          uPipeHandle,        /* Pipe Handle Identifier        */
    pUSBHST_URB    pURB                /* Pointer to User Request Block */
    )
    {
    /* Check the validity of the parameters */

    if ((NULL == pHCDData) ||
        (0 == uPipeHandle) ||
        (NULL == pURB))
        {
        USB_XHCD_ERR("usbXhcdRootHubCancelURB - Invalid parameter,"
                     "pHCDData %p uPipeHandle %p pURB %p\n",
                     pHCDData, uPipeHandle, pURB, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }

    /*
     * If cancel the urb of the root hub, just return the urb callback,
     * there is no pending on hardware.
     */

    (void) semTake(pHCDData->hcdMutex, WAIT_FOREVER);

    if (pHCDData->USB2RH.pPendingInterruptURB == pURB)
        {
        pHCDData->USB2RH.pPendingInterruptURB = NULL;
        }
    else if (pHCDData->USB3RH.pPendingInterruptURB == pURB)
        {
        pHCDData->USB3RH.pPendingInterruptURB = NULL;
        }

    (void) semGive(pHCDData->hcdMutex);

    /* Update the URB status to cancelled */

    pURB->nStatus = USBHST_TRANSFER_CANCELLED;

    /* Call the callback function if it is registered */

    if (NULL != pURB->pfCallback)
        {
        (pURB->pfCallback)(pURB);
        }

    /* Return a success status */

    return USBHST_SUCCESS;
    }


