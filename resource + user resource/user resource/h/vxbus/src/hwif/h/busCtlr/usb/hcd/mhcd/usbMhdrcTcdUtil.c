/* usbMhdrcTcdUtil.c -  USB Mentor Graphics TCD utility module */

/*
 * Copyright (c) 2010, 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01f,08jun11,m_y  reset request in usbMhdrcRequestRelease routine
01e,29mar11,m_y  modify to support more than one buffer list
01d,23mar11,m_y  add usbMhdrcRequestReserve and usbMhdrcRequestRelease routine
01c,23feb11,m_y  modify usbMhdrcTcdNextReqGet and usbMhdrcTcdReqComplete
01b,20oct10,m_y  write
01a,18may10,s_z  created
*/

/*
DESCRIPTION

This module defines the functions which serve as utility functions for the
USB Mentor Graphics Target Controller Driver.

INCLUDE FILES: usb/usbOtg.h, usb/usbTgt.h, usbMhdrc.h, usbMhdrcTcd.h,
               usbMhdrcTcdUtil.h, usbMhdrcTcdIsr.h, usbMhdrcHsDma.h 
*/

/* includes */


#include <usb/usbOtg.h>
#include <usb/usbTgt.h>
#include <usbMhdrc.h>
#include <usbMhdrcTcd.h>
#include <usbMhdrcTcdUtil.h>
#include <usbMhdrcTcdIsr.h>
#include <usbMhdrcHsDma.h>

/*******************************************************************************
*
* usbMhdrcTcdFirstReqGet - get first request of the pipe
*
* This routine is used to get the first request of the pipe.
*
* RETURNS: NULL, or pointer of the first request if have 
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_MHDRC_TCD_REQUEST usbMhdrcTcdFirstReqGet
    (
    pUSB_MHDRC_TCD_PIPE pTCDPipe
    )
    {
    NODE *                 pNode = NULL;
    pUSB_MHDRC_TCD_REQUEST pRequest = NULL;

    if (pTCDPipe != NULL)
        {
        OS_WAIT_FOR_EVENT(pTCDPipe->pPipeSyncMutex, WAIT_FOREVER);
        pNode = lstFirst(&(pTCDPipe->requestList));
        if (pNode == NULL)
            pRequest = NULL;
        else
            pRequest = REQUEST_NODE_TO_USB_MHDRC_TCD_REQUEST(pNode);
        OS_RELEASE_EVENT(pTCDPipe->pPipeSyncMutex);
        }
    return pRequest;
    }

/*******************************************************************************
*
* usbMhdrcTcdNextReqGet - get next request
*
* This routine gets the next request 
*
* RETURNS: NULL, or pointer of the next request if found
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_MHDRC_TCD_REQUEST usbMhdrcTcdNextReqGet
    (
    pUSB_MHDRC_TCD_REQUEST pRequest
    )
    {
    NODE *                 pNode = NULL;
    pUSB_MHDRC_TCD_REQUEST pNextRequest = NULL;
    pUSB_MHDRC_TCD_PIPE    pTCDPipe = NULL;

    if (pRequest != NULL)
        {
        pTCDPipe = pRequest->pTCDPipe;
        if (pTCDPipe != NULL)
            {
            OS_WAIT_FOR_EVENT(pTCDPipe->pPipeSyncMutex, WAIT_FOREVER);
            pNode = &(pRequest->requestNode);
            pNode = lstNext(pNode);
            if (pNode == NULL)
                pNextRequest = NULL;
            else
                pNextRequest = REQUEST_NODE_TO_USB_MHDRC_TCD_REQUEST(pNode);
            OS_RELEASE_EVENT(pTCDPipe->pPipeSyncMutex);
            }
        }
    return pNextRequest;
    }

/*******************************************************************************
*
* usbMhdrcRequestReserve - reserve a free request from the pipe
*
* This routine reserves a free request from the pipe
*
* RETURNS: the pointer of the request, or NULL if fail
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_MHDRC_TCD_REQUEST usbMhdrcRequestReserve
    (
    pUSB_MHDRC_TCD_PIPE pTCDPipe
    )
    {
    NODE *                 pNode;
    pUSB_MHDRC_TCD_REQUEST pRequest;

    if (pTCDPipe == NULL)
        return NULL;

    /* Get the first request from the free list */
    
    OS_WAIT_FOR_EVENT(pTCDPipe->pPipeSyncMutex, WAIT_FOREVER);

    pNode = lstFirst(&pTCDPipe->freeRequestList);

    /* 
     * If exist a free request, 
     * remove it from the free list,return the pointer of the request 
     * or else, creat a new one and return the pointer of the request 
     */
     
    if (pNode != NULL)
        {
        lstDelete(&pTCDPipe->freeRequestList, pNode);
        OS_RELEASE_EVENT(pTCDPipe->pPipeSyncMutex);
        return REQUEST_NODE_TO_USB_MHDRC_TCD_REQUEST(pNode);
        }
    else
        {
        OS_RELEASE_EVENT(pTCDPipe->pPipeSyncMutex);
        pRequest = OSS_CALLOC(sizeof(USB_MHDRC_TCD_REQUEST));
        if (pRequest == NULL)
            {
            USB_MHDRC_ERR("Malloc pRequest fail\n", 1, 2, 3, 4, 5, 6);
            return NULL;
            }
        return pRequest;
        }
    }

/*******************************************************************************
*
* usbMhdrcRequestRelease - release a used request 
*
* This routine release a used request add it to the pipe's free list
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcRequestRelease
    (
    pUSB_MHDRC_TCD_PIPE    pTCDPipe,
    pUSB_MHDRC_TCD_REQUEST pRequest
    )
    {
    if ((pTCDPipe == NULL) || (pRequest == NULL))
        return;

    memset(pRequest, 0, sizeof(USB_MHDRC_TCD_REQUEST));
    
    if (ERROR == lstFind(&pTCDPipe->freeRequestList, &pRequest->requestNode))
        lstAdd(&pTCDPipe->freeRequestList, &pRequest->requestNode);
    }

/*******************************************************************************
*
* usbMhdrcTcdGetPipeByEpAddr - find the pipe by the endpoint address and 
* direction
*
* This routine is to find the pipe by the endpoint address and direction.
*
* RETURNS: NULL, or pointer of the pipe if have
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_MHDRC_TCD_PIPE usbMhdrcTcdGetPipeByEpAddr
    (
    pUSB_MHDRC_TCD_DATA pTCDData,
    int                 uEpAddr,
    int                 uEpDir
    )
    {
    NODE *              pNode = NULL;
    pUSB_MHDRC_TCD_PIPE pTCDPipe = NULL;

    if (pTCDData == NULL)
        {
        USB_MHDRC_ERR("Invalid Paramere: pTCDData\n",
                      1, 2, 3, 4, 5, 6);
        return NULL;
        }

    OS_WAIT_FOR_EVENT(pTCDData->tcdSyncMutex, WAIT_FOREVER);

    pNode = lstFirst(&pTCDData->pipeList);

    while (pNode != NULL)
        {
        pTCDPipe = PIPE_NODE_TO_USB_MHDRC_TCD_PIPE(pNode);
        if (pTCDPipe != NULL)
            {
            if ((pTCDPipe->uEpAddress == uEpAddr) &&
                (pTCDPipe->uEpDir == uEpDir))
                {
                OS_RELEASE_EVENT(pTCDData->tcdSyncMutex);
                return pTCDPipe;
                }
            }
        pNode = lstNext(pNode);
        }

    OS_RELEASE_EVENT(pTCDData->tcdSyncMutex);
    return NULL;
    }


/*******************************************************************************
*
* usbMhdrcTcdReqComplete - complete a request 
*
* This routine is to complete a request 
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcTcdReqComplete
    (
    pUSB_MHDRC_TCD_REQUEST   pRequest,
    int                      status
    )
    {
    pUSB_MHDRC_TCD_PIPE  pTCDPipe = NULL;
    pUSB_ERP             pErp = NULL;

    if ((pRequest == NULL) || 
        ((pTCDPipe = pRequest->pTCDPipe) == NULL))
        {
        USB_MHDRC_ERR("Invalid Paramter: pRequest %p, pRequest->pTCDPipe %p\n",
                      pRequest, (pRequest == NULL)? NULL : pRequest->pTCDPipe, 
                      3, 4, 5, 6);
        return;
        }

    /* Update status and call erp's callback to notify the upper */

    pErp = pRequest->pErp;

    if (pErp != NULL)
        {
        /* Remove the request from the pipe list */
        
        OS_WAIT_FOR_EVENT(pTCDPipe->pPipeSyncMutex, WAIT_FOREVER);

        /* 
         * Set the pErp to NULL to avoid re-enter this 
         * routine for the same request 
         */
        
        pRequest->pErp = NULL;
        if (ERROR != lstFind(&(pTCDPipe->requestList), &(pRequest->requestNode)))
            {                                                                 
            pRequest->pTCDPipe = NULL;                                    
            lstDelete(&(pTCDPipe->requestList), &(pRequest->requestNode));
            }  
        
        /* Update actual length */

        if (pRequest->uErpBufIndex < pRequest->uErpBufCount)
            pErp->bfrList[pRequest->uErpBufIndex].actLen = pRequest->uActLength;

        OS_RELEASE_EVENT(pTCDPipe->pPipeSyncMutex);                                                 

        /* 
         * The tcdPtr is equal to the tailRequest 
         * if this request is the last one, we will call the callback routine
         */
         
        if (pErp->tcdPtr == pRequest)                                         
            {                                                              
            pErp->result = status;                                        
            pErp->tcdPtr = NULL;                                          
            if (pErp->targCallback != NULL)                                
                pErp->targCallback(pErp);                                  
            }                                                              
        
        /* release the request's resource */
        
        usbMhdrcRequestRelease(pTCDPipe, pRequest);
        }
    
    return;
    }

/*******************************************************************************
*
* usbMhdrcTcdPipeFlush - flush the pipe to complete all requests
*
* This routine is to complete all requests on the pipe.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcTcdPipeFlush
    (
    pUSB_MHDRC_TCD_PIPE  pTCDPipe,
    int                  status
    )
    {
    NODE *                    pNode = NULL;
    NODE *                    pNextNode = NULL;
    pUSB_MHDRC_TCD_REQUEST    pRequest = NULL;

    if (pTCDPipe == NULL)
        return;
    
    /* Complete all the request, add request into freeRequest list */
    
    pNode = lstFirst(&(pTCDPipe->requestList));
    while (pNode != NULL)
        {
        pRequest = REQUEST_NODE_TO_USB_MHDRC_TCD_REQUEST(pNode);
        if (pRequest != NULL)
            usbMhdrcTcdReqComplete(pRequest, status);
        pNode = lstNext(pNode);
        }

    /* Free request in the freeRequest list */
        
    pNode = lstFirst(&(pTCDPipe->freeRequestList));
    while (pNode != NULL)
        {
        pNextNode = lstNext(pNode);
        pRequest = REQUEST_NODE_TO_USB_MHDRC_TCD_REQUEST(pNode);
        if (pRequest != NULL)
            {
            lstDelete(&(pTCDPipe->freeRequestList), pNode);
            OSS_FREE(pRequest);
            }
        pNode = pNextNode;
        }
    }

