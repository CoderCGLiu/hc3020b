/* usbPlxTcdDma.c -  USB PLX TCD DMA module */

/*
 * Copyright (c) 2012-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,03may13,wyy  Remove compiler warning (WIND00356717)
01a,18may12,s_z  Writen
*/

/*
DESCRIPTION

This module defines the routines which serve as DMA transaction for the
USB PLX Target Controller Driver.

INCLUDE FILES: usbPlxTcdDma.h, usbPlxTcdUtil.h
*/

/* includes */

#include <usbPlxTcdDma.h>
#include <usbPlxTcdUtil.h>

/*******************************************************************************
*
* usbPlxTcdDmaStart - start the DMA transfer
*
* This routine is used to start the DMA transfer for one request.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbPlxTcdDmaStart
    (
    pUSB_PLX_TCD_DATA    pTCDData,
    pUSB_PLX_TCD_PIPE    pTCDPipe,
    pUSB_PLX_TCD_REQUEST pRequest
    )
    {
    pUSB_PLX_CTRL pPlxCtrl = NULL;
    UINT32        uReg32;

    /* Parameter verification */

    if ((NULL == pTCDData) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)) ||
        (NULL == pTCDPipe) ||
        (NULL == pRequest))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTCDData) ? "pTCDData" :
                     (NULL == pPlxCtrl) ? "pPlxCtrl" :
                     (NULL == pTCDPipe) ? "pTCDPipe" :
                    "pRequest"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    USB_PLX_VDBG (" usbPlxTcdDmaStart():uCurXferLength %x\n",
                  pRequest->uCurXferLength, 2, 3, 4, 5, 6);

    if ((pTCDPipe->uDmaChannel == USB_PLX_DMA_CH_IVALID) ||
        ((pRequest->pCurrentBuffer == NULL) &&
         (pRequest->pScatDesc == NULL)))
        {
        USB_PLX_ERR (" usbPlxTcdSingleDmaStart():Invalid Channel\n",
                     1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Prepare the DMA */

    pRequest->bDmaActive = TRUE;

    /*
     * Prepare the DMA instead write to the FIFO
     * Write the address regiser for single DMA type,
     * write the descriptor register for scatter/gather dma type.
     */

    if ((pTCDData->uDmaType == USB_PLX_SG_DMA) &&
        (pTCDPipe->uEpDir == USB_ENDPOINT_IN))
        {
        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_DMADESC_OFFSET(pTCDPipe->uDmaChannel),
                             (ULONG)pRequest->pScatDesc);
        }
    else
        {
        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_DMAADDR_OFFSET(pTCDPipe->uDmaChannel),
                             (ULONG)pRequest->pCurrentBuffer);
        }

    /*
     * Set the DMA Enable bit of DMA Control Register.
     */

    uReg32 = USB_PLX_REG_READ32 (pPlxCtrl,
                                 USB_PLX_DMACTL_OFFSET(pTCDPipe->uDmaChannel));

    if (USB_ENDPOINT_IN == pTCDPipe->uEpDir)
        {
        /*
         * Short packet validation
         * 1. The transfer has the short packet
         * 2. ZLP needed - TODO
         */

        if (pRequest->uCurXferLength % pTCDPipe->uMaxPacketSize)
            {
            uReg32 |= USB_PLX_DMACTL_FIFOVAL;
            }
        }
    else
        {
        uReg32 &= ~(USB_PLX_DMACTL_FIFOVAL);
        }

    /* Set the scatter/gather dma type,and enable interrupt if needed */

    if ((pTCDData->uDmaType == USB_PLX_SG_DMA) &&
        (pTCDPipe->uEpDir == USB_ENDPOINT_IN))
        {
        uReg32 |= (USB_PLX_DMACTL_SGE |
                   USB_PLX_DMACTL_SG_INT_EN);
        }

    /* Enable the DMA */

    /*
     * Set the DMA request outstanding (0x6 << 5) did help the performance
     * but if this bit be set for in endpoit, mass storage function
     * format will be fail.
     */

    if (USB_ENDPOINT_IN == pTCDPipe->uEpDir)
        {
        uReg32 |= (USB_PLX_DMACTL_EN) /* | (0x6 << 5) */;
        }
    else
        {
        uReg32 |= (USB_PLX_DMACTL_EN)  | (0x6 << 5) ;
        }

    USB_PLX_REG_WRITE32 (pPlxCtrl,
                         USB_PLX_DMACTL_OFFSET (pTCDPipe->uDmaChannel),
                         uReg32);

    /* Set the DMA dir, enable the DMA inertupt, and the data length */

    uReg32 = USB_PLX_DMACOUNT_DIE |
             pRequest->uCurXferLength;

    if (USB_ENDPOINT_IN == pTCDPipe->uEpDir)
        {
        uReg32 |= USB_PLX_DMACOUNT_DIR;
        }

    USB_PLX_REG_WRITE32 (pPlxCtrl,
                         USB_PLX_DMACOUNT_OFFSET (pTCDPipe->uDmaChannel),
                         uReg32);

    /*
     * Disable the endpoint interrupts for the corresponding endpoint.
     * Enable short packet OUT done interrupt.
     */

    USB_PLX_REG_WRITE32 (pPlxCtrl,
                         USB_PLX_EP_IRQENB_OFFSET(pTCDPipe->uPhyEpAddr),
                         USB_PLX_EP_IRQENB_SPOD);

    /* Clear all the interrupts for this endpoint */

    uReg32 = USB_PLX_REG_READ32 (pPlxCtrl,
                                 USB_PLX_EP_STAT_OFFSET(pTCDPipe->uPhyEpAddr));

    uReg32 &= ~USB_PLX_EP_STAT_SPOD;

    USB_PLX_REG_WRITE32 (pPlxCtrl,
                         USB_PLX_EP_STAT_OFFSET(pTCDPipe->uPhyEpAddr),
                         uReg32);

    /* Start the DMA Transfer */

    uReg32 = USB_PLX_REG_READ32 (pPlxCtrl,
                     USB_PLX_DMASTAT_OFFSET (pTCDPipe->uDmaChannel));



    USB_PLX_REG_WRITE32 (pPlxCtrl,
                         USB_PLX_DMASTAT_OFFSET (pTCDPipe->uDmaChannel),
                         (uReg32 | USB_PLX_DMASTAT_START));

    USB_PLX_VDBG("usbPlxTcdDmaStart pipe %p with pRequest %p\n",
                 pTCDPipe, pRequest, 3, 4, 5, 6);

    return OK;
    }

/*******************************************************************************
*
* usbPlxTcdDmaStop - stop the DMA transaction
*
* This routine is used to stop the DMA transaction for the right pipe.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbPlxTcdDmaStop
    (
    pUSB_PLX_TCD_DATA pTCDData,
    pUSB_PLX_TCD_PIPE pTCDPipe
    )
    {
    pUSB_PLX_CTRL pPlxCtrl = NULL;
    UINT32        uReg32;

    /* Parameter verification */

    if ((NULL == pTCDData) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)) ||
        (NULL == pTCDPipe))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTCDData) ? "pTCDData" :
                     (NULL == pPlxCtrl) ? "pPlxCtrl" :
                    "pTCDPipe"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Disable the DMA */

    if (pTCDPipe->uDmaChannel != USB_PLX_DMA_CH_IVALID)
        {
        uReg32 = USB_PLX_REG_READ32 (pPlxCtrl,
                                     USB_PLX_DMACTL_OFFSET (pTCDPipe->uDmaChannel));

        uReg32 &= ~(USB_PLX_DMACTL_EN | USB_PLX_DMACTL_FIFOVAL);

        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_DMACTL_OFFSET (pTCDPipe->uDmaChannel),
                             uReg32);
        }

    return OK;
    }



