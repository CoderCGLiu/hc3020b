/* usbTgtMscInit.c - USB Target Mass Storage Class initialized library */

/*
 * Copyright (c) 2010-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification History
--------------------
01l,14nov13,m_y  Adjust LUN index when delete one LUN (WIND00442820)
01k,06may13,s_z  Remove compiler warning (WIND00356717)
01j,17oct12,s_z  Correct compiler warning (WIND00374594) 
01i,15may12,s_z  Correct coverity warning 
01l,10apr12,s_z  Add media change notification (WIND00328309)
01k,13dec11,m_y  Modify according to code check result (WIND00319317)
01j,10oct11,m_y  modify to fix the code coverity defect (WIND00310604)
01i,16aug11,m_y  add usbTgtMscConfigShow help user know the configure              
                 information (WIND00288709)
01h,08apr11,ghs  Fix code coverity issue(WIND00264893)
01g,25mar11,m_y  correct some debug message
01f,24mar11,s_z  Changes for unused routines removed
01e,23mar11,m_y  modify configuration items
                 modify the usbTgtMscLunAdd and usbTgtMscDel routine
01d,17mar11,m_y  modify code based on the code review
01c,03mar11,m_y  code clean up
01b,07feb11,x_f  add for dynamically configuring the storage devices
01a,26sep10,m_y  written.
*/

/*
DESCRIPTION

This module defines those routines directly referenced by the USB peripheral
stack; namely, the routines that intialize the data structure.
Additional routines are also provided which are specific to the
target mass storage class driver.

INCLUDES: vxWorks.h, stdio.h, errnoLib.h, logLib.h, string.h, blkIo.h,
          usb/usbPlatform.h, usb/usb.h, usb/usbOsal.h, usb/usbDescrCopyLib.h,
          usb/usbLib.h, drv/xbd/xbd.h, drv/erf/erfLib.h, mount.h
          usbTgtMscCmd.h, usbTgtMscUtil.h
*/


/* includes */

#include <vxWorks.h>
#include <stdio.h>
#include <errnoLib.h>
#include <logLib.h>
#include <string.h>
#include <blkIo.h>
#include <usb/usbPlatform.h>
#include <usb/usb.h>
#include <usb/usbOsal.h>
#include <usb/usbDescrCopyLib.h>
#include <usb/usbLib.h>
#include <usb/usbTgtMsc.h>
#include <drv/xbd/xbd.h>
#include <drv/erf/erfLib.h>
#include <mount.h>
#include <usbTgtMscCmd.h>
#include <usbTgtMscUtil.h>

/* Function Declartion */

void usbTgtMscExit(void);
LOCAL pUSB_TGT_MSC_LUN usbTgtMscLunNew (pUSB_TGT_MSC_DEV pUsbTgtMscDev);
LOCAL void usbTgtMscLunDelete
    (
    pUSB_TGT_MSC_DEV pUsbTgtMscDev,
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    );
LOCAL pUSB_TGT_MSC_DEV usbTgtMscDevNew (void);
LOCAL void usbTgtMscDevDelete(pUSB_TGT_MSC_DEV pUsbTgtMscDev);
LOCAL void usbTgtMscLunAttach (pUSB_TGT_MSC_DEV, pUSB_TGT_MSC_LUN, device_t);
LOCAL void usbTgtMscLunDetach (pUSB_TGT_MSC_DEV, pUSB_TGT_MSC_LUN);
LOCAL void usbTgtMscAttachHandler (UINT16, UINT16, void *, void *);
LOCAL void usbTgtMscDetachHandler (UINT16, UINT16, void *, void *);

/* Globals */

LOCAL int g_usbConfigCount = 0;

/* Global device list */

LIST g_usbTgtMscDevList;

/* Global event to sync the device list */

OS_EVENT_ID g_usbTgtMscListEvent = NULL;

/* Global flag to indicate the usb target MSC function driver init or not */

BOOLEAN g_usbTgtMscInit = FALSE;

/* Externs */
IMPORT USR_USBTGT_MSC_CONFIG usrUsbTgtMscConfigTable[];

IMPORT STATUS usbTgtMscDriverBind
    (
    pUSB_TGT_MSC_DEV  pUsbTgtMscDev,
    int               index
    );

IMPORT STATUS usbTgtMscDriverUnBind
    (
    pUSB_TGT_MSC_DEV  pUsbTgtMscDev
    );

/*******************************************************************************
*
* usbTgtMscInit - initialize the struct for devices and LUNs
*
* This routine initializes the struct for devices and LUNs.
*
* RETURNS: OK, or ERROR
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbTgtMscInit
    (
    int devCount
    )
    {
    int          i;
    char *       pStr1 = NULL;
    char *       pStr2 = NULL;
    char *       pStr3 = NULL;
    char *       pStrName= NULL;
    char *       pStrRwAttr = NULL;
    char *       pStrRmAttr = NULL;
    device_t     xbdDev = NULLDEV;
    int          retVal = ERROR;
    pUSB_TGT_MSC_LUN  pUsbTgtMscLun = NULL;
    pUSB_TGT_MSC_DEV  pUsbTgtMscDev = NULL;

    if (g_usbTgtMscInit == TRUE)
        {
        USBTGT_MSC_ERR("usbTgtMscInit - already init\n",
                       1, 2, 3, 4, 5, 6);
        return ERROR;
        }
    
    g_usbConfigCount = devCount;

    /* Init the global event  */

    g_usbTgtMscListEvent = OS_CREATE_EVENT(OS_EVENT_SIGNALED);
    if (g_usbTgtMscListEvent == NULL)
        {
        USBTGT_MSC_ERR("usbTgtMscInit - create g_usbTgtMscListEvent fail\n",
                       1, 2, 3, 4, 5, 6);
        return ERROR;
        }
    /* Init the golbal device list */

    lstInit(&g_usbTgtMscDevList);

    /* Set init flag as true */

    g_usbTgtMscInit = TRUE;

    for (i = 0; i < devCount; i++)
        {
        /* Check the protocol type, must be RBC */

        if (strcmp(usrUsbTgtMscConfigTable[i].devProtocol, "RBC") != 0)
            {

            USBTGT_MSC_ERR("Only Support RBC Protocal"
                           "Please check your configuration \n",
                           1, 2, 3, 4, 5, 6);
            usbTgtMscExit();
            return ERROR;
            }

        pUsbTgtMscDev = usbTgtMscDevNew();

        if (pUsbTgtMscDev == NULL)
            {
            usbTgtMscExit();
            return ERROR;
            }

        strncpy(pUsbTgtMscDev->tcdName, usrUsbTgtMscConfigTable[i].tcdName,
                (USBTGT_MAX_NAME_SZ - 1));
        pUsbTgtMscDev->tcdUnit = usrUsbTgtMscConfigTable[i].tcdUnit;
        strncpy(pUsbTgtMscDev->protocolType,
                usrUsbTgtMscConfigTable[i].devProtocol,
                (USBTGT_COMMON_STR_LEN - 1));

        pStrName = usrUsbTgtMscConfigTable[i].devName;
        pStrRwAttr = usrUsbTgtMscConfigTable[i].devReadonly;
        pStrRmAttr = usrUsbTgtMscConfigTable[i].devRemovable;

        pUsbTgtMscDev->nLuns = 0;

        FOREVER
            {
            pStr1 = strtok_r (NULL, ",", &pStrName);
            pStr2 = strtok_r (NULL, ",", &pStrRwAttr);
            pStr3 = strtok_r (NULL, ",", &pStrRmAttr);

            USBTGT_MSC_DBG("pStr1 %s, pStr2 %s, pStr3 %s\n",
                           pStr1, pStr2, pStr3, 4, 5, 6);

            if ((pStr1 == NULL) ||
                (pStr2 == NULL) ||
                (pStr3 == NULL) )
                break;

            pUsbTgtMscLun = usbTgtMscLunNew(pUsbTgtMscDev);

            if (pUsbTgtMscLun == NULL)
                {
                usbTgtMscExit();
                return ERROR;
                }

            strncpy(pUsbTgtMscLun->attachDevName, pStr1, (USBTGT_COMMON_STR_LEN - 1));

            if (strcmp(pStr2, "y") == 0)
                pUsbTgtMscLun->bReadOnly = TRUE;
            else
                pUsbTgtMscLun->bReadOnly = FALSE;

            if (strcmp(pStr3, "y") == 0)
                pUsbTgtMscLun->bRemovable = TRUE;
            else
                pUsbTgtMscLun->bRemovable = FALSE;

            xbdDev = usbTgtMscXBDPartHandleGet(pUsbTgtMscLun->attachDevName);

            if (xbdDev != NULLDEV)
                {
                usbTgtMscLunAttach(pUsbTgtMscDev, pUsbTgtMscLun, xbdDev);
                }
            else
                {
                /*
                 * Even the attached device is not ready, we also add nLuns
                 */

                pUsbTgtMscLun->mediaReady     = FALSE;
                pUsbTgtMscLun->mediaRemoved   = TRUE;
                }
            }
        if (OK != usbTgtMscDriverBind(pUsbTgtMscDev, i))
            {
            USBTGT_MSC_ERR("usbTgtMscInit - init the device structure fail\n",
                           1, 2, 3, 4, 5, 6);
            usbTgtMscExit();
            return ERROR;
            }
        }

    /* Set default lun */

    pUsbTgtMscDev->pCurrentLun = pUsbTgtMscLun;

    /* Register erf handler */

    retVal = erfHandlerRegister(xbdEventCategory, xbdEventPrimaryInsert,
                                usbTgtMscAttachHandler, NULL, ERF_FLAG_NONE);
    if (retVal != OK)
        {
        USBTGT_MSC_ERR("usbTgtMscInit - register xbdEventPrimaryInsert fail\n",
                       1, 2, 3, 4, 5, 6);
        usbTgtMscExit();
        return ERROR;
        }
    
    retVal = erfHandlerRegister(xbdEventCategory, xbdEventSecondaryInsert,
                                usbTgtMscAttachHandler, NULL, ERF_FLAG_NONE);
    if (retVal != OK)
        {
        USBTGT_MSC_ERR("usbTgtMscInit - register xbdEventSecondaryInsert fail\n",
                       1, 2, 3, 4, 5, 6);
        usbTgtMscExit();
        return ERROR;
        }
    
    retVal = erfHandlerRegister(xbdEventCategory, xbdEventRemove,
                                usbTgtMscDetachHandler, NULL, ERF_FLAG_NONE);
    if (retVal != OK)
        {
        USBTGT_MSC_ERR("usbTgtMscInit - register xbdEventRemove fail\n",
                       1, 2, 3, 4, 5, 6);
        usbTgtMscExit();
        return ERROR;
        }
    
    retVal = erfHandlerRegister(xbdEventCategory, xbdEventMediaChanged,
                                usbTgtMscDetachHandler, NULL, ERF_FLAG_NONE);

    if (retVal != OK)
        {
        USBTGT_MSC_ERR("usbTgtMscInit - register xbdEventMediaChanged fail\n",
                       1, 2, 3, 4, 5, 6);
        usbTgtMscExit();
        return ERROR;
        }

    return OK;
    }

/*******************************************************************************
*
* usbTgtMscExit - USB Target Mass Storage Class exit routine
*
* This routine exites the USB Target MSC routine.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbTgtMscExit (void)
    {
    NODE *            pDevNode;
    NODE *            pLunNode;
    int               retVal;
    pUSB_TGT_MSC_DEV  pUsbTgtMscDev;
    pUSB_TGT_MSC_LUN  pUsbTgtMscLun;

    if (g_usbTgtMscInit == TRUE)
        {
        pDevNode = lstFirst(&g_usbTgtMscDevList);

        while (pDevNode != NULL)
            {
            /* Free the devcie structure */

            pUsbTgtMscDev = DEV_NODE_TO_USB_TGT_MSC_DEV(pDevNode);

            if (pUsbTgtMscDev != NULL)
                {
                /* Free all the lun attached to the device */

                pLunNode = lstFirst(&pUsbTgtMscDev->lunList);

                while (pLunNode != NULL)
                    {
                    pUsbTgtMscLun = LUN_NODE_TO_USB_TGT_MSC_LUN(pLunNode);
                    if (pUsbTgtMscLun != NULL)
                        {
                        /* Detach the lun from the device */

                        usbTgtMscLunDetach(pUsbTgtMscDev, pUsbTgtMscLun);

                        /* Delete the lun structure */

                        usbTgtMscLunDelete(pUsbTgtMscDev, pUsbTgtMscLun);
                        }
                    pLunNode = lstNext(pLunNode);
                    }

                /* Unbind with the function driver */

                usbTgtMscDriverUnBind (pUsbTgtMscDev);

                /* Delete the device structure */

                usbTgtMscDevDelete(pUsbTgtMscDev);
                }

            /* Move to next node */

            pDevNode = lstNext(pDevNode);
            }

        /* 
         * Unregister all the erf handlers
         * The return value:ERROR means can't find the related register entry
         * This is acceptable.
         */
        
        retVal = erfHandlerUnregister(xbdEventCategory, xbdEventPrimaryInsert, 
                                      usbTgtMscAttachHandler, NULL);
        if (retVal != OK)
            {
            USBTGT_MSC_WARN("Unregister xbdEventPrimaryInsert fail\n", 
                            1, 2, 3, 4, 5, 6);
            }

        retVal = erfHandlerUnregister(xbdEventCategory, xbdEventSecondaryInsert, 
                                      usbTgtMscAttachHandler, NULL);
        if (retVal != OK)
            {
            USBTGT_MSC_WARN("Unregister xbdEventSecondaryInsert fail\n", 
                            1, 2, 3, 4, 5, 6);
            }

        retVal = erfHandlerUnregister(xbdEventCategory, xbdEventRemove, 
                                      usbTgtMscDetachHandler, NULL);
        if (retVal != OK)
            {
            USBTGT_MSC_WARN("Unregister xbdEventRemove fail\n", 
                            1, 2, 3, 4, 5, 6);
            }

        retVal = erfHandlerUnregister(xbdEventCategory, xbdEventMediaChanged, 
                                      usbTgtMscDetachHandler, NULL);
        if (retVal != OK)
            {
            USBTGT_MSC_WARN("Unregister xbdEventMediaChanged fail\n", 
                            1, 2, 3, 4, 5, 6);
            }

        if (g_usbTgtMscListEvent != NULL)
            semDelete(g_usbTgtMscListEvent);

        g_usbTgtMscInit = FALSE;
        }
    return ;
    }

/*******************************************************************************
*
* usbTgtMscLunNew - create structure for a new LUN
*
* This routine creates a new LUN structure.
*
* RETURNS: pointer of the new LUN structure, or NULL if fail
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL pUSB_TGT_MSC_LUN usbTgtMscLunNew
    (
    pUSB_TGT_MSC_DEV  pUsbTgtMscDev
    )
    {
    pUSB_TGT_MSC_LUN  pUsbTgtMscLun = NULL;

    if (pUsbTgtMscDev == NULL)
        return NULL;

    pUsbTgtMscLun = OSS_CALLOC(sizeof(USB_TGT_MSC_LUN));

    if (pUsbTgtMscLun == NULL)
        return NULL;

    pUsbTgtMscLun->pSenseData = OSS_CALLOC(sizeof(SENSE_DATA));
    if (pUsbTgtMscLun->pSenseData == NULL)
        {
        usbTgtMscLunDelete(pUsbTgtMscDev, pUsbTgtMscLun);
        return NULL;
        }

    pUsbTgtMscLun->pCapacityData = OSS_CALLOC(sizeof(CAPACITY_DATA));
    if (pUsbTgtMscLun->pCapacityData == NULL)
        {
        usbTgtMscLunDelete(pUsbTgtMscDev, pUsbTgtMscLun);
        return NULL;
        }

    if (semBInit ((SEMAPHORE *)&pUsbTgtMscLun->bioDoneSem, 0, SEM_EMPTY) == ERROR)
        {
        USBTGT_MSC_ERR("usbTgtMscLunNew - init bioDoneSem error\n",
                       1, 2, 3, 4, 5, 6);
        usbTgtMscLunDelete(pUsbTgtMscDev, pUsbTgtMscLun);
        return NULL;
        }

    /*
     * Init the senseData
     * except responseCode and additionalSenseLgth other field is 0 as default
     */

    pUsbTgtMscLun->pSenseData->responseCode = 0x70;

    /* additional data to follow should always 0x0a */

    pUsbTgtMscLun->pSenseData->additionalSenseLgth = 0x0a;

    /* Update the lunIndex */

    pUsbTgtMscLun->lunIndex = lstCount(&(pUsbTgtMscDev->lunList));

    /* Add lun to the device list */

    lstAdd(&pUsbTgtMscDev->lunList, &pUsbTgtMscLun->lunNode);
    pUsbTgtMscDev->nLuns ++;
    pUsbTgtMscLun->pUsbTgtMscDev = pUsbTgtMscDev;

    /* Set default lunState */

    pUsbTgtMscLun->lunState = USB_MSC_LUN_INIT;

    return pUsbTgtMscLun;
    }

/*******************************************************************************
*
* usbTgtMscLunDelete - delete the LUN's structure
*
* This routine deletes the LUN's structure.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbTgtMscLunDelete
    (
    pUSB_TGT_MSC_DEV pUsbTgtMscDev,
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    )
    {
    NODE *pNode;
    pUSB_TGT_MSC_LUN pNextUsbTgtMscLun = NULL;
    
    if ((pUsbTgtMscDev == NULL) || (pUsbTgtMscLun == NULL))
        return;

    if (pUsbTgtMscLun->pSenseData != NULL)
        OSS_FREE(pUsbTgtMscLun->pSenseData);

    if (pUsbTgtMscLun->pCapacityData != NULL)
        OSS_FREE(pUsbTgtMscLun->pCapacityData);
  
    /* Adjust lunIndex of other LUNs */
      
    pNode = lstFirst(&pUsbTgtMscDev->lunList);
    while(pNode != NULL)
        {
        pNextUsbTgtMscLun = LUN_NODE_TO_USB_TGT_MSC_LUN(pNode);
        if (pNextUsbTgtMscLun->lunIndex > pUsbTgtMscLun->lunIndex)
            pNextUsbTgtMscLun->lunIndex --;
        pNode = lstNext(pNode);
        }

    /* Remove the lun from the device's lun list */

    if (ERROR != lstFind(&pUsbTgtMscDev->lunList, &pUsbTgtMscLun->lunNode))
        {
        lstDelete(&pUsbTgtMscDev->lunList, &pUsbTgtMscLun->lunNode);
        pUsbTgtMscDev->nLuns --;
        }

     /* Reset all the field to 0 */

    memset(pUsbTgtMscLun, 0, sizeof(USB_TGT_MSC_LUN));
    OSS_FREE(pUsbTgtMscLun);
    return;
    }

/*******************************************************************************
*
* usbTgtMscLunListUpdate - update the lun list
*
* This routine updates the lun list.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbTgtMscLunListUpdate
    (
    pUSB_TGT_MSC_DEV pUsbTgtMscDev
    )
    {
    NODE *           pNode = NULL;
    NODE *           pNextNode = NULL;
    pUSB_TGT_MSC_LUN pUsbTgtMscLun = NULL;

    if (pUsbTgtMscDev == NULL)
        return ;

    pNode = lstFirst(&pUsbTgtMscDev->lunList);

    while (pNode != NULL)
        {
        pUsbTgtMscLun = LUN_NODE_TO_USB_TGT_MSC_LUN(pNode);
        pNextNode = lstNext(pNode);
        if (pUsbTgtMscLun != NULL)
            {
            if (pUsbTgtMscLun->lunState == USB_MSC_LUN_DELETE)
                usbTgtMscLunDelete(pUsbTgtMscDev, pUsbTgtMscLun);
            }
        pNode = pNextNode;
        }
    return ;
    }

/*******************************************************************************
*
* usbTgtMscDevNew - create structure for a new device
*
* This routine creates structure for a new device.
*
* RETURNS: pointer of the new device structure, or NULL if fail
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL pUSB_TGT_MSC_DEV usbTgtMscDevNew
    (
    void
    )
    {
    pUSB_TGT_MSC_DEV   pUsbTgtMscDev = NULL;

    pUsbTgtMscDev = OSS_CALLOC(sizeof(USB_TGT_MSC_DEV));
    if (pUsbTgtMscDev == NULL)
        return NULL;

    /* Init the member of the g_pUsbTargetMsDev */

    pUsbTgtMscDev->lunEventId = semBCreate(SEM_Q_PRIORITY, SEM_FULL);
    if (pUsbTgtMscDev->lunEventId == NULL)
        {
        usbTgtMscDevDelete(pUsbTgtMscDev);
        return NULL;
        }

    /* Malloc memmory for CBW */

    pUsbTgtMscDev->pCBW = OSS_CALLOC(sizeof(USB_MSC_CBW));
    if (pUsbTgtMscDev->pCBW == NULL)
        {
        usbTgtMscDevDelete(pUsbTgtMscDev);
        return NULL;
        }
    /* Malloc memmory for CSW */

    pUsbTgtMscDev->pCSW = OSS_CALLOC(sizeof(USB_MSC_CSW));
    if (pUsbTgtMscDev->pCBW == NULL)
       {
       usbTgtMscDevDelete(pUsbTgtMscDev);
       return NULL;
       }


    pUsbTgtMscDev->pBuf = OSS_CALLOC(MSC_DATA_BUF_LEN);
    if (pUsbTgtMscDev->pBuf == NULL)
       {
       usbTgtMscDevDelete(pUsbTgtMscDev);
       return NULL;
       }


    lstInit(&pUsbTgtMscDev->lunList);

    /* Add this device to the global list */

    semTake(g_usbTgtMscListEvent, WAIT_FOREVER);
    lstAdd(&g_usbTgtMscDevList, &pUsbTgtMscDev->devNode);
    semGive(g_usbTgtMscListEvent);

    return pUsbTgtMscDev;
    }

/*******************************************************************************
*
* usbTgtMscDevDelete - delete device's structure
*
* This routine deletes the device's structure.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbTgtMscDevDelete
    (
    pUSB_TGT_MSC_DEV pUsbTgtMscDev
    )
    {
    if (pUsbTgtMscDev == NULL)
        return;

    if (pUsbTgtMscDev->pCSW != NULL)
        OSS_FREE(pUsbTgtMscDev->pCSW);

    if (pUsbTgtMscDev->pCBW != NULL)
        OSS_FREE(pUsbTgtMscDev->pCBW);


    if (pUsbTgtMscDev->pBuf != NULL)
        OSS_FREE(pUsbTgtMscDev->pBuf);


    if (pUsbTgtMscDev->lunEventId != NULL)
        semDelete(pUsbTgtMscDev->lunEventId);

    /* Remove the device from the global list */

    semTake(g_usbTgtMscListEvent, WAIT_FOREVER);
    if (ERROR != lstFind(&g_usbTgtMscDevList, &pUsbTgtMscDev->devNode))
        lstDelete(&g_usbTgtMscDevList, &pUsbTgtMscDev->devNode);
    semGive(g_usbTgtMscListEvent);

    /* Reset all the field to 0 */

    memset(pUsbTgtMscDev, 0 , sizeof(USB_TGT_MSC_DEV));

    OSS_FREE(pUsbTgtMscDev);
    return ;
    }

/*******************************************************************************
*
* usbTgtMscAttachHandler - handle the attach event
*
* This routine handles the attach event.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbTgtMscAttachHandler
    (
    UINT16  Category,
    UINT16  Event,
    void *  eventData,
    void *  userData
    )
    {
    device_t             xbdDev;
    struct device *      dev;
    NODE *               pDevNode;
    NODE *               pLunNode;
    pUSB_TGT_MSC_LUN      pUsbTgtMscLun = NULL;
    pUSB_TGT_MSC_DEV      pUsbTgtMscDev = NULL;

    xbdDev = (device_t) ((ULONG)eventData);
    if (xbdDev == NULLDEV)
        return;

    dev = devMap (xbdDev);
    if (dev == NULL)
        return;

    pDevNode = lstFirst(&g_usbTgtMscDevList);

    while (pDevNode != NULL)
        {
        pUsbTgtMscDev = DEV_NODE_TO_USB_TGT_MSC_DEV(pDevNode);

        if (pUsbTgtMscDev != NULL)
            {
            pLunNode = lstFirst(&pUsbTgtMscDev->lunList);
            while (pLunNode != NULL)
                {
                pUsbTgtMscLun = LUN_NODE_TO_USB_TGT_MSC_LUN(pLunNode);

                if (pUsbTgtMscLun != NULL)
                    {
                    if (strcmp(dev->dv_xname, pUsbTgtMscLun->attachDevName)
                         == 0)
                        {
                        usbTgtMscLunAttach(pUsbTgtMscDev, pUsbTgtMscLun, xbdDev);
                        }
                    }

                pLunNode = lstNext(pLunNode);
                }
            }
        pDevNode = lstNext(pDevNode);
        }

    return;
    }

/*******************************************************************************
*
* usbTgtMscDetachHandler -handle the detach event
*
* This routine handles the detach event.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbTgtMscDetachHandler
    (
    UINT16  Category,
    UINT16  Event,
    void *  eventData,
    void *  userData
    )
    {
    device_t             xbdDev;
    NODE *               pDevNode;
    NODE *               pLunNode;
    pUSB_TGT_MSC_LUN      pUsbTgtMscLun = NULL;
    pUSB_TGT_MSC_DEV      pUsbTgtMscDev = NULL;

    xbdDev = (device_t)((ULONG) eventData);
    if (xbdDev == NULLDEV)
        return;

    pDevNode = lstFirst(&g_usbTgtMscDevList);

    while (pDevNode != NULL)
        {
        pUsbTgtMscDev = DEV_NODE_TO_USB_TGT_MSC_DEV(pDevNode);

        if (pUsbTgtMscDev != NULL)
            {
            pLunNode = lstFirst(&pUsbTgtMscDev->lunList);
            while (pLunNode != NULL)
                {
                pUsbTgtMscLun = LUN_NODE_TO_USB_TGT_MSC_LUN(pLunNode);

                if (pUsbTgtMscLun != NULL)
                    {
                    if (xbdDev == pUsbTgtMscLun->devXbdHandle)
                        {
                        usbTgtMscLunDetach (pUsbTgtMscDev, pUsbTgtMscLun);
                        }
                    }

                pLunNode = lstNext(pLunNode);
                }
            }
        pDevNode = lstNext(pDevNode);
        }

    return;
    }

/*******************************************************************************
*
* usbTgtMscLunAttach - attach LUN to device
*
* This routine attaches LUN to device.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbTgtMscLunAttach
    (
    pUSB_TGT_MSC_DEV pUsbTgtMscDev,
    pUSB_TGT_MSC_LUN pUsbTgtMscLun,
    device_t         devHandle
    )
    {
    unsigned         blockSize = 0;
    sector_t         nBlocks = 0;

    if ((pUsbTgtMscDev == NULL) || (pUsbTgtMscLun == NULL) ||
        (devHandle == NULLDEV))
        return;

    USBTGT_MSC_DBG("usbTgtMscLunAttach - nLuns %d, lunIndex %d\n",
                   pUsbTgtMscDev->nLuns, pUsbTgtMscLun->lunIndex,
                   3, 4, 5, 6);

    if (pUsbTgtMscLun->lunState < USB_MSC_LUN_ATTACH)
        {
        pUsbTgtMscLun->devXbdHandle = devHandle;

        if (OK == xbdNBlocks(devHandle, &nBlocks))
            pUsbTgtMscLun->nBlocks = nBlocks;

        if (OK == xbdBlockSize(devHandle, &blockSize))
            pUsbTgtMscLun->perBlockSize = blockSize;

        usbTgtMscSenseDataInit(pUsbTgtMscLun->pSenseData);
        usbTgtMscCapacityDataForm(pUsbTgtMscLun->pCapacityData,
                                  blockSize,
                                  nBlocks);

        pUsbTgtMscLun->mediaReady     = TRUE;         /* TRUE - if media ready */
        pUsbTgtMscLun->mediaRemoved   = FALSE;        /* TRUE - if media removed */
        pUsbTgtMscLun->mediaPrevent   = 0;
        pUsbTgtMscLun->pwrConditions  = 0;

        usbTgtMscDeviceParamListInit(pUsbTgtMscLun);
        usbTgtMscDeviceParamListMaskInit(pUsbTgtMscLun);
        usbTgtMscDeviceParamList10Init(pUsbTgtMscLun);
        usbTgtMscDeviceParamListMask10Init(pUsbTgtMscLun);

        /* The removable media has been changed */

        pUsbTgtMscLun->mediaChanged = TRUE;

        pUsbTgtMscLun->lunState = USB_MSC_LUN_ATTACH;
        }

    return;
    }

/*******************************************************************************
*
* usbTgtMscLunDetach - detach LUN from device
*
* This routine detaches LUN from device.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbTgtMscLunDetach
    (
    pUSB_TGT_MSC_DEV pUsbTgtMscDev,
    pUSB_TGT_MSC_LUN pUsbTgtMscLun
    )
    {
    if ((pUsbTgtMscDev == NULL) || (pUsbTgtMscLun == NULL))
        return;

    if (pUsbTgtMscLun->lunState != USB_MSC_LUN_DETACH)
        {
        pUsbTgtMscLun->devXbdHandle = NULLDEV;
        pUsbTgtMscLun->nBlocks = 0;
        pUsbTgtMscLun->perBlockSize = 0;

        usbTgtMscCapacityDataForm(pUsbTgtMscLun->pCapacityData,
                                  pUsbTgtMscLun->perBlockSize,
                                  pUsbTgtMscLun->nBlocks);

        pUsbTgtMscLun->mediaReady = FALSE;
        pUsbTgtMscLun->mediaRemoved = TRUE;

        /* The removable media has been changed */

        pUsbTgtMscLun->mediaChanged = TRUE;

        pUsbTgtMscLun->lunState = USB_MSC_LUN_DETACH;
        }

    return;
    }

/*******************************************************************************
*
* usbTgtMscLunAdd - add new LUN to TCD
*
* This routine addes a new LUN to TCD.
*
* RETURNS: OK, or ERROR if something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbTgtMscLunAdd
    (
    char *     tcdName,
    int        tcdUnit,
    char *     devName,
    char *     devReadonly,
    char *     devRemovable,
    char *     devProtocol
    )
    {
    device_t            xbdDev = NULLDEV;
    pUSB_TGT_MSC_DEV    pUsbTgtMscDev = NULL;
    pUSB_TGT_MSC_LUN    pUsbTgtMscLun = NULL;

    if ((devName == NULL) ||
        (tcdName == NULL) ||
        (devReadonly == NULL) ||
        (devRemovable == NULL) ||
        (devProtocol == NULL))
        {
        USBTGT_MSC_ERR("Invalid parameters ", 1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    USBTGT_MSC_DBG("usbTgtMscLunAdd tcdName %s, tcdUnit %d"
                   "devName %s, devReadonly %s, devRemovable %s,devProtocol %s",
                   tcdName, tcdUnit, devName,
                   devReadonly, devRemovable, devProtocol);

    /* Find the USB target MSC device structure by tcdName and tcdUnit */

    pUsbTgtMscDev = usbTgtMscDevFind(tcdName, tcdUnit);

    if (pUsbTgtMscDev == NULL)
        {
        USBTGT_MSC_ERR("Can't find a device structure with name %s unit %d ",
                       tcdName, tcdUnit, 3, 4, 5, 6);
        return ERROR;
        }

    /* Find the LUN structure by the devName */

    pUsbTgtMscLun = usbTgtMscLunGetByName(pUsbTgtMscDev, devName);

    if (pUsbTgtMscLun != NULL)
        {
        USBTGT_MSC_ERR("The LUN with name %s already exist",
                       devName, 2, 3, 4, 5, 6);
        return ERROR;
        }

    /* Create a new Lun */

    pUsbTgtMscLun = usbTgtMscLunNew(pUsbTgtMscDev);

    if (pUsbTgtMscLun == NULL)
        {
        USBTGT_MSC_ERR("Create LUN structure fail\n",
                       1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    /* update the lun's information */

    strncpy(pUsbTgtMscLun->attachDevName, devName, (USBTGT_COMMON_STR_LEN - 1));

    if (strcmp(devReadonly, "y") == 0)
        pUsbTgtMscLun->bReadOnly = TRUE;
    else
        pUsbTgtMscLun->bReadOnly = FALSE;

    if (strcmp(devRemovable, "y") == 0)
        pUsbTgtMscLun->bRemovable = TRUE;
    else
        pUsbTgtMscLun->bRemovable = FALSE;

    xbdDev = usbTgtMscXBDPartHandleGet(pUsbTgtMscLun->attachDevName);
    if (xbdDev != NULLDEV)
        usbTgtMscLunAttach(pUsbTgtMscDev, pUsbTgtMscLun, xbdDev);
    else
        {
        /*
         * Even the attached device is not ready, we also add nLuns
         */

        pUsbTgtMscLun->mediaReady     = FALSE;
        pUsbTgtMscLun->mediaRemoved   = TRUE;
        }
    /*
     * Although we add the lun, the usb host get the new lun
     * after the next emulation stage. As the "Get Max Lun" command
     * will execute at the init process of the emulation stage
     */

    return OK;
    }

/*******************************************************************************
*
* usbTgtMscLunDel - delete an LUN from the USB target MSC device
*
* This routine deletes an LUN from the USB target MSC device.
*
* RETURNS: OK, or ERROR if something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbTgtMscLunDel
    (
    char *     tcdName,
    int        tcdUnit,
    char *     devName
    )
    {
    device_t            xbdDev = NULLDEV;
    pUSB_TGT_MSC_DEV    pUsbTgtMscDev = NULL;
    pUSB_TGT_MSC_LUN    pUsbTgtMscLun = NULL;

    if ((tcdName == NULL) || (devName == NULL))
        {
        USBTGT_MSC_ERR("Invalid parameters ", 1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    /* Find the USB target MSC device structure by tcdName and tcdUnit */

    pUsbTgtMscDev = usbTgtMscDevFind(tcdName, tcdUnit);

    if (pUsbTgtMscDev == NULL)
        {
        USBTGT_MSC_ERR("Can't find a device structure with name %s unit %d ",
                       tcdName, tcdUnit, 3, 4, 5, 6);
        return ERROR;
        }

    /* Find the LUN structure by the devName */

    pUsbTgtMscLun = usbTgtMscLunGetByName(pUsbTgtMscDev, devName);

    if (pUsbTgtMscLun == NULL)
        {
        USBTGT_MSC_ERR("Can't find a LUN with name %s ",
                       devName, 2, 3, 4, 5, 6);
        return ERROR;
        }

    /*
     * First we will detach the lun, so  any access of this lun will be
     * rejected, Then we set the lunState as delete
     * In next emulation stage we will remove this lun from the list and
     * free the structure
     */

    usbTgtMscLunDetach(pUsbTgtMscDev, pUsbTgtMscLun);
    pUsbTgtMscLun->lunState = USB_MSC_LUN_DELETE;

    return OK;
    }


/*******************************************************************************
*
* usbTgtMscShow - show the information for devices and LUNs
*
* This routine shows the information for devices and LUNs.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbTgtMscShow(void)
    {
    NODE *             pDevNode = NULL;
    NODE *             pLunNode = NULL;
    NODE *             pErfNode = NULL;
    pUSB_TGT_MSC_DEV   pUsbTgtMscDev = NULL;
    pUSB_TGT_MSC_LUN   pUsbTgtMscLun = NULL;

    if (lstCount(&g_usbTgtMscDevList) == 0)
        return;

    pDevNode = lstFirst(&g_usbTgtMscDevList);
    while (pDevNode != NULL)
        {
        pUsbTgtMscDev = DEV_NODE_TO_USB_TGT_MSC_DEV(pDevNode);
        if (pUsbTgtMscDev != NULL)
            {
            printf("\n pUsbTgtMscDev %p, nLuns %d, tcdName %s,config %d\n\n",
                    pUsbTgtMscDev,
                    pUsbTgtMscDev->nLuns,
                    pUsbTgtMscDev->tcdName,
                    pUsbTgtMscDev->uCurConfig);

            pLunNode = lstFirst(&pUsbTgtMscDev->lunList);
            while (pLunNode != NULL)
                {
                pUsbTgtMscLun = LUN_NODE_TO_USB_TGT_MSC_LUN(pLunNode);
                if (pUsbTgtMscLun != NULL)
                    {
                    printf("pUsbTgtMscLun %p, lunIndex %d, devXbdHandle 0x%x, "
                           "nBlocks = 0x%lX, perBlockSize = 0x%lX\n",
                           pUsbTgtMscLun,
                           pUsbTgtMscLun->lunIndex,
                           pUsbTgtMscLun->devXbdHandle,
                           (ULONG)pUsbTgtMscLun->nBlocks,
                           (ULONG)pUsbTgtMscLun->perBlockSize);

                    printf("attachDevName %s, ReadOnly %d, Removable %d\n",
                           pUsbTgtMscLun->attachDevName,
                           pUsbTgtMscLun->bReadOnly,
                           pUsbTgtMscLun->bRemovable);

                    if (pUsbTgtMscLun->mediaReady)
                        printf("media: ready, ");
                    else
                        printf("media: not ready, ");
                    if (pUsbTgtMscLun->mediaRemoved)
                        printf("removed\n\n");
                    else
                        printf("not removed\n\n");

                    }
                pLunNode = lstNext(pLunNode);
                }
            }
        pDevNode = lstNext(pDevNode);
        }
    return;
    }

/*******************************************************************************
*
* usbTgtMscConfigShow - show user configuration for the MSC function driver
*
* This routine shows user configutaion for the MSC function driver
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbTgtMscConfigShow()
    {
    int i = 0;

    printf("Msc Function have %d configurations\n\n", 
           g_usbConfigCount);
    
    for (i = 0; i < g_usbConfigCount; i++)
        {
        printf("usrUsbTgtMscConfigTable[%d].devName: %s \n", 
               i, usrUsbTgtMscConfigTable[i].devName);
        printf("usrUsbTgtMscConfigTable[%d].devProtocol: %s \n",
               i, usrUsbTgtMscConfigTable[i].devProtocol);

        if (strcmp(usrUsbTgtMscConfigTable[i].devProtocol, "RBC") != 0)
            printf("The protocol field is wrong, must be RBC only\n");
        
        printf("usrUsbTgtMscConfigTable[%d].devReadonly: %s \n",
               i, usrUsbTgtMscConfigTable[i].devReadonly);
        printf("usrUsbTgtMscConfigTable[%d].devRemovable: %s \n",
               i, usrUsbTgtMscConfigTable[i].devRemovable);
        printf("usrUsbTgtMscConfigTable[%d].tcdName: %s \n",
               i, usrUsbTgtMscConfigTable[i].tcdName);
        printf("usrUsbTgtMscConfigTable[%d].tcdUnit: %d \n",
               i, usrUsbTgtMscConfigTable[i].tcdUnit);
        printf("usrUsbTgtMscConfigTable[%d].funcName: %s \n",
               i, usrUsbTgtMscConfigTable[i].funcName);
        printf("usrUsbTgtMscConfigTable[%d].funcUnit: %d \n",
               i, usrUsbTgtMscConfigTable[i].funcUnit);
        printf("\n\n");
        }
    }

