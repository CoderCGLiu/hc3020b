/* usbTgtRndis.c - USB Remote NDIS Function Class Driver Module */

/*
 * Copyright (c) 2011-2013, 2015 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
Modification history
--------------------
01o,30jan15,lan  Add WCID support (VXW6-83565)
01k,04jan13,s_z  Remove compiler warning (WIND00390357)
01j,18may12,s_z  Add support for USB 3.0 target (WIND00326012) 
01i,15may12,s_z  Correct debugging display issue (WIND00348017)
01h,13dec11,m_y  Modify according to code check result (WIND00319317)
01g,29sep11,s_z  Add direct callback mode to avoid asynch issue (WIND00306920)
01f,28apr11,s_z  Update the module description
01e,11apr11,s_z  Add Open Specifications Documentation description
01d,22mar11,s_z  Code clean up based on the code review
01c,09mar11,s_z  Code clean up
01b,23feb11,s_z  Add device descriptor for target stack using
01a,04jan11,s_z  written
*/

/*
DESCRIPTION

 This module is the Remote NDIS (RNDIS) function class driver based on the GEN2
 USB target stack APIs.
 
 This RNDIS function driver follows the [MS-RNDIS]:Remote Network Driver 
 Interface Specification (RNDIS) Protocol Specification 1.0, which is one of the
 Open Specifications Documentation.

 Following is the Intellectual Property Rights Notice for Open Specifications 
 Documentation:
 
\h Intellectual Property Rights Notice for Open Specifications Documentation

\h Technical Documentation. 

 Microsoft publishes Open Specifications documentation for protocols, 
 file formats, languages, standards as well as overviews of the interaction 
 among each of these technologies.

\h Copyrights. 

 This documentation is covered by Microsoft copyrights. Regardless of any other 
 terms that are contained in the terms of use for the Microsoft website that
 hosts this documentation, you may make copies of it in order to develop 
 implementations of the technologies described in the Open Specifications 
 and may distribute portions of it in your implementations using these 
 technologies or your documentation as necessary to properly document the 
 implementation. You may also distribute in your implementation, with or without 
 modification, any schema, IDL's, or code samples that are included in the 
 documentation. This permission also applies to any documents that are 
 referenced in the Open Specifications.

\h No Trade Secrets.

 Microsoft does not claim any trade secret rights in this documentation.

\h Patents.

 Microsoft has patents that may cover your implementations of the technologies 
 described in the Open Specifications. Neither this notice nor Microsoft's 
 delivery of the documentation grants any licenses under those or any other 
 Microsoft patents. However, a given Open Specification may be covered by 
 Microsoft's Open Specification Promise (available here: 
 http://www.microsoft.com/interop/osp) or the Community Promise 
 (available here: http://www.microsoft.com/interop/cp/default.mspx).
 If you would prefer a written license, or if the technologies described in 
 the Open Specifications are not covered by the Open Specifications Promise 
 or Community Promise, as applicable, patent licenses are available by 
 contacting iplg@microsoft.com.

\h Trademarks. 

 The names of companies and products contained in this 
 documentation may be covered by trademarks or similar intellectual 
 property rights. This notice does not grant any licenses under those rights.

\h Fictitious Names.

 The example companies, organizations, products, 
 domain names, e-mail addresses, logos, people, places, and events depicted in 
 this documentation are fictitious. No association with any real company, 
 organization, product, domain name, email address, logo, person, place, or 
 event is intended or should be inferred.

\h Reservation of Rights. 

 All other rights are reserved, and this notice does 
 not grant any rights other than specifically described above, whether by 
 implication, estoppel, or otherwise.

\h Tools. 

 The Open Specifications do not require the use of Microsoft 
 programming tools or programming environments in order for you to develop an 
 implementation. If you have access to Microsoft programming tools and 
 environments you are free to take advantage of them. Certain Open 
 Specifications are intended for use in conjunction with publicly available 
 standard specifications and network programming art, and assumes that the 
 reader either is familiar with the aforementioned material or has immediate 
 access to it.

ARCHITECTURE

Follow is the design of the USB target network function driver architecture.

\cs
                   +-----------------------------------------+ 
                   |             User Application            | 
                   |            (ioLib,Network,Fs...)        | 
                   +--------------^----------@---------------+ 
                                  |          |                 
                                  |          |                  
                   +--------------@----------v---------------+
                   |                   TFD                   |
                   |  USB Target Network Function Driver(s)  |
                   |                                         |
+---------------+  |  +-----------------------------------+  |  +---------------+
|USB Virtual END<--+--@   END Medium Agent Module         @--+-->NIC END Driver |
|Driver         |  |  |      +--------+    +-------+      |  |  |(sme0,fei0...) |
|(usbEnd0)      @--+-->      |Control |    |Data   |      <--+--@               |
+---------------+  |  |      |Flow    |    |Flow   |      |  |  +---------------+
                   |  |      |Control |    |Control|      |  |
                   |  |      +--------+    +-------+      |  |
                   |  +-----------^----------@------------+  |
                   |              |          |               |      
                   |  +-----------@----------v------------+  |
                   |  |   Network Binder Agent Module     |  |      
                   |  |+--------++--------------++-------+|  |      
                   |  ||Control ||Registration &||Data   ||  |      
                   |  ||Flow    ||Binder Buffer ||Flow   ||  |      
                   |  ||Control ||Management    ||Control||  |      
                   |  |+--------++--------------++-------+|  |      
                   |  +-----------^----------@------------+  |      
                   |              |          |               |      
                   |  +-----------@----------v------------+  |      
                   |  |     USB RNDIS Transport Module    |  |
                   |  |  +--------+ +----------+ +------+ |  |    
                   |  |  |Control | |Hardware  | |Data  | |  |    
                   |  |  |Requests| |Events    | |Tx/Rx | |  |    
                   |  |  |Callback| |Callback  | |Unwrap| |  |    
                   |  |  |        | |(Reset...)| |wrap  | |  |    
                   |  |  +--------+ +----------+ +------+ |  |    
                   |  +---------- ------------------------+  |    
                   +--------------^----------@---------------+       
                                  |          |
                                  |          |
                   +--------------@----------v---------------+
                   |                   TML                   |
                   |       USB Target Management Layer       |
                   |                                         |
                   |+--------++----------++-----++----------+|
                   ||Control ||Hardware  ||Data ||Registra- ||
                   ||Requests||Events    ||Tx/Rx||tion      ||
                   ||        ||(Reset...)||     ||Management||
                   |+--------++----------++-----++----------+|
                   +-----------------------------------------+  

\ce

\is
\i TML
   The target management level, which is the core of the GEN2 target stack 
   who used to manage all the function drivers and the target controller 
   drivers. Please refer to the usbTgt.c for more detail.
\i USB RNDIS Transport Module
   This module works with the USB target stack TML module, realizes the 
   specific USB protocol command process interface, carries all the network 
   packets which has been wrapped by the USB data packet. It help the network 
   medium device communicate the USB host by USB protocol. And this file
   is the realization of the module.
\i Network Binder Agent Module
   This module is used to binder the USB transport module with the END 
   Medium device. The developer need register the dataToMedium callback,
   dataToLowDev callback, mediumIoctl callback and lowDevIoctl callback 
   routines to this module. If the USB transport module got some requests 
   or the data related to the Medium, the right callback can be called 
   to process by the medium, and vice versa. 
\i END Medium Agent Module
   This module will be bind to the Network binder Agent Module, and as an 
   agent to communicate with the END device. This module used to process 
   the medium related control and data requests. It is the interface of the 
   hardware driver. In this design, it is the interface to the virtual END 
   driver and the hardware END driver.
\i NIC END Driver Module
   This module is the driver for the real hardware such as one Ethernet 
   network controller. All the data and command from the USB will route to 
   this module by the Ioctl/Data callback bridge interface which registered
   to the END Medium Agent Module. 
\i USB Virtual END Driver Module
   This module is the virtual USB device driver. It emulates one device 
   such as one Ethernet device. All the data and command from the USB will 
   route to this module by the Ioctl/Data callback bridge interface which 
   registered to the END Medium Agent Module.
\i User Application
   This is the user application may needed by the TFL. Different function 
   driver may use different applications. Such as the network stack for the 
   RNDIS and the FS framework for the mass storage function driver. This is
   totally depend on the function driver and the user's use case.
\ie


SUPPORTED MEDIUM TYPE

In this design, we only support the NdisMedium802_3 medium type by 
<'usbTgtMediumEnd'> END medium agent. The developer need add the right medium 
agents to supported other kinds of medium type.

INCLUDE FILES:  endLib.h, usb/usbCdc.h, usb/usbTgtRndis.h, 
                usbTgtNetMediumBinder.h, usbTgtRndisDebug.c
                
SEE ALSO:

\tb [MS-RNDIS]: Remote Network Driver Interface Specification (RNDIS) Protocol Specification http://msdn.microsoft.com/en-us/library/ee524902(v=PROT.10).aspx

*/

/* includes */

#include <endLib.h>
#include <usb/usbCdc.h>
#include <usb/usbTgtRndis.h>
#include <usbTgtNetMediumBinder.h>
#include <usbTgtRndisDebug.c>

/* defines */

#define RNDIS_COMM_INTF_NUM         0
#define RNDIS_DATA_INTF_NUM         1
#define RNDIS_INTF_ALT_SETTING      0     /* Alternate setting */


#define RNDIS_INT_EP_PKT_SIZE       0x08  /* Interrupt endpoint max packet size */
#define RNDIS_BULK_EP_PKT_SIZE_64   0x40  /* Bulk endpoint max packet size */
#define RNDIS_BULK_EP_PKT_SIZE_512  0x200 /* Bulk endpoint max packet size */


#define RNDIS_INT_IN_EP_ADDR        0x81
#define RNDIS_BULK_IN_EP_ADDR       0x82
#define RNDIS_BULK_OUT_EP_ADDR      0x02

#define USB_RNDIS_WCID_STRING       "RNDIS"
#define USB_RNDIS_WCID_SUB_STRING   "5162001"
#define USB_RNDIS_WCID_VC           0x1



/* globals */

IMPORT int usrUsbTgtRndisCountGet(void);
IMPORT char * usrUsbTgtRndisMediumNameGet(int index);
IMPORT UINT8 usrUsbTgtRndisMediumUnitGet(int index);
IMPORT UINT8 usrUsbTgtRndisMediumTypeGet(int index);
IMPORT char * usrUsbTgtRndisNameGet(int index);
IMPORT char * usrUsbTgtRndisTcdNameGet(int index);
IMPORT UINT8 usrUsbTgtRndisUnitGet(int index);
IMPORT UINT8 usrUsbTgtRndisTcdUnitGet(int index);
IMPORT UINT8 usrUsbTgtRndisConfigNumGet(int index);
IMPORT void usrUsbTgtRndisAttachCallback(void);
IMPORT void usrUsbTgtRndisDetachCallback(void);
IMPORT STATUS usbTgtControlPipeStall(pUSBTGT_TCD pTcd);
IMPORT int usrUsbTgtNetworkDataPoolSizeGet(void);
IMPORT int usrUsbTgtNetworkDataPoolCountGet(void);
IMPORT int usrUsbTgtRndisTaskPriorityGet(int index);
IMPORT STATUS usbTgtRndisBulkInErpSubmit
    (
    pUSBTGT_RNDIS    pUsbTgtRndis
    );
IMPORT STATUS usbTgtRndisBulkOutErpSubmit
    (
    pUSBTGT_RNDIS    pUsbTgtRndis
    );

/* Global callback table */

IMPORT USB_TARG_CALLBACK_TABLE gUsbTgtRndisCallbackTable;

/* locals */

/* The default device descriptor */

LOCAL USB_DEVICE_DESCR gUsbTgtRndisDevDescr =
    {
    USB_DEVICE_DESCR_LEN,                   /* bLength */
    USB_DESCR_DEVICE,                       /* bDescriptorType */
    TO_LITTLEW (USBTGT_VERSION_SUPPORTED),  /* bcdUsb */
    2,                                      /* bDeviceClass */
    0,                                      /* bDeviceSubclass */
    0,                                      /* bDeviceProtocol */
    USB_MAX_CTRL_PACKET_SIZE,               /* maxPacketSize0 */
    0,                                      /* idVendor */
    0,                                      /* idProduct */
    0,                                      /* bcdDevice */
    0,                                      /* iManufacturer */
    0,                                      /* iProduct */
    0,                                      /* iSerialNumber */
    USBTGT_MIN_CONFIG_NUM                   /* bNumConfigurations */
    };

/* The communication interface */

LOCAL USB_INTERFACE_DESCR gUsbTgtRndisCommIfDescr =
    {
    USB_INTERFACE_DESCR_LEN,           /* bLength */
    USB_DESCR_INTERFACE,               /* bDescriptorType */
    RNDIS_COMM_INTF_NUM,               /* bInterfaceNumber */
    RNDIS_INTF_ALT_SETTING,            /* bAlternateSetting */
    1,                                 /* bNumEndpoints */
    USB_CLASS_COMM,                    /* bInterfaceClass */
    USB_CDC_SUBCLASS_ACM,              /* bInterfaceSubClass */
    USB_CDC_ACM_PROTO_VENDOR,          /* bInterfaceProtocol */
    0                                  /* iInterface */
    };

/* The communication endpoint for the communication interface */

LOCAL USB_ENDPOINT_DESCR gUsbTgtRndisCommEpDescr =
    {
    USB_ENDPOINT_DESCR_LEN,            /* bLength */
    USB_DESCR_ENDPOINT,                /* bDescriptorType */
    RNDIS_INT_IN_EP_ADDR,              /* bEndpointAddress */
    USB_ATTR_INTERRUPT,                /* bmAttributes */
    TO_LITTLEW(RNDIS_INT_EP_PKT_SIZE), /* maxPacketSize */
    0x1                                /* bInterval */
    };

LOCAL USB_ENDPOINT_COMPANION_DESCR gUsbTgtRndisCommEpCompDescr = /* companion descriptor */
    {
    USB_ENDPOINT_COMP_DESCR_LEN,                 /* bLength */
    USB_DESCR_SS_ENDPOINT_COMPANION,             /* bDescriptorType */
    0x0,                                         /* bMaxBurst */
    0x0,                                         /* bmAttributes */
    0x0                                          /* wBytesPerInterval */
    };

/* The data interface */

LOCAL USB_INTERFACE_DESCR gUsbTgtRndisDataIfDescr =
    {
    USB_INTERFACE_DESCR_LEN,           /* bLength */
    USB_DESCR_INTERFACE,               /* bDescriptorType */
    RNDIS_DATA_INTF_NUM,               /* bInterfaceNumber */
    RNDIS_INTF_ALT_SETTING,            /* bAlternateSetting */
    2,                                 /* bNumEndpoints */
    USB_CLASS_CDC_DATA,                /* bInterfaceClass */
    0,                                 /* bInterfaceSubClass */
    0,                                 /* bInterfaceProtocol */
    0                                  /* iInterface */
    };

/* The data endpoints of the data interface with full speed */

LOCAL USB_ENDPOINT_DESCR gUsbTgtRndisDataInEpDescr =
    {
    USB_ENDPOINT_DESCR_LEN,            /* bLength */
    USB_DESCR_ENDPOINT,                /* bDescriptorType */
    RNDIS_BULK_IN_EP_ADDR,             /* bEndpointAddress */
    USB_ATTR_BULK,                     /* bmAttributes */
    TO_LITTLEW(RNDIS_BULK_EP_PKT_SIZE_64),/* maxPacketSize */
    0                                  /* bInterval */
    };

LOCAL USB_ENDPOINT_DESCR gUsbTgtRndisDataOutEpDescr =
    {
    USB_ENDPOINT_DESCR_LEN,            /* bLength */
    USB_DESCR_ENDPOINT,                /* bDescriptorType */
    RNDIS_BULK_OUT_EP_ADDR,            /* bEndpointAddress */
    USB_ATTR_BULK,                     /* bmAttributes */
    TO_LITTLEW(RNDIS_BULK_EP_PKT_SIZE_64),/* maxPacketSize */
    0                                  /* bInterval */
    };

/* The data endpoints of the data interface with high speed */

LOCAL USB_ENDPOINT_DESCR gUsbTgtRndisHsDataInEpDescr =
    {
    USB_ENDPOINT_DESCR_LEN,            /* bLength */
    USB_DESCR_ENDPOINT,                /* bDescriptorType */
    RNDIS_BULK_IN_EP_ADDR,             /* bEndpointAddress */
    USB_ATTR_BULK,                     /* bmAttributes */
    TO_LITTLEW(RNDIS_BULK_EP_PKT_SIZE_512),/* maxPacketSize */
    0                                  /* bInterval */
    };

LOCAL USB_ENDPOINT_DESCR gUsbTgtRndisHsDataOutEpDescr =
    {
    USB_ENDPOINT_DESCR_LEN,            /* bLength */
    USB_DESCR_ENDPOINT,                /* bDescriptorType */
    RNDIS_BULK_OUT_EP_ADDR,            /* bEndpointAddress */
    USB_ATTR_BULK,                     /* bmAttributes */
    TO_LITTLEW(RNDIS_BULK_EP_PKT_SIZE_512),/* maxPacketSize */
    0                                  /* bInterval */
    };

/* The data endpoints of the data interface with full speed */

LOCAL USB_ENDPOINT_DESCR gUsbTgtRndisSsDataInEpDescr =
    {
    USB_ENDPOINT_DESCR_LEN,            /* bLength */
    USB_DESCR_ENDPOINT,                /* bDescriptorType */
    RNDIS_BULK_IN_EP_ADDR,             /* bEndpointAddress */
    USB_ATTR_BULK,                     /* bmAttributes */
    TO_LITTLEW(1024),                  /* maxPacketSize */
    0                                  /* bInterval */
    };

LOCAL USB_ENDPOINT_COMPANION_DESCR gUsbTgtRndisSsDataInEpCompDescr = /* companion descriptor */
    {
    USB_ENDPOINT_COMP_DESCR_LEN,                 /* bLength */
    USB_DESCR_SS_ENDPOINT_COMPANION,             /* bDescriptorType */
    0x0,                                         /* bMaxBurst */
    0x0,                                         /* bmAttributes */
    0x0                                          /* wBytesPerInterval */
    };

LOCAL USB_ENDPOINT_DESCR gUsbTgtRndisSsDataOutEpDescr =
    {
    USB_ENDPOINT_DESCR_LEN,            /* bLength */
    USB_DESCR_ENDPOINT,                /* bDescriptorType */
    RNDIS_BULK_OUT_EP_ADDR,            /* bEndpointAddress */
    USB_ATTR_BULK,                     /* bmAttributes */
    TO_LITTLEW(1024),                  /* maxPacketSize */
    0                                  /* bInterval */
    };

LOCAL USB_ENDPOINT_COMPANION_DESCR gUsbTgtRndisSsDataOutEpCompDescr = /* companion descriptor */
    {
    USB_ENDPOINT_COMP_DESCR_LEN,                 /* bLength */
    USB_DESCR_SS_ENDPOINT_COMPANION,             /* bDescriptorType */
    0x0,                                         /* bMaxBurst */
    0x0,                                         /* bmAttributes */
    0x0                                          /* wBytesPerInterval */
    };

/* The global list to record all the RNDIS device */

LIST  gUsbTgtRndisList;
LOCAL BOOL  gUsbTgtRndisInited = FALSE;

LOCAL pUSB_DESCR_HDR gUsbTgtRndisDescHdr[] =
    {
    (pUSB_DESCR_HDR )&gUsbTgtRndisDevDescr,          /* Function device descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtRndisCommIfDescr,       /* Communication Interface descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtRndisCommEpDescr,    /* Interrupt Endpoint descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtRndisDataIfDescr,       /* Data Interface descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtRndisDataInEpDescr,  /* Data IN Endpoint descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtRndisDataOutEpDescr, /* Data OUT Endpoint descriptor */
    NULL                                             /* End of descriptors, must have */
    };

LOCAL pUSB_DESCR_HDR gHsUsbTgtRndisDescHdr[] =
    {
    (pUSB_DESCR_HDR )&gUsbTgtRndisDevDescr,           /* Function device descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtRndisCommIfDescr,        /* Communication Interface descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtRndisCommEpDescr,     /* Interrupt Endpoint descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtRndisDataIfDescr,        /* Data Interface descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtRndisHsDataInEpDescr, /* Data IN Endpoint descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtRndisHsDataOutEpDescr,/* Data OUT Endpoint descriptor */
    NULL                                              /* End of descriptors, must have */
    };
LOCAL pUSB_DESCR_HDR gSsUsbTgtRndisDescHdr[] =
    {
    (pUSB_DESCR_HDR )&gUsbTgtRndisDevDescr,        /* Function device descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtRndisCommIfDescr,     /* Communication Interface descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtRndisCommEpDescr,     /* Interrupt Endpoint descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtRndisCommEpCompDescr, /* Interrupt Ep comp desc */
    (pUSB_DESCR_HDR )&gUsbTgtRndisDataIfDescr,     /* Data Interface descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtRndisSsDataInEpDescr, /* Data IN Endpoint descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtRndisSsDataInEpCompDescr,
    (pUSB_DESCR_HDR )&gUsbTgtRndisSsDataOutEpDescr,/* Data OUT Endpoint descriptor */
    (pUSB_DESCR_HDR )&gUsbTgtRndisSsDataOutEpCompDescr,
    NULL                                              /* End of descriptors, must have */
    };



/***************************************************************************
*
* usbTgtNetNotifyHandler - handle the network related notifications
*
* This routine is used to process all the network related notifications which 
* issued by the transport layer or the medium layer.
*
* NOTE: The caller should check the validity of the parameter, such as:
*       (NULL == pBinder) ?
*       (NULL == pNetNotifyInfo) ?
*       (NULL == pNetNotifyInfo->pBinder) ?
*       (pBinder != pNetNotifyInfo->pBinder) ?
*       (USBTGT_RNDIS_NOTIFY_UNKNOWN == pNetNotifyInfo->NotifyCode) ?
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbTgtRndisNotifyHandler
    (
    pUSBTGT_RNDIS             pUsbTgtRndis,
    pUSBTGT_RNDIS_NOTIFY_INFO pRndisNotifyInfo
    )
    {
    /* The caller should check the validity of the parameter */

    switch (pRndisNotifyInfo->NotifyCode)
        {
        /* Transfer the data to the medium */
        
        case USBTGT_RNDIS_NOTIFY_XMIT_DATA_TO_MEDIUM:
            {
            /* Get one pkt of data, which need to xmit to the medium */

            if (pUsbTgtRndis->pBinder->dataToMedium)
                {
                (pUsbTgtRndis->pBinder->dataToMedium)
                    (pUsbTgtRndis->pBinder->pMediumDev);
                }
            }
            break;
        case USBTGT_RNDIS_NOTIFY_REQUIRE_DATA_OUT:
            {
            USBTGT_RNDIS_VDBG ("USBTGT_RNDIS_NOTIFY_REQUIRE_DATA_OUT\n",
                               1, 2, 3, 4, 5, 6);

            if (FALSE == pUsbTgtRndis->bBulkOutErpUsed)
                {
                /* Re submit the bulk out transaction */

                if ( ERROR == usbTgtRndisBulkOutErpSubmit(pUsbTgtRndis))
                    {
                    USBTGT_RNDIS_DBG("Re-submit bulkOutErp fail\n",
                                    1, 2, 3, 4, 5, 6);
                    }
                }
            else
                {
                USBTGT_RNDIS_DBG("pUsbTgtRndis->bBulkOutErpUsed %p \n", 
                                 pUsbTgtRndis->bBulkOutErpUsed, 2, 3,4,5,6);
                }
             }
             break;

        /* Transfer the data to the transport */
        
        case USBTGT_RNDIS_NOTIFY_RCV_DATA_FROM_MEDIUM:
            {
            /* Get one pkt of data, which need to xmit to the medium */

            USBTGT_RNDIS_VDBG ("USBTGT_RNDIS_NOTIFY_RCV_DATA_FROM_MEDIUM\n",
                               1, 2, 3, 4, 5, 6);

            if (pUsbTgtRndis->pBinder->dataToTransport)
                {
                (pUsbTgtRndis->pBinder->dataToTransport)
                    (pUsbTgtRndis->pBinder->pTransportDev);
                }
            }
            break;
        case USBTGT_RNDIS_NOTIFY_DATA_CHANNEL_RESET:
            {
            USBTGT_RNDIS_DBG("USBTGT_RNDIS_NOTIFY_DATA_CHANNEL_RESET\n",
                             1, 2, 3, 4, 5, 6);

            if (pUsbTgtRndis->bulkOutPipeHandle != NULL)
                {
                if (TRUE == pUsbTgtRndis->bBulkOutErpUsed)
                    {
                    if (OK != usbTgtCancelErp(pUsbTgtRndis->bulkOutPipeHandle,
                                    &pUsbTgtRndis->bulkOutErp))
                        {
                        USBTGT_RNDIS_DBG("Cancel bulkOutErp fail\n",
                                        1, 2, 3, 4, 5, 6);
                        } 
                    }
                /* Re submit the bulk out transaction */

                if ( ERROR == usbTgtRndisBulkOutErpSubmit(pUsbTgtRndis))
                    {
                    USBTGT_RNDIS_DBG("Re-submit bulkOutErp fail\n",
                                    1, 2, 3, 4, 5, 6);
                    }
                }

            if (pUsbTgtRndis->bulkInPipeHandle != NULL)
                {
                if (TRUE == pUsbTgtRndis->bBulkInErpUsed)
                    {
                    if (OK != usbTgtCancelErp(pUsbTgtRndis->bulkInPipeHandle,
                                    &pUsbTgtRndis->bulkInErp))
                        {
                        USBTGT_RNDIS_DBG("Cancel bulkInErp fail\n",
                                        1, 2, 3, 4, 5, 6);
                        } 
                    }

                /* Re submit the bulk in transaction */
                
                if (ERROR == usbTgtRndisBulkInErpSubmit(pUsbTgtRndis))
                    {
                    USBTGT_RNDIS_DBG("Re-submit bulkInErp fail\n",
                                    1, 2, 3, 4, 5, 6);
                    }
                }
             }
            break;
        default:
            {
            /* This can be expand to handle all the ioctrl notifier next setps */
            
            USBTGT_RNDIS_VDBG("Unknown Notifiy\n",
                             1, 2, 3, 4, 5, 6);
            
            }
            break;
        }
    return;
    }

/*******************************************************************************
*
* usbTgtRndisManagementNotify - send message to usbTgtRndisManagerTask task
*
* This routine is used to send transfer message to usbTgtRndisManagerTask
* task.
*
* RETURNS: N/A.
*
* ERRNO: N/A
*/

void usbTgtRndisManagementNotify
    (
    pUSBTGT_RNDIS            pUsbTgtRndis,
    USBTGT_RNDIS_NOTIFY_CODE NotifyCode,
    void *                   pContext
    )
    {
    USBTGT_RNDIS_NOTIFY_INFO RndisNotifyInfo;

    if ((NULL == pUsbTgtRndis) ||
#if defined (USBTGT_NET_TASK_MODE)
        (NULL == pUsbTgtRndis->RndisTaskMsgQID)||
#endif /* USBTGT_NET_TASK_MODE */
        (USBTGT_RNDIS_NOTIFY_UNKNOWN == NotifyCode))
        {
        USBTGT_RNDIS_ERR("MsgQueue doesn't exist\n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }

    /* Fill the information */

    RndisNotifyInfo.pUsbTgtRndis = pUsbTgtRndis;
    RndisNotifyInfo.NotifyCode = NotifyCode;
    RndisNotifyInfo.pContext = pContext;
    
#if defined (USBTGT_NET_TASK_MODE)
    /* Send the mesQ to the task */

    if (OK != msgQSend (pUsbTgtRndis->RndisTaskMsgQID,
                         (char*) &RndisNotifyInfo,
                         sizeof(USBTGT_RNDIS_NOTIFY_INFO),
                         USBTGT_WAIT_TIMEOUT,
                         MSG_PRI_NORMAL))
        {
        USBTGT_RNDIS_ERR("Message Send Failed\n",
                     1, 2, 3, 4, 5 ,6);

        }
#else

    usbTgtRndisNotifyHandler(pUsbTgtRndis, &RndisNotifyInfo);

#endif /* USBTGT_NET_TASK_MODE */

    return ;
    }

/*******************************************************************************
*
* usbTgtRndisRcvDataFromMediumNotify - send message to usbTgtRndisManagerTask task
*
* This routine is used to send transfer message to usbTgtRndisManagerTask
* task.
*
* RETURNS: N/A.
*
* ERRNO: N/A
*/

LOCAL void usbTgtRndisRcvDataFromMediumNotify
    (
    void * pUsbTgtRndisDev
    )
    {
    pUSBTGT_RNDIS pUsbTgtRndis = (pUSBTGT_RNDIS)pUsbTgtRndisDev;

    if (NULL == pUsbTgtRndis)
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         "pUsbTgtRndis", 2, 3, 4, 5, 6);

        return;
        }

    if (FALSE == pUsbTgtRndis->bBulkInErpUsed)
        {
        USBTGT_RNDIS_VDBG ("bBulkInErpUsed is False\n",
                           1, 2, 3, 4, 5, 6);

        if (OK != usbTgtRndisBulkInErpSubmit(pUsbTgtRndis))
            {
            USBTGT_RNDIS_VDBG (" Give the sema which can not submit the erp\n",
                                1, 2, 3, 4, 5, 6);

            pUsbTgtRndis->bBulkInErpUsed = FALSE;
            }
        }
    /*
     * If the pipe still pending tranaction, just return, since new tranaction
     * will be issued by the bulk in callback routine.
     */

    return ;
    }

#if defined (USBTGT_NET_TASK_MODE)
/***************************************************************************
*
* usbTgtRndisManagementTask - handle the target notifications
*
* This is the task which manages the target notifiactions.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbTgtRndisManagementTask
    (
    pUSBTGT_RNDIS      pUsbTgtRndis
    )
    {
    USBTGT_RNDIS_NOTIFY_INFO RndisNotifyInfo;
    UINT8                    nBytes = 0;

    /* Check the validity of the parameter */

    if (NULL == pUsbTgtRndis)
        {
        USBTGT_RNDIS_ERR("Invalid parameter, pTcd is NULL\n",
                         1, 2, 3, 4, 5 ,6);

        return;
        }

    while (TRUE)
        {
        if (pUsbTgtRndis->RndisTaskMsgQID == NULL)
            {
            USBTGT_RNDIS_ERR("Invalid parameter, pTcd->TransferThreadMsgID is NULL\n",
                             1, 2, 3, 4, 5 ,6);

            break;
            }

        /* Reset the transfer task information */

        OS_MEMSET(&RndisNotifyInfo, 0,
                  sizeof(USBTGT_RNDIS_NOTIFY_INFO));

        /* Wait on the signalling of the event */

        nBytes = msgQReceive (pUsbTgtRndis->RndisTaskMsgQID,
                              (char*) & RndisNotifyInfo,
                              sizeof(USBTGT_RNDIS_NOTIFY_INFO),
                              USBTGT_WAIT_TIMEOUT);

        if ((NULL == RndisNotifyInfo.pUsbTgtRndis) ||
            (USBTGT_RNDIS_NOTIFY_UNKNOWN == RndisNotifyInfo.NotifyCode) ||
            (nBytes != sizeof(USBTGT_RNDIS_NOTIFY_INFO)))
            {
            /*
             * Kill the task ? Kill the task means one
             * usb target has one such task
             */

            continue;
            }

        usbTgtRndisNotifyHandler(pUsbTgtRndis, &RndisNotifyInfo);
        }
    return;
    }
#endif /* USBTGT_NET_TASK_MODE */

/*******************************************************************************
*
* usbTgtRndisDestroy - destroy one pUSBTGT_RNDIS resource
*
* This routine destroys one pUSBTGT_RNDIS resource which alloced by the
* usbTgtRndisCreate routine.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtRndisDestroy
    (
    pUSBTGT_RNDIS pUsbTgtRndis
    )
    {
    if (NULL == pUsbTgtRndis)
        {
        USBTGT_RNDIS_ERR("Invalid parameter %s is NULL\n",
                         "pUsbTgtRndis", 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Delete the mutex */

    if (NULL != pUsbTgtRndis->rndisMutex)
        {
        semTake (pUsbTgtRndis->rndisMutex, USBTGT_WAIT_TIMEOUT);
        semDelete (pUsbTgtRndis->rndisMutex);

        pUsbTgtRndis->rndisMutex = NULL;
        }

#if defined (USBTGT_NET_TASK_MODE)
    /* Destroy the management task */

    if (pUsbTgtRndis->RndisTaskID != OS_THREAD_FAILURE
        && pUsbTgtRndis->RndisTaskID != 0)
        {
        OS_DESTROY_THREAD(pUsbTgtRndis->RndisTaskID);
        pUsbTgtRndis->RndisTaskID = OS_THREAD_FAILURE;
        }

    /* Delete the MsqQ ID*/

    if (pUsbTgtRndis->RndisTaskMsgQID)
        {
        msgQDelete(pUsbTgtRndis->RndisTaskMsgQID);
        pUsbTgtRndis->RndisTaskMsgQID = NULL;
        }
#endif /* USBTGT_NET_TASK_MODE */

    /* Delete the Interrupt In Mutex */

    if (pUsbTgtRndis->intInErpMutex)
        {
        /* Mutex should be taken before delete it */
        semTake (pUsbTgtRndis->intInErpMutex, USBTGT_WAIT_TIMEOUT);

        semDelete (pUsbTgtRndis->intInErpMutex);
        pUsbTgtRndis->intInErpMutex = NULL;
        }

    /* Delete the Bulk In Mutex */

    if (pUsbTgtRndis->bulkInErpMutex)
        {
        /* Mutex should be taken before delete it */
        semTake (pUsbTgtRndis->bulkInErpMutex, USBTGT_WAIT_TIMEOUT);

        semDelete (pUsbTgtRndis->bulkInErpMutex);
        pUsbTgtRndis->bulkInErpMutex = NULL;
        }

    /* Delete the Bulk Out Mutex */

    if (pUsbTgtRndis->bulkOutErpMutex)
        {
        /* Mutex should be taken before delete it */
        semTake (pUsbTgtRndis->bulkOutErpMutex, USBTGT_WAIT_TIMEOUT);

        semDelete (pUsbTgtRndis->bulkOutErpMutex);
        pUsbTgtRndis->bulkOutErpMutex = NULL;
        }

    if (pUsbTgtRndis->pBinder)
        {
        usbTgtNetMediumBinderDetach(pUsbTgtRndis->pBinder);
        usbTgtNetMediumBinderDestroy (pUsbTgtRndis->pBinder);
        pUsbTgtRndis->pBinder = NULL;
        }

    /* Delete the resouce from the global list */

    if (ERROR != lstFind(&gUsbTgtRndisList, &pUsbTgtRndis->rndisNode))
        {
        lstDelete(&gUsbTgtRndisList, &pUsbTgtRndis->rndisNode);
        }

    USBTGT_RNDIS_VDBG ("usbTgtRndisDestroy the dev 0x%X\n",
                        (ULONG)pUsbTgtRndis, 2, 3, 4, 5, 6);

    OS_FREE(pUsbTgtRndis);

    return OK;
    }

/*******************************************************************************
*
* usbTgtRndisCreate - create one new RNDIS device data
*
* This routine creates one new RNDIS device data
*
* RETURNS: NULL or the pointer of pUSBTGT_RNDIS
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL pUSBTGT_RNDIS usbTgtRndisCreate
    (
    void
    )
    {
    pUSBTGT_RNDIS  pUsbTgtRndis = NULL;
    pUSBTGT_BINDER pBinder = NULL;

    UCHAR         RndisMTaskName[20];
    int           index = 0;
    STATUS        status = ERROR;

    pUsbTgtRndis = (pUSBTGT_RNDIS)OSS_CALLOC(sizeof (USBTGT_RNDIS));

    if (NULL == pUsbTgtRndis)
        {
        USBTGT_RNDIS_ERR("No enough memory to melloc the RNDIS resource\n",
                          1, 2, 3, 4, 5 ,6);

        return NULL;
        }

    /* Create the rndis mutex */

    pUsbTgtRndis->rndisMutex = semMCreate(USBTGT_MUTEX_CREATION_OPTS);

    if (NULL == pUsbTgtRndis->rndisMutex)
        {
        USBTGT_RNDIS_ERR("Create the rndisMutex fail\n",
                          1, 2, 3, 4, 5 ,6);

        usbTgtRndisDestroy (pUsbTgtRndis);

        return NULL;
        }

     /* Create the Interrupt In Erp Mutex */

     pUsbTgtRndis->intInErpMutex = semMCreate (USBTGT_MUTEX_CREATION_OPTS);

     if (NULL == pUsbTgtRndis->intInErpMutex)
         {
         USBTGT_RNDIS_ERR ("Basic Mutex Creation fro interrupt In Erp failed\n",
                       1, 2, 3, 4, 5, 6);

         usbTgtRndisDestroy(pUsbTgtRndis);

         return NULL;
         }

     /* Create the Bulk In Erp Mutex */

     pUsbTgtRndis->bulkInErpMutex = semMCreate (USBTGT_MUTEX_CREATION_OPTS);

     if (NULL == pUsbTgtRndis->bulkInErpMutex)
         {
         USBTGT_RNDIS_ERR ("Basic Mutex Creation for Bulk In erp failed\n",
                       1, 2, 3, 4, 5, 6);

         usbTgtRndisDestroy(pUsbTgtRndis);

         return NULL;
         }

     /* Create the Bulk Out Erp Mutex */

     pUsbTgtRndis->bulkOutErpMutex = semMCreate (USBTGT_MUTEX_CREATION_OPTS);

     if (NULL == pUsbTgtRndis->bulkOutErpMutex)
         {
         USBTGT_RNDIS_ERR ("Basic Mutex Creation failed\n",
                       1, 2, 3, 4, 5, 6);

         usbTgtRndisDestroy(pUsbTgtRndis);

         return NULL;
         }

    /* Create the semaphone for the bulk Out Erp */

    pBinder = usbTgtNetMediumBinderCreate(usrUsbTgtNetworkDataPoolCountGet(),
                                          usrUsbTgtNetworkDataPoolSizeGet(),
                                          sizeof (RNDIS_PACKET_MSG));

    if (pBinder == NULL)
        {
        USBTGT_RNDIS_ERR ("Create the binder "
                          "failed\n", 1, 2, 3, 4, 5, 6);

        usbTgtRndisDestroy(pUsbTgtRndis);

        return NULL;
        }

    pUsbTgtRndis->pBinder = pBinder;
    pBinder->pTransportDev = pUsbTgtRndis;
    pBinder->dataToTransport = usbTgtRndisRcvDataFromMediumNotify;

    pBinder->uTransportType = USBTGT_BINDER_TRANSPORT_USB_RNDIS;

    pUsbTgtRndis->pMediumName = usrUsbTgtRndisMediumNameGet(index);
    pUsbTgtRndis->uMediumUnit = usrUsbTgtRndisMediumUnitGet(index);
    pUsbTgtRndis->uMediumType = usrUsbTgtRndisMediumTypeGet(index);

    status = usbTgtNetMediumBinderAttach(pBinder,
                             pUsbTgtRndis->pMediumName,
                             pUsbTgtRndis->uMediumUnit,
                             pUsbTgtRndis->uMediumType);

    if (ERROR == status)
        {
        USBTGT_RNDIS_ERR ("Create the binder "
                          "failed\n", 1, 2, 3, 4, 5, 6);

        usbTgtRndisDestroy(pUsbTgtRndis);

        return NULL;
        }

#if defined (USBTGT_NET_TASK_MODE)
    /* Create the MsgQ ID */

    pUsbTgtRndis->RndisTaskMsgQID = msgQCreate(USBTGT_MAX_MSG_COUNT * 10,
                                     sizeof(USBTGT_RNDIS_NOTIFY_INFO),
                                     MSG_Q_FIFO);

    if (pUsbTgtRndis->RndisTaskMsgQID == NULL)
        {
        USBTGT_RNDIS_ERR("Create Tcd RndisTaskMsgQID error\n",
                         1, 2, 3, 4, 5 ,6);

        usbTgtRndisDestroy (pUsbTgtRndis);

        return NULL;
        }
#endif /* USBTGT_NET_TASK_MODE */

    /* Add to the global list */

    lstAdd (&gUsbTgtRndisList, &pUsbTgtRndis->rndisNode);

    /*
     * Create the rndis mamagement task
     * Using the node index to mark the task number
     * we need lstFind the pUsbTgtRndis->rndisNode,
     * since this node just added to the list, use lstCount to mark this
     */

    index = lstCount (&gUsbTgtRndisList);

#if defined (USBTGT_NET_TASK_MODE)
    snprintf((char*)RndisMTaskName, 20, "RNDIS_M%d", index - 1);

    pUsbTgtRndis->RndisTaskID = OS_CREATE_THREAD((char *)RndisMTaskName,
                                               usrUsbTgtRndisTaskPriorityGet(index - 1),
                                               usbTgtRndisManagementTask,
                                               pUsbTgtRndis);

    if (OS_THREAD_FAILURE == pUsbTgtRndis->RndisTaskID)
        {
        USBTGT_RNDIS_ERR("Create the Management task for 0x%X fail\n",
                         pUsbTgtRndis,
                         2, 3, 4, 5, 6);

        usbTgtRndisDestroy (pUsbTgtRndis);

        return NULL;
        }
#endif /* USBTGT_NET_TASK_MODE */

    USBTGT_RNDIS_VDBG ("usbTgtRndisCreate the dev 0x%X\n",
                        (ULONG)pUsbTgtRndis, 2, 3, 4, 5, 6);

    return pUsbTgtRndis;
    }

/*******************************************************************************
*
* usbTgtRndisUnInit - uninitialize the RNDIS driver and destroy resouce which created
* before
*
* This routine uninitializes the RNDIS driver and destroy resouce which created
* before.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*/

STATUS usbTgtRndisUnInit
    (
    void
    )
    {
    pUSBTGT_RNDIS   pUsbTgtRndis = NULL;
    int             index;
    int             count;

    /* Get the count of the global RNDIS devcie list */

    count = lstCount(&gUsbTgtRndisList);

    for (index = 1; index <= count; index ++)
        {
        pUsbTgtRndis = (pUSBTGT_RNDIS)lstNth(&gUsbTgtRndisList, index);

        if (NULL != pUsbTgtRndis)
            {
            if (OK != usbTgtFuncUnRegister(pUsbTgtRndis->targChannel))
                {
                USBTGT_RNDIS_WARN("Un-register from the TML fail\n",
                                  1, 2, 3, 4, 5 ,6);

                }

            if (OK != usbTgtRndisDestroy(pUsbTgtRndis))
                {
                USBTGT_RNDIS_WARN("Destroy the Rndis device fail\n",
                                  1, 2, 3, 4, 5 ,6);

                }
            }
        }

    gUsbTgtRndisInited = FALSE;

    return OK;

    }

/*******************************************************************************
*
* usbTgtRndisInit - initialize the RNDIS driver and create resouce according to the
* user's configuration.
*
* This routine initializes the RNDIS driver and create resouce according the
* user's configuration.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*/

STATUS usbTgtRndisInit
    (
    void
    )
    {
    pUSBTGT_RNDIS       pUsbTgtRndis = NULL;
    USBTGT_FUNC_INFO    usbTgtFuncInfo;
    int                 index;

    /* This module has been initialized */

    if (gUsbTgtRndisInited)
        {
        return OK;
        }

    /* Init the global RNDIS devcie list */

    lstInit(&gUsbTgtRndisList);

    /* Allocal memory resource */

    for (index = 0; index < usrUsbTgtRndisCountGet(); index++)
        {
        pUsbTgtRndis = usbTgtRndisCreate();

        if (pUsbTgtRndis == NULL)
            {
            USBTGT_RNDIS_ERR("Create new Rndis device fail\n",
                              1, 2, 3, 4, 5 ,6);

            usbTgtRndisUnInit();

            return ERROR;
            }

        usbTgtFuncInfo.ppFsFuncDescTable = (USB_DESCR_HDR **)gUsbTgtRndisDescHdr;
        usbTgtFuncInfo.ppHsFuncDescTable = (USB_DESCR_HDR **)gHsUsbTgtRndisDescHdr;
        usbTgtFuncInfo.ppSsFuncDescTable = (USB_DESCR_HDR **)gSsUsbTgtRndisDescHdr;
        usbTgtFuncInfo.pFuncCallbackTable =
                            (USB_TARG_CALLBACK_TABLE *)&gUsbTgtRndisCallbackTable;
        usbTgtFuncInfo.pCallbackParam = (pVOID)pUsbTgtRndis;
        usbTgtFuncInfo.pFuncSpecific = (pVOID)pUsbTgtRndis;

        usbTgtFuncInfo.pFuncName = usrUsbTgtRndisNameGet(index);
        usbTgtFuncInfo.pTcdName = usrUsbTgtRndisTcdNameGet(index);
        usbTgtFuncInfo.uFuncUnit = usrUsbTgtRndisUnitGet(index);
        usbTgtFuncInfo.uTcdUnit = usrUsbTgtRndisTcdUnitGet(index);
        usbTgtFuncInfo.uConfigToBind = usrUsbTgtRndisConfigNumGet(index);
        usbTgtFuncInfo.pWcidString = USB_RNDIS_WCID_STRING;
        usbTgtFuncInfo.pSubWcidString = USB_RNDIS_WCID_SUB_STRING;
        usbTgtFuncInfo.uWcidVc = USB_RNDIS_WCID_VC;

        pUsbTgtRndis->targChannel = usbTgtFuncRegister(&usbTgtFuncInfo);

        if (USBTGT_TARG_CHANNEL_DEAD == pUsbTgtRndis->targChannel)
            {
            USBTGT_RNDIS_ERR("Register to the TML fail\n",
                             1, 2, 3, 4, 5, 6);
            
            (void)usbTgtRndisDestroy (pUsbTgtRndis);

            return ERROR;
            }
        }

    gUsbTgtRndisInited = TRUE;

    USBTGT_RNDIS_DBG("usbTgtRndisInit successfully\n",
                     1, 2, 3, 4, 5 ,6);

    return OK;
    }



