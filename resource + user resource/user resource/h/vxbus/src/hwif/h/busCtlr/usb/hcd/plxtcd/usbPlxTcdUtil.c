/* usbPlxTcdUtil.c -  USB PLX TCD Utility Module */

/*
 * Copyright (c) 2012-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01i,03may13,wyy  Remove compiler warning (WIND00356717)
01h,23nov12,s_z  Fix data swap issue (WIND00390391)
01g,21nov12,s_z  Add Reset exit workaround based Errata Issue_4
                 Reset squeuence number after endpoint halt (WIND00389964)
01f,21nov12,s_z  Change debugging message (WIND00389887)
01e,23oct12,s_z  Add U1/U2 rejection workaround based Errata (WIND00382685)
01d,17oct12,s_z  Fix short packet receive issue (WIND00374594)
01c,10oct12,s_z  Remove un-used routine (WIND00382688)
01b,18sep12,s_z  Pass CV2.0 testing (WIND00375402)
01a,18may12,s_z  Writen
*/

/*
DESCRIPTION

This module defines the utility functions for the USB PLX Target
Controller Driver.

INCLUDE FILES: usbPlxTcdUtil.h usbPlxTcdDma.h
*/

/* includes */


#include <usbPlxTcdUtil.h>
#include <usbPlxTcdDma.h>

/*******************************************************************************
*
* usbPlxTcdFIFOWrite - write data to the PLX TCD FIFO from data buffer
*
* This routine is used to write data to the PLX TCD FIFO from data buffer.
* It is used in un-DMA (PIO) mode.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbPlxTcdFIFOWrite
    (
    pUSB_PLX_CTRL pPlxCtrl,   /* Pointer to common data structure */
    UINT8 *       pSrcBuffer, /* Pointer to the source buffer */
    UINT32        uCount,     /* Count of byte to write */
    UINT8         uEpIndex    /* Endpoint index */
    )
    {
    UINT8 * pTemp = pSrcBuffer;
    UINT32  uData32;
    UINT16  i;
    BOOL    bValid = TRUE;

    /* Parameter verification */

    if ((NULL == pPlxCtrl) ||
        (NULL == pSrcBuffer))
        {
        USB_PLX_ERR("Invalid parameter, %s is NULL\n",
                    ((NULL == pPlxCtrl) ? "pPlxCtrl" :
                     "pSrcBuffer"),
                     2, 3, 4, 5 ,6);
        return;
        }

    if (USB_PLX_MAX_EP_COUNT <= uEpIndex)
        {
        USB_PLX_ERR("uEpIndex = %u is too big\n", uEpIndex, 2, 3, 4, 5, 6);

        return;
        }

    USB_PLX_VDBG ("usbPlxTcdFIFOWrite(): Write 0x%X bytes to FIFO of EP %d\n",
                  uCount, uEpIndex, 3, 4, 5, 6);

    if (pPlxCtrl->epReg[uEpIndex].uEpMaxPktSize == uCount)
        bValid = FALSE;

    /*
     * Determines the number of bytes writen to the
     * endpoint FIFO for the next write transaction.
     */

    if (uCount >= 4)
        {
        uData32 = USB_PLX_REG_READ32 (pPlxCtrl,
                                      USB_PLX_EP_CFG_OFFSET (uEpIndex));

        /* Clean the EP FIFO bytes counts first */

        uData32 &= ~ (USB_PLX_EP_CFG_EBC);

        /* Up to 4 bytes to be writen */

        uData32 |= 4 << USB_PLX_EP_CFG_BYTE_CNT_SHIFT;

        /* Set the EP_CFG register */

        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_EP_CFG_OFFSET (uEpIndex),
                             uData32);
        }

    /* Start FIFO write */

    while (uCount >= 4)
        {
        uData32 = OS_UINT32_LE_TO_CPU( *(UINT32 *)pTemp);

        /* Write 4 bytes of data into the Endpoint FIFO */

        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_EP_DATA_OFFSET (uEpIndex),
                             uData32);

        pTemp += 4;

        /* Decrement size to copy */

        uCount -= 4;
        }

    /* Write the remain data */

     if ((0 != uCount) ||
         (TRUE == bValid))
        {
        uData32 = USB_PLX_REG_READ32 (pPlxCtrl,
                                      USB_PLX_EP_CFG_OFFSET (uEpIndex));

        /* Mask off the bits for Byte Count */

        uData32 &= ~USB_PLX_EP_CFG_EBC;

        if (uCount)
            uData32 |= (uCount << USB_PLX_EP_CFG_BYTE_CNT_SHIFT);

        /* Write the value in the EP_CFG register */

        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_EP_CFG_OFFSET (uEpIndex),
                             uData32);

       /* Write the data from TRB Buffer in the FIFO */

       uData32 = 0;

       for (i = 0 ; i < uCount; i++ )
           {
           uData32 |= (*(pTemp + i) << (i * 8));
           }

        /* Write the data in the Endpoint FIFO */

        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_EP_DATA_OFFSET (uEpIndex),
                             uData32);

        }

    return;
    }

/*******************************************************************************
*
* usbPlxTcdFIFORead - read data from the PLX TCD FIFO
*
* This routine is used to read data from the PLX TCD FIFO.
* It is mainly used in un-DMA mode.
*
* RETURNS: the bytes has been read from the FIFO
*
* ERRNO: N/A
*
* \NOMANUAL
*/

UINT32 usbPlxTcdFIFORead
    (
    pUSB_PLX_CTRL pPlxCtrl,   /* Pointer to common data structure */
    UINT8 *       pDisBuffer, /* Pointer to the distance buffer */
    UINT32        uCount,     /* Count of byte to read */
    UINT8         uEpIndex    /* Endpoint index */
    )
    {
    UINT8 * pTemp = pDisBuffer;
    UINT32  uData32;
    UINT32  uDataToRead;
    UINT8   i;

    /* Parameter verification */

    if ((NULL == pPlxCtrl) ||
        (NULL == pDisBuffer))
        {
        /* Debug print */

        USB_PLX_ERR("Invalid parameter, %s is NULL\n",
                    ((NULL == pPlxCtrl) ? "pPlxCtrl" :
                     "pDisBuffer"),
                     2, 3, 4, 5 ,6);

        return 0;
        }

    /* Check the data count available in the FIFO */

    uData32 = USB_PLX_REG_READ32 (pPlxCtrl,
                                  USB_PLX_EP_AVAIL_OFFSET (uEpIndex));

    uDataToRead = min(uData32, uCount);

    USB_PLX_VDBG ("usbPlxTcdFIFORead(): To read 0x%X bytes From EP %d FIFO, available 0x%X\n",
                  uCount, uEpIndex, uDataToRead, 4, 5, 6);

    uCount = uDataToRead;

    while (uDataToRead >= 4)
        {
         uData32  = USB_PLX_REG_READ32(pPlxCtrl,
                                       USB_PLX_EP_DATA_OFFSET (uEpIndex));

        *((UINT32 *)pTemp) = OS_UINT32_LE_TO_CPU(uData32);

        pTemp += 4;
        uDataToRead -= 4;
        }

    if (uDataToRead != 0)
        {
        uData32 = USB_PLX_REG_READ32(pPlxCtrl,
                           USB_PLX_EP_DATA_OFFSET (uEpIndex));

        for (i = 0; i < uDataToRead; i++)

            {
            *(pTemp)++ = (UINT8) (uData32 & 0xFF);

            uData32 >>= 8;
            }
        }

    return uCount;
    }


/*******************************************************************************
*
* usbPlxTcdFirstPendingReqGet - get first pending request of the pipe
*
* This routine is used to get the first pending request of the pipe.
*
* RETURNS: NULL, or pointer of the first request
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_PLX_TCD_REQUEST usbPlxTcdFirstPendingReqGet
    (
    pUSB_PLX_TCD_PIPE pTCDPipe
    )
    {
    NODE *               pNode = NULL;
    pUSB_PLX_TCD_REQUEST pRequest = NULL;

    /* Parameter verification */

    if (NULL == pTCDPipe)
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    "pTCDPipe", 2, 3, 4, 5, 6);

        return NULL;
        }

    OS_WAIT_FOR_EVENT(pTCDPipe->pPipeSyncMutex, WAIT_FOREVER);

    pNode = lstFirst(&(pTCDPipe->pendingReqList));

    if (NULL != pNode)
        {
        pRequest = USB_PLX_TCD_REQUEST_CONV_FROM_NODE(pNode);
        }

    OS_RELEASE_EVENT(pTCDPipe->pPipeSyncMutex);

    USB_PLX_VDBG ("usbPlxTcdFirstPendingReqGet(): pRequest %p\n",
                  pRequest, 2, 3, 4, 5, 6);

    return pRequest;
    }

/*******************************************************************************
*
* usbPlxTcdPipeFind - find the pipe according to the endpoint index
*
* This routine is used to find the pipe according to the endpoint index.
*
* RETURNS: NULL, or pointer of the first request if have
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_PLX_TCD_PIPE usbPlxTcdPipeFind
    (
    pUSB_PLX_TCD_DATA pTCDData,
    UINT8             uPhyEpIndex
    )
    {
    NODE *            pNode = NULL;
    pUSB_PLX_TCD_PIPE pTCDPipe = NULL;

    /* Parameter verification */

    if (NULL == pTCDData)
        {
        USB_PLX_ERR("Invalid Paramere: pTCDData is NULL\n",
                    1, 2, 3, 4, 5, 6);

        return NULL;
        }

    OS_WAIT_FOR_EVENT(pTCDData->tcdSyncMutex, WAIT_FOREVER);

    pNode = lstFirst(&pTCDData->tcdPipeList);

    while (NULL != pNode)
        {
        pTCDPipe = USB_PLX_TCD_PIPE_CONV_FROM_NODE(pNode);

        pNode = lstNext (pNode);

        if ((NULL != pTCDPipe) &&
            (pTCDPipe->uPhyEpAddr == uPhyEpIndex))
            {
            OS_RELEASE_EVENT(pTCDData->tcdSyncMutex);

            USB_PLX_VDBG ("usbPlxTcdPipeFind(): pTCDPipe %p\n",
                          pTCDPipe, 2, 3, 4, 5, 6);

            return pTCDPipe;
            }
        }

    OS_RELEASE_EVENT(pTCDData->tcdSyncMutex);

    USB_PLX_VDBG ("usbPlxTcdPipeFind(): No find pTCDPipe for Ep %d\n",
                  uPhyEpIndex, 2, 3, 4, 5, 6);

    return NULL;
    }


/*******************************************************************************
*
* usbPlxTcdRequestDestroy - destroy a request of the pipe
*
* This routine is used to destroy a request of the pipe. It will destroy
* the DMA map associated with this request structure.
*
* <pHCDPipe> - Pointer to the pipe
* <pRequest> - Pointer to the request structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the PLX TCD. Assumption is that valid parameters are passed by the TCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* Note: It is expetect the pipe lock has been taken when calling this routine.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbPlxTcdRequestDestroy
    (
    pUSB_PLX_TCD_PIPE    pTCDPipe,
    pUSB_PLX_TCD_REQUEST pRequest
    )
    {

    /* Delete it from the request list */

    if (ERROR != lstFind(&(pTCDPipe->freeReqList), &(pRequest->reqNode)))
        lstDelete(&(pTCDPipe->freeReqList), &(pRequest->reqNode));

    /* Free the descriptor */

    if (pRequest->pScatDesc)
        OSS_FREE(pRequest->pScatDesc);

    /* Free the request */

    OS_FREE(pRequest);

    return;
    }


/*******************************************************************************
*
* usbPlxTcdRequestCreate - create a request info of the pipe
*
* This routine is used to create a request info of the pipe. It will create
* the DMA map associated with this request info structure.
*
* <pHCDPipe> - Pointer to the pipe
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* Note: It is expetect the pipe lock has been taken when calling this routine.
*
* RETURNS: Pointer to the request info structure
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL pUSB_PLX_TCD_REQUEST usbPlxTcdRequestCreate
    (
    pUSB_PLX_TCD_PIPE pTCDPipe
    )
    {
    /* Request information */

    pUSB_PLX_TCD_REQUEST  pRequest = NULL;

    /* Get the request info */

    pRequest = (pUSB_PLX_TCD_REQUEST)OSS_CALLOC(sizeof(USB_PLX_TCD_REQUEST));

    if (pRequest == NULL)
        {
        USB_PLX_ERR("usbPlxTcdRequestCreate - allocate pRequest fail\n",
                    1, 2, 3, 4, 5, 6);

        return NULL;
        }

    /* Record the pipe we work for */

    pRequest->pTCDPipe = pTCDPipe;

    /* Add the request into the free request list */

    if (ERROR == lstFind(&(pTCDPipe->freeReqList), &(pRequest->reqNode)))
        lstAdd(&(pTCDPipe->freeReqList), &(pRequest->reqNode));

    USB_PLX_VDBG("usbPlxTcdRequestCreate - allocate pRequest %p\n",
                 pRequest, 2, 3, 4, 5, 6);

    return pRequest;
    }

/*******************************************************************************
*
* usbPlxTcdRequestReserve - reserve a request of the pipe for a ERP
*
* This routine is used to reserve a request structure of the pipe for a
* ERP. It is commonly called when submitting a ERP. A request info is considered
* to have been reserved when its associated with a URB (the URB pointer of this
* request info structure being non-NULL).
*
* <pTCDPipe> - Pointer to the pipe
*
* WARNING : No parameter validation is done as this routine is used internally
* by the TCD. Assumption is that valid parameters are passed by the TCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* Note: It is expetect the pipe lock has been taken when calling this routine.
*
* RETURNS: the pointer to the reserved request info structure,
*   or NULL if no more free request on this pipe.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_PLX_TCD_REQUEST usbPlxTcdRequestReserve
    (
    pUSB_PLX_TCD_PIPE pTCDPipe
    )
    {
    NODE *               pNode;
    pUSB_PLX_TCD_REQUEST pRequest;

    /* Parameter verification */

    if (NULL == pTCDPipe)
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ("pTCDPipe"), 2, 3, 4, 5, 6);

        return NULL;
        }

    OS_WAIT_FOR_EVENT(pTCDPipe->pPipeSyncMutex, WAIT_FOREVER);

    /* Get the first request from the free list */

    pNode = lstFirst(&pTCDPipe->freeReqList);

    /* If there is no free request, create a new one */

    if (NULL == pNode)
        {
        pRequest = usbPlxTcdRequestCreate(pTCDPipe);
        }
    else
        {
        pRequest = USB_PLX_TCD_REQUEST_CONV_FROM_NODE(pNode);
        }

    if (NULL != pRequest)
        {
        if (ERROR != lstFind(&pTCDPipe->freeReqList, &pRequest->reqNode))
            lstDelete(&pTCDPipe->freeReqList, &pRequest->reqNode);
        }

    OS_RELEASE_EVENT(pTCDPipe->pPipeSyncMutex);

    USB_PLX_VDBG("usbPlxTcdRequestReserve(): pRequest %x is reserved\n",
                 pRequest,2,3,4,5,6);

    return pRequest;
    }


/*******************************************************************************
*
* usbPlxTcdRequestRelease - release a used request
*
* This routine release a used request add it to the pipe's free list
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbPlxTcdRequestRelease
    (
    pUSB_PLX_TCD_PIPE    pTCDPipe,
    pUSB_PLX_TCD_REQUEST pRequest
    )
    {
    pUSB_PLX_SCAT_DMA_DESC pScatDesc;
    UINT16                 uErpCount;

    /* Parameter verification */

    if ((NULL == pTCDPipe) ||
        (NULL == pRequest))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTCDPipe) ? "pTCDPipe" :
                    "pRequest"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    USB_PLX_VDBG("usbPlxTcdRequestRelease(): pRequest %x is released\n",
                 pRequest,2,3,4,5,6);

    OS_WAIT_FOR_EVENT(pTCDPipe->pPipeSyncMutex, WAIT_FOREVER);

    /* Do not update the scatter/gather dma descripter buffer */

    pScatDesc = pRequest->pScatDesc;
    uErpCount = pRequest->uErpBufCount;

    OS_MEMSET(pRequest, 0, sizeof(USB_PLX_TCD_REQUEST));

    pRequest->pScatDesc = pScatDesc;
    pRequest->uErpBufCount = uErpCount;

    if (ERROR == lstFind(&pTCDPipe->freeReqList, &pRequest->reqNode))
        lstAdd(&pTCDPipe->freeReqList, &pRequest->reqNode);

    OS_RELEASE_EVENT(pTCDPipe->pPipeSyncMutex);

    return OK;
    }


/*******************************************************************************
*
* usbPlxTcdRequestComplete - complete a request
*
* This routine is to complete a request
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbPlxTcdRequestComplete
    (
    pUSB_PLX_TCD_REQUEST pRequest,
    int                  status
    )
    {
    pUSB_PLX_TCD_PIPE  pTCDPipe = NULL;
    pUSB_PLX_TCD_DATA  pTCDData = NULL;
    pUSB_ERP           pErp = NULL;

    /* Parameter verification */

    if ((NULL == pRequest) ||
        (NULL == (pTCDPipe = pRequest->pTCDPipe)) ||
        (NULL == (pTCDData = pTCDPipe->pTCDData)))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pRequest) ? "pRequest" :
                     (NULL == pTCDPipe) ? "pTCDPipe" :
                    "pTCDData"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    USB_PLX_VDBG("usbPlxTcdRequestComplete pRequest %p with Act %x Xfer %x\n",
                 pRequest, pRequest->uActLength, pRequest->uXferSize,
                 4, 5, 6);

    /* Remove the request from the pipe list */

    OS_WAIT_FOR_EVENT(pTCDPipe->pPipeSyncMutex, WAIT_FOREVER);

    pTCDPipe->status &= ~USBTGT_PIPE_STATUS_PROCESSING;

    if (ERROR != lstFind(&(pTCDPipe->pendingReqList), &(pRequest->reqNode)))
        {
        pRequest->pTCDPipe = NULL;
        lstDelete(&(pTCDPipe->pendingReqList), &(pRequest->reqNode));
        }

    /*
     * Set the pErp to NULL to avoid re-enter this
     * routine for the same request
     */

    pErp = pRequest->pErp;
    pRequest->pErp = NULL;

    /* If the DMA still active, please stop it and abort */

    if(pRequest->bDmaActive == TRUE)
        {
        usbPlxTcdDmaStop(pTCDData, pTCDPipe);

        USB_PLX_REG_WRITE32 (pTCDData->pPlxCtrl,
                             USB_PLX_DMASTAT_OFFSET (pTCDPipe->uDmaChannel),
                             USB_PLX_DMASTAT_ABORT);
        }

    OS_RELEASE_EVENT(pTCDPipe->pPipeSyncMutex);

    if (pErp != NULL)
        {
        /* Update the actual length */

        if (pRequest->uErpBufIndex < pRequest->uErpBufCount)
            pErp->bfrList[pRequest->uErpBufIndex].actLen = pRequest->uActLength;

        /*
         * The tcdPtr is equal to the tailRequest
         * call the callback routine
         */

        if (pErp->tcdPtr == pRequest)
            {
            USB_PLX_VDBG("usbPlxTcdRequestComplete actLen %d \n",
                        pErp->bfrList[0].actLen, 2, 3, 4, 5, 6);

            pErp->result = status;

            pErp->tcdPtr = NULL;

            if (pErp->targCallback != NULL)
                pErp->targCallback(pErp);
            }
        }

    USB_PLX_VDBG("usbPlxTcdRequestComplete(): Release the request for future using\n",
                 pRequest, 2, 3, 4, 5, 6);

    /* Release the request's resource */

    usbPlxTcdRequestRelease(pTCDPipe, pRequest);

    usbPlxTcdSchedule(pTCDData);
    return OK;
    }

/*******************************************************************************
*
* usbPlxTcdPipePhyEpAssign - assign one physical endpoint to the pipe
*
* This routine is used to assign one physical endpoint to the pipe.
*
* RETURNS:always OK
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbPlxTcdPipePhyEpAssign
    (
    pUSB_PLX_TCD_DATA pTCDData,
    pUSB_PLX_TCD_PIPE pTCDPipe
    )
    {
    pUSB_PLX_CTRL pPlxCtrl = NULL;
    UINT32        uEpIrqEnb;
    UINT8         uIndex;

    /* Parameter verification */

    if ((NULL == pTCDData) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)) ||
        (NULL == pTCDPipe))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTCDData) ? "pTCDData" :
                    (NULL == pPlxCtrl) ? "pPlxCtrl" :
                    "pTCDPipe"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    OS_WAIT_FOR_EVENT(pTCDData->tcdSyncMutex, WAIT_FOREVER);

    if (pTCDPipe->uEpType == USB_ATTR_CONTROL)
        {
        pTCDPipe->uPhyEpAddr = USB_PLX_ENDPT_0;

        USB_PLX_VDBG("Ep 0 will be used for control pipe\n",
                     1, 2, 3, 4, 5, 6);

        pPlxCtrl->epReg[0].uEpMaxPktSize = pTCDPipe->uMaxPacketSize;

        /* Enable the data packet transfer and receive interrupt */

        uEpIrqEnb = USB_PLX_EP_IRQENB_DPR |
                     USB_PLX_EP_IRQENB_DPT;

        USB_PLX_REG_WRITE32(pPlxCtrl, USB_PLX_EP_IRQENB_OFFSET(0),
                            uEpIrqEnb);

        /* Ep 0 is always ebabled */

        }
    else
        {
        /* Get the endpoint index */

        uIndex = (UINT8)(pTCDPipe->uEpAddr & ~USB_ENDPOINT_DIR_MASK);

        if (USB_ENDPOINT_IN ==  pTCDPipe->uEpDir)
            {
            uEpIrqEnb = USB_PLX_EP_IRQENB_DPT;
            }
        else
            {
            uEpIrqEnb = USB_PLX_EP_IRQENB_DPR;
            }

        /* Clean the status register and flush the FIFO */

        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_EP_STAT_OFFSET(uIndex),
                             USB_PLX_REG_READ32 (pPlxCtrl,
                             USB_PLX_EP_STAT_OFFSET(uIndex)) |
                             (0x200));

        /* Enable the endpoint for using */

        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_EP_RSP_OFFSET(uIndex),
                             USB_PLX_REG_READ32 (pPlxCtrl,
                             USB_PLX_EP_RSP_OFFSET(uIndex)) &
                             (~ 0x10));

        pPlxCtrl->epReg[uIndex].uEpMaxPktSize = pTCDPipe->uMaxPacketSize;

        USB_PLX_REG_WRITE32(pPlxCtrl,
                            USB_PLX_IDXADDR_REG,
                            USB_PLX_EP_X_SS_MAXPKT_IDX(uIndex));


        /* The frame number is valid of bit 10:0 */

        /*
         * According to the data book, the biggest max packet per interval
         * is 2, set it as 2.
         */

        USB_PLX_REG_WRITE32(pPlxCtrl,
                            USB_PLX_IDXDATA_REG,
                            pPlxCtrl->epReg[uIndex].uEpMaxPktSize | (2 << 11));

        USB_PLX_REG_WRITE32(pPlxCtrl,
                            USB_PLX_IDXADDR_REG,
                            USB_PLX_EP_X_HS_MAXPKT_IDX(uIndex));


        /* The frame number is valid of bit 10:0 */

        USB_PLX_REG_WRITE32(pPlxCtrl,
                            USB_PLX_IDXDATA_REG,
                            pPlxCtrl->epReg[uIndex].uEpMaxPktSize /* |
                            (1 << 11)*/) ;

        USB_PLX_REG_WRITE32(pPlxCtrl,
                            USB_PLX_IDXADDR_REG,
                            USB_PLX_EP_X_FS_MAXPKT_IDX(uIndex));


        USB_PLX_REG_WRITE32(pPlxCtrl,
                            USB_PLX_IDXDATA_REG,
                            pPlxCtrl->epReg[uIndex].uEpMaxPktSize);

        /*
         * Set the FIFO size and base:
         * NOTE:This is only for test, we should care more the base
         * configuratin for different endpoint using.
         * Marked as TODO for other kinds of function drivers
         */

        if (USB_ENDPOINT_IN ==  pTCDPipe->uEpDir)
            {
            USB_PLX_REG_WRITE32(pPlxCtrl,
                                USB_PLX_EP_FIFO_SIZE_BASE(uIndex),
                                (0x6 << 16) |
                                (0x9 << 22));
            }
        else
            {
            USB_PLX_REG_WRITE32(pPlxCtrl,
                                USB_PLX_EP_FIFO_SIZE_BASE(uIndex),
                                (0x6 << 0) |
                                (0x8 << 6));
            }

        /* Enable the interrupt type */

        USB_PLX_REG_WRITE32(pPlxCtrl,
                            USB_PLX_EP_IRQENB_OFFSET(uIndex),
                            uEpIrqEnb);

        /*
         * PLX suggest to set the max burst to 3.
         * But from the internal testing, how much of the burst size
         * is no much help. (Help but limited)
         * All the transaction speed is depend the FIFO size which set
         * above
         */

        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_EP_CFG_OFFSET(uIndex),
                             ((pTCDPipe->uEpAddr) |
                              (0x3 << 24) |
                              (pTCDPipe->uEpType << 8) |
                              USB_PLX_EP_CFG_ENABLE));

        USB_PLX_VDBG("The pipe is assigned to Ep %d,[CFG 0x%X]\n",
                     uIndex, 2, 3, 4, 5, 6);

        pTCDPipe->uPhyEpAddr = uIndex;

        usbPlxTcdPipeSequNumReset(pPlxCtrl, pTCDPipe);

        pPlxCtrl->uIrqMask0 |=  (0x1 << uIndex);

        USB_PLX_REG_WRITE32(pPlxCtrl,
                            USB_PLX_PCIIRQENB0_REG,
                            pPlxCtrl->uIrqMask0);


        /* Set the DMA channel number to illegal value */
        }

    OS_RELEASE_EVENT(pTCDData->tcdSyncMutex);

    USB_PLX_VDBG("usbPlxTcdPipePhyEpAssign(): Ep %d has been assigned to pipe %p\n",
                 pTCDPipe->uPhyEpAddr, pTCDPipe, 3, 4, 5, 6);

    return OK;
    }

/*******************************************************************************
*
* usbPlxTcdPipePhyEpRelease - release one physical endpoint used by one pipe
*
* This routine is used to release one physical endpoint used by one pipe.
*
* RETURNS: alwasy OK
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbPlxTcdPipePhyEpRelease
    (
    pUSB_PLX_TCD_DATA pTCDData,
    pUSB_PLX_TCD_PIPE pTCDPipe
    )
    {
    pUSB_PLX_CTRL pPlxCtrl = NULL;

    /* Parameter verification */

    if ((NULL == pTCDData) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)) ||
        (NULL == pTCDPipe))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTCDData) ? "pTCDData" :
                    (NULL == pPlxCtrl) ? "pPlxCtrl" :
                    "pTCDPipe"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    OS_WAIT_FOR_EVENT(pTCDData->tcdSyncMutex, WAIT_FOREVER);

    if (pTCDPipe->uPhyEpAddr != USB_ENDPOINT_MAX_COUNT)
        {
        USB_PLX_VDBG("Release physical endpoint %d \n",
                     pTCDPipe->uPhyEpAddr, 2, 3, 4, 5, 6);

        /* usbPlxTcdDmaChRelease(pPlxCtrl, pTCDPipe->uPhyEpAddr);
         */
        /* Disable the endpoint */

        USB_PLX_REG_WRITE32(pPlxCtrl,
                            USB_PLX_EP_CFG_OFFSET(pTCDPipe->uPhyEpAddr),
                            0);

        /* Clear all the status */

        USB_PLX_REG_WRITE32(pPlxCtrl,
                            USB_PLX_EP_STAT_OFFSET(pTCDPipe->uPhyEpAddr),
                            0xFFFFFFFF);

        /* Disable all the interrupt */

        USB_PLX_REG_WRITE32(pPlxCtrl,
                            USB_PLX_EP_IRQENB_OFFSET(pTCDPipe->uPhyEpAddr),
                            0);
        /*
         * Clear all the interrupt status
         * Bit 31 - DMA completion sequence error status
         * Bit 27 - DMA abort done interrupt
         * Bit 26 - DMA pause done interrupt
         * Bit 25 - DMA last descriptor done interrupt
         * Bit 24 - DMA transfer done interrupt
         *
         * 0x8F00 0000 will be write to clear the DMA status register
         */

        USB_PLX_REG_WRITE32(pPlxCtrl,
                            USB_PLX_DMASTAT_OFFSET(pTCDPipe->uPhyEpAddr),
                            0x8F000000);

        /* Clear the DMA control register */

        USB_PLX_REG_WRITE32(pPlxCtrl,
                            USB_PLX_DMACTL_OFFSET(pTCDPipe->uPhyEpAddr),
                            0);

        /* Clear the DMA count register */

        USB_PLX_REG_WRITE32(pPlxCtrl,
                            USB_PLX_DMACOUNT_OFFSET(pTCDPipe->uPhyEpAddr),
                            0);

        /* Mark the phy endpoint value as invalid */

        pTCDPipe->uPhyEpAddr = USB_ENDPOINT_MAX_COUNT;
        }

    OS_RELEASE_EVENT(pTCDData->tcdSyncMutex);

    USB_PLX_VDBG("usbPlxTcdPipePhyEpRelease(): Ep %d has been re to pipe %p\n",
                 pTCDPipe->uPhyEpAddr, pTCDPipe, 3, 4, 5, 6);

    return OK;
    }

/*******************************************************************************
*
* usbPlxTcdEpRegRead - read the configurable endpoint registers
*
* This routine is used to read the configurable endpoint registers of the ep
* index.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbPlxTcdEpRegRead
    (
    pUSB_PLX_CTRL pPlxCtrl,
    UINT8         uEpIndex
    )
    {
    /* Parameter verification */

    if ((NULL == pPlxCtrl) ||
        (uEpIndex > pPlxCtrl->uMaxEpNum))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    "pPlxCtrl", 2, 3, 4, 5, 6);

        return;
        }

    /* Get the endpoint specific configure registers */

    pPlxCtrl->epReg[uEpIndex].uEpCfg  = USB_PLX_REG_READ32 (pPlxCtrl,
                                USB_PLX_EP_CFG_OFFSET(uEpIndex));

    pPlxCtrl->epReg[uEpIndex].uEpRsp = USB_PLX_REG_READ32 (pPlxCtrl,
                                USB_PLX_EP_RSP_OFFSET(uEpIndex));

    pPlxCtrl->epReg[uEpIndex].uEpStat = USB_PLX_REG_READ32 (pPlxCtrl,
                                USB_PLX_EP_STAT_OFFSET(uEpIndex));

    pPlxCtrl->epReg[uEpIndex].uEpAvail = USB_PLX_REG_READ32 (pPlxCtrl,
                                USB_PLX_EP_AVAIL_OFFSET(uEpIndex));

    pPlxCtrl->epReg[uEpIndex].uEpIrqEnb = USB_PLX_REG_READ32 (pPlxCtrl,
                                USB_PLX_EP_IRQENB_OFFSET(uEpIndex));

    USB_PLX_VDBG("usbPlxTcdEpRegRead(): uEpRsp 0x%X uEpStat 0x%X "
                 "uEpAlail 0x%X uEpCfg %x of uEpIrqEnb %x uEpIndex %d\n",
                  pPlxCtrl->epReg[uEpIndex].uEpRsp,
                  pPlxCtrl->epReg[uEpIndex].uEpStat,
                  pPlxCtrl->epReg[uEpIndex].uEpAvail,
                  pPlxCtrl->epReg[uEpIndex].uEpCfg,
                  pPlxCtrl->epReg[uEpIndex].uEpIrqEnb,
                  uEpIndex);

    return;
    }


/*******************************************************************************
*
* usbPlxTcdBusSpeedGet - get the hardware speed
*
* This routine is used to get the hardware speed.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

UINT8 usbPlxTcdBusSpeedGet
    (
    pUSB_PLX_CTRL pPlxCtrl
    )
    {
    UINT32 uUsbStat;
    UINT8  uSpeed = 0xFF;

    /* Parameter verification */

    if (NULL == pPlxCtrl)
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    "pPlxCtrl", 2, 3, 4, 5, 6);

        return uSpeed;
        }

    uUsbStat = USB_PLX_REG_READ32 (pPlxCtrl, USB_PLX_USBSTAT_REG);

    /* Determine the hardware speed */

    if ((uUsbStat & USB_PLX_USBSTAT_HS) != 0)
        {
        /* Device controller is operating in high Speed */

        USB_PLX_VDBG("Hight speed detected \n", 1, 2, 3, 4, 5, 6);

        uSpeed = USB_SPEED_HIGH;
        }
    else if ((uUsbStat & USB_PLX_USBSTAT_FS) != 0)
        {
        /* Device controller is operating in full Speed */

        USB_PLX_VDBG("Full speed detected\n", 1, 2, 3, 4, 5, 6);

        uSpeed = USB_SPEED_FULL;
        }
    else if ((uUsbStat & USB_PLX_USBSTAT_SS) != 0)
        {
        /* Device controller is operating in super Speed */

        USB_PLX_VDBG("Susper speed detected\n", 1, 2, 3, 4, 5, 6);

        uSpeed = USB_SPEED_SUPER;
        }
    else
        {
        USB_PLX_VDBG("Unknown speed of the hardware, with uUsbStat 0x%08X\n",
                     uUsbStat, 2, 3, 4, 5, 6);
        }

    return uSpeed;
    }


/*******************************************************************************
*
* usbPlxTcdEp0RequestProcess - start the pipe process
*
* This routine is used to start the pipe process, such as get the data from
* USB host or issue data to the USB host.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbPlxTcdEp0RequestProcess
    (
    pUSB_PLX_TCD_DATA    pTCDData,
    pUSB_PLX_TCD_PIPE    pTCDPipe,
    pUSB_PLX_TCD_REQUEST pRequest
    )
    {
    pUSB_PLX_CTRL pPlxCtrl = NULL;
    pUSBTGT_TCD   pTcd = NULL;
    USB_SETUP *   pSetup = NULL;
    UINT32        uSetup0123;
    UINT32        uSetup4567;

    /* Parameter verification */

    if ((NULL == pTCDData) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)) ||
        (NULL == (pTcd = pTCDData->pTcd)) ||
        (NULL == pTCDPipe) ||
        (NULL == pRequest))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTCDData) ? "pTCDData" :
                    (NULL == pTcd) ? "pTcd" :
                     (NULL == pPlxCtrl) ? "pPlxCtrl" :
                     (NULL == pTCDPipe) ? "pTCDPipe" :
                    "pRequest"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Start the request processing */

    USB_PLX_VDBG("usbPlxTcdEp0RequestProcess pipe %p with pRequest %p ep0Stage %d uPid 0x%X\n",
                 pTCDPipe, pRequest, pTCDData->ep0Stage, pRequest->uPid, 5, 6);

    /* For EP0 */

    switch (pTCDData->ep0Stage)
        {
        case USB_TCD_EP0_STAGE_SETUP_PEND:
            {
            /* One setup packet received, read the data */

            USB_PLX_VDBG ("usbPlxTcdEp0RequestProcess() "
                          "USB_TCD_EP0_STAGE_SETUP_PEND\n", 1,2,3,4,5,6);

            if (USB_PID_SETUP == pRequest->uPid)
                {
                pSetup = (USB_SETUP *)pRequest->pCurrentBuffer;

                /* Read the setup packet */

                uSetup0123 = USB_PLX_REG_READ32(pPlxCtrl,
                                                USB_PLX_SETUP0123_REG);

                *(UINT32 *)pRequest->pCurrentBuffer =
                             OS_UINT32_LE_TO_CPU(uSetup0123);

                pRequest->uActLength += 4;
                pRequest->pCurrentBuffer += pRequest->uActLength;

                uSetup4567 = USB_PLX_REG_READ32(pPlxCtrl,
                                                USB_PLX_SETUP4567_REG);

                *(UINT32 *)pRequest->pCurrentBuffer =
                             OS_UINT32_LE_TO_CPU(uSetup4567 );

                pRequest->uActLength += 4;

                USB_PLX_VDBG("usbPlxTcdEp0RequestProcess(): Setup PKT "
                              "[0x%02X 0x%02X 0x%04X 0x%04X 0x%04X]\n",
                              pSetup->requestType,
                              pSetup->request,
                              pSetup->value,
                              pSetup->index,
                              pSetup->length, 6);

                /* Update the Ep0 status */

                if (pSetup->length)
                    {
                    if (pSetup->requestType & USB_RT_DEV_TO_HOST)
                        {
                        /* Data needed from the device to the Host */

                        pTCDData->ep0Stage = USB_TCD_EP0_STAGE_DATA_IN;
                        }
                    else
                        {
                        pTCDData->ep0Stage = USB_TCD_EP0_STAGE_DATA_OUT;
                        }
                    }
                else
                    {
                    pTCDData->ep0Stage = USB_TCD_EP0_STAGE_STATUS_IN;
                    if (USB_REQ_SET_ADDRESS == pSetup->request)
                        {
                        pTcd->uAddrToSet = (UINT8)pSetup->value | 0x80;
                        }
                    }

                USB_PLX_REG_WRITE32 (pPlxCtrl, USB_PLX_EP_STAT_OFFSET(0),
                                     pPlxCtrl->epReg[0].uEpStat );

                USB_PLX_VDBG("usbPlxTcdEp0RequestProcess ep0Stage %d\n",
                             pTCDData->ep0Stage, 2, 3, 4, 5, 6);

                /* Complete the request */

                (void) usbPlxTcdRequestComplete (pRequest,
                                             S_usbTgtTcdLib_ERP_SUCCESS);

                /* Ack the setup packet */

                }  /* if (USB_PID_SETUP ... */
            else
                {
                /* The status is not match, callback */

                (void) usbPlxTcdRequestComplete (pRequest,
                                             S_usbTgtTcdLib_ERP_RESTART);

                }
            }
            break;
        case USB_TCD_EP0_STAGE_DATA_IN:
            {
            if (USB_PID_IN == pRequest->uPid)
                {
                /* Write the data to the endpoint FIFO */

                USB_PLX_VDBG("usbPlxTcdEp0RequestProcess(): uXferSize 0x%X ack "
                              "0x%X maxpkt 0x%X\n",
                              pRequest->uXferSize, pRequest->uActLength,
                              pTCDPipe->uMaxPacketSize, 4, 5, 6);

                pRequest->uCurXferLength = min (pTCDPipe->uMaxPacketSize,
                           pRequest->uXferSize - pRequest->uActLength);

                /*
                 * From the Data book
                 * Regiser 15-59. 300h EP_CFG Endpoint Configuration for EP 0
                 * Bit 7, Endpoint Direction
                 * Selects the endpoint direction. The direction is with
                 * respect with the USB Host point of view. This bit is
                 * _automatically changed, according to the direction specified
                 * in the setup packet.
                 *
                 * But, internal testing on 3380 AA version chipset.
                 * Sometimes, this bit not update correctly according to the
                 * direction specified in the setup packet. If the direction
                 * is not correct, no data will be write to the FIFO in DATA
                 * IN stage.
                 *
                 * Set the direction IN manually for DATA IN process.
                 */

                USB_PLX_REG_WRITE32(pPlxCtrl,
                                    USB_PLX_EP_CFG_OFFSET (0),
                                    (0x01 << USB_PLX_EP_CFG_DIR_SHIFT));

                /* Write to the FIFO */

                usbPlxTcdFIFOWrite(pPlxCtrl,
                                   pRequest->pCurrentBuffer,
                                   pRequest->uCurXferLength,
                                   0);

                /*
                 * TODO: need check more
                 * NOTE: for NET2282, when write to the FIFO, the data will be
                 * available, but for PLX3380, the FIFO Valid bit should be set
                 *
                 * if (pPlxCtrl->uDeviceId ==  USB_PLX_DEVICE_ID_3380)
                 *     USB_PLX_REG_WRITE32 (pPlxCtrl,
                 *                          USB_PLX_EP_STAT_OFFSET (0),
                 *                          0x100);
                 */

                }
            else
                {
                if (USB_PID_SETUP == pRequest->uPid)
                    {
                    pTCDData->ep0Stage = USB_TCD_EP0_STAGE_SETUP;
                    }
                }
            }
            break;
        case USB_TCD_EP0_STAGE_STATUS_IN:
            {
            if (USB_PID_IN == pRequest->uPid)
                {
                /* Write the data to the endpoint FIFO */

                USB_PLX_DBG("usbPlxTcdEp0RequestProcess(): STATUS_IN uXferSize 0x%X ack 0x%X stat %x\n",
                              pRequest->uXferSize, pRequest->uActLength,
                              pPlxCtrl->epReg[0].uEpStat, 4, 5, 6);

                }
            }
            break;
        case USB_TCD_EP0_STAGE_DATA_OUT_PEND:
             {
             if ((USB_PID_OUT == pRequest->uPid) &&
                 ((pPlxCtrl->epReg[0].uEpStat & USB_PLX_EP_STAT_FIFOVC) ||
                  (pPlxCtrl->epReg[0].uEpAvail != 0x40) ||
                  (pPlxCtrl->epReg[0].uEpStat & USB_PLX_EP_STAT_FIFO_FULL)))
                 {
                 /* Read the data from the endpoint FIFO */

                 USB_PLX_VDBG("usbPlxTcdEp0RequestProcess(): uXferSize 0x%X "
                             "ack 0x%X maxpkt 0x%X stat %x\n",
                             pRequest->uXferSize, pRequest->uActLength,
                             pTCDPipe->uMaxPacketSize,
                             pPlxCtrl->epReg[0].uEpStat, 5, 6);

                 pRequest->uCurXferLength = usbPlxTcdFIFORead(pPlxCtrl,
                                   pRequest->pCurrentBuffer,
                                   pRequest->uXferSize - pRequest->uActLength,
                                   0);

                 USB_PLX_REG_WRITE32 (pPlxCtrl,
                      USB_PLX_EP_STAT_OFFSET(0),
                      pPlxCtrl->epReg[0].uEpStat);

                 pRequest->uActLength += pRequest->uCurXferLength;
                 pRequest->pCurrentBuffer = pRequest->pCurrentBuffer +
                                           pRequest->uCurXferLength;

                 if ((pRequest->uCurXferLength < pTCDPipe->uMaxPacketSize) ||
                     (pRequest->uActLength == pRequest->uXferSize))
                     {
                     pTCDData->ep0Stage = USB_TCD_EP0_STAGE_STATUS_IN;
                     (void) usbPlxTcdRequestComplete (pRequest,
                                                  S_usbTgtTcdLib_ERP_SUCCESS);
                     }
                }
             }
            break;
        case USB_TCD_EP0_STAGE_STATUS_OUT_PEND:
            {
             pTCDData->ep0Stage = USB_TCD_EP0_STAGE_SETUP;
             (void) usbPlxTcdRequestComplete (pRequest,
                                          S_usbTgtTcdLib_ERP_SUCCESS);
            }
            break;
        case USB_TCD_EP0_STAGE_IDLE:
            /* Following through */
        default:
            break;
        }

    return OK;
    }

/*******************************************************************************
*
* usbPlxTcdTxRequestProcess - start the pipe process
*
* This routine is used to start the pipe process, such as get the data from
* USB host or issue data to the USB host.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbPlxTcdTxRequestProcess
    (
    pUSB_PLX_TCD_DATA    pTCDData,
    pUSB_PLX_TCD_PIPE    pTCDPipe,
    pUSB_PLX_TCD_REQUEST pRequest
    )
    {
    pUSB_PLX_CTRL pPlxCtrl = NULL;
    STATUS        status;

    /* Parameter verification */

    if ((NULL == pTCDData) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)) ||
        (NULL == pTCDPipe) ||
        (NULL == pRequest))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTCDData) ? "pTCDData" :
                     (NULL == pPlxCtrl) ? "pPlxCtrl" :
                     (NULL == pTCDPipe) ? "pTCDPipe" :
                    "pRequest"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Start the request processing */

    USB_PLX_VDBG("usbPlxTcdTxRequestProcess() pipe %p pRequest %p ep0Stage %d\n",
                 pTCDPipe, pRequest, pTCDData->ep0Stage,
                 4, 5, 6);

    /* A valid channel indicate the DMA is enabled */

    pTCDPipe->uDmaChannel = pTCDPipe->uPhyEpAddr;

    /*
     * Using DMA if the transfer length > USB_PLX_DMA_DOOR_KEEPER
     */

    if ((pTCDPipe->uDmaChannel != USB_PLX_DMA_CH_IVALID) &&
        ((ULONG)pRequest->pCurrentBuffer % 4 == 0) &&
        (pRequest->uXferSize > USB_PLX_DMA_DOOR_KEEPER))
        {

        pRequest->uCurXferLength =
                   pRequest->uXferSize - pRequest->uActLength;

        if ((NULL != pRequest->pScatDesc) &&
            (NULL == pRequest->pCurrentBuffer) &&
            (pTCDData->uDmaType == USB_PLX_SG_DMA))
            {

            cacheFlush(DATA_CACHE,
                       (void *)(pRequest->pScatDesc),
                       pRequest->uErpBufCount * sizeof (USB_PLX_SCAT_DMA_DESC));
            }
        else
            {
            /* Flush the DMA Buffer */

            cacheFlush(DATA_CACHE,
                       (void *)(pRequest->pCurrentBuffer),
                       pRequest->uCurXferLength);

            USB_PLX_VDBG ("usbPlxTcdTxRequestProcess Dma start pCurrentBuffer %x\n",
                           pRequest->pCurrentBuffer,2,3,4,5,6);
            }

        /* Start DMA */

         status = usbPlxTcdDmaStart(pTCDData, pTCDPipe, pRequest);

         if (OK == status)
            {
            /* DMA used return */

            return OK;
            }

        }

        {
        /* Enable the PIO interrupt */

        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_EP_IRQENB_OFFSET(pTCDPipe->uPhyEpAddr),
                             USB_PLX_EP_IRQENB_DPT);

        USB_PLX_REG_BIT_SET(pPlxCtrl,
                            USB_PLX_PCIIRQENB0_REG,
                            USB_PLX_XIRQENB0_EP(pTCDPipe->uPhyEpAddr));

        pRequest->uCurXferLength = min (pTCDPipe->uMaxPacketSize,
                   pRequest->uXferSize - pRequest->uActLength);

        USB_PLX_VDBG ("usbPlxTcdTxRequestProcess PIO start pCurrentBuffer %x\n",
                       pRequest->uCurXferLength,
                       pRequest->uXferSize,3,4,5,6);

        /* Write the data to the endpoint FIFO */

        usbPlxTcdFIFOWrite(pPlxCtrl,
                           pRequest->pCurrentBuffer,
                           pRequest->uCurXferLength,
                           pTCDPipe->uPhyEpAddr);

        }

    return OK;
    }

/*******************************************************************************
*
* usbPlxTcdRxRequestProcess - start the Rx request process
*
* This routine is used to start Rx request process to issue the request to get
* the data from the USB HOST.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbPlxTcdRxRequestProcess
    (
    pUSB_PLX_TCD_DATA    pTCDData,
    pUSB_PLX_TCD_PIPE    pTCDPipe,
    pUSB_PLX_TCD_REQUEST pRequest
    )
    {
    pUSB_PLX_CTRL pPlxCtrl = NULL;
    UINT32        uEpSata;
    UINT32        uEpAvail;

    /* Parameter verification */

    if ((NULL == pTCDData) ||
        (NULL == (pPlxCtrl = pTCDData->pPlxCtrl)) ||
        (NULL == pTCDPipe) ||
        (NULL == pRequest))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTCDData) ? "pTCDData" :
                     (NULL == pPlxCtrl) ? "pPlxCtrl" :
                     (NULL == pTCDPipe) ? "pTCDPipe" :
                    "pRequest"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * The TCD set this bit(USBTGT_PIPE_STATUS_DATA_PENDING) when
     * the controller get one RX interrupt, but no find one
     * valid request.
     */

    if (pTCDPipe->status & USBTGT_PIPE_STATUS_DATA_PENDING)
        {
        pTCDPipe->status &= ~ USBTGT_PIPE_STATUS_DATA_PENDING;

        /* Get the current endpoint status */

        uEpSata = USB_PLX_REG_READ32 (pPlxCtrl,
                             USB_PLX_EP_STAT_OFFSET(pTCDPipe->uPhyEpAddr));

        uEpAvail = USB_PLX_REG_READ32 (pPlxCtrl,
                             USB_PLX_EP_AVAIL_OFFSET(pTCDPipe->uPhyEpAddr));

        USB_PLX_VDBG ("usbPlxTcdRxRequestProcess() FIFO Avail 0x%x bytes\n",
                      uEpAvail, 2, 3, 4, 5, 6);

        /* Use PIO to read data in the FIFO */

        pRequest->uCurXferLength = usbPlxTcdFIFORead(pPlxCtrl,
                          pRequest->pCurrentBuffer,
                          pRequest->uXferSize - pRequest->uActLength,
                          pTCDPipe->uPhyEpAddr);

        /* Update the act data length */

        pRequest->uActLength += pRequest->uCurXferLength;
        pRequest->pCurrentBuffer = pRequest->pCurrentBuffer +
                                   pRequest->uCurXferLength;

        if ((pRequest->uActLength == 0) ||
            (pRequest->uActLength % pTCDPipe->uMaxPacketSize) ||
            (pRequest->uActLength == pRequest->uXferSize))
            {
            USB_PLX_VDBG ("usbPlxTcdRxRequestProcess(): complete uActLength %x\n",
                           pRequest->uActLength, 2,3,4,5,6);

            /* Enable the DPR interrupt */

            USB_PLX_REG_WRITE32 (pPlxCtrl,
                                 USB_PLX_EP_IRQENB_OFFSET(pTCDPipe->uPhyEpAddr),
                                 USB_PLX_EP_IRQENB_DPR);

            USB_PLX_REG_BIT_SET(pPlxCtrl,
                                USB_PLX_PCIIRQENB0_REG,
                                USB_PLX_XIRQENB0_EP(pTCDPipe->uPhyEpAddr));

            /* Clean the status */

            USB_PLX_REG_WRITE32 (pPlxCtrl,
                     USB_PLX_EP_STAT_OFFSET(pTCDPipe->uPhyEpAddr),
                     uEpSata);

            /* Complete the request */

            (void) usbPlxTcdRequestComplete (pRequest,
                                         S_usbTgtTcdLib_ERP_SUCCESS);
            }

        }
   else
        {
        /*
         * Do not eanble the DMA before get the exist data length 0627
         * Need verify on the AB version
         */

         /*
          *        pRequest->uCurXferLength =
          *         pRequest->uXferSize - pRequest->uActLength;
          *
          * usbPlxTcdRxDmaPrepare (pTCDData, pTCDPipe, pRequest);
          */

        if (pTCDData->isAlaysShortPkt == FALSE)
            {
            USB_PLX_DBG("usbPlxTcdRxRequestProcess pipe %p with pRequest %p "
                        "uCurXferLength %x\n",
                         pTCDPipe, pRequest,
                         pRequest->uCurXferLength, 4, 5, 6);

            pRequest->uCurXferLength =
                pRequest->uXferSize - pRequest->uActLength;

            /* Start DMA */

            (void)usbPlxTcdDmaStart (pTCDData, pTCDPipe, pRequest);
            }
        else
            {
            USB_PLX_REG_WRITE32 (pPlxCtrl,
                                 USB_PLX_EP_IRQENB_OFFSET(pTCDPipe->uPhyEpAddr),
                                 USB_PLX_EP_IRQENB_DPR);
            }

        }

    /* Start the request processing */

    USB_PLX_VDBG("usbPlxTcdRxRequestProcess pipe %p with pRequest %p\n",
                 pTCDPipe, pRequest, 3, 4, 5, 6);

    return OK;
    }

/*******************************************************************************
*
* usbPlxTcdPipeProcessStart - start to process the pipe
*
* This routine is used to start to process the pipe, such as get the data from
* USB host or issue data to the USB host.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbPlxTcdPipeProcessStart
    (
    pUSB_PLX_TCD_DATA    pTCDData,
    pUSB_PLX_TCD_PIPE    pTCDPipe
    )
    {
    pUSB_PLX_TCD_REQUEST pRequest = NULL;

    /* Validate parameters */

    if ((NULL == pTCDData) ||
        (NULL == pTCDPipe))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTCDData) ? "pTCDData" :
                    "pTCDPipe"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    USB_PLX_VDBG("usbPlxTcdPipeProcessStart 0 pipe %p uEpAddr %x uEpType %x EpDir %x stat %x\n",
                 pTCDPipe, pTCDPipe->uEpAddr, pTCDPipe->uEpType, pTCDPipe->uEpDir, pTCDPipe->status, 6);

    /* Get the first request */

    pRequest = usbPlxTcdFirstPendingReqGet(pTCDPipe);

    if (NULL == pRequest)
        {
        USB_PLX_WARN("No pending request on this pipe Index %x\n",
                     pTCDPipe->uPhyEpAddr, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* The pipe status update to processing */

    OS_WAIT_FOR_EVENT(pTCDPipe->pPipeSyncMutex, WAIT_FOREVER);

    if (pTCDPipe->status & USBTGT_PIPE_STATUS_PROCESSING)
        {
        /* Release the event */

        OS_RELEASE_EVENT(pTCDPipe->pPipeSyncMutex);

        return OK;
        }

    /* Update the pipe status */

    pTCDPipe->status |= USBTGT_PIPE_STATUS_PROCESSING;

    USB_PLX_VDBG("usbPlxTcdPipeProcessStart uEpAddr %x uEpType %x EpDir %x \n",
                 pTCDPipe->uEpAddr, pTCDPipe->uEpType, pTCDPipe->uEpDir,
                 4, 5, 6);

    /* Release the mutex */

    OS_RELEASE_EVENT(pTCDPipe->pPipeSyncMutex);

    USB_PLX_VDBG("usbPlxTcdPipeProcessStart pipe %p pRequest %p\n",
                 pTCDPipe, pRequest, 3, 4, 5, 6);

    if (pTCDPipe->uEpType == USB_ATTR_CONTROL)
        {
        /* The control request */

        usbPlxTcdEp0RequestProcess (pTCDData, pTCDPipe, pRequest);
        }
    else
        {
        if (USB_ENDPOINT_IN == pTCDPipe->uEpDir)
            {
            /* Tx request */

            usbPlxTcdTxRequestProcess (pTCDData, pTCDPipe, pRequest);
            }
        else
            {
            /* Rx request */

            usbPlxTcdRxRequestProcess (pTCDData, pTCDPipe, pRequest);
            }
        }

    return OK;
    }


/*******************************************************************************
*
* usbPlxTcdSchedule - schedule the TCD for transfer
*
* This routine is used to schedule the TCD for transfer.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbPlxTcdSchedule
    (
    pUSB_PLX_TCD_DATA pTCDData
    )
    {
    pUSB_PLX_TCD_PIPE pTCDPipe = NULL;
    NODE *            pNode = NULL;

    /* Validate parameters */

    if (NULL == pTCDData)
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    "pTCDData", 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Get the first pipe */

    pNode = lstFirst(&(pTCDData->tcdPipeList));

    while (NULL != pNode)
        {
        pTCDPipe = USB_PLX_TCD_PIPE_CONV_FROM_NODE(pNode);

        pNode = lstNext(pNode);

        USB_PLX_VDBG("usbPlxTcdSchedule pipe %p status 0x%X\n",
                     pTCDPipe, pTCDPipe->status, 3, 4, 5, 6);

        if ((0 == (USBTGT_PIPE_STATUS_DELETING & pTCDPipe->status)) &&
            (0 == (USBTGT_PIPE_STATUS_PROCESSING & pTCDPipe->status)))
            {
            /* This pipe is valid, and not be deleting, schedule it */

             (void )usbPlxTcdPipeProcessStart(pTCDData, pTCDPipe);
            }
        }

    return OK;
    }

/*******************************************************************************
*
* usbPlxTcdPipeFlush - flush the pipe to complete all requests
*
* This routine is to complete all requests on the pipe with the given status.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbPlxTcdPipeFlush
    (
    pUSB_PLX_TCD_PIPE pTCDPipe,
    int               status
    )
    {
    pUSB_PLX_TCD_REQUEST pRequest = NULL;
    NODE *               pNode = NULL;

    /* Validate parameters */

    if (NULL == pTCDPipe)
      {
      USB_PLX_ERR("Invalid parameter %s is NULL\n",
                  "pTCDPipe", 2, 3, 4, 5, 6);

      return;
      }

    /* Complete all the request, add request into freeRequest list */

    pNode = lstFirst(&pTCDPipe->pendingReqList);

    while (pNode != NULL)
        {
        pRequest = USB_PLX_TCD_REQUEST_CONV_FROM_NODE(pNode);

        if (pRequest != NULL)
            {
            USB_PLX_VDBG("usbPlxTcdPipeFlush(): pRequest %p pTCDPipe %p status 0x%X\n",
                         pRequest, pRequest->pTCDPipe, status, 4, 5, 6);

            /* Complete all the request */

            usbPlxTcdRequestComplete(pRequest, status);
            }

        pNode = lstNext(pNode);
        }

    /* Free request in the freeRequest list */

    pNode = lstFirst(&pTCDPipe->freeReqList);

    while (pNode != NULL)
        {
        pRequest = USB_PLX_TCD_REQUEST_CONV_FROM_NODE(pNode);

        pNode = lstNext(pNode);

        /* Destroy the request resource */

        if (NULL != pRequest)
            {
            usbPlxTcdRequestDestroy (pTCDPipe, pRequest);
            }
        }

    USB_PLX_DBG("usbPlxTcdPipeFlush(): Flush all requests of this pipe\n",
                1, 2, 3, 4, 5, 6);

    return;
    }

/*******************************************************************************
*
* usbPlxTcdDisableDataEps - disable all data endpoints
*
* This routine is used to disable all data endpoints. It used as the workaround
* for USB3380AB silicon LGO_Ux rejection issue. It undoes the earlier phase
* of the workaround effectively returning things back to normal. It is called
* (and must be called) during the host's first Control Read before the device
* responds to the Data Phase of the Control Read to prevent data EPs
* from corrupting EP0's Data Phase content.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbPlxTcdDisableDataEps
    (
    pUSB_PLX_CTRL pPlxCtrl
    )
    {
    UINT8 uIndex;
    UINT8 uEpSel;

    /* Parameter verification */

    if (NULL == pPlxCtrl)
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    "pPlxCtrl", 2, 3, 4, 5, 6);

        return;
        }

    USB_PLX_VDBG("usbPlxTcdDisableDataEps() Disable all endpoints \n",
                 1, 2, 3, 4, 5, 6);

    for (uIndex = USB_PLX_ENDPT_A; uIndex <= USB_PLX_ENDPT_D; uIndex ++)
        {
        /* Disable the endpoint */

        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_EP_CFG_OFFSET(uIndex),
                             0);
        }

    /* Disable the dedicated endpoints */

    USB_PLX_REG_WRITE32 (pPlxCtrl,
                         USB_PLX_DEP_CFGOUT_REG_BASE +
                         USB_PLX_DEP_CFG_OFFSET,
                         0);
    USB_PLX_REG_WRITE32 (pPlxCtrl,
                         USB_PLX_DEP_CFGIN_REG_BASE +
                         USB_PLX_DEP_CFG_OFFSET,
                         0);
    USB_PLX_REG_WRITE32 (pPlxCtrl,
                         USB_PLX_DEP_PCIOUT_REG_BASE +
                         USB_PLX_DEP_CFG_OFFSET,
                         0);
    USB_PLX_REG_WRITE32 (pPlxCtrl,
                         USB_PLX_DEP_PCIIN_REG_BASE +
                         USB_PLX_DEP_CFG_OFFSET,
                         0);
    USB_PLX_REG_WRITE32 (pPlxCtrl,
                         USB_PLX_DEP_STATIN_REG_BASE +
                         USB_PLX_DEP_CFG_OFFSET,
                         0);
    USB_PLX_REG_WRITE32 (pPlxCtrl,
                         USB_PLX_DEP_RCIN_REG_BASE +
                         USB_PLX_DEP_CFG_OFFSET,
                         0);

    for (uEpSel = 0; uEpSel <= 21; uEpSel++)
        {
        /* For all IN and OUT EPs except EP0 */
        /*
         * USB_PLX_REG_WRITE32 (pPlxCtrl,
         *                    USB_PLX_PL_EP_CTRL,
         *                    (USB_PLX_REG_READ32 (pPlxCtrl,
         *                     USB_PLX_PL_EP_CTRL) &
         *                    USB_PLX_PL_EP_CTRL_ENDPOINT_SELECT_MASK) |
         *                    uEpSel);
         *
         * Do not touch other part of the register
         */

        *(UINT8 *)(pPlxCtrl->uRegBase[0] + USB_PLX_PL_EP_CTRL) = uEpSel;

        /* Change settings on some selected endpoints */

        switch (uEpSel)
            {
            case 2:
            /* GPEP0 OUT, fall through */
            case 3:
            /* GPEP0 IN, fall through */
            case 4:
            /* GPEP1 OUT, fall through */
            case 5:
            /* GPEP1 IN, fall through */
            case 6:
            /* GPEP2 OUT, fall through */
            case 7:
            /* GPEP2 IN, fall through */
            case 8:
            /* GPEP3 OUT, fall through */
            case 9:
            /* GPEP3 IN, fall through */
            case 14:
            /* CSROUT, fall through */
            case 15:
            /* CSRIN, fall through */
            case 16:
            /* PCIOUT, fall through */
            case 17:
            /* PCIIN, fall through */
            case 19:
            /* STATIN, fall through */
            case 21:
            /* RCIN */
                {
                USB_PLX_REG_WRITE32 (pPlxCtrl,
                         USB_PLX_PL_EP_CFG_4,
                         USB_PLX_REG_READ32 (pPlxCtrl,
                         USB_PLX_PL_EP_CFG_4) &
                         ~ USB_PLX_PL_EP_CFG_4_NON_CTRL_IN_TOLERATE_BAD_DIR);

                USB_PLX_REG_WRITE32 (pPlxCtrl,
                                     USB_PLX_PL_EP_CTRL,
                                     USB_PLX_REG_READ32 (pPlxCtrl,
                                     USB_PLX_PL_EP_CTRL) |
                                     USB_PLX_PL_EP_CTRL_EP_INITIALIZED);
                }
                break;
            default:
                /* Nothing here */
                break;
            }
        }

    USB_PLX_VDBG("usbPlxTcdDisableDataEps() : Done\n",
                 1, 2, 3, 4, 5, 6);

    return;
    }

/*******************************************************************************
*
* usbPlxTcdEnableDataEpsAsZero - enable all data endpoints as zero
*
* This routine is used to enable all data endpoints as zero. It used as the
* workaround for USB3380AB silicon LGO_Ux rejection issue. It enable all of the
* chip's data EPs, set them to respond to requests on USB EP0.
*
* NOTE:
* - Called before the host's first Control Read, to program all of
*   the chip's EPs to respond to EP0. Once prepared, the ACK of
*   an SS host's first Control Read performs the actual workaround.
* - Called before firmware enables the USB connection (USB_DETECT_ENABLE
*   in USBCTL set to TRUE) OR attachment to live host detected
*   (USB_DETECT_ENABLE is TRUE when VBUS_PIN in USBCTL transitions to TRUE.)
* - Later, another phase of the workaround 'undoes' this phase.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbPlxTcdEnableDataEpsAsZero
    (
    pUSB_PLX_CTRL pPlxCtrl
    )
    {
    UINT32 uScratch;
    UINT32 uFsmValue;
    UINT32 uEpCfg;
    UINT8  uEpSel;

    /* Parameter verification */

    if (NULL == pPlxCtrl)
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    "pPlxCtrl", 2, 3, 4, 5, 6);

        return;
        }

    /* If not 3380/3382 silicon return */

    if (((pPlxCtrl->uDeviceId != USB_PLX_DEVICE_ID_3380) &&
         (pPlxCtrl->uDeviceId != USB_PLX_DEVICE_ID_3382)))
        {
        USB_PLX_DBG("Not USB 3380/2 AB silicon Return\n",
                    1, 2, 3, 4, 5, 6);

        return;
        }

    USB_PLX_REG_WRITE32(pPlxCtrl,
                        USB_PLX_IDXADDR_REG,
                        USB_PLX_SCRATCH_IDX);

    uScratch = USB_PLX_REG_READ32 (pPlxCtrl,
                         USB_PLX_IDXDATA_REG);

    uFsmValue = uScratch & (UINT32)USB_PLX_LGO_UX_FSM_RESET;

    uScratch &= ~(USB_PLX_LGO_UX_FSM_RESET);

    /* See if firmware needs to set up for workaround: */

    if (uFsmValue != USB_PLX_LGO_UX_FSM_SS_CONTROL_READ)
        {
        /*
         * The defect workaround has not been applied to SS connection:
         *  - Apply first phase of LGO_Ux rejection workaround.
         *  - Enable and program data EPs for USB EP zero (and more).
         *  - The goal is for all data EPs to internally
         *    respond to the host's 'Start of Data Phase' of
         *    its first Control Read. (SS hosts issue an ACK
         *    to start the data phase.) The SS host's ACK clears
         *    problematic flip flops related to LGO_Ux request
         *    rejection in each data EP.
         */

        uEpCfg = ((0 & USB_PLX_EP_CFG_NUMBER) |
                  (USB_ENDPOINT_IN & USB_PLX_EP_CFG_DIRECTION) |
                  (USB_PLX_EP_CFG_TYPE_BULK) |
                  (USB_PLX_EP_CFG_ENABLE));

        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_EP_CFG_OFFSET(USB_PLX_ENDPT_A),
                             uEpCfg);
        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_EP_CFG_OFFSET(USB_PLX_ENDPT_B),
                             uEpCfg);
        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_EP_CFG_OFFSET(USB_PLX_ENDPT_C),
                             uEpCfg);
        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_EP_CFG_OFFSET(USB_PLX_ENDPT_D),
                             uEpCfg);

        /* CSRIN, PCIIN, STATIN, RCIN */

        uEpCfg = ((0 & USB_PLX_EP_CFG_NUMBER) |
                  (USB_PLX_EP_CFG_ENABLE));

        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_DEP_CFGIN_REG_BASE +
                             USB_PLX_DEP_CFG_OFFSET,
                             uEpCfg);
        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_DEP_PCIIN_REG_BASE +
                             USB_PLX_DEP_CFG_OFFSET,
                             uEpCfg);
        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_DEP_STATIN_REG_BASE +
                             USB_PLX_DEP_CFG_OFFSET,
                             uEpCfg);
        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_DEP_RCIN_REG_BASE +
                             USB_PLX_DEP_CFG_OFFSET,
                             uEpCfg);

        for (uEpSel = 0; uEpSel <= 21; uEpSel++)
            {
            /* Select an endpoint for subsequent operations: */

            /*
             *
             * USB_PLX_REG_WRITE32 (pPlxCtrl,
             *                     USB_PLX_PL_EP_CTRL,
             *                    (USB_PLX_REG_READ32 (pPlxCtrl,
             *                    USB_PLX_PL_EP_CTRL) &
             *                    USB_PLX_PL_EP_CTRL_ENDPOINT_SELECT_MASK) |
             *                    uEpSel);
             *
             * Do not touch other part of the register
             */

            *(UINT8 *)(pPlxCtrl->uRegBase[0] + USB_PLX_PL_EP_CTRL) = uEpSel;

            /* Change settings on some selected endpoints*/

            switch (uEpSel)
                {
                case 1:
                    /* Ep 0 IN */
                    USB_PLX_REG_WRITE32 (pPlxCtrl,
                                         USB_PLX_PL_EP_CTRL,
                                         USB_PLX_REG_READ32 (pPlxCtrl,
                                         USB_PLX_PL_EP_CTRL) |
                                         USB_PLX_PL_EP_CTRL_CLEAR_ACK_ERROR_CODE |
                                         0);
                    break;
                case 2:
                /* GPEP0 OUT, fall through */
                case 3:
                /* GPEP0 IN, fall through */
                case 4:
                /* GPEP1 OUT, fall through */
                case 5:
                /* GPEP1 IN, fall through */
                case 6:
                /* GPEP2 OUT, fall through */
                case 7:
                /* GPEP2 IN, fall through */
                case 8:
                /* GPEP3 OUT, fall through */
                case 9:
                /* GPEP3 IN, fall through */
                case 14:
                /* CSROUT, fall through */
                case 15:
                /* CSRIN, fall through */
                case 16:
                /* PCIOUT, fall through */
                case 17:
                /* PCIIN, fall through */
                case 19:
                /* STATIN, fall through */
                case 21:
                /* RCIN */
                    {
                    USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_PL_EP_CFG_4,
                             USB_PLX_REG_READ32 (pPlxCtrl,
                             USB_PLX_PL_EP_CFG_4) |
                             USB_PLX_PL_EP_CFG_4_NON_CTRL_IN_TOLERATE_BAD_DIR);

                    USB_PLX_REG_WRITE32 (pPlxCtrl,
                                         USB_PLX_PL_EP_CTRL,
                                         USB_PLX_REG_READ32 (pPlxCtrl,
                                         USB_PLX_PL_EP_CTRL) &
                                         ~ USB_PLX_PL_EP_CTRL_EP_INITIALIZED);
                    }
                    break;
                default:
                    /* Nothing to do */
                    break;
                }
        }

        /* Set FSM to focus on the first Control Read: */

        uScratch |= USB_PLX_LGO_UX_FSM_WAITING_FOR_CONTROL_READ;

        USB_PLX_VDBG ("usbPlxTcdEnableDataEpsAsZero(): wait for control read\n",
                       1, 2,3,4,5,6);

        USB_PLX_REG_WRITE32(pPlxCtrl,
                            USB_PLX_IDXADDR_REG,
                            USB_PLX_SCRATCH_IDX);

        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_IDXDATA_REG,
                             uScratch);

        }
    else
        {
        /*
         * Defect workaround was applied earlier, but not this time:
         * - Remind engineer, as it can be confounding. Workaround is only
         *   applied once, after chip is powered up AND connected to SS.
         */

         USB_PLX_VDBG("usbPlxTcdEnableDataEpsAsZero() : workaround NOT needed "
                      "this time. Cold-reboot and SS connection required\n",
                      1, 2, 3, 4, 5, 6);
        }

    return;
    }

/*******************************************************************************
*
* usbPlxTcdLgoUxWorkaround - the workaround for Lgo_Ux rejection
*
* This routine is used to as the workaround for Lgo_Ux rejection, which need
* be called at the frist control read request before the data stage.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbPlxTcdLgoUxWorkaround
    (
    pUSB_PLX_CTRL pPlxCtrl
    )
    {
    UINT32 uScratch;
    UINT32 uFsmValue;
    UINT32 uSetup0123;
    UINT32 uUsbStat;
    UINT32 uAckWaitTimeout;
    UINT32 uState;

    /* Parameter verification */

    if (NULL == pPlxCtrl)
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    "pPlxCtrl", 2, 3, 4, 5, 6);

        return;
        }

    /* If not 3380/3382 silicon return */

    if (((pPlxCtrl->uDeviceId != USB_PLX_DEVICE_ID_3380) &&
         (pPlxCtrl->uDeviceId != USB_PLX_DEVICE_ID_3382)))
        {
        USB_PLX_DBG("Not USB 3380/2 AB silicon Return\n",
                    1, 2, 3, 4, 5, 6);

        return;
        }

    /* Workaround for LGO_Ux U1/U2 erroneously rejection */

    uSetup0123 = USB_PLX_REG_READ32(pPlxCtrl,
                                    USB_PLX_SETUP0123_REG);

    uUsbStat = USB_PLX_REG_READ32 (pPlxCtrl,
                                  USB_PLX_USBSTAT_REG);

    if (uUsbStat & USB_PLX_USBSTAT_SUSPEND)
        {
        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_USBSTAT_REG,
                             USB_PLX_USBSTAT_SUSPEND);
        }

    USB_PLX_REG_WRITE32(pPlxCtrl,
                        USB_PLX_IDXADDR_REG,
                        USB_PLX_SCRATCH_IDX);

    uScratch = USB_PLX_REG_READ32 (pPlxCtrl,
                         USB_PLX_IDXDATA_REG);

    uFsmValue = (uScratch & (UINT32)USB_PLX_LGO_UX_FSM_RESET);

    uScratch &= ~USB_PLX_LGO_UX_FSM_RESET;

    if ((uFsmValue == USB_PLX_LGO_UX_FSM_WAITING_FOR_CONTROL_READ) &&
        (uSetup0123 & (1 << 7)))

        {
        /* Fist control read request */

        if (uUsbStat & USB_PLX_USBSTAT_SS)
            {
            /*
             * Connection is SS:
             *   - Wait for SS host to start the Data Phase ACK for
             *     its first Control Read.
             *   - Important: The defect condition should be cleared
             *     when the host issues its Data Phase ACK. Upon that ACK,
             *     data EPs can reprogrammed to normal settings (disabled),
             *     and normal EP0 operations can be resumed.
             */

            for (uAckWaitTimeout = 0;
                 uAckWaitTimeout < USB_PLX_LGO_UX_FSM_MAX_WAIT_LOOPS;
                 uAckWaitTimeout++)
                {
               /*
                *  Wait for host's Data Phase ACK:
                *   - USB analyzer traces taken in the lab with various host
                *     and hub combinations show no more than about 300uSec
                *     between host's setup request and its associated data
                *     phase ACK.
                *   - To be safe, assume interrupt overhead time is zero.
                */

                uState = USB_PLX_REG_READ32 (pPlxCtrl,
                                             USB_PLX_PL_EP_STATUS_1) &
                                    USB_PLX_PL_EP_STATUS_1_STATE_MASK;

                if ((uState >= USB_PLX_PL_EP_STATUS_1_ACK_GOOD_NORMAL) &&
                    (uState <= USB_PLX_PL_EP_STATUS_1_ACK_GOOD_MORE_ACKS_TO_COME))
                    {
                   /*
                    *  Received host's Data Phase ACK:
                    *   - Good! Workaround successful and complete.
                    *   - The host's ACK should have cleared errant flip-flops
                    *     in USB3380-AB, allowing its link layer to now accept
                    *     host's LGO_Ux requests.
                    *   - This FSM state should 'lock up' the FSM, preventing
                    *     defect workaround software from running again (until
                    *     chip's power cycles, perhaps due to system hibernate).
                    */

                    uScratch |= USB_PLX_LGO_UX_FSM_SS_CONTROL_READ;

                    USB_PLX_VDBG ("usbPlxTcdLgoUxWorkaround(): workaround "
                                 "included\n",1,2,3,4,5,6);

                    break;
                    }

               /*
                *  We have not yet received host's Data Phase ACK
                *   - Wait and try again.
                */

                taskDelay(0);

                continue;
                }
            if (uAckWaitTimeout >= USB_PLX_LGO_UX_FSM_MAX_WAIT_LOOPS)
                {
                USB_PLX_VDBG ("usbPlxTcdLgoUxWorkaround(): include workaround"
                              "time out\n",1,2,3,4,5,6);
                }
            }
        else
            {
            /*
             * Connection is NOT SS:
             *  - Connection must be FS or HS.
             *   - This FSM state should allow workaround software to
             *   run after the next USB connection.
             */

            USB_PLX_VDBG ("usbPlxTcdLgoUxWorkaround():Not super speed uUsbStat %x\n",
                          uUsbStat, 2,3,4,5,6);

            uScratch |= USB_PLX_LGO_UX_FSM_NON_SS_CONTROL_READ;
            }

        /*
         * Restore data EPs to their pre-workaround settings (disabled,
         *  initialized, and other details).
         */

        usbPlxTcdDisableDataEps(pPlxCtrl);

        USB_PLX_REG_WRITE32(pPlxCtrl,
                            USB_PLX_IDXADDR_REG,
                            USB_PLX_SCRATCH_IDX);

        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_IDXDATA_REG,
                             uScratch);

        }
    else
        {
        USB_PLX_VDBG ("usbPlxTcdLgoUxWorkaround():uFsmValue %x uSetup0123 %x\n",uFsmValue, uSetup0123,
                    3,4,5,6);
        }

    return;
    /* End of workaround for LGO_Ux rejection */
    }

/*******************************************************************************
*
* usbPlxTcdPipeSequNumReset - reset the sequeue number of the pipe
*
* This routine is used to reset the sequeue number of the pipe.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbPlxTcdPipeSequNumReset
    (
    pUSB_PLX_CTRL     pPlxCtrl,
    pUSB_PLX_TCD_PIPE pTCDPipe
    )
    {
    UINT8         uEpSel;

    /* Parameter verification */

    if ((NULL == pPlxCtrl) ||
        (NULL == pTCDPipe))
        {
        USB_PLX_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pPlxCtrl) ? "pPlxCtrl" :
                    "pTCDPipe"), 2, 3, 4, 5, 6);

        return;
        }

    if (pTCDPipe->uEpType == USB_ATTR_CONTROL)
        {
        /*
         * For EP0, reset select number 0 and 1 both.
         */

        *(UINT8 *)(pPlxCtrl->uRegBase[0] + USB_PLX_PL_EP_CTRL) = 0x0;


        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_PL_EP_CTRL,
                             USB_PLX_REG_READ32 (pPlxCtrl,
                             USB_PLX_PL_EP_CTRL) |
                             USB_PLX_PL_EP_CTRL_EP_SQU_NUM_RESET);

        *(UINT8 *)(pPlxCtrl->uRegBase[0] + USB_PLX_PL_EP_CTRL) = 0x1;


        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_PL_EP_CTRL,
                             USB_PLX_REG_READ32 (pPlxCtrl,
                             USB_PLX_PL_EP_CTRL) |
                             USB_PLX_PL_EP_CTRL_EP_SQU_NUM_RESET);

        }
    else
        {
        /* The select number is the address x 2 */

        uEpSel = (UINT8)(pTCDPipe->uPhyEpAddr << 1);

        /* IN type endpoint ? */

        if (pTCDPipe->uEpDir)
            uEpSel = (UINT8)(uEpSel + 1);

        *(UINT8 *)(pPlxCtrl->uRegBase[0] + USB_PLX_PL_EP_CTRL) = uEpSel;


        USB_PLX_REG_WRITE32 (pPlxCtrl,
                             USB_PLX_PL_EP_CTRL,
                             USB_PLX_REG_READ32 (pPlxCtrl,
                             USB_PLX_PL_EP_CTRL) |
                             USB_PLX_PL_EP_CTRL_EP_SQU_NUM_RESET);
        }

    return;
    }

