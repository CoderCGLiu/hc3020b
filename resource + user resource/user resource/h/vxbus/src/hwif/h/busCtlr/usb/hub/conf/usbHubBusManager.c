/* usbHubBusManager.c - Manages topology and other information about bus */

/*
 * Copyright (c) 2003-2004, 2008-2014 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2003-2004, 2008-2014 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
Modification history
--------------------
01y,16jul14,wyy  Unify APIs of USB message to support error report (VXW6-16596)
01x,15jun14,j_x  Modified the type for uNumberOfHubEvents (VXW6-80778)
01w,03may13,wyy  Remove compiler warning (WIND00356717)
01v,04jan13,s_z  Remove compiler warning (WIND00390357)
01u,17oct12,w_x  Fix compiler warnings (WIND00370525)
01t,10oct12,w_x  Add hub show routines to aid hub tree debugging (WIND00379876)
01s,18sep12,w_x  Scan through root hub in single BusM mode (WIND00376869)
01r,24aug12,w_x  Fix task schedule issue in single BusM mode (WIND00370558)
01q,19jul12,w_x  Add support for USB 3.0 host (WIND00188662)
01p,07jan11,ghs  Clean up compile warnings (WIND00247082)
01o,08jul10,m_y  Modify for coding convention
01n,29jun10,w_x  Add more port reset delay and clean hub logging (WIND00216628)
01m,24may10,m_y  Modify hub thread for usb robustness (WIND00183499)
01l,27apr10,j_x  Error debug adaption and hard code replacement (WIND00183874)
01k,23mar10,j_x  Changed for USB debug (WIND00184542)
01j,16mar10,ghs  Fix vxWorks 6.9 LP64 adapting
01i,08jan10,y_l  Add hub current status buffer changed flag (WIND00191906)
01h,13jan10,ghs  vxWorks 6.9 LP64 adapting
01g,15oct09,w_x  Revert status copy code change by WIND00183403 (WIND00185172)
01f,30sep09,w_x  Adjust status change handling (WIND00183403)
01e,27aug09,w_x  Do not delete parent hub when port handling fails (WIND00179078)
01d,21aug09,ghs  Add USB_HUB_POLLING_INTERVAL define(WIND00160843)
01c,17dec08,w_x  Added mutex to protect hub status data ;
                 Use seperate copy of hub status to avoid potential corruption;
                 Use DMA/cache safe buffer for hub status (WIND00148225)
01b,15oct04,ami  Refgen Changes
01a,27jun03,nrv  Changing the code to WRS standards
*/

/*
DESCRIPTION

This is the module that manages the topology and other  information
about a bus. This is an autonomous thread that is initiated every time
a root hub is detected and this ends only when the USB Hub Class Driver
is unloaded or when the root hub is disabled. This is used in conjunction
with the Port Event Handling Module and Hub Event Handling Module.

INCLUDE FILES:  usb2/usbOsal.h, usb2/usbHst.h, usb2/usbHubCommon.h,
usb2/usbHubUtility.h, usb2/usbHubEventHandler.h,
usb2/usbHubPortEventHandler.h, usb2/usbHubBusManager.h,
usb2/usbHubGlobalVariables.h
*/

/*
INTERNAL
 *******************************************************************************
 * Filename         : HUB_BusManager.c
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 * Description      : This is the module that manages the topology and other
 *                    information about a bus. This is an autonomous thread that
 *                    is initiated every time a root hub is detected and this
 *                    ends only when the USB Hub Class Driver is unloaded or
 *                    when the root hub is disabled. This is used in conjunction
 *                    with the Port Event Handling Module and
 *                    Hub Event Handling Module.
 *
 *
 ******************************************************************************/



/************************** INCLUDE FILES *************************************/


#include "usb/usbOsal.h"
#include "usb/usbHst.h"
#include "usbHubCommon.h"
#include "usbHubUtility.h"
#include "usbHubEventHandler.h"
#include "usbHubPortEventHandler.h"
#include "usbHubBusManager.h"
#include "usbHubGlobalVariables.h"

/****************** MODULE SPECIFIC FUNCTIONS DECLARATION *********************/


/*
 * This function is used to recursively work on a hub to handle any event that
 * may have occurred on the hub.
 */

LOCAL void usbHubStatusChangeHandler (pUSB_HUB_INFO pHub);

extern USBHST_FUNCTION_LIST g_usbHstFunctionList;
/************************ GLOBAL FUNCTIONS DEFINITION *************************/

IMPORT UINT32 usbHubPollingIntervalGet(void);

/******************* MODULE SPECIFIC FUNCTIONS DEFINITION *********************/

/*******************************************************************************
*
* usbHubStatusChangeHandler - handle hub events recursively for a hub
*
* This routine is used to recursively work on a hub to handle any event
* that may have occurred on the hub.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbHubStatusChangeHandler
    (
    pUSB_HUB_INFO pHub
    )
    {
    /* For Storing the results. */

    USBHST_STATUS Result = USBHST_SUCCESS;

    BOOLEAN bResult = FALSE;

    /* Counter for the ports */

    UINT8 uPortCount = 0;

    pUSB_HUB_INFO pParentHub = NULL;

    /* If the pHub is NULL then return.*/
 //printk("%s:%d\n",__FUNCTION__,__LINE__);
    if ((NULL == pHub) ||
        (NULL == pHub->pCopiedStatus) ||
        (NULL == pHub->pCurrentStatus))
        {
        /* Debug Message */

        USB_HUB_ERR("invalid parameters \n",
                    1, 2, 3, 4, 5, 6);

        /* return */
        return;

        } /* End of if (NULL==.. */

    /* Take the mutex befor access the status buffer */
 //printk("%s:%d\n",__FUNCTION__,__LINE__);
    if (pHub->hubMutex) /* No need to check semTake return value */
        (void)semTake(pHub->hubMutex, WAIT_FOREVER);

    pParentHub = usbHubFindParentHubInBuses(pHub->uDeviceHandle);
//printk("%s:%d\n",__FUNCTION__,__LINE__);
    /*
     * The function "usbHubInterruptRequestCallback" could update the
     * "pHub->StateOfHub" to "USB_MARKED_FOR_DELETION" asynchronously.
     * We must make sure that the HUB is still alive when we deal with it.
     */

    if (pParentHub != NULL)
        {
        // printk("%s:%d\n",__FUNCTION__,__LINE__);
        if ((pParentHub->StateOfHub == USB_MARKED_FOR_DELETION) &&
            (pHub->StateOfHub != USB_MARKED_FOR_DELETION))
            {
            pHub->StateOfHub = USB_MARKED_FOR_DELETION;
            }
        }

    if (pHub->StateOfHub == USB_MARKED_FOR_DELETION)
        {
        /* Release the mutex */
 //printk("%s:%d\n",__FUNCTION__,__LINE__);
        if (pHub->hubMutex)
            semGive(pHub->hubMutex);

        /* Debug Message */

        USB_HUB_DBG("usbHubStatusChangeHandler - hub 0x%X being deleted\n",
                    pHub->uDeviceHandle, 2, 3, 4, 5, 6);

        /* Call the USBHST function to remove the hub */
        g_usbHstFunctionList.UsbdToHubFunctionList.removeDevice(pHub->uDeviceHandle);

        return;
        }

    /* Check if hub current status has been changed */
//printk("%s:%d\n",__FUNCTION__,__LINE__);
    if (TRUE == pHub->bCurrentStatusChanged)
        {
        /* Clean the pHub->pCopiedStatus  */
// printk("%s:%d\n",__FUNCTION__,__LINE__);
        OS_MEMSET(pHub->pCopiedStatus,
                  0,
                  pHub->uTotalStatusLength);

        /* Copy current status to the actual working copy */

        OS_MEMCPY(pHub->pCopiedStatus,
                  pHub->pCurrentStatus,
                  pHub->uActualStatusLength);

        OS_MEMSET(pHub->pCurrentStatus,
                  0,
                  pHub->uTotalStatusLength);

        /* reset the status change to FALSE */

        pHub->bCurrentStatusChanged = FALSE;
        }

    /* Release the mutex */

    if (pHub->hubMutex)
        semGive(pHub->hubMutex);

    /*
     * Call HUB_IS_HUB_EVENT() to check if any hub event has occurred on this
     * hub. If this calls returns TRUE,
     *
     * i.  Call HUB_HubEventHandler() with pHub and if this call fails, set the
     *     pHub::StateOfHub as MARKED_FOR_DELETION.
     */

    if (TRUE == USB_HUB_IS_EVENT(pHub->pCopiedStatus))
        {
        /* Debug Message */
// printk("%s:%d\n",__FUNCTION__,__LINE__);
        USB_HUB_DBG("usbHubStatusChangeHandler - hub 0x%X event detected"
                    "CopiedStatus=0x%X\n",
                    pHub->uDeviceHandle,
                    (pHub->pCopiedStatus)[0], 3, 4, 5, 6);

        /* Call the Hub event handler */

        Result = usbHubEventHandler(pHub);

        /* check the result */

        if (USBHST_SUCCESS != Result)
            {
            /* Debug Message */
// printk("%s:%d\n",__FUNCTION__,__LINE__);
            USB_HUB_ERR("failed to handle hub event, " \
                        "uDeviceHandle=0x%X, Result=%d \n",
                        pHub->uDeviceHandle, Result, 3, 4, 5, 6);

            /* Mark the hub for deletion */

            pHub->StateOfHub = USB_MARKED_FOR_DELETION;
            } /* End of if (USBHST_SUCCESS !=Result) */
        }/* End of if (TRUE== HUB_IS_HUB_EVENT()) */

    if (USB_MARKED_FOR_DELETION != pHub->StateOfHub)
        {
         //printk("%s:%d\n",__FUNCTION__,__LINE__);
        /*
         * For each port available, do:
         * i.  If pHub::pBus::nNumberOfHubEvents is non zero then  go to step 6.
         * ii. If pHub::StateOfHub is MARKED_FOR_DELETION then go to step 5.
         * iii.If pHub::pStatus contains 1 for the index corresponding to the
         *     port number then,
         *     a.    Call HUB_PortEventHandler() with the pHub and the port
         *           number if this call fails, set the pHub::StateOfHub as
         *           MARKED_FOR_DELETION and go to step 5.
         * iv. Retrieve the HUB_PORT_INFO structure from the
         *     pHub::pPortList[port number]. If this port is enabled,
         *     a.    If the HUB_PORT_INFO::StateOfPort is HUB_DEBOUNCE_PENDING
         *           then call HUB_DebounceHandler(). If this call fails, then
         *           set the pHub::StateOfHub as MARKED_FOR_DELETION and
         *           go to step 5.
         *     b.    If the HUB_PORT_INFO::StateOfPort is HUB_RESET_PENDING then
         *           call the HUB_HandleDeviceConnection(). If this call fails,
         *           then set the pHub::StateOfHub as MARKED_FOR_DELETION
         *           and go to step 5.
         *     c.    If the HUB_PORT_INFO::StateOfPort is not
         *           MARKED_FOR_DELETION then go to step 3
         *     d.    If HUB_PORT_INFO::pHub is NULL then call
         *           HUB_RemoveDevice() and free HUB_PORT_INFO and set the
         *           pHub::pPortList[port count] as NULL.
         *     e.    If HUB_PORT_INFO::pHub is not NULL then call
         *           USBHST_RemoveDevice().
         *     f.    Call HUB_CLEAR_PORT_FEATURE() with the PORT_ENABLE as the
         *           feature. If this call fails then set the pHub::StateOfHub
         *           as MARKED_FOR_DELETION and go to step 5.
         */
        for (uPortCount = 0;
             uPortCount < pHub->HubDescriptor.bNbrPorts;
             uPortCount++)
            {
            /* To store the HUB_PORT_INFO structure */

            pUSB_HUB_PORT_INFO pPort = NULL;
            atomicVal_t        uNumberOfHubEvents = 0;
// printk("%s:%d\n",__FUNCTION__,__LINE__);
            /*
             * i.  If pHub::pBus::uNumberOfHubEvents is non zero then
             * go to step 6.
             */

            if ((uNumberOfHubEvents =
                 vxAtomicGet(&(pHub->pBus->uNumberOfHubEvents))) > 0)
                {
                /* Debug Message */

                USB_HUB_VDBG("usbHubStatusChangeHandler - "
                             "hub 0x%X event detected,"
                             "uNumberOfHubEvents=0x%X\n",
                             pHub->uDeviceHandle,
                             uNumberOfHubEvents, 3, 4, 5, 6);
// printk("%s:%d\n",__FUNCTION__,__LINE__);
                break;

                } /* End of if (0<pHub->pBus->nNumberOfHubEvents) */

            /*
             * ii. If pHub::StateOfHub is MARKED_FOR_DELETION then
             * go to step 5.
             */

            if (USB_MARKED_FOR_DELETION == pHub->StateOfHub)
                {
                /* Debug Message */
// printk("%s:%d\n",__FUNCTION__,__LINE__);
                USB_HUB_DBG("usbHubStatusChangeHandler - "
                            "hub 0x%X marked for deletion\n",
                            pHub->uDeviceHandle, 2, 3, 4, 5, 6);
                break;

                } /* End of if (MARKED_FOR_DELETION ==pHub->StateOfHub)  */

            /*
             * iii.If pHub::pStatus contains 1 for the index
             * corresponding to the port number then,
             * Note: The port number is port count +1
             */
// printk("%s:%d\n",__FUNCTION__,__LINE__);
            bResult = (BOOLEAN)USB_HUB_IS_PORT_EVENT(pHub->pCopiedStatus,
                                                     uPortCount + 1);
            if (TRUE == bResult)
                {
                /* Debug Message */
//printk("%s:%d\n",__FUNCTION__,__LINE__);
                USB_HUB_DBG("usbHubStatusChangeHandler - "
                            "hub 0x%X port %d event detected\n",
                            pHub->uDeviceHandle, uPortCount, 3, 4, 5, 6);

                /*
                 * a.Call HUB_PortEventHandler() with the pHub and the port
                 * number if this call fails, set the pHub::StateOfHub as
                 * MARKED_FOR_DELETION and go to step 5.
                 */

                /*
                 * No need to check the result of this entry, if it fails
                 * the port is maked for deleted and will delted in the
                 * follow action
                 */
// printk("%s:%d\n",__FUNCTION__,__LINE__);
                usbHubPortEventHandler(pHub, uPortCount);
			//	 printk("%s:%d\n",__FUNCTION__,__LINE__);
                } /* End of if (TRUE==bResult) */

            /*
             * iv. Retrieve the HUB_PORT_INFO structure from the
             *     pHub::pPortList[port number]. If this port is enabled,
             */

            /* Retrive the Port info */
            pPort = pHub->pPortList[uPortCount];

            /* Check if the port is enabled */
            if (NULL != pPort)
                {
                /*
                 * a.If the HUB_PORT_INFO::StateOfPort is HUB_DEBOUNCE_PENDING
                 *  then call HUB_PortDebounceHandler(). If this call fails,
                 *  then set the pHub::StateOfHub as MARKED_FOR_DELETION and
                 *  go to step 5.
                 */
//printk("%s:%d\n",__FUNCTION__,__LINE__);
                if (USB_HUB_DEBOUNCE_PENDING == pPort->StateOfPort)
                    {
                    /* Debug Message */

                    USB_HUB_DBG("usbHubStatusChangeHandler - "
                                 "hub 0x%X port %d debounce pending detected\n",
                                 pHub->uDeviceHandle, uPortCount, 3, 4, 5, 6);
// printk("%s:%d\n",__FUNCTION__,__LINE__);
                    /*
                     * handle the debounce
                     * Once this entry occur error, if "pPort->StateOfPort" is set
                     * as USB_MARKED_FOR_DELETION we will delete the port in current
                     * circle. If "pPort->StateOfPort" is not equal to the
                     * "USB_MARKED_FOR_DELETION" we will continue going according
                     * to the current state of the port.
                     */

                    usbHubPortDebounceHandler(pHub,uPortCount);

                    }/* End of HUB_DEBOUNCE... */

                /*
                 * b.If the HUB_PORT_INFO::StateOfPort is HUB_RESET_PENDING then
                 * call the HUB_HandleDeviceConnection(). If this call fails,
                 * then set the pHub::StateOfHub as MARKED_FOR_DELETION
                 * and go to step 5.
                 */

                if (USB_HUB_RESET_PENDING == pPort->StateOfPort)
                    {
                    /* Debug Message */
// printk("%s:%d\n",__FUNCTION__,__LINE__);
                    USB_HUB_DBG("usbHubStatusChangeHandler -"
                                 "hub 0x%X port %d reset pending detected\n",
                                 pHub->uDeviceHandle, uPortCount, 3, 4, 5, 6);

                    /* No need to check the result here */
                   //  printk("%s:%d\n",__FUNCTION__,__LINE__);
                    (void)usbHubHandleDeviceConnection(pHub,uPortCount);
                    }/* End of HUB_RESET... */

                /*
                 * c.If the HUB_PORT_INFO::StateOfPort is not
                 * MARKED_FOR_DELETION then go to next iteration of for loop
                 */
// printk("%s:%d\n",__FUNCTION__,__LINE__);
                if (USB_MARKED_FOR_DELETION != pPort->StateOfPort)
                    {
                    /* Continue to try other ports */
                    continue;
                    }/* End of MARKED_FOR_DELETION... */

                /* Debug Message */
// printk("%s:%d\n",__FUNCTION__,__LINE__);
                USB_HUB_DBG("usbHubStatusChangeHandler - "
                            "hub 0x%X port 0x%X will be deleted\n ",
                            pHub->uDeviceHandle, uPortCount, 3, 4, 5, 6);
               /*
                * If HUB_PORT_INFO::pHub is NULL then call
                * HUB_RemoveDevice() and free HUB_PORT_INFO and set the
                * pHub::pPortList[port count] as NULL.
                * 9/5/2k3:NM: Changed here to centralise the effect
                */

                usbHubRemoveDevice(pHub,uPortCount);
// printk("%s:%d\n",__FUNCTION__,__LINE__);
                /* Set the port variable to NULL */

                pPort = NULL;

                /*
                 * f. Call HUB_CLEAR_PORT_FEATURE() with the PORT_ENABLE as the
                 * feature. If this call fails then set the pHub::StateOfHub
                 * as MARKED_FOR_DELETION and go to step 5.
                 * Note: the actual port number is the port count + 1
                 */

                Result = USB_HUB_CLEAR_PORT_FEATURE(pHub, uPortCount, USB_PORT_ENABLE);
//printk("%s:%d\n",__FUNCTION__,__LINE__);
                if (USBHST_SUCCESS != Result)
                    {
                    /* Debug Message */
// printk("%s:%d\n",__FUNCTION__,__LINE__);
                    USB_HUB_ERR("failed to clear hub 0x%X port 0x%X "
                                "USB_PORT_ENABLE feature Result=%d\n",
                                pHub->uDeviceHandle, uPortCount, Result,
                                4, 5, 6);
                    /*
                     * Port event handling failure does not mean the hub
                     * itself is bad, we can continue to work with other
                     * ports of this hub (if any), so let's just continue.
                     */
                    continue; /* continue to try other ports */
                    }/* End of if (USBHST_SUCCESS!=Result) */
                } /* End of If (NULL != pPort) */
            } /* End of for (uPortCount.... */

        /* if the hub is marked for deletion fall thru to step 5 */
// printk("%s:%d\n",__FUNCTION__,__LINE__);
        if (USB_MARKED_FOR_DELETION != pHub->StateOfHub)
            {
//printk("%s:%d\n",__FUNCTION__,__LINE__);
            /*
             * Call usbHubSubmitInterruptRequest to submit an interrupt
             * URB. In this function we will judge if the URB is already
             * submitted or not.
             * If the URB already submitted we will return USBHST_SUCCESS.
             * or else we will submit an interrupt URB and return the submit
             * result.
             */
// printk("%s:%d\n",__FUNCTION__,__LINE__);
            Result = usbHubSubmitInterruptRequest(pHub);
// printk("%s:%d\n",__FUNCTION__,__LINE__);
            /* If failed, delete the hub */
            if (USBHST_SUCCESS != Result)
                {
                /* Debug Message */

                USB_HUB_ERR("failed to submit interrupt request "
                            "for hub 0x%X, Result=%d\n",
                            pHub->uDeviceHandle,
                            Result, 3, 4, 5, 6);
// printk("%s:%d\n",__FUNCTION__,__LINE__);
                /* Mark the hub for deletion */
                pHub->StateOfHub = USB_MARKED_FOR_DELETION;
                } /* End of if (USBHST_SUCCESS !=Result) */

            }/* End of if (MARKED_FOR_DELETION != pHub->StateOfHub) */

        } /* End of (MARKED_FOR_DELETION != pHub->StateOfHub) */
// printk("%s:%d\n",__FUNCTION__,__LINE__);
    /*
     * 5:
     * If pHub::StateOfHub is MARKED_FOR_DELETION then
     * i.    Call the USBHST_RemoveDevice() function to remove the hub device.
     * ii.    Return  USBHST_SUCCESS.
     */

     if (USB_MARKED_FOR_DELETION == pHub->StateOfHub)
        {
        /* Debug Message */
// printk("%s:%d\n",__FUNCTION__,__LINE__);
        USB_HUB_DBG("usbHubStatusChangeHandler - hub 0x%X being deleted\n",
                    pHub->uDeviceHandle, 2, 3, 4, 5, 6);

        /* Call the USBHST function to remove the hub */
        g_usbHstFunctionList.UsbdToHubFunctionList.removeDevice(pHub->uDeviceHandle);

        return;
        } /* End of if (MARKED_FOR_DELETION ==pHub->StateOfHub) */

   /*
    * For all enabled ports in pHub::pPortList,
    * i.    Retrieve the HUB_PORT_INFO structure from the
    *       pHub::pPortList[port number].
    * ii.   If HUB_PORT_INFO::pHub is not NULL then call
    *       HUB_StatusChangeHandler() with HUB_PORT_INFO::pHub.
    */
// printk("%s:%d\n",__FUNCTION__,__LINE__);
    for (uPortCount = 0;
         uPortCount < pHub->HubDescriptor.bNbrPorts;
         uPortCount++)
         {
         /* Retrieve the HUB_PORT_INFO structure */
         pUSB_HUB_PORT_INFO pPort = pHub->pPortList[uPortCount];
// printk("%s:%d\n",__FUNCTION__,__LINE__);
         /* Check if the port is enabled */
         if (NULL != pPort)
            {
             /* Check if this is a hub */
             if (NULL != pPort->pHub)
                {
                /* Debug Message */

                USB_HUB_VDBG("usbHubStatusChangeHandler - "
                             "checking hub 0x%X on tier %d for events\n",
                             pHub->uDeviceHandle,
                             pHub->uCurrentTier, 3, 4, 5, 6);
               /*
                * we could have events on this hub device too.. so call
                * the handler
                */
// printk("%s:%d\n",__FUNCTION__,__LINE__);
                usbHubStatusChangeHandler(pPort->pHub);

                }/* End of if (NULL != pPort->pHub) */

            } /* End of If (NULL != pPort) */

        } /* End of for (uPortCount.... */
// printk("%s:%d\n",__FUNCTION__,__LINE__);
    return;
    }/* End of HUB_StatusChangehandler() */
#ifdef USB_HUB_CREATE_BUSM_TASK_FOR_EACH_BUS

/***************************************************************************
*
* usbHubThread - thread that keeps checking every hub on a bus.
*
* This routine checks every hub on a bus for events that require handling.
*
* RETURNS: N/A
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL void usbHubThread
    (
    void * pContext
    )
    {
    /* The pointer for the scanning of the buses */

    pUSB_HUB_BUS_INFO pWorkingBus = NULL;

    /* If pContext is NULL then return*/

    if (NULL == pContext)
        {
        /* Debug Message */
        USB_HUB_ERR("invalid parameter \n",
                    1, 2, 3, 4, 5, 6);

        return;
        }

    /* Retrieve the HUB_BUS_INFO structure from the pContext.*/

    pWorkingBus = (pUSB_HUB_BUS_INFO) pContext;

    /* Debug Message */

    USB_HUB_VDBG("usbHubThread - Thread started for bus ID:0x%x \n",
                 pWorkingBus->uBusHandle, 2, 3, 4, 5, 6);
    /*
     * Loop infintely
     * i.    If HUB_BUS_INFO::pRootHubInfo is not NULL then call
     * HUB_StatusChangeHandler() with HUB_BUS_INFO::pRootHubInfo
     */

    while (1)
        {
		/* Check pWorkingBus at first */
		
        if (pWorkingBus == NULL)
            {
            /* This is a fatal error, report it, and stop task */

            usbMsgPost(USBMSG_HCD_USBD_ERROR,
                       NULL,
                       NULL);

            return;
            }

        if (NULL != pWorkingBus->pRootHubInfo)
            {
            /* Call the status change handler for the Root hub info */
            usbHubStatusChangeHandler(pWorkingBus->pRootHubInfo);
            } /* End of if (NULL!=pWorking.... */

        /*
         * Sleep for some time to allow for the rest of the threads to do thier
         * work
         */
        OS_DELAY_MS((int)usbHubPollingIntervalGet());

        } /* End of while (1) */

    } /* End of HUB_BusManager() */

#else /* USB_HUB_CREATE_BUSM_TASK_FOR_EACH_BUS */

/* Notification semaphore */

LOCAL OS_EVENT_ID usbHubEventId;

IMPORT void usbdGlobalLockTake (void);
IMPORT void usbdGlobalLockGive (void);

/*******************************************************************************
*
* usbHubEventInit - initialize the hub event management resources
*
* This routine initializes the hub event management resources.
*
* RETURNS: OK when the event management initialization is fine, ERROR if not
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbHubEventInit (void)
    {
    usbHubEventId = OS_CREATE_EVENT(OS_EVENT_NON_SIGNALED);

    if (usbHubEventId == OS_INVALID_EVENT_ID)
        return ERROR;

    return OK;
    }

/*******************************************************************************
*
* usbHubAddEvent - add a hub with events on the event list
*
* This routine adds a hub with events on the event list.
*
* RETURNS: OK when the event is added to the list, ERROR if not
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbHubAddEvent
    (
    pUSB_HUB_INFO   pHub
    )
    {
    /* Release the message */
    
    OS_RELEASE_EVENT(usbHubEventId);

    return OK;
    }

/*******************************************************************************
*
* usbHubRemoveEvent - remove a hub with events on the event list
*
* This routine removes a hub with events from the event list.
*
* RETURNS: OK when the event is removed from the list, ERROR if not
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbHubRemoveEvent
    (
    pUSB_HUB_INFO   pHub
    )
    {
    return OK;
    }

/*******************************************************************************
*
* usbHubThread - thread that keeps checking every hub on all buses
*
* This routine checks every hub on all buses for events that require handling.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbHubThread
    (
    void * pContext
    )
    {
    /* Bus List pointer */
    
    pUSB_HUB_BUS_INFO pBusList = NULL;
    
    if (usbHubEventInit() != OK)
        {
        USB_HUB_ERR("usbHubEventInit failed\n", 1, 2, 3, 4, 5, 6);
        
        return;
        }
    
    while (1)
        {        
        /* Wait for any event change messages */

        OS_WAIT_FOR_EVENT(usbHubEventId, WAIT_FOREVER);

        /* 
         * Take the USBD global lock, since this is the lock 
         * that actually protects the gpGlobalBus list, because
         * it is the outer lock that wraps the Root Hub add
         * and remove routines.
         */
        
        usbdGlobalLockTake();

        /* Scan through the global bus list */
        
        pBusList = gpGlobalBus;

        /* Make sure the list is not NULL */
        
        while (NULL != pBusList)
            {
            /* Scan from the root hub for recursive handling */
            
            usbHubStatusChangeHandler(pBusList->pRootHubInfo);
            
            /* Move the pointer to the next bus */
            
            pBusList = pBusList->pNextBus;
            }  

        /* Release the USB global lock */
        
        usbdGlobalLockGive();
        
        /* 
         * Reschedule the task so that it is behavior compatible
         * to legacy "one BusM task per bus mode", in which case 
         * the "interrupt event polling mode" is usable as in the
         * UHCI case.
         */
        
        OS_RESCHEDULE_THREAD();
        } 
    } 

#endif

#ifdef USB_DEBUG_ENABLE

/*******************************************************************************
*
* usbHubShowInternal - show states for a specific hub
*
* This routine shows states for a specific hub.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbHubShowInternal 
    (
    pUSB_HUB_INFO           pHubInfo,
    pUSB_HUB_PORT_STATUS    pPortStatus,
    UINT32                  uHubHandle,
    BOOL                    bRecursive
    )
    {
    UINT32  uNumPorts = 0;
    UINT32  uPortIndex = 0;
    UINT8   uLength = sizeof(USB_HUB_PORT_STATUS);
    UINT16  wPortStatus;                
    UINT16  wPortChange;                 
    UINT8   uSpaces[32];
    UINT8   uSpaceChars;
    BOOL    bShowThisHub = FALSE;
    
    if (pHubInfo == NULL)
        {
        return;
        }

    if (pHubInfo->uCurrentTier > 6)
        {
        printf ("uCurrentTier %d is too big, invalid pointer?\n", 
            pHubInfo->uCurrentTier);
        return;
        }

    if ((pPortStatus == NULL) && 
        (pHubInfo->pBus != NULL))
        {
#ifdef USB_HUB_CREATE_BUSM_TASK_FOR_EACH_BUS
        printf ("Hub Bus '%s' Bus Speed %d Bus Handle 0x%x Hub Handle 0x%x Tier %d\n", 
            taskName(pHubInfo->pBus->BusManagerThreadID),
            pHubInfo->pBus->uBusSpeed, 
            pHubInfo->pBus->uBusHandle,
            pHubInfo->uDeviceHandle,
            pHubInfo->uCurrentTier);
#else
        printf ("Hub Bus Speed %d Bus Handle 0x%x Hub Handle 0x%x Tier %d\n", 
            pHubInfo->pBus->uBusSpeed, 
            pHubInfo->pBus->uBusHandle,
            pHubInfo->uDeviceHandle,
            pHubInfo->uCurrentTier);
#endif
        }
    
    uNumPorts = pHubInfo->HubDescriptor.bNbrPorts;

    /* Set the condition to show this hub information details */
    
    if ((pPortStatus != NULL) &&
         ((uHubHandle == pHubInfo->uDeviceHandle) ||
          (uHubHandle == 0)))
        bShowThisHub = TRUE;

    /* 
     * Once specific hub is found, set it to 0 to allow lower 
     * tiers to show 
     */
     
    if ((uHubHandle == pHubInfo->uDeviceHandle) &&
        (bRecursive == TRUE))
        {
        uHubHandle = 0;
        }
    
    if (bShowThisHub == TRUE)
        {
        uSpaceChars = (UINT8)(pHubInfo->uCurrentTier * 3);
        
        OS_MEMSET(uSpaces, '-', uSpaceChars);
        uSpaces[uSpaceChars]= '\0';

        printf("==============================================================\n");
        printf("%s-uCurrentTier %d\n", uSpaces, pHubInfo->uCurrentTier);
        printf("%s-uDeviceHandle 0x%x\n", uSpaces, pHubInfo->uDeviceHandle);
        printf("%s-uPowerPerPort %d\n", uSpaces, pHubInfo->uPowerPerPort);
        printf("%s-bNbrPorts %d\n", uSpaces, pHubInfo->HubDescriptor.bNbrPorts);
        printf("%s-wHubCharacteristics 0x%04x\n", uSpaces, pHubInfo->HubDescriptor.wHubCharacteristics);
        printf("%s-bPwrOn2PwrGood %d\n", uSpaces, pHubInfo->HubDescriptor.bPwrOn2PwrGood);
        printf("%s-bHubContrCurrent %d\n", uSpaces, pHubInfo->HubDescriptor.bHubContrCurrent);
        printf("%s-bURBSubmitted %d\n", uSpaces, pHubInfo->bURBSubmitted);
        printf("%s-uNumErrors %d\n", uSpaces, pHubInfo->uNumErrors);
        printf("%s-uHubTTInfo 0x%x\n", uSpaces, pHubInfo->uHubTTInfo);
        printf("%s-uPortIndex 0x%x\n", uSpaces, pHubInfo->uPortIndex);
        printf("%s-pParentHub %p\n", uSpaces, pHubInfo->pParentHub);
        printf("%s-pParentPort %p\n", uSpaces, pHubInfo->pParentPort);
        }
    
    for (uPortIndex = 0; 
         uPortIndex < uNumPorts; 
         uPortIndex++)
        {
        USBHST_STATUS Result;
        
        USB_HUB_PORT_INFO*   pPortInfo = pHubInfo->pPortList[uPortIndex];

        if (bShowThisHub == TRUE)
            {
            /* Re-get the pPortStatus */
            
            OS_MEMSET(pPortStatus, 0, sizeof(USB_HUB_PORT_STATUS));
            
            /*
             * Call the HUB_GET_PORT_STATUS() function to retrieve the HUB_PORT_STATUS
             * structure. If failed, then return USBHST_FAILURE.
             * Note: actual port number is 1 + the port index
             */
            
            Result = USB_HUB_GET_PORT_STATUS(pHubInfo,
                                             uPortIndex,
                                             (UINT8 *)(pPortStatus),
                                             &uLength);

            wPortChange = OS_UINT16_LE_TO_CPU(pPortStatus->wPortChange);
            wPortStatus = OS_UINT16_LE_TO_CPU(pPortStatus->wPortStatus);

            printf ("%s--Tier %d Hub 0x%x Port %d wPortChange 0x%04x wPortStatus 0x%04x\n", 
                uSpaces,
                pHubInfo->uCurrentTier,
                pHubInfo->uDeviceHandle,
                uPortIndex, 
                wPortChange,
                wPortStatus);
            
            if (pHubInfo->pBus->uBusSpeed == USB_SPEED_SUPER)
                {            
                printf ("%s---C_PORT_CONNECTION:   %s\n", 
                    uSpaces, (wPortChange & (1 << 0)) ? "SET":"CLR");
                printf ("%s---C_PORT_OVER_CURRENT: %s\n", 
                    uSpaces, (wPortChange & (1 << 3)) ? "SET":"CLR");
                printf ("%s---C_PORT_RESET:        %s\n", 
                    uSpaces, (wPortChange & (1 << 4)) ? "SET":"CLR");
                printf ("%s---C_BH_PORT_RESET:     %s\n", 
                    uSpaces, (wPortChange & (1 << 5)) ? "SET":"CLR");
                printf ("%s---C_PORT_LINK_STATE:   %s\n", 
                    uSpaces, (wPortChange & (1 << 6)) ? "SET":"CLR");
                printf ("%s---C_PORT_CONFIG_ERROR: %s\n", 
                    uSpaces, (wPortChange & (1 << 7)) ? "SET":"CLR");
                }
            else
                {
                printf ("%s---C_PORT_CONNECTION:   %s\n", 
                    uSpaces, (wPortChange & (1 << 0)) ? "SET":"CLR");
                printf ("%s---C_PORT_ENABLE:       %s\n", 
                    uSpaces, (wPortChange & (1 << 1)) ? "SET":"CLR");
                printf ("%s---C_PORT_SUSPEND:      %s\n", 
                    uSpaces, (wPortChange & (1 << 2)) ? "SET":"CLR");
                printf ("%s---C_PORT_OVER_CURRENT: %s\n", 
                    uSpaces, (wPortChange & (1 << 3)) ? "SET":"CLR");
                printf ("%s---C_PORT_RESET:        %s\n", 
                    uSpaces, (wPortChange & (1 << 4)) ? "SET":"CLR");
                }
                    
            if (pHubInfo->pBus->uBusSpeed == USB_SPEED_SUPER)
                {            
                printf ("%s---PORT_CONNECTION:     %s\n", 
                    uSpaces, (wPortStatus & (1 << 0)) ? "SET":"CLR");
                printf ("%s---PORT_ENABLE:         %s\n", 
                    uSpaces, (wPortStatus & (1 << 1)) ? "SET":"CLR");
                printf ("%s---PORT_OVER_CURRENT:   %s\n", 
                    uSpaces, (wPortStatus & (1 << 3)) ? "SET":"CLR");
                printf ("%s---PORT_RESET:          %s\n", 
                    uSpaces, (wPortStatus & (1 << 4)) ? "SET":"CLR");
                printf ("%s---PORT_LINK_STATE:     %d\n", 
                    uSpaces, ((wPortStatus & (0xF << 5)) >> 5));
                printf ("%s---PORT_POWER:          %s\n", 
                    uSpaces, (wPortStatus & (1 << 9)) ? "SET":"CLR");
                printf ("%s---PORT_SPEED:          %d\n", 
                    uSpaces, ((wPortStatus & (0x7 << 10)) >> 10));
                }
            else
                {
                printf ("%s---PORT_CONNECTION:     %s\n", 
                    uSpaces, (wPortStatus & (1 << 0)) ? "SET":"CLR");
                printf ("%s---PORT_ENABLE:         %s\n", 
                    uSpaces, (wPortStatus & (1 << 1)) ? "SET":"CLR");
                printf ("%s---PORT_SUSPEND:        %s\n", 
                    uSpaces, (wPortStatus & (1 << 2)) ? "SET":"CLR");
                printf ("%s---OPORT_OVER_CURRENT:  %s\n", 
                    uSpaces, (wPortStatus & (1 << 3)) ? "SET":"CLR");
                printf ("%s---PORT_RESET:          %s\n", 
                    uSpaces, (wPortStatus & (1 << 4)) ? "SET":"CLR");
                printf ("%s---PORT_POWER:          %s\n", 
                    uSpaces, (wPortStatus & (1 << 8)) ? "SET":"CLR");
                printf ("%s---PORT_LOW_SPEED:      %s\n", 
                    uSpaces, (wPortStatus & (1 << 9)) ? "SET":"CLR");
                printf ("%s---PORT_HIGH_SPEED:     %s\n", 
                    uSpaces, (wPortStatus & (1 << 10)) ? "SET":"CLR");
                printf ("%s---PORT_TEST:           %s\n", 
                    uSpaces, (wPortStatus & (1 << 11)) ? "SET":"CLR");
                printf ("%s---PORT_INDICATOR:      %s\n", 
                    uSpaces, (wPortStatus & (1 << 12)) ? "SET":"CLR");
                }

            if (pPortInfo != NULL)
                {
                pUSBD_DEVICE_INFO    pDeviceInfo = NULL;

                /* Obtain the device info of the device handle */
                usbdTranslateDeviceHandle(pPortInfo->uDeviceHandle, &pDeviceInfo);
                
                /* Check if the device info is valid */
                if (pDeviceInfo != NULL)
                    {
                    UINT32 uInterface = 0;
                    
                    if (pDeviceInfo->pDriver)
                        printf ("%s---Tier %d Hub 0x%x Port %d Device Driver Name %s\n", 
                        uSpaces, 
                        pHubInfo->uCurrentTier,
                        pHubInfo->uDeviceHandle,
                        uPortIndex,
                        pDeviceInfo->pDriver->vxbDriverInfo.drvName);

                    if (pDeviceInfo->pInterfacesInfo != NULL)
                        {
                        for (uInterface = 0; 
                             uInterface < pDeviceInfo->uInterfaceCount; 
                             uInterface++)
                            {
                            pUSBD_INTERFACE_INFO pInterfacesInfo;
                            pInterfacesInfo = &pDeviceInfo->pInterfacesInfo[uInterface];

                            if (pInterfacesInfo != NULL)
                                {
                                if (pInterfacesInfo->pDriver)
                                    {
                                    printf ("%s----Tier %d Hub 0x%x Port %d Interface %d Driver Name %s\n", 
                                    uSpaces, 
                                    pHubInfo->uCurrentTier,
                                    pHubInfo->uDeviceHandle,
                                    uPortIndex,
                                    uInterface, 
                                    pInterfacesInfo->pDriver->vxbDriverInfo.drvName);
                                    }
                                }
                            }
                        }
                    }
                }
            }

        /* Show the next tier */
        
        if ((pPortInfo != NULL) &&
            (pPortInfo->pHub != NULL) &&
            ((bRecursive == TRUE) || (uHubHandle != 0)))
            {
            usbHubShowInternal(pPortInfo->pHub, 
                               pPortStatus, 
                               uHubHandle, 
                               bRecursive);
            }
        }
    }

/*******************************************************************************
*
* usbHubBusShow - show states of the specific hub bus tree
*
* This routine shows states of the specific hub bus tree. The parameters can be
* retrived from the usbHubLisShow() output.
*
* <uBusHandle> - the bus handle
* <uHubHandle> - the hub handle
* <bRecursive> - is the show goes on recursively
*
* The <uBusHandle> should be one of the "Bus Handle" value from usbHubLisShow();
*
* The <uHubHandle> could be a specific "Hub Handle" value from usbHubLisShow(),
* or if it is set to 0, it will show the whole bus specified by <uBusHandle>,
* and the show is done recursively. 
*
* The <bRecursive> indicates if the show goes on recursively if <uHubHandle> is
* not zero. If <bRecursive> is FALSE, show only the hub with "Hub Handle" equals
* <uHubHandle> on the bus with <uBusHandle>. If <bRecursive> is TRUE, show from 
* the hub with "Hub Handle" equals <uHubHandle> on the bus with <uBusHandle>, 
* recursively.  
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbHubBusShow 
    (
    UINT8   uBusHandle,
    UINT32  uHubHandle,
    BOOL    bRecursive
    )
    {    
    pUSB_HUB_BUS_INFO pBusList = NULL;

    pUSB_HUB_PORT_STATUS pPortStatus = NULL;

    if (uHubHandle == 0)
        bRecursive = TRUE;
    
    /* Scan through the global bus list */
    
    pBusList = gpGlobalBus;

    /* Make sure the list is not NULL */
    
    while (NULL != pBusList)
        { 
        if (pBusList->uBusHandle == uBusHandle)
            {
            /* Allocate memory for the port status */
            
            pPortStatus = OSS_CALLOC(sizeof(USB_HUB_PORT_STATUS));
            
            /* Check if memory allocation is successful */
            if (pPortStatus == NULL)
                {
                /* Debug Message */
                printf("memory allocate failed for port status \n");
            
                return;
                }

            printf ("\n###########################################################\n");

            #ifdef USB_HUB_CREATE_BUSM_TASK_FOR_EACH_BUS
            printf ("Showing Hub Bus '%s' Bus Handle 0x%x Bus Speed %d Hub Handle 0x%x %s\n", 
                taskName(pBusList->BusManagerThreadID),
                uBusHandle,
                pBusList->uBusSpeed, uHubHandle, (bRecursive ? "recursively": ""));
            #else
            printf ("Showing Hub Bus 0x%x Bus Speed %d Hub Handle 0x%x %s\n", 
                uBusHandle, 
                pBusList->uBusSpeed, uHubHandle, (bRecursive ? "recursively": ""));
            #endif
            
            usbHubShowInternal(pBusList->pRootHubInfo, 
                            pPortStatus,
                            uHubHandle,
                            bRecursive);

            OSS_FREE(pPortStatus);
            
            return;
            }
        
        /* Move the pointer to the next bus */
        
        pBusList = pBusList->pNextBus;
        } 

    printf("No Hub Bus with Bus Handle 0x%x found\n", uBusHandle);
    }

/*******************************************************************************
*
* usbHubListShow - list all hubs in the system
*
* This routine lists all hubs in the system. The output of this show routine 
* can be used to call usbHubBusShow() to get more detailed information for
* a specific bus or a specific hub.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* SEE ALSO: usbHubBusShow
*
* \NOMANUAL
*/

void usbHubListShow (void)
    {    
    pUSB_HUB_BUS_INFO pBusList = NULL;
    
    /* Scan through the global bus list */
    
    pBusList = gpGlobalBus;

    /* Make sure the list is not NULL */
    
    while (NULL != pBusList)
        {         
        usbHubShowInternal(pBusList->pRootHubInfo, NULL, 0, TRUE);
                
        /* Move the pointer to the next bus */
        
        pBusList = pBusList->pNextBus;
        } 
    }
#endif /* USB_DEBUG_ENABLE */

/**************************** End of File usbHubBusManager.c ******************/
