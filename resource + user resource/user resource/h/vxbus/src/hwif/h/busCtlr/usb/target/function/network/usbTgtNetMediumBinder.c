/* usbTgtNetMediumBinder.c - The Medium Binder Module to Connect The USB With Medium Device */

/*
 * Copyright (c) 2011-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
Modification history
--------------------
01h,06may13,s_z  Remove compiler warning (WIND00356717)
01g,04jan13,s_z  Remove compiler warning (WIND00390357)
01f,03sep12,s_z  Remove coverity warning with checking semTake value  
01e,15may12,s_z  Correct coverity warning 
01d,15may12,s_z  Initialize binder buffer with zero (WIND00348017)
01c,12jul11,s_z  Fix the binder buffer delete issue
01b,09mar11,s_z  Code clean up
01a,04jan11,s_z  written
*/

/*
DESCRIPTION

This file is the medium binder module to connect the USB with the medium which 
need be attached to the USB target.

This module manages the bind buffers which can be used by the low level 
transport device driver and the high level medium device.

INCLUDE FILES:  vxWorks.h usb/usbOsal.h usb/usbTgt.h
                usbTgtMediumEnd.h usbTgtNetMediumBinder.h
*/

/* includes */

#include <vxWorks.h>
#include <usb/usbOsal.h>
#include <usb/usbTgt.h>
#include <usbTgtMediumEnd.h>
#include <usbTgtNetMediumBinder.h>


/*******************************************************************************
*
* usbTgtNetMediumBinderBufDestroy - destroy a binder buffer which created before
*
* This routine destroys a binder buffer which created by the usbTgtBinderBufCreate
* routine.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtNetMediumBinderBufDestroy
    (
    pUSBTGT_BINDER_BUF pBinderBuf
    )
    {
    if (NULL == pBinderBuf)
        {
        USBTGT_END_ERR("Invalid parameter %s is NULL\n",
                       ("pBinderBuf"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    if (NULL != pBinderBuf->pBuf)
        {
        /* Free the Media buf */

        OS_FREE (pBinderBuf->pBuf);

        pBinderBuf->pBuf = NULL;        
        }
    
    USBTGT_END_VDBG("usbTgtNetMediumBinderBufDestroy pBinderBuf 0x%X\n",
                    pBinderBuf, 2, 3, 4, 5, 6);
    
    /* Free the Medium buf */ 
    
    OS_FREE (pBinderBuf);
    
    return OK;
    }

/*******************************************************************************
*
* usbTgtNetMediumBinderBufCreate - create binder buffer with buffer size and header length
*
* This routine creates binder buffer with buffer size and header lenght.
*
* NOTE: This routine will create one data buffer with size of <'dataPoolSize'> +
* <'dataHeaderLen'>. The first <'dataHeaderLen'> length of the buffer is used 
* to store the header of the data which to send/receive. the following length 
* of the <'dataPoolSize'> buffer used to store the real data.
*
* RETURNS: NULL or the pBIND_BUF pointer if there is no error
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL pUSBTGT_BINDER_BUF usbTgtNetMediumBinderBufCreate
    (
    int dataPoolSize,
    int dataHeaderLen
    )
    {
    pUSBTGT_BINDER_BUF pBinderBuf = NULL;

    if ((dataPoolSize < 0) ||
        (dataHeaderLen < 0))
        {
        USBTGT_END_ERR("Invalid parameter %s is less than 0\n",
                       ((dataPoolSize < 0) ? "dataPoolSize" :
                       "dataHeaderLen"), 2, 3, 4, 5, 6);

        return NULL;
        }
    
    pBinderBuf = (pUSBTGT_BINDER_BUF)OSS_CALLOC(sizeof (USBTGT_BINDER_BUF));

    if (NULL == pBinderBuf)
        {
        USBTGT_END_ERR("No room to malloc the binder buffer struct\n",
                         1, 2, 3, 4, 5, 6);

        return NULL;
        }

    pBinderBuf->pBuf = (char *)OSS_CALLOC(dataPoolSize + dataHeaderLen);

    if (NULL == pBinderBuf->pBuf)
        {
        USBTGT_END_ERR("No room to create Binder buffer with dataPoolSize "
                       "0x%X dataHeaderLen 0x%X\n",
                       dataPoolSize, dataHeaderLen, 3, 4, 5, 6);

        usbTgtNetMediumBinderBufDestroy(pBinderBuf);

        return NULL;
        }
    
    USBTGT_END_VDBG("Created Binder buffer with pBinderBuf 0x%X length %d dataPoolSize "
                    "%d dataHeaderLen %d\n",
                    pBinderBuf, dataPoolSize, dataHeaderLen, 4, 5, 6);

    /* The buffer size if the sum of dataPoolSize with dataHeaderLen */
    
    pBinderBuf->brfLen = dataPoolSize + dataHeaderLen;   

    /*
     * The header is used by the low level dev driver 
     * If used for RNDIS, before send to the USB, it need fill up the 
     * RNDIS header in the buffer. Since this module can be used on orther
     * dev module such as CDC/EEM, CDC/ECM, CDC/NCM
     * one header length need be inited too. Every low level dev driver need
     * set configure this value.
     */
     
    pBinderBuf->headerLen = dataHeaderLen;
    
    return pBinderBuf;
    }

/*******************************************************************************
*
* usbTgtNetMediumBinderBufListDestroy - destroy the binder buffer list created
*
* This routine destroys the binder buffer list created using the routine 
* usbTgtBinderBufListCreate.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtNetMediumBinderBufListDestroy
    (
    LIST * pList
    )
    {
    pUSBTGT_BINDER_BUF pBinderBuf = NULL;
    int                index;
    int                count;
    
    if (NULL == pList)
        {
        USBTGT_END_ERR("Invalid parameter %s is NULL\n",
                       "pList", 2, 3, 4, 5 ,6);

        return ERROR;
        }
    
    count = lstCount (pList);

    USBTGT_END_VDBG("usbTgtNetMediumBinderBufListDestroy %p count %d\n",
                    pList, count, 3, 4, 5 ,6);
        
    for (index = 1; index <= count; index ++)
        {
        pBinderBuf = (pUSBTGT_BINDER_BUF)lstFirst (pList);

        if (NULL == pBinderBuf)
            {
            USBTGT_END_ERR ("pBinderBuf is NULL, how can ?\n",
                            1, 2, 3, 4, 5, 6);
            
            return ERROR;
            }

        /* Delete the buffer from the list */
        
        lstDelete (pList, &pBinderBuf->binderBufNode);

        /* Destroy the binder buffer */

        usbTgtNetMediumBinderBufDestroy (pBinderBuf);        
        }

    return OK;
    }

/*******************************************************************************
*
* usbTgtNetMediumBinderBufListCreate - create a list of bind buffers 
*
* This routine creates a list of bind buffers with list count and binder 
* buffer data pool size and header length.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtNetMediumBinderBufListCreate
    (
    LIST * pList,
    int    listCount,
    int    dataPoolSize,
    int    dataHeaderLen
    )
    {
    pUSBTGT_BINDER_BUF pBinderBuf = NULL;
    int                index;

    if (NULL == pList)
        {
        USBTGT_END_ERR("Invalid parameter %s is NULL\n",
                       "pList", 2, 3, 4, 5 ,6);

        return ERROR;
        }

    for (index = 0; index < listCount; index ++)
        {
        pBinderBuf = usbTgtNetMediumBinderBufCreate(dataPoolSize, dataHeaderLen);

        if (NULL == pBinderBuf)
            {
            USBTGT_END_ERR ("NO enough memory to create the binder buffer, "
                            "destroy the list\n",
                            1, 2, 3, 4, 5 ,6);

            usbTgtNetMediumBinderBufListDestroy(pList);
            
            return ERROR;
            }
        
        /* Add to the list */

        lstAdd (pList, &pBinderBuf->binderBufNode);
        }

    return OK;
    }

/*******************************************************************************
*
* usbTgtNetMediumBinderBufGet - get one binder buffer from the right buffer list  
* according to the binder buffer type.
*
* This routine gets one bind buf from the right buffer list according 
* to the binder buffer type.
*
* RETURNS: NULL or the pUSBTGT_BINDER_BUF pointer if there is no error
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSBTGT_BINDER_BUF usbTgtNetMediumBinderBufGet
    (
    pUSBTGT_BINDER         pBinder,
    USBTGT_BINDER_BUF_TYPE binderBufType
    )
    {
    pUSBTGT_BINDER_BUF pBinderBuf = NULL;
    LIST *             pList = NULL;
    
    if ((NULL == pBinder) ||
        (USBTGT_BINDER_BUF_UNKNOWN == binderBufType))
        {
        USBTGT_END_ERR("Invalid parameter %s is NULL\n",
                       ((NULL == pBinder) ? "pBinder" :
                       "binderBufType"), 2, 3, 4, 5 ,6);

        return NULL;
        }

    /* Take the owner ship */
    
    if (OK == semTake (pBinder->binderMutex, USBTGT_WAIT_TIMEOUT))
        {

    switch (binderBufType)
        {
        case USBTGT_BINDER_BUF_FREE_XMIT:

            pList = &pBinder->freeXmitBufList;
            
            break;
        case USBTGT_BINDER_BUF_FREE_RCV:

            pList = &pBinder->freeRcvBufList;
            
            break;            
        case USBTGT_BINDER_BUF_XMIT_TO_MEDIUM:
            
            pList = &pBinder->xmitToMediumBufList;
            
            break;
        case USBTGT_BINDER_BUF_RCV_FROM_MEDIUM:

            pList = &pBinder->rcvFromMediumBufList;
            
            break;
        default:
            break;
        }

    /* The list pointer is NULL */
    
    if (NULL == pList)
        {
        USBTGT_END_VDBG("usbTgtNetMediumBinderBufGet list is NULL type %d \n",
                       (binderBufType), 2, 3, 4, 5 ,6);
        
        semGive (pBinder->binderMutex);
        
        return NULL;
        }

    pBinderBuf = (pUSBTGT_BINDER_BUF)lstFirst (pList);

    /* Remove the binder buffer from the list */

    if (NULL != pBinderBuf)
        {
        lstDelete (pList, &pBinderBuf->binderBufNode);
        }
    
    /* 
     * TODO : if we there is no FREE binder buffer for using, 
     * create a new one 
     */
   
    semGive (pBinder->binderMutex);
        }

    USBTGT_END_VDBG("usbTgtNetMediumBinderBufGet 0x%X with type %d \n",
                    pBinderBuf, binderBufType, 3, 4, 5 ,6);
    
    return pBinderBuf;
    }

/*******************************************************************************
*
* usbTgtNetMediumBinderBufPut - put one binder buffer to the right buffer list  
* according to the binder buffer type.
*
* This routine puts one binder buffer to the right buffer list according 
* to the binder buffer type.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbTgtNetMediumBinderBufPut
    (
    pUSBTGT_BINDER         pBinder,
    pUSBTGT_BINDER_BUF     pBinderBuf,
    USBTGT_BINDER_BUF_TYPE binderBufType
    )
    {
    LIST *      pList = NULL;
    
    if ((NULL == pBinder) ||
        (NULL == pBinderBuf) ||
        (USBTGT_BINDER_BUF_UNKNOWN == binderBufType))
        {
        USBTGT_END_ERR("Invalid parameter %s is NULL\n",
                       ((NULL == pBinder) ? "pBinder" :
                       (NULL == pBinderBuf) ? "pBinderBuf" :
                       "binderBufType"), 2, 3, 4, 5 ,6);

        return ERROR;
        }

    /* Take the owner ship */
    
    if (OK == semTake (pBinder->binderMutex, USBTGT_WAIT_TIMEOUT))
        {
    switch (binderBufType)
        {
        case USBTGT_BINDER_BUF_FREE_XMIT:

            pList = &pBinder->freeXmitBufList;

            /* Clean the header */
            
            memset(pBinderBuf->pBuf, 0, pBinderBuf->headerLen);
            
            break;

        case USBTGT_BINDER_BUF_FREE_RCV:

            pList = &pBinder->freeRcvBufList;

            /* Clean the header */
            
            memset(pBinderBuf->pBuf, 0, pBinderBuf->headerLen);
            
            break;
        case USBTGT_BINDER_BUF_XMIT_TO_MEDIUM:
            
            pList = &pBinder->xmitToMediumBufList;
            
            break;
        case USBTGT_BINDER_BUF_RCV_FROM_MEDIUM:

            pList = &pBinder->rcvFromMediumBufList;
            
            break;
        default:
            break;
        }

    /* The list pointer is NULL */
    
    if (NULL == pList)
        {
        /* TODO: If give a wrong type, destroy the binder buffer */

        semGive (pBinder->binderMutex);

        usbTgtNetMediumBinderBufDestroy(pBinderBuf);
        
        USBTGT_END_WARN("usbTgtBinderBufPut list %d not exit, destroy the buffer 0x%X\n",
                        binderBufType, pBinderBuf, 3, 4, 5 ,6);
        
        return ERROR;
        }

    lstAdd (pList, &pBinderBuf->binderBufNode);
   
    semGive (pBinder->binderMutex);
        }

    USBTGT_END_VDBG("usbTgtBinderBufPut binder buffer 0x%X with type %d done\n",
                    pBinderBuf, binderBufType, 3, 4, 5 ,6);
    
    return OK;
    }

/*******************************************************************************
*
* usbTgtNetMediumBinderDestroy - destroy the pUSBTGT_BINDER data 
*
* This routine destroys the pUSBTGT_BINDER data created by the routine 
* usbTgtBinderCreate. It also will destroy all the binder buffer lists of the 
* used by the pUSBTGT_BINDER.
*
* RETURNS: OK or ERROR if there is something wrong.
*
* ERRNO: N/A
*/

STATUS usbTgtNetMediumBinderDestroy
    (
    pUSBTGT_BINDER pBinder
    )
    {

    if (NULL == pBinder)
        {
        USBTGT_END_ERR("Invalid parameter %s is NULL\n",
                       "pBinder", 2, 3, 4, 5 ,6);

        return ERROR;
        }
    
    if (OK == semTake (pBinder->binderMutex, USBTGT_WAIT_TIMEOUT))
        {    
    /* Delete the free Xmit binder buffer list */

    usbTgtNetMediumBinderBufListDestroy (&pBinder->freeXmitBufList);
    
    /* Delete the free Rcv binder buffer list */

    usbTgtNetMediumBinderBufListDestroy (&pBinder->freeRcvBufList);
    
    /* Delete the xmit binder buffer list */

    usbTgtNetMediumBinderBufListDestroy (&pBinder->xmitToMediumBufList);

    /* Delete the rcv binder buffer list */

    usbTgtNetMediumBinderBufListDestroy (&pBinder->rcvFromMediumBufList);

        semDelete (pBinder->binderMutex);

        pBinder->binderMutex = NULL;

    USBTGT_END_ERR("usbTgtBinderDestroy %s successfully\n",
                   pBinder, 2, 3, 4, 5 ,6);

    OS_FREE (pBinder);
        }

    return OK;
    }

/*******************************************************************************
*
* usbTgtNetMediumBinderCreate - create one usb tgt binder data structure
*
* This routine creates one usb tgt binder data stucture.It will create 4 lists:
*
* <'freeXmitBufList'> : link a list of free bind buffers used to transfer data 
* to the medium. 
* 
* <'freeRcvBufList'> : link a list of free bind buffers used to transfer data
* to the USB
* 
* <'xmitToMediumBufList'> : link a list of binder buffer which contains the 
* data to the medium. The programer will get binder buffer from the list and
* send the data to the medium
* 
* <'rcvFromMediumBufList'> : link a list of binder buffer which contains the 
* data get from the medium. The programer will get binder buffer from the list 
* and send the data to the USB.
*
* This routine will use the parameters <'freeBufCount'>, <'dataPoolSize'> and
* <'dataHeaderLen'> to create <'freeXmitBufList'> and <'freeRcvBufList'>. The 
* programer can get free binder buffer from those list and put the right list
* ready to process.
*
* RETURNS: NULL or the pUSBTGT_BINDER pointer if there is no error
*
* ERRNO: N/A
*/

pUSBTGT_BINDER usbTgtNetMediumBinderCreate
    (
    int freeBufCount,
    int dataPoolSize,
    int dataHeaderLen
    )
    {
    pUSBTGT_BINDER pBinder = NULL;
    STATUS         status;
    
    pBinder = (pUSBTGT_BINDER)OSS_CALLOC(sizeof (USBTGT_BINDER));

    if (NULL == pBinder)
        {
        USBTGT_END_ERR("No room to malloc the binder buffer struct\n",
                       1, 2, 3, 4, 5, 6);

        return NULL;
        }

    /* Create the binder mutex */

    pBinder->binderMutex = semMCreate(USBTGT_MUTEX_CREATION_OPTS); 
    
    if (NULL == pBinder->binderMutex)
        {
        USBTGT_END_ERR("Create the mutex fail\n",
                       1, 2, 3, 4, 5 ,6);

        usbTgtNetMediumBinderDestroy (pBinder);
        
        return NULL;
        }

    /* Take the ownership */

    if (OK == semTake (pBinder->binderMutex, USBTGT_WAIT_TIMEOUT))
        {        
    /* Init the free Xmit binder buffer */

    lstInit (&pBinder->freeXmitBufList);
    
    /* Init the free Rcv binder buffer */

    lstInit (&pBinder->freeRcvBufList);
    
    /* Init the xmit binder buffer */

    lstInit (&pBinder->xmitToMediumBufList);

    /* Init the rcv binder buffer */

    lstInit (&pBinder->rcvFromMediumBufList);

    /* Create one free list binder buffer used to trnasfer data to Meidum */

    status = usbTgtNetMediumBinderBufListCreate(&pBinder->freeXmitBufList,
                                     freeBufCount,
                                     dataPoolSize,
                                     dataHeaderLen);

    if (ERROR == status)
        {
        semGive (pBinder->binderMutex);
        
        usbTgtNetMediumBinderDestroy (pBinder);
        
        return NULL;
        }

    /* Create one free list binder buffer used to receive data from medium */
    
    status = usbTgtNetMediumBinderBufListCreate(&pBinder->freeRcvBufList,
                                     freeBufCount,
                                     dataPoolSize,
                                     dataHeaderLen);

    if (ERROR == status)
        {
        semGive (pBinder->binderMutex);
        
        usbTgtNetMediumBinderDestroy (pBinder);
        
        return NULL;
        }

    pBinder->binderBufCount = freeBufCount;
    pBinder->binderBufSize =  dataPoolSize + dataHeaderLen;
    
    semGive (pBinder->binderMutex); 
        }
    USBTGT_END_DBG("usbTgtBinderCreate 0x%X successfully \n",
                   pBinder, 2, 3, 4, 5 ,6);


    return pBinder;    
    }


/*******************************************************************************
*
* usbTgtNetMediumBinderAttach - attach the medium device to the binder 
*
* This routine attachs the medium device to the binder 
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO: N/A
*/

STATUS usbTgtNetMediumBinderAttach
    (
    pUSBTGT_BINDER pBinder,
    char *         pMediumName,   /* Medium name which need to bind */
    int            mediumUnit,    /* Medium unit which need to bind */
    UINT8          uMediumType    /* Medium type */
    )
    {
    STATUS status = ERROR;
    
    /* Parameter validation */
    
    if ((NULL == pBinder) ||
        (NULL == pMediumName))
        {
        USBTGT_END_ERR("Invalid parameter %s is NULL\n",
                       ((NULL == pBinder) ? "pBinder" :
                       "pMediumName"), 2, 3, 4, 5, 6);

        return ERROR;
        }
    
    switch (uMediumType)
        {
        case USBTGT_BINDER_MEDIUM_VIRTUAL_END:
        case USBTGT_BINDER_MEDIUM_END_BRIGGE:
            {
            status = usbTgtMediumEndAttach (pBinder,
                                            pMediumName,
                                            mediumUnit,
                                            uMediumType,
                                            &pBinder->pMediumDev);

            pBinder->pMediumName = pMediumName;
            pBinder->uMediumUint = (UINT8) (mediumUnit);
            pBinder->uMediumType = uMediumType;
            }
            break;
        default:
            break;
        }

    if (status == ERROR)
        {
         USBTGT_END_ERR("Attach the Medium %s%d whith type 0x%X error\n",
                        (pMediumName),
                         mediumUnit,
                         uMediumType, 4, 5, 6);

        }  
    
     USBTGT_END_DBG("Attach the Medium %s%d whith type 0x%X successfully\n",
                    (pMediumName),
                    mediumUnit,
                    uMediumType, 4, 5, 6);
    return status;
    }

/*******************************************************************************
*
* usbTgtNetMediumBinderDetach - detach the medium device from the binder 
*
* This routine detaches the medium device from the binder 
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO: N/A
*/

STATUS usbTgtNetMediumBinderDetach
    (
    pUSBTGT_BINDER pBinder
    )
    {
    STATUS status = ERROR;
    
    /* Parameter validation */
    
    if ((NULL == pBinder) ||
        (NULL == pBinder->pMediumDev))
        {
        USBTGT_END_ERR("Invalid parameter %s is NULL\n",
                       ((NULL == pBinder) ? "pBinder" :
                       "pBinder->pMediumDev"), 2, 3, 4, 5, 6);

        return ERROR;
        }
    
    switch (pBinder->uMediumType)
        {
        case USBTGT_BINDER_MEDIUM_VIRTUAL_END:
        case USBTGT_BINDER_MEDIUM_END_BRIGGE:
            {
            /* 
             * Destroy the Medium dev and disconnect the connection with
             * the binder
             */

            status = usbTgtMediumEndDetach (pBinder->pMediumDev);
            
            if (OK == status)
                {
                pBinder->pMediumDev = NULL;            
                }
            }
            break;
        default:
            break;
        }

    if (status == ERROR)
        {
         USBTGT_END_ERR("Deatch the Medium %s%d whith type 0x%X error\n",
                        pBinder->pMediumName,
                        pBinder->uMediumUint,
                        pBinder->uMediumType,4,5,6);

        }    
    
    USBTGT_END_DBG("usbTgtBinderMediumDetach successfully\n",
                   1, 2, 3, 4, 5, 6);
    
    return status;
    }



