/* usbOtg.c - USB OTG Framework with OTG State Machine and API routines */

/*
 * Copyright (c) 2010, 2011, 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01l,04nov13,wyy  Remove compiler warning (WIND00442017)
01k,06may13,s_z  Remove compiler warning (WIND00356717)
01j,04jan13,s_z  Remove compiler warning (WIND00390357)
01i,21apr11,w_x  Reset a_bus_suspend/a_bus_resume when entering b_peripheral
01h,15apr11,w_x  Force to enable new stack mode after role switch (WIND00267720)
01g,08apr11,w_x  cleared Coverity CHECKED_RETURN for vxAtomicSet (WIND00264893)
01f,30mar11,w_x  correct stack uninitialization with OCD (WIND00262787) 
01e,25mar11,w_x  reset some extended parameters when unused (WIND00263292)
01d,23mar11,w_x  address more code review comments and documentation cleanup
01c,08mar11,w_x  address code review comments and testing reported issues
01b,16feb11,w_x  don't USBOTG_AUTO_SUSP2DIS_ENABLE on receiving b_hnp_enable
01a,30may10,w_x  written
*/

/*
DESCRIPTION

This file is the VxWorks USB OTG Framework, with OTG State Machine and Core API
routines.

The following documentation is provided as a reference for developers to 
understand the details of the OTG stack. Controller driver developers are
encounraged to understand the component model and the driver initialization.
Application developers may just need to understand the user level APIs. 
Further implementation details are also provided here but they are only 
for stack maintainers.

\h 1 VXWORKS USB OTG COMPONENT MODEL

\h 1.1 VxWorks USB OTG Stack Overview

This documentation will use the following concepts;

- an USB Controller could be an integration of Host Controller, Target Controller,
and OTG Controller; the combination can make OTG Dual-Role capable device,
or SRP-capable peripheral-only B-device, or SRP-capable Host-only A-device, or 
simply normal single role Host or Target (we call then Pure Host or Pure Target).

- an OTG Controller Driver (OCD) implements USBOTG_OCD interfaces.

- a Host Controller Driver (HCD) implements USBHST_HC_DRIVER interfaces.

- a Target Controller Driver (TCD) implements USBTGT_TCD interfaces.

- an OTG Manager is an instance of USBOTG_MANAGER which forms OTG Framework.

The chart below is a simplified architecture of the VxWorks USB OTG Stack with
OTG Manager as the core control unit.

\cs
                        VxWorks USB OTG Stack Overview
  |------------------------------------------------------------------|
  |                         User Interfaces                          |
  |      (VxWorks Middleware, e.g, FS, NWK, WIND ML, or iosLib)      |
  |-------------@------------------@-------------------@-------------|
                |                  |                   |
  |-------------@-----------\      |      /------------@-------------|
  | Host Class Drivers      |      |      | Target Function Drivers  |
  |---^------\              |      |      |             /-------@----|
      |RPT   | USB Ethernet |      |      | USB RNDIS   |       |
  |---@---|  | HID          |      |      | USB Serial  |  |----@----|
  |  Bus  |  | Mass Storage |      |      | Mass Storage|  |Composite|
  |Monitor|  | ...          |      |      | ...         |  |Framework|
  |---@---|  |----@---------/      |      \----------@--|  |---@-----|
      |           |                |                 |         |
      |           |      |---------@----------|      |         |
      |           |   RPT|                    |RPT   |         |
      |           |   -->|    OTG Manager     |<--   |         |
  |---@-----------@--/   |   (State Machine   |   \--@---------@-----|
  |     Host USBD    |   |    Stack Switch)   |   |   Target TML     |
  |                  |   \------^------@------/   |                  |
  |---^------@-------/          |      |          \-------^------@---|
     R|      |C                R|      |C                R|      |C
     P|      |T                P|      |T                P|      |T
     T|      |L                T|      |L                T|      |L
  |---@------v-----|            |      |             |----@------v---|
  |   Host Ctlr    |LD/ULD |----@------v----| LD/ULD |   Target Ctlr |
  |     Driver     |<------|                |------->|    Driver     |
  |--------@-----^-|EN/DIS |    OTG Ctlr    | EN/DIS |-^-----@-------|
           |  RPT|         |     Driver     |          |RPT  |
           |  |--@---------/                 \-------@----|  |
           |  |         Common Interrupt Handling         |  |
           |  |---------------------@---------------------|  |
           |                        |                        |
  |--------@------------------------@------------------------@-------|
  |                        USB OTG Dual-Role Ctlr                    |
  |            (Composed by Host, Target, and OTG Controller)        |
  |------------------------------------------------------------------|

RPT = Report
CTL = Control
EN  = Enable
DIS = Disable
LD  = Load
ULD = Unload

\ce

\h 1.2 VxWorks USB OTG Driver Components

When creating drivers for an OTG Dual-Role capable device, the developer needs
to create the following:

- one OTG Controller Driver (OCD)

- one Host Controller Driver (HCD)

- one Target Controller Driver (TCD)

When creating drivers for a SRP-capable peripheral-only B-device, the developer
needs to create the following:

- one OTG Controller Driver (OCD)

- one Target Controller Driver (TCD)

When creating drivers for a SRP-capable Host-only A-device, the developer
needs to create the following:

- one OTG Controller Driver (OCD)

- one Host Controller Driver (HCD)

The following description uses the MUSBMHDRC as an example to show the required
settings.

\h 1.3 VxWorks USB Stack CDF File <40usb.cdf>

The VxWorks CDF file <40usb.cdf> will need to have one component created for
each of the Controller Drivers. For example, the MUSBMHDRC (such as one in TI
OMAP3530) is an OTG Dual-Role capable USB Controller, it will have following
components in the <40usb.cdf>:

- INCLUDE_USB_MHDRC_HCD

- INCLUDE_USB_MHDRC_TCD

- INCLUDE_USB_MHDRC_OCD

Besides the dependency on both INCLUDE_USB_MHDRC_HCD and INCLUDE_USB_MHDRC_TCD,
the INCLUDE_USB_MHDRC_OCD will have dependency on the OTG Framework component
which is implemented by the INCLUDE_USB_OTG component:

- INCLUDE_USB_OTG

The user could select either INCLUDE_USB_MHDRC_HCD or INCLUDE_USB_MHDRC_TCD to 
create Non-OTG usage model (either Host or Target). However, if the user wants 
to use it as OTG model, then INCLUDE_USB_MHDRC_OCD must be selected, and that
will pull in all supporting components.

NOTE:

There are five usage models that can be supported by the above components using
an OTG Dual-Role capable USB controller.

\ml

\m - Pure Host
\m - Pure Target
\m - SRP capable Host-only A-device
\m - SRP capable Peripheral-only B-device
\m - OTG Dual-Role capable device

\me

To aid users to select the desired usage models, some initialization components
can be implemented.

- INCLUDE_USB_MHDRC_HCD_INIT

Initializes HCD and registers the driver with VxBus; when this component is 
selected, it will only pull in the HCD and exclude TCD component. This makes 
the Pure Host usage model.

Requires INCLUDE_USB_MHDRC_HCD;

Excludes INCLUDE_USB_MHDRC_TCD_INIT.

- INCLUDE_USB_MHDRC_TCD_INIT

Initializes TCD and registers the driver with VxBus; when this component is 
selected, it will only pull in the TCD and exclude HCD component. This makes 
the Pure Target usage model.

Requires INCLUDE_USB_MHDRC_TCD; 

Excludes INCLUDE_USB_MHDRC_HCD_INIT.

- INCLUDE_USB_MHDRC_OCD_INIT

Initializes OCD and registers the driver with VxBus; this component is hidden
from user selection but is set as dependent component for the other two OTG 
usage model components, namely the INCLUDE_USB_MHDRC_OCD_HCD_INIT and the
INCLUDE_USB_MHDRC_OCD_HCD_INIT. So this component will be automatically 
selected when OTG usage model is selected by the user.

Requires INCLUDE_USB_MHDRC_OCD.

- INCLUDE_USB_MHDRC_OCD_HCD_INIT

Initializes both OCD and HCD, and registers them with VxBus; when this 
component is selected, the OCD and HCD component as well as OCD initialization
components are included, but it also excludes the pure HCD and pure TCD 
initialization components. This makes the SRP-capable Host-only A-device 
usage model.

Requires INCLUDE_USB_MHDRC_OCD, INCLUDE_USB_MHDRC_HCD, and 
INCLUDE_USB_MHDRC_OCD_INIT; 

Excludes INCLUDE_USB_MHDRC_HCD_INIT and INCLUDE_USB_MHDRC_TCD_INIT.

- INCLUDE_USB_MHDRC_OCD_TCD_INIT

Initializes both OCD and TCD, and registers them with VxBus; when this 
component is selected, the OCD and TCD component as well as OCD initialization 
components are included, but it also excludes the pure HCD and pure TCD 
initialization components. This makes the SRP-capable Peripheral-only B-device
usage model.

Requires INCLUDE_USB_MHDRC_OCD, INCLUDE_USB_MHDRC_TCD, and 
INCLUDE_USB_MHDRC_OCD_INIT; 

Excludes INCLUDE_USB_MHDRC_HCD_INIT and INCLUDE_USB_MHDRC_TCD_INIT.

NOTE:

There is no dedicated component for OTG Dual-Role usage model, this model is 
supported when both the INCLUDE_USB_MHDRC_OCD_TCD_INIT and 
INCLUDE_USB_MHDRC_OCD_TCD_INIT components are selected.

NOTE:

The INCLUDE_USB_MHDRC_OCD_INIT component is not exported in the Workbench 
Component Configuration page (hidden from user selection). This is because 
the OCD cannot work alone; it needs at least either HCD or TCD, or both HCD 
and TCD to make a valid usage model.

NOTE:

If the OTG Controller hardware supports only SRP-capable Host-only A-device 
or SRP-capable Peripheral-only B-device usage models, the CDF component 
definition should eliminate the non-available TCD or HCD component, and 
the corresponding OCD/TCD or OCD/HCD initialization component.

NOTE:

Some fake "OTG Controller" hardware does not support both SRP and HNP, but 
it could report ID pin status, reflecting it is A-device or B-device, and 
thus could be initialized to be working in either Host or Target mode. 
Using such fake "OTG Controller", users could only implement "insertion 
based stack switching", so that when Mini-A plug is inserted (as A-device), 
switch to Host Stack, and when no plug or Mini-B plug is inserted, switch 
to Target Stack. For such fake "OTG Controller", all driver components 
should be implemented, only that the OCD doesn't report SRP or HNP related 
attributes and events. The functionalities of usrUsbOtgHostRequest() and 
usrUsbOtgHostGiveup() APIs are also limited for this kind of controllers.

\h 1.4 VxWorks BSP Hardware Configure File <hwconf.c>

The CDF component configuration (Workbench or vxprj) can only specify the 
overall usage models at the driver level, but it cannot specify usage models
at instance level. That is to say, after the user selects a combination of 
driver components for a driver level usage model, in some cases instance 
level usage model would be still required (if multiple OTG controller instance
of the same IP is mounted on one single system). If instance level usage model
must be supported, BSP resource configuration mechanisms should be used to 
identify each OTG controller instance usage model selections. This section 
describes the mechanisms for the implementation of various instance level 
usage models. 

The following hardware configurations could be supported by these mechanisms:

- Single OTG capable USB Controller instance on one system
- Multiple OTG capable USB Controller instances using different 
  IP cores on one system
- Multiple OTG capable USB Controller instances using same IP 
  core on one system

NOTE: 

The mechanisms are documented in the following sub-sections to facilitate 
developers writing drivers for very complicated system hardware configuration. 
Multiple OTG controller instances cases are described using the MUSBMHDRC 
as examples by assuming multiple MUSBMHDRC based OTG controllers are mounted 
on one system, through either PLB or PCI. It might look complicated at the 
first glance of several configuration entries. 

However, in almost all cases, an OTG enabled system will have only one OTG 
controller instance in one system, so in this simple case, the driver level 
usage model selection could be used. The OMAP3530 BSPs supported by this 
release all have one single MUSBMHDRC mounted, so the actual implementation 
used in these BSPs are using the driver level usage model selection. 
Developers writing controller drivers for the simple case as OMAP3530 can 
ignore the complicated cases as described here, and just refer to the 
supported BSPs in this release as examples to derive their own configurations, 
by just changing the component name to match their own need.

\h 1.4.1 PLB based OTG controllers

The hwconf.c provides VxBus device instances and related resources and 
parameters for PLB based devices in its struct hcfDevice hcfDeviceList[] array, 
one entry in this array corresponds to one VxBus device instance.

A typical configuration for OTG enabled system has only one OTG capable USB 
Controller instance (for example, the OMAP3530 has only one MUSBMHDRC instance).

However, we can support multiple USB controllers on a system with hybrid usage 
configurations, for example, multiple USB OTG controllers of the same 
controller IP (using the same set of driver components), but they are 
configured as various usage models. As an example, the OMAP3530 has an 
on-chip MUSBMHDRC OTG Controller; now consider an OMAP3530 system mounted 
with an external TUSB6010B off-chip USB OTG Controller which also uses the 
MUSBMHDRC OTG Controller IP core. In this setup, there are two MUSBMHDRC 
OTG Controllers, and thus should be supported using the same set of drivers 
(INCLUDE_USB_MHDRC_HCD, INCLUDE_USB_MHDRC_TCD and INCLUDE_USB_MHDRC_OCD). 
The two OTG Controllers correspond to two VxBus device instances, 
differentiated by the "devUnit" value. 

The struct hcfDevice hcfDeviceList[] array use the same way to fill the device 
entries for the two OTG Controller instances:

const struct hcfDevice hcfDeviceList[] = {

/@ ...omit other unrelated entries... @/

/@ Dual-Role, SRP-capable Host-only, or SRP-capable Peripheral-only @/

#if defined (INCLUDE_USB_MHDRC_OCD) && 
(defined (INCLUDE_USB_MHDRC_HCD) || defined (INCLUDE_USB_MHDRC_TCD))
   { "vxbPlbUsbMhdrcOcd", 0, VXB_BUSID_PLB, 0, vxbPlbUsbMhdrcDevNum0, vxbPlbUsbMhdrcDevResources0 },
#endif /@ INCLUDE_USB_MHDRC_OCD ... @/

/@ Pure Host @/

#if defined (INCLUDE_USB_MHDRC_HCD) && 
    !defined (INCLUDE_USB_MHDRC_TCD) &&
    !defined (INCLUDE_USB_MHDRC_OCD) 
   { "vxbPlbUsbMhdrcHcd", 0, VXB_BUSID_PLB, 0, vxbPlbUsbMhdrcDevNum0, vxbPlbUsbMhdrcDevResources0 },
#endif /@ INCLUDE_USB_MHDRC_HCD ... @/

/@ Pure Peripheral @/

#if defined (INCLUDE_USB_MHDRC_TCD) && 
    !defined (INCLUDE_USB_MHDRC_HCD) &&
    !defined (INCLUDE_USB_MHDRC_OCD)
   { "vxbPlbUsbMhdrcTcd", 0, VXB_BUSID_PLB, 0, vxbPlbUsbMhdrcDevNum0, vxbPlbUsbMhdrcDevResources0 },
#endif /@ INCLUDE_USB_MHDRC_TCD ... @/

/@ Dual-Role, SRP-capable Host-only, or SRP-capable Peripheral-only @/

#if defined (INCLUDE_USB_MHDRC_OCD) && 
(defined (INCLUDE_USB_MHDRC_HCD) || defined (INCLUDE_USB_MHDRC_TCD))
   { "vxbPlbUsbMhdrcOcd", 1, VXB_BUSID_PLB, 0, vxbPlbUsbMhdrcDevNum1, vxbPlbUsbMhdrcDevResources1 },
#endif /@ INCLUDE_USB_MHDRC_OCD ... @/

/@ Pure Host @/

#if defined (INCLUDE_USB_MHDRC_HCD) && 
    !defined (INCLUDE_USB_MHDRC_TCD) &&
    !defined (INCLUDE_USB_MHDRC_OCD) 
   { "vxbPlbUsbMhdrcHcd", 1, VXB_BUSID_PLB, 0, vxbPlbUsbMhdrcDevNum1, vxbPlbUsbMhdrcDevResources1 },
#endif /@ INCLUDE_USB_MHDRC_HCD ... @/

/@ Pure Peripheral @/

#if defined (INCLUDE_USB_MHDRC_TCD) && 
    !defined (INCLUDE_USB_MHDRC_HCD) &&
    !defined (INCLUDE_USB_MHDRC_OCD)
   { "vxbPlbUsbMhdrcTcd", 1, VXB_BUSID_PLB, 0, vxbPlbUsbMhdrcDevNum1, vxbPlbUsbMhdrcDevResources1 },
#endif /@ INCLUDE_USB_MHDRC_TCD ... @/

/@ ...omit other unrelated entries... @/

};

With the above code, we could implement the following:

1) When only INCLUDE_USB_MHDRC_HCD or INCLUDE_USB_MHDRC_TCD component is 
included, there will be only one hcfDevice entry for one USB Controller 
instance, and it makes single role Pure Host or Pure Target usage model. 
In this case, instance level usage model control is not required. 

2) When the INCLUDE_USB_MHDRC_OCD component is included (and either or both 
INCLUDE_USB_MHDRC_HCD and INCLUDE_USB_MHDRC_TCD are included), it makes 
driver level Dual-Role, SRP-capable Host-only, or SRP-capable Peripheral-only 
usage models. 

- If INCLUDE_USB_MHDRC_OCD, INCLUDE_USB_MHDRC_HCD and INCLUDE_USB_MHDRC_TCD 
  are all included, all driver components are available; in this case, one 
  OTG Controller instance may be used as Dual-Role usage model, but the other
  OTG Controller instance may be used as any other usage models (SRP-capable 
  Host-only, or SRP-capable Peripheral-only). 
- If INCLUDE_USB_MHDRC_OCD and INCLUDE_USB_MHDRC_HCD are included, the only 
  available driver usage model is "SRP-capable Host-only".
- If INCLUDE_USB_MHDRC_OCD and INCLUDE_USB_MHDRC_TCD are included, the only 
  available driver usage model is "SRP-capable Peripheral-only".
3) In a system with multiple OTG capable controllers of the same IP, if one 
  of the OTG capable USB Controllers needs to be used as Pure Host or Pure 
  Target while other instances must be used as OTG usage model, then we could
  just specify the dedicated Pure Host or Pure Target entry in the struct 
  hcfDevice hcfDeviceList[]. For example, in the above example, if the second
  MUSBMHDRC OTG Controller instance needs to be used as Pure Host, then we 
  could just specify: 

/@ Pure Host @/

#if defined (INCLUDE_USB_MHDRC_HCD) 
   { "vxbPlbUsbMhdrcHcd", 1, VXB_BUSID_PLB, 0, vxbPlbUsbMhdrcDevNum1, vxbPlbUsbMhdrcDevResources1 },
#endif /@ INCLUDE_USB_MHDRC_HCD @/

4) When all controller driver components are selected, we could choose to use 
"VxBus Parameter Override" mechanism to support instance level driver usage 
control if multiple controller instances of the same IP are mounted in one 
system. 

The $(WIND_BASE)/target/h/usb/usbOtg.h defines the 5 possible usage models.

typedef enum usbotg_ctlr_usage
    {
    USBOTG_DURAL_ROLE = 0,
    USBOTG_SRP_HOST = 1,
    USBOTG_SRP_TARGET = 2,
    USBOTG_PURE_HOST = 3,
    USBOTG_PURE_TARGET = 4,
    }USBOTG_CTLR_USAGE;

VXB_INST_PARAM_OVERRIDE sysInstParamTable[] = {

#ifdef INCLUDE_USB_MHDRC_OCD

#if defined(INCLUDE_USB_MHDRC_TCD) && defined (INCLUDE_USB_MHDRC_HCD)

{ " vxbPlbUsbMhdrcOcd ", 0, "otgCtlrUsage",   VXB_PARAM_INT32,   { USBOTG_DURAL_ROLE } }, 

/@ Can also be USBOTG_SRP_HOST or USBOTG_SRP_TARGET @/

#endif

#if defined(INCLUDE_USB_MHDRC_TCD) && !defined (INCLUDE_USB_MHDRC_HCD)

{ " vxbPlbUsbMhdrcOcd ", 0, "otgCtlrUsage",   VXB_PARAM_INT32,   { USBOTG_SRP_TARGET } }, 

/@ Can only be USBOTG_SRP_TARGET @/

#endif 

#if defined(INCLUDE_USB_MHDRC_HCD) && !defined (INCLUDE_USB_MHDRC_TCD)

{ " vxbPlbUsbMhdrcOcd", 0, "otgCtlrUsage",   VXB_PARAM_INT32,   { USBOTG_SRP_TARGET } }, 

/@ Can only be USBOTG_SRP_HOST @/

#endif

#endif /@ INCLUDE_USB_MHDRC_OCD @/

    { NULL, 0, NULL, VXB_PARAM_END_OF_LIST, { (void *)0 } }
};

During the OTG Controller Driver initialization, once the OTG Manager tries 
to load the HCD or the TCD, depending on the "otgCtlrUsage", the OCD may 
purposely fail the HCD or TCD loading if the usage model doesn��t require 
the corresponding HCD or TCD. 

\h 1.4.2 PCI based OTG controllers

For a PCI based OTG Controller, the VxBus device instance for it is created 
dynamically during PCI bus scanning, and there is no hwconf.c hcfDevice entry 
for the instance. The device-driver matching for PCI devices are through the
"devProbe" interface of the PCI_DRIVER_REGISTRATION (if a matching is found, 
the routine should return TRUE, otherwise return FALSE.).

Consider the case for a PCI based MUSBMHDRC OTG Controller (e.g, the TUSB6010B 
to be mounted on a PCI card), using the same component model as described in 
previous sections. 

1) If INCLUDE_USB_MHDRC_OCD is included, either or both INCLUDE_USB_MHDRC_HCD 
   and INCLUDE_USB_MHDRC_TCD are included. In this case, we need to use the 
   OCD to actually load the HCD and/or TCD. During the VxBus PCI device 
   scanning (probing), we should only return TRUE when OCD probing routine is 
   called, and return FALSE when HCD or TCD probing is called (if the HCD or 
   TCD probing routine returns TRUE, the VxBus will call the HCD or TCD 
   devInstanceConnect() and thus consider the instance as Pure Host or Pure
   Target usage model). 
2) If INCLUDE_USB_MHDRC_OCD is not included (either INCLUDE_USB_MHDRC_HCD or 
   INCLUDE_USB_MHDRC_TCD is selected), meaning either Pure Host or Pure Target 
   usage model is selected, then the HCD or TCD probing routine should return
   TRUE.

- Implement a usrUsbIsMhdrcOtgEnabled() routine to indicate if OTG usage model 
  is selected by the user in the file $(WIND_BASE)/target/config/comps/src/usrUsbOtgInit.c;
- HCD and TCD PCI probing routine only return TRUE if usrUsbIsMhdrcOtgEnabled() 
  returns FALSE (meaning there is no OCD component included) and PCI VID/PCI/PGIF 
  matching is passed.
- OCD loads HCD/TCD if included, otherwise the behavior is same as normal 

BOOL usrUsbIsMhdrcOtgEnabled (void)
    {
    #ifdef INCLUDE_USB_MHDRC_OCD
    return TRUE;
    #else
    return FALSE;
    #endif
    }

BOOL usbMhdrcOcdDevicePciProbe
    (
    VXB_DEVICE_ID       pDev
    )
    {
    if (PCI_ID_MATCH_MHDRC(pDev) == TRUE)
        return TRUE;
    else
        return FALSE;
    }

BOOL usbMhdrcHcdDevicePciProbe
    (
    VXB_DEVICE_ID       pDev
    )
    {
    /@ If OTG is enabled, HCD is loaded by OCD @/

    if (usrUsbIsMhdrcOtgEnabled() == TRUE)
        return FALSE;
    else if (PCI_ID_MATCH_MHDRC(pDev) == TRUE)
        return TRUE;
    else
        return FALSE;
    }

BOOL usbMhdrcTcdDevicePciProbe
    (
    VXB_DEVICE_ID       pDev
    )
    {
    /@ If OTG is enabled, TCD is loaded by OCD @/

    if (usrUsbIsMhdrcOtgEnabled() == TRUE)
        return FALSE;
    else if (PCI_ID_MATCH_MHDRC(pDev) == TRUE)
        return TRUE;
    else
        return FALSE;
    }

The instance level usage model can be still specified in the VXB_INST_PARAM_OVERRIDE 
sysInstParamTable[] in hwconf.c, but should use the PCI_DRIVER_REGISTRATION 
name as the entry name (for example, "vxbPciUsbMhdrcOcd"). 

NOTE:

The above mechanism for PCI based OTG controller is not currently implemented
by the controller drivers for MUSBMHDRC, because there is no actual PCI based 
MUSBMHDRC USB OTG controllers for testing.

\h 2 CONTROLLER DRIVERS INITIALIZATION

The Controller Driver initialization depends on the usage model, i.e, what
components are selected.

Both Non-OTG and OTG usage models are described below, taking the MUSBMHDRC
as an example.

\h 2.1 Non-OTG Usage Model

When only INCLUDE_USB_MHDRC_HCD or INCLUDE_USB_MHDRC_TCD is selected to work 
as Non-OTG usage model, the controller driver initialization is the same as 
typical VxBus device initialization procedure.

1) The HCD/TCD will register a DRIVER_REGISTRATION to VxBus Framework.

2) Then the VxBus creates a VXB_DEVICE_ID as the instance of the Host/Target
Controller, and calls the 'drvBusFuncs' of the HCD/TCD DRIVER_REGISTRATION.
The HCD/TCD should create a USBHST_HCD/USBTGT_TCD instance for the Host/Target
Controller instance.

3) During the initialization, the driver needs to populate the hardware information
by probing the hardware or retrieving user configurable parameters from the BSP.

4) As the last step of initialization, call Host or Target stack API routine to
add the new HCD or TCD instance.

With this setup, it doesn't change too much to the existing model of VxBus
device initialization.

\h 2.2 OTG Usage Model

When INCLUDE_USB_MHDRC_OCD is selected, both INCLUDE_USB_MHDRC_HCD and 
INCLUDE_USB_MHDRC_TCD are included, and the hwconf.c doesn't provide the 
corresponding hcfDevice entries for the Host and Target Controller instances, 
only one single entry for the OTG Controller instance is provided in hwconf.c.

1) With this setup, OCD, HCD, and TCD will all register their DRIVER_REGISTRATION
to VxBus Framework normally.

2) However, the VxBus creates only one single instance of VXB_DEVICE_ID for the
OTG Controller (representing the whole USB Controller). The VxBus then calls
the 'drvBusFuncs' of OCD DRIVER_REGISTRATION.

3) The following operations will happen during the OCD initialization for this
OTG Controller.

- devInstanceInit() could implement some board specific hardware bring up, such
as Pin Mux Configuration. For example, this can be done by calling devResourceGet()
to get a platform specific hardware initialization routine provided by the BSP.

NOTE: The BSP is responsible to provide such a resource if required.

- devInstanceInit2() can be null routine

- devInstanceConnect() performs as below:

a) Create an instance of USBOTG_OCD and a common data structure which will be
used to save common information among OCD, HCD and TCD.

NOTE: The driver developer should decide what are common among OCD, HCD and TCD,
and then define a common data structure to hold these information; These drivers
may need some other driver specific information, they could encapsulate these
together with the common data structure (as a pointer) to form a driver private
data structure. When the HCD/TCD is used in Non-OTG single role model, all these
information should be populated by the HCD/TCD itself; However, when the HCD/TCD
is used in OTG model, the OCD is responsible to populate the common information,
the HCD/TCD can retrieve the common information when they are loaded.

b) Call devResourceGet() to get the user configurable parameters, and probe
the hardware to retrieve the common hardware resource information to be saved in
the common data structure.

c) Reset the OTG Controller and do any IP specific hardware initialization so
that it is able to detect the ID pin state, and thus able to distinguish the
default role (A-device or B-device).

d) Populate the following data structure elements:

- The <'USBOTG_OCD::bmAttributes'> must be set with the OTG SRP/HNP/ADP support
attributes bitwise OR'ed mask at this stage, by probing the hardware or through
programming manual the driver writer should know what OTG capability the OTG
controller supports. This will be used to construct the OTG descriptor when
the hardware is working as peripheral.

- The <'USBOTG_OCD::pDev'> must be set to point to the VXB_DEVICE_ID passed in
to 'drvBusFuncs'. This VXB_DEVICE_ID will be used across OCD, HCD and TCD as the
single instance for the whole USB Controller.

- The <'USBOTG_OCD::pOcdFuncs'> must be set to point to the USBOTG_OCD_FUNCS
which has been filled with function pointers of the OCD. See <'DATA STRUCTURES'>
section for what functions pointers should be provided by an OCD.

- The <'VXB_DEVICE_ID::pDrvCtrl'> must be set to point to the common data
structure. This can be used by the HCD and TCD to retrieve the common information
when they are loaded by the OTG Manager Framework.

e) Call usbOtgOcdAdd() using the created USBOTG_OCD instance; The following
actions will be taken during this call:

- Create an USBOTG_MANAGER as the OTG Manager instance.

- Associate the USBOTG_OCD and USBOTG_MANAGER so that <'USBOTG_MANAGER::pOCD'>
points to the USBOTG_OCD instance, and the <'USBOTG_OCD::pOTG'> points to the
USBOTG_MANAGER instance.

- Create other OTG Manager Framework resources such as a watchdog timer which
could be used as the OTG timer if the OCD doesn't provide hardware OTG timers.

- Load Host and Target Controller Drivers by calling <'USBOTG_OCD::pLoadHCD()'>
and <'USBOTG_OCD::pLoadTCD()'> interface routines of the OTG Controller Driver.
See <'HOST AND TARGET CONTROLLER DRIVER OTG SUPPORT CONSIDERATIONS'> for details.

- Based on the current ID pin state, either the Host or the Target Stack will
be enabled.

- Create OTG Manager Task to advance the OTG State Machine according to various
OTG parameters.

- Add a character device into the iosLib which allows user application to access
and control the OTG Manager such as to request to become host role.

f) The OTG Controller Driver is responsible for interrupt handling for both
the Host and Target Controllers. Upon returning from usbOtgOcdAdd(), the OTG
Controller Driver connects and enables interrupts to get the stack running.

\h 3. HOST AND TARGET CONTROLLER DRIVER OTG SUPPORT CONSIDERATIONS

\h 3.1 Controller Driver Loading Support for OTG Usage Model

As described in the <'Controller Driver Initialization'> section, no matter if
OTG support is enabled or not, the Host Controller Driver and Target Controller
Driver can be registered to VxBus Framework normally (without the knowledge of
INCLUDE_USB_OTG).

When an instance of a Host/Target is to be added by the 'drvBusFuncs' of the
DRIVER_REGISTRATION registered by the HCD/TCD, it means the VxBus Framework is
adding a controller instance for Non-OTG usage model. In this case the normal
VxBus device initialization procedure applies.

To support OTG usage model, the HCD/TCD should be responsible to provide a
routine to allow the OCD to load the HCD or TCD driver instance for the
Host/Target. It is up to the driver developer to decide how this routine is
constructed, but it should pass the common hardware information (including 
the VXB_DEVICE_ID of the controller instance) to the HCD/TCD.

For example, the OCD for MUSBMHDRC could implement <'USBOTG_OCD::pLoadHCD()'>
as something like below:

1) In the code which is shared among HCD/TCD/OCD, a global function pointer
variable is declared and is set as NULL:

\cs

usbMhdrcHcdTcdInterfacePrototype gpUsbMhdrcHcdUnLoad = NULL;

\ce

2) In the HCD initialization code usbMhdrcHcdInit(), the global function 
pointer is set to the actual HCD instance add routine.

\cs

gpUsbMhdrcHcdLoad = usbMhdrcHcdInstanceAdd;

\ce

3) The component initialization process makes sure both HCD and TCD are 
initialized before OCD is initialized. This assures that if HCD or TCD 
components are included, then corresponding function pointers are initialized
to the actual function; but if HCD or TCD is not included, then the function
pointers are still kept as NULL. Using function pointers (instead of calling
the actual function directly) makes the HCD or TCD components not pulled 
into the final image if they are not needed, thus reduces the image size.

4) In the OCD, for the <'USBOTG_OCD::pLoadHCD()'>, the the global function 
pointer is called if it is not NULL. If the global function pointer is still
NULL, then it indicates no HCD component is included, thus it should return
ERROR to tell the OTG Manager.

\cs

STATUS usbMhdrcOcdLoadHCD
    (
    pUSBOTG_OCD         pOCD
    )
    {
    pUSB_MUSBMHDRC      pMHDRC;

    /@ Cast into platform specific data @/

    pMHDRC = (pUSB_MUSBMHDRC)pOCD->pOcdSpecific;

    if (gpUsbMhdrcHcdLoad)
        {
        return gpUsbMhdrcHcdLoad(pMHDRC);
        }
        
    USB_MHDRC_WARN("usbMhdrcOcdLoadHCD - No HCD included\n",
               1, 2, 3, 4, 5, 6);
               
    return ERROR;
    }

\ce

The following are the general guidelines for the HCD/TCD loading routine:

1) Do only minimum Host/Target specific hardware initialization in this routine,
leave all common hardware initialization to the OCD.

2) The OCD is also responsible for handling common interrupts, and only delivers
the Host/Target specific interrupts to the HCD/TCD. So this routine doesn't need
to connect or enable ISR (except for the ISR that is specific to either the Host
or Target).

3) Retrieve the common hardware information the parameter. This requires the 
HCD/TCD to have agreement on the common hardware information, and the HCD/TCD 
specific data structure should contain a pointer to that common hardware 
information structure. (If it is not OTG enabled, then the HCD/TCD should 
retrieve the information by its own means.)

4) Get "VXB_DEVICE_ID pDev" from the common data structure passed in to this 
routine.

5) Call Host or Target Stack API to add the HCD/TCD instance.

NOTE:

It is recommended that both HCD and TCD could implement the driver controller 
addition process with modular sub-routines:

\ml

\m - sub-routine to get common hardware information and resources
\m - sub-routine to add the controller instance to Host or Target Stack 
\m - sub-routine to connect and enable interrupts
\m - sub-routine to enable the HCD or TCD for operation (transfer scheduling)

\me

The controller removal process would be better to use the reverse procedure 
of these above sub-routines:

\ml 

\m - sub-routine to disable HCD or TCD operation
\m - sub-routine to disable interrupts
\m - sub-routine to remove controller instance from Host or Target Stack
\m - sub-routine to release common hardware information and resources

\me

With the above modular design, it is easier to use these common sub-routines 
across OCD, HCD and TCD. 

The MUSBMHDRC controller drivers supported by this stack can be used as 
examples to implement such modular and staged driver load, unload, enable 
and disable interfaces.

\h 3.2 OTG Controller Driver Interrupt Handling

\h 3.2.1 Reporting OTG Related Events to OTG Manager

The OCD ISR may report the following events to the OTG Manager through the 
OTG event notification API usbOtgEventRaise(). 

1) ID pin state

If the OTG Controller could generate interrupt if the ID pin state changes, 
it should be reported with USBOTG_EVENT_ID_ID_STATE_CHANGED.

This interrupt is not delivered to HCD or TCD.

NOTE:

The OCD must implement the USBOTG_OCD_FUNCS::pGetIdState() interface routine 
so that the OTG Manager could also poll the ID state.

2) VBUS state

If the OTG Controller could generate interrupt if the VBUS state changes across
these OTG defined VBUS thresholds, specially, when there is VBUS error 
conditions, the current VBUS state should be reported with 
USBOTG_EVENT_ID_VBUS_STATE_CHANGED.

This interrupt is not delivered to HCD or TCD.

NOTE:

The OCD must implement the USBOTG_OCD_FUNCS::pGetVbusState() interface routine
so that the OTG Manager could poll also the VBUS state.

3) SRP detected event

If the OTG Controller could generate interrupt if it detects a SRP request 
when it is A-device, the event should be reported with 
USBOTG_EVENT_ID_SRP_DETECTED.

This interrupt is not delivered to HCD or TCD.

NOTE:

The OCD may choose to implement the USBOTG_OCD_FUNCS::pGetSrpDetState() 
interface routine so that the OTG Manager could poll the SRP detect state 
when it is in OTG a_idle state. But this interface routine is not required,
since most OTG Controllers could report the event by interrupt.

4) Bus Reset event

The Bus Reset event must be reported to the OTG Manager as it is an indication 
to set the active stack to Target Stack (if it is not already in Target Stack). 
This is reported with USBOTG_EVENT_ID_BUS_RESET_DETECTED.

The Bus Reset event will also be delivered to the TCD.

5) Connect and Disconnect events

The Connect and Disconnect event must be reported to the OTG Manager as it may 
be used as an indication to set the active stack to Host Stack (if it is not 
already in Host Stack). This is reported with 
USBOTG_EVENT_ID_CONNECT_DISCONNECT_DETECTED.

The Connect and Disconnect events will also be delivered to the HCD.

6) Suspend and Resume (or Remote Wakeup) events

The Suspend and Resume (or Remote Wakeup) events must be reported to the OTG 
Manager as they are very important to advance the OTG State Machine for HNP. 
This is reported with USBOTG_EVENT_ID_SUSPEND_RESUME_DETECTED.

Suspend and Resume events will also be reported to the TCD, and Remote Wakeup 
event will be reported to the HCD.

\h 3.2.2 Deliver Interrupts to HCD or TCD

Some OTG related events such as ID change or SRP detection should be reported
only to OTG Manager; some interrupts such as the Connect and Disconnect events 
should be reported to both the OTG Manager and the HCD or TCD. Some interrupts, 
such as the data transfer related interrupts must only be reported to the 
proper controller driver (based on the current active stack) to be processed. 

The OCD implemented for the MUSBMHDRC can be used as an example for how to 
report the OTG related events to OTG manager, as well as deliver specific 
interrupts to HCD or TCD.

NOTE:

Because the OTG Manager depends on the "Bus Reset" and "Device Connect" event
to do the actual stack mode switching if it is not in the proper stack mode. 
The OCD should implement logic to make sure the "Bus Reset" event is always 
reported to OTG Manager and TCD (even if the current stack mode is still in 
Host mode); and the "Device Connect" event is always reported to OTG Manager 
and HCD (even if the current stack mode is still in Target mode). 
 
\h 4 DATA STRUCTURES

\h 4.1 USBOTG_MANAGER

Each USB OTG Controller will have a USBOTG_MANAGER data structure, which plays
the core role in managing the OTG states. The structure is defined as below:

\cs

/@
 * USB OTG Manager Abstraction
 @/

typedef struct usbotg_manager
    {
    DEV_HDR                 devHdr;     /@ I/O system device header @/
    NODE                    node;       /@ Node in the OTG controller list @/
    atomic_t                devOpened;  /@ TRUE if the OTG manager is opened @/
    VXB_DEVICE_ID           pDev;       /@ OTG Controller instance @/
    pUSBOTG_OCD             pOCD;       /@ Pointer to OTG Controller Driver @/
    pUSBOTG_OCD_FUNCS       pOcdFuncs;  /@ Short cut to OCD Functions @/
    BOOL                    isHcdLoaded;/@ Is HCD loaded @/
    BOOL                    isTcdLoaded;/@ Is TCD loaded @/
    USBOTG_STATE            prevState;  /@ Previous OTG state @/
    USBOTG_STATE            currState;  /@ Current OTG state @/
    USBOTG_ID_STATE         idState;    /@ Current ID state @/
    USBOTG_VBUS_STATE       vbusState;  /@ Current VBUS state @/
    USBOTG_PARAM            param;      /@ Current OTG parameters @/
    USBOTG_MODE             stackMode;  /@ Host or Target Stack active @/
    USBOTG_TIMER            otgTimer;   /@ Active OTG Timer ID @/
    WDOG_ID                 wdId;       /@ Watchdog Timer (maybe OTG timer) @/
    atomic_t                wdInUse;    /@ TRUE if watchdog used as OTG Timer @/
    SEM_ID                  otgLock;    /@ Lock for the OTG instance @/
    SEM_ID                  paramSem;   /@ Parameter changed notification @/
    TASK_ID                 mgrTaskId;  /@ Task ID for OTG manager @/
    int                     mgrInterval;/@ OTG manager task polling interval @/
    UINT16                  otgVersion; /@ bcdOTG version that we support @/
    UINT32                  hOtgDevice; /@ OTG device handle if non-zero @/
    UINT32                  uValidMagic;/@ Check if the structure is valid @/
    USBOTG_USR_CALLBACK *   pUsrCallback; /@ User notification callback @/
    void *                  pUsrContext;/@ User callback context @/
    }USBOTG_MANAGER, * pUSBOTG_MANAGER;


\ce

The following items describe the major elements of the USBOTG_MANAGER data
structure.

\h 4.1.1 <'USBOTG_MANAGER::devHdr'> and <'USBOTG_MANAGER::devOpened'>

These two elements are related the iosLib interface to allow user applications
to communicate with the OTG Manager. Each USBOTG_MANAGER will create one iosLib
device entry. Most interactions between user application and OTG Manger is through
<'ioctl'> interface, the detailed ioctl command codes are described in the
comment for <'usbOtgManagerIoctl()'> routine.

\h 4.1.2 <'USBOTG_MANAGER::node'>

This is used to link the OTG manager instance into a global list of OTG manager 
instances. Used for track and show the internal states.

\h 4.1.3 <'USBOTG_MANAGER::pOCD'> and <'USBOTG_MANAGER::pOcdFuncs'>

When an OTG controller instance is added, it creates a USBOTG_OCD data structure,
populates it with proper attributes and the pointer to the USBOTG_OCD_FUNCS, and
calls usbOtgOcdAdd() to add that OCD instance. The <'USBOTG_MANAGER::pOCD'> is
set to point to that OCD instance. The OTG Manager will need to call the OCD
functions frequently to update OTG parameters or control OTG hardware behavior,
so the <'USBOTG_MANAGER::pOcdFuncs'> is set to be a shot cut to the OCD functions
(so it is the same as <'USBOTG_OCD::pOcdFuncs'>).

\h 4.1.4 <'USBOTG_MANAGER::isHcdLoaded'> and <'USBOTG_MANAGER::isTcdLoaded'>

Flags used for indicating if HCD or TCD instance is loaded. Used to determine
if the related operations for HCD ot TCD can be performed (for example, if HCD
is not loaded, HNP can not be supported).

\h 4.1.5 <'USBOTG_MANAGER::prevState'> and <'USBOTG_MANAGER::currState'>

These two elements are the current OTG state and the previous state it was in.
We keep the <'USBOTG_MANAGER::prevState'> so that we know where we come from, in
some cases it could help to decide what actions to take while entering a state.

\h 4.1.6 <'USBOTG_MANAGER::idState'> and <'USBOTG_MANAGER::vbusState'>

Latest ID and VBUS state. They can be returned to the user application for diagnostics
or take specific actions to control the OTG Manager (and thus the OTG State Machine).

\h 4.1.7 <'USBOTG_MANAGER::param'>

The OTG State Machine is advanced as a result of parameter changes. The parameters
are classified into various groups according to the OTG specification. We also
added an "extend" parameter group, which is handy to keep some stack implementation
related variables. See the comments in usbOtg.h for more details.

\cs

/@
 * USB OTG State Machine Parameters
 @/

typedef struct usbOtgParam
    {
    USBOTG_INPUT_PARAM      input;
    USBOTG_INTERNAL_PARAM   internal;
    USBOTG_TIMEOUT_PARAM    timeout;
    USBOTG_OUTPUT_PARAM     output;
    USBOTG_EXTEND_PARAM     extend;
    }USBOTG_PARAM;

\ce

\h 4.1.8 <'USBOTG_MANAGER::stackMode'>

This is the current stack active mode. This can be used by the common ISR to direct
the interrupts to either HCD or TCD for further processing.

\h 4.1.9 <'USBOTG_MANAGER::otgTimer'> and <'USBOTG_MANAGER::wdId/wdInUse'>

The OTG State Machine involves several timers in specific states. At one point
in time, there may be at most one active timer. The <'USBOTG_MANAGER::otgTimer'>
specifies the current active timer.

Some OTG Controllers may provide hardware timer facility. For such hardware, the
OCD may implment the <'USBOTG_OCD_FUNCS::pOtgTimerStart()/pOtgTimerStop()'> to
let the OTG Manager to take advantage of the hardware timer facility. If there
is no such hardware timer facility, or the timer can not be started successfully,
then the OTG Manager will fall back to use the OS specific watchdog timer.

\h 4.1.10 <'USBOTG_MANAGER::otgLock'>

This is a mutex semaphore which is used to protect the USBOTG_MANAGER elements
being concurrent updated, especially the <'USBOTG_MANAGER::param'>. Because the
<'USBOTG_MANAGER::param'> may be changed by the user requests, or by OTG Notifier
functions reporting events, or by timers timeout, or by the internal OTG Manager
task while advancing the OTG State Machine.

\h 4.1.11 <'USBOTG_MANAGER::mgrTaskId'> and <'USBOTG_MANAGER::paramSem'> and 
<'USBOTG_MANAGER::mgrInterval'> 

The <'USBOTG_MANAGER::mgrTaskId'> specifies the task which will wait for the
parameter changed event notification <'USBOTG_MANAGER::paramSem'> with a
predefined timeout value <'USBOTG_MANAGER::mgrInterval'>, but it does not 
always depend on the event notification, thus it does not check if the wait 
exits because of timeout or not. This is to implement the following behavior:

1) If the parameters are changed due to user request, or the low level drivers
event report, or OTG timer timeout action, then the semaphore will be released
by these handler routines, and thus this OTG manager task would be activated by
the event. This is to avoid the need for taskDelay() which would always block
the task the fixed amount of time.

2) If there is no parameter change event notification, it doesn't mean there is
no parameter change. Some parameters may not be reported by event notifications,
for example, some hardware can not report ID pin changes through interrupt, thus
can not be noticed by the OTG Manager without explicit state polling, these
parameters must be polled. For this reason, the semaphore wait time out gives
us a chance to perform the parameter state polling and check if we need to
advance the OTG State Machine.

\h 4.1.12 <'USBOTG_MANAGER::otgVersion'>

The OTG version we support. It is set to the macro USBOTG_VERSION_SUPPORTED at
compile time. This can be used to construct the OTG descriptor bcdOTG filed when
we support OTG 2.0. We currently implement support for OTG 1.3.

NOTE: Although we implement the support for OTG 1.3, we have added enough code
to be easily extensible to support OTG 2.0. We also enabled the OTG 2.0 HNP
polling mechanism to support user request of HNP by HNP polling. This is useful 
when both ends of the OTG connection are using our OTG stack. If the other end
doesn't support HNP polling, then the behavior is the same as normal OTG.

\cs

#define USBOTG_VERSION0100              0x0100
#define USBOTG_VERSION0120              0x0120
#define USBOTG_VERSION0130              0x0130
#define USBOTG_VERSION0200              0x0200
#define USBOTG_VERSION_SUPPORTED        USBOTG_VERSION0130

\ce

\h 4.1.13 <'USBOTG_MANAGER::hOtgDevice'>

This is the device handle of the remote OTG device when we are in host mode. We
could use it to suspend the bus (starting HNP), or we could use it to get the
OTG status (for HNP polling).

\h 4.1.14 <'USBOTG_MANAGER::uValidMagic'>

A magic value which is set to USBOTG_MAGIC_VALID when the USBOTG_MANAGER is
created, and set to USBOTG_MAGIC_INVALID before it is destroyed. This is handy
for code that refers to the USBOTG_MANAGER instance to check if it is safe to
access the USBOTG_MANAGER (its value must be USBOTG_MAGIC_VALID).

\h 4.1.15 <'USBOTG_MANAGER::pUsrCallback'> and <'USBOTG_MANAGER::pUsrContext'>

Pointer to user application installed message callback and its context.

\h 4.2 USBOTG_OCD and USBOTG_OCD_FUNCS

Each OTG Controller will have an USBOTG_OCD instance, in which there is a pointer
to the USBOTG_OCD_FUNCS. These data structures are defined as below.

\cs

/@
 * OTG Controller Driver abstraction
 @/

typedef struct usbotg_ocd
    {
    /@ USB OTG Controller VxBus Device Instance @/

    VXB_DEVICE_ID       pDev;

    /@ OTG Controller Driver Functions @/

    pUSBOTG_OCD_FUNCS   pOcdFuncs;

    /@ Pointer to OTG Controller hardware specific data @/

    pVOID               pOcdSpecific;

    /@ Pointer to the OTG Manager Framework instance @/

    pUSBOTG_MANAGER     pOTG;

    /@ The OTG attributes that we support @/

    UINT8               bmAttributes;
    }USBOTG_OCD, *pUSBOTG_OCD;

/@
 * USB OTG Controller Driver Functions
 @/

typedef struct usbotg_ocd_funcs
    {
    /@ Get the VBUS state of OTG port @/

    STATUS (* pOtgVbusStateGet)
            (
            struct usbotg_ocd * pOCD,
            USBOTG_VBUS_STATE * pVbusState
            );

    /@ Get the ID state of OTG port @/

    STATUS (* pOtgIdStateGet)
            (
            struct usbotg_ocd * pOCD,
            USBOTG_ID_STATE *   pIdState
            );

    /@ Get the line state of OTG port @/

    BOOL (* pOtgIsLineStateSE0)
            (
            struct usbotg_ocd * pOCD
            );

    /@ Get the SRP Detection state of OTG port @/

    BOOL (* pOtgSrpDetStateGet)
            (
            struct usbotg_ocd * pOCD
            );

    /@
     * Enable or disable driving the VBUS
     @/

    STATUS (* pOtgDrvVbusEnable)
            (
            struct usbotg_ocd * pOCD,
            BOOL                bEn
            );

    /@
     * Enable or disable charging the VBUS
     * (Used for manually control of Vbus pulsing SRP)
     @/

    STATUS (* pOtgChrgVbusEnable)
            (
            struct usbotg_ocd * pOCD,
            BOOL                bEn
            );

    /@
     * Enable or disable discharging the VBUS
     * (Accelerate SRP initial conditions for Vbus pulsing SRP)
     @/

    STATUS (* pOtgDisChrgVbusEnable)
            (
            struct usbotg_ocd * pOCD,
            BOOL                bEn
            );

    /@
     * Enable or disable automatic SRP detection
     * of the OTG Controller
     @/

    STATUS (* pOtgAutoSrpDetEnable)
            (
            struct usbotg_ocd * pOCD,
            BOOL                bEn
            );

    /@
     * Enable or disable automatic Suspend to Disconnect
     * of the OTG Controller (support for automatic HNP)
     @/

    STATUS (* pOtgAutoSusp2DisEnable)
            (
            struct usbotg_ocd * pOCD,
            BOOL                bEn
            );

    /@
     * Enable or disable automatic Disconnect to Connect
     * of the OTG Controller (support for automatic HNP)
     @/

    STATUS (* pOtgAutoDis2ConEnable)
            (
            struct usbotg_ocd * pOCD,
            BOOL                bEn
            );

    /@
     * Enable or disable automatic Connect to Reset
     * of the OTG Controller (support for automatic HNP)
     @/

    STATUS (* pOtgAutoCon2RstEnable)
            (
            struct usbotg_ocd * pOCD,
            BOOL                bEn
            );

    /@ Enable or disable D+ pull-up of the OTG port @/

    STATUS (* pOtgDpPullUpEnable)
            (
            struct usbotg_ocd * pOCD,
            BOOL                bEn
            );

    /@ Enable or disable D+ pull-down of the OTG port @/

    STATUS (* pOtgDpPullDownEnable)
            (
            struct usbotg_ocd * pOCD,
            BOOL                bEn
            );

    /@ Enable or disable D- pull-down of the OTG port @/

    STATUS (* pOtgDmPullDownEnable)
            (
            struct usbotg_ocd * pOCD,
            BOOL                bEn
            );

    /@ Set the Controller to work in the requested operation mode @/

    STATUS (* pOtgSetCtlrMode)
            (
            struct usbotg_ocd * pOCD,
            USBOTG_MODE         mode
            );

    /@ Request the OTG Controller to initiate SRP signaling @/

    STATUS (* pOtgRequestSrp)
            (
            struct usbotg_ocd * pOCD,
            UINT32              uSrpTypeFlags
            );

    /@
     * Start the hardware OTG timer if the OTG Contorller
     * provides one
     @/

    STATUS (* pOtgTimerStart)
            (
            struct usbotg_ocd *     pOCD,
            USBOTG_TIMER            timerId,
            int                     timeout,
            usbOtgTimerTimeoutFunc  timeoutFunc
            );

    /@
     * Stop the hardware OTG timer if the it is using the
     * hardware timer as the OTG timer
     @/

    STATUS (* pOtgTimerStop)
            (
            struct usbotg_ocd *     pOCD,
            USBOTG_TIMER            timerId
            );

    /@ Load the HCD for the OTG enabled Host Controller @/

    STATUS (* pOtgLoadHCD)
            (
            struct usbotg_ocd *     pOCD
            );

    /@ Load the TCD for the OTG enabled Target Controller @/

    STATUS (* pOtgLoadTCD)
            (
            struct usbotg_ocd *     pOCD
            );

    /@ UnLoad the HCD for the OTG enabled Host Controller @/

    STATUS (* pOtgUnLoadHCD)
            (
            struct usbotg_ocd *     pOCD
            );

    /@ UnLoad the TCD for the OTG enabled Target Controller @/

    STATUS (* pOtgUnLoadTCD)
            (
            struct usbotg_ocd *     pOCD
            );

    /@ Function pointer to enable the TCD @/

    STATUS (* pOtgEnableTCD)
            (
            struct usbotg_ocd *     pOCD
            );

    /@ Function pointer to disable the TCD @/

    STATUS (* pOtgDisableTCD)
            (
            struct usbotg_ocd *     pOCD
            );

    /@ Fucntion pointer to enable the HCD @/

    STATUS (* pOtgEnableHCD)
            (
            struct usbotg_ocd *     pOCD
            );

    /@ Fucntion pointer to disable the HCD @/

    STATUS (* pOtgDisableHCD)
            (
            struct usbotg_ocd *     pOCD
            );
    }USBOTG_OCD_FUNCS, *pUSBOTG_OCD_FUNCS;
\ce

Most of the elments for USBOTG_OCD are referred to and described in the section
<'Controller Driver Initialization'>, so this section will only describe these
elements in USBOTG_OCD_FUNCS.

\h 4.2.1 <'USBOTG_OCD_FUNCS::pOtgVbusStateGet()'>

This interface routine must be implemented by the OCD. It must be able to detect
and report the current VBUS state. Note that in OTG 1.3 and earlier there were
4 different VBUS threshold levels (SessEnd, A-SessValid, B-SessValid, and Vbus-Valid),
but in OTG 2.0 it is reduced to 3 VBUS threshold levels by merging A-SessValid and
B-SessValid with SessValid. We also follow the OTG 2.0 defitions, so that the
implementation only needs to report 3 different VBUS states:

- USBOTG_VBUS_STATE_SessEnd (VBUS < VB_SESS_END)

- USBOTG_VBUS_STATE_SessValid (VB_SESS_END <= VBUS < VA_VBUS_VLD)

- USBOTG_VBUS_STATE_VBusValid (VA_VBUS_VLD <= VBUS)

If the OTG Controller can report VBUS changes through interrupt, such as VBUS 
at VBUS error conditions, the VBUS state could be reported by the OTG event 
USBOTG_EVENT_ID_VBUS_STATE_CHANGED.

\h 4.2.2 <'USBOTG_OCD_FUNCS::pOtgIdStateGet()'>

This interface routine must be implemented by the OCD. It must be able to detect
and report the current ID state.

If the OTG Controller can report ID changes through interrupt, the ID state 
could be reported by the OTG event USBOTG_EVENT_ID_ID_STATE_CHANGED.

\h 4.2.3 <'USBOTG_OCD_FUNCS::pOtgIsLineStateSE0()'>

This interface routine must be implemented by the OCD. It must be able to detect
and report the current D+/D- LINE state. Especially, the USBOTG_LINE_STATE_SE0
state is an important factor for the B-device to initiate SRP.

\h 4.2.4 <'USBOTG_OCD_FUNCS::pOtgSrpDetStateGet()'>

The OCD MAY choose to implement this interface routine, but is optional.

Most OTG Controllers report SRP detection through interrupt, so the SRP 
detection could be reported by the OTG USBOTG_EVENT_ID_SRP_DETECTED.

\h 4.2.5 <'USBOTG_OCD_FUNCS::pOtgDrvVbusEnable()'>

This interface routine must be implemented by the OCD. As A-device, it must be
able to enable or disable driving the VBUS (so that it could raise and maintain
at VbusValid, or fall below SessValid if disabled driving the VBUS). This is to
save power if there is no need to power the bus, or start the session if requested
by the B-device.

\h 4.2.6 <'USBOTG_OCD_FUNCS::pOtgChrgVbusEnable()'>

This interface routine only needs to be implemented if the OTG Controller needs
manually control of the VBUS pulsing SRP.

Some OTG Controllers need to explicitly enable or disable the VBUS charging for
the VBUS pulsing SRP (such as the ISP1362). For such OTG Controllers, the OCD
needs to implement this interface routine to enable or disable the VBUS charging.
The OTG Manager will use this interface to initiate the VBUS pulsing with proper
timing.

However, most OTG Controllers could implement automatical SRP process without
user software intervention, except only to set some register bit to kick start
of SRP (the underlying SRP type and timing of pulsing is controlled by hardware).
For such OTG Controllers, <'USBOTG_OCD_FUNCS::pOtgChrgVbusEnable()'> must be set
to NULL, and <'USBOTG_OCD_FUNCS::pOtgRequestSrp()'> must be implemented to allow
the OTG Manger to request to iniate the SRP process.

\h 4.2.7 <'USBOTG_OCD_FUNCS::pOtgDisChrgVbusEnable()'>

The OCD MAY choose to implement this interface routine, but is optional.

Before SRP can be initiated by the OTG Manager, it needs to make sure all SRP
initial conditions are met. One of the SRP initial conditions is the VBUS should
fall below the SessEnd threshold so that the VBUS pulsing can be correctly
interpreted by the A-device.

Some OTG Controllers provide some means to accelerate the SessEnd condition to
be met by using a discharging resister. For such OTG Controllers, the OCD could
implement this interface routine to enable or disable the VBUS discharging.

\h 4.2.8 <'USBOTG_OCD_FUNCS::pOtgAutoSrpDetEnable()'>

The OCD MAY choose to implement this interface routine, but is optional.

Most OTG Controllers report SRP detection through interrupt, the OCD may choose
to implement this routine to enable or disable the SRP detection interrupt. The
OTG Manager only needs to enable SRP detection while in a_idle state, and disable
it in other states.

If the SRP detection is always enabled, this interface routine can be no-op, or
just set <'USBOTG_OCD_FUNCS::pOtgAutoSrpDetEnable()'> to NULL.

\h 4.2.9 <'USBOTG_OCD_FUNCS::pOtgAutoSusp2DisEnable()'>

The OCD needs to implement this interface routine if the OTG Controller has
automatic Suspend to Disconnect feature for HNP and the feature can be enabled
or disabled at run time.

During HNP, while the B-device is in b_peripheral state, it shall be able to
detect the Suspend from the A-device and transition to the b_wait_acon state
within TB_AIDL_BDIS (Min 4ms, Max 150ms) (A-Idle to B-Disconnect Time) after
Suspend is detected at the B-device, and upon entering b_wait_acon state, the
B-device should turns off its pull-up resistor on D+, starts b_ase0_brst_tmr
timer, and waits for the A-device to signal a connect.

The A-Idle (Suspend) to B-Disconnect could be done by the software to detect
the A-device Suspend, then deassert B-device D+ as soon as possible. However,
due to unpredictable interrupt and software latency, it may be risky to use
pure software to handle this procedure (possibly exceed the 150ms TB_AIDL_BDIS
max limitation).

Some hardware provides hardware automatic assistance for such critical timing
requirement, so that once the A-device Suspend is detected, the B-device deasserts
its D+ pull-up automatically (without software intervention). If the hardware
provides such an automatic Suspend to Disconnect feature, the OCD should implement
the OtgAutoSus2DisEnable() interface to let the USB OTG Framework enable/disable
the feature when required; If the feature is always enabled, then the implementation
can just be no-op and return OK;

If the hardware doesn't provide such a feature, then the OCD must set the field
<'USBOTG_OCD_FUNCS::pOtgAutoSusp2DisEnable()'> to be NULL, the OTG Framework will
use this NULL value as an indication that it needs to handle the Suspend to
Disconnect procedure by software.

\h 4.2.10 <'USBOTG_OCD_FUNCS::pOtgAutoDis2ConEnable()'>

The OCD needs to implement this interface routine if the OTG Controller has
automatic Disconnect to Connect feature for HNP and the feature can be enabled
or disabled at run time.

During HNP, the B-disconnect occurs from the OTG a_suspend state and within 3ms
(TA_BDIS_ACON max), the A-device must enable the pull-up on the D+ in the
a_peripheral state to signal Connect to the B-device.

This could be done by the software to detect the B-device disconnection, then
assert A-device D+ pull-up as soon as possible. However, due to unpredictable
interrupt and software latency, it may be risky to use pure software to handle
this procedure (possibly exceed the 3ms TA_BDIS_ACON max limitation).

Some hardware provides hardware automatic assistance for such critical timing
requirement, so that once the B-device disconnection is detected, the A-device
signals connection to the B-device automatically (without software intervention).
If the hardware provides such an automatic Disconnect to Connect feature, the
OCD should implement the <'USBOTG_OCD_FUNCS::pOtgAutoDis2ConEnable()'> interface
to let the USB OTG Manager to enable/disable the feature when required; If the
feature is always enabled, then the implementation can just be no-op and return
OK;

If the hardware doesn't provide such a feature, then the OCD must set the field
<'USBOTG_OCD_FUNCS::pOtgAutoDis2ConEnable()'> as NULL, the OTG Manager will use
this NULL value as an indication that it needs to handle the disconnection to
connection procedure by software.

\h 4.2.11 <'USBOTG_OCD_FUNCS::pOtgAutoCon2RstEnable()'>

The OCD needs to implement this interface routine if the OTG Controller has
automatic Connect to Reset feature for HNP and the feature can be enabled
or disabled at run time.

During HNP, while the B-device is in b_wait_acon state, it shall be able to
detect the connect from the A-device and transition to the b_host state within
1ms (TB_ACON_BSE0 max) after D+ is detected high at the B-device, and upon
entering b_host state, the B-device should issue a bus reset, and start generating
SOF.

This could be done by the software to detect the A-device connection, then
assert B-device Bus Reset as soon as possible. However, due to unpredictable
interrupt and software latency, it may be risky to use pure software to handle
this procedure (possibly exceed the 1ms TB_ACON_BSE0 max limitation).

Some hardware provides hardware automatic assistance for such critical timing
requirement, so that once the A-device connection is detected, the B-device
signals Bus Reset to the A-device automatically (without software intervention).
If the hardware provides such an automatic Connect to Reset feature, the OCD
should implement the <'USBOTG_OCD_FUNCS::pOtgAutoCon2RstEnable()'> interface
to let the USB OTG Manager enable/disable the feature when required; If the
feature is always enabled, then the implementation can just be no-op and return
OK;

If the hardware doesn't provide such a feature, then the OCD must set the field
<'USBOTG_OCD_FUNCS::pOtgAutoCon2RstEnable()'> as NULL, the OTG Manager will use
this NULL value as an indication that it needs to handle the connection to reset
procedure by software.

\h 4.2.12 <'USBOTG_OCD_FUNCS::pOtgDpPullUpEnable()'>

The OCD needs to implement this interface routine if the OTG Controller needs to
initiate data line pulsing SRP manually by enabling and then disabling of D+ line
pull-up (in which case the <'USBOTG_OCD_FUNCS::pOtgRequestSrp()'> is set as NULL).

The OCD needs to implement this interface routine if the OTG Controller has no
hardware assistance for automatic B-Disconnect to A-Connect handling during HNP,
(in which case the <'USBOTG_OCD_FUNCS::pOtgAutoDis2ConEnable()'> is set as NULL).
And the OTG Manager should assert the A-device D+ pull-up to signal connection
to B-device.

In other cases, ifthe OTG Controller can perform autoamtic data line SRP, and
can do automatic B-Disconnect to A-Connect handling during HNP, then OCD should
set <'USBOTG_OCD_FUNCS::pOtgDpPullUpEnable()'> as NULL.

\h 4.2.13 <'USBOTG_OCD_FUNCS::pOtgDpPullDownEnable()'>

The OCD needs to implement this interface routine only if the OTG Controller needs
explicitly to enable or disable the D+ line pull-down by software at specific time
points.

When an OTG device or Embedded Host is idle or acting as a host, it shall
activate both the D+ and D- pull-down resistors. These resistors shall be within
the range of RPD as defined in [USB2.0].

When an OTG A-device or B-device is acting as a peripheral, it shall disable
the pull-down on the D+ line but shall maintain the D- pull-down.

Most hardware implement the D+ line pull-down enable/disable automatically, thus
there is no need for software to control, so in most cases this interface can be
set to NULL by the OCD. Some OTG Controllers, such as the ISP1362, still needs
to implement this controlling over software.

\h 4.2.14 <'USBOTG_OCD_FUNCS::pOtgDmPullDownEnable()'>

The OCD needs to implement this interface routine only if the OTG Controller needs
explicitly to enable or disable the D- line pull-down by software at specific time
points.

The D- line pull-down is always enabled in almost all cases, either it is in
host or peripheral mode. Even an OTG device changes between host and peripheral
roles, the D- pull-down shall remain asserted. This prevents the D- line on the
A-device from floating if the B-device is detached.

An OTG device is allowed to disable both pull-down resistors during the interval
of a packet transmission while acting as either a host or a peripheral. However
this kind of pull-down is mostly implemented by hardware autoamatically. Some
OTG Controllers, such as the ISP1362, still needs to implement this controlling
over software.

\h 4.2.15 <'USBOTG_OCD_FUNCS::pOtgSetCtlrMode()'>

The OCD needs to implement this interface routine only if the OTG Controller needs
explicitly to set the controller operation mode by software at specific time points.

During HNP, the peripheral and host will exchange role to each other. Some
hardware can switch into proper mode while detecting specific signaling at
specific time points. For example, when HNP is enabled on the B-device, if bus
suspend detected, some hardware could switch into host operation mode automatically.
However some hardware may need a manual setting to make the controller work in
either peripheral mode or host mode. If the hardware can switch into proper
operation mode automatically, the <'USBOTG_OCD_FUNCS::pOtgSetCtlrMode()'> could
be set to NULL, or do nothing. Otherwise, it must be implemented to set to the
proper operation mode.

\h 4.2.16 <'USBOTG_OCD_FUNCS::pOtgRequestSrp()'>

The OCD needs to implement this interface routine if the OTG Controller supports
automatic SRP signaling (by setting some register bits).

The OTG Controller may perform this request by pulsing the D+ data line, followed
by pulsing the VBUS line (if it is OTG 1.3 or earlier). The OCD may choose to
drive the signaling directly in this call, or delay the operation at a later time.
However it is recommended to do the SRP signaling ASAP.

The OTG Manager will make sure that the initial conditions for driving SRP are
satisfied before calling this interface routine.

As per OTG 2.0, the B-device may not attempt to start a new session until 1.5
sec (TB_SSEND_SRP) after VBUS has gone below 0.8V (VOTG_SESS_VLD), and both the 
D+ and D- data lines have been low (SE0) for at least 1 sec (TB_SE0_SRP min).
This ensures both the end of the previous session and that the A-device has
detected a disconnect condition from the B-device. To help the OTG Manager to
achieve the SRP initial condition, the <'USBOTG_OCD_FUNCS::pOtgIsLineStateSE0()'>
must be implemented by the OCD; and it is helpfull if the OCD also implements
<'USBOTG_OCD_FUNCS::pOtgDisChrgVbusEnable()'> interface routine if possible.

The OTG Controller Driver doesn't need to monitor or report SRP status. If the
SRP successes, the A-device will raise VBUS and the OTG Manager can get aware
of the result through the OTG Notifier callback or by probing the VBUS state.

A watchdog timer will be started once the SRP signaling is requested. If the
timer times out and the VBUS is still not valid, then the OTG Manager will
report failure to the user application and the user can request the SRP again
at some later time.

\h 4.2.17 <'USBOTG_OCD_FUNCS::pOtgTimerStart()/pOtgTimerStop()'>

The OCD needs to implement this interface routine if the OTG Controller supports
hardware OTG timer facility.

Some USB OTG Controller hardware provides hardware support for these USB OTG
timers, because some of the timers are in minimum 1ms granularity and not all
OS timers can always provide such fine granularity. If the OCD wants to use
such hardware OTG timers, it should implement the pair of interface routines
<'USBOTG_OCD_FUNCS::pOtgTimerStart()/pOtgTimerStop()'> and return OK when the
timer is successfully started/stopped; If the OTG Controller Driver doesn't
implement the rotines (set <'USBOTG_OCD_FUNCS::pOtgTimerStart()/pOtgTimerStop()'>
as NULL), or the call failes, then the OTG Manager will use an OS specific
watchdog timer.

\h 4.2.19 <'USBOTG_OCD_FUNCS::pOtgLoadHCD()/pOtgLoadTCD()/pOtgUnLoadHCD()/pOtgUnLoadTCD()'>

The OCD must implement these interface routines. The detailed process and how to
implement these routines are described in <'Controller Driver Initialization'>.

\h 4.2.20 <'USBOTG_OCD_FUNCS:: pOtgEnableHCD()/pOtgEnableTCD()/pOtgDisableHCD()/pOtgDisableTCD()'>

The OCD must implement these interface routines. The detailed process and how to
implement these routines are described in <'Controller Driver Initialization'>.

\h 5 USB OTG SRP IMPLEMENTATION NOTES

This section describes the VxWorks USB OTG stack implementation for SRP protocol.

In order to conserve power, an A-device is allowed to leave VBUS turned off
when the bus is not being used. The Session Request Protocol (SRP) allows a
B-device to request the A-device to turn on the power supply to the USB interface
(VBUS) and start a session. A session is defined as the period of time that
VBUS is powered. The session ends when VBUS is no longer powered.

\h 5.1 B-device user application detects VBUS state not valid

The B-device user application can get the current VBUS state through IOCTL command
USBOTG_IOCTL_VBUS_STATE_GET, which in turns calls the usbOtgVbusStateGet() to
get the VBUS state. The usbOtgVbusStateGet() actually gets the VBUS state through
a call to <'USBOTG_OCD_FUNCS::pOtgVbusStateGet()'>.

\h 5.2 B-device requests VBUS to be powered by A-device

If the current VBUS state is not valid, the user application could use the
IOCTL command USBOTG_IOCTL_VBUS_POWER_ON to request A-device to power up VBUS.
This in turn calls usbOtgVbusPowerOn(), and it will set the b_bus_req to TRUE.
Then the OTG Manager task is notified the parameter change.

\h 5.3 B-device waiting for SRP initial conditions to be met

The OTG Manager will wait for the SRP initial conditions to be met before actually
initiating SRP.

- It needs to call <'USBOTG_OCD_FUNCS::pOtgIsLineStateSE0()'> to get the current
line state, as one of the inital condition is it should be in SE0 state for
enough time (TB_SE0_SRP min). In OTG 2.0 this is around 1 sec.

- It may call <'USBOTG_OCD_FUNCS::pOtgDisChrgVbusEnable()'> to discharge the
VBUS to below SessEnd threashold as another initial condition requires.

\h 5.4 B-device initiats SRP

When all initial conditions are met for SRP, the OTG Manager could initiate SRP.
Depending on if there is automatical SRP hardware support, there may be two
ways to initiate SRP.

a) With automatical SRP hardware support

If there is automatical SRP hardware support, the OCD should implement the interface
routine <'USBOTG_OCD_FUNCS::pOtgRequestSrp()'>, and it is called with the expected
SRP type bit mask (Data line pulsing, VBUS pulsing). It is up to the OCD to decide
how and when to drive the SRP signaling (but it is recommended to drive it ASAP).
Also the SRP timing (Data line and VBUS pulsing length) is controlled by the hardware.

b) Without automatical SRP hardware support

If there is no automatical SRP hardware support, the OCD should set the interface
routine <'USBOTG_OCD_FUNCS::pOtgRequestSrp()'> as NULL. The OCD should implment
the <'USBOTG_OCD_FUNCS::pOtgDpPullUpEnable()'>; Optionally, if VBUS pulsing SRP
needs to be implemented, <'USBOTG_OCD_FUNCS::pOtgChrgVbusEnable()'> should be
implemented by the OCD.

Once the SRP initial conditions are met, the OTG Manager starts data line pulsing
by asserting D+ pull-up with a call to <'USBOTG_OCD_FUNCS::pOtgDpPullUpEnable()'>;
It then starts a watchdog timer 'USBOTG_TIMER_b_data_pulse_tmr', with timeout
set to be around 7ms (in the range 5ms to 10ms); Once the timer times out, in the
timer timeout handling routine, it deasserts the D+ pull-up; This forms a data
line pulse. It then initiates the VBUS pulsing by asserting the VBUS charging
using <'USBOTG_OCD_FUNCS::pOtgChrgVbusEnable()'> and starts a watchdog timer
with timeout set to around 20ms (in the range 0ms to 30ms), and in the timeout
handling it deasserts the VBUS charging, which effectively forms a VBUS pulse.

Note that the data line pulsing is required to be in the range 5ms to 10ms, if
the OTG controller has hardware timer support and the OCD provides implementation
for the hardware timer interface routines, then the data line pulsing will be
accurate. However, if there is no hardware timer support, the system watchdog
timer is used. In this case, the default sysClkRate of 60 is not accurate enough
to get a 5ms to 10ms data line pulsing. It is recommended to set the sysClkRate
to be at least 1000 in such a case.

No matter if there is automatical SRP support or not, once the SRP initiation is
done, the b_srp_done is set to TRUE and the OTG state moves back to b_idle again.

\h 5.5 B-device waits VBUS to be valid

Once the OTG state moves from b_srp_init back to b_idle and the b_srp_done is
TRUE (id pin also TRUE), the OTG Manager starts a 'USBOTG_TIMER_b_srp_fail_tmr'
timer, with timeout around 6000ms (6sec) (in the range 5sec to 6sec). If the
VBUS raises above VBUS valid threshold within the timeout, then the timer is
canceled. If the timer times out but VBUS still not valid, the OTG Manager
could report the SRP failure to the user, and the user could request SRP again.

\h 5.6 A-device detects SRP request

Once the B-device initiates the SRP, the data line and/or the VBUS pulsing could
be detected by the A-device. The A-device may trigger an interrupt, the OCD ISR
should report the SRP detection to the OTG Manager.

\h 5.7 A-device responses SRP by driving VBUS

In response to the SRP detection event, if the A-device user application could
allow the bus to be used (a_bus_drop set to FALSE), then the OTG Manager will
start driving the VBUS by calling <'USBOTG_OCD_FUNCS::pOtgDrvVbusEnable()'>,
and moves from a_idle state to a_wait_vrise, and up through to a_wait_bcon,
then a_host if the B-device signals connection.

\h 6 USB OTG HNP IMPLEMENTATION NOTES

This section describes the VxWorks USB OTG stack implementation for HNP protocol.

The Host Negotiation Protocol (HNP) allows the host function to be transferred
between two directly connected OTG devices and eliminates the need for a user
to switch the cable connections in order to allow a change in control of
communications between the devices. HNP will typically be initiated in response
to input from the user or an Application on the OTG B-device. HNP may only be
implemented through the Micro-AB receptacle on a device. The A-device is always
responsible for powering the USB interface regardless of whether it is acting
in host or peripheral role.

At the start of a session, the A-device defaults to having the role of host.
During a session, the role of host can be transferred back and forth between
the A-device and the B-device any number of times, using HNP.

The HNP protocol is the key of the USB OTG to allow dynamical role switching.

The HNP procedure is described in this section(assume the A-device is currently
working as a_host and B-device is working as b_peripheral).

\h 6.1 B-device exhibits HNP capability to A-device

B-device indicates its HNP capability in its USB OTG descriptor bmAttributes
HNP support bit field; This is done by B-device side Target Stack, with the
knowledge of the existence of the OTG support, by checking if <'USBOTG_OCD::pOTG'>
is set to non-NULL and <'USBOTG_OCD::usbOtgEnabled'> is set to TRUE. And then
retrieve the <'USBOTG_OCD::bmAttributes'> to fill the OTG descriptor when a
GetDescriptor(Configuration) is received. If we OTG 2.0 support is enabled, we
will also return the OTG descriptor when GetDescriptor(OTG) is received from
the host.

\h 6.2 A-device learns HNP capability from B-device

A-device learns B-device HNP capability from the USB OTG descriptor while
enumerating the B-device; This is done by the A-device side Host Stack. And the
Host Stack will inform HNP capability to the OTG Manager through the OTG event
USBOTG_EVENT_ID_OTG_DEVICE_ENUMERATED.

\h 6.3 A-device enables B-device for HNP

A-device sends a SetFeature(b_hnp_enable) command through the default control
pipe any time while it is working in the a_host state; Also it sets A-device
side a_set_b_hnp_en flag to TRUE if the previous command successfully completed;
Note that it is not required for the HNP procedure to happen immediately when
the SetFeature(b_hnp_enable) is issued, it is merely to tell the B-device that
it could consider that HNP is now allowed by the A-device and when the B-device
detects a bus suspend later, the B-device can assume the HNP procedure has been
actually started by the A-device, and it can follow the HNP procedure to proceed.

NOTE: 

Although SetFeature(b_hnp_enable) could be sent any time while the A-device
is working as host, we do this just after the OTG device is enumerated.

\h 6.4 B-device gets notified HNP to have been enabled

The B-device will also set its B-device side b_hnp_en flag to TRUE when it
receives the SetFeature(b_hnp_enable) command; This is done by the B-device
side Target Stack, informing the OTG Manager to set the flag, by raising OTG
event USBOTG_EVENT_ID_SET_OTG_FEATURE_RECEIVED.

\h 6.5 A-device actually starts HNP under user control

A-device finishes using the bus (in a_host), and tries to give a chance for
the B-device to take over the bus (switch from current b_peripheral state to
b_host state); This involves the following steps:

\h 6.5.1 A-device user application gives up host role

A-device side OTG Manager suspends the bus by calling the Host Stack
usbHstSuspendDevice() function in response to user application call to
usbOtgHostRoleGiveup() routine; This causes the Host Controller to stop
generating any SOF on the bus and thus there is no bus activity on the bus;
The D+/D- data lines go into IDLE line state (3ms later it will be bus suspend);
And the A-device goes into a_suspend state;

At this point in time the A-device OTG Manager will also start a watchdog
timer a_aidl_bdis_tmr. This timer is started by the A-device when it enters
the a_suspend state if HNP has been enabled (a_set_b_hnp_en = TRUE) (which
should have already happened in previous steps). During the watchdog timer
period TA_AIDL_BDIS (minimum 200ms) the A-device is expecting to detect B-device
disconnection. If the A-device does not detect a disconnect before TA_AIDL_BDIS
(a_aidl_bdis_tmout = TRUE), then the A-device is allowed to stop waiting for
a disconnect and end the session (a_aidl_bdis_tmout triggers a_wait_vfall).

\h 6.5.2 B-device detects A-device has started HNP, then disconnects from A-device

B-device will recognize the bus entering bus suspend state after 3ms
of the D+/D- going into IDLE line state; Since the B-device has indicated
its HNP capability to the A-device and b_hnp_en flag has been set, it will
consider this bus suspend state entry as an indication from the A-device to
have accepted and started the HNP role switch procedure; Note that there
may be a suspend event (maybe interrupt) generated, the OTG Controller Driver
should notify this event to the OTG Manager through raising the OTG event 
USBOTG_EVENT_ID_SUSPEND_RESUME_DETECTED. The OTG Manager will then proceed 
to cause a role switch as detailed below.

If the two connected devices were previously working in HS, the B-device
will now turn up it D+ pull-up, which effectively makes the data lines go
into FS (can be under software control or done by hardware automatically);
If the two devices were already working in FS, the B-device D+ should have
already been pulled-up; The B-device will now disconnect from the bus by
removing the D+ pull-up. Note that this disconnection should happen in no
more than 150ms (TB_AIDL_BDIS max) after the detection of the bus suspend;
If not, the A-device may have turned off its VBUS and the session will be
end.

The disconnection can be done by the hardware automatically or by software
intervention;

a) If the disconnection is done by hardware automatically upon detecting
bus suspend state, then that capability of auto-disconnection should be
enabled through <'USBOTG_OCD_FUNCS::pOtgAutoSusp2DisEnable()'>. By programming
some register control bits so that when the suspend is recognized by the
hardware can perform auto-disconnection.

Note that <'USBOTG_OCD_FUNCS::pOtgAutoSusp2DisEnable()'> should be called
by the OTG Manager in response to user application request on the B-device
before the HNP actually takes place.

NOTE: B-device user application requests host role by calling the API routine
usbOtgHostRoleRequest(), which sets b_bus_req to TRUE. The user application
may also request host role with <ASAP> set to TRUE, in which case the host
request flag is set and it may force the host to give up host role if it is
doing HNP polling.

NOTE: This API routine usbOtgHostRoleRequest() and other APIs are encapsulated
by iosLib interface, user applications should call them through IOCTL commands.
See <'USER APPLICATION IOCTL COMMANDS'> for more details.

b) If the disconnection is not done by hardware automatically upon detection
of bus suspend, for example, if the disconnection should be done by some
Soft Connect capability of the hardware through direct software control, then
the <'USBOTG_OCD_FUNCS::pOtgAutoSusp2DisEnable()'> must be set as NULL to
indicate there is no hardware assistance for "Suspend to Disconnect", which
leaves the disconnection to be handled by the OTG Manager to call another
interface <'USBOTG_OCD_FUNCS::pOtgDpPullUpEnable()'>  to control D+ pull-up
on detection of bus suspend.

No matter what caused the B-device disconnection, the B-device has now turned
off its D+ pull-up (and both D+/D- pull-down should also be asserted), so
the data line now goes into SE0 state; The OTG Manager will move B-device
OTG State Machine from previous b_peripheral state into b_wait_acon state;

At this point in time, the B-device OTG Framework will start a watchdog
timer b_ase0_brst_tmr. This timer is used by B-device in the b_wait_acon
state, to wait for the A-device to signal a connection (a_conn = TRUE).
During this timer period TB_ASE0_BRST (3.125ms minimum), the B-device is
expecting A-device to connect. If the A-device does not connect before
TB_ASE0_BRST (b_ase0_brst_tmout = TRUE), then the B-device shall return to
the b_peripheral state. Note that the TB_ASE0_BRST is specified as 3.125ms
min in OTG 1.3, but is specified as 155ms min in OTG 2.0. Anyway, to cover
the worst case, we will set the b_ase0_brst_tmr timeout period to be longer
than 155ms, even WAIT_FOREVER.

\h 6.5.3 A-device detects B-device disconnect, and connects to B-device as peripheral

A-device side Host Stack detects the B-device disconnection (although
the A-device bus has been suspended);

Note that there is a strict timing requirement at this point. The A-device
must transition to the peripheral role within 3ms (TA_BDIS_ACON max) after
detecting the bus becoming SE0 (due to the B-device turning off its D+
pull-up to disconnect). The A-device must also turn on its A-device side
D+ pull-up quickly at this stage (in the 3ms duration); The TA_BDIS_ACON
max is specified in OTG 1.3 as 3ms, but in OTG 2.0 it is specified as 150ms.
Anyway the A-device role switch and connection should happen as quickly as
it can, ensure worst case of 3ms.

The A-device is still in a_suspend state at this point. Once the B-device
disconnection is detected, the OTG Controller Driver should turn on the
D+ pull-up immediately (maybe in the ISR directly) and switch the hardware
into peripheral mode (if such a hardware mode switching is required). In
fact, to meet the 3ms TA_BDIS_ACON max time requirement, some OTG Controllers
may support some kind of hardware automatic assist, once enabled, the hardware
will detect the "B-device disconnection during suspend" situation and switch
the hardware into peripheral mode and assert the D+ pull-up automatically.
The OTG specification (6.7.2 A-device becoming Peripheral) describes a case
for this kind of automatic HNP response. If the hardware has such automatic
HNP response, then <'USBOTG_OCD_FUNCS::pOtgAutoDis2ConEnable()'> should be
set as the routine to be used to enable/disable such feature. If the feature
is always enabled, then this routine can just do nothing (but still needs
to be set as non-NULL). If there is no such hardware automatic assist, then
the <'USBOTG_OCD_FUNCS::pOtgAutoDis2ConEnable()'> of the OCD should be set
as NULL, which indicates to the OTG Manager to do the proper handling by
the D+ pull-up interface <'USBOTG_OCD_FUNCS::pOtgDpPullUpEnable()'>.

No matter if the hardware assist exists or not, once the B-device disconnection
event is detected by the A-device Host Controller Driver, it should report it
to the BusM through the Hub Class Diver Interrupt URB. The A-device Host
Stack will handle the B-device disconnection normally (for example, free
its class driver allocated resources and do any other device unloading stuff).
This OTG Controller Driver ISR should notify the B-device disconnection to
OTG Manager by raising OTG event USBOTG_EVENT_ID_CONNECT_DISCONNECT_DETECTED,
and the OTG Manager will advance the OTG State Machine into a_peripheral
state if it was in a_suspend before.

It might be desirable that we could switch from Host Stack into Target Stack
at this point (when the B-device disconnection is detected). However, the
following facts would also affect the design decision:

a) The stack switching would include firstly disable the Host Stack, and
then enable the Target Stack. This process may have to be done in task context
(maybe in the OTG Manager task context), the time taken to do this is not
fully predictable (although we would try the best to minimize the operations
taken in this process, we could not avoid the possibility of being preempted
by other higher priority task in the system), thus chances are that when we
finally successfully switch into the Target Stack, we may have missed the
Bus Reset issued from the B-device.

b) Considering TB_ACON_BSE0 (Max 1ms in OTG 1.3, Max 150ms in OTG 2.0), the
B-device at this point will also in a critical time, that it needs to assert
the Bus Reset as soon as it detects the A-device connection. The B-device
side may very likely involve some kind of hardware assist, so that once the
A-device signals the connection, the Bus Reset may be asserted immediately.

c) There is also a case that the B-device is truly physically disconnected
(detached) while in the HNP process (after A-device suspends the bus) (e.g,
the Mini-A plug or Mini-B plug removal, or simply the B-device fails HNP). In
this case switching into Target Stack makes no sense.

So, one point to be made clear is that once the D+ pull-up is asserted, the
B-device may immediately signal the Bus Reset to the A-device (quick as in
a few us time). So the design decision here is that we do not perform the
actual stack switching at the time when B-device disconnection is detected,
instead we continue in the Host Stack until a Bus Reset is received.

This design decision requires a common ISR used by both Host and Target
Controller Driver. Once the Bus Reset (mostly an interrupt) is seen, the
common ISR notifies the OTG Manager of this event through rasing OTG event
USBOTG_EVENT_ID_BUS_RESET_DETECTED. The OTG Manager responds to "Bus Reset 
event" by calling usbOtgStackSwitch() to switch stack from Host Stack to 
Target Stack (OTG state should still be a_peripheral state);

Two important points should be noted here:

a) Note that once entering the Target Stack (through the Target Controller
Driver <USBTGT_TCD::pTcdEnable()> interface), the Target Controller Driver
should report a "Bus Reset" (through callback) to the upper Target HAL (or
Target Application) to bring the upper layers out of reset (like in normal
TCD Bus Reset handling). In this case, the OTG Controller Driver common ISR
will not deliver the "Bus Reset" event to the Target Controller ISR.

b) Note that the stack switch process should happen as quickly as possible.
The Target Controller Driver should make sure it is prepared to receive the
Setup packets, because after the Bus Reset, the B-device (b_host) may start
to enumerate the A-device by reading its descriptors. However, this may not
be as critical as the TB_ACON_BSE0 or TA_BDIS_ACON, even if the Setup fails
once, the B-device (b_host) may involve some kind of retry reset mechanism
at this stage.

At this point in time, the A-device is in a_peripheral state and the Target
Stack is working (Host Stack disabled).

\h 6.5.4 B-device recognizes A-device connection, issues Bus Reset as b_host

B-device detects the data line J-state (D+ pull-up) before b_ase0_brst_tmr
timer times out (we set TB_ASE0_BRST as more than 155ms) and takes this as
A-device connection. B-device side OTG Controller Diver ISR will detect this
A-device connection and notify the A-device connection event to the OTG Manager
through the rasing OTG event USBOTG_EVENT_ID_CONNECT_DISCONNECT_DETECTED,
the OTG Manager will take this notification to transition the OTG State Machine
into b_host state (and also switch the stack from Target Stack to Host Stack).

At this point B-device (in b_host) Host Stack generates a bus Reset (SE0) to
the A-device (which is now in a_peripheral) within 1ms (TB_ACON_BSE0 max)
upon detecting of A-device connection; Note that TB_ACON_BSE0 max is specified
in OTG 1.3 as 1ms, but in OTG 2.0 it is specified as 150ms. However, the
specification doesn't seem to specify what will happen if the Reset doesn't
happen in time. Anyway, we should respect the worst case requirement by
asserting the Reset within 1ms.

The B-device starts to work as a normal host and A-device starts to work as
a normal peripheral. The forward part of the HNP procedure is completed here.

\h 6.6 B-device returns host role to A-device

B-device finishes using the bus, and try to return the host role to the
A-device (switch from current a_peripheral state to a_host state); This
involves the following steps:

\h 6.6.1 B-device user application gives up host role

B-device side OTG Manager suspends the bus by calling the Host Stack
usbHstSuspendDevice() function in response to the B-device side user call
to usbOtgHostRoleGiveup(); This causes the Host Controller to stop generating
any SOF on the bus and thus there is no bus activity on the bus; The D+/D-
data lines go into IDLE line state (3ms later it will be bus suspend); And
the B-device goes directly into b_peripheral state (Note that there is no
such a thing as "b_suspend" state);

The B-device OTG Manager calls usbOtgStackSwitch() to switch from Host into
Target stack (OTG State Machine will also be changed from b_host into b_peripheral).

At this point in time, the B-device side Target Stack is working.

Optionally, the B-device may turn on its D+ pull-up when a FS idle condition
is detected on the bus (Done by the B-device side Target Controller Driver).

\h 6.6.2 A-device detects bus suspend, and then disconnects from B-device

A-device detects lack of bus activity for more than TA_BIDL_ADIS min (3ms)
and turns off its D+ pull-up. Alternatively, if the A-device has no
further need to communicate with the B-device, the A-device may turn off
VBUS and end the session (the user application calls usbOtgVbusPowerOff()).

Once the A-device detects the bus enters IDLE state while it is in a_peripheral
state, it starts a watchdog timer a_bidl_adis_tmr. During the period of
the timer, A-device is expecting to go into a_wait_bcon state. The A-device
transitions from the a_peripheral state to the a_wait_bcon state if the
a_bidl_adis_tmr times out (a_bidl_adis_tmout = TRUE). If the A-device
detects more than TA_BIDL_ADIS min (155ms) of continuous idle (i.e. J_state),
on the bus, then the A-device may transition to the a_wait_bcon state. If no
activity is detected after TA_BIDL_ADIS max (200ms) the A-device shall
transition back to the a_wait_bcon state.

Note that this a_bidl_adis_tmr is added in USB OTG 2.0, but it is listed
in the a_peripheral state "output variable" part of the A-device state
diagram. It seems from the state diagram that this timer should be started
when the A-device enters the a_peripheral state; If the a_bidl_adis_tmr timer
times out, the A-device will enter a_wait_bcon state; But the timeout value
TA_BIDL_ADIS (B-Idle to A-Disconnect) is 155 ms (min) to 200 ms (max); So,
if this timer is started once the a_peripheral state is entered, it means
the A-device can at most stay in a_peripheral state for 200ms, and that
doesn't make sense because the B-device (b_host) may need more time to work
with the A-device (a_pheripheral). Seen from the name of this timer, it
implies it should be started when the A-device detects B-device idles
the bus (or suspend the bus) while the A-device is in a_peripheral
state (and this is our choice of time point to start it). We should note
that the "out put variables" in the OTG state diagram are not necessarily
to be output immediately when entering the state, but may be at some point
during the state.

NOTE: According to USB-IF explanation, the a_bidl_adis_tmr should be started
once a_peripheral is entered, and the timer must be reset every SOF because
if there is SOF, the bus is not idle so the timer should not timeout. This
indicates we should reset the timer at around every 1ms, and mostly we
should do it at SOF interrupt. However, SOF interrupt is not necessarily
always enabled, and that is some overhead doing so in software. In this
regard, the a_bidl_adis_tmr should ideally implemented by the OTG Controller
hardware. From software perspective, we will still only to start the timer
when we detect the bus suspend by the B-device.

A-device will recognize the bus entering bus suspend state after 3ms of
the D+/D- going into IDLE state; A-device will consider this bus suspend
state entry as an indication from the B-device to return the host role back
(A-device goes from a_peripheral into a_wait_bcon); Note that there may be
a suspend event (maybe interrupt) generated, the A-device side OTG Controller
Driver common ISR which detects this suspend event should notify the A-device
OTG Manager by rasing OTG event USBOTG_EVENT_ID_SUSPEND_RESUME_DETECTED;
The OTG Manager will then proceed to cause a role switch as detailed below.

If the two connected devices were previously working in HS, the A-device
will now turn up it D+ pull-up, which effectively makes the data lines go
into FS (can be under software control or done by hardware automatically);
If the two devices were already working in FS, the A-device D+ should have
already been pulled-up; The A-device will now disconnect from the bus by
removing the D+ pull-up. Note that this disconnection should happen in no
more than 200ms (TA_BIDL_ADIS max) after the detection of the bus suspend;

The disconnection can also be done by the hardware automatically or by
software intervention;

No matter what caused the A-device disconnection, the A-device has now
turned off its D+ pull-up (and D+/D- pull-down should also be turned on),
so the data line now goes into SE0 state; The OTG Manager will switch into
host stack by calling usbOtgStackSwitch(); At this time the A-device Host
Stack is working and Target Stack is disabled. The OTG Manager will also
move its OTG State Machine from previous a_peripheral state into a_wait_bcon
state;

Note that even the A-device doesn't disconnect, it will still be put into
a_wait_bcon state once the a_bidl_adis_tmr  times out. If the Mini-A plug
is removed (id float) or the A-device application doesn't want to use the
bus (a_bus_drop set to TRUE), it will enter a_wait_vfall state.

The A-device starts to work as a normal host and B-device starts to work
as a normal peripheral. The revert part of the HNP procedure is completed
here.

Note that the Host Controller Driver or the Target Controller Driver will
always report device events (e.g, connect, disconnect, suspend, resume, reset)
normally, and that at the proper points the Host Stack or Target Stack will
communicate with the OTG Manager so that the OTG State Machine can be properly
advanced.

The OTG Controller Driver doesn't need to monitor or report HNP status. These
OTG Notifier functions should be enough to make the OTG Manager aware of the
HNP success or failure.

\h 7 USB OTG PROTOCOL COMPLIANCE IMPLEMENTATION NOTES

This section describes the VxWorks USB OTG Stack implementation details with
regard to OTG protocol checklist as outlined in the <'OTG Compliance Checklist'>.

\h 7.1 OTG descriptor

For an OTG B-device or SRP capable peripheral, an OTG descriptor will be
included in the response to all valid GetDescriptor(Configuration) requests.
Note that this is the requirment of OTG 1.3. In OTG 2.0, B-device that
supports either HNP or SRP or ADP shall respond to a GetDescriptor(OTG)
request with its OTG descriptor.

NOTE: For the current implementation we may only include the OTG descriptor
along with the Configuration descriptor. If a GetDescriptor(OTG) is received,
we should STALL it (unless when we fully support OTG 2.0).

\h 7.2 SRP initial conditions

For an OTG B-device or SRP capable peripheral, the SRP can only be
initiated when the voltage on VBUS is thought to be below VB_SESS_END
and the bus has been in the SE0 state for at least 2ms (TB_SE0_SRP ).

NOTE: The OTG Controller Driver should be able to provide VBUS state and
the line state (such as SE0). OTG Controller Driver may need to access
some PHY register to get these information (e.g, the ULPI DEBUG register
provides the line state information).

NOTE: We may need to create some generic PHY driver, but these are to be
encapsulated by the OTG Controller Driver to provide functionalities as
reporting VBUS state and line state.

\h 7.3 Indication of SRP in progress

For an OTG B-device or SRP capable peripheral, before SRP is initiated,
an indication should be made to the user that communications are trying
to be established.

NOTE: The user application may call usbOtgVbusStateGet() to get the current
session state and inform the user of the progress information. The user can
also be notified by OTG Manager notification mechanism.

NOTE:

We have some choices to implement user notification:

a) The user may install a callback through USBOTG_IOCTL_CALLBACK_INSTALL;
the user callback will be called once b_srp_init is entered, with a message ID
of USBOTG_USR_MSG_ID_SRP_ACTIVE.

b) The user application may create a task, and a msgQ to get event
notifications from the OTG Manager.

c) Use select interface of the iosLib, add the message to the ring buffer,
and notify user of message data availability, and let the user read the message.

d) Use signal interface

Currently we just implemented the user callback mechanism; we may choose to
add the select interface.

\h 7.4 Indication of SRP failure

For an OTG B-device or SRP capable peripheral, an indication should be
made to the user that SRP has failed if a session has not been started
within TB_SRP_FAIL of initiating SRP.

NOTE: When the B-device user application calls usbOtgVbusPowerOn() to request
SRP, it returns immediately after the b_bus_req request flag is set, it is up
to the OTG Manager to wait for the SRP inital condition to be met and actually
starts SRP. Once the SRP is initiated, the USBOTG_TIMER_b_srp_fail_tmr will be
started, and if the timer times out but VBUS still not valid, the SRP is marked
as failure.

NOTE: The user may install a callback through USBOTG_IOCTL_CALLBACK_INSTALL;
the user callback will be called once USBOTG_TIMER_b_srp_fail_tmr times out but
VBUS still not valid, with a message ID of USBOTG_USR_MSG_ID_SRP_FAIL.

\h 7.5 SRP init time requirement

For an OTG B-device or SRP capable peripheral, the SRP procedure should
take less than 100ms (TB_SRP_INIT).

NOTE: This is the time required for B-device to complete its SRP activities,
this may involve writing some register bits to initiate the hardware to
trigger Data-line pulse (and VBUS pulse if implemented). Most of this is
accomplished by hardware and thus is not a big software concern. If there has
to be software controlled data line and/or VBUS pulsing, two timers will be
used to trigger the pulses with timeout value set to meet the pulse widths
required, but the total time doesn't exceed the TB_SRP_INIT. See the section
<'USB OTG SRP IMPLEMENTATION NOTES'> for details.

\h 7.6 Indication of failure to raise VBUS

For an OTG A-device, an indication should be made to the user that the
B-device is not supported when VBUS takes longer than TA_VBUS_RISE to
reach VA_VBUS_VLD.

NOTE: When this situation happens, the OTG Manager will instruct the
Controller Driver to power off the VBUS and show a warning message that
the attached B-device can not be supported. The user application callback
can be called with message ID of USBOTG_USR_MSG_ID_VRISE_FAIL.

\h 7.7 Choice on debounce interval

For an OTG A-device, there are two possible debounce interval to be
used (short TA_BCON_SDB [Min 2.5us], or long TA_BCON_LDB [Min 100ms]).
The details are as below:

If b_conn is FALSE, it is set to TRUE if the B-device pulls D+ or D-
high for longer than the debounce interval. The debounce interval varies
depending on how the a_wait_bcon state was entered, and how long it has
been since either D+ or D- has been pulled high.

1) If the a_wait_bcon state was entered from the a_peripheral, a_host
or a_suspend states, then the short debounce interval (TA_BCON_SDB) is
allowed.

2) If the a_wait_bcon state was entered from the a_wait_vrise state then
the long debounce interval (TA_BCON_LDB) is required.

The A-device is only allowed to apply the short debounce to b_conn in a
window of time. If the A-device stays in the a_wait_bcon state for longer
than TA_BCON_SDB_WIN max (100ms), then the long debounce interval applies
no matter how the a_wait_bcon state was entered. Note that if the state
machine is timing the short debounce interval, changes to D- and D+ do
not affect the setting of b_conn if they occur before TLDIS_DSCHG min.

NOTE: Since TA_BCON_SDB is only "allowed", not "required", we will always
use TA_BCON_LDB for the A-device.

NOTE: However, to meet the 1ms limitation of TB_ACON_BSE0, during HNP,
when B-device detects A-device connection, we will issue the Bus Reset
immediately once the connection is detected (the BusM debounce will be
dummy process in this case). This doesn't affect the debounce internal
of the A-device, because this is applied for the B-device.

\h 7.8 SRP capable host debounce using TA_BCON_LDB

For an SRP capable host, when waiting for a B-device connection, the connection
can be debounced using TA_BCON_LDB.

NOTE: As discussed above, we will not use TA_BCON_SDB, and TA_BCON_LDB is
used always.

\h 7.9 TA_BCON_SDB debounce interval starting point

For an OTG A-device, if waiting for a B-device connection and using a
debounce of TA_BCON_SDB, the debounce interval should be started after
TLDIS_DSCHG (Local Disconnect to Data Line Discharge Time).

NOTE: This doesn't apply to our situation since we don't use TA_BCON_SDB.

\h 7.10 B-device HNP Suspend to Disconnect time (TB_AIDL_BDIS)

For an OTG B-device, when operating as a peripheral, the HNP should only be
started (with a disconnect) if the bus has been idle for more than 5ms
(TB_AIDL_BDIS min).

NOTE: Both OTG 1.3 and 2.0 specify TB_AIDL_BDIS as (Min 4ms, Max 150ms).
However the compliance checklist specifies as 5ms which gives some 1ms
slit. Also, most OTG Controller will handle this in hardware and thus not
a big software concern (meaning once HNP is enabled on B-device, when
detecting A-device suspending the bus, the B-device hardware will start
the disconnection automatically within TB_AIDL_BDIS).

\h 7.11 HS B-device switching to FS before HNP starting

For a HS capable OTG B-device, operating as a HS peripheral, when
no bus activity is detected for 5 ms (TB_AIDL_BDIS), FS mode should be
entered, with the D+ pull-up resistor turned on, and the D+ line checked
to be high for at least 1ms (TB_FS_BDIS min) before starting HNP.

NOTE: This should be a OTG Controller hardware concern for most USB OTG
Controllers. For fully software controlled HNP, we have to conform to the
timing requirements.

\h 7.12 HS B-device responding to A-device bus reset during HNP

For a HS capable OTG B-device, after operating as a HS peripheral
and entering FS mode so that HNP can be started, if the D+ line is not
high within TWTRSTHS of turning on the D+ pull-up, then a HS chirp should
be started.

TWTRSTHS: Time a device must wait after reverting to full-speed before
sampling the bus state for SE0 and beginning the high-speed detection
handshake.

NOTE: Starting HS B-device HS chirp means response to the A-device Reset.
This maybe due to the fact that after the A-device suspends the bus, and
at the point the B-device wants to start HNP, the A-device wants to exit
the suspend and Reset the bus (may be due to user intervention).

NOTE: This should be a OTG Controller hardware concern.

\h 7.13 B-device starts HNP but A-device doesn't respond the HNP

For an OTG B-device, if after initiating HNP and 3.125ms (TB_ASE0_BRST
min) of SE0 is seen, the B-device should return to being a peripheral and
start to process the reset before (TB_ASE0_BRST max).

After the B-device turns off its pull-up resistor the bus will be pulled
to the SE0 state by the pull-downs on the A and B devices. This SE0 is
not a resume indication. If the SE0 persists for more than TB_ASE0_BRST
min, then the A-device is not responding to the HNP request from the
B-device and the B-device shall treat the SE0 on the bus as a bus reset
indication. This takes the B-device back to the b_peripheral state.

NOTE: This adds a design challenge for the HNP timing. The following
facts contibute to the design choice for when to do the controller mode
and stack switching:

1) There is a strict timing requirement for B-device TB_ACON_BSE0 (Max 1ms)
(A-Connect to B-SE0 Time). Meanwhile, there is also a requirement for the
A-device to meet TA_BDIS_ACON (Max 3ms) (B-Disconnect to A-Connect Time).
This means once the B-device disconnects, the A-device may soon (Max 3ms)
signal connection to the B-device, and the B-device should immediately
respond to the A-device connection very quickly in Max of 1ms (to assert
the Bus Rest to the A-device). With the strict B-device timing constraints,
it might be desirable that the OTG Manager switch into host stack as soon
as possible by calling usbOtgStackSwitch() just after B-device disconnects
from the A-device;

2) However, considering the case that the A-device may not actually respond
to HNP (not to signal connection to the B-device, but to signal Bus Reset
to the B-device while the B-device is expecting connection), we may need
to wait around 3.125ms (TB_ASE0_BRST min) to ensure there is no SE0 from
the A-device and then actually starts the stack switching (so that we are
still in the Target Stack and can response to the Reset in this condition).

3) The above two scenarios indicates that we need to monitor both for the
A-device connection case and the A-device reset (or resume) cases during
the 3ms time (or around 3.125ms). To meet both goals, the design decision
is as below:

3.1) When the B-device sees the bus suspend and disconnects (starting HNP),
we do not switch stack immediately (but the controller mode can be changed);
instead, at this point, the B-device OTG Manager starts a watchdog timer
b_ase0_brst_tmr (and move the B-device OTG state into b_wait_acon). The
b_ase0_brst_tmr timer is used by B-device in the b_wait_acon state, to
wait for the A-device to signal a connection (a_conn = TRUE). During this
timer period TB_ASE0_BRST (3.125ms minimum), the B-device is expecting
A-device to connect. Note that the TB_ASE0_BRST is specified as 3.125ms min
in OTG 1.3, but is specified as 155ms min in OTG 2.0. Anyway, to cover the
worst case, we will set the b_ase0_brst_tmr timeout period to be longer
than 155ms, even WAIT_FOREVER.

3.2) If the A-device does not connect to the B-device before TB_ASE0_BRST
(b_ase0_brst_tmout = TRUE), then the B-device shall return to the
b_peripheral state.

3.3) If the A-device signals Bus Reset (SE0), or signals resume (J-to-K)
(if the B-device is in the b_wait_acon state, then only a J-to-K transition
is treated as a resume), then the B-device will return to b_peripheral state.

3.4) If during the b_ase0_brst_tmr period, the A-device connects to B-device
(the A-device actually reponds to the HNP), then the B-device transition
to b_host state. To guarantee the TB_ACON_BSE0 (Max 1ms) limit, once the
B-device recognizes the A-device connection (while still in b_wait_acon
state and the stack is still Target Stack), the OTG Controller Driver ISR
should assert the Bus Reset to the A-device immediately (can be in the ISR
directly). Once the Bus Reset is done, the A-device connection event
is notified to the OTG Manager through usbOtgNotifyConnectDisconnect(),
and then the OTG Manager can call usbOtgStackSwitch() to switch the stack
from Target Stack to Host Stack and the OTG state is also moved from
b_wait_acon to b_host state.

3.5) At this time the B-device Host Stack is working and Target Stack is
disabled. Once the Host Stack is entered, the Host Controller Driver would
construct a dummy Connection Change and report to the BusM through Hub
Class Driver Interrupt URB (before this the Host Controller Driver could
issue a Bus Reset once again, a Bus Reset at this point doesn't cause any
harm). Since the Bus Reset has been issued to the A-device, the b_host Host
Stack could proceed with normal device enumeration.

\h 7.14 B-device starts HNP and responds to the HNP by connecting to B-device

For an OTG B-device, if after initiating HNP and the D+ line is high
after 25us (TLDIS_DSCHG min) for at least 2.5us (TB_ACON_DBNC min) after
the B-device turns off its D+ pull-up, the B-device should become the host.

If the B-device is in the b_wait_acon state, the B-device sets a_conn TRUE
if the B-device detects a connection from the A-device. In order to detect
a connection from the A-device, the B-device must insure that there is no
residual voltage on the D+ line from the B-device's pull-up (TLDIS_DSCHG).
When the B-device has qualified the high level on the D+ line as being from
the A-device, and the level has been present for at least TB_ACON_DBNC min
then a_conn is set to TRUE.

NOTE:

(See Also Note 13). While in the b_wait_acon state, when detected A-device
connectin (D+ pull-up) (still in Target Stack), we will issue Bus Reset
immediately to meet TB_ACON_BSE0 (Max 1ms) limit and then notify the OTG
Manager to call usbOtgStackSwitch() to switch the stack from Target Stack
to Host Stack and the OTG state is also moved from b_wait_acon to b_host
state.

NOTE: An OTG device is required to operate as a FS or HS peripheral.
Therefore, a B-device shall only accept a connection from an A-device
when D+ is pulled up. If the B-device detects a high on the D- line after
disconnecting, this shall be interpreted as resume signaling from the
A-device.

\h 7.15 B-device in b_host driving Bus Reset

For an OTG B-device, after successfully becoming host, a bus reset
asserted within 1ms (TB_ACON_BSE0 max) of detecting the A-device connect.

NOTE:

(See Also Note 13). Once the B-device recognizes the A-device connection
(while still in b_wait_acon state and the stack is still Target Stack),
the OTG Controller Driver ISR should assert the Bus Reset to the A-device
immediately (maybe in the ISR directly).

\h 7.16 B-device accepting OTG SetFeature commands from A-device

For an OTG B-device, SetFeature(b_hnp_enable), SetFeature(a_hnp_support)
and SetFeature(a_alt_hnp_support) should all be accepted in the Default,
Address and Configured states.

It should be noted that the USB 2.0 specification [USB2.0] states that
setting a feature in the Default state for other than test modes is
unspecified. But OTG specification adds to the list of features that can
be set in the Default state.

NOTE: The OTG event USBOTG_EVENT_ID_SET_OTG_FEATURE_RECEIVED is reported
by the Target Stack to notify the OTG Manager to set the features.

NOTE: Allowing setting these features in these states inidicate that HNP
could happen in these states. However VxWorks OTG Stack will only issue
these OTG SetFeature commands after setting the device address.

\h 7.17 OTG feature clear time points

For an OTG device, the features b_hnp_enable, a_hnp_support and
a_alt_hnp_support are cleared on a bus reset and at the end of a session
but not with a ClearFeature() command.

NOTE: The OTG Manager will clear these features when these events happen:

- USBOTG_EVENT_ID_BUS_RESET_DETECTED

- USBOTG_EVENT_ID_VBUS_STATE_CHANGED

- in usbOtgIdVbusProbe() when it finds the VBUS is below SessEnd

\h 7.18 A-device SetFeature(b_hnp_enable) only to directly connected B-device

For an OTG A-device, a SetFeature(b_hnp_enable) command should only be
sent to a directly connected B-device (no intervening hubs).

NOTE: The Host Stack USBD will determine the tier of the connected device
and make sure the device connected on the root hub port is not a hub
before reporting to the OTG Manager of a B-device connection. Only when
a normal device is connected on the root port and that it is determined
to be OTG HNP capable device through the OTG descriptor, we can issue the
SetFeature(b_hnp_enable) command (and also set the a_set_b_hnp_en flag)
with the instructuon from the OTG Manager.

\h 7.19 A-device SetFeature(a_hnp_support) time point

For an OTG A-device, a SetFeature(a_hnp_support) command should only
be sent between the start of a session and the selecting of an OTG B-device
configuration.

NOTE: Before putting the legacy B-device into a configuration, the A-device
has the following three options with regards to the b_hnp_enable and
a_hnp_support features:

- set the b_hnp_enable feature
- set the a_hnp_support feature but not the b_hnp_enable feature
- set neither the b_hnp_enable nor a_hnp_support features

If the b_hnp_enable feature is set, the B-device is allowed to do HNP,
regardless of whether or not the a_hnp_support feature is set.

If the a_hnp_support feature is set, but the b_hnp_enable feature is not
set, then it is likely that b_hnp_enable will be set later when the
A-device is finished using the bus.

If neither the b_hnp_enable or a_hnp_support features are set before
the A-device puts the B-device in a non-default configuration, then
the B-device may indicate to the user that HNP is not supported through
the current connection.

NOTE: A notification message will be sent to the user application callback,
informing USBOTG_USR_MSG_ID_HOST_NO_HNP.

\h 7.20 A-device connecting to B-device, responding HNP from B-device

For an OTG A-device, if it was successful in setting b_hnp_enable
during the current session and a disconnect was detected during suspend,
it's D+ pull-up resistor should be turned on within 3ms (TA_BDIS_ACON) of
detecting the disconnect.

NOTE: This is another critical timing requirement enforced by the USB OTG
specification.

The A-device is still in a_suspend state at this point. Once the B-device
disconnection is detected, the OTG Controller Driver should turn on the
D+ pull-up immediately (maybe in the ISR directly) and switch the hardware
into peripheral mode (if such a hardware mode switching is required). In
fact, to meet the 3ms TA_BDIS_ACON max requirement, some OTG Controllers
may support some kind of hardware automatic assist, once enabled, the
hardware will detect the "B-device disconnection during suspend" situation
and switch the hardware into peripheral mode and assert the D+ pull-up
automatically. The OTG 1.3 specification (6.7.2 A-device becoming Peripheral)
describes a case for this kind of automatic HNP response.

No matter if the hardware assist exists or not, once B-device disconnection
event is detected by the A-device Host Controller Driver, it should report
it to the BusM through the Hub Class Diver Interrupt URB. The A-device
Host Stack will handle the B-device disconnection normally (for example,
free its class driver allocated resources and do any other device unloading
stuff).

\h 7.21 A-device as peripheral responding to B-device bus suspend (return host role)

For an OTG A-device, after detecting 3ms (TA_BIDL_ADIS min) of continuous idle
whilst acting as a peripheral, the A-device should disconnect within 197ms
(TA_BIDL_ADIS max - TA_BIDL_ADIS min).

The A-device transitions from the a_peripheral state to the a_wait_bcon
state if the a_bidl_adis_tmr times out (a_bidl_adis_tmout = TRUE).
If the A-device detects more than TA_BIDL_ADIS min of continuous idle
(i.e. J_state), on the bus, then the A-device may transition to the
a_wait_bcon state. If no activity is detected after TA_BIDL_ADIS max the
A-device shall transition back to the a_wait_bcon state.

NOTE: The A-device hardware should detect the bus IDLE for more than 3ms
as a bus suspend, and OTG Controller Driver ISR (A-device in a_peripheral)
should report to the OTG Manager as USBOTG_EVENT_ID_SUSPEND_RESUME_DETECTED.

NOTE: Once detecting the bus suspend while in a_peripheral, the OTG Manager
will start a watchdog timer a_bidl_adis_tmr (added in OTG 2.0). The timer
triggers at TA_BIDL_ADIS (Min 150ms, Max 200ms).

\h 7.22 HS capable OTG A-device responding to B-device Bus Reset

For a HS capable OTG A-device, if a bus reset is detected whilst acting
as a peripheral, the high-speed detection handshake should be started.

NOTE: This should be handled by the OTG Controller hardware (there maybe some
hardware configurable register bits that allows enable or disable of the HS
support). Anyway this is not a big software concern.

\h 7.23 A-device starts session upon detecting Mini-A plug

For an OTG A-device, the device can initiate a session upon the insertion
of a Mini-A plug.

NOTE: Normally for OTG A-device, we can keep the VBUS powered off (session
not in progress) even there is a B-device insertion, and require B-device
to use SRP to request us to power on the VBUS. However, it is very likely
that a non-OTG capable device will be connected in the other end of the USB
cable, and that device may need the VBUS so that it could signal the device
connection to the A-device. For this purpose, we may need to power on the
VBUS at the insertion of the Mini-A plug (there will be ID pin state change).
We will go through the a_idle -> a_wait_vrise -> a_wait_bcon process, and
once the a_wait_bcon state is entered, the a_wait_bcon_tmr is started, on
timeout of this timer, we can finally power off the VBUS if there is no
device connection signaled. This way, it also saves the SRP time if the
other end of the USB cable is actually a OTG SRP capable device.

\h 7.24 A-device supporting SRP

For an OTG A-device, although we can initiate a session upon the insertion of
a Mini-A plug, the device can support SRP.

NOTE: The SRP and initiation of a session upon the insertion of a Mini-A plug
doesn't conflict with each other.

\h 7.25 A-device supporting Remote Wakeup

For an OTG A-device, the device should support remote wakeup operation
when acting as a host.

NOTE: If USB system wishes to place the bus in the Suspended state, it
commands the Host Controller to stop all bus traffic, including SOFs.
This causes all USB devices to enter the Suspended state. In this state,
the USB system may enable the Host Controller to respond to bus wakeup
events. This allows the Host Controller to respond to bus wakeup signaling
to restart the host system.

NOTE: The USB stack handling of Suspend/Resume is enhanced to support OTG HNP,
including Remote Wakeup.

\h 7.26 A-device SRP response time

For an OTG A device, a session should be always started within 30 seconds of
a valid SRP.

NOTE: After initiating SRP, the B-device is required to wait at least
TB_SRP_FAIL min (5 sec) for the A-device to respond. After this the B-device
may inform the user that the communication attempt has failed. For this
reason, it is recommended that the A-device respond to SRP in less than
TA_SRP_RSPNS max (4.9 sec). The minimum response from the A-device is to
turn on VBUS and generate a bus reset.

NOTE: We may need to power on the VBUS as soon as possible, within 5 seconds.

\h 7.27 A-device VBUS valid time

When an OTG A-device starts a session, the VBUS should always valid for at
least TA_WAIT_BCON (1 second).

NOTE: Once a_wait_bcon is entered, a watchdog timer a_wait_bcon_tmr is
started. This timer is used by an A-device in the a_wait_bcon state to wait
for the B-device to signal a connection, (b_conn = TRUE). If the B-device
does not connect before TA_WAIT_BCON, (a_wait_bcon_tmout = TRUE), then the
A-device is allowed to stop waiting for a connection.

NOTE: We may set the TA_WAIT_BCON to be WAIT_FOREVER (or user configurable),
but should respect the TA_WAIT_BCON to be Min 1 second.

\h 7.28 A-device Bus Reset time point

For an OTG A-device, a reset should be always issued within 30 seconds
of a B device connect.

NOTE: (See Also Note 20). With the implementation of Note 20, this is not a
software concern anymore (when a connection is detected, we issue Bus Reset
immediately).

\h 7.29 A-device set feature hnp_enable time point for unsupported B-device

For an OTG A-device, a set feature hnp_enable command should be issued
within 30 seconds of the connection of an unsupported B-device.

NOTE: We should check the OTG descriptor to see if the B-device supports HNP
and then if the B-device can not be supported by the A-device, then we
should set feature hnp_enable command immediately, 30 seconds is enough to
allow this determination.

\h 7.30 A-device bus suspend time point for unsupported B-device

For an OTG A-device, the bus should be suspended within 30 seconds of
bus reset with an unsupported B device acknowledges the set feature
hnp_enalbe command.

NOTE: Once we set feature hnp_enable command to the B-device, we should start
to suspend the bus immediately (since we can not work with the device), 30
seconds is enough to allow this determination.

NOTE: We will indicate to the user application the B-device cannot be supported
by the A-device by USBOTG_USR_MSG_ID_DEV_UNSUPPORTED. The user application should
decide if it will load a new driver or use ioctl USBOTG_IOCTL_HOST_GIVEUP to call
usbOtgHostRoleGiveup() and start the HNP.

\h 7.31 A-device aborting HNP

The OTG A-device could abort HNP after suspending when interacting with an
unsupported device.

NOTE: This may be determined by the user application (for example, the user
application on the A-device side could load a class driver that may support
the device, thus it could request to abort the HNP).

\h 7.32 A-device should enumerate successfully as a peripheral following HNP

The OTG A-device should always enumerate successfully as a peripheral
following an HNP transition.

NOTE: (See Note 20). We should be able to be enumerated successfully.

\h 7.33 A-device respond to remote wakeup signaling without enabling remote wakeup

The OTG A-device should respond to remote wakeup signaling by driving
resume for at least 20 milliseconds even if it has not sent an enable
remote wakeup command to the downstream device.

In a_suspend state, the A-device transitions to the a_host state if
either a_bus_req is asserted, or if the B-device signals a resume by
putting a K state on the bus, even if the remote_wakeup feature has
not been enabled.

NOTE: (See Also Note 25). We should not depend on the downstream device remote
wakeup enabled or not.

\h 7.34 B-device HNP response time point

For an OTG B-device, when operating as a peripheral, the HNP should be
started (with a disconnection) within 150 ms of the bus being idle (TB_AIDL_BDIS
max).

NOTE: In the HNP process, B-device detects that bus is idle for more than
TB_AIDL_BDIS min and begins HNP by turning off pull-up on D+ (If the bus
was operating in HS mode, the B-device will first enter the full-speed
mode). This allows the bus to discharge to the SE0 state.

Most OTG Controllers peform this in hardware. When the user application
calls usbOtgHostRoleRequest() to request host role for B-device, the OTG
Manager will call <'USBOTG_OCD_FUNCS::pOtgAutoSusp2DisEnable()'> of the OTG
Controller Driver to make the OTG Controller to be prepared for HNP procedure
to start; When the Host suspends the bus, the hardware disconnects automatically.
So this time limitation would mostly be assured by hardware.

\h 7.35 B-device bus suspend time point for unsupported A-device

The OTG B-device end the session within 30 seconds of becoming host for the
unsupported device following an HNP transition.

NOTE: (See Also Note 30) When B-device finds out it can not support the A-device
after HNP, it should suspend the bus as soon as possible (30 seconds should be
enough). Following this, the A-device could transition to power off the VBUS when
it finds both ends can not use the peer device each other.

\h 8 OTG TIMING CONSTRAINTS AND DESIGN CHALLENGES

This section describes the various OTG specific timing constraints and the
challenges they put on for designing a generic USB OTG Stack, it also describes
how VxWorks USB OTG Stack solves the challenges.

The USB OTG specification has some timing constraints that must be met by
compliant driver software. On some events triggered by the OTG hardware,
the software must response to them within limited time defined by the OTG
specification, otherwise the system may not perform correctly.

\h 8.1 THOST_REQ_POLL (Min 1sec, Max 2sec) - HNP polling period for host request flag

In OTG 2.0 with HNP polling, whenever there is an active session between
two OTG devices the host is required to execute a GetStatus(OTG) with a
frequency of THOST_REQ_POLL in order to determine the state of the Host
request flag as defined in the OTG status information. After the host has
detected that the Host request flag is set to one, the host shall allow
the peripheral to take the host role within THOST_REQ_SUSP. If the Host
request flag is reset to zero when an A-device has finished its activities
it can end the session immediately.

NOTE: We use the OTG Manager task, with a parameter change notification semaphore,
which normally waits for the input parameters to be changed by the event notification
or from user application requests, but the timeout value is set to be around 1.5
sec, which meets the THOST_REQ_POLL time interval. Once the semaphore times out,
it checkes if there is active OTG session between the two connected devices, and
if the two devices are capable of doing HNP with each other, then it initiates
the HNP polling by usbOtgHnpPolling().

\h 8.2 THOST_REQ_SUSP (Max 2sec) - Time from detection of B-device host request until suspend

In OTG 2.0 with HNP polling, the A-device is required to set b_hnp_enable
feature and suspend the bus within THOST_REQ_SUSP when it determines that
the B-device wishes to become host (host_req_flag = TRUE).

NOTE: Once we detect the host request flag, we will notify the user application
with a message ID of USBOTG_USR_MSG_ID_DEV_REQ_HOST. The user application decides
if it want to give up the host role, and if so, it uses USBOTG_IOCTL_HOST_GIVEUP
ioctl to call usbOtgHostRoleGiveup() to give up its host role. The callback
gives the user application a chance to do some clean up work, but it is the user
application's responsibility to keep the Max 2sec time requirement to be met.

NOTE: From the B-device side, if it uses the host request flag to request host
role (by calling usbOtgHostRoleRequest() with <'ASAP'> set to TRUE), then that
routine will report a notification message ID of USBOTG_USR_MSG_ID_REQ_HOST_RISKY
to inform the user that he should be take his own risk in doing so.

We have warned you that this is risky, take your own risk in doing so!

\h 8.3 TB_DATA_PLS (Min 5ms, Max 10ms) - Data-Line Pulse Time

To indicate a request for a new session using the data-line pulsing SRP,
the B-device waits until the initial conditions are met and then turns
on its D+ pull-up resistor for a period within the range specified by
TB_DATA_PLS.

Note that the above TB_DATA_PLS requirement has put limitations for both
A-device and B-device:

\h 8.3.1 For B-device initating SRP

- If the OTG Controller can perform SRP by hardware automatically under
software control (for example, by setting a register bit), then the timing
is totally controlled by hardware and software can do nothing.

- If the SRP must be controlled by software, for example, to set a register
bit to pull-up D+ for TB_DATA_PLS of time, and then clear the register bit
to disable the pull-up on D+, then there must be a timer that guarantee
the pull-up on D+ is disabled in between 5ms and 10ms. Note that a software
delay using taskDelay() may not work because the task maybe preempted by
other higher priority tasks and cause the TB_DATA_PLS to be expired.

NOTE: In the software controlled SRP case, we use a timer to control the
pulse width. If the OTG Controller has hardware timer support, it should
implement <'USBOTG_OCD_FUNCS::pOtgTimerStart()/pOtgTimerStop()'> to make
the timing more accurate. If not, we fall back to use OS provided watch
dog timer, in this case, we recommend to set the sysClkRate to at least
1000 so that the watchdog is able to provide 1ms accuracy.

\h 8.3.2 For A-device detecting SRP

Most OTG Controllers detect SRP by hardware. An A-device shall detect SRP
if D+ goes high (while VBUS is off). Since the D+ data-line pulse is
characterized by both its rising and falling edges; both edges shall be
seen before SRP is reported by the A-device. For this reason, the detection
of offending device (D+ pull-up longer than 10ms while VBUS is off) could
only be done by hardware. The hardware may not report SRP detection if such
a device is detected (but it doesn't harm if it reports SRP in this case,
because in response to the SRP is to provide VBUS, and providing VBUS for
such self-powered USB devices at connection should be OK.)

\h 8.4 TB_SRP_FAIL, TA_SRP_RSPNS and TB_SRP_INIT

- TB_SRP_FAIL (Min 5sec, Max 6sec) - SRP Fail Time of B-device

- TA_SRP_RSPNS (Max 4.9sec) - SRP Response Time of A-device

- TB_SRP_INIT (Max 100ms) - SRP Initiate Time (removed from OTG 2.0)

After initiating SRP, the B-device is required to wait at least TB_SRP_FAIL
min for the A-device to respond. After this the B-device may inform the user
that the communication attempt has failed. For this reason, it is recommended
that the A-device respond to SRP in less than TA_SRP_RSPNS max. The minimum
response from the A-device is to turn on VBUS and generate a bus reset.

The B-device is required to complete its SRP activities in less than
TB_SRP_INIT max. On entry to this state, the B-device shall provide an
indication to the user that it is trying to establish communications with
the A-device and a timer is started. This timer will continue to run until
the B-device enters the b_peripheral state. If the timer reaches a vendor
specific limit between TB_SRP_FAIL min and TB_SRP_FAIL max the B-device
will indicate to the user that the A-device did not respond.

See <'USB OTG SRP IMPLEMENTATION NOTES'> for more details.

NOTE: We will report USBOTG_USR_MSG_ID_SRP_ACTIVE and USBOTG_USR_MSG_ID_SRP_FAIL
at these specific points.

\h 8.5 TB_AIDL_BDIS (Min 4ms, Max 150ms) - A-Idle to B-Disconnect Time

In the HNP process, B-device detects that bus is idle for more than
TB_AIDL_BDIS min and begins HNP by turning off pull-up on D+ (If the bus
was operating in HS mode, the B-device will first enter the full-speed
mode). This allows the bus to discharge to the SE0 state.

Most OTG Controllers peform this in hardware. When the user application
calls usbOtgHostRoleRequest() to request host role for B-device, the OTG
Manager will call OtgAutoSus2DisEnable() of the OTG Controller Driver
to make the OTG Controller to be prepared for HNP procedure to start;
When the Host suspends the bus, the hardware disconnects automatically.
So this time limitation would mostly assured by hardware.

See <'USB OTG HNP IMPLEMENTATION NOTES'> for more details.

\h 8.6 TB_ACON_BSE0 and TA_BIDL_ADIS

- TB_ACON_BSE0 (Max 1ms in OTG 1.3, Max 150ms in OTG 2.0) - A-Connect to B-SE0

- TA_BIDL_ADIS (Min 3ms in OTG 1.3, Min 155ms in OTG 2.0, Max 200ms)

While during the b_wait_acon state, if the A-device signals a connection
(a_conn = TRUE) before the b_ase0_brst_tmr expires, then the B-device
transitions to the b_host state. The B-device shall be able to detect
the connect from the A-device and transition to the b_host state within
TB_ACON_BSE0 max after D+ is detected to be high at the B-device.

After the A-device signals a connect, it shall continue to signal a
connect for at least TA_BIDL_ADIS min, while waiting for the B-device
to issue a bus reset. This ensures that the B-device has at least
TB_ACON_BSE0 max to detect and respond to the A-device connect.

This is the most critical timing requirement by the OTG specification
(worst case of 1ms Max for OTG 1.3 compliant)! At a first look at this,
for the B-device side, it is desirable that we can switch the Target
Stack into Host Stack as soon as possible, just at the point B-device
disconnects from A-device (when B-device detects A-device suspending
the bus), so that when the A-device connects to the B-device, the Host
Stack is working and we can proceed as a normal host detecting a normal
device connection and continue the enumeration as normal. However, there
are other facts that may make this choice hard to be feasible enough.

The design decision is that we only switch from Target to Host Stack when we
actually gets a connection from A-device.

See <'USB OTG HNP IMPLEMENTATION NOTES'> and
<'USB OTG PROTOCOL COMPLIANCE IMPLEMENTATION NOTES'> for more details.

\h 8.7 TA_WAIT_VRISE (OTG 1.3)/TA_VBUS_RISE (OTG 2.0) (Max 100ms) - Wait for VBUS Rise Time

The a_wait_vrise_tmr timer is used by an A-device in the a_wait_vrise
state to wait for the voltage on VBUS to rise above the A-device VBUS
Valid threshold (a_vbus_vld = TRUE). If VBUS is not above this threshold
before and after TA_WAIT_VRISE (a_wait_vrise_tmout = TRUE), then this
is an indication that the B-device is drawing too much current.

If VBUS does not reach VA_VBUS_AVG_LO min (4.4V) within TA_VBUS_RISE,
then the A-device shall turn off VBUS.

NOTE: If we fail to raise the VBUS in time, we will notify the user application
with a message ID of USBOTG_USR_MSG_ID_VRISE_FAIL.

\h 8.8 TA_BDIS_ACON (Max 3ms in OTG 1.3, Max 150ms in OTG 2.0) - B-Disconnect to A-Connect

During HNP process, when the A-device suspends the bus and when A-device
detects the SE0 on the bus and recognizes this as a request from the
B-device to become host. The A-device responds by turning on its D+
pull-up within TA_BDIS_ACON max of first detecting the SE0 on the bus.
If the A-device does not detect a disconnect before TA_AIDL_BDIS min
after suspending the bus, then the A-device is allowed to stop waiting
for a disconnect and end the session.

It might be desirable to process the B-device disconnection while
the A-device is in suspend as a normal device disconnection, and then
reports to the OTG Manager of this disconnection event, which will
trigger a stack switch from Host Stack to Target Stack. However, some
other facts make this not feasible enough.

The design decision is that we only switch from Host to Target Stack when we
actually get a Bus Reset from B-device.

See <'USB OTG HNP IMPLEMENTATION NOTES'> and
<'USB OTG PROTOCOL COMPLIANCE IMPLEMENTATION NOTES'> for more details.

\h 8.9 TA_BIDL_ADIS (Min 3ms in OTG 1.3, Min 155ms in OTG 2.0, Max 200ms) - B-Idle to A-Disconnect Time

In the a_peripheral state, the A-device transitions from the a_peripheral
state to the a_wait_bcon state if the a_bidl_adis_tmr times out
(a_bidl_adis_tmout = TRUE). If the A-device detects more than TA_BIDL_ADIS
min of continuous idle (i.e. J_state), on the bus, then the A-device may
transition to the a_wait_bcon state. If no activity is detected after
TA_BIDL_ADIS max the A-device shall transition back to the a_wait_bcon
state.

NOTE: The OTG Manager will switch stack when the B-device bus suspend happens
after 3ms. The stack switch from Target Stack to Host Stack should be done
in Max 200ms.

\h 9 USB OTG USAGE SCENARIOS

This section describes the OTG usage scenarios.

Note that we distinguish the "attached" and "connected" concepts; being
attached is the situation in which the cable is plugged in the USB port;
while being "connected" is the situation in which the cable is not only
plugged in the USB port, but also the peripheral side applied its D+
pull-up.

\h 9.1 No USB plug attached

In this scenario, the ID pin is floating. The stack should be started as
Target Stack and in b_idle state.

\h 9.2 Mini-A plug attached, but no device at other end of the cable

In this scenario, the ID pin is grounded. The stack should be started as
Host Stack and wait for the device at other end to be attached. The ports
VBUS may be powered off to save power (can also be powered on as normal
Hosts do!)

NOTE: Once detecting a ID pin change from HIGH to LOW, we must power on VBUS
and see if there is device connection happen (use a_wait_bcon_tmr). This is
to watch out for the case A-device is inserted with a normal Non-OTG capable
B-device.

\h 9.3 Mini-A plug connected with a non-OTG device in the other end

In this scenario, the ID pin is grounded. The stack should be started as
Host Stack and does normal Host Stack operation with the device.

\h 9.4 Mini-A plug connected with a OTG Dual-Role device in the other end

In this scenario, the ID pin is grounded. The stack should be started as
Host Stack and does normal Host Stack operation with the device. This
device is the A-device and the other end of the cable is the B-device.
The Host Stack will check if the device at the other end can be supported
(in its TPL list); If the device at the other end can not be supported,
then it enables the HNP feature of the OTG B-device, and suspend the bus
to start the HNP process (try to allow the B-device to control the bus
and see if the B-device can support this A-device in a_peripheral mode).

\h 9.5 Mini-B plug attached, but no device at other end of the cable

In this scenario, the ID pin is floating. The stack should be started as
Target Stack and in b_idle state.

\h 9.6 Mini-B plug attached with a non-OTG device (peripheral only) in the other end

In this scenario, the ID pin is floating. The stack starts in Target Stack,
and tries to initiate SRP to the device at the other end (since the device
in the other end of the cable is non-OTG capable, it doesn't respond to
SRP and doesn't raise VBUS). So this device will switch from peripheral
to host mode if it can turn on VBUS (in b_host state).

NOTE: This seems kind of weird, the USB cable may not allow this kind of connection.

\h 9.7 Mini-B plug attached with a OTG Dual-Role device in the other end

In this scenario, the ID pin is floating. The stack starts in Target Stack,
and tries to initiate SRP to the device at the other end (since the device
in the other end of the cable is OTG capable, it can respond to the
SRP and raise VBUS to the valid range). So this device will work in
peripheral mode if the other end turns on VBUS. If the other end device
finds out it can not support this device, then the other end will suspend
the bus and give this device a chance to switch into b_host mode to control
the bus. If this device also finds out it can not support the other end of
the device, then it will suspend the bus again and the other end will find
out both device will not communicate, and thus will turn off the VBUS.

\h 9.8 Working as A-device but VBUS is turned off, gets SRP from the other end

In this scenario, the ID pin is grounded. The stack is in Host Stack and
will turn on the VBUS in response to the SRP.

\h 9.9 Working as A-device host, gets HNP (host request flag) from the other end

In this scenario, the ID pin is grounded. The stack is in Host Stack and
will suspend the bus when A-device application finished using the bus,
accepting the HNP. When the other end disconnects this device connects to
the other end and switch to Target Stack.

\h 9.10 Working as A-device host, the other end disconnects or Mini-B plug detached

In this scenario, the ID pin is grounded. The stack is in Host Stack and
when the other end disconnects, it will handle device disconnection as
normal Host Stack, but will still keep in Host Stack (since Mini-A plug is
still attached) waiting for B-device attachment and connection.

\h 9.11 Working as A-device host, Mini-A plug detached

In this scenario, the ID pin is grounded (will become floating soon). The stack
is in Host Stack and when the Mini-A plug detached, it will handle device
disconnection as normal Host Stack and then will find the ID pin get floating,
so the Stack will switch to Target Stack and in b_idle.

\h 9.12 Working as B-device peripheral, HNP feature enabled from the other end

In this scenario, the ID pin is floating. The stack is in Target Stack.
When the other end suspend the bus this device disconnects from the other
end and switch to Host Stack if a connection is detected from A-device.

\h 9.13 Working as B-device peripheral, Mini-A plug or Mini-B plug detached

In this scenario, the ID pin is floating. The stack is in Target Stack.
When detects either Mini-A plug or Mini-B plug detached, it will handle peripheral
side disconnection but will still keep in Target Stack (if the device is self
powered); If the device is bus powered, then the B-device is dead of course.

\h 10. SUPPORTED IOCTL COMMANDS

The VxWorks USB OTG Stack iosLib interface supports the following ioctls. 
These ioctls are furhter encapsulated to form the user level API routines.

\h 10.1 USBOTG_IOCTL_OTG_STATE_GET :

      Get current USB OTG state; See comments for usrUsbOtgStateGet().

\h 10.2 USBOTG_IOCTL_VBUS_STATE_GET :

      Get the VBUS state; See comments for usrUsbOtgVbusStateGet().

\h 10.3 USBOTG_IOCTL_DEV_TYPE_GET :

      Get the local OTG device type (A-device or B-device); See comments for
      usrUsbOtgDeviceTypeGet().

\h 10.4 USBOTG_IOCTL_HOST_REQUEST :

      Request for host role; See comments for usrUsbOtgHostRequest().

\h 10.5 USBOTG_IOCTL_HOST_GIVEUP :

      Give up host role; See comments for usrUsbOtgHostGiveup().

\h 10.6 USBOTG_IOCTL_VBUS_POWER_ON :

      Request to power on VBUS; See comments for usrUsbOtgVbusPowerOn().

\h 10.7 USBOTG_IOCTL_VBUS_POWER_OFF :
      Request to power off VBUS; See comments for usrUsbOtgVbusPowerOff().
      
\h 10.8 USBOTG_IOCTL_CALLBACK_INSTALL :

      Request to install user message callback. See comments for 
      usrUsbOtgCallbackInstall().
      
\h 10.9 USBOTG_IOCTL_STACK_MODE_GET :

      Get current USB OTG stack mode; See comments for usrUsbOtgStackModeGet().    

INCLUDE FILES: iosLib.h drv/erf/erfLib.h errnoLib.h poolLib.h 
               usb/usbOsalDebug.h usb/usbOtg.h
*/

#include <vxWorks.h>
#include <iosLib.h>
#include <drv/erf/erfLib.h>
#include <errnoLib.h>
#include <poolLib.h>
#include <usb/usbOsalDebug.h>
#include <usb/usbOtg.h>

/* globals */

UINT16 usbOtgEventCategory = 0;
UINT16 usbOtgCoreEventType = 0;
UINT16 usbOtgUserEventType = 0;

atomic_t usbOtgInitialized = _VX_ATOMIC_INIT(0);

/* externs */

/* locals */

LOCAL LIST                  usbOtgCtlrList;
LOCAL SEM_ID                usbOtgCtlrListLock = NULL;
LOCAL int                   usbOtgDrvNum = 0;
LOCAL POOL_ID               usbOtgEventDataPoolId = NULL;

LOCAL const char * usbOtgStateName[] = /* Must be 1-1 match with USBOTG_STATE */
    {
    "USBOTG_STATE_unknown",
    "USBOTG_STATE_a_idle",
    "USBOTG_STATE_a_wait_vrise",
    "USBOTG_STATE_a_wait_bcon",
    "USBOTG_STATE_a_host",
    "USBOTG_STATE_a_suspend",
    "USBOTG_STATE_a_peripheral",
    "USBOTG_STATE_a_wait_vfall",
    "USBOTG_STATE_a_vbus_err",
    "USBOTG_STATE_b_idle",
    "USBOTG_STATE_b_srp_init",
    "USBOTG_STATE_b_peripheral",
    "USBOTG_STATE_b_wait_acon",
    "USBOTG_STATE_b_host"
    };

LOCAL const char * usbOtgTimerName[] = /* Must be 1-1 match with USBOTG_TIMER */
    {
    "USBOTG_TIMER_unknown_tmr",
    "USBOTG_TIMER_a_wait_vrise_tmr",
    "USBOTG_TIMER_a_wait_vfall_tmr",
    "USBOTG_TIMER_a_wait_bcon_tmr",
    "USBOTG_TIMER_a_aidl_bdis_tmr",
    "USBOTG_TIMER_a_bidl_adis_tmr",
    "USBOTG_TIMER_b_ase0_brst_tmr",
    "USBOTG_TIMER_b_data_pulse_tmr",
    "USBOTG_TIMER_b_vbus_pulse_tmr",
    "USBOTG_TIMER_b_srp_fail_tmr",
    "USBOTG_TIMER_b_srp_ready_tmr",
    "USBOTG_TIMER_a_powerup_tmr"
    };

LOCAL const char * usbOtgVbusName[] = /* Must be 1-1 match with USBOTG_VBUS_STATE */
    {
    "USBOTG_VBUS_STATE_SessEnd",
    "USBOTG_VBUS_STATE_SessValid",
    "USBOTG_VBUS_STATE_VBusValid"
    };

LOCAL STATUS usbOtgStateAdvance
    (
    pUSBOTG_MANAGER     pOTG
    );

LOCAL VOID usbOtgNotifyIdStateChanged
    (
    pUSBOTG_MANAGER     pOTG,
    USBOTG_ID_STATE     idState
    );

LOCAL VOID usbOtgNotifyVbusStateChanged
    (
    pUSBOTG_MANAGER     pOTG,
    USBOTG_VBUS_STATE   vbusState
    );

LOCAL VOID usbOtgNotifySrpDetected
    (
    pUSBOTG_MANAGER     pOTG
    );

LOCAL VOID usbOtgNotifySuspendResumeDetected
    (
    pUSBOTG_MANAGER     pOTG,
    BOOL                bSuspend
    );

LOCAL VOID usbOtgNotifyConnectDisconnectDetected
    (
    pUSBOTG_MANAGER     pOTG,
    BOOL                bConnect
    );

LOCAL VOID usbOtgNotifyBusResetDetected
    (
    pUSBOTG_MANAGER     pOTG
    );

LOCAL VOID usbOtgNotifyOtgDeviceEnumerated
    (
    pUSBOTG_MANAGER     pOTG,
    UINT32              hDevice,
    UCHAR *             pOtgDescr
    );

LOCAL VOID usbOtgNotifySetOtgFeatureReceived
    (
    pUSBOTG_MANAGER     pOTG,
    UINT8               bOtgFeature
    );

LOCAL STATUS usbOtgNotifyGetOtgStatusReceived
    (
    pUSBOTG_MANAGER     pOTG,
    UINT8 *             pOtgStatus
    );

LOCAL VOID usbOtgTimerTimeoutAction
    (
    pUSBOTG_MANAGER     pOTG
    );

LOCAL VOID usbOtgTimerTimeout
    (
    pUSBOTG_MANAGER     pOTG
    );

LOCAL STATUS usbOtgTimerStop
    (
    pUSBOTG_MANAGER     pOTG,
    USBOTG_TIMER        timerId
    );

LOCAL STATUS usbOtgTimerStart
    (
    pUSBOTG_MANAGER     pOTG,
    USBOTG_TIMER        timerId
    );

LOCAL STATUS usbOtgTgtDisable
    (
    pUSBOTG_MANAGER     pOTG
    );

LOCAL STATUS usbOtgHstDisable
    (
    pUSBOTG_MANAGER     pOTG
    );

LOCAL STATUS usbOtgTgtEnable
    (
    pUSBOTG_MANAGER     pOTG
    );

LOCAL STATUS usbOtgHstEnable
    (
    pUSBOTG_MANAGER     pOTG
    );

LOCAL STATUS usbOtgStackSwitch
    (
    pUSBOTG_MANAGER     pOTG,
    USBOTG_MODE         mode,
    BOOL                bForce
    );

LOCAL STATUS usbOtgIdVbusProbe
    (
    pUSBOTG_MANAGER     pOTG
    );

LOCAL VOID usbOtgNewStateEnter
    (
    pUSBOTG_MANAGER     pOTG,
    USBOTG_STATE        newState
    );

LOCAL USBOTG_STATE usbOtgStateSingleStep
    (
    pUSBOTG_MANAGER     pOTG
    );

LOCAL STATUS usbOtgStateAdvance
    (
    pUSBOTG_MANAGER     pOTG
    );

LOCAL STATUS usbOtgStateGet
    (
    pUSBOTG_MANAGER     pOTG,
    USBOTG_STATE *      pState
    );

LOCAL STATUS usbOtgVbusStateGet
    (
    pUSBOTG_MANAGER     pOTG,
    USBOTG_VBUS_STATE * pVbus
    );

LOCAL STATUS usbOtgDeviceTypeGet
    (
    pUSBOTG_MANAGER     pOTG,
    USBOTG_DEVICE_TYPE *pType
    );

LOCAL STATUS usbOtgHostRoleRequest
    (
    pUSBOTG_MANAGER             pOTG,
    USBOTG_HOST_REQUEST_TYPE    reqType,
    _Vx_ticks_t                 waitTime
    );

LOCAL STATUS usbOtgHostRoleGiveup
    (
    pUSBOTG_MANAGER     pOTG,
    int                 waitTime
    );

LOCAL STATUS usbOtgVbusPowerOn
    (
    pUSBOTG_MANAGER     pOTG,
    _Vx_ticks_t         onTime
    );

LOCAL STATUS usbOtgVbusPowerOff
    (
    pUSBOTG_MANAGER     pOTG,
    _Vx_ticks_t         offTime
    );

LOCAL VOID usbOtgHnpPolling
    (
    pUSBOTG_MANAGER     pOTG
    );

LOCAL STATUS usbOtgUsrMsgNotify
    (
    pUSBOTG_MANAGER     pOTG,
    void *              pUsrMsg
    );

LOCAL VOID usbOtgManagerTask
    (
    pUSBOTG_MANAGER     pOTG
    );

LOCAL STATUS usbOtgManagerDestroy
    (
    pUSBOTG_OCD         pOCD
    );

LOCAL STATUS usbOtgManagerCreate
    (
    pUSBOTG_OCD         pOCD
    );

LOCAL STATUS usbOtgInit (void);

LOCAL STATUS usbOtgUnInit (void);

/* routines */

/*******************************************************************************
*
* usbOtgNotifyGetOtgStatusReceived - notify GetStatus(OTG status) received
*
* This routine is called by USB Target Stack TML to notify GetStatus
* (OTG status) received event to the OTG Framework. The USB Target Stack
* HAL shall check the return value of this call, if it is OK, then
* it shall use the value saved in the <pOtgStatus> buffer as the status
* value to return to the host. If the return value is ERROR, it shall
* STALL the default control endpoint.
*
* RETURNS: OK, or ERROR if failed to get OTG status
*
* ERRNO: N/A
*/

LOCAL STATUS usbOtgNotifyGetOtgStatusReceived
    (
    pUSBOTG_MANAGER     pOTG,
    UINT8 *             pOtgStatus
    )
    {
    STATUS sts = ERROR;

    /* Check if the parameters are valid */

    if (!pOTG || !pOTG->pOCD)
        {
        USBOTG_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    USBOTG_DBG("usbOtgNotifyGetOtgStatusReceived\n",
                1, 2, 3, 4, 5, 6);

    /* Lock the OTG instance */

    USBOTG_LOCK_INSTANCE(pOTG);

    /*
     * If the B-device is not HNP capable, it shall return a STALL
     * if it receives a GetStatus command for the OTG feature selector.
     */

    if ((pOTG->isHcdLoaded == TRUE) &&
        (pOTG->pOCD->bmAttributes & USBOTG_ATTR_HNP_SUPPORT))
        {
        *pOtgStatus = (UINT8)(pOTG->param.extend.host_request_flag ?
                      USBOTG_STS_HOST_REQ_FLAG : 0);
                
        USBOTG_DBG("usbOtgNotifyGetOtgStatusReceived - returns OtgStatus 0x%X\n",
                    *pOtgStatus, 2, 3, 4, 5, 6);
        sts = OK;
        }

    /* Unlock the OTG instance */

    USBOTG_UNLOCK_INSTANCE(pOTG);

    return sts;
    }

/*******************************************************************************
*
* usbOtgNotifySetOtgFeatureReceived - notify SetFeature of OTG features received
*
* This routine is called by USB Target Stack TML to notify SetFeature of OTG
* features (such as b_hnp_enable, a_hnp_support, a_alt_hnp_support) received
* event to the OTG Framework.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL VOID usbOtgNotifySetOtgFeatureReceived
    (
    pUSBOTG_MANAGER pOTG,
    UINT8           bOtgFeature
    )
    {
    /* Check if the parameters are valid */

    if (!pOTG || !pOTG->pOCD)
        {
        USBOTG_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);

        return;
        }

    USBOTG_DBG("usbOtgNotifySetOtgFeatureReceived\n",
                1, 2, 3, 4, 5, 6);

    /* Lock the OTG instance */

    USBOTG_LOCK_INSTANCE(pOTG);

    /*
     * If the ID pin is FALSE, it means we are A-device, but only when
     * we are B-device shall we receive and process these HNP related
     * features from the A-device.
     */

    if (pOTG->param.input.id == FALSE)
        {
        /*
         * If we are A-device (and in a_peripheral state), we should
         * ignore setting of these HNP related features defined as far
         * as OTG 2.0. It is possible that later OTG evolution might
         * add other OTG features that can be received and processed
         * when an A-device is in a_peripheral state. Until then we
         * should add code here to process these new feature commands.
         * For now, there is nothing to do here since there is no OTG
         * feature commands intended for A-device (a_peripheral).
         */

        USBOTG_WARN("Unsupported feature 0x%X for A-device\n",
                    bOtgFeature, 2, 3, 4, 5, 6);
        }
    else /* if (pOTG->param.input.id == TRUE) */
        {
        /*
         * As of OTG 2.0, only HNP related OTG features might be issued
         * from the A-device, these features are only to be accepted
         * when we (B-device) support HNP. Note that "pOCD->bmAttributes"
         * represents the OTG Controller attributes and is retrieved when
         * OTG Framework is initialized.
         */

        if (pOTG->pOCD->bmAttributes & USBOTG_ATTR_HNP_SUPPORT)
            {
            switch (bOtgFeature)
                {
                case USBOTG_FEATURE_b_hnp_enable:
                    {
                    USBOTG_WARN("B-device got USBOTG_FEATURE_b_hnp_enable\n",
                                1, 2, 3, 4, 5, 6);

                    pOTG->param.internal.b_hnp_en = TRUE;
                    }
                    break;
                case USBOTG_FEATURE_a_hnp_support:
                    {
                    USBOTG_WARN("B-device got USBOTG_FEATURE_a_hnp_support\n",
                                1, 2, 3, 4, 5, 6);

                    pOTG->param.extend.b_a_hnp_support = TRUE;
                    }
                    break;
                case USBOTG_FEATURE_a_alt_hnp_support:
                    {
                    USBOTG_WARN("B-device got USBOTG_FEATURE_a_alt_hnp_support\n",
                                1, 2, 3, 4, 5, 6);

                    pOTG->param.extend.b_a_alt_hnp_support = TRUE;
                    }
                    break;
                default:
                    {
                    USBOTG_WARN("Unsupported feature 0x%X for B-device\n",
                                bOtgFeature, 2, 3, 4, 5, 6);

                    /* Unlock the OTG instance */

                    USBOTG_UNLOCK_INSTANCE(pOTG);

                    return;
                    }
                }

            /* Unlock the OTG instance */

            USBOTG_UNLOCK_INSTANCE(pOTG);

            /* Notify parameter changed */

            USBOTG_NOTIFY_PARAM_CHANGED(pOTG);

            return;
            }
        }

    /* Unlock the OTG instance */
    
    USBOTG_UNLOCK_INSTANCE(pOTG);

    return;
    }

/*******************************************************************************
*
* usbOtgNotifySetConfigReceived - notify SetConfiguration received
*
* This routine is called by USB Target Stack TML to notify reception of a
* SetConfiguration request with a none-default configuration.
*
* If neither the b_hnp_enable or a_hnp_support features are set before
* the A-device puts the B-device in a non-default configuration, then
* the B-device may indicate to the user that HNP is not supported through
* the current connection.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL VOID usbOtgNotifySetConfigReceived
    (
    pUSBOTG_MANAGER pOTG
    )
    {
    /* Check if the parameters are valid */

    if (!pOTG || !pOTG->pOCD)
        {
        USBOTG_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);

        return;
        }

    USBOTG_DBG("usbOtgNotifySetConfigReceived\n",
                1, 2, 3, 4, 5, 6);

    if ((pOTG->param.input.id == TRUE) &&
        (pOTG->param.internal.b_hnp_en != TRUE) && 
        (pOTG->param.extend.b_a_hnp_support != TRUE))
        {
        pUSBOTG_USR_MSG_HDR pUsrMsgHdr = 
            (pUSBOTG_USR_MSG_HDR)usbOtgEventDataGet();

        if (pUsrMsgHdr)
            {
            /* Notify the user of this event */

            pUsrMsgHdr->uMsgId = USBOTG_USR_MSG_ID_HOST_NO_HNP;
            
            usbOtgUsrMsgNotify(pOTG, (void *)pUsrMsgHdr);
            }
        else
            {
            USBOTG_WARN("Get msg USBOTG_USR_MSG_ID_HOST_NO_HNP failed!\n",
                        1, 2, 3, 4, 5, 6);
            }
        }
    
    return;
    }

/*******************************************************************************
*
* usbOtgNotifyGetOtgDescrReceived - notify GetDescriptor(OTG) received
*
* This routine is called by USB Target Stack TML to notify GetDescriptor(OTG)
* received event to the OTG Framework. The USB Target Stack TML shall check the
* return value of this call, if it is OK, then it shall use the value copied in
* the <pOtgDescr> buffer, and <pOtgDescrSize> as size to return to the host. 
* If the return value is ERROR, it shall not include any OTG descriptor.
*
* RETURNS: OK, or ERROR if failed to get OTG descriptor
*
* ERRNO: N/A
*/

LOCAL STATUS usbOtgNotifyGetOtgDescrReceived
    (
    pUSBOTG_MANAGER     pOTG,
    UINT8 *             pOtgDescr
    )
    {
    USBOTG_OTG_DESCR otgDescr;

    USBOTG_DBG("usbOtgNotifyGetOtgDescrReceived\n",
            1, 2, 3, 4, 5, 6);
    
    if (!pOtgDescr)
        return ERROR;
    
    otgDescr.bLength = sizeof(USBOTG_OTG_DESCR);
    otgDescr.bDescriptorType = USBOTG_DESC_TYPE;
    otgDescr.bmAttributes = pOTG->pOCD->bmAttributes;

    /* If the HCD is not loaded, we can not support HNP */
    
    if (pOTG->isHcdLoaded != TRUE)
        {
        otgDescr.bmAttributes = (UINT8) (otgDescr.bmAttributes & ~USBOTG_ATTR_HNP_SUPPORT);
        }
    
#if (USBOTG_VERSION_SUPPORTED >= USBOTG_VERSION0200)
    /* Added field in OTG 2.0 */

    otgDescr.bcdOTG = USBOTG_VERSION_SUPPORTED;
#endif

    /* Copy the minimum size requsted by the Target Stack */

    memcpy(pOtgDescr, &otgDescr, sizeof(USBOTG_OTG_DESCR));

    return OK;
    }

/*******************************************************************************
*
* usbOtgNotifyOtgDeviceEnumerated - notify OTG device enumerated
*
* This routine is called by USB Host Stack USBD to notify OTG device
* enumerated event to the OTG Framework.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL VOID usbOtgNotifyOtgDeviceEnumerated
    (
    pUSBOTG_MANAGER pOTG,
    UINT32         hDevice,
    UCHAR *        pOtgDescr
    )
    {
    pUSBOTG_OTG_DESCR pOtgDesc;

    /* Check if the parameters are valid */

    if (!pOTG || !pOTG->pOCD)
        {
        USBOTG_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);

        return;
        }

    USBOTG_DBG("usbOtgNotifyOtgDeviceEnumerated - hDevice = %p\n",
                hDevice, 2, 3, 4, 5, 6);

    /* Lock the OTG instance */

    USBOTG_LOCK_INSTANCE(pOTG);

    /* Save the remote OTG devie handle */

    pOTG->hOtgDevice = hDevice;

    /* Only A-device needs to check the OTG Descriptor */

    pOtgDesc = (pUSBOTG_OTG_DESCR)pOtgDescr;

    USBOTG_DBG("Got an OTG device 0x%X with OTG descriptor %p\n",
               pOTG->hOtgDevice, pOtgDesc, 3, 4, 5, 6);

    /*
     * If the ID pin is TRUE, it means we are B-device, but only when
     * we are A-device shall we set these HNP related features to the
     * B-device. Thus if we are B-device, we will return here.
     *
     * We will also return here if it is not OTG Descriptor.
     */

    if ((pOtgDesc == NULL) ||
        (pOtgDesc->bDescriptorType != USBOTG_DESC_TYPE))
        {
        /* Unlock the OTG instance */

        USBOTG_UNLOCK_INSTANCE(pOTG);

        return;
        }

    /* Reset a_set_b_hnp_en to FALSE */

    pOTG->param.internal.a_set_b_hnp_en = FALSE;

    pOTG->param.extend.rmt_bm_attr = pOtgDesc->bmAttributes;

    /* The bcdOTG filed is only added field in OTG 2.0 */

#if (USBOTG_VERSION_SUPPORTED >= USBOTG_VERSION0200)

    if (pOtgDesc->bLength >= USBOTG_DESC_LENGTH_VER20)
        {
        pOTG->param.extend.rmt_bcd_otg = pOtgDesc->bcdOTG;

        if (pOtgDesc->bcdOTG < USBOTG_VERSION0200)
            {
            USBOTG_WARN("Invalid bcdOTG 0x%X,"
                        "possibly corrupted OTG descriptor"
                        "or the device is a quirky device?\n",
                        pOtgDesc->bcdOTG, 2, 3, 4, 5, 6);
            }
        }
    else /* We could ignore it, but let's default to OTG 1.3 level */
#endif
        {
        pOTG->param.extend.rmt_bcd_otg = USBOTG_VERSION0130;

        /* ADP support is only added in OTG 2.0 */

        if (pOTG->param.extend.rmt_bm_attr & USBOTG_ATTR_ADP_SUPPORT)
            {
            USBOTG_WARN("Invalid bmAttributes 0x%X,"
                        "possibly corrupted OTG descriptor"
                        "or the device is a quirky device?\n",
                        pOtgDesc->bmAttributes, 2, 3, 4, 5, 6);
            }
        }

    /*
     * Only if both A-device and B-device are capable of HNP
     * can we (A-device) enable B-device for HNP.
     *
     * Note that "pOTG->bmAttributes" represents the hardware
     * attributes of our underlying OTG Controller, and it is
     * retrieved when the OTG Framework is initialized.
     */

    if ((pOTG->pOCD->bmAttributes & USBOTG_ATTR_HNP_SUPPORT) &&
        (pOtgDesc->bmAttributes & USBOTG_ATTR_HNP_SUPPORT))
        {
        /* Indicate to the B-device that we can support HNP */

        if (usbHstSetFeature(hDevice,
                             USBHST_RECIPIENT_DEVICE,
                             0,
                             USBOTG_FEATURE_a_hnp_support,
                             0) != USBHST_SUCCESS)
            {
            USBOTG_WARN("Setting USBOTG_FEATURE_a_hnp_support failed\n",
                        1, 2, 3, 4, 5, 6);
            }

        /*
         * Indicate to the B-device that it is allowed to do HNP
         * but it has to wait until we (A-device) suspend the bus.
         *
         * We (A-device) will only suspend the bus when our user
         * application doesn't need to use the bus any more (by
         * setting a_bus_drop to TRUE and a_bus_req to FALSE).
         */

        if (usbHstSetFeature(hDevice,
                             USBHST_RECIPIENT_DEVICE,
                             0,
                             USBOTG_FEATURE_b_hnp_enable,
                             0) != USBHST_SUCCESS)
            {
            USBOTG_WARN("Setting USBOTG_FEATURE_b_hnp_enable failed\n",
                        1, 2, 3, 4, 5, 6);
            }
        else
            {
            USBOTG_WARN("Setting USBOTG_FEATURE_b_hnp_enable OK\n",
                        1, 2, 3, 4, 5, 6);
            
            if (pOTG->param.input.id == FALSE)
                {
                /* If we have successfully set the b_hnp_enable, record it! */
    
                pOTG->param.internal.a_set_b_hnp_en = TRUE;
                }

            /* Change the OTG Manager task wait time as polling interval */
            
            pOTG->mgrInterval = usrUsbOtgHnpPollingIntervalGet();
            }
        }

    /* Unlock the OTG instance */

    USBOTG_UNLOCK_INSTANCE(pOTG);

    /* Notify parameter changed */

    USBOTG_NOTIFY_PARAM_CHANGED(pOTG);
    }

/*******************************************************************************
*
* usbOtgNotifyOtgDeviceEnumerated - notify OTG device enumerated
*
* This routine is called by USB Host Stack USBD to notify OTG device unsupported
* event to the OTG Framework.
*
* If a B-device can not be supported by A-device, we will indicate to the user 
* application the B-device cannot be supported by the A-device by a user message.
* The user application should decide if it will load a new driver or use ioctl 
* USBOTG_IOCTL_HOST_GIVEUP to call usbOtgHostRoleGiveup() and start the HNP.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL VOID usbOtgNotifyOtgDeviceUnsupported
    (
    pUSBOTG_MANAGER pOTG,
    UINT32          hDevice
    )
    {
    /* Check if the parameters are valid */

    if (!pOTG || !pOTG->pOCD)
        {
        USBOTG_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);

        return;
        }

    USBOTG_DBG("usbOtgNotifyOtgDeviceUnsupported - hDevice = %p\n",
                hDevice, 2, 3, 4, 5, 6);

    /* Check if this is the OTG device just attached */
    
    if (hDevice == pOTG->hOtgDevice)
        {
        pUSBOTG_USR_MSG_HDR pUsrMsgHdr = 
            (pUSBOTG_USR_MSG_HDR)usbOtgEventDataGet();

        if (pUsrMsgHdr)
            {
            /* Cast to specific message */
            
            pUSBOTG_USR_MSG_DEV_UNSUPPORTED pUsrMsg = 
                (pUSBOTG_USR_MSG_DEV_UNSUPPORTED)(pUsrMsgHdr);
            
            /* Fill the message specific information */
            
            pUsrMsg->hDevice = pOTG->hOtgDevice;
            
            /* Notify the user of this event */

            pUsrMsgHdr->uMsgId = USBOTG_USR_MSG_ID_DEV_UNSUPPORTED;
            
            usbOtgUsrMsgNotify(pOTG, (void *)pUsrMsgHdr);
            }
        else
            {
            USBOTG_WARN("Get msg USBOTG_USR_MSG_ID_DEV_UNSUPPORTED failed!\n",
                        1, 2, 3, 4, 5, 6);
            }
        }
    }

/*******************************************************************************
*
* usbOtgNotifyIdStateChanged - notify ID state change
*
* This routine is called by tErfTask context to notify ID state change to the 
* OTG Framework.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL VOID usbOtgNotifyIdStateChanged
    (
    pUSBOTG_MANAGER     pOTG,
    USBOTG_ID_STATE     idState
    )
    {
    /* Check if the parameters are valid */

    if (!pOTG || !pOTG->pOCD)
        {
        USBOTG_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);

        return;
        }

    USBOTG_WARN("usbOtgNotifyIdStateChanged - ID %s\n",
                (idState == USBOTG_ID_LOW) ? "LOW" : "HIGH", 2, 3, 4, 5, 6);
    
    /* Lock the OTG instance */

    USBOTG_LOCK_INSTANCE(pOTG);

    /* Save the latest ID state */

    pOTG->idState = idState;

    if (idState == USBOTG_ID_LOW)
        pOTG->param.input.id = FALSE;
    else if (idState == USBOTG_ID_HIGH)
        pOTG->param.input.id = TRUE;

    /* Unlock the OTG instance */

    USBOTG_UNLOCK_INSTANCE(pOTG);

    /* Notify parameter changed */

    USBOTG_NOTIFY_PARAM_CHANGED(pOTG);
    }

/*******************************************************************************
*
* usbOtgNotifyVbusStateChanged - notify VBUS state change
*
* This routine is called by tErfTask context to notify VBUS state change to the 
* OTG Framework.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL VOID usbOtgNotifyVbusStateChanged
    (
    pUSBOTG_MANAGER     pOTG,
    USBOTG_VBUS_STATE   vbusState
    )
    {
    /* Check if the parameters are valid */

    if (!pOTG || !pOTG->pOCD)
        {
        USBOTG_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);

        return;
        }

    USBOTG_WARN("usbOtgNotifyVbusStateChanged - vbusState = %s\n",
                usrUsbOtgVbusStateNameGet(vbusState), 2, 3, 4, 5, 6);

    /* Lock the OTG instance */

    USBOTG_LOCK_INSTANCE(pOTG);

    /* Save the latest VBUS state */

    pOTG->vbusState = vbusState;

    /* ID pin as FALSE means A-device */

    if (pOTG->param.input.id == FALSE)
        {
        if (vbusState == USBOTG_VBUS_STATE_SessEnd)
            {
            pOTG->param.input.a_sess_vld = FALSE;
            pOTG->param.input.a_vbus_vld = FALSE;
            }
        else if (vbusState == USBOTG_VBUS_STATE_SessValid)
            {
            pOTG->param.input.a_sess_vld = TRUE;
            pOTG->param.input.a_vbus_vld = FALSE;
            }
        else if (vbusState == USBOTG_VBUS_STATE_VBusValid)
            {
            pOTG->param.input.a_sess_vld = TRUE;
            pOTG->param.input.a_vbus_vld = TRUE;
            }
        }
    else /* ID pin as TRUE means B-device */
        {
        if (vbusState == USBOTG_VBUS_STATE_SessEnd)
            {
            pOTG->param.input.b_sess_end = TRUE;
            pOTG->param.input.b_sess_vld = FALSE;
            }
        else if (vbusState == USBOTG_VBUS_STATE_SessValid)
            {
            pOTG->param.input.b_sess_end = FALSE;
            pOTG->param.input.b_sess_vld = TRUE;
            }
        else if (vbusState == USBOTG_VBUS_STATE_VBusValid)
            {
            pOTG->param.input.b_sess_end = FALSE;
            pOTG->param.input.b_sess_vld = TRUE;
            }

        /* Reset the features that may be set from the A-device */

        if (pOTG->param.input.b_sess_vld == FALSE)
            {
            USBOTG_WARN("usbOtgNotifyVbusStateChanged - Vbus low, reset b_hnp_en\n",
                        1, 2, 3, 4, 5, 6);
            /*
             * The b_hnp_en is cleared on a bus reset or when
             * b_sess_vld is FALSE
             */

            pOTG->param.internal.b_hnp_en = FALSE;

            /*
             * The a_hnp_support feature is only cleared on a
             * bus reset or at the end of a session.
             */

            pOTG->param.extend.b_a_hnp_support = FALSE;

            /*
             * The a_alt_hnp_support feature is only cleared on a
             * bus reset or at the end of a session.
             */

            pOTG->param.extend.b_a_alt_hnp_support = FALSE;
            }
        }

    /* Unlock the OTG instance */

    USBOTG_UNLOCK_INSTANCE(pOTG);

    /* Notify parameter changed */

    USBOTG_NOTIFY_PARAM_CHANGED(pOTG);
    }

/*******************************************************************************
*
* usbOtgNotifySrpDetected - notify SRP detected event
*
* This routine is called by tErfTask context to notify SRP detected event to the 
* OTG Framework.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL VOID usbOtgNotifySrpDetected
    (
    pUSBOTG_MANAGER pOTG
    )
    {
    pUSBOTG_USR_MSG_HDR pUsrMsgHdr;
    
    /* Check if the parameters are valid */

    if (!pOTG || !pOTG->pOCD)
        {
        USBOTG_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);

        return;
        }

    /* Lock the OTG instance */

    USBOTG_LOCK_INSTANCE(pOTG);

    /* ID pin as FALSE means A-device */

    if (pOTG->param.input.id == FALSE)
        {
        /* The a_srp_det only is meaningfull for A-device */

        pOTG->param.input.a_srp_det = TRUE;

        USBOTG_DBG("usbOtgNotifySrpDetected\n",
                    1, 2, 3, 4, 5, 6);
        
        /* Unlock the OTG instance */

        USBOTG_UNLOCK_INSTANCE(pOTG);
        }
    else
        {
        USBOTG_DBG("usbOtgNotifySrpDetected - ignored by B-device\n",
                    1, 2, 3, 4, 5, 6);
                
        /* Unlock the OTG instance */

        USBOTG_UNLOCK_INSTANCE(pOTG);

        return;
        }
    
    pUsrMsgHdr = (pUSBOTG_USR_MSG_HDR)usbOtgEventDataGet();
    
    if (pUsrMsgHdr)
        {
        /* Notify the user of this event */
    
        pUsrMsgHdr->uMsgId = USBOTG_USR_MSG_ID_SRP_DETECTED;
        
        usbOtgUsrMsgNotify(pOTG, (void *)pUsrMsgHdr);
        }
    else
        {
        USBOTG_WARN("Get msg USBOTG_USR_MSG_ID_SRP_DETECTED failed!\n",
                    1, 2, 3, 4, 5, 6);
        }
    
    /* Notify parameter changed */

    USBOTG_NOTIFY_PARAM_CHANGED(pOTG);
    }

/*******************************************************************************
*
* usbOtgNotifyBusResetDetected - notify Bus Reset detected event
*
* This routine is called by tErfTask context to notify Bus Reset detected event
* to the OTG Framework.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL VOID usbOtgNotifyBusResetDetected
    (
    pUSBOTG_MANAGER pOTG
    )
    {
    /* Check if the parameters are valid */

    if (!pOTG || !pOTG->pOCD)
        {
        USBOTG_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);

        return;
        }

    USBOTG_WARN("usbOtgNotifyBusResetDetected - %s-device got Bus Reset!\n",
        (pOTG->param.input.id == FALSE) ? "A" : "B", 2, 3, 4, 5, 6);

    /* Lock the OTG instance */

    USBOTG_LOCK_INSTANCE(pOTG);

    /* Set the suspend/resume as FALSE */
    
    pOTG->param.input.a_bus_suspend = FALSE;
    pOTG->param.input.a_bus_resume = FALSE;
    pOTG->param.input.b_bus_suspend = FALSE;
    pOTG->param.input.b_bus_resume = FALSE;

    /* The device should have disconnected */
    
    pOTG->hOtgDevice = 0;

    /* The a_conn can not be TRUE now since a reset is here */
    
    pOTG->param.input.a_conn = FALSE;

    /* 
     * Pretend that we need the bus so that once we got switched 
     * back to host mode we will be in the a_host or b_host state,
     * instead of jumping to a_suspend...
     */
    
    if (pOTG->param.input.id == FALSE)
        {
        pOTG->param.input.a_bus_req = TRUE;
        pOTG->param.input.a_suspend_req = FALSE;
        }
    else
        {
        pOTG->param.input.b_bus_req = TRUE;
        }

    if (pOTG->stackMode == USBOTG_MODE_PERIPHERAL)
        {
        USBOTG_DBG("usbOtgNotifyBusResetDetected - ignored by %s-device\n",
                    (pOTG->param.input.id == FALSE) ? "A" : "B", 2, 3, 4, 5, 6);

        /* Unlock the OTG instance */

        USBOTG_UNLOCK_INSTANCE(pOTG);

        return;
        }

    /*
     * The b_hnp_en is cleared on a bus reset or when
     * b_sess_vld is FALSE
     */

    pOTG->param.internal.b_hnp_en = FALSE;

    /*
     * The a_hnp_support feature is only cleared on a
     * bus reset or at the end of a session.
     */

    pOTG->param.extend.b_a_hnp_support = FALSE;

    /*
     * The a_alt_hnp_support feature is only cleared on a
     * bus reset or at the end of a session.
     */

    pOTG->param.extend.b_a_alt_hnp_support = FALSE;
    
    /*
     * Try to switch into Target Stack if not already
     */

    usbOtgStackSwitch(pOTG, USBOTG_MODE_PERIPHERAL, TRUE);

    /* Unlock the OTG instance */

    USBOTG_UNLOCK_INSTANCE(pOTG);
    }

/*******************************************************************************
*
* usbOtgNotifySuspendResumeDetected - notify suspend/resume event detected
*
* This routine is called by tErfTask context to notify suspend or resume event
* to the OTG Framework.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL VOID usbOtgNotifySuspendResumeDetected
    (
    pUSBOTG_MANAGER pOTG,
    BOOL            bSuspend
    )
    {
    /* Check if the parameters are valid */

    if (!pOTG || !pOTG->pOCD)
        {
        USBOTG_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);

        return;
        }

    USBOTG_WARN("usbOtgNotifySuspendResumeDetected - %s\n",
        bSuspend ? "Suspend" : "Resume", 2, 3, 4, 5, 6);

    /* Lock the OTG instance */

    USBOTG_LOCK_INSTANCE(pOTG);

    /* Suspend received */

    if (bSuspend == TRUE)
        {
        /* If ID is FALSE, it is the A-device */

        if (pOTG->param.input.id == FALSE)
            {
            /* Update the b_bus_suspend input parameter */

            pOTG->param.input.b_bus_suspend = TRUE;

            /*
             * If we are currently in a_peripheral, the bus
             * suspend will lead us into a_wait_bcon state;
             * Let's enter Host Stack ASAP.
             */

            if (pOTG->currState == USBOTG_STATE_a_peripheral)
                {
                USBOTG_WARN("A-device got suspend in %s state, back to Host Stack\n",
                    usbOtgStateName[pOTG->currState], 2, 3, 4, 5, 6);
                
                /* 
                 * Try to switch into Host Stack. 
                 * 
                 * Note, in some extream cases, even we have been
                 * in a_peripheral state, we could immediately get a 
                 * suspend from the B-device before the stack switch 
                 * to Target mode. Taht is, when we are here, we might 
                 * still be in Host stack mode. In this case, we do not 
                 * need to force into Host stack.
                 */

                usbOtgStackSwitch(pOTG, USBOTG_MODE_HOST, FALSE);
                }
            }
        else /* If ID is TRUE, it is the B-device */
            {
            /*
             * Update the a_bus_suspend input parameter to TRUE;
             * This might move us into b_wait_acon, but we won't
             * switch into Host Stack until we get connection from
             * A-device. We will also watch out for the case where
             * resume is sent from A-device while in b_wait_acon,
             * in which case we will go back into b_peripheral state.
             */
            USBOTG_WARN("B-device got suspend in %s state\n",
                usbOtgStateName[pOTG->currState], 2, 3, 4, 5, 6);

            pOTG->param.input.a_bus_suspend = TRUE;
            }
        }
    else /* Resume received */
        {
        /* If ID is FALSE, it is the A-device */

        if (pOTG->param.input.id == FALSE)
            {
            /*
             * The b_bus_resume only applies to a_suspend state
             * and will lead us into a_host state.
             */

            if (pOTG->currState == USBOTG_STATE_a_suspend)
                {
                /* Update the a_bus_resume input parameter */

                pOTG->param.input.b_bus_resume = TRUE;

                USBOTG_WARN("A-device got resume in %s state\n",
                    usbOtgStateName[pOTG->currState], 2, 3, 4, 5, 6);

                #if (USBOTG_VERSION_SUPPORTED >= USBOTG_VERSION0200)
                /*
                 * OTG 2.0:
                 *
                 * The a_bus_req can also be set to TRUE in response
                 * to remote wakeup signaling from the B-device should
                 * the A-device decide to resume the bus.
                 */

                pOTG->param.input.a_bus_req = TRUE;
                #endif /* (USBOTG_VERSION_SUPPORTED ...) */
                }
            else
                {
                USBOTG_WARN("A-device ignored resume in %s state\n",
                    usbOtgStateName[pOTG->currState], 2, 3, 4, 5, 6);
                }
            }
        else /* If ID is TRUE, it is the B-device */
            {
            /*
             * The a_bus_resume only applies to b_wait_acon state
             * and it will lead us into b_peripheral state.
             */

            if (pOTG->currState == USBOTG_STATE_b_wait_acon)
                {
                /* Update the a_bus_resume input parameter */

                pOTG->param.input.a_bus_resume = TRUE;

                USBOTG_WARN("B-device got resume in %s state\n",
                    usbOtgStateName[pOTG->currState], 2, 3, 4, 5, 6);
                }
            else
                {
                USBOTG_WARN("B-device ignored resume in %s state\n",
                    usbOtgStateName[pOTG->currState], 2, 3, 4, 5, 6);
                }
            }
        }

    /* Unlock the OTG instance */

    USBOTG_UNLOCK_INSTANCE(pOTG);

    /* Notify parameter changed */

    USBOTG_NOTIFY_PARAM_CHANGED(pOTG);
    }

/*******************************************************************************
*
* usbOtgNotifyConnectDisconnectDetected - notify connect/disconnect event
*
* This routine is called by tErfTask context to notify peripheral connection 
* or diconnection detected event to the OTG Framework.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL VOID usbOtgNotifyConnectDisconnectDetected
    (
    pUSBOTG_MANAGER pOTG,
    BOOL            bConnect
    )
    {
    /* Check if the parameters are valid */

    if (!pOTG || !pOTG->pOCD)
        {
        USBOTG_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);

        return;
        }

    USBOTG_WARN("usbOtgNotifyConnectDisconnectDetected - %s\n",
        bConnect ? "Connect" : "Disconnect", 2, 3, 4, 5, 6);

    /* Lock the OTG instance */

    USBOTG_LOCK_INSTANCE(pOTG);
 
    /* 
     * If it is disconnection event, we should clear the status
     * set while in Host Stack mode.
     */

    if (bConnect == FALSE)
        {
        if ((pOTG->param.input.a_conn == FALSE) &&
            (pOTG->param.input.b_conn == FALSE))
            {
            USBOTG_WARN("Disconnect event but no device connected, ignroed!\n",
                       1, 2, 3, 4, 5, 6);
            
            /* Unlock the OTG instance */

            USBOTG_UNLOCK_INSTANCE(pOTG);
            
            return;
            }
        /*
         * If it is a disconnection, set the remote OTG device handle
         * to be invalid! The device handle can't be 0 if valid.
         */
        
        pOTG->hOtgDevice = 0;

        /* Clear the remote device attributes and parameters */
        
        pOTG->param.extend.rmt_bm_attr = 0;
        pOTG->param.extend.rmt_otg_sts = 0;
        pOTG->param.extend.rmt_bcd_otg = 0;

        /* Restore the default polling interval */
        
        pOTG->mgrInterval = usrUsbOtgMgrEventWaitTimeGet();
        }

    /*
     * Update the a_conn and b_con input parameters to FALSE,
     * and then set them to the current reported status.
     */

    pOTG->param.input.a_conn = FALSE;
    pOTG->param.input.b_conn = FALSE;

    /* If ID is FALSE, it is the A-device */

    if (pOTG->param.input.id == FALSE)
        {
        /* Update the b_conn input parameter */

        pOTG->param.input.b_conn = bConnect;

        /*
         * If we are A-device and in a_suspend, once a B-device
         * disconnection is detected, we should connect to B-device.
         *
         * It might be a Mini-A/B plug removal causing B-device
         * disconnection event, but it is still OK to do a dummy
         * connection in this case, only that there will be no
         * response (no subsequent bus reset).
         */

        if ((bConnect == FALSE) &&
            (pOTG->currState == USBOTG_STATE_a_suspend) &&
            (pOTG->pOcdFuncs->pOtgAutoDis2ConEnable == NULL))
            {
            USBOTG_WARN("HNP - A-device connecting to B-device\n",
                       1, 2, 3, 4, 5, 6);

            USBOTG_DP_PULL_UP_ENABLE(pOTG, TRUE);
            }
        }
    else /* If ID is TRUE, it is the B-device */
        {
        /* Update the a_conn input parameter */

        pOTG->param.input.a_conn = bConnect;

        /*
         * B-device stack switching happens on A-device
         * connection/disconnection events.
         */

        if (bConnect == TRUE)
            {
            /*
             * While in b_wait_acon state and a connection is got,
             * we move into Host Stack ASAP (and then the OTG State
             * will be in b_host). Theorytically we should check to
             * make sure we are currently in b_wait_acon state, but
             * in some extream cases, we might be in b_peripheral
             * while a connection is reported...
             */

            USBOTG_WARN("B-device got connection in %s state,"
                       "switch into Host Stack!\n",
                       usbOtgStateName[pOTG->currState], 
                       2, 3, 4, 5, 6);

            pOTG->currState = USBOTG_STATE_b_wait_acon;
            
            /* Switch into Host Stack */

            usbOtgStackSwitch(pOTG, USBOTG_MODE_HOST, TRUE);
            }
        else
            {
            /*
             * While in b_host state and a disconnection is got,
             * we move into Target Stack ASAP (and then the OTG State
             * will be in b_peripheral)
             */
             
            USBOTG_WARN("B-device got disconnection in %s state,"
                        "switch into Target Stack\n",
                       usbOtgStateName[pOTG->currState], 2, 3, 4, 5, 6);

            pOTG->prevState = pOTG->currState;
            pOTG->currState = USBOTG_STATE_b_peripheral;
            
            /* Switch into Target Stack */

            usbOtgStackSwitch(pOTG, USBOTG_MODE_PERIPHERAL, TRUE);
            }
        }

    /* Unlock the OTG instance */

    USBOTG_UNLOCK_INSTANCE(pOTG);

    /* Notify parameter changed */

    USBOTG_NOTIFY_PARAM_CHANGED(pOTG);
    }

/*******************************************************************************
*
* usbOtgTimerTimeoutAction - timeout action for the USB OTG timer
*
* This routine is called from tExcTask context when the OTG timer times out.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL VOID usbOtgTimerTimeoutAction
    (
    pUSBOTG_MANAGER pOTG
    )
    {
    /* Check if the parameters are valid */

    if (!pOTG || !pOTG->pOCD)
        {
        USBOTG_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);

        return;
        }

    USBOTG_DBG("usbOtgTimerTimeoutAction - OTG timer %s times out!\n",
        usbOtgTimerName[pOTG->otgTimer], 2, 3, 4, 5, 6);

    /* Lock the OTG instance */

    USBOTG_LOCK_INSTANCE(pOTG);

    switch (pOTG->otgTimer)
        {
        case USBOTG_TIMER_a_wait_vrise_tmr:
            pOTG->param.timeout.a_wait_vrise_tmout = TRUE;
            break;
        case USBOTG_TIMER_a_wait_vfall_tmr:
            pOTG->param.timeout.a_wait_vfall_tmout = TRUE;
            break;
        case USBOTG_TIMER_a_wait_bcon_tmr:
            pOTG->param.timeout.a_wait_bcon_tmout = TRUE;
            break;
        case USBOTG_TIMER_a_aidl_bdis_tmr:
            pOTG->param.timeout.a_aidl_bdis_tmout = TRUE;
            break;
        case USBOTG_TIMER_a_bidl_adis_tmr:
            pOTG->param.timeout.a_bidl_adis_tmout = TRUE;
            break;
        case USBOTG_TIMER_b_ase0_brst_tmr:
            pOTG->param.timeout.b_ase0_brst_tmout = TRUE;
            break;
        case USBOTG_TIMER_b_data_pulse_tmr:
            {
            /* Stop data line pulsing! */

            USBOTG_DP_PULL_UP_ENABLE(pOTG, FALSE);

            /* Check if we could charge the VBUS for VBUS pulsing! */

            if (pOTG->pOcdFuncs->pOtgChrgVbusEnable)
                {
                /* Now initiate VBUS line pulsing! */

                USBOTG_CHRG_VBUS_ENABLE(pOTG, TRUE);

                /* And then start VBUS pulsing timer. */

                usbOtgTimerStart(pOTG, USBOTG_TIMER_b_vbus_pulse_tmr);
                }
            else
                {
                /* Now SRP init can be marked as DONE! */

                pOTG->param.internal.b_srp_done = TRUE;
                }
            }
            break;
        case USBOTG_TIMER_b_vbus_pulse_tmr:
            {
            /* Now stop VBUS line pulsing! */

            USBOTG_CHRG_VBUS_ENABLE(pOTG, TRUE);

            /* Now SRP init can be marked as DONE! */

            pOTG->param.internal.b_srp_done = TRUE;
            }
            break;
        case USBOTG_TIMER_b_srp_fail_tmr:
            {
            /*
             * If b_srp_fail_tmr expires and the VBUS is still
             * not in the Session Valid range, then we will notify
             * the user that SRP has failed.
             */

            if (pOTG->param.input.b_sess_vld == FALSE)
                {
                USBOTG_WARN("usbOtgTimerTimeoutAction - "
                            "A-device did not respond to B-device SRP!"
                            "Still has %d times to retry!\n",
                            pOTG->param.extend.b_srp_retry_count,
                            2, 3, 4, 5, 6);
                /*
                 * See comments for b_srp_retry_count on why we
                 * use this logic.
                 */

                pOTG->param.input.b_bus_req = TRUE;

                if (pOTG->param.extend.b_srp_retry_count > 0)
                    {
                    pOTG->param.extend.b_srp_retry_count--;
                    }
                else if (pOTG->param.extend.b_srp_retry_count != WAIT_FOREVER)
                    {
                    pUSBOTG_USR_MSG_HDR pUsrMsgHdr;
                    
                    /* Do not retry SRP any more! */

                    pOTG->param.input.b_bus_req = FALSE;

                    pOTG->param.extend.b_srp_retry_count = 0;

                    USBOTG_WARN("usbOtgTimerTimeoutAction - "
                                "A-device did not respond to B-device SRP!"
                                "Retry TIMEOUT!\n", 1, 2, 3, 4, 5, 6);

                    pUsrMsgHdr = (pUSBOTG_USR_MSG_HDR)usbOtgEventDataGet();
                    
                    if (pUsrMsgHdr)
                        {
                        /* Notify the user of this event */
                    
                        pUsrMsgHdr->uMsgId = USBOTG_USR_MSG_ID_SRP_FAIL;
                        
                        usbOtgUsrMsgNotify(pOTG, (void *)pUsrMsgHdr);
                        }
                    else
                        {
                        USBOTG_WARN("Get msg USBOTG_USR_MSG_ID_SRP_FAIL failed!\n",
                                    1, 2, 3, 4, 5, 6);
                        }
                    }
                }
            }
            break;
        case USBOTG_TIMER_b_srp_ready_tmr:
            {
            pOTG->param.extend.b_srp_ready_tmout = TRUE;
            }
            break;
        case USBOTG_TIMER_a_powerup_tmr:
            {
            pOTG->param.input.power_up = TRUE;
            pOTG->param.input.a_bus_drop = FALSE;
            }
            break;
        case USBOTG_TIMER_unknown_tmr:
        /* Fall through ! */
        default:
            {
            USBOTG_ERR("Invalid timer id %d triggered\n",
                pOTG->otgTimer, 2, 3, 4, 5, 6);
            }
            break;
        }

    /* Reset the timer to unknown ID indicating no active timer */

    pOTG->otgTimer = USBOTG_TIMER_unknown_tmr;

    /* Unlock the OTG instance */

    USBOTG_UNLOCK_INSTANCE(pOTG);

    /* Notify parameter changed */

    USBOTG_NOTIFY_PARAM_CHANGED(pOTG);

    return;
    }

/*******************************************************************************
*
* usbOtgTimerTimeout - timeout entry for the USB OTG timer
*
* This routine is called from ISR context when the OTG timer times out.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL VOID usbOtgTimerTimeout
    (
    pUSBOTG_MANAGER pOTG
    )
    {
    pUSBOTG_OTG_TIMER_TIMEOUT_EVENT pEvent;
    
    /* Check if the parameters are valid */

    if (!pOTG || !pOTG->pOCD)
        {
        USBOTG_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);

        return;
        }

    /*
     * Clear the indication of using watchdog timer.
     * No matter if it was actually used!
     */

    if (vxAtomicSet(&pOTG->wdInUse, 0) == 0)
        {
        USBOTG_VDBG("usbOtgTimerStart - watch dog was not being used\n",
            1, 2, 3, 4, 5, 6);
        }

    /* Get the event buffer which will be released by event handler */
    
    pEvent = usbOtgEventDataGet();

    if (pEvent == NULL)
        {
        USBOTG_ERR("No memory for USBOTG_OTG_TIMER_TIMEOUT_EVENT\n",
            1, 2, 3, 4, 5, 6);
        
        return;
        }

    pEvent->header.id = USBOTG_EVENT_ID_OTG_TIMER_TIMEOUT;
    pEvent->header.pDev = pOTG->pDev;
    pEvent->header.result = ERROR;

    /*
     * Queue the actual event handling to the tExcTask
     * at priority 0; This is because we are going to update
     * the OTG parameters, but we should also watch out for
     * the concurrent updates to the OTG parameters from
     * both the user side and the OTG Framework (thus we
     * should access the parameters under mutex lock).
     */

    usbOtgEventRaise(FALSE, pEvent);  
   
    return;
    }

/*******************************************************************************
*
* usbOtgTimerStop - stop the USB OTG timer
*
* This routine is to stop the active USB OTG timer.
*
* Some USB OTG Controller hardware provides hardware support for these USB
* OTG timers, because some of the timers are in minimum 1ms granularity and
* not all OS timers can always provide such fine granularity. If the OTG
* Controller Driver wants to use such hardware OTG timers, it should implement
* the OtgTimerStart()/OtgTimerStop() interface and return OK when the timer
* is successfully started/stopped; If the OTG Controller Driver doesn't
* implement these interfaces (set "pOtgTimerStart" and "pOtgTimerStop" as NULL),
* or the call fails, then this routine will use an OS specific watchdog timer.
*
* NOTE: No parameter validity is checked since it is used internally and
* the calling routine should have assured the validity.
*
* RETURNS: OK when the OTG timer is stopped successfully, ERROR when failed.
*
* ERRNO: N/A
*/

LOCAL STATUS usbOtgTimerStop
    (
    pUSBOTG_MANAGER pOTG,
    USBOTG_TIMER    timerId
    )
    {
    /* If the timer to be stopped is not the current active timer, return! */

    if (timerId != pOTG->otgTimer)
        {
        USBOTG_VDBG("usbOtgTimerStop - Timer %s is not current timer %s\n",
                   usbOtgTimerName[timerId],
                   usbOtgTimerName[pOTG->otgTimer], 3, 4, 5, 6);

        return ERROR;
        }

    /* Cancel the watchdog if it is in use */

    if (vxAtomicSet(&pOTG->wdInUse, 0) == 1)
        {
        USBOTG_DBG("usbOtgTimerStop - Cancel watchdog timer %s!\n",
            usbOtgTimerName[pOTG->otgTimer], 2, 3, 4, 5, 6);

        if (pOTG->wdId)
            wdCancel(pOTG->wdId);

        /* Reset the timer to unknown ID indicating no active timer */

        pOTG->otgTimer = USBOTG_TIMER_unknown_tmr;

        return OK;
        }

    /* If the OTG Controller Driver provides a hardware timer, use it! */

    if (pOTG->pOcdFuncs->pOtgTimerStop)
        {
        USBOTG_DBG("usbOtgTimerStop - Cancel hardware timer!\n",
            usbOtgTimerName[pOTG->otgTimer], 2, 3, 4, 5, 6);

        pOTG->pOcdFuncs->pOtgTimerStop(pOTG->pOCD, timerId);

        /* Reset the timer to unknown ID indicating no active timer */

        pOTG->otgTimer = USBOTG_TIMER_unknown_tmr;
        }

    return OK;
    }

/*******************************************************************************
*
* usbOtgTimerStart - start the USB OTG timer
*
* This routine is to start the USB OTG timer. It will use the timeout value
* specified by the user configuration (or use the default value if the user
* doesn't specify the timeout value). If the user specifies the timeout value,
* this routine will check and make sure the proper Min and Max limitations
* are met.
*
* Some USB OTG Controller hardware provides hardware support for these USB
* OTG timers, because some of the timers are in minimum 1ms granularity and
* not all OS timers can always provide such fine granularity. If the OTG
* Controller Driver wants to use such hardware OTG timers, it should implement
* the OtgTimerStart()/OtgTimerStop() interface and return OK when the timer
* is successfully started/stopped; If the OTG Controller Driver doesn't
* implement these interfaces (set "pOtgTimerStart" and "pOtgTimerStop" as NULL),
* or the call fails, then this routine will use an OS specific watchdog timer.
*
* NOTE: No parameter validity is checked since it is used internally and
* the calling routine should have assured the validity.
*
* RETURNS: OK when the OTG timer is started successfully, ERROR when failed.
*
* ERRNO: N/A
*/

LOCAL STATUS usbOtgTimerStart
    (
    pUSBOTG_MANAGER pOTG,
    USBOTG_TIMER    timerId
    )
    {
    STATUS sts = ERROR;
    int timeout = 0;

    /*
     * Currently we use default timeout values, we could get user
     * configured values (and make sure they are in valid range!)
     * The default values should be already good enough!
     */

    switch (timerId)
        {
        case USBOTG_TIMER_a_wait_vrise_tmr:
            timeout = USBOTG_DEFAULT_a_wait_vrise_tmout;
            pOTG->param.timeout.a_wait_vrise_tmout = FALSE;
            break;
        case USBOTG_TIMER_a_wait_vfall_tmr:
            timeout = USBOTG_DEFAULT_a_wait_vfall_tmout;
            pOTG->param.timeout.a_wait_vfall_tmout = FALSE;
            break;
        case USBOTG_TIMER_a_wait_bcon_tmr:
            timeout = pOTG->param.extend.a_wait_bcon_time;
            pOTG->param.timeout.a_wait_bcon_tmout = FALSE;
            break;
        case USBOTG_TIMER_a_aidl_bdis_tmr:
            timeout = USBOTG_DEFAULT_a_aidl_bdis_tmout;
            pOTG->param.timeout.a_aidl_bdis_tmout = FALSE;
            break;
        case USBOTG_TIMER_a_bidl_adis_tmr:
            timeout = USBOTG_DEFAULT_a_bidl_adis_tmout;
            pOTG->param.timeout.a_bidl_adis_tmout = FALSE;
            break;
        case USBOTG_TIMER_b_ase0_brst_tmr:
            timeout = USBOTG_DEFAULT_b_ase0_brst_tmout;
            pOTG->param.timeout.b_ase0_brst_tmout = FALSE;
            break;
        case USBOTG_TIMER_b_data_pulse_tmr:
            timeout = USBOTG_DEFAULT_b_data_pulse_tmout;
            break;
        case USBOTG_TIMER_b_vbus_pulse_tmr:
            timeout = USBOTG_DEFAULT_b_vbus_pulse_tmout;
            break;
        case USBOTG_TIMER_b_srp_fail_tmr:
            timeout = USBOTG_DEFAULT_b_srp_fail_tmout;
            break;
        case USBOTG_TIMER_b_srp_ready_tmr:
            timeout = USBOTG_DEFAULT_b_srp_ready_tmout;
            pOTG->param.extend.b_srp_ready_tmout = FALSE;
            break;
        case USBOTG_TIMER_a_powerup_tmr:
            timeout = pOTG->param.extend.a_idle_powerup_time;
            break;
        case USBOTG_TIMER_unknown_tmr:
        /* Fall through ! */
        default:
            {
            USBOTG_ERR("Invalid timer Id %d parameter\n",
                timerId, 2, 3, 4, 5, 6);

            return ERROR;
            }
        }

    USBOTG_VDBG("usbOtgTimerStart - Trying to start OTG timer %s\n",
        usbOtgTimerName[timerId], 2, 3, 4, 5, 6);

    /* Set the current OTG timer ID */

    pOTG->otgTimer = timerId;

    /* If the OTG Controller Driver provides a hardware timer, use it! */

    if (pOTG->pOcdFuncs->pOtgTimerStart)
        {
        USBOTG_VDBG("usbOtgTimerStart - Using hardware timer for OTG timer %s\n",
            usbOtgTimerName[timerId], 2, 3, 4, 5, 6);

        sts = pOTG->pOcdFuncs->pOtgTimerStart(pOTG->pOCD,
                                              timerId,
                                              timeout,
                                              usbOtgTimerTimeout);
        
        if (vxAtomicSet(&pOTG->wdInUse, 0) == 1)
            {
            USBOTG_VDBG("usbOtgTimerStart - watch dog was being used\n",
                1, 2, 3, 4, 5, 6);
            }
        }

    /*
     * If the OTG Controller Driver doesn't provide a hardware timer or
     * failed to start, then we use our watchdog timer instead!
     */

    if (sts != OK)
        {
        USBOTG_VDBG("usbOtgTimerStart - Using watchdog timer for OTG timer %s\n",
            usbOtgTimerName[timerId], 2, 3, 4, 5, 6);

        /* Convert timeout into ticks for unconverted timeouts */
        
        if ((timerId != USBOTG_TIMER_a_powerup_tmr) &&
            (timerId != USBOTG_TIMER_a_wait_bcon_tmr))
            timeout = OS_CONVERT_MILLISECONDS_TO_WAIT_VALUE(timeout);

        /*
         * Indicate we are using watchdog timer.
         * We should set this indication before calling wdStart()
         * because there are chances that once wdStart() is called,
         * the timer times out and the timeout handler can see
         * invalid indication of wdInUse (still FALSE).
         */

        if (vxAtomicSet(&pOTG->wdInUse, 1) == 1)
            {
            USBOTG_VDBG("usbOtgTimerStart - watch dog was being used\n",
                1, 2, 3, 4, 5, 6);
            }

        /* Start the timer now ! */

        sts = wdStart(pOTG->wdId,
                      timeout,
                      (FUNCPTR)usbOtgTimerTimeout,
                      (_Vx_usr_arg_t)pOTG);

        /* If faild to start the watchdog, set the indication to FALSE */

        if (sts != OK)
            {
            if (vxAtomicSet(&pOTG->wdInUse, 0) == 1)
                {
                USBOTG_VDBG("usbOtgTimerStart - watch dog was being used\n",
                    1, 2, 3, 4, 5, 6);
                }
            }
        }

    if (sts != OK)
        {
        /* Set as unknown OTG timer ID */

        pOTG->otgTimer = USBOTG_TIMER_unknown_tmr;

        USBOTG_ERR("usbOtgTimerStart - Failed to start %s\n",
            usbOtgTimerName[timerId], 2, 3, 4, 5, 6);
        }
    else
        USBOTG_DBG("usbOtgTimerStart - OK to start %s\n",
            usbOtgTimerName[timerId], 2, 3, 4, 5, 6);

    return sts;
    }

/*******************************************************************************
*
* usbOtgStackSwitch - switch between USB Host and Target Stack
*
* This routine is to switch between USB Host and Target Stack. It will disable
* the previous stack and enable the new stack as specified by <mode>
*
* 1) When switch from Host Stack into Target Stack
*
* - Host Stack Stops
*   - BusM stops polling
*   - HCD stops host transaction schedule
* - Target Stack Starts
*   - TCD prepares for receiving Setup Packets
*   - Target HAL signals Bus Reset to Target Application
*
* 2) When switch from Target Stack into Host Stack
*
* - Target Stack Stops
*   - Target HAL signals Disconnection to Target Application
*   - tHAL stops event processing (automatically since there is no event)
*   - TCD stops target transaction schedule (if any)
* - Host Stack Starts
*   - HCD starts host transaction schedule
*   - BusM starts polling
*
* NOTE: No parameter validity is checked since it is used internally and
* the calling routine should have assured the validity.
*
* RETURNS: OK when the specified USB Stack is enabled, ERROR if failed.
*
* ERRNO: N/A
*/

LOCAL STATUS usbOtgStackSwitch
    (
    pUSBOTG_MANAGER pOTG,
    USBOTG_MODE     mode,
    BOOL            bForce
    )
    {
    STATUS sts = OK;

    if (((mode == USBOTG_MODE_HOST) && (pOTG->isHcdLoaded == FALSE)) ||
        ((mode == USBOTG_MODE_PERIPHERAL) && (pOTG->isTcdLoaded == FALSE)))
        {
        USBOTG_DBG("usbOtgStackSwitch - %s is not loaded, ignore switching!\n",
            (mode == USBOTG_MODE_HOST) ? "HCD" : "TCD", 2, 3, 4, 5, 6);

        return ERROR;;
        }

    USBOTG_DBG("usbOtgStackSwitch - %s switch to %s!\n",
        bForce ? "Forcing" : "Trying", 
        (mode == USBOTG_MODE_HOST) ? "HCD" : "TCD", 3, 4, 5, 6);

    if ((bForce == TRUE) || (pOTG->stackMode != mode))
        {
        if (mode == USBOTG_MODE_HOST)
            {
            USBOTG_VDBG("usbOtgStackSwitch - usbOtgTgtDisable\n",
                1, 2, 3, 4, 5, 6);
            
            if ((pOTG->isTcdLoaded == FALSE) || (usbOtgTgtDisable(pOTG) == OK))
                {
                USBOTG_VDBG("usbOtgStackSwitch - usbOtgHstEnable\n",
                    1, 2, 3, 4, 5, 6);
                
                sts = usbOtgHstEnable(pOTG);

                if (sts == OK)
                    {
                    pUSBOTG_USR_MSG_HDR pUsrMsgHdr;
                    
                    /* Set it as desired stack */

                    pOTG->stackMode = mode;

                    /* Clear the host request flag */
                    
                    pOTG->param.extend.host_request_flag = FALSE;
                            
                    USBOTG_VDBG("usbOtgStackSwitch - usbOtgHstEnable OK\n",
                          1, 2, 3, 4, 5, 6);
                    
                    pUsrMsgHdr = (pUSBOTG_USR_MSG_HDR)usbOtgEventDataGet();
                    
                    if (pUsrMsgHdr)
                        {
                        /* Notify the user of this event */
                    
                        pUsrMsgHdr->uMsgId = USBOTG_USR_MSG_ID_HOST_ACTIVE;
                        
                        usbOtgUsrMsgNotify(pOTG, (void *)pUsrMsgHdr);
                        }
                    else
                        {
                        USBOTG_WARN("Get msg USBOTG_USR_MSG_ID_HOST_ACTIVE failed!\n",
                                    1, 2, 3, 4, 5, 6);
                        }
                    }
                }
            else
                {
                USBOTG_DBG("usbOtgStackSwitch - usbOtgTgtDisable failed\n",
                    1, 2, 3, 4, 5, 6);
                }
            }
        else /* if (mode == USBOTG_MODE_PERIPHERAL) */
            {
            USBOTG_VDBG("usbOtgStackSwitch - usbOtgHstDisable\n",
                  1, 2, 3, 4, 5, 6);
            
            if ((pOTG->isHcdLoaded == FALSE) || (usbOtgHstDisable(pOTG) == OK))
                {
                USBOTG_VDBG("usbOtgStackSwitch - usbOtgTgtEnable\n",
                      1, 2, 3, 4, 5, 6);
                
                sts = usbOtgTgtEnable(pOTG);

                if (sts == OK)
                    {
                    pUSBOTG_USR_MSG_HDR pUsrMsgHdr;
                    
                    USBOTG_VDBG("usbOtgStackSwitch - usbOtgTgtEnable OK\n",
                          1, 2, 3, 4, 5, 6);
                    
                    /* Set it as desired stack */

                    pOTG->stackMode = mode;
                    
                    /* Clear the host request flag */
                    
                    pOTG->param.extend.host_request_flag = FALSE;
                    
                    pUsrMsgHdr = (pUSBOTG_USR_MSG_HDR)usbOtgEventDataGet();
                    
                    if (pUsrMsgHdr)
                        {
                        /* Notify the user of this event */
                    
                        pUsrMsgHdr->uMsgId = USBOTG_USR_MSG_ID_TARGET_ACTIVE;
                        
                        usbOtgUsrMsgNotify(pOTG, (void *)pUsrMsgHdr);
                        }
                    else
                        {
                        USBOTG_WARN("Get msg USBOTG_USR_MSG_ID_TARGET_ACTIVE failed!\n",
                                    1, 2, 3, 4, 5, 6);
                        }
                    }
                }
            else
                {
                USBOTG_DBG("usbOtgStackSwitch - usbOtgHstDisable failed\n",
                    1, 2, 3, 4, 5, 6);
                }
            }
        }

    if (sts != OK)
        {
        USBOTG_DBG("usbOtgStackSwitch - Failed to switch into %s stack\n",
                    (mode == TRUE) ? "Host" : "Target", 2, 3, 4, 5, 6);
        }

    return sts;
    }

/*******************************************************************************
*
* usbOtgIdVbusProbe - probe ID and VBUS states possibly triggered by hardware
*
* This routine is called by OTG Framework to probe the ID and VBUS states
* which are possibly triggered by hardware, these include parameters
* such as id, a_sess_vld, a_vbus_vld, b_sess_vld, and b_sess_end.
*
* OTG Controller Drivers could implement the GetIdState() and GetVbusState()
* interfaces to provide this functionality. Some OTG Controllers may report
* ID and VBUS state changes through interrupts, for these reports, the OTG
* Controller Driver ISR should report them through the OTG Notifier routines.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*/

LOCAL STATUS usbOtgIdVbusProbe
    (
    pUSBOTG_MANAGER pOTG
    )
    {
    STATUS sts = ERROR;

    /* Check if the parameters are valid */

    if (!pOTG || !pOTG->pOCD)
        {
        USBOTG_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Update ID state first */

    if (pOTG->pOcdFuncs->pOtgIdStateGet)
        {
        USBOTG_ID_STATE idState = USBOTG_ID_LOW;
        UINT32 idReCheck = 3;

        do 
            {
            if (pOTG->pOcdFuncs->pOtgIdStateGet(pOTG->pOCD, &idState) != OK)
                {
                USBOTG_ERR("Failed to get ID state\n", 1, 2, 3, 4, 5, 6);

                return ERROR;
                }
            
            /* Force a fake power up, for OTG Protocol Check List 23 */

            if ((pOTG->idState == USBOTG_ID_HIGH) &&
                (idState == USBOTG_ID_LOW))
                {
                USBOTG_WARN("ID HIGH to LOW in %s state, debounce - %d!\n",
                           usbOtgStateName[pOTG->currState], idReCheck, 3, 4, 5, 6);
                idReCheck--;
                }
            else
                break;

            }while (idReCheck);

        /* Force a fake power up, for OTG Protocol Check List 23 */
        
        if ((pOTG->idState == USBOTG_ID_HIGH) &&
            (idState == USBOTG_ID_LOW))
            {
            USBOTG_WARN("ID HIGH to LOW in %s state, faking power up!\n",
                       usbOtgStateName[pOTG->currState], 2, 3, 4, 5, 6);
        
            pOTG->param.input.power_up = TRUE;
        
            /* Default to make use of the bus */
            
            if (pOTG->param.input.a_bus_drop == FALSE)
                pOTG->param.input.a_bus_req = TRUE;
            }
        
        /* Save the latest ID state */

        pOTG->idState = idState;

        USBOTG_VDBG("Got ID state - %s\n", 
            (idState == USBOTG_ID_LOW) ? 
            "LOW (A-Device)" : "HIGH (B-device)", 2, 3, 4, 5, 6);

        if (idState == USBOTG_ID_LOW)
            pOTG->param.input.id = FALSE;
        else /* if (idState == USBOTG_ID_HIGH) */
            pOTG->param.input.id = TRUE;
        }

    /* Update VBUS state */

    if (pOTG->pOcdFuncs->pOtgVbusStateGet)
        {
        USBOTG_VBUS_STATE vbusState;

        if (pOTG->pOcdFuncs->pOtgVbusStateGet(pOTG->pOCD, &vbusState) != OK)
            {
            USBOTG_ERR("Failed to Get VBUS state\n", 1, 2, 3, 4, 5, 6);

            return ERROR;
            }

        /* Save the latest VBUS state */

        pOTG->vbusState = vbusState;

        USBOTG_VDBG("Got VBUS state - %s\n", 
            usrUsbOtgVbusStateNameGet(vbusState), 2, 3, 4, 5, 6);

        /* ID pin as FALSE means A-device */

        if (pOTG->param.input.id == FALSE)
            {
            if (vbusState == USBOTG_VBUS_STATE_SessEnd)
                {
                pOTG->param.input.a_sess_vld = FALSE;
                pOTG->param.input.a_vbus_vld = FALSE;
                }
            else if (vbusState == USBOTG_VBUS_STATE_SessValid)
                {
                pOTG->param.input.a_sess_vld = TRUE;
                pOTG->param.input.a_vbus_vld = FALSE;
                }
            else if (vbusState == USBOTG_VBUS_STATE_VBusValid)
                {
                pOTG->param.input.a_sess_vld = TRUE;
                pOTG->param.input.a_vbus_vld = TRUE;
                }
            }
        else /* ID pin as TRUE means B-device */
            {
            if (vbusState == USBOTG_VBUS_STATE_SessEnd)
                {
                pOTG->param.input.b_sess_end = TRUE;
                pOTG->param.input.b_sess_vld = FALSE;
                }
            else if (vbusState == USBOTG_VBUS_STATE_SessValid)
                {
                pOTG->param.input.b_sess_end = FALSE;
                pOTG->param.input.b_sess_vld = TRUE;
                }
            else if (vbusState == USBOTG_VBUS_STATE_VBusValid)
                {
                pOTG->param.input.b_sess_end = FALSE;
                pOTG->param.input.b_sess_vld = TRUE;
                }

            /* Reset the features that may be set from the A-device */

            if (pOTG->param.input.b_sess_vld == FALSE)
                {
                USBOTG_VDBG("usbOtgIdVbusProbe - Vbus low - reset b_hnp_en\n",
                            1, 2, 3, 4, 5, 6);
                
                /*
                 * The b_hnp_en is cleared on a bus reset or when
                 * b_sess_vld is FALSE
                 */

                pOTG->param.internal.b_hnp_en = FALSE;

                /*
                 * The a_hnp_support feature is only cleared on a
                 * bus reset or at the end of a session.
                 */

                pOTG->param.extend.b_a_hnp_support = FALSE;

                /*
                 * The a_alt_hnp_support feature is only cleared on a
                 * bus reset or at the end of a session.
                 */

                pOTG->param.extend.b_a_alt_hnp_support = FALSE;
                }
            }
        }

    return sts;
    }

/*******************************************************************************
*
* usbOtgNewStateEnter - enter new state of the OTG State Machine
*
* This routine is to enter new state of the OTG State Machine. Proper actions
* will be taken when the new state is entered.
*
* NOTE: No parameter validity is checked since it is used internally and
* the calling routine should have assured the validity.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL VOID usbOtgNewStateEnter
    (
    pUSBOTG_MANAGER pOTG,
    USBOTG_STATE    newState
    )
    {
    /* If there is no state change, do nothing! */

    if (newState == pOTG->currState)
        {
        USBOTG_VDBG("No state change, stay in %s state\n",
                    usbOtgStateName[pOTG->currState], 2, 3, 4, 5, 6);
        return;
        }

    USBOTG_DBG("usbOtgNewStateEnter - State Change %s ====> %s\n",
                usbOtgStateName[pOTG->currState],
                usbOtgStateName[newState], 3, 4, 5, 6);

    /* Save the previous state */

    pOTG->prevState = pOTG->currState;

    /* Enter new state */

    pOTG->currState = newState;

    /* Perform new state actions */

    switch (newState)
        {
        case USBOTG_STATE_a_idle:
            {
            /*
             * In a_idle state, the chrg_vbus, drv_vbus, loc_conn,
             * and loc_sof output parameters are all FALSE.
             */

            pOTG->param.output.chrg_vbus = FALSE;
            pOTG->param.output.drv_vbus = FALSE;
            pOTG->param.output.loc_conn = FALSE;
            pOTG->param.output.loc_sof = FALSE;

            pOTG->param.input.a_srp_det = FALSE;
            pOTG->param.internal.a_set_b_hnp_en = FALSE;

            /* Actually perform the actions */

            USBOTG_DRV_VBUS_ENABLE(pOTG, FALSE); 

            USBOTG_CHRG_VBUS_ENABLE(pOTG, FALSE);

            USBOTG_DISCHRG_VBUS_ENABLE(pOTG, FALSE);

            USBOTG_DP_PULL_UP_ENABLE(pOTG, FALSE);

            USBOTG_DP_PULL_DOWN_ENABLE(pOTG, TRUE);

            USBOTG_DM_PULL_DOWN_ENABLE(pOTG, TRUE);

            /* Enter Host Stack if not already, do nothing if already */

            usbOtgStackSwitch(pOTG, USBOTG_MODE_HOST, FALSE);

            /*
             * Start a timer to workaround some OTG controllers which 
             * can not detect SRP signal. If this timer times out, we 
             * can powerup the VBUS and finally it would go off if there
             * is no external devices need power.
             */
             
            usbOtgTimerStart(pOTG, USBOTG_TIMER_a_powerup_tmr);
            }
            break;
        case USBOTG_STATE_a_wait_vrise:
            {
            /*
             * In a_wait_vrise state, drv_vbus is TRUE and other
             * output parameters (chrg_vbus, loc_conn and loc_sof)
             * are all FALSE.
             */

            pOTG->param.output.drv_vbus = TRUE;
            pOTG->param.output.chrg_vbus = FALSE;
            pOTG->param.output.loc_conn = FALSE;
            pOTG->param.output.loc_sof = FALSE;

            /* Actually perform the actions */

            USBOTG_DRV_VBUS_ENABLE(pOTG, TRUE);

            /*
             * In this state, the A-device waits for the voltage on
             * VBUS to rise above the A-Device VBUS Valid threshold
             * (a_vbus_vld = TRUE). Upon entering this state, the
             * A-device starts a timer: a_wait_vrise_tmr.
             */

            usbOtgTimerStart(pOTG, USBOTG_TIMER_a_wait_vrise_tmr);

            }
            break;
        case USBOTG_STATE_a_wait_bcon:
            {
            /*
             * In a_wait_vrise state, drv_vbus is TRUE and other
             * output parameters (chrg_vbus, loc_conn and loc_sof)
             * are all FALSE.
             *
             * Note that it is possible that a_wait_bcon is entered
             * from a_peripheral, in this path, loc_conn is changed
             * from TRUE to FALSE.
             */

            pOTG->param.output.drv_vbus = TRUE;
            pOTG->param.output.chrg_vbus = FALSE;
            pOTG->param.output.loc_conn = FALSE;
            pOTG->param.output.loc_sof = FALSE;

            /*
             * The A-device clears a_set_b_hnp_en, upon entry into
             * the a_wait_bcon state or when the A-device asserts
             * a bus reset.
             */

            pOTG->param.internal.a_set_b_hnp_en = FALSE;

            /* Set the controller to work as host */

            USBOTG_SET_CTLR_MODE(pOTG, USBOTG_MODE_HOST);

            /* Enter Host Stack if not already, do nothing if already */

            usbOtgStackSwitch(pOTG, USBOTG_MODE_HOST, FALSE);

            /* Actually perform the actions */

            USBOTG_DRV_VBUS_ENABLE(pOTG, TRUE); 

            /*
             * In this state, the A-device waits for the B-device to
             * signal a connection. Upon entering this state, the
             * A-device starts a timer: a_wait_bcon_tmr.
             */

            usbOtgTimerStart(pOTG, USBOTG_TIMER_a_wait_bcon_tmr);
            }
            break;
        case USBOTG_STATE_a_host:
            {
            /*
             * In a_host state, drv_vbus and loc_sof are TRUE, other
             * output parameters (chrg_vbus, loc_conn) are all FALSE.
             */

            pOTG->param.output.drv_vbus = TRUE;
            pOTG->param.output.loc_sof = TRUE;
            pOTG->param.output.chrg_vbus = FALSE;
            pOTG->param.output.loc_conn = FALSE;

            /* Actually perform the actions */

            USBOTG_DRV_VBUS_ENABLE(pOTG, TRUE); 

            /* Disable hardware automatic HNP request (A-Suspend to B-Disconnect) */
            
            USBOTG_AUTO_SUSP2DIS_ENABLE(pOTG, FALSE);

            /* Enter Host Stack if not already, do nothing if already */

            usbOtgStackSwitch(pOTG, USBOTG_MODE_HOST, FALSE);

            /*
             * Note that the loc_sof action could be implemented
             * by the Bus Reset (from a_wait_bcon state) or Bus
             * Resume (from a_suspend). These actions could be
             * performed by the Host Controller Driver when leaving
             * these state.
             *
             * The "local SOF" (loc_sof) bit is TRUE when the local
             * device is generating activity on the bus. Activity
             * begins with a bus reset followed by start of frame
             * packets (SOF's) or low-speed keep-alives or any
             * other bus activity that occurs with enough frequency
             * to prevent the peripheral device from sensing a lack
             * of bus activity.
             */

            if (pOTG->prevState == USBOTG_STATE_a_wait_bcon)
                {
                /*
                 * Upon entering this state, the A-device resets
                 * the bus to prepare the B-device for packet traffic.
                 */

                /*
                 * Drive Bus Reset is done by the Host Stack or
                 * if the hardware has AUTO_CON2RST feature and
                 * is endalbed, Bus Reset is done automatically.
                 */

                USBOTG_DBG("B-device connected!\n",
                           1, 2, 3, 4, 5, 6);
                }
            else if (pOTG->prevState == USBOTG_STATE_a_suspend)
                {
                if (pOTG->param.input.a_bus_req == TRUE)
                    {
                    /*
                     * Drive Bus Resume since this is an user
                     * requested resume.
                     */

                    if (pOTG->hOtgDevice)
                        {
                        USBOTG_DBG("usbHstSelectiveResume B-device!\n",
                                               1, 2, 3, 4, 5, 6);

                        usbHstSelectiveResume(pOTG->hOtgDevice);
                        }
                    }

                if (pOTG->param.input.b_bus_resume == TRUE)
                    {
                    /*
                     * B-device signals resume while A-device is in
                     * suspend, the OTG Controller Driver ISR should
                     * have responded to this resume!
                     */

                    USBOTG_DBG("B-device signals remote wakeup!\n",
                               1, 2, 3, 4, 5, 6);
                    }
                }

            /*
             * If the hardware has support for automatic B-Disconnect
             * to A-Connect handling, we can disable the feature since
             * we are now in a_host state! This feature can be enabled
             * when we enter suspend state.
             */

            USBOTG_AUTO_CON2RST_ENABLE(pOTG, FALSE);
            }
            break;
        case USBOTG_STATE_a_suspend:
            {
            /*
             * In a_suspend state, drv_vbus is TRUE, other output
             * parameters (chrg_vbus, loc_conn and loc_sof) are all
             * FALSE.
             *
             * Note that there is only one way to enter the a_suspend
             * state, that is from a_host state! And this transition
             * cause the loc_sof changes from TRUE to FALSE!
             */

            pOTG->param.output.drv_vbus = TRUE;
            pOTG->param.output.chrg_vbus = FALSE;
            pOTG->param.output.loc_conn = FALSE;
            pOTG->param.output.loc_sof = FALSE;

            /* Actually perform the actions */

            USBOTG_DRV_VBUS_ENABLE(pOTG, TRUE); 
            
            /*
             * If the hardware has support for automatic B-Disconnect
             * to A-Connect handling, it is time to enable the feature!
             */

            USBOTG_AUTO_CON2RST_ENABLE(pOTG, TRUE);

            /*
             * Drive Bus Suspend
             *
             * We call the USB Host Stack API usbHstSuspendDevice()
             * to actually suspend the bus (actually in OTG this is
             * the only port).
             *
             * This API is going to be created, which may
             * need to take into considerations for Host
             * class drivers, at suspend state, class drivers
             * should stop sending commands to the device,
             * but still should NOT consider the device as
             * failed, because the communication is just
             * blocked. This behavior has to be reviewed.
             */

            if (pOTG->hOtgDevice)
                {
                USBOTG_DBG("usbHstSelectiveSuspend B-device 0x%X in %s state\n",
                            pOTG->hOtgDevice,
                            usbOtgStateName[pOTG->currState], 3, 4, 5, 6);

                usbHstSelectiveSuspend(pOTG->hOtgDevice);
                }

            /*
             * Upon entering the a_suspend state, the A-device starts
             * the a_aidl_bdis_tmr. This timer can be set to an
             * arbitrarily long time, but must be longer than
             * TA_AIDL_BDIS min.
             *
             * Note that e only start the timer if the a_set_b_hnp_en
             * is TRUE, this is to allow the A-device user or B-device
             * to resume the A-device into a_host state.
             */

            if (pOTG->param.internal.a_set_b_hnp_en == TRUE)
                usbOtgTimerStart(pOTG, USBOTG_TIMER_a_aidl_bdis_tmr);
            }
            break;
        case USBOTG_STATE_a_peripheral:
            {
            /*
             * In a_peripheral state, drv_vbus and loc_conn are TRUE,
             * other output parameters (chrg_vbus and loc_sof) are all
             * FALSE.
             *
             * Note that there is only one way to enter the a_peripheral
             * state, that is from a_suspend state! And this transition
             * cause the loc_conn changes from FALSE to TRUE! This is the
             * only state in which the A-device will signal a connection
             * to the B-device.
             */

            pOTG->param.output.drv_vbus = TRUE;
            pOTG->param.output.loc_conn = TRUE;
            pOTG->param.output.chrg_vbus = FALSE;
            pOTG->param.output.loc_sof = FALSE;

            /* On entering a_peripheral we are not suspended */

            pOTG->param.input.b_bus_suspend = FALSE;

            /* Set the controller to work as peripheral */

            USBOTG_SET_CTLR_MODE(pOTG, USBOTG_MODE_PERIPHERAL);

            /* Actually perform the actions */

            USBOTG_DRV_VBUS_ENABLE(pOTG, TRUE); 

            /*
             * If there is no hardware assistance for automatic
             * B-Disconnect to A-Connect handling, then we should
             * assert the A-device D+ pull-up to signal connection
             * to B-device. The OTG Contrller Driver ISR handler
             * may have already signaled this A-device connection
             * to the B-device directly in the ISR (to meet 3 ms
             * (TA_BDIS_ACON max) time limitation); However, it is
             * no harm to assert the D+ pull-up again here (if it
             * hasn't already been done, it will be done here!)
             */

            if (pOTG->pOcdFuncs->pOtgAutoDis2ConEnable == NULL)
                {
                USBOTG_DP_PULL_UP_ENABLE(pOTG, TRUE);
                }

            USBOTG_DP_PULL_DOWN_ENABLE(pOTG, FALSE);

            USBOTG_DM_PULL_DOWN_ENABLE(pOTG, TRUE);

            /*
             * Once entered peripheral state, clear the host request flag;
             * If the user application wants to use host role, then it need
             * to reqest host role explictly.
             */
             
            pOTG->param.extend.host_request_flag = FALSE;
            }
            break;
        case USBOTG_STATE_a_wait_vfall:
            {
            /*
             * In a_wait_vfall state, all output parameters (drv_vbus,
             * loc_conn, chrg_vbus and loc_sof) are all FALSE.
             *
             * Note that it is possible to enter a_wait_vfall from the
             * a_peripheral state, and this transition causes drv_vbus
             * and loc_conn change from TRUE to FALSE!
             */

            pOTG->param.output.chrg_vbus = FALSE;
            pOTG->param.output.drv_vbus = FALSE;
            pOTG->param.output.loc_conn = FALSE;
            pOTG->param.output.loc_sof = FALSE;

            /* Actually perform the actions */

            USBOTG_DRV_VBUS_ENABLE(pOTG, FALSE);

            /*
             * We may come here from a_peripheral state or other states.
             * No matter where we come from, we can't pull-up anymore!
             */

            USBOTG_DP_PULL_UP_ENABLE(pOTG, FALSE);

            /*
             * Upon entering the a_wait_vfall state, the A-device starts
             * the a_wait_vfall_tmr. This timer is used by an A-device
             * in the a_wait_vfall state while waiting for the voltage on
             * VBUS to fall below the VBUS leakage voltage VOTG_VBUS_LKG.
             * When this timer expires the A-device transitions to a_idle.
             */

            usbOtgTimerStart(pOTG, USBOTG_TIMER_a_wait_vfall_tmr);
            }
            break;
        case USBOTG_STATE_a_vbus_err:
            {
            /*
             * In a_vbus_err state, all output parameters (drv_vbus,
             * loc_conn, chrg_vbus and loc_sof) are all FALSE.
             *
             * Note that it is possible to enter a_vbus_err from the
             * a_peripheral state, and this transition causes drv_vbus
             * and loc_conn change from TRUE to FALSE!
             */

            pOTG->param.output.chrg_vbus = FALSE;
            pOTG->param.output.drv_vbus = FALSE;
            pOTG->param.output.loc_conn = FALSE;
            pOTG->param.output.loc_sof = FALSE;

            pOTG->param.internal.a_clr_err = TRUE;

            /* Actually perform the actions */

            USBOTG_DRV_VBUS_ENABLE(pOTG, FALSE);

            /*
             * We may come here from a_peripheral state or other states.
             * No matter where we come from, we can't pull-up anymore!
             */

            USBOTG_DP_PULL_UP_ENABLE(pOTG, FALSE);
            }
            break;
        case USBOTG_STATE_b_idle:
            {
            /*
             * In b_idle state, all output parameters (drv_vbus,
             * loc_conn, chrg_vbus and loc_sof) are all FALSE.
             */

            pOTG->param.output.chrg_vbus = FALSE;
            pOTG->param.output.drv_vbus = FALSE;
            pOTG->param.output.loc_conn = FALSE;
            pOTG->param.output.loc_sof = FALSE;

            /* Actually perform the actions */

            /* USBOTG_DRV_VBUS_ENABLE(pOTG, FALSE); */

            USBOTG_CHRG_VBUS_ENABLE(pOTG, FALSE);

            USBOTG_DP_PULL_UP_ENABLE(pOTG, FALSE);

            USBOTG_DP_PULL_DOWN_ENABLE(pOTG, FALSE);

            USBOTG_DM_PULL_DOWN_ENABLE(pOTG, TRUE);

            /* Enter Target Stack if not already, do nothing if already */

            usbOtgStackSwitch(pOTG, USBOTG_MODE_PERIPHERAL, FALSE);

            /* 
             * If the b_idle state is entered from b_srp_init
             * and the SRP init is done, we should start the
             * timer to wait for the A-device to raise the VBUS.
             */

            if ((pOTG->prevState == USBOTG_STATE_b_srp_init) &&
                (pOTG->param.internal.b_srp_done == TRUE))
                {
                /*
                 * Note that even SRP init is done, the A-device may
                 * still need some time to raise the VBUS, and that
                 * is why we need to start the b_srp_fail_tmr here.
                 */

                usbOtgTimerStart(pOTG, USBOTG_TIMER_b_srp_fail_tmr);
                }
            }
            break;
        case USBOTG_STATE_b_srp_init:
            {
            /*
             * In b_srp_init state, all output parameters (drv_vbus,
             * loc_conn, chrg_vbus and loc_sof) are initially all FALSE.
             * Then when data line pulsing SRP is initiated, the loc_conn
             * should go TRUE for TB_DATA_PLS (Min 5ms, Max 10ms) of time;
             * And if when VBUS pulsing is enabled, then the drv_vbus
             * should go TRUE for TB_VBUS_PLS (Min 0ms, Max 30ms) of time.
             *
             * However, these intermediate assert of loc_conn and chrg_vbus
             * are only in the b_srp_init state itself. There is no need
             * to change them during this state, because these will not
             * affect the OTG State Machine. Thus we keep all of them FALSE.
             *
             * Note that there is another output parameter data_pulse added
             * in OTG 2.0, and that also doesn't need to be set and cleared
             * during this state.
             */

            pOTG->param.output.chrg_vbus = FALSE;
            pOTG->param.output.drv_vbus = FALSE;
            pOTG->param.output.loc_conn = FALSE;
            pOTG->param.output.loc_sof = FALSE;

            /* Initialy set the b_srp_done as FALSE */

            pOTG->param.internal.b_srp_done = FALSE;

            /* Actually perform the actions */

            /*
             * If the OTG Controller Driver provides an interface to
             * initiate SRP, then we will depend on that to action.
             * Otherwise, we have to form the pulsing by ourselves
             * playing with the D+ and VBUS line.
             */

            if (pOTG->pOcdFuncs->pOtgRequestSrp)
                {
                pOTG->pOcdFuncs->pOtgRequestSrp(pOTG->pOCD,
                    USBOTG_SRP_TYPE_DATA_PULSE | USBOTG_SRP_TYPE_VBUS_PULSE);

                /* Now SRP init can be marked as DONE! */

                pOTG->param.internal.b_srp_done = TRUE;
                }
            else
                {
                pUSBOTG_USR_MSG_HDR pUsrMsgHdr;
                
                pUsrMsgHdr = (pUSBOTG_USR_MSG_HDR)usbOtgEventDataGet();
                
                if (pUsrMsgHdr)
                    {
                    /* Notify the user of this event */
                
                    pUsrMsgHdr->uMsgId = USBOTG_USR_MSG_ID_SRP_ACTIVE;
                    
                    usbOtgUsrMsgNotify(pOTG, (void *)pUsrMsgHdr);
                    }
                else
                    {
                    USBOTG_WARN("Get msg USBOTG_USR_MSG_ID_SRP_ACTIVE failed!\n",
                                1, 2, 3, 4, 5, 6);
                    }
                /*
                 * Make sure we are in a state that can initiate
                 * pulsing, although when we enter this state, the
                 * initial conditions should have been met, there
                 * should be no harm trying to make sure the case.
                 */

                USBOTG_DRV_VBUS_ENABLE(pOTG, FALSE);

                USBOTG_CHRG_VBUS_ENABLE(pOTG, FALSE);

                USBOTG_DP_PULL_UP_ENABLE(pOTG, FALSE);

                USBOTG_DP_PULL_DOWN_ENABLE(pOTG, FALSE);

                USBOTG_DM_PULL_DOWN_ENABLE(pOTG, TRUE);

                /*
                 * We may simply use delay based timing, but this
                 * has the possibility to be preempted by other
                 * higher priority tasks, which causes unpredictable
                 * pulse lengths; However both data line pulsing
                 * and VBUS pulsing are with Max length requirement,
                 * so that the D+ and VBUS are not over driven.
                 * So using delayed based timing is not always good!
                 *
                 * Instead, we could use timer based solution, that
                 * the duration will be timed in interrupt context,
                 * (maybe in system clock interrupt context), and that
                 * is more predictable. There is only one problem with
                 * the data line pulsing, where it is in the range of
                 * 5ms to 10ms; But normally in VxWorks the watchdog
                 * is in system tick basis, in a typical sysClkRate of
                 * 60, one tick may span about 17ms, the accuracy is
                 * still not fine enough for the 5~10ms data line pulse.
                 * If it must use the watchdog timer to implement more
                 * accturate pulsing, it might be desirable the sysClkRate
                 * would better be changed to 1000 (or higher if possible),
                 * making at least 1ms accuracy.
                 *
                 * Of course, if there is hardware timer for this purpose,
                 * then there should be no problem of any.
                 *
                 * NOTE: We could make the delay version as an extended
                 * parameter to control (let the user to decide the choice
                 * at run time!)
                 */

                #ifdef USBOTG_USE_DELAY_FOR_SRP

                /* Initiate data line pulsing first! */

                USBOTG_DP_PULL_UP_ENABLE(pOTG, TRUE);

                OS_DELAY_MS(USBOTG_DEFAULT_b_data_pulse_tmout);

                USBOTG_DP_PULL_UP_ENABLE(pOTG, FALSE);

                if (pOTG->pOcdFuncs->pOtgChrgVbusEnable)
                    {
                    /* Now initiate VBUS line pulsing! */

                    USBOTG_CHRG_VBUS_ENABLE(pOTG, TRUE);

                    OS_DELAY_MS(USBOTG_DEFAULT_b_vbus_pulse_tmout);

                    USBOTG_CHRG_VBUS_ENABLE(pOTG, FALSE);
                    }

                /* Now SRP init can be marked as DONE! */

                pOTG->param.internal.b_srp_done = TRUE;

                #else /* USBOTG_USE_DELAY_FOR_SRP */

                /* Initiate data line pulsing first! */

                USBOTG_DP_PULL_UP_ENABLE(pOTG, TRUE);

                /*
                 * Starts the data line pulse timer, when this timer
                 * expires, the timeout handler will stop driving D+,
                 * and starts VBUS pulsing, then b_vbus_pulse_tmr is
                 * started; When b_vbus_pulse_tmr expires, VBUS pulsing
                 * will be stopped and b_srp_done is set to TRUE.
                 */

                usbOtgTimerStart(pOTG, USBOTG_TIMER_b_data_pulse_tmr);

                #endif
                }
            }
            break;
        case USBOTG_STATE_b_peripheral:
            {
            /*
             * In b_peripheral state, loc_conn is TRUE, all other output
             * parameters (drv_vbus, chrg_vbus and loc_sof) are all FALSE.
             */

            pOTG->param.output.loc_conn = TRUE;

            pOTG->param.output.chrg_vbus = FALSE;
            pOTG->param.output.drv_vbus = FALSE;
            pOTG->param.output.loc_sof = FALSE;
            
            /* 
             * Reset the a_bus_suspend/a_bus_resume to avoid toggling 
             * between b_wait_acon and b_peripheral in some cases.
             */
                
            pOTG->param.input.a_bus_suspend = FALSE;
            pOTG->param.input.a_bus_resume = FALSE;
            
            /*
             * If the b_peripheral is entered from b_host state
             * it might be the user application on B-device wants
             * to return host role back to A-device, if so we need
             * to suspend the bus so that the A-device can know
             * this situation and then disconnect from us (B-device).
             */

            if ((pOTG->prevState == USBOTG_STATE_b_host) &&
                (pOTG->hOtgDevice))
                {
                USBOTG_DBG("HNP - usbHstSelectiveSuspend Device 0x%X in %s state\n",
                            pOTG->hOtgDevice,
                            usbOtgStateName[pOTG->currState], 3, 4, 5, 6);

                usbHstSelectiveSuspend(pOTG->hOtgDevice);
                }

            /*
             * Once entered peripheral state, clear the host request flag;
             * If the user application wants to use host role, then it need
             * to reqest host role explictly.
             */
             
            pOTG->param.extend.host_request_flag = FALSE;

            /* Set the controller to work as peripheral */

            USBOTG_SET_CTLR_MODE(pOTG, USBOTG_MODE_PERIPHERAL);

            /* Enter Target Stack if not already, do onthing if already */

            usbOtgStackSwitch(pOTG, USBOTG_MODE_PERIPHERAL, FALSE);

            /*
             * When a B-device enters this state it shall enable its
             * pull-up on D+. After the B-device enables its pull-up,
             * it shall monitor the state of the bus to determine if
             * a bus reset is being signaled by the A-device. If the
             * B-device is capable of HS, it shall begin the high-speed
             * detection handshake any time that a bus reset condition
             * exists.
             */

            USBOTG_DP_PULL_UP_ENABLE(pOTG, TRUE);

            USBOTG_DP_PULL_DOWN_ENABLE(pOTG, FALSE);

            USBOTG_DM_PULL_DOWN_ENABLE(pOTG, TRUE);
            }
            break;
        case USBOTG_STATE_b_wait_acon:
            {
            /*
             * In b_wait_acon state, all output parameters (loc_conn,
             * drv_vbus, chrg_vbus and loc_sof) are all FALSE.
             */

            pOTG->param.output.loc_conn = FALSE;
            pOTG->param.output.chrg_vbus = FALSE;
            pOTG->param.output.drv_vbus = FALSE;
            pOTG->param.output.loc_sof = FALSE;

            /*
             * In this state, the OTG B-device has received a SetFeature
             * (b_hnp_enable) giving it permission to assume the role
             * of host and it has detected that the bus has gone to the
             * Suspend state. Upon entering this state, the OTG B-device
             * turns off its pull-up resistor on D+, starts a timer
             * (b_ase0_brst_tmr), and waits for the A-device to signal
             * a connect.
             */

            USBOTG_SET_CTLR_MODE(pOTG, USBOTG_MODE_HOST);

            /*
             * A host will have its D+ pull-up deasserted and
             * both D+/D- pull-down asserted.
             */

            USBOTG_DP_PULL_UP_ENABLE(pOTG, FALSE);

            USBOTG_DP_PULL_DOWN_ENABLE(pOTG, TRUE);

            USBOTG_DM_PULL_DOWN_ENABLE(pOTG, TRUE);

            usbOtgTimerStart(pOTG, USBOTG_TIMER_b_ase0_brst_tmr);
            }
            break;
        case USBOTG_STATE_b_host:
            {

            /*
             * In b_host state, loc_sof becomes TRUE, all other output
             * parameters (loc_conn, drv_vbus, chrg_vbus) are all FALSE.
             */

            pOTG->param.output.loc_conn = FALSE;
            pOTG->param.output.chrg_vbus = FALSE;
            pOTG->param.output.drv_vbus = FALSE;
            pOTG->param.output.loc_sof = TRUE;

            /* Once enter the b_host state, clear the a_bus_suspend flag */
            
            pOTG->param.input.a_bus_suspend = FALSE;
            pOTG->param.internal.b_hnp_en = FALSE;

            /* Keep as Host Stack */

            usbOtgStackSwitch(pOTG, USBOTG_MODE_HOST, FALSE);

            /*
             * Upon entering this state, the B-device resets
             * the bus to prepare the A-device for packet traffic.
             * And this is why the loc_sof becomes TRUE.
             */

            /* Drive Bus Reset is done in the Host Stack */
            }
            break;
        case USBOTG_STATE_unknown:
        /* Fall through */
        default:
            break;
        }

    return;
    }

/*******************************************************************************
*
* usbOtgStateSingleStep - single step the OTG State Machine
*
* This routine is to single step the OTG State Machine. The OTG State Machine
* is advanced according to the current OTG state, input parameters and other
* state variables.
*
* NOTE: No parameter validity is checked since it is used internally and
* the calling routine should have assured the validity.
*
* RETURNS: USBOTG_STATE value of the next state (may be same as current state)
*
* ERRNO: N/A
*/

LOCAL USBOTG_STATE usbOtgStateSingleStep
    (
    pUSBOTG_MANAGER pOTG
    )
    {
    /*
     * The new state is initially set to the current state,
     * based on the input parameter values, it may be updated
     * to the next state according to the State Diagram.
     */

    USBOTG_STATE newState = pOTG->currState;

    USBOTG_DBG("usbOtgStateSingleStep - Current state %s\n",
                usbOtgStateName[pOTG->currState], 2, 3, 4, 5, 6);

    /*
     * See OTG 1.3:
     *
     * - Figure 6-2 On-The-Go A-device State Diagram
     * - Figure 6-3 On-The-Go B-device State Diagram
     *
     * See also OTG 2.0:
     *
     * - Figure 7-1: A-device State Diagram
     * - Figure 7-2: Embedded Host state diagram
     * - Figure 7-3: OTG B-device State Diagram
     * - Figure 7-4: SRP-Capable Peripheral-Only B-device State Diagram
     */

    switch (pOTG->currState)
        {
        case USBOTG_STATE_a_idle:
            {
            /*
             * If the OTG Controller provides polling interface
             * for SRP detection report, call it to update a_srp_det!
             */

            if ((pOTG->param.input.id == FALSE) &&
                (pOTG->pOcdFuncs->pOtgSrpDetStateGet != NULL))
                {
                pOTG->param.input.a_srp_det =
                    pOTG->pOcdFuncs->pOtgSrpDetStateGet(pOTG->pOCD);
                }

            /*
             * If we come here with host_request_flag as TRUE, meaning
             * the user requested host role ASAP, we force a_bus_drop
             * to FALSE so we could enter a_wait_vrise
             */

            if ((pOTG->param.input.id == FALSE) &&
                (pOTG->param.extend.host_request_flag == TRUE))
                {
                pOTG->param.input.a_bus_drop = FALSE;

                USBOTG_VDBG("User needs host role ASAP, set a_bus_drop FALSE\n",
                            1, 2, 3, 4, 5, 6);
                }

            if (pOTG->param.input.id == TRUE)
                {
                /* Switch to b_idle only when we have TCD working for us */

                if (pOTG->isTcdLoaded == TRUE)
                    {
                    /*
                     * A change in id from FALSE to TRUE causes a transition
                     * to the b_idle state.
                     *
                     * While in a_idle the id is low (Mini-A plug is inserted),
                     * if the Mini-A plug is removed, the id gets high, and
                     * we should enter b_idle state at this id change.
                     */

                    newState = USBOTG_STATE_b_idle;
                    }
                else
                    {
                    USBOTG_VDBG("Ignored switching to USBOTG_STATE_b_idle\n",
                                1, 2, 3, 4, 5, 6);
                    }
                }
            else if ((pOTG->param.input.a_bus_drop == FALSE) &&
                     ((pOTG->param.input.a_bus_req == TRUE) ||
                      (pOTG->param.input.a_srp_det == TRUE) ||
                      (pOTG->param.input.adp_change == TRUE) ||
                      (pOTG->param.input.power_up == TRUE)))
                {
                /*
                 * The A-device transitions to the a_wait_vrise state:
                 *
                 * - if the A-device application is not wanting to drop
                 *   the bus (a_bus_drop = FALSE), and either of the
                 *   following are TRUE:
                 *
                 *   - the A-device Application is requesting the bus
                 *     (a_bus_req = TRUE), or
                 *   - SRP is detected on the bus (a_srp_det = TRUE).
                 *   - an ADP change has been detected (adp_change = TRUE) or
                 *   - the A-device has only just powered up its USB system
                 *     (power_up = TRUE).
                 */

                newState = USBOTG_STATE_a_wait_vrise;

                /* Reset these one shot input parameters */

                pOTG->param.input.a_srp_det = FALSE;
                pOTG->param.input.adp_change = FALSE;
                pOTG->param.input.power_up = FALSE;
                }
            
            if ((newState == USBOTG_STATE_a_idle) && 
                (pOTG->param.input.a_bus_drop == TRUE) &&
                (pOTG->param.input.a_sess_vld == TRUE))
                {
                USBOTG_DRV_VBUS_ENABLE(pOTG, FALSE); 
                }
            }
            break;
        case USBOTG_STATE_a_wait_vrise:
            {
            #if (USBOTG_VERSION_SUPPORTED < USBOTG_VERSION0200)

            /*
             * In OTG 1.3 and earlier, there is only one way to move
             * out of a_wait_vrise state, that is to enter a_wait_bcon
             * state when the following conditions are met. Logically,
             * only a_vbus_vld = TRUE should cause transiton to the
             * a_wait_bcon state, other input changes (id, a_bus_drop,
             * and a_wait_vrise_tmout should transiton to a_wait_vfall);
             * In OTG 1.3, there has to be an intermediate (a_wait_bcon)
             * for these input changes. This is fixed in OTG 2.0.
             */

            if ((pOTG->param.input.id == TRUE) ||
                (pOTG->param.input.a_bus_drop == TRUE) ||
                (pOTG->param.input.a_vbus_vld == TRUE) ||
                (pOTG->param.timeout.a_wait_vrise_tmout == TRUE))
                {
                /*
                 * Only one way out!
                 *
                 * The A-device transitions to the a_wait_bcon state:
                 * - if the voltage on VBUS has risen above the A-device
                 *   VBUS Valid threshold (a_vbus_vld = TRUE), or
                 * - if the a_wait_vrise_tmr expires.
                 */

                newState = USBOTG_STATE_a_wait_bcon;

                if (pOTG->param.input.a_vbus_vld != TRUE)
                    {
                    pUSBOTG_USR_MSG_HDR pUsrMsgHdr;
                    
                    pUsrMsgHdr = (pUSBOTG_USR_MSG_HDR)usbOtgEventDataGet();
                    
                    if (pUsrMsgHdr)
                        {
                        /* Notify the user of this event */
                    
                        pUsrMsgHdr->uMsgId = USBOTG_USR_MSG_ID_VRISE_FAIL;
                        
                        usbOtgUsrMsgNotify(pOTG, (void *)pUsrMsgHdr);
                        }
                    else
                        {
                        USBOTG_WARN("Get msg USBOTG_USR_MSG_ID_VRISE_FAIL failed!\n",
                                    1, 2, 3, 4, 5, 6);
                        }
                    }
                }
            #else /* if (USBOTG_VERSION_SUPPORTED >= USBOTG_VERSION0200) */

            if (pOTG->param.input.a_vbus_vld == TRUE)
                {
                /*
                 * The A-device transitions to the a_wait_bcon state:
                 *
                 * - if the voltage on VBUS is in regulation
                 *   (a_vbus_vld = TRUE)
                 */

                newState = USBOTG_STATE_a_wait_bcon;
                }
            else if ((pOTG->param.input.id == TRUE) ||
                     (pOTG->param.input.a_bus_drop == TRUE) ||
                     (pOTG->param.timeout.a_wait_vrise_tmout == TRUE))
                 {
                 pUSBOTG_USR_MSG_HDR pUsrMsgHdr;
                 
                 /*
                  * The A-device transitions to the a_wait_vfall state:
                  *
                  * - if the Micro-A plug is detached (id = TRUE, applies
                  *   to OTG A-devices only), or
                  * - if the a_wait_vrise_tmr times out (a_wait_vrise_tmout
                  *   = TRUE) or
                  * - if the A-device application wants to drop the bus
                  *   (a_bus_drop = TRUE).
                  */

                 newState = USBOTG_STATE_a_wait_vfall;
                 
                 pUsrMsgHdr = (pUSBOTG_USR_MSG_HDR)usbOtgEventDataGet();
                 
                 if (pUsrMsgHdr)
                     {
                     /* Notify the user of this event */
                 
                     pUsrMsgHdr->uMsgId = USBOTG_USR_MSG_ID_VRISE_FAIL;
                     
                     usbOtgUsrMsgNotify(pOTG, (void *)pUsrMsgHdr);
                     }
                 else
                     {
                     USBOTG_WARN("Get msg USBOTG_USR_MSG_ID_VRISE_FAIL failed!\n",
                                 1, 2, 3, 4, 5, 6);
                     }
                 }
            #endif /* if (USBOTG_VERSION_SUPPORTED ...) */

            /*
             * USB OTG Timers should be stopped on exiting from
             * their associated states
             */

            if (newState != USBOTG_STATE_a_wait_vrise)
                {
                /* Stop the a_wait_vrise_tmr as we are out of a_wait_vrise */

                usbOtgTimerStop(pOTG, USBOTG_TIMER_a_wait_vrise_tmr);

                /* Reset the a_wait_vrise_tmout as it is a one shot timer */

                pOTG->param.timeout.a_wait_vrise_tmout = FALSE;
                }
            else
                {
                /* 
                 * If we keep in a_wait_vrise state, better to drive the VBUS
                 * so that it could raise up!
                 */
                USBOTG_DRV_VBUS_ENABLE(pOTG, TRUE);
                }
            }
            break;
        case USBOTG_STATE_a_wait_bcon:
            {
            /*
             * While in a_wait_bcon state, the VBUS should be valid; but if
             * it changes into invalid, we are going to a_vbus_err state.
             */

            if (pOTG->param.input.a_vbus_vld == FALSE)
                {
                /*
                 * If VBUS drops below the A-device VBUS Valid threshold
                 * (a_vbus_vld = FALSE), then the A-device transitions to
                 * the a_vbus_err state.
                 */

                newState = USBOTG_STATE_a_vbus_err;
                }
            else if ((pOTG->param.input.id == TRUE) ||
                     (pOTG->param.input.a_bus_drop == TRUE) ||
                     (pOTG->param.timeout.a_wait_bcon_tmout == TRUE))
                {
                /*
                 * The A-device transitions to the a_wait_vfall state:
                 * - if the cable is removed (id = TRUE), or
                 * - if the A-device Application wants to drop the bus
                 *   (a_bus_drop = TRUE), or
                 * - if the a_wait_bcon_tmr times out (a_wait_bcon_tmout
                 *   = TRUE).
                 */

                newState = USBOTG_STATE_a_wait_vfall;
                }
            else if (pOTG->param.input.b_conn == TRUE)
                {
                /*
                 * If the A-device detects the B-device signaling a
                 * connection (b_conn = TRUE), then the A-device shall
                 * end the session or transition to the a_host state
                 * and generate a bus reset within TA_BCON_ARST
                 */

                newState = USBOTG_STATE_a_host;
                }

            /*
             * USB OTG Timers should be stopped on exiting from
             * their associated states
             */

            if (newState != USBOTG_STATE_a_wait_bcon)
                {
                /* Stop the a_wait_bcon_tmr as we are out of a_wait_bcon */

                usbOtgTimerStop(pOTG, USBOTG_TIMER_a_wait_bcon_tmr);

                /* Reset the a_wait_bcon_tmout as it is a one shot timer */

                pOTG->param.timeout.a_wait_bcon_tmout = FALSE;
                }
            }
            break;
        case USBOTG_STATE_a_host:
            {
            /*
             * While in a_host state, the VBUS should be valid; but if
             * it changes into invalid, we are going to a_vbus_err state.
             */

            if (pOTG->param.input.a_vbus_vld == FALSE)
                {
                /*
                 * If VBUS drops below the A-device VBUS Valid threshold
                 * (a_vbus_vld = FALSE), then the A-device transitions to
                 * the a_vbus_err state.
                 */

                newState = USBOTG_STATE_a_vbus_err;
                }
            else if ((pOTG->param.input.id == TRUE) ||
                     (pOTG->param.input.a_bus_drop == TRUE) ||
                     (pOTG->param.input.b_conn == FALSE))
                {
                #if (USBOTG_VERSION_SUPPORTED < USBOTG_VERSION0200)

                /*
                 * OTG 1.3 and earlier, under the following conditions,
                 * the A-device transitions from the a_host state to
                 * the a_wait_bcon state:
                 *
                 * - if the cable is removed (id = TRUE), or
                 * - if the A-device wishes to stop powering VBUS
                 *   (a_bus_drop = TRUE), or
                 * - if the B-device disconnects (b_conn = FALSE).
                 */

                newState = USBOTG_STATE_a_wait_bcon;

                #else /* if (USBOTG_VERSION_SUPPORTED >= USBOTG_VERSION0200) */

                /*
                 * In OTG 2.0, while in a_host state, A-device goes
                 * into a_wait_bcon only when B-device disconnects.
                 */

                if (pOTG->param.input.b_conn == FALSE)
                    {
                    /*
                     * OTG 2.0, if the B-device disconnects (b_conn = FALSE)
                     * then the A-device transitions from the a_host state
                     * to the a_wait_bcon state.
                     */

                    newState = USBOTG_STATE_a_wait_bcon;
                    }
                else if ((pOTG->param.input.id == TRUE) ||
                         (pOTG->param.input.a_bus_drop == TRUE))
                    {
                    /*
                     * OTG 2.0, under the following conditions the A-device
                     * transitions from the a_host state to the a_wait_vfall
                     * state.
                     *
                     * - if the Micro-A plug is detached (id = TRUE, applies
                     *   to OTG A-devices only), or
                     * - if the A-device wishes to stop powering VBUS
                     *  (a_bus_drop = TRUE)
                     */

                    newState = USBOTG_STATE_a_wait_vfall;
                    }
                #endif /* if (USBOTG_VERSION_SUPPORTED ...) */
                }
            else if ((pOTG->hOtgDevice != 0) &&
                     ((pOTG->param.input.a_bus_req == FALSE) ||
                      (pOTG->param.input.a_suspend_req == TRUE)))
                {
                /*
                 * OTG 2.0, under the following conditions, the A-device
                 * transitions from the a_host state to the a_suspend state:
                 *
                 * - if the application on the A-device no longer wishes
                 * to use the bus (a_bus_req = FALSE)
                 */

                /*
                 * Note, OTG 2.0 removed a_suspend_req from OTG 1.3, but
                 * for compatibility we still keep it and if it is set,
                 * we also enter a_suspend state according to the State
                 * Diagram.
                 */

                newState = USBOTG_STATE_a_suspend;
                }
            }
            break;
        case USBOTG_STATE_a_suspend:
            {
            /*
             * While in a_suspend state, the VBUS should be valid; but if
             * it changes into invalid, we are going to a_vbus_err state.
             */

            if (pOTG->param.input.a_vbus_vld == FALSE)
                {
                /*
                 * If VBUS drops below the A-device VBUS Valid threshold
                 * (a_vbus_vld = FALSE), then the A-device transitions to
                 * the a_vbus_err state.
                 */

                newState = USBOTG_STATE_a_vbus_err;
                }
            else if (pOTG->param.input.b_conn == FALSE)
                {
                /*
                 * If the B-device disconnects during suspend, and
                 * we have set b_hnp_enable feature to the B-device,
                 * it means the B-device is starting HNP.
                 */

                if (pOTG->param.internal.a_set_b_hnp_en == TRUE)
                    {
                    /*
                     * If the B-device signals a disconnect (b_conn = FALSE),
                     * and the A-device was successful in setting b_hnp_enable
                     * (a_set_b_hnp_en = TRUE), then the A-device transitions
                     * to the a_peripheral state.
                     */

                    newState = USBOTG_STATE_a_peripheral;
                    }
                else
                    {
                    /*
                     * If the B-device signals a disconnect (b_conn = FALSE),
                     * and the A-device did not set b_hnp_enable
                     * (a_set_b_hnp_en = FALSE), then the A-device transitions
                     * to the a_wait_bcon state.
                     */

                    newState = USBOTG_STATE_a_wait_bcon;
                    }
                }
            else if ((pOTG->param.input.a_bus_req == TRUE) ||
                     (pOTG->param.input.b_bus_resume == TRUE))
                {
                /*
                 * The A-device transitions to the a_host state if either
                 * a_bus_req is asserted, or if the B-device signals a
                 * resume by putting a K state on the bus, even if the
                 * remote_wakeup feature has not been enabled. If a_bus_req
                 * is asserted, then the A-device can either do a resume
                 * by putting a K state on the bus, or it can do a bus reset
                 * by outputting SE0 for longer than TB_ASE0_BRST max. The
                 * On-The-Go A-device is not allowed to resume unless the
                 * attached device is supported, or until TA_AIDL_BDIS max
                 * time has elapsed.
                 */

                newState = USBOTG_STATE_a_host;
                }
            else if ((pOTG->param.input.id == TRUE) ||
                     (pOTG->param.input.a_bus_drop == TRUE) ||
                     (pOTG->param.timeout.a_aidl_bdis_tmout == TRUE))
                {
                /*
                 * The A-device transitions to the a_wait_vfall state:
                 *
                 * - if the a_aidl_bdis_tmr times out (a_aidl_bdis_tmout
                 *   = TRUE), or
                 * - if the A-device wishes to stop powering VBUS
                 *  (a_bus_drop = TRUE) , or
                 * - if the the cable is removed (id = TRUE).
                 */

                newState = USBOTG_STATE_a_wait_vfall;
                }

            /*
             * USB OTG Timers should be stopped on exiting from
             * their associated states
             */

            if (newState != USBOTG_STATE_a_suspend)
                {
                /* Stop the a_aidl_bdis_tmr as we are out of a_suspend */

                usbOtgTimerStop(pOTG, USBOTG_TIMER_a_aidl_bdis_tmr);

                /* Reset the a_aidl_bdis_tmout as it is a one shot timer */

                pOTG->param.timeout.a_aidl_bdis_tmout = FALSE;
                }
            }
            break;
        case USBOTG_STATE_a_peripheral:
            {
            /*
             * While in a_peripheral state, the VBUS should be valid; but
             * if it changes into invalid, we are going to a_vbus_err state.
             */

            if (pOTG->param.input.a_vbus_vld == FALSE)
                {
                /*
                 * If VBUS drops below the A-device VBUS Valid threshold
                 * (a_vbus_vld = FALSE), then the A-device transitions to
                 * the a_vbus_err state.
                 */

                newState = USBOTG_STATE_a_vbus_err;
                }
            else if ((pOTG->param.input.id == TRUE) ||
                     (pOTG->param.input.a_bus_drop == TRUE) /*||
                     (pOTG->param.extend.host_request_flag == TRUE)*/)
                {
                /*
                 * The A-device transitions from the a_peripheral state
                 * to the a_wait_vfall state:
                 *
                 * - if the cable is removed (id = TRUE), or
                 * - if the A-device is no longer capable of powering
                 *   VBUS (a_bus_drop = TRUE).
                 *
                 * NOTE: As an extension to the OTG State Machine, if
                 * the host_request_flag is set to TRUE by the user while
                 * we are in a_peripheral state, we will drop the VBUS
                 * first (a_peripheral -> a_wait_vfall -> a_idle), then
                 * raise the VBUS (a_idle -> a_wait_vrise -> a_wait_bcon).
                 */

                newState = USBOTG_STATE_a_wait_vfall;
                }
            else if ((pOTG->param.input.b_bus_suspend == TRUE) ||
                     (pOTG->param.timeout.a_bidl_adis_tmout == TRUE))
                {
                /*
                 * The A-device transitions from the a_peripheral state
                 * to the a_wait_bcon state if the a_bidl_adis_tmr
                 * times out (a_bidl_adis_tmout = TRUE). (OTG 2.0)
                 *
                 * If the A-device detects more than TA_BIDL_ADIS min
                 * of continuous idle (i.e. J_state for full-speed or
                 * SE0 for high-speed), on the bus, then the A-device
                 * may transition to the a_wait_bcon state. If no
                 * activity is detected after TA_BIDL_ADIS max the
                 * A-device must transition back to the a_wait_bcon
                 * state.
                 */

                newState = USBOTG_STATE_a_wait_bcon;
                }

            /*
             * USB OTG Timers should be stopped on exiting from
             * their associated states
             */

            if (newState != USBOTG_STATE_a_peripheral)
                {
                /* Stop the a_bidl_adis_tmr as we are out of a_peripheral */

                usbOtgTimerStop(pOTG, USBOTG_TIMER_a_bidl_adis_tmr);

                /* Reset the a_bidl_adis_tmout as it is a one shot timer */

                pOTG->param.timeout.a_bidl_adis_tmout = FALSE;
                }
            }
            break;
        case USBOTG_STATE_a_wait_vfall:
            {
            /*
             * The following conditions make sure both OTG 1.3 and
             * OTG 2.0 conditions are met.
             */

            if ((pOTG->param.input.id == TRUE) ||
                (pOTG->param.input.a_bus_req == TRUE) ||
                ((pOTG->param.input.a_sess_vld == FALSE) &&
                 (pOTG->param.input.b_conn == FALSE)) ||
                (pOTG->param.timeout.a_wait_vfall_tmout == TRUE))
                {
                /*
                 * OTG 1.3:
                 *
                 * In this state, the A-device waits for the voltage on
                 * VBUS to drop below the A-device Session Valid threshold
                 * (a_sess_vld = FALSE) and for the B-device to drop D+/D-
                 * to indicate that it has detected the end of the session.
                 * Once both of these conditions are met, then the A-device
                 * transitions to the a_idle state.
                 *
                 * OTG 2.0:
                 *
                 * The A-device transitions from the a_wait_vfall state
                 * to the a_idle state:
                 *
                 * - if the a_wait_vfall_tmr expires (a_wait_vfall_tmout
                 *   = TRUE)
                 */

                newState = USBOTG_STATE_a_idle;
                }

            /*
             * USB OTG Timers should be stopped on exiting from
             * their associated states
             */

            if (newState != USBOTG_STATE_a_wait_vfall)
                {
                /* Stop the a_wait_vfall_tmr as we are out of a_wait_vfall */

                usbOtgTimerStop(pOTG, USBOTG_TIMER_a_wait_vfall_tmr);

                /* Reset the a_wait_vfall_tmout as it is a one shot timer */

                pOTG->param.timeout.a_wait_vfall_tmout = FALSE;
                }
            }
            break;
        case USBOTG_STATE_a_vbus_err:
            {
            if ((pOTG->param.input.id == TRUE) ||
                (pOTG->param.input.a_bus_drop == TRUE) ||
                (pOTG->param.internal.a_clr_err == TRUE))
                {
                /*
                 * In this state, the A-device waits for recovery of
                 * the overcurrent condition that caused it to enter
                 * this state.
                 *
                 * The A-device transitions to the a_wait_vfall state
                 * under the following conditions:
                 *
                 * - if the Micro-A plug is detached (id = TRUE, applies
                 *   to OTG A-devices only), or
                 * - if the A-device is no longer capable of powering VBUS
                 *   (a_bus_drop = TRUE)
                 * - upon assertion of a_clr_err (nominally by system
                 *   software)
                 */

                newState = USBOTG_STATE_a_wait_vfall;
                }
            }
            break;
        case USBOTG_STATE_b_idle:
            {
            /* Discharge the VBUS to meet SRP initial conditions quiker */

            if ((pOTG->prevState != USBOTG_STATE_b_srp_init) &&
                (pOTG->param.input.b_sess_vld == FALSE) &&
                (pOTG->param.input.id == TRUE) &&
                (pOTG->param.input.b_bus_req == TRUE))
                {
                USBOTG_DISCHRG_VBUS_ENABLE(pOTG, TRUE);
                }

            /*
             * The conditions for transition from b_idle to b_srp_init
             * are different between OTG 1.3 and OTG 2.0.
             */

            #if (USBOTG_VERSION_SUPPORTED < USBOTG_VERSION0200)

            /* Check if SRP init conditions are met */

            if ((pOTG->param.input.id == TRUE) &&
                (pOTG->param.input.b_sess_vld == FALSE) &&
                (pOTG->pOcdFuncs->pOtgIsLineStateSE0(pOTG->pOCD) == TRUE))
                {
                /* Wait for TB_SE0_SRP (SE0 Time Before SRP) */

                OS_DELAY_MS(USBOTG_DEFAULT_b_se0_srp_tmout);

                /* Probe the input parameters again */

                usbOtgIdVbusProbe(pOTG);

                /* If SE0 condition is till TRUE, b_se0_srp goes TRUE */

                if ((pOTG->param.input.id == TRUE) &&
                    (pOTG->param.input.b_sess_vld == FALSE) &&
                    (pOTG->pOcdFuncs->pOtgIsLineStateSE0(pOTG->pOCD) == TRUE))
                    {
                    pOTG->param.input.b_se0_srp = TRUE;
                    }
                }
            #else /* if (USBOTG_VERSION_SUPPORTED >= USBOTG_VERSION0200) */

            if ((pOTG->param.input.id == TRUE) &&
                (pOTG->param.input.b_sess_vld == FALSE))
                {
                /* If the SRP ready wait timer has not been started yet */

                if ((pOTG->otgTimer != USBOTG_TIMER_b_srp_ready_tmr) &&
                    (pOTG->param.extend.b_srp_ready_tmout == FALSE))
                    {
                    /* And if both Session End and SE0 conditions are TRUE */

                    if ((pOTG->param.input.b_sess_end == TRUE) &&
                        (pOTG->pOcdFuncs->pOtgIsLineStateSE0(pOTG->pOCD) == TRUE))
                        {
                        /* We can start the SRP ready wait timer */

                        usbOtgTimerStart(pOTG, USBOTG_TIMER_b_srp_ready_tmr);
                        }
                    }

                /* Check if the SRP ready timer expires */

                if (pOTG->param.extend.b_srp_ready_tmout == FALSE)
                    {
                    /*
                     * During the wait for SRP ready if any of b_sess_end
                     * or line state violates the SRP init condition,
                     * we will restart the timer.
                     */

                    if ((pOTG->param.input.b_sess_end != TRUE) ||
                        (pOTG->pOcdFuncs->pOtgIsLineStateSE0(pOTG->pOCD) == TRUE))
                        {
                        /* Stop the SRP ready wait timer */

                        usbOtgTimerStop(pOTG, USBOTG_TIMER_b_srp_ready_tmr);

                        /* Make sure there is no race condition */

                        pOTG->param.extend.b_srp_ready_tmout = FALSE;

                        /*
                         * The next time the b_idle state is checked, if
                         * the Session End and SE0 are both met, we could
                         * restart this timer again. See the code above!
                         */
                        }

                    /*
                     * Else just do nothing and wait b_srp_ready_tmout
                     * to be set!
                     */

                    }
                else /* (pOTG->param.extend.b_srp_ready_tmout == TRUE) */
                    {
                    /* And if both Session End and SE0 conditions are TRUE */

                    if ((pOTG->param.input.b_sess_end == TRUE) &&
                        (pOTG->pOcdFuncs->pOtgIsLineStateSE0(pOTG->pOCD) == TRUE))
                        {
                        /*
                         * We have been in both Session End and SE0 for
                         * enough time, now we may set both b_se0_srp and
                         * b_ssend_srp to TRUE and that may let us initiate
                         * SRP.
                         */

                        pOTG->param.input.b_se0_srp = TRUE;

                        pOTG->param.input.b_ssend_srp = TRUE;

                        /* Reset this one shot timeout parameter */

                        pOTG->param.extend.b_srp_ready_tmout = FALSE;
                        }
                    }
                }
            #endif /* (USBOTG_VERSION_SUPPORTED ...) */

            if (pOTG->param.input.id == FALSE)
                {
                /* Switch to a_idle only when we have HCD working for us */

                if (pOTG->isHcdLoaded == TRUE)
                    {
                    /*
                     * If the Micro-A plug is inserted (id = FALSE), then
                     * the On-The-Go B-device transitions to the a_idle
                     * state and becomes an A-device.
                     */

                    newState = USBOTG_STATE_a_idle;

                    /*
                     * Set the default timeout for the a_dile to 
                     * powerup timer and a_wait_bcon timer
                     */
                    
                    pOTG->param.extend.a_idle_powerup_time = 
                                OS_CONVERT_MILLISECONDS_TO_WAIT_VALUE(
                                USBOTG_DEFAULT_a_powerup_tmout);
                    
                    pOTG->param.extend.a_wait_bcon_time = 
                                OS_CONVERT_MILLISECONDS_TO_WAIT_VALUE(
                                USBOTG_DEFAULT_a_wait_bcon_tmout);

                    USBOTG_WARN("Switching from USBOTG_STATE_b_idle "
                                "to USBOTG_STATE_a_idle\n",
                                1, 2, 3, 4, 5, 6);
                    }
                else
                    {
                    USBOTG_VDBG("Ignored switching to USBOTG_STATE_a_idle\n",
                                1, 2, 3, 4, 5, 6);
                    }

                /* Stop the b_srp_fail_tmr as we out of b_idle */

                usbOtgTimerStop(pOTG, USBOTG_TIMER_b_srp_fail_tmr);
                }
            else if (pOTG->param.input.b_sess_vld == TRUE)
                {
                /*
                 * In this state, the OTG B-device waits for a session
                 * to start by monitoring if VBUS rises above the Session
                 * Valid threshold (b_sess_vld = TRUE). If VBUS rises
                 * above this threshold, the OTG B-device enters the
                 * b_peripheral state and signals a connect (loc_conn = TRUE).
                 */

                newState = USBOTG_STATE_b_peripheral;

                /* Stop the b_srp_fail_tmr as we have been given VBUS */

                usbOtgTimerStop(pOTG, USBOTG_TIMER_b_srp_fail_tmr);
                }
            /*
             * The conditions for transition from b_idle to b_srp_init
             * are different between OTG 1.3 and OTG 2.0.
             */

            #if (USBOTG_VERSION_SUPPORTED < USBOTG_VERSION0200)

            else if ((pOTG->param.input.b_bus_req == TRUE) &&
                     (pOTG->param.input.b_sess_end == TRUE) &&
                     (pOTG->param.input.b_se0_srp == TRUE))

            #else /* if (USBOTG_VERSION_SUPPORTED >= USBOTG_VERSION0200) */

            else if (((pOTG->param.input.b_bus_req == TRUE) ||
                      (pOTG->param.input.adp_change == TRUE)||
                      (pOTG->param.input.power_up == TRUE)) &&
                     (pOTG->param.input.b_ssend_srp == TRUE) &&
                     (pOTG->param.input.b_se0_srp == TRUE))

            #endif /* (USBOTG_VERSION_SUPPORTED ...) */

                {
                /*
                 * The following conditions are stated for OTG 2.0,
                 * but OTG 1.3 conditions are also covered.
                 *
                 * The B-device transitions from the b_idle to the
                 * b_srp_init state:
                 *
                 * - if the Application indicates that it wants to start
                 *   a session (b_bus_req = TRUE), or
                 * - an ADP change has been detected (adp_change = TRUE) or
                 * - the B-device has only just powered up its USB system
                 *   (power_up = TRUE).
                 * - and if VBUS has been below the Session End threshold
                 *   for at least TB_SSEND_SRP (b_ssend_srp = TRUE),
                 * - and if the bus has been in the SE0 state for the minimum
                 *   time before generating SRP (b_se0_srp = TRUE).
                 */

                newState = USBOTG_STATE_b_srp_init;

                /* Reset these one shot parameters */

                pOTG->param.input.adp_change = FALSE;
                pOTG->param.input.power_up = FALSE;

                pOTG->param.input.b_ssend_srp = FALSE;
                pOTG->param.input.b_se0_srp = FALSE;
                }

            /* Stop discharging VBUS */

            if (newState == USBOTG_STATE_b_srp_init)
                {
                USBOTG_DISCHRG_VBUS_ENABLE(pOTG, FALSE);
                }
            }
            break;
        case USBOTG_STATE_b_srp_init:
            {
            if ((pOTG->param.input.id == FALSE) ||
                (pOTG->param.internal.b_srp_done == TRUE))
                {
                /*
                 * Upon entering this state, the OTG B-device attempts
                 * to initiate a session via SRP. Upon completion,
                 * (b_srp_done = TRUE) the OTG B-device returns to the
                 * b_idle state, to wait for the A-device to drive VBUS
                 * above the Session Valid threshold (b_sess_vld = TRUE).
                 *
                 * If the Micro-A plug is inserted (id = FALSE), then
                 * the OTG B-device transitions to the b_idle state.
                 */

                newState = USBOTG_STATE_b_idle;
                }
            }
            break;
        case USBOTG_STATE_b_peripheral:
            {
            if ((pOTG->param.input.id == FALSE) ||
                (pOTG->param.input.b_sess_vld == FALSE))
                {
                /*
                 * The OTG B-device transitions to the b_idle state:
                 *
                 * - If a Micro-A plug is inserted (id = FALSE) or
                 * - if VBUS drops below the Session Valid threshold
                 *   (b_sess_vld = FALSE)
                 */

                newState = USBOTG_STATE_b_idle;
                }
            else if ((pOTG->param.input.b_bus_req == TRUE) &&
                     (pOTG->param.internal.b_hnp_en == TRUE) &&
                     (pOTG->param.input.a_bus_suspend == TRUE))
                {
                /*
                 * The OTG B-device transitions to the b_wait_acon state:
                 *
                 * - if the Application indicates that it wants to start
                 *   a session (b_bus_req = TRUE) and
                 * - the A-device has granted the OTG B-device permission
                 *  (b_hnp_en = TRUE) and
                 * - the bus is in the Suspend state, (a_bus_suspend = TRUE)
                 */

                if (pOTG->isHcdLoaded == TRUE)
                    {
                    newState = USBOTG_STATE_b_wait_acon;
                    }
                else
                    {
                    USBOTG_WARN("Ignored switching to USBOTG_STATE_b_wait_acon\n",
                                1, 2, 3, 4, 5, 6);
                    }
                }
            }
            break;
        case USBOTG_STATE_b_wait_acon:
            {
            if ((pOTG->param.input.id == FALSE) ||
                (pOTG->param.input.b_sess_vld == FALSE))
                {
                /*
                 * The OTG B-device transitions to the b_idle state:
                 *
                 * - If a Micro-A plug is inserted (id = FALSE) or
                 * - if VBUS drops below the Session Valid threshold
                 *   (b_sess_vld = FALSE)
                 */

                newState = USBOTG_STATE_b_idle;
                }
            else if (pOTG->param.input.a_conn == TRUE)
                {
                /*
                 * If the A-device signals a connect (a_conn = TRUE)
                 * before the b_ase0_brst_tmr expires, then the B-device
                 * transitions to the b_host state. The B-device shall
                 * be able to detect the connect from the A-device and
                 * transition to the b_host state within TB_ACON_BSE0 max
                 * after D+ is detected to be high at the B-device.
                 */

                newState = USBOTG_STATE_b_host;
                }
            else if ((pOTG->param.input.a_bus_resume == TRUE) ||
                     ((pOTG->param.timeout.b_ase0_brst_tmout == TRUE)))
                {
                /*
                 * The OTG B-device returns to the b_peripheral state:
                 *
                 * - if the b_ase0_brst_tmr expires (b_ase0_brst_tmout = TRUE),
                 *  or
                 * - if the B-device detects a K_state on the bus, indicating
                 * that the A-device is signaling a resume (a_bus_resume = TRUE)
                 */

                newState = USBOTG_STATE_b_peripheral;

                /* Set the a_bus_resume as FALSE since it is one shot */

                pOTG->param.input.a_bus_resume = FALSE;
                }

            /*
             * USB OTG Timers should be stopped on exiting from
             * their associated states
             */

            if (newState != USBOTG_STATE_b_wait_acon)
                {
                /* Stop the b_ase0_brst_tmr as we are out of b_wait_acon */

                usbOtgTimerStop(pOTG, USBOTG_TIMER_b_ase0_brst_tmr);

                /* Reset the b_ase0_brst_tmout as it is a one shot timer */

                pOTG->param.timeout.b_ase0_brst_tmout = FALSE;
                }
            }
            break;
        case USBOTG_STATE_b_host:
            {
            #if (USBOTG_VERSION_SUPPORTED < USBOTG_VERSION0200)

            if ((pOTG->param.input.id == FALSE) ||
                    (pOTG->param.input.b_sess_vld == FALSE))
                {
                /*
                 * The OTG B-device transitions to the b_idle state:
                 *
                 * - If a Micro-A plug is inserted (id = FALSE) or
                 * - if VBUS drops below the Session Valid threshold
                 *   (b_sess_vld = FALSE)
                 */

                newState = USBOTG_STATE_b_idle;
                }
            else
            #endif /* if (USBOTG_VERSION_SUPPORTED ...) */

            if ((pOTG->param.input.b_bus_req == FALSE) ||
                (pOTG->param.input.a_conn == FALSE) ||
                (pOTG->param.input.test_device == TRUE))
                {
                /*
                 * The B-device stops generating bus activity (loc_sof = FALSE)
                 * and transitions to the b_peripheral state:
                 *
                 * - once the B-device has completed its usage of the A-device
                 *   (b_bus_req = FALSE), or
                 * - if the B-device detects that the A-device has signaled a
                 *   disconnect (a_conn = FALSE), or
                 * - within 30 seconds of the OTG B-device detecting that a
                 *   test device is attached (test_device = TRUE)
                 */

                newState = USBOTG_STATE_b_peripheral;
                }
            }
            break;
        case USBOTG_STATE_unknown:
        /* Fall through - do nonthing! */
        default:
            {
            /* Update the ID and VBUS state */

            usbOtgIdVbusProbe(pOTG);

            pOTG->param.input.power_up = TRUE;

            /*
             * Enter the A-device or B-device initial idle state.
             * When power up, assume the A-device Application wants
             * to use the bus by default
             */

            if (pOTG->param.input.id == FALSE)
                {
                newState = USBOTG_STATE_a_idle;
                
                /*
                 * Set the default timeout for the a_dile to 
                 * powerup timer and a_wait_bcon timer
                 */
                
                pOTG->param.extend.a_idle_powerup_time = 
                            OS_CONVERT_MILLISECONDS_TO_WAIT_VALUE(
                            USBOTG_DEFAULT_a_powerup_tmout);

                pOTG->param.extend.a_wait_bcon_time = 
                            OS_CONVERT_MILLISECONDS_TO_WAIT_VALUE(
                            USBOTG_DEFAULT_a_wait_bcon_tmout);

                pOTG->param.input.a_bus_req = TRUE;
                }
            else
                {
                newState = USBOTG_STATE_b_idle;

                pOTG->param.input.b_bus_req = TRUE;
                }
            }
            break;
        }

    if (newState != pOTG->currState)
        USBOTG_DBG("usbOtgStateSingleStep - New state %s\n",
                    usbOtgStateName[newState], 2, 3, 4, 5, 6);

    return newState;
    }

/*******************************************************************************
*
* usbOtgStateAdvance - update the USB OTG state machine to the next state
*
* This routine is to update the USB OTG state machine to the next state. The
* state machine is advanced according to the current OTG state and input
* parameters and other state variables.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*/

LOCAL STATUS usbOtgStateAdvance
    (
    pUSBOTG_MANAGER pOTG
    )
    {
    USBOTG_STATE newState;
    USBOTG_STATE prevState;

    /* Check if the parameters are valid */

    if (!pOTG || !pOTG->pOCD)
        {
        USBOTG_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Lock the OTG instance */

    USBOTG_LOCK_INSTANCE(pOTG);

    do
        {
        /* Save the current state as the previous state */

        prevState = pOTG->currState;

        /* Update the ID and VBUS states first */

        usbOtgIdVbusProbe(pOTG);

        /* Advance the A-device OTG State Machine one single step */

        newState = usbOtgStateSingleStep(pOTG);

        /*
         * If the new state is different than the current state,
         * then we need to enter that state and take proper actions
         * as required by that state.
         */

        if (newState != pOTG->currState)
            {
            USBOTG_VDBG("usbOtgStateAdvance - New state %s\n",
                        usbOtgStateName[newState], 2, 3, 4, 5, 6);

            usbOtgNewStateEnter(pOTG, newState);
            }

        }while (prevState != pOTG->currState);

    /* Unlock the OTG instance */

    USBOTG_UNLOCK_INSTANCE(pOTG);

    return OK;
    }

/*******************************************************************************
*
* usbOtgStateGet - get current USB OTG state
*
* This routine is to get the current USB OTG state.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*/

LOCAL STATUS usbOtgStateGet
    (
    pUSBOTG_MANAGER pOTG,
    USBOTG_STATE *  pState
    )
    {

    /* Check if the parameters are valid */

    if (!pOTG || !pOTG->pOCD)
        {
        USBOTG_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Lock the OTG instance */

    USBOTG_LOCK_INSTANCE(pOTG);

    *pState = pOTG->currState;

    /* Unlock the OTG instance */

    USBOTG_UNLOCK_INSTANCE(pOTG);

    return OK;
    }

/*******************************************************************************
*
* usbOtgVbusStateGet - get the VBUS state
*
* This routine is to get the local OTG device type (A-device or B-device).
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*/

LOCAL STATUS usbOtgVbusStateGet
    (
    pUSBOTG_MANAGER         pOTG,
    USBOTG_VBUS_STATE *     pVbus
    )
    {
    /* Check if the parameters are valid */

    if (!pOTG || !pOTG->pOCD)
        {
        USBOTG_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Lock the OTG instance */

    USBOTG_LOCK_INSTANCE(pOTG);

    /* Update the ID and VBUS states first */

    usbOtgIdVbusProbe(pOTG);

    /* Cast to device type */

    *pVbus = pOTG->vbusState;

    /* Unlock the OTG instance */

    USBOTG_UNLOCK_INSTANCE(pOTG);

    return OK;
    }

/*******************************************************************************
*
* usbOtgDeviceTypeGet - get the local OTG device type (A-device or B-device)
*
* This routine is to get the local OTG device type (A-device or B-device).
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*/

LOCAL STATUS usbOtgDeviceTypeGet
    (
    pUSBOTG_MANAGER         pOTG,
    USBOTG_DEVICE_TYPE *    pType
    )
    {
    /* Check if the parameters are valid */

    if (!pOTG || !pOTG->pOCD)
        {
        USBOTG_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Lock the OTG instance */

    USBOTG_LOCK_INSTANCE(pOTG);

    /* Update the ID and VBUS states first */

    usbOtgIdVbusProbe(pOTG);

    /* Cast to device type */

    if (pOTG->idState == USBOTG_ID_LOW)
        {
        *pType = USBOTG_A_DEVICE;
        }
    else /* if (pOTG->idState == USBOTG_ID_HIGH) */
        {
        *pType = USBOTG_B_DEVICE;
        }

    /* Unlock the OTG instance */

    USBOTG_UNLOCK_INSTANCE(pOTG);

    return OK;
    }

/*******************************************************************************
*
* usbOtgHostGiveupAllowed - check if give up host role is allowed
*
* This routine is to check if give up host role is allowed.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*/

LOCAL STATUS usbOtgHostGiveupAllowed
    (
    pUSBOTG_MANAGER         pOTG,
    BOOL *                  pAllowed
    )
    {
    BOOL allowGiveup = FALSE;
    
    /* Check if the parameters are valid */

    if (!pOTG || !pOTG->pOCD)
        {
        USBOTG_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);

        *pAllowed = FALSE;
        
        return ERROR;
        }
    
    /* Lock the OTG instance */

    USBOTG_LOCK_INSTANCE(pOTG);

    /* Check if we are A-device or B-device */
    
    if (pOTG->idState == USBOTG_ID_LOW)
        {
        /*
         * We are A-device, so we are only able to do HNP when an OTG
         * capable device is there and we have enabled the HNP feature.
         */

        if ((pOTG->hOtgDevice != 0) && 
            (pOTG->currState == USBOTG_STATE_a_host) &&
            (pOTG->param.internal.a_set_b_hnp_en == TRUE))
            allowGiveup = TRUE;
        }
    else /* if (pOTG->idState == USBOTG_ID_HIGH) */
        {
        /*
         * We are B-device, we can do HNP (transfering host role back) 
         * only if we have become host and a device is connected (the 
         * device connected, A-device, of course should be OTG capable
         * and it has already enabled HNP for us - otherwise it should
         * not have become our device!)
         */

        if ((pOTG->hOtgDevice != 0) && 
            (pOTG->currState == USBOTG_STATE_b_host))
            allowGiveup = TRUE;
        }

    *pAllowed = allowGiveup;
    
    /* Unlock the OTG instance */

    USBOTG_UNLOCK_INSTANCE(pOTG);

    return OK;
    }

/*******************************************************************************
*
* usbOtgHostRequestAllowed - check if request host role is allowed
*
* This routine is to check if request host role is allowed.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*/

LOCAL STATUS usbOtgHostRequestAllowed
    (
    pUSBOTG_MANAGER         pOTG,
    BOOL *                  pAllowed
    )
    {
    BOOL allowRequest = FALSE;
    
    /* Check if the parameters are valid */

    if (!pOTG || !pOTG->pOCD)
        {
        USBOTG_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);

        *pAllowed = FALSE;
        
        return ERROR;
        }
    
    /* Lock the OTG instance */

    USBOTG_LOCK_INSTANCE(pOTG);

    /* Check if we are A-device or B-device */
    
    if (pOTG->idState == USBOTG_ID_LOW)
        {
        /*
         * We are A-device, so we are only able to do HNP host reqest when
         * an OTG capable device is there and we have been in peripheral
         * role.
         */

        if ((pOTG->currState == USBOTG_STATE_a_peripheral) || 
            (pOTG->currState == USBOTG_STATE_a_wait_bcon) ||
            ((pOTG->hOtgDevice != 0) && 
             (pOTG->currState == USBOTG_STATE_a_suspend)))
            allowRequest = TRUE;
        }
    else /* if (pOTG->idState == USBOTG_ID_HIGH) */
        {
        /*
         * We are B-device, we can do HNP (requesting host role) 
         * only if we are in peripheral role and the A-device has
         * enabled us to do HNP (b_hnp_en = TRUE).
         */

        if (((pOTG->currState == USBOTG_STATE_b_peripheral) ||
             (pOTG->currState == USBOTG_STATE_b_wait_acon)) &&
            (pOTG->param.internal.b_hnp_en == TRUE))
            allowRequest = TRUE;
        }

    *pAllowed = allowRequest;
    
    /* Unlock the OTG instance */

    USBOTG_UNLOCK_INSTANCE(pOTG);

    return OK;
    }

/*******************************************************************************
*
* usbOtgHostRoleRequest - request for host role
*
* This routine is to request to be in host role. The actual actions taken
* depends on the current device mode:
*
* For A-device:
*
* 1) If it is already working as host (a_host), or is waiting for B-device
* connection (a_wait_bcon), then it will keep as it is.
*
* 2) If it is working as host but is in suspend (a_suspend), it will resume
* operating as host (a_host).
*
* 3) The user application can monitor the OTG state to become a_host.
*
* For B-device:
*
* 1) The B-device needs HNP support in order to become host, otherwise
* the request will fail with ERROR.
*
* 2) If the B-device is already in host mode (b_host) or waiting for
* A-device connection (b_wait_acon), it will keep at it is.
*
* 3) The user application can monitor the OTG state to become b_host.
*
* WARNNING:
*
* No matter whether it is A-device or B-device, setting <reqType> value as
* USBOTG_HOST_REQUEST_HNP_POLLING might be dangerous! The user should use 
* it at his/her own risk with the following consideration in mind!
*
* The caller should be warned that this might cause data loss if there are
* any data transfers in progress since the host may shutdown the transfer
* by force.
*
* NOTE:
*
* If <reqType> is USBOTG_HOST_REQUEST_HNP_BASIC, then the OK return value 
* of this call is just to indicate that the host role is requested, but has 
* to be decided by the current host when it will actually give up the host 
* role and let the requester to become host.
*
* Even if <reqType> is set to USBOTG_HOST_REQUEST_HNP_POLLING, if the current
* host doesn't support HNP polling introduced in OTG 2.0, the effect is the 
* same as <reqType> set to USBOTG_HOST_REQUEST_HNP_BASIC.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*/

LOCAL STATUS usbOtgHostRoleRequest
    (
    pUSBOTG_MANAGER             pOTG,
    USBOTG_HOST_REQUEST_TYPE    reqType,
    _Vx_ticks_t                 waitTime
    )
    {
    BOOL    allowed = FALSE;
    
    /* Check if the parameters are valid */

    if (!pOTG || !pOTG->pOCD)
        {
        USBOTG_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* We need HCD in oder to request host role */

    if (pOTG->isHcdLoaded != TRUE)
        {
        USBOTG_ERR("Can not request host role - HCD not loaded\n",
            1, 2, 3, 4, 5, 6);
        
        return ERROR;
        }

    /* If we are already there, no need to do request again */
    
    if ((pOTG->currState == USBOTG_STATE_a_host) ||
        (pOTG->currState == USBOTG_STATE_b_host))
        {
        USBOTG_DBG("No need to request host role - Alreay in host role\n",
                    1, 2, 3, 4, 5, 6);
                
        return OK;
        }
    
    allowed = FALSE;
    
    do
        {
        /* Check if we are allowed to give up host role */
                
        if (usbOtgHostRequestAllowed(pOTG, &allowed) != OK)
            {
            USBOTG_ERR("Can not request host role - condition check failed\n",
                1, 2, 3, 4, 5, 6);
            
            return ERROR;
            }

        /* 
         * If we are now allowed to request the host role,
         * break to do it! 
         */
        
        if (allowed == TRUE)
            break;
        
        if (waitTime == NO_WAIT)
            {
            USBOTG_DBG("Can not request host role - wait time expire in %s\n",
                usbOtgStateName[pOTG->currState], 2, 3, 4, 5, 6);
        
            return ERROR;
            }
        else if (waitTime != WAIT_FOREVER)
            {
            /* 
             * Decrease the wait count if a posstive wait time 
             * is specified (until the count reach to 0, NO_WAIT); 
             * WAIT_FOEVER (-1U) case would not decrease and thus
             * will loop forever until it is allowed to request.
             */
             
            waitTime --;
            }
        
        /* Relinquish one tick to get the state machine running */
        
        OS_RESCHEDULE_THREAD();  
        
        } while (TRUE);

    /* Lock the OTG instance */

    USBOTG_LOCK_INSTANCE(pOTG);
    
    /* Update the ID and VBUS states first */

    usbOtgIdVbusProbe(pOTG);

    /* If ID is FALSE, it is A-device */

    if (pOTG->param.input.id == FALSE)
        {
        /*
         * If the user needs host role ASAP (ASAP = TRUE),
         * but the current OTG state is a_peripheral, we will:
         *
         * 1) First drop the VBUS
         *
         *  a_peripheral -> a_wait_vfall -> a_idle
         *
         * 2) Then raise the VBUS
         *
         *  a_idle -> a_wait_vrise -> a_wait_bcon
         *
         * In all other A-device states, setting a_bus_req as TRUE
         * and a_bus_drop as FALSE is able to bring the OTG state
         * to either a_wait_bcon or a_host.
         */

        pOTG->param.input.a_bus_req = TRUE;
        pOTG->param.input.a_bus_drop = FALSE;
        pOTG->param.input.a_suspend_req = FALSE;

        /*
         * There might be a VBUS drop if ASAP is TRUE, take it
         * one your own risk!
         *
         * You have been warned by the API documentation!
         */

        pOTG->param.extend.host_request_flag = 
            (reqType == USBOTG_HOST_REQUEST_HNP_POLLING) ? TRUE : FALSE;

        /* Always clear VBUS error automatically */

        pOTG->param.internal.a_clr_err = TRUE;

        /* Enable hardware automatic HNP (B-Suspend to A-Disconnect) */
        
        USBOTG_AUTO_SUSP2DIS_ENABLE(pOTG, TRUE);

        /* Warn the user of the risk */
        
        if (reqType == USBOTG_HOST_REQUEST_HNP_POLLING)
            {
            pUSBOTG_USR_MSG_HDR pUsrMsgHdr;

            pUsrMsgHdr = (pUSBOTG_USR_MSG_HDR)usbOtgEventDataGet();
            
            if (pUsrMsgHdr)
                {
                /* Notify the user of this event */
            
                pUsrMsgHdr->uMsgId = USBOTG_USR_MSG_ID_REQ_HOST_RISKY;
                
                usbOtgUsrMsgNotify(pOTG, (void *)pUsrMsgHdr);
                }
            else
                {
                USBOTG_WARN("Get msg USBOTG_USR_MSG_ID_REQ_HOST_RISKY failed!\n",
                            1, 2, 3, 4, 5, 6);
                }
            }
        }
    else /* If ID is TRUE, it is B-device */
        {
        /* B-device needs HNP to ask for host role */

        if ((pOTG->pOCD->bmAttributes & USBOTG_ATTR_HNP_SUPPORT) &&
            (pOTG->param.internal.b_hnp_en == TRUE))
            {
            /* Enable hardware automatic HNP (A-Suspend to B-Disconnect) */
            
            USBOTG_AUTO_SUSP2DIS_ENABLE(pOTG, TRUE);
            
            /*
             * When b_bus_req, b_hnp_en and a_bus_suspend are all TRUE,
             * the OTG State Machine will go from b_peripheral to b_wait_acon
             */

            pOTG->param.input.b_bus_req = TRUE;

            /*
             * Set the host_request_flag so that when the B-device gets
             * a GetStatus(OTG status selector) from the A-device, it will
             * tell (force) the A-device to stop the bus activity and proceed
             * to the HNP process. The A-device will need to respond to this
             * within THOST_REQ_SUSP (Max 2sec).
             */

            pOTG->param.extend.host_request_flag = 
                (reqType == USBOTG_HOST_REQUEST_HNP_POLLING) ? TRUE : FALSE;

            /* Warn the user of the risk */
            
            if (reqType == USBOTG_HOST_REQUEST_HNP_POLLING)
                {
                pUSBOTG_USR_MSG_HDR pUsrMsgHdr;
                
                pUsrMsgHdr = (pUSBOTG_USR_MSG_HDR)usbOtgEventDataGet();
                
                if (pUsrMsgHdr)
                    {
                    /* Notify the user of this event */
                
                    pUsrMsgHdr->uMsgId = USBOTG_USR_MSG_ID_REQ_HOST_RISKY;
                    
                    usbOtgUsrMsgNotify(pOTG, (void *)pUsrMsgHdr);
                    }
                else
                    {
                    USBOTG_WARN("Get msg USBOTG_USR_MSG_ID_REQ_HOST_RISKY failed!\n",
                                1, 2, 3, 4, 5, 6);
                    }
                }
            }
        else
            {   
            USBOTG_DBG("No need to request host role!\n",
                        1, 2, 3, 4, 5, 6);
            
            /* Unlock the OTG instance */

            USBOTG_UNLOCK_INSTANCE(pOTG);

            return OK;
            }
        }

    /* Unlock the OTG instance */

    USBOTG_UNLOCK_INSTANCE(pOTG);

    /* Notify parameter changed */

    USBOTG_NOTIFY_PARAM_CHANGED(pOTG);

    return OK;
    }

/*******************************************************************************
*
* usbOtgHostRoleGiveup - give up host role
*
* This routine is used for user applications to drop (or give up) host role.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*/

LOCAL STATUS usbOtgHostRoleGiveup
    (
    pUSBOTG_MANAGER pOTG,
    int             waitTime
    )
    {
    BOOL    allowed = FALSE;
    
    /* Check if the parameters are valid */

    if (!pOTG || !pOTG->pOCD)
        {
        USBOTG_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* We need TCD in oder to giveup host role */
    
    if (pOTG->isTcdLoaded != TRUE)
        {
        USBOTG_ERR("Can not giveup host role - TCD not loaded\n",
            1, 2, 3, 4, 5, 6);
        
        return ERROR;
        }

    allowed = FALSE;
    
    do
        {
        /* Check if we are allowed to give up host role */
                
        if (usbOtgHostGiveupAllowed(pOTG, &allowed) != OK)
            {
            USBOTG_ERR("usbOtgHostGiveupAllowed failed\n",
                1, 2, 3, 4, 5, 6);
            
            return ERROR;
            }

        /* 
         * If we are now allowed to give up the host role,
         * break to do it! 
         */
        
        if (allowed == TRUE)
            break;
        
        if (waitTime == NO_WAIT)
            {
            USBOTG_DBG("Does not allow host role give up yet in %s state\n",
                usbOtgStateName[pOTG->currState], 2, 3, 4, 5, 6);
        
            return ERROR;
            }
        else if (waitTime != WAIT_FOREVER)
            {
            /* 
             * Decrease the wait count if a posstive wait time 
             * is specified (until the count reach to 0, NO_WAIT); 
             * WAIT_FOEVER (-1U) case would not decrease and thus
             * will loop forever until it is allowed to giveup.
             */
             
            waitTime --;
            }
        
        /* Relinquish one tick to get the state machine running */
        
        OS_RESCHEDULE_THREAD();  
        
        } while (TRUE);
    
    /* Lock the OTG instance */

    USBOTG_LOCK_INSTANCE(pOTG);

    /* Update the ID and VBUS states first */

    usbOtgIdVbusProbe(pOTG);

    /* If the ID pin is FALSE, it means we are A-device */

    if (pOTG->param.input.id == FALSE)
        {
        /*
         * Set the input parameters to take the OTG State
         * Machine into a_idle state (no matter what the
         * current state it is).
         */

        pOTG->param.input.a_bus_req = FALSE;
        pOTG->param.input.a_suspend_req = TRUE;
        }
    else /* If the ID pin is TRUE, it means we are B-device */
        {
        /*
         * Set the input parameters to take the OTG State
         * Machine into b_peripheral state or b_idle state
         */

        pOTG->param.input.b_bus_req = FALSE;
        }

    /* Clear the host request flag */
    
    pOTG->param.extend.host_request_flag = FALSE;

    /* Disable hardware automatic HNP (A-Suspend to B-Disconnect) */
    
    USBOTG_AUTO_SUSP2DIS_ENABLE(pOTG, FALSE);

    /* Unlock the OTG instance */

    USBOTG_UNLOCK_INSTANCE(pOTG);

    /* Notify parameter changed */

    USBOTG_NOTIFY_PARAM_CHANGED(pOTG);

    return OK;
    }

/*******************************************************************************
*
* usbOtgVbusPowerOn - request to power on VBUS
*
* This routine is used for user applications request to power on VBUS.
* The actual actions taken depends on the current device mode:
*
* For A-device:
*
* 1) The a_bus_drop will be set to FALSE, and a_bus_req will be set to
* TRUE if it is not in a_suspend state. This would cause the VBUS to
* be raised and kept driving if it already raised.
*
* 2) If the A-device is already in a_suspend state, a_bus_req won't be
* changed, but a_bus_drop will be set to FALSE. This makes sure the VBUS
* is kept driving, and there will be no resume from a_suspend to a_host.
*
* 3) The user application can monitor the VBUS state to become
* USBOTG_VBUS_STATE_VBusValid.
*
* For B-device:
*
* 1) The b_bus_req will be set to TRUE, if the VBUS is not valid, it will
* initiate SRP when SRP initial conditions are met.
*
* 2) The user application can monitor the VBUS state to become
* USBOTG_VBUS_STATE_VBusValid.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*/

LOCAL STATUS usbOtgVbusPowerOn
    (
    pUSBOTG_MANAGER pOTG,
    _Vx_ticks_t     onTime
    )
    {
    /* Check if the parameters are valid */

    if (!pOTG || !pOTG->pOCD)
        {
        USBOTG_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Lock the OTG instance */

    USBOTG_LOCK_INSTANCE(pOTG);

    /* Update the ID and VBUS states first */

    usbOtgIdVbusProbe(pOTG);

    /* If the ID pin is FALSE, it means we are A-device */

    if (pOTG->param.input.id == FALSE)
        {
        _Vx_ticks_t defTime;
        
        /*
         * If the A-device is in a_suspend state, setting
         * a_bus_req would cause it to resume, which might
         * be a side effect that the user doesn't require,
         * so we just don't try that in that case.
         */

        if (pOTG->currState != USBOTG_STATE_a_suspend)
            pOTG->param.input.a_bus_req = TRUE;

        pOTG->param.input.a_bus_drop = FALSE;

        defTime = OS_CONVERT_MILLISECONDS_TO_WAIT_VALUE(
                            USBOTG_DEFAULT_a_wait_bcon_tmout);

        if (onTime < defTime)
            onTime = defTime;
        
        pOTG->param.extend.a_wait_bcon_time = onTime;
        }
    else /* If the ID pin is TRUE, it means we are B-device */
        {
        /*
         * Set the input parameters to take the OTG State
         * Machine into b_peripheral state (it might be followed
         * by HNP and then enter b_host state, but that is not
         * controlled by this code, we just fulfilled our request
         * to get the VBUS powered up!)
         */

        pOTG->param.input.b_bus_req = TRUE;

        #if (USBOTG_VERSION_SUPPORTED >= USBOTG_VERSION0200)
        pOTG->param.input.power_up = TRUE;
        #endif /* (USBOTG_VERSION_SUPPORTED >= USBOTG_VERSION0200) */
        }

    /* Unlock the OTG instance */

    USBOTG_UNLOCK_INSTANCE(pOTG);

    /* Notify parameter changed */

    USBOTG_NOTIFY_PARAM_CHANGED(pOTG);

    return OK;
    }

/*******************************************************************************
*
* usbOtgVbusPowerOff - request to power off VBUS
*
* This routine is used for user applications request to power off VBUS.
* The actual actions taken depends on the current device mode:
*
* For A-device:
*
* 1) The a_bus_drop will be set to TRUE, and a_bus_req will be set to
* FALSE. This will cause the A-device OTG state into a_idle state
*
* 2) The user application can monitor the VBUS state to become
* USBOTG_VBUS_STATE_SessEnd.
*
* For B-device:
*
* 1) There is no "OTG defined way" for the B-device to request A-device
* to power off the VBUS; In this case the B-device has to wait until
* the A-device to power off the VBUS. So for B-device this call will always
* return ERROR.
*
* 2) Although the above situation applies, the user application can still
* monitor the VBUS state to become USBOTG_VBUS_STATE_SessEnd.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*/

LOCAL STATUS usbOtgVbusPowerOff
    (
    pUSBOTG_MANAGER     pOTG,
    _Vx_ticks_t         offTime
    )
    {
    /* Check if the parameters are valid */

    if (!pOTG || !pOTG->pOCD)
        {
        USBOTG_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Lock the OTG instance */

    USBOTG_LOCK_INSTANCE(pOTG);

    /* Update the ID and VBUS states first */

    usbOtgIdVbusProbe(pOTG);

    /* If the ID pin is FALSE, it means we are A-device */

    if (pOTG->param.input.id == FALSE)
        {
        /*
         * This will lead to a_idle state in which VBUS is off
         */

        pOTG->param.input.a_bus_drop = TRUE;

        /*
         * Save the user specified timeout for the a_dile to 
         * powerup timer 
         */
        
        pOTG->param.extend.a_idle_powerup_time = offTime;
        }
    else /* If the ID pin is TRUE, it means we are B-device */
        {
        /* Unlock the OTG instance */

        USBOTG_UNLOCK_INSTANCE(pOTG);
        
        return ERROR;
        }

    /* Unlock the OTG instance */

    USBOTG_UNLOCK_INSTANCE(pOTG);

    /* Notify parameter changed */

    USBOTG_NOTIFY_PARAM_CHANGED(pOTG);

    return OK;
    }

/*******************************************************************************
*
* usbOtgHnpPolling - perform HNP polling as defined in OTG 2.0
*
* This routine is to perform HNP polling as defined in OTG 2.0. Although the
* HNP polling is defined in OTG 2.0, the advantage of it could be taken by
* OTG 1.x implementations to ask for role switch ASAP. If there is no such
* a mechanism, two problems might be hard to be resolved:
*
* 1) When the B-device wants to get host role, it may have reported its HNP
* capability in its OTG descriptor, and the A-device may have set the
* b_hnp_enable feature, but if A-device doesn't give up the host role on
* it's own, there is no way to tell the A-device that the B-device wants
* to use the bus (become host) Now!
*
* 2) If the A-device has given it's host role to B-device, it has no way to
* ask back the host role unless the B-device gives up its host role!
*
* With HNP polling, if both A-device and B-device supports HNP polling and
* respond to the polling, then the above problems can be solved. There is
* only one thing that it needs to warn the user the possible of lossing data
* if the ASAP role switch is requested while there are on going transfers.
* Note that not all ongoing transfers will suffer from this risk, for example,
* if there are mouse movements ongoing, it should be no harm to stop it anytime.
*
* NOTE: No parameter validity is checked since it is used internally and
* the calling routine should have assured the validity.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL VOID usbOtgHnpPolling
    (
    pUSBOTG_MANAGER pOTG
    )
    {
    /* Check if the parameters are valid */

    if (!pOTG || !pOTG->pOCD)
        {
        USBOTG_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);

        return;
        }

    /* Lock the OTG instance */

    USBOTG_LOCK_INSTANCE(pOTG);
    
    /*
     * If both the A-device and B-device supports HNP,
     * we need to do HNP polling
     */

    if (((pOTG->currState == USBOTG_STATE_a_host) ||
         (pOTG->currState == USBOTG_STATE_b_host)) &&
        (pOTG->pOCD->bmAttributes & USBOTG_ATTR_HNP_SUPPORT) &&
        (pOTG->hOtgDevice) &&
        (pOTG->param.extend.rmt_bm_attr & USBOTG_ATTR_HNP_SUPPORT))
        {
        /* Clear the b_otg_status */

        pOTG->param.extend.rmt_otg_sts = 0;

        /* Get peripheral OTG status */
        
        USBOTG_DBG("Get peripheral OTG status for device 0x%X!\n",
                    pOTG->hOtgDevice, 2, 3, 4, 5, 6);

        /*
         * NOTE that we have changed usbHstGetStatus() API to allow
         * wLength of 1 for USBOTG_OTGSTS_SELECTOR.
         */

        if (usbHstGetStatus(pOTG->hOtgDevice,
                            USBHST_RECIPIENT_DEVICE,
                            USBOTG_OTGSTS_SELECTOR,
                            &pOTG->param.extend.rmt_otg_sts
                            ) == USBHST_SUCCESS)
            {
            USBOTG_VDBG("Remote device 0x%X OTG status 0x%X!\n",
                        pOTG->hOtgDevice, 
                        pOTG->param.extend.rmt_otg_sts, 3, 4, 5, 6);
            
            /* Check if the Host Request Flag is set */

            if (pOTG->param.extend.rmt_otg_sts & USBOTG_STS_HOST_REQ_FLAG)
                {
                pUSBOTG_USR_MSG_HDR pUsrMsgHdr;
                
                pUsrMsgHdr = (pUSBOTG_USR_MSG_HDR)usbOtgEventDataGet();
                
                if (pUsrMsgHdr)
                    {
                    /* Cast to specific message */
                    
                    pUSBOTG_USR_MSG_DEV_REQ_HOST pUstMsg = 
                        (pUSBOTG_USR_MSG_DEV_REQ_HOST)(pUsrMsgHdr);
                    
                    /* Fill message specific information */
                    
                    pUstMsg->hDevice = pOTG->hOtgDevice;
                    
                    /* Notify the user of this event */
                
                    pUsrMsgHdr->uMsgId = USBOTG_USR_MSG_ID_DEV_REQ_HOST;
                    
                    usbOtgUsrMsgNotify(pOTG, (void *)pUsrMsgHdr);
                    }
                else
                    {
                    USBOTG_WARN("Get msg USBOTG_USR_MSG_ID_DEV_REQ_HOST failed!\n",
                                1, 2, 3, 4, 5, 6);
                    }
                }
            }
        else
            {
            USBOTG_DBG("usbOtgHnpPolling - Get OTG Status failed for device %p\n",
                        pOTG->hOtgDevice, 2, 3, 4, 5, 6);
            }
        }

    /* Unlock the OTG instance */

    USBOTG_UNLOCK_INSTANCE(pOTG);

    return;
    }

/*******************************************************************************
*
* usbOtgUsrMsgNotify - notify OTG user message by raising OTG user event
*
* This routine is to notify OTG user message to user application by raise an
* OTG user event.
*
* NOTE: These user message is considered to be only possible to occur one 
* at a time, so there is no need to dynamically allocate the message, but to 
* use a pre-allocated message structure to send to the user.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL STATUS usbOtgUsrMsgNotify
    (
    pUSBOTG_MANAGER pOTG,
    void *          pUsrMsg
    )
    {    
    return erfEventRaise(usbOtgEventCategory,
                        usbOtgUserEventType,
                        ERF_ASYNC_PROC,
                        (void *)pUsrMsg,
                        usbOtgEventDataPut);
    }

/*******************************************************************************
*
* usbOtgUsrMsgHandler - notify OTG user message to user application
*
* This routine is to notify OTG user message to user application by calling 
* user callback.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL VOID usbOtgUsrMsgHandler
    (
    UINT16 Category,
    UINT16 Type,
    VOID * eventData,
    VOID * userData
    )
    {
    pUSBOTG_MANAGER pOTG = (pUSBOTG_MANAGER)userData;
    pUSBOTG_USR_MSG_HDR pUsrMsgHdr = (pUSBOTG_USR_MSG_HDR)eventData;

    if (!pOTG || pOTG->uValidMagic != USBOTG_MAGIC_VALID || !pUsrMsgHdr)
        {
        USBOTG_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);
        
        return;
        }
    
    /* Check if user application installed a user defined message handler */
    
    if (pOTG->pUsrCallback)
        {
        pOTG->pUsrCallback(pUsrMsgHdr, pOTG->pUsrContext);
        }
    else
        {
        /* Use the default hanlding */
        
        switch (pUsrMsgHdr->uMsgId)
            {
            case USBOTG_USR_MSG_ID_SRP_ACTIVE:
                {
                USBOTG_INFO("USBOTG_USR_MSG_ID_SRP_ACTIVE\n",
                    1, 2, 3, 4, 5, 6);
                }
                break;
            case USBOTG_USR_MSG_ID_SRP_FAIL:
                {
                USBOTG_INFO("USBOTG_USR_MSG_ID_SRP_FAIL\n",
                    1, 2, 3, 4, 5, 6);
                }
                break;
            case USBOTG_USR_MSG_ID_VRISE_FAIL:
                {
                USBOTG_INFO("USBOTG_USR_MSG_ID_VRISE_FAIL\n",
                    1, 2, 3, 4, 5, 6);
                }
                break;
            case USBOTG_USR_MSG_ID_HOST_NO_HNP:
                {
                USBOTG_INFO("USBOTG_USR_MSG_ID_HOST_NO_HNP\n",
                    1, 2, 3, 4, 5, 6);
                }
                break;
            case USBOTG_USR_MSG_ID_DEV_UNSUPPORTED:
                {
                    
                USBOTG_INFO("USBOTG_USR_MSG_ID_DEV_UNSUPPORTED - dev handle 0x%X\n",
                    ((pUSBOTG_USR_MSG_DEV_UNSUPPORTED)pUsrMsgHdr)->hDevice, 2, 3, 4, 5, 6);
                }
                break;
            case USBOTG_USR_MSG_ID_DEV_REQ_HOST:
                {
                
                USBOTG_INFO("USBOTG_USR_MSG_ID_DEV_REQ_HOST - dev handle 0x%X\n",
                    ((pUSBOTG_USR_MSG_DEV_UNSUPPORTED)pUsrMsgHdr)->hDevice, 2, 3, 4, 5, 6);

                }
                break;
            case USBOTG_USR_MSG_ID_REQ_HOST_RISKY:
                {
                USBOTG_INFO("USBOTG_USR_MSG_ID_REQ_HOST_RISKY\n",
                    1, 2, 3, 4, 5, 6);
                }
                break;
            case USBOTG_USR_MSG_ID_HOST_ACTIVE:
                {
                USBOTG_INFO("USBOTG_USR_MSG_ID_HOST_ACTIVE\n",
                    1, 2, 3, 4, 5, 6);
                }
                break;
            case USBOTG_USR_MSG_ID_TARGET_ACTIVE:
                {
                USBOTG_INFO("USBOTG_USR_MSG_ID_TARGET_ACTIVE\n",
                    1, 2, 3, 4, 5, 6);
                }
                break;
            case USBOTG_USR_MSG_ID_DEV_UNRESPONDING:
                {
                
                USBOTG_INFO("USBOTG_USR_MSG_ID_DEV_UNRESPONDING - dev handle 0x%X\n",
                    ((pUSBOTG_USR_MSG_DEV_UNSUPPORTED)pUsrMsgHdr)->hDevice, 2, 3, 4, 5, 6);
                }
                break;
            case USBOTG_USR_MSG_ID_SRP_DETECTED:
                {
                USBOTG_INFO("USBOTG_USR_MSG_ID_SRP_DETECTED\n",
                    1, 2, 3, 4, 5, 6);
                
                usbOtgVbusPowerOn(pOTG, USBOTG_DEFAULT_a_wait_bcon_tmout);
                }
                break;
            default:
                {
                USBOTG_INFO("Unknown USB OTG message %d\n", 
                    pUsrMsgHdr->uMsgId, 2, 3, 4, 5, 6);
                }
                break;
            }
        }
    }

/*******************************************************************************
*
* usbOtgEventNotify - notify and process OTG event
*
* This routine is to notify and process OTG event.
*
* RETURNS: event process result
*
* ERRNO: N/A
*/

LOCAL STATUS usbOtgEventNotify
    (
    pUSBOTG_MANAGER pOTG,
    USBOTG_EVENT_ID eventId,
    VOID *          eventData
    )
    {
    STATUS result = OK;

    switch (eventId)
        {
        case USBOTG_EVENT_ID_GET_OTG_STATUS_RECEIVED:
            {
            pUSBOTG_GET_OTG_STATUS_RECEIVED_EVENT pEvent;
            pEvent = (pUSBOTG_GET_OTG_STATUS_RECEIVED_EVENT) eventData;

            result = usbOtgNotifyGetOtgStatusReceived(pOTG, &pEvent->otgStatus);
            }
            break;
        case USBOTG_EVENT_ID_SET_OTG_FEATURE_RECEIVED:
            {
            pUSBOTG_SET_OTG_FEATURE_EVENT pEvent;
            pEvent = (pUSBOTG_SET_OTG_FEATURE_EVENT) eventData;

            usbOtgNotifySetOtgFeatureReceived(pOTG, (UINT8)pEvent->otgFeature);
            }
            break;
        case USBOTG_EVENT_ID_OTG_DEVICE_ENUMERATED:
            {
            pUSBOTG_OTG_DEVICE_ENUMERATED_EVENT pEvent;
            pEvent = (pUSBOTG_OTG_DEVICE_ENUMERATED_EVENT)eventData;

            usbOtgNotifyOtgDeviceEnumerated(pOTG,
                                            pEvent->hDevice,
                                            pEvent->pOtgDescr);
            }
            break;
        case USBOTG_EVENT_ID_ID_STATE_CHANGED:
            {
            pUSBOTG_ID_STATE_CHANGED_EVENT pEvent;
            pEvent = (pUSBOTG_ID_STATE_CHANGED_EVENT)eventData;

            usbOtgNotifyIdStateChanged(pOTG, pEvent->idState);
            }
            break;
        case USBOTG_EVENT_ID_VBUS_STATE_CHANGED:
            {
            pUSBOTG_VBUS_STATE_CHANGED_EVENT pEvent;
            pEvent = (pUSBOTG_VBUS_STATE_CHANGED_EVENT)eventData;

            usbOtgNotifyVbusStateChanged(pOTG, pEvent->vbusState);
            }
            break;
        case USBOTG_EVENT_ID_SRP_DETECTED:
            {
            pUSBOTG_SRP_DETECTED_EVENT pEvent;
            pEvent = (pUSBOTG_SRP_DETECTED_EVENT)eventData;

            usbOtgNotifySrpDetected(pOTG);
            }
            break;
        case USBOTG_EVENT_ID_BUS_RESET_DETECTED:
            {
            pUSBOTG_BUS_RESET_DETECTED_EVENT pEvent;
            pEvent = (pUSBOTG_BUS_RESET_DETECTED_EVENT)eventData;

            usbOtgNotifyBusResetDetected(pOTG);
            }
            break;
        case USBOTG_EVENT_ID_SUSPEND_RESUME_DETECTED:
            {
            pUSBOTG_SUSPEND_RESUME_DETECTED_EVENT pEvent;
            pEvent = (pUSBOTG_SUSPEND_RESUME_DETECTED_EVENT)eventData;

            usbOtgNotifySuspendResumeDetected(pOTG, pEvent->isSuspend);
            }
            break;
        case USBOTG_EVENT_ID_CONNECT_DISCONNECT_DETECTED:
            {
            pUSBOTG_CONNECT_DISCONNECT_DETECTED_EVENT pEvent;
            pEvent = (pUSBOTG_CONNECT_DISCONNECT_DETECTED_EVENT)eventData;

            usbOtgNotifyConnectDisconnectDetected(pOTG, pEvent->isConnect);
            }
            break;
        case USBOTG_EVENT_ID_GET_OTG_DESCR_RECEIVED:
            {
            pUSBOTG_GET_OTG_DESCR_RECEIVED_EVENT pEvent;
            pEvent = (pUSBOTG_GET_OTG_DESCR_RECEIVED_EVENT)eventData;
        
            usbOtgNotifyGetOtgDescrReceived(pOTG, pEvent->pOtgDescr);
            }
            break;
        case USBOTG_EVENT_ID_SET_CONFIG_RECEIVED:
            {        
            usbOtgNotifySetConfigReceived(pOTG);
            }
            break;
        case USBOTG_EVENT_ID_OTG_TIMER_TIMEOUT:
            {
            usbOtgTimerTimeoutAction(pOTG);
            }
            break;
        case USBOTG_EVENT_ID_OTG_DEVICE_UNSUPPORTED:
            {
            pUSBOTG_OTG_DEVICE_UNSUPPORTED_EVENT pEvent;
            pEvent = (pUSBOTG_OTG_DEVICE_UNSUPPORTED_EVENT)eventData;
            
            usbOtgNotifyOtgDeviceUnsupported(pOTG, pEvent->hDevice);
            }
            break;
        default:
            result = ERROR;
            break;
        }
    return result;
    }

/*******************************************************************************
*
* usbOtgEventHandler - handle OTG event report using erfLib
*
* This routine is to handle OTG event report.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL VOID usbOtgEventHandler
    (
    UINT16 Category,
    UINT16 Type,
    VOID * eventData,
    VOID * userData
    )
    {
    pUSBOTG_MANAGER pOTG = (pUSBOTG_MANAGER)userData;
    pUSBOTG_EVENT_HEADER pEventHeader = (pUSBOTG_EVENT_HEADER)eventData;

    if (!pOTG || !pEventHeader || pOTG->uValidMagic != USBOTG_MAGIC_VALID)
        {
        USBOTG_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);
        return;
        }

    /*
     * If the event does not belong to OTG event category,
     * or not for our controller instance, ignore it!
     */

    if ((Category != usbOtgEventCategory) ||
        (pEventHeader->pDev != pOTG->pDev))
        {
        USBOTG_VDBG("Not our event with Type 0x%X!! \n"
                    "Category 0x%X usbOtgEventCategory 0x%X,"
                    "pEventHeader->pDev %p, pOTG->pDev %p eventData %p\n",
                    Type, Category, usbOtgEventCategory,
                    pEventHeader->pDev, pOTG->pDev, eventData);

        return;
        }

    USBOTG_DBG("usbOtgEventHandler - USBOTG_EVENT_ID %d\n", 
               pEventHeader->id, 2, 3, 4, 5, 6);

    /* Notify and process the event */

    pEventHeader->result = usbOtgEventNotify(pOTG, pEventHeader->id, eventData);

    return;
    }

/*******************************************************************************
*
* usbOtgEventDataGet - get a buffer as USB OTG event data
*
* This routine is to get a buffer as USB OTG event data.
*
* RETURNS: memory buffer to be used as event data, NULL if failed.
*
* ERRNO: N/A
*/

void * usbOtgEventDataGet (void)
    {
    void * pEventData;
    
    if (!usbOtgEventDataPoolId)
        return NULL;

    /*
     * The poolItemGet() and poolItemReturn() functions may be called from 
     * interrupt context as long as the pool was created without the 
     * POOL_THREAD_SAFE.
     */  
     
    pEventData = poolItemGet(usbOtgEventDataPoolId);
    
    USBOTG_VDBG("usbOtgEventDataGet %p\n",
               pEventData, 2, 3, 4, 5, 6);

    return pEventData;
    }

/*******************************************************************************
*
* usbOtgEventDataPut - return a buffer which has been used as USB OTG event data
*
* This routine is to return a buffer which has been used as USB OTG event data.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbOtgEventDataPut 
    (
    void * pEventData
    )
    {
    USBOTG_VDBG("usbOtgEventDataPut %p\n",
               pEventData, 2, 3, 4, 5, 6);
    
    if (!usbOtgEventDataPoolId)
        return;

    /*
     * The poolItemGet() and poolItemReturn() functions may be called from 
     * interrupt context as long as the pool was created without the 
     * POOL_THREAD_SAFE.
     */      
     
    poolItemReturn(usbOtgEventDataPoolId, pEventData);
    }

/*******************************************************************************
*
* usbOtgEventRaise - raise USB OTG related event
*
* This routine is to raise USB OTG related event.
*
* RETURNS: OK if USB OTG related event is rasied successfully, ERROR if failed.
*
* ERRNO: N/A
*/

STATUS usbOtgEventRaise
    (
    BOOL   bSyncProc,
    void * pEventData
    )
    {
    STATUS                  sts = ERROR;

    if (pEventData != NULL)
        {
        sts = erfEventRaise(usbOtgEventCategory,
                      usbOtgCoreEventType,
                      bSyncProc ? ERF_SYNC_PROC : ERF_ASYNC_PROC,
                      (void *)pEventData,
                      bSyncProc ? NULL : usbOtgEventDataPut);
        }

    return sts;
    }

/*******************************************************************************
*
* usbOtgEventDbDestroy - destroy USB OTG event database for use with erfLib
*
* This routine is to destroy USB OTG event database for use with erfLib.
*
* RETURNS: OK if the event database is created successfully, ERROR if failed.
*
* ERRNO: N/A
*/

LOCAL STATUS usbOtgEventDbDestroy (void)
    {    
    /* Destroy the OTG event data pool */
    
    if (!usbOtgEventDataPoolId)
        {
        poolDelete (usbOtgEventDataPoolId, TRUE);
        
        usbOtgEventDataPoolId = NULL;
        }

    /* 
     * Note that there is no corresponding code to "destroy" the event 
     * category and types since the they are actually variable values 
     * in the "data" section of the final image. 
     */
    
    return (OK);
    }

/*******************************************************************************
*
* usbOtgEventDbCreate - create USB OTG event database for use with erfLib
*
* This routine is to create USB OTG event database for use with erfLib.
*
* RETURNS: OK if the event database is created successfully, ERROR if failed.
*
* ERRNO: N/A
*/

LOCAL STATUS usbOtgEventDbCreate (void)
    {
    STATUS retVal;
    
    /* Create the OTG event data pool */
    
    if (!usbOtgEventDataPoolId)
        {
        /*
         * Create an event data pool for the event type.
         *
         * Note that we do not use the "POOL_THREAD_SAFE" option, which will
         * cause the pool to be protexted by mutex, however we will use the
         * pool alloation in ISR context, so mutex is not usable.
         */
         
        usbOtgEventDataPoolId = poolCreate("usbOtgEventDataPool", 
                                          64,
                                          0, 
                                          1024,
                                          0, 
                                          NULL, 
                                          0);
        
        if (!usbOtgEventDataPoolId)
            {
            USBOTG_ERR("poolCreate failed for usbOtgEventDataPoolId\n",
                       1, 2, 3, 4, 5, 6);

            return ERROR;
            }
        }

    retVal = erfCategoryAllocate(&usbOtgEventCategory);
    if (retVal == ERROR)
        {
        USBOTG_ERR("erfCategoryAllocate failed for usbOtgEventCategory\n",
                   1, 2, 3, 4, 5, 6);
        
        usbOtgEventDbDestroy();
        
        return (ERROR);
        }

    USBOTG_VDBG("usbOtgEventCategory 0x%X\n",
               usbOtgEventCategory, 2, 3, 4, 5, 6);

    retVal = erfTypeAllocate (usbOtgEventCategory, &usbOtgCoreEventType);
    if (retVal == ERROR)
        {
        USBOTG_ERR("erfTypeAllocate failed for usbOtgCoreEventType\n",
                   1, 2, 3, 4, 5, 6);
        
        usbOtgEventDbDestroy();
        
        return (ERROR);
        }

    USBOTG_VDBG("usbOtgCoreEventType 0x%X\n",
               usbOtgCoreEventType, 2, 3, 4, 5, 6);

    retVal = erfTypeAllocate (usbOtgEventCategory, &usbOtgUserEventType);
    if (retVal == ERROR)
        {
        USBOTG_ERR("erfTypeAllocate failed for usbOtgUserEventType\n",
                   1, 2, 3, 4, 5, 6);
        
        usbOtgEventDbDestroy();
        
        return (ERROR);
        }
    
    USBOTG_VDBG("usbOtgUserEventType 0x%X\n",
               usbOtgUserEventType, 2, 3, 4, 5, 6);

    return (OK);
    }

/*******************************************************************************
*
* usbOtgEventUnregister - unregister event handler for use with erfLib
*
* This routine is to unregister event handler for use with erfLib.
*
* RETURNS: OK if event handler is deregistered successfully, ERROR if failed.
*
* ERRNO: N/A
*/

LOCAL STATUS usbOtgEventUnregister
    (
    pUSBOTG_MANAGER     pOTG
    )
    {
    STATUS retVal;

    /* Unregister OTG Core event handler */
    
    retVal = erfHandlerUnregister (usbOtgEventCategory,
                                 usbOtgCoreEventType,
                                 (erfHandlerPrototype *)usbOtgEventHandler,
                                 pOTG);

    /* Unregister OTG User event handler */

    retVal = erfHandlerUnregister (usbOtgEventCategory,
                                 usbOtgCoreEventType,
                                 (erfHandlerPrototype *)usbOtgUsrMsgHandler,
                                 pOTG);

    return (OK);
    }

/*******************************************************************************
*
* usbOtgEventRegister - register event handler for use with erfLib
*
* This routine is to register event handler for use with erfLib.
*
* RETURNS: OK if event handler is registered successfully, ERROR if failed.
*
* ERRNO: N/A
*/

LOCAL STATUS usbOtgEventRegister
    (
    pUSBOTG_MANAGER     pOTG
    )
    {
    STATUS retVal;

    retVal = erfHandlerRegister (usbOtgEventCategory,
                                 usbOtgCoreEventType,
                                 (erfHandlerPrototype *)usbOtgEventHandler,
                                 pOTG,
                                 (UINT16)0);
    if (retVal == ERROR)
        return (ERROR);

    retVal = erfHandlerRegister (usbOtgEventCategory,
                                 usbOtgUserEventType,
                                 (erfHandlerPrototype *)usbOtgUsrMsgHandler,
                                 pOTG,
                                 (UINT16)0);
    if (retVal == ERROR)
        return (ERROR);

    return (OK);
    }

/*
 * usbOtgManagerTask - task for USB OTG Manager state machine advancing
 *
 * This routine implements USB OTG Manager event handling task.
 *
 * The task will wait for the parameter changed event notification with a
 * predefined timeout value, but it does not always depend on the event
 * notification, thus it does not check if the wait exits because of timeout
 * or not. This is to implement the following behaviors:
 *
 * 1) If the parameters are changed due to user request, or the low level
 *    drivers event report, or OTG timer timeout action, then the semaphore
 *    will be released by these handler routines, and thus this OTG manager
 *    task would be activated by the event. This is to avoid the need for
 *    taskDelay() which would always block the task the fixed amount of time.
 *
 * 2) If there is no parameter change event notification, it doesn't mean
 *    there is no parameter change. Some paramters may not be reported by
 *    event notifications, such as the ID pin state, these parameters must be
 *    polled. For this, the semaphore wait time out gives us a chance to
 *    perform the parameter state polling and check if we need to advance
 *    the OTG State Machine.
 *
 * RETURNS: N/A
 *
 * ERRNO: N/A
 */

LOCAL VOID usbOtgManagerTask
    (
    pUSBOTG_MANAGER pOTG
    )
    {
    USBOTG_STATE    prevState;  /* Previous OTG state */

    /*
     * The OTG Controller Driver must be set before this
     * routine is entered
     */

    if (!pOTG || !pOTG->pOCD || pOTG->uValidMagic != USBOTG_MAGIC_VALID)
        {
        USBOTG_ERR("Invalid parameter!\n", 1, 2, 3, 4, 5, 6);

        return;
        }
    
    /*
     * Initially set the polling intervalue to a big value,
     * then when HNP polling is required, we can set it to 
     * in the range of the HNP polling interval.
     */
    
    pOTG->mgrInterval = usrUsbOtgMgrEventWaitTimeGet();
    
    while (TRUE)
        {
        /* Wait for the parameter change notification */

        USBOTG_WAIT_PARAM_CHANGE(pOTG, pOTG->mgrInterval);

        /* Peroform HNP polling if requred */

        usbOtgHnpPolling(pOTG);

        /* Save current state as 'previous' state before advancing */
        prevState = pOTG->currState;
        
        /* Advance the OTG State Machine */

        usbOtgStateAdvance(pOTG);

        /* Only log if state changed */
        
        if (prevState != pOTG->currState)
            {
            USBOTG_DBG("USBOTG_MANAGER state advanced from %s to %s\n",
                        usbOtgStateName[prevState], 
                        usbOtgStateName[pOTG->currState], 3, 4, 5, 6);
            }
        }
    }


/*******************************************************************************
*
* usbOtgManagerDestroy - destroy the USB OTG Manager Framework instance
*
* This routine is to destroy the USB OTG Manager Framework instance.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*/

LOCAL STATUS usbOtgManagerDestroy
    (
    pUSBOTG_OCD         pOCD
    )
    {
    pUSBOTG_MANAGER     pOTG;

    /* Get the OTG Manager Framework instance */

    pOTG = pOCD->pOTG;

    /* If the OTG magic is set, then I/O device has been added */

    if (pOTG->uValidMagic == USBOTG_MAGIC_VALID)
        {
        (void)iosDevDelete(&pOTG->devHdr);
        }

    /*
     * Any external code that refers to the OTG instance will
     * test the magic value, if it is not USBOTG_MAGIC_VALID,
     * don't use it!
     */

    pOTG->uValidMagic = USBOTG_MAGIC_INVALID;

    /* Delete the OTG watchdog timer */

    if (pOTG->wdId)
        {
        wdDelete(pOTG->wdId);

        pOTG->wdId = NULL;
        }

#ifdef USBOTG_USE_MANAGER_TASK

    /* Delete the OTG manager task */

    if ((pOTG->mgrTaskId != TASK_ID_ERROR) && 
        ((pOTG->mgrTaskId != 0)))
        {        
        taskDelete(pOTG->mgrTaskId);

        pOTG->mgrTaskId = TASK_ID_ERROR;
        }

#endif /* USBOTG_USE_MANAGER_TASK */

    /* Unload the HCD */

    if (pOTG->isHcdLoaded == TRUE)
        {
        pOTG->pOcdFuncs->pOtgUnLoadHCD(pOTG->pOCD);

        pOTG->isHcdLoaded = FALSE;
        }

    /* Unload the TCD */

    if (pOTG->isTcdLoaded == TRUE)
        {
        pOTG->pOcdFuncs->pOtgUnLoadTCD(pOTG->pOCD);

        pOTG->isTcdLoaded = FALSE;
        }

    /* Delete the OTG lock */

    if (pOTG->otgLock)
        {
        /* Take the mutex before delete it */
        
        semTake(pOTG->otgLock, WAIT_FOREVER);
        
        /* Delete the semaphore created */

        semDelete(pOTG->otgLock);

        pOTG->otgLock = NULL;
        }

    /* Delete the OTG prameter change notification semaphore */

    if (pOTG->paramSem)
        {
        /* Delete the semaphore created */

        semDelete(pOTG->paramSem);

        pOTG->paramSem = NULL;
        }

    /* Unregister the event handlers */
    
    usbOtgEventUnregister(pOTG);

    /*
     * Deassociate the OTG Controller Driver with
     * the OTG Manager instance
     */

    pOCD->pOTG = NULL;
    pOTG->pOCD = NULL;

    /* Remove the OTG controller from the global controller list */
    
    lstDelete(&usbOtgCtlrList, &pOTG->node);
    
    OS_FREE(pOTG);

    return OK;
    }


/*******************************************************************************
*
* usbOtgManagerCreate - create the USB OTG Manager Framework instance
*
* This routine is to init the USB OTG Manager Framework instance.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*/

LOCAL STATUS usbOtgManagerCreate
    (
    pUSBOTG_OCD     pOCD
    )
    {
    pUSBOTG_MANAGER pOTG;
    char            otgDevName[USB_OTG_MGR_NAME_MAX_SIZE];
    UINT32          count;
    USBOTG_DEVICE_TYPE devType = USBOTG_A_DEVICE;

    /* Create the OTG Manager instance - returned buffer is cleared */

    pOTG = OSS_CALLOC(sizeof(USBOTG_MANAGER));

    if (pOTG == NULL)
        {
        USBOTG_ERR("Could not create USBOTG_MANAGER\n",
                    1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * Associate the OTG Controller Driver with
     * the new OTG Manager instance
     */

    pOTG->pOCD = pOCD;
    pOCD->pOTG = pOTG;
    pOTG->pDev = pOCD->pDev;

    /*
     * Initilally set to unknown state and the OTG Manager
     * task will find out what state we should go by detecting
     * the ID pin state.
     */

    pOTG->prevState = pOTG->currState = USBOTG_STATE_unknown;

    /* Set the initial stack mode as idle (unknown) mode */

    pOTG->stackMode = USBOTG_MODE_HOST;

    /* Set the initial timer ID as unknown timer */

    pOTG->otgTimer = USBOTG_TIMER_unknown_tmr;

    /* Save short cut to OCD Functions */

    pOTG->pOcdFuncs = pOTG->pOCD->pOcdFuncs;

    /* Set the OTG version that we support */

    pOTG->otgVersion = USBOTG_VERSION_SUPPORTED;

    /* Set the OTG device handle as 0 since it is not detected yet */

    pOTG->hOtgDevice = 0;

    /* Create the mutext for the OTG Manager Framework instance */

    pOTG->otgLock = semMCreate(SEM_Q_PRIORITY |
                               SEM_INVERSION_SAFE |
                               SEM_DELETE_SAFE);

    if (pOTG->otgLock == NULL)
        {
        USBOTG_ERR("Failed to create OTG lock\n",
                    1, 2, 3, 4, 5, 6);

        usbOtgManagerDestroy(pOCD);

        return ERROR;
        }

    /*
     * Create the parameter change notification semaphore for
     * the OTG Manager Framework instance
     */

    pOTG->paramSem = semBCreate(SEM_Q_PRIORITY, SEM_FULL);

    if (pOTG->paramSem == NULL)
        {
        USBOTG_ERR("Failed to create OTG lock\n",
                    1, 2, 3, 4, 5, 6);

        usbOtgManagerDestroy(pOCD);

        return ERROR;
        }

    /*
     * Create the Watchdog which might be used as OTG timer
     * if the OTG Controller Driver does not provide one hardware
     * OTG timer.
     */

    pOTG->wdId = wdCreate();

    if (pOTG->wdId == NULL)
        {
        USBOTG_ERR("Failed to create OTG watchdog timer\n",
                    1, 2, 3, 4, 5, 6);

        usbOtgManagerDestroy(pOCD);

        return ERROR;
        }

    /* Watch dog timer is not initially used */

    vxAtomicClear(&pOTG->wdInUse);

    /* Register the event hander failed */

    if (usbOtgEventRegister(pOTG) != OK)
        {
        USBOTG_ERR("Register event handler failed\n",
                    1, 2, 3, 4, 5, 6);

        usbOtgManagerDestroy(pOCD);

        return ERROR;
        }

    /* Initially, set both HCD and TCD not loaded */

    pOTG->isHcdLoaded = FALSE;
    pOTG->isTcdLoaded = FALSE;

    USBOTG_DBG("Trying to load HCD and TCD\n",
                1, 2, 3, 4, 5, 6);

    /* Load the HCD if the OTG Controller Driver requires */

    if (pOTG->pOcdFuncs->pOtgLoadHCD)
        {
        USBOTG_DBG("Trying to load HCD\n",
                    1, 2, 3, 4, 5, 6);
        if (pOTG->pOcdFuncs->pOtgLoadHCD(pOTG->pOCD) == OK)
            pOTG->isHcdLoaded = TRUE;
        }

    /* Load the TCD if the OTG Controller Driver requires */

    if (pOTG->pOcdFuncs->pOtgLoadTCD)
        {
        USBOTG_DBG("Trying to load TCD\n",
                    1, 2, 3, 4, 5, 6);
        
        if (pOTG->pOcdFuncs->pOtgLoadTCD(pOTG->pOCD) == OK)
            pOTG->isTcdLoaded = TRUE;
        }

    /* We need at least either HCD or TCD */

    if (pOTG->isHcdLoaded == FALSE && pOTG->isTcdLoaded == FALSE)
        {
        USBOTG_ERR("Failed to load both HCD and TCD\n",
                    1, 2, 3, 4, 5, 6);

        usbOtgManagerDestroy(pOCD);

        return ERROR;
        }

    /* The OTG Manager instance is not initially opened */

    vxAtomicClear(&pOTG->devOpened);

    /* Create the OTG Manager Task */

    count = lstCount(&usbOtgCtlrList);

    /* Create the OTG user interface I/O device */

    snprintf(otgDevName, USB_OTG_MGR_NAME_MAX_SIZE,
             "%s%d", usrUsbOtgMgrBaseNameGet(), count);

    /* Add the device into the iosLib */

    if (iosDevAdd(&pOTG->devHdr, otgDevName, usbOtgDrvNum) != OK)
        {
        USBOTG_ERR("Failed to add OTG Controller device to iosLib\n",
            1, 2, 3, 4, 5, 6);

        usbOtgManagerDestroy(pOCD);

        return ERROR;
        }

    /* Set the OTG Manager Framework instance as valid */

    pOTG->uValidMagic = USBOTG_MAGIC_VALID;

    /* Siwtch into proper stack */
    
    if (usbOtgDeviceTypeGet(pOTG, &devType) == OK)
        {
        if (devType == USBOTG_A_DEVICE)
            usbOtgStackSwitch(pOTG, USBOTG_MODE_HOST, TRUE);
        else
            usbOtgStackSwitch(pOTG, USBOTG_MODE_PERIPHERAL, TRUE);
        }
    else
        {
        USBOTG_ERR("Failed to get OTG Controller device type\n",
            1, 2, 3, 4, 5, 6);

        usbOtgManagerDestroy(pOCD);

        return ERROR;
        }

    /* Create the OTG Manager Task */

    snprintf(otgDevName, USB_OTG_MGR_NAME_MAX_SIZE, "tUsbOtgM%d", count);

    /* Add the OTG controller into the global list */
    
    lstAdd(&usbOtgCtlrList, &pOTG->node);
    
#ifdef USBOTG_USE_MANAGER_TASK
    /*
     * We will make these parameters configurable when things are clear
     */

    pOTG->mgrTaskId = taskSpawn(otgDevName,                     /* name */
                                usrUsbOtgMgrTaskPriorityGet(),  /* priority */
                                USBOTG_MANAGER_TASK_OPTIONS|VX_UNBREAKABLE,    /* options */
                                USBOTG_MANAGER_TASK_STACK_SIZE, /* stackSize */
                                (FUNCPTR)usbOtgManagerTask,
                                (_Vx_usr_arg_t)pOTG,
                                (_Vx_usr_arg_t)2,
                                (_Vx_usr_arg_t)3,
                                (_Vx_usr_arg_t)4,
                                (_Vx_usr_arg_t)5,
                                (_Vx_usr_arg_t)6,
                                (_Vx_usr_arg_t)7,
                                (_Vx_usr_arg_t)8,
                                (_Vx_usr_arg_t)9,
                                (_Vx_usr_arg_t)10);

    if (pOTG->mgrTaskId == TASK_ID_ERROR)
        {
        USBOTG_ERR("Failed to create OTG manager task\n",
                    1, 2, 3, 4, 5, 6);

        usbOtgManagerDestroy(pOCD);

        return ERROR;
        }
#endif /* USBOTG_USE_MANAGER_TASK */

    return OK;
    }

/*******************************************************************************
*
* usbOtgManagerOpen - io open call for the OTG Manager Framework instance
*
* This routine is to check the IO of the device, and create a file descriptor
* for latter access.
*
* RETURNS:  pointer to USB2_FILE_DESC, or ERROR if there is any failure.
*
* ERRNO:
* \is
* \i EACCES
* Permission denied.
*
* \i EINVAL
* Any parameter is invalid.
* \ie
*/

LOCAL void * usbOtgManagerOpen
    (
    DEV_HDR *   pDevHdr,
    char *      notUsed,
    int         flags,
    int         mode
    )
    {
    pUSBOTG_MANAGER pOTG = (pUSBOTG_MANAGER)pDevHdr;

    /* Check if the parameter is still valid */

    if (!pOTG || pOTG->uValidMagic != USBOTG_MAGIC_VALID)
        {
        USBOTG_ERR("Invalid parameter\n",
                    1, 2, 3, 4, 5, 6);

        errnoSet(EINVAL);

        return (void *)ERROR;
        }

    /*
     * We don't want the device to be created so refuse that flag.
     */

    if (O_CREAT & flags)
        {
        USBOTG_ERR("OTG Manager device not creatable\n",
            1, 2, 3, 4, 5, 6);

        errnoSet(EINVAL);

        return (void *)ERROR;
        }

    /* We only allow one open instance */

    if (vxAtomicSet(&pOTG->devOpened, 1) != 0)
        {
        USBOTG_ERR("OTG Manager device is already opened\n",
                    1, 2, 3, 4, 5, 6);

        errnoSet(EACCES);

        return (void *)ERROR;
        }

    return (void *)pOTG;
    }

/*******************************************************************************
*
* usbOtgManagerClose - io close call to the OTG Manager Framework instance
*
* This routine is to close the file descriptor to this device.
*
* RETURNS: OK, or ERROR if there is any failure.
*
* ERRNO:
* \is
* \i EINVAL
* Any parameter is invalid.
*
* \ie
*/

LOCAL int usbOtgManagerClose
    (
    pUSBOTG_MANAGER pOTG /* File descriptor used by application */
    )
    {
    /* Check if the parameter is still valid */

    if (!pOTG || pOTG->uValidMagic != USBOTG_MAGIC_VALID)
        {
        USBOTG_ERR("Invalid parameter\n",
                    1, 2, 3, 4, 5, 6);

        errnoSet(EINVAL);

        return ERROR;
        }

    /* We only need to clear the opened flag */

    vxAtomicClear(&pOTG->devOpened);

    return OK;
    }

/*******************************************************************************
*
* usbOtgManagerRead - io read call to the OTG Manager Framework instance
*
* This routine is to read the device and stores data to the application buffer.
* The OTG Manager doesn't allow READ access so it will only return ERROR.
*
* RETURNS: ERROR as the OTG Manager doesn't allow READ access.
*
* ERRNO:
* \is
* \i EACCES
* Permission denied.
* \ie
*/

LOCAL ssize_t usbOtgManagerRead
    (
    pUSBOTG_MANAGER     pOTG,       /* File descriptor used by application */
    UCHAR *             pBuffer,    /* Buffer to read to */
    size_t              bfrSize     /* Number of bytes to read */
    )
    {
    USBOTG_ERR("OTG Manager doesn't allow READ access\n",
               1, 2, 3, 4, 5, 6);

    errnoSet(EACCES);

    return ERROR;
    }

/*******************************************************************************
*
* usbOtgManagerWrite - io write call to the OTG Manager Framework instance
*
* This routine is to write to the device from the application buffer.
*
* The OTG Manager doesn't allow WRITE access so it will only return ERROR.
*
* RETURNS: ERROR as the OTG Manager doesn't allow WRITE access.
*
* ERRNO:
* \is
* \i EACCES
* Permission denied.
* \ie
*/

LOCAL ssize_t usbOtgManagerWrite
    (
    pUSBOTG_MANAGER     pOTG,       /* File descriptor used by application */
    UCHAR *             pBuffer,    /* application buffer */
    size_t              bfrSize     /* size to write */
    )
    {
    USBOTG_ERR("OTG Manager doesn't allow WRITE access\n",
               1, 2, 3, 4, 5, 6);

    errnoSet(EACCES);

    return ERROR;
    }

/*******************************************************************************
*
* usbOtgManagerIoctl - ioctl call to control the OTG Manager Framework instance
*
* This routine supports the following ioctls :
*
* \ml
* \m 1. USBOTG_IOCTL_OTG_STATE_GET :
*       Get current USB OTG state; See comments for usbOtgStateGet().
*
* \m 2. USBOTG_IOCTL_VBUS_STATE_GET :
*       Get the VBUS state; See comments for usbOtgVbusStateGet().
*
* \m 3. USBOTG_IOCTL_DEV_TYPE_GET :
*       Get the local OTG device type (A-device or B-device); See
*       comments for usbOtgDeviceTypeGet().
*
* \m 4. USBOTG_IOCTL_HOST_REQUEST :
*       Request for host role; See comments for usbOtgHostRoleRequest().
*
* \m 5. USBOTG_IOCTL_HOST_GIVEUP :
*       Give up host role; See comments for usbOtgHostRoleGiveup().
*
* \m 6. USBOTG_IOCTL_VBUS_POWER_ON :
*       Request to power on VBUS; See comments for usbOtgVbusPowerOn().
*
* \m 7. USBOTG_IOCTL_VBUS_POWER_OFF :
*       Request to power off VBUS; See comments for usbOtgVbusPowerOff().
*
* \m 8. USBOTG_IOCTL_CALLBACK_INSTALL :
*       Install user message callback to report user messages.
*
* \m 9. USBOTG_IOCTL_STACK_MODE_GET
*       Get the current stack mode.
*
* \me
*
* RETURNS: status of the requested operation.
*
* ERRNO:
* \is
*
* \i EINVAL
* Any parameter is invalid.
* \ie
*/

LOCAL int usbOtgManagerIoctl
    (
    pUSBOTG_MANAGER     pOTG,       /* File descriptor used by application */
    int                 function,   /* Function to use (see above) */
    void *              arg         /* Argument to use */
    )
    {
    STATUS sts = ERROR;

    /* Check if the parameter is still valid */

    if (!pOTG || pOTG->uValidMagic != USBOTG_MAGIC_VALID)
        {
        USBOTG_ERR("Invalid parameter - pOTG %p uValidMagic %p\n",
                    pOTG, pOTG ? pOTG->uValidMagic : 0, 3, 4, 5, 6);

        errnoSet(EINVAL);

        return ERROR;
        }

    switch (function)
        {
        case USBOTG_IOCTL_OTG_STATE_GET:
            sts = usbOtgStateGet(pOTG, (USBOTG_STATE *)arg);
            break;
        case USBOTG_IOCTL_VBUS_STATE_GET:
            sts = usbOtgVbusStateGet(pOTG, (USBOTG_VBUS_STATE *)arg);
            break;
        case USBOTG_IOCTL_DEV_TYPE_GET:
            sts = usbOtgDeviceTypeGet(pOTG, (USBOTG_DEVICE_TYPE *)arg);
            break;
        case USBOTG_IOCTL_HOST_REQUEST:
            if (arg != NULL)
                {
                USBOTG_USR_HOST_REQ_INFO * pHostReqInfo;

                pHostReqInfo = (USBOTG_USR_HOST_REQ_INFO *)arg;

                sts = usbOtgHostRoleRequest(pOTG, 
                                            pHostReqInfo->reqType,
                                            pHostReqInfo->reqWaitTime);
                }
            break;
        case USBOTG_IOCTL_HOST_GIVEUP:
             if (arg != NULL)   
                {
                USBOTG_USR_HOST_GIVEUP_INFO * pGiveupInfo;
                
                pGiveupInfo = (USBOTG_USR_HOST_GIVEUP_INFO *)arg;
                
                sts = usbOtgHostRoleGiveup(pOTG, pGiveupInfo->giveupWaitTime);
                }
            break;
        case USBOTG_IOCTL_VBUS_POWER_ON:
            sts = usbOtgVbusPowerOn(pOTG, (_Vx_ticks_t)((ULONG)arg));
            break;
        case USBOTG_IOCTL_VBUS_POWER_OFF:
            sts = usbOtgVbusPowerOff(pOTG, (_Vx_ticks_t)((ULONG)arg));
            break;
        case USBOTG_IOCTL_CALLBACK_INSTALL:
            if (arg != NULL)
                {
                USBOTG_USR_CALLBACK_INFO * pCallbackInfo;
                
                pCallbackInfo = (USBOTG_USR_CALLBACK_INFO *)arg;
                
                pOTG->pUsrCallback = pCallbackInfo->pUsrCallback;
                
                pOTG->pUsrContext = pCallbackInfo->pUsrContext;
                
                sts = OK;
                }
            break;
        case USBOTG_IOCTL_STACK_MODE_GET:
            *(USBOTG_MODE *)arg = pOTG->stackMode;
            sts = OK;
            break;
        default: break;
        }

    return sts;
    }

/*******************************************************************************
*
* usbOtgOcdAdd - add an USB OTG Controller Driver instance
*
* This routine is to add an USB OTG Controller Driver instance. A OTG Manager
* Framework instance will be created to work with the USB OTG Controller Driver
* instance.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*/

STATUS usbOtgOcdAdd
    (
    pUSBOTG_OCD pOCD
    )
    {
    STATUS sts;

    /* Check if the parameter is valid */

    if (!pOCD || !pOCD->pDev || !pOCD->pOcdFuncs || pOCD->pOTG)
        {
        USBOTG_ERR("Invalid parameter\n",
                   1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Try to initialize the OTG Framework if necessary */

    if (usbOtgInit() != OK)
        {
        USBOTG_ERR("usbOtgInit failed\n",
                   1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Once OTG Framework is initialized, usbOtgCtlrListLock is created */

    OS_WAIT_FOR_EVENT(usbOtgCtlrListLock, WAIT_FOREVER);

    /* Create the OTG Manager Framework instance */

    sts = usbOtgManagerCreate(pOCD);

    OS_RELEASE_EVENT(usbOtgCtlrListLock);

    return sts;
    }

/*******************************************************************************
*
* usbOtgOcdRemove - remove an USB OTG Controller Driver instance
*
* This routine is to remove an USB OTG Controller Driver instance. The OTG
* Manager Framework instance that was created to work with the USB OTG
* Controller Driver instance will be destroyed.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*/

STATUS usbOtgOcdRemove
    (
    pUSBOTG_OCD pOCD
    )
    {
    STATUS sts;

    /* Check if the parameter is valid */

    if (!pOCD || !pOCD->pDev || !pOCD->pOcdFuncs || !pOCD->pOTG)
        {
        USBOTG_ERR("Invalid parameter\n",
                   1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Once OTG Framework is initialized, usbOtgCtlrListLock is created */

    OS_WAIT_FOR_EVENT(usbOtgCtlrListLock, WAIT_FOREVER);

    /* Destroy the OTG Manager Framework instance */

    usbOtgManagerDestroy(pOCD);

    OS_RELEASE_EVENT(usbOtgCtlrListLock);

    /* Try to uninitialize the OTG Framework if necessary */

    return usbOtgUnInit();
    }

/*******************************************************************************
*
* usbOtgTgtDisable - disable the USB Target Stack
*
* This routine is to disable the USB Target Stack.
*
* NOTE: No parameter validity is checked since it is used internally and
* the calling routine should have assured the validity.
*
* RETURNS: OK when the USB Target Stack is disabled, ERROR if failed.
*
* ERRNO: N/A
*/

LOCAL STATUS usbOtgTgtDisable
    (
    pUSBOTG_MANAGER pOTG
    )
    {
    STATUS sts;

    sts = pOTG->pOcdFuncs->pOtgDisableTCD(pOTG->pOCD);

    return sts;
    }

/*******************************************************************************
*
* usbOtgHstDisable - disable the USB Host Stack
*
* This routine is to disable the USB Host Stack.
*
* NOTE: No parameter validity is checked since it is used internally and
* the calling routine should have assured the validity.
*
* RETURNS: OK when the USB Host Stack is disabled, ERROR if failed.
*
* ERRNO: N/A
*/

LOCAL STATUS usbOtgHstDisable
    (
    pUSBOTG_MANAGER pOTG
    )
    {
    STATUS sts;
    
    if (pOTG->hOtgDevice)
        {
        usbOtgNotifyConnectDisconnectDetected(pOTG, FALSE);
        }
    
    sts = pOTG->pOcdFuncs->pOtgDisableHCD(pOTG->pOCD);

    return sts;
    }

/*******************************************************************************
*
* usbOtgTgtEnable - enable the USB Target Stack
*
* This routine is to enable the USB Target Stack.
*
* NOTE: No parameter validity is checked since it is used internally and
* the calling routine should have assured the validity.
*
* RETURNS: OK when the USB Target Stack is enabled, ERROR if failed.
*
* ERRNO: N/A
*/

LOCAL STATUS usbOtgTgtEnable
    (
    pUSBOTG_MANAGER pOTG
    )
    {
    STATUS sts;

    sts = pOTG->pOcdFuncs->pOtgEnableTCD(pOTG->pOCD);

    return sts;
    }

/*******************************************************************************
*
* usbOtgHstEnable - enable the USB Host Stack
*
* This routine is to enable the USB Host Stack.
*
* NOTE: No parameter validity is checked since it is used internally and
* the calling routine should have assured the validity.
*
* RETURNS: OK when the USB Host Stack is enabled, ERROR if failed.
*
* ERRNO: N/A
*/

LOCAL STATUS usbOtgHstEnable
    (
    pUSBOTG_MANAGER pOTG
    )
    {
    STATUS sts;

    sts = pOTG->pOcdFuncs->pOtgEnableHCD(pOTG->pOCD);

    return sts;
    }

/*******************************************************************************
*
* usbOtgInit - initialize USB OTG Framework
*
* This routine is to initialize USB OTG Framework. This will check if we
* need to initialize the OTG Framework, and if so actually initialize it.
*
* RETURNS: OK if USB OTG Framework initialized successfully, ERROR if failed.
*
* ERRNO: N/A
*/

LOCAL STATUS usbOtgInit (void)
    {
    /* If the OTG Controller list is NULL, time to initialize things */

    if (vxAtomicSet(&usbOtgInitialized, 1) == 0)
        {
        /*
         * Create lock to avoid race conditions while accessing
         * global OTG Controller list
         */

        usbOtgCtlrListLock = OS_CREATE_EVENT(OS_EVENT_SIGNALED);

        if (usbOtgCtlrListLock == NULL)
            {
            USBOTG_ERR("Could not create usbOtgCtlrListLock\n",
                        1, 2, 3, 4, 5, 6);
            return ERROR;
            }

        /* Create the event database to use erfLib */

        if (usbOtgEventDbCreate() != OK)
            {
            USBOTG_ERR("usbOtgEventDbCreate failed\n",
                1, 2, 3, 4, 5, 6);

            OS_DESTROY_EVENT(usbOtgCtlrListLock);

            usbOtgCtlrListLock = NULL;

            return ERROR;
            }

        usbOtgDrvNum = iosDrvInstall (
                            (DRV_CREATE_PTR) NULL,/* No create routine */
                            (DRV_REMOVE_PTR) NULL,/* No delete routine */
                            (DRV_OPEN_PTR)  usbOtgManagerOpen,  /* Open routine */
                            (DRV_CLOSE_PTR) usbOtgManagerClose, /* Close routine */
                            (DRV_READ_PTR)  usbOtgManagerRead,  /* Read routine */
                            (DRV_WRITE_PTR) usbOtgManagerWrite, /* Write routine */
                            (DRV_IOCTL_PTR) usbOtgManagerIoctl); /* Ioctl routine */


        /* Check if we have enabled direct access module */

        if (!usbOtgDrvNum)
            {
            USBOTG_ERR("Direct Access not enabled\n",
                1, 2, 3, 4, 5, 6);

            OS_DESTROY_EVENT(usbOtgCtlrListLock);

            usbOtgCtlrListLock = NULL;

            return ERROR;
            }

        lstInit(&usbOtgCtlrList);

        usbOtgEventDataGetFunc = usbOtgEventDataGet;
        usbOtgEventDataPutFunc = usbOtgEventDataPut;
        
        usbOtgEventRaiseFunc = usbOtgEventRaise;
        }

    return OK;
    }

/*******************************************************************************
*
* usbOtgUnInit - uninitialize USB OTG Framework
*
* This routine is to uninitialize USB OTG Framework. This will check if we
* need to uninitialize the OTG Framework, and if so actually uninitialize it.
*
* RETURNS: OK if USB OTG Framework uninitialized successfully, ERROR if failed.
*
* ERRNO: N/A
*/

LOCAL STATUS usbOtgUnInit (void)
    {
    /*
     * If the OTG Controller list is NULL, no OTG Controller
     * could have been added.
     */

    if ((vxAtomicGet(&usbOtgInitialized) == 0) || usbOtgCtlrListLock == NULL)
        {
        USBOTG_ERR("OTG Framework not initialized\n",
                   1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    OS_WAIT_FOR_EVENT(usbOtgCtlrListLock, WAIT_FOREVER);

    /* If there is no OTG Controller instance, let's clean up! */

    if (lstCount(&usbOtgCtlrList) == 0)
        {
        /* Remove the OTG driver iosLib entry */
        
        iosDrvRemove(usbOtgDrvNum, TRUE);

        /* Set the OTG iosLib driver number as 0 */
        
        usbOtgDrvNum = 0;

        /* Destroy the event database */
        
        usbOtgEventDbDestroy();

        /* Destroy the OTG controller list lock */
        
        OS_DESTROY_EVENT(usbOtgCtlrListLock);

        /* Set the OTG controller list lock as NULL */
        
        usbOtgCtlrListLock = NULL;

        /* Clear the flag indicating not initialized */
        
        if (vxAtomicSet(&usbOtgInitialized, 0) == 1)
            {
            USBOTG_VDBG("usbOtgInitialized now cleared\n",
                       1, 2, 3, 4, 5, 6);
            }
        
        return OK;
        }
    
    OS_RELEASE_EVENT(usbOtgCtlrListLock);

    return OK;
    }

/*******************************************************************************
*
* usrUsbOtgVbusStateNameGet - convert OTG VBUS state into printable name string
*
* This routine is to convert the OTG VBUS state into printable name string.
*
* RETURNS: Pointer to the printable OTG VBUS state name string
*
* ERRNO: N/A
*/

char * usrUsbOtgVbusStateNameGet
    (
    USBOTG_VBUS_STATE state
    )
    {
    return (char *)usbOtgVbusName[state];
    }

/*******************************************************************************
*
* usrUsbOtgStateNameGet - convert OTG state into printable name string
*
* This routine is to convert the OTG state into printable name string.
*
* RETURNS: Pointer to the printable OTG state name string
*
* ERRNO: N/A
*/

char * usrUsbOtgStateNameGet
    (
    USBOTG_STATE state
    )
    {
    return (char *)usbOtgStateName[state];
    }

/*******************************************************************************
*
* usrUsbOtgTimerNameGet - convert the OTG state into printable name string
*
* This routine is to convert the OTG state into printable name string.
*
* RETURNS: Pointer to the printable OTG state name string
*
* ERRNO: N/A
*/

char * usrUsbOtgTimerNameGet
    (
    USBOTG_TIMER     timerId
    )
    {
    return (char *)usbOtgTimerName[timerId];
    }
    
/*******************************************************************************
*
* usrUsbOtgOpen - open an OTG controller
*
* This routine is to open an USB OTG controller. If the <otgName> is NULL or
* empty string, then it will use the first available OTG controller name as 
* the default name to open.
*
* Call it like below from the shell or user application.
*
* otgFd = usrUsbOtgOpen("/usbOtg0");
*
* RETURNS: Non-negative file descriptor if opened, or negative value if failed.
*
* ERRNO: N/A
*/

int usrUsbOtgOpen
    (
    char * otgName
    )
    {
    int otgFd = -1;
    char defaultName[USB_OTG_MGR_NAME_MAX_SIZE];
    
    if (otgName == NULL || strlen(otgName) == 0)
        {
        /* Construct a default name */
        snprintf(defaultName, 
                 USB_OTG_MGR_NAME_MAX_SIZE, 
                 "%s%d", usrUsbOtgMgrBaseNameGet(), 0);
        
        otgName = defaultName;
        
        USBOTG_DBG("Using default otgName %s\n",
            otgName, 2, 3, 4, 5, 6);
        }

    otgFd = open(otgName, O_RDWR, 0);

    if (otgFd <= 0)
        USBOTG_ERR("usrUsbOtgOpen - failed to open %s\n",
            otgName, 2, 3, 4, 5, 6);
    else
        USBOTG_DBG("usrUsbOtgOpen - OK to open %s\n",
            otgName, 2, 3, 4, 5, 6);

    return otgFd;
    }

/*******************************************************************************
*
* usrUsbOtgClose - close an OTG controller
*
* This routine is to close an USB OTG controller which has been opened before.
*
* Call it like below from the shell or user application.
*
* sts = usrUsbOtgClose(otgFd);
*
* RETURNS: OK file descriptor if closed, or ERROR value if failed.
*
* ERRNO: N/A
*/

STATUS usrUsbOtgClose
    (
    int otgFd
    )
    {
    if (otgFd <= 0)
        {
        USBOTG_ERR("usrUsbOtgClose - invalid file descriptor %d\n",
            otgFd, 2 ,3 ,4, 5, 6);
        
        return ERROR;
        }
    
    if (close(otgFd) == ERROR)
        {
        USBOTG_ERR("usrUsbOtgClose - close failed for otgFd %d\n",
            otgFd, 2, 3, 4, 5, 6);
        
        return ERROR;
        }
    else
        {
        USBOTG_DBG("usrUsbOtgClose - close OK for otgFd %d\n", 
            otgFd, 2, 3, 4, 5, 6);
        
        return OK;
        }
    }

/*******************************************************************************
*
* usrUsbOtgStateGet - get OTG state of an OTG controller
*
* This routine is to get OTG state of an USB OTG controller which has been
* opened before.
*
* Call it as below from the shell or user application.
*
* sts = usrUsbOtgStateGet(otgFd, &otgState);
*
* RETURNS: OK if the status is get successfully, ERROR otherwise; if it 
*          returns OK, then USBOTG_STATE is saved in the <'pOtgState'>.
*
* ERRNO: OK if the status is get successfully, ERROR otherwise
*/

STATUS usrUsbOtgStateGet
    (
    int             otgFd,
    USBOTG_STATE *  pOtgState
    )
    {
    USBOTG_STATE    otgState = USBOTG_STATE_unknown;
    STATUS          sts;

    if ((otgFd <= 0) || (pOtgState == NULL))
        {
        USBOTG_ERR("usrUsbOtgStateGet - invalid parameter,"
            "otgFd %d, pOtgState %p\n",
            otgFd, pOtgState, 3,4, 5, 6);
        
        return ERROR;
        }
    
    if ((sts = ioctl(otgFd, USBOTG_IOCTL_OTG_STATE_GET, &otgState)) != OK)
        {
        USBOTG_ERR("usrUsbOtgStateGet - failed for %d\n",
            otgFd, 2, 3, 4, 5, 6);

        otgState = USBOTG_STATE_unknown;
        }
    else
        {
        USBOTG_DBG("usrUsbOtgStateGet - otgFd %d state is now %s\n",
            otgFd, usrUsbOtgStateNameGet(otgState), 3, 4, 5, 6);
        }

    *pOtgState = otgState;

    return sts;
    }

/*******************************************************************************
*
* usrUsbOtgVbusStateGet - get VBUS state of an OTG controller
*
* This routine is to get OTG state of an USB OTG controller which has been
* opened before.
*
* Call it as below from the shell or user application.
*
* sts = usrUsbOtgVbusStateGet(otgFd, &vbusState);
*
* RETURNS: OK if the VBUS state is got successfully, ERROR otherwise; if it
*          returns OK, then the USBOTG_VBUS_STATE is saved in <'pVbusState'>.
*
* ERRNO: N/A
*/

STATUS usrUsbOtgVbusStateGet
    (
    int                 otgFd,
    USBOTG_VBUS_STATE * pVbusState
    )
    {
    USBOTG_VBUS_STATE   vbusState = USBOTG_VBUS_STATE_SessEnd;
    STATUS              sts;

    if ((otgFd <= 0) || (pVbusState == NULL))
        {
        USBOTG_ERR("usrUsbOtgVbusStateGet - invalid parameter,"
            "otgFd %d, pVbusState %p\n",
            otgFd, pVbusState, 3, 4, 5, 6);
        
        return ERROR;
        }

    if ((sts = ioctl(otgFd, USBOTG_IOCTL_VBUS_STATE_GET, &vbusState)) != OK)
        {
        USBOTG_DBG("usrUsbOtgVbusStateGet - failed for %d\n",
            otgFd, 2, 3, 4, 5, 6);
        
        vbusState = USBOTG_VBUS_STATE_SessEnd;
        }
    else
        {
        USBOTG_DBG("usrUsbOtgVbusStateGet - otgFd %d VBUS is now %s\n",
            otgFd, usrUsbOtgVbusStateNameGet(vbusState), 3, 4, 5, 6);
        }

    *pVbusState = vbusState;

    return sts;
    }

/*******************************************************************************
*
* usrUsbOtgDeviceTypeGet - get device type of an OTG controller
*
* This routine is to get device type of an USB OTG controller which has been
* opened before.
*
* Call it like below from the shell or user application.
*
* sts = usrUsbOtgDeviceTypeGet(otgFd, &devType);
*
* RETURNS: OK if the device type is got successfully, ERROR otherwise; if it
*          returns OK, then the USBOTG_DEVICE_TYPE is saved in <'pDeviceType'>.
*
* ERRNO: N/A
*/

STATUS usrUsbOtgDeviceTypeGet
    (
    int                     otgFd,
    USBOTG_DEVICE_TYPE *    pDeviceType
    )
    {
    USBOTG_DEVICE_TYPE      devType = USBOTG_A_DEVICE;
    STATUS                  sts;

    if ((otgFd <= 0) || (pDeviceType == NULL))
        {
        USBOTG_ERR("usrUsbOtgDeviceTypeGet - invalid parameter,"
            "otgFd %d, pDeviceType %p\n",
            otgFd, pDeviceType, 3, 4, 5, 6);
        
        return ERROR;
        }

    if ((sts = ioctl(otgFd, USBOTG_IOCTL_DEV_TYPE_GET, &devType)) != OK)
        {
        USBOTG_DBG("usrUsbOtgDeviceTypeGet - failed for otgFd %d\n",
            otgFd, 2, 3, 4, 5, 6);

        devType = USBOTG_A_DEVICE;
        }
    else
        {
        USBOTG_DBG("usrUsbOtgDeviceTypeGet - otgFd %d is now as %s\n",
            otgFd,
            (devType == USBOTG_A_DEVICE) ? "A-device" : "B-device", 3, 4, 5, 6);

        }

    /* Update the return status if it is not NULL */

    if (pDeviceType)
        *pDeviceType = devType;

    return sts;
    }

/*******************************************************************************
*
* usrUsbOtgStackModeGet - get stack mode of an OTG controller
*
* This routine is to get stack mode of an USB OTG controller which has been
* opened before.
*
* Call it like below from the shell or user application.
*
* sts = usrUsbOtgStackModeGet(otgFd, &stackMode);
*
* RETURNS: OK if the stack mode is got successfully, ERROR otherwise; if it
*          returns OK, then the USBOTG_MODE is saved in <'pStackMode'>.
*
* ERRNO: N/A
*/

STATUS usrUsbOtgStackModeGet
    (
    int             otgFd,
    USBOTG_MODE *   pStackMode
    )
    {
    STATUS          sts;
    USBOTG_MODE     stackMode = USBOTG_MODE_HOST;

    if ((otgFd <= 0) || (pStackMode == NULL))
        {
        USBOTG_ERR("usrUsbOtgStackModeGet - invalid parameter,"
            "otgFd%d, pStackMode %p\n",
            otgFd, pStackMode, 3, 4, 5, 6);
        
        return ERROR;
        }

    if ((sts = ioctl(otgFd, USBOTG_IOCTL_STACK_MODE_GET, &stackMode)) != OK)
        {
        USBOTG_DBG("usrUsbOtgStackModeGet - failed for otgFd %d\n",
            otgFd, 2, 3, 4, 5, 6);
       
        }
    else
        {
        USBOTG_DBG("usrUsbOtgStackModeGet - otgFd %d is now in stack mode %s\n",
            otgFd,
            (stackMode == USBOTG_MODE_HOST) ?
            "USBOTG_MODE_HOST" : "USBOTG_MODE_PERIPHERAL", 3, 4, 5, 6);

        }

    *pStackMode = stackMode;
    
    return sts;
    }

/*******************************************************************************
*
* usrUsbOtgHostRequest - request to work in host role for an USB OTG controller 
*
* This routine is to request to work in host role for an USB OTG controller.
*
* Call it like below from the shell or user application:
*
* sts = usrUsbOtgHostRequest(otgFd, reqType, waitTime);
*
* The <'reqType'> specifies the protocol to be used for the host request. 
* It has two possible values according to the latest USB OTG specification, 
* USBOTG_HOST_REQUEST_HNP_BASIC and USBOTG_HOST_REQUEST_HNP_POLLING.
*
* 1) USBOTG_HOST_REQUEST_HNP_BASIC - basic HNP method 
*
* This type of HNP has been introduced since the initial version of USB OTG 
* specification and is the core protocol used by USB OTG for role switching
* between two Dual-Role capable USB OTG devices.
*
* This protocol is initiated by the device in peripheral  role (e.g, a B-device
* in b_periperial state) by specifying that the applicaiton on it needs to 
* use the USB bus; With that requirement specified, the device in peripheral  
* role monitors the USB bus to be suspended by the host; Once such a suspend
* event is detected after the host request requirement is specified, it will
* disconnect from the peer host (e.g, a A-device in a_host state), and the 
* peer host will switch its role into peripheral role and then connects to 
* the current peripheral  role device, if that connection event is detected, 
* then it will switch its role from the peripheral role into host role, which 
* completes the role switching as defined by the basic HNP protocol.
*
* Note, however, there is no OTG defined contract between two connected OTG
* devices on when the current peripheral  role device has actually "requested" 
* the host role. That is to say, the current host role device has no knowledge  
* when it needs to "giveup" the host role by suspending the USB bus except 
* that the user application explicitly directs it to do so (for example, by 
* using usrUsbOtgHostGiveup()). If the peripheral role device does not request
* host role, a suspend from the host role device is not considered as a chance
* to switch roles between them. 
*
* This may present usability complication for the user. For example, with two
* OTG Dual-Role capable cell phones connected and you want to switch their
* roles, you may need to press one button on one phone (in peripheral role) 
* which initiates the host role request; Then you may need to press another 
* button on another phone (in host role) to actually starts the role switching 
* (which then suspends the USB bus).
*
* The above example might be simplified by application on the peripheral  role 
* device to initiate the host role request once the HNP feature is enabled by
* the host role device during the enumeration process. This way, the peripheral  
* role device could continue to work in peripheral  role until the host role  
* device gives up its host role, starts the HNP procedulre (suspending USB bus). 
* This makes it possible to just press the button on the phone which is in host
* role to switch the roles (as the host role request has already been made by 
* the application when the HNP feature is enabled). To do so, the application 
* could install an user message callback by usrUsbOtgCallbackInstall(), and in
* USBOTG_USR_MSG_ID_HNP_ENABLED handling code, call usrUsbOtgHostRequest()
* with USBOTG_HOST_REQUEST_HNP_BASIC to initiate the host role request.
*
* Note that the aforementioned way to simplify the usability may not suit all
* cases. For example, if you do not want to take any "bus suspend" as a trigger
* for role switching, then this method should not be used. A noted case when 
* using this method is that it may fail the USB OTG CV Test. In the USB OTG CV
* test, the test tool is installed on a standard PC system in which the host 
* controller is a standard, non-OTG capable host controller. The test would
* exercise the various OTG specific requests and commands, including the OTG
* specific SetFeature(b_hnp_enable) command even if the host does not actually
* support OTG/HNP. During the test, the test tool may also suspend the bus, and
* this creates a problem if the aforementioned way is used, which would cause
* the Device Under Test (DUT) to disconnect from the test tool host controller,
* because, the DUT "thought" it's a real HNP capable host giving up host role. 
* However, due to the fact that test tool host is not OTG capable, it would not 
* connect to the DUT according to the HNP protocol. The effect is that the DUT
* is considered as being "phsically disconnected" from the test tool host, thus
* the USB OTG CV Test would fail in this case. Therefore, this method may not
* suit for production usage, but it may be used to facilitate autoamtic HNP 
* testing during development.
*
* The complication is due to the fact that there is no OTG defined way (before
* the OTG 2.0 specification is released) for the peripheral role device to 
* inform the host role device that host role is requested by the peripheral 
* role device. 
*
* The OTG 2.0 specification adds, among other capabilities, a way for the 
* peripheral role device to inform the host role device that host role is 
* being requested by the peripheral role device. This is called "HNP polling"
* and is supported using the <'reqType'> of USBOTG_HOST_REQUEST_HNP_POLLING
* as described below.
*
* 2) USBOTG_HOST_REQUEST_HNP_POLLING - HNP polling method
*
* As described above, for basic HNP protocol, the lack of a way to inform
* host role device about when a peripheral device has "requested" host role,
* created some usbility complication. The OTG 2.0 specification has added a
* protocol called "HNP polling" which effectively addressed this problem. 
* Basically, the "HNP polling" is done through a control transfer, namely
* GetStatus(OTG status). When a OTG device is enumerated, the host side OTG 
* stack would issue GetStatus(OTG status) regularly, to check if the peripheral
* device has set the "host request flag" in the returned status. If the request
* succeeded, and the "host request flag" is set, then it knows the peripheral 
* role device is requesting host role and it can inform the user application
* on the host role device. The user applcation could then respond to the request
* by doing some cleanup work then giveup its host role by usrUsbOtgHostGiveup(). 
* If the request for GetStatus(OTG status) failed, it indicates the peripheral
* role device may not support "HNP polling" thus it has to resort to basic HNP 
* protocol. For the peripheral role device, if "HNP polling" is supported and
* a GetStatus(OTG status) is received, it returns "host request flag" telling
* the host role device about the current host request status (cleared as not 
* requested, set as requested).
*
* By using "HNP polling", the host side could automatically detect that the
* host role is being requested by the peripheral role device. Thus it is 
* possible to trigger automatic response, eliminating the need for user to 
* "press a button to start". If such automatic response is desired by the user,
* the application could install a user message callback, and in the message
* handling code for USBOTG_USR_MSG_ID_DEV_REQ_HOST, call usrUsbOtgHostGiveup().
* Note that calling usrUsbOtgHostGiveup() may trigger role switch, during the
* time the communication between the two device will be shut down by force.
* This might cause some risk for potential data loss. So it might be more 
* reasonable to perform some cleanup before calling usrUsbOtgHostGiveup().
*
* The OTG 2.0 specification also made clear that it might be risky to request
* host role using "HNP polling", since the host side may immdiately shut down 
* communicaton for role switching in response to "host request flag" being set.
* As a result, you will also get a USBOTG_USR_MSG_ID_REQ_HOST_RISKY user message
* if you called usrUsbOtgHostRequest() with USBOTG_HOST_REQUEST_HNP_POLLING. 
* The message is to warn the user of this potential risk, the application may
* disaplay this warnning message to the end user. But once this request is 
* issued, role switching may be on the way according to host side capability.
* Thus it is the applicaiton's responsibility to make sure any cleanup work has
* been done so that the risk has been eliminated.
*
* Note that "HNP polling" still needs the basic HNP protocol to be in effect.
* That is, requesting using USBOTG_HOST_REQUEST_HNP_POLLING naturally enables
* the USBOTG_HOST_REQUEST_HNP_BASIC to be in effect. Without basic HNP protocol,
* "HNP polling" would not work to actually switch the roles. "HNP polling" only 
* adds a way to let the host role device to decide when to start the basic HNP 
* protocol (by suspending USB bus). Without "HNP polling", the decision has to 
* be made by the user mannualy, for example, by "pressing a button". Also, the
* "HNP polling" will only work if both ends of the OTG connection supports it.
*
* Note also that "HNP polling" is only introduced in OTG 2.0 specification. 
* Although this OTG stack is designed with OTG 2.0 in mind, due to lack of real
* OTG 2.0 hardware for testing, the current OTG stack is configured to support
* OTG 1.3 (the latest OTG specification version as found before OTG 2.0). 
* However, since the "HNP polling" feature is pure software based protocol, no
* special need for hardware assist, and it addresses the usbility complication
* problem for basic HNP protocol without any backword compatibility issues, it 
* is believed that this feature could coexist within the current OTG stack. 
* With this insight, the USBOTG_HOST_REQUEST_HNP_POLLING related code is still
* included in the stack, thus the described usages for this <'reqType'> still
* apply for the current OTG stack configuration.
*
* The need for <'waitTime'> parameter is described below:
*
* Before the host role request is actually made, some prerequisite condition 
* must be met, for example, for a B-device to be able to request host role 
* from the A-device, the A-device should have enabled the B-device for HNP
* feature. The <'waitTime'> parameter is used by the implementation to control
* how to wait such prerequisite condition to be met. If <'waitTime'> is set as
* NO_WAIT (0), then it tries to check if the prerequisite condition can be met,
* if not, it will return ERROR; if <'waitTime'> is set as WAIT_FOREVER (-1U),
* then the prerequisite condition check will last forever until the condition 
* is met; if <'waitTime'> is set as other positive value, the prerequisite 
* condition check will be done at least with the specified amount of time before
* it retuerns ERROR if the condition can not be met; If prerequisite condition 
* check is passed, then the actual host role request will be made.
*
* Note that the host role switching procedure may be performed asynchronously
* with the calling task for host role request. So even if usrUsbOtgHostRequest()
* returns OK, it does not necessarily mean the calling device has become host 
* role. There are several factors which may affect if the role switching will 
* actually take place. For example, as noted in the descriptions above, the 
* role has to be given by the peer host role device with its own decision 
* whether to giveup its host role or not. In any cases, the calling task could
* monitor OTG state to become USBOTG_STATE_a_host or USBOTG_STATE_b_host by 
* usrUsbOtgStateGet().
*
* Along with role switching, there may be stack mode switching to happen. The
* user application may install a user message callback to receive the related
* user messages USBOTG_USR_MSG_ID_HOST_ACTIVE and USBOTG_USR_MSG_ID_TARGET_ACTIVE.
* 
* RETURNS: OK if the host role request has been made, ERROR otherwise.
*
* ERRNO: N/A
*
* SEE ALSO: usrUsbOtgHostGiveup(), usrUsbOtgStackModeGet(), usrUsbOtgStateGet()
*/

STATUS usrUsbOtgHostRequest
    (
    int                         otgFd,
    USBOTG_HOST_REQUEST_TYPE    reqType,
    _Vx_ticks_t                 waitTime
    )
    {
    STATUS                      sts;
    USBOTG_USR_HOST_REQ_INFO    hostReqInfo;
    
    if ((otgFd <= 0) || 
        ((reqType != USBOTG_HOST_REQUEST_HNP_BASIC) &&
         (reqType != USBOTG_HOST_REQUEST_HNP_POLLING)))
        {
        USBOTG_ERR("usrUsbOtgHostRequest - invalid parameter,"
            "otgFd %d, reqType %d\n",
            otgFd, reqType ,3 ,4, 5, 6);
        
        return ERROR;
        }

    hostReqInfo.reqType = reqType;
    hostReqInfo.reqWaitTime = waitTime;

    if ((sts = ioctl(otgFd, USBOTG_IOCTL_HOST_REQUEST, &hostReqInfo)) != OK)
        {
        USBOTG_ERR("usrUsbOtgHostRequest - failed for otgFd %d\n",
            otgFd, 2, 3, 4, 5, 6);

        }
    else
        {
        USBOTG_DBG("usrUsbOtgHostRequest - requested host role for otgFd %d\n",
            otgFd, 2, 3, 4, 5, 6);

        }

    return sts;
    }
    
/*******************************************************************************
*
* usrUsbOtgHostGiveup - request to giveup host role for an USB OTG controller 
*
* This routine is to request to giveup host role for an USB OTG controller 
* which has been opened before.
*
* Call it as below from the shell or user application.
*
* sts = usrUsbOtgHostGiveup(otgFd, waitTime);
*
* The need for <'waitTime'> parameter is described below:
*
* Before the host role give up request is actually made, some prerequisite  
* conditions must be met, for example, for a A-device to be able to giveup 
* host role, the A-device should have connected with an OTG/HNP capable 
* B-device and has enabled the B-device for HNP feature. The <'waitTime'> 
* parameter is used by the implementation to control how to wait such 
* prerequisite conditions to be met. If <'waitTime'> is set as NO_WAIT (0), 
* then it tries to check if the prerequisite condition can be met, if not, 
* it will return ERROR; if <'waitTime'> is set as WAIT_FOREVER (-1U),
* then the prerequisite condition check will last forever until the condition 
* is met; if <'waitTime'> is set as other positive value, the prerequisite 
* condition check will be done at least with the specified amount of time 
* before it retuerns ERROR if the condition can not be met; If prerequisite 
* condition check is passed, then the actual host role giveup request will
* be made.
*
* Note that the host role switching procedure may be performed asynchronously
* with the calling task for host role giveup. So even if usrUsbOtgHostGiveup()
* returns OK, it does not necessarily mean the calling device has become  
* peripheral role. The calling task could monitor the OTG state to become 
* USBOTG_STATE_a_peripheral or USBOTG_STATE_b_peripheral by usrUsbOtgStateGet().
*
* Along with role switching, there may be stack mode switching to happen. The
* user application may install a user message callback to receive the related
* user messages USBOTG_USR_MSG_ID_HOST_ACTIVE and USBOTG_USR_MSG_ID_TARGET_ACTIVE.
* 
* RETURNS: OK if the host role giveup request has been made, ERROR otherwise.
*
* ERRNO: N/A
*
* SEE ALSO: usrUsbOtgHostRequest(), usrUsbOtgStackModeGet(), usrUsbOtgStateGet()
*/

STATUS usrUsbOtgHostGiveup
    (
    int         otgFd,
    _Vx_ticks_t waitTime
    )
    {
    STATUS                      sts;
    USBOTG_USR_HOST_GIVEUP_INFO giveupInfo;
    
    if (otgFd <= 0)
        {
        USBOTG_ERR("usrUsbOtgHostGiveup - invalid parameter, otgFd %d\n",
            otgFd, 2, 3, 4, 5, 6);
        
        return ERROR;
        }
    
    giveupInfo.giveupWaitTime = waitTime;
    
    if ((sts = ioctl(otgFd, USBOTG_IOCTL_HOST_GIVEUP, &giveupInfo)) != OK)
        {
        USBOTG_ERR("usrUsbOtgHostGiveup - failed for otgFd %d\n",
            otgFd, 2 ,3 ,4, 5, 6);

        }
    else
        {
        USBOTG_DBG("usrUsbOtgHostGiveup - requested to giveup host role for otgFd %d\n",
            otgFd, 2 ,3 ,4, 5, 6);
        }

    return sts;
    }

/*******************************************************************************
*
* usrUsbOtgVbusPowerOn - request to power on VBUS for an OTG controller
*
* This routine is to request to power on VBUS for an USB OTG controller.
*
* For A-device, it requests the VBUS to be powered up (if not already) by the 
* OTG Controller Driver. 
* 
* For B-device, since VBUS is provided and controlled by A-device, if VBUS 
* is not currently powered up, then it requests the A-device to provide VBUS
* through the OTG SRP procedure. Depending on the A-device capability (whether
* it could detect SRP signal), the A-device may, or may not respond to SRP.
*
* The actual VBUS control may be done asynchronously to the calling task and 
* it may take some time for VBUS to transition to USBOTG_VBUS_STATE_VBusValid
* due to hardware effect. So after calling this routine, the application could 
* monitor VBUS to become USBOTG_VBUS_STATE_VBusValid by querying the VBUS state
* using usrUsbOtgVbusStateGet().
*
* A <'onTime'> parameter is required for this routine, which is used when the 
* OTG Controller is working as A-device in "a_wait_bcon" state to wait for 
* B-device connection. It is used to setup a timer once "a_wait_bcon" state
* is entered. If the timer expires before any device connection is detected, 
* then the VBUS will be powered off automatically. If there is any device 
* connection detected before the timer expires, then the timer is canceled
* and the OTG state transtion to "a_host" state in which VBUS will not be 
* powered off automatically (but application could power off VBUS by force).
* The value passed in could be WAIT_FOREVER (-1U), which means once it enters
* "a_wait_bcon" state, it does not power off VBUS even if there is no B-device
* connection is detected (as the case for normal USB host). Although the value 
* could be any other positive time in ticks, or even NO_WAIT (0), if the time
* is too short, the B-device connection might be lost because it poweres off
* VBUS too soon to make the connection event being recognized and processed. 
* For this reason, there is a minimum default time (TA_WAIT_BCON min) used to 
* prevent the aforementioned situation. If the value passed in is smaller than
* the minimum default time, then the minimum default time will take effect.
*
* RETURNS: OK when the VBUS power on request is signaled, ERROR otherwise.
*
* ERRNO: N/A
*
* SEE ALSO: usrUsbOtgVbusPowerOff(), usrUsbOtgVbusStateGet()
*/

STATUS usrUsbOtgVbusPowerOn
    (
    int         otgFd,
    _Vx_ticks_t onTime
    )
    {
    STATUS       sts;

    if (otgFd <= 0)
        {
        USBOTG_ERR("usrUsbOtgVbusPowerOn - invalid parameter, otgFd %d\n",
            otgFd, 2, 3, 4, 5, 6);
        
        return ERROR;
        }

    if ((sts = ioctl(otgFd, USBOTG_IOCTL_VBUS_POWER_ON, onTime)) != OK)
        {
        USBOTG_ERR("usrUsbOtgVbusPowerOn - failed for otgFd %d\n",
            otgFd, 2, 3, 4, 5, 6);

        }
    else
        {
        USBOTG_DBG("usrUsbOtgVbusPowerOn - requested to power on VBUS for otgFd %d\n",
            otgFd, 2, 3, 4, 5, 6);
        }

    return sts;
    }

/*******************************************************************************
*
* usrUsbOtgVbusPowerOff - request to power off VBUS for an OTG controller
*
* This routine is to request to power off VBUS by the USB OTG Controller Driver.
*
* For A-device, this will request the OTG Controller Driver to power off the
* VBUS. For B-device, since VBUS is provided and controlled by A-device, there
* is no OTG defined way to request VBUS to be powered off. So this routine is
* only supported when the OTG Controller is working as A-device. When the OTG
* Controller is working as B-device, calling this routine will have no effect
* and will return ERROR.
*
* The actual VBUS control may be done asynchronously to the calling task and 
* it may take some time for VBUS to transition to USBOTG_VBUS_STATE_SessEnd
* due to hardware effect. So after calling this routine, the application could 
* monitor VBUS to become USBOTG_VBUS_STATE_SessEnd by querying the VBUS state 
* using usrUsbOtgVbusStateGet().
*
* A <'offTime'> parameter is required for this routine and the reason for it
* is given below:
*
* USB host device connection detection and operation needs VBUS to be provided.
* In OTG environment, if the A-device VBUS is powered off, the B-device could
* request A-device to power up the VBUS through the SRP procedure. However,
* some OTG controllers may have trouble in detecting SRP signal from the
* B-device; Or the B-device might be not able to signal the SRP signal (for 
* example, a non-OTG capable B-device). In these cases, if the A-device VBUS
* is powered off, then it can not detect any device connection until the 
* user on the A-device has explictly powered up the VBUS. While it may help 
* to save some power when VBUS is powered off, this behaviour may decrease 
* user experiences since B-device connection may not be automatically detected
* when A-device VBUS is powered off as compared to basic USB host/device model.
*
* To provide better user experiences, the OTG stack provides an automatic
* VBUS power up control option. In this mode, after A-device VBUS is powered
* off, it will wait in the "a_idle" state for an amount of time as specified 
* by <'offTime'>; then it tries to power up the VBUS and see if there is new 
* device connection in "a_wait_bcon" state, if after another amount of time 
* as specified by <'onTime'> for the usrUsbOtgVbusPowerOn() routine in the 
* "a_wait_bcon" state there is no device connection, then it poweres off the 
* VBUS automatically. This effectively makes a loop in which VBUS transtions
* between on and off until a new device connection is detected, or an SRP 
* signal is detected.
*
* The <'offTime'> parameter can be NO_WAIT (0), WAIT_FOREVER (-1U), or any 
* other positive time in ticks. The value can be used to setup a timer, once 
* the timer expires, it signals the OTG Manager to power on the VBUS. If set
* to NO_WAIT, then the effect is that once the OTG state enters "a_idle" 
* state after VBUS goes off, it will immediately signal for VBUS to power up;
* if set to WAIT_FOREVER, then the timer will not expire, the effect is that
* the automatic VBUS power up is disabled, and the VBUS will only be powered 
* up explicitly by the A-device application, or in response to a B-device SRP.
*
* RETURNS: OK when the request is signaled for A-device, ERROR otherwise. 
*
* ERRNO: N/A
*
* SEE ALSO: usrUsbOtgVbusPowerOn(), usrUsbOtgVbusStateGet()
*/

STATUS usrUsbOtgVbusPowerOff
    (
    int         otgFd,
    _Vx_ticks_t offTime
    )
    {
    STATUS      sts;

    if (otgFd <= 0)
        {
        USBOTG_ERR("usrUsbOtgVbusPowerOn - invalid parameter, otgFd %d\n",
            otgFd, 2, 3, 4, 5, 6);
        
        return ERROR;
        }

    if ((sts = ioctl(otgFd, USBOTG_IOCTL_VBUS_POWER_OFF, offTime)) != OK)
        {
        USBOTG_DBG("usrUsbOtgVbusPowerOff - failed for otgFd %d\n",
            otgFd, 2, 3, 4, 5, 6);

        }
    else
        {
        USBOTG_DBG("usrUsbOtgVbusPowerOff - requested to power off VBUS for otgFd %d\n",
            otgFd, 2, 3, 4, 5, 6);
        }

    return sts;
    }

/*******************************************************************************
*
* usrUsbOtgCallbackInstall - install user message callback for an OTG controller
*
* This routine is to request to install usr message callback for an USB OTG 
* controller which has been opened before.
*
* Call it like below from the shell or user application:
*
* sts = usrUsbOtgCallbackInstall(otgFd, pUsrCallback, pUsrContext);
*
* The <'pUsrCallback'> is a pointer to a user application implemented user 
* message callback which takes the prototype as below:
*
* typedef VOID USBOTG_USR_CALLBACK 
*    (
*    void *  pUsrMsgBuff,    /@ User message buffer @/
*    void *  pUsrContext     /@ User context @/
*    );
*
* The <'pUsrMsg'> points to a message buffer area which could be casted to be
* of pointer to USBOTG_USR_MSG_HDR structure, which is of type as below:
*
* typedef struct usbotg_usr_msg_hdr
*    {
*    UINT32  uMsgId;     /@ Message ID @/
*    }USBOTG_USR_MSG_HDR, *pUSBOTG_USR_MSG_HDR;
*
* Based on the <'uMsgId'> in the USBOTG_USR_MSG_HDR structure, the <'pUsrMsg'> 
* could be further casted to specific message structure for the message ID. 
* The message IDs are defined by the following enum type:
*
* /@ Message ID to send to user application @/
*
* typedef enum usbotg_usr_msg_id
*    {
*    /@ 
*     * B-device SRP procedure is in progress, waiting 
*     * for A-device to raise VBUS. 
*     @/
*     
*    USBOTG_USR_MSG_ID_SRP_ACTIVE      = 1,
*
*    /@
*     * B-device SRP procdure failed, A-device does not 
*     * provide valid VBUS in time. 
*     @/
*     
*    USBOTG_USR_MSG_ID_SRP_FAIL        = 2,
*
*    /@ 
*     * A-device tried to raise the VBUS but failed to 
*     * make it into a valid VBUS in time.
*     @/
*     
*    USBOTG_USR_MSG_ID_VRISE_FAIL      = 3,
*
*    /@ The peer host does not support HNP @/
*    
*    USBOTG_USR_MSG_ID_HOST_NO_HNP     = 4,
*
*    /@ An device on the root hub is detected but no
*     * matching class driver is found. The messeage 
*     * has a paramter specifying the device handle
*     * for which the device is bound to.
*     @/
*     
*    USBOTG_USR_MSG_ID_DEV_UNSUPPORTED = 5,
*
*    /@ 
*     * HNP pooling detected that the peer device is 
*     * requesting host role.
*     @/
*     
*    USBOTG_USR_MSG_ID_DEV_REQ_HOST    = 6,
*
*    /@
*     * The application is requesting host role by specifying request
*     * type to be USBOTG_HOST_REQUEST_HNP_POLLING, which might cause 
*     * the HNP pooling mechanism to be effective when appropriate, 
*     * but that is considered to be risky since it may immediatly
*     * cease the host-device communication. 
*     @/
*     
*    USBOTG_USR_MSG_ID_REQ_HOST_RISKY  = 7,
*
*    /@ 
*     * The active stack mode has switched from target stack 
*     * to host stack.
*     @/
*    USBOTG_USR_MSG_ID_HOST_ACTIVE     = 8,
*
*    /@ 
*     * The active stack mode has switched from host stack 
*     * to target stack.
*     @/
*     
*    USBOTG_USR_MSG_ID_TARGET_ACTIVE   = 9,
*
*    /@ The device does not respond to requests made by the host @/
*    
*    USBOTG_USR_MSG_ID_DEV_UNRESPONDING = 10,
*
*    /@
*     * A-device detected that B-device is asking for VBUS 
*     * to be provided (SRP signaling is detected)
*     @/
*     
*    USBOTG_USR_MSG_ID_SRP_DETECTED = 11,
*
*    /@ 
*     * The peripheral role device received a SetFeature(b_hnp_enable)
*     * command from the host role device.
*     @/
*
*    USBOTG_USR_MSG_ID_HNP_ENABLED = 12
*    }USBOTG_USR_MSG_ID;
*
* The following data structures have been defined for each of the message IDs.
*
* typedef struct usbotg_usr_msg_srp_active
*     {
*     USBOTG_USR_MSG_HDR header;
*     }USBOTG_USR_MSG_SRP_ACTIVE, *pUSBOTG_USR_MSG_SRP_ACTIVE;
* 
* typedef struct usbotg_usr_msg_srp_fail
*     {
*     USBOTG_USR_MSG_HDR header;
*     }USBOTG_USR_MSG_SRP_FAIL, *pUSBOTG_USR_MSG_SRP_FAIL;
* 
* typedef struct usbotg_usr_msg_vrise_fail
*     {
*     USBOTG_USR_MSG_HDR header;
*     }USBOTG_USR_MSG_VRISE_FAIL, *pUSBOTG_USR_MSG_VRISE_FAIL;
* 
* typedef struct usbotg_usr_msg_host_no_hnp
*     {
*     USBOTG_USR_MSG_HDR header;
*     }USBOTG_USR_MSG_HOST_NO_HNP, *pUSBOTG_USR_MSG_HOST_NO_HNP;
* 
* typedef struct usbotg_usr_msg_dev_unsupported
*     {
*     USBOTG_USR_MSG_HDR header;
*     UINT32             hDevice;
*     }USBOTG_USR_MSG_DEV_UNSUPPORTED, *pUSBOTG_USR_MSG_DEV_UNSUPPORTED;
* 
* typedef struct usbotg_usr_msg_dev_req_host
*     {
*     USBOTG_USR_MSG_HDR header;
*     UINT32             hDevice;
*     }USBOTG_USR_MSG_DEV_REQ_HOST, *pUSBOTG_USR_MSG_DEV_REQ_HOST;
* 
* typedef struct usbotg_usr_msg_req_host_risky
*     {
*     USBOTG_USR_MSG_HDR header;
*     }USBOTG_USR_MSG_REQ_HOST_RISKY, *pUSBOTG_USR_MSG_REQ_HOST_RISKY;
* 
* typedef struct usbotg_usr_msg_host_active
*     {
*     USBOTG_USR_MSG_HDR header;
*     }USBOTG_USR_MSG_HOST_ACTIVE, *pUSBOTG_USR_MSG_HOST_ACTIVE;
* 
* typedef struct usbotg_usr_msg_target_active
*     {
*     USBOTG_USR_MSG_HDR header;
*     }USBOTG_USR_MSG_TARGET_ACTIVE, *pUSBOTG_USR_MSG_TARGET_ACTIVE;
* 
* typedef struct usbotg_usr_msg_dev_unresponding
*     {
*     USBOTG_USR_MSG_HDR header;
*     UINT32             hDevice;
*     }USBOTG_USR_MSG_DEV_UNRESPONDING, *pUSBOTG_USR_MSG_DEV_UNRESPONDING;
* 
* typedef struct usbotg_usr_msg_srp_detected
*     {
*     USBOTG_USR_MSG_HDR header;
*     }USBOTG_USR_MSG_SRP_DETECTED, *pUSBOTG_USR_MSG_SRP_DETECTED;
* 
* typedef struct usbotg_usr_msg_hnp_enabled
*     {
*     USBOTG_USR_MSG_HDR header;
*     }USBOTG_USR_MSG_HNP_ENABLED, *pUSBOTG_USR_MSG_HNP_ENABLED;
*
* Some of these message structures are only with the a single header element,
* while some others may have companion parameters further describing them.
* Especially, the <'hDevice'> element specifies the associated device handle
* when that message occurs. The application may use that device handle to get
* more information about the device and then take appropriate action. For an 
* example, if a message of ID USBOTG_USR_MSG_ID_DEV_REQ_HOST is received, the
* application may have a chance to decide whether to give up its host role to
* the requesting device.
*
* NOTE: The installed user message callback may be called in the context of
* some system management task, such as the tErfTask if the implementation uses
* erfLib to report such messages. With this insight, it is vital for the user
* application to not take too long to process just in the callback. If a long
* time processing is required, you may consider to record the message and then
* put the actual processing into an application specific task.
* 
* RETURNS: OK if the user message callback is installed, ERROR otherwise.
*
* ERRNO: N/A
*/

STATUS usrUsbOtgCallbackInstall
    (
    int                         otgFd,
    USBOTG_USR_CALLBACK *       pUsrCallback,
    void *                      pUsrContext
    )
    {
    STATUS                      sts;
    USBOTG_USR_CALLBACK_INFO    callbackInfo;

    if ((otgFd <= 0) || (pUsrCallback == NULL))
        {
        USBOTG_ERR("usrUsbOtgCallbackInstall - invalid parameter,"
            "otgFd %d, pUsrCallback %p\n",
            otgFd, pUsrCallback, 3, 4, 5, 6);
        
        return ERROR;
        }

    callbackInfo.pUsrCallback = pUsrCallback;
    callbackInfo.pUsrContext = pUsrContext;
    
    if ((sts = ioctl(otgFd, USBOTG_IOCTL_CALLBACK_INSTALL, &callbackInfo)) != OK)
        {
        USBOTG_ERR("usrUsbOtgCallbackInstall - failed for otgFd %d\n",
            otgFd, 2, 3, 4, 5, 6);

        }
    else
        {
        USBOTG_DBG("usrUsbOtgCallbackInstall - installed callback for otgFd %d\n",
            otgFd, 2, 3, 4, 5, 6);

        }

    return sts;
    }

#define _WRS_CONFIG_USB_OTG_SHOW
#ifdef _WRS_CONFIG_USB_OTG_SHOW

/*******************************************************************************
*
* usbOtgShow - show the internal information of the OTG Manager
*
* This routine is to show the internal information of the OTG Manager.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbOtgShow (void)
    {
    UINT32 count;
    UINT32 idx;
    NODE * pOtgNode;
    pUSBOTG_MANAGER pOTG;

    if (usbOtgCtlrListLock == NULL)
        return;
    
    /* Once OTG Framework is initialized, usbOtgCtlrListLock is created */

    OS_WAIT_FOR_EVENT(usbOtgCtlrListLock, WAIT_FOREVER);

    count = lstCount(&usbOtgCtlrList);
    
    printf("Total %d OTG controllers\n", count);
    
    for (idx = 0; idx < count; idx++)
        {
        pOtgNode = lstNth(&usbOtgCtlrList, idx + 1);
        
        if (!pOtgNode)
            continue;
        
        pOTG = USBOTG_NODE_TO_USBOTG_MANAGER(pOtgNode);

        printf("Showing OTG controller %d\n", idx);

        printf("pDev                %p\n", pOTG->pDev);
        printf("pOCD                %p\n", pOTG->pOCD);
        printf("devOpened           %ld\n", pOTG->devOpened);
        printf("isHcdLoaded         %d\n", pOTG->isHcdLoaded);
        printf("IsTcdLoaded         %d\n", pOTG->isTcdLoaded);
        printf("prevState           %s\n", usbOtgStateName[pOTG->prevState]);
        printf("currState           %s\n", usbOtgStateName[pOTG->currState]);
        printf("vbusState           %s\n", 
            usrUsbOtgVbusStateNameGet(pOTG->vbusState));
        printf("idState             %s\n", 
            (pOTG->idState == USBOTG_ID_HIGH) ? "HIGH" : "LOW");
        printf("stackMode           %s\n", 
            (pOTG->stackMode == USBOTG_MODE_HOST) ?
               "USBOTG_MODE_HOST" : "USBOTG_MODE_PERIPHERAL");
        printf("ctlrMode            %s\n", 
            (pOTG->pOCD->ctlrMode == USBOTG_MODE_HOST) ?
               "USBOTG_MODE_HOST" : "USBOTG_MODE_PERIPHERAL");
        printf("otgTimer            %s\n", usbOtgTimerName[pOTG->otgTimer]);
        printf("wdId                %p\n", pOTG->wdId);
        printf("wdInUse             0x%08lX\n", pOTG->wdInUse);
        printf("otgVersion          0x%02X\n", pOTG->otgVersion);
        printf("hOtgDevice          0x%X\n", pOTG->hOtgDevice);
        printf("pUsrCallback        %p\n", pOTG->pUsrCallback);
        printf("pUsrContext         %p\n", pOTG->pUsrContext);
        printf("mgrInterval         %d (ms)\n", pOTG->mgrInterval);
        printf("mgrTaskId           0x%08lX\n", (ULONG)pOTG->mgrTaskId);
        
        printf("param.input.a_bus_drop              %d\n", 
            pOTG->param.input.a_bus_drop);
        
        printf("param.input.a_bus_req               %d\n", 
            pOTG->param.input.a_bus_req);

        printf("param.input.a_bus_resume            %d\n", 
            pOTG->param.input.a_bus_resume);

        printf("param.input.a_bus_suspend           %d\n", 
            pOTG->param.input.a_bus_suspend);

        printf("param.input.a_conn                  %d\n", 
            pOTG->param.input.a_conn);

        printf("param.input.a_sess_vld              %d\n", 
            pOTG->param.input.a_sess_vld);

        printf("param.input.a_srp_det               %d\n", 
            pOTG->param.input.a_srp_det);

        printf("param.input.a_vbus_vld              %d\n", 
            pOTG->param.input.a_vbus_vld);

        printf("param.input.b_bus_req               %d\n", 
            pOTG->param.input.b_bus_req);

        printf("param.input.b_bus_resume            %d\n", 
            pOTG->param.input.b_bus_resume);

        printf("param.input.b_bus_suspend           %d\n", 
            pOTG->param.input.b_bus_suspend);

        printf("param.input.b_conn                  %d\n", 
            pOTG->param.input.b_conn);

        printf("param.input.b_se0_srp               %d\n", 
            pOTG->param.input.b_se0_srp);

        printf("param.input.b_sess_end              %d\n", 
            pOTG->param.input.b_sess_end);

        printf("param.input.b_ssend_srp             %d\n", 
            pOTG->param.input.b_ssend_srp);

        printf("param.input.b_sess_vld              %d\n", 
            pOTG->param.input.b_sess_vld);

        printf("param.input.id                      %d\n", 
            pOTG->param.input.id);

        printf("param.input.a_suspend_req           %d\n", 
            pOTG->param.input.a_suspend_req);

        printf("param.input.adp_change              %d\n", 
            pOTG->param.input.adp_change);

        printf("param.input.power_up                %d\n", 
            pOTG->param.input.power_up);

        printf("param.output.chrg_vbus              %d\n", 
            pOTG->param.output.chrg_vbus);

        printf("param.output.drv_vbus               %d\n", 
            pOTG->param.output.drv_vbus);

        printf("param.output.loc_conn               %d\n", 
            pOTG->param.output.loc_conn);

        printf("param.output.loc_sof                %d\n", 
            pOTG->param.output.loc_sof);
        
        printf("param.output.data_pulse             %d\n", 
            pOTG->param.output.data_pulse);

        printf("param.output.adp_prb                %d\n", 
            pOTG->param.output.adp_prb);

        printf("param.output.adp_sns                %d\n", 
            pOTG->param.output.adp_sns);

        printf("param.internal.a_set_b_hnp_en       %d\n", 
            pOTG->param.internal.a_set_b_hnp_en);

        printf("param.internal.b_srp_done           %d\n", 
            pOTG->param.internal.b_srp_done);

        printf("param.internal.b_hnp_en             %d\n", 
            pOTG->param.internal.b_hnp_en);

        printf("param.internal.a_clr_err            %d\n", 
            pOTG->param.internal.a_clr_err);

        printf("param.extend.b_a_hnp_support        %d\n", 
            pOTG->param.extend.b_a_hnp_support);

        printf("param.extend.b_a_alt_hnp_support    %d\n", 
            pOTG->param.extend.b_a_alt_hnp_support);

        printf("param.extend.host_request_flag      %d\n", 
            pOTG->param.extend.host_request_flag);

        printf("param.extend.a_hnp_accepted         %d\n", 
            pOTG->param.extend.a_hnp_accepted);

        printf("param.extend.b_srp_ready_tmout      %d\n", 
            pOTG->param.extend.b_srp_ready_tmout);

        printf("param.extend.b_srp_retry_count      %d\n", 
            pOTG->param.extend.b_srp_retry_count);

        printf("param.extend.rmt_bm_attr            0x%X (%s, %s, %s)\n", 
            pOTG->param.extend.rmt_bm_attr, 
            (pOTG->param.extend.rmt_bm_attr & USBOTG_ATTR_SRP_SUPPORT) ? 
            "SRP" : "NO SRP",
            (pOTG->param.extend.rmt_bm_attr & USBOTG_ATTR_HNP_SUPPORT) ? 
            "HNP" : "NO HNP",
            (pOTG->param.extend.rmt_bm_attr & USBOTG_ATTR_ADP_SUPPORT) ? 
            "ADP" : "NO ADP"
            );
        
        printf("param.extend.rmt_bcd_otg            0x%X\n", 
            pOTG->param.extend.rmt_bcd_otg);
        
        printf("param.extend.rmt_otg_sts            0x%X (%s)\n", 
            pOTG->param.extend.rmt_otg_sts, 
            (pOTG->param.extend.rmt_otg_sts & USBOTG_STS_HOST_REQ_FLAG) ? 
            "Requesting Host Role" : "Not Requesting Host Role");

        printf("param.timeout.a_wait_vrise_tmout    %d\n", 
            pOTG->param.timeout.a_wait_vrise_tmout);

        printf("param.timeout.a_wait_bcon_tmout     %d\n", 
            pOTG->param.timeout.a_wait_bcon_tmout);

        printf("param.timeout.a_aidl_bdis_tmout     %d\n", 
            pOTG->param.timeout.a_aidl_bdis_tmout);

        printf("param.timeout.b_ase0_brst_tmout     %d\n", 
            pOTG->param.timeout.b_ase0_brst_tmout);

        printf("param.timeout.a_wait_vfall_tmout    %d\n", 
            pOTG->param.timeout.a_wait_vfall_tmout);

        printf("param.timeout.a_bidl_adis_tmout     %d\n", 
            pOTG->param.timeout.a_bidl_adis_tmout);
        }
    
    OS_RELEASE_EVENT(usbOtgCtlrListLock);
    }

#endif /* _WRS_CONFIG_USB_OTG_SHOW */
