/* usbTgtMscUtil.c - USB Target Mass Storage Class Utility Module */

/*
 * Copyright (c) 2010, 2011, 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification History
--------------------
01j,06may13,s_z  Remove compiler warning (WIND00356717)
01i,13dec11,m_y  Modify according to code check result (WIND00319317)
01h,16aug11,m_y  remove useless variable
01g,08apr11,ghs  Fix code coverity issue(WIND00264893)
01f,25mar11,m_y  modify routine usbTgtMscEmptyBufGet
01e,23mar11,m_y  add usbTgtMscDevFind and usbTgtMscLunGetByName
01d,17mar11,m_y  modify code based on the code review and roll back the 01c
                 change
01c,03mar11,m_y  add usbTgtMscCBWValid
01b,28dec10,x_f  add more functions for util interface
01a,26sep10,m_y  written
*/


/*
DESCRIPTION

This module implements support routines to the mass storage driver.

INCLUDES: usbTgtMscDataStructure.h, usbTgtMscLib.h, usbTgtMscUtil.h
*/

/* includes */

#include <usbTgtMscDataStructure.h>
#include <usbTgtMscLib.h>
#include <usbTgtMscUtil.h>

/* externs */

IMPORT device_t devGetByName
    (
    char *deviceName
    );

/* Function Declartion */

/*******************************************************************************
*
* usbTgtMscLunGetByIndex - get LUN by lunIndex from LUN list
*
* This routine gets LUN by lunIndex from LUN list.
*
* RETURNS: The pointer of the USB_TGT_MSC_LUN structure, or NULL if something
* wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_TGT_MSC_LUN usbTgtMscLunGetByIndex
    (
    pUSB_TGT_MSC_DEV pUsbTgtMscDev,
    UINT32           lunIndex
    )
    {
    NODE *           pLunNode = NULL;
    pUSB_TGT_MSC_LUN pUsbTgtMscLun = NULL;

    if (pUsbTgtMscDev == NULL)
        return NULL;

    if (lunIndex > lstCount(&(pUsbTgtMscDev->lunList)))
        return NULL;

    pLunNode = lstNth(&(pUsbTgtMscDev->lunList), lunIndex + 1);

    pUsbTgtMscLun = LUN_NODE_TO_USB_TGT_MSC_LUN(pLunNode);

    return LUN_NODE_TO_USB_TGT_MSC_LUN(pLunNode);
    }

/*******************************************************************************
*
* usbTgtMscLunGetByName - get LUN by device name from LUN list
*
* This routine gets LUN by device name from LUN list.
*
* RETURNS: The pointer of the USB_TGT_MSC_LUN structure, or NULL if something
* wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_TGT_MSC_LUN usbTgtMscLunGetByName
    (
    pUSB_TGT_MSC_DEV pUsbTgtMscDev,
    char *           devName
    )
    {
    NODE *           pNode = NULL;
    pUSB_TGT_MSC_LUN pUsbTgtMscLun = NULL;

    if ((pUsbTgtMscDev == NULL) || (devName == NULL))
        return NULL;

    pNode = lstFirst(&pUsbTgtMscDev->lunList);

    while (pNode != NULL)
        {
        pUsbTgtMscLun = LUN_NODE_TO_USB_TGT_MSC_LUN(pNode);
        if (pUsbTgtMscLun != NULL)
            {
            if (strcmp(pUsbTgtMscLun->attachDevName, devName) == 0)
                return pUsbTgtMscLun;
            }
        pNode = lstNext(pNode);
        }

    return NULL;
    }

/*******************************************************************************
*
* usbTgtMscDevFind - find the USB target MSC device structure
*
* This routine finds the usb target msc device structure by name and unit number.
*
* RETURNS: The pointer of the USB target MSC device, or NULL if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_TGT_MSC_DEV usbTgtMscDevFind
    (
    char * tcdName,
    int    tcdUnit
    )
    {
    NODE *           pNode;
    pUSB_TGT_MSC_DEV pUsbTgtMscDev;

    if (tcdName == NULL)
        return NULL;

    semTake(g_usbTgtMscListEvent, WAIT_FOREVER);

    pNode = lstFirst(&g_usbTgtMscDevList);

    while (pNode != NULL)
        {
        pUsbTgtMscDev = DEV_NODE_TO_USB_TGT_MSC_DEV(pNode);
        if (pUsbTgtMscDev != NULL)
            {
            /* Compare the tcdUnit and the tcdName */

            if ((pUsbTgtMscDev->tcdUnit == tcdUnit)&&
                (strcmp(pUsbTgtMscDev->tcdName, tcdName) == 0))
                {
                semGive(g_usbTgtMscListEvent);
                return pUsbTgtMscDev;
                }
            }
        pNode = lstNext(pNode);
        }
    semGive(g_usbTgtMscListEvent);
    return NULL;
    }

/*******************************************************************************
*
* usbTgtMscEmptyBufGet - get pointer of the empty buf
*
* This routine gets pointer of the empty buf.
*
* RETURNS: The buffer pointer of the empty buf, or NULL if something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

UINT8 * usbTgtMscEmptyBufGet
    (
    pUSB_TGT_MSC_DEV pUsbTgtMscDev
    )
    {
    if (pUsbTgtMscDev == NULL)
        {
        USBTGT_MSC_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);
        return NULL;
        }
    if (pUsbTgtMscDev->uBufState == USB_TGT_MSC_BUF_BUSY)
        {
        USBTGT_MSC_ERR("usbTgtMscEmptyBufGet Buf busy\n", 1, 2, 3, 4, 5, 6);
        return NULL;
        }

    /* Reserve the buffer */

    USBTGT_MSC_BUF_RESERVE(pUsbTgtMscDev);
    return (pUsbTgtMscDev->pBuf);
    }

/*******************************************************************************
*
* usbTgtMscIsPartition - verify the device containing partition or not
*
* This routine verifies the device containing partition or not.
*
* RETURNS: TRUE, or FALSE
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOLEAN usbTgtMscIsPartition
    (
    char * deviceName
    )
    {
    char *p = NULL;
    int  index = 0;

    if (deviceName == NULL)
        return FALSE;

    /* Check if the name match the partition */

    if ((p = strchr(deviceName,':')) == NULL)
        return FALSE;

    /* Get the partition number */

    index = atoi((char *)(p + 1));

    if ((index > 0) && (index < 25))
        return TRUE;
    else
        return FALSE;
    }

/*******************************************************************************
*
* usbTgtMscXBDPartHandleGet - get XBD partition handle for the device
*
* This routine gets XBD partition handle for the device.
*
* RETURNS: XBD partition handle of the device
*
* ERRNO: N/A
*
* \NOMANUAL
*/

device_t usbTgtMscXBDPartHandleGet
    (
    char * deviceName
    )
    {
    devname_t devPartitionName;
    device_t  index;

    if (deviceName == NULL)
        return NULLDEV;

    if (usbTgtMscIsPartition(deviceName))
        {
        strncpy((char *)devPartitionName, deviceName, (MAX_DEVNAME - 1));

        if (strchr(devPartitionName,':') == NULL)
            {
            if (MAX_DEVNAME - strlen(devPartitionName) > 0)
                strncat(devPartitionName, ":0",
                        MAX_DEVNAME - strlen(devPartitionName));
            }

        index = devGetByName(devPartitionName);
        USBTGT_MSC_DBG("dev %s handler is 0x%x\n",
            devPartitionName, index, 3, 4, 5, 6);
        }
    else
        {
        index = devGetByName(deviceName);
        USBTGT_MSC_DBG("dev %s handler is 0x%x\n",
            deviceName, index, 3, 4, 5, 6);
        }

    return index;
    }

/*******************************************************************************
*
* usbTgtMscBulkErpInit - initialize the bulk ERP
*
* This routine initializes the Bulk ERP.
*
* RETURNS: OK, or ERROR if unable to submit ERP.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbTgtMscBulkErpInit
    (
    pUSB_TGT_MSC_DEV pUsbTgtMscDev,/* pointer to usbTgtMscDev */
    UINT8 *          pData,        /* pointer to data */
    UINT32           size,         /* size of data */
    UINT32           pid,          /* pid of the data */
    ERP_CALLBACK     erpCallback   /* erp callback */
    )
    {
    pUSB_ERP pErp;

    if ((pUsbTgtMscDev == NULL) || (pData == NULL))
        {
        USBTGT_MSC_ERR("usbTgtMscBulkErpInit - pUsbTgtMscDev or pData is NULL\n",
                       1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    if ((pid != USB_PID_IN) && (pid != USB_PID_OUT))
        return ERROR;

    if (pid == USB_PID_IN)
        {
        if (pUsbTgtMscDev->bulkInErpState == USB_TGT_MSC_ERP_BUSY)
            {
            USBTGT_MSC_ERR("usbTgtMscBulkErpInit - bulkInErpState is busy\n",
                           1, 2, 3, 4, 5, 6);
            return ERROR;
            }
        pErp  = &pUsbTgtMscDev->bulkInErp;
        }
    else
        {
        if (pUsbTgtMscDev->bulkOutErpState == USB_TGT_MSC_ERP_BUSY)
            {
            USBTGT_MSC_ERR("usbTgtMscBulkErpInit - bulkOutErpState is busy\n",
                           1, 2, 3, 4, 5, 6);
            return ERROR;
            }
        pErp  = &pUsbTgtMscDev->bulkOutErp;
        }


    memset (pErp, 0, sizeof (USB_ERP));
    pErp->erpLen = sizeof (USB_ERP);
    pErp->targCallback = erpCallback;
    pErp->bfrCount = 1;
    pErp->bfrList[0].pBfr = pData;
    pErp->bfrList[0].bfrLen = size;
    pErp->bfrList[0].pid = (UINT16)pid;
    pErp->userPtr = pUsbTgtMscDev;


    if (pid == USB_PID_IN)
        {
        pUsbTgtMscDev->bulkInErpState = USB_TGT_MSC_ERP_BUSY;

        if (usbTgtSubmitErp (pUsbTgtMscDev->bulkInPipe,
                             pErp) != OK)
            {
            pUsbTgtMscDev->bulkInErpState = USB_TGT_MSC_ERP_IDLE;
            USBTGT_MSC_ERR("usbTgtMscBulkErpInit - "
                           "submit bulk in erp fail\n",
                           1, 2, 3, 4, 5, 6);
            return ERROR;
            }
        }
    else
        {
        pUsbTgtMscDev->bulkOutErpState = USB_TGT_MSC_ERP_BUSY;

        if (usbTgtSubmitErp (pUsbTgtMscDev->bulkOutPipe,
                             pErp) != OK)
            {
            pUsbTgtMscDev->bulkOutErpState = USB_TGT_MSC_ERP_IDLE;
            USBTGT_MSC_ERR("usbTgtMscBulkErpInit - "
                           "submit bulk out erp fail\n",
                           1, 2, 3, 4, 5, 6);
            return ERROR;
            }
        }


    return OK;
    }

/*******************************************************************************
*
* usbTgtMscDevBioDone - give semaphore for bio caller
*
* This routine gives semaphore for bio caller.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbTgtMscDevBioDone
    (
    struct bio * pBio
    )
    {
    USBTGT_MSC_VDBG("usbTgtMscDevBioDone - semGive\n",
                    1, 2, 3, 4, 5, 6);

    if (pBio->bio_caller1)
        semGive ((SEM_ID) pBio->bio_caller1);
    return ;
    }
