/* usbTgtMediumEnd.h - Definitions of USB END Medium Interface for USB Network Function */

/*
 * Copyright (c) 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
Modification history
--------------------
01b,22mar11,s_z  Code clean up based on the code review
01a,04jan11,s_z  written
*/

/*
DESCRIPTION

This file includes the definitions of USB medium END agent module.

*/

#ifndef INCusbTgtMediumEndh
#define INCusbTgtMediumEndh

#include <etherMultiLib.h>
#include <endLib.h>
#include <usbTgtNetMediumBinder.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct usbtgt_medium_end
    {
    NODE      mediumEndNode;
    void *    pBinder;
    END_OBJ * pEndObj;
    char *    pEndName;
    int       endUnit;
    UINT8     uMediumType;    
    STATUS (* dataToMediumRtn) (); /* Store the send data to medium routine */
    STATUS (* oldReceiveRtn) ();   /* Store the old receive routine of END OBJ */
    }USBTGT_MEDIUM_END,*pUSBTGT_MEDIUM_END;

IMPORT STATUS usbTgtMediumEndDetach
    (
    void * pMediumDev
    );

IMPORT STATUS usbTgtMediumEndAttach
    (
    pUSBTGT_BINDER pBinder,
    char *         pMediumName,   
    int            mediumUnit,    
    UINT8          uMediumType,   
    void **        ppMediumDev   
    );


#ifdef __cplusplus
}
#endif

#endif /* INCusbTgtMediumEndh */

