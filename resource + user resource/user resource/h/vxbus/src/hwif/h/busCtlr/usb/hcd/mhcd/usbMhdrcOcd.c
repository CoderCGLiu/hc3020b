/* usbMhdrcOcd.c - USB OTG Controller Driver for MUSBMHDRC */

/*
 * Copyright (c) 2010, 2011, 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01k,03may13,wyy  Remove compiler warning (WIND00356717)
01j,24nov11,m_y  Remove unused variables
01i,22apr11,w_x  Workaround missing DISCONNECT during long time HNP
01h,08apr11,w_x  Clean up MUSBMHDRC platform definitions
01g,30mar11,w_x  Correct stack uninitialization with OCD (WIND00262787)
01f,28mar11,w_x  Correct spinlock usage on interrupt report (WIND00262862)
01e,23mar11,w_x  Address code review comments
01d,15mar11,w_x  Move BSP specific init to platform init (WIND00258032)
01c,08mar11,w_x  Report connected device to HCD when enabling HCD
01b,17nov10,m_y  Add TCD related code
01a,30may10,w_x  written
*/

/*
DESCRIPTION

This file is the USB OTG Conotroller Driver (OCD) for MUSBMHDRC which is
an USB HS Dual-Role Capable OTG Controller. This driver provides interfaces
to the VxWorks OTG Manager Framework, as well as providing common ISR handling
for both the Host and Target Controller Drivers when this USB Contorller is
working as OTG mode. Pure Host or Pure Target mode ISR are handled by the Host
or Target Controller Driver itself.

INCLUDE FILES: vxWorks.h
               hwif/util/vxbParamSys.h hwif/vxbus/vxBus.h
               hwif/vxbus/vxbPciLib.h hwif/vxbus/hwConf.h
               usb/usbOtg.h usb/usbPhyUlpi.h usbMhdrc.h
               usbMhdrcPlatform.h
*/

#include <vxWorks.h>
#include <hwif/util/vxbParamSys.h>
#include <hwif/vxbus/vxBus.h>
#include <hwif/vxbus/vxbPciLib.h>
#include <hwif/vxbus/hwConf.h>
#include <usb/usbOtg.h>
#include <usb/usbPhyUlpi.h>
#include "usbMhdrc.h"
#include "usbMhdrcPlatform.h"

/* defines */

#define PCI_ID_MATCH_MHDRC(pDev)              FALSE

/* locals */

LOCAL VOID usbMhdrcOcdPlbInit
    (
    VXB_DEVICE_ID pDev
    );

LOCAL VOID usbMhdrcOcdDeviceConnect
    (
    VXB_DEVICE_ID   pDev
    );

LOCAL STATUS usbMhdrcOcdDeviceRemove
    (
    VXB_DEVICE_ID   pDev
    );

BOOL usbMhdrcOcdDevicePciProbe
    (
    VXB_DEVICE_ID       pDev
    );

BOOL  usbMhdrcOcdRegistered = FALSE;

LOCAL PCI_DEVVEND       usbVxbPciMhdrcOcdDevVendId[1] =
    {
    {0xffff,             /* device ID */
    0xffff}              /* vendor ID */
    };

LOCAL DRIVER_METHOD     usbVxbMhdrcOcdMethods[2] =
    {
    DEVMETHOD(vxbDrvUnlink, usbMhdrcOcdDeviceRemove),
    { 0, NULL }
    };

LOCAL struct drvBusFuncs usbVxbMhdrcOcdPlbDriverFuncs =
    {
    usbMhdrcOcdPlbInit,                         /* init 1 */
    NULL,                                       /* init 2 */
    usbMhdrcOcdDeviceConnect                    /* device connect */
    };

LOCAL struct drvBusFuncs usbVxbMhdrcOcdPciDriverFuncs =
    {
    NULL,                                       /* init 1 */
    NULL,                                       /* init 2 */
    usbMhdrcOcdDeviceConnect                    /* device connect */
    };

/* Default PLB MUSBMHDRC parameters */

LOCAL VXB_PARAMETERS usbVxbPlbMhdrcOcdDevParamDefaults[] =
    {
    {NULL, VXB_PARAM_END_OF_LIST, {NULL}}
    };

LOCAL DRIVER_REGISTRATION       usbVxbPlbMhdrcOcdDevRegistration =
    {
    NULL,                                   /* register next driver */
    VXB_DEVID_BUSCTRL,                      /* bus controller */
    VXB_BUSID_PLB,                          /* bus id - PLB Bus Type */
    USB_VXB_VERSIONID,                      /* vxBus version Id */
    "vxbPlbUsbMhdrcOcd",                    /* drv name */
    &usbVxbMhdrcOcdPlbDriverFuncs,          /* pDrvBusFuncs */
    &usbVxbMhdrcOcdMethods[0],              /* pMethods */
    NULL,                                   /* probe routine */
    usbVxbPlbMhdrcOcdDevParamDefaults       /* vxbParams */
    };

/* Default PCI MUSBMHDRC parameters */

LOCAL VXB_PARAMETERS usbVxbPciMhdrcOcdDevParamDefaults[] =
    {
    {NULL, VXB_PARAM_END_OF_LIST, {NULL}}
    };

LOCAL PCI_DRIVER_REGISTRATION   usbVxbPciMhdrcOcdDevRegistration =
    {
        {
        NULL,                                   /* register next driver */
        VXB_DEVID_BUSCTRL,                      /* bus controller */
        VXB_BUSID_PCI,                          /* bus id - PCI Bus Type */
        USB_VXB_VERSIONID,                      /* vxBus version Id */
        "vxbPciUsbMhdrcOcd",                    /* drv name */
        &usbVxbMhdrcOcdPciDriverFuncs,          /* pDrvBusFuncs */
        &usbVxbMhdrcOcdMethods[0],              /* pMethods */
        usbMhdrcOcdDevicePciProbe,              /* probe routine */
        usbVxbPciMhdrcOcdDevParamDefaults       /* vxbParams */
        },
        NELEMENTS(usbVxbPciMhdrcOcdDevVendId),  /* idListLen */
        & usbVxbPciMhdrcOcdDevVendId [0]        /* idList */
    };

/*
 * This will be hooked with actual functions when the OCD is registered.
 * This could be statically assigned with elements, but it the static
 * initialization must match exactly with the USBOTG_OCD_FUNCS structure
 * defintion.
 */

LOCAL USBOTG_OCD_FUNCS usbMhdrcOcdOcdFuncs;

/* externs */

IMPORT STATUS usbMhdrcHcdInstanceRemove
    (
    pUSB_MUSBMHDRC      pMHDRC
    );

IMPORT STATUS usbMhdrcHcdInstanceAdd
    (
    pUSB_MUSBMHDRC      pMHDRC
    );

IMPORT VOID usbMhdrcReportHostInterrupts
    (
    pUSB_MUSBMHDRC      pMHDRC
    );

IMPORT VOID usbMhdrcReportTargetInterrupts
    (
    pUSB_MUSBMHDRC      pMHDRC
    );

/*******************************************************************************
*
* usbMhdrcOcdGetIdState - get the ID state of OTG port
*
* This routine is to get the ID state of OTG port.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*/

STATUS usbMhdrcOcdGetIdState
    (
    pUSBOTG_OCD         pOCD,
    USBOTG_ID_STATE *   pIdState
    )
    {
    pUSB_MUSBMHDRC  pMHDRC;
    UINT8           uReg8;

    if ((pOCD == NULL) ||
        (pOCD->pOTG == NULL) ||
        (pOCD->pOcdSpecific == NULL) ||
        (pOCD->pOTG->uValidMagic != USBOTG_MAGIC_VALID))
        {
        USB_MHDRC_ERR("Invalid parameter\n",
                   1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Cast into platform specific data */

    pMHDRC = (pUSB_MUSBMHDRC)pOCD->pOcdSpecific;

    /*
     * Assume BSP provided hook is more accurate than built-in
     * method for ID pin state detection
     */

    if (pMHDRC->pPhyIdPinStateGet)
        {
        if (pMHDRC->pPhyIdPinStateGet() == FALSE)
            *pIdState = USBOTG_ID_LOW;
        else
            *pIdState = USBOTG_ID_HIGH;

        USB_MHDRC_DBG("usbMhdrcOcdGetIdState - BSP USBOTG_ID_STATE %d\n",
                   *pIdState, 2, 3, 4, 5, 6);

        return OK;
        }

/*
 * Detecting actual ID pin state is one of the most crucial parts in OTG.
 *
 * However the MUSBMHDRC is quite annoying in its way of detecting ID
 * pin state. From register level, the direct way to detect the ID pin
 * state is the BDEVICE bit in the DEVCTL register. But it is not very
 * consistent - if you have a Mini-A plug inserted in the port, you would
 * expect the bit to refelct A-device (ID pin LOW) state, but if you
 * have not set the SESSION bit to drive the VBUS bits to VbusValid,
 * then you may get that state as B-device (ID pin high) (although you
 * have made sure the ID pin have been pulled high when initializing
 * the controller!). This will cause problems while in the a_idle state,
 * once ID is incorrectly detected to be high, then the OTG state would
 * be moved to b_idle state, and then SRP would be requested, which causes
 * SESSION bit to be set, which causes VBUS to have a transition, and then
 * the BDEVICE state would change to 0 (back to A-device) state, and then
 * the state would go back to a_idle...things go on...
 *
 * So you may want to use ULPI to get that state correctly! However,
 * even ULPI sometimes gives you incorrect result - if there is no Mini-A
 * plug inserted in the port, using ULPI, sometimes the ID pin is still
 * alternating between 0 and 1. This has been seen at least on the OMAOP3530
 * EVM (Mistral REVG). In other cases, ULPI seems more consistent than DEVCTL.
 *
 * Although ULPI seems better, there are still some drawbacks:
 *
 * - Some MUSBMHDRC enabled SOCs do not provide any way to access ULPI directly.
 * In this case, you have to resort to DEVCTL, playing with the beast!
 * - ULPI access sometimes would still fail, with timeout completing access.
 */

#ifndef USB_MHDRC_USE_DEVCTL_ID_DETECT

    /* Read the DEVCTL register */

    uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_DEVCTL);

    /* If SESSION is not active, or VBUS is not Valid, favor ULPI */

    if (!(uReg8 & USB_MHDRC_DEVCTL_SESSION) ||
         ((uReg8 & USB_MHDRC_DEVCTL_VBUS) != USB_MHDRC_DEVCTL_VBUS))
        {
        /* Make sure the ID pull-up is set */

        usbMhdrcUlpiWrite(pMHDRC, USB_ULPI_OTG_CTL + USB_ULPI_SET_ADJ,
                          ULPI_OTG_CTL_ID_PULL_UP);

        /* If ULPI access is OK, then use ULPI value */

        if (usbMhdrcUlpiRead(pMHDRC, USB_ULPI_INTR_STATUS, &uReg8) != OK)
            {
            /* ULPI access fail, resort to DEVCTL */

            /* Read the DEVCTL register */

            uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_DEVCTL);

            *pIdState = (uReg8 & USB_MHDRC_DEVCTL_BDEVICE) ?
                    USBOTG_ID_HIGH : USBOTG_ID_LOW;

            USB_MHDRC_DBG("usbMhdrcOcdGetIdState - DEVCTL USBOTG_ID_STATE %d\n",
                       *pIdState, 2, 3, 4, 5, 6);
            }
        else
            {
            *pIdState = (uReg8 & ULPI_INTR_IdGnd) ?
                    USBOTG_ID_HIGH : USBOTG_ID_LOW;

            USB_MHDRC_DBG("usbMhdrcOcdGetIdState - ULPI USBOTG_ID_STATE %d\n",
                       *pIdState, 2, 3, 4, 5, 6);
            }
        }
    else
        {
        *pIdState = (uReg8 & USB_MHDRC_DEVCTL_BDEVICE) ?
                USBOTG_ID_HIGH : USBOTG_ID_LOW;

        USB_MHDRC_DBG("usbMhdrcOcdGetIdState - DEVCTL USBOTG_ID_STATE %d (SESSION active)\n",
                   *pIdState, 2, 3, 4, 5, 6);
        }

#else /* USB_MHDRC_USE_DEVCTL_ID_DETECT */

    /* Read the DEVCTL register */

    uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_DEVCTL);

    *pIdState = (uReg8 & USB_MHDRC_DEVCTL_BDEVICE) ?
            USBOTG_ID_HIGH : USBOTG_ID_LOW;

    USB_MHDRC_DBG("usbMhdrcOcdGetIdState - DEVCTL USBOTG_ID_STATE %d\n",
               *pIdState, 2, 3, 4, 5, 6);

#endif /* USB_MHDRC_USE_DEVCTL_ID_DETECT */

    return OK;
    }

/*******************************************************************************
*
* usbMhdrcOcdGetVbusState - get the VBUS state of OTG port
*
* This routine is to get the VBUS state of OTG port.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*/

STATUS usbMhdrcOcdGetVbusState
    (
    pUSBOTG_OCD         pOCD,
    USBOTG_VBUS_STATE * pVbusState
    )
    {
    pUSB_MUSBMHDRC  pMHDRC;
    UINT8           uReg8;

    if ((pOCD == NULL) ||
        (pOCD->pOTG == NULL) ||
        (pOCD->pOcdSpecific == NULL) ||
        (pOCD->pOTG->uValidMagic != USBOTG_MAGIC_VALID))
        {
        USB_MHDRC_ERR("Invalid parameter\n",
                   1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Cast into platform specific data */

    pMHDRC = (pUSB_MUSBMHDRC)pOCD->pOcdSpecific;

    /* Assume BSP provided VBUS state detection is more accurate */

    if (pMHDRC->pPhyVbusPinStateGet)
        {
        USBOTG_ID_STATE idState = USBOTG_ID_LOW;

        if ((usbMhdrcOcdGetIdState(pOCD, &idState) == OK) &&
            (idState == USBOTG_ID_HIGH))
            {
            if (pMHDRC->pPhyVbusPinStateGet() == TRUE)
                {
                /* Above VBusValid */

                *pVbusState = USBOTG_VBUS_STATE_VBusValid;
                }
            else
                {
                /* Below Session End */

                *pVbusState = USBOTG_VBUS_STATE_SessEnd;
                }

            USB_MHDRC_DBG("usbMhdrcOcdGetVbusState - BSP USBOTG_VBUS_STATE %d\n",
                       *pVbusState, 2, 3, 4, 5, 6);

            return OK;
            }
        }

#ifndef USB_MHDRC_USE_DEVCTL_VBUS_DETECT

    if (usbMhdrcUlpiRead(pMHDRC, USB_ULPI_INTR_STATUS, &uReg8) == OK)
        {
        if (uReg8 & ULPI_INTR_SessEnd)
            {
            /* Below Session End */

            *pVbusState = USBOTG_VBUS_STATE_SessEnd;
            }
        else if (uReg8 & ULPI_INTR_VbusValid)
            {
            /* Above VBusValid */

            *pVbusState = USBOTG_VBUS_STATE_VBusValid;
            }
        else /* if (uReg8 & ULPI_INTR_SessValid) */
            {
            /*
             * 1 - Above Session End, below AValid
             * 2 - Above AValid, below VBusValid
             */

            *pVbusState = USBOTG_VBUS_STATE_SessValid;
            }

        USB_MHDRC_DBG("usbMhdrcOcdGetVbusState - ULPI USBOTG_VBUS_STATE %d\n",
                   *pVbusState, 2, 3, 4, 5, 6);
        }
    else
#endif  /* USB_MHDRC_USE_DEVCTL_VBUS_DETECT */
        {
        /* Read the DEVCTL register */

        uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_DEVCTL);

        /* Get the 2 bits encoding for current VBUS levels */

        uReg8 = (UINT8)((uReg8 & USB_MHDRC_DEVCTL_VBUS) >> 3);

        /* Cast into abstracted VBUS state */

        if (uReg8 == 0)
            {
            /* Below Session End */

            *pVbusState = USBOTG_VBUS_STATE_SessEnd;
            }
        else if (uReg8 == 3)
            {
            /* Above VBusValid */

            *pVbusState = USBOTG_VBUS_STATE_VBusValid;
            }
        else /* if ((uReg8 == 1)  || (uReg8 == 2)) */
            {
            /*
             * 1 - Above Session End, below AValid
             * 2 - Above AValid, below VBusValid
             */

            *pVbusState = USBOTG_VBUS_STATE_SessValid;
            }

        USB_MHDRC_DBG("usbMhdrcOcdGetVbusState - DEVCTL USBOTG_VBUS_STATE %d\n",
                   *pVbusState, 2, 3, 4, 5, 6);
        }

    return OK;
    }

/*******************************************************************************
*
* usbMhdrcOcdIsLineStateSE0 - check if the current D+/D- line state is SE0
*
* This routine is to check if the current D+/D- line state is SE0.
*
* RETURNS: TRUE if the line state is SE0, FALSE if not.
*
* ERRNO: N/A
*/

BOOL usbMhdrcOcdIsLineStateSE0
    (
    pUSBOTG_OCD         pOCD
    )
    {
    pUSB_MUSBMHDRC  pMHDRC;
    UINT8           uReg8;

    if ((pOCD == NULL) ||
        (pOCD->pOTG == NULL) ||
        (pOCD->pOcdSpecific == NULL) ||
        (pOCD->pOTG->uValidMagic != USBOTG_MAGIC_VALID))
        {
        USB_MHDRC_ERR("Invalid parameter\n",
                   1, 2, 3, 4, 5, 6);

        return FALSE;
        }

    /* Cast into platform specific data */

    pMHDRC = (pUSB_MUSBMHDRC)pOCD->pOcdSpecific;

    if (usbMhdrcUlpiRead(pMHDRC, USB_ULPI_DEBUG, &uReg8) != OK)
        {
        USB_MHDRC_ERR("READ ULPI USB_ULPI_DEBUG register failed\n",
                   1, 2, 3, 4, 5, 6);

        return FALSE;
        }

    USB_MHDRC_DBG("usbMhdrcOcdIsLineStateSE0 - ULPI USB_ULPI_DEBUG %p\n",
               uReg8, 2, 3, 4, 5, 6);

    /*
     * Get the Line State bits - the ULPI DEBUG register low 2 bits
     * encodes line state, so we mask it using 3 to get the actual value
     */

    uReg8 = (UINT8)(uReg8 & (ULPI_DEBUG_LINESTATE_MSK));

    /* Return if the Line State is in SE0 */

    return (uReg8 == ULPI_DEBUG_LINESTATE_SE0) ? TRUE : FALSE;
    }

/*******************************************************************************
*
* usbMhdrcOcdRequestSrp - request SRP to be initiated on the OTG port
*
* This routine is to request SRP to be initiated  on the OTG port.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*/

STATUS usbMhdrcOcdRequestSrp
    (
    pUSBOTG_OCD         pOCD,
    UINT32              uSrpTypeFlags
    )
    {
    pUSB_MUSBMHDRC  pMHDRC;
    UINT8           uReg8;
    UINT32          uCount;

    if ((pOCD == NULL) ||
        (pOCD->pOTG == NULL) ||
        (pOCD->pOcdSpecific == NULL) ||
        (pOCD->pOTG->uValidMagic != USBOTG_MAGIC_VALID))
        {
        USB_MHDRC_ERR("Invalid parameter\n",
                   1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Cast into platform specific data */

    pMHDRC = (pUSB_MUSBMHDRC)pOCD->pOcdSpecific;

    /* Read the DEVCTL register */

    uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_DEVCTL);

    if ((uSrpTypeFlags & USBOTG_SRP_TYPE_DATA_PULSE) ||
        (uSrpTypeFlags & USBOTG_SRP_TYPE_VBUS_PULSE))
        {
        USB_MHDRC_DBG("usbMhdrcOcdRequestSrp - ON\n",
                   1, 2, 3, 4, 5, 6);

        /* Set the SESSION bit */

        USB_MHDRC_REG_WRITE8 (pMHDRC,
                              USB_MHDRC_DEVCTL,
                              uReg8 | USB_MHDRC_DEVCTL_SESSION);


        uCount = 0;

        uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_DEVCTL);

        while (!(uReg8 & USB_MHDRC_DEVCTL_SESSION) &&
                (uCount++ < USB_MHDRC_REG_ACCESS_WAIT_COUNT))
            {
            uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_DEVCTL);
            }

        USB_MHDRC_DBG("usbMhdrcOcdRequestSrp - OK 1 - USB_MHDRC_DEVCTL 0x%X\n",
                   uReg8, 2, 3, 4, 5, 6);

        uCount = 0;

        uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_DEVCTL);

        while ((uReg8 & USB_MHDRC_DEVCTL_SESSION) &&
               (uCount++ < USB_MHDRC_REG_ACCESS_WAIT_COUNT))
            {
            uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_DEVCTL);
            }

        USB_MHDRC_DBG("usbMhdrcOcdRequestSrp - OK 2 - USB_MHDRC_DEVCTL 0x%X\n",
                   uReg8, 2, 3, 4, 5, 6);

        }
    else
        {
        USB_MHDRC_DBG("usbMhdrcOcdRequestSrp - OFF\n",
                   1, 2, 3, 4, 5, 6);

        /* Clear the SESSION bit */

        USB_MHDRC_REG_WRITE8 (pMHDRC,
                              USB_MHDRC_DEVCTL,
                              uReg8 & (~USB_MHDRC_DEVCTL_SESSION));
        }

    return OK;
    }

/*******************************************************************************
*
* usbMhdrcOcdSetCtlrMode - set the operation mode of the OTG port
*
* This routine is to set the operation mode of the OTG port.
*
* For MUSBMHDRC, this is does not do anything to control the hardware but just
* to record the required hardware mode. The hardware itself can "swtich between
* controller modes" automatically.
*
* RETURNS: OK
*
* ERRNO: N/A
*/

STATUS usbMhdrcOcdSetCtlrMode
    (
    pUSBOTG_OCD         pOCD,
    USBOTG_MODE         mode
    )
    {
    /*
     * Do nothing but just save the value and return OK, as this OTG
     * Controller can "swtich between controller modes" automatically
     * during HNP and the feature is built-in, no way to alter the
     * hardware mode by software.
     *
     * The purpose of the existence of this routine is only
     * for demostration, the <pSetCtlrMode> of the OTG Controller
     * Driver can be set to NULL in this case.
     */

    USB_MHDRC_DBG("usbMhdrcOcdSetCtlrMode - Set Ctlr mode to %d\n", mode, 2, 3, 4, 5 ,6);

    pOCD->ctlrMode = mode;

    return OK;
    }

/*******************************************************************************
*
* usbMhdrcOcdAutoCon2RstEnable - setup the "A-Connect to B-Reset" feature
*
* This routine is to setup the "A-Connect to B-Reset" feature of the OTG port.
*
* For the MUSBMHDRC, this routine does nothing and it is just an empty routine;
* But it must exist and assigned to <USBOTG_OCD_FUNCS::pOtgAutoCon2RstEnable>
* to inform the OTG Framework that it has the "A-Connect to B-Reset" feature
* and thus not to do manual HNP response as B-device.
*
* RETURNS: OK.
*
* ERRNO: N/A
*/

STATUS usbMhdrcOcdAutoCon2RstEnable
    (
    pUSBOTG_OCD         pOCD,
    BOOL                bEn
    )
    {
    /*
     * Do nothing but just return OK, as this OTG Controller
     * can do "A-Connect to B-Reset" automatically during HNP
     * and the feature is built-in, no way to enable or disable.
     *
     * The purpose of the existence of this routine is to inform
     * the OTG Framework not to do manual HNP response as B-device.
     */

    USB_MHDRC_DBG("usbMhdrcOcdAutoCon2RstEnable - bEn %d\n",
                  bEn, 2, 3, 4, 5 ,6);

    return OK;
    }

/*******************************************************************************
*
* usbMhdrcOcdAutoDis2ConEnable - setup the "B-Disconnect to A-Connect" feature
*
* This routine is to setup "B-Disconnect to A-Connect" feature of the OTG port.
*
* For the MUSBMHDRC, this routine does nothing and it is just an empty routine;
* But it must exist and assigned to <USBOTG_OCD_FUNCS::pOtgAutoDis2ConEnable>
* to inform the OTG Framework that it has the "B-Disconnect to A-Connect" feature
* and thus not to do manual HNP response as A-device.
*
* RETURNS: OK.
*
* ERRNO: N/A
*/

STATUS usbMhdrcOcdAutoDis2ConEnable
    (
    pUSBOTG_OCD         pOCD,
    BOOL                bEn
    )
    {
    /*
     * Do nothing but just return OK, as this OTG Controller
     * can do "B-Disconnect to A-Connect" automatically during HNP
     * and the feature is built-in, no way to enable or disable.
     *
     * The purpose of the existence of this routine is to inform
     * the OTG Framework not to do manual HNP response as A-device.
     */

    USB_MHDRC_DBG("usbMhdrcOcdAutoDis2ConEnable - bEn %d\n",
                  bEn, 2, 3, 4, 5 ,6);

    return OK;
    }

/*******************************************************************************
*
* usbMhdrcOcdAutoSusp2DisEnable - setup the "Suspend to Disconnect" feature
*
* This routine is to setup "Suspend to Disconnect" feature of the OTG port.
*
* For MUSBMHDRC, this is to setup the DEVCTL register HOSTREQ bit to enable
* or disable HNP request. Once enabled, the hardware will recognise the host
* suspend signaling and automatically disconnects from host.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*/

STATUS usbMhdrcOcdAutoSusp2DisEnable
    (
    pUSBOTG_OCD         pOCD,
    BOOL                bEn
    )
    {
    pUSB_MUSBMHDRC  pMHDRC;
    UINT8           uReg8;

    if ((pOCD == NULL) ||
        (pOCD->pOTG == NULL) ||
        (pOCD->pOcdSpecific == NULL) ||
        (pOCD->pOTG->uValidMagic != USBOTG_MAGIC_VALID))
        {
        USB_MHDRC_ERR("Invalid parameter\n",
                   1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Cast into platform specific data */

    pMHDRC = (pUSB_MUSBMHDRC)pOCD->pOcdSpecific;

    /* Read the DEVCTL register */

    uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_DEVCTL);

    if (bEn == TRUE)
        {
        USB_MHDRC_DBG("usbMhdrcOcdAutoSusp2DisEnable - Set HOSTREQ\n",
                      1, 2, 3, 4, 5 ,6);

        /* Set the HOSTREQ bit */

        USB_MHDRC_REG_WRITE8 (pMHDRC,
                              USB_MHDRC_DEVCTL,
                              uReg8 | USB_MHDRC_DEVCTL_HOSTREQ);
        }
    else
        {
        USB_MHDRC_DBG("usbMhdrcOcdAutoSusp2DisEnable - Clr HOSTREQ\n",
                      1, 2, 3, 4, 5 ,6);

        /* Clear the HOSTREQ bit */

        USB_MHDRC_REG_WRITE8 (pMHDRC,
                              USB_MHDRC_DEVCTL,
                              uReg8 & (~USB_MHDRC_DEVCTL_HOSTREQ));
        }

    return OK;
    }

/*******************************************************************************
*
* usbMhdrcOcdAutoSrpDetEnable - setup the "SRP detection" feature
*
* This routine is to setup "SRP detection" feature of the OTG port.
*
* For MUSBMHDRC, this is to setup the INTRUSBE register SESSREQ bit to enable
* or disable SRP detection interrupt. Once enabled, the hardware will recognise
* the B-device SRP signaling and automatically trigger interrupt.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*/

STATUS usbMhdrcOcdAutoSrpDetEnable
    (
    pUSBOTG_OCD         pOCD,
    BOOL                bEn
    )
    {
    pUSB_MUSBMHDRC  pMHDRC;
    UINT8           uReg8;

    if ((pOCD == NULL) ||
        (pOCD->pOTG == NULL) ||
        (pOCD->pOcdSpecific == NULL) ||
        (pOCD->pOTG->uValidMagic != USBOTG_MAGIC_VALID))
        {
        USB_MHDRC_ERR("Invalid parameter\n",
                   1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Cast into platform specific data */

    pMHDRC = (pUSB_MUSBMHDRC)pOCD->pOcdSpecific;

    /* Read the INTRUSBE register */

    uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_INTRUSBE);

    if (bEn == TRUE)
        {
        USB_MHDRC_DBG("usbMhdrcOcdAutoSrpDetEnable - Enable SESSREQ INTR\n",
                      1, 2, 3, 4, 5 ,6);

        /* Set the SESSREQ bit */

        USB_MHDRC_REG_WRITE8 (pMHDRC,
                              USB_MHDRC_INTRUSBE,
                              uReg8 | USB_MHDRC_INTR_SESSREQ);
        }
    else
        {
        USB_MHDRC_DBG("usbMhdrcOcdAutoSrpDetEnable - Disable SESSREQ INTR\n",
                      1, 2, 3, 4, 5 ,6);

        /* Clear the SESSREQ bit */

        USB_MHDRC_REG_WRITE8 (pMHDRC,
                              USB_MHDRC_INTRUSBE,
                              uReg8 & (~USB_MHDRC_INTR_SESSREQ));
        }

    return OK;
    }

/*******************************************************************************
*
* usbMhdrcOcdDrvVbus - setup the "Drive VBUS" feature
*
* This routine is to setup "Drive VBUS" feature of the OTG port.
*
* For MUSBMHDRC, this is to setup the DEVCTL register SESSION bit to enable
* or disable if the VBUS is driven when acting as A-device. Once enabled, the
* hardware will drive the VBUS apply the "DrvVbus" to the charge pump.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*/

STATUS usbMhdrcOcdDrvVbus
    (
    pUSBOTG_OCD         pOCD,
    BOOL                bEn
    )
    {
    pUSB_MUSBMHDRC  pMHDRC;
    UINT8           uReg8;
    UINT32          uCount;

    if ((pOCD == NULL) ||
        (pOCD->pOTG == NULL) ||
        (pOCD->pOcdSpecific == NULL) ||
        (pOCD->pOTG->uValidMagic != USBOTG_MAGIC_VALID))
        {
        USB_MHDRC_ERR("Invalid parameter\n",
                   1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Cast into platform specific data */

    pMHDRC = (pUSB_MUSBMHDRC)pOCD->pOcdSpecific;

    /* Read the DEVCTL register */

    uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_DEVCTL);

    if (bEn == TRUE)
        {
        USB_MHDRC_DBG("usbMhdrcOcdDrvVbus - Set the SESSION bit\n",
            1, 2, 3, 4, 5 ,6);

        /* Set the SESSION bit */

        USB_MHDRC_REG_WRITE8 (pMHDRC,
                              USB_MHDRC_DEVCTL,
                              uReg8 | USB_MHDRC_DEVCTL_SESSION);

        uCount = 0;

        uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_DEVCTL);

        /* Wait until SESSION active */

        while (!(uReg8 & USB_MHDRC_DEVCTL_SESSION) &&
                (uCount++ < USB_MHDRC_REG_ACCESS_WAIT_COUNT))
            {
            /* Set the SESSION bit */

            USB_MHDRC_REG_WRITE8 (pMHDRC,
                                  USB_MHDRC_DEVCTL,
                                  uReg8 | USB_MHDRC_DEVCTL_SESSION);

            uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_DEVCTL);
            }
        }
    else
        {
        USB_MHDRC_DBG("usbMhdrcOcdDrvVbus - Clr the SESSION bit\n",
            1, 2, 3, 4, 5 ,6);

        /* Clear the SESSION bit */

        USB_MHDRC_REG_WRITE8 (pMHDRC,
                              USB_MHDRC_DEVCTL,
                              uReg8 & (~USB_MHDRC_DEVCTL_SESSION));

        uCount = 0;

        uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_DEVCTL);

        /* Wait until the VBUS drops below Session End */

        while ((uReg8 & USB_MHDRC_DEVCTL_VBUS) &&
               (uCount++ < USB_MHDRC_REG_ACCESS_WAIT_COUNT))
            {
            /* Clear the SESSION bit */

            USB_MHDRC_REG_WRITE8 (pMHDRC,
                                  USB_MHDRC_DEVCTL,
                                  uReg8 & (~USB_MHDRC_DEVCTL_SESSION));

            uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_DEVCTL);
            }

        if (pMHDRC->bIsConnected == TRUE)
            {
            pMHDRC->uUsbIrqStatus |= USB_MHDRC_INTR_DISCONNECT;

            usbMhdrcReportHostInterrupts(pMHDRC);
            }

        }
    return OK;
    }

/*******************************************************************************
*
* usbMhdrcOcdCommonIsr - common ISR for the USB controller when OTG is supported
*
* This routine is the common ISR for the USB controller when OTG is supported.
*
* For MUSBMHDRC, OTG related interrupts are reported to the OTG Manager Framework
* as OTG events; if these events are only related for OTG, then the specific
* interrupt status bits are cleared; thre remaining interrupts are routed to
* either the HCD or TCD according to the current active stack mode.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

VOID usbMhdrcOcdCommonIsr
    (
    void *      pMhdrc
    )
    {
    pUSB_MUSBMHDRC      pMHDRC;
    UINT16              uIntTx;
    UINT16              uIntRx;
    UINT8               uIntUsb;
    UINT8               uDevCtl;
    UINT8               uPower;
    pUSBOTG_OCD         pOCD;

    pMHDRC = (pUSB_MUSBMHDRC)pMhdrc;

    if (pMHDRC == NULL)
        {
        USB_MHDRC_ERR("Invalid parameter 1\n",
                   1, 2, 3, 4, 5, 6);

        return;
        }

    pOCD = pMHDRC->pOCDData;

    if ((pOCD == NULL) ||
        (pOCD->pOTG == NULL) ||
        (pOCD->pOcdSpecific == NULL) ||
        (pOCD->pOTG->uValidMagic != USBOTG_MAGIC_VALID) ||
        (pMHDRC->pDev != pOCD->pDev) ||
        (pMHDRC->pDev != pOCD->pOTG->pDev))
        {
        USB_MHDRC_ERR("Invalid parameter 2\n",
                   1, 2, 3, 4, 5, 6);

        return;
        }

    uIntTx = USB_MHDRC_REG_READ16 (pMHDRC, USB_MHDRC_INTRTX) &
             USB_MHDRC_REG_READ16 (pMHDRC, USB_MHDRC_INTRTXE);
    uIntRx = USB_MHDRC_REG_READ16 (pMHDRC, USB_MHDRC_INTRRX) &
             USB_MHDRC_REG_READ16 (pMHDRC, USB_MHDRC_INTRRXE);
    uIntUsb = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_INTRUSB) &
              USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_INTRUSBE);
    uDevCtl = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_DEVCTL);
    uPower  = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_POWER);

    USB_MHDRC_VDBG("Reporting interrupts in mode %d..."
        "uIntTx 0x%X uIntRx 0x%X uIntUsb 0x%X uDevCtl 0x%X uPower 0x%X\n",
       USBOTG_OCD_CURRENT_STACK(pOCD),
       uIntTx,
       uIntRx,
       uIntUsb,
       uDevCtl, uPower);

    /* Handle the 1st stage of MUSBMHDRC interrupts */

    /* 1 - Resume */

    if (uIntUsb & USB_MHDRC_INTR_RESUME)
        {
        pUSBOTG_SUSPEND_RESUME_DETECTED_EVENT pEvent;

        USB_MHDRC_WARN("Got RESEUME interrupt!\n", 1, 2, 3, 4, 5, 6);

        pEvent = (pUSBOTG_SUSPEND_RESUME_DETECTED_EVENT)USBOTG_EVENT_DATA_GET();

        if (pEvent)
            {
            pEvent->header.id = USBOTG_EVENT_ID_SUSPEND_RESUME_DETECTED;
            pEvent->header.pDev = pMHDRC->pDev;
            pEvent->header.result = ERROR;
            pEvent->isSuspend = FALSE;

            usbOtgEventRaise(FALSE, pEvent);
            }
        else
            {
            USB_MHDRC_ERR("No mem for USBOTG_SUSPEND_RESUME_DETECTED_EVENT\n",
                       1, 2, 3, 4, 5, 6);
            }
        }

    /* 2 - Session Request */

    if (uIntUsb & USB_MHDRC_INTR_SESSREQ)
        {
        pUSBOTG_SRP_DETECTED_EVENT pEvent;

        /* Clear the Session Request Interrupt */

        uIntUsb = (UINT8)(uIntUsb & ~USB_MHDRC_INTR_SESSREQ);

        USB_MHDRC_WARN("Got SESSION request!\n", 1, 2, 3, 4, 5, 6);

        pEvent = (pUSBOTG_SRP_DETECTED_EVENT)USBOTG_EVENT_DATA_GET();

        if (pEvent)
            {
            pEvent->header.id = USBOTG_EVENT_ID_SRP_DETECTED;
            pEvent->header.pDev = pMHDRC->pDev;
            pEvent->header.result = ERROR;

            usbOtgEventRaise(FALSE, pEvent);
            }
        else
            {
            USB_MHDRC_ERR("No mem for USBOTG_SRP_DETECTED_EVENT\n",
                       1, 2, 3, 4, 5, 6);
            }
        }

    /* 3 - Vbus Error */

    if (uIntUsb & USB_MHDRC_INTR_VBUSERROR)
        {
        if ((USBOTG_OCD_CURRENT_STATE(pOCD) == USBOTG_STATE_a_wait_vrise) ||
            (USBOTG_OCD_CURRENT_STATE(pOCD) == USBOTG_STATE_a_wait_bcon) ||
            (USBOTG_OCD_CURRENT_STATE(pOCD) == USBOTG_STATE_a_host))
            {
            if (pMHDRC->uVbusError < USB_MHDRC_VBUSERROR_RECOVERY_MAX)
                {
                USB_MHDRC_WARN("Vbus Error! Retry %d\n",
                    pMHDRC->uVbusError, 2, 3, 4, 5, 6);

                pMHDRC->uVbusError++;

                USB_MHDRC_REG_WRITE8 (pMHDRC,
                                      USB_MHDRC_DEVCTL,
                                      USB_MHDRC_DEVCTL_SESSION);

                /* Clear it so that it won't be reported to Host Stack */

                uIntUsb = (UINT8)(uIntUsb & ~USB_MHDRC_INTR_VBUSERROR);
                }
            else
                {
                UINT8 vbusValue;
                USBOTG_VBUS_STATE vbusState;
                pUSBOTG_VBUS_STATE_CHANGED_EVENT pEvent;

                pMHDRC->uVbusError = 0;

                /* Get the actual VBUS state */

                vbusValue = (UINT8)((uDevCtl & USB_MHDRC_DEVCTL_VBUS) >>
                            USB_MHDRC_DEVCTL_S_VBUS);

                if ((vbusValue == USB_MHDRC_DEVCTL_VBUS_SESS_END) ||
                    (vbusValue == USB_MHDRC_DEVCTL_VBUS_A_VALID))
                    {
                    vbusState = USBOTG_VBUS_STATE_SessEnd;
                    }
                else if (vbusValue == USB_MHDRC_DEVCTL_VBUS_B_VALID)
                    {
                    vbusState = USBOTG_VBUS_STATE_SessValid;
                    }
                else /* if (vbusValue == USB_MHDRC_DEVCTL_VBUS_VALID) */
                    {
                    vbusState = USBOTG_VBUS_STATE_VBusValid;
                    }

                USB_MHDRC_WARN("Vbus Error! vbusState %d\n",
                    vbusState, 2, 3, 4, 5, 6);

                pEvent = (pUSBOTG_VBUS_STATE_CHANGED_EVENT)USBOTG_EVENT_DATA_GET();

                if (pEvent)
                    {
                    pEvent->header.id = USBOTG_EVENT_ID_VBUS_STATE_CHANGED;
                    pEvent->header.pDev = pMHDRC->pDev;
                    pEvent->header.result = ERROR;
                    pEvent->vbusState = vbusState;

                    usbOtgEventRaise(FALSE, pEvent);
                    }
                else
                    {
                    USB_MHDRC_ERR("No mem for USBOTG_VBUS_STATE_CHANGED_EVENT\n",
                               1, 2, 3, 4, 5, 6);
                    }
                }
            }
        else
            {
            USB_MHDRC_DBG("Unhandled Vbus Error!\n", 1, 2, 3, 4, 5, 6);
            }
        }

    /* 4 - Suspend */

    if (uIntUsb & USB_MHDRC_INTR_SUSPEND)
        {
        pUSBOTG_SUSPEND_RESUME_DETECTED_EVENT pEvent;

        USB_MHDRC_WARN("Got SUSPEND interrupt!\n", 1, 2, 3, 4, 5, 6);

        pEvent = (pUSBOTG_SUSPEND_RESUME_DETECTED_EVENT)USBOTG_EVENT_DATA_GET();

        if (pEvent)
            {
            pEvent->header.id = USBOTG_EVENT_ID_SUSPEND_RESUME_DETECTED;
            pEvent->header.pDev = pMHDRC->pDev;
            pEvent->header.result = ERROR;
            pEvent->isSuspend = TRUE;

            usbOtgEventRaise(FALSE, pEvent);
            }
        else
            {
            USB_MHDRC_ERR("No mem for USBOTG_SUSPEND_RESUME_DETECTED_EVENT\n",
                       1, 2, 3, 4, 5, 6);
            }
        }

    /* 5 - Connect */

    if (uIntUsb & USB_MHDRC_INTR_CONNECT)
        {
        if (uDevCtl & USB_MHDRC_DEVCTL_HM)
            {
            pUSBOTG_CONNECT_DISCONNECT_DETECTED_EVENT pEvent;

            USB_MHDRC_WARN("Got CONNECT interrupt, stack mode %d!\n",
                        USBOTG_OCD_CURRENT_STACK(pOCD), 2, 3, 4, 5, 6);

            pMHDRC->bIsConnected = TRUE;

            pEvent = (pUSBOTG_CONNECT_DISCONNECT_DETECTED_EVENT)USBOTG_EVENT_DATA_GET();

            if (pEvent)
                {
                pEvent->header.id = USBOTG_EVENT_ID_CONNECT_DISCONNECT_DETECTED;
                pEvent->header.pDev = pMHDRC->pDev;
                pEvent->header.result = ERROR;
                pEvent->isConnect = TRUE;

                usbOtgEventRaise(FALSE, pEvent);
                }
            else
                {
                USB_MHDRC_ERR("No mem for USBOTG_CONNECT_DISCONNECT_DETECTED_EVENT\n",
                           1, 2, 3, 4, 5, 6);
                }
            }
        else
            {
            USB_MHDRC_WARN("Invalid connection interrupt ignored!\n",
                        1, 2, 3, 4, 5, 6);

            /* Clear it so that it won't be reported to Host Stack */

            uIntUsb = (UINT8)(uIntUsb & ~USB_MHDRC_INTR_CONNECT);
            }
        }

    /* 6 - Disconnect */

    /*
     * If we get a Bus Reset but there is still a device connected,
     * disconnect the device.
     */

    if ((uIntUsb & USB_MHDRC_INTR_RESET) &&
        !(uDevCtl & USB_MHDRC_DEVCTL_HM) &&
        (pMHDRC->bIsConnected == TRUE))
        {
        USB_MHDRC_WARN("Got RESET while a Device connected, DISCONNECT it "
            "(uPower 0x%X, uDevCtl 0x%X, uIntUsb 0x%X )!\n",
            uPower, uDevCtl, uIntUsb, 4, 5, 6);

        uIntUsb |= USB_MHDRC_INTR_DISCONNECT;

        pMHDRC->bIsResetActive = TRUE;
        }

    if (uIntUsb & USB_MHDRC_INTR_DISCONNECT)
        {
        USB_MHDRC_WARN("Got DISCONNECT interrupt power %p stack mode %d, uIntUsb 0x%X!\n",
            uPower, USBOTG_OCD_CURRENT_STACK(pOCD), uIntUsb, 4, 5, 6);

        if (pMHDRC->bIsConnected == TRUE)
            {
            pUSBOTG_CONNECT_DISCONNECT_DETECTED_EVENT pEvent;

            pMHDRC->bIsConnected = FALSE;

            pEvent = (pUSBOTG_CONNECT_DISCONNECT_DETECTED_EVENT)USBOTG_EVENT_DATA_GET();

            if (pEvent)
                {
                pEvent->header.id = USBOTG_EVENT_ID_CONNECT_DISCONNECT_DETECTED;
                pEvent->header.pDev = pMHDRC->pDev;
                pEvent->header.result = ERROR;
                pEvent->isConnect = FALSE;

                usbOtgEventRaise(FALSE, pEvent);
                }
            else
                {
                USB_MHDRC_ERR("No mem for USBOTG_CONNECT_DISCONNECT_DETECTED_EVENT\n",
                           1, 2, 3, 4, 5, 6);
                }

            /*
             * This is a special case workaround.
             *
             * During long time OTG HNP testing, in some extreame cases, when
             * the host side gives up it host role, the host doesn't get a
             * DISCONNECT interrupt, but it gets a RESET interrupt directly.
             * However, the normal HNP procedure should be that the host get
             * the DISCONNECT interrupt first, then it CONNECTs to the peripheral,
             * and the peripheral becoming HOST and issue the RESET (thus a
             * RESET interrupt).
             *
             * When this happens, a RESET is got but we still have a connected
             * device, so we made up a "fake" DISCONNECT to disconnect the
             * device. We need to report the disconnect interrupt to the HCD,
             * while a RESET interrupt should be reported to the TCD, this
             * makes a conflict. So we have to report the DISCONNECT earlier
             * here, then clear the USB_MHDRC_INTR_DISCONNECT bit in uIntUsb,
             * so that this bit won't be reported to TCD again (because this
             * bit is considered by the TCD as a SESSION END, which will cause
             * the TCD to clear the DEVCTL SESSION bit, thus for a A-device,
             * actually it drops the VBUS incorrectly!)
             *
             * This workaround looks pretty ugly, but for now there is no better
             * way to avoid it since the hardware missed reporting DISCONNECT.
             *
             * Note that by doing this, the normal disconnect interrupt is also
             * reported here, but it doesn't do any harm.
             */

            pMHDRC->uUsbIrqStatus = USB_MHDRC_INTR_DISCONNECT;
            usbMhdrcReportHostInterrupts(pMHDRC);
            uIntUsb = (UINT8)(uIntUsb & ~USB_MHDRC_INTR_DISCONNECT);
            }
        }

    /* 7 - Bus Reset */

    if (uIntUsb & USB_MHDRC_INTR_RESET)
        {
        /* In host mode this bit indicates Babble Detected Error! */

        if (uDevCtl & USB_MHDRC_DEVCTL_HM)
            {
            if (uDevCtl & (USB_MHDRC_DEVCTL_FSDEV | USB_MHDRC_DEVCTL_LSDEV))
                {
                USB_MHDRC_WARN("Babble Detected for FS/LS! Ignored!\n",
                   1, 2, 3, 4, 5, 6);
                }
            else
                {
                USB_MHDRC_WARN("Babble Detected! Stopping Session!\n",
                   1, 2, 3, 4, 5, 6);

                USB_MHDRC_REG_WRITE8 (pMHDRC, USB_MHDRC_DEVCTL, 0);

                }
            }
        else
            {
            pUSBOTG_BUS_RESET_DETECTED_EVENT pEvent;

            USB_MHDRC_WARN("Got RESET interrupt!\n", 1, 2, 3, 4, 5, 6);

            /*
             * This will force into Target Stack and
             * direct us into TCD ISR
             */

            pEvent = (pUSBOTG_BUS_RESET_DETECTED_EVENT)USBOTG_EVENT_DATA_GET();

            if (pEvent)
                {
                pEvent->header.id = USBOTG_EVENT_ID_BUS_RESET_DETECTED;
                pEvent->header.pDev = pMHDRC->pDev;
                pEvent->header.result = ERROR;

                usbOtgEventRaise(FALSE, pEvent);
                }
            else
                {
                USB_MHDRC_ERR("No mem for USBOTG_BUS_RESET_DETECTED_EVENT\n",
                           1, 2, 3, 4, 5, 6);
                }
            }
        }

    pMHDRC->uTxIrqStatus = uIntTx;

    pMHDRC->uRxIrqStatus = uIntRx;

    pMHDRC->uUsbIrqStatus = uIntUsb;

    /*
     * Don't rely on USB_MHDRC_DEVCTL_HM since the hardware
     * may behave earlier than software. For example, while
     * the Target Stack is running, if a bus suspend is
     * received, the hardware may switch directly into Host
     * mode, but the software still needs to inform the Target
     * Stack of the suspend event.
     */

    /* 8 - Call Host or Target ISR handling */

    if (uIntTx | uIntRx | uIntUsb)
        {
        if (((USBOTG_OCD_CURRENT_STACK(pOCD) == USBOTG_MODE_HOST) ||
            (uIntUsb & USB_MHDRC_INTR_CONNECT)) &&
            (!(uIntUsb & USB_MHDRC_INTR_RESET)))
            {
            if (uIntUsb & USB_MHDRC_INTR_MASK_NO_SOF)
                USB_MHDRC_WARN("Reporting interrupts to HCD (ctlr mode %d)..."
                    "uTxIrqStatus 0x%X uRxIrqStatus 0x%X uUsbIrqStatus 0x%X\n",
                   pOCD->ctlrMode,
                   pMHDRC->uTxIrqStatus,
                   pMHDRC->uRxIrqStatus,
                   pMHDRC->uUsbIrqStatus, 5, 6);

            usbMhdrcReportHostInterrupts(pMHDRC);
            }
        else
            {
            if (uIntUsb & USB_MHDRC_INTR_MASK_NO_SOF)
                USB_MHDRC_WARN("Reporting interrupts to TCD (ctlr mode %d)..."
                    "uTxIrqStatus 0x%X uRxIrqStatus 0x%X uUsbIrqStatus 0x%X\n",
                   pOCD->ctlrMode,
                   pMHDRC->uTxIrqStatus,
                   pMHDRC->uRxIrqStatus,
                   pMHDRC->uUsbIrqStatus, 5, 6);

            usbMhdrcReportTargetInterrupts(pMHDRC);
            }
        }

    return;
    }

/*******************************************************************************
*
* usbMhdrcOcdLoadHCD - load the HCD for the MUSBMHDRC USB controller
*
* This routine is to load the HCD for the MUSBMHDRC USB controller.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*/

STATUS usbMhdrcOcdLoadHCD
    (
    pUSBOTG_OCD         pOCD
    )
    {
    pUSB_MUSBMHDRC      pMHDRC;

    if ((pOCD == NULL) ||
        (pOCD->pOTG == NULL) ||
        (pOCD->pOcdSpecific == NULL))
        {
        USB_MHDRC_ERR("Invalid parameter\n",
                   1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Cast into platform specific data */

    pMHDRC = (pUSB_MUSBMHDRC)pOCD->pOcdSpecific;

    if (gpUsbMhdrcHcdLoad)
        {
        return gpUsbMhdrcHcdLoad(pMHDRC);
        }

    USB_MHDRC_DBG("usbMhdrcOcdLoadHCD - No HCD included\n",
               1, 2, 3, 4, 5, 6);

    return ERROR;
    }

/*******************************************************************************
*
* usbMhdrcOcdUnLoadHCD - unload the HCD for the MUSBMHDRC USB controller
*
* This routine is to unload the HCD for the MUSBMHDRC USB controller.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*/

STATUS usbMhdrcOcdUnLoadHCD
    (
    pUSBOTG_OCD         pOCD
    )
    {
    pUSB_MUSBMHDRC      pMHDRC;

    if ((pOCD == NULL) ||
        (pOCD->pOTG == NULL) ||
        (pOCD->pOcdSpecific == NULL))
        {
        USB_MHDRC_ERR("Invalid parameter\n",
                   1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Cast into platform specific data */

    pMHDRC = (pUSB_MUSBMHDRC)pOCD->pOcdSpecific;

    if (gpUsbMhdrcHcdUnLoad)
        {
        return gpUsbMhdrcHcdUnLoad(pMHDRC);
        }

    USB_MHDRC_DBG("usbMhdrcOcdUnLoadHCD - No HCD included\n",
               1, 2, 3, 4, 5, 6);

    return ERROR;
    }

/*******************************************************************************
*
* usbMhdrcOcdLoadTCD - load the TCD for the MUSBMHDRC USB controller
*
* This routine is to load the TCD for the MUSBMHDRC USB controller.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*/

STATUS usbMhdrcOcdLoadTCD
    (
    pUSBOTG_OCD         pOCD
    )
    {
    pUSB_MUSBMHDRC      pMHDRC;

    USB_MHDRC_DBG("usbMhdrcOcdLoadTCD called\n",
               1, 2, 3, 4, 5, 6);

    if ((pOCD == NULL) ||
        (pOCD->pOTG == NULL) ||
        (pOCD->pOcdSpecific == NULL))
        {
        USB_MHDRC_ERR("Invalid parameter\n",
                   1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Cast into platform specific data */

    pMHDRC = (pUSB_MUSBMHDRC)pOCD->pOcdSpecific;

    if (gpUsbMhdrcTcdLoad)
        {
        return gpUsbMhdrcTcdLoad(pMHDRC);
        }

    USB_MHDRC_DBG("usbMhdrcOcdLoadTCD - No TCD included\n",
               1, 2, 3, 4, 5, 6);

    return ERROR;
    }

/*******************************************************************************
*
* usbMhdrcOcdUnLoadTCD - unload the TCD for the MUSBMHDRC USB controller
*
* This routine is to unload the TCD for the MUSBMHDRC USB controller.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*/

STATUS usbMhdrcOcdUnLoadTCD
    (
    pUSBOTG_OCD         pOCD
    )
    {
    pUSB_MUSBMHDRC      pMHDRC;

    USB_MHDRC_DBG("usbMhdrcOcdUnLoadTCD called\n",
               1, 2, 3, 4, 5, 6);

    if ((pOCD == NULL) ||
        (pOCD->pOTG == NULL) ||
        (pOCD->pOcdSpecific == NULL))
        {
        USB_MHDRC_ERR("Invalid parameter\n",
                   1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Cast into platform specific data */

    pMHDRC = (pUSB_MUSBMHDRC)pOCD->pOcdSpecific;

    if (gpUsbMhdrcTcdUnLoad)
        {
        return gpUsbMhdrcTcdUnLoad(pMHDRC);
        }

    USB_MHDRC_DBG("usbMhdrcOcdUnLoadTCD - No TCD included\n",
               1, 2, 3, 4, 5, 6);

    return ERROR;
    }

/*******************************************************************************
*
* usbMhdrcOcdEnableTCD - enable the TCD for the MUSBMHDRC USB controller
*
* This routine is to enable the TCD for the MUSBMHDRC USB controller.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*/

STATUS usbMhdrcOcdEnableTCD
    (
    pUSBOTG_OCD         pOCD
    )
    {
    USB_MHDRC_DBG("usbMhdrcOcdEnableTCD called\n",
               1, 2, 3, 4, 5, 6);

    if (gpUsbMhdrcTcdEnable)
        {

        pUSB_MUSBMHDRC      pMHDRC = pOCD->pOcdSpecific;

        /* Make sure the ID pull-up is set */

        usbMhdrcUlpiWrite(pMHDRC, USB_ULPI_OTG_CTL + USB_ULPI_SET_ADJ,
                          ULPI_OTG_CTL_ID_PULL_UP);

        if (gpUsbMhdrcTcdEnable(pMHDRC) == OK)
            {
            pMHDRC->uUsbIrqStatus |= USB_MHDRC_INTR_RESET;

            usbMhdrcReportTargetInterrupts(pMHDRC);

            return OK;
            }

        return ERROR;
        }

    USB_MHDRC_DBG("usbMhdrcOcdEnableTCD - No TCD included\n",
               1, 2, 3, 4, 5, 6);

    return ERROR;
    }

/*******************************************************************************
*
* usbMhdrcOcdDisableTCD - disable the TCD for the MUSBMHDRC USB controller
*
* This routine is to disable the TCD for the MUSBMHDRC USB controller.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*/

STATUS usbMhdrcOcdDisableTCD
    (
    pUSBOTG_OCD         pOCD
    )
    {
    USB_MHDRC_DBG("usbMhdrcOcdDisableTCD called\n",
               1, 2, 3, 4, 5, 6);

    if (gpUsbMhdrcTcdDisable)
        {
        return gpUsbMhdrcTcdDisable(pOCD->pOcdSpecific);
        }

    USB_MHDRC_DBG("usbMhdrcOcdDisableTCD - No TCD included\n",
               1, 2, 3, 4, 5, 6);

    return ERROR;
    }

/*******************************************************************************
*
* usbMhdrcOcdEnableHCD - enable the HCD for the MUSBMHDRC USB controller
*
* This routine is to enable the HCD for the MUSBMHDRC USB controller.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*/

STATUS usbMhdrcOcdEnableHCD
    (
    pUSBOTG_OCD         pOCD
    )
    {
    USB_MHDRC_DBG("usbMhdrcOcdEnableHCD called\n",
               1, 2, 3, 4, 5, 6);

    if (gpUsbMhdrcHcdEnable)
        {
        pUSB_MUSBMHDRC  pMHDRC = pOCD->pOcdSpecific;
        STATUS ret;

        /* Make sure the ID pull-up is set */

        usbMhdrcUlpiWrite(pMHDRC, USB_ULPI_OTG_CTL + USB_ULPI_SET_ADJ,
                          ULPI_OTG_CTL_ID_PULL_UP);

        ret = gpUsbMhdrcHcdEnable(pMHDRC);

        if (pMHDRC->bIsConnected)
            {
            USB_MHDRC_WARN("usbMhdrcOcdEnableHCD - Reporting CONNECT\n",
                       1, 2, 3, 4, 5, 6);

            pMHDRC->uUsbIrqStatus |= USB_MHDRC_INTR_CONNECT;

            usbMhdrcReportHostInterrupts(pMHDRC);
            }

        return ret;
        }

    USB_MHDRC_DBG("usbMhdrcOcdEnableHCD - No HCD included\n",
               1, 2, 3, 4, 5, 6);

    return ERROR;
    }

/*******************************************************************************
*
* usbMhdrcOcdDisableHCD - disable the HCD for the MUSBMHDRC USB controller
*
* This routine is to disable the HCD for the MUSBMHDRC USB controller.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*/

STATUS usbMhdrcOcdDisableHCD
    (
    pUSBOTG_OCD         pOCD
    )
    {
    USB_MHDRC_DBG("usbMhdrcOcdDisableHCD called\n",
               1, 2, 3, 4, 5, 6);

    if (gpUsbMhdrcHcdDisable)
        {
        pUSB_MUSBMHDRC  pMHDRC = pOCD->pOcdSpecific;

        if (pMHDRC->bIsConnected)
            {
            USB_MHDRC_WARN("usbMhdrcOcdDisableHCD - Reporting DISCONNECT\n",
                       1, 2, 3, 4, 5, 6);
            pMHDRC->uUsbIrqStatus |= USB_MHDRC_INTR_DISCONNECT;

            usbMhdrcReportHostInterrupts(pMHDRC);

            pMHDRC->bIsConnected = FALSE;
            }

        return gpUsbMhdrcHcdDisable(pMHDRC);
        }

    USB_MHDRC_DBG("usbMhdrcOcdDisableHCD - No HCD included\n",
               1, 2, 3, 4, 5, 6);

    return ERROR;
    }


/*******************************************************************************
*
* usbMhdrcOcdPlbInit - perform the BSP specific Initializaion
*
* For many PLB based controllers, different BSP level initialization is
* requried. This function does the BSP specific initializaiton in case it is
* required
*
* RETURNS: N/A
*
* ERRNO: N/A
*
*\NOMANUAL
*/

LOCAL VOID usbMhdrcOcdPlbInit
    (
    VXB_DEVICE_ID   pDev
    )
    {
    /*
     * The BSP specific initializaton will be done by the platform
     * initalizaton hook in usbMhdrcPlatformHardwareInit(), which
     * will make the boot process quicker since it is called in the
     * VxBus tDevConn task where communication with the PHY using
     * slow link such as I2C is more proper.
     */

    return;
    }

/*******************************************************************************
*
* usbMhdrcOcdDevicePciProbe - determine whether matching device is MUSBMHDRC
*
* This routine determines whether the matching device is an MUSBMHDRC OTG
* Controller or not. For PCI device type, the routine will probe the PCI
* Configuration space to check if it is required device. If a matching device
* is found, the routine will return TRUE.
*
* RETURNS: TRUE if it is the matching device, FALSE if not
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOL usbMhdrcOcdDevicePciProbe
    (
    VXB_DEVICE_ID       pDev
    )
    {
    if (PCI_ID_MATCH_MHDRC(pDev) == TRUE)
        return TRUE;
    else
        return FALSE;
    }

/*******************************************************************************
*
* usbMhdrcOcdDeviceConnect - initialize the MUSBMHDRC OTG Controller instance
*
* This routine intializes the the MUSBMHDRC OTG Controller. The routine is
* called by vxBus on a successful driver - device match with VXB_DEVICE_ID
* as parameter. This structure has all information about the device. This
* routine resets all MUSBMHDRC registers and then initializes them with
* default value.
*
* RETURN : N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbMhdrcOcdDeviceConnect
    (
    VXB_DEVICE_ID   pDev
    )
    {
    pUSBOTG_OCD     pOCD;
    pUSB_MUSBMHDRC  pMHDRC;

    USB_MHDRC_DBG("MUSBMHDRC USBOTG_OCD adding pDev %p\n", pDev, 2, 3, 4, 5, 6);

    if (!pDev)
        {
        USB_MHDRC_ERR("pDev invalid\n",
                  1, 2, 3, 4, 5, 6);
        return;
        }

    /* Allocate the OCD instance */

    pOCD = OSS_CALLOC(sizeof(USBOTG_OCD));

    if (!pOCD)
        {
        USB_MHDRC_ERR("MUSBMHDRC USBOTG_OCD not allocated\n",
                  1, 2, 3, 4, 5, 6);

        return;
        }

    /* Allocate memory for the USB_MUSBMHDRC structure */

    pMHDRC = (pUSB_MUSBMHDRC)OSS_CALLOC(sizeof(USB_MUSBMHDRC));

    /* Check if memory allocation is successful */

    if (NULL == pMHDRC)
        {
        USB_MHDRC_ERR("Memory not allocated for USB_MUSBMHDRC\n",
                     1, 2, 3, 4, 5 ,6);

        OS_FREE(pOCD);

        return;
        }

    /* Set the VxBus Device instance */

    pMHDRC->pDev = pDev;
    pMHDRC->pOCDData = pOCD;
    pMHDRC->pISR = usbMhdrcOcdCommonIsr;
    pDev->pDrvCtrl = pMHDRC;

    /* Set the OCD information */

    pOCD->pOcdSpecific = pMHDRC;
    pOCD->pOcdFuncs = &usbMhdrcOcdOcdFuncs;
    pOCD->pDev = pDev;
    pOCD->bmAttributes = USBOTG_ATTR_SRP_SUPPORT | USBOTG_ATTR_HNP_SUPPORT;

    USB_MHDRC_DBG("Initializing common hardware...\n",
               1, 2, 3, 4, 5, 6);

    /* Initialize the common hardware data structure */

    if (usbMhdrcPlatformHardwareInit(pMHDRC) != OK)
        {
        USB_MHDRC_ERR("usbMhdrcPlatformHardwareInit failed\n",
                  1, 2, 3, 4, 5, 6);

        pDev->pDrvCtrl = NULL;

        OS_FREE(pMHDRC);

        OS_FREE(pOCD);

        return;
        }

    USB_MHDRC_DBG("Calling usbOtgOcdAdd()...\n",
               1, 2, 3, 4, 5, 6);

    /* Add one OTG Controller instance */

    if (usbOtgOcdAdd(pOCD) != OK)
        {
        USB_MHDRC_ERR("MUSBMHDRC USBOTG_OCD not added\n",
                  1, 2, 3, 4, 5, 6);

        /* Uninitialize the hardware */

        usbMhdrcPlatformHardwareUnInit(pMHDRC);

        pDev->pDrvCtrl = NULL;

        OS_FREE(pMHDRC);

        OS_FREE(pOCD);

        return;
        }

    USB_MHDRC_DBG("Activating OCD...\n",
               1, 2, 3, 4, 5, 6);

    pMHDRC->isrMagic = USB_MHDRC_MAGIC_ALIVE;

    /* Enable HC interrupt */

    usbMhdrcHardwareIntrEnable(pMHDRC);

    USB_MHDRC_DBG("MUSBMHDRC USBOTG_OCD added OK\n", 1, 2, 3, 4, 5, 6);

    return;
    }

/*******************************************************************************
*
* usbHcdEhciDeviceRemove - remove the MUSBMHDRC OTG Controller instance
*
* This routine un-initializes the MUSBMHDRC OTG Controller instance. The routine
* is registered with vxBus and called when the driver is de-registered with
* vxBus. This routine will have VXB_DEVICE_ID as its parameter. This structure
* consists of all the information about the device.
*
* RETURNS: OK if the OTG Controller instance successfully removed, ERROR if not
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbMhdrcOcdDeviceRemove
    (
    VXB_DEVICE_ID   pDev
    )
    {
    pUSB_MUSBMHDRC  pMHDRC = pDev->pDrvCtrl;
    pUSBOTG_OCD     pOCD;

    /* We need to return OK, otherwise VxBus will consider we fail! */

    if ((pMHDRC == NULL) || (pMHDRC->pOCDData == NULL))
        return OK;

    /* Get OTG Controller instance */

    pOCD = pMHDRC->pOCDData;

    /* Disable the interrupt */

    usbMhdrcHardwareIntrDisable(pMHDRC);

    /* Uninitialize the hardware */

    usbMhdrcPlatformHardwareUnInit(pMHDRC);

    /* We can not respond to interrupts */

    pMHDRC->isrMagic = USB_MHDRC_MAGIC_DEAD;

    /* Remove the OTG Controller instance */

    if (usbOtgOcdRemove(pOCD) != OK)
        {
        USB_MHDRC_ERR("MUSBMHDRC USBOTG_OCD not removed\n",
                  1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    /* Free memory allocated */

    OS_FREE(pMHDRC);

    OS_FREE(pOCD);

    pDev->pDrvCtrl = NULL;

    return OK;
    }

/*******************************************************************************
* vxbUsbMhdrcOcdRegister - register MUSBMHDRC OCD with vxBus
*
* This routine registers register MUSBMHDRC OTG Controller Driver with vxBus.
* Note that this can be called early in the initialization sequence.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void vxbUsbMhdrcOcdRegister (void)
    {
    pUSBOTG_OCD_FUNCS pOcdFuncs = &usbMhdrcOcdOcdFuncs;

    USB_MHDRC_DBG("Trying to register MUSBMHDRC OCD\n",
              1, 2, 3, 4, 5, 6);

    /* Do not register multiple times */

    if (usbMhdrcOcdRegistered != FALSE)
        {
        USB_MHDRC_ERR("MUSBMHDRC OCD already registered\n",
                  1, 2, 3, 4, 5, 6);
        return;
        }

    /*
     * Clear the USBOTG_OCD_FUNCS and hook the functions that
     * we need to implement, some functions are not needed and
     * they must be set to NULL.
     */

    OS_MEMSET(pOcdFuncs, 0, sizeof(USBOTG_OCD_FUNCS));

    pOcdFuncs->pOtgVbusStateGet = usbMhdrcOcdGetVbusState;
    pOcdFuncs->pOtgIdStateGet = usbMhdrcOcdGetIdState;
    pOcdFuncs->pOtgIsLineStateSE0 = usbMhdrcOcdIsLineStateSE0;
    pOcdFuncs->pOtgAutoCon2RstEnable = usbMhdrcOcdAutoCon2RstEnable;
    pOcdFuncs->pOtgAutoDis2ConEnable = usbMhdrcOcdAutoDis2ConEnable;
    pOcdFuncs->pOtgAutoSrpDetEnable = usbMhdrcOcdAutoSrpDetEnable;
    pOcdFuncs->pOtgAutoSusp2DisEnable = usbMhdrcOcdAutoSusp2DisEnable;
    pOcdFuncs->pOtgDrvVbusEnable = usbMhdrcOcdDrvVbus;
    pOcdFuncs->pOtgRequestSrp = usbMhdrcOcdRequestSrp;
    pOcdFuncs->pOtgSetCtlrMode = usbMhdrcOcdSetCtlrMode;
    pOcdFuncs->pOtgLoadHCD = usbMhdrcOcdLoadHCD;
    pOcdFuncs->pOtgLoadTCD = usbMhdrcOcdLoadTCD;
    pOcdFuncs->pOtgUnLoadHCD = usbMhdrcOcdUnLoadHCD;
    pOcdFuncs->pOtgUnLoadTCD = usbMhdrcOcdUnLoadTCD;
    pOcdFuncs->pOtgEnableHCD = usbMhdrcOcdEnableHCD;
    pOcdFuncs->pOtgDisableHCD = usbMhdrcOcdDisableHCD;
    pOcdFuncs->pOtgEnableTCD = usbMhdrcOcdEnableTCD;
    pOcdFuncs->pOtgDisableTCD = usbMhdrcOcdDisableTCD;

    /* Register the MUSBMHDRC OTG Controller Driver for PCI bus */

    if (vxbDevRegister ((DRIVER_REGISTRATION *)
        &usbVxbPciMhdrcOcdDevRegistration)
        == ERROR)
        {
        USB_MHDRC_ERR("Failed to register MUSBMHDRC OCD for PCI\n",
                  1, 2, 3, 4, 5, 6);

        return;
        }

    /* Register the MUSBMHDRC OTG Controller Driver for PLB bus */

    if (vxbDevRegister ((DRIVER_REGISTRATION *)
        &usbVxbPlbMhdrcOcdDevRegistration)
        == ERROR)
        {
        USB_MHDRC_ERR("Failed to register MUSBMHDRC OCD for PLB\n",
                  1, 2, 3, 4, 5, 6);
        return;
        }

    /* Mark as regsitered */

    usbMhdrcOcdRegistered = TRUE;

    USB_MHDRC_DBG("Register MUSBMHDRC OCD OK\n",
              1, 2, 3, 4, 5, 6);

    return;
    }

/*******************************************************************************
* vxbUsbMhdrcOcdDeRegister - deregister MUSBMHDRC OCD with vxBus
*
* This routine deregisters register MUSBMHDRC OTG Controller Driver with vxBus.
*
* RETURNS: OK if the OCD deregistered, ERROR if failed.
*
* ERRNO: N/A
*/

STATUS vxbUsbMhdrcOcdDeRegister (void)
    {
    /* Do not deregister if not registered */

    if (usbMhdrcOcdRegistered != TRUE)
        {
        USB_MHDRC_ERR("MUSBMHDRC OCD not registered\n",
                  1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    /* DeRegister the MUSBMHDRC OTG Controller Driver for PLB bus */

    if (vxbDriverUnregister ((DRIVER_REGISTRATION *)
        &usbVxbPlbMhdrcOcdDevRegistration)
        == ERROR)
        {
        USB_MHDRC_ERR("Failed to deregister MUSBMHDRC OCD for PLB\n",
                  1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    /* DeRegister the MUSBMHDRC OTG Controller Driver for PCI bus */

    if (vxbDriverUnregister ((DRIVER_REGISTRATION *)
        &usbVxbPciMhdrcOcdDevRegistration)
        == ERROR)
        {
        USB_MHDRC_ERR("Failed to deregister MUSBMHDRC OCD for PCI\n",
                  1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Mark as not regsitered */

    usbMhdrcOcdRegistered = FALSE;

    return OK;
    }

