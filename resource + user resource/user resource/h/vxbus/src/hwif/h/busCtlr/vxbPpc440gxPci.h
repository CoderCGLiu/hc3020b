/* ppc440GX.h - IBM PowerPC 440GX Chip header */

/*
********************************************************************************
   This source and object code has been made available to you by IBM on an
   AS-IS basis.

   IT IS PROVIDED WITHOUT WARRANTY OF ANY KIND, INCLUDING THE WARRANTIES OF
   MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE OR OF NONINFRINGEMENT
   OF THIRD PARTY RIGHTS.  IN NO EVENT SHALL IBM OR ITS LICENSORS BE LIABLE
   FOR INCIDENTAL, CONSEQUENTIAL OR PUNITIVE DAMAGES.  IBM'S OR ITS LICENSOR'S
   DAMAGES FOR ANY CAUSE OF ACTION, WHETHER IN CONTRACT OR IN TORT, AT LAW OR
   AT EQUITY, SHALL BE LIMITED TO A MAXIMUM OF $1,000 PER LICENSE.  No license
   under IBM patents or patent applications is to be implied by the copyright
   license.

   Any user of this software should understand that neither IBM nor its
   licensors will be responsible for any consequences resulting from the use
   of this software.

   Any person who transfers this source code or any derivative work must
   include the IBM copyright notice, this paragraph, and the preceding two
   paragraphs in the transferred software.

   Any person who transfers this object code or any derivative work must
   include the IBM copyright notice in the transferred software.

   COPYRIGHT   I B M   CORPORATION 2000
   LICENSED MATERIAL  -  PROGRAM PROPERTY OF  I B M"

********************************************************************************
\NOMANUAL
*/

/*
 * Copyright (c) 2005-2008 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River License agreement.
 */

/*
modification history
--------------------
01g,18jun08,h_k  removed vxbDevControl
01f,02jun08,h_k  removed unused resource fields.
01e,11apr08,h_k  added pRegHandle.
01d,10mar08,h_k  removed pciConfigMech.
01c,06aug07,h_k  fixed IO space conversion.
01b,10apr07,rcs  removed Defines for the PLB_PCIX bridge config regs
01a,22sep06,rcs  created from ppc440gx.h 
*/

#ifndef INCvxbPpc440gxPcih
#define INCvxbPpc440gxPcih

#ifdef __cplusplus
    extern "C" {
#endif

typedef struct vxbPpc440gxPciDrvCtrl
    {
    struct vxbDev * pDev;
    void * pRegHandle;
    UINT32 plbPciIo1Start;
    UINT32 plbPciIo1Size;
    UINT32 pciIo1Start;
    UINT32 plbPciIo2Start;
    UINT32 plbPciIo2Size;
    UINT32 pciIo2Start;
    UINT32 pom0PciSizeAttrib;
    UINT32 pom0LocalAdrsLow;
    UINT32 pom0LocalAdrsHigh;
    UINT32 pom0PciAdrsLow;
    UINT32 pom0PciAdrsHigh;
    UINT32 pom1PciSizeAttrib;
    UINT32 pom1LocalAdrsLow;
    UINT32 pom1LocalAdrsHigh;
    UINT32 pom1PciAdrsLow;
    UINT32 pom1PciAdrsHigh;
    UINT32 pim0PciSizeAttrib;
    UINT32 pim0LocalAdrsLow;
    UINT32 pim0LocalAdrsHigh;
    UINT32 pim0PciAdrsLow;
    UINT32 pim0PciAdrsHigh;
    UINT32 pciCfgAddr;
    UINT32 pciCfgData;
    UINT32 autoConfig;
    struct vxbPciConfig *pPciConfig;
    struct vxbPciInt *pIntInfo;
    int     pciMaxBus;     /* Max number of sub-busses */
    FUNCPTR intAssignFuncSet;
    } VXB_PPC440GX_DRV_CTRL;


/* Configuration registers offsets */

#define PPC440EP_PCI_CONFIG_ADDR 0
#define PPC440EP_PCI_CONFIG_DATA 4 


/*
 * Outbound PLB_PCIX bridge address map
 * The outbound memory map consists of 5 fixed address areas (relative to the
 * base address of the PLB_PCIX bridge) and 3 programmable areas.
 *   Fixed:
 *      1. PCI I/O outbound window
 *      2. PCI extra I/O outbound window
 *      3. PCI configuration access register pair
 *      4. PCI configuration registers of the PLB_PCIX bridge itself.
 *      5. PCI special cycle outbound window
 *   Programmable:
 *      1. PCI Memory outbound window 0 (POM0)
 *      2. PCI Memory outbound window 0 (POM1)
 *      3. PCI Memory outbound window 0 (POM2)
 */

/*
 *  Base address of the PCI-X core in the chip.
 *  The physical address is 0x2.0000.0000
 */
#define PCIX0_BASE_PLB_UA	0x2

/*
 * PCI I/O outbound window (PLB and the PCI translation).
 */
#define PLB_PCI_IO_REG_1_START   pDrvCtrl->plbPciIo1Start   /* PLB side */
#define PLB_PCI_IO_REG_1_SIZE    pDrvCtrl->plbPciIo1Size                  /* 64KB     */
#define PCI_IO_REG_1_START       pDrvCtrl->pciIo1Start                  /* PCI side */
#define PCI_IO_REG_1_END         (PCI_IO_REG_1_START + (PLB_PCI_IO_REG_1_SIZE - 1)) 

/*
 * PCI extra I/O outbound window (PLB and the PCI translation).
 */
#define PLB_PCI_IO_REG_2_START   pDrvCtrl->plbPciIo2Start   /* PLB side */
#define PLB_PCI_IO_REG_2_SIZE    pDrvCtrl->plbPciIo2Size                  /* 64KB     */
#define PCI_IO_REG_2_START       pDrvCtrl->pciIo2Start                  /* PCI side */
#define PCI_IO_REG_2_END         (PCI_IO_REG_2_START + (PLB_PCI_IO_REG_2_SIZE - 1)) 

/*
 * Base address where the PCI configuration registers of the PLB_PCIX bridge
 * itself reside.  Individual register address definitions are below.
 * This region can be mapped with a single 4KB page in the MMU.
 */
#define PCIX0_CFG_BASE              pDrvCtrl->regBase 

/*
 * Defines for the standard PCI device configuration header (offsets and bits).
 * Offsets are relative to PCIX0_CFG_BASE.
 */
#define PCIC_VENDID    0x00
#define PCIC_DEVID     0x02
#define PCIC_CMD       0x04
  #define PCIC_SE    0x0100
  #define PCIC_ME    0x0004
  #define PCIC_MA    0x0002
  #define PCIC_IOA   0x0001
#define PCIC_STATUS    0x06
  #define PCIC_C66   0x0020
#define PCIC_REVID     0x08
#define PCIC_CLS       0x09
  #define PCIC_INTCLS  0x09
  #define PCIC_SUBCLS  0x0A
  #define PCIC_BASECLS 0x0B
#define PCIC_CACHEL    0x0C
#define PCIC_LATTIM    0x0D
#define PCIC_HDTYPE    0x0E
#define PCIC_BIST      0x0F
#define PCIC_BAR0      0x10
#define PCIC_BAR1      0x14
#define PCIC_BAR2      0x18
#define PCIC_BAR3      0x1C
#define PCIC_BAR4      0x20
#define PCIC_BAR5      0x24
#define PCIC_CISPTR    0x28
#define PCIC_SBSYSVID  0x2C
#define PCIC_SBSYSID   0x2E
#define PCIC_EROMBA    0x30
#define PCIC_CAP       0x34
#define PCIC_INTLN     0x3C
#define PCIC_INTPN     0x3D
#define PCIC_MINGNT    0x3E
#define PCIC_MAXLTNCY  0x3F
/* Offsets for IBM PLB_PCIX bridge specific registers start here. */
#define PCIC_BRDGOPT1  0x40
#define   BRDGOPT1_RBPL7      0xE0000000
#define     BRDGOPT1_RBPL7_0  0x20000000
#define     BRDGOPT1_RBPL7_1  0x40000000
#define     BRDGOPT1_RBPL7_2  0x80000000
#define   BRDGOPT1_RBPL6      0x1C000000
#define     BRDGOPT1_RBPL6_0  0x04000000
#define     BRDGOPT1_RBPL6_1  0x08000000
#define     BRDGOPT1_RBPL6_2  0x10000000
#define   BRDGOPT1_RBPL5      0x03800000
#define     BRDGOPT1_RBPL5_0  0x00800000
#define     BRDGOPT1_RBPL5_1  0x01000000
#define     BRDGOPT1_RBPL5_2  0x02000000
#define   BRDGOPT1_RBPL4      0x00700000
#define     BRDGOPT1_RBPL4_0  0x00100000
#define     BRDGOPT1_RBPL4_1  0x00200000
#define     BRDGOPT1_RBPL4_2  0x00400000
#define   BRDGOPT1_RBPL3      0x000E0000
#define     BRDGOPT1_RBPL3_0  0x00020000
#define     BRDGOPT1_RBPL3_1  0x00040000
#define     BRDGOPT1_RBPL3_2  0x00080000
#define   BRDGOPT1_RBPL2      0x0001C000
#define     BRDGOPT1_RBPL2_0  0x00004000
#define     BRDGOPT1_RBPL2_1  0x00008000
#define     BRDGOPT1_RBPL2_2  0x00010000
#define   BRDGOPT1_RBPL1      0x00003800
#define     BRDGOPT1_RBPL1_0  0x00000800
#define     BRDGOPT1_RBPL1_1  0x00001000
#define     BRDGOPT1_RBPL1_2  0x00002000
#define   BRDGOPT1_RBPL0      0x00000700
#define     BRDGOPT1_RBPL0_0  0x00000100
#define     BRDGOPT1_RBPL0_1  0x00000200
#define     BRDGOPT1_RBPL0_2  0x00000400
#define   BRDGOPT1_MSIEN      0x00000010
#define   BRDGOPT1_PCIARB     0x00000008
#define   BRDGOPT1_PCITMR     0x00000006
#define     BRDGOPT1_PCITMR_MEMRD     0x00000000
#define     BRDGOPT1_PCITMR_MEMRDLN   0x00000002
#define     BRDGOPT1_PCITMR_MEMRDMULT 0x00000004
#define PCIC_BRDGOPT2  0x44
#define   BRDGOPT2_PCIFREQ   0x00018000
#define   BRDGOPT2_PCIFREQ00 0x00000000
#define   BRDGOPT2_PCIFREQ01 0x00008000
#define   BRDGOPT2_PCIFREQ02 0x00010000
#define   BRDGOPT2_PCIFREQ03 0x00018000
#define   BRDGOPT2_PCIXENB   0x00004000
#define   BRDGOPT2_EXWRCI    0x00002000
#define   BRDGOPT2_PLBENBL   0x00001000
#define   BRDGOPT2_PDTD      0x00000400
#define PCIC_ERREN     0x50
#define PCIC_ERRSTS    0x54
#define PCIC_PLBSEA    0x58
#define PCIC_PLBBEARL  0x5C
#define PCIC_PLBBEARH  0x60
#define PCIL_POM0LAL   0x68
#define PCIL_POM0LAH   0x6C
#define PCIL_POM0SA    0x70
#define PCIL_POM0PCILA 0x74
#define PCIL_POM0PCIHA 0x78
#define PCIL_POM1LAL   0x7C
#define PCIL_POM1LAH   0x80
#define PCIL_POM1SA    0x84
#define PCIL_POM1PCILA 0x88
#define PCIL_POM1PCIHA 0x8C
#define PCIL_POM2SA    0x90
#define PCIL_PIM0SAL   0x98
#define PCIL_PIM0SAH   0xF8
#define PCIL_PIM0LAL   0x9C
#define PCIL_PIM0LAH   0xA0
#define PCIL_PIM1SA    0xA4
#define PCIL_PIM1LAL   0xA8
#define PCIL_PIM1LAH   0xAC
#define PCIL_PIM2SAL   0xB0
#define PCIL_PIM2SAH   0xFC
#define PCIL_PIM2LAL   0xB4
#define PCIL_PIM2LAH   0xB8
#define PCIM_CAPID     0xC0
#define PCIM_NEXTIPTR  0xC1
#define PCIM_MC        0xC2
#define PCIM_MA        0xC4
#define PCIM_MUA       0xC8
#define PCIM_MD        0xCC
#define PCIM_MEOI      0xCE
#define PCIC_CAPID     0xD0
#define PCIC_NEXTIPTR  0xD1
#define PCIC_PMC       0xD2
#define PCIC_PMCSR     0xD4
#define PCIC_PMCSRBSE  0xD6
#define PCIC_DATA      0xD7
#define PCIC_PMCSRR    0xD8
#define PCIB_CAPID     0xDC
#define PCIB_NEXTIPTR  0xDD
#define PCIB_CMD       0xDE
#define PCIB_STS       0xE0
#define   PCIXSTS_BUSNUM     0x0000FF00
#define   PCIXSTS_DEVNUM     0x000000F8
#define PCIB_IDR       0xE4
#define PCIB_CID       0xE8
#define PCIB_RID       0xEC
#define PCI_VPDCAPID   0xF0
#define PCI_VPDNIPTR   0xF1
#define PCI_VPDADR     0xF2
#define PCI_VPDDATA    0xF4
#define PCIS_MSGI      0x100
#define PCIS_MSGIH     0x104
#define PCIS_MSGO      0x108
#define PCIS_MSGOH     0x10C
#define PCIS_IMSI      0x1F8

#ifdef __cplusplus
    }
#endif

#endif  /* INCvxbPpc440gxPcih */
