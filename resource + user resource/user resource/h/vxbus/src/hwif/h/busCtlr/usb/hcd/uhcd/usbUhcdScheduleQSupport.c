/* usbUhcdScheduleQSupport.c - USB UHCD HCD schedule queue support */

/*
 * Copyright (c) 2003-2006, 2008-2011, 2014 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2003-2006, 2008-2011, 2014 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
Modification history
--------------------
01u,25Feb14,wyy  Enable vxbDmaBuf alignment checking (VXW6-70065)
01t,03may13,wyy  Remove compiler warning (WIND00356717)
01s,29dec11,ljg  Correct max length allowed for transfer (WIND00320518)
01r,13dec11,m_y  Modify according to code check result (WIND00319317)
01q,07jan11,ghs  Clean up compile warnings (WIND00247082)
01p,18may10,w_x  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
01o,10mar10,j_x  Changed for USB debug (WIND00184542)
01n,13jan10,ghs  vxWorks 6.9 LP64 adapting
01m,20jun09,w_x  Correct modification time for WIND00156002
01l,20mar09,w_x  Change the non-isoc request queue management (WIND00156002)
01k,04jun08,w_x  Added parameter nIreqQ for usbUhcdMakeTds;
                 changed usbUhcdSemCritical from binary semaphore to mutex;
                 changed some lines to meet line width.(WIND00121282 fix)
01j,08oct06,ami  Changes for USB-vxBus changes
01i,18may05,mta  Fix for bug while updating DWORD0 of QueueHead
01h,28mar05,pdg  non-PCI changes
01g,02mar05,ami  SPR #106383 (Max UHCI Host Controller Issue)
01f,25feb05,mta  SPR 106276
01e,15oct04,ami  Refgen Changes
01d,05oct04,mta  SPR100704- Removal of floating point math

01c,28jul03,mat  Endian related changes
01b,26jun03,mat  changing the code to WRS standards.
01a,25apr03,ram  written.
*/

/*
DESCRIPTION

This file contains functions which provide support to the
Schedule and Queue management module.

INCLUDE FILES:  usb2/usbOsal.h, usb2/usbHst.h,
usb2/usbUhci.h, usb2/usbUhcdScheduleQueue.h,
usb2/usbUhcdScheduleQSupport.h, usb2/usbUhcdCommon.h,
usb2/usbUhcdSupport.h
*/

/*
INTERNAL
 *******************************************************************************
 * Filename         : usbUhcdScheduleQSupport.c
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      :  This file contains functions which provide support
 *                     to the Schedule and Queue management module.
 *
 *
 ******************************************************************************/

/* includes */

#include "usb/usbOsal.h"
#include "usb/usbHst.h"
#include "usbUhci.h"
#include "usbUhcdScheduleQueue.h"
#include "usbUhcdScheduleQSupport.h"
#include "usbUhcdCommon.h"
#include "usbUhcdSupport.h"


/* forward declarations */

/* External global variables - Start */

/* Pointer to the global array of HOst controller structures.*/

extern pUSB_UHCD_DATA *g_pUHCDData;

/* External global variables - End */

/*******************************************************************************
*
* usbUhcdDeleteRequestInfo - delete a request info of the pipe
*
* This routine is used to delete a request info of the pipe. It will destroy
* the DMA map associated with this request info structure.
*
* <pHCDData> - Pointer to the HCD data
* <pHCDPipe> - Pointer to the pipe
* <pRequest> - Pointer to the request info structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* Note: It is expetect the pipe lock has been taken when calling this routine.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbUhcdDeleteRequestInfo
    (
    pUSB_UHCD_DATA          pHCDData,
    pUSB_UHCD_PIPE          pHCDPipe,
    pUSB_UHCD_REQUEST_INFO  pRequest
    )
    {
    /* Destroy the maps creatred */

    if (pRequest->ctrlSetupDmaMapId != NULL)
        {
        vxbDmaBufMapDestroy (pHCDPipe->ctrlSetupDmaTagId,
            pRequest->ctrlSetupDmaMapId);

        pRequest->ctrlSetupDmaMapId = NULL;
        }

    if (pRequest->usrDataDmaMapId != NULL)
        {
        vxbDmaBufMapDestroy (pHCDPipe->usrBuffDmaTagId,
            pRequest->usrDataDmaMapId);

        pRequest->usrDataDmaMapId = NULL;
        }

    /* Free the request */

    OS_FREE(pRequest);
    }

/*******************************************************************************
*
* usbUhcdCreateRequestInfo - create a request info of the pipe
*
* This routine is used to create a request info of the pipe. It will create
* the DMA map associated with this request info structure.
*
* <pHCDData> - Pointer to the HCD data
* <pHCDPipe> - Pointer to the pipe
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* Note: It is expetect the pipe lock has been taken when calling this routine.
*
* RETURNS: Pointer to the request info structure
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_UHCD_REQUEST_INFO usbUhcdCreateRequestInfo
    (
    pUSB_UHCD_DATA  pHCDData,
    pUSB_UHCD_PIPE  pHCDPipe
    )
    {
    /* Request information */

    pUSB_UHCD_REQUEST_INFO  pRequest;

    /* Get the request info */

    pRequest = (pUSB_UHCD_REQUEST_INFO)
                OS_MALLOC(sizeof(USB_UHCD_REQUEST_INFO));

    if (pRequest == NULL)
        {
        USB_UHCD_ERR("usbUhcdCreateRequestInfo - allocate pRequest fail\n",
            0, 0, 0, 0, 0, 0);

        return NULL;
        }

    /* Clear the request info */

    OS_MEMSET(pRequest, 0, sizeof(USB_UHCD_REQUEST_INFO));

    /* Record the pipe we work for */

    pRequest->pHCDPipe = pHCDPipe;

    /* Create Data DMA MAP ID */

    pRequest->usrDataDmaMapId = vxbDmaBufMapCreate (pHCDData->pDev,
        pHCDPipe->usrBuffDmaTagId,
        0,
        NULL);

    if (pRequest->usrDataDmaMapId == NULL)
        {
        USB_UHCD_ERR("usbUhcdCreateRequestInfo - "
            "allocate usrDataDmaMapId fail\n",
            0, 0, 0, 0, 0, 0);

        /* Delete the request resources */

        usbUhcdDeleteRequestInfo(pHCDData, pHCDPipe, pRequest);

        return NULL;
        }

    /* Control pipe needs special treatment for the Setup */

    if (pHCDPipe->uEndpointType == USBHST_CONTROL_TRANSFER)
        {
        /* Create control Setup DMA MAP ID */

        pRequest->ctrlSetupDmaMapId = vxbDmaBufMapCreate (pHCDData->pDev,
            pHCDPipe->ctrlSetupDmaTagId,
            0,
            NULL);

        if (pRequest->ctrlSetupDmaMapId == NULL)
            {
            USB_UHCD_ERR("usbUhcdCreateRequestInfo - "
                "allocate ctrlSetupDmaMapId fail\n",
                0, 0, 0, 0, 0, 0);

            /* Delete the request resources */

            usbUhcdDeleteRequestInfo(pHCDData, pHCDPipe, pRequest);

            return NULL;
            }
        }

    /* Link the new request info onto the free requst queue */

    if (pHCDPipe->pFreeRequestQueueHead == NULL)
        {
        pHCDPipe->pFreeRequestQueueHead = pRequest;
        }
    else
        {
        pHCDPipe->pFreeRequestQueueTail->pNext = pRequest;
        }

    pHCDPipe->pFreeRequestQueueTail = pRequest;

    return pRequest;
    }

/*******************************************************************************
*
* usbUhcdReserveRequestInfo - reserve a request info of the pipe for a URB
*
* This routine is used to reserve a request info structure of the pipe for a
* URB. It is commonly called when submitting a URB. A request info is considered
* to have been reserved when its associated with a URB (the URB pointer of this
* request info structure being non-NULL).
*
* <pHCDData> - Pointer to the HCD data
* <pHCDPipe> - Pointer to the pipe
* <pUrb> - Pointer to the URB
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* Note: It is expetect the pipe lock has been taken when calling this routine.
*
* RETURNS: the pointer to the reserved request info structure,
*   or NULL if no more free request on this pipe.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_UHCD_REQUEST_INFO usbUhcdReserveRequestInfo
    (
    pUSB_UHCD_DATA  pHCDData,
    pUSB_UHCD_PIPE  pHCDPipe,
    pUSBHST_URB     pUrb
    )
    {
    /* Request information */

    pUSB_UHCD_REQUEST_INFO  pRequest;

    /* We can not allow bigger than the specified size */

    if (pUrb->uTransferLength > pHCDPipe->uMaxTransferSize)
        {
        USB_UHCD_ERR(
            "usbUhcdReserveRequestInfo - uAddress %d uEndpointAddress %p\n"
            "uEndpointType %d uTransferLength %d is too big\n",
            pHCDPipe->uAddress,
            pHCDPipe->uEndpointAddress,
            pHCDPipe->uEndpointType,
            pUrb->uTransferLength, 0, 0);

        return NULL;
        }

    /* Check if there is a free request info */

    if (pHCDPipe->pFreeRequestQueueHead == NULL)
        {
        /* Create a new request */

        pRequest = usbUhcdCreateRequestInfo(pHCDData, pHCDPipe);

        if (pRequest == NULL)
            {
            USB_UHCD_ERR(
                "usbUhcdReserveRequestInfo -"
                "uTransferLength (%d) uMaxTransferSize (%d) \n",
                pUrb->uTransferLength,
                pHCDPipe->uMaxTransferSize,
                0, 0, 0, 0);

            return NULL;
            }
        }

    /* Point to the head of the free requests */

    pRequest = pHCDPipe->pFreeRequestQueueHead;

    /* Point the head to the next */

    pHCDPipe->pFreeRequestQueueHead = pRequest->pNext;

    /* If this is the only free request, update the head and tail to NULL */

    if (pRequest == pHCDPipe->pFreeRequestQueueTail)
        {
        pHCDPipe->pFreeRequestQueueHead = NULL;

        pHCDPipe->pFreeRequestQueueTail = NULL;
        }

    /* Break from the original list */

    pRequest->pNext = NULL;

    /* Associate the request info with the URB */

    pRequest->pUrb = pUrb;

    /* Save the requested length */

    pRequest->uRequestLength = pUrb->uTransferLength;

    /* Update the pipe pointer */

    pRequest->pHCDPipe = pHCDPipe;

    /* Give control pipe request some software retries on STALL */

    pRequest->uRetryCount = USB_UHCD_MAX_TD_RETRY_COUNT;

    /* Associate the URB with the request */

    pUrb->pHcdSpecific = (void *)pRequest;

    /* Add the request to the active request list of the pipe */

    if (pHCDPipe->pRequestQueueHead == NULL)
        {
        pHCDPipe->pRequestQueueHead = pRequest;
        }
    else
        {
        pHCDPipe->pRequestQueueTail->pNext = pRequest;
        }

    /* Move the tail pointer to point to this request */

    pHCDPipe->pRequestQueueTail = pRequest;

    return pRequest;
    }

/*******************************************************************************
*
* usbUhcdReleaseRequestInfo - release a request info of the pipe
*
* This routine is used to release a request info structure of the pipe.
* It is commonly called when returnning a URB to the user.
*
* <pHCDData> - Pointer to the HCD data
* <pHCDPipe> - Pointer to the pipe
* <pRequest> - Pointer to the request info structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* Note: It is expetect the pipe lock has been taken when calling this routine.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbUhcdReleaseRequestInfo
    (
    pUSB_UHCD_DATA          pHCDData,
    pUSB_UHCD_PIPE          pHCDPipe,
    pUSB_UHCD_REQUEST_INFO  pRequest
    )
    {
    /* Temp request info */

    pUSB_UHCD_REQUEST_INFO  pTempRequest;

    /* Do this only when the URB pointer is non-NULL */

    if ((pRequest != NULL) && (pRequest->pUrb != NULL))
        {
        /*
         * Set the URB HCD specific pointer to be NULL
         * indicating it is completed
         */

        pRequest->pUrb->pHcdSpecific = NULL;

        /* Set the pUrb as NULL to indicate it is free */

        pRequest->pUrb = NULL;

        /* Set the head TD pointer to be NULL */

        pRequest->pHead = NULL;

        /* Set the tail TD pointer to be NULL */

        pRequest->pTail = NULL;

        /* Remove the request from the active request list of this pipe */

        if (pRequest == pHCDPipe->pRequestQueueHead)
            {
            /*
             * If this is the only request on the list,
             * then set both head and tail to NULL
             */

            if (pRequest == pHCDPipe->pRequestQueueTail)
                {
                pHCDPipe->pRequestQueueHead = NULL;

                pHCDPipe->pRequestQueueTail = NULL;
                }
            else
                {
                /*
                 * If this is the head but not the tail,
                 * then only update the head pointer
                 */

                pHCDPipe->pRequestQueueHead = pRequest->pNext;
                }
            }
        else
            {
            /* If this is not the head, then find the previous request */

            pTempRequest = pHCDPipe->pRequestQueueHead;

            while (pTempRequest != NULL)
                {
                if (pTempRequest->pNext == pRequest)
                    break;

                pTempRequest = pTempRequest->pNext;
                }
            /*
             * Check if the previous request is not NULL.
             * This should be always true, but just to watch out.
             */

            if (pTempRequest != NULL)
                {
                pTempRequest->pNext = pRequest->pNext;

                /*
                 * If this is the tail, then update the tail to point to
                 * the previous request we found
                 */

                if (pRequest == pHCDPipe->pRequestQueueTail)
                    {
                    pHCDPipe->pRequestQueueTail = pTempRequest;
                    }
                }
            }

        /* Break from the active list */

        pRequest->pNext = NULL;

        /* Link the request info onto the free request queue */

        if (pHCDPipe->pFreeRequestQueueHead == NULL)
            {
            pHCDPipe->pFreeRequestQueueHead = pRequest;
            }
        else
            {
            pHCDPipe->pFreeRequestQueueTail->pNext = pRequest;
            }

        pHCDPipe->pFreeRequestQueueTail = pRequest;
        }

    return;
    }

/*******************************************************************************
*
* usbUhcdReleaseRequestInfo - check if a request info is reserved
*
* This routine is used to check if a request info is reserved.
*
* <pHCDData> - Pointer to the HCD data
* <pHCDPipe> - Pointer to the pipe
* <pRequest> - Pointer to the request info structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* Note: It is expetect the pipe lock has been taken when calling this routine.
*
* RETURNS: TRUE if the request is reserved, FALSE if not.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOLEAN usbUhcdIsRequestInfoUsed
    (
    pUSB_UHCD_DATA          pHCDData,
    pUSB_UHCD_PIPE          pHCDPipe,
    pUSB_UHCD_REQUEST_INFO  pRequest
    )
    {
    /* If the request info used */

    BOOLEAN used;

    used = (BOOLEAN)((pRequest->pUrb != NULL) ? TRUE : FALSE);

    return used;
    }

/*******************************************************************************
*
* usbUhcdAddToFreeTDList - add a TD to the free list
*
* This routine is used to add a TD to the free QH list.
*
* <pHCDData> - Pointer to the USB_UHCD_DATA structure
* <pHCDPipe> - Pointer to the USB_UHCD_PIPE structure
* <pTD> - Pointer to the USB_UHCD_TD data structure.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* Note: It is expetect the pipe lock has been taken when calling this routine.
*
* RETURNS: TRUE if the TD is added successfully.
*          FALSE if the TD is not added successfully.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOLEAN usbUhcdAddToFreeTDList
    (
    pUSB_UHCD_DATA  pHCDData,
    pUSB_UHCD_PIPE  pHCDPipe,
    pUSB_UHCD_TD    pTD
    )
    {
    /* Save the original DMA MAP ID */

    VXB_DMA_MAP_ID  dmaMapId;

    /* Get original DMA MAP ID */

    dmaMapId = pTD->dmaMapId;

    /* Reinitialize the memory */

    OS_MEMSET(pTD, 0, USB_UHCD_MAX_TD_SIZE);

    /* Copy the DMA MAP ID back */

    pTD->dmaMapId = dmaMapId;

    /* Update the next trasnfer descriptor of the TD */

    pTD->hNextTd = pHCDPipe->pFreeTDHead;

    /* Make this TD as the head of the free list */

    pHCDPipe->pFreeTDHead = pTD;

    /* If tail pointer is NULL, this TD becomes the tail pointer */

    if (NULL == pHCDPipe->pFreeTDTail)
        {
        pHCDPipe->pFreeTDTail = pTD;
        }

    return TRUE;
    }/* End of usbUhcdAddToFreeTDList() */


/*******************************************************************************
*
* usbUhcdGetFreeTD - get a free TD of the pipe
*
* This routine is used to retrieve a TD from the free TD list of the pipe,
* if the free TD list has no elements then create one TD.
*
* <pHCDData> - Pointer to the USB_UHCD_DATA structure
* <pHCDPipe> - Pointer to the USB_UHCD_PIPE structure
*
* Note: It is expetect the pipe lock has been taken when calling this routine.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* Note: It is expetect the pipe lock has been taken when calling this routine.
*
* RETURNS: Pointer to the USB_UHCD_TD data structure or NULL
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_UHCD_TD usbUhcdGetFreeTD
    (
    pUSB_UHCD_DATA  pHCDData,   /* Pointer to the USB_UHCD_DATA structure */
    pUSB_UHCD_PIPE  pHCDPipe    /* Pointer to the USB_UHCD_PIPE structure */
    )
    {
    /* Pointer to the TD */

    pUSB_UHCD_TD pTD = NULL;

    /*
     * Check if there is any trasnfer descriptor in the free list and
     * extract the trasnfer descriptor.Update the free list. This is done
     * acquiring and releasing an event.
     */

    /* There are trasnfer descriptors in the free list */

    if (NULL != pHCDPipe->pFreeTDHead)
        {
        /* Get the head trasnfer descriptor */

        pTD = pHCDPipe->pFreeTDHead;

        /* The head itself is returned, update head ptr to the next */

        pHCDPipe->pFreeTDHead = pTD->hNextTd;

        /* If this is the last trasnfer descriptor in the list, update the tail */

        if (pTD == pHCDPipe->pFreeTDTail)
            {
            pHCDPipe->pFreeTDTail = NULL;
            pHCDPipe->pFreeTDHead = NULL;
            }

        /* Break from the free list */

        pTD->hNextTd = NULL;

        }
    else
        {
        /* Create a new TD */

        pTD = usbUhcdCreateTD(pHCDData);

        if (NULL == pTD)
            {
            USB_UHCD_ERR("usbUhcdGetFreeTD - usbUhcdCreateTD fail\n",
                0, 0, 0, 0, 0, 0);

            return NULL;
            }
        }

    /* Return the pointer to TD */

    return pTD;
    }/* End of usbUhcdGetFreeTD() */

/*******************************************************************************
*
* usbUhcdDestroyQH - free a QH
*
* This routine frees the QH structure.
*
* <pHCDData> - Pointer to the HCD data
* <pQH> - Pointer to the USB_UHCD_QH structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: Status of the free operation.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbUhcdDestroyQH
    (
    pUSB_UHCD_DATA      pHCDData,    /* Pointer to the USB_UHCD_DATA structure */
    pUSB_UHCD_QH        pQH          /* Pointer to the USB_UHCD_QH structure */
    )
    {

    /* Unload the DMA MAP */

    if (vxbDmaBufMapUnload (pHCDData->qhDmaTagId, pQH->dmaMapId) != OK)
        {
        USB_UHCD_ERR("usbUhcdDestroyQH - vxbDmaBufMapUnload fail\n",
            0, 0, 0, 0, 0, 0);

        return ERROR;
        }

    /* Free the memory allocated */

    return vxbDmaBufMemFree(pHCDData->qhDmaTagId, pQH, pQH->dmaMapId);
    }

/*******************************************************************************
*
* usbUhcdDestroyTD - free a TD
*
* This routine frees the TD structure.
*
* <pHCDData> - Pointer to the HCD data
* <pTD> - Pointer to the USB_UHCD_TD structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: Status of the free operation.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbUhcdDestroyTD
    (
    pUSB_UHCD_DATA      pHCDData,    /* Pointer to the USB_UHCD_DATA structure */
    pUSB_UHCD_TD        pTD          /* Pointer to the pUSB_UHCD_TD structure */
    )
    {
    /* Unload the DMA MAP */

    if (vxbDmaBufMapUnload (pHCDData->tdDmaTagId, pTD->dmaMapId) != OK)
        {
        USB_UHCD_ERR("usbUhcdDestroyQH - vxbDmaBufMapUnload fail\n",
            0, 0, 0, 0, 0, 0);

        return ERROR;
        }

    /* Free the memory allocated */

    return vxbDmaBufMemFree(pHCDData->tdDmaTagId, pTD, pTD->dmaMapId);
    }

/*******************************************************************************
*
* usbUhcdDestroyAllTDs - destroy all free TDs on the free TD list of the pipe
*
* This routine destroy all free TDs on the free TD list of the pipe.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbUhcdDestroyAllTDs
    (
    pUSB_UHCD_DATA  pHCDData,   /* Pointer to the USB_UHCD_DATA structure */
    pUSB_UHCD_PIPE  pHCDPipe    /* Pointer to the USB_UHCD_PIPE structure */
    )
    {
    /* Pointer to the USB_UHCD_TD data structure */

    pUSB_UHCD_TD pTD;

    /* Free the trasnfer descriptors in the TD list */

    while ((pTD = pHCDPipe->pFreeTDHead) != NULL)
        {
        /* Head point to the next */

        pHCDPipe->pFreeTDHead = pTD->hNextTd;

        /* Destroy the TD */

        usbUhcdDestroyTD(pHCDData, pTD);

        /* When the tail is reached, break */

        if (pTD == pHCDPipe->pFreeTDTail)
            break;
        }

    /* Update the head and tail pointers to NULL */

    pHCDPipe->pFreeTDHead = NULL;
    pHCDPipe->pFreeTDTail = NULL;

    return;
    }/* End of function usbUhcdFreeAllLists() */

/*******************************************************************************
*
* usbUhcdCreateQH - create a QH
*
* This routine creates a QH structure.
*
* <pHCDData> - Pointer to the USB_UHCD_DATA structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: the pointer to the USB_UHCD_QH structure, or NULL if fail to create
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_UHCD_QH usbUhcdCreateQH
    (
    pUSB_UHCD_DATA      pHCDData    /* Pointer to the USB_UHCD_DATA structure */
    )
    {
    /* To hold the pointer to the QH */

    pUSB_UHCD_QH    pQH;

    /* Result of the vxbDmaBufMapLoad */

    STATUS          sts;

    /* The DMA MAP ID */

    VXB_DMA_MAP_ID  dmaMapId = NULL;

    /* Create memory and DMA map for the QH */

    pQH = vxbDmaBufMemAlloc (pHCDData->pDev,
            pHCDData->qhDmaTagId,
            NULL,
            VXB_DMABUF_MEMALLOC_CLEAR_BUF,
            &dmaMapId);

    /* Check if allocation is successful */

    if (pQH == NULL)
        {
        USB_UHCD_ERR("usbUhcdCreateQH - creating pQH fail\n",
            0, 0, 0, 0, 0, 0);

        return NULL;
        }

    /* Save our MAP ID */

    pQH->dmaMapId = dmaMapId;

    /*
     * Load the DMA MAP so that we get the correct BUS address
     * to program the hardware
     */

    sts = vxbDmaBufMapLoad (pHCDData->pDev,
            pHCDData->qhDmaTagId,
            pQH->dmaMapId,
            pQH,
            USB_UHCD_MAX_QH_SIZE,
            0);

    if (sts != OK)
        {
        USB_UHCD_ERR("usbUhcdCreateQH - loading map fail\n",
            0, 0, 0, 0, 0, 0);

        /* Free the memory allocated */

        (void) vxbDmaBufMemFree(pHCDData->qhDmaTagId, pQH, pQH->dmaMapId);

        return NULL;
        }

    return pQH;
    }

/*******************************************************************************
*
* usbUhcdCreateTD - create a TD
*
* This routine creates a TD structure.
*
* <pHCDData> - Pointer to the USB_UHCD_DATA structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: the pointer to the USB_UHCD_TD structure, or NULL if fail to create
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_UHCD_TD usbUhcdCreateTD
    (
    pUSB_UHCD_DATA      pHCDData    /* Pointer to the USB_UHCD_DATA structure */
    )
    {
    /* To hold the pointer to the TD */

    pUSB_UHCD_TD   pTD;

    /* Result of the vxbDmaBufMapLoad */

    STATUS          sts;

    /* The DMA MAP ID */

    VXB_DMA_MAP_ID  dmaMapId = NULL;

    /* Create memory and DMA map for the TD */

    pTD = vxbDmaBufMemAlloc (pHCDData->pDev,
            pHCDData->tdDmaTagId,
            NULL,
            VXB_DMABUF_MEMALLOC_CLEAR_BUF,
            &dmaMapId);

    /* Check if allocation is successful */

    if (pTD == NULL)
        {
        USB_UHCD_ERR("usbUhcdCreateTD - creating pTD fail\n",
            0, 0, 0, 0, 0, 0);

        return NULL;
        }

    /* Save our MAP ID */

    pTD->dmaMapId = dmaMapId;

    /*
     * Load the DMA MAP so that we get the correct BUS address
     * to program the hardware
     */

    sts = vxbDmaBufMapLoad (pHCDData->pDev,
            pHCDData->tdDmaTagId,
            pTD->dmaMapId,
            pTD,
            USB_UHCD_MAX_TD_SIZE,
            0);

    if (sts != OK)
        {
        USB_UHCD_ERR("usbUhcdCreateTD - loading map fail\n",
            0, 0, 0, 0, 0, 0);

        /* Free the memory allocated */

        (void) vxbDmaBufMemFree(pHCDData->tdDmaTagId, pTD, pTD->dmaMapId);

        return NULL;
        }

    return pTD;
    }

/*******************************************************************************
*
* usbUhcdFormEmptyTD - form an empty Transfer Descriptor
*
* This routine forms an empty Transfer Descriptor.
*
* RETURNS: NULL if creation fails
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USB_UHCD_TD *usbUhcdFormEmptyTD
    (
    pUSB_UHCD_DATA      pHCDData,    /* Pointer to the USB_UHCD_DATA structure */
    pUSB_UHCD_PIPE      pHCDPipe /* Pointer to the USB_UHCD_PIPE structure */
    )
    {
    /* To hold the pointer to the TD */

    USB_UHCD_TD *   pTD = NULL;

    /*
     * To hold the last 4 bits of the allocated address. This is used to check
     * whether the address is 16 byte alligned
     */

    UINT32          ctlSts = 0;
    UINT32          token = 0;
    UINT32          dword0 = 0;

    /* If the pipe is non-NULL, get from the pipe free TD list */

    if (pHCDPipe != NULL)
        {
        pTD = usbUhcdGetFreeTD(pHCDData, pHCDPipe);
        }
    else
        {
        pTD = usbUhcdCreateTD(pHCDData);
        }

    /* Check if the TD is allocated */

    if (pTD == NULL)
        {
        USB_UHCD_ERR("usbUhcdFormEmptyTD - failed to get free TD\n",
            0, 0, 0, 0, 0, 0);

        return NULL;
        }

    /* TD at this point is 16 byte alligned */

    /* The Link pointer field in TD references a TD and not a Queue Head */

    dword0 &= ~(USBUHCD_LINK_QH);

    /*
     * Invalidate Link pointer pointer
     * This means that there are no more valid elements in the queue
     */

    dword0 |= USBUHCD_LINK_TERMINATE;

    /* Make the TD active */

    ctlSts |= USBUHCD_TDCS_STS_ACTIVE;

    /* If a valid pipe is passed, other fields of the TD can be populated */

    if (pHCDPipe != NULL)
        {

        /* Populate the speed field */


        if (pHCDPipe->uSpeed == LOW_SPEED)
            {
            ctlSts |= USBUHCD_TDCS_LOWSPEED;
            }
        else
            {
            ctlSts &= ~(USBUHCD_TDCS_LOWSPEED);
            }

        /* If Isochronous pipe, set iso to 1 */

        if (pHCDPipe->uEndpointType == USBHST_ISOCHRONOUS_TRANSFER)
            {
            ctlSts |= USBUHCD_TDCS_ISOCH;
            }

        /* If non isochronous populate CERR field - number of retries to be 3 */

        else
            {
            /* Make the path of traversal for the HC as depth first. */

            dword0 |= USBUHCD_LINK_VF;

            ctlSts |= USBUHCD_TDCS_ERRCTR_3ERR;
            }

        /*
         * Populate MAXLEN field with the
         * maximum packet size supported by the pipe
         */

        if (pHCDPipe->uMaximumPacketSize)
            {

            token |= USBUHCD_TDTOK_MAXLEN_FMT
                        (pHCDPipe->uMaximumPacketSize - 1);

            }
        else
            {

            /*
             * Write 0x7FF to Maxlen for a 0 maximum packet size
             * A value of 0 written would indicate a max packet size of 1.
             * This is done to avoid that
             */

            token |= USBUHCD_TDTOK_MAXLEN_FMT (USBUHCD_TDTOK_MAXLEN_ZERO);
            }

         /* Populate the endpoint number */

        token |= USBUHCD_TDTOK_ENDPT_FMT (pHCDPipe->uEndpointAddress);


        /* Populate the function address */

        token |= USBUHCD_TDTOK_DEVADRS_FMT (pHCDPipe->uAddress);

        }

    pTD->dWord0Td = dword0;

    /* store the control status  */

    pTD->dWord1Td = ctlSts;

    /* store the token */

    pTD->dWord2Td = token;

    /* Return the pointer to the alligned TD address */

    return pTD;
    }/* End of usbUhcdFormEmptyTD() */


/*******************************************************************************
*
* usbUhcdFormEmptyQH - form an empty Queue Head
*
* This routine forms an empty Queue Head.
*
* RETURNS: Pointer to the QH, or NULL if creation fails
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USB_UHCD_QH *usbUhcdFormEmptyQH
    (
    pUSB_UHCD_DATA      pHCDData    /* Pointer to the USB_UHCD_DATA structure */
    )
    {
    /* To hold the pointer to the Queue head */

    USB_UHCD_QH *   pQH = NULL;

    /* Try to create the QH */

    pQH = usbUhcdCreateQH(pHCDData);

    if (pQH == NULL)
        {
        USB_UHCD_ERR("usbUhcdFormEmptyQH() - usbUhcdCreateQH() fail\n",
            1, 2, 3, 4, 5, 6);

        return NULL;
        }

    /* QH at this point is 16 byte alligned */

    /* Populate the fields of the QH */

    /*
     * Make the link pointer invalid
     * This is done to indicate that this is the last QH in the schedule
     */

    pQH->dWord0Qh |= USBUHCD_LINK_TERMINATE;
    pQH->dWord1Qh |= USBUHCD_LINK_TERMINATE;

    /*
     * The item referenced by the Queue Head link pointer
     * (mentioned in DWORD0) is a QH
     */

    pQH->dWord0Qh |= USBUHCD_LINK_QH;

    /*
     * The item referenced by the Queue element link pointer
     * (mentioned in DWORD1) is a TD
     */

    pQH->dWord1Qh &= ~(USBUHCD_LINK_QH);

    /* Return the pointer to the QH */

    return pQH;
    }/* End of usbUhcdFormEmptyQH () */


/***************************************************************************
*
* usbUhcdFindLinkForQh - find the index of the next node in the list
*
* This routine finds the index of the next node in the list.
*
* RETURNS: 0 if node not found
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/

INT32 usbUhcdFindLinkForQh
    (
    INT32 n
    )
    {

    /* To hold the number of elements in the tree till this point */

    INT32 numElement = 0;

    /*
     * To hold the level(polling interval supported by the HCD)
     * of the node indicated by 'n'
     */

    INT32 level = 0;

    /* To hold the index of the first QH in this level */

    INT32 first = 0;

    /* To hold the index of the first QH in the next lower level */

    INT32 firstPrev = 0;

    /* To hold the index of the middle QH in the present level */

    INT32 mid = 0;

    /* If paramenter is -Ve or 0, it is an error */

    if (n <= 0)
        {
        return (-1);
        }

    /* As this is an index, add 1 to get the number of nodes till this point */

    numElement = n + 1;

    /* Determine the level which contains the present QH */

    level = (INT32)usbUhciLog2((UINT32)numElement) ;

    /* Determine the index of the 1st QH at this level */

    first = (INT32) 1 << level;

    /* Determine the index of the 1st QH at the next lower level */

    firstPrev = first / 2;

    /* Determine the index of the middle QH in the present level */

    mid = first + firstPrev;

    /*
     * If numElement is in the upper half, return
     *  firstPrev - 1 + (numElement - first)
     */

    if (numElement < mid)
        {
        return (firstPrev - 1 + (numElement - first));
        }

    /* If N is in the bottom half, return firstPrev - 1 + (N - mid) */

    else
        {
        return (firstPrev - 1 + (numElement - mid));
        }
    }/* End of usbUhcdFindLinkForQh () */


/***************************************************************************
*
* usbUhcdFindQlinkToTree - determines the index in periodic tree
*
* This function is used to determine the Index into the periodic tree
* which, the Nth entry of the frame list must point to. We have a 1024
* entries in the frame list and a 128 leafed tree. Thus every 128th
* entry of the frame list points to the same point of the periodic tree
*
* RETURNS: 0 if index not found
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/

INT32 usbUhcdFindQlinkToTree
    (
    INT32 n
    )
    {

    /*
     * The Host Controller maintains an array of 1024 pointers which
     * point to the isochronous TD which are added in each list
     */

    return (127 + n % 128);
    }/* End of usbUhcdFindQlinkToTree() */


/*******************************************************************************
*
* usbUhcdFindHcdPipe - locate an HCD pipe structure
*
* This routine locates an HCD pipe structure with the specified properties.
*
* RETURNS: Pointer to the pipe found, NULL if pipe not found
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USB_UHCD_PIPE *usbUhcdFindHcdPipe
    (
    UINT8 BusIndex,
    UINT8 deviceNum,
    UINT8 endPointNum,
    UINT8 direction,
    UINT8 type
    )
    {
    /* Pointer to the host controller stucture */

    pUSB_UHCD_DATA  pHCDData = NULL;

    /* Temporary pointer to hold the HCD specific pipe pointer */

    USB_UHCD_PIPE * pScanPipe = NULL;

    /* Temporary pointer to hold the HCD specific pipe pointer */

    USB_UHCD_PIPE * pScanPipeList = NULL;

    /* Check if the pointer is valid */

    if (g_pUHCDData == NULL)
        {
        USB_UHCD_ERR("usbUhcdFindHcdPipe - invalid parameter \n",
                     1, 2, 3, 4, 5, 6);

        return NULL;
        }

    /* Retrieve the pointer to hcd data structure */

    pHCDData = g_pUHCDData[BusIndex];

    /* Exclusively access the active pipe list */

    OS_WAIT_FOR_EVENT(pHCDData->ActivePipeListSynchEventID, OS_WAIT_INFINITE);

    /* Get the pipe list to scan through */

    if (type != USBHST_ISOCHRONOUS_TRANSFER)
        {
        pScanPipeList = pHCDData->pNonIsochPipeList;
        }
    else
        {
        pScanPipeList = pHCDData->pIsochPipeList;
        }

    /* Scan each HCD pipe block in the queue of pipes maintained */

    for (pScanPipe = pScanPipeList;
         pScanPipe != NULL;
         pScanPipe = pScanPipe->pNext)
        {

        /*
         * Check for the device number, endpoint number, direction of the pipe,
         * and the type of the pipe
         */

        if (pScanPipe->uAddress == deviceNum &&
            pScanPipe->uEndpointAddress == endPointNum  &&
            pScanPipe->uEndpointDir == direction &&
            pScanPipe->uEndpointType == type)
            {
            /* Return the pipe block's address */

            break;
            }

        }/* End of for () */

    /* Release the synchronization event */

    OS_RELEASE_EVENT(pHCDData->ActivePipeListSynchEventID);

    /* If Pipe not found */

    USB_UHCD_VDBG("usbUhcdFindHcdPipe - pipe not found \n",
                  1, 2, 3, 4, 5, 6);

    return pScanPipe;
    }/* End of usbUhcdFindHcdPipe() */

/*******************************************************************************
*
* usbUhcdFormEmptyPipe - create an empty pipe block
*
* This function is called to allocate a new pipe structure
*
* RETURNS: pointer to the pipe, or NULL if pipe creation fails
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USB_UHCD_PIPE * usbUhcdFormEmptyPipe (void)
    {
    /* Temporary pointer to the USB_UHCD_PIPE structure */

    USB_UHCD_PIPE * pipe = NULL;

    /* Allocate memory for the pipe data structure */

    pipe = (USB_UHCD_PIPE *) OS_MALLOC(sizeof(USB_UHCD_PIPE));

    /* Check if memory allocation is successful */

    if (pipe)
        {
        OS_MEMSET (pipe, 0, sizeof (USB_UHCD_PIPE));
        }
    else
        {
        USB_UHCD_ERR("pipe could not allocated successfully \n",
                     1, 2, 3, 4, 5, 6);
        return NULL;
        }

    /* Return the address of the allocated memory */

    return(pipe);
    }/* End of usbUhcdFormEmptyPipe() */


/*******************************************************************************
*
* usbUhcdLinkQheadBetween - link a new Queue Head
*
* This routine is used to link a new Queue Head after the existing Queue Head.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbUhcdLinkQheadBetween
    (
    UINT32          uBusIndex,
    USB_UHCD_QH *   newQh,
    USB_UHCD_QH *   presentQh
    )
    {

    /* Check if the parameters are valid */

    if (NULL == newQh || NULL == presentQh)
        {
        USB_UHCD_ERR("parameters are invalid \n",
                     1, 2, 3, 4, 5, 6);
        return;
        }

    /* Check if the new QH is to be inserted between 2 QHs */

    if (presentQh->pNextQH)
        {
        USB_UHCD_VDBG("new Qh is to be linked in between \n",
                      1, 2, 3, 4, 5, 6);

        /* Manipulate the next link pointers */

        newQh->pNextQH = presentQh->pNextQH;

        /* Update the new QH's link pointer */

        newQh->dWord0Qh = presentQh->dWord0Qh;

        /* Update the previous pointers also */

        if (presentQh->pNextQH->pPrevQH)
            {
            USB_UHCD_VDBG("updating previous pointers \n",
                          1, 2, 3, 4, 5, 6);

            presentQh->pNextQH->pPrevQH = newQh;
            }
        }/* End of if() */

    /* If there is no QH which is already linked to presentQh */

    else
        {
        USB_UHCD_VDBG("no Qh present linked to current \n",
                      1, 2, 3, 4, 5, 6);
        /*
         * There is no element in the queue after the element
         * which is to be linked. So make the next pointer as NULL
         */

        newQh->pNextQH = NULL;

        /* Make this as the last element in the queue */

        newQh->dWord0Qh = (UINT32)USBUHCD_END_OF_LIST( uBusIndex);
        }/* End of else */

    /* Manipuate the link pointers to complete insertion/addition */

    /* Update the previous element pointer */

    newQh->pPrevQH = presentQh;

    /* Update the next link pointer of the already existing QH */

    presentQh->pNextQH = newQh;

    /* Update the link pointer in DWORD0 of the already existing QH */

    presentQh->dWord0Qh = USB_UHCD_SWAP_DATA(uBusIndex,
                            (USB_UHCD_DESC_LO32(newQh) | USBUHCD_LINK_QH));

    return;
    }/* End of usbUhcdLinkQheadBetween() */


/*******************************************************************************
*
* usbUhcdCalculateBusTime - calculate the BW occupied by a pipe
*
* This routine is used to calculate the bandwidth occupied by a pipe.
*
* RETURNS: bus time in nano seconds, or -1 if the speed it invalid
*
* ERRNO: N/A
*
* \NOMANUAL
*/

INT32 usbUhcdCalculateBusTime
    (
    UINT8 speed,
    UINT8 direction,
    UINT8 pipeType,
    UINT16 data
    )
    {

    /* Calculate the bus time for a low speed pipe */

    if (speed == LOW_SPEED)
        {
        USB_UHCD_VDBG("calculate the bus time for a low speed pipe \n",
                      1, 2, 3, 4, 5, 6);

        /* Calculate the bustime for an IN pipe */

        if (direction == DIR_IN )
            {

            return( INT32 )
            (64060 + (2 * USB_UHCD_HUB_LS_SETUP) +
             (677 *  (4 + usbUhcdBitStuffTime (data)))
             + USB_UHCD_HOST_DELAY);
            }

        /* Calculate the bustime for an OUT pipe */

        else
            {

            return(INT32)
            (64107 + (2 * USB_UHCD_HUB_LS_SETUP) +
             (667 *  ( 4 + usbUhcdBitStuffTime (data)))
             + USB_UHCD_HOST_DELAY);
            }/* End of else */
        }/* End of if(Speed == LOW_SPEED) */

    /* Calculate the bus time for a Full speed pipe */

    else if (speed == FULL_SPEED )
        {
        USB_UHCD_VDBG("calculate the bus time for a full speed pipe \n",
                      1, 2, 3, 4, 5, 6);

        /* Calculate the bus time for a control, bulk or interrupt pipe */

        if (pipeType != USBHST_ISOCHRONOUS_TRANSFER )
            {

            return(INT32)
            (9107 + (84 * (4 + usbUhcdBitStuffTime (data)))
             + USB_UHCD_HOST_DELAY);
            }

        /* Calculate the bus time for an isochronous pipe */

        else
            {
            /* Calculate the bus time for an IN endpoint */

            if (direction == DIR_IN)
                {

                return(INT32)
                (7268 + (84 * (4 + usbUhcdBitStuffTime (data)))
                 +USB_UHCD_HOST_DELAY);
                }

            /* Calculate the bus time for an OUT endpoint */

            else
                {

                return( INT32 )
                (6265 + (84 * (4 + usbUhcdBitStuffTime (data)))
                 +USB_UHCD_HOST_DELAY);
                }/* End of else */
            }/* End of else */
        }/* End of else if () */

    USB_UHCD_ERR("invalid speed %d \n", speed, 2, 3, 4, 5, 6);

    return -1;
    }/* End of usbUhcdCalculateBusTime() */


/***************************************************************************
*
* usbUhcdFindTreeBw - determines the bandwidth occupied
*
* This function is used to determine the bandwidth occupied by the
* list of nodes starting from index "treeIndex".
*
* RETURNS: bandwidth occupied
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/

INT32 usbUhcdFindTreeBw
    (
    INT32     treeIndex,
    USB_UHCD_PERIODIC_TREE_NODE  * usbUhcdTree
    )
    {
    /* Copy the index of the node into a local variable */
    INT32 index = treeIndex;

    /* To store the bandwidth occupied by the list */
    INT32 bw = 0;

    /*
     * Starting from "treeIndex" till the tip of the tree,
     * add up all the BandWidth
     */
    do
        {

        /* Add the bandwidth occupied in the node in the list */

        bw += (INT32)usbUhcdTree[index].bandWidth;

        /* Find the index of the next node in the list */

        index = usbUhcdFindLinkForQh (index);


        }while (index >= 0);

    /* Return the computed BW */

    return(bw);
    }/* End of usbUhcdFindTreeBw() */


/***************************************************************************
*
* usbUhcdMaxIsoBwLinkedTo - determines maximum bandwidth
*
* This function is used to determine the maximum bandwidth linked to the
* isochronous list which is linked to the tree node of index 'index'.
*
* RETURNS: maximum isochronous bandwidth linked to the list
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/

INT32 usbUhcdMaxIsoBwLinkedTo
    (
    INT32      index,
    USB_UHCD_PERIODIC_TABLE_ENTRY * usbUhcdTable
    )
    {
    /* Retrun the bandwith of the indexed usbUhcdTable */
    return(INT32)(usbUhcdTable[index].bandWidth);
    }/* End of usbUhcdMaxIsoBwLinkedTo() */


/*******************************************************************************
*
* usbUhcdFindPosForIntQh - locate point for queueing the interrupt QH
*
* This routine is used to locate a suitable point for queueing the
* interrupt QH based on the bandwidth required and the poll rate
*
* RETURNS: TRUE, or FALSE if bandwidth cannot be accomodated for the pipe
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOLEAN usbUhcdFindPosForIntQh
    (
    INT32                               bwReqd,
    UINT                                reqPollRate,
    INT32 *                             listIndex,
    USB_UHCD_PERIODIC_TABLE_ENTRY *     usbUhcdTable,
    USB_UHCD_PERIODIC_TREE_NODE  *      usbUhcdTree
    )
    {

    /* To hold the least periodic bandwidth */

    INT32 leastPeriodicBw = 0;

    /* Temporary variable to hold the bandwidth required */

    INT32 bw = 0;

    /* To hold the index into the leaf of the periodic tree */

    UINT i = 0;

    /* To hold the current polling rate */

    UINT currPollRate = 0;

    /* Determine the Bandwidth taken up by the upper most branch of the tree */

    leastPeriodicBw = usbUhcdFindTreeBw (127,usbUhcdTree)
                        + usbUhcdMaxIsoBwLinkedTo (127,usbUhcdTable);

    /* Start from the first node in the leaf */

    *listIndex = 127;

    /* Determine the least bandwidth occupying branch of the tree */

    for (i = 128; i < 255; i++)
        {

        /* Determine BW of Branch "i" */

        bw  = usbUhcdFindTreeBw ((INT32)i, usbUhcdTree)
                + usbUhcdMaxIsoBwLinkedTo ((INT32)i,usbUhcdTable);

        /*
         * If bandwidth occupied in this list is the least,
         * update the variable holding the least periodic bandwidth
         * Also update the list index.
         */

        if (bw < leastPeriodicBw)
            {
            USB_UHCD_VDBG("BW occupied is list \n", 1, 2, 3, 4, 5, 6);

            leastPeriodicBw = bw;

            *listIndex = (INT32)i;
            }

        /* If the lowest possible BW is hit, break out of this loop */

        if (leastPeriodicBw == 0)
            {
            USB_UHCD_VDBG("reached the lowest BW \n", 1, 2, 3, 4, 5, 6);

            break;
            }
        }/* End of for() */

    /*
     * If the least periodic BW plus required bandwidth excceeds max available,
     * QH cannot be accomodated
     */

    if (leastPeriodicBw + bwReqd + USB_UHCD_SAFETY_MARGIN >=
        USB_UHCD_NINETY_PERCENT_BW)
        {
        USB_UHCD_ERR("BW > max available \n", 1, 2, 3, 4, 5, 6);

        return (FALSE);
        }

    /*
     * In the current list,
     * determine the node that has the required poll rate
     */

    for (currPollRate = 128; currPollRate > reqPollRate;
        currPollRate /= 2)
        {
        *listIndex = usbUhcdFindLinkForQh  (*listIndex);
        }

    /*
     * If the QH's position is other than at the leaf of the tree,
     * determine whether any path of the tree violates the maximim BW rule
     */

    if (*listIndex < 127)
        {
        USB_UHCD_VDBG("check for QH violating \n", 1, 2, 3, 4, 5, 6);

        for (i = 127; i < 255; i++)
            {
            /*
             * If the current branch is linked to the newly found position,
             * check if BW exceeds what is available.
             *
             * Return FALSE if the following conditions are not successful
             * - Check whether the index specified by list_index pointer
             *   is present in the node containing this leaf index 'i'.
             * - The bandwidth reserved already in addition to the bandwidth
             *   requested for has exceeded the 90% of the allocation
             *   meant for the periodic transfers.
             */

            if (usbUhcdIsLinked((INT32)i, *listIndex) &&
                (usbUhcdFindTreeBw ((INT32)i,usbUhcdTree) +
                  usbUhcdMaxIsoBwLinkedTo ((INT32)i, usbUhcdTable) +
                 bwReqd + USB_UHCD_SAFETY_MARGIN) >= USB_UHCD_NINETY_PERCENT_BW)
                {
                USB_UHCD_ERR("BW exceeds available \n",1, 2, 3, 4, 5, 6);

                return FALSE;
                }
            }/* End of for() */
        }/* End of if() */

    return(TRUE);
    }/* End of usbUhcdFindPosForIntQh() */


/***************************************************************************
*
* usbUhcdIsLinked - determines if node is linked
*
* This function is used to determine whether 2 nodes of the tree is
* the part of the same list or not
*
* RETURNS: FALSE if the nodes are not linked.
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/

BOOLEAN usbUhcdIsLinked
    (
    INT32 listIndexOuter,
    INT32 listIndexInner
    )
    {

    /* To hold the index value */

    INT32 index = 0;

    /*
     * From the outer node, traverse the branch till we reach the inner node
     * or the tip of the tree is reached
     */

    for (index = listIndexOuter;
        index != listIndexInner && index >= 0;
        index = usbUhcdFindLinkForQh  (index));

    /* if the inner node is hit, return TRUE */

    if (index == listIndexInner)
        {
        return(TRUE);
        }

    /* Not hit, return FALSE */

    return(FALSE);
    }/* End of usbUhcdIsLinked() */


/*******************************************************************************
*
* usbUhcdMakeTds - create the TDs needed for a transfer
*
* This routine creates the TDs needed for a transfer.
*
* RETURNS: FALSE if TDs are not created successfully.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOLEAN usbUhcdMakeTds
    (
    UINT32                              uBusIndex,
    USB_UHCD_TD **                      head,
    USB_UHCD_TD **                      tail,
    USBHST_URB *                        pUrb,
    UINT8 *                             toggle ,
    USB_UHCD_PIPE *                     pHCDPipe,
    USB_UHCD_REQUEST_INFO *             pRequest
    )
    {

    /* To hold the pointer to the current TD */

    USB_UHCD_TD *                       currentTd = NULL;

    /* To hold the pointer to the temporary TD */

    USB_UHCD_TD *                       temp = NULL;

    /* To hold the number of bytes to be transferred */

    UINT                                bytesTransfered = 0;

    /* Pointer to the Host Controller Data stucture */

    pUSB_UHCD_DATA                      pHCDData = g_pUHCDData[uBusIndex];

    /* Extract the setup packet information */

    pUSBHST_SETUP_PACKET pSetupPacket = (pUSBHST_SETUP_PACKET)
                                        pUrb->pTransferSpecificData;

    /* Get the LOW 32 bit of the user buffer BUS address */

    UINT32 uDword = USB_UHCD_BUS_ADDR_LO32(
                    pRequest->usrDataDmaMapId->fragList[0].frag);

    /* Call the function to create the 1st TD needed for the transfer */

    currentTd = *head = usbUhcdFormEmptyTD (pHCDData, pHCDPipe);

    /* If unable to create the TD, return FALSE */

    if (currentTd == NULL)
        {
        USB_UHCD_ERR("usbUhcdMakeTds - unable to create TD \n",
                     1, 2, 3, 4, 5, 6);
        return FALSE;
        }

    /*
     * This loop will create that many TDs based on the number of
     * bytes to be transmitted
     */

    while (bytesTransfered < pUrb->uTransferLength)
        {

        /* Short packet detection is valid only for non Isochronous pipe */

        currentTd->dWord1Td |= USBUHCD_TDCS_SHORT;

        /* Set the buffer pointer of the TD */

        currentTd->dWord3Td = uDword + bytesTransfered;


        /* Set the Maxlength Field of the TD */

        /* Check if the bytes to transfer is less than maximum packet size */

        if ((pUrb->uTransferLength - bytesTransfered) <
            pHCDPipe->uMaximumPacketSize)
            {
            USB_UHCD_VDBG("usbUhcdMakeTds - byte transfer < max \n",
                          1, 2, 3, 4, 5, 6);

            /* Check if it is a non 0 byte data transfer to be done */

            if (pUrb->uTransferLength - bytesTransfered)
                {
                USB_UHCD_VDBG("usbUhcdMakeTds - non zero byte transfer \n",
                              1, 2, 3, 4, 5, 6);

                /* Update the maximum length of data in the TD */


                currentTd->dWord2Td &= ~(USBUHCD_TDTOK_MAXLEN_MASK);

                currentTd->dWord2Td |= USBUHCD_TDTOK_MAXLEN_FMT(
                                 pUrb->uTransferLength - bytesTransfered - 1);
                }

            /* A zero byte data transfer */

            else
                {
                USB_UHCD_VDBG("usbUhcdMakeTds - zero byte transfer \n",
                              1, 2, 3, 4, 5, 6);

                currentTd->dWord2Td &= ~(USBUHCD_TDTOK_MAXLEN_MASK);

                currentTd->dWord2Td |= USBUHCD_TDTOK_MAXLEN_FMT(0x7FFU);
                }

            /* Keep track of the number of bytes transfered */

            bytesTransfered += ( pUrb->uTransferLength - bytesTransfered );
            }

        /* If the bytes to transfer is more than maximum packet size */

        else
            {
            USB_UHCD_VDBG("usbUhcdMakeTds - byte transfer > max \n",
                          1, 2, 3, 4, 5, 6);

            bytesTransfered += pHCDPipe->uMaximumPacketSize;
            }

        /* Set the toggle bit of the TD for non isochronous TD */

        if (pHCDPipe->uEndpointType != USBHST_ISOCHRONOUS_TRANSFER)
            {
            USB_UHCD_VDBG("usbUhcdMakeTds -"
                          "pipe is not Isochronous -- set toggle bit \n",
                          1, 2, 3, 4, 5, 6);

            if (*toggle)
                {
                currentTd->uToggle = 1;
                currentTd->dWord2Td |= USBUHCD_TDTOK_DATA_TOGGLE;
                }
            else
                {
                currentTd->uToggle = 0;
                currentTd->dWord2Td &= ~(USBUHCD_TDTOK_DATA_TOGGLE);
                }

            *toggle = (UINT8)((*toggle == 0) ? 1 : 0);
            }

        /* For isochronous, set toggle to 0, since toggle is not used */

        /* Update PID field of the TD */

        if (pHCDPipe->uEndpointType != USBHST_CONTROL_TRANSFER)
            {

            currentTd->dWord2Td &= ~(USBUHCD_TDTOK_PID_MASK);

            if (pHCDPipe ->uEndpointDir == 1)
                currentTd->dWord2Td |= USBUHCD_TDTOK_PID_FMT(0x69);
            else
                currentTd->dWord2Td |= USBUHCD_TDTOK_PID_FMT(0xE1);
            }
        else
            {

            currentTd->dWord2Td &=~(USBUHCD_TDTOK_PID_MASK);

            if (0 != (pSetupPacket->bmRequestType & 0x80))
                currentTd->dWord2Td |= USBUHCD_TDTOK_PID_FMT(0x69);
            else
                currentTd->dWord2Td |= USBUHCD_TDTOK_PID_FMT(0xE1);
            }

        /* Check if more bytes need to be transfered */

        if (bytesTransfered < pUrb->uTransferLength)
            {
            USB_UHCD_VDBG("usbUhcdMakeTds - more bytes to be transfered \n",
                          1, 2, 3, 4, 5, 6);

            /* Non-isochronous transfer */

            if (pHCDPipe->uEndpointType != USBHST_ISOCHRONOUS_TRANSFER)
                {
                USB_UHCD_VDBG("usbUhcdMakeTds - pipe != Isochronous \n",
                              1, 2, 3, 4, 5, 6);

                /* Form a new TD */

                currentTd->hNextTd = usbUhcdFormEmptyTD (pHCDData, pHCDPipe);

                /* If unable to allocate memory for the TD */

                if (currentTd->hNextTd == NULL)
                    {
                    USB_UHCD_ERR("usbUhcdMakeTds - unable to allocate memory\n",
                                 1, 2, 3, 4, 5, 6);

                    /* Clean up memory allocated for the TD till now */

                    for (currentTd = *head; currentTd != NULL;)
                        {
                        temp = currentTd->hNextTd;

                        usbUhcdAddToFreeTDList(pHCDData, pHCDPipe, currentTd);

                        currentTd = temp;
                        }

                    /* Return FALSE */

                    return FALSE;
                    }

                /* Update the HC TD's link pointer */


                currentTd->dWord0Td &= ~(USBUHCD_LINK_PTR_MASK);

                currentTd->dWord0Td |= USBUHCD_LINK_PTR_FMT(
                            USB_UHCD_DESC_LO32(currentTd->hNextTd) >> 4);

                /* Indicate that this is not the last element in the queue */

                currentTd->dWord0Td &= ~(USBUHCD_LINK_TERMINATE);

                /* Store the previous element of the next TD */

                currentTd->hNextTd->hPrevTd = currentTd;

                /* Update the current pointer */

                currentTd = currentTd->hNextTd;

                /* Make the next pointer as NULL */

                currentTd->hNextTd = NULL;
                }/* End of if() */
            }/* End of if(bytesTransfered < pUrb->uTransferBufferLen) */
        }/* End of while () */

    /* Update the tail pointer so as to make it point to the last TD */

    *tail = currentTd;

    /* Return TRUE */

    return (TRUE);
    }/* End of usbUhcdMakeTds() */

/*******************************************************************************
*
* usbUhcdMakeIsocTds - create the TDs needed for a transfer
*
* This routine creates the TDs needed for a transfer.
*
* RETURNS: FALSE if TDs are not created successfully.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOLEAN usbUhcdMakeIsocTds
    (
    UINT32                              uBusIndex,
    USB_UHCD_TD **                      head,
    USB_UHCD_TD **                      tail,
    USBHST_URB *                        pUrb,
    UINT8 *                             toggle ,
    USB_UHCD_PIPE *                     pHCDPipe,
    USB_UHCD_REQUEST_INFO *             pRequest
    )
    {

    /* To hold the pointer to the current TD */

    USB_UHCD_TD *currentTd = NULL;

    /* To hold the pointer to the temporary TD */

    USB_UHCD_TD *temp = NULL;

    /* To hold the number of TDs this URB requires */

    UINT32 numberOfTDs = 0;

    /* To hold the pointer to the isoc packets */

    pUSBHST_ISO_PACKET_DESC pIsocPackets = NULL;

    /* Pointer to the Host Controller Data stucture */

    pUSB_UHCD_DATA  pHCDData = g_pUHCDData[uBusIndex];

    /* Get the LOW 32 bit of the user buffer BUS address */

    UINT32 uDword = USB_UHCD_BUS_ADDR_LO32(
                    pRequest->usrDataDmaMapId->fragList[0].frag);

    /* Call the function to create the 1st TD needed for the transfer */

    currentTd = *head = usbUhcdFormEmptyTD (pHCDData, pHCDPipe);

    /* If unable to create the TD, return FALSE */

    if (currentTd == NULL)
        {
        USB_UHCD_ERR("usbUhcdMakeIsocTds - unable to create TD \n",
                     1, 2, 3, 4, 5, 6);

        return FALSE;
        }

    /* Get the number of TDs this URB requires */

    numberOfTDs = pUrb->uNumberOfPackets;

    /* Get the ISOC packet descriptor for this URB */

    pIsocPackets = (pUSBHST_ISO_PACKET_DESC) pUrb->pTransferSpecificData;

    /*
     * This loop will create TDs based on the number of
     * bytes to be transmitted
     */

    while (numberOfTDs)
        {

        /* Set the buffer pointer of the TD */

        /*
         * currentTd->dWord3Td.bufferPointer =
         *         pUrb->pTransferBuffer + pIsocPackets->uOffset;
         */

        currentTd->dWord3Td = uDword + pIsocPackets->uOffset;

       /* Set the Maxlength Field of the TD */

        currentTd->dWord2Td  &= ~(USBUHCD_TDTOK_MAXLEN_MASK);

        /* As UHCI spec (3.2 Transfer Descriptor TD), the value programmed in 
        this register is encoded as n-1 (see Maximum Length field description 
        in the TD Token, Dword 2). Note that values from 500h to 7FEh are 
        illegal and cause a consistency check failure. */

        if (pIsocPackets->uLength <= USB_UHCD_SPECIFY_MAXLEN)
            {
            currentTd->dWord2Td |= 
                USBUHCD_TDTOK_MAXLEN_FMT(pIsocPackets->uLength -1);
            }
        else
            {
            USB_UHCD_ERR("uLength of pIsocPackets is illegal.\n", 
			             1, 2, 3, 4, 5, 6);

            return FALSE;
            }
        
        /* Check if the bytes to transfer is less than maximum packet size */

        /* Toggle bit is zero */

        currentTd->uToggle = 0;
        currentTd->dWord2Td &= ~(USBUHCD_TDTOK_DATA_TOGGLE);

        /* Update PID field of the TD */

        currentTd->dWord2Td &= ~(USBUHCD_TDTOK_PID_MASK);

        if (pHCDPipe ->uEndpointDir == 1)
              currentTd->dWord2Td |= USBUHCD_TDTOK_PID_FMT(USBUHCD_TDTOK_PID_IN);
        else
             currentTd->dWord2Td |= USBUHCD_TDTOK_PID_FMT(USBUHCD_TDTOK_PID_OUT);

        /* Decrement the TD count */

        numberOfTDs--;

        /* Increment the pointer to the next Isoc data */

        pIsocPackets++;

        /* More TDs required */

        if (numberOfTDs)
            {
            /* Create another TD */

            currentTd->vNextTd = usbUhcdFormEmptyTD (pHCDData, pHCDPipe);

            /* If unable to allocate memory for the TD */

            if (currentTd->vNextTd == NULL)
                {
                USB_UHCD_ERR("usbUhcdMakeIsocTds - unable to create TD \n",
                             1, 2, 3, 4, 5, 6);

                /* Clean up memory allocated for the TD till now */

                for (currentTd = *head; currentTd != NULL;)
                    {
                    temp = currentTd->vNextTd;

                    usbUhcdAddToFreeTDList(pHCDData, pHCDPipe, currentTd);

                    currentTd = temp;
                    }

                /* Return FALSE */

                return FALSE;
                }

            /* Store the previous element of the next TD */

            currentTd->vNextTd->vPrevTd = currentTd;

            /* Update the current pointer */

            currentTd = currentTd->vNextTd;

            /* Make the next pointer as NULL */

            currentTd->vNextTd = NULL;
            }/* End of if() */
        else
            {
            /* This is the last TD */

            currentTd->vNextTd = NULL;
            }
        }/* End of while () */

    /*
     * Indicate that an interrupt needs to be generated on completion of
     * the transfer indicated by the tail TD
     */

    uDword = currentTd->dWord1Td | USBUHCD_TDCS_COMPLETE;
    currentTd->dWord1Td = uDword;

    /* Update the tail pointer so as to make it point to the last TD */

    *tail = currentTd;

    /* Return TRUE */

    return (TRUE);
    }/* End of usbUhcdMakeIsocTds() */

/***************************************************************************
*
* usbUhcdFillNonisoStatus - fills the status of a non isochronous
*
* This routine fills the status of a non isochronous.
*
* RETURNS: N/A
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/

VOID usbUhcdFillNonisoStatus
    (
    UINT32 status,
    INT8 *statusInUrb,
    BOOL markedForDel,
    UINT8 reqThatHalted
    )
    {

    /*
     * If the pipe is being deleted and the pipe is still active
     * and the request did not result in a stall
     */

    if (markedForDel && (status & USBUHCD_TDCS_STS_ACTIVE) && !reqThatHalted)
        {
        USB_UHCD_WARN("%s[L%d]: pipe is being deleted \n",
                      __FILE__, __LINE__, 3, 4, 5, 6);

        /* record status as cancelled */

        *statusInUrb = USBHST_TRANSFER_CANCELLED;
        }

    /* If the pipe is not being deleted */

    else
        {

        /* If pipe has stalled */

        if ((status & USBUHCD_TDCS_STS_STALLED) || reqThatHalted)
            {
            USB_UHCD_WARN("%s[L%d]: pipe has stalled \n",
                          __FILE__, __LINE__, 3, 4, 5, 6);
            *statusInUrb = USBHST_STALL_ERROR ;
            }

        /* If Data underrun */

        else if (status & USBUHCD_TDCS_STS_DBUFERR)
            {
            USB_UHCD_WARN("%s[L%d]: data underrun \n",
                          __FILE__, __LINE__, 3, 4, 5, 6);
            *statusInUrb = USBHST_DATA_UNDERRUN_ERROR;
            }

        /* If Babble error */

        else if (status & USBUHCD_TDCS_STS_BABBLE)
            {
            USB_UHCD_WARN("%s[L%d]: babble error \n",
                          __FILE__, __LINE__, 3, 4, 5, 6);
            *statusInUrb = USBHST_FAILURE;
            }

        /* If NAK */

        else if (status & USBUHCD_TDCS_STS_NAK)
            {
            USB_UHCD_WARN("%s[L%d]: NAK \n",
                          __FILE__, __LINE__, 3, 4, 5, 6);
            *statusInUrb = USBHST_FAILURE;
            }

        /* If Time out error */

        else if (status & USBUHCD_TDCS_STS_TIME_CRC)
            {
            USB_UHCD_WARN("%s[L%d]: time out \n",
                          __FILE__, __LINE__, 3, 4, 5, 6);
            *statusInUrb = USBHST_TIMEOUT;
            }

        /* If Bit stuff error */

        else if (status & USBUHCD_TDCS_STS_BITSTUFF)
            {
            USB_UHCD_WARN("%s[L%d]: bit stuff error \n",
                          __FILE__, __LINE__, 3, 4, 5, 6);
            *statusInUrb = USBHST_FAILURE;
            }

        /* Everything is OK, normal transfer completion */

        else
            {
            *statusInUrb = USBHST_SUCCESS;
            }
        }/* End of else */

    }/* End of usbUhcdFillNonisoStatus() */



/***************************************************************************
*
* usbUhcdFillIsoStatus - fills the status of a isochronous
*
* This function is used to fill the status of a Isochronous data transfer
* upon completion, called by uhc_ProcessCompletedTDs.
*
* RETURNS: N/A
*
* ERRNO:
*   None
*
* \NOMANUAL
*/

VOID usbUhcdFillIsoStatus
    (
    UINT32                      uBusIndex,
    USBHST_STATUS               status,
    pUSBHST_URB                 pUrb,
    USB_UHCD_REQUEST_INFO  *iReQ
    )
    {
    /* Since it is an Isoc request the pUrb->pTransferSpecificData contains
     * the Isoc packets information
     */
    pUSBHST_ISO_PACKET_DESC pIsocPackets = NULL;

    /* To hold the TD for the current request */
    pUSB_UHCD_TD    iTD = NULL;

    /* To hold the number of TDs for the Request */
    UINT16  uCount = (UINT16)pUrb->uNumberOfPackets;

    UINT32 tdstatus;

    pIsocPackets = (pUSBHST_ISO_PACKET_DESC)pUrb->pTransferSpecificData;

    /* Updating the status on a per TD basis */
    iTD = iReQ->pHead;
    while (uCount)
        {
        /* Update the status */

        tdstatus = USB_UHCD_SWAP_DATA(uBusIndex, iTD->dWord1Td);

        /* Check for the various error conditions */

        if (tdstatus & USBUHCD_TDCS_STS_STALLED)
            {
            USB_UHCD_WARN("%s[L%d]: pipe has stalled \n",
                          __FILE__, __LINE__, 3, 4, 5, 6);
            pIsocPackets->nStatus = USBHST_STALL_ERROR ;
            }

        /* If Data underrun */


        else if (tdstatus & USBUHCD_TDCS_STS_DBUFERR)
            {
            USB_UHCD_WARN("%s[L%d]: data underrun \n",
                          __FILE__, __LINE__, 3, 4, 5, 6);
            pIsocPackets->nStatus = USBHST_DATA_UNDERRUN_ERROR;
            }

        /* If Babble error */

        else if (tdstatus & USBUHCD_TDCS_STS_BABBLE)
            {
            USB_UHCD_WARN("%s[L%d]: babble error \n",
                          __FILE__, __LINE__, 3, 4, 5, 6);
            pIsocPackets->nStatus = USBHST_STALL_ERROR;
            }

        else if (tdstatus & USBUHCD_TDCS_STS_TIME_CRC)
            {
            USB_UHCD_WARN("%s[L%d]: time out \n",
                          __FILE__, __LINE__, 3, 4, 5, 6);
            pIsocPackets->nStatus = USBHST_FAILURE;
            }

        /* If Bit stuff error */

       else if (tdstatus & USBUHCD_TDCS_STS_BITSTUFF)
            {
            USB_UHCD_WARN("%s[L%d]: bit stuff error \n",
                          __FILE__, __LINE__, 3, 4, 5, 6);
            pIsocPackets->nStatus = USBHST_FAILURE;
            }

        /* Everything is OK, normal transfer completion */

        else
            {
            pIsocPackets->nStatus = USBHST_SUCCESS;
            }

        /* Point to the next TD */
        iTD = iTD->vNextTd;

        /* Point to the next Isoc packet */
        pIsocPackets++;

        /* Decrement the count */
        uCount--;

        }/* End while (uCount)*/



    /* Update the status of the URB with status of Tail*/

    pIsocPackets--;
    pUrb->nStatus = pIsocPackets->nStatus;

    } /* End usbUhcdFillIsoStatus() */


/*******************************************************************************
*
* usbUhcdFreeTds - free up memory allocated for a chain of TDs
*
* This routine frees up memory allocated for a chain of TDs.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbUhcdFreeTds
    (
    pUSB_UHCD_DATA      pHCDData,
    USB_UHCD_TD *       start,
    USB_UHCD_TD *       stop
    )
    {
    /* Pointer to the TD */

    USB_UHCD_TD *td;

    /* Pointer to hold the next TD */

    USB_UHCD_TD *next;

    /* If the very first TD's addr is NULL, there is nothing to free */

    if (start == NULL || stop == NULL)
        {
        USB_UHCD_ERR("usbUhcdFreeTds - parameters are invalid \n",
                     1, 2, 3, 4, 5, 6);

        return;
        }

    /* Store the start address in the temporary pointer */

    td = start;

    /* Check if the request has only one TD... */

    if (start == stop)
        {
        /* Free up memory allocated for the TD */

        usbUhcdDestroyTD(pHCDData, td);

        return;
        }

    /* This loop gets executed till the address indicated by stop is reached */

    while (td != NULL)
        {

        /* Record link to next TD */

        next = td->hNextTd;

        /* Free up memory allocated for the TD */

        usbUhcdDestroyTD(pHCDData, td);

        /* If the last TD has been freed, break */

        if (td == stop || next == NULL)
            {
            USB_UHCD_VDBG("usbUhcdFreeTds - last TD is freed \n",
                          1, 2, 3, 4, 5, 6);
            break;
            }

        /* Update the 'td' pointer to the next element pointed */

        td = next;

        }/* End of while () */

    return;
    }/* End of usbUhcdFreeTds() */

/*******************************************************************************
*
* usbUhcdCanIsoBeAccomodated  - determine Isochronous transfer accomodation
*
* This routine is used to determine whether the Isochronous transfer can
* be accomodated, called by usbUhcdIsBandwidthAvailable.
*
* RETURNS: FALSE if isochronous endpoint cannot be accomodated.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOLEAN usbUhcdCanIsoBeAccomodated
    (
    INT32 bwReqd,
    INT32 startIndex,
    INT32 maxNumOfItd,
    USB_UHCD_PERIODIC_TABLE_ENTRY * usbUhcdTable,
    USB_UHCD_PERIODIC_TREE_NODE  * usbUhcdTree
    )
    {

    /* To hold the index value */

    INT32 i = 0;

    /* To hold the index into the number of isochronous TDs */

    INT32 n = 0;

    /*
     * From the desired position, till all desired positions
     * are scanned, check if the isochronous TD insertion disturbs the maximum
     * BW condition of the tree
     */

    for (i = startIndex, n = 0; n < maxNumOfItd; ++n)
        {

        /* If BW is exceeded, return error from the function */

        if ((INT32)usbUhcdTable[i].bandWidth +
            usbUhcdFindTreeBw(usbUhcdFindQlinkToTree(i), usbUhcdTree)+
            bwReqd + USB_UHCD_SAFETY_MARGIN >= USB_UHCD_NINETY_PERCENT_BW)
            {
            USB_UHCD_WARN("usbUhcdCanIsoBeAccomodated - BW exceeded \n",
                          1, 2, 3, 4, 5, 6);
            return(FALSE);
            }

        /* If not, move on to the next TD */

        else
            {
            USB_UHCD_VDBG("usbUhcdCanIsoBeAccomodated - moving to next TD\n",
                          1, 2, 3, 4, 5, 6);
            i++;

            if (i > (USBUHCD_FRAME_LIST_ENTRIES - 1))
                {
                i = 0;
                }
            }/* End of else */
        }/* End of for () */

    /* Return TRUE */

    return(TRUE);
    }/* End of usbUhcdCanIsoBeAccomodated() */


/*******************************************************************************
*
* usbUhcdLinkItds - link the isochronous TDs
*
* This routine is used to link the isochronous TDs to the HC list. This adds
* each TD to the periodic table.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbUhcdLinkItds
    (
    UINT8           uBusIndex,
    USB_UHCD_TD *   pITD,
    INT32           startIndex
    )
    {
    /* Copy the start index in the local variable */

    INT32           index = startIndex;

    /* To store the pointer to the previous isochronous TD */

    USB_UHCD_TD *   pPrevITD = NULL;

    /* Pointer to the Host Controller Data stucture */

    pUSB_UHCD_DATA  pHCDData = g_pUHCDData[uBusIndex];

    /* This loop will be executed till all the isochronous TDs are linked */

    while (pITD != NULL)
        {
        /*
         * Get the pointer to the iTD in the
         * index specified in the periodic table
         */

        pPrevITD = (USB_UHCD_TD *)(pHCDData->pPeriodicTable[index].pTD);

        /* Update link pointer of the isochronous TD */

        pITD->dWord0Td = pPrevITD->dWord0Td;

        /*
         * Indicate whether the next element is a QH or
         * TD by copying the value in the previous TD
         */

        /* Update the previous TD element */

        pITD->hPrevTd = pPrevITD;

        /* Update the next TD pointer */

        pITD->hNextTd = pPrevITD->hNextTd;

        /* Update the next TD's previous pointer */

        if (pITD->hNextTd)
            {
            USB_UHCD_VDBG("usbUhcdLinkItds - "
                          "updating the next TD's previous pointer \n",
                          1, 2, 3, 4, 5, 6);

            pITD->hNextTd->hPrevTd = pITD;
            }

        /* Update the next pointer of the previous pointer */

        pPrevITD->hNextTd = pITD;

        /* Update the HC TD's link pointer */

        USB_UHCD_TD_LINK_TD_ACTIVE(uBusIndex, pPrevITD, pITD);

        /* Update the pITD pointer to point to the next element */

        pITD = pITD->vNextTd;

        /* Move on to the next point of linkage */

        index++;

        /* If the index exceeds the maximum framelist number list, rollover */

        if (index > (USBUHCD_FRAME_LIST_ENTRIES - 1))
            {
            USB_UHCD_VDBG("usbUhcdLinkItds - "
                          "index %d exceeds max framelist number\n",
                          index, 2, 3, 4, 5, 6);
            index = 0;
            }
        }/* End of while () */
    }/* End of usbUhcdLinkItds() */


/*******************************************************************************
*
* usbUhcdUnlinkItds - unlink and frees the isochronous TDs
*
* This routine is used to unlink and free up the iTDs from the periodic table.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbUhcdUnlinkItds
    (
    pUSB_UHCD_DATA  pHCDData,
    USB_UHCD_TD *   itd,
    BOOLEAN         freeTds
    )
    {
    /* To hold the temporary TD pointer */

    USB_UHCD_TD *   pTempTD = NULL;

    /* This loop will be executed till all the isochronous TDs are unlinked */

    while (itd != NULL)
        {

        /* Indicate that the previous element is the last element in the list */

        /* itd->hPrevTd->dWord0Td.t = 1; */

        itd->hPrevTd->dWord0Td |= USB_UHCD_SWAP_DATA(pHCDData->uBusIndex,
                                        USBUHCD_LINK_TERMINATE);

        /* Update the HC TD's link pointer */

        itd->hPrevTd->dWord0Td = itd->dWord0Td;

        itd->hPrevTd->dWord0Td &= USB_UHCD_SWAP_DATA(pHCDData->uBusIndex,
                                        ~((ULONG)USBUHCD_LINK_TERMINATE));


        /* Update the next element pointers of the previous pointer */

        itd->hPrevTd->hNextTd = itd->hNextTd;

        /*
         * Update the next element's previous pointer to point to the
         * itd's previous pointer
         */

        if (itd->hNextTd)
            {
            USB_UHCD_VDBG("usbUhcdUnlinkItds - updating pointers \n",
                1, 2, 3, 4, 5, 6);

            itd->hNextTd->hPrevTd = itd->hPrevTd;
            }

        /* Store the next pointer in a temporary variable */

        pTempTD = itd->vNextTd;

        if (freeTds)
            {
            /* Free up the memory allocated for the iTD */

            usbUhcdDestroyTD(pHCDData, itd);
            }

        /* Update the new itd as the value stored in the temporary variable */

        itd = pTempTD;
        }/* End of while () */
    }/* End of usbUhcdUnlinkItds() */


/*******************************************************************************
*
* usbUhcdGetIsoTransferLength - get the isochronous transfer length
*
* This routine gets the isochronous transfer length.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbUhcdGetIsoTransferLength
    (
    UINT32                  uBusIndex,
    USB_UHCD_REQUEST_INFO * pRequest
    )
    {
    /* To hold the pointer to the TD */

    USB_UHCD_TD *   pTD = NULL;

    /* To hold the pointer to the Urb */

    USBHST_URB *    pUrb = pRequest->pUrb;

    /* To hold Isoc Specific information */

    pUSBHST_ISO_PACKET_DESC pIsocPackets =
        (pUSBHST_ISO_PACKET_DESC) pUrb->pTransferSpecificData;

    /* Copy the head of the isochronous request queue */

    pTD = pRequest->pHead;

    /*
     * This loop adds up the transfer length
     * starting from head to tail of the request queue
     */

    do
        {

        UINT32 len;

        /* Update the length Information on a per TD basis */

        /* if actual length not equal to zero */

        len = USBUHCD_TDCS_ACTLEN(USB_UHCD_SWAP_DATA(uBusIndex, pTD->dWord1Td));

        if (len != USBUHCD_TDCS_ACTLEN_ZERO)
            {
            /* Update the transfered length */

            pIsocPackets->uLength = len + 1 ;
            }
        else
            {
            /* ulength is zero */

            pIsocPackets->uLength = 0;
            }

        /* If the tail is not reached, move on to the next TD */

        if (pTD != pRequest->pTail)
            {
            pTD = pTD->vNextTd;

            /* Increment to the next Isoc packet */

            pIsocPackets++;

            }
        else
            {
            break;
            }
        } while (1);

    return;
    }/* End of usbUhcdGetIsoTransferLength() */

/********************* End of File UHCD_ScheduleQsupport.c **************************/


