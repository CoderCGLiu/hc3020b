/* usbMhdrcHcd.c - USB Mentor Graphics HCD Entry and Exit points */

/*
 * Copyright (c) 2009-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01x,21apr13,wyy  Cut the relationship among busIndex of HCDData, unitNumber of 
                 vxBus Device, and HC count (such as g_EHCDControllerCount or 
                 g_UhcdHostControllerCount) to dynamically release or announce
                 a HCD driver (WIND00362065)
01w,31jan13,ljg  add usb support for ti816x EVM
01v,22aug12,ljg  Fix coverity issue "CHECKED_RETURN' (WIND00371808)
01u,15sep11,m_y  add CPPIDMA support
01t,13dec11,m_y  Modify according to code check result (WIND00319317)
01s,21apr11,w_x  Check DMA enabled flag when enable/disable HCD DMA
01r,08apr11,w_x  Use USB_MHDRC_TASK_NAME_MAX for Coverity (WIND00264893)
01q,30mar11,w_x  Correct stack uninitialization with OCD (WIND00262787)
01p,15mar11,w_x  Move BSP specific init to platform init (WIND00258032)
01o,09mar11,w_x  Code clean up for make man
01n,14dec10,ghs  Change the usage of the reboot hook APIs to specific
                 rebootHookAdd/rebootHookDelete (WIND00240804)
01m,16sep10,m_y  Update the prototype for MHCI's exit routine (WIND00232860)
01l,13sep10,ghs  Fix defect found by code review of merge (WIND00232740)
01k,07sep10,ghs  Merge to vxWorks6.9 and LP64 adaption
01j,03jun10,s_z  Debug macro changed, Add more debug message
01i,16apr10,s_z  Remove unused event id
01h,13mar10,s_z  Add Inventra DMA supported on OMAP3EVM
01g,25feb10,s_z  Rename request list in USB_MHCD_DATA structure(WIND00201709)
01f,25jan10,s_z  Fix OMAP3530 detection issue when boot up and code cleaning
01e,05jan10,s_z  Add OMAP3530RevG support, which uses external power supply
01d,01dec09,s_z  Add Fixed FIFO size initialization routine(usbMhdrcFIFOInit)
01c,05nov09,s_z  Rewrite APIs
01b,03nov09,s_z  Update the data structures and funtions,some of them borrowed
                 from EHCD
01a,06jul09,j_x  initial version
*/

/*
DESCRIPTION

This file provides the entry and exit points for USB MHCD (Mentor Graphics USB
Host Controller Driver).

INCLUDE FILES:  vxWorks.h, rebootLib.h, hookLib.h,
                hwif/vxbus/vxBus.h, hwif/vxbus/vxbPciLib.h, hwif/vxbus/hwConf.h
                usb/usb.h usb/usbOsal.h usb/usbHst.h,
                usbMhdrc.h, usbMhdrcHcd.h usbMhdrcHcdIsr.h, usbMhdrcHcdSched.h,
                usbMhdrcHcdCore.h, usbMhdrcPlatform.h, usbMhdrcDma.h,
                usbMhdrcHcdDebug.h
*/

/* includes */

#include <vxWorks.h>
#include <rebootLib.h>
#include <hookLib.h>
#include <hwif/vxbus/vxBus.h>
#include <hwif/vxbus/vxbPciLib.h>
#include <hwif/vxbus/hwConf.h>
#include <usb/usb.h>
#include <usb/usbOsal.h>
#include <usb/usbHst.h>
#include "usbMhdrc.h"
#include "usbMhdrcHcd.h"
#include "usbMhdrcHcdIsr.h"
#include "usbMhdrcHcdSched.h"
#include "usbMhdrcHcdCore.h"
#include "usbMhdrcPlatform.h"
#include "usbMhdrcDma.h"
#include "usbMhdrcHcdDebug.h"

/* defines */

#define USB_MHCD_INT_THREAD_PRIORITY              (100)

/* The name register to vxBus */
#define USB_MHCI_HUB_NAME "vxbPlbUsbMhdrcHcdHub"
#define USB_MHCI_PLB_NAME "vxbPlbUsbMhdrcHcd"
#define USB_MHCI_PCI_NAME "vxbPciUsbMhdrcHcd"

/* globals */

/* To hold the array of pointers to the HCD maintained data structures */

pUSB_MHCD_DATA * gpMHCDData = NULL;

/* To hold the handle returned by the USBD during HC driver registration */

UINT32  gMHCDHandle = 0; /* This is actually an index so its safe for LP64 */

/* Number of host controllers present in the system */

UINT32  gMHCDControllerCount = 0;

/* Event used for synchronising the access of the free lists */

OS_EVENT_ID gMHCDListAccessEvent = NULL;

/* externals */

/* function to add root hub */

IMPORT void usbVxbRootHubAdd (VXB_DEVICE_ID pDev);

/* function to remove root hub */

IMPORT void usbVxbRootHubRemove (VXB_DEVICE_ID pDev);

/* forward declarations */

/* function to remove MHCI device */

LOCAL STATUS usbMhdrcHcdDeviceRemove (VXB_DEVICE_ID pDev);

/* function to intialize the MHCI device */

LOCAL void usbMhdrcHcdDeviceConnect (VXB_DEVICE_ID pDev);

LOCAL void usbVxbMhdrcHcdNullFunction (VXB_DEVICE_ID pDev);

/* Function to initailize the MHCI Host Controller */

LOCAL void usbMhdrcHcdPlbInit (VXB_DEVICE_ID pDev);

int usbMhdrcHcdDisable (int startType);

LOCAL PCI_DEVVEND usbVxbMhdrcHcdPciDevVendId[1] =
    {
       {
        0xffff,             /* device ID */
        0xffff              /* vendor ID */
       }
    };

LOCAL DRIVER_METHOD usbVxbMhdrcHcdMethods[2] =
    {
    DEVMETHOD (vxbDrvUnlink, usbMhdrcHcdDeviceRemove),
    DEVMETHOD_END
    };

LOCAL struct drvBusFuncs usbVxbHcdMhdrcDriverFuncs =
    {
    usbMhdrcHcdPlbInit,                               /* init 1 */
    usbVxbMhdrcHcdNullFunction,                       /* init 2 */
    usbMhdrcHcdDeviceConnect                          /* device connect */
    };

/* initialization structure for MHCI Root Hub */

LOCAL struct drvBusFuncs usbVxbMhdrcHcdRootHubDriverFuncs =
    {
    usbVxbMhdrcHcdNullFunction,                       /* init 1 */
    usbVxbMhdrcHcdNullFunction,                       /* init 2 */
    usbVxbRootHubAdd                                  /* device connect */
    };

/* method structure for MHCI Root Hub */

LOCAL struct vxbDeviceMethod usbVxbRootHubMethods[2] =
    {
    DEVMETHOD (vxbDrvUnlink, usbVxbRootHubRemove),
    DEVMETHOD_END
    };

/* DRIVER_REGISTRATION for MHCI Root hub */

LOCAL DRIVER_REGISTRATION usbVxbHcdMhdrcHcdHub =
    {
    NULL,                                   /* pNext */
    VXB_DEVID_BUSCTRL,                      /* hub driver is bus controller */
    VXB_BUSID_USB_HOST_MHCI,                /* parent bus ID */
    USB_VXB_VERSIONID,                      /* version */
    USB_MHCI_HUB_NAME,                      /* driver name */
    &usbVxbMhdrcHcdRootHubDriverFuncs,      /* struct drvBusFuncs */
    &usbVxbRootHubMethods[0],               /* struct vxbDeviceMethod */
    NULL,                                   /* probe routine */
    NULL                                    /* vxbParams */
    };

/* DRIVER_REGISTRATION for the MHCI device residing on PLB */

LOCAL DRIVER_REGISTRATION usbVxbPlbMhdrcHcdDevRegistration =
    {
    NULL,
    VXB_DEVID_BUSCTRL,                             /* bus controller */
    VXB_BUSID_PLB,                                 /* bus id - PLB Bus Type */
    USB_VXB_VERSIONID,                             /* vxBus version Id */
    USB_MHCI_PLB_NAME,                             /* drv name */
    &usbVxbHcdMhdrcDriverFuncs,                    /* pDrvBusFuncs */
    &usbVxbMhdrcHcdMethods[0],                     /* pMethods */
    NULL,                                          /* probe routine */
    NULL                                           /* vxbParams */
    };

/* DRIVER_REGISTRATION for MHCI device residing on PCI Bus */

LOCAL PCI_DRIVER_REGISTRATION usbVxbPciMhdrcHcdDevRegistration =
    {
        {
        NULL,
        VXB_DEVID_BUSCTRL,                         /* bus controller */
        VXB_BUSID_PCI,                             /* bus id - PCI Bus Type */
        USB_VXB_VERSIONID,                         /* vxBus version Id */
        USB_MHCI_PCI_NAME,                         /* drv name */
        &usbVxbHcdMhdrcDriverFuncs,                /* pDrvBusFuncs */
        &usbVxbMhdrcHcdMethods [0],                /* pMethods */
        NULL,                                      /* probe routine */
        NULL                                       /* vxbParams */
        },
    NELEMENTS(usbVxbMhdrcHcdPciDevVendId),         /* idListLen */
    &usbVxbMhdrcHcdPciDevVendId [0]                /* idList */
    };


/*******************************************************************************
*
* usbVxbMhdrcHcdInstantiate - instantiate Mentor Graphics USB host controller driver
*
* This routine instantiates the Mentor Graphics USB host controller driver and
* allows the Mentor Graphics USB host controller driver to be included with the
* vxWorks image but not registered with VxBus. Devices will remain orphan devices
* until the usbMhdrcInit() routine is called. This supports the INCLUDE_USB_MHDRC_HCD
* behaviour of previous vxWorks releases.
*
* The routine itself does nothing.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void usbVxbMhdrcHcdInstantiate (void)
    {
    return;
    }

/*******************************************************************************
*
* usbMhdrcHcdInit - initialize the USB Mentor Graphics host controller driver
*
* This routine initializes internal data structures in the Mentor Graphics
* USB host controller driver. This routine is typically called prior to the
* VxBus invocation of the device connect.
*
* The USBD must be initialized prior to calling this routine.
* This routine registers the Mentor Graphics HCD with the USBD Layer.
*
* RETURNS: OK, or ERROR if the initialization fails
*
* ERRNO: N/A
*/

STATUS usbMhdrcHcdInit (void)
    {
    USBHST_STATUS       status = USBHST_FAILURE;
    USBHST_HC_DRIVER    hostControllerDriverEntry;

    /*
     * Do not check gMHCDHandle for 0 to determine if it is the first time
     * to call this routine, becasue it is an index, it can be 0. The
     * gpMHCDData check below is enough to make sure the driver is initalized
     * only once.
     */

    if (gpMHCDData != NULL)
        {
        USB_MHDRC_ERR("MHCD already initialized\n",
                     1, 2, 3, 4, 5 ,6);

        return ERROR;
        }

    /* Initialize the global array */

    gpMHCDData = (pUSB_MHCD_DATA *)OS_MALLOC
                      (sizeof(pUSB_MHCD_DATA) * USB_MAX_MHCI_COUNT);

    /* Check if memory allocation is successful */

    if (gpMHCDData == NULL)
        {
        USB_MHDRC_ERR("Allocation failed for gpMHCDData\n",
                     1, 2, 3, 4, 5 ,6);

        return ERROR;
        }

    /* Reset the global array */

    OS_MEMSET (gpMHCDData, 0, sizeof(pUSB_MHCD_DATA) * USB_MAX_MHCI_COUNT);

    /* Create the event used for synchronising the free list accesses */

    gMHCDListAccessEvent = OS_CREATE_EVENT(OS_EVENT_SIGNALED);

    /* Check if the event is created successfully */

    if (NULL == gMHCDListAccessEvent)
        {
        USB_MHDRC_ERR("Error in creating the list event\n",
                     1, 2, 3, 4, 5 ,6);

        /* free the global array */

        OS_FREE (gpMHCDData);

        gpMHCDData = NULL;

        return ERROR;
        }

    /* Hook the routine which needs to be called on a reboot */

    if (ERROR == rebootHookAdd((FUNCPTR)usbMhdrcHcdDisable))
        {
        USB_MHDRC_ERR("Error in hooking the MhdrcHcd routine to disable HC\n",
                     1, 2, 3, 4, 5 ,6);

        /* Destroy the event */

        OS_DESTROY_EVENT(gMHCDListAccessEvent);

        gMHCDListAccessEvent = NULL;

        /* free the global array */

        OS_FREE (gpMHCDData);

        gpMHCDData = NULL;

        return ERROR;
        }

    /* Initialize the members of the data structure */

    OS_MEMSET(&hostControllerDriverEntry, 0, sizeof(USBHST_HC_DRIVER));

    /* Populate the members of the HC Driver data structure - start */

    /* Function to create the pipes for the endpoint */

    hostControllerDriverEntry.createPipe = usbMhdrcHcdCreatePipe;

    /* Function to delete a pipe corresponding to an endpoint */

    hostControllerDriverEntry.deletePipe = usbMhdrcHcdDeletePipe;

    /* This function is used to submit a request to the device */

    hostControllerDriverEntry.submitURB = usbMhdrcHcdSubmitURB;

    /* This function is used to cancel a request submitted to the device */

    hostControllerDriverEntry.cancelURB = usbMhdrcHcdCancelURB;

    /*
     * Function to determine whether the bandwidth is available to support the
     * new configuration or an alternate interface.
     */

    hostControllerDriverEntry.isBandwidthAvailable = usbMhdrcHcdIsBandwidthAvailable;

    /*
     * Function to modify the properties of the default control pipe, i.e. the
     * pipe corresponding to Address 0, Endpoint 0.
     */

    hostControllerDriverEntry.modifyDefaultPipe = usbMhdrcHcdModifyDefaultPipe;

    /* Function to check whether any request is pending on a pipe */

    hostControllerDriverEntry.isRequestPending = usbMhdrcHcdIsRequestPending;

    /* Function to obtain the current frame number */

    hostControllerDriverEntry.getFrameNumber = usbMhdrcHcdGetFrameNumber;

    /* Function to modify the frame width */

    hostControllerDriverEntry.setBitRate = usbMhdrcHcdSetBitRate;

    /* Register the Mentor Graphics host controller driver with USBD */

    status = usbHstHCDRegister
                 (&hostControllerDriverEntry,
                  &gMHCDHandle,
                  NULL, /* Context for USB 2.0 TT but not supported yet */
                  VXB_BUSID_USB_HOST_MHCI);

    /* Check whether the registration is successful */

    if (USBHST_SUCCESS != status)
        {
        USB_MHDRC_ERR("Error in registering the HCD \n",
                     1, 2, 3, 4, 5 ,6);

        /* Destroy the event */

        OS_DESTROY_EVENT(gMHCDListAccessEvent);

        gMHCDListAccessEvent = NULL;

        /* Free the global array */

        OS_FREE (gpMHCDData);

        gpMHCDData = NULL;

        gMHCDHandle = 0;

        if (ERROR == rebootHookDelete(usbMhdrcHcdDisable))
            {
            USB_MHDRC_ERR("Failure in rebootHookDelete\n",
                          1, 2, 3, 4, 5 ,6);
            }

        return ERROR;
        }

    return OK;
    }


/*******************************************************************************
*
* usbMhdrcHcdExit - uninitialize the MHCI USB Host Controller Driver
*
* This routine uninitializes the MHCI USB Host Controller Driver and detaches
* it from the USBD interface layer.
*
* RETURNS: OK, or ERROR if there is an error during HCD uninitialization.
*
* ERRNO: N/A
*/

STATUS usbMhdrcHcdExit (void)
    {
    USBHST_STATUS Status = USBHST_FAILURE;

    /* Deregister the HCD from USBD */

    Status = usbHstHCDDeregister(gMHCDHandle);

    /* Check if HCD is deregistered successfully */

    if (USBHST_SUCCESS != Status)
        {
        USB_MHDRC_ERR("Failure in deregistering the HCD\n",
                     1, 2, 3, 4, 5 ,6);

        return ERROR;
        }

    if (ERROR == rebootHookDelete(usbMhdrcHcdDisable))
        {
        USB_MHDRC_ERR("Failure in rebootHookDelete\n",
                     1, 2, 3, 4, 5 ,6);

        return ERROR;
        }

    gMHCDHandle = 0;

    /* Here we need disable the controller */

    usbMhdrcHcdDisable(0);

    /* Destroy the event used for synchronisation of the free list */

    OS_DESTROY_EVENT(gMHCDListAccessEvent);

    gMHCDListAccessEvent = NULL;

    /* Free the memory allocated for the global data structure */

    OS_FREE(gpMHCDData);

    gpMHCDData = NULL;

    return OK;
    }


/*******************************************************************************
* vxbUsbMhdrcHcdRegister - register the MHCI USB HCD with vxBus
*
* This routine registers the MHCI USB Host Controller Driver and MHCI Root-hub
* driver with vxBus. Note that this can be called early in the initialization
* sequence.
*
* RETURNS: OK, or ERROR if registeration failed.
*
* ERRNO: N/A
*/

STATUS vxbUsbMhdrcHcdRegister (void)
    {
    /* Register the MHCD driver for PCI bus as underlying bus */

    if (vxbDevRegister ((DRIVER_REGISTRATION *)
        &usbVxbPciMhdrcHcdDevRegistration)
        == ERROR)
        {
        USB_MHDRC_ERR("Error registering MHCD Driver over PCI Bus\n",
                     1, 2, 3, 4, 5 ,6);

        return ERROR;
        }

    /* Register the MHCD driver for PLB bus as underlying bus */

    if (vxbDevRegister ((DRIVER_REGISTRATION *)
        &usbVxbPlbMhdrcHcdDevRegistration)
        == ERROR)
        {
        USB_MHDRC_ERR("Error registering MHCD Driver over PLB Bus\n",
                     1, 2, 3, 4, 5 ,6);

        return ERROR;
        }

    /* Register the MHCD driver for root hub */

    if (vxbDevRegister ((DRIVER_REGISTRATION *)
        &usbVxbHcdMhdrcHcdHub)
        == ERROR)
        {
        USB_MHDRC_ERR("vxbUsbMhdrcHcdRegister(): "
                     "Error registering MHCD root hub\n",
                     1, 2, 3, 4, 5 ,6);

        return ERROR;
        }

    /* Set the interfaces for OCD to use as a hook */

    gpUsbMhdrcHcdLoad = usbMhdrcHcdInstanceAdd;
    gpUsbMhdrcHcdUnLoad = usbMhdrcHcdInstanceRemove;
    gpUsbMhdrcHcdEnable = usbMhdrcHcdInstanceEnable;
    gpUsbMhdrcHcdDisable = usbMhdrcHcdInstanceDisable;

    return OK;
    }


/*******************************************************************************
*
* vxbUsbMhdrcHcdDeregister - deregister MHCI USB HCD with vxBus
*
* This routine deregisters the MHCD USB HCD with vxBus. The routine
* first de-registers the MHCD Root hub. This is followed by deregistration of
* MHCD Controller for PCI and PLB bus types
*
* RETURNS: OK, or ERROR if not able to deregister with vxBus
*
* ERRNO: N/A
*/

STATUS vxbUsbMhdrcHcdDeregister (void)
    {
    /* Deregister the MHCD root hub as bus controller driver */

    if (vxbDriverUnregister (&usbVxbHcdMhdrcHcdHub) == ERROR)
        {
        USB_MHDRC_ERR("vxbUsbMhdrcHcdDeregister(): "
                     "Error de-registering MHCD Root Hub with vxBus\n",
                     1, 2, 3, 4, 5 ,6);

        return ERROR;
        }

    /* Deregister the MHCD driver for PLB bus as underlying bus */

    if (vxbDriverUnregister (&usbVxbPlbMhdrcHcdDevRegistration) == ERROR)
        {
        USB_MHDRC_ERR("Error de-registering MHCD Driver over PLB Bus\n",
                     1, 2, 3, 4, 5 ,6);

        return ERROR;
        }

    /* Deregisterr the MHCD driver for PCI bus as underlying bus */

    if (vxbDriverUnregister ((DRIVER_REGISTRATION *)
        &usbVxbPciMhdrcHcdDevRegistration)
        == ERROR)
        {
        USB_MHDRC_ERR("Error de-registering MHCD Driver over PCI Bus\n",
                     1, 2, 3, 4, 5 ,6);

        return ERROR;
        }

    /* Clear the interfaces for OCD */

    gpUsbMhdrcHcdLoad = NULL;
    gpUsbMhdrcHcdUnLoad = NULL;
    gpUsbMhdrcHcdEnable = NULL;
    gpUsbMhdrcHcdDisable = NULL;

    return OK;
    }


/*******************************************************************************
*
* usbVxbMhdrcHcdNullFunction - dummy routine
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbVxbMhdrcHcdNullFunction
    (
    VXB_DEVICE_ID       pDev
    )
    {

    /* This is a dummy routine which simply returns */

    return ;
    }

/*******************************************************************************
*
* usbMhdrcHcdReboot - perform the host bus specific initialization
*
* This routine performs the host bus specific initialization of the
* MHCI USB Host Controller.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcHcdReboot
    (
    void
    )
    {
    pUSB_MHCD_DATA pHCDData = NULL;
    int            i = 0;

    /*
     * Recently we do not use this routine
     * It can be used in the debugging project.
     */

    for (i = 0; i < USB_MAX_MHCI_COUNT; i ++)
        {
        pHCDData = gpMHCDData[i];

        usbMhdrcHardwareIntrDisable(pHCDData->pMHDRC);

        usbMhdrcHardwareIntrEnable(pHCDData->pMHDRC);
        }

    return;
    }

/*******************************************************************************
*
* usbMhdrcHcdDataUnInit - free the initialized resouces
*
* This routine frees all the resources which have been initialized in
* usbMhdrcHcdDataInit routine.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcHcdDataUnInit
    (
    pUSB_MHCD_DATA pHCDData
    )
    {
    if (NULL == pHCDData)
        {
        USB_MHDRC_ERR("usbMhdrcHcdDataUnInit(): "
                     "Invalid parameter, pHCDData is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }

    /* Destroy the thread created for handling the interrupts */

    if (pHCDData->IntHandlerThreadID != OS_THREAD_FAILURE
        && pHCDData->IntHandlerThreadID != 0)
        {
        OS_DESTROY_THREAD(pHCDData->IntHandlerThreadID);
        pHCDData->IntHandlerThreadID = OS_THREAD_FAILURE;
        }

    /* Destroy the signalling event */

    if (NULL != pHCDData->isrEvent)
        {
        OS_DESTROY_EVENT(pHCDData->isrEvent);
        pHCDData->isrEvent = NULL;
        }

    /* Destroy the thread created for handling the interrupts */

    if (pHCDData->TransferThreadID != OS_THREAD_FAILURE
        && pHCDData->TransferThreadID != 0)
        {
        OS_DESTROY_THREAD(pHCDData->TransferThreadID);
        pHCDData->TransferThreadID = OS_THREAD_FAILURE;
        }

    /* Destroy the MsgQ  */

    if (pHCDData->TransferThreadMsgID)
        {
        msgQDelete (pHCDData->TransferThreadMsgID);
        pHCDData->TransferThreadMsgID = NULL;
        }

    if (pHCDData->pHcdSynchMutex)
        {
        /* Mutex should be taken before delete it */

        semDelete (pHCDData->pHcdSynchMutex);
        pHCDData->pHcdSynchMutex = NULL;
        }

    /* Free memory allocated for the default pipe */

    if (NULL != pHCDData->pDefaultPipe)
        {

        if (NULL != pHCDData->pDefaultPipe->pPipeSynchMutex)
            {
            /* Mutex should be taken before delete it */

            semTake (pHCDData->pDefaultPipe->pPipeSynchMutex, WAIT_FOREVER);
            semDelete (pHCDData->pDefaultPipe->pPipeSynchMutex);
            pHCDData->pDefaultPipe->pPipeSynchMutex = NULL;
            }
        OS_FREE(pHCDData->pDefaultPipe);
        pHCDData->pDefaultPipe = NULL;
        }

    /* Free memory allocated for the interrupt data */

    if (NULL != pHCDData->RHData.pHubInterruptData)
        {
        OS_FREE(pHCDData->RHData.pHubInterruptData);
        pHCDData->RHData.pHubInterruptData = NULL;
        }

    return;
    }


/*******************************************************************************
*
* usbMhdrcHcdDataInit - initialize USB MHCD data structure.
*
* This routine initializes the usb_mhcd_data structure.
*
* RETURNS: OK, or ERROR if usb_mhcd_data initialization is unsuccessful.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbMhdrcHcdDataInit
    (
    pUSB_MHCD_DATA   pHCDData
    )
    {
    UCHAR  ThreadName[USB_MHDRC_TASK_NAME_MAX]; /* To hold the name of the thread */
    UCHAR  TransferThreadName[USB_MHDRC_TASK_NAME_MAX];
    UINT32 uSize = 0; /* Temporary variable for Root hub interrupt data size */

    /* Check the validity of the parameter */

    if (NULL == pHCDData)
        {
        USB_MHDRC_ERR("Invalid parameter, pHCDData is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return ERROR;
        }

    /* Init root hub related data */

    pHCDData->RHData.uNumDownstreamPorts = pHCDData->pMHDRC->uRootHubNumPorts;

    /* Check if the number of ports is valid */

    if (0 == pHCDData->RHData.uNumDownstreamPorts)
        {
        USB_MHDRC_ERR("Invalid parameter, uNumDownstreamPorts is 0\n",
                     1, 2, 3, 4, 5 ,6);

        return ERROR;

        }

    /* Allocate memory for the Root hub port status */

    pHCDData->RHData.pPortStatus = (UCHAR *)
                                    OS_MALLOC(pHCDData->RHData.uNumDownstreamPorts
                                    * USB_HUB_STATUS_SIZE);

    /* Check if memory allocation is successful */

    if (NULL == pHCDData->RHData.pPortStatus)
        {
        USB_MHDRC_ERR("Memory not allocated for port status\n",
                     1, 2, 3, 4, 5 ,6);

        usbMhdrcHcdDataUnInit(pHCDData);

        return ERROR;
        }

    /* Initialize the fields of the port status register */

    OS_MEMSET(pHCDData->RHData.pPortStatus, 0,
              (pHCDData->RHData.uNumDownstreamPorts
              * USB_HUB_STATUS_SIZE));

    /*
     * The interrupt transfer data is of the following format
     * D0 - Holds the status change information of the hub
     * D1 - Holds the status change information of the Port 1
     *
     * Dn - holds the status change information of the Port n
     * So if the number of downstream ports is N, the size of interrupt
     * transfer data would be N + 1 bits.
     */

    uSize = (pHCDData->RHData.uNumDownstreamPorts + 1)/ 8;
    if (0 != ((pHCDData->RHData.uNumDownstreamPorts + 1) % 8))
        {
        uSize = uSize + 1;
        }

    /* Allocate memory for the interrupt transfer data */

    pHCDData->RHData.pHubInterruptData = OS_MALLOC(uSize);

    /* Check if memory allocation is successful */

    if (NULL == pHCDData->RHData.pHubInterruptData)
        {
        USB_MHDRC_ERR("Memory not allocatedfor interrupt transfer data\n",
                     1, 2, 3, 4, 5 ,6);

        usbMhdrcHcdDataUnInit(pHCDData);

        return ERROR;
        }

    /* Initialize the interrupt data */

    OS_MEMSET(pHCDData->RHData.pHubInterruptData, 0, uSize);

    /* Copy the size of the interrupt data */

    pHCDData->RHData.uSizeInterruptData = uSize;

    /* Create the default pipe */

    pHCDData->pDefaultPipe = (pUSB_MHCD_PIPE)OS_MALLOC(sizeof(USB_MHCD_PIPE));

    /* Check if default pipe is created successfully */

    if (NULL == pHCDData->pDefaultPipe)
        {
        USB_MHDRC_ERR("Memory not allocated for the default pipe\n",
                     1, 2, 3, 4, 5 ,6);

        usbMhdrcHcdDataUnInit(pHCDData);

        return ERROR;
        }

    /* Initialize the USB_MHCD_PIPE data structure */

    OS_MEMSET(pHCDData->pDefaultPipe, 0, sizeof(USB_MHCD_PIPE));
    pHCDData->pDefaultPipe->uPipeFlag = USB_MHCD_PIPE_FLAG_OPEN;

    /* Record the endpoint type */

    pHCDData->pDefaultPipe->uEndpointType = USB_ATTR_CONTROL;

    pHCDData->pDefaultPipe->pPipeSynchMutex =
        semMCreate (USB_MHDRC_HCD_MUTEX_CREATION_OPTS);

    if (NULL == pHCDData->pDefaultPipe->pPipeSynchMutex)
        {
        USB_MHDRC_ERR("Memory not allocated for the default pipe\n",
                     1, 2, 3, 4, 5 ,6);

        usbMhdrcHcdDataUnInit(pHCDData);

        return ERROR;
        }

    /* Init the requestinfo list */

    lstInit(&(pHCDData->pDefaultPipe->RequestInfoList));

    /* Create the event which is used for signalling the thread on interrupt */

    pHCDData->isrEvent = OS_CREATE_EVENT(OS_EVENT_NON_SIGNALED);

    /* Check if the event is created successfully */

    if (NULL == pHCDData->isrEvent)
        {
        USB_MHDRC_ERR("ISR Signal event not created\n", 1, 2, 3, 4, 5 ,6);

        usbMhdrcHcdDataUnInit(pHCDData);

        return ERROR;
        }

    pHCDData->pHcdSynchMutex = semMCreate (USB_MHDRC_HCD_MUTEX_CREATION_OPTS);

    if (NULL == pHCDData->pHcdSynchMutex)
        {
        USB_MHDRC_ERR("Synch event not created\n", 1, 2, 3, 4, 5 ,6);

        usbMhdrcHcdDataUnInit(pHCDData);

        return ERROR;
        }

    /* Init the Schedule list for transfer data */

    lstInit(&(pHCDData->PeriodicPipeList));
    lstInit(&(pHCDData->NonPeriodicPipeList));

    /* Init the idleHardwareChannel, 1 means idle */

    pHCDData->uFreeEpChannelBitMap = (0x1 << USB_MHDRC_ENDPOINT_MAX) - 0x1;

    /* Assign an unique name for the interrupt handler thread */

    snprintf((char*)ThreadName,
        USB_MHDRC_TASK_NAME_MAX, "MHCD_IH%d", pHCDData->uBusIndex);

    /* Create the interrupt handler thread for handling the interrupts */

    pHCDData->IntHandlerThreadID = OS_CREATE_THREAD((char *)ThreadName,
                                                    USB_MHCD_INT_THREAD_PRIORITY,
                                                    usbMhdrcHcdInterruptProcessHandler,
                                                    pHCDData);

    /* Check whether the thread creation is successful */

    if (OS_THREAD_FAILURE == pHCDData->IntHandlerThreadID)
        {
        USB_MHDRC_ERR("Interrupt handler thread is not created\n",
                     1, 2, 3, 4, 5 ,6);

        usbMhdrcHcdDataUnInit(pHCDData);

        return ERROR;
        }

    /* Create the event which is used for signalling the thread on interrupt */

    pHCDData->TransferThreadMsgID = msgQCreate (USB_MHDRC_HCD_MAX_MSG,
                                            sizeof(USB_MHCD_TRANSFER_TASK_INFO),
                                            MSG_Q_FIFO);

    if (NULL == pHCDData->TransferThreadMsgID)
        {
        USB_MHDRC_ERR("Transfer Thread MsgID not created\n", 1, 2, 3, 4, 5 ,6);

        usbMhdrcHcdDataUnInit(pHCDData);

        return ERROR;
        }

    /* Assign an unique name for the transfer mamangement task thread */

    snprintf((char*)TransferThreadName,
        USB_MHDRC_TASK_NAME_MAX, "MHCD_XM%d", pHCDData->uBusIndex);

    /* Create the interrupt handler thread for handling the interrupts */

    pHCDData->TransferThreadID = OS_CREATE_THREAD((char *)TransferThreadName,
                                                    USB_MHCD_INT_THREAD_PRIORITY,
                                                    usbMhdrcHcdTransferManagementTask,
                                                    pHCDData);

    /* Check whether the thread creation is successful */

    if (OS_THREAD_FAILURE == pHCDData->TransferThreadID)
        {
        USB_MHDRC_ERR("Interrupt handler thread is not created\n",
                     1, 2, 3, 4, 5 ,6);

        usbMhdrcHcdDataUnInit(pHCDData);

        return ERROR;
        }

    return OK;
    }


/*******************************************************************************
*
* usbMhdrcHcdDisable - reboot hook to disable the host controller
*
* This routine is called on a warm reboot to disable the host controller by
* the BSP.
*
* RETURNS: 0, always
*
* ERRNO: N/A
*
* \NOMANUAL
*/

int usbMhdrcHcdDisable
    (
    int startType
    )
    {
    /* Pointer to the HCD data structure */

    pUSB_MHCD_DATA pHCDData = NULL;
    UINT8          index = 0;

    if ((0 == gMHCDControllerCount) ||
        (NULL == gMHCDListAccessEvent) ||
        (gpMHCDData == NULL))
        {
        return 0;
        }

    /* This loop releases the resources for all the host controllers present */

    for (index = 0; index < USB_MAX_MHCI_COUNT; index ++)
        {
        /* Extract the pointer from the global array */

        pHCDData = gpMHCDData[index];

        /* Check if the pointer is valid */

        if (pHCDData == NULL)
            continue;

        /* Disable the hardware interrupt */

        usbMhdrcHardwareIntrDisable(pHCDData->pMHDRC);

        /* Un-hook the ISR from the interrupt line */

        usbMhdrcPlatformHardwareUnInit(pHCDData->pMHDRC);
        }

    return 0;
    }


/*******************************************************************************
*
* usbMhdrcHcdDeviceConnect - initialize the Mentor Graphics USB host controller
*
* This routine initializes Mentor Graphics host controller and activates it
* to handle all USB operations. The routine is called by vxBus on a successful
* driver - device match with VXB_DEVICE_ID as parameter. This structure has all
* information about the device. This routine resets all controller registers and
* then initializes them with default values. Once all the registers are properly
* initialized, it sets the host controller into the operational state.
*
* RETURN: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcHcdDeviceConnect
    (
    VXB_DEVICE_ID pDev    /* struct vxbDev */
    )
    {
    pUSB_MUSBMHDRC  pMHDRC;

    /* Paramter verification */

    if (pDev == NULL)
        {
        USB_MHDRC_ERR("Invalid Parameters, pDev NULL\n",
                     1, 2, 3, 4, 5 ,6);
        return;
        }

    /* Allocate memory for the USB_MUSBMHDRC structure */

    pMHDRC = (pUSB_MUSBMHDRC)OS_MALLOC(sizeof(USB_MUSBMHDRC));

    /* Check if memory allocation is successful */

    if (NULL == pMHDRC)
        {
        USB_MHDRC_ERR("Memory not allocated for USB_MUSBMHDRC\n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }

    /* Clear the memory allocated */

    OS_MEMSET(pMHDRC, 0, sizeof(USB_MUSBMHDRC));

    /* Set the VxBus Device instance */

    pMHDRC->pDev = pDev;

    /* Save the common hardware information as driver control */

    pDev->pDrvCtrl = pMHDRC;

    /* Initialize the common hardware data structure */

    if (usbMhdrcPlatformHardwareInit(pMHDRC) != OK)
        {
        USB_MHDRC_ERR("usbMhdrcPlatformHardwareInit failed\n",
                  1, 2, 3, 4, 5, 6);

        OS_FREE(pMHDRC);

        pDev->pDrvCtrl = NULL;

        return;
        }

    /* Add the HCD bus instance */

    if (usbMhdrcHcdInstanceAdd(pMHDRC) != OK)
        {
        USB_MHDRC_ERR("usbMhdrcHcdInstanceAdd failed\n",
                  1, 2, 3, 4, 5, 6);

        /* Uninitialize the hardware */

        usbMhdrcPlatformHardwareUnInit(pMHDRC);

        OS_FREE(pMHDRC);

        pDev->pDrvCtrl = NULL;

        return;
        }

    /* Enable the HCD bus instance */

    if (usbMhdrcHcdInstanceEnable(pMHDRC) != OK)
        {
        USB_MHDRC_ERR("usbMhdrcHcdInstanceEnable failed\n",
                  1, 2, 3, 4, 5, 6);

        /* Remove the HCD instance */

        usbMhdrcHcdInstanceRemove(pMHDRC);

        /* Uninitialize the hardware */

        usbMhdrcPlatformHardwareUnInit(pMHDRC);

        OS_FREE(pMHDRC);

        pDev->pDrvCtrl = NULL;

        return;
        }

    pMHDRC->isrMagic = USB_MHDRC_MAGIC_ALIVE;

    /* Enable HC interrupt */

    usbMhdrcHardwareIntrEnable(pMHDRC);

    return;
    }


/*******************************************************************************
*
* usbMhdrcHcdDeviceRemove - remove the MHCI Host Controller device
*
* This routine un-initializes the USB host controller device. The routine
* is registered with vxBus and called when the driver is de-registered with
* vxBus. The routine will have VXB_DEVICE_ID as its parameter. This structure
* consists of all the information about the device. The routine will
* subsequently de-register the bus from USBD.
*
* <pDev> - vxBus Device Id.
*
* RETURNS: OK, or ERROR if not able to remove the MHCD
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbMhdrcHcdDeviceRemove
    (
    VXB_DEVICE_ID pDev /* struct vxbDev */
    )
    {
    pUSB_MUSBMHDRC      pMHDRC;

    /* Paramter verification */

    if ((pDev == NULL) || (pDev->pDrvCtrl == NULL))
        {
        USB_MHDRC_ERR("Invalid Parameters, pDev or pDev->pDrvCtrl NULL\n",
                     1, 2, 3, 4, 5 ,6);
        return ERROR;
        }

    /* Extract the pointer from the VxBus Device instance */

    pMHDRC = pDev->pDrvCtrl;

    /* Stop any interrupt reporting ASAP */

    pMHDRC->isrMagic = USB_MHDRC_MAGIC_DEAD;

    /* Disable the hardware interrupts */

    usbMhdrcHardwareIntrDisable(pMHDRC);

    /* Disable the HC bus instance */

    usbMhdrcHcdInstanceDisable(pMHDRC);

    /* Remove the device instance */

    return usbMhdrcHcdInstanceRemove(pMHDRC);
    }


/*******************************************************************************
*
* usbMhdrcHcdPlbInit - perform platform specific Initializaion
*
* For many PLB based controllers, different BSP level initialization is
* requried. This routine does the BSP specific initializaiton in case it is
* required
*
* RETURNS: N/A
*
* ERRNO: N/A
*
*\NOMANUAL
*/

LOCAL void usbMhdrcHcdPlbInit
    (
    VXB_DEVICE_ID pDev
    )
    {
    /*
     * The BSP specific initializaton will be done by the platform
     * initalizaton hook in usbMhdrcPlatformHardwareInit(), which
     * will make the boot process quicker since it is called in the
     * VxBus tDevConn task where communication with the PHY using
     * slow link such as I2C is more proper.
     */

    return;
    }

/*******************************************************************************
*
* usbMhdrcHcdInstanceRemove - remove a host controller instance
*
* This routine is to remove a host controller instance.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbMhdrcHcdInstanceRemove
    (
    pUSB_MUSBMHDRC      pMHDRC
    )
    {
    pUSB_MHCD_DATA      pHCDData;

    OS_WAIT_FOR_EVENT (gMHCDListAccessEvent, WAIT_FOREVER);

    /* Check if the pointer is valid */

    if ((NULL == pMHDRC) || (NULL == pMHDRC->pHCDData))
        {
        USB_MHDRC_ERR("Invalid parameter, pHCDData NULL\n",
                     1, 2, 3, 4, 5 ,6);

        OS_RELEASE_EVENT (gMHCDListAccessEvent);

        return ERROR;
        }

    /* Get the HCD data */

    pHCDData = pMHDRC->pHCDData;

    /*
     * Call the function to de-register the bus and
     * Check if the bus is deregistered successfully
     */

    if (USBHST_SUCCESS != usbHstBusDeregister(gMHCDHandle,
                                 pHCDData->uBusIndex,
                                 (ULONG)pHCDData->pDefaultPipe))
        {
        USB_MHDRC_ERR("Failure in deregistering the bus\n",
                     1, 2, 3, 4, 5 ,6);

        OS_RELEASE_EVENT (gMHCDListAccessEvent);

        return ERROR;
        }

    /* Call the function to uninitialize the MHCD data structure */

    usbMhdrcHcdDataUnInit(pHCDData);

    gpMHCDData[pHCDData->uBusIndex] = NULL;

    /* Release the memory allocated for the HCD data structure */

    MHDRC_FREE(pHCDData);

    /* Decrement the global counter by 1 */

    gMHCDControllerCount--;

    pMHDRC->pHCDData = NULL;

    OS_RELEASE_EVENT (gMHCDListAccessEvent);

    return OK;
    }

/*******************************************************************************
*
* usbMhdrcHcdInstanceAdd - add a host controller instance
*
* This routine is to add a host controller instance.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbMhdrcHcdInstanceAdd
    (
    pUSB_MUSBMHDRC      pMHDRC
    )
    {
    pUSB_MHCD_DATA      pHCDData;
    VXB_DEVICE_ID       pDev;
    UINT8               index;

    if (!pMHDRC || !pMHDRC->pDev)
        {
        USB_MHDRC_ERR("invalid parameter\n",
                  1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Get the VxBus Device instance */

    pDev = pMHDRC->pDev;

    /*
     * Determine whether the usage counter has exceeded maximum number of
     * host controllers
     */

    OS_WAIT_FOR_EVENT (gMHCDListAccessEvent, WAIT_FOREVER);

    if (gMHCDControllerCount == USB_MAX_MHCI_COUNT)
        {
        USB_MHDRC_ERR("Out of resources\n", 1, 2, 3, 4, 5 ,6);

        OS_RELEASE_EVENT (gMHCDListAccessEvent);

        return ERROR;
        }

    /* Allocate memory for the MHCD_DATA structure */

    pHCDData = (pUSB_MHCD_DATA)MHDRC_MALLOC(sizeof(USB_MHCD_DATA));

    /* Check if memory allocation is successful */

    if (NULL == pHCDData)
        {
        USB_MHDRC_ERR("Memory not allocated for USB_MHCD_DATA structure\n",
                     1, 2, 3, 4, 5 ,6);

        OS_RELEASE_EVENT (gMHCDListAccessEvent);

        return ERROR;
        }

    /* Initialize all the elements of the structure */

    OS_MEMSET(pHCDData, 0, sizeof(USB_MHCD_DATA));

    /* Get bus index */

    for (index = 0; index < USB_MAX_MHCI_COUNT; index ++)
        {
        if (gpMHCDData[index]== NULL)
            break;
        }

    if (index == USB_MAX_MHCI_COUNT)
        {
        USB_EHCD_ERR("usbMhdrcHcdInstanceAdd - gpMHCDData is full\n",
            0, 0, 0, 0, 0, 0);

        MHDRC_FREE(pHCDData);

        OS_RELEASE_EVENT (gMHCDListAccessEvent);

        return ERROR;
        }

    /* If this device is a PCI device, unitNumber shall be assigned */
    
    if(0 == strncmp(pDev->pName, USB_MHCI_PCI_NAME, sizeof(USB_MHCI_PCI_NAME)))
        vxbNextUnitGet(pDev);

    pHCDData->uBusIndex = index;

    /* Save the common hardware information structure */

    pHCDData->pMHDRC = pMHDRC;

    /* Save the HCD information structure */

    pMHDRC->pHCDData = pHCDData;

    /* If this is called by OCD then we just need wait forever */

    if (pMHDRC->pISR)
        {
        pHCDData->isrPollingRateMs = WAIT_FOREVER;
        }
    else
        {
        pHCDData->isrPollingRateMs = USB_MHDRC_HCD_ISR_POLLING_MS;
        }

    /* Initialize the global array with the element */

    gpMHCDData[pHCDData->uBusIndex] = pHCDData;

    /* Initialize the Host Controller data structure */

    if (OK != usbMhdrcHcdDataInit(pHCDData))
        {
        USB_MHDRC_ERR("usbMhdrcHcdDataInit failed\n",
                      1, 2, 3, 4, 5 ,6);

        /* Reset the global array */

        gpMHCDData[pHCDData->uBusIndex] = NULL;

        /* Free the memory allocated */

        MHDRC_FREE(pHCDData);

        pMHDRC->pHCDData = NULL;

        OS_RELEASE_EVENT (gMHCDListAccessEvent);

        return ERROR;
        }

    /* Increase the global counter */

    gMHCDControllerCount++;

    /*
     * Register the bus with the USBD and
     * Check if the bus is registered successfully
     */

    if (USBHST_SUCCESS != usbHstBusRegister (gMHCDHandle,
                                   USBHST_HIGH_SPEED,
                                   (ULONG)pHCDData->pDefaultPipe,
                                   pDev))
        {
        USB_MHDRC_ERR("Error in registering the bus\n", 1, 2, 3, 4, 5 ,6);

        /* Decrement the global counter */

        gMHCDControllerCount--;

        /* Reset the array element */

        gpMHCDData[pHCDData->uBusIndex] = NULL;

        usbMhdrcHcdDataUnInit(pHCDData);

        /* Free the memory allocated */

        MHDRC_FREE(pHCDData);

        pMHDRC->pHCDData = NULL;

        OS_RELEASE_EVENT (gMHCDListAccessEvent);

        return ERROR;
        }

    OS_RELEASE_EVENT (gMHCDListAccessEvent);

    return OK;
    }

/*******************************************************************************
*
* usbMhdrcHcdInstanceRemove - enable a host controller instance
*
* This routine is to enable a host controller instance.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbMhdrcHcdInstanceEnable
    (
    pUSB_MUSBMHDRC      pMHDRC
    )
    {
    UINT8 uReg8;
    UINT32 uCount;

    /* Perform hardware specific mode change action */

    usbMhdrcPlatformModeChange (pMHDRC, USBOTG_MODE_HOST);

    /* Read the DEVCTL register */

    uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_DEVCTL);

    /* Set the SESSION bit */

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                          USB_MHDRC_DEVCTL,
                          uReg8 | USB_MHDRC_DEVCTL_SESSION);

    uCount = 0;

    uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_DEVCTL);

    USB_MHDRC_WARN("usbMhdrcHcdInstanceEnable - USB_MHDRC_DEVCTL 1 0x%X\n",
        uReg8, 2, 3, 4, 5 ,6);

    /* Wait until SESSION active */

    while (!(uReg8 & USB_MHDRC_DEVCTL_SESSION) && (uCount++ < 100))
        {
        /* Set the SESSION bit */

        USB_MHDRC_REG_WRITE8 (pMHDRC,
                              USB_MHDRC_DEVCTL,
                              uReg8 | USB_MHDRC_DEVCTL_SESSION);

        uReg8 = USB_MHDRC_REG_READ8 (pMHDRC, USB_MHDRC_DEVCTL);
        }

    if (pMHDRC->bDmaEnabled == TRUE)
        {
        switch (pMHDRC->uPlatformType)
            {
            case USB_MHDRC_PLATFORM_OMAP35xx:
                /* Install the DMA callback routine */
    
                usbMhdrcDmaCallbackSet(&(pMHDRC->dmaData),
                    (pUSB_MHDRC_DMA_CALLBACK_FUNC)usbMhdrcHcdHsDmaInterruptHandler);
                break;
            case USB_MHDRC_PLATFORM_CENTAURUS:    
                /* fall through */
			
            case USB_MHDRC_PLATFORM_CENTAURUS_TI816X:
                /* Install the DMA callback routine */
    
                usbMhdrcDmaCallbackSet(&(pMHDRC->dmaData),
                    (pUSB_MHDRC_DMA_CALLBACK_FUNC)usbMhdrcHcdCppiDmaInterruptHandler);
    
                break;
            default:
                USB_MHDRC_ERR("Invalid platformType %d\n", 
                              pMHDRC->uPlatformType, 2, 3, 4, 5, 6);
                return ERROR;
            }

        /* Enable the DMA interrupt */

        usbMhdrcDmaIntrEnable(pMHDRC);
        }

    return OK;
    }

/*******************************************************************************
*
* usbMhdrcHcdInstanceDisable - disable a host controller instance
*
* This routine is to disable a host controller instance.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbMhdrcHcdInstanceDisable
    (
    pUSB_MUSBMHDRC      pMHDRC
    )
    {
    if (pMHDRC->bDmaEnabled == TRUE)
        {
        switch (pMHDRC->uPlatformType)
            {
            case USB_MHDRC_PLATFORM_OMAP35xx:
            case USB_MHDRC_PLATFORM_CENTAURUS:
                 /* fall through */
				
            case USB_MHDRC_PLATFORM_CENTAURUS_TI816X:

        /* Disable DMA interrupts */
        
                usbMhdrcDmaIntrDisable(pMHDRC);
        
        /* Remove the DMA callback routine */

                usbMhdrcDmaCallbackSet(&(pMHDRC->dmaData),
                                 NULL);
                break;
           default:            
                USB_MHDRC_ERR("Invalid platformType %d\n", 
                              pMHDRC->uPlatformType, 2, 3, 4, 5, 6);
                return ERROR;
            }
        }

    return OK;
    }

