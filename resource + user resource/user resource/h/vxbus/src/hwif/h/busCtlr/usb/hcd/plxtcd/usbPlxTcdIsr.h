/* usbPlxTcdIsr.h -  USB PLX TCD Interrupt Handler module */

/*
 * Copyright (c) 2012 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01a,18may12,s_z  Writen
*/

/*
DESCRIPTION

This module includes the definition used by the interrupt handler module
for the USB PLX Target Controller Driver.
*/

#ifndef __INCusbPlxTcdIsrh
#define __INCusbPlxTcdIsrh

#ifdef  __cplusplus
extern "C" {
#endif

/* declartion */

IMPORT void usbPlxTcdIsr
    (
    void * pPLXCTRL
    );
IMPORT void usbPlxTcdInterruptHandler
    (
    void * pTCDDATA
    );

#endif /* __INCusbPlxTcdIsrh */

