/* usbOhciRegisterInfo.h - OHCI host controller register definition */

/*
 * Copyright (c) 2003, 2005-2008, 2010 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2003, 2005-2008, 2010 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
modification history
--------------------
01k,18may10,m_y  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
01j,02mar10,w_x  Avoid taking up 100% CPU time by USB task (WIND00201956)
01i,13jan10,ghs  vxWorks 6.9 LP64 adapting
01h,11jul08,w_x  Added HcRhDescriptorA NPS mask value definition (for WIND00120108)
01g,06aug07,jrp  Changing Register access methods
01f,30mar07,sup  changed the read/write routine for one additional parameter
                 flag
01e,07oct06,ami  Changes for USB-vxBus porting
01d,02apr05,pdg  Fix for exception during initialization
01c,29mar05,pdg  non-PCI changes
01b,26jun03,nld  Changing the code to WRS standards
01a,01jan03,ssh  Initial Version
*/

/*
DESCRIPTION

This file defines the register offsets and macros for the OHCI Host Controller.
*/

/*
INTERNAL
 *******************************************************************************
 * Filename         : RegisterInfo.h
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      : This file defines the register offsets and macros for the
 *                    OHCI Host Controller.
 *
 *
 ******************************************************************************/

#ifndef __INCusbOhciRegisterInfoh
#define __INCusbOhciRegisterInfoh

/* Includes */

#include <usbOhciTransferManagement.h>
#include <../src/hwif/h/vxbus/vxbAccess.h>

#ifdef  __cplusplus
extern "C" {
#endif

/* defines */

/*********************** MACROS FOR REGISTER OFFSETS **************************/

/* Offset of the OHCI Controller Revision register */

#define USB_OHCI_REVISION_REGISTER_OFFSET               0x00

/* Offset of the OHCI Controller Control register */

#define USB_OHCI_CONTROL_REGISTER_OFFSET                0x04

/* Offset of the OHCI Controller Command Status register */

#define USB_OHCI_COMMAND_STATUS_REGISTER_OFFSET         0x08

/* Offset of the OHCI Controller Interrupt Status register */

#define USB_OHCI_INTERRUPT_STATUS_REGISTER_OFFSET       0x0C

/* Offset of the OHCI Controller Interrupt Enable register */

#define USB_OHCI_INTERRUPT_ENABLE_REGISTER_OFFSET       0x10

/* Offset of the OHCI Controller Interrupt Disable register */

#define USB_OHCI_INTERRUPT_DISABLE_REGISTER_OFFSET      0x14

/* Offset of the OHCI Contoller Communication Area register */

#define USB_OHCI_HCCA_REGISTER_OFFSET                   0x18

/* Offset of the OHCI Controller Periodic Current ED register */

#define USB_OHCI_PERIOD_CURRENT_ED_REGISTER_OFFSET      0x1C

/* Offset of the OHCI Controller Control Head ED register */

#define USB_OHCI_CONTROL_HEAD_ED_REGISTER_OFFSET        0x20

/* Offset of the OHCI Controller Control Current ED register */

#define USB_OHCI_CONTROL_CURRENT_ED_REGISTER_OFFSET     0x24

/* Offset of the OHCI Controller Bulk Head ED register */

#define USB_OHCI_BULK_HEAD_ED_REGISTER_OFFSET           0x28

/* Offset of the OHCI Controller Bulk Current ED register */

#define USB_OHCI_BULK_CURRENT_ED_REGISTER_OFFSET        0x2C

/* Offset of the OHCI Controller Done Head register */

#define USB_OHCI_DONE_HEAD_REGISTER_OFFSET              0x30

/* Offset of the OHCI Controller Frame Interval register */

#define USB_OHCI_FM_INTERVAL_REGISTER_OFFSET            0x34

/* Offset of the OHCI Controller Frame Remaining register */

#define USB_OHCI_FM_REMAINING_REGISTER_OFFSET           0x38

/* Offset of the OHCI Controller Frame Number register */

#define USB_OHCI_FM_NUMBER_REGISTER_OFFSET              0x3C

/* Offset of the OHCI Controller Periodic Start register */

#define USB_OHCI_PERIODIC_START_REGISTER_OFFSET         0x40

/* Offset of the OHCI Controller LS Threshold register */

#define USB_OHCI_LC_THRESHOLD_REGISTER_OFFSET           0x44

/* Offset of the OHCI Controller Root Hub Descriptor A register */

#define USB_OHCI_RH_DESCRIPTOR_A_REGISTER_OFFSET        0x48

/* Offset of the OHCI Controller Root Hub Descriptor B register */

#define USB_OHCI_RH_DESCRIPTOR_B_REGISTER_OFFSET        0x4C

/* Offset of the OHCI Controller Root Hub Status register */

#define USB_OHCI_RH_STATUS_REGISTER_OFFSET              0x50

/*
 * Offset of the OHCI Controller Root Hub Port Status register. The
 * port status corresponding to the root hub port 1 is stared at the
 * following offset.
 *
 * The following is the calculation to obtain the offset of port
 * status register corresponding to root hub port 'n':
 *
 * USB_OHCI_RH_PORT_STATUS_BASE_REGISTER_OFFSET + (n * 4)
 */

#define USB_OHCI_RH_PORT_STATUS_BASE_REGISTER_OFFSET    0x54


/****************** MACROS FOR THE REGISTER FIELD VALUES **********************/

/* Mask value for the HCFS field of the HcControl register */

#define USB_OHCI_CONTROL_HCFS_MASK                      0x000000C0

/* Value to set the HCFS field of the HcControl register (for USB Reset) */

#define USB_OHCI_CONTROL_HCFS_USB_RESET                 0x00000000

/* Value to set the HCFS field of the HcControl register (for USB Resume) */

#define USB_OHCI_CONTROL_HCFS_USB_RESUME                0x00000040

/*
 * Value to set the HCFS field of the HcControl register (for USB Operational)
 */

#define USB_OHCI_CONTROL_HCFS_USB_OPERATIONAL           0x00000080

/* Value to set the HCFS field of the HcControl register (for USB Suspend) */

#define USB_OHCI_CONTROL_HCFS_USB_SUSPEND               0x000000C0

/*
 * Value to clear the Remote Wakeup Enable (RWE) bit of the HcControl
 * register
 */

#define USB_OHCI_CONTROL_RWE                            0x00000400

/* Value to set the Control List Enable (CLE) bit of the HcControl register */

#define USB_OHCI_CONTROL_CLE                            0x00000010

/* Value to set the Bulk List Enable (BLE) bit of the HcControl register */

#define USB_OHCI_CONTROL_BLE                            0x00000020

/* Value to set the Periodic List Enable (PLE) bit of the HcControl register */

#define USB_OHCI_CONTROL_PLE                            0x00000004

/* Value to set Isochronous List Enable (IE) bit of the HcControl register */

#define USB_OHCI_CONTROL_IE                             0x00000008

/* Value to set the HCR field of the HcCommandStatus register */

#define USB_OHCI_COMMAND_STATUS_HCR                     0x00000001

/*
 * Value to set the Control List Filled (CLF) bit of the HcCommandStatus
 * register
 */

#define USB_OHCI_COMMAND_STATUS_CLF                     0x00000002

/*
 * Value to set the Bulk List Filled (BLF) bit of the HcCommandStatus
 * register
 */

#define USB_OHCI_COMMAND_STATUS_BLF                     0x00000004

/* Value to check whether SOF interrupt is pending */

#define USB_OHCI_INTERRUPT_STATUS_SOF                   0x00000004

/* Value to check whether a Write Back Done Head interrupt has occurred */

#define USB_OHCI_INTERRUPT_STATUS_DONE_HEAD             0x00000002

/* Value to check whether UE interrupt is pending */

#define USB_OHCI_INTERRUPT_STATUS_UE                    0x00000010

/* Value to check whether root hub status change interrupt is pending */

#define USB_OHCI_INTERRUPT_STATUS_RHSC                  0x00000040

/*
 * Value to check whether resume detect interrupt is pending.
 *
 * NOTE: This value is also used to enable or disable 'Resume Detect' interrupt
 *       based on the ClearFeature(REMOTE_WAKEUP) or SetFeature(REMOTE_WAKEUP)
 *       request.
 */

#define USB_OHCI_INTERRUPT_STATUS_RESUME_DETECT         0x00000008

/*
 * Mask value to check whether the interrupt routing bit is set or not in
 * HcControl Register
 */

#define USB_OHCI_INTERRUPT_ROUTING_CONTROL_REGISTER     0x00000100

/*
 * Value to write to allow the OS to gain ownership of the host controller in
 * the command status register
 */

#define USB_OHCI_CHANGE_OWNERSHIP                       0x00000008

/*
 * Mask for the retrieving the Host controller functional state in the control
 * register
 */

#define USB_OHCI_HCFS_CONTROL_REGISTER                  0x000000D0

/* Value of the host controller operational states */

#define USB_OHCI_HCFS_USB_RESET                         0x00000000
#define USB_OHCI_HCFS_USB_OPERATIONAL                   0x00000080
#define USB_OHCI_HCFS_USB_RESUME                        0x00000040

/*
 * Mask to set the host controller operational state without changing any other
 * bits
 */

#define USB_OHCI_HCFS_CHANGE_MASK                       0xFFFFFF3F

/*
 * Mask value to obtain the contents of the number of downsteam ports (NDP)
 * field of the HcRhDescriptorA register.
 */

#define USB_OHCI_RH_DESCRIPTOR_A_REGISTER_NDP_MASK      0x000000FF

/*
 * Mask value to obtain the contents of the power switching mode (PSM)
 * field of the HcRhDescriptorA register.
 */

#define USB_OHCI_RH_DESCRIPTOR_A_REGISTER__PSM_MASK     0x00000100

/*
 * Mask value to obtain the contents of the no power switching (NPS)
 * field of the HcRhDescriptorA register.
 */

#define USB_OHCI_RH_DESCRIPTOR_A_REGISTER__NPS_MASK     0x00000200

/*
 * Mask value to obtain the contents of the Port Power Control
 * Mask (PPCM) field of the HcRhDescriptorB register.
 */

#define USB_OHCI_HC_RH_DESCRIPTOR_B_REGISTER__PPCM_MASK 0xFFFE0000


/* To hold the mask value to indicate a status change on the root hub */

#define USB_OHCI_RH_STATUS_CHANGE_MASK                  0x00030000

/*
 * Value to check the local power status (LPS) bit of the HcRhStatus
 * register. This value will be used for clearing the global power status.
 */


#define USB_OHCI_RH_STATUS__LPS                         0x00000001

/*
 * Value to check the local power status change (LPSC) bit of the HcRhStatus
 * register. This value will be used for setting the global power status.
 */

#define USB_OHCI_RH_STATUS_LPSC                         0x00010000

/*
 * Value to check the over current indicator change (OCIC) bit of the
 * HcRhStatus register. This value will be used for clearing the OCIC bit.
 */

#define USB_OHCI_RH_STATUS_OCIC                         0x00020000

/* To hold the mask value to indicate a status change on the root hub port */

#define USB_OHCI_RH_PORT_STATUS_CHANGE_MASK             0x001F0000

/* Value to check the port current connect status (CCS) bit */

#define USB_OHCI_RH_PORT_STATUS_CCS                     0x00000001

/* Value to check the port enable status (PES) bit */

#define USB_OHCI_RH_PORT_STATUS_PES                     0x00000002

/* Value to check the port suspend status (PSS) bit */

#define USB_OHCI_RH_PORT_STATUS_PSS                     0x00000004

/* Value to check the port over current indicator (POCI) bit */

#define USB_OHCI_RH_PORT_STATUS_POCI                    0x00000008

/* Value to check the port reset status (PRS) bit */

#define USB_OHCI_RH_PORT_STATUS_PRS                     0x00000010

/* Value to check the port power status (PPS) bit */

#define USB_OHCI_RH_PORT_STATUS_PPS                     0x00000100

/* Value to check the low speed device attached (LSDA) bit */

#define USB_OHCI_RH_PORT_STATUS_LSDA                    0x00000200

/* Value to check the connect status change (CSC) bit */

#define USB_OHCI_RH_PORT_STATUS_CSC                     0x00010000

/* Value to check the port enable status change (PESC) bit */

#define USB_OHCI_RH_PORT_STATUS_PESC                    0x00020000

/* Value to check the port suspend status change (PSSC) bit */

#define USB_OHCI_RH_PORT_STATUS_PSSC                    0x00040000

/* Value to check the over current indicator chage (OCIC) bit */

#define USB_OHCI_RH_PORT_STATUS_OCIC                    0x00080000

/* Value to check the port reset status change (PRSC) bit */

#define USB_OHCI_RH_PORT_STATUS_PRSC                    0x00100000

/* globals */

#ifdef NO_CACHE_COHERENCY

extern UINT32 usbOhciRegRead
    (
    VXB_DEVICE_ID    pDev,    /* pointer to the vxBus device structure */
    UINT32           offset   /* offset from the base address
                               * from which contents have to be read  */
    );

extern VOID usbOhciRegWrite
    (
    VXB_DEVICE_ID    pDev,   /* pointer to the vxBus device structure */
    UINT32           offset, /* Address to which contents have to be written  */
    UINT32           pValue  /* Value to be written */
    );
#endif
/********************* MACROS FOR REGISTER READ/WRITE *************************/

/*******************************************************************************
 * MACRO NAME    : USB_OHCI_RH_PORT_STATUS_REGISTER_OFFSET
 * DESCRIPTION   : Macro to obtain the register offset for the root hub port.
 * PARAMETERS    : PORT_NUMBER  IN  Port number corresponding to the root hub
 *                                  port.
 * RETURN TYPE   : None
 ******************************************************************************/

#define USB_OHCI_RH_PORT_STATUS_REGISTER_OFFSET(PORT_NUMBER)                   \
        (USB_OHCI_RH_PORT_STATUS_BASE_REGISTER_OFFSET + (4 * (PORT_NUMBER - 1)))

/*******************************************************************************
 * MACRO NAME    : USB_OHCI_REG_WRITE
 * DESCRIPTION   : Macro to write to the register.
 * PARAMETERS    : INDEX    IN  Index of the host controller.
                   ADDRESS  IN  Address of the location.
 *                 VALUE    IN  Value to be written into the register.
 * RETURN TYPE   : None
 ******************************************************************************/

#ifndef NO_CACHE_COHERENCY

#define USB_OHCI_REG_WRITE(pDEV, OFFSET, VALUE)                                \
    vxbWrite32 (((pUSB_OHCD_DATA)(pDEV->pDrvCtrl))->pRegAccessHandle,  \
        (VOID *)((ULONG)((pUSB_OHCD_DATA)(pDEV->pDrvCtrl))->regBase + (ULONG)OFFSET),\
        VALUE)


#else

#define USB_OHCI_REG_WRITE(pDEV, OFFSET, VALUE)                                \
                      usbOhciRegWrite(pDEV,OFFSET,VALUE)

#endif

/*******************************************************************************
 * MACRO NAME    : USB_OHCI_REG_READ
 * DESCRIPTION   : Macro to read the contents of a register.
 * PARAMETERS    : INDEX    IN  Index of the host controller
                   ADDRESS  IN  Address of the location.
 * RETURN TYPE   : Returns the contents of the register.
 ******************************************************************************/

#ifndef NO_CACHE_COHERENCY
#define USB_OHCI_REG_READ(pDEV, OFFSET)                                        \
    vxbRead32 (((pUSB_OHCD_DATA)(pDEV->pDrvCtrl))->pRegAccessHandle,   \
        (VOID *)((ULONG)(((pUSB_OHCD_DATA)(pDEV->pDrvCtrl))->regBase) + (ULONG)OFFSET))


#else

#define USB_OHCI_REG_READ(pDEV, OFFSET)                                        \
        usbOhciRegRead (pDEV, OFFSET)

#endif

#ifdef  __cplusplus
}
#endif

#endif /* __INCusbOhciRegisterInfoh */

/* End of file usbOhciRegisterInfo.h */
