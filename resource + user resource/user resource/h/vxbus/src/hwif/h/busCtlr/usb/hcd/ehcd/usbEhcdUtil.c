/* usbEhcdUtil.c - contains the utility functions of EHCD */

/*
 * Copyright (c) 2002-2014 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2002-2014 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
modification history
--------------------
02l,25Feb14,wyy  Enable vxbDmaBuf alignment checking (VXW6-70065)
02k,03may13,wyy  Remove compiler warning (WIND00356717)
02j,16nov12,s_z  Update the qTD status by checking the qTD list (WIND00378462)
02i,07nov12,s_z  Set/Clear asynch schedule enable bit only once (WIND00378871)
02h,10jul12,ljg  Update nStatus of pUrb after isochronous transfer (WIND00353767)
02g,27jun12,ljg  Update uTransferLength after isochronous transfer (WIND00353767)
02f,16sep11,s_z  Add usbEhcdAsynchScheduleEnable and usbEhcdAsynchScheduleDisable
                 routines to avoid dead loop to process the asynchnorous schedule
                 (WIND00293308)
02e,11jan11,ghs  Return FALSE if URB is NULL when copy interrupt status data
                 (WIND00237602)
02d,07jan11,ghs  Clean up compile warnings (WIND00247082)
02c,30jul10,w_x  Correct URB cancel handling (WIND00223154)
02b,18may10,w_x  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
02a,09mar10,j_x  Changed for USB debug (WIND00184542)
01z,13jan10,ghs  vxWorks 6.9 LP64 adapting
01y,27sep09,y_l  Fix bug: missing semaphore release operation (WIND00183805)
01x,12aug09,ghs  Remove 64 bit additional defines(WIND00176520)
01w,15jul09,ghs  Fix for WIND00171264, remove align defines
01v,09apr09,w_x  Remove tabs and coding convention changes
01u,10mar09,j_x  Correct the return error status for non isochronous transfer
                 (WIND00156037)
01t,09feb09,w_x  Modify usbEhcdUpdateNonIsochStatusAndBytes to correct URB
                 status (WIND00155564 and WIND00150815)
01s,04feb09,w_x  Added support for FSL quirky EHCI with various register and
                 descriptor endian format (such as MPC5121E)
01r,12dec08,w_x  Correct some debug macro call to pass compile (WIND00148149)
01q,04sep07,ami  Changes tp support PLB based controllers
01p,07oct06,ami  Changes for USB-vxBus porting
01o,31jan07,jrp  Defect 86909
01n,22apr05,pdg  Fix for 64 bit split isochronous transfers
01m,28mar05,pdg  non-PCI changes
01l,22mar05,mta  64-bit support added (SPR #104950)
01k,25feb05,mta  SPR 106276
01j,03dec04,ami  Merged IP Changes
01i,26oct04,ami  Severity Changes for Debug Messages
01h,15oct04,ami  Apigen Changes
01g,05oct04,mta  SPR100704- Removal of floating point math
01f,15oct04,ami  Apigen Changes
01e,05oct04,mta  SPR100704- Removal of floating point math
01d,23Jul03,???  Incorporated changes identified during testing on MIPS.
01c,16Jul03,???  updated the next link pointer of the aligned pointer in the
                 free list to be NULL.
01b,26jun03,gpd  changing the code to WRS standards.
01a,25apr02,ram  written.
*/

/*
DESCRIPTION

This module defines the functions which serve as utility functions for the
EHCI Host Controller Driver.

INCLUDE FILES: usb/usbOsal.h usb/usbHst.h usbEhcdDataStructures.h 
               usbEhcdUtil.h usb/usbHcdInstr.h usbEhcdHal.h

SEE ALSO:
 <USB specification, revision 2.0>
 <EHCI specification, revision 1.0>

*/

/*
 INTERNAL
 *******************************************************************************
 *
 * Filename         : EHCD_Util.c
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 * Description      :  This contains the utility functions which are used by the
 *                     EHCI Driver.
 *
 *
 ******************************************************************************/

#include "usb/usbOsal.h"
#include "usb/usbHst.h"
#include "usbEhcdDataStructures.h"
#include "usbEhcdUtil.h"
#include "usb/usbHcdInstr.h"
#include "usbEhcdHal.h"

/* extern */

IMPORT pUSB_EHCD_DATA *     g_pEHCDData;

/* forward declarations */

LOCAL UINT32 usbEhcdFillITD
    (
    UINT8                   index,          /* index of the host controller */
    pUSB_EHCD_ITD           pITD,           /* Pointer to the ITD */
    UINT8                   uMicroFrameMask,/* Microframe mask value */
    bus_addr_t              pBuffer,        /* Pointer to the buffer value */
    UINT32                  uPktCnt,        /* No of packet */
    pUSBHST_ISO_PACKET_DESC pIsoPktDesc     /* array of packet descriptor */
    );

LOCAL BOOLEAN usbEhcdFillSITD
    (
    UINT8               index,          /* index of the host controller */
    pUSB_EHCD_SITD      pSITD,          /* Pointer to the SITD */
    bus_addr_t          pBuffer,        /* Pointer to the start of the buffer */
    bus_size_t          uTransferLength /* Transfer length */
    );

/*******************************************************************************
*
* usbEhcdDeleteRequestInfo - delete a request info of the pipe
*
* This routine is used to delete a request info of the pipe. It will destroy
* the DMA map associated with this request info structure.
*
* <pHCDData> - Pointer to the HCD data
* <pHCDPipe> - Pointer to the pipe
* <pRequest> - Pointer to the request info structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* Note: It is expetect the pipe lock has been taken when calling this routine.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbEhcdDeleteRequestInfo
    (
    pUSB_EHCD_DATA          pHCDData,
    pUSB_EHCD_PIPE          pHCDPipe,
    pUSB_EHCD_REQUEST_INFO  pRequest
    )
    {
    /* Destroy the maps creatred */

    if (pRequest->ctrlSetupDmaMapId != NULL)
        {
        vxbDmaBufMapDestroy (pHCDPipe->ctrlSetupDmaTagId,
            pRequest->ctrlSetupDmaMapId);

        pRequest->ctrlSetupDmaMapId = NULL;
        }

    if (pRequest->usrDataDmaMapId != NULL)
        {
        vxbDmaBufMapDestroy (pHCDPipe->usrBuffDmaTagId,
            pRequest->usrDataDmaMapId);

        pRequest->usrDataDmaMapId = NULL;
        }

    /* Free the request */

    OS_FREE(pRequest);
    }

/*******************************************************************************
*
* usbEhcdCreateRequestInfo - create a request info of the pipe
*
* This routine is used to create a request info of the pipe. It will create
* the DMA map associated with this request info structure.
*
* <pHCDData> - Pointer to the HCD data
* <pHCDPipe> - Pointer to the pipe
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* Note: It is expetect the pipe lock has been taken when calling this routine.
*
* RETURNS: Pointer to the request info structure
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_EHCD_REQUEST_INFO usbEhcdCreateRequestInfo
    (
    pUSB_EHCD_DATA  pHCDData,
    pUSB_EHCD_PIPE  pHCDPipe
    )
    {
    /* Request information */

    pUSB_EHCD_REQUEST_INFO  pRequest;

    /* Get the request info */

    pRequest = (pUSB_EHCD_REQUEST_INFO)
                OS_MALLOC(sizeof(USB_EHCD_REQUEST_INFO));

    if (pRequest == NULL)
        {
        USB_EHCD_ERR("usbEhcdCreateRequestInfo - allocate pRequest fail\n",
            0, 0, 0, 0, 0, 0);

        return NULL;
        }

    /* Clear the request info */

    OS_MEMSET(pRequest, 0, sizeof(USB_EHCD_REQUEST_INFO));

    /* Record the pipe we work for */

    pRequest->pHCDPipe = pHCDPipe;

    /* Create Data DMA MAP ID */

    pRequest->usrDataDmaMapId = vxbDmaBufMapCreate (pHCDData->pDev,
        pHCDPipe->usrBuffDmaTagId,
        0,
        NULL);

    if (pRequest->usrDataDmaMapId == NULL)
        {
        USB_EHCD_ERR("usbEhcdCreateRequestInfo - "
            "allocate usrDataDmaMapId fail\n",
            0, 0, 0, 0, 0, 0);

        /* Delete the request resources */

        usbEhcdDeleteRequestInfo(pHCDData, pHCDPipe, pRequest);

        return NULL;
        }

    /* Control pipe needs special treatment for the Setup */

    if (pHCDPipe->uEndpointType == USBHST_CONTROL_TRANSFER)
        {
        /* Create control Setup DMA MAP ID */

        pRequest->ctrlSetupDmaMapId = vxbDmaBufMapCreate (pHCDData->pDev,
            pHCDPipe->ctrlSetupDmaTagId,
            0,
            NULL);

        if (pRequest->ctrlSetupDmaMapId == NULL)
            {
            USB_EHCD_ERR("usbEhcdCreateRequestInfo - "
                "allocate ctrlSetupDmaMapId fail\n",
                0, 0, 0, 0, 0, 0);

            /* Delete the request resources */

            usbEhcdDeleteRequestInfo(pHCDData, pHCDPipe, pRequest);

            return NULL;
            }
        }

    /* Link the new request info onto the free requst queue */

    if (pHCDPipe->pFreeRequestQueueHead == NULL)
        {
        pHCDPipe->pFreeRequestQueueHead = pRequest;
        }
    else
        {
        pHCDPipe->pFreeRequestQueueTail->pNext = pRequest;
        }

    pHCDPipe->pFreeRequestQueueTail = pRequest;

    return pRequest;
    }

/*******************************************************************************
*
* usbEhcdReserveRequestInfo - reserve a request info of the pipe for a URB
*
* This routine is used to reserve a request info structure of the pipe for a
* URB. It is commonly called when submitting a URB. A request info is considered
* to have been reserved when its associated with a URB (the URB pointer of this
* request info structure being non-NULL).
*
* <pHCDData> - Pointer to the HCD data
* <pHCDPipe> - Pointer to the pipe
* <pUrb> - Pointer to the URB
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* Note: It is expetect the pipe lock has been taken when calling this routine.
*
* RETURNS: the pointer to the reserved request info structure,
*   or NULL if no more free request on this pipe.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_EHCD_REQUEST_INFO usbEhcdReserveRequestInfo
    (
    pUSB_EHCD_DATA  pHCDData,
    pUSB_EHCD_PIPE  pHCDPipe,
    pUSBHST_URB     pUrb
    )
    {
    /* Request information */

    pUSB_EHCD_REQUEST_INFO  pRequest;

    /* We can not allow bigger than the specified size */

    if (pUrb->uTransferLength > pHCDPipe->uMaxTransferSize)
        {
        USB_EHCD_ERR(
            "usbEhcdReserveRequestInfo - uAddress %d uEndpointAddress %p\n"
            "uEndpointType %d uTransferLength %d is too big\n",
            pHCDPipe->uAddress,
            pHCDPipe->uEndpointAddress,
            pHCDPipe->uEndpointType,
            pUrb->uTransferLength, 0, 0);

        return NULL;
        }

    /* Check if there is a free request info */

    if (pHCDPipe->pFreeRequestQueueHead == NULL)
        {
        /* Create a new request */

        pRequest = usbEhcdCreateRequestInfo(pHCDData, pHCDPipe);

        if (pRequest == NULL)
            {
            USB_EHCD_ERR(
                "usbEhcdReserveRequestInfo -"
                "uTransferLength (%d) uMaxTransferSize (%d) \n",
                pUrb->uTransferLength,
                pHCDPipe->uMaxTransferSize,
                0, 0, 0, 0);

            return NULL;
            }
        }

    /* Point to the head of the free requests */

    pRequest = pHCDPipe->pFreeRequestQueueHead;

    /* Point the head to the next */

    pHCDPipe->pFreeRequestQueueHead = pRequest->pNext;

    /* If this is the only free request, update the head and tail to NULL */

    if (pRequest == pHCDPipe->pFreeRequestQueueTail)
        {
        pHCDPipe->pFreeRequestQueueHead = NULL;

        pHCDPipe->pFreeRequestQueueTail = NULL;
        }

    /* Break from the original list */

    pRequest->pNext = NULL;

    /* Associate the request info with the URB */

    pRequest->pUrb = pUrb;

    /* Save the requested length */

    pRequest->uRequestLength = pUrb->uTransferLength;

    /* Associate the URB with the request */

    pUrb->pHcdSpecific = (void *)pRequest;

    /* Add the request to the active request list of the pipe */

    if (pHCDPipe->pRequestQueueHead == NULL)
        {
        pHCDPipe->pRequestQueueHead = pRequest;
        }
    else
        {
        pHCDPipe->pRequestQueueTail->pNext = pRequest;
        }

    /* Move the tail pointer to point to this request */

    pHCDPipe->pRequestQueueTail = pRequest;

    return pRequest;
    }

/*******************************************************************************
*
* usbEhcdReleaseRequestInfo - release a request info of the pipe
*
* This routine is used to release a request info structure of the pipe.
* It is commonly called when returnning a URB to the user.
*
* <pHCDData> - Pointer to the HCD data
* <pHCDPipe> - Pointer to the pipe
* <pRequest> - Pointer to the request info structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* Note: It is expetect the pipe lock has been taken when calling this routine.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbEhcdReleaseRequestInfo
    (
    pUSB_EHCD_DATA          pHCDData,
    pUSB_EHCD_PIPE          pHCDPipe,
    pUSB_EHCD_REQUEST_INFO  pRequest
    )
    {
    /* Temp request info */

    pUSB_EHCD_REQUEST_INFO  pTempRequest;

    /* Do this only when the URB pointer is non-NULL */

    if (pRequest != NULL)
        {
        if (pRequest->pUrb)
            {
            /*
             * Set the URB HCD specific pointer to be NULL
             * indicating it is completed
             */

            pRequest->pUrb->pHcdSpecific = NULL;

            /* Set the pUrb as NULL to indicate it is free */

            pRequest->pUrb = NULL;
            }

        /* Remove the request from the active request list of this pipe */

        if (pRequest == pHCDPipe->pRequestQueueHead)
            {
            /*
             * If this is the only request on the list,
             * then set both head and tail to NULL
             */

            if (pRequest == pHCDPipe->pRequestQueueTail)
                {
                pHCDPipe->pRequestQueueHead = NULL;

                pHCDPipe->pRequestQueueTail = NULL;
                }
            else
                {
                /*
                 * If this is the head but not the tail,
                 * then only update the head pointer
                 */

                pHCDPipe->pRequestQueueHead = pRequest->pNext;
                }
            }
        else
            {
            /* If this is not the head, then find the previous request */

            pTempRequest = pHCDPipe->pRequestQueueHead;

            while (pTempRequest != NULL)
                {
                if (pTempRequest->pNext == pRequest)
                    break;

                pTempRequest = pTempRequest->pNext;
                }
            /*
             * Check if the previous request is not NULL.
             * This should be always true, but just to watch out.
             */

            if (pTempRequest != NULL)
                {
                pTempRequest->pNext = pRequest->pNext;

                /*
                 * If this is the tail, then update the tail to point to
                 * the previous request we found
                 */

                if (pRequest == pHCDPipe->pRequestQueueTail)
                    {
                    pHCDPipe->pRequestQueueTail = pTempRequest;
                    }
                }
            }

        /* Break from the active list */

        pRequest->pNext = NULL;

        /* Link the request info onto the free request queue */

        if (pHCDPipe->pFreeRequestQueueHead == NULL)
            {
            pHCDPipe->pFreeRequestQueueHead = pRequest;
            }
        else
            {
            pHCDPipe->pFreeRequestQueueTail->pNext = pRequest;
            }

        pHCDPipe->pFreeRequestQueueTail = pRequest;
        }

    return;
    }

/*******************************************************************************
*
* usbEhcdReleaseRequestInfo - check if a request info is reserved
*
* This routine is used to check if a request info is reserved.
*
* <pHCDData> - Pointer to the HCD data
* <pHCDPipe> - Pointer to the pipe
* <pRequest> - Pointer to the request info structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* Note: It is expetect the pipe lock has been taken when calling this routine.
*
* RETURNS: TRUE if the request is reserved, FALSE if not.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOLEAN usbEhcdIsRequestInfoUsed
    (
    pUSB_EHCD_DATA          pHCDData,
    pUSB_EHCD_PIPE          pHCDPipe,
    pUSB_EHCD_REQUEST_INFO  pRequest
    )
    {
    /* If the request info used */

    BOOLEAN used;

    used = (BOOLEAN)((pRequest->pUrb != NULL) ? TRUE : FALSE);

    return used;
    }

/*******************************************************************************
*
* usbEhcdAddToFreeQTDList - add a QTD to the free list
*
* This routine is used to add a QTD to the free QH list.
*
* <pHCDData> - Pointer to the USB_EHCD_DATA structure
* <pHCDPipe> - Pointer to the USB_EHCD_PIPE structure
* <pQTD> - Pointer to the USB_EHCD_QTD data structure.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* Note: It is expetect the pipe lock has been taken when calling this routine.
*
* RETURNS: TRUE if the QTD is added successfully.
*          FALSE if the QTD is not added successfully.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOLEAN usbEhcdAddToFreeQTDList
    (
    pUSB_EHCD_DATA  pHCDData,    /* Pointer to the USB_EHCD_DATA structure */
    pUSB_EHCD_PIPE  pHCDPipe,    /* Pointer to the USB_EHCD_PIPE structure */
    pUSB_EHCD_QTD   pQTD         /* Pointer to the USB_EHCD_QTD structure */
    )
    {
    /* Save the original DMA MAP ID */

    VXB_DMA_MAP_ID  dmaMapId;

    /* Get original DMA MAP ID */

    dmaMapId = pQTD->dmaMapId;

    /* Reinitialize the memory */

    OS_MEMSET(pQTD, 0, USB_EHCD_MAX_QTD_SIZE);

    /* Copy the DMA MAP ID back */

    pQTD->dmaMapId = dmaMapId;

    /* Update the next trasnfer descriptor of the QTD */

    pQTD->pNext = pHCDPipe->pHeadFreeTD;

    /* Make this QTD as the head of the free list */

    pHCDPipe->pHeadFreeTD = pQTD;

    /* If tail pointer is NULL, this QTD becomes the tail pointer */

    if (NULL == pHCDPipe->pTailFreeTD)
        {
        pHCDPipe->pTailFreeTD = pQTD;
        }

    return TRUE;
    }/* End of usbEhcdAddToFreeQTDList() */

/*******************************************************************************
*
* usbEhcdAddToFreeITDList - add an ITD to the free list
*
* This routine is used to add an ITD to the free QH list.
*
* <pHCDData> - Pointer to the USB_EHCD_DATA structure
* <pHCDPipe> - Pointer to the USB_EHCD_PIPE structure
* <pITD> - Pointer to the USB_EHCD_ITD data structure.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* Note: It is expetect the pipe lock has been taken when calling this routine.
*
* RETURNS: TRUE if the ITD is added successfully.
*          FALSE if the ITD is not added successfully.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOLEAN usbEhcdAddToFreeITDList
    (
    pUSB_EHCD_DATA  pHCDData,    /* Pointer to the USB_EHCD_DATA structure */
    pUSB_EHCD_PIPE  pHCDPipe,    /* Pointer to the USB_EHCD_PIPE structure */
    pUSB_EHCD_ITD   pITD         /* Pointer to the USB_EHCD_ITD structure */
    )
    {
    /* Save the original DMA MAP ID */

    VXB_DMA_MAP_ID  dmaMapId;

    /* Get original DMA MAP ID */

    dmaMapId = pITD->dmaMapId;

    /* Reinitialize the memory */

    OS_MEMSET(pITD, 0, USB_EHCD_MAX_ITD_SIZE);

    /* Copy the DMA MAP ID back */

    pITD->dmaMapId = dmaMapId;

    /* Update the next trasnfer descriptor of the ITD */

    pITD->pNext = pHCDPipe->pHeadFreeTD;

    /* Make this ITD as the head of the free list */

    pHCDPipe->pHeadFreeTD = pITD;

    /* If tail pointer is NULL, this ITD becomes the tail pointer */

    if (NULL == pHCDPipe->pTailFreeTD)
        {
        pHCDPipe->pTailFreeTD = pITD;
        }

    return TRUE;
    }/* End of usbEhcdAddToFreeITDList() */

/*******************************************************************************
*
* usbEhcdAddToFreeSITDList - add the SITD to the free list
*
* This routine is used to add a SITD to the free QH list.
*
* <pHCDData> - Pointer to the USB_EHCD_DATA structure
* <pHCDPipe> - Pointer to the USB_EHCD_PIPE structure
* <pSITD> - Pointer to the USB_EHCD_ITD data structure.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* Note: It is expetect the pipe lock has been taken when calling this routine.
*
* RETURNS: TRUE if the SITD is added successfully.
*          FALSE if the SITD is not added successfully.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOLEAN usbEhcdAddToFreeSITDList
    (
    pUSB_EHCD_DATA  pHCDData,    /* Pointer to the USB_EHCD_DATA structure */
    pUSB_EHCD_PIPE  pHCDPipe,    /* Pointer to the USB_EHCD_PIPE structure */
    pUSB_EHCD_SITD  pSITD        /* Pointer to the USB_EHCD_SITD structure */
    )
    {
    /* Save the original DMA MAP ID */

    VXB_DMA_MAP_ID  dmaMapId;

    /* Get original DMA MAP ID */

    dmaMapId = pSITD->dmaMapId;

    /* Reinitialize the memory */

    OS_MEMSET(pSITD, 0, USB_EHCD_MAX_SITD_SIZE);

    /* Copy the DMA MAP ID back */

    pSITD->dmaMapId = dmaMapId;

    /* Update the next trasnfer descriptor of the SITD */

    pSITD->pNext = pHCDPipe->pHeadFreeTD;

    /* Make this ITD as the head of the free list */

    pHCDPipe->pHeadFreeTD = pSITD;

    /* If tail pointer is NULL, this SITD becomes the tail pointer */

    if (NULL == pHCDPipe->pTailFreeTD)
        {
        pHCDPipe->pTailFreeTD = pSITD;
        }

    return TRUE;
    }/* End of usbEhcdAddToFreeSITDList() */

/*******************************************************************************
*
* usbEhcdGetFreeQTD - get a free QTD of the pipe
*
* This routine is used to retrieve a QTD from the free TD list of the pipe,
* if the free TD list has no elements then create one TD.
*
* <pHCDData> - Pointer to the USB_EHCD_DATA structure
* <pHCDPipe> - Pointer to the USB_EHCD_PIPE structure
*
* Note: It is expetect the pipe lock has been taken when calling this routine.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* Note: It is expetect the pipe lock has been taken when calling this routine.
*
* RETURNS: Pointer to the USB_EHCD_QTD data structure or NULL
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_EHCD_QTD usbEhcdGetFreeQTD
    (
    pUSB_EHCD_DATA  pHCDData,   /* Pointer to the USB_EHCD_DATA structure */
    pUSB_EHCD_PIPE  pHCDPipe    /* Pointer to the USB_EHCD_PIPE structure */
    )
    {
    /* Pointer to the TD */

    pUSB_EHCD_QTD pTD = NULL;

    /*
     * Check if there is any trasnfer descriptor in the free list and
     * extract the trasnfer descriptor.Update the free list. This is done
     * acquiring and releasing an event.
     */

    /* There are trasnfer descriptors in the free list */

    if (NULL != pHCDPipe->pHeadFreeTD)
        {
        /* Get the head trasnfer descriptor */

        pTD = pHCDPipe->pHeadFreeTD;

        /* The head itself is returned, update head ptr to the next */

        pHCDPipe->pHeadFreeTD = pTD->pNext;

        /* If this is the last trasnfer descriptor in the list, update the tail */

        if (pTD == pHCDPipe->pTailFreeTD)
            {
            pHCDPipe->pTailFreeTD = NULL;
            pHCDPipe->pHeadFreeTD = NULL;
            }

        /* Break from the free list */

        pTD->pNext = NULL;

        }
    else
        {
        /* Create a new TD */

        pTD = usbEhcdCreateQTD(pHCDData);

        if (NULL == pTD)
            {
            USB_EHCD_ERR("usbEhcdGetFreeQTD - usbEhcdCreateQTD fail\n",
                0, 0, 0, 0, 0, 0);

            return NULL;
            }
        }

    /* Return the pointer to TD */

    return pTD;
    }/* End of usbEhcdGetFreeQTD() */

/*******************************************************************************
*
* usbEhcdGetFreeITD - get a free ITD of the pipe
*
* This routine is used to retrieve a ITD from the free TD list of the pipe,
* if the free TD list has no elements then create one TD.
*
* <pHCDData> - Pointer to the USB_EHCD_DATA structure
* <pHCDPipe> - Pointer to the USB_EHCD_PIPE structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* Note: It is expetect the pipe lock has been taken when calling this routine.
*
* RETURNS: Pointer to the USB_EHCD_ITD data structure or NULL
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_EHCD_ITD usbEhcdGetFreeITD
    (
    pUSB_EHCD_DATA  pHCDData,   /* Pointer to the USB_EHCD_DATA structure */
    pUSB_EHCD_PIPE  pHCDPipe    /* Pointer to the USB_EHCD_PIPE structure */
    )
    {
    /* Pointer to the TD */

    pUSB_EHCD_ITD pTD = NULL;

    /*
     * Check if there is any trasnfer descriptor in the free list and
     * extract the trasnfer descriptor.Update the free list. This is done
     * acquiring and releasing an event.
     */

    /* There are trasnfer descriptors in the free list */

    if (NULL != pHCDPipe->pHeadFreeTD)
        {
        /* Get the head trasnfer descriptor */

        pTD = pHCDPipe->pHeadFreeTD;

        /* The head itself is returned, update head ptr to the next */

        pHCDPipe->pHeadFreeTD = pTD->pNext;

        /* If this is the last trasnfer descriptor in the list, update the tail */

        if (pTD == pHCDPipe->pTailFreeTD)
            {
            pHCDPipe->pTailFreeTD = NULL;
            pHCDPipe->pHeadFreeTD = NULL;
            }

        /* Break from the free list */

        pTD->pNext = NULL;

        }
    else
        {
        /* Create a new TD */

        pTD = usbEhcdCreateITD(pHCDData);

        if (NULL == pTD)
            {
            USB_EHCD_ERR("usbEhcdGetFreeITD - usbEhcdCreateITD fail\n",
                0, 0, 0, 0, 0, 0);

            return NULL;
            }
        }

    /* Return the pointer to TD */

    return pTD;
    }/* End of usbEhcdGetFreeITD() */

/*******************************************************************************
*
* usbEhcdGetFreeSITD - get a free SITD of the pipe
*
* This routine is used to retrieve a SITD from the free TD list of the pipe,
* if the free TD list has no elements then create one TD.
*
* <pHCDData> - Pointer to the USB_EHCD_DATA structure
* <pHCDPipe> - Pointer to the USB_EHCD_PIPE structure
*
* Note: It is expetect the pipe lock has been taken when calling this routine.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* Note: It is expetect the pipe lock has been taken when calling this routine.
*
* RETURNS: Pointer to the USB_EHCD_SITD data structure or NULL
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_EHCD_SITD usbEhcdGetFreeSITD
    (
    pUSB_EHCD_DATA  pHCDData,   /* Pointer to the USB_EHCD_DATA structure */
    pUSB_EHCD_PIPE  pHCDPipe    /* Pointer to the USB_EHCD_PIPE structure */
    )
    {
    /* Pointer to the TD */

    pUSB_EHCD_SITD pTD = NULL;

    /*
     * Check if there is any trasnfer descriptor in the free list and
     * extract the trasnfer descriptor.Update the free list. This is done
     * acquiring and releasing an event.
     */

    /* There are trasnfer descriptors in the free list */

    if (NULL != pHCDPipe->pHeadFreeTD)
        {
        /* Get the head trasnfer descriptor */

        pTD = pHCDPipe->pHeadFreeTD;

        /* The head itself is returned, update head ptr to the next */

        pHCDPipe->pHeadFreeTD = pTD->pNext;

        /* If this is the last trasnfer descriptor in the list, update the tail */

        if (pTD == pHCDPipe->pTailFreeTD)
            {
            pHCDPipe->pTailFreeTD = NULL;
            pHCDPipe->pHeadFreeTD = NULL;
            }

        /* Break from the free list */

        pTD->pNext = NULL;

        }
    else
        {
        /* Create a new TD */

        pTD = usbEhcdCreateSITD(pHCDData);

        if (NULL == pTD)
            {
            USB_EHCD_ERR("usbEhcdGetFreeSITD - usbEhcdCreateSITD fail\n",
                0, 0, 0, 0, 0, 0);

            return NULL;
            }
        }

    /* Return the pointer to TD */

    return pTD;
    }/* End of usbEhcdGetFreeSITD() */

/*******************************************************************************
*
* usbEhcdInitQTD - initialize the QTD to a default state
*
* This routine is used to rinitialize the QTD to a default state.
*
* <pHCDData> - Pointer to the HCD data
* <pHCDPipe> - Pointer to the pipe
* <pQTD> - Pointer to the QTD
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbEhcdInitQTD
    (
    pUSB_EHCD_DATA  pHCDData,    /* Pointer to the USB_EHCD_DATA structure */
    pUSB_EHCD_PIPE  pHCDPipe,    /* Pointer to the USB_EHCD_PIPE structure */
    pUSB_EHCD_QTD   pQTD         /* Pointer to the QTD */
    )
    {
    /* To hold the HCD bus index */

    UINT32          uBusIndex = pHCDData->uBusIndex;

    /* Populate the fields of the QTD - Start */

    /* Invalidate Link pointer */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          QTD,
                          pQTD->uNextQTDPointer,
                          USB_EHCD_INVALID_LINK,
                          NEXTQTD_POINTER_T);

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          QTD,
                          pQTD->uAlternateNextQTDPointer,
                          USB_EHCD_INVALID_LINK,
                          ALTERNATE_NEXTQTDPOINTER_T);

    /* Indicate that a maximum of 2 errors is acceptable during a transfer */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          QTD,
                          pQTD->uTransferInfo,
                          USB_EHCD_MAX_ERRORS,
                          TOKEN_CERR);

    /* Enable the HC to start executing the Transaction */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          QTD,
                          pQTD->uTransferInfo,
                          USB_EHCD_QTD_STATUS_ACTIVE,
                          TOKEN_STATUS);

    /* Populate the fields of the QTD - End */

    return;
    }/* End of usbEhcdInitQTD() */

/*******************************************************************************
*
* usbEhcdInitITD - initialize the ITD to a default state
*
* This routine is used to rinitialize the ITD to a default state.
*
* <pHCDData> - Pointer to the HCD data
* <pHCDPipe> - Pointer to the pipe
* <pITD> - Pointer to the ITD
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbEhcdInitITD
    (
    pUSB_EHCD_DATA  pHCDData,    /* Pointer to the USB_EHCD_DATA structure */
    pUSB_EHCD_PIPE  pHCDPipe,    /* Pointer to the USB_EHCD_PIPE structure */
    pUSB_EHCD_ITD   pITD         /* Pointer to the ITD */
    )
    {
    /* To hold the HCD bus index */

    UINT32          uBusIndex = pHCDData->uBusIndex;

    /* To hold the endpoint number */

    UINT32          uEndPointNo;

    /*
     * Indicate the type of data structure pointed by the link pointer
     * to be a QH
     */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          ITD,
                          pITD->uNextLinkPointer,
                          USB_EHCD_TYPE_QH,
                          NEXT_LINK_POINTER_TYPE);

    /* Update the function address */

    /* pITD->dword9_plus[0].special = pHCDPipe->uAddress & 0x7F; */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          ITD,
                          pITD->uBufferPointerList[0],
                          (pHCDPipe->uAddress & 0x7F),
                          BUFFER_POINTER_DEVICE_ADDRESS);

    /* Update the endpoint number */

    uEndPointNo = (UINT32)USB_EHCD_GET_BITFIELD(uBusIndex,
                                       ITD,
                                       pITD->uBufferPointerList[0],
                                       BUFFER_POINTER_END_POINT_NUMBER);

    USB_EHCD_SET_BITFIELD(uBusIndex,
                           ITD,
                           pITD->uBufferPointerList[0],
                           (uEndPointNo |
                           (pHCDPipe->uEndpointAddress & ~USB_EHCD_DIR_IN)),
                           BUFFER_POINTER_END_POINT_NUMBER);

    /* Update the maximum packet size */

    /*  pITD->dword9_plus[1].special = pHCDPipe->uMaximumPacketSize & 0x7FF; */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          ITD,
                          pITD->uBufferPointerList[1],
                          (pHCDPipe->uMaximumPacketSize & 0x7FF),
                          BUFFER_POINTER_MAX_PACKET_SIZE);

    /* Update the data transfer direction */

    if (0 != (pHCDPipe->uEndpointAddress & USB_EHCD_DIR_IN))
        {
        UINT32 direction;

        direction = (UINT32)USB_EHCD_GET_BITFIELD(uBusIndex,
                                          ITD,
                                          pITD->uBufferPointerList[1],
                                          BUFFER_POINTER_DIRECTION);

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              ITD,
                              pITD->uBufferPointerList[1],
                              (direction | 1),
                              BUFFER_POINTER_DIRECTION);
        }

    /* Update the number of transactions possible in a microframe */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          ITD,
                          pITD->uBufferPointerList[2],
                           (((pHCDPipe->uMaximumPacketSize &
                           USB_EHCD_ENDPOINT_NUMBER_OF_TRANSACTIONS_MASK) >> 11) + 1),
                          BUFFER_POINTER_MULT);

    /* Indicate that this is the last trasnfer descriptor in the list */

    /*  pITD->dword0.t = USB_EHCD_INVALID_LINK; */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          ITD,
                          pITD->uNextLinkPointer,
                          USB_EHCD_INVALID_LINK,
                          NEXT_LINK_POINTER_T);

    }/* End of usbEhcdInitITD() */

/*******************************************************************************
*
* usbEhcdInitSITD - initialize the SITD to a default state
*
* This routine is used to rinitialize the SITD to a default state.
*
* <pHCDData> - Pointer to the HCD data
* <pHCDPipe> - Pointer to the pipe
* <pITD> - Pointer to the SITD
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbEhcdInitSITD
    (
    pUSB_EHCD_DATA  pHCDData,    /* Pointer to the USB_EHCD_DATA structure */
    pUSB_EHCD_PIPE  pHCDPipe,    /* Pointer to the USB_EHCD_PIPE structure */
    pUSB_EHCD_SITD  pSITD        /* Pointer to the SITD */
    )
    {
    /* To hold the HCD bus index */

    UINT32          uBusIndex = pHCDData->uBusIndex;

    /*
     * Indicate the type of data structure pointed by the link pointer
     * to be a QH
     */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          SITD,
                          pSITD->uNextLinkPointer,
                          USB_EHCD_TYPE_QH,
                          NEXT_LINK_POINTER_TYPE);

    /* Enable the HC to start executing the Transaction */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          SITD,
                          pSITD->uTransferState,
                          USB_EHCD_SITD_STATUS_ACTIVE,
                          TRANSFER_STATE_STATUS);

    /* Update the device address */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          SITD,
                          pSITD->uEndPointCharacteristics,
                          (pHCDPipe->uAddress & 0x7F),
                          ENDPOINT_CHARACTERISTICS_DEVICE_ADDRESS);

    /* Update the endpoint number */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          SITD,
                          pSITD->uEndPointCharacteristics,
                          (pHCDPipe->uEndpointAddress & 0x0F),
                          ENDPOINT_CHARACTERISTICS_ENDPT_NUMBER);


    /* Update the data transfer direction */

    if (0 != (pHCDPipe->uEndpointAddress & USB_EHCD_DIR_IN))
        {
        USB_EHCD_SET_BITFIELD(uBusIndex,
                          SITD,
                          pSITD->uEndPointCharacteristics,
                          1U,
                          ENDPOINT_CHARACTERISTICS_DIRECTION);
        }
    else
        {
        USB_EHCD_SET_BITFIELD(uBusIndex,
                          SITD,
                          pSITD->uEndPointCharacteristics,
                          0,
                          ENDPOINT_CHARACTERISTICS_DIRECTION);
        }

    /* Indicate that this is the last trasnfer descriptor in the list */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          SITD,
                          pSITD->uNextLinkPointer,
                          USB_EHCD_INVALID_LINK,
                          NEXT_LINK_POINTER_T);

    /* Update the hub info */

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          SITD,
                          pSITD->uEndPointCharacteristics,
                          ((pHCDPipe->uHubInfo &
                            USB_EHCD_PARENT_HUB_ADDRESS_MASK) >> 8),
                          ENDPOINT_CHARACTERISTICS_HUB_ADDR);

    USB_EHCD_SET_BITFIELD(uBusIndex,
                          SITD,
                          pSITD->uEndPointCharacteristics,
                          (pHCDPipe->uHubInfo & USB_EHCD_HUB_PORT_NUMBER_MASK),
                          ENDPOINT_CHARACTERISTICS_PORT_NUMBER);

    /* Update the start and complete split masks */

    if (0 != pHCDPipe->uUFrameMaskValue)
        {
        /* To hold temporarily the mask value */

        UINT8 uTempMask = pHCDPipe->uUFrameMaskValue;

        /* To hold the mask value */

        UINT8 uSMask = 0x01;

        /* To hold the shift count */

        UINT8 uShiftCount = 0;

        /* This loop extracts only the start split mask */

        while (0 != uTempMask)
            {
            /* Update the start split mask value */

            if (0 != (uTempMask & 0x01))
                {
                uSMask = (UINT8)(uSMask << uShiftCount);

                break;
                }
            else
                {
                uTempMask >>= 1;
                }

            uShiftCount++;
            }

        /* Update the start split mask */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                          SITD,
                          pSITD->uMicroFrameScheduleControl,
                          uSMask,
                          SCHEDULE_CONTROL_UFRAME_S);

        /* Update the complete split mask */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                          SITD,
                          pSITD->uMicroFrameScheduleControl,
                          (pHCDPipe->uUFrameMaskValue & ~uSMask),
                          SCHEDULE_CONTROL_UFRAME_C);
        }
    }/* End of usbEhcdInitSITD() */

/*******************************************************************************
*
* usbEhcdDestroyQH - free a QH
*
* This routine frees the QH structure.
*
* <pHCDData> - Pointer to the HCD data
* <pQH> - Pointer to the USB_EHCD_QH structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: Status of the free operation.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbEhcdDestroyQH
    (
    pUSB_EHCD_DATA  pHCDData,    /* Pointer to the USB_EHCD_DATA structure */
    pUSB_EHCD_QH    pQH          /* Pointer to the USB_EHCD_QH structure */
    )
    {
    /* Unload the DMA MAP */

    if (vxbDmaBufMapUnload (pHCDData->qhDmaTagId, pQH->dmaMapId) != OK)
        {
        USB_EHCD_ERR("usbEhcdDestroyQH - vxbDmaBufMapUnload fail\n",
            0, 0, 0, 0, 0, 0);

        return ERROR;
        }

    /* Free the memory allocated */

    return vxbDmaBufMemFree(pHCDData->qhDmaTagId, pQH, pQH->dmaMapId);
    }

/*******************************************************************************
*
* usbEhcdDestroyQTD - free a QTD
*
* This routine frees the QTD structure.
*
* <pHCDData> - Pointer to the HCD data
* <pQTD> - Pointer to the USB_EHCD_QTD structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: Status of the free operation.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbEhcdDestroyQTD
    (
    pUSB_EHCD_DATA   pHCDData,     /* Pointer to the USB_EHCD_DATA structure */
    pUSB_EHCD_QTD    pQTD          /* Pointer to the USB_EHCD_QTD structure */
    )
    {
    /* Unload the DMA MAP */

    if (vxbDmaBufMapUnload (pHCDData->qtdDmaTagId, pQTD->dmaMapId) != OK)
        {
        USB_EHCD_ERR("usbEhcdDestroyQTD - vxbDmaBufMapUnload fail\n",
            0, 0, 0, 0, 0, 0);

        return ERROR;
        }

    /* Free the memory allocated */

    return vxbDmaBufMemFree(pHCDData->qtdDmaTagId, pQTD, pQTD->dmaMapId);
    }

/*******************************************************************************
*
* usbEhcdDestroyITD - free a ITD
*
* This routine frees the ITD structure.
*
* <pHCDData> - Pointer to the HCD data
* <pITD> - Pointer to the USB_EHCD_ITD structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: Status of the free operation.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbEhcdDestroyITD
    (
    pUSB_EHCD_DATA  pHCDData,     /* Pointer to the USB_EHCD_DATA structure */
    pUSB_EHCD_ITD   pITD          /* Pointer to the USB_EHCD_ITD structure */
    )
    {
    /* Unload the DMA MAP */

    if (vxbDmaBufMapUnload (pHCDData->itdDmaTagId, pITD->dmaMapId) != OK)
        {
        USB_EHCD_ERR("usbEhcdDestroyITD - vxbDmaBufMapUnload fail\n",
            0, 0, 0, 0, 0, 0);

        return ERROR;
        }

    /* Free the memory allocated */

    return vxbDmaBufMemFree(pHCDData->itdDmaTagId, pITD, pITD->dmaMapId);
    }

/*******************************************************************************
*
* usbEhcdDestroySITD - free a SITD
*
* This routine frees the SITD structure.
*
* <pSITD> - Pointer to the USB_EHCD_SITD structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: Status of the free operation.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbEhcdDestroySITD
    (
    pUSB_EHCD_DATA  pHCDData,  /* Pointer to the USB_EHCD_DATA structure */
    pUSB_EHCD_SITD  pSITD      /* Pointer to the USB_EHCD_SITD structure */
    )
    {
    /* Unload the DMA MAP */

    if (vxbDmaBufMapUnload (pHCDData->sitdDmaTagId, pSITD->dmaMapId) != OK)
        {
        USB_EHCD_ERR("usbEhcdDestroyITD - vxbDmaBufMapUnload fail\n",
            0, 0, 0, 0, 0, 0);

        return ERROR;
        }

    /* Free the memory allocated */

    return vxbDmaBufMemFree(pHCDData->sitdDmaTagId, pSITD, pSITD->dmaMapId);
    }

/*******************************************************************************
*
* usbEhcdDestroyAllTDs - destroy all free TDs on the free TD list of the pipe
*
* This routine destroy all free TDs on the free TD list of the pipe.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbEhcdDestroyAllTDs
    (
    pUSB_EHCD_DATA  pHCDData,   /* Pointer to the USB_EHCD_DATA structure */
    pUSB_EHCD_PIPE  pHCDPipe    /* Pointer to the USB_EHCD_PIPE structure */
    )
    {
    if (USBHST_ISOCHRONOUS_TRANSFER != pHCDPipe->uEndpointType)
        {
        /* Pointer to the USB_EHCD_QTD data structure */

        pUSB_EHCD_QTD pQTD = NULL;

        /* Free the trasnfer descriptors in the QTD list */

        while ((pQTD = pHCDPipe->pHeadFreeTD) != NULL)
            {
            /* Head point to the next */

            pHCDPipe->pHeadFreeTD = pQTD->pNext;

            /* Destroy the QTD */

            usbEhcdDestroyQTD(pHCDData, pQTD);

            /* When the tail is reached, break */

            if (pQTD == pHCDPipe->pTailFreeTD)
                break;
            }
        }
    else
        {

        if (USBHST_HIGH_SPEED == pHCDPipe->uSpeed)
            {
            /* Pointer to the USB_EHCD_ITD data structure */

            pUSB_EHCD_ITD pITD = NULL;

            /* Free the trasnfer descriptors in the ITD list */

            while ((pITD = pHCDPipe->pHeadFreeTD) != NULL)
                {
                /* Head point to the next */

                pHCDPipe->pHeadFreeTD = pITD->pNext;

                /* Destroy the ITD */

                usbEhcdDestroyITD(pHCDData, pITD);

                /* When the tail is reached, break */

                if (pITD == pHCDPipe->pTailFreeTD)
                    break;
                }
            }
        else
            {
            /* Pointer to the USB_EHCD_SITD data structure */

            pUSB_EHCD_SITD pSITD = NULL;

            /* Free the trasnfer descriptors in the SITD list */

            while ((pSITD = pHCDPipe->pHeadFreeTD) != NULL)
                {
                /* Head point to the next */

                pHCDPipe->pHeadFreeTD = pSITD->pNext;

                /* Destroy the SITD */

                usbEhcdDestroySITD(pHCDData, pSITD);

                /* When the tail is reached, break */

                if (pSITD == pHCDPipe->pTailFreeTD)
                    break;
                }
            }
        }

    /* Update the head and tail pointers to NULL */

    pHCDPipe->pHeadFreeTD = NULL;
    pHCDPipe->pTailFreeTD = NULL;

    return;
    }/* End of function usbEhcdFreeAllLists() */

/*******************************************************************************
*
* usbEhcdCreateQH - create a QH
*
* This routine creates a QH structure.
*
* <pHCDData> - Pointer to the USB_EHCD_DATA structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: the pointer to the USB_EHCD_QH structure, or NULL if fail to create
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_EHCD_QH usbEhcdCreateQH
    (
    pUSB_EHCD_DATA  pHCDData    /* Pointer to the USB_EHCD_DATA structure */
    )
    {
    /* To hold the pointer to the QH */

    pUSB_EHCD_QH    pQH;

    /* Result of the vxbDmaBufMapLoad */

    STATUS          sts;

    /* The DMA MAP ID */

    VXB_DMA_MAP_ID  dmaMapId = NULL;

    /* Create memory and DMA map for the QH */

    pQH = vxbDmaBufMemAlloc (pHCDData->pDev,
            pHCDData->qhDmaTagId,
            NULL,
            0,
            &dmaMapId);

    /* Check if allocation is successful */

    if (pQH == NULL)
        {
        USB_EHCD_ERR("usbEhcdCreateQH - creating pQH fail\n",
            0, 0, 0, 0, 0, 0);

        return NULL;
        }

    /* Zero the memory allocated */

    bzero ((char *)pQH, USB_EHCD_MAX_QH_SIZE);

    /* Save our MAP ID */

    pQH->dmaMapId = dmaMapId;

    /*
     * Load the DMA MAP so that we get the correct BUS address
     * to program the hardware
     */

    sts = vxbDmaBufMapLoad (pHCDData->pDev,
            pHCDData->qhDmaTagId,
            pQH->dmaMapId,
            pQH,
            USB_EHCD_MAX_QH_SIZE,
            0);

    if (sts != OK)
        {
        USB_EHCD_ERR("usbEhcdCreateQH - loading map fail\n",
            0, 0, 0, 0, 0, 0);

        /* Free the memory allocated */

        (void) vxbDmaBufMemFree(pHCDData->qhDmaTagId, pQH, pQH->dmaMapId);

        return NULL;
        }

    return pQH;
    }

/*******************************************************************************
*
* usbEhcdCreateQTD - create a QTD
*
* This routine creates a QTD structure.
*
* <pHCDData> - Pointer to the USB_EHCD_DATA structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: the pointer to the USB_EHCD_QTD structure, or NULL if fail to create
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_EHCD_QTD usbEhcdCreateQTD
    (
    pUSB_EHCD_DATA  pHCDData    /* Pointer to the USB_EHCD_DATA structure */
    )
    {
    /* To hold the pointer to the QTD */

    pUSB_EHCD_QTD   pQTD;

    /* Result of the vxbDmaBufMapLoad */

    STATUS          sts;

    /* The DMA MAP ID */

    VXB_DMA_MAP_ID  dmaMapId = NULL;

    /* Create memory and DMA map for the QTD */

    pQTD = vxbDmaBufMemAlloc (pHCDData->pDev,
            pHCDData->qtdDmaTagId,
            NULL,
            0,
            &dmaMapId);

    /* Check if allocation is successful */

    if (pQTD == NULL)
        {
        USB_EHCD_ERR("usbEhcdCreateQTD - creating pQTD fail\n",
            0, 0, 0, 0, 0, 0);

        return NULL;
        }

    /* Zero the memory allocated */

    bzero ((char *)pQTD, USB_EHCD_MAX_QTD_SIZE);

    /* Save our MAP ID */

    pQTD->dmaMapId = dmaMapId;

    /*
     * Load the DMA MAP so that we get the correct BUS address
     * to program the hardware
     */

    sts = vxbDmaBufMapLoad (pHCDData->pDev,
            pHCDData->qtdDmaTagId,
            pQTD->dmaMapId,
            pQTD,
            USB_EHCD_MAX_QTD_SIZE,
            0);

    if (sts != OK)
        {
        USB_EHCD_ERR("usbEhcdCreateQTD - loading map fail\n",
            0, 0, 0, 0, 0, 0);

        /* Free the memory allocated */

        (void) vxbDmaBufMemFree(pHCDData->qtdDmaTagId, pQTD, pQTD->dmaMapId);

        return NULL;
        }

    return pQTD;
    }

/*******************************************************************************
*
* usbEhcdCreateITD - create a ITD
*
* This routine creates a ITD structure.
*
* <pHCDData> - Pointer to the USB_EHCD_DATA structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: the pointer to the USB_EHCD_ITD structure, or NULL if fail to create
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_EHCD_ITD usbEhcdCreateITD
    (
    pUSB_EHCD_DATA  pHCDData    /* Pointer to the USB_EHCD_DATA structure */
    )
    {
    /* To hold the pointer to the QTD */

    pUSB_EHCD_ITD   pITD;

    /* Result of the vxbDmaBufMapLoad */

    STATUS          sts;

    /* The DMA MAP ID */

    VXB_DMA_MAP_ID  dmaMapId = NULL;

    /* Create memory and DMA map for the QTD */

    pITD = vxbDmaBufMemAlloc (pHCDData->pDev,
            pHCDData->itdDmaTagId,
            NULL,
            0,
            &dmaMapId);

    /* Check if allocation is successful */

    if (pITD == NULL)
        {
        USB_EHCD_ERR("usbEhcdCreateITD - creating pITD fail\n",
            0, 0, 0, 0, 0, 0);

        return NULL;
        }

    /* Zero the memory allocated */

    bzero ((char *)pITD, USB_EHCD_MAX_ITD_SIZE);

    /* Save our MAP ID */

    pITD->dmaMapId = dmaMapId;

    /*
     * Load the DMA MAP so that we get the correct BUS address
     * to program the hardware
     */

    sts = vxbDmaBufMapLoad (pHCDData->pDev,
            pHCDData->itdDmaTagId,
            pITD->dmaMapId,
            pITD,
            USB_EHCD_MAX_ITD_SIZE,
            0);

    if (sts != OK)
        {
        USB_EHCD_ERR("usbEhcdCreateITD - loading map fail\n",
            0, 0, 0, 0, 0, 0);

        /* Free the memory allocated */

        (void) vxbDmaBufMemFree(pHCDData->itdDmaTagId, pITD, pITD->dmaMapId);

        return NULL;
        }

    return pITD;
    }

/*******************************************************************************
*
* usbEhcdCreateSITD - create a SITD
*
* This routine creates a SITD structure.
*
* <pHCDData> - Pointer to the USB_EHCD_DATA structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: the pointer to the USB_EHCD_SITD structure, or NULL if fail to create
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_EHCD_SITD usbEhcdCreateSITD
    (
    pUSB_EHCD_DATA  pHCDData    /* Pointer to the USB_EHCD_DATA structure */
    )
    {
    /* To hold the pointer to the QTD */

    pUSB_EHCD_SITD  pSITD;

    /* Result of the vxbDmaBufMapLoad */

    STATUS          sts;

    /* The DMA MAP ID */

    VXB_DMA_MAP_ID  dmaMapId = NULL;

    /* Create memory and DMA map for the QTD */

    pSITD = vxbDmaBufMemAlloc (pHCDData->pDev,
            pHCDData->sitdDmaTagId,
            NULL,
            0,
            &dmaMapId);

    /* Check if allocation is successful */

    if (pSITD == NULL)
        {
        USB_EHCD_ERR("usbEhcdCreateSITD - creating pSITD fail\n",
            0, 0, 0, 0, 0, 0);

        return NULL;
        }

    /* Zero the memory allocated */

    bzero ((char *)pSITD, USB_EHCD_MAX_SITD_SIZE);

    /* Save our MAP ID */

    pSITD->dmaMapId = dmaMapId;

    /*
     * Load the DMA MAP so that we get the correct BUS address
     * to program the hardware
     */

    sts = vxbDmaBufMapLoad (pHCDData->pDev,
            pHCDData->sitdDmaTagId,
            pSITD->dmaMapId,
            pSITD,
            USB_EHCD_MAX_SITD_SIZE,
            0);

    if (sts != OK)
        {
        USB_EHCD_ERR("usbEhcdCreateSITD - loading map fail\n",
            0, 0, 0, 0, 0, 0);

        /* Free the memory allocated */

        (void) vxbDmaBufMemFree(pHCDData->sitdDmaTagId, pSITD, pSITD->dmaMapId);

        return NULL;
        }

    return pSITD;
    }

/*******************************************************************************
*
* usbEhcdFormEmptyQH - form an empty QH and populate it with default values
*
* This routine checks if a QH can be retrieved from the free list. If a
* QH cannot be retrieved, it creates an empty QH. After retrieving a valid QH,
* populates the fields of the QH.
*
* <pHCDData> - Pointer to the USB_EHCD_DATA structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: Pointer to the USB_EHCD_QH data structure.
*
* ERRNO:N/A
*
* \NOMANUAL
*/

pUSB_EHCD_QH usbEhcdFormEmptyQH
    (
    pUSB_EHCD_DATA  pHCDData    /* Pointer to the USB_EHCD_DATA structure */
    )
    {
    /* To hold the pointer to the Queue Head */

    pUSB_EHCD_QH    pQH;

    /* To hold the HCD bus index */

    UINT32          uBusIndex = pHCDData->uBusIndex;

    /* Create a new QH */

    pQH = usbEhcdCreateQH(pHCDData);

    if (NULL == pQH)
        {
        USB_EHCD_ERR("usbEhcdFormEmptyQH - usbEhcdCreateQH fail\n",
            0, 0, 0, 0, 0, 0);

        return NULL;
        }

    /* Populate the fields of the QH - Start */

    /*
     * This field indicates to the hardware that
     * the item referenced by the link pointer is a QH
     */

    USB_EHCD_SET_BITFIELD(uBusIndex, QH, pQH->uQueueHeadHorizontalLinkPointer,
                             USB_EHCD_TYPE_QH, HORIZONTAL_LINK_POINTER_TYPE);

    /*
     * Indicate that there are no valid TDs
     * in this queue as the QH is newly created
     */

    USB_EHCD_SET_BITFIELD(uBusIndex, QH, pQH->uNextQtdPointer,
                             USB_EHCD_INVALID_LINK, NEXTQTD_POINTER_T);

    USB_EHCD_SET_BITFIELD(uBusIndex, QH, pQH->uAlternateNextQtdPointer,
                             USB_EHCD_INVALID_LINK, ALTERNATE_NEXTQTDPOINTER_T);

    OS_MEMSET(pQH->uExtBufferPointerPageList, 0, 20);

    OS_MEMSET(pQH->uBufferPagePointerList, 0, 20);

    /* Populate the fields of the QH - End */

    return pQH;
    }/* End of function usbEhcdFormEmptyQH() */

/*******************************************************************************
*
* usbEhcdFormEmptyQTD - form an empty QTD and populate it with default values
*
* This routine checks if a QTD can be retrieved from the free list. If a
* QTD cannot be retrieved, it creates an empty QTD. After retrieving a valid QTD,
* populates the fields of the QTD.
*
* <pHCDData> - Pointer to the USB_EHCD_DATA structure
* <pHCDPipe> - Pointer to the USB_EHCD_PIPE structure
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: Pointer to the USB_EHCD_QTD data structure or NULL.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_EHCD_QTD usbEhcdFormEmptyQTD
    (
    pUSB_EHCD_DATA  pHCDData,   /* Pointer to the USB_EHCD_DATA structure */
    pUSB_EHCD_PIPE  pHCDPipe    /* Pointer to the USB_EHCD_PIPE structure */
    )
    {
    /* To hold the pointer to the QTD */

    pUSB_EHCD_QTD   pQTD;

    /* Get a free QTD from the free list */

    pQTD = usbEhcdGetFreeQTD(pHCDData, pHCDPipe);

    /* Init QTD to be in default state */

    if (pQTD != NULL)
        usbEhcdInitQTD(pHCDData, pHCDPipe, pQTD);

    return pQTD;
    }/* End of usbEhcdFormEmptyQTD() */

/*******************************************************************************
*
* usbEhcdFillQTDBuffer - populate the buffer pointers of the QTD
*
* This routine is used to populate the buffer pointer fields of the QTD.
*
* <index>  - index of the host controller
* <pQTD>   - Pointer to the QTD.
* <pBuffer>  - Pointer to the start of the buffer
* <uSize> - Size of the data to be filled.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: Returns the number of bytes which were filled in qTD.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

UINT32 usbEhcdFillQTDBuffer
    (
    UINT8               index,          /* index of the host controller */
    pUSB_EHCD_QTD       pQTD,           /* Pointer to the QTD */
    bus_addr_t          pBuffer,        /* Pointer to the start of the buffer */
    bus_size_t          uSize,          /* Size of the buffer to be filled */
    UINT32              uMaxPacketSize  /* Maximum packet size */
    )
    {
    /* To hold the index into the buffer pointers of the qTD */

    UINT32      uIndex;

    /* To hold the current offset to fill the QTD */

    UINT32      uOffset;

    /* To hold the low 32 bit of a pointer page to fill the QTD */

    UINT32      uLow32;

    /* To hold the high 32 bit of a pointer page to fill the QTD */

    UINT32      uHigh32;

    /* To hold the first aligned page of the buffer to fill the QTD */

    bus_addr_t  uPagePointer;

    /* Get the current offset value */

    uOffset = (UINT32)(pBuffer & 0xFFF);

    /* Get the page aligned page pointer of the buffer */

    uPagePointer = pBuffer - uOffset;

    /*
     * Fill the number of bytes to transfer -
     * Totally 5 * 4K bytes can be
     * transferred using 1 qTD.
     */

    pQTD->uBytesToTransfer = (UINT32)(
        (uSize <= (bus_size_t)(USB_EHCD_QTD_MAX_TRANSFER_SIZE - uOffset)) ?
        uSize : (USB_EHCD_QTD_MAX_TRANSFER_SIZE - uOffset));

    /*
     * If uSize is more than BytesToTransfer for this QTD
     * and BytesToTransfer is not multiple of maximum packet
     * size supported by endpoint, then for IN transaction
     * BytesToTransfer should change to next multiple of
     * maximum packet size
     */

    if ((uSize > pQTD->uBytesToTransfer) &&
        (pQTD->uBytesToTransfer % uMaxPacketSize))
        {
        pQTD->uBytesToTransfer = pQTD->uBytesToTransfer -
            (pQTD->uBytesToTransfer % uMaxPacketSize);
        }

    /* Fill the Total Bytes to Transfer field */

    USB_EHCD_SET_BITFIELD(index,
                          QTD,
                          pQTD->uTransferInfo,
                          pQTD->uBytesToTransfer,
                          TOKEN_TOTAL_BYTES_TO_TRANSFER);

    /* Fill qTD's current offset into the buffer pointer */

    USB_EHCD_SET_BITFIELD(index,
                          QTD,
                          pQTD->uBufferPagePointerList[0],
                          (uOffset >> 0),
                          BUFFERPOINTER_CURRENT_OFFSET);

    /*
     * Update other buffer pointer fields of the qTD .
     * Each buffer pointer will be pointing to a different page.
     *
     * Note that we don't check the uSize and uBytesToTransfer,
     * we fill all the pointer pages with contiguous pages starting
     * from the request transfer buffer page, even though some
     * times the later pages may not be accessed by the host
     * controller if the Total Bytes to Transfer is small.
     */

    for (uIndex = 0; 5 > uIndex; ++uIndex)
        {
        /* Get the low 32 bit of the page pointer */

        uLow32 = USB_EHCD_BUS_ADDR_LO32 (uPagePointer);

        /* Get the high 32 bit of the page pointer */

        uHigh32 = USB_EHCD_BUS_ADDR_HI32 (uPagePointer);

        /* Fill low 32 bit of qTD's buffer pointer */

        USB_EHCD_SET_BITFIELD(index,
                              QTD,
                              pQTD->uBufferPagePointerList[uIndex],
                              (uLow32 >> 12),
                              BUFFER_POINTER);

        /* Fill hight 32 bit of qTD's buffer pointer */

        USB_EHCD_SET_BITFIELD(index,
                              QTD,
                              pQTD->uExtBufferPointerPageList[uIndex],
                              (uHigh32),
                              EXT_BUFFER_POINTER);

        /* Go to the next page */

        uPagePointer += USB_EHCD_PAGE_SIZE;
        }

    /* Return the number of bytes being transfered by this qTD */

    return (pQTD->uBytesToTransfer);
    }/* End of usbEhcdFillQTDBuffer() */

/*******************************************************************************
*
* usbEhcdCreateQTDs - create TDs and populate them
*
* This routine is used to create QTDs for a data transfer and to populate the
* fields of the QTDs.
*
* <pHCDData> - Pointer to the USB_EHCD_DATA structure
* <pHCDPipe> - Pointer to the pipe
* <pRequest> - Pointer to the request info
* <ppDataHead> - Pointer holding the pointer to the
*               head of the list of QTDS.
* <ppDataTail> - Pointer holding the pointer to the
*               tail of the list of QTDS.
* <pTransferBuffer> - Data buffer
* <uTransferLength> - length of the data buffer
* <uToggle>  - initial toggle value
* <uPID>  - PID value
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: Returns TRUE if the QTDs are created successfully.
*          Returns FALSE if the QTDs are not created successfully.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOLEAN usbEhcdCreateQTDs
    (
    pUSB_EHCD_DATA  pHCDData,       /* Pointer to the USB_EHCD_DATA structure */
    pUSB_EHCD_PIPE  pHCDPipe,       /* Pointer to the pipe */
    pUSB_EHCD_REQUEST_INFO  pRequest, /* Pointer to the request */
    pUSB_EHCD_QTD * ppDataHead,     /* Pointer to the head QTD */
    pUSB_EHCD_QTD * ppDataTail,     /* Pointer to the tail QTD */
    bus_addr_t      pTransferBuffer,/* Transfer buffer */
    bus_size_t      uTransferLength,/* Transfer length */
    UINT32          uMaximumPacketSize, /* Maximum packet size */
    UINT8           uToggle,        /* Initial toggle value */
    UINT8           uPID            /* PID value */
    )
    {
    /* Pointer to the current qTD */

    pUSB_EHCD_QTD pQTD = NULL;

    /* To hold the temporary pointer to the qTD */

    pUSB_EHCD_QTD pTempQTD = NULL;

    /* To hold the number of bytes transferred */

    UINT32 uBytesTransfered = 0;

    /* To hold the HCD bus index */

    UINT32      index = pHCDData->uBusIndex;


    /* Check the validity of the parameters */

    if (NULL == ppDataHead ||
        NULL == ppDataTail ||
        0 == pTransferBuffer)
        {
        USB_EHCD_ERR("usbEhcdCreateQTDs - Parameters are not valid\n",
            0, 0, 0, 0, 0, 0);

        return FALSE;
        }

    /* Create the 1st TD needed for the transfer */

    pQTD = *ppDataHead = usbEhcdFormEmptyQTD(pHCDData, pHCDPipe);

    /* If unable to create the TD, return FALSE */

    if (NULL == pQTD)
        {
        USB_EHCD_ERR("usbEhcdCreateQTDs - usbEhcdGetRequestFreeQTD fail 1\n",
            0, 0, 0, 0, 0, 0);

        return FALSE;
        }

    /* This loop gets executed till all the TDs are created */

    while (uBytesTransfered < uTransferLength)
        {
        /* Populate the TD's buffer pointers */

        uBytesTransfered += usbEhcdFillQTDBuffer((UINT8)index,
                                      pQTD,
                                      pTransferBuffer + uBytesTransfered,
                                      uTransferLength - uBytesTransfered,
                                      uMaximumPacketSize);

        /* Update the data toggle bit */

        USB_EHCD_SET_BITFIELD(index,
                             QTD,
                             pQTD->uTransferInfo,
                             uToggle,
                             TOKEN_DT);

        /* Update PID field of the qTD */

        USB_EHCD_SET_BITFIELD(index,
                             QTD,
                             pQTD->uTransferInfo,
                             uPID,
                             TOKEN_PID_CODE);

        /* If the bytes transfered is less than the total transfer size */

        if (uBytesTransfered < uTransferLength)
            {
            /* Temporay pointer to hold the QTD next pointer */

            UINT32  uNextQTDPointer;

            /* Create 1 more qTD */

            pQTD->pNext = usbEhcdFormEmptyQTD(pHCDData, pHCDPipe);

            /* Check if memory allocation is a failure */

            if (NULL == pQTD->pNext)
                {
                USB_EHCD_ERR("usbEhcdCreateQTDs - usbEhcdFormEmptyQTD fail 2\n",
                    0, 0, 0, 0, 0, 0);

                /* Free up all the TDs created */

                for (pQTD = *ppDataHead; NULL != pQTD; )
                    {
                    pTempQTD = pQTD->pNext;

                    /* Return the QTD back to the free list */

                    usbEhcdAddToFreeQTDList(pHCDData, pHCDPipe, pQTD);

                    pQTD = pTempQTD;
                    }

                /* Return FALSE */

                return FALSE;
                }/* End of if() */

            /* Fetch the host controller accessible region */

            /* Get the lower 32 bit of the BUS address */

            uNextQTDPointer = USB_EHCD_DESC_LO32(pQTD->pNext);

            /* Update the HC's link pointers */

            USB_EHCD_SET_BITFIELD(index,
                                  QTD,
                                  pQTD->uNextQTDPointer,
                                  (uNextQTDPointer >> 5),
                                  NEXTQTD_POINTER);

            /* Indicate that this is not the last trasnfer descriptor in the queue */

            USB_EHCD_SET_BITFIELD(index,
                                  QTD,
                                  pQTD->uNextQTDPointer,
                                  0,
                                  NEXTQTD_POINTER_T);

            /* Update the next trasnfer descriptor as the current qTD trasnfer descriptor */

            pQTD = pQTD->pNext;

            /*
             * There are no other trasnfer descriptors in the queue
             * after the newly created trasnfer descriptor.
             */

            pQTD->pNext = NULL;

            }/* End of if(uBytesTransfered < uTransferLength) */
        }/* End of while (uBytesTransfered < uTransferLength) */

    /* Update the tail pointer */

    *ppDataTail = pQTD;

    /* Return TRUE from the function */

    return TRUE;
    }/* End of function usbEhcdCreateQTDs() */

/*******************************************************************************
*
* usbEhcdUpdateQTDData - update the status of the transfer
*
* This routine is used to update the status of the non-isochronous
* transfer and the number of bytes transferred.
*
* <pHead> - Pointer to the head of the list of QTDS.
* <pTail> - Pointer to the tail of the list of QTDS.
* <pUrb> - Pointer to the URB.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbEhcdUpdateQTDData
    (
    UINT8           index,   /* index of the host controller */
    pUSB_EHCD_QTD   pHead,   /* Pointer to the head QTD */
    pUSB_EHCD_QTD   pTail,   /* Pointer to the tail QTD */
    pUSBHST_URB     pUrb     /* Pointer to the URB */
    )
    {
    /* To hold the temp qTD */

    pUSB_EHCD_QTD   pQtd = NULL;

    /* To hold the status of the TD */

    UINT8 uStatus = 0;

    /* To hold the current error counter of the TD */

    UINT8 uCerr = 0;

    /* Flag indicating that it is a control transfer request */

    BOOLEAN bIsControl = FALSE;

    pQtd = pHead;
    
    do
        {
        /* Check the pQtd */
	
        if (pQtd == NULL)
            {
            pUrb->nStatus = USBHST_FAILURE;
            pUrb->uTransferLength = 0;
            return;
            }

        /* Get the status */

        uStatus = (UINT8)USB_EHCD_GET_BITFIELD(index,
                                               QTD,
                                               pQtd->uTransferInfo,
                                               TOKEN_STATUS);

        /* If the status is not 0, break */

        if (uStatus != 0)
            break;
        
        /* If all TDs have been processed, break */

        if (pQtd == pTail)
            {
            break;
            }

        /* Move on to next TD */

        pQtd = pQtd->pNext;

        } while (1); /* End of do ... */
    
    /* Extract the current error counter of the qTD */

    uCerr = (UINT8)USB_EHCD_GET_BITFIELD(index,
                          QTD,
                          pQtd->uTransferInfo,
                          TOKEN_CERR );

    /* Update the URB status based on the status parameter  */

    if (0 == uStatus)
        {
        pUrb->nStatus = USBHST_SUCCESS;
        }

    /* Halted error condition */

    else if (0 != (uStatus & USB_EHCD_QTD_STATUS_HALTED))
        {
        pUrb->nStatus = USBHST_STALL_ERROR;

        USB_EHCD_DBG("Set to USBHST_STALL_ERROR 1\n", 0, 0, 0, 0, 0, 0);
        }

    /* Data buffer error- can be underrun or overrun. Should be checked */

    else if (0 != (uStatus & USB_EHCD_QTD_STATUS_DATA_BUFFER_ERROR))
        {
        pUrb->nStatus = USBHST_BUFFER_UNDERRUN_ERROR;

        /* Not Halted */

        if (0 == (uStatus & USB_EHCD_QTD_STATUS_HALTED))
            {
            /*
             * It was really a success
             */

            pUrb->nStatus = USBHST_SUCCESS;

            USB_EHCD_VDBG("Reset USBHST_BUFFER_UNDERRUN_ERROR to success\n",
                0, 0, 0, 0, 0, 0);
            }
        else
            {
            USB_EHCD_DBG("Set to USBHST_BUFFER_UNDERRUN_ERROR\n",
                0, 0, 0, 0, 0, 0);
            }
        }

    /*
     * babble error - this is set along with halt- so this condition
     * should always fail. Mentioned here for completion
     */

    else if (0 != (uStatus & USB_EHCD_QTD_STATUS_BABBLE_DETECTED))
        {
        pUrb->nStatus = USBHST_STALL_ERROR;

        /* Not Halted */

        if (0 == (uStatus & USB_EHCD_QTD_STATUS_HALTED))
            {
            /*
             * It was really a success
             */

            pUrb->nStatus = USBHST_SUCCESS;

            USB_EHCD_VDBG("Reset USBHST_STALL_ERROR to success \n",
                0, 0, 0, 0, 0, 0);
            }
        else
            {
            USB_EHCD_VDBG("Set to USBHST_STALL_ERROR 2\n",
                0, 0, 0, 0, 0, 0);
            }
        }

    /*
     * Transaction error.
     * Can possibly be Timeout, CRC or bad PID
     */

    else if (0 != (uStatus & USB_EHCD_QTD_STATUS_XACTERR))
        {
        pUrb->nStatus = USBHST_TIMEOUT;

        /* Transaction error occured. determine the error count. Should be 0 */

        USB_EHCD_VDBG(
                "Transaction Error count 0x%x Status - 0x%X Bytes left - %d\n",
                            (UINT8)USB_EHCD_GET_BITFIELD(index,
                            QTD,
                            pTail->uTransferInfo,
                            TOKEN_CERR),
                            uStatus,
                            (UINT8)USB_EHCD_GET_BITFIELD(index,
                            QTD,
                            pTail->uTransferInfo,
                            TOKEN_TOTAL_BYTES_TO_TRANSFER),
                            0, 0, 0);


        /*
         * In the case of retries, the sticky bits in the status word indicate
         * a accumulation of the errors.  If the transfer was successful, it is
         * possible these sticky bits will indicate an error (The EHCI spec is a
         * bit vague on this).  So the criteria for a successful transaction is
         * 1) Active bit clear, 2)Halt bit clear.
         *
         * Take a look at the status bits and if they meet the above criteria
         * make it a success after all
         */

        /* Not Active */

        if (0 == (uStatus & USB_EHCD_QTD_STATUS_ACTIVE))
            {
            /* Not Halted */

            if (0 == (uStatus & USB_EHCD_QTD_STATUS_HALTED))
                {
                /*
                 * It was really a success
                 */

                pUrb->nStatus = USBHST_SUCCESS;

                USB_EHCD_DBG("EHCI Status reset to success \n",
                    0, 0, 0, 0, 0, 0);
                }
            else
                {
                if (uCerr)
                    {
                    pUrb->nStatus = USBHST_STALL_ERROR;

                    USB_EHCD_VDBG("Set to USBHST_STALL_ERROR 3\n",
                        0, 0, 0, 0, 0, 0);
                    }
                else
                    {
                    USB_EHCD_DBG("Set to USBHST_TIMEOUT 1\n",
                        0, 0, 0, 0, 0, 0);
                    }
                }
            }
        else
            {
            USB_EHCD_VDBG("Set to USBHST_TIMEOUT 2\n",
                0, 0, 0, 0, 0, 0);
            }
        }

    /* This is set only for a full/low speed periodic transfer */

    else if (0 != (uStatus & USB_EHCD_QTD_STATUS_MISSED_UFRAME))
        {
        pUrb->nStatus = USBHST_FAILURE;

        /* Not Active */

        if (0 == (uStatus & USB_EHCD_QTD_STATUS_ACTIVE))
            {
            /* Not Halted */

            if (0 == (uStatus & USB_EHCD_QTD_STATUS_HALTED))
                {
                /*
                 * It really was a success
                 */

                pUrb->nStatus = USBHST_SUCCESS;

                USB_EHCD_VDBG("EHCI Status (microframe) reset to success \n",
                    0, 0, 0, 0, 0, 0);
                }
            else
                {
                USB_EHCD_DBG(
                    "Set to USBHST_FAILURE 1\n",
                    0, 0, 0, 0, 0, 0);
                }

            }
        else
            {
            USB_EHCD_DBG(
                "Set to USBHST_FAILURE 2\n",
                0, 0, 0, 0, 0, 0);
            }

        }
    else  /* USB_EHCD_QTD_STATUS_PING_STATE */
        {
        pUrb->nStatus = USBHST_SUCCESS;
        }

    /* If it is a SETUP PID, jump to next TD */

    if (USB_EHCD_SETUP_PID == USB_EHCD_GET_BITFIELD(index,
                                                  QTD,
                                                  pHead->uTransferInfo,
                                                  TOKEN_PID_CODE))
        {
        pHead = pHead->pNext;

        bIsControl = TRUE;
        }

    /* Update the number of bytes transferred to be 0 initially */

    pUrb->uTransferLength = 0;

    /*
     * This loop traverses the queue of requests
     * and updates the number of bytes transferred
     */

    do
        {
        /* Check if the TD has been processed */

        if (0 == (USB_EHCD_GET_BITFIELD(index,
                                       QTD,
                                       pHead->uTransferInfo,
                                       TOKEN_STATUS) &
                                       USB_EHCD_QTD_STATUS_ACTIVE))
            {
            /* Check if it is a control transfer & we have reached tail */

            if ((TRUE == bIsControl) && (pHead == pTail))
                {
                break;
                }

            /* Update the number of bytes transfered */

            pUrb->uTransferLength += (pHead->uBytesToTransfer -
                (UINT32)USB_EHCD_GET_BITFIELD(index,
                                    QTD,
                                    pHead->uTransferInfo,
                                    TOKEN_TOTAL_BYTES_TO_TRANSFER));
            }
            /* End of if(0 == (pHead->dword2.status & */

        else
            {
            break;
            }/* End of else */

        /* If all TDs have been processed, break */

        if (pHead == pTail)
            {
            break;
            }

        /* Move on to next TD */

        pHead = pHead->pNext;

        } while (1);

    return;
    }

/*******************************************************************************
*
* usbEhcdUpdateITDData - update the status in an isochronous transfer
*
* This routine is used to update the number of bytes transferred and
* status in a isochronous transfer.
*
* <index> - index of the host controller
* <uMicroFrameMask> - Micro frame mask to be tested for status check.
* <pHead> - Pointer to the head of the list of ITDS.
* <pTail> - Pointer to the tail of the list of ITDS.
* <pVoid> - Pointer for extend.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbEhcdUpdateITDData
    (
    UINT8           index,           /* index of the host controller */
    UINT8           uMicroFrameMask, /* Micro frame mask to be tested */
    pUSB_EHCD_ITD   pHead,           /* Pointer to the head QTD */
    pUSB_EHCD_ITD   pTail,           /* Pointer to the tail QTD */
    void *          pVoid            /* Pointer for extend */
    )
    {
    UINT32                  uCount = 0;
    UINT32                  uTxLen = 0;
    UINT8                   uStatus;
    UINT8                   uMicroFrameIndex;
    pUSBHST_ISO_PACKET_DESC pPacketDes = NULL; /* Pointer to the Packet descriptor */
    pUSBHST_URB             pUrb       = NULL; /* Pointer to the URB */

    pUrb = (pUSBHST_URB) pVoid;
    pPacketDes = (pUSBHST_ISO_PACKET_DESC)(pUrb->pTransferSpecificData);

    /*
     * This loop traverses the queue of requests
     * and updates the number of bytes transferred and status
     */
    
    pUrb->nStatus = USBHST_SUCCESS;
    pUrb->uTransferLength = 0;

    do
        {
        for (uMicroFrameIndex=0;
             USB_EHCD_MAX_MICROFRAME_NUMBER > uMicroFrameIndex;
             uMicroFrameIndex++)
            {

            if ((uMicroFrameMask >> uMicroFrameIndex) & 0x01)
                {
                /* Update the length of each microframe */

                uTxLen = (UINT32)USB_EHCD_GET_BITFIELD(index,
                         ITD,
                         pHead->uTransactionStatusControlList[uMicroFrameIndex],
                         TRANSACTION_LENGTH);

                pPacketDes[uCount].uLength -= uTxLen;
                
                /* get the status and update for per packet  */

                uStatus = (UINT8)USB_EHCD_GET_BITFIELD(index,
                          ITD,
                          pHead->uTransactionStatusControlList[uMicroFrameIndex],
                          TRANSACTION_STATUS);

                if ((uStatus & USB_EHCD_ITD_STATUS_ERROR_MASK) != 0)
                    {
                    USB_EHCD_ERR("usbEhcdUpdateITDData - "
                                 "transfer failed %d, %d, 0x%x. \n", 
                                 uCount, uTxLen, uStatus, 0, 0, 0);
                    
                    pUrb->nStatus = USBHST_FAILURE;
                    }
                else if (uTxLen != 0)
                    {
                    USB_EHCD_WARN("usbEhcdUpdateITDData - short packet or "
                                  "transfer has not been end %d, %d, 0x%x.\n",
                                  uCount, uTxLen, uStatus, 0, 0, 0);
                    }


                if ((0 != (uStatus & USB_EHCD_ITD_STATUS_BABBLE_DETECTED))||
                    (0 != (uStatus & USB_EHCD_ITD_STATUS_XACTERR)))
                    {
                    pPacketDes[uCount].nStatus = USBHST_FAILURE;
                    }
                else if (0 != (uStatus & USB_EHCD_ITD_STATUS_BUFFER_ERROR))
                    {
                    pPacketDes[uCount].nStatus = USBHST_BUFFER_UNDERRUN_ERROR;
                    }
                else /* Success */
                    {
                    pPacketDes[uCount].nStatus = USBHST_SUCCESS;
                    
                    pUrb->uTransferLength += pPacketDes[uCount].uLength;
                    }

                uCount++;
                } /* End of if statement .... */
            else
                {
                USB_EHCD_ERR("usbEhcdUpdateITDData - parameters not valid 0x%x, 0x%x.\n",
                             uMicroFrameMask, uMicroFrameIndex, 0, 0, 0, 0);
                }
            } /* End of for loop */

        if (pHead == pTail) 
            break;

        /* Move on to next TD */

        pHead = pHead->pVerticalNext;

        } while (1);

    return;
    }/* End of Function usbEhcdUpdateITDData */

/*******************************************************************************
*
* usbEhcdUpdateSITDData - update the status in an isochronous transfer
*
* This function is used to update the number of bytes transferred and
* status in an isochronous transfer.
*
* <index> - index of the host controller
* <pHead> - Pointer to the head of the list of SITDS.
* <pTail> - Pointer to the tail of the list of SITDS.
* <pVoid> - Pointer for extend.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO:N/A
*
* \NOMANUAL
*/

VOID usbEhcdUpdateSITDData
    (
    UINT8           index,   /* index of the host controller */
    pUSB_EHCD_SITD  pHead,   /* Pointer to the head QTD */
    pUSB_EHCD_SITD  pTail,   /* Pointer to the tail QTD */
    void *          pVoid    /* Pointer for extend */
    )
    {
    UINT32  uCount = 0;
    UINT32  uTxLen = 0;
    UINT8   uStatus;
    pUSBHST_ISO_PACKET_DESC pPacketDes = NULL; /* Pointer to the Packet descriptor */
    pUSBHST_URB             pUrb       = NULL; /* Pointer to the URB */

    pUrb = (pUSBHST_URB) pVoid;
    pPacketDes = (pUSBHST_ISO_PACKET_DESC)(pUrb->pTransferSpecificData);

    pUrb->nStatus = USBHST_SUCCESS;
    pUrb->uTransferLength = 0;
    
    /*
     * This loop traverses the queue of requests
     * and updates the number of bytes transferred and status
     */

    do
        {
        /* Update the length of each microframe */

        uTxLen = (UINT32)USB_EHCD_GET_BITFIELD(index,
                        SITD,
                        pHead->uTransferState,
                        TRANSFER_STATE_TOTAL_BYTES_TO_TRANSFER);

        pPacketDes[uCount].uLength -= uTxLen;

        /* Get the status and update for per packet  */

        uStatus = (UINT8)USB_EHCD_GET_BITFIELD(index,
                        SITD,
                        pHead->uTransferState,
                        TRANSFER_STATE_STATUS);

        if ((uStatus & USB_EHCD_SITD_STATUS_ERROR_MASK) != 0)
            {
            USB_EHCD_ERR("usbEhcdUpdateSITDData - "
                         "transfer failed %d, %d, 0x%x. \n", 
                         uCount, uTxLen, uStatus, 0, 0, 0);

            pUrb->nStatus = USBHST_FAILURE;
            }
        else if (uTxLen != 0)
            {
            USB_EHCD_WARN("usbEhcdUpdateSITDData - short packet or "
                          "transfer has not been end %d, %d, 0x%x.\n",
                          uCount, uTxLen, uStatus, 0, 0, 0);
            }


        if (0 != (uStatus & USB_EHCD_SITD_STATUS_ERROR))
            {
            pPacketDes[uCount].nStatus = USBHST_FAILURE;
            }
        else if (0 != (uStatus & USB_EHCD_SITD_STATUS_BUFFER_ERROR))
            {
            pPacketDes[uCount].nStatus = USBHST_BUFFER_UNDERRUN_ERROR;
            }
        else if (0 != (uStatus & USB_EHCD_SITD_STATUS_BABBLE_DETECTED))
            {
            pPacketDes[uCount].nStatus = USBHST_FAILURE;
            }
        else if (0 != (uStatus & USB_EHCD_SITD_STATUS_XACTERR))
            {
            pPacketDes[uCount].nStatus = USBHST_FAILURE;
            }
        else if (0 != (uStatus & USB_EHCD_SITD_STATUS_MISSED_FRAME))
            {
            pPacketDes[uCount].nStatus = USBHST_FAILURE;
            }
        else /* Success */
            {
            pPacketDes[uCount].nStatus = USBHST_SUCCESS;
            
            pUrb->uTransferLength += pPacketDes[uCount].uLength;
            }

        uCount++;

        if (pHead == pTail) 
            break;

        /* Move on to next TD */

        pHead = pHead->pVerticalNext;

        } while (1);

    return;
    }/* End of Function usbEhcdUpdateSITDData */

/*******************************************************************************
*
* usbEhcdFormEmptyITD - form an empty ITD
*
* This function creates an empty ITD and populates it.
*
* <pHCDData> - Pointer to the USB_EHCD_DATA structure.
* <pHCDPipe> - Pointer to the USB_EHCD_PIPE structure.
*
* RETURNS: TRUE if the bandwidth reserved is within the limits.
*          FALSE if the bandwidth is not within the limits.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_EHCD_ITD usbEhcdFormEmptyITD
    (
    pUSB_EHCD_DATA  pHCDData,/* Pointer to the USB_EHCD_DATA structure */
    pUSB_EHCD_PIPE  pHCDPipe /* Pointer to the USB_EHCD_PIPE structure */
    )
    {
    /* To hold the pointer to the isochronous TD data structure */

    pUSB_EHCD_ITD   pITD;

    /* Get a free ITD from the free list */

    pITD = usbEhcdGetFreeITD(pHCDData, pHCDPipe);

    /* Init ITD to be in default state */

    if (pITD != NULL)
        usbEhcdInitITD(pHCDData, pHCDPipe, pITD);

    /* Return the pointer to the isochronous TD */

    return (pITD);
    }/* End of usbEhcdFormEmptyITD() */

/*******************************************************************************
*
* usbEhcdFillITD - fill the ITD buffer
*
* This routine is used to fill the buffer pointer fields of the ITD.
*
* <index> - index of the host controller.
* <pITD> - Pointer to the USB_EHCD_ITD structure.
* <uMicroFrameMask> - mask value
* <pBuffer>  - Pointer to the buffer.
* <uSize>  - Size of the buffer.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: number of bytes which can be transferred using the iTD.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL UINT32 usbEhcdFillITD
    (
    UINT8           index,           /* index of the host controller */
    pUSB_EHCD_ITD   pITD,            /* Pointer to the ITD */
    UINT8           uMicroFrameMask, /* Microframe mask value */
    bus_addr_t      pBuffer,         /* Pointer to the buffer value */
    UINT32          uPktCnt,             /* No of packet */
    pUSBHST_ISO_PACKET_DESC pIsocPktDesc /* array of packet descriptor */
    )
    {

    /* To hold the index of the microframe */

    UINT8       uMicroFrameIndex;

    /* To hold the page number field */

    INT32       nCpg = -1;

    /*
     * To hold the maximum number of transactions
     * that can happen in a microframe
     */

    UINT8       uCount = 0;

    /* To hold the current offset to fill the QTD */

    UINT32      uOffset;

    /* To hold the low 32 bit of a pointer page to fill the QTD */

    UINT32      uLow32;

    /* To hold the high 32 bit of a pointer page to fill the QTD */

    UINT32      uHigh32;

    /* To hold the the buffer pointer to fill the QTD */

    bus_addr_t  uBufferPointer;

    /* To hold the aligned page of the buffer to fill the QTD */

    bus_addr_t  uPagePointer;

    /* Check if the buffer is NULL or no bytes to transfer */

    if (0 == pBuffer ||  0 == uPktCnt)
        {
        USB_EHCD_ERR("Invalid parameters (%p, %d)\n",
            pBuffer, uPktCnt, 0, 0, 0, 0);

        return 0;
        }

    /* This loop populates the buffer pointers for all the microframes */

    for (uMicroFrameIndex = 0;
        (USB_EHCD_MAX_MICROFRAME_NUMBER > uMicroFrameIndex);
        ++uMicroFrameIndex)
        {

        /* Check if this microframe is to be used */

        if ((uMicroFrameMask >> uMicroFrameIndex) & 0x01)
            {
            /* Get the the buffer pointer */

            uBufferPointer = (pBuffer + (pIsocPktDesc[uCount].uOffset));

            /* Get the current offset value */

            uOffset = (UINT32)(uBufferPointer & 0xFFF);

            /* Get the page aligned page pointer of the buffer */

            uPagePointer = uBufferPointer - uOffset;

            /* Get the low 32 bit of the page pointer */

            uLow32 = USB_EHCD_BUS_ADDR_LO32 (uPagePointer);

            /* Get the high 32 bit of the page pointer */

            uHigh32 = USB_EHCD_BUS_ADDR_HI32 (uPagePointer);

            /* Update the status of the iTD - Indicate that the iTD is active */

            USB_EHCD_SET_BITFIELD(index,
                ITD,
                pITD->uTransactionStatusControlList[uMicroFrameIndex],
                USB_EHCD_ITD_STATUS_ACTIVE,
                TRANSACTION_STATUS);

            /*
             * Update the offset of the buffer from
             * where the transaction should start
             */

            USB_EHCD_SET_BITFIELD(index,
                ITD,
                pITD->uTransactionStatusControlList[uMicroFrameIndex],
                uOffset,
                TRANSACTION_OFFSET);

            /* Populate the length field of the iTD */

            USB_EHCD_SET_BITFIELD(index,
                ITD,
                pITD->uTransactionStatusControlList[uMicroFrameIndex],
                pIsocPktDesc[uCount].uLength,
                TRANSACTION_LENGTH);

            /*
             * If this is the first buffer pointer to be updated
             * or if the same buffer offset is not filled earlier
             */

            if ((nCpg == -1) ||
                (USB_EHCD_GET_BITFIELD(index,
                                       ITD,
                                       pITD->uBufferPointerList[nCpg],
                                       BUFFER_POINTER) !=
                                       (uLow32 >> 12)))

                {
                /* Increment the page number */

                ++nCpg;

                /* Update the buffer pointer in the iTD */

                USB_EHCD_SET_BITFIELD(index,
                                      ITD,
                                       pITD->uBufferPointerList[nCpg],
                                       (uLow32 >> 12),
                                       BUFFER_POINTER);

                /* Update the Extended buffer pointer in the iTD */

                USB_EHCD_SET_BITFIELD(index,
                                      ITD,
                                       pITD->uExtBufferPointerPageList[nCpg],
                                       (uHigh32 >> 12),
                                       EXT_BUFFER_POINTER);
                }

            /* Update the page number in the iTD */

            USB_EHCD_SET_BITFIELD(index,
                       ITD,
                       pITD->uTransactionStatusControlList[uMicroFrameIndex],
                       nCpg,
                       TRANSACTION_PAGE_SELECT);


            /*
             * Check if buffer's last byte of present transaction is
             * not represented in the buffer_pointers of iTD
             */

            if (((uBufferPointer +
                  USB_EHCD_GET_BITFIELD(index,
                    ITD,
                    pITD->uTransactionStatusControlList[uMicroFrameIndex],
                    TRANSACTION_LENGTH) - 1) >> 12)
                != USB_EHCD_GET_BITFIELD(index,
                                         ITD,
                                         pITD->uBufferPointerList[nCpg],
                                         BUFFER_POINTER))

                {

                /* Increment the page number */

                ++nCpg;

                /* Update the buffer pointer in the iTD */

                USB_EHCD_SET_BITFIELD(index,
                                      ITD,
                                      pITD->uBufferPointerList[nCpg],
                                      (USB_EHCD_GET_BITFIELD(index,
                                        ITD,
                                        pITD->uBufferPointerList[nCpg - 1],
                                        BUFFER_POINTER) + 1),
                                      BUFFER_POINTER);

                /* Update the Extended buffer pointer in the iTD */

                USB_EHCD_SET_BITFIELD(index,
                                      ITD,
                                      pITD->uExtBufferPointerPageList[nCpg],
                                      (USB_EHCD_GET_BITFIELD(index,
                                        ITD,
                                        pITD->uExtBufferPointerPageList[nCpg - 1],
                                        EXT_BUFFER_POINTER) + 1),
                                      EXT_BUFFER_POINTER);

                }

            /* Update the number of packets transfered */

            uCount++;

            if (uCount >= uPktCnt)
                {
                /* Set the ioc for the last microframe which has data */

                USB_EHCD_SET_BITFIELD(index,
                                      ITD,
                                      pITD->uTransactionStatusControlList[uMicroFrameIndex],
                                      1,
                                      TRANSACTION_IOC);
                break;
                }
            }/* End of if( (uframeMask >> uf) & 0x1) */
        }/* End of for() */

    /* Return the number of bytes filled */

    return (uCount);
    }/* End of usbEhcdFillITDBuffer() */

/*******************************************************************************
*
* usbEhcdGenerateITDs - create iTDs needed for a transfer
*
* This routine creates the isochronous TDs required for an isochronous
* data transfer.
*
* <pHCDData> - Pointer to the USB_EHCD_DATA structure.
* <pHCDPipe> - Pointer to the USB_EHCD_PIPE structure.
* <pRequest> - Pointer to the request info
* <ppHead> - Pointer to the memory location holding
*            the head of the iTDs queued.
* <ppTail> - Pointer to the memory location holding the tail of the iTDs queued.
* <pBuffer> - Pointer to the buffer holding the data.
* <uSize>  - Size of the buffer.
* <uMicroFrameMask> - Bitmap value holding the microframes
*                     in which the data can be transferred
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: TRUE if the iTDs are created successfully..
*          FALSE if the iTDs are not created successfully.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOLEAN usbEhcdGenerateITDs
    (
    pUSB_EHCD_DATA  pHCDData,/* Pointer to the USB_EHCD_DATA structure */
    pUSB_EHCD_PIPE  pHCDPipe, /* Pointer to the USB_EHCD_PIPE structure */
    pUSB_EHCD_REQUEST_INFO  pRequest, /* Pointer to the request */
    pUSB_EHCD_ITD   *ppHead,  /* Pointer to the head ITD pointer */
    pUSB_EHCD_ITD   *ppTail,  /* Pointer to the tail ITD pointer */
    bus_addr_t      pBuffer,         /* Pointer to the buffer */
    UINT32          uPktCnt,         /* Size of the buffer */
    pUSBHST_ISO_PACKET_DESC pIsocPktDesc /* array of packet descriptor */
    )
    {
    /* To hold the number of bytes transferred */

    UINT32 uPktTransfered = 0;

    /* To hold the pointer to the current iTD */

    pUSB_EHCD_ITD pCurrentITD;

    /* To hold the pointer to the temporary iTD trasnfer descriptor */

    pUSB_EHCD_ITD pTempITD;

    /* To hold the microframe index */

    UINT8 uMicroFrameIndex = 0;

    /* Microframe mask value */

    UINT8 uMicroFrameMask = 0;

    /* To hold the HCD bus index */

    UINT32      index = pHCDData->uBusIndex;

    /* Create the 1st TD needed for the transfer */

    pCurrentITD = *ppHead = usbEhcdFormEmptyITD(pHCDData, pHCDPipe);

    /* If unable to create the TD, return FALSE */

    if (NULL == pCurrentITD)
        {
        USB_EHCD_ERR("ITD memory not allocated\n", 0, 0, 0, 0, 0, 0);

        return FALSE;
        }

    /* Get the mask value for this pipe */

    uMicroFrameMask = pHCDPipe->uUFrameMaskValue;

    /*
     * This loop will create the TDs required
     * to transfer all the bytes required
     */

    while (uPktTransfered < uPktCnt)
        {
        /*
         * Fill the iTDs buffer pointers and
         * update the number of bytes transferred
         */

        uPktTransfered += usbEhcdFillITD((UINT8)index,
                           pCurrentITD,
                           uMicroFrameMask,
                           pBuffer, (uPktCnt - uPktTransfered),
                           &pIsocPktDesc[uPktTransfered]);

        /* If not all bytes have been transfered */

        if (uPktTransfered < uPktCnt)
            {
            /* Create once more iTD */

            pCurrentITD->pVerticalNext =
                         usbEhcdFormEmptyITD(pHCDData, pHCDPipe);

            /* If unable to allocate memory */

            if (NULL == pCurrentITD->pVerticalNext)
                {
                /* Free up all allocated iTDs */

                for (pCurrentITD = *ppHead; pCurrentITD != NULL; )
                    {
                    pTempITD = pCurrentITD->pVerticalNext;

                    /* Return the ITD to the free ITD list */

                    usbEhcdAddToFreeITDList(pHCDData, pHCDPipe, pCurrentITD);

                    /* Update the current ITD */

                    pCurrentITD = pTempITD;
                    }

                return FALSE;
                }

            /* Update the current iTD pointer */

            pCurrentITD = pCurrentITD->pVerticalNext;

            /* There are no trasnfer descriptors after the newly created iTD */

            pCurrentITD->pVerticalNext = NULL;
            }/* End of if(uBytesTransfered < uSize) */
        }/* End of while(uBytesTransfered < uSize) */

    /* Update the tail pointer */

    *ppTail = pCurrentITD;

    /*
     * Mark the last uFrames Interrupt on completion bit as 1
     * This will generate an interrupt on completion of the last iTD transfer
     */

    for (uMicroFrameIndex = 0;
         USB_EHCD_MAX_MICROFRAME_NUMBER > uMicroFrameIndex;
         uMicroFrameIndex++)
        {
        if ((uMicroFrameMask << uMicroFrameIndex) & 0x80)
            {
            USB_EHCD_SET_BITFIELD(index,
                                  ITD,
                                  pCurrentITD->uTransactionStatusControlList[
                                  (USB_EHCD_MAX_MICROFRAME_NUMBER - 1 - uMicroFrameIndex)],
                                  1,
                                  TRANSACTION_IOC);
            break;
            }
        }/* End of for () */

    /* Return success */

    return TRUE;
    }/* End of usbEhcdGenerateITDs() */

/*******************************************************************************
*
* usbEhcdLinkITDs - link the ITDs
*
* This routine links the isochronous TDs created in the frame list.
*
* <pHCDData> - Pointer to the EHCD_DATA structure
* <pHCDPipe> - Pointer to the USB_EHCD_PIPE structure.
* <uFrameNumber> - Frame index for linking
* <pHead> - Pointer to the head of the ITDs
* <pTail> - Pointer to the tail of the iTDs
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbEhcdLinkITDs
    (
    pUSB_EHCD_DATA  pHCDData, /* Pointer to the EHCD_DATA structure */
    pUSB_EHCD_PIPE  pHCDPipe, /* Pointer to the USB_EHCD_PIPE structure */
    UINT32          uFrameNumber, /* Frame index for linking */
    pUSB_EHCD_ITD   pHead,    /* Pointer to the head ITD */
    pUSB_EHCD_ITD   pTail     /* Pointer to the tail ITD */
    )
    {
    /* To hold the index into the periodic frame list */

    /* Get the correct position of frame to start with */

    UINT32          uCount = uFrameNumber;

    /* To hold the ITD of list */

    pUSB_EHCD_ITD   pITD;

    /* Pointer to the frame list data array */

    pUSB_EHCD_LIST_DATA pFrameListData;

    /* Index of the host controller */

    UINT32          uBusIndex;

    /* Next trasnfer descriptor low 32 bit BUS address */

    UINT32          uNextLow32;

    /* Extract the index of the host controller */

    uBusIndex = pHCDData->uBusIndex;

    /* Get the first ITD of the list. */

    pITD = pHead;

    while (pITD != NULL)
        {
        /* Get pointer to the frame list data */

        pFrameListData = &pHCDData->FrameListData[uCount];

        /* Check if there is any isochronous TDs in this frame. */

        if (pFrameListData->pHeadPointer == NULL)
            {
            pFrameListData->pHeadPointer = pFrameListData->pTailPointer = pITD;
            }
        else
            {
            /* Add the new ITD to the head of the list */

            if (pFrameListData->uHeadPointerType == USB_EHCD_TYPE_ITD)
                {
                pITD->pNext = pFrameListData->pHeadPointer;
                ((pUSB_EHCD_ITD)(pITD->pNext))->pPreviousTD = pITD;
                ((pUSB_EHCD_ITD)(pITD->pNext))->uPreviousTDType = USB_EHCD_TYPE_ITD;
                pITD->uNextTDType = USB_EHCD_TYPE_ITD;
                }
            else
                {
                pITD->pNext =  pFrameListData->pHeadPointer;
                ((pUSB_EHCD_SITD)(pITD->pNext))->pPreviousTD = pITD;
                ((pUSB_EHCD_SITD)(pITD->pNext))->uPreviousTDType = USB_EHCD_TYPE_ITD;
                pITD->uNextTDType = USB_EHCD_TYPE_SITD;
                }

            /* Update the head pointer to point to the new ITD */

            pFrameListData->pHeadPointer = pITD;
            }

        /* Set the head pointer type */

        pFrameListData->uHeadPointerType = USB_EHCD_TYPE_ITD;

        /* Copy the next pointers details from frame list */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                               ITD,
                               pITD->uNextLinkPointer,
                               USB_EHCD_GET_BITFIELD(uBusIndex,
                                               FRAME_LIST,
                                               *(pHCDData->pFrameList + uCount),
                                               POINTER),
                               NEXT_LINK_POINTER);

        USB_EHCD_SET_BITFIELD(uBusIndex,
                               ITD,
                               pITD->uNextLinkPointer,
                               USB_EHCD_GET_BITFIELD(uBusIndex,
                                               FRAME_LIST,
                                               *(pHCDData->pFrameList + uCount),
                                               POINTER_VALID_ENTRY),
                               NEXT_LINK_POINTER_T);

        USB_EHCD_SET_BITFIELD(uBusIndex,
                               ITD,
                               pITD->uNextLinkPointer,
                               USB_EHCD_GET_BITFIELD(uBusIndex,
                                               FRAME_LIST,
                                               *(pHCDData->pFrameList + uCount),
                                               POINTER_TYPE),
                               NEXT_LINK_POINTER_TYPE);

        /* Get the lower 32 bit of the BUS address */

        uNextLow32 = USB_EHCD_DESC_LO32(pITD);

        /* Attach ITD to the frame list */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                               FRAME_LIST,
                               *(pHCDData->pFrameList + uCount),
                               (uNextLow32 >> 5),
                               POINTER);

        /* Update the frame list pointer details */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                               FRAME_LIST,
                               *(pHCDData->pFrameList + uCount),
                               0,
                               POINTER_VALID_ENTRY);

        USB_EHCD_SET_BITFIELD(uBusIndex,
                               FRAME_LIST,
                               *(pHCDData->pFrameList + uCount),
                               USB_EHCD_TYPE_ITD,
                               POINTER_TYPE);

        /* Update the frame list index in the ITD */

        pITD->uFrameListIndex = (UINT16)uCount;

        /* Move SITD pointer to the next */

        pITD = pITD->pVerticalNext;

        /* Increment frame index to point to next frame */

        uCount++;

        /* Get the last 10 bits of frame Index after roolback */

        uCount &= 0x3FF;

        }

    /* Update the frame list index */

    pHCDPipe->uLastIndex = (INT16)uCount;
    }/* End of usbEhcdLinkITDs() */

/*******************************************************************************
*
* usbEhcdLinkSITDs - link the SITDs
*
* This routine links the isochronous TDs created in the frame list.
*
* <pHCDData> - Pointer to the EHCD_DATA structure
* <pHCDPipe> - Pointer to the USB_EHCD_PIPE structure.
* <uFrameNumber> - Frame index for linking
* <pHead> - Pointer to the head of the SITDs
* <pTail> - Pointer to the tail of the SiTDs
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbEhcdLinkSITDs
    (
    pUSB_EHCD_DATA  pHCDData, /* Pointer to the EHCD_DATA structure */
    pUSB_EHCD_PIPE  pHCDPipe, /* Pointer to the USB_EHCD_PIPE structure */
    UINT32          uFrameNumber, /* Frame index for linking */
    pUSB_EHCD_SITD  pHead,    /* Pointer to the head SITD */
    pUSB_EHCD_SITD  pTail     /* Pointer to the tail SITD */
    )
    {
    /* To hold the index into the periodic frame list */

    UINT32          uCount = uFrameNumber;

    /* Index of the host controller */

    UINT32          uBusIndex;

    /* To hold the SITD of list */

    pUSB_EHCD_SITD  pSITD;

    /* Pointer to the frame list data array */

    pUSB_EHCD_LIST_DATA pFrameListData;

    /* Next trasnfer descriptor low 32 bit BUS address */

    UINT32          uNextLow32;

    /* Extract the index of the host controller */

    uBusIndex = pHCDData->uBusIndex;

    /* Get the first SITD of the list. */

    pSITD = pHead;

    while (pSITD != NULL)
        {
        /* Get pointer to the frame list data */

        pFrameListData = &pHCDData->FrameListData[uCount];

        /* Check if there is any isochronous TDs in this frame. */

        if (pFrameListData->pHeadPointer == NULL)
            {
            pFrameListData->pHeadPointer = pFrameListData->pTailPointer = pSITD;
            }
        else
            {
            /* Add the new ITD to the head of the list */

            if (pFrameListData->uHeadPointerType == USB_EHCD_TYPE_ITD)
                {
                pSITD->pNext = pFrameListData->pHeadPointer;

                ((pUSB_EHCD_ITD)(pSITD->pNext))->pPreviousTD = pSITD;

                ((pUSB_EHCD_ITD)(pSITD->pNext))->uPreviousTDType =
                    USB_EHCD_TYPE_SITD;

                pSITD->uNextTDType = USB_EHCD_TYPE_ITD;
                }
            else
                {
                pSITD->pNext = pFrameListData->pHeadPointer;

                ((pUSB_EHCD_SITD)(pSITD->pNext))->pPreviousTD = pSITD;

                ((pUSB_EHCD_SITD)(pSITD->pNext))->uPreviousTDType =
                    USB_EHCD_TYPE_SITD;

                pSITD->uNextTDType = USB_EHCD_TYPE_SITD;
                }

            /* Update the head pointer to point to the new ITD */

            pFrameListData->pHeadPointer = pSITD;
            }

        /* Set the head pointer type */

        pFrameListData->uHeadPointerType = USB_EHCD_TYPE_SITD;

        /* Copy the next pointers details from frame list */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              SITD,
                              pSITD->uNextLinkPointer,
                              USB_EHCD_GET_BITFIELD(uBusIndex,
                                                FRAME_LIST,
                                                *(pHCDData->pFrameList + uCount),
                                                POINTER),
                              NEXT_LINK_POINTER);

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              SITD,
                              pSITD->uNextLinkPointer,
                              USB_EHCD_GET_BITFIELD(uBusIndex,
                                                FRAME_LIST,
                                                *(pHCDData->pFrameList + uCount),
                                                POINTER_VALID_ENTRY),
                              NEXT_LINK_POINTER_T);

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              SITD,
                              pSITD->uNextLinkPointer,
                              USB_EHCD_GET_BITFIELD(uBusIndex,
                                                FRAME_LIST,
                                                *(pHCDData->pFrameList + uCount),
                                                POINTER_TYPE),
                              NEXT_LINK_POINTER_TYPE);

        /* Get the lower 32 bit of the BUS address */

        uNextLow32 = USB_EHCD_DESC_LO32(pSITD);

        /* Attach SITD to the frame list */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              FRAME_LIST,
                              *(pHCDData->pFrameList + uCount),
                              (uNextLow32 >> 5),
                              POINTER);

        /* Update the frame list pointer details */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              FRAME_LIST,
                              *(pHCDData->pFrameList + uCount),
                              0,
                              POINTER_VALID_ENTRY);

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              FRAME_LIST,
                              *(pHCDData->pFrameList + uCount),
                              USB_EHCD_TYPE_SITD,
                              POINTER_TYPE);

        /* Update the frame list index in the SITD */

        pSITD->uFrameListIndex = (UINT16)uCount;

        /* Move SITD pointer to the next */

        pSITD = pSITD->pVerticalNext;

        /* Increment frame index to point to next frame */

        uCount++;

        /* Get the last 10 bits of frame Index after rollback */

        uCount &= 0x3FF;

        }

    /* Update the frame list index */

    pHCDPipe->uLastIndex = (INT16)uCount;

    }/* End of usbEhcdLinkSITDs() */

/*******************************************************************************
*
* usbEhcdUnLinkSITD - unlink the SITDs
*
* This routine unlinks the isochronous TD created in the frame list.
*
* <pHCDPipe> - Pointer to the USB_EHCD_PIPE structure.
* <pSITD> - Pointer to the SITD
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbEhcdUnLinkSITD
    (
    pUSB_EHCD_DATA   pHCDData, /* Pointer to the EHCD_DATA structure */
    pUSB_EHCD_SITD   pSITD     /* Pointer to the tail SITD */
    )
    {
    /* To hold the index into the periodic frame list */

    UINT16 uFrameIndex = 0;

    /* Index of the host controller */

    UINT32 uBusIndex = 0;

    /* Pointer to the frame list data array */

    pUSB_EHCD_LIST_DATA pFrameListData;

    /* Get the frame list index */

    uFrameIndex = pSITD->uFrameListIndex;

    /* Extract the index of the host controller */

    uBusIndex = pHCDData->uBusIndex;

    /* Update the forward link */

    /* Check if this is head SITD */

    if (NULL == pSITD->pPreviousTD)
        {
        /* Get pointer to the frame list data */

        pFrameListData = &pHCDData->FrameListData[uFrameIndex];

        /*
         * If the next trasnfer descriptor is not NULL then, there are some more
         * ITDs or siTDs in this frame.
         */

        if (pSITD->pNext != NULL)
            {
            /*
             * Set the pointer to SITD->pNext
             * either uNextTDType is USB_EHCD_TYPE_ITD or not
             */

            pFrameListData->pHeadPointer = pSITD->pNext;
            }
        /* If there is no other iTD or sITD, update the head & tail to NULL */
        else
            {
            pFrameListData->pHeadPointer = NULL;
            pFrameListData->pTailPointer = NULL;
            }

        /* Update the head pointer type */

        pFrameListData->uHeadPointerType = pSITD->uNextTDType;

        /* Update the frame list */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              FRAME_LIST,
                              *(pHCDData->pFrameList + uFrameIndex),
                              USB_EHCD_GET_BITFIELD(uBusIndex,
                                                    SITD,
                                                    pSITD->uNextLinkPointer,
                                                    NEXT_LINK_POINTER),
                              POINTER);
        /* Update the frame list pointer details */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              FRAME_LIST,
                              *(pHCDData->pFrameList + uFrameIndex),
                              USB_EHCD_GET_BITFIELD(uBusIndex,
                                                    SITD,
                                                    pSITD->uNextLinkPointer,
                                                    NEXT_LINK_POINTER_T),
                              POINTER_VALID_ENTRY);

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              FRAME_LIST,
                              *(pHCDData->pFrameList + uFrameIndex),
                              USB_EHCD_GET_BITFIELD(uBusIndex,
                                                    SITD,
                                                    pSITD->uNextLinkPointer,
                                                    NEXT_LINK_POINTER_TYPE),
                              POINTER_TYPE);
        }

    else
        {
        if (pSITD->uPreviousTDType == USB_EHCD_TYPE_ITD)
            {
            pUSB_EHCD_ITD  pPrevITD;

            pPrevITD = (pUSB_EHCD_ITD)(pSITD->pPreviousTD);

            USB_EHCD_SET_BITFIELD(uBusIndex,
                                  ITD,
                                  pPrevITD->uNextLinkPointer,
                                  USB_EHCD_GET_BITFIELD(uBusIndex,
                                                        SITD,
                                                        pSITD->uNextLinkPointer,
                                                        NEXT_LINK_POINTER),
                                  NEXT_LINK_POINTER);

            USB_EHCD_SET_BITFIELD(uBusIndex,
                                  ITD,
                                  pPrevITD->uNextLinkPointer,
                                  USB_EHCD_GET_BITFIELD(uBusIndex,
                                                        SITD,
                                                        pSITD->uNextLinkPointer,
                                                        NEXT_LINK_POINTER_T),
                                  NEXT_LINK_POINTER_T);

            USB_EHCD_SET_BITFIELD(uBusIndex,
                                  ITD,
                                  pPrevITD->uNextLinkPointer,
                                  USB_EHCD_GET_BITFIELD(uBusIndex,
                                                        SITD,
                                                        pSITD->uNextLinkPointer,
                                                        NEXT_LINK_POINTER_TYPE),
                                  NEXT_LINK_POINTER_TYPE);

            pPrevITD->pNext = pSITD->pNext;

            pPrevITD->uNextTDType = pSITD->uNextTDType;
            }
        else
            {
            pUSB_EHCD_SITD  pPrevSITD;

            pPrevSITD = (pUSB_EHCD_SITD)(pSITD->pPreviousTD);

            USB_EHCD_SET_BITFIELD(uBusIndex,
                                  SITD,
                                  pPrevSITD->uNextLinkPointer,
                                  USB_EHCD_GET_BITFIELD(uBusIndex,
                                                        SITD,
                                                        pSITD->uNextLinkPointer,
                                                        NEXT_LINK_POINTER),
                                  NEXT_LINK_POINTER);

            USB_EHCD_SET_BITFIELD(uBusIndex,
                                  SITD,
                                  pPrevSITD->uNextLinkPointer,
                                  USB_EHCD_GET_BITFIELD(uBusIndex,
                                                        SITD,
                                                        pSITD->uNextLinkPointer,
                                                        NEXT_LINK_POINTER_T),
                                  NEXT_LINK_POINTER_T);

            USB_EHCD_SET_BITFIELD(uBusIndex,
                                  SITD,
                                  pPrevSITD->uNextLinkPointer,
                                  USB_EHCD_GET_BITFIELD(uBusIndex,
                                                        SITD,
                                                        pSITD->uNextLinkPointer,
                                                        NEXT_LINK_POINTER_TYPE),
                                  NEXT_LINK_POINTER_TYPE);

            pPrevSITD->pNext = pSITD->pNext;

            pPrevSITD->uNextTDType = pSITD->uNextTDType;
            }
        }

    /* Update the backward link */

    /* If this td is not last TD in this frame */

    if (NULL != pSITD->pNext)
        {
        if (pSITD->uNextTDType == USB_EHCD_TYPE_ITD)
            {
            pUSB_EHCD_ITD  pNextITD;
            pNextITD = (pUSB_EHCD_ITD)(pSITD->pNext);
            pNextITD->uPreviousTDType = pSITD->uPreviousTDType;
            pNextITD->pPreviousTD = pSITD->pPreviousTD;
            }
        else
            {
            pUSB_EHCD_SITD  pNextSITD;
            pNextSITD = (pUSB_EHCD_SITD)(pSITD->pNext);
            pNextSITD->uPreviousTDType = pSITD->uPreviousTDType;
            pNextSITD->pPreviousTD = pSITD->pPreviousTD;
            }
        }
    }/* End of usbEhcdUnLinkSITD() */

/*******************************************************************************
*
* usbEhcdUnLinkITD - unlink the ITDs
*
* This routine unlinks the isochronous TD created in the frame list.
*
* <pHCDPipe> - Pointer to the USB_EHCD_PIPE structure.
* <pITD> - Pointer to the ITD
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbEhcdUnLinkITD
    (
    pUSB_EHCD_DATA  pHCDData, /* Pointer to the EHCD_DATA structure */
    pUSB_EHCD_ITD   pITD      /* Pointer to the ITD */
    )
    {
    /* To hold the index into the periodic frame list */

    UINT16 uFrameIndex = 0;

    /* Index of the host controller */

    UINT32 uBusIndex = 0;

    /* Pointer to the frame list data array */

    pUSB_EHCD_LIST_DATA pFrameListData;

    /* Extract the index of the host controller */

    uBusIndex = pHCDData->uBusIndex;

    uFrameIndex = pITD->uFrameListIndex;

    /* Update the forward link in the list */

    /* Check for the head of ITD */

    if (NULL == pITD->pPreviousTD)
        {
        /* Get pointer to the frame list data */

        pFrameListData = &pHCDData->FrameListData[uFrameIndex];

        /*
         * If the next trasnfer descriptor is not NULL then, there are some more
         * ITDs or siTDs in this frame.
         */

        if (pITD->pNext != NULL)
            {
            /*
             * set the pointer to trasnfer descriptor->pNext
             * either uNextTDType is USB_EHCD_TYPE_ITD or not
             */

            pFrameListData->pHeadPointer = pITD->pNext;
            }
        /* If there is no other iTD or sITD, update the head & tail to NULL */
        else
            {
            pFrameListData->pHeadPointer = NULL;
            pFrameListData->pTailPointer = NULL;
            }

        /* Update the head pointer type */

        pFrameListData->uHeadPointerType = pITD->uNextTDType;


        /* Update the frame list */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              FRAME_LIST,
                              *(pHCDData->pFrameList + uFrameIndex),
                              USB_EHCD_GET_BITFIELD(uBusIndex,
                                                    ITD,
                                                    pITD->uNextLinkPointer,
                                                    NEXT_LINK_POINTER),
                              POINTER);

        /* Update the frame list pointer details */

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              FRAME_LIST,
                              *(pHCDData->pFrameList + uFrameIndex),
                              USB_EHCD_GET_BITFIELD(uBusIndex,
                                                    ITD,
                                                    pITD->uNextLinkPointer,
                                                    NEXT_LINK_POINTER_T),
                              POINTER_VALID_ENTRY);

        USB_EHCD_SET_BITFIELD(uBusIndex,
                              FRAME_LIST,
                              *(pHCDData->pFrameList + uFrameIndex),
                              USB_EHCD_GET_BITFIELD(uBusIndex,
                                                    ITD,
                                                    pITD->uNextLinkPointer,
                                                    NEXT_LINK_POINTER_TYPE),
                              POINTER_TYPE);
        }
    else
        {
        if(pITD->uPreviousTDType == USB_EHCD_TYPE_ITD)
            {
            pUSB_EHCD_ITD  pPrevITD;

            pPrevITD = (pUSB_EHCD_ITD)(pITD->pPreviousTD);

            USB_EHCD_SET_BITFIELD(uBusIndex,
                                  ITD,
                                  pPrevITD->uNextLinkPointer,
                                  USB_EHCD_GET_BITFIELD(uBusIndex,
                                                        ITD,
                                                        pITD->uNextLinkPointer,
                                                        NEXT_LINK_POINTER),
                                  NEXT_LINK_POINTER);

            USB_EHCD_SET_BITFIELD(uBusIndex,
                                  ITD,
                                  pPrevITD->uNextLinkPointer,
                                  USB_EHCD_GET_BITFIELD(uBusIndex,
                                                        ITD,
                                                        pITD->uNextLinkPointer,
                                                        NEXT_LINK_POINTER_T),
                                  NEXT_LINK_POINTER_T);

            USB_EHCD_SET_BITFIELD(uBusIndex,
                                  ITD,
                                  pPrevITD->uNextLinkPointer,
                                  USB_EHCD_GET_BITFIELD(uBusIndex,
                                                        ITD,
                                                        pITD->uNextLinkPointer,
                                                        NEXT_LINK_POINTER_TYPE),
                                  NEXT_LINK_POINTER_TYPE);

            pPrevITD->pNext = pITD->pNext;

            pPrevITD->uNextTDType = pITD->uNextTDType;
            }
        else
            {
            pUSB_EHCD_SITD  pPrevSITD;

            pPrevSITD = (pUSB_EHCD_SITD)(pITD->pPreviousTD);

            USB_EHCD_SET_BITFIELD(uBusIndex,
                                  SITD,
                                  pPrevSITD->uNextLinkPointer,
                                  USB_EHCD_GET_BITFIELD(uBusIndex,
                                                        ITD,
                                                        pITD->uNextLinkPointer,
                                                        NEXT_LINK_POINTER),
                                  NEXT_LINK_POINTER);

            USB_EHCD_SET_BITFIELD(uBusIndex,
                                  SITD,
                                  pPrevSITD->uNextLinkPointer,
                                  USB_EHCD_GET_BITFIELD(uBusIndex,
                                                        ITD,
                                                        pITD->uNextLinkPointer,
                                                        NEXT_LINK_POINTER_T),
                                  NEXT_LINK_POINTER_T);

            USB_EHCD_SET_BITFIELD(uBusIndex,
                                  SITD,
                                  pPrevSITD->uNextLinkPointer,
                                  USB_EHCD_GET_BITFIELD(uBusIndex,
                                                        ITD,
                                                        pITD->uNextLinkPointer,
                                                        NEXT_LINK_POINTER_TYPE),
                                  NEXT_LINK_POINTER_TYPE);

            pPrevSITD->pNext = pITD->pNext;

            pPrevSITD->uNextTDType = pITD->uNextTDType;
            }
        } /* End of updating forward link */

    /* Update the backward link */

    if (NULL != pITD->pNext)
        {
        if(pITD->uNextTDType == USB_EHCD_TYPE_ITD)
            {
            pUSB_EHCD_ITD  pNextITD;
            pNextITD = (pUSB_EHCD_ITD)(pITD->pNext);
            pNextITD->uPreviousTDType = pITD->uPreviousTDType;
            pNextITD->pPreviousTD = pITD->pPreviousTD;
            }
        else
            {
            pUSB_EHCD_SITD  pNextSITD;
            pNextSITD = (pUSB_EHCD_SITD)(pITD->pNext);
            pNextSITD->uPreviousTDType = pITD->uPreviousTDType;
            pNextSITD->pPreviousTD = pITD->pPreviousTD;
            }
        }
    }/* End of usbEhcdUnLinkITDs() */

/*******************************************************************************
*
* usbEhcdFormEmptySITD - form an empty SITD
*
* This routine creates an empty SITD and populates it.
*
* <pHCDData> - Pointer to the USB_EHCD_DATA structure.
* <pHCDPipe> - Pointer to the USB_EHCD_PIPE structure.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: TRUE if the bandwidth reserved is within the limits.
*          FALSE if the bandwidth is not within the limits.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_EHCD_SITD usbEhcdFormEmptySITD
    (
    pUSB_EHCD_DATA  pHCDData,/* Pointer to the USB_EHCD_DATA structure */
    pUSB_EHCD_PIPE  pHCDPipe /* Pointer to the USB_EHCD_PIPE structure */
    )
    {
    /* To hold the pointer to the isochronous TD data structure */

    pUSB_EHCD_SITD  pSITD;

    /* Get a free SITD from the free list */

    pSITD = usbEhcdGetFreeSITD(pHCDData, pHCDPipe);

    /* Init SITD to be in default state */

    if (pSITD != NULL)
        usbEhcdInitSITD(pHCDData, pHCDPipe, pSITD);

    /* Return the pointer to the isochronous TD */

    return (pSITD);

    }/* End of usbEhcdFormEmptySITD() */

/*******************************************************************************
*
* usbEhcdFillSITD - populate the buffer pointers of the SITD
*
* This routine is used to populate the buffer pointer fields of the SITD.
*
* <index>    - index of the host controller
* <pSITD>   - Pointer to the SITD.
* <pBuffer>  - Pointer to the start of the buffer
* <uSize> - Size of the data to be filled.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: Returns the number of bytes which were filled in SITD.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL BOOLEAN usbEhcdFillSITD
    (
    UINT8             index,    /* index of the host controller */
    pUSB_EHCD_SITD    pSITD,    /* Pointer to the SITD */
    bus_addr_t        pBuffer,  /* Pointer to the start of the buffer */
    bus_size_t        uTransferLength  /* Transfer length */
    )
    {
    /* To hold the current offset to fill the QTD */

    UINT32      uOffset;

    /* To hold the low 32 bit of a pointer page to fill the QTD */

    UINT32      uLow32;

    /* To hold the high 32 bit of a pointer page to fill the QTD */

    UINT32      uHigh32;

    /* To hold the aligned page of the buffer to fill the QTD */

    bus_addr_t  uPagePointer;

    /* Get the current offset value */

    uOffset = (UINT32)(pBuffer & 0xFFF);

    /* Get the page aligned page pointer of the buffer */

    uPagePointer = pBuffer - uOffset;

    /* Get the low 32 bit of the page pointer */

    uLow32 = USB_EHCD_BUS_ADDR_LO32 (uPagePointer);

    /* Get the high 32 bit of the page pointer */

    uHigh32 = USB_EHCD_BUS_ADDR_HI32 (uPagePointer);

    /* Fill SITD's buffer pointer */

    /* Set the low 32 bit */

    USB_EHCD_SET_BITFIELD(index,
                          SITD,
                          pSITD->uBufferPointerList[0],
                          (uLow32 >> 12),
                          BUFFERPOINTER);

    /* Set the high 32 bit */

    USB_EHCD_SET_BITFIELD(index,
                          SITD,
                          pSITD->uExtBufferPointerPageList[0],
                          (uHigh32),
                          EXT_BUFFERPOINTER);

    /* Fill SITD's current offset into the buffer pointer */

    USB_EHCD_SET_BITFIELD(index,
                          SITD,
                          pSITD->uBufferPointerList[0],
                          uOffset,
                          BUFFERPOINTER_CURRENT_OFFSET);

    /* Go to the next 4K page */

    uPagePointer += USB_EHCD_PAGE_SIZE;

    /* Get the low 32 bit of the page pointer */

    uLow32 = USB_EHCD_BUS_ADDR_LO32 (uPagePointer);

    /* Get the high 32 bit of the page pointer */

    uHigh32 = USB_EHCD_BUS_ADDR_HI32 (uPagePointer);

    /* Set the low 32 bit */

    USB_EHCD_SET_BITFIELD(index,
                          SITD,
                          pSITD->uBufferPointerList[1],
                          (uLow32 >> 12),
                          BUFFERPOINTER);

    /* Set the high 32 bit */

    USB_EHCD_SET_BITFIELD(index,
                          SITD,
                          pSITD->uExtBufferPointerPageList[1],
                          (uHigh32),
                          EXT_BUFFERPOINTER);


    /*
     * Fill the number of bytes to transfer
     * Totally USB_EHCD_SITD_MAX_TRANSFER_SIZE = 1023 bytes can be
     * transferred using 1 SITD.
     */

    pSITD->uBytesToTransfer = (UINT32)uTransferLength;

    USB_EHCD_SET_BITFIELD(index,
                          SITD,
                          pSITD->uTransferState,
                          pSITD->uBytesToTransfer,
                          TRANSFER_STATE_TOTAL_BYTES_TO_TRANSFER);

    /* Update tp and T_count  if it is an OUT transaction */

    if (USB_EHCD_GET_BITFIELD(index,
                              SITD,
                              pSITD->uEndPointCharacteristics,
                              ENDPOINT_CHARACTERISTICS_DIRECTION) == 0)
        {
        /* Update the transaction position */

        if (pSITD->uBytesToTransfer <= USB_EHCD_SITD_MAX_SPLIT_SIZE)
            {
            USB_EHCD_SET_BITFIELD(index,
                                  SITD,
                                  pSITD->uBufferPointerList[1],
                                  USB_EHCD_SITD_TP_ALL,
                                  BUFFERPOINTER_TP);
            }
        else
            {
            USB_EHCD_SET_BITFIELD(index,
                                  SITD,
                                  pSITD->uBufferPointerList[1],
                                  USB_EHCD_SITD_TP_BEGIN,
                                  BUFFERPOINTER_TP);
            }

        /* Update the total split count */

        USB_EHCD_SET_BITFIELD(index,
                              SITD,
                              pSITD->uBufferPointerList[1],
                              (((pSITD->uBytesToTransfer)
                                 /USB_EHCD_SITD_MAX_SPLIT_SIZE) + 1),
                              BUFFERPOINTER_TCOUNT);
        } /* End of if (pSITD->dword3.i_o == 0) */

    /*
     * Update the back pointer validity field as Non valid,
     * because initially it is NULL.
     */

    USB_EHCD_SET_BITFIELD(index,
                          SITD,
                          pSITD->uBackPointer,
                          USB_EHCD_INVALID_LINK,
                          BUFFERPOINTER_BACKPTR_T);

    /* Return the number of bytes being transfered by this SITD */

    return TRUE;
    }/* End of usbEhcdFillSITD() */

/*******************************************************************************
*
* usbEhcdGenerateSITDs - create TDs and populates them.
*
* This routine is used to create TDs for a data transfer and to populate the
* fields of the TDs.
*
* <pHCDData> - Pointer to the USB_EHCD_DATA structure
* <pHCDPipe> - Pointer to the USB_EHCD_PIPE structure
* <pRequest> - Pointer to the request info
* <ppDataHead> - Pointer holding the pointer to the
*               head of the list of SITDS.
* <ppDataTail> - Pointer holding the pointer to the
*               tail of the list of SITDS.
* <pTransferBuffer> - Data buffer
* <uPktCnt> - number of packets for this isoch transfer
* <pIsocPktDesc>  - pointer to the array of packet descriptor
*
* WARNING : No parameter validation is done as this routine is used internally
* by the EHCD. Assumption is that valid parameters are passed by the EHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: Returns TRUE if the SITDs are created successfully.
*          Returns FALSE if the SITDs are not created successfully.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOLEAN usbEhcdGenerateSITDs
    (
    pUSB_EHCD_DATA  pHCDData,   /* Pointer to the USB_EHCD_DATA structure */
    pUSB_EHCD_PIPE  pHCDPipe,   /* Pointer to the USB_EHCD_PIPE structure */
    pUSB_EHCD_REQUEST_INFO  pRequest, /* Pointer to the request */
    pUSB_EHCD_SITD *ppDataHead, /* Pointer to the head SITD */
    pUSB_EHCD_SITD *ppDataTail, /* Pointer to the tail SITD */
    bus_addr_t      pTransferBuffer, /* Transfer buffer */
    UINT32          uPktCnt,         /* Num of packets for this transfer */
    pUSBHST_ISO_PACKET_DESC pIsocPktDesc /* array of packet descriptor */
    )
    {
    /* Pointer to the current SITD */

    pUSB_EHCD_SITD pSITD;

    /* To hold the temporary pointer to the SITD */

    pUSB_EHCD_SITD pTempSITD = NULL;

    /* To hold the number of bytes transferred */

    UINT32 uPktTransfered = 0;

    /* To hold the HCD bus index */

    UINT32      index = pHCDData->uBusIndex;


    /* Check the validity of the parameters */

    if ((NULL == pHCDPipe) ||
        (NULL == ppDataHead) ||
        (NULL == ppDataTail) ||
        (0 == pTransferBuffer))
        {
        USB_EHCD_ERR("usbEhcdGenerateSITDs - Parameters are not valid\n",
            0, 0, 0, 0, 0, 0);

        return FALSE;
        }

    /* Create the 1st TD needed for the transfer */

    pSITD = *ppDataHead = usbEhcdFormEmptySITD(pHCDData, pHCDPipe);

    /* If unable to create the TD, return FALSE */

    if (NULL == pSITD)
        {
        USB_EHCD_ERR("usbEhcdGenerateSITDs - unable to create head TD\n",
            0, 0, 0, 0, 0, 0);

        return FALSE;
        }

    /* This loop gets executed till all the TDs are created */

    while (uPktTransfered < uPktCnt)
        {
        /* Populate the TD's buffer pointers */

        usbEhcdFillSITD((UINT8)index,
                        pSITD,
                        pTransferBuffer + pIsocPktDesc[uPktTransfered].uOffset,
                        pIsocPktDesc[uPktTransfered].uLength);

        uPktTransfered++;

        /* If the bytes transfered is less than the total transfer size */

        if (uPktTransfered < uPktCnt)
            {
            /* Create 1 more SiTD */

            pSITD->pVerticalNext =
                usbEhcdFormEmptySITD(pHCDData, pHCDPipe);

            /* Check if memory allocation is a failure */

            if (NULL == pSITD->pVerticalNext)
                {
                USB_EHCD_ERR("usbEhcdGenerateSITDs - unable to create more TD\n",
                    0, 0, 0, 0, 0, 0);

                /* Free up all the TDs created */

                for (pSITD = *ppDataHead; NULL != pSITD; )
                    {
                    pTempSITD = pSITD->pVerticalNext;

                    /* Return the ITD to the free SITD list */

                    usbEhcdAddToFreeSITDList(pHCDData, pHCDPipe, pSITD);

                    pSITD = pTempSITD;
                    }

                /* Return FALSE */

                return FALSE;
                }/* End of if() */

            /* Indicate that this is not the last trasnfer descriptor in the queue */

            USB_EHCD_SET_BITFIELD(index,
                                  SITD,
                                  pSITD->uNextLinkPointer,
                                  0,
                                  NEXT_LINK_POINTER_T);

            /* Update the next trasnfer descriptor as the current SITD trasnfer descriptor */

            pSITD = pSITD->pVerticalNext;

            /*
             * There are no other trasnfer descriptors in the queue
             * after the newly created trasnfer descriptor.
             */

            pSITD->pVerticalNext = NULL;

            }
            /* End of if(uBytesTransfered < uTransferLength) */
        }
        /* End of while (uBytesTransfered < uTransferLength) */

    /*
     * Mark the last SiTD's Interrupt on completion bit as 1
     * This will generate an interrupt on completion of the last SiTD transfer
     */

    USB_EHCD_SET_BITFIELD(index,
                           SITD,
                           pSITD->uTransferState,
                           1U,
                           TRANSFER_STATE_IOC);

    /* Update the tail pointer */

    *ppDataTail = pSITD;

    /* Return TRUE from the function */

    return TRUE;
    }/* End of function usbEhcdGenerateSITDs() */

/*******************************************************************************
*
* usbEhcdCalculateBusTime - calculate the bus time
*
* This routine is used to calculate the bus time required for the endpoint.
*
* <uSpeed> - Speed of the device.
* <uDirection> - Direction of the pipe.
* <uPipeType> - Type of the endpoint.
* <uDataByteCount>  - maximum byte count supported by the pipe.
*
* RETURNS: Calculated bus time in nanoseconds.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

INT32 usbEhcdCalculateBusTime
    (
    UINT32 uSpeed,     /* Speed of the device */
    UINT32 uDirection, /* Direction of the pipe */
    UINT32 uPipeType,  /* type of the pipe */
    UINT32 uDataByteCount /* Maximum byte count supported by the pipe */
    )
    {
    /* Calculate the bus time for a low speed pipe */

    if (USBHST_LOW_SPEED == uSpeed)
        {
        /* Calculate the bustime for an IN pipe */

        if (USB_EHCD_DIR_IN == uDirection)
            {
            return (INT32)(64060 + (2 * USB_EHCD_HUB_LS_SETUP) + (677 *
                    (4 + USB_EHCD_BIT_STUFF_TIME(uDataByteCount)))+
                    USB_EHCD_HOST_DELAY);
            }

        /* Calculate the bustime for an OUT pipe */

        else
            {
            return (INT32)(64107 + (2 * USB_EHCD_HUB_LS_SETUP) + (667 *
                    (4 + USB_EHCD_BIT_STUFF_TIME(uDataByteCount))) +
                    USB_EHCD_HOST_DELAY);
            }

        }
        /* End of if(Speed == USBHST_LOW_SPEED) */

    /* Calculate the bus time for a Full speed pipe */

    else if(USBHST_FULL_SPEED == uSpeed)
        {
        /* Calculate the bus time for a control, bulk or interrupt pipe */

        if( USBHST_ISOCHRONOUS_TRANSFER != uPipeType)
            {
            return(INT32)(9107 + (84 *
                   (4 +
                   USB_EHCD_BIT_STUFF_TIME(uDataByteCount))) +
                   USB_EHCD_HOST_DELAY);
            }
            /* Calculate the bus time for an isochronous pipe */

        else
            {
            /* Calculate the bus time for an IN endpoint */

            if  (USB_EHCD_DIR_IN == uDirection)
                {
                return (INT32)(7268 + (84 *
                        (4 +
                        USB_EHCD_BIT_STUFF_TIME(uDataByteCount))) +
                        USB_EHCD_HOST_DELAY);
                }
                /* Calculate the bus time for an OUT endpoint */

            else
                {
                return (INT32)(6265 + (84 *
                        (4 + USB_EHCD_BIT_STUFF_TIME(uDataByteCount))) +
                        USB_EHCD_HOST_DELAY);
                }
            }
            /* End of else */

        }
        /* End of else if(Speed == USBHST_FULL_SPEED) */

    /* Calculate the bus time for a high speed pipe */

    else if(USBHST_HIGH_SPEED == uSpeed)
        {
        /* Calculate the bus time for a control, bulk or interrupt pipe */

        if(USBHST_ISOCHRONOUS_TRANSFER != uPipeType)
            {
            return (INT32)((55 * 8 * 2)+(3 *
                    (4 + USB_EHCD_BIT_STUFF_TIME(uDataByteCount))) +
                    USB_EHCD_HOST_DELAY);
            }

        /* Calculate the bus time for an isochronous pipe */

        else
            {
            return (INT32)((38 * 8 * 2)+(3 *
                    (4 + USB_EHCD_BIT_STUFF_TIME(uDataByteCount))) +
                    USB_EHCD_HOST_DELAY);
            }
        }/* End of else if(Speed == USBHST_HIGH_SPEED) */

    return -1;
    }/* End of usbEhcdCalculateBusTime() */

/*******************************************************************************
*
* usbEhcdCheckBandwidth - check the bandwidth availability
*
* This routine is used to check whether bandwidth is available and returns the
* list which can hold the endpoint and also the mask value which indicates the
* microframes which can hold the endpoint transfers.
*
* <pHCDData> - Pointer to the EHCD_DATA structure.
* <uBandwidth > - Bandwidth to be reserved.
* <uSpeed> - Speed of the transfer.
* <pEndpointDesc>  - Pointer to the endpoint descriptor.
* <puListIndex> - Pointer which can hold the list index which holds the endpoint
*                 transfers
* <puMicroFrameMask> - Pointer to the mask value which can hold the microframes
*                      which can hold the endpoint transfers.
* RETURNS: TRUE if the bandwidth can be accomodated.
*          FALSE if the bandwidth cannot be accomodated.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOLEAN usbEhcdCheckBandwidth
    (
    pUSB_EHCD_DATA  pHCDData,  /* Pointer to the EHCD_DATA structure */
    ULONG       uBandwidth, /* Bandwidth to be reserved */
    UINT32      uSpeed,     /* Speed of the transfer */
    pUSBHST_ENDPOINT_DESCRIPTOR pEndpointDesc, /* Pointer to the endpoint desc */
    UINT32  *   puListIndex,/* Pointer to the list index */
    UINT32  *   puMicroFrameMask /* Pointer to the microframe mask value */
    )
    {
    /* To hold the index into the periodic frame list */

    UINT32 uCount = 0;

   /* To hold the no. of microframe to be reserved in a frame */

    UINT32 uUFrameCount = 0;

    /* To hold the polling interval */

    UINT32 uPollInterval = 0;

    /* To hold the bandwidth calculated */

    UINT32 uCalculatedBandwidth = 0;

    /* To hold the least occupied bandwidth */

    UINT32 uLeastBandwidth = 0;

    /* To hold the index of the frame which has the least occupied bandwidth */

    UINT32 uLeastBwIndex = 0;

    /* Check the validity of the parameters */

    if ((NULL == pHCDData) ||
        (0 == uBandwidth) ||
        (NULL == pEndpointDesc) ||
        (USBHST_HIGH_SPEED != uSpeed &&
         USBHST_FULL_SPEED != uSpeed &&
         USBHST_LOW_SPEED != uSpeed) ||
        (NULL == puListIndex) ||
        (NULL == puMicroFrameMask))
        {
        USB_EHCD_ERR("usbEhcdCheckBandwidth - Invalid parameter\n",
            0, 0, 0, 0, 0, 0);

        return FALSE;
        }

    /* Return error if the type is not a periodic endpoint type */

    if ((USBHST_ISOCHRONOUS_TRANSFER !=
         (pEndpointDesc->bmAttributes & USB_EHCD_ENDPOINT_TYPE_MASK)) &&
        (USBHST_INTERRUPT_TRANSFER !=
         (pEndpointDesc->bmAttributes & USB_EHCD_ENDPOINT_TYPE_MASK)))
        {
        USB_EHCD_ERR("usbEhcdCheckBandwidth - Not a periodic endpoint\n",
            0, 0, 0, 0, 0, 0);

        return FALSE;
        }
    /* Exclusively access the bandwidth resource */

    OS_WAIT_FOR_EVENT(pHCDData->BandwidthEventID,OS_WAIT_INFINITE);

    if (USBHST_ISOCHRONOUS_TRANSFER ==
        (pEndpointDesc->bmAttributes & USB_EHCD_ENDPOINT_TYPE_MASK))
        {
        UINT32 uMaxBandwidth = 0;

       /* Starting from the leaf nodes, check whether
        * supplied bandwidth can be accomodated here */

        for (uCount = 0; USB_EHCD_MAX_FRAMELIST_ENTIRES > uCount; uCount++)
            {

            uCalculatedBandwidth = 0;

            /* Calculate the bandwidth occupied in this frame */

            USB_EHCD_CALCULATE_FRAME_BANDWIDTH(pHCDData,
                                               uCount,
                                               uCalculatedBandwidth);

            /* Get the maximum bandwidth used in a frame */

            if (uCalculatedBandwidth > uMaxBandwidth)
                {
                uMaxBandwidth = uCalculatedBandwidth;
                }
            }
        /* End of for () */

        /* If there is no bandwidth available, return an error */

        if ((USB_EHCD_EIGHTY_PERCENT_BW * USB_EHCD_MAX_MICROFRAME_NUMBER) <
            (uMaxBandwidth + uBandwidth))
            {
            USB_EHCD_ERR("usbEhcdCheckBandwidth - Bandwidth not available\n",
                0, 0, 0, 0, 0, 0);

            /* Release the bandwidth exclusive access */

            OS_RELEASE_EVENT(pHCDData->BandwidthEventID);
            return FALSE;
            }

        }

    /* Initialize the least bandwidth to be the maximum bandwidth applicable */

    uLeastBandwidth = USB_EHCD_EIGHTY_PERCENT_BW * USB_EHCD_MAX_MICROFRAME_NUMBER;

    /* Starting from the leaf nodes, check whether bandwidth is available */

    for (uCount = 0; USB_EHCD_MAX_FRAMELIST_ENTIRES > uCount; uCount++)
        {

        /* initialize the calculated bandwidth to be 0 */

        uCalculatedBandwidth = 0;

        /* Calculate the bandwidth occupied in this frame */

        USB_EHCD_CALCULATE_FRAME_BANDWIDTH(pHCDData,
                                           uCount,
                                           uCalculatedBandwidth);

        /* If the bandwidth occupied is less than the previous least bandwidth,
         * this becomes the least bandwidth reserved.
         */

        if (uCalculatedBandwidth < uLeastBandwidth)
            {
            uLeastBandwidth = uCalculatedBandwidth;
            uLeastBwIndex = uCount;
            }
        }
    /* End of for () */

    /* If there is no bandwidth available, return an error */

    if ((USB_EHCD_EIGHTY_PERCENT_BW * USB_EHCD_MAX_MICROFRAME_NUMBER) <
        (uLeastBandwidth + uBandwidth))
        {
        USB_EHCD_ERR("usbEhcdCheckBandwidth - Bandwidth not available\n",
            0, 0, 0, 0, 0, 0);

        /* Release the bandwidth exclusive access */

        OS_RELEASE_EVENT(pHCDData->BandwidthEventID);

        return FALSE;
        }

    /* Get the polling interval of the endpoint */

    uPollInterval = pEndpointDesc->bInterval;

    /* Check if it is a low or full speed device endpoint */

    if (USBHST_HIGH_SPEED != uSpeed)
        {

        /* No. of microframe count will be max used for full speed */
        uUFrameCount = USB_EHCD_MAX_FULL_SPEED_UFRAME;

        /* Check if it is an interrupt endpoint */

        if (USBHST_INTERRUPT_TRANSFER ==
            (pEndpointDesc->bmAttributes & USB_EHCD_ENDPOINT_TYPE_MASK))
            {

            /* Array holding the start and complete split masks */

            UINT32 uSplitMask[USB_EHCD_MAX_INT_SPLIT_MASKS] =   {
                USB_EHCD_SPLIT_MASK_1,
                USB_EHCD_SPLIT_MASK_2,
                USB_EHCD_SPLIT_MASK_3,
                USB_EHCD_SPLIT_MASK_4
            };
            /* Identify the index of the leaf of the tree, which is to be used*/

            *puListIndex =
            pHCDData->FrameListData[uLeastBwIndex].uNextListIndex;

            /* 32 is the maximum polling interval which is supported.
             * So if the polling interval is greater than 32, the endpoint
             * will be polled every 32 ms.
             */
            if (USB_EHCD_MAX_USB11_INTERRUPT_POLL_INTERVAL > uPollInterval)
                {
                /* To hold the number of times the list is to be traversed */

                UINT8 uTraversalCount = 0;

                /* Calculate the traversal count */

                USB_EHCD_CALCULATE_TRAVERSAL_COUNT(uPollInterval,uTraversalCount);

                /* Get the Tree Index */

                while (0 < uTraversalCount)
                    {
                    *puListIndex =
                    pHCDData->TreeListData[*puListIndex].uNextListIndex;

                    /* Decrement the traversal count */
                    uTraversalCount--;
                    }/* End of while */

                }/* End of if (EHCD_MAX_USB11_INTERRUPT_POLL_INTERVAL > ...*/

            /* Initialize the least bandwidth occupied */

            uLeastBandwidth = USB_EHCD_EIGHTY_PERCENT_BW *
                              USB_EHCD_MAX_MICROFRAME_NUMBER;

            /* Retrieve the mask value which best fits this bandwidth */

            for (uCount = 0; USB_EHCD_MAX_INT_SPLIT_MASKS > uCount; uCount++)
                {
                /* Initialize the uCalculatedBandwidth = 0 */
                uCalculatedBandwidth = 0;

                /* Calculate the bandwidth with the node trasnfer descriptor added */

                USB_EHCD_CALCULATE_BW_MICROFRAMES(pHCDData,
                                                  uSplitMask[uCount],
                                                  uLeastBwIndex,
                                                  uCalculatedBandwidth);

                /* If this mask has the least bandwidth occupied, use this */

                if (uCalculatedBandwidth < uLeastBandwidth)
                    {
                    uLeastBandwidth = uCalculatedBandwidth;
                    *puMicroFrameMask = uSplitMask[uCount];
                    }
                }

            /* Check if the bandwidth cannot be accomodated */

            if (USB_EHCD_EIGHTY_PERCENT_BW * USB_EHCD_MAX_MICROFRAME_NUMBER
                < (uLeastBandwidth + (uBandwidth*uUFrameCount)))
                {
                USB_EHCD_ERR("usbEhcdCheckBandwidth - No bandwidth \n",
                    0, 0, 0, 0, 0, 0);

                /* Release the bandwidth exclusive access */

                OS_RELEASE_EVENT(pHCDData->BandwidthEventID);

                return FALSE;
                }

            }/* End of if (USBHST_INTERRUPT_TRANSFER... */

        /* The following is for isochronous transfer type */

        else
            {
            /* Array holding the split masks */
            UINT32 uSplitMask[USB_EHCD_MAX_MICROFRAME_NUMBER];

            /* Initialize the split mask as per direction */
            if (0 == (pEndpointDesc->bEndpointAddress & USB_EHCD_DIR_IN))
                {
                /* Array holding the start split masks for OUT transaction */

                uSplitMask[0] = 0x01; /* 00000001 */
                uSplitMask[1] = 0x02; /* 00000010 */
                uSplitMask[2] = 0x04; /* 00000100 */
                uSplitMask[3] = 0x08; /* 00001000 */
                uSplitMask[4] = 0x10; /* 00010000 */
                uSplitMask[5] = 0x20; /* 00100000 */
                uSplitMask[6] = 0x40; /* 01000000 */
                uSplitMask[7] = 0x80; /* 10000000 */

                }
            else
                {
                /* Array holding the start and complete split masks for IN transaction*/

                uSplitMask[0] = USB_EHCD_SPLIT_MASK_1;
                uSplitMask[1] = USB_EHCD_SPLIT_MASK_2;
                uSplitMask[2] = USB_EHCD_SPLIT_MASK_3;
                uSplitMask[3] = USB_EHCD_SPLIT_MASK_4;
                uSplitMask[4] = 0x00;
                uSplitMask[5] = 0x00;
                uSplitMask[6] = 0x00;
                uSplitMask[7] = 0x00;

                }

            /* Initialize the least bandwidth occupied */

            uLeastBandwidth = USB_EHCD_EIGHTY_PERCENT_BW
                            * USB_EHCD_MAX_MICROFRAME_NUMBER;

            /* Retrieve the mask value which best fits this bandwidth */

            for (uCount = 0;
                 ((USB_EHCD_MAX_MICROFRAME_NUMBER > uCount) &&
                  (0 != uSplitMask[uCount]));
                uCount++)
                {
                /* Initialize the uCalculatedBandwidth = 0 */
                uCalculatedBandwidth = 0;

                /* Calculate the bandwidth with the node trasnfer descriptor added */

                USB_EHCD_CALCULATE_BW_MICROFRAMES(pHCDData,
                                                  uSplitMask[uCount],
                                                  uLeastBwIndex,
                                                  uCalculatedBandwidth);

                /* If this mask has the least bandwidth occupied, use this */

                if (uCalculatedBandwidth < uLeastBandwidth)
                    {
                    uLeastBandwidth = uCalculatedBandwidth;
                    *puMicroFrameMask = uSplitMask[uCount];
                    }
                }

            /* Check if the bandwidth cannot be accomodated */

            if (USB_EHCD_EIGHTY_PERCENT_BW * USB_EHCD_MAX_MICROFRAME_NUMBER
                < (uLeastBandwidth + (uBandwidth*uUFrameCount)))
                {
                USB_EHCD_ERR("usbEhcdCheckBandwidth - No bandwidth \n",
                    0, 0, 0, 0, 0, 0);

                /* Release the bandwidth exclusive access */

                OS_RELEASE_EVENT(pHCDData->BandwidthEventID);

                return FALSE;
                }

            }/* End of else if (USBHST_ISOCHRONOUS_TRANSFER... */

        }/* End of if (USBHST_HIGH_SPEED != uSpeed) */

    /* The following is for high speed interrupt and isochronous transfers */

    else
        {
        /* Check if it is an interrupt endpoint */

        if (USBHST_INTERRUPT_TRANSFER ==
            (pEndpointDesc->bmAttributes & USB_EHCD_ENDPOINT_TYPE_MASK))
            {

            /* To hold the supported poll interval */

            UINT32 uSupportedPollInterval = 0;

            /* To hold the number of times the list is to be traversed */

            UINT8 uTraversalCount = 0;

            /* To hold the index into the microframe */

            UINT8 uCount = 0;

            /* Array to hold the possible mask values */

            UINT8 uUFrameMask[USB_EHCD_MAX_MICROFRAME_NUMBER] = {
                0x00,
                0x00,
                0x00,
                0x00,
                0x00,
                0x00,
                0x00,
                0x00
            };
            /* Check the value of polling interval */

            if (USB_EHCD_MAX_USB20_POLLING_INTERVAL < uPollInterval)
                {
                USB_EHCD_ERR("usbEhcdCheckBandwidth - Invalid polling interval\n",
                    0, 0, 0, 0, 0, 0);

                /* Release the bandwidth exclusive access */

                OS_RELEASE_EVENT(pHCDData->BandwidthEventID);

                return FALSE;
                }

            /* Identify the index of the leaf of the tree,
             * which is to be used
             */

            *puListIndex =
            pHCDData->FrameListData[uLeastBwIndex].uNextListIndex;

            /* Calculate the updated polling interval. This is in
             * units of microframes
             */

            uPollInterval = (UINT32)(1 << (uPollInterval - 1));

            /* Calculate the polling interval in terms of frames */

            uSupportedPollInterval =
            uPollInterval / USB_EHCD_MAX_MICROFRAME_NUMBER;
            if (0 != (uPollInterval % USB_EHCD_MAX_MICROFRAME_NUMBER))
                {
                uSupportedPollInterval += 1;
                }

            /* If the bandwidth is more than what is supported,
             * update the polling interval to the supported polling interval
             */

            if (USB_EHCD_MAX_USB11_INTERRUPT_POLL_INTERVAL <
                uSupportedPollInterval)
                {
                uSupportedPollInterval =
                USB_EHCD_MAX_USB11_INTERRUPT_POLL_INTERVAL;
                uPollInterval =
                uSupportedPollInterval * USB_EHCD_MAX_MICROFRAME_NUMBER;
                }

            /* The polling interval is less than
             * USB_EHCD_MAX_USB11_INTERRUPT_POLL_INTERVAL
             */
            else
                {
                /* Calculate the traversal count */

                USB_EHCD_CALCULATE_TRAVERSAL_COUNT(uSupportedPollInterval,
                                                   uTraversalCount);

                /* Get the Tree Index */

                while (0 < uTraversalCount)
                    {
                    *puListIndex =
                    pHCDData->TreeListData[*puListIndex].uNextListIndex;

                    /* Decrement the traversal count */

                    uTraversalCount--;
                    }/* End of while */
                }

            /* Update the mask value based on the polling interval */

            switch (uPollInterval % USB_EHCD_MAX_MICROFRAME_NUMBER)
                {
                /* The endpoint is to be polled one microframe in the frame */

                case 0:
                    {
                    uUFrameMask[0] = 0x01;  /* 00000001 */
                    uUFrameMask[1] = 0x02;  /* 00000010 */
                    uUFrameMask[2] = 0x04;  /* 00000100 */
                    uUFrameMask[3] = 0x08;  /* 00001000 */
                    uUFrameMask[4] = 0x10;  /* 00010000 */
                    uUFrameMask[5] = 0x20;  /* 00100000 */
                    uUFrameMask[6] = 0x40;  /* 01000000 */
                    uUFrameMask[7] = 0x80;  /* 10000000 */
                    break;
                    }
                    /* The endpoint is to be polled every microframe in the frame */

                case 1:
                    {
                    uUFrameMask[0] = 0xFF; /* 11111111 */
                    break;
                    }
                    /* The endpoint is to be polled every
                     * alternate microframe in the frame
                     */

                case 2:
                    {
                    uUFrameMask[0] = 0x55; /* 01010101 */
                    uUFrameMask[1] = 0xAA; /* 10101010 */
                    break;
                    }
                    /* The endpoint is to be polled every
                     * 4 microframes in the frame
                     */
                case 4:
                    {
                    uUFrameMask[0] = 0x11; /* 00010001 */
                    uUFrameMask[1] = 0x22; /* 00100010 */
                    uUFrameMask[0] = 0x44; /* 01000100 */
                    uUFrameMask[1] = 0x88; /* 10001000 */
                        break;
                    }
                default:
                    {
                    /* Any other combination is not possible as the polling
                     * interval is always equal to 2 ^ (x -1)
                     */

                    OS_ASSERT(FALSE);
                    break;
                    }
                }/* End of switch () */

            /* Initialize the least bandwidth occupied */

            uLeastBandwidth =
            USB_EHCD_EIGHTY_PERCENT_BW * USB_EHCD_MAX_MICROFRAME_NUMBER;

            /* Retrieve the mask value which best fits this bandwidth */

            for (uCount = 0;
                (USB_EHCD_MAX_MICROFRAME_NUMBER > uCount) &&
                (0 != uUFrameMask[uCount]);
                uCount++)
                {
                /* initialize the calculated bandwidth to be 0 */

                uCalculatedBandwidth = 0;

                /* Calculate the bandwidth with the node trasnfer descriptor added */

                USB_EHCD_CALCULATE_BW_MICROFRAMES(pHCDData,
                                                  uUFrameMask[uCount],
                                                  uLeastBwIndex,
                                                  uCalculatedBandwidth);

                /* If this mask has the least bandwidth occupied, use this */

                if (uCalculatedBandwidth < uLeastBandwidth)
                    {
                    uLeastBandwidth = uCalculatedBandwidth;
                    *puMicroFrameMask = uUFrameMask[uCount];
                    }
                }

            /* Check if the bandwidth cannot be accomodated */

            if (USB_EHCD_EIGHTY_PERCENT_BW * USB_EHCD_MAX_MICROFRAME_NUMBER
                < uLeastBandwidth)
                {
                USB_EHCD_ERR("usbEhcdCheckBandwidth - No bandwidth \n",
                    0, 0, 0, 0, 0, 0);

                /* Release the bandwidth exclusive access */

                OS_RELEASE_EVENT(pHCDData->BandwidthEventID);

                return FALSE;
                }
            }
        /* The following is for the isochronous transfers */

        else
            {
            /* To hold the supported poll interval */

            UINT32 uSupportedPollInterval = 0;

            /* To hold the index into the microframe */

            UINT8 uCount = 0;

            /* Array to hold the possible mask values */

            UINT8 uUFrameMask[USB_EHCD_MAX_MICROFRAME_NUMBER] = {
                0x00,
                0x00,
                0x00,
                0x00,
                0x00,
                0x00,
                0x00,
                0x00
            };

            /* If the poll interval is x, the actual polling interval is
             * 2 ^ (x - 1).
             * Calculate the updated polling interval.
             */

            uPollInterval = (UINT32)(1 << (uPollInterval - 1));

            /* Calculate the polling interval in units of frames */

            uSupportedPollInterval =
            uPollInterval / USB_EHCD_MAX_MICROFRAME_NUMBER;
            if (0 != (uPollInterval % USB_EHCD_MAX_MICROFRAME_NUMBER))
                {
                uSupportedPollInterval += 1;
                }

            /* Check if the polling interval is within the limits
             * and update the polling interval.
             */
            if (USB_EHCD_MAX_FRAMELIST_ENTIRES < uSupportedPollInterval)
                {
                uSupportedPollInterval = USB_EHCD_MAX_FRAMELIST_ENTIRES;
                uPollInterval =
                uSupportedPollInterval * USB_EHCD_MAX_MICROFRAME_NUMBER;
                }

            /* Update the mask value which contains the microframes in which
             * the transfers should happen based on the polling interval.
             */
            switch (uPollInterval % USB_EHCD_MAX_MICROFRAME_NUMBER)
                {
                /* The endpoint is to be polled one microframe in the frame */
                case 0:
                    {
                    uUFrameMask[0] = 0x01;  /* 00000001 */
                    uUFrameMask[1] = 0x02;  /* 00000010 */
                    uUFrameMask[2] = 0x04;  /* 00000100 */
                    uUFrameMask[3] = 0x08;  /* 00001000 */
                    uUFrameMask[4] = 0x10;  /* 00010000 */
                    uUFrameMask[5] = 0x20;  /* 00100000 */
                    uUFrameMask[6] = 0x40;  /* 01000000 */
                    uUFrameMask[7] = 0x80;  /* 10000000 */
                    break;
                    }
                    /* The endpoint is to be polled every microframe in the frame */
                case 1:
                    {
                    uUFrameMask[0] = 0xFF; /* 11111111 */
                    break;
                    }
                    /* The endpoint is to be polled every
                     * alternate microframe in the frame
                     */
                case 2:
                    {
                    uUFrameMask[0] = 0x55; /* 01010101 */
                    uUFrameMask[1] = 0xAA; /* 10101010 */
                    break;
                    }
                    /* The endpoint is to be polled every
                     * 4 microframes in the frame
                     */
                case 4:
                    {
                    uUFrameMask[0] = 0x11; /* 00010001 */
                    uUFrameMask[1] = 0x22; /* 00100010 */
                    uUFrameMask[0] = 0x44; /* 01000100 */
                    uUFrameMask[1] = 0x88; /* 10001000 */
                    break;
                    }
                default:
                    {
                    /* Any other combination is not possible as the polling
                     * interval is always equal to 2 ^ (x -1)
                     */
                    OS_ASSERT(FALSE);
                    }
                }/* End of switch () */

            /* Initialize the least bandwidth occupied */

            uLeastBandwidth =
            USB_EHCD_EIGHTY_PERCENT_BW * USB_EHCD_MAX_MICROFRAME_NUMBER;

            /* Retrieve the mask value which best fits this bandwidth */

            for (uCount = 0;
                (USB_EHCD_MAX_MICROFRAME_NUMBER > uCount) &&
                (0 != uUFrameMask[uCount]);
                uCount++)
                {
                /* initialize the calculated bandwidth to be 0 */

                uCalculatedBandwidth = 0;

                /* Calculate the bandwidth with the node trasnfer descriptor added */

                USB_EHCD_CALCULATE_BW_MICROFRAMES(pHCDData,
                                                  uUFrameMask[uCount],
                                                  uLeastBwIndex,
                                                  uCalculatedBandwidth);

                /* If this mask has the least bandwidth occupied, use this */

                if (uCalculatedBandwidth < uLeastBandwidth)
                    {
                    uLeastBandwidth = uCalculatedBandwidth;
                    *puMicroFrameMask = uUFrameMask[uCount];
                    }
                }

            /* Check if the bandwidth cannot be accomodated */

            if (USB_EHCD_EIGHTY_PERCENT_BW * USB_EHCD_MAX_MICROFRAME_NUMBER
                < uLeastBandwidth + (uBandwidth*uUFrameCount))
                {
                USB_EHCD_ERR("usbEhcdCheckBandwidth - No bandwidth \n",
                    0, 0, 0, 0, 0, 0);

                /* Release the bandwidth exclusive access */

                OS_RELEASE_EVENT(pHCDData->BandwidthEventID);

                return FALSE;
                }

                *puListIndex = uLeastBwIndex;
                }
            }

    /* Release the bandwidth exclusive access */

    OS_RELEASE_EVENT(pHCDData->BandwidthEventID);

    return TRUE;
    }/* End of usbEhcdCheckBandwidth() */

/*******************************************************************************
*
* usbEhcdUpdateBandwidth - update the bandwidth occupied in all the frames
*
* This routine is used to update the bandwidth occupied by the frames.
*
* <pHCDData> - Pointer to the EHCD_DATA structure.
*
* RETURNS: TRUE if the bandwidth reserved is within the limits.
*          FALSE if the bandwidth is not within the limits.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOLEAN usbEhcdUpdateBandwidth
    (
    pUSB_EHCD_DATA pHCDData  /* Pointer to the EHCD_DATA structure */
    )
    {
    /* To hold the bandwidth occupied */

    UINT32 uBandwidth = 0;

    /* To hold the index into the microframe */

    UINT8 uUFrameIndex = 0;

    /* To hold the index into the list */

    UINT8 uListIndex = 0;

    /* To hold the index into the frame */

    UINT16  uFrameIndex = 0;

    /* Check the validity of the parameters */

    if (NULL == pHCDData)
        {
        USB_EHCD_ERR("usbEhcdUpdateBandwidth - parameters not valid\n",
            0, 0, 0, 0, 0, 0);

        return FALSE;
        }

    /* Exclusively access the bandwidth resource */

    OS_WAIT_FOR_EVENT(pHCDData->BandwidthEventID,OS_WAIT_INFINITE);


    /* This loop checks the bandwidth in each frame */

    for (;USB_EHCD_MAX_FRAMELIST_ENTIRES > uFrameIndex; uFrameIndex++)
        {
        /*
         * This loop will check if the bandwidth exceeds the limits and
         * updates the total bandwidth for all the microframes in a frame
         */

        for (uUFrameIndex = 0;
             USB_EHCD_MAX_MICROFRAME_NUMBER > uUFrameIndex;
             uUFrameIndex++)
            {

            /* Copy the bandwidth occupied
             * in the frame by isochronous endpoints
             */
            uBandwidth = pHCDData->FrameListData[uFrameIndex].
                                                    uBandwidth[uUFrameIndex];

            /* Hold the index of the list into the periodic tree */

            uListIndex = (UINT8)pHCDData->FrameListData[uFrameIndex].uNextListIndex;

            /* This loop checks whether the bandwidth can be accomodated
             * and updates the bandwidth in the microframe
             */

            for (; (USB_EHCD_MAX_TREE_NODES > uListIndex);
                   uListIndex =
                       (UINT8)pHCDData->TreeListData[uListIndex].uNextListIndex)
                {
                uBandwidth +=
                    pHCDData->TreeListData[uListIndex].uBandwidth[uUFrameIndex];
                }

            /*
             * Check if the bandwidth exceeds the maximum limits
             * set for USB 2.0. ie if it exceeds 80%. If it does not exceed
             * update the total bandwidth occupied in the frame
             */

            if ((USB_EHCD_EIGHTY_PERCENT_BW * USB_EHCD_MAX_MICROFRAME_NUMBER) >
                 uBandwidth)
                {
                pHCDData->FrameBandwidth[uFrameIndex][uUFrameIndex] = uBandwidth;
                }
            else
                {
                USB_EHCD_ERR("Bandwidth not available\n",
                    0, 0, 0, 0, 0, 0);

                /* Release the bandwidth exclusive access */

                OS_RELEASE_EVENT(pHCDData->BandwidthEventID);

                return FALSE;
                }
            }
        }

    /* Release the bandwidth exclusive access */

    OS_RELEASE_EVENT(pHCDData->BandwidthEventID);

    return TRUE;
    }/* End of usbEhcdUpdateBandwidth() */

/*******************************************************************************
*
* usbEhcdCopyRHInterruptData - copy the interrupt status data.
*
* This function is used to copy the interrupt status data
* to the request buffer if a request is pending or to
* the interrupt status buffer if a request is not pending.
*
* <pHCDData> - Pointer to the EHCD_DATA structure.
* <uStatusChange > - Status change data.
*
* RETURNS: TRUE if the data is successfully copied.
*          FALSE if the data is not successfully copied.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOLEAN usbEhcdCopyRHInterruptData
    (
    pUSB_EHCD_DATA  pHCDData, /* Pointer to the EHCD_DATA structure */
    UINT32      uStatusChange  /* Status change data */
    )
    {
    /* To hold the pointer to the request information */

    pUSB_EHCD_REQUEST_INFO pRequestInfo = NULL;

    /* Interrupt data */

    UINT32 uInterruptData = 0;

    UINT32 uBusIndex = 0; /* index of the host controller */

    /* Check the validity of the parameters */

    if (NULL == pHCDData ||
        0 == uStatusChange)
        {
        USB_EHCD_ERR("usbEhcdCopyRHInterruptData - parameters not valid\n",
            0, 0, 0, 0, 0, 0);

        return FALSE;
        }

    /* Extract the index of the host controller */

    uBusIndex = pHCDData->uBusIndex;

    /* If there is any request which is pending for the root hub
     * interrupt endpoint, populate the URB.
     * This has to be done by exclusively accessing the request list.
     */

    OS_WAIT_FOR_EVENT(pHCDData->RequestSynchEventID, OS_WAIT_INFINITE);

    /* This condition will pass when devices are kept connected
     * when the system is booted
     */

    if (NULL == pHCDData->RHData.pInterruptPipe)
        {
        /* Copy the status information which is stored already */

        OS_MEMCPY(&uInterruptData,
                  pHCDData->RHData.pHubInterruptData,
                  pHCDData->RHData.uSizeInterruptData);

        /* Swap the data to CPU endian format */

        uInterruptData = USB_EHCD_SWAP_USB_DATA(uBusIndex,
                                            uInterruptData);

        uInterruptData |= uStatusChange;

        /* Swap it back to LE */

        uInterruptData = USB_EHCD_SWAP_USB_DATA(uBusIndex, uInterruptData);

        /* Copy the data to the interrupt data buffer */

        OS_MEMCPY(pHCDData->RHData.pHubInterruptData,
                   &uInterruptData,
                   pHCDData->RHData.uSizeInterruptData);

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDData->RequestSynchEventID);

        return TRUE;
        }

    /* Check if this is the head of the list */

    if (NULL != pHCDData->RHData.pInterruptPipe->pRequestQueueHead)
        {
        USB_EHCD_DBG("usbEhcdCopyRHInterruptData - Request is pending \n",
            0, 0, 0, 0, 0, 0);

        /* Copy the request information which needs to be populated */

        pRequestInfo = pHCDData->RHData.pInterruptPipe->pRequestQueueHead;

        /* If both the head and tail are the same, reset the head and tail */

        if (pRequestInfo == pHCDData->RHData.pInterruptPipe->pRequestQueueTail)
            {
            pHCDData->RHData.pInterruptPipe->pRequestQueueHead = NULL;
            pHCDData->RHData.pInterruptPipe->pRequestQueueTail = NULL;
            }
        else
            {
            /* Release this trasnfer descriptor from the list */

            pHCDData->RHData.pInterruptPipe->pRequestQueueHead =
                                pRequestInfo->pNext;
            }
        }

    /*
     * If there is no request pending, copy the data
     * to the interrupt data buffer
     */

    if (NULL == pRequestInfo)
        {
        USB_EHCD_DBG("usbEhcdCopyRHInterruptData - No request pending \n",
            0, 0, 0, 0, 0, 0);

        /* Copy the status information which is stored already */

        OS_MEMCPY(&uInterruptData,
                  pHCDData->RHData.pHubInterruptData,
                  pHCDData->RHData.uSizeInterruptData);

        /* Swap the data to CPU endian format */

        uInterruptData = USB_EHCD_SWAP_USB_DATA(uBusIndex,uInterruptData);

        uInterruptData |= uStatusChange;

        /* Swap it back to LE */

        uInterruptData = USB_EHCD_SWAP_USB_DATA(uBusIndex,uInterruptData);

        /* Copy the data to the interrupt data buffer */

        OS_MEMCPY(pHCDData->RHData.pHubInterruptData,
                   &uInterruptData,
                   pHCDData->RHData.uSizeInterruptData);

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDData->RequestSynchEventID);
        }
    else
        {
        USB_EHCD_DBG("usbEhcdCopyRHInterruptData - Calling callback :"\
            "uStatusChange = 0x%X\n", uStatusChange, 0, 0, 0, 0, 0);

        OS_RELEASE_EVENT(pHCDData->RequestSynchEventID);

        if (NULL == pRequestInfo->pUrb ||
            NULL == pRequestInfo->pUrb->pTransferBuffer)
            return FALSE;

        /* Swap the data to LE */

        uStatusChange = USB_EHCD_SWAP_USB_DATA(uBusIndex, uStatusChange);

        /* Populate the data buffer */

        OS_MEMCPY(pRequestInfo->pUrb->pTransferBuffer,
                  &uStatusChange,
                  pHCDData->RHData.uSizeInterruptData);

        /* Update the status */

        pRequestInfo->pUrb->nStatus = USBHST_SUCCESS;

        /* Update the length */

        pRequestInfo->pUrb->uTransferLength =
                        pHCDData->RHData.uSizeInterruptData;

        /*
         * If a callback function is registered, call the callback
         * function.
         */

        if (NULL != pRequestInfo->pUrb->pfCallback)
            {
            pRequestInfo->pUrb->pfCallback(pRequestInfo->pUrb);
            }

        OS_FREE (pRequestInfo);
        }/* End of else */

    return TRUE;
    }/* End of usbEhcdCopyRHInterruptData () */

/*******************************************************************************
*
* usbEhcdAsynchScheduleEnable - enable the Asynchronous Schedule of EHCI
*
* This routine is used to enable the Asynchronous Schedule of EHCI.
* 
* NOTE: The caller should make sure the parameter valid.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbEhcdAsynchScheduleEnable
    (
    pUSB_EHCD_DATA pHCDData
    )
    {
    UINT32 uRetryTimes = 0;

    /* Do not check the parameter since the caller done that */
    
    /* Check the cmd and sts bit before try the set this bit */
    
    if ((USB_EHCD_GET_FIELD(pHCDData,                                   
                            USBCMD,                                     
                            ASYNCH_SCHEDULE_ENABLE) == 0) ||            
        (USB_EHCD_GET_FIELD(pHCDData,                                  
                            USBSTS,                                    
                            ASYCHRONOUS_SCHEDULE_STATUS) == 0))         
        {  
        /* Enable the Asynchronous Schedule */           
        
        USB_EHCD_SET_BIT(pHCDData,                                         
                     USBCMD,                                               
                     ASYNCH_SCHEDULE_ENABLE); 
        
        /*
         * If only the asynch schedule enable bit and status bit are
         * set, the schedule can be termed to be enabled.
         * Wait till both of the bits are set or the max retry times.
         * Do not wait to avoid dead loop.
         */
         
        while ((USB_EHCD_GET_FIELD(pHCDData,                                   
                                   USBCMD,                                     
                                   ASYNCH_SCHEDULE_ENABLE) == 0) ||            
               (USB_EHCD_GET_FIELD(pHCDData,                                  
                                   USBSTS,                                    
                                   ASYCHRONOUS_SCHEDULE_STATUS) == 0))
            {        
            OS_DELAY_MS(1);                                                    
            
            if (USB_EHCD_CONFIG_ASYNCH_SCHEDULE_MAX_RETRY == uRetryTimes ++)   
                break;                                                       
            }
        }  

    if (uRetryTimes >= USB_EHCD_CONFIG_ASYNCH_SCHEDULE_MAX_RETRY)
        {
        USB_EHCD_VDBG("usbEhcdAsynchScheduleEnable - time out to set " \
                      "ASYNCH_SCHEDULE_ENABLE\n", 0, 0, 0, 0, 0, 0);

        return ERROR;
        }
    
    return OK;
    }

/*******************************************************************************
*
* usbEhcdAsynchScheduleDisable - disable the Asynchronous Schedule of EHCI
*
* This routine is used to disable the Asynchronous Schedule of EHCI.
* 
* NOTE: The caller should make sure the parameter valid.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbEhcdAsynchScheduleDisable
    (
    pUSB_EHCD_DATA pHCDData
    )
    {
    UINT32 uRetryTimes = 0;
    
    /* Check the cmd and sts bit before try the clear this bit */
    
    if ((USB_EHCD_GET_FIELD(pHCDData,                                   
                            USBCMD,                                     
                            ASYNCH_SCHEDULE_ENABLE) != 0) ||            
         (USB_EHCD_GET_FIELD(pHCDData,                                  
                            USBSTS,                                     
                            ASYCHRONOUS_SCHEDULE_STATUS) != 0))         
        {                                                                  
        /* Disable the Asynchronous Schedule */                   
        
        USB_EHCD_CLR_BIT(pHCDData,                                         
                     USBCMD,                                               
                     ASYNCH_SCHEDULE_ENABLE);    
        
        /*
         * If only the asynch schedule enable bit and status bit are
         * cleared, the schedule can be termed to be disabled.
         * Wait till both of the bits are cleared or the max retry times.
         * Do not wait to avoid dead loop.
         */
         
        while ((USB_EHCD_GET_FIELD(pHCDData,                                   
                                   USBCMD,                                     
                                   ASYNCH_SCHEDULE_ENABLE) != 0) ||            
                (USB_EHCD_GET_FIELD(pHCDData,                                  
                                   USBSTS,                                     
                                   ASYCHRONOUS_SCHEDULE_STATUS) != 0))         
            {
            OS_DELAY_MS(1);                                                    
                                                                               
            if (USB_EHCD_CONFIG_ASYNCH_SCHEDULE_MAX_RETRY == uRetryTimes ++)   
                break;                         
            }
        }  

    if (uRetryTimes >= USB_EHCD_CONFIG_ASYNCH_SCHEDULE_MAX_RETRY)
        {
        USB_EHCD_VDBG("usbEhcdAsynchScheduleDisable - time out to clear " \
                      "ASYNCH_SCHEDULE_ENABLE\n", 0, 0, 0, 0, 0, 0);
        
        return ERROR;
        }
    
    return OK;
    }

/* End of file */

