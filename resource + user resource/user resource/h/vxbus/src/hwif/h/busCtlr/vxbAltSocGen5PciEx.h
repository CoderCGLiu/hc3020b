/* vxbAltSocGen5PciEx.h - PCI Express driver hardware defintions */

/*
 * Copyright (c) 2013,2015 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
11may15,m_w  updated function for parsing TLP completion. (VXW6-84465)
09oct13,swu  created
*/

#ifndef __INCvxbAltSocGen5PciExh
#define __INCvxbAltSocGen5PciExh

#ifdef __cplusplus
extern "C" {
#endif

#define PCI_EX_DRIVER_NAME    "pciEx"

/* Avalon-MM-to-PCI Express Address Translation Table */

#define PCI_EX_A2P_ADDR_MAP_LO(n)       (0x1000 + (n) * 8)
#define PCI_EX_A2P_ADDR_MAP_HI(n)       (0x1004 + (n) * 8)

/* Root Port TLP Data Registers */

#define PCI_EX_TX_REG0        0x2000    /* Lower 32 bits of the TX TLP */
#define PCI_EX_TX_REG1        0x2004    /* Upper 32 bits of the TX TLP */
#define PCI_EX_TX_CNTRL       0x2008
#define PCI_EX_RXCPL_STATUS   0x2010
#define PCI_EX_RXCPL_REG0     0x2014    /* Lower 32 bits of a Completion TLP */
#define PCI_EX_RXCPL_REG1     0x2018    /* Upper 32 bits of a Completion TLP */

#define PCI_EX_TX_CNTRL_EOP   0x2       /* End of Packet */
#define PCI_EX_TX_CNTRL_SOP   0x1       /* Start of Packet */
#define PCI_EX_RXCPL_EOP      0x2       /* End of Packet */
#define PCI_EX_RXCPL_SOP      0x1       /* Start of Packet */

/* Avalon-MM Interrupt Status Registers for Root Ports */

#define PCI_EX_P2A_INT_STATUS    0x3060        
#define PCI_EX_P2A_INT_STS_ALL   0x0F   /* RPRX_CPL_RECEIVED and INT[A:D] */

/* INT-X Interrupt Enable Register for Root Ports */

#define PCI_EX_P2A_INT_ENABLE    0x3070
#define PCI_EX_P2A_INT_ENA_ALL   0x0F

/* TLP Command */

/* TLP completion MAX read loop counter */

#define TLP_PAYLOAD_SIZE        0x01

/* TLP_PAYLOAD_SIZE + 1(SOP) + 2(header) + 3(detection loop) */

#define TLP_CPL_READ_LOOP       (TLP_PAYLOAD_SIZE+1+2+3) 

/* TLP Configuration Request field */

#define TLP_FMTTYPE_IORD        0x02    /* IORd */
#define TLP_FMTTYPE_IOWR        0x42    /* IOWr */
#define TLP_FMTTYPE_CFGRD_TYPE0 0x04    /* CfgRd0 */
#define TLP_FMTTYPE_CFGWR_TYPE0 0x44    /* CfgWr0 */
#define TLP_FMTTYPE_CFGRD_TYPE1 0x05    /* CfgRd1 */
#define TLP_FMTTYPE_CFGWR_TYPE1 0x45    /* CfgWr1 */

/* TLP format */

#define TLP_FMT_3H_NO_DATA      0x0     /* 3 DW header, no data */
#define TLP_FMT_4H_NO_DATA      0x1     /* 4 DW header, no data */
#define TLP_FMT_3H_WITH_DATA    0x2     /* 3 DW header, with data */
#define TLP_FMT_4H_WITH_DATA    0x3     /* 4 DW header, with data */
#define TLP_FMT_PREFIX          0x4     /* TLP prefix */

/* TLP type */

#define TLP_TYPE_CPL            0xA     /* Completion packet */

#define TLP_TYPE(header0)       (((header0) >> 24) & 0x1f)  /* TLP type */
#define TLP_FMT(header0)        (((header0) >> 29) & 7)   /* TLP format */
#define TLP_CPL_STATUS(header1) (((header1) & 0x0000E000) >> 13) /* status code */

#define TLP_FMTTYPE_SHIFT       24
#define TLP_PLAYLOAD(n)         (n)
#define TLP_CFG_REQID_SHIFT     16
#define TLP_CFG_TAG_SHIFT       8
#define TLP_CFG_FIRST_DWBE      0x0f
#define TLP_CFG_BUSID_SHIFT     24
#define TLP_CFG_FN_SHIFT        16

/* TLP completion status code */

#define TLP_CSC_SC              0x0 /* successful completion */
#define TLP_CSC_UR              0x1 /* unsupported request */
#define TLP_CSC_CRS             0x2 /* config request retry status */
#define TLP_CSC_CA              0x4 /* completer abort */

/* TLP Configuration Request DWord 0/1/2 */

#define TLP_CFG_0(fmttype)      ((fmttype << TLP_FMTTYPE_SHIFT) | \
    TLP_PLAYLOAD(1))

#define TLP_CFG_1(reqid, tag)   ((reqid << TLP_CFG_REQID_SHIFT) | \
    (tag << TLP_CFG_TAG_SHIFT) | TLP_CFG_FIRST_DWBE)

#define TLP_CFG_2(busid, fn, offset)    ((busid << TLP_CFG_BUSID_SHIFT) | \
    (fn << TLP_CFG_FN_SHIFT) | offset)

#define PCI_EX_TLP_CFG_TAG_RD   0x1D
#define PCI_EX_TLP_CFG_TAG_WR   0x10

/* Header type 1 (PCI-to-PCI bridges) */

#define PCI_PRIMARY_BUS         0x18    /* Primary bus number */
#define PCI_SECONDARY_BUS       0x19    /* Secondary bus number */
#define PCI_SUBORDINATE_BUS     0x1a    /* Highest bus number behind the bridge */

/* Latency timer for secondary interface */

#define PCI_SEC_LATENCY_TIMER   0x1b
#define PCI_IO_BASE             0x1c    /* I/O range behind the bridge */
#define PCI_IO_LIMIT            0x1d
#define PCI_IO_RANGE_TYPE_MASK  0x0fUL  /* I/O bridging type */
#define PCI_IO_RANGE_TYPE_16    0x00
#define PCI_IO_RANGE_TYPE_32    0x01
#define PCI_IO_RANGE_MASK       (~0x0fUL)       /* Standard 4K I/O windows */
#define PCI_IO_1K_RANGE_MASK    (~0x03UL)       /* Intel 1K I/O windows */
#define PCI_SEC_STATUS          0x1e    /* Secondary status register */
#define PCI_MEMORY_BASE         0x20    /* Memory range behind */
#define PCI_MEMORY_LIMIT        0x22
#define PCI_MEMORY_RANGE_TYPE_MASK 0x0fUL
#define PCI_MEMORY_RANGE_MASK   (~0x0fUL)
#define PCI_PREF_MEMORY_BASE    0x24    /* Prefetchable memory range behind */
#define PCI_PREF_MEMORY_LIMIT   0x26
#define PCI_PREF_RANGE_TYPE_MASK 0x0fUL
#define PCI_PREF_RANGE_TYPE_32  0x00
#define PCI_PREF_RANGE_TYPE_64  0x01
#define PCI_PREF_RANGE_MASK     (~0x0fUL)

/* Upper half of prefetchable memory range */

#define PCI_PREF_BASE_UPPER32   0x28    
#define PCI_PREF_LIMIT_UPPER32  0x2c
#define PCI_IO_BASE_UPPER16     0x30    /* Upper half of I/O addresses */
#define PCI_IO_LIMIT_UPPER16    0x32    /* 0x34 same as for htype 0 */
#define PCI_ROM_ADDRESS1        0x38    /* Same as PCI_ROM_ADDRESS, but for htype 1 */

#define PCI_BRIDGE_CONTROL      0x3e

/* PCI Express Link Statue */

#define PCI_LINKSTATUS          0x90
#define PCI_LINKSTATUS_SHIFT    16
#define PCI_LINKSTATUS_WIDTH_MASK       0x3f0
#define PCI_LINKSTATUS_WIDTH_DFL        0x0

/* Enable parity detection on secondary interface */

#define PCI_BRIDGE_CTL_PARITY   0x01    
#define PCI_BRIDGE_CTL_SERR     0x02    /* The same for SERR forwarding */
#define PCI_BRIDGE_CTL_ISA      0x04    /* Enable ISA mode */
#define PCI_BRIDGE_CTL_VGA      0x08    /* Forward VGA addresses */
#define PCI_BRIDGE_CTL_MASTER_ABORT     0x20    /* Report master aborts */
#define PCI_BRIDGE_CTL_BUS_RESET        0x40    /* Secondary bus reset */
#define PCI_BRIDGE_CTL_FAST_BACK        0x8

/* structure holding the instance specific details */

typedef struct pci_ex_drv_ctrl {
    VXB_DEVICE_ID       pciexDev;
    VXB_DEVICE_ID       fpgaMgrDev;
    void *              pciexHandle;
    SEM_ID              pciexDevSem;
    void *              regBase;
    BOOL                statue;
    BOOL                checkLink;

    UINT32              mem32Addr;
    UINT32              mem32Size;
    UINT32              memIo32Addr;
    UINT32              memIo32Size;
    UINT32              txsAddr;
    UINT32              minBusSet;
    UINT32              maxBusSet;
    UINT32              autoConfig;  

    struct vxbPciConfig *   pPciConfig; 
    struct vxbPciInt *      pIntInfo;

    FUNCPTR             fpgaMgrIsReady;
    BOOL                fpgaStatue;

    UINT8               primaryBusNo;
} PCI_EX_DRV_CTRL;

typedef struct {
    DL_NODE         node;               /* double link list */
    INT32 vector;
    INT32 enabled;
    void (*routine) (_Vx_usr_arg_t);    /* interrupt handler */
    _Vx_usr_arg_t   parameter;          /* parameter of the handler */
} PCIE_INT_RTN; 

/* PCI Express read and write interface */

#define PCI_EX_BAR(p)           ((char *)((p)->pRegBase[0]))
#define PCI_EX_HANDLE(p)        (((PCI_EX_DRV_CTRL *)(p)->pDrvCtrl)->pciexHandle)

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbAltSocGen5PciExh */
