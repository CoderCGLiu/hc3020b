/* usbOhci.c - USB OHCI Driver Entry and Exit points */

/*
 * Copyright (c) 2002-2003, 2005-2011, 2013-2014 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2002-2003, 2005-2011, 2013-2014 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
modification history
--------------------
03c,25Feb14,wyy  Enable vxbDmaBuf alignment checking (VXW6-70065)
03b,03may13,wyy  Remove compiler warning (WIND00356717)
03a,21apr13,wyy  Cut the relationship among busIndex of HCDData, unitNumber of
                 vxBus Device, and HC count (such as g_EHCDControllerCount or
                 g_UhcdHostControllerCount) to dynamically release or announce
                 a HCD driver (WIND00362065)
02z,31jan13,ghs  Correct uninitialize order (WIND00360509)
02y,04jan13,s_z  Remove compiler warning (WIND00390357)
02x,13dec11,m_y  Modify according to code check result (WIND00319317)
02w,07jan11,ghs  Clean up compile warnings (WIND00247082)
02v,14dec10,ghs  Change the usage of the reboot hook APIs to specific
                 rebootHookAdd/rebootHookDelete (WIND00240804)
02u,22nov10,ghs  Code Coverity CID(9696, 9662, 9595) (WIND00242477)
02t,06sep10,j_x  Extern usbOhciDisable function (WIND00205038)
02s,02sep10,ghs  Use OS_THREAD_FAILURE to check taskSpawn failure (WIND00229830)
02r,23aug10,m_y  Update prototype for EHCI/OHCI/UHCI init routine
                 (WIND00229662)
02q,02jul10,m_y  Modify for coding convention
02p,21jun10,ghs  Fix for polling mode(WIND00218930)
02o,18may10,m_y  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
02n,08mar10,j_x  Changed for USB debug (WIND00184542)
02m,13jan10,ghs  vxWorks 6.9 LP64 adapting
02l,11sep09,ghs  Redefine max hcd as global(WIND00152418)
02k,27aug09,y_l  Fix compile error, isrEvent is not defined in polling mode
                 (WIND00178501)
02j,20aug09,y_l  Remove float point handle in usb driver, fix defect (WIND00174612)
02i,17jul09,ghs  Add USB_VXB_VERSIONID for usb (WIND00172037)
02h,29jun09,w_x  Correct usbHcdOhciDeviceInit (WIND00171028 and WIND00171156)
02g,13feb09,w_x  Fix USB HCD re-initialization vxBus issue and change
                 vxbDrvUnlink methods to have STATUS return type (WIND00152849)
02f,12dec08,d_l  fix an error in usbOhciDisable. (WIND00147247)
02e,01aug08,j_x  vxBus USB add dataSwap method for PLB devices
02d,18jun08,h_k  removed pAccess.
02c,18jul08,w_x  Legacy support fix for Defect WIND00044349 corrected
02b,16jul08,w_x  Speaker sound jump issue fix merged (WIND00119020)
02a,11jul08,w_x  Fix OHCI power on/off ports inconsistent code issue (WIND00120108)
01z,03jun08,j_x  Convert vxBus API busCfgRead/Wirte to vxbPciDevCfgRead/Write
01y,18mar08,l_z  Should return TRUE in probe function when the bus is PLB
01x,25sep07,tor  VXB_VERSION_3
01w,19sep07,ami  CQ:WIND00102614 Fix (Defect WIND00102576)
01v,24aug07,ami  Support for Non-PCI added in vxBus code
01u,05sep07,jrp  APIGEN updates
01t,23aug07,jrp  WIND00101202 - vxBus command line prototypes
01s,06aug07,jrp  Changing register access methods
01r,30jul07,adh  Defect WIND00089560 fix, HcFmInterval set incorrectly
01q,25jul07,jrp  WIND00099137 - changing initialization order
01p,17jul07,jrp  Adding Instantiation routine
01o,10jul07,jrp  Removing OS_ENTER_CRITICAL_SECTION
01n,13jun07,tor  remove VIRT_ADDR
01m,30mar07,sup  changed the routine for read and write for one additional
                 parameter flag. Also the legacy support updated.
01l,08oct06,ami  Changes for USB-vxBus changes
01k,11Dec06,jrp  Defect 61612 - changes to power sequence
01j,24nov05,ami  required check implemented in the function usbOhciExit
                 (SPR#115337 fix)
01i,26apr05,mta  Diab compiler warning removal.
01h,15apr05,pdg  Fix for compilation error when USB_OHCI_POLLING_MODE is
                 defined
01g,11apr05,ami  Changes made in Warm Reboot Function
01f,06apr05,mta  Fix for warm reboot giving mempartfree error
01e,28mar05,pdg  non-PCI changes
01d,02mar05,ami  SPR #106373 (Max OHCI Controller Issue) & correction in
                 usbOhciCreatePipe function call
01c,25feb05,mta  SPR 106276
01b,27jun03,nld  Changing the code to WRS standards
01a,17mar02,ssh  Initial Version
*/

/*
DESCRIPTION

This provides the entry and exit points for the USB OHCI driver.

INCLUDE FILES:    usbOhci.h, usbOhciRegisterInfo.h, usbOhciTransferManagement.h,
    usbOhciRootHubEmulation.c, usbOhciTransferManagement.c, usbOhciIsr.c
rebootLib.h
*/

/*
INTERNAL
 *******************************************************************************
 * Filename         : usbOhci.c
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 * Description      : This file contains entry and exit points for the
 *                    OHCI driver.
 *
 *
 ******************************************************************************/

/* includes */

#include <usb/usbOsal.h>
#include <usb/usbHst.h>
#include <usbOhci.h>
#include <usbOhciRegisterInfo.h>
#include <usbOhciTransferManagement.h>
#include <usbOhciUtil.h>
#include <rebootLib.h>
#include <hwif/vxbus/vxBus.h>
#include <hwif/vxbus/vxbPciLib.h>
#include <hwif/vxbus/hwConf.h>
#include <hookLib.h>

/* defines */

#define USB_OHCI_BASEC          0x0C000000      /* class code */
#define USB_OHCI_SCC            0x00030000      /* sub class code */
#define USB_OHCI_PI             0x00001000      /* programming interface */

/********************** MODULE SPECIFIC MACRO DEFINITION **********************/

/* To hold the maximum bandwidth support in USB 1.1.
 *
 * NOTE: The formula used to compute the bandwidth required by an endpoint
 *       (COMPUTE_BANDWIDTH_FOR_INTERRUPT_ENDPOINT and
 *       COMPUTE_BANDWIDTH_FOR_ISOCHRONOUS_ENDPOINT) return the value in
 *       nano seconds. Hence, the following variable stores the bandwidth
 *       available in nano seconds.
 */

#define USB_OHCI_MAXIMUM_USB11_BANDWIDTH                        1000000

/*
 * To hold the percentage of bandwidth reserved for periodic transfers
 * for example: the value 90 means 90%
 */

#define USB_OHCI_PERCENTAGE_BANDWIDTH_FOR_PERIODIC_TRANSFERS_PERCENT  90

#define NUM_RETRIES_TO_DETECT_CHANGE          1000

#define WAIT_FOR_RESUME_TO_COMPLETE           1
#define WAIT_TO_REFLECT_OWNERSHIP_CHANGE      1

/*
 * Mask value for the interrupts to be enabled. Enable the following
 * interupts:
 *
 *  - Schedule Overrun
 *  - Done Head Write Back
 *  - Start Of Frame
 *  - Resume Detect
 *  - Unrecoverable Error
 *  - Frame Number Overflow
 *  - Root Hub Status Change
 *
 * Finally set the Master Interrupt Enable bit.
 */

/*
 * NOTE: For debugging, only the following interrupts are enabled
 *
 *       - Schedule Overrun
 *       - Done Head Write Back
 *       - SOF
 *       - Root Hub Status Change
 */
#define USB_OHCI_INTERRUPT_MASK                                 0x80000042

/*
 * Specifies the time (in milli seconds) to wait for the host controller
 * reset operation to complete.
 */

#define USB_OHCI_WAIT_FOR_HOST_CONTROLLER_RESET_COMPLETION      1

/*
 * On completion of the reset, the host controller is in USB_SUSPEND
 * state. The host controller should remain in the USB_SUSPEND state
 * for atleast 2ms.
 */

#define USB_OHCI_MINIMUM_DURATION_IN_SUSPEND_STATE               2

/*
 * To hold the configuration values for the default control endpoint (BEGIN)
 *
 * This endpoint will be used to configure all the newly attached devices.
 */

/* To hold the default device address */

#define USB_DEFAULT_DEVICE_ADDRESS                  0

/* To hold the default device speed */

#define USB_DEFAULT_DEVICE_SPEED                    USB_OHCI_DEVICE_FULL_SPEED

/* To hold the default device maximum packet size */

#define USB_DEFAULT_DEVICE_MAXIMUM_PACKET_SIZE      0x40

/* The name to register to vxBus */
#define USB_OHCI_PLB_NAME "vxbPlbUsbOhci"
#define USB_OHCI_PCI_NAME "vxbPciUsbOhci"
#define USB_OHCI_HUB_NAME "vxbUsbOhciHub"


/* globals */

IMPORT VOID usbVxbRootHubAdd (VXB_DEVICE_ID pDev);      /* func to add root */
                                                        /* hub */

IMPORT STATUS usbVxbRootHubRemove (VXB_DEVICE_ID pDev);   /* function to remove */
                                                          /* root hub */

VOID usbOhciPowerOffPorts
    (
    UINT32   uHostControllerIndex
    );

int usbOhciDisable
    (
    int    startType
    );

/* To hold the configuration values for the default control endpoint (END) */

/****************** MODULE SPECIFIC DATA STRUCTURE DECLARATION ****************/

/* forward declarations */

LOCAL STATUS usbHcdOhciDeviceRemove (VXB_DEVICE_ID pDev); /* function to remove
                                                           * OHCI Device
                                                           */

LOCAL STATUS usbHcdOhciDeviceInit (VXB_DEVICE_ID pDev);   /* function to
                                                           * handle legacy the
                                                           * support
                                                           */

LOCAL VOID usbHcdOhciDeviceConnect (VXB_DEVICE_ID pDev);/* function to
                                                         * intialize the
                                                         * OHCI device
                                                         */

LOCAL BOOL usbVxbHcdOhciDeviceProbe   (VXB_DEVICE_ID pDev);

/*
 * this function definition should moved to usbd.c as this function would be
 * used by all the host controllers
 */

LOCAL VOID usbVxbOhciNullFunction (VXB_DEVICE_ID pDev);

/* Function to initialize the OHCI data structures  */

void vxbUsbOhciRegister (void);

/* Function to deregister the OHCI driver with vxBus */

LOCAL STATUS vxbUsbOhciDeregister (void);

/* Function to initailize the OHCI Host Controller */

LOCAL BOOLEAN usbOhciInitializeHostController
    (
    VXB_DEVICE_ID pDev,
    UINT8         uBusIndex
    );

/* Function to uninitialize the OHCI Host Controller */

LOCAL VOID usbOhciUnInitializeHostController
    (
    pUSB_OHCD_DATA  pHCDData
    );

/* locals */

LOCAL PCI_DEVVEND       usbVxbPcidevVendId[1] =
    {
        {
        0xffffU,             /* device ID */
        0xffffU              /* vendor ID */
        }
    };

LOCAL DRIVER_METHOD     usbVxbHcdOhciHCMethods[2] =
    {
    DEVMETHOD (vxbDrvUnlink, usbHcdOhciDeviceRemove),
    DEVMETHOD_END
    };

LOCAL struct drvBusFuncs usbVxbHcdOhciDriverFuncs =
    {
    usbVxbOhciNullFunction,                          /* init 1 */
    usbVxbOhciNullFunction,                          /* init 2 */
    usbHcdOhciDeviceConnect                          /* device connect */
    };


/* initialization structure for OHCI Root Hub */

LOCAL struct drvBusFuncs        usbVxbRootHubDriverFuncs =
    {
    usbVxbOhciNullFunction,         /* init 1 */
    usbVxbOhciNullFunction,         /* init 2 */
    usbVxbRootHubAdd                /* device connect */
    };

/* method structure for OHCI Root Hub */

LOCAL struct vxbDeviceMethod    usbVxbRootHubMethods[2] =
    {
    DEVMETHOD (vxbDrvUnlink, usbVxbRootHubRemove),
    DEVMETHOD_END
    };


/* DRIVER_REGISTRATION for OHCI Root hub */

LOCAL DRIVER_REGISTRATION       usbVxbHcdOhciHub =
    {
    NULL,                           /* pNext */
    VXB_DEVID_BUSCTRL,              /* hub driver is bus
                                     * controller
                                     */
    VXB_BUSID_USB_HOST_OHCI,        /* parent bus ID */
    USB_VXB_VERSIONID,              /* version */
    USB_OHCI_HUB_NAME,              /* driver name */
    &usbVxbRootHubDriverFuncs,      /* struct drvBusFuncs * */
    &usbVxbRootHubMethods[0],       /* struct vxbDeviceMethod */
    NULL,                           /* probe routine */
    NULL                            /* vxbParams */
    };

/* DRIVER_REGISTRATION for the OHCI device residing on PLB */

LOCAL DRIVER_REGISTRATION       usbVxbPlbHcdOhciDevRegistration =
    {

    NULL,                                   /* register next driver */

    VXB_DEVID_BUSCTRL,                      /* bus controller */
    VXB_BUSID_PLB,                          /* bus id - PCI Bus Type */
    USB_VXB_VERSIONID,                      /* vxBus version Id */
    USB_OHCI_PLB_NAME,                      /* drv name */
    &usbVxbHcdOhciDriverFuncs,              /* pDrvBusFuncs */
    &usbVxbHcdOhciHCMethods[0],             /* pMethods */
    NULL,                                   /* probe routine */
    NULL                                    /* vxbParams */
    };

/* DRIVER_REGISTRATION for OHI device residing on PCI Bus */

LOCAL PCI_DRIVER_REGISTRATION   usbVxbPciHcdOhciDevRegistration =
    {
        {
        NULL,                                   /* register next driver */
        VXB_DEVID_BUSCTRL,                      /* bus controller */
        VXB_BUSID_PCI,                          /* bus id - PCI Bus Type */
        USB_VXB_VERSIONID,                      /* vxBus version Id */
        USB_OHCI_PCI_NAME,                      /* drv name */
        &usbVxbHcdOhciDriverFuncs,              /* pDrvBusFuncs */
        &usbVxbHcdOhciHCMethods [0],            /* pMethods */
        usbVxbHcdOhciDeviceProbe,               /* probe routine */
        NULL                                    /* vxbParams */
        },
    NELEMENTS(usbVxbPcidevVendId),              /* idListLen */
    & usbVxbPcidevVendId [0]                    /* idList */
    };

/****************** MODULE SPECIFIC FUNCTIONS DECLARATION *********************/

/******************* MODULE SPECIFIC VARIABLES DEFINITION *********************/

/* To hold the OHCI Controller information */

/*LOCAL*/   USB_OHCD_DATA * g_OHCDData = NULL;

/* Flag to indicate that the OHCI Controllers are initialized */

LOCAL   BOOLEAN bInitialized = FALSE;

/*
 * To hold the handle to the host controller driver. This handle will be
 * returned by USBD in response to the USBHST_RegisterHCD() function call.
 * This handle should be used to deregister the host controller driver, i.e.
 * the handle will be passed as a parameter to USBHST_DeregisterHCD()
 * function.
 */

LOCAL   UINT32 hHostControllerDriver = 0;

LOCAL   UINT32 initializedOHCDCount = 0;

/**************************** SUB MODULE INCLUDES *****************************/

/*
 * Root hub emulation module
 *
 * Since the transfer management module uses the function root hub emulation
 * module, include the root hub emulation module before the transfer
 * management module.
 */

#include "usbOhciRootHubEmulation.c"
#include "usbOhciUtil.c"

/*
 * Transfer Management module
 *
 * Since the ISR uses the functions in transfer management module, include
 * the transfer management module before the ISR module.
 */

#include "usbOhciTransferManagement.c"
#include "usbOhciIsr.c"
#include "usbOhciDebug.c"

void usbOhciInstantiate (void);

/************************ GLOBAL FUNCTIONS DEFINITION *************************/
/***************************************************************************
*
* usbOhciInstantiate - instantiate the USB OHCI Host Controller Driver.
*
* This routine instantiates the OHCI Host Controller Driver and allows
* the OHCI Controller driver to be included with the vxWorks image and
* not be registered with vxBus.  OHCI devices will remain orphan devices
* until the usbOhciInit() routine is called.  This supports the
* INCLUDE_OHCI behaviour of previous vxWorks releases.
*
* The routine itself does nothing.
*
* RETURNS: N/A
*
* ERRNO:
*   None.
*/

void usbOhciInstantiate (void)
    {
    return;
    }

/*******************************************************************************
*
* usbOhcdInit - initialize the USB OHCI Host Controller Driver.
*
* This function initializes internal data structues in the OHCI Host Controller
* Driver.  This routine is typically called prior the the vxBus invocation of
* the device connect.
*
* This routine requires that the USBD has been initialized.
*
* This function registers the OHCI HCD with the USBD Layer.
*
* PARAMETERS: N/A
*
* RETURNS: OK or ERROR
*
* ERRNO: N/A
*/

STATUS usbOhcdInit (void)
    {

    /*
     * To hold the OHCI Host Controller Driver functions to be registered
     * with USBD.
     */

    USBHST_HC_DRIVER    hostControllerDriverEntry;

    /* To hold the status of deregistering the OHCI host controller driver */

    USBHST_STATUS   uUsbStatus = USBHST_SUCCESS;


    /*
     * This routine should be called only once during the initialization of the
     * host controller driver.
     *
     * Check whether the OHCI Controller information pointer is valid. If
     * this pointer is valid, then the function is called for the second time.
     * Hence, return without processing the request.
     */

    if (bInitialized)
        {
        /* WindView Instrumentation */

        USB_HCD_LOG_EVENT(
            USB_OHCI_WV_INIT_EXIT,
            "usbOhcdInit() exits : Error - already called once",
            USB_OHCD_WV_FILTER);

        return OK;
        }

    /* Clear the OHCI host controller driver entry functions information */

    OS_MEMSET(&hostControllerDriverEntry, 0, sizeof(USBHST_HC_DRIVER));

    /* Initialize the OHCI host controller driver entry points (BEGIN) */

    /* Function to create the pipes for the endpoint */

    hostControllerDriverEntry.createPipe = usbOhciCreatePipe;

    /* Function to delete a pipe corresponding to an endpoint */

    hostControllerDriverEntry.deletePipe = usbOhciDeletePipe;

    /* This function is used to submit a request to the device */

    hostControllerDriverEntry.submitURB = usbOhciSubmitUrb;

    /* This function is used to cancel a request submitted to the device */

    hostControllerDriverEntry.cancelURB = usbOhciCancelUrb;

    /* Function to control the pipe characteristics */

    hostControllerDriverEntry.pipeControl = usbOhciPipeControl;

    /*
     * Function to determine whether the bandwidth is available to support the
     * new configuration or an alternate interface.
     */

    hostControllerDriverEntry.isBandwidthAvailable =
        usbOhciIsBandwidthAvailable;

    /*
     * Function to modify the properties of the default control pipe, i.e. the
     * pipe corresponding to Address 0, Endpoint 0.
     */

    hostControllerDriverEntry.modifyDefaultPipe = usbOhciModifyDefaultPipe;

    /* Function to check whether any request is pending on a pipe */

    hostControllerDriverEntry.isRequestPending = usbOhciIsRequestPending;

    /* Function to obtain the current frame number */

    hostControllerDriverEntry.getFrameNumber = usbOhciGetFrameNumber;

    /* Function to modify the frame width */

    hostControllerDriverEntry.setBitRate = usbOhciSetBitRate;

    /* Initialize the OHCI host controller driver entry points (END) */

    /* Register the OHCI host controller driver with USBD */

    uUsbStatus = usbHstHCDRegister (&hostControllerDriverEntry,
                                    &hHostControllerDriver,
                                    NULL,
                                    VXB_BUSID_USB_HOST_OHCI);

    /*
     * Check whether the OHCI host controller driver was registered
     * successfully
     */

    if (uUsbStatus != USBHST_SUCCESS)
        {
        /* Failed to register the OHCI host controller driver */

        USB_OHCD_ERR (
            "Failed to register OHCI host controller driver.\n",
            1, 2, 3, 4, 5, 6);

        /* WindView Instrumentation */

        USB_HCD_LOG_EVENT(
            USB_OHCI_WV_INIT_EXIT,
            "usbOhcdInit() exits - OHCD registration failed",
            USB_OHCD_WV_FILTER);

        /* Return without proceeding */

        return ERROR;
        }

    /* Allocate the memory for  OHCI host controller */

    g_OHCDData = (USB_OHCD_DATA *)OS_MALLOC
                            (sizeof(USB_OHCD_DATA) * USB_MAX_OHCI_COUNT);

    if (g_OHCDData == NULL)
        {

        /* Deregister the HCD with the USBD */

        if (USBHST_SUCCESS != usbHstHCDDeregister (hHostControllerDriver))
            {
            USB_OHCD_DBG("usbHstHCDDeregister fail\n",
                        1, 2, 3, 4, 5, 6);
            }

        /* WindView Instrumentation */

        USB_HCD_LOG_EVENT(
            USB_OHCI_WV_INIT_EXIT,
            "usbOhcdInit() exits : Error - no OHCI Controller",
            USB_OHCD_WV_FILTER);

        return ERROR;
        }

    /* Clear the OHCI Host Controller information */

    OS_MEMSET (g_OHCDData,
               0,
               sizeof (USB_OHCD_DATA) * USB_MAX_OHCI_COUNT);

    /* Hook the function that should be called on reboot */
#if 0
    if (ERROR == rebootHookAdd(usbOhciDisable))
        {

        /* Deregister the HCD with the USBD */

        if (USBHST_SUCCESS != usbHstHCDDeregister (hHostControllerDriver))
            {
            USB_OHCD_DBG("usbHstHCDDeregister fail\n",
                        1, 2, 3, 4, 5, 6);
            }

        /* Free the memory allocated for the host controller driver */

        OS_FREE (g_OHCDData);

        g_OHCDData = NULL;

        return ERROR;
        }
#endif //zj
    /* Set the flag to specify that the OHCI Controllers are initialized */

    bInitialized = TRUE;

    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT(
        USB_OHCI_WV_INIT_EXIT,
        "usbOhcdInit() exits successfully",
        USB_OHCD_WV_FILTER);

    return OK;
    } /* End of function usbOhcdInit */

/*******************************************************************************
*
* usbOhcdExit - uninitialize the USB OHCI Host Controller Driver.
*
* This routine uninitializes the OHCI Host Controller Driver.
*
* RETURNS: ERROR, OK if all the OHCI Host Controllers are reset and the
* cleanup is successful.
*
* ERRNO: N/A
*/

STATUS usbOhcdExit (void)
    {
    /*
     * To hold the index into the OHCI_INFORMATION array corresponding to the
     * number of OHCI Controllers found on the system.
     */

    UINT32                  uOhciControllerIndex = 0;

    /* To hold the status of deregistering the OHCI host controller driver */

    USBHST_STATUS           uUsbStatus = USBHST_SUCCESS;

    /*
     * Check whether the OHCI hardware in intialized. If not the host controller
     * is not attached
     * return FALSE
     */

    if (bInitialized == FALSE)
        {
        /* WindView Instrumentation */

        USB_HCD_LOG_EVENT(
        USB_OHCI_WV_INIT_EXIT,
        "usbOhcdExit() exits : ERROR - OHCI not initialized",
            USB_OHCD_WV_FILTER);

        return ERROR;
        }

    /* Deregister the OHCI host controller driver with the vxBus */

    if (vxbUsbOhciDeregister() == ERROR)
        {
        USB_OHCD_ERR("failed to deregister OHCI host "\
                     "controller driver with the vxBus \n",
                      1, 2, 3, 4, 5, 6);
        return ERROR;
        }


    for (uOhciControllerIndex = 0;
         uOhciControllerIndex < USB_MAX_OHCI_COUNT;
         uOhciControllerIndex++)
        {
        if (g_OHCDData[uOhciControllerIndex].bHostControllerInitialized
            == TRUE)
            {
            USB_OHCD_ERR("Host controller is initializing... \n",
                         1, 2, 3, 4, 5, 6);
            return ERROR;
            }
        }

    /* Call the function to deregister the OHCI host controller driver */

    uUsbStatus = usbHstHCDDeregister (hHostControllerDriver);

    /*
     * Check whether the OHCI host controller driver was deregistered
     * successfully
     */

    if (uUsbStatus != USBHST_SUCCESS)
        {
        /* Debug message */

        USB_OHCD_ERR("failed to deregister OHCI host " \
                     "controller driver with the usb stack \n",
                     1, 2, 3, 4, 5, 6);


        /*
         * NOTE: There is no error recovery mechanism. Hence the error is
         *       ignored.
         */
        }

    /* Delete the hook function from the table */
#if 0
    (void) rebootHookDelete(usbOhciDisable);
#endif //zj
    /* Free the usbOhciControllerInfo */

    OS_FREE (g_OHCDData) ;

    g_OHCDData = NULL;

    /* Set the flag to specify that the OHCI Controllers are not initialized */

    bInitialized = FALSE;

    initializedOHCDCount = 0;

    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT(
        USB_OHCI_WV_INIT_EXIT,
        "usbOhcdExit() exits successfully",
        USB_OHCD_WV_FILTER);

    return OK;

    } /* End of function usbOhcdExit () */


/******************* MODULE SPECIFIC FUNCTIONS DEFINITION *********************/

/*******************************************************************************
*
* usbOhciUnInitializeHostController - uninitialize the USB OHCI Host Controller
*
* This routine uninitializes the USB OHCI Host Controller.
*
* RETURNS: N/A.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbOhciUnInitializeHostController
    (
    pUSB_OHCD_DATA   pHCDData
    )
    {
    if ((pHCDData == NULL) ||
        (pHCDData->pDev == NULL))
        {
        USB_OHCD_ERR("invalid parameter of pHCDData\n",
                      1, 2, 3, 4, 5, 6);
        return;
        }

    /* Delete default pipe */

    if (NULL != pHCDData->pDefaultPipe)
        {
        usbOhciCleanUpPipe(pHCDData,
                           pHCDData->pDefaultPipe);
        pHCDData->pDefaultPipe = NULL;
        }

    /* Destroy pipeListAccessEvent */

    if (pHCDData->pipeListAccessEvent != OS_INVALID_EVENT_ID)
        {
        OS_DESTROY_EVENT (pHCDData->pipeListAccessEvent);
        pHCDData->pipeListAccessEvent = OS_INVALID_EVENT_ID;
        }

    /* Delete the memMapTable semphore */

    if (pHCDData->memMapTableEvent != NULL)
        {
        if (semTake(pHCDData->memMapTableEvent, WAIT_FOREVER) == OK)
            semDelete(pHCDData->memMapTableEvent);

        pHCDData->memMapTableEvent = NULL;
        }

    /* Delete the common semphore */

    if (pHCDData->ohciCommonMutex != NULL)
        {
        OS_DESTROY_EVENT(pHCDData->ohciCommonMutex);
        pHCDData->ohciCommonMutex = NULL;
        }

    /* Free HCCA */

    if (NULL != pHCDData->pHcca)
        {
        /*
         * Free the memory allocated for the Periodic Frame List.
         *
         * If pHCDData->pHcca is non-NULL, then we are sure
         * pHCDData->hccaDmaTagId and pHCDData->hccaDmaMapId
         * have already been created, thus no need to check them for NULL.
         */

        (void) vxbDmaBufMemFree(pHCDData->hccaDmaTagId,
                         pHCDData->pHcca,
                         pHCDData->hccaDmaMapId);
        }

    /* Destroy hcca dma tag */

    if (pHCDData->hccaDmaTagId)
        {
        (void) vxbDmaBufTagDestroy(pHCDData->hccaDmaTagId);
        pHCDData->hccaDmaTagId = NULL;
        }

    /* Reset pDev->pDrvCtr to NULL */

    if (NULL != pHCDData->pDev->pDrvCtrl)
        pHCDData->pDev->pDrvCtrl = NULL;

    /* Destroy ED dma tag */

    if (pHCDData->edDmaTagId != NULL)
        {
        (void) vxbDmaBufTagDestroy(pHCDData->edDmaTagId);
        pHCDData->edDmaTagId = NULL;
        }

    /* Destroy TD dma tag */

    if (pHCDData->tdDmaTagId != NULL)
        {
        (void) vxbDmaBufTagDestroy(pHCDData->tdDmaTagId);
        pHCDData->tdDmaTagId = NULL;
        }

#ifndef USB_OHCI_POLLING_MODE
    /* Destroy the isrEvent */

    if(OS_INVALID_EVENT_ID != pHCDData->isrEvent)
        {
        OS_DESTROY_EVENT (pHCDData->isrEvent);
        pHCDData->isrEvent = OS_INVALID_EVENT_ID;
        }
#endif

    if (0 != pHCDData->regBase)
        vxbRegUnmap (pHCDData->pDev, pHCDData->regIndex);

    return;
    }

/*******************************************************************************
*
* usbOhciInitializeHostController - initialise the USB OHCI Host Controller
*
* This routine initialises the OHCI Host Controller. It
*    a) Identifies the PCI IRQ Number
*    b) Identifies the Base Address of USB Registers
*    c) Reset the OHCI Host Controller
*    d) Initialize the OHCI Host Controller
*
* RETURNS: FALSE,TRUE if the OHCI Host Controller is initialized successfully
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL BOOLEAN usbOhciInitializeHostController
    (
    VXB_DEVICE_ID      pDev,
    UINT8              uHCIndex
    )
    {
    INT32           i;

    /* To hold the value read from the registers */

    UINT32          uRegisterValue = 0;

    /* To hold the status of the operation */

    USBHST_STATUS   uStatus = USBHST_SUCCESS;

    /* To hold the value of the HcFmInterval register */

    UINT32          uHcFmInterval = 0;

    ULONG           uDefaultPipeHandle = 0;

    /* counter */

    UINT32      count = 0;

    STATUS      vxbSts;

    UINT32      uBufferAddr = 0x0;

    /* To hold the default control endpoint descriptor */

    USB_ENDPOINT_DESCRIPTOR controlEndpointDescriptor =
        {
        7,                                      /* bLength */
        USB_OHCI_ENDPOINT_DESCRIPTOR_TYPE,      /* Endpoint Descritpor Type */
        0x00,                                   /* Endpoint Address */
        USB_OHCI_CONTROL_TRANSFER,              /* bmAttributes */
        USB_DEFAULT_DEVICE_MAXIMUM_PACKET_SIZE, /* wMaxPacketSize */
        0                                       /* bInterval */
        };


    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA          pHCDData = NULL;
    HCF_DEVICE *            pHcf = NULL;

    /* Initialize the OHCI Controller (BEGIN) */

    /*
     * The following initization steps should be followed to initialize
     * the OHCI Host Controller:
     *
     * a) Save the contents of the HcFmInterval register.
     * b) Issue a software reset to the host controller. It takes 1 micro
     *    second to complete the software reset.
     * c) Restore the contents of the HcFmInterval register.
     * d) Initialize the device data HCCA block to match the current
     *    device data state.
     * e) Initialize the Operational Registers to match the current device
     *    data state.
     * f) Program the HcHCCA register with the physical address of the
     *    HCCA block.
     * g) Set HcInterruptEnable to have all interrupt enabled except SOF
     *    detect.
     * h) Set HcControl to have "all queues on".
     * i) Set HcPeriodicStart to a value that is 90% of the value in
     *    FrameInterval field of HcFmInterval register.
     * j) Put the host control in Operation State.
     */


    /* validate the function parameters */

    if (pDev == NULL)
        {
        /* invalid pDev */

        USB_OHCD_ERR("usbOhciInitializeHostController - "
            "invalid parameter of pDev \n",
            1, 2, 3, 4, 5, 6);

        return FALSE;
        }

    /* Obtain the pointer to the host controller information */

    pHCDData = &g_OHCDData[uHCIndex];

    /* Remember the bus index */

    pHCDData->uBusIndex = (UINT8)uHCIndex;

    /* Initialize the frame number rollover event status */

    pHCDData->bLastFrameRollover = FALSE;

    /* Init the disable list */

    lstInit(&(pHCDData->disableEDList));

    /* Init ED memMap Table List */

    lstInit((LIST *)(&(pHCDData->edMemMapTableList)));

    /* Init TD memMap Table List */

    lstInit((LIST *)(&(pHCDData->tdMemMapTableList)));

    /* Create memMap Table Event */

    pHCDData->memMapTableEvent =
        semMCreate(SEM_INVERSION_SAFE|SEM_Q_PRIORITY);

    /* Check if the memMap table event is sucessfully created */

    if (pHCDData->memMapTableEvent == NULL)
        {
        USB_OHCD_ERR("usbOhciInitializeHostController - "
            "memMapTableEvent not created\n", 1, 2, 3, 4, 5, 6);

        usbOhciUnInitializeHostController(pHCDData);

        return FALSE;
        }

    /* Create the endpoint list access event */

    pHCDData->pipeListAccessEvent =
        OS_CREATE_EVENT (OS_EVENT_SIGNALED);

    /* Check whether the event was created successfully */

    if (pHCDData->pipeListAccessEvent == OS_INVALID_EVENT_ID)
        {
        USB_OHCD_ERR("usbOhciInitializeHostController - "
            "pipeListAccessEvent not created\n", 1, 2, 3, 4, 5, 6);

        usbOhciUnInitializeHostController(pHCDData);

        return FALSE;
        }

    /*
     * Initialize the semaphore used for synchronization.  Use a mutex to take
     * advantage of the inversion safe capability
     */

    pHCDData->ohciCommonMutex = semMCreate
                                (SEM_INVERSION_SAFE|SEM_Q_PRIORITY);

    if (pHCDData->ohciCommonMutex == NULL)
        {
        USB_OHCD_ERR("usbOhciInitializeHostController - "
            "ohciCommonSem not created\n", 1, 2, 3, 4, 5, 6);

        usbOhciUnInitializeHostController(pHCDData);

        return FALSE;
        }

    /* set usrDmaMapFlags */
    pHCDData->usrDmaMapFlags = VXB_DMABUF_CACHEALIGNCHECK;
    /* Get paraent DMA TAG ID */

    pHCDData->ohciParentTag = vxbDmaBufTagParentGet (pDev, 0);

    /* Create edDmaTagID for USB_OHCD_PIPE */

    pHCDData->edDmaTagId = vxbDmaBufTagCreate (pDev,
           pHCDData->ohciParentTag,        /* parent */
           USB_OHCI_ED_DESC_ALIGN,         /* alignment */
           USB_OHCI_DESC_BOUNDARY_LIMIT,   /* boundary */
           VXB_SPACE_MAXADDR_32BIT,        /* lowaddr */
           VXB_SPACE_MAXADDR,              /* highaddr */
           NULL,                           /* filter */
           NULL,                           /* filterarg */
           USB_OHCI_MAX_ED_SIZE,           /* max size */
           1,                              /* nSegments */
           USB_OHCI_MAX_ED_SIZE,           /* max seg size */
           VXB_DMABUF_NOCACHE,             /* flags */
           NULL,                           /* lockfunc */
           NULL,                           /* lockarg */
           NULL);                          /* ppDmaTag */

    if (pHCDData->edDmaTagId == NULL)
        {
        USB_OHCD_ERR("usbOhciInitializeHostController - "
                     "creating edDmaTagId fail\n",
                      1, 2, 3, 4, 5, 6);

         usbOhciUnInitializeHostController(pHCDData);

         return FALSE;
         }

    /* Create tdDmaTagId for USB_OHCI_GENERAL_TRANSFER_DESCRIPTOR */

    pHCDData->tdDmaTagId = vxbDmaBufTagCreate (pDev,
          pHCDData->ohciParentTag,                   /* parent */
          USB_OHCI_TD_DESC_ALIGN,                    /* alignment */
          USB_OHCI_DESC_BOUNDARY_LIMIT,              /* boundary */
          VXB_SPACE_MAXADDR_32BIT,                   /* lowaddr */
          VXB_SPACE_MAXADDR,                         /* highaddr */
          NULL,                                      /* filter */
          NULL,                                      /* filterarg */
          USB_OHCI_MAX_TD_SIZE,                      /* max size */
          1,                                         /* nSegments */
          USB_OHCI_MAX_TD_SIZE,                      /* max seg size */
          VXB_DMABUF_NOCACHE,                        /* flags */
          NULL,                                      /* lockfunc */
          NULL,                                      /* lockarg */
          NULL);                                     /* ppDmaTag */

    if (pHCDData->tdDmaTagId == NULL)
        {
        USB_OHCD_ERR("usbOhciInitializeHostController - "
                     "creating tdDmaTagId fail\n",
                     1, 2, 3, 4, 5, 6);

        usbOhciUnInitializeHostController(pHCDData);

        return FALSE;
        }

   /* Create hccaDmaTagId for USB_OHCI_HCCA */

   pHCDData->hccaDmaTagId = vxbDmaBufTagCreate (pDev,
      pHCDData->ohciParentTag,                   /* parent */
      USB_OHCI_HCCA_ALIGN,                       /* alignment */
      USB_OHCI_DESC_BOUNDARY_LIMIT,              /* boundary */
      VXB_SPACE_MAXADDR_32BIT,                   /* lowaddr */
      VXB_SPACE_MAXADDR,                         /* highaddr */
      NULL,                                      /* filter */
      NULL,                                      /* filterarg */
      USB_OHCI_MAX_HCCA_SIZE,                    /* max size */
      1,                                         /* nSegments */
      USB_OHCI_MAX_HCCA_SIZE,                    /* max seg size */
      VXB_DMABUF_NOCACHE,                        /* flags */
      NULL,                                      /* lockfunc */
      NULL,                                      /* lockarg */
      NULL);                                     /* ppDmaTag */

    if (pHCDData->hccaDmaTagId == NULL)
        {
        USB_OHCD_ERR("usbOhciInitializeHostController - "
                     "creating hccaDmaTagId fail\n",
                     1, 2, 3, 4, 5, 6);

        usbOhciUnInitializeHostController(pHCDData);

        return FALSE;
        }

    /* Allocate memory for USB_OHCI_HCCA */

    pHCDData->pHcca = vxbDmaBufMemAlloc (pDev,
        pHCDData->hccaDmaTagId,
        NULL,
        VXB_DMABUF_MEMALLOC_CLEAR_BUF,
        &pHCDData->hccaDmaMapId);

    if (pHCDData->pHcca == NULL)
        {
        USB_OHCD_ERR("usbOhciInitializeHostController - "
                     "creating HCCA fail\n",
                     1, 2, 3, 4, 5, 6);

        usbOhciUnInitializeHostController(pHCDData);

        return FALSE;
        }

    /*
     * Load the DMA MAP so that we get the correct BUS address
     * to program the hardware
     */

    vxbSts = vxbDmaBufMapLoad (pDev,
                               pHCDData->hccaDmaTagId,
                               pHCDData->hccaDmaMapId,
                               pHCDData->pHcca,
                               USB_OHCI_MAX_HCCA_SIZE,
                               0);

    if (vxbSts != OK)
        {
        USB_OHCD_ERR("usbOhciInitializeHostController - "
                     "loading map HCCA fail\n",
                     1, 2, 3, 4, 5, 6);

        usbOhciUnInitializeHostController(pHCDData);

        return FALSE;
        }

    /* store the vxBus information in the host controller structure */

    pHCDData->pDev = pDev;

    /* store the host controller information in the vxBus structure */

    pDev->pDrvCtrl = pHCDData;

    /*
     * Map the access registers.  This is needed for subsequent
     * operations.  We look at the pDev->regBaseFlags which are set when the
     * pci bus device is announced to vxBus.  The device announce, based on
     * the BAR will set VXB_REG_IO, VXB_REG_MEM, VXB_REG_NONE or VXB_REG_SPEC.
     *
     * The following test identifies the BARs to use.
     */

    for (i = 0; i < VXB_MAXBARS; i++)
        {
        if (pDev->regBaseFlags[i] != VXB_REG_NONE)
            {
            if(ERROR == vxbRegMap (pDev, i, &pHCDData->pRegAccessHandle))
                {
                USB_OHCD_ERR("usbOhciInitializeHostController - "
                             "vxbRegMap fail\n",
                             1, 2, 3, 4, 5, 6);

                return FALSE;
                }

            /* Save the register base just to keep things a little simpler */

            pHCDData->regBase = (ULONG) pDev->pRegBase[i];

            /* Save the register index */

            pHCDData->regIndex = i;

            break ;
            }
        }

    if (i >= VXB_MAXBARS)
        {
        /* Free the resources alloctaed before */

        usbOhciUnInitializeHostController(pHCDData);

        USB_OHCD_ERR("usbOhciInitializeHostController - "
                     "Unable to locate usable BAR\n",
                     1, 2, 3, 4, 5, 6);

        return FALSE;
        }

    /* Populate the required function pointers for data conversions */

    switch (pDev->busID)
        {
        case VXB_BUSID_PLB:

            /*
             * The underlying bus type is PLB. Query the hwconf file for
             * address conversion functions and populate the required
             * function pointers
             */

            pHcf = hcfDeviceGet (pDev);

            if (NULL == pHcf || OK != devResourceGet (pHcf, "dataSwap",
                HCF_RES_ADDR, (void *)&(pHCDData->pDataSwap)))
                {
                USB_OHCD_DBG("devResourceGet dataSwap fail\n",
                            1, 2, 3, 4, 5, 6);
                }

            /* update the vxBus read-write handle */

            if (pHCDData->pDataSwap != NULL)
                pHCDData->pRegAccessHandle = (void *)
                VXB_HANDLE_SWAP ((ULONG)(pHCDData->pRegAccessHandle));

            break;

        case VXB_BUSID_PCI :

            /*
             * populate the function pointer for byte converions. Byte swapping
             * is required only if bus type is PCI and architecture is BE.
             * NOTE: for PLB no byte swapping is required
             */

#if (_BYTE_ORDER == _BIG_ENDIAN)
            pHCDData->pDataSwap = vxbSwap32;
#endif
            break;

        default:

            USB_OHCD_ERR("usbOhciInitializeHostController - "
                         "invalid busID %p\n",
                         pDev->busID, 2, 3, 4, 5, 6);

            usbOhciUnInitializeHostController(pHCDData);

            return FALSE;
        }

    /* Create the pipe for the default control endpoint */

    uStatus = usbOhciCreatePipe (
                uHCIndex,
                USB_DEFAULT_DEVICE_ADDRESS,
                USB_DEFAULT_DEVICE_SPEED,
                (PUCHAR) (&controlEndpointDescriptor),
                (UINT16) 0,
                &uDefaultPipeHandle);
    /*
     * Check whether the pipe for default control endpoint was created
     * successfully
     */

    if (uStatus != USBHST_SUCCESS)
        {
        USB_OHCD_ERR("usbOhciInitializeHostController - "
                     "usbOhciCreatePipe for default pipe fail (%d)\n",
                     uStatus, 2, 3, 4, 5, 6);

        usbOhciUnInitializeHostController(pHCDData);

        return FALSE;
        }

    /* Update default pipe handler */

    pHCDData->pDefaultPipe = (pUSB_OHCD_PIPE)uDefaultPipeHandle;

    /*
     * Check to see if we need to transition from SMM mode or BIOS Active. We
     * do this by getting the contents of the HcControl register and looking
     * to see if InterruptRouting Bit is set.
     *
     * If the bit is set, an SMM driver is active.  We fix that by writing a
     * one to the OwnerShipChangeRequestBit in the command status register.
     *
     * If the bit is clear, we then take a look at the functional state.
     *
     * If the functional state is USBRESET, all is well and we proceed.  If it
     * is usbOperational, all is well and we proceed.  Otherwise set USB RESUME
     * and wait a bit, then proceed
     */

    /* Obtain the control of the HC */

    if (usbHcdOhciDeviceInit(pDev) == ERROR)
        {
        USB_OHCD_ERR("usbOhciInitializeHostController - "
                     "usbHcdOhciDeviceInit fail\n",
                     1, 2, 3, 4, 5, 6);

        usbOhciUnInitializeHostController(pHCDData);

        return FALSE;
        }

    /* Read the contents of the HcFmInterval register */

    uHcFmInterval = USB_OHCI_REG_READ (pDev,
                                       USB_OHCI_FM_INTERVAL_REGISTER_OFFSET);

    /* Reset the OHCI Controller */

    USB_OHCI_REG_WRITE (pDev,
                        USB_OHCI_COMMAND_STATUS_REGISTER_OFFSET,
                        USB_OHCI_COMMAND_STATUS_HCR);

    /*
     * Wait for the reset operation to complete. The reset operation should
     * be completed in 1 micro second. However, a delay of 1 milli second
     * is provided.
     *
     * NOTE: This extra delay will not create any performance issues.
     *       Since the host controller is being enabled, this delay is
     *       acceptable.
     */

    OS_DELAY_MS (USB_OHCI_WAIT_FOR_HOST_CONTROLLER_RESET_COMPLETION);

    while (count < NUM_RETRIES_TO_DETECT_CHANGE)
        {
        if (((USB_OHCI_REG_READ (pDev,
                           USB_OHCI_COMMAND_STATUS_REGISTER_OFFSET)) &
                           USB_OHCI_COMMAND_STATUS_HCR) == 0)
            {
            break;
            }
        else
            {
            count++;

            OS_DELAY_MS(1);
            }
        }

    if (count == NUM_RETRIES_TO_DETECT_CHANGE)
        {
        USB_OHCD_ERR("usbOhciInitializeHostController - "
                     "wait for reset timeout\n",
                     1, 2, 3, 4, 5, 6);

        usbOhciUnInitializeHostController(pHCDData);

        return FALSE;
        }

    /* Check the status of the OHCI Controller */

    uRegisterValue =
        USB_OHCI_REG_READ (pDev,
                           USB_OHCI_CONTROL_REGISTER_OFFSET);

    /* Check whether the OHCI Controller is in SUSPEND state */

    if ((uRegisterValue & USB_OHCI_CONTROL_HCFS_USB_SUSPEND) !=
        USB_OHCI_CONTROL_HCFS_USB_SUSPEND)
        {
        /* Failed to reset the OHCI Controller */

        USB_OHCD_ERR("usbOhciInitializeHostController - "
                     "Failed to reset the host controller\n",
                     1, 2, 3, 4, 5, 6);

        usbOhciUnInitializeHostController(pHCDData);

        return FALSE;
        }

    /*
     * Update the maximum packet size that can be transmitted by the host
     * controller without causing a schedule overrun. (BEGIN)
     */

    /*
     * Reset the FSMPS value of the uHcFmInterval register (read earlier)
     * to prevent merges from a previous value
     */

    uHcFmInterval &= ~(0x7fff << 16);

    /* Update the largets packet size that can be transmitted in a frame */
    /* PENDING: Give proper explanation for this value */

    uHcFmInterval |= (0x2781 << 16);

    /*
     * Update the maximum packet size that can be transmitted by the host
     * controller without causing a schedule overrun. (END)
     */

    /* Restore the contents of the HcFmInterval register */

    USB_OHCI_REG_WRITE (pDev,
                        USB_OHCI_FM_INTERVAL_REGISTER_OFFSET,
                        uHcFmInterval);

    /*
     * Program the Host Controller Communication Area into the HcHCCA Register
     */

    uBufferAddr = USB_OHCI_BUS_ADDR_LO32(
        pHCDData->hccaDmaMapId->fragList[0].frag);

    USB_OHCI_REG_WRITE (pDev,
                        USB_OHCI_HCCA_REGISTER_OFFSET,
                        uBufferAddr);

    /*
     * Initialize the Host Controller Communication Area for interrupt and
     * isochronous transfers.
     *
     * NOTE: No initialization is required. The transfers will be queued
     *       based on the need.
     */

    /*
     * Initialize the head pointer for bulk transfers.
     *
     * NOTE: No initailization is required. The transfers will be queued
     *       based on the need.
     */

    /* Initialize the head pointer for control transfers (BEGIN) */

    /* Program the HcControlHeadED register */

    uBufferAddr = USB_OHCI_DESC_LO32(pHCDData->pDefaultPipe);

    USB_OHCI_REG_WRITE (pDev,
                        USB_OHCI_CONTROL_HEAD_ED_REGISTER_OFFSET,
                        uBufferAddr);

    /* Initialize the head pointer for control transfers (END) */

    /*
     * Enable the data transfers lists in the HcControl Register (BEGIN)
     *
     * The following lists are initialized.
     *
     * a) Control List Enable
     * b) Bulk List Enable
     * c) Periodic List Enable (Interrupt Only)
     * d) Isochronous List Enable
     */

    /* Read the contents of the HcControl register */

    uRegisterValue = USB_OHCI_REG_READ (pDev,
                                        USB_OHCI_CONTROL_REGISTER_OFFSET);

    /* Enable the processing of the endpoint descriptor list */

    uRegisterValue |= (USB_OHCI_CONTROL_CLE |
                       USB_OHCI_CONTROL_BLE |
                       USB_OHCI_CONTROL_PLE |
                       USB_OHCI_CONTROL_IE);

    /* Update the contents of the HcControl register */

    USB_OHCI_REG_WRITE (pDev,
                        USB_OHCI_CONTROL_REGISTER_OFFSET,
                        (UINT32)uRegisterValue);

    /* Enable the control list in the HcControl Register (END) */

    /* Update the HcPeriodicStart Register (BEGIN) */

    /* Read the contents of the HcFmInterval register */

    uHcFmInterval = USB_OHCI_REG_READ (pDev,
                               USB_OHCI_FM_INTERVAL_REGISTER_OFFSET);

    /* Obtain the contents of the HcFmInterval::FrameInterval field */

    uRegisterValue = (UINT32)(uHcFmInterval & 0x00003FFF);

    /* Compute the frame interval for starting the periodic transfers */

    uRegisterValue = (uRegisterValue *
            USB_OHCI_PERCENTAGE_BANDWIDTH_FOR_PERIODIC_TRANSFERS_PERCENT / 100);

    uRegisterValue = (uRegisterValue & 0x00003FFF);

    /*
     * Program the HcPeriodicStart register with the frame interval for
     * starting the periodic transfers.
     */

    USB_OHCI_REG_WRITE (pDev,
                        USB_OHCI_PERIODIC_START_REGISTER_OFFSET,
                        uRegisterValue);

    /* Update the HcPeriodicStart Register (END) */

    /*
     * Update the maximum periodic bandwidth available on this OHCI host
     * controller.
     */

    pHCDData->uMaximumBandwidthAvailable =
        (UINT32) (USB_OHCI_MAXIMUM_USB11_BANDWIDTH *
            USB_OHCI_PERCENTAGE_BANDWIDTH_FOR_PERIODIC_TRANSFERS_PERCENT / 100);

    /* Set the OHCI Controller to operational state (BEGIN) */

    /*
     * NOTE: After host controller reset, the host controller enters the
     *       USB_SUSPEND state. The host controller should not remain in
     *       this state for more than 2 ms. If not, the host controller
     *       will enter USB_RESUME state and the host controller should
     *       remain in this state for the minimum duration specified in
     *       the USB specification (i.e. 5 ms).
     */

    /* Read the contents of the HC Control Register */

    uRegisterValue = USB_OHCI_REG_READ (pDev,
                                        USB_OHCI_CONTROL_REGISTER_OFFSET);

    /* Clear the previous state */

    uRegisterValue &= (~USB_OHCI_CONTROL_HCFS_MASK);

    /* Set the OHCI Controller in operational state */

    uRegisterValue |= USB_OHCI_CONTROL_HCFS_USB_OPERATIONAL;

    /* Program the HC Control Register */

    USB_OHCI_REG_WRITE (pDev,
                        USB_OHCI_CONTROL_REGISTER_OFFSET,
                        uRegisterValue);

    /* Set the OHCI Controller to operational state (END) */

    /*
     * Update the frame interval register (BEGIN)
     *
     * NOTE: It is observed that some controllers like Opti OHCI card
     *       do not allow the HcFmInterval register to be updated when the
     *       controller is in USB_SUSPEND state. Hence, the HcFmInterval
     *       register is updated after the host controller is in
     *       USB_OPERATIONAL state.
     */

    OS_DELAY_MS(2);

     /* Update the contents of the HcFmInterval register */

    USB_OHCI_REG_WRITE (pDev,
                        USB_OHCI_FM_INTERVAL_REGISTER_OFFSET,
                        uHcFmInterval);

    /* Update the frame interval register (END) */

    /*
     * Initialize the root hub emulation module
     * Check whether the root hub emulation module was initialize
     * successfully
     */

    if (usbOhciRootHubEmulationInit (uHCIndex) != USBHST_SUCCESS)
        {
        USB_OHCD_ERR("usbOhciInitializeHostController - "
                     "Failed to initialize the root hub emulation module\n",
                     1, 2, 3, 4, 5, 6);

        usbOhciUnInitializeHostController(pHCDData);

        return FALSE;
        }


    /* Read the contents of the HcRhDescriptorA Register */

    uRegisterValue = USB_OHCI_REG_READ (pDev,
                           USB_OHCI_RH_DESCRIPTOR_A_REGISTER_OFFSET);


    uRegisterValue &= ~0x300;

    /*
     * Set the bit which indicates that the ports are to be powered on
     * a per-port basis
     */

    uRegisterValue |= 0x100;

    USB_OHCI_REG_WRITE (pDev,
                        USB_OHCI_RH_DESCRIPTOR_A_REGISTER_OFFSET,
                        uRegisterValue);

    USB_OHCI_REG_WRITE (pDev,
                        USB_OHCI_RH_DESCRIPTOR_B_REGISTER_OFFSET,
                        0xFFFF0000);

    /* Disable and power off the ports and again power on the ports */

    usbOhciPowerOffPorts(uHCIndex);

    /*
     * Clear any pending interrupts
     */

    USB_OHCI_REG_WRITE (pDev,
                        USB_OHCI_INTERRUPT_STATUS_REGISTER_OFFSET,
                        USB_OHCI_INTERRUPT_MASK);

#ifndef USB_OHCI_POLLING_MODE

    /* Create the ISR event */

    pHCDData->isrEvent = OS_CREATE_EVENT(OS_EVENT_NON_SIGNALED);

    /* Check whether the event was created successfully */

    if (OS_INVALID_EVENT_ID == pHCDData->isrEvent)
        {
        USB_OHCD_ERR("usbOhciInitializeHostController - "
                     "Failed to create ISR event.\n",
                     1, 2, 3, 4, 5, 6);

        usbOhciUnInitializeHostController(pHCDData);

        return FALSE;
        }

    /* Call the function to register an ISR */
#if 0 //zj
    if (vxbIntConnect(pDev,
                      0,
                      (VOIDFUNCPTR)usbOhciIsr,
                      (void *)(ULONG)(pHCDData))== ERROR)
        {
        USB_OHCD_ERR("usbOhciInitializeHostController - "
                     "vxbIntConnect fail\n",
                     1, 2, 3, 4, 5, 6);

        usbOhciUnInitializeHostController(pHCDData);

        return FALSE;
        }

    if (vxbIntEnable(pDev,
                     0,
                     (VOIDFUNCPTR)usbOhciIsr,
                     (void *)(ULONG)(pHCDData))== ERROR)
        {
        vxbIntDisconnect (pHCDData->pDev,
                          0,
                          (VOIDFUNCPTR)usbOhciIsr,
                          (void *)(ULONG)(pHCDData));

        USB_OHCD_ERR("usbOhciInitializeHostController - "
                     "vxbIntEnable fail\n",
                     1, 2, 3, 4, 5, 6);

        usbOhciUnInitializeHostController(pHCDData);

        return FALSE;
        }
#endif
    int irq_val = 0;
	vxbPciBusCfgRead (pHCDData->pDev, 0x3c, 1, &irq_val);
	printk("@@@@@@@@@@ohci irq val %d\n",irq_val);
		int_install_handler("ohci",irq_val, 0,usbOhciIsr,   (void *)(ULONG)(pHCDData));
		int_enable_pic(irq_val);

    /* Enable all the OHCI interrupts */

    USB_OHCI_REG_WRITE (pDev,
                        USB_OHCI_INTERRUPT_ENABLE_REGISTER_OFFSET,
                        USB_OHCI_INTERRUPT_MASK);
#endif /* End of #ifndef USB_OHCI_POLLING_MODE */


    /* Spawn the interrupt handler in polling mode */

    pHCDData->isrThreadId = OS_CREATE_THREAD("usbOhciIsr",  /* Task Name */
                                 USB_OHCI_ISR_THREAD_PRI,   /* Task Priority */
                                 usbOhciPollingIsr,         /* Task Entry Function */
                                 uHCIndex);                 /* Argument to the task */

    /* Check whether the interrupt handler thread was created successfully */

    if (pHCDData->isrThreadId == OS_THREAD_FAILURE)
        {
        USB_OHCD_ERR("usbOhciInitializeHostController - "
                     "Failed to spawn the interrupt handler thread\n",
                     1, 2, 3, 4, 5, 6);
        /*
         * Unregister the interrupt handler for the OHCI Controller.
         *
         * NOTE: First disable the interrupt and then disconnect
         */

#ifndef USB_OHCI_POLLING_MODE
        (void) vxbIntDisable (pHCDData->pDev,
                       0,
                       (VOIDFUNCPTR)usbOhciIsr,
                       (void *)(ULONG)(pHCDData));

        (void) vxbIntDisconnect (pHCDData->pDev,
                          0,
                          (VOIDFUNCPTR)usbOhciIsr,
                          (void *)(ULONG)(pHCDData));
#endif

        usbOhciUnInitializeHostController(pHCDData);

        return FALSE;
        }

    /* Initialize the OHCI Controller (END) */

    /* Set the flag to indicate the host controller is initialized */

    pHCDData->bHostControllerInitialized = TRUE;

    pHCDData->isrMagic = USB_OHCD_MAGIC_ALIVE;

    return TRUE;
    } /* End of function usbOhciInitializeHostController() */

/*******************************************************************************
*
* usbOhciPowerOffPorts - power off the USB ports
*
* This function will power off all the downstream ports of the root hub.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbOhciPowerOffPorts
    (
    UINT32   uHostControllerIndex
    )
    {

    /* To hold the value read from the registers */

    UINT32  uRegisterValue = 0;

    /* To hold the loop index */

    UINT32  uIndex = 0;

    /* To hold the offset of the port status register */

    UINT32  uPortStatusRegisterOffset = 0;

    /* To hold the number of downstream ports */

    UINT32  uNumberOfDownstreamPorts = 0;

    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA   pHCDData = NULL;

    /* Obtain the pointer to the host controller information */

    pHCDData = &g_OHCDData[uHostControllerIndex];

    /* Obtain the number of ports supported by the host controller (BEGIN) */

    /* Read the contents of the root hub descriptor A register */

    uNumberOfDownstreamPorts = USB_OHCI_REG_READ(pHCDData->pDev,
                USB_OHCI_RH_DESCRIPTOR_A_REGISTER_OFFSET);

    /* Mask the other fields of the root hub description register */

    uNumberOfDownstreamPorts = uNumberOfDownstreamPorts & 0x000000FF;

    /* Obtain the number of ports supported by the host controller (END) */

    /* Disable all the downstream ports then power off the ports */
    for (uIndex = 1; uIndex <= uNumberOfDownstreamPorts; uIndex++)
        {
        uPortStatusRegisterOffset =
        USB_OHCI_RH_PORT_STATUS_REGISTER_OFFSET(uIndex);

        /* Clear Port Enable */

        USB_OHCI_REG_WRITE(pHCDData->pDev,
                           uPortStatusRegisterOffset,
                           USB_OHCI_RH_PORT_STATUS_CCS);
        }

    /*
     * NOTE: If the root hub supports ganged power on the down stream ports,
     *       power off all the downstream ports. Else power off the specified
     *       port.
     */

    /* Read the contents of the root hub descriptor A register */
    uRegisterValue =
        USB_OHCI_REG_READ(pHCDData->pDev,
                          USB_OHCI_RH_DESCRIPTOR_A_REGISTER_OFFSET);

    /*
    * Refer to OHCI spec 7.4.1 HcRhDescriptorA Register bit definition for NPS bit:
    * NoPowerSwitching
    * These bits are used to specify whether power switching is
    * supported or port are always powered. It is implementationspecific.
    * When this bit is cleared, the PowerSwitchingMode
    * specifies global or per-port switching.
    * 0: Ports are power switched
    * 1: Ports are always powered on when the HC is powered on
    */

    /*
     * Check whether the root hub supports  power switching
     */

    if ((uRegisterValue & USB_OHCI_RH_DESCRIPTOR_A_REGISTER__NPS_MASK) != 0)
        {
        /*
         * If the root hub always power on ports when HC is powered on, we can
         * do nothing to power off the ports.
         */
        return;
        }

    /*
     * Check whether the root hub supports ganged power on the down
     * stream ports (global power mode).
     */
    if ((uRegisterValue & USB_OHCI_RH_DESCRIPTOR_A_REGISTER__PSM_MASK) == 0)
        {
        /*
         * NOTE: Write 1 to the Local Power Status  (LPS) bit
         *       to clear the port power state for all the ports.
         */

        USB_OHCI_REG_WRITE(pHCDData->pDev,
                          USB_OHCI_RH_STATUS_REGISTER_OFFSET,
                           USB_OHCI_RH_STATUS__LPS);
        return;
        }

   /*
    * Read the contents of the root hub descriptor B
    * register
    */

   uRegisterValue =  USB_OHCI_REG_READ(pHCDData->pDev,
                             USB_OHCI_RH_DESCRIPTOR_B_REGISTER_OFFSET);

    /*
     * Check whether the Port Power Control Mask bit
     * is set for the specified port.
     *
     * a) If this bit is set, the port's power state
     *    is only affected by per-port power control
     *    (Set/ClearPortPower).
     * b) If this bit is cleared, th port is
     *    controlled by the global power switch
     *    (Set/ClearGlobalPower).
     */


    if (0 ==
        (uRegisterValue &
        USB_OHCI_HC_RH_DESCRIPTOR_B_REGISTER__PPCM_MASK))
        {
        /*
         * Port power state is controlled by the
         * global power switch
         */

        /*
         * NOTE: Write 1 to the Local Power Status (LPS)
         *       bit to clear the port power state for all the ports whose associated
         *       PPCM bits are set.
         */

        /*
         * Root hub supports ganged power. Power off
         * all the down stream ports whose PPCM bits are set (per-port power mode).
         */

        USB_OHCI_REG_WRITE(pHCDData->pDev,
                        USB_OHCI_RH_STATUS_REGISTER_OFFSET,
                         USB_OHCI_RH_STATUS__LPS);
        return;
        }


    /* Obtain the number of ports supported by the host controller (BEGIN) */

    /* uNumberOfDownstreamPorts is already obtaind above */

    /* Obtain the number of ports supported by the host controller (END) */

    /* Power on all the downstream ports */

    for (uIndex = 1; uIndex <= uNumberOfDownstreamPorts; uIndex++)
        {
        /* Obtain the base address of the port status register */

        uPortStatusRegisterOffset =
            USB_OHCI_RH_PORT_STATUS_REGISTER_OFFSET(uIndex);

        /* Read the contents of the port status register */

        uRegisterValue =
            USB_OHCI_REG_READ(pHCDData->pDev,
                              uPortStatusRegisterOffset);

        /*
         * NOTE: For HcRhPortStatus registers, read and write to them have
         * different meanings;
         * Also when writing '0' to these bits has no effects; thus we can
         * not use a read-modify-write procedue to set specific functional
         * bits. So we just need to set the desired bits in order to
         * perform some actions.
         */

        /*
         * NOTE: Write 1 to the Low Speed Device Attach
         * (LSDA) bit to clear the port power state.
         */

        /* Clear the port power state */

        USB_OHCI_REG_WRITE(pHCDData->pDev,
                           uPortStatusRegisterOffset,
                           USB_OHCI_RH_PORT_STATUS_LSDA);

        }

    return;
    } /* End of function usbOhciPowerOffPorts () */

/*******************************************************************************
*
* usbOhciDisable - routine to disable the OHCI controller
*
* This routine is called on a warm reboot. It power-offs all the port of the
* controller followed by resetting hte host controller.
*
* RETURNS: 0, always
*
* ERRNO: none
*
* \NOMANUAL
*/

int usbOhciDisable
    (
    int    startType
    )

    {
    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA   pHCDData = NULL;
    UINT8            index = 0;

    /* if the global array is NULL, return */

    if (g_OHCDData == NULL || initializedOHCDCount == 0)
        return 0;

    while (index < USB_MAX_OHCI_COUNT)
        {
        pHCDData = &g_OHCDData[index];

        if (pHCDData && pHCDData->pDev != NULL)
            {
            /* power off the ports */

            usbOhciPowerOffPorts (index);

            /* Reset the OHCI Controller */

            USB_OHCI_REG_WRITE (pHCDData->pDev,
                USB_OHCI_COMMAND_STATUS_REGISTER_OFFSET,
                USB_OHCI_COMMAND_STATUS_HCR);

#ifndef USB_OHCI_POLLING_MODE
            /* Unregister the IRQ */
            /* first disable then disconnect */

            (void) vxbIntDisable (pHCDData->pDev, 0, (VOIDFUNCPTR)usbOhciIsr,
                           (void *)(ULONG)(pHCDData));
            (void) vxbIntDisconnect (pHCDData->pDev, 0, (VOIDFUNCPTR)usbOhciIsr,
                              (void *)(ULONG)(pHCDData));

#endif
            }

        index++;
        }

    return 0;
    }

#ifdef NO_CACHE_COHERENCY
/***************************************************************************
* usbOhciRegWrite - write contents to a HC register
*
* This function is used to write the contents to a host controller register.
*
* RETURNS: None
*
* ERRNO:
*   None.
*
*\NOMANUAL
*/

VOID usbOhciRegWrite
    (
    VXB_DEVICE_ID    pDev,   /* pointer to the vxBus device structure */
    UINT32           offset, /* offset from  addressa to write */
    UINT32           value   /* Value to be written */
    )
    {

    USB_REG_WRITE32 (pDev,
                    (VOID *)((pUSB_OHCD_DATA)(pDev->pDrvCtrl))->regBase,
                    offset, value, 0);

    cacheFlush(DATA_CACHE,
        (pVOID)(((pUSB_OHCD_DATA)(pDev->pDrvCtrl))->regBase + offset),4);
    }

/***************************************************************************
*
* usbOhciRegRead - read contents of a HC register
*
* This function is used to read the contents of a host controller register.
*
* RETURNS: UINT32 value which indicates contents of the register
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/

UINT32 usbOhciRegRead
    (
    VXB_DEVICE_ID        pDev,    /* pointer to the vxBus device structure */
    UINT32               offset   /* offset of the address to read */
    )
    {
    cacheInvalidate(DATA_CACHE,
        (pVOID)(((pUSB_OHCD_DATA)(pDev->pDrvCtrl))->regBase + offset),4);

    /* read the value */

    return (USB_REG_READ32 (pDev,
        (VOID *)((pUSB_OHCD_DATA)(pDev->pDrvCtrl))->regBase,offset, 0));

    }

#endif /* End of NO_CACHE_COHERENCY */


/*******************************************************************************
*
* usbHcdOhciDeviceConnect - initialize the OHCI Host Controller Device
*
* This routine initializes the OHCI host controller and activates it to
* handle all USB operations. The function is called by vxBus on a successful
* driver - device match with VXB_DEVICE_ID as parameter. This structure has all
* information about the device. This routine resets all OHCI registers and
* then initializes them with default value. Once all the registers are properly
* initialized, it sets the host controller into the operational state.
* operations
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

LOCAL VOID usbHcdOhciDeviceConnect
    (
    VXB_DEVICE_ID               pDev            /* struct vxbDev */
    )
    {

    /* to store the status */

    BOOLEAN bStatus = TRUE;

    /* counter */

    UINT32     index = 0;

    /* to store the bus registration status */

    USBHST_STATUS   uUsbStatus = USBHST_SUCCESS;

    /*
     * Initialize the OHCI Controller information corresponding to the OHCI
     * Controllers found on the system. The following operations are
     * performed:
     *
     * a) Initialize the PCI IRQ Number
     * b) Initialize the Base Address of USB Registers
     * c) Reset the OHCI Host Controller
     * d) Initialize the OHCI Host Controller
     */

    /* To hold the pointer to the default pipe */

    pUSB_OHCD_PIPE pDefaultPipe = NULL;

    pUSB_OHCD_DATA   pHCDData = NULL;

    if (initializedOHCDCount == USB_MAX_OHCI_COUNT)
        {
        /* WindView Instrumentation */

        USB_HCD_LOG_EVENT(
            USB_OHCI_WV_INIT_EXIT,
            "usbHcdOhciDeviceConnect() - maximum count exceeded",
            USB_OHCD_WV_FILTER);

        /* Set the pDrvCtrl as NULL */

        pDev->pDrvCtrl = NULL;

        return;
        }

    /* initialize the vxBus device structure member */

    index = 0;

    while (index < USB_MAX_OHCI_COUNT)
        {
        if (g_OHCDData[index].pDev == NULL)
            break;
        else
            index ++;
        }

    if (index == USB_MAX_OHCI_COUNT)
        {
        USB_OHCD_ERR("usbHcdOhciDeviceConnect - Reached MAX OHCI count %d\n",
                     USB_MAX_OHCI_COUNT, 2, 3, 4, 5, 6);

        /* Set the pDrvCtrl as NULL */

        pDev->pDrvCtrl = NULL;

        return;
        }

    /* If this OHCI device is a PCI device, set its unitNumber */
    if( 0 == strncmp(pDev->pName, USB_OHCI_PCI_NAME, sizeof(USB_OHCI_PCI_NAME)))
        {
        vxbNextUnitGet(pDev);
        }

    initializedOHCDCount++;

    /* Call the function to initialize the OHCI Controller */

    bStatus = usbOhciInitializeHostController(pDev, (UINT8)index);

    /* Check whether the host controller was initialized successfully */

    if (!bStatus)
        {
        /* WindView Instrumentation */

        USB_HCD_LOG_EVENT(
            USB_OHCI_WV_INIT_EXIT,
            "usbHcdOhciDeviceConnect() - host controller initialization failed",
            USB_OHCD_WV_FILTER);

        initializedOHCDCount--;

        g_OHCDData[index].pDev = NULL;

        /* Set the pDrvCtrl as NULL */

        pDev->pDrvCtrl = NULL;

        return;
        }

    /* Obtain the pointer to the default pipe */

    pDefaultPipe = g_OHCDData[index].pDefaultPipe;

    /* Call the function to register the bus with USBD */

    uUsbStatus = usbHstBusRegister(hHostControllerDriver,
                                   USBHST_FULL_SPEED,
                                   (ULONG)pDefaultPipe,
                                   pDev);

    /* Check whether the bus was registered successfully */

    if (uUsbStatus != USBHST_SUCCESS)
        {
        USB_OHCD_ERR("usbHcdOhciDeviceConnect - "
            "failed to register the bus for host controller \n",
            1, 2, 3, 4, 5, 6);

        /* WindView Instrumentation */

        USB_HCD_LOG_EVENT(
            USB_OHCI_WV_INIT_EXIT,
            "usbHcdOhciDeviceConnect() - Bus registration failed",
            USB_OHCD_WV_FILTER);

        pHCDData = pDev->pDrvCtrl;

        usbOhciUnInitializeHostController(pHCDData);

        initializedOHCDCount--;

        g_OHCDData[index].pDev = NULL;

        /* Set the pDrvCtrl as NULL */

        pDev->pDrvCtrl = NULL;

        /* Return without proceeding */

        return;
        }
    }

/*******************************************************************************
*
* usbOhcdPipeClean - clean all the pipe
*
* This routine will clean all the pipe created for the pHCDData
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbOhcdPipeClean
    (
    pUSB_OHCD_DATA   pHCDData
    )
    {
    UINT32         uConCnt = 0;
    UINT32         uBlkCnt = 0;
    UINT32         uPerCnt = 0;
    UINT32         uIndex;
    UINT32         uHcIndex;
    UINT32         uBufferAddr;
    PUINT32        pInterruptTable = NULL;
    VXB_DEVICE_ID  pDev;
    pUSB_OHCD_PIPE pHCDPipe;
    pUSB_OHCD_PIPE pHCDNextPipe;

    if (pHCDData == NULL || pHCDData->pDev == NULL ||
        pHCDData->uBusIndex >= USB_MAX_OHCI_COUNT)
        {
        USB_OHCD_ERR("Invaild parameter of pHCDData\n",
                 1, 2, 3, 4, 5, 6);
        return;
        }

    pDev = pHCDData->pDev;
    uHcIndex = pHCDData->uBusIndex;

    OS_WAIT_FOR_EVENT(pHCDData->pipeListAccessEvent, WAIT_FOREVER);

    /* Delete all the control pipe */

    uBufferAddr = USB_OHCI_REG_READ (pDev,
                                     USB_OHCI_CONTROL_HEAD_ED_REGISTER_OFFSET);

    pHCDPipe = (pUSB_OHCD_PIPE)
          usbOhciPhyToVirt(pHCDData, uBufferAddr, USB_OHCI_MEM_MAP_ED);

    while (pHCDPipe != NULL)
        {
        uConCnt++;
        pHCDNextPipe = pHCDPipe->pHCDNextPipe;
        usbOhciDeletePipe((UINT8)uHcIndex, (ULONG)pHCDPipe);
        pHCDPipe = pHCDNextPipe;
        }

    /* Delete all the bulk pipe */

    uBufferAddr = USB_OHCI_REG_READ (pDev,
                                     USB_OHCI_BULK_HEAD_ED_REGISTER_OFFSET);

    pHCDPipe = (pUSB_OHCD_PIPE)
          usbOhciPhyToVirt(pHCDData, uBufferAddr, USB_OHCI_MEM_MAP_ED);

    while (pHCDPipe != NULL)
        {
        uBlkCnt++;
        pHCDNextPipe = pHCDPipe->pHCDNextPipe;
        usbOhciDeletePipe((UINT8)uHcIndex, (ULONG)pHCDPipe);
        pHCDPipe = pHCDNextPipe;
        }

    /* Delete all the periodic pipe if exist */

    pInterruptTable = (PUINT32)(pHCDData->pHcca);


    /*
     * Search for the endpoint descriptor in the list of periodic pipes
     * for the specified polling interval
     */

    for (uIndex = 0; uIndex < 32; uIndex++)
        {
        /*
         * Obtain the pointer to the head of the periodic list for
         * the specified polling interval.
         */

        uBufferAddr = pInterruptTable[uIndex];

        /* Get the bus address in CPU endian */

        uBufferAddr = USB_OHCD_SWAP_DATA(uHcIndex, uBufferAddr);

        pHCDPipe = (pUSB_OHCD_PIPE)
            usbOhciPhyToVirt(pHCDData, uBufferAddr, USB_OHCI_MEM_MAP_ED);

        while (pHCDPipe != NULL)
            {
            uPerCnt++;
            pHCDNextPipe = pHCDPipe->pHCDNextPipe;
            usbOhciDeletePipe((UINT8)uHcIndex, (ULONG)pHCDPipe);
            pHCDPipe = pHCDNextPipe;
            }
        }


    OS_RELEASE_EVENT(pHCDData->pipeListAccessEvent);

    /*
     * delete pipe will add the pipe into disable list, call the routine to
     * clean all the disable pipe
     */

    USB_OHCD_DBG("uConCnt %d, uBlkCnt %d, uPerCnt %d\n",
                 uConCnt, uBlkCnt, uPerCnt, 4, 5, 6);

    usbOhciCleanDisabledPipe(pHCDData);

    USB_OHCD_DBG("disable cont %d \n",
        lstCount(&pHCDData->disableEDList), 2, 3, 4, 5, 6);
    }

/*******************************************************************************
*
* usbHcdOhciDeviceRemove - removes the OHCI Host Controller device
*
* This function un-initializes the USB host controller device. The function
* is registered with vxBus and called when the driver is de-registered with
* vxBus. The function will have VXB_DEVICE_ID as its parameter. This structure
* will consists of all the information about the device. The function will
* subsequently de-register the bus from USBD also.
*
* RETURN : NA
*
* ERRNO: none
*/

LOCAL STATUS usbHcdOhciDeviceRemove
    (
    VXB_DEVICE_ID               pDev            /* struct vxbDev */
    )
    {

    /* To hold the pointer to the host controller information */

    pUSB_OHCD_DATA   pHCDData = NULL;

    /* To hold the status of deregistering the OHCI host controller driver */

    USBHST_STATUS           uUsbStatus = USBHST_SUCCESS;

    /* To hold the status of the host controller */

    UINT32                  uOhciControllerState = 0;

    UINT32                  count = 0;

    UINT32                  regValue;

    /* check for valid function parameters */

    if (pDev == NULL)
        {
        USB_OHCD_ERR("Invalid parameter of pDev\n", 1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    /* check if any host controller is initialized */

    if (initializedOHCDCount == 0)
        {
        USB_OHCD_ERR("No host controller is initialized\n", 1, 2, 3, 4, 5, 6);
        return ERROR;
        }
    /*
     * pDev->pDrvCtrl is NULL means we don't need to clean the pHCDData
     * we should return OK to indicate the device can be removed
     */

    if (pDev->pDrvCtrl == NULL)
        {
        USB_OHCD_DBG("usbHcdOhciDeviceRemove - pDev->pDrvCtrl is NULL\n",
                     1, 2, 3, 4, 5, 6);
        return OK;
        }

    /*
     * Stop all the host controllers and release the resources allocated
     * for them.
     */

    /* Obtain the pointer to the host controller information */

    pHCDData = pDev->pDrvCtrl;

    /* Check whether the host controller is initialized */

    if (TRUE == pHCDData->bHostControllerInitialized)
        {
        /* Call the function to deregister the bus */

        uUsbStatus = usbHstBusDeregister (
                        hHostControllerDriver,
                        pHCDData->uBusIndex,
                        (ULONG)pHCDData->pDefaultPipe);

        /* Check whether the bus was deregistered successfully */

        if (uUsbStatus != USBHST_SUCCESS)
            {
            /* Debug message */
            USB_OHCD_ERR("failed to deregister the bus for " \
                         "host controller, busIndex = %d \n",
                         pHCDData->uBusIndex, 2, 3, 4, 5, 6);
            /*
             * NOTE: If the deregisteration of the bus fails, it is likely
             *       that there are active devices on the bus. Hence, stop
             *       the cleanup operations for the bus.
             */
            }


        pHCDData->uRootHubState = USB_OHCI_DEVICE_DEFAULT_STATE;
        pHCDData->bCurrentConfigurationValue = 0;
        pHCDData->uNumberOfDownStreamPorts = 0;
        }

    else
        return ERROR;

    /* Read the contents of the HcControl register */

    regValue = USB_OHCI_REG_READ (pDev,
                               USB_OHCI_CONTROL_REGISTER_OFFSET);

    /* Disable the processing of the endpoint descriptor list */

    regValue &= ~(USB_OHCI_CONTROL_CLE |
                  USB_OHCI_CONTROL_BLE |
                  USB_OHCI_CONTROL_PLE |
                  USB_OHCI_CONTROL_IE);

    /* Update the contents of the HcControl register */

    USB_OHCI_REG_WRITE (pDev,
                        USB_OHCI_CONTROL_REGISTER_OFFSET,
                        regValue);

    /*
     * Check whether the default pipe is valid.
     *
     * NOTE: The default pipe should be deleted before the ISR is
     *       unregistered (OR) the polling mode ISR task is destroyed.
     *
     *       This is bacause the OHCI_DeletePipe() function synchronizes
     *       with the host controller before deleting the pipe. In order
     *       to synchronize the OHCI_DeletePipe() function depends on SOF
     *       interrupt.
     *
     *       Hence, the default pipe should be deleted when the host
     *       controller is in operational state.
     */

    if (pHCDData->pDefaultPipe != NULL)
        {
        /* Call the function to delete the default pipe */

        usbOhciDeletePipe ((UINT8) pHCDData->uBusIndex,
                           (ULONG) pHCDData->pDefaultPipe);

        /* Reset the pointer to the default pipe */

        pHCDData->pDefaultPipe = NULL;
        }

    /* Clean all the pipe */

    usbOhcdPipeClean(pHCDData);


#ifndef USB_OHCI_POLLING_MODE
    /* Disable the interrupts */

    USB_OHCI_REG_WRITE (pDev,
                USB_OHCI_INTERRUPT_DISABLE_REGISTER_OFFSET,
                 USB_OHCI_INTERRUPT_MASK);

    /*
     * Unregister the interrupt handler for the OHCI Controller.
     *
     * NOTE: First disable the interrupt and then disconnect
     */

    (void) vxbIntDisable (pDev,
                   0,
                   (VOIDFUNCPTR)usbOhciIsr,
                   (void *)(ULONG)(pHCDData));

    (void) vxbIntDisconnect (pDev,
                      0,
                      (VOIDFUNCPTR)usbOhciIsr,
                      (void *)(ULONG)(pHCDData));

#endif /* USB_OHCI_POLLING_MODE */

    pHCDData->isrMagic = USB_OHCD_MAGIC_DEAD;

    /* Reset the OHCI Controller */

    USB_OHCI_REG_WRITE (pDev,
                USB_OHCI_COMMAND_STATUS_REGISTER_OFFSET,
                USB_OHCI_COMMAND_STATUS_HCR);

    /*
     * Wait for the reset operation to complete. The reset operation should
     * be completed in 1 micro second. However, a delay of 1 milli second
     * is provided.
     *
     * NOTE: This extra delay will not create any performance issues.
     *       Since the host controller is being disabled, this delay is
     *       acceptable.
     */

    OS_DELAY_MS (USB_OHCI_WAIT_FOR_HOST_CONTROLLER_RESET_COMPLETION);

    while (count < NUM_RETRIES_TO_DETECT_CHANGE)
        {
        if (((USB_OHCI_REG_READ (pDev,
                           USB_OHCI_COMMAND_STATUS_REGISTER_OFFSET)) &
                           USB_OHCI_COMMAND_STATUS_HCR) == 0)
            {
            break;
            }
        else
            {
            count++;

            OS_DELAY_MS(1);
            }
        }

    if (count == NUM_RETRIES_TO_DETECT_CHANGE)
        {
        USB_OHCD_ERR("Reset failed\n", 1, 2, 3, 4, 5, 6);
        }


    /* Check the status of the OHCI Controller */

    uOhciControllerState =
            USB_OHCI_REG_READ (pDev,
                               USB_OHCI_CONTROL_REGISTER_OFFSET);

    /* Check whether the OHCI Controller is in SUSPEND state */

    if ((uOhciControllerState & USB_OHCI_CONTROL_HCFS_USB_SUSPEND) !=
            USB_OHCI_CONTROL_HCFS_USB_SUSPEND)
        {
        /* WindView Instrumentation */
        USB_HCD_LOG_EVENT(
                USB_OHCI_WV_INIT_EXIT,
                "usbOhcdExit() exits - Controller reset failed",
                USB_OHCD_WV_FILTER);

        /*
         * NOTE: There is no error recovery mechanism. Hence the error
         *       is ignored.
         */
        }

    /* Check whether the OHCI interrupt handler thread is valid */

    if (pHCDData->isrThreadId != OS_THREAD_FAILURE
        && pHCDData->isrThreadId != 0)
        {
        /* Delete the OHCI interrupt handler thread */

        OS_DESTROY_THREAD (pHCDData->isrThreadId);

        /* Reset the OHCI interrupt handler thread ID */

        pHCDData->isrThreadId = OS_THREAD_FAILURE;
        }

#ifndef USB_OHCI_POLLING_MODE

    /* Check whether the ISR event was created successfully */

    if (OS_INVALID_EVENT_ID != pHCDData->isrEvent)
        {
        /* Call the function to delete the ISR event */

        OS_DESTROY_EVENT (pHCDData->isrEvent);

        /* Reset the ISR event pointer */

        pHCDData->isrEvent = OS_INVALID_EVENT_ID;
        }
#endif

    /* Check whether the endpoint list access event is valid */

    if (pHCDData->pipeListAccessEvent != OS_INVALID_EVENT_ID)
        {
        /* Call the function to delete the endpoint list access event */

        OS_DESTROY_EVENT (pHCDData->pipeListAccessEvent);

        /* Reset the endpoint list access event pointer */

        pHCDData->pipeListAccessEvent = OS_INVALID_EVENT_ID;
        }

    /* Delete the common semphore */

    if (pHCDData->ohciCommonMutex != NULL)
        {
        /* Should take the mutex before delete it */

        OS_WAIT_FOR_EVENT(pHCDData->ohciCommonMutex, WAIT_FOREVER);
        OS_DESTROY_EVENT(pHCDData->ohciCommonMutex);
        pHCDData->ohciCommonMutex = NULL;
        }

     /* Destroy ED dma tag */

    if (pHCDData->edDmaTagId)
        {
        (void) vxbDmaBufTagDestroy(pHCDData->edDmaTagId);
        pHCDData->edDmaTagId = NULL;
        }

    /* Destroy TD dma tag */

    if (pHCDData->tdDmaTagId)
        {
        (void) vxbDmaBufTagDestroy(pHCDData->tdDmaTagId);
        pHCDData->tdDmaTagId = NULL;
        }

    /* Free HCCA */

    if (NULL != pHCDData->pHcca)
        {
        /*
         * Free the memory allocated for the Periodic Frame List.
         *
         * If pHCDData->pHcca is non-NULL, then we are sure
         * pHCDData->hccaDmaTagId and pHCDData->hccaDmaMapId
         * have already been created, thus no need to check them for NULL.
         */

        (void) vxbDmaBufMemFree(pHCDData->hccaDmaTagId,
                         pHCDData->pHcca,
                         pHCDData->hccaDmaMapId);
        }

    /* Destroy hcca dma tag */

    if (pHCDData->hccaDmaTagId)
        {
        (void) vxbDmaBufTagDestroy(pHCDData->hccaDmaTagId);
        pHCDData->hccaDmaTagId = NULL;
        }

    /* Call vxbRegUnmap unmap the register */

    if (0 != pHCDData->regBase)
        vxbRegUnmap (pHCDData->pDev, pHCDData->regIndex);

    /* Reset the flag to indicate the host controller is not initialized */

    pHCDData->bHostControllerInitialized = FALSE;

    pDev->pDrvCtrl = NULL;

    /* remove the reference of the vxBus device structure */

    pHCDData->pDev = NULL;

    /* decrement the count of the initialized host controller */

    initializedOHCDCount--;

    return OK;

    }

/*******************************************************************************
*
* usbHcdOhciDeviceInit - legacy support implementation
*
* This function implements the legacy support for OHCI Controller. This
* function handles the handoff of USB OHCI Controller from BIOS/SMM to System
* Software. Either SMM driver or the BIOS would be active at a time. Both cannot
* be active simutaneously. The OHCI control register tells us which is active.
* The function checks which driver is active and performs the handoff to OS
* driver accordingly. The handoff procedure is specified in the OHCI
* specification section 5.1.1.3.
*
* RETURNS: OK or ERROR
*
* ERRNO: N/A
*/

LOCAL STATUS usbHcdOhciDeviceInit
    (
    VXB_DEVICE_ID       pDev                    /* struct vxbDev * */
    )
    {

    /* to store the control register value */

    UINT32              hcControlValue = 0;

    /* to store the command status register value */

    UINT32              hcCommandStatus = 0;

    /* to store the counter value */

    UINT32              count = 0;

    /* to store the count value */

    /*
     * there can be three conditions according to the specifications
     * 1. OS driver SMM active
     * 2. OS driver BIOS active
     * 3. OS driver neither SMM nor BIOS active
     */

    /*
     * OS driver SMM active the InterruptRouting bit is set if SMM is active in
     * the HcControl register
     */

    /* read the HcControlRegister of the host controller */

    hcControlValue = USB_OHCI_REG_READ (pDev, USB_OHCI_CONTROL_REGISTER_OFFSET);

    /* check is InterruptRouting bit in the HcControl Register is set */

    if ((hcControlValue & USB_OHCI_INTERRUPT_ROUTING_CONTROL_REGISTER) ==
        USB_OHCI_INTERRUPT_ROUTING_CONTROL_REGISTER)
        {

        /* read the HcCommandStatus register */

        hcCommandStatus = USB_OHCI_REG_READ (pDev,
                                      USB_OHCI_COMMAND_STATUS_REGISTER_OFFSET);

        /* write one to ownership change request */

        hcCommandStatus |= USB_OHCI_CHANGE_OWNERSHIP;

        /* Perform the actual write */

        USB_OHCI_REG_WRITE (pDev,
                           USB_OHCI_COMMAND_STATUS_REGISTER_OFFSET,
                           hcCommandStatus);

        /* monitor the InterruptRouting bit to be reset */

        while (count < NUM_RETRIES_TO_DETECT_CHANGE)
            {

            /* read the HcControl register */

            hcControlValue = USB_OHCI_REG_READ (pDev,
                                             USB_OHCI_CONTROL_REGISTER_OFFSET);

            /* if the InterruptRouting bit is reset control has been handled to
             * System software
             */

            if (!(hcControlValue & USB_OHCI_INTERRUPT_ROUTING_CONTROL_REGISTER))
                break;

            else
                {
                OSS_THREAD_SLEEP (WAIT_TO_REFLECT_OWNERSHIP_CHANGE);
                count ++;
                }
            }

        if (count == NUM_RETRIES_TO_DETECT_CHANGE)
            {
            /* pDev can not be freed here, it should be freed by vxBus */

            return ERROR;
            }

        }

    else
        {

        /*
         * check for OS driver BIOS active
         * the HostControllerFunctionalState (HCFS) should not USB_RESET
         * in HcControl register the HCFS are bit numbers 6 and 7
         * USB_RESET state is 00
         */

        if ((hcControlValue & USB_OHCI_HCFS_CONTROL_REGISTER) !=
            USB_OHCI_HCFS_USB_RESET)
            {

            /* check for host controller operational state */

            if ((hcControlValue & USB_OHCI_HCFS_CONTROL_REGISTER) ==
                USB_OHCI_HCFS_USB_OPERATIONAL)
                {
                /* if BIOS is active and HCFS is USB_OPERATIONAL setup the
                 * host controller. The host controller initialization is
                 * done later so return from the function
                 */
                return OK;
                }

            /*
             * if host controller is not operational perform the following
             */

            /* set the HCFS to USB RESUME */

            hcControlValue = (hcControlValue & USB_OHCI_HCFS_CHANGE_MASK) |
                              USB_OHCI_HCFS_USB_RESUME;

            /*
             * So to resume, we are allowed to write to control
             * register and set the state.  We need to make sure we
             * preserve the other bit settings.
             */

            USB_OHCI_REG_WRITE (pDev,
                               USB_OHCI_CONTROL_REGISTER_OFFSET,
                               hcControlValue);

            /* wait for the resume to complete */

            OSS_THREAD_SLEEP (WAIT_FOR_RESUME_TO_COMPLETE);

            return OK;

            }

        /* OS driver neither SMM nor BIOS driver */

        else
            {

            /* wait for resume to complete */

            OSS_THREAD_SLEEP(WAIT_FOR_RESUME_TO_COMPLETE);
            }
        }
    return OK;
    }

/*******************************************************************************
*
* vxbUsbOhciRegister - register OHCI driver with vxBus
*
* This routine registers the OHCI Driver with vxBus. The registration is
* done for both PCI and Local bus type by calling the routine vxbDevRegister().
*
* Once the OHCI driver is registered, this routine also registers the
* OHCI Root hub as bus-controller type with vxBus
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

void vxbUsbOhciRegister (void)
    {
    /*
     * Register the OHCI driver for PCI bus as underlying bus
     */

    if (vxbDevRegister ((DRIVER_REGISTRATION *)
        &usbVxbPciHcdOhciDevRegistration)
        == ERROR)
        {
        USB_OHCD_ERR("vxbUsbOhciRegister - "
            "error registering OHCI driver over PCI Bus \n",
                    1, 2, 3, 4, 5, 6);
        return;
        }

    /* Register the OHCI driver for PLB bus as underlying bus */

    if (vxbDevRegister ((DRIVER_REGISTRATION *)
        &usbVxbPlbHcdOhciDevRegistration)
        == ERROR)
        {
        USB_OHCD_ERR("vxbUsbOhciRegister - "
            "error registering OHCI Driver over PLB Bus \n",
                    1, 2, 3, 4, 5, 6);
        return;
        }

    /* Register the OHCI driver for root hub */

    if (vxbDevRegister ((DRIVER_REGISTRATION *)
        &usbVxbHcdOhciHub)
        == ERROR)
        {
        USB_OHCD_ERR("vxbUsbOhciRegister - "
            "error registering OHCI root hub\n",
                    1, 2, 3, 4, 5, 6);
        return;
        }

    return;
    }


/*******************************************************************************
*
* vxbUsbOhciDeregister - de-registers OHCI driver with vxBus
*
* This routine de-registers the OHCI Driver with vxBus. The routine
* first de-registers the OHCI Root hub. This is followed by deregistration of
* OHCI Controller for PCI and PLB bus types
*
* RETURNS: OK or ERROR; if not able to register with vxBus
*
* ERRNO: none
*/

LOCAL STATUS vxbUsbOhciDeregister (void)
    {

    /* de- register the OHCI root hub as bus controller driver */

    if (vxbDriverUnregister (&usbVxbHcdOhciHub) == ERROR)
        {
        USB_OHCD_ERR("error de-registering OHCI root hub with vxBus \n",
                     1, 2, 3, 4, 5, 6);
        return ERROR;
        }


    /* de- register the OHCI driver for PCI bus as underlying bus */

    if (vxbDriverUnregister ((DRIVER_REGISTRATION *) &usbVxbPciHcdOhciDevRegistration)
        == ERROR)
        {
        USB_OHCD_ERR("Error de-registering OHCI Driver over PCI Bus\n",
                     1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    /* de-register the OHCI driver for PLB bus as underlying bus */

    if (vxbDriverUnregister (&usbVxbPlbHcdOhciDevRegistration) == ERROR)
        {
        USB_OHCD_ERR("Error de-registering OHCI Driver over PLB Bus\n",
                     1, 2, 3, 4, 5, 6);
        return ERROR;
        }


    return OK;
    }

/*******************************************************************************
*
* usbVxbHcdOhciDeviceProbe - determines whether matching device is OHCI
*
* This routine determines whether the matching device is an OHCI Controller or
* not. For PCI device type, the routine will probe the PCI Configuration Device
* for Bus ID, Device Id and Function ID to determine wheter the device is of
* OHCI type.
* For PLC bus it will porbe for vendor id and device id.
* If a matching device is found, the routine will return TRUE.
*
* RETURNS: TRUE or FALSE
*
* ERRNO: none
*/

LOCAL BOOL usbVxbHcdOhciDeviceProbe
    (
    VXB_DEVICE_ID       pDev
    )

    {
    UINT32              busId = 0;      /* bus id of parent bus */

    UINT32              pciConfigInfo = 0;/* buffer for holding pci configuration
                                           * space information
                                           */

    /* validate the paramter */

    if ((pDev == NULL) || (pDev->pParentBus == NULL)
        || (pDev->pParentBus->pBusType == NULL))
        return FALSE;


    /* determine the bus type of parent bus */

    busId = pDev->pParentBus->pBusType->busID;

    switch (busId)
        {
        case VXB_BUSID_PCI :

            /*
             * PCI Type Device
             * read the config space using access functions
             */

            VXB_PCI_BUS_CFG_READ (pDev, 0x08, 4, pciConfigInfo);
            /*
             * to determine whether the device is the OHCI host controller the
             * bytes read from configuration space should be 0C0310h
             * 23:16 is Base Class Code should be 0Ch (serial bus controller
             * indication)
             * 15:8 is Sub Class Code should be 03h (universal serial bus
             * controller indication)
             * 7:0 is Programming interface should be 10h indicating USB OHCI Host
             * Controller that conforms to this specification
             */

            if (((pciConfigInfo & 0xFF000000) == USB_OHCI_BASEC) &&
               ((pciConfigInfo  & 0x00FF0000) == USB_OHCI_SCC) &&
               ((pciConfigInfo  & 0x0000FF00) == USB_OHCI_PI))
            {

           /* matching device found */
            printk("%s %d :device match\n",__FUNCTION__,__LINE__);
            return TRUE;
            }
            break;
        case VXB_BUSID_PLB :

            /*
             * PLB Type Device
             * return TRUE
             */

            return TRUE;

        default :
            return FALSE;

        }
        return FALSE;

    }

/*******************************************************************************
*
* usbVxbOhciNullFunction - dummy routine
*
* RETURNS: N/A
*
* ERRNO: none
*/

LOCAL VOID usbVxbOhciNullFunction
    (
    VXB_DEVICE_ID       pDev
    )
    {
    /* this is a dummy routine which simply returns */

    return;
    }

/* End of File usbOhci.c */
