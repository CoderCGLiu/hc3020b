/* usbEhcdRhEmulation.c - USB EHCI HCD Roothub Emulation */

/*
 * Copyright (c) 2003-2005, 2007-2011, 2013-2014 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2003-2005, 2007-2011, 2013-2014 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
Modification history
--------------------
02b,25feb14,wyy  increase delay when port reset not finished (VXW6-73538)
02a,15nov13,wyy  reduce delay time between to clean and to set USBCMD[RS] for 
                 errata USB-A003 (WIND00442812)
01z,03nov13,wyy  check NULL pointer before using it in usbEhcdRHCancelURB
                 function (WIND00440390)
01y,25sep13,wyy  add workaround for P1010 and p1020 chip Errata USB-A003 
                 (WIND00436313)
01x,12sep13,ljg  Add workaround for P2020 and P2010 Chip Errata USB-A003 
                 (WIND00434281)
01w,06jan13,ljg  Modify root hub port status pointer (WIND00364050)
01v,22jun11,ghs  Update URB status after process request (WIND00268641)
01u,27may10,w_x  Handle over ownership when has companion chip (WIND00212469)
01t,18may10,w_x  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
01s,09mar10,j_x  Changed for USB debug (WIND00184542)
01r,13jan10,ghs  vxWorks 6.9 LP64 adapting
01q,27sep09,y_l  Fix bug: missing semaphore release operation (WIND00183805)
01p,04feb09,w_x  Added support for FSL quirky EHCI with various register and
                 descriptor endian format (such as MPC5121E)
01o,16jul08,w_x  Added non-standard root hub TT support (WIND00127895)
01n,07jul08,w_x  Code coverity changes (WIND00126885)
01m,04sep07,ami  Changes to support Non-PCI based controller
01l,27jul07,jrp  WIND00099649 - USB documentation changes
01k,21sep05,dtr  Remove extra reset after fix for SPR111361.
01j,13sep05,dtr  Fix for SPR 111361.
01i,28mar05,pdg  non-PCI changes
01h,04feb05,pdg  Fix for SPR #104950(USB STACK 2.1 FROM PID 2.1 ISN'T
                 CORRECTLY SUPPORTING INTEL 82801DB/DBM EHCI HOST CONTROLLER)
01g,03dec04,ami  Merged IP Changes
01f,26oct04,ami  Severity Changes for Debug Messages
01e,15oct04,ami  Apigen Changes
01d,23Jul03,???  Incorporated the changes after testing on MIPS.
01c,12Jul03,gpd  updated status to USBHST_SUCCESS in setconfig request.
01b,26jun03,psp  changing the code to WRS standards.
01a,25apr03,ram  written.
*/

/*
DESCRIPTION

This contains functions which handle the requests to the Root hub

INCLUDE FILES:  usb2/usbOsal.h usb2/usbHst.h usb2/usbEhcdDataStructures.h
                usb2/usbEhcdRhEmulation.h usb2/usbEhcdHal.h usb2/usbEhcdUtil.h
                usb2/usbHcdInstr.h
*/


/*
INTERNAL
 *******************************************************************************
 * Filename         : usbEhcdRhEmulate.c
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      :  This contains functions which handle the requests to
 *                     the Root hub.
 *
 *
 ******************************************************************************/
#include "usb/usbOsal.h"
#include "usb/usbHst.h"
#include "usbEhcdDataStructures.h"
#include "usbEhcdRhEmulation.h"
#include "usbEhcdHal.h"
#include "usbEhcdUtil.h"
#include "usb/usbHcdInstr.h"

/* globals */

/* Device Descriptor value for the Root hub */
UCHAR gRHDeviceDescriptor[] = {
                                 0x12,        /* bLength */
                                 0x01,        /* Device Descriptor type */
                                 0x00, 0x02,  /* bcdUSB - USB 2.0 */
                                 0x09,        /* Hub DeviceClass */
                                 0x00,        /* bDeviceSubClass */
                                 0x01,        /* bDeviceProtocol High Speed single TT support*/
                                 0x40,        /* Max packet size is 64 */
                                 0x00, 0x00,  /* idVendor */
                                 0x00, 0x00,  /* idProduct */
                                 0x00, 0x00,  /* bcdDevice */
                                 0x00,        /* iManufacturer */
                                 0x00,        /* iProduct */
                                 0x00,        /* iSerialNumber */
                                 0x01         /* 1 configuration */
                                };

/*
 * Descriptor structure returned on a request for
 * Configuration descriptor for the Root hub.
 * This includes the Configuration descriptor, interface descriptor and
 * the endpoint descriptor
 */

UCHAR gRHConfigDescriptor[] = {
                                /* Configuration descriptor */
                                0x09,        /* bLength */
                                0x02,        /*
                                              * Configuration descriptor
                                              * type
                                              */
                                0x19, 0x00,  /* wTotalLength */
                                0x01,        /* 1 interface */
                                0x01,        /* bConfigurationValue */
                                0x00,        /* iConfiguration */
                                0xE0,        /* bmAttributes */
                                0x00,        /* bMaxPower */

                                /* Interface Descriptor */
                                0x09,        /* bLength */
                                0x04,        /*
                                              * Interface Descriptor
                                              * type
                                              */
                                0x00,        /* bInterfaceNumber */
                                0x00,        /* bAlternateSetting */
                                0x01,        /* bNumEndpoints */
                                0x09,        /* bInterfaceClass */
                                0x00,        /* bInterfaceSubClass */
                                0x00,        /* bInterfaceProtocol */
                                0x00,        /* iInterface */

                                /* Endpoint Descriptor */
                                0x07,        /* bLength */
                                0x05,        /*
                                              * Endpoint Descriptor
                                              * Type
                                              */
                                0x81,         /* bEndpointAddress */
                                0x03,         /* bmAttributes */
                                0x08,   0x00, /* wMaxPacketSize */
                                0x0C          /* bInterval - 255 ms*/
                              };


extern pUSB_EHCD_DATA *g_pEHCDData;


/* Function to process a control transfer request */

USBHST_STATUS
usbEhcdRhProcessControlRequest(pUSB_EHCD_DATA pHCDData,
                               pUSBHST_URB    pURB);

/* Function to process an interrupt transfer request */
USBHST_STATUS
usbEhcdRhProcessInterruptRequest(pUSB_EHCD_DATA pHCDData,
                                 pUSBHST_URB    pURB);

/* Function to process a standard request */
USBHST_STATUS
usbEhcdRhProcessStandardRequest(pUSB_EHCD_DATA pHCDData,
                                pUSBHST_URB    pURB);

/* Function to process a class specific request */
USBHST_STATUS
usbEhcdRhProcessClassSpecificRequest(pUSB_EHCD_DATA pHCDData,
                                     pUSBHST_URB    pURB);

USBHST_STATUS usbEhcdRhClearPortFeature(pUSB_EHCD_DATA   pHCDData,
                                        pUSBHST_URB     pURB);

USBHST_STATUS usbEhcdRhGetHubDescriptor(pUSB_EHCD_DATA   pHCDData,
                                        pUSBHST_URB     pURB);

USBHST_STATUS usbEhcdRhGetPortStatus(pUSB_EHCD_DATA   pHCDData,
                                     pUSBHST_URB  pURB);

USBHST_STATUS usbEhcdRhSetPortFeature(pUSB_EHCD_DATA   pHCDData,
                                      pUSBHST_URB  pURB);


/***************************************************************************
*
* usbEhcdRhCreatePipe - creates a pipe specific to an endpoint.
*
* This routine creates a pipe specific to an endpoint.
*
* RETURNS:
*  USBHST_SUCCESS - if the pipe was created successfully.
*  USBHST_INVALID_PARAMETER if the parameters are not valid.
*  USBHST_INSUFFICIENT_MEMORY if the memory allocation for the pipe failed.
*
* ERRNO:
*   None.
*/

USBHST_STATUS usbEhcdRhCreatePipe
    (
    pUSB_EHCD_DATA pHCDData,           /* Ptr to HCD block           */
    UINT8   uDeviceAddress,            /* Device Address             */
    UINT8   uDeviceSpeed,              /* Device Speed               */
    UCHAR  *pEndpointDescriptor,       /* Ptr to EndPoint Descriptor */
    ULONG  *puPipeHandle               /* Ptr to pipe handle         */
    )
    {

    /* To hold the status of the function call */

    USBHST_STATUS Status = USBHST_FAILURE;

    /* Pointer to the Endpoint Descriptor */

    pUSBHST_ENDPOINT_DESCRIPTOR pEndpointDesc = NULL;

    /* Check the validity of the parameters */

    if ((NULL == pHCDData) ||
        (USB_EHCD_MAX_DEVICE_ADDRESS < uDeviceAddress) ||
        (NULL == pEndpointDescriptor) ||
        (NULL == puPipeHandle) ||
        (USBHST_HIGH_SPEED != uDeviceSpeed))
        {
        USB_EHCD_ERR(
            "usbEhcdRhCreatePipe - parameters are not valid\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the endpoint descriptor */

    pEndpointDesc = (pUSBHST_ENDPOINT_DESCRIPTOR)pEndpointDescriptor;

    /* Switch based on the endpoint type */

    switch(pEndpointDesc->bmAttributes & USB_EHCD_ENDPOINT_TYPE_MASK)
        {
        case USBHST_CONTROL_TRANSFER:/* Control endpoint */
            {
            /* If the endpoint number is not 0, it is an error */

            if (0 != (pEndpointDesc->bEndpointAddress &
                USB_EHCD_ENDPOINT_NUMBER_MASK))
                {
                return USBHST_INVALID_REQUEST;
                }
            else
                {

                /* Allocate memory for the control endpoint */

                pHCDData->RHData.pControlPipe =
                                (pUSB_EHCD_PIPE)OS_MALLOC(sizeof(USB_EHCD_PIPE));

                /* Check if memory allocation is successful */

                if (NULL == pHCDData->RHData.pControlPipe)
                    {
                    USB_EHCD_ERR(
                        "usbEhcdRhCreatePipe - "
                        "Memory not allocated for control pipe\n",
                        0, 0, 0, 0, 0, 0);

                    return USBHST_INSUFFICIENT_MEMORY;
                    }

                /* Initialize the memory allocated */

                USB_EHCD_PIPE_INITIALIZE(pHCDData->RHData.pControlPipe);

                /* Populate the fields of the control pipe - Start */

                /* Copy the endpoint address */

                pHCDData->RHData.pControlPipe->uEndpointAddress =
                                            pEndpointDesc->bEndpointAddress;

                /* Copy the address */

                pHCDData->RHData.pControlPipe->uAddress = uDeviceAddress;

                /* Update the speed */

                pHCDData->RHData.pControlPipe->uSpeed = USBHST_HIGH_SPEED;

                /* Update the endpoint type */

                pHCDData->RHData.pControlPipe->uEndpointType =
                                                USBHST_CONTROL_TRANSFER;

                /* Populate the fields of the control pipe - End */

                /* Update the pipe handle information */

                *(puPipeHandle) = (ULONG)pHCDData->RHData.pControlPipe;

                Status = USBHST_SUCCESS;
                }
            break;
            }
        case USBHST_INTERRUPT_TRANSFER:/* Interrupt endpoint */
            {

            /* Allocate memory for the interrupt endpoint */

            pHCDData->RHData.pInterruptPipe =
                            (pUSB_EHCD_PIPE)OS_MALLOC(sizeof(USB_EHCD_PIPE));

            /* Check if memory allocation is successful */

            if (NULL == pHCDData->RHData.pInterruptPipe)
                {
                USB_EHCD_ERR(
                    "usbEhcdRhCreatePipe - "
                    "Memory not allocated for interrupt pipe\n",
                    0, 0, 0, 0, 0, 0);

                return USBHST_INSUFFICIENT_MEMORY;
                }

            /* Initialize the memory allocated */

            USB_EHCD_PIPE_INITIALIZE(pHCDData->RHData.pInterruptPipe);

            /* Populate the fields of the interrupt pipe - Start */

            /* Copy the endpoint address */

            pHCDData->RHData.pInterruptPipe->uEndpointAddress =
                                        pEndpointDesc->bEndpointAddress;

            /* Copy the address */

            pHCDData->RHData.pInterruptPipe->uAddress = uDeviceAddress;

            /* Update the speed */

            pHCDData->RHData.pInterruptPipe->uSpeed = USBHST_HIGH_SPEED;

            /* Update the endpoint type */

            pHCDData->RHData.pInterruptPipe->uEndpointType =
                                            USBHST_INTERRUPT_TRANSFER;

            /* Populate the fields of the interrupt pipe - End */

            /* Update the pipe handle information */

            *(puPipeHandle) = (ULONG)pHCDData->RHData.pInterruptPipe;

            Status = USBHST_SUCCESS;
            break;
            }
        default:
            {
            Status = USBHST_INVALID_REQUEST;
            }
    }
    /* End of switch */

    return Status;

    }
    /* End of function usbEhcdRhCreatePipe() */

/***************************************************************************
*
* usbEhcdRHDeletePipe - deletes a pipe specific to an endpoint.
*
* This routine deletes a pipe specific to an endpoint.
*
* RETURNS:
*  USBHST_SUCCESS if the pipe was deleted successfully.
*  USBHST_INVALID_PARAMETER if the parameters are not valid.
*
* ERRNO:
*   None.
*/

USBHST_STATUS usbEhcdRHDeletePipe
    (
    pUSB_EHCD_DATA pHCDData,           /* Ptr to HCD block             */
    ULONG          uPipeHandle         /* Pipe Handle Identifier       */
    )
    {
    /* To hold the pointer to the pipe */

    pUSB_EHCD_PIPE  pHCDPipe = NULL;

    /* Check the validity of the parameters */

    if ((NULL == pHCDData) ||
        (0 == uPipeHandle))
        {
        USB_EHCD_ERR(
            "usbEhcdRHDeletePipe - Parameters not valid\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the USB_EHCD_PIPE pointer */

    pHCDPipe = (pUSB_EHCD_PIPE)uPipeHandle;

    /* Check if it is a control pipe delete request */

    if (pHCDPipe == pHCDData->RHData.pControlPipe)
        {
        OS_FREE(pHCDData->RHData.pControlPipe);
        pHCDData->RHData.pControlPipe = NULL;
        }

    /* Check if it is an interrupt pipe delete request */

    else if (pHCDPipe == pHCDData->RHData.pInterruptPipe)
        {

        /* Pointer to the request information */

        pUSB_EHCD_REQUEST_INFO pRequestInfo = NULL;

        /* Pointer to the temporary request information */

        pUSB_EHCD_REQUEST_INFO pTempRequestInfo = NULL;

        /* Exclusively access the list */

        OS_WAIT_FOR_EVENT(pHCDData->RequestSynchEventID, OS_WAIT_INFINITE);

        /* Set the flag indicating that the pipe is deleted */

        pHCDPipe->PipeDeletedFlag = TRUE;

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDData->RequestSynchEventID);

        /* Remove the requests queued for the endpoint */

        for (pRequestInfo = pHCDPipe->pRequestQueueHead;
             NULL != pRequestInfo;
             pRequestInfo = pTempRequestInfo)
            {

            /* Assert if URB is not valid */

            OS_ASSERT(NULL != pRequestInfo->pUrb);

            /* Hold the next pointer temporarily */

            pTempRequestInfo = pRequestInfo->pNext;

            /* Update the request status to cancelled */

            pRequestInfo->pUrb->nStatus = USBHST_TRANSFER_CANCELLED;

            /* Call the callback functions if registered */

            if (NULL != pRequestInfo->pUrb->pfCallback)
                {
                pRequestInfo->pUrb->pfCallback(pRequestInfo->pUrb);
                }

            /* Free memory allocated for the request information */

            OS_FREE(pRequestInfo);
            }

        OS_FREE(pHCDData->RHData.pInterruptPipe);
        pHCDData->RHData.pInterruptPipe = NULL;
        }
    else
        {
        USB_EHCD_ERR(
            "usbEhcdRHDeletePipe - Invalid pipe handle\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    return USBHST_SUCCESS;
    }
    /* End of usbEhcdRHDeletePipe() */


/*******************************************************************************
*
* usbEhcdRHSubmitURB - submits a request to an endpoint.
*
* This routine submits a request to an endpoint.
*
* RETURNS:
*  USBHST_SUCCESS if the URB is submitted successfully.
*  USBHST_INVALID_PARAMETER if the parameters are not valid.
*  USBHST_INSUFFICIENT_BANDWIDTH if memory is insufficient for the request.
*
* ERRNO:
*   None.
*/

USBHST_STATUS usbEhcdRHSubmitURB
    (
    pUSB_EHCD_DATA pHCDData,           /* Ptr to HCD block             */
    ULONG          uPipeHandle,        /* Pipe Handle Identifier       */
    pUSBHST_URB    pURB                /* Ptr to User Request Block    */
    )
    {

    /* Status of the request */

    USBHST_STATUS Status = USBHST_FAILURE;

    /* Check if the parameters are valid */

    if (NULL == pHCDData ||
        0 == uPipeHandle ||
        NULL == pURB)
        {
        USB_EHCD_ERR(
            "usbEhcdRHSubmitURB - Invalid parameters\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Check if it is a control request */

    if (uPipeHandle == (ULONG)(pHCDData->RHData.pControlPipe) ||
        ((uPipeHandle == (ULONG)pHCDData->pDefaultPipe) &&
        (0 == pHCDData->RHData.uDeviceAddress)))
        {
        Status = usbEhcdRhProcessControlRequest(pHCDData, pURB);
        }

    /* Check if it is an interrupt request */

    else if (uPipeHandle == (ULONG)(pHCDData->RHData.pInterruptPipe))
        {
        Status = usbEhcdRhProcessInterruptRequest(pHCDData, pURB);
        }
    else
        {
        USB_EHCD_ERR(
            "usbEhcdRHSubmitURB - Invalid pipe handle\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    return Status;
    }
    /* End of usbEhcdRHSubmitURB() */

/***************************************************************************
*
* usbEhcdRhProcessControlRequest - processes a control transfer request
*
* This routine processes a control transfer request.
*
* RETURNS:
*  USBHST_SUCCESS if the URB is submitted successfully.
*  USBHST_INVALID_PARAMETER if the parameters are not valid.
*  USBHST_INSUFFICIENT_BANDWIDTH if memory is insufficient for the request.
*
* ERRNO:
*   None.
*/

USBHST_STATUS usbEhcdRhProcessControlRequest
    (
    pUSB_EHCD_DATA pHCDData,           /* Ptr to HCD block           */
    pUSBHST_URB    pURB                /* Ptr to User Request Block  */
    )
    {

    /* Status of the request */

    USBHST_STATUS Status = USBHST_FAILURE;

    /* Pointer to the setup packet */

    pUSBHST_SETUP_PACKET pSetup = NULL;

    /* Check the validity of the parameters */

    if (NULL == pHCDData ||
        NULL == pURB ||
        NULL == pURB->pTransferSpecificData)
        {
        USB_EHCD_ERR("usbEhcdRhProcessControlRequest - Invalid parameters\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the setup packet from the URB */

    pSetup = (pUSBHST_SETUP_PACKET)pURB->pTransferSpecificData;

    /* Check if it is a standard request */

    if (USB_EHCD_RH_STANDARD_REQUEST ==
                            (USB_EHCD_RH_REQUEST_TYPE & pSetup->bmRequestType))
        {
        Status = usbEhcdRhProcessStandardRequest(pHCDData, pURB);
        }

    /* Check if it is a class specific request */

    else if (USB_EHCD_RH_CLASS_SPECIFIC_REQUEST ==
                        (USB_EHCD_RH_REQUEST_TYPE & pSetup->bmRequestType))
        {
        Status = usbEhcdRhProcessClassSpecificRequest(pHCDData, pURB);
        }
    else
        {
        USB_EHCD_ERR("usbEhcdRhProcessControlRequest - Invalid request\n",
            0, 0, 0, 0, 0, 0);
        Status = USBHST_INVALID_PARAMETER;
        }

    return Status;
    }
    /* End of function usbEhcdRhProcessControlRequest() */

/***************************************************************************
*
* usbEhcdRhProcessInterruptRequest - processes a interrupt transfer request
*
* This routine processes a interrupt transfer request.
*
* RETURNS:
* USBHST_SUCCESS if the URB is submitted successfully.
* USBHST_INVALID_PARAMETER if the parameters are not valid.
* USBHST_INSUFFICIENT_BANDWIDTH if memory is insufficient for the request.
*
* ERRNO:
*   None.
*/

USBHST_STATUS usbEhcdRhProcessInterruptRequest
    (
    pUSB_EHCD_DATA pHCDData,           /* Ptr to HCD block           */
    pUSBHST_URB    pURB                /* Ptr to User Request Block  */
    )
    {

    /* To hold the return status of the function */

    USBHST_STATUS Status = USBHST_SUCCESS;

    /* To hold the interrupt data */

    UINT32 uInterruptData = 0;

    /* Check the validity of the parameters */

    if (NULL == pHCDData ||
        NULL == pURB ||
        NULL == pURB->pTransferBuffer)
        {
        USB_EHCD_ERR("usbEhcdRhProcessInterruptRequest - Invalid parameters\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /*
     * Check if the Root hub is configured
     * to accept any interrupt transfer request.
     */

    if ((0 == pHCDData->RHData.uConfigValue) ||
        (NULL == pHCDData->RHData.pInterruptPipe) ||
        (TRUE == pHCDData->RHData.pInterruptPipe->PipeDeletedFlag))
        {
        USB_EHCD_ERR("usbEhcdRhProcessInterruptRequest - "
                     "Invalid request - device not configured\n",
                     0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_REQUEST;
        }

    /* Assert if the structure members are invalid */

    OS_ASSERT(NULL != pHCDData->RHData.pHubInterruptData);

    /* Exclusively access the request resource */

    OS_WAIT_FOR_EVENT(pHCDData->RequestSynchEventID, OS_WAIT_INFINITE);

    /* Copy the existing interrupt data  */

    OS_MEMCPY(&uInterruptData,
              pHCDData->RHData.pHubInterruptData,
              pHCDData->RHData.uSizeInterruptData);

    /* If interrupt data is available, copy the data directly to URB buffer */

    if (0 != uInterruptData)
        {
        OS_MEMCPY(pURB->pTransferBuffer,
                  &uInterruptData,
                  pHCDData->RHData.uSizeInterruptData);

        /* Initialize the interrupt data */

        OS_MEMSET(pHCDData->RHData.pHubInterruptData,
                  0,
                  pHCDData->RHData.uSizeInterruptData);

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDData->RequestSynchEventID);

        /* Update the length */

        pURB->uTransferLength = pHCDData->RHData.uSizeInterruptData;

        /* Update the status of URB */

        pURB->nStatus = Status;

        /*
         * If a callback function is registered, call the callback
         * function.
         */

         if (pURB->pfCallback)
             {
             pURB->pfCallback(pURB);
             }
        }
    else
        {

        /* Pointer to the request information */

        pUSB_EHCD_REQUEST_INFO pRequest = NULL;

        /* Allocate memory for the request data structure */

        pRequest = (pUSB_EHCD_REQUEST_INFO)OS_MALLOC(sizeof(USB_EHCD_REQUEST_INFO));

        /* Check if memory allocation is successful */

        if (NULL == pRequest)
            {
            /* Release the exclusive access */

            OS_RELEASE_EVENT(pHCDData->RequestSynchEventID);

            USB_EHCD_ERR("usbEhcdRhProcessInterruptRequest - "
                         "memory not allocated for pRequest\n",
                         0, 0, 0, 0, 0, 0);

            return USBHST_INSUFFICIENT_MEMORY;
            }

        /* Initialize the request structure */

        OS_MEMSET(pRequest, 0, sizeof(USB_EHCD_REQUEST_INFO));

        /* Copy the USB_EHCD_PIPE pointer */

        pRequest->pHCDPipe = pHCDData->RHData.pInterruptPipe;

        /* Store the URB pointer in the request data structure */

        pRequest->pUrb = pURB;

        /* Add to the request list */

        if (NULL == pRequest->pHCDPipe->pRequestQueueHead)
            {
            pRequest->pHCDPipe->pRequestQueueHead = pRequest;
            pRequest->pHCDPipe->pRequestQueueTail = pRequest;
            }
        else
            {
            /* Update the next pointer of the tail element */

            pRequest->pHCDPipe->pRequestQueueTail->pNext = pRequest;

            /* Update the tail element to point to the request */

            pRequest->pHCDPipe->pRequestQueueTail = pRequest;
            }

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDData->RequestSynchEventID);
        }
    return Status;

    }
    /* End of function usbEhcdRhProcessInterruptRequest() */

/***************************************************************************
*
* usbEhcdRhProcessStandardRequest - processes a standard transfer request
*
* This routine processes a standard transfer request.
*
* RETURNS:
*  USBHST_SUCCESS if the URB is submitted successfully.
*  USBHST_INVALID_PARAMETER if the parameters are not valid.
*  USBHST_INSUFFICIENT_BANDWIDTH if memory is insufficient for the request.
*
* ERRNO:
*   None.
*/

USBHST_STATUS usbEhcdRhProcessStandardRequest
    (
    pUSB_EHCD_DATA pHCDData,           /* Ptr to HCD block           */
    pUSBHST_URB    pURB                /* Ptr to User Request Block  */
    )
    {
    /* To hold the return status of the function */

    USBHST_STATUS Status = USBHST_SUCCESS;

    /* Pointer to the setup packet */

    pUSBHST_SETUP_PACKET pSetup = NULL;

    /* Check the validity of the parameters */

    if (NULL == pHCDData ||
        NULL == pURB ||
        NULL == pURB->pTransferSpecificData)
        {
        USB_EHCD_ERR("usbEhcdRhProcessStandardRequest - Invalid parameters\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the setup packet from the URB */

    pSetup = (pUSBHST_SETUP_PACKET)pURB->pTransferSpecificData;

    /* Swap the 16 bit values */
    pSetup->wValue = OS_UINT16_CPU_TO_LE(pSetup->wValue);
    pSetup->wIndex = OS_UINT16_CPU_TO_LE(pSetup->wIndex);
    pSetup->wLength = OS_UINT16_CPU_TO_LE(pSetup->wLength);

    /* Handle the standard request */

    switch(pSetup->bRequest)
        {
        case USBHST_REQ_CLEAR_FEATURE:/* Clear feature request */
            {
            /* Based on the recipient, handle the request */
            switch (pSetup->bmRequestType &USB_EHCD_RH_RECIPIENT_MASK)
                {
                case USBHST_RECIPIENT_DEVICE: /* Device recipient */
                    {
                    /* Check the feature selector */
                    if (USBHST_FEATURE_DEVICE_REMOTE_WAKEUP == pSetup->wValue)
                        {
                        /* Disable the device remote wakeup feature */
                        pHCDData->RHData.bRemoteWakeupEnabled = FALSE;
                        }
                        /* Update the URB status */
                        pURB->nStatus = USBHST_SUCCESS;
                        break;
                    }
                case USBHST_RECIPIENT_INTERFACE: /* Interface recipient */
                case USBHST_RECIPIENT_ENDPOINT:  /* Endpoint recipient */
                    {

                    /* Update the URB status */

                    pURB->nStatus = USBHST_SUCCESS;
                    break;
                    }
                default:
                    {
                    /* Invalid recipient value */

                    pURB->nStatus = USBHST_INVALID_REQUEST;
                    break;
                    }
                }
            break;
            }
        case USBHST_REQ_GET_CONFIGURATION:/* Get Configuration request */
            {

            /*
             * Update the URB transfer buffer with the current configuration
             * value for the root hub
             */

            pURB->pTransferBuffer[0] = pHCDData->RHData.uConfigValue;

            /* Update the URB transfer length */

            pURB->uTransferLength = USB_EHCD_RH_GET_CONFIG_SIZE;

            /* Update the URB status */

            pURB->nStatus = USBHST_SUCCESS;
            break;
            }
        case USBHST_REQ_GET_DESCRIPTOR:/* Get Descriptor request */
            {
            /* Check the descriptor type */

            switch (pSetup->wValue >> USB_EHCD_RH_DESCRIPTOR_BITPOSITION)
                {
                case USBHST_DEVICE_DESC:
                    {
                    /* Check the length of descriptor requested */

                    if (pSetup->wLength >= USB_EHCD_RH_DEVICE_DESC_SIZE)
                        {
                        /* Update the URB transfer length */

                        pURB->uTransferLength = USB_EHCD_RH_DEVICE_DESC_SIZE;
                        }
                    else
                        {
                        /* Update the URB transfer length */

                        pURB->uTransferLength = pSetup->wLength;
                        }

                    /* Copy the descriptor to the URB transfer buffer */

                    OS_MEMCPY(pURB->pTransferBuffer,
                              gRHDeviceDescriptor,
                              pURB->uTransferLength);


                    /* Update the URB status */

                    pURB->nStatus = USBHST_SUCCESS;

                    break;
                    }
                case USBHST_CONFIG_DESC:
                    {

                    /* Check the length of descriptor requested */

                    if (pSetup->wLength >= USB_EHCD_RH_CONFIG_DESC_SIZE)
                        {
                        /* Update the URB transfer length */

                        pURB->uTransferLength = USB_EHCD_RH_CONFIG_DESC_SIZE;
                        }
                    else
                        {
                        /* Update the URB transfer length */

                        pURB->uTransferLength = pSetup->wLength;
                        }

                    /* Copy the descriptor to the URB transfer buffer */

                    OS_MEMCPY(pURB->pTransferBuffer,
                              gRHConfigDescriptor,
                              pURB->uTransferLength);

                    /* Update the URB status */

                    pURB->nStatus = USBHST_SUCCESS;
                    break;
                    }
                default:
                    {
                    /* Invalid descriptor type */

                    pURB->nStatus = USBHST_INVALID_REQUEST;
                    break;
                    }
                }
            break;
            }
        case USBHST_REQ_GET_STATUS:/* Get Status request */
            {
            /* Based on the recipient value, handle the request */

            switch (pSetup->bmRequestType & USB_EHCD_RH_RECIPIENT_MASK)
                {
                case USBHST_RECIPIENT_DEVICE:/* Device recipient */
                    {
                    /* Clear the URB transfer buffer */

                    OS_MEMSET(pURB->pTransferBuffer,
                              0,
                              USB_EHCD_RH_GET_STATUS_SIZE);

                    /* Update the device status - Self powered */

                    pURB->pTransferBuffer[0] = 0x01;

                    /* If remote wakeup is enabled, update the status */

                    if (TRUE == pHCDData->RHData.bRemoteWakeupEnabled)
                        {
                        /* Remote wakeup is enabled */

                        pURB->pTransferBuffer[0] |= 0x02;
                        }

                    /* Update the URB transfer length */

                    pURB->uTransferLength = USB_EHCD_RH_GET_STATUS_SIZE;

                    /* Update the URB status */

                    pURB->nStatus = USBHST_SUCCESS;

                    break;
                    }
                case USBHST_RECIPIENT_INTERFACE: /* Interface recipient */
                case USBHST_RECIPIENT_ENDPOINT:  /* Endpoint recipient */
                    {
                    /* Update the URB transfer buffer */

                    OS_MEMSET(pURB->pTransferBuffer,
                              0,
                              USB_EHCD_RH_GET_STATUS_SIZE);

                    /* Update the URB transfer length */

                    pURB->uTransferLength = USB_EHCD_RH_GET_STATUS_SIZE;

                    /* Update the URB status */

                    pURB->nStatus = USBHST_SUCCESS;

                    break;
                    }
                default :
                    {
                    /* Invalid recipient value */

                    pURB->nStatus = USBHST_INVALID_REQUEST;

                    break;
                    }

                }
            break;
            }
        case USBHST_REQ_SET_ADDRESS:/* Set Address request */
            {
            /* Check whether the address is valid */

            if (0 == pSetup->wValue)
                {
                /* Address is not valid */

                pURB->nStatus = USBHST_INVALID_REQUEST;

                break;
                }

            /* Update the RH address */

            pHCDData->RHData.uDeviceAddress = (UINT8)pSetup->wValue;

            /* Update the URB status */

            pURB->nStatus = USBHST_SUCCESS;

            break;
            }
        case USBHST_REQ_SET_CONFIGURATION:/* Set Configuration request */
            {
            /* Check whether the configuration value is valid */

            if ((0 != pSetup->wValue) && (1 != pSetup->wValue))
                {
                /* Invalid configuration value. Update the URB status */

                pURB->nStatus = USBHST_INVALID_REQUEST;

                break;
                }

            /* Update the current configuration value for the root hub */

            pHCDData->RHData.uConfigValue = (UINT8)pSetup->wValue;

            /*
             * Enable the EHCI interrupts - this is done as only
             * after configuration of the Root hub any device other than
             * Root hub will be serviced.
             */

            USB_EHCD_WRITE_REG(pHCDData,
                           USB_EHCD_USBINTR,
                           USB_EHCD_RH_CONFIG_INTERRUPT_MASK);

            /* Update the URB status */

            pURB->nStatus = USBHST_SUCCESS;

            break;
            }
        case USBHST_REQ_SET_FEATURE:/* Set feature request */
            {
            /* Based on the recipient, handle the request */
            switch (pSetup->bmRequestType & USB_EHCD_RH_RECIPIENT_MASK)
                {
                case USBHST_RECIPIENT_DEVICE: /* Device recipient */
                    {
                    /* Check the feature selector */

                    if (USBHST_FEATURE_DEVICE_REMOTE_WAKEUP == pSetup->wValue)
                        {
                        /* Disable the device remote wakeup feature */

                        pHCDData->RHData.bRemoteWakeupEnabled = TRUE;
                        }

                    /* Update the URB status */

                    pURB->nStatus = USBHST_SUCCESS;
                    break;
                    }
                case USBHST_RECIPIENT_INTERFACE: /* Interface recipient */
                case USBHST_RECIPIENT_ENDPOINT:  /* Endpoint recipient */
                    {

                    /* Update the URB status */

                    pURB->nStatus = USBHST_SUCCESS;
                    break;
                    }
                default:
                    {
                    /* Invalid recipient value */

                    pURB->nStatus = USBHST_INVALID_REQUEST;
                    break;
                    }
                }
            break;
            }
        default :/* Invalid request */
            {
            pURB->nStatus = USBHST_INVALID_REQUEST;
            break;
            }
        }/* End of switch () */

    /* If a callback function is registered, call the callback function */

    if (NULL != pURB->pfCallback)
        {
        (pURB->pfCallback)(pURB);
        }
    return Status;
    }
    /* End of usbEhcdRhProcessStandardRequest() */

/***************************************************************************
*
* usbEhcdRhClearPortFeature - clears a feature of the port
*
* This routine clears a feature of the port.
*
* RETURNS:
*  USBHST_SUCCESS - if the URB is submitted successfully.
*  USBHST_INVALID_PARAMETER- if the parameters are not valid.
*
* ERRNO:
*   None.
*/

USBHST_STATUS usbEhcdRhClearPortFeature
    (
    pUSB_EHCD_DATA pHCDData,           /* Ptr to HCD block           */
    pUSBHST_URB    pURB                /* Ptr to User Request Block  */
    )
    {
    /* To hold the request status */

    USBHST_STATUS Status = USBHST_SUCCESS;

    /* Pointer to the setup packet */

    pUSBHST_SETUP_PACKET pSetup = NULL;

    UINT32 uBusIndex = 0; /* index of the host controller */

    /* Check the validity of the parameters */

    if (NULL == pHCDData ||
        NULL == pURB ||
        NULL == pURB->pTransferSpecificData)
        {
        USB_EHCD_ERR("usbEhcdRhClearPortFeature - Invalid parameters\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the index of the host controller */

    uBusIndex = pHCDData->uBusIndex;

    /* Extract the setup packet */

    pSetup = (pUSBHST_SETUP_PACKET)pURB->pTransferSpecificData;

    /* Check whether the members are valid */

    if (pSetup->wIndex >
                pHCDData->RHData.uNumDownstreamPorts)
        {
        USB_EHCD_ERR("usbEhcdRhClearPortFeature - Invalid port index\n",
            0, 0, 0, 0, 0, 0);

        pURB->nStatus = USBHST_INVALID_PARAMETER;

        return USBHST_INVALID_PARAMETER;
        }

    /* Handle the request based on the feature to be cleared */

    switch(pSetup->wValue)
        {
        case USB_EHCD_RH_PORT_ENABLE:/* Port enable */
            {
            /* Clear the enable bit */

            USB_EHCD_CLR_BIT_PORT(pHCDData,
                              (pSetup->wIndex) -1,
                              PORT_ENABLED_DISABLED);

            /* Update the URB status */

            pURB->nStatus = USBHST_SUCCESS;
            break;
            }
        case USB_EHCD_RH_PORT_SUSPEND:/* Port Suspend */
            {
            /* To hold the status of the port */

            UINT32 uPortStatus = 0;

            /* Drive port resume */

            USB_EHCD_SET_BIT_PORT(pHCDData,
                              ((pSetup->wIndex) -1),
                              FORCE_PORT_RESUME);

            /*
             * The resume signal should be generated for atleast
             * 20ms. Additional 10 ms is for handling the
             * software delays.
             * Should be checked whether there is any performance issue
             * with this delay.
             */

            OS_DELAY_MS(30);

            /* Stop port resume */

            USB_EHCD_CLR_BIT_PORT(pHCDData,
                              (pSetup->wIndex) -1,
                              FORCE_PORT_RESUME);

            /* Update the URB status */

            pURB->nStatus = USBHST_SUCCESS;

            /* Copy the value in the port status register */

            OS_MEMCPY(&uPortStatus,
                      (pHCDData->RHData.pPortStatus +
                       (pSetup->wIndex -1) * USB_EHCD_RH_PORT_STATUS_SIZE),
                      USB_EHCD_RH_PORT_STATUS_SIZE);

            /* Swap the data to the LE format */

            uPortStatus = USB_EHCD_SWAP_USB_DATA(uBusIndex,uPortStatus);

            /* Clear the suspend bit */

            uPortStatus &= ~(USB_EHCD_RH_PORT_SUSPEND_MASK);

            /* Update the suspend change */

            uPortStatus |= USB_EHCD_RH_PORT_SUSPEND_CHANGE;

            /* Swap the data to CPU format data */

            uPortStatus = USB_EHCD_SWAP_USB_DATA(uBusIndex,uPortStatus);

            /* Copy the status back to the port status */

            OS_MEMCPY((pHCDData->RHData.pPortStatus +
                       (pSetup->wIndex -1) * USB_EHCD_RH_PORT_STATUS_SIZE),
                      &uPortStatus,
                      USB_EHCD_RH_PORT_STATUS_SIZE);
            /*
             * Call the function to populate the interrupt
             * status data
             */

            usbEhcdCopyRHInterruptData(
                     pHCDData,
                     (UINT32)(USB_EHCD_RH_MASK_VALUE << (pSetup->wIndex -1)));
            break;
            }
        case USB_EHCD_RH_PORT_POWER:  /* Port Power */
            {
            /*
             * If only the host controller supports port
             * power switching, power off the port.
             */
            if (USB_EHCD_GET_FIELD(pHCDData,
                                HCSPARAMS,
                                PPC) == 1)
                {
                /* Switch off power in the port */
                USB_EHCD_CLR_BIT_PORT(pHCDData,
                                     (pSetup->wIndex) -1,
                                     PORT_POWER);
                }

            /* Update the URB status */

            pURB->nStatus = USBHST_SUCCESS;
            break;
            }
        case USB_EHCD_RH_C_PORT_CONNECTION:/* c_port_connection */
            {
            if (USB_EHCD_GET_FIELD_PORT(pHCDData,
                                        ((pSetup->wIndex) -1),
                                        CONNECT_STATUS_CHANGE) != 0)
                {

                /* Clear the connection status change */

                USB_EHCD_CLR_BIT_PORT(pHCDData,
                                  ((pSetup->wIndex) -1),
                                  CONNECT_STATUS_CHANGE);
                }
            /* This condition can happen if the EHCI does not support
             * port power switching. In this case, if a device is connected
             * while the target boots up, there is no connect status change
             * and no root hub status change interrupt which is generated
             */
            else
                {
                /* To hold the status of the port */

                UINT32 uPortStatus = 0;

                /* Copy the value in the port status register */

                OS_MEMCPY(&uPortStatus,
                          (pHCDData->RHData.pPortStatus +
                           ((pSetup->wIndex) -1) * USB_EHCD_RH_PORT_STATUS_SIZE),
                          USB_EHCD_RH_PORT_STATUS_SIZE);

                /* Swap the data from the LE format */

                uPortStatus = USB_EHCD_SWAP_USB_DATA(uBusIndex,
                                                 uPortStatus);

                /* Update the connect status change */

                uPortStatus &= ~(USB_EHCD_RH_PORT_CONNECT_CHANGE);

                /* Swap the data to LE format data */

                uPortStatus = USB_EHCD_SWAP_USB_DATA(uBusIndex,
                                                 uPortStatus);

                /* Copy the status back to the port status */

                OS_MEMCPY((pHCDData->RHData.pPortStatus +
                           ((pSetup->wIndex) -1) * USB_EHCD_RH_PORT_STATUS_SIZE),
                          &uPortStatus,
                          USB_EHCD_RH_PORT_STATUS_SIZE);

                }
            /* Update the URB status */

            pURB->nStatus = USBHST_SUCCESS;
            break;
            }
        case USB_EHCD_RH_C_PORT_ENABLE:/* c_port_enabled */
            {
            /* Clear the port enable disable change */

            USB_EHCD_CLR_BIT_PORT(pHCDData,
                              ((pSetup->wIndex) -1),
                              PORT_ENABLE_DISABLE_CHANGE);

            /* Update the URB status */

            pURB->nStatus = USBHST_SUCCESS;
            break;
            }
        case USB_EHCD_RH_C_PORT_OVER_CURRENT:/* c_port_over_current */
            {
            /* Clear the overcurrent change */

            USB_EHCD_CLR_BIT_PORT(pHCDData,
                              ((pSetup->wIndex) -1),
                              OVER_CURRENT_CHANGE);

            /* Update the URB status */

            pURB->nStatus = USBHST_SUCCESS;
            break;
            }
        case USB_EHCD_RH_PORT_INDICATOR:/* Port Indicator */
            {
            /* Clear the Port indicator */

            USB_EHCD_CLR_BIT_PORT(pHCDData,
                              ((pSetup->wIndex & USB_EHCD_RH_PORT_NUMBER_MASK) -1),
                              PORT_INDICATOR_CONTROL);

            /* Update the URB status */

            pURB->nStatus = USBHST_SUCCESS;
            break;
            }
        case USB_EHCD_RH_PORT_TEST: /* Port test */
            {
            /* Clear the Port test */

            USB_EHCD_CLR_BIT_PORT(pHCDData,
                              ((pSetup->wValue & USB_EHCD_RH_PORT_NUMBER_MASK) -1),
                              PORT_TEST_CONTROL);

            /* Update the URB status */

            pURB->nStatus = USBHST_SUCCESS;
            break;
            }

        case USB_EHCD_RH_C_PORT_RESET:/* c_port_reset */
            {
            /* To hold the status of the port */

            UINT32 uPortStatus = 0;

            /* Clear the internal data for reset change */
            /* Copy the value in the port status register */

            OS_MEMCPY(&uPortStatus,
                      (pHCDData->RHData.pPortStatus +
                       (pSetup->wIndex -1) * USB_EHCD_RH_PORT_STATUS_SIZE),
                      USB_EHCD_RH_PORT_STATUS_SIZE);

            /* Swap the data to the LE format */

            uPortStatus = USB_EHCD_SWAP_USB_DATA(uBusIndex,uPortStatus);

            /* Update the reset change */

            uPortStatus &= (~USB_EHCD_RH_PORT_RESET_CHANGE);

            /* Swap the data to CPU format data */

            uPortStatus = USB_EHCD_SWAP_USB_DATA(uBusIndex,uPortStatus);

            /* Copy the status back to the port status */

            OS_MEMCPY((pHCDData->RHData.pPortStatus +
                       (pSetup->wIndex -1) * USB_EHCD_RH_PORT_STATUS_SIZE),
                      &uPortStatus,
                      USB_EHCD_RH_PORT_STATUS_SIZE);

            /* Update the URB status */

            pURB->nStatus = USBHST_SUCCESS;

            /*
             * Call the function to populate the interrupt
             * status data
             */
#if 0
            usbEhcdCopyRHInterruptData(
                     pHCDData,
                     (USB_EHCD_RH_MASK_VALUE << (pSetup->wIndex -1)));
#endif
            break;
            }

        case USB_EHCD_RH_C_PORT_SUSPEND:/* c_port_suspend */
            {
            /* To hold the status of the port */

            UINT32 uPortStatus = 0;

            /* Clear the internal data for suspend change */

            /* Copy the value in the port status register */

            OS_MEMCPY(&uPortStatus,
                      (pHCDData->RHData.pPortStatus +
                       (pSetup->wIndex -1) * USB_EHCD_RH_PORT_STATUS_SIZE),
                      USB_EHCD_RH_PORT_STATUS_SIZE);

            /* Update the suspend change */

            uPortStatus &= (~USB_EHCD_RH_PORT_SUSPEND_CHANGE);

            /* Copy the status back to the port status */

            OS_MEMCPY((pHCDData->RHData.pPortStatus +
                       (pSetup->wIndex -1) * USB_EHCD_RH_PORT_STATUS_SIZE),
                      &uPortStatus,
                      USB_EHCD_RH_PORT_STATUS_SIZE);

            /* Update the URB status */

            pURB->nStatus = USBHST_SUCCESS;

            /*
             * Call the function to populate the interrupt
             * status data
             */

            usbEhcdCopyRHInterruptData(
                     pHCDData,
                     (UINT32)(USB_EHCD_RH_MASK_VALUE << (pSetup->wIndex -1)));
            break;
            }
        default:  /* Unknown feature */
            {
            pURB->nStatus = USBHST_INVALID_REQUEST;
            break;
            }/* End of default */
        }/* End of switch () */

    return Status;
    }
    /* End of usbEhcdRhClearPortFeature() */

/***************************************************************************
*
* usbEhcdRhGetHubDescriptor - get the hub descriptor
*
* This routine gets the hub descriptor.
*
* RETURNS:
*  USBHST_SUCCESS - if the URB is submitted successfully.
*  USBHST_INVALID_PARAMETER - if the parameters are not valid.
*
* ERRNO:
*   None.
*/

USBHST_STATUS usbEhcdRhGetHubDescriptor
    (
    pUSB_EHCD_DATA pHCDData,           /* Ptr to HCD block           */
    pUSBHST_URB    pURB                /* Ptr to User Request Block  */
    )
    {
    /* To hold the request status */

    USBHST_STATUS Status = USBHST_SUCCESS;

    /* Pointer to the setup packet */

    pUSBHST_SETUP_PACKET pSetup = NULL;

    /* To hold the number of bytes alloted for the port information */

    UINT32 uPortBytes = 0;

    /* To hold the index into the ports */

    UINT32 uIndex = 0;

    /* Pointer to the buffer */

    UCHAR *pBuffer = NULL;

    /* Check the validity of the parameters */

    if (NULL == pHCDData ||
        NULL == pURB ||
        NULL == pURB->pTransferSpecificData ||
        NULL == pURB->pTransferBuffer)
        {
        USB_EHCD_ERR("usbEhcdRhGetHubDescriptor - Invalid parameters\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the setup packet */

    pSetup = (pUSBHST_SETUP_PACKET)pURB->pTransferSpecificData;

    /*
     * Check whether there are any invalid conditions.
     * i.e invalid DevRequest parameters
     */

    if (0x2900 != pSetup->wValue || 0 != pSetup->wIndex)
        {
        USB_EHCD_ERR("usbEhcdRhGetHubDescriptor - Invalid parameters\n",
            0, 0, 0, 0, 0, 0);

        pURB->nStatus = USBHST_INVALID_PARAMETER;

        return USBHST_INVALID_PARAMETER;
        }

    /*
     * Determine the number of bytes that the descriptor would occupy
     * ie. the num of bytes req to accomodate info about the ports
     */

    uPortBytes = (pHCDData->RHData.uNumDownstreamPorts) / 8;
    if (0 != pHCDData->RHData.uNumDownstreamPorts % 8)
        {
        uPortBytes++;
        }

    /* Allocate memory for the buffer */

    pBuffer = (UCHAR *)OS_MALLOC(7 + (uPortBytes * 2));

    /* Check if memory allocation is successful */

    if(NULL == pBuffer)
        {
        USB_EHCD_ERR("usbEhcdRhGetHubDescriptor - "
                     "Memory not allocated\n",
                     0, 0, 0, 0, 0, 0);

        pURB->nStatus = USBHST_MEMORY_NOT_ALLOCATED;

        return USBHST_MEMORY_NOT_ALLOCATED;
        }

    /* Population of the values of hub descriptor - Start */

    /* Length of the descriptor */

    pBuffer[0] = (UINT8)(7 + (uPortBytes * 2));

    /* Hub Descriptor type */

    pBuffer[1] = 0x29;

    /* Number of downstream ports */

    pBuffer[2] = pHCDData->RHData.uNumDownstreamPorts;

    /*
     * The following 2 bytes give the hub characteristics
     * The root hub has individual port overcurrent indication
     * Read the Port power control field and update whether the root hub
     * supports per port power switching or ganged power switching.
     */

    pBuffer[3] = (UINT8)(0x08 | USB_EHCD_GET_FIELD(pHCDData,
                                       HCSPARAMS,
                                       PPC));
    pBuffer[4] = 0;

    /* The power On to Power Good Time for the Root hub is 1 * 2ms */
    pBuffer[5] = 1;

    /* There are no specific maximim current requirements */

    pBuffer[6] = 0;

    /* The last few bytes of the descriptor is based on the number of ports */

    for(uIndex = 0;uIndex < uPortBytes ; uIndex++)
        {
        /* Indicates whether the hub is removable */

        pBuffer[7+uIndex] = 0;

        /* Port power control mask should be 1 for all the ports */

        pBuffer[7+uPortBytes+uIndex] = 0xff;
        }

    /* Population of the values of hub descriptor - End */

    /* Update the length */

    if (pURB->uTransferLength >= 7 + (uPortBytes * 2))
        {
        pURB->uTransferLength = 7 + (uPortBytes * 2);
        }

    /* Copy the data */

    OS_MEMCPY(pURB->pTransferBuffer, pBuffer, pURB->uTransferLength);

    /* Update the status */

    pURB->nStatus = USBHST_SUCCESS;

    /* Free memory allocated for the buffer */

    OS_FREE(pBuffer);

    return Status;

    }
    /* End of usbEhcdRhGetHubDescriptor() */

/***************************************************************************
*
* usbEhcdRhGetPortStatus - get the status of the port
*
* This routine gets the status of the port.
*
* RETURNS:
*  USBHST_SUCCESS - if the URB is submitted successfully.
*  USBHST_INVALID_PARAMETER - if the parameters are not valid.
*
* ERRNO:
*   None.
*/

USBHST_STATUS usbEhcdRhGetPortStatus
    (
    pUSB_EHCD_DATA pHCDData,           /* Ptr to HCD block           */
    pUSBHST_URB    pURB                /* Ptr to User Request Block  */
    )
    {
    /* To hold the request status */

    USBHST_STATUS Status = USBHST_SUCCESS;

    /* Pointer to the setup packet */

    pUSBHST_SETUP_PACKET pSetup = NULL;

    /* To hold the status of the hub port */

    UINT32 uPortStatus = 0;

    UINT32 uPortSpeed = 0;

    /*
     * This holds the port status which is available in the
     * root hub port status buffer
     */

    UINT32 uPortStatusAvailable = 0;

    UINT32 uBusIndex = 0; /* index of the host controller */

    /* Check the validity of the parameters */

    if (NULL == pHCDData ||
        NULL == pURB ||
        NULL == pURB->pTransferSpecificData ||
        NULL == pURB->pTransferBuffer)
        {
        USB_EHCD_ERR("usbEhcdRhGetPortStatus - Invalid parameters\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the index of the host controller */

    uBusIndex = pHCDData->uBusIndex;

    /* Extract the setup packet */

    pSetup = (pUSBHST_SETUP_PACKET)pURB->pTransferSpecificData;

    /* Check whether the members are valid */

    if (pSetup->wIndex >
                pHCDData->RHData.uNumDownstreamPorts)
        {
        USB_EHCD_ERR("usbEhcdRhGetPortStatus - Invalid port index\n",
            0, 0, 0, 0, 0, 0);

        pURB->nStatus = USBHST_INVALID_PARAMETER;

        return USBHST_INVALID_PARAMETER;
        }

    /*
     * The hub's port status contains 4 bytes of information, out of which
     * the hub's port status is reported in the byte offsets 0 to 1
     * and the hub's port status change in the byte offsets 2 to 3
     */

    /* Update the port status change bytes - Start */
    /*
     * Following is the interpretation of the 16 bits of port status change
     * Bit 5-15 Reserved
     * Bit 4 - Reset change
     * Bit 3 - Overcurrent indicator Change
     * Bit 2 - Suspend Change
     * Bit 1 - Port Enable/Disable change
     * Bit 0 - Connect status change
     */

    /* Update the overcurrent change bit */

    uPortStatus |= USB_EHCD_GET_FIELD_PORT(pHCDData,
                                       (pSetup->wIndex)- 1,
                                       OVER_CURRENT_CHANGE);
    uPortStatus <<= 1;

    /*
     * Update the suspend change bit
     * (ie) 0 since there is no suspend-change indication in EHCI
     */

    uPortStatus |= 0;
    uPortStatus <<= 1;

    /* Store enable/disable change bit */

    uPortStatus |= USB_EHCD_GET_FIELD_PORT(pHCDData,
                                  (pSetup->wIndex)- 1,
                                  PORT_ENABLE_CHANGE);
    uPortStatus <<= 1;

    /* Store connect status change */

    uPortStatus |= USB_EHCD_GET_FIELD_PORT(pHCDData,
                                  (pSetup->wIndex)- 1,
                                  CONNECT_STATUS_CHANGE);
    uPortStatus <<= 1;

    /* Update the port status change bytes - End */
    /* Update the port status bytes - Start */
    /*
     * Following is the interpretation of the 16 bits of port status
     * Bit 13-15 Reserved
     * Bit 12- Port indicator displays software controller colour
     * Bit 11- Port is in Port Test Mode
     * Bit 10- High speed device is attached
     * Bit 9 - Low speed device is attached
     * Bit 8 - Port is powered on
     * Bit 5-7 Reserved
     * Bit 4 - Reset signalling asserted
     * Bit 3 - An overcurrent condition exists on the port
     * Bit 2 - Port is suspended
     * Bit 1 - Port is enabled
     * Bit 0 - Device is present on the port
     */
    /* fill next 3 bits with 0 */

    uPortStatus <<= 3;

    /* Check whether the port indicator field is set and update the status */

    if (0 != USB_EHCD_GET_FIELD_PORT(pHCDData,
                                 (pSetup->wIndex)- 1,
                                  PORT_INDICATOR_CONTROL))
        {
        uPortStatus |= 0x01;
        }
    uPortStatus <<= 1;

    /* Check whether the port test mode field is set and update the status */

    if (0 != USB_EHCD_GET_FIELD_PORT(pHCDData,
                                (pSetup->wIndex)- 1,
                                 PORT_TEST_CONTROL))
        {
        uPortStatus |= 0x01;
        }

    uPortStatus <<= 1;

    if (pHCDData->hasEmbeddedTT == FALSE)
        {
        /* Check whether a high speed device is attached and update the status */

        uPortStatus |= USB_EHCD_GET_FIELD_PORT(pHCDData,
                                      (pSetup->wIndex)- 1,
                                      PORT_ENABLED_DISABLED);
        uPortStatus <<= 1;

        /* This cannot be updated as low/ full speed is not supported in Root hub */

        if (USBHST_LOW_SPEED == USB_EHCD_GET_FIELD_PORT(pHCDData,
                                                    (pSetup->wIndex)- 1,
                                                    LINE_STATUS))
            {
            uPortStatus |= 0x01;
            }

        uPortStatus <<= 1;
        }
    else
        {
        /* Host controllers with embedded TT has port speed indication in the PSPS filed */
        uPortSpeed = USB_EHCD_GET_FIELD_PORT(pHCDData,
                                                (pSetup->wIndex)- 1,
                                                PSPD);
        /* Get the speed from the non-EHCI PSPD field */
        if (USBHST_HIGH_SPEED != uPortSpeed)
            {
            /* Shift the high speed bit */
            uPortStatus <<= 1;

            if (USBHST_LOW_SPEED == uPortSpeed)
                {
                /* Set the low speed  bit */
                uPortStatus |= 0x01;
                }
            else if (USBHST_FULL_SPEED == uPortSpeed)
                {
                /* No full speed bit to set */
                }
            else
                {
                /*
                * Even this indicate an invalid port speed, it is not an error;
                * We should continue, without setting any speed bit;
                * This conditon can happen when a device is detached.
                */
                }

            /* Shift the low speed bit */
            uPortStatus <<= 1;

            }
        else
            {
            /* Set the high speed  bit */
            uPortStatus |= 0x01;

            /* Shift the high speed bit */
            uPortStatus <<= 1;

            /* Shift the low speed bit */
            uPortStatus <<= 1;

            }
        }

    /* Check whether the port is powered on and update the status */

    uPortStatus |= USB_EHCD_GET_FIELD_PORT(pHCDData,
                                  (pSetup->wIndex)- 1,
                                  PORT_POWER);
    uPortStatus <<= 1;

    /* Bits 5 to 7 are reserved */

    uPortStatus <<= 3;

    /* Read the bit indicating whether a reset is asserted. Update the status */

    uPortStatus |= USB_EHCD_GET_FIELD_PORT(pHCDData,
                                  (pSetup->wIndex)- 1,
                                  PORT_RESET);
    uPortStatus <<= 1;

    /*
     * Check whether a over current is detected
     * in the port and update the status
     */

    uPortStatus |= USB_EHCD_GET_FIELD_PORT(pHCDData,
                                  (pSetup->wIndex)- 1,
                                  OVER_CURRENT_ACTIVE);
    uPortStatus <<= 1;

    /* Check whether the port is suspended and update the status */

    uPortStatus |= USB_EHCD_GET_FIELD_PORT(pHCDData,
                                  (pSetup->wIndex)- 1,
                                  SUSPEND);
    uPortStatus <<= 1;

    /* Check whether the port is enabled and update the status */

    uPortStatus |= USB_EHCD_GET_FIELD_PORT(pHCDData,
                                  (pSetup->wIndex)- 1,
                                  PORT_ENABLED_DISABLED);
    uPortStatus <<= 1;

    /* Check whether a device is connected and update the status */

    uPortStatus |= USB_EHCD_GET_FIELD_PORT(pHCDData,
                                  (pSetup->wIndex)- 1,
                                  CURRENT_CONNECT_STATUS);


    /* Update the port status bytes - End */

    /* Copy the data which is available in the port status */

    OS_MEMCPY(&uPortStatusAvailable,
              (pHCDData->RHData.pPortStatus + 
               ((pSetup->wIndex) - 1) * USB_EHCD_RH_PORT_STATUS_SIZE),
              USB_EHCD_RH_PORT_STATUS_SIZE);

    /* Swap the data to LE format */

    uPortStatusAvailable = USB_EHCD_SWAP_USB_DATA(uBusIndex,
                                              uPortStatusAvailable);

    /* Update the port status value */

    uPortStatus |= uPortStatusAvailable;

    /* Swap the data to CPU format */

    uPortStatus = USB_EHCD_SWAP_USB_DATA(uBusIndex,uPortStatus);

    /* Copy the port status */

    OS_MEMCPY(pURB->pTransferBuffer, &uPortStatus, 4);

    /* Update the status of URB */

    pURB->nStatus = USBHST_SUCCESS;

    return Status;

    }
    /* End of usbEhcdRhGetPortStatus() */

/***************************************************************************
*
* usbEhcdRhSetPortFeature - set the features of the port
*
* This routine sets the features of the port.
*
* RETURNS:
*  USBHST_SUCCESS if the URB is submitted successfully.
*  USBHST_INVALID_PARAMETER if the parameters are not valid.
*
* ERRNO:
*   None.
*/

USBHST_STATUS usbEhcdRhSetPortFeature
    (
    pUSB_EHCD_DATA pHCDData,           /* Ptr to HCD block           */
    pUSBHST_URB    pURB                /* Ptr to User Request Block  */
    )
    {
    /* To hold the request status */

    USBHST_STATUS Status = USBHST_SUCCESS;

    /* Pointer to the setup packet */

    pUSBHST_SETUP_PACKET pSetup = NULL;

    UINT32 uBusIndex = 0; /* index of the host controller */
    UINT8  uUsbCmdRsBack = 0;

    /* WindView Instrumentation */
    USB_HCD_LOG_EVENT(
        USB_EHCI_WV_PORT_ROUTE,
        "usbEhcdRhSetPortFeature() starts",
        USB_EHCD_WV_FILTER);

    /* Check the validity of the parameters */

    if (NULL == pHCDData ||
        NULL == pURB ||
        NULL == pURB->pTransferSpecificData)
        {
        USB_EHCD_ERR("usbEhcdRhSetPortFeature - Invalid parameters\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the index of the host controller */

    uBusIndex = pHCDData->uBusIndex;

    /* Extract the setup packet */

    pSetup = (pUSBHST_SETUP_PACKET)pURB->pTransferSpecificData;

    /* Switch on the feature to be selected */

    switch(pSetup->wValue)
        {
        case USB_EHCD_RH_PORT_RESET:/* If port reset */
            {
            /* Attempt port reset only if HC is not halted */

            if (0 == USB_EHCD_GET_FIELD(pHCDData,
                                   USBSTS,
                                   HCHALTED))
                {

                /*
                 * With embdded TT and no companion chip,
                 * we have to handle FULL/LOW speed devices
                 * by our own.
                 */

                /*
                 * If a low speed device is detected, change the
                 * ownership of the port to the companion host controller
                 */

                if ((pHCDData->hasEmbeddedTT == FALSE) &&
                     (USBHST_LOW_SPEED == USB_EHCD_GET_FIELD_PORT(pHCDData,
                                                            (pSetup->wIndex)- 1,
                                                            LINE_STATUS)) )
                    {
                    /* WindView Instrumentation */

                    USB_HCD_LOG_EVENT(
                        USB_EHCI_WV_PORT_ROUTE,
                        "SetPortFeature : Low speed device detected",
                        USB_EHCD_WV_FILTER);

                    /* Only handle over ownership when has companion chip */

                    if (pHCDData->hasCompanion == TRUE)
                        {
                        USB_EHCD_DBG("usbEhcdRhSetPortFeature - "
                                     "LS Port %d being handed over\n",
                                     pSetup->wIndex, 0, 0, 0, 0, 0);

                        /*
                         * Release ownership of the port
                         * to the companion controller
                         */

                        USB_EHCD_SET_BIT_PORT(pHCDData,
                                          ((pSetup->wIndex) - 1),
                                          PORT_OWNER);
                        }
                    }

                /* A high/Full speed device is attached */

                else
                    {
                    /*
                     * At this point, it is not known whether
                     * the connected device is a full/ high speed device.
                     * After giving a port reset, if the port is enabled,
                     * then a high speed device is connected. If it is
                     * not enabled, then a full speed device is
                     * connected.
                     */

                    /*
                     * Give a settling time for the mechanical
                     * and electrical connections
                     * Check whether there is any performance issue with this
                     * wait.
                     */

                    OS_DELAY_MS(100);

                    /* Disable the port */

                    USB_EHCD_CLR_BIT_PORT(pHCDData,
                                      ((pSetup->wIndex)- 1),
                                      PORT_ENABLED_DISABLED);

                    /* Initiate a reset to the port */

                    USB_EHCD_SET_BIT_PORT(pHCDData,
                                      ((pSetup->wIndex) - 1),
                                      PORT_RESET);

                    /*
                     * The reset signal should be driven atleast for 50ms
                     * from the Root hub.
                     * Additional 10ms is given here for software delays.
                     */

                    OS_DELAY_MS(60);
					
                    /* 
                     * P2020, P2010, P1010 and P1020 Chip Errata 
                     * USB-A003: Illegal NOPID TX CMD issued by USB controller 
                     *           with ULPI interface
                     * Workaround: Do not enable USBCMD[RS] for 300 uSec after
                     * the USB reset has been completed (after PORTSCx[PR] reset 
                     * to 0). This ensures that the host does not send the SOF 
                     * until the ULPI post reset processing has been completed.
                     */

                    /* Check if the host controller is running or stopping */

                    if (((pHCDData->uPlatformType == USB_EHCD_PLATFORM_P2010) || 
                         (pHCDData->uPlatformType == USB_EHCD_PLATFORM_P2020) ||
                         (pHCDData->uPlatformType == USB_EHCD_PLATFORM_P1020) ||
                         (pHCDData->uPlatformType == USB_EHCD_PLATFORM_P1010)) &&
                         (1 == USB_EHCD_GET_FIELD(pHCDData, USBCMD, RS)))
                        {
                        uUsbCmdRsBack = 1;

                        /* Clear the Run/Stop bit to stop the Host Controller */

                        USB_EHCD_CLR_BIT(pHCDData, USBCMD, RS);
                        }

                    /* Stop port reset */

                    USB_EHCD_CLR_BIT_PORT(pHCDData,
                                      ((pSetup->wIndex)- 1),
                                      PORT_RESET);

                    if (uUsbCmdRsBack == 1)
                        {
                        OS_BUSY_WAIT_US(300);

                        /* Set the Run/Stop bit to start the Host Controller */

                        USB_EHCD_SET_BIT(pHCDData,
                                         USBCMD,
                                         RS);
                        }
						
                    /*
                     * give a delay of 1 MS to allow the port to reset. This is
                     * as per EHCI Specification.
                     */

                    OS_BUSY_WAIT_MS (1);
					
                    /* Some boards need more time to wait RESET finished */
                    if (USB_EHCD_GET_FIELD_PORT (pHCDData,
                                                ((pSetup->wIndex)- 1),
                                                 PORT_RESET) == 1)
                        OS_DELAY_MS(10);

                    /*
                     * check if the port is reset; if not return failure and
                     * break
                     */

                    if (USB_EHCD_GET_FIELD_PORT (pHCDData,
                                                ((pSetup->wIndex)- 1),
                                                 PORT_RESET) == 1)
                        {
                        UINT32 uLineStatus = USB_EHCD_GET_FIELD_PORT (pHCDData,
                                                ((pSetup->wIndex)- 1),
                                                 LINE_STATUS);

                        USB_EHCD_ERR("usbEhcdRhSetPortFeature - "
                                     "Port %d reset can not be removed\n",
                                     pSetup->wIndex, 0, 0, 0, 0, 0);

                        if (uLineStatus == USB_EHCD_PORTSC_LINE_STATUS_J)
                            {
                            USB_EHCD_WARN("usbEhcdRhSetPortFeature - "
                                         "Port %d is in J state (FS/HS)\n",
                                         pSetup->wIndex, 0, 0, 0, 0, 0);
                            }
                        else if (uLineStatus == USB_EHCD_PORTSC_LINE_STATUS_K)
                            {
                            USB_EHCD_WARN("usbEhcdRhSetPortFeature - "
                                         "Port %d is K state (LS)\n",
                                         pSetup->wIndex, 0, 0, 0, 0, 0);

                            }
                        else if (uLineStatus == USB_EHCD_PORTSC_LINE_STATUS_SE0)
                            {
                            USB_EHCD_WARN("usbEhcdRhSetPortFeature - "
                                         "Port %d is in SE0 state (FS/HS)\n",
                                         pSetup->wIndex, 0, 0, 0, 0, 0);
                            }
                        else
                            {
                            USB_EHCD_WARN("usbEhcdRhSetPortFeature - "
                                         "Port %d state is unknown\n",
                                         pSetup->wIndex, 0, 0, 0, 0, 0);
                            }

                        pURB->nStatus = USBHST_FAILURE;

                        break;
                        }

                    /*
                     * If Port is not enabled, it is a full speed which
                     * is connected, so release the ownership of the
                     * port to the companion controller
                     */

                    if (0 == USB_EHCD_GET_FIELD_PORT(pHCDData,
                                                ((pSetup->wIndex) - 1),
                                                PORT_ENABLED_DISABLED))
                        {
                        /* Check if the connection is recognised */

                        if (USB_EHCD_GET_FIELD_PORT(pHCDData,
                                              ((pSetup->wIndex) - 1),
                                               CURRENT_CONNECT_STATUS))
                            {

                            UINT32 uLineStatus = USB_EHCD_GET_FIELD_PORT (pHCDData,
                                                    ((pSetup->wIndex)- 1),
                                                     LINE_STATUS);

                            /* WindView Instrumentation */
                            USB_HCD_LOG_EVENT(
                                USB_EHCI_WV_PORT_ROUTE,
                                "SetPortFeature : Full speed device detected",
                                USB_EHCD_WV_FILTER);

                            if (uLineStatus == USB_EHCD_PORTSC_LINE_STATUS_J)
                                {
                                USB_EHCD_WARN("usbEhcdRhSetPortFeature - "
                                             "Port %d is in J state (FS/HS)\n",
                                             pSetup->wIndex, 0, 0, 0, 0, 0);
                                }
                            else if (uLineStatus == USB_EHCD_PORTSC_LINE_STATUS_K)
                                {
                                USB_EHCD_WARN("usbEhcdRhSetPortFeature - "
                                             "Port %d is K state (LS)\n",
                                             pSetup->wIndex, 0, 0, 0, 0, 0);

                                }
                            else if (uLineStatus == USB_EHCD_PORTSC_LINE_STATUS_SE0)
                                {
                                USB_EHCD_WARN("usbEhcdRhSetPortFeature - "
                                             "Port %d is in SE0 state (FS/HS)\n",
                                             pSetup->wIndex, 0, 0, 0, 0, 0);
                                }
                            else
                                {
                                USB_EHCD_WARN("usbEhcdRhSetPortFeature - "
                                             "Port %d state is unknown\n",
                                             pSetup->wIndex, 0, 0, 0, 0, 0);
                                }

                            /* Only handle over ownership when has companion chip */

                            if (pHCDData->hasCompanion == TRUE)
                                {
                                USB_EHCD_DBG("usbEhcdRhSetPortFeature - "
                                             "FS Port %d being handed over\n",
                                             pSetup->wIndex, 0, 0, 0, 0, 0);

                                /*
                                 * Release ownership of the port
                                 * to the companion controller
                                 */

                                USB_EHCD_SET_BIT_PORT(pHCDData,
                                                  ((pSetup->wIndex) -1),
                                                  PORT_OWNER);
                                }
                            pURB->nStatus = USBHST_SUCCESS;
                            }
                        else
                            {
                            UINT32 uLineStatus = USB_EHCD_GET_FIELD_PORT (pHCDData,
                                                    ((pSetup->wIndex)- 1),
                                                     LINE_STATUS);
                            USB_EHCD_ERR(
                                "usbEhcdRhSetPortFeature - "
                                "Port %d disabled after port reset\n",
                                pSetup->wIndex, 0, 0, 0, 0, 0);

                            if (uLineStatus == 0x2)
                                {
                                USB_EHCD_WARN("usbEhcdRhSetPortFeature - "
                                             "Port %d is a FULL speed device\n",
                                             pSetup->wIndex, 0, 0, 0, 0, 0);
                                }
                            else if (uLineStatus == 0x1)
                                {
                                USB_EHCD_WARN("usbEhcdRhSetPortFeature - "
                                             "Port %d is a LOW speed device\n",
                                             pSetup->wIndex, 0, 0, 0, 0, 0);

                                }
                            else
                                {
                                USB_EHCD_WARN("usbEhcdRhSetPortFeature - "
                                             "Port %d is a HIGH speed device\n",
                                             pSetup->wIndex, 0, 0, 0, 0, 0);
                                }

                            pURB->nStatus = USBHST_FAILURE;
                            }
                        break;
                        }
                    else
                        {
                        /*
                         * To hold the port status value indicating a
                         * reset change
                         */

                        UINT32 uPortStatus = 0;

                        /* WindView Instrumentation */

                        USB_HCD_LOG_EVENT(
                            USB_EHCI_WV_PORT_ROUTE,
                            "SetPortFeature : High speed device detected",
                            USB_EHCD_WV_FILTER);

                        /* Copy the value in the port status register */

                        OS_MEMCPY(&uPortStatus,
                                  (pHCDData->RHData.pPortStatus +
                                   (pSetup->wIndex - 1) * USB_EHCD_RH_PORT_STATUS_SIZE),
                                  USB_EHCD_RH_PORT_STATUS_SIZE);

                        /* Swap the data to LE format */

                        uPortStatus = USB_EHCD_SWAP_USB_DATA(uBusIndex,
                                                         uPortStatus);

                        /* Update the reset change */

                        uPortStatus |= USB_EHCD_RH_PORT_RESET_CHANGE;

                        /* Swap the data to LE format */

                        uPortStatus = USB_EHCD_SWAP_USB_DATA(uBusIndex,
                                                         uPortStatus);

                        /* Copy the status back to the port status */

                        OS_MEMCPY((pHCDData->RHData.pPortStatus +
                                   (pSetup->wIndex - 1) * USB_EHCD_RH_PORT_STATUS_SIZE),
                                  &uPortStatus,
                                  USB_EHCD_RH_PORT_STATUS_SIZE);

                        /*
                         * Call the function to populate the interrupt
                         * status data
                         */

                        usbEhcdCopyRHInterruptData(
                                 pHCDData,
                                 (UINT32)(USB_EHCD_RH_MASK_VALUE << (pSetup->wIndex - 1)));

                        }
                    }/* End of else */

                /* Update the status */

                pURB->nStatus = USBHST_SUCCESS;

                }/* End of if(0 == USB_EHCD_GET_FIELD(pHCDData,...*/

            /* Host Controller is halted */
            else
                {
                USB_EHCD_ERR("usbEhcdRhSetPortFeature - "
                             "Host controller is halted\n",
                             0, 0, 0, 0, 0, 0);

                pURB->nStatus = USBHST_FAILURE;
                }
            break;
            }/* End of case USB20_PORT_RESET */
        case USB_EHCD_RH_PORT_SUSPEND: /* If port suspend */
            {
            /* If only port is enabled, suspend the port */
            if (1 == USB_EHCD_GET_FIELD_PORT(pHCDData,
                                        (pSetup->wIndex) - 1,
                                        PORT_ENABLED_DISABLED))
                {
                /*
                 * To hold the port status value indicating a
                 * suspend change
                 */

                UINT32 uPortStatus = 0;

                /* Suspend port */

                USB_EHCD_SET_BIT_PORT(pHCDData,
                                  ((pSetup->wIndex) - 1),
                                  SUSPEND);

                /* Update the status */

                pURB->nStatus = USBHST_SUCCESS;

                /* Copy the value in the port status register */

                OS_MEMCPY(&uPortStatus,
                          (pHCDData->RHData.pPortStatus +
                           (pSetup->wIndex - 1) * USB_EHCD_RH_PORT_STATUS_SIZE),
                          USB_EHCD_RH_PORT_STATUS_SIZE);

                /* Swap the data to LE format */

                uPortStatus = USB_EHCD_SWAP_USB_DATA(uBusIndex,
                                                 uPortStatus);

                /* Update the suspend change */

                uPortStatus |=
                  (USB_EHCD_RH_PORT_SUSPEND_CHANGE |
                                     USB_EHCD_RH_PORT_SUSPEND_MASK);

                /* Swap the data to LE format */

                uPortStatus = USB_EHCD_SWAP_USB_DATA(uBusIndex,
                                                 uPortStatus);

                /* Copy the status back to the port status */

                OS_MEMCPY((pHCDData->RHData.pPortStatus +
                           (pSetup->wIndex - 1) * USB_EHCD_RH_PORT_STATUS_SIZE),
                          &uPortStatus,
                          USB_EHCD_RH_PORT_STATUS_SIZE);
                /*
                 * Call the function to populate the interrupt
                 * status data
                 */

                usbEhcdCopyRHInterruptData(
                         pHCDData,
                         (UINT32)(USB_EHCD_RH_MASK_VALUE << (pSetup->wIndex - 1)));
                }
                /* Cannot suspend if not enabled */
            else
                {
                USB_EHCD_ERR("usbEhcdRhSetPortFeature - "
                             "Port %d is not enabled for a suspend\n",
                             pSetup->wIndex, 0, 0, 0, 0, 0);

                pURB->nStatus = USBHST_FAILURE;
                }

            break;
            }/* End of case USB20_PORT_SUSPEND */
        case USB_EHCD_RH_PORT_POWER:/* Port Power */
            {
            /*
             * If only the host controller supports port
             * power switching, power on the port.
             */
            if (USB_EHCD_GET_FIELD(pHCDData,
                                HCSPARAMS,
                                PPC) == 1)
                {
                /* Switch on power on the port */

                USB_EHCD_SET_BIT_PORT(pHCDData,
                                      ((pSetup->wIndex) -1),
                                       PORT_POWER);
                }
            /* Update the status */

            pURB->nStatus = USBHST_SUCCESS;
            break;
            }
        case USB_EHCD_RH_PORT_INDICATOR:/* Port Indicator */
            {
            /* Set the Port indicator */

            USB_EHCD_SET_FIELD_PORT(pHCDData,
                     (pSetup->wIndex & USB_EHCD_RH_PORT_NUMBER_MASK) -1,
                      PORT_INDICATOR_CONTROL, pSetup->wIndex >> 8);

            /* Update the URB status */

            pURB->nStatus = USBHST_SUCCESS;
            break;
            }
        case USB_EHCD_RH_PORT_TEST: /* Port test */
            {
            /* Set the Port test */

            USB_EHCD_SET_FIELD_PORT(pHCDData,
                               (pSetup->wIndex & USB_EHCD_RH_PORT_NUMBER_MASK) -1,
                                PORT_TEST_CONTROL, pSetup->wIndex >> 8);

            /* Update the URB status */

            pURB->nStatus = USBHST_SUCCESS;
            break;
            }
        default :
            {
            USB_EHCD_ERR("usbEhcdRhSetPortFeature - Invalid request\n",
                0, 0, 0, 0, 0, 0);

            /* Update the URB status */

            pURB->nStatus = USBHST_INVALID_REQUEST;
            break;
            }
        }/* End of switch */

    return Status;
    }
    /* End of usbEhcdRhSetPortFeature() */

/***************************************************************************
*
* usbEhcdRhProcessClassSpecificRequest - processes a class specific request
*
* This routine processes a class specific request.
*
* RETURNS:
*  USBHST_SUCCESS if the URB is submitted successfully.
*  USBHST_INVALID_PARAMETER if the parameters are not valid.
*  USBHST_INSUFFICIENT_BANDWIDTH if memory is insufficient for the request.
*
* ERRNO:
*   None.
*/

USBHST_STATUS usbEhcdRhProcessClassSpecificRequest
    (
    pUSB_EHCD_DATA pHCDData,           /* Ptr to HCD block           */
    pUSBHST_URB    pURB                /* Ptr to User Request Block  */
    )
    {
    /* To hold the request status */

    USBHST_STATUS Status = USBHST_SUCCESS;

    /* Pointer to the setup packet */

    pUSBHST_SETUP_PACKET pSetup = NULL;

    /* Check the validity of the parameters */

    if (NULL == pHCDData ||
        NULL == pURB ||
        NULL == pURB->pTransferSpecificData)
        {
        USB_EHCD_ERR("usbEhcdRhProcessClassSpecificRequest - "
                     "Invalid parameters\n",
                     0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /*
     * Check if the Root hub is configured
     * to accept any class specific request
     */

    if (0 == pHCDData->RHData.uConfigValue)
        {
        USB_EHCD_ERR("usbEhcdRhProcessClassSpecificRequest - "
                     "Invalid request - device not configured\n",
                     0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_REQUEST;
        }
    /* Extract the setup packet from the URB */

    pSetup = (pUSBHST_SETUP_PACKET)pURB->pTransferSpecificData;

    /* Swap the 16 bit values */
    pSetup->wValue = OS_UINT16_CPU_TO_LE(pSetup->wValue);
    pSetup->wIndex = OS_UINT16_CPU_TO_LE(pSetup->wIndex);
    pSetup->wLength = OS_UINT16_CPU_TO_LE(pSetup->wLength);

    /* Handle the class specific request */

    switch(pSetup->bRequest)
        {
        case USBHST_REQ_CLEAR_FEATURE:/* Clear feature request */
            {
            /* Based on the recipient value, handle the request */
            switch (pSetup->bmRequestType & USB_EHCD_RH_RECIPIENT_MASK)
                {
                case USBHST_RECIPIENT_DEVICE:
                    {
                    /* None of the hub class features are supported */
                    pURB->nStatus = USBHST_INVALID_REQUEST;
                    break;
                    }
                case USBHST_RECIPIENT_OTHER: /* Clear Port Feature request */
                    {
                    /*
                     * Call the function which handles
                     * the clear port feature request
                     */

                    Status = usbEhcdRhClearPortFeature(pHCDData, pURB);
                    break;
                    }
                default :
                    {
                    pURB->nStatus = USBHST_INVALID_REQUEST;
                    break;
                    }

                }
            break;
            }
        case USBHST_REQ_GET_DESCRIPTOR:/* Hub Get Descriptor request */
            {
            /*
             * Call the function to handle the
             * Root hub Get descripor request.
             */

            Status = usbEhcdRhGetHubDescriptor(pHCDData, pURB);
            break;
            }
        case USBHST_REQ_GET_STATUS:/* Get status request */
            {
            /* Based on the recipient value, handle the request */
            switch (pSetup->bmRequestType & USB_EHCD_RH_RECIPIENT_MASK)
                {
                case USBHST_RECIPIENT_DEVICE:
                    {
                    /*
                     * The hub status cannot be retrieved. So send the status
                     * always as zero
                     */

                    OS_MEMSET(pURB->pTransferBuffer, 0, USB_EHCD_HUBSTATUS_SIZE);

                    /* Update the URB status */

                    pURB->nStatus = USBHST_SUCCESS;
                    break;
                    }
                case USBHST_RECIPIENT_OTHER: /* GetPortStatus request */
                    {
                    /*
                     * Call the function which handles
                     * the Get Port Status request
                     */
                    Status = usbEhcdRhGetPortStatus(pHCDData, pURB);
                    break;
                    }
                default :
                    {
                    pURB->nStatus = USBHST_INVALID_REQUEST;
                    break;
                    }
                }
            break;
            }
        case USBHST_REQ_SET_FEATURE:/* Set Feature request */
            {
            /* Based on the recipient value, handle the request */

            switch (pSetup->bmRequestType & USB_EHCD_RH_RECIPIENT_MASK)
                {
                case USBHST_RECIPIENT_DEVICE:
                    {
                    /*
                     * None of the sethubfeature requests are supported
                     * in the Root hub
                     */
                    /* Update the URB status */

                    pURB->nStatus = USBHST_INVALID_REQUEST;

                    break;
                    }
                case USBHST_RECIPIENT_OTHER: /* GetPortStatus request */
                    {
                    /*
                     * Call the function which handles
                     * the Get Port Status request
                     */
                    Status = usbEhcdRhSetPortFeature(pHCDData, pURB);
                    break;
                    }
                default :
                    {
                    /* Invalid request */
                    pURB->nStatus = USBHST_INVALID_REQUEST;
                    break;
                    }

                }

            break;
            }
        /*
         * Some EHCI controllers such as those in some FSL SOCs have
         * embeededTT built into the EHCI itself. We should handle these
         * requests!
         *
         * However, as of this writing there is no offical documentation
         * to tell us how to handle this in the hardware level. So we just
         * log this right now! We will investigate this further.
         */

        case USB_EHCD_RH_REQ_CLEAR_TT_BUFFER: /* CLEAR_TT_BUFFER */
            {
            USB_EHCD_DBG(
                "usbEhcdRhProcessClassSpecificRequest - "
                "CLEAR_TT_BUFFER :\n"
                "bRequest 0x%X wValue 0x%X wIndex 0x%X wLength 0x%X\n",
                pSetup->bRequest,
                pSetup->wValue,
                pSetup->wIndex,
                pSetup->wLength, 5, 6);

            pURB->nStatus = USBHST_FAILURE;
            break;
            }
        case USB_EHCD_RH_REQ_RESET_TT: /* RESET_TT */
            {
            USB_EHCD_DBG(
                "usbEhcdRhProcessClassSpecificRequest - "
                "RESET_TT :\n"
                "bRequest 0x%X wValue 0x%X wIndex 0x%X wLength 0x%X\n",
                pSetup->bRequest,
                pSetup->wValue,
                pSetup->wIndex,
                pSetup->wLength, 5, 6);

            pURB->nStatus = USBHST_FAILURE;
            break;
            }
        case USB_EHCD_RH_REQ_GET_TT_STATE: /* GET_TT_STATE */
            {
            USB_EHCD_DBG(
                "usbEhcdRhProcessClassSpecificRequest - "
                "GET_TT_STATE :\n"
                "bRequest 0x%X wValue 0x%X wIndex 0x%X wLength 0x%X\n",
                pSetup->bRequest,
                pSetup->wValue,
                pSetup->wIndex,
                pSetup->wLength, 5, 6);

            pURB->nStatus = USBHST_FAILURE;
            break;
            }
        case USB_EHCD_RH_REQ_STOP_TT: /* STOP_TT */
            {
            USB_EHCD_DBG(
                "usbEhcdRhProcessClassSpecificRequest - "
                "STOP_TT :\n"
                "bRequest 0x%X wValue 0x%X wIndex 0x%X wLength 0x%X\n",
                pSetup->bRequest,
                pSetup->wValue,
                pSetup->wIndex,
                pSetup->wLength, 5, 6);

            pURB->nStatus = USBHST_FAILURE;
            break;
            }
        default:
            {
            /* Invalid request */
            USB_EHCD_DBG(
                "usbEhcdRhProcessClassSpecificRequest - "
                "USBHST_INVALID_REQUEST :\n"
                "bRequest 0x%X wValue 0x%X wIndex 0x%X wLength 0x%X\n",
                pSetup->bRequest,
                pSetup->wValue,
                pSetup->wIndex,
                pSetup->wLength, 5, 6);

            pURB->nStatus = USBHST_INVALID_REQUEST;
            }

        }/* End of switch */

    /* If a callback function is registered, call the callback function */

    if (NULL != pURB->pfCallback)
        {
        (pURB->pfCallback)(pURB);
        }

    return Status;

    }
    /* End of usbEhcdRhProcessClassSpecificRequest() */

/***************************************************************************
*
* usbEhcdRHCancelURB - cancels a request submitted for an endpoint
*
* This routine cancels a request submitted for an endpoint.
*
* RETURNS:
*  USBHST_SUCCESS if the URB is submitted successfully.
*  USBHST_INVALID_PARAMETER if the parameters are not valid.
*  USBHST_INSUFFICIENT_BANDWIDTH if memory is insufficient for the request.
*
* ERRNO:
*   None.
*/
USBHST_STATUS usbEhcdRHCancelURB
    (
    pUSB_EHCD_DATA pHCDData,           /* Ptr to HCD block           */
    ULONG          uPipeHandle,        /* Pipe Handle Identifier     */
    pUSBHST_URB    pURB                /* Ptr to User Request Block  */
    )
    {
    /* Pointer to the HCD maintained pipe */

    pUSB_EHCD_PIPE pHCDPipe = NULL;

    /* To hold the request information */

    pUSB_EHCD_REQUEST_INFO  pRequestInfo = NULL;

    /* To hold the temporary request information */

    pUSB_EHCD_REQUEST_INFO  pTempRequestInfo = NULL;


    /* Check the validity of the parameters */

    if ((NULL == pHCDData) ||
        (0 == uPipeHandle) ||
        (NULL == pURB))
        {
        USB_EHCD_ERR("usbEhcdRhCancelURB - Invalid parameters\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the pipe data structure */

    pHCDPipe = (pUSB_EHCD_PIPE)uPipeHandle;

    /* Exclusively access the list */

    OS_WAIT_FOR_EVENT(pHCDData->RequestSynchEventID,OS_WAIT_INFINITE);



    /* Search for the URB's request element */

    pTempRequestInfo = pHCDPipe->pRequestQueueHead;
    if (NULL != pTempRequestInfo) 
        {
        if (pURB == pTempRequestInfo->pUrb) /* Check the first element*/
            pRequestInfo = pTempRequestInfo;
        else  /* Search all nonfirst element */
            {
            while (NULL != pTempRequestInfo->pNext)
                {
                if (pURB == pTempRequestInfo->pNext->pUrb)
                    {
                    pRequestInfo = pTempRequestInfo;
                    break;
                    }
                pTempRequestInfo = pTempRequestInfo->pNext;
                }
            }
        }
    pTempRequestInfo = NULL;

    /* If the request is not found, return an error */

    if (NULL == pRequestInfo)
        {
        USB_EHCD_ERR("usbEhcdRhCancelURB - Request is not present\n",
            0, 0, 0, 0, 0, 0);

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDData->RequestSynchEventID);

        return USBHST_INVALID_REQUEST;

        }

    /* Assert if the HCDPipe pointers do not match */

    OS_ASSERT(pRequestInfo->pHCDPipe == pHCDPipe);

    /* Check if it is the head element */

    if (pRequestInfo == pHCDPipe->pRequestQueueHead)
        {
        /* If it is the only element,
         * update the head and tail pointers to NULL.
         */
        if (pRequestInfo == pHCDPipe->pRequestQueueTail)
            {
            pHCDPipe->pRequestQueueTail = NULL;
            pHCDPipe->pRequestQueueHead = NULL;
            }
        /* Update the head element */

        else
            {
            pHCDPipe->pRequestQueueHead = pRequestInfo->pNext;
            }
        }
    else
        {
        /* Store the previous pointer in the temporary pointer */

        pTempRequestInfo = pRequestInfo;

        /* Update the request info pointer to point to the actual request */

        pRequestInfo = pRequestInfo->pNext;

        /* Update the tail if the request is the tail */

        if (pHCDPipe->pRequestQueueTail == pRequestInfo)
            {
            pHCDPipe->pRequestQueueTail = pTempRequestInfo;
            }

        /* Update the next pointers */

        pTempRequestInfo->pNext = pRequestInfo->pNext;
        }

    /* Release the exclusive access */

    OS_RELEASE_EVENT(pHCDData->RequestSynchEventID);

    /* update the URB status to cancelled */

    pRequestInfo->pUrb->nStatus = USBHST_TRANSFER_CANCELLED;

    /* Call the callback function if it is registered */

    if (NULL != pRequestInfo->pUrb->pfCallback)
        {
        (pRequestInfo->pUrb->pfCallback)(pRequestInfo->pUrb);
        }

    /* Free the memory allocated for the request information */

    OS_FREE(pRequestInfo);

    /* Return a success status */

    return USBHST_SUCCESS;
    }
    /* End of usbEhcdRhCancelURB() */
/*********************** End of file EHCD_RHEmulation.c************************/
