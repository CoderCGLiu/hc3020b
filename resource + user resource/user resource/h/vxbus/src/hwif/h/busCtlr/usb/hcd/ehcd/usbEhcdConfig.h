/* usbEhcdConfig.h - Utility Functions for EHCI */

/*
 * Copyright (c) 2004, 2010 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2004, 2010 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
modification history
--------------------
01a,13jan10,ghs  vxWorks 6.9 LP64 adapting
*/

/*
DESCRIPTION
This contains the configuration parameters of the USB
EHCD.
*/

/*
INTERNAL
 *******************************************************************************
 * Filename         : usbEhcdConfig.h
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      :  This contains the configuration parameters of the USB
 *                     EHCD.
 *
 ******************************************************************************/
#ifndef __INCehcdConfigh
#define __INCehcdConfigh

#ifdef  __cplusplus
extern "C" {
#endif

#define USB_EHCD_VENDOR_ID               0x1033  /*
                                                  * Vendor ID of the EHCI
                                                  * Host Controller
                                                  */
#define USB_EHCD_DEVICE_ID               0x00E0  /*
                                                  * Device ID of the EHCI
                                                  * Host Controller
                                                  */

#ifdef  __cplusplus
}
#endif

#endif /* End of __INCehcdConfigh */
/***************************** End of file usbEhcdConfig.h*********************/
