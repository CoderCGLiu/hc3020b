/* usbMhdrcPlatformDavinci.h - MUSBMHDRC DAVINCI specific definitions */

/*
 * Copyright (c) 2009-2011, 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01d,21apr13,wyy  Make the preprocessor look for usbMhdrc.h in the
                 current directory first
01c,09mar11,w_x  Code clean up for make man
01b,13mar10,s_z  Add platform related definations
01a,13jul09,j_x  initial version
*/

/*
DESCRIPTION

This file defines the DAVINCI special register offsets and macros for the MHCI
Host Controller.

INCLUDE FILES: usbMhdrc.h
*/

#ifndef __INCusbMhdrcPlatformDavincih
#define __INCusbMhdrcPlatformDavincih

#ifdef __cplusplus
extern "C" {
#endif

/* includes */

#include "usbMhdrc.h" 

/* defines */

/* Offset of the MHCI Controller Revision Register */
#define USB_MHDRC_REVISION               0x00

/* Offset of the MHCI Controller Control Register */
#define USB_MHDRC_CONTROL                0x04

/* Offset of the MHCI Controller Status Register */
#define USB_MHDRC_STATUS                 0x08

/* Offset of the MHCI Controller RNDIS Register */
#define USB_MHDRC_RNDIS                  0x10

/* Offset of the MHCI Controller Auto Request Register */
#define USB_MHDRC_AUTOREQ                0x14

/* Offset of the MHCI Controller Interrupt Source Register */
#define USB_MHDRC_INTSRC                 0x20

/* Offset of the MHCI Controller Interrupt Source Set Register */
#define USB_MHDRC_INTSET                 0x24

/* Offset of the MHCI Controller Interrupt Source Clear Register */
#define USB_MHDRC_INTCLR                 0x28

/* Offset of the MHCI Controller Interrupt Mask Register */
#define USB_MHDRC_INTMSK                 0x2C

/* Offset of the MHCI Controller Interrupt Mask Set Register */
#define USB_MHDRC_INTMSKSET              0x30

/* Offset of the MHCI Controller Interrupt Mask Clear Register */
#define USB_MHDRC_INTMSKCLR              0x34

/* Offset of the MHCI Controller Interrupt Source Mask Register */
#define USB_MHDRC_INTMASKED              0x38

/* Offset of the MHCI Controller End of Interrupt Register */
#define USB_MHDRC_EOI                    0x3C

/* Offset of the MHCI Controller Interrupt Vector Register */
#define USB_MHDRC_INTVECT                0x40

/* Offset of the MHCI Controller Transmit CPPI Control Register */
#define USB_MHDRC_TCPPIC                 0x80

/* Offset of the MHCI Controller Transmit CPPI Teardown Register */
#define USB_MHDRC_TCPPITD                0x84

/* Offset of the MHCI Controller CPPI DMA End of Interrupt Register */
#define USB_MHDRC_CPPIEOI                0x88

/* Offset of the MHCI Controller Transmit CPPI Masked Status Register */
#define USB_MHDRC_TCPPIMSKS              0x90

/* Offset of the MHCI Controller Transmit CPPI Raw Status Register */
#define USB_MHDRC_TCPPIRAWS              0x94

/* Offset of the MHCI Controller Transmit CPPI Interrupt Enable Set Register */
#define USB_MHDRC_TCPPIIENSET            0x98

/* Offset of the MHCI Controller Transmit CPPI Interrupt Enable Clear Register */
#define USB_MHDRC_TCPPIIENCLR            0x9C

/* Offset of the MHCI Controller Receive CPPI Control Register */
#define USB_MHDRC_RCPPIC                 0xC0

/* Offset of the MHCI Controller Receive CPPI Masked Status Register */
#define USB_MHDRC_RCPPIMSKS              0xD0

/* Offset of the MHCI Controller Receive CPPI Raw Status Register */
#define USB_MHDRC_RCPPIRAWS              0xD4

/* Offset of the MHCI Controller Receive CPPI Interrupt Enable Set Register */
#define USB_MHDRC_RCPPIIENSET            0xD8

/* Offset of the MHCI Controller Receive CPPI Interrupt Enable Clear Register */
#define USB_MHDRC_RCPPIIENCLR            0xDC

/* Offset of the MHCI Controller Receive Buffer Count 0 Register */
#define USB_MHDRC_RBUFCNT0               0xE0

/* Offset of the MHCI Controller Receive Buffer Count 1 Register */
#define USB_MHDRC_RBUFCNT1               0xE4

/* Offset of the MHCI Controller Receive Buffer Count 2 Register */
#define USB_MHDRC_RBUFCNT2               0xE8

/* Offset of the MHCI Controller Receive Buffer Count 3 Register */
#define USB_MHDRC_RBUFCNT3               0xEC

/* Transmit/Receive CPPI Channel 0 State Block */

/* Offset of the MHCI Controller Transmit CPPI DMA State Word 0 Register */
#define USB_MHDRC_TCPPIDMASTATEW0_CH(n)      (0x100 + (n) * 40)

/* Offset of the MHCI Controller Transmit CPPI DMA State Word 1 Register */
#define USB_MHDRC_TCPPIDMASTATEW1_CH(n)      (0x104 + (n) * 40)

/* Offset of the MHCI Controller Transmit CPPI DMA State Word 2 Register */
#define USB_MHDRC_TCPPIDMASTATEW2_CH(n)      (0x108 + (n) * 40)

/* Offset of the MHCI Controller Transmit CPPI DMA State Word 3 Register */
#define USB_MHDRC_TCPPIDMASTATEW3_CH(n)      (0x10C + (n) * 40)

/* Offset of the MHCI Controller Transmit CPPI DMA State Word 4 Register */
#define USB_MHDRC_TCPPIDMASTATEW4_CH(n)      (0x110 + (n) * 40)

/* Offset of the MHCI Controller Transmit CPPI DMA State Word 5 Register */
#define USB_MHDRC_TCPPIDMASTATEW5_CH(n)      (0x114 + (n) * 40)

/* Offset of the MHCI Controller Transmit CPPI Completion Pointer Register */
#define USB_MHDRC_TCPPICOMPPTR_CH(n)         (0x11C + (n) * 40)

/* Offset of the MHCI Controller Receive CPPI DMA State Word 0 Register */
#define USB_MHDRC_RCPPIDMASTATEW0_CH(n)      (0x120 + (n) * 40)

/* Offset of the MHCI Controller Receive CPPI DMA State Word 1 Register */
#define USB_MHDRC_RCPPIDMASTATEW1_CH(n)      (0x124 + (n) * 40)

/* Offset of the MHCI Controller Receive CPPI DMA State Word 2 Register */
#define USB_MHDRC_RCPPIDMASTATEW2_CH(n)      (0x128 + (n) * 40)

/* Offset of the MHCI Controller Receive CPPI DMA State Word 3 Register */
#define USB_MHDRC_RCPPIDMASTATEW3_CH(n)      (0x12C + (n) * 40)

/* Offset of the MHCI Controller Receive CPPI DMA State Word 4 Register */
#define USB_MHDRC_RCPPIDMASTATEW4_CH(n)      (0x130 + (n) * 40)

/* Offset of the MHCI Controller Receive CPPI DMA State Word 5 Register */
#define USB_MHDRC_RCPPIDMASTATEW5_CH(n)      (0x134 + (n) * 40)

/* Offset of the MHCI Controller Receive CPPI DMA State Word 6 Register */
#define USB_MHDRC_RCPPIDMASTATEW6_CH(n)      (0x138 + (n) * 40)

/* Offset of the MHCI Controller Receive CPPI Completion Pointer Register */
#define USB_MHDRC_RCPPICOMPPTR_CH(n)         (0x13C + (n) * 40)

#define USB_MHDRC_DAVINCI_COMMON_REG_OFFSET  0x400

#define USB_MHDRC_DAVINCI_PRIVATE_REG_WRITE32(pMHDRC, OFFSET, VALUE)         \
    vxbWrite32(((pUSB_MUSBMHDRC)(pMHDRC))->pRegAccessHandle,                 \
        (void *)(((pUSB_MUSBMHDRC)(pMHDRC))->uRegBase + OFFSET),              \
        VALUE)

#define USB_MHDRC_DAVINCI_PRIVATE_REG_READ32(pMHDRC, OFFSET)                 \
    vxbRead32(((pUSB_MUSBMHDRC)(pMHDRC))->pRegAccessHandle,                  \
        (void *)(((pUSB_MUSBMHDRC)(pMHDRC))->uRegBase + OFFSET))


#ifdef __cplusplus
}
#endif

#endif /* __INCusbMhdrcPlatformDavincih */


