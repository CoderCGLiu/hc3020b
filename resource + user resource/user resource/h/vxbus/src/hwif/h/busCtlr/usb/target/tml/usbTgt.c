/* usbTgt.c - USB Taget Transfer and Management Module */

/*
 * Copyright (c) 2010-2015 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
02c,05jan15,lan  Add WCID support (VXW6-83565)
02b,10sep14,lan  check the buffer USBTGT_TCD::dataBfr overflow (VXW6-83141)
02a,06jul14,j_x  Add support for HS/FS if SS table set to NULL (VXW6-83095) 
01z,06may13,s_z  Remove compiler warning (WIND00356717)
01y,24apr13,wyy  Clean warnings of code coverity
01x,04jan13,s_z  Remove compiler warning (WIND00390357)
01w,25oct12,s_z  MHDRC TCD pass CV testing (WIND00385197) 
01v,17oct12,s_z  Fix short packet receive issue (WIND00374594)
01u,15oct12,s_z  Add USBTGT_TCD_MAX_CONFIG_COUNT (WIND00382057)
01t,18sep12,s_z  Pass CV2.0 testing (WIND00375402)
01s,03sep12,s_z  Fix BOS buffer using issue (WIND00374269)
                 Add usbTgtTcdRegister/usbTgtTcdUnRegister API
01r,18may12,s_z  Add support for USB 3.0 target (WIND00326012)
01q,16apr12,ljg  Change printf to be USB_TGT_INFO (WIND00328214)
01p,24feb12,s_z  Delete function node from TCD funcList while detaching
                 function driver from TCD (WIND00328618)
01o,13dec11,m_y  Modify according to code check result (WIND00319317)
01n,10oct11,m_y  modify to fix the code coverity defect (WIND00310604)
01m,28jul11,s_z  Fix IAD description generation issue, adjust the EP0 
                 max packet size accroding to the device speed, correct
                 usbTgtDevShow routine                 
01l,12jul11,s_z  Fix string descriptor management issue
01k,08apr11,ghs  Fix code coverity issue(WIND00264893)
01j,07apr11,ghs  Disconnect from host before remove any function driver
                 (WIND00265861)
01i,30mar11,s_z  Correct the IAD using to support composite device
                 Changed based on the redefinintion of struct usbtgt_device_info
                 Add the multiple language support
01h,28mar11,s_z  Update according to some routines changes
01g,22mar11,s_z  Remove unused routines
01f,09mar11,s_z  Code clean up
01e,23feb11,s_z  Add IAD and function device descriptor support
01d,18feb11,w_x  Correct usbTgtShow
01c,17feb11,s_z  Correct some comments 
                 and integrate devQualifierDesc support
01b,30jan11,ghs  Extern usbTgtErpCallback for target serial emulator
01a,30jun10,s_z  written
*/

/*
DESCRIPTION

 This module implements the USB target transfer and management module of the USB
 target stack for the VxWorks operating system. This is written using the Wind 
 River USB 2.x API.

VXWORKS USB TARGET STACK OVERVIEW

 The chart below is a simplified architecture of the whole USB target stack:

\cs
         +-----------------------------------------+
         |             User Application            |
         |            (ioLib,Network,Fs...)        |
         +--------------^----------@---------------+
                        |          |
                        |          |
         +--------------@----------v---------------+
         |                   TFD                   |
         |        USB Target Function Driver(s)    |
         |                                         |
         | +---------------+     +---------------+ |
         | |Mass Storage   |     |RNDIS          | |
         | |Function Driver| ... |Function Driver| |
         | +---------------+     +---------------+ |
         +--------------^----------@---------------+
                        |          |
                        |          |
         +--------------@----------v---------------+
         |                   TML                   |
         |       USB Target Management Layer       |
         |                                         |
         |+--------++----------++-----++----------+|
         ||Control ||Hardware  ||Data ||Registra- ||
         ||Requests||Events    ||Tx/Rx||tion      ||
         ||        ||(Reset...)||     ||Management||
         |+--------++----------++-----++----------+|
         +--------------^----------@---------------+
                        |          |
                        |          |
         +--------------@----------v---------------+
         |                  TCD                    |
         |      USB Target Controller Driver(s)    |
         +--------------^----------@---------------+
                        |          |
                        |          |
         +--------------@----------v---------------+
         |                Hardware                 |
         |      USB Target Controller Hardware     |
         +-----------------------------------------+
\ce

\is
\i Hardware
   Which is the USB target controller hardware
\i TCD
   The USB target controller driver(TCD) who need be registered to the target
   management layer(TML).
\i TML
   The USB target management layer(TML), which contains the process of all the 
   control requests and the hardware events, and the transfer the data flow 
   between the TCD and TFL.
\i TFL
   The USB target function driver layer(TFL). This layer contains different
   independent function drivers. If you need use one kind of function on the
   target, you need to register the function driver to the TML, and make sure
   the correct TCD name which need be attached to has been configured.
\i User Application
   The user application may needed by the TFL. Different function 
   driver may use different applications. Such as the network stack application
   for the RNDIS virtual END driver or the FS framework for the mass storage 
   function driver. This is totally depend on the function driver and the 
   user's use case.
\ie

INITIALIZATION 

 To use this module, please make sure the usbTgtInit() routine has been called
 in advance and successfully. This routine will initialize two lists 
 (gUsbTgtTcdList and gUsbTgtFuncDriverList), which will be used when a new
 TCD or function driver be added to the target stack.

DATA STRUCTURES

\h 1. USBTGT_TCD

Each USB Target Controller will have a USBTGT_TCD data structure, which plays
the core role to connect the TCD with function drivers. The structure is 
defined as below:

\cs

/@
 * USB Target Management Abstraction
 @/

typedef struct usbtgt_tcd
    {
    NODE                  usbTgtTcdNode; /@ Tcd node in the global list @/
    SEM_ID                tcdMutex;      /@ Used to protect the resource @/
    LIST                  funcList;      /@ Function drivers list @/
    LIST                  configList;    /@ Configuration list @/
    LIST                  strDescList;   /@ String descriptors list @/    
    
    atomic_t              controlErpUsed; /@ The control ERP is used or not @/    
    USB_TARG_PIPE         controlPipe;    /@ Handle to default control pipe @/
    USB_ERP               controlErp;     /@ Erp used to transfer to/from @/
    USB_SETUP             SetupPkt;       /@ Setup pecket @/
    UINT8                 dataBfr[USBTGT_MAX_CONTORL_BUF_SIZE]; /@ Sending/receiving data @/
    
    /@ User configurable parameters @/

    UINT32                uTCDMagicID;  /@ TCD magic ID @/
    UINT16                uVendorID;    /@ VID @/
    UINT16                uProductID;   /@ PID @/
    char *                pMfgString;   /@ Manufacture string @/
    char *                pProdString;  /@ Producture string @/
    char *                pSerialString;/@ Serial num string @/
    char *                pTcdName;     /@ TCD name @/
    UINT8                 uTcdUnit;     /@ Tcd unit number @/

    /@ Device specific parameters @/

    VXB_DEVICE_ID         pDev;         /@ Pointer to VxBus Device instance @/
    USBTGT_TCD_FUNCS *    pTcdFuncs;    /@ Functions implemented by the TCD @/
    pVOID                 pTcdSpecific; /@ Pointer to TCD specific structure @/
    USBTGT_DEVICE_INFO    DeviceInfo;   /@ Device information @/
    
    USB_DEVICE_DESCR      DeviceDesc;   /@ device Descriptor @/
    USB_DEVICE_QUALIFIER_DESCR devQualifierDesc;/@ Device qualifier descriptor @/
    USB_LANGUAGE_DESCR *  pUsbTgtLangDescr; /@ Language descriptor @/
    
    UINT8                 uSpeed;
    UINT8                 uDeviceAddress; /@ Device Address @/
    UINT8                 uAddrToSet;     /@ Address we want to set @/
    UINT8                 uCurrentConfig; /@ 0 means the target is unconfigured @/
    BOOL                  bWakeupEnable;  /@ Is the target enabled the wakeup feature @/
    
    }USBTGT_TCD, *pUSBTGT_TCD;

\ce

The following items describe the major elements of the USBTGT_TCD data
structure.

\h 1.1 <'USBTGT_TCD::usbTgtTcdNode'> 

This element is used to link the resource to the global TCD list
<'gUsbTgtTcdList'>. So the target mamagement can find the right <'pUSBTGT_TCD'>
from the list to connect/disconnect with the right function driver 
<'pUSBTGT_FUNC_DRIVER'>. Specially when the TCD has initialized, and the user
can dynamicly attach/detach the function driver. The target mamagement can find the 
right <'pUSBTGT_TCD'> for using.

\h 1.2 <'USBTGT_TCD::tcdMutex'> 

This is a mutex semaphore which is used to protect the USBTGT_TCD elements
being concurrent updated.

\h 1.3 <'USBTGT_TCD::funcList'> 

This element is the function driver list which links all the function driver
attached to the TCD.This is used for the support of multiple function drivers
on one TCD.

\h 1.4 <'USBTGT_TCD::configList'> 

This element is the configuration list which links all the configurations
belong to the TCD. The user may configure more than one configuration on the 
TCD.

\h 1.5 <'USBTGT_TCD::strDescList'> 

This element is the string descriptor list which links all the string for the
TCD. Such as the string descriptor for different configuration, different
interfaces. This string descriptor list also used to management the descriptor
index used by each members.


\h 1.6 <'USBTGT_TCD::controlPipe'>, <'USBTGT_TCD::controlErp'> and <'USBTGT_TCD::controlErpUsed'>

Those elements are used for the control endpoint to transfer the control
requests and responce. <'USBTGT_TCD::controlPipe'> will be created/re-created 
when get one BUS RESET by the USB target controller, according to the working 
speed of the controller. All the transaction will use the 
<'USBTGT_TCD::controlErp'> which need be initialized before submitted by 
calling the routine <'usbTgtSubmitControlErp()'>.
Meanwhile, the <'USBTGT_TCD::controlErpUsed'> element is the flag which is
used to indicate whether the control pipe is using or not.

There are there kinds of stage of control transfer, which are SETUP stage, DATA IN/OUT 
stage and STATUS IN/OUT stage. Following are the APIs to be used to initialize
the transaction for the control pipe. Please go to <'usbTgtControlRequest.c'> for
detail.

\is

\i <'usbTgtInitSetupErp()'> 

Initialize the control ERP for the SETUP stage.

\i <'usbTgtInitDataErp()'>

Initialize the control ERP for the DATA IN/OUT stage.

\i <'usbTgtInitStatusErp'>

Initialize the control ERP for the STATUS IN/OUT stage.

\i <'usbTgtControlResponseSend'>

Initialize the control ERP for the DATA IN stage, which is an interface for 
the function drivers.

\i <'usbTgtControlPayloadRcv'>

Initialize the control ERP for the DATA OUT stage, which is an interface for
the function drivers.

\i <'usbTgtControlStatusSend'>

Initialize the control ERP for the DATA OUT stage, which is an interface for 
the function drivers.

\ie


\h 1.7 <'USBTGT_TCD::SetupPkt'> and <'USBTGT_TCD::dataBfr'>

<'USBTGT_TCD::SetupPkt'> is used to store the received SETUP packet from the USB
host.<'USBTGT_TCD::dataBfr'> is used to store the data to be sent to or 
recieved from the USB host. <'USBTGT_MAX_CONTORL_BUF_SIZE'> is the buffer size
of the <'USBTGT_TCD::dataBfr'>. The user can adjust it according to his needed.

\h 1.8 <'USBTGT_TCD::uTCDMagicID'>

TODO : s_z want to remove it 

This element is used to mark the valid TCD controller type. The user can see what
is the hardware USB target controller is attached.

\h 1.9 <'USBTGT_TCD::uVendorID'> and <'USBTGT_TCD::uProductID'>

Those two elements are get from the user's configuration or the default 
value if the user do not set them.They are indentified the USB target's VID/PID.
They are very usful on the host driver (such as the vendor's class driver on
Windows).


\h 1.10 <'USBTGT_TCD::pMfgString'>, <'USBTGT_TCD::pProdString'> and <'USBTGT_TCD::pSerialString'>

Those elements are also get from the user's configuration or the default 
value if the user do not set them. They provide the device description 
information which can be read by the USB host. <'USBTGT_TCD::pMfgString'> is
the manufacture string of the USB target, <'USBTGT_TCD::pProdString'> is
product string of the USB target and  <'USBTGT_TCD::pSerialString'> is the 
serial string of the USB target. 

\h 1.11 <'USBTGT_TCD::pTcdName'> and <'USBTGT_TCD::uTcdUnit'>

Those two elements indicate the TCD's name and unit number be managed by the 
USB target management.

\h 1.12 <'USBTGT_TCD::pDev'> 

<'USBTGT_TCD::pDev'> specifics the VxBus device instance, if this TCD 
compatible with the VxBus

\h 1.13 <'USBTGT_TCD::pTcdFuncs'> and <'USBTGT_TCD::pTcdSpecific'>

Those two elements are used for the TCD. The TCD need specific them befor 
register to the TML by calling the routine <'usbTgtTcdAdd()'>.

<'USBTGT_TCD::pTcdFuncs'> is the USB target controller driver(TCD) function 
driver table, which contains the useful routines such as submitErp function 
pointer which can be called by the TML.

<'USBTGT_TCD::pTcdSpecific'> is the TCD spcific data structure pointer, it 
used to identify the validation of the real hardware which can be used or not.

\h 1.14 <'USBTGT_TCD::DeviceInfo'>

<'USBTGT_TCD::DeviceInfo'> specifics the TCD hardware feature. Such as the 
endpoints features and controller features.

\h 1.15 <'USBTGT_TCD::DeviceDesc'> 

This element is the device descriptor structure which is used to store the 
device descriptor of the target.

\h 1.16 <'USBTGT_TCD::devQualifierDesc'> 

This element is the device qualifier descriptor structure which is used to 
store the device qualifier descriptor of the target if it have one.

\h 1.17 <'USBTGT_TCD::pUsbTgtLangDescr'> 

This element is the language descriptor structure pointer which is used to 
indicate the language supported of all the string descriptor of the target.

\h 1.18 <'USBTGT_TCD::uSpeed'>

This element indicates the current speed the TCD controller working at.

\h 1.19 <'USBTGT_TCD::uDeviceAddress'> and <'USBTGT_TCD::uAddrToSet'> 

This two elements indicate the current devcie address and the address to be 
set by the host.

\h 1.20 <'USBTGT_TCD::uDeviceAddress'> and <'USBTGT_TCD::uAddrToSet'> 

This two elements indicate the current devcie address and the address to be 
set by the host.

\h 1.21 <'USBTGT_TCD::uCurrentConfig'> 

<'USBTGT_TCD::uCurrentConfig'> specifics the current configuration set by the 
USB host. It must be one of the <'USBTGT_TCD::configList'> 

\h 2. USBTGT_FUNC_DRIVER

Each USB function driver will have a USBTGT_FUNC_DRIVER data structure, which 
plays the core role to connect the TCD with function drivers. The structure is 
defined as below:

\cs

/@
 * USB Target Function Driver Abstraction
 @/

typedef struct usbtgt_funcion_driver
    {
    NODE                       funcDriverNode;       /@ Function driver node in global list @/
    NODE                       funcInTcdListNode;    /@ Function driver node in TCD list @/
    VXB_DEVICE_ID              pDev;                 /@ Pointer to VxBus Device instance @/    
    SEM_ID                     funcMutex;            /@ Used to protect the resource @/
    UINT32                     uFuncMagicID;         /@ Func magic Id @/
    pVOID                      pFuncSpecific;        /@ Specific data @/    
    USB_DESCR_HDR **           ppFsFuncDescTable;    /@ Function descriptors table @/
    USB_DESCR_HDR **           ppHsFuncDescTable;    /@ Function descriptors table in High speed mode @/
    USB_DESCR_HDR **           ppSsFuncDescTable;    /@ Function descriptors table in Super speed mode @/
    USB_TARG_CALLBACK_TABLE *  pFuncCallbackTable;   /@ Function driver callback table @/    
    pVOID                      pCallbackParam;       /@ Callback parameter @/    
    USB_TARG_CHANNEL           targChannel;          /@ Targ channel @/
    LIST                       pipeList;             /@ List of the pipes @/
    struct usbtgt_tcd *        pTcd;                 /@ Attach to the TCD @/
    char *                     pFuncName;            /@ Function driver name @/
    char *                     pTcdName;             /@ Used for the TCD's name @/
    UINT8                      uFuncUnit;            /@ Function driver unit @/
    UINT8                      uTcdUnit;             /@ Used for the TCD's unit @/    
    UINT8                      uConfigToBind;        /@ The configuration group in @/
    UINT8                      uCurrentConfig;       /@ The configuration num included in @/    
    pUSBTGT_CONFIG             pConfig;
    UINT8 *                    pDescBuf;
    UINT16                     uDescBufLen;
    UINT8                      uIfNumUsed;           /@ How many ifterface in this function @/
    UINT8                      uIfNumMin;            /@ The min interface num @/
    UINT8                      uIfNumMax;            /@ The max interface num @/
    }USBTGT_FUNC_DRIVER, *pUSBTGT_FUNC_DRIVER;

\ce

The following items describe the major elements of the USBTGT_FUNC_DRIVER data
structure.

\h 2.1 <'USBTGT_FUNC_DRIVER::funcDriverNode'> 

This element is one list node which is used to link the function driver in the
global function driver list <'gUsbTgtFuncDriverList'>. So others, such as TCD
can find it to connect it with each other.

\h 2.2 <'USBTGT_FUNC_DRIVER::funcInTcdListNode'> 

This element is one list node which is used to link the function driver in the
TCD function driver list <'USBTGT_TCD::funcList'>. When the function driver
was found marched with the right TCD which to be attached. The target management
will link the function driver to the TCD's function list.

\h 2.3 <'USBTGT_FUNC_DRIVER::pDev'> 

<'USBTGT_FUNC_DRIVER::pDev'> specifics the VxBus device instance, if this 
function driver compatible with the VxBus.

\h 2.4 <'USBTGT_FUNC_DRIVER::funcMutex'> 

This is a mutex semaphore which is used to protect the USBTGT_FUNC_DRIVER 
elements being concurrent updated.

\h 2.5 <'USBTGT_FUNC_DRIVER::uFuncMagicID'> 

TODO : s_z want to remove it 

This element is used to mark the valid function driver type. The user can see 
what the function driver it is.

\h 2.6 <'USBTGT_FUNC_DRIVER::pFuncSpecific'> 

<'USBTGT_FUNC_DRIVER::pFuncSpecific'> is the function driver specific data 
structure pointer, it used to identify the validation of the function driver
which can be used or not.


\h 2.7 <'USBTGT_FUNC_DRIVER::ppFsFuncDescTable'>, <'USBTGT_FUNC_DRIVER::ppHsFuncDescTable'>
<'USBTGT_FUNC_DRIVER::ppSsFuncDescTable'>

Those elements are the full, high and super speed function descriptors table.
They contains the interface descriptors and endpoint descriptors and other 
descriptors which will be added to the global configuration descriptor of the 
target. And the TML will use this information to require the endpoint resource
from the TCD.

For example:

This is the RNDIS function driver which contains 2 interfaces and
3 endpoints. And the descriptors table is configured as following.

\cs

LOCAL pUSB_DESCR_HDR gUsbTgtRndisDescHdr[] =    
    {    
    (pUSB_DESCR_HDR )&gUsbTgtRndisCommIfDescr,       /@ Communication Interface descriptor @/  
    (pUSB_DESCR_HDR )&gUsbTgtRndisCommEndPtDescr,    /@ Interrupt Endpoint descriptor @/
    (pUSB_DESCR_HDR )&gUsbTgtRndisDataIfDescr,       /@ Data Interface descriptor @/  
    (pUSB_DESCR_HDR )&gUsbTgtRndisDataInEndPtDescr,  /@ Data IN Endpoint descriptor @/
    (pUSB_DESCR_HDR )&gUsbTgtRndisDataOutEndPtDescr, /@ Data OUT Endpoint descriptor @/  
    NULL                                             /@ End of descriptors, must have @/ 
    };
\ce

The TML will use this information to require 2 interface numbers and 3 endpoints
can be used from the TCD.

<'USBTGT_FUNC_DRIVER::ppHsFuncDescTable'> is the function descriptors table 
for working at high speed. Normally, only the endpoints max packet size 
different with the <'USBTGT_FUNC_DRIVER::ppFsFuncDescTable'>.

\h 2.8 <'USBTGT_FUNC_DRIVER::pFuncCallbackTable'> and <'USBTGT_FUNC_DRIVER::pCallbackParam'>

Those two elements are the callback function table and callback parameters which
are used to manage the function driver and deal with the control requests(both
standard requests and class/vendor requests).

Since the TML has be do some standard request in <usbTgtControlRequest.c>, the
callback table no need fill all the elements.

Follow is the USB_TARG_CALLBACK_TABLE structure definition.

\cs

/@ USB_TARG_CALLBACK_TABLE @/

typedef struct usbTargCallbackTable  
    {
    /@ device management callbacks @/

    USB_TARG_MANAGEMENT_FUNC        mngmtFunc; /@ management callback @/

    /@ Control pipe callbacks @/

    USB_TARG_FEATURE_CLEAR_FUNC     featureClear;   /@ feature clear @/
    USB_TARG_FEATURE_SET_FUNC       featureSet;     /@ feature set @/
    USB_TARG_CONFIGURATION_GET_FUNC configurationGet; /@configuration get@/
    USB_TARG_CONFIGURATION_SET_FUNC configurationSet; /@configuration set@/
    USB_TARG_DESCRIPTOR_GET_FUNC    descriptorGet;  /@ descriptor get @/
    USB_TARG_DESCRIPTOR_SET_FUNC    descriptorSet;  /@ descriptor set @/
    USB_TARG_INTERFACE_GET_FUNC     interfaceGet;   /@ interface get @/
    USB_TARG_INTERFACE_SET_FUNC     interfaceSet;   /@ interface set @/
    USB_TARG_STATUS_GET_FUNC        statusGet;      /@ status get @/
    USB_TARG_ADDRESS_SET_FUNC       addressSet;     /@ address set @/
    USB_TARG_SYNCH_FRAME_GET_FUNC   synchFrameGet;  /@ frame get @/
    USB_TARG_VENDOR_SPECIFIC_FUNC   vendorSpecific; /@ vendor specific @/
    } USB_TARG_CALLBACK_TABLE, *pUSB_TARG_CALLBACK_TABLE;

\ce

\h 2.9 <'USBTGT_FUNC_DRIVER::targChannel'>

<'USBTGT_FUNC_DRIVER::targChannel'> is used for multiple function drivers
used on one TCD to indicate different instance. It can also be used to compatible
with the GEN1 target stack.

\h 2.10 <'USBTGT_FUNC_DRIVER::pipeList'>

<'USBTGT_FUNC_DRIVER::pipeList'> is the USBTGT_PIPE list which likes all the 
created USBTGT_PIPE for the function driver by calling the routine 
<'usbTgtCreatePipe'>.

\h 2.11 <'USBTGT_FUNC_DRIVER::pTcd'>

<'USBTGT_FUNC_DRIVER::pipeList'> specifics the USBTGT_TCD attached by the 
function driver function driver. 

\h 2.12 <'USBTGT_FUNC_DRIVER::pFuncName'> and <'USBTGT_FUNC_DRIVER::pFuncName'> 

Those two elements are used to identify the function driver name and unit which
configured by the user.
. 
\h 2.13 <'USBTGT_FUNC_DRIVER::pTcdName'> and <'USBTGT_FUNC_DRIVER::pTcdName'> 

Those two elements are used to identify the USBTGT_TCD name and unit which
need be attached by the function driver.

\h 2.14 <'USBTGT_FUNC_DRIVER::uConfigToBind'> and <'USBTGT_FUNC_DRIVER::uCurrentConfig'> 

TODO s_z remove one  Do not want to support the configuration automatic switch.
Since it is not so useful.

<'USBTGT_FUNC_DRIVER::uConfigToBind'> specifics the configuration number which
the function driver will be build in. If the user do not set this, the default
value configuration #1 will be used.

<'USBTGT_FUNC_DRIVER::uCurrentConfig'> specifics the current configuration the
function driver build in. When there are multiple function drivers need build 
in to one configuration, and the hardware endpoint resource may be not enough
for more function driver. So the function drive can not be used correctly. This
design also wants to cover such case. It will build the function driver into 
a new configuration automatically. But the problem is the Host side driver need
be changed to switch the configuration if the user wants to use the function 
driver.

\h 2.15 <'USBTGT_FUNC_DRIVER::pConfig'> 

<'USBTGT_FUNC_DRIVER::pConfig'> specifics the current configuration 
pointer which build in.

\h 2.16 <'USBTGT_FUNC_DRIVER::pDescBuf'> and <'USBTGT_FUNC_DRIVER::uDescBufLen'>

Those two elements are used to store the function driver descriptors which
will linked to the configuration descriptors.

\h 2.17 <'USBTGT_FUNC_DRIVER::uIfNumUsed'> 

<'USBTGT_FUNC_DRIVER::uIfNumUsed'> specifics how many interface count occupied
by the function driver.

\h 2.18 <'USBTGT_FUNC_DRIVER::uIfNumMin'> and <'USBTGT_FUNC_DRIVER::uIfNumMax'>

<'USBTGT_FUNC_DRIVER::uIfNumMin'> specifics the min interface number occupied
by the function driver.Meanwhile <'USBTGT_FUNC_DRIVER::uIfNumMax'> specifics 
the max interface number occupied by the function driver.

\h 3. USBTGT_TCD_FUNCS

Each USB Target Controller will have a USBTGT_TCD_FUNCS data structure, which 
as the element of the <'USBTGT_TCD::pTcdFuncs'>. This elements contains the 
interfaces between the TML and the USBTGT_TCD. The structure is defined as below:

\cs

/@
 * Target Controller Driver abstraction
 @/

typedef struct usbtgt_tcd_funcs
    {
    /@
     * Enable the Target Controller so that the hardware can be considered
     * as running!
     @/

    STATUS (* pTcdEnable)(struct usbtgt_tcd * pTcd);

    /@
     * Disable the Target Controller so that the hardware can be considered
     * as stopped!
     @/

    STATUS (* pTcdDisable)(struct usbtgt_tcd * pTcd);
    /@
     * Suspend the Target Controller hardware
     @/

    STATUS (* pTcdSuspend)(struct usbtgt_tcd * pTcd);
    
    /@
     * Resume the Target Controller hardware
     @/
    
    STATUS (* pTcdResume)(struct usbtgt_tcd * pTcd);

    /@
     * Wakeup the Target Controller hardware
     @/
    
    STATUS (* pTcdWakeup)(struct usbtgt_tcd * pTcd);

    /@
     * Disconnect the Target Controller hardware, maybe NULL
     @/

    
    STATUS (* pTcdDisconnect)(struct usbtgt_tcd * pTcd);
    
    /@
     * Reset the Target Controller hardware
     @/    
     
    STATUS (* pTcdReset)(struct usbtgt_tcd * pTcd);
    
    /@
     * Get the Frame Number of the Target Controller hardware
     @/
     
    STATUS (* pTcdGetFrameNum)(struct usbtgt_tcd * pTcd,UINT16 * pFrameNum );

    /@
     * Soft Connect/Disconnect of the Target Controller hardware
     @/
    
    STATUS (* pTcdSoftConnect)(struct usbtgt_tcd * pTcd,BOOL isConnectUp);

    /@
     * TCD interface to create pipe, which used by the TML
     @/     
     
    STATUS (* pTcdCreatePipe)
        (
        struct usbtgt_tcd * pTcd,
        pUSB_ENDPOINT_DESCR pEndpointDesc,
        UINT16              uConfigurationValue,
        UINT16              uInterface,
        UINT16              uAltSetting,
        pUSB_TARG_PIPE      pPipeHandle
        );
        
    /@
     * TCD interface to delete pipe, which used by the TML
     @/        
     
    STATUS (* pTcdDeletePipe)
        (
        struct usbtgt_tcd * pTcd,
        USB_TARG_PIPE       pipeHandle
        );

    /@
     * TCD interface to submit ERP, which used by the TML
     @/
        
    STATUS (* pTcdSubmitErp)
        (
        struct usbtgt_tcd * pTcd,
        USB_TARG_PIPE       pipeHandle,
        pUSB_ERP            pErp
        );
        
    /@
     * TCD interface to cancel ERP, which used by the TML
     @/  
     
    STATUS (* pTcdCancelErp)
        (
        struct usbtgt_tcd * pTcd,
        USB_TARG_PIPE       pipeHandle,
        pUSB_ERP            pErp
        );
        
    /@
     * TCD interface to get status of the pipe, which used by the TML
     @/  
     
    STATUS (* pTcdPipeStatusSet)
        (
        struct usbtgt_tcd * pTcd,
        USB_TARG_PIPE       pipeHandle,
        UINT16              uStatus
        );   
        
    /@
     * TCD interface to set status of the pipe, which used by the TML
     @/    
     
    STATUS (* pTcdPipeStatusGet)
        (
        struct usbtgt_tcd * pTcd,
        USB_TARG_PIPE       pipeHandle,
        UINT16 *            pStatus
        );
        
    /@
     * TCD interface to notifiy the USB irq, which used by the TML
     @/
     
    void   (* usbIsrNotify)(struct usbtgt_tcd * pTcd, void * pContext);

    /@
     * TCD interface to notifiy the DMA irq, which used by the TML
     @/   
     
    void   (* dmaIsrNotify)(struct usbtgt_tcd * pTcd, void * pContext);
    
    /@
     * TCD interface Ioctl interface, which used by the TML
     @/   

    int  (* pTcdIoctl)(struct usbtgt_tcd * pTcd,int cmd, void * pContext);

    }USBTGT_TCD_FUNCS, *pUSBTGT_TCD_FUNCS;

\ce

The following items describe the major elements of the USBTGT_TCD_FUNCS data
structure.

\h 3.1 <'USBTGT_TCD_FUNCS::pTcdEnable'> and <'USBTGT_TCD_FUNCS::pTcdDisable'>

Those two elements are used by the OTG stack, which can be used to switch the
Host stack and the Target Stack.

\h 3.2 <'USBTGT_TCD_FUNCS::pTcdSuspend'> and <'USBTGT_TCD_FUNCS::pTcdResume'>

Those two elements are not must have. They are used to suspend/resume the 
USB target controller.

\h 3.3 <'USBTGT_TCD_FUNCS::pTcdWakeup'>

This element is not must have. It is used to issue the remote Wakeup to the USB
host.

\h 3.4 <'USBTGT_TCD_FUNCS::pTcdReset'>

This element is not must have. It is used to issue the reset by force to the USB
target controller.

\h 3.5 <'USBTGT_TCD_FUNCS::pTcdGetFrameNum'>

This element is not must have, but better have. It is used to get the current frame number from
the USB target controller. It may used in some ISO transaction.

\h 3.6 <'USBTGT_TCD_FUNCS::pTcdSoftConnect'>

This element is not must have, but better have. This should according to the 
real hardware. It is used to issue the soft connect/disconnect of the USB target
controller. Some hardware support this feature to simulate the connect/disconnect
the USB target to/from the USB host.

NOTE:
And some hardware will be in disconnect state after BUS RESET, the software 
should do the soft connect to make the USB target controller work. So this
element should not be NULL in this case.

<'USBTGT_TCD_FUNCS::pTcdSoftConnect'> also can be used in case that the user
need dynamicly attach/detach the function driver. The TML need to do the soft
disconnect first and attach/detach the function drivers then do soft connect to 
make the USB target be updated.

\h 3.7 <'USBTGT_TCD_FUNCS::pTcdCreatePipe'>

Must have, this element is used by the TML to issue the pipe create to require
the hardware resource such as occuping the endpoint. Please go to the 
<'usbTgtCreatePipe()'> routine for detail.

\h 3.8 <'USBTGT_TCD_FUNCS::pTcdDeletePipe'>

Must have, this element is used by the TML to issue the pipe delete to release
the hardware resource such as the occupied endpoint. Please go to the 
<'usbTgtDeletePipe()'> routine for detail.

\h 3.9 <'USBTGT_TCD_FUNCS::pTcdSubmitErp'>

Must have, this element is used by the TML to issue the one transaction on one
endpoint. Please go to the <'usbTgtSubmitErp()'> routine for detail.

\h 3.10 <'USBTGT_TCD_FUNCS::pTcdCancelErp'>

Must have, this element is used by the TML to cancel the one transaction on one
endpoint. Please go to the <'usbTgtCancelErp()'> routine for detail.

\h 3.11 <'USBTGT_TCD_FUNCS::pTcdPipeStatusSet'>

Must have, this element is used by the TML to set the status of one endpoint,
STALL or UNSTALL.Please go to the <'usbTgtSetPipeStatus()'> routine for detail.

\h 3.12 <'USBTGT_TCD_FUNCS::pTcdPipeStatusGet'>

Must have, this element is used by the TML to get the status of one endpoint,
STALL or UNSTALL.Please go to the <'usbTgtGetPipeStatus()'> routine for detail.

\h 3.13 <'USBTGT_TCD_FUNCS::usbIsrNotify'> 

Not must have, this element is used by the TML to notify the USB target stack
one USB interrupt just received, and issue the notification to the USB target
ISR to process it.

NOTE:
This can be used by the OTG stack, to issue the interrupt notification to the 
USB target stack.Since the OTG stack will hand over the IRQ of the USB
controller. If the USB target controller do not support OTG, or the OTG stack
do not use this interface, the element will be NULL.

\h 3.14 <'USBTGT_TCD_FUNCS::dmaIsrNotify'> 

Not must have, this element is used by the TML to notify the USB target stack
one USB DMA interrupt just received, and issue the notification to the USB target
DMA ISR to process it.

NOTE:
This can be used by the OTG stack, to issue the interrupt notification to the 
USB target stack.Since the OTG stack will hand over the DMA IRQ of the USB
controller. If the USB target controller do not support OTG, or the OTG stack
do not use this interface, the element will be NULL.

On some platform, the DMA IRQ is separated with the USB IRQ. So DMA ISR 
notification may needed. But on some platform, which do not have the separated 
DMA IRQ, this elements will be NULL.

\h 3.15 <'USBTGT_TCD_FUNCS::pTcdIoctl'> 

This is the ioctl interface used by the TML, which also can be used to 
compatible with the GEN1 target stack.


HOW TO ADD ONE TCD TO TML

NOTE:Before one valid target controller driver (TCD) ready to used, it must have
been initialized and added to the target management layer.

\h 1.Create USBTGT_TCD Data Structure

   The routine <'usbTgtTcdResourceCreate()'> is used to create the USBTGT_TCD
   data structure and add itself to the global TCD list <'gUsbTgtTcdList'>.With 
   return a valid USBTGT_TCD pointer or NULL of something wrong.
   Now, the resource can be used by the hardware TCD driver to use.

\h 2.Set the User's Configuration

   Following list the parameters which can be get from the user's configuration.
   
\cs
    /@ Parameters get from the user's configuration to initialize the TCD @/
    
    UINT32                uTCDMagicID;  /@ TCD magic ID @/
    UINT16                uVendorID;    /@ VID @/
    UINT16                uProductID;   /@ PID @/
    char *                pMfgString;   /@ Manufacture string @/
    char *                pProdString;  /@ Producture string @/
    char *                pSerialString;/@ Serial num string @/
    char *                pTcdName;     /@ TCD name @/
    UINT8                 uTcdUnit;     /@ Tcd unit number @/
\ce

\is
\i uTCDMagicID
   The magic ID to identify the TCD type
\i uVendorID
   The vendor ID of the USB target
\i uProductID
   The product ID of the USB target
\i pMfgString
   The manufacture string of the USB target
\i pProdString
   The manufacture string of the USB target
\i pSerialString
   The serial string of the USB target
\i pTcdName
   The tcd name to indentify the hardware
\i uTcdUnit
   The tcd unit number to indentify the hardware   
\ie

NOTE: There maybe other parameters need be set by the user which not be included.
The developer should configure them in the same way.

\h 3.Initialize the TCD
   The USB target controller driver(TCD) who need be intialized and registered 
   to the target management layer(TML). Please refor to the usrUsbTgtTcdInit().

\h 3.1 Get one unused TCD resource with the magic ID

   Get the TCD resource with the magic ID by calling usbTgtTcdResourceRequest().
   The magic ID to identify the TCD type. <'pTcdSpecific'> be <'NULL'> indicate
   to get one unused resource by others.

   NOTE: This step is not necessary, if the user directly use the resource
   created by step 1.
   
\h 3.2 Initialize the TCD related parameters 

 Followed are the parameters of USBTGT_TCD data structure (please refer to the
 usbTgt.h for detail). Those parameters need be initialized in the TCD 
 initialization routines before add to the TML.

\cs
    /@ Parameters which need be initalized in the TCD @/
    
    VXB_DEVICE_ID         pDev;         /@ Pointer to VxBus Device instance @/
    USBTGT_TCD_FUNCS *    pTcdFuncs;    /@ Functions implemented by the TCD @/
    pVOID                 pTcdSpecific; /@ Pointer to TCD specific structure @/
    USBTGT_DEVICE_INFO    DeviceInfo;   /@ Device information @/
\ce

\is
\i pDev
   VxBus device instance, if this TCD compatible with the VxBus
\i pTcdFuncs
   The USB target controller driver(TCD) function table, which contains some
   useful routines such as submitErp function pointer which can be called by
   the TML. Must have.
\i pTcdSpecific
   This is the TCD specific data structure pointer, it used to identify the 
   real hardware.Used by the TML to check if this TCD has bind with the hardware
   or not. Must have.
\i DeviceInfo
   This data structure specifics the TCD hardware feature.Such as the 
   endpoints features(Supported Tx and Rx endpoints) and controller features.
\ie

\h 4.Add the TCD to the TML 

   Add the TCD to the TML by calling the routine usbTgtTcdAdd(). This routine 
   will check if there are any function driver need attach to it. If yes, it 
   will attach all the function drivers onto it, and generate the device 
   characters such as device descriptor and configuration descriptor ready for
   using.
    
HOW TO ADD FUNCTION DRIVER TO TML

 NOTE:Before one valid function driver ready to used, it must have
 been initialized and added to the target management layer.

\h 1.Create USBTGT_FUNC_DRIVER data structure

   The routine <'usbTgtFuncResourceCreate()'> is used to create one 
   USBTGT_FUNC_DRIVER.data structure and add itself to the global function  
   driver list <'gUsbTgtFuncDriverList'>.With return a valid USBTGT_FUNC_DRIVER
   pointer if successed or NULL of something wrong.
   called in the usrTgtInit.c, and the user's configration will be set when it
   be initialized.

\h 2.Set the User's Configuration

   Following list the parameters which can be get from the user's configuration.
   
\cs
    /@ Parameters get from the user's configuration to initalize the TCD @/
    UINT32                     uFuncMagicID;         /@ Func magic Id @/
    char *                     pTcdName;             /@ Used for the TCD's name @/
    UINT8                      uTcdUnit;             /@ Used for the TCD's unit @/
    UINT8                      uConfigToBind;        /@ The configuration group in @/
\ce

\is
\i uTCDMagicID
   The magic ID to identify the function driver type
\i pTcdName
   The TCD name witch need to be attached.Must have, or the function driver will
   do not know which TCD to be attached. The target stack will use the user's 
   configuration, or use the default value.
\i uTcdUnit
   The TCD unit number which need be attached.Must have, or the function driver 
   will do not know which TCD to be attached. The target stack will use the 
   user's configuration, or use the default value.
\i uConfigToBind
   The configuration which the function driver expect to build in.Must have, 
   or the function driver will do not know which configuration to be bind. The 
   target stack will use the user's configuration, or use the default value.
\ie

NOTE: There maybe other parameters need be set by the user which not be included.
The developer should configure them in the same way.


\h 3.Initialize the function driver
   The USB function driver who need be initialized and registered to the target
   management layer(TML).


\h 3.1 Get the function resource with the magic ID

   Get the function resource with the magic ID by calling 
   usbTgtFuncResourceRequest(). The magic ID to identify the function type.
   <'pFuncSpecific'> be <'NULL'> indicate to get one unused resource by others.

   NOTE: This step is not necessary, if the user directly use the resource
   created by step 1.
   
\h 3.2 Initialize the function driver related parameters 

 Followed are the parameters of USBTGT_FUNC_DRIVER data structure (please refer to the
 usbTgt.h). Those parameters need be initialized in the function initialization 
 routines before add to the TML.
 
\cs
    /@ Parameters which need be initalized in the TCD @/
    
    pVOID                      pFuncSpecific;      /@ Specific data @/
    USB_DESCR_HDR **           ppFsFuncDescTable;  /@ Function descriptors table @/
    USB_DESCR_HDR **           ppHsFuncDescTable;  /@ Function descriptors table in High speed mode @/
    USB_DESCR_HDR **           ppSsFuncDescTable;  /@ Function descriptors table in super speed mode @/
    USB_TARG_CALLBACK_TABLE *  pFuncCallbackTable; /@ Function driver callback table @/    
    pVOID                      pCallbackParam;     /@ Callback parameter @/
\ce

\is
\i pFuncSpecific
   This is the function driver specific data structure pointer, it used to used 
   by the TML to check if this function driver has bind with the hardware
   or not, and also used by the function driver itself. Must have.
\i ppFsFuncDescTable
   The descriptors table which can describe the characters of the function 
   driver. Such as interface descriptors to describ the Class/Subclass/Protocol
   the function driver use.
\i ppHsFuncDescTable
   The descriptors table which can describe the characters of the function 
   driver. Such as interface descriptors to describe the Class/Subclass/Protocol
   the function driver use. This table describe the characters in the high speed.
\i ppSsFuncDescTable
   The descriptors table which can describe the characters of the function 
   driver. Such as interface descriptors to describe the Class/Subclass/Protocol
   the function driver use. This table describe the characters in the super speed.
\i pFuncCallbackTable
   The function driver's callback routines table.
\i pCallbackParam
   The function driver's callback routines parameter.
\ie

\h 4.Add the function driver to the TML 
   Add the function to the TML by calling the routine usbTgtFuncAdd(). This  
   routine will check if the exactly TCD exist or not who has the right pTcdName
   and uTcdUnit. If yes, It will attach them together by usbTgtFuncAttach().


HOW TO DETACH ONE FUNCTION DRIVER FROM TCD

The user may want to dynamically remove one <attached> function driver from one
TCD.Which can switch the TCD emulate as other function else. Such as switch the
Mass storage device to RNDIS device.

The routine <'usbTgtFuncRemove'> can achieve this. It will detach the function 
driver from the TCD, and also call the detach call back routine 
<'usbTgtFuncMngmtCallback'> to notify the function driver to detach itself 
from the pUSBTGT_FUNC_DRIVER resource. Then it will remove the connection from
the TCD which has been attached.

The user can find the <'pUSBTGT_FUNC_DRIVER'> resource by name and remove it 
by the <'usbTgtFuncRemove'> routine.

NOTE: soft disconnect will be seen on the BUS.

HOW TO ATTACH ONE FUNCTION DRIVER TO TCD

The user may want to dynamically add one <detached> function driver to one
TCD which asigned.Which can switch the TCD emulate as other function else.
Such as switch the Mass storage device to RNDIS device.

The routine <'usbTgtFuncAdd'> can achieve this. It will attach the function 
driver to the TCD, and also call the attach call back routine 
<'usbTgtFuncMngmtCallback'> to notify the function driver to attach itself 
to the pUSBTGT_FUNC_DRIVER resource. Then it will add the connection to
the TCD which has been attached.

The user can find the <'pUSBTGT_FUNC_DRIVER'> resource by name and add it 
by the <'usbTgtFuncAdd'> routine.

NOTE: soft connect will be seen on the BUS.

INCLUDE FILES:  vxWorks.h lstLib.h usb/usb.h usb/usbCdc.h usb/usbOsal.h
                usb/usbTgt.h usbTgtControlRequest.h
                
*/

/* includes */

#include <vxWorks.h>
#include <lstLib.h>
#include <usb/usb.h>
#include <usb/usbCdc.h>
#include <usb/usbOsal.h>
#include <usb/usbTgt.h>
#include "usbTgtControlRequest.h"
#include "usbTgtUtil.h"

/* Debug */

#include "usbTgtDebug.c"

/* defines */

/* externs */

IMPORT USB_LANGUAGE_DESCR * usrUsbTgtLangDescrGet
    (
    void
    );

IMPORT STATUS usrUsbTgtDescGet
    (
    UINT8    uDescType,
    char *   pDescBuf
    );

IMPORT BOOL usrUsbTgtDevDescFuncEnableGet
    (
    void
    );

IMPORT char * usrUsbTgtDevDescOfFuncNameGet
    (
    void
    );
    
IMPORT UINT8 usrUsbTgtDevDescOfFuncUnitGet
    (
    void
    );

BOOL usrUsbTgtIadDescEnableGet
    (
    void
    );

/* globals */

/* locals */

LOCAL STATUS usbTgtFuncAttach
    (
    pUSBTGT_CONFIG        pConfig,
    pUSBTGT_FUNC_DRIVER   pFuncDriver
    );

LOCAL STATUS usbTgtFuncDetach
    (
    pUSBTGT_CONFIG        pConfig,
    pUSBTGT_FUNC_DRIVER   pFuncDriver
    );

LOCAL void usbTgtTcdInfoUpdate
    (
    pUSBTGT_TCD pTcd 
    );
LOCAL void usbTgtFuncDescUpdate
    (
    pUSBTGT_FUNC_DRIVER   pFuncDriver
    );

/* The TGT TCD list */

LIST gUsbTgtTcdList;

/* The TGT function driver list */

LIST gUsbTgtFuncDriverList;

/* Event used for synchronising the access of the TGT list */

OS_EVENT_ID gUsbTgtTcdListLock = OS_INVALID_EVENT_ID;
OS_EVENT_ID gUsbTgtFuncListLock = OS_INVALID_EVENT_ID;


/* The targ channel list */

LOCAL LIST gUsbTgtTargChList;

/* The flag indicates targetM initialized or not */

LOCAL atomic_t gUsbTgtInited = _VX_ATOMIC_INIT(0);


/* The targ channel list lock */

LOCAL OS_EVENT_ID gUsbTgtTargChListLock = OS_INVALID_EVENT_ID;

LOCAL UINT16 gUsbTgtTcdMaggicIdIndex = 0;
LOCAL UINT16 gUsbTgtFuncMaggicIdIndex = 0;


/*******************************************************************************
*
* usbTgtMaggicIDRequire - require one valid maggic ID according to the signature
*
* This routine is used to require one valid maggic ID according to the 
* <'uSignature'>. 
*
* NOTE:
* We do not release the reqired maggic ID, even it not used anymore. This way
* will make the maggic ID un-reusable, but save the memory and the time.
* Infect, 65535 is enouth for using. 
*
* RETURNS: USBTGT_TARG_CHANNEL_DEAD, or a valid targ channel handler
*
* ERRNO: N/A
*
* \NOMANUAL 
*/

LOCAL UINT32 usbTgtMaggicIDRequire
    (
    UINT32 uSignature
    )
    {
    UINT32 uMaggicID;
    
    switch (uSignature)
        {
        case USBTGT_TCD_SIG:
            {
            gUsbTgtTcdMaggicIdIndex ++;
            uMaggicID = USBTGT_TCD_MAGIC_CODE(gUsbTgtTcdMaggicIdIndex);
            }
            break;
        case USBTGT_FUNC_SIG:
            {
            gUsbTgtFuncMaggicIdIndex ++;
            uMaggicID = USBTGT_FUNC_MAGIC_CODE(gUsbTgtFuncMaggicIdIndex);
            }
            break;
        default:
            uMaggicID = USBTGT_MAGIC_ID_DEAD;
            break;
        }
    return uMaggicID;
    }

/*******************************************************************************
*
* usbTgtFuncChannelFind - find the right pUSBTGT_FUNC_CHANNEL from the global list
*
* This routine is used to find the pUSBTGT_FUNC_CHANNEL according to the 
* USB_TARG_CHANNEL ID from the global list.
*
* RETURNS: NULL, or a valid pUSBTGT_FUNC_CHANNEL printer
*
* ERRNO: N/A
*
* \NOMANUAL 
*/

LOCAL pUSBTGT_FUNC_CHANNEL usbTgtTargChannelFind
    (
    USB_TARG_CHANNEL targChannel
    )
    {
    pUSBTGT_FUNC_CHANNEL pChannel = NULL;
    NODE *               pNode    = NULL;

    if (FALSE == IS_USBTGT_TARG_CHANNEL_VALID(targChannel))
        {
        return NULL;
        }

    OS_WAIT_FOR_EVENT(gUsbTgtTargChListLock, USBTGT_WAIT_TIMEOUT);

    pNode = lstFirst(&gUsbTgtTargChList);

    pChannel = (pUSBTGT_FUNC_CHANNEL)pNode;
    
    while (pChannel)
        {
        if (pChannel->targChannel == targChannel)
            {
            (void)OS_RELEASE_EVENT(gUsbTgtTargChListLock);
            return pChannel;
            }
        pNode = lstNext(pNode);
        pChannel = (pUSBTGT_FUNC_CHANNEL)pNode;
        }

    (void)OS_RELEASE_EVENT(gUsbTgtTargChListLock);
    
    return NULL;
    }

/*******************************************************************************
*
* usbTgtTargChannelToFuncDriver - find the pFuncDriver according to targChannel
*
* This routine is used to find the <'pFuncDriver"> according to <'targChannel'>
*
* RETURNS: NULL, or a valid pUSBTGT_FUNC_DRIVER pointer
*
* ERRNO: N/A
*/

pUSBTGT_FUNC_DRIVER usbTgtTargChannelToFuncDriver
    (
    USB_TARG_CHANNEL targChannel
    )
    {
    pUSBTGT_FUNC_CHANNEL pChannel = NULL;

    pChannel = usbTgtTargChannelFind(targChannel);

    if (NULL != pChannel)
        {
        return pChannel->pFuncDriver;
        }

    return NULL;
    }

/*******************************************************************************
*
* usbTgtFuncTargChannelDestroy - destroy the targ channel 
*
* This routine destroys the targ channel which created by the 
* routine <'usbTgtTargChannelCreate'>.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL 
*/

LOCAL STATUS usbTgtFuncTargChannelDestroy
    (
    USB_TARG_CHANNEL targChannel
    )
    {
    pUSBTGT_FUNC_CHANNEL pChannel = NULL;
    
    pChannel = usbTgtTargChannelFind(targChannel);

    OS_WAIT_FOR_EVENT(gUsbTgtTargChListLock, USBTGT_WAIT_TIMEOUT);

    if (NULL != pChannel)
        {
        if (ERROR != lstFind(&gUsbTgtTargChList, &pChannel->funcChNode))
            lstDelete(&gUsbTgtTargChList, &pChannel->funcChNode);
        
        OS_FREE (pChannel);
        
        (void)OS_RELEASE_EVENT(gUsbTgtTargChListLock);
        return OK;
        }
    
    (void)OS_RELEASE_EVENT(gUsbTgtTargChListLock);
    return ERROR;    
    }

/*******************************************************************************
*
* usbTgtFuncTargChannelCreate - create a targ channel handler 
*
* This routine creates a targ channel handler use the <'pUSBTGT_FUNC_DRIVER'> 
* data information.
*
* NOTE: 
*
* The caller should make sure:
*
* <'pFuncDriver'> is valid
* <'pFuncDriver->uFuncMagicID'> is valid, which is used to identify the 
* signature of one valid <'USB_TARG_CHANNEL'>
*
* RETURNS: USBTGT_TARG_CHANNEL_DEAD, or a valid targ channel handler
*
* ERRNO: N/A
*
* \NOMANUAL 
*/

LOCAL USB_TARG_CHANNEL usbTgtFuncTargChannelCreate
    (
    pUSBTGT_FUNC_DRIVER pFuncDriver
    )
    {
    /* Validate the endpoint descriptor */
    
    pUSBTGT_FUNC_CHANNEL pChannel = NULL;
    USB_TARG_CHANNEL     targChannel;
    UINT16               uIndex   = 1;
    
    if ((NULL == pFuncDriver) ||
        (FALSE == IS_FUNC_DRIVER_MAGICID_VALID(pFuncDriver->uFuncMagicID)))
        {
        USB_TGT_ERR("Invalid parameter %s\n",
                    ((NULL == pFuncDriver) ? "pFuncDriver is NULL" :
                    (NULL == pFuncDriver->pFuncSpecific) ? 
                    "pFuncDriver->pFuncSpecific is NULL" :
                    "pFuncDriver->uFuncMagicID is invalid"),
                    2, 3, 4, 5, 6);

        return USBTGT_TARG_CHANNEL_DEAD;
        }

    pChannel = (pUSBTGT_FUNC_CHANNEL)OSS_CALLOC(sizeof (USBTGT_FUNC_CHANNEL));

    if (NULL == pChannel)
        {
        USB_TGT_ERR("Create targ channel data structure error\n", 1, 2, 3, 4, 5 ,6);
        
        return USBTGT_TARG_CHANNEL_DEAD;
        }

    pChannel->uChannelSig = pFuncDriver->uFuncMagicID;
    pChannel->pFuncDriver = pFuncDriver;

    /* Get one free targ channel handler */

    for (uIndex = 1; uIndex < 0xFFFF; uIndex ++)
        {
        targChannel = USBTGT_TARG_CHANNEL_SIG | uIndex;

        /* Not find the targ channel in this list, means it is free */
        
        if (NULL == usbTgtTargChannelFind(targChannel))
            {
            pChannel->targChannel = targChannel;

            OS_WAIT_FOR_EVENT(gUsbTgtTargChListLock, USBTGT_WAIT_TIMEOUT);
            lstAdd(&gUsbTgtTargChList, &pChannel->funcChNode);
            (void)OS_RELEASE_EVENT(gUsbTgtTargChListLock);

            return pChannel->targChannel;
            } 
        }

    /* The list is full, free the pChannel and return */
        
    OS_FREE (pChannel);
    
    return USBTGT_TARG_CHANNEL_DEAD;    
    }

/*******************************************************************************
*
* usbTgtCreatePipe - create a pipe for communication on an endpoint
*
* This routine creates a pipe for communication on an endpoint. The endpoint's
* information such as the address and max packet size, can be get from the 
* <'pEndpointDesc'>. It will call the TCD specific <'pTcdCreatePipe'>
* to create one pipe handle which be set as the <'pUsbTgtPipe->pipeHandle'>
* value, and return one valided pUSB_TARG_PIPE to the caller. 
*
* This routine will also record the pipe in the function driver's pipe list
* <'pFuncDriver->pipeList'>.
*
* RETURNS: OK, or ERROR if unable to create pipe
*
* ERRNO: N/A
*/

STATUS usbTgtCreatePipe
    (
    USB_TARG_CHANNEL     targChannel,        /* Target channel */
    pUSB_ENDPOINT_DESCR  pEndpointDesc,      /* USB_ENDPOINT_DESCR */
    UINT8                uConfigurationValue,/* Configuration value */
    UINT8                uInterface,         /* Number of interface */
    UINT8                uAltSetting,        /* Alternate Setting */
    pUSB_TARG_PIPE       pPipeHandle         /* Pointer to pipe handle */
    )
    {
    STATUS              status = ERROR;
    pUSBTGT_PIPE        pUsbTgtPipe = NULL;
    pUSBTGT_FUNC_DRIVER pFuncDriver = usbTgtTargChannelToFuncDriver(targChannel);
    
    /* Validate the endpoint descriptor */

    if ((NULL == pFuncDriver) ||
        (NULL == pEndpointDesc) ||
        (NULL == pPipeHandle) ||
        (NULL == pFuncDriver->pTcd) ||
        (NULL == pFuncDriver->pTcd->pTcdFuncs) ||
        (NULL == pFuncDriver->pTcd->pTcdFuncs->pTcdCreatePipe))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pFuncDriver) ? "targChannel" :
                    (NULL == pEndpointDesc) ? "pEndpointDesc" :
                    (NULL == pPipeHandle) ? "pPipeHandle" :
                    (NULL == pFuncDriver->pTcd) ? "pFuncDriver->pTcd" :
                    (NULL == pFuncDriver->pTcd->pTcdFuncs) ? "pTcdFuncs" :
                    "pTcdCreatePipe"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Check the pipe for the endpoint is exist or not */

    if (NULL != usbTgtFindPipeByEpIndex(pFuncDriver, 
                                        pEndpointDesc->endpointAddress))
        {
        USB_PLX_ERR("Already exist a pipe for endpointAddress [%d] \n",
                     pEndpointDesc->endpointAddress, 2, 3, 4, 5, 6);
        
        return ERROR;
        }

    /* Create one new pipe */
    
    pUsbTgtPipe = (pUSBTGT_PIPE)OSS_CALLOC(sizeof(USBTGT_PIPE));

    if (NULL == pUsbTgtPipe)
        {
        /* Create pipe, TODO: do not dymatic */
        
        USB_TGT_ERR("Alloc memeory error to create pipe \n",
                    1, 2, 3, 4, 5, 6);
        
        return ERROR;
        }

    /* Call the low level TCD to create the pipe */

    USB_TGT_VDBG("Call the TCD low level to create pipe \n",
                 1, 2, 3, 4, 5, 6);
    
    /* Create the specific pipe data of TCD */
    
    status = (pFuncDriver->pTcd->pTcdFuncs->pTcdCreatePipe)
              (pFuncDriver->pTcd,
               pEndpointDesc,
               uConfigurationValue,
               uInterface,
               uAltSetting,
               &pUsbTgtPipe->pipeHandle);

    if (ERROR == status)
        {
        USB_TGT_ERR("Create pipe error \n",
                    1, 2, 3, 4, 5, 6);

        OS_FREE (pUsbTgtPipe);
        
        return ERROR;
        }

    /* Init the pipe */
    
    pUsbTgtPipe->pFuncDriver = pFuncDriver;
    pUsbTgtPipe->uEpAddr = pEndpointDesc->endpointAddress;
    pUsbTgtPipe->uInterface = uInterface;
    pUsbTgtPipe->uAltSetting = uAltSetting;
    pUsbTgtPipe->uEpType = (UINT8) (pEndpointDesc->attributes & USB_ATTR_EPTYPE_MASK);
    pUsbTgtPipe->uEpDir = (UINT8) (pEndpointDesc->endpointAddress & USB_ENDPOINT_IN);
        
    USB_TGT_VDBG("usbTgtCreatedPIPE checked [%x %x %x %x] \n", 
                 pUsbTgtPipe->uInterface,
                 pUsbTgtPipe->uAltSetting,
                 pUsbTgtPipe->uEpType,
                 pUsbTgtPipe->uEpDir,5,6);    


    semTake (pFuncDriver->funcMutex, USBTGT_WAIT_TIMEOUT);
    
    lstAdd (&pFuncDriver->pipeList, &pUsbTgtPipe->pipeListNode);
    
    (void)semGive (pFuncDriver->funcMutex);
    
    * pPipeHandle = pUsbTgtPipe;
    
    USB_TGT_DBG("Created pipe with handle 0x%X maxPacketSize %d\n",
                 pUsbTgtPipe, FROM_LITTLEW(pEndpointDesc->maxPacketSize), 
                 3, 4, 5, 6);

    return OK;
    }

/*******************************************************************************
*
* usbTgtDeletePipe - delete an pipe created for an endpoint
*
* This routine deletes an pipe created for one edpoint created by 
* usbTgtCreatePipe() routine.This routine will call the delete pipe routine 
* which registered in the TCD driver table and remove the record of the pipe 
* in the function driver.
*
* RETURNS: OK, or ERROR if unable to destroy pipe.
*
* ERRNO: N/A
*/

STATUS usbTgtDeletePipe
    (
    USB_TARG_PIPE   pipeHandle        /* pipe to be destroyed */
    )
    {
    STATUS              status = ERROR;
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;
    pUSBTGT_PIPE        pUsbTgtPipe = (pUSBTGT_PIPE)pipeHandle;
    
    /* Validate parameters */

    if ((NULL == pUsbTgtPipe) ||
        (NULL == pUsbTgtPipe->pFuncDriver) ||
        (NULL == pUsbTgtPipe->pFuncDriver->pTcd) ||
        (NULL == pUsbTgtPipe->pFuncDriver->pTcd->pTcdFuncs) ||
        (NULL == pUsbTgtPipe->pFuncDriver->pTcd->pTcdFuncs->pTcdDeletePipe))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pUsbTgtPipe) ? "pipeHandle" :
                    (NULL == pUsbTgtPipe->pFuncDriver) ? "pFuncDriver" :
                    (NULL == pUsbTgtPipe->pFuncDriver->pTcd) ? "pTcd" :
                    (NULL == pUsbTgtPipe->pFuncDriver->pTcd->pTcdFuncs) ?
                    "pTcdFuncs" : "pTcdDeletePipe"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    pFuncDriver = pUsbTgtPipe->pFuncDriver;

    /* Call the low level TCD to delete the pipe */

    USB_TGT_VDBG("Call the TCD low level to delete pipe \n",
                 1, 2, 3, 4, 5, 6);
    
    status = (pUsbTgtPipe->pFuncDriver->pTcd->pTcdFuncs->pTcdDeletePipe)
              (pFuncDriver->pTcd,
              pUsbTgtPipe->pipeHandle);
    
    if (ERROR == status)
        {
        USB_TGT_ERR("Delete pipe error \n",
                    1, 2, 3, 4, 5, 6);
        
        return ERROR;
        }

    /* Delete it from the function pipe list */

    semTake (pFuncDriver->funcMutex, USBTGT_WAIT_TIMEOUT);
    
    if (ERROR != lstFind (&pFuncDriver->pipeList, &pUsbTgtPipe->pipeListNode))
    lstDelete(&pFuncDriver->pipeList, &pUsbTgtPipe->pipeListNode);
    
    (void)semGive (pFuncDriver->funcMutex);

    OS_FREE (pUsbTgtPipe);

    USB_TGT_DBG("Deleted pipe with handle 0x%X status %d\n",
                (ULONG)pUsbTgtPipe,
                status, 3, 4, 5, 6);

    return status;
    }

/*******************************************************************************
*
* usbTgtUnConfigPipe - un-configure a pipe for communication on an endpoint
*
* This routine creates a pipe for communication on an endpoint. The endpoint's
* information such as the address and max packet size, can be get from the 
* <'pEndpointDesc'>. It will call the TCD specific <'pTcdCreatePipe'>
* to create one pipe handle which be set as the <'pUsbTgtPipe->pipeHandle'>
* value, and return one valided pUSB_TARG_PIPE to the caller. 
*
* This routine will also record the pipe in the function driver's pipe list
* <'pFuncDriver->pipeList'>.
*
* RETURNS: OK, or ERROR if unable to create pipe
*
* ERRNO: N/A
*/

STATUS usbTgtUnConfigPipe
    (
    USB_TARG_PIPE        pipeHandle
    )
    {
    STATUS              status = ERROR;
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;
    pUSBTGT_PIPE        pUsbTgtPipe = (pUSBTGT_PIPE)pipeHandle;
    USBTGT_PIPE_CONFIG_PARAM Param;
    
    /* Validate parameters */

    if ((NULL == pUsbTgtPipe) ||
        (NULL == (pFuncDriver = pUsbTgtPipe->pFuncDriver)) ||
        (NULL == pFuncDriver->pTcd) ||
        (NULL == pFuncDriver->pTcd->pTcdFuncs) ||
        (NULL == pFuncDriver->pTcd->pTcdFuncs->pTcdIoctl))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pUsbTgtPipe) ? "pipeHandle" :
                    (NULL == pFuncDriver) ? "pFuncDriver" :
                    (NULL == pFuncDriver->pTcd) ? "pTcd" :
                    (NULL == pFuncDriver->pTcd->pTcdFuncs) ?
                    "pTcdFuncs" : "pTcdIoctl"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Set the pipe infor to the TCD driver */

    Param.pPrivData = (void *)pUsbTgtPipe->pipeHandle;
    
    status = (pFuncDriver->pTcd->pTcdFuncs->pTcdIoctl)
              (pFuncDriver->pTcd,
              USBTGT_TCD_IOCTL_CMD_UNCONIG_PIPE,
              (void *)&Param);
    
    if (ERROR == status)
        {
        USB_TGT_ERR("Unconfigure the pipe error \n",
                    1, 2, 3, 4, 5, 6);
        
       /* return ERROR; */
        }
    
    USB_TGT_DBG("Un-Cofnig pipe with handle 0x%X Done\n",
                 pUsbTgtPipe, 2, 3, 4, 5, 6);

    return status;
    }

/*******************************************************************************
*
* usbTgtConfigPipe - configure a pipe for communication on an endpoint
*
* This routine creates a pipe for communication on an endpoint. The endpoint's
* information such as the address and max packet size, can be get from the 
* <'pEndpointDesc'>. It will call the TCD specific <'pTcdCreatePipe'>
* to create one pipe handle which be set as the <'pUsbTgtPipe->pipeHandle'>
* value, and return one valided pUSB_TARG_PIPE to the caller. 
*
* This routine will also record the pipe in the function driver's pipe list
* <'pFuncDriver->pipeList'>.
*
* RETURNS: OK, or ERROR if unable to create pipe
*
* ERRNO: N/A
*/

STATUS usbTgtConfigPipe
    (
    USB_TARG_PIPE             pipeHandle,    /* Pipe handle */
    pUSBTGT_PIPE_CONFIG_PARAM pParam         /* Pointer to pipe handle */
    )
    {
    STATUS              status = ERROR;
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;
    pUSBTGT_PIPE        pUsbTgtPipe = (pUSBTGT_PIPE)pipeHandle;
    
    /* Validate parameters */

    if ((NULL == pUsbTgtPipe) ||
        (NULL == (pFuncDriver = pUsbTgtPipe->pFuncDriver)) ||
        (NULL == pFuncDriver->pTcd) ||
        (NULL == pFuncDriver->pTcd->pTcdFuncs))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pUsbTgtPipe) ? "pipeHandle" :
                    (NULL == pFuncDriver) ? "pFuncDriver" :
                    (NULL == pFuncDriver->pTcd) ? "pTcd" :
                    "pTcdFuncs"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    if (NULL == pFuncDriver->pTcd->pTcdFuncs->pTcdIoctl)
        {
        USB_TGT_DBG("No configure pipe interface, return OK\n",
                     pUsbTgtPipe, 2, 3, 4, 5, 6);
        
        return OK;
        }
    
    /* Set the pipe infor to the TCD driver */

    pParam->pPrivData = (void *)pUsbTgtPipe->pipeHandle;
    
    status = (pFuncDriver->pTcd->pTcdFuncs->pTcdIoctl)
              (pFuncDriver->pTcd,
              USBTGT_TCD_IOCTL_CMD_CONIG_PIPE,
              (void *)pParam);
    
    if (ERROR == status)
        {
        USB_TGT_ERR("Configure the pipe error \n",
                    1, 2, 3, 4, 5, 6);
        
        return ERROR; 
        }
    
    USB_TGT_DBG("Cofnig pipe with handle 0x%X Done\n",
                 pUsbTgtPipe, 2, 3, 4, 5, 6);

    return status;
    }

/*******************************************************************************
*
* usbTgtSubmitErp - submit one Erp to transfer data through the pipe
*
* This routine is used to submit one Erp to transfer data through a pipe
* indicated by <pipeHandle>. The transfer is described by an ERP, or endpoint
* request packet, which must be allocated and initialized by the caller.This 
* routine will call the submit ERP routine which registered in the TCD 
* driver table
*
* RETURNS: OK or Error if unable to transfer data.
*
* ERRNO: N/A
*/

STATUS usbTgtSubmitErp
    (
    USB_TARG_PIPE   pipeHandle,    /* Handle to the pipe */
    pUSB_ERP        pErp           /* ERP to be transfered */
    )
    {
    STATUS              status = ERROR;
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;
    pUSBTGT_PIPE        pUsbTgtPipe = (pUSBTGT_PIPE)pipeHandle;
    
    /* Validate parameters */

    if ((NULL == pUsbTgtPipe) ||
        (NULL == pErp) ||
        (NULL == pUsbTgtPipe->pFuncDriver) ||
        (NULL == pUsbTgtPipe->pFuncDriver->pTcd) ||
        (NULL == pUsbTgtPipe->pFuncDriver->pTcd->pTcdFuncs) ||
        (NULL == pUsbTgtPipe->pFuncDriver->pTcd->pTcdFuncs->pTcdSubmitErp))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pipeHandle) ? "pipeHandle" :
                    (NULL == pErp) ? "pErp" :
                    (NULL == pUsbTgtPipe->pFuncDriver) ? "pFuncDriver" :
                    (NULL == pUsbTgtPipe->pFuncDriver->pTcd) ? "pTcd" :
                    (NULL == pUsbTgtPipe->pFuncDriver->pTcd->pTcdFuncs) ?
                    "pTcdFuncs" : "pTcdSubmitErp"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    pFuncDriver = pUsbTgtPipe->pFuncDriver;

    /* Call the low level TCD to submit erp */
    
    USB_TGT_VDBG("Call the TCD low level to submit erp \n",
                 1, 2, 3, 4, 5, 6);
    
    status = (pFuncDriver->pTcd->pTcdFuncs->pTcdSubmitErp)
             (pFuncDriver->pTcd,
              pUsbTgtPipe->pipeHandle,
              pErp);

    USB_TGT_DBG("Summit Erp with handle 0x%X pErp 0x%X status %d\n",
                (ULONG)pUsbTgtPipe,
                (ULONG)pErp,
                status, 4, 5, 6);

    return status;
    }


/*************************************************************************
*
* usbTgtCancelErp - cancel a previously submitted USB_ERP
*
* This routine cancels an ERP which was previously submitted through
* a call to usbTgtSubmitErp(). This routine will call the cancel ERP routine 
* which registered in the TCD driver table.
*
* RETURNS: OK, or ERROR if unable to cancel USB_ERP
*
* ERRNO: N/A
*/

STATUS usbTgtCancelErp
    (
    USB_TARG_PIPE   pipeHandle,    /* pipe for transfer to abort */
    pUSB_ERP        pErp           /* ERP to be aborted */
    )
    {
    STATUS                  status = ERROR;
    pUSBTGT_FUNC_DRIVER     pFuncDriver = NULL;
    pUSBTGT_PIPE            pUsbTgtPipe = (pUSBTGT_PIPE)pipeHandle;
    
    /* Validate parameters */

    if ((NULL == pUsbTgtPipe) ||
        (NULL == pErp) ||
        (NULL == pUsbTgtPipe->pFuncDriver) ||
        (NULL == pUsbTgtPipe->pFuncDriver->pTcd) ||
        (NULL == pUsbTgtPipe->pFuncDriver->pTcd->pTcdFuncs) ||
        (NULL == pUsbTgtPipe->pFuncDriver->pTcd->pTcdFuncs->pTcdCancelErp))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pipeHandle) ? "pipeHandle" :
                    (NULL == pErp) ? "pErp" :
                    (NULL == pUsbTgtPipe->pFuncDriver) ? "pFuncDriver" :
                    (NULL == pUsbTgtPipe->pFuncDriver->pTcd) ? "pTcd" :
                    (NULL == pUsbTgtPipe->pFuncDriver->pTcd->pTcdFuncs) ?
                    "pTcdFuncs" : "pTcdCancelErp"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    pFuncDriver = pUsbTgtPipe->pFuncDriver;

    /* Call the low level TCD to cancel erp */

    USB_TGT_VDBG("Call the TCD low level to cancel erp \n",
                 1, 2, 3, 4, 5, 6);
    
    status = (pFuncDriver->pTcd->pTcdFuncs->pTcdCancelErp)
              (pFuncDriver->pTcd,
              pUsbTgtPipe->pipeHandle,
              pErp);

    USB_TGT_DBG("Cancel Erp with handle 0x%X pErp 0x%X status %d\n",
                (ULONG)pUsbTgtPipe,
                (ULONG)pErp,
                status, 4, 5, 6);

    return status;
    }


/*******************************************************************************
*
* usbTgtSetPipeStatus - set pipe stalled/unstalled status
*
* This routine sets the status of the specific pipe. If the target application 
* detects an error while servicing a pipe, it may choose to stall the endpoint
* associated with that pipe.This routine allows the caller to set the state 
* of a pipe as <"stalled"> or <"un-stalled">.
*
* RETURNS: OK, or ERROR if unable to set indicated state
*
* ERRNO: N/A
*/

STATUS usbTgtSetPipeStatus
    (
    USB_TARG_PIPE   pipeHandle,     /* Handle to the pipe */
    UINT16          state           /* State of the pipe to be set */
    )
    {
    STATUS                  status = ERROR;
    pUSBTGT_FUNC_DRIVER     pFuncDriver = NULL;
    pUSBTGT_PIPE            pUsbTgtPipe = (pUSBTGT_PIPE)pipeHandle;
    
    /* Validate parameters */

    if ((NULL == pipeHandle)||
        (NULL == pUsbTgtPipe->pFuncDriver) ||
        (NULL == pUsbTgtPipe->pFuncDriver->pTcd) ||
        (NULL == pUsbTgtPipe->pFuncDriver->pTcd->pTcdFuncs) ||
        (NULL == pUsbTgtPipe->pFuncDriver->pTcd->pTcdFuncs->pTcdPipeStatusSet))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pipeHandle) ? "pipeHandle" :
                    (NULL == pUsbTgtPipe->pFuncDriver) ? "pFuncDriver" :
                    (NULL == pUsbTgtPipe->pFuncDriver->pTcd) ? "pTcd" :
                    (NULL == pUsbTgtPipe->pFuncDriver->pTcd->pTcdFuncs) ?
                    "pTcdFuncs" : "pTcdPipeStatusSet"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    pFuncDriver = pUsbTgtPipe->pFuncDriver;

    /* Call the low level TCD to set the pipe status erp */

    USB_TGT_VDBG("Call the TCD low level to set pipe status \n",
                 1, 2, 3, 4, 5, 6);
    
    status = (pFuncDriver->pTcd->pTcdFuncs->pTcdPipeStatusSet)
              (pFuncDriver->pTcd,
               pUsbTgtPipe->pipeHandle,
               state);

    USB_TGT_DBG("Set pipe status with handle 0x%X set status as 0x%X return %d\n",
                (ULONG)pUsbTgtPipe,
                state,
                status, 4, 5, 6);
    
    return status;
    }


/*******************************************************************************
*
* usbTgtGetPipeStatus - get the status of the pipe
*
* This routine is used to get the status of the pipes per GET_STATUS
* request. The status of the pipe is stored in the pointer variable pBuf
*
* RETURNS: OK, or ERROR if unable to get the status
*
* ERRNO: N/A
*/

STATUS usbTgtGetPipeStatus
    (
    USB_TARG_PIPE   pipeHandle,     /* Handle to the pipe */
    UINT16 *        pBuf            /* Buffer to hold the pipe status */
    )
    {
    STATUS                  status = ERROR;
    pUSBTGT_FUNC_DRIVER     pFuncDriver = NULL;
    pUSBTGT_PIPE            pUsbTgtPipe = (pUSBTGT_PIPE)pipeHandle;
    
    /* Validate parameters */

    if ((NULL == pipeHandle) ||
        (NULL == pBuf) ||
        (NULL == pUsbTgtPipe->pFuncDriver) ||
        (NULL == pUsbTgtPipe->pFuncDriver->pTcd) ||
        (NULL == pUsbTgtPipe->pFuncDriver->pTcd->pTcdFuncs) ||
        (NULL == pUsbTgtPipe->pFuncDriver->pTcd->pTcdFuncs->pTcdPipeStatusGet))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pipeHandle) ? "pipeHandle" :
                    (NULL == pBuf) ? "pBuf" :
                    (NULL == pUsbTgtPipe->pFuncDriver) ? "pFuncDriver" :
                    (NULL == pUsbTgtPipe->pFuncDriver->pTcd) ? "pTcd" :
                    (NULL == pUsbTgtPipe->pFuncDriver->pTcd->pTcdFuncs) ?
                    "pTcdFuncs" : "pTcdPipeStatusGet"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    pFuncDriver = pUsbTgtPipe->pFuncDriver;

    /* Call the low level TCD to get the pipe status erp */

    USB_TGT_VDBG("Call the TCD low level to get pipe status \n",
                 1, 2, 3, 4, 5, 6);
    
    status = (pFuncDriver->pTcd->pTcdFuncs->pTcdPipeStatusGet)
             (pFuncDriver->pTcd,
              pUsbTgtPipe->pipeHandle,
              pBuf);

    USB_TGT_DBG("Get pipe status with handle 0x%X status 0x%X return %d\n",
                (ULONG)pUsbTgtPipe,
                (UINT16) *pBuf,
                status, 4, 5, 6);

    return status;
    }

/*******************************************************************************
*
* usbTgtTcdSoftConnect - do soft connect of the TCD
*
* This routine is used to do soft connect of the TCD. <bConnectUp> is used to 
* indicate to connect (set to be TRUE) or disconnect (clear to be FALSE).
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*/

STATUS usbTgtTcdSoftConnect
    (
    pUSBTGT_TCD pTcd,
    BOOL        bConnectUp
    )
    {
    STATUS status = ERROR;
    
    /* Validate parameters */

    if ((NULL == pTcd) ||
        (NULL == pTcd->pTcdFuncs) ||
        (NULL == pTcd->pTcdFuncs->pTcdSoftConnect)) 
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTcd) ? "pTcd" :
                     (NULL == pTcd->pTcdFuncs) ? "pTcdFuncs" :
                     "pTcdSoftConnect"), 2, 3, 4, 5, 6);

        return ERROR;
        }
    
    /* Call the low level TCD to do the soft connect/disconnect */

    status = (pTcd->pTcdFuncs->pTcdSoftConnect)
              (pTcd, bConnectUp);

    USB_TGT_DBG("Call the TCD softconnect to %s with result 0x%X\n",
                ((TRUE == bConnectUp) ? " Connect" :
                "Disconnect"),
                status,3, 4, 5, 6);

    return status;
    }


/*******************************************************************************
*
* usbTgtFuncMngmtCallback - do the function driver management callback
*
* This routine is used to do the function driver management callback with the
* management code <'mngmtCode'> and <'pContext'>.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*/

STATUS usbTgtFuncMngmtCallback
    (
    pUSBTGT_FUNC_DRIVER   pFuncDriver,
    UINT16                mngmtCode,
    void *                pContext
    )
    {
    STATUS status = ERROR;
    
    /* Validate parameters */

    if ((NULL == pFuncDriver) ||
        (NULL == pFuncDriver->pFuncCallbackTable) ||
        (NULL == pFuncDriver->pFuncCallbackTable->mngmtFunc)) 
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pFuncDriver) ? "pFuncDriver" :
                     (NULL == pFuncDriver->pFuncCallbackTable) ?
                     "pFuncCallbackTable" : "mngmtFunc"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Call the management callback routine registered in the callback table */
    
    status = (pFuncDriver->pFuncCallbackTable->mngmtFunc)
             (pFuncDriver->pCallbackParam, 
              pFuncDriver->targChannel,
              mngmtCode,
              pContext);       

    USB_TGT_DBG("usbTgtFuncMngmtCallback with mngmtCode %d, result 0x%d\n",
                mngmtCode,
                status,3, 4, 5, 6);

    return status;
    }

/*******************************************************************************
*
* usbTgtErpCallback - invoked when ERP completes
*
* The TCD invokes this callback when an ERP completes. This gives us the
* opportunity to monitor ERP execution. We reflect the callback to the
* calling application after we finish our processing.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

VOID usbTgtErpCallback
    (
    pVOID    pErp            /* ptr to ERP */
    )
    {
    pUSB_ERP pErpCallback = (pUSB_ERP) pErp;

    /* Validate Parameter */

    if (pErp == NULL)
        {
        return;
        }

    /* Invoke the user's callback routine */

    if (pErpCallback->userCallback != NULL)
        (*pErpCallback->userCallback) (pErpCallback);

    return;
    }

/*******************************************************************************
*
* usbTgtFuncRemoteWakeUp - issue a remote wakeup signal from the USB device
*
* This routine is used to issue a remote wakeup signal from the USB device.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*/

STATUS usbTgtFuncRemoteWakeUp
    (
    pUSBTGT_FUNC_DRIVER pFuncDriver
    )
    {
    if ((NULL == pFuncDriver) ||
        (NULL == pFuncDriver->pTcd) ||
        (NULL == pFuncDriver->pTcd->pTcdFuncs) ||
        (NULL == pFuncDriver->pTcd->pTcdFuncs->pTcdWakeup))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pFuncDriver) ? "pFuncDriver" :
                     (NULL == pFuncDriver->pTcd) ? "->pTcd" :
                     (NULL == pFuncDriver->pTcd->pTcdFuncs) ? "->pTcdFuncs" :
                     "->pTcdWakeup"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    return (pFuncDriver->pTcd->pTcdFuncs->pTcdWakeup)(pFuncDriver->pTcd);
    
    }

/*******************************************************************************
*
* usbTgtFuncFrameNumGet - get the current frame number from the TCD
*
* This routine is used to iget the current frame number from the USB target 
* controller. It mainly be used in the ISO transaction.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*/

STATUS usbTgtFuncFrameNumGet
    (
    pUSBTGT_FUNC_DRIVER pFuncDriver,
    UINT16 *            pFrameNum
    )
    {
    if ((NULL == pFuncDriver) ||
        (NULL == pFrameNum) ||
        (NULL == pFuncDriver->pTcd) ||
        (NULL == pFuncDriver->pTcd->pTcdFuncs) ||
        (NULL == pFuncDriver->pTcd->pTcdFuncs->pTcdGetFrameNum))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pFuncDriver) ? "pFuncDriver" :
                     (NULL == pFrameNum) ? "pFrameNum" :
                     (NULL == pFuncDriver->pTcd) ? "->pTcd" :
                     (NULL == pFuncDriver->pTcd->pTcdFuncs) ? "->pTcdFuncs" :
                     "->pTcdGetFrameNum"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    return (pFuncDriver->pTcd->pTcdFuncs->pTcdGetFrameNum)
            (pFuncDriver->pTcd, pFrameNum);
    
    }


/*******************************************************************************
*
* usbTgtFuncControlPipeStall - stall the function driver's control pipe
*
* This routine is used to stall the function driver's control pipe. It will be 
* used when someting wrong on the control pipe when process some class/vendor
* requests.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*/

STATUS usbTgtFuncControlPipeStall
    (
    pUSBTGT_FUNC_DRIVER pFuncDriver
    )
    {
    STATUS status = ERROR;
    
    if ((NULL == pFuncDriver)||
        (NULL == pFuncDriver->pTcd))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return ERROR;
        }

    /* Call the interface of usb target stall routine */
    
    status = usbTgtControlPipeStall (pFuncDriver->pTcd);

    return status;
    }

/*******************************************************************************
*
* usbTgtFuncControlSetupErpSubmit - submit the SETUP pecket on the control pipe
*
* This routine is used to submit the SETUP pecket on the control pipe by the 
* function drivers which is ready for processing next class or vendor request.
*
* NOTE: 
* <targChannel> should be indicate to the pUSBTGT_FUNC_DRIVER pointer
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*/

STATUS usbTgtFuncControlSetupErpSubmit
    (
    USB_TARG_CHANNEL targChannel
    )
    {
    pUSBTGT_FUNC_DRIVER pFuncDriver = usbTgtTargChannelToFuncDriver(targChannel);
    pUSBTGT_TCD         pTcd = NULL;
    STATUS              status = ERROR;
    
    /* Validate the endpoint descriptor */

    if ((NULL == pFuncDriver) ||
        (NULL == pFuncDriver->pTcd))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pFuncDriver) ? "targChannel" :
                     "pFuncDriver->pTcd"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Is this supported the new driver */
    
    pTcd = pFuncDriver->pTcd;

    if (TRUE == usbTgtControlErpUsedFlagSet(pTcd))
        {
        usbTgtInitSetupErp(pTcd);

        /* Submit erp */

        if ((status = usbTgtSubmitControlErp (pTcd)) != OK)
            {
            usbTgtControlErpUsedFlagClear(pTcd);
            }
        }
    
    USB_TGT_DBG("usbTgtFuncControlSetupErpSubmit into setup stage with status %d\n",
                 status, 2, 3, 4, 5, 6); 

    return status;
    }


/*******************************************************************************
*
* usbTgtFuncControlDataErpSubmit - submit the DATA IN/OUT stage transaction 
*
* This routine is used to submit the DATA IN/OUT stage transaction on 
* the control pipe by the function drivers which issues the DATA stage 
* transaction after received and processed one class/vendor request.
*
* NOTE: 
* <targChannel> should be indicate to the pUSBTGT_FUNC_DRIVER pointer
* <bfrLen> is the length of the data to be send
* <pBfr> is the pointer of the buffer to store the data
* <userCallback> is used as the client callback by the function driver
* <bDataIn> is used to indicate to issue the DATA IN/OUT stage
* <erpFlags> indicates the ERP transaction flags, such as whether one ZLP 
* pecket will be needed after all the data transfered.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*/

STATUS usbTgtFuncControlDataErpSubmit
    (
    USB_TARG_CHANNEL targChannel, /* Target channel */
    UINT16           bfrLen,      /* Length of response 0 */
    pUINT8           pBfr,        /* Pointer to the data buffer */
    ERP_CALLBACK     userCallback,/* USB Target Applcaition Callback */ 
    BOOL             bDataIn,     /* Is this for data in erp */
    USB_ERP_FLAGS    erpFlags     /* The ERP transaction flags */
    )
    {
    pUSBTGT_FUNC_DRIVER pFuncDriver = usbTgtTargChannelToFuncDriver(targChannel);
    pUSBTGT_TCD         pTcd = NULL;
    STATUS              status = ERROR;
    
    /* Validate the endpoint descriptor */

    if ((NULL == pFuncDriver) ||
        (NULL == pFuncDriver->pTcd) ||
        ((NULL == pBfr) && (0 != bfrLen)))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pFuncDriver) ? "targChannel" :
                    (NULL == pFuncDriver->pTcd) ? "pFuncDriver->pTcd" :
                    "pBfr and bfrLen"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Is this supported the new driver */
    
    pTcd = pFuncDriver->pTcd;

    if (TRUE == usbTgtControlErpUsedFlagSet(pTcd))
        {
        if(bfrLen > USBTGT_MAX_CONTORL_BUF_SIZE)
            {
            USB_TGT_ERR("Please set USBTGT_MAX_CONTORL_BUF_SIZE bigger than 0x%x \n",
                         bfrLen, 2, 3, 4, 5, 6);
            return ERROR;
            }
        /* Copy pBfr tp Targ_Tcd. dataBfr */
    
        OS_MEMCPY(&pTcd->dataBfr[0], pBfr, bfrLen);
    
        /* Re-initialize the defaultControlErp and update it */
    
        usbTgtInitDataErp (pTcd ,
                           pFuncDriver->pFuncSpecific, /* Specific for the function driver */
                           &pTcd->dataBfr[0], /* Data buffer */
                           bfrLen ,           /* Date length */
                           userCallback,      /* User callback */
                           bDataIn,           /* bDirIn */
                           erpFlags);         /* erpFlags */
    
        /* submit the erp */
    
        if ((status = usbTgtSubmitControlErp(pTcd)) != OK)
            {
            usbTgtControlErpUsedFlagClear (pTcd);
            }
        }
    
    USB_TGT_DBG("usbTgtFuncControlDataErpSubmit into setup stage with status %d\n",
                status, 2, 3, 4, 5, 6); 

    return status;

    }

/*******************************************************************************
*
* usbTgtFuncControlStatusErpSubmit - submit the STATUS stage ERP on control pipe
*
* This routine is used to submit the STATUS stage ERP on the control to the host
* by the function drivers, which issues the DATA STAGE transaction
* after received and processed one class/vendor request or finished the DATA
* stage. <bStatusIn> is the parameter to indicate the direction STATUS IN/OUT.
* 
* NOTE: 
*
* <targChannel> should be indicate to the pUSBTGT_FUNC_DRIVER pointer
* <bStatusIn> indicates the direction of the STATUS stage , STATUS IN/OUT
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*/

STATUS usbTgtFuncControlStatusErpSubmit
    (
    USB_TARG_CHANNEL    targChannel,    /* Target channel */
    BOOL                bStatusIn       /* Is status in */
    )
    {
    pUSBTGT_FUNC_DRIVER pFuncDriver = usbTgtTargChannelToFuncDriver(targChannel);
    pUSBTGT_TCD         pTcd = NULL;
    STATUS              status = ERROR;
    
    /* Validate the endpoint descriptor */

    if ((NULL == pFuncDriver) ||
        (NULL == pFuncDriver->pTcd))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pFuncDriver) ? "targChannel" :
                    "pFuncDriver->pTcd"), 2, 3, 4, 5, 6);

        return ERROR;
        }

    pTcd = pFuncDriver->pTcd;

    /* One channel indicate one function device */
    
    if (TRUE == usbTgtControlErpUsedFlagSet(pTcd))
        {

        /* Re-initialize the defaultControlErp and update it */
    
        usbTgtInitStatusErp(pTcd, bStatusIn);
    
        /* submit the erp */
    
        if ((status = usbTgtSubmitControlErp(pTcd))!= OK)
            {
            usbTgtControlErpUsedFlagClear(pTcd);
            }
        }
    
    USB_TGT_DBG("usbTgtFuncControlStatusErpSubmit into setup stage with status %d\n",
                status, 2, 3, 4, 5, 6); 

    return status;
    }


/*******************************************************************************
*
* usbTgtConfigDestroy - destroy one USBTGT_CONFIG structure resource 
*
* This routine destroys one USBTGT_CONFIG structure resource which do not
* needed.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtConfigDestroy
    (
    pUSBTGT_CONFIG pConfig
    )
    {
    if ((NULL == pConfig) ||
        (NULL == pConfig->ConfigMutex))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pConfig) ? "pConfig" :
                     "pFuncDriver"), 2, 3, 4, 5, 6); 

        return ERROR;
        }

    if (NULL != pConfig->ConfigMutex)
        {
        semTake (pConfig->ConfigMutex, USBTGT_WAIT_TIMEOUT);

        (void)semDelete (pConfig->ConfigMutex);
        pConfig->ConfigMutex = NULL;
        }
    
    OS_FREE (pConfig);

    return OK;
    }


/*******************************************************************************
*
* usbTgtEndpointCreate - create one USBTGT_CONFIG structure
*
* This routine creates one USBTGT_CONFIG structure and fill the 
* configuration desrciptor as default value. The user need update the value 
* when use it.
*
* RETURNS: NULL or pointer of one USBTGT_CONFIG structure
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL pUSBTGT_CONFIG usbTgtConfigCreate
    (
    void
    )
    {
    pUSBTGT_CONFIG pConfig = NULL;
    
    pConfig = (pUSBTGT_CONFIG)OSS_CALLOC(sizeof (USBTGT_CONFIG));

    if (NULL == pConfig)
        {
        USB_TGT_ERR("No enough resource to alloc %s \n",
                    "pConfig", 2, 3, 4, 5, 6);
        
        return NULL;
        }
    
    
    pConfig->ConfigMutex = semMCreate(USBTGT_MUTEX_CREATION_OPTS); 
    
    if (NULL == pConfig->ConfigMutex) 
        {
        OS_FREE (pConfig);
        return NULL;
        } 

    semTake (pConfig->ConfigMutex, USBTGT_WAIT_TIMEOUT);

    /* Init the configure descriptor */

    (void)usrUsbTgtDescGet (USB_DESCR_CONFIGURATION, 
                     (char *)&pConfig->ConfigDesc);

    
    (void)semGive (pConfig->ConfigMutex);
    
    USB_TGT_DBG("usbTgtConfigCreate(): One new config created 0x%X \n",
                (ULONG)pConfig, 2, 3, 4, 5, 6);

    return pConfig;
    }


/*******************************************************************************
*
* usbTgtTcdDestroy - destroy the USBTGT_TCD resource which unused anymore
*
* This routine destroys the USBTGT_TCD resource which unused anymore. It will 
* destroy the mutex and delete it from the global list <'gUsbTgtTcdList'>.
* 
* NOTE:
* 
* Before this routine is called, the caller should make sure all the resource 
* has destroied by the caller.
* 
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*/

STATUS usbTgtTcdDestroy
    (
    pUSBTGT_TCD pTcd
    )
    {
    pUSBTGT_FUNC_DRIVER   pFuncDriver = NULL;
    NODE *                pNode = NULL;
    pUSBTGT_CONFIG        pConfig = NULL;
    pUSBTGT_STRING        pString = NULL;

    /* Validate parameters */

    if ((NULL == pTcd))
        {
        USB_TGT_ERR("Invalid parameter, %s is NULL\n",
                    "pTcd", 2, 3, 4, 5 ,6);

        return ERROR;
        }
    
    if (NULL != pTcd->tcdMutex)
        {
        semTake (pTcd->tcdMutex, USBTGT_WAIT_TIMEOUT);
        pTcd->uTCDMagicID = USBTGT_MAGIC_ID_DEAD;

        /* 
         * Check if all the function driver which attached before
         * have been detached
         */

        pNode = lstFirst(&pTcd->funcList);

        while (NULL != pNode)
            {
            pFuncDriver = TCD_LIST_NODE_TO_USBTGT_FUNC(pNode);
            pNode = lstNext(pNode);
            
                USB_TGT_VDBG("Function drivers still attached, detach it\n",
                             1, 2, 3, 4, 5 ,6);
                
           if (ERROR == usbTgtFuncDetach(pFuncDriver->pConfig, pFuncDriver))
               {
               USB_TGT_WARN("Detach the function driver %s fail\n ",
                           (NULL != pFuncDriver->pFuncName) ? 
                           "pFuncDriver->pFuncName" : NULL, 2, 3, 4, 5, 6); 
               }

            if (ERROR != lstFind(&pTcd->funcList, &pFuncDriver->funcInTcdListNode))
                lstDelete (&pTcd->funcList, &pFuncDriver->funcInTcdListNode);
            
            }

        /* 
         * Delete all the configurations created
         */

        pNode = lstFirst(&pTcd->configList);
        while (NULL != pNode)
            {
            pConfig = (pUSBTGT_CONFIG)pNode;
            pNode = lstNext(pNode);
            
                USB_TGT_VDBG("Destroy all the config data resource\n",
                             1, 2, 3, 4, 5 ,6);
                
                if (ERROR != lstFind(&pTcd->configList, &pConfig->configNode))
                    lstDelete (&pTcd->configList, &pConfig->configNode);
                
                (void)usbTgtConfigDestroy(pConfig);
                }

        /*
         * Delete all the resource created for string list 
         */
            
        pNode = lstFirst(&pTcd->strDescList);

        while (NULL != pNode)
            {
            pString = (pUSBTGT_STRING)pNode;
            pNode = lstNext(pNode);
            
            USB_TGT_VDBG("Destroy all the config data resource\n",
                         1, 2, 3, 4, 5 ,6);

            if (ERROR != lstFind(&pTcd->strDescList, &pString->strNode))
                lstDelete (&pTcd->strDescList, &pString->strNode);
            
            OSS_FREE(pString);
            }

        /* Delete the mutex */
        
        (void)semDelete (pTcd->tcdMutex);
        pTcd->tcdMutex = NULL;
        }
    
    /* Free the BOS descriptor buffer */
    
    if (NULL != pTcd->pBosBuf)
        {
        OSS_FREE (pTcd->pBosBuf);
        pTcd->pBosBuf = NULL;        
        }
    
    OS_WAIT_FOR_EVENT(gUsbTgtTcdListLock, USBTGT_WAIT_TIMEOUT);

    if (ERROR != lstFind (&gUsbTgtTcdList, &pTcd->usbTgtTcdNode))
        {
        lstDelete (&gUsbTgtTcdList, &pTcd->usbTgtTcdNode);
        }

    (void)OS_RELEASE_EVENT(gUsbTgtTcdListLock);

    /* Free the resource */

    OSS_FREE (pTcd);

    return OK;
    }

/*******************************************************************************
*
* usbTgtTcdCreate - create one USBTGT_TCD resource for target stack
*
* This routine creates one pUSBTGT_TCD resource for target stack and add to 
* the global list<'gUsbTgtTcdList'> ready for using.
*
* NOTE:
* Before this routine is called,the caller need fill up the 
* <'USBTGT_TCD_INFO'> data structure, which are the parameters to create the
* <'USBTGT_TCD'>
*
* RETURNS: NULL or one valid USBTGT_TCD resource point
*
* ERRNO: N/A
*/

pUSBTGT_TCD usbTgtTcdCreate
    (
    pUSBTGT_TCD_INFO pTcdInfo
    )
    {
    pUSBTGT_TCD    pTcd = NULL;
    pUSBTGT_CONFIG pConfig = NULL;
    UINT8          uConfigIndex;
    
    if (NULL == pTcdInfo)
        {
        USB_TGT_ERR("Invalid parameter, %s is NULL\n",
                    "pTcdInfo",
                     2, 3, 4, 5 ,6);

        return NULL;
        }

    pTcd = (pUSBTGT_TCD)OSS_CALLOC(sizeof(USBTGT_TCD));

    /* Check if memory allocation is successful */

    if (NULL == pTcd)
        {
        USB_TGT_ERR("No enough memory to melloc the pUSBTGT_TCD resource\n",
                     1, 2, 3, 4, 5 ,6);

        return NULL;
        }

    pTcd->tcdMutex = semMCreate (USBTGT_MUTEX_CREATION_OPTS);

    if (NULL == pTcd->tcdMutex)
        {
        USB_TGT_ERR("Create Tcd mutex error\n", 1, 2, 3, 4, 5 ,6);

        (void)usbTgtTcdDestroy (pTcd);

        return NULL;
        }

    /* Create the BOS descriptor buffer */

    pTcd->uBosBufLen = USB_BOS_DESCR_LEN +
                       USB_20EXT_DESCR_LEN +
                       USB_SS_DEV_CAP_DESCR_LEN + 
                       USB_CONTAINER_ID_DESCR_LEN;
    
    pTcd->pBosBuf = (UINT8 *)OSS_CALLOC(pTcd->uBosBufLen);

    if (NULL == pTcd->pBosBuf)
        {
        USB_TGT_ERR("Create BOS descriptor buffer fail\n", 1, 2, 3, 4, 5 ,6);

        (void)usbTgtTcdDestroy (pTcd);

        return NULL;
        }
    
    /* Init the function driver lists */

    lstInit(&pTcd->funcList);

    /* Init the configuration lists */

    lstInit(&pTcd->configList);

    /* Init the string descriptors lists */
    
    lstInit(&pTcd->strDescList);

    /* Update the create information */
    
    pTcd->pDev =pTcdInfo->pDev;
    pTcd->pTcdFuncs = pTcdInfo->pTcdFuncs;    
    pTcd->pTcdSpecific = pTcdInfo->pTcdSpecific; 
    
    pTcd->uVendorID = pTcdInfo->uVendorID;  
    pTcd->uProductID = pTcdInfo->uProductID;  
    pTcd->uBcdDevice = pTcdInfo->uBcdDevice;
    pTcd->pMfgString = pTcdInfo->pMfgString;  
    pTcd->pProdString = pTcdInfo->pProdString;  
    pTcd->pSerialString = pTcdInfo->pSerialString;
    pTcd->pTcdName = pTcdInfo->pTcdName;    
    pTcd->uTcdUnit = pTcdInfo->uTcdUnit; 

    memcpy(&pTcd->DeviceInfo, &pTcdInfo->DeviceInfo,
           sizeof(USBTGT_DEVICE_INFO));

    for (uConfigIndex = 0; 
         uConfigIndex < pTcdInfo->uConfigCount; 
         uConfigIndex ++)
        {
        pConfig = usbTgtConfigCreate();
        if (NULL == pConfig)
            {
            USB_TGT_ERR("Create config fail \n",
                        uConfigIndex, 2, 3, 4, 5 ,6);
            
            (void)usbTgtTcdDestroy (pTcd);
            
            return NULL;
            }
        
        /* Copy the Device Info to all the configurations */

        memcpy(&pConfig->DeviceInfo, &pTcdInfo->DeviceInfo,
               sizeof(USBTGT_DEVICE_INFO));

        pConfig->pTcd = pTcd;
        pConfig->uConfigIndex = uConfigIndex;
        lstAdd (&pTcd->configList,&pConfig->configNode);
        }        

    pTcd->uTCDMagicID = USBTGT_MAGIC_ID_ADD_EN;

    /* 
     * Add the device language descriptor
     * The user can set the language descriptor in usrUsbTgtInit.c
     * according to his specific needs. If not set, the ENGLISH language
     * will be used as default.
     */

    pTcd->pUsbTgtLangDescr = usrUsbTgtLangDescrGet();

    /* Init the device descriptor */
    
    (void)usrUsbTgtDescGet (USB_DESCR_DEVICE, (char *)&pTcd->DeviceDesc);
        
    OS_WAIT_FOR_EVENT(gUsbTgtTcdListLock, USBTGT_WAIT_TIMEOUT);

    lstAdd (&gUsbTgtTcdList, &pTcd->usbTgtTcdNode);

    (void)OS_RELEASE_EVENT(gUsbTgtTcdListLock);
    
    return pTcd;
    }


/*******************************************************************************
*
* usbTgtFuncDestroy - destroy the target function driver resource  
*
* This routine destroys the target function driver resource USBTGT_FUNC_DRIVER 
* of the target stack which created by <'usbTgtFuncResourceCreate'> routine.
*
* NOTE:
* 
* Before this routine is called, the caller should make sure all the resource 
* has destroied by the caller.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*/

STATUS usbTgtFuncDestroy
    (
    pUSBTGT_FUNC_DRIVER pFuncDriver
    )
    {

    /* Validate parameters */

    if (NULL == pFuncDriver)
        {
        USB_TGT_ERR("Invalid parameter, pFuncDriver is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return ERROR;
        }
    
    if (NULL != pFuncDriver->funcMutex)
        {
        /* Mutex should be taken before delete it */

        semTake (pFuncDriver->funcMutex, USBTGT_WAIT_TIMEOUT);
        pFuncDriver->uFuncMagicID = USBTGT_MAGIC_ID_DEAD;

        if (NULL != pFuncDriver->pConfig)
            {
            if (ERROR == usbTgtFuncDetach(pFuncDriver->pConfig, pFuncDriver))
               {
               USB_TGT_WARN("Detach the function driver %s fail\n ",
                           (NULL != pFuncDriver->pFuncName) ? 
                           "pFuncDriver->pFuncName" : NULL, 2, 3, 4, 5, 6); 
               }
            }
        
        (void)semDelete (pFuncDriver->funcMutex);
        pFuncDriver->funcMutex = NULL;
        }

    /* Delete the function driver from the global list */
    
    OS_WAIT_FOR_EVENT(gUsbTgtFuncListLock, USBTGT_WAIT_TIMEOUT);
    if (ERROR != lstFind(&gUsbTgtFuncDriverList, &pFuncDriver->funcDriverNode))
        lstDelete(&gUsbTgtFuncDriverList, &pFuncDriver->funcDriverNode);

    (void)OS_RELEASE_EVENT(gUsbTgtFuncListLock);

    /* Free the resource */
    
    OS_FREE (pFuncDriver);

    return OK;
    }


/*******************************************************************************
*
* usbTgtFuncCreate - create one target function driver resource 
*
* This routine creates one target function driver resource pUSBTGT_FUNC_DRIVER
* for target and add to the global list<'gUsbTgtFuncDriverList'> ready for using.
*
* NOTE:
* Before this routine is called,the caller need fill up the 
* <'USBTGT_FUNC_INFO'> data structure, which are the parameters to create the
* <'pUSBTGT_FUNC_DRIVER'>
*
* RETURNS: NULL or target function driver resource point pUSBTGT_FUNC_DRIVER
*
* ERRNO: N/A
*/

pUSBTGT_FUNC_DRIVER usbTgtFuncCreate
    (
    pUSBTGT_FUNC_INFO pFuncInfo
    )
    {
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;

    if (NULL == pFuncInfo)
        {
        USB_TGT_ERR("Invalid parameter, %s is NULL\n",
                    "pFuncInfo",
                     2, 3, 4, 5 ,6);

        return NULL;
        }
    
    pFuncDriver = (pUSBTGT_FUNC_DRIVER)OSS_CALLOC(sizeof(USBTGT_FUNC_DRIVER));

    /* Check if memory allocation is successful */

    if (NULL == pFuncDriver)
        {
        USB_TGT_ERR("No enough memory to melloc the pUSBTGT_FUNC_DRIVER resource\n",
                     1, 2, 3, 4, 5 ,6);

        return NULL;
        }

    pFuncDriver->funcMutex = semMCreate (USBTGT_MUTEX_CREATION_OPTS);

    if (NULL == pFuncDriver->funcMutex)
        {
        USB_TGT_ERR("Create function driver mutex error\n", 
                    1, 2, 3, 4, 5 ,6);

        if (OK != usbTgtFuncDestroy (pFuncDriver))
            {
            USB_TGT_ERR("usbTgtFuncDestroy fail\n",
                       1, 2, 3, 4, 5, 6);
            }   

        return NULL;
        }

    pFuncDriver->ppFsFuncDescTable = pFuncInfo->ppFsFuncDescTable;      
    pFuncDriver->ppHsFuncDescTable = pFuncInfo->ppHsFuncDescTable;
    pFuncDriver->ppSsFuncDescTable = pFuncInfo->ppSsFuncDescTable;
    pFuncDriver->pFuncCallbackTable = pFuncInfo->pFuncCallbackTable;
    pFuncDriver->pCallbackParam = pFuncInfo->pCallbackParam; 
    pFuncDriver->pFuncSpecific = pFuncInfo->pFuncSpecific;        
    pFuncDriver->pFuncName = pFuncInfo->pFuncName;         
    pFuncDriver->pTcdName = pFuncInfo->pTcdName;             
    pFuncDriver->uFuncUnit = pFuncInfo->uFuncUnit;            
    pFuncDriver->uTcdUnit = pFuncInfo->uTcdUnit;             
    pFuncDriver->uConfigToBind = pFuncInfo->uConfigToBind;
    pFuncDriver->pWcidString = pFuncInfo->pWcidString;
    pFuncDriver->pSubWcidString = pFuncInfo->pSubWcidString;
    pFuncDriver->uWcidVc = pFuncInfo->uWcidVc;

    pFuncDriver->uFuncMagicID = USBTGT_MAGIC_ID_ADD_EN;

    /* Add the function driver to the global function list */
    
    OS_WAIT_FOR_EVENT(gUsbTgtFuncListLock, USBTGT_WAIT_TIMEOUT);

    lstAdd (&gUsbTgtFuncDriverList, &pFuncDriver->funcDriverNode);

    (void)OS_RELEASE_EVENT(gUsbTgtFuncListLock);

    USB_TGT_DBG("usbTgtFuncCreate pFuncDriver %p with tcdName %s funcName %s\n",
                pFuncDriver, 
                pFuncDriver->pTcdName, 
                pFuncDriver->pFuncName, 4,5,6);
    
    return pFuncDriver;
    }


/*******************************************************************************
*
* usbTgtDefaultPipeReset - reset the default control pipe
*
* This routine is used to reset the default control pipe of the TCD according
*  to the hardware speed.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtDefaultPipeReset
    (
    pUSBTGT_TCD pTcd
    )
    {
    USB_ENDPOINT_DESCR  Ep0Descr;
    STATUS              status = ERROR;
    
    /* Validate parameters */
    
    if ((NULL == pTcd) ||
        (NULL == pTcd->pTcdFuncs) ||
        (NULL == pTcd->pTcdFuncs->pTcdDeletePipe) ||
        (NULL == pTcd->pTcdFuncs->pTcdCreatePipe))
        {
        USB_TGT_ERR("Invalid parameter, %s is NULL\n",
                    "pTcd", 2, 3, 4, 5 ,6);
        
        return ERROR;
        }

    /* Create/Re-create control pipe */

    if (pTcd->controlPipe)
        {
        /* Delete the pipe */
        
        USB_TGT_DBG("Delete the control pipe 0x%X\n",
                    pTcd->controlPipe, 2, 3, 4, 5 ,6);
        
        pTcd->pTcdFuncs->pTcdDeletePipe(pTcd, pTcd->controlPipe);
        
        pTcd->controlPipe = NULL;
        }
    
    USB_TGT_DBG ("usbTgtDefaultPipeReset uSpeed %x\n",
                 pTcd->uSpeed, 2,3,4,5,6);
    
    pTcd->uCurrentConfig = 0;

    
    memset (&Ep0Descr, 0 , sizeof (USB_ENDPOINT_DESCR));

    /* Set the descriptor for Control In Endpoint */
    
    /* Size of Descriptor */

    Ep0Descr.length = sizeof (USB_ENDPOINT_DESCR);

    /* Descriptor Type */

    Ep0Descr.descriptorType = USB_DESCR_ENDPOINT;

    /* Endpoint Address */

    Ep0Descr.endpointAddress = USB_ENDPOINT_DEFAULT_CONTROL |
                               USB_ENDPOINT_IN;
    
    /* Control Endpoint */

    Ep0Descr.attributes = USB_ATTR_CONTROL;

    if (USBTGT_EP0_SUPERSPEED_PKT_INDEX == pTcd->DeviceDesc.maxPacketSize0)
        {
        Ep0Descr.maxPacketSize = TO_LITTLEW(USBTGT_EP0_SUPERSPEED_PKT_SIZE);
        }
    else
        {
        UINT16 maxPacketSize0 = (UINT16)pTcd->DeviceDesc.maxPacketSize0;
        Ep0Descr.maxPacketSize = TO_LITTLEW(maxPacketSize0);
        }

    /* Interval for control In */

    Ep0Descr.interval = 0;
    
    status = (pTcd->pTcdFuncs->pTcdCreatePipe)
              (pTcd,
               &Ep0Descr,
               0,
               0,
               0,
               &pTcd->controlPipe);

    if (ERROR == status)
        {
        USB_TGT_ERR("Create the control pipe fail\n",
                    1, 2, 3, 4, 5 ,6);

        return ERROR;
        }
    
    /* Submit the Setup pkt Erp */

    if (TRUE == usbTgtControlErpUsedFlagSet(pTcd))
        {
        usbTgtInitSetupErp (pTcd);
    
        if (ERROR == (status = usbTgtSubmitControlErp (pTcd)))
            {
            usbTgtControlErpUsedFlagClear(pTcd);
            }
        }
    else
        {
        USB_TGT_ERR("usbTgtDefaultPipeReset pending?\n ",
                     1, 2, 3, 4, 5, 6); 
        
        status = ERROR;
        }

    USB_TGT_VDBG("usbTgtDefaultPipeReset status %d\n ",
                 status, 2, 3, 4, 5, 6); 
    
    return status;
    }


/*******************************************************************************
*
* usbTgtResetEventHandler - handle the reset event from the hardware
*
* This routine is used to handle the reset event from the hardware. It will
* notify all the function drivers reset event received by calling the 
* function driver's management callback routine.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtResetEventHandler
    (
    pUSBTGT_TCD pTcd
    )
    {
    USB_ENDPOINT_DESCR  Ep0Descr;
    STATUS              status = ERROR;
    NODE *              pNode = NULL;
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;
    
    /* Validate parameters */
    
    if ((NULL == pTcd) ||
        (NULL == pTcd->pTcdFuncs) ||
        (NULL == pTcd->pTcdFuncs->pTcdDeletePipe) ||
        (NULL == pTcd->pTcdFuncs->pTcdCreatePipe))
        {
        USB_TGT_ERR("Invalid parameter, %s is NULL\n",
                    "pTcd", 2, 3, 4, 5 ,6);
        
        return ERROR;
        }
    
    pTcd->uCurrentConfig = 0;

    /* Update the TCD descritpors */
    
    usbTgtTcdInfoUpdate(pTcd);
    
    /* Notify all the functions the Reset event received */

    semTake (pTcd->tcdMutex, USBTGT_WAIT_TIMEOUT);
    
    pNode = lstFirst (&pTcd->funcList);

    while (pNode)
        {
        pFuncDriver = TCD_LIST_NODE_TO_USBTGT_FUNC(pNode);

        status = usbTgtFuncMngmtCallback(pFuncDriver,
                       TARG_MNGMT_BUS_RESET,/* USBTGT_NOTIFY_RESET_EVENT, */
                       (pVOID)(ULONG)pTcd->uSpeed);

        if (NULL != pFuncDriver) 
            {
            pFuncDriver->uCurrentConfig = 0;
            }

        if (ERROR == status)
           {
           USB_TGT_WARN("Notify the function one Reset info 0x%X error\n ",
                         pFuncDriver, 2, 3, 4, 5, 6); 

           /* Do not return */

           /* return ERROR; */
           }

        usbTgtFuncDescUpdate(pFuncDriver);
        
        pNode = lstNext(pNode);
        }
    
    (void)semGive (pTcd->tcdMutex);

    /* Reset the control pipe */

    if (ERROR == usbTgtDefaultPipeReset(pTcd))
        {
        USB_TGT_ERR("usbTgtResetEventHandler() reset the pipe fail\n",
                    1, 2, 3, 4, 5 ,6);
        
        return ERROR;
        }

   /* Do not do the soft connect here */
    
    USB_TGT_VDBG("usbTgtResetEventHandler status %d\n ",
                 status, 2, 3, 4, 5, 6); 
    
    return status;
    }


/*******************************************************************************
*
* usbTgtDisconnectEventHandler - handle the disconnect event from the hardware
*
* This routine is used to handle the disconnect event from the hardware. It will
* notify all the function drivers disconnect event received by calling the 
* function driver's management callback routine.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtDisconnectEventHandler
    (
    pUSBTGT_TCD pTcd
    )
    {
    NODE *                pNode = NULL;
    pUSBTGT_FUNC_DRIVER   pFuncDriver = NULL;
    STATUS                status;

    /* Validate parameters */
 
    if ((NULL == pTcd) ||
        (NULL == pTcd->pTcdFuncs) ||
        (NULL == pTcd->pTcdFuncs->pTcdDeletePipe))
        {
        USB_TGT_ERR("Invalid parameter, %s is NULL\n",
                    ((NULL == pTcd) ? "pTcd" :
                    (NULL == pTcd->pTcdFuncs) ? "pTcdFuncs" :
                    "pTcdDeletePipe"), 2, 3, 4, 5 ,6);
        
        return ERROR;
        }

    /* Delete the control pipe */

    if (pTcd->controlPipe)
        {
        /* Delete the pipe */
        
        USB_TGT_DBG("Delete the control piope 0x%X\n",
                    pTcd->controlPipe, 2, 3, 4, 5 ,6);

        pTcd->pTcdFuncs->pTcdDeletePipe(pTcd, pTcd->controlPipe);

        pTcd->controlPipe = NULL;
        }
    
    /* Set default value */

    pTcd->uCurrentConfig = 0;

    /* Notify all the functions the disconnect event received */

    semTake (pTcd->tcdMutex, USBTGT_WAIT_TIMEOUT);
    pNode = lstFirst (&pTcd->funcList);

    while (pNode)
        {
        pFuncDriver = TCD_LIST_NODE_TO_USBTGT_FUNC(pNode);
        
        status = usbTgtFuncMngmtCallback(pFuncDriver,
                       TARG_MNGMT_DISCONNECT,/* USBTGT_NOTIFY_DISCONNECT_EVENT,*/
                       NULL);

        if (ERROR == status)
           {
           USB_TGT_WARN("Notify the function one Reset info 0x%X error\n ",
                       (ULONG)pFuncDriver, 2, 3, 4, 5, 6); 

           /* Do not return */

           /* return ERROR; */
           }
        
        pNode = lstNext(pNode);
        }
    (void)semGive (pTcd->tcdMutex);
    
    return OK;
  
    }

/*******************************************************************************
*
* usbTgtResumeEventHandler - handle the resume event from the hardware
*
* This routine is used to handle the resume event from the hardware. It will
* notify all the function drivers resume event received by calling the function
* driver's management callback routine.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL 
*/

LOCAL STATUS usbTgtResumeEventHandler
    (
    pUSBTGT_TCD pTcd
    )
    {
    NODE *                pNode = NULL;
    pUSBTGT_FUNC_DRIVER   pFuncDriver = NULL;
    STATUS                status;

    /* Validate parameters */

    if (NULL == pTcd)
        {
        USB_TGT_ERR("Invalid parameter, %s is NULL\n",
                    "pTcd", 2, 3, 4, 5 ,6);
        
        return ERROR;
        }

    /* Notify all the functions the suspend event received */

    semTake (pTcd->tcdMutex, USBTGT_WAIT_TIMEOUT);
    pNode = lstFirst (&pTcd->funcList);

    while (pNode)
        {
        pFuncDriver = TCD_LIST_NODE_TO_USBTGT_FUNC(pNode);

        status = usbTgtFuncMngmtCallback(pFuncDriver,
                       TARG_MNGMT_RESUME,/* USBTGT_NOTIFY_RESUME_EVENT,*/
                       NULL);

        if (ERROR == status)
           {
           USB_TGT_WARN("Notify the function one Reset info 0x%X error\n ",
                       (ULONG)pFuncDriver, 2, 3, 4, 5, 6); 

           /* Do not return */

           /* return ERROR; */
           }
        
        pNode = lstNext(pNode);
        }
    (void)semGive (pTcd->tcdMutex);
    
    return OK;
    }

/*******************************************************************************
*
* usbTgtSuspendEventHandler - handle the suspend event from the hardware
*
* This routine is used to handle the suspend event from the hardware. It will
* notify all the function drivers suspend event received by calling the function
* driver's management callback routine.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL 
*/

LOCAL STATUS usbTgtSuspendEventHandler
    (
    pUSBTGT_TCD pTcd
    )
    {
    NODE *                pNode = NULL;
    pUSBTGT_FUNC_DRIVER   pFuncDriver = NULL;
    STATUS                status;

    /* Validate parameters */

    if (NULL == pTcd)
        {
        USB_TGT_ERR("Invalid parameter, %s is NULL\n",
                    "pTcd", 2, 3, 4, 5 ,6);
        
        return ERROR;
        }

    /* Notify all the functions the suspend event received */

    semTake (pTcd->tcdMutex, USBTGT_WAIT_TIMEOUT);
    
    pNode = lstFirst (&pTcd->funcList);

    while (pNode)
        {
        pFuncDriver = TCD_LIST_NODE_TO_USBTGT_FUNC(pNode);

        status = usbTgtFuncMngmtCallback(pFuncDriver,
                       TARG_MNGMT_SUSPEND,/*USBTGT_NOTIFY_SUSPEND_EVENT,*/
                       NULL);

        if (ERROR == status)
           {
           USB_TGT_WARN("Notify the function one Reset info 0x%X error\n ",
                       (ULONG)pFuncDriver, 2, 3, 4, 5, 6); 

           /* Do not return */

           /* return ERROR; */
           }
        pNode = lstNext(pNode);
        }

    (void)semGive (pTcd->tcdMutex);
    
    return OK;
    }

/*******************************************************************************
*
* usbTgtTcdEventNotify - send one Tcd event notification
*
* This routine is used to send one Tcd event notification to the target 
* management.It will call the event handler according to the <'NotifyCode'>
* code.
*
* RETURNS: N/A.
*
* ERRNO: N/A
*/

void usbTgtTcdEventNotify
    (
    pUSBTGT_TCD        pTcd,
    USBTGT_NOTIFY_CODE NotifyCode
    )
    {
    /* Validate parameters */
    
    if ((NULL == pTcd) ||
        (USBTGT_NOTIFY_UNKNOWN == NotifyCode))
        {
        USB_TGT_ERR("invalid parameter %s \n",
                    ((NULL == pTcd) ? "pTcd is NULL" :
                     "Unknown NotifyCode"), 2, 3, 4, 5 ,6);

        return;
        }

    switch (NotifyCode)
        {
        case USBTGT_NOTIFY_RESET_EVENT:
            
            USB_TGT_DBG("usbTgtTcdEventNotify %s \n",
                        "USBTGT_NOTIFY_RESET_EVENT", 2, 3, 4, 5 ,6);
            
             (void)usbTgtResetEventHandler(pTcd);
             
            break;
        case USBTGT_NOTIFY_DISCONNECT_EVENT:
            
            USB_TGT_DBG("usbTgtTcdEventNotify %s \n",
                        "USBTGT_NOTIFY_DISCONNECT_EVENT", 2, 3, 4, 5 ,6);
            
             (void)usbTgtDisconnectEventHandler(pTcd);
             
            break;
        case USBTGT_NOTIFY_SUSPEND_EVENT:
            
            USB_TGT_DBG("usbTgtTcdEventNotify %s \n",
                        "USBTGT_NOTIFY_SUSPEND_EVENT", 2, 3, 4, 5 ,6);
            
             (void)usbTgtSuspendEventHandler(pTcd);
             
            break;
        case USBTGT_NOTIFY_RESUME_EVENT:
            
            USB_TGT_DBG("usbTgtTcdEventNotify %s \n",
                        "USBTGT_NOTIFY_RESUME_EVENT", 2, 3, 4, 5 ,6);
            
             (void)usbTgtResumeEventHandler(pTcd);
             
            break;
        default:

            USB_TGT_ERR("Unknown NotifyCode %d \n",
                        NotifyCode, 2, 3, 4, 5 ,6);
            
            break;
        }

    return ;
    }

/*******************************************************************************
*
* usbTgtFindFuncByName - find the function driver pointer by name and unit number
*
* This routine finds the function driver pointer from the global list 
* <'gUsbTgtFuncDriverList'> by funtion name <'pFuncName'>and the unit number
* <'uFuncUnit'>.
*
* RETURNS: pointer of the function driver or NULL if no found
*
* ERRNO: N/A
*/

pUSBTGT_FUNC_DRIVER usbTgtFindFuncByName
    (
    char * pFuncName,
    UINT8  uFuncUnit
    )
    {
    NODE *              pNode = NULL;
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;

    /* Validate parameters */

    if (pFuncName == NULL)
        {
        USB_TGT_ERR("Invalid parameter, function driver's name is NULL\n",
                     1, 2, 3, 4, 5 ,6);
        
        return (NULL);
        }
    
    OS_WAIT_FOR_EVENT(gUsbTgtFuncListLock, USBTGT_WAIT_TIMEOUT);

    for (pNode = lstFirst(&gUsbTgtFuncDriverList); 
         pNode != NULL;
         pNode = pNode->next)
        {
        pFuncDriver = (pUSBTGT_FUNC_DRIVER)pNode;

        if ((pFuncDriver->uFuncUnit == uFuncUnit) &&
            (0 == (strncmp(pFuncDriver->pFuncName, pFuncName, USBTGT_MAX_NAME_SZ))))
            {
            /* Release the event */
        
            (void)OS_RELEASE_EVENT(gUsbTgtFuncListLock);

            USB_TGT_DBG("Find the pFuncDriver 0x%X who is named as %s%d\n",
                       (ULONG)pFuncDriver, pFuncName, uFuncUnit, 4, 5 ,6);
            
            return pFuncDriver;
            }
        }
         
    /* Release the event */

    (void)OS_RELEASE_EVENT(gUsbTgtFuncListLock);

    USB_TGT_ERR("No find the function driver who is named as %s%d\n",
                pFuncName, uFuncUnit, 3, 4, 5 ,6);

    return NULL;
    }

/*******************************************************************************
*
* usbTgtFindTargChannelByName - find function targChannel by name and unit num
*
* This routine finds the function driver's targChannel from the global list 
* <'gUsbTgtFuncDriverList'> by funtion name <'pFuncName'>and the unit number
* <'uFuncUnit'>.
*
* RETURNS: USBTGT_TARG_CHANNEL_DEAD, or valid function driver targChannel
*
* ERRNO: N/A
*/

USB_TARG_CHANNEL usbTgtFindTargChannelByName
    (
    char * pFuncName,
    UINT8  uFuncUnit
    )
    {
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;

    /* Get the function driver */
    
    pFuncDriver = usbTgtFindFuncByName(pFuncName, uFuncUnit);

    if (NULL != pFuncDriver)
        {
        USB_TGT_VDBG("Return the function targChannel %p who is named as %s%d\n",
                     pFuncDriver->targChannel, pFuncName, uFuncUnit, 4, 5 ,6);
        
        return pFuncDriver->targChannel;
        }

    USB_TGT_ERR("No find the function targChannel who is named as %s%d\n",
                pFuncName, uFuncUnit, 3, 4, 5 ,6);

    return USBTGT_TARG_CHANNEL_DEAD;
    }


/*******************************************************************************
*
* usbTgtFindTcdByName - find the Tcd pointer by the name and unit number
*
* This routine finds the target pointer from the global list<'gUsbTgtTcdList'> 
* by the name <'pTcdName'> and unit number <'uTcdUnit'>
*
* RETURNS: pointer to the target or NULL if no found
*
* ERRNO: N/A
*/

pUSBTGT_TCD usbTgtFindTcdByName
    (
    char * pTcdName,
    UINT8  uTcdUnit
    )
    {
    NODE *      pNode = NULL;
    pUSBTGT_TCD pTcd = NULL;

    /* Validate parameters */

    if (pTcdName == NULL)
        {
        USB_TGT_ERR("Invalid parameter, pTcdName is NULL\n",
                     1, 2, 3, 4, 5 ,6);
        
        return (NULL);
        }
    
    OS_WAIT_FOR_EVENT(gUsbTgtTcdListLock, USBTGT_WAIT_TIMEOUT);

    for (pNode = lstFirst(&gUsbTgtTcdList); 
         pNode != NULL;
         pNode = pNode->next)
        {
        pTcd = (pUSBTGT_TCD)pNode;

        if ((pTcd->uTcdUnit == uTcdUnit) &&
            (0 == (strncmp(pTcd->pTcdName, pTcdName, USBTGT_MAX_NAME_SZ))))
            {
            /* Release the event */
        
            (void)OS_RELEASE_EVENT(gUsbTgtTcdListLock);

            USB_TGT_DBG("Find the pTcd 0x%X who is named as %s unit %d\n",
                       (ULONG)pTcd, pTcdName, uTcdUnit, 4, 5 ,6);
            
            return pTcd;
            }
        }
         
    /* Release the event */

    (void)OS_RELEASE_EVENT(gUsbTgtTcdListLock);

    USB_TGT_ERR("No find the TCD who is named as %s unit %d\n",
                 pTcdName, uTcdUnit, 3, 4, 5 ,6);

    return NULL;
    }


/*******************************************************************************
*
* usbTgtFindTcdByName - find the Tcd pointer by the name and unit number
*
* This routine finds the target pointer from the global list<'gUsbTgtTcdList'> 
* by the name <'pTcdName'> and unit number <'uTcdUnit'>
*
* RETURNS: pointer to the target or NULL if no found
*
* ERRNO: N/A
*/

USBTGT_TCD_HANDLE usbTgtFindTcdHandleByName
    (
    char * pTcdName,
    UINT8  uTcdUnit
    )
    {
    pUSBTGT_TCD pTcd = NULL;

    pTcd = usbTgtFindTcdByName(pTcdName, uTcdUnit);

    if (NULL == pTcd)
        {
        USB_TGT_ERR("No find the TCD handle who is named as %s%d\n",
                     pTcdName, uTcdUnit, 3, 4, 5 ,6);
        }
    
    return (USBTGT_TCD_CONV_TO_TCD_HANDLE(pTcd));
    }


/*******************************************************************************
*
* usbTgtFuncDescLenGet - get the all the descriptors length 
*
* This routine gets the all the descriptors length of the function 
* driver according to the descriptors table "ppDescTable".
*
* RETURNS: the length of the desctiptors or 0
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL UINT16 usbTgtFuncDescLenGet 
    (
    pUSB_DESCR_HDR * ppDescTable
    )
    {
    pUSB_DESCR_HDR pDescHeader = NULL;
    UINT16         uDataLen = 0;

    /* Validate parameters */

    if (NULL == ppDescTable)
        {
        USB_TGT_ERR("Invalid parameter %s \n",
                    "ppDescTable", 2, 3, 4, 5, 6);
        
        return 0;
        }
    
    pDescHeader = * ppDescTable;

    while (pDescHeader)
        {        
        if ((pDescHeader->descriptorType != USB_DESCR_DEVICE) &&
            (pDescHeader->descriptorType != USB_DESCR_CONFIGURATION))
            {
            uDataLen = (UINT16) (uDataLen + pDescHeader->length);
            }
        
        USB_TGT_DBG("pDescHeader 0x%X length is 0x%X\n",
                    (ULONG)pDescHeader,
                    pDescHeader->length, 3, 4, 5, 6);

        ppDescTable++;
        pDescHeader = * ppDescTable;
        }
    
    USB_TGT_DBG("All the descriptors length is 0x%X \n",
                uDataLen, 2, 3, 4, 5, 6);

    return uDataLen;
    }

/*******************************************************************************
*
* usbTgtEpIndexRelease - release one occupied endpoint index
*
* This routine releases one endpoint index which asigned to the endpoint, and 
* return it to the endpoint capability table for future using. This routine will
* simply set the <'bUsable'> flag to TRUE for the right endpoint index.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtEpIndexRelease
    (
    pUSBTGT_DEVICE_INFO pDevInfo, 
    pUSB_ENDPOINT_DESCR pEpDesc
    )
    {
    UINT8  uEpIndex = 0;

    /* Validate parameters */
    
    if ((NULL == pDevInfo) ||
        (NULL == pEpDesc))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pDevInfo) ? "pDevInfo" :
                     "pEpDesc"), 2, 3, 4, 5, 6);

        /* Return the max endpoint num, ivalid num in the spec */
        
        return ERROR;
        }
    
    uEpIndex = (UINT8) (pEpDesc->endpointAddress & (~USB_ENDPOINT_IN));

    if (pEpDesc->endpointAddress & USB_ENDPOINT_IN)
        {        
        pDevInfo->txEpCaps[uEpIndex].bUsable = TRUE;  
        if (pDevInfo->txEpCaps[uEpIndex].bRxTxShared == TRUE)
        {
        pDevInfo->rxEpCaps[uEpIndex].bUsable = TRUE;        
           }        
        }
    else
        {
        pDevInfo->rxEpCaps[uEpIndex].bUsable = TRUE;        
        if (pDevInfo->rxEpCaps[uEpIndex].bRxTxShared == TRUE)
           {
            pDevInfo->txEpCaps[uEpIndex].bUsable = TRUE;
           }
        }
    
    /* Return the max endpoint num, ivalid num in the spec */

    return OK;

    }

/*******************************************************************************
*
* usbTgtEpIndexReqest - request one valid endpoint index 
*
* This routine requests one valid endpoint index according to the endpoint 
* descriptor from the endpoint capability table, and clear the 
* <'bUsable'> bit flag to mark the occupation if <'bOverWrite'> is set 
* to be TRUE.
*
* RETURNS: one valid endpoint index or USBTGT_MAX_EP_NUM which is 
* invalid in the spec
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL UINT8 usbTgtEpIndexReqest
    (
    pUSBTGT_DEVICE_INFO pDevInfo, 
    pUSB_ENDPOINT_DESCR pEpDesc,
    BOOL                bOverWrite
    )
    {
    UINT8  uEpIndex = 0;
    UINT8  uXferType = 0;
    UINT8  uSynchType = 0;
    UINT8  uUsageType = 0;
    UINT16 uMaxPktSize = 0;
    UINT16 uMaxPktSizeTemp = 0;
    UINT8  uMoreTrans = 0;
    
    /* Validate parameters */

    if ((NULL == pDevInfo) ||
        (NULL == pEpDesc))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pDevInfo) ? "pDevInfo" :
                     "pEpDesc"), 2, 3, 4, 5, 6);

        /* Return the max endpoint num, ivalid num in the spec */
        
        return USBTGT_MAX_EP_NUM;
        }

    /* Get the endpoint's transfer type */
    
    uXferType = (UINT8) (pEpDesc->attributes & USB_ATTR_EPTYPE_MASK);

    /* Get the synch type and usage type for the ISO type endpoint */
    
    if (USB_ATTR_ISOCH == uXferType)
        {
        uSynchType = (UINT8) ((pEpDesc->attributes >> USB_ATTR_EP_SYNCH_TYPE_OFFSET) &
                      USB_ATTR_EP_SYNCH_TYPE_MASK);
        uUsageType = (UINT8) ((pEpDesc->attributes >> USB_ATTR_EP_USAGE_TYPE_OFFSET) &
                      USB_ATTR_EP_USAGE_TYPE_MASK);
        }

    /* Get the max pecket size and additional transactions */
    
    uMaxPktSizeTemp = pEpDesc->maxPacketSize;

    /* Change to the CPU endin */
    
    uMaxPktSizeTemp = FROM_LITTLEW(uMaxPktSizeTemp);
    
    uMaxPktSize = (UINT16) (uMaxPktSizeTemp & USB_MAX_PACKET_SIZE_MASK);
    
    uMoreTrans = (UINT8) ((uMaxPktSizeTemp & USB_NUMBER_OF_TRANSACTIONS_MASK) >>
                 USB_NUMBER_OF_TRANSACTIONS_BITPOSITION);
        
    /* Go through the endpoint capability map to find the next valid ep index */

    if (pEpDesc->endpointAddress & USB_ENDPOINT_IN)
        {
        for (uEpIndex = 0; uEpIndex < USBTGT_MAX_EP_NUM; uEpIndex ++)
            {
            /*
             * Check the endpoint capiblity
             * 1. It is usable
             * 2. It supports the transfer type
             * 3. The endpoint's max packet size >= required max packet size
             *    which descrbed in endpoint's descriptor
             */
             
            if ((TRUE == pDevInfo->txEpCaps[uEpIndex].bUsable) &&
                (pDevInfo->txEpCaps[uEpIndex].uXferTypeBitMap & (0x1 << uXferType))/* &&
                (pDevInfo->txEpCaps[uEpIndex].uMaxPktSize >= uMaxPktSize)*/)
                 {
                 /*
                  * When we got here, 
                  * 4. For non-ISO type endpoint, the endpoint can be used
                  * 5. For ISO type endpoint, we still need check
                  *  5.1 If the endpoint support the synch type
                  *  5.2 If the endpoint support th usage type
                  *  5.3 If the hardware support enough additional transactions.
                  */
                  
                 if ((USB_ATTR_ISOCH != uXferType) ||
                     ((pDevInfo->txEpCaps[uEpIndex].uSyncTypeBitMap & (0x1 << uSynchType)) &&
                      (pDevInfo->txEpCaps[uEpIndex].uUsageTypeBitMap & (0x1 << uUsageType)) &&
                      (pDevInfo->txEpCaps[uEpIndex].uMaxTransactions > uMoreTrans)))
                    {
                    USB_TGT_DBG("Find one usable endport with index 0x%X \n",
                               uEpIndex,
                               2, 3, 4, 5, 6);
                    
                     if (bOverWrite)
                         {
                         pDevInfo->txEpCaps[uEpIndex].bUsable = FALSE;
                         if (pDevInfo->txEpCaps[uEpIndex].bRxTxShared == TRUE)
                            {
                             pDevInfo->rxEpCaps[uEpIndex].bUsable = FALSE;
                            }
                         }
                     return uEpIndex;
                    }                 
                 }
            }
        }
    else
        {
        for (uEpIndex = 0; uEpIndex < USBTGT_MAX_EP_NUM; uEpIndex ++)
            {
            /*
             * Check the endpoint capiblity
             * 1. It is usable
             * 2. It supports the transfer type
             * 3. The endpoint's max packet size >= required max packet size
             *    which descrbed in endpoint's descriptor
             */
             
            if ((TRUE == pDevInfo->rxEpCaps[uEpIndex].bUsable) &&
                (pDevInfo->rxEpCaps[uEpIndex].uXferTypeBitMap & (0x1 << uXferType))/* &&
                (pDevInfo->rxEpCaps[uEpIndex].uMaxPktSize >= uMaxPktSize)*/)
                 {
                USB_TGT_DBG("Find one usable endport with index 0x%X \n",
                           uEpIndex,
                           2, 3, 4, 5, 6);
                 /*
                  * When we got here, 
                  * 4. For non-ISO type endpoint, the endpoint can be used
                  * 5. For ISO type endpoint, we still need check
                  *  5.1 If the endpoint support the synch type
                  *  5.2 If the endpoint support th usage type
                  *  5.3 If the hardware support enough additional transactions.
                  */
                  
                 if ((USB_ATTR_ISOCH != uXferType) ||
                     ((USB_ATTR_ISOCH == uXferType) &&
                      (pDevInfo->rxEpCaps[uEpIndex].uSyncTypeBitMap & (0x1 << uSynchType)) &&
                      (pDevInfo->rxEpCaps[uEpIndex].uUsageTypeBitMap & (0x1 << uUsageType)) &&
                      (pDevInfo->rxEpCaps[uEpIndex].uMaxTransactions > uMoreTrans)))
                    {
                    USB_TGT_DBG("Find one usable endport with index 0x%X \n",
                               uEpIndex,
                               2, 3, 4, 5, 6);
                    
                     if (bOverWrite)
                         {
                         pDevInfo->rxEpCaps[uEpIndex].bUsable = FALSE;
                         if (pDevInfo->rxEpCaps[uEpIndex].bRxTxShared == TRUE)
                            {
                             pDevInfo->txEpCaps[uEpIndex].bUsable = FALSE;
                            }
                         }
                     return uEpIndex;
                    }
                 
                 }
            }
        }

    /* Return the max endpoint num, ivalid num in the spec */
    
    USB_TGT_ERR("No resource found for ep Add 0x%x type 0x%x\n",
               pEpDesc->endpointAddress,
               pEpDesc->attributes, 3, 4, 5, 6);

    return USBTGT_MAX_EP_NUM;

    }

/*******************************************************************************
*
* usbTgtFuncStringDescUnLink - unlink all the function driver's string descriptors
* 
* The routine is used to find and unlink all the function driver's string
* descriptors from the TCD's string list.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtFuncStringDescUnLink
    (
    pUSBTGT_TCD         pTcd,
    pUSBTGT_FUNC_DRIVER pFuncDriver
    )
    {
    pUSBTGT_STRING pString = NULL;
    NODE *         pNode = NULL;
    
    if ((NULL == pTcd) ||
        (NULL == pFuncDriver))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTcd) ? "pTcd" :
                      "pFuncDriver"), 2, 3, 4, 5, 6);
                     
        return ERROR;
        }    
    
    pNode = lstFirst(&pTcd->strDescList);

    while(NULL != pNode)
        {
        pString = (pUSBTGT_STRING)pNode;

        /* Record the next node */
        
        pNode = lstNext (pNode);
        
        if (pString->pFuncDriver == pFuncDriver)
            {
            /* Find the node befer delete it */
            
            if (ERROR != lstFind(&pTcd->strDescList, &pString->strNode))
                {
                lstDelete(&pTcd->strDescList, &pString->strNode);
                OSS_FREE (pString);
                }
            }
        }
    
    return OK;
    }

/*******************************************************************************
*
* usbTgtFuncStringDescLink - get and link the function driver's string descriptors
* 
* The routine is used to get and link the function driver's string descriptors
* to the TCD's string list.
*
* RETURNS: TRUE if get the string and linked, or FALSE if not
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL BOOL usbTgtFuncStringDescLink
    (
    pUSBTGT_TCD          pTcd,
    pUSBTGT_FUNC_DRIVER  pFuncDriver,
    UINT16               uLangId,
    pUSB_INTERFACE_DESCR pIfDesc
    )
    {
    STATUS         status = ERROR;
    pUSBTGT_STRING pString = NULL;
    pUSBTGT_STRING pNodeStr = NULL;
    NODE *         pNode    = NULL;
    UINT16         uStrLen = 0;
    
    if ((NULL == pTcd) ||
        (NULL == pIfDesc) ||
        (NULL == pFuncDriver) ||
        (NULL == pFuncDriver->pFuncCallbackTable) ||
        (NULL == pFuncDriver->pFuncCallbackTable->descriptorGet))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pTcd) ? "pTcd" :
                     (NULL == pIfDesc) ? "pIfDesc" :
                     (NULL == pFuncDriver) ? "pFuncDriver" :
                     (NULL == pFuncDriver->pFuncCallbackTable) ? 
                      "pFuncCallbackTable" :"descriptorGet"), 
                      2, 3, 4, 5, 6);
                     
        return FALSE;
        }    
    
     pString = (pUSBTGT_STRING)OSS_CALLOC(sizeof (USBTGT_STRING));
  
     if (NULL == pString)
        {
        USB_TGT_ERR("No enough memory to stall descriptors\n",
                    1, 2, 3, 4, 5, 6);
        
        return FALSE;
        }
     
     status = (pFuncDriver->pFuncCallbackTable->descriptorGet)
              (pFuncDriver->pCallbackParam, 
               pFuncDriver->targChannel,
               USB_RT_DEV_TO_HOST | USB_RT_STANDARD | USB_RT_DEVICE, 
               USB_DESCR_STRING, 
               pIfDesc->interfaceIndex, 
               uLangId, 
               USBTGT_STRING_LEN, 
               &pString->strBuf[0], 
               &uStrLen);
     
     if (ERROR == status)
        {
        USB_TGT_ERR("Get string descriptors for index 0x%X fail\n",
                    pIfDesc->interfaceIndex, 2, 3, 4, 5, 6);
        
        pIfDesc->interfaceIndex = 0;
        
        OSS_FREE (pString);
        
        return FALSE;                      
        }
     
    pNode = lstFirst(&pTcd->strDescList);

    while (NULL != pNode)
        {
        pNodeStr = (pUSBTGT_STRING) pNode;

        if ((pNodeStr->uLangId == uLangId) &&
            (pNodeStr->pFuncDriver == pFuncDriver) &&
            (0 == strncmp((const char *)pNodeStr->strBuf, 
                           (const char *)pString->strBuf,
                            uStrLen)))
            {
            pIfDesc->interfaceIndex = pNodeStr->uNewStrIndex;
            
            OSS_FREE (pString);
            return TRUE;
            }

        pNode = lstNext (pNode);
        }
    
    pString->uLangId = uLangId;
    pString->pFuncDriver = pFuncDriver;
    pString->uOldStrIndex = pIfDesc->interfaceIndex;
    pString->uNewStrIndex = (UINT8) (USBTGT_SERIAL_STRING_INDEX + 
                            lstCount(&pTcd->strDescList) + 1);
    
    pIfDesc->interfaceIndex = pString->uNewStrIndex;
        
    /* Add it to the string descriptors list */

    lstAdd (&pTcd->strDescList, &pString->strNode);
    
    return TRUE;
    }

/*******************************************************************************
*
* usbTgtTcdInfoUpdate - updata the USB target information.
* 
* The routine is used to updata the USB target information, such as the device
* descriptor, IAD descriptor and BOS descriptor and etc.
* It will be called after funtion driver attach/detach to/from the Tcd 
* hardware. 
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbTgtTcdInfoUpdate 
    (
    pUSBTGT_TCD pTcd
    )
    {
    pUSBTGT_CONFIG pConfig = NULL;
    NODE *         pNode = NULL;
    UINT8          uConfigCount = 0;
    UINT8          uMaxPktSize = 0;
    BOOL           bIadNeeded = FALSE;

    /* Validate parameters */

    if (NULL == pTcd)
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    "pTcd", 2, 3, 4, 5, 6);
        return;
        }

    semTake (pTcd->tcdMutex, USBTGT_WAIT_TIMEOUT);
    
    pNode = lstFirst (&pTcd->configList);

    while (NULL != pNode)
        {
        pConfig = (pUSBTGT_CONFIG)pNode;

        if (pConfig->ConfigDesc.numInterfaces != 0)
            {
            uConfigCount ++;
            }        

        pNode = lstNext (pNode);
        }

    /*
     * Get the hardware capability of the EP 0
     */
     
    if (pTcd->DeviceInfo.txEpCaps[0].bUsable &&
        pTcd->DeviceInfo.rxEpCaps[0].bUsable)
        {
        uMaxPktSize = (UINT8) min (pTcd->DeviceInfo.txEpCaps[0].uMaxPktSize,
                                   pTcd->DeviceInfo.rxEpCaps[0].uMaxPktSize);

        }
    else 
        {
        if (pTcd->DeviceInfo.txEpCaps[0].bUsable)
            {
            uMaxPktSize = (UINT8) (pTcd->DeviceInfo.txEpCaps[0].uMaxPktSize);
            }
        else
            {
            uMaxPktSize = (UINT8) (pTcd->DeviceInfo.rxEpCaps[0].uMaxPktSize);
            }        
        }

    if (0 == uMaxPktSize)
        {
        USB_TGT_WARN("usbTgtTcdInfoUpdate(): MaxPktSize of Ep0 is invalid\n",
                     1, 2, 3, 4, 5 ,6);
        }

    USB_TGT_DBG("usbTgtTcdInfoUpdate the Hardware EP0 MaxPktSize is %p\n",
                uMaxPktSize, 2, 3, 4, 5, 6);

    /* 
     * According to the USB 2.0 Specification 
     *
     * 5.5.3  Control Transfer Packet Size Constraints
     *
     * The allowable maximum control transfer data payload sizes for full-speed 
     * devices is 8, 16, 32, or 64 bytes; for high-speed devices, it 
     * is 64 bytes and for low-speed devices, it is 8 bytes. 
     * 
     * NOTE: for the USB3 capable device, bcdUsb should be set 2.10 version
     * for HS/FS/LS speed.
     */

    if ((NULL != pTcd->pTcdFuncs) &&
        (NULL != pTcd->pTcdFuncs->pTcdIoctl))
        {
        (pTcd->pTcdFuncs->pTcdIoctl)(pTcd, 
                                     USBTGT_TCD_IOCTL_CMD_SPEED_UPDATE,
                                     NULL);
        }
    
    switch (pTcd->uSpeed)
        {
        case USB_SPEED_LOW:
            {
            /* Low speed control endpoint uMaxPktSize is 8 bytes */
            
            if (pTcd->DeviceInfo.uDevSpeedCap & (0x1 << USB_SPEED_SUPER))
                {
                pTcd->DeviceDesc.bcdUsb = TO_LITTLEW(USBTGT_VERSION_21);
                }
            else
                {
                pTcd->DeviceDesc.bcdUsb = TO_LITTLEW(USBTGT_VERSION_10);
                }
            pTcd->DeviceDesc.maxPacketSize0 = USBTGT_EP0_MIN_PKT_SZIE;
            pTcd->devQualifierDesc.maxPacketSize0 = USBTGT_EP0_MAX_PKT_SZIE;
            }
            break;
        case USB_SPEED_FULL:
            {
            /* 
             * Full speed control endpoint uMaxPktSize can 
             * be 8, 16, 32, 64 bytes, get the min one 
             */
             
            if (pTcd->DeviceInfo.uDevSpeedCap & (0x1 << USB_SPEED_SUPER))
                {
                pTcd->DeviceDesc.bcdUsb = TO_LITTLEW(USBTGT_VERSION_21);
                }
            else
                {
                pTcd->DeviceDesc.bcdUsb = TO_LITTLEW(USBTGT_VERSION_10);
                }
            pTcd->DeviceDesc.maxPacketSize0 = (UINT8) min (USBTGT_EP0_MAX_PKT_SZIE,
                                                   uMaxPktSize);
            pTcd->devQualifierDesc.maxPacketSize0 = (UINT8) (USBTGT_EP0_MAX_PKT_SZIE);
            }                
            break;
        case USB_SPEED_SUPER:
            {
            /* 
             * Super speed control endpoint uMaxPktSize is 512 bytes 
             * Using 0x09 as the index.
             */
             
            pTcd->DeviceDesc.bcdUsb = TO_LITTLEW(USBTGT_VERSION_30);
            pTcd->DeviceDesc.maxPacketSize0 = (UINT8) (USBTGT_EP0_SUPERSPEED_PKT_INDEX);
            }
            break;
        case USB_SPEED_HIGH:
        default:
            {
            /* High speed control endpoint uMaxPktSize is 64 bytes */
            
            if (pTcd->DeviceInfo.uDevSpeedCap & (0x1 << USB_SPEED_SUPER))
                {
                pTcd->DeviceDesc.bcdUsb = TO_LITTLEW(USBTGT_VERSION_21);
                }
            else
            {
            pTcd->DeviceDesc.bcdUsb = TO_LITTLEW(USBTGT_VERSION_20);
                }
            
            pTcd->DeviceDesc.maxPacketSize0 = USBTGT_EP0_MAX_PKT_SZIE;
            pTcd->devQualifierDesc.maxPacketSize0 = (UINT8) min (USBTGT_EP0_MAX_PKT_SZIE,
                                                         uMaxPktSize);
            }
            break;
        }
    
    /*
     * The user may defined the VID/PID, if the VID is invalid,
     * use the global VID/PID.
     */
    
    if (0x0 == pTcd->DeviceDesc.vendor) 
       {
       pTcd->DeviceDesc.vendor = TO_LITTLEW(pTcd->uVendorID);
       pTcd->DeviceDesc.product = TO_LITTLEW(pTcd->uProductID);
       }

    /* Asigned according to the global table */
        
    pTcd->DeviceDesc.manufacturerIndex = USBTGT_MFG_STRING_INDEX;
    pTcd->DeviceDesc.productIndex = USBTGT_PROD_STRING_INDEX;
    pTcd->DeviceDesc.serialNumberIndex = USBTGT_SERIAL_STRING_INDEX;
    pTcd->DeviceDesc.manufacturerIndex =  USBTGT_MFG_STRING_INDEX; 
    pTcd->DeviceDesc.productIndex =  USBTGT_PROD_STRING_INDEX; 
    pTcd->DeviceDesc.serialNumberIndex =  USBTGT_SERIAL_STRING_INDEX; 
    pTcd->DeviceDesc.numConfigurations = uConfigCount;

    /* 
     * Update the device descriptor if IAD supported 
     * When the first/right configuration contains one
     * function driver needed IAD support, update the device 
     * class code.
     */

    if (pTcd->uCurrentConfig == 0)
        {
        pConfig = (pUSBTGT_CONFIG)lstFirst (&pTcd->configList);
        if (NULL != pConfig)
            {
            bIadNeeded = pConfig->bIadNeeded;
            }
        }
    else
        {
        pConfig = (pUSBTGT_CONFIG)lstNth (&pTcd->configList, pTcd->uCurrentConfig);
        if (NULL != pConfig)
            {
            bIadNeeded = pConfig->bIadNeeded;
            }
        }

    /* IAD needed */
    
    if ((TRUE == usrUsbTgtIadDescEnableGet()) &&
        (TRUE == bIadNeeded))
        {
        pTcd->DeviceDesc.deviceClass = USB_CLASS_MISC;
        pTcd->DeviceDesc.deviceSubClass = USB_SUBCLASS_COMMON;
        pTcd->DeviceDesc.deviceProtocol = USB_PROTOCOL_IAD;
        }

    /* USB 3.0 do not support such descriptor */
    
    if (USB_SPEED_SUPER != pTcd->uSpeed)
        {
        pTcd->devQualifierDesc.length = USB_DEVICE_QUALIFIER_DESCR_LEN;
        pTcd->devQualifierDesc.descriptorType = USBHST_DEVICE_QUALIFIER_DESC;
        
        /* The version number for this descriptor must be at least 2.0 (0200H). */
        
        pTcd->devQualifierDesc.bcdUsb = TO_LITTLEW(USBTGT_VERSION_20);        

    /* Update the qualifier descriptor */
    
    pTcd->devQualifierDesc.deviceClass = pTcd->DeviceDesc.deviceClass;
    pTcd->devQualifierDesc.deviceSubClass = pTcd->DeviceDesc.deviceSubClass;
    pTcd->devQualifierDesc.deviceProtocol = pTcd->DeviceDesc.deviceProtocol;
    pTcd->devQualifierDesc.numConfigurations = pTcd->DeviceDesc.numConfigurations;
    pTcd->devQualifierDesc.reserved = 0;
        }

    /* No matter the speed */
    
    /* Prepare the BOS descriptor */
    
    (void)usrUsbTgtDescGet (USB_DESCR_BOS, 
                     (char *)pTcd->pBosBuf);

    /* Reset the BOS descriptor length */

    pTcd->uBosBufLen = TO_LITTLEW (*(UINT16 *)&pTcd->pBosBuf[2]);
    
    /* 
     * Update the device capiblity 
     * LTM capable
     * Speed supported
     * Functionality support
     * U1ExitLat
     * U2ExitLat
     */    
    
    (void)semGive (pTcd->tcdMutex);

    USB_TGT_DBG("usbTgtTcdInfoUpdate done\n",
                1, 2, 3, 4, 5, 6);
 
    return;
    }

/*******************************************************************************
*
* usbTgtFuncDevNotification - issue the device notification to the USB host
*
* This routine is used to issue the device notification to the USB host.
* This routine is not used right now, ready for feature using.
*
* RETURNS: Always return OK
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbTgtFuncDeviceNotification
    (
    USB_TARG_CHANNEL     targChannel,
    UINT8                uNotifType,
    void *               pContext
    )
    {

    /* Always return OK, will be changed */
    
    return OK;
    }


/*******************************************************************************
*
* usbTgtFuncDetach - detach the funtion driver from the right configuration 
*
* This routine is used to detach the funtion driver from the right configuration 
* <'pConfig'>.
*
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtFuncDetach
    (
    pUSBTGT_CONFIG        pConfig,
    pUSBTGT_FUNC_DRIVER   pFuncDriver
    )
    {
    pUSBTGT_TCD           pTcd = NULL;
    pUSB_DESCR_HDR        pDescHeader = NULL;
    pUSBTGT_DEVICE_INFO   pDevInfo = NULL;
    UINT16                uDescBuffLen = 0;
    UINT16                uLength = 0;
    pUSB_ENDPOINT_DESCR   pEpDesc = NULL;
    STATUS                status = ERROR;

    /* Validate parameters */

    if ((NULL == pConfig) ||
        (NULL == pConfig->pTcd) ||
        (NULL == pFuncDriver)||
        ((NULL == pFuncDriver->pFsDescBuf) && 
         (NULL == pFuncDriver->pHsDescBuf) && 
         (NULL == pFuncDriver->pSsDescBuf)))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pConfig) ? "pConfig" :
                     "pFuncDriver"), 2, 3, 4, 5, 6);
                     
        return ERROR;
        }

    /* Call the function driver detach call back */

    status = usbTgtFuncMngmtCallback(pFuncDriver,
               TARG_MNGMT_DETACH,/* USBTGT_NOTIFY_DETACH_EVENT,*/
               NULL);

    if (ERROR == status)
       {
       USB_TGT_WARN("Notify the function to 0x%X detach error\n ",
                    pFuncDriver, 2, 3, 4, 5, 6); 

       /* Do not return */

        return ERROR; 
       }

    pDevInfo = & pConfig->DeviceInfo;
    pTcd = pConfig->pTcd;

    /* 
     * Release all the endpoints occupied by the function
     */
    
    switch (pTcd->uSpeed)
        {
        case USB_SPEED_HIGH:
            pDescHeader = (pUSB_DESCR_HDR)(pFuncDriver->pHsDescBuf);
            uDescBuffLen = pFuncDriver->uHsDescBufLen;
            break;
        case USB_SPEED_FULL:
        /* Fall through */
        case USB_SPEED_LOW:
            pDescHeader = (pUSB_DESCR_HDR)(pFuncDriver->pFsDescBuf);
            uDescBuffLen = pFuncDriver->uFsDescBufLen;
            break;
        case USB_SPEED_SUPER:
        /* Fall through */
        default:
            pDescHeader = (pUSB_DESCR_HDR)(pFuncDriver->pSsDescBuf);
            uDescBuffLen = pFuncDriver->uSsDescBufLen;
            break;            
        }

    /* Copy all the descriptors to the pDescBuff */
  
    while ((NULL != pDescHeader) &&
          (uLength <= uDescBuffLen) &&
          (pDescHeader->length != 0))
        {
        
        switch (pDescHeader->descriptorType)
            {
            case USB_DESCR_DEVICE:
                break;
            case USB_DESCR_CONFIGURATION:
                 break;
            case USB_DESCR_INTERFACE:

                /* Remove the interface information in the configuration */

                /* Remove the interface index for the string */
                
                break;
            case USB_DESCR_ENDPOINT:

                pEpDesc = (pUSB_ENDPOINT_DESCR)pDescHeader;
                
                semTake (pFuncDriver->funcMutex,USBTGT_WAIT_TIMEOUT);

                if (ERROR == usbTgtEpIndexRelease (pDevInfo, pEpDesc))
                    {
                    USB_TGT_ERR("The endpoint 0x%X not exist? \n",
                               pEpDesc->endpointAddress, 2, 3, 4, 5, 6);
                    
                    (void)semGive (pFuncDriver->funcMutex);

                    return ERROR;
                    }
                (void)semGive (pFuncDriver->funcMutex);
          
                break;
            default:
                break;
            }

        /* Update next step to check the data */
        
        pDescHeader = (pUSB_DESCR_HDR)((UINT8 *)pDescHeader +
                                        pDescHeader->length);
        
        uLength = (UINT16) (uLength + pDescHeader->length);
        }

     /* 
      * Now, we released all the endpoints occupied 
      * by the functiond driver 
      * Update the configuration interface num
      */

     semTake (pConfig->ConfigMutex,USBTGT_WAIT_TIMEOUT);

     pConfig->uIfNumUsed = (UINT8) (pConfig->uIfNumUsed - pFuncDriver->uIfNumUsed);
     
     pConfig->ConfigDesc.numInterfaces = (UINT8)(pConfig->ConfigDesc.numInterfaces -
                                                 pFuncDriver->uIfNumUsed);
     
     /* NOTE:: flush all the other function drivers of this configuration */

     (void)semGive (pConfig->ConfigMutex);

     /* Take the ownership of the function driver */
     
     semTake (pFuncDriver->funcMutex,USBTGT_WAIT_TIMEOUT);

     /* Free the descriptors buffer */

     if (NULL != pFuncDriver->pFsDescBuf)
        {
        OS_FREE (pFuncDriver->pFsDescBuf);
        pFuncDriver->pFsDescBuf = NULL;
        }

     if (NULL != pFuncDriver->pHsDescBuf)
        {
        OS_FREE (pFuncDriver->pHsDescBuf);
        pFuncDriver->pHsDescBuf = NULL;
        }

     if (NULL != pFuncDriver->pSsDescBuf)
        {
        OS_FREE (pFuncDriver->pSsDescBuf);
        pFuncDriver->pSsDescBuf = NULL;
        }
     
     /* Reset the interface num */
     
     pFuncDriver->uIfNumUsed = 0;
     pFuncDriver->uIfNumMin = 0;
     pFuncDriver->uIfNumMax = 0;

     pFuncDriver->pConfig = NULL;
     
     pFuncDriver->pTcd = NULL;
     
     /* Release the ownership of the function driver */

     (void)usbTgtFuncTargChannelDestroy(pFuncDriver->targChannel);
     
     (void)semGive (pFuncDriver->funcMutex);
      
    /* Detached, remove the function driver from the Tcd's func list */
    
    semTake (pTcd->tcdMutex, USBTGT_WAIT_TIMEOUT);
     
    /* 
     * String descriptors update 
     * In this routine we only simplily remove the string descriptors
     */
    
    (void)usbTgtFuncStringDescUnLink(pTcd, pFuncDriver);
    
    (void)semGive (pTcd->tcdMutex);

    USB_TGT_VDBG("usbTgtFuncDetach done\n ",
                1, 2, 3, 4, 5, 6); 
     
     return OK;
    }

/*******************************************************************************
*
* usbTgtFuncEpDescUpdate - update endpoint descriptors 
*
* This routine is used to update the endpoint descriptors according to the 
* speed.
* 
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbTgtFuncEpDescUpdate
    (
    UINT8 *               pDescBuf,
    UINT16                uDescBufLen,
    UINT8                 uSpeed
    )
    {
    pUSB_DESCR_HDR        pDescHdr;
    pUSB_ENDPOINT_DESCR   pEpDesc = NULL;
    UINT16                uMaxPktSize;
    
    /* Validate parameters */
    
    if (NULL == pDescBuf)
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ("pDescHeader"), 2, 3, 4, 5, 6);
                     
        return;
        }

    pDescHdr = (pUSB_DESCR_HDR)pDescBuf;
    
    switch (uSpeed)
        {
        case USB_SPEED_FULL:                        
            {            
            uMaxPktSize = USB_MAX_FULL_SPEED_BULK_SIZE;
            }
            break;        
        case USB_SPEED_HIGH:
            {            
            uMaxPktSize = USB_MAX_HIGH_SPEED_BULK_SIZE;
            }
            break;        
        case USB_SPEED_SUPER:
            /* Fall through */
        default:
            {            
            uMaxPktSize = USB_MAX_SUPER_SPEED_BULK_SIZE;
            }
            break;        
        }

    /* Update the BULK endpoint max packet size according the USB 3.0 Spec */
    
    while ((uDescBufLen > 0 ) &&
           (NULL != pDescHdr))
        {
        if (USB_DESCR_ENDPOINT == pDescHdr->descriptorType)
            {
            pEpDesc = (pUSB_ENDPOINT_DESCR)pDescHdr;

            if (USB_ATTR_BULK == pEpDesc->attributes)
                {
                pEpDesc->maxPacketSize = (UINT16) (pEpDesc->maxPacketSize & (~
                       (FROM_LITTLEW (USB_MAX_PACKET_SIZE_MASK))));
                
                pEpDesc->maxPacketSize = (UINT16) (pEpDesc->maxPacketSize | FROM_LITTLEW(uMaxPktSize));
                }
            }
        
        /* Move on for next step */

        uDescBufLen = (UINT16) (uDescBufLen - pDescHdr->length);
        pDescHdr = (pUSB_DESCR_HDR)((UINT8 *)pDescHdr + pDescHdr->length);
        }

    return;
    }


/*******************************************************************************
*
* usbTgtFuncDescUpdate - update function descriptors 
*
* This routine is used to update all the descriptors of the  function 
* driver according to the TCD speed and the hardware capability.
* 
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbTgtFuncDescUpdate
    (
    pUSBTGT_FUNC_DRIVER   pFuncDriver
    )
    {
    pUSBTGT_TCD           pTcd = NULL;
    
    /* Validate parameters */
    
    if ((NULL == pFuncDriver) ||
        (NULL == (pTcd = pFuncDriver->pTcd)))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pFuncDriver) ? "pFuncDriver" :
                     "pTcd"), 2, 3, 4, 5, 6);
                     
        return;
        }

    /* 
     * This routine only used to update the BULK endpoint descriptor 
     * TODO: Update other descriptors belong to this function driver.
     */
     
    USB_TGT_DBG ("usbTgtFuncDescUpdate pTcd->uSpeed %x\n",
                 pTcd->uSpeed, 2,3,4,5,6);

    switch (pTcd->uSpeed)
        {
        case USB_SPEED_SUPER:
            {
            /* Only update the pSsDescBuf */
            
            usbTgtFuncEpDescUpdate(pFuncDriver->pSsDescBuf,
                                   pFuncDriver->uSsDescBufLen,
                                   USB_SPEED_SUPER);
            
            }
            break;        
        case USB_SPEED_HIGH:
            {            
            usbTgtFuncEpDescUpdate(pFuncDriver->pHsDescBuf,
                                   pFuncDriver->uHsDescBufLen,
                                   USB_SPEED_HIGH);
            
            usbTgtFuncEpDescUpdate(pFuncDriver->pFsDescBuf,
                                   pFuncDriver->uFsDescBufLen,
                                   USB_SPEED_FULL);
            }
            break;
        case USB_SPEED_FULL:
            /* Fall through */
        case USB_SPEED_LOW:
            {
            
            usbTgtFuncEpDescUpdate(pFuncDriver->pHsDescBuf,
                                   pFuncDriver->uHsDescBufLen,
                                   USB_SPEED_HIGH);
            
            usbTgtFuncEpDescUpdate(pFuncDriver->pFsDescBuf,
                                   pFuncDriver->uFsDescBufLen,
                                   USB_SPEED_FULL);
            }
            break;
        default:
            {
            usbTgtFuncEpDescUpdate(pFuncDriver->pFsDescBuf,
                                   pFuncDriver->uFsDescBufLen,
                                   USB_SPEED_FULL);
            
            usbTgtFuncEpDescUpdate(pFuncDriver->pHsDescBuf,
                                   pFuncDriver->uHsDescBufLen,
                                   USB_SPEED_HIGH);
            
            usbTgtFuncEpDescUpdate(pFuncDriver->pSsDescBuf,
                                   pFuncDriver->uSsDescBufLen,
                                   USB_SPEED_SUPER);
            }
            break;        
        }

    return;
    }

/*******************************************************************************
*
* usbTgtFuncAttach - attach the functiond driver to the configuration
*
* This routine attachs the functiond driver to the configuration of the target.
* 
* NOTE: Before this routine is called, the caller should make sure the function
* resource has been initialized. Such as, the descriptors table (include all the
* endpoints' descriptors) has been linked in the function driver.
*
* The routine will check if the pTcd's Endpoints can satisfy the function's 
* endpoints reqirements. If one exist configuration has no much endpoints, it 
* still try to check another new configuration can satisfy the function or not.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbTgtFuncAttach
    (
    pUSBTGT_CONFIG        pConfig,
    pUSBTGT_FUNC_DRIVER   pFuncDriver
    )
    {
    pUSBTGT_TCD           pTcd = NULL;
    pUSB_DESCR_HDR *      ppDescTable = NULL;
    pUSB_DESCR_HDR        pDescHeader = NULL;
    pUSBTGT_DEVICE_INFO   pDevInfo = NULL;
    pUSB_INTERFACE_DESCR  pIfDesc = NULL;
    pUSB_ENDPOINT_DESCR   pEpDesc = NULL;
    pUSB_IAD_DESCR        pIadDesc = NULL;
    USB_CDC_UNION_DESCR * pUnionDesc = NULL;
    UINT8 *               pDescBuff = NULL;
    UINT8 *               pBufTemp = NULL;
    UINT16                uDescBuffLen = 0;
    UINT8                 uIfNumUsed = 0;
    BOOL                  bHasIad = FALSE;
    UINT8                 uLangIdCount;
    UINT8                 uIndex;
    UINT16                uLangId;
    STATUS                status;
    UINT8                 i;
    
    /* Validate parameters */
    
    if ((NULL == pConfig) ||
        (NULL == pConfig->pTcd) ||
        (NULL == pFuncDriver)||
        ((NULL == pFuncDriver->ppHsFuncDescTable) &&
         (NULL == pFuncDriver->ppFsFuncDescTable) &&
         (NULL == pFuncDriver->ppSsFuncDescTable)))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    ((NULL == pConfig) ? "pConfig" :
                     (NULL == pConfig->pTcd) ? "pConfig->pTcd" :
                     (NULL == pFuncDriver) ? "pFuncDriver" :
                     "All FuncDescTable"), 2, 3, 4, 5, 6);
                     
        return ERROR;
        }

    /* Find the configuration which need be attached to */

    pDevInfo = & pConfig->DeviceInfo;
    
    pTcd = pConfig->pTcd;

    /* Disconnet first */
    
    (void)usbTgtTcdSoftConnect(pTcd, FALSE);

    semTake (pFuncDriver->funcMutex, USBTGT_WAIT_TIMEOUT);

    /* If we have ppFsFuncDescTable, create the pDescBuf */
    
    if (NULL != pFuncDriver->ppSsFuncDescTable)
        {
        USB_TGT_DBG("ppSsDescTable 0x%X\n",
                    (ULONG)pFuncDriver->ppSsFuncDescTable,
                     2, 3, 4, 5, 6);
        
        uDescBuffLen = usbTgtFuncDescLenGet (pFuncDriver->ppSsFuncDescTable);

        /* Create the data buffer for the descriptor */
        
        pFuncDriver->pSsDescBuf = (UINT8 *)OSS_CALLOC(uDescBuffLen);

        if (NULL == pFuncDriver->pSsDescBuf)
            {
            USB_TGT_ERR("No enough memory to store descriptors\n",
                        1, 2, 3, 4, 5, 6);
            
            (void)semGive (pFuncDriver->funcMutex);
            
           if (ERROR == usbTgtFuncDetach(pConfig, pFuncDriver))
               {
               USB_TGT_WARN("Detach the function driver %s fail\n ",
                           (NULL != pFuncDriver->pFuncName) ? 
                           "pFuncDriver->pFuncName" : NULL, 2, 3, 4, 5, 6); 
               }
                
            return ERROR;
        }
        
        /* Update the function driver descriptor buffer length */
    
        pFuncDriver->uSsDescBufLen = uDescBuffLen;

        ppDescTable = pFuncDriver->ppSsFuncDescTable;

        /* Copy the existed descriptors */

        pDescHeader = * ppDescTable;
        
        pDescBuff = pFuncDriver->pSsDescBuf;
        }

    /* If we have ppHsFuncDescTable, create the pDescBuf */
    
    else if (NULL != pFuncDriver->ppHsFuncDescTable)
        {
        USB_TGT_DBG("ppHsDescTable 0x%X\n",
                    (ULONG)pFuncDriver->ppHsFuncDescTable,
                     2, 3, 4, 5, 6);
        
        uDescBuffLen = usbTgtFuncDescLenGet (pFuncDriver->ppHsFuncDescTable);

        /* Create the data buffer for the descriptor */
        
        pFuncDriver->pHsDescBuf = (UINT8 *)OSS_CALLOC(uDescBuffLen);

        if (NULL == pFuncDriver->pHsDescBuf)
            {
            USB_TGT_ERR("No enough memory to store HS descriptors\n",
                        1, 2, 3, 4, 5, 6);
            
            (void)semGive (pFuncDriver->funcMutex);
            
           if (ERROR == usbTgtFuncDetach(pConfig, pFuncDriver))
               {
               USB_TGT_WARN("Detach the function driver %s fail\n ",
                           (NULL != pFuncDriver->pFuncName) ? 
                           "pFuncDriver->pFuncName" : NULL, 2, 3, 4, 5, 6); 
               }
                
            return ERROR;
        }
        
        /* Update the function driver descriptor buffer length */
    
        pFuncDriver->uHsDescBufLen = uDescBuffLen;

        ppDescTable = pFuncDriver->ppHsFuncDescTable;

        /* Copy the existed descriptors */

        pDescHeader = * ppDescTable;
        
        pDescBuff = pFuncDriver->pHsDescBuf;
        }


    /* If we have ppFsFuncDescTable, create the pDescBuf */
    
    else
        {
        USB_TGT_DBG("ppFsDescTable 0x%X\n",
                    (ULONG)pFuncDriver->ppFsFuncDescTable,
                     2, 3, 4, 5, 6);
        
        uDescBuffLen = usbTgtFuncDescLenGet (pFuncDriver->ppFsFuncDescTable);

        /* Create the data buffer for the descriptor */
        
        pFuncDriver->pFsDescBuf = (UINT8 *)OSS_CALLOC(uDescBuffLen);

        if (NULL == pFuncDriver->pFsDescBuf)
            {
            USB_TGT_ERR("No enough memory to store FS descriptors\n",
                        1, 2, 3, 4, 5, 6);
            
            (void)semGive (pFuncDriver->funcMutex);
            
           if (ERROR == usbTgtFuncDetach(pConfig, pFuncDriver))
               {
               USB_TGT_WARN("Detach the function driver %s fail\n ",
                           (NULL != pFuncDriver->pFuncName) ? 
                           "pFuncDriver->pFuncName" : NULL, 2, 3, 4, 5, 6); 
               }
                
            return ERROR;
        }
        
        /* Update the function driver descriptor buffer length */
    
        pFuncDriver->uFsDescBufLen = uDescBuffLen;

        ppDescTable = pFuncDriver->ppFsFuncDescTable;

        /* Copy the existed descriptors */

        pDescHeader = * ppDescTable;
        
        pDescBuff = pFuncDriver->pFsDescBuf;
        }
    
    /* Copy all the descriptors to the pDescBuff */
  
    while (NULL != pDescHeader)
        {
        switch (pDescHeader->descriptorType)
            {
            case USB_DESCR_DEVICE:
                {
                /* 
                 * NOTE: if the function has device descriptors and 
                 * USBTGT_USE_FUNC_DESC defined
                 * it will overwrite the original device descriptor which
                 * created by the usb targetM.
                 * Normally, do not add the device descriptors
                 *
                 * But, if the user insist to use one specific function driver's
                 * device descriptor. Please set the following paramenters:
                 * 
                 * USBTGT_USE_FUNC_DEVICE_DESC_ENABLE   TRUE
                 * USBTGT_USE_DEVICE_DESC_OF_FUNC_NAME  "The Function Driver Name"
                 * USBTGT_USE_DEVICE_DESC_OF_FUNC_UNIT  The function Driver Unit
                 * 
                 * The TML will use the function driver's device descriptor
                 * as the global device descriptor.
                 * 
                 * But, some parameters still will be rewrite for the device
                 * speed changes, and the global parameter's setting such as
                 * the VID/PID. Please refer to usbTgtTcdActivate() routine.
                 */

                if (TRUE == usrUsbTgtDevDescFuncEnableGet())
                    {
                    if ((0 == strcmp((const char *)usrUsbTgtDevDescOfFuncNameGet(), 
                                      pFuncDriver->pFuncName)) &&
                         (usrUsbTgtDevDescOfFuncUnitGet() == pFuncDriver->uFuncUnit))
                        {                        
                        memcpy ((void *)&pTcd->DeviceDesc, 
                                (void * )pDescHeader, 
                                pDescHeader->length);
                        }
                    }
                }
                break;
            case USB_DESCR_CONFIGURATION:
                
                /* 
                 * NOTE: if the function has device descriptors, 
                 * USBTGT_USE_FUNC_DESC defined
                 * it will overwrite the original configuration descriptor 
                 * which created by the usb TML
                 * Normally, do not add the configuration descriptors
                 */   

                 break;
            case USB_DESCR_INTERFACE:
                {
                
                /* Copy the descriptor to the pDescBuff */
                
                memcpy (pDescBuff, (void *)pDescHeader, pDescHeader->length);

                pIfDesc = (pUSB_INTERFACE_DESCR)pDescBuff;

                USB_TGT_DBG("If Descriptor  0x%04X 0x%04X 0x%02X 0x%04X 0x%02X 0x%02X\n", 
                            pIfDesc->length << 8 | pIfDesc->descriptorType,
                            pIfDesc->interfaceNumber << 8 | pIfDesc->alternateSetting,
                            pIfDesc->numEndpoints,
                            pIfDesc->interfaceClass << 8 | pIfDesc->interfaceSubClass,
                            pIfDesc->interfaceProtocol,
                            pIfDesc->interfaceIndex);

                /* Update the interface num in the configuration */
                
                if (pIfDesc->alternateSetting == 0)
                    {
                    uIfNumUsed ++;
                    }

                /* Update the IAD descriptor */

                if (TRUE == usrUsbTgtIadDescEnableGet())
                    {
                    pIadDesc = (pUSB_IAD_DESCR)&pFuncDriver->uIADDesc[0];

                    if ((pIadDesc->bFunctionClass == 0) &&
                        (pIadDesc->bFunctionSubClass == 0) &&
                        (pIadDesc->bFunctionProtocol == 0))
                       {
                       pIadDesc->bFunctionClass = pIfDesc->interfaceClass;       
                       pIadDesc->bFunctionSubClass = pIfDesc->interfaceSubClass;  
                       pIadDesc->bFunctionProtocol = pIfDesc->interfaceProtocol;  
                       }
                   }
                pIfDesc->interfaceNumber = (UINT8) (pIfDesc->interfaceNumber + pConfig->uIfNumUsed);

                /* Need update the string descriptors in the global list */

                /* Get string descriptor for the interface */

                if ((pIfDesc->interfaceIndex) &&
                    (NULL != pTcd->pUsbTgtLangDescr))
                    {
                    /* The language ID is UINT16 */
                    
                    uLangIdCount = (UINT8) ((pTcd->pUsbTgtLangDescr->length >> 1) - 1);

                    for (uIndex = 0; uIndex < uLangIdCount ; uIndex ++)
                        {
                        uLangId = pTcd->pUsbTgtLangDescr->langId[uIndex];

                        /* Do not check the return value */
                        
                        (void)usbTgtFuncStringDescLink(pTcd,
                                                 pFuncDriver,
                                                 uLangId,
                                                 pIfDesc);
                        }
                    }
                
                /* Update next step to copy the data */
                
                pDescBuff = pDescBuff + pDescHeader->length;
                }
                break;
            case USB_DESCR_ENDPOINT:
                {
                /* Copy the descriptor to the pDescBuff */
                
                memcpy (pDescBuff, (void * )pDescHeader, pDescHeader->length);

                pEpDesc = (pUSB_ENDPOINT_DESCR)pDescBuff;
              
                USB_TGT_DBG("Ep Descriptor  0x%02X 0x%02X 0x%02X 0x%02X 0x%04X 0x%02X\n",
                            pEpDesc->length,
                            pEpDesc->descriptorType,
                            pEpDesc->endpointAddress,
                            pEpDesc->attributes,
                            pEpDesc->maxPacketSize,
                            pEpDesc->interval);

                /* 
                 * Update the endpoint num in the configuration 
                 * Keep the endpoint dir, and update the endpoint index
                 *
                 * Note, normally, we should check the return of the 
                 * usbTgtNextEpIndexGet, 
                 */

                pEpDesc->endpointAddress = (UINT8) ((pEpDesc->endpointAddress & 
                                    USB_ENDPOINT_IN) |
                                    usbTgtEpIndexReqest(pDevInfo, pEpDesc, TRUE));

                if (pEpDesc->endpointAddress & USBTGT_MAX_EP_NUM)
                    {
                    (void)semGive (pFuncDriver->funcMutex);
                    
                    if (ERROR == usbTgtFuncDetach(pConfig, pFuncDriver))
                       {
                       USB_TGT_WARN("Detach the function driver %s fail\n ",
                                   (NULL != pFuncDriver->pFuncName) ? 
                                   "pFuncDriver->pFuncName" : NULL, 2, 3, 4, 5, 6); 
                       }
                        
                    return ERROR;
                    }

                /* Update next step to copy the data */
                
                pDescBuff = pDescBuff + pDescHeader->length;
                }                
                break;
            case USB_DESCR_CS_INTERFACE:
                {
                /* Copy the descriptor to the pDescBuff */
                
                memcpy (pDescBuff, (void * )pDescHeader, pDescHeader->length);

                /* 
                 * For many class driver such as Audio,
                 * HID, CDC, also have union descriptors 
                 */
                
                if (pDescHeader->length >= sizeof(USB_CDC_UNION_DESCR))
                    {
                    pUnionDesc = (USB_CDC_UNION_DESCR *)pDescBuff;
                    if (pUnionDesc->bDescriptorSubType == USB_CDC_CS_UNION_TYPE)
                        {
                        /*
                         * NOTE: There may be more than one slave interface
                         * in some function driver.
                         * pUnionDesc->bMasterInterface0 += uCurrentIfNum;
                         * pUnionDesc->bSlaveInterface0 += uCurrentIfNum;
                         */
                         
                        pBufTemp = pDescBuff;
                        
                        for (i = 3; i < pUnionDesc->bLength; i ++)
                            {
                            *(pBufTemp + i) = (UINT8) (*(pBufTemp + i) + pConfig->uIfNumUsed);
                            }
                        }
                    }
                
                /* Update next step to copy the data */
                
                pDescBuff = pDescBuff + pDescHeader->length;
                }
                break;
            case USB_DESCR_IAD:
                {
                /* Copy the descriptor to the pDescBuff */
                
                memcpy (pDescBuff, (void * )pDescHeader, pDescHeader->length);
                
                pIadDesc = (USB_IAD_DESCR *)pDescBuff;

                pIadDesc->bFirstInterface = pConfig->uIfNumUsed;

                bHasIad = TRUE;
             
                /* Update next step to copy the data */
                
                pDescBuff = pDescBuff + pDescHeader->length;
                
                pConfig->bIadNeeded = TRUE;
                
                USB_TGT_DBG("USB_DESCR_IAD Descriptor show up %p\n",
                            bHasIad, 2, 3, 4, 5, 6);                
                }
                break;                
            default:
                {
                /* Copy the descriptor to the pDescBuff */
                
                memcpy (pDescBuff, (void * )pDescHeader, pDescHeader->length);
                
                /* Update next step to copy the data */
                
                pDescBuff = pDescBuff + pDescHeader->length;
                }
                break;
            }

        ppDescTable++;
        pDescHeader = * ppDescTable;
        }

     /*
      * NOTE: To support multiple speed, the function driver normally has
      * the high speed endpoint descriptors which have bigger max packet size.
      * The target management uses two descriptor buffers (<'pDescBuf'> and
      * <'pHsDescBuf'>) to store those descriptors. So when the hardware switch
      * the speed, the target management will switch the descriptor buffer.
      *
      * The target management suspects that, <ppHsFuncDescTable> has no 
      * difference with <ppFsFuncDescTable> expect the endpoint max pecket size.
      */

    /* Create the descriptor for high speed */

    /* If we have ppHsFuncDescTable, create the pHsDescBuf */

    if ((NULL != pFuncDriver->ppSsFuncDescTable) && 
        (NULL != pFuncDriver->ppHsFuncDescTable) && 
        (NULL == pFuncDriver->pHsDescBuf))
        {
        USB_TGT_DBG("ppHsFuncDescTable 0x%X\n",
                    (ULONG)pFuncDriver->ppHsFuncDescTable,
                     2, 3, 4, 5, 6);
        
        uDescBuffLen = usbTgtFuncDescLenGet (pFuncDriver->ppHsFuncDescTable);

        /* Create the data buffer for the descriptor */
        
        pFuncDriver->pHsDescBuf = (UINT8 *)OSS_CALLOC(uDescBuffLen);

        if (NULL == pFuncDriver->pHsDescBuf)
            {
            USB_TGT_ERR("No enough memory to store descriptors\n",
                        1, 2, 3, 4, 5, 6);

            (void)semGive (pFuncDriver->funcMutex);
            
           if (ERROR == usbTgtFuncDetach(pConfig, pFuncDriver))
               {
               USB_TGT_WARN("Detach the function driver %s fail\n ",
                           (NULL != pFuncDriver->pFuncName) ? 
                           "pFuncDriver->pFuncName" : NULL, 2, 3, 4, 5, 6); 
               }
           
            return ERROR;
            }     

        if (NULL != pFuncDriver->pHsDescBuf)
            {

            /* Copy the existed descriptors */

            pDescHeader = (pUSB_DESCR_HDR) pFuncDriver->pSsDescBuf;
            pDescBuff = pFuncDriver->pHsDescBuf;
            uDescBuffLen = pFuncDriver->uSsDescBufLen;
            pFuncDriver->uHsDescBufLen = 0;

            while ((uDescBuffLen > 0) &&
                   (NULL != pDescHeader))
                {
                if (pDescHeader->descriptorType != USB_DESCR_SS_ENDPOINT_COMPANION)
                    {        

                    memcpy (pDescBuff, pDescHeader, pDescHeader->length);
                    pFuncDriver->uHsDescBufLen =(UINT16) (pFuncDriver->uHsDescBufLen + pDescHeader->length);
                    pDescBuff += pDescHeader->length;
                    }
                
                uDescBuffLen = (UINT16)(uDescBuffLen - pDescHeader->length);
                pDescHeader = (pUSB_DESCR_HDR) ((UINT8 *)pDescHeader +
                                                 pDescHeader->length);
                }
            }

        
        }

   
    /* Create the descriptor for full speed */

    if ((NULL != pFuncDriver->ppSsFuncDescTable) && 
        (NULL != pFuncDriver->ppFsFuncDescTable) && 
        (NULL == pFuncDriver->pFsDescBuf))
        {
        uDescBuffLen = usbTgtFuncDescLenGet (pFuncDriver->ppFsFuncDescTable);

        /* Create the data buffer for the descriptor */
        
        pFuncDriver->pFsDescBuf = (UINT8 *)OSS_CALLOC(uDescBuffLen);

        if (NULL != pFuncDriver->pFsDescBuf)
            {
            /* Copy the existed descriptors */

            pDescHeader = (pUSB_DESCR_HDR) pFuncDriver->pSsDescBuf;
            pDescBuff = pFuncDriver->pFsDescBuf;
            uDescBuffLen = pFuncDriver->uSsDescBufLen;
            pFuncDriver->uFsDescBufLen = 0;

            while ((uDescBuffLen > 0) &&
                   (NULL != pDescHeader))
                {
                if (pDescHeader->descriptorType != USB_DESCR_SS_ENDPOINT_COMPANION)
                    {        
                    memcpy (pDescBuff, pDescHeader, pDescHeader->length);
                    pFuncDriver->uFsDescBufLen = (UINT16)(pFuncDriver->uFsDescBufLen + pDescHeader->length);
                    pDescBuff += pDescHeader->length;
                    }
                
                uDescBuffLen = (UINT16) (uDescBuffLen - pDescHeader->length);
                pDescHeader = (pUSB_DESCR_HDR) ((UINT8 *)pDescHeader +
                                                 pDescHeader->length);
                }
                
            }
        }


    pFuncDriver->pConfig = pConfig;
    
    pFuncDriver->pTcd = pConfig->pTcd;
    
    pFuncDriver->uIfNumUsed = uIfNumUsed;
    pFuncDriver->uIfNumMin = pConfig->uIfNumUsed;
    pFuncDriver->uIfNumMax = (UINT8) (pConfig->uIfNumUsed + uIfNumUsed);
    
    /* 
     * Update the IAD descriptor 
     * When there are more than one interface in one function
     * IAD will be needed.
     * Update the flag in the configuration
     */

    if ((TRUE == usrUsbTgtIadDescEnableGet()) &&
        (pFuncDriver->uIfNumUsed > 1) &&
        (FALSE == bHasIad))
        {
        pIadDesc = (pUSB_IAD_DESCR)&pFuncDriver->uIADDesc[0];
        pIadDesc->length = USB_IAD_DESCR_LEN;               /* bLength */
        pIadDesc->descriptorType = USB_DESCR_IAD;           /* bDescriptorType */
        pIadDesc->bFirstInterface = pFuncDriver->uIfNumMin; /* bFirstInterface  */
        pIadDesc->bInterfaceCount = pFuncDriver->uIfNumUsed;/* bInterfaceCount */

        /*
         * Do not support the string descriptor for the function driver
         */
         
        pIadDesc->iFunction = 0;                            /* iFunction */

        /* Update the configuration descriptor lenght */
        
        pConfig->bIadNeeded = TRUE;

        }

    /* Create the targ channel */
    
    pFuncDriver->targChannel = usbTgtFuncTargChannelCreate(pFuncDriver);
        
    (void)semGive (pFuncDriver->funcMutex);

    semTake (pConfig->ConfigMutex,USBTGT_WAIT_TIMEOUT);

    /* Update the configuration interface num */

    pConfig->uIfNumUsed = (UINT8) (pConfig->uIfNumUsed + uIfNumUsed);
     
    /* Update the configure descriptor interface number */
     
    pConfig->ConfigDesc.numInterfaces = (UINT8)(pConfig->ConfigDesc.numInterfaces+ uIfNumUsed);

    /* Update the configuration total length */


    (void)semGive (pConfig->ConfigMutex);

    USB_TGT_DBG("usbTgtFuncAttach(): Configuration 0x%X with"
                " Index %d, value %d numInterfaces %d totalLength 0x%X\n",
                pConfig,
                pConfig->ConfigDesc.configurationIndex,
                pConfig->ConfigDesc.configurationValue,
                pConfig->ConfigDesc.numInterfaces,
                TO_LITTLEW(pConfig->ConfigDesc.totalLength), 6);

    /* Call the function driver attach call back */

    status = usbTgtFuncMngmtCallback(pFuncDriver,
               TARG_MNGMT_ATTACH,/* USBTGT_NOTIFY_ATTACH_EVENT,*/
               (void *)&pConfig->DeviceInfo);

    if (ERROR == status)
       {
       USB_TGT_WARN("Notify the function to 0x%X attach error\n ",
                   (ULONG)pFuncDriver, 2, 3, 4, 5, 6); 

       if (ERROR == usbTgtFuncDetach(pConfig, pFuncDriver))
           {
           USB_TGT_WARN("Detach the function driver %s fail\n ",
                       (NULL != pFuncDriver->pFuncName) ? 
                       "pFuncDriver->pFuncName" : NULL, 2, 3, 4, 5, 6); 
           }
       
       return ERROR; 
       }
    
    USB_TGT_DBG("usbTgtFuncAttach successfully\n",
                1, 2, 3, 4, 5, 6);

    /* Attached, add the function driver to the Tcd's func list */
    
    semTake (pTcd->tcdMutex, USBTGT_WAIT_TIMEOUT);
    lstAdd (&pTcd->funcList, &pFuncDriver->funcInTcdListNode);
    (void)semGive (pTcd->tcdMutex);
    
    return OK;
    }

/*******************************************************************************
*
* usbTgtFuncRemove - remove the function driver from the target stack.
*
* This routine removes the function driver from the target stack. It also will
* detach itslef from the tcd if they have been attached together.
* 
* NOTE:
* This routine will not destroy the <'pUSBTGT_FUNC_DRIVER'> data structure.
* It also will not delelte the <'pFuncDriver'> from the global function driver
* list. It is used to remove the function driver from the TML, and detach it
* from the TCD which attached to it. Meanwhile, it will reset the 
* <'pFuncSpecific'> item to NULL.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*/

STATUS usbTgtFuncRemove
    (
    pUSBTGT_FUNC_DRIVER pFuncDriver
    )
    {
    pUSBTGT_TCD    pTcd = NULL;
    pUSBTGT_CONFIG pConfig = NULL;

    if ((NULL == pFuncDriver) ||
        (FALSE == IS_FUNC_DRIVER_MAGICID_VALID(pFuncDriver->uFuncMagicID)))
        {
        USB_TGT_ERR("Invalid parameter %s \n",
                    ((NULL == pFuncDriver) ? "pFuncDriver is NULL" :
                     "pFuncDriver->uFuncMagicID is invald"), 2, 3, 4, 5, 6);
                     
        return ERROR;
        }

    pTcd = pFuncDriver->pTcd;

    if (NULL != pTcd)
        {        
        /* Detach the function driver from the configuration */

        pConfig = (pUSBTGT_CONFIG)lstNth(&pTcd->configList, 
                                         pFuncDriver->uConfigToBind);

        if (NULL == pConfig)
            {
            USB_TGT_ERR("No find the required configuration, should not happen\n",
                        1, 2, 3, 4, 5, 6);
           
            return ERROR;
            }

        /* Must disconnect from host before remove any function driver */

        (void)usbTgtTcdSoftConnect(pTcd, FALSE);

        if (ERROR == usbTgtFuncDetach (pConfig, pFuncDriver))
            {
            USB_TGT_ERR("Detach the function fail\n",
                        1, 2, 3, 4, 5, 6);
           
            return ERROR;
            }
        
        if (ERROR != lstFind (&pTcd->funcList, &pFuncDriver->funcInTcdListNode))
            lstDelete (&pTcd->funcList, &pFuncDriver->funcInTcdListNode);
        }
    
    /* Update the maggic ID */
    
    semTake (pFuncDriver->funcMutex, USBTGT_WAIT_TIMEOUT);
    pFuncDriver->uFuncMagicID = USBTGT_MAGIC_ID_ADD_EN;
    (void)semGive (pFuncDriver->funcMutex);

    /* 
     * Updata device information and do soft connect, 
     * in case there is any other function 
     * driver attached on the TCD. 
     */
    
    if (NULL != pTcd)
        {
        usbTgtTcdInfoUpdate(pTcd);
        /* Reset the default pipe */
        
        if (ERROR == usbTgtDefaultPipeReset(pTcd))
            {
            USB_TGT_ERR("Reset the default pipe fail\n",
                        1, 2, 3, 4, 5, 6);
           
            return ERROR;
            }

        /* Do soft connect ? */
        
        if (lstCount(&pTcd->funcList))
            {
            if (ERROR == usbTgtTcdSoftConnect(pTcd, TRUE))
                {
                USB_TGT_ERR("Do soft connect fail\n",
                            1, 2, 3, 4, 5, 6);
               
                return ERROR;
                }
            }
        }

    return OK;
    }


/*******************************************************************************
*
* usbTgtFuncAdd - add the function driver to the target stack
*
* This routine adds the function driver to the target stack. It also will check
* and attach to the specific tcd if it exists in the target stack. 
*
* This routine will first check the pTcd exist or not according to the pTcdName
* and uTcdUnit. It also will check if the resource of the tcd can satisfy 
* the function driver's requirement or not (Such as if there are enough endpoints
* for the function driver to use.)
*
* NOTE: Before this routine is called, the caller should make sure the function
* resource has been initialized. That is the function driver knows which tcd 
* will be attached to.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*/

STATUS usbTgtFuncAdd
    (
    pUSBTGT_FUNC_DRIVER pFuncDriver
    )
    {
    pUSBTGT_TCD    pTcd = NULL;
    pUSBTGT_CONFIG pConfig = NULL;
    int            listCount = 0;
    int            configIndex;

    /* Need check all the items list above */
    
    if ((NULL == pFuncDriver) ||
        (USBTGT_MAGIC_ID_ADD_EN != pFuncDriver->uFuncMagicID) ||
        (NULL == pFuncDriver->pTcdName))
        {
        USB_TGT_ERR("Invalid parameter %s\n",
                    ((NULL == pFuncDriver) ? "pFuncDriver is NULL" :
                     (USBTGT_MAGIC_ID_ADD_EN != pFuncDriver->uFuncMagicID) ?
                     "pFuncDriver->uFuncMagicID is not USBTGT_MAGIC_ID_ADD_EN" :
                     "pFuncDriver->pTcdName is NULL"), 2, 3, 4, 5, 6);
                     
        return ERROR;
        }

    /* 
     * The reqiured TCD has not been added to TML 
     * set the uFuncMagicID to valid, meens the function driver
     * has been added to the TML, no mater attached to the TCD or not
     */
    
    semTake(pFuncDriver->funcMutex, USBTGT_WAIT_TIMEOUT);        
    pFuncDriver->uFuncMagicID = usbTgtMaggicIDRequire(USBTGT_FUNC_SIG);
    (void)semGive (pFuncDriver->funcMutex);
    
    /* 
     * One new function driver need be attached to one TCD 
     * Check if the Tcd existed in the global list
     */

    pTcd = usbTgtFindTcdByName (pFuncDriver->pTcdName, pFuncDriver->uTcdUnit);

    if ((NULL == pTcd) ||
        (FALSE == IS_TCD_MAGICID_VALID(pTcd->uTCDMagicID)))
        {
        USB_TGT_WARN("The TCD %s need be attached not exit\n",
                     pFuncDriver->pTcdName,
                     2, 3, 4, 5, 6);

        return OK;
        }
    
    USB_TGT_DBG("usbTgtFuncAdd():Find The TCD %s%d to be attached\n",
                 pFuncDriver->pTcdName,
                 pFuncDriver->uTcdUnit, 3, 4, 5, 6);

    listCount = lstCount(&pTcd->configList);
    
    if ((pFuncDriver->uConfigToBind <= USBTGT_TCD_MAX_CONFIG_COUNT) &&
        (pFuncDriver->uConfigToBind > listCount))
        {
        for (configIndex = listCount; 
             configIndex < pFuncDriver->uConfigToBind; 
             configIndex ++)
            {
            pConfig = usbTgtConfigCreate();
            if (NULL == pConfig)
                {
                USB_TGT_ERR("Create config fail \n",
                            configIndex, 2, 3, 4, 5 ,6);

                /* Even fail, we do nothing here*/
                
                return ERROR;
                }
            
            /* Copy the Device Info to all the configurations */

            memcpy(&pConfig->DeviceInfo, &pTcd->DeviceInfo,
                   sizeof(USBTGT_DEVICE_INFO));

            pConfig->pTcd = pTcd;
            pConfig->uConfigIndex = (UINT8)configIndex;
            lstAdd (&pTcd->configList,&pConfig->configNode);
            }        
        }
        
    /* Find the configuration which the function driver to be attached */

    pConfig = (pUSBTGT_CONFIG)lstNth(&pTcd->configList, 
                                     pFuncDriver->uConfigToBind);

    if (NULL == pConfig)
        {
        USB_TGT_ERR("No find the required configuration,Please check the config\n",
                    1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    USB_TGT_DBG("usbTgtFuncAdd():The TCD satisfy the function driver's requirement\n",
                 1, 2, 3, 4, 5, 6);

    /* Attach the function driver to the reqired configuration of the TCD */
    
    if (OK != usbTgtFuncAttach (pConfig, pFuncDriver))
        {
        USB_TGT_ERR("Attach to the tcd fail\n",
                    1, 2, 3, 4, 5, 6);
        
        if (OK != usbTgtFuncRemove(pFuncDriver))
            {
            USB_TGT_WARN("usbTgtFuncRemove fail\n", 1, 2, 3, 4, 5, 6);
            }
        
        /* Do not return */
        }    

    USB_TGT_DBG("usbTgtFuncAdd():Attach function driver to the TCD\n",
                 1, 2, 3, 4, 5, 6);
    
    /* Updata and do soft connect */
    
    usbTgtTcdInfoUpdate(pTcd);
    
    /* Reset the default pipe */
    
    if (ERROR == usbTgtDefaultPipeReset(pTcd))
        {
        USB_TGT_ERR("Reset the default pipe fail\n",
                    1, 2, 3, 4, 5, 6);
       
        return ERROR;
        }

    /* Do soft connect ? */
    
    if (lstCount(&pTcd->funcList))
        {
        if (ERROR == usbTgtTcdSoftConnect(pTcd, TRUE))
            {
            USB_TGT_ERR("Do soft connect fail\n",
                        1, 2, 3, 4, 5, 6);
           
            return ERROR;
            }
        }
    
    return OK;
    }

/*******************************************************************************
*
* usbTgtTcdRemove - remove one Target controller driver from the the target stack.
*
* This routine removes one Target controller driver from the the target stack.
* It will search the global function driver list to find if there is any 
* function drivers need be detached from the target controller driver (TCD).
* If yes, detach them.
* 
* The target stack will automiticlly flush the Device descriptors and 
* the configuration descriptors which used to indicate the USB device
* chararactics.
*
* NOTE:
* This routine will not destroy the <'pUSBTGT_TCD'> data structure, will not 
* delelte the <'pTcd'> from the global TCD list. It is used to remove the low
* level target controller driver from the TML, and detach all the function 
* driver's which attached to it. Meanwhile, it will reset the <'pTcdSpecific'>
* item to NULL.
*
* RETURNS: OK, or ERROR if there is something wrong.
*
* ERRNO: N/A
*/

STATUS usbTgtTcdRemove
    (
    pUSBTGT_TCD pTcd
    )
    {
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;
    NODE *              pNode = NULL;
 
    if ((NULL == pTcd) ||
        (FALSE == IS_TCD_MAGICID_VALID(pTcd->uTCDMagicID)))
        {
        USB_TGT_ERR("Invalid parameter %s\n",
                    ((NULL == pTcd) ? "pTcd is NULL" :
                     "pTcd->uTCDMagicID is invalid"), 2, 3, 4, 5, 6);
                     
        return ERROR;
        }

    semTake(pTcd->tcdMutex, USBTGT_WAIT_TIMEOUT);

    /* Search the function list */

    pNode = lstFirst(&pTcd->funcList);

    while (NULL != pNode)
        {
        pFuncDriver = TCD_LIST_NODE_TO_USBTGT_FUNC(pNode);

        /* Record the next node */
        
        pNode = lstNext(pNode);        
        
        if ((NULL !=  pFuncDriver) &&
            (TRUE == IS_FUNC_DRIVER_MAGICID_VALID(pFuncDriver->uFuncMagicID)) &&
            (pTcd == usbTgtFindTcdByName 
                     (pFuncDriver->pTcdName,pFuncDriver->uTcdUnit)))
            {
            if ( ERROR != usbTgtFuncDetach(pFuncDriver->pConfig,pFuncDriver))
                {
                if (ERROR != lstFind (&pTcd->funcList, &pFuncDriver->funcInTcdListNode))
                    lstDelete (&pTcd->funcList, &pFuncDriver->funcInTcdListNode);

                USB_TGT_WARN("usbTgtTcdRemove() detach pFuncDriver %p from "
                             "pConfig %p fail\n",
                             pFuncDriver, pFuncDriver->pConfig, 3, 4, 5, 6);

                /* Do not return to scan all function drivers */
                }
            }
        }

    (void)semGive(pTcd->tcdMutex);

    /* 
     * Reset the pTcd->uTCDMagicID to mark it removed from the TML
     * And can be re-add to TML if needed.
     */

    semTake (pTcd->tcdMutex, USBTGT_WAIT_TIMEOUT);
    pTcd->uTCDMagicID = USBTGT_MAGIC_ID_ADD_EN;
    (void)semGive (pTcd->tcdMutex);
    
    /* Updata and do soft connect */
    
    usbTgtTcdInfoUpdate(pTcd);
    
    /* Reset the default pipe */
    
    if (ERROR == usbTgtDefaultPipeReset(pTcd))
        {
        USB_TGT_ERR("Reset the default pipe fail\n",
                    1, 2, 3, 4, 5, 6);
       
        return ERROR;
        }

    /* Do soft connect ? */
    
    if (lstCount(&pTcd->funcList))
        {
        if (ERROR == usbTgtTcdSoftConnect(pTcd, TRUE))
            {
            USB_TGT_ERR("Do soft connect fail\n",
                        1, 2, 3, 4, 5, 6);
           
            return ERROR;
            }
        }

    return OK;
    }

/*******************************************************************************
*
* usbTgtTcdAdd - add one target controller driver in the the target stack.
*
* This routine adds one target controller driver in the the target stack.
* It will search the global function driver list to find if there is any 
* function drivers need be attached to the target controller driver.
* If yes, attach them.
* 
* The target stack will automiticlly generate the Device descriptors and 
* the configuration descriptors which will be used to indicate the USB device
* characteristics.
*
* RETURNS: OK, or ERROR if there is something wrong.
*
* ERRNO: N/A
*/

STATUS usbTgtTcdAdd
    (
    pUSBTGT_TCD pTcd
    )
    {
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;
    pUSBTGT_CONFIG      pConfig = NULL;
    int                 index = 0;
    int                 count = 0;
    int                 listCount;
    int                 configIndex;

    /* Validate parameters */

    if ((NULL == pTcd) ||
        (USBTGT_MAGIC_ID_ADD_EN != pTcd->uTCDMagicID))
        {
        USB_TGT_ERR("Invalid parameter %s\n",
                    ((NULL == pTcd) ? "pTcd is NULL" :
                     "pTcd->uTCDMagicID is not USBTGT_MAGIC_ID_ADD_EN"),
                     2, 3, 4, 5, 6);
                     
        return ERROR;
        }
    
    /* 
     * Require one vallid magic ID to mark the TCD has been added to TML 
     * do not care whether any function driver attached to it or not.
     */
     
    semTake (pTcd->tcdMutex,USBTGT_WAIT_TIMEOUT);
    pTcd->uTCDMagicID = usbTgtMaggicIDRequire(USBTGT_TCD_SIG);
    (void)semGive (pTcd->tcdMutex);
    
    /* 
     * When the tcd added, find and attach all the 
     * functions by searching the function list 
     */

    /* Disconnet first */
    
    (void)usbTgtTcdSoftConnect(pTcd, FALSE);
    
    OS_WAIT_FOR_EVENT(gUsbTgtFuncListLock, USBTGT_WAIT_TIMEOUT);

    count = lstCount(&gUsbTgtFuncDriverList);

    for (index = 1; index <= count; index++)
        {
        pFuncDriver = (pUSBTGT_FUNC_DRIVER)lstNth(&gUsbTgtFuncDriverList, index);
        if ((NULL != pFuncDriver) &&
            (TRUE == IS_FUNC_DRIVER_MAGICID_VALID(pFuncDriver->uFuncMagicID)) &&
            (NULL == pFuncDriver->pTcd) &&
            (pTcd == usbTgtFindTcdByName
                     (pFuncDriver->pTcdName, pFuncDriver->uTcdUnit)))
            {
            USB_TGT_DBG("Adding Function Driver %s\n",
                        pFuncDriver->pFuncName ? pFuncDriver->pFuncName :
                        "NULL", 2, 3, 4, 5, 6);
                        
            listCount = lstCount(&pTcd->configList);
            
            if ((pFuncDriver->uConfigToBind <= USBTGT_TCD_MAX_CONFIG_COUNT) &&
                (pFuncDriver->uConfigToBind > listCount))
                {
                for (configIndex = listCount; 
                     configIndex < pFuncDriver->uConfigToBind; 
                     configIndex ++)
                    {
                    pConfig = usbTgtConfigCreate();
                    if (NULL == pConfig)
                        {
                        USB_TGT_ERR("Create config fail \n",
                                    configIndex, 2, 3, 4, 5 ,6);

                        /* Even fail, we do nothing here*/
                        
                        return ERROR;
                        }
                    
                    /* Copy the Device Info to all the configurations */

                    memcpy(&pConfig->DeviceInfo, &pTcd->DeviceInfo,
                           sizeof(USBTGT_DEVICE_INFO));

                    pConfig->pTcd = pTcd;
                    pConfig->uConfigIndex = (UINT8)configIndex;
                    lstAdd (&pTcd->configList,&pConfig->configNode);
                    }        
                }
            
            /* Find the configuration which the function driver to be attached */

            pConfig = (pUSBTGT_CONFIG)lstNth(&pTcd->configList, 
                                             pFuncDriver->uConfigToBind);

            if (NULL == pConfig)
                {
                USB_TGT_ERR("No find the required configuration\n",
                            1, 2, 3, 4, 5, 6);
                
                continue;
                }

            /* Attach the function driver with TCD */
            
            if (ERROR == usbTgtFuncAttach(pConfig, pFuncDriver))
                {
                USB_TGT_DBG("Attach Function Driver %s fail\n",
                            pFuncDriver->pFuncName ? pFuncDriver->pFuncName :
                            "NULL", 2, 3, 4, 5, 6);

                /* Do not return, till all the listed function attached */
                }
            
            }
        }

    (void)OS_RELEASE_EVENT(gUsbTgtFuncListLock);

    /* Updata and do soft connect */
    
    usbTgtTcdInfoUpdate(pTcd);

    /* Reset the default pipe */
    
    if (ERROR == usbTgtDefaultPipeReset(pTcd))
        {
        USB_TGT_ERR("Reset the default pipe fail\n",
                    1, 2, 3, 4, 5, 6);
       
        return ERROR;
        }

    /* Do soft connect ? */
    
    if (lstCount(&pTcd->funcList))
        {
        if (ERROR == usbTgtTcdSoftConnect(pTcd, TRUE))
            {
            USB_TGT_ERR("Do soft connect fail\n",
                        1, 2, 3, 4, 5, 6);
           
            return ERROR;
            }
        }

    USB_TGT_DBG("Adding TCD Driver 0x%X done \n",
                pTcd, 2, 3, 4, 5, 6);

    return OK;
    }


/*******************************************************************************
*
* usbTgtFuncUnRegister - un-register the function driver from the target stack
*
* This routine un-registers the function driver from the target stack. It will
* detach the function driver from the right TCD, and destroy all the data
* structure resource.
*
* RETURNS: always OK
*
* ERRNO: N/A
*/

STATUS usbTgtFuncUnRegister
    (
    USB_TARG_CHANNEL    targChannel
    )
    {
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;

    /* Validate parameters */

    pFuncDriver = usbTgtTargChannelToFuncDriver(targChannel);

    if (NULL == pFuncDriver)
        {
        USB_TGT_WARN("Invalid parameter: targChannel 0x%X, "
                     "funntion already un-register\n",
                     targChannel, 2, 3, 4, 5 ,6);

        return OK;
        }

    if (usbTgtFuncRemove(pFuncDriver) != OK) 
        {
        USB_TGT_WARN("Remove pFuncDriver 0x%X with targChannel 0x%X fail\n",
                     pFuncDriver, targChannel, 3, 4, 5, 6);

        }        
        
    (void)usbTgtFuncTargChannelDestroy(pFuncDriver->targChannel);

    if (usbTgtFuncDestroy (pFuncDriver) != OK) 
        {
        USB_TGT_WARN("Destroy pFuncDriver 0x%X with targChannel 0x%X fail\n",
                      pFuncDriver, targChannel, 3, 4, 5, 6);
        
        }
    
    USB_TGT_DBG("Ru-register the function driver from TML\n",
                pFuncDriver, 2, 3, 4, 5 ,6);

    return OK;
    }


/*******************************************************************************
*
* usbTgtFuncRegister - register the function driver to the target stack
*
* This routine registers the function driver to the target stack.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*/

USB_TARG_CHANNEL usbTgtFuncRegister
    (
    pUSBTGT_FUNC_INFO pFuncInfo
    )
    {
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;

    /* Validate parameters */

    if (NULL == pFuncInfo)
        {
        USB_TGT_ERR("Invalid parameter, %s is NULL\n",
                    "pFuncInfo",
                     2, 3, 4, 5 ,6);

        return USBTGT_TARG_CHANNEL_DEAD;
        }

    /* Create one function driver with the information */

    pFuncDriver = usbTgtFuncCreate(pFuncInfo);

    if (NULL == pFuncDriver)
        {
        USB_TGT_ERR("Create pFuncDriver data structure fail \n",
                    1, 2, 3, 4, 5 ,6);

        return USBTGT_TARG_CHANNEL_DEAD;
        }
    
    USB_TGT_DBG("Create the function driver %p sucessfully\n",
                pFuncDriver, 2, 3, 4, 5 ,6);

    

    /* Add the function driver to the TML */
    
    if (OK != usbTgtFuncAdd (pFuncDriver))
        {
        USB_TGT_ERR("Add the function driver to TML fail \n",
                    1, 2, 3, 4, 5 ,6);
        
        (void)usbTgtFuncDestroy (pFuncDriver);

        return USBTGT_TARG_CHANNEL_DEAD;
        }
    
    /* To create one valid targ channel */
    
    USB_TGT_DBG("Create the targChannel 0x%X sucessfully\n",
                pFuncDriver->targChannel, 2, 3, 4, 5 ,6);
    
    pFuncDriver->targChannel = usbTgtFuncTargChannelCreate(pFuncDriver);

    if (USBTGT_TARG_CHANNEL_DEAD == pFuncDriver->targChannel)
        {
        USB_TGT_ERR("Create targChannel fail \n",
                    1, 2, 3, 4, 5 ,6);
        
        (void )usbTgtFuncRemove(pFuncDriver);

        (void)usbTgtFuncDestroy (pFuncDriver);
        
        return USBTGT_TARG_CHANNEL_DEAD;
        }

    USB_TGT_DBG("Register targChannel 0x%X sucessfully\n",
                pFuncDriver->targChannel, 2, 3, 4, 5 ,6);

    return pFuncDriver->targChannel;
    }

/*******************************************************************************
*
* usbTgtTcdUnRegister - un-register the TCD from the target stack
*
* This routine un-registers the TCD from the target stack. It will
* detach all the function driver from the right TCD, and destroy the TCD data
* structure resource.
*
* RETURNS: always OK
*
* ERRNO: N/A
*/

STATUS usbTgtTcdUnRegister
    (
    pUSBTGT_TCD pTcd
    )
    {
    /* Validate parameters */

    if (NULL == pTcd)
        {
        USB_TGT_WARN("Invalid parameter: pTcd 0x%X \n",
                     pTcd, 2, 3, 4, 5 ,6);

        return OK;
        }

    /* Remove TCD from TML */
    
    if (usbTgtTcdRemove(pTcd) != OK) 
        {
        USB_TGT_WARN("Remove pTcd 0x%X fail\n",
                     pTcd, 2, 3, 4, 5, 6);
        }        

    /* Destroy and release the resource */

    if (usbTgtTcdDestroy (pTcd) != OK) 
        {
        USB_TGT_WARN("Destroy pTcd 0x%X fail\n",
                      pTcd, 2, 3, 4, 5, 6);        
        }
    
    USB_TGT_DBG("Ru-register the TCD driver from TML\n",
                pTcd, 2, 3, 4, 5 ,6);

    return OK;
    }


/*******************************************************************************
*
* usbTgtTcdRegister - register the tcd controller to the target stack
*
* This routine registers the tcd controller to the target stack.
*
* RETURNS: NULL, or valid pUSBTGT_TCD pointer
*
* ERRNO: N/A
*/

pUSBTGT_TCD usbTgtTcdRegister
    (
    pUSBTGT_TCD_INFO pTcdInfo
    )
    {
    pUSBTGT_TCD pTcd = NULL;

    /* Validate parameters */

    if (NULL == pTcdInfo)
        {
        USB_TGT_ERR("Invalid parameter, %s is NULL\n",
                    "pTcdInfo",
                     2, 3, 4, 5 ,6);

        return NULL;
        }

    /* Create one function driver with the information */

    pTcd = usbTgtTcdCreate(pTcdInfo);

    if (NULL == pTcd)
        {
        USB_TGT_ERR("Create pTcd data structure fail \n",
                    1, 2, 3, 4, 5 ,6);

        return NULL;
        }
    
    USB_TGT_DBG("Create the pTcd data %p sucessfully\n",
                pTcd, 2, 3, 4, 5 ,6);

    /* Add the function driver to the TML */
    
    if (OK != usbTgtTcdAdd (pTcd))
        {
        USB_TGT_ERR("Add the TCD driver to TML fail \n",
                    1, 2, 3, 4, 5 ,6);
        
        (void)usbTgtTcdDestroy (pTcd);

        return NULL;
        }

    return (pTcd);
    }


/*******************************************************************************
*
* usbTgtExit - uninitialize the global usb target resource
*
* This routine uninitializes the global usb target resource.Before this routine
* is called, please make sure all the TCD and function driver has been removed
* and no one will use them.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*/

STATUS usbTgtExit
    (
    void
    )
    {    
    if ((vxAtomicSet(&gUsbTgtInited, FALSE) != TRUE))
        {
        USB_TGT_DBG ("usb target stack has been uninitialized\n",
                     1, 2, 3, 4, 5, 6);
        
        return OK;
        }

    if (OS_INVALID_EVENT_ID != gUsbTgtTcdListLock)
        {
        OS_WAIT_FOR_EVENT(gUsbTgtTcdListLock, USBTGT_WAIT_TIMEOUT);

        /* Check the resource on the TCD list */

        if (0 != lstCount (&gUsbTgtTcdList))
            {
            USB_TGT_DBG ("Usb TCD list is not empty\n",
                          1, 2, 3, 4, 5, 6);

            (void)OS_RELEASE_EVENT(gUsbTgtTcdListLock);

            return ERROR;
            } 
        (void)OS_RELEASE_EVENT(gUsbTgtTcdListLock);
        
        (void)OS_DESTROY_EVENT(gUsbTgtTcdListLock);
        
        gUsbTgtTcdListLock = OS_INVALID_EVENT_ID;
        }
    
    if (OS_INVALID_EVENT_ID != gUsbTgtFuncListLock)
        {
        /* Check the resource on the Function driver list */
        
        OS_WAIT_FOR_EVENT(gUsbTgtFuncListLock, USBTGT_WAIT_TIMEOUT);
        
        if (0 != lstCount (&gUsbTgtFuncDriverList))
            {
            USB_TGT_DBG ("Usb function driver list is not empty\n",
                          1, 2, 3, 4, 5, 6);

            (void)OS_RELEASE_EVENT(gUsbTgtFuncListLock);

            return ERROR;
            } 
        (void)OS_RELEASE_EVENT(gUsbTgtFuncListLock);
        
        (void)OS_DESTROY_EVENT(gUsbTgtFuncListLock);
        
        gUsbTgtFuncListLock = OS_INVALID_EVENT_ID;
        }

    if (OS_INVALID_EVENT_ID != gUsbTgtTargChListLock)
        {
        /* Check the resource on the Function driver list */
        
        OS_WAIT_FOR_EVENT(gUsbTgtTargChListLock, USBTGT_WAIT_TIMEOUT);
        
        if (0 != lstCount (&gUsbTgtTargChList))
            {
            USB_TGT_DBG ("Usb function driver list is not empty\n",
                          1, 2, 3, 4, 5, 6);

            (void)OS_RELEASE_EVENT(gUsbTgtTargChListLock);
            
            return ERROR;
            } 
        
        (void)OS_RELEASE_EVENT(gUsbTgtTargChListLock);
        
        (void)OS_DESTROY_EVENT(gUsbTgtTargChListLock);
        
        gUsbTgtTargChListLock = OS_INVALID_EVENT_ID;
        }
    
    USB_TGT_DBG ("Usb target uninitialized successfully\n",
                 1, 2, 3, 4, 5, 6);

    return OK;
    }


/*******************************************************************************
*
* usbTgtInit - initialize the usb target stack
*
* This routine initializes the usb target stack.It will initialize one global
* TCD list <'gUsbTgtTcdList'> and one function driver global list 
* <'gUsbTgtFuncDriverList'> which used to record the added TCD and function
* driver which installed in the system.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*/

STATUS usbTgtInit
    (
    void
    )
    {
    if (vxAtomicSet(&gUsbTgtInited, TRUE) != FALSE)
        {
        USB_TGT_DBG ("usb target stack has been initialized\n",
                     1, 2, 3, 4, 5, 6);
        
        return OK;
        }

    if (ossInitialize () != OK)
        {
        USB_TGT_ERR ("Init the oss Lib fail\n",
                     1, 2, 3, 4, 5, 6);

        if (TRUE == vxAtomicSet(&gUsbTgtInited, FALSE))
            {
            USB_TGT_DBG ("Re-Init gUsbTgtInited as un-initialized \n",
                         1, 2, 3, 4, 5, 6);
            }
        
        return ERROR;
        }
    
    /* Init the global TCD list */

    lstInit(&gUsbTgtTcdList);

    /* Init the global function driver list */

    lstInit(&gUsbTgtFuncDriverList);

    /* Init the targ channel handler list */

    lstInit (&gUsbTgtTargChList);
    
    /* Create the list lock */
    
    gUsbTgtTcdListLock = OS_CREATE_EVENT(OS_EVENT_SIGNALED);

    /* Check if the device driver list lock is valid */

    if (OS_INVALID_EVENT_ID == gUsbTgtTcdListLock)
        {
        USB_TGT_ERR ("No enough resource to create usb target list lock\n",
                     1, 2, 3, 4, 5, 6);

        (void)usbTgtExit ();
        
        return ERROR;
        }

    /* Create the list lock */
    
    gUsbTgtFuncListLock = OS_CREATE_EVENT(OS_EVENT_SIGNALED);

    /* Check if the device driver list lock is valid */

    if (OS_INVALID_EVENT_ID == gUsbTgtFuncListLock)
        {
        USB_TGT_ERR ("No enough resource to create usb target list lock\n",
                     1, 2, 3, 4, 5, 6);

        (void)usbTgtExit ();
        
        return ERROR;
        }

    /* Create the targ channel handler list lock */
    
    gUsbTgtTargChListLock = OS_CREATE_EVENT(OS_EVENT_SIGNALED);

    /* Check if the device driver list lock is valid */

    if (OS_INVALID_EVENT_ID == gUsbTgtTargChListLock)
        {
        USB_TGT_ERR ("No enough resource to create usb targ channel handler list lock\n",
                     1, 2, 3, 4, 5, 6);

        (void)usbTgtExit ();
        
        return ERROR;
        }

    USB_TGT_INFO ("USB Target initialized successfully\n",
                  1, 2, 3, 4, 5, 6);

    return OK;

    }


