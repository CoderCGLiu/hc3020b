/* usbTgtNetMediumBinder.h - Definitions of The Network Medium Binder Module */

/*
 * Copyright (c) 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
Modification history
--------------------
01b,09mar11,s_z  Code clean up
01a,04jan11,s_z  written
*/

/*
DESCRIPTION

This file includes the definitions of USB medium binder modules.

INCLUDES: vxWorks.h
*/

#ifndef __INCusbTgtNetMediumBinderh
#define __INCusbTgtNetMediumBinderh

#include <vxWorks.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum 
    {
    /* This is the statistics feature seletor code defined in CDC/ECM */
    
    USBTGT_NET_IOCTL_END_RESERVED                   = (0x0000),
    USBTGT_NET_IOCTL_END_GET_XMIT_OK                = (0x0001),
    USBTGT_NET_IOCTL_END_GET_RCV_OK                 = (0x0002),
    USBTGT_NET_IOCTL_END_GET_XMIT_ERROR             = (0x0003),
    USBTGT_NET_IOCTL_END_GET_RCV_ERROR              = (0x0004),
    USBTGT_NET_IOCTL_END_GET_RCV_NO_BUFFER          = (0x0005),
    USBTGT_NET_IOCTL_END_GET_DIRECTED_BYTES_XMIT    = (0x0006),
    USBTGT_NET_IOCTL_END_GET_DIRECTED_FRAMES_XMIT   = (0x0007),
    USBTGT_NET_IOCTL_END_GET_MULTICAST_BYTES_XMIT   = (0x0008),
    USBTGT_NET_IOCTL_END_GET_MULTICAST_FRAMES_XMIT  = (0x0009),
    USBTGT_NET_IOCTL_END_GET_BRODACAST_BYTES_XMIT   = (0x000A),
    USBTGT_NET_IOCTL_END_GET_BRODACAST_FRAMES_XMIT  = (0x000B),
    USBTGT_NET_IOCTL_END_GET_DIRECTED_BYTES_RCV     = (0x000C),
    USBTGT_NET_IOCTL_END_GET_DIRECTED_FRAMES_RCV    = (0x000D),
    USBTGT_NET_IOCTL_END_GET_MULTICAST_BYTES_RCV    = (0x000E),
    USBTGT_NET_IOCTL_END_GET_MULTICAST_FRAMES_RCV   = (0x000F),
    USBTGT_NET_IOCTL_END_GET_BRODACAST_BYTES_RCV    = (0x0010),
    USBTGT_NET_IOCTL_END_GET_BRODACAST_FRAMES_RCV   = (0x0011),
    USBTGT_NET_IOCTL_END_GET_RCV_CRC_ERROR          = (0x0012),
    USBTGT_NET_IOCTL_END_GET_TRANSMIT_QUEUE_LENGTH  = (0x0013),
    USBTGT_NET_IOCTL_END_GET_RCV_ERROR_ALIGNMENT    = (0x0014),
    USBTGT_NET_IOCTL_END_GET_XMIT_ONE_COLLISION     = (0x0015),
    USBTGT_NET_IOCTL_END_GET_XMIT_MORE_COLLISIONS   = (0x0016),
    USBTGT_NET_IOCTL_END_GET_XMIT_DEFERRED          = (0x0017),
    USBTGT_NET_IOCTL_END_GET_XMIT_MAX_COLLISIONS    = (0x0018),
    USBTGT_NET_IOCTL_END_GET_RCV_OVERRUN            = (0x0019),
    USBTGT_NET_IOCTL_END_GET_XMIT_UNDERRUN          = (0x001A),
    USBTGT_NET_IOCTL_END_GET_XMIT_HEARTBEAT_FAILURE = (0x001B),
    USBTGT_NET_IOCTL_END_GET_XMIT_TIMES_CRS_LOST    = (0x001C),
    USBTGT_NET_IOCTL_END_GET_XMIT_LATE_COLLISIONS   = (0x001D),

    /* RNDIS OIDs exchanged to the IOCTL code */

    USBTGT_NET_IOCTL_END_GET_MTU                    = (0x0020),
    USBTGT_NET_IOCTL_END_SET_MTU                    = (0x8020),
    USBTGT_NET_IOCTL_END_GET_MULTIPLE_ADDR          = (0x0021),
    USBTGT_NET_IOCTL_END_SET_MULTIPLE_ADDR_ADD      = (0x8021),
    USBTGT_NET_IOCTL_END_SET_MULTIPLE_ADDR_DEL      = (0x8022),
    USBTGT_NET_IOCTL_END_GET_MAC_ADDR               = (0x0023),
    USBTGT_NET_IOCTL_END_SET_MAC_ADDR               = (0x8023),
    
    /* Power management defination used by RNDIS */
    
    USBTGT_NET_IOCTL_END_PNP_CAPABILITIES_GET       = (0x0030),
    USBTGT_NET_IOCTL_END_PNP_POWER_GET              = (0x0031),
    USBTGT_NET_IOCTL_END_PNP_POWER_SET              = (0x8031),
    USBTGT_NET_IOCTL_END_PNP_WAKEUP_PATTERN_ADD     = (0x8032),
    USBTGT_NET_IOCTL_END_PNP_WAKEUP_PATTERN_REMOVE  = (0x8033),
    USBTGT_NET_IOCTL_END_PNP_WAKEUP_ENABLE_GET      = (0x0034),
    USBTGT_NET_IOCTL_END_PNP_WAKEUP_ENABLE_SET      = (0x8034),

    /* General using */
    
    USBTGT_NET_IOCTL_END_GET_MAX_FRAME_SIZE         = (0x0043),
    USBTGT_NET_IOCTL_END_GET_LINK_SPEED             = (0x0044),
    USBTGT_NET_IOCTL_END_GET_TX_BLOCK_SIZE          = (0x0045),
    USBTGT_NET_IOCTL_END_GET_RX_BLOCK_SIZE          = (0x0046),    
    } USBTGT_NET_IOCTL_CMD;

typedef enum usbtgt_binder_buf_type
    {
    USBTGT_BINDER_BUF_UNKNOWN         = (0),
    USBTGT_BINDER_BUF_FREE_XMIT       = (1),
    USBTGT_BINDER_BUF_FREE_RCV        = (2),
    USBTGT_BINDER_BUF_XMIT_TO_MEDIUM  = (3),
    USBTGT_BINDER_BUF_RCV_FROM_MEDIUM = (4)
    }USBTGT_BINDER_BUF_TYPE;


typedef enum usbtgt_binder_medium_type
    {
    USBTGT_BINDER_MEDIUM_UNKNOWN         = (0),
    USBTGT_BINDER_MEDIUM_VIRTUAL_END     = (1),
    USBTGT_BINDER_MEDIUM_END_BRIGGE      = (2)
    }USBTGT_BINDER_MEDIUM_TYPE;

typedef enum usbtgt_binder_transport_type
    {
    USBTGT_BINDER_TRANSPORT_UNKNOWN      = (0),
    USBTGT_BINDER_TRANSPORT_USB_RNDIS    = (1)
    }USBTGT_BINDER_TRANSPORT_TYPE;


typedef enum usbtgt_binder_notify_type
    {
    USBTGT_BINDER_NOTIFY_UNKNOWN           = (0),
    USBTGT_BINDER_NOTIFY_DATA_TO_MEDIUM    = (1),
    USBTGT_BINDER_NOTIFY_DATA_TO_TRANSPORT = (2)
    }USBTGT_BINDER_NOTIFY_TYPE;


typedef struct usbtgt_binder_buf 
    {
    NODE   binderBufNode;
    char * pBuf;
    int    brfLen;
    int    actDataLen;
    int    headerLen;
    BOOL   bOccupied;
    }USBTGT_BINDER_BUF, * pUSBTGT_BINDER_BUF;

typedef struct usbtgt_binder
    {
    void *   pMediumDev;           /* The mediun device pointer, END, Wlan or other medium */
    void *   pTransportDev;        /* Low level transport interface, RNDIS or CDC */
    SEM_ID   binderMutex;          /* Used to protect the resource */
    int      binderBufCount;       /* The binder buffer count */
    int      binderBufSize;        /* The binder buffer size sum of data pool and header */
    LIST     freeXmitBufList;      /* The free transmit buffer list */
    LIST     freeRcvBufList;       /* The free receive buffer list */
    LIST     xmitToMediumBufList;  /* The transmit buffer list for data to medium */
    LIST     rcvFromMediumBufList; /* The receiver buffer list for data from medium */
    char *   pMediumName;          /* The medium name */
    char *   pTransportName;       /* The low dev name */
    UINT8    uMediumUint;          /* The medium uint */
    UINT8    uTransportUint;       /* The low dev uint */
    UINT8    uMediumType;          /* Medium type */
    UINT8    uTransportType;       /* Low dev type */    
    void   (* dataToTransport)(void * pTransportDev);
    STATUS (* dataToMedium)(void * pMediumDev);
    int    (* mediumIoctl)(void * pMediumDev,int funcCode,char * arg);    
    int    (* lowDevIoctl)(void * pTransportDev,int funcCode,char * arg);    
    }USBTGT_BINDER, *pUSBTGT_BINDER;

/* extern the routines */

IMPORT pUSBTGT_BINDER_BUF usbTgtNetMediumBinderBufGet
    (
    pUSBTGT_BINDER         pBinder,
    USBTGT_BINDER_BUF_TYPE binderBufType
    );

IMPORT STATUS usbTgtNetMediumBinderBufPut
    (
    pUSBTGT_BINDER         pBinder,
    pUSBTGT_BINDER_BUF     pBinderBuf,
    USBTGT_BINDER_BUF_TYPE binderBufType
    );

IMPORT STATUS usbTgtNetMediumBinderDestroy
    (
    pUSBTGT_BINDER  pBinder
    );

IMPORT pUSBTGT_BINDER usbTgtNetMediumBinderCreate
    (
    int             freeBufCount,
    int             dataPoolSize,
    int             dataHeaderLen
    );

IMPORT STATUS usbTgtNetMediumBinderAttach
    (
    pUSBTGT_BINDER pBinder,
    char *         pMediumName,   /* Medium name which need to bind */
    int            mediumUnit,    /* Medium unit which need to bind */
    UINT8          uMediumType    /* Medium type */
    );
IMPORT STATUS usbTgtNetMediumBinderDetach
    (
    pUSBTGT_BINDER pBinder
    );

#ifdef __cplusplus
}
#endif

#endif /* __INCusbTgtNetMediumBinderh */


