/* usbXhcdUtil.c - USB XHCI Driver Utility Routines */

/*
 * Copyright (c) 2011-2015 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01o,23jul15,j_x  Check the return value when get address is root hub address (VXW6-84657)
01n,03apr15,j_x  add post reset hook. (US54139)
01m,02dec14,wyy  Don't set port power for hardwired hub of xHCI (VXW6-83862)
01l,16jun14,wyy  Support INTEL 8 series and 9 series PCH (VXW6-70103)
01k,03sep13,wyy  Add macro OS_BUSY_WAIT_US and OS_BUSY_WAIT_MS to busy wait
                 when rebooting (WIND00432482)
01j,03may13,wyy  Remove compiler warning (WIND00356717)
01i,20feb13,j_z  Change MSI/MSI-X connect/disconnect usage
01h,19oct12,w_x  Add polling mode support
01g,17oct12,w_x  Fix compiler warnings (WIND00370525)
01f,17oct12,j_x  Change device speed to USBHST_HIGH_SPEED for TT hubs (WIND00383003)
01e,17oct12,w_x  Support for Warm Reset (WIND00374209)
01d,16oct12,j_x  Add device speed parameter to usbXhcdDeviceGet() and
                 usbXhcdHubDeviceUpdate() (WIND00381845)
01c,20sep12,w_x  Use separate default pipe for USB2 and USB3 bus (WIND00377413)
01b,04sep12,w_x  Address review comments
01a,09may11,w_x  written
*/

#include "usbXhcdInterfaces.h"
#include "ffslib.h"
/*******************************************************************************
*
* vxbUsbXhcdWrite64 - write a 64 bit value to a 64 bit xHCI register
*
* This routine is to write a 64 bit value to a 64 bit xHCI register.
*
* If the xHC supports 64-bit addressing (AC64 = '1'), then software should
* write registers containing 64-bit address fields using only Qword accesses.
* If a system is incapable of issuing Qword accesses, then writes to the
* 64-bit address fields shall be performed using 2 Dword accesses; low
* Dword-first, high-Dword second.
*
* If the xHC supports 32-bit addressing (AC64 = '0'), then the high Dword of
* registers containing 64-bit address fields are unused and software should
* write addresses using only Dword accesses.
*
* To support all systems, we choose to use two Dword accesses. This won't have
* significant performance impact compared to testing of AC64 then choose which
* way to use.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void vxbUsbXhcdWrite64
    (
    void *      handle,
    UINT64 *    addr,
    UINT64      val
    )
    {
    /* Cast to 32 bit pointer */

    UINT32 *    pAddr = (UINT32 *)addr;

    /* Low Dword-first */

    vxbWrite32 (handle, pAddr, USB_XHCD_LO32(val));

    /* High-Dword second */

    vxbWrite32 (handle, (pAddr + 1), USB_XHCD_HI32(val));
    }

/*******************************************************************************
*
* vxbUsbXhcdRead64 - read a 64 bit value from a 64 bit xHCI register
*
* This routine is to read a 64 bit value from a 64 bit xHCI register.
*
* RETURNS: 64 bit register value read from the specified address.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

UINT64 vxbUsbXhcdRead64
    (
    void *      handle,
    UINT64 *    addr
    )
    {
    UINT32 *    pAddr = (UINT32 *)addr;
    UINT64      lo32 = (UINT64)(vxbRead32(handle, pAddr));
    UINT64      hi32 = (UINT64)(vxbRead32(handle, (pAddr + 1)));

    return (UINT64)(lo32 + ((hi32) << 32));
    }

/*******************************************************************************
*
* usbXhcdTrbName - convert TRB Type into printable name string
*
* This routine is to convert TRB Type into printable name string.
*
* RETURNS: pointer to TRB Type name string.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

char * usbXhcdTrbName
    (
    UINT8 uTrbType
    )
    {
    char * TrbTypeName[50] =
                        {"Reserved0",                           /* 0 */
                         "Normal",                              /* 1 */
                         "Setup Stage",                         /* 2 */
                         "Data Stage",                          /* 3 */
                         "Status Stage",                        /* 4 */
                         "Isoch",                               /* 5 */
                         "Link",                                /* 6 */
                         "Event Data",                          /* 7 */
                         "No-Op Transfer",                      /* 8 */
                         "Enable Slot Command",                 /* 9 */
                         "Disable Slot Command",                /* 10 */
                         "Address Device Command",              /* 11 */
                         "Configure Endpoint Command",          /* 12 */
                         "Evaluate Context Command",            /* 13 */
                         "Reset Endpoint Command",              /* 14 */
                         "Stop Endpoint Command",               /* 15 */
                         "Set TR Dequeue Pointer Command",      /* 16 */
                         "Reset Device Command",                /* 17 */
                         "Force Event Command",                 /* 18 */
                         "Negotiate Bandwidth Command",         /* 19 */
                         "Set Latency Tolerance Value Command", /* 20 */
                         "Get Port Bandwidth Command",          /* 21 */
                         "Force Header Command",                /* 22 */
                         "No Op Command",                       /* 23 */
                         "Reserved24",                          /* 24 */
                         "Reserved25",                          /* 25 */
                         "Reserved26",                          /* 26 */
                         "Reserved27",                          /* 27 */
                         "Reserved28",                          /* 28 */
                         "Reserved29",                          /* 29 */
                         "Reserved30",                          /* 30 */
                         "Reserved31",                          /* 31 */
                         "Transfer Event",                      /* 32 */
                         "Command Completion Event",            /* 33 */
                         "Port Status Change Event",            /* 34 */
                         "Bandwidth Request Event",             /* 35 */
                         "Doorbell Event",                      /* 36 */
                         "Host Controller Event",               /* 37 */
                         "Device Notification Event",           /* 38 */
                         "MFINDEX Wrap Event",                  /* 39 */
                         "Reserved40",
                         "Reserved41",
                         "Reserved42",
                         "Reserved43",
                         "Reserved44",
                         "Reserved45",
                         "Reserved46",
                         "Reserved47",
                         "Vendor Defined",                      /* 48 */
                         "Incorrect"};                          /* 49 */

    /*
     * Yes! Hard code here and no excuse!
     * See xHCI spec Table 131: TRB Type Definitions.
     */

    if (uTrbType <= 47)
        return TrbTypeName[uTrbType];
    else if (uTrbType <= 63)
        return TrbTypeName[48];
    else
        return TrbTypeName[49];
    }

/*******************************************************************************
*
* usbXhcdCmdCompCodeName - convert TRB Type into printable name string
*
* This routine is to convert TRB Type into printable name string.
*
* RETURNS: pointer to TRB Type name string.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

char * usbXhcdCmdCompCodeName
    (
    UINT8 uTrbCompCode
    )
    {
    char * CmdCompCodeName[40] =
                        {"Invalid",                             /* 0 */
                         "Success",                             /* 1 */
                         "Data Buffer Error",                   /* 2 */
                         "Babble Detected Error",               /* 3 */
                         "USB Transaction Error",               /* 4 */
                         "TRB Error",                           /* 5 */
                         "Stall Error",                         /* 6 */
                         "Resource Error",                      /* 7 */
                         "Bandwidth Error",                     /* 8 */
                         "No Slots Available Error",            /* 9 */
                         "Invalid Stream Type Error",           /* 10 */
                         "Slot Not Enabled Error",              /* 11 */
                         "Endpoint Not Enabled Error",          /* 12 */
                         "Short Packet",                        /* 13 */
                         "Ring Underrun",                       /* 14 */
                         "Ring Overrun",                        /* 15 */
                         "VF Event Ring Full Error",            /* 16 */
                         "Parameter Error",                     /* 17 */
                         "Bandwidth Overrun Error",             /* 18 */
                         "Context State Error",                 /* 19 */
                         "No Ping Response Error",              /* 20 */
                         "Event Ring Full Error",               /* 21 */
                         "Incompatible Device Error",           /* 22 */
                         "Missed Service Error",                /* 23 */
                         "Command Ring Stopped",                /* 24 */
                         "Command Aborted",                     /* 25 */
                         "Transfer Stopped",                    /* 26 */
                         "Transfer Stopped - Length Invalid",   /* 27 */
                         "Reserved28",                          /* 28 */
                         "Max Exit Latency Too Large Error",    /* 29 */
                         "Reserved30",                          /* 30 */
                         "Isoch Buffer Overrun",                /* 31 */
                         "Event Lost Error",                    /* 32 */
                         "Undefined Error",                     /* 33 */
                         "Invalid Stream ID Error",             /* 34 */
                         "Secondary Bandwidth Error",           /* 35 */
                         "Split Transaction Error",             /* 36 */
                         "Reserved(37-191)",                    /* 37 */
                         "Vendor Defined Error(192-223)",       /* 38 */
                         "Vendor Defined Info(224-255)"};       /* 39 */

    /*
     * Yes! Hard code here and no excuse!
     * See xHCI spec Table 130: TRB Completion Code Definitions.
     */

    if (uTrbCompCode <= 36)
        return CmdCompCodeName[uTrbCompCode];
    else if (uTrbCompCode <= 191)
        return CmdCompCodeName[37];
    else if (uTrbCompCode <= 223)
        return CmdCompCodeName[38];
    else
        return CmdCompCodeName[39];
    }

/*******************************************************************************
*
* usbXhcdTrbFormData64 - get 64 bit data from the TRB
*
* This routine is to get 64 bit data from the TRB.
*
* RETURNS: 64 bit data from the TRB.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

UINT64 usbXhcdGetTrbData64
    (
    pUSB_XHCD_DATA  pHCDData,
    pUSB_XHCI_TRB   pTRB
    )
    {
    UINT64 uDataLo = (UINT64)USB_XHCD_SWAP_DESC_DATA32(pHCDData, pTRB->uDataLo);
    UINT64 uDataHi = (UINT64)USB_XHCD_SWAP_DESC_DATA32(pHCDData, pTRB->uDataHi);

    return (UINT64)(uDataLo + ((uDataHi) << 32));
    }

/*******************************************************************************
*
* usbXhcdSetTrbData64 - fill 64 bit data into two 32 bit data high/low in TRB
*
* This routine is to form 64 bit data from the TRBfill 64 bit data into two
* 32 bit data high/low in TRB.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbXhcdSetTrbData64
    (
    pUSB_XHCD_DATA  pHCDData,
    pUSB_XHCI_TRB   pTRB,
    UINT64          uData64
    )
    {
    pTRB->uDataLo = USB_XHCD_SWAP_DESC_DATA32(pHCDData,
                        USB_XHCD_LO32(uData64));
    pTRB->uDataHi = USB_XHCD_SWAP_DESC_DATA32(pHCDData,
                        USB_XHCD_HI32(uData64));
    }

/*******************************************************************************
*
* usbXhcdEpIntervalGet - calculate endpoint context Interval filed
*
* This routine is to calculate endpoint context Interval filed based on device
* speed, endpoint transfer type and the endpoint descriptor bInterval value.
*
* RETURNS: value to set for endpoint context Interval filed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

UINT8 usbXhcdEpIntervalGet
    (
    UINT8 uDevSpeed,
    UINT8 uEpDir,
    UINT8 uEpXferType,
    UINT8 bInterval
    )
    {
    UINT8 uInterval;

    /*
     * Initially set the xHCI EP context Interval value to 0, it will
     * be overriden by proper calculated value according to the EP dir,
     * type and speed.
     */

    uInterval = 0;

    if ((uDevSpeed == USBHST_HIGH_SPEED) ||
        (uDevSpeed == USBHST_SUPER_SPEED))
        {
        if ((uEpXferType == USBHST_INTERRUPT_TRANSFER) ||
            (uEpXferType == USBHST_ISOCHRONOUS_TRANSFER))
            {
            /*
             * For SS/HS interrupt or isochronous endpoints, the
             * bInterval value is used as the exponent for a
             * 2^(bInterval-1) value, and the value corresponds
             * to number of microframes periods (number of 125 us).
             * And the bInterval value must be from 1 to 16.
             *
             * Mapping the bInterval in the EP descriptor to the
             * xHCI EP context Interval field, where the period is
             * calculated as 125 us * 2^Interval, so we should do
             * a substract by 1 from bInterval to get the Interval.
             *
             * We also take care of quirky devices which incorrectly
             * set bInterval to 0 (it should be from 1 to 16). For
             * such cases, we make it a 0, not to substract a 1 to
             * make it rollover to 0xFF.
             */

            uInterval = (UINT8)((bInterval == 0) ?
                                    0 : (bInterval - 1));
            }

        if ((uDevSpeed == USBHST_HIGH_SPEED) &&
            (uEpDir == USB_ENDPOINT_OUT) &&
            ((uEpXferType == USBHST_BULK_TRANSFER) ||
             (uEpXferType == USBHST_CONTROL_TRANSFER)))
            {
            /*
             * For HS bulk/control OUT endpoints, the bInterval must
             * specify the maximum NAK rate of the endpoint. This
             * matches directly the xHCI Interval field for HS bulk/
             * control OUT endpoints. So we just copy directly.
             */

            uInterval = bInterval;
            }
        }
    else
        {
        /* FS isochronous endpoints */

        if ((uDevSpeed == USBHST_FULL_SPEED) &&
            (uEpXferType == USBHST_ISOCHRONOUS_TRANSFER))
            {
            /*
             * For FS isochronous endpoints, the bInterval value is
             * used as the exponent for a 2^(bInterval-1) value, and
             * the value corresponds to number of frames periods
             * (number of 1 ms). And the bInterval value must be from
             * 1 to 16.
             *
             * Mapping the bInterval in the EP descriptor to the
             * xHCI EP context Interval field, where the period is
             * calculated as 125 us * 2^Interval, so we should do
             * the following equation to convert bInterval to the
             * Interval.
             *
             * 2^Interval * 125us = 2^(bInterval - 1) * 1ms ==>
             *
             * 2^Interval * 125us = 2^(bInterval - 1) * 8 * 125us ==>
             *
             * 2^Interval = 2^(bInterval - 1) * 8 ==>
             *
             * 2^Interval = 2^(bInterval - 1) * 2^3 ==>
             *
             * 2^Interval = 2^(bInterval - 1 + 3) ==>
             *
             * Interval = bInterval + 2
             *
             * We also take care of quirky devices which incorrectly
             * set bInterval to 0 (it should be from 1 to 16). For
             * such cases, we make it a 3, since for FS, the minimum
             * period is 1ms.
             */

            uInterval = (UINT8)((bInterval == 0) ?
                                    3 : (bInterval + 2) );
            }

        /* FS/LS interrupt endpoints */

        if (uEpXferType == USBHST_INTERRUPT_TRANSFER)
            {
            /*
             * For FS/LS interrupt endpoints, the value of bInterval
             * field may be from 1 to 255 and is interpreted as number
             * of frames periods (number of 1 ms).
             *
             * Mapping the bInterval to the xHCI EP context Interval
             * field, we need to do the following equation:
             *
             * 2^Interval * 125us = bInterval * 1ms ==>
             *
             * 2^Interval * 125us = bInterval * 8 * 125us ==>
             *
             * 2^Interval = bInterval * 8 ==>
             *
             * Interval = 3 + log2(bInterval)
             *
             * We use FFB_MSB to get the log2, ignoring the low-order
             * bits of the value, which effecitively tighten the polling
             * to 1 microframe of time and it should be fine. Note that
             * the FFB_MSB returns starting from 1, but we should count
             * from 0, so we substract 1 from the FFS_MSB output.
             *
             * We also take care of quirky devices which incorrectly
             * set bInterval to 0 (it should be from 1 to 255). For
             * such cases, we make it a 3, since for FS/LS, the minimum
             * period is 1ms.
             */

            uInterval = (UINT8)(3 + (bInterval == 0 ? 0 : FFS_MSB(bInterval) - 1));
            }
        }

    return uInterval;
    }

/*******************************************************************************
*
* usbXhcdNextExtCapReg - get to the next xHCI Extended Capability register
*
* This routine is to get to the next xHCI Extended Capability register. This
* routine is intended to be used in a series of call to locate the xHCI
* Extended Capability registers. Before the first call to this routine, it is
* assumed that the first xHCI Extended Capability register base address has
* already been found and saved in the <pHCDData>::uXpRegBase element. In
* the first call to this routine, the <uOffset> should be passed a value '0',
* which means the offset from the <pHCDData>::uXpRegBase address, the code
* sequence should check the return value of this call, if the return value
* is '0', meaning it has reached the end of the xHCI Extended Capabilities,
* the loop should stop there; If the return value is not '0', then the return
* value can be used as the <uOffset> parameter to go to the next xHCI Extended
* Capability register.
*
* RETURNS: Next xHCI Extended Capability offset, or 0 when reached end.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

UINT32 usbXhcdNextExtCapReg
    (
    pUSB_XHCD_DATA  pHCDData,
    UINT32          uOffset
    )
    {
    /* Current Capbility register value */

    UINT32 uNext;

    /* If there is no xHCI Extended Capability, reutnr ERROR */

    if (pHCDData->uXpRegBase == 0)
        {
        USB_XHCD_WARN("usbXhcdNextExtCapReg - uXpRegBase not set!\n",
            1, 2, 3, 4, 5, 6);

        return 0;
        }

    /* Read the xHCI Extended Capability Pointer Register */

    uNext = USB_XHCD_READ_XP_REG32(pHCDData, uOffset);

    /* Get the Next field */

    uNext = USB_XHCI_EXT_CAP_NEXT(uNext);

    /*
     * If the Next field is 0 then we have reached the end of xHCI
     * Extended Capabilities; Otherwise, return the next offset.
     */

    if (uNext == 0)
        return 0;
    else
        return uOffset + (uNext << 2);
    }

/*******************************************************************************
*
* usbXhcdGotoExtCap - go to the xHCI Extended Capability with the specified ID
*
* This routine is to go to the xHCI Extended Capability with the specified ID.
*
* RETURNS: OK if the Capbility ID is found and the actual offset set in the
*          <puActOffset>, ERROR if ID not found.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdLocateExtCap
    (
    pUSB_XHCD_DATA  pHCDData,
    UINT32          uOffset,
    UINT32          uExtCapId,
    UINT32 *        puActOffset
    )
    {
    /* Current Capbility register value */

    UINT32 uReg32;

    /* If there is no xHCI Extended Capability, reutnr ERROR */

    if (pHCDData->uXpRegBase == 0)
        {
        USB_XHCD_WARN("usbXhcdNextExtCapReg - uXpRegBase not set!\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    do
        {
        /* Read the xHCI Extended Capability Pointer Register */

        uReg32 = USB_XHCD_READ_XP_REG32(pHCDData, uOffset);

        /* Get the Capbility field */

        uReg32 = USB_XHCI_EXT_CAP_ID(uReg32);

        /* If we found the Capbility ID, return OK and its offset */

        if (uReg32 == uExtCapId)
            {
            *puActOffset = uOffset;

            USB_XHCD_VDBG("usbXhcdLocateExtCap - found uExtCapId 0x%X at uOffset %d!\n",
                uExtCapId, uOffset, 3, 4, 5, 6);

            return OK;
            }

        /* Go to the next ExtCapReg */

        uOffset = usbXhcdNextExtCapReg(pHCDData, uOffset);
        }
    while (uOffset != 0);

    USB_XHCD_WARN("usbXhcdLocateExtCap - did not find expected uExtCapId 0x%X!\n",
        uExtCapId, 2, 3, 4, 5, 6);

    /* Reached the end but ID still not found, return ERROR */

    return ERROR;
    }

/*******************************************************************************
*
* usbXhcdLocatePciCap - find the PCI/PCIe Standard Capability with specified ID
*
* This routine is to find the PCI/PCIe Standard Capability with specified ID.
*
* RETURNS: OK if the Capbility ID is found and the actual offset set in the
*          <puActOffset>, ERROR if ID not found.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdLocatePciCap
    (
    pUSB_XHCD_DATA  pHCDData,
    UINT8           uPciCapId,
    UINT8 *         puActOffset
    )
    {
    UINT8  uCapIdPci = 0;
    UINT8  uOffsetPci = 0;
    UINT8  uOffsetPciNext = 0;

    /* If there is no xHCI Extended Capability, reutnr ERROR */

    if (pHCDData->uPciCapPtr == 0)
        {
        USB_XHCD_WARN("usbXhcdNextExtCapReg - uXpRegBase not set!\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    uOffsetPci = pHCDData->uPciCapPtr;

    do
        {
        /*
         * The bottom 2 bits of this offset are Reserved and must
         * be implemented as 00b although software must mask them
         * to allow for future uses of these bits.
         */

        uOffsetPci = (UINT8)(uOffsetPci & ~0x03);
        uOffsetPciNext = 0;

        /*
         * Read the config space using access functions
         */

        /* Get the Capability ID field */

        VXB_PCI_BUS_CFG_READ(pHCDData->pDev, (uOffsetPci + 0), 1, uCapIdPci);

        /* Get the offset of the next Standard PCI Capability */

        VXB_PCI_BUS_CFG_READ(pHCDData->pDev, (uOffsetPci + 1), 1, uOffsetPciNext);

        /* If we found the Capbility ID, return OK and its offset */

        if (uCapIdPci == uPciCapId)
            {
            *puActOffset = uOffsetPci;

            USB_XHCD_VDBG("usbXhcdLocatePciCap - found uPciCapId 0x%X at uOffset %d!\n",
                uPciCapId, uOffsetPci, 3, 4, 5, 6);

            return OK;
            }

        /* Go to the next PCI/PCIe Standard Capability */

        uOffsetPci = uOffsetPciNext;
        }
    while (uOffsetPci != 0);

    USB_XHCD_WARN("usbXhcdLocatePciCap - did not find expected uPciCapId 0x%X!\n",
        uPciCapId, 2, 3, 4, 5, 6);

    /* Reached the end but ID still not found, return ERROR */

    return ERROR;
    }

/*******************************************************************************
*
* usbXhcdEnableSSPorts - enable Supper Speed ports
*
* This routine is to enable Supper Speed ports in chip specific way.
*
* RETURNS: OK if the Capbility ID is found and the actual offset set in the
*          <puActOffset>, ERROR if ID not found.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdEnableSSPorts
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    if ((USB_XHCD_VENDOR_INTEL == pHCDData->uPciVID) &&
        ((USB_XHCD_DEVICE_INTEL_7_SERIES_PCH == pHCDData->uPciPID) ||
         (USB_XHCD_DEVICE_INTEL_8_SERIES_PCH == pHCDData->uPciPID) ||
         (USB_XHCD_DEVICE_INTEL_9_SERIES_PCH == pHCDData->uPciPID)))
        {
        UINT8  uOffsetPci = 0;
        UINT32 uData = 0xFFFFFFFF;


        uOffsetPci = USB_XHCI_INTEL_XUSB2PRM;

        /* Get the xHC USB 2.0 Port Routing Mask Register value */

        VXB_PCI_BUS_CFG_READ(pHCDData->pDev, uOffsetPci, 4, uData);

        USB_XHCD_DBG("usbXhcdEnableSSPorts - "
            "xHC USB 2.0 Port Routing Mask Register = %x\n",
            uData, 2, 3, 4, 5, 6);

        uOffsetPci = USB_XHCI_INTEL_USB3PRM;

        /* Get the xHC USB 3.0 Port Routing Mask Register value */

        VXB_PCI_BUS_CFG_READ(pHCDData->pDev, uOffsetPci, 4, uData);

        USB_XHCD_DBG("usbXhcdEnableSSPorts - "
            "USB 3.0 Port Routing Mask Register = %x\n",
            uData, 2, 3, 4, 5, 6);

        uOffsetPci = USB_XHCI_INTEL_USB3_PSSEN;

        uData = 0xFFFFFFFF;

        /* Get the xHC USB 3.0 Port SuperSpeed Enable Register value */

        VXB_PCI_BUS_CFG_WRITE(pHCDData->pDev, uOffsetPci, 4, uData);
        VXB_PCI_BUS_CFG_READ(pHCDData->pDev, uOffsetPci, 4, uData);

        USB_XHCD_DBG("usbXhcdEnableSSPorts - "
            "USB 3.0 Port SuperSpeed Enable Register = %x\n",
            uData, 2, 3, 4, 5, 6);

        uOffsetPci = USB_XHCI_INTEL_XUSB2PR;

        uData = 0xFFFFFFFF;

        /* Get the xHC USB 2.0 Port Routing Register value */

        VXB_PCI_BUS_CFG_WRITE(pHCDData->pDev, uOffsetPci, 4, uData);
        VXB_PCI_BUS_CFG_READ(pHCDData->pDev, uOffsetPci, 4, uData);

        USB_XHCD_DBG("usbXhcdEnableSSPorts - "
            "xHC USB 2.0 Port Routing Register = %x\n",
            uData, 2, 3, 4, 5, 6);
        }

    return OK;
    }

/*******************************************************************************
*
* usbXhcdHandleOffPreOS - handle off from Pre-OS (BIOS) and take control of xHCI
*
* This routine is to handle off from Pre-OS (BIOS) and take control of xHCI.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdHandleOffPreOS
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    /* The USB Legacy Support Capability (USBLEGSUP) offset */

    UINT32 uLegacySupportOffset = 0;

    /* USB Legacy register value */

    UINT32 uLegacy;

    /*
     * If there is no xHCI Extended Capability, return OK
     * (no handle off needed).
     */

    if (!pHCDData->uXpRegBase)
        {
        USB_XHCD_DBG("usbXhcdHandleOffPreOS - "
            "No xHCI Extended Capability, no need to handle off BIOS!\n",
            1, 2, 3, 4, 5, 6);

        return OK;
        }

    /* Go to the USB Legacy Support Capability */

    if (usbXhcdLocateExtCap(pHCDData,
        0, USB_XHCI_EXT_CAP_LEGACY, &uLegacySupportOffset) != OK)
        {
        USB_XHCD_DBG("usbXhcdHandleOffPreOS - "
            "No USB Legacy Support Capability, no need to handle off BIOS!\n",
            1, 2, 3, 4, 5, 6);

        return OK;
        }

    /* Read the USB Legacy Support Capability (USBLEGSUP) Register */

    uLegacy = USB_XHCD_READ_XP_REG32(pHCDData,
                (uLegacySupportOffset + USB_XHCI_USBLEGSUP));

    USB_XHCD_WARN("usbXhcdHandleOffPreOS - uLegacy = 0x%X!\n",
        uLegacy, 2, 3, 4, 5, 6);

    /*
     * If the BIOS has control over the xHCI, tell it that
     * we want take the control over, and then wait until
     * it release the contorl.
     */

    if ((uLegacy & USB_XHCI_HC_BIOS_OWNED) ||
        (!(uLegacy & USB_XHCI_HC_OS_OWNED)))
        {
        /* Timeout counter */

        UINT32 uCount = 0;

        USB_XHCD_WARN("usbXhcdHandleOffPreOS - xHCI ownded by BIOS, taking over!\n",
            1, 2, 3, 4, 5, 6);

        /* Set the OS Owned bit */

        uLegacy |= USB_XHCI_HC_OS_OWNED;

        /* Write the USB Legacy Support Capability (USBLEGSUP) Register */

        USB_XHCD_WRITE_XP_REG32(pHCDData,
            (uLegacySupportOffset + USB_XHCI_USBLEGSUP), uLegacy);

        /* Clear the counter to zero */

        uCount = 0;

        /* Wait until the HC is halted or error or timeout */

        while (TRUE)
            {
            /* Read the USB Legacy Support Capability (USBLEGSUP) Register */

            uLegacy = USB_XHCD_READ_XP_REG32(pHCDData,
                (uLegacySupportOffset + USB_XHCI_USBLEGSUP));

            /* If the OS Owned bit is set, we now own the xHCI */

            if (uLegacy & USB_XHCI_HC_OS_OWNED)
                break;

            /* Check if we have run out of time */

            if ((uCount ++) > USB_XHCI_PREOS_HANDLE_OFF_WAIT_MS)
                {
                USB_XHCD_WARN("usbXhcdHandleOffPreOS - "
                    "Timeout trying to handle off the xHC from BIOS!\n"
                    "Trying to take it over by force!",
                    1, 2, 3, 4, 5, 6);

                /* Set the OS Owned bit */

                uLegacy |= USB_XHCI_HC_OS_OWNED;

                /* Clear the BIOS Owned bit */

                uLegacy &= ~(USB_XHCI_HC_BIOS_OWNED);

                /* Force to take over from BIOS */

                USB_XHCD_WRITE_XP_REG32(pHCDData,
                    uLegacySupportOffset, uLegacy);

                break;
                }

            /* Delay 1 MS - the smallest common time delay that we can make */

            OS_DELAY_MS(1);
            }
        }

    USB_XHCD_WARN("usbXhcdHandleOffPreOS - xHCI ownded by OS now!\n",
        1, 2, 3, 4, 5, 6);

    /* Read USB Legacy Support Control/Status (USBLEGCTLSTS) Register */

    uLegacy = USB_XHCD_READ_XP_REG32(pHCDData,
        (uLegacySupportOffset + USB_XHCI_USBLEGCTLSTS));

    /* Disable all SMI generation */

    uLegacy &= ~(USB_XHCI_USBLEGCTLSTS_SMI_EN_MASK |
                 USB_XHCI_USBLEGCTLSTS_RsvdZ_MASK);

    /* Write USB Legacy Support Control/Status (USBLEGCTLSTS) Register */

    USB_XHCD_WRITE_XP_REG32(pHCDData,
        (uLegacySupportOffset + USB_XHCI_USBLEGCTLSTS), uLegacy);

    /* Enable all xHCI SuperSpeed ports */

    (void) usbXhcdEnableSSPorts(pHCDData);

    return OK;
    }

/*******************************************************************************
*
* usbXhcdHardwareParamInit - get basic hardware information
*
* This routine is to get basic hardware infomation.
*
* Note: On entry to this routine we should have already setup the vxBus
* facilities and the Host Controller Capability Registers base address
* should have been saved. This routine will read the Capability Registers
* to setup some hardware parameters, including the base addresses to other
* register spaces such as the Runtime registers.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdHardwareParamInit
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    /* Value of a 32 bit register */

    UINT32  uReg32 = 0;

    /* The page shift bit */

    UINT32  uPageShift = 0;

    /* VxBus representation of this xHCI instance */

    VXB_DEVICE_ID   pDev = pHCDData->pDev;

    /* Check if the parameter is valid */

    if (pDev == NULL)
        {
        USB_XHCD_ERR("usbXhcdHardwareParamInit - pDev NULL\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * If this is PCI/PCIe based host controller,
     * do PCI related parameter initialization.
     */

    if (pDev->busID == VXB_BUSID_PCI)
        {
        /*
         * Read the config space using access functions
         */

        VXB_PCI_BUS_CFG_READ(pDev,
            USB_XHCI_PCI_VID_PID_OFFSET,
            USB_XHCI_PCI_VID_PID_SIZE,
            uReg32);

        /* Save the PCI VID */

        pHCDData->uPciVID = (UINT16)USB_XHCI_PCI_VID_GET(uReg32);

        /* Save the PCI PID */

        pHCDData->uPciPID = (UINT16)USB_XHCI_PCI_PID_GET(uReg32);

        /*
         * Read the config space using access functions
         */

        VXB_PCI_BUS_CFG_READ(pDev,
            USB_XHCI_PCI_CAP_PTR_OFFSET,
            USB_XHCI_PCI_CAP_PTR_SIZE,
            uReg32);

        /* Save the Capabilities Pointer for PCI Standard Capabilities */

        pHCDData->uPciCapPtr = (UINT8)USB_XHCI_PCI_CAP_PTR_GET(uReg32);

        /* Locate the PCI Power Management Interface Capability */

        if (usbXhcdLocatePciCap(pHCDData,
                                USB_XHCI_PCI_CAP_ID_PCIPM,
                                &pHCDData->uPmCapPtr) != OK)
            {
            pHCDData->uPmCapPtr = 0;
            }
        else
            {
            pHCDData->pciPmcsr = (UINT16)(pHCDData->uPmCapPtr + 4);
            }

        /* Locate the MSI Capability */

        if (usbXhcdLocatePciCap(pHCDData,
                                USB_XHCI_PCI_CAP_ID_MSI,
                                &pHCDData->uMsiCapPtr) != OK)
            {
            pHCDData->uMsiCapPtr = 0;
            }

        /* Locate the MSI-X Capability */

        if (usbXhcdLocatePciCap(pHCDData,
                                USB_XHCI_PCI_CAP_ID_MSIx,
                                &pHCDData->uMsixCapPtr) != OK)
            {
            pHCDData->uMsixCapPtr = 0;
            }

        /* Locate the PCIe Capability */

        if (usbXhcdLocatePciCap(pHCDData,
                                USB_XHCI_PCI_CAP_ID_PCIe,
                                &pHCDData->uPcieCapPtr) != OK)
            {
            pHCDData->uPcieCapPtr = 0;
            }
        }

    /* Read the HCSPARAMS1 register */

    pHCDData->uHCSPARAMS1 = USB_XHCD_READ_CP_REG32(pHCDData,
        USB_XHCI_HCSPARAMS1);

    /* Read the HCSPARAMS2 register */

    pHCDData->uHCSPARAMS2 = USB_XHCD_READ_CP_REG32(pHCDData,
        USB_XHCI_HCSPARAMS2);

    /* Read the HCSPARAMS3 register */

    pHCDData->uHCSPARAMS3 = USB_XHCD_READ_CP_REG32(pHCDData,
        USB_XHCI_HCSPARAMS3);

    /* Read the HCCPARAMS register */

    pHCDData->uHCCPARAMS = USB_XHCD_READ_CP_REG32(pHCDData,
        USB_XHCI_HCCPARAMS);

    /* Read the CAPLENGTH register */

    uReg32 = USB_XHCD_READ_CP_REG32(pHCDData, USB_XHCI_CAPLENGTH);

    /* Save the HCI version */

    pHCDData->uHCIVERSION = (UINT16)USB_XHCI_HCI_VERSION(uReg32);

    /* Save the Capbility registers length */

    pHCDData->uCAPLENGTH = (UINT8)USB_XHCI_CAP_LENGTH(uReg32);

    /* Save the max number of device slots supported */

    pHCDData->uMaxSlots = (UINT8)USB_XHCI_MAX_SLOTS(pHCDData->uHCSPARAMS1);

    /* Save the max number of interrupters supported */

    pHCDData->uMaxIntrs = (UINT16)USB_XHCI_MAX_INTRS(pHCDData->uHCSPARAMS1);

    /* Save the max number of root hub ports supported */

    pHCDData->uMaxPorts = (UINT8)USB_XHCI_MAX_PORTS(pHCDData->uHCSPARAMS1);

    /* Save the max number of Scratchpad Buffers supported */

    pHCDData->uMaxSPBs = USB_XHCI_MAX_SCRATCHPAD_BUFFS(pHCDData->uHCSPARAMS2);

    /* Save the max number of Event Ring Segment Table entries supported */

    pHCDData->uMaxERSTEs = USB_XHCI_MAX_ERST_ENTRIES(pHCDData->uHCSPARAMS2);

    if (pHCDData->uMaxERSTEs > USB_XHCI_ERST_MAX_ENTRIES)
        {
        USB_XHCD_WARN("usbXhcdHardwareParamInit - limit uMaxERSTEs %d to %d\n",
            pHCDData->uMaxERSTEs, USB_XHCI_ERST_MAX_ENTRIES, 3, 4, 5, 6);

        pHCDData->uMaxERSTEs = USB_XHCI_ERST_MAX_ENTRIES;
        }

    /* Save the Operational Base */

    pHCDData->uOpRegBase = pHCDData->uCpRegBase + pHCDData->uCAPLENGTH;

    /* Read the RTSOFF register */

    uReg32 = USB_XHCD_READ_CP_REG32(pHCDData, USB_XHCI_RTSOFF);

    /* Save the Runtime Base */

    pHCDData->uRtRegBase = pHCDData->uCpRegBase + USB_XHCI_RTS_OFFSET(uReg32);

    /* Read the DBOFF register */

    uReg32 = USB_XHCD_READ_CP_REG32(pHCDData, USB_XHCI_DBOFF);

    /* Save the Doorbell Base */

    pHCDData->uDbRegBase = pHCDData->uCpRegBase + USB_XHCI_DB_OFFSET(uReg32);

    /* Check if there is xHCI Extended Capabilities Pointer (xECP) */

    if ((uReg32 = USB_XHCI_XECP(pHCDData->uHCCPARAMS)) != 0)
        {
        /*
         * Save the xECP Base - The value of this field indicates
         * a relative offset, in 32-bit words, from Base to
         * the beginning of the first extended capability.
         */

        pHCDData->uXpRegBase = pHCDData->uCpRegBase + (uReg32 << 2);
        }
    else
        {
        /* No xHCI Extended Capabilities */

        pHCDData->uXpRegBase = 0;
        }

    /*
     * Context Size (CSZ). If this bit is set to '1', then the xHC
     * uses 64 byte Context data structures. If this bit is cleared
     * to '0', then the xHC uses 32 byte Context data structures.
     *
     * Note: This flag does not apply to Stream Contexts.
     */

    /* Save the context structure size supported */

    pHCDData->uCSZ64 = (UINT8)USB_XHCI_CSZ(pHCDData->uHCCPARAMS);

    /* Save the context max size to be used */

    pHCDData->uCtxArraySize = pHCDData->uCSZ64 ?
        USB_XHCI_CTX_MAX_SIZE64 : USB_XHCI_CTX_MAX_SIZE32;

    pHCDData->uCtxSize = pHCDData->uCSZ64 ? 64 : 32;

    /* Save the flag indicating HC supportting 64 bit addressing or not */

    pHCDData->uAC64 = (UINT8)USB_XHCI_AC64(pHCDData->uHCCPARAMS);

    /* Read the PAGESIZE register */

    uReg32 = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_PAGESIZE);

    /* Scan through the low 16 bits for the 1st bit */

    for (uPageShift = 0; uPageShift < 16; uPageShift ++)
        {
        /*
         * This xHC supports a page size of 2^(n+12) if bit n is Set.
         *
         * If a bit is set, then the bit corresponds to a page
         * size that can be supported by this xHCI implementation.
         * We find the first bit that is set (that is the smallest
         * page size that can be supported) to use as the alignment
         * limit for some data structures.
         */

        if ((uReg32 & (1 << uPageShift)))
            break;
        }

    /* Set the smallest page size */

    if (uPageShift < 16)
        {
        pHCDData->uPageSize = (1 << (uPageShift + 12));
        }
    else
        {
        pHCDData->uPageSize = USB_XHCI_DEFAULT_PAGESIZE;
        }

    USB_XHCD_VDBG("usbXhcdHardwareParamInit - USB_XHCI_PAGESIZE %p uPageSize %p\n",
        uReg32, pHCDData->uPageSize, 3, 4, 5, 6);

    return OK;
    }

#if defined (_WRS_CONFIG_USB_XHCD_ENABLE_MSI_INTR) || \
    defined (_WRS_CONFIG_USB_XHCD_ENABLE_MSIX_INTR)
/*******************************************************************************
*
* usbXhcdMsgIntrSetup - setup xHCI MSI-X or MSI interrupt mechanism
*
* This routine is to setup xHCI MSI-X or MSI interrupt mechanism if the
* xHCI support it and it is enabled by the user configuration.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdMsgIntrSetup
    (
    pUSB_XHCD_DATA  pHCDData,
    UINT32          uMsgIntrType
    )
    {
    UINT16                  uCMD = 0;
    VXB_DEVICE_ID           busCtrlID = vxbDevParent(pHCDData->pDev);
    struct vxbPciDevice *   pPciDevice = pHCDData->pDev->pBusSpecificDevInfo;
    UINT8                   uMsixOffset = 0;
    UINT8                   uPciCap;
    UINT32                  uMaxMsgs;
    UINT32                  uVecType;

    /* Check message interrupt type */

    if (uMsgIntrType == USB_XHCD_INTR_TYPE_MSIX)
        {
        uPciCap = PCI_EXT_CAP_MSIX;
        uMaxMsgs = USB_XHCD_MAX_MSIX;
        uVecType = VXB_INT_PCI_MSIX;
        }
    else if (uMsgIntrType == USB_XHCD_INTR_TYPE_MSI)
        {
        uPciCap = PCI_EXT_CAP_MSI;
        uMaxMsgs = USB_XHCD_MAX_MSI;
        uVecType = VXB_INT_PCI_MSI;
        }
    else
        {
        USB_XHCD_ERR("usbXhcdMsgIntrSetup - wrong uMsgIntrType %d\n",
            uMsgIntrType, 2, 3, 4, 5, 6);
        return ERROR;
        }

    /*
     * We need to make sure we have the required capability in the PCI
     * Extended Capability space.
     */

    if (vxbPciConfigExtCapFind (busCtrlID, uPciCap,
                                pPciDevice->pciBus,
                                pPciDevice->pciDev,
                                pPciDevice->pciFunc,
                                &uMsixOffset) == OK)
        {
        UINT32 uIndex;
        UINT32 getVecCnt;
        struct vxbIntDynaVecInfo dynaVecs[USB_XHCD_MAX_MSGS];

        bzero((char*)&dynaVecs[0],
              USB_XHCD_MAX_MSGS * sizeof(struct vxbIntDynaVecInfo));

        /* Connect the ISRs using vxBus Dynamic interrupt library */

        for (uIndex = 0; uIndex < uMaxMsgs; uIndex++)
            {
            dynaVecs[uIndex].index   = uIndex;
            dynaVecs[uIndex].isr     = (VOIDFUNCPTR)usbXhcdISR;
            dynaVecs[uIndex].pArg    = pHCDData->ppEvtRings[uIndex];
            dynaVecs[uIndex].vecType = uVecType;
            }

        USB_XHCD_WARN("usbXhcdMsgIntrSetup - Connecting %s (%d vectors)!\n",
            (uMsgIntrType == USB_XHCD_INTR_TYPE_MSIX) ? "MSI-X" : "MSI",
            uMaxMsgs, 3, 4, 5, 6);

        if ((getVecCnt = vxbIntAlloc (pHCDData->pDev, uVecType, uMaxMsgs)) < uMaxMsgs)
            {
            USB_XHCD_WARN("usbXhcdMsgIntrSetup - vxbIntAlloc fail\n",
                1, 2, 3, 4, 5, 6);
            if (getVecCnt > 0);
                vxbIntFree (pHCDData->pDev);
            return ERROR;
            }

        for (uIndex = 0; uIndex < uMaxMsgs; uIndex ++)
            {
            if (vxbIntConnect(pHCDData->pDev, uIndex, dynaVecs[uIndex].isr,
                dynaVecs[uIndex].pArg) != OK)
                {
                USB_XHCD_WARN("usbXhcdMsgIntrSetup - vxbIntConnect fail\n",
                    1, 2, 3, 4, 5, 6);

                return ERROR;
                }
            }

        /*
         * Read the config space using access functions
         */

        VXB_PCI_BUS_CFG_READ(pHCDData->pDev,
            USB_XHCI_PCI_CMD_OFFSET,
            USB_XHCI_PCI_CMD_SIZE,
            uCMD);

        /* If MSI/MSI-x is used, we have to disable INTx */

        if (!(uCMD & USB_XHCI_PCI_CMD_INTx_DISABLE))
            {
            uCMD |= USB_XHCI_PCI_CMD_INTx_DISABLE;

            VXB_PCI_BUS_CFG_WRITE(pHCDData->pDev,
                USB_XHCI_PCI_CMD_OFFSET,
                USB_XHCI_PCI_CMD_SIZE,
                uCMD);
            }

        for (uIndex = 0; uIndex < uMaxMsgs; uIndex++)
            {
            /* Enable the interrupts */

            if (vxbIntEnable (pHCDData->pDev,
                              uIndex,
                              usbXhcdISR,
                              pHCDData->ppEvtRings[uIndex]) != OK)
                {
                USB_XHCD_ERR("usbXhcdMsgIntrSetup - vxbIntEnable fail\n",
                    1, 2, 3, 4, 5, 6);

                return ERROR;
                }
            }

        pHCDData->uIntrType = uMsgIntrType;

        USB_XHCD_WARN("usbXhcdMsgIntrSetup - %s enabled!\n",
            ((uMsgIntrType == USB_XHCD_INTR_TYPE_MSIX) ? "MSI-X" : "MSI"),
            2, 3, 4, 5, 6);

        return OK;
        }
    else
        {
        USB_XHCD_WARN("usbXhcdMsgIntrSetup - NOT using %s!\n",
            ((uMsgIntrType == USB_XHCD_INTR_TYPE_MSIX) ? "MSI-X" : "MSI"),
            2, 3, 4, 5, 6);

        return ERROR;
        }
    }

/*******************************************************************************
*
* usbXhcdMsgIntrUnSetup - setup xHCI MSI-X or MSI interrupt mechanism
*
* This routine is to setup xHCI MSI-X or MSI interrupt mechanism if the
* xHCI support it and it is enabled by the user configuration.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdMsgIntrUnSetup
    (
    pUSB_XHCD_DATA  pHCDData,
    UINT32          uMsgIntrType
    )
    {
    UINT16                  uCMD = 0;
    VXB_DEVICE_ID           busCtrlID = vxbDevParent(pHCDData->pDev);
    struct vxbPciDevice *   pPciDevice = pHCDData->pDev->pBusSpecificDevInfo;
    UINT8                   uMsixOffset = 0;
    UINT8                   uPciCap;
    UINT32                  uMaxMsgs;
    UINT32                  uVecType;

    /* Check message interrupt type */

    if (uMsgIntrType == USB_XHCD_INTR_TYPE_MSIX)
        {
        uPciCap = PCI_EXT_CAP_MSIX;
        uMaxMsgs = USB_XHCD_MAX_MSIX;
        uVecType = VXB_INT_PCI_MSIX;
        }
    else if (uMsgIntrType == USB_XHCD_INTR_TYPE_MSI)
        {
        uPciCap = PCI_EXT_CAP_MSI;
        uMaxMsgs = USB_XHCD_MAX_MSI;
        uVecType = VXB_INT_PCI_MSI;
        }
    else
        {
        USB_XHCD_ERR("usbXhcdMsgIntrSetup - wrong uMsgIntrType %d\n",
            uMsgIntrType, 2, 3, 4, 5, 6);
        return ERROR;
        }

    if (vxbPciConfigExtCapFind (busCtrlID, uPciCap,
                                pPciDevice->pciBus,
                                pPciDevice->pciDev,
                                pPciDevice->pciFunc,
                                &uMsixOffset) == OK)
        {
        UINT32 uIndex;
        struct vxbIntDynaVecInfo dynaVecs[USB_XHCD_MAX_MSGS];

        for (uIndex = 0; uIndex < uMaxMsgs; uIndex++)
            {
            /* Disable the interrupts */

            if (vxbIntDisable (pHCDData->pDev,
                              uIndex,
                              usbXhcdISR,
                              pHCDData->ppEvtRings[uIndex]) != OK)
                {
                USB_XHCD_ERR("usbXhcdMsgIntrSetup - vxbIntEnable fail\n",
                    1, 2, 3, 4, 5, 6);

                return ERROR;
                }
            }

        bzero((char*)&dynaVecs[0],
              USB_XHCD_MAX_MSGS * sizeof(struct vxbIntDynaVecInfo));

        for (uIndex = 0; uIndex < uMaxMsgs; uIndex++)
            {
            dynaVecs[uIndex].index   = uIndex;
            dynaVecs[uIndex].isr     = (VOIDFUNCPTR)usbXhcdISR;
            dynaVecs[uIndex].pArg    = pHCDData->ppEvtRings[uIndex];
            dynaVecs[uIndex].vecType = uVecType;
            }

        USB_XHCD_WARN("usbXhcdMsgIntrSetup - Connecting %s (%d vectors)!\n",
            (uMsgIntrType == USB_XHCD_INTR_TYPE_MSIX) ? "MSI-X" : "MSI",
            uMaxMsgs, 3, 4, 5, 6);

        for (uIndex = 0; uIndex < uMaxMsgs; uIndex++)
            {
            if (vxbIntDisconnect(pHCDData->pDev, uIndex, dynaVecs[uIndex].isr,
                dynaVecs[uIndex].pArg) != OK)
                {
                USB_XHCD_WARN("usbXhcdMsgIntrSetup - vxbIntDisConnect fail\n",
                    1, 2, 3, 4, 5, 6);
                return ERROR;
                }
            }

        /* Release allocated MSI interrupt vectors */

        vxbIntFree (pHCDData->pDev);

        /*
         * Read the config space using access functions
         */

        VXB_PCI_BUS_CFG_READ(pHCDData->pDev,
            USB_XHCI_PCI_CMD_OFFSET,
            USB_XHCI_PCI_CMD_SIZE,
            uCMD);

        if (uCMD & USB_XHCI_PCI_CMD_INTx_DISABLE)
            {
            uCMD &= ~(USB_XHCI_PCI_CMD_INTx_DISABLE);

            VXB_PCI_BUS_CFG_WRITE(pHCDData->pDev,
                USB_XHCI_PCI_CMD_OFFSET,
                USB_XHCI_PCI_CMD_SIZE,
                uCMD);
            }

        USB_XHCD_WARN("usbXhcdMsgIntrSetup - %s disabled!\n",
            ((uMsgIntrType == USB_XHCD_INTR_TYPE_MSIX) ? "MSI-X" : "MSI"),
            2, 3, 4, 5, 6);

        return OK;
        }
    else
        {
        USB_XHCD_WARN("usbXhcdMsgIntrSetup - NOT using %s!\n",
            ((uMsgIntrType == USB_XHCD_INTR_TYPE_MSIX) ? "MSI-X" : "MSI"),
            2, 3, 4, 5, 6);

        return ERROR;
        }
    }

#endif /* defined (_WRS_CONFIG_USB_XHCD_ENABLE_MSI_INTR) ... */

/*******************************************************************************
*
* usbXhcdIntxIntrSetup - setup xHCI INTx interrupt mechanism
*
* This routine is to setup xHCI INTx interrupt mechanism if the xHCI support
* INTx and if INTx is enabled by the user configuration.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/
#if 0

STATUS usbXhcdIntxIntrSetup
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    pVOID pParam = pHCDData->ppEvtRings[0];

    /* Check whether the interrupt handler was registered successfully */

    if (vxbIntConnect (pHCDData->pDev, 0, usbXhcdISR, pParam) == ERROR)
        {
        USB_XHCD_ERR("usbXhcdIntxIntrSetup - vxbIntConnect fail\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Enable the interrupts */

    if (vxbIntEnable (pHCDData->pDev, 0, usbXhcdISR, pParam) == ERROR)
        {
        USB_XHCD_ERR("usbXhcdIntxIntrSetup - vxbIntEnable fail\n",
            1, 2, 3, 4, 5, 6);

#else
extern int vxbDevGetPciIntLine (VXB_DEVICE_ID dev);
extern int vxbDevGetPciIntVector (VXB_DEVICE_ID dev);
#ifdef __ENABLE_MSI__
IMPORT s32 its_request_irq(void *);
IMPORT void * its_create_pci_msi_device(s32,s32,s32);
IMPORT void * its_irq_compose_msi_msg(void * ,s32);
IMPORT void its_release_msi_msg(void *);
IMPORT void pci_config_msi_data(s32 busno,s32 devno,s32 funcno,void * msi_data);
extern void	lpi_install_handler(s32,void *,void *);
extern void lpi_enable_int(s32);
#else
#include <irq.h>
#if defined(FT2000AHK)
extern unsigned char board_find_pci_irq(int busno);
#endif
#endif


STATUS usbXhcdIntxIntrSetup
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    PCI_HARDWARE * pPciDev;
    pPciDev = (PCI_HARDWARE *)pHCDData->pDev->pBusSpecificDevInfo;
    pVOID pParam = pHCDData->ppEvtRings[0];
    int irq=0,result ;
	void * msi_device;
	void * msi_msg;
    /* Check whether the interrupt handler was registered successfully */

#ifdef __ENABLE_MSI__
	//printk("File:%s,Line:%d,[%d.%d.%d]\n",__FILE__,__LINE__,pPciDev->pciBus,pPciDev->pciDev,pPciDev->pciFunc);
	msi_device = its_create_pci_msi_device(pPciDev->pciBus,pPciDev->pciDev,pPciDev->pciFunc);			
	if(NULL!=msi_device)
	{
		irq = its_request_irq(msi_device);		
		msi_msg = its_irq_compose_msi_msg(msi_device,irq);
		if(NULL!=msi_msg)
		{
			pci_config_msi_data(pPciDev->pciBus,pPciDev->pciDev,pPciDev->pciFunc,msi_msg);
			its_release_msi_msg(msi_msg);
		}
	}
	lpi_install_handler(irq,usbXhcdISR, pParam);
	lpi_enable_int(irq);
#elif defined(FT2000AHK)
	irq = board_find_pci_irq(pPciDev->pciBus);
	int_install_handler("usb_intr",irq,0,usbXhcdISR, pParam);
	int_enable_pic(irq);
#endif
#if 0
//    printk("%s:%d,irq=%d\n",__FUNCTION__,__LINE__,irq);
	 result = shared_int_install(irq,usbXhcdISR, pParam);
      if (result == ERROR)
         {
       	// printk("usbXhcdIntxIntrSetup: connect interrupt ERROR !\n");    
         	return ERROR;
         }   
    /* Enable the interrupts */


    if (int_enable_pic (irq) == ERROR)
	{
	USB_XHCD_ERR("usbXhcdIntxIntrSetup - vxbIntEnable fail\n",
		1, 2, 3, 4, 5, 6);
	//printk("%s:%d fail enable int\n",__FUNCTION__,__LINE__);
	return ERROR;
	}
	    // printk("[%s][%d],irq=%d\n", __FUNCTION__, __LINE__,irq);     
 
    USB_XHCD_WARN("usbXhcdIntxIntrSetup - INTx enabled!\n",
        1, 2, 3, 4, 5, 6);
#endif
    pHCDData->uIntrType = USB_XHCD_INTR_TYPE_INTX;

    return OK;
    }
#endif

/*******************************************************************************
*
* usbXhcdIntxIntrUnSetup - unsetup xHCI INTx interrupt mechanism
*
* This routine is to unsetup xHCI INTx interrupt mechanism if the xHCI support
* INTx and if INTx is enabled by the user configuration.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdIntxIntrUnSetup
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    pVOID pParam = pHCDData->ppEvtRings[0];

    /* Disable the interrupts */

    if (vxbIntDisable (pHCDData->pDev, 0, usbXhcdISR, pParam) == ERROR)
        {
        USB_XHCD_ERR("usbXhcdIntxIntrUnSetup - vxbIntDisable fail\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Check whether the interrupt handler was unregistered successfully */

    if (vxbIntDisconnect (pHCDData->pDev, 0, usbXhcdISR, pParam) == ERROR)
        {
        USB_XHCD_ERR("usbXhcdIntxIntrUnSetup - vxbIntDisconnect fail\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    USB_XHCD_WARN("usbXhcdIntxIntrUnSetup - INTx disabled!\n",
        1, 2, 3, 4, 5, 6);

    return OK;
    }

/*******************************************************************************
*
* usbXhcdInterruptSetup - setup xHCI interrupt mechanism
*
* This routine is to setup xHCI interrupt mechanism.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdInterruptSetup
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    UINT32 uIndex;
    UINT32 uReg32;
    UINT64 uERSTBA;
    UINT64 uReg64;

#ifndef _WRS_CONFIG_USB_XHCD_POLLING_MODE
    STATUS uSts = ERROR;

#ifdef _WRS_CONFIG_USB_XHCD_ENABLE_MSIX_INTR

    /* Try to setup MSI-X */

    if ((uSts = usbXhcdMsgIntrSetup(pHCDData, USB_XHCD_INTR_TYPE_MSIX)) != OK)
        {
        USB_XHCD_WARN("usbXhcdInterruptSetup - MSI-X interrupt not setup!\n",
            1, 2, 3, 4, 5, 6);
        }
#endif /* _WRS_CONFIG_USB_XHCD_ENABLE_MSIX_INTR */

#ifdef _WRS_CONFIG_USB_XHCD_ENABLE_MSI_INTR

    /* If MSI-X fails to setup, try MSI */

    if ((uSts != OK) &&
        ((uSts = usbXhcdMsgIntrSetup(pHCDData, USB_XHCD_INTR_TYPE_MSI)) != OK))
        {
        USB_XHCD_WARN("usbXhcdInterruptSetup - MSI interrupt not setup!\n",
            1, 2, 3, 4, 5, 6);
        }
#endif /* _WRS_CONFIG_USB_XHCD_ENABLE_MSI_INTR */

    /* If MSI-X/MSI both fails to setup, try INTx */

    if ((uSts != OK) &&
        ((uSts = usbXhcdIntxIntrSetup(pHCDData)) != OK))
        {
        USB_XHCD_WARN("usbXhcdInterruptSetup - INTx interrupt not setup!\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

#endif /* _WRS_CONFIG_USB_XHCD_POLLING_MODE */

    /* Read the USBCMD register */

    uReg32 = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_USBCMD);

    /* Make sure we do not run it right now */

    uReg32 &= ~(USB_XHCI_USBCMD_RS);

    /*
     * Enable the xHCI interrupts!
     */

    uReg32 |= (USB_XHCI_USBCMD_INTR_MASK);

    /* Write the USBCMD register with updated value */

    USB_XHCD_WRITE_OP_REG32(pHCDData, USB_XHCI_USBCMD, uReg32);

    /* Get the base BUS address of the ERST array */

    uERSTBA = pHCDData->erstDmaMapId->fragList[0].frag;

    /* Setup the interrupters for the enabled event rings */

    for (uIndex = 0; uIndex < pHCDData->uNumEvtRings; uIndex++)
        {
        /* Get the allocated event ring */

        pUSB_XHCD_RING pEvtRing = pHCDData->ppEvtRings[uIndex];

        /* Read the ERSTSZ register */

        uReg32 = USB_XHCD_READ_RT_REG32(pHCDData, USB_XHCI_ERSTSZ(uIndex));

        /* Clear the filed to be filled */

        uReg32 &= ~(USB_XHCI_ERSTSZ_SIZE_MASK);

        /* Write the ERSTSZ register with proper number of segments */

        USB_XHCD_WRITE_RT_REG32(pHCDData,
            USB_XHCI_ERSTSZ(uIndex), uReg32 | pHCDData->uMaxERSTEs);

        /* Write the ERSTBA register with the BUS address of the ERST */

        USB_XHCD_WRITE_RT_REG64(pHCDData,
            USB_XHCI_ERSTBA(uIndex), uERSTBA);

        uReg64 = USB_XHCD_READ_RT_REG64(pHCDData, USB_XHCI_ERDP(uIndex));

        USB_XHCD_VDBG("usbXhcdInterruptSetup - "
            "Current ERDP[%d] %p ==> %p using ERST @%p[BUS]!\n",
            uIndex,
            uReg64,
           (pEvtRing->pHeadSeg->uHeadTRB | USB_XHCI_ERDP_EHB),
            uERSTBA, 5, 6);

        /* Write the ERDP register with the BUS address of the event ring */

        USB_XHCD_WRITE_RT_REG64(pHCDData,
            USB_XHCI_ERDP(uIndex),
            (pEvtRing->pHeadSeg->uHeadTRB));

        /* Get the BUS address of the next ERST for next event ring */

        uERSTBA += pHCDData->uERSTSize;

        /*
         * TODO: We can make it configurable, if too big, it will affect
         * performance.
         */

        uReg32 = USB_XHCI_IMOD_IMODI_DEFAULT;

        USB_XHCD_WRITE_RT_REG32(pHCDData,
            USB_XHCI_IMOD(uIndex), uReg32);

        /* Read the IMAN register */

        uReg32 = USB_XHCD_READ_RT_REG32(pHCDData, USB_XHCI_IMAN(uIndex));

        USB_XHCD_VDBG("usbXhcdInterruptSetup - Current IMAN[%d] %p ==> %p\n",
            uIndex,
            uReg32,
            (uReg32 | USB_XHCI_IMAN_IE), 4, 5, 6);

        uReg32 |= USB_XHCI_IMAN_IP;
        uReg32 |= USB_XHCI_IMAN_IE;

        /* Clear and pending interrupts and enable interrupts */

        USB_XHCD_WRITE_RT_REG32(pHCDData,
            USB_XHCI_IMAN(uIndex), uReg32);

        /*
         * Read the IMAN register again to guarantee that the write has been
         * flushed from posted buffers.
         */

        uReg32 = USB_XHCD_READ_RT_REG32(pHCDData, USB_XHCI_IMAN(uIndex));

        USB_XHCD_VDBG("usbXhcdInterruptSetup - NEW IMAN[%d] %p\n",
            uIndex,
            uReg32,
            3, 4, 5, 6);
        }

    return OK;
    }

/*******************************************************************************
*
* usbXhcdInterruptUnSetup - unsetup xHCI interrupt mechanism
*
* This routine is to setup xHCI interrupt mechanism.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdInterruptUnSetup
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
#ifndef _WRS_CONFIG_USB_XHCD_POLLING_MODE

    STATUS uSts = ERROR;

#if defined (_WRS_CONFIG_USB_XHCD_ENABLE_MSI_INTR) || \
    defined (_WRS_CONFIG_USB_XHCD_ENABLE_MSIX_INTR)
    if (pHCDData->uIntrType != USB_XHCD_INTR_TYPE_INTX)
        {
        /* Try to unsetup MSI-X */

        uSts = usbXhcdMsgIntrUnSetup(pHCDData, pHCDData->uIntrType);

        if (uSts != OK)
            {
            USB_XHCD_WARN("usbXhcdInterruptUnSetup - MSI-X interrupt not setup!\n",
                1, 2, 3, 4, 5, 6);
            }
        }
#endif /* _WRS_CONFIG_USB_XHCD_ENABLE_MSIX_INTR ||
          _WRS_CONFIG_USB_XHCD_ENABLE_MSIX_INTR */

    /* If MSI-X/MSI both fails to setup, try INTx */

    if ((uSts != OK) &&
        ((uSts = usbXhcdIntxIntrUnSetup(pHCDData)) != OK))
        {
        USB_XHCD_WARN("usbXhcdInterruptUnSetup - INTx interrupt not unsetup!\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

#endif /* _WRS_CONFIG_USB_XHCD_POLLING_MODE */

    return OK;
    }

/*******************************************************************************
*
* usbXhcdCmdRingDeqSet - set command ring dequeue pointer for the xHCI
*
* This routine is to set command ring dequeue pointer for the xHCI.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdCmdRingDeqSet
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    UINT64 uCRCR;

    (void) usbXhcdRingReset(pHCDData, pHCDData->pCmdRing);

    /* Read the Command Ring Control Register */

    uCRCR = USB_XHCD_READ_OP_REG64(pHCDData, USB_XHCI_CRCR);

    /* Write the Command Ring Control Register to stop the Command Ring */

    USB_XHCD_WRITE_OP_REG64(pHCDData, USB_XHCI_CRCR,
        (uCRCR | USB_XHCI_CRCR_CS | USB_XHCI_CRCR_CA));

    /* Read the Command Ring Control Register again */

    uCRCR = USB_XHCD_READ_OP_REG64(pHCDData, USB_XHCI_CRCR);

    if (uCRCR & USB_XHCI_CRCR_CRR)
        {
        USB_XHCD_WARN("usbXhcdHostCtlrStart - Command Ring Still Running!\n",
            1, 2, 3, 4, 5, 6);
        }

    USB_XHCD_VDBG("usbXhcdHostCtlrStart - Before programing CRCR, CRCR = %p"
        "(ignored some read 0 bits)!\n",
        uCRCR, 2, 3, 4, 5, 6);

    /* Set the Command Ring Pointer without affecting control bits */

    uCRCR = (uCRCR & USB_XHCI_CRCR_Rsvd_MASK) |
        (pHCDData->pCmdRing->pHeadSeg->uHeadTRB & USB_XHCI_CRCR_CRP_MASK) |
        (pHCDData->pCmdRing->uCycle);

    USB_XHCD_VDBG("usbXhcdHostCtlrStart - "
        "write CMD RING DEQ PTR @%p[BUS] @%p[CPU] into CRCR as %p\n",
        pHCDData->pCmdRing->pHeadSeg->uHeadTRB,
        pHCDData->pCmdRing->pHeadSeg->pTRBs,
        uCRCR, 0, 0, 0);

    /* Write the Command Ring Control Register with updated value */

    USB_XHCD_WRITE_OP_REG64(pHCDData, USB_XHCI_CRCR, uCRCR);

    /* Read the Command Ring Control Register again */

    uCRCR = USB_XHCD_READ_OP_REG64(pHCDData, USB_XHCI_CRCR);

    USB_XHCD_VDBG("usbXhcdHostCtlrStart - After programing CRCR, CRCR = %p"
        "(ignored some read 0 bits)!\n",
        uCRCR, 2, 3, 4, 5, 6);

    return OK;
    }

/*******************************************************************************
*
* usbXhcdHostCtlrWait - wait xHCI for Doorbell or Operational register write
*
* This routine is to wait the xHCI become ready for Doorbell or Operational
* register write access. After Chip Hardware Reset wait until the Controller
* Not Ready (CNR) flag in the USBSTS is '0' before writing any xHC Operational
* or Runtime registers.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdHostCtlrWait
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    /* Timeout counter */

    UINT32 uCount;

    /* USBSTS register value */

    UINT32 uSts;

    /*
     * Controller Not Ready (CNR) - RO. Default = '1'. '0' = Ready and
     * '1' = Not Ready. Software shall not write any Doorbell or Operational
     * register of the xHC, other than the USBSTS register, until CNR = '0'.
     * This flag is set by the xHC after a Chip Hardware Reset and cleared
     * when the xHC is ready to begin accepting register writes. This flag
     * shall remain cleared ('0') until the next Chip Hardware Reset.
     */

    /* Reset the counter */

    uCount = 0;

    /* Wait until the HC is halted or error or timeout */

    while (TRUE)
        {
        /* Get the status */

        uSts = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_USBSTS);

        /* If the Controller Not Ready bit is cleared, break and return OK */

        if (!(uSts & USB_XHCI_USBSTS_CNR))
            break;

        /* Check if we have run out of time */

        if ((uCount ++) > USB_XHCI_USBSTS_CNR_WAIT_MS)
            {
            USB_XHCD_ERR("usbXhcdHostCtlrWait - timeout trying to wait the xHC\n",
                1, 2, 3, 4, 5, 6);

            return ERROR;
            }

        /* Delay 1 MS - the smallest common time delay that we can make */

        OS_DELAY_MS(1);
        }

    return OK;
    }

/*******************************************************************************
*
* usbXhcdHostCtlrHalt - disable interrupts and halt the xHCI
*
* This routine is to disable interrupts and halt the xHCI.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdHostCtlrHalt
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    UINT32 uCmd;
    UINT32 uCount;
    UINT32 uSts;

    /* Read the USBCMD register */

    uCmd = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_USBCMD);

    /*
     * Clear all interrupts enabled and the Run/Stop bit
     */

    uCmd &= ~(USB_XHCI_USBCMD_INTR_MASK | USB_XHCI_USBCMD_RS);

    /*
     * Write the value back, which actually disables interrupts
     * and halt the HC.
     */

    USB_XHCD_WRITE_OP_REG32(pHCDData, USB_XHCI_USBCMD, uCmd);

    /*
     * When USB_XHCI_USBCMD_RS bit is cleared to '0', the xHC completes the
     * current and any actively pipelined transactions on the USB and then
     * halts. The xHC shall halt within 16 ms after software clears the
     * Run/Stop bit. The HCHalted (HCH) bit in the USBSTS register indicates
     * when the xHC has finished its pending pipelined transactions and has
     * entered the stopped state.
     */

    OS_BUSY_WAIT_US(USB_XHCI_HALT_MAX_WAIT_US);

    uCount = 0;

    /* Wait until the HC is halted or error or timeout */

    while (TRUE)
        {
        /* Get the status */

        uSts = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_USBSTS);

        /* If the HCHalted bit is set, break and return OK */

        if (uSts & USB_XHCI_USBSTS_HCH)
            break;

        /* Check if we have run out of time */

        if ((uCount ++) > USB_XHCI_HALT_MAX_WAIT_US)
            {
            USB_XHCD_WARN("usbXhcdHostCtlrHalt - timeout trying to halt the xHC\n",
                1, 2, 3, 4, 5, 6);

            return ERROR;
            }

        /* Read the USBCMD register */

        uCmd = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_USBCMD);

        USB_XHCD_DBG("usbXhcdHostCtlrHalt - "
            "xHCI still running with USBCMD 0x%X USBSTS 0x%X\n",
            uCmd, uSts, 3, 4, 5, 6);

        /* Delay 1 US - the smallest common time delay that we can make */

        OS_BUSY_WAIT_US(1);
        }

    return OK;
    }

/*******************************************************************************
*
* usbXhcdHostCtlrReset - reset the xHCI Host Controller
*
* This routine is to reset the xHCI. The Host Controller resets its internal
* pipelines, timers, counters, state machines, etc. to their initial value.
* Any transaction currently in progress on the USB is immediately terminated.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdHostCtlrReset
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    UINT32 uCmd;
    UINT32 uCount;

    /*
     * Make sure the xHCI is halted.
     *
     * Software shall not set Host Controller Reset (HCRST) bit to '1' when
     * the HCHalted (HCH) bit in the USBSTS register is a '0'. Attempting
     * to reset an actively running host controller will result in undefined
     * behavior.
     */

    if (usbXhcdHostCtlrHalt(pHCDData) != OK)
        {
        USB_XHCD_WARN("usbXhcdHostCtlrReset - usbXhcdHostCtlrHalt fail\n",
            1, 2, 3, 4, 5, 6);

        /* We could move on, since some controllers actually would work */
        }

    /* Read the USBCMD register */

    uCmd = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_USBCMD);

    /*
     * Set Host Controller Reset (HCRST) bit.
     */

    uCmd |= USB_XHCI_USBCMD_HCRST;

    /*
     * Write the value back, which actually resets the HC.
     */

    USB_XHCD_WRITE_OP_REG32(pHCDData, USB_XHCI_USBCMD, uCmd);

    /*
     * The Host Controller Reset (HCRST) bit is cleared to '0' by the
     * Host Controller when the reset process is complete. Software cannot
     * terminate the reset process early by writing a '0' to this bit
     * and shall not write any xHC Operational or Runtime registers while
     * HCRST is '1'.
     */

    uCount = 0;

    /* Wait until the HC reset is done or error or timeout */

    while (TRUE)
        {
        /* Get the status */

        uCmd = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_USBCMD);

        /*
         * If the Host Controller Reset (HCRST) bit is cleared, break
         * and return OK.
         */

        if (!(uCmd & USB_XHCI_USBCMD_HCRST))
            break;

        /* Check if we have run out of time */

        if ((uCount ++) > USB_XHCI_RESET_MAX_WAIT_US)
            {
            USB_XHCD_ERR("usbXhcdHostCtlrReset - timeout trying to reset the xHC\n",
                1, 2, 3, 4, 5, 6);

            return ERROR;
            }

        /* Delay 1 US - the smallest common time delay that we can make */

        OS_BUSY_WAIT_US(1);
        }

    return OK;
    }

/*******************************************************************************
*
* usbXhcdHostCtlrStart - start the xHCI Host Controller
*
* This routine is to start the xHCI. This is to perform the steps as stated in
* "4.2 Host Controller Initialization" of the xHCI specification.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdHostCtlrStart
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    /* Value of a 32 bit register */

    UINT32  uReg32;

    /* Value of a counter */

    UINT32  uCount;

    /* Wait until the xHCI hardware is ready for register write access */

    if (usbXhcdHostCtlrWait(pHCDData) != OK)
        {
        USB_XHCD_ERR("usbXhcdHostCtlrStart - wait xHCI hardware ready failed\n",
            0, 0, 0, 0, 0, 0);

        return ERROR;
        }

    /* Handle off the controller from Pre-OS software (BIOS) */

    if (usbXhcdHandleOffPreOS(pHCDData) != OK)
        {
        USB_XHCD_ERR("usbXhcdHostCtlrStart - Pre-OS handle off failed\n",
            0, 0, 0, 0, 0, 0);

        return ERROR;
        }

    /* Reset the xHCI hardware into quiescent state */

    if (usbXhcdHostCtlrReset(pHCDData) != OK)
        {
        USB_XHCD_ERR("usbXhcdHostCtlrStart - reset xHCI hardware failed\n",
            0, 0, 0, 0, 0, 0);

        return ERROR;
        }

    /* If anything needs to be done after the reset, do it now */

    if (pHCDData->pPostResetHook != NULL)
        {
        pHCDData->pPostResetHook();
        }

    /* Read the CONFIG register */

    uReg32 = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_CONFIG);

    /*
     * Fill Max Device Slots Enabled (MaxSlotsEn) to enable
     * all supported device slots.
     */

    uReg32 &= ~(USB_XHCI_CONFIG_MAX_SLOTS_ENABLED_MASK);
    uReg32 |= pHCDData->uMaxSlots;

    USB_XHCD_VDBG("usbXhcdHostCtlrStart - "
        "write uMaxSlots %d as MaxSlotsEn into CONFIG register 0x%X!\n",
        pHCDData->uMaxSlots,
        uReg32, 3, 4, 5, 6);

    /* Write the CONFIG register with updated value */

    USB_XHCD_WRITE_OP_REG32(pHCDData, USB_XHCI_CONFIG, uReg32);

    /* Setup the interrupt mechanism and enable interrupt delivery */

    if (usbXhcdInterruptSetup(pHCDData) != OK)
        {
        USB_XHCD_ERR("usbXhcdHostCtlrStart - set command ring DeqPtr failed\n",
            0, 0, 0, 0, 0, 0);

        return ERROR;
        }

    USB_XHCD_VDBG("usbXhcdHostCtlrStart - DCBAA @%p[BUS] @%p[CPU]!\n",
        pHCDData->dcbaaDmaMapId->fragList[0].frag,
        pHCDData->dcbaaDmaMapId->fragList[0].fragVirt, 3, 4, 5, 6);

    /* Write the DCBAAP register with the BUS address of the DCBAA */

    USB_XHCD_WRITE_OP_REG64(pHCDData,
        USB_XHCI_DCBAAP,
        pHCDData->dcbaaDmaMapId->fragList[0].frag);

    /* Set the command ring Dequeue Pointer */

    if (usbXhcdCmdRingDeqSet(pHCDData) != OK)
        {
        USB_XHCD_ERR("usbXhcdHostCtlrStart - set command ring DeqPtr failed\n",
            0, 0, 0, 0, 0, 0);

        return ERROR;
        }

    /* Power on all root ports */

    for (uCount = 0; uCount < pHCDData->uMaxPorts; uCount++)
        {
        UINT32 uPortSc;
        
        /*
         * Check PPC bit at first to decide to set Port Power.
         */

        if (USB_XHCI_PPC(pHCDData->uHCCPARAMS) == 1)
            {
            usbXhcdPortPower(pHCDData, uCount, FALSE);

            OS_DELAY_MS(100);

            usbXhcdPortPower(pHCDData, uCount, TRUE);
            }
        else /* simulate power cycle */
            {
            usbXhcdPortReset(pHCDData, uCount, TRUE);
            } 

        /* Read the PORTSC register */

        uPortSc = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_PORTSC(uCount));

        USB_XHCD_WARN("usbXhcdHostCtlrStart - "
            "Port %d is %s and %s at %s in %s [uPortSc=0x%08x]\n",
            (uCount + 1),
            (uPortSc & USB_XHCI_PORTSC_CCS) ? "Connected" : "Disconnected",
            (uPortSc & USB_XHCI_PORTSC_PED) ? "Enabled" : "Disabled",
            gUsbXhcdSpeedStr[USB_XHCI_PORTSC_PS_GET(uPortSc)],
            gUsbXhcdLinkStateStr[USB_XHCI_PORTSC_PLS_GET(uPortSc)], uPortSc);

        pHCDData->pRootPorts[uCount].uPortSc = uPortSc;
        }

    /* We can now respond to interrupts */

    pHCDData->uIsrMagic = USB_XHCD_MAGIC_ALIVE;

    USB_XHCD_DBG("usbXhcdHostCtlrStart - Done! xHCI should be running now!\n",
        1, 2, 3, 4, 5, 6);

    return OK;
    }

/*******************************************************************************
*
* usbXhcdHostCtlrStop - stop the xHCI Host Controller
*
* This routine is to stop the xHCI.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdHostCtlrStop
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    /* Reset the xHCI hardware into quiescent state */

    if (usbXhcdHostCtlrReset(pHCDData) != OK)
        {
        USB_XHCD_ERR("usbXhcdHostCtlrStop - reset xHCI hardware failed\n",
            0, 0, 0, 0, 0, 0);

        /* Even it fails, we need to continue to unsetup the interrupts */
        }

    /* If anything needs to be done after the reset, do it now */

    if (pHCDData->pPostResetHook != NULL)
        {
        pHCDData->pPostResetHook();
        }
        
    /* UnSetup the interrupt mechanism and disable interrupt delivery */

    (void) usbXhcdInterruptUnSetup(pHCDData);

    /* We can now stop respond to interrupts */

    pHCDData->uIsrMagic = USB_XHCD_MAGIC_DEAD;

    USB_XHCD_DBG("usbXhcdHostCtlrStop - Done! xHCI should be stopped now!\n",
        1, 2, 3, 4, 5, 6);

    return OK;
    }

/*******************************************************************************
*
* usbXhcdHardwareStart - start the xHCI Host Controller
*
* This routine is to start the xHCI.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdHardwareStart
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    UINT32 uReg32;
    UINT32 uCount;

    /* Read the USBCMD register */

    uReg32 = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_USBCMD);

    /*
     * Start the xHCI running!
     */

    uReg32 |= (USB_XHCI_USBCMD_RS);

    /* Write the USBCMD register with updated value */

    USB_XHCD_WRITE_OP_REG32(pHCDData, USB_XHCI_USBCMD, uReg32);

    uCount = 0;

    /* Wait until the HC is un-halted or error or timeout */

    while (TRUE)
        {
        /* Get the status */

        uReg32 = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_USBSTS);

        /* If the HCHalted bit is cleared, break and return OK */

        if (!(uReg32 & USB_XHCI_USBSTS_HCH))
            break;

        /* Check if we have run out of time */

        if ((uCount ++) > USB_XHCI_START_MAX_WAIT_MS)
            {
            USB_XHCD_WARN("usbXhcdHardwareStart - "
                "timeout trying to run the xHC, USBSTS = 0x%X\n",
                uReg32, 2, 3, 4, 5, 6);

            return ERROR;
            }

        /* Delay 1 MS - the smallest common time delay that we can make */

        OS_DELAY_MS(1);
        }

    USB_XHCD_DBG("usbXhcdHardwareStart - Done! xHCI should be running now!\n",
        1, 2, 3, 4, 5, 6);

    return OK;
    }

/*******************************************************************************
*
* usbXhcdHardwareStop - stop the xHCI
*
* This routine is to stop the xHCI.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdHardwareStop
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    UINT32 uCmd;
    UINT32 uCount;
    UINT32 uSts;

    /* Read the USBCMD register */

    uCmd = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_USBCMD);

    /*
     * Clear all interrupts enabled and the Run/Stop bit
     */

    uCmd &= ~(USB_XHCI_USBCMD_RS);

    /*
     * Write the value back, which actually disables interrupts
     * and halt the HC.
     */

    USB_XHCD_WRITE_OP_REG32(pHCDData, USB_XHCI_USBCMD, uCmd);

    /*
     * When USB_XHCI_USBCMD_RS bit is cleared to '0', the xHC completes the
     * current and any actively pipelined transactions on the USB and then
     * halts. The xHC shall halt within 16 ms after software clears the
     * Run/Stop bit. The HCHalted (HCH) bit in the USBSTS register indicates
     * when the xHC has finished its pending pipelined transactions and has
     * entered the stopped state.
     */

    OS_BUSY_WAIT_US(USB_XHCI_HALT_MAX_WAIT_US);

    uCount = 0;

    /* Wait until the HC is halted or error or timeout */

    while (TRUE)
        {
        /* Get the status */

        uSts = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_USBSTS);

        /* If the HCHalted bit is set, break and return OK */

        if (uSts & USB_XHCI_USBSTS_HCH)
            break;

        /* Check if we have run out of time */

        if ((uCount ++) > USB_XHCI_HALT_MAX_WAIT_US)
            {
            USB_XHCD_WARN("usbXhcdHardwareStop - timeout trying to halt the xHC\n",
                1, 2, 3, 4, 5, 6);

            return ERROR;
            }

        /* Read the USBCMD register */

        uCmd = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_USBCMD);

        USB_XHCD_DBG("usbXhcdHardwareStop - "
            "xHCI still running with USBCMD 0x%X USBSTS 0x%X\n",
            uCmd, uSts, 3, 4, 5, 6);

        /* Delay 1 US - the smallest common time delay that we can make */

        OS_BUSY_WAIT_US(1);
        }

    USB_XHCD_DBG("usbXhcdHardwareStop - Done! xHCI should be stopped now!\n",
        1, 2, 3, 4, 5, 6);

    return OK;
    }

/*******************************************************************************
*
* usbXhcdDeviceCtxDmaBufUnInit - uninitialize the Device Context DMA buffer
*
* This routine is to uninitialize the Device Context DMA buffer.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdDeviceCtxDmaBufUnInit
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    /* Status of vxBus routine call */

    STATUS  vxbSts = OK;

    /*
     * If the buffer is allocated, then we already have
     * the DMA tag and DMA map
     */

    if (pHCDData->pDCBAA != NULL)
        {
        vxbSts = vxbDmaBufMemFree(pHCDData->dcbaaDmaTagId,
                                  pHCDData->pDCBAA,
                                  pHCDData->dcbaaDmaMapId);

        if (vxbSts != OK)
            {
            USB_XHCD_ERR("usbXhcdDeviceCtxDmaBufUnInit - "
                "vxbDmaBufMemFree pDCBAA fail\n",
                1, 2, 3, 4, 5, 6);
            }
        else
            {
            pHCDData->dcbaaDmaMapId = NULL;
            pHCDData->pDCBAA = NULL;
            }
        }

    /* If we have DMA tag, destroy it */

    if (pHCDData->dcbaaDmaTagId != NULL)
        {
        vxbSts = vxbDmaBufTagDestroy(pHCDData->dcbaaDmaTagId);

        if (vxbSts != OK)
            {
            USB_XHCD_ERR("usbXhcdDeviceCtxDmaBufUnInit - "
                "vxbDmaBufTagDestroy dcbaaDmaTagId fail\n",
                1, 2, 3, 4, 5, 6);
            }
        else
            {
            pHCDData->dcbaaDmaTagId = NULL;
            }
        }

    /* If we have DMA tag, destroy it */

    if (pHCDData->ctxDmaTagId != NULL)
        {
        vxbSts = vxbDmaBufTagDestroy(pHCDData->ctxDmaTagId);

        if (vxbSts != OK)
            {
            USB_XHCD_ERR("usbXhcdDeviceCtxDmaBufUnInit - "
                "vxbDmaBufTagDestroy ctxDmaTagId fail\n",
                1, 2, 3, 4, 5, 6);
            }
        else
            {
            pHCDData->ctxDmaTagId = NULL;
            }
        }

    /* If we have DMA tag, destroy it */

    if (pHCDData->lscaDmaTagId != NULL)
        {
        vxbSts = vxbDmaBufTagDestroy(pHCDData->lscaDmaTagId);

        if (vxbSts != OK)
            {
            USB_XHCD_ERR("usbXhcdDeviceCtxDmaBufUnInit - "
                "vxbDmaBufTagDestroy lscaDmaTagId fail\n",
                1, 2, 3, 4, 5, 6);
            }
        else
            {
            pHCDData->lscaDmaTagId = NULL;
            }
        }

    /* If we have DMA tag, destroy it */

    if (pHCDData->pscaDmaTagId != NULL)
        {
        vxbSts = vxbDmaBufTagDestroy(pHCDData->pscaDmaTagId);

        if (vxbSts != OK)
            {
            USB_XHCD_ERR("usbXhcdDeviceCtxDmaBufUnInit - "
                "vxbDmaBufTagDestroy pscaDmaTagId fail\n",
                1, 2, 3, 4, 5, 6);
            }
        else
            {
            pHCDData->pscaDmaTagId = NULL;
            }
        }

    return vxbSts;
    }

/*******************************************************************************
*
* usbXhcdDeviceCtxDmaBufInit - initialize the Device Context DMA buffer
*
* This routine is to initialize the Device Context DMA buffer.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdDeviceCtxDmaBufInit
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    /* Status of vxBus routine call */

    STATUS  vxbSts;

    /*
     * Create tag for mapping DCBAA - can be located in 64 bit
     * memory space and aligned on 64 byte boundary.
     */

    pHCDData->dcbaaDmaTagId = vxbDmaBufTagCreate (pHCDData->pDev,
        pHCDData->xhciParentTag,        /* parent */
        USB_XHCI_DCBAA_ALIGNMENT,       /* alignment */
        pHCDData->uPageSize,            /* boundary */
        VXB_SPACE_MAXADDR,              /* lowaddr */
        VXB_SPACE_MAXADDR,              /* highaddr */
        NULL,                           /* filter */
        NULL,                           /* filterarg */
        USB_XHCI_DCBAA_MAX_SIZE,        /* max size */
        1,                              /* nSegments */
        USB_XHCI_DCBAA_MAX_SIZE,        /* max seg size */
        VXB_DMABUF_NOCACHE,             /* flags */
        NULL,                           /* lockfunc */
        NULL,                           /* lockarg */
        NULL);                          /* ppDmaTag */

    if (pHCDData->dcbaaDmaTagId == NULL)
        {
        USB_XHCD_ERR("usbXhcdDeviceCtxDmaBufInit - creating dcbaaDmaTagId fail\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Allocate the memory for DCBAA */

    pHCDData->pDCBAA = vxbDmaBufMemAlloc (pHCDData->pDev,
            pHCDData->dcbaaDmaTagId,
            NULL,
            VXB_DMABUF_MEMALLOC_CLEAR_BUF,
            &pHCDData->dcbaaDmaMapId);

    if (pHCDData->pDCBAA == NULL)
        {
        USB_XHCD_ERR("usbXhcdDeviceCtxDmaBufInit - creating pDCBAA fail\n",
            1, 2, 3, 4, 5, 6);

        (void) usbXhcdDeviceCtxDmaBufUnInit(pHCDData);

        return ERROR;
        }

    /*
     * Load the DMA MAP so that we get the correct BUS address
     * to program the hardware. The returned BUS address in the
     * loaded map can be used to program Device Context Base
     * Address Array Pointer (DCBAAP) register.
     */

    vxbSts = vxbDmaBufMapLoad (pHCDData->pDev,
        pHCDData->dcbaaDmaTagId,
        pHCDData->dcbaaDmaMapId,
        pHCDData->pDCBAA,
        USB_XHCI_DCBAA_MAX_SIZE,
        0);

    if (vxbSts != OK)
        {
        USB_XHCD_ERR("usbXhcdDeviceCtxDmaBufInit - loading dcbaaDmaMapId fail\n",
            1, 2, 3, 4, 5, 6);

        (void) usbXhcdDeviceCtxDmaBufUnInit(pHCDData);

        return ERROR;
        }

    /*
     * Create tag for mapping context - can be located in 64 bit
     * memory space and aligned on 64 byte boundary.
     */

    pHCDData->ctxDmaTagId = vxbDmaBufTagCreate (pHCDData->pDev,
        pHCDData->xhciParentTag,        /* parent */
        USB_XHCI_CTX_ALIGNMENT,         /* alignment */
        pHCDData->uPageSize,            /* boundary */
        VXB_SPACE_MAXADDR,              /* lowaddr */
        VXB_SPACE_MAXADDR,              /* highaddr */
        NULL,                           /* filter */
        NULL,                           /* filterarg */
        pHCDData->uCtxArraySize,        /* max size */
        1,                              /* nSegments */
        pHCDData->uCtxArraySize,        /* max seg size */
        VXB_DMABUF_NOCACHE,             /* flags */
        NULL,                           /* lockfunc */
        NULL,                           /* lockarg */
        NULL);                          /* ppDmaTag */

    if (pHCDData->ctxDmaTagId == NULL)
        {
        USB_XHCD_ERR("usbXhcdDeviceCtxDmaBufUnInit - creating ctxDmaTagId fail\n",
            1, 2, 3, 4, 5, 6);

        (void) usbXhcdDeviceCtxDmaBufUnInit(pHCDData);

        return ERROR;
        }

    return OK;
    }

/*******************************************************************************
*
* usbXhcdStreamCtxDmaBufUnInit - uninitialize the Stream Context DMA buffer
*
* This routine is to uninitialize the Device Context DMA buffer.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdStreamCtxDmaBufUnInit
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    /* Status of vxBus routine call */

    STATUS  vxbSts = OK;

    /* If we have DMA tag, destroy it */

    if (pHCDData->lscaDmaTagId != NULL)
        {
        vxbSts = vxbDmaBufTagDestroy(pHCDData->lscaDmaTagId);

        if (vxbSts != OK)
            {
            USB_XHCD_ERR("usbXhcdStreamCtxDmaBufUnInit - "
                "vxbDmaBufTagDestroy lscaDmaTagId fail\n",
                1, 2, 3, 4, 5, 6);
            }
        else
            {
            pHCDData->lscaDmaTagId = NULL;
            }
        }

    /* If we have DMA tag, destroy it */

    if (pHCDData->pscaDmaTagId != NULL)
        {
        vxbSts = vxbDmaBufTagDestroy(pHCDData->pscaDmaTagId);

        if (vxbSts != OK)
            {
            USB_XHCD_ERR("usbXhcdStreamCtxDmaBufUnInit - "
                "vxbDmaBufTagDestroy pscaDmaTagId fail\n",
                1, 2, 3, 4, 5, 6);
            }
        else
            {
            pHCDData->pscaDmaTagId = NULL;
            }
        }

    return vxbSts;
    }

/*******************************************************************************
*
* usbXhcdStreamCtxDmaBufInit - initialize the Stream Context DMA buffer
*
* This routine is to initialize the Stream Context DMA buffer.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdStreamCtxDmaBufInit
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    /*
     * Create tag for mapping Steam Array (Linear) - can be located in
     * 64 bit memory space and aligned on 16 byte boundary.
     *
     * Note: Steam Array (Linear) has no boundary limit.
     */

    pHCDData->lscaDmaTagId = vxbDmaBufTagCreate (pHCDData->pDev,
        pHCDData->xhciParentTag,        /* parent */
        USB_XHCI_LSCA_ALIGNMENT,        /* alignment */
        0,                              /* boundary */
        VXB_SPACE_MAXADDR,              /* lowaddr */
        VXB_SPACE_MAXADDR,              /* highaddr */
        NULL,                           /* filter */
        NULL,                           /* filterarg */
        USB_XHCI_LSCA_MAX_SIZE,         /* max size */
        1,                              /* nSegments */
        USB_XHCI_LSCA_MAX_SIZE,         /* max seg size */
        VXB_DMABUF_NOCACHE,             /* flags */
        NULL,                           /* lockfunc */
        NULL,                           /* lockarg */
        NULL);                          /* ppDmaTag */

    if (pHCDData->lscaDmaTagId == NULL)
        {
        USB_XHCD_ERR("usbXhcdDeviceCtxDmaBufUnInit - creating lscaDmaTagId fail\n",
            1, 2, 3, 4, 5, 6);

        (void) usbXhcdDeviceCtxDmaBufUnInit(pHCDData);

        return ERROR;
        }

    /*
     * Create tag for mapping Steam Array (Primary/Secondary) - can be
     * located in 64 bit memory space and aligned on 16 byte boundary.
     */

    pHCDData->pscaDmaTagId = vxbDmaBufTagCreate (pHCDData->pDev,
        pHCDData->xhciParentTag,        /* parent */
        USB_XHCI_PSCA_ALIGNMENT,        /* alignment */
        pHCDData->uPageSize,            /* boundary */
        VXB_SPACE_MAXADDR,              /* lowaddr */
        VXB_SPACE_MAXADDR,              /* highaddr */
        NULL,                           /* filter */
        NULL,                           /* filterarg */
        USB_XHCI_PSCA_MAX_SIZE,         /* max size */
        1,                              /* nSegments */
        USB_XHCI_PSCA_MAX_SIZE,         /* max seg size */
        VXB_DMABUF_NOCACHE,             /* flags */
        NULL,                           /* lockfunc */
        NULL,                           /* lockarg */
        NULL);                          /* ppDmaTag */

    if (pHCDData->pscaDmaTagId == NULL)
        {
        USB_XHCD_ERR("usbXhcdDeviceCtxDmaBufUnInit - creating pscaDmaTagId fail\n",
            1, 2, 3, 4, 5, 6);

        (void) usbXhcdDeviceCtxDmaBufUnInit(pHCDData);

        return ERROR;
        }

    return OK;
    }

/*******************************************************************************
*
* usbXhcdScratchPadDmaBufUnInit - uninitialize the Scratchpad Buffer DMA buffer
*
* This routine is to uninitialize the Scratchpad Buffer DMA buffer.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdScratchPadDmaBufUnInit
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    /* Status of vxBus routine call */

    STATUS  vxbSts = OK;

    /*
     * If there is no need for Scratch Buffer specified by the xHCI,
     * do not bother to create/destroy the DMA tags and maps.
     */

    if (pHCDData->uMaxSPBs == 0)
        {
        USB_XHCD_DBG("usbXhcdScratchPadDmaBufUnInit - "
            "No need for Scratch Buffer\n",
            1, 2, 3, 4, 5, 6);

        return OK;
        }

    /*
     * If the buffer is allocated, then we already have
     * the DMA tag and DMA map
     */

    if (pHCDData->pSPBs != NULL)
        {
        vxbSts = vxbDmaBufMemFree(pHCDData->spbDmaTagId,
                                  pHCDData->pSPBs,
                                  pHCDData->spbDmaMapId);

        if (vxbSts != OK)
            {
            USB_XHCD_ERR("usbXhcdScratchPadDmaBufUnInit - "
                "vxbDmaBufMemFree pSPBs fail\n",
                1, 2, 3, 4, 5, 6);
            }
        else
            {
            pHCDData->spbDmaMapId = NULL;
            pHCDData->pSPBs = NULL;
            }
        }

    /*
     * If the buffer is allocated, then we already have
     * the DMA tag and DMA map
     */

    if (pHCDData->pSPBA != NULL)
        {
        vxbSts = vxbDmaBufMemFree(pHCDData->spbaDmaTagId,
                                  pHCDData->pSPBA,
                                  pHCDData->spbaDmaMapId);

        if (vxbSts != OK)
            {
            USB_XHCD_ERR("usbXhcdScratchPadDmaBufUnInit - "
                "vxbDmaBufMemFree pSPBA fail\n",
                1, 2, 3, 4, 5, 6);
            }
        else
            {
            pHCDData->spbaDmaMapId = NULL;
            pHCDData->pSPBA = NULL;
            }
        }

    /* If we have DMA tag, destroy it */

    if (pHCDData->spbDmaTagId != NULL)
        {
        vxbSts = vxbDmaBufTagDestroy(pHCDData->spbDmaTagId);

        if (vxbSts != OK)
            {
            USB_XHCD_ERR("usbXhcdScratchPadDmaBufUnInit - "
                "vxbDmaBufTagDestroy spbDmaTagId fail\n",
                1, 2, 3, 4, 5, 6);
            }
        else
            {
            pHCDData->spbDmaTagId = NULL;
            }
        }

    /* If we have DMA tag, destroy it */

    if (pHCDData->spbaDmaTagId != NULL)
        {
        vxbSts = vxbDmaBufTagDestroy(pHCDData->spbaDmaTagId);

        if (vxbSts != OK)
            {
            USB_XHCD_ERR("usbXhcdScratchPadDmaBufUnInit - "
                "vxbDmaBufTagDestroy spbaDmaTagId fail\n",
                1, 2, 3, 4, 5, 6);
            }
        else
            {
            pHCDData->spbaDmaTagId = NULL;
            }
        }

    return vxbSts;
    }

/*******************************************************************************
*
* usbXhcdScratchPadDmaBufInit - initialize the Scratchpad Buffer DMA buffer
*
* This routine is to initialize the Scratchpad Buffer DMA buffer.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdScratchPadDmaBufInit
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    /* Status of vxBus routine call */

    STATUS  vxbSts;

    /* Loop index */

    UINT32  uIndex;

    /* Max Size for Scratch Buffer pages */

    UINT32  uMaxSPBSize;

    /* Scratch Buffer page BUS Addresss */

    UINT64  uBusAddr;

    /*
     * If there is no need for Scratch Buffer specified by the xHCI,
     * do not bother to create/destroy the DMA tags and maps.
     */

    if (pHCDData->uMaxSPBs == 0)
        {
        USB_XHCD_DBG("usbXhcdScratchPadDmaBufInit - "
            "No need for Scratch Buffer\n",
            1, 2, 3, 4, 5, 6);

        return OK;
        }

    if (pHCDData->pDCBAA == NULL)
        {
        USB_XHCD_ERR("usbXhcdScratchPadDmaBufInit - "
            "DCBAA not allocated yet\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * Create tag for mapping Scratchpad Buffer Array - can be located in
     * 64 bit memory space and aligned on 64 byte boundary.
     */

    pHCDData->spbaDmaTagId = vxbDmaBufTagCreate (pHCDData->pDev,
        pHCDData->xhciParentTag,        /* parent */
        USB_XHCI_SPBA_ALIGNMENT,        /* alignment */
        pHCDData->uPageSize,            /* boundary */
        VXB_SPACE_MAXADDR,              /* lowaddr */
        VXB_SPACE_MAXADDR,              /* highaddr */
        NULL,                           /* filter */
        NULL,                           /* filterarg */
        USB_XHCI_SPBA_MAX_SIZE,         /* max size */
        1,                              /* nSegments */
        USB_XHCI_SPBA_MAX_SIZE,         /* max seg size */
        VXB_DMABUF_NOCACHE,             /* flags */
        NULL,                           /* lockfunc */
        NULL,                           /* lockarg */
        NULL);                          /* ppDmaTag */

    if (pHCDData->spbaDmaTagId == NULL)
        {
        USB_XHCD_ERR(
            "usbXhcdScratchPadDmaBufInit - creating spbaDmaTagId fail\n",
            1, 2, 3, 4, 5, 6);

        (void) usbXhcdScratchPadDmaBufUnInit(pHCDData);

        return ERROR;
        }

    /* Allocate the memory for SPBA */

    pHCDData->pSPBA = vxbDmaBufMemAlloc (pHCDData->pDev,
            pHCDData->spbaDmaTagId,
            NULL,
            VXB_DMABUF_MEMALLOC_CLEAR_BUF,
            &pHCDData->spbaDmaMapId);

    if (pHCDData->pSPBA == NULL)
        {
        USB_XHCD_ERR(
            "usbXhcdScratchPadDmaBufInit - creating pSPBA fail\n",
            1, 2, 3, 4, 5, 6);

        (void) usbXhcdScratchPadDmaBufUnInit(pHCDData);

        return ERROR;
        }

    /*
     * Load the DMA MAP so that we get the correct BUS address
     * to program the hardware
     */

    vxbSts = vxbDmaBufMapLoad (pHCDData->pDev,
        pHCDData->spbaDmaTagId,
        pHCDData->spbaDmaMapId,
        pHCDData->pSPBA,
        USB_XHCI_SPBA_MAX_SIZE,
        0);

    if (vxbSts != OK)
        {
        USB_XHCD_ERR(
            "usbXhcdScratchPadDmaBufInit - loading spbaDmaMapId fail\n",
            1, 2, 3, 4, 5, 6);

        (void) usbXhcdScratchPadDmaBufUnInit(pHCDData);

        return ERROR;
        }

    /* We allocate contiguous pages for Scratchpad Buffer */

    uMaxSPBSize = pHCDData->uMaxSPBs * pHCDData->uPageSize;

    /*
     * Create tag for mapping Scratchpad Buffer - can be located in
     * 64 bit memory space and aligned on PAGESIZE boundary.
     */

    pHCDData->spbDmaTagId = vxbDmaBufTagCreate (pHCDData->pDev,
        pHCDData->xhciParentTag,        /* parent */
        pHCDData->uPageSize,            /* alignment */
        0,                              /* boundary */
        VXB_SPACE_MAXADDR,              /* lowaddr */
        VXB_SPACE_MAXADDR,              /* highaddr */
        NULL,                           /* filter */
        NULL,                           /* filterarg */
        uMaxSPBSize,                    /* max size */
        1,                              /* nSegments */
        uMaxSPBSize,                    /* max seg size */
        VXB_DMABUF_NOCACHE,             /* flags */
        NULL,                           /* lockfunc */
        NULL,                           /* lockarg */
        NULL);                          /* ppDmaTag */

    if (pHCDData->spbDmaTagId == NULL)
        {
        USB_XHCD_ERR(
            "usbXhcdScratchPadDmaBufInit - creating spbDmaTagId fail\n",
            1, 2, 3, 4, 5, 6);

        (void) usbXhcdScratchPadDmaBufUnInit(pHCDData);

        return ERROR;
        }

    /* Allocate the memory for SPB Pages */

    pHCDData->pSPBs = vxbDmaBufMemAlloc (pHCDData->pDev,
            pHCDData->spbDmaTagId,
            NULL,
            VXB_DMABUF_MEMALLOC_CLEAR_BUF,
            &pHCDData->spbDmaMapId);

    if (pHCDData->pSPBs == NULL)
        {
        USB_XHCD_ERR("usbXhcdScratchPadDmaBufInit - creating pSPBs fail\n",
            1, 2, 3, 4, 5, 6);

        (void) usbXhcdScratchPadDmaBufUnInit(pHCDData);

        return ERROR;
        }

    /*
     * Load the DMA MAP so that we get the correct BUS address
     * to program the hardware
     */

    vxbSts = vxbDmaBufMapLoad (pHCDData->pDev,
        pHCDData->spbDmaTagId,
        pHCDData->spbDmaMapId,
        pHCDData->pSPBs,
        uMaxSPBSize,
        0);

    if (vxbSts != OK)
        {
        USB_XHCD_ERR("usbXhcdScratchPadDmaBufInit - loading spbDmaMapId fail\n",
            1, 2, 3, 4, 5, 6);

        (void) usbXhcdScratchPadDmaBufUnInit(pHCDData);

        return ERROR;
        }

    /* Get the first page address of Scratchpad Buffers */

    uBusAddr = pHCDData->spbDmaMapId->fragList[0].frag;

    /* Create Scratchpad Buffers and load them into the SPBA elements */

    for (uIndex = 0; uIndex < pHCDData->uMaxSPBs; uIndex ++)
        {
        USB_XHCD_VDBG("usbXhcdScratchPadDmaBufInit - fill SPBA[%d] with %p[BUS]\n",
            uIndex, uBusAddr, 3, 4, 5, 6);

        /* Fill the SPB BUS address into the SPBA element */

        pHCDData->pSPBA[uIndex] =
            USB_XHCD_SWAP_DESC_DATA64(pHCDData, uBusAddr);

        /* Go to the next SPB */

        uBusAddr += pHCDData->uPageSize;
        }

    /* Get the BUS address of Scratchpad Buffers Array */

    uBusAddr = pHCDData->spbaDmaMapId->fragList[0].frag;

    /*
     * If the Max Scratchpad Buffers field of the HCSPARAMS2
     * register is > '0', then the first entry (entry_0) in
     * the DCBAA shall contain a pointer to the Scratchpad
     * Buffer Array.
     *
     * Fill the SPBA BUS address into the DCBAA entry_0.
     */

    pHCDData->pDCBAA[0] =
        USB_XHCD_SWAP_DESC_DATA64(pHCDData, uBusAddr);

    return OK;
    }

/*******************************************************************************
*
* usbXhcdRingDmaBufUnInit - uninitialize the Ring DMA buffer
*
* This routine is to uninitialize the Ring DMA buffer.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdRingDmaBufUnInit
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    /* Status of vxBus routine call */

    STATUS  vxbSts = OK;

    /* Loop index */

    UINT32  uIndex;

    /* Destroy the Command Ring if it exists */

    if (pHCDData->pCmdRing != NULL)
        {
        /* Destroy this Command Ring */

        vxbSts = usbXhcdRingDestroy(pHCDData, pHCDData->pCmdRing);

        if (vxbSts != OK)
            {
            USB_XHCD_ERR("usbXhcdRingDmaBufUnInit - "
                "usbXhcdRingDestroy pCmdRing fail\n",
                1, 2, 3, 4, 5, 6);
            }
        else
            {
            pHCDData->pCmdRing = NULL;
            }
        }

    /* Destroy all the event rings */

    if (pHCDData->ppEvtRings != NULL)
        {
        for (uIndex = 0; uIndex < pHCDData->uNumEvtRings; uIndex++)
            {
            /* If this entry is empty, go to the next one */

            if (pHCDData->ppEvtRings[uIndex] == NULL)
                continue;

            /* Destroy this Event Ring */

            vxbSts = usbXhcdRingDestroy(pHCDData,
                                        pHCDData->ppEvtRings[uIndex]);

            if (vxbSts != OK)
                {
                USB_XHCD_ERR("usbXhcdRingDmaBufUnInit - "
                    "usbXhcdRingDestroy ppEvtRings[%d] fail\n",
                    uIndex, 2, 3, 4, 5, 6);
                }
            else
                {
                pHCDData->ppEvtRings[uIndex] = NULL;
                }
            }

        /* Free the Event Ring pointer array */

        OSS_FREE(pHCDData->ppEvtRings);

        /* Set the pointer as NULL */

        pHCDData->ppEvtRings = NULL;
        }

    /*
     * If the buffer is allocated, then we already have
     * the DMA tag and DMA map
     */

    if (pHCDData->pERSTs != NULL)
        {
        vxbSts = vxbDmaBufMemFree(pHCDData->erstDmaTagId,
                                  pHCDData->pERSTs,
                                  pHCDData->erstDmaMapId);

        if (vxbSts != OK)
            {
            USB_XHCD_ERR("usbXhcdRingDmaBufUnInit - "
                "vxbDmaBufMemFree pERSTs fail\n",
                1, 2, 3, 4, 5, 6);
            }
        else
            {
            pHCDData->erstDmaMapId = NULL;
            pHCDData->pERSTs = NULL;
            }
        }

    /* If we have DMA tag, destroy it */

    if (pHCDData->erstDmaTagId != NULL)
        {
        vxbSts = vxbDmaBufTagDestroy(pHCDData->erstDmaTagId);

        if (vxbSts != OK)
            {
            USB_XHCD_ERR("usbXhcdRingDmaBufUnInit - "
                "vxbDmaBufTagDestroy erstDmaTagId fail\n",
                1, 2, 3, 4, 5, 6);
            }
        else
            {
            pHCDData->erstDmaTagId = NULL;
            }
        }

    /* If we have DMA tag, destroy it */

    if (pHCDData->segDmaTagId != NULL)
        {
        vxbSts = vxbDmaBufTagDestroy(pHCDData->segDmaTagId);

        if (vxbSts != OK)
            {
            USB_XHCD_ERR("usbXhcdRingDmaBufUnInit - "
                "vxbDmaBufTagDestroy segDmaTagId fail - mapCount %d\n",
                pHCDData->segDmaTagId->mapCount, 2, 3, 4, 5, 6);
            }
        else
            {
            pHCDData->segDmaTagId = NULL;
            }
        }

    return vxbSts;
    }

/*******************************************************************************
*
* usbXhcdRingDmaBufInit - initialize the Ring DMA buffer
*
* This routine is to initialize the Ring DMA buffer.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdRingDmaBufInit
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    /* Status of vxBus routine call */

    STATUS  vxbSts;

    /* Loop index */

    UINT32  uIndex;

    /* ERST Size */

    UINT32  uERSTSize;

    /*
     * TODO: We could make the number of event rings configurable.
     * Currently we use uMaxIntrs for this.
     */

    /*
     * TODO: We could make the number of event segments in each event
     * ring configurable. Currently we use uMaxERSTEs for this.
     */

    /* Save proper number of event rings */

#if defined (_WRS_CONFIG_USB_XHCD_ENABLE_MSI_INTR) || \
        defined (_WRS_CONFIG_USB_XHCD_ENABLE_MSIX_INTR)

    /*
     * When MSI/MSI-X is used, we can enable the smaller of the
     * MAX required/implemented number of interrupters.
     */

    pHCDData->uNumEvtRings = min(USB_XHCD_MAX_MSGS, pHCDData->uMaxIntrs);
#else
    /* When INTx is used, we can only use Primary interrupter */

    pHCDData->uNumEvtRings = 1;
#endif

    /*
     * Create tag for mapping segment - can be located in 64 bit
     * memory space and aligned on 64 byte boundary.
     *
     * Note: Semgment has 64KB boundary limit.
     */

    pHCDData->segDmaTagId = vxbDmaBufTagCreate (pHCDData->pDev,
        pHCDData->xhciParentTag,        /* parent */
        USB_XHCD_SEG_ALIGNMENT,         /* alignment */
        USB_XHCI_SEG_BOUNDARY_LIMIT,    /* boundary */
        VXB_SPACE_MAXADDR,              /* lowaddr */
        VXB_SPACE_MAXADDR,              /* highaddr */
        NULL,                           /* filter */
        NULL,                           /* filterarg */
        USB_XHCD_SEG_MAX_SIZE,          /* max size */
        1,                              /* nSegments */
        USB_XHCD_SEG_MAX_SIZE,          /* max seg size */
        VXB_DMABUF_NOCACHE,             /* flags */
        NULL,                           /* lockfunc */
        NULL,                           /* lockarg */
        NULL);                          /* ppDmaTag */

    if (pHCDData->segDmaTagId == NULL)
        {
        USB_XHCD_ERR(
            "usbXhcdDmaBuffInit - creating segDmaTagId fail\n",
            1, 2, 3, 4, 5, 6);

        (void) usbXhcdRingDmaBufUnInit(pHCDData);

        return ERROR;
        }

    /*
     * DESIGN CONSIDERATIONS:
     *
     * Each ERST is a small block of memory. Each Event Ring needs a
     * corresponding ERST.
     *
     * We create the ERST tables for all Event Rings in a contiguous
     * block of memory in order to reduce memory fragmentation. We
     * can carve the memory to form seperate ERSTs. However, since
     * each ERST has alignment requirement, we should make sure each
     * carved ERST size does not break the next ERST alignment. For
     * this to work, we round up the ERST size to match the ERST
     * alignment requirement.
     */

    /* Calculate the xHCI required ERST size */

    uERSTSize = (UINT32)(pHCDData->uMaxERSTEs * sizeof(USB_XHCI_ERST_ENTRY));

    /* If the ERST size is not multiple of its alignment, round up */

    if ((uERSTSize % USB_XHCI_ERST_ALIGNMENT) != 0)
        uERSTSize += (USB_XHCI_ERST_ALIGNMENT -
                     (uERSTSize % USB_XHCI_ERST_ALIGNMENT));

    /* Record each ERST actual size */

    pHCDData->uERSTSize = uERSTSize;

    /* Create a contiguous block of memory for ERSTs of all Event Rings */

    uERSTSize = (UINT32)(uERSTSize * pHCDData->uNumEvtRings);

    /*
     * Create tag for mapping ERST - can be located in 64 bit
     * memory space and aligned on 64 byte boundary.
     *
     * Note: ERST has no boundary limit.
     */

    pHCDData->erstDmaTagId = vxbDmaBufTagCreate (pHCDData->pDev,
        pHCDData->xhciParentTag,        /* parent */
        USB_XHCI_ERST_ALIGNMENT,        /* alignment */
        0,                              /* boundary */
        VXB_SPACE_MAXADDR,              /* lowaddr */
        VXB_SPACE_MAXADDR,              /* highaddr */
        NULL,                           /* filter */
        NULL,                           /* filterarg */
        uERSTSize,                      /* max size */
        1,                              /* nSegments */
        uERSTSize,                      /* max seg size */
        VXB_DMABUF_NOCACHE,             /* flags */
        NULL,                           /* lockfunc */
        NULL,                           /* lockarg */
        NULL);                          /* ppDmaTag */

    if (pHCDData->erstDmaTagId == NULL)
        {
        USB_XHCD_ERR(
            "usbXhcdDmaBuffInit - creating ctxDmaTagId fail\n",
            1, 2, 3, 4, 5, 6);

        (void) usbXhcdRingDmaBufUnInit(pHCDData);

        return ERROR;
        }

    /* Allocate the memory for ERST entries */

    pHCDData->pERSTs = vxbDmaBufMemAlloc (pHCDData->pDev,
            pHCDData->erstDmaTagId,
            NULL,
            VXB_DMABUF_MEMALLOC_CLEAR_BUF,
            &pHCDData->erstDmaMapId);

    if (pHCDData->pERSTs == NULL)
        {
        USB_XHCD_ERR(
            "usbXhcdDmaBuffInit - creating pERST fail\n",
            1, 2, 3, 4, 5, 6);

        (void) usbXhcdRingDmaBufUnInit(pHCDData);

        return ERROR;
        }

    /*
     * Load the DMA MAP so that we get the correct BUS address
     * to program the hardware
     */

    vxbSts = vxbDmaBufMapLoad (pHCDData->pDev,
        pHCDData->erstDmaTagId,
        pHCDData->erstDmaMapId,
        pHCDData->pERSTs,
        uERSTSize,
        0);

    if (vxbSts != OK)
        {
        USB_XHCD_ERR(
            "usbXhcdDmaBuffInit - loading map dcbaaDmaMapId fail\n",
            1, 2, 3, 4, 5, 6);

        (void) usbXhcdRingDmaBufUnInit(pHCDData);

        return ERROR;
        }

    /* Create the Command Ring */

    pHCDData->pCmdRing = usbXhcdRingCreate(pHCDData,
                                           USB_XHCD_CMD_RING,
                                           USB_XHCD_CMD_RING_MAX_SEGS,
                                           TRUE);

    if (pHCDData->pCmdRing == NULL)
        {
        USB_XHCD_ERR("usbXhcdDmaBuffInit - creating pCmdRing fail\n",
            1, 2, 3, 4, 5, 6);

        (void) usbXhcdRingDmaBufUnInit(pHCDData);

        return ERROR;
        }

    /* Allocate the memory for event ring pointer array */

    pHCDData->ppEvtRings = OSS_CALLOC(sizeof(pUSB_XHCD_RING) *
                                      pHCDData->uNumEvtRings);

    if (pHCDData->ppEvtRings == NULL)
        {
        USB_XHCD_ERR(
            "usbXhcdDmaBuffInit - creating ppEvtRings fail\n",
            1, 2, 3, 4, 5, 6);

        (void) usbXhcdRingDmaBufUnInit(pHCDData);

        return ERROR;
        }

    /* Allocate proper number of event rings */

    for (uIndex = 0; uIndex < pHCDData->uNumEvtRings; uIndex++)
        {
        /* Pointer to the newly create Event Ring */

        pUSB_XHCD_RING          pEventRing;

        /* Pointer to the segment structure */

        pUSB_XHCD_SEGMENT       pNextSeg;

        /* ERST current entry */

        pUSB_XHCI_ERST_ENTRY    pERSTEntry;

        /* ERST entry index */

        UINT32 uEntryIdx;

        /*
         * Create the Event Ring - it has no need for Link TRB to link
         * its Event Segments together, the ERST entries will help
         * this out (providing base and number of TRBs in a Segement)!
         */

        pEventRing = usbXhcdRingCreate(pHCDData,
                                       USB_XHCD_EVT_RING,
                                       pHCDData->uMaxERSTEs,
                                       FALSE);

        if (pEventRing == NULL)
            {
            USB_XHCD_ERR("usbXhcdDmaBuffInit - "
                "creating pHCDData->ppEvtRings[%d] fail\n",
                uIndex, 2, 3, 4, 5, 6);

            usbXhcdRingDmaBufUnInit(pHCDData);

            return ERROR;
            }

        /* Get to the start of new ERST (note the cast is required) */

        pERSTEntry = (pUSB_XHCI_ERST_ENTRY)(((UINT8 *)(pHCDData->pERSTs)) +
                      (uIndex * pHCDData->uERSTSize));

        /* Save the pointer to the current ERST */

        pEventRing->pERST = pERSTEntry;

        /* Start from the head Event Segment */

        pNextSeg = pEventRing->pHeadSeg;

        /*
         * Initialize each xHCI required entry as valid BUS
         * address for the new Event Ring Segments. There may
         * be padding entris in the ERST not filled, these are
         * used only for aligning the next ERST.
         */

        for (uEntryIdx = 0; uEntryIdx < pHCDData->uMaxERSTEs; uEntryIdx++)
            {
            /* Setup 64-bit BUS address of the Event Ring Segment Base */

            pERSTEntry->uRingSegBase =
                USB_XHCD_SWAP_DESC_DATA64(pHCDData,
                pNextSeg->uHeadTRB);

            /* Setup Ring Segment Size - # of TRBs supported by Segment */

            pERSTEntry->uRingSegSize =
                USB_XHCD_SWAP_DESC_DATA32(pHCDData,
                pNextSeg->uNumTRBs);

            /* Reserved - should be zero */

            pERSTEntry->uRsvdZ = 0;

            /* Go to the next ERST entry */

            pERSTEntry++;

            /* Go to the next Event Segment */

            pNextSeg = pNextSeg->pNextSeg;
            }

        /* Set the interrupter ID */

        pEventRing->uIntr = uIndex;

        pHCDData->ppEvtRings[uIndex] = pEventRing;
        }

    return OK;
    }

/*******************************************************************************
*
* usbXhcdDeviceArrayUnInit - uninitialize the device array
*
* This routine is to uninitialize the device array.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdDeviceArrayUnInit
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    STATUS uSts = OK;
    UINT32 uIndex;

    /* Allocate proper number of device structures */

    for (uIndex = 1; uIndex < (pHCDData->uMaxSlots + 1); uIndex++)
        {
        if (pHCDData->ppDevSlots[uIndex] != NULL)
            {
            if ((uSts = usbXhcdDeviceDestroy(pHCDData,
                pHCDData->ppDevSlots[uIndex])) != OK)
                {
                USB_XHCD_ERR("usbXhcdDeviceArrayUnInit - "
                    "destroy ppDevSlots[%d] fail\n",
                    uIndex, 2, 3, 4, 5, 6);
                }
            else
                pHCDData->ppDevSlots[uIndex] = NULL;
            }
        }

    return uSts;
    }

/*******************************************************************************
*
* usbXhcdDeviceArrayInit - initialize the device array
*
* This routine is to initialize the device array.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdDeviceArrayInit
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    /*
     * Allocate the device slots pointer array and clear it,
     * entry_0 is reserved to be consistent with DCBAA usage.
     * The DCBAA entry_0 might be used either as pointer to
     * the Scratchpad Buffer Array if used, or '0' if not used.
     */

    pHCDData->ppDevSlots = OSS_CALLOC((pHCDData->uMaxSlots + 1) *
                                      sizeof(pUSB_XHCD_DEVICE));

    if (pHCDData->ppDevSlots == NULL)
        {
        USB_XHCD_ERR("usbXhcdDeviceArrayInit - allocate ppDevSlots fail\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    return OK;
    }

/*******************************************************************************
*
* usbXhcdCommandArrayUnInit - uninitialize the command array
*
* This routine is to uninitialize the command array.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdCommandArrayUnInit
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    if (pHCDData->pCMDs != NULL)
        {
        OSS_FREE(pHCDData->pCMDs);

        pHCDData->pCMDs = NULL;
        }

    return OK;
    }

/*******************************************************************************
*
* usbXhcdCommandArrayInit - initialize the command array
*
* This routine is to initialize the command array.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdCommandArrayInit
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    UINT32          uIndex;
    pUSB_XHCD_CMD   pCMD;
    pUSB_XHCI_TRB   pTRB;

    /* Allocate the Command Structures as a single array */

    pHCDData->pCMDs = OSS_CALLOC(USB_XHCD_SEG_MAX_TRBS * sizeof(USB_XHCD_CMD));

    if (pHCDData->pCMDs == NULL)
        {
        USB_XHCD_ERR("usbXhcdDeviceArrayInit - allocate pCMDs fail\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Initialize the active command list */

    lstInit(&pHCDData->activeCmdList);

    /* Start from the first command and TRB structure */

    pCMD = pHCDData->pCMDs;
    pTRB = pHCDData->pCmdRing->pHeadSeg->pTRBs;

    /*
     * We create a 1-1 correspondence between the Command TRBs and
     * the Command structures, so that we can have a simple "offset
     * math" to locate the Structure using the Command TRB address
     * (either CPU Virtual or BUS address).
     */

    for (uIndex = 0; uIndex < USB_XHCD_SEG_MAX_TRBS; uIndex++)
        {
        pCMD->pCmdTRB = pTRB;

        /* Go to the next command and TRB structure */

        pCMD++;

        pTRB++;
        }

    return OK;
    }

/*******************************************************************************
*
* usbXhcdRegSaveUnInit - uninitialize the host controller register save area
*
* This routine is to uninitialize the host controller register save area.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdRegSaveUnInit
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    if (pHCDData->pRegSave == NULL)
        {
        USB_XHCD_DBG("usbXhcdRegSaveInit - no pRegSave allocated\n",
            1, 2, 3, 4, 5, 6);

        return OK;
        }

    OSS_FREE(pHCDData->pRegSave);

    pHCDData->pRegSave = NULL;

    return OK;
    }

/*******************************************************************************
*
* usbXhcdRegSaveInit - initialize the host controller register save area
*
* This routine is to initialize the host controller register save area.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdRegSaveInit
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    /*
     * Allocate the host controller power management register save area
     */

    pHCDData->pRegSave = OSS_CALLOC(sizeof(USB_XHCD_REG_SAVE) +
                       (pHCDData->uMaxIntrs * sizeof(USB_XHCD_INTR_REG_SAVE)));

    if (pHCDData->pRegSave == NULL)
        {
        USB_XHCD_ERR("usbXhcdRegSaveInit - allocate pRegSave fail\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    return OK;
    }

/*******************************************************************************
*
* usbXhcdRegSave - save the host controller registers for PM suspend
*
* This routine is to save the host controller registers for PM suspend.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdRegSave
    (
    pUSB_XHCD_DATA      pHCDData
    )
    {
    UINT32              uIndex;
    pUSB_XHCD_REG_SAVE  pRegSave = pHCDData->pRegSave;

    if (pRegSave == NULL)
        {
        USB_XHCD_ERR("usbXhcdRegSaveInit - no pRegSave allocated\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Read the USBCMD register */

    pRegSave->uUSBCMD = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_USBCMD);

    /* Read the DNCTRL register */

    pRegSave->uDNCTRL = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_DNCTRL);

    /* Read the CONFIG register */

    pRegSave->uCONFIG = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_CONFIG);

    /* Read the CRCR register */

    pRegSave->uCRCR = USB_XHCD_READ_OP_REG64(pHCDData, USB_XHCI_CRCR);

    /* Read the DCBAAP register */

    pRegSave->uDCBAAP = USB_XHCD_READ_OP_REG64(pHCDData, USB_XHCI_DCBAAP);

    /* Save the interrupter registers */

    for (uIndex = 0; uIndex < pHCDData->uMaxIntrs; uIndex++)
        {
        pUSB_XHCD_INTR_REG_SAVE pIntrSave = &pRegSave->xIntrSave[uIndex];

        /* Read the IMAN register */

        pIntrSave->uIMAN = USB_XHCD_READ_RT_REG32(pHCDData,
                                                  USB_XHCI_IMAN(uIndex));

        /* Read the IMOD register */

        pIntrSave->uIMOD = USB_XHCD_READ_RT_REG32(pHCDData,
                                                  USB_XHCI_IMOD(uIndex));

        /* Read the ERSTSZ register */

        pIntrSave->uERSTSZ = USB_XHCD_READ_RT_REG32(pHCDData,
                                                  USB_XHCI_ERSTSZ(uIndex));

        /* Read the ERSTBA register */

        pIntrSave->uERSTBA = USB_XHCD_READ_RT_REG64(pHCDData,
                                                  USB_XHCI_ERSTBA(uIndex));

        /* Read the ERDP register */

        pIntrSave->uERDP = USB_XHCD_READ_RT_REG64(pHCDData,
                                                  USB_XHCI_ERDP(uIndex));
        }

    return OK;
    }

/*******************************************************************************
*
* usbXhcdRegRestore - restore the host controller registers for PM resume
*
* This routine is to restore the host controller registers for PM resume.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdRegRestore
    (
    pUSB_XHCD_DATA      pHCDData
    )
    {
    UINT32              uIndex;
    pUSB_XHCD_REG_SAVE  pRegSave = pHCDData->pRegSave;

    if (pRegSave == NULL)
        {
        USB_XHCD_ERR("usbXhcdRegSaveInit - no pRegSave allocated\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Write the USBCMD register */

    USB_XHCD_WRITE_OP_REG32(pHCDData, USB_XHCI_USBCMD, pRegSave->uUSBCMD);

    /* Write the DNCTRL register */

    USB_XHCD_WRITE_OP_REG32(pHCDData, USB_XHCI_DNCTRL, pRegSave->uDNCTRL);

    /* Write the CONFIG register */

    USB_XHCD_WRITE_OP_REG32(pHCDData, USB_XHCI_CONFIG, pRegSave->uCONFIG);

    /* Write the CRCR register */

    USB_XHCD_WRITE_OP_REG64(pHCDData, USB_XHCI_CRCR, pRegSave->uCRCR);

    /* Write the DCBAAP register */

    USB_XHCD_WRITE_OP_REG64(pHCDData, USB_XHCI_DCBAAP, pRegSave->uDCBAAP);

    /* Restore the interrupter registers */

    for (uIndex = 0; uIndex < pHCDData->uMaxIntrs; uIndex++)
        {
        pUSB_XHCD_INTR_REG_SAVE pIntrSave = &pRegSave->xIntrSave[uIndex];

        /* Write the IMAN register */

        USB_XHCD_WRITE_RT_REG32(pHCDData, USB_XHCI_IMAN(uIndex),
                               pIntrSave->uIMAN);

        /* Write the IMOD register */

        USB_XHCD_WRITE_RT_REG32(pHCDData, USB_XHCI_IMOD(uIndex),
                               pIntrSave->uIMOD);

        /* Write the ERSTSZ register */

        USB_XHCD_WRITE_RT_REG32(pHCDData, USB_XHCI_ERSTSZ(uIndex),
                               pIntrSave->uERSTSZ);


        /* Write the ERSTBA register */

        USB_XHCD_WRITE_RT_REG64(pHCDData, USB_XHCI_ERSTBA(uIndex),
                               pIntrSave->uERSTBA);

        /* Write the ERDP register */

        USB_XHCD_WRITE_RT_REG64(pHCDData, USB_XHCI_ERDP(uIndex),
                               pIntrSave->uERDP);
        }

    return OK;
    }

/*******************************************************************************
*
* usbXhcdInternalStateSave - save xHC internal state for PM suspend
*
* This routine is to save xHC internal state for PM suspend.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdInternalStateSave
    (
    pUSB_XHCD_DATA      pHCDData
    )
    {
    UINT32 uUSBCMD;
    UINT32 uUSBSTS;
    UINT32 uCount;

    /*
     * Set the Controller Save State (CSS) flag in the USBCMD register
     * and wait for the Save State Status (SSS) flag in the USBSTS register
     * to transition to '0'.
     */

    uUSBCMD = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_USBCMD);
    uUSBCMD |= USB_XHCI_USBCMD_CSS;
    USB_XHCD_WRITE_OP_REG32(pHCDData, USB_XHCI_USBCMD, uUSBCMD);

    uCount = 0;

    /* Wait until the HC is halted or error or timeout */

    while (TRUE)
        {
        /* Get the status */

        uUSBSTS = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_USBSTS);

        /* If the Save State Status (SSS) bit is cleared, break and return OK */

        if (!(uUSBSTS & USB_XHCI_USBSTS_SSS))
            break;

        /* Check if we have run out of time */

        if ((uCount ++) > USB_XHCI_STATE_CHANGE_MAX_WAIT_MS)
            {
            USB_XHCD_WARN("usbXhcdInternalStateSave - timeout waiting xHC SSS\n",
                1, 2, 3, 4, 5, 6);

            return ERROR;
            }

        /* Read the USBCMD register */

        uUSBCMD = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_USBCMD);

        USB_XHCD_WARN("usbXhcdHostCtlrHalt - "
            "xHCI state not saved with USBCMD 0x%X USBSTS 0x%X\n",
            uUSBCMD, uUSBSTS, 3, 4, 5, 6);

        /* Delay 1 MS - the smallest common time delay that we can make */

        OS_DELAY_MS(1);
        }

    return OK;
    }

/*******************************************************************************
*
* usbXhcdInternalStateRestore - restore xHC internal state for PM resume
*
* This routine is to restore xHC internal state for PM resume.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdInternalStateRestore
    (
    pUSB_XHCD_DATA      pHCDData
    )
    {
    UINT32 uUSBCMD;
    UINT32 uUSBSTS;
    UINT32 uCount;

    /*
     * Set the Controller Restore State (CRS) flag in the USBCMD register
     * to '1' and wait for the Restore State Status (RSS) flag in the USBSTS
     * register to transition to '0'
     */

    uUSBCMD = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_USBCMD);
    uUSBCMD |= USB_XHCI_USBCMD_CRS;
    USB_XHCD_WRITE_OP_REG32(pHCDData, USB_XHCI_USBCMD, uUSBCMD);

    uCount = 0;

    /* Wait until the HC is halted or error or timeout */

    while (TRUE)
        {
        /* Get the status */

        uUSBSTS = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_USBSTS);

        /* If the Restore State Status (RSS) bit is cleared, break and return OK */

        if (!(uUSBSTS & USB_XHCI_USBSTS_RSS))
            break;

        /* Check if we have run out of time */

        if ((uCount ++) > USB_XHCI_STATE_CHANGE_MAX_WAIT_MS)
            {
            USB_XHCD_WARN("usbXhcdInternalStateRestore - timeout waiting xHC RSS\n",
                1, 2, 3, 4, 5, 6);

            return ERROR;
            }

        /* Read the USBCMD register */

        uUSBCMD = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_USBCMD);

        USB_XHCD_WARN("usbXhcdInternalStateRestore - "
            "xHCI state not restored with USBCMD 0x%X USBSTS 0x%X\n",
            uUSBCMD, uUSBSTS, 3, 4, 5, 6);

        /* Delay 1 MS - the smallest common time delay that we can make */

        OS_DELAY_MS(1);
        }

    return OK;
    }

/*******************************************************************************
*
* usbXhcdCmdReserve - reserve a command structure
*
* This routine is to initialize the command array.
*
* RETURNS: the pointer to the reserved command structure,
*   or NULL if no more free command structure.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdCmdRelease
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_DEVICE    pHCDDevice,
    pUSB_XHCD_CMD       pCMD
    )
    {
    /*
     * If releasing for a Device Slot, remove it from the
     * per device active command list, otherwise remove it
     * from the HCD instance active command list
     */

    if (pHCDDevice != NULL)
        lstDelete(&pHCDDevice->activeCmdList, &pCMD->cmdNode);
    else
        lstDelete(&pHCDData->activeCmdList, &pCMD->cmdNode);

    return OK;
    }

/*******************************************************************************
*
* usbXhcdCmdReserve - reserve a command structure
*
* This routine is to initialize the command array.
*
* RETURNS: the pointer to the reserved command structure,
*   or NULL if no more free command structure.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_XHCD_CMD usbXhcdCmdReserve
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_DEVICE    pHCDDevice,
    pUSB_XHCI_TRB       pCmdTRB
    )
    {
    UINT32          uCtrl;
    pUSB_XHCD_CMD   pCMD = &pHCDData->pCMDs[pCmdTRB -
                            pHCDData->pCmdRing->pHeadSeg->pTRBs];;

    /*
     * If reserving for a Device Slot, add it to the per
     * device active command list, otherwise add it to
     * HCD instance active command list.
     */

    if (pHCDDevice != NULL)
        lstAdd(&pHCDDevice->activeCmdList, &pCMD->cmdNode);
    else
        lstAdd(&pHCDData->activeCmdList, &pCMD->cmdNode);


    /* Get the TRB Control in CPU endian */

    uCtrl = USB_XHCD_SWAP_DESC_DATA32(pHCDData, pCmdTRB->uCtrl);

    /* Get the Command TRB Type */

    pCMD->uCmdType = (UINT8)USB_XHCI_TRB_CTRL_TYPE_GET(uCtrl);

    /* Set the Completion Code as invalid for now */

    pCMD->uCompCode = USB_XHCI_COMP_INVALID;

    return pCMD;
    }

/*******************************************************************************
*
* usbXhcdCmdMatch - match the Command structure in the active command list
*
* This routine is to match the Command structure in the active command list.
*
* RETURNS: TRUE if a match found, FALSE if not.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOL usbXhcdCmdMatch
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_DEVICE    pHCDDevice,
    pUSB_XHCD_CMD       pHCDCmd
    )
    {
    pUSB_XHCD_CMD   pCMD = NULL;

    /* Command node */

    NODE * pCmdNode;

    /* Command list */

    LIST * pActiveCmdList;

    /*
     * If not reserving for a Device Slot, add it to the HCD
     * instance active command list, otherwise add it to the
     * per device active command list.
     */

    if (pHCDDevice != NULL)
        pActiveCmdList = &pHCDDevice->activeCmdList;
    else
        pActiveCmdList = &pHCDData->activeCmdList;

    pCmdNode = lstFirst(pActiveCmdList);

    while (pCmdNode != NULL)
        {
        /* Cast to the command structure */

        pCMD = USB_XHCD_CMD_NODE_TO_CMD(pCmdNode);

        /* If a match if found, break */

        if (pCMD == pHCDCmd)
            {
            return TRUE;
            }

        pCmdNode = lstNext(pCmdNode);
        }

    return FALSE;
    }

/*******************************************************************************
*
* usbXhcdCmdComplete - complete a Command TRB
*
* This routine is to complete a Command TRB.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbXhcdCmdComplete
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_CMD       pHCDCmd
    )
    {
    UINT8               uSlotId;
    UINT8               uCompCode;
    UINT8               uCmdTrbType;
    BOOL                bMatched = FALSE;
    pUSB_XHCD_DEVICE    pHCDDevice = NULL;
    pUSB_XHCI_TRB       pCmdTRB = NULL;

    /* Get the pointer to the Command TRB */

    pCmdTRB = pHCDCmd->pCmdTRB;

    /* Get the Slot ID */

    uSlotId = pHCDCmd->uSlotId;

    /* Get the Completion Code */

    uCompCode = pHCDCmd->uCompCode;

    /* Get the Command TRB Type */

    uCmdTrbType = pHCDCmd->uCmdType;

    if ((uSlotId != 0) &&
        (uCmdTrbType != USB_XHCI_TRB_ENABLE_SLOT))
        {
        /* Get Device structure for the Slot */

        pHCDDevice = pHCDData->ppDevSlots[uSlotId];
        }
    else
        {
        pHCDDevice = NULL;
        }

    /* Exclusively access the pipe request list */

    (void) OS_WAIT_FOR_EVENT(pHCDData->hcdMutex, OS_WAIT_INFINITE);

    /* See if we can find a command structure posted */

    bMatched = usbXhcdCmdMatch(pHCDData, pHCDDevice, pHCDCmd);

    /* Release the exclusive access */

    OS_RELEASE_EVENT(pHCDData->hcdMutex);

    if (bMatched == FALSE)
        {
        USB_XHCD_ERR("usbXhcdCmdComplete - "
            "matching Slot ID %d DEV %p CMD TRB %p fail\n",
            uSlotId, pHCDDevice, pCmdTRB, 4, 5, 6);

        return;
        }

    switch (uCmdTrbType)
        {
        case USB_XHCI_TRB_ENABLE_SLOT:
            {
            pHCDData->uNewSlotId = uSlotId;

            USB_XHCD_WARN("usbXhcdCmdComplete - "
                "Enable Slot Command Completed - Got Slot ID %d - (%s)\n",
                uSlotId, usbXhcdCmdCompCodeName(uCompCode), 3, 4, 5, 6);
            }
            break;
        case USB_XHCI_TRB_DISABLE_SLOT:
            {
            USB_XHCD_WARN("usbXhcdCmdComplete - "
                "Disable Slot Command Completed - Slot ID %d - (%s)\n",
                uSlotId, usbXhcdCmdCompCodeName(uCompCode), 3, 4, 5, 6);
            }
            break;
        case USB_XHCI_TRB_ADDR_DEV:
            {
            USB_XHCD_WARN("usbXhcdCmdComplete - "
                "Address Device Command Completed - Slot ID %d - (%s)\n",
                uSlotId, usbXhcdCmdCompCodeName(uCompCode), 3, 4, 5, 6);
            }
            break;
        case USB_XHCI_TRB_CONFIG_EP:
            {
            USB_XHCD_WARN("usbXhcdCmdComplete - "
                "Configure Endpoint Command Completed - Slot ID %d - (%s)\n",
                uSlotId, usbXhcdCmdCompCodeName(uCompCode), 3, 4, 5, 6);
            }
            break;
        case USB_XHCI_TRB_EVAL_CONTEXT:
            {
            USB_XHCD_WARN("usbXhcdCmdComplete - "
                "Evaluate Context Command Completed - Slot ID %d - (%s)\n",
                uSlotId, usbXhcdCmdCompCodeName(uCompCode), 3, 4, 5, 6);
            }
            break;
        case USB_XHCI_TRB_RESET_EP:
            {
            UINT8 uEpId = (UINT8)USB_XHCI_TRB_CTRL_EP_ID_GET(pCmdTRB->uCtrl);

            USB_XHCD_WARN("usbXhcdCmdComplete - "
                "Reset Endpoint Command Completed - Slot ID %d EP ID %d - (%s)\n",
                uSlotId, uEpId, usbXhcdCmdCompCodeName(uCompCode), 4, 5, 6);

            if (pHCDCmd->waitTime == NO_WAIT)
                {
                USB_XHCD_WARN("usbXhcdCmdComplete - Reset Endpoint Command for "
                    "Soft Retrying USB Transaction Error\n",
                    1, 2, 3, 4, 5, 6);

                if (uCompCode == USB_XHCI_COMP_SUCCESS)
                    usbXhcdNotifyEndpointReady(pHCDData, uSlotId, uEpId, 0);
                }
            }
            break;
        case USB_XHCI_TRB_STOP_RING:
            {
            USB_XHCD_WARN("usbXhcdCmdComplete - "
                "Stop Transfer Ring Command Completed - Slot ID %d - (%s)\n",
                uSlotId, usbXhcdCmdCompCodeName(uCompCode), 3, 4, 5, 6);
            }
            break;
        case USB_XHCI_TRB_SET_DEQ:
            {
            USB_XHCD_WARN("usbXhcdCmdComplete - "
                "Set Transfer Ring Dequeue Pointer Command Completed - "
                "Slot ID %d - (%s)\n",
                uSlotId, usbXhcdCmdCompCodeName(uCompCode), 3, 4, 5, 6);
            }
            break;
        case USB_XHCI_TRB_RESET_DEV:
            {
            USB_XHCD_WARN("usbXhcdCmdComplete - "
                "Reset Device Command Completed - Slot ID %d - (%s)\n",
                uSlotId, usbXhcdCmdCompCodeName(uCompCode), 3, 4, 5, 6);
            }
            break;
        case USB_XHCI_TRB_FORCE_EVENT:
            {
            USB_XHCD_WARN("usbXhcdCmdComplete - "
                "Force Event Command Completed - Slot ID %d - (%s)\n",
                uSlotId, usbXhcdCmdCompCodeName(uCompCode), 3, 4, 5, 6);
            }
            break;
        case USB_XHCI_TRB_NEG_BANDWIDTH:
            {
            USB_XHCD_WARN("usbXhcdCmdComplete - "
                "Negotiate Bandwidth Command Completed - Slot ID %d - (%s)\n",
                uSlotId, usbXhcdCmdCompCodeName(uCompCode), 3, 4, 5, 6);
            }
            break;
        case USB_XHCI_TRB_SET_LTV:
            {
            USB_XHCD_WARN("usbXhcdCmdComplete - "
                "Set Latency Tolerance Value Command Completed - "
                "Slot ID %d - (%s)\n",
                uSlotId, usbXhcdCmdCompCodeName(uCompCode), 3, 4, 5, 6);
            }
            break;
        case USB_XHCI_TRB_GET_PORT_BW:
            {
            USB_XHCD_WARN("usbXhcdCmdComplete - "
                "Get port bandwidth Command Completed - Slot ID %d - (%s)\n",
                uSlotId, usbXhcdCmdCompCodeName(uCompCode), 3, 4, 5, 6);
            }
            break;
        case USB_XHCI_TRB_FORCE_HEADER:
            {
            USB_XHCD_WARN("usbXhcdCmdComplete - "
                "Force Header Command Completed - Slot ID %d - (%s)\n",
                uSlotId, usbXhcdCmdCompCodeName(uCompCode), 3, 4, 5, 6);
            }
            break;
        case USB_XHCI_TRB_CMD_NOOP:
            {
            USB_XHCD_WARN("usbXhcdCmdComplete - "
                "No-op Command Completed - Slot ID %d - (%s)\n",
                uSlotId, usbXhcdCmdCompCodeName(uCompCode), 3, 4, 5, 6);
            }
            break;
        default : break;
        }

    if (pHCDCmd->waitTime != NO_WAIT)
        {
        if (pHCDDevice != NULL)
            OS_RELEASE_EVENT(pHCDDevice->cmdDoneSem);
        else
            OS_RELEASE_EVENT(pHCDData->cmdDoneSem);
        }
    else
        {
        /* Exclusively access the active command list */

        (void) OS_WAIT_FOR_EVENT(pHCDData->hcdMutex, OS_WAIT_INFINITE);

        /* Release the command structure to the HCD free command list */

        (void) usbXhcdCmdRelease(pHCDData, pHCDDevice, pHCDCmd);

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDData->hcdMutex);
        }
    }

/*******************************************************************************
*
* usbXhcdDmaBuffUnInit - uninitialize the DMA buffer management facility
*
* This routine is to uninitialize the DMA buffer management facility.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdDmaBuffUnInit
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    /* UnInitialize the Ring DMA buffer facilities */

    if (usbXhcdRingDmaBufUnInit(pHCDData) != OK)
        {
        USB_XHCD_ERR("usbXhcdDmaBuffUnInit - usbXhcdRingDmaBufUnInit fail\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* UnInitialize the ScratchPad Buffer DMA buffer facilities */

    if (usbXhcdScratchPadDmaBufUnInit(pHCDData) != OK)
        {
        USB_XHCD_ERR("usbXhcdDmaBuffUnInit - usbXhcdScratchPadDmaBufUnInit fail\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* UnInitialize the Stream Context DMA buffer facilities */

    if (usbXhcdStreamCtxDmaBufUnInit(pHCDData) != OK)
        {
        USB_XHCD_ERR("usbXhcdDmaBuffUnInit - usbXhcdStreamCtxDmaBufUnInit fail\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* UnInitialize the Device Context DMA buffer facilities */

    if (usbXhcdDeviceCtxDmaBufUnInit(pHCDData) != OK)
        {
        USB_XHCD_ERR("usbXhcdDmaBuffUnInit - usbXhcdDeviceCtxDmaBufUnInit fail\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    return OK;
    }

/*******************************************************************************
*
* usbXhcdDmaBuffInit - initialize the DMA buffer management facility
*
* This routine is to initialize the DMA buffer management facility.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdDmaBuffInit
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    /* Check if the parameter is valid */

    if (pHCDData->pDev == NULL)
        {
        USB_XHCD_ERR("usbXhcdDmaBuffInit - pDev NULL\n",
            1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Get a reference to our parent tag */

    pHCDData->xhciParentTag = vxbDmaBufTagParentGet (pHCDData->pDev, 0);

    /* Initialize the Device Context DMA buffer facilities */

    if (usbXhcdDeviceCtxDmaBufInit(pHCDData) != OK)
        {
        USB_XHCD_ERR("usbXhcdDmaBuffInit - usbXhcdDeviceCtxDmaBufInit fail\n",
            1, 2, 3, 4, 5, 6);

        if (usbXhcdDmaBuffUnInit(pHCDData) != OK)
            {
            USB_XHCD_ERR("usbXhcdDmaBuffInit - usbXhcdDmaBuffUnInit fail 1\n",
                1, 2, 3, 4, 5, 6);
            }

        return ERROR;
        }

    /* Initialize the Stream Context DMA buffer facilities */

    if (usbXhcdStreamCtxDmaBufInit(pHCDData) != OK)
        {
        USB_XHCD_ERR("usbXhcdDmaBuffInit - usbXhcdStreamCtxDmaBufInit fail\n",
            1, 2, 3, 4, 5, 6);

        if (usbXhcdDmaBuffUnInit(pHCDData) != OK)
            {
            USB_XHCD_ERR("usbXhcdDmaBuffInit - usbXhcdDmaBuffUnInit fail 2\n",
                1, 2, 3, 4, 5, 6);
            }

        return ERROR;
        }

    /* Initialize the ScratchPad Buffer DMA buffer facilities */

    if (usbXhcdScratchPadDmaBufInit(pHCDData) != OK)
        {
        USB_XHCD_ERR("usbXhcdDmaBuffInit - usbXhcdScratchPadDmaBufInit fail\n",
            1, 2, 3, 4, 5, 6);

        if (usbXhcdDmaBuffUnInit(pHCDData) != OK)
            {
            USB_XHCD_ERR("usbXhcdDmaBuffInit - usbXhcdDmaBuffUnInit fail 3\n",
                1, 2, 3, 4, 5, 6);
            }

        return ERROR;
        }

    /* Initialize the Ring DMA buffer facilities */

    if (usbXhcdRingDmaBufInit(pHCDData) != OK)
        {
        USB_XHCD_ERR("usbXhcdDmaBuffInit - usbXhcdRingDmaBufInit fail\n",
            1, 2, 3, 4, 5, 6);

        if (usbXhcdDmaBuffUnInit(pHCDData) != OK)
            {
            USB_XHCD_ERR("usbXhcdDmaBuffInit - usbXhcdDmaBuffUnInit fail 4\n",
                1, 2, 3, 4, 5, 6);
            }

        return ERROR;
        }

    return OK;
    }

/*******************************************************************************
*
* usbXhcdRootHubDataUnInit - uninitialize the HCD Root Hub data
*
* This routine is to uninitialize the HCD Root Hub data.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdRootHubDataUnInit
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    /* Root hub index */

    UINT32 uIndex;

    /* Pointer to Root Hub Data */

    pUSB_XHCD_ROOT_HUB_DATA  pRootHub;

    for (uIndex = 0; uIndex < 2; uIndex++)
        {
        if (uIndex == 0)
            pRootHub = &pHCDData->USB3RH;
        else
            pRootHub = &pHCDData->USB2RH;

        if (pRootHub->pPortStatus != NULL)
            {
            OSS_FREE(pRootHub->pPortStatus);
            pRootHub->pPortStatus = NULL;
            }

        if (pRootHub->pHubInterruptData != NULL)
            {
            OSS_FREE(pRootHub->pHubInterruptData);
            pRootHub->pHubInterruptData = NULL;
            }
        }

    if (pHCDData->pRootPorts != NULL)
        {
        OSS_FREE(pHCDData->pRootPorts);
        pHCDData->pRootPorts = NULL;
        }

    return OK;
    }

/*******************************************************************************
*
* usbXhcdDataInit - initialize the HCD Root Hub data
*
* This routine is to initialize the HCD Root Hub data.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdRootHubDataInit
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    UINT32 uSize;

    /* The xHCI Supported Protocol Capability offset */

    UINT32 uProtocolCapOffset = 0;

    /* Supported Protocol Revision register value */

    UINT32 uProtRev;

    /* Supported Protocol Count register value */

    UINT32 uProtCount;

    /* Port index */

    UINT32 uIndex;

    /* Port Number */

    UINT32 uPortNumber;

    /* Port Offset */

    UINT32 uPortOffset;

    /* Pointer to Port info */

    pUSB_XHCD_ROOT_PORT_INFO pPortInfo;

    /* Pointer to Root Hub Data */

    pUSB_XHCD_ROOT_HUB_DATA  pRootHub;

    /*
     * If there is no xHCI Extended Capability, it is bad
     * since at least one xHCI Supported Protocol Capability
     * structure is required for all xHCI implementations.
     */

    if (pHCDData->uXpRegBase == 0)
        {
        USB_XHCD_ERR("usbXhcdRootHubDataInit - "
            "No xHCI Extended Capability!\n",
            1, 2, 3, 4, 5, 6);

        (void) usbXhcdRootHubDataUnInit(pHCDData);

        return ERROR;
        }

    /* Go to the USB xHCI Supported Protocol Capability */

    if (usbXhcdLocateExtCap(pHCDData,
        0, USB_XHCI_EXT_CAP_PROTOCOL, &uProtocolCapOffset) != OK)
        {
        USB_XHCD_DBG("usbXhcdRootHubDataInit - "
            "No USB xHCI Supported Protocol Capability!\n",
            1, 2, 3, 4, 5, 6);

        (void) usbXhcdRootHubDataUnInit(pHCDData);

        return ERROR;
        }

    pHCDData->pRootPorts = OSS_CALLOC((pHCDData->uMaxPorts) *
        sizeof(USB_XHCD_ROOT_PORT_INFO));

    if (pHCDData->pRootPorts == NULL)
        {
        USB_XHCD_ERR("usbXhcdRootHubDataInit - "
            "allocate pRootPorts fail\n",
            0, 0, 0, 0, 0, 0);

        (void) usbXhcdRootHubDataUnInit(pHCDData);

        return ERROR;
        }

    do
        {
        uProtRev = USB_XHCD_READ_XP_REG32(pHCDData,
            uProtocolCapOffset + USB_XHCI_SUPPORTED_PROT_VERSION);

        if (USB_XHCI_EXT_CAP_ID(uProtRev) == USB_XHCI_EXT_CAP_PROTOCOL)
            {
            UINT8   uRevMajor;
            UINT8   uRevMinor;

            USB_XHCD_WARN("USB_XHCI_SUPPORTED_PROT_VERSION = 0x%08X (USB %x.%x)\n",
                uProtRev,
                USB_XHCI_SUPPORTED_PROT_MAJOR(uProtRev),
                USB_XHCI_SUPPORTED_PROT_MINOR(uProtRev), 4, 5, 6);

            uProtCount = USB_XHCD_READ_XP_REG32(pHCDData,
                uProtocolCapOffset + USB_XHCI_SUPPORTED_PROT_COUNT);

            uRevMajor = (UINT8)USB_XHCI_SUPPORTED_PROT_MAJOR(uProtRev);
            uRevMinor = (UINT8)USB_XHCI_SUPPORTED_PROT_MINOR(uProtRev);

            uPortOffset = USB_XHCI_SUPPORTED_PROT_PORT_OFFSET(uProtCount);
            uProtCount = USB_XHCI_SUPPORTED_PROT_PORT_COUNT(uProtCount);

            if (uRevMajor == USB_XHCI_SUPPORTED_PROT_USB3)
                {
                pRootHub = &pHCDData->USB3RH;
                }
            else /* if (uRevMajor == USB_XHCI_SUPPORTED_PROT_USB2) */
                {
                pRootHub = &pHCDData->USB2RH;
                }

            pRootHub->uNumPorts = (UINT8)uProtCount;
            pRootHub->uPortOffset = (UINT8)(uPortOffset - 1);
            pRootHub->uRevMajor = uRevMajor;
            pRootHub->uRevMinor = uRevMinor;

            for (uIndex = 0; uIndex < uProtCount; uIndex++)
                {
                uPortNumber = uPortOffset + uIndex;

                pPortInfo = &pHCDData->pRootPorts[uPortNumber - 1];

                pPortInfo->pRootHub = pRootHub;
                pPortInfo->uPortNumber = (UINT8)(uIndex + 1);

                USB_XHCD_WARN("Root Hub Port %d is USB %x.%x\n",
                    uPortNumber,
                    uRevMajor,
                    uRevMinor, 4, 5, 6);
                }
            }

        /* Go to the next xHCI Extended Capability */

        uProtocolCapOffset = usbXhcdNextExtCapReg(pHCDData, uProtocolCapOffset);

        }while(uProtocolCapOffset != 0);

    /* Initialize the root hub data */

    for (uIndex = 0; uIndex < 2; uIndex++)
        {
        if (uIndex == 0)
            pRootHub = &pHCDData->USB3RH;
        else
            pRootHub = &pHCDData->USB2RH;

        /* Allocate the USB_HUB_PORT_STATUS buffer */

        pRootHub->pPortStatus = OSS_CALLOC(pRootHub->uNumPorts *
            sizeof(USB_HUB_PORT_STATUS));

        if (pRootHub->pPortStatus == NULL)
            {
            USB_XHCD_ERR("usbXhcdRootHubDataInit - "
                "allocate pPortStatus fail\n",
                0, 0, 0, 0, 0, 0);

            (void) usbXhcdRootHubDataUnInit(pHCDData);

            return ERROR;
            }

        /*
         * The interrupt transfer data is of the following format
         * D0 - Holds the status change information of the hub
         * D1 - Holds the status change information of the Port 1
         * ...
         * Dn - holds the status change information of the Port n
         *
         * So if the number of downstream ports is N, the size of
         * interrupt transfer data would be N + 1 bits.
         */

        uSize = (UINT32)((pRootHub->uNumPorts + 1)/ 8);

        if (0 != ((pRootHub->uNumPorts + 1) % 8))
            {
            uSize = uSize + 1;
            }

        /* Save the interrupt transfer data size */

        pRootHub->uHubInterruptDataSize = uSize;

        /* Allocate memory for the interrupt transfer data */

        pRootHub->pHubInterruptData = OSS_CALLOC(uSize);

        /* Check if memory allocation is successful */

        if (NULL == pRootHub->pHubInterruptData)
            {
            USB_XHCD_ERR("usbXhcdRootHubDataInit - "
                "allocate pHubInterruptData fail\n",
                0, 0, 0, 0, 0, 0);

            (void) usbXhcdRootHubDataUnInit(pHCDData);

            return ERROR;
            }
        }
    return OK;
    }

/*******************************************************************************
*
* usbXhcdDataUnInit - uninitialize the HCD data
*
* This routine is to uninitialize the HCD data.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdDataUnInit
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    STATUS uSts;

    /* UnInitialize Root Hub data */

    if ((uSts = usbXhcdRootHubDataUnInit(pHCDData)) != OK)
        {
        USB_XHCD_ERR("usbHcdXhciDeviceConnect - "
            "usbXhcdRootHubDataUnInit failed\n",
            0, 0, 0, 0, 0, 0);
        }

    /* UnInitialize the Device Array */

    if ((uSts = usbXhcdDeviceArrayUnInit(pHCDData)) != OK)
        {
        USB_XHCD_ERR("usbXhcdDataUnInit - "
            "usbXhcdDeviceArrayUnInit fail\n",
            1, 2, 3, 4, 5, 6);
        }

    /* UnInitialize the Command Array */

    if ((uSts = usbXhcdCommandArrayUnInit(pHCDData)) != OK)
        {
        USB_XHCD_ERR("usbXhcdDataUnInit - "
            "usbXhcdCommandArrayUnInit fail\n",
            1, 2, 3, 4, 5, 6);
        }

    /* Destroy the Power Management Register Save Area */

    if ((uSts = usbXhcdRegSaveUnInit(pHCDData)) != OK)
        {
        USB_XHCD_ERR("usbXhcdDataUnInit - "
            "usbXhcdCommandArrayUnInit fail\n",
            1, 2, 3, 4, 5, 6);
        }

    /* Destroy the DMA buffer resources */

    if ((uSts = usbXhcdDmaBuffUnInit(pHCDData)) != OK)
         {
         USB_XHCD_ERR("usbXhcdDataUnInit - "
             "usbXhcdDmaBuffUnInit fail\n",
             1, 2, 3, 4, 5, 6);
         }

    /* Destroy the interrupt notification event */

    if (pHCDData->InterruptEvent != NULL)
        {
        OS_DESTROY_EVENT(pHCDData->InterruptEvent);

        pHCDData->InterruptEvent = NULL;
        }

    /* Destroy the command done notification event */

    if (pHCDData->cmdDoneSem != NULL)
        {
        OS_DESTROY_EVENT(pHCDData->cmdDoneSem);

        pHCDData->cmdDoneSem = NULL;
        }

    /* Destroy the HCD synchronization mutex */

    if (pHCDData->hcdMutex != NULL)
        {
        OSS_MUTEX_TAKE(pHCDData->hcdMutex, WAIT_FOREVER);

        (void) OSS_MUTEX_DESTROY(pHCDData->hcdMutex);

        pHCDData->hcdMutex = NULL;
        }

    return uSts;
    }

/*******************************************************************************
*
* usbXhcdDataInit - initialize the HCD data
*
* This routine is to initialize the HCD data.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdDataInit
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    /* Initialize the DMA buffer resources */

    if (usbXhcdDmaBuffInit(pHCDData) != OK)
        {
        USB_XHCD_ERR("usbHcdXhciDeviceConnect - init DMA resources failed\n",
            0, 0, 0, 0, 0, 0);

        (void)usbXhcdDataUnInit(pHCDData);

        return ERROR;
        }

    /* Initialize Root Hub data */

    if (usbXhcdRootHubDataInit(pHCDData) != OK)
        {
        USB_XHCD_ERR("usbXhcdDataInit - usbXhcdRootHubDataInit failed\n",
            0, 0, 0, 0, 0, 0);

        (void)usbXhcdDataUnInit(pHCDData);

        return ERROR;
        }

    /* Initialize the Device Array */

    if (usbXhcdDeviceArrayInit(pHCDData) != OK)
        {
        USB_XHCD_ERR("usbXhcdDataInit - usbXhcdDeviceArrayInit fail\n",
            1, 2, 3, 4, 5, 6);

        (void)usbXhcdDataUnInit(pHCDData);

        return ERROR;
        }

    /* Initialize the Command Array */

    if (usbXhcdCommandArrayInit(pHCDData) != OK)
        {
        USB_XHCD_ERR("usbXhcdDataInit - usbXhcdCommandArrayInit fail\n",
            1, 2, 3, 4, 5, 6);

        (void)usbXhcdDataUnInit(pHCDData);

        return ERROR;
        }

    /* Initialize the Power Management Register Save Area */

    if (usbXhcdRegSaveInit(pHCDData) != OK)
        {
        USB_XHCD_ERR("usbXhcdDataInit - usbXhcdRegSaveInit fail\n",
            1, 2, 3, 4, 5, 6);

        (void)usbXhcdDataUnInit(pHCDData);

        return ERROR;
        }

    /* Create the HCD synchronization mutex */

    pHCDData->hcdMutex = semMCreate(SEM_Q_PRIORITY |
                                    SEM_INVERSION_SAFE |
                                    SEM_DELETE_SAFE);

    if (pHCDData->hcdMutex == /*SEM_ID_NULL*/0)
        {
        USB_XHCD_ERR("usbXhcdDataInit - create hcdMutex fail\n",
                     1, 2, 3, 4, 5 ,6);

        (void)usbXhcdDataUnInit(pHCDData);

        return ERROR;
        }

    /* Create the HCD interrupt notification event */

    pHCDData->InterruptEvent = OS_CREATE_EVENT(OS_EVENT_NON_SIGNALED);

    if (pHCDData->InterruptEvent == OS_INVALID_EVENT_ID)
        {
        USB_XHCD_ERR("usbXhcdDataInit - create InterruptEvent failed\n",
            0, 0, 0, 0, 0, 0);

        (void)usbXhcdDataUnInit(pHCDData);

        return ERROR;
        }

    /* Create the HCD command completion notification event */

    pHCDData->cmdDoneSem = OS_CREATE_EVENT(OS_EVENT_NON_SIGNALED);

    if (pHCDData->cmdDoneSem == OS_INVALID_EVENT_ID)
        {
        USB_XHCD_ERR("usbXhcdDataInit - create cmdDoneSem failed\n",
            0, 0, 0, 0, 0, 0);

        (void)usbXhcdDataUnInit(pHCDData);

        return ERROR;
        }

    /*
     * Initialize the ISR Spinlock
     */

    SPIN_LOCK_ISR_INIT(&pHCDData->spinLockIsr, 0);

    return OK;
    }

/*******************************************************************************
*
* usbXhcdSegmemtDestroy - destroy the segment structure
*
* This routine is to destroy the segment structure.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdSegmemtDestroy
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_SEGMENT   pSeg
    )
    {
    if (pSeg->pTRBs)
        {
        /* Free the memory allocated */

        if (vxbDmaBufMemFree(pHCDData->segDmaTagId,
            pSeg->pTRBs, pSeg->dmaMapId) != OK)
            {
            USB_XHCD_ERR("usbXhcdSegmemtDestroy - vxbDmaBufMemFree fail\n",
                0, 0, 0, 0, 0, 0);

            return ERROR;
            }

        pSeg->pTRBs = NULL;
        }

    OSS_FREE(pSeg);

    return OK;
    }

/*******************************************************************************
*
* usbXhcdSegmentCreate - create the segment structure
*
* This routine is to create the segment structure.
*
* RETURNS: Pointer to the created segment structure, NULL when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_XHCD_SEGMENT usbXhcdSegmentCreate
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    /* Pointer to the segment structure */

    pUSB_XHCD_SEGMENT pSeg;

    /* Return status of vxBus routine call */

    STATUS vxbSts;

    /* Allocate a segment structure */

    pSeg = OSS_CALLOC(sizeof(USB_XHCD_SEGMENT));

    if (!pSeg)
        {
        USB_XHCD_ERR(
            "usbXhcdSegmentCreate - creating pSeg fail\n",
            1, 2, 3, 4, 5, 6);

        return NULL;
        }

    /* Allocate the memory for TRBs of the segment */

    pSeg->pTRBs = vxbDmaBufMemAlloc (pHCDData->pDev,
            pHCDData->segDmaTagId,
            NULL,
            VXB_DMABUF_MEMALLOC_CLEAR_BUF,
            &pSeg->dmaMapId);

    if (pSeg->pTRBs == NULL)
        {
        USB_XHCD_ERR("usbXhcdSegmentCreate - creating pSeg->pTRBs fail\n",
            1, 2, 3, 4, 5, 6);

        (void) usbXhcdSegmemtDestroy(pHCDData, pSeg);

        return NULL;
        }

    /*
     * Load the DMA MAP so that we get the correct BUS address
     * to program the hardware
     */

    vxbSts = vxbDmaBufMapLoad (pHCDData->pDev,
        pHCDData->segDmaTagId,
        pSeg->dmaMapId,
        pSeg->pTRBs,
        USB_XHCD_SEG_MAX_SIZE,
        0);

    if (vxbSts != OK)
        {
        USB_XHCD_ERR(
            "usbXhcdSegmentCreate - loading map pSeg->dmaMapId fail\n",
            1, 2, 3, 4, 5, 6);

        (void) usbXhcdSegmemtDestroy(pHCDData, pSeg);

        return NULL;
        }

    /* Save the number of TRBs in this Segment */

    pSeg->uNumTRBs = USB_XHCD_SEG_MAX_TRBS;

    /* Save a short cut to the Segment first TRB Bus Address */

    pSeg->uHeadTRB = pSeg->dmaMapId->fragList[0].frag;
    pSeg->uTailTRB = pSeg->uHeadTRB +
                        ((USB_XHCD_SEG_MAX_TRBS - 1) * USB_XHCI_TRB_SIZE);

    return pSeg;
    }

/*******************************************************************************
*
* usbXhcdLinkSegmemts - link the two segment structures together
*
* This routine is to link the two segment structures together.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdLinkSegmemts
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_SEGMENT   pPrevSeg,
    pUSB_XHCD_SEGMENT   pNextSeg,
    BOOL                bHardLink
    )
    {
    /* Check the parameter validity */

    if ((pPrevSeg == NULL) || (pNextSeg == NULL))
        {
        USB_XHCD_ERR(
            "usbXhcdLinkSegmemts - invalid pPrevSeg %p or pNextSeg %p\n",
            pPrevSeg, pNextSeg, 3, 4, 5, 6);

        return ERROR;
        }

    /* Software link next */

    pPrevSeg->pNextSeg = pNextSeg;

    /* Hardware link next */

    if (bHardLink)
        {
        /* Pointer to the last TRB of the previous segment */

        pUSB_XHCI_TRB pPrevLastTRB;

        /* TRB Control */

        UINT32 uCtrl;

        /* The last TRB in a segment is a LINK TRB */

        pPrevLastTRB = &pPrevSeg->pTRBs[USB_XHCD_SEG_MAX_TRBS - 1];

        /* Fill the BUS address into the Ring Segment Pointer element */

        usbXhcdSetTrbData64(pHCDData,
                            pPrevLastTRB,
                            pNextSeg->dmaMapId->fragList[0].frag);

        /* Get the TRB Control in CPU endian */

        uCtrl = USB_XHCD_SWAP_DESC_DATA32(pHCDData, pPrevLastTRB->uCtrl);

        /* Clear the TRB type field */

        uCtrl &= ~(USB_XHCI_TRB_CTRL_TYPE_MASK);

        /* Set the TRB type as LINK TRB */

        uCtrl |= USB_XHCI_TRB_CTRL_TYPE(USB_XHCI_TRB_LINK);

        /* Write the TRB Control in BUS endian */

        pPrevLastTRB->uCtrl = USB_XHCD_SWAP_DESC_DATA32(pHCDData, uCtrl);
        }

    return OK;
    }

/*******************************************************************************
*
* usbXhcdRingReset - initialze the ring structure
*
* This routine is to initialze the ring structure.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdRingReset
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_RING      pRing
    )
    {
    /* Pointer to the segment structure */

    pUSB_XHCD_SEGMENT   pNextSeg;

    /* Pointer to the Next TRB */

    pUSB_XHCI_TRB       pNextTRB;

    /* TRB index */

    UINT32              uIndex;

    /* Cycle bit mask */

    UINT32              uCycleMask;

    /* Initially Enqueue segment is the head segment */

    pRing->pEnqSeg = pRing->pHeadSeg;

    /* Initially Enqueue TRB is the first TRB of the Enqueue segment */

    pRing->pEnqTRB = &pRing->pEnqSeg->pTRBs[0];

    /* Initially Dequeue segment is the same as Enqueue segment - empty */

    pRing->pDeqSeg = pRing->pEnqSeg;

    /* Initially Dequeue TRB is the first TRB of the Enqueue segment */

    pRing->pDeqTRB = pRing->pEnqTRB;

    /* Initially set PCS (Producer Cyclye State) to 1 */

    pRing->uCycle = 1;

    /* Reset the number of Enqueues and Dequeues to 0 */

    pRing->uNumDeq = pRing->uNumEnq = 0;

    /*
     * Start from the first Segment, set the Cycle bit all TRBs in all
     * Segments to be 0.
     */

    /* Get the first Segment */

    pNextSeg = pRing->pHeadSeg;

    /* Get the Cyclye bit mask in BUS endian */

    uCycleMask = USB_XHCD_SWAP_DESC_DATA32(pHCDData,
                    USB_XHCI_TRB_CTRL_CYCLE_BIT_MASK);

    while (pNextSeg != NULL)
        {
        /* Start from the first TRB */

        pNextTRB = pNextSeg->pTRBs;

        /* Check all TRBs in the Segment */

        for (uIndex = 0; uIndex < pNextSeg->uNumTRBs; uIndex++)
            {
            /* Clear the Cycle bit (in BUS endian) */

            pNextTRB->uCtrl &= ~uCycleMask;

            /* Go to the next TRB */

            pNextTRB++;
            }

        /* Go to the next Segment */

        pNextSeg = pNextSeg->pNextSeg;

        /* If we reached the first Segment again, break as are done! */

        if (pNextSeg == pRing->pHeadSeg)
            break;
        }

    return OK;
    }

/*******************************************************************************
*
* usbXhcdRingDestroy - destroy the ring structure
*
* This routine is to destroy the ring structure.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdRingDestroy
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_RING      pRing
    )
    {
    /* Pointer to the segment structure */

    pUSB_XHCD_SEGMENT   pHeadSeg;

    /* Pointer to the segment structure */

    pUSB_XHCD_SEGMENT   pNextSeg;

    /* Pointer to the segment structure */

    pUSB_XHCD_SEGMENT   pSeg;


    /* Check if parameters are valid */

    if (!pRing || !pRing->pHeadSeg)
        {
        USB_XHCD_ERR("usbXhcdRingDestroy - invalid parameter\n",
            0, 0, 0, 0, 0, 0);

        return ERROR;
        }

    /* Save the head segment pointer */

    pHeadSeg = pRing->pHeadSeg;

    /* Start destroying from the next segment */

    pSeg = pHeadSeg->pNextSeg;

    while (pSeg != pHeadSeg)
        {
        /* Save next segment pointer */

        pNextSeg = pSeg->pNextSeg;

        /* Destroy this segment */

        (void) usbXhcdSegmemtDestroy(pHCDData, pSeg);

        /* Go to the next segment */

        pSeg = pNextSeg;
        }

    /* Destroy this segment */

    (void) usbXhcdSegmemtDestroy(pHCDData, pHeadSeg);

    pRing->pHeadSeg = NULL;

    OSS_FREE(pRing);

    return OK;
    }

/*******************************************************************************
*
* usbXhcdRingCreate - create the ring structure
*
* This routine is to create the ring structure.
*
* RETURNS: Pointer to the created ring structure, NULL when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_XHCD_RING usbXhcdRingCreate
    (
    pUSB_XHCD_DATA  pHCDData,
    UINT32          uRingType,
    UINT32          uNumSegs,
    BOOL            bHardLink
    )
    {
    /* Pointer to the ring structure */

    pUSB_XHCD_RING      pRing;

    /* Pointer to the segment structure */

    pUSB_XHCD_SEGMENT   pPrevSeg;

    /* Pointer to the segment structure */

    pUSB_XHCD_SEGMENT   pNextSeg;

    /* Allocate the ring structure */

    pRing = OSS_CALLOC(sizeof(USB_XHCD_RING));

    if (!pRing)
        {
        USB_XHCD_ERR(
            "usbXhcdRingCreate - creating pRing fail\n",
            1, 2, 3, 4, 5, 6);

        return NULL;
        }

    /* Initialize the request list */

    lstInit(&pRing->reqList);

    /* Set the ring type */

    pRing->uType = uRingType;

    /* Save the number of Segments in the Ring */

    pRing->uNumSegs = uNumSegs;

    /* If there is no segment to create, return the ring */

    if (uNumSegs == 0)
        return pRing;

    /* Create the head segment */

    pRing->pHeadSeg = pNextSeg = usbXhcdSegmentCreate(pHCDData);

    if (pNextSeg == NULL)
        {
        USB_XHCD_ERR("usbXhcdRingCreate - creating pRing->pHeadSeg fail\n",
            1, 2, 3, 4, 5, 6);

        if (usbXhcdRingDestroy(pHCDData, pRing) != OK)
            {
            USB_XHCD_WARN("usbXhcdRingCreate - destroy ring fail 1\n",
                1, 2, 3, 4, 5, 6);
            }

        return NULL;
        }

    /* Decrease the number of segments to allocate */

    uNumSegs --;

    /* Set the previous segment as the head of the ring */

    pPrevSeg = pRing->pHeadSeg;

    while (uNumSegs)
        {
        /* Create the next segment */

        pNextSeg = usbXhcdSegmentCreate(pHCDData);

        if (pNextSeg == NULL)
            {
            USB_XHCD_ERR(
                "usbXhcdRingCreate - creating pNextSeg fail\n",
                1, 2, 3, 4, 5, 6);

            if (usbXhcdRingDestroy(pHCDData, pRing) != OK)
                {
                USB_XHCD_WARN("usbXhcdRingCreate - destroy ring fail 2\n",
                    1, 2, 3, 4, 5, 6);
                }

            return NULL;
            }

        /* Link the two segments together */

        (void) usbXhcdLinkSegmemts(pHCDData, pPrevSeg, pNextSeg, bHardLink);

        /* Update the previous segment */

        pPrevSeg = pNextSeg;

        /* Decrease the number of segments to allocate */

        uNumSegs --;
        }

    /* Link the last Segment to the first Segment to form a Ring */

    (void) usbXhcdLinkSegmemts(pHCDData, pPrevSeg, pRing->pHeadSeg, bHardLink);

    if (bHardLink)
        {
        /* Pointer to the last TRB of the previous segment */

        pUSB_XHCI_TRB pPrevLastTRB;

        /* TRB Control */

        UINT32 uCtrl;

        /* The last TRB in a segment is a LINK TRB */

        pPrevLastTRB = &pPrevSeg->pTRBs[USB_XHCD_SEG_MAX_TRBS - 1];

        /* Get the TRB Control in CPU endian */

        uCtrl = USB_XHCD_SWAP_DESC_DATA32(pHCDData, pPrevLastTRB->uCtrl);

        /* Set the Toggle Cycle (TC) bit for the last TRB in the ring */

        uCtrl |= USB_XHCI_TRB_CTRL_TC_MASK;

        /* Write the TRB Control in BUS endian */

        pPrevLastTRB->uCtrl = USB_XHCD_SWAP_DESC_DATA32(pHCDData, uCtrl);

        }

    /* Reset the ring */

    (void) usbXhcdRingReset(pHCDData, pRing);

    /* Record the pointer to the HCD */

    pRing->pHCDData = pHCDData;

    return pRing;
    }

/*******************************************************************************
*
* usbXhcdDeviceDestroy - destroy the device structure
*
* This routine is to destroy the device structure.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdDeviceDestroy
    (
    pUSB_XHCD_DATA          pHCDData,
    pUSB_XHCD_DEVICE        pDevice
    )
    {
    STATUS uSts = OK;
    UINT32 uIndex = 0;

    if (pDevice == NULL)
        {
        USB_XHCD_ERR("usbXhcdDeviceDestroy - pDevice NULL\n",
            0, 0, 0, 0, 0, 0);

        return ERROR;
        }

    if (pDevice->pIntputCtx != NULL)
        {
        uSts = usbXhcdCtxWrapperDestroy(pHCDData, pDevice->pIntputCtx);

        if (uSts != OK)
            {
            USB_XHCD_ERR("usbXhcdDeviceDestroy - destroy pIntputCtx fail\n",
                0, 0, 0, 0, 0, 0);

            return ERROR;
            }
        else
            pDevice->pIntputCtx = NULL;
        }

    if (pDevice->pOutputCtx != NULL)
        {
        uSts = usbXhcdCtxWrapperDestroy(pHCDData, pDevice->pOutputCtx);

        if (uSts != OK)
            {
            USB_XHCD_ERR("usbXhcdDeviceDestroy - destroy pOutputCtx fail\n",
                0, 0, 0, 0, 0, 0);

            return ERROR;
            }
        else
            pDevice->pOutputCtx = NULL;
        }

    for (uIndex = 0; uIndex < USB_XHCI_MAX_DEV_PIPES; uIndex++)
        {
        pUSB_XHCD_PIPE pHCDPipe = pDevice->pDevPipes[uIndex];

        if (pHCDPipe == NULL)
            continue;

        if ((pHCDPipe != pHCDData->pDefaultUSB3Pipe) &&
            (pHCDPipe != pHCDData->pDefaultUSB2Pipe))
            {
            /* Destroy the pipe */

            if (usbXhcdPipeDestroy(pHCDData, pHCDPipe) != OK)
                {
                USB_XHCD_WARN("usbXhcdDeviceDestroy - destroy pipe fail\n",
                    0, 0, 0, 0, 0, 0);
                }

            /* Clear the corresponding value in the pipe pointer array */

            pDevice->pDevPipes[uIndex] = NULL;
            }
        }

    if (pDevice->cmdDoneSem != NULL)
        {
        OS_DESTROY_EVENT(pDevice->cmdDoneSem);
        pDevice->cmdDoneSem = NULL;
        }

    OSS_FREE(pDevice);

    return OK;
    }

/*******************************************************************************
*
* usbXhcdDeviceCreate - create the device structure
*
* This routine is to create the device structure.
*
* RETURNS: Pointer to the created device structure, NULL when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_XHCD_DEVICE usbXhcdDeviceCreate
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    pUSB_XHCD_DEVICE    pDevice;

    /* Allocate the device structure */

    pDevice = OSS_CALLOC(sizeof(USB_XHCD_DEVICE));

    if (pDevice == NULL)
        {
        USB_XHCD_ERR("usbXhcdDeviceCreate - allocate pDevice fail\n",
            0, 0, 0, 0, 0, 0);

        return NULL;
        }

    /* Allocate the Input Context structure */

    pDevice->pIntputCtx = usbXhcdCtxWrapperCreate(pHCDData,
                            USB_XHCI_INPUT_CTX_TYPE);

    if (pDevice->pIntputCtx == NULL)
        {
        USB_XHCD_ERR("usbXhcdDeviceCreate - allocate pIntputCtx fail\n",
            0, 0, 0, 0, 0, 0);

        if (usbXhcdDeviceDestroy(pHCDData, pDevice) != OK)
            {
            USB_XHCD_WARN("usbXhcdDeviceCreate - destroy pDevice fail 1\n",
                0, 0, 0, 0, 0, 0);
            }

        return NULL;
        }

    /* Allocate the Output Context structure */

    pDevice->pOutputCtx = usbXhcdCtxWrapperCreate(pHCDData,
                            USB_XHCI_DEV_CTX_TYPE);

    if (pDevice->pOutputCtx == NULL)
        {
        USB_XHCD_ERR("usbXhcdDeviceCreate - allocate pOutputCtx fail\n",
            0, 0, 0, 0, 0, 0);

        if (usbXhcdDeviceDestroy(pHCDData, pDevice) != OK)
            {
            USB_XHCD_WARN("usbXhcdDeviceCreate - destroy pDevice fail 2\n",
                0, 0, 0, 0, 0, 0);
            }

        return NULL;
        }

    /* Create the HCD command completion notification event */

    pDevice->cmdDoneSem = OS_CREATE_EVENT(OS_EVENT_NON_SIGNALED);

    if (pDevice->cmdDoneSem == NULL)
        {
        USB_XHCD_ERR("usbXhcdDeviceCreate - create cmdDoneSem failed\n",
            0, 0, 0, 0, 0, 0);

        if (usbXhcdDeviceDestroy(pHCDData, pDevice) != OK)
            {
            USB_XHCD_WARN("usbXhcdDeviceCreate - destroy pDevice fail 1\n",
                0, 0, 0, 0, 0, 0);
            }

        return NULL;
        }
    /*
     * The other pipes for the device are created later by USBD
     * calling the createPipe interface.
     */

    return pDevice;
    }

/*******************************************************************************
*
* usbXhcdDeleteRequestInfo - delete a request info of the pipe
*
* This routine is used to delete a request info of the pipe. It will destroy
* the DMA map associated with this request info structure.
*
* <pHCDData> - Pointer to the HCD data
* <pHCDPipe> - Pointer to the pipe
* <pRequest> - Pointer to the request info structure
*
* Note: It is expetect the pipe lock has been taken when calling this routine.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbXhcdDeleteRequestInfo
    (
    pUSB_XHCD_DATA          pHCDData,
    pUSB_XHCD_PIPE          pHCDPipe,
    pUSB_XHCD_REQUEST_INFO  pRequest
    )
    {
    if (pRequest->dataDmaMapId != NULL)
        {
        if (pHCDPipe->dataDmaTagId == NULL)
            {
            USB_XHCD_WARN("usbXhcdDeleteRequestInfo - dataDmaTagId NULL\n",
                0, 0, 0, 0, 0, 0);
            }
        else
            {
            (void) vxbDmaBufMapDestroy (pHCDPipe->dataDmaTagId, pRequest->dataDmaMapId);

            pRequest->dataDmaMapId = NULL;
            }
        }

    /* Free the request */

    OSS_FREE(pRequest);
    }

/*******************************************************************************
*
* usbXhcdCreateRequestInfo - create a request info of the pipe
*
* This routine is used to create a request info of the pipe. It will create
* the DMA map associated with this request info structure.
*
* <pHCDData> - Pointer to the HCD data
* <pHCDPipe> - Pointer to the pipe
*
* Note: It is expetect the pipe lock has been taken when calling this routine.
*
* RETURNS: Pointer to the request info structure
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_XHCD_REQUEST_INFO usbXhcdCreateRequestInfo
    (
    pUSB_XHCD_DATA  pHCDData,
    pUSB_XHCD_PIPE  pHCDPipe
    )
    {
    /* Request information */

    pUSB_XHCD_REQUEST_INFO  pRequest;

    /* Make sure we have valid DMA Tag */

    if (pHCDPipe->dataDmaTagId == NULL)
        {
        USB_XHCD_ERR("usbXhcdCreateRequestInfo - dataDmaTagId NULL\n",
            0, 0, 0, 0, 0, 0);

        return NULL;
        }

    /* Get the request info */

    pRequest = (pUSB_XHCD_REQUEST_INFO)
                OSS_CALLOC(sizeof(USB_XHCD_REQUEST_INFO));

    if (pRequest == NULL)
        {
        USB_XHCD_ERR("usbXhcdCreateRequestInfo - allocate pRequest fail\n",
            0, 0, 0, 0, 0, 0);

        return NULL;
        }

    /* Record the pipe we work for */

    pRequest->pHCDPipe = pHCDPipe;

    /* Create Data DMA MAP ID */

    pRequest->dataDmaMapId = vxbDmaBufMapCreate (pHCDData->pDev,
                                                 pHCDPipe->dataDmaTagId,
                                                 0,
                                                 NULL);

    if (pRequest->dataDmaMapId == NULL)
        {
        USB_XHCD_ERR("usbXhcdCreateRequestInfo - "
            "allocate usrDataDmaMapId fail\n",
            0, 0, 0, 0, 0, 0);

        /* Delete the request resources */

        usbXhcdDeleteRequestInfo(pHCDData, pHCDPipe, pRequest);

        return NULL;
        }

    lstAdd(&pHCDPipe->freeReqList, &pRequest->epNode);

    return pRequest;
    }

/*******************************************************************************
*
* usbXhcdReserveRequestInfo - reserve a request info of the pipe for a URB
*
* This routine is used to reserve a request info structure of the pipe for a
* URB. It is commonly called when submitting a URB. A request info is considered
* to have been reserved when its associated with a URB (the URB pointer of this
* request info structure being non-NULL).
*
* <pHCDData> - Pointer to the HCD data
* <pHCDPipe> - Pointer to the pipe
* <pURB> - Pointer to the URB
*
* Note: It is expetect the pipe lock has been taken when calling this routine.
*
* RETURNS: the pointer to the reserved request info structure,
*   or NULL if no more free request on this pipe.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_XHCD_REQUEST_INFO usbXhcdReserveRequestInfo
    (
    pUSB_XHCD_DATA  pHCDData,
    pUSB_XHCD_PIPE  pHCDPipe,
    pUSBHST_URB     pURB
    )
    {
    /* Request information */

    pUSB_XHCD_REQUEST_INFO  pRequest;

    /* Request node */

    NODE * pReqNode;

    /* We can not allow bigger than the specified size */

    if (pURB->uTransferLength > pHCDPipe->uMaxTransferSize)
        {
        USB_XHCD_ERR(
            "usbXhcdReserveRequestInfo - uDevAddr %d uEpAddr %p\n"
            "uEpXferType %d uTransferLength %d is too big\n",
            pHCDPipe->uDevAddr,
            pHCDPipe->uEpAddr,
            pHCDPipe->uEpXferType,
            pURB->uTransferLength, 0, 0);

        return NULL;
        }

    /* Check if there is a free request info */

    if (lstCount(&pHCDPipe->freeReqList) == 0)
        {
        /* Create a new request and add it to the free request list */

        pRequest = usbXhcdCreateRequestInfo(pHCDData, pHCDPipe);

        if (pRequest == NULL)
            {
            USB_XHCD_ERR(
                "usbXhcdReserveRequestInfo - usbXhcdCreateRequestInfo fail - "
                "uTransferLength (%d) uMaxTransferSize (%d) \n",
                pURB->uTransferLength,
                pHCDPipe->uMaxTransferSize, 0, 0, 0, 0);

            return NULL;
            }
        }

    /* Get the first free node */

    pReqNode = lstGet(&pHCDPipe->freeReqList);

    if (pReqNode == NULL)
        {
        USB_XHCD_ERR(
            "usbXhcdReserveRequestInfo - lstGet fail - "
            "uTransferLength (%d) uMaxTransferSize (%d) \n",
            pURB->uTransferLength,
            pHCDPipe->uMaxTransferSize, 0, 0, 0, 0);

        return NULL;
        }

    /* Cast to the request info */

    pRequest = USB_XHCD_EP_NODE_TO_REQ_INFO(pReqNode);

    /* Associate the request info with the URB */

    pRequest->pURB = pURB;

    /* Save the requested length */

    pRequest->uReqLen = pURB->uTransferLength;

    /* Associate the URB with the request */

    pURB->pHcdSpecific = (void *)pRequest;

    /* Record the pipe we are working on */

    pRequest->pHCDPipe = pHCDPipe;

    /*
     * Initialize the elements that helps finding
     * the request through TRB Bus Address to 0.
     */

    pRequest->pHeadSeg = NULL;
    pRequest->pTailSeg = NULL;
    pRequest->pHeadTRB = NULL;
    pRequest->pTailTRB = NULL;
    pRequest->uHeadTRB = 0;
    pRequest->uTailTRB = 0;

    /* Add the request to the active request list of the pipe */

    lstAdd(&pHCDPipe->activeReqList, pReqNode);

    return pRequest;
    }

/*******************************************************************************
*
* usbXhcdReleaseRequestInfo - release a request info of the pipe
*
* This routine is used to release a request info structure of the pipe.
* It is commonly called when returnning a URB to the user.
*
* <pHCDData> - Pointer to the HCD data
* <pHCDPipe> - Pointer to the pipe
* <pRequest> - Pointer to the request info structure
*
* Note: It is expetect the pipe lock has been taken when calling this routine.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbXhcdReleaseRequestInfo
    (
    pUSB_XHCD_DATA          pHCDData,
    pUSB_XHCD_PIPE          pHCDPipe,
    pUSB_XHCD_REQUEST_INFO  pRequest
    )
    {
    /* Do this only when the URB pointer is non-NULL */

    if (pRequest != NULL)
        {
        if (pRequest->pURB)
            {
            /*
             * Set the URB HCD specific pointer to be NULL
             * indicating it is completed
             */

            pRequest->pURB->pHcdSpecific = NULL;

            /* Set the pURB as NULL to indicate it is free */

            pRequest->pURB = NULL;
            }

        /* Remove the request from the active request list of pipe */

        lstDelete(&pHCDPipe->activeReqList, &pRequest->epNode);

        /* Link the request info onto the free request queue */

        lstAdd(&pHCDPipe->freeReqList, &pRequest->epNode);
        }

    return;
    }

/*******************************************************************************
*
* usbXhcdIsRequestInfoUsed - check if a request info is reserved
*
* This routine is used to check if a request info is reserved.
*
* <pHCDData> - Pointer to the HCD data
* <pHCDPipe> - Pointer to the pipe
* <pRequest> - Pointer to the request info structure
*
* Note: It is expetect the pipe lock has been taken when calling this routine.
*
* RETURNS: TRUE if the request is reserved, FALSE if not.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOLEAN usbXhcdIsRequestInfoUsed
    (
    pUSB_XHCD_DATA          pHCDData,
    pUSB_XHCD_PIPE          pHCDPipe,
    pUSB_XHCD_REQUEST_INFO  pRequest
    )
    {
    /* If the request info used */

    BOOLEAN used;

    used = (BOOLEAN)((pRequest->pURB != NULL) ? TRUE : FALSE);

    return used;
    }

/*******************************************************************************
*
* usbXhcdUnSetupPipe - destroy any allocated transfer resources for a pipe
*
* This routine is used to destroy any allocated transfer resource for a pipe.
* The transfer resources, including request info structures associated DMA
* tag and DMA map are destroyed.
*
* RETURNS: USBHST_SUCCESS
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdUnSetupPipe
    (
    pUSB_XHCD_DATA pHCDData,
    pUSB_XHCD_PIPE pHCDPipe
    )
    {
    /* Request information */

    pUSB_XHCD_REQUEST_INFO  pRequest;

    /* Request node */

    NODE * pReqNode;

    /* Destroy the created request info structures */

    while ((pReqNode = lstGet(&pHCDPipe->freeReqList)) != NULL)
        {
        /* Cast to the request info */

        pRequest = USB_XHCD_EP_NODE_TO_REQ_INFO(pReqNode);

        /* Delete the request resources */

        usbXhcdDeleteRequestInfo(pHCDData, pHCDPipe, pRequest);
        }

    /* Destroy the user Data DMA TAG */

    if (pHCDPipe->dataDmaTagId != NULL)
        {
        USB_XHCD_DBG("usbXhcdUnSetupPipe - free dataDmaTagId\n",
            0, 0, 0, 0, 0, 0);

        if (vxbDmaBufTagDestroy(pHCDPipe->dataDmaTagId) != OK)
            {
            USB_XHCD_WARN("usbXhcdUnSetupPipe - free dataDmaTagId fail\n",
                0, 0, 0, 0, 0, 0);
            }

        pHCDPipe->dataDmaTagId = NULL;
        }

    return OK;
    }

/*******************************************************************************
*
* usbXhcdSetupPipe - allocate transfer resources for a pipe
*
* This routine is used to allocate transfer resources for a pipe.
* The transfer resources, including request info structures and associated DMA
* tag and DMA map are created.
*
* RETURNS: USBHST_SUCCESS when all resources allocation is done
*          USBHST_INVALID_PARAMETER when any paramter is invalid
*          USBHST_INVALID_REQUEST when there is any active transfer on the pipe
*          USBHST_INSUFFICIENT_MEMORY when any resource can not be allocated
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdSetupPipe
    (
    pUSB_XHCD_DATA              pHCDData,
    pUSB_XHCD_PIPE              pHCDPipe,
    pUSB_TRANSFER_SETUP_INFO    pSetupInfo
    )
    {
    /* Request information */

    pUSB_XHCD_REQUEST_INFO  pRequest;

    /* Transfer request index */

    UINT32                  uReqIndex;

    /* Check if the parameters are valid */

    if ((pHCDData == NULL) ||
        (pHCDPipe == NULL) ||
        (pSetupInfo == NULL) ||
        (pSetupInfo->uMaxNumReqests == 0)) /* Must have some transfer */
        {
        USB_XHCD_ERR("usbXhcdSetupPipe - invalid parameter\n",
                      1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * The pipe setup must be performed when there is no active transfers
     */

    if (lstCount(&pHCDPipe->activeReqList) != 0)
        {
        USB_XHCD_ERR("usbXhcdSetupPipe - "
            "There are still %d active transfers in progress\n",
            lstCount(&pHCDPipe->activeReqList), 0, 0, 0, 0, 0);

        return ERROR;
        }

    /* Remove any previous setting */

    if (usbXhcdUnSetupPipe(pHCDData, pHCDPipe) != OK)
        {
        USB_XHCD_WARN("usbXhcdSetupPipe - usbXhcdUnSetupPipe fail 1\n",
            0, 0, 0, 0, 0, 0);
        }

    /* Copy transfer setup info */

    pHCDPipe->uMaxTransferSize = pSetupInfo->uMaxTransferSize;

    pHCDPipe->uMaxNumReqests = pSetupInfo->uMaxNumReqests;

    pHCDPipe->uXferFlags = pSetupInfo->uFlags;

    /* Create tag for mapping Data buffer */

    pHCDPipe->dataDmaTagId = vxbDmaBufTagCreate (pHCDData->pDev,
        pHCDData->xhciParentTag,        /* parent */
        1,                              /* alignment */
        0,                              /* boundary */
        VXB_SPACE_MAXADDR,              /* lowaddr */
        VXB_SPACE_MAXADDR,              /* highaddr */
        NULL,                           /* filter */
        NULL,                           /* filterarg */
        pHCDPipe->uMaxTransferSize,     /* max size */
        1,                              /* nSegments */
        pHCDPipe->uMaxTransferSize,     /* max seg size */
        VXB_DMABUF_CACHEALIGNCHECK,     /* flags */
        NULL,                           /* lockfunc */
        NULL,                           /* lockarg */
        NULL);                          /* ppDmaTag */

    if (pHCDPipe->dataDmaTagId == NULL)
        {
        USB_XHCD_ERR("usbXhcdSetupPipe - allocate dataDmaTagId fail\n",
            0, 0, 0, 0, 0, 0);

        if (usbXhcdUnSetupPipe(pHCDData, pHCDPipe) != OK)
            {
            USB_XHCD_WARN("usbXhcdSetupPipe - usbXhcdUnSetupPipe fail 2\n",
                0, 0, 0, 0, 0, 0);
            }

        return ERROR;
        }

    /* Create the requested number of request info structures */

    for (uReqIndex = 0; uReqIndex < pHCDPipe->uMaxNumReqests; uReqIndex++)
        {
        /* Create the request info */

        pRequest = usbXhcdCreateRequestInfo(pHCDData, pHCDPipe);

        if (pRequest == NULL)
            {
            USB_XHCD_ERR("usbXhcdSetupPipe - allocate pRequest fail\n",
                0, 0, 0, 0, 0, 0);

            if (usbXhcdUnSetupPipe(pHCDData, pHCDPipe) != OK)
                {
                USB_XHCD_WARN("usbXhcdSetupPipe - usbXhcdUnSetupPipe fail 3\n",
                    0, 0, 0, 0, 0, 0);
                }

            return ERROR;
            }
        }

    return USBHST_SUCCESS;
    }

/*******************************************************************************
*
* usbXhcdPipeDestroy - destroy the pipe structure
*
* This routine is to destroy the pipe structure.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdPipeDestroy
    (
    pUSB_XHCD_DATA  pHCDData,
    pUSB_XHCD_PIPE  pHCDPipe
    )
    {
    if ((pHCDData == NULL) || (pHCDPipe == NULL))
        {
        USB_XHCD_WARN("usbXhcdPipeDestroy - nothing to do!\n",
                     1, 2, 3, 4, 5 ,6);

        return OK;
        }

    /* If the pipe mutex is not valid, the pipe is not created */

    if (pHCDPipe->pipeMutex == OS_INVALID_EVENT_ID)
        {
        USB_XHCD_ERR("usbXhcdDeletePipe - pipeMutex not valid!\n",
            0, 0, 0, 0, 0, 0);

        return ERROR;
        }

    /* Exclusively access the pipe request list */

    (void) OS_WAIT_FOR_EVENT(pHCDPipe->pipeMutex, OS_WAIT_INFINITE);

    /* Try to destroy the ring */

    if (usbXhcdPipeRingDestroy(pHCDData, pHCDPipe) != OK)
        {
        USB_XHCD_WARN("usbXhcdPipeDestroy - pipe ring destroy fail!\n",
                     1, 2, 3, 4, 5 ,6);

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDPipe->pipeMutex);

        return ERROR;
        }

    if (usbXhcdUnSetupPipe(pHCDData, pHCDPipe) != OK)
        {
        USB_XHCD_WARN("usbXhcdPipeDestroy - usbXhcdUnSetupPipe fail!\n",
                     1, 2, 3, 4, 5 ,6);

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDPipe->pipeMutex);

        return ERROR;
        }

    /* Try to destroy the pipe mutex */

    (void) OSS_MUTEX_DESTROY(pHCDPipe->pipeMutex);

    /* Set the mutex as NULL */

    pHCDPipe->pipeMutex = NULL;

    /* Set the pipe as valid */

    pHCDPipe->uValidMaggic = USB_XHCD_MAGIC_DEAD;

    /* Destroy the pipe itself */

    OSS_FREE(pHCDPipe);

    return OK;
    }

/*******************************************************************************
*
* usbXhcdPipeCreate - create a working pipe structure
*
* This routine is to create a working pipe structure.
*
* RETURNS: Pointer to the created pipe structure, NULL when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_XHCD_PIPE usbXhcdPipeCreate
    (
    pUSB_XHCD_DATA                  pHCDData,
    pUSB_XHCD_DEVICE                pHCDDevice,
    pUSBHST_ENDPOINT_DESCRIPTOR     pEpDesc
    )
    {
    pUSB_XHCD_PIPE                          pHCDPipe = NULL;
    pUSBHST_ENDPOINT_COMPANION_DESCRIPTOR   pEpCompanionDesc = NULL;

    /* Retrieve the endpoint companion descriptor */

    pEpCompanionDesc = (pUSBHST_ENDPOINT_COMPANION_DESCRIPTOR)
        (((UINT8 *)pEpDesc) + pEpDesc->bLength);

    /*
     * Each SuperSpeed endpoint described in an interface is followed
     * by a SuperSpeed Endpoint Companion descriptor. This descriptor
     * contains additional endpoint characteristics that are only
     * defined for SuperSpeed endpoints. The Default Control Pipe does
     * not have an Endpoint Companion descriptor.
     */

    if ((pHCDDevice != NULL) &&
        (pHCDDevice->uDevSpeed == USBHST_SUPER_SPEED) &&
        (pEpDesc->bEndpointAddress != 0) &&
        (pEpCompanionDesc->bDescriptorType != USB_DESCR_SS_ENDPOINT_COMPANION))
        {
        USB_XHCD_ERR("usbXhcdPipeCreate - "
                     "SS non-default control EP has NO EP COMPANION\n",
                     1, 2, 3, 4, 5, 6);

        return NULL;
        }

    /* Allocate memory for the USB_XHCD_PIPE data structure */

    pHCDPipe = (pUSB_XHCD_PIPE)OSS_CALLOC(sizeof(USB_XHCD_PIPE));

    /* Check if memory allocation is successful */

    if (NULL == pHCDPipe)
        {
        USB_XHCD_ERR("usbXhcdPipeCreate - allocate pHCDPipe fail\n",
                     1, 2, 3, 4, 5 ,6);

        return NULL;
        }

    /* Create the pipe mutex lock */

    if ((OSS_MUTEX_CREATE(&pHCDPipe->pipeMutex) != OK) ||
        (pHCDPipe->pipeMutex == OS_INVALID_EVENT_ID))
        {
        USB_XHCD_ERR("usbXhcdPipeCreate - create pipeMutex fail\n",
                     1, 2, 3, 4, 5 ,6);

        OSS_FREE(pHCDPipe);

        return NULL;
        }

    /* Init the request list */

    lstInit(&(pHCDPipe->activeReqList));
    lstInit(&(pHCDPipe->freeReqList));
    lstInit(&(pHCDPipe->cancelReqList));

    /*

     * If a valid device structure has been passed, then we should
     * get these information from the device structure, otherwise,
     * we can fake these information (which is the only case when
     * creating the HCD default pipe is done in HCD initialization
     * phase.)
     */

    if (pHCDDevice != NULL)
        {
        /* Update the device address which holds the endpoint */

        pHCDPipe->uDevAddr = pHCDDevice->uDevAddr;

        /* Update the Device Slot ID */

        pHCDPipe->uSlotId = pHCDDevice->uSlotId;

        /* Update the endpoint speed */

        pHCDPipe->uDevSpeed = pHCDDevice->uDevSpeed;
        }
    else
        {
        /* Update the device address which holds the endpoint */

        pHCDPipe->uDevAddr = 0;

        /* Update the Device Slot ID */

        pHCDPipe->uSlotId = 0;

        /* Update the endpoint speed */

        pHCDPipe->uDevSpeed = USBHST_HIGH_SPEED;
        }

    /* Update Endpoint address */

    pHCDPipe->uEpAddr = pEpDesc->bEndpointAddress;

    /* Update the direction of the endpoint */

    pHCDPipe->uEpDir = (UINT8)(pEpDesc->bEndpointAddress & USB_ENDPOINT_DIR_MASK);

    /* Update the type of endpoint to be created */

    pHCDPipe->uEpXferType = (UINT8)(pEpDesc->bmAttributes & USB_ATTR_EPTYPE_MASK);

    /* Update Endpoint ID (DCI) */

    if (pHCDPipe->uEpXferType == USBHST_CONTROL_TRANSFER)
        {
        pHCDPipe->uEpId = USB_XHCI_CTRL_EP_CTX_DCI(pHCDPipe->uEpAddr);
        }
    else
        {
        pHCDPipe->uEpId = USB_XHCI_NON_CTRL_EP_CTX_DCI(pHCDPipe->uEpAddr);
        }

    /*
     * Get the value needed to program into the EP context
     * Endpoint Type (EP Type).
     *
     * Endpoint Type (EP Type). This field identifies whether
     * an Endpoint Context is Valid, and if so, what type of
     * endpoint the context defines.
     *
     *     Value     Endpoint Type      Direction
     *       0        Not Valid           N/A
     *       1        Isoch               Out
     *       2        Bulk                Out
     *       3        Interrupt           Out
     *       4        Control             Bidirectional
     *       5        Isoch               In
     *       6        Bulk                In
     *       7        Interrupt           In
     */

    if (pHCDPipe->uEpXferType == USBHST_CONTROL_TRANSFER)
        {
        pHCDPipe->uEpType = USB_XHCI_CTRL_EP;
        }
    else
        {
        /*
         * This is based on the uEpXferType and uEpType encoding
         * correspondence to do a simple math play.
         */

        if (pHCDPipe->uEpDir == USB_ENDPOINT_IN)
            {
            pHCDPipe->uEpType = USB_XHCI_CTRL_EP | (pHCDPipe->uEpXferType);
            }
        else
            {
            pHCDPipe->uEpType = (pHCDPipe->uEpXferType);
            }
        }

    /* Get the proper value for the EP context Interval field */

    pHCDPipe->uInterval = usbXhcdEpIntervalGet(pHCDPipe->uDevSpeed,
                                               pHCDPipe->uEpDir,
                                               pHCDPipe->uEpXferType,
                                               pEpDesc->bInterval);
    /*
     * SuperSpeed ISOCHRONOUS endpoint has a Mult filed in its companion
     * descriptor, which is a zero based value that determines the maximum
     * number of packets within a service interval that this endpoint
     * supports.
     *
     * Maximum number of packets = bMaxBurst * (Mult + 1)
     */

    if ((pHCDPipe->uDevSpeed == USBHST_SUPER_SPEED) &&
        (pHCDPipe->uEpXferType == USBHST_ISOCHRONOUS_TRANSFER))
        {
        pHCDPipe->uMult = (UINT8)(pEpCompanionDesc->bmAttributes &
                           USBHST_SS_EP_COMPANION_ATTR_MULT_MASK);
        }
    else
        {
        pHCDPipe->uMult = 0;
        }

    /*
     * SuperSpeed Bulk endpoint may support Streams, indicated by its
     * companion descriptor MaxStreams field, which is the maximum
     * number of streams this endpoint supports. Valid values are
     * from 0 to 16, where a value of 0 indicates that the endpoint
     * does not define streams. For the values 1 to 16, the number
     * of streams supported equals 2^MaxStreams.
     */

    if ((pHCDPipe->uDevSpeed == USBHST_SUPER_SPEED) &&
        (pHCDPipe->uEpXferType == USBHST_BULK_TRANSFER))
        {
        UINT8 uMaxPStreams = (UINT8)(pEpCompanionDesc->bmAttributes &
                              USBHST_SS_EP_COMPANION_ATTR_MAX_STREAMS_MASK);

        /*
         * A value of 0 in MaxStreams indicates that the endpoint does
         * not define streams. For the values 1 to 16, the number of
         * streams supported equals 2^MaxStreams.
         */

        if (uMaxPStreams != 0)
            {
            /*
             * For SS Bulk endpoints, the range of valid values for
             * this field is defined by the MaxPSASize field in the
             * HCCPARAMS register.
             */

            pHCDPipe->uMaxPStreams = min(uMaxPStreams,
                USB_XHCI_MAX_PSA_SIZE(pHCDData->uHCCPARAMS));
            }
        else
            {
            pHCDPipe->uMaxPStreams = 0;
            }
        }
    else
        {
        pHCDPipe->uMaxPStreams = 0;
        }

    /* Linear Stream Array identifies how a Stream ID shall be interpreted */

    if (pHCDPipe->uMaxPStreams != 0)
        pHCDPipe->uLSA = 1; /* TODO: This could be user configurable */
    else
        pHCDPipe->uLSA = 0;

    /*
     * Store the maximum packet size. This field shall be set to the value
     * defined in bits 10:0 of the USB Endpoint Descriptor wMaxPacketSize
     * field. However, we initially set it to be the wMaxPacketSize, it
     * will be masked in following code after uMaxBusrtSize is calculated.
     */

    pHCDPipe->uMaxPacketSize = (OS_UINT16_LE_TO_CPU(pEpDesc->wMaxPacketSize));

    USB_XHCD_DBG("usbXhcdPipeCreate - "
                 "Pipe %p for dev %d ep 0x%02x uMaxPacketSize 0x%04x\n",
                  pHCDPipe,
                  pHCDPipe->uDevAddr,
                  pHCDPipe->uEpAddr,
                  pHCDPipe->uMaxPacketSize, 5, 6);

    /* Get the proper uMaxBusrtSize according to speed */

    if ((pHCDPipe->uDevSpeed == USBHST_SUPER_SPEED) ||
        (pHCDPipe->uDevSpeed == USBHST_HIGH_SPEED))
        {
        /* FS endpoints should split wMaxPacketSize into two parts */

        if (pHCDPipe->uDevSpeed == USBHST_HIGH_SPEED)
            {
            /* Get uMaxBusrtSize from bits 12:11 of wMaxPacketSize */

            pHCDPipe->uMaxBusrtSize = (UINT8)((pHCDPipe->uMaxPacketSize &
                USB_ENDPOINT_NUM_TRANSACTIONS_MASK) >>
                USB_ENDPOINT_NUM_TRANSACTIONS_OFFSET);

            /* Get real uMaxPacketSize from bits 10:0 of wMaxPacketSize */

            pHCDPipe->uMaxPacketSize = (UINT16)(pHCDPipe->uMaxPacketSize &
                                                USB_ENDPOINT_MAX_PACKET_SIZE_MASK);
            }
        else
            {
            /* SS endpoints use bMaxBurst from EP companion descriptor */

            pHCDPipe->uMaxBusrtSize = pEpCompanionDesc->bMaxBurst;
            }
        }
    else
        {
        /* For all LS/FS endpoints this field shall be cleared to '0' */

        pHCDPipe->uMaxBusrtSize = 0;
        }

    /* Error Count (CErr) */

    pHCDPipe->uCErr = USB_XHCI_EP_CTX_CERR_MAX;

    /* Host Initiate Disable (HID) - we allow host try its best! */

    pHCDPipe->uHID = 0;

    /* Update the high speed hub information - will be done later */

    pHCDPipe->uHubInfo = 0;

    /*
     * Average TRB Length
     *
     * If precise values for the Average TRB Length of an endpoint
     * are not available, software may calculate a running average
     * of the size of TRBs scheduled for an endpoint in real-time
     * and periodically updating Average TRB Length.
     *
     * Reasonable initial values of Average TRB Length for Control
     * endpoints would be 8B, Interrupt endpoints 1KB, and Bulk and
     * Isoch endpoints 3KB.
     */

    switch (pHCDPipe->uEpXferType)
        {
        case USBHST_CONTROL_TRANSFER:
            pHCDPipe->uAvrgLen = USB_XHCI_EP_CTX_AVRG_TRB_LEN_CTRL;
            break;
        case USBHST_BULK_TRANSFER:
            pHCDPipe->uAvrgLen = USB_XHCI_EP_CTX_AVRG_TRB_LEN_BULK;
            break;
        case USBHST_INTERRUPT_TRANSFER:
            pHCDPipe->uAvrgLen = USB_XHCI_EP_CTX_AVRG_TRB_LEN_INTR;
            break;
        case USBHST_ISOCHRONOUS_TRANSFER:
            pHCDPipe->uAvrgLen = USB_XHCI_EP_CTX_AVRG_TRB_LEN_ISOC;
            break;
        default: break;
        }

    /*
     * The xHC uses the Max Endpoint Service Time Interval Payload
     * (Max ESIT Payload) and Interval fields in the Endpoint Context
     * to compute the USB bandwidth that it shall reserve for a
     * periodic endpoint.
     *
     * Software shall define the maximum periodic payload per ESIT
     * as follows for USB2 periodic endpoints:
     *
     * Max ESIT Payload in Bytes = Max Packet Size * (Max Burst Size+1).
     *
     * Software shall define the maximum periodic payload per ESIT as
     * follows for SS periodic endpoints:
     *
     * Max ESIT Payload in Bytes = SuperSpeed Endpoint Companion
     *                             Descriptor:wBytesPerInterval.
     */

    if ((pHCDPipe->uEpXferType == USBHST_INTERRUPT_TRANSFER) ||
        (pHCDPipe->uEpXferType == USBHST_ISOCHRONOUS_TRANSFER))
        {
        if (pHCDPipe->uDevSpeed == USBHST_SUPER_SPEED)
            {
            pHCDPipe->uMaxESIT =
                OS_UINT16_LE_TO_CPU(pEpCompanionDesc->wBytesPerInterval);
            }
        else
            {
            pHCDPipe->uMaxESIT = (UINT16)(pHCDPipe->uMaxPacketSize *
                (pHCDPipe->uMaxBusrtSize + 1));
            }
        }

    /* Create the pipe rings */

    if (usbXhcdPipeRingCreate(pHCDData, pHCDPipe) != OK)
        {
        USB_XHCD_ERR("usbXhcdPipeCreate - "
                     "usbXhcdPipeRingCreate for dev %d ep 0x%02x fail\n",
                     pHCDPipe->uDevAddr,
                     pHCDPipe->uEpAddr,
                     3, 4, 5, 6);

        if (usbXhcdPipeDestroy(pHCDData, pHCDPipe) != OK)
            {
            USB_XHCD_WARN("usbXhcdPipeCreate - usbXhcdPipeDestroy fail\n",
                         1, 2, 3, 4, 5, 6);
            }

        return NULL;
        }

    USB_XHCD_DBG("usbXhcdPipeCreate - "
                 "New pipe %p created for dev %d ep 0x%02x OK\n",
                  pHCDPipe,
                  pHCDPipe->uDevAddr,
                  pHCDPipe->uEpAddr,
                  4, 5, 6);

    /* Clear the tranaction error count */

    pHCDPipe->uNumTransErrors = 0;

    /* Set the pipe as valid */

    pHCDPipe->uValidMaggic = USB_XHCD_MAGIC_ALIVE;

    /* When created, the Endpoint Context is to be added */

    pHCDPipe->uEpCtxState = USB_XHCD_EP_CTX_STATE_NEW;

    /* Set the pointer to our device structure */

    pHCDPipe->pHCDDevice = pHCDDevice;

    return pHCDPipe;
    }

/*******************************************************************************
*
* usbXhcdPipeUpdate - check and update pipe if its Endpoint Context needs change
*
* This routine is to check and update pipe if its Endpoint Context needs change.
*
* RETURNS: TRUE if any Endpoint Context needs change, FALSE if no need.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOL usbXhcdPipeUpdate
    (
    pUSB_XHCD_DATA                  pHCDData,
    pUSB_XHCD_PIPE                  pHCDPipe,
    pUSBHST_ENDPOINT_DESCRIPTOR     pEpDesc
    )
    {
    pUSBHST_ENDPOINT_COMPANION_DESCRIPTOR   pEpCompanionDesc = NULL;
    BOOL    bChanged = FALSE;
    UINT8   uNewValue8;
    UINT16  uNewValue16;

    /* Retrieve the endpoint companion descriptor */

    pEpCompanionDesc = (pUSBHST_ENDPOINT_COMPANION_DESCRIPTOR)
        (((UINT8 *)pEpDesc) + pEpDesc->bLength);

    /*
     * Each SuperSpeed endpoint described in an interface is followed
     * by a SuperSpeed Endpoint Companion descriptor. This descriptor
     * contains additional endpoint characteristics that are only
     * defined for SuperSpeed endpoints. The Default Control Pipe does
     * not have an Endpoint Companion descriptor.
     */

    if ((pHCDPipe->uDevSpeed == USBHST_SUPER_SPEED) &&
        (pEpDesc->bEndpointAddress != 0) &&
        (pEpCompanionDesc->bDescriptorType != USB_DESCR_SS_ENDPOINT_COMPANION))
        {
        USB_XHCD_ERR("usbXhcdPipeUpdate - "
                     "SS non-default control EP has NO EP COMPANION\n",
                     1, 2, 3, 4, 5 ,6);

        return FALSE;
        }

    uNewValue8 = (UINT8)(pEpDesc->bmAttributes & USB_ATTR_EPTYPE_MASK);

    if (pHCDPipe->uEpXferType != uNewValue8)
        {
        USB_XHCD_WARN("usbXhcdPipeUpdate - "
                      "EP 0x%02x uEpXferType changed from %d to %d\n",
                      pHCDPipe->uEpAddr,
                      pHCDPipe->uEpXferType,
                      uNewValue8, 4, 5, 6);

        /* Set it as need to be changed */

        bChanged = TRUE;

        /* Apply the change in software side */

        pHCDPipe->uEpXferType = uNewValue8;

        /*
         * Get the value needed to program into the EP context
         * Endpoint Type (EP Type).
         *
         * Endpoint Type (EP Type). This field identifies whether
         * an Endpoint Context is Valid, and if so, what type of
         * endpoint the context defines.
         *
         *     Value     Endpoint Type      Direction
         *       0        Not Valid           N/A
         *       1        Isoch               Out
         *       2        Bulk                Out
         *       3        Interrupt           Out
         *       4        Control             Bidirectional
         *       5        Isoch               In
         *       6        Bulk                In
         *       7        Interrupt           In
         */

        if (pHCDPipe->uEpXferType == USBHST_CONTROL_TRANSFER)
            {
            pHCDPipe->uEpType = USB_XHCI_CTRL_EP;
            }
        else
            {
            /*
             * This is based on the uEpXferType and uEpType encoding
             * correspondence.
             */

            if (pHCDPipe->uEpDir == USB_ENDPOINT_IN)
                {
                pHCDPipe->uEpType = (UINT8)(pHCDPipe->uEpXferType + 1);
                }
            else
                {
                pHCDPipe->uEpType = (UINT8)(USB_XHCI_CTRL_EP | (pHCDPipe->uEpXferType + 1));
                }
            }
        }

    /* Get the new value for EP context Interval field */

    uNewValue8 = usbXhcdEpIntervalGet(pHCDPipe->uDevSpeed,
                                     pHCDPipe->uEpDir,
                                     pHCDPipe->uEpXferType,
                                     pEpDesc->bInterval);

    if (pHCDPipe->uInterval != uNewValue8)
        {
        USB_XHCD_WARN("usbXhcdPipeUpdate - "
                      "EP 0x%02x uInterval changed from %d to %d\n",
                      pHCDPipe->uEpAddr,
                      pHCDPipe->uInterval,
                      uNewValue8, 4, 5, 6);

        /* Set it as need to be changed */

        bChanged = TRUE;

        /* Apply the change in software side */

        pHCDPipe->uInterval = uNewValue8;
        }

    /*
     * SuperSpeed ISOCHRONOUS endpoint has a Mult filed in its companion
     * descriptor, which is a zero based value that determines the maximum
     * number of packets within a service interval that this endpoint
     * supports.
     *
     * Maximum number of packets = bMaxBurst * (Mult + 1)
     */

    if ((pHCDPipe->uDevSpeed == USBHST_SUPER_SPEED) &&
        (pHCDPipe->uEpXferType == USBHST_ISOCHRONOUS_TRANSFER))
        {
        uNewValue8 = (UINT8)(pEpCompanionDesc->bmAttributes &
                             USBHST_SS_EP_COMPANION_ATTR_MULT_MASK);
        }
    else
        {
        uNewValue8 = 0;
        }

    if (pHCDPipe->uMult != uNewValue8)
        {
        USB_XHCD_WARN("usbXhcdPipeUpdate - "
                      "EP 0x%02x uMult changed from %d to %d\n",
                      pHCDPipe->uEpAddr,
                      pHCDPipe->uMult,
                      uNewValue8, 4, 5, 6);

        /* Set it as need to be changed */

        bChanged = TRUE;

        /* Apply the change in software side */

        pHCDPipe->uMult = uNewValue8;
        }

    /*
     * SuperSpeed Bulk endpoint may support Streams, indicated by its
     * companion descriptor MaxStreams field, which is the maximum
     * number of streams this endpoint supports. Valid values are
     * from 0 to 16, where a value of 0 indicates that the endpoint
     * does not define streams. For the values 1 to 16, the number
     * of streams supported equals 2^MaxStreams.
     */

    if ((pHCDPipe->uDevSpeed == USBHST_SUPER_SPEED) &&
        (pHCDPipe->uEpXferType == USBHST_BULK_TRANSFER))
        {
        UINT8 uMaxPStreams = (UINT8)(pEpCompanionDesc->bmAttributes &
                                     USBHST_SS_EP_COMPANION_ATTR_MAX_STREAMS_MASK);
        /*
         * A value of 0 in MaxStreams indicates that the endpoint does
         * not define streams. For the values 1 to 16, the number of
         * streams supported equals 2^MaxStreams.
         */

        if (uMaxPStreams != 0)
            {
            /*
             * For SS Bulk endpoints, the range of valid values for
             * this field is defined by the MaxPSASize field in the
             * HCCPARAMS register.
             */

            uNewValue8 = (UINT8)min(uMaxPStreams,
                USB_XHCI_MAX_PSA_SIZE(pHCDData->uHCCPARAMS));
            }
        else
            {
            uNewValue8 = 0;
            }
        }
    else
        {
        uNewValue8 = 0;
        }

    if (pHCDPipe->uMaxPStreams != uNewValue8)
        {
        USB_XHCD_WARN("usbXhcdPipeUpdate - "
                      "EP 0x%02x uMaxPStreams changed from %d to %d\n",
                      pHCDPipe->uEpAddr,
                      pHCDPipe->uMaxPStreams,
                      uNewValue8, 4, 5, 6);

        /* Set it as need to be changed */

        bChanged = TRUE;

        /* Apply the change in software side */

        pHCDPipe->uMaxPStreams = uNewValue8;
        }

    /* Linear Stream Array identifies how a Stream ID shall be interpreted */

    if (pHCDPipe->uMaxPStreams != 0)
        uNewValue8 = 1; /* TODO: This could be user configurable */
    else
        uNewValue8 = 0;

    if (pHCDPipe->uLSA != uNewValue8)
        {
        USB_XHCD_WARN("usbXhcdPipeUpdate - "
                      "EP 0x%02x uLSA changed from %d to %d\n",
                      pHCDPipe->uEpAddr,
                      pHCDPipe->uLSA,
                      uNewValue8, 4, 5, 6);

        /* Set it as need to be changed */

        bChanged = TRUE;

        /* Apply the change in software side */

        pHCDPipe->uLSA = uNewValue8;
        }

    /*
     * Store the maximum packet size. This field shall be set to the value
     * defined in bits 10:0 of the USB Endpoint Descriptor wMaxPacketSize
     * field. However, we initially set it to be the wMaxPacketSize, it
     * will be masked in following code after uMaxBusrtSize is calculated.
     */

    uNewValue16 = (OS_UINT16_LE_TO_CPU(pEpDesc->wMaxPacketSize));

    /* Get the proper uMaxBusrtSize according to speed */

    if ((pHCDPipe->uDevSpeed == USBHST_SUPER_SPEED) ||
        (pHCDPipe->uDevSpeed == USBHST_HIGH_SPEED))
        {
        /* FS endpoints should split wMaxPacketSize into two parts */

        if (pHCDPipe->uDevSpeed == USBHST_HIGH_SPEED)
            {
            /* Get uMaxBusrtSize from bits 12:11 of wMaxPacketSize */

            uNewValue8 = (UINT8)((uNewValue16 &
                USB_ENDPOINT_NUM_TRANSACTIONS_MASK) >>
                USB_ENDPOINT_NUM_TRANSACTIONS_OFFSET);

            /* Get real uNewValue16 from bits 10:0 of wMaxPacketSize */

            uNewValue16 = (UINT16)(uNewValue16 & USB_ENDPOINT_MAX_PACKET_SIZE_MASK);
            }
        else
            {
            /* SS endpoints use bMaxBurst from EP companion descriptor */

            uNewValue8 = pEpCompanionDesc->bMaxBurst;
            }
        }
    else
        {
        /* For all LS/FS endpoints this field shall be cleared to '0' */

        uNewValue8 = 0;
        }

    if (pHCDPipe->uMaxPacketSize != uNewValue16)
        {
        USB_XHCD_WARN("usbXhcdPipeUpdate - "
                      "EP 0x%02x uMaxPacketSize changed from %d to %d\n",
                      pHCDPipe->uEpAddr,
                      pHCDPipe->uMaxPacketSize,
                      uNewValue16, 4, 5, 6);

        /* Set it as need to be changed */

        bChanged = TRUE;

        /* Apply the change in software side */

        pHCDPipe->uMaxPacketSize = uNewValue16;
        }

    if (pHCDPipe->uMaxBusrtSize != uNewValue8)
        {
        USB_XHCD_WARN("usbXhcdPipeUpdate - "
                      "EP 0x%02x uMaxBusrtSize changed from %d to %d\n",
                      pHCDPipe->uEpAddr,
                      pHCDPipe->uMaxBusrtSize,
                      uNewValue8, 4, 5, 6);

        /* Set it as need to be changed */

        bChanged = TRUE;

        /* Apply the change in software side */

        pHCDPipe->uMaxBusrtSize = uNewValue8;
        }

    if ((pHCDPipe->uEpXferType == USBHST_INTERRUPT_TRANSFER) ||
        (pHCDPipe->uEpXferType == USBHST_ISOCHRONOUS_TRANSFER))
        {
        if (pHCDPipe->uDevSpeed == USBHST_SUPER_SPEED)
            {
            pHCDPipe->uMaxESIT =
                OS_UINT16_LE_TO_CPU(pEpCompanionDesc->wBytesPerInterval);
            }
        else
            {
            pHCDPipe->uMaxESIT = (UINT16)(pHCDPipe->uMaxPacketSize *
                (pHCDPipe->uMaxBusrtSize + 1));
            }
        }

    /* Check if anything changed */

    if (bChanged == TRUE)
        {
        /* When changed, the Endpoint Context is to be modified */

        pHCDPipe->uEpCtxState = USB_XHCD_EP_CTX_STATE_MOD;
        }
    else
        {
        /* When nothing changed, the Endpoint Context is kept as is */

        pHCDPipe->uEpCtxState = USB_XHCD_EP_CTX_STATE_OLD;
        }

    return bChanged;
    }

/*******************************************************************************
*
* usbXhcdSetupControlPipe - allocate transfer resources for a control pipe
*
* This routine is used to allocate transfer resources for a cotnrol pipe.
* The transfer resources, including request info structures and associated DMA
* tag and DMA map are created. This is done by calling usbEhcdSetupPipe with
* maxiumn trasnfer request size for control pipe.
*
* RETURNS: USBHST_SUCCESS when all resources allocation is done
*          USBHST_FAILURE if the pipe specified is not a control pipe
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdSetupControlPipe
    (
    pUSB_XHCD_DATA pHCDData,
    pUSB_XHCD_PIPE pHCDPipe
    )
    {
    /*
     * Set up the transfer characteristics such as
     * creating proper DMA TAG and DMA MAPs
     */

    if (pHCDPipe->uEpXferType == USBHST_CONTROL_TRANSFER)
        {
        USB_TRANSFER_SETUP_INFO pipeSetupInfo;

        pUSB_TRANSFER_SETUP_INFO pSetupInfo = &pipeSetupInfo;

        /* Setup a default transfer limitations */

        OS_MEMSET(&pipeSetupInfo, 0, sizeof(USB_TRANSFER_SETUP_INFO));

        /* Control transfer will have a UINT16 limit in the Data phase */

        pSetupInfo->uMaxTransferSize = USB_XHCD_CTRL_MAX_DATA_SIZE;

        /* We allow only 1 control request for one pipe at a time */

        pSetupInfo->uMaxNumReqests = 1;

        /* No special flags */

        pSetupInfo->uFlags = 0;

        /* Setup the pipe */

        return usbXhcdSetupPipe(pHCDData, pHCDPipe, pSetupInfo);
        }

    return ERROR;
    }

/*******************************************************************************
*
* usbXhcdDefaultPipeCreate - create the HCD default pipe structure
*
* This routine is to create the HCD default pipe structure.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdDefaultPipeDestroy
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    if (pHCDData->pDefaultUSB3Pipe != NULL)
        {
        /* Destroy the default pipe */

        if (usbXhcdPipeDestroy(pHCDData, pHCDData->pDefaultUSB3Pipe) != OK)
            {
            USB_XHCD_ERR("usbXhcdDefaultPipeDestroy - destroy default USB3 pipe failed\n",
                          1, 2, 3, 4, 5 ,6);
            }
        }

    if (pHCDData->pDefaultUSB2Pipe != NULL)
        {
        /* Destroy the default pipe */

        if (usbXhcdPipeDestroy(pHCDData, pHCDData->pDefaultUSB2Pipe) != OK)
            {
            USB_XHCD_ERR("usbXhcdDefaultPipeDestroy - destroy default USB2 pipe failed\n",
                          1, 2, 3, 4, 5 ,6);
            }
        }

    return OK;
    }

/*******************************************************************************
*
* usbXhcdDefaultPipeCreate - create the HCD default pipe structure
*
* This routine is to create the HCD default pipe structure.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdDefaultPipeCreate
    (
    pUSB_XHCD_DATA  pHCDData
    )
    {
    pUSB_XHCD_PIPE              pHCDPipe = NULL;
    UINT32 uIndex = 0;

    USBHST_ENDPOINT_DESCRIPTOR  ctrlEpDesc =
        {
        USB_ENDPOINT_DESCR_LEN,                 /* bLength */
        USBHST_ENDPOINT_DESC,                   /* Endpoint Descritpor Type */
        0x00,                                   /* Endpoint Address */
        USBHST_CONTROL_TRANSFER,                /* bmAttributes */
        USB_MAX_CTRL_PACKET_SIZE,               /* wMaxPacketSize */
        0                                       /* bInterval */
        };

    /* Create two default pipes for both USB2 and USB3 */

    for (uIndex = 0; uIndex < 2; uIndex++)
        {
        /*
         * Create the default pipe with a NULL device and
         * default endpoint descriptor
         */

        pHCDPipe = usbXhcdPipeCreate(pHCDData, NULL, &ctrlEpDesc);

        if (pHCDPipe == NULL)
            {
            USB_XHCD_ERR("usbXhcdDefaultPipeCreate - create default pipe failed\n",
                          1, 2, 3, 4, 5 ,6);

            if (usbXhcdDefaultPipeDestroy(pHCDData) != OK)
                {
                USB_XHCD_WARN("usbXhcdDefaultPipeCreate - destroy default pipe fail\n",
                              1, 2, 3, 4, 5 ,6);
                }

            return ERROR;
            }

        /* Set it as the HCD default pipe */

        if (pHCDData->pDefaultUSB3Pipe == NULL)
            pHCDData->pDefaultUSB3Pipe = pHCDPipe;
        else
            pHCDData->pDefaultUSB2Pipe = pHCDPipe;

        if (usbXhcdSetupControlPipe(pHCDData, pHCDPipe) != USBHST_SUCCESS)
            {
            USB_XHCD_ERR("usbXhcdCreatePipe - "
                         "usbXhcdSetupControlPipe for default pipe fail\n",
                         1, 2, 3, 4, 5 ,6);

            if (usbXhcdDefaultPipeDestroy(pHCDData) != OK)
                {
                USB_XHCD_WARN("usbXhcdDefaultPipeCreate - destroy default pipe fail\n",
                              1, 2, 3, 4, 5 ,6);
                }

            return ERROR;
            }
        }

    USB_XHCD_VDBG("usbXhcdDefaultPipeCreate - default pipe %p created\n",
                  pHCDPipe, 2, 3, 4, 5 ,6);

    return OK;
    }

/*******************************************************************************
*
* usbXhcdPipeRingDestroy - destroy the pipe ring structure
*
* This routine is to destroy the pipe ring structure.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdPipeRingDestroy
    (
    pUSB_XHCD_DATA  pHCDData,
    pUSB_XHCD_PIPE  pHCDPipe
    )
    {
    /* Stream ID */

    UINT32 uStreamId;

    /* Endpoint Streams info */

    pUSB_XHCD_STREAM_INFO   pStreamInfo;

    if (pHCDPipe->pEpRing != NULL)
        {
        if (usbXhcdRingDestroy(pHCDData, pHCDPipe->pEpRing) != OK)
            {
            USB_XHCD_ERR("usbXhcdPipeRingDestroy - "
                "usbXhcdRingDestroy pEpRing fail\n",
                1, 2, 3, 4, 5, 6);

            return ERROR;
            }

        pHCDPipe->pEpRing = NULL;
        }

    /* Get the stream info pointer */

    pStreamInfo = &pHCDPipe->streamInfo;

    if (pStreamInfo->pStreamCtxs)
        {
        /* Free the memory allocated */

        if (vxbDmaBufMemFree(pStreamInfo->ctxDmaTagId,
            pStreamInfo->pStreamCtxs, pStreamInfo->ctxDmaMapId) != OK)
            {
            USB_XHCD_ERR("usbXhcdPipeRingDestroy - vxbDmaBufMemFree fail\n",
                0, 0, 0, 0, 0, 0);

            return ERROR;
            }

        pStreamInfo->pStreamCtxs = NULL;
        }

    /* Destroy the Transfer Rings */

    if (pStreamInfo->ppStreamRings != NULL)
        {
        for (uStreamId = 0; uStreamId < pStreamInfo->uNumStreams; uStreamId++)
            {
            if (pStreamInfo->ppStreamRings[uStreamId] != NULL)
                {
                if (usbXhcdRingDestroy(pHCDData,
                    pStreamInfo->ppStreamRings[uStreamId]) != OK)
                    {
                    USB_XHCD_ERR("usbXhcdPipeRingDestroy - "
                        "usbXhcdRingDestroy ppStreamRings[%d] fail\n",
                        uStreamId, 2, 3, 4, 5, 6);

                    return ERROR;
                    }

                pStreamInfo->ppStreamRings[uStreamId] = NULL;
                }
            }

        OSS_FREE(pStreamInfo->ppStreamRings);

        pStreamInfo->ppStreamRings = NULL;
        }

    return OK;
    }

/*******************************************************************************
*
* usbXhcdPipeRingCreate - create the pipe ring structure
*
* This routine is to create the pipe ring structure.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdPipeRingCreate
    (
    pUSB_XHCD_DATA  pHCDData,
    pUSB_XHCD_PIPE  pHCDPipe
    )
    {
    /* Transfer Ring */

    pUSB_XHCD_RING          pRing;

    /* Stream Context */

    pUSB_XHCI_STREAM_CTX    pStreamCtx;

    /* Stream ID */

    UINT32                  uStreamId;

    /* Endpoint Streams info */

    pUSB_XHCD_STREAM_INFO   pStreamInfo;

    /* Number of segments per ring */

    UINT32                  uNumSegs;

    /* Set the number of segments according to transfer type */

    if (pHCDPipe->uEpXferType != USBHST_ISOCHRONOUS_TRANSFER)
        {
        uNumSegs = USB_XHCD_NON_ISOCHRONOUS_RING_MAX_SEGS;
        }
    else
        {
        uNumSegs = USB_XHCD_ISOCHRONOUS_RING_MAX_SEGS;
        }

    /* If the pipe does not support Streams, we just create a normal ring */

    if (pHCDPipe->uMaxPStreams == 0)
        {
        pRing = usbXhcdRingCreate(pHCDData, USB_XHCD_XFER_RING, uNumSegs, TRUE);

        if (pRing == NULL)
            {
            USB_XHCD_ERR("usbXhcdPipeRingCreate - "
                "usbXhcdRingCreate pRing fail\n",
                1, 2, 3, 4, 5, 6);

            return ERROR;
            }

        pHCDPipe->pEpRing = pRing;
        }
    else
        {
        /* vxBus call return status */

        STATUS vxbSts;

        /* Max size for Streams Context Array */

        UINT32 uMaxCtxSize;

        /* Get the stream info pointer */

        pStreamInfo = &pHCDPipe->streamInfo;

        /* Save the number of Streams */

        pStreamInfo->uNumStreamCtxs =
            pStreamInfo->uNumStreams = (1 << (pHCDPipe->uMaxPStreams + 1));

        /* Choose between Steam Array (Pri/Sec) and Steam Array (Linear) */

        if (pStreamInfo->uNumStreams <= USB_XHCI_PSCA_MAX_CTX)
            {
            uMaxCtxSize = USB_XHCI_PSCA_MAX_SIZE;

            pStreamInfo->ctxDmaTagId = pHCDData->pscaDmaTagId;
            }
        else
            {
            uMaxCtxSize = USB_XHCI_LSCA_MAX_SIZE;
            pStreamInfo->ctxDmaTagId = pHCDData->lscaDmaTagId;
            }

        /* Allocate the memory for Streams Contexts */

        pStreamInfo->pStreamCtxs = vxbDmaBufMemAlloc (pHCDData->pDev,
                pStreamInfo->ctxDmaTagId,
                NULL,
                VXB_DMABUF_MEMALLOC_CLEAR_BUF,
                &pStreamInfo->ctxDmaMapId);

        if (pStreamInfo->pStreamCtxs == NULL)
            {
            USB_XHCD_ERR(
                "usbXhcdPipeRingCreate - creating pStreamCtxs fail\n",
                1, 2, 3, 4, 5, 6);

            (void) usbXhcdPipeRingDestroy(pHCDData, pHCDPipe);

            return ERROR;
            }

        /*
         * Load the DMA MAP so that we get the correct BUS address
         * to program the hardware.
         */

        vxbSts = vxbDmaBufMapLoad (pHCDData->pDev,
            pStreamInfo->ctxDmaTagId,
            pStreamInfo->ctxDmaMapId,
            pStreamInfo->pStreamCtxs,
            uMaxCtxSize,
            0);

        if (vxbSts != OK)
            {
            USB_XHCD_ERR(
                "usbXhcdScratchPadDmaBufInit - loading ctxDmaMapId fail\n",
                1, 2, 3, 4, 5, 6);

            (void) usbXhcdPipeRingDestroy(pHCDData, pHCDPipe);

            return ERROR;
            }

        /* Save the BUS address of the Stream Context Array */

        pStreamInfo->uStreamCtxs =
            pStreamInfo->ctxDmaMapId->fragList[0].frag;

        pStreamInfo->ppStreamRings = OSS_CALLOC(pStreamInfo->uNumStreams *
            (sizeof(pUSB_XHCD_RING)));

        if (pStreamInfo->ppStreamRings == NULL)
            {
            USB_XHCD_ERR("usbXhcdPipeRingCreate - "
                "Allocate ppStreamRings fail\n",
                1, 2, 3, 4, 5, 6);

            (void) usbXhcdPipeRingDestroy(pHCDData, pHCDPipe);

            return ERROR;
            }

        /* Create the Transfer Rings */

        for (uStreamId = 0; uStreamId < pStreamInfo->uNumStreams; uStreamId++)
            {
            pRing = usbXhcdRingCreate(pHCDData, USB_XHCD_XFER_RING, uNumSegs, TRUE);

            if (pRing == NULL)
                {
                USB_XHCD_ERR("usbXhcdPipeRingCreate - "
                    "usbXhcdRingCreate pRing fail for uStreamId %d\n",
                    uStreamId, 2, 3, 4, 5, 6);

                (void) usbXhcdPipeRingDestroy(pHCDData, pHCDPipe);

                return ERROR;
                }

            pStreamInfo->ppStreamRings[uStreamId] = pRing;

            pStreamCtx = &pStreamInfo->pStreamCtxs[uStreamId];

            pStreamCtx->uStreamDeqPtr = USB_XHCD_SWAP_DESC_DATA64(pHCDData,
                (USB_XHCI_STREAM_CTX_TR_DEQ_PTR(pRing->pHeadSeg->uHeadTRB) |
                 USB_XHCI_STREAM_CTX_SCT(USB_XHCI_SCT_PRIMARY) |
                 USB_XHCI_STREAM_CTX_DCS(pRing->uCycle)));

            pStreamCtx->uRsvdO[0] = pStreamCtx->uRsvdO[1] = 0;

            USB_XHCD_VDBG("usbXhcdPipeRingCreate - "
                "uSlotId %d uEpAddr 0x%02x uEpId %d uStreamId %d "
                "pStreamCtx @%p uStreamDeqPtr %p\n",
                pHCDPipe->uSlotId,
                pHCDPipe->uEpAddr,
                pHCDPipe->uEpId,
                uStreamId,
                pStreamCtx,
                pStreamCtx->uStreamDeqPtr);
            }
        }

    return OK;
    }

/*******************************************************************************
*
* usbXhcdPipeRequestFind - find containing request for a given TRB bus address
*
* This routine is to find containing request for a given TRB bus address.
*
* RETURNS: Pointer to the request information structure, NULL when not found.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbXhcdPipeRequestCleanup
    (
    pUSB_XHCD_DATA          pHCDData,
    pUSB_XHCD_PIPE          pHCDPipe,
    pUSB_XHCD_REQUEST_INFO  pRequest
    )
    {

    }

/*******************************************************************************
*
* usbXhcdPipeRequestFind - find containing request for a given TRB bus address
*
* This routine is to find containing request for a given TRB bus address.
*
* RETURNS: Pointer to the request information structure, NULL when not found.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_XHCD_REQUEST_INFO usbXhcdPipeRequestFind
    (
    pUSB_XHCD_DATA          pHCDData,
    UINT8                   uSlotId,
    UINT8                   uEpId,
    UINT64                  uTrbBusAddr
    )
    {
    pUSB_XHCD_DEVICE        pHCDDevice;
    pUSB_XHCD_PIPE          pHCDPipe;
    pUSB_XHCD_REQUEST_INFO  pRequest;
    NODE *                  pReqNode;
    pUSB_XHCD_SEGMENT       pNextSeg;

    /* Find the device */

    pHCDDevice = pHCDData->ppDevSlots[uSlotId];

    if (pHCDDevice == NULL)
        {
        USB_XHCD_ERR("usbXhcdPipeRequestFind - "
            "Device slot for Slot ID %d NULL\n",
            uSlotId, 2, 3, 4, 5, 6);

        return NULL;
        }

    /* Find the pipe */

    pHCDPipe = pHCDDevice->pDevPipes[uEpId];

    if (pHCDPipe == NULL)
        {
        USB_XHCD_ERR("usbXhcdPipeRequestFind - "
            "Device pipe for Slot ID %d EP ID %d NULL\n",
            uSlotId, uEpId, 3, 4, 5, 6);

        return NULL;
        }

    /* Exclusively access the pipe request list */

    (void) OS_WAIT_FOR_EVENT(pHCDPipe->pipeMutex, OS_WAIT_INFINITE);

    /* Get the first active node */

    pReqNode = lstFirst(&pHCDPipe->activeReqList);

    while (pReqNode != NULL)
        {
        /* Cast to the request info */

        pRequest = USB_XHCD_EP_NODE_TO_REQ_INFO(pReqNode);

        /* Start comparing from the Head Segment of the request */

        pNextSeg = pRequest->pHeadSeg;

        while (pNextSeg != NULL)
            {
            /*
             * If the TRB falls into the range of this Segment,
             * the corresponding request is our candicate request.
             */

            if ((uTrbBusAddr >= pNextSeg->uHeadTRB) &&
                (uTrbBusAddr <= pNextSeg->uTailTRB))
                {
                /* Release the exclusive access */

                OS_RELEASE_EVENT(pHCDPipe->pipeMutex);

                return pRequest;
                }

            /*
             * If we have reached the Tail Segment of this request
             * but still not found, this request is not our dish.
             * We break out to try next active request on the pipe.
             */

            if (pNextSeg == pRequest->pTailSeg)
                {
                USB_XHCD_VDBG("usbXhcdPipeRequestFind - "
                    "pRequest %p for Slot ID %d EP ID %d has no TRB @%p[BUS]\n",
                    pRequest, uSlotId, uEpId, uTrbBusAddr, 5, 6);

                break;
                }

            /* Go to the next Segment */

            pNextSeg = pNextSeg->pNextSeg;
            }

        /* Find the next request on the active request list */

        /* Get the next active node */

        pReqNode = lstNext(pReqNode);
        }

    /* Release the exclusive access */

    OS_RELEASE_EVENT(pHCDPipe->pipeMutex);

    USB_XHCD_DBG("usbXhcdPipeRequestFind - "
        "Slot ID %d EP ID %d has NO request containing TRB @%p[BUS]\n",
        uSlotId, uEpId, uTrbBusAddr, 4, 5, 6);

    return NULL;
    }

/*******************************************************************************
*
* usbXhcdDeviceAddressGet - get the device address from output context structure
*
* This routine is to get the device address from output context structure.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdDeviceAddressGet
    (
    pUSB_XHCD_DATA  pHCDData,
    UINT32          uSlotId,
    UINT8 *         pDevAddress
    )
    {
    pUSB_XHCD_DEVICE            pHCDDevice;
    pUSB_XHCI_SLOT_CTX          pOutputSlotCtx;

    pHCDDevice = pHCDData->ppDevSlots[uSlotId];

    if (pHCDDevice == NULL)
        {
        USB_XHCD_ERR("usbXhcdDeviceAddressGet - "
            "Device slot for Slot ID %d NULL\n",
            uSlotId, 2, 3, 4, 5, 6);

        return ERROR;
        }

    pOutputSlotCtx = USB_XHCI_SLOT_CTX_CAST(pHCDData, pHCDDevice->pOutputCtx);

    *pDevAddress = (UINT8)USB_XHCI_SLOT_CTX_DEV_ADDR_GET(
        USB_XHCD_SWAP_DESC_DATA32(pHCDData, pOutputSlotCtx->uSlotInfo3));

    return OK;
    }

/*******************************************************************************
*
* usbXhcdPipeStateGet - get pipe endpoint state from output endpoint context
*
* This routine is to get pipe endpoint state from output endpoint context.
*
* RETURNS: pipe state.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

UINT8 usbXhcdPipeStateGet
    (
    pUSB_XHCD_DATA  pHCDData,
    pUSB_XHCD_PIPE  pHCDPipe
    )
    {
    pUSB_XHCD_DEVICE    pDevice;
    pUSB_XHCI_EP_CTX    pOutputEpCtx;
    UINT8               uEpState;

    pDevice = pHCDData->ppDevSlots[pHCDPipe->uSlotId];

    if (pDevice == NULL)
        {
        USB_XHCD_ERR("usbXhcdDeviceAddressGet - "
            "Device slot for Slot ID %d NULL\n",
            pHCDPipe->uSlotId, 2, 3, 4, 5, 6);

        return USB_XHCI_EP_STATE_DISABLED;
        }

    if (pHCDPipe->uEpXferType == USBHST_CONTROL_TRANSFER)
        {
        pOutputEpCtx = USB_XHCI_EP_CTX_CAST(pHCDData,
                                            pDevice->pOutputCtx,
                                            pHCDPipe->uEpId);
        }
    else
        {
        pOutputEpCtx = USB_XHCI_EP_CTX_CAST(pHCDData,
                                            pDevice->pOutputCtx,
                                            pHCDPipe->uEpId);
        }

    uEpState = (UINT8)USB_XHCI_EP_CTX_EP_STATE_GET(
               USB_XHCD_SWAP_DESC_DATA32(pHCDData, pOutputEpCtx->uEpInfo0));

    return uEpState;
    }

/*******************************************************************************
*
* usbXhcdEpCtxCommit - commit new or changed Endpoint Context values
*
* This routine is to commit new or changed Endpoint Context values.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdEpCtxCommit
    (
    pUSB_XHCD_DATA                  pHCDData,
    pUSB_XHCD_DEVICE                pHCDDevice,
    pUSB_XHCD_PIPE                  pHCDPipe
    )
    {
    pUSB_XHCI_EP_CTX                pEpCtx = NULL;

    pEpCtx = USB_XHCI_EP_CTX_CAST(pHCDData,
                                  pHCDDevice->pIntputCtx,
                                  pHCDPipe->uEpId);
    pEpCtx->uEpInfo0 = USB_XHCD_SWAP_DESC_DATA32(pHCDData,
                           (USB_XHCI_EP_CTX_INTERVAL(pHCDPipe->uInterval) |
                           USB_XHCI_EP_CTX_MAX_PRI_STREAMS(pHCDPipe->uMaxPStreams) |
                           USB_XHCI_EP_CTX_MULT(pHCDPipe->uMult)|
                           USB_XHCI_EP_CTX_LSA(pHCDPipe->uLSA) |
                           USB_XHCI_EP_CTX_EP_STATE(pHCDPipe->uEpState)));

    pEpCtx->uEpInfo1 = USB_XHCD_SWAP_DESC_DATA32(pHCDData,
                           (USB_XHCI_EP_CTX_EP_TYPE(pHCDPipe->uEpType) |
                           USB_XHCI_EP_CTX_MAX_PACKET_SIZE(pHCDPipe->uMaxPacketSize) |
                           USB_XHCI_EP_CTX_MAX_BURST_SIZE(pHCDPipe->uMaxBusrtSize) |
                           USB_XHCI_EP_CTX_CERR(pHCDPipe->uCErr) |
                           USB_XHCI_EP_CTX_HID(pHCDPipe->uHID)));

    if (pHCDPipe->uMaxPStreams > 0)
        {
        /*
         * If MaxPStreams > '0' then this field shall point to a
         * Stream Context Array. Dequeue Cycle State (DCS) shall
         * be '0' if MaxPStreams > '0'
         */
        pEpCtx->uEpDeqLo = USB_XHCD_SWAP_DESC_DATA32(pHCDData,
                           (USB_XHCD_LO32(pHCDPipe->streamInfo.uStreamCtxs) |
                            USB_XHCI_EP_CTX_DCS(0)));
        pEpCtx->uEpDeqHi = USB_XHCD_SWAP_DESC_DATA32(pHCDData,
                           (USB_XHCD_HI32(pHCDPipe->streamInfo.uStreamCtxs)));
        }
    else
        {
        pEpCtx->uEpDeqLo = USB_XHCD_SWAP_DESC_DATA32(pHCDData,
                           (USB_XHCD_LO32(pHCDPipe->pEpRing->pEnqSeg->uHeadTRB) |
                            USB_XHCI_EP_CTX_DCS(pHCDPipe->pEpRing->uCycle)));
        pEpCtx->uEpDeqHi = USB_XHCD_SWAP_DESC_DATA32(pHCDData,
                           (USB_XHCD_HI32(pHCDPipe->pEpRing->pEnqSeg->uHeadTRB)));
        }

    pEpCtx->uEpInfo2 = USB_XHCD_SWAP_DESC_DATA32(pHCDData,
                           (USB_XHCI_EP_CTX_AVRG_TRB_LEN(pHCDPipe->uAvrgLen) |
                            USB_XHCI_EP_CTX_MAX_ESIT_PAYLOAD(pHCDPipe->uMaxESIT)));

    USB_XHCD_DBG("usbXhcdEpCtxCommit - EP ID %d "
        "uEpInfo0 0x%08x uEpInfo1 0x%08x uEpDeqPtr 0x%08x : %08x uEpInfo2 0x%08x\n",
        pHCDPipe->uEpId,
        USB_XHCD_SWAP_DESC_DATA32(pHCDData, pEpCtx->uEpInfo0),
        USB_XHCD_SWAP_DESC_DATA32(pHCDData, pEpCtx->uEpInfo1),
        USB_XHCD_SWAP_DESC_DATA32(pHCDData, pEpCtx->uEpDeqHi),
        USB_XHCD_SWAP_DESC_DATA32(pHCDData, pEpCtx->uEpDeqLo),
        USB_XHCD_SWAP_DESC_DATA32(pHCDData, pEpCtx->uEpInfo2));

    return OK;
    }

/*******************************************************************************
*
* usbXhcdInterfaceScan - scan an interface for bandwidth requirement
*
* This routine is to scan an interface for bandwidth requirement.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdInterfaceScan
    (
    pUSB_XHCD_DATA                  pHCDData,
    pUSB_XHCD_DEVICE                pHCDDevice,
    pUSBHST_INTERFACE_DESCRIPTOR    pIfDesc
    )
    {
    /* Pointer to the endpoint descriptor */

    pUSBHST_ENDPOINT_DESCRIPTOR pEpDesc = NULL;

    /* Pointer to the pipe structure */

    pUSB_XHCD_PIPE  pHCDPipe = NULL;

    /* Total # of endpoints present in the interface */

    UINT8 bNumEndpoints = 0;

    /* Endpoint index */

    UINT8 uEpIndex = 0;

    /* Endpoint ID (DCI) */

    UINT8 uEpId = 0;

    /* Endpoint Transfer Type */

    UINT8 uEpXferType = 0;

    /* New interface information */

    USB_XHCD_INTERFACE xInterface;

    /* Pointer to current interface information */

    pUSB_XHCD_INTERFACE pInterface;

    /* Set the Hub field if it is Hub class */

    if (pIfDesc->bInterfaceClass == USBHST_HUB_CLASS)
        {
        pHCDDevice->uHub = 1;
        }

    /* Clear the New interface information */

    OS_MEMSET(&xInterface, 0, sizeof(USB_XHCD_INTERFACE));

    /* Get the pointer to current interface */

    pInterface = &pHCDDevice->xInterfaces[pIfDesc->bInterfaceNumber];

    /* Get the number of endpoints in this interface */

    bNumEndpoints = pIfDesc->bNumEndpoints;

    /* Start from the first endpoint */

    pEpDesc = (pUSBHST_ENDPOINT_DESCRIPTOR)
        ((UINT8 *)(pIfDesc) + pIfDesc->bLength);

    for (uEpIndex = 0; uEpIndex < bNumEndpoints; uEpIndex++)
        {
        /* Go to the next Endpoint descriptor */

        while (pEpDesc->bDescriptorType != USBHST_ENDPOINT_DESC)
            {
            if (pEpDesc->bLength == 0)
                {
                USB_XHCD_ERR("usbXhcdInterfaceScan - "
                    "pEpDesc @%p is corrupted\n",
                    pEpDesc, 2, 3, 4, 5, 6);

                return ERROR;
                }

            pEpDesc = (pUSBHST_ENDPOINT_DESCRIPTOR)
                ((UINT8 *)(pEpDesc) + pEpDesc->bLength);
            }

        if (pEpDesc->bLength == 0)
            {
            USB_XHCD_ERR("usbXhcdInterfaceScan - "
                "pEpDesc @%p is corrupted\n",
                pEpDesc, 2, 3, 4, 5, 6);

            return ERROR;
            }

        uEpXferType = (UINT8)(pEpDesc->bmAttributes & USB_ATTR_EPTYPE_MASK);

        if (uEpXferType == USBHST_CONTROL_TRANSFER)
           {
           uEpId = (UINT8)USB_XHCI_CTRL_EP_CTX_DCI(pEpDesc->bEndpointAddress);
           }
        else
           {
           uEpId = (UINT8)USB_XHCI_NON_CTRL_EP_CTX_DCI(pEpDesc->bEndpointAddress);
           }

        /* Indicate this endpoint is in use */

        xInterface.uEndpoints[uEpId] = uEpId;

        if (pHCDDevice->pDevPipes[uEpId] == NULL)
            {
            /* Create the new pipe */

            pHCDDevice->pDevPipes[uEpId] = usbXhcdPipeCreate(pHCDData,
                                                             pHCDDevice,
                                                             pEpDesc);

            if (pHCDDevice->pDevPipes[uEpId] == NULL)
                {
                USB_XHCD_ERR("usbXhcdInterfaceScan - "
                             "create pipe for EP ID %d failed\n",
                              uEpId, 2, 3, 4, 5 ,6);

                return ERROR;
                }

            USB_XHCD_WARN("usbXhcdInterfaceScan - "
                         "interface %d altsetting %d EP ADD - "
                         "dev %d EP ID %d ep 0x%02x pipe %p\n",
                          pIfDesc->bInterfaceNumber,
                          pIfDesc->bAlternateSetting,
                          pHCDDevice->uDevAddr,
                          uEpId,
                          pEpDesc->bEndpointAddress,
                          pHCDDevice->pDevPipes[uEpId]);

            /* Add this Endpoint */

            pHCDDevice->uAddFlags |= USB_XHCI_ADD_EP(uEpId);
            }
        else
            {
            UINT32 uMaxPStreams;

            pHCDPipe = pHCDDevice->pDevPipes[uEpId];

            /* Save the old uMaxPStreams */

            uMaxPStreams = pHCDPipe->uMaxPStreams;

            /* Check if the EP context has any changes */

            if (usbXhcdPipeUpdate (pHCDData,
                                   pHCDPipe,
                                   pEpDesc) == TRUE)
                {
                USB_XHCD_WARN("usbXhcdInterfaceScan - "
                             "interface %d altsetting %d EP MOD - "
                             "dev %d EP ID %d ep 0x%02x pipe %p\n",
                              pIfDesc->bInterfaceNumber,
                              pIfDesc->bAlternateSetting,
                              pHCDDevice->uDevAddr,
                              uEpId,
                              pEpDesc->bEndpointAddress,
                              pHCDPipe);

                /*
                 * If there is any change in uMaxPStreams, we
                 * try to update the Ring structures.
                 */

                if (uMaxPStreams != pHCDPipe->uMaxPStreams)
                    {
                    usbXhcdPipeRingDestroy(pHCDData, pHCDPipe);

                    if (usbXhcdPipeRingCreate(pHCDData, pHCDPipe) != OK)
                        {
                        USB_XHCD_ERR("usbXhcdInterfaceScan - "
                                      "usbXhcdPipeRingCreate fail for EP 0x%02x\n",
                                      pHCDPipe->uEpAddr,
                                      2, 3, 4, 5, 6);

                        return ERROR;
                        }
                    }

                /* Drop this Endpoint */

                pHCDDevice->uDropFlags |= USB_XHCI_DROP_EP(uEpId);

                /* Add this Endpoint */

                pHCDDevice->uAddFlags |= USB_XHCI_ADD_EP(uEpId);
                }
            }

        /* Go to the next Endpoint Descriptor */

        pEpDesc = (pUSBHST_ENDPOINT_DESCRIPTOR)
            ((UINT8 *)(pEpDesc) + pEpDesc->bLength);
        }

    /*
     * Scan all possible endpoints in the current interface
     * Note that EP ID (DCI) starts from 1 which represents
     * the Default Control Endpoint, but we should always
     * keep it, so we try to drop from DCI 2.
     */

    for (uEpId = 2; uEpId < USB_XHCI_MAX_DEV_PIPES; uEpId++)
        {
        /*
         * If an endpoint exists in current interface alternate setting,
         * but does not exist in the new interface alternate setting,
         * then we should drop this endpoint.
         */

        if ((pInterface->uEndpoints[uEpId] != 0) &&
            (xInterface.uEndpoints[uEpId] == 0))
            {
            /* Drop this Endpoint */

            pHCDDevice->uDropFlags |= USB_XHCI_DROP_EP(uEpId);

            /* Get the pipe pointer */

            pHCDPipe = pHCDDevice->pDevPipes[uEpId];

            if (pHCDPipe != NULL)
                {
                /* And mark the pipe to be destroyed */

                pHCDPipe->uEpCtxState = USB_XHCD_EP_CTX_STATE_DEL;
                }
            }
        }

    /* Set the current interface alternate setting to new one */

    pInterface->uAlternateSetting = pIfDesc->bAlternateSetting;
    pInterface->uNumEndpoints = pIfDesc->bNumEndpoints;

    return OK;
    }

/*******************************************************************************
*
* usbXhcdDeviceGet - get the device structure with the specific address
*
* This routine is to get the device structure with the specific address.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_XHCD_DEVICE usbXhcdDeviceGet
    (
    pUSB_XHCD_DATA  pHCDData,
    UINT8           uDevAddr,
    UINT8           uSpeed
    )
    {
    UINT32              uIndex;
    pUSB_XHCD_DEVICE    pDevice;

    for (uIndex = 0; uIndex < pHCDData->uMaxSlots; uIndex++)
        {
        pDevice = pHCDData->ppDevSlots[uIndex];

        if (pDevice == NULL)
            {
            continue;
            }

        if ((pDevice->uDevSpeed == uSpeed) && (pDevice->uDevAddr == uDevAddr))
            return pDevice;
        }

    return NULL;
    }

/*******************************************************************************
*
* usbXhcdHubDeviceUpdate - update hub device context with hub specific info
*
* This routine is to update hub device context with hub specific information.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdHubDeviceUpdate
    (
    pUSB_XHCD_DATA  pHCDData,
    UINT32          uHubAddr,
    UINT32          uNumPorts,
    UINT32          uMTT,
    UINT32          uTTT,
    UINT8           uSpeed
    )
    {
    pUSB_XHCD_DEVICE            pHubDevice;
    pUSB_XHCI_SLOT_CTX          pInputSlotCtx;
    pUSB_XHCI_INPUT_CTRL_CTX    pInputCtrlCtx;

    /*
     * Upon here, we should already have a device structure
     * created for that device during the initial steps!
     */

    pHubDevice = usbXhcdDeviceGet(pHCDData, (UINT8)uHubAddr, uSpeed);

    if (pHubDevice == NULL)
        {
        USB_XHCD_ERR("usbXhcdHubDeviceUpdate - usbXhcdDeviceGet for dev %d fail\n",
                     uHubAddr, 2, 3, 4, 5 ,6);

        return ERROR;
        }

    /* We got to know this during the hub being configured */

    if (pHubDevice->uHub != 1)
        {
        USB_XHCD_ERR("usbXhcdHubDeviceUpdate - dev %d is not a hub device!\n",
                     uHubAddr, 2, 3, 4, 5 ,6);

        return ERROR;
        }

    pHubDevice->uMTT = (UINT8)uMTT;
    pHubDevice->uTTT = (UINT8)uTTT;
    pHubDevice->uNumPorts = (UINT8)uNumPorts;

    pInputSlotCtx = USB_XHCI_SLOT_CTX_CAST(pHCDData,
                        pHubDevice->pIntputCtx);

    pInputSlotCtx->uSlotInfo0 |= USB_XHCD_SWAP_DESC_DATA32(pHCDData,
        (USB_XHCI_SLOT_CTX_MTT(pHubDevice->uMTT)|
         USB_XHCI_SLOT_CTX_HUB(pHubDevice->uHub)));

    pInputSlotCtx->uSlotInfo1 |= USB_XHCD_SWAP_DESC_DATA32(pHCDData,
        (USB_XHCI_SLOT_CTX_NUM_PORTS(pHubDevice->uNumPorts)));

    pInputSlotCtx->uSlotInfo2 &= USB_XHCD_SWAP_DESC_DATA32(pHCDData,
        (~(USB_XHCI_SLOT_CTX_TT_TT_MASK)));

    pInputSlotCtx->uSlotInfo2 |= USB_XHCD_SWAP_DESC_DATA32(pHCDData,
        (USB_XHCI_SLOT_CTX_TT_TT(pHubDevice->uTTT)));

    pInputCtrlCtx = USB_XHCI_INPUT_CTRL_CTX_CAST(pHCDData,
                        pHubDevice->pIntputCtx);

    /*
     * Make sure the DropFlags are cleared to 0 for
     * Evaluate Context Command
     */

    pInputCtrlCtx->uDropFlags = 0;

    pInputCtrlCtx->uAddFlags = USB_XHCD_SWAP_DESC_DATA32(pHCDData,
        USB_XHCI_ADD_EP(0));
#if 1
    if (usbXhcdQueueConfigureEndpointCmd(pHCDData,
        pHubDevice->pIntputCtx->uCtxBusAddr,
        pHubDevice->uSlotId,
        0,
        USB_XHCD_DEFAULT_CMD_WAIT_TIME) != OK)
        {
        USB_XHCD_WARN("usbXhcdHubDeviceUpdate - Config Endpoint Command fail "
                     "for dev %d slot %d!\n",
                     uHubAddr, pHubDevice->uSlotId, 3, 4, 5 ,6);

        return ERROR;
        }
    else
        {
        USB_XHCD_WARN("usbXhcdHubDeviceUpdate - "
                     "dev %d slot %d is now a working hub device!\n",
                     uHubAddr, pHubDevice->uSlotId, 3, 4, 5 ,6);
        return OK;
        }
  #else
    if (usbXhcdQueueEvaluateContextCmd(pHCDData,
                                   pHubDevice->pIntputCtx->uCtxBusAddr,
                                   pHubDevice->uSlotId,
                                   USB_XHCD_DEFAULT_CMD_WAIT_TIME) != OK)
        {
        USB_XHCD_WARN("usbXhcdHubDeviceUpdate - Evaluate Context Command fail "
                     "for dev %d slot %d!\n",
                     uHubAddr, pHubDevice->uSlotId, 3, 4, 5 ,6);
        return ERROR;
        }
    else
        {
        USB_XHCD_WARN("usbXhcdHubDeviceUpdate - "
                     "dev %d slot %d is now a working hub device!\n",
                     uHubAddr, pHubDevice->uSlotId, 3, 4, 5 ,6);

        return OK;
        }
    #endif
    }

/*******************************************************************************
*
* usbXhcdDeviceSlotInit - initialize the device slot structure
*
* This routine is to initialize the device slot structure.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdDeviceSlotInit
    (
    pUSB_XHCD_DATA  pHCDData,
    UINT32          uRhPort,
    UINT32          uRouteStr,
    UINT32          uTTHubAddr,
    UINT32          uTTHubPort,
    UINT8           uPortSpeed,
    UINT8 *         pDevAddr
    )
    {
    pUSB_XHCD_DEVICE            pNewDevice;
    pUSB_XHCD_DEVICE            pHubDevice;
    pUSB_XHCI_INPUT_CTRL_CTX    pInputCtrlCtx;
    pUSB_XHCI_SLOT_CTX          pInputSlotCtx;
    pUSB_XHCI_EP_CTX            pCtrlEpCtx;
    pUSB_XHCD_RING              pCtrlRing;
    UINT32                      uSlotId;
    UINT16                      uMaxPacketSize0;
    UINT8                       uSlotCtxSpeed;
    pUSB_XHCD_ROOT_HUB_DATA     pRootHub;
    pUSB_XHCD_PIPE              pDefaultPipe;

    /* Try to get a Device Slot ID from xHC */

    if (usbXhcdQueueEnableSlotCmd(pHCDData,
        USB_XHCD_DEFAULT_CMD_WAIT_TIME) != OK)
        {
        USB_XHCD_ERR("usbXhcdDeviceControl - "
            "usbXhcdQueueEnableSlotCmd fail\n",
            0, 0, 0, 0, 0, 0);

        return ERROR;
        }

    /* Retrieve the new Slot ID */

    uSlotId = pHCDData->uNewSlotId;

    USB_XHCD_WARN("usbXhcdDeviceSlotInit - Slot ID %d uPortSpeed %d\n",
        uSlotId, uPortSpeed, 3, 4, 5, 6);

    /*
     * For LS, HS, and SS devices; 8, 64, and 512 bytes, respectively,
     * are the only packet sizes allowed for Default Control Endpoint.
     * For FS devices, in theory we need to first probe from the Device
     * Descriptor, but this creates a Chicken-Egg problem in xHCI since
     * we need this in order to issue the "Address Device Command". So
     * we fake FS as 8 initially, then the USBD will help us to figure
     * out the real uMaxPacketSize0!
     */

    switch (uPortSpeed)
        {
        case USBHST_HIGH_SPEED:
            uMaxPacketSize0 = 64;
            uSlotCtxSpeed = USB_XHCI_PORTSC_PS_HS;
            break;
        case USBHST_SUPER_SPEED:
            uMaxPacketSize0 = 512;
            uSlotCtxSpeed = USB_XHCI_PORTSC_PS_SS;
            break;
        case USBHST_LOW_SPEED:
            uMaxPacketSize0 = 8;
            uSlotCtxSpeed = USB_XHCI_PORTSC_PS_LS;
            break;
        case USBHST_FULL_SPEED:
            uMaxPacketSize0 = 64;
            uSlotCtxSpeed = USB_XHCI_PORTSC_PS_FS;
            break;
        default:
            USB_XHCD_ERR("usbXhcdDeviceSlotInit - "
                "Wrong Speed for Slot ID %d\n",
                uPortSpeed, uSlotId, 3, 4, 5, 6);
            return ERROR;
            break;
        }

    /*
     * 1. Allocate an Input Context data structure (6.2.5) and
     * initialize all fields to '0'.
     */

    pNewDevice = usbXhcdDeviceCreate(pHCDData);

    if (pNewDevice == NULL)
        {
        USB_XHCD_ERR("usbXhcdDeviceSlotInit - "
            "usbXhcdDeviceCreate fail for Slot ID %d\n",
            uSlotId, 2, 3, 4, 5, 6);

        /* Disable the Slot */

        if (usbXhcdQueueDisableSlotCmd(pHCDData,
                                   uSlotId,
                                   USB_XHCD_DEFAULT_CMD_WAIT_TIME) != OK)
            {
            USB_XHCD_WARN("usbXhcdDeviceSlotInit - "
                "disable slot fail for Slot ID %d\n",
                uSlotId, 2, 3, 4, 5, 6);
            }

        return ERROR;
        }

    /* Choose the Root Hub based on the device speed */

    if (uPortSpeed == USBHST_SUPER_SPEED)
        {
        pRootHub = &pHCDData->USB3RH;
        pDefaultPipe = pHCDData->pDefaultUSB3Pipe;
        }
    else
        {
        pRootHub = &pHCDData->USB2RH;
        pDefaultPipe = pHCDData->pDefaultUSB2Pipe;
        }

    /* Assign the Slot ID and other fields passed down from USBD */

    pNewDevice->uSlotId = uSlotId;
    pNewDevice->uRootHubPort = (UINT8)(uRhPort + pRootHub->uPortOffset);
    pNewDevice->uRouteString = uRouteStr;
    pNewDevice->uDevSpeed = uPortSpeed;

    USB_XHCD_DBG("usbXhcdDeviceSlotInit - "
        "uSlotId %d uRootHubPort %d uRouteString 0x%05x uDevSpeed %d\n",
        pNewDevice->uSlotId,
        pNewDevice->uRootHubPort,
        pNewDevice->uRouteString,
        pNewDevice->uDevSpeed, 5, 6);

    /*
     * If the device is LS/FS and connected through a HS hub,
     * then the TT Hub Slot ID field references a Device Slot
     * that is assigned to the HS hub, the MTT field indicates
     * whether the HS hub supports Multi-TTs, and the TT Port
     * Number field indicates the correct TT port number on
     * the HS hub, else these fields are cleared to '0'.
     */

    pNewDevice->uMTT = 0;
    pNewDevice->uTTHubSlotId = 0;
    pNewDevice->uTTPort = 0;

    if ((uPortSpeed == USBHST_LOW_SPEED) ||
        (uPortSpeed == USBHST_FULL_SPEED))
        {
        /* Check if we are under any High-Speed Hub */

        pHubDevice = usbXhcdDeviceGet(pHCDData, (UINT8)uTTHubAddr, USBHST_HIGH_SPEED);

        if ((pHubDevice != NULL) &&
            (pHubDevice->uDevSpeed == USBHST_HIGH_SPEED))
            {
            pNewDevice->uTTHubSlotId = (UINT8)pHubDevice->uSlotId;
            pNewDevice->uMTT = pHubDevice->uMTT;
            pNewDevice->uTTPort = (UINT8)(uTTHubPort & USB_HUB_MASK_MULTIPLE_TT_PORT);
            }
        }
    USB_XHCD_DBG("usbXhcdDeviceSlotInit - "
                 "uTTHubSlotId %d uTTPort %d uMTT %d\n",
                 pNewDevice->uTTHubSlotId,
                 pNewDevice->uTTPort,
                 pNewDevice->uMTT, 4, 5, 6);

    pHCDData->ppDevSlots[uSlotId] = pNewDevice;
    pCtrlRing = pDefaultPipe->pEpRing;
    pDefaultPipe->uSlotId = uSlotId;

    usbXhcdRingReset(pHCDData, pCtrlRing);

    /*
     * Initially set the Device default pipe as the HCD
     * default pipe, it will be overriden when the USBD
     * tries to create a new pipe for the Device default
     * pipe.
     */

    pNewDevice->pDevPipes[USB_XHCI_CTRL_EP_CTX_DCI(0)] = pDefaultPipe;

    /*
     * 2. Initialize the Input Control Context (6.2.5.1) of the
     * Input Context by setting the A0 and A1 flags to '1'. These
     * flags indicate that the Slot Context and the Endpoint 0
     * Context of the Input Context are affected by the command.
     */

    pInputCtrlCtx = USB_XHCI_INPUT_CTRL_CTX_CAST(pHCDData,
                        pNewDevice->pIntputCtx);

    pNewDevice->uAddFlags = (USB_XHCI_ADD_EP(0) | USB_XHCI_ADD_EP(1));

    pInputCtrlCtx->uAddFlags = USB_XHCD_SWAP_DESC_DATA32(pHCDData,
                               pNewDevice->uAddFlags);
    pInputCtrlCtx->uDropFlags = 0;

    /*
     * 3. Initialize the Input Slot Context data structure (6.2.2).
     *    - Root Hub Port Number = Topology defined.
     *    - Route String = Topology defined. Refer to section 8.9
     *      in the USB3 spec. Note that the Route String does not
     *      include the Root Hub Port Number.
     *    - Context Entries = 1.
     */

    pInputSlotCtx = USB_XHCI_SLOT_CTX_CAST(pHCDData,
                        pNewDevice->pIntputCtx);


    pNewDevice->uContextEntries = 1;

    pInputSlotCtx->uSlotInfo0 = USB_XHCD_SWAP_DESC_DATA32(pHCDData,
        (USB_XHCI_SLOT_CTX_ROUTE_STRING(pNewDevice->uRouteString) |
         USB_XHCI_SLOT_CTX_ENTRIES(pNewDevice->uContextEntries) |
         USB_XHCI_SLOT_CTX_SPEED(uSlotCtxSpeed) |
         USB_XHCI_SLOT_CTX_MTT(pNewDevice->uMTT)));

    pInputSlotCtx->uSlotInfo1 = USB_XHCD_SWAP_DESC_DATA32(pHCDData,
        USB_XHCI_SLOT_CTX_RH_PORT(pNewDevice->uRootHubPort));

    pInputSlotCtx->uSlotInfo2 = USB_XHCD_SWAP_DESC_DATA32(pHCDData,
        USB_XHCI_SLOT_CTX_TT_HUB_SLOT_ID(pNewDevice->uTTHubSlotId) |
        USB_XHCI_SLOT_CTX_TT_PORT(pNewDevice->uTTPort));

    /*
     * 4. Allocate and initialize the Transfer Ring for the Default
     * Control Endpoint. This has been done in step 1.
     */

    /*
     * 5. Initialize the Input default control Endpoint 0 Context (6.2.3).
     *    - EP Type = Control.
     *    - Max Packet Size = The default maximum packet size for the
     *      Default Control Endpoint, as function of the PORTSC Port Speed
     *      field.
     *    - Max Burst Size = 0.
     *    - TR Dequeue Pointer = Start address of first segment of the
     *      Default Control Endpoint Transfer Ring.
     *    - Dequeue Cycle State (DCS) = 1. Reflects Cycle bit state for
     *      valid TRBs written by software.
     *    - Interval = 0.
     *    - Max Primary Streams (MaxPStreams) = 0.
     *    - Mult = 0.
     *    - Error Count (CErr) = 3.
     */

    pCtrlEpCtx = USB_XHCI_EP_CTX_CAST(pHCDData,
                                      pNewDevice->pIntputCtx,
                                      USB_XHCI_CTRL_EP_CTX_DCI(0));

    pCtrlEpCtx->uEpInfo0 = USB_XHCD_SWAP_DESC_DATA32(pHCDData,
                           (USB_XHCI_EP_CTX_INTERVAL(0) |
                           USB_XHCI_EP_CTX_MAX_PRI_STREAMS(0) |
                           USB_XHCI_EP_CTX_MULT(0)));

    pCtrlEpCtx->uEpInfo1 = USB_XHCD_SWAP_DESC_DATA32(pHCDData,
                           (USB_XHCI_EP_CTX_EP_TYPE(USB_XHCI_CTRL_EP) |
                           USB_XHCI_EP_CTX_MAX_PACKET_SIZE(uMaxPacketSize0) |
                           USB_XHCI_EP_CTX_MAX_BURST_SIZE(0) |
                           USB_XHCI_EP_CTX_CERR(3)));

    pCtrlEpCtx->uEpDeqLo = USB_XHCD_SWAP_DESC_DATA32(pHCDData,
                           (USB_XHCD_LO32(pCtrlRing->pEnqSeg->uHeadTRB) |
                            USB_XHCI_EP_CTX_DCS(pCtrlRing->uCycle)));
    pCtrlEpCtx->uEpDeqHi = USB_XHCD_SWAP_DESC_DATA32(pHCDData,
                           (USB_XHCD_HI32(pCtrlRing->pEnqSeg->uHeadTRB)));

    pCtrlEpCtx->uEpInfo2 = USB_XHCD_SWAP_DESC_DATA32(pHCDData,
             (USB_XHCI_EP_CTX_AVRG_TRB_LEN(pDefaultPipe->uAvrgLen) |
              USB_XHCI_EP_CTX_MAX_ESIT_PAYLOAD(pDefaultPipe->uMaxESIT)));

    USB_XHCD_VDBG("usbXhcdDeviceSlotInit - "
        "Control pipe uEpDeqPtr 0x%08x:%08x\n",
        pCtrlEpCtx->uEpDeqHi, pCtrlEpCtx->uEpDeqLo, 0, 0, 0, 0);

    /*
     * 6. Allocate the Output Device Context data structure (6.2.1)
     * and initialize it to '0'.
     */

    /*
     * 7. Load the appropriate (Device Slot ID) entry in the Device
     * Context Base Address Array (5.4.6) with a pointer to the Output
     * Device Context data structure (6.2.1).
     */

    pHCDData->pDCBAA[uSlotId] = USB_XHCD_SWAP_DESC_DATA64(pHCDData,
                    pNewDevice->pOutputCtx->uCtxBusAddr);

    /*
     * 8. Issue an Address Device Command for the Device Slot, where
     * the command points to the Input Context data structure described
     * above. Refer to sections 4.6.5 and 6.4.3.4 for more information
     * on the Address Device Command
     */

    if (usbXhcdQueueAddressDeviceCmd(pHCDData,
                pNewDevice->pIntputCtx->uCtxBusAddr,
                uSlotId,
                USB_XHCD_DEFAULT_CMD_WAIT_TIME) != OK)
        {
        USB_XHCD_ERR("usbXhcdDeviceSlotInit - "
            "usbXhcdQueueAddressDeviceCmd fail\n",
            0, 0, 0, 0, 0, 0);

        /*
         * If the SET_ADDRESS request was unsuccessful, system software
         * may issue a Disable Slot Command for the slot or reset the
         * device and attempt the Address Device Command again. An
         * unsuccessful Address Device Command shall leave the Device
         * Slot in the Default state.
         */

        /* Disable the Slot */

        if (usbXhcdQueueDisableSlotCmd(pHCDData,
                                       uSlotId,
                                       USB_XHCD_DEFAULT_CMD_WAIT_TIME) != OK)
            {
            USB_XHCD_WARN("usbXhcdDeviceSlotInit - "
                "disable slot fail for Slot ID %d\n",
                uSlotId, 2, 3, 4, 5, 6);
            }

        /* Destroy the device allocated */

        if (usbXhcdDeviceDestroy(pHCDData, pNewDevice) != OK)
            {
            USB_XHCD_WARN("usbXhcdDeviceSlotInit - "
                "destroy device fail for Slot ID %d\n",
                uSlotId, 2, 3, 4, 5, 6);
            }


        pHCDData->ppDevSlots[uSlotId] = NULL;
        pHCDData->pDCBAA[uSlotId] = 0ULL;

        return ERROR;
        }

    /*
     * Get the xHCI generated/reported device address from the
     * Output Device Context
     */

    if ((usbXhcdDeviceAddressGet(pHCDData, uSlotId, pDevAddr) != OK) ||
         (*pDevAddr == USB_ROOT_HUB_ADDRESS))
        {
        USB_XHCD_ERR("usbXhcdDeviceSlotInit - "
            "usbXhcdDeviceAddressGet fail\n",
            0, 0, 0, 0, 0, 0);

        /* Disable the Slot */

        if (usbXhcdQueueDisableSlotCmd(pHCDData,
                                   uSlotId,
                                   USB_XHCD_DEFAULT_CMD_WAIT_TIME) != OK)
            {
            USB_XHCD_WARN("usbXhcdDeviceSlotInit - "
                "disable slot fail for Slot ID %d\n",
                uSlotId, 2, 3, 4, 5, 6);
            }

        /* Destroy the device allocated */

        if (usbXhcdDeviceDestroy(pHCDData, pNewDevice) != OK)
            {
            USB_XHCD_WARN("usbXhcdDeviceSlotInit - "
                "destroy device fail for Slot ID %d\n",
                uSlotId, 2, 3, 4, 5, 6);
            }

        pHCDData->ppDevSlots[uSlotId] = NULL;
        pHCDData->pDCBAA[uSlotId] = 0ULL;

        return ERROR;
        }

    /* Save the xHCI generated device address in the Device stucture */

    pNewDevice->uDevAddr = *pDevAddr;

    pDefaultPipe->uDevAddr = pNewDevice->uDevAddr;

    USB_XHCD_DBG("usbXhcdDeviceSlotInit - "
        "Allocated uDevAddr 0x%x for new Slot %d\n",
        pNewDevice->uDevAddr, pNewDevice->uSlotId, 0, 0, 0, 0);

    return OK;
    }

/*******************************************************************************
*
* usbXhcdDeviceSlotStop - stop the device slot
*
* This routine is to stop the device slot. This will stop all USB activity by
* issuing Stop Endpoint Commands for each endpoint in the Running state.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdDeviceSlotStop
    (
    pUSB_XHCD_DATA              pHCDData,
    pUSB_XHCD_DEVICE            pHCDDevice
    )
    {
    pUSB_XHCD_PIPE              pHCDPipe;
    UINT32                      uIndex;
    UINT8                       uEpState;
    STATUS                      uSts = OK;

    for (uIndex = 1; uIndex < USB_XHCI_MAX_DEV_PIPES; uIndex++)
        {
        pHCDPipe = pHCDDevice->pDevPipes[uIndex];

        if (pHCDPipe == NULL)
            continue;

        uEpState = usbXhcdPipeStateGet(pHCDData, pHCDPipe);

        if (uEpState != USB_XHCI_EP_STATE_RUNNING)
            {
            USB_XHCD_WARN("usbXhcdDeviceSlotStop - "
                         "Pipe for dev %d ep 0x%x ep id %d not running %d!\n",
                         pHCDPipe->uDevAddr,
                         pHCDPipe->uEpAddr,
                         pHCDPipe->uEpId,
                         uEpState, 5 ,6);
            continue;
            }

        /*
         * Stop all USB activity by issuing Stop Endpoint Command.
         * This shall cause the xHC to update the respective Endpoint
         * or Stream Context TR Dequeue Pointer and DCS fields.
         */

        uSts = usbXhcdQueueStopEndpointCmd(pHCDData,
                                           pHCDPipe->uSlotId,
                                           pHCDPipe->uEpId,
                                           1,
                                           100);
        if (uSts != OK)
            {
            uEpState = usbXhcdPipeStateGet(pHCDData, pHCDPipe);

            if (uEpState == USB_XHCI_EP_STATE_STOPPED)
                {
                USB_XHCD_WARN("usbXhcdDeviceSlotStop - "
                             "Stopping pipe for dev %d ep 0x%x ep id %d timeout but stopped!\n",
                             pHCDPipe->uDevAddr,
                             pHCDPipe->uEpAddr,
                             pHCDPipe->uEpId, 4, 5 ,6);

                uSts = OK;
                }
            else
                {
                USB_XHCD_WARN("usbXhcdDeviceSlotStop - "
                             "Stopping pipe for dev %d ep 0x%x ep id %d fail\n",
                             pHCDPipe->uDevAddr,
                             pHCDPipe->uEpAddr,
                             pHCDPipe->uEpId, 4, 5 ,6);
                }

            return uSts;
            }
        }

    return uSts;
    }

/*******************************************************************************
*
* usbXhcdCtxWrapperDestroy - destroy the context wrapper structure
*
* This routine is to destroy the context wrapper structure.
*
* RETURNS: OK when the desired operation successfully done, ERROR when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdCtxWrapperDestroy
    (
    pUSB_XHCD_DATA          pHCDData,
    pUSB_XHCD_CTX_WRAPPER   pCtxWrapper
    )
    {
    if (pCtxWrapper->pCtxArea)
        {
        /* Free the memory allocated */

        if (vxbDmaBufMemFree(pHCDData->ctxDmaTagId,
            pCtxWrapper->pCtxArea, pCtxWrapper->dmaMapId) != OK)
            {
            USB_XHCD_ERR("usbXhcdCtxWrapperDestroy - vxbDmaBufMemFree fail\n",
                0, 0, 0, 0, 0, 0);

            return ERROR;
            }

        pCtxWrapper->pCtxArea = NULL;
        }

    OSS_FREE(pCtxWrapper);

    return OK;
    }

/*******************************************************************************
*
* usbXhcdCtxWrapperCreate - create the context wrapper structure
*
* This routine is to create the context wrapper structure.
*
* RETURNS: Pointer to the created context wrapper structure, NULL when fail.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_XHCD_CTX_WRAPPER usbXhcdCtxWrapperCreate
    (
    pUSB_XHCD_DATA  pHCDData,
    UINT8           uCtxType
    )
    {
    /* Pointer to the context wrapper */

    pUSB_XHCD_CTX_WRAPPER pCtxWrapper;

    /* Return status of vxBus routine call */

    STATUS vxbSts;

    /* Only two types of context wrapper */

    if ((uCtxType != USB_XHCI_DEV_CTX_TYPE) &&
        (uCtxType != USB_XHCI_INPUT_CTX_TYPE))
        {
        USB_XHCD_ERR("usbXhcdCtxWrapperCreate - invalid context type %d\n",
            uCtxType, 2, 3, 4, 5, 6);

        return NULL;
        }

    /* Allocate memory for the context wrapper */

    pCtxWrapper = OSS_CALLOC(sizeof(USB_XHCD_CTX_WRAPPER));

    if (pCtxWrapper == NULL)
        {
        USB_XHCD_ERR("usbXhcdCtxWrapperCreate - creating pCtxWrapper fail\n",
            1, 2, 3, 4, 5, 6);

        return NULL;
        }

    /* Record the context type */

    pCtxWrapper->uCtxType = uCtxType;

    /* Allocate the memory for TRBs of the segment */

    pCtxWrapper->pCtxArea = vxbDmaBufMemAlloc (pHCDData->pDev,
            pHCDData->ctxDmaTagId,
            NULL,
            VXB_DMABUF_MEMALLOC_CLEAR_BUF,
            &pCtxWrapper->dmaMapId);

    if (pCtxWrapper->pCtxArea == NULL)
        {
        USB_XHCD_ERR("usbXhcdCtxWrapperCreate - creating pCtxArea fail\n",
            1, 2, 3, 4, 5, 6);

        if (usbXhcdCtxWrapperDestroy(pHCDData, pCtxWrapper) != OK)
            {
            USB_XHCD_WARN("usbXhcdCtxWrapperCreate - destroy wapper fail 1\n",
                1, 2, 3, 4, 5, 6);
            }

        return NULL;
        }

    /*
     * Load the DMA MAP so that we get the correct BUS address
     * to program the hardware
     */

    vxbSts = vxbDmaBufMapLoad (pHCDData->pDev,
        pHCDData->ctxDmaTagId,
        pCtxWrapper->dmaMapId,
        pCtxWrapper->pCtxArea,
        pHCDData->uCtxArraySize,
        0);

    if (vxbSts != OK)
        {
        USB_XHCD_ERR("usbXhcdCtxWrapperCreate - "
            "loading map pSeg->dmaMapId fail\n",
            1, 2, 3, 4, 5, 6);

        if (usbXhcdCtxWrapperDestroy(pHCDData, pCtxWrapper) != OK)
            {
            USB_XHCD_WARN("usbXhcdCtxWrapperCreate - destroy wapper fail 2\n",
                1, 2, 3, 4, 5, 6);
            }

        return NULL;
        }

    pCtxWrapper->uCtxBusAddr = pCtxWrapper->dmaMapId->fragList[0].frag;

    return pCtxWrapper;
    }

/*******************************************************************************
*
* usbXhcdTrbBusAddr - get the TRB bus address
*
* This routine is to get the TRB bus address.
*
* RETURNS: TRB bus address, 0 when TRB not valid or not in the segment.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

UINT64 usbXhcdTrbBusAddr
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_SEGMENT   pSeg,
    pUSB_XHCI_TRB       pTRB
    )
    {
    /* TRB offset from segment TRBs starting address */

    size_t uOffset;

    /* Check if the parameter is valid */

    if (!pSeg || !pTRB || (pTRB < pSeg->pTRBs))
        return 0;

    /* Get the offset (in unit of TRB size) */

    uOffset = pTRB - pSeg->pTRBs;

    /* Too far away */

    if (uOffset > USB_XHCD_SEG_MAX_TRBS)
        return 0;

    /* Get the TRB BUS address */

    return (pSeg->uHeadTRB + uOffset * USB_XHCI_TRB_SIZE);
    }

/*******************************************************************************
*
* usbXhcdIsLastTrbOfRing - check if the TRB is the last one of the ring
*
* This routine is to check if the TRB is the last one of the ring.
*
* RETURNS: TRUE if the TRB is the last one of the ring, FALSE if not.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOL usbXhcdIsLastTrbOfRing
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_RING      pRing,
    pUSB_XHCD_SEGMENT   pSeg,
    pUSB_XHCI_TRB       pTRB
    )
    {
    /* Check if the ring is event ring */

    if (pRing->uType == USB_XHCD_EVT_RING)
        {
        /* The event segment has no LINK TRB */

        return ((pTRB == &pSeg->pTRBs[USB_XHCD_SEG_MAX_TRBS - 1]) &&
            (pSeg->pNextSeg == pRing->pHeadSeg));
        }
    else
        {
        /* TRB Control */

        UINT32 uCtrl = pTRB->uCtrl;

        /* Get the TRB Control in CPU endian */

        uCtrl = USB_XHCD_SWAP_DESC_DATA32(pHCDData, uCtrl);

        /* The Toggle Cycle (TC) bit for the last TRB in the ring is set */

        return ((uCtrl & USB_XHCI_TRB_CTRL_TC_MASK) != 0);
        }
    }

/*******************************************************************************
*
* usbXhcdIsLastTrbOfSegment - check if the TRB is the last one on the segment
*
* This routine is to check if the TRB is the last one on the segment.
*
* Note: For event ring segment, there is no LINK TRB, so the last TRB is just
* the last TRB in the TRBs array. For other ring segment, the last TRB is the
* LINK TRB.
*
* RETURNS: TRUE if the TRB is the last one on the segment, FALSE if not.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOL usbXhcdIsLastTrbOfSegment
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_RING      pRing,
    pUSB_XHCD_SEGMENT   pSeg,
    pUSB_XHCI_TRB       pTRB
    )
    {
    /* Check if the ring is event ring */

    if (pRing->uType == USB_XHCD_EVT_RING)
        {
        /* The event segment has no LINK TRB */

        return (pTRB == &pSeg->pTRBs[USB_XHCD_SEG_MAX_TRBS - 1]);
        }
    else
        {
        /* TRB Control */

        UINT32 uCtrl;

        /* Get the TRB Control in CPU endian */

        uCtrl = USB_XHCD_SWAP_DESC_DATA32(pHCDData, pTRB->uCtrl);

        /* Check if it is a LINK TRB */

        return (USB_XHCI_TRB_CTRL_TYPE_GET(uCtrl) == USB_XHCI_TRB_LINK);
        }
    }

/*******************************************************************************
*
* usbXhcdDeqPtrAdvance - advance the Dequeue Pointer of the ring
*
* This routine is to advance the Dequeue Pointer of the ring.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbXhcdDeqPtrAdvance
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_RING      pRing
    )
    {
    /* Pointer to the next TRB */

    pUSB_XHCI_TRB   pNextTRB;

    /* Get the current Dequeue Pointer */

    pNextTRB = (pRing->pDeqTRB);

    /* Increase the Dequeue Pointer to the next TRB */

    pRing->pDeqTRB++;

    /* Increase the Dequeue advance counter */

    pRing->uNumDeq ++;

    /* Check if the next TRB is last of the segment */

    while (usbXhcdIsLastTrbOfSegment(pHCDData,
        pRing, pRing->pDeqSeg, pNextTRB))
        {
        /*
         * If it is event ring, check if the TRB is the last
         * TRB of the ring, and toggle the software maintained
         * Cycle bit state.
         */

        if ((pRing->uType == USB_XHCD_EVT_RING) &&
            usbXhcdIsLastTrbOfRing(pHCDData, pRing, pRing->pDeqSeg, pNextTRB))
            {
            pRing->uCycle = (UINT8)(pRing->uCycle ? 0 : 1);
            }

        /* Go to the next segment */

        pRing->pDeqSeg = pRing->pDeqSeg->pNextSeg;

        /* FIXME: May need WMB here! */

        /* Go to the next TRB on the next segment */

        pRing->pDeqTRB = &pRing->pDeqSeg->pTRBs[0];

        /* FIXME: May need WMB here! */

        /* Go to the next TRB */

        pNextTRB = pRing->pDeqTRB;
        }
    }

/*******************************************************************************
*
* usbXhcdEnqPtrAdvance - advance the Enqueue Pointer of the ring
*
* This routine is to advance the Enqueue Pointer of the ring.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbXhcdEnqPtrAdvance
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_RING      pRing
    )
    {
    /* Pointer to the next TRB */

    pUSB_XHCI_TRB   pNextTRB;

    /* The Chain bit */

    UINT32 uChain;

    /* TRB Control */

    UINT32 uCtrl;

    /* Get the TRB Control field and swap into CPU endian */

    uCtrl = USB_XHCD_SWAP_DESC_DATA32(pHCDData, pRing->pEnqTRB->uCtrl);

    /* Get the Chain bit in CPU endian */

    uChain = uCtrl & USB_XHCI_TRB_CTRL_CHAIN_BIT_MASK;

    /* Increase the Enqueue Pointer to the next TRB */

    pNextTRB = ++(pRing->pEnqTRB);

    /* Increase the Enqueue advance counter */

    pRing->uNumEnq ++;

    /* Check if the next TRB is last of the segment */

    while (usbXhcdIsLastTrbOfSegment(pHCDData,
        pRing, pRing->pEnqSeg, pNextTRB))
        {
        /*
         * If it is event ring, the software is consumer, then
         * so check if the TRB is the last TRB of the ring, and
         * toggle the software maintained Cycle bit state.
         */

        if (pRing->uType != USB_XHCD_EVT_RING)
            {
            /* FIXME: May need WMB here! */

            /* Get the TRB Control and swap into CPU endian */

            uCtrl = USB_XHCD_SWAP_DESC_DATA32(pHCDData, pNextTRB->uCtrl);

            /* Toggle the Cycle bit state */

            if (uCtrl & USB_XHCI_TRB_CTRL_CYCLE_BIT_MASK)
                {
                /* Clear the Chain bit */

                uCtrl &= ~(USB_XHCI_TRB_CTRL_CYCLE_BIT_MASK);
                }
            else
                {
                /* Set the Chain bit */

                uCtrl |= (USB_XHCI_TRB_CTRL_CYCLE_BIT_MASK);
                }
            /*
              * In a Transfer Ring a Link TRB is always assumed to be
              * linked to the first TRB of the next segment. If the
              * Chain bit (CH) of the previous TRB is '1', then the
              * multi-TRB TD that it defines spans segments and shall
              * continue with the first TRB of the next segment. In a
              * Command Ring the Link TRB Chain bit (CH) is ignored
              * by the xHC.
              */

            if (uChain)
                uCtrl |= (USB_XHCI_TRB_CTRL_CHAIN_BIT_MASK);
            else
                uCtrl &= ~(USB_XHCI_TRB_CTRL_CHAIN_BIT_MASK);

            /* Swap into BUS endian */

            pNextTRB->uCtrl = USB_XHCD_SWAP_DESC_DATA32(pHCDData, uCtrl);

            /*
             * Check if the next TRB is the last TRB of the ring,
             * and if so update the Cycle bit.
             */

            if (usbXhcdIsLastTrbOfRing(pHCDData,
                pRing, pRing->pEnqSeg, pNextTRB))
                {
                pRing->uCycle = (UINT8)(pRing->uCycle ? 0 : 1);
                }
            }

        /* Go to the next segment */

        pRing->pEnqSeg = pRing->pEnqSeg->pNextSeg;

        /* FIXME: May need WMB here! */

        /* Go to the next TRB on the next segment */

        pRing->pEnqTRB = &pRing->pEnqSeg->pTRBs[0];

        /* FIXME: May need WMB here! */

        /* Go to the next TRB */

        pNextTRB = pRing->pEnqTRB;
        }
    }

/*******************************************************************************
*
* usbXhcdHasRoomOnRing - check if the ring has room for the number of free TRBs
*
* This routine is to check if the ring has room for the number of free TRBs.
*
* RETURNS: TRUE if the ring has room for the number of free TRBs, FALSE if not.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOL usbXhcdHasRoomOnRing
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_RING      pRing,
    size_t              uNumTRBs
    )
    {
    /* Loop index */

    size_t uIndex;

    /* Pointer to the Enqueue TRB */

    pUSB_XHCI_TRB   pEnqTRB = pRing->pEnqTRB;

    /* Pointer to the Enqueue segment */

    pUSB_XHCD_SEGMENT   pEnqSeg = pRing->pEnqSeg;

    /* When Enqueue Pointer equals Dequeue Pointer the ring is empty */

    if (pEnqTRB == pRing->pDeqTRB)
        return TRUE;

    /* See if there are enough room for the specified number of TRBs */

    for (uIndex = 0; uIndex < uNumTRBs; uIndex++)
        {
        /* Go to the next TRB */

        pEnqTRB++;

        /* Bypass all the LINK TRBs */

        while (usbXhcdIsLastTrbOfSegment(pHCDData,
            pRing, pEnqSeg, pEnqTRB))
            {
            /* Go to the next segment */

            pEnqSeg = pEnqSeg->pNextSeg;

            /* Go to the next TRB on the next segment */

            pEnqTRB = &pEnqSeg->pTRBs[0];
            }
        }

    return TRUE;
    }

/*******************************************************************************
*
* usbXhcdQueueTRB - queue a TRB onto the ring
*
* This routine is to queue a TRB onto the ring.
*
* RETURNS: Pointer to the TRB structure queued.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_XHCI_TRB usbXhcdQueueTRB
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_RING      pRing,
    UINT32              uDataLo,
    UINT32              uDataHi,
    UINT32              uInfo,
    UINT32              uCtrl
    )
    {
    /* Pointer to the Enqueue TRB */

    pUSB_XHCI_TRB      pTRB = pRing->pEnqTRB;

    /* Fill the Enqueue TRB fields in BUS endian */

    pTRB->uDataLo = USB_XHCD_SWAP_DESC_DATA32(pHCDData, uDataLo);
    pTRB->uDataHi = USB_XHCD_SWAP_DESC_DATA32(pHCDData, uDataHi);
    pTRB->uInfo = USB_XHCD_SWAP_DESC_DATA32(pHCDData, uInfo);
    pTRB->uCtrl = USB_XHCD_SWAP_DESC_DATA32(pHCDData, uCtrl);

    /* Advance the Enqueue Pointer */

    usbXhcdEnqPtrAdvance(pHCDData, pRing);

    USB_XHCD_VDBG("usbXhcdQueueTRB - "
        "TRB @%p [uData 0x%016X uInfo 0x%08X uCtrl 0x%08X] "
        "with Type %d is %s TRB\n",
        pTRB,
        usbXhcdGetTrbData64(pHCDData, pTRB),
        USB_XHCD_SWAP_DESC_DATA32(pHCDData, pTRB->uInfo),
        USB_XHCD_SWAP_DESC_DATA32(pHCDData, pTRB->uCtrl),
        USB_XHCI_TRB_CTRL_TYPE_GET(
        USB_XHCD_SWAP_DESC_DATA32(pHCDData, pTRB->uCtrl)),
        usbXhcdTrbName((UINT8)USB_XHCI_TRB_CTRL_TYPE_GET(
                       USB_XHCD_SWAP_DESC_DATA32(pHCDData, pTRB->uCtrl))));

    return pTRB;
    }

/*******************************************************************************
*
* usbXhcdQueueCmd - queue a command TRB onto the command ring
*
* This routine is to queue a command TRB onto the command ring.
*
* RETURNS: OK when the command is queued, ERROR when there is no room on ring.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdQueueCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_DEVICE    pHCDDevice,
    _Vx_ticks_t         waitTime,
    UINT32              uDataLo,
    UINT32              uDataHi,
    UINT32              uInfo,
    UINT32              uCtrl
    )
    {
    pUSB_XHCD_CMD       pCMD;
    pUSB_XHCI_TRB       pCmdTRB;
    STATUS              uSts;
    UINT32              uRetry;

    uRetry = _WRS_CONFIG_USB_XHCD_CMD_RETRIES;

    do
        {
        /* Exclusively access the pipe request list */

        (void) OS_WAIT_FOR_EVENT(pHCDData->hcdMutex, OS_WAIT_INFINITE);

        /* Check if there is room on the command ring */

        if (usbXhcdHasRoomOnRing(pHCDData, pHCDData->pCmdRing, 1) != TRUE)
            {
            USB_XHCD_ERR("usbXhcdQueueCmd - usbXhcdHasRoomOnRing FALSE!\n",
                1, 2, 3, 4, 5, 6);

            /* Release the exclusive access */

            OS_RELEASE_EVENT(pHCDData->hcdMutex);

            return ERROR;
            }

        /* Queue the command TRB with PCS (Producer Cycle State) */

        pCmdTRB = usbXhcdQueueTRB(pHCDData,
                                  pHCDData->pCmdRing,
                                  uDataLo,
                                  uDataHi,
                                  uInfo,
                                  uCtrl | pHCDData->pCmdRing->uCycle);

        /* Reserve a command structure */

        pCMD = usbXhcdCmdReserve(pHCDData, pHCDDevice, pCmdTRB);

        /* Save the wait time */

        pCMD->waitTime = waitTime;

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDData->hcdMutex);

        uSts = usbXhcdNotifyCommandReady(pHCDData, pHCDDevice, pCMD);

        if ((uSts == OK) || (uRetry == 0))
            break;

        uRetry--;

        USB_XHCD_ERR("usbXhcdQueueCmd - cmd fail, trying %d!\n",
            uRetry, 2, 3, 4, 5, 6);
        }while (uSts != OK);

    return uSts;
    }

/*******************************************************************************
*
* usbXhcdQueueVendorCmd - queue a Vendor-Specific Command
*
* This routine is to queue a queue a Vendor-Specific Command TRB onto the
* command ring.
*
* RETURNS: OK when the command is queued, ERROR when there is no room on ring.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdQueueVendorCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_DEVICE    pHCDDevice,
    _Vx_ticks_t         waitTime,
    UINT32              uDataLo,
    UINT32              uDataHi,
    UINT32              uInfo,
    UINT32              uCtrl
    )
    {
    return usbXhcdQueueCmd(pHCDData,
                           pHCDDevice,
                           waitTime,
                           uDataLo, uDataHi, uInfo, uCtrl);
    }

/*******************************************************************************
*
* usbXhcdQueueNoOpCmd - queue a No Op Command
*
* This routine is to queue a No Op Command TRB onto the command ring.
*
* The No Op command can be issued by software to exercise the TRB Ring
* mechanism of the xHC without affecting any xHC or USB Device state, or
* to report the current value of the Command Ring Dequeue Pointer.
*
* RETURNS: OK when the command is queued, ERROR when there is no room on ring.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdQueueNoOpCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    UINT32              uIntr
    )
    {
    /* TRB Control */

    UINT32 uCtrl;

    /* TRB Info */

    UINT32 uInfo;

    /*
     * Insert a No Op Command TRB on the Command Ring and initialize
     * the following fields:
     * - TRB Type = No Op Command.
     * - Clear all other fields of the command TRB to '0'.
     * - Cycle bit = Command Ring's PCS flag.
     */

    uCtrl = USB_XHCI_TRB_CTRL_TYPE(USB_XHCI_TRB_CMD_NOOP);

    uInfo = USB_XHCI_TRB_INFO_INTR_TARGET(uIntr);

    return usbXhcdQueueCmd(pHCDData,
                           NULL,
                           NO_WAIT,
                           0, 0, uInfo, uCtrl);
    }

/*******************************************************************************
*
* usbXhcdQueueEnableSlotCmd - queue a Enable Slot Command
*
* This routine is to queue a Enable Slot Command TRB onto the command ring.
*
* The Enable Slot Command is issued by software to obtain an available Device
* Slot and to transition a Device Slot from the Disabled to the Default state.
*
* When an Enable Slot Command is processed by the xHC, it will look for an
* available Device Slot. If a slot is available, the ID of a selected slot will
* be returned in the Slot ID field of a successful Command Completion Event on
* the Event Ring. If a Device Slot is not available, the Slot ID field shall be
* cleared to '0' and a No Slots Available Error will be returned in the Command
* Completion Event.
*
* RETURNS: OK when the command is queued, ERROR when there is no room on ring.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdQueueEnableSlotCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    _Vx_ticks_t         waitTime
    )
    {
    /* TRB Control */

    UINT32 uCtrl;

    /*
     * Insert an Enable Slot Command TRB on the Command Ring and initialize
     * the following fields:
     * - TRB Type = Enable Slot command.
     * - Clear all other fields of the command TRB to '0'.
     * - Cycle bit = Command Ring's PCS flag.
     */

    uCtrl = USB_XHCI_TRB_CTRL_TYPE(USB_XHCI_TRB_ENABLE_SLOT);

    return usbXhcdQueueCmd(pHCDData,
                           NULL,
                           waitTime,
                           0, 0, 0, uCtrl);
    }


/*******************************************************************************
*
* usbXhcdQueueDisableSlotCmd - queue a Disable Slot Command
*
* This routine is to queue a Disable Slot Command TRB onto the command ring.
*
* The Disable Slot Command is issued by software to force a Device Slot to
* the Disabled state. A typical use would be to free a Device Slot when a USB
* device is disconnected.
*
* When a Disable Slot Command is processed by the xHC it shall: disable the
* Doorbell register for the slot, abort any pending events not already posted
* to an Event Ring, free any bandwidth allocated to the periodic endpoints of
* the device, terminate any slot related USB activity (e.g. packet transfers),
* free any internal resources associated with the slot, and internally flag the
* slot as "available" for subsequent reassignment by an Enable Slot Command.
* i.e. the Device Context Base Address Array entry for the slot is no longer
* considered valid by the xHC and software can free the Device Context,
* Transfer Ring, Stream Context Array, etc. data structures associated with
* the slot.
*
* RETURNS: OK when the command is queued, ERROR when there is no room on ring.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdQueueDisableSlotCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    UINT32              uSlotId,
    _Vx_ticks_t         waitTime
    )
    {
    /* TRB Control */

    UINT32              uCtrl;

    /* Device structure for the Slot */

    pUSB_XHCD_DEVICE    pHCDDevice;

    /* Get Device structure for the Slot */

    pHCDDevice = pHCDData->ppDevSlots[uSlotId];

    if (pHCDDevice == NULL)
        {
        USB_XHCD_ERR("usbXhcdQueueDisableSlotCmd - "
            "pHCDDevice for Slot ID %d NULL\n",
            uSlotId, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * Insert an Disable Slot Command TRB on the Command Ring and initialize
     * the following fields:
     * - TRB Type = Disable Slot Command.
     * - Slot ID = ID of the Device Slot to be disabled.
     * - Clear all other fields of the command TRB to '0'.
     * - Cycle bit = Command Ring's PCS flag.
     */

    uCtrl = USB_XHCI_TRB_CTRL_TYPE(USB_XHCI_TRB_DISABLE_SLOT) |
        USB_XHCI_TRB_CTRL_SLOT_ID(uSlotId);

    return usbXhcdQueueCmd(pHCDData,
                           pHCDDevice,
                           waitTime,
                           0, 0, 0, uCtrl);
    }

/*******************************************************************************
*
* usbXhcdQueueAddressDeviceCmd - queue a Address Device Command
*
* This routine is to queue a Address Device Command TRB onto the command ring.
*
* The Address Device Command is issued by software to transition a Device Slot
* from the Default to the Addressed state.
*
* When an Address Device Command is processed by the xHC it shall enable the
* device's Default Control Endpoint, select an address for the USB device,
* and issue a USB SET_ADDRESS request to the USB Device. The SET_ADDRESS
* request for a USB2 device shall be issued to Address '0'. The SET_ADDRESS
* request for a USB3 device shall be issued using the Route String.
*
* RETURNS: OK when the command is queued, ERROR when there is no room on ring.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdQueueAddressDeviceCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    UINT64              uInputCtxBusAddr,
    UINT32              uSlotId,
    _Vx_ticks_t         waitTime
    )
    {
    /* TRB Control */

    UINT32              uCtrl;

    /* Device structure for the Slot */

    pUSB_XHCD_DEVICE    pHCDDevice;

    /* LOW 32 bits of the 64 bit DATA */

    UINT32              uDataLo;

    /* HIGH 32 bits of the 64 bit DATA */

    UINT32              uDataHi;

    /* Get Device structure for the Slot */

    pHCDDevice = pHCDData->ppDevSlots[uSlotId];

    if (pHCDDevice == NULL)
        {
        USB_XHCD_ERR("usbXhcdQueueAddressDeviceCmd - "
            "pHCDDevice for Slot ID %d NULL\n",
            uSlotId, 2, 3, 4, 5, 6);

        return ERROR;
        }

    uDataLo = USB_XHCD_LO32(uInputCtxBusAddr);
    uDataHi = USB_XHCD_HI32(uInputCtxBusAddr);

    /*
     * 1. Ensure that the Device Context Base Address Array entry points to a
     * properly sized and initialized Device Context data structure for the
     * device.
     *
     * Note: This step should have been done by the Enable Slot Command.
     */

    /*
     * 2. Allocate and initialize an Input Context data structure for the
     * command.
     *
     * The Add Context flags for the Slot Context and the Endpoint 0 Context
     * shall be set to '1'. All fields of the Input Context Slot Context data
     * structure shall define valid values, except for the Slot State and USB
     * Device Address fields which shall be cleared to '0'. The Endpoint 0
     * Context data structure in the Input Context shall define valid values
     * for the TR Dequeue Pointer, EP Type, Force Event (FE), Error Count
     * (CErr), and Max Packet Size fields. The MaxPStreams, Max Burst Size,
     * and EP State values shall be cleared to '0'.
     *
     * Note: This step should have been done by the caller and the BUS address
     * of the Input Context data structure is provided as parameter to this
     * routine.
     */

    /*
     * 3. Insert an Address Device Command TRB on the Command Ring and initialize
     * the following fields:
     * - TRB Type = Address Device Command.
     * - Slot ID = ID of the target Device Slot.
     * - Input Context Pointer = Base address of Input Context data structure.
     * - Clear all other fields of the command TRB to '0'.
     * - Cycle bit = Command Ring's PCS flag.
     */

    uCtrl = USB_XHCI_TRB_CTRL_TYPE(USB_XHCI_TRB_ADDR_DEV) |
        USB_XHCI_TRB_CTRL_SLOT_ID(uSlotId);
#if 0
    if ((pHCDDevice->uDevSpeed == USBHST_FULL_SPEED) ||
        (pHCDDevice->uDevSpeed == USBHST_LOW_SPEED))
        uCtrl |= USB_XHCI_TRB_CTRL_BSR(1);
#endif
    return usbXhcdQueueCmd(pHCDData,
                           pHCDDevice,
                           waitTime,
                           uDataLo, uDataHi, 0, uCtrl);
    }

/*******************************************************************************
*
* usbXhcdQueueConfigureEndpointCmd - queue a Configure Endpoint Command
*
* This routine is to queue a Configure Endpoint Command TRB onto the command ring.
*
* The Configure Endpoint Command is issued by software to enable, disable,
* or reconfigure endpoints associated with a target configuration.
*
* This command is issued under the following circumstances:
*
* - Configuring a device.
*
* The Configure Endpoint Command shall be issued prior to issuing USB
* SET_CONFIGURATION request to a device. This command shall be used to enable
* the set of Device Slot endpoints selected by the target configuration, and
* transition a Device Slot from the Addressed to the Configured state.
*
* - Deconfiguring a device.
*
* The Configure Endpoint Command shall be issued prior to "deconfiguring" a
* device. A USB device is "deconfigured" by issuing a SET_CONFIGURATION request
* to a USB device with configuration '0' selected. Software shall issue a
* Configure Endpoint Command with Deconfigure DC)= '1' to inform the xHC that
* a SET_CONFIGURATION request with a configuration value of zero shall be sent
* to the device. This command shall be used to disable all enabled endpoints
* (except for the Default Control Endpoint) of a Device Slot, and transition
* the Device Slot from the Configured to the Addressed state.
*
* - Setting an Alternate Interface on a device.
*
* The Configure Endpoint Command shall be issued prior to issuing
* USB SET_INTERFACE request to a device. This command shall be used to disable,
* enable, or reconfigure a selected set of endpoints determined by the target
* Alternate Interface.
*
* RETURNS: OK when the command is queued, ERROR when there is no room on ring.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdQueueConfigureEndpointCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    bus_addr_t          uInputCtxBusAddr,
    UINT32              uSlotId,
    UINT32              uDC,
    _Vx_ticks_t         waitTime
    )
    {
    /* TRB Control */

    UINT32              uCtrl;

    /* Device structure for the Slot */

    pUSB_XHCD_DEVICE    pHCDDevice;

    /* LOW 32 bits of the 64 bit DATA */

    UINT32              uDataLo;

    /* HIGH 32 bits of the 64 bit DATA */

    UINT32              uDataHi;

    /* Get Device structure for the Slot */

    pHCDDevice = pHCDData->ppDevSlots[uSlotId];

    if (pHCDDevice == NULL)
        {
        USB_XHCD_ERR("usbXhcdQueueConfigureEndpointCmd - "
            "pHCDDevice for Slot ID %d NULL\n",
            uSlotId, 2, 3, 4, 5, 6);

        return ERROR;
        }

    uDataLo = USB_XHCD_LO32(uInputCtxBusAddr);
    uDataHi = USB_XHCD_HI32(uInputCtxBusAddr);

    USB_XHCD_WARN("usbXhcdQueueConfigureEndpointCmd - "
        "uInputCtxBusAddr 0x%016x - uDataHi 0x%08x uDataLo 0x%08x uSlotId %d uDC %d\n",
        uInputCtxBusAddr, uDataHi, uDataLo, uSlotId, uDC, 6);

    /*
     * 1. Ensure that the Device Context Base Address Array entry points to a
     * properly sized and initialized Device Context data structure for the
     * device.
     *
     * Note: This step should have been done by the Enable Slot Command.
     */

    /*
     * 2. Allocate and initialize an Input Context data structure for the
     * command.
     *
     * Note: This step should have been done by the caller and the BUS address
     * of the Input Context data structure is provided as parameter to this
     * routine.
     */

    /*
     * 3. Insert an Configure Endpoint Command TRB on the Command Ring and
     * initialize the following fields:
     * - TRB Type = Configure Endpoint Command.
     * - Slot ID = ID of the target Device Slot.
     * - Input Context Pointer = Base address of Input Context data structure.
     * - Clear all other fields of the command TRB to '0'.
     * - Cycle bit = Command Ring's PCS flag.
     */

    uCtrl = USB_XHCI_TRB_CTRL_TYPE(USB_XHCI_TRB_CONFIG_EP) |
        USB_XHCI_TRB_CTRL_SLOT_ID(uSlotId) |
        USB_XHCI_TRB_CTRL_DC(uDC);

    return usbXhcdQueueCmd(pHCDData,
                           pHCDDevice,
                           waitTime,
                           uDataLo, uDataHi, 0, uCtrl);
    }

/*******************************************************************************
*
* usbXhcdQueueEvaluateContextCmd - queue a Evaluate Context Command
*
* This routine is to queue a Evaluate Context Command TRB onto the command ring.
*
* The Evaluate Context Command is issued by software to inform the xHC that
* specific fields in the Device Context data structures have been modified.
* There are several cases where parameters associated with a Slot Context or
* the Default Control Endpoint Context are initially unknown, which shall be
* updated after the slot has entered the Addressed state. e.g. the Max Packet
* Size of the control endpoint may be determined only after software reads the
* Device Descriptor from the device through the control endpoint. The Device
* Descriptor shall be read to determine whether a device is a hub or not, etc.
* The Evaluate Context Command allows software to update these fields and
* others while a Device Slot is in the Default, Addressed or Configured state.
*
* RETURNS: OK when the command is queued, ERROR when there is no room on ring.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdQueueEvaluateContextCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    bus_addr_t          uInputCtxBusAddr,
    UINT32              uSlotId,
    _Vx_ticks_t         waitTime
    )
    {
    /* TRB Control */

    UINT32              uCtrl;

    /* Device structure for the Slot */

    pUSB_XHCD_DEVICE    pHCDDevice;

    /* LOW 32 bits of the 64 bit DATA */

    UINT32              uDataLo;

    /* HIGH 32 bits of the 64 bit DATA */

    UINT32              uDataHi;

    /* Get Device structure for the Slot */

    pHCDDevice = pHCDData->ppDevSlots[uSlotId];

    if (pHCDDevice == NULL)
        {
        USB_XHCD_ERR("usbXhcdQueueEvaluateContextCmd - "
            "pHCDDevice for Slot ID %d NULL\n",
            uSlotId, 2, 3, 4, 5, 6);

        return ERROR;
        }

    uDataLo = USB_XHCD_LO32(uInputCtxBusAddr);
    uDataHi = USB_XHCD_HI32(uInputCtxBusAddr);

    /*
     * 1. Ensure that the Device Context Base Address Array entry points to a
     * properly sized and initialized Device Context data structure for the
     * device.
     *
     * Note: This step should have been done by the Enable Slot Command.
     */

    /*
     * 2. Allocate and initialize an Input Context data structure for the
     * command.
     *
     * Note: This step should have been done by the caller and the BUS address
     * of the Input Context data structure is provided as parameter to this
     * routine.
     */

    /*
     * 3. Insert an Evaluate Context Command TRB on the Command Ring and
     * initialize the following fields:
     * - TRB Type = Evaluate Context Command.
     * - The Add Context flags shall be initialized to indicate the IDs of
     *   the Contexts affected by the command.
     * - Set all Drop Context flags to '0'.
     * - Slot ID = ID of the target Device Slot.
     * - Input Context Pointer = Base address of Input Context data structure.
     * - Clear all other fields of the command TRB to '0'.
     * - Cycle bit = Command Ring's PCS flag.
     */

    uCtrl = USB_XHCI_TRB_CTRL_TYPE(USB_XHCI_TRB_EVAL_CONTEXT) |
        USB_XHCI_TRB_CTRL_SLOT_ID(uSlotId);

    return usbXhcdQueueCmd(pHCDData,
                           pHCDDevice,
                           waitTime,
                           uDataLo, uDataHi, 0, uCtrl);
    }

/*******************************************************************************
*
* usbXhcdQueueResetEndpointCmd - queue a Reset Endpoint Command
*
* This routine is to queue a Reset Endpoint Command TRB onto the command ring.
*
* The Reset Endpoint Command is issued by software to recover from a halted
* condition on an endpoint.
*
* When a Transfer Ring or Stream is halted; the associated endpoint is removed
* from the xHC's pipe schedule, the Doorbell Register for that pipe is disabled,
* the state of the associated Endpoint Context is set to Halted, and any
* subsequent packets received for the endpoint will be silently dropped. The
* Reset Endpoint Command defines Slot ID and Endpoint ID fields. The Slot
* ID and Endpoint ID fields identify the USB device, and the endpoint of that
* device that is the target of the command.
*
* RETURNS: OK when the command is queued, ERROR when there is no room on ring.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdQueueResetEndpointCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    UINT32              uSlotId,
    UINT32              uEpId,
    UINT32              uTSP,
    _Vx_ticks_t         waitTime
    )
    {
    /* TRB Control */

    UINT32              uCtrl;

    /* Device structure for the Slot */

    pUSB_XHCD_DEVICE    pHCDDevice;

    /* Get Device structure for the Slot */

    pHCDDevice = pHCDData->ppDevSlots[uSlotId];

    if (pHCDDevice == NULL)
        {
        USB_XHCD_ERR("usbXhcdQueueResetEndpointCmd - "
            "pHCDDevice for Slot ID %d NULL\n",
            uSlotId, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * 1. Ensure that the Device Context Base Address Array entry points to a
     * properly sized and initialized Device Context data structure for the
     * device.
     *
     * Note: This step should have been done by the Enable Slot Command.
     */

    /*
     * 2. Allocate and initialize an Input Context data structure for the
     * command.
     *
     * Note: This step should have been done by the caller and the BUS address
     * of the Input Context data structure is provided as parameter to this
     * routine.
     */

    /*
     * 3. Insert an Reset Endpoint Command TRB on the Command Ring and
     * initialize the following fields:
     * - TRB Type = Reset Endpoint Command.
     * - Transfer State Preserve = Desired Transfer State result. The USB2
     *   Data Toggle or the USB3 Sequence Number for the pipe will be cleared
     *   to '0' if the value of this field is '1'. If '1' their state is
     *   preserved.
     * - Endpoint ID = ID of the target endpoint.
     * - Slot ID = ID of the target Device Slot.
     * - Clear all other fields of the command TRB to '0'.
     * - Cycle bit = Command Ring's PCS flag.
     */

    /*
     * If the endpoint is not in the Halted state when an Reset Endpoint
     * Command is executed:
     *
     *     - The xHC shall reject the command and generate a Command
     *     Completion Event with the Completion Code set to Context
     *     State Error.
     * else
     *
     *     - If the Transfer State Preserve (TSP) flag is '0':
     *
     *        - Reset the Data Toggle for USB2 devices or the Sequence
     *        Number for USB3 devices.
     *        - Invalidate any xHC TDs that may be cached, forcing xHC
     *        to fetch Transfer TRBs from memory when the pipe transitions
     *        from the Stopped to Running state.
     *
     *     - else // TSP = '1'
     *
     *        - The USB2 Data Toggle or the USB3 Sequence Number for the
     *        pipe shall be preserved.
     *        - The endpoint shall continue execution by retrying the last
     *        transaction the next time the doorbell is rung, if no other
     *        commands have been issued to the endpoint.
     */

    uCtrl = USB_XHCI_TRB_CTRL_TYPE(USB_XHCI_TRB_RESET_EP) |
        USB_XHCI_TRB_CTRL_SLOT_ID(uSlotId) |
        USB_XHCI_TRB_CTRL_EP_ID(uEpId) |
        USB_XHCI_TRB_CTRL_TSP(uTSP);

    return usbXhcdQueueCmd(pHCDData,
                           pHCDDevice,
                           waitTime,
                           0, 0, 0, uCtrl);
    }

/*******************************************************************************
*
* usbXhcdQueueStopEndpointCmd - queue aStop Endpoint Command
*
* This routine is to queue a Stop Endpoint Command TRB onto the command ring.
*
* The Stop Endpoint Command is issued by software to stop the xHC execution
* of the TDs on an endpoint. An endpoint may be stopped by software so that
* it can temporarily take ownership of Transfer Ring TDs that had previously
* been passed to the xHC, or to stop USB activity prior to powering down the
* xHC. While the endpoint is stopped, software may add, delete, or otherwise
* rearrange TDs on an associated Transfer Ring. e.g. this command allows
* software to insert "high-priority" TDs at the Dequeue Pointer so they will
* be executed immediately when the ring is restarted, or to "abort" one or
* more TDs by removing them from the ring.
*
* RETURNS: OK when the command is queued, ERROR when there is no room on ring.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdQueueStopEndpointCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    UINT32              uSlotId,
    UINT32              uEpId,
    UINT8               uSuspend,
    _Vx_ticks_t         waitTime
    )
    {
    /* TRB Control */

    UINT32              uCtrl;

    /* Device structure for the Slot */

    pUSB_XHCD_DEVICE    pHCDDevice;

    /* Get Device structure for the Slot */

    pHCDDevice = pHCDData->ppDevSlots[uSlotId];

    if (pHCDDevice == NULL)
        {
        USB_XHCD_ERR("usbXhcdQueueStopEndpointCmd - "
            "pHCDDevice for Slot ID %d NULL\n",
            uSlotId, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * 1. Ensure that the Device Context Base Address Array entry points to a
     * properly sized and initialized Device Context data structure for the
     * device.
     *
     * Note: This step should have been done by the Enable Slot Command.
     */

    /*
     * 2. Allocate and initialize an Input Context data structure for the
     * command.
     *
     * Note: This step should have been done by the caller and the BUS address
     * of the Input Context data structure is provided as parameter to this
     * routine.
     */

    /*
     * 3. Insert an Stop Endpoint Command TRB on the Command Ring and
     * initialize the following fields:
     * - TRB Type = Stop Endpoint Command.
     * - Endpoint ID = ID of the target endpoint.
     * - Slot ID = ID of the target Device Slot.
     * - Clear all other fields of the command TRB to '0'.
     * - Cycle bit = Command Ring's PCS flag.
     */

    uCtrl = USB_XHCI_TRB_CTRL_TYPE(USB_XHCI_TRB_STOP_RING) |
        USB_XHCI_TRB_CTRL_SLOT_ID(uSlotId) |
        USB_XHCI_TRB_CTRL_EP_ID(uEpId) |
        USB_XHCI_TRB_CTRL_SP(uSuspend);

    return usbXhcdQueueCmd(pHCDData,
                           pHCDDevice,
                           waitTime,
                           0, 0, 0, uCtrl);
    }

/*******************************************************************************
*
* usbXhcdQueueSetTRDeqPtrCmd - queue a Set TR Dequeue Pointer
*
* This routine is to queue a Set TR Dequeue Pointer TRB onto the command ring.
*
* The Set TR Dequeue Pointer Command is issued by software to modify the TR
* Dequeue Pointer field of an Endpoint or Stream Context.
*
* The Slot ID and Endpoint ID fields of the Set TR Dequeue Pointer Command
* identify the USB device, and the endpoint of that device, that is the target
* of the command. If Streams are enabled for the endpoint, the Set TR Dequeue
* Pointer Command Stream ID field identifies the Stream Context that shall be
* modified.
*
* This command may be executed only if the target endpoint is in the Error or
* Stopped state.
*
* RETURNS: OK when the command is queued, ERROR when there is no room on ring.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdQueueSetTRDeqPtrCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    bus_addr_t          uDeqPtrBusAddr,
    UINT32              uSlotId,
    UINT32              uEpId,
    UINT32              uStreamId,
    UINT32              uDCS,
    UINT32              uSCT,
    _Vx_ticks_t         waitTime
    )
    {
    /* Device structure for the Slot */

    pUSB_XHCD_DEVICE    pHCDDevice;

    /* LOW 32 bits of the 64 bit DATA */

    UINT32              uDataLo;

    /* HIGH 32 bits of the 64 bit DATA */

    UINT32              uDataHi;

    /* TRB Infomation */

    UINT32              uInfo;

    /* TRB Control */

    UINT32              uCtrl;

    /* Get Device structure for the Slot */

    pHCDDevice = pHCDData->ppDevSlots[uSlotId];

    if (pHCDDevice == NULL)
        {
        USB_XHCD_ERR("usbXhcdQueueConfigureEndpointCmd - "
            "pHCDDevice for Slot ID %d NULL\n",
            uSlotId, 2, 3, 4, 5, 6);

        return ERROR;
        }

    uDataLo = USB_XHCD_LO32(uDeqPtrBusAddr);
    uDataHi = USB_XHCD_HI32(uDeqPtrBusAddr);

    /*
     * Insert an Set TR Dequeue Pointer Command TRB on the Command Ring and
     * initialize the following fields:
     * - TRB Type = Set TR Dequeue Pointer Command.
     * - Endpoint ID = ID of the target endpoint.
     * - Stream ID = ID of the target Stream Context or '0' if MaxPStreams = '0'.
     * - Slot ID = ID of the target Device Slot.
     * - New TR Dequeue Pointer = The new TR Dequeue Pointer field value for
     *   the target endpoint.
     * - Dequeue Cycle State (DCS) = The state of the xHCI CCS flag for the
     *   TRB pointed to by the TR Dequeue Pointer field.
     * - Clear all other fields of the command TRB to '0'.
     * - Cycle bit = Command Ring's PCS flag.
     */

    uCtrl = USB_XHCI_TRB_CTRL_TYPE(USB_XHCI_TRB_SET_DEQ) |
        USB_XHCI_TRB_CTRL_SLOT_ID(uSlotId) |
        USB_XHCI_TRB_CTRL_EP_ID(uEpId);

    uDataLo |= (USB_XHCI_TRB_DATALO_DCS(uDCS) |
        USB_XHCI_TRB_DATALO_SCT(uSCT));

    uInfo = USB_XHCI_TRB_INFO_STREAM_ID(uStreamId);

    /*
     * If the endpoint is not in the Error or Stopped state when the Set
     * TR Dequeue Pointer Command is executed:
     *
     * The xHC shall reject the command and generate a Command Completion
     * Event with the Completion Code set to Context State Error.
     *
     * else if the endpoint is in the Error or Stopped state:
     *
     * Set the Dequeue Pointer to the value of the New TR Dequeue Pointer
     * field in the Set TR Dequeue Pointer TRB.
     *
     * Invalidate any xHC TDs that may be cached, forcing xHC to fetch
     * Transfer TRBs from memory when the pipe transitions from the Stopped
     * to the Running state.
     */

    return usbXhcdQueueCmd(pHCDData,
                           pHCDDevice,
                           waitTime,
                           uDataLo, uDataHi, uInfo, uCtrl);
    }

/*******************************************************************************
*
* usbXhcdQueueResetDeviceCmd - queue a Reset Device Command
*
* This routine is to queue a Reset Device Command TRB onto the command ring.
*
* The Reset Device Command is used by software to inform the xHC that the USB
* Device associated with a Device Slot has been Reset (by either; clearing the
* Root Hub port PR flag if the device is attached to a Root Hub port, or
* issuing a SetPortFeature(PORT_RESET) request the external hub port upstream
* of the device). In the Slot Context of the selected device slot, the reset
* operation sets the Slot State field to the Default state and the USB Device
* Address field to '0'. The reset operation also disables all endpoints of the
* slot except for the Default Control Endpoint by setting the Endpoint Context
* EP State field to Disabled in all enabled Endpoint Contexts. For all endpoints
* except the Default Control Endpoint the xHC shall terminate any USB activity
* (e.g. packet transfers), disable the endpoints Doorbell, abort any pending
* events not already posted to an Event Ring, free any bandwidth allocated to
* the periodic endpoints, and free any internal resources associated with the
* endpoint. For the Default Control Endpoint the xHC shall terminate any USB
* activity, abort any pending events not already posted to an Event Ring, and
* reset the endpoint state (e.g. Sequence Number = 0, etc.). Undefined behavior
* may occur if this command is executed and the device associated with it is
* not successfully reset.
*
* RETURNS: OK when the command is queued, ERROR when there is no room on ring.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdQueueResetDeviceCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    UINT32              uSlotId,
    _Vx_ticks_t         waitTime
    )
    {
    /* TRB Control */

    UINT32              uCtrl;

    /* Device structure for the Slot */

    pUSB_XHCD_DEVICE    pHCDDevice;

    /* Get Device structure for the Slot */

    pHCDDevice = pHCDData->ppDevSlots[uSlotId];

    if (pHCDDevice == NULL)
        {
        USB_XHCD_ERR("usbXhcdQueueResetDeviceCmd - "
            "pHCDDevice for Slot ID %d NULL\n",
            uSlotId, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * Insert an Reset Device Command TRB on the Command Ring and initialize
     * the following fields:
     * - TRB Type = Reset Device Command.
     * - Slot ID = The ID of the Device Slot to reset.
     * - Clear all other fields of the command TRB to '0'.
     * - Cycle bit = Command Ring's PCS flag.
     */

    uCtrl = USB_XHCI_TRB_CTRL_TYPE(USB_XHCI_TRB_RESET_DEV) |
        USB_XHCI_TRB_CTRL_SLOT_ID(uSlotId);

    return usbXhcdQueueCmd(pHCDData,
                           pHCDDevice,
                           waitTime,
                           0, 0, 0, uCtrl);
    }

/*******************************************************************************
*
* usbXhcdQueueForceEventCmd - queue a Force Event Command
*
* This routine is to queue a Force Event Command TRB onto the command ring.
*
* The Force Event Command is used by a VMM to insert an Event TRB in an Event
* Ring of a target VM when the VMM is emulating an xHC device to a VM.
*
* When a Force Event Command is processed by the xHC it shall insert an Event
* TRB on the target VF's Event Ring and copy the data pointed to by the Force
* Event Command, with the exception of the Cycle bit, to the target Event TRB.
* The xHC shall set the Cycle bit to be consistent with the target VF's Event
* Ring.
*
* A Command Completion Event with a TRB Error will be generated if the VF ID
* of the Force Event Command is not valid. A VF Event Ring Full Error will be
* generated if the Target VF's Event Ring is full.
*
* RETURNS: OK when the command is queued, ERROR when there is no room on ring.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdQueueForceEventCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    UINT64              uEvtTrbBusAddr,
    UINT32              uVfId,
    UINT32              uIntrTarget,
    _Vx_ticks_t         waitTime
    )
    {
    /* TRB Control */

    UINT32 uCtrl;

    /* TRB Info */

    UINT32 uInfo;

    UINT32 uDataLo = USB_XHCD_LO32(uEvtTrbBusAddr);
    UINT32 uDataHi = USB_XHCD_HI32(uEvtTrbBusAddr);

    /*
     * 1. Ensure that the Device Context Base Address Array entry points to a
     * properly sized and initialized Device Context data structure for the
     * device.
     *
     * Note: This step should have been done by the Enable Slot Command.
     */

    /*
     * 2. Allocate and initialize the VF Event TRB that will be sent to the
     * target VF's Event Ring. The details of the VF Event TRB initialization
     * will depend on the type of Event that is being forced.
     *
     * Note: This step should have been done by the caller and the BUS address
     * of the VF Event TRB data structure is provided as parameter to this
     * routine.
     */

    /*
     * 3. Insert an Force Event Command TRB on the Command Ring and initialize
     * the following fields:
     * - TRB Type = Force Event Command.
     * - VF ID = ID of the target VF.
     * - VF Interrupter Target = The ID of the target Interrupter assigned to
     *   the VF.
     * - Event TRB Pointer = The address of the VF Event TRB.
     * - Clear all other fields of the command TRB to '0'.
     * - Cycle bit = Command Ring's PCS flag.
     */

    uCtrl = USB_XHCI_TRB_CTRL_TYPE(USB_XHCI_TRB_FORCE_EVENT) |
        USB_XHCI_TRB_CTRL_VF_ID(uVfId);

    uInfo = USB_XHCI_TRB_INFO_INTR_TARGET(uIntrTarget);

    return usbXhcdQueueCmd(pHCDData,
                           NULL,
                           waitTime,
                           uDataLo, uDataHi, uInfo, uCtrl);
    }

/*******************************************************************************
*
* usbXhcdQueueNegotiateBandwidthCmd - queue a Negotiate Bandwidth Command
*
* This routine is to queue a Negotiate Bandwidth Command TRB onto command ring.
*
* The Negotiate Bandwidth Command is used by system software to initiate
* Bandwidth Request Events for periodic endpoints. This command should be used
* to recover unused USB bandwidth from the system.
*
* If the BW Negotiation Capability (BNC) bit in the HCCPARAMS register is '1',
* the xHC shall support this command.
*
* This command will complete with a Success Completion Code if the command is
* supported, or a TRB Error Completion Code if the command is not supported.
*
* The xHC shall generate Bandwidth Request Events upon the reception of the
* command to all target periodic endpoints. The command will complete when all
* Bandwidth Request Events have been generated.
*
* RETURNS: OK when the command is queued, ERROR when there is no room on ring.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdQueueNegotiateBandwidthCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    UINT32              uSlotId,
    _Vx_ticks_t         waitTime
    )
    {
    /* TRB Control */

    UINT32              uCtrl;

    /* Device structure for the Slot */

    pUSB_XHCD_DEVICE    pHCDDevice;

    /* Get Device structure for the Slot */

    pHCDDevice = pHCDData->ppDevSlots[uSlotId];

    if (pHCDDevice == NULL)
        {
        USB_XHCD_ERR("usbXhcdQueueResetDeviceCmd - "
            "pHCDDevice for Slot ID %d NULL\n",
            uSlotId, 2, 3, 4, 5, 6);

        return ERROR;
        }

    /*
     * Insert an Negotiate Bandwidth Command TRB on the Command Ring and
     * initialize the following fields:
     * - TRB Type = Negotiate Bandwidth Command.
     * - Slot ID = ID of the Device Slot to be disabled.
     * - Clear all other fields of the command TRB to '0'.
     * - Cycle bit = Command Ring's PCS flag.
     */

    uCtrl = USB_XHCI_TRB_CTRL_TYPE(USB_XHCI_TRB_NEG_BANDWIDTH) |
        USB_XHCI_TRB_CTRL_SLOT_ID(uSlotId);

    return usbXhcdQueueCmd(pHCDData,
                           pHCDDevice,
                           waitTime,
                           0, 0, 0, uCtrl);
    }

/*******************************************************************************
*
* usbXhcdQueueSetLTVCmd - queue a Set Latency Tolerance Value (LTV) Command
*
* This routine is to queue a Set Latency Tolerance Value (LTV) TRB onto
* command ring.
*
* The Set LTV Command provides a simple means for host software to provide a
* Best Effort Latency Tolerance (BELT) value to the xHC. This command is
* optional normative, however it shall be supported if the xHC also supports a
* corresponding host interconnect LTM mechanism.
*
* Note: The host's interconnect LTM definition is owned by the respective system
* bus specification (e.g. PCI Express, AHBA, etc.)
*
* The value of the BELT field in the Set LTV Command TRB shall be treated in
* exactly the same way as BELT values received from USB3 devices by the xHC.
*
* Note: The manner in which these values are stored is implementation specific.
*
* If the Latency Tolerance Messaging Capability (LTC) bit in the HCCPARAMS
* register is '0', the xHC shall not support this command.
*
* Note: If LTC = 0, then this xHC implementation does not translate LTM
* messages from a device into system LTM messages. However, if enabled in the
* DNCTRL register (N2 = '1'), then LTM Device Notification TPs are received by
* the xHC shall generate Device Notification Events.
*
* This command will complete with a Success Completion Code if the command is
* supported, or a TRB Error Completion Code if the command is not supported.
*
* RETURNS: OK when the command is queued, ERROR when there is no room on ring.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdQueueSetLTVCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    UINT32              uBELT,
    _Vx_ticks_t         waitTime
    )
    {
    /* TRB Control */

    UINT32 uCtrl;

    /*
     * Insert an Set Latency Tolerance Value Command TRB on the Command Ring
     * and initialize the following fields:
     * - TRB Type = Set Latency Tolerance Value Command.
     * - BELT = The Best Effort Latency Tolerance value provided by software.
     * - Clear all other fields of the command TRB to '0'.
     * - Cycle bit = Command Ring's PCS flag.
     */

    uCtrl = USB_XHCI_TRB_CTRL_TYPE(USB_XHCI_TRB_SET_LTV) |
        USB_XHCI_TRB_CTRL_BELT(uBELT);

    return usbXhcdQueueCmd(pHCDData,
                           NULL,
                           waitTime,
                           0, 0, 0, uCtrl);
    }

/*******************************************************************************
*
* usbXhcdQueueGetPortBandwidthCmd - queue a Get Port Bandwidth Command
*
* This routine is to queue a Get Port Bandwidth TRB onto the command ring.
*
* The Get Port Bandwidth Command is issued by software to retrieve the
* percentage of Total Available Bandwidth on each Root Hub Port of the xHC or
* on the downstream facing ports of a external USB hub. This information can
* be used by system software to recommend topology changes to the user if they
* were unable to enumerate a device due to a Bandwidth Error (Root Hub) or
* Secondary Bandwidth Error (external hub).
*
* An xHC may support multiple USB Bus Instances (BI), where each BI represents
* a "unit" bandwidth at the speed that the BI supports. Also note that multiple
* Root Hub ports may be assigned to a single BI. For instance, an xHCI
* implementation that supports 8 ports may provide 1 SS BI, 2 HS BIs, and 4
* LS/FS BIs. So in this example there are 7 USB BIs, 1 SS(5Gb/s), 2 HS(480 Mb/s)
* and 4 LS/FS(12Mb/s). Any SS device attached to a root hub port shares the SS
* BI bandwidth. If the 2 HS BIs are mapped to ports 0 to 3 and 4 to 7, and the
* 4 LS/FS BIs are mapped to ports 0 and 1, 2 and 3, 4 and 5, and 6 and 7,
* respectively, then an LS/FS device attached to port 5 shares the BW available
* on port 4 provided by one the LS/FS BIs, but not with any other ports. A more
* sophisticated xHC implementation may have the ability to dynamically map ports
* to BIs as function of a device's bandwidth requirements.
*
* A USB2 hub may support a single or multiple Transaction Translators (TT),
* where a single TT is capable of providing the equivalent of a LS/FS BI's
* bandwidth. If a USB2 Hub supports a single TT, then all of its downstream
* facing ports attached to LS or FS devices shall share the bandwidth of the
* single TT (i.e. a LS/FS BI). If a USB2 Hub supports a multi-TT capability,
* then a separate TT exists for each of its downstream facing ports and each
* port is capable of providing the bandwidth of a LS/FS BI.
*
* When software issues a Get Port Bandwidth Command it is trying to accommodate
* the bandwidth requirements of a particular device. By providing a Device
* Speed parameter in the Get Port Bandwidth Command, the xHC can supply software
* with Total Available Bandwidth on each port of the Root Hub or USB2 hub, at a
* particular speed, without exposing its BI or TT to Port mapping scheme.
*
* Software, knowing the percentage of Total Available Bandwidth on a hub port,
* the speed that the device in question is operating at, and the device's
* bandwidth requirement, may determine if a particular port will meet the
* device's bandwidth needs.
*
* The xHC uses the Device Speed parameter to establish a speed for a Root Hub
* port that does not currently have a device attached, when it calculates the
* Total Available Bandwidth on that port.
*
* The Get Port Bandwidth Command passes a pointer to a Port Bandwidth Context
* data structure to the xHC. The xHC updates this context with the percentage
* of Total Available Bandwidth on each port. If a hub is attached to a Root Hub
* port then the reported bandwidth is available on any unused port of the hub
* or any port of the hub that is operating at the Device Speed.
*
* For the Root Hub the Port Bandwidth Context shall be at least NumPorts+1
* bytes in size or for an external hub the Port Bandwidth Context shall be at
* least bNbrPorts+1 bytes in size, rounded up to the nearest Dword boundary.
*
* The xHC overwrites the Port Bandwidth Context when it executes the Get Port
* Bandwidth Command, so software does not need to initialize the context data
* structure before passing it to the xHC.
*
* - A Root Hub port assigned to the Debug Capability shall report '0' bandwidth
*   available.
* - If the Device Speed parameter is LS, FS, or HS, then USB3 (SS) Root Hub
*   ports shall report '0' bandwidth available.
* - If the Device Speed parameter is SS, then USB2 Root Hub ports shall report
*   '0' bandwidth available.
*
* Note: Software shall consider any port that reports '0' bandwidth available
* as being unusable. A port that, as far as software is concerned, does not
* have a device attached may report '0' bandwidth available. e.g. a VMM shall
* report '0' bandwidth for a port if the device attached to it is assigned to
* another VF.
*
* Consider a physical connector that is "USB3 compatible" and has a SS device
* attached it. The connector will be wired to a USB2 and a USB3 Root Hub Port.
* When the USB2 Root Hub Port is queried for its HS bandwidth availability, it
* will not know that a SS device is attached to physical connector and report
* a non-zero HS bandwidth availability, when in reality the USB2 Root Hub port
* is not available because it is associated with a physical connector that is
* attached to SS device. The same problem will occur with a USB3 Root Hub port
* if a USB2 device or hub is attached to the physical connector. Note that the
* problem does not occur if a USB3 hub is attached because both Root Hub Ports
* see a hub attached. Software, knowing the Root Hub Port to physical USB
* connector mapping and whether the attached device is a USB2 or USB3 hub, shall
* be responsible for correcting the reported Port Bandwidth Values.
*
* RETURNS: OK when the command is queued, ERROR when there is no room on ring.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdQueueGetPortBandwidthCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    bus_addr_t          uPortBwCtxBusAddr,
    UINT32              uDevSpeed,
    UINT32              uHubSlotId,
    _Vx_ticks_t         waitTime
    )
    {
    /* TRB Control */

    UINT32              uCtrl;

    /* Device structure for the Slot */

    pUSB_XHCD_DEVICE    pHCDDevice;

    /* LOW 32 bits of the 64 bit DATA */

    UINT32              uDataLo;

    /* HIGH 32 bits of the 64 bit DATA */

    UINT32              uDataHi;

    /* Get Device structure for the Slot */

    pHCDDevice = pHCDData->ppDevSlots[uHubSlotId];

    if (pHCDDevice == NULL)
        {
        USB_XHCD_WARN("usbXhcdQueueGetPortBandwidthCmd - "
            "pHCDDevice for Slot ID %d NULL - "
            "assume getting Root Hub port bandwidth\n",
            uHubSlotId, 2, 3, 4, 5, 6);
        }

    uDataLo = USB_XHCD_LO32(uPortBwCtxBusAddr);
    uDataHi = USB_XHCD_HI32(uPortBwCtxBusAddr);

    /*
     * 1. Ensure that the Device Context Base Address Array entry points to a
     * properly sized and initialized Device Context data structure for the
     * device.
     *
     * Note: This step should have been done by the Enable Slot Command.
     */

    /*
     * 2. Allocate and initialize an Port Bandwidth Context data structure.
     *
     * Note: This step should have been done by the caller and the BUS address
     * of the Port Bandwidth Context data structure is provided as parameter
     * to this routine.
     */

    /*
     * 3. Insert an  Get Port Bandwidth Command TRB on the Command Ring and
     * initialize the following fields:
     * - TRB Type =  Get Port Bandwidth Command.
     * - Dev Speed = The bus speed of the target device.
     * - Hub Slot ID = '0' if referencing Root Hub ports (i.e. the Primary
     *   Bandwidth Domain) or the value of the respective hub's Slot ID if
     *   referencing the ports of a USB2 hub (i.e. a Secondary Bandwidth
     *   Domain).
     * - Port Bandwidth Context Pointer = The base address of the Port
     *   Bandwidth Context data structure.
     * - Clear all other fields of the command TRB to '0'.
     * - Cycle bit = Command Ring's PCS flag.
     */

    uCtrl = USB_XHCI_TRB_CTRL_TYPE(USB_XHCI_TRB_GET_PORT_BW) |
        USB_XHCI_TRB_CTRL_DEV_SPEED(uDevSpeed) |
        USB_XHCI_TRB_CTRL_SLOT_ID(uHubSlotId);

    return usbXhcdQueueCmd(pHCDData,
                           pHCDDevice,
                           waitTime,
                           uDataLo, uDataHi, 0, uCtrl);
    }

/*******************************************************************************
*
* usbXhcdQueueForceHeaderCmd - queue a Force Header Command
*
* This routine is to queue a Force Header Command TRB onto the command ring.
*
* The Force Header Command is issued by software to send a Link Management
* (LMP) or Transaction Packet (TP) to a USB device, through a selected Root
* Hub Port. For instance, it may be used to send a PING TP or a Vendor Device
* Test LMP.
*
* RETURNS: OK when the command is queued, ERROR when there is no room on ring.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdQueueForceHeaderCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    UINT8               uRootHubPort,
    UINT8               uHdrType,
    UINT32              uHdrInfoLow,
    UINT32              uHdrInfoMid,
    UINT32              uHdrInfoHigh
    )
    {
    /* TRB Control */

    UINT32 uCtrl;

    /*
     * To issue a Force Header Command, system software shall perform
     * the following operations:
     *
     * Insert a Force Header Command TRB on the Command Ring
     * TRB Type = Force Header Command.
     * Root Hub Port Number = The number of the Root Hub Port that defines
     * the target of the Header packet.
     * Packet Type = The field identifies the SS packet type.
     * Header Info = The header Type specific data to send to the target device.
     * Clear all other fields of the command TRB to '0'
     * Cycle bit = Command Ring's PCS flag.
     */

    uCtrl = USB_XHCI_TRB_CTRL_TYPE(USB_XHCI_TRB_FORCE_HEADER) |
            USB_XHCI_TRB_CTRL_RH_PORT(uRootHubPort);

    /* Format the DataLo field (Header Info Lo starts at bit offset 5) */

    uHdrInfoLow = (uHdrInfoLow << 5) | uHdrType;

    return usbXhcdQueueCmd(pHCDData,
                           NULL,
                           NO_WAIT,
                           uHdrInfoLow, uHdrInfoMid, uHdrInfoHigh, uCtrl);
    }

/*******************************************************************************
*
* usbXhcdNotifyCommandReady - notify the HC that the command is ready
*
* This routine is to notify the HC that the command is ready to be processed by
* writting the Doorbell Register 0 with Host Controller Command (0).
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbXhcdNotifyCommandReady
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_DEVICE    pHCDDevice,
    pUSB_XHCD_CMD       pHCDCmd
    )
    {
    STATUS uSts = OK;

    /* Value of Doorbell Register 0 register */

    UINT32 uReg32;

    /* TRB Control Field */

    UINT32 uCtrl;

    /* Doorbell Register 0 is dedicated to Command Ring */

    uReg32 = USB_XHCD_READ_DB_REG32(pHCDData, USB_XHCI_DOOR_BELL(0));

    /* Clear the other fields except for RsvdZ field */

    uReg32 &= USB_XHCI_DB_RsvdZ_MASK;

    /* Set the DB Target to Host Controller Command */

    uReg32 |= USB_XHCI_DOOR_BELL_DB_TARGET(USB_XHCI_DB_TARGET_HCCMD);

    USB_XHCD_VDBG("usbXhcdNotifyCommandReady - Ring HC Command DB @%p!\n",
        (pHCDData->uDbRegBase + USB_XHCI_DOOR_BELL(0)), 2, 3, 4, 5, 6);

    /* Write the Doorbell Register 0 register */

    USB_XHCD_WRITE_DB_REG32(pHCDData, USB_XHCI_DOOR_BELL(0), uReg32);

    /* Read it back to flush PCI posted writes */

    (void) USB_XHCD_READ_DB_REG32(pHCDData, USB_XHCI_DOOR_BELL(0));

    if (pHCDCmd->waitTime != NO_WAIT)
        {
        USB_XHCD_VDBG("usbXhcdNotifyCommandReady - "
            "Wait for command %d ticks to complete...\n",
            pHCDCmd->waitTime, 2, 3, 4, 5, 6);

        if (pHCDDevice != NULL)
            {
            uSts = OS_WAIT_FOR_EVENT(pHCDDevice->cmdDoneSem, pHCDCmd->waitTime);
            }
        else
            {
            uSts = OS_WAIT_FOR_EVENT(pHCDData->cmdDoneSem, pHCDCmd->waitTime);
            }

        /*
         * No matter if the command is waited (completed in time) or
         * not, we release the command strucuture back to the HCD
         * free commands list. If the command does not complete in
         * time (the wait times out), there might be a race condition
         * in the wait task and the task handling the command completion
         * if it just completed at the time we are withdrawing it.
         *
         * If the waiting task times out, we convert the command TRB
         * to be a No-Op Command TRB so that it does not do any actual
         * work. We make sure the conversion to No-Op does not change
         * the Cycle bit (so it does not break the xHCI Command Ring!)
         */

        /* Exclusively access the pipe request list */

        (void) OS_WAIT_FOR_EVENT(pHCDData->hcdMutex, OS_WAIT_INFINITE);

        if (uSts != OK)
            {
            UINT32 uCycle;

            /* TRB Control */

            uCtrl = USB_XHCD_SWAP_DESC_DATA32(pHCDData,
                            (pHCDCmd->pCmdTRB->uCtrl));

            USB_XHCD_WARN("usbXhcdNotifyCommandReady - %s TRB Timed Out!\n",
                 usbXhcdTrbName((UINT8)USB_XHCI_TRB_CTRL_TYPE_GET(uCtrl)),
                 2, 3, 4, 5, 6);

             /*
              * Insert a No Op Command TRB on the Command Ring and initialize
              * the following fields:
              * - TRB Type = No Op Command.
              * - Clear all other fields of the command TRB to '0'.
              * - Cycle bit = Command Ring's PCS flag.
              */

            uCycle = uCtrl & USB_XHCI_TRB_CTRL_CYCLE_BIT_MASK;
            uCtrl = USB_XHCI_TRB_CTRL_TYPE(USB_XHCI_TRB_CMD_NOOP);

            pHCDCmd->pCmdTRB->uDataLo = 0;
            pHCDCmd->pCmdTRB->uDataHi = 0;
            pHCDCmd->pCmdTRB->uInfo = 0;
            pHCDCmd->pCmdTRB->uCtrl =
                USB_XHCD_SWAP_DESC_DATA32(pHCDData, (uCtrl | uCycle));

            }
        else
            {
            if (pHCDCmd->uCompCode != USB_XHCI_COMP_SUCCESS)
                {
                /* TRB Control */

                uCtrl = USB_XHCD_SWAP_DESC_DATA32(pHCDData,
                                (pHCDCmd->pCmdTRB->uCtrl));

                USB_XHCD_WARN("usbXhcdNotifyCommandReady - %s TRB fail (%s)!\n",
                     usbXhcdTrbName((UINT8)USB_XHCI_TRB_CTRL_TYPE_GET(uCtrl)),
                     usbXhcdCmdCompCodeName(pHCDCmd->uCompCode), 3, 4, 5, 6);

                uSts = ERROR;
                }
            }

        (void) usbXhcdCmdRelease(pHCDData, pHCDDevice, pHCDCmd);

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDData->hcdMutex);

        USB_XHCD_VDBG("usbXhcdNotifyCommandReady - %s!\n",
            (uSts == OK) ? "done" : "timeout", 2, 3, 4, 5, 6);
        }

    return uSts;
    }

/*******************************************************************************
*
* usbXhcdNotifyEndpointReady - notify the HC that the endpoint is ready
*
* This routine is to notify the HC that the endpoint is ready to be processed by
* writting Device Context Doorbell register with proper DB target and Stream ID.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbXhcdNotifyEndpointReady
    (
    pUSB_XHCD_DATA  pHCDData,
    UINT32          uSlotId,
    UINT32          uEpId,
    UINT32          uStreamId
    )
    {
    /* Don't ring the Door Bell if the endpoint is in any of the states */

    /* Value of the Device Context Doorbell register */

    UINT32 uReg32;

    /* Get the Device Context Doorbell register offset */

    UINT32 uDbOffset = USB_XHCI_DOOR_BELL(uSlotId);

    /* Read the Device Context Doorbell register for the device slot */

    uReg32 = USB_XHCD_READ_DB_REG32(pHCDData, uDbOffset);

    /* Clear the other fields except for RsvdZ field */

    uReg32 &= USB_XHCI_DB_RsvdZ_MASK;

    /* Set the DB Target and Stream ID field */

    uReg32 |= USB_XHCI_DOOR_BELL_DB_TARGET(uEpId) |
              USB_XHCI_DOOR_BELL_DB_STREAM_ID(uStreamId);

    USB_XHCD_VDBG("usbXhcdNotifyEndpointReady - "
        "Ring HC EP DB @%p for Slot ID %d EP ID %d uStreamId %d!\n",
        (pHCDData->uDbRegBase + uDbOffset), uSlotId, uEpId, uStreamId, 5, 6);

    /* Write the Device Context Doorbell register */

    USB_XHCD_WRITE_DB_REG32(pHCDData, uDbOffset, uReg32);

    /* Read it back to flush PCI posted writes */

    (void) USB_XHCD_READ_DB_REG32(pHCDData, uDbOffset);
    }

/*******************************************************************************
*
* usbXhcdNotifyEndpointReadyAll - notify HC all streams of endpoint are ready
*
* This routine is to notify the HC that all streams of the endpoint are ready
* to be processed.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

VOID usbXhcdNotifyEndpointReadyAll
    (
    pUSB_XHCD_DATA  pHCDData,
    pUSB_XHCD_PIPE  pHCDPipe
    )
    {
    /* Stream ID */

    UINT32 uStreamId;

    /* Endpoint Streams info */

    pUSB_XHCD_STREAM_INFO   pStreamInfo;

    /* If the endpoint doesn't have streams, just notify the endpoint ready */

    if (!(pHCDPipe->uFeatures & USB_XHCD_EP_HAS_STREAMS))
        {
        /* Check if the REQ list is not empty */

        if (lstCount(&pHCDPipe->pEpRing->reqList) > 0)
            {
            /* Notify endpoint ready */

            usbXhcdNotifyEndpointReady(pHCDData,
                                       pHCDPipe->uSlotId,
                                       pHCDPipe->uEpId,
                                       0);
            }

        return;
        }

    /* Get the stream info pointer */

    pStreamInfo = &pHCDPipe->streamInfo;

    /* If the endpoint has streams, notify for each stream ready */

    for (uStreamId = 1; uStreamId < pStreamInfo->uNumStreams; uStreamId ++)
        {
        /* Check if the stream TD list is not empty */

        if (lstCount(&pStreamInfo->ppStreamRings[uStreamId]->reqList) > 0)
            {
            /* Notify endpoint stream ready */

            usbXhcdNotifyEndpointReady(pHCDData,
                                       pHCDPipe->uSlotId,
                                       pHCDPipe->uEpId,
                                       uStreamId);
            }
        }
    }

/*******************************************************************************
*
* usbXhcdLocateSegmentOfTrb - locate the containing segment of specified TRB
*
* This routine is to locate the containing segment of specified TRB.
*
* RETURNS: Pointer to the containing segment if found, NULL if not.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_XHCD_SEGMENT usbXhcdLocateSegmentOfTrb
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_SEGMENT   pStartSeg,
    pUSB_XHCI_TRB       pTRB,
    UINT32 *            puCycle
    )
    {
    /* TRB Control */

    UINT32 uCtrl;

    /* Current segment under chceck */

    pUSB_XHCD_SEGMENT   pCurSeg = pStartSeg;

    /* Last TRB of segment */

    pUSB_XHCI_TRB       pLastTRB = &pCurSeg->pTRBs[USB_XHCD_SEG_MAX_TRBS - 1];

    /* Loop until the TRB is found in the segment TRBs address range */

    while ((pTRB < pCurSeg->pTRBs) || (pTRB > pLastTRB))
        {
        /* Get the TRB Control in CPU endian */

        uCtrl = USB_XHCD_SWAP_DESC_DATA32(pHCDData, pLastTRB->uCtrl);

        /*
         * If the last TRB of the segment is a LINK TRB and it has the
         * Cycle bit set, then toggle the Cycle bit.
         */

        if ((USB_XHCI_TRB_CTRL_TYPE_GET(uCtrl) == USB_XHCI_TRB_LINK) &&
            ((uCtrl & USB_XHCI_TRB_CTRL_TC_MASK)))
            {
            /* Toggle the Cycle bit state */

            *puCycle = (*puCycle) ? 0 : 1;
            }

        /* Go to the next segment */

        pCurSeg = pCurSeg->pNextSeg;

        /* If reachs the start segment, then we have scanned all segments */

        if (pCurSeg == pStartSeg)
            {
            USB_XHCD_ERR("usbXhcdLocateSegmentOfTrb - no containing segment!\n",
                1, 2, 3, 4, 5, 6);

            return NULL;
            }
        }

    /* Return the segment found */

    return pCurSeg;
    }

/*******************************************************************************
*
* usbXhcdPortPower - power on or off a Root Hub port
*
* This routine is to power on or off a Root Hub port.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

VOID usbXhcdPortPower
    (
    pUSB_XHCD_DATA  pHCDData,
    UINT32          uPortIndex,
    BOOL            bOn
    )
    {
    UINT32          uPortSc;
    UINT32          uPortScRetries = 0;

    /* Read the PORTSC register */

    uPortSc = USB_XHCD_READ_OP_REG32(pHCDData,
                    USB_XHCI_PORTSC(uPortIndex));

    uPortSc = USB_XHCI_PORTSC_KEEP(uPortSc);

    if (bOn)
        uPortSc |= USB_XHCI_PORTSC_PP;
    else
        uPortSc &= ~USB_XHCI_PORTSC_PP;

    /* Write the PORTSC with updated value */

    USB_XHCD_WRITE_OP_REG32(pHCDData,
                    USB_XHCI_PORTSC(uPortIndex),
                    uPortSc);

    do
        {
        /* Read the PORTSC register */

        uPortSc = USB_XHCD_READ_OP_REG32(pHCDData,
                        USB_XHCI_PORTSC(uPortIndex));

        /*
         * After modifying PP, software shall read PP and confirm that
         * it is reached its target state before modifying it again.
         */

        if (((bOn == TRUE) && (uPortSc & USB_XHCI_PORTSC_PP)) ||
            ((bOn == FALSE) && !(uPortSc & USB_XHCI_PORTSC_PP)))
            break;

        OS_DELAY_MS(1);

        uPortScRetries++;

        }while (uPortScRetries < USB_XHCD_MAX_PORTSC_RETRIES);

    }

/*******************************************************************************
*
* usbXhcdPortReset - reset a Root Hub port
*
* This routine is to reset a Root Hub port.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

VOID usbXhcdPortReset
    (
    pUSB_XHCD_DATA  pHCDData,
    UINT32          uPortIndex,
    BOOL            bWarmReset
    )
    {
    UINT32          uPortSc;
    UINT32          uPortScRetries = 0;

    /* Read the PORTSC register */

    uPortSc = USB_XHCD_READ_OP_REG32(pHCDData,
                    USB_XHCI_PORTSC(uPortIndex));

    uPortSc = USB_XHCI_PORTSC_KEEP(uPortSc);

    if (bWarmReset == TRUE)
        uPortSc |= USB_XHCI_PORTSC_WPR;
    else
        uPortSc |= USB_XHCI_PORTSC_PR;

    /* Write the PORTSC with updated value */

    USB_XHCD_WRITE_OP_REG32(pHCDData,
                    USB_XHCI_PORTSC(uPortIndex),
                    uPortSc);

    do
        {
        /* Read the PORTSC register */

        uPortSc = USB_XHCD_READ_OP_REG32(pHCDData,
                        USB_XHCI_PORTSC(uPortIndex));

        /*
         * When software writes a '1' to WPR bit, the Warm Reset
         * sequence as defined in the USB3 Specification is initiated
         * and the PR flag is set to '1'. Once initiated, the PR,
         * PRC, and WRC flags shall reflect the progress of the Warm
         * Reset sequence. The WPR flag shall always return '0' when
         * read.
         *
         * PR remains set until reset signaling is completed by the
         * root hub.
         */

        if (!(uPortSc & USB_XHCI_PORTSC_PR))
            break;

        OS_DELAY_MS(1);

        uPortScRetries++;

        } while (uPortScRetries < USB_XHCD_MAX_PORTSC_RETRIES);
    }

