/* usbXhcdUtil.h - USB XHCI Driver Utility Definitions */

/*
 * Copyright (c) 2011, 2012 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01f,17oct12,w_x  Fix compiler warnings (WIND00370525)
01e,17oct12,w_x  Support for Warm Reset (WIND00374209)
01d,16oct12,j_x  Add device speed parameter to usbXhcdDeviceGet() and
                 usbXhcdHubDeviceUpdate() (WIND00381845)
01c,12sep12,j_x  Add some RequestInfo functions definition (WIND00375618)
01b,04sep12,w_x  Address review comments
01a,09may11,w_x  written
*/

#ifndef __INCusbXhcdUtilh
#define __INCusbXhcdUtilh

#ifdef __cplusplus
extern "C" {
#endif

#include "usbXhcdInterfaces.h"

char * usbXhcdTrbName
    (
    UINT8 uTrbType
    );

char * usbXhcdCmdCompCodeName
    (
    UINT8 uTrbCompCode
    );

UINT32 usbXhcdNextExtCapReg
    (
    pUSB_XHCD_DATA  pHCDData,
    UINT32          uOffset 
    );

STATUS usbXhcdLocateExtCap
    (
    pUSB_XHCD_DATA  pHCDData,
    UINT32          uOffset,
    UINT32          uExtCapId,
    UINT32 *        puActOffset      
    );

STATUS usbXhcdEnableSSPorts
    (
    pUSB_XHCD_DATA  pHCDData
    );

STATUS usbXhcdHandleOffPreOS
    (
    pUSB_XHCD_DATA  pHCDData
    );

STATUS usbXhcdPciMemoryWriteInvalidateSetup 
    (
    pUSB_XHCD_DATA  pHCDData
    );

STATUS usbXhcdHardwareParamInit
    (
    pUSB_XHCD_DATA  pHCDData
    );

STATUS usbXhcdCmdRingDeqSet
    (
    pUSB_XHCD_DATA  pHCDData
    );

STATUS usbXhcdHostCtlrWait
    (
    pUSB_XHCD_DATA  pHCDData
    );

STATUS usbXhcdHostCtlrHalt
    (
    pUSB_XHCD_DATA  pHCDData
    );

STATUS usbXhcdHostCtlrReset
    (
    pUSB_XHCD_DATA  pHCDData
    );

STATUS usbXhcdHostCtlrStart
    (
    pUSB_XHCD_DATA  pHCDData
    );

STATUS usbXhcdHostCtlrStop
    (
    pUSB_XHCD_DATA  pHCDData
    );

STATUS usbXhcdHardwareStart
    (
    pUSB_XHCD_DATA  pHCDData
    );

STATUS usbXhcdHardwareStop
    (
    pUSB_XHCD_DATA  pHCDData
    );


STATUS usbXhcdInterruptSetup 
    (
    pUSB_XHCD_DATA  pHCDData
    );

STATUS usbXhcdDmaBuffUnInit
    (
    pUSB_XHCD_DATA  pHCDData
    );

STATUS usbXhcdDmaBuffInit
    (
    pUSB_XHCD_DATA  pHCDData
    );


VOID usbXhcdCmdComplete
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_CMD       pHCDCmd
    );

STATUS usbXhcdRegSave
    (
    pUSB_XHCD_DATA      pHCDData
    );

STATUS usbXhcdRegRestore
    (
    pUSB_XHCD_DATA      pHCDData
    );

STATUS usbXhcdInternalStateSave
    (
    pUSB_XHCD_DATA      pHCDData
    );

STATUS usbXhcdInternalStateRestore
    (
    pUSB_XHCD_DATA      pHCDData
    );

STATUS usbXhcdDataUnInit
    (
    pUSB_XHCD_DATA  pHCDData
    );

STATUS usbXhcdDataInit
    (
    pUSB_XHCD_DATA  pHCDData
    );

STATUS usbXhcdSegmemtDestroy
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_SEGMENT   pSeg
    );

pUSB_XHCD_SEGMENT usbXhcdSegmentCreate
    (
    pUSB_XHCD_DATA  pHCDData
    );

STATUS usbXhcdLinkSegmemts
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_SEGMENT   pPrevSeg,
    pUSB_XHCD_SEGMENT   pNextSeg,
    BOOL                bHardLink
    );

STATUS usbXhcdRingReset
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_RING      pRing
    );

STATUS usbXhcdRingDestroy
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_RING      pRing
    );

pUSB_XHCD_RING usbXhcdRingCreate
    (
    pUSB_XHCD_DATA  pHCDData,
    UINT32          uRingType,
    UINT32          uNumSegs,
    BOOL            bHardLink
    );

STATUS usbXhcdDeviceDestroy
    (
    pUSB_XHCD_DATA          pHCDData,
    pUSB_XHCD_DEVICE        pDevice
    );

pUSB_XHCD_DEVICE usbXhcdDeviceCreate
    (
    pUSB_XHCD_DATA  pHCDData
    );

VOID usbXhcdDeleteRequestInfo
    (
    pUSB_XHCD_DATA          pHCDData,
    pUSB_XHCD_PIPE          pHCDPipe,
    pUSB_XHCD_REQUEST_INFO  pRequest
    );

pUSB_XHCD_REQUEST_INFO usbXhcdCreateRequestInfo
    (
    pUSB_XHCD_DATA  pHCDData,
    pUSB_XHCD_PIPE  pHCDPipe
    );

pUSB_XHCD_REQUEST_INFO usbXhcdReserveRequestInfo
    (
    pUSB_XHCD_DATA  pHCDData,
    pUSB_XHCD_PIPE  pHCDPipe,
    pUSBHST_URB     pURB
    );

VOID usbXhcdReleaseRequestInfo
    (
    pUSB_XHCD_DATA          pHCDData,
    pUSB_XHCD_PIPE          pHCDPipe,
    pUSB_XHCD_REQUEST_INFO  pRequest
    );

BOOLEAN usbXhcdIsRequestInfoUsed
    (
    pUSB_XHCD_DATA          pHCDData,
    pUSB_XHCD_PIPE          pHCDPipe,
    pUSB_XHCD_REQUEST_INFO  pRequest
    );

STATUS usbXhcdPipeDestroy
    (
    pUSB_XHCD_DATA  pHCDData,
    pUSB_XHCD_PIPE  pHCDPipe
    );

pUSB_XHCD_PIPE usbXhcdPipeCreate
    (
    pUSB_XHCD_DATA                  pHCDData,
    pUSB_XHCD_DEVICE                pHCDDevice,     
    pUSBHST_ENDPOINT_DESCRIPTOR     pEpDesc
    );

STATUS usbXhcdUnSetupPipe
    (
    pUSB_XHCD_DATA pHCDData,
    pUSB_XHCD_PIPE pHCDPipe
    );

STATUS usbXhcdSetupPipe
    (
    pUSB_XHCD_DATA              pHCDData,
    pUSB_XHCD_PIPE              pHCDPipe,
    pUSB_TRANSFER_SETUP_INFO    pSetupInfo
    );

STATUS usbXhcdSetupControlPipe
    (
    pUSB_XHCD_DATA pHCDData,
    pUSB_XHCD_PIPE pHCDPipe
    );

STATUS usbXhcdDefaultPipeDestroy
    (
    pUSB_XHCD_DATA  pHCDData
    );

STATUS usbXhcdDefaultPipeCreate
    (
    pUSB_XHCD_DATA  pHCDData
    );

BOOL usbXhcdPipeUpdate
    (
    pUSB_XHCD_DATA                  pHCDData,
    pUSB_XHCD_PIPE                  pHCDPipe,     
    pUSBHST_ENDPOINT_DESCRIPTOR     pEpDesc
    );

STATUS usbXhcdPipeRingDestroy
    (
    pUSB_XHCD_DATA  pHCDData,
    pUSB_XHCD_PIPE  pHCDPipe
    );

STATUS usbXhcdPipeRingCreate
    (
    pUSB_XHCD_DATA  pHCDData,
    pUSB_XHCD_PIPE  pHCDPipe
    );

pUSB_XHCD_REQUEST_INFO usbXhcdPipeRequestFind
    (
    pUSB_XHCD_DATA          pHCDData,
    UINT8                   uSlotId,
    UINT8                   uEpId,
    UINT64                  uTrbBusAddr
    );

STATUS usbXhcdDeviceAddressGet
    (
    pUSB_XHCD_DATA  pHCDData,
    UINT32          uSlotId,
    UINT8 *         pDevAddress
    );

UINT8 usbXhcdPipeStateGet
    (
    pUSB_XHCD_DATA  pHCDData,
    pUSB_XHCD_PIPE  pHCDPipe
    );

STATUS usbXhcdEpCtxCommit
    (
    pUSB_XHCD_DATA                  pHCDData,
    pUSB_XHCD_DEVICE                pHCDDevice,
    pUSB_XHCD_PIPE                  pHCDPipe
    );

STATUS usbXhcdInterfaceScan
    (
    pUSB_XHCD_DATA                  pHCDData,
    pUSB_XHCD_DEVICE                pHCDDevice,
    pUSBHST_INTERFACE_DESCRIPTOR    pIfDesc
    );

pUSB_XHCD_DEVICE usbXhcdDeviceGet
    (
    pUSB_XHCD_DATA  pHCDData,
    UINT8           uDevAddr,
    UINT8           uSpeed
    );

STATUS usbXhcdHubDeviceUpdate
    (
    pUSB_XHCD_DATA  pHCDData,
    UINT32          uHubAddr,
    UINT32          uNumPorts,
    UINT32          uMTT,
    UINT32          uTTT,
    UINT8           uSpeed
    );

STATUS usbXhcdDeviceSlotInit
    (
    pUSB_XHCD_DATA  pHCDData,
    UINT32          uRhPort,
    UINT32          uRouteStr,
    UINT32          uTTHubAddr,
    UINT32          uTTHubPort,
    UINT8           uPortSpeed,
    UINT8 *         pDevAddr
    );

STATUS usbXhcdDeviceSlotStop
    (
    pUSB_XHCD_DATA              pHCDData,
    pUSB_XHCD_DEVICE            pHCDDevice
    );

STATUS usbXhcdCtxWrapperDestroy
    (
    pUSB_XHCD_DATA          pHCDData,
    pUSB_XHCD_CTX_WRAPPER   pCtxWrapper
    );

pUSB_XHCD_CTX_WRAPPER usbXhcdCtxWrapperCreate
    (
    pUSB_XHCD_DATA  pHCDData,
    UINT8           uCtxType
    );

UINT64 usbXhcdTrbBusAddr
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_SEGMENT   pSeg,
    pUSB_XHCI_TRB       pTRB
    );

BOOL usbXhcdIsLastTrbOfRing
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_RING      pRing,
    pUSB_XHCD_SEGMENT   pSeg,
    pUSB_XHCI_TRB       pTRB
    );

BOOL usbXhcdIsLastTrbOfSegment
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_RING      pRing,
    pUSB_XHCD_SEGMENT   pSeg,
    pUSB_XHCI_TRB       pTRB
    );

VOID usbXhcdDeqPtrAdvance
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_RING      pRing
    );

VOID usbXhcdEnqPtrAdvance
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_RING      pRing
    );

BOOL usbXhcdHasRoomOnRing
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_RING      pRing,
    size_t              uNumTRBs
    );

pUSB_XHCI_TRB usbXhcdQueueTRB
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_RING      pRing,
    UINT32              uDataLo, 
    UINT32              uDataHi, 
    UINT32              uInfo,
    UINT32              uCtrl
    );

STATUS usbXhcdQueueCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_DEVICE    pHCDDevice,
    _Vx_ticks_t         waitTime, 
    UINT32              uDataLo, 
    UINT32              uDataHi, 
    UINT32              uInfo,
    UINT32              uCtrl
    );

STATUS usbXhcdQueueVendorCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_DEVICE    pHCDDevice,
    _Vx_ticks_t         waitTime, 
    UINT32              uDataLo, 
    UINT32              uDataHi, 
    UINT32              uInfo,
    UINT32              uCtrl
    );

STATUS usbXhcdQueueNoOpCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    UINT32              uIntr
    );

STATUS usbXhcdQueueEnableSlotCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    _Vx_ticks_t         waitTime
    );

STATUS usbXhcdQueueDisableSlotCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    UINT32              uSlotId,
    _Vx_ticks_t         waitTime
    );

STATUS usbXhcdQueueAddressDeviceCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    UINT64              uInputCtxBusAddr,
    UINT32              uSlotId,
    _Vx_ticks_t         waitTime
    );

STATUS usbXhcdQueueConfigureEndpointCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    bus_addr_t          uInputCtxBusAddr,
    UINT32              uSlotId,
    UINT32              uDC,
    _Vx_ticks_t         waitTime
    );

STATUS usbXhcdQueueEvaluateContextCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    bus_addr_t          uInputCtxBusAddr,
    UINT32              uSlotId,
    _Vx_ticks_t         waitTime
    );

STATUS usbXhcdQueueResetEndpointCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    UINT32              uSlotId,
    UINT32              uEpId,
    UINT32              uTSP,
    _Vx_ticks_t         waitTime
    );

STATUS usbXhcdQueueStopEndpointCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    UINT32              uSlotId,
    UINT32              uEpId,
    UINT8               uSuspend,
    _Vx_ticks_t         waitTime
    );

STATUS usbXhcdQueueSetTRDeqPtrCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    bus_addr_t          uDeqPtrBusAddr,
    UINT32              uSlotId,
    UINT32              uEpId,
    UINT32              uStreamId,
    UINT32              uDCS,
    UINT32              uSCT,
    _Vx_ticks_t         waitTime
    );

STATUS usbXhcdQueueResetDeviceCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    UINT32              uSlotId,
    _Vx_ticks_t         waitTime
    );

STATUS usbXhcdQueueForceEventCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    UINT64              uEvtTrbBusAddr,
    UINT32              uVfId,
    UINT32              uIntrTarget,
    _Vx_ticks_t         waitTime
    );

STATUS usbXhcdQueueNegotiateBandwidthCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    UINT32              uSlotId,
    _Vx_ticks_t         waitTime
    );

STATUS usbXhcdQueueSetLTVCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    UINT32              uBELT,
    _Vx_ticks_t         waitTime
    );

STATUS usbXhcdQueueGetPortBandwidthCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    bus_addr_t          uPortBwCtxBusAddr,
    UINT32              uDevSpeed,
    UINT32              uHubSlotId,
    _Vx_ticks_t         waitTime
    );

STATUS usbXhcdQueueForceHeaderCmd
    (
    pUSB_XHCD_DATA      pHCDData,
    UINT8               uRootHubPort,
    UINT8               uHdrType,
    UINT32              uHdrInfoLow,
    UINT32              uHdrInfoMid,
    UINT32              uHdrInfoHigh
    );

STATUS usbXhcdNotifyEventProcessed
    (
    pUSB_XHCD_DATA      pHCDData,
    UINT32              uIntrIndex
    );

STATUS usbXhcdNotifyCommandReady
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_DEVICE    pHCDDevice,
    pUSB_XHCD_CMD       pHCDCmd
    );

VOID usbXhcdNotifyEndpointReady
    (
    pUSB_XHCD_DATA  pHCDData,
    UINT32          uSlotId,
    UINT32          uEpId,
    UINT32          uStreamId
    );

VOID usbXhcdNotifyEndpointReadyAll
    (
    pUSB_XHCD_DATA  pHCDData,
    pUSB_XHCD_PIPE  pHCDPipe
    );

pUSB_XHCD_SEGMENT usbXhcdLocateSegmentOfTrb
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_SEGMENT   pStartSeg,
    pUSB_XHCI_TRB       pTRB,
    UINT32 *            puCycle
    );

void vxbUsbXhcdWrite64
    (
    void *      handle,
    UINT64 *    addr,
    UINT64      val
    );

UINT64 vxbUsbXhcdRead64
    (
    void * handle,
    UINT64 * addr
    );

UINT64 usbXhcdGetTrbData64
    (
    pUSB_XHCD_DATA  pHCDData,
    pUSB_XHCI_TRB   pTRB
    );

VOID usbXhcdSetTrbData64
    (
    pUSB_XHCD_DATA  pHCDData,
    pUSB_XHCI_TRB   pTRB,
    UINT64          uData64
    );

UINT8 usbXhcdEpIntervalGet
    (
    UINT8 uDevSpeed,
    UINT8 uEpDir,
    UINT8 uEpXferType,
    UINT8 bInterval
    );

VOID usbXhcdPortPower
    (
    pUSB_XHCD_DATA  pHCDData,
    UINT32          uPortIndex,
    BOOL            bOn
    );

VOID usbXhcdPortReset
    (
    pUSB_XHCD_DATA  pHCDData,
    UINT32          uPortIndex,
    BOOL            bWarmReset
    );

#ifdef __cplusplus
    }
#endif
#endif  /* __INCusbXhcdUtilh */

