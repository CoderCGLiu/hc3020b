/* usbTgtFunc.c - USB Target Function Class Driver Common Module */

/*
 * Copyright (c) 2011-2014 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
Modification history
--------------------
01f,02Sep14,bbz  clear the warnings of no check of the return value (VXW6-83095)
01e,06may13,s_z  Remove compiler warning (WIND00356717)
01d,23oct12,s_z  Remove un-used logs (WIND00382685)
01c,18sep12,s_z  Pass CV2.0 testing (WIND00375402)
01b,04Sep12,s_z  clean up 
01a,04Oct11,s_z  written
*/

/*
DESCRIPTION

This mode provides common interfaces for the function drivers.

INCLUDE FILES:  usb/usbTgt.h, usbTgtFunc.h, usbTgtControlRequest.h

*/

/* includes */

#include <usb/usbTgt.h>
#include <usbTgtFunc.h>
#include "usbTgtControlRequest.h"


/*******************************************************************************
*
* usbTgtFuncInterfaceDescFind - find interface descriptor pointer
*
* This routine is used to find the interface descriptor pointer with the
* right interface number and altsetting value.
* 
* NOTE: This routine mainly used after the function driver has been attached
* to the right TCD. 
*
* From the USB 2.0 Spec:
* If there are no class specific or vendor specific descriptors following
* this interface descriptor, all the endpoint descriptors will immediately 
* follow the interface descriptor. The caller can get all the information of 
* the endpoints belong to the interface.
* There may be other class specific or vendor specific descriptors following 
* to the interface descripor if the function driver defined.
* 
* RETURNS: NULL or the pointer of pUSB_INTERFACE_DESCR 
*
* ERRNO: N/A
*/

pUSB_INTERFACE_DESCR usbTgtFuncInterfaceDescFind
    (    
    USB_TARG_CHANNEL     targChannel,    /* targChannel */
    UINT8                uInterfaceNum,  /* Interface number        */
    UINT8                uAltSetting     /* Alternate setting index */
    )
    {
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;
    
    pUSBTGT_TCD           pTcd = NULL;
    pUSB_DESCR_HDR        pDescHdr = NULL; /* Descriptor header */
    pUSB_INTERFACE_DESCR  pIfDesc = NULL;  /* Interface desc   */
    UINT8 *               pDescBuf = NULL;
    UINT16                uDescBufLen;

    /* Validate parameters */

    pFuncDriver = usbTgtTargChannelToFuncDriver(targChannel);

    if ((NULL == pFuncDriver)||
        (NULL == (pTcd = pFuncDriver->pTcd)))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return NULL;
        }

    /* Find the right descriptor buffer according to the speed */

    switch (pTcd->uSpeed)
        {
        case USB_SPEED_HIGH:
            pDescBuf = pFuncDriver->pHsDescBuf;
            uDescBufLen = pFuncDriver->uHsDescBufLen;
            break;
        case USB_SPEED_FULL:
            /* Fall through */
        case USB_SPEED_LOW:            
            pDescBuf = pFuncDriver->pFsDescBuf;
            uDescBufLen = pFuncDriver->uFsDescBufLen;
            break;
        case USB_SPEED_SUPER:
            /* Fall through */
        default:
            pDescBuf = pFuncDriver->pSsDescBuf;
            uDescBufLen = pFuncDriver->uSsDescBufLen;
            break;
        }
        
    /* Get the descriptor buffer pointer */

    pDescHdr = (pUSB_DESCR_HDR)(pDescBuf);

    while ((0 < uDescBufLen) && 
           (NULL != pDescHdr))
        {
        if (pDescHdr->descriptorType == USB_DESCR_INTERFACE)
            {
            pIfDesc = (pUSB_INTERFACE_DESCR)pDescHdr;

            /* Check the interface number and alternate setting */
            
            if ((uInterfaceNum == pIfDesc->interfaceNumber) &&
                (uAltSetting == pIfDesc->alternateSetting))
                {
                USB_TGT_DBG("Find the interface desc %p with uInterfaceNum %p "
                            "uAltSetting %p\n",
                            pIfDesc, uInterfaceNum, uAltSetting, 4, 5, 6);
                
                return pIfDesc;
                }
            }
        
        uDescBufLen = (UINT16) (uDescBufLen - pDescHdr->length);
        pDescHdr = (pUSB_DESCR_HDR)((UINT8 *)pDescHdr + pDescHdr->length);
        }

    USB_TGT_ERR("usbTgtFindInterface(): No matching interface found, "
                "returning NULL\n", 1, 2, 3, 4, 5, 6);

    return NULL;
    }

/*******************************************************************************
*
* usbTgtFuncEndpointDescFind - find endpoint descriptor pointer
*
* This routine is used to find the endpoint descriptor pointer with the given
* attributes of the right interface.
* 
* NOTE: This routine mainly used after the function driver has been attached
* to the right TCD. 
*
* From the USB 2.0 Spec:
* If there are no class specific or vendor specific descriptors following
* this interface descriptor, all the endpoint descriptors will immediately 
* follow the interface descriptor. The caller can get all the information of 
* the endpoints belong to the interface.
* There may be other class specific or vendor specific descriptors following 
* to the interface descripor if the function driver defined.
* 
* RETURNS: NULL or the pointer of pUSB_ENDPOINT_DESCR 
*
* ERRNO: N/A
*/

pUSB_ENDPOINT_DESCR usbTgtFuncEndpointDescFind
    (    
    USB_TARG_CHANNEL targChannel,    /* targChannel */
    UINT8            uInterfaceNum,  /* Interface number */
    UINT8            uAltSetting,    /* Alternate setting index */
    UINT8            uEpType,        /* Endpoint type */
    UINT8            uEpDir,         /* Endpoint dir */
    UINT8            uSyncType,      /* Synchronisation type */
    UINT8            uUsageType,     /* Usage Type for Iso/Int EP */
    UINT8            uNth            /* The Index of such endpoints, 1 based */
    )
    {
    pUSB_DESCR_HDR        pDescHdr = NULL; /* Descriptor header */
    pUSB_INTERFACE_DESCR  pIfDesc = NULL;  /* Interface desc   */
    pUSB_ENDPOINT_DESCR   pEpDesc = NULL;
    UINT8                 uAttributes = 0;
    UINT8                 uIndex = 0;
    UINT8                 uFindCount = 0;

    /* Find the right interface */
    
    pIfDesc = usbTgtFuncInterfaceDescFind(targChannel,
                                          uInterfaceNum,
                                          uAltSetting);

    /* The index is not NULL, and have enough endpoints */

    if ((NULL == pIfDesc) ||
        (pIfDesc->numEndpoints < uNth))
        {
        USB_TGT_ERR("No way to find the %d one\n",
                    uNth, 2, 3, 4, 5 ,6);
        
        return NULL;
        }

    pDescHdr = (pUSB_DESCR_HDR)pIfDesc;
    
    /* Find the right endpoint descriptor */
    
    while (uIndex < pIfDesc->numEndpoints)
        {        
        pDescHdr = (pUSB_DESCR_HDR)
                    ((UINT8 *)pDescHdr + pDescHdr->length);

        /* Check if the descriptor is endpoint */

        while (USB_DESCR_ENDPOINT != pDescHdr->descriptorType)
            {
            /* Move the pointer to read the next descriptor */

            pDescHdr = (pUSB_DESCR_HDR)
                    ((UINT8 *)pDescHdr + pDescHdr->length);
            }

        /* Find one endpoint descriptor */

        pEpDesc = (pUSB_ENDPOINT_DESCR)pDescHdr;
        
        uIndex ++;

        /* Check if this endpoint attributes is what we needed */

        if ((USB_ATTR_ISOCH == uEpType) ||
            (USB_ATTR_INTERRUPT == uEpType))
            {
            /* ISO/Interrupt */

            uAttributes = (UINT8) ((uEpType & USB_ATTR_EPTYPE_MASK) |
                                   ((uSyncType & USB_ATTR_EP_SYNCH_TYPE_MASK) <<
                                     USB_ATTR_EP_SYNCH_TYPE_OFFSET) |
                                   ((uUsageType & USB_ATTR_EP_USAGE_TYPE_MASK) <<
                                     USB_ATTR_EP_USAGE_TYPE_OFFSET));
            
            }
        else
            {
            /* Bulk/Control endpoint */
            
            uAttributes = (UINT8) ((uEpType & USB_ATTR_EPTYPE_MASK));
            
            }
        
        if ((uAttributes == pEpDesc->attributes) &&
            (uEpDir == (pEpDesc->endpointAddress & USB_ENDPOINT_IN)))
            {
            uFindCount ++;
            }

        /* Is is the right one */
        
        if (uFindCount == uNth)
            {
            return pEpDesc;
            }
        }

    USB_TGT_ERR("usbTgtFuncEndpointDescFind(): No found the endpoint desc "
                "returning NULL\n", 1, 2, 3, 4, 5, 6);

    return NULL;
    }


/*******************************************************************************
*
* usbTgtFuncAltsetPipesDelete - delete all the pipes of the given interface
*
* This routine deletes all the pipes of the given interface and the altsetting
* number from the pipe list. 
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbTgtFuncAltsetPipesDelete
    (
    USB_TARG_CHANNEL    targChannel,
    UINT8               uInterface,
    UINT8               uAltSetting
    )
    {
    pUSBTGT_FUNC_DRIVER  pFuncDriver = NULL; 
    pUSBTGT_PIPE         pUsbTgtPipe = NULL;
    NODE *               pNode = NULL;
    STATUS               status;
    
    /* Validate parameters */

    pFuncDriver = usbTgtTargChannelToFuncDriver(targChannel);

    if (NULL == pFuncDriver)
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    "pFuncDriver", 2, 3, 4, 5, 6);

        return ERROR;
        }

    pNode = lstFirst (&pFuncDriver->pipeList);

    while (NULL != pNode)
        {
        pUsbTgtPipe = (pUSBTGT_PIPE)pNode; 
        pNode = lstNext (pNode);
        
        if ((pUsbTgtPipe->uInterface == uInterface) &&
            (pUsbTgtPipe->uAltSetting == uAltSetting))
            {
            status = usbTgtDeletePipe ((USB_TARG_PIPE)pUsbTgtPipe);
            
            if (ERROR == status)
                {
                USB_TGT_WARN("Delete the pipe %p of uInterface %x "
                             "uAltSetting %x fail\n",
                             pUsbTgtPipe, uInterface, uAltSetting, 4, 5, 6);
                }
            }
        }
    
    USB_TGT_DBG("All the pipes of uInterface %x uAltSetting %x deleted\n",
                 pUsbTgtPipe, uInterface, uAltSetting, 4, 5, 6);
    
    return OK;
    }


/*******************************************************************************
*
* usbTgtFuncInterfacePipesCreate - create all the pipes of the given interface
*
* This routine creates create all the pipes of the given interface. It will
* search all the endpiont descriptors belong to the interface, create the 
* pipe using the routine usbTgtCreatePipe(), and added the pipe to the pipe list
* of the function driver.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbTgtFuncAltsetPipesCreate
    (
    USB_TARG_CHANNEL    targChannel,
    UINT8               uInterface,
    UINT8               uAltSetting
    )
    {
    pUSBTGT_FUNC_DRIVER  pFuncDriver = NULL;
    pUSB_DESCR_HDR       pDescHdr = NULL;
    pUSB_ENDPOINT_DESCR  pEpDesc = NULL;
    pUSB_INTERFACE_DESCR pIfDesc = NULL;
    USB_TARG_PIPE        usbTargPipeTemp;
    UINT8                uEpIndex = 0;
    STATUS               status = ERROR;
    
    /* Validate parameters */

    pFuncDriver = usbTgtTargChannelToFuncDriver(targChannel);

    if (NULL == pFuncDriver)
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    "pFuncDriver", 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* Get the right interface */
    
    pIfDesc = usbTgtFuncInterfaceDescFind(targChannel,
                                          uInterface,
                                          uAltSetting);

    /* Find all the endpoints belong to this interface */
    
    pDescHdr = (pUSB_DESCR_HDR)pIfDesc;

    while ((NULL != pDescHdr) &&
           (uEpIndex < pIfDesc->numEndpoints))
        {
        USB_TGT_VDBG ("usbTgtFuncAltsetPipesCreate() pDescHdr->descriptorType %x\n",
                      pDescHdr->descriptorType, 2,3,4,5,6);
        
        if (USB_DESCR_ENDPOINT == pDescHdr->descriptorType)
            {                 
            pEpDesc = (pUSB_ENDPOINT_DESCR)pDescHdr;
            
            status = usbTgtCreatePipe(targChannel,
                                      (pUSB_ENDPOINT_DESCR)pDescHdr,
                                      pFuncDriver->uConfigToBind,
                                      uInterface,                
                                      uAltSetting,               
                                      &usbTargPipeTemp);

            if (ERROR == status)
                {
                USB_TGT_ERR("Create pipe for pIfDesc %p ifNum %p "
                            "altSetting %p []error\n",
                            pIfDesc, pIfDesc->interfaceNumber, 
                            pIfDesc->alternateSetting,
                            pEpDesc->endpointAddress,
                            pEpDesc->attributes, 6);
                
                (void)usbTgtFuncAltsetPipesDelete(targChannel,
                                            uInterface,
                                            uAltSetting);

                return ERROR;
                }
            
            USB_TGT_VDBG("Create pipe for pIfDesc %p ifNum %p "
                        "altSetting %p [Add %x Attr %x]\n",
                        pIfDesc, pIfDesc->interfaceNumber, 
                        pIfDesc->alternateSetting,
                        pEpDesc->endpointAddress,
                        pEpDesc->attributes, 6);

            /* Find one Endpoint descriptor */
            
            uEpIndex ++;                    
            }
        
        /* Move on for next step */

        pDescHdr = (pUSB_DESCR_HDR)((UINT8 *)pDescHdr + pDescHdr->length);
        }
    
    USB_TGT_DBG("Create %p pipes of pIfDesc %p \n",
                pIfDesc->numEndpoints, pIfDesc, 3, 4, 5, 6);
    
    return OK;
    }

/*******************************************************************************
*
* usbTgtFuncAllPipesDelete - delete all the pipes created
*
* This routine is used to delete all the pipes created of the fucntion driver,
* which has been added to the pipe list of the function driver.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbTgtFuncAllPipesDelete
    (
    USB_TARG_CHANNEL    targChannel
    )
    {
    pUSBTGT_FUNC_DRIVER  pFuncDriver = NULL;
    USB_TARG_PIPE pipeHandle = NULL;
    NODE *        pNode = NULL;
    STATUS        status;
    
    /* Validate parameters */

    pFuncDriver = usbTgtTargChannelToFuncDriver(targChannel);

    if (NULL == pFuncDriver)
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    "pFuncDriver", 2, 3, 4, 5, 6);

        return ERROR;
        }
    
    pNode = lstFirst (&pFuncDriver->pipeList);

    while (NULL != pNode)
        {
        pipeHandle = (USB_TARG_PIPE)pNode;
        pNode = lstNext (pNode);
        
        status = usbTgtDeletePipe (pipeHandle);
        if (ERROR == status)
            {
            USB_TGT_ERR("Delete pipe %p fail\n",
                        pipeHandle, 2, 3, 4, 5, 6);
            
            return ERROR;
            }
        }
    
    USB_TGT_VDBG("All pipe of function %p has been deleted\n",
                 pFuncDriver, 2, 3, 4, 5, 6);
    
    return OK;
    }


/*******************************************************************************
*
* usbTgtFuncAllPipesCreate - create all the pipes of the function driver
*
* NOTE: This routine will find all the interfaces' altsetting,
* and create pipes of those interfaces. If the interface do not has altsetting
* or there is something wrong with the altsetting get process, this routine 
* will take 0 as the altsetting value.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbTgtFuncAllPipesCreate
    (
    USB_TARG_CHANNEL    targChannel
    )
    {
    pUSBTGT_FUNC_DRIVER  pFuncDriver = NULL;
    UINT8                uIfIndex;
    UINT8                uAltSetting = 0;
    STATUS               status;
    
    /* Validate parameters */

    pFuncDriver = usbTgtTargChannelToFuncDriver(targChannel);

    if (NULL == pFuncDriver)
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    "pFuncDriver", 2, 3, 4, 5, 6);

        return ERROR;
        }

    /* 
     * This routine will find all the endpoints of the function driver 
     * and create pipes.
     * If the function driver has alternate setting interface, use the 
     * right altsetting value, or use 0 as default.
     */

    for (uIfIndex = pFuncDriver->uIfNumMin; 
         uIfIndex < pFuncDriver->uIfNumMax; uIfIndex ++)
        {        
        if ((NULL == pFuncDriver->pFuncCallbackTable) ||
            (NULL == pFuncDriver->pFuncCallbackTable->interfaceGet) ||
             (OK != (pFuncDriver->pFuncCallbackTable->interfaceGet)
                     (pFuncDriver->pCallbackParam,
                      pFuncDriver->targChannel,
                      (UINT16)uIfIndex,
                      &uAltSetting)))
            {
            uAltSetting = 0;
            }
        
        status = usbTgtFuncAltsetPipesCreate(targChannel, 
                                          uIfIndex,
                                          uAltSetting);
        if (ERROR == status)
            {
            USB_TGT_ERR("No find the inerface with uIfIndex %p altsetting %p\n",
                        uIfIndex, 0, 3, 4, 5, 6);
            
            (void)usbTgtFuncAllPipesDelete(targChannel);

            return ERROR;
            }        
        }
         
    USB_TGT_VDBG("All pipes create of the function driver\n",
                 1, 2, 3, 4, 5, 6);
    
    return OK;
    }

/******************************************************************************
*
* usbTgtFuncFindPipeHandleByEpAttr - find the right pipe from the pipe list
*
* This routine is used to find the right pipe from the pipe list of the function
* driver. 
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO:N/A
*
* \NOMANUAL
*/

USB_TARG_PIPE usbTgtFuncFindPipeHandleByEpAttr
    (
    USB_TARG_CHANNEL    targChannel,
    UINT8               uInterface,
    UINT8               uAltSetting,
    UINT8               uEpType,      
    UINT8               uEpDir,       
    UINT8               uNth
    )
    {
    pUSBTGT_PIPE        pUsbTgtPipe = NULL;
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;
    NODE *              pNode = NULL;
    UINT8               uIndex = 0;

    /* Validate parameters */

    pFuncDriver = usbTgtTargChannelToFuncDriver(targChannel);

    if (NULL == pFuncDriver)
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    "pFuncDriver", 2, 3, 4, 5, 6);

        return NULL;
        }
    
    pNode = lstFirst(&pFuncDriver->pipeList);

    while (NULL != pNode)
        {
        pUsbTgtPipe = (pUSBTGT_PIPE)pNode;
        pNode = lstNext(pNode);
        
        if ((uInterface == pUsbTgtPipe->uInterface) &&
            (uAltSetting == pUsbTgtPipe->uAltSetting) &&
            (uEpType == pUsbTgtPipe->uEpType) &&
            (uEpDir == pUsbTgtPipe->uEpDir))
            {
            uIndex ++;

            if (uIndex == uNth)
                {
                USB_TGT_VDBG("usbTgtFuncFindPipeHandleByEpAttr():"
                                 " get the rigth pipe %p \n", 
                                 pUsbTgtPipe, 2,3,4,5,6);      
                
                return (USB_TARG_PIPE)pUsbTgtPipe;
                }
            }                
        }
    
    USB_TGT_WARN("usbTgtFuncFindPipeHandleByEpAttr No Find this one \n",
                      1, 2, 3, 4, 5, 6);
    
    return NULL;
    }


/*******************************************************************************
*
* usbTgtFuncFindPipeHandleByEpIndex - find the pipe handle by the endpoint index
*
* This routine finds the pipe <'pUSBTGT_PIPE'> from the function driver's 
* usbTgtPipe list by the endpoint index.
*
* NOTE: the <uEpIndex> includes the endpoint direction and endpoint address
* information, such as the endpoint address of endpoint descripor.
* 
* RETURNS: USB_TARG_PIPE indicate one valid pipe  or NULL if no found
*
* ERRNO: N/A
*/

USB_TARG_PIPE usbTgtFuncFindPipeHandleByEpIndex
    (
    USB_TARG_CHANNEL    targChannel,
    UINT8               uEpIndex
    )
    {
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;
    pUSBTGT_PIPE        pUsbTgtPipe = NULL;
    NODE *              pNode       = NULL;
    
    /* Validate parameters */
    
    pFuncDriver = usbTgtTargChannelToFuncDriver(targChannel);

    if (NULL == pFuncDriver)
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    "pFuncDriver", 2, 3, 4, 5, 6);

        return NULL;
        }
    
    OS_WAIT_FOR_EVENT(pFuncDriver->funcMutex, USBTGT_WAIT_TIMEOUT);

    pNode = lstFirst(&pFuncDriver->pipeList);
    
    while (NULL != pNode)
        {
        pUsbTgtPipe = (pUSBTGT_PIPE)pNode;
        pNode = lstNext(pNode);
        
        if ((NULL != pUsbTgtPipe) &&
            (uEpIndex == pUsbTgtPipe->uEpAddr))
           {
           (void)OS_RELEASE_EVENT(pFuncDriver->funcMutex);
           
           USB_TGT_DBG("Find the pUsbTgtPipe 0x%X with the endpoint index 0x%X\n",
                       (ULONG)pUsbTgtPipe, uEpIndex, 3, 4, 5 ,6);

           return (USB_TARG_PIPE)pUsbTgtPipe;
           }
        }

    /* Release the event */

    (void)OS_RELEASE_EVENT(pFuncDriver->funcMutex);
    
    USB_TGT_WARN("No find the pUsbTgtPipe who uses the endpoint index 0x%X\n",
                uEpIndex, 2, 3, 4, 5 ,6);

    return NULL;
    }

/*******************************************************************************
*
* usbTgtFuncInfoMemberGet - get the information member value of function driver
*
* This routine is used to get the information member value of function driver.
*
* RETURNS: always OK
*
* ERRNO: N/A
*/


STATUS usbTgtFuncInfoMemberGet
    (
    USB_TARG_CHANNEL    targChannel,
    UINT16              uMemberType,
    void *              pContext
    )
    {
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;
    STATUS              status = OK;
    
    /* Validate parameters */

    pFuncDriver = usbTgtTargChannelToFuncDriver(targChannel);

    if (NULL == pFuncDriver)
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    "pFuncDriver", 2, 3, 4, 5, 6);

        return ERROR;
        }

    switch (uMemberType)
        {
        case USBTGT_FUNC_INFO_MEM_TYPE_uIfNumMin:
            * (UINT8 *)pContext = pFuncDriver->uIfNumMin;
            break;
        case USBTGT_FUNC_INFO_MEM_TYPE_uVendorID:
            if (NULL == pFuncDriver->pTcd)
                {
                status = ERROR;
                }
            else
                {
                * (UINT16 *)pContext = pFuncDriver->pTcd->uVendorID;
                }
            break;
        case USBTGT_FUNC_INFO_MEM_TYPE_pMfgString:
            if (NULL == pFuncDriver->pTcd)
                {
                status = ERROR;
                }
            else
                {
                * (char **)pContext = pFuncDriver->pTcd->pMfgString;
                }
            break;
        default:
            status = ERROR;
            break;        
        }
    
    return status;
    }

/*******************************************************************************
*
* usbTgtFuncInfoMemberSet - set the information member value of function driver
*
* This routine is used to set the information member value of function driver.
*
* RETURNS: always OK
*
* ERRNO: N/A
*/


STATUS usbTgtFuncInfoMemberSet
    (
    USB_TARG_CHANNEL    targChannel,
    UINT16              uMemberType,
    void *              pContext
    )
    {
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;
    STATUS              status = OK;
    
    /* Validate parameters */

    pFuncDriver = usbTgtTargChannelToFuncDriver(targChannel);

    if (NULL == pFuncDriver)
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                    "pFuncDriver", 2, 3, 4, 5, 6);

        return ERROR;
        }

    switch (uMemberType)
        {
        default:
            status = ERROR;
            break;        
        }
    
    return status;
    }

/*******************************************************************************
*
* usbTgtFuncDefaultPipeStall - stall the function driver's control pipe
*
* This routine is used to stall the function driver's control pipe. It will be 
* used when someting wrong on the control pipe when process some class/vendor
* requests.
*
* RETURNS: OK, or ERROR if there is something wrong
*
* ERRNO: N/A
*/

STATUS usbTgtFuncDefaultPipeStall
    (
    USB_TARG_CHANNEL    targChannel
    )
    {
    pUSBTGT_FUNC_DRIVER pFuncDriver = NULL;
    
    /* Validate parameters */

    pFuncDriver = usbTgtTargChannelToFuncDriver(targChannel);

    if ((NULL == pFuncDriver)||
        (NULL == pFuncDriver->pTcd))
        {
        USB_TGT_ERR("Invalid parameter %s is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return ERROR;
        }

    /* Call the interface of usb target stall routine */
    
    return usbTgtControlPipeStall (pFuncDriver->pTcd);
    }

/******************************************************************************
*
* usbTgtErpListDelete - create one pUSB_ERP data stucture with count of buffer
*
* This routine creates one pUSB_ERP data stucture with count of buffer.
* It will create <'bfrCount'> counts of buffer with the buffer size as 
* <'bfrLen'>.
*
* RETURNS: NULL or the pointer of the pUSB_ERP
*
* ERRNO:N/A
*
* \NOMANUAL
*/

STATUS usbTgtErpBufListDelete
    (
    LIST * pList
    )
    {
    pUSBTGT_ERP_BUF pErpBuf = NULL;
    NODE *          pNode = NULL;
    
    /* Validate parameters */
    
    if (NULL == pList)
        {
        USB_TGT_ERR ("usbTgtErpBufListDelete pList is NULL\n",
                      1, 2, 3, 4, 5, 6);
        
        return ossStatus(EINVAL);
        }

    /* Delete all the node on the list */
    
    pNode = lstFirst(pList);

    while (NULL != pNode)
        {
        pErpBuf = (pUSBTGT_ERP_BUF)pNode;

        pNode = lstNext(pNode);

        /* Free the data buffer */
        
        if (NULL != pErpBuf->pBfr)
            {
            OSS_FREE(pErpBuf->pBfr);
            }

        if (ERROR != lstFind(pList, &pErpBuf->node))
            {
            lstDelete(pList, &pErpBuf->node);
            }

        /* Free the data structure */
        
        OSS_FREE(pErpBuf);
        }
    
    USB_TGT_VDBG ("usbTgtErpBufListDelete done\n",
                  1, 2, 3, 4, 5, 6);
                  
    return OK;
    }

/******************************************************************************
*
* usbTgtErpBufListCreate - create one pUSBTGT_ERP data stucture with count of buffer
*
* This routine creates one pUSB_ERP data stucture with count of buffer.
* It will create <'bfrCount'> counts of buffer with the buffer size as 
* <'bfrLen'>.
*
* RETURNS: NULL or the pointer of the pUSB_ERP
*
* ERRNO:N/A
*
* \NOMANUAL
*/

STATUS usbTgtErpBufListCreate
    (
    LIST *     pList,        /* List pointer */
    UINT16     uBfrCount,    /* Count of the Erp buffer to be created */
    UINT32     uHeaderLen,   /* Indicates the header length of the buffer */
    UINT32     uPayloadLen,  /* Indicates the payload length of buffer */
    UINT32     uTailLen      /* Indicates the tail length of the buffer */
    )
    {
    pUSBTGT_ERP_BUF pErpBuf = NULL;
    UINT32          uBufSize = 0;
    UINT16          uBfrIndex = 0;

    /* Validate parameters */
    
    if ((NULL == pList) ||
        (0 == uBfrCount) ||
        ((0 == uPayloadLen) && (0 == uHeaderLen) && (0 == uHeaderLen)))
        {        
        USB_TGT_ERR ("usbTgtErpBufListCreate parameter invalid %s\n",
                     ((NULL == pList) ? "pList is NULL" :
                      (0 == uBfrCount) ? "to create 0 buffers" :
                      "buffer size is 0"), 2, 3, 4, 5, 6);
        
        return ossStatus(EINVAL);
        }

    /* The data buffer size */
    
    uBufSize = uPayloadLen + uHeaderLen + uHeaderLen;
    
     for (uBfrIndex = 0; uBfrIndex < uBfrCount; uBfrIndex ++)
        {
        /* Calloc erp buffer */
        
        pErpBuf = (pUSBTGT_ERP_BUF) OSS_CALLOC(sizeof (USBTGT_ERP_BUF));

        if (NULL == pErpBuf)
            {
            USB_TGT_ERR ("usbTgtErpBufListCreate No enought memory\n",
                         1, 2, 3, 4, 5, 6);
            
            (void )usbTgtErpBufListDelete(pList);

            return ossStatus(ENOMEM);
            }

        /* Calloc the date buffer */
        
        pErpBuf->pBfr = (UINT8 *)OSS_CALLOC(uBufSize);
        
        if (NULL == pErpBuf->pBfr)
            {
            USB_TGT_ERR ("usbTgtErpBufListCreate No enought memory\n",
                         1, 2, 3, 4, 5, 6);
            
            (void )usbTgtErpBufListDelete(pList);

            return ossStatus(ENOMEM);
            }
        
        pErpBuf->uHeaderLen = uHeaderLen;
        pErpBuf->uPayloadLen = uPayloadLen;
        pErpBuf->uTailLen = uTailLen;

        /* Add the erp buffer to the list */
        
        lstAdd(pList, &pErpBuf->node);
        }
     
    USB_TGT_VDBG ("usbTgtErpBufListCreate 0x%X beffers with buf size as 0x%X\n",
                  uBfrCount, uBufSize, 3, 4, 5, 6);
    
    return OK;
    }


/******************************************************************************
*
* usbTgtErpListDelete - create one pUSB_ERP data stucture with count of buffer
*
* This routine creates one pUSB_ERP data stucture with count of buffer.
* It will create <'bfrCount'> counts of buffer with the buffer size as 
* <'bfrLen'>.
*
* RETURNS: NULL or the pointer of the pUSB_ERP
*
* ERRNO:N/A
*
* \NOMANUAL
*/

STATUS usbTgtErpListDelete
    (
    LIST * pList
    )
    {
    pUSBTGT_ERP pUsbTgtErp = NULL;
    NODE *      pNode = NULL;

    /* Validate parameters */

    if (NULL == pList)
        {
        USB_TGT_ERR ("usbTgtErpListDelete pHead is NULL\n",
                      1, 2, 3, 4, 5, 6);
        
        return ossStatus(EINVAL);
        }

    pNode = lstFirst(pList);

    while (NULL != pNode)
        {
        pUsbTgtErp = (pUSBTGT_ERP)pNode;

        pNode = lstNext(pNode);

        /* Delete all the erp buffers used by this erp */
        
        (void)usbTgtErpBufListDelete (&pUsbTgtErp->bufList);

        /* Delete the pUsbTgtErp from the list */
        
        if (ERROR != lstFind(pList, &pUsbTgtErp->node))
            {
            lstDelete(pList, &pUsbTgtErp->node);
            }
            
        OSS_FREE(pUsbTgtErp);
        }

    USB_TGT_VDBG ("usbTgtErpBufListDelete done\n",
                  1, 2, 3, 4, 5, 6);

    return OK;
    }

/******************************************************************************
*
* usbTgtErpListCreate - create one pUSBTGT_ERP data stucture with count of buffer
*
* This routine creates one pUSB_ERP data stucture with count of buffer.
* It will create <'bfrCount'> counts of buffer with the buffer size as 
* <'bfrLen'>.
*
* RETURNS: NULL or the pointer of the pUSB_ERP
*
* ERRNO:N/A
*
* \NOMANUAL
*/

STATUS usbTgtErpListCreate
    (
    LIST *     pList,        /* List head */
    UINT16     uErpCount,    /* Count of the Erp List */
    UINT16     uBufCount,    /* Count of the Erp buffer to be created */
    UINT32     uHeaderLen,   /* Indicates the header length of the buffer */
    UINT32     uPayloadLen,  /* Indicates the payload length of buffer */
    UINT32     uTailLen      /* Indicates the tail length of the buffer */
    )
    {
    pUSBTGT_ERP        pUsbTgtErp = NULL;
    UINT16             uErpLen;
    UINT8              uErpIndex = 0;
    STATUS             status; 

    /* Validate parameters */

    if ((NULL == pList) ||
        (0 == uErpCount))
        {
        USB_TGT_ERR ("usbTgtErpListCreate pList is NULL\n",
                      1, 2, 3, 4, 5, 6);
        
        return ossStatus(EINVAL);
        }

    for (uErpIndex = 0; uErpIndex < uErpCount; uErpIndex ++)
        {
        /* Create uErpCount ERPs data Structure */

        uErpLen = (UINT16) (sizeof (USBTGT_ERP) + sizeof (USB_ERP_BFR_LIST) * 
                                     ((uBufCount >= 1) ? (uBufCount - 1) : 0));

        pUsbTgtErp = (USBTGT_ERP *)OSS_CALLOC(uErpLen);

        if (NULL == pUsbTgtErp)
            {
            (void)usbTgtErpListDelete(pList);

            return ossStatus(ENOMEM);
            }

        pUsbTgtErp->Erp.erpLen = (UINT16) (sizeof (USB_ERP) + sizeof (USB_ERP_BFR_LIST) * 
                                     ((uBufCount >= 1) ? (uBufCount - 1) : 0));
                 
        pUsbTgtErp->Erp.bfrCount = uBufCount;           

       /* 
        * NOTE: 
        * if we use one global list to record the buffers, we can get the 
        * right 
        */
        
        status = usbTgtErpBufListCreate(&pUsbTgtErp->bufList,
                               uBufCount,
                               uHeaderLen,
                               uPayloadLen,
                               uTailLen);

        if (ERROR == status)
            {
            (void)usbTgtErpListDelete(pList);

            return ERROR;
            }

        pUsbTgtErp->uMaxBufSize = uHeaderLen + uPayloadLen + uTailLen;
        
        /* Add the pUsbTgtErp to the list */
        
        lstAdd(pList, & pUsbTgtErp->node);        
        }
    
    return OK;
    }

/******************************************************************************
*
* usbTgtFuncErpSubmit - send the response available notification
*
* This routine sends the response available notification
* to the host on the interrupt IN channel.
*
* RETURNS: OK or ERROR if there is something wrong
*
* ERRNO:N/A
*
* \NOMANUAL
*/

STATUS usbTgtFuncErpSubmit
    (
    USB_TARG_PIPE      pipeHandle,
    pUSB_ERP           pErp,         /* Erp to be submit */
    pVOID              userPtr,      /* Ptr field for use by client */
    ERP_CALLBACK       userCallback, /* Client's completion callback routine */
    UINT16             bfrCount,     /* Indicates count of buffers in BfrList */
    pVOID              pContext,     /* The contex of the current transation */
    USB_ERP_FLAGS      erpFlags,     /* The flags of the current transaction */
    USB_ERP_BFR_LIST * pBfrList      /* Buffer list pointer */  
    )
    {
    STATUS   status;
    UINT16   uIndex; 

    pErp->userPtr = userPtr;
    pErp->userCallback = userCallback;

    /* 
     * The USB TML will always using usbTgtErpCallback() routine 
     * as the targCallback pointer
     */
     
    pErp->targCallback = usbTgtErpCallback;
    pErp->bfrCount = bfrCount;
    pErp->pContext = pContext;
    pErp->erpFlags = erpFlags;
    
    /* NOTE: the caller should make sure enouth bfrList to be used by the ERP */
    
    for (uIndex = 0; uIndex < bfrCount; uIndex ++)
        {
        pErp->bfrList[uIndex].pid = pBfrList[uIndex].pid;
        pErp->bfrList[uIndex].pBfr = pBfrList[uIndex].pBfr;
        pErp->bfrList[uIndex].bfrLen = pBfrList[uIndex].bfrLen;
        }

    /* Submit the Erp */

    USB_TGT_VDBG ("usbTgtRndisCommonErpSubmit submit the Erp \n",
                  1, 2, 3, 4, 5, 6);

    status = usbTgtSubmitErp(pipeHandle, pErp);

    /* Check the status */

    if (status != OK)
        {
        USB_TGT_ERR("Submit the Erp fail with status %d\n",
                    status, 2, 3, 4, 5, 6);

        return ERROR;
        }
    
    USB_TGT_VDBG("usbTgtRndisCommonErpSubmit submit Erp with status %d\n",
                 status, 2, 3, 4, 5, 6);

    return (status);
    }

