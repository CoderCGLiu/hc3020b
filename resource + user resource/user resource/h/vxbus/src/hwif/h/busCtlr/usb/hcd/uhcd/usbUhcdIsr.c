/* usbUhcdIsr.c - USB UHCI HCD interrupt handler */

/*
 * Copyright (c) 2002-2010 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2002-2010 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
Modification history
--------------------
01o,18may10,w_x  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
01n,09mar10,j_x  Changed for USB debug (WIND00184542)
01m,13jan10,ghs  vxWorks 6.9 LP64 adapting
01l,20mar09,w_x  Check for USB_UHCD_MAGIC_ALIVE
01k,15feb09,w_x  Move spinLockIsrUhci[] into UHCD_DATA as spinLockIsr
01j,04jun08,w_x  Corrected interrupt enable/disable according to UHCI spec
01i,19May07,jrp  Changed the register access method initialization
01h,07may07,jrp  vxBus SMP Integration
01g,08oct06,ami  Changes for USB-vxBus changes
01f,12oct05,hch  Fix compiler error with -DDEBUG flag(SPR #113650)
01e,28mar05,pdg  non-PCI changes
01d,15oct04,ami  Refgen changes
01c,28jul03,mat  Endian Related Changes
01b,26jun03,mat  changing the code to WRS standards.
01a,25apr02,ram  written.
*/

/*
DESCRIPTION

This file contains the Interrupt Service Routine for the UHCI driver.

INCLUDE FILES: usb2/usbOsal.h, usb2/usbHst.h, usb2/usbUhci.h,
usb2/usbUhcdSupport.h, usb2/usbUhcdCommon.h, usb2/usbUhcdScheduleQueue.h,
usb2/BusAbstractionLayer.h
*/

/*
INTERNAL
 *******************************************************************************
 * Filename         : usbUhcdIsr.c
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      :  This file contains the Interrupt Service Routine for the
 *                     UHCI driver.
 *
 *
 ******************************************************************************/

/* includes */

#include <usb/usbOsal.h>
#include <usb/usbHst.h>
#include "usbUhci.h"
#include "usbUhcdSupport.h"
#include "usbUhcdCommon.h"
#include "usbUhcdScheduleQueue.h"

/* forward declarations */

void usbUhcdIsr
    (
    ULONG ptrHCDData
    );

/*******************************************************************************
*
* usbUhcdIsr - interrupt service routine for the UHCI Driver
*
* This routine is the interrupt service routine for the UHCI Driver.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbUhcdIsr
    (
    ULONG ptrHCDData
    )
    {
    /* To hold the value of the Interrupt Enable register */

    UINT16 uIntEnable;

    /* To hold the value of Interrupt Status register  */

    UINT16 uIntStatus;

    /* Pointer to the host controller stucture*/

    pUSB_UHCD_DATA pHCDData = (pUSB_UHCD_DATA)ptrHCDData;


    /*
     * Check this to disallow ISR in case we are now dead;
     * This is important because sometimes the PCI interrupt
     * is shared, and there maybe buggy vxbIntDisconnect
     * that does not fully discoonect the ISR, so if that
     * happens, we may get invalid pointer access.
     */

     if ((!pHCDData) || (pHCDData->magic != USB_UHCD_MAGIC_ALIVE))
        {
        USB_UHCD_ERR("usbUhcdIsr - invalid parameters, pHCDData->magic %p\n",
                    pHCDData, pHCDData ? pHCDData->magic : 0, 3, 4, 5, 6);
        return;
        }

    /*
     * Spinlock.  This ISR is invoked 2 ways - one way is the normal
     * interrupt that comes from the controller and the other is through
     * a polling task that invokes the isr at a 50msec rate.  Hence the
     * spinlock.
     */

    SPIN_LOCK_ISR_TAKE (&pHCDData->spinLockIsr);

    /* Read the Interrupt Enable register to get the enabled interrupts */

    uIntEnable = USB_UHCD_READ_WORD (pHCDData->pDev, USB_UHCD_USBINTR);

    /*
     * Disable the interrupts by setting  all bits to zero in
     * Interrupt Enable register - according to the UHCI specification
     */

    USB_UHCD_WRITE_WORD (pHCDData->pDev, USB_UHCD_USBINTR, 0);

    /* USB_HCD_READ_WORD is spinlock safe. */

    /*
     * Read the Interrupt Status Register...
     */

    uIntStatus = USB_UHCD_READ_WORD (pHCDData->pDev, USB_UHCD_USBSTS);

    /* Check if any interrupt status bits are set - only for bits [0:5] */

    if (uIntStatus & USB_UHCD_USBSTS_MASK)
        {
        /* Update the interrupt status register into the HCD data structure */

        pHCDData->uIntStatus |= uIntStatus;

        /* Clears the interrupt status bits by writing '1' to it. */

        USB_UHCD_WRITE_WORD(pHCDData->pDev, USB_UHCD_USBSTS, uIntStatus);
        }

    /* Release the spin lock */

    SPIN_LOCK_ISR_GIVE (&pHCDData->spinLockIsr);

    /*
     * Once the spin lock is released, release the semaphore and log it
     */

    if (uIntStatus & USB_UHCD_USBSTS_MASK)
        {
        /* Release semaphore for handling the transfer completion */

        OS_RELEASE_EVENT(pHCDData->tdCompleteSem);

        USB_UHCD_VDBG("usbUhcdIsr - semaphore released ID[%p], sts[0x%X] \n",
                      pHCDData->tdCompleteSem,
                      uIntStatus, 3, 4, 5, 6);

        }

    /*
     * Update the value which is to be written -
     * save and restore only for bits [0:3]
     */

    uIntEnable = (UINT16)(uIntEnable & USB_UHCD_USBINTR_MASK);

    /* Enable the interrupts by writing to the Interrupt Enable register */

    USB_UHCD_WRITE_WORD (pHCDData->pDev, USB_UHCD_USBINTR, uIntEnable);

    /* Return from the ISR function */

    return;
    }/* End of usbUhcdIsr() */


/*********************** end of usbUhcdIsr.c **********************************/


