/* usbTgtUtilP.h - Definitions for USB GEN2 Target Stack Utility Module */

/*
 * Copyright (c) 2012 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
Modification history
--------------------
01a,05mar10,s_z  created
*/

/*
DESCRIPTION

This file includes the defination and routines of the USB GEN2 target stack
management modules.

INCLUDE FILES:  semLib.h lstLib.h vwModNum.h hwif/vxbus/vxBus.h usb/usb.h
                usb/usbOtg.h usb/target/usbTargLib.h

*/

#ifndef __INCusbTgtUtilPh
#define __INCusbTgtUtilPh


/* includes */

#include <usb/usbTgt.h>

#ifdef  __cplusplus
extern "C" {
#endif

/* defines */


/* extern */

IMPORT pUSBTGT_PIPE usbTgtFindPipeByEpIndex
    (
    pUSBTGT_FUNC_DRIVER pFuncDriver,
    UINT8               uEpIndex
    );

IMPORT pUSBTGT_FUNC_DRIVER usbTgtFindFuncByEpIndex
    (
    pUSBTGT_TCD pTcd,
    UINT8       uEpIndex
    );

IMPORT pUSBTGT_FUNC_DRIVER usbTgtFindFuncByInterface
    (
    pUSBTGT_TCD pTcd,
    UINT8       uInterface
    );

#ifdef  __cplusplus
}
#endif

#endif  /* __INCusbTgtUtilPh */



