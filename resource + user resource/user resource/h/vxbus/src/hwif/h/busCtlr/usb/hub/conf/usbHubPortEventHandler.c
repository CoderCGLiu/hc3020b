/* usbHubPortEventHandler.c - Functions for handling the port events */

/*
 * Copyright (c) 2003-2006, 2008-2010, 2012-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright  2003-2006, 2008-2010, 2012-2013 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
Modification history
--------------------
02h,03may13,wyy  Remove compiler warning (WIND00356717)
02g,14may13,ghs  Make a delay after all ports are powered on in hub (WIND00417261)
02f,17oct12,w_x  Reset Super Speed port by Warm Reset (WIND00374209)
02e,09oct12,w_x  Faking connection change for cold attached device (WIND00379876)
02d,26sep12,w_x  Correct the way to calculate Route String (WIND00378361)
02c,19sep12,w_x  Remove device without retry for delayed removal (WIND00373115)
02b,19sep12,w_x  Delay device removal during device mounting reset (WIND00373115)
02a,04sep12,w_x  Fix quick device detach/attach issue (WIND00370634)
01z,27aug12,w_x  Fix device incorrect parent bus issue (WIND00372253)
01y,19jul12,w_x  Add support for USB 3.0 host (WIND00188662)
01x,03aug10,m_y  Modify routine usbHubPortConnectChangeHandler (WIND00226326)
01w,26jul10,m_y  Modify routine usbHubPortEnableChangeHandler (WIND00173382)
01v,08jul10,m_y  Modify for coding convention
01u,02jul10,w_x  Correct Over Current Change code (further fix WIND00173382)
01t,29jun10,w_x  Add more port reset delay and clean hub logging (WIND00216628)
01s,24may10,m_y  Modify usbHubPortEnableChangeHandler for usb robustness (WIND00183499)
01r,27apr10,j_x  Error debug adaption and hard code replacement (WIND00183874)
01q,27jan10,y_l  Change hub device reset (WIND00151882)
01p,11mar10,j_x  Changed for USB debug (WIND00184542)
01o,10nov09,w_x  Prevent endless enable change loop for quirky hubs that set
                 both reset and enable change at reset complete (WIND00189783)
01n,13jan10,ghs  vxWorks 6.9 LP64 adapting
01m,16oct09,ghs  Delay 10 ms after reset device for t6(WIND00186053)
01l,30sep09,w_x  Fix ghost device issue (WIND00183403)
01k,24aug09,ghs  Fix usbHubPortOverCurrentChangeHandler (WIND00173382)
01j,17jul09,w_x  Fix crash in reset callback (WIND00172353)
01i,26mar09,w_x  debounce for hub disconnection (WIND00105458)
01h,20mar09,w_x  reset port if port enable change while connected (WIND00105458)
01g,13jan09,w_x  Added uParentPortIndex/uParentTier in device info(WIND00151472)
01f,17dec08,w_x  Added mutex to protect hub status data; Use seperate copy
                 of hub status to avoid potential corruption (WIND00148225)
01e,06may08,j_x  Fix WIND00083543
01d,08oct06,ami  Changes for USB-vxBus changes
01c,28mar05,pdg  Allocation of hardware accessed memory changed
01b,15oct04,ami  Refgen Changes
01a,27jun03,nrv  Changing the code to WRS standards
*/

/*
DESCRIPTION

This provides the functions for handling the port events. This module is
used in conjunction with the Hub Event Handling Module and Bus Manager Module.

INCLUDE FILES:  usb2/usbOsal.h, usb2/usbHst.h, usb2/usbHubGlobalVariables.h,
usb2/usbHubUtility.h, usb2/usbHubPortEventHandler.h, usb2/usbHubEventHandler.h
*/

/*
INTERNAL
 *******************************************************************************
 * Filename         : HUB_PortEventHandler.c
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 *
 *
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      : This provides the functions for handling the port events.
 *                    This module is used in conjunction with the Hub Event
 *                    Handling Module and Bus Manager Module.
 *
 *
 ******************************************************************************/



/************************** INCLUDE FILES *************************************/


#include "usb/usbOsal.h"
#include "usb/usbHst.h"
#include "usb/usbd.h"
#include "usbHubGlobalVariables.h"
#include "usbHubUtility.h"
#include "usbHubPortEventHandler.h"
#include "usbHubEventHandler.h"



/****************** MODULE SPECIFIC FUNCTIONS DECLARATION *********************/


/* This function is the handler for the port connection change. */

LOCAL USBHST_STATUS usbHubPortConnectChangeHandler
                   (
                   pUSB_HUB_INFO        pHub,
                   UINT8                uPortIndex,
                   pUSB_HUB_PORT_STATUS pPortStatus
                   );

/* This function is the handler for the port enable change. */

LOCAL USBHST_STATUS usbHubPortEnableChangeHandler
                   (
                   pUSB_HUB_INFO        pHub,
                   UINT8                uPortIndex,
                   pUSB_HUB_PORT_STATUS pPortStatus
                   );

/* This function is the handler for the port suspend change. */

LOCAL USBHST_STATUS usbHubPortSuspendChangeHandler
                   (
                   pUSB_HUB_INFO        pHub,
                   UINT8                uPortIndex,
                   pUSB_HUB_PORT_STATUS pPortStatus
                   );

/* This function is the handler for the port reset state change. */

LOCAL USBHST_STATUS usbHubPortResetChangeHandler
                   (
                   pUSB_HUB_INFO        pHub,
                   UINT8                uPortIndex,
                   pUSB_HUB_PORT_STATUS pPortStatus);

/* This function is the handler for the port over current change. */

LOCAL USBHST_STATUS usbHubPortOverCurrentChangeHandler
                   (
                   pUSB_HUB_INFO        pHub,
                   UINT8                uPortIndex,
                   pUSB_HUB_PORT_STATUS pPortStatus
                   );

/* This function is the handler for the port link state change. */

LOCAL USBHST_STATUS usbHubPortLinkStateChangeHandler
                    (
                    pUSB_HUB_INFO           pHub,
                    UINT8                   uPortIndex,
                    pUSB_HUB_PORT_STATUS    pPortStatus
                    );

/* This function is the handler for the port BH reset state change. */

LOCAL USBHST_STATUS usbHubPortBhResetChangeHandler
                    (
                    pUSB_HUB_INFO           pHub,
                    UINT8                   uPortIndex,
                    pUSB_HUB_PORT_STATUS    pPortStatus
                    );

/* This function is the handler for the port config error change. */

LOCAL USBHST_STATUS usbHubPortConfigErrorChangeHandler
                    (
                    pUSB_HUB_INFO           pHub,
                    UINT8                   uPortIndex,
                    pUSB_HUB_PORT_STATUS    pPortStatus
                    );

/************************ GLOBAL FUNCTIONS DEFINITION *************************/

/***************************************************************************
*
* usbHubPortEventHandler - Handler for any port event that has happened.
*
* This routine handles any port event that has happened.
*
* RETURNS: USBHST_SUCCESS, USBHST_INVALID_PARAMETER, USBHST_FAILURE If any event
* handler failed.
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubPortEventHandler
    (
    pUSB_HUB_INFO pHub,
    UINT8         uPortIndex
    )
    {
    /* To store the result of the port status. */

    pUSB_HUB_PORT_STATUS pPortStatus = NULL;

    /* To store the length of the transfers */

    UINT8 uLength = sizeof(USB_HUB_PORT_STATUS);

    /* To store the results */

    USBHST_STATUS Result = USBHST_FAILURE;

    /* For calulation which byte of pStatus to clear */

    UINT8 uByteToClear=0;

    /* To calculate which bit of uByteToClear in pStatus to clear */

    UINT8 uBitToClear=0;

    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
        USB_HUB_WV_EVENT_HANDLER,
        "Entering usbHubPortEventHandler() Function",
        USB_HUB_WV_FILTER);

    /* If pHub is NULL then return USBHST_INVALID_PARAMETER. */

    if (NULL == pHub)
        {
        /* Debug Message */
        USB_HUB_ERR("invalid parameter \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of (NULL == pHub) */

    /* If pHub::pBus is NULL then return USBHST_INVALID_PARAMETER. */
    if (NULL == pHub->pBus)
        {
        /* Debug Message */
        USB_HUB_ERR("invalid parameter \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of (NULL == pHub->pBus) */

    /* Allocate memory for the port status */

    pPortStatus = OSS_CALLOC(sizeof(USB_HUB_PORT_STATUS));

    /* Check if memory allocation is successful */
    if (pPortStatus == NULL)
        {
        /* Debug Message */
        USB_HUB_ERR("memory allocate failed for port status \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INSUFFICIENT_MEMORY;

        }

    /*
     * Call the HUB_GET_PORT_STATUS() function to retrieve the HUB_PORT_STATUS
     * structure. If failed, then return USBHST_FAILURE.
     * Note: actual port number is 1+ the port index
     */

    Result = USB_HUB_GET_PORT_STATUS(pHub,
                                     uPortIndex,
                                     (UINT8 *)(pPortStatus),
                                     &uLength);

    if ((USBHST_SUCCESS != Result) || ( uLength != sizeof(USB_HUB_PORT_STATUS)))
        {
        /* Debug Message */
        USB_HUB_ERR("get port status failed: result=%d, length=0x%X \n",
                    Result, uLength, 3, 4, 5, 6);

        /* Free the memory allocated for the port status */
        OS_FREE(pPortStatus);
        return USBHST_FAILURE;

        }/* End of if (USBHST_SUCCESS... */

    pPortStatus->wPortChange  = OS_UINT16_LE_TO_CPU(pPortStatus->wPortChange);
    pPortStatus->wPortStatus  = OS_UINT16_LE_TO_CPU(pPortStatus->wPortStatus);

    /*
     * Some Super Speed hubs will not report connection changes
     * for already connected devices, but report reset changes
     * directly. This would make us not able to detect the devices.
     *
     * If the port is connected while no port structured created,
     * we need to fake a connection change so that the port structure
     * can be created and try to mount the port.
     */

    if ((pPortStatus->wPortStatus & USB_PORT_CONNECTION_VALUE) &&
        (!(pPortStatus->wPortChange & USB_C_PORT_CONNECTION_VALUE)) &&
        (pHub->pPortList[uPortIndex] == NULL))
        {
        USB_HUB_WARN("Faking Connection Change on Hub 0x%x Port %d Tier %d\n",
            pHub->uDeviceHandle,
            uPortIndex,
            pHub->uCurrentTier,4,5,6);

        pPortStatus->wPortChange |= USB_C_PORT_CONNECTION_VALUE;
        }

    /*
     * Call HUB_IS_CONNECTION_CHANGE() with HUB_PORT_STATUS::wPortChange. If
     * there is a change in connect status, call HUB_PortConnectChangeHandler()
     * with HUB_PORT_STATUS structure, pHub and uPortIndex.
     * If failed, return result.
     */
    if (TRUE == USB_HUB_IS_CONNECTION_CHANGE(pPortStatus->wPortChange))
        {
        Result = usbHubPortConnectChangeHandler(pHub, uPortIndex, pPortStatus);
        if (USBHST_SUCCESS != Result)
            {
            /* Debug Message */
            USB_HUB_ERR("failed to handle port connect change: " \
                        "result=%d \n",
                        Result, 2, 3, 4, 5, 6);

            }/* End of if USBHST_SUCCESS != Result */

        }/* End of if TRUE == HUB... */

    if (pHub->pBus->uBusSpeed == USBHST_SUPER_SPEED)
        {
        /*
         * Call USB_HUB_IS_LINK_STATE_CHANGE() with HUB_PORT_STATUS::wPortChange.
         * If there is a change in link state, call usbHubPortLinkStateChangeHandler()
         * with HUB_PORT_STATUS structure, pHub and uPortIndex.
         */

        if (TRUE == USB_HUB_IS_LINK_STATE_CHANGE(pPortStatus->wPortChange))
            {
            Result = usbHubPortLinkStateChangeHandler(pHub, uPortIndex, pPortStatus);

            if (USBHST_SUCCESS != Result)
                {
                /* Debug Message */
                USB_HUB_ERR("failed to handle port link state change: " \
                            "result= %d \n",
                            Result, 2, 3, 4, 5, 6);

                }

            }

        /*
         * Call USB_HUB_IS_BH_RESET_CHANGE() with HUB_PORT_STATUS::wPortChange.
         * If there is a change in BH reset, call usbHubPortBhResetChangeHandler()
         * with HUB_PORT_STATUS structure, pHub and uPortIndex.
         */

        if (TRUE == USB_HUB_IS_BH_RESET_CHANGE(pPortStatus->wPortChange))
            {
            Result = usbHubPortBhResetChangeHandler(pHub, uPortIndex, pPortStatus);

            if (USBHST_SUCCESS != Result)
                {
                /* Debug Message */
                USB_HUB_ERR("failed to handle port BH reset change: " \
                            "result= %d \n",
                            Result, 2, 3, 4, 5, 6);

                }
            }

        /*
         * Call USB_HUB_IS_CONFIG_ERROR_CHANGE() with HUB_PORT_STATUS::wPortChange.
         * If there is a change in config error, call usbHubPortConfigErrorChangeHandler()
         * with HUB_PORT_STATUS structure, pHub and uPortIndex.
         */

        if (TRUE == USB_HUB_IS_CONFIG_ERROR_CHANGE(pPortStatus->wPortChange))
            {
            Result = usbHubPortConfigErrorChangeHandler(pHub, uPortIndex, pPortStatus);

            if (USBHST_SUCCESS != Result)
                {
                /* Debug Message */
                USB_HUB_ERR("failed to handle port config error change: " \
                            "result= %d \n",
                            Result, 2, 3, 4, 5, 6);

                }
            }
        }

    /*
     * Call HUB_IS_RESET_CHANGE() with HUB_PORT_STATUS::wPortChange.
     * If there is a change in reset status, call HUB_PortResetChangeHandler()
     * with HUB_PORT_STATUS structure, pHub and uPortIndex.
     * If failed, return result.
     */

    if (TRUE == USB_HUB_IS_RESET_CHANGE(pPortStatus->wPortChange))
        {
        Result = usbHubPortResetChangeHandler(pHub, uPortIndex, pPortStatus);

        if (USBHST_SUCCESS != Result)
            {
            /* Debug Message */
            USB_HUB_ERR("failed to handle port reset change: " \
                        "result = %d \n",
                        Result, 2, 3, 4, 5, 6);

            }/* eND of if USBHST_SUCCESS != Result */
        }/* End of if TRUE == HUB... */

    /*
     * Call HUB_IS_ENABLE_CHANGE() with HUB_PORT_STATUS::wPortChange.
     * If there is a change in enable status, call HUB_PortEnableChangeHandler()
     * with HUB_PORT_STATUS structure, pHub and uPortIndex.
     * If failed, return result.
     */

    if (TRUE == USB_HUB_IS_ENABLE_CHANGE(pPortStatus->wPortChange))
        {
        Result = usbHubPortEnableChangeHandler(pHub, uPortIndex, pPortStatus);

        if (USBHST_SUCCESS != Result)
            {
            /* Debug Message */
            USB_HUB_ERR("failed to handle port enable change: " \
                        "result= %d \n",
                        Result, 2, 3, 4, 5, 6);

            }/* End of if USBHST_SUCCESS != Result */

        }/* End of if TRUE == HUB... */

    /*
     * Call HUB_IS_SUSPEND_CHANGE() with HUB_PORT_STATUS::wPortChange.
     * If there is a change in suspend status,
     * call HUB_PortSuspendChangeHandler() with HUB_PORT_STATUS structure,
     * pHub and uPortIndex. If failed, return result.
     */

    if (TRUE == USB_HUB_IS_SUSPEND_CHANGE(pPortStatus->wPortChange))
        {
        Result = usbHubPortSuspendChangeHandler(pHub, uPortIndex, pPortStatus);

        if (USBHST_SUCCESS != Result)
            {
            /* Debug Message */
            USB_HUB_ERR("failed to handle port suspend change: " \
                        "result= %d \n",
                        Result, 2, 3, 4, 5, 6);

            }/* End of if USBHST_SUCCESS != Result */

        }/* End of if TRUE == HUB... */

    /*
     * Call HUB_IS_OVER_CURRENT_CHANGE() with HUB_PORT_STATUS::wPortChange.
     * If there is a change in over current status, call
     * HUB_PortOverCurrentChangeHandler() with HUB_PORT_STATUS structure,
     * pHub and uPortIndex. If failed, return result.
     */

    if (TRUE == USB_HUB_IS_OVER_CURRENT_CHANGE(pPortStatus->wPortChange))
        {

        Result = usbHubPortOverCurrentChangeHandler(pHub, uPortIndex, pPortStatus);

        if (USBHST_SUCCESS != Result)
            {
            /* Debug Message */
            USB_HUB_ERR("failed to handle port over current change: " \
                        "result= %d \n",
                        Result, 2, 3, 4, 5, 6);

            }/* End of if USBHST_SUCCESS != Result */
        }/* End of if TRUE == HUB... */

    /* Free the memory allocated for the port status */

    OS_FREE(pPortStatus);

    /* Clear the port number  bit of the pHub::pStatus. (BEGIN) */

    /* Find the byte by taking an integer division */

    uByteToClear = (UINT8)((uPortIndex + 1) / 8);

    /* Find the bit by taking the remainder */

    uBitToClear = (UINT8)((uPortIndex + 1) % 8);

    /* Clean up the bit */
    pHub->pCopiedStatus[uByteToClear] =(UINT8)
        (pHub->pCopiedStatus[uByteToClear] & (~((UINT8)0x1<<(uBitToClear))));

    /* Clear the port number bit of the pHub::pStatus. (END) */

    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
        USB_HUB_WV_EVENT_HANDLER,
        "Exiting usbHubPortEventHandler() Function",
        USB_HUB_WV_FILTER);

    /*
     * We want to deal with all the status change bits even one of the
     * change handler occur error. If error happened the port's status
     * will be set in right value and dealt at the caller
     * usbHubStatusChangeHandler.
     * So we always return "USBHST_SUCCESS" here to indicate this
     * routine is handled.
     */

    return USBHST_SUCCESS;
    }/* End of HUB_PortEventHandler() */


/******************* MODULE SPECIFIC FUNCTIONS DEFINITION *********************/

/***************************************************************************
*
* usbHubPortConnectChangeHandler - handler for the connection change.
*
* This routine handles the connection change.
*
* RETURNS: USBHST_SUCCESS, USBHST_INVALID_PARAMETER, USBHST_FAILURE If any event
* handler failed.
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL  USBHST_STATUS usbHubPortConnectChangeHandler
    (
    pUSB_HUB_INFO           pHub,
    UINT8                   uPortIndex,
    pUSB_HUB_PORT_STATUS    pPortStatus
    )
    {
    /* Storage for the port information */

    pUSB_HUB_PORT_INFO      pPort = NULL;

    /* To store the result of submissions */

    USBHST_STATUS           Result;

    /* To store the length of the transfers */

    UINT8 uLength = sizeof(USB_HUB_PORT_STATUS);

    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
        USB_HUB_WV_EVENT_HANDLER,
        "Entering usbHubPortConnectChangeHandler() Function",
        USB_HUB_WV_FILTER);

    /* If the pHub is NULL then return USBHST_INVALID_PARAMETER */

    if (NULL == pHub)
        {
        /* Debug Message */

        USB_HUB_ERR("invalid parameter \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of (NULL == pHub) */

    /*
     * If the uPortIndex greater than pHub::HubDescriptor::bNbrPorts then
     * return USBHST_INVALID_PARAMETER.
     * Note: the actual port number is the port index + 1
     */

    if (uPortIndex >= pHub->HubDescriptor.bNbrPorts)
        {
        /* Debug Message */
        USB_HUB_ERR("port number %d > max %d \n",
                    uPortIndex,
                    pHub->HubDescriptor.bNbrPorts,
                    3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of if (uPortIndex >= pHub->HubDescriptor.bNbrPorts) */

    USB_HUB_DBG("usbHubPortConnectChangeHandler - "
                "hub 0x%X port %d connection change happened\n",
                pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

    /*
     * Retrieve the HUB_PORT_INFO structure from pHub::pPortList
     * for the uPortIndex.
     */

    pPort = pHub->pPortList[uPortIndex];

    /*
     * If the port is enabled then Call the
     * HUB_RemoveDevice() function with
     * HUB_PORT_INFO::uDeviceHandle.
     * 9/5/2k3:NM: Changed here to centralise the effect
     */

    if (pPort != NULL)
        {
        pPort->bDelayedRemove = FALSE;

        if (FALSE == pPort->bResetCheckDevice)
            usbHubRemoveDevice(pHub, uPortIndex);
        else
            pPort->bDelayedRemove = TRUE;
        }

    /*
     * Call HUB_CLEAR_PORT_FEATURE() to clear the C_PORT_CONNECTION.
     * If failed return result.
     * Note: the actual port number is the port index + 1
     */

    Result = USB_HUB_CLEAR_PORT_FEATURE(pHub,
                                        uPortIndex,
                                        USB_C_PORT_CONNECTION);

    if (USBHST_SUCCESS != Result)
        {
        /* Debug Message */

        USB_HUB_ERR("failed to clear port USB_C_PORT_CONNECTION feature for "
                    "hub 0x%X port %d, result %d\n",
                    pHub->uDeviceHandle, uPortIndex, Result, 4, 5, 6);

        return Result;

        }/* End of USBHST_SUCCESS... */


   /* Re-get the pPortStatus */

   OS_MEMSET(pPortStatus, 0, sizeof(USB_HUB_PORT_STATUS));

   /*
    * Call the HUB_GET_PORT_STATUS() function to retrieve the HUB_PORT_STATUS
    * structure. If failed, then return USBHST_FAILURE.
    * Note: actual port number is 1 + the port index
    */

   Result = USB_HUB_GET_PORT_STATUS(pHub,
                                    uPortIndex,
                                    (UINT8 *)(pPortStatus),
                                    &uLength);

   if ((USBHST_SUCCESS != Result) || ( uLength != sizeof(USB_HUB_PORT_STATUS)))
       {
       /* Debug Message */

       USB_HUB_ERR("get port status failed: result=%d, length=0x%X \n",
                   Result, uLength, 3, 4, 5, 6);

       return USBHST_FAILURE;

       }/* End of if (USBHST_SUCCESS... */

   pPortStatus->wPortChange  = OS_UINT16_LE_TO_CPU(pPortStatus->wPortChange);
   pPortStatus->wPortStatus  = OS_UINT16_LE_TO_CPU(pPortStatus->wPortStatus);

    /*
     * If the pPortStatus::wPortStatus is DISCONNECTED then return
     * USBHST_SUCCESS.
     */

    if (0 == (pPortStatus->wPortStatus & USB_PORT_CONNECTION_VALUE))
        {
        /* Debug Message */

        USB_HUB_DBG("usbHubPortConnectChangeHandler - "
                    "device disconnected on hub 0x%X port %d\n",
                    pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

        return USBHST_SUCCESS;

        } /* End of (0== (pPortStatus... */

    /*
     * Call OS_MALLOC() to allocate memory for HUB_PORT_INFO and set
     * it to pPort. If failed, then return with USBHST_INSUFFICIENT_MEMORY.
     */

    pPort = OS_MALLOC(sizeof(USB_HUB_PORT_INFO));

    if (NULL == pPort)
        {
        /* Debug Message */

        USB_HUB_ERR("memory allocate failed for port information "
                    "for hub 0x%X port %d\n",
                    pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

        return USBHST_INSUFFICIENT_MEMORY;

        }/* End of if (NULL == pPort) */

    /* Clear the allocated buffer. */

    OS_MEMSET(pPort, 0, sizeof(USB_HUB_PORT_INFO));

    /*
     * Call OS_CREATE_EVENT() to create an OS_EVENT_ID event. If this fails
     * return USBHST_FAILURE.
     */

    pPort->hPortStatusSem = OS_CREATE_EVENT(OS_EVENT_NON_SIGNALED);

    if (NULL == pPort->hPortStatusSem)
        {
        /* Debug Message */

        USB_HUB_ERR("event create failed for hub 0x%X port %d\n",
                    pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

        OS_FREE(pPort);

        return USBHST_FAILURE;
        }/* End of (NULL == EventId) */

    /* Set the parent hub pointer so that we can find the parent */

    pPort->pParentHub = pHub;

    /* Record where we are on the parent hub */

    pPort->uPortIndex = uPortIndex;

    /*
     * Reset the number of retries for connection by setting the
     * HUB_PORT_INFO::uConnectRetries as HUB_CONFIG_RETRY_MAX
     */

    pPort->uConnectRetry = USB_HUB_CONFIG_RETRY_MAX;

    /* Set pHub::pPortList[uPortIndex] as the hub_PORT_INFO structure. */

    pHub->pPortList[uPortIndex] = pPort;

    /*
     * For Super Speed devices, which always sit on Super Speed bus, we
     * do not need to reset the device (it is always enabled when the
     * connection change is reported), so we make a fake reset change.
     */

    if (pHub->pBus->uBusSpeed == USBHST_SUPER_SPEED)
        {
        /*
         * Set the HUB_PORT_INFO::StateOfPort as
         * USB_HUB_RESET_COMPLETION_PENDING.
         */

        pPort->StateOfPort = USB_HUB_RESET_COMPLETION_PENDING;

        /* Yes, we know its Super Speed since it is on Super Speed bus */

        pPort->uPortSpeed = USBHST_SUPER_SPEED;

        /* Make a fake Reset Change */

        pPortStatus->wPortChange |= USB_C_PORT_RESET_VALUE;

        USB_HUB_WARN("Adding a Supper Speed Port\n",
            1, 2, 3, 4, 5, 6);
        }
    else
        {
        pPort->uPortSpeed = USB_HUB_DEVICE_SPEED(pPortStatus->wPortStatus);

        /* Set the HUB_PORT_INFO::StateOfPort as HUB_DEBOUNCE_PENDING. */

        pPort->StateOfPort = USB_HUB_DEBOUNCE_PENDING;
        }

    /* Set bResetCheckDevice as FALSE when the pPort is created */

    pPort->bResetCheckDevice = FALSE;

    /* Set bDelayedRemove as FALSE when the pPort is created */

    pPort->bDelayedRemove = FALSE;

    /*
     * Record the Route String. If the hub is at tier 1, it is root hub,
     * so the port route string does not apply according to USB 3.0 spec.
     * However, we use the same idea to use this field as the "Root Hub
     * Port Number", so that we can pass this information to newDevice()
     * to extract it here. For tiers further down the hub tree, we can
     * construct the Route String according to the spec.
     *
     * The hub uses a Hub Depth value multiplied by four as an offset
     * into the Route String to locate the bits it uses to determine the
     * downstream port number. The "Hub Depth" here is equal to the hub
     * tier mius 1. So that the 1st external hub gets a "Hub Depth" of 0
     * and the next tier down external hub gets a "Hub Depth" of 1, and
     * so on.
     */

    if (pHub->uCurrentTier == 1)
        {
        pPort->uRouteString = (uPortIndex + 1);
        }
    else
        {
        /*
         * The "Port Number" is (uPortIndex + 1) and it should be
         * at most 15 (for USB 3.0 hubs). For USB 2.0 hubs, it may
         * have more than 15 downstream ports, but the spec requires
         * we set it to 15 for that case.
         */

        if ((uPortIndex + 1) <= 15)
            {
            pPort->uRouteString = pHub->pParentPort->uRouteString +
                    (((uPortIndex + 1) << ((pHub->uCurrentTier - 1) * 4)));
            }
        else
            {
            pPort->uRouteString = pHub->pParentPort->uRouteString +
                    ((15 << ((pHub->uCurrentTier - 1) * 4)));
            }
        }

    USB_HUB_WARN("Adding a new device at hub 0x%x port %d uRouteString 0x%06x\n",
        pHub->uDeviceHandle, uPortIndex, pPort->uRouteString, 4, 5, 6);

    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
        USB_HUB_WV_EVENT_HANDLER,
        "Exiting usbHubPortConnectChangeHandler() Function",
        USB_HUB_WV_FILTER);

    /* Return USBHST_SUCCESS. */

    return USBHST_SUCCESS;

    } /* End of usbHubPortConnectChangeHandler () */


/***************************************************************************
*
* usbHubPortEnableChangeHandler - handle the port enable change
*
* This routine handles the port enable change.
*
* RETURNS: USBHST_SUCCESS, USBHST_INVALID_PARAMETER, USBHST_FAILURE If any event
* handler failed.
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubPortEnableChangeHandler
    (
    pUSB_HUB_INFO           pHub,
    UINT8                   uPortIndex,
    pUSB_HUB_PORT_STATUS    pPortStatus
    )
    {
    /* Storage for the port information */

    pUSB_HUB_PORT_INFO pPort = NULL;

    /* To store the result of submissions */

    USBHST_STATUS Result;

    UINT8 uLength = sizeof(USB_HUB_PORT_STATUS);

    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
        USB_HUB_WV_EVENT_HANDLER,
        "Entering usbHubPortEnableChangeHandler() Function",
        USB_HUB_WV_FILTER);

    /* If the pHub is NULL then return USBHST_INVALID_PARAMETER */

    if (NULL == pHub)
        {
        /* Debug Message */
        USB_HUB_ERR("invalid parameter \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of (NULL == pHub) */

    /*
     * If the uPortIndex greater than pHub::HubDescriptor::bNbrPorts then
     * return USBHST_INVALID_PARAMETER.
     * Note: the actual port number is the port index + 1
     */

    if (uPortIndex >= pHub->HubDescriptor.bNbrPorts)
        {
        /* Debug Message */
        USB_HUB_ERR("port number %d > max %d \n",
                    uPortIndex,
                    pHub->HubDescriptor.bNbrPorts,
                    3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of if (uPortIndex >= pHub->HubDescriptor.bNbrPorts) */

    USB_HUB_DBG("usbHubPortEnableChangeHandler - "
                "hub 0x%X port %d enable change happened\n",
                pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

    /*
     * Call HUB_CLEAR_PORT_FEATURE()  to clear the C_PORT_ENABLE.
     * If failed return result.
     * Note: the actual port number is the port index + 1
     */

    Result = USB_HUB_CLEAR_PORT_FEATURE(pHub, uPortIndex, USB_C_PORT_ENABLE);
    if (USBHST_SUCCESS != Result)
        {
        /* Debug Message */

        USB_HUB_ERR("failed to clear port USB_C_PORT_ENABLE feature for "
                    "hub 0x%X port %d enable change happened\n",
                    pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

        return Result;

        }/* End of USBHST_SUCCESS != Result */

    OS_MEMSET(pPortStatus, 0, sizeof(USB_HUB_PORT_STATUS));

    /*
     * Call the HUB_GET_PORT_STATUS() function to retrieve the HUB_PORT_STATUS
     * structure. If failed, then return USBHST_FAILURE.
     * Note: actual port number is 1 + the port index
     */

    Result = USB_HUB_GET_PORT_STATUS(pHub,
                                     uPortIndex,
                                     (UINT8 *)(pPortStatus),
                                     &uLength);

    if ((USBHST_SUCCESS != Result) || ( uLength != sizeof(USB_HUB_PORT_STATUS)))
        {
        /* Debug Message */

        USB_HUB_ERR("get port status failed: result=%d, length=0x%X \n",
                    Result, uLength, 3, 4, 5, 6);

        return USBHST_FAILURE;

        }/* End of if (USBHST_SUCCESS... */

    pPortStatus->wPortChange  = OS_UINT16_LE_TO_CPU(pPortStatus->wPortChange);
    pPortStatus->wPortStatus  = OS_UINT16_LE_TO_CPU(pPortStatus->wPortStatus);

    /*
     * Check the status is enable or not. If enable return USBHST_SUCCESS.
     *
     * From the USB2.0 Spec 11.24.2.7.2.2 C_PORT_ENABLE
     * This bit is set when the PORT_ENABLE bit changes from one to zero as a
     * result of a Port Error condition. This bit is not set on any other
     * changes to PORT_ENABLE.
     *
     * Some host controllers will report enable change even the port status
     * change from disable to enable. We must avoid this condition, or else
     * we will delete the device wrongly.
     */

    if (TRUE == USB_HUB_IS_PORT_ENABLED(pPortStatus->wPortStatus))
        {
        /* Debug Message */

        USB_HUB_DBG("usbHubPortEnableChangeHandler - "
                    "hub 0x%X port %d is enabled!Ignore the enable change!\n",
                    pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

        return USBHST_SUCCESS;
        }

    /*
     * Retrieve the HUB_PORT_INFO structure from pHub::pPortList for the
     * uPortIndex, if there is no port then, there is no device in topology,
     * hence return USBHST_SUCCESS.
     */

    pPort = pHub->pPortList[uPortIndex];
    if (NULL == pPort)
        {
        /* Debug Message */

        USB_HUB_DBG("usbHubPortEnableChangeHandler - "
                    "no device connected on hub 0x%X port %d.\n",
                    pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

        return USBHST_SUCCESS;

        }/* End of if (NULL != pPort ) */

    /*
     * Sometimes, when we reveive enable change event of a device,
     * the device may still connected and should be re-probed.
     *
     * At first, we add a member "uEnableChangeCount" of the structure
     * USB_HUB_PORT_INFO as the retry times.  If the retry times reach
     * USB_HUB_ENABLE_CHANGE_MAX, we will mark the port as deleted.
     *
     * In some customer environment such as bad EMC/EMI situation, it
     * would be desirabled to do endless retry until the device gets
     * enabled again without user intervention, thus the limited retry
     * count mechanism is removed.
     *
     * Re-get the port's status, if the device is still connected
     * and powered, call usbHubPortConnectChangeHandler to re-probe the
     * device.
     */

    if (pPortStatus->wPortStatus == (USB_PORT_CONNECTION_VALUE|USB_PORT_POWER_VALUE))
        {
        if (USBHST_SUCCESS !=
            usbHubPortConnectChangeHandler(pHub, uPortIndex, pPortStatus))
            {
            USB_HUB_ERR("usbHubPortConnectChangeHandler failed\n",
                        1, 2, 3, 4, 5, 6);
            return USBHST_FAILURE;
            }
        }
    else
        {
        /* Set the HUB_PORT_INFO::StateOfPort as MARKED_FOR_DELETION */

        USB_MARK_FOR_DELETE_PORT(pHub, pPort);
        }

    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
        USB_HUB_WV_EVENT_HANDLER,
        "Exiting usbHubPortEnableChangeHandler() Function",
        USB_HUB_WV_FILTER);

    /* Return USBHST_SUCCESS */
    return USBHST_SUCCESS;

    } /* End of usbHubPortEnableChangeHandler() */

/***************************************************************************
*
* usbHubPortSuspendChangeHandler - handler for the port suspend change.
*
* This routine handles the the port suspend change.
*
* RETURNS: USBHST_SUCCESS, USBHST_INVALID_PARAMETER, USBHST_FAILURE If any event
* handler failed.
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubPortSuspendChangeHandler
    (
    pUSB_HUB_INFO        pHub,
    UINT8                uPortIndex,
    pUSB_HUB_PORT_STATUS pPortStatus
    )
    {
    /* Storage for the port information */

    pUSB_HUB_PORT_INFO pPort=NULL;

    /* to store the result of submissions */

    USBHST_STATUS Result;

    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
        USB_HUB_WV_EVENT_HANDLER,
        "Entering usbHubPortSuspendChangeHandler() Function",
        USB_HUB_WV_FILTER);

    /* If the pHub is NULL then return USBHST_INVALID_PARAMETER */

    if (NULL == pHub)
        {
        /* Debug Message */

        USB_HUB_ERR("invalid parameter \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of (NULL == pHub) */

    /*
     * If the uPortIndex greater than pHub::HubDescriptor::bNbrPorts then
     * return USBHST_INVALID_PARAMETER.
     * Note: the actual port number is the port index + 1
     */
    if (uPortIndex >= pHub->HubDescriptor.bNbrPorts)
        {
        /* Debug Message */
        USB_HUB_ERR("port number %d > max %d \n",
                    uPortIndex,
                    pHub->HubDescriptor.bNbrPorts,
                    3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of if (uPortIndex >= pHub->HubDescriptor.bNbrPorts) */

    USB_HUB_DBG("usbHubPortSuspendChangeHandler - "
                "hub 0x%X port %d suspend change happened\n",
                pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

    /*
     * Call HUB_CLEAR_PORT_FEATURE()  to clear the C_PORT_SUSPEND.
     * If failed return result.
     * Note: the actual port number is the port index + 1
     */

    Result = USB_HUB_CLEAR_PORT_FEATURE(pHub, uPortIndex, USB_C_PORT_SUSPEND);

    if (USBHST_SUCCESS != Result)
        {
        /* Debug Message */

        USB_HUB_ERR("failed to clear port USB_C_PORT_SUSPEND feature on "
                    "hub 0x%X port %d.n",
                    pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

        return Result;

        }/* End of USBHST_SUCCESS != Result */

    /*
     * Retrieve the HUB_PORT_INFO structure from pHub::pPortList for the
     * uPortIndex, if there is no port then, there is no device in topology,
     * hence return USBHST_SUCCESS.
     */

    pPort = pHub->pPortList[uPortIndex];
    if (NULL == pPort)
        {
        /* Debug Message */

        USB_HUB_DBG("usbHubPortSuspendChangeHandler - no device connected on "
                    "hub 0x%X port %d.\n",
                    pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

        return USBHST_SUCCESS;

        }/* End of if (NULL != pPort ) */

    /*
     * If the suspend status is enabled in the pPortStatus::wPortStatus
     * i.    Call g_USBHSTFunctionList::USBHST_SuspendDevice() with
     *       HUB_PORT_INFO::uDeviceHandle to suspend the device driver.
     * ii.   Return USBHST_SUCCESS
     */

    if (0 != (pPortStatus->wPortStatus & USB_PORT_SUSPEND_VALUE))
        {
        /* Debug Message */

        USB_HUB_DBG("usbHubPortSuspendChangeHandler - suspendDevice for device "
                    "0x%X on hub 0x%X port %d.\n",
                    pPort->uDeviceHandle,
                    pHub->uDeviceHandle, uPortIndex, 4, 5, 6);

        g_usbHstFunctionList.UsbdToHubFunctionList.suspendDevice(pPort->uDeviceHandle);

        return USBHST_SUCCESS;

        } /* End of (0== (pPortStatus... */

    /* Debug Message */

    USB_HUB_DBG("usbHubPortSuspendChangeHandler - resumeDevice for device "
                "0x%X on hub 0x%X port %d.\n",
                pPort->uDeviceHandle,
                pHub->uDeviceHandle, uPortIndex, 4, 5, 6);

    /* Call the g_USBHSTFunctionList::USBHST_ResumeDevice() with
     * HUB_PORT_INFO::uDeviceHandle to notify the resume the device driver.
     */

    g_usbHstFunctionList.UsbdToHubFunctionList.resumeDevice(pPort->uDeviceHandle);

    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
        USB_HUB_WV_EVENT_HANDLER,
        "Exiting usbHubPortSuspendChangeHandler() Function",
        USB_HUB_WV_FILTER);

    /* Return USBHST_SUCCESS */
    return USBHST_SUCCESS;

    } /* End of usbHubPortSuspendChangeHandler () */

/***************************************************************************
*
* usbHubPortResetChangeHandler - Handler for the reset change.
*
* This routine handles the reset change.
*
* RETURNS: USBHST_SUCCESS, USBHST_INVALID_PARAMETER, USBHST_FAILURE If any event
* handler failed.
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubPortResetChangeHandler
    (
    pUSB_HUB_INFO        pHub,
    UINT8                uPortIndex,
    pUSB_HUB_PORT_STATUS pPortStatus
    )
    {
    /* Storage for the port information */

    pUSB_HUB_PORT_INFO pPort = NULL;

    /* device handle for parent hub */

    UINT32 uParentDeviceHandle = 0;

    /* storage for parent hub */

    pUSB_HUB_INFO  pParentHub = NULL;

    /* to store the result of submissions */

    USBHST_STATUS Result;

    /* Storage of the device Handle */

    UINT32 uDeviceHandle = 0;

    /* Port number on the hub */

    UINT8 uPortNumber = 0;
    /* to store hub device info */

    pUSBD_DEVICE_INFO pHubInfo = NULL;

    /* to store device info */

    pUSBD_DEVICE_INFO pDeviceInfo = NULL;

    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
        USB_HUB_WV_EVENT_HANDLER,
        "Entering usbHubPortResetChangeHandler() Function",
        USB_HUB_WV_FILTER);

    /* If the pHub is NULL then return USBHST_INVALID_PARAMETER */

    if (NULL == pHub)
        {
        /* Debug Message */
        USB_HUB_ERR("invalid parameter \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of (NULL == pHub) */

    /*
     * If the uPortIndex greater than pHub::HubDescriptor::bNbrPorts then
     * return USBHST_INVALID_PARAMETER.
     * Note: the actual port number is the port index + 1
     */

    if (uPortIndex >= pHub->HubDescriptor.bNbrPorts)
        {
        /* Debug Message */
        USB_HUB_ERR("port number %d > max %d \n",
                    uPortIndex,
                    pHub->HubDescriptor.bNbrPorts,
                    3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of if (uPortIndex >= pHub->HubDescriptor.bNbrPorts) */

    USB_HUB_WARN("usbHubPortResetChangeHandler - "
                "hub 0x%X port %d reset change happened\n",
                pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

    /* Update the pHub::bDeviceBeingConfigured to FALSE. */

    pHub->pBus->bDeviceBeingConfigured = FALSE;

    /*
     * Call HUB_CLEAR_PORT_FEATURE() to clear the C_PORT_RESET.
     * If failed return result.
     * Note: the actual port number is the port index + 1
     */

    Result = USB_HUB_CLEAR_PORT_FEATURE(pHub, uPortIndex, USB_C_PORT_RESET);

    if (USBHST_SUCCESS != Result)
        {
        /* Debug Message */

        USB_HUB_ERR("failed to clear port USB_C_PORT_RESET feature for "
                    "hub 0x%X port %d.\n",
                    pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

        return Result;

        }/* End of USBHST_SUCCESS != Result */

    /*
     * If the pPortStatus::wPortStatus is Port Disabled then,
     * i.    Call HUB_RetryDeviceConfiguration (). If failed, return the result.
     * ii.   Return USBHST_SUCCESS.
     */

    /*
     * Sandeep 15/07/2003 Changed from PORT_CONNECTION_VALUE
     * If the port has been disabled before the reset completion
     * happens ,retry the device configuration
     */

    if (0 == (pPortStatus->wPortStatus & USB_PORT_ENABLE_VALUE))
        {
        /* Debug Message */

        USB_HUB_DBG("usbHubPortResetChangeHandler - "
                    "hub 0x%X port %d is disabled at reset complete.\n",
                    pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);
       /*
        * Previously at this point the code is to call
        * usbHubRetryDeviceConfiguration to try to reset the port again.
        * But at this point, the enable change is also set, so the port
        * event handler will continue to be called for enable change.
        * However the enable change handler was also changed to reset
        * the port. This needs to be changed so that the port only
        * get reset in the enable change handler, with a limitted enable
        * change retry count.
        */

        return USBHST_SUCCESS;

        } /* End of (0== (pPortStatus... */

    /* Retrieve the HUB_PORT_INFO from pHub::pPortList[uPortIndex]. */

    pPort = pHub->pPortList[uPortIndex];

    if (pPort == NULL)
        {
        /* Debug Message */

        USB_HUB_DBG("usbHubPortResetChangeHandler - "
                    "hub 0x%X port %d pPort still null, returning.\n",
                    pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

        /* Return the result */
        return USBHST_SUCCESS;
        }

    /* Update the HUB_PORT_INFO::StateOfPort to HUB_PORT_DEFAULT. */

    pPort->StateOfPort = USB_HUB_PORT_DEFAULT;

    if (USBHST_HIGH_SPEED != USB_HUB_DEVICE_SPEED(pPortStatus->wPortStatus))
        {
        /* port is  full speed */

        /*
         * If this hub is a high speed hub then this is the
         * hub handle and port number to be passed
         */

        if (pHub->uHubTTInfo > 0)
            {
            uParentDeviceHandle = pHub->uDeviceHandle;

            /* actual port number is port index +1 */

            uPortNumber = (UINT8)(uPortIndex  + 1);

            /*
             * Clear the last bit of uPortNumber and set it to One
             * if the The hub is multiple TT hub
             */

            uPortNumber =
                (UINT8)((uPortNumber & HUB_MASK_MULTIPLE_TT_PORT) |
                        ((pHub->uHubTTInfo == 2) ? HUB_MASK_MULTIPLE_TT : 0));

            }/*endof if(pHub->uHubTTInfo   )*/

        else
            {
            /* this is a high speed bus */

            if (pHub->pBus->pRootHubInfo->uHubTTInfo > 0)
                {
                pParentHub = usbHubFindNearestParent(pHub->uDeviceHandle,
                                pHub->pBus->pRootHubInfo, &uPortNumber) ;

                if (NULL == pParentHub)
                    {
                    /* Debug Message */
                    USB_HUB_ERR("parent hub not found \n",
                                1, 2, 3, 4, 5, 6);

                    return USBHST_INVALID_PARAMETER;

                    }/* End of if if( NULL == pParentHub) */

                uParentDeviceHandle = pParentHub->uDeviceHandle;

                /* Actual port number is uPortNumber + 1 */

                uPortNumber = (UINT8)(uPortNumber + 1);

                /*
                 * Clear the last bit of uPortNumber and set it to One
                 * if the The hub is multiple TT hub
                 */

                uPortNumber =
                    (UINT8)((uPortNumber & HUB_MASK_MULTIPLE_TT_PORT) |
                            ((pHub->uHubTTInfo == 2) ? HUB_MASK_MULTIPLE_TT : 0));

                }/* end of if(pHub->pBus...)*/
            else /* for full low speed bus pParentHub is amde NULL and port number is Zero*/
                {
                uParentDeviceHandle = 0;

                uPortNumber = 0;
                }/* end of else */

            }/*  end of else  */

        }/* end of if( 0 == (pPortStatus->....) )*/

    /*
     * 7.1.7.3 Connect and Disconnect Signaling
     * ...
     * t6 (TRSTRCY) The USB System Software guarantees a minimum of 10 ms for
     * reset recovery. Device response to any bus transactions addressed to the
     * default device address during the reset recovery time is undefined.
	 *
	 * Note: Move it from before the check for port enable value here because
	 * there is no need to do the delay if the port is disabled.
     */

    OS_DELAY_MS(10);

    /* Update the port speed */

    if (pPort->uPortSpeed != USBHST_SUPER_SPEED)
        {
        pPort->uPortSpeed = USB_HUB_DEVICE_SPEED(pPortStatus->wPortStatus);
        }

    /*
     * For upper driver just like class driver's device reset request, no need
     * to create device again, just set the StateOfPort to USB_HUB_PORT_CONFIGURED
     * and return success, Let the caller to send SET_CONFIG and SET_INTERFACE
     * to let the device run to a correct state.
     */

    if (TRUE == pPort->bResetCheckDevice)
        {
        pPort->bResetCheckDevice = FALSE;

        Result = g_usbHstFunctionList.UsbdToHubFunctionList.resetDeviceCheck(
            pPort->uDeviceHandle,
            pPort->uPortSpeed,
            pHub->pBus->uBusHandle);

        if (USBHST_SUCCESS != Result)
            {
            USB_HUB_ERR("resetDeviceCheck failed for hub 0x%X port %d.\n",
                        pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

            return USBHST_FAILURE;
            }

        pPort->StateOfPort = USB_HUB_PORT_CONFIGURED;

        USB_HUB_DBG("resetDeviceCheck successful for hub 0x%X port %d.\n",
                    pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

        return USBHST_SUCCESS;
        }

    /*
     * Call g_USBHSTFunctionList::USBHST_NewDevice(). If this call fails
     * i.    Call HUB_RetryDeviceConfiguration (). If failed, return the result.
     * ii.   Return USBHST_SUCCESS.
     */

    Result = g_usbHstFunctionList.UsbdToHubFunctionList.newDevice(
             pPort->uPortSpeed,
             pHub->pBus->uBusHandle,
             uParentDeviceHandle,
             uPortNumber,
             pHub->uCurrentTier,
             uPortIndex,
             pPort->uRouteString,
             &uDeviceHandle);

	
    /*
     * Obtain the USB device information structure for the device from the
     * handle
     */

    usbdTranslateDeviceHandle (uDeviceHandle, &pDeviceInfo);

    if ((USBHST_SUCCESS != Result) || (pDeviceInfo == NULL))
        {
        /* Debug Message */

        USB_HUB_WARN("usbHubPortResetChangeHandler - "
                     "newDevice failed to obtain device information "
                     "for hub 0x%X port %d, retry... \n",
                    pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

        Result = usbHubRetryDeviceConfiguration(pHub,uPortIndex);

        /* Debug Helping code */

        if (USBHST_SUCCESS != Result)
            {
            /* Debug Message */

            USB_HUB_WARN("usbHubPortResetChangeHandler - "
                         "failed to retry device configuration "
                         "for hub 0x%X port %d, Result=%d \n",
                        pHub->uDeviceHandle, uPortIndex, Result, 4, 5, 6);
            }
        else
            {
            /* Debug Message */

            USB_HUB_DBG("usbHubPortResetChangeHandler - "
                         "success to retry device configuration "
                         "for hub 0x%X port %d.\n",
                        pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);
            }

        /* End of if (USBHST_SUCCESS != Result) */

        return Result;

        } /* End of if (0 !=..  */

    /* Update the HUB_PORT_INFO::PortState to HUB_PORT_ADDRESSED. */
    pPort->StateOfPort = USB_HUB_PORT_ADDRESSED;

    /*
     * Update the HUB_PORT_INFO::uDeviceHandle with the device handle returned
     * by the USBHST_NewDevice() call.
     */
    pPort->uDeviceHandle = uDeviceHandle;

    /*
     * Store the parent bus information for the vxBus device information
     * structure
     */

    /*
     * Obtain the USB device information structure for the root hub to which
     * the device is connected from the handle.
     */

#ifdef USB_HUB_CREATE_BUS_INSTANCE_FOR_EACH_HUB
    usbdTranslateDeviceHandle (pHub->uDeviceHandle, &pHubInfo);
#else
    usbdTranslateDeviceHandle (pHub->pBus->uDeviceHandle, &pHubInfo);
#endif

    /* Store the pParentBus information in vxBus device info structure */

    pDeviceInfo->pDev->pParentBus = pHubInfo->pDev->u.pSubordinateBus;

    /* Store the bus id */

    pDeviceInfo->pDev->busID = VXB_BUSID_USB_HUB;

    /*
     * Call the g_USBHSTFunctionList::USBHST_ConfigureDevice() function.
     * If this call fails,
     * i.    Call HUB_RetryDeviceConfiguration (). If failed, return the result.
     * ii.   Return USBHST_SUCCESS.
     */
    Result = g_usbHstFunctionList.UsbdToHubFunctionList.configureDevice(uDeviceHandle);

    /*
     * During the configureDevice() call, the stack will try to match
     * corresponding class driver for the device and let the class driver
     * mount it by calling class driver addDevice() call, so the initial
     * mouting is done in the BusM task. There is chance that the device
     * is removed (or get any error, not responding, etc) during mounting,
     * the class driver is not aware of this removal or error situation
     * and tries to do error handling when any transfer failure happens.
     *
     * One special error handling is to "reset the device" by calling the
     * usbHubResetDevice(). The usbHubResetDevice() allows the case when
     * it is called just in BusM task, in which case it will wait for the
     * Reset Change event after issuing the reset request, and when any
     * change happens on the port, it will call usbHubPortEventHandler()
     * to handle these change events (including possibly the Disconnect
     * Change). So this creates a special case, where the port has been
     * removed during the mounting process, and originally it would call
     * usbHubRemoveDevice() directly in usbHubPortConnectChangeHandler()
     * to remove the device, so it would call class driver removeDevice()
     * to "unmount" the device.
     *
     * The following things would happen in the above special case:
     *
     * configureDevice()->
     *          -> addDevice() ->
     *                 -> create some data structure ->
     *                 -> usbHubResetDevice ->
     *                        -> reset change and disconnect change ->
     *                        -> connect change handler ->
     *                                 -> removeDevice() ->
     *                                      -> destroy data structure created
     *                 <-...........back from usbHubResetDevice ... ->
     *                 <-...........back into addDevice() ...->
     *                 -> continue to use the data structures already destroyed
     *                 -> CRASH, and the BusM task stops!!!
     *
     * To avoid the above crash problem, we should not remove the device
     * while it is being reset when it is being mounted. We use a special
     * flag to record this situation (bDelayedRemove is set to TRUE). When
     * the addDevice() returns, and the BusM moves out configureDevice(),
     * we are safe to remove the device.
     */

    if (pPort->bDelayedRemove == TRUE)
        {
        USB_HUB_WARN("usbHubPortResetChangeHandler - "
                    "hub 0x%X port %d has been removed while mounting\n",
                    pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

        usbHubRemoveDevice(pHub, uPortIndex);

        /* Return USBHST_SUCCESS. */
        return USBHST_SUCCESS;
        }

    /* Now we can check the configureDevice() result */

    if (USBHST_SUCCESS != Result)
        {
        /* Debug Message */

        USB_HUB_WARN("usbHubPortResetChangeHandler - "
                     "configureDevice failed to configure device "
                     "for hub 0x%X port %d, Result=%d, retry... \n",
                    pHub->uDeviceHandle, uPortIndex, Result, 4, 5, 6);

        /* We need to remove the device created by newDevice() */

        g_usbHstFunctionList.UsbdToHubFunctionList.removeDevice(uDeviceHandle);

        /* TODO: Call the Remove device of the USBHST */

        Result = usbHubRetryDeviceConfiguration(pHub, uPortIndex);

        /* Debug help */

        if (USBHST_SUCCESS != Result)
            {
            /* Debug Message */

            USB_HUB_WARN("usbHubPortResetChangeHandler - "
                         "failed to retry device configuration "
                         "for hub 0x%X port %d, Result=%d.\n",
                        pHub->uDeviceHandle, uPortIndex, Result, 4, 5, 6);
            }
        else
            {
            /* Debug Message */

            USB_HUB_DBG("usbHubPortResetChangeHandler - "
                         "success to retry device configuration "
                         "for hub 0x%X port %d, Result=%d.\n",
                        pHub->uDeviceHandle, uPortIndex, Result, 4, 5, 6);
            }/* End of if (USBHST_SUCCESS != Result) */

        /* Return result */
        return Result;

        } /* End of if (USBHST_SUCCESS != Result) */

    /* Update the HUB_PORT_INFO::PortState to HUB_PORT_CONFIGURED. */
    pPort->StateOfPort = USB_HUB_PORT_CONFIGURED;

    if (pPort->pHub != NULL)
        {
        UINT8 uPortCount = 0;

        USB_HUB_WARN("usbHubPortResetChangeHandler - "
                    "Added new hub 0x%x at port %d tier %d\n",
                    pPort->pHub->uDeviceHandle, uPortIndex, pPort->pHub->uCurrentTier, 4, 5, 6);

        pPort->pHub->uPortIndex = uPortIndex;

        pPort->pHub->pParentHub = pHub;

        /*
         * For newly attached hubs, try to poll the ports since
         * some hubs will not report any changes for cold attached
         * devices.
         */

        for (uPortCount = 0;
            uPortCount < pPort->pHub->HubDescriptor.bNbrPorts;
            uPortCount++)
            {
            usbHubPortEventHandler(pPort->pHub, uPortCount);
            }
        }

    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
        USB_HUB_WV_EVENT_HANDLER,
        "Exiting usbHubPortResetChangeHandler() Function",
        USB_HUB_WV_FILTER);

    /* Return USBHST_SUCCESS. */
    return USBHST_SUCCESS;

    } /* End of usbHubPortResetChangeHandler() */


/***************************************************************************
*
* usbHubPortOverCurrentChangeHandler - Handler for the over current change.
*
* This routine handles the over current change.
*
* RETURNS: USBHST_SUCCESS, USBHST_INVALID_PARAMETER, USBHST_FAILURE If any event
* handler failed.
*
* ERRNO: None
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubPortOverCurrentChangeHandler
    (
    pUSB_HUB_INFO        pHub,
    UINT8                uPortIndex,
    pUSB_HUB_PORT_STATUS pPortStatus
    )
    {
    /* To store the result of submissions */

    USBHST_STATUS Result;

    /* To store the length of the transfers */

    UINT8 uLength = sizeof(USB_HUB_PORT_STATUS);

    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
        USB_HUB_WV_EVENT_HANDLER,
        "Entering usbHubPortOverCurrentChangeHandler() Function",
        USB_HUB_WV_FILTER);

    /* If the pHub is NULL then return USBHST_INVALID_PARAMETER */

    if (NULL == pHub)
        {
        /* Debug Message */
        USB_HUB_ERR("invalid parameter \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }/* End of (NULL == pHub) */

    USB_HUB_DBG("usbHubPortOverCurrentChangeHandler - clear hub %p port %d OC\n",
                pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

    /*
     * Call HUB_CLEAR_PORT_FEATURE() to clear the C_PORT_OVER_CURRENT.
     * If failed return result.
     * Note: the actual port number is the port index + 1
     */

    Result = USB_HUB_CLEAR_PORT_FEATURE(pHub, uPortIndex, USB_C_PORT_OVER_CURRENT);
    if (USBHST_SUCCESS != Result)
        {
        /* Debug Message */
        USB_HUB_ERR("failed to clear port feature, result=%d \n",
                    Result, 2, 3, 4, 5, 6);

        return Result;

        }/* End of USBHST_SUCCESS != Result */

   /*
    * Get the port status again after clearing the Over Current Change.
    *
    * When there is an Over Current situation, there will be an Over Current
    * Change event reported through the Interrupt report pipe; When we get
    * the port status we will find the Over Current status is on.
    *
    * After an Over Current Change event, we need to clear the Over Current
    * Change, but the Over Current status may not immediately be removed (for
    * example, a short current situation may exist for some long time). Once
    * the Over Current status is actually removed, there would be another
    * Over Current Change event, and after clearing that Over Current Change
    * and then checking the status, the Over Current status would then be off.
    * Only at that time we can power on the port again.
    */

   /* Initialize all the fields of the allocated memory */

   OS_MEMSET(pPortStatus, 0, sizeof(USB_HUB_PORT_STATUS));

   /*
    * Call the HUB_GET_PORT_STATUS() function to retrieve the HUB_PORT_STATUS
    * structure. If failed, then return USBHST_FAILURE.
    * Note: actual port number is 1 + the port index
    */

   Result = USB_HUB_GET_PORT_STATUS(pHub,
                                    uPortIndex,
                                    (UINT8 *)(pPortStatus),
                                    &uLength);

   if ((USBHST_SUCCESS != Result) || ( uLength != sizeof(USB_HUB_PORT_STATUS)))
       {
       /* Debug Message */

       USB_HUB_ERR("get port status failed: result=%d, length=0x%X \n",
                   Result, uLength, 3, 4, 5, 6);

       return USBHST_FAILURE;

       }/* End of if (USBHST_SUCCESS... */

   pPortStatus->wPortChange  = OS_UINT16_LE_TO_CPU(pPortStatus->wPortChange);
   pPortStatus->wPortStatus  = OS_UINT16_LE_TO_CPU(pPortStatus->wPortStatus);

   /*
    * Check if over-current bit in wPortStatus is cleared. According to the
    * USB 2.0 specification, we can only power on the port again when the
    * Over Current status is off.
    */

   if (((pPortStatus->wPortStatus) & USB_PORT_OVER_CURRENT_VALUE) == 0)
        {
        /*
         * Call HUB_PowerOnPort() to re-enable the ports with the pHub
         * and uPortIndex.
         */

        USB_HUB_DBG("usbHubPortOverCurrentChangeHandler - power hub %p port %d\n",
                    pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

        Result= usbHubPowerOnPort(pHub, uPortIndex);

        if (USBHST_SUCCESS == Result)
            {
            /* Debug Message */

            USB_HUB_DBG("usbHubPowerOnPort - waiting %d ms for power on hub port\n",
                        (pHub->HubDescriptor.bPwrOn2PwrGood * 2), 2, 3, 4, 5, 6);
            /*
             * There is a minimal time for the system to stabilize on
             * setting a port power. This time is provided in the hub
             * descriptor. we have to wait for this time period.
             */

            /*
             * Note on bPwrOn2PwrGood:
             * Time (in 2 ms intervals) from the time the power-on
             * sequence begins on a port until power is good on that
             * port. The USB System Software uses this value to
             * determine how long to wait before accessing a
             * powered-on port.
             */

            OSS_THREAD_SLEEP ((UINT32)(pHub->HubDescriptor.bPwrOn2PwrGood * 2)) ;
            } /* End of if (USBHST_SUCCESS==Result) */

        /* WindView Instrumentation */
        USB_HUB_LOG_EVENT(
            USB_HUB_WV_EVENT_HANDLER,
            "Exiting usbHubPortOverCurrentChangeHandler() Function",
            USB_HUB_WV_FILTER);
       }

    /* Return result of the HUB_PowerOnPort() function call. */
    return Result;

    } /* End of usbHubPortOverCurrentChangeHandler () */

/*******************************************************************************
*
* usbHubPortLinkStateChangeHandler - handle the port link state change
*
* This routine handles the port link state change.
*
* RETURNS: USBHST_SUCCESS, USBHST_INVALID_PARAMETER, USBHST_FAILURE If any event
* handler failed.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubPortLinkStateChangeHandler
    (
    pUSB_HUB_INFO           pHub,
    UINT8                   uPortIndex,
    pUSB_HUB_PORT_STATUS    pPortStatus
    )
    {
    /* To store the result of submissions */

    USBHST_STATUS Result;

    /* To Store the port information */

    pUSB_HUB_PORT_INFO pPort = NULL;

    /* Port Link State */

    UINT8 uPortLinkState;

    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
        USB_HUB_WV_EVENT_HANDLER,
        "Entering usbHubPortLinkStateChangeHandler() Function",
        USB_HUB_WV_FILTER);

    /* If the pHub is NULL then return USBHST_INVALID_PARAMETER */

    if (NULL == pHub)
        {
        USB_HUB_ERR("invalid parameter \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }

    /*
     * If the uPortIndex greater than pHub::HubDescriptor::bNbrPorts then
     * return USBHST_INVALID_PARAMETER.
     * Note: the actual port number is the port index + 1
     */

    if (uPortIndex >= pHub->HubDescriptor.bNbrPorts)
        {
        USB_HUB_ERR("port number %d > max %d \n",
                    uPortIndex,
                    pHub->HubDescriptor.bNbrPorts,
                    3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }

    USB_HUB_WARN("usbHubPortLinkStateChangeHandler - "
                "hub 0x%X port %d link state change happened\n",
                pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

    /*
     * Call HUB_CLEAR_PORT_FEATURE() to clear the C_PORT_LINK_STATE.
     * If failed return result.
     * Note: the actual port number is the port index + 1
     */

    Result = USB_HUB_CLEAR_PORT_FEATURE(pHub,
                                        uPortIndex,
                                        USB3_HUB_FSEL_C_PORT_LINK_STATE);
    if (USBHST_SUCCESS != Result)
        {
        USB_HUB_ERR("failed to clear port C_PORT_LINK_STATE feature for "
                    "hub 0x%X port %d link state change happened\n",
                    pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

        return Result;
        }

    /* Copy the pPort value */

    pPort = pHub->pPortList[uPortIndex];

    /* Get the link state */

    uPortLinkState = (UINT8)USB3_HUB_STS_PORT_PLS_GET(pPortStatus->wPortStatus);

    /*
     * We currently only support the port in U0 link state.
     * When we support USB Link Power Management, we may need
     * to change this logic to allow other link states.
     */

    if ((pPort != NULL) &&
        (pPort->uDeviceHandle != 0) &&
        (uPortLinkState != USB3_PORT_PLS_U0))
        {
        pPort->uConnectRetry--;

        USB_HUB_WARN("Hub 0x%x Port %d uConnectRetry %d (link state=%d)\n",
            pHub->uDeviceHandle,
            uPortIndex,
            pPort->uConnectRetry, uPortLinkState, 5, 6);

        if (pPort->uConnectRetry == 0)
            {
            USB_HUB_WARN("Removing Hub 0x%x Port %d completely\n",
                pHub->uDeviceHandle,
                uPortIndex, 3, 4, 5, 6);

            usbHubRemoveDevice(pHub, uPortIndex);
            }
        else
            {
            USB_HUB_WARN("Retry Hub 0x%x Port %d connection by Warm Reset\n",
                pHub->uDeviceHandle,
                uPortIndex, 3, 4, 5, 6);

            g_usbHstFunctionList.UsbdToHubFunctionList.removeDevice(pPort->uDeviceHandle);

            Result = usbHubRetryDeviceConfiguration(pHub, uPortIndex);
            }
        }

    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
        USB_HUB_WV_EVENT_HANDLER,
        "Exiting usbHubPortLinkStateChangeHandler() Function",
        USB_HUB_WV_FILTER);

    return Result;
    }

/*******************************************************************************
*
* usbHubPortBhResetChangeHandler - handle the port BH reset change
*
* This routine handles the port BH reset change.
*
* RETURNS: USBHST_SUCCESS, USBHST_INVALID_PARAMETER, USBHST_FAILURE If any event
* handler failed.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubPortBhResetChangeHandler
    (
    pUSB_HUB_INFO           pHub,
    UINT8                   uPortIndex,
    pUSB_HUB_PORT_STATUS    pPortStatus
    )
    {
    /* To store the result of submissions */

    USBHST_STATUS Result;

    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
        USB_HUB_WV_EVENT_HANDLER,
        "Entering usbHubPortBhResetChangeHandler() Function",
        USB_HUB_WV_FILTER);

    /* If the pHub is NULL then return USBHST_INVALID_PARAMETER */

    if (NULL == pHub)
        {
        USB_HUB_ERR("invalid parameter \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }

    /*
     * If the uPortIndex greater than pHub::HubDescriptor::bNbrPorts then
     * return USBHST_INVALID_PARAMETER.
     * Note: the actual port number is the port index + 1
     */

    if (uPortIndex >= pHub->HubDescriptor.bNbrPorts)
        {
        USB_HUB_ERR("port number %d > max %d \n",
                    uPortIndex,
                    pHub->HubDescriptor.bNbrPorts,
                    3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }

    USB_HUB_WARN("usbHubPortBhResetChangeHandler - "
                "hub 0x%X port %d BH reset change happened\n",
                pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

    /*
     * Call HUB_CLEAR_PORT_FEATURE() to clear the C_BH_PORT_RESET.
     * If failed return result.
     * Note: the actual port number is the port index + 1
     */

    Result = USB_HUB_CLEAR_PORT_FEATURE(pHub,
                                        uPortIndex,
                                        USB3_HUB_FSEL_C_BH_PORT_RESET);
    if (USBHST_SUCCESS != Result)
        {
        USB_HUB_ERR("failed to clear port C_BH_PORT_RESET feature for "
                    "hub 0x%X port %d BH reset change happened\n",
                    pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

        return Result;

        }

    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
        USB_HUB_WV_EVENT_HANDLER,
        "Exiting usbHubPortBhResetChangeHandler() Function",
        USB_HUB_WV_FILTER);

    return USBHST_SUCCESS;
    }

/*******************************************************************************
*
* usbHubPortConfigErrorChangeHandler - handle the port config error change
*
* This routine handles the port config error change.
*
* RETURNS: USBHST_SUCCESS, USBHST_INVALID_PARAMETER, USBHST_FAILURE If any event
* handler failed.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbHubPortConfigErrorChangeHandler
    (
    pUSB_HUB_INFO           pHub,
    UINT8                   uPortIndex,
    pUSB_HUB_PORT_STATUS    pPortStatus
    )
    {
    /* To store the result of submissions */

    USBHST_STATUS Result;

    /* WindView Instrumentation */
    USB_HUB_LOG_EVENT(
        USB_HUB_WV_EVENT_HANDLER,
        "Entering usbHubPortConfigErrorChangeHandler() Function",
        USB_HUB_WV_FILTER);

    /* If the pHub is NULL then return USBHST_INVALID_PARAMETER */

    if (NULL == pHub)
        {
        USB_HUB_ERR("invalid parameter \n",
                    1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }

    /*
     * If the uPortIndex greater than pHub::HubDescriptor::bNbrPorts then
     * return USBHST_INVALID_PARAMETER.
     * Note: the actual port number is the port index + 1
     */

    if (uPortIndex >= pHub->HubDescriptor.bNbrPorts)
        {
        USB_HUB_ERR("port number %d > max %d \n",
                    uPortIndex,
                    pHub->HubDescriptor.bNbrPorts,
                    3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;

        }

    USB_HUB_WARN("usbHubPortConfigErrorChangeHandler - "
                "hub 0x%X port %d config error change happened\n",
                pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

    /*
     * Call HUB_CLEAR_PORT_FEATURE() to clear the C_PORT_CONFIG_ERROR.
     * If failed return result.
     * Note: the actual port number is the port index + 1
     */

    Result = USB_HUB_CLEAR_PORT_FEATURE(pHub,
                                        uPortIndex,
                                        USB3_HUB_FSEL_C_PORT_CONFIG_ERROR);
    if (USBHST_SUCCESS != Result)
        {
        USB_HUB_ERR("failed to clear port C_PORT_CONFIG_ERROR feature for "
                    "hub 0x%X port %d config error change happened\n",
                    pHub->uDeviceHandle, uPortIndex, 3, 4, 5, 6);

        return Result;

        }

    /* WindView Instrumentation */

    USB_HUB_LOG_EVENT(
        USB_HUB_WV_EVENT_HANDLER,
        "Exiting usbHubPortConfigErrorChangeHandler() Function",
        USB_HUB_WV_FILTER);

    return USBHST_SUCCESS;
    }

/**************************** End of File HUB_PortEventHandler.c **************/

