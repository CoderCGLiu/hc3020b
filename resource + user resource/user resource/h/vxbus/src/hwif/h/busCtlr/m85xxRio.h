/* m85xxRio.h - support for m85xx RapidIO host bridge */

/* Copyright (c) 2005-2006 Wind River Systems, Inc. */

/* 
modification history
--------------------
01c,03aug07,dtr  Allow src compiling of driver.
01b,15feb06,dtr  SPR#117948
01a,05Oct05,tor  created
*/

/* includes */

#ifndef __INC_m85xxVxBusRioH
#define __INC_m85xxVxBusRioH


#ifndef VUINT32

#ifdef  _ASMLANGUAGE
#define CAST(x)
#else /* _ASMLANGUAGE */
typedef volatile UCHAR VCHAR;   /* shorthand for volatile UCHAR */
typedef volatile INT32 VINT32; /* volatile unsigned word */
typedef volatile INT16 VINT16; /* volatile unsigned halfword */
typedef volatile INT8 VINT8;   /* volatile unsigned byte */
typedef volatile UINT32 VUINT32; /* volatile unsigned word */
typedef volatile UINT16 VUINT16; /* volatile unsigned halfword */
typedef volatile UINT8 VUINT8;   /* volatile unsigned byte */
#define CAST(x) (x)
#endif  /* _ASMLANGUAGE */

#endif 

#ifndef _ASM_LANGUAGE
typedef struct
	{
	UINT32 deviceID;
	UINT32 deviceInfo;
	UINT32 assemblyID;
	UINT32 assemblyInfo;
	UINT32 processorFeatures;
	UINT32 switchPortFeatures;
	UINT32 sourceOperations;
	UINT32 destOperations;
	UINT32 reserved1[8];
	UINT32 msr;
	UINT32 pwdcsr;
	UINT32 reserved2;
	UINT32 pellccsr;
	UINT32 reserved3[3];
	UINT32 lcsba1csr;
	UINT32 bdidcsr;
	UINT32 reserved4;
	UINT32 hbdidlcsr;
	UINT32 ctcsr;
	} RAPIDIOCAR;
	
typedef union pefcar_desc 
    {
    struct
	{
	UINT32 bridge:1;
	UINT32 memory:1;
	UINT32 proc:1;
	UINT32 sw:1;
	UINT32 reserved1:4;
	UINT32 mailbox:4;
	UINT32 doorbell:1;
	UINT32 reserved2:14;
	UINT32 ctls:1;
	UINT32 ef:1;
	UINT32 eaf:3;
	} desc;
    UINT32 word;
    }PEFCAR_DESC;

#endif

#define M85XXRIO_ATMU_CHANNELS	15



#define M85XXRIO_SEGS_PER_CHANNEL	4

/* DeviceID not assigned to channel */

#define RIO_CHANNEL_GENERIC		0x00000001
#define RIO_CHANNEL_AVAILABLE		NULL

/* initialization states */

#define RIO_NO_HARDWARE			0
#define RIO_NOT_INITIALIZED		1
#define RIO_BRIDGE_ININTIALIZED		2
#define RIO_BUS_CONFIG_IN_PROGRESS	3
#define RIO_BUS_CONFIG_DONE		4
#define RIO_WORKING			5

#define RIO_HOST_ID_OFFSET  16

#define SERIAL_RAPIDIO /* Default for now*/

#define MAX_NUMBER_RIO_TARGETS 10

#define SRIO_STATUS_CHECK 0x2
#define PRIO_STATUS_CHECK 0xa
#define PGCCSR_HOST 0x80000000		
#define PGCCSR_MASTER 0x40000000	
/* RapidIO base address */
#define RAPIDIO_BA   0xC0000

/* Device identity capability register R 0x0003_0002 */
#define M85XXRIO_DIDCAR(base) (CAST(VUINT32 *)(((char*)base) +  0x0000 ))

/* Device information capability register R 0x8060_0010 */
#define M85XXRIO_DICAR(base) (CAST(VUINT32 *)(((char*)base) +  0x0004 ))
#define M85XXRIO_DICAR_OFFSET 0x4

/* Assembly identity capability register Special 1 0x0000_0000 */
#define M85XXRIO_AIDCAR(base) (CAST(VUINT32 *)(((char*)base) +  0x0008))

/* Assembly information capability register Special1 0x0000_0100 */
#define M85XXRIO_AICAR(base) (CAST(VUINT32 *)(((char*)base) +  0x000C))

/* Processing element features capability register R 0xE088_0009 */
#define M85XXRIO_PEFCAR(base) (CAST(VUINT32 *)(((char*)base) +  0x0010))
#define M85XXRIO_PEFCAR_OFFSET 0x10
/* Switch port information capability register R 0x0000_0100 */
#define M85XXRIO_SPICAR(base) (CAST(VUINT32 *)(((char*)base) +  0x0014))

/* Source operations capability register R 0x0600_FCF0 */
#define M85XXRIO_SOCAR(base) (CAST(VUINT32 *)(((char*)base) +  0x0018))

/* Destination operations capability register R 0x0600_FCF4 */
#define M85XXRIO_DOCAR(base) (CAST(VUINT32 *)(((char*)base) +  0x0001C))


/* Mailbox command and status register R 0x0000_0000 */
#define M85XXRIO_MSR(base) (CAST(VUINT32 *)(((char*)base) +  0x0040 ))

/* Port-write and doorbell command and status register R 0x0000_0020 */
#define M85XXRIO_PWDCSR(base) (CAST(VUINT32 *)(((char*)base) +  0x00044 ))

/* Processing element logical layer control command and status register R 
 * 0x0000_0001 */
#define M85XXRIO_PELLCCSR(base) (CAST(VUINT32 *)(((char*)base) +  0x0004C ))

/* Local configuration space base address 1 command and status register R/W
 * 0x0000_0000 */
#define M85XXRIO_LCSBA1CSR(base) (CAST(VUINT32 *)(((char*)base) +  0x0005C ))
#define M85XXRIO_LCSBA1CSR_OFFSET 0x5C

/* Base device ID command and status register R/W 0x00[cfg]_0000 */
#define M85XXRIO_BDIDCSR(base) (CAST(VUINT32 *)(((char*)base) +  0x00060 ))
#define M85XXRIO_BDIDCSR_OFFSET 0x60
/* Host base device ID lock command and status register Special1 
 * 0x0000_FFFF */
#define M85XXRIO_HBDIDLCSR(base) (CAST(VUINT32 *)(((char*)base) +  0x00068 ))
#define M85XXRIO_HBDIDLCSR_OFFSET 0x68

/* Component tag command and status register R/W 0x0000_0000 */
#define M85XXRIO_CTCSR(base) (CAST(VUINT32 *)(((char*)base) +  0x0006C ))


/* 8/16 LP-LVDS port maintenance block header 0 command and status register 
 * R 0x0000_0002 */
#define M85XXRIO_PMBH0CSR(base) (CAST(VUINT32 *)(((char*)base) +  0x00100 ))

/* Port link time-out control command and status register R/W 0xFFFF_FF00 */ 
#define M85XXRIO_PLTOCCSR(base) (CAST(VUINT32 *)(((char*)base) +  0x00120 ))

/* Port response time-out control command and status register R/W 
 * 0xFFFF_FF00 */
#define M85XXRIO_PRTOCCSR(base) (CAST(VUINT32 *)(((char*)base) +  0x00124 ))

 /* Port general control command and status register R/W 0x[cfg]000_0000 */
#define M85XXRIO_PGCCSR(base) (CAST(VUINT32 *)(((char*)base) +  0x0013C ))
#define M85XXRIO_PGCCSR_OFFSET 0x13c
/* Port link maintenance request command and status register R/W 
 * 0x0000_0000 */
#define M85XXRIO_PLMREQCSR(base) (CAST(VUINT32 *)(((char*)base) +  0x00140 ))

/* Port link maintenance response command and status register R 0x0000_0000 */
#define M85XXRIO_PLMRESPCSR(base) (CAST(VUINT32 *)(((char*)base) +  0x00144 ))

/* Port local ackID status command and status register Special1 0x0000_0000 */
#define M85XXRIO_PLASCSR(base) (CAST(VUINT32 *)(((char*)base) +  0x00148 ))

/* Port error and status command and status register Special1 0x0000_0000 */
#define M85XXRIO_PESCSR(base) (CAST(VUINT32 *)(((char*)base) +  0x00158 ))
#define M85XXRIO_PESCSR_OFFSET 0x0158

	

/* Port control command and status register Special1 0x4400_0000 */
#define M85XXRIO_PCCSR(base) (CAST(VUINT32 *)(((char*)base) +  0x0015C ))
    
    /* RapidIO Implementation Registers */

/* Configuration register R/W 0x0000_0001 */
#define M85XXRIO_CR(base) (CAST(VUINT32 *)(((char*)base) +  0x10000 ))

/* Port configuration register R/W 0x0000_0010 */    
#define M85XXRIO_PCR(base) (CAST(VUINT32 *)(((char*)base) +  0x10010 ))

/* Port error injection register R/W 0x0000_0000 */    
#define M85XXRIO_PEIR(base) (CAST(VUINT32 *)(((char*)base) +  0x10014 ))
    

    /* RapidIO ATMU Registers */

/* RapidIO outbound window translation address register 0 R/W 0x0000_0000 */
#define M85XXRIO_ROWTAR0(base) (CAST(VUINT32 *)(((char*)base) +  0x10C00 ))

/* RapidIO outbound window attributes register 0 R/W 0x8004_401F */
#define M85XXRIO_ROWAR0(base) (CAST(VUINT32 *)(((char*)base) +  0x10C10 ))


/* RapidIO outbound window translation address register 1 R/W 0x0000_0000 */
#define M85XXRIO_ROWTARn(base,n) \
    (CAST(VUINT32 *)(((char*)base) + 0x10C00 + ((n)*0x20)))

#define M85XXRIO_ROWTAR_OFFSET(n) \
			(0x10C00 + ((n)*0x20))

/* RapidIO outbound window base address register 1 R/W 0x0000_0000 */
#define M85XXRIO_ROWBARn(base,n) \
    (CAST(VUINT32 *)(((char*)base) + 0x10C08 + ((n)*0x20)))

#define M85XXRIO_ROWBAR_OFFSET(n) \
		(0x10C08 + ((n)*0x20))

/* RapidIO outbound window attributes register 1 R/W 0x0004_401F */
#define M85XXRIO_ROWARn(base,n) \
    (CAST(VUINT32 *)(((char*)base) + 0x10C10 + ((n)*0x20)))

#define M85XXRIO_ROWAR_OFFSET(n) \
			(0x10C10 + ((n)*0x20))

#define M85XX_RAPIDIO_ROWS1R18n(base,n) \
	(CAST(VUINT32 *)(((char*)base) + 0x10C14 + ((n)*0x20)))

#define M85XXRIO_ROWS1R18_OFFSET(n) \
				(0x10C14 + ((n)*0x20))
#define M85XXRIO_ROWS2R18_OFFSET(n) \
				(0x10C18 + ((n)*0x20))
#define M85XXRIO_ROWS3R18_OFFSET(n) \
				(0x10C1C + ((n)*0x20))

#define M85XX_RAPIDIO_ROWS2R18n(base,n) \
	(CAST(VUINT32 *)(((char*)base) + 0x10C18 + ((n)*0x20)))

#define M85XX_RAPIDIO_ROWS3R18n(base,n) \
	(CAST(VUINT32 *)(((char*)base) + 0x10C1C + ((n)*0x20)))
    
/* RapidIO inbound window translation address register n R/W 0x0000_0000 */
#define M85XXRIO_RIWTARn(base,n) \
    (CAST(VUINT32 *)(((char*)base) + 0x10D60 + (0x80 - (n*0x20))))

/* Shift Inbound address down by 12 bits  for 32 bit address */
#define M85XXRIO_RIWTAR_TRAD(adrs) (adrs >> 12)

/* RapidIO inbound window base address register 4 R/W 0x0000_0000 */
#define M85XXRIO_RIWBARn(base,n) \
    (CAST(VUINT32 *)(((char*)base) + 0x10D68 + (0x80 - (n*0x20))))

#define M85XXRIO_RIWBAR_BADDR(adrs) (adrs >> 12)
#define M85XXRIO_RIWBAR_BEXAD(adrs) (adrs << 22)


/* RapidIO inbound window attributes register 4 R/W 0x0004_401F */
#define M85XXRIO_RIWARn(base,n) \
    (CAST(VUINT32 *)(((char*)base) + 0x10D70 + (0x80 - (n*0x20))))

#define M85XXRIO_RxWAR_EN  0x80000000
#define M85XXRIO_RIWAR_TGINT_MSK 0xf
#define M85XXRIO_RIWAR_PCI   0x00
#define M85XXRIO_RIWAR_PCI2  0x01
#define M85XXRIO_RIWAR_PCIEX 0x02

#define M85XXRIO_RIWAR_LOCAL 0xf
#define M85XXRIO_RIWAR_TGINT(val) (val << 20)
#define M85XXRIO_RxWAR_TYPE_MSK 0xf
#define M85XXRIO_RxWAR_TYPE_READ(val) (val << 16)
#define M85XXRIO_RxWAR_TYPE_WRITE(val) (val << 12)

#define M85XXRIO_RIWAR_IO_TYPE_R 4
#define M85XXRIO_RIWAR_IO_TYPE_W 4

#define M85XXRIO_RIWAR_LOCAL_R_NO_SNOOP 4
#define M85XXRIO_RIWAR_LOCAL_R_SNOOP_CORE 5
#define M85XXRIO_RIWAR_LOCAL_R_UNLOCK_L2 7

#define M85XXRIO_RIWAR_LOCAL_W_NO_SNOOP 4
#define M85XXRIO_RIWAR_LOCAL_W_SNOOP_CORE 5
#define M85XXRIO_RIWAR_LOCAL_W_ALLOC_L2 6
#define M85XXRIO_RIWAR_LOCAL_W_LOCK_L2 7

#define M85XXRIO_ROWAR_RDTYPE_IO_READ_HOME 0x2
#define M85XXRIO_ROWAR_RDTYPE_NREAD      0x4
#define M85XXRIO_ROWAR_RDTYPE_MAINT_READ 0x7
#define M85XXRIO_ROWAR_RDTYPE_ATOMIC_INC 0xc
#define M85XXRIO_ROWAR_RDTYPE_ATOMIC_DEC 0xd
#define M85XXRIO_ROWAR_RDTYPE_ATOMIC_SET 0xe
#define M85XXRIO_ROWAR_RDTYPE_ATOMIC_CLR 0xf

#define M85XXRIO_ROWAR_WRTYPE_FLUSH      0x1
#define M85XXRIO_ROWAR_WRTYPE_SWRITE     0x3
#define M85XXRIO_ROWAR_WRTYPE_NWRITE     0x4
#define M85XXRIO_ROWAR_WRTYPE_NWRITE_R   0x5
#define M85XXRIO_ROWAR_WRTYPE_MAINT_WRITE 0x7


/* Size is 2 power of n+1 ie 4k == 2 power of 10 == size of 11 */
#define M85XXRIO_RxWAR_SIZE_MSK 0x3f
#define M85XXRIO_RxWAR_SIZE_1MB 21
#define M85XXRIO_RxWAR_SIZE_4MB 23
#define M85XXRIO_RxWAR_SIZE_16MB 25


    /* RapidIO Error Management Registers */

/* 
#define M85XXRIO_PNFEDR 0x00E00 
?Port notification/fatal error detect register Special1 0x0000_0000 16.3.2.3.1/16-43
#define M85XXRIO_PNFEDIR 0x00E04 
Port notification/fatal error detect disable
register
R/W 0x0000_0000 16.3.2.3.2/16-46
#define M85XXRIO_PNFEIER 0x00E08 
?Port notification/fatal error interrupt enable
register
R/W 0x0000_0000 16.3.2.3.3/16-49
#define M85XXRIO_PECSR 0x00E0C
?Port error capture status register Special1 0x0000_0000 16.3.2.3.4/16-52
#define M85XXRIO_EPCR0 0x00E10
?Error packet capture register 0 Special1 0x0000_0000 16.3.2.3.5/16-52
#define M85XXRIO_EPCR1 0x00E14
?Error packet capture register 1 Special1 0x0000_0000 16.3.2.3.6/16-53
#define M85XXRIO_EPCR2 0x00E18 
?Error packet capture register 2 Special1 0x0000_0000 16.3.2.3.16/16-58
#define M85XXRIO_PREDR 0x00E20 
?Port recoverable error detect register Special1 0x0000_0000 16.3.2.3.23/16-61
#define M85XXRIO_PERTR 0x00E28 
?Port error recovery threshold register Special1 0x00FF_0000 16.3.2.3.24/16-63
#define M85XXRIO_PRTR 0x00E2C ?Port retry threshold register Special1 0x00FF_0000 16.3.2.3.25/16-64
*/



    /* RapidIO Message Unit */

    /* RapidIO Outbound Message Registers */

/* Outbound mode register R/W 0x0000_0000 */
#define M85XXRIO_OMR(base) (CAST(VUINT32 *)(((char*)base) + 0x11000)) 

/* Outbound status register Special1 0x0000_0000 */
#define M85XXRIO_OSR(base) (CAST(VUINT32 *)(((char*)base) + 0x11004))

/* Outbound descriptor queue dequeue pointer address register R/W 
 * 0x0000_0000 */
#define M85XXRIO_ODQDPAR(base) (CAST(VUINT32 *)(((char*)base) + 0x1100C)) 

/* Outbound source address register R/W 0x0000_0000 */
#define M85XXRIO_OSAR(base) (CAST(VUINT32 *)(((char*)base) + 0x11014))

/* Outbound destination port register R/W 0x0000_0000 */
#define M85XXRIO_ODPR(base) (CAST(VUINT32 *)(((char*)base) + 0x11018))

/* Outbound destination attributes register R/W 0x0006_0000 */
#define M85XXRIO_ODATR(base) (CAST(VUINT32 *)(((char*)base) + 0x1101C))

/* Outbound double-word count register R/W 0x0000_0000 */
#define M85XXRIO_ODCR(base) (CAST(VUINT32 *)(((char*)base) + 0x11020))

/* Outbound descriptor queue enqueue pointer address register R/W 
 * 0x0000_0000 */
#define M85XXRIO_ODQEPAR(base) (CAST(VUINT32 *)(((char*)base) + 0x11028)) 

    /* RapidIO Inbound Message Registers */

/* Inbound mailbox mode register R/W 0x0000_0000 */
#define M85XXRIO_IMR(base) (CAST(VUINT32 *)(((char*)base) + 0x11060))

/* Inbound mailbox status register Special1 0x0000_0000 */
#define M85XXRIO_ISR(base) (CAST(VUINT32 *)(((char*)base) + 0x11064))

/*Inbound frame queue dequeue pointer address register R/W 0x0000_0000 */
#define M85XXRIO_IFQDPAR(base) (CAST(VUINT32 *)(((char*)base) + 0x0106C)) 

/* Inbound frame queue enqueue pointer address register R/W 0x0000_0000 */
#define M85XXRIO_IFQEPAR(base) (CAST(VUINT32 *)(((char*)base) + 0x11074))


    /* RapidIO Doorbell Registers */

/* Inbound Doorbell mode register R/W 0x0000_0000 */
#define M85XXRIO_IDMR(base) (CAST(VUINT32 *)(((char*)base) +  0x13460  ))

/* Inbound Doorbell status register Special1 0x0000_0000 */
#define M85XXRIO_IDSR(base) (CAST(VUINT32 *)(((char*)base) +  0x13464 ))

/* Inbound Doorbell queue dequeue pointer address register R/W 0x0000_0000 */
#define M85XXRIO_IDQDPAR(base) (CAST(VUINT32 *)(((char*)base) +  0x1346C ))

/* Inbound Doorbell queue enqueue pointer address register R/W 0x0000_0000 */
#define M85XXRIO_IDQEPAR(base) (CAST(VUINT32 *)(((char*)base) +  0x13474 ))

    /* RapidIO Port-Write Registers */
/* Port-write mode register R/W 0x0000_0000 */
#define M85XXRIO_IPWMR(base) (CAST(VUINT32 *)(((char*)base) +  0x134E0 ))

/* Port-write status register Special1 0x0000_0000 */
#define M85XXRIO_IPWSR(base) (CAST(VUINT32 *)(((char*)base) +  0x134E4 ))

/* Port-write queue base address register R/W 0x0000_0000 */
#define M85XXRIO_IPWQBAR(base) (CAST(VUINT32 *)(((char*)base) +  0x134EC ))


/* oubound Doorbell Registers */
#define M85XXRIO_ODMR(base) (CAST(VUINT32 *)(((char*)base) +  0x13400))
#define M85XXRIO_ODSR(base) (CAST(VUINT32 *)(((char*)base) +  0x13404))
#define M85XXRIO_ODDPR(base) (CAST(VUINT32 *)(((char*)base) +  0x13418))
#define M85XXRIO_ODDATR(base) (CAST(VUINT32 *)(((char*)base) +  0x1341C))
#define M85XXRIO_ODRETCR(base) (CAST(VUINT32 *)(((char*)base) +  0x1342C))
    

#endif /* __INC_m85xxRioH */
