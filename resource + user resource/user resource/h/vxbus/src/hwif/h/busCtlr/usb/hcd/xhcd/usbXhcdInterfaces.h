/* usbXhcdInterfaces.h - USB XHCI Driver Interfaces Definitions */

/*
 * Copyright (c) 2011, 2012, 2015 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01f,03apr15,j_x  add post reset hook. (US54139)
01e,19oct12,w_x  Add polling mode support
01d,17oct12,w_x  Fix compiler warnings (WIND00370525)
01c,20sep12,w_x  Use separate default pipe for USB2 and USB3 bus (WIND00377413)
01b,04sep12,w_x  Address review comments and fix some defects (WIND00370637)
01a,09may11,w_x  written
*/

#ifndef __INCusbXhcdInterfaceh
#define __INCusbXhcdInterfaceh

#ifdef __cplusplus
extern "C" {
#endif

#include <vxWorks.h>
#include <spinLockLib.h>
#include <string.h>
#include <rebootLib.h>
#include <hookLib.h>
#include <hwif/vxbus/vxBus.h>
#include <hwif/vxbus/hwConf.h>
#include <hwif/vxbus/vxbPciLib.h>
#include <hwif/util/vxbParamSys.h>
#include <hwif/util/vxbDmaBufLib.h>
#if defined (_WRS_CONFIG_PWR_MGMT) && defined (_WRS_ARCH_HAS_DEV_PWR_MGMT)
#include <hwif/pwr/pwrDeviceLib.h>
#endif /* (_WRS_CONFIG_PWR_MGMT) && (_WRS_ARCH_HAS_DEV_PWR_MGMT) */
#include <drv/pci/pciConfigLib.h>
#include <usb/usb.h>
#include <usb/usbHst.h>
#include <usb/usbd.h>
#include "usbXhcdRegisters.h"
#include "usbXhcdStructures.h"

IMPORT char * gUsbXhcdSpeedStr[];
IMPORT char * gUsbXhcdLinkStateStr[];

#undef _WRS_CONFIG_USB_XHCD_USE_EVENT_DATA_TRB
#undef  _WRS_CONFIG_USB_XHCD_ENABLE_MSI_INTR
#undef  _WRS_CONFIG_USB_XHCD_ENABLE_MSIX_INTR
#define  _WRS_CONFIG_USB_XHCD_TRANS_ERR_RETRIES 0
#define  _WRS_CONFIG_USB_XHCD_CMD_RETRIES 1
#undef  _WRS_CONFIG_USB_XHCD_POLLING_MODE
#define  _WRS_CONFIG_USB_XHCD_POLLING_INTERVAL (1)

#define USB_XHCD_INT_THREAD_PRIORITY    90
#define USB_XHCD_MAGIC_ALIVE    0xbeefbeef
#define USB_XHCD_MAGIC_DEAD     0xdeadbeef

#define USB_XHCD_MUTEX_CREATION_OPTS   (SEM_Q_PRIORITY |       \
                                        SEM_INVERSION_SAFE |   \
                                        SEM_DELETE_SAFE)
#define USB_XHCD_DEFAULT_CMD_WAIT_TIME      (60 * sysClkRateGet())

#define USB_XHCD_CTRL_MAX_DATA_SIZE (1 << 16)    /* Maximum size ctrl
                                                    DATA Transaction */

#define USB_XHCD_MAX_PORTSC_RETRIES (100) /* Maximum ms retries PORTSC update */

/* Macro used for swapping the 32 bit values of HC data strcutures */

#define USB_XHCD_SWAP_DESC_DATA32(pHCDData,VALUE)                  \
        ((((pHCDData)->pDescSwap32) == NULL) ? ((UINT32)(VALUE)) : \
                    (*(pHCDData->pDescSwap32))((UINT32)(VALUE)))

/* Macro used for swapping the 32 bit values of USB formated data */

#define USB_XHCD_SWAP_USB_DATA32(pHCDData,VALUE)                   \
        ((((pHCDData)->pUsbSwap32) == NULL) ? ((UINT32)(VALUE)) :  \
                    (*((pHCDData)->pUsbSwap32))((UINT32)(VALUE)))

/* Macro used for swapping the 64 bit values of HC data strcutures */

#define USB_XHCD_SWAP_DESC_DATA64(pHCDData,VALUE)                  \
        ((((pHCDData)->pDescSwap64) == NULL) ? ((UINT64)(VALUE)) : \
                    (*(pHCDData->pDescSwap64))((UINT64)(VALUE)))

/* Macro used for swapping the 64 bit values of USB formated data */

#define USB_XHCD_SWAP_USB_DATA64(pHCDData,VALUE)                   \
        ((((pHCDData)->pUsbSwap64) == NULL) ? ((UINT64)(VALUE)) :  \
                    (*((pHCDData)->pUsbSwap64))((UINT64)(VALUE)))
                                
/* Get the low 32 bit of the BUS address */

#define USB_XHCD_BUS_ADDR_LO32(VALUE)    \
                    ((UINT32)((UINT64)(VALUE)))

/* Get the high 32 bit of the BUS address */

#define USB_XHCD_BUS_ADDR_HI32(VALUE)    \
                    ((UINT32)((UINT64)(VALUE) >> 32))

/* Get the low 32 bit of the descriptor BUS address */

#define USB_XHCD_DESC_LO32(DESC)    \
            USB_XHCD_BUS_ADDR_LO32((DESC)->dmaMapId->fragList[0].frag)

/* Get the high 32 bit of the descriptor BUS address */

#define USB_XHCD_DESC_HI32(DESC)    \
            USB_XHCD_BUS_ADDR_HI32((DESC)->dmaMapId->fragList[0].frag)

/*
 * Device Context and Input Context Wrapper
 *
 * We wrap the Device Context and Input Context Wrapper raw bytes, and use
 * the macros defined below to cast from the proper offset in the raw bytes
 * to the actual context data structures based on the context type.
 */

typedef struct usbXhcdCtxWrapper
    {
    UINT8           uCtxType;       /* Type of the context area */
    UINT8 *         pCtxArea;       /* Raw bytes of the context area */
    UINT64          uCtxBusAddr;    /* Bus Address of the context area */
    VXB_DMA_MAP_ID  dmaMapId;       /* DMA map for context area */
    }USB_XHCD_CTX_WRAPPER, *pUSB_XHCD_CTX_WRAPPER;

/*
 * 4.9.2.1 Segmented Rings
 *
 * The Link TRB provides support for non-contiguous TRB Rings. For instance,
 * if contiguous Pages of memory cannot be allocated by system software to
 * form a large TRB Ring, then Link TRBs can be used to tie together multiple
 * memory Pages to form a single large Transfer Ring. A non-contiguous TRB
 * Ring is composed of Ring Segments. A Ring Segment is a contiguous block of
 * physical memory. The Link TRB provides a 64-bit pointer which points to
 * the next segment of a ring. If the ring is comprised of only a single
 * segment then the only Link TRB points to the beginning of the ring.
 */

/* Segment strcuture */

typedef struct usbXhcdSegment
    {
    pUSB_XHCI_TRB           pTRBs;          /* Contiguous TRBs of this segment */
    UINT64                  uHeadTRB;       /* Segment first TRB Bus Address */
    UINT64                  uTailTRB;       /* Segment last TRB Bus Address */
    size_t                  uNumTRBs;       /* Number of TRBs in this segment */
    struct usbXhcdSegment * pNextSeg;       /* Pointer to the next segment */
    VXB_DMA_MAP_ID          dmaMapId;       /* DMA map for TRBs of this segment */
    }USB_XHCD_SEGMENT, * pUSB_XHCD_SEGMENT;

/* TRB size */

#define USB_XHCI_TRB_SIZE       16

/* Number of TRBs in one segment */

#define USB_XHCD_SEG_MAX_TRBS   256 

/* Number of Segments in one ISOCHRONOUS ring */

#define USB_XHCD_ISOCHRONOUS_RING_MAX_SEGS   256 

/* Number of Segments in one non-ISOCHRONOUS ring */

#define USB_XHCD_NON_ISOCHRONOUS_RING_MAX_SEGS   2 

/* Number of TRBs reserved for command ring */

#define USB_XHCD_CMD_RING_RSVD_TRBS   (USB_XHCD_SEG_MAX_TRBS - 3) 

/* Segment size in bytes */

#define USB_XHCD_SEG_MAX_SIZE   (USB_XHCD_SEG_MAX_TRBS * USB_XHCI_TRB_SIZE) 

/* Segment alignment size in bytes */

#define USB_XHCD_SEG_ALIGNMENT  64 

/* Number of segment in one Command Ring */

#define USB_XHCD_CMD_RING_MAX_SEGS  1

/* Ring structure */

#define USB_XHCD_CMD_RING       0x1
#define USB_XHCD_XFER_RING      0x2
#define USB_XHCD_EVT_RING       0x3

typedef struct usbXhcdRing
    {
    pVOID                   pHCDData;   /* Pointer to the HCD Data */
    pUSB_XHCD_SEGMENT       pHeadSeg;   /* Pointer to the first segment */
    pUSB_XHCI_TRB           pEnqTRB;    /* Pointer to the Enqueue TRB */
    pUSB_XHCD_SEGMENT       pEnqSeg;    /* Pointer to the Enqueue segment */
    pUSB_XHCI_TRB           pDeqTRB;    /* Pointer to the Dequeue TRB */
    pUSB_XHCD_SEGMENT       pDeqSeg;    /* Pointer to the Dequeue segment */
    pUSB_XHCI_ERST_ENTRY    pERST;      /* Pointer to the Head of ERST */
    LIST                    reqList;    /* Request/TD info list */
    UINT32                  uNumEnq;    /* Number of Enqueue on the ring */ 
    UINT32                  uNumDeq;    /* Number of Dequeue on the ring */ 
    UINT32                  uNumSegs;   /* Number of Segments on the ring */ 
    UINT32                  uIntr;      /* Interrupter */
    atomic_t                uNumEvents; /* Event Ring # of events */
    UINT32                  uType;      /* Ring Type */
    UINT8                   uCycle;     /* Cycle bit state */
    }USB_XHCD_RING, * pUSB_XHCD_RING;

/*
 * Command structure 
 *
 * There are two categories of Command TRBs, commands that originated from 
 * or designated to a Device Slot, and commands that are for the xHC itself.
 *
 * We design the command handling as below:
 *
 * 1) There is a single Command Ring for a single xHCI instance, and that 
 * single Command Ring has a single Segment of Command TRBs.
 *
 * 2) During system initialization, an array of USB_XHCD_CMD structures are
 * created, with same number of entries as the number of Command Ring TRBs.
 * We create a 1-1 correspondence between the Command TRBs and the Command
 * structures, so that we can have a simple "offset math" to locate the 
 * Command Structure using the Command TRB address (either CPU Virtual or
 * BUS address).
 *
 * 3) Each USB_XHCD_DEVICE has a list of active commands queued for that 
 * Device Slot, also the xHCI instance (a USB_XHCD_DATA structure instance)
 * has a list of active commands queued for the xHCI instance itself.
 *
 * 4) Each USB_XHCD_DEVICE and the xHCI instance has a binary semaphore to
 * be used as the "Command Completion Notification" semaphore if the command
 * is specified with a non-zero wait time. 
 *
 * 5) When a new command is queued up, it is linked onto "active command
 * list" of the waiting entity. 
 *
 * 6) More than one "active commands" can be posted and linked onto the 
 * "active command list".
 *
 * 7) When a Command TRB completes and, if the command originates from a 
 * Device Slot, it uses the Slot ID field of the TRB to find corresponding  
 * device structure to complete the command; if it originates from Command  
 * TRBs with not Slot ID available, we use the HCD command done semaphore
 * to complete.
 *
 * An alternate way is to create a binary semaphore for each command, but 
 * that may be a big resource requirement.
 */

typedef struct usbXhcdCmd
    {
    NODE                    cmdNode;
    pUSB_XHCI_TRB           pCmdTRB;
    UINT8                   uCmdType;
    UINT8                   uSlotId;
    UINT8                   uCompCode;
    _Vx_ticks_t             waitTime;
    }USB_XHCD_CMD, *pUSB_XHCD_CMD;

#define USB_XHCD_CMD_NODE_TO_CMD(pNode)             \
       ((USB_XHCD_CMD *) ((char *) (pNode) -        \
                     OFFSET(USB_XHCD_CMD, cmdNode)))

/* Stream Info */

typedef struct usbXhcdStreamInfo
    {
    pUSB_XHCD_RING *        ppStreamRings;
    size_t                  uNumStreams;
    pUSB_XHCI_STREAM_CTX    pStreamCtxs;
    UINT64                  uStreamCtxs;
    size_t                  uNumStreamCtxs;    
    VXB_DMA_TAG_ID          ctxDmaTagId; /* DMA TAG ID for Steam Array */
    VXB_DMA_MAP_ID          ctxDmaMapId; /* DMA map for Streams Contexts */
    }USB_XHCD_STREAM_INFO, * pUSB_XHCD_STREAM_INFO;

#define USB_XHCD_EP_HAS_STREAMS         (1 << 0)	

/* Values for uEpCtxState below */

#define USB_XHCD_EP_CTX_STATE_NEW       (1) /* The EP CTX is to be added */
#define USB_XHCD_EP_CTX_STATE_MOD       (2) /* The EP CTX is to be modified */
#define USB_XHCD_EP_CTX_STATE_DEL       (3) /* The EP CTX is to be deleted */
#define USB_XHCD_EP_CTX_STATE_OLD       (4) /* The EP CTX is to be kept old */

/* Pre-define usbXhcdDevice structure */

struct usbXhcdDevice;

/* Endpoint pipe structure */

typedef struct usbXhcdPipe
    {
    NODE    pipeNode;       /* Node in the pipe list */
    LIST    freeReqList;    /* Free Request list */
    LIST    activeReqList;  /* Active Request list */
    LIST    cancelReqList;  /* Cancel Request list */
    MUTEX_HANDLE  pipeMutex;/* Pipe sync mutex */
    UINT8   uEpCtxState; /* Endpoint Context State */
    UINT8   uDevAddr; /* Address of the device holding the EP */
    UINT8   uDevSpeed;/* Speed of the device */
    UINT8   uEpAddr;  /* Address of the EP */
    UINT8   uEpDir;   /* Direction of the EP */
    UINT8   uEpXferType ; /* Endpoint Transfer Type (no direction included) */
    UINT8   uEpState; /* Endpoint State (EP State) */
    UINT8   uMult;    /* Max # of bursts within an Interval this EP supports */
    UINT8   uLSA;     /* Linear Stream Array (LSA) */
    UINT8   uInterval;/* Period between consecutive requests to a USB EP */
    UINT8   uCErr;    /* # of consecutive USB Bus Errors allowed executing a TD */
    UINT8   uEpType ; /* Endpoint Type (EP Type) */
    UINT8   uHID;     /* Host Initiate Disable (HID) */
    UINT8   uMaxBusrtSize; /* Max # of consecutive USB transactions that 
                            * should be executed per scheduling opportunity 
                            */
    UINT16  uMaxPacketSize; /* Max Packet Size in bytes that this EP is
                             * capable of sending or receiving when configured
                             */
    UINT16  uAvrgLen; /* Average TRB Length */ 
    UINT16  uMaxESIT; /* Max Endpoint Service Time Interval Payload */  
    UINT32  uMaxPStreams; /* Max # of Primary Stream IDs this EP supports */
    UINT16  uErrStreamId; /* Stream ID with Error */
    UINT16  uHubInfo; /* Hub Infomation */
    UINT32  uFeatures; /* Any special pipe feature flags */
    UINT32  uSlotId;  /* Slot ID where this pipe exists for */
    UINT32  uEpId;    /* Endpoint ID (DCI) for this pipe */

    size_t   uMaxTransferSize; /* Maxium possible transfer size of this pipe */
    size_t   uMaxNumReqests;   /* Maxium possible transfer requests at a time */
    UINT32   uXferFlags;       /* Any possible flags */
    UINT32   uNumTransErrors;  /* Number of continous errors with recovery */
    VXB_DMA_TAG_ID dataDmaTagId; /* DMA TAG ID used for user data */

    BOOL    bRootHubPipe;  /* Is this root hub pipe */
    UINT32  uValidMaggic;  /* Valid magic number */      
    struct usbXhcdDevice *  pHCDDevice; /* Pointer to the device we belong to */
    pUSB_XHCD_RING          pEpRing; /* Defaul transfer ring of the pipe */
    USB_XHCD_STREAM_INFO    streamInfo; /* Streams information of pipe */
    }USB_XHCD_PIPE, * pUSB_XHCD_PIPE;

/* Request info structure - corresponding to an xHCI TD */

typedef struct usbXhcdRequestInfo
    {
    NODE                epNode;     /* Node on endpoint request list */
    pUSB_XHCD_PIPE      pHCDPipe;   /* The pipe we are working on */
    pUSBHST_URB         pURB;       /* The URB that we work for */
    pUSB_XHCD_RING      pRing;      /* The Transfer Ring we use */
    pUSB_XHCD_SEGMENT   pHeadSeg;   /* Pointer to the first segment */
    pUSB_XHCI_TRB       pHeadTRB;   /* Pointer to the first TRB */
    pUSB_XHCD_SEGMENT   pTailSeg;   /* Pointer to the last segment */
    pUSB_XHCI_TRB       pTailTRB;   /* Pointer to the last TRB */
    UINT64              uHeadTRB;   /* Head TRB Bus Address */
    UINT64              uTailTRB;   /* Tail TRB Bus Address */
    UINT32              uReqLen;    /* Request length */
    VXB_DMA_MAP_ID      dataDmaMapId; /* DMA MAP ID used for user data */
    UINT64              uDataBusAddr; /* user data bus address */
    }USB_XHCD_REQUEST_INFO, * pUSB_XHCD_REQUEST_INFO;

#define USB_XHCD_EP_NODE_TO_REQ_INFO(pNode)                 \
       ((USB_XHCD_REQUEST_INFO *) ((char *) (pNode) -       \
                        OFFSET(USB_XHCD_REQUEST_INFO, epNode)))

/* 
 * We may have at most 30 normal endpoints, these endponts may 
 * at most be assigned for 30 interfaces. 
 */
 
#define USB_XHCI_MAX_INTERFACES         (30)

/* 
 * We use the same mechanism as the Device Context data structure
 * to organize pipes in a device structure. Each pipe corresponds
 * to a Endpoint Context data structure in the Device Context. With
 * this mechanism, we can use Endpoint ID (DCI) to locate the pipes 
 * in a device, which can make our life easier to access both the
 * Endpoint Context and the pipe. 
 *
 * This means that we will have an empty entry_0 in the pDevPipes[]
 * below, since DCI starts from 1 (the default control endpoint).
 *
 * Don't be afraid of one pointer waste since it will help you more!
 */

#define	USB_XHCI_MAX_DEV_PIPES	    (31 + 1)

typedef struct usbXhcdInterface
    {
    UINT8 uAlternateSetting;
    UINT8 uNumEndpoints;
    UINT8 uEndpoints[USB_XHCI_MAX_DEV_PIPES];
    }USB_XHCD_INTERFACE, * pUSB_XHCD_INTERFACE;

typedef struct usbXhcdDevice
    {
    /* Index the Device Context in the DCBAA */
    UINT32                  uSlotId;

    /* Number of interfaces in the current configuration */
    UINT32                  uNumInterfaces;
    
    /* This forms a software reflection of the Device Contexts */
    pUSB_XHCD_CTX_WRAPPER   pIntputCtx; /* Input Context */
    pUSB_XHCD_CTX_WRAPPER   pOutputCtx; /* Output Context */

    /* 
     * This Device Pipe Pointer Array matches Endpoint Contexts,
     * which can also be indexed by DCI (Device Context Index)
     */
    
    pUSB_XHCD_PIPE          pDevPipes[USB_XHCI_MAX_DEV_PIPES]; 

    /* Array of active interfaces for current configuration */
    USB_XHCD_INTERFACE      xInterfaces[USB_XHCI_MAX_INTERFACES];
    
    /* The following matches Figure 76: Input Control Context */
    UINT32                  uDropFlags; /* Drop Context flags (D2 - D31) */
    UINT32                  uAddFlags;  /* Add Context flags (A0 - A31) */
    
    /* The following matches Figure 72: Slot Context Data Structure */
    UINT32                  uRouteString;   /* Route String */
    UINT8                   uDevSpeed;      /* Device Speed */
    UINT8                   uMTT;           /* Multi-TT (MTT) */
    UINT8                   uHub;           /* Set if its Hub */
    UINT8                   uContextEntries;/* Context Entries */
    UINT16                  uMaxExitLatency;/* Maximum Exit Latency in us */
    UINT8                   uRootHubPort;   /* Root Hub Port Number */
    UINT8                   uNumPorts;      /* Number of Ports */
    UINT8                   uTTHubSlotId;   /* TT Hub Slot ID */
    UINT8                   uTTPort;        /* TT Port Number */
    UINT8                   uTTT;           /* TT Think Time (TTT) */
    UINT16                  uIntrTarget;    /* Interrupter Target */   
    UINT8                   uDevAddr;       /* USB Device Address */
    UINT8                   uSlotState;     /* Slot State */    

    /* Command List for this Device Slot */
    LIST                    activeCmdList;
    SEM_ID                  cmdDoneSem;

    BOOL                    bIsConfiged;
    }USB_XHCD_DEVICE, * pUSB_XHCD_DEVICE;

/* ERST */

typedef struct usbxhcdErst
    {
    pUSB_XHCI_ERST_ENTRY pERSTEntries; /* ERST Entries */
    size_t  uNumERSTEntries;
    }USB_XHCD_ERST, * pUSB_XHCD_ERST;

/* DCBAA */

typedef struct usbXhcdDcbaa
    {
    UINT64 *        pDCBAA; /* USB_XHCI_MAX_DEV_SLOTS UINT64 entries */
    VXB_DMA_MAP_ID  dmaMapId;  /* DMA map for DCBAA */
    }USB_XHCD_DCBAA, *pUSB_XHCD_DCBAA;

/* Scratchpad Buffer */

typedef struct usbXhcdSPB
    {
    UINT8 *         pBuff;
    VXB_DMA_MAP_ID  dmaMapId;  /* DMA MAP ID for Scratchpad Buffer */
    }USB_XHCD_SPB, * pUSB_XHCD_SPB;

#define USB_XHCI_INPUT_CTRL_CTX_INDEX   0
#define USB_XHCI_SLOT_CTX_INDEX_DEV     0
#define USB_XHCI_SLOT_CTX_INDEX_INPUT   1

/* Cast to pUSB_XHCI_INPUT_CTRL_CTX */ 

#define USB_XHCI_INPUT_CTRL_CTX_CAST(pHCDData, pCtxWrapper)     \
    (pUSB_XHCI_INPUT_CTRL_CTX)((pCtxWrapper)->pCtxArea + 0)

/* Cast to pUSB_XHCI_SLOT_CTX */ 

#define USB_XHCI_SLOT_CTX_CAST(pHCDData, pCtxWrapper)           \
    (pUSB_XHCI_SLOT_CTX)                                        \
    (((pCtxWrapper)->uCtxType == USB_XHCI_DEV_CTX_TYPE) ?       \
    ((pCtxWrapper)->pCtxArea + 0) :                             \
    ((pCtxWrapper)->pCtxArea + (pHCDData)->uCtxSize))

/* Cast to pUSB_XHCI_EP_CTX */ 

#define USB_XHCI_EP_CTX_CAST(pHCDData, pCtxWrapper, uEpId)   \
    (pUSB_XHCI_EP_CTX)                                          \
    (((pCtxWrapper)->uCtxType == USB_XHCI_DEV_CTX_TYPE) ?       \
    ((pCtxWrapper)->pCtxArea + ((uEpId) * (pHCDData)->uCtxSize)) :  \
    ((pCtxWrapper)->pCtxArea + (((uEpId) + 1) * (pHCDData)->uCtxSize)))

#define USB_XHCI_REV_MAJOR_USB1 0x01
#define USB_XHCI_REV_MAJOR_USB2 0x02
#define USB_XHCI_REV_MAJOR_USB3 0x03

struct usbXhcdRootHubData;

typedef struct usbXhcdRootPortInfo
    {
    /* Pointer to Root Hub Data */
    
    struct usbXhcdRootHubData *  pRootHub;

    /* Port Number */
    
    UINT8   uPortNumber;

    /* Port Status */
    
    UINT32  uPortSc;
    }USB_XHCD_ROOT_PORT_INFO, *pUSB_XHCD_ROOT_PORT_INFO;

typedef struct usbXhcdRootHubData
    {
    /* Number of downstream ports supported by the Root Hub */
    UINT8        uNumPorts;  
    UINT8        uPortOffset;
    UINT8        uRevMajor;
    UINT8        uRevMinor;
    
    /* Pointer to root hub port status */
    USB_HUB_PORT_STATUS * pPortStatus; 

    /* Buffer holding the hub status */
    UINT8        pHubStatus[USB_HUB_STATUS_SIZE];

    /* Buffer holding the data to be returned on an interrupt request */
    UINT8 *      pHubInterruptData;

    /* To hold the size of the interrupt data */
    UINT32       uHubInterruptDataSize;   

    /* Control pipe information */    
    pUSB_XHCD_PIPE  pControlPipe; 

    /* Interrupt pipe information */
    pUSB_XHCD_PIPE  pInterruptPipe;        

    /* Pending interrupt request */
    pUSBHST_URB     pPendingInterruptURB;

    /* Flag indicating whether Remote Wakeup is enabled or not */
    UINT8        bRemoteWakeupEnabled; 

    /* Flag indicating whether the interrupt endpoint is halted or not */
    UINT8        bInterruptEndpointHalted; 
    
    /* Address of the Root hub. */
    UINT8        uDeviceAddress;   

    /* Value of the configuration. */
    UINT8        uConfigValue;             
    }USB_XHCD_ROOT_HUB_DATA, *pUSB_XHCD_ROOT_HUB_DATA;

typedef struct usbXhcdIntrRegSave
    {
    UINT32  uIMAN;
    UINT32  uIMOD;
    UINT32  uERSTSZ;
    UINT64  uERSTBA;
    UINT64  uERDP;
    }USB_XHCD_INTR_REG_SAVE, * pUSB_XHCD_INTR_REG_SAVE;

typedef struct usbXhcdRegSave
    {
    UINT32  uUSBCMD;
    UINT32  uDNCTRL;
    UINT32  uCONFIG;
    UINT64  uCRCR;
    UINT64  uDCBAAP;
    USB_XHCD_INTR_REG_SAVE xIntrSave[0];
    }USB_XHCD_REG_SAVE, * pUSB_XHCD_REG_SAVE;

#define USB_XHCD_INTR_TYPE_NONE     0
#define USB_XHCD_INTR_TYPE_MSIX     1
#define USB_XHCD_INTR_TYPE_MSI      2
#define USB_XHCD_INTR_TYPE_INTX     3

#define USB_XHCD_MAX_MSIX           1
#define USB_XHCD_MAX_MSI            1
#define USB_XHCD_MAX_INTX           1
#define USB_XHCD_MAX_MSGS           max(USB_XHCD_MAX_MSIX, USB_XHCD_MAX_MSI)

typedef struct usbXhcdData
    {
    VXB_DEVICE_ID   pDev;      /* VxBus representation of this xHCI instance */
    VOID *          pRegAccessHandle; /* Register access handle */
    UINT32          uBarIndex; /* Index of the MMIO space base register */
    UINT32          uBusIndex; /* Relative Bus Index */
    
    ULONG   uCpRegBase;  /* Capability Registers Base Addrress */
    ULONG   uOpRegBase;  /* Operational Registers Base Address */
    ULONG   uRtRegBase;  /* Runtime Registers Base Address */
    ULONG   uDbRegBase;  /* Doorbell Registers Base Address */
    ULONG   uXpRegBase;  /* xHCI Extended Capabilities Base Address */

    UINT32  uHCSPARAMS1; /* Saved value of Structural Parameters 1 */
    UINT32  uHCSPARAMS2; /* Saved value of Structural Parameters 2 */
    UINT32  uHCSPARAMS3; /* Saved value of Structural Parameters 3 */
    UINT32  uHCCPARAMS;  /* Saved value of Capability Parameters */
    UINT16  uHCIVERSION; /* Saved value of Interface Version Number */
    UINT8   uCAPLENGTH;  /* Saved value of Capability Length Register */
    
    UINT16  uPciVID;    /* PCI VID */
    UINT16  uPciPID;    /* PCI PID */
    UINT8   uPciCapPtr; /* PCI Standard Capbitlity Pointer */
    UINT8   uMsiCapPtr; /* PCI MSI Capbitlity Pointer */
    UINT8   uMsixCapPtr;/* PCI MSI-X Capbitlity Pointer */
    UINT8   uPmCapPtr;  /* PCI PM Capbitlity Pointer */
    UINT8   uPcieCapPtr;/* PCIe Capbitlity Pointer */
    UINT8   uAC64;      /* Addressing 64 bit capability */
    UINT8   uCSZ64;     /* Device Context size (32 or 64) */
    UINT8   uSBRN;      /* Serial Bus Release Number */
    UINT8   uMaxSlots;  /* Max # device slots supported */
    UINT8   uMaxPorts;  /* Max # root ports supported */
    UINT16  uMaxIntrs;  /* Max # interrupters supported */
    UINT32  uMaxSPBs;   /* Max # Scratchpad Buffers supported */
    UINT32  uMaxERSTEs; /* Max # Event Ring Segment Table entries */
    UINT32  uPageSize;  /* Page size supported */
    UINT32  uCtxArraySize; /* Context Array size used */
    UINT32  uCtxSize;   /* Context size used */
    UINT32  uERSTSize;  /* ERST size supported */
    UINT32  uQuirkFlags;/* Various Quirky Flags */

    UINT8   uUSB2Ports; /* Number of USB2 ports */
    UINT8   uUSB3Ports; /* Number of USB3 ports */
    
    VXB_DMA_TAG_ID xhciParentTag; /* Our parent TAG ID */

    VXB_DMA_TAG_ID dcbaaDmaTagId; /* DMA TAG ID for DCBAA */
    VXB_DMA_TAG_ID segDmaTagId;   /* DMA TAG ID for segments */
    VXB_DMA_TAG_ID ctxDmaTagId;   /* DMA TAG ID for contexts */
    VXB_DMA_TAG_ID erstDmaTagId;  /* DMA TAG ID for ERST */
    VXB_DMA_TAG_ID spbaDmaTagId;  /* DMA TAG ID for Scratchpad Buffer Array  */
    VXB_DMA_TAG_ID spbDmaTagId;   /* DMA TAG ID for Scratchpad Buffer */
    VXB_DMA_TAG_ID lscaDmaTagId;  /* DMA TAG ID for Steam Array (Linear)  */
    VXB_DMA_TAG_ID pscaDmaTagId;  /* DMA TAG ID for Steam Array (Pri/Sec) */
    VXB_DMA_TAG_ID dataDmaTagId;  /* DMA TAG ID for Transfer Data */

    UINT64 *        pDCBAA;       /* DCBAA */
    VXB_DMA_MAP_ID  dcbaaDmaMapId;/* DMA MAP ID for DCBAA */
    
    UINT64 *        pSPBA;        /* Scratchpad Buffer Array */
    VXB_DMA_MAP_ID  spbaDmaMapId; /* DMA MAP ID for Scratchpad Buffer Array */
    
    UINT8 *         pSPBs;        /* Scratchpad Buffer Pages */ 
    VXB_DMA_MAP_ID  spbDmaMapId;  /* DMA MAP ID for Scratchpad Buffer Pages */

    pUSB_XHCI_ERST_ENTRY    pERSTs;        /* ERST entries */  
    VXB_DMA_MAP_ID          erstDmaMapId;  /* DMA MAP ID for ERST */

    UINT32              uNewSlotId; /* Slot ID returned by Enable Slot Command */
    pUSB_XHCD_DEVICE *  ppDevSlots; /* Device Slots */

    UINT32           uIntrType;     /* Interrupt type - MSI-X, MSI, or INTx */
    size_t           uNumEvtRings;  /* Number of event rings - TODO: configurable */
    pUSB_XHCD_RING * ppEvtRings;    /* Array of event ring pointers */
    
    UINT32          uUSBSTS;       /* Current USBSTS register value */

    
    OS_EVENT_ID     InterruptEvent;/*
                                    * Event indicating that an interrupt
                                    * has occurred.
                                    */
    OS_THREAD_ID    IntHandlerThreadID;
                                   /*
                                    * Thread ID of the thread which handles
                                    * xHCI interrupts.
                                    */
    UINT32          uIsrMagic;     /* Watch out for shared interrupts */

    spinlockIsr_t   spinLockIsr;   /* SMP safe lock for ISR handling */

    LIST            activeCmdList;  /* Command List */
    pUSB_XHCD_CMD   pCMDs;          /* Command Array */
    pUSB_XHCD_RING  pCmdRing;       /* Command ring */
    OS_EVENT_ID     cmdDoneSem;
                                   /*
                                    * Event indicating that an command has
                                    * been completed.
                                    */

    OS_EVENT_ID     hcdMutex;       /* Mutex to protect this data structure */


    UINT32  (*pDescSwap32)(UINT32 data);/* function pointer to hold the
                                         * function doing byte conversions
                                         * for HC data structure
                                         */

    UINT32  (*pUsbSwap32)(UINT32 data); /* function pointer to hold the
                                         * function doing byte conversions
                                         * for USB endian
                                         */

    UINT32  (*pRegSwap32)(UINT32 data); /* function pointer to hold the
                                         * function doing byte conversions
                                         * for HC register endian
                                         */
    UINT64  (*pDescSwap64)(UINT64 data);/* function pointer to hold the
                                      * function doing byte conversions
                                      * for HC data structure
                                      */

    UINT64  (*pUsbSwap64)(UINT64 data); /* function pointer to hold the
                                      * function doing byte conversions
                                      * for USB endian
                                      */

    UINT64  (*pRegSwap64)(UINT64 data); /* function pointer to hold the
                                      * function doing byte conversions
                                      * for HC register endian
                                      */
                                      
    pUSB_XHCD_PIPE   pDefaultUSB3Pipe;   /* Default USB3 pipe information */
    pUSB_XHCD_PIPE   pDefaultUSB2Pipe;   /* Default USB2 pipe information */
    USB_XHCD_ROOT_PORT_INFO * pRootPorts; /* Root hub port info pointer */
    USB_XHCD_ROOT_HUB_DATA USB2RH;   /* Root hub data for USB2 ports */
    USB_XHCD_ROOT_HUB_DATA USB3RH;   /* Root hub data for USB3 ports */

    /* Power Management stuff */
    
    UINT16 pciPmcsr; /* Saved PMCSR register value */
    UINT32 pciConfig[16]; /* Saved PCI config space registers */
    
    pUSB_XHCD_REG_SAVE pRegSave; /* xHCI specific register save area */
    
    FUNCPTR            pPostResetHook;  /* function pointer to hold the
                                         * function doing post reset operation
                                         */
    }USB_XHCD_DATA, *pUSB_XHCD_DATA;

IMPORT pUSB_XHCD_DATA * gpXHCDData;

STATUS usbXhcdPwrStateSet
    (
    VXB_DEVICE_ID   pDev,
    void *          state
    );

USBHST_STATUS usbXhcdGetFrameNumber
    (
    UINT8   uBusIndex,    /* Index of the host controller */
    UINT16 *puFrameNumber /* Pointer to the variable to hold the frame number */
    );

USBHST_STATUS usbXhcdSetBitRate
    (
    UINT8   uBusIndex,          /* Index of the host controller       */
    BOOL    bIncrement,         /* Flag for increment or decrement    */
    UINT32 *puCurrentFrameWidth /* Pointer to the current frame width */
    );

USBHST_STATUS usbXhcdIsBandwidthAvailable
    (
    UINT8   uBusIndex,            /* Host controller bus index */
    UINT8   uDeviceAddress,       /* Handle to the device addr */
    UINT8   uDeviceSpeed,         /* Speed of the device in default state */
    UCHAR * pCurrentDescriptor,   /* Ptr to current configuration */
    UCHAR * pNewDescriptor        /* Ptr to new configuration */
    );

USBHST_STATUS usbXhcdIsRequestPending
    (
    UINT8  uBusIndex,   /* Index of the host controller */
    ULONG  uPipeHandle  /* Pipe handle */
    );

USBHST_STATUS usbXhcdCreatePipe
    (
    UINT8   uBusIndex,          /* Host controller index      */
    UINT8   uDeviceAddress,     /* USB device address         */
    UINT8   uDeviceSpeed,       /* USB device speed           */
    UCHAR  *pEndpointDescriptor,/* Endpoint descriptor        */
    UINT16  uHighSpeedHubInfo,  /* High speed hub information */
    ULONG  *puPipeHandle        /* Pointer to the pipe handle */
    );

USBHST_STATUS usbXhcdDeletePipe
    (
    UINT8   uBusIndex,   /* Index of the host controller     */
    ULONG   uPipeHandle  /* Handle of the pipe to be deleted */
    );

USBHST_STATUS usbXhcdModifyDefaultPipe
    (
    UINT8   uBusIndex,          /* Host controller bus index               */
    ULONG   uDefaultPipeHandle, /* Handle to the default pipe              */
    UINT8   uDeviceSpeed,       /* Speed of the device in default state    */
    UINT8   uMaxPacketSize,     /* Maximum packet size of the default pipe */
    UINT16  uHighSpeedHubInfo   /* High speed hub info for USB 1.1 device  */
    );

USBHST_STATUS usbXhcdSubmitURB
    (
    UINT8           uBusIndex,          /* Index of the host controller */
    ULONG           uPipeHandle,        /* Pipe handle */
    pUSBHST_URB     pURB                /* Pointer to the URB */
    );

USBHST_STATUS usbXhcdCancelURB
    (
    UINT8       uBusIndex,   /* Index of the host controller */
    ULONG       uPipeHandle, /* Pipe handle */
    pUSBHST_URB pURB         /* Pointer to the URB */
    );

USBHST_STATUS usbXhcdPipeControl
    (
    UINT8  uBusIndex,                   /* Index of the host controller */
    ULONG  uPipeHandle,                 /* Pipe handle */
    USBHST_PIPE_CONTROL_INFO * pPipeCtrl /* Pointer to the pipe control info */
    );

USBHST_STATUS usbXhcdDeviceControl
    (
    UINT8  uBusIndex,                   /* Index of the host controller */
    USBHST_DEVICE_CONTROL_INFO * pDeviceCtrl /* Pointer to the Device control info */
    );

USBHST_STATUS usbXhcdClearTTRequestComplete
    (
    UINT8         uRelativeBusIndex,  /* bus index of host controller */
    VOID *        pContext,           /* usbd context value           */
    USBHST_STATUS nStatus             /* status of request completion */
    );

USBHST_STATUS usbXhcdResetTTRequestComplete
    (
    UINT8         uRelativeBusIndex,  /* bus index of host controller */
    VOID *        pContext,           /* usbd context value           */
    USBHST_STATUS nStatus             /* status of request completion */
    );

VOID usbXhcdISR
    (
    pVOID pParam
    );

VOID usbXhcdInterruptHandler
    (
    pUSB_XHCD_DATA pHCDData
    );

USBHST_STATUS usbXhcdResetPipe
    (
    pUSB_XHCD_DATA  pHCDData,
    pUSB_XHCD_PIPE  pHCDPipe
    );

USBHST_STATUS usbXhcdRootHubCreatePipe
    (
    pUSB_XHCD_DATA pHCDData,            
    UINT8          uDeviceAddress,      
    UINT8          uDeviceSpeed,        
    UCHAR *        pEndpointDescriptor, 
    ULONG *        puPipeHandle         
    );

USBHST_STATUS usbXhcdRootHubDeletePipe
    (
    pUSB_XHCD_DATA pHCDData,           
    ULONG          uPipeHandle         
    );

USBHST_STATUS usbXhcdRootHubSubmitURB
    (
    pUSB_XHCD_DATA pHCDData,           
    ULONG          uPipeHandle,        
    pUSBHST_URB    pURB                
    );

USBHST_STATUS usbXhcdRootHubCancelURB
    (
    pUSB_XHCD_DATA pHCDData,           
    ULONG          uPipeHandle,        
    pUSBHST_URB    pURB                
    );

BOOLEAN usbXhcdRootHubCopyInterruptData
    (
    pUSB_XHCD_DATA           pHCDData,      
    pUSB_XHCD_ROOT_HUB_DATA  pRootHub,      
    UINT32                   uStatusChange 
    );

#include "usbXhcdUtil.h"

#ifdef __cplusplus
}
#endif
#endif  /* __INCusbXhcdInterfaceh */

