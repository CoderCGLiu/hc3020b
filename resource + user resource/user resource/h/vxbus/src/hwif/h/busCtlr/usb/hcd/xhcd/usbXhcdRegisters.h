/* usbXhcdRegisters.h - USB XHCI Driver Register Definitions */

/*
 * Copyright (c) 2011, 2012-2014 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01d,16jun14,wyy  Support INTEL 8 series and 9 series PCH (VXW6-70103)
01c,03sep13,wyy  Add macro OS_BUSY_WAIT_US and OS_BUSY_WAIT_MS to busy wait
                 when rebooting (WIND00432482)
01b,17oct12,w_x  Fix compiler warnings (WIND00370525)
01a,09may11,w_x  written
*/

#ifndef __INCusbXhcdRegistersh
#define __INCusbXhcdRegistersh

#ifdef __cplusplus
extern "C" {
#endif

#include <vxWorks.h>
#include <hwif/vxbus/vxBus.h>
#include <hwif/util/vxbDmaBufLib.h>

/* Read 32 bit Host Controller Capability Registers */

#define USB_XHCD_READ_CP_REG32(pHCDData, OFFSET)                           \
    vxbRead32 ((pHCDData)->pRegAccessHandle,                               \
    (UINT32 *)((pHCDData)->uCpRegBase + (ULONG)(OFFSET)))

/* Write 32 bit Host Controller Capability Registers */

#define USB_XHCD_WRITE_CP_REG32(pHCDData, OFFSET, VALUE)                   \
    vxbWrite32 ((pHCDData)->pRegAccessHandle,                              \
    (VOID *)((pHCDData)->uCpRegBase + (ULONG)(OFFSET)), (VALUE))

/* Read 32 bit Host Controller Operational Registers */

#define USB_XHCD_READ_OP_REG32(pHCDData, OFFSET)                           \
    vxbRead32 ((pHCDData)->pRegAccessHandle,                               \
    (UINT32 *)((pHCDData)->uOpRegBase + (ULONG)(OFFSET)))

/* Write 32 bit Host Controller Operational Registers */

#define USB_XHCD_WRITE_OP_REG32(pHCDData, OFFSET, VALUE)                   \
    vxbWrite32 ((pHCDData)->pRegAccessHandle,                              \
    (VOID *)((pHCDData)->uOpRegBase + (ULONG)(OFFSET)), (VALUE))

/* Read 64 bit Host Controller Operational Registers (DCBAAP) */

#define USB_XHCD_READ_OP_REG64(pHCDData, OFFSET)                           \
    vxbUsbXhcdRead64 ((pHCDData)->pRegAccessHandle,                        \
    (UINT64 *)((pHCDData)->uOpRegBase + (ULONG)(OFFSET)))

/* Write 64 bit Host Controller Operational Registers (DCBAAP) */

#define USB_XHCD_WRITE_OP_REG64(pHCDData, OFFSET, VALUE)                   \
    vxbUsbXhcdWrite64 ((pHCDData)->pRegAccessHandle,                       \
    (UINT64 *)((pHCDData)->uOpRegBase + (ULONG)(OFFSET)), (VALUE))

/* Read 32 bit Host Controller Runtime Registers */

#define USB_XHCD_READ_RT_REG32(pHCDData, OFFSET)                           \
    vxbRead32 ((pHCDData)->pRegAccessHandle,                               \
    (UINT32 *)((pHCDData)->uRtRegBase + (ULONG)(OFFSET)))

/* Write 32 bit Host Controller Runtime Registers */

#define USB_XHCD_WRITE_RT_REG32(pHCDData, OFFSET, VALUE)                   \
    vxbWrite32 ((pHCDData)->pRegAccessHandle,                              \
    (VOID *)((pHCDData)->uRtRegBase + (ULONG)(OFFSET)), (VALUE))

/* Read 64 bit Host Controller Runtime Registers */

#define USB_XHCD_READ_RT_REG64(pHCDData, OFFSET)                           \
    vxbUsbXhcdRead64 ((pHCDData)->pRegAccessHandle,                        \
    (UINT64 *)((pHCDData)->uRtRegBase + (ULONG)(OFFSET)))

/* Write 64 bit Host Controller Runtime Registers */

#define USB_XHCD_WRITE_RT_REG64(pHCDData, OFFSET, VALUE)                   \
    vxbUsbXhcdWrite64 ((pHCDData)->pRegAccessHandle,                       \
    (UINT64 *)((pHCDData)->uRtRegBase + (ULONG)(OFFSET)), (VALUE))

/* Read 32 bit Host Controller Doorbell Registers */

#define USB_XHCD_READ_DB_REG32(pHCDData, OFFSET)                           \
    vxbRead32 ((pHCDData)->pRegAccessHandle,                               \
    (UINT32 *)((pHCDData)->uDbRegBase + (ULONG)(OFFSET)))

/* Write 32 bit Host Controller Doorbell Registers */

#define USB_XHCD_WRITE_DB_REG32(pHCDData, OFFSET, VALUE)                   \
    vxbWrite32 ((pHCDData)->pRegAccessHandle,                              \
    (VOID *)((pHCDData)->uDbRegBase + (ULONG)(OFFSET)), (VALUE))

/* Read 32 bit xHCI Extended Capability Registers */

#define USB_XHCD_READ_XP_REG32(pHCDData, OFFSET)                           \
    vxbRead32 ((pHCDData)->pRegAccessHandle,                               \
    (UINT32 *)((pHCDData)->uXpRegBase + (ULONG)(OFFSET)))

/* Write 32 bit xHCI Extended Capability Registers */

#define USB_XHCD_WRITE_XP_REG32(pHCDData, OFFSET, VALUE)                   \
    vxbWrite32 ((pHCDData)->pRegAccessHandle,                              \
    (VOID *)((pHCDData)->uXpRegBase + (ULONG)(OFFSET)), (VALUE))

/* xHCI PCI Config Header bit fields - Start */

#define USB_XHCI_PCI_VID_PID_OFFSET  0x0
#define USB_XHCI_PCI_VID_PID_SIZE    0x4
#define USB_XHCI_PCI_VID_GET(uVidPid)    \
    ((uVidPid) & 0xFFFF)
#define USB_XHCI_PCI_PID_GET(uVidPid)    \
    (((uVidPid) >> 16) & 0xFFFF)

#define USB_XHCI_PCI_CMD_OFFSET  0x4
#define USB_XHCI_PCI_CMD_SIZE    0x2
#define USB_XHCI_PCI_CMD_INTx_DISABLE_GET(uCMD)    \
    (((uCMD) >> 10) & 0x01)
#define USB_XHCI_PCI_CMD_INTx_DISABLE (1 << 10)

/* Class Code and Revision */

#define USB_XHCI_PCI_CLASS_REV_OFFSET    0x8
#define USB_XHCI_PCI_CLASS_REV_SIZE      0x4
#define USB_XHCI_PCI_CLASS_GET(uClassRev)    \
    (((uClassRev) >> 8) & 0xFFFFFF)
#define USB_XHCI_PCI_REV_GET(uClassRev)      \
    (((uClassRev) >> 0) & 0xFF)
#define USB_XHCI_PCI_CLASS_CODE          0x0C0330

/* PCI Standard Capabilities Pointer (not xHCI Extended Capabilities) */

#define USB_XHCI_PCI_CAP_PTR_OFFSET      0x34
#define USB_XHCI_PCI_CAP_PTR_SIZE        0x4
#define USB_XHCI_PCI_CAP_PTR_GET(uECP)    ((uECP) & (0xFC))

#define USB_XHCI_PCI_CAP_ID_GET(uPciCap)    (((uPciCap) >> 0) & 0xFF)
#define USB_XHCI_PCI_CAP_NEXT_GET(uPciCap)  (((uPciCap) >> 8) & 0xFF)
#define USB_XHCI_PCI_CAP_VAL_GET(uPciCap)   (((uPciCap) >> 16) & 0xFFFF)

/* Some of PCI Standard Capabilities that may relate to USB/xHCI */

#define USB_XHCI_PCI_CAP_ID_PCIPM    0x01 /* PCI Power Management Interface */
#define USB_XHCI_PCI_CAP_ID_MSI      0x05 /* MSI */
#define USB_XHCI_PCI_CAP_ID_PCIe     0x10 /* PCI Express */
#define USB_XHCI_PCI_CAP_ID_MSIx     0x11 /* MSI-X */

/*
 * Bit masks for the Power Management Capability 
 * Power Management Capabilities (PMC) field 
 */
 
#define USB_XHCI_PCI_PM_VER_GET(uPMC) \
        (((uPMC) >> 0) & 0x7)              /* Version */
#define USB_XHCI_PCI_PM_PME_CLK     (1 << 3)/* PME Clock */
#define USB_XHCI_PCI_PM_DSI         (1 << 5)/* Device Specific Initialization */
#define USB_XHCI_PCI_PM_AUX_CURRENT_GET(uPMC) \
        (((uPMC) >> 6) & 0x7)              /* AUX Current */
#define USB_XHCI_PCI_PM_D1_SUPPORT  (1 << 9)/* D1 Support */
#define USB_XHCI_PCI_PM_D2_SUPPORT  (1 << 10)/* D2 Support */
#define USB_XHCI_PCI_PM_PME_SUPPORT_GET(uPMC) \
        (((uPMC) >> 11) & 0x1F)            /* PME Support */

/*
 * Bit masks for the Power Management Capability 
 * Power Management Control/Status - PMCSR field 
 */
 
#define USB_XHCI_PCI_PM_POWER_STATE_GET(uPMCSR) \
            (((uPMCSR) >> 0) & 0x3)              /* Power State */
#define USB_XHCI_PCI_PM_NO_SFT_RST     (1 << 3)   /* No Soft Reset */
#define USB_XHCI_PCI_PM_PME_EN         (1 << 8)   /* PME_En */
#define USB_XHCI_PCI_PM_DATA_SELECT_GET(uPMCSR) \
        (((uPMCSR) >> 9) & 0xF)                  /* Data Select */
#define USB_XHCI_PCI_PM_DATA_SCALE_GET(uPMCSR) \
        (((uPMCSR) >> 13) & 0x3)                 /* Data Scale */
#define USB_XHCI_PCI_PM_PME_STS        (1 << 15)  /* PME Status */
#define USB_XHCI_PCI_PM_B2B3_SUPPORT   (1 << 22)  /* B2/B3 Support */
#define USB_XHCI_PCI_PM_BP_CC_EN       (1 << 23)  /* Bus Power/Clock Control Enable */

/* Bit masks for the MSI Capability Message Control field */

#define USB_XHCI_PCI_MSI_EN (1 << 0)   /* MSI Enable */
#define USB_XHCI_PCI_MSI_MMC_GET(uMsgCtrl) \
    (((uMsgCtrl) >> 1) & 0x7)          /* Multiple Message Capable */
#define USB_XHCI_PCI_MSI_MME_GET(uMsgCtrl) \
    (((uMsgCtrl) >> 4) & 0x7)          /* Multiple Message Enable */
#define USB_XHCI_PCI_MSI_MME(uMME)         \
    ((uMME) << 4)                      /* Multiple Message Enable */
#define USB_XHCI_PCI_MSI_AC64 (1 << 7) /* 64 bit address capable */
#define USB_XHCI_PCI_MSI_VMC  (1 << 8) /* Per-vector masking capable */

/* Bit masks for the MSI-X Capability Message Control field */

#define USB_XHCI_PCI_MSIX_EN    (1 << 15)/* MSI-X Enable */
#define USB_XHCI_PCI_MSIX_FMASK (1 << 14)/* Function Mask */
#define USB_XHCI_PCI_MSIX_TB_SIZE_GET(uMsgCtrl) \
    (((uMsgCtrl) & 0x3FF) + 1)          /* Table Size, n-1 encoded, so +1 */
#define USB_XHCI_PCI_MSIX_TB_OFFSET_GET(uMsix32)\
    (uMsix32 >> 3)                      /* Table Offset */
#define USB_XHCI_PCI_MSIX_TB_BIR_GET(uMsix32)\
    (uMsix32 & 0x7)                     /* Table BIR */
#define USB_XHCI_PCI_MSIX_PBA_OFFSET_GET(uMsix32)\
    (uMsix32 >> 3)                      /* Table Offset */
#define USB_XHCI_PCI_MSIX_PBA_BIR_GET(uMsix32)\
    (uMsix32 & 0x7)                     /* Table BIR */

/* Structure and Bit masks for PCI Express Capability Structure */

typedef struct usb_xhci_pcie_cap
    {
    UINT8   uCapId;             /* PCI Express Capability ID */
    UINT8   uNextCap;           /* Next Capability Pointer */
    UINT16  uPcieCaps;          /* PCI Express Capabilities Register */
    UINT32  uDeviceCaps;        /* Device Capabilities Register */
    UINT16  uDeviceCtrl;        /* Device Control Register */
    UINT16  uDeviceStat;        /* Device Status Register */
    UINT32  uLinkCaps;          /* Link Capabilities Register */
    UINT16  uLinkCtrl;          /* Link Control Register */
    UINT16  uLinkStat;          /* Link Status Register */
    UINT32  uSlotCaps;          /* Slot Capabilities Register */
    UINT16  uSlotCtrl;          /* Slot Control Register */
    UINT16  uSlotStat;          /* Slot Status Register */
    UINT16  uRootCtrl;          /* Root Control Register */
    UINT16  uRootCaps;          /* Root Capabilities Register */
    UINT32  uRootStat;          /* Root Status Register */
    UINT32  uDeviceCaps2;       /* Device Capabilities 2 Register */
    UINT16  uDeviceCtrl2;       /* Device Control 2 Register */
    UINT16  uDeviceStat2;       /* Device Status 2 Register */
    UINT32  uLinkCaps2;         /* Link Capabilities 2 Register */
    UINT16  uLinkCtrl2;         /* Link Control 2 Register */
    UINT16  uLinkStat2;         /* Link Status 2 Register */
    UINT32  uSlotCaps2;         /* Slot Capabilities 2 Register */
    UINT16  uSlotCtrl2;         /* Slot Control 2 Register */
    UINT16  uSlotStat2;         /* Slot Status 2 Register */
    }USB_XHCI_PCIe_CAP,*pUSB_XHCI_PCIe_CAP;

/* PCI Express Extended Capabilities (not xHCI Extended Capabilities) */

#define USB_XHCI_PCIe_CAP_ID(uPcieCap)    (((uPcieCap) >> 0) & 0xFF)
#define USB_XHCI_PCIe_CAP_NEXT(uPcieCap)  (((uPcieCap) >> 8) & 0xFF)
#define USB_XHCI_PCIe_CAP_VAL(uPcieCap)   (((uPcieCap) >> 16) & 0xFFFF)

/* 
 * PCI Express Extended Capabilities in Configuration Space always 
 * begin at offset 100h with a PCI Express Extended Capability header.
 */
 
#define USB_XHCI_PCIe_EXT_CAP_BASE      0x100

/* Some of PCI Express Extended Capabilities that may relate to USB/xHCI */

/* Single-Root I/O Virtualization (SR-IOV) (xHCI used for IOV) */

#define USB_XHCI_PCIe_EXT_CAP_ID_SR_IOV  0x10 

/* Multi-Root I/O Virtualization (MR-IOV) (not used by xHCI) */

#define USB_XHCI_PCIe_EXT_CAP_ID_MR_IOV  0x11 

/* SBRN */

#define USB_XHCI_PCI_SBRN_OFFSET    0x60
#define USB_XHCI_PCI_SBRN_SIZE      0x1
#define USB_XHCI_PCI_SBRN(uSBRN)    ((uSBRN) & 0xFF)

/*
 * 5.2.4 Frame Length Adjustment Register (FLADJ)
 *
 * This register is in the Auxiliary Power well. This feature is used to
 * adjust any offset from the clock source that generates the clock that
 * drives the SOF counter. When a new value is written into these six bits,
 * the length of the frame is adjusted for all USB buses implemented by an xHC.
 * Its initial programmed value is system dependent based on the accuracy of
 * hardware USB clock and is initialized by system software (typically the BIOS).
 * This register should only be modified when the HCHalted (HCH) bit in the
 * USBSTS register is '1'. Changing value of this register while the host
 * controller is operating yields undefined results.
 */

#define USB_XHCI_PCI_FLADJ_OFFSET    0x61
#define USB_XHCI_PCI_FLADJ_SIZE        0x1
#define USB_XHCI_PCI_FLADJ(uFLADJ)    ((uFLADJ) & 0x3F)

/* xHCI PCI Config Header bit fields - End */

/*
 * 5.3 Host Controller Capability Registers
 *
 * These registers specify the limits and capabilities of the host controller
 * implementation. All Capability Registers are Read-Only (RO) or hardware
 * Initialized (HwInit attribute). The offsets for these registers are all
 * relative to the beginning of the host controller's MMIO address space.
 * The beginning of the host controller's MMIO address space is referred to
 * as "Base" throughout this document.
 */

#define USB_XHCI_CAPLENGTH   0x00   /* Capability Register Length */
#define USB_XHCI_HCIVERSION  0x02   /* Interface Version Number */
#define USB_XHCI_HCSPARAMS1  0x04   /* Structural Parameters 1 */
#define USB_XHCI_HCSPARAMS2  0x08   /* Structural Parameters 2 */
#define USB_XHCI_HCSPARAMS3  0x0C   /* Structural Parameters 3 */
#define USB_XHCI_HCCPARAMS   0x10   /* Capability Parameters */
#define USB_XHCI_DBOFF       0x14   /* Doorbell Offset */
#define USB_XHCI_RTSOFF      0x18   /* Runtime Register Space Offset */

/* USB_XHCI_CAPLENGTH and USB_XHCI_HCIVERSION bit fields - Start */

/*
 * Note: We use a 32 bit read to get the USB_XHCI_CAPLENGTH and
 * USB_XHCI_HCIVERSION register values, so we need to split them apart.
 */

/*
 * 5.3.1 Capability Registers Length (CAPLENGTH)
 *
 * This register is used as an offset to add to register base to find the
 * beginning of the Operational Register Space.
 */

#define USB_XHCI_CAP_LENGTH(uCAPLENGTH) (((uCAPLENGTH) >> 0) & 0xFF)

/*
 * 5.3.2 Host Controller Interface Version Number (HCIVERSION)
 *
 * This is a two-byte register containing a BCD encoding of the xHCI
 * specification revision number supported by this host controller. The
 * most significant byte of this register represents a major revision and
 * the least significant byte is the minor revision. e.g. 0100h corresponds
 * to xHCI version 1.0.
 *
 * Note: Pre-release versions of the xHC shall declare the specific
 * version of the xHCI that it was implemented against.
 * e.g. 0090h = version 0.9.
 */

#define USB_XHCI_HCI_VERSION(uCAPLENGTH) (((uCAPLENGTH) >> 16) & 0xFFFF)

/* USB_XHCI_CAPLENGTH and USB_XHCI_HCIVERSION bit fields - End */

/*
 * 5.3.3 Structural Parameters 1 (HCSPARAMS1)
 *
 * This register defines basic structural parameters supported by this xHC
 * implementation: Number of Device Slots support, Interrupters, Root Hub
 * ports, etc.
 */

/* USB_XHCI_HCSPARAMS1 bit fields - Start */

/*
 * Number of Device Slots (MaxSlots). This field specifies the maximum
 * number of Device Context Structures and Doorbell Array entries this
 * host controller can support. Valid values are in the range of 1 to 255.
 * The value of '0' is reserved.
 */

#define USB_XHCI_MAX_SLOTS(uHCSPARAMS1) (((uHCSPARAMS1) >> 0) & 0xFF)

/*
 * Number of Interrupters (MaxIntrs). This field specifies the number of
 * Interrupters implemented on this host controller. Each Interrupter is
 * allocated to a vector of MSI-X and controls its generation and moderation.
 *
 * The value of this field determines how many Interrupter Register Sets
 * are addressable in the Runtime Register Space. Valid values are in the
 * range of 1h to 400h. A '0' in this field is undefined.
 */

#define USB_XHCI_MAX_INTRS(uHCSPARAMS1) (((uHCSPARAMS1) >> 8) & 0x7FF)

/*
 * Number of Ports (MaxPorts). This field specifies the number of physical
 * downstream ports implemented on this host controller. The value of this
 * field determines how many port registers are addressable in the Operational
 * Register Space. Valid values are in the range of 1h to FFh.
 */

#define USB_XHCI_MAX_PORTS(uHCSPARAMS1) (((uHCSPARAMS1) >> 24) & 0xFF)

/* USB_XHCI_HCSPARAMS1 bit fields - End */

/*
 * 5.3.4 Structural Parameters 2 (HCSPARAMS2)
 *
 * This register defines additional xHC structural parameters.
 */

/* USB_XHCI_HCSPARAMS2 bit fields - Start */

/*
 * Isochronous Scheduling Threshold (IST). Default = implementation dependent.
 * The value in this field indicates to system software the minimum distance
 * (in time) that it is required to stay ahead of the host controller while
 * adding TRBs, in order to have the host controller process them at the
 * correct time. The value shall be specified in terms of number of frames/
 * microframes.
 *
 * If bit[3] of IST is cleared to '0', software can add a TRB no later than
 * IST[2:0] Microframes before that TRB is scheduled to be executed.
 *
 * If bit[3] of IST is set to '1', software can add a TRB no later than
 * IST[2:0] Frames before that TRB is scheduled to be executed.
 */

#define USB_XHCI_IST(uHCSPARAMS2) \
    (((uHCSPARAMS2) >> 0) & 0xF)

/*
 * Event Ring Segment Table Max (ERST Max). Default = implementation dependent.
 * Valid values are 0 - 15. This field determines the maximum value supported
 * the Event Ring Segment Table Base/Size registers, where:
 *
 *     The maximum number of Event Ring Segment Table entries = 2^(ERST Max).
 *
 * e.g. if the ERST Max = 7, then the xHC Event Ring Segment Table(s) supports
 * up to 128 entries, 15 then 32K entries, etc.
 */

#define USB_XHCI_MAX_ERST_ENTRIES(uHCSPARAMS2) \
    (1 << (((uHCSPARAMS2) >> 4) & 0xF))

/*
 * IOC Interval. Default = implementation dependent. Valid values are 0 - 23.
 * This field determines the maximum frequency with which the IOC flag may
 * be set for this xHC implementation. The IOC flag may be set in TRBs that
 * define data buffers that are more than 2 ^ (IOC Interval) bytes apart.
 */

#define USB_XHCI_IOC_INTERVAL(uHCSPARAMS2) \
    (((uHCSPARAMS2) >> 8) & 0x1F)

/*
 * Scratchpad Restore (SPR). Default = implementation dependent. If Max
 * Scratchpad Buffers is > '0' then this flag indicates whether the xHC
 * uses the Scratchpad Buffers for saving state when executing Save and
 * Restore State operations. If Max Scratchpad Buffers is = '0' then this
 * flag shall be '0'.
 *
 * A value of '1' indicates that the xHC requires the integrity of the
 * Scratchpad Buffer space to be maintained across power events.
 *
 * A value of '0' indicates that the Scratchpad Buffer space may be freed
 * and reallocated between power events.
 */

#define USB_XHCI_SCRATCHPAD_RESTORE(uHCSPARAMS2) \
    (((uHCSPARAMS2) >> 26) & 0x1)

/*
 * Max Scratchpad Buffers (Max Scratchpad Bufs). Default = implementation
 * dependent. Valid values are 0-31. This field indicates the number of
 * Scratchpad Buffers system software shall reserve for the xHC.
 */

#define USB_XHCI_MAX_SCRATCHPAD_BUFFS(uHCSPARAMS2) \
    (((uHCSPARAMS2) >> 27) & 0x1F)

/* USB_XHCI_HCSPARAMS2 bit fields - End */

/*
 * 5.3.5 Structural Parameters 3 (HCSPARAMS3)
 *
 * This register defines link exit latency related structural parameters.
 */

/* USB_XHCI_HCSPARAMS3 bit fields - Start */

/*
 * U1 Device Exit Latency. Worst case latency to transition a root hub Port
 * Link State (PLS) from U1 to U0. Applies to all root hub ports.
 *
 * The following are permissible values:
 *
 *     Value         Description
 *
 *      00h          Zero
 *      01h          Less than 1 us
 *      02h          Less than 2 us.
 *         ...
 *      0Ah          Less than 10 us.
 *    0B-FFh         Reserved
 */

#define USB_XHCI_U1_DEV_EXIT_LATENCY(uHCSPARAMS3) \
    (((uHCSPARAMS3) >> 0) & 0xFF)

/*
 * U2 Device Exit Latency. Worst case latency to transition from U2 to U0.
 * Applies to all root hub ports.
 *
 * The following are permissible values:
 *
 *      Value        Description
 *
 *      0000h        Zero
 *      0001h        Less than 1 us.
 *      0002h        Less than 2 us.
 *          ...
 *      07FFh        Less than 2047 us.
 * 0800-FFFFh        Reserved
 */

#define USB_XHCI_U2_DEV_EXIT_LATENCY(uHCSPARAMS3) \
    (((uHCSPARAMS3) >> 16) & 0xFFFF)

/* USB_XHCI_HCSPARAMS3 bit fields - End */

/*
 * 5.3.6 Capability Parameters (HCCPARAMS)
 *
 * This register defines optional capabilities supported by the xHCI.
 */

/* USB_XHCI_HCCPARAMS bit fields - Start */

/*
 * 64-bit Addressing Capability (AC64). This flag documents the addressing
 * range capability of this implementation. The value of this flag determines
 * whether the xHC has implemented the high order 32 bits of 64 bit register
 * and data structure pointer fields. Values for this flag have the following
 * interpretation:
 *
 *      Value          Description
 *
 *         0           32-bit address memory pointers implemented
 *         1           64-bit address memory pointers implemented
 *
 * If 32-bit address memory pointers are implemented, the xHC shall ignore
 * the high order 32 bits of 64 bit data structure pointer fields, and system
 * software shall ignore the high order 32 bits of 64 bit xHC registers.
 */

#define USB_XHCI_AC64(uHCCPARAMS)   (((uHCCPARAMS) >> 0) & 0x1)


/*
 * BW Negotiation Capability (BNC). This flag identifies whether the xHC has
 * implemented the Bandwidth Negotiation. Values for this flag have the
 * following interpretation:
 *
 *   Value          Description
 *
 *      0           BW Negotiation not implemented
 *      1           BW Negotiation implemented
 */

#define USB_XHCI_BNC(uHCCPARAMS)    (((uHCCPARAMS) >> 1) & 0x1)

/*
 * Context Size (CSZ). If this bit is set to '1', then the xHC uses 64 byte
 * Context data structures. If this bit is cleared to '0', then the xHC uses
 * 32 byte Context data structures.
 *
 * Note: This flag does not apply to Stream Contexts.
 */

#define USB_XHCI_CSZ(uHCCPARAMS)    (((uHCCPARAMS) >> 2) & 0x1)

/*
 * Port Power Control (PPC). This flag indicates whether the host controller
 * implementation includes port power control. A '1' in this bit indicates
 * the ports have port power switches. A '0' in this bit indicates the port
 * do not have port power switches. The value of this flag affects the
 * functionality of the PP flag in each port status and control register
 */

#define USB_XHCI_PPC(uHCCPARAMS)    (((uHCCPARAMS) >> 3) & 0x1)

/*
 * Port Indicators (PIND). This bit indicates whether the xHC root hub ports
 * support port indicator control. When this bit is a '1', the port status
 * and control registers include a read/writeable field for controlling the
 * state of the port indicator.
 */

#define USB_XHCI_PIND(uHCCPARAMS)   (((uHCCPARAMS) >> 4) & 0x1)

/*
 * Light HC Reset Capability (LHRC). This flag indicates whether the host
 * controller implementation supports a Light Host Controller Reset. A '1'
 * in this bit indicates that Light Host Controller Reset is supported. A '0'
 * in this bit indicates that Light Host Controller Reset is not supported.
 * The value of this flag affects the functionality of the Light Host
 * Controller Reset (LHCRST) flag in the USBCMD register
 */

#define USB_XHCI_LHRC(uHCCPARAMS)   (((uHCCPARAMS) >> 5) & 0x1)

/*
 * Latency Tolerance Messaging Capability (LTMC). This flag indicates whether
 * the host controller implementation supports Latency Tolerance Messaging (LTM).
 * A '1' in this bit indicates that LTM is supported. A '0' in this bit
 * indicates that LTM is not supported.
 */

#define USB_XHCI_LTMC(uHCCPARAMS)   (((uHCCPARAMS) >> 6) & 0x1)

/*
 * No Secondary SID Support (NSS). This flag indicates whether the host
 * controller implementation supports Secondary Stream IDs. A '1' in this
 * bit indicates that Secondary Stream ID decoding is not supported. A '0'
 * in this bit indicates that Secondary Stream ID decoding is supported.
 */

#define USB_XHCI_NSS(uHCCPARAMS)    (((uHCCPARAMS) >> 7) & 0x1)

/*
 * Force Stopped Event (FSE). This flag indicates whether the host controller
 * implementation generates Stopped Transfer Event when a Transfer Ring stops
 * between TDs. A '1' in this bit indicates that Forced Stopped Events are
 * supported. A '0' in this bit indicates that Forced Stopped Events are not
 * supported.
 */

#define USB_XHCI_FSE(uHCCPARAMS)    (((uHCCPARAMS) >> 8) & 0x1)

/*
 * Secondary Bandwidth Domain Reporting (SBD). This flag indicates whether
 * the host controller implementation is capable of reporting Secondary
 * Bandwidth Domain information. A '1' in this bit indicates that Secondary
 * Bandwidth Domain reporting is supported. A '0' in this bit indicates that
 * Secondary Bandwidth Domain reporting is not supported.
 */

#define USB_XHCI_SBD(uHCCPARAMS)    (((uHCCPARAMS) >> 9) & 0x1)

/*
 * Maximum Primary Stream Array Size (MaxPSASize). This fields identifies the 
 * maximum size of Primary Stream Array that the xHC supports. The Primary
 * Stream Array size = 2 ^ (MaxPSASize+1). Valid MaxPSASize values are 1 to 15.
 *
 * NOTE: We can not add the "1" here, otherwise the NEC chip will report 
 * "Parameter Error". So this parameter is considered as the real MAX.
 */

#define USB_XHCI_MAX_PSA_SIZE(uHCCPARAMS)   \
    ((((uHCCPARAMS) >> 12) & 0xF))

/*
 * xHCI Extended Capabilities Pointer (xECP). This field indicates the
 * existence of a capabilities list. The value of this field indicates a
 * relative offset, in 32-bit words, from Base to the beginning of the
 * first extended capability.
 *
 * For example, using the offset of Base is 1000h and the xECP value of 0068h,
 * we can calculated the following effective address of the first extended
 * capability:
 *
 * 1000h + (0068h << 2) -> 1000h + 01A0h -> 11A0h
 */

#define USB_XHCI_XECP(uHCCPARAMS)   (((uHCCPARAMS) >> 16) & 0xFFFF)

/* USB_XHCI_HCCPARAMS bit fields - End */

/*
 * 5.3.7 Doorbell Offset (DBOFF)
 *
 * This register defines the offset in Dwords of the Doorbell Array base
 * address from the Base.
 *
 * Note: Normally the Doorbell Array is Dword aligned, however if
 * virtualization is supported by the xHC then it shall be PAGESIZE
 * aligned. e.g. If the PAGESIZE = 4K (1000h), and the Doorbell Array is
 * positioned at a 3 page offset from the Base, then this register shall
 * report 0000 3000h.
 */

/* USB_XHCI_DBOFF bit fields - Start */

/*
 * Doorbell Array Offset - Default = implementation dependent.
 * This field defines the Dword offset of the Doorbell Array base address
 * from the Base (i.e. the base address of the xHCI Capability register
 * address space).
 */

#define USB_XHCI_DB_OFFSET(uDBOFF)  (((uDBOFF) >> 0) & (~0x3))

/* USB_XHCI_DBOFF bit fields - End */

/*
 * 5.3.8 Runtime Register Space Offset (RTSOFF)
 *
 * This register defines the offset of the xHCI Runtime Registers from the Base.
 *
 * Note: Normally the Runtime Register Space is 32-byte aligned, however
 * if virtualization is supported by the xHC then it shall be PAGESIZE
 * aligned. e.g. If the PAGESIZE = 4K and the Runtime Register Space is
 * positioned at a 1 page offset from the Base, then this register shall
 * report 0000 1000h.
 */

/* USB_XHCI_RTSOFF bit fields - Start */

/*
 * Runtime Register Space Offset - Default = implementation dependent.
 *
 * This field defines the 32-byte offset of the xHCI Runtime Registers
 * from the Base. i.e. Runtime Register Base Address = Base + Runtime
 * Register Set Offset.
 */

#define USB_XHCI_RTS_OFFSET(uRTSOFF) (((uRTSOFF) >> 0) & (~0x1F))

/* USB_XHCI_RTSOFF bit fields - End */

/*
 * 5.4 Host Controller Operational Registers
 *
 * This section defines the xHCI Operational Registers. The base address of
 * this register space is referred to as Operational Base. The Operational Base
 * shall be Dword aligned and is calculated by adding the value of the
 * Capability Registers Length (CAPLENGTH) register to the Capability Base
 * address. All registers are multiples of 32 bits in length.
 *
 * Unless otherwise stated, all registers should be accessed as a 32-bit width
 * on reads with an appropriate software mask, if needed. A software
 * read/modify/write mechanism should be invoked for partial writes.
 *
 * These registers are located at a positive offset from the Capabilities
 * Registers.
 */

#define USB_XHCI_USBCMD      0x00   /* USB Command */
#define USB_XHCI_USBSTS      0x04   /* USB Status */
#define USB_XHCI_PAGESIZE    0x08   /* Page Size */
#define USB_XHCI_DNCTRL      0x14   /* Device Notification Control */
#define USB_XHCI_CRCR        0x18   /* Command Ring Control - 64 bit */
#define USB_XHCI_DCBAAP      0x30   /* DCBAA Pointer - 64 bit */
#define USB_XHCI_CONFIG      0x38   /* Configure */
#define USB_XHCI_PORTSC(n)   (0x400 + (n) * 16) /* Port Status and Control n */
#define USB_XHCI_PORTPMSC(n) (0x404 + (n) * 16) /* Port PM Statusand Control n */
#define USB_XHCI_PORTLI(n)   (0x408 + (n) * 16) /* Port Link Info n */

/*
 * 5.4.1 USB Command Register (USBCMD)
 *
 * The Command Register indicates the command to be executed by the
 * serial bus host controller. Writing to the register causes a command
 * to be executed.
 */

/* USB_XHCI_USBCMD bit fields - Start */

/*
 * Run/Stop (R/S) - RW. Default = '0'. '1' = Run. '0' = Stop. When set to a '1',
 * the xHC proceeds with execution of the schedule. The xHC continues execution
 * as long as this bit is set to a '1'. When this bit is cleared to '0', the
 * xHC completes the current and any actively pipelined transactions on the USB
 * and then halts. The xHC shall halt within 16 ms after software
 * clears the Run/Stop bit. The HCHalted (HCH) bit in the USBSTS register
 * indicates when the xHC has finished its pending pipelined transactions and
 * has entered the stopped state. Software shall not write a '1' to this flag
 * unless the xHC is in the Halted state (i.e. HCH in the USBSTS register is
 * '1'). Doing so will yield undefined results. When this register is exposed
 * by a Virtual Function (VF), this bit only controls the run state of
 * the xHC instance presented by the selected VF.
 */

#define USB_XHCI_USBCMD_RS_GET(uUSBCMD)    (((uUSBCMD) >> 0) & 0x1)
#define USB_XHCI_USBCMD_RS          (0x1 << 0)
#define USB_XHCI_HALT_MAX_WAIT_US   18000 /* MAX 16 ms, but we do more */
#define USB_XHCI_STATE_CHANGE_MAX_WAIT_MS    100 /* 100ms */
#define USB_XHCI_START_MAX_WAIT_MS  5000 /* MAX wait for 5 seconds */

/*
 * Host Controller Reset (HCRST) - RW. Default = '0'. This control bit is used
 * by software to reset the host controller. The effects of this bit on the
 * xHC and the Root Hub registers are similar to a Chip Hardware Reset.
 *
 * When software writes a '1' to this bit, the Host Controller resets its
 * internal pipelines, timers, counters, state machines, etc. to their initial
 * value. Any transaction currently in progress on USB is immediately
 * terminated. A USB reset is not driven on downstream ports. PCI Configuration
 * registers are not affected by this reset. All operational registers,
 * including port registers and port state machines are set to their initial
 * values. Software shall reinitialize the host controller as described in
 * Section 4.1 in order to return the host controller to an operational state.
 *
 * This bit is cleared to '0' by the Host Controller when the reset process
 * is complete. Software cannot terminate the reset process early by writing
 * a '0' to this bit and shall not write any xHC Operational or Runtime
 * registers while HCRST is '1'. Software shall not set this bit to '1' when
 * the HCHalted (HCH) bit in the USBSTS register is a '0'. Attempting to reset
 * an actively running host controller will result in undefined behavior.
 *
 * When this register is exposed by a Virtual Function (VF), this bit only
 * resets the xHC instance presented by the selected VF.
 */

#define USB_XHCI_USBCMD_HCRST_GET(uUSBCMD)    (((uUSBCMD) >> 1) & 0x1)
#define USB_XHCI_USBCMD_HCRST   (0x1 << 1)
#define USB_XHCI_RESET_MAX_WAIT_US   1000000 /* MAX wait for 1 second */

/*
 * Interrupter Enable (INTE) - RW. Default = '0'. This bit provides system software
 * with a means of enabling or disabling the host system interrupts generated
 * by Interrupters. When this bit is a '1', then Interrupter host system
 * interrupt generation is allowed, e.g. the xHC shall issue an interrupt at
 * the next interrupt threshold if the host system interrupt mechanism (e.g.
 * MSI, MSIX, etc.) is enabled. The interrupt is acknowledged by a host system
 * interrupt specific mechanism.
 *
 * When this register is exposed by a Virtual Function (VF), this bit only
 * enables the set of Interrupters assigned to the selected VF
 */

#define USB_XHCI_USBCMD_INTE_GET(uUSBCMD)    (((uUSBCMD) >> 2) & 0x1)
#define USB_XHCI_USBCMD_INTE   (0x1 << 2)

/*
 * Host System Error Enable (HSEE) - RW. Default = '0'. When this bit is a '1',
 * and the HSE bit in the USBSTS register is a '1', the xHC shall assert
 * out-of-band error signaling to the host. The signaling is acknowledged
 * by software clearing the HSE bit.
 *
 * When this register is exposed by a Virtual Function (VF), the effect of
 * the assertion of this bit on the Physical Function (PF0) is determined
 * by the VMM.
 */

#define USB_XHCI_USBCMD_HSEE_GET(uUSBCMD)    (((uUSBCMD) >> 3) & 0x1)
#define USB_XHCI_USBCMD_HSEE   (0x1 << 3)

/*
 * Light Host Controller Reset (LHCRST) - RO or RW. Optional normative. Default
 * '0'. If the Light HC Reset Capability (LHRC) bit in the HCCPARAMS register is
 * '1', then this flag allows the driver to reset the xHC without affecting the
 * state of the ports. A system software read of this bit as '0' indicates
 * the Light Host Controller Reset has completed and it is safe for software
 * to re-initialize the xHC. A software read of this bit as a '1' indicates
 * the Light Host Controller Reset has not yet completed. If not implemented,
 * a read of this flag will always return a '0'. All registers in the Auxiliary
 * well will maintain the values that had been asserted prior to the Light
 * Host Controller Reset.
 *
 * When this register is exposed by a Virtual Function (VF), this bit only
 * generates a Light Reset to the xHC instance presented by the selected VF,
 * e.g. Disable the VF's device slots and set the associated VF Run bit to
 * Stopped.
 */

#define USB_XHCI_USBCMD_LHCRST_GET(uUSBCMD)    (((uUSBCMD) >> 7) & 0x1)
#define USB_XHCI_USBCMD_LHCRST   (0x1 << 7)

/*
 * Controller Save State (CSS) - RW. Default = '0'. When written by software with
 * '1' and HCHalted (HCH) = '1', then the xHC shall save any internal state
 * that will be restored by a subsequent Restore State operation. When written
 * by software with '1' and HCHalted (HCH) = '0', or written with '0', no Save
 * State operation shall be performed. This flag always returns '0' when read.
 * Refer to the Save State Status (SSS) flag in the USBSTS register for
 * information on Save State completion. Note that undefined behavior may
 * occur if a Save State operation is initiated while Restore State Status
 * (RSS) = '1'.
 *
 * When this register is exposed by a Virtual Function (VF), this bit only
 * controls saving the state of the xHC instance presented by the selected VF.
 */

#define USB_XHCI_USBCMD_CSS_GET(uUSBCMD)    (((uUSBCMD) >> 8) & 0x1)
#define USB_XHCI_USBCMD_CSS   (0x1 << 8)

/*
 * Controller Restore State (CRS) - RW. Default = '0'. When set to '1', and
 * HCHalted (HCH) = '1', then the xHC shall perform a Restore State operation
 * and restore its internal state. When set to '1' and Run/Stop (R/S) = '1' or
 * HCHalted (HCH) = '0', or when cleared to '0', no Restore State operation
 * shall be performed. This flag always returns '0' when read. Refer to the
 * Restore State Status (RSS) flag in the USBSTS register for information
 * on Restore State completion. Note that undefined behavior may occur if a
 * Restore State operation is initiated while Save State Status (SSS) = '1'.
 *
 * When this register is exposed by a Virtual Function (VF), this bit only
 * controls restoring the state of the xHC instance presented by the selected VF.
 */

#define USB_XHCI_USBCMD_CRS_GET(uUSBCMD)    (((uUSBCMD) >> 9) & 0x1)
#define USB_XHCI_USBCMD_CRS   (0x1 << 9)

/*
 * Enable Wrap Event (EWE) - RW. Default = '0'. When set to '1', the xHC shall
 * generate a MFINDEX Wrap Event every time the MFINDEX register transitions
 * from 03FFFh to 0. When cleared to '0' no MFINDEX Wrap Events are generated.
 *
 * When this register is exposed by a Virtual Function (VF), the generation
 * of MFINDEX Wrap Events to VFs shall be emulated by the VMM.
 */

#define USB_XHCI_USBCMD_EWE_GET(uUSBCMD)    (((uUSBCMD) >> 10) & 0x1)
#define USB_XHCI_USBCMD_EWE   (0x1 << 10)

/*
 * Enable U3 MFINDEX Stop (EU3S) - RW. Default = '0'. When set to '1', the xHC
 * may stop the MFINDEX counting action if all Root Hub ports are in the U3,
 * Disconnected, Disabled, or Powered-off state. When cleared to '0' the xHC
 * may stop the MFINDEX counting action if all Root Hub ports are in the
 * Disconnected, Disabled, or Powered-off state.
 */

#define USB_XHCI_USBCMD_EU3S_GET(uUSBCMD)    (((uUSBCMD) >> 11) & 0x1)
#define USB_XHCI_USBCMD_EU3S   (0x1 << 11)

/* Interrupts to be enabled */

#define USB_XHCI_USBCMD_INTR_MASK        \
    (USB_XHCI_USBCMD_INTE |              \
     USB_XHCI_USBCMD_HSEE)

/* USB_XHCI_USBCMD bit fields - End */

/* USB_XHCI_USBSTS bit fields - Start */

/*
 * HCHalted (HCH) 每RO. Default = '1'. This bit is a '0' whenever the Run/Stop
 * (R/S) bit is a '1'. The xHC sets this bit to '1' after it has stopped
 * executing as a result of the Run/Stop (R/S) bit being cleared to '0',
 * either by software or by the xHC hardware (e.g. internal error). If this
 * bit is '1', then SOFs, microSOFs, or Isochronous Timestamp Packets (ITP)
 * shall not be generated by the xHC.
 *
 * When this register is exposed by a Virtual Function (VF), this bit only
 * reflects the Halted state of the xHC instance presented by the selected VF.
 */

#define USB_XHCI_USBSTS_HCH_GET(uUSBSTS)    (((uUSBSTS) >> 0) & 0x1)
#define USB_XHCI_USBSTS_HCH   (0x1 << 0)

/*
 * Host System Error (HSE) 每 RW1C. Default = '0'. The xHC sets this bit to '1'
 * when a serious error is detected, either internal to the xHC or during a
 * host system access involving the xHC module. (In a PCI system, conditions
 * that set this bit to '1' include PCI Parity error, PCI Master Abort, and
 * PCI Target Abort.) When this error occurs, the xHC clears the Run/Stop (R/S)
 * bit in the USBCMD register to prevent further execution of the scheduled TDs.
 * If the HSEE bit in the USBCMD register is a '1', the xHC shall also assert
 * out-of-band error signaling to the host.
 *
 * When this register is exposed by a Virtual Function (VF), the assertion of
 * this bit affects all VFs and reflects the Host System Error state of the
 * Physical Function (PF0).
 */

#define USB_XHCI_USBSTS_HSE_GET(uUSBSTS)    (((uUSBSTS) >> 2) & 0x1)
#define USB_XHCI_USBSTS_HSE   (0x1 << 2)


/*
 * Event Interrupt (EINT) 每RW1C. Default = '0'. The xHC sets this bit to '1'
 * when the Interrupt Pending (IP) bit of any Interrupter transitions from '0'
 * to '1'. Software that uses EINT shall clear it prior to clearing any IP
 * flags. A race condition will occur if software clears the IP flags then
 * clears the EINT flag, and between the operations another IP '0' to '1'
 * transition occurs. In this case the new IP transition will be lost.
 *
 * When this register is exposed by a Virtual Function (VF), this bit is the
 * logical 'OR' of the IP bits for the Interrupters assigned to the selected VF.
 * And it shall be cleared to '0' when all associated interrupter IP bits are
 * cleared, i.e. all the VF's Interrupter Event Ring(s) are empty.
 */

#define USB_XHCI_USBSTS_EINT_GET(uUSBSTS)    (((uUSBSTS) >> 3) & 0x1)
#define USB_XHCI_USBSTS_EINT   (0x1 << 3)

/*
 * Port Change Detect (PCD) 每RW1C. Default = '0'. The xHC sets this bit to a
 * '1' when any port has a change bit transition from a '0' to a '1'. This bit
 * is allowed to be maintained in the Auxiliary power well. Alternatively, it
 * is also acceptable that on a D3 to D0 transition of the xHC, this bit is
 * loaded with the OR of all of the PORTSC change bits. This bit provides
 * system software an efficient means of determining if there has been Root Hub
 * port activity.
 *
 * When this register is exposed by a Virtual Function (VF), the VMM determines
 * the state of this bit as a function of the Root Hub Ports associated with
 * the Device Slots assigned to the selected VF.
 */

#define USB_XHCI_USBSTS_PCD_GET(uUSBSTS)    (((uUSBSTS) >> 4) & 0x1)
#define USB_XHCI_USBSTS_PCD   (0x1 << 4)

/*
 * Save State Status (SSS) - RO. Default = '0'. When the Controller Save State
 * (CSS) flag in the USBCMD register is written with '1' this bit shall be set
 * to '1' and remain 1 while the xHC saves its internal state. When the Save
 * State operation is complete, this bit shall be cleared to '0'.
 *
 * When this register is exposed by a Virtual Function (VF), the VMM determines
 * the state of this bit as a function of the saving the state for the selected
 * VF.
 */

#define USB_XHCI_USBSTS_SSS_GET(uUSBSTS)    (((uUSBSTS) >> 8) & 0x1)
#define USB_XHCI_USBSTS_SSS   (0x1 << 8)

/*
 * Restore State Status (RSS) - RO. Default = '0'. When the Controller Restore
 * State (CRS) flag in the USBCMD register is written with ＆1＊ this bit shall
 * be set to ＆1＊ and remain 1 while the xHC restores its internal state. When
 * the Restore State operation is complete, this bit shall be cleared to '0'.
 *
 * When this register is exposed by a Virtual Function (VF), the VMM determines
 * the state of this bit as a function of the restoring the state for the
 * selected VF.
 */

#define USB_XHCI_USBSTS_RSS_GET(uUSBSTS)    (((uUSBSTS) >> 9) & 0x1)
#define USB_XHCI_USBSTS_RSS   (0x1 << 9)

/*
 * Save/Restore Error (SRE) - RW1C. Default = '0'. If an error occurs during
 * a Save or Restore operation this bit shall be set to '1'. This bit shall
 * be cleared to '0' when a Save or Restore operation is initiated or when
 * written with '1'.
 *
 * When this register is exposed by a Virtual Function (VF), the VMM determines
 * the state of this bit as a function of the Save/Restore completion status
 * for the selected VF.
 */

#define USB_XHCI_USBSTS_SRE_GET(uUSBSTS)    (((uUSBSTS) >> 10) & 0x1)
#define USB_XHCI_USBSTS_SRE   (0x1 << 10)

/*
 * Controller Not Ready (CNR) 每RO. Default = '1'. '0' = Ready and '1' = Not
 * Ready. Software shall not write any Doorbell or Operational register of the
 * xHC, other than the USBSTS register, until CNR = '0'. This flag is set by
 * the xHC after a Chip Hardware Reset and cleared when the xHC is ready to
 * begin accepting register writes. This flag shall remain cleared ('0') until
 * the next Chip Hardware Reset.
 */

#define USB_XHCI_USBSTS_CNR_GET(uUSBSTS)    (((uUSBSTS) >> 11) & 0x1)
#define USB_XHCI_USBSTS_CNR   (0x1 << 11)
#define USB_XHCI_USBSTS_CNR_WAIT_MS 5000

/*
 * Host Controller Error (HCE) 每RO. Default = 0. '0' = No internal xHC error
 * conditions exist and '1' = Internal xHC error condition. This flag shall
 * be set to indicate that an internal error condition has been detected which
 * requires software to reset and reinitialize the xHC.
 */

#define USB_XHCI_USBSTS_HCE_GET(uUSBSTS)    (((uUSBSTS) >> 12) & 0x1)
#define USB_XHCI_USBSTS_HCE   (0x1 << 12)

/*
 * Note: The Event Interrupt (EINT) and Port Change Detect (PCD) flags are
 * typically only used by system software for managing the xHCI when interrupts
 * are disabled or during an SMI.
 *
 * Note: The EINT flag does not generate an interrupt, it is simply a logical
 * OR of the IMAN register IP flag '0' to '1' transitions. As such, it does not
 * need to be cleared to clear an xHC interrupt.
 */

/* USB_XHCI_USBSTS bit fields - End */

/* 5.4.3 Page Size Register (PAGESIZE) */

/* USB_XHCI_PAGESIZE bit fields - Start */

/*
 * Page Size 每RO. Default = Implementation defined. This field defines the
 * page size supported by the xHC implementation. This xHC supports a page
 * size of 2^(n+12) if bit n (where 0 <= n <= 15) is Set. For example, if bit
 * 0 is Set, the xHC supports 4k byte page sizes.
 *
 * For a Virtual Function, this register reflects the page size selected in
 * the System Page Size field of the SR-IOV Extended Capability structure.
 *
 * For the Physical Function 0, this register reflects the implementation
 * dependent default xHC page size.
 *
 * Various xHC resources reference PAGESIZE to describe their minimum alignment
 * requirements. The maximum possible page size is 128M.
 */

#define USB_XHCI_PAGESIZE_BITMASK_GET(uPAGESIZE) (((uPAGESIZE) >> 0) & 0xFFFF)

/*
 * Note: The page size bit mask specifies the possible page sizes that can
 * be supported, the software can choose to support one of the sizes (mostly
 * the smalles page size, typically 4KB).
 */

/* USB_XHCI_PAGESIZE bit fields - End */

/*
 * 5.4.4 Device Notification Control Register (DNCTRL)
 *
 * This register is used by software to enable or disable the reporting of
 * the reception of specific USB Device Notification Transaction Packets.
 * A Notification Enable (Nx, where x = 0 to 15) flag is defined for each of
 * the 16 possible device notification types. If a flag is set for a specific
 * notification type, a Device Notification Event will be generated when the
 * respective notification packet is received. After reset all notifications
 * are disabled.
 *
 * This register shall be written as a Dword. Byte writes produce undefined
 * results.
 *
 * Note: Of the currently defined USB3 Device Notification Types, only the
 * FUNCTION_WAKE type is not handled automatically by the xHC. Only under
 * debug conditions would software write the DNCTRL register with a value
 * other than 0002h. Refer to section 8.5.6 in the USB3 specification for more
 * information on Notification Types
 */

/* USB_XHCI_DNCTRL bit fields - Start */

/*
 * Notification Enable (N0-N15) 每RW. When a Notification Enable bit is set,
 * a Device Notification Event will be generated when a Device Notification
 * Transaction Packet is received with the matching value in the Notification
 * Type field. For example, setting N1 to '1' enables Device Notification Event
 * generation if a Device Notification TP is received with its Notification Type
 * field set to '1' (FUNCTION_WAKE), etc.
 */

#define USB_XHCI_DNCTRL_BITMASK_GET(uDNCTRL) (((uDNCTRL) >> 0) & 0xFFFF)
#define USB_XHCI_DNCTRL_NOTIFY(uNotifyType) ((1 << (uNotifyType)) & 0xFFFF)

/* USB3 8.5.6 Device Notification (DEV_NOTIFICATION) Transaction Packet */

/*
 * Notification Type (USB3 currently only defines these types below).
 *
 * FIXME: These macros should be moved to common USB header files when ready.
 */

#define USB_DEV_NOTIFY_FUNCTION_WAKE                      0x1
#define USB_DEV_NOTIFY_LATENCY_TOLERANCE_MESSAGE          0x2
#define USB_DEV_NOTIFY_BUS_INTERVAL_ADJUSTMENT_MESSAGE    0x3

/* USB_XHCI_DNCTRL bit fields - End */

/*
 * 5.4.5 Command Ring Control Register (CRCR)
 *
 * The Command Ring Control Register provides Command Ring control and status
 * capabilities, and identifies the address and Cycle bit state of the Command
 * Ring Dequeue Pointer.
 */

/* USB_XHCI_CRCR bit fields - Start */

/*
 * Ring Cycle State (RCS) - RW. This bit identifies the value of the xHC
 * Consumer Cycle State (CCS) flag for the TRB referenced by the Command Ring
 * Pointer.
 *
 * Writes to this flag are ignored if Command Ring Running (CRR) is '1'.
 * If the CRCR is written while the Command Ring is stopped (CRR = '0'), then
 * the value of this flag shall be used to fetch the first Command TRB the
 * next time the Host Controller Doorbell register is written with the DB
 * Reason field set to Host Controller Command.
 *
 * If the CRCR is not written while the Command Ring is stopped (CRR = '0'),
 * then the Command Ring will begin fetching Command TRBs using the current
 * value of the internal Command Ring CCS flag.
 *
 * Reading this flag always returns '0'.
 */

#define USB_XHCI_CRCR_RCS_GET(uCRCR)    (((uCRCR) >> 0) & 0x1)
#define USB_XHCI_CRCR_RCS   (0x1 << 0)

/*
 * Command Stop (CS) - RW. Default = '0'. Writing a '1' to this bit shall
 * stop the operation of the Command Ring after the completion of the currently
 * executing command, and generate a Command Completion Event with the
 * Completion Code set to Command Ring Stopped and the Command TRB Pointer set
 * to the current value of the Command Ring Dequeue Pointer.
 *
 * The next write to the Host Controller Doorbell with DB Reason field set
 * to Host Controller Command shall restart the Command Ring operation.
 *
 * Writes to this flag are ignored by the xHC if Command Ring Running (CRR)='0'.
 * Reading this bit shall always return '0'.
 */

#define USB_XHCI_CRCR_CS_GET(uCRCR)    (((uCRCR) >> 1) & 0x1)
#define USB_XHCI_CRCR_CS   (0x1 << 1)

/*
 * Command Abort (CA) - RW. Default = '0'. Writing a '1' to this bit shall
 * immediately terminate the currently executing command, stop the Command Ring,
 * and generate a Command Completion Event with the Completion Code set to
 * Command Ring Stopped.
 *
 * The next write to the Host Controller Doorbell with DB Reason field set
 * to Host Controller Command shall restart the Command Ring operation.
 *
 * Writes to this flag are ignored by the xHC if Command Ring Running (CRR)='0'.
 * Reading this bit always returns ＆0＊.
 */

#define USB_XHCI_CRCR_CA_GET(uCRCR)    (((uCRCR) >> 2) & 0x1)
#define USB_XHCI_CRCR_CA   (0x1 << 2)

/*
 * Command Ring Running (CRR) - RO. Default = 0. This flag is set to '1' if
 * the Run/Stop (R/S) bit is '1' and the Host Controller Doorbell register
 * is written with the DB Reason field set to Host Controller Command. It is
 * cleared to '0' when the Command Ring is "stopped" after writing a '1'
 * to the Command Stop (CS) or Command Abort (CA) flags, or if the R/S bit
 * is cleared to '0'.
 */

#define USB_XHCI_CRCR_CRR_GET(uCRCR)    (((uCRCR) >> 3) & 0x1)
#define USB_XHCI_CRCR_CRR   (0x1 << 3)

/*
 * Command Ring Pointer - RW. Default = '0'. This field defines high order
 * bits of the initial value of the 64-bit Command Ring Dequeue Pointer.
 *
 * Writes to this field are ignored when Command Ring Running (CRR) = '1'.
 * If the CRCR is written while the Command Ring is stopped (CCR = '0'), the
 * value of this field shall be used to fetch the first Command TRB the next
 * time the Host Controller Doorbell register is written with the DB Reason
 * field set to Host Controller Command.
 *
 * If the CRCR is not written while the Command Ring is stopped (CCR = '0')
 * then the Command Ring shall begin fetching Command TRBs at the current
 * value of the internal xHC Command Ring Dequeue Pointer.
 *
 * Reading this field always returns '0'.
 */

#define USB_XHCI_CRCR_CRP_MASK   ((UINT64)~(0x3FULL))
#define USB_XHCI_CRCR_Rsvd_MASK  (0x3F)

/*
 * Note: Setting the Command Stop (CS) or Command Abort (CA) flags while
 * CRR = '1' shall generate a Command Ring Stopped Command Completion Event.
 *
 * Note: Setting both the Command Stop (CS) and Command Abort (CA) flags with
 * a single write to the CRCR while CRR = '1' shall be interpreted as a
 * Command Abort (CA) by the xHC.
 *
 * Note: The values of the internal xHC Command Ring CCS flag and Dequeue
 * Pointer are undefined after hardware reset, so these fields (RCS and CRP)
 * shall be initialized before setting USBCMD Run/Stop (R/S) to '1'.
 *
 * Note: After asserting Command Stop (CS) if the Command doorbell is rung
 * before CRR = '0', (i.e. the ring is not fully stopped), then the behavior
 * is undefined, e.g. the Command Ring may not restart. (So we need to wait
 * for the CS Command Completion Event before issue another Command.)
 */

/* USB_XHCI_CRCR bit fields - End */

/*
 * 5.4.6 Device Context Base Address Array Pointer Register (DCBAAP)
 *
 * The Device Context Base Address Array Pointer Register identifies the
 * base address of the Device Context Base Address Array.
 *
 * The memory structure referenced by this physical memory pointer is assumed
 * to be physically contiguous and 64-byte aligned
 */

/* USB_XHCI_DCBAAP bit fields - Start */

/*
 * Device Context Base Address Array Pointer - RW. Default = '0'. This field
 * defines high order bits of the 64-bit base address of the Device Context
 * Pointer Array table. A table of address pointers that reference Device
 * Context structures for the devices attached to the host.
 */

#define USB_XHCI_DCBAAP_MASK   (UINT64)(~0x3F)

/* USB_XHCI_DCBAAP bit fields - End */

/*
 * 5.4.7 Configure Register (CONFIG)
 *
 * This register defines runtime xHC configuration parameters.
 */

/* USB_XHCI_CONFIG bit fields - Start */

/*
 * Max Device Slots Enabled (MaxSlotsEn) 每RW. Default = '0'. This field
 * specifies the maximum number of enabled Device Slots. Valid values are
 * in the range of 0 to MaxSlots. Enabled Devices Slots are allocated
 * contiguously. e.g. A value of 16 specifies that Device Slots 1 to 16
 * are active. A value of '0' disables all Device Slots. A disabled Device
 * Slot shall not respond to Doorbell Register references.
 *
 * This field shall not be modified if the xHC is running (Run/Stop (R/S) = '1').
 *
 * Note: Writing the Max Device Slots Enabled (MaxSlotsEn) field with a
 * non-zero value, signals to the xHC that the host controller driver for
 * the xHC is loaded. The Run/Stop (R/S) flag in the USBCMD register can be
 * checked to determine if the driver is running.
 */

#define USB_XHCI_CONFIG_MAX_SLOTS_ENABLED_GET(uCRCR)    (((uCRCR) >> 0) & 0xFF)
#define USB_XHCI_CONFIG_MAX_SLOTS_ENABLED(n)   ((n) & 0xFF)
#define USB_XHCI_CONFIG_MAX_SLOTS_ENABLED_MASK (0xFF)

/* USB_XHCI_CONFIG bit fields - End */

/*
 * 5.4.8 Port Status and Control Register (PORTSC)
 *
 * A host controller shall implement one or more port registers. The number of
 * port registers implemented by a particular instantiation of a host
 * controller is documented in the HCSPARAMS1 register. Software uses this
 * information as an input parameter to determine how many ports need to be
 * serviced.
 *
 * This register is in the Auxiliary Power well. It is only reset by platform
 * hardware during a cold reset or in response to a Host Controller Reset
 * (HCRST).
 *
 * Software cannot change the state of the port unless Port Power (PP) is
 * asserted ('1'), regardless of the Port Power Control (PPC) capability. The
 * host is required to have power stable to the port within 20 milliseconds
 * of the '0' to '1' transition of PP. If PPC = '1' software is responsible
 * for waiting 20 ms after asserting PP, before attempting to change the state
 * of the port.
 *
 * Note: If a port has been assigned to the Debug Capability, then the port
 * shall not report device connected (i.e. CCS = '0') and enabled when the
 * Port Power Flag is '1'.
 */

/* USB_XHCI_PORTSC bit fields - Start */

/*
 * Current Connect Status (CCS) 每ROS. Default = '0'. '1' = Device is present
 * on port. '0' = No device is present. This value reflects the current state
 * of the port, and may not correspond directly to the event that caused the
 * Connect Status Change (CSC) bit to be set to '1'. This flag is '0' if PP
 * is '0'.
 */

#define USB_XHCI_PORTSC_CCS_GET(uPORTSC)    (((uPORTSC) >> 0) & 0x1)
#define USB_XHCI_PORTSC_CCS   (0x1 << 0)

/*
 * Port Enabled/Disabled (PED) 每RW1CS. Default = '0'. '1' = Enabled.
 * '0' = Disabled. Ports may only be enabled by the xHC. Software cannot enable
 * a port by writing a '1' to this flag.
 *
 * A port may be disabled by software writing a '1' to this flag.
 *
 * This flag shall automatically be cleared to '0' by a disconnect event or
 * other fault condition.
 *
 * Note that the bit status does not change until the port state actually
 * changes. There may be a delay in disabling or enabling a port due to other
 * host controller or bus events.
 *
 * When the port is disabled (PED = '0') downstream propagation of data is
 * blocked on this port, except for reset.
 *
 * For USB2 protocol ports:
 *
 * When the port is in the Disabled state, software shall reset the port
 * (PR = '1') to transition PED to '1' and the port to the Enabled state.
 *
 * For USB3 protocol ports:
 *
 * When the port is in the Polling state (after detecting an attach), the port
 * shall automatically transition to the Enabled state and set PED to '1' upon
 * the completion of successful link training.
 *
 * When the port is in the Disabled state, software shall write a '5' (RxDetect)
 * to the PLS field to transition the port to the Disconnected state.
 *
 * PED shall automatically be cleared to '0' when PR is set to '1', and set to
 * '1' when PR transitions from '1' to '0' after a successful reset. Refer to
 * Port Reset (PR) bit for more information on how the PED bit is managed.
 *
 * Note that when software writes this bit to a '1', it shall also write a '0'
 * to the PR bita. This flag is '0' if PP is '0'.
 */

#define USB_XHCI_PORTSC_PED_GET(uPORTSC)    (((uPORTSC) >> 1) & 0x1)
#define USB_XHCI_PORTSC_PED   (0x1 << 1)

/*
 * Over-current Active (OCA) 每RO. Default = '0'. '1' = This port currently
 * has an over-current condition. '0' = This port does not have an over-current
 * condition. This bit shall automatically transition from a '1' to a '0' when
 * the over-current condition is removed.
 */

#define USB_XHCI_PORTSC_OCA_GET(uPORTSC)    (((uPORTSC) >> 3) & 0x1)
#define USB_XHCI_PORTSC_OCA   (0x1 << 3)

/*
 * Port Reset (PR) 每RW1S. Default = '0'. '1' = Port Reset signaling is
 * asserted. '0' = Port is not in Reset. When software writes a '1' to this
 * bit (from a '0') the bus reset sequence is initiated;
 *
 * USB2 protocol ports shall execute the bus reset sequence as defined in
 * the USB2 Spec. USB3 protocol ports shall execute the Hot Reset sequence
 * as defined in the USB3 Spec. PR remains set until reset signaling is
 * completed by the root hub.
 *
 * Note that software shall write a '1' to the this flag to transition a USB2
 * port from the Polling state to the Enabled state.
 *
 * This flag is '0' if PP is '0'.
 */

#define USB_XHCI_PORTSC_PR_GET(uPORTSC)    (((uPORTSC) >> 4) & 0x1)
#define USB_XHCI_PORTSC_PR   (0x1 << 4)

/*
 * Port Link State (PLS) - RWS. Default = RxDetect ('5'). This field is used to
 * power manage the port and reflects its current link state.
 *
 * When the port is in the Enabled state, system software may set the link U
 * state by writing this field. System software may also write this field to
 * force a Disabled to Disconnected state transition of the port.
 *
 *   Write Value        Description
 *
 *         0            The link shall transition to a U0 state from any of the
 *                      U states.
 *         2            USB2 protocol ports only. The link should transition to
 *                      the U2 State.
 *         3            The link shall transition to a U3 state from any of the
 *                      U states. This action selectively suspends the device
 *                      connected to this port. While the Port Link State = U3,
 *                      the hub does not propagate downstream-directed traffic
 *                      to this port, but the hub will respond to resume
 *                      signaling from the port.
 *         5            USB3 protocol ports only. If the port is in the Disabled
 *                      state (PLS = Disabled, PP = 1), then the link shall
 *                      transition to a RxDetect state and the port shall
 *                      transition to the Disconnected state, else ignored.
 *     1,4,6-14         Ignored.
 *        15            USB2 protocol ports only. If the port is in the U3 state
 *                      (PLS = U3), then the link shall remain in the U3 state
 *                      and the port shall transition to the U3Exit substate,
 *                      else ignored.
 *
 * Note: The Port Link State Write Strobe (LWS) shall also be set to '1' to
 * write this field.
 *
 * For USB2 protocol ports: Writing a value of '2' to this field shall request
 * LPM, asserting L1 signaling on the USB2 bus. Software may read this field
 * to determine if the transition to the U2 state was successful. Writing a
 * value of '0' shall deassert L1 signaling on the USB. Writing a value of '1'
 * shall have no affect. The U1 state shall never be reported by a USB2 protocol
 * port.
 *
 *   Read Value         Meaning
 *         0            Link is in the U0 State
 *         1            Link is in the U1 State
 *         2            Link is in the U2 State
 *         3            Link is in the U3 State (Device Suspended)
 *         4            Link is in the Disabled State
 *         5            Link is in the RxDetect State
 *         6            Link is in the Inactive State
 *         7            Link is in the Polling State
 *         8            Link is in the Recovery State
 *         9            Link is in the Hot Reset State
 *        10            Link is in the Compliance Mode State
 *        11            Link is in the Test Mode State
 *       12:14          Reserved
 *        15            Link is in the Resume State
 *
 * This field is undefined if PP = '0'.
 *
 * Note: Transitions between different states are not reflected until the
 * transition is complete.
 */

#define USB_XHCI_PORTSC_PLS_GET(uPORTSC)    (((uPORTSC) >> 5) & 0xF)
#define USB_XHCI_PORTSC_PLS(uPLS)   (((uPLS) & 0xF) << 5)
#define USB_XHCI_PORTSC_PLS_MASK    (0xF << 5)

/*
 * Link Sate values
 *
 *(FIXME: These should be moved to USB common header file when ready.)
 */

#define USB_LINK_STATE_U0               0
#define USB_LINK_STATE_U1               1
#define USB_LINK_STATE_U2               2
#define USB_LINK_STATE_U3               3
#define USB_LINK_STATE_Disabled         4
#define USB_LINK_STATE_RxDetect         5
#define USB_LINK_STATE_Inactive         6
#define USB_LINK_STATE_Polling          7
#define USB_LINK_STATE_Recovery         8
#define USB_LINK_STATE_HotReset         9
#define USB_LINK_STATE_ComplianceMode   10
#define USB_LINK_STATE_TestMode         11
#define USB_LINK_STATE_Resume           15
/* 12-14 are reserved */

/*
 * Port Power (PP) - RWS. Default = '1'. This flag reflects a port's logical,
 * power control state. Because host controllers can implement different methods
 * of port power switching, this flag may or may not represent whether (VBus)
 * power is actually applied to the port. When PP equals a '0' the port is
 * nonfunctional and shall not report attaches, detaches, or Port Link State
 * (PLS) changes. However, the port shall report over-current conditions when
 * PP = '0' if PPC = '0'.
 *
 * 0 = This port is in the Powered-off state.
 * 1 = This port is not in the Powered-off state.
 *
 * If the Port Power Control (PPC) flag in the HCCPARAMS register is '1', then
 * xHC has port power control switches and this bit represents the current
 * setting of the switch ('0' = off, '1' = on).
 *
 * If the Port Power Control (PPC) flag in the HCCPARAMS register is '0', then
 * xHC does not have port power control switches and each port is hard wired
 * to power, and not affected by this bit.
 *
 * When an over-current condition is detected on a powered port, the xHC shall
 * transition the PP bit in each affected port from a '1' to '0' (removing
 * power from the port).
 */

#define USB_XHCI_PORTSC_PP_GET(uPORTSC)    (((uPORTSC) >> 9) & 0x1)
#define USB_XHCI_PORTSC_PP   (0x1 << 9)

/*
 * Port Speed (Port Speed) - ROS. Default = '0'. This field identifies the
 * speed of the attached USB Device. This field is only relevant if a device
 * is attached (CCS = '1') in all other cases this field shall indicate
 * Undefined Speed.
 *      Value          Meaning
 *         0           Undefined Speed
 *         1           Full-speed device attached
 *         2           Low-speed device attached
 *         3           High-speed device attached
 *         4           SuperSpeed device attached
 *       5-15          Reserved
 *
 * Note: Values the 1, 2 and 3 are exclusive to USB2 protocol ports and the
 * value 4 is exclusive to USB3 protocol ports
 */

#define USB_XHCI_PORTSC_PS_GET(uPORTSC)    (((uPORTSC) >> 10) & 0xF)
#define USB_XHCI_PORTSC_PS(uPS)   (((uPS) & 0xF) << 10)
#define USB_XHCI_PORTSC_PS_MASK   (0xF << 10)

#define USB_XHCI_PORTSC_PS_FS 1
#define USB_XHCI_PORTSC_PS_LS 2
#define USB_XHCI_PORTSC_PS_HS 3
#define USB_XHCI_PORTSC_PS_SS 4

/*
 * Port Indicator Control (PIC) - RWS. Default = 0. Writing to these bits has
 * no effect if the Port Indicators (PIND) bit in the HCCPARAMS register is a
 * '0'. If PIND bit is a '1', then the bit encodings are:
 *
 *      Value            Meaning
 *        0                   Port indicators are off
 *        1                   Amber
 *        2                   Green
 *        3                   Undefined
 *
 * Refer to the USB2 Specification for a description on how these bits are
 * to be used. This field is '0' if PP is '0'.
 */

#define USB_XHCI_PORTSC_PIC_GET(uPORTSC)    (((uPORTSC) >> 14) & 0x3)
#define USB_XHCI_PORTSC_PIC(uPIC)   (((uPIC) & 0x3) << 14)
#define USB_XHCI_PORTSC_PIC_MASK    (0x3 << 14)

/*
 * Port Link State Write Strobe (LWS) - RW. Default = '0'. When this bit is
 * set to '1' on a write reference to this register, this flag enables writes
 * to the PLS field. When '0', write data in PLS field is ignored. Reads to
 * this bit return '0'.
 */

#define USB_XHCI_PORTSC_LWS_GET(uPORTSC)    (((uPORTSC) >> 16) & 0x1)
#define USB_XHCI_PORTSC_LWS   (0x1 << 16)

/*
 * Connect Status Change (CSC) - RW1CS. Default = '0'. '1' = Change in CCS.
 * '0' = No change. This flag indicates a change has occurred in the port's
 * Current Connect Status (CCS).
 *
 * Note that this flag shall not be set if the CCS transition was due to
 * software setting PP to '0'.
 *
 * The xHC sets this bit to '1' for all changes to the port device connect
 * status, even if system software has not cleared an existing Connect Status
 * Change. For example, the insertion status changes twice before system
 * software has cleared the changed condition, root hub hardware will be
 * "setting" an already-set bit (i.e., the bit will remain '1'). Software
 * shall clear this bit by writing a '1' to it.
 */

#define USB_XHCI_PORTSC_CSC_GET(uPORTSC)    (((uPORTSC) >> 17) & 0x1)
#define USB_XHCI_PORTSC_CSC   (0x1 << 17)

/*
 * Port Enabled/Disabled Change (PEC) - RW1CS. Default = '0'. '1' = change in
 * PED. '0' = No change. Note that this flag shall not be set if the PED
 * transition was due to software setting PP to '0'. Software shall clear this
 * bit by writing a '1' to it.
 *
 * For a USB2 protocol port, this bit shall be set to '1' only when the port
 * is disabled due to the appropriate conditions existing at the EOF2 point
 * (refer to section 11.8.1 of the USB2 Specification for the definition of
 * a Port Error).
 *
 * For a USB3 protocol port, this bit shall never be set to '1'.
 */

#define USB_XHCI_PORTSC_PEC_GET(uPORTSC)    (((uPORTSC) >> 18) & 0x1)
#define USB_XHCI_PORTSC_PEC   (0x1 << 18)

/*
 * Warm Port Reset Change (WRC) - RW1CS/RsvdZ. Default = '0'. This bit is set
 * when Warm Reset processing on this port completes. '0' = No change. '1' =
 * Warm Reset complete. Note that this flag shall not be set to '1' if the
 * Warm Reset processing was forced to terminate due to software clearing PP
 * or PED to '0'. Software shall clear this bit by writing a '1' to it.
 *
 * This bit only applies to USB3 protocol ports. For USB2 protocol ports it
 * shall be RsvdZ.
 */

#define USB_XHCI_PORTSC_WRC_GET(uPORTSC)    (((uPORTSC) >> 19) & 0x1)
#define USB_XHCI_PORTSC_WRC   (0x1 << 19)

/*
 * Over-current Change (OCC) - RW1CS. Default = '0'. This bit shall be set to
 * a '1' when there is a '0' to '1' or '1' to '0' transition of Over-current
 * Active (OCA). Software shall clear this bit by writing a '1' to it.
 */

#define USB_XHCI_PORTSC_OCC_GET(uPORTSC)    (((uPORTSC) >> 20) & 0x1)
#define USB_XHCI_PORTSC_OCC   (0x1 << 20)

/*
 * Port Reset Change (PRC) - RW1CS. Default = '0'. This flag is set to '1' due
 * a '1' to '0' transition of Port Reset (PR). e.g. when any reset processing
 * (Warm or Hot) on this port is complete. Note that this flag shall not be set
 * to '1' if the reset processing was forced to terminate due to software
 * clearing PP or PED to '0'. '0' = No change. '1' = Reset complete.
 *
 * Software shall clear this bit by writing a '1' to it.
 */

#define USB_XHCI_PORTSC_PRC_GET(uPORTSC)    (((uPORTSC) >> 21) & 0x1)
#define USB_XHCI_PORTSC_PRC   (0x1 << 21)

/*
 * Port Link State Change (PLC) - RW1CS. Default = '0'. This flag is set to
 * '1' due to the following PLS transitions:
 *
 *  Transition                Condition
 *  U3 -> Resume              Wakeup signaling from a device
 *  Resume -> Recovery -> U0  Device Resume complete (USB3 protocol ports only)
 *  Resume -> U0              Device Resume complete (USB2 protocol ports only)
 *  U3 -> Recovery -> U0      Software Resume complete (USB3 protocol ports only)
 *  U3 -> U0                  Software Resume complete (USB2 protocol ports only)
 *  U2 -> U0                  L1 Resume complete (USB2 protocol ports only)
 *  U0 -> U0                  L1 Entry Reject (USB2 protocol ports only)
 *  Any state -> Inactive     Error (USB3 protocol ports only). Note: PLC is
 *                            asserted only if there is an
 *                            SS.Inactive.Disconnect.Detect to SS.Inactive.Quiet
 *                            transition in the LTSSM.
 *
 * Note that this flag shall not be set if the PLS transition was due to
 * software setting PP to '0'. '0' = No change. '1' = Link Status Changed.
 * Software shall clear this bit by writing a '1' to it.
 */

#define USB_XHCI_PORTSC_PLC_GET(uPORTSC)    (((uPORTSC) >> 22) & 0x1)
#define USB_XHCI_PORTSC_PLC   (0x1 << 22)

/*
 * Port Config Error Change (CEC) - RW1CS/RsvdZ. Default = '0'. This flag
 * indicates that the port failed to configure its link partner. 0 = No change.
 * 1 = Port Config Error detected. Software shall clear this bit by writing a
 * '1' to it.
 *
 * Note: This flag is valid only for USB3 protocol ports. For USB2 protocol
 * ports this bit shall be RsvdZ.
 */

#define USB_XHCI_PORTSC_CEC_GET(uPORTSC)    (((uPORTSC) >> 22) & 0x1)
#define USB_XHCI_PORTSC_CEC   (0x1 << 22)

/*
 * Wake on Connect Enable (WCE) - RWS. Default = '0'. Writing this bit to a
 * '1' enables the port to be sensitive to device connects as system wake-up
 * events.
 */

#define USB_XHCI_PORTSC_WCE_GET(uPORTSC)    (((uPORTSC) >> 23) & 0x1)
#define USB_XHCI_PORTSC_WCE   (0x1 << 23)

/*
 * Cold Attach Status (CAS) 每 RO. Default = '0'. '1' = Far-end Receiver 
 * Terminations were detected in the Disconnected state and the Root Hub
 * Port State Machine was unable advance to the Enabled state. Refer to 
 * sections 4.19.8 for more details on the Cold Attach Status (CAS) 
 * assertion conditions. Software shall clear this bit by writing a '1' 
 * to WPR or the xHC shall clear this bit if CCS transitions to '1'.
 * This flag is '0' if PP is '0' or for USB2 protocol ports.
 */

#define USB_XHCI_PORTSC_CAS_GET(uPORTSC)    (((uPORTSC) >> 24) & 0x1)
#define USB_XHCI_PORTSC_CAS   (0x1 << 24)

/*
 * Wake on Over-current Enable (WOE) - RWS. Default = '0'. Writing this bit to
 * a '1' enables the port to be sensitive to over-current conditions as system
 * wake-up events.
 */

#define USB_XHCI_PORTSC_WOE_GET(uPORTSC)    (((uPORTSC) >> 25) & 0x1)
#define USB_XHCI_PORTSC_WOE   (0x1 << 25)

/*
 * Wake on Disconnect Enable (WDE) - RWS. Default = '0'. Writing this bit to a
 * '1' enables the port to be sensitive to device disconnects as system wake-up
 * eventsi.
 */

#define USB_XHCI_PORTSC_WDE_GET(uPORTSC)    (((uPORTSC) >> 24) & 0x1)
#define USB_XHCI_PORTSC_WDE   (0x1 << 26)

/*
 * Device Removable (DR) - RO. This flag indicates if this port has a removable
 * device attached. '1' = Device is non-removable. '0' = Device is removable.
 */

#define USB_XHCI_PORTSC_DR_GET(uPORTSC)    (((uPORTSC) >> 30) & 0x1)
#define USB_XHCI_PORTSC_DR   (0x1 << 30)

/*
 * Warm Port Reset (WPR) - RW1S/RsvdZ. Default = '0'. When software writes a
 * '1' to this bit, the Warm Reset sequence as defined in the USB3 Specification
 * is initiated and the PR flag is set to '1'. Once initiated, the PR, PRC, and
 * WRC flags shall reflect the progress of the Warm Reset sequence. This flag
 * shall always return '0' when read.
 *
 * This flag only applies to USB3 protocol ports. For USB2 protocol ports it
 * shall be RsvdZ
 */

#define USB_XHCI_PORTSC_WPR_GET(uPORTSC)    (((uPORTSC) >> 31) & 0x1)
#define USB_XHCI_PORTSC_WPR   (UINT32)(0x1 << 31)

#define USB_XHCI_PORTSC_SC_BITS  \
    (USB_XHCI_PORTSC_CSC |          \
     USB_XHCI_PORTSC_PEC |          \
     USB_XHCI_PORTSC_WRC |          \
     USB_XHCI_PORTSC_OCC |          \
     USB_XHCI_PORTSC_PRC |          \
     USB_XHCI_PORTSC_PLC |          \
     USB_XHCI_PORTSC_CEC)

#define USB_XHCI_PORTSC_RW1CS_BITS  \
    (USB_XHCI_PORTSC_PED | USB_XHCI_PORTSC_SC_BITS)

#define USB_XHCI_PORTSC_RWS_BITS    \
    (USB_XHCI_PORTSC_PLS_MASK |     \
     USB_XHCI_PORTSC_PP |           \
     USB_XHCI_PORTSC_PIC_MASK |     \
     USB_XHCI_PORTSC_WCE |          \
     USB_XHCI_PORTSC_WDE |          \
     USB_XHCI_PORTSC_WOE)   

#define USB_XHCI_PORTSC_RO_BITS     \
    (USB_XHCI_PORTSC_CCS |          \
     USB_XHCI_PORTSC_OCA |          \
     USB_XHCI_PORTSC_PS_MASK |      \
     USB_XHCI_PORTSC_DR)

#define USB_XHCI_PORTSC_RW1S_BITS   \
    (USB_XHCI_PORTSC_PR |          \
     USB_XHCI_PORTSC_WPR)

#define USB_XHCI_PORTSC_KEEP(uPortSc) \
    (((uPortSc) & USB_XHCI_PORTSC_RO_BITS) | \
     ((uPortSc) & USB_XHCI_PORTSC_RWS_BITS))
    
/* USB_XHCI_PORTSC bit fields - End */

/*
 * 5.4.9 Port PM Status and Control Register (PORTPMSC)
 *
 * The definition of the fields in the PORTPMSC register depend on the USB
 * protocol supported by the port.
 *
 * The PORTPMSC register in USB3 mode controls the SuperSpeed USB link
 * U-State timeouts.
 *
 * The PORTPMSC register in USB2 mode provides the USB2 LPM parameters
 * necessary for the xHC to generate a LPM Token to the downstream device.
 *
 * This register is in the Auxiliary Power well. It is only reset by platform
 * hardware during a cold reset or in response to a Host Controller Reset (HCRST).
 */

/* USB_XHCI_PORTPMSC bit fields - Start */

/*
 * U1 Timeout - RWS. Default = '0'. Timeout value for U1 inactivity timer.
 * If equal to FFh, the port is disabled from initiating U1 entry. This field
 * shall be set to '0' by the assertion of PR to '1'.
 *
 * The following are permissible values:
 *
 *      Value           Description
 *       00h            Zero (default)
 *       01h            1 us.
 *       02h            2 us.
 *        ...
 *       7Fh            127 us.
 *    80h-FEh           Reserved
 *       FFh            Infinite
 *
 * Note: This only applies when the register is used for USB3.
 */

#define USB_XHCI_PORTPMSC_U1_TIMEOUT_GET(uPORTPMSC) (((uPORTPMSC) >> 0) & 0xFF)
#define USB_XHCI_PORTPMSC_U1_TIMEOUT(uTime)         (((uTime) & 0xFF) << 0)
#define USB_XHCI_PORTPMSC_U1_TIMEOUT_MASK           (0xFF)

#define USB_XHCI_U1_TIMEOUT_DISABLE_U1_ENTRY        0xFF
#define USB_XHCI_US_TO_U1_TIMEOUT(uUS)              (uUS)

/*
 * U2 Timeout - RWS. Default = '0'. Timeout value for U2 inactivity timer. If
 * equal to FFh, the port is disabled from initiating U2 entry. This field
 * shall be set to '0' by the assertion of PR to '1'.
 *
 * The following are permissible values:
 *
 *       Value           Description
 *        00h            Zero (default)
 *        01h            256 us
 *        02h            512 us
 *         ...
 *        FEh            65.024 ms
 *        FFh            Infinite
 *
 * A U2 Inactivity Timeout LMP shall be sent by the xHC to the device connected
 * on this port when this field is written.
 *
 * Note: This only applies when the register is used for USB3.
 */

#define USB_XHCI_PORTPMSC_U2_TIMEOUT_GET(uPORTPMSC) (((uPORTPMSC) >> 8) & 0xFF)
#define USB_XHCI_PORTPMSC_U2_TIMEOUT(uTime)         (((uTime) & 0xFF) << 8)
#define USB_XHCI_PORTPMSC_U2_TIMEOUT_MASK           (0xFF << 8)

#define USB_XHCI_U2_TIMEOUT_DISABLE_U2_ENTRY        0xFF

/*
 * Convert us to U2 timeout value, 0 and 0xFF will not be converted,
 * others will be converted
 */

#define USB_XHCI_US_TO_U2_TIMEOUT(uUS) \
    ((((uUS) != 0xFF) && ((uUS) != 0x0)) ? (((uUS) / 256) + 1) : 0xFF)


/*
 * Force Link PM Accept (FLA) - RW. Default = '0'. When this bit is set to
 * '1', the port shall generate a Set Link Function LMP with the
 * Force_LinkPM_Accept bit asserted.
 *
 * This flag shall be set to '0' by the assertion of PR to '1' or when
 * CCS = transitions from '0' to '1'.
 *
 * Writes to this flag have no affect if PP = '0'.
 *
 * The Set Link Function LMP is sent by the xHC to the device connected on
 * this port when this bit transitions from '0' to '1'.
 *
 * Improper use of the SS Force_LinkPM_Accept functionality can impact the
 * performance of the link significantly. This bit shall only be used for
 * compliance and testing purposes. Software shall ensure that there are no
 * pending packets at the link level before setting this bit.
 *
 * This flag is '0' if PP is '0'.
 *
 * Note: This only applies when the register is used for USB3.
 */

#define USB_XHCI_PORTPMSC_FLA_GET(uPORTPMSC)    (((uPORTPMSC) >> 16) & 0x1)
#define USB_XHCI_PORTPMSC_FLA   (0x1 << 16)

/*
 * L1 Status (L1S) - RO. Default = 0. This field is used by software to
 * determine whether an L1-based suspend request (LMP transaction) was
 * successful, specifically:
 *
 * Value    Meaning
 *
 * 0        Invalid - This field shall be ignored by software.
 * 1        Success - Port successfully transitioned to L1 (ACK)
 * 2        Not Yet - Device is unable to enter L1 at this time (NYET)
 * 3        Not Supported - Device does not support L1 transitions (STALL)
 * 4        Timeout/Error - Device failed to respond to the LPM Transaction
 *                          or an error occurred
 * 5-7      Reserved
 *
 * The value of this field is only valid when the port resides in the L0 or L1
 * state (PLS = '0' or '2').
 *
 * If the USB2 L1 Capability is not supported (L1C = '0') this field shall
 * be RsvdZ.
 *
 * Note: This only applies when the register is used for USB2.
 */

#define USB_XHCI_PORTPMSC_L1S_GET(uPORTPMSC)    (((uPORTPMSC) >> 0) & 0x7)
#define USB_XHCI_PORTPMSC_L1S(uL1S)   (((uL1S) & 0x7) << 0)

/* L1 Status values */

#define USB_L1S_Invalid         0
#define USB_L1S_Success         1
#define USB_L1S_NotYet          2
#define USB_L1S_NotSupported    3
#define USB_L1S_TimeoutError    4
/* 5-7 are Reserved */

/*
 * Remote Wake Enable (RWE) - RW. Default = '0'. The host system sets this
 * flag to enable or disable the device for remote wake from L1. The value
 * of this flag will temporarily (while in L1) override the current setting
 * of the Remote Wake feature set by the standard Set/ClearFeature()
 * commands defined in USB2 Spec, Chapter 9.
 *
 * Note: This only applies when the register is used for USB2.
 */

#define USB_XHCI_PORTPMSC_RWE_GET(uPORTPMSC)    (((uPORTPMSC) >> 3) & 0x1)
#define USB_XHCI_PORTPMSC_RWE   (0x1 << 3)

/*
 * Host Initiated Resume Duration (HIRD) - RW. Default = '0'. System software
 * sets this field to indicate to the recipient device how long the xHC will
 * drive resume if it (the xHC) initiates an exit from L1. The HIRD value is
 * encoded as follows: The value of 0000b is interpreted as 50 us. Each
 * incrementing value up adds 75 us to the previous value. For example, 0001b
 * is 125 米s, 0010b is 200 us and so on. Based on this rule, the maximum value
 * resume drive time is at encoding value 1111b which represents 1.2ms.
 *
 * Refer to Section 4.1 of the USB2 LPM spec for more information on the use
 * of the HIRD field.
 *
 * If the USB2 L1 Capability is not supported (L1C = '0') this field shall be
 * RsvdZ.
 *
 * Note: This only applies when the register is used for USB2.
 */

#define USB_XHCI_PORTPMSC_HIRD_GET(uPORTPMSC)    (((uPORTPMSC) >> 4) & 0xF)
#define USB_XHCI_PORTPMSC_HIRD(uHIRD)   (((uHIRD) & 0xF) << 4)

/* Host Initiated Resume Duration values */

#define USB_XHCI_HIRD_50US      0x0
#define USB_XHCI_HIRD_125US     0x1
#define USB_XHCI_HIRD_200US     0x2
#define USB_XHCI_HIRD_275US     0x3
#define USB_XHCI_HIRD_350US     0x4
#define USB_XHCI_HIRD_425US     0x5
#define USB_XHCI_HIRD_500US     0x6
#define USB_XHCI_HIRD_575US     0x7
#define USB_XHCI_HIRD_650US     0x8
#define USB_XHCI_HIRD_725US     0x9
#define USB_XHCI_HIRD_800US     0xA
#define USB_XHCI_HIRD_875US     0xB
#define USB_XHCI_HIRD_950US     0xC
#define USB_XHCI_HIRD_1025US    0xD
#define USB_XHCI_HIRD_1100US    0xE
#define USB_XHCI_HIRD_1175US    0xF

/*
 * L1 Device Slot - RW. Default = '0'. System software sets this field to
 * indicate the ID of the Device Slot associated with the device directly
 * attached to the Root Hub port. A value of '0' indicates no device is
 * present. The xHC uses this field to lookup information necessary to
 * generate the LMP Token packet.
 *
 * If the USB2 L1 Capability is not supported (L1C = '0') this field shall
 * be RsvdZ.
 *
 * Note: This only applies when the register is used for USB2.
 */

#define USB_XHCI_PORTPMSC_L1_DEV_SLOT_GET(uPORTPMSC) (((uPORTPMSC) >> 8) & 0xFF)
#define USB_XHCI_PORTPMSC_L1_DEV_SLOT(uSlot)   (((uSlot) & 0xFF) << 8)

/*
 * Port Test Control 每RW. Default = '0'. When this field is '0', the port is
 * NOT operating in a test mode. A non-zero value indicates that it is
 * operating in test mode and the specific test mode is indicated by the
 * specific value.
 *
 * A non-zero Port Test Control value is only valid to a port that is in the
 * Powered-Off state (PLS = Disabled). If the port is not in this state, the
 * xHC shall respond with the Port Test Control field set to Port Test Control
 * Error.
 *
 * The encoding of the Test Mode bits for a USB2 protocol port are:
 *
 * Value Test Mode
 * 0 Test mode not enabled
 * 1 Test J_STATE
 * 2 Test K_STATE
 * 3 Test SE0_NAK
 * 4 Test Packet
 * 5 Test FORCE_ENABLE
 * 6-14 Reserved.
 * 15 Port Test Control Error.
 *
 * Refer to the sections 7.1.20 and 11.24.2.13 of the USB2 spec for more
 * information on Test Modes.
 *
 * Note: This only applies when the register is used for USB2.
 */

#define USB_XHCI_PORTPMSC_PTC_GET(uPORTPMSC)    (((uPORTPMSC) >> 28) & 0xF)
#define USB_XHCI_PORTPMSC_PTC(uPTC)   (((uPTC) & 0xF) << 28)

/* Port Test Control values */

#define USB_XHCI_PTC_NON    0
/* 1-5 have been defined as USBHST_TEST_J, USBHST_TEST_K, ...etc */
/* 6-14 are Reserved */
#define USB_XHCI_PTC_ERR    15

/*
 * Note: USB2 Link Power Management is an optional normative feature. The
 * xHC supports USB2 LPM only if a USB2 xHCI Supported Protocol Capability
 * structure is declared (i.e. the Major Revision field = 02h) and the L1
 * Capability (L1C) flag is set.
 */

/* USB_XHCI_PORTPMSC bit fields - End */

/*
 * 5.4.10 Port Link Info Register (PORTLI)
 *
 * The definition of the fields in the PORTLI register depend on the USB
 * protocol supported by the port.
 *
 * The USB3 Port Link Info register reports the Link Error Count.
 *
 * The USB2 Port Link Info register is reserved and shall be treated
 * as RsvdP by software.
 */

/* USB_XHCI_PORTLI bit fields - Start */

/*
 * Link Error Count 每RO. Default = '0'. This field returns the number of
 * link errors detected by the port. This value shall be reset to '0' by the
 * assertion of a Chip Hardware Reset, HCRST, when PR transitions from '1'
 * to '0', or when CCS = transitions from '0' to '1'.
 *
 * Note: This only applies when the register is used for USB3.
 */

#define USB_XHCI_PORTLI_LINK_ERR_COUNT_GET(uPORTLI) (((uPORTLI) >> 0) & 0xFFFF)

/* USB_XHCI_PORTLI bit fields - End */


/*
 * 5.5 Host Controller Runtime Registers
 *
 * This section defines the xHCI Runtime Register space. The base address of
 * this register space is referred to as Runtime Base. The Runtime Base shall
 * be 32-byte aligned and is calculated by adding the value Runtime Register
 * Space Offset register to the Capability Base address. All Runtime registers
 * are multiples of 32 bits in length.
 *
 * Unless otherwise stated, all registers should be accessed with Dword
 * references on reads, with an appropriate software mask if needed. A software
 * read/modify/write mechanism should be invoked for partial writes.
 *
 * Software should write registers containing a Qword address field using only
 * Qword references. If a system is incapable of issuing Qword references,
 * then writes to the Qword address fields shall be performed using 2 Dword
 * references; low Dword-first, high-Dword second.
 */

#define USB_XHCI_MFINDEX     0x00   /* Microframe Index */
#define USB_XHCI_IMAN(n)     (0x20 + (n) * 32)  /* Interrupter Management n */
#define USB_XHCI_IMOD(n)     (0x24 + (n) * 32)  /* Interrupter Moderation n */
#define USB_XHCI_ERSTSZ(n)   (0x28 + (n) * 32)  /* ERST Size n */
#define USB_XHCI_ERSTBA(n)   (0x30 + (n) * 32)  /* ERST Base Address n */
#define USB_XHCI_ERDP(n)     (0x38 + (n) * 32)  /* Event Ring Dequeue Pointer n */

/*
 * 5.5.1 Microframe Index Register (MFINDEX)
 *
 * This register is used by the system software to determine the current
 * periodic frame. The register value is incremented every 125 microseconds
 * (once each microframe).
 *
 * This register is only incremented while Run/Stop (R/S) = '1'.
 *
 * The value of this register affects the SOF value generated by USB2 Bus
 * Instances.
 */

/* USB_XHCI_MFINDEX bit fields - Start */

/*
 * Microframe Index 每RO. The value in this register increments at the end
 * of each microframe (e.g. 125us.). Bits [13:3] may be used to determine
 * the current 1ms Frame Index.
 */

#define USB_XHCI_MFINDEX_MICROFRAME_GET(uMFINDEX) (((uMFINDEX) >> 0) & 0x3FFF)
#define USB_XHCI_MFINDEX_FRAME_GET(uMFINDEX) (((uMFINDEX) >> 3) & 0x7FF)

/* USB_XHCI_MFINDEX bit fields - End */

/*
 * 5.5.2.1 Interrupter Management Register (IMAN)
 *
 * The Interrupter Management register allows system software to enable,
 * disable, detect, and force xHC interrupts.
 */

/* USB_XHCI_IMAN bit fields - Start */

/*
 * Interrupt Pending (IP) - RW1C. Default = '0'. This flag represents the
 * current state of the Interrupter. If IP = '1', an interrupt is pending
 * for this Interrupter. This flag is set to '1' when IE = '1', the IMODC
 * Interrupt Moderation Counter field = '0', the Event Ring associated with
 * the Interrupter is not empty, and EHB = '0'. A '0' value indicates that
 * no interrupt is pending for the Interrupter. If MSI interrupts are enabled,
 * this flag shall be cleared automatically when the PCI Dword write generated
 * by the Interrupt assertion is complete. If PCI Pin Interrupts are enabled,
 * this flag shall be cleared by software.
 *
 * Note: In systems that do not support MSI or MSI-X, the IP bit may be
 * cleared by writing a '1' to it. Most systems have write buffers that
 * minimizes overhead, but this may require a read operation to guarantee
 * that the write has been flushed from posted buffers.
 */

#define USB_XHCI_IMAN_IP_GET(uPORTPMSC)    (((uPORTPMSC) >> 0) & 0x1)
#define USB_XHCI_IMAN_IP   (0x1 << 0)

/*
 * Interrupt Enable (IE) 每RW. Default = '0'. This flag specifies whether
 * the Interrupter is capable of generating an interrupt. When this bit and
 * the IP bit are set ('1'), the Interrupter shall generate an interrupt
 * when the Interrupter Moderation Counter reaches '0'. If this bit is '0',
 * then the Interrupter is prohibited from generating interrupts.
 */

#define USB_XHCI_IMAN_IE_GET(uPORTPMSC)    (((uPORTPMSC) >> 1) & 0x1)
#define USB_XHCI_IMAN_IE   (0x1 << 1)

/* USB_XHCI_IMAN bit fields - End */

/*
 * 5.5.2.2 Interrupter Moderation Register (IMOD)
 *
 * The Interrupter Moderation Register controls the "interrupt moderation"
 * feature of an Interrupter, allowing system software to throttle the
 * interrupt rate generated by the xHC.
 */

/* USB_XHCI_IMOD bit fields - Start */

/*
 * Interrupt Moderation Interval (IMODI) 每RW. Default = '4000' (~1ms).
 * Minimum inter-interrupt interval. The interval is specified in 250ns
 * increments. A value of '0' disables interrupt throttling logic and
 * interrupts shall be generated immediately if IP = '1', EHB = '0', and
 * the Event Ring is not empty.
 *
 * Note: Software may use the following algorithm to convert between the
 * inter-interrupt Interval value and  the common 'interrupts/sec'
 * performance metric:
 *
 * interrupts/sec = 1/(250℅10^(-9)sec * Interval)
 *
 * inter-interrupt interval = (250℅10^(-9)sec * interrupts/sec) -1
 */

#define USB_XHCI_IMOD_IMODI_GET(uIMOD)    (((uIMOD) >> 0) & 0xFFFF)
#define USB_XHCI_IMOD_IMODI(uIMODI)       (((uIMODI) & 0xFFFF) << 0)
#define USB_XHCI_IMOD_IMODI_MS(n)         (4000 * (n))
#define USB_XHCI_IMOD_IMODI_DEFAULT       (100)

/*
 * Interrupt Moderation Counter (IMODC) 每RW. Default = undefined. Down
 * counter. Loaded with Interval value whenever IP is cleared to '0', counts
 * down to '0', and stops. The associated interrupt shall be signaled whenever
 * this counter is '0', the Event Ring is not empty, the IE and IP flags = '1',
 * and EHB = '0'.
 *
 * This counter may be directly written by software at any time to alter the
 * interrupt rate.
 */

#define USB_XHCI_IMOD_IMODC_GET(uIMOD)    (((uIMOD) >> 16) & 0xFFFF)


/* USB_XHCI_IMOD bit fields - End */

/*
 * 5.5.2.3.1 Event Ring Segment Table Size Register (ERSTSZ)
 *
 * The Event Ring Segment Table Size Register defines the number of segments
 * supported by the Event Ring Segment Table.
 */

/* USB_XHCI_ERSTSZ bit fields - Start */

/*
 * Event Ring Segment Table Size 每RW. Default = '0'. This field identifies
 * the number of valid Event Ring Segment Table entries in the Event Ring
 * Segment Table pointed to by the Event Ring Segment Table Base Address
 * register. The maximum value supported by an xHC implementation for this
 * register is defined by the ERST Max field in the HSCPARAMS2 register
 *
 * For Secondary Interrupters: Writing a value of '0' to this field disables
 * the Event Ring. Any events targeted at this Event Ring when it is disabled
 * shall result in undefined behavior of the Event Ring.
 *
 * For the Primary Interrupter: Writing a value of '0' to this field shall
 * result in undefined behavior of the Event Ring. The Primary Event Ring
 * cannot be disabled.
 */

#define USB_XHCI_ERSTSZ_SIZE_GET(uERSTSZ)    (((uERSTSZ) >> 0) & 0xFFFF)
#define USB_XHCI_ERSTSZ_SIZE(uERSTSZ)        (((uERSTSZ) & 0xFFFF) << 0)
#define USB_XHCI_ERSTSZ_SIZE_MASK            0xFFFF

/* USB_XHCI_ERSTSZ bit fields - End */

/*
 * 5.5.2.3.2 Event Ring Segment Table Base Address Register (ERSTBA)
 *
 * The Event Ring Segment Table Base Address Register identifies the start
 * address of the Event Ring Segment Table.
 */

/* USB_XHCI_ERSTBA bit fields - Start */

#define USB_XHCI_ERSTBA_MASK   (0xFFFFFFFFFFFFFFF0ULL)

/* USB_XHCI_ERSTBA bit fields - End */

/*
 * 5.5.2.3.3 Event Ring Dequeue Pointer Register (ERDP)
 *
 * The Event Ring Dequeue Pointer Register is written by software to define
 * the Event Ring Dequeue Pointer location to the xHC. Software updates this
 * pointer when it is finished the evaluation of an Event(s) on the Event Ring.
 */

/* USB_XHCI_ERDP bit fields - Start */

/*
 * Dequeue ERST Segment Index (DESI). Default = '0'. This field may be used
 * by the xHC to accelerate checking the Event Ring full condition. This field
 * is written with the low order 3 bits of the offset of the ERST entry which
 * defines the Event Ring segment that Event Ring Dequeue Pointer resides in.
 *
 * Dequeue ERST Segment Index (DESI) usage:
 *
 * When software finishes processing an Event TRB, it will write the address
 * of that Event TRB to the ERDP. Before enqueuing an Event, the xHC shall
 * check that space is available on the Event Ring. This check can be skipped
 * if the xHC is currently enqueuing Event TRBs in a different ERST segment
 * than the one that software is using to dequeue Events.
 */

#define USB_XHCI_ERDP_DESI_GET(uERDP)    (((uERDP) >> 0) & 0x7)
#define USB_XHCI_ERDP_DESI(uDESI)   (((uDESI) & 0x7) << 0)

/*
 * Event Handler Busy (EHB) - RW1C. Default = '0'. This flag shall be set to '1'
 * when the IP bit is set to '1' and cleared to '0' by software when the
 * Dequeue Pointer register is written.
 */

#define USB_XHCI_ERDP_EHB_GET(uERDP)    (((uERDP) >> 3) & 0x1)
#define USB_XHCI_ERDP_EHB   (0x1 << 3)

/*
 * Event Ring Dequeue Pointer - RW. Default = '0'. This field defines the high
 * order bits of the 64-bit address of the current Event Ring Dequeue Pointer.
 */

#define USB_XHCI_ERDP_MASK   (0xF)

/* USB_XHCI_ERDP bit fields - End */

/*
 * 5.6 Doorbell Registers
 *
 * The Doorbell Array is organized as an array of up to 256 Doorbell Registers.
 * One 32-bit Doorbell Register is defined in the array for each Device Slot.
 * System software utilizes the Doorbell Register to notify the xHC that it
 * has Device Slot related work for the xHC to perform.
 *
 * The number of Doorbell Registers implemented by a particular instantiation
 * of a host controller is documented in the Number of Device Slots (MaxSlots)
 * field of the HCSPARAMS1 register.
 *
 * These registers are pointed to by the Doorbell Offset Register (DBOFF) in
 * the xhC Capability register space. The Doorbell Array base address shall
 * be Dword aligned and is calculated by adding the value in the DBOFF register
 * to "Base" (the base address of the xHCI Capability register address space).
 */

#define USB_XHCI_DOOR_BELL(n) ((n) * 4)

/* USB_XHCI_DOOR_BELL bit fields - Start */

/*
 * DB Target 每RW. Doorbell Target. This field defines the target of the
 * doorbell reference. The table below defines the xHC notification that is
 * generated by ringing the doorbell. Note that Doorbell Register 0 is
 * dedicated to Command Ring and decodes this field differently than the
 * other Doorbell Registers.
 *
 * Device Context Doorbells (1-255):
 *
 * Value    Definition
 * 0        Reserved
 * 1        Control EP 0 Enqueue Pointer Update
 * 2        EP 1 OUT Enqueue Pointer Update
 * 3        EP 1 IN Enqueue Pointer Update
 * 4        EP 2 OUT Enqueue Pointer Update
 * 5        EP 2 IN Enqueue Pointer Update
 * ＃ ...
 * 30       EP 15 OUT Enqueue Pointer Update
 * 31       EP 15 IN Enqueue Pointer Update
 * 32:247   Reserved
 * 248:255  Vendor Defined
 *
 * Host Controller Doorbell (0):
 *
 * Value    Definition
 * 0        Host Controller Command
 * 1:247    Reserved
 * 248:255  Vendor Defined
 *
 * This field returns '0' when read and should be treated as "undefined" by
 * software. When the Host Controller Doorbell (0) is written, the DB Stream
 * ID field shall be cleared to '0'.
 */

#define USB_XHCI_DOOR_BELL_DB_TARGET_GET(uDB)    (((uDB) >> 0) & 0xFF)
#define USB_XHCI_DOOR_BELL_DB_TARGET(uDbTarget)   (((uDbTarget) & 0xFF) << 0)

#define USB_XHCI_DB_RsvdZ_MASK          (0xFF << 8) /* RsvdZ field */
#define USB_XHCI_DB_TARGET_HCCMD        (0)
#define USB_XHCI_DB_TARGET_EP0          (1)
#define USB_XHCI_DB_TARGET_OUT_EP(n)    (2 * (n))
#define USB_XHCI_DB_TARGET_IN_EP(n)     (2 * (n) + 1)

/*
 * DB Stream ID - RW. Doorbell Stream ID. If the endpoint of a Device Context
 * Doorbell defines Streams, then this field shall be used to identify which
 * Stream of the endpoint the doorbell reference is targeting. System software
 * is responsible for ensuring that the value written to this field is valid.
 *
 * If the endpoint does not define Streams (MaxPStreams = 0) and a non-'0'
 * value is written to this field, the doorbell reference shall be ignored.
 * This field only applies to Device Context Doorbells and shall be cleared to
 * '0' for Host Controller Commands. This field returns '0' when read.
 */

#define USB_XHCI_DOOR_BELL_DB_STREAM_ID(uDbStreamID)   \
    (((uDbStreamID) & 0xFFFF) << 16)

/* USB_XHCI_DOOR_BELL bit fields - End */

/* xHCI Extended Capabilities */

/* xHCI Extended Capability Pointer Register bit fields - Start */

#define USB_XHCI_EXT_CAP_ID(uExtCap)    (((uExtCap) >> 0) & 0xFF)
#define USB_XHCI_EXT_CAP_NEXT(uExtCap)    (((uExtCap) >> 8) & 0xFF)
#define USB_XHCI_EXT_CAP_VAL(uExtCap)    (((uExtCap) >> 16) & 0xFFFF)

/* xHCI Extended Capability Pointer Register bit fields - End */

/* Extended capability IDs */

/* ID 0 is reserved */

/*  
 * USB Legacy Support
 *
 * This capability provides the xHCI Pre-OS to OS Handoff Synchronization support capability.
 */
 
#define USB_XHCI_EXT_CAP_LEGACY        1

/*
 * Supported Protocol
 *
 * This capability enumerates the protocols and revisions supported by this xHC. 
 * At least one of these capability structures is required for all xHC
 * implementations.
 */
 
#define USB_XHCI_EXT_CAP_PROTOCOL      2

/*
 * Extended Power Management Capability
 *
 * This capability is required for all xHC non-PCI implementations.
 */
 
#define USB_XHCI_EXT_CAP_PM            3

/*
 * I/O Virtualization
 *
 * This capability is optional-normative for xHC implementations that require
 * hardware virtualization support.
 */
 
#define USB_XHCI_EXT_CAP_VIRT          4

/*
 * Message Interrupt 
 *
 * Either this or the xHCI Extended Message Interrupt capability is required 
 * for all xHC non-PCI implementations.
 */
 
#define USB_XHCI_EXT_CAP_MSI           5

/*
 * Local Memory
 *
 * This capability is optional-normative for xHC implementations that require
 * local memory support
 */

#define USB_XHCI_EXT_CAP_LMEM          6

/* IDs 7-9 are reserved */

/*
 * USB Debug Capability
 *
 * This capability is optional-normative for xHC implementations and describes 
 * the xHCI USB Debug Capability.
 */
 
#define USB_XHCI_EXT_CAP_DEBUG         10

/* IDs 11-16 are reserved */

/*
 * Extended Message Interrupt
 *
 * Either this or the xHCI Message Interrupt capability is required for all xHC
 * non-PCI implementations.
 */
 
#define USB_XHCI_EXT_CAP_MSIX          17

/* IDs 19-191 are reserved */

/*
 * Vendor Defined (192-255)
 *
 * These IDs are available for vendor specific extensions to the xHCI.
 */

/* 7.1 USB Legacy Support Capability */

/* 7.1.1 USB Legacy Support Capability (USBLEGSUP) */

#define USB_XHCI_USBLEGSUP      (0x00)

/* 
 * HC BIOS Owned Semaphore - RW. Default = '0'. The BIOS sets this bit to
 * establish ownership of the xHC. System BIOS will set this bit to a '0' 
 * in response to a request for ownership of the xHC by system software.
 */
 
#define USB_XHCI_HC_BIOS_OWNED    (1 << 16)

/*
 * HC OS Owned Semaphore - RW. Default = '0'. System software sets this bit
 * to request ownership of the xHC. Ownership is obtained when this bit reads
 * as '1' and the HC BIOS Owned Semaphore bit reads as '0'.
 */
 
#define USB_XHCI_HC_OS_OWNED    (1 << 24)

#define USB_XHCI_PREOS_HANDLE_OFF_WAIT_MS 5000

/* 7.1.2 USB Legacy Support Control/Status (USBLEGCTLSTS) */

#define USB_XHCI_USBLEGCTLSTS    (0x04)

#define USB_XHCI_USBLEGCTLSTS_SMI_EN_MASK               \
    ((1 << 0)   | /* USB SMI Enable */                  \
     (1 << 4)   | /* SMI on Host System Error Enable */ \
     (1 << 13)  | /* SMI on OS Ownership Enable */      \
     (1 << 14)  | /* SMI on PCI Command Enable */       \
     (1 << 15))   /* SMI on BAR Enable */

#define USB_XHCI_USBLEGCTLSTS_RsvdP_MASK   \
    ((0x7 << 1) | ( 0xFF << 5) | (0x7 << 17))

#define USB_XHCI_USBLEGCTLSTS_RsvdZ_MASK   (0xFF << 21)

/* 7.2 xHCI Supported Protocol Capability */

#define USB_XHCI_SUPPORTED_PROT_VERSION    (0x00)
#define USB_XHCI_SUPPORTED_PROT_NAME       (0x04)
#define USB_XHCI_SUPPORTED_PROT_COUNT      (0x08)
#define USB_XHCI_SUPPORTED_PROT_ID(n)      (0x10 + ((n) << 2))

#define USB_XHCI_SUPPORTED_PROT_MAJOR(uVersion)     ((uVersion >> 24) & 0xFF)
#define USB_XHCI_SUPPORTED_PROT_MINOR(uVersion)     ((uVersion >> 16) & 0xFF)
#define USB_XHCI_SUPPORTED_PROT_PSIC(uCount)        ((uCount >> 28) & 0xF)
#define USB_XHCI_SUPPORTED_PROT_PORT_COUNT(uCount)  ((uCount >> 8) & 0xFF)
#define USB_XHCI_SUPPORTED_PROT_PORT_OFFSET(uCount) ((uCount >> 0) & 0xFF)

#define USB_XHCI_SUPPORTED_PROT_USB1 0x01
#define USB_XHCI_SUPPORTED_PROT_USB2 0x02
#define USB_XHCI_SUPPORTED_PROT_USB3 0x03

/* 7.3 xHCI Extended Power Management Capability */

#define USB_XHCI_PMC            (0x00)
#define USB_XHCI_PMCSR          (0x04)

/* PCI Vendor ID */

#define USB_XHCD_VENDOR_NEC         (0x1033)
#define USB_XHCD_VENDOR_FRESCO      (0x1B73)
#define USB_XHCD_VENDOR_INTEL       (0x8086)

/* PCI Device ID */
#define USB_XHCD_DEVICE_INTEL_7_SERIES_PCH       (0x1E31) /* PANTHER POINT */
#define USB_XHCD_DEVICE_INTEL_8_SERIES_PCH       (0x8C31) /* LYNX POINT */
#define USB_XHCD_DEVICE_INTEL_9_SERIES_PCH       (0x8CB1)

/* Intel Panther Point chipset has some private PCI config registers */

#define USB_XHCI_INTEL_XOCM         0xC0 /* USB 3.0 Port SuperSpeed Enable Register */
#define USB_XHCI_INTEL_XUSB2PR      0xD0 /* xHC USB 2.0 Port Routing Register */
#define USB_XHCI_INTEL_XUSB2PRM     0xD4 /* xHC USB 2.0 Port Routing Mask Register */
#define USB_XHCI_INTEL_USB3_PSSEN   0xD8 /* USB 3.0 Port SuperSpeed Enable Register */
#define USB_XHCI_INTEL_USB3PRM      0xDC /* USB 3.0 Port Routing Mask Register */

#ifdef __cplusplus
}
#endif
#endif  /* __INCusbXhcdRegistersh */


