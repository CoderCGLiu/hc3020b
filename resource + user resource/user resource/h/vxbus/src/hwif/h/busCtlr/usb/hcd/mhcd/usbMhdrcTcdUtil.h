/* usbMhdrcTcdUtil.h - USB Mentor Graphics TCD utility module */

/*
 * Copyright (c) 2010, 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01c,23mar11,m_y  add usbMhdrcRequestReserve and usbMhdrcRequestRelease routine
01b,09mar11,s_z  Code clean up
01a,20oct10,m_y  write
*/

/*
DESCRIPTION

This file contains the prototypes of the utility functions
which are used by the USB Mentor Graphics Target Controller Driver. 

*/

#ifndef __INCusbMhdrcTcdUtilh
#define __INCusbMhdrcTcdUtilh

#ifdef  __cplusplus
extern "C" {
#endif

/* defines */

#define USB_MHDRC_TCD_CALL_ERP_CALLBACK(pErp, nStatus)                     \
    {                                                                      \
    if (pErp != NULL)                                                      \
        {                                                                  \
        if (pErp->tcdPtr != NULL)                                         \
            {                                                              \
            pErp->result = nStatus;                                        \
            pErp->tcdPtr = NULL;                                          \
            if (pErp->targCallback != NULL)                                \
                pErp->targCallback(pErp);                                  \
            }                                                              \
        }                                                                  \
    }

#define USB_MHDRC_TCD_VAILD_TCDDATA_PARAMETER(pTCDData)                  \
    do {                                                                 \
    if (pTCDData == NULL)                                                \
        {                                                                \
        USB_MHDRC_ERR("Invalid parameter: pTCDData\n", 1, 2, 3, 4, 5, 6);\
        return;                                                          \
        }                                                                \
    if (pTCDData->pMHDRC == NULL)                                        \
        {                                                                \
        USB_MHDRC_ERR("Invalid parameter: pTCDData->pMHDRC\n",           \
                      1, 2, 3, 4, 5, 6);                                 \
        return;                                                          \
        }                                                                \
    if (pTCDData->pTCD == NULL)                                          \
        {                                                                \
        USB_MHDRC_ERR("Invalid parameter: pTCDData->pTCD\n",             \
                      1, 2, 3, 4, 5, 6);                                 \
        return;                                                          \
        }                                                                \
    }while(0);

#define USB_MHDRC_COMMON_VAILD_TCD_PARAMETER(pTCD)                       \
    {                                                                    \
    pUSB_MHDRC_TCD_DATA pTempTcdData;                                    \
    if (pTCD == NULL)                                                    \
        {                                                                \
        USB_MHDRC_ERR("Invalid parameter: pTCD\n", 1, 2, 3, 4, 5, 6);    \
        return ERROR;                                                    \
        }                                                                \
    if ((pTempTcdData = pTCD->pTcdSpecific) == NULL)                     \
        {                                                                \
        USB_MHDRC_ERR("Invalid parameter: pTCD->pTcdSpecific\n",         \
                      1, 2, 3, 4, 5, 6);                                 \
        return ERROR;                                                    \
        }                                                                \
    if (pTempTcdData->pMHDRC == NULL)                                    \
        {                                                                \
        USB_MHDRC_ERR("Invalid parameter: pMHDRC\n",1, 2, 3, 4, 5, 6);   \
        return ERROR;                                                    \
        }                                                                \
    }

/* typedefs */

/* declartion */

pUSB_MHDRC_TCD_REQUEST usbMhdrcTcdFirstReqGet
    (
    pUSB_MHDRC_TCD_PIPE pTCDPipe
    );
pUSB_MHDRC_TCD_REQUEST usbMhdrcTcdNextReqGet
    (
    pUSB_MHDRC_TCD_REQUEST pRequest
    );
pUSB_MHDRC_TCD_REQUEST usbMhdrcRequestReserve
    (
    pUSB_MHDRC_TCD_PIPE pTCDPipe
    );
void usbMhdrcRequestRelease
    (
    pUSB_MHDRC_TCD_PIPE    pTCDPipe,
    pUSB_MHDRC_TCD_REQUEST pRequest
    );
pUSB_MHDRC_TCD_PIPE usbMhdrcTcdGetPipeByEpAddr
    (
    pUSB_MHDRC_TCD_DATA pTCDData,
    int                 uEpAddr,
    int                 uEpDir
    );

void usbMhdrcTcdReqComplete
    (
    pUSB_MHDRC_TCD_REQUEST   pRequest,
    int                      status
    );

void usbMhdrcTcdPipeFlush
    (
    pUSB_MHDRC_TCD_PIPE      pTCDPipe,
    int                      status
    );
#endif /* __INCusbMhdrcTcdUtilh */
