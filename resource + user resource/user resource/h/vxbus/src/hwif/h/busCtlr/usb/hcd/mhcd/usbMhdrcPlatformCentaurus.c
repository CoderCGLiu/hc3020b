/* usbMhdrcCentaurus.c - platform Centaurus configuration for Mentor Graphics HCD  */

/*
 * Copyright (c) 2011, 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01d,03may13,wyy  Remove compiler warning (WIND00356717)
01c,31jan13,ljg  add usb support for ti816x EVM
01b,29jun11,jws  removed the SESSION bit in DEVCTL register for target mode.
01a,14jun11,jws  created for CENTAURUS
*/

/*
DESCRIPTION

This file provides the centaurus platform related hardware configure routines
for the MHDRC. It also proviedes the setup routine to configure AM387X platforms.

INCLUDE FILES: usb/usb.h, usb/usbOsal.h, usb/usbHst.h, usb/usbd.h,
               usbMhdrc.h, usbMhdrcHcd.h, usbMhdrcPlatform.h
*/

/* includes */

#include <usb/usb.h>
#include <usb/usbOsal.h>
#include <usb/usbHst.h>
#include <usb/usbd.h>
#include "usbMhdrc.h"
#include "usbMhdrcHcd.h"
#include "usbMhdrcPlatform.h"

/*******************************************************************************
*
* usbMhdrcCentaurusIsr - interrupt handler for handling MHCI controller interrupts
*
* This routine is registered as the interrupt handler for handling MHCI
* controller interrupts.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcCentaurusIsr
    (
    void * pMhdrc
    )
    {
    pUSB_MUSBMHDRC pMHDRC = (pUSB_MUSBMHDRC)pMhdrc;
    UINT16         uIntTx = 0;
    UINT16         uIntRx = 0;
    UINT8          uIntUsb = 0;
    UINT32         uIntIqrSts0 = 0;
    UINT32         uIntIqrSts1 = 0;

    /* Check the validity of the parameter */

    if ((NULL == pMHDRC) ||
        (NULL == pMHDRC->pDev))
        {

        USB_MHDRC_ERR("usbMhdrcCentaurusIsr(): "
                     "Invalid parameter, %s is NULL\n",
                     ((NULL == pMHDRC) ? "pMHDRC" :
                     "pMHDRC->pDev"),
                     2, 3, 4, 5 ,6);
        return;
        }

    if (pMHDRC->isrMagic != USB_MHDRC_MAGIC_ALIVE)
        {
        USB_MHDRC_ERR("usbMhdrcCentaurusIsr(): "
                     "ISR not active, maybe shared?\n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }

    /* read status */

    uIntIqrSts0 = USB_MHDRC_CENTAURUS_PRIVATE_REG_READ32(pMHDRC, USB_MHDRC_CENTAURUS_IRQSTATRAW0);
    uIntIqrSts1 = USB_MHDRC_CENTAURUS_PRIVATE_REG_READ32(pMHDRC, USB_MHDRC_CENTAURUS_IRQSTATRAW1);

    uIntTx = (UINT16)(uIntIqrSts0 & 0xffff);
    uIntRx = (UINT16)((uIntIqrSts0 >> 16) & 0xffff);
    uIntUsb = (UINT8)(uIntIqrSts1 & 0xffff);

    /* clear interrupt source */
    USB_MHDRC_CENTAURUS_PRIVATE_REG_WRITE32 (pMHDRC,
                         USB_MHDRC_CENTAURUS_IRQSTAT0,
                         uIntIqrSts0
                         );

    /* clear interrupt source */
    USB_MHDRC_CENTAURUS_PRIVATE_REG_WRITE32 (pMHDRC,
                         USB_MHDRC_CENTAURUS_IRQSTAT1,
                         uIntIqrSts1
                         );

    if (uIntTx)
        {
        pMHDRC->uTxIrqStatus |= uIntTx;
        }

    if (uIntRx)
        {
        pMHDRC->uRxIrqStatus |= uIntRx;
        }

    if (uIntUsb)
        {
        pMHDRC->uUsbIrqStatus |= uIntUsb;
        }

    /* Signal the ISR event */

    if (uIntTx | uIntRx | uIntUsb)
        {
        USB_MHDRC_VDBG("usbMhdrcCentaurusIsr(): "
                      "Interrupt Registers uIntTx %p uIntRx %p, uIntUsb %p\n",
                       uIntTx, uIntRx, uIntUsb, 4, 5 ,6);

        if (pMHDRC->pHCDData != NULL)
            USB_MHDRC_HCD_REPORT_EVENT(pMHDRC, (pUSB_MHCD_DATA)pMHDRC->pHCDData);
        if (pMHDRC->pTCDData != NULL)
            USB_MHDRC_TCD_REPORT_EVENT(pMHDRC, (pUSB_MHDRC_TCD_DATA)pMHDRC->pTCDData);
        }

    return;
    }

/*******************************************************************************
*
* usbMhdrcCentaurusIsrProcessDone - disable USB MHCI host controller interrupt
*
* This routine is used as callback to disable MHCI host controller interrupt.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcCentaurusIsrProcessDone
    (
    void * pMhdrc
    )
    {
    pUSB_MUSBMHDRC  pMHDRC = (pUSB_MUSBMHDRC)pMhdrc;

    /* Check the validity of the parameter */

    if (NULL == pMHDRC)
        {
        USB_MHDRC_ERR("usbMhdrcCentaurusIsrProcessDone(): "
                     "Invalid parameter, pMHDRC is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }

    /*
     * Clear EOIR to acknowledge the completion of the USB core interrupt
     */

    USB_MHDRC_CENTAURUS_PRIVATE_REG_WRITE32 (pMHDRC,
                                  USB_MHDRC_CENTAURUS_IRQEOI,
                                  0);
    USB_MHDRC_VDBG("usbMhdrcCentaurusIsrProcessDone(): "
                   "EOI issued\n",
                   1, 2, 3, 4, 5 ,6);

    }

/*******************************************************************************
*
* usbMhdrcCentaurusInterruptEnable - enable MHCI USB host controller interrupt
*
* This routine is used as callback to enable MHCI USB host controller interrupt.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcCentaurusInterruptEnable
    (
    void * pMhdrc
    )
    {
    pUSB_MUSBMHDRC pMHDRC = (pUSB_MUSBMHDRC)pMhdrc;

    /* Check the validity of the parameter */

    if (NULL == pMHDRC)
        {
        USB_MHDRC_ERR("usbMhdrcCentaurusInterruptEnable(): "
                     "Invalid parameter, pMHDRC is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }

    /* Set USB interrupt in INTRTXE register */

    USB_MHDRC_REG_WRITE16 (pMHDRC,
                          USB_MHDRC_INTRTXE,
                          USB_MHDRC_INTMSKCLR_TX_MASK);

    /* Set USB interrupt in INTRRXE register */

    USB_MHDRC_REG_WRITE16 (pMHDRC,
                          USB_MHDRC_INTRRXE,
                          USB_MHDRC_INTMSKCLR_RX_MASK);

    /* Set USB interrupt in INTRUSBE register, except SOF interrupt */

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                         USB_MHDRC_INTRUSBE,
                         USB_MHDRC_INTMSKCLR_SOF_MASK);

    /* Disable USB2.0 Test Modes */

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                         USB_MHDRC_TESTMODE,
                         0x00);

    /* Put into basic highspeed mode, set SESSION */

    USB_MHDRC_REG_WRITE8 (pMHDRC, USB_MHDRC_POWER,
                          USB_MHDRC_REG_READ8(pMHDRC, USB_MHDRC_POWER) |
                          USB_MHDRC_POWER_HSEN);

    /* Set USB Core interrupt in IRQENABLESET0 register */

    USB_MHDRC_CENTAURUS_PRIVATE_REG_WRITE32(pMHDRC,
                          USB_MHDRC_CENTAURUS_IRQENABLESET0,
                          0xFFFFFFFF);

    /* Set USB Core interrupt in IRQENABLESET1 register except SOF */

    USB_MHDRC_CENTAURUS_PRIVATE_REG_WRITE32(pMHDRC,
                         USB_MHDRC_CENTAURUS_IRQENABLESET1,
                         ~USB_MHDRC_CENTAURUS_IRQENABLE_SOF);

    USB_MHDRC_DBG("usbMhdrcCentaurusInterruptEnable(): "
                  "Enable Done\n", 1, 2, 3, 4, 5, 6);

    return;
    }


/*******************************************************************************
*
* usbMhdrcCentaurusInterruptDisable - disable USB MHCI host controller interrupt
*
* This routine is used as callback to disable MHCI host controller interrupt.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcCentaurusInterruptDisable
    (
    void * pMhdrc
    )
    {
    pUSB_MUSBMHDRC  pMHDRC = (pUSB_MUSBMHDRC)pMhdrc;
    UINT16 uReg16;
    UINT8  uReg8;

    if (NULL == pMHDRC)
        {
        USB_MHDRC_ERR("usbMhdrcCentaurusInterruptDisable(): "
                     "Invalid parameter, pMHDRC is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }


   /* Disable USB Rx/Tx interrupt in IRQENABLESET0 register 4 RX Endpoint + 4 TX Endpoint + EP0*/

    USB_MHDRC_CENTAURUS_PRIVATE_REG_WRITE32(pMHDRC,
                          USB_MHDRC_CENTAURUS_IRQENABLECLR0,
                          0xffffffff);

    /* Disable USB Core interrupt in IRQENABLESET1 register, except SOF interrupt */

    USB_MHDRC_CENTAURUS_PRIVATE_REG_WRITE32(pMHDRC,
                         USB_MHDRC_CENTAURUS_IRQENABLECLR1,
                         0xffffffff);

    /* Mask USB Tx interrupt in INTRTXE */

    USB_MHDRC_REG_WRITE16 (pMHDRC,
                          USB_MHDRC_INTRTXE,
                          0);

    /* Mask USB Rx interrupt in INTRRXE */

    USB_MHDRC_REG_WRITE16 (pMHDRC,
                          USB_MHDRC_INTRRXE,
                          0);

    /* Mask USB core interrupt in USB_MHDRC_INTRUSBE */

    USB_MHDRC_REG_WRITE8 (pMHDRC,
                         USB_MHDRC_INTRUSBE,
                         0);


    USB_MHDRC_CENTAURUS_PRIVATE_REG_WRITE32 (pMHDRC,
                                  USB_MHDRC_CENTAURUS_IRQEOI,
                                  0);

    /* Read the register to clean the 3 registers */

    uReg16 = USB_MHDRC_REG_READ16 (pMHDRC,
                         USB_MHDRC_INTRTX);


    uReg16 =  USB_MHDRC_REG_READ16 (pMHDRC,
                         USB_MHDRC_INTRRX);

    uReg8 =  USB_MHDRC_REG_READ8 (pMHDRC,
                         USB_MHDRC_INTRUSB);

    USB_MHDRC_DBG("usbMhdrcCentaurusInterruptDisable(): "
                 "Disable Done\n",
                 1, 2, 3, 4, 5 ,6);

    return ;
    }


/*******************************************************************************
*
* usbMhdrcCentaurusSetup - configure centaurus platform
*
* This routine configures centaurus platform information.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcCentaurusSetup
    (
    void * pMhdrc,
    void * pPlatformInfo
    )
    {
    pUSB_MUSBMHDRC  pMHDRC = (pUSB_MUSBMHDRC)pMhdrc;
    pUSB_MHDRC_PLATFORM_DATA pPlatformData =
                  (pUSB_MHDRC_PLATFORM_DATA)pPlatformInfo;

    if (NULL == pMHDRC || NULL == pPlatformData)
        {
        USB_MHDRC_ERR("usbMhdrcCentaurusSetup(): "
                     "Invalid parameter, pMHDRC is NULL\n",
                     1, 2, 3, 4, 5 ,6);

        return;
        }

    /* Fill up the platform configure information */

    pPlatformData->uComRegOffset = USB_MHDRC_CENTAURUS_COMMON_REG_OFFSET;
    pPlatformData->pUsbCoreIsr = usbMhdrcCentaurusIsr;
    pPlatformData->pUsbIsrDone = usbMhdrcCentaurusIsrProcessDone;
    pPlatformData->pHwInterruptEnable = usbMhdrcCentaurusInterruptEnable;
    pPlatformData->pHwInterruptDisable = usbMhdrcCentaurusInterruptDisable;

    return;
    }

/*******************************************************************************
*
* usbMhdrcCentaurusModeChange - enable the hardware by user configure mode
*
* This routine enables the hardware by user configure mode.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcCentaurusModeChange
    (
    void * pMhdrc,
    UINT8  mode
    )
    {
    pUSB_MUSBMHDRC  pMHDRC = (pUSB_MUSBMHDRC)pMhdrc;

    if ((NULL == pMhdrc) || (mode > 2))
        {
        USB_MHDRC_ERR("Invalid parameter \n", 1, 2, 3, 4, 5, 6);
        return;
        }

    /*
     * This a workaroud to make the hardware work in right mode
     * Quoted from the datasheet:
     * Via the setting of the USB0MODE[Bit7=Iddig_Mux], the user has the
     * capability of allowing the control to be coming from a register field
     * (USB0MODE[Bit8=iddig]) or from the USB_ID pin which is indirectly
     * controlled by the cable end. if USB0MODE[iddig_mux]=0, the state of the
     * USB_ID pin controls the role the controller assumes. However,
     * if USBMODE[Iddig_mux]=1, then the setting USB0MODE[iddig] will control
     * the role the controller assumes.
     * Quoted end.
     * This means the work mode can be set by softwore or by ID_pin if we set
     * Bit7(iddig_mux) to 0. We find the detection of the ID_pin is not correct.
     * And, the working mode must be-reset according to the user's configuration.
     * We will re-set the working mode according to the user's configuration
     * after the system bootup.
     */

    if (mode == USBOTG_MODE_HOST)
        {
        if (pMHDRC->uPlatformType == USB_MHDRC_PLATFORM_CENTAURUS_TI816X)
            {
            /* The bit7 should be set 1 for ti816x. */

            USB_MHDRC_CENTAURUS_PRIVATE_REG_WRITE32(pMHDRC,
                                                    USB_MHDRC_CENTAURUS_MODE,
                                                    0x80 | USB_MHDRC_CENTAURUS_MODE_IDDIG_A);
            }
        else
            {
            USB_MHDRC_CENTAURUS_PRIVATE_REG_WRITE32(pMHDRC,
                                                    USB_MHDRC_CENTAURUS_MODE,
                                                    USB_MHDRC_CENTAURUS_MODE_IDDIG_A);
            }
        }
    else if (mode == USBOTG_MODE_PERIPHERAL)
        {
        USB_MHDRC_CENTAURUS_PRIVATE_REG_WRITE32(pMHDRC,
                                                USB_MHDRC_CENTAURUS_MODE,
                                                USB_MHDRC_CENTAURUS_MODE_IDDIG_B);
        }

    if (pMHDRC->uPlatformType == USB_MHDRC_PLATFORM_CENTAURUS_TI816X)
        {
        /* The bit21 should be set 1 for ti816x. */

        USB_MHDRC_CENTAURUS_PRIVATE_REG_WRITE32(pMHDRC,
                                                USB_MHDRC_CENTAURUS_UTMI,
                                                0x00000002);
        }
    else
        {
        USB_MHDRC_CENTAURUS_PRIVATE_REG_WRITE32(pMHDRC,
                                                USB_MHDRC_CENTAURUS_UTMI,
                                                0x00200002);
        }
    USB_MHDRC_CENTAURUS_PRIVATE_REG_WRITE32(pMHDRC,
                                            USB_MHDRC_CENTAURUS_IRQEOI,
                                            0x00000000);

    USB_MHDRC_CENTAURUS_PRIVATE_REG_WRITE32(pMHDRC,
                                            USB_MHDRC_CENTAURUS_IRQMSTAT,
                                            0x00000000);
    return;
    }
