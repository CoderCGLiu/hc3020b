/* vxbLsiSRio.h - LSI Serial RapdIO Controller header file */

/*
 * Copyright (c) 2012  Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
DESCRIPTION
This file contains the definitons for LSI SRIO module.
*/

/*
modification history
--------------------
01a,03sep12,j_z  written
*/

#ifndef __INCvxbLsiSRioH
#define __INCvxbLsiSRioH

#ifdef __cplusplus
extern "C" {
#endif

#include <../src/hwif/h/vxbus/vxbRapidIO.h>

#define LSI_SRIO_HOST          "lsiSRio"

#define RAB_VER                  0x0000 /*- Bridge Version Register  */
#define RAB_BAPA                 0x0004 /* - Bridge Capability Register  */
#define RAB_CTRL                 0x0008 /* - Bridge Control Register Register */
   #define RAB_RPIO_EN                  0x00000002
   #define RAB_APIO_EN                  0x00000001
#define RAB_STAT                 0x000c /* - Bridge Status Register */
#define RAB_AXI_TIMEOUT          0x0010 /*- AXI Timeout Register  */
#define RAB_DME_TIMEOUT          0x0014 /*- DME Timeout Register  */
#define RAB_RST_CTLR             0x0018 /*- Engine Reset Control Register  */
   #define RAB_RST_RIO_PIO              0x00000100
   #define RAB_RST_AXI_PIO              0x00000001
   #define RAB_RST_RIO                  (RAB_RST_RIO_PIO | RAB_RST_AXI_PIO)
#define RAB_COOP_LOCK            0x001c /*- Cooperative Lock Control Register */
#define RAB_STAT_ESEL            0x0020 /*- Statistics Engine Selection Register */
#define RAB_STAT_STAT            0x0024 /* - Statistics Status Register */
#define RAB_IB_PW_CSR            0x0028 /* - Inbound Port Write Control Register  */
#define RAB_IN_PW_DATA           0x002c /*- Inbound Port Write Data Register */
#define RAB_APB_CSR              0x0030 /*- APB Control Status Register  */
#define RAB_ARB_TIMEOUT          0x0034 /*- Arbitration Timeout Register  */
#define RAB_INTR_ENABLE_GNRL     0x0040 /*- General Interrupt Enable Register  */
   #define EN_INTR_OB_IDB               0x80000000
   #define EN_INTR_MISC                 0x00000040
   #define EN_INTR_OB_DME               0x00000020
   #define EN_INTR_IB_DME               0x00000010
   #define EN_INTR_RPIO                 0x00000002
   #define EN_INTR_APIO                 0x00000001
#define RAB_INTR_ENABLE_APIO     0x0044 /* - AXI PIO Interrupt Enable Register  */
#define RAB_INTR_ENABLE_RPIO     0x0048 /* - RIO PIO Interrupt Enable Register  */
#define RAB_INTR_ENABLE_IDME     0x0054 /* - Inbound DME Interrupt Enable Register  */
#define RAB_INTR_ENABLE_ODME     0x0058 /* - Outbound DME Interrupt Enable Register  */
#define RAB_INTR_ENABLE_MISC     0x005c /* - Misc Interrupt Enable Register  */
   #define EN_INTR_OB_DB         0x00000002
   #define EN_INTR_IB_DB         0x00000001
#define RAB_INTR_STAT_GNRL       0x0060 /* - General Interrupt Status Register  */
   #define GNRL_INT_STAT_INTERNAL       0x80000000
   #define GNRL_INT_STAT_MISC           0x00000040
   #define GNRL_INT_STAT_OBDME          0x00000020
   #define GNRL_INT_STAT_IBDME          0x00000010
#define RAB_INTR_STAT_APIO       0x0064 /* - AXI PIO Interrupt Status Register  */
#define RAB_INTR_STAT_RPIO       0x0068 /* - RIO PIO Interrupt Status Register  */
#define RAB_INTR_STAT_IDME       0x0074 /* - Inbound DME Interrupt Status Register  */
#define RAB_INTR_STAT_ODME       0x0078 /* - Outbound DME Interrupt Status Register  */
#define RAB_INTR_STAT_MISC       0x007c /*- Misc Interrupt Status Register  */
   #define MISC_INT_STAT_OBDB           0x00000002
   #define MISC_INT_STAT_IBDB           0x00000001
#define RAB_RPIO_CTRL            0x0080 /* - RIO PIO Control Register  */
   #define RAB_RPIO_ENABLE              0x00000001
#define RAB_RPIO_STAT            0x0084 /* - RIO PIO Status Register  */
#define RAB_RPIO_AMAP_LUT(x)     0x0100 + ((x)*4) /*- RIO PIO Address Mapping Look Up Table Entry  */
#define RAB_RPIO_AMAP_IDSL       0x0140 /* - RIO PIO Address Mapping LUT index select Register  */
    #define RAB_RAPIO_IDSL_BIT33_BIT30  0x1
    #define RAB_RIO_LUT_SEL_SHIT        30
#define RAB_RPIO_AMAP_BYPS       0x0144 /* - RIO PIO Address Mappiing Bypass Register  */
    #define RAB_RPIO_BYPS_ADDBIT_SEC_IDSL  0x1
#define RAB_APIO_CTRL            0x0180 /*- AXI PIO Control Register  */
   #define RAB_APIO_MEM_EN              0x00000002 /*  NWRITE, SWITE, NREAD */
   #define RAB_APIO_MAINT_EN            0x00000004
   #define RAB_APIO_PIO_EN              0x00000001
#define RAB_APIO_STAT            0x0184 /*- AXI PIO Status Register  */
#define RAB_ASLV_STAT_CMD        0x01c0 /*- AMBA Slave Status Register */
#define RAB_ASLV_STAT_ADDR       0x01c4 /* - AMBA Slave Register First Error Address  */
#define RAB_AMAST_STAT           0x01e0 /*- AMBA Master Status Register */
#define AMAP_CTLR(x)            (0x200 + (x)*0x10)
   #define AMAP_WIN_EN                  0x00000001
#define AMAP_SIZE(x)            (0x204 + (x)*0x10)
#define AMAP_ABAR(x)            (0x208 + (x)*0x10)
#define AMAP_RBAR(x)            (0x20C + (x)*0x10)
#define RAB_OBDB_N_CSR(x)       (0x400 + (x)*0x8)
#define RAB_OBDB_N_INFO(x)      (0x404 + (x)*0x4)
#define RAB_OB_IDB_CSR           0x0478 /* - Outbound Interrupt Doorbell Control status Register */
#define RAB_OB_IDB_INFO          0x047c /*- Outbound Interrupt Doorbell Information Register  */
#define RAB_IB_DB_CSR            0x0480 /* - Inbound Doorbell Control status Register  */
#define RAB_IB_DB_INFO           0x0484 /* - Inbound Doorbell Information Register  */

#define RAB_OB_DME_CTRL(x)      (0x0500 + (x)* 0x10)
#define RAB_OB_DME_WAKEUP               0x00000002
#define RAB_OB_DME_ENABLE               0x00000001

#define RAB_OB_DME_DESC_ADRS(x) (0x0504 + (x)* 0x10)
#define RAB_OB_DME_STAT(x)      (0x0508 + (x)* 0x10)
   #define RAB_OB_DME_PEND               0x00000100
   #define RAB_OB_DME_ERR_DATA_TRANS     0x00000020
   #define RAB_OB_DME_ERR_DESC_UPD       0x00000010
   #define RAB_OB_DME_ERR_DESC           0x00000008
   #define RAB_OB_DME_ERR_DESC_FETCH     0x00000004

#define RAB_OB_DME_ERR (RAB_OB_DME_ERR_DATA_TRANS | RAB_OB_DME_ERR_DESC_UPD | \
    RAB_OB_DME_ERR_DESC|RAB_OB_DME_ERR_DESC_FETCH)

#define RAB_OB_DME_DESC(x)      (0x050C + (x)* 0x10)
#define RAB_OB_DME_TID_MASK(x)  (0x05f0 + (x)* 0x10)

#define RAB_IB_DME_CTRL(x)      (0x0600 + (x)* 0x10)
   #define RAB_IB_DME_WAKEUP            0x00000001
   #define RAB_IB_DME_ENABLE            0x00000002
   #define RAB_IB_DME_LETTER_SHIT       4
   #define RAB_IB_DME_MBOX_SHIT         6

#define RAB_IB_DME_DESC_ADRS(x) (0x0604 + (x)* 0x10)
#define RAB_IB_DME_STAT(x)      (0x0608 + (x)* 0x10)
   #define RAB_IB_DME_ERR_TIMEOUT       0x00000080
   #define RAB_IB_DME_ERR_MSG           0x00000040
   #define RAB_IB_DME_ERR_DATA_TRAN     0x00000020
   #define RAB_IB_DME_ERR_DESC_UPDATE   0x00000010
   #define RAB_IB_DME_ERR_DESC          0x00000008
   #define RAB_IB_DME_ERR_DESC_FETCH    0x00000004

#define  RAB_IB_DME_ERR (RAB_IB_DME_ERR_TIMEOUT | RAB_IB_DME_ERR_MSG | \
    RAB_IB_DME_ERR_DATA_TRAN | RAB_IB_DME_ERR_DESC_UPDATE | RAB_IB_DME_ERR_DESC | \
    RAB_IB_DME_ERR_DESC_FETCH)

#define RAB_IB_DME_DESC(x)      (0x060c + (x)* 0x10)

#define RAB_SET_PAGE(pDev, page) \
    PER_WRITE_4(pDev, RAB_APB_CSR, (PER_READ_4(pDev, RAB_APB_CSR) & 0xE000FFFF)| \
    (((page) & 0x1fff) << 16) )


#define PER_RIO_BASE_OFFSET             0x20000

#define AXI_WIN_BASE_UPPER              0x20
#define AXI_WIN_BASE_LOWER              0x80000000

/* Port n Pass-Through/Accept-All Configuration  */

#define PnPTAACR(n) (0x10120 + n*0x80)
   #define PnPTAACR_ACC_ALL     0x00000001 /* accept all packet */

#define RIO_TYPE_MAINT             0x0
#define RIO_TYPE_NREAD_NWRITE      0x1
#define RIO_TYPE_NREAD_NWRITER     0x2
#define RIO_TYPE_NREAD_SWRITE      0x3
#define RIO_TX_TIMEOUT             100000


#define RIO_BDID_SHIFT             16
#define SHIFT_TTYPE_TO_APIO_CSR    1

#define RAB_APIO_AMAP_LUT_N(n)    (0x100 + (n)*0x4)
   #define RIO_MMAP_WINSIZE_1M     0x0
   #define RIO_MMAP_WINSIZE_2M     0x1
   #define RIO_MMAP_WINSIZE_4M     0x2
   #define RIO_MMAP_WINSIZE_8M     0x3
   #define RIO_MMAP_WINSIZE_16M    0x4
   #define RIO_MMAP_WINSIZE_32M    0x5
   #define RIO_MMAP_WINSIZE_64M    0x6
   #define RIO_MMAP_WINSIZE_128M   0x7
   #define RIO_MMAP_WIN_EN         0x1
   #define SHIFT_AXI_ADDR_TO_LUT   6
   #define SHIFT_WIN_BASE_UPPER    22
   #define SHIFT_HOPCNT_TO_RBAR    14

#define LSI_RIO_MAINT_CHANNEL                0
#define LSI_RIO_MAINT_WIN_SIZE               0x1000000

#define LSI_RIO_HOST_CFG_OUT_CHANNEL         1
#define LSI_RIO_HOST_SM_IN_CHANNEL           2

#define LSI_RIO_SLAVE_SM_OUT_CHANNEL         0
#define LSI_RIO_SLAVE_TAS_SET_OUT_CHANNEL    1
#define LSI_RIO_SLAVE_TAS_CLEAR_OUT_CHANNEL  2

enum MBOX_INOUT
    {
    RIO_MBOX_TX,
    RIO_MBOX_RX
    };

#define RAB_DME_DESC_PAGE         0x60
#define RAB_DME_DESC_BASE         0x10000
#define RAB_PAGE_2K               0x800
#define RAB_DME_DESC_CHAIN_16     0x100

#define RAB_MAX_MSG_SIZE          4096
#define RAB_MAX_MSG_SEG_SIZE      256
#define RAB_MAX_MSG_DW_LEN        512
#define RAB_MAX_OBMSG_UNIT        2
#define RAB_MAX_IBMSG_UNIT        16

#define RAB_DME_DESC_WORD0(base, idx)  ((base)%0x800 + 0x10*(idx) + 0x0)
#define RAB_DME_DESC_WORD1(base, idx)  ((base)%0x800 + 0x10*(idx) + 0x4)
#define RAB_DME_DESC_WORD2(base, idx)  ((base)%0x800 + 0x10*(idx) + 0x8)
#define RAB_DME_DESC_WORD3(base, idx)  ((base)%0x800 + 0x10*(idx) + 0xc)

#define SHIFT_TO_DESC_DEV_ID      16
#define SHIFT_TO_DESC_BUF_SIZE    4
#define DESC_INT_EN               0x8
#define DESC_END_OF_CHAIN         0x4
#define DESC_VALID                0x1
#define SHIFT_TO_DESC_SEG_SIZE    18
#define SHIFT_TO_DESC_MSG_LEN     8
#define SHIFT_TO_DESC_MBOX        2
#define SHIFT_TO_IB_DME_MBOX      6
#define SHIFT_TO_IB_DME_LETTER    4

#define DESC_SEG_SIZE_8_BYTES     1
#define DESC_SEG_SIZE_16_BYTES    2
#define DESC_SEG_SIZE_32_BYTES    3
#define DESC_SEG_SIZE_64_BYTES    4
#define DESC_SEG_SIZE_128_BYTES   5
#define DESC_SEG_SIZE_256_BYTES   6
#define RIO_256B_ALIGNED          8
#define RIO_16B_ALIGNED           4

#define DESC_BUF_SIZE_512B        0
#define DESC_BUF_SIZE_1KB         1
#define DESC_BUF_SIZE_2KB         2
#define DESC_BUF_SIZE_4KB         3

#define MBOX_RX_DESC_CHAN_MAX     1
#define LSI_RIO_MBOX_RXCNT        32
#define LSI_RIO_PER_BUFSIZE       4096

typedef struct lsiRioIntArg
    {
    FUNCPTR pRxCallBack;
    FUNCPTR pTxCallBack;
    void  * pRxArg;
    void  * pTxArg;
    }LSI_RIO_INT_CBARG;

typedef struct lsiRioDrvCtrl
    {
    VXB_DEVICE_ID       pDev;
    void *              riohandle;
    void *              rioPerhandle;
    void *              rioBar;
    void *              rioPerBar;
    SEM_ID              rioSem;
    UINT32              rioHost;
    void *              rioDevBase;
    void *              rioSmHostBase;
    UINT32              rioSmWinSize;
    LSI_RIO_INT_CBARG   rioTypeXCallBackArg [RIO_TYPE_NUM];
    char *              rioPMboxRxbuf;
    } LSI_SRIO_DRV_CTRL;

#define SRIO_BAR(p)     ((LSI_SRIO_DRV_CTRL *)(p)->pDrvCtrl)->rioBar
#define PER_BAR(p)      ((LSI_SRIO_DRV_CTRL *)(p)->pDrvCtrl)->rioPerBar

#define SRIO_HANDLE(p)   ((LSI_SRIO_DRV_CTRL *)(p)->pDrvCtrl)->riohandle
#define PER_HANDLE(p)   ((LSI_SRIO_DRV_CTRL *)(p)->pDrvCtrl)->rioPerhandle

#undef CSR_READ_4
#define CSR_READ_4(pDev, addr)                                 \
    vxbRead32 (SRIO_HANDLE(pDev), (UINT32 *)((UINT32)SRIO_BAR(pDev)+((UINT32)(addr))))

#undef CSR_WRITE_4
#define CSR_WRITE_4(pDev, addr, data)                          \
    vxbWrite32 (SRIO_HANDLE(pDev),                             \
        (UINT32 *)((UINT32)SRIO_BAR(pDev)+((UINT32)(addr))), data)

#undef CSR_SETBIT_4
#define CSR_SETBIT_4(pDev, offset, val)                        \
    CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#undef CSR_CLRBIT_4
#define CSR_CLRBIT_4(pDev, offset, val)                        \
    CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#define PER_READ_4(pDev, addr)                                  \
    vxbRead32 (PER_HANDLE(pDev), (UINT32 *)((UINT32)PER_BAR(pDev)+((UINT32)(addr))))

#define PER_WRITE_4(pDev, addr, data)                          \
    vxbWrite32 (PER_HANDLE(pDev),                              \
        (UINT32 *)((UINT32)PER_BAR(pDev)+((UINT32)(addr))), data)

#define PER_SETBIT_4(pDev, offset, val)                        \
    PER_WRITE_4(pDev, offset, PER_READ_4(pDev, offset) | (val))

#define PER_CLRBIT_4(pDev, offset, val)                        \
    PER_WRITE_4(pDev, offset, PER_READ_4(pDev, offset) & ~(val))

#define DESC_READ_4(pDev, addr)                                \
    vxbRead32 (PER_HANDLE(pDev), (UINT32 *)((UINT32)SRIO_BAR(pDev)+((UINT32)(addr))))

#define DESC_WRITE_4(pDev, addr, data)                         \
    vxbWrite32 (PER_HANDLE(pDev),                              \
        (UINT32 *)((UINT32)SRIO_BAR(pDev)+((UINT32)(addr))), data)

#define DESC_SETBIT_4(pDev, offset, val)                       \
    DESC_WRITE_4(pDev, offset, DESC_READ_4(pDev, offset) | (val))

#define DESC_CLRBIT_4(pDev, offset, val)                       \
    DESC_WRITE_4(pDev, offset, DESC_READ_4(pDev, offset) & ~(val))

IMPORT STATUS lsiRioUsrIntGen (int, int, int);

#ifdef __cplusplus
}
#endif

#endif /*__INCvxbLsiSRioH */

