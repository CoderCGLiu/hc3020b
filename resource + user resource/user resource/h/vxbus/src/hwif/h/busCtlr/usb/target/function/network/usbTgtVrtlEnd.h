/* usbTgtVrtlEnd.h - Definitions of USB Virtual END Interface for USB Network Function */

/*
 * Copyright (c) 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
Modification history
--------------------
01b,12jul11,s_z  Remove unused elements
01a,04jan11,s_z  written
*/

/*
DESCRIPTION

*/

#ifndef INCusbTgtVrtlEndh
#define INCusbTgtVrtlEndh

#include <vxWorks.h>
#include <semLib.h>
#include <lstLib.h>
#include <vwModNum.h>
#include <vxBusLib.h>

#include <netLib.h>
#include <endLib.h>
#include <endMedia.h>
#include <miiLib.h>
#include <etherMultiLib.h>
#include <../src/hwif/h/mii/miiBus.h>
#include <hwif/vxbus/vxBus.h>


#ifdef __cplusplus
extern "C" {
#endif

/* The defination of the USB virtual specific IOCTL command */

#define UIOCSSUBSENDRTN _IOW('u', 1, FUNCPTR)     /* Set the sub-send routine */
#define UIOCSREMOTEADDR _IOW('u', 2, END_PHYADDR) /* Set the remote MAC Address  */
#define UIOCGREMOTEADDR _IOW('u', 3, END_PHYADDR) /* Get the remote MAC Address  */


#define USBTGT_VRTL_END_MAX_PKT              1556
#define USBTGT_VRTL_END_POOL_TUPLE_CNT       64


typedef STATUS (* VXB_END_SEND_SUB_RNT) (END_OBJ * pEnd,M_BLK_ID pMblk);


typedef struct usbtgt_vrtl_end
    {
    END_OBJ         EndObj;              /* The first one, ENDObj*/
    VXB_DEVICE_ID   pDev;                /* VxBus device ID */
    SEM_ID          endMutex;            /* Mutex to protect the data structure */
     
    void *          pMuxDevCookie;       /* Mux cookie of the END Obj */
    
    JOB_QUEUE_ID    endJobQueue;         /* End job queure */
    

    M_BLK_ID        MBlkId;              /* The MBlk Id */
    
    /* Begin MII/ifmedia required fields. */
    
    VXB_DEVICE_ID    dumyMiiBus;      /* The dumy MII bus */
    UINT8            dumyMiiPhyAddr;  /* Default dumy Mii Phy addr : 0*/
    END_MEDIALIST *  pEndMediaList;   /* Media list pointer */
    UINT32           uCurMedia;       /* The current media */
    UINT32           uCurStatus;      /* The current status s*/
    END_ERR          endLastError;    /* The last error */
    char             localMacAddr[ETHER_ADDR_LEN];  /* Record the local MAC address */
    char             remoteMacAddr[ETHER_ADDR_LEN]; /* Record the remote MAC address */
    
    END_CAPABILITIES endCaps;            /* The END cababilities */
    VXB_END_SEND_SUB_RNT sendSubRoutine;  /* The sub-routine in the send method*/    
    UINT32          uEndSpeed;            /* Indicate the END speed */
    UINT32          uEndfullDuplex;       /* Indicate the Full Duplex mode */

    int             maxPktSize;           /* The max pkt size to support by this driver */
    
    }USBTGT_VRTL_END, *pUSBTGT_VRTL_END;


#ifdef __cplusplus
}
#endif

#endif /* INCusbTgtVrtlEndh */




