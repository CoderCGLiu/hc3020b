/* usbHubGlobalVariables.c - Stores all the global variables used by HUB class driver */

/*
 * Copyright (c) 2003, 2010 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2003, 2010 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
Modification history
--------------------
01b,13jan10,ghs  vxWorks 6.9 LP64 adapting
01a,27jun03,nrv  Changing the code to WRS standards
*/

/*
DESCRIPTION

This module stores all the global variables used by HUB class driver.

INCLUDE FILES: usb2/usbOsal.h usb2/usbHubCommon.h usb2/usbHst.h

*/



/*
INTERNAL
 *******************************************************************************
 * Filename         : HUB_GlobalVariables.c
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      : This module stores all the global variables.
 *
 *
 ******************************************************************************/


/************************** INCLUDE FILES *************************************/


#include "usb/usbOsal.h"
#include "usbHubCommon.h"
#include "usb/usbHst.h"

/******************* GLOBAL VARIABLES DEFINITION ******************************/

/* This holds the pointer to the USB bus information.*/
pUSB_HUB_BUS_INFO gpGlobalBus;

/* This holds the functions exposed by the USB Host Software Stack. */
USBHST_FUNCTION_LIST g_usbHstFunctionList;


/**************************** End of File HUB_GlobalVariables.c ***************/
