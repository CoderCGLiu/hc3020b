/* usbMhdrcHcdIsr.h - USB MHCD Interrupt Handler file */

/*
 * Copyright (c) 2009 - 2011 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01e,15sep11,m_y  Add CppiDMA support
01d,09mar11,w_x  Code clean up for make man
01c,16aug10,w_x  VxWorks 64 bit audit and warning removal
01b,03jun10,s_z  Coding convention
01a,04nov09,s_z  initial version
*/

/*
DESCRIPTION
This file defines the interrupt handler for the USB MHCI Driver.
*/

/* includes */

#ifndef __INCusbMhdrcHcdIsrh
#define __INCusbMhdrcHcdIsrh

#ifdef __cplusplus
extern "C" {
#endif

#include "usbMhdrcHcd.h"

/* imports */

void usbMhdrcHcdInterruptProcessHandler (pUSB_MHCD_DATA pHCDData);
void usbMhdrcHcdHsDmaInterruptHandler
    (
    pUSB_MUSBMHDRC  pMHDRC,
    UINT8           uChannel
    );

void usbMhdrcHcdCppiDmaInterruptHandler
    (
    pUSB_MUSBMHDRC  pMHDRC,
    UINT8           uChannel
    );

#ifdef __cplusplus
}
#endif

#endif /* __INCusbMhdrcHcdIsrh */

