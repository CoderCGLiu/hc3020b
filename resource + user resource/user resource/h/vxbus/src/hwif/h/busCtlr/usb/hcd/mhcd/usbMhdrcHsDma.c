/* usbMhdrcHsDma.c - Internal DMA (HSDMA) procession of Mentor Graphics */

/*
 * Copyright (c) 2010-2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01i,03may13,wyy  Remove compiler warning (WIND00356717)
01h,22aug12,ljg  Fix coverity issue "CHECKED_RETURN' (WIND00371808)
01g,15may12,s_z  Correct error process in usbMhdrcOmap3evmHsDmaSetup (WIND00348017)
01f,15sep11,m_y  using one structure to describe HSDMA and CPPIDMA
01e,16may11,w_x  Add DMA channel number in USB_MHDRC_DMA_CHANNEL (WIND00276544)
01d,12apr11,m_y  modify USB_MHDRC_DMA_DATA structure (WIND00265911)
01c,08apr11,w_x  Clean up MUSBMHDRC platform definitions
01b,09mar11,w_x  Code clean up for make man
01a,06dec10,m_y  written
*/

/*
DESCRIPTION

This file provides the management and configuration routines of the HSDMA,
which is the internal DMA of Mentor Graphics Dual-Role USB controller
(also refered to as Inventra DMA by some documents).

The user needs to provide the platform and DMA type parameters to the MHCD
to use this feature.

INCLUDE FILES: usb/usbOsal.h, usb/usbHst.h, usb/usb.h, usbMhdrc.h,
               usbMhdrcPlatform.h, usbMhdrcHSDma.h,
*/

/* includes */

#include <usb/usbOsal.h>
#include <usb/usbHst.h>
#include <usb/usb.h>
#include "usbMhdrc.h"
#include "usbMhdrcPlatform.h"
#include "usbMhdrcHsDma.h"


/* globals */

/*******************************************************************************
*
* usbMhdrcHsDmaFreeChannelRequest - request one free channel of the DMA
*
* This routine is to request one free channel of the DMA.
*
* RETURNS: Free DMA channel index, or ERROR if no free channel is found
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL int usbMhdrcHsDmaFreeChannelRequest
    (
    pUSB_MHDRC_DMA_DATA  pDMAData
    )
    {
    int index;

    if (pDMAData == NULL)
        return USB_MHDRC_DMA_INVALID_CHANNEL;

    semTake(pDMAData->pSynchMutex, WAIT_FOREVER);
    index =
          USB_MHDRC_FIND_FIRST_SET_BIT(pDMAData->uFreeDmaChannelBitMap);

    /* Mark the channel as busy */

    if (index < pDMAData->uMaxDmaChannel)
        {
        pDMAData->uFreeDmaChannelBitMap = (UINT16)(pDMAData->uFreeDmaChannelBitMap &
                                                   ~(0x01 << index));
        }
    else
        index = USB_MHDRC_DMA_INVALID_CHANNEL;

    semGive(pDMAData->pSynchMutex);

    return index;
    }

/*******************************************************************************
*
* usbMhdrcHsDmaChannelRelease - release a busy DMA channel
*
* This routine is used to release a busy DMA channel.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbMhdrcHsDmaChannelRelease
    (
    pUSB_MHDRC_DMA_DATA  pDMAData,
    int                  uChannel
    )
    {
    if ((pDMAData == NULL) || (uChannel > pDMAData->uMaxDmaChannel))
        return ERROR;

    semTake(pDMAData->pSynchMutex, WAIT_FOREVER);
    pDMAData->uFreeDmaChannelBitMap |= (UINT16)((0x01 << uChannel));
    semGive(pDMAData->pSynchMutex);
    return OK;
    }

/*******************************************************************************
*
* usbMhdrcHsDmaChannelConfigure - configure the DMA channel
*
* This routine is to configure the DMA channel.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbMhdrcHsDmaChannelConfigure
    (
    pUSB_MUSBMHDRC   pMHDRC,   /* Pointer to USB_MUSBMHDRC */
    void *           pRequest, /* Pointer to request info */
    ULONG            uAddr,    /* Start buffer address */
    UINT32           uLength,  /* Length to copy */
    UINT8            uChannel, /* Dma channel */
    UINT8            uMode,    /* Dma Mode*/
    UINT8            uEp,      /* Endpoint Address*/
    BOOLEAN          bTX,      /* TX or not */
    UINT16           uMaxPacketSize /* Max packet size */
    )
    {
    UINT32                    uDmaCntl = 0;
    pUSB_MHDRC_DMA_DATA       pDMAData = NULL;
    pUSB_MHDRC_DMA_CHANNEL    pDMAChannel = NULL;

    if ((pMHDRC == NULL) ||
        (pRequest == NULL) ||
        (uChannel > pMHDRC->uNumDmaChannels) ||
        (uAddr == 0))
        {
        USB_MHDRC_ERR("Invalid paramter\n", 1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    USB_MHDRC_VDBG("usbMhdrcHsDmaChannelConfigure - "
                   "pRequest %p, uAddr %p, uLength %p, uEp %p uMode %p, bTX %p\n",
                   pRequest, uAddr, uLength, uEp, uMode, bTX);

    pDMAData = &(pMHDRC->dmaData);
    pDMAChannel = &(pDMAData->dmaChannel[uChannel]);

    /* Record the information for this channel */

    pDMAChannel->pRequest = pRequest;
    pDMAChannel->uAddr = uAddr;
    pDMAChannel->uTotalLen = uLength;
    pDMAChannel->uEpAddress = uEp;
    pDMAChannel->uMode = uMode;
    pDMAChannel->bTX = bTX;
    pDMAChannel->uMaxPacketSize = uMaxPacketSize;
    pDMAChannel->uActLen = 0;
    pDMAChannel->uChannel = uChannel;

    /* Mark the channel as busy */

    pDMAChannel->uStatus = USB_MHDRC_DMA_CHANNEL_BUSY;

    /* DMA CNTL register */

    uDmaCntl = (uEp & 0xF) << USB_MHDRC_HS_DMA_CTNL_EP_OFFSET;
    uDmaCntl |= USB_MHDRC_HS_DMA_CTNL_BRSTM_INCR16;

    if (bTX == TRUE)
        uDmaCntl |= USB_MHDRC_HS_DMA_CTNL_DIR_TX;

    if (uMode)
        uDmaCntl |= USB_MHDRC_HS_DMA_CTNL_MODE_1;

    /* Enable DMA */

    uDmaCntl |= (USB_MHDRC_HS_DMA_CTNL_DMA_ENAB |
                 USB_MHDRC_HS_DMA_CTNL_IE);


    /* DMA ADDR and DMA COUNT fill up */

    USB_MHDRC_REG_WRITE32(pMHDRC,
                          USB_MHDRC_HS_DMA_ADDR_CH(uChannel),
                          (UINT32)USB_MHDRC_VIRT_TO_PHYS(uAddr));

    USB_MHDRC_REG_WRITE32(pMHDRC,
                          USB_MHDRC_HS_DMA_COUNT_CH(uChannel),
                          uLength);

    /* DMA CNTL fill up */

    USB_MHDRC_REG_WRITE32(pMHDRC,
                          USB_MHDRC_HS_DMA_CTNL_CH(uChannel),
                          uDmaCntl);

    return OK;
    }

/*******************************************************************************
*
* usbMhdrcHsDmaCancel - cancel the transfer of the related DMA channel
*
* This routine is to cancel the transfer of the related DMA channel.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbMhdrcHsDmaCancel
    (
    pUSB_MUSBMHDRC    pMHDRC,
    UINT8             uChannel
    )
    {
    UINT16                    uCsrReg;
    UINT8                     uDevctl;
    pUSB_MHDRC_DMA_DATA       pDMAData = NULL;
    pUSB_MHDRC_DMA_CHANNEL    pDMAChannel = NULL;

    /* Parameter vefification */

    if ((NULL == pMHDRC) || (uChannel > pMHDRC->uNumDmaChannels))
        {
        USB_MHDRC_ERR("usbMhdrcHsDmaCancel(): Invalid parameter\n",
                      1, 2, 3, 4, 5 ,6);

        return ERROR;
        }

    pDMAData = &(pMHDRC->dmaData);
    pDMAChannel = &(pDMAData->dmaChannel[uChannel]);

    if (pDMAChannel->pRequest == NULL)
        {
        USB_MHDRC_ERR("usbMhdrcHsDmaCancel(): pRequest is NULL\n",
                      1, 2, 3, 4, 5, 6);

        return ERROR;
        }

    uDevctl = USB_MHDRC_REG_READ8(pMHDRC, USB_MHDRC_DEVCTL);

    if (uDevctl & USB_MHDRC_DEVCTL_HM) /* Host Mode */
        {
        if (pDMAChannel->bTX)
            {
            uCsrReg = USB_MHDRC_REG_READ16 (pMHDRC,
                            USB_MHDRC_HOST_TXCSR_EP(pDMAChannel->uEpAddress));
            uCsrReg = (UINT16)(uCsrReg &
                            ~(USB_MHDRC_HOST_TXCSR_AUTOSET |
                              USB_MHDRC_HOST_TXCSR_DMAEN |
                              USB_MHDRC_HOST_TXCSR_DMAMODE));
            USB_MHDRC_REG_WRITE16 (pMHDRC,
                  USB_MHDRC_HOST_TXCSR_EP(pDMAChannel->uEpAddress),
                  uCsrReg);
            }
        else
            {
            uCsrReg = USB_MHDRC_REG_READ16 (pMHDRC,
                         USB_MHDRC_HOST_RXCSR_EP(pDMAChannel->uEpAddress));
            uCsrReg = (UINT16)(uCsrReg &
                            ~(USB_MHDRC_HOST_RXCSR_AUTOCLEAR |
                              USB_MHDRC_HOST_RXCSR_DMAEN |
                              USB_MHDRC_HOST_RXCSR_DMAMODE));

            USB_MHDRC_REG_WRITE16 (pMHDRC,
                  USB_MHDRC_HOST_RXCSR_EP(pDMAChannel->uEpAddress),
                  uCsrReg);

            }
        }
    else /* Target Mode */
        {
        /* If the DMA channel still transfer data */

        if (pDMAChannel->uStatus == USB_MHDRC_DMA_CHANNEL_BUSY)
            {
            if (pDMAChannel->bTX)
                {
                uCsrReg = USB_MHDRC_REG_READ16 (pMHDRC,
                                USB_MHDRC_PERI_TXCSR_EP(pDMAChannel->uEpAddress));
                uCsrReg = (UINT16)(uCsrReg & ~(USB_MHDRC_PERI_TXCSR_AUTOSET |
                                                USB_MHDRC_PERI_TXCSR_DMAEN));
                USB_MHDRC_REG_WRITE16 (pMHDRC,
                                       USB_MHDRC_PERI_TXCSR_EP(pDMAChannel->uEpAddress),
                                       uCsrReg);
                uCsrReg = (UINT16)(uCsrReg & ~USB_MHDRC_PERI_TXCSR_DMAMODE);

                USB_MHDRC_REG_WRITE16 (pMHDRC,
                                       USB_MHDRC_PERI_TXCSR_EP(pDMAChannel->uEpAddress),
                                       uCsrReg);
                }
            else
                {
                uCsrReg = USB_MHDRC_REG_READ16 (pMHDRC,
                             USB_MHDRC_PERI_RXCSR_EP(pDMAChannel->uEpAddress));
                uCsrReg = (UINT16)(uCsrReg & ~(USB_MHDRC_PERI_RXCSR_AUTOCLEAR |
                             USB_MHDRC_PERI_RXCSR_DMAEN |
                             USB_MHDRC_PERI_RXCSR_DMAMODE));

                USB_MHDRC_REG_WRITE16 (pMHDRC,
                                       USB_MHDRC_PERI_RXCSR_EP(pDMAChannel->uEpAddress),
                                       uCsrReg);
                }
            }


        }

    /* DMA ADDR and DMA COUNT clean up */

    USB_MHDRC_REG_WRITE32 (pMHDRC,
                           USB_MHDRC_HS_DMA_ADDR_CH(uChannel),
                           0);
    USB_MHDRC_REG_WRITE32 (pMHDRC,
                           USB_MHDRC_HS_DMA_COUNT_CH(uChannel),
                           0);

    USB_MHDRC_REG_WRITE32 (pMHDRC,
                           USB_MHDRC_HS_DMA_CTNL_CH(uChannel),
                           0);

    /* Set the channel as default value */

    memset(pDMAChannel, 0, sizeof(USB_MHDRC_DMA_CHANNEL));

    return OK;
    }

/*******************************************************************************
*
* usbMhdrcHsDmaIsr - interrupt service routine for internal DMA
*
* This routine is the interrupt service routine for the internal DMA.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcHsDmaIsr
    (
    pUSB_MUSBMHDRC  pMHDRC
    )
    {
    UINT8                   uIntDma  = 0;
    pUSB_MHDRC_DMA_DATA     pDMAData = NULL;

    /* Check the validity of the parameter */

    if (NULL == pMHDRC)
        {
        USB_MHDRC_ERR("Invalid parameter \n", 1, 2, 3, 4, 5, 6);
        return;
        }

    pDMAData = &(pMHDRC->dmaData);

    if (pDMAData->isrMagic != USB_MHDRC_MAGIC_ALIVE)
        {
        USB_MHDRC_ERR("usbMhdrcHsDmaIsr(): "
                      "ISR not active, maybe shared?\n",
                      1, 2, 3, 4, 5 ,6);
        return;
        }

    SPIN_LOCK_ISR_TAKE(&pMHDRC->spinLock);

    /* Read the contents of the interrupt source mask register */

    uIntDma = USB_MHDRC_REG_READ8 (pMHDRC,
                                   USB_MHDRC_HS_DMA_INTR);
    pDMAData->uIrqStatus |= uIntDma;

    SPIN_LOCK_ISR_GIVE(&pMHDRC->spinLock);

    if (pDMAData->uIrqStatus)
        {
        OS_RELEASE_EVENT(pDMAData->isrEvent);
        }

    return ;
    }

/*******************************************************************************
*
* usbMhdrcHsDmaInterruptHandler - interrupt handler for the internal DMA
*
* This routine is the interrupt handler for the internal DMA.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcHsDmaInterruptHandler
    (
    pUSB_MUSBMHDRC  pMHDRC
    )
    {
    pUSB_MHDRC_DMA_DATA     pDMAData;
    UINT16                  uIrqStatus;
    int                     uChannel;

    if (pMHDRC == NULL)
        {
        USB_MHDRC_ERR("Invalid Paramter\n", 1, 2, 3, 4, 5, 6);
        return;
        }

    pDMAData = &(pMHDRC->dmaData);

    while (TRUE)
        {
        OS_WAIT_FOR_EVENT(pDMAData->isrEvent, WAIT_FOREVER);

        /* Handle the interrupt here */

        USB_MHDRC_VDBG("usbMhdrcHsDmaInterruptHandler(): IRQ %p\n",
                       pDMAData->uIrqStatus, 2, 3, 4, 5, 6);

        SPIN_LOCK_ISR_TAKE(&pMHDRC->spinLock);
        uIrqStatus = pDMAData->uIrqStatus;
        pDMAData->uIrqStatus = 0;
        SPIN_LOCK_ISR_GIVE(&pMHDRC->spinLock);

        while(uIrqStatus != 0)
            {
            uChannel = USB_MHDRC_FIND_FIRST_SET_BIT(uIrqStatus);

            if ((uChannel < pDMAData->uMaxDmaChannel)&&
                (pDMAData->pDmaCallback != NULL))
                pDMAData->pDmaCallback(pMHDRC, uChannel);

            uIrqStatus = (UINT16)(uIrqStatus & ~(0x01 << uChannel));
            }
        }

    }

/*******************************************************************************
*
* usbMhdrcOmap3evmHsDmaSetup - configure omap3 internal DMA information
*
* This routine configures omap3evm internal DMA information.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbMhdrcOmap3evmHsDmaSetup
    (
    pUSB_MUSBMHDRC  pMHDRC
    )
    {
    pUSB_MHDRC_DMA_DATA   pDMAData = NULL;
    char                  ThreadName[USB_MHDRC_TASK_NAME_MAX];

    pDMAData = &(pMHDRC->dmaData);

#if defined (USB_MHDRC_DMA_ENABLE)

    if (pMHDRC->bDmaEnabled)
        {
        pDMAData->pSynchMutex = semMCreate(SEM_Q_PRIORITY |
                                           SEM_INVERSION_SAFE |
                                           SEM_DELETE_SAFE);

        if (pDMAData->pSynchMutex == NULL)
            {
            USB_MHDRC_ERR("Creat pSynchMutex for DmaData failed\n",
                          1, 2, 3, 4, 5, 6);
            return ERROR;
            }

        pDMAData->isrEvent = OS_CREATE_EVENT(OS_EVENT_NON_SIGNALED);

        if (pDMAData->isrEvent == NULL)
            {
            USB_MHDRC_ERR("Creat pSynchMutex for DmaData failed\n",
                          1, 2, 3, 4, 5, 6);
            (void) usbMhdrcPlatformHsDmaUnSetup(pMHDRC);
            return ERROR;
            }

        snprintf((char*)ThreadName,
            USB_MHDRC_TASK_NAME_MAX, "MHSDMA_IH%d", pMHDRC->pDev->unitNumber);

        pDMAData->irqThread =
                     OS_CREATE_THREAD((char *)ThreadName,
                                       USB_MHDRC_DMA_INT_THREAD_PRIORITY,
                                       usbMhdrcHsDmaInterruptHandler,
                                       pMHDRC);

        /* Check whether the thread creation is successful */

        if (OS_THREAD_FAILURE == pDMAData->irqThread)
            {
            USB_MHDRC_ERR("Interrupt handler thread is not created\n",
                         1, 2, 3, 4, 5 ,6);
            (void) usbMhdrcPlatformHsDmaUnSetup(pMHDRC);
            return ERROR;
            }
        /* DMA channel number */

        pDMAData->uMaxDmaChannel = pMHDRC->uNumDmaChannels;

        /*
         * Set FreeDmaChannelBitMap bit[n] = 1 means the channel[n] is idle
         * or else it's busy
         */

        pDMAData->uFreeDmaChannelBitMap = (UINT16)
            ((0x1 << pDMAData->uMaxDmaChannel) - 1);

        /* Set DMA Type */

        pDMAData->uDMAType = USB_MHDRC_HS_DMA;

        /* Update the operation pointers */

        pDMAData->pDmaChannelRequest = usbMhdrcHsDmaFreeChannelRequest;
        pDMAData->pDmaChannelRelease = usbMhdrcHsDmaChannelRelease;
        pDMAData->pDmaConfigure = usbMhdrcHsDmaChannelConfigure;
        pDMAData->pDmaCancel = usbMhdrcHsDmaCancel;
        pDMAData->pDmaIsr = usbMhdrcHsDmaIsr;
        pDMAData->pDmaIrqHandler = usbMhdrcHsDmaInterruptHandler;

        if (pDMAData->pDmaIsr != NULL)
            {
            if (ERROR == (vxbIntConnect (pMHDRC->pDev,
                                         1,
                                         pDMAData->pDmaIsr,
                                         (void *)pMHDRC)))
                {
                USB_MHDRC_ERR("Error hooking the DMA ISR\n",
                              1, 2, 3, 4, 5 ,6);
                (void) usbMhdrcPlatformHsDmaUnSetup(pMHDRC);
                return ERROR;
                }
            pDMAData->isrMagic = USB_MHDRC_MAGIC_ALIVE;
            }
        }
#endif

    return OK;
    }

/*******************************************************************************
*
* usbMhdrcPlatformHsDmaSetup - configure platform DMA based on platform type
*
* This routine configures the platform DMA according to the platform type.
*
* RETURNS: OK, or ERROR if failed to configure the platform DMA.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbMhdrcPlatformHsDmaSetup
    (
    pUSB_MUSBMHDRC  pMHDRC
    )
    {
    STATUS result;
    switch (pMHDRC->uPlatformType)
        {
        case USB_MHDRC_PLATFORM_OMAP35xx:
#if  defined (USB_MHDRC_OMAP)
            result = usbMhdrcOmap3evmHsDmaSetup(pMHDRC);
            if (result == ERROR)
                {
                USB_MHDRC_ERR("usbMhdrcOmap3evmTcdDmaSetup failed\n",
                              1, 2, 3, 4, 5, 6);
                return ERROR;
                }
#else
            printf ("usbMhdrcPlatformHcdDmaSetup(): USB_MHDRC_OMAP not defined\n");
            return ERROR;
#endif
            break;
        case USB_MHDRC_PLATFORM_DAVINIC:
        case USB_MHDRC_PLATFORM_OMAPL13x:
        default:
            return ERROR;
        }
    return OK;
    }

/*******************************************************************************
*
* usbMhdrcPlatformHsDmaUnSetup - deconfigure platform DMA based on platform type
*
* This routine deconfigures the platform DMA according to the platform type.
*
* RETURNS: OK, or ERROR if failed to configure the platform DMA.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbMhdrcPlatformHsDmaUnSetup
    (
    pUSB_MUSBMHDRC  pMHDRC
    )
    {
    pUSB_MHDRC_DMA_DATA   pDMAData = NULL;

    if (pMHDRC == NULL)
        {
        USB_MHDRC_ERR("Invalid Paramter pMHDRC %p\n",
                      pMHDRC, 2, 3, 4, 5, 6);
        return ERROR;
        }

    pDMAData = &(pMHDRC->dmaData);

    /* Disable Dma ISR if DMA ISR is sepreate with usb core ISR */

    if (pDMAData->pDmaIsr)
        {
        if (pDMAData->isrMagic == USB_MHDRC_MAGIC_ALIVE)
            {
            vxbIntDisconnect (pMHDRC->pDev,
                              1,
                              pDMAData->pDmaIsr,
                              (void *)(pMHDRC));
            pDMAData->isrMagic = USB_MHDRC_MAGIC_DEAD;
            }
        }

    if (pDMAData->pSynchMutex)
        {
        semDelete(pDMAData->pSynchMutex);
        pDMAData->pSynchMutex = NULL;
        }

    if (pDMAData->isrEvent)
        {
        semDelete(pDMAData->isrEvent);
        pDMAData->isrEvent = NULL;
        }

    if ((pDMAData->irqThread != 0) &&
        (pDMAData->irqThread != OS_THREAD_FAILURE))
        {
        OS_DESTROY_THREAD(pDMAData->irqThread);
        pDMAData->irqThread = OS_THREAD_FAILURE;
        }

    return OK;
    }

/*******************************************************************************
*
* usbMhdrcHsDmaIntrEnable - enable MHDRC USB HS DMA interrupt
*
* This routine enables MHDRC USB HS DMA interrupt.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcHsDmaIntrEnable
    (
    pUSB_MUSBMHDRC pMHDRC
    )
    {
    pUSB_MHDRC_DMA_DATA   pDMAData = NULL;

    if (NULL == pMHDRC)
        {
        USB_MHDRC_VDBG("usbMhdrcHsDmaIntrEnable(): Invalid parameter\n",
                       1, 2, 3, 4, 5 ,6);

        return;
        }

    pDMAData = &(pMHDRC->dmaData);

    /* Enable Dma Isr if needed */

    if (pDMAData->pDmaIsr)
        {
        if (vxbIntEnable (pMHDRC->pDev,
                          1,
                          pDMAData->pDmaIsr,
                          (void *)pMHDRC) == ERROR)
            {
            USB_MHDRC_ERR("Error enabling DMA intrrupts\n", 1, 2, 3, 4, 5 ,6);
            return;
            }
        pDMAData->isrMagic = USB_MHDRC_MAGIC_ALIVE;
        }

    return;
    }

/*******************************************************************************
*
* usbMhdrcHsDmaIntrDisable - disable MHDRC USB HS DMA interrupt
*
* This routine disables MHDRC USB HS DMA interrupt.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcHsDmaIntrDisable
    (
    pUSB_MUSBMHDRC pMHDRC
    )
    {
    pUSB_MHDRC_DMA_DATA   pDMAData = NULL;

    if (NULL == pMHDRC)
        {
        USB_MHDRC_VDBG("usbMhdrcHsDmaIntrDisable(): Invalid parameter\n",
                       1, 2, 3, 4, 5 ,6);

        return;
        }

    pDMAData = &(pMHDRC->dmaData);

    /* Disable Dma Isr if needed */

    if (pDMAData->pDmaIsr)
        {
        if (vxbIntDisable (pMHDRC->pDev,
                           1,
                           pDMAData->pDmaIsr,
                          (void *)pMHDRC) == ERROR)
            {
            USB_MHDRC_ERR("Error disabling DMA intrrupts\n", 1, 2, 3, 4, 5 ,6);
            return;
            }
        pDMAData->isrMagic = USB_MHDRC_MAGIC_DEAD;
        }

    return ;
    }

