/* usbMhdrcCppiDma.c - CPPI 4.1 DMA procession of Mentor Graphics USB Controller  */

/*
 * Copyright (c) 2011, 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01h,03may13,wyy  Remove compiler warning (WIND00356717)
01g,26feb13,ljg  Add CPPI DMA support USB1 (WIND00404388)
01f,05jan13,ljg  Add CPPI DMA support for MHDRC TCD (WIND00398723)
01e,31jan13,ljg  add usb support for ti816x EVM
01d,09sep11,m_y  code clean up
01c,29jun11,jws  cleaned up
01b,22jun11,jws  ported to new MHDRC stack
01a,13jun11,ita  created for TI CENTAURUS
*/

/*
DESCRIPTION

This file provides the CENTAURUS platform related hardware configure routines
which will be used to configure the  CPPI 4.1 DMA of MHDRC.
And also proviedes the fixup routine to configure Am387x platform.

INCLUDE FILES: usb/usbOsal.h, usb/usbHst.h, usbMhdrc.h,
               usbMhdrcCppiDma.h, usbMhdrcPlatform.h
*/

/* includes */

#include <usb/usbOsal.h>
#include <usb/usbHst.h>
#include "usbMhdrc.h"
#include "usbMhdrcCppiDma.h"
#include "usbMhdrcPlatform.h"

/* Packet Descriptors Reserved For Unallocated Memory Regions. */

LOCAL UINT32 nextDescIndex[USB_MHDRC_CPPIDMA_NUM_QUEUE_MGR] = { 1 << 5 };
LOCAL UINT8  nextMemRegion[USB_MHDRC_CPPIDMA_NUM_QUEUE_MGR];


/* Queue-endpoint assignments : Tx Queue EP1..15 */

#define USB_MHDRC_CPPI_DMA_QM_NUM(ch) (32 + 2 * (ch))

/* Queue-endpoint assignments : Tx Completion Queue EP1..15 */

#define USB_MHDRC_CCPI_DMA_TD_Q_NUM               156
#define USB_MHDRC_CPPI_DMA_TX_TD_Q_NUM            197


/*******************************************************************************
*
* usbMhdrcCppiDmaPDGet - get a free Packet Descriptor
*
* This routine will get a free Packet Descriptor.
*
* RETURNS: pointer of the USB_MHDRC_CPPIDMA_PKT_DESC
*
* ERRNO: N/A
*
* \NOMANUAL
*/

pUSB_MHDRC_CPPIDMA_PKT_DESC usbMhdrcCppiDmaPDGet
    (
    pUSB_MHDRC_CPPIDMA pCppi
    )
    {
    pUSB_MHDRC_CPPIDMA_PKT_DESC pFreePD = NULL;

    if (pCppi == NULL)
        return NULL;

    pFreePD = pCppi->pPdPoolTop;

    if (pFreePD != NULL)
        {
        pCppi->pPdPoolTop = pFreePD->pNextPD;
        pFreePD->pNextPD = NULL;
        }
    return pFreePD;
    }

/*******************************************************************************
*
* usbMhdrcCppiDmaPDPut - put a free Packet Descriptor
*
* This routine will put a free Packet Descriptor.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcCppiDmaPDPut
    (
    pUSB_MHDRC_CPPIDMA           pCppi,
    pUSB_MHDRC_CPPIDMA_PKT_DESC  pFreePD
    )
    {
    if ((pCppi == NULL) ||(pFreePD == NULL))
        {
        USB_MHDRC_ERR("Invalid parameter\n", 1, 2, 3, 4, 5, 6);
        return;
        }

    pFreePD->pNextPD = pCppi->pPdPoolTop;
    pCppi->pPdPoolTop = pFreePD;
    return;
    }

/*******************************************************************************
*
* usbMhdrcCppiDmaFreeChannelRequest - get one free channel of the CPPI41 DMA.
*
* This routine will find one free DMA channel which is ready for using.
*
* RETURNS: Free DMA channel index, or ERROR if no free channel is found
*          CPPI41 always return (1)
*
* ERRNO: N/A
*
*
* \NOMANUAL
*/

LOCAL int usbMhdrcCppiDmaFreeChannelRequest
    (
    pUSB_MHDRC_DMA_DATA  pDMAData
    )
    {
    /* Parameter vefification */

    if (NULL == pDMAData)
        {
        USB_MHDRC_ERR("usbMhdrcCppiDmaFreeChannelGet(): "
                      "Parameter not valid\n", 1, 2, 3, 4, 5, 6);

        return USB_MHDRC_DMA_INVALID_CHANNEL;
        }

    /*
     * For CPPI DMA, the DMA channel is bound to the endpoint
     * So alwarys return 1, just to make the caller success
     */

    return 1;

    }

/*******************************************************************************
*
* usbMhdrcHsDmaChannelRelease - release a busy DMA channel
*
* This routine is used to release a busy DMA channel. - This is dummy function.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbMhdrcCppiDmaChannelRelease
    (
    pUSB_MHDRC_DMA_DATA  pDMAData,
    int                  uChannel
    )
    {
    if ((pDMAData == NULL) || (uChannel > pDMAData->uMaxDmaChannel))
       {
       USB_MHDRC_ERR("usbMhdrcCppiDmaChannelRelease(): "
                      "Parameter not valid\n", 1, 2, 3, 4, 5, 6);
       return ERROR;
       }

    /*
     * For CPPI DMA, the DMA channel is bound to the endpoint
     * So alwarys return OK, to make the caller success
     */

    return OK;
    }


/*******************************************************************************
*
* usbMhdrcCppiDmaCancel - cancel the transfer of the related DMA channel
*
* This routine is to cancel the transfer of the related DMA channel.
* Currenltly this is dummy and nothing to do.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbMhdrcCppiDmaCancel
    (
    pUSB_MUSBMHDRC    pMHDRC,
    UINT8             uChannel
    )
    {
    if ((NULL == pMHDRC) || (uChannel > pMHDRC->uNumDmaChannels))
       {
       USB_MHDRC_ERR("usbMhdrcCppiDmaCancel(): "
                      "Parameter not valid\n", 1, 2, 3, 4, 5, 6);
       return ERROR;
       }

    return OK;
    }


/*******************************************************************************
*
* usbMhdrcCppiDmaChannelConfigure - configure the channel of the CPPI41 DMA.
*
* This routine will fill up the free DMA channel which is ready to be used.
*
* RETURNS: OK, or ERROR if the parameter is invalid
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbMhdrcCppiDmaChannelConfigure
    (
    pUSB_MUSBMHDRC   pMHDRC,   /* Pointer to USB_MUSBMHDRC */
    void *           pRequest, /* Pointer to request info */
    ULONG            uAddr,    /* Start buffer address */
    UINT32           uLength,  /* Length to copy */
    UINT8            uChannel, /* Dma channel */
    UINT8            uMode,    /* Dma Mode*/
    UINT8            uEp,      /* Endpoint Address*/
    BOOLEAN          bTX,      /* TX or not */
    UINT16           uMaxPacketSize /* Max packet size */
    )
    {
    pUSB_MHDRC_DMA_DATA          pDMAData = NULL;
    pUSB_MHDRC_DMA_CHANNEL       pDMAChannel = NULL;
    pUSB_MHDRC_CPPIDMA_PKT_DESC  pCurrentPd = NULL;
    UINT32 uOffset = 0;
    UINT32 pktSize;
    UINT32 uRegVal;
    UINT16 txCompQueue;
    UINT16 wCsrVal;
    UINT32 freeDesNum;
    int    chNum;
    int    chOffset;
    int    numPd;
    int    i;

    if ((NULL == pMHDRC) ||
        (uChannel > pMHDRC->uNumDmaChannels) ||
        (uMaxPacketSize == 0))
        {
        USB_MHDRC_ERR("usbMhdrcCppiDmaChannelConfigure(): "
                      "Parameter not valid\n", 1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    pDMAData = &(pMHDRC->dmaData);

    /*
     * For CPPI DMA, the DMA channel is bound to the endpoint
     * So the parameter <uChannel> is always 1
     * We should ingore this parameter
     * The channel number start from zero.
     */

    chOffset = 0;

    if (pDMAData->uDmaOffset != 0)
        {
        /* USB 1 */

        chOffset = 15;
        }

    chNum = uEp - 1;

    pDMAChannel = &(pDMAData->dmaChannel[chNum]);

    /* Record the information for this channel */

    pDMAChannel->pRequest = pRequest;
    pDMAChannel->uAddr = uAddr;
    pDMAChannel->uTotalLen = uLength;
    pDMAChannel->uEpAddress = uEp;
    pDMAChannel->uMode = uMode;
    pDMAChannel->bTX = bTX;
    pDMAChannel->uMaxPacketSize = uMaxPacketSize;
    pDMAChannel->uActLen = 0;
    pDMAChannel->uChannel = (UINT8)chNum;

    /* Mark the channel as busy */

    pDMAChannel->uStatus = USB_MHDRC_DMA_CHANNEL_BUSY;

    if (bTX)
        {
        txCompQueue = (UINT16)(chNum + USB_MHDRC_CPPI_TXQ_START_NUM + pDMAData->uDmaOffset);

        /* Enable the DMA channel */

        USB_MHDRC_CPPIDMA_REG_WRITE32 (pMHDRC,
                                DMA_TX_CHANNEL_GLOBAL_CONFIG(chNum + chOffset),
                                DMA_CH_TX_ENABLE_MASK | USB_MHDRC_CPPI_DMA_TX_TD_Q_NUM);

        wCsrVal = USB_MHDRC_REG_READ16(pMHDRC, USB_MHDRC_HOST_TXCSR_EP(uEp));

        wCsrVal |=  (USB_MHDRC_HOST_TXCSR_DMAEN |
                     USB_MHDRC_TXCSR_DMAMODE);

        USB_MHDRC_REG_WRITE16(pMHDRC,
                              USB_MHDRC_HOST_TXCSR_EP(uEp),
                              wCsrVal);

        /*  Packet Descriptor making */

        pktSize = uMaxPacketSize;
        numPd  = (uLength + pktSize - 1) / pktSize;

        for (i = 0; i < numPd; i++)
            {
            semTake(pDMAData->pSynchMutex, WAIT_FOREVER);
            pCurrentPd = usbMhdrcCppiDmaPDGet(pDMAData->pCppi);
            semGive(pDMAData->pSynchMutex);

            if (pCurrentPd == NULL)
                {
                USB_MHDRC_ERR("usbMhdrcCppiDmaChannelConfigure(): No Tx PDs\n",
                                1, 2, 3, 4, 5, 6);
                break;
                }

            if (uLength < pktSize)
                pktSize = uLength;

            /* Fill the hardware desc of the PD */

            pCurrentPd->pktInfoWd0  = (UINT32)(USB_MHDRC_CPPIDMA_DESC_TYPE_HOST |
                                               pktSize);
            pCurrentPd->pktInfoWd1  = (uEp << USB_MHDRC_CPPIDMA_SRC_TAG_PORT_NUM_SHIFT);
            pCurrentPd->ptkInfoWd2  = USB_MHDRC_CPPIDMA_PKT_TYPE_USB |
                                      USB_MHDRC_CPPIDMA_RETURN_LINKED;
            pCurrentPd->ptkInfoWd2 |= ((txCompQueue << USB_MHDRC_CPPIDMA_RETURN_QNUM_SHIFT));
            pCurrentPd->bufPtr      = (UINT32)USB_MHDRC_VIRT_TO_PHYS(uAddr + uOffset);
            pCurrentPd->bufLen      = pktSize;
            pCurrentPd->nextDescPrt = 0;
            pCurrentPd->orgBufLen   = (UINT32)(pktSize |
                                              USB_MHDRC_CPPIDMA_PKT_INTR_FLAG);
            pCurrentPd->orgBufPtr   = pCurrentPd->bufPtr;

            pCurrentPd->chNum = (UINT8)chNum;
            pCurrentPd->epNum = uEp;

            uOffset += pktSize;
            uLength -= pktSize;

            /* push PD in queue */

            uRegVal = (((USB_MHDRC_CPPIDMA_DESC_ALIGN - 24) >> (2 - QMGR_QUEUE_DESC_SIZE_SHIFT)) &
                         QMGR_QUEUE_DESC_SIZE_MASK) |
                        (pCurrentPd->dmaAddr & QMGR_QUEUE_DESC_PTR_MASK);

            USB_MHDRC_QMGR_REG_WRITE32 (pMHDRC,
                                        QMGR_QUEUE_CTRL_D(USB_MHDRC_CPPI_DMA_QM_NUM(chNum + chOffset)),
                                        uRegVal);
            }
        }
    else
        {
        /* rx */

        freeDesNum =0;

        /* Set USB 0 use Free Descriptor 0. Set USB 1 use Free Descriptor 1. */

        if (pDMAData->uDmaOffset != 0) freeDesNum = 0x10001;

        /* Host Packet Configuration Register A init */

        USB_MHDRC_CPPIDMA_REG_WRITE32 (pMHDRC,
                                       DMA_RX_CHANNEL_HOST_PACKET_CONFIG_A(chNum + chOffset),
                                       freeDesNum);

        /* Host Packet Configuration Register B init */

        USB_MHDRC_CPPIDMA_REG_WRITE32 (pMHDRC,
                                       DMA_RX_CHANNEL_HOST_PACKET_CONFIG_B(chNum + chOffset),
                                       freeDesNum);

        /* Enable DMA channel */

        uRegVal = DMA_CH_RX_ENABLE_MASK |
                  DMA_CH_RX_ERROR_HANDLING_BIT_ON |
                  DMA_CH_RX_DEFAULT_DESC_TYPE_HOST |
                  (chNum + USB_MHDRC_CPPI_RXQ_START_NUM + pDMAData->uDmaOffset);

        USB_MHDRC_CPPIDMA_REG_WRITE32 (pMHDRC,
                                       DMA_RX_CHANNEL_GLOBAL_CONFIG(chNum + chOffset),
                                       uRegVal);

        /*
         * Packet Descriptor making
         */

        numPd = (uLength + USB_MHDRC_CPPI_MAX_RX_SIZE - 1)/
                 USB_MHDRC_CPPI_MAX_RX_SIZE;

        for (i = 0; i < numPd ; ++i)
            {
            /* Get Rx PD from free pool */

            semTake(pDMAData->pSynchMutex, WAIT_FOREVER);
            pCurrentPd = usbMhdrcCppiDmaPDGet(pDMAData->pCppi);
            semGive(pDMAData->pSynchMutex);

            if (pCurrentPd == NULL)
               {
                USB_MHDRC_ERR("usbMhdrcCppiDmaChannelConfigure(): No Rx PDs\n",
                                1, 2, 3, 4, 5, 6);
                break;
                }

            pktSize = (uLength > USB_MHDRC_CPPI_MAX_RX_SIZE) ?
                       USB_MHDRC_CPPI_MAX_RX_SIZE : uLength;

            pCurrentPd->orgBufPtr = (UINT32)USB_MHDRC_VIRT_TO_PHYS(uAddr + uOffset);
            pCurrentPd->orgBufLen = (UINT32)(pktSize |
                                             USB_MHDRC_CPPIDMA_PKT_INTR_FLAG);
            pCurrentPd->chNum = (UINT8)chNum;
            pCurrentPd->epNum = uEp;
            pCurrentPd->eop   = (UINT8)((uLength -= pktSize) ? 0 : 1);
            uOffset    += pktSize;

            /* push does PD in queue */

            uRegVal = (((USB_MHDRC_CPPIDMA_DESC_ALIGN - 24) >>
                        (2 - QMGR_QUEUE_DESC_SIZE_SHIFT)) & QMGR_QUEUE_DESC_SIZE_MASK) |
                        (pCurrentPd->dmaAddr & QMGR_QUEUE_DESC_PTR_MASK);

            if (pDMAData->uDmaOffset != 0)
                {
                /*  Set USB 1 use Free Descriptor 1.  */

                USB_MHDRC_QMGR_REG_WRITE32 (pMHDRC,
                                            QMGR_QUEUE_CTRL_D(1),
                                            uRegVal);
                }
            else
                {
                /*  Set USB 0 use Free Descriptor 0. */

                USB_MHDRC_QMGR_REG_WRITE32 (pMHDRC,
                                            QMGR_QUEUE_CTRL_D(0),
                                            uRegVal);
                }
            }

        /* receive one packet */

        wCsrVal = USB_MHDRC_REG_READ16 (pMHDRC, USB_MHDRC_HOST_RXCSR_EP(uEp));
        wCsrVal = (UINT16)(wCsrVal & ~USB_MHDRC_HOST_RXCSR_REQPKT);
        wCsrVal |= USB_MHDRC_HOST_RXCSR_DMAEN;
        USB_MHDRC_REG_WRITE16 (pMHDRC, USB_MHDRC_HOST_RXCSR_EP(uEp), wCsrVal);

        }
    return OK;
    }

/*******************************************************************************
*
* usbMhdrcCppiDmaIsr - interrupt handler for handling DMA interrupts
*
* This routine is registered as the interrupt handler for handling MHDRC
* controller CPPI41 DMA interrupts.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcCppiDmaIsr
    (
    pUSB_MUSBMHDRC  pMHDRC
    )
    {
    /* To hold the value read from the registers */

    UINT32  uIntDma32;
    pUSB_MHDRC_DMA_DATA  pDMAData = NULL;

    /* Check the validity of the parameter */

    if (NULL == pMHDRC)
        {
        USB_MHDRC_ERR("usbMhdrcCppiDmaIsr(): "
                      "Parameter is not valid\n", 1, 2, 3, 4, 5, 6);
        return;
        }

    pDMAData = &(pMHDRC->dmaData);

    if (pDMAData->isrMagic != USB_MHDRC_MAGIC_ALIVE)
        {
        USB_MHDRC_ERR("usbMhdrcCppiDmaIsr(): "
                      "ISR not active, maybe shared?\n", 1, 2, 3, 4, 5, 6);
        return;
        }

    SPIN_LOCK_ISR_TAKE(&pMHDRC->spinLock);

    uIntDma32 = USB_MHDRC_USBSS_REG_READ32(pMHDRC, USB_MHDRC_USBSS_IRQ_STATUS);

    /* check this is for USBx */

    if ((pMHDRC->uRegBase & USB_MHDRC_USBx_CHECK_ADR_MASK) == 0x000)
        {
        /* mask tx_pkt_cmp_1 and rx_pkt_cmp_1 for USB 0 */

        uIntDma32 &= ~(USB_MHDRC_USBSS_IRQSTAT_TX_PKT_CMP_1|
                       USB_MHDRC_USBSS_IRQSTAT_RX_PKT_CMP_1);
        }
    else
        {
        /* mask tx_pkt_cmp_0 and rx_pkt_cmp_0 for USB 1 */

        uIntDma32 &= ~(USB_MHDRC_USBSS_IRQSTAT_TX_PKT_CMP_0|
                       USB_MHDRC_USBSS_IRQSTAT_RX_PKT_CMP_0);
        }

    /* interrupt status clear */

    USB_MHDRC_USBSS_REG_WRITE32(pMHDRC, USB_MHDRC_USBSS_IRQ_STATUS, uIntDma32);

    SPIN_LOCK_ISR_GIVE(&pMHDRC->spinLock);

    if (uIntDma32)
        {
        /*
         * Just set 0x01 to let the interrupt thread know there is an interrupt
         * CPPI DMA don't using this to record the real interrupt status
         */

        pDMAData->uIrqStatus |= 0x01;
        OS_RELEASE_EVENT(pDMAData->isrEvent);
        USB_MHDRC_VDBG("usbMhdrcCppiDmaIsr(): "
                       "Released OS Event:0x%x\n",
                        pDMAData->uIrqStatus, 2, 3, 4, 5, 6);
        }

    return;
    }

/*******************************************************************************
*
* usbMhdrcCppiDmaInterruptHandler - interrupt handler for the internal DMA
*
* This routine is the interrupt handler for the internal DMA.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL void usbMhdrcCppiDmaInterruptHandler
    (
    pUSB_MUSBMHDRC  pMHDRC
    )
    {
    pUSB_MHDRC_DMA_DATA  pDMAData;
    UINT16               uIrqStatus;

    if (pMHDRC == NULL)
        {
        USB_MHDRC_ERR("usbMhdrcCppiDmaInterruptHandler(): Invalid Paramter\n",
                      1, 2, 3, 4, 5, 6);
        return;
        }

    pDMAData = &(pMHDRC->dmaData);

    while (TRUE)
        {
        OS_WAIT_FOR_EVENT(pDMAData->isrEvent, WAIT_FOREVER);

        /* Handle the interrupt here */

        USB_MHDRC_VDBG("usbMhdrcCppiDmaInterruptHandler(): IRQ %p\n",
                       pDMAData->uIrqStatus, 2, 3, 4, 5, 6);

        SPIN_LOCK_ISR_TAKE(&pMHDRC->spinLock);
        uIrqStatus = pDMAData->uIrqStatus;
        pDMAData->uIrqStatus = 0;
        SPIN_LOCK_ISR_GIVE(&pMHDRC->spinLock);

        if (uIrqStatus)
            {
            if (pDMAData->pDmaCallback != NULL)
                pDMAData->pDmaCallback(pMHDRC, 1);

            }
        }

    }

/*******************************************************************************
*
* usbMhdrcCppiDmaMemRgnAlloc - MHDRC DMA allocate a memory region within the
* queue manager
*
* This routine processes allocate a memory region within the queue manager
*
* RETURNS: OK, or ERROR if the parameter is invalid
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbMhdrcCppiDmaMemRgnAlloc
    (
    pUSB_MUSBMHDRC  pMHDRC,
    UINT8           queueMgr,
    void *          rgnAddr,
    UINT8           sizeOrder,
    UINT8           numOrder,
    UINT8 *         memRgn
    )
    {
    UINT32 numDesc = 1 << numOrder;
    UINT32 index;
    UINT32 ctrl;
    int    rgn;

    rgn = nextMemRegion[queueMgr];
    index = nextDescIndex[queueMgr];
    if ((rgn >= USB_MHDRC_CPPIDMA_MAX_MEM_RGN) ||
        (index + numDesc > 0x4000))
        return ERROR;

    nextMemRegion[queueMgr] = (UINT8)(rgn + 1);
    nextDescIndex[queueMgr] = index + numDesc;

    /* Memory Region R Base Address Register */
    /*
     * The memory region R base address register is written by the host to set
     * the base address of memory region R. This memory region will store a
     * number of descriptors of a particular size as determined by the
     * Memory Region R Control Register. It does not support byte accesses.
     * R ranges from 0 to 15.
     */
    /* Write base register */

    USB_MHDRC_QMGR_REG_WRITE32 (pMHDRC,
                                QMGR_MEM_REGION_BASE_ADDR(rgn),
                                USB_MHDRC_VIRT_TO_PHYS(rgnAddr));

    /* Write control register */

    ctrl = ((index << QMGR_MEM_RGN_INDEX_SHIFT) &
            QMGR_MEM_RGN_INDEX_MASK) |
               (((sizeOrder - 5) << QMGR_MEM_RGN_DESC_SIZE_SHIFT) &
            QMGR_MEM_RGN_DESC_SIZE_MASK) |
               (((numOrder - 5) << QMGR_MEM_RGN_SIZE_SHIFT) &
            QMGR_MEM_RGN_SIZE_MASK);

    /*
     * 2 - 0 reg_size  This field indicates the size of the memory region (in R/W 0h
     *                 terms of number of descriptors). It is an encoded value
     *                 that specifies region size as 2^(5+reg_size) number of
     *                 descriptors.
     * 11 - 8 desc_size  This field indicates the size of each descriptor in this R/W 0h
     *                   memory region. It is an encoded value that specifies
     *                   descriptor size as 2^(5+desc_size) number of bytes.
     *                   The settings of desc_size from 9-15 are reserved.
     * 29 -16 start_index This field indicates where in linking RAM does the R/W 0h
     *                    descriptor linking information corresponding to memory
     *                    region R starts.
     */

    /* Memory Region R Control Register */

    USB_MHDRC_QMGR_REG_WRITE32 (pMHDRC,
                                QMGR_MEM_REGION_CTRL(rgn),
                                ctrl);
    *memRgn = (UINT8)rgn;
    return OK;
    }


/*******************************************************************************
*
* usbMhdrcCppiDmaUnInit - un-initialize the MHDRC CPPI DMA
*
* This routine un-initializes the MHDRC CPPI DMA.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbMhdrcCppiDmaUnInit
    (
    pUSB_MUSBMHDRC  pMHDRC
    )
    {
    pUSB_MHDRC_DMA_DATA              pDMAData = NULL;
    pUSB_MHDRC_CPPIDMA               pCppi = NULL;

    pDMAData = &(pMHDRC->dmaData);
    pCppi = pDMAData->pCppi;

    if (pCppi != NULL)
        {
        if (pCppi->pPdMem != NULL)
            cacheDmaFree(pCppi->pPdMem);
        if (pCppi->tearDownRegion.addr != NULL)
            cacheDmaFree(pCppi->tearDownRegion.addr);
        if (pCppi->pLinkRamRegion != NULL)
            cacheDmaFree(pCppi->pLinkRamRegion);

        MHDRC_FREE(pDMAData->pCppi);
        pDMAData->pCppi = NULL;
        }

    return OK;
    }

/*******************************************************************************
*
* usbMhdrcCppiDmaInit - initialize the MHDRC CPPI DMA
*
* This routine initialize the MHDRC CPPI41 DMA.
*
* RETURNS: OK, or ERROR (Memory not allocated)
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbMhdrcCppiDmaInit
    (
    pUSB_MUSBMHDRC  pMHDRC
    )
    {
    pUSB_MHDRC_DMA_DATA              pDMAData = NULL;
    pUSB_MHDRC_CPPIDMA_TEARDOWN_DESC pCurTD = NULL;
    pUSB_MHDRC_CPPIDMA               pCppi = NULL;
    pUSB_MHDRC_CPPIDMA_PKT_DESC      pCurPD = NULL;

    UINT32  uRegValue;
    UINT32  uTDAddr;
    UINT32  uPDAddr;
    size_t  uRgnSize;
    int     status;
    int     i;

    /*
     * Schedule table
     * Each word of this table descirbe 4 entries of the DMA scheduler.
     * One byte for one entry,
     * bit 7 descirbe the entry's rxtx attribute
     * bit 4-0 descibe the entry's DMA channnel number.
     */

    UINT32 dmaScheduleTable[] = {
        0x81018000, 0x83038202, 0x85058404, 0x87078606,
        0x89098808, 0x8b0b8a0a, 0x8d0d8c0c, 0x8f0f8e0e,
        0x91119010, 0x93139212, 0x95159414, 0x97179616,
        0x99199818, 0x9b1b9a1a, 0x9d1d9c1c, 0x00009e1e,
    };

    /* Malloc CPPI DMA private data */

    pCppi = (USB_MHDRC_CPPIDMA *)MHDRC_MALLOC(sizeof(USB_MHDRC_CPPIDMA));

    if (NULL == pCppi)
        {
        USB_MHDRC_ERR("usbMhdrcCppiDmaInit(): "
                      "Memory not allocated for pCppi\n",
                        1, 2, 3, 4, 5, 6);
        return ERROR;
        }

    OS_MEMSET(pCppi, 0, sizeof(USB_MHDRC_CPPIDMA));

    pDMAData = &(pMHDRC->dmaData);
    pDMAData->pCppi = (void *)pCppi;

    /*
     * Allocate Linking RAM
     * This region should not exceed 64K
     */

    pCppi->pLinkRamRegion =  (void *)cacheDmaMalloc(USB_MHDRC_LINK_RAM_SIZE * 4);
    if (NULL == pCppi->pLinkRamRegion)
        {
        USB_MHDRC_ERR("usbMhdrcCppiDmaInit(): "
                      "Memory not allocated for pMemRegion\n",
                      1, 2, 3, 4, 5, 6);
        usbMhdrcCppiDmaUnInit(pMHDRC);
        return ERROR;
        }

    OS_MEMSET(pCppi->pLinkRamRegion, 0, (USB_MHDRC_LINK_RAM_SIZE * 4));

    /* Set ram0 base address and size */

    uRegValue = (UINT32)USB_MHDRC_VIRT_TO_PHYS(pCppi->pLinkRamRegion);

    USB_MHDRC_QMGR_REG_WRITE32 (pMHDRC,
                                QMGR_LINK_RAM0_BASE_ADDR,
                                uRegValue);
    USB_MHDRC_QMGR_REG_WRITE32 (pMHDRC,
                                QMGR_LINK_RAM0_SIZE,
                                USB_MHDRC_LINK_RAM_SIZE);
    /*
     * Set TDFDQ
     * The CPPI DMA teardown free descriptor queue control register (TDFDQ)
     * is used to inform the DMA of the location in memory or descriptor
     * array which is to be used for signaling of a teardown complete for each
     * Tx and Rx channel.
     * 13,12 td_desc_qmgr
     * 11-0  td_desc_qnum
     */

    uRegValue = (USB_MHDRC_CCPI_DMA_TD_Q_NUM << DMA_TD_DESC_QNUM_SHIFT);

    USB_MHDRC_CPPIDMA_REG_WRITE32 (pMHDRC,
                                   DMA_TEADOWN_FREE_DESCRIPTOR_QUEUE_CTRL,
                                   uRegValue);

    uRgnSize = 64 * sizeof(USB_MHDRC_CPPIDMA_TEARDOWN_DESC);

    pCppi->tearDownRegion.rgnSize = uRgnSize;
    pCppi->tearDownRegion.addr = (void *)cacheDmaMalloc(uRgnSize);
    if (pCppi->tearDownRegion.addr == NULL)
        {
        USB_MHDRC_ERR("usbMhdrcCppiDmaInit(): "
                      "Memory not allocated for usbMhdrcCppiDmaTeardown\n",
                       1, 2, 3, 4, 5, 6);
        usbMhdrcCppiDmaUnInit(pMHDRC);
        return ERROR;
        }

    status = usbMhdrcCppiDmaMemRgnAlloc(pMHDRC, 0,
                                        pCppi->tearDownRegion.addr, 5,
                                        6, &pCppi->tearDownRegion.memRgn);
    if (status < 0)
        {
        USB_MHDRC_ERR("usbMhdrcCppiDmaInit(): "
                      "MemRgnAlloc Error!!\n",
                       1, 2, 3, 4, 5, 6);
        usbMhdrcCppiDmaUnInit(pMHDRC);
        return ERROR;
        }

    pCppi->tearDownRegion.queNum = USB_MHDRC_CCPI_DMA_TD_Q_NUM;
    pCppi->tearDownRegion.queMgr = 0;


    pCurTD = (pUSB_MHDRC_CPPIDMA_TEARDOWN_DESC)pCppi->tearDownRegion.addr;
    uTDAddr = (UINT32)USB_MHDRC_VIRT_TO_PHYS(pCurTD);

    /*
     * Queue N register D
     * write to add a packet to the queue
     * read to pop a packet off the queue
     * 31 - 5 desc ptr; 4 - 0 desc size (x * 4 + 24) = real size
     */

    for (i = 0; i < 64; i++)
        {
        uRegValue = (((sizeof(USB_MHDRC_CPPIDMA_TEARDOWN_DESC) - 24) >>
                (2 - QMGR_QUEUE_DESC_SIZE_SHIFT)) & QMGR_QUEUE_DESC_SIZE_MASK);

        uRegValue |=(uTDAddr & QMGR_QUEUE_DESC_PTR_MASK);

        USB_MHDRC_QMGR_REG_WRITE32 (pMHDRC,
                                    QMGR_QUEUE_CTRL_D(USB_MHDRC_CCPI_DMA_TD_Q_NUM),
                                    uRegValue);
        uTDAddr = (UINT32)(uTDAddr + sizeof(USB_MHDRC_CPPIDMA_TEARDOWN_DESC));
        }

    /* Initialize DMA scheduler for USB0 and USB1. */

    for (i = 0; i < 15; i++)
        {
        USB_MHDRC_DMASCHED_REG_WRITE32 (pMHDRC,
                                        DMA_SCHEDULER_TABLE_WORD(i),
                                        dmaScheduleTable[i]);
        }

    /* CPPI DMA Scheduler Control Register set for USB0 and USB 1 */

    uRegValue = ((USB_MHDRC_CPPIDMA_NUM_CH * 4 - 1) << DMA_SCHED_LAST_ENTRY_SHIFT)
                | DMA_SCHED_ENABLE_MASK;

    USB_MHDRC_DMASCHED_REG_WRITE32 (pMHDRC,
                                    DMA_SCHEDULER_CTRL,
                                    uRegValue);

    /* Enable all the interrupt */

    USB_MHDRC_USBSS_REG_WRITE32 (pMHDRC, USB_MHDRC_USBSS_EOI, 0);
    uRegValue = USB_MHDRC_USBSS_REG_READ32 (pMHDRC, USB_MHDRC_USBSS_IRQ_ENABLE_SET);

    /* check of this is for USBx with Base Register Addr */

    if ((pMHDRC->uRegBase & USB_MHDRC_USBx_CHECK_ADR_MASK) == 0x000)
        {
        /*  rx_pkt_cmp_0 | tx_pkt_cmp_0 | pd_cmp_flag for USB 0 */

        uRegValue = (uRegValue & 0xfffffc00) | 0x304;
        USB_MHDRC_USBSS_REG_WRITE32 (pMHDRC,
                                     USB_MHDRC_USBSS_IRQ_ENABLE_SET,
                                     uRegValue);
        USB_MHDRC_USBSS_REG_WRITE32 (pMHDRC,
                                     USB_MHDRC_USBSS_IRQ_DMA_ENABLE0,
                                     0xFFFEFFFE);
        }
    else
        {
        /*  rx_pkt_cmp_1 | tx_pkt_cmp_1 | pd_cmp_flag for USB 1 */

        uRegValue = (uRegValue & 0xfffff300) | 0xc04;
        USB_MHDRC_USBSS_REG_WRITE32 (pMHDRC,
                                     USB_MHDRC_USBSS_IRQ_ENABLE_SET,
                                     uRegValue);
        USB_MHDRC_USBSS_REG_WRITE32 (pMHDRC,
                                     USB_MHDRC_USBSS_IRQ_DMA_ENABLE1,
                                     0xFFFEFFFE);
        }

    /* USB_MHDRC_CPPIDMA_MAX_PD = 64*15 */

    pCppi->pPdMem = (void *)cacheDmaMalloc(USB_MHDRC_CPPIDMA_MAX_PD * USB_MHDRC_CPPIDMA_DESC_ALIGN);

    if (pCppi->pPdMem == NULL)
        {
        usbMhdrcCppiDmaUnInit(pMHDRC);
        return ERROR;
        }

    status = usbMhdrcCppiDmaMemRgnAlloc(pMHDRC, 0, pCppi->pPdMem,
                                        USB_MHDRC_CPPIDMA_DESC_SIZE_SHIFT,
                                        10,
                                        &pCppi->pdMemRegion);
    if (status < 0)
        {
        USB_MHDRC_ERR("usbMhdrcCppiDmaInit(): "
                      "MemRgnAlloc Error!!\n", 1, 2, 3, 4, 5, 6);
        usbMhdrcCppiDmaUnInit(pMHDRC);
        return ERROR;
        }

    pCurPD = (pUSB_MHDRC_CPPIDMA_PKT_DESC)pCppi->pPdMem;
    uPDAddr = (UINT32)USB_MHDRC_VIRT_TO_PHYS(pCppi->pPdMem);

    /* Link all the pd */

    semTake(pDMAData->pSynchMutex, WAIT_FOREVER);
    for (i = 0; i < USB_MHDRC_CPPIDMA_MAX_PD; i++)
        {
        pCurPD->dmaAddr = uPDAddr;
        usbMhdrcCppiDmaPDPut(pCppi, pCurPD);
        pCurPD = (pUSB_MHDRC_CPPIDMA_PKT_DESC)
                 ((char *)pCurPD + USB_MHDRC_CPPIDMA_DESC_ALIGN);
        uPDAddr += USB_MHDRC_CPPIDMA_DESC_ALIGN;
        }
    semGive(pDMAData->pSynchMutex);

    /* Disable auto request mode */

    USB_MHDRC_CENTAURUS_PRIVATE_REG_WRITE32 (pMHDRC,
                                             USB_MHDRC_CENTAURUS_CENTAURUS_AUTOREQ,
                                             0);

    /* Disable the CDC/RNDIS modes */

    USB_MHDRC_CENTAURUS_PRIVATE_REG_WRITE32 (pMHDRC,
                                             USB_MHDRC_CENTAURUS_TXMODE,
                                             0);
    USB_MHDRC_CENTAURUS_PRIVATE_REG_WRITE32 (pMHDRC,
                                             USB_MHDRC_CENTAURUS_RXMODE,
                                             0);

    return OK;
    }

/*******************************************************************************
*
* usbMhdrcCentaurusCppiDmaSetup - configure Centaurus CPPI41 DMA information
*
* This routine configures Centaurus(814x/387x) CPPI41 DMA information.
*
* RETURNS: OK, or ERROR if failed
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL STATUS usbMhdrcCentaurusCppiDmaSetup
    (
    pUSB_MUSBMHDRC  pMHDRC
    )
    {
    pUSB_MHDRC_DMA_DATA   pDMAData = NULL;
    char                  ThreadName[USB_MHDRC_TASK_NAME_MAX];

    pDMAData = &(pMHDRC->dmaData);

#if defined (USB_MHDRC_DMA_ENABLE)
    USB_MHDRC_VDBG("usbMhdrcCentaurusCppiDmaSetup(): Called\n",
                   1, 2, 3, 4, 5, 6);

    if (pMHDRC->bDmaEnabled)
        {
        pDMAData->pSynchMutex = semMCreate(SEM_Q_PRIORITY |
                                           SEM_INVERSION_SAFE |
                                           SEM_DELETE_SAFE);

        if (pDMAData->pSynchMutex == NULL)
            {
            USB_MHDRC_ERR("usbMhdrcCentaurusCppiDmaSetup(): "
                          "Creat pSynchMutex for DmaData failed\n",
                          1, 2, 3, 4, 5, 6);
            return ERROR;
            }

        pDMAData->isrEvent = OS_CREATE_EVENT(OS_EVENT_NON_SIGNALED);

        if (pDMAData->isrEvent == NULL)
            {
            USB_MHDRC_ERR("usbMhdrcCentaurusCppiDmaSetup(): "
                          "Creat pSynchMutex for DmaData failed\n",
                          1, 2, 3, 4, 5, 6);
            usbMhdrcPlatformCppiDmaUnSetup(pMHDRC);
            return ERROR;
            }

        snprintf((char*)ThreadName,
            USB_MHDRC_TASK_NAME_MAX, "MCPPIDMA_IH%d", pMHDRC->pDev->unitNumber);

        pDMAData->irqThread =
                     OS_CREATE_THREAD((char *)ThreadName,
                                       USB_MHDRC_DMA_INT_THREAD_PRIORITY,
                                       usbMhdrcCppiDmaInterruptHandler,
                                       pMHDRC);

        /* Check whether the thread creation is successful */

        if (OS_THREAD_FAILURE == pDMAData->irqThread)
            {
            USB_MHDRC_ERR("usbMhdrcCentaurusCppiDmaSetup(): "
                          "Interrupt handler thread is not created\n",
                         1, 2, 3, 4, 5 ,6);
            usbMhdrcPlatformCppiDmaUnSetup(pMHDRC);
            return ERROR;
            }

        /* DMA channel number */

        pDMAData->uMaxDmaChannel = pMHDRC->uNumDmaChannels;

        /*
         * Set FreeDmaChannelBitMap bit[n] = 1 means the channel[n] is idle
         * or else it's busy
         */

        pDMAData->uFreeDmaChannelBitMap = (UINT16)
            ((0x1 << pDMAData->uMaxDmaChannel) - 1);

        /* Set DMA type */

        pDMAData->uDMAType = USB_MHDRC_CPPI_DMA;

        /* Update the operation pointers */

        pDMAData->pDmaChannelRequest = usbMhdrcCppiDmaFreeChannelRequest;
        pDMAData->pDmaChannelRelease = usbMhdrcCppiDmaChannelRelease;
        pDMAData->pDmaConfigure = usbMhdrcCppiDmaChannelConfigure;
        pDMAData->pDmaCancel = usbMhdrcCppiDmaCancel;
        pDMAData->pDmaIsr = usbMhdrcCppiDmaIsr;
        pDMAData->pDmaIrqHandler = usbMhdrcCppiDmaInterruptHandler;

        if ((pMHDRC->uRegBase & USB_MHDRC_USBx_CHECK_ADR_MASK) == 0x000)
            pDMAData->uDmaOffset = 0;  /* USB 0 */
        else                           /* USB 1 */
            pDMAData->uDmaOffset = USB_MHDRC_CPPI_START_NUM_RANGE;

        if (usbMhdrcCppiDmaInit(pMHDRC) != OK)
            {
            USB_MHDRC_ERR("usbMhdrcCentaurusCppiDmaSetup(): "
                          "failed in usbMhdrcCppiDmaInit()\n",
                            1, 2, 3, 4, 5 ,6);
            usbMhdrcPlatformCppiDmaUnSetup(pMHDRC);
            return ERROR;
            }

        if (pDMAData->pDmaIsr != NULL)
            {
            if (ERROR == (vxbIntConnect (pMHDRC->pDev,
                                         1,
                                         pDMAData->pDmaIsr,
                                         (void *)pMHDRC)))
                {
                USB_MHDRC_ERR("usbMhciCppiDmaSetup(): "
                              "Error hooking the DMA ISR\n",
                              1, 2, 3, 4, 5 ,6);
                usbMhdrcPlatformCppiDmaUnSetup(pMHDRC);
                return ERROR;
                }
            pDMAData->isrMagic = USB_MHDRC_MAGIC_ALIVE;
            }
        }
#endif

    return OK;
    }


/*******************************************************************************
*
* usbMhdrcPlatformCppiDmaSetup - configure platform DMA based on platform type
*
* This routine configures the platform DMA according to the platform type.
*
* RETURNS: OK, or ERROR if failed to configure the platform DMA.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbMhdrcPlatformCppiDmaSetup
    (
    pUSB_MUSBMHDRC  pMHDRC
    )
    {
    STATUS result;

    USB_MHDRC_VDBG("usbMhdrcPlatformCppiDmaSetup(): Called\n",
                      1, 2, 3, 4, 5, 6);

    switch (pMHDRC->uPlatformType)
        {
        case USB_MHDRC_PLATFORM_CENTAURUS:
            /* fall through */

        case USB_MHDRC_PLATFORM_CENTAURUS_TI816X:
#if  defined (USB_MHDRC_CENTAURUS)
            result = usbMhdrcCentaurusCppiDmaSetup(pMHDRC);
            if (result == ERROR)
                {
                USB_MHDRC_ERR("usbMhdrcPlatformCppiDmaSetup(): failed\n",
                              1, 2, 3, 4, 5, 6);
                return ERROR;
                }
#else
            USB_MHDRC_WARN("usbMhdrcPlatformCppiDmaSetup(): USB_MHDRC_OMAP not defined\n");
            return ERROR;
#endif
            break;
        default:
            return ERROR;
        }
    return OK;
    }

/*******************************************************************************
*
* usbMhdrcPlatformCppiDmaUnSetup - deconfigure platform DMA based on platform type
*
* This routine deconfigures the platform DMA according to the platform type.
*
* RETURNS: OK, or ERROR if failed to configure the platform DMA.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

STATUS usbMhdrcPlatformCppiDmaUnSetup
    (
    pUSB_MUSBMHDRC  pMHDRC
    )
    {
    pUSB_MHDRC_DMA_DATA   pDMAData = NULL;

    if (pMHDRC == NULL)
        {
        USB_MHDRC_ERR("usbMhdrcPlatformCppiDmaUnSetup(): Invalid Paramter pMHDRC %p\n",
                      pMHDRC, 2, 3, 4, 5, 6);
        return ERROR;
        }

    USB_MHDRC_VDBG("usbMhdrcPlatformCppiDmaUnSetup(): Called\n",
                      1, 2, 3, 4, 5, 6);

    pDMAData = &(pMHDRC->dmaData);

    /* Disable Dma ISR if DMA ISR is sepreate with usb core ISR */

    if (pDMAData->pDmaIsr)
        {
        if (pDMAData->isrMagic == USB_MHDRC_MAGIC_ALIVE)
            {
            vxbIntDisconnect (pMHDRC->pDev,
                              1,
                              pDMAData->pDmaIsr,
                              (void *)(pMHDRC));
            pDMAData->isrMagic = USB_MHDRC_MAGIC_DEAD;
            }
        }

    if (pDMAData->pSynchMutex)
        {
        semTake(pDMAData->pSynchMutex, WAIT_FOREVER);
        semDelete(pDMAData->pSynchMutex);
        pDMAData->pSynchMutex = NULL;
        }

    if (pDMAData->isrEvent)
        {
        semDelete(pDMAData->isrEvent);
        pDMAData->isrEvent = NULL;
        }

    if ((pDMAData->irqThread != 0) &&
        (pDMAData->irqThread != OS_THREAD_FAILURE))
        {
        OS_DESTROY_THREAD(pDMAData->irqThread);
        pDMAData->irqThread = OS_THREAD_FAILURE;
        }

    /* Release all the memory allocated for CPPI */

    usbMhdrcCppiDmaUnInit(pMHDRC);

    return OK;
    }

/*******************************************************************************
*
* usbMhdrcCppiDmaIntrEnable - enable MHDRC USB CPPI DMA interrupt
*
* This routine enables MHDRC USB CPPI DMA interrupt.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcCppiDmaIntrEnable
    (
    pUSB_MUSBMHDRC pMHDRC
    )
    {
    pUSB_MHDRC_DMA_DATA   pDMAData = NULL;

    if (NULL == pMHDRC)
        {
        USB_MHDRC_ERR("usbMhdrcCppiDmaIntrEnable(): Invalid parameter\n",
                       1, 2, 3, 4, 5 ,6);

        return;
        }

    USB_MHDRC_VDBG("usbMhdrcCppiDmaIntrEnable(): Called\n",
                       1, 2, 3, 4, 5 ,6);

    pDMAData = &(pMHDRC->dmaData);

    /* Enable Dma Isr if needed */

    if (pDMAData->pDmaIsr)
        {
        if (vxbIntEnable (pMHDRC->pDev,
                          1,
                          pDMAData->pDmaIsr,
                          (void *)pMHDRC) == ERROR)
            {
            USB_MHDRC_ERR("usbMhdrcCppiDmaIntrEnable():Error enabling DMA intrrupts\n", 1, 2, 3, 4, 5 ,6);
            return;
            }
        pDMAData->isrMagic = USB_MHDRC_MAGIC_ALIVE;
        }

    return;
    }

/*******************************************************************************
*
* usbMhdrcCppiDmaIntrDisable - disable MHDRC USB CPPI DMA interrupt
*
* This routine disables MHDRC USB CPPI DMA interrupt.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

void usbMhdrcCppiDmaIntrDisable
    (
    pUSB_MUSBMHDRC pMHDRC
    )
    {
    pUSB_MHDRC_DMA_DATA   pDMAData = NULL;

    if (NULL == pMHDRC)
        {
        USB_MHDRC_ERR("usbMhdrcCppiDmaIntrDisable(): Invalid parameter\n",
                       1, 2, 3, 4, 5 ,6);

        return;
        }

    USB_MHDRC_VDBG("usbMhdrcCppiDmaIntrDisable(): Called",
                    1, 2, 3, 4, 5 ,6);


    pDMAData = &(pMHDRC->dmaData);

    /* Disable Dma Isr if needed */

    if (pDMAData->pDmaIsr)
        {
        if (vxbIntDisable (pMHDRC->pDev,
                           1,
                           pDMAData->pDmaIsr,
                          (void *)pMHDRC) == ERROR)
            {
            USB_MHDRC_ERR("usbMhdrcCppiDmaIntrDisable(): "
                          "Error disabling DMA intrrupts\n", 1, 2, 3, 4, 5 ,6);
            return;
            }
        pDMAData->isrMagic = USB_MHDRC_MAGIC_DEAD;
        }

    return ;
    }
