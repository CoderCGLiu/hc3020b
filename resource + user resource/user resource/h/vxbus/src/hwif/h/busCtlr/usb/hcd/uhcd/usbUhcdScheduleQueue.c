/*  usbUhcdScheduleQueue.c - USB UHCD HCD schdule queue routines */

/*
 * Copyright (c) 2003-2011, 2013-2014, 2016 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/* Copyright 2003-2011, 2013-2014, 2016 Wind River Systems, Inc.

   This software includes software licensed to Wind River Systems, Inc.
   by Wipro, Ltd. Wind River licensees may use this software according
   to the terms of their Wind River license agreement(s) applicable to
   this software.
*/

/*
Modification history
--------------------
02l,03feb16,j_x  fix crash during usbUhcdCancelUrb (VXW6-85005)
02k,25Feb14,wyy  Enable vxbDmaBuf alignment checking (VXW6-70065)
02j,01aug13,wyy  Remove END debugging errors on detach (WIND00427931)
02i,03may13,wyy  Remove compiler warning (WIND00356717)
02h,21apr13,wyy  Cut the relationship among busIndex of HCDData, unitNumber of 
                 vxBus Device, and HC count (such as g_EHCDControllerCount or 
                 g_UhcdHostControllerCount) to dynamically release or announce
                 a HCD driver (WIND00362065)
02g,13dec11,m_y  Modify according to code check result (WIND00319317)
02f,01aug11,ghs  Fix issue found during code review (WIND00255117)
02e,22jul10,m_y  Modify host controller index compare
02d,08jul10,m_y  Modify for coding convention
02c,25jun10,w_x  Correct debug logging macro
02b,18may10,w_x  USB transition to vxbDmaBufLib supporting 64 bit (WIND00193267)
02a,23mar10,j_x  Changed for USB debug (WIND00184542)
01z,16mar10,ghs  Fix vxWorks 6.9 LP64 adapting
01y,13jan10,ghs  vxWorks 6.9 LP64 adapting
01x,16oct09,ghs  Fix coverity fix error(WIND00186558)
01w,17sep09,y_l  Code Coverity CID(6316, 6317): NULL pointer check(WIND00182326)
01v,20jun09,w_x  Correct modification time for WIND00156002
01u,10jun09,w_x  Fix use pointer after free in delete QH context (WIND00169520)
01t,20mar09,w_x  Change the non-isoc request queue management (WIND00156002)
01s,19dec08,w_x  Update usbUhcdCancelUrb in favor of cancel URB which is in the
                 middle of reqeust queue; And big endian change.
01r,20aug08,w_x  Do not check scanPipe->qh == NULL for isoc pipe (WIND00130646)
01q,30jul08,w_x  Remvoed forward declaration for uhc_ProcessCompletedTDs
01p,07jul08,w_x  Code coverity changes (WIND00126885)
01o,04jun08,w_x  Added usbUhcdDeleteQhContext to separate pipe deletion;
                 some protection area adjustment;
                 DMA invalidate/flush QH/TD when update fields;
                 some coding error corrections;
                 WIND00104481 merge. (WIND00121282 fix)
01n,30aug07,adh  'Defect WIND00089969 fix
01m,08oct06,ami  Changes for USB-vxBus changes
01l,17may05,mta  Bandwidth Reclamation Enabled under macro
01k,12may05,mta  Fix for retrying NAKed tokens in same SOF
01j,03mar05,mta  SPR 96604
01i,02mar05,ami  SPR #106383 (Max UHCI Host Controller Issue)
01h,25feb05,mta  SPR 106276
01g,15oct04,ami  Refgen Changes
01f,05oct04,mta  SPR100704- Removal of floating point math
01e,11aug04,mta  Fix for print-stop-print issue with ISP1582
01d,03aug04,mta  coverity error fixes
01c,03aug04,mta  Coverity error fixes
01b,26jun03,mat  changing the code to WRS standards.
01a,25apr03,ram  written.
*/

/*
DESCRIPTION

This file contains functions which are used for transfer
scheduling and management.

INCLUDE FILES:  usb2/usbOsal.h, usb2/usbHst.h, usb2/usbUhci.h,
usb2/usbUhcdCommon.h, usb2/usbUhcdScheduleQueue.h,
usb2/usbUhcdSupport.h, usb2/usbUhcdScheduleQSupport.h,
usb2/usbUhcdRhEmulate.h, usb/usbPciLib.h

*/

/*
INTERNAL
 *******************************************************************************
 * Filename         : UHCD_ScheduleQueue.c
 *
 * Copyright        :
 *
 * THE COPYRIGHT IN THE CONTENTS OF THIS SOFTWARE VEST WITH WIPRO
 * LIMITED A COMPANY INCORPORATED UNDER THE LAWS OF INDIA AND HAVING
 * ITS REGISTERED OFFICE AT DODDAKANNELLI SARJAPUR ROAD  BANGALORE
 * 560 035. DISTRIBUTION OR COPYING OF THIS SOFTWARE BY
 * ANY INDIVIDUAL OR ENTITY OTHER THAN THE ADDRESSEE IS STRICTLY
 * PROHIBITED AND MAY INCUR LEGAL LIABILITY. IF YOU ARE NOT THE
 * ADDRESSEE PLEASE NOTIFY US IMMEDIATELY BY PHONE OR BY RETURN EMAIL.
 * THE ADDRESSEE IS ADVISED TO MAINTAIN THE PROPRIETARY INTERESTS OF
 * THIS COPYRIGHT AS PER APPLICABLE LAWS.
 *
 *
 * Description      :  This file contains functions which are used for
 *                     transfer scheduling and management.
 *
 *
 ******************************************************************************/


/* includes */

#include "usb/usbOsal.h"
#include "usb/usbHst.h"
#include "usbUhci.h"
#include "usbUhcdCommon.h"
#include "usbUhcdScheduleQueue.h"
#include "usbUhcdSupport.h"
#include "usbUhcdScheduleQSupport.h"
#include "usbUhcdRhEmulate.h"

/* Pointer to the global array of Host controller structures.*/

extern pUSB_UHCD_DATA * g_pUHCDData;
extern UINT32           g_UhcdHostControllerCount;
/* forward declarations */

USBHST_STATUS usbUhcdSetupControlPipe
    (
    pUSB_UHCD_DATA pHCDData,
    pUSB_UHCD_PIPE pHCDPipe
    );

BOOLEAN usbUhcdIsValidPipe
    (
    UINT8               uBusIndex,
    USB_UHCD_PIPE *     pHCDPipe
    );

USBHST_STATUS usbUhcdResetPipe
    (
    pUSB_UHCD_DATA  pHCDData,
    pUSB_UHCD_PIPE  pHCDPipe
    );

/*******************************************************************************
*
* usbUhcdCreateFrameList - create frame list
*
* The HCD maintains
* - a tree of 256 elements to maintain the bandwidth usage in every frame
* for the interrupt transfers.
* - an array of 1024 static isochronous Transfer Descriptor elements.
*
* This routine allocates memory for the data structures and initialises
* them. Also this function allocates memory for the frame list maintained
* by the UHCI Host controller and initialises the list.
*
* RETURNS: FALSE if the frame list creation is a failure, TRUE if OK
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOLEAN usbUhcdCreateFrameList
    (
    pUSB_UHCD_DATA      pHCDData
    )
    {
    /* Temporary pointer to hold the memory allocated for an isochronous TD */

    USB_UHCD_TD *   pTD = NULL;

    /* Temporary pointer to hold the memory allocated for the Queue Head */

    USB_UHCD_QH *   pQH = NULL;

    /* To serve as an index element */

    UINT16          i = 0;

    /*
     * To hold the index of the link element in the periodic tree
     * maintained by the HCD.
     */

    INT32           linkIndex = 0;

    /* To hold the pointer to the TD */

    USB_UHCD_TD *   pDummyTD = NULL;

    UINT32          dword0 = 0;

    STATUS          sts;

    /*
     * The HCD maintains the following information
     * - A periodic tree of 256 elements to hold the bandwidth usage of
     *   the interrupt endpoints. The tip of the periodic tree will be
     *   pointing to the list containing the control and bulk Queue Heads.
     * - A periodic table which is a static array of 1024 elements with each
     *   element in the array containing information on the bandwidth usage
     *   of the isochronous endpoints.
     * Each element of the periodic table will be pointing to a leaf of
     * the periodic tree.
     *
     * The UCHI Host Controller maintains a frame list with each element in the
     * list pointing to the corresponding element of the periodic table. This
     * enables the Host controller to traverse the list in every frame.
     */

    /*
     * UHCI spec 1.3 Scheduling (copied here to asist understanding)
     *
     * The HCD software sets up and manages the data structures to ensure
     * that Isochronous traffic has the highest priority in the Host Controller.
     * The HCD scheduling allows up to 90% of the frame bandwidth to be
     * allocated to isochronous and interrupt traffic, and up to 10% of the
     * frame bandwidth for control. Any remaining bandwidth can be reclaimed
     * for control and bulk transfers. Scheduling with the UHCI is handled by
     * a Frame List (up to 1024 entries). Each entry is a pointer to the first
     * structure to process in a given frame. Because these pointers are a
     * full 32 bits long, a 1024 entry Frame List occupies 4096 bytes of memory
     * (one page).
     *
     * Control and bulk transfers are scheduled last to allow bandwidth
     * reclamation on a lightly loaded USB. Bandwidth reclamation allows the
     * hardware to continue executing a schedule until time runs out in the
     * frame, cycling through queue entries as frame time allows. Control is
     * scheduled first to prioritize it over bulk transfers. Also, the software
     * does the scheduling to guarantee that at least 10% of the bandwidth is
     * available for control transfers. UHCI only allows for bandwidth
     * reclamation of full speed control and bulk transfers. The software
     * must schedule low speed control transfers such that they are guaranteed
     * to complete within the current frame. Low speed bulk transfers are
     * not allowed by the USB specification. If full speed control or bulk
     * transfers are in the schedule, the last QH points back to the beginning
     * of the full speed control and bulk queues to allow bandwidth reclamation.
     * As long as time remains in the frame, the full speed control and bulk
     * queues continue to be processed. If bandwidth reclamation is not
     * required, the last QH contains a terminate bit to inform the Host
     * Controller to wait until the beginning of the next frame.
     */

    /* Allocate memory for the periodic tree */

    pHCDData->pPeriodicTree = (USB_UHCD_PERIODIC_TREE_NODE *)
                            OS_MALLOC(sizeof(USB_UHCD_PERIODIC_TREE_NODE) * 255);

    /* Check if memory allocation is successful */

    if (pHCDData->pPeriodicTree == NULL)
        {
        USB_UHCD_ERR("usbUhcdCreateFrameList - "
            "allocate memory failed for periodic tree \n",
            1, 2, 3, 4, 5, 6);

        return (FALSE);
        }

    /* Allocate memory for the periodic table */

    pHCDData->pPeriodicTable = (USB_UHCD_PERIODIC_TABLE_ENTRY *)
                             OS_MALLOC(sizeof(USB_UHCD_PERIODIC_TABLE_ENTRY) *
                             USBUHCD_FRAME_LIST_ENTRIES);

    /* Check if memory allocation is successful */

    if (pHCDData->pPeriodicTable == NULL)
        {
        USB_UHCD_ERR("usbUhcdCreateFrameList - "
            "allocate memory failed for periodic table\n",
            1, 2, 3, 4, 5, 6);

        usbUhcdDeleteFrameList(pHCDData);

        return(FALSE);
        }

    /* Create map for periodic frame list */

    pHCDData->pFrameList = vxbDmaBufMemAlloc (pHCDData->pDev,
        pHCDData->frameListDmaTagId,
        NULL,
        VXB_DMABUF_MEMALLOC_CLEAR_BUF,
        &pHCDData->frameListDmaMapId);

    if (pHCDData->pFrameList == NULL)
        {
        USB_UHCD_ERR(
            "usbUhcdCreateFrameList - creating usbUhcdFrameList fail\n",
            1, 2, 3, 4, 5, 6);

        usbUhcdDeleteFrameList(pHCDData);

        return FALSE;
        }

    USB_UHCD_DBG(
        "usbUhcdCreateFrameList - created usbUhcdFrameList %p\n",
        pHCDData->pFrameList, 0, 0, 0, 0, 0);

    /*
     * Load the DMA MAP so that we get the correct BUS address
     * to program the hardware
     */

    sts = vxbDmaBufMapLoad (pHCDData->pDev,
        pHCDData->frameListDmaTagId,
        pHCDData->frameListDmaMapId,
        pHCDData->pFrameList,
        USB_UHCD_MAX_FRAMELIST_SIZE,
        0);

    if (sts != OK)
        {
        USB_UHCD_ERR(
            "usbUhcdCreateFrameList - loading map frameListDmaMapId fail\n",
            1, 2, 3, 4, 5, 6);

        usbUhcdDeleteFrameList(pHCDData);

        return FALSE;
        }

    /* Fill the periodic table memory allocated with 0s */

    OS_MEMSET((void *) pHCDData->pPeriodicTable, 0,
              sizeof (USB_UHCD_PERIODIC_TABLE_ENTRY) *
              USBUHCD_FRAME_LIST_ENTRIES);

    /* Fill the periodic tree memory allocated with 0s */

    OS_MEMSET((void *) pHCDData->pPeriodicTree, 0,
              sizeof (USB_UHCD_PERIODIC_TREE_NODE) * 255);

    /* Create a control QH */

    pHCDData->pControlQH = usbUhcdFormEmptyQH(pHCDData);

    if (!pHCDData->pControlQH)
        {
        USB_UHCD_ERR("usbUhcdCreateFrameList - "
            "allocate memory failed for control QH\n",
            1, 2, 3, 4, 5, 6);

        usbUhcdDeleteFrameList(pHCDData);

        return (FALSE);
        }

    /* Create a bulk QH */

    pHCDData->pBulkQH = usbUhcdFormEmptyQH(pHCDData);

    if (!pHCDData->pBulkQH)
        {
        USB_UHCD_ERR("allocate memory failed for bulk QH \n",
                     1, 2, 3, 4, 5, 6);

        usbUhcdDeleteFrameList(pHCDData);

        return (FALSE);
        }

    /* Create a Bandwidth Reclamation QH */

    pHCDData->pBandwidthReclaimQH = usbUhcdFormEmptyQH(pHCDData);

    if (!pHCDData->pBandwidthReclaimQH)
        {
        USB_UHCD_ERR("usbUhcdCreateFrameList - "
            "allocate memory failed for bandwidth reclamation QH \n",
            1, 2, 3, 4, 5, 6);

        usbUhcdDeleteFrameList(pHCDData);

        return (FALSE);
        }

    /* Create a dummy TD */

    pDummyTD = usbUhcdCreateTD(pHCDData);

    if (!pDummyTD)
        {
        USB_UHCD_ERR("usbUhcdCreateFrameList - "
            "allocate memory failed for dummy TD\n",
            1, 2, 3, 4, 5, 6);

        usbUhcdDeleteFrameList(pHCDData);

        return (FALSE);
        }

    /* TD at this point is 16 byte alligned */

    /* The Link pointer field in TD references a TD and not a Queue Head */

    /* Indicate that this is not the last element in the queue */

    /* Make the path of traversal for the HC as depth first. */

    dword0 = 0;
    dword0 &= ~(USBUHCD_LINK_QH |
               USBUHCD_LINK_TERMINATE |
               USBUHCD_LINK_VF |
               USBUHCD_LINK_PTR_MASK);

    /* Update the TD's link pointer */

    dword0 |= USBUHCD_LINK_PTR_FMT(USB_UHCD_DESC_LO32(pDummyTD) >> 4);

    pDummyTD->dWord0Td = dword0;

    USB_UHCD_VDBG("usbUhcdCreateFrameList - "
        "dword0=0x%X, pDummyTD->dWord0Td=0x%X\n",
        dword0, pDummyTD->dWord0Td, 3, 4, 5, 6);

    USB_UHCD_MAKE_TD_LE(pHCDData->uBusIndex, pDummyTD);

    /* Update the Bandwidth Reclamation Queue head */

    pHCDData->pBandwidthReclaimQH->pTD = pDummyTD;

    dword0 = 0;
    dword0 &= ~(USBUHCD_LINK_PTR_MASK | USBUHCD_LINK_TERMINATE);
    dword0 |= (USBUHCD_LINK_PTR_FMT(USB_UHCD_DESC_LO32(pDummyTD) >> 4));

    pHCDData->pBandwidthReclaimQH->dWord1Qh = dword0;

    USB_UHCD_VDBG("usbUhcdCreateFrameList - "
        "dword0=0x%X, pHCDData->pBandwidthReclaimQH->dWord1Qh=0x%X \n",
        dword0, pHCDData->pBandwidthReclaimQH->dWord1Qh, 3, 4, 5, 6);
    /*
     * Update the link pointers of the control QH.
     * ie control QH points to bulk QH
     */

    pHCDData->pControlQH->pNextQH = pHCDData->pBulkQH;

    dword0 = 0;

    /* Make the QH as not the last element in the schedule */

    dword0 &= ~(USBUHCD_LINK_PTR_MASK | USBUHCD_LINK_TERMINATE);

    /* Indicate that the element next to the control QH is a bulk QH */

    dword0 |=
        USBUHCD_LINK_PTR_FMT(USB_UHCD_DESC_LO32(pHCDData->pBulkQH) >> 4);

    pHCDData->pControlQH->dWord0Qh = USBUHCD_LINK_QH | dword0;

    USB_UHCD_VDBG("dword0=0x%X, pHCDData->pControlQH->dWord0Qh=0x%X \n",
                  dword0, pHCDData->pControlQH->dWord0Qh, 3, 4, 5, 6);
    /*
     * Update the link pointers of the Bulk QH.
     * ie Bulk QH points to Bandwidth reclamation queue head
     */

    pHCDData->pBulkQH->pNextQH = pHCDData->pBandwidthReclaimQH;

    dword0 = 0;

    /* Make the QH as not the last element in the schedule */

    dword0 &= ~(USBUHCD_LINK_PTR_MASK | USBUHCD_LINK_TERMINATE);

    /* Indicate that the element next to the control QH is a bulk QH */

    dword0 |= USBUHCD_LINK_QH |
        USBUHCD_LINK_PTR_FMT(USB_UHCD_DESC_LO32(pHCDData->pBandwidthReclaimQH) >> 4);

    pHCDData->pBulkQH->dWord0Qh = dword0;

    USB_UHCD_VDBG("usbUhcdCreateFrameList - "
        "dword0=0x%X, pHCDData->pBulkQH->dWord0Qh=0x%X \n",
        dword0, pHCDData->pBulkQH->dWord0Qh, 3, 4, 5, 6);
    /*
     * Note :
     * Enable this only for specific UHCI controlers;
     * See  ftp://download.intel.com/design/chipsets/specupdt/29063508.pdf
     * search for "USB Bandwidth Reclamation Errata"
     */

    /*
     * On page 19 of the above documentation writes:
     *
     * 4. USB Bandwidth Reclamation Errata
     *
     * Problem: This errata affects data transfers in conjunction with a
     * UHCI driver utilizing bandwidth reclamation. In a data structure
     * which implements bandwidth reclamation, when all the queue heads
     * have their terminate bit set (empty QH's), the USB subsystem will
     * be unable to read a new frame pointer and will continuously loop
     * through the bandwidth reclamation queue heads. The effect of the
     * errata is that the USB subsystem will continue to send out Start
     * Of Frame packets but transfer no data. On the PCI bus the PIIX4
     * will continuously read the queue heads within the bandwidth
     * reclamation loop. For additional information on PIIX4 host
     * controller operation refer to the Universal Host Controller
     * Interface (UHCI) Design Guide (order number 297650).
     *
     * Implication: The USB host controller stops transferring data on
     * the USB bus. The non-USB functions in the system will continue to
     * operate normally.
     *
     * Workaround: When using bandwidth reclamation, the UHCI driver should
     * insert a pseudo queue head with a pseudo transfer descriptor within
     * the bandwidth reclamation loop. The PIIX4 will fetch this queue head
     * and transfer descriptor on every frame, but will not transfer any
     * data and will never be terminated. The following bits must be properly
     * set to implement the workaround:
     *
     * TD LINK POINTER (DWORD 0: 00-03h)
     *
     * The Link Pointer (LP=bits [31:4]) must be set to point to itself.
     *
     * The Depth/Breadth Select bit (Vf=bit 2) must be set to 0 indicating
     * that the PIIX4 should execute breadth first.
     *
     * The QH/TD Select (Q=bit 1) must be set to 0 indicating it is a TD.
     *
     * The Terminate bit (T=bit 0) must be set to 0 indicating that the
     * link pointer field is valid.
     *
     * TD CONTROL AND STATUS (DWORD 1: 04-07h)
     *
     * The Active status bit (bit 23) must be left unset at 0 indicating
     * that the PIIX4 should not execute this TD.
     *
     * QUEUE HEAD LINK POINTER (DWORD 0: 00-03h)
     *
     * The Queue Head Link Pointer (QHLP=bits [31:4]) must be set to point
     * to the pseudo TD.
     *
     * The QH/TD Select (Q=bit 1) must be set to 1 indicating it is a QH.
     *
     * The Terminate bit (T=bit 0) must be set to 0 indicating that the
     * link pointer field points to a valid TD.
     *
     */

    /*
     * On page 38 of the above documentation writes:
     *
     * 23. Correction to the USB Bandwidth Reclamation Errata Workaround
     *
     * The workaround for the USB Bandwidth Reclamation Errata workaround is
     * not correctly documented in Errata number 4. The following changes are
     * required.
     * 1) The Queue Head Link Pointer must be set to point to the next Queue
     * Head, not the pseudo TD as indicated.
     * 2) The Queue Head Link Element Pointer (DW 04-07h) must be set to point
     * to the Pseudo TD.
     */

#ifdef UHCD_BANDWIDTH_RECLAMATION_SUPPORT

    /*
     * Update the link pointers of the Bandwidth reclamation QH.
     * ie BandWidth Reclamation QH points to Control queue head
     */

    pHCDData->pBandwidthReclaimQH->pNextQH = pHCDData->pControlQH;

    dword0 = 0;

    /* Make the QH as not the last element in the schedule */

    dword0 &= ~(USBUHCD_LINK_PTR_MASK | USBUHCD_LINK_TERMINATE);

    /* Indicate that the element next to the control QH is a bulk QH */

    dword0 |= USBUHCD_LINK_QH |
        USBUHCD_LINK_PTR_FMT(USB_UHCD_DESC_LO32(pHCDData->pControlQH) >> 4);

    pHCDData->pBandwidthReclaimQH->dWord0Qh = dword0;

    USB_UHCD_VDBG("usbUhcdCreateFrameList - "
        "dword0=0x%X, pHCDData->pBandwidthReclaimQH->dWord0Qh=0x%X \n",
        dword0, pHCDData->pBandwidthReclaimQH->dWord0Qh, 3, 4, 5, 6);
#endif

    USB_UHCD_MAKE_QH_LE(pHCDData->uBusIndex, pHCDData->pControlQH);
    USB_UHCD_MAKE_QH_LE(pHCDData->uBusIndex, pHCDData->pBulkQH);
    USB_UHCD_MAKE_QH_LE(pHCDData->uBusIndex, pHCDData->pBandwidthReclaimQH);

    /* Create the static QH of the periodic tree */

    for (i = 0; i < 255; ++i)
        {

        /* Create the QH */

        pQH = usbUhcdFormEmptyQH(pHCDData);

        /* Check if memory allocation is successful */

        if (pQH == NULL)
            {
            USB_UHCD_ERR("usbUhcdCreateFrameList - "
                "allocate memory failed for QH \n",
                1, 2, 3, 4, 5, 6);

            usbUhcdDeleteFrameList(pHCDData);

            return (FALSE);
            }

        /* Make the previous pointer as NULL */

        pQH->pPrevQH = NULL;

        /* Make the tree element's QH point to the allocated QH */

        pHCDData->pPeriodicTree[i].pQH = pQH;

        /* Initialise the tree element's BW to 0 */

        pHCDData->pPeriodicTree[i].bandWidth = 0;

        /* If the QH is the tip of the tree, make it point to the control QH */

        if (i == 0)
            {
            USB_UHCD_VDBG("usbUhcdCreateFrameList - QH is the tip of tree\n",
                          1, 2, 3, 4, 5, 6);

            pQH->pNextQH = pHCDData->pControlQH;
            }

        /* If it is not the tip of the tree, update the link element index */

        else
            {
            linkIndex = usbUhcdFindLinkForQh(i);

            pQH->pNextQH = (USB_UHCD_QH *) pHCDData->pPeriodicTree[linkIndex].pQH;
            }

        /* update the link pointers */

        dword0 = 0;

        /* Make the QH as not the last element in the schedule */

        dword0 &= ~(USBUHCD_LINK_PTR_MASK | USBUHCD_LINK_TERMINATE);

        /* Indicate that the element next to the control QH is a bulk QH */

        dword0 |= USBUHCD_LINK_QH |
            USBUHCD_LINK_PTR_FMT(USB_UHCD_DESC_LO32(pQH->pNextQH) >> 4);

        pQH->dWord0Qh = dword0;

        USB_UHCD_VDBG("usbUhcdCreateFrameList - dword0=0x%X, pQH->dWord0Qh=0x%X\n",
                      dword0, pQH->dWord0Qh, 3, 4, 5, 6);

        USB_UHCD_MAKE_QH_LE ( pHCDData->uBusIndex, pQH);
        }/* End of for () */

    /* Form the static isochronous TDs */

    for (i = 0; i < USBUHCD_FRAME_LIST_ENTRIES; ++i)
        {

        /* Create an empty isochronous TD */

        pTD = usbUhcdFormEmptyTD (pHCDData, NULL);

        /* Check if memory allocation is successful */

        if (pTD == NULL)
            {
            USB_UHCD_ERR("usbUhcdCreateFrameList - "
                "allocate memory failed for iTD %d\n",
                i, 2, 3, 4, 5, 6);

            usbUhcdDeleteFrameList(pHCDData);

            return (FALSE);
            }

        /* Initialise the status field */

        pTD->dWord1Td &= ~(USBUHCD_TDCS_STS_MASK);

        /* Indicate that this is an isochronous TD */

        pTD->dWord1Td |= USBUHCD_TDCS_ISOCH;

        /* PID is OUT; if pid is invalid UHC will weep */

        pTD->dWord2Td &= ~(USBUHCD_TDTOK_PID_MASK);

        pTD->dWord2Td |= USBUHCD_TDTOK_PID_FMT(USBUHCD_TDTOK_PID_OUT);

        /* Update the link pointers of the TD */

        linkIndex = usbUhcdFindQlinkToTree(i);

        dword0 = 0;

        /* Make the QH as not the last element in the schedule */

        dword0 &= ~(USBUHCD_LINK_PTR_MASK | USBUHCD_LINK_TERMINATE);

        /* Indicate that the element next to the control QH is a bulk QH */

        dword0 |= USBUHCD_LINK_QH |
                 USBUHCD_LINK_PTR_FMT(
                 USB_UHCD_DESC_LO32(pHCDData->pPeriodicTree[linkIndex].pQH) >> 4);

        pTD->dWord0Td = dword0;

        USB_UHCD_VDBG("usbUhcdCreateFrameList - "
            "dword0=0x%X, pTD->dWord0Td=0x%X \n",
            dword0, pTD->dWord0Td, 3, 4, 5, 6);

        USB_UHCD_MAKE_TD_LE (pHCDData->uBusIndex, pTD);

        /* Update the fields of the usbUhcdTable - Start */

        /* Store the pointer as a periodic table array element */

        pHCDData->pPeriodicTable[i].pTD = pTD;

        /* Initialise the bandwidth reserved */

        pHCDData->pPeriodicTable[i].bandWidth = 0;

        /* Update the fields of the usbUhcdTable - End */

        /* Update the first list element of the Host Controller - Start */

        /* Update the list pointer to the corresponding periodictable element */

        dword0 = 0;

        /* Indicate that the next element in the schedule is a TD */

        /* Indicate that this is not the last element in the schedule */

        dword0 &= ~(USBUHCD_LINK_PTR_MASK |
                    USBUHCD_LINK_TERMINATE |
                    USBUHCD_LINK_QH);

        dword0 |= USBUHCD_LINK_PTR_FMT(USB_UHCD_DESC_LO32(pTD) >> 4);

        /* Convert the frame list into bus endian and save it */

        pHCDData->pFrameList->pfl[i] =
            USB_UHCD_SWAP_DATA(pHCDData->uBusIndex, dword0);

        /* Update the first list element of the Host Controller - End */
        }

    /* Return success from the function */

    return TRUE;
    }/* End of usbUhcdCreateFrameList() */


/*******************************************************************************
*
* usbUhcdDeleteFrameList - delete frame list
*
* The HCD maintains
* - a tree of 256 elements to maintain the bandwidth usage in every frame
* for the interrupt transfers.
* - an array of 1024 static isochronous Transfer Descriptor elements.
* This function deallocates memory for the data structures.
*
* RETURNS: FALSE if the frame list deletion is a failure, TRUE if OK
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOLEAN usbUhcdDeleteFrameList
    (
    pUSB_UHCD_DATA pHCDData
    )
    {
    UINT16 i = 0;

    /* Free the periodic tree */

    if (pHCDData->pPeriodicTree)
        {
        for (i = 0; i < 255; ++i)
            {
            if (pHCDData->pPeriodicTree[i].pQH)
                {
                usbUhcdDestroyQH(pHCDData, pHCDData->pPeriodicTree[i].pQH);

                pHCDData->pPeriodicTree[i].pQH = NULL;
                }
            }

        OS_FREE(pHCDData->pPeriodicTree);

        pHCDData->pPeriodicTree = NULL;
        }

    /* Free the periodic table */

    if (pHCDData->pPeriodicTable)
        {
        for (i = 0; i < USBUHCD_FRAME_LIST_ENTRIES; ++i)
            {
            if (pHCDData->pPeriodicTable[i].pTD)
                {
                usbUhcdDestroyTD(pHCDData, pHCDData->pPeriodicTable[i].pTD);

                pHCDData->pPeriodicTable[i].pTD = NULL;
                }

            }

        OS_FREE(pHCDData->pPeriodicTable);

        pHCDData->pPeriodicTable = NULL;
        }

    /* Free the control QH */

    if (pHCDData->pControlQH)
        {
        usbUhcdDestroyQH(pHCDData, pHCDData->pControlQH);

        pHCDData->pControlQH = NULL;
        }

    /* Free the bulk QH */

    if (pHCDData->pBulkQH)
        {
        usbUhcdDestroyQH(pHCDData, pHCDData->pBulkQH);

        pHCDData->pBulkQH = NULL;
        }

    /* Free the band width QH */

    if (pHCDData->pBandwidthReclaimQH)
        {
        if (pHCDData->pBandwidthReclaimQH->pTD)
            {
            usbUhcdDestroyTD(pHCDData, pHCDData->pBandwidthReclaimQH->pTD);

            pHCDData->pBandwidthReclaimQH->pTD = NULL;
            }

        usbUhcdDestroyQH(pHCDData, pHCDData->pBandwidthReclaimQH);

        pHCDData->pBandwidthReclaimQH = NULL;
        }

    /* Free the memory allocated for frame list */

    if (pHCDData->pFrameList)
        {
        /*
         * Free the memory allocated for the Periodic Frame List.
         *
         * If pHCDData->pFrameList is non-NULL, then we are sure
         * pHCDData->frameListDmaTagId and pHCDData->frameListDmaMapId
         * have already been created, thus no need to check them for NULL.
         */

        (void) vxbDmaBufMemFree(pHCDData->frameListDmaTagId,
                         pHCDData->pFrameList,
                         pHCDData->frameListDmaMapId);

        pHCDData->pFrameList = NULL;

        pHCDData->frameListDmaMapId = NULL;
        }

    /* Return success from the function */

    return TRUE;
    }/* End of usbUhcdDeleteFrameList() */

/*******************************************************************************
*
* usbUhcdDeletePipeContext - delete pipe context
*
* This routine is to unlink the TDs from the pipe and delete the resources
* associated with these TDs;
*
* Note: The caller is responsible for concurrent protection.
*
* RETURNS: TRUE, or FALSE if pipe context deletion is a failure
*
* ERRNO: N/A
*
* \NOMANUAL
*/

BOOLEAN usbUhcdDeletePipeContext
    (
    pUSB_UHCD_DATA      pHCDData,
    pUSB_UHCD_PIPE      pHCDPipe
    )
    {
    /* To hold the indiex of the pHCDData in the array */

    UINT8                       uBusIndex = 0;

    /* Validity check */

    if ((pHCDData == NULL) ||
        (pHCDPipe == NULL) ||
        (pHCDPipe->bMarkedForDeletion != TRUE))
        {
        USB_UHCD_ERR("usbUhcdDeletePipeContext - parameters are invalid \n",
                     1, 2, 3, 4, 5, 6);

        return FALSE;
        }

    /* Get the bus index */

    uBusIndex = pHCDData->uBusIndex;

    /* Stop the HC */

    USB_UHCD_HALT_HC(pHCDData);

    if (pHCDPipe->pQH != NULL)
        {
        /*
         * Tell the HC to do no more transactions for this QH, this stops the
         * HC from processing this endpoint requests
         */

        USB_UHCD_QH_SET_ELEMENT_T_BIT(uBusIndex, pHCDPipe->pQH);

        /* Delay 1 frame of time so that the hardware is not accessing the QH */

        OS_DELAY_MS(1);
        }

    /* Restart the HC */

    USB_UHCD_RESTART_HC(pHCDData);

    /* Un-setup the pipe */

    usbUhcdUnSetupPipe(pHCDData, pHCDPipe);

    /* If the QH is non-NULL, destroy it */

    if (pHCDPipe->pQH != NULL)
        {
        /* Free QH */

        usbUhcdDestroyQH(pHCDData, pHCDPipe->pQH);

        /* Set the pipe QH as NULL */

        pHCDPipe->pQH = NULL;
        }

    /* Destroy the pipe synch event */

    if (pHCDPipe->PipeSynchEventID != NULL)
        {
        OS_DESTROY_EVENT(pHCDPipe->PipeSynchEventID);

        /* Set the pipe synch event as NULL */

        pHCDPipe->PipeSynchEventID = NULL;
        }

    /* Free the HCD pipe block */

    OS_FREE(pHCDPipe);

    return TRUE;
    }

/*******************************************************************************
*
* usbUhcdIsValidPipe - check if the pipe is valid
*
* This routine is to check if the pipe is valid.
*
* RETURNS: TRUE if pipe is valid, or FALSE if the pipe is not valid
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/

BOOLEAN usbUhcdIsValidPipe
    (
    UINT8               uBusIndex,
    USB_UHCD_PIPE *     pHCDPipe
    )
    {
    /* Pointer to the host controller data stucture */

    pUSB_UHCD_DATA      pHCDData = g_pUHCDData[uBusIndex];

    /* To hold the pipe object which is used for scanning the list of pipes */

    USB_UHCD_PIPE *     pScanPipe = NULL;

    /* Check if the parameters are non NULL and  uBusIndex is valid  */

    if ((pHCDData == NULL) ||
        (pHCDPipe == NULL) ||
        (uBusIndex >= USB_MAX_UHCI_COUNT))
        {
        USB_UHCD_WARN("usbUhcdIsValidPipe - parameters are invalid 1\n",
            1, 2, 3, 4, 5, 6);

        return FALSE;
        }

    /* Check if the pipe parameters are in the valid range */

    if ((pHCDPipe->uEndpointAddress >= 16) ||
        (pHCDPipe->uEndpointType >= PIPE_MAX_TYPES))
        {
        USB_UHCD_ERR("usbUhcdIsValidPipe - parameters are invalid 2\n",
            1, 2, 3, 4, 5, 6);

        return FALSE;
        }

    /* Check if this is the default pipe or the pipe for the root hub */

    if ((pHCDPipe == pHCDData->pDefaultPipe) ||
        (pHCDPipe->uAddress == pHCDData->rootHub.uDeviceAddress))
        {
        USB_UHCD_VDBG("usbUhcdIsValidPipe - pipe for the root hub \n",
            1, 2, 3, 4, 5, 6);

        return TRUE;
        }

    /* Exclusively access the active pipe list */

    OS_WAIT_FOR_EVENT(pHCDData->ActivePipeListSynchEventID, OS_WAIT_INFINITE);

    /* Check if the pipe is in the pipe list matained by the UHCD */

    if (pHCDPipe->uEndpointType != USBHST_ISOCHRONOUS_TRANSFER)
        {
        pScanPipe = pHCDData->pNonIsochPipeList;
        }
    else
        {
        pScanPipe = pHCDData->pIsochPipeList;
        }

    while ((pScanPipe != NULL) && (pScanPipe != pHCDPipe))
        {
        pScanPipe = pScanPipe->pNext;
        }

    /* Release the synchronization event */

    OS_RELEASE_EVENT(pHCDData->ActivePipeListSynchEventID);

    if (NULL == pScanPipe)
        {
        USB_UHCD_ERR("usbUhcdIsValidPipe - pipe not found in the pipe list\n",
            1, 2, 3, 4, 5, 6);

        return FALSE;
        }
    else
        {
        return TRUE;
        }
    }


/*******************************************************************************
*
* usbUhcdPipeReturnAll - return the URB for all active request of a pipe
*
* This routine is used to return the URB for all active request of a pipe
* to the user code (class drivers) by calling the associated URB callback.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: N/A
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL VOID usbUhcdPipeReturnAll
    (
    pUSB_UHCD_DATA          pHCDData,
    pUSB_UHCD_PIPE          pHCDPipe
    )
    {
    /* To hold the pointer to the request information */

    pUSB_UHCD_REQUEST_INFO  pRequest;

    /* To hold the pointer to the next request information */

    pUSB_UHCD_REQUEST_INFO  pNextRequest;

    /* To hold the pointer to the tail request information */

    pUSB_UHCD_REQUEST_INFO  pTailRequest;

    /* Pointer to the temporary TD */

    pUSB_UHCD_TD            pTempTD;

    /* Pointer to the head TD */

    pUSB_UHCD_TD            pHeadTD;

    /* Pointer to the URB */

    pUSBHST_URB             pUrb;

    /* Start from the head of the active request list */

    pRequest = pHCDPipe->pRequestQueueHead;

    /* Save the tail request */

    pTailRequest = pHCDPipe->pRequestQueueTail;

    while (pRequest != NULL)
        {
        /* Save the URB of this request info */

        pUrb = pRequest->pUrb;

        /*
         * Save the next active reuqest on the active request
         * list of this pipe so that we can safely move to the
         * next request once this request has been processed.
         */

        pNextRequest = pRequest->pNext;

        /*
         * If there is any real data transfer, we should do
         * vxbDmaBufSync and unload the DMA MAP.
         */

        if (pRequest->uRequestLength != 0)
            {
            /* Unload the Data DMA MAP */

            vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
                pRequest->usrDataDmaMapId);
            }

        /* Control transfer should also deal with the Setup buffer */

        if (pHCDPipe->uEndpointType == USBHST_CONTROL_TRANSFER)
            {
            /* Unload the control Setup DMA MAP */

            vxbDmaBufMapUnload (pHCDPipe->ctrlSetupDmaTagId,
                pRequest->ctrlSetupDmaMapId);
            }

        /* Isochrnous pipe needs to unlink the TDs first */

        else if (pHCDPipe->uEndpointType == USBHST_ISOCHRONOUS_TRANSFER)
            {
            /* Stop the HC */

            USB_UHCD_HALT_HC(pHCDData);

            /* Unlink the isochrnous TDs */

            usbUhcdUnlinkItds (pHCDData, pRequest->pHead, FALSE);

            /* Restart the HC */

            USB_UHCD_RESTART_HC(pHCDData);
            }

        /*
         * Return the request info to the pipe which also
         * release the URB from the request, so that the
         * URB callback can submit the URB again just in
         * the callback and it can find free request info
         * to be used.
         *
         * Note: This has to be done before calling the
         * URB callback.
         */

        /* Store the address of the head in the local */

        pHeadTD = (pUSB_UHCD_TD)(pRequest->pHead);

        /* Release all the TDs associated with this Request */

        while (pHeadTD != NULL)
            {
            /* Store the next pointer temporarily */

            pTempTD = pHeadTD->hNextTd;

            /* Return the TD back to the free list */

            usbUhcdAddToFreeTDList(pHCDData, pHCDPipe, pHeadTD);

            /* When reach the tail TD of the request, break */

            if (pHeadTD == (pUSB_UHCD_TD)pRequest->pTail)
                break;

            /* Go to the next TD */

            pHeadTD = pTempTD;
            }

        /* Release the request info */

        usbUhcdReleaseRequestInfo(pHCDData, pHCDPipe, pRequest);

        /* Call the callback if any */

        if (pUrb && pUrb->pfCallback)
            {
            USB_UHCD_DBG("usbUhcdCancelURB - calling callback %p\n",
                pUrb->pfCallback, 0, 0, 0, 0, 0);

            /* Update the URB status to cancelled */

            pUrb->nStatus = USBHST_TRANSFER_CANCELLED;

            /* Call the callback */

            (pUrb->pfCallback)(pUrb);
            }

        /*
         * If this request is already the tail of the pipe's
         * active request list, then we can break out.
         */

        if (pRequest == pTailRequest)
            break;

        /* Go to the next request */

        pRequest = pNextRequest;
        }
    }

/*******************************************************************************
*
* usbUhcdDeletePipe - delete the pipe
*
* This routine is used to delete the HCD maintained pipe and the QH created
* for the pipe.
*
* RETURNS: USBHST_STATUS for the pipe deletion
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbUhcdDeletePipe
    (
    UINT8       uBusIndex,
    ULONG       uPipeHandle
    )
    {
    /* Pointer to the host controller data stucture */

    pUSB_UHCD_DATA      pHCDData = g_pUHCDData[uBusIndex];

    /* To hold the pipe object to be deleted */

    USB_UHCD_PIPE *     pHCDPipe = NULL;

    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT(
        USB_UHCI_WV_TRANSFER,
        "usbUhcdDeletePipe() starts",
        USB_UHCD_WV_FILTER);

    /*
     * The deletePipe routine can be called in the "BusManager" task when
     * the hub detects a device has been removed; It can also be
     * called in the "TD completion handler" task when the URB fails
     * and class driver issues a "reset pipe" request.
     *
     * In either case, the usbUhcdDeletePipeContext below will call the URB
     * callback, with the status indicating a failue; Then the class driver
     * may again issue a "reset pipe" request, causing a nested deletePipe
     * call. This's why the markedForDeletion is used.
     */

    /* Find the HCD maintained Pipe object to be deleted */

    pHCDPipe = (USB_UHCD_PIPE *)uPipeHandle;

    /* Check if this is a valid pipe to delete */

    if ((usbUhcdIsValidPipe(uBusIndex, pHCDPipe) != TRUE) ||
        (pHCDPipe->bMarkedForDeletion == TRUE))

        {
        USB_UHCD_ERR("usbUhcdDeletePipe - pipe is not valid \n",
                     1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_REQUEST;
        }

    /* Check if the request is for the Root hub and route it */

    if (pHCDPipe->uAddress == pHCDData->rootHub.uDeviceAddress)
        {

        /* To hold the status of the request */

        USBHST_STATUS status = USBHST_FAILURE;

        USB_UHCD_VDBG("usbUhcdDeletePipe - request is for root hub \n",
                      1, 2, 3, 4, 5, 6);

        status = usbUhcdRhDeletePipe(pHCDData, uPipeHandle);

        return status;
        }

    /* Mark the pipe to be deleted */

    pHCDPipe->bMarkedForDeletion = TRUE;

    /* If the pipe is Non Isochronous delete the QH linked with the pipe */

    if (pHCDPipe->uEndpointType != USBHST_ISOCHRONOUS_TRANSFER)
        {
        /* Unlink the QH from the HC's list - Start */

        if (pHCDPipe->pQH != NULL && pHCDPipe->pQH->pPrevQH != NULL)
            {
            /* Indicate that the previous QH is the last QH in the schedule */

            /* pHCDPipe->qh->prevQh->dWord0Qh.t = 1; */

            USB_UHCD_QH_SET_LINK_T_BIT (uBusIndex, pHCDPipe->pQH->pPrevQH);

            /*
             * Update the next link pointer in the previous QH pointer
             * to point to the deleted QH's next link pointer
             */

            pHCDPipe->pQH->pPrevQH->dWord0Qh = pHCDPipe->pQH->dWord0Qh;

            /* Update the HCD maintained next pointers */

            pHCDPipe->pQH->pPrevQH->pNextQH = pHCDPipe->pQH->pNextQH;
            }

        /* Update the previous QH pointers */

        if (pHCDPipe->pQH
            && pHCDPipe->pQH->pNextQH
            && pHCDPipe->pQH->pNextQH->pPrevQH)
            {
            USB_UHCD_VDBG("usbUhcdDeletePipe - update previous QH pointers\n",
                          1, 2, 3, 4, 5, 6);

            pHCDPipe->pQH->pNextQH->pPrevQH = pHCDPipe->pQH->pPrevQH;
            }

        /* Unlink the QH from the HC's list - End */

        /* Wait for some time (at least 1ms) so that its safe to delete */

        OS_DELAY_MS(2);
        }

    /* Call all incompleted URB callbacks */

    usbUhcdPipeReturnAll(pHCDData, pHCDPipe);

    /* Enter critical section to protect the bandwidth */

    OS_WAIT_FOR_EVENT (pHCDData->BandwidthMutexEvent, WAIT_FOREVER);

    /* Release the pipe bandwidth */

    /* If pipe type is interrupt, reclaim BW */

    if (pHCDPipe->uEndpointType == USBHST_INTERRUPT_TRANSFER)
        {
        USB_UHCD_VDBG("usbUhcdDeletePipe - pipe type is interrupt \n",
                      1, 2, 3, 4, 5, 6);

        pHCDData->pPeriodicTree[pHCDPipe->listIndex].bandWidth -=
            (UINT32)pHCDPipe->bandwidth;
        }

    /* If pipe type is isochonous, reclaim BW */

    else if (pHCDPipe->uEndpointType == USBHST_ISOCHRONOUS_TRANSFER)
        {
        /* Count for the loop */

        UINT32  i;

        USB_UHCD_VDBG("usbUhcdDeletePipe - pipe type is isochonous \n",
                      1, 2, 3, 4, 5, 6);

        for (i = 0; i < USBUHCD_FRAME_LIST_ENTRIES; i++)
            {
            pHCDData->pPeriodicTable[i].bandWidth -=
                (UINT32)pHCDPipe->bandwidth;
            }
        }

    /* Leave critical section to protect the bandwidth */

    OS_RELEASE_EVENT (pHCDData->BandwidthMutexEvent);

    /*
     * If the pipe removal is done in the context of URB completion
     * processing task, we should delay the pipe list modification
     * until the URB completion processing is done. Otherwise, we can
     * take the ActivePipeListSynchEventID and remove the pipe.
     */

    if ((TASK_ID)taskIdCurrent == pHCDData->IntHandlerThreadID)
        {
        USB_UHCD_DBG(
            "usbUhcdDeletePipe - in IntHandlerThreadID context\n",
            0, 0, 0, 0, 0, 0);

        USB_UHCD_ADD_TO_DELAYED_PIPE_REMOVAL_LIST(pHCDData, pHCDPipe);
        }
    else
        {
        USB_UHCD_DBG(
            "usbUhcdDeletePipe - out of IntHandlerThreadID context\n",
            0, 0, 0, 0, 0, 0);

        /* Exclusively access the list */

        OS_WAIT_FOR_EVENT(pHCDData->ActivePipeListSynchEventID,
            OS_WAIT_INFINITE);

        /* Remove the pipe from the original active pipe list */

        if (pHCDPipe->uEndpointType != USBHST_ISOCHRONOUS_TRANSFER)
            {
            USB_UHCD_REMOVE_NON_ISOCH_PIPE(pHCDData, pHCDPipe);
            }
        else
            {
            USB_UHCD_REMOVE_ISOCH_PIPE(pHCDData, pHCDPipe);
            }

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDData->ActivePipeListSynchEventID);

        /* Delete the resources associated with the pipe */

        usbUhcdDeletePipeContext(pHCDData, pHCDPipe);
        }

    /* Return success from the function */

    return USBHST_SUCCESS;
    }/* End of usbUhcdDeletePipe() */


/*******************************************************************************
*
* usbUhcdCreatePipe - create the pipe
*
* This routine creates
*  - the HCD maintained pipe
*  - HC maintained QH for control, bulk and interrupt transfers
*  - Updates the bandwidth utilised.
*
* RETURNS: USBHST_STATUS
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbUhcdCreatePipe
    (
    UINT8       uBusIndex,
    UINT8       uDeviceAddress,
    UINT8       uDeviceSpeed,
    UCHAR *     pEndpointDescriptor,
    UINT16      uHighSpeedHubInfo,
    ULONG *     puPipeHandle
    )
    {
    /* Pointer to the host controller data stucture */

    pUSB_UHCD_DATA          pHCDData = g_pUHCDData[uBusIndex];

    /* To hold endpoint number */

    UINT8                   uEndpointNum = 0;

    /* To hold direction */

    UINT8                   uDirection = 0;

    /* To hold endpoint type */

    UINT8                   uEndpointType = 0;

    /* To hold the temporary QH pointer */

    USB_UHCD_QH *           pQH = NULL;

    /* To hold the present QH pointer */

    USB_UHCD_QH *           pPresentQH = NULL;

    /* To hold the HCD maintained current pipe pointer */

    USB_UHCD_PIPE *         pHCDPipe = NULL;

    /* To hold the bandwidth required */

    UINT32                  uBwRequired = 0;

    /* To hold the polling rate calculated */

    UINT8                   uReqPollRate = 0;

    /* To hold the index into the list */

    INT32                   listIndex = 0;

    /* Count for the For loop */

    UINT32                  i = 0;

    pUSBHST_ENDPOINT_DESCRIPTOR pUsbHstEndPointDesc =
        (pUSBHST_ENDPOINT_DESCRIPTOR)pEndpointDescriptor;

    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT(
        USB_UHCI_WV_TRANSFER,
        "usbUhcdCreatePipe() starts",
        USB_UHCD_WV_FILTER);

    /* Endpoint number is lower 4 bits */

    uEndpointNum = (UINT8)((pUsbHstEndPointDesc->bEndpointAddress) & 0x0F);

    /* Direction is the MSB */

    uDirection = (pUsbHstEndPointDesc->bEndpointAddress) >> 7;

    /* Get the endpoint type */

    uEndpointType = (UINT8)((pUsbHstEndPointDesc->bmAttributes &
                    USB_UHCD_ATTR_EPTYPE_MASK));


    /* Check if the request is for the Root hub and route it */

    if (uDeviceAddress == pHCDData->rootHub.uDeviceAddress)
        {
        USB_UHCD_VDBG("usbUhcdCreatePipe - request is for root hub \n",
            1, 2, 3, 4, 5, 6);

        return usbUhcdRhCreatePipe (pHCDData,
                                      uDeviceAddress,
                                      uDeviceSpeed,
                                      pEndpointDescriptor,
                                      puPipeHandle);
        }

    /* Check if the pipe already exists */

    if (usbUhcdFindHcdPipe (uBusIndex,
                            uDeviceAddress,
                            uEndpointNum,
                            uDirection,
                            uEndpointType)
        != NULL)
        {
        USB_UHCD_WARN("usbUhcdCreatePipe - pipe already exists \n",
            1, 2, 3, 4, 5, 6);

        return USBHST_SUCCESS;
        }

    /* Call the function to create the HCD maintained pipe */

    pHCDPipe = usbUhcdFormEmptyPipe();

    if (pHCDPipe == NULL)
        {
        USB_UHCD_ERR(
            "usbUhcdCreatePipe - pHCDPipe not created\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INSUFFICIENT_MEMORY;
        }

    /* Create the event used for synchronization of requests for this pipe */

    pHCDPipe->PipeSynchEventID = OS_CREATE_EVENT(OS_EVENT_SIGNALED);

    /* Check if the event is created successfully */

    if (NULL == pHCDPipe->PipeSynchEventID)
        {
        USB_UHCD_ERR(
            "usbUhcdCreatePipe - PipeSynchEventID not created\n",
            0, 0, 0, 0, 0, 0);

        OS_FREE(pHCDPipe);

        return USBHST_INSUFFICIENT_RESOURCE;
        }

    /* Update the fields of the Pipe - Start */

    pHCDPipe->uAddress = uDeviceAddress;

    pHCDPipe->uEndpointAddress = uEndpointNum;

    /* Copy the pipe uDirection */

    pHCDPipe->uEndpointDir = uDirection;

    /* Copy the pipe type */

    pHCDPipe->uEndpointType = uEndpointType;

    /* Do some default setup for control transfer */

    if (pHCDPipe->uEndpointType == USBHST_CONTROL_TRANSFER)
        {
        if (usbUhcdSetupControlPipe(pHCDData, pHCDPipe) != USBHST_SUCCESS)
            {
            USB_UHCD_ERR("usbUhcdCreatePipe - usbUhcdSetupControlPipe fail\n",
                0, 0, 0, 0, 0, 0);

            /* Destroy the pipe synch event */

            OS_DESTROY_EVENT(pHCDPipe->PipeSynchEventID);

            pHCDPipe->PipeSynchEventID = NULL;

            /*
             * Free the memory allocated for the pipe structure.
             */

            OS_FREE(pHCDPipe);

            return USBHST_FAILURE;
            }
        }

    /*
     * There is no QH which is to be created for an isochronous endpoint
     * Return TRUE from the function
     */

    if (pHCDPipe->uEndpointType == USBHST_ISOCHRONOUS_TRANSFER)
        {
        USB_UHCD_VDBG("usbUhcdCreatePipe - creating an iso pipe, no QH\n",
                      1, 2, 3, 4, 5, 6);

        pQH = NULL;

        }
    else
        {

        /* Call the function to create the QH */

        pQH = usbUhcdFormEmptyQH(pHCDData);

        /* Check if the QH is created successfully */

        if (pQH == NULL)
            {
            USB_UHCD_ERR("usbUhcdCreatePipe - QH creation is unsuccessful\n",
                         1, 2, 3, 4, 5, 6);

            /* Destroy the pipe synch event */

            OS_DESTROY_EVENT(pHCDPipe->PipeSynchEventID);

            pHCDPipe->PipeSynchEventID = NULL;

            /* Free the pipe Stucture */

            OS_FREE(pHCDPipe);

            return USBHST_INSUFFICIENT_MEMORY;
            }

        /* Associate the QH with the PIPE */

        pQH->pPipe = pHCDPipe;
        }

    /* Update the empty QH pointer in the pipe created */

    pHCDPipe->pQH = pQH;

    pHCDPipe->uSpeed = uDeviceSpeed;

    pHCDPipe->uMaximumPacketSize  =
        pUsbHstEndPointDesc->wMaxPacketSize;

    /* Swap to CPU endian */

    pHCDPipe->uMaximumPacketSize =
        OS_UINT16_LE_TO_CPU(pHCDPipe->uMaximumPacketSize);

    /* Update the fields of the Pipe - End */

    /* Claculate the bus time (BW) required */

    uBwRequired = (UINT32)usbUhcdCalculateBusTime(uDeviceSpeed,
                                     pHCDPipe->uEndpointDir,
                                     pHCDPipe->uEndpointType,
                                     pHCDPipe->uMaximumPacketSize);
    /* Enter critical section to protect the bandwidth */

    OS_WAIT_FOR_EVENT (pHCDData->BandwidthMutexEvent, WAIT_FOREVER);

    /* If pipe is control or bulk */

    if ((pHCDPipe->uEndpointType == USBHST_CONTROL_TRANSFER) ||
        (pHCDPipe->uEndpointType == USBHST_BULK_TRANSFER))
        {
        /* Update the bandwidth required for the control/bulk pipe */

        /* pHCDData->usbUhcdCBBw += uBwRequired; */

        /* Update the bandwidth occupied in the current pipe */

        pHCDPipe->bandwidth = (INT32)uBwRequired;

        /* Check if the pipe is a control pipe */

        if (pHCDPipe->uEndpointType == USBHST_CONTROL_TRANSFER)
            {
            pQH->dWord1Qh = USB_UHCD_SWAP_DATA(uBusIndex, pQH->dWord1Qh);

            /* Add to the Control QH */

            usbUhcdLinkQheadBetween(uBusIndex, pQH, pHCDData->pControlQH);
            }

        /* Check if the pipe is a Bulk pipe */

        else
            {
            /* Add to the Bulk QH */

            pQH->dWord1Qh = USB_UHCD_SWAP_DATA(uBusIndex, pQH->dWord1Qh);

            usbUhcdLinkQheadBetween(uBusIndex, pQH, pHCDData->pBulkQH);
            }/* End of else */
        }/* End of if(pHCDPipe->uEndpointType == USBHST_CONTROL_TRANSFER).... */

    /* The following is for the Interrupt pipe */

    else if (pHCDPipe->uEndpointType == USBHST_INTERRUPT_TRANSFER)
        {
        /*
         * ch - ams Determine the poll rate which can be supported by the HCD.
         * The HCD supports a poll rate of 1,2,4,8,16,32,64,128 & 256 ms only.
         */

        uReqPollRate = (UINT8)(1 << usbUhciLog2(pUsbHstEndPointDesc->bInterval));

        USB_UHCD_VDBG("usbUhcdCreatePipe - "
                      "interrupt pipe bInterval=%d, uReqPollRate=%d\n",
                      pUsbHstEndPointDesc->bInterval, uReqPollRate, 3, 4, 5, 6);
        /*
         * Locate suitable position for QH - listIndex will hold the
         * index of the list in which the QH should be linked
         */

        if (usbUhcdFindPosForIntQh ((INT32)uBwRequired,
                                    uReqPollRate,
                                    &listIndex,
                                    pHCDData->pPeriodicTable,
                                    pHCDData->pPeriodicTree) == FALSE)
            {
            USB_UHCD_ERR("usbUhcdCreatePipe - no position for QH \n",
                1, 2, 3, 4, 5, 6);

            /* Destroy the pipe synch event */

            OS_DESTROY_EVENT(pHCDPipe->PipeSynchEventID);

            pHCDPipe->PipeSynchEventID = NULL;

            usbUhcdDestroyQH(pHCDData, pQH);

            OS_FREE(pHCDPipe);

            /* Leave critical section to protect the bandwidth */

            OS_RELEASE_EVENT (pHCDData->BandwidthMutexEvent);

            return (USBHST_FAILURE);
            }

        /* Update the bandwidth required by the pipe */

        pHCDPipe->bandwidth = (INT32)uBwRequired;

        /* Update the list index of the pipe */

        pHCDPipe->listIndex = (UINT32)listIndex;

        /* Add the bandwidth required by the pipe to the list */

        pHCDData->pPeriodicTree[listIndex].bandWidth += uBwRequired;

        /* Get the last created QH in the list */

        pPresentQH = (USB_UHCD_QH *) pHCDData->pPeriodicTree[listIndex].pQH;

        /* Link the newly created QH to the list */

        pQH->dWord1Qh = USB_UHCD_SWAP_DATA(uBusIndex, pQH->dWord1Qh);

        usbUhcdLinkQheadBetween(uBusIndex, pQH, pPresentQH);
        }/* End of else (pHCDPipe->uEndpointType == USBHST_INTERRUPT_TRANSFER) */
    else /* if (pHCDPipe->uEndpointType == USBHST_ISOCHRONOUS_TRANSFER) */
        {
        /* Update the bandwidth required by the pipe */

        pHCDPipe->bandwidth = (INT32)uBwRequired;

        /* Updating the usbUhcdTable Stucture with the allocated bandwidth */

        for (i = 0; i < USBUHCD_FRAME_LIST_ENTRIES; i++)
            {
            pHCDData->pPeriodicTable[i].bandWidth += uBwRequired;
            }
        }

    /* Leave critical section to protect the bandwidth */

    OS_RELEASE_EVENT (pHCDData->BandwidthMutexEvent);

    USB_UHCD_VDBG("usbUhcdCreatePipe - created pipe 0x%X \n",
        pHCDPipe, 2, 3, 4, 5, 6);

    *puPipeHandle = (ULONG)pHCDPipe;

    /*
     * If the pipe creation is called in the context of the URB completion
     * processing, then we should delay the active pipe list modification
     * until the URB completion processing is done in the end of routine
     * usbUhcdProcessTransferCompletion.
     */

    if ((TASK_ID)taskIdCurrent == pHCDData->IntHandlerThreadID)
        {
        /* Add to the delayed pipe addition list */

        USB_UHCD_ADD_TO_DELAYED_PIPE_ADDITION_LIST(pHCDData, pHCDPipe);
        }
    else
        {
        /* Add the pipe into the HCD maintained pipe list */

        if (uEndpointType != USBHST_ISOCHRONOUS_TRANSFER)
            {
            USB_UHCD_ADD_TO_NON_ISOCH_PIPE_LIST(pHCDData, pHCDPipe);
            }
        else
            {
            USB_UHCD_ADD_TO_ISOCH_PIPE_LIST(pHCDData, pHCDPipe);
            }
        }

    /* Return success from the function */

    return USBHST_SUCCESS;
    }/* End of usbUhcdCreatePipe() */

/*******************************************************************************
*
* usbUhcdUnSetupPipe - destroy any allocated transfer resources for a pipe
*
* This routine is used to destroy any allocated transfer resource for a pipe.
* The transfer resources, including request info structures associated DMA
* tag and DMA map are destroyed.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: USBHST_SUCCESS
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbUhcdUnSetupPipe
    (
    pUSB_UHCD_DATA pHCDData,
    pUSB_UHCD_PIPE pHCDPipe
    )
    {
    /* Request information */

    pUSB_UHCD_REQUEST_INFO  pRequest;

    /* Temp request information */

    pUSB_UHCD_REQUEST_INFO  pTempRequest;

    /* Destroy the created request info structures */

    for (pRequest = pHCDPipe->pFreeRequestQueueHead; /* Start from the head */
         pRequest != NULL;  /* Make sure it is not NULL */
         pRequest = pTempRequest) /* Goto the next request when complete */
        {
        /* Get the next request info */

        pTempRequest = pRequest->pNext;

        /* Delete the request resources */

        usbUhcdDeleteRequestInfo(pHCDData, pHCDPipe, pRequest);
        }

    /* Set the head and tal as NULL */

    pHCDPipe->pFreeRequestQueueHead = NULL;

    pHCDPipe->pFreeRequestQueueTail = NULL;

    /* Destroy all the free TDs */

    usbUhcdDestroyAllTDs(pHCDData, pHCDPipe);

    /* Destroy the user Data DMA TAG */

    if (pHCDPipe->usrBuffDmaTagId != NULL)
        {
        USB_UHCD_DBG("usbUhcdUnSetupPipe - free usrBuffDmaTagId\n",
            0, 0, 0, 0, 0, 0);

        (void) vxbDmaBufTagDestroy(pHCDPipe->usrBuffDmaTagId);

        pHCDPipe->usrBuffDmaTagId = NULL;
        }

    /* Destroy the control Setup DMA TAG */

    if (pHCDPipe->ctrlSetupDmaTagId != NULL)
        {
        USB_UHCD_DBG("usbUhcdUnSetupPipe - free ctrlSetupDmaTagId\n",
            0, 0, 0, 0, 0, 0);

        (void) vxbDmaBufTagDestroy(pHCDPipe->ctrlSetupDmaTagId);

        pHCDPipe->ctrlSetupDmaTagId = NULL;
        }

    return USBHST_SUCCESS;
    }

/*******************************************************************************
*
* usbUhcdSetupPipe - allocate transfer resources for a pipe
*
* This routine is used to allocate transfer resources for a pipe.
* The transfer resources, including request info structures and associated DMA
* tag and DMA map are created.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: USBHST_SUCCESS when all resources allocation is done
*          USBHST_INVALID_PARAMETER when any paramter is invalid
*          USBHST_INVALID_REQUEST when there is any active transfer on the pipe
*          USBHST_INSUFFICIENT_MEMORY when any resource can not be allocated
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbUhcdSetupPipe
    (
    pUSB_UHCD_DATA              pHCDData,
    pUSB_UHCD_PIPE              pHCDPipe,
    pUSB_TRANSFER_SETUP_INFO    pSetupInfo
    )
    {
    /* Request information */

    pUSB_UHCD_REQUEST_INFO  pRequest;

    /* Transfer request index */

    UINT32                  uReqIndex;

    /* Check if the parameters are valid */

    if ((pHCDData == NULL) ||
        (pHCDPipe == NULL) ||
        (pSetupInfo == NULL) ||
        (pSetupInfo->uMaxNumReqests == 0)) /* Must have some transfer */
        {
        USB_UHCD_ERR("usbUhcdSetupPipe - invalid parameter\n",
            1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }

    /*
     * The pipe setup must be performed when there is no active transfers
     */

    if (pHCDPipe->pRequestQueueHead != NULL)
        {
        USB_UHCD_ERR("usbUhcdSetupPipe - pRequestQueueHead non NULL\n"
            "There are still active transfers in progress\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_REQUEST;
        }

    /* Remove any previous setting */

    usbUhcdUnSetupPipe(pHCDData, pHCDPipe);

    /* Copy transfer setup info */

    pHCDPipe->uMaxTransferSize = pSetupInfo->uMaxTransferSize;

    pHCDPipe->uMaxNumReqests = pSetupInfo->uMaxNumReqests;

    pHCDPipe->uFlags = pSetupInfo->uFlags;

    /* Create tag for mapping Data buffer */

    pHCDPipe->usrBuffDmaTagId = vxbDmaBufTagCreate (pHCDData->pDev,
        pHCDData->uhciParentTag,        /* parent */
        1,                              /* alignment */
        0,                              /* boundary */
        pHCDData->usrDmaTagLowAddr,     /* lowaddr */
        VXB_SPACE_MAXADDR,              /* highaddr */
        NULL,                           /* filter */
        NULL,                           /* filterarg */
        pHCDPipe->uMaxTransferSize,     /* max size */
        1,                              /* nSegments */
        pHCDPipe->uMaxTransferSize,     /* max seg size */
        pHCDData->usrDmaMapFlags,       /* flags */
        NULL,                           /* lockfunc */
        NULL,                           /* lockarg */
        NULL);                          /* ppDmaTag */

    if (pHCDPipe->usrBuffDmaTagId == NULL)
        {
        USB_UHCD_ERR("usbUhcdSetupPipe - allocate usrBuffDmaTagId fail\n",
            0, 0, 0, 0, 0, 0);

        usbUhcdUnSetupPipe(pHCDData, pHCDPipe);

        return USBHST_INSUFFICIENT_MEMORY;
        }

    /* Control pipe needs special treatment for the Setup */

    if (pHCDPipe->uEndpointType == USBHST_CONTROL_TRANSFER)
        {
        /*
         * Create tag for mapping Setup buffer.
         * We create the TAG with 32 bytes instead of
         * 8 bytes (Setup packet size)
         */

        pHCDPipe->ctrlSetupDmaTagId = vxbDmaBufTagCreate (pHCDData->pDev,
            pHCDData->uhciParentTag,        /* parent */
            1,                              /* alignment */
            0,                              /* boundary */
            pHCDData->usrDmaTagLowAddr,     /* lowaddr */
            VXB_SPACE_MAXADDR,              /* highaddr */
            NULL,                           /* filter */
            NULL,                           /* filterarg */
            32,                             /* max size */
            1,                              /* nSegments */
            32,                             /* max seg size */
            pHCDData->usrDmaMapFlags,       /* flags */
            NULL,                           /* lockfunc */
            NULL,                           /* lockarg */
            NULL);                          /* ppDmaTag */

        if (pHCDPipe->ctrlSetupDmaTagId == NULL)
            {
            USB_UHCD_ERR("usbUhcdSetupPipe - allocate "
                "ctrlSetupDmaTagId fail\n",
                0, 0, 0, 0, 0, 0);

            usbUhcdUnSetupPipe(pHCDData, pHCDPipe);

            return USBHST_INSUFFICIENT_MEMORY;
            }
        }

    /* Create the requested number of request info structures */

    for (uReqIndex = 0; uReqIndex < pHCDPipe->uMaxNumReqests; uReqIndex++)
        {
        /* Create the request info */

        pRequest = usbUhcdCreateRequestInfo(pHCDData, pHCDPipe);

        if (pRequest == NULL)
            {
            USB_UHCD_ERR("usbUhcdSetupPipe - allocate pRequest fail\n",
                0, 0, 0, 0, 0, 0);

            usbUhcdUnSetupPipe(pHCDData, pHCDPipe);

            return USBHST_INSUFFICIENT_MEMORY;
            }
        }

    return USBHST_SUCCESS;
    }

/*******************************************************************************
*
* usbUhcdSetupControlPipe - allocate transfer resources for a control pipe
*
* This routine is used to allocate transfer resources for a cotnrol pipe.
* The transfer resources, including request info structures and associated DMA
* tag and DMA map are created. This is done by calling usbUhcdSetupPipe with
* maxiumn trasnfer request size for control pipe.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: USBHST_SUCCESS when all resources allocation is done
*          USBHST_FAILURE if the pipe specified is not a control pipe
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbUhcdSetupControlPipe
    (
    pUSB_UHCD_DATA pHCDData,
    pUSB_UHCD_PIPE pHCDPipe
    )
    {
    USBHST_STATUS hstSts = USBHST_FAILURE;

    USB_TRANSFER_SETUP_INFO pipeSetupInfo;

    pUSB_TRANSFER_SETUP_INFO pSetupInfo = &pipeSetupInfo;

    /* Setup a default transfer limitations */

    OS_MEMSET(&pipeSetupInfo, 0, sizeof(USB_TRANSFER_SETUP_INFO));

    /*
     * Set up the transfer characteristics such as
     * creating proper DMA TAG and DMA MAPs
     */

    switch (pHCDPipe->uEndpointType)
        {
        case USBHST_CONTROL_TRANSFER:
            {
            /* Control transfer will have a UINT16 limit in the Data phase */

            pSetupInfo->uMaxTransferSize = USB_UHCD_CTRL_MAX_DATA_SIZE;

            /* We allow only 1 control request for one pipe at a time */

            pSetupInfo->uMaxNumReqests = 1;

            /* No special flags */

            pSetupInfo->uFlags = 0;

            hstSts = usbUhcdSetupPipe(pHCDData, pHCDPipe, pSetupInfo);
            }
            break;
         default:
            break;
        }
    return hstSts;
    }

/*******************************************************************************
*
* usbUhcdResetPipe - reset a pipe created already
*
* This routine is used to reset a pipe specific to an endpoint, which cancels
* all the pending requests on the pipe and force the data toggle to return
* DATA0 in the next transfer.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the pipe was reset successfully
*   USBHST_INVALID_PARAMETER - Returned if the parameters are not valid.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbUhcdResetPipe
    (
    pUSB_UHCD_DATA  pHCDData,
    pUSB_UHCD_PIPE  pHCDPipe
    )
    {

    /* Exclusively access the pipe request list */

    OS_WAIT_FOR_EVENT(pHCDPipe->PipeSynchEventID, OS_WAIT_INFINITE);

    /*
     * Only need to reset non-ISOCHRONOUS pipes and just need to reset
     * the uToggle bit, the URB submitt routine will use the new uToggle
     * bit as the Data Toggle bit for new URBs.
     */

    if (pHCDPipe->uEndpointType != USBHST_ISOCHRONOUS_TRANSFER)
        {
        /* Call all incompleted URB callbacks */

        usbUhcdPipeReturnAll(pHCDData, pHCDPipe);

        pHCDPipe->pQH->uToggle = 0;
        }

    /* Release the exclusive access */

    OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

    /* Return the success status */

    return USBHST_SUCCESS;
    }/* End of function usbUhcdResetPipe() */

/*******************************************************************************
*
* usbUhcdPipeControl - control the pipe characteristics
*
* This routine is used to control the pipe characteristics.
*
* <uBusIndex> specifies the host controller bus index.
* <uPipeHandle> holds the pipe handle.
* <pPipeCtrl> is the pointer to the USBHST_PIPE_CONTROL_INFO holding
*             the request details.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS:
*   USBHST_SUCCESS - Returned if the URB is submitted successfully.
*   USBHST_INVALID_PARAMETER - Returned if the parameters are not valid.
*   USBHST_INVALID_REQUEST - Returned if the request is not valid
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbUhcdPipeControl
    (
    UINT8  uBusIndex,                   /* Index of the host controller */
    ULONG  uPipeHandle,                 /* Pipe handle */
    USBHST_PIPE_CONTROL_INFO * pPipeCtrl /* Pointer to the pipe control info */
    )
    {
    /* Pointer to the HCD maintained pipe */

    pUSB_UHCD_PIPE pHCDPipe = NULL;

    /* Pointer to the HCD specific data structure */

    pUSB_UHCD_DATA  pHCDData = NULL;

    /* The control result */

    USBHST_STATUS   hstSts = USBHST_FAILURE;

    /* Check the validity of the parameters */

    if ((USB_MAX_UHCI_COUNT <= uBusIndex) ||
        (0 == uPipeHandle) ||
        (NULL == pPipeCtrl))
        {
        USB_UHCD_ERR("usbUhcdPipeControl - parameters are not valid\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the global data structure */

    pHCDData = g_pUHCDData[uBusIndex];

    /* Return if the global pointer is not valid */

    if (NULL == pHCDData)
        {
        USB_UHCD_ERR("usbUhcdPipeControl - pHCDData is NULL\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /* Extract the USB_UHCD_PIPE data structure */

    pHCDPipe = (pUSB_UHCD_PIPE)uPipeHandle;

    /* Do the pipe control based on the control code selection */

    switch (pPipeCtrl->uControlCode)
        {
        case USBHST_CMD_PIPE_RESET:
            {
            /*
             * Do pipe reset so that the pipe will be in DATA0 state
             * in next transfer
             */

            hstSts = usbUhcdResetPipe(pHCDData, pHCDPipe);
            }

            break;

        case USBHST_CMD_TRANSFER_SETUP:
            {
            pUSB_TRANSFER_SETUP_INFO pSetupInfo;

            /* Root hub doesn't need any real DMA transfer */

            if ((pHCDData->rootHub.pInterruptPipe == pHCDPipe) ||
                (pHCDData->rootHub.pControlPipe == pHCDPipe))
                {
                USB_UHCD_DBG("usbUhcdPipeControl - setup root hub pipe\n",
                    0, 0, 0, 0, 0, 0);

                return USBHST_SUCCESS;
                }

            /* For non-root hub device the setup should really happen */

            pSetupInfo = (pUSB_TRANSFER_SETUP_INFO)
                          pPipeCtrl->uControlParam;

            if (pSetupInfo == NULL)
                {
                USB_UHCD_ERR("usbUhcdPipeControl - "
                    "USBHST_CMD_TRANSFER_SETUP pSetupInfo NULL\n",
                    0, 0, 0, 0, 0, 0);

                return USBHST_INVALID_PARAMETER;
                }

            USB_UHCD_DBG(
                "usbUhcdPipeControl - USBHST_CMD_TRANSFER_SETUP for EP %p:\n"
                "uMaxTransferSize %p uMaxNumReqests %d uFlags %p\n",
                pHCDPipe->uEndpointAddress,
                pSetupInfo->uMaxTransferSize,
                pSetupInfo->uMaxNumReqests,
                pSetupInfo->uFlags, 0, 0);


            /* Exclusively access the pipe request list */

            OS_WAIT_FOR_EVENT(pHCDPipe->PipeSynchEventID, OS_WAIT_INFINITE);

            /*
             * Set up the transfer characteristics such as
             * creating proper DMA TAG and DMA MAPs
             */

            hstSts = usbUhcdSetupPipe(pHCDData, pHCDPipe, pSetupInfo);

            /* Release the exclusive access */

            OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);
            }

            break;

        default :

            break;
        }

    return hstSts;
    }

/*******************************************************************************
*
* usbUhcdQueueTDs - link the new TDs onto the QH transfer queue
*
* This routine is used for common part of the URB submitting process for
* non-isoch URB which requires linking the new TDs onto the QH transfer queue.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: USBHST_SUCCESS
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbUhcdQueueTDs
    (
    pUSB_UHCD_DATA          pHCDData,
    pUSB_UHCD_PIPE          pHCDPipe,
    pUSB_UHCD_REQUEST_INFO  pRequest,
    pUSBHST_URB             pUrb
    )
    {
    /* To hold the HCD bus index */

    UINT8                   uBusIndex;

    /* Pointer to the temp TD */

    USB_UHCD_TD *           pTempTD;

    /* Get the HCD bus index */

    uBusIndex = (UINT8)pHCDData->uBusIndex;

    /* Make the TDs visiable to HC hardware */

    pTempTD = pRequest->pHead;

    while (pTempTD)
        {
        USB_UHCD_MAKE_TD_LE(uBusIndex, pTempTD);

        if (pTempTD == pRequest->pTail)
            break;

        pTempTD = pTempTD->hNextTd;
        }

     /* Link the list of TDs to the qHead - Start */

     /* Stop the HC */

     USB_UHCD_HALT_HC(pHCDData);

     /* There is no TD linked to the QH */

     if (pHCDPipe->pQH->pTD == NULL)
         {
         USB_UHCD_VDBG("usbUhcdQueueTDs - adding TDs of URB %p to QH head "
                       "(type = 0x%X, len = %d) \n",
                       pUrb,
                       pHCDPipe->uEndpointType,
                       pUrb->uTransferLength, 4, 5, 6);

         /* Add the head of the request as the first TD in the QH */

         pHCDPipe->pQH->pTD = pRequest->pHead;
         }

     /* There are TDs already linked to the QH */

     else
         {
         USB_UHCD_VDBG("usbUhcdQueueTDs - adding TDs of URB %p to QH tail "
                       "(type = 0x%X, len = %d) \n",
                       pUrb,
                       pHCDPipe->uEndpointType,
                       pUrb->uTransferLength, 4, 5, 6);

         /* Locate the last TD of the last request linked to the QH */

         pTempTD = pHCDPipe->pRequestQueueTail->pTail;

         /*
          * Update the head of the request to be the
          * next element of the last TD
          */

         pTempTD->hNextTd = pRequest->pHead;

         /* Update the HC TD's link pointer */

         USB_UHCD_TD_LINK_TD_ACTIVE(uBusIndex, pTempTD, pRequest->pHead);
         }/* End of else */

    /* If the QH is not activated yet, let's warm it up */

    if (USB_UHCD_SWAP_DATA(uBusIndex, USBUHCD_LINK_TERMINATE) &
        pHCDPipe->pQH->dWord1Qh)
         {
         USB_UHCD_VDBG("usbUhcdQueueTDs - "
                       "QH on device 0x%X ep 0x%X len 0x%X still "
                       "inactive, active it... \n",
                       pUrb->hDevice,
                       pUrb->uEndPointAddress,
                       pUrb->uTransferLength, 4, 5, 6);

         /* Update the HC QH's element link pointer */

         USB_UHCD_QH_LINK_TD_ELEMENT_ACTIVE(uBusIndex,
                                            pHCDPipe->pQH,
                                            pHCDPipe->pQH->pTD);

         }

    /* Restart the HC */

    USB_UHCD_RESTART_HC(pHCDData);

    return USBHST_SUCCESS;
    }

/*******************************************************************************
*
* usbUhcdSubmitControlURB - submit a new URB to the control pipe
*
* This routine is used to submit a new URB to the control pipe. It is done by
* loading a request info with the proper TDs and then call the common queue
* link routine to put the TDs onto the hardware schedule.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: USBHST_INVALID_PARAMETER if any of the parameter is invalid
*          USBHST_FAILURE if any resouce can not be requested
*          USBHST_SUCCESS if the URB is successfully loaded onto the hardware
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbUhcdSubmitControlURB
    (
    pUSB_UHCD_DATA          pHCDData,
    pUSB_UHCD_PIPE          pHCDPipe,
    pUSB_UHCD_REQUEST_INFO  pRequest,
    pUSBHST_URB             pUrb
    )
    {
    /* To hold the HCD bus index */

    UINT8                   uBusIndex;

    /* To hold the data toggle information */

    UINT8                   uToggle = 0;

    /* To hold the DWORD of TD */

    UINT32                  uDword;

    /* To hold the status of the function call */

    STATUS                  vxbStatus;

    /* Pointer to the setup packet buffer */

    pUSBHST_SETUP_PACKET    pSetup;

    /* Pointer to the TD which forms the head of the request info */

    USB_UHCD_TD *           pDataHead = NULL;

    /* Pointer to the TD which forms the tail of the request info */

    USB_UHCD_TD *           pDataTail = NULL;

    /* Get the HCD bus index */

    uBusIndex = (UINT8)pHCDData->uBusIndex;

    /*
     * Control transfer must have the Setup packet
     * which is kept in pTransferSpecificData
     */

    if (pUrb->pTransferSpecificData == NULL)
        {
        USB_UHCD_ERR(
            "usbUhcdSubmitControlURB - pTransferSpecificData NULL\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /*
     * Control transfer Data phase is limited in a UINT16 size,
     * if greater that that, it should be an invalid request.
     */

    if (pUrb->uTransferLength > USB_UHCD_CTRL_MAX_DATA_SIZE)
        {
        USB_UHCD_ERR(
            "usbUhcdSubmitControlURB - uTransferLength %d is larger than %d\n",
            pUrb->uTransferLength,
            USB_UHCD_CTRL_MAX_DATA_SIZE, 0, 0, 0, 0);

        return USBHST_INVALID_PARAMETER;
        }

    /*
     * If non Isochronous the pUrb->pTransferSpecificData contains
     * the setup packet
     */

    pSetup = (pUSBHST_SETUP_PACKET )pUrb->pTransferSpecificData;

    /* Create TD for SETUP TRANSACTION - Start */

    /* Form an empty TD and make it as the head */

    pRequest->pHead = usbUhcdFormEmptyTD (pHCDData, pHCDPipe);

    /* Check if the TD is created successfully */

    if (pRequest->pHead == NULL)
        {
        USB_UHCD_ERR("usbUhcdSubmitControlURB - "
                     "failed to create TD for setup transfer \n",
                     1, 2, 3, 4, 5, 6);

        return (USBHST_INSUFFICIENT_MEMORY);
        }

    /* Load the DMA MAP for Control Setup */

    vxbStatus = vxbDmaBufMapLoad (pHCDData->pDev,
        pHCDPipe->ctrlSetupDmaTagId,
        pRequest->ctrlSetupDmaMapId,
        pUrb->pTransferSpecificData,
        sizeof(USBHST_SETUP_PACKET),
        VXB_DMABUF_MAPLOAD_CACHEALIGNCHECK);

    if (vxbStatus != OK)
        {
        USB_UHCD_ERR(
            "usbUhcdSubmitControlURB - load ctrlSetupDmaMapId fail\n",
            0, 0, 0, 0, 0, 0);

        /* Return the TD back to the free list */

        usbUhcdAddToFreeTDList(pHCDData, pHCDPipe, pRequest->pHead);

        /* Reset the head TD pointer */

        pRequest->pHead = NULL;

        return USBHST_FAILURE;
        }

    /* <copy to bounce buffer and> DMA cache flush */

    vxbStatus = vxbDmaBufSync(pHCDData->pDev,
        pHCDPipe->ctrlSetupDmaTagId,
        pRequest->ctrlSetupDmaMapId,
        _VXB_DMABUFSYNC_DMA_PREWRITE);

    if (vxbStatus != OK)
        {
        USB_UHCD_ERR("usbUhcdSubmitControlURB - sync ctrlSetupDmaMapId fail\n",
            0, 0, 0, 0, 0, 0);

        /* Unload the control Setup DMA MAP */

        vxbDmaBufMapUnload (pHCDPipe->ctrlSetupDmaTagId,
            pRequest->ctrlSetupDmaMapId);

        /* Return the TD back to the free list */

        usbUhcdAddToFreeTDList(pHCDData, pHCDPipe, pRequest->pHead);

        /* Reset the head TD pointer */

        pRequest->pHead = NULL;

        return USBHST_FAILURE;
        }

    /* Update buffer and Length fields of the TD */

    /* Update the buffer pointer to the DevRequest pointer */

    pRequest->pHead->dWord3Td =
        USB_UHCD_BUS_ADDR_LO32(pRequest->ctrlSetupDmaMapId->fragList[0].frag);

    /*
     * Update the maximum length of the buffer to be 7
     * ie 8 bytes of setup packet is there in the buffer
     */

    uDword = pRequest->pHead->dWord2Td & (~(USBUHCD_TDTOK_MAXLEN_MASK));

    uDword |= USBUHCD_TDTOK_MAXLEN_FMT(7);

    /* Data uToggle is 0 for setup */

    pRequest->pHead->uToggle = 0;

    uDword &= ~(USBUHCD_TDTOK_DATA_TOGGLE);

    /* Indicate that it is a SETUP PID */

    uDword &= ~(USBUHCD_TDTOK_PID_MASK);

    uDword |= USBUHCD_TDTOK_PID_FMT(USBUHCD_TDTOK_PID_SETUP);

    pRequest->pHead->dWord2Td = uDword;

    /* Update the previous TD pointer to be NULL */

    pRequest->pHead->hPrevTd = NULL;

    /* Create TD for SETUP TRANSACTION - End */

    /* Create TD for STATUS TRANSACTION - Start */

    /*
     * The Status TD forms the tail of the control request. So
     * make the TD to be created as the request queue's tail.
     */

    /* Call the function to form an empty TD for the status phase */

    pRequest->pTail = usbUhcdFormEmptyTD (pHCDData, pHCDPipe);

    /* Check if the TD is created successfully */

    if (pRequest->pTail == NULL)
        {
        USB_UHCD_ERR("usbUhcdSubmitControlURB - "
                     "failed to create TD for status transfer \n",
                     1, 2, 3, 4, 5, 6);

        /* Unload the control Setup DMA MAP */

        vxbDmaBufMapUnload (pHCDPipe->ctrlSetupDmaTagId,
            pRequest->ctrlSetupDmaMapId);

        /* Return the TD back to the free list */

        usbUhcdAddToFreeTDList(pHCDData, pHCDPipe, pRequest->pHead);

        /* Reset the head TD pointer */

        pRequest->pHead = NULL;

        return (USBHST_FAILURE);
        }

    /*
     * Update the maximum length of data to be 0. This is done as
     * writing a zero would mean a maximum length of 1
     */

    uDword = pRequest->pTail->dWord2Td & (~(USBUHCD_TDTOK_MAXLEN_MASK));

    uDword |= USBUHCD_TDTOK_MAXLEN_FMT(USBUHCD_TDTOK_MAXLEN_ZERO);

    /* Update the uToggle field to be 1 */

    pRequest->pTail->uToggle = 1;

    uDword |= USBUHCD_TDTOK_DATA_TOGGLE;

    pRequest->pTail->dWord2Td = uDword;

    /*
     * Indicate that an interrupt is
     * to be generated on transfer completion
     */

    uDword = pRequest->pTail->dWord1Td | USBUHCD_TDCS_COMPLETE;

    pRequest->pTail->dWord1Td = uDword;

    /* Update the next pointer to be NULL */

    pRequest->pTail->hNextTd = NULL;

    /*
     * The direction of the status phase would be the opposite of the
     * Data phase.
     * Check if the direction is IN, indicate the
     * direction of the TD to be OUT and vice-versa
     */

    /* Update the PID field from the setup packet */

    uDword = pRequest->pTail->dWord2Td & (~(USBUHCD_TDTOK_PID_MASK));

    if (0 != (pSetup->bmRequestType & 0x80))
        uDword |= USBUHCD_TDTOK_PID_FMT(USBUHCD_TDTOK_PID_OUT);
    else
        uDword |= USBUHCD_TDTOK_PID_FMT(USBUHCD_TDTOK_PID_IN);

    pRequest->pTail->dWord2Td = uDword;

    /* Create TDs for DATA TRANSACTION - Start */

    /* Check if the length of data to be transferred is non-zero */

    if (pUrb->uTransferLength != 0)
        {
        /* Load the DMA MAP for Control Data */

        /* Load the DMA MAP */

        vxbStatus = vxbDmaBufMapLoad (pHCDData->pDev,
            pHCDPipe->usrBuffDmaTagId,
            pRequest->usrDataDmaMapId,
            pUrb->pTransferBuffer,
            pUrb->uTransferLength,
            VXB_DMABUF_MAPLOAD_CACHEALIGNCHECK);

        if (vxbStatus != OK)
            {
            USB_UHCD_ERR("usbUhcdSubmitControlURB - load usrDataDmaMapId fail\n",
                0, 0, 0, 0, 0, 0);

            /* Unload the control Setup DMA MAP */

            vxbDmaBufMapUnload (pHCDPipe->ctrlSetupDmaTagId,
                pRequest->ctrlSetupDmaMapId);

            /* Return the TD back to the free list */

            usbUhcdAddToFreeTDList(pHCDData, pHCDPipe, pRequest->pHead);

            /* Reset the head TD pointer */

            pRequest->pHead = NULL;

            /* Return the TD back to the free list */

            usbUhcdAddToFreeTDList(pHCDData, pHCDPipe, pRequest->pTail);

            /* Reset the tail TD pointer */

            pRequest->pTail = NULL;

            return USBHST_FAILURE;
            }

        /* Perform vxbDmaBufSync operation according to Data direction */

        if (pSetup->bmRequestType & USB_UHCD_DIR_IN)
            {
            /* Save the Data direction */

            pRequest->uDataDir = USB_UHCD_DIR_IN;

            /* DMA cache invalidate before data receive */

            vxbStatus = vxbDmaBufSync(pHCDData->pDev,
                pHCDPipe->usrBuffDmaTagId,
                pRequest->usrDataDmaMapId,
                _VXB_DMABUFSYNC_DMA_PREREAD);
            }
        else
            {
            /* Save the Data direction */

            pRequest->uDataDir = USB_UHCD_DIR_OUT;

            /* <copy to bounce buffer and> DMA cache flush */

            vxbStatus = vxbDmaBufSync(pHCDData->pDev,
                pHCDPipe->usrBuffDmaTagId,
                pRequest->usrDataDmaMapId,
                _VXB_DMABUFSYNC_DMA_PREWRITE);
            }

        if (vxbStatus != OK)
            {
            USB_UHCD_ERR("usbUhcdSubmitControlURB - "
                "vxbDmaBufSync usrDataDmaMapId fail \n",
                0, 0, 0, 0, 0, 0);

            /* Unload the control Setup DMA MAP */

            vxbDmaBufMapUnload (pHCDPipe->ctrlSetupDmaTagId,
                pRequest->ctrlSetupDmaMapId);

            /* Unload the control Data DMA MAP */

            vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
                pRequest->usrDataDmaMapId);

            /* Return the TD back to the free list */

            usbUhcdAddToFreeTDList(pHCDData, pHCDPipe, pRequest->pHead);

            /* Reset the head TD pointer */

            pRequest->pHead = NULL;

            /* Return the TD back to the free list */

            usbUhcdAddToFreeTDList(pHCDData, pHCDPipe, pRequest->pTail);

            /* Reset the tail TD pointer */

            pRequest->pTail = NULL;

            return USBHST_FAILURE;
            }

        /* The uToggle bit is 1 */

        uToggle = 1;

        if (usbUhcdMakeTds(uBusIndex,
                           &pDataHead,
                           &pDataTail,
                           pUrb,
                           &uToggle,
                           pHCDPipe,
                           pRequest) == FALSE)
            {
            USB_UHCD_ERR("usbUhcdSubmitControlURB - "
                         "failed to create TD for control data transfer \n",
                         1, 2, 3, 4, 5, 6);

            /* Unload the control Setup DMA MAP */

            vxbDmaBufMapUnload (pHCDPipe->ctrlSetupDmaTagId,
                pRequest->ctrlSetupDmaMapId);

            /* Unload the control Data DMA MAP */

            vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
                pRequest->usrDataDmaMapId);

            /* Return the TD back to the free list */

            usbUhcdAddToFreeTDList(pHCDData, pHCDPipe, pRequest->pHead);

            /* Reset the head TD pointer */

            pRequest->pHead = NULL;

            /* Return the TD back to the free list */

            usbUhcdAddToFreeTDList(pHCDData, pHCDPipe, pRequest->pTail);

            /* Reset the tail TD pointer */

            pRequest->pTail = NULL;

            return (USBHST_FAILURE);
            }

        /*
         * Update the Previous link pointer of the head of data TDs
         * to be the TD created for the SETUP stage.
         */

        pDataHead->hPrevTd = pRequest->pHead;

        /*
         * Update the next link pointer of the SETUP TD
         * to be the head of the data TDs created
         */

        pRequest->pHead->hNextTd = pDataHead;

        /* Update the HC TD's link pointer */

        uDword = pRequest->pHead->dWord0Td & (~(USBUHCD_LINK_PTR_MASK));

        uDword |= USBUHCD_LINK_PTR_FMT(USB_UHCD_DESC_LO32(pDataHead) >> 4);

        /* Indicate that this is not the last element in the queue */

        uDword &= ~(USBUHCD_LINK_TERMINATE);

        pRequest->pHead->dWord0Td = uDword;

        /*
         * The data stage is followed by the Status Stage.
         * So the tail TD of the data stage forms the TD
         * previous to the Status Stage.
         */

        pRequest->pTail->hPrevTd = pDataTail;

        /*
         * Update the next pointer of the tail of the data request
         * to point to the Status TD
         */

        pDataTail->hNextTd = pRequest->pTail;

        /* Update the HC TD's link pointer */

        uDword = pDataTail->dWord0Td & (~(USBUHCD_LINK_PTR_MASK));

        uDword |= USBUHCD_LINK_PTR_FMT(USB_UHCD_DESC_LO32(pRequest->pTail) >> 4);

        /* Indicate that this is not the last element in the queue */

        uDword &= ~(USBUHCD_LINK_TERMINATE);

        pDataTail->dWord0Td = uDword;
        }

    /* The control transfer has no data satage */

    else
        {
        /*
         * Update the next pointer of the head
         * to be pointing to the tail
         */

        pRequest->pHead->hNextTd = pRequest->pTail;

        /* Update the HC TD's link pointer */

        uDword = pRequest->pHead->dWord0Td & (~(USBUHCD_LINK_PTR_MASK));

        uDword |= USBUHCD_LINK_PTR_FMT(USB_UHCD_DESC_LO32(pRequest->pTail) >> 4);

        /*
         * Indicate that the head pointer is not
         * the last element in the queue.
         */

        uDword &= ~(USBUHCD_LINK_TERMINATE);

        pRequest->pHead->dWord0Td = uDword;

        /*
         * Update the element previous to the tail of the request to
         * be the head of the request as there is no data stage
         */

        pRequest->pTail->hPrevTd = pRequest->pHead;
        }

    /* Create TDs for DATA TRANSACTION - End */

    /* Make the TDs visiable to the host controller hardware */

    usbUhcdQueueTDs(pHCDData, pHCDPipe, pRequest, pUrb);

    return USBHST_SUCCESS;
    }

/*******************************************************************************
*
* usbUhcdSubmitControlURB - submit a new URB to the bulk or interrupt pipe
*
* This routine is used to submit a new URB to the bulk or interrupt pipe.
* It is done by loading a request info with the proper TDs and then
* call the common queue link routine to put the TDs onto the hardware
* schedule.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: USBHST_FAILURE if any resouce can not be requested
*          USBHST_SUCCESS if the URB is successfully loaded onto the hardware
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbUhcdSubmitBulkIntrURB
    (
    pUSB_UHCD_DATA          pHCDData,
    pUSB_UHCD_PIPE          pHCDPipe,
    pUSB_UHCD_REQUEST_INFO  pRequest,
    pUSBHST_URB             pUrb
    )
    {
    /* To hold the HCD bus index */

    UINT8                   uBusIndex;

    /* To hold the DWORD of TD */

    UINT32                  uDword;

    /* To hold the status of the function call */

    STATUS                  vxbStatus;

    /* Get the HCD bus index */

    uBusIndex = (UINT8)pHCDData->uBusIndex;

    /* Load the DMA MAP */

    vxbStatus = vxbDmaBufMapLoad (pHCDData->pDev,
        pHCDPipe->usrBuffDmaTagId,
        pRequest->usrDataDmaMapId,
        pUrb->pTransferBuffer,
        pUrb->uTransferLength,
        VXB_DMABUF_MAPLOAD_CACHEALIGNCHECK);

    if (vxbStatus != OK)
        {
        USB_UHCD_ERR("usbUhcdSubmitBulkIntrURB - load usrDataDmaMapId fail\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_FAILURE;
        }

    /* Perform vxbDmaBufSync operation according to Data direction */

    if (pHCDPipe->uEndpointDir)
        {
        /* Save the Data direction */

        pRequest->uDataDir = USB_UHCD_DIR_IN;

        /* DMA cache invalidate before data receive */

        vxbStatus = vxbDmaBufSync(pHCDData->pDev,
            pHCDPipe->usrBuffDmaTagId,
            pRequest->usrDataDmaMapId,
            _VXB_DMABUFSYNC_DMA_PREREAD);
        }
    else
        {
        /* Save the Data direction */

        pRequest->uDataDir = USB_UHCD_DIR_OUT;

        /* <copy to bounce buffer and> DMA cache flush */

        vxbStatus = vxbDmaBufSync(pHCDData->pDev,
            pHCDPipe->usrBuffDmaTagId,
            pRequest->usrDataDmaMapId,
            _VXB_DMABUFSYNC_DMA_PREWRITE);
        }

    if (vxbStatus != OK)
        {
        USB_UHCD_ERR("usbUhcdSubmitBulkIntrURB - "
            "vxbDmaBufSync usrDataDmaMapId fail \n",
            0, 0, 0, 0, 0, 0);

        /* Unload the control Data DMA MAP */

        vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
            pRequest->usrDataDmaMapId);

        return USBHST_FAILURE;
        }

    /* Call the function to create the TDs required for the transfer */

    if (usbUhcdMakeTds (uBusIndex,
                        &pRequest->pHead,
                        &pRequest->pTail,
                        pUrb,
                        &(pHCDPipe->pQH->uToggle),
                        pHCDPipe,
                        pRequest) == FALSE)
        {
        USB_UHCD_ERR("usbUhcdSubmitBulkIntrURB - failed to create TD for " \
                     "bulk data transfer \n",
                     1, 2, 3, 4, 5, 6);

        /* Unload the Data DMA MAP */

        vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
            pRequest->usrDataDmaMapId);

        return(USBHST_FAILURE);
        }

    /*
     * Generate an interrupt on completion
     * of transfer indicated by the tail element of the request
     */

    uDword = pRequest->pTail->dWord1Td | USBUHCD_TDCS_COMPLETE;

    pRequest->pTail->dWord1Td = uDword;

    /* Make the TDs visiable to the host controller hardware */

    usbUhcdQueueTDs(pHCDData, pHCDPipe, pRequest, pUrb);

    return USBHST_SUCCESS;
    }

/*******************************************************************************
*
* usbUhcdSubmitIsochURB - submit a new URB to the isochronous pipe
*
* This routine is used to submit a new URB to the isochronous pipe.
* It is done by loading a request info with the proper isochronous TDs and
* link these isochronous TDs onto hardware schedule.
*
* WARNING : No parameter validation is done as this routine is used internally
* by the UHCD. Assumption is that valid parameters are passed by the UHCD.
* This is for performance considerations. The calling routine will be
* responsible to make sure the parameters are valid.
*
* RETURNS: USBHST_FAILURE if any resouce can not be requested
*          USBHST_SUCCESS if the URB is successfully loaded onto the hardware
*
* ERRNO: N/A
*
* \NOMANUAL
*/

LOCAL USBHST_STATUS usbUhcdSubmitIsochURB
    (
    pUSB_UHCD_DATA          pHCDData,
    pUSB_UHCD_PIPE          pHCDPipe,
    pUSB_UHCD_REQUEST_INFO  pRequest,
    pUSBHST_URB             pUrb
    )
    {
    /* To hold the HCD bus index */

    UINT8                   uBusIndex;

    /* To hold the status of the function call */

    STATUS                  vxbStatus;

    /* To hold the maximum number of isochronous TDs */

    UINT16                  uMaxNumOfItd = 0;

    /* To hold the starting index of the list */

    INT32                   uStartIndex = 0;

    /* To hold the starting frame of the current isoc request */

    INT32                   uReqStartFrame = 0;

    /* To hold the ending frame of the current isoc request */

    INT32                   uReqEndFrame = 0;

    /* To hold the current frameNumber */

    UINT16                  uFrameNum = 0;

    /* To hold the data toggle information */

    UINT8                   uToggle;

    /* Pointer to the temp TD */

    USB_UHCD_TD *           pTempTD;

    /* Get the HCD bus index */

    uBusIndex = (UINT8)pHCDData->uBusIndex;

    /*
     * Check whether the transfer can be supported based on the available
     * isochronous frame window.
     *
     * NOTE: The following algorithm is used for validating whether the
     *       transfer can be supported within the isochronous frame window.
     *
     *       a) Starting frame number of the new transfer should be greater
     *          than ending frame number for the last queued transfer.
     *
     *       b) If condition (a) is FALSE, the following condition should be
     *          met.
     *
     *          Starting frame number of the new transfer is less than the
     *          ending frame number of the last queued transfer.
     *
     *                                      (AND)
     *
     *          Starting frame number of the new transfer is less than the
     *          current SOF (hardware frame number).
     *
     *       c) If conditions (a) and (b) are not met, the transfer cannot
     *          be supported.
     *
     *       d) Ending frame number of the transfer is greater than the
     *          the current SOF
     *
     *                                      (OR)
     *
     *          Ending frame number of the transfer is less than the current
     *          SOF
     *
     *       e) If condition ((a) AND (d)) or ((b) AND (d2)) are met the transfer
     *          can be supported. If not, the transfer cannot be supported.
     */

    /*
     * For Isoc the pUrb->uNumberOfPackets field contains the number
     * of Isoc Tds to be made
     */

    uMaxNumOfItd = (UINT16)pUrb->uNumberOfPackets;

    /*
     * Only a maximum of 1024 isochronous TDs can be accomocated in the list
     */

    if (USBUHCD_FRAME_LIST_ENTRIES < uMaxNumOfItd)
        {
        USB_UHCD_ERR("usbUhcdSubmitIsochURB - window size exceeded \n",
                     1, 2, 3, 4, 5, 6);

        return (USBHST_FAILURE);
        }

    /*
     * Get the current HC's frame number Use it for queueing
     */

    usbUhcdGetFrameNumber(uBusIndex, &uFrameNum);

    uFrameNum = (UINT16)(uFrameNum  & 0x3FF);

    /* Check if the request is to be serviced AS_SOON_AS_POSSIBLE */

    if (USBHST_START_ISOCHRONOUS_TRANSFER_ASAP ==
        (pUrb->uTransferFlags & USBHST_START_ISOCHRONOUS_TRANSFER_ASAP))
        {
        BOOLEAN rollover = FALSE;

        if (pHCDPipe->uLastStartIndex > pHCDPipe->lastIndexAllocated)
           rollover = TRUE;

        if (!rollover)
            {
            if ((pHCDPipe->uLastStartIndex == 0) &&
                (pHCDPipe->lastIndexAllocated == 0))
                {
                uStartIndex = uFrameNum +
                    USB_UHCD_PERIODIC_LIST_MODIFICATION_MARGIN;
                }

            else if ((uFrameNum >= pHCDPipe->uLastStartIndex) &&
                     (uFrameNum <= pHCDPipe->lastIndexAllocated))
                {
                uStartIndex = (INT32)pHCDPipe->lastIndexAllocated + 1;
                }
            else
                {
                uStartIndex = uFrameNum +
                     USB_UHCD_PERIODIC_LIST_MODIFICATION_MARGIN;
                }
            }
        else
            {

            if (((uFrameNum >= pHCDPipe->uLastStartIndex) &&
                 (uFrameNum <= (USBUHCD_FRAME_LIST_ENTRIES - 1))) ||
                 ((uFrameNum <= pHCDPipe->lastIndexAllocated)))
                {
                uStartIndex = (INT32)pHCDPipe->lastIndexAllocated + 1;
                }
            else
                {
                uStartIndex = uFrameNum +
                      USB_UHCD_PERIODIC_LIST_MODIFICATION_MARGIN;
                }

            }

        /* To handle roll over */

        if (uStartIndex > (USBUHCD_FRAME_LIST_ENTRIES - 1))
            {
            uStartIndex -= USBUHCD_FRAME_LIST_ENTRIES;
            }

        } /* End of if (USBHST_START_ISOCHRONOUS_TRANSFER_ASAP) */
    else /* The request is to be serviced in the specific frame number */
        {
        USB_UHCD_VDBG("usbUhcdSubmitIsochURB - "
                      "request is in specific frame number \n",
                      1, 2, 3, 4, 5, 6);

        /* Update the start Index to the startindex provided by the urb */

        uStartIndex = pUrb->uStartFrame;

        if (uStartIndex > 1023)
            {
            USB_UHCD_WARN("usbUhcdSubmitIsochURB - urb start frame > 1023 \n",
                          1, 2, 3, 4, 5, 6);

            return(USBHST_BAD_START_OF_FRAME);
            }
        }

    /* Load the DMA MAP */

    vxbStatus = vxbDmaBufMapLoad (pHCDData->pDev,
        pHCDPipe->usrBuffDmaTagId,
        pRequest->usrDataDmaMapId,
        pUrb->pTransferBuffer,
        pUrb->uTransferLength,
        VXB_DMABUF_MAPLOAD_CACHEALIGNCHECK);

    if (vxbStatus != OK)
        {
        USB_UHCD_ERR("usbUhcdSubmitIsochURB - load usrDataDmaMapId fail\n",
            0, 0, 0, 0, 0, 0);

        return USBHST_FAILURE;
        }

    /* Perform vxbDmaBufSync operation according to Data direction */

    if (pHCDPipe->uEndpointDir)
        {
        /* Save the Data direction */

        pRequest->uDataDir = USB_UHCD_DIR_IN;

        /* DMA cache invalidate before data receive */

        vxbStatus = vxbDmaBufSync(pHCDData->pDev,
            pHCDPipe->usrBuffDmaTagId,
            pRequest->usrDataDmaMapId,
            _VXB_DMABUFSYNC_DMA_PREREAD);
        }
    else
        {
        /* Save the Data direction */

        pRequest->uDataDir = USB_UHCD_DIR_OUT;

        /* <copy to bounce buffer and> DMA cache flush */

        vxbStatus = vxbDmaBufSync(pHCDData->pDev,
            pHCDPipe->usrBuffDmaTagId,
            pRequest->usrDataDmaMapId,
            _VXB_DMABUFSYNC_DMA_PREWRITE);
        }

    if (vxbStatus != OK)
        {
        USB_UHCD_ERR("usbUhcdSubmitIsochURB - "
            "vxbDmaBufSync usrDataDmaMapId fail \n",
            0, 0, 0, 0, 0, 0);

        /* Unload the control Data DMA MAP */

        vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
            pRequest->usrDataDmaMapId);

        return USBHST_FAILURE;
        }

    /* Current frame starts after the last allocated index */

    uReqStartFrame = uStartIndex;

    /*
     * Add the number of tds to the uReqStartFrame
     * to get the End frame
     */

    uReqEndFrame = uReqStartFrame + uMaxNumOfItd;

    /* To handle rounding off */

    if (uReqEndFrame > (USBUHCD_FRAME_LIST_ENTRIES - 1))
        {
        USB_UHCD_DBG("usbUhcdSubmitIsochURB - uReqEndFrame (%d) > 1023\n",
                      uReqEndFrame, 2, 3, 4, 5, 6);

        uReqEndFrame -= USBUHCD_FRAME_LIST_ENTRIES;
        }

    pHCDPipe->uLastStartIndex = (UINT32)uReqStartFrame;

    /* Update the starting index of the transfer */

    pRequest->startIndex = uStartIndex;

    /* The toggle bit is always 0 for isochronous */

    uToggle = 0;

    /* Call the function to create the TDs needed for the transfer */

    if (usbUhcdMakeIsocTds(uBusIndex,
                           &pRequest->pHead,
                           &pRequest->pTail,
                           pUrb,
                           &uToggle,
                           pHCDPipe,
                           pRequest) == FALSE)
        {
        USB_UHCD_ERR("usbUhcdSubmitIsochURB - make TD failed \n",
                     1, 2, 3, 4, 5, 6);

        /* Unload the control Data DMA MAP */

        vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
            pRequest->usrDataDmaMapId);

        return(USBHST_FAILURE);
        }

    /* Make the TDs visiable to HC hardware */

    for (pTempTD = pRequest->pHead;
         pTempTD != pRequest->pTail->vNextTd;
         pTempTD = pTempTD->vNextTd)
        {
        USB_UHCD_MAKE_TD_LE(uBusIndex, pTempTD);
        }

    /* Update the last allocated Index for the pipe */

    pHCDPipe->lastIndexAllocated = (UINT32)uReqEndFrame;

    /* Stop the HC */

    USB_UHCD_HALT_HC(pHCDData);

    /* Link the TDs to the periodic list */

    usbUhcdLinkItds (uBusIndex, pRequest->pHead, uStartIndex);

    /* Restart the HC */

    USB_UHCD_RESTART_HC(pHCDData);

    return USBHST_SUCCESS;

    }

/*******************************************************************************
*
* usbUhcdSubmitUrb - submit a transfer request to the HCD
*
* This routine is used to submit an URB to the HCD.
*
* RETURNS: USBHST_STATUS for the request submittion
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbUhcdSubmitUrb
    (
    UINT8           uBusIndex,
    ULONG           uPipeHandle,
    USBHST_URB *    pUrb
    )
    {
    /* Pointer to the Host Controller Data stucture */

    pUSB_UHCD_DATA                  pHCDData = g_pUHCDData[uBusIndex];

    /* Pointer to the HCD maintained pipe */

    USB_UHCD_PIPE *                 pHCDPipe;

    /* Pointer to the request info structure */

    USB_UHCD_REQUEST_INFO *         pRequest;

    /* To hold the status of the request */

    USBHST_STATUS                   Status;

    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT(
        USB_UHCI_WV_TRANSFER,
        "usbUhcdSubmitUrb() starts",
        USB_UHCD_WV_FILTER);

    /* Cast to pipe strcuture */

    pHCDPipe = (USB_UHCD_PIPE *)uPipeHandle;

    /*
     * There is possibility that when this submit routine is called,
     * the mutex has already been taken by anothor task, and that task
     * may try to delete the pipe from the pipe list, making the pipe passed
     * in by the parameter invalid;
     *
     * Thus the validity check should be done after the critical section has
     * entered; this is to ensure race conditon is resolved;
     */

    if ((pUrb == NULL) ||
        (usbUhcdIsValidPipe(uBusIndex, pHCDPipe) != TRUE) ||
        (pHCDPipe->bMarkedForDeletion == TRUE))
        {
        USB_UHCD_WARN("usbUhcdSubmitUrb - invalid parameters \n",
                      1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_REQUEST;
        }

    if (((pHCDData->rootHub.uDeviceAddress == 0) &&
         (pHCDPipe == pHCDData->pDefaultPipe)) ||
         (pHCDPipe->uAddress == pHCDData->rootHub.uDeviceAddress))
        {
        if (usbUhcdQueueRhRequest(pHCDData, pUrb, uPipeHandle) == TRUE)
            {
            USB_UHCD_VDBG("usbUhcdSubmitUrb - usbUhcdQueueRhRequest OK\n",
                         1, 2, 3, 4, 5, 6);
            return USBHST_SUCCESS;
            }
        else
            {
            USB_UHCD_ERR("usbUhcdSubmitUrb - usbUhcdQueueRhRequest ERROR\n",
                         1, 2, 3, 4, 5, 6);
            return USBHST_FAILURE;
            }
        }

    USB_UHCD_VDBG("usbUhcdSubmitURB - submit to dev 0x%X, ep 0x%X, len %d\n",
                  pUrb->hDevice,
                  pUrb->uEndPointAddress,
                  pUrb->uTransferLength, 4, 5, 6);

    /* Exclusively access the pipe request list */

    OS_WAIT_FOR_EVENT(pHCDPipe->PipeSynchEventID, OS_WAIT_INFINITE);

    /* Get a free request info structure from the pipe */

    pRequest = usbUhcdReserveRequestInfo(pHCDData, pHCDPipe, pUrb);

    if (pRequest == NULL)
        {
        USB_UHCD_ERR("usbUhcdSubmitURB - no free pRequest found\n",
            0, 0, 0, 0, 0, 0);

        /* Release the synchronization event */

        OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

        return USBHST_INVALID_REQUEST;
        }

    /* Check if it is a control transfer request */

    if (USBHST_CONTROL_TRANSFER == pHCDPipe->uEndpointType)
        {
        Status = usbUhcdSubmitControlURB(pHCDData,
                    pHCDPipe, pRequest, pUrb);
        }
    else if ((USBHST_BULK_TRANSFER == pHCDPipe->uEndpointType) ||
             (USBHST_INTERRUPT_TRANSFER == pHCDPipe->uEndpointType))
        {
        Status = usbUhcdSubmitBulkIntrURB(pHCDData,
                    pHCDPipe, pRequest, pUrb);
        }
    else
        {
        Status = usbUhcdSubmitIsochURB(pHCDData,
                    pHCDPipe, pRequest, pUrb);
        }

    /* Check if there is any failure */

    if (Status != USBHST_SUCCESS)
        {
        USB_UHCD_ERR("usbUhcdSubmitURB - URB submit fail (%d)\n",
            Status, 0, 0, 0, 0, 0);

        /* Return the request info to the pipe */

        usbUhcdReleaseRequestInfo(pHCDData, pHCDPipe, pRequest);
        }

    /* Release the exclusive access */

    OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

    /* Return success from the function */

    return Status;

    }/* End of usbUhcdSubmitUrb() */


/*******************************************************************************
*
* usbUhcdGetFrameNumber - get the current frame number of the Host Controller
*
* This routine gets the current frame number of the Host Controller.
*
* RETURNS: UINT the current frame number
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbUhcdGetFrameNumber
    (
    UINT8 uBusIndex,
    UINT16 * puFrameNumber
    )
    {
    if (puFrameNumber == NULL)
        return USBHST_FAILURE;

    *puFrameNumber = (UINT16)(USB_UHCD_READ_WORD (
             g_pUHCDData[uBusIndex]->pDev, USB_UHCD_FRNUM ) & 0x7FF);

    return(USBHST_SUCCESS);

    }/* End of usbUhcdGetFrameNumber() */


/*******************************************************************************
*
* usbUhcdCancelUrb - cancel a request queued for the pipe
*
* This routine cancels a queued request for the pipe.
*
* RETURNS: USBHST_SUCCESS / USBHST_FAILURE.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbUhcdCancelUrb
    (
    UINT8           uBusIndex,
    ULONG           uPipeHandle,
    USBHST_URB *    pUrb
    )
    {
    /* Pointer to the Host Controller Data stucture */

    pUSB_UHCD_DATA              pHCDData = g_pUHCDData[uBusIndex];

    /* Pointer to the HCD maintained pipe */

    USB_UHCD_PIPE *             pHCDPipe = NULL;

    /* Pointer to the non isochronous request queue structure */

    USB_UHCD_REQUEST_INFO *     pRequest = NULL;

    USB_UHCD_TD *               pTempTD = NULL;

    USB_UHCD_TD *               pTD = NULL;

    USBHST_STATUS               hstSts;

    if ((uBusIndex >= USB_MAX_UHCI_COUNT)|| (uPipeHandle == 0) ||
        (pUrb == NULL))
        {
        USB_UHCD_ERR ("%s: error: invalid parameter: "
                      "uBusIndex = %d, uPipeHandle = %p, pUrb = %p\n",
                      __FUNCTION__, uBusIndex, uPipeHandle, pUrb, 5, 6);
        
        return USBHST_INVALID_PARAMETER;
        }
    
    /* Cast to pipe strcuture */

    pHCDPipe = (USB_UHCD_PIPE *)uPipeHandle;

    /* Exclusively access the pipe request list */

    OS_WAIT_FOR_EVENT(pHCDPipe->PipeSynchEventID, OS_WAIT_INFINITE);

    /* Get the HCD specific information of this URB */

    pRequest = (pUSB_UHCD_REQUEST_INFO)pUrb->pHcdSpecific;

    /* If the request is not found, return an error */

    if (NULL == pRequest)
        {
        USB_UHCD_ERR("usbUhcdCancelUrb - Request is not found\n",
            0, 0, 0, 0, 0, 0);

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

        return USBHST_INVALID_REQUEST;
        }

    /* Check if the request is for the Root hub and route it */

    if (pHCDPipe->uAddress == pHCDData->rootHub.uDeviceAddress)
        {
        USB_UHCD_VDBG("usbUhcdCancelUrb - request is for root hub \n",
                      1, 2, 3, 4, 5, 6);

        hstSts = usbUhcdCancelRhRequest(pHCDData, pUrb, uPipeHandle);

        /* Release the exclusive access */

        OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

        return hstSts;
        }

    /* Check if the request is for a non-isochronous transfer */

    switch (pHCDPipe->uEndpointType)
        {
        case USBHST_CONTROL_TRANSFER:
        case USBHST_BULK_TRANSFER:
        case USBHST_INTERRUPT_TRANSFER:

            USB_UHCD_VDBG("usbUhcdCancelUrb - remove request from endpoint list\n",
                1, 2, 3, 4, 5, 6);

            /* Non-Isoch pipe should have a QH */

            if (pHCDPipe->pQH == NULL)
                {
                USB_UHCD_ERR("usbUhcdCancelUrb - Non-Isoch pipe has no QH!\n",
                    0, 0, 0, 0, 0, 0);

                /* Release the exclusive access */

                OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

                return USBHST_INVALID_PARAMETER;
                }

            /* Stop the HC */

            USB_UHCD_HALT_HC(pHCDData);

            /* Update the Terminate bit of the earlier TD/QH. */

            /* Check if it is the head element of the pipe */

            if (pHCDPipe->pRequestQueueHead == pRequest)
                {
                /*
                 * If it is the only element,
                 * update the head and tail pointers to NULL.
                 */

                if (pHCDPipe->pRequestQueueTail == pRequest)
                    {
                    pHCDPipe->pRequestQueueHead = NULL;
                    pHCDPipe->pRequestQueueTail = NULL;

                    /* Update the head of the TD link to be NULL */

                    pHCDPipe->pQH->pTD = NULL;

                    /* Indicate that there are no elements in the list */

                    USB_UHCD_QH_UNLINK_TD_ELEMENT(uBusIndex,
                        pHCDPipe->pQH);

                    }
                else
                    {
                    /* Update the head of the request */

                    pHCDPipe->pRequestQueueHead = pRequest->pNext;

                    /* Update the head of the TD list for the endpoint */

                    pHCDPipe->pQH->pTD = pRequest->pNext->pHead;

                    /*
                     * Update the HC's next pointer and
                     * indicate that this is a valid element in the list
                     */

                    USB_UHCD_QH_LINK_TD_ELEMENT_ACTIVE(uBusIndex,
                                                       pHCDPipe->pQH,
                                                       pRequest->pNext->pHead);
                    }
                }
            else
                {
                /* This is not the head of the request list for the pipe */

                /* Pointer to the non isochronous request queue structure */

                USB_UHCD_REQUEST_INFO *    pPrevRequest = NULL;

                /* Pointer to the TD */

                USB_UHCD_TD *   pPrevTailTD = NULL;

                /* Pointer to the tail TD of the request being removed */

                USB_UHCD_TD *   pCurrTailTD = NULL;

                /* Search for the request in the list */

                for (pPrevRequest = pHCDPipe->pRequestQueueHead;
                    (NULL != pPrevRequest) &&
                    (pPrevRequest->pNext != pRequest);
                    pPrevRequest = pPrevRequest->pNext);

                /*
                 * If the temporary request pointer is NULL,
                 * it is an error as the request was located
                 * already in the HCD maintained list.
                 */

                if (pPrevRequest == NULL)
                    {
                    USB_UHCD_ERR("usbUhcdCancelUrb - no previous request!\n",
                        0, 0, 0, 0, 0, 0);

                    /* Release the exclusive access */

                    OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

                    return USBHST_INVALID_REQUEST;
                    }

                /* Update the request's next pointers */

                pPrevRequest->pNext = pRequest->pNext;

                /*
                 * Check if the request is the tail
                 * and update the tail pointer
                 */

                if (pRequest == pHCDPipe->pRequestQueueTail)
                    {
                    pHCDPipe->pRequestQueueTail = pPrevRequest;
                    }

                /* Extract the tail of the previous request */

                pPrevTailTD = pPrevRequest->pTail;

                /* Extract the tail of the present request */

                pCurrTailTD = pRequest->pTail;

                /* If the next element is valid, update the next pointer */

                if (NULL != pRequest->pNext)
                    {
                    /*
                     * Update the next pointer of the
                     * previous request's tail TD
                     */

                    pPrevTailTD->hNextTd = pRequest->pNext->pHead;
                    }
                else
                    {
                    /* Update the next pointer of the tail TD as NULL */

                    pPrevTailTD->hNextTd = NULL;
                    }

                /*
                 * Link the previous request last TD's next to
                 * the current request last Td's next
                 */

                pPrevTailTD->dWord0Td = pCurrTailTD->dWord0Td;

                }

            /* Restart the HC */

            USB_UHCD_RESTART_HC(pHCDData);

            break;

        case USBHST_ISOCHRONOUS_TRANSFER:
            {
            /* Stop the HC */

            USB_UHCD_HALT_HC(pHCDData);

            /* Unlink the subsequent TDs */

            usbUhcdUnlinkItds(pHCDData, pRequest->pHead, FALSE);

            /* Restart the HC */

            USB_UHCD_RESTART_HC(pHCDData);

            /* Wait for one frame to delete the TDs */

            OS_DELAY_MS(1);

            /*
             * Check if this is the request for which
             * the index was last allocated
             */

            if (((UINT32)pRequest->startIndex + pUrb->uNumberOfPackets) ==
                  pHCDPipe->lastIndexAllocated)
                {
                /*
                 * If true, update the lastIndexAllocated to the startIndex
                 * of the cancelled request
                 */

                pHCDPipe->lastIndexAllocated = (UINT32)pRequest->startIndex;
                }

            }/* End-if for Isochronous Request */

            break;

        default:

            break;

        }

    /*
     * Free the memory allocated to these TDs
     */

    /* Check for Isoc or non Isoc Req */

    if (pHCDPipe->uEndpointType != USBHST_ISOCHRONOUS_TRANSFER)
        {
        /* Search for the first active TD */

        for (pTempTD = pRequest->pHead;
             pTempTD != pRequest->pTail->hNextTd;
             pTempTD = pTempTD->hNextTd)
            {
            if ((USB_UHCD_SWAP_DATA(uBusIndex, pTempTD->dWord1Td) &
                    USBUHCD_TDCS_STS_ACTIVE) != 0)
                break;
            }

        if ((pTempTD == pRequest->pTail->hNextTd) && (pTempTD == NULL))
            {
            /* All TDs have been serviced */

            USB_UHCD_VDBG("usbUhcdCancelUrb - update toggle\n",
                          1, 2, 3, 4, 5, 6);

            pHCDPipe->pQH->uToggle =
                (UINT8)((pRequest->pTail->uToggle == 0) ? 1 : 0);
            }
        else if (pTempTD != pRequest->pTail->hNextTd)
            {
            /*
             * If toggle value of next request is the same
             * as the current active TD's toggle value,
             * we need not update the toggle
             */

            if (pRequest->pTail->hNextTd != NULL &&
                (pTempTD->uToggle != pRequest->pTail->hNextTd->uToggle))
                {
                UINT8 lastToggle = 0;

                /* Fetch the next data toggle to be used */

                lastToggle = pTempTD->uToggle;

                /* Check if pipe is interrupt or bulk */

                if (pHCDPipe->uEndpointType == USBHST_INTERRUPT_TRANSFER ||
                    pHCDPipe->uEndpointType == USBHST_BULK_TRANSFER)
                    {
                    /* Stop the HC */

                    USB_UHCD_HALT_HC(pHCDData);

                    /* Fix data toggle for other TDs */

                    for (pTD = pRequest->pTail->hNextTd;
                         pTD != NULL;
                         pTD = pTD->hNextTd)
                        {
                        if (lastToggle == 1)
                            {
                            USB_UHCD_TD_SET_TOGGLE_BIT (uBusIndex, pTD);
                            }
                        else
                            {
                            USB_UHCD_TD_CLR_TOGGLE_BIT (uBusIndex, pTD);
                            }

                        pTD->uToggle = lastToggle;

                        lastToggle = (UINT8)((lastToggle == 0) ? 1 : 0);
                        }

                    /* Restart the HC */

                    USB_UHCD_RESTART_HC(pHCDData);

                    }

                /* Update the new toggle value for the request */

                pHCDPipe->pQH->uToggle = lastToggle;
                }
            else
                {
                pHCDPipe->pQH->uToggle = pTempTD->uToggle;
                }
            }
        }

    /*
     * If there is any real data transfer, we should do
     * vxbDmaBufSync and unload the DMA MAP.
     */

    if (pRequest->uRequestLength != 0)
        {
        /* Unload the Data DMA MAP */

        vxbDmaBufMapUnload (pHCDPipe->usrBuffDmaTagId,
            pRequest->usrDataDmaMapId);
        }

    /* Control transfer should also deal with the Setup buffer */

    if (pHCDPipe->uEndpointType == USBHST_CONTROL_TRANSFER)
        {
        /* Unload the control Setup DMA MAP */

        vxbDmaBufMapUnload (pHCDPipe->ctrlSetupDmaTagId,
            pRequest->ctrlSetupDmaMapId);
        }

    /* Store the address of the head in the local */

    pTD = (pUSB_UHCD_TD)(pRequest->pHead);

    /* Release all the TDs associated with this Request */

    while (pTD != NULL)
        {
        /* Store the next pointer temporarily */

        pTempTD = pTD->hNextTd;

        /* Return the TD back to the free list */

        usbUhcdAddToFreeTDList(pHCDData, pHCDPipe, pTD);

        /* When reach the tail TD of the request, break */

        if (pTD == (pUSB_UHCD_TD)pRequest->pTail)
            break;

        /* Go to the next TD */

        pTD = pTempTD;
        }

    /* Add the request info back to the pipe free request list - Start */

    /* Un-associate the URB with the request info */

    pUrb->pHcdSpecific = NULL;

    /* Un-associate the request info with the URB */

    pRequest->pUrb = NULL;

    /* Break from the active list */

    pRequest->pNext = NULL;

    /* Link the request info onto the free request queue */

    if (pHCDPipe->pFreeRequestQueueHead == NULL)
        {
        pHCDPipe->pFreeRequestQueueHead = pRequest;
        }
    else
        {
        pHCDPipe->pFreeRequestQueueTail->pNext = pRequest;
        }

    pHCDPipe->pFreeRequestQueueTail = pRequest;

    /* Add the request info back to the pipe free request list - End */

    /* Release the exclusive access */

    OS_RELEASE_EVENT(pHCDPipe->PipeSynchEventID);

    /*
     * We need to call the callback of the URB
     */

    if (pUrb->pfCallback)
        {
        pUrb->nStatus = USBHST_TRANSFER_CANCELLED ;

        pUrb->pfCallback(pUrb);
        }

    return USBHST_SUCCESS;
    }/* End of usbUhcdCancelUrb() */

/*******************************************************************************
*
* usbUhcdModifyDefaultPipe - modify the default pipe
*
* This routine modifies the default pipe.
*
* RETURNS: USBHST_SUCCESS if the default pipe is modified successfully.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbUhcdModifyDefaultPipe
    (
    UINT8   uBusIndex,
    ULONG   uDefaultPipeHandle,
    UINT8   uDeviceSpeed,
    UINT8   uMaxPacketSize,
    UINT16  uHighSpeedHubInfo
    )
    {

    /*  To hold the pipe object to be Modified */

    USB_UHCD_PIPE * pHCDPipe;

    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT(
        USB_UHCI_WV_TRANSFER,
        "usbUhcdModifyDefaultPipe() starts",
        USB_UHCD_WV_FILTER);

    /* All TDs have been serviced */

    USB_UHCD_VDBG("usbUhcdModifyDefaultPipe - pHCDPipe %p\n",
                  uDefaultPipeHandle, 2, 3, 4, 5, 6);

    pHCDPipe = (USB_UHCD_PIPE * )uDefaultPipeHandle;

    pHCDPipe->uSpeed = uDeviceSpeed;

    pHCDPipe->uMaximumPacketSize = uMaxPacketSize;

    return USBHST_SUCCESS;

    }/* End of usbUhcdModifyDefaultPipe() */


/***************************************************************************
*
* usbUhcdSetBitRate - set the bit rate
*
* This routine sets the bit rate.
*
* RETURNS: USBHST_FAILURE if bit rate set failed.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbUhcdSetBitRate
    (
    UINT8       uBusIndex,
    BOOL        bIncrement,
    PUINT32     puCurrentFrameWidth
    )
    {
    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT(
        USB_UHCI_WV_TRANSFER,
        "usbUhcdSetBitRate() starts",
        USB_UHCD_WV_FILTER);

    /* This is not supported */

    return USBHST_FAILURE;

    }/* End of usbUhcdSetBitRate() */


/*******************************************************************************
*
* usbUhcdIsRequestPending - determine if request is pending.
*
* This routine determines if a request is pending.
*
* RETURNS: USBHST_FAILURE if URB is not pending for pipe.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbUhcdIsRequestPending
    (
    UINT8 uBusIndex,
    ULONG uPipeHandle
    )
    {
    /* Pointer to the Host Controller Data stucture */

    pUSB_UHCD_DATA  pHCDData = g_pUHCDData[uBusIndex];

    /* Pointer to the HCD maintained pipe */

    USB_UHCD_PIPE * pHCDPipe =NULL;

    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT(
        USB_UHCI_WV_TRANSFER,
        "usbUhcdIsRequestPending() starts",
        USB_UHCD_WV_FILTER);

    /* Check Validity of the Parameter */

    if ((pHCDData == NULL) ||
        (uPipeHandle == 0))
        {
        USB_UHCD_ERR("usbUhcdIsRequestPending - invalid parameter \n",
                     1, 2, 3, 4, 5, 6);

        return USBHST_INVALID_PARAMETER;
        }

    pHCDPipe = (USB_UHCD_PIPE *)uPipeHandle;

    /* Check if any request is pending for the endpoint */

    if (NULL == pHCDPipe->pRequestQueueHead)
        {
        return USBHST_FAILURE;
        }

    /* Return success indicating that a request is pending for the pipe */

    return USBHST_SUCCESS;

    }/* End of usbUhcdIsRequestPending() */


/*******************************************************************************
*
* usbUhcdIsBandwidthAvailable - determine if bandwidth is availbale
*
* This routine determines if bandwidth is available.
*
* RETURNS: USBHST_FAILURE if bandwidth is not available.
*
* ERRNO: N/A
*
* \NOMANUAL
*/

USBHST_STATUS usbUhcdIsBandwidthAvailable
    (
    UINT8 uBusIndex,
    UINT8 uDeviceAddress,
    UINT8 uDeviceSpeed,
    UCHAR *pCurrentDescriptor,
    UCHAR *pNewDescriptor
    )
    {
    /* To hold the interface desc */

    pUSBHST_INTERFACE_DESCRIPTOR pNewInterfaceDesc;

    pUSBHST_INTERFACE_DESCRIPTOR pCurrentInterfaceDesc;

    /* To hold the Endpoint Desc */

    pUSBHST_ENDPOINT_DESCRIPTOR pEndpointDesc;

    /* To hold the Configuration Descriptor */

    pUSBHST_CONFIG_DESCRIPTOR pDescriptor =
        (pUSBHST_CONFIG_DESCRIPTOR)pNewDescriptor;

    pUSBHST_CONFIG_DESCRIPTOR  pConfigDesc = NULL;

    /*
     * To hold a local copy of usbUhcdTable -
     * --- Isoc bandwidth used for bandwidth calculations
     */

    USB_UHCD_PERIODIC_TABLE_ENTRY *usbUhcdLocalTable = NULL;

    /*
     * To hold a local copy of usbUhcdTree -
     * --- Interrupt bandwidth used for bandwidth calculations
     */

    USB_UHCD_PERIODIC_TREE_NODE  *usbUhcdLocalTree = NULL;

    /* To store the details of the endpoint in the current desc */

    USB_UHCD_PIPE *pCurrentEndpointPipes[16] = {0};

    /* To hold the length of the  desc */

    UINT8   uConfigDescLength;

    UINT8   uInterfaceDescLength;

    /* To hold the count value */

    UINT8 uCount = 0,uNumberOfEndpoints,uNumberOfInterfaces;

    /* To hold the for loop count */

    UINT16 i ;

    /* To hold the Isoc Bandwidth */

    INT32 uCurrentIsocBw = 0;

    /* To hold the details of the endpoints of the new interface */

    USB_UHCD_NEW_ENDPOINT_DETAILS pNewEndpointDetails[16] ;

    /* To flag to check whether all the interfaces are done */

    BOOLEAN bAllInterfacesNotDone = TRUE;

    /* WindView Instrumentation */

    USB_HCD_LOG_EVENT(
        USB_UHCI_WV_TRANSFER,
        "usbUhcdIsBandwidthAvailable() starts",
        USB_UHCD_WV_FILTER);

    /* Allocate memory for the Local table */

    usbUhcdLocalTable = (USB_UHCD_PERIODIC_TABLE_ENTRY *)
        OS_MALLOC(sizeof(USB_UHCD_PERIODIC_TABLE_ENTRY) *
        USBUHCD_FRAME_LIST_ENTRIES);

    if (usbUhcdLocalTable == NULL)
        {
        USB_UHCD_ERR("usbUhcdIsBandwidthAvailable():"
                     "memory allocate failed for UHCD local table \n",
                     1, 2, 3, 4, 5, 6);

        return USBHST_INSUFFICIENT_MEMORY;
        }

    /* Set the memory to zero */

    OS_MEMSET(usbUhcdLocalTable,
        0,
        sizeof(USB_UHCD_PERIODIC_TABLE_ENTRY) * USBUHCD_FRAME_LIST_ENTRIES);

    /* Copy the actual usbUhcdTable to the local variable */

    OS_MEMCPY(usbUhcdLocalTable,
        g_pUHCDData[uBusIndex]->pPeriodicTable,
        sizeof(USB_UHCD_PERIODIC_TABLE_ENTRY) * USBUHCD_FRAME_LIST_ENTRIES);


    /* Allocate memory for the Local Tree */

    usbUhcdLocalTree = (USB_UHCD_PERIODIC_TREE_NODE *)
        OS_MALLOC(sizeof(USB_UHCD_PERIODIC_TREE_NODE) * 255);

    if (usbUhcdLocalTree == NULL)
        {
        USB_UHCD_ERR("usbUhcdIsBandwidthAvailable():"
                     "memory allocate failed for UHCD local tree \n",
                     1, 2, 3, 4, 5, 6);

        OS_FREE(usbUhcdLocalTable);

        return USBHST_INSUFFICIENT_MEMORY;
        }

    /* Set the memory to zero */

    OS_MEMSET(usbUhcdLocalTree,0,sizeof(USB_UHCD_PERIODIC_TREE_NODE) * 255);

    /* Copy the actual usbUcdLocalTree to the local variable */

    OS_MEMCPY(usbUhcdLocalTree,
              g_pUHCDData[uBusIndex]->pPeriodicTree,
              sizeof(USB_UHCD_PERIODIC_TREE_NODE) * 255);

    /* Check if pNewDescriptor is a configuration descriptor */

    if (pDescriptor->bDescriptorType == USBHST_CONFIG_DESC)
        {
        /*
         * Ignore the pCurrentDescriptor parameter and
         * pNewDescriptor contain a configuration descriptor
         */

        pConfigDesc = pDescriptor;

        /* Store the length of the config desc */

        uConfigDescLength = pConfigDesc->bLength;

        /* Store the number of Interfaces */

        uNumberOfInterfaces = pConfigDesc->bNumInterfaces;

        /* Point to the head Interface Desc */

        pNewInterfaceDesc =  (pUSBHST_INTERFACE_DESCRIPTOR)((UINT8 *)
                (pConfigDesc) + uConfigDescLength);


        /* Find the pipes for the current Interface */

        usbUhcdGetCurrentEndpointDetails(uBusIndex,
                                         uDeviceAddress,
                                         pCurrentEndpointPipes,
                                          NULL,
                                         &uNumberOfEndpoints);

        /* Check if the list of pipes in not NULL */

        if (pCurrentEndpointPipes[0] != NULL)
            {
            /* Initialise count to zero */

            uCount = 0;

            /* Determine the Isoc bandwidth to be deducded */

            while (uCount<uNumberOfEndpoints)
                {
                if (pCurrentEndpointPipes[uCount]->uEndpointType ==
                    USBHST_ISOCHRONOUS_TRANSFER)
                    {
                    /* Add to the current isoc bandwidth */

                    uCurrentIsocBw += pCurrentEndpointPipes[uCount]->bandwidth;
                    }

                /* Increment the count */

                uCount++;
                }/* end while (uCount<uNumberOfEndpoints)*/


            /*
             * Change the Bandwidth in the tree if the
             * uCurrentIsocBw is non zero
             */

            if (uCurrentIsocBw)
                {
                /* Deduct isoc band width from the usbUhcdLocaltable */

                for (i = 0; i < USBUHCD_FRAME_LIST_ENTRIES; i++)
                    {
                    /* Deduct the isoc bandwidth from each frame */

                    usbUhcdLocalTable[i].bandWidth -= (UINT32)uCurrentIsocBw;
                    }

                }

            /* Determine the Interrupt bandwidth to be deducted */

            /* Initialise count to zero */

            uCount = 0;

            /* Determine the interrupt pipes */

            while (uCount<uNumberOfEndpoints)
                {

                if (pCurrentEndpointPipes[uCount]->uEndpointType ==
                    USBHST_INTERRUPT_TRANSFER)
                    {
                    /* Deduct the Interrupt bandwidth from the correct link */

                    usbUhcdLocalTree
                    [(pCurrentEndpointPipes[uCount]->listIndex)].bandWidth -=
                        (UINT32)pCurrentEndpointPipes[uCount]->bandwidth;
                    }

                /* Increment the count */

                uCount++;
                }/* End of while (uCount<uNumberOfEndpoints) */

            }/* End of if (pCurrentEndpointPipes != NULL) */

        while(bAllInterfacesNotDone)
            {
            /* Search for the Interface desc with alternate setting 0 */

            while (uNumberOfInterfaces)
                {

                /* Check if the desc is Interface desc */

                if (pNewInterfaceDesc->bDescriptorType != USBHST_INTERFACE_DESC)
                    {
                    USB_UHCD_VDBG("usbUhcdIsBandwidthAvailable():"
                                  "this is not an interface "
                                  "descriptor, increment and continue \n",
                                  __FILE__, __LINE__, 3, 4, 5, 6);

                    /* Increment by the length and continue */

                    pNewInterfaceDesc =(pUSBHST_INTERFACE_DESCRIPTOR)
                    ( (UINT8 *)(pNewInterfaceDesc) + pNewInterfaceDesc->bLength);

                    continue;
                    }

                /* Store the length of the Interface desc */

                uInterfaceDescLength = pNewInterfaceDesc->bLength;

                /* Check if alternate setting is zero */

                if (pNewInterfaceDesc->bAlternateSetting == 0 )
                    {
                    USB_UHCD_DBG("usbUhcdIsBandwidthAvailable():"
                                 "interface descriptor found \n",
                                 1, 2, 3, 4, 5, 6);

                    /* Exit the while ()*/

                    break;
                    }

                /* Decrease the INterface count */
                /* If the next desc exist point to the next one */

                if (uNumberOfInterfaces)
                    {
                    pNewInterfaceDesc =
                        (pUSBHST_INTERFACE_DESCRIPTOR)((UINT8 *)(pNewInterfaceDesc)
                                + uInterfaceDescLength);
                    }


                uNumberOfInterfaces--;

                /* Check if all the interfaces are done */

                if (uNumberOfInterfaces == 0)
                    {
                    bAllInterfacesNotDone = FALSE;

                    }

                }

            if(bAllInterfacesNotDone == FALSE)
                break;

            /* Having identified the Interface desc */
            /* Check if the desc is an interface desc*/

            if (pNewInterfaceDesc->bDescriptorType == USBHST_INTERFACE_DESC)
                {
                /* Store the number of Endpoints */

                uNumberOfEndpoints = pNewInterfaceDesc->bNumEndpoints;


                if(uNumberOfEndpoints != 0)
                    {

                    /* point to the first endpoint desc */

                    pEndpointDesc = (pUSBHST_ENDPOINT_DESCRIPTOR)((UINT8 *)
                            (pNewInterfaceDesc) + pNewInterfaceDesc->bLength);

                    /* Point to the first endpoint desc */

                    while (pEndpointDesc->bDescriptorType != USBHST_ENDPOINT_DESC)
                        {
                        /* point to the next desc */

                        pEndpointDesc =(pUSBHST_ENDPOINT_DESCRIPTOR)((UINT8 *)
                                (pEndpointDesc) + pEndpointDesc->bLength);
                        }

                    /* Get details for the New endpoints of pNewInterfaceDesc */

                    usbUhcdGetDetailsForEndpoints(uDeviceSpeed,
                                                  &uNumberOfEndpoints,
                                                  &pEndpointDesc,
                                                  pNewEndpointDetails);


                    /*
                     * Calculating bandwidth availabilty for the new interface
                     * Using the Local table and tree structure
                     */

                    /* Initialising the count */

                    uCount = 0;

                    while (uCount < uNumberOfEndpoints)

                        {
                        /* Check if pipe type is Isoc */

                        if (pNewEndpointDetails[uCount].endpointType ==
                            USBHST_ISOCHRONOUS_TRANSFER)
                            {
                            if (usbUhcdCanIsoBeAccomodated(pNewEndpointDetails[uCount].Bw,
                                                           0,
                                                           USBUHCD_FRAME_LIST_ENTRIES,
                                                           usbUhcdLocalTable,
                                                           usbUhcdLocalTree)== FALSE)
                                {
                                USB_UHCD_ERR("usbUhcdIsBandwidthAvailable():"
                                             "bandwidth not available \n",
                                             1, 2, 3, 4, 5, 6);

                                /* Free local table and tree structure */

                                OS_FREE(usbUhcdLocalTable) ;

                                OS_FREE(usbUhcdLocalTree);

                                return USBHST_FAILURE;
                                }
                            else
                                {
                                /* Update the bandwidth allocated in the local tree */

                                for (i = 0; i < USBUHCD_FRAME_LIST_ENTRIES; i++)
                                    {
                                    usbUhcdLocalTable[i].bandWidth +=
                                        (UINT32)pNewEndpointDetails[uCount].Bw;
                                    }
                                }
                            }
                        else if (pNewEndpointDetails[uCount].endpointType ==
                                USBHST_INTERRUPT_TRANSFER)
                            {
                            /* list index for the interrupt */

                            INT32 listIndex;

                            if (usbUhcdFindPosForIntQh(pNewEndpointDetails[uCount].Bw,
                                            pNewEndpointDetails[uCount].pollingInterval,
                                            &listIndex,
                                            usbUhcdLocalTable,
                                            usbUhcdLocalTree) == FALSE)
                                {
                                USB_UHCD_ERR("usbUhcdIsBandwidthAvailable():"
                                             "bandwidth not available \n",
                                             1, 2, 3, 4, 5, 6);

                                /* Free local table and tree structure */

                                OS_FREE(usbUhcdLocalTable) ;

                                OS_FREE(usbUhcdLocalTree);

                                return USBHST_FAILURE;
                                }
                            else
                                {
                                /* Update the bandwidth that was successfully allocated */

                                (usbUhcdLocalTree[listIndex]).bandWidth =
                                                (UINT32)pNewEndpointDetails[uCount].Bw;
                                }
                            }
                        /* Increment the uCount */
                        uCount ++;
                        }

                    /* Move to the next Interface Desc */

                    pNewInterfaceDesc =(pUSBHST_INTERFACE_DESCRIPTOR)((UINT8 *)
                        (pEndpointDesc) + pEndpointDesc->bLength);


                    }  /* end of if(uNumberOfEndpoints != 0)   */
                else
                    {
                    /* Move to the next Interface Desc */

                    pNewInterfaceDesc =(pUSBHST_INTERFACE_DESCRIPTOR)((UINT8 *)
                            (pNewInterfaceDesc) + pNewInterfaceDesc->bLength);

                    }

                /* Decrement the interface count */

                uNumberOfInterfaces --;

                /* Check if all the interfaces are done */

                if (uNumberOfInterfaces == 0)
                    {
                    bAllInterfacesNotDone = FALSE;
                    }
                }
            else
                {
                USB_UHCD_ERR("usbUhcdIsBandwidthAvailable():"
                             "error while parsing the config descriptor \n",
                             1, 2, 3, 4, 5, 6);

                OS_FREE(usbUhcdLocalTable);

                OS_FREE(usbUhcdLocalTree);

                return USBHST_FAILURE;
                }
            }/* end of while(bAllInterfacesNotDone)*/

        }/*  End of Check if pNewDescriptor is a configuration descriptor */

    else if (pDescriptor->bDescriptorType == USBHST_INTERFACE_DESC)
        {
        /*
         * pCurrentDescriptor contains the current interface desc
         * pNewDescriptor contain the new interface desc to be checked
         * for bandwidth availability
         */

        pCurrentInterfaceDesc = (pUSBHST_INTERFACE_DESCRIPTOR)pCurrentDescriptor;


        /* Check if the parameter pCurrentDescriptor also points to a
         * Interface descriptor
         */
        if (pCurrentInterfaceDesc->bDescriptorType != USBHST_INTERFACE_DESC)
            {
            USB_UHCD_ERR("usbUhcdIsBandwidthAvailable():"
                         "the pCurrentDescriptor does not contain "
                         "an INterface descriptor \n",
                         1, 2, 3, 4, 5, 6);

            /* Free local table and tree */

            OS_FREE(usbUhcdLocalTable);

            OS_FREE(usbUhcdLocalTree);

            return USBHST_INVALID_PARAMETER;
            }

        /* Reduce the bandwith used by the current Interfaces desc endpoints */

        /* Store the length of the interface desc */

        uInterfaceDescLength = pCurrentInterfaceDesc->bLength;

        /* Store the number of Endpoints */

        uNumberOfEndpoints = pCurrentInterfaceDesc->bNumEndpoints;

        /* Point to the first endpoint desc */

        pEndpointDesc = (pUSBHST_ENDPOINT_DESCRIPTOR)((UINT8 *)
            (pCurrentInterfaceDesc) + uInterfaceDescLength);

        /* Check if the desc is an Endpoint desc */

        while (pEndpointDesc->bDescriptorType != USBHST_ENDPOINT_DESC)
            {
            /* point to the next desc */

            pEndpointDesc =(pUSBHST_ENDPOINT_DESCRIPTOR)((UINT8 *)(pEndpointDesc) + pEndpointDesc->bLength);
            }

        /* Number of endpoints != 0 */

        if (uNumberOfEndpoints)
            {
            /* Find the pipes for the current Interface */

            usbUhcdGetCurrentEndpointDetails(uBusIndex,
                                             uDeviceAddress,
                                             pCurrentEndpointPipes,
                                             pEndpointDesc,
                                             &uNumberOfEndpoints);

            /* Check if the list of pipes in not NULL */

            if (pCurrentEndpointPipes[0]  != NULL)
                {
                /* Initialise count to zero */

                uCount = 0;

                /* Determine the Isoc bandwidth to be deducded */

                while (uCount<uNumberOfEndpoints)
                    {
                    if (pCurrentEndpointPipes[uCount]->uEndpointType == USBHST_ISOCHRONOUS_TRANSFER)
                        {
                        /* Add to the current isoc bandwidth */

                        uCurrentIsocBw += pCurrentEndpointPipes[uCount]->bandwidth;
                        }

                    /* Increment the count */

                    uCount++;
                    }/* end while (uCount<uNumberOfEndpoints)*/

                /*
                 * Change the Bandwidth in the tree if the
                 * uCurrentIsocBw is non zero
                 */

                if (uCurrentIsocBw)
                    {
                    /* Deduct isoc band width from the usbUhcdLocaltable */

                    for (i= 0; i < USBUHCD_FRAME_LIST_ENTRIES; i++)
                        {
                        /* Deduct the isoc bandwidth from each frame */

                        usbUhcdLocalTable[i].bandWidth -=
                            (UINT32)uCurrentIsocBw;
                        }
                    }

                /* Determine the Interrupt bandwidth to be deducted */

                /* Initialise count to zero */

                uCount = 0;

                /* Determine the interrupt pipes */

                while (uCount<uNumberOfEndpoints)
                    {

                    if (pCurrentEndpointPipes[uCount]->uEndpointType ==
                        USBHST_INTERRUPT_TRANSFER)
                        {
                        /* Deduct the Interrupt bandwidth from the correct link */

                        usbUhcdLocalTree[(pCurrentEndpointPipes[uCount]->listIndex)].bandWidth -=
                        (UINT32)pCurrentEndpointPipes[uCount]->bandwidth;
                        }

                    /* Increment the count */

                    uCount++;
                    }/* End of while (uCount<uNumberOfEndpoints) */

                }/* End of if (pCurrentEndpointPipes != NULL) */

            }/* End of if (uNumberOfEndpoints) */

        /* End of Reduce the bandwith used by the current Interfaces desc endpoints */

        /* Get the bandwith for the New Interface desc */

        /* Store the New interface desc */

        pNewInterfaceDesc = (pUSBHST_INTERFACE_DESCRIPTOR)pNewDescriptor;

        /* Store the length of the interface desc */

        uInterfaceDescLength = pNewInterfaceDesc->bLength;

        /* Determine the Bandwidth for the New Interface setting */

        /* Store the number of Endpoints */

        uNumberOfEndpoints = pNewInterfaceDesc->bNumEndpoints;

        /* Point to the first endpoint desc */
        pEndpointDesc = (pUSBHST_ENDPOINT_DESCRIPTOR)((UINT8 *)
            (pNewInterfaceDesc) + uInterfaceDescLength);

        while (pEndpointDesc->bDescriptorType != USBHST_ENDPOINT_DESC)
            {
            /* point to the next desc */

            pEndpointDesc = (pUSBHST_ENDPOINT_DESCRIPTOR)((UINT8 *)
                (pEndpointDesc) + pEndpointDesc->bLength);
            }

        /* Get details for the New endpoints of pNewInterfaceDesc */

        usbUhcdGetDetailsForEndpoints(uDeviceSpeed,
                                      &uNumberOfEndpoints,
                                      &pEndpointDesc,
                                      pNewEndpointDetails);

        /*
         * Calculating bandwidth availabilty for the new interface
         * Using the Local table and tree structure
         */

        /* Initialising the count */

        uCount = 0;

        while (uCount < uNumberOfEndpoints)
            {
            /* Check if pipe type is Isoc */

            if (pNewEndpointDetails[uCount].endpointType == USBHST_ISOCHRONOUS_TRANSFER)
                {
                if (usbUhcdCanIsoBeAccomodated(pNewEndpointDetails[uCount].Bw,
                                               0,
                                               USBUHCD_FRAME_LIST_ENTRIES,
                                               usbUhcdLocalTable,
                                               usbUhcdLocalTree)== FALSE)
                    {
                    USB_UHCD_ERR("usbUhcdIsBandwidthAvailable():"
                                 "bandwidth not available \n",
                                 1, 2, 3, 4, 5, 6);

                    /* Free local table and tree structure */

                    OS_FREE(usbUhcdLocalTable) ;

                    OS_FREE(usbUhcdLocalTree);

                    return USBHST_FAILURE;
                    }
                else
                    {
                    /* Update the bandwidth allocated in the local tree */

                    for (i = 0; i < USBUHCD_FRAME_LIST_ENTRIES; i++)
                        {
                        usbUhcdLocalTable[i].bandWidth +=
                            (UINT32)pNewEndpointDetails[uCount].Bw;
                        }
                    }
                }
            else
                if (pNewEndpointDetails[uCount].endpointType == USBHST_INTERRUPT_TRANSFER)
                {
                /* list index for the interrupt */

                INT32 listIndex;

                if (usbUhcdFindPosForIntQh(pNewEndpointDetails[uCount].Bw,
                                           pNewEndpointDetails[uCount].pollingInterval,
                                           &listIndex,
                                           usbUhcdLocalTable,
                                           usbUhcdLocalTree) == FALSE)
                    {
                    USB_UHCD_ERR("usbUhcdIsBandwidthAvailable():"
                                 "bandwidth not available \n",
                                 1, 2, 3, 4, 5, 6);

                    /* Free local table and tree structure */

                    OS_FREE(usbUhcdLocalTable) ;

                    OS_FREE(usbUhcdLocalTree);

                    return USBHST_FAILURE;
                    }

                else
                    {
                    /* Update the bandwidth that was successfully allocated */

                    (usbUhcdLocalTree[listIndex]).bandWidth =
                        (UINT32)pNewEndpointDetails[uCount].Bw;
                    }
                }

            /* Increment the uCount */

            uCount ++;
            }

        }
    else
        {
        USB_UHCD_ERR("usbUhcdIsBandwidthAvailable():"
                     "invalid descriptor type \n",
                     1, 2, 3, 4, 5, 6);

        /* Free local table and tree structure */

        OS_FREE(usbUhcdLocalTable) ;

        OS_FREE(usbUhcdLocalTree);

        return USBHST_FAILURE;
        }

    /* If the execution reaches here the bandwidth is available */

    USB_UHCD_DBG("usbUhcdIsBandwidthAvailable():"
                 "bandwidth available for new setting \n",
                 1, 2, 3, 4, 5, 6);

    /* Free local table and tree structure */

    OS_FREE(usbUhcdLocalTable) ;

    OS_FREE(usbUhcdLocalTree);

    return USBHST_SUCCESS;
    }


/***************************************************************************
*
* usbUhcdGetDetailsForEndpoints - Get details for the the new interface
*
* This routine gets details for the the new interface.
*
* RETURNS: N/A
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/
VOID usbUhcdGetDetailsForEndpoints
    (
    UINT8  uDeviceSpeed,
    UINT8 *uNumberOfEndpoints,
    pUSBHST_ENDPOINT_DESCRIPTOR *pEndpointDesc,
    PUSB_UHCD_NEW_ENDPOINT_DETAILS pNewEndpointDetails
    )
    {
    /* Store the Bandwidth calculated */
    UINT32 bwRedq = 0;

    /* Store the pipe type */
    UINT8 uPipeType;

    /* Store the Endpoint direction */
    UINT8 uDirection;

    /* Store the Length of the endpoint desc */
    UINT8 uEndpointDescLength = 0;

    /*To hold the count */
    UINT8 uCount = 0;

    /* To hold the number of Periodic endpoints found */
    UINT8 uPeriodicEndpoints = 0;

    while (uCount < *uNumberOfEndpoints)
        {
        /* Store the length of the Endpoint Desc */
        uEndpointDescLength =  (*pEndpointDesc)->bLength;

        /* Check if the endpoint is periodic */
        if ((((*pEndpointDesc)->bmAttributes & USB_UHCD_ATTR_EPTYPE_MASK)==
             USBHST_ISOCHRONOUS_TRANSFER) ||
            (((*pEndpointDesc)->bmAttributes & USB_UHCD_ATTR_EPTYPE_MASK)==
             USBHST_INTERRUPT_TRANSFER))
            {
            /* Store the direction of the Endpoint */
            uDirection = ((*pEndpointDesc)->bEndpointAddress)>>7;

            /* Store the Pipe Type */
            uPipeType = (UINT8)((*pEndpointDesc)->bmAttributes & USB_UHCD_ATTR_EPTYPE_MASK);

            /* Calculate the bandwith for this Endpoint */
            bwRedq = (UINT32)usbUhcdCalculateBusTime(uDeviceSpeed,
                                             uDirection,
                                             uPipeType,
                                             OS_UINT16_CPU_TO_LE((*pEndpointDesc)->wMaxPacketSize));

            /* Update the  details in pNewEndpointDetails */
            /* Update pipe type */
            pNewEndpointDetails[uCount].endpointType = uPipeType;

            /* update bandwidth */
            pNewEndpointDetails[uCount].Bw = (INT32)bwRedq;

            /*If pipe type Interrupt Update polling interval */
            if (uPipeType == USBHST_INTERRUPT_TRANSFER)
                {
                pNewEndpointDetails[uCount].pollingInterval =
                (UINT8)(1 << usbUhciLog2((*pEndpointDesc)->bInterval));
                }
            uPeriodicEndpoints ++;

            }

        /* Decrement the number of endpoints */
        uCount++;

        /* Check if more Endpoints exist */
        if (uCount < *uNumberOfEndpoints)
            {
            /* point to the next Endpoint */
            (*pEndpointDesc) = (pUSBHST_ENDPOINT_DESCRIPTOR)((UINT8 *)(*pEndpointDesc) + (*pEndpointDesc)->bLength);

            /* check whether pEndpointDesc points to an endpoint desc */
            while ((*pEndpointDesc)->bDescriptorType != USBHST_ENDPOINT_DESC)
                {
                /* point to the next Endpoint */
                (*pEndpointDesc) = (pUSBHST_ENDPOINT_DESCRIPTOR)((UINT8 *)(*pEndpointDesc) + (*pEndpointDesc)->bLength);
                }
            }

        }
    /*Update the number of endpoints to the number of periodic endpoits found */
    *uNumberOfEndpoints = uPeriodicEndpoints;
    }


/***************************************************************************
*
* usbUhcdGetCurrentEndpointDetails - find pipes for current interface
*
* This function returnes a list of pipes for current interface.
*
* RETURNS: N/A
*
* ERRNO:
*   None.
*
* \NOMANUAL
*/

VOID usbUhcdGetCurrentEndpointDetails
    (
    UINT8    uBusIndex,
    UINT8    uDeviceAddress,
    USB_UHCD_PIPE **pListOfCurrentEndpointPipes,
    pUSBHST_ENDPOINT_DESCRIPTOR pCurrentEndpointDesc,
    UINT8 *uNumberOfEndpoint
    )
    {
    /* To hold the list of pipe */
    /* Initialise it to the list of pipes */
    USB_UHCD_PIPE *pListOfPipe =  g_pUHCDData[uBusIndex]->pNonIsochPipeList;

    /* Array count */
    UINT8 i = 0, uCount = 0, uNumberOfPeriodicEndpoints = 0,index = 0;

    /* To hold the Endpoint Address */
    UINT8 uEndpointAddress = 0;

    /* To hold the list of periodic endpoint addresses for the Current interface setting */
    UINT8 uCurrentEndpointAddress[16] = {0};

    /* To hold the flag whether to check whether parsing of Endpoint Desc is required */
    BOOLEAN uParseForEndpointAddress = TRUE;

    /* Depending on whether  pCurrentEndpointDesc is Non NUll or not
     * Decide whether we need to parse and identify the Endpoint address
     */
    if (pCurrentEndpointDesc == NULL)
        {
        /* Set the flag to true */
        uParseForEndpointAddress = FALSE;
        }

    if (uParseForEndpointAddress)
        {

        /* First parse the all the endpoint desc and identify the Endpoint addresses */
        /* Initialize the count */
        uCount = 0;

        while (uCount < *uNumberOfEndpoint)
            {

            /* Check if the endpoint is periodic */
            if (((pCurrentEndpointDesc->bmAttributes & USB_UHCD_ATTR_EPTYPE_MASK)==
                 USBHST_ISOCHRONOUS_TRANSFER) ||
                ((pCurrentEndpointDesc->bmAttributes & USB_UHCD_ATTR_EPTYPE_MASK)==
                 USBHST_INTERRUPT_TRANSFER))
                {
                /* Update the Endpoint address in the array */
                uCurrentEndpointAddress[uNumberOfPeriodicEndpoints] =
                pCurrentEndpointDesc->bEndpointAddress;

                /* Update the uNumberOfPeriodicEndpoints count */
                uNumberOfPeriodicEndpoints++;
                }

            /* Decrement the number of endpoints */
            uCount++;

            /* Check if more Endpoints exist */
            if (uCount < *uNumberOfEndpoint)
                {
                /* point to the next Endpoint */
                pCurrentEndpointDesc = (pUSBHST_ENDPOINT_DESCRIPTOR)((UINT8 *)(pCurrentEndpointDesc) +
                                                                     pCurrentEndpointDesc->bLength);

                /* check whether pEndpointDesc points to an endpoint desc */
                while (pCurrentEndpointDesc->bDescriptorType != USBHST_ENDPOINT_DESC)
                    {
                    /* point to the next Endpoint */
                    pCurrentEndpointDesc = (pUSBHST_ENDPOINT_DESCRIPTOR)((UINT8 *)(pCurrentEndpointDesc) +
                                                                         pCurrentEndpointDesc->bLength);
                    }
                }
            }
        }


    /* Search for the pipe with matching address */

    /* Calculate the current bandwidth */
    while (pListOfPipe != NULL)
        {
        /* IF uParseForEndpointAddress is true we already know whether the periodic
         * endpoint exist or not
         * If they donot exist exit the while loop*/
        if (uParseForEndpointAddress)
            {
            if (uNumberOfPeriodicEndpoints == 0)
                {
                break;
                }
            }
        if ((pListOfPipe->uAddress) == uDeviceAddress)
            {

            /* Check if the endpoint is periodic */
            if ((pListOfPipe->uEndpointType == USBHST_ISOCHRONOUS_TRANSFER ) ||
                (pListOfPipe->uEndpointType == USBHST_INTERRUPT_TRANSFER))
                {

                /* if ParseForEndpintAddress is TRUE additionally also check
                 * the Endpoint address*/
                if (uParseForEndpointAddress)
                    {

                    /* Initialise Endpoitn address to 0 */
                    uEndpointAddress = 0;

                    /* Construct the endpoint address for the pipe */
                    uEndpointAddress =  pListOfPipe->uEndpointAddress;

                    /* Check the direction of the pipe */
                    if (pListOfPipe->uEndpointDir == 1)
                        {
                        /* Set the MSB of uEndpointAddress to 1 */
                        uEndpointAddress = uEndpointAddress & 0xFF;
                        }
                    else if (pListOfPipe->uEndpointDir == 0)
                        {
                        /* Set the MSB of uEndpointAddress to 0 */
                        uEndpointAddress = (UINT8)(uEndpointAddress & 0x7F);
                        }

                    /* Check if the Endpoint address mathches with the Endpoint address of the
                       Current interface */
                    for (index = 0; index < uNumberOfPeriodicEndpoints; index++)
                        {
                        if (uEndpointAddress == uCurrentEndpointAddress[index])
                            {
                            /* Store the pipe handle */
                            pListOfCurrentEndpointPipes[i] = pListOfPipe;
                            /* Increment the array count */
                            i++;
                            }
                        }
                    }
                else
                /* if ParseForEndpointAddress = FALSE*/
                    {
                    /* Store the pipe handle */
                    pListOfCurrentEndpointPipes[i] = pListOfPipe;
                    /* Increment the array count */
                    i++;
                    }
                }
            }
        pListOfPipe = pListOfPipe->pNext;
        }
    /* If the no pipes with matching address found */
    if (i == 0)
        {
        /* Set the list to NULL */
        pListOfCurrentEndpointPipes[i] = NULL;
        }
    /* Return the number of endpoint found*/
    *uNumberOfEndpoint = i;

    }/* End of usbUhcdGetCurrentEndpointDetails()*/


/********************* End of File UHCD_ScheduleQueue.c **********************/



