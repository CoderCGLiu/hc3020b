/* usbXhcdDebug.c - USB XHCI Driver Debug Show Routines */

/*
 * Copyright (c) 2011-2014 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify, or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01g,04Dec14,wyy  Clean compiler warning
01f,03may13,wyy  Remove compiler warning (WIND00356717)
01e,17oct12,w_x  Fix compiler warnings (WIND00370525)
01d,17oct12,w_x  Support for Warm Reset (WIND00374209)
01c,25sep12,j_x  Move usbXhcdDebug to usrUsbDebug.c (WIND00378698)
01b,20sep12,w_x  Use separate default pipe for USB2 and USB3 bus (WIND00377413)
01a,16may11,w_x  written
*/

#include "usbXhcdInterfaces.h"

/* globals */

char * gUsbXhcdSpeedStr[] = {"Undefined Speed",
                             "Full Speed",
                             "Low Speed",
                             "High Speed",
                             "Super Speed"};
char * gUsbXhcdLinkStateStr[] = {"U0 State",
                                 "U1 State",
                                 "U2 State",
                                 "U3 State (Device Suspended)",
                                 "Disabled State",
                                 "RxDetect State",
                                 "Inactive State",
                                 "Polling State",
                                 "Recovery State",
                                 "Hot Reset State",
                                 "Compliance Mode State",
                                 "LTest Modef State",
                                 "Reserved12",
                                 "Reserved13",
                                 "Reserved14",
                                 "Resume State"
                                 };
char * gUsbXhcdIndicatorStr[] = {"Port indicators are off",
                                 "Amber",
                                 "Green",
                                 "Undefined"
                                 };

char * gUsbXhcdEpStateStr[] = {"Disabled",
                               "Running",
                               "Halted",
                               "Stopped",
                               "Error",
                               "Reserved5",
                               "Reserved6",
                               "Reserved7"
                               };

char * gUsbXhcdEpTypeStr[] = {"Not Valid",
                              "Isoch Out",
                              "Bulk Out",
                              "Interrupt Out",
                              "Control Bidirectional",
                              "Isoch In",
                              "Bulk In",
                              "Interrupt In"
                              };

char * gUsbXhcdSlotStateStr[] = {"Disabled/Enabled",
                                 "Default",
                                 "Addressed",
                                 "Configured"};

/*******************************************************************************
*
* usbXhcdCmdTest - test xHCI command ring mechanism by an issue no-op command
*
* This routine is to test xHCI command ring mechanism by an issue no-op command.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

VOID usbXhcdCmdTest
    (
    UINT32 uIndex,
    UINT32 uIntr
    )
    {
    pUSB_XHCD_DATA pHCDData = NULL;

    if (uIndex > USB_MAX_HOSTS_PER_HCD)
        {
        printf("usbXhcdRegShow - Invalid uIndex %d\n",
            uIndex);

        return;
        }

    /* Get the xHCI instance at the specific index */

    pHCDData = gpXHCDData[uIndex];

    if (pHCDData == NULL)
        {
        printf("usbXhcdRegShow - No xHCI at uIndex %d\n",
            uIndex);

        return;
        }

    /* Setup the interrupters for the enabled event rings */

    for (uIndex = 0; uIndex < pHCDData->uNumEvtRings; uIndex++)
        {
        UINT32 uReg32;

        /* Read the IMAN register */

        uReg32 = USB_XHCD_READ_RT_REG32(pHCDData, USB_XHCI_IMAN(uIndex));

        printf("usbXhcdInterruptSetup - Current IMAN[%d] 0x%08x ==> 0x%08x\n",
            uIndex,
            uReg32,
            (uReg32 | USB_XHCI_IMAN_IE));

        uReg32 |= USB_XHCI_IMAN_IP;
        uReg32 |= USB_XHCI_IMAN_IE;

        /* Clear and pending interrupts and enable interrupts */

        USB_XHCD_WRITE_RT_REG32(pHCDData,
            USB_XHCI_IMAN(uIndex), uReg32);

        /*
         * Read the IMAN register again to guarantee that the write has been
         * flushed from posted buffers.
         */

        uReg32 = USB_XHCD_READ_RT_REG32(pHCDData, USB_XHCI_IMAN(uIndex));

        printf("usbXhcdInterruptSetup - NEW IMAN[%d] 0x%08x\n",
            uIndex,
            uReg32);
        }

    (void) usbXhcdQueueNoOpCmd(pHCDData, uIntr);
    }

/*******************************************************************************
*
* usbXhcdPortResetTest - test xHCI port reset
*
* This routine is to test xHCI port reset.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

VOID usbXhcdPortResetTest
    (
    UINT32 uIndex,
    UINT32 uPortId
    )
    {
    pUSB_XHCD_DATA  pHCDData = NULL;
    UINT32          uPortIndex;

    if (uIndex > USB_MAX_HOSTS_PER_HCD)
        {
        printf("usbXhcdRegShow - Invalid uIndex %d\n",
            uIndex);

        return;
        }

    /* Get the xHCI instance at the specific index */

    pHCDData = gpXHCDData[uIndex];

    if (pHCDData == NULL)
        {
        printf("usbXhcdRegShow - No xHCI at uIndex %d\n",
            uIndex);

        return;
        }

    if ((uPortId == 0) || (uPortId > pHCDData->uMaxPorts))
        {
        /* Pointer to Port info */

        pUSB_XHCD_ROOT_PORT_INFO pPortInfo;

        /* Pointer to Root Hub Data */

        pUSB_XHCD_ROOT_HUB_DATA  pRootHub;

        printf("Please specify correct Port ID (from 1 up to %d)\n",
            pHCDData->uMaxPorts);

        for (uPortIndex = 0; uPortIndex < pHCDData->uMaxPorts; uPortIndex++)
            {
            pPortInfo = &pHCDData->pRootPorts[uPortIndex];
            pRootHub = pPortInfo->pRootHub;

            printf("Port Index %d Port ID %d is USB %x.%x\n",
                uPortIndex,
                pPortInfo->uPortNumber,
                pRootHub->uRevMajor,
                pRootHub->uRevMinor);
            }

        return;
        }

    usbXhcdPortReset(pHCDData, uPortId - 1, TRUE);
    }

/*******************************************************************************
*
* usbXhcdPortPowerTest - test xHCI port power
*
* This routine is to test xHCI port power.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

VOID usbXhcdPortPowerTest
    (
    UINT32 uIndex,
    UINT32 uPortId,
    UINT8  uOnOff
    )
    {
    pUSB_XHCD_DATA  pHCDData = NULL;
    UINT32          uPortIndex;

    if (uIndex > USB_MAX_HOSTS_PER_HCD)
        {
        printf("usbXhcdRegShow - Invalid uIndex %d\n",
            uIndex);

        return;
        }

    /* Get the xHCI instance at the specific index */

    pHCDData = gpXHCDData[uIndex];

    if (pHCDData == NULL)
        {
        printf("usbXhcdRegShow - No xHCI at uIndex %d\n",
            uIndex);

        return;
        }

    if ((uPortId == 0) || (uPortId > pHCDData->uMaxPorts))
        {
        /* Pointer to Port info */

        pUSB_XHCD_ROOT_PORT_INFO pPortInfo;

        /* Pointer to Root Hub Data */

        pUSB_XHCD_ROOT_HUB_DATA  pRootHub;

        printf("Please specify correct Port ID (from 1 up to %d)\n",
            pHCDData->uMaxPorts);

        for (uPortIndex = 0; uPortIndex < pHCDData->uMaxPorts; uPortIndex++)
            {
            pPortInfo = &pHCDData->pRootPorts[uPortIndex];
            pRootHub = pPortInfo->pRootHub;

            printf("Port Index %d Port ID %d is USB %x.%x\n",
                uPortIndex,
                pPortInfo->uPortNumber,
                pRootHub->uRevMajor,
                pRootHub->uRevMinor);
            }

        return;
        }

    usbXhcdPortPower(pHCDData, (uPortId - 1), uOnOff);
    }

/*******************************************************************************
*
* usbXhcdHostCtlrResetTest - test xHCI HCRST
*
* This routine is to test xHCI HCRST.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

VOID usbXhcdHostCtlrResetTest
    (
    UINT32 uIndex
    )
    {
    pUSB_XHCD_DATA  pHCDData = NULL;
    UINT16          uPcieDevCtrl = 0;
    UINT32          uPcieDevCaps = 0;

    if (uIndex > USB_MAX_HOSTS_PER_HCD)
        {
        printf("usbXhcdHostCtlrResetTest - Invalid uIndex %d\n",
            uIndex);

        return;
        }

    /* Get the xHCI instance at the specific index */

    pHCDData = gpXHCDData[uIndex];

    if (pHCDData == NULL)
        {
        printf("usbXhcdHostCtlrResetTest - No xHCI at uIndex %d\n",
            uIndex);

        return;
        }

    VXB_PCI_BUS_CFG_READ(pHCDData->pDev,
                (pHCDData->uPcieCapPtr + 4), 4, uPcieDevCaps);

    VXB_PCI_BUS_CFG_READ(pHCDData->pDev,
                (pHCDData->uPcieCapPtr + 8), 2, uPcieDevCtrl);

    /* Function Level Reset Capability at bit 28 */

    if (uPcieDevCaps & (1 << 28))
        {
        printf("usbXhcdHostCtlrResetTest - issue PCIe Function Level Reset\n");

        uPcieDevCtrl |= (1 << 15);

        VXB_PCI_BUS_CFG_WRITE(pHCDData->pDev,
                    (pHCDData->uPcieCapPtr + 8), 2, uPcieDevCtrl);
        }

    if (usbXhcdHostCtlrStart(pHCDData) != OK)
        {
        printf ("usbXhcdHostCtlrResetTest - usbXhcdHostCtlrStart fail\n");
        }
    }

/*******************************************************************************
*
* usbXhcdPortScShowInternal - internal show the xHCI PORTSC
*
* This routine is used to internal show the xHCI PORTSC.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

VOID usbXhcdPortScShowInternal
    (
    UINT32 uPORTSC
    )
    {
    printf("====Current Connect Status (CCS)        - %s\n",
        (uPORTSC & USB_XHCI_PORTSC_CCS) ? "SET" : "CLR");

    printf("====Port Enabled/Disabled (PED)         - %s\n",
        (uPORTSC & USB_XHCI_PORTSC_PED) ? "SET" : "CLR");

    printf("====Over-current Active (OCA)           - %s\n",
        (uPORTSC & USB_XHCI_PORTSC_OCA) ? "SET" : "CLR");

    printf("====Port Reset (PR)                     - %s\n",
        (uPORTSC & USB_XHCI_PORTSC_PR) ? "SET" : "CLR");

    printf("====Port Link State (PLS)               - %d (%s)\n",
        USB_XHCI_PORTSC_PLS_GET(uPORTSC),
        gUsbXhcdLinkStateStr[USB_XHCI_PORTSC_PLS_GET(uPORTSC)]);

    printf("====Port Power (PP)                     - %s\n",
        (uPORTSC & USB_XHCI_PORTSC_PP) ? "SET" : "CLR");

    printf("====Port Speed (Port Speed)             - %d (%s)\n",
        USB_XHCI_PORTSC_PS_GET(uPORTSC),
        gUsbXhcdSpeedStr[USB_XHCI_PORTSC_PS_GET(uPORTSC)]);

    printf("====Port Indicator Control (PIC)        - %d (%s)\n",
        USB_XHCI_PORTSC_PIC_GET(uPORTSC),
        gUsbXhcdIndicatorStr[USB_XHCI_PORTSC_PIC_GET(uPORTSC)]);

    printf("====Port Link State Write Strobe (LWS)  - %s\n",
        (uPORTSC & USB_XHCI_PORTSC_LWS) ? "SET" : "CLR");

    printf("====Connect Status Change (CSC)         - %s\n",
        (uPORTSC & USB_XHCI_PORTSC_CSC) ? "SET" : "CLR");

    printf("====Port Enabled/Disabled Change (PEC)  - %s\n",
        (uPORTSC & USB_XHCI_PORTSC_PEC) ? "SET" : "CLR");

    printf("====Warm Port Reset Change (WRC)        - %s\n",
        (uPORTSC & USB_XHCI_PORTSC_WRC) ? "SET" : "CLR");

    printf("====Over-current Change (OCC)           - %s\n",
        (uPORTSC & USB_XHCI_PORTSC_OCC) ? "SET" : "CLR");

    printf("====Port Reset Change (PRC)             - %s\n",
        (uPORTSC & USB_XHCI_PORTSC_PRC) ? "SET" : "CLR");

    printf("====Port Link State Change (PLC)        - %s\n",
        (uPORTSC & USB_XHCI_PORTSC_PLC) ? "SET" : "CLR");

    printf("====Port Config Error Change (CEC)      - %s\n",
        (uPORTSC & USB_XHCI_PORTSC_CEC) ? "SET" : "CLR");

    printf("====Cold Attach Status (CAS)            - %s\n",
        (uPORTSC & USB_XHCI_PORTSC_CAS) ? "SET" : "CLR");

    printf("====Wake on Connect Enable (WCE)        - %s\n",
        (uPORTSC & USB_XHCI_PORTSC_WCE) ? "SET" : "CLR");

    printf("====Wake on Disconnect Enable (WDE)     - %s\n",
        (uPORTSC & USB_XHCI_PORTSC_WDE) ? "SET" : "CLR");

    printf("====Wake on Over-current Enable (WOE)   - %s\n",
        (uPORTSC & USB_XHCI_PORTSC_WOE) ? "SET" : "CLR");

    printf("====Device Removable (DR)               - %s\n",
        (uPORTSC & USB_XHCI_PORTSC_DR) ? "SET" : "CLR");

    printf("====Warm Port Reset (WPR)               - %s\n",
        (uPORTSC & USB_XHCI_PORTSC_WPR) ? "SET" : "CLR");
    }

/*******************************************************************************
*
* usbXhcdRegShow - show the xHCI Registers
*
* This routine is used to show the xHCI Registers.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

VOID usbXhcdRegShow
    (
    UINT32 uIndex
    )
    {
    pUSB_XHCD_DATA pHCDData = NULL;
    UINT32 uReg32;
    UINT64 uReg64;
    UINT32 uIdx;

    if (uIndex > USB_MAX_HOSTS_PER_HCD)
        {
        printf("usbXhcdRegShow - Invalid uIndex %d\n",
            uIndex);

        return;
        }

    /* Get the xHCI instance at the specific index */

    pHCDData = gpXHCDData[uIndex];

    if (pHCDData == NULL)
        {
        printf("usbXhcdRegShow - No xHCI at uIndex %d\n",
            uIndex);

        return;
        }

    /* Operational Registers Base Address */

    printf("uOpRegBase              = 0x%lx\n", pHCDData->uOpRegBase);

    /* Read the USBCMD register */

    uReg32 = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_USBCMD);
    printf("USB_XHCI_USBCMD         = 0x%x\n", uReg32);

    printf("====Run/Stop (R/S)                      - %s\n",
        (uReg32 & USB_XHCI_USBCMD_RS) ? "SET" : "CLR");

    printf("====Host Controller Reset (HCRST)       - %s\n",
        (uReg32 & USB_XHCI_USBCMD_HCRST) ? "SET" : "CLR");

    printf("====Interrupter Enable (INTE)           - %s\n",
        (uReg32 & USB_XHCI_USBCMD_INTE) ? "SET" : "CLR");

    printf("====Host System Error Enable (HSEE)     - %s\n",
        (uReg32 & USB_XHCI_USBCMD_HSEE) ? "SET" : "CLR");

    printf("====Light Host Controller Reset (LHCRST)- %s\n",
        (uReg32 & USB_XHCI_USBCMD_LHCRST) ? "SET" : "CLR");

    printf("====Controller Save State (CSS)         - %s\n",
        (uReg32 & USB_XHCI_USBCMD_CSS) ? "SET" : "CLR");

    printf("====Controller Restore State (CRS)      - %s\n",
        (uReg32 & USB_XHCI_USBCMD_CRS) ? "SET" : "CLR");

    printf("====Enable Wrap Event (EWE)             - %s\n",
        (uReg32 & USB_XHCI_USBCMD_EWE) ? "SET" : "CLR");

    printf("====Enable U3 MFINDEX Stop (EU3S)       - %s\n",
        (uReg32 & USB_XHCI_USBCMD_EU3S) ? "SET" : "CLR");

    /* Read the USBSTS register */

    uReg32 = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_USBSTS);
    printf("USB_XHCI_USBSTS         = 0x%x\n", uReg32);

    printf("====HCHalted (HCH)                      - %s\n",
        (uReg32 & USB_XHCI_USBSTS_HCH) ? "SET" : "CLR");

    printf("====Host System Error (HSE)             - %s\n",
        (uReg32 & USB_XHCI_USBSTS_HSE) ? "SET" : "CLR");

    printf("====Event Interrupt (EINT)              - %s\n",
        (uReg32 & USB_XHCI_USBSTS_EINT) ? "SET" : "CLR");

    printf("====Port Change Detect (PCD)            - %s\n",
        (uReg32 & USB_XHCI_USBSTS_PCD) ? "SET" : "CLR");

    printf("====Save State Status (SSS)             - %s\n",
        (uReg32 & USB_XHCI_USBSTS_SSS) ? "SET" : "CLR");

    printf("====Restore State Status (RSS)          - %s\n",
        (uReg32 & USB_XHCI_USBSTS_RSS) ? "SET" : "CLR");

    printf("====Save/Restore Error (SRE)            - %s\n",
        (uReg32 & USB_XHCI_USBSTS_SRE) ? "SET" : "CLR");

    printf("====Controller Not Ready (CNR)          - %s\n",
        (uReg32 & USB_XHCI_USBSTS_CNR) ? "SET" : "CLR");

    printf("====Host Controller Error (HCE)         - %s\n",
        (uReg32 & USB_XHCI_USBSTS_HCE) ? "SET" : "CLR");

    /* Read the PAGESIZE register */

    uReg32 = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_PAGESIZE);
    printf("USB_XHCI_PAGESIZE       = 0x%x\n", uReg32);

    /* Read the DNCTRL register */

    uReg32 = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_DNCTRL);
    printf("USB_XHCI_DNCTRL         = 0x%x\n", uReg32);

    /* Read the CRCR register */

    uReg64 = USB_XHCD_READ_OP_REG64(pHCDData, USB_XHCI_CRCR);
    printf("USB_XHCI_CRCR           = 0x%016llx\n", uReg64);

    /* Read the DCBAAP register */

    uReg64 = USB_XHCD_READ_OP_REG64(pHCDData, USB_XHCI_DCBAAP);
    printf("USB_XHCI_DCBAAP         = 0x%016llx\n", uReg64);

    /* Read the CONFIG register */

    uReg32 = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_CONFIG);
    printf("USB_XHCI_CONFIG         = 0x%x\n", uReg32);

    for (uIdx = 0; uIdx < pHCDData->uMaxPorts; uIdx++)
        {
        UINT32 uPORTSC;

        /* Read the PORTSC register */

        uPORTSC = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_PORTSC(uIdx));
        printf("USB_XHCI_PORTSC[%d]      = 0x%x\n", uIdx, uPORTSC);
        usbXhcdPortScShowInternal(uPORTSC);

        /* Read the PORTPMSC register */

        uPORTSC = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_PORTPMSC(uIdx));
        printf("USB_XHCI_PORTPMSC[%d]    = 0x%x\n", uIdx, uPORTSC);

        /* Read the PORTLI register */

        uPORTSC = USB_XHCD_READ_OP_REG32(pHCDData, USB_XHCI_PORTLI(uIdx));
        printf("USB_XHCI_PORTLI[%d]      = 0x%x\n", uIdx, uPORTSC);
        }

    /* Runtime Registers Base Address */

    printf("uRtRegBase              = 0x%lx\n", pHCDData->uRtRegBase);

    /* Read the MFINDEX register */

    uReg32 = USB_XHCD_READ_RT_REG32(pHCDData, USB_XHCI_MFINDEX);
    printf("USB_XHCI_MFINDEX        = 0x%x\n", uReg32);

    for (uIdx = 0; uIdx < pHCDData->uMaxIntrs; uIdx++)
        {
        /* Read the IMAN register */

        uReg32 = USB_XHCD_READ_RT_REG32(pHCDData, USB_XHCI_IMAN(uIdx));
        printf("USB_XHCI_IMAN[%d]        = 0x%x (IE:%s,IP:%s)\n",
            uIdx, uReg32,
            ((uReg32 & USB_XHCI_IMAN_IE) ? "SET":"CLR"),
            ((uReg32 & USB_XHCI_IMAN_IP) ? "SET":"CLR"));

        /* Read the IMOD register */

        uReg32 = USB_XHCD_READ_RT_REG32(pHCDData, USB_XHCI_IMOD(uIdx));
        printf("USB_XHCI_IMOD[%d]        = 0x%x\n", uIdx, uReg32);

        /* Read the ERSTSZ register */

        uReg32 = USB_XHCD_READ_RT_REG32(pHCDData, USB_XHCI_ERSTSZ(uIdx));
        printf("USB_XHCI_ERSTSZ[%d]      = 0x%x\n", uIdx, uReg32);

        /* Read the ERSTBA register */

        uReg64 = USB_XHCD_READ_RT_REG64(pHCDData, USB_XHCI_ERSTBA(uIdx));
        printf("USB_XHCI_ERSTBA[%d]      = 0x%016llx\n", uIdx, uReg64);

        /* Read the ERDP register */

        uReg64 = USB_XHCD_READ_RT_REG64(pHCDData, USB_XHCI_ERDP(uIdx));
        printf("USB_XHCI_ERDP[%d]        = 0x%016llx\n", uIdx, uReg64);
        }
    }

/*******************************************************************************
*
* usbXhcdRingShowInternal - internal show the xHCI Ring
*
* This routine is used to internal show the xHCI Ring.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

VOID usbXhcdRingShowInternal
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCD_RING      pRing
    )
    {
    pUSB_XHCD_SEGMENT   pSeg;
    pUSB_XHCI_TRB       pTRB;
    UINT32              uCtrl;
    UINT32              uSegIndex;
    UINT32              uIdx;
    UINT8               uTrbType;
    BOOL                bNewLine = FALSE;

    if (pRing == NULL)
        return;

    printf("RING: uType %d uNumSegs %d uNumEnq %d uNumDeq %d uCycle %d "
            "pDeqSeg %p pDeqTRB %p pEnqSeg %p pEnqTRB %p pHeadSeg %p REQs %d\n",
            pRing->uType, pRing->uNumSegs, pRing->uNumEnq, pRing->uNumDeq,
            pRing->uCycle, pRing->pDeqSeg, pRing->pDeqTRB, pRing->pEnqSeg,
            pRing->pEnqTRB, pRing->pHeadSeg, lstCount(&pRing->reqList));

    /* Show each Segment of the Ring */

    pSeg = pRing->pHeadSeg;
    uSegIndex = 0;

    while (pSeg != NULL)
        {
        printf("\nNon-Empty TRBs for Segment[%08d] BUS ADDR @0x%016llx:\n",
            uSegIndex++, pSeg->dmaMapId->fragList[0].frag);

        for (uIdx = 0; uIdx < pSeg->uNumTRBs; uIdx++)
            {
            pTRB = &pSeg->pTRBs[uIdx];

            if (/*(uIdx == 0) ||
                (uIdx == (pSeg->uNumTRBs - 1)) ||*/
                (pTRB->uDataLo != 0) ||
                (pTRB->uDataHi != 0) ||
                (pTRB->uInfo != 0) ||
                (pTRB->uCtrl != 0))
                {
                if (bNewLine)
                    {
                    printf("\n");

                    bNewLine = FALSE;
                    }

                uCtrl = USB_XHCD_SWAP_DESC_DATA32(pHCDData, pTRB->uCtrl);
                uTrbType = (UINT8)USB_XHCI_TRB_CTRL_TYPE_GET(uCtrl);

                printf("TRB[%08d] @%p - "
                    "[uData 0x%016llx uInfo 0x%08x "
                    "uCtrl 0x%08x (TYPE:%02d,CH:%d,C:%d)] - "
                    "(%s%s TRB)\n",
                    uIdx,
                    pTRB,
                    usbXhcdGetTrbData64(pHCDData, pTRB),
                    USB_XHCD_SWAP_DESC_DATA32(pHCDData, pTRB->uInfo),
                    uCtrl,
                    USB_XHCI_TRB_CTRL_TYPE_GET(uCtrl),
                    USB_XHCI_TRB_CTRL_CHAIN_BIT_GET(uCtrl),
                    USB_XHCI_TRB_CTRL_CYCLE_BIT_GET(uCtrl),
                    ((uTrbType == USB_XHCI_TRB_XFER_EVENT)?
                    ((uCtrl & USB_XHCI_TRB_CTRL_ED_MASK)?
                      "Event Data " : "Normal ") : ("")),
                    usbXhcdTrbName(uTrbType));

                }
            else
                {
                bNewLine = TRUE;

                /*printf(".");*/
                }
            }

        pSeg = pSeg->pNextSeg;

        if (uSegIndex >= pRing->uNumSegs)
            break;
        }
    }

/*******************************************************************************
*
* usbXhcdRingShow - show the xHCI Rings
*
* This routine is used to show the xHCI Rings.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

VOID usbXhcdRingShow
    (
    UINT32 uIndex,
    UINT32 uMaxEvtRings
    )
    {
    pUSB_XHCD_DATA          pHCDData;
    pUSB_XHCI_ERST_ENTRY    pERSTEntry;
    pUSB_XHCD_DEVICE        pHCDDevice;
    pUSB_XHCD_PIPE          pHCDPipe;
    UINT32 uIdx;
    UINT32 uIdx2;
    UINT32 uIdx3;
    UINT64 uERSTBA;

    if (uIndex > USB_MAX_HOSTS_PER_HCD)
        {
        printf("usbXhcdRingShow - Invalid uIndex %d\n",
            uIndex);

        return;
        }

    /* Get the xHCI instance at the specific index */

    pHCDData = gpXHCDData[uIndex];

    if (pHCDData == NULL)
        {
        printf("usbXhcdRingShow - No xHCI at uIndex %d\n",
            uIndex);

        return;
        }

    printf("\n---------------------------------------------------------\n");

    printf("\nShow Command Ring @%p:\n", pHCDData->pCmdRing);

    usbXhcdRingShowInternal(pHCDData, pHCDData->pCmdRing);

    printf("\n---------------------------------------------------------\n");

    printf("Total %lu Event Rings, first %u of them shown below\n",
        pHCDData->uNumEvtRings, (uMaxEvtRings + 1));

    for (uIdx = 0; uIdx < pHCDData->uNumEvtRings; uIdx++)
        {
        /* Only show the specified number of event rings */

        if (uIdx > uMaxEvtRings)
            break;

        uERSTBA = USB_XHCD_READ_RT_REG64(pHCDData, USB_XHCI_ERSTBA(uIndex));
        printf("\n---------------------------------------------------------\n");
        printf("\nShow ERST [ERSTBA 0x%016llx] for Event Ring [%d] @%p:\n",
               uERSTBA, uIdx, pHCDData->ppEvtRings[uIdx]);

        /* Get to the start of new ERST (note the cast is required) */

        pERSTEntry = (pUSB_XHCI_ERST_ENTRY)(((UINT8 *)(pHCDData->pERSTs)) +
                      (uIdx * pHCDData->uERSTSize));

        for (uIdx2 = 0; uIdx2 < pHCDData->uMaxERSTEs; uIdx2++)
            {
            printf("ERST Entry [%d] @%p: 0x%016llx 0x%08x 0x%08x\n",
                uIdx2,
                pERSTEntry,
                pERSTEntry->uRingSegBase,
                pERSTEntry->uRingSegSize,
                pERSTEntry->uRsvdZ);

            pERSTEntry++;
            }

        printf("\nShow Event Ring [%d] @%p:\n",
            uIdx, pHCDData->ppEvtRings[uIdx]);

        usbXhcdRingShowInternal(pHCDData, pHCDData->ppEvtRings[uIdx]);

        printf("\n---------------------------------------------------------\n");
        }

    printf("Show HCD Default USB3 Control Pipe EP RING @%p\n",
           pHCDData->pDefaultUSB3Pipe->pEpRing);

    usbXhcdRingShowInternal(pHCDData, pHCDData->pDefaultUSB3Pipe->pEpRing);

    printf("Show HCD Default USB2 Control Pipe EP RING @%p\n",
           pHCDData->pDefaultUSB2Pipe->pEpRing);

    usbXhcdRingShowInternal(pHCDData, pHCDData->pDefaultUSB2Pipe->pEpRing);

    for (uIdx = 1; uIdx < (pHCDData->uMaxSlots + 1); uIdx++)
        {
        pHCDDevice = pHCDData->ppDevSlots[uIdx];

        if (pHCDDevice == NULL)
            {
            continue;
            }

        for (uIdx2 = 1; uIdx2 < USB_XHCI_MAX_DEV_PIPES; uIdx2++)
            {
            pHCDPipe = pHCDDevice->pDevPipes[uIdx2];

            if (pHCDPipe == NULL)
                continue;

            if (pHCDPipe->pEpRing != NULL)
                {
                printf("Show Slot ID %d EP ID %d EP RING @%p\n",
                        uIdx, uIdx2, pHCDPipe->pEpRing);
                usbXhcdRingShowInternal(pHCDData, pHCDPipe->pEpRing);
                }
            else
                {
                printf("Show Slot ID %d EP ID %d Streams RINGs:\n",
                        uIdx, uIdx2);

                for (uIdx3 = 0; uIdx3 < pHCDPipe->streamInfo.uNumStreams; uIdx3++)
                    {
                    printf("Stream CTX[%08d] @%p DEQ PTR @0x%016llx Xfer Ring[%08d]:\n",
                        uIdx3,
                        &pHCDPipe->streamInfo.pStreamCtxs[uIdx3],
                        pHCDPipe->streamInfo.pStreamCtxs[uIdx3].uStreamDeqPtr,
                        uIdx3);

                    usbXhcdRingShowInternal(pHCDData,
                        pHCDPipe->streamInfo.ppStreamRings[uIdx3]);
                    }
                }
            }
        }
    }

/*******************************************************************************
*
* usbXhcdSlotCtxShowInternal - internal show the Slot Context data structure
*
* This routine is used to internal show the Slot Context data structure.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

VOID usbXhcdSlotCtxShowInternal
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCI_SLOT_CTX  pSlotCtx
    )
    {
    UINT32 uValue;
    UINT32 uSlotInfo0 = USB_XHCD_SWAP_DESC_DATA32(pHCDData, pSlotCtx->uSlotInfo0);
    UINT32 uSlotInfo1 = USB_XHCD_SWAP_DESC_DATA32(pHCDData, pSlotCtx->uSlotInfo1);
    UINT32 uSlotInfo2 = USB_XHCD_SWAP_DESC_DATA32(pHCDData, pSlotCtx->uSlotInfo2);
    UINT32 uSlotInfo3 = USB_XHCD_SWAP_DESC_DATA32(pHCDData, pSlotCtx->uSlotInfo3);

    printf("Show SLOT CTX @%p\n", pSlotCtx);

    printf("uSlotInfo0 = 0x%08x\n", uSlotInfo0);

    printf("===Route String         - 0x%05x\n",
        USB_XHCI_SLOT_CTX_ROUTE_STRING_GET(uSlotInfo0));

    uValue = USB_XHCI_SLOT_CTX_SPEED_GET(uSlotInfo0);

    printf("===Speed                - %d (%s)\n",
        uValue,
        (uValue <= 4) ? gUsbXhcdSpeedStr[uValue] : "Reserved");

    printf("===Multi-TT (MTT)       - %d\n",
        USB_XHCI_SLOT_CTX_MTT_GET(uSlotInfo0));

    printf("===Hub                  - %d\n",
        USB_XHCI_SLOT_CTX_HUB_GET(uSlotInfo0));

    printf("===Context Entries      - %d\n",
        USB_XHCI_SLOT_CTX_ENTRIES_GET(uSlotInfo0));

    printf("uSlotInfo1 = 0x%08x\n", uSlotInfo1);

    printf("===Max Exit Latency     - %d\n",
        USB_XHCI_SLOT_CTX_MAX_EXIT_LATENCY_GET(uSlotInfo1));

    printf("===Root Hub Port Number - %d\n",
        USB_XHCI_SLOT_CTX_RH_PORT_GET(uSlotInfo1));

    printf("===Number of Ports      - %d\n",
        USB_XHCI_SLOT_CTX_NUM_PORTS_GET(uSlotInfo1));

    printf("uSlotInfo2 = 0x%08x\n", uSlotInfo2);

    printf("===TT Hub Slot ID       - %d\n",
        USB_XHCI_SLOT_CTX_TT_HUB_SLOT_ID_GET(uSlotInfo2));

    printf("===TT Port Number       - %d\n",
        USB_XHCI_SLOT_CTX_TT_PORT_GET(uSlotInfo2));

    printf("===TT Think Time (TTT)  - %d\n",
        USB_XHCI_SLOT_CTX_TT_TT_GET(uSlotInfo2));

    printf("===Interrupter Target   - %d\n",
        USB_XHCI_SLOT_CTX_INTR_TARGET_GET(uSlotInfo2));

    printf("uSlotInfo3 = 0x%08x\n", uSlotInfo3);

    printf("===USB Device Address   - %d\n",
        USB_XHCI_SLOT_CTX_DEV_ADDR_GET(uSlotInfo3));

    uValue = USB_XHCI_SLOT_CTX_SLOT_STATE_GET(uSlotInfo3);

    printf("===Slot State           - %d (%s)\n",
        uValue, (uValue <= 3) ? gUsbXhcdSlotStateStr[uValue] : "Reserved");
    }

/*******************************************************************************
*
* usbXhcdEpCtxShowInternal - internal show the Endpoint Context data structure
*
* This routine is used to internal show the Endpoint Context data structure.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

VOID usbXhcdEpCtxShowInternal
    (
    pUSB_XHCD_DATA      pHCDData,
    pUSB_XHCI_EP_CTX    pEpCtx
    )
    {
    UINT32 uEpInfo0 = USB_XHCD_SWAP_DESC_DATA32(pHCDData, pEpCtx->uEpInfo0);
    UINT32 uEpInfo1 = USB_XHCD_SWAP_DESC_DATA32(pHCDData, pEpCtx->uEpInfo1);
    UINT32 uEpDeqLo = USB_XHCD_SWAP_DESC_DATA32(pHCDData, pEpCtx->uEpDeqLo);
    UINT32 uEpDeqHi = USB_XHCD_SWAP_DESC_DATA32(pHCDData, pEpCtx->uEpDeqHi);
    UINT32 uEpInfo2 = USB_XHCD_SWAP_DESC_DATA32(pHCDData, pEpCtx->uEpInfo2);

    printf("Show EP CTX @%p\n", pEpCtx);

    printf("uEpInfo0 = 0x%08x\n", uEpInfo0);

    printf("===Endpoint State (EP State)            - %d (%s)\n",
        USB_XHCI_EP_CTX_EP_STATE_GET(uEpInfo0),
        gUsbXhcdEpStateStr[USB_XHCI_EP_CTX_EP_STATE_GET(uEpInfo0)]);

    printf("===Mult                                 - %d\n",
        USB_XHCI_EP_CTX_MULT_GET(uEpInfo0));

    printf("===Max Primary Streams (MaxPStreams)    - %d\n",
        USB_XHCI_EP_CTX_MAX_PRI_STREAMS_GET(uEpInfo0));

    printf("===Linear Stream Array (LSA)            - %d\n",
        USB_XHCI_EP_CTX_LSA_GET(uEpInfo0));

    printf("===Interval                             - %d\n",
        USB_XHCI_EP_CTX_INTERVAL_GET(uEpInfo0));

    printf("uEpInfo1 = 0x%08x\n", uEpInfo1);

    printf("===Error Count (CErr)                   - %d\n",
        USB_XHCI_EP_CTX_CERR_GET(uEpInfo1));

    printf("===Endpoint Type (EP Type)              - %d (%s)\n",
        USB_XHCI_EP_CTX_EP_TYPE_GET(uEpInfo1),
        gUsbXhcdEpTypeStr[USB_XHCI_EP_CTX_EP_TYPE_GET(uEpInfo1)]);

    printf("===Host Initiate Disable (HID)          - %d\n",
        USB_XHCI_EP_CTX_HID_GET(uEpInfo1));

    printf("===Max Burst Size                       - %d\n",
        USB_XHCI_EP_CTX_MAX_BURST_SIZE_GET(uEpInfo1));

    printf("===Max Packet Size                      - %d\n",
        USB_XHCI_EP_CTX_MAX_PACKET_SIZE_GET(uEpInfo1));

    printf("uEpDeqPtr = 0x%08x : %08x\n", uEpDeqHi, uEpDeqLo);

    printf("===Dequeue Cycle State (DCS)            - %d\n",
        USB_XHCI_EP_CTX_DCS_GET(uEpDeqLo));

    printf("===TR Dequeue Pointer                   - 0x%08x : %08x\n",
        uEpDeqHi, USB_XHCI_EP_CTX_TR_DEQ_PTR_GET(uEpDeqLo));

    printf("uEpInfo2 = 0x%08x\n", uEpInfo2);

    printf("===Average TRB Length                   - %d\n",
        USB_XHCI_EP_CTX_AVRG_TRB_LEN_GET(uEpInfo2));

    printf("===Max Endpoint Service Time Interval Payload (Max ESIT Payload) - %d\n",
        USB_XHCI_EP_CTX_MAX_ESIT_PAYLOAD_GET(uEpInfo2));
    }

/*******************************************************************************
*
* usbXhcdCtxShow - show the xHCI Contexts
*
* This routine is used to show the xHCI Contexts.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

VOID usbXhcdCtxShow
    (
    UINT32 uIndex,
    BOOL   bShowInputCtx,
    BOOL   bShowRing
    )
    {
    pUSB_XHCD_DATA          pHCDData;
    UINT32                  uIdx;
    UINT32                  uIdx2;
    UINT32                  uIdx3;
    BOOL                    bNewLine;
    UINT64                  uDCBAAEntry;
    pUSB_XHCD_DEVICE        pDevice;
    pUSB_XHCD_PIPE          pHCDPipe;
    pUSB_XHCI_EP_CTX        pEpCtx;
    pUSB_XHCI_SLOT_CTX      pSlotCtx;
    pUSB_XHCI_INPUT_CTRL_CTX    pInputCtrlCtx;

    if (uIndex > USB_MAX_HOSTS_PER_HCD)
        {
        printf("usbXhcdCtxShow - Invalid uIndex %d\n",
            uIndex);

        return;
        }

    /* Get the xHCI instance at the specific index */

    pHCDData = gpXHCDData[uIndex];

    if (pHCDData == NULL)
        {
        printf("usbXhcdCtxShow - No xHCI at uIndex %d\n",
            uIndex);

        return;
        }

    /* Read the DCBAA pointer register */

    uDCBAAEntry = USB_XHCD_READ_OP_REG64(pHCDData, USB_XHCI_DCBAAP);

    /*
     * The DCBAA shall contain MaxSlotsEn + 1 entries, first entry (entry_0)
     * in the DCBAA shall contain pointer to the Scratchpad Buffer Array if
     * the Max Scratchpad Buffers field of the HCSPARAMS2 register is > '0';
     * otherwise first entry (entry_0) should be cleared to '0'; other entries
     * shall contain pointer to a Device Context data structure.
     */

    printf("MaxSlotsEn %d, Max Scratchpad Buffers %d, DCBAAP 0x%016llx\n",
        pHCDData->uMaxSlots, pHCDData->uMaxSPBs, uDCBAAEntry);

    bNewLine = FALSE;

    for (uIdx = 0; uIdx < (pHCDData->uMaxSlots + 1); uIdx++)
        {
        uDCBAAEntry = USB_XHCD_SWAP_DESC_DATA64(pHCDData,
                                                pHCDData->pDCBAA[uIdx]);

        if ((uIdx == 0) ||
            (uIdx == pHCDData->uMaxSlots) ||
            (uDCBAAEntry != 0ULL))
            {
            if (bNewLine == TRUE)
                {
                bNewLine = FALSE;
                printf("\n");
                }

            printf("DCBAA entry [%08d] @%p = 0x%016llx\n",
                uIdx, &pHCDData->pDCBAA[uIdx], uDCBAAEntry);
            }
        else
            {
            printf(".");
            bNewLine = TRUE;
            }
        }

    for (uIdx = 1; uIdx < (pHCDData->uMaxSlots + 1); uIdx++)
        {
        pDevice = pHCDData->ppDevSlots[uIdx];

        if (pDevice == NULL)
            {
            continue;
            }

        if (bShowInputCtx)
            {
            printf("\n========================================================\n");

            printf("Show Slot ID %d INPUT CTX @%p - Context @%p[CPU] - @0x%016llx[BUS]\n",
                   uIdx,
                   pDevice->pIntputCtx,
                   pDevice->pIntputCtx->pCtxArea,
                   pDevice->pIntputCtx->uCtxBusAddr);

            pInputCtrlCtx = USB_XHCI_INPUT_CTRL_CTX_CAST(pHCDData,
                                pDevice->pIntputCtx);

            printf("INPUT CTRL CTX "
                   "uDropFlags = 0x%08x uAddFlags = 0x%08x uRsvdZ[0] %x\n",
                   pInputCtrlCtx->uDropFlags,
                   pInputCtrlCtx->uAddFlags,
                   pInputCtrlCtx->uRsvdZ[0]);

            pSlotCtx = USB_XHCI_SLOT_CTX_CAST(pHCDData, pDevice->pIntputCtx);

            printf("\n========================================================\n");

            usbXhcdSlotCtxShowInternal(pHCDData, pSlotCtx);

            for (uIdx2 = 1; uIdx2 < USB_XHCI_MAX_DEV_PIPES; uIdx2++)
                {
                if ((pDevice->pDevPipes[uIdx2] == NULL) &&
                    (!(pInputCtrlCtx->uDropFlags & (1 << uIdx2))) &&
                    (!(pInputCtrlCtx->uAddFlags & (1 << uIdx2))))
                    continue;

                pEpCtx = USB_XHCI_EP_CTX_CAST(pHCDData,
                                              pDevice->pIntputCtx,
                                              (uIdx2));

                printf("\n========================================================\n");

                printf("Show Slot ID %d EP ID %d INPUT EP CTX @%p %s%s\n",
                        uIdx, uIdx2, pEpCtx,
                        ((pInputCtrlCtx->uDropFlags & (1 << uIdx2)) ? "[DROPPED] " : ""),
                        ((pInputCtrlCtx->uAddFlags & (1 << uIdx2)) ? "[ADDED]" : ""));

                usbXhcdEpCtxShowInternal(pHCDData, pEpCtx);
                }

            printf("\n========================================================\n");
            }

        printf("\n========================================================\n");

        printf("Show Slot ID %d OUTPUT CTX @%p - Context @%p[CPU] - @0x%016llx[BUS]\n",
               uIdx,
               pDevice->pOutputCtx,
               pDevice->pOutputCtx->pCtxArea,
               pDevice->pOutputCtx->uCtxBusAddr);

        pSlotCtx = USB_XHCI_SLOT_CTX_CAST(pHCDData, pDevice->pOutputCtx);

        usbXhcdSlotCtxShowInternal(pHCDData, pSlotCtx);

        for (uIdx2 = 1; uIdx2 < USB_XHCI_MAX_DEV_PIPES; uIdx2++)
            {
            pHCDPipe = pDevice->pDevPipes[uIdx2];

            if (pHCDPipe == NULL)
                continue;

            pEpCtx = USB_XHCI_EP_CTX_CAST(pHCDData,
                                          pDevice->pOutputCtx,
                                          (uIdx2));

            printf("\n========================================================\n");

            printf("Show Slot ID %d EP ADDR 0x%02x EP ID %d OUTPUT EP CTX @%p\n",
                    uIdx, pHCDPipe->uEpAddr, uIdx2, pEpCtx);

            usbXhcdEpCtxShowInternal(pHCDData, pEpCtx);

            if (bShowRing)
                {
                printf("Stream CTX BUS ADDR @0x%016llx\n",
                    pHCDPipe->streamInfo.uStreamCtxs);

                for (uIdx3 = 0; uIdx3 < pHCDPipe->streamInfo.uNumStreams; uIdx3++)
                    {
                    printf("Stream CTX[%08d] @%p DEQ PTR @0x%016llx Xfer Ring[%08d]:\n",
                        uIdx3,
                        &pHCDPipe->streamInfo.pStreamCtxs[uIdx3],
                        pHCDPipe->streamInfo.pStreamCtxs[uIdx3].uStreamDeqPtr,
                        uIdx3);

                    usbXhcdRingShowInternal(pHCDData,
                        pHCDPipe->streamInfo.ppStreamRings[uIdx3]);
                    }
                }
            }
        }
    }

/*******************************************************************************
*
* usbXhcdDataShow - show the xHCI Data Structures
*
* This routine is used to show the xHCI Data Structures.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

VOID usbXhcdDataShow
    (
    UINT32 uIndex
    )
    {
    pUSB_XHCD_DATA pHCDData = NULL;

    if (uIndex > USB_MAX_HOSTS_PER_HCD)
        {
        printf("usbXhcdDataShow - Invalid uIndex %d\n",
            uIndex);

        return;
        }

    /* Get the xHCI instance at the specific index */

    pHCDData = gpXHCDData[uIndex];

    if (pHCDData == NULL)
        {
        printf("usbXhcdDataShow - No xHCI at uIndex %d\n",
            uIndex);

        return;
        }

    /* Pointer to HCD data structure */

    printf("pHCDData       = %p\n", pHCDData);

    /* Capability Registers Base Addrress */

    printf("uCpRegBase       = 0x%lx\n", pHCDData->uCpRegBase);

    /* Operational Registers Base Address */

    printf("uOpRegBase       = 0x%lx\n", pHCDData->uOpRegBase);

    /* Runtime Registers Base Address */

    printf("uRtRegBase       = 0x%lx\n", pHCDData->uRtRegBase);

    /* Doorbell Registers Base Address */

    printf("uDbRegBase       = 0x%lx\n", pHCDData->uDbRegBase);

    /* xHCI Extended Capabilities Base Address */

    printf("uXpRegBase       = 0x%lx\n", pHCDData->uXpRegBase);

    /* Saved value of Structural Parameters 1 */

    printf("uHCSPARAMS1      = 0x%x\n", pHCDData->uHCSPARAMS1);

    /* Saved value of Structural Parameters 2 */

    printf("uHCSPARAMS2      = 0x%x\n", pHCDData->uHCSPARAMS2);

    /* Saved value of Structural Parameters 3 */

    printf("uHCSPARAMS3      = 0x%x\n", pHCDData->uHCSPARAMS3);

    /* Saved value of Capability Parameters */

    printf("uHCCPARAMS       = 0x%x\n", pHCDData->uHCCPARAMS);

    /* Saved value of Interface Version Number */

    printf("uHCIVERSION      = 0x%x\n", pHCDData->uHCIVERSION);

    /* Saved value of Capability Register Length */

    printf("uCAPLENGTH       = 0x%x\n", pHCDData->uCAPLENGTH);

    /* PCI VID */

    printf("uPciVID          = 0x%02X\n", pHCDData->uPciVID);

    /* PCI PID */

    printf("uPciPID          = 0x%02X\n", pHCDData->uPciPID);

    /* Max number of device slots supported */

    printf("uMaxSlots        = 0x%x\n", pHCDData->uMaxSlots);

    /* Max number of root ports supported */

    printf("uMaxPorts        = 0x%x\n", pHCDData->uMaxPorts);

    /* Max number of interrupters supported */

    printf("uMaxIntrs        = 0x%x\n", pHCDData->uMaxIntrs);

    /* Max number of Scratchpad Buffers supported */

    printf("uMaxSPBs         = 0x%x\n", pHCDData->uMaxSPBs);

    /* Max number of Event Ring Segment Table entries */

    printf("uMaxERSTEs       = 0x%x\n", pHCDData->uMaxERSTEs);

    /* Page size supported */

    printf("uPageSize        = 0x%x\n", pHCDData->uPageSize);

    /* Device Context size (32 or 64) */

    printf("uCSZ64           = 0x%x\n", pHCDData->uCSZ64);

    /* Addressing 64 bit capability */

    printf("uAC64            = 0x%x\n", pHCDData->uAC64);

    /* PCI Standard Capbitlity Pointer */

    printf("uPciCapPtr       = 0x%x\n", pHCDData->uPciCapPtr);

    /* PCIe Capbitlity Pointer */

    printf("uPcieCapPtr      = 0x%x\n", pHCDData->uPcieCapPtr);

    /* PCI MSI Capbitlity Pointer */

    printf("uMsiCapPtr       = 0x%x\n", pHCDData->uMsiCapPtr);

    /* PCI MSI-X Capbitlity Pointer */

    printf("uMsixCapPtr      = 0x%x\n", pHCDData->uMsixCapPtr);

    /* PCI PM Capbitlity Pointer */

    printf("uPmCapPtr        = 0x%x\n", pHCDData->uPmCapPtr);
    }

/*******************************************************************************
*
* usbXhcdPciCapShow - show the xHCI PCI/PCIe Capabilities
*
* This routine is used to show the xHCI PCI/PCIe Capabilities.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

VOID usbXhcdPciCapShow
    (
    UINT32 uIndex
    )
    {
    pUSB_XHCD_DATA pHCDData;
    VXB_DEVICE_ID  pDev;
    UINT8  uCapIdPci = 0;
    UINT8  uOffsetPci = 0;
    UINT8  uOffsetPciNext = 0;
    UINT16 uCapIdPcie = 0;
    UINT16 uOffsetPCIe = 0;
    UINT8  uCapVerPcie = 0;

    if (uIndex > USB_MAX_HOSTS_PER_HCD)
        {
        printf("usbXhcdPciCapShow - Invalid uIndex %d\n",
            uIndex);

        return;
        }

    /* Get the xHCI instance at the specific index */

    pHCDData = gpXHCDData[uIndex];

    if (pHCDData == NULL)
        {
        printf("usbXhcdPciCapShow - No xHCI at uIndex %d\n",
            uIndex);

        return;
        }

    /* VxBus representation of this xHCI instance */

    pDev = pHCDData->pDev;

    /* Check if the parameter is valid */

    if (pDev == NULL)
        {
        printf("usbXhcdPciCapShow - pDev NULL\n");

        return;
        }

    /* If there is no xHCI PCI Extended Capability, reutnr ERROR */

    if (pHCDData->uPciCapPtr == 0)
        {
        printf("usbXhcdPciCapShow - No PCI Standard Capabilities!\n");

        return;
        }

    uOffsetPci = pHCDData->uPciCapPtr;

    do
        {
        /*
         * The bottom 2 bits of this offset are Reserved and must
         * be implemented as 00b although software must mask them
         * to allow for future uses of these bits.
         */

        uOffsetPci = (UINT8)(uOffsetPci & ~0x03);
        uOffsetPciNext = 0;

        printf("Standard PCI/PCIe Capability ID @0x%x", uOffsetPci);

        /*
         * Read the config space using access functions
         */

        /* Get the Capability ID field */

        VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 0), 1, uCapIdPci);

        /* Get the offset of the next Standard PCI Capability */

        VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 1), 1, uOffsetPciNext);

        switch (uCapIdPci)
            {
            case USB_XHCI_PCI_CAP_ID_PCIPM:
                {
                UINT16 uPMC = 0;
                UINT32 uPMCSR = 0;

                printf("====USB_XHCI_PCI_CAP_ID_PCIPM - "
                    "PCI Power Management Interface\n");

                /* Get PM Capabilities (PMC) for PCI PM Capability */

                VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 2), 2, uPMC);

                printf("--------PMC 0x%04x:\n", uPMC);
                printf("--------Version %d\n",
                    USB_XHCI_PCI_PM_VER_GET(uPMC));

                printf("--------PCI clock for PME# operation %s required\n",
                    (uPMC & USB_XHCI_PCI_PM_PME_CLK) ? "is" : "is not");

                printf("--------Device Specific Initialization %s required\n",
                    (uPMC & USB_XHCI_PCI_PM_DSI) ? "is" : "is not");

                printf("--------AUX Current %d\n",
                    USB_XHCI_PCI_PM_AUX_CURRENT_GET(uPMC));

                printf("--------D1 Power Management State %s supported\n",
                    (uPMC & USB_XHCI_PCI_PM_D1_SUPPORT) ? "is" : "is not");

                printf("--------D2 Power Management State %s supported\n",
                    (uPMC & USB_XHCI_PCI_PM_D2_SUPPORT) ? "is" : "is not");

                printf("--------PME Support 0x%x\n",
                    USB_XHCI_PCI_PM_PME_SUPPORT_GET(uPMC));

                /* Get PM Control/Status - PMCSR for PCI PM Capability */

                VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 4), 4, uPMCSR);
                printf("--------PMCSR 0x%08x:\n", uPMCSR);

                printf("--------Power State 0x%x\n",
                    USB_XHCI_PCI_PM_POWER_STATE_GET(uPMCSR));

                printf("--------%s Soft Reset transitioning from D3hot to D0\n",
                    (uPMCSR & USB_XHCI_PCI_PM_NO_SFT_RST) ? "No" : "Do");

                printf("--------%s the function to assert PME#\n",
                    (uPMCSR & USB_XHCI_PCI_PM_PME_EN) ? "Enabled" : "Disabled");

                printf("--------Data %d reported in Data and Data_Scale field\n",
                    USB_XHCI_PCI_PM_DATA_SELECT_GET(uPMCSR));

                printf("--------Data_Scale %d reported\n",
                    USB_XHCI_PCI_PM_DATA_SCALE_GET(uPMCSR));

                printf("--------PME# signal %s asserted\n",
                    (uPMCSR & USB_XHCI_PCI_PM_PME_STS) ? "is" : "is not");

                printf("--------B2/B3 Power Management State %s supported\n",
                    (uPMCSR & USB_XHCI_PCI_PM_B2B3_SUPPORT) ? "is" : "is not");

                printf("--------Bus Power/Clock Control %s\n",
                    (uPMCSR & USB_XHCI_PCI_PM_BP_CC_EN) ? "Enabled" : "Disabled");
                }
                break;
            case USB_XHCI_PCI_CAP_ID_MSI:
                {
                UINT16 uMsgCtrl = 0;
                UINT32 uMsi32 = 0;

                printf("====USB_XHCI_PCI_CAP_ID_MSI   - "
                    "PCI Message Signaled Interrupts\n");

                /* Get the Message Control PCI MSI Capability */

                VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 2), 2, uMsgCtrl);

                printf("--------MSI %s\n",
                    (uMsgCtrl & USB_XHCI_PCI_MSI_EN) ? "Enabled" : "Disabled");
                printf("--------MSI Multiple Message Capable %d\n",
                    (1 << USB_XHCI_PCI_MSI_MMC_GET(uMsgCtrl)));
                printf("--------MSI Multiple Message Enable %d\n",
                    (1 << USB_XHCI_PCI_MSI_MME_GET(uMsgCtrl)));
                printf("--------64 bit address %s\n",
                    (uMsgCtrl & USB_XHCI_PCI_MSI_AC64) ? "capable" : "incapable");
                printf("--------Per-vector masking %s\n",
                    (uMsgCtrl & USB_XHCI_PCI_MSI_VMC) ? "capable" : "incapable");

                if (uMsgCtrl & USB_XHCI_PCI_MSI_AC64)
                    {
                    VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 4), 4, uMsi32);
                    printf("--------Message Address - 0x%x\n", uMsi32);

                    VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 8), 4, uMsi32);
                    printf("--------Message Upper Address - 0x%x\n", uMsi32);

                    VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 12), 4, uMsi32);
                    printf("--------Message Data - 0x%x\n", uMsi32 & 0xFFFF);

                    if (uMsgCtrl & USB_XHCI_PCI_MSI_VMC)
                        {
                        VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 16), 4, uMsi32);
                        printf("--------Mask Bits - 0x%x\n", uMsi32);

                        VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 20), 4, uMsi32);
                        printf("--------Pending Bits - 0x%x\n", uMsi32);
                        }
                    }
                else
                    {
                    VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 4), 4, uMsi32);
                    printf("--------Message Address - 0x%x\n", uMsi32);

                    VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 8), 4, uMsi32);
                    printf("--------Message Data - 0x%x\n", uMsi32 & 0xFFFF);

                    if (uMsgCtrl & USB_XHCI_PCI_MSI_VMC)
                        {
                        VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 12), 4, uMsi32);
                        printf("--------Mask Bits - 0x%x\n", uMsi32);

                        VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 16), 4, uMsi32);
                        printf("--------Pending Bits - 0x%x\n", uMsi32);
                        }
                    }

                }
                break;
            case USB_XHCI_PCI_CAP_ID_PCIe:
                {
                USB_XHCI_PCIe_CAP pcieCap;

                printf("====USB_XHCI_PCI_CAP_ID_PCIe  - "
                    "PCI Express\n");

                uOffsetPCIe = USB_XHCI_PCIe_EXT_CAP_BASE;

                pcieCap.uCapId = USB_XHCI_PCI_CAP_ID_PCIe;

                VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 0), 1, pcieCap.uCapId);
                VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 1), 1, pcieCap.uNextCap);
                VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 2), 2, pcieCap.uPcieCaps);
                VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 4), 4, pcieCap.uDeviceCaps);
                VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 8), 2, pcieCap.uDeviceCtrl);
                VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 10), 2, pcieCap.uDeviceStat);
                VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 12), 4, pcieCap.uLinkCaps);
                VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 16), 2, pcieCap.uLinkCtrl);
                VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 18), 2, pcieCap.uLinkStat);
                VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 20), 4, pcieCap.uSlotCaps);
                VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 24), 2, pcieCap.uSlotCtrl);
                VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 26), 2, pcieCap.uSlotStat);
                VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 28), 2, pcieCap.uRootCtrl);
                VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 30), 2, pcieCap.uRootCaps);
                VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 32), 4, pcieCap.uRootStat);
                VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 36), 4, pcieCap.uDeviceCaps2);
                VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 40), 2, pcieCap.uDeviceCtrl2);
                VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 42), 2, pcieCap.uDeviceStat2);
                VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 44), 4, pcieCap.uLinkCaps2);
                VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 46), 2, pcieCap.uLinkCtrl2);
                VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 48), 2, pcieCap.uLinkStat2);
                VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 52), 4, pcieCap.uSlotCaps2);
                VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 56), 2, pcieCap.uSlotCtrl2);
                VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 58), 2, pcieCap.uSlotStat2);

                printf("PCI Express Capability ID 0x%x\n", pcieCap.uCapId);
                printf("Next Capability Pointer 0x%x\n", pcieCap.uNextCap);
                printf("PCI Express Capabilities 0x%x\n", pcieCap.uPcieCaps);
                printf("Device Capabilities 0x%x\n", pcieCap.uDeviceCaps);
                printf("Device Control 0x%x\n", pcieCap.uDeviceCtrl);
                printf("Device Status 0x%x\n", pcieCap.uDeviceStat);
                printf("Link Capabilities 0x%x\n", pcieCap.uLinkCaps);
                printf("Link Control 0x%x\n", pcieCap.uLinkCtrl);
                printf("Link Status 0x%x\n", pcieCap.uLinkStat);
                printf("Slot Capabilities 0x%x\n", pcieCap.uSlotCaps);
                printf("Slot Control 0x%x\n", pcieCap.uSlotCtrl);
                printf("Slot Status 0x%x\n", pcieCap.uSlotStat);
                printf("Root Control 0x%x\n", pcieCap.uRootCtrl);
                printf("Root Capabilities 0x%x\n", pcieCap.uRootCaps);
                printf("Root Status 0x%x\n", pcieCap.uRootStat);
                printf("Device Capabilities 2 0x%x\n", pcieCap.uDeviceCaps2);
                printf("Device Control 2 0x%x\n", pcieCap.uDeviceCtrl2);
                printf("Device Status 2 0x%x\n", pcieCap.uDeviceStat2);
                printf("Link Capabilities 2 0x%x\n", pcieCap.uLinkCaps2);
                printf("Link Control 2 0x%x\n", pcieCap.uLinkCtrl2);
                printf("Link Status 2 0x%x\n", pcieCap.uLinkStat2);
                printf("Slot Capabilities 2 0x%x\n", pcieCap.uSlotCaps2);
                printf("Slot Control 2 0x%x\n", pcieCap.uSlotCtrl2);
                printf("Slot Status 2 0x%x\n", pcieCap.uSlotStat2);
                }
                break;
            case USB_XHCI_PCI_CAP_ID_MSIx:
                {
                UINT16 uMsgCtrl = 0;
                UINT32 uMsix32 = 0;

                printf("====USB_XHCI_PCI_CAP_ID_MSIx  - "
                    "PCI Message Signaled Interrupts (Extended)\n");

                /* Get the Message Control for PCI MSI-X Capability */

                VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 2), 2, uMsgCtrl);

                printf("--------MSI-X %s\n",
                    (uMsgCtrl & USB_XHCI_PCI_MSIX_EN) ? "Enabled" : "Disabled");
                printf("--------Function MSI-X interrupt %s\n",
                    (uMsgCtrl & USB_XHCI_PCI_MSIX_FMASK) ? "Masked" : "UnMasked");
                printf("--------Table Size %d\n",
                    USB_XHCI_PCI_MSIX_TB_SIZE_GET(uMsgCtrl));

                /* Get the Table Offset/Table BIR for PCI MSI-X Capability */

                VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 4), 4, uMsix32);

                printf("--------Table Offset 0x%x\n",
                    USB_XHCI_PCI_MSIX_TB_OFFSET_GET(uMsix32));
                printf("--------Table BIR    0x%x\n",
                    USB_XHCI_PCI_MSIX_TB_BIR_GET(uMsix32));

                /* Get the PBA Offset/PBA BIR for PCI MSI-X Capability */

                VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPci + 4), 4, uMsix32);

                printf("--------PBA Offset 0x%x\n",
                    USB_XHCI_PCI_MSIX_PBA_OFFSET_GET(uMsix32));
                printf("--------PBA BIR    0x%x\n",
                    USB_XHCI_PCI_MSIX_PBA_BIR_GET(uMsix32));
                }
                break;
            default:
                {
                printf("====xHCI unrelated Standard PCI Capability ID 0x%x\n",
                    uCapIdPci);
                }
            }

       /* Go to the next Standard PCI Capability */

       if (uOffsetPci < uOffsetPciNext)
            uOffsetPci = uOffsetPciNext;
       else
            break;
        }
    while (uOffsetPci != 0);

    /*
     * The following code will not work on pcPentium4 since
     * the PCI driver does not support ECS access!
     */

    /*
     * If we have found the PCIe Capability in the previous loop,
     * scan the PCIe Extended Capabilities!
     */

    while (uOffsetPCIe)
        {
        /*
         * The bottom 2 bits of this offset are Reserved and must
         * be implemented as 00b although software must mask them
         * to allow for future uses of these bits.
         */

        uOffsetPCIe = (UINT16)(uOffsetPCIe & ~0x03);

        printf("PCIe Extended Capability ID    @0x%x", uOffsetPCIe);

        /*
         * Read the config space using access functions
         */

        /* Get the Capability ID field */

        VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPCIe + 0), 2, uCapIdPcie);

        /* Get the offset of the next PCIe Extended Capability */

        VXB_PCI_BUS_CFG_READ(pDev, (uOffsetPCIe + 2), 2, uOffsetPCIe);

        uCapVerPcie = (UINT8)(uOffsetPCIe & 0xF);
        uOffsetPciNext = (UINT8)((uOffsetPCIe >> 4) & 0xFFF);

        switch (uCapIdPcie)
            {
            case USB_XHCI_PCIe_EXT_CAP_ID_SR_IOV:
                {
                printf("====USB_XHCI_PCIe_EXT_CAP_ID_SR_IOV - "
                    "PCIe Single-Root I/O Virtualization (SR-IOV) [ver %d]\n",
                    uCapVerPcie);
                }
                break;
            case USB_XHCI_PCIe_EXT_CAP_ID_MR_IOV:
                {
                printf("====USB_XHCI_PCIe_EXT_CAP_ID_MR_IOV - "
                    "PCIe Multi-Root I/O Virtualization (MR-IOV) [ver %d]\n",
                    uCapVerPcie);
                }
                break;
            default:
                {
                printf("====xHCI unrelated PCIe Extended Capability ID 0x%x [ver %d]\n",
                    uCapIdPcie, uCapVerPcie);
                }
            }

        /* Go to the next Extended PCI Capability */

        if (uOffsetPCIe < uOffsetPciNext)
             uOffsetPCIe = uOffsetPciNext;
        else
             break;
        }
    }

/*******************************************************************************
*
* usbXhcdExtCapShow - show the xHCI Extended Capabilities
*
* This routine is used to show the xHCI Extended Capabilities.
*
* RETURNS: N/A
*
* ERRNO: N/A
*/

VOID usbXhcdExtCapShow
    (
    UINT32 uIndex
    )
    {
    pUSB_XHCD_DATA pHCDData;
    UINT32 uOffset;
    UINT32 uReg32;
    UINT32 uCapId;

    if (uIndex > USB_MAX_HOSTS_PER_HCD)
        {
        printf("usbXhcdExtCapShow - Invalid uIndex %d\n",
            uIndex);

        return;
        }

    /* Get the xHCI instance at the specific index */

    pHCDData = gpXHCDData[uIndex];

    if (pHCDData == NULL)
        {
        printf("usbXhcdExtCapShow - No xHCI at uIndex %d\n",
            uIndex);

        return;
        }

    /* If there is no xHCI Extended Capability, reutnr ERROR */

    if (pHCDData->uXpRegBase == 0)
        {
        printf("usbXhcdExtCapShow - No xHCI Extended Capability!\n");

        return;
        }

    uOffset = 0;

    do
        {
        /* Read the xHCI Extended Capability Pointer Register */

        uReg32 = USB_XHCD_READ_XP_REG32(pHCDData, uOffset);

        /* Get the Capability ID field */

        uCapId = USB_XHCI_EXT_CAP_ID(uReg32);

        switch (uCapId)
            {
            case USB_XHCI_EXT_CAP_LEGACY:
                {
                printf("USB_XHCI_EXT_CAP_LEGACY - "
                    "xHCI USB Legacy Support Capability\n");

                uReg32 = USB_XHCD_READ_XP_REG32(pHCDData,
                    (uOffset + USB_XHCI_USBLEGSUP));

                printf("USB_XHCI_USBLEGSUP              = 0x%08X\n", uReg32);

                uReg32 = USB_XHCD_READ_XP_REG32(pHCDData,
                    (uOffset + USB_XHCI_USBLEGCTLSTS));

                printf("USB_XHCI_USBLEGCTLSTS           = 0x%08X\n", uReg32);
                }
                break;
            case USB_XHCI_EXT_CAP_PROTOCOL:
                {
                UINT32 uPSIC;
                UINT32 uIdx;

                printf("USB_XHCI_EXT_CAP_PROTOCOL - "
                    "xHCI Supported Protocol Capability\n");

                uReg32 = USB_XHCD_READ_XP_REG32(pHCDData,
                    (uOffset + USB_XHCI_SUPPORTED_PROT_VERSION));

                printf("USB_XHCI_SUPPORTED_PROT_VERSION = 0x%08X (USB %x.%x)\n",
                    uReg32,
                    USB_XHCI_SUPPORTED_PROT_MAJOR(uReg32),
                    USB_XHCI_SUPPORTED_PROT_MINOR(uReg32));

                uReg32 = USB_XHCD_READ_XP_REG32(pHCDData,
                    (uOffset + USB_XHCI_SUPPORTED_PROT_NAME));

                printf("USB_XHCI_SUPPORTED_PROT_NAME    = 0x%08X\n", uReg32);

                uReg32 = USB_XHCD_READ_XP_REG32(pHCDData,
                    (uOffset + USB_XHCI_SUPPORTED_PROT_COUNT));

                printf("USB_XHCI_SUPPORTED_PROT_COUNT   = 0x%08X\n", uReg32);

                uPSIC = USB_XHCI_SUPPORTED_PROT_PSIC(uReg32);

                for (uIdx = 0; uIdx < uPSIC; uIdx++)
                    {
                    uReg32 = USB_XHCD_READ_XP_REG32(pHCDData,
                        (uOffset + USB_XHCI_SUPPORTED_PROT_ID(uIdx)));

                    printf("USB_XHCI_SUPPORTED_PROT_ID(%d)  = 0x%08X\n",
                        uIdx, uReg32);
                    }
                }
                break;
            case USB_XHCI_EXT_CAP_PM:
                {
                printf("USB_XHCI_EXT_CAP_PM - "
                    "xHCI Extended Power Management Capability\n");

                uReg32 = USB_XHCD_READ_XP_REG32(pHCDData,
                    (uOffset + USB_XHCI_PMC));

                printf("USB_XHCI_PMC                    = 0x%08X\n", uReg32);

                uReg32 = USB_XHCD_READ_XP_REG32(pHCDData,
                    (uOffset + USB_XHCI_PMCSR));

                printf("USB_XHCI_PMCSR                  = 0x%08X\n", uReg32);

                }
                break;
            case USB_XHCI_EXT_CAP_VIRT:
                {
                printf("USB_XHCI_EXT_CAP_VIRT - "
                    "xHCI I/O Virtualization Capability\n");
                }
                break;
            case USB_XHCI_EXT_CAP_MSI:
                {
                printf("USB_XHCI_EXT_CAP_MSI - "
                    "xHCI Message Interrupt Capability\n");
                }
                break;
            case USB_XHCI_EXT_CAP_LMEM:
                {
                printf("USB_XHCI_EXT_CAP_LMEM - "
                    "xHCI Local Memory Capability\n");
                }
                break;
            case USB_XHCI_EXT_CAP_DEBUG:
                {
                printf("USB_XHCI_EXT_CAP_DEBUG - "
                    "xHCI USB Debug Capability\n");
                }
                break;
            case USB_XHCI_EXT_CAP_MSIX:
                {
                printf("USB_XHCI_EXT_CAP_DEBUG - "
                    "xHCI USB Debug Capability\n");
                }
                break;
            default:
                {
                if ((uCapId == 0) ||
                    ((uCapId >= 7) && (uCapId <= 9)) ||
                    ((uCapId >= 11) && (uCapId <= 16)) ||
                    ((uCapId >= 18) && (uCapId <= 191)))
                    {
                    printf("xHCI Reserved Capability ID %d\n", uCapId);
                    }
                else /* if ((uCapId >= 192) && (uCapId <= 255)) */
                    {
                    printf("xHCI Vendor Defined Capability ID %d\n", uCapId);
                    }
                }
            }

        /* Go to the next xHCI Extended Capability */

        uOffset = usbXhcdNextExtCapReg(pHCDData, uOffset);
        }
    while (uOffset != 0);
    }

