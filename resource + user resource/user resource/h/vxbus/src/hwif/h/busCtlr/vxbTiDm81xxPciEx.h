/* vxbTiDm81xxPciEx.h - header file for TI DM81xx PCIe bus driver */

/*
 * Copyright (c) 2011, 2013 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01b,14mar13,c_l  Set PL_GEN2 bit-17 to re-trigger link training. (WIND00408231)
01a,08oct11,clx  created
*/

#ifndef __INCvxbTiDm81xxPciExh
#define __INCvxbTiDm81xxPciExh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void vxbTi81xxPcieRegister (void);

/* define */

/* Application Registers */

#define PID                     (0x0)
#define CMD_STATUS              (0x4)
#define CFG_SETUP               (0x8)
#define IOBASE                  (0xC)
#define TLPCFG                  (0x10)
#define RSTCMD                  (0x14)
#define PMCMD                   (0x20)
#define PMCFG                   (0x24)
#define ACT_STATUS              (0x28)
#define OB_SIZE                 (0x30)
#define DIAG_CTRL               (0x34)
#define ENDIAN                  (0x38)
#define PRIORITY                (0x3C)
#define IRQ_EOI                 (0x50)
#define MSI_IRQ                 (0x54)
#define EP_IRQ_SET              (0x64)
#define EP_IRQ_CLR              (0x68)
#define EP_IRQ_STATUS           (0x6C)
#define GPR0                    (0x70)
#define GPR1                    (0x74)
#define GPR2                    (0x78)
#define GPR3                    (0x7C)
#define MSI0_IRQ_STATUS_RAW     (0x100)
#define MSI0_IRQ_STATUS         (0x104)
#define MSI0_IRQ_ENABLE_SET     (0x108)
#define MSI0_IRQ_ENABLE_CLR     (0x10C)
#define IRQ_STATUS_RAW          (0x180)
#define IRQ_STATUS              (0x184)
#define IRQ_ENABLE_SET          (0x188)
#define IRQ_ENABLE_CLR          (0x18C)
#define ERR_IRQ_STATUS_RAW      (0x1C0)
#define ERR_IRQ_STATUS          (0x1C4)
#define ERR_IRQ_ENABLE_SET      (0x1C8)
#define ERR_IRQ_ENABLE_CLR      (0x1CC)
#define PMRST_IRQ_STATUS_RAW    (0x1D0)
#define PMRST_IRQ_STATUS        (0x1D4)
#define PMRST_ENABLE_SET        (0x1D8)
#define PMRST_ENABLE_CLR        (0x1DC)
#define OB_OFFSET_INDEX(n)      (0x200 + (8 * n))
#define OB_OFFSET_HI(n)         (0x204 + (8 * n))
#define IB_BAR(n)               (0x300 + (0x10 * n))
#define IB_START_LO(n)          (0x304 + (0x10 * n))
#define IB_START_HI(n)          (0x308 + (0x10 * n))
#define IB_OFFSET(n)            (0x30c + (0x10 * n))
#define PCS_CFG0                (0x380)
#define PCS_CFG1                (0x384)
#define PCS_STATUS              (0x388)
#define SERDES_CFG0             (0x390)
#define SERDES_CFG1             (0x394)

/* PCIe Capability Registers */

#define PCIES_CAP               (0x00)
#define DEVICE_CAP              (0x04)
#define DEV_STAT_CTRL           (0x08)
#define LINK_CAP                (0x0C)
#define LINK_STAT_CTRL          (0x10)
#define SLOT_CAP                (0x14)
#define SLOT_STAT_CTRL          (0x18)
#define ROOT_CTRL_CAP           (0x1C)
#define ROOT_STATUS             (0x20)
#define DEV_CAP2                (0x24)
#define DEV_STAT_CTRL2          (0x28)
#define LINK_CTRL2              (0x30)

/* PCIe Extended Capability Registers */

#define PCIE_EXTCAP             (0x100)
#define PCIE_UNCERR             (0x104)
#define PCIE_UNCERR_MASK        (0x108)
#define PCIE_UNCERR_SVRTY       (0x10C)
#define PCIE_CERR               (0x110)
#define PCIE_CERR_MASK          (0x114)
#define PCIE_ACCR               (0x118)
#define HDR_LOG0                (0x11C)
#define HDR_LOG1                (0x120)
#define HDR_LOG2                (0x124)
#define HDR_LOG3                (0x128)
#define RC_ERR_CMD              (0x12C)
#define RC_ERR_ST               (0x130)
#define ERR_SRC_ID              (0x134)

/* Message Signaled Interrupts Registers */

#define MSI_CAP                 (0x000)
#define MSI_LOW32               (0x004)
#define MSI_UP32                (0x008)
#define MSI_DATA                (0x00c)

/* Power Management Capability Registers */

#define PMCAP                   (0x000)
#define PM_CTL_STAT             (0x004)

/* Port Logic Registers */

#define PL_ACKTIMER             (0x700)
#define PL_OMSG                 (0x704)
#define PL_FORCE_LINK           (0x708)
#define ACK_FREQ                (0x70c)
#define PL_LINK_CTRL            (0x710)
#define LANE_SKEW               (0x714)
#define SYM_NUM                 (0x718)
#define SYMTIMER_FLTMASK        (0x71c)
#define FLT_MASK2               (0x720)
#define DEBUG0                  (0x728)
#define DEBUG1                  (0x72c)
#define PL_GEN2                 (0x80c)

/* Various regions in PCIESS address space0 */

#define SPACE0_LOCAL_CFG_OFFSET  (0x1000)
#define SPACE0_REMOTE_CFG_OFFSET (0x2000)
#define SPACE0_REMOTE_IO_OFFSET  (0x3000)

/* Application command register values */

#define DBI_CS2_EN              (1 << 5)
#define IB_XLT_EN               (1 << 2)
#define OB_XLT_EN               (1 << 1)
#define LTSSM_EN                (1 << 0)

/* Link training encodings as indicated in DEBUG0 register */

#define LTSSM_STATE_MASK        0x1f
#define LTSSM_STATE_L0          0x11
#define LTSSM_STATE_TIMEOUT     0x1000000

/* Default value used for Command register */

#define PCI_COMMAND_INVALIDATE      (1 << 19)
#define PCI_COMMAND_INTX_DISABLE    (1 << 10)
#define PCI_COMMAND_SERR            (1 << 8)
#define PCI_COMMAND_PARITY          (1 << 6)
#define PCI_COMMAND_MASTER          (1 << 2)
#define PCI_COMMAND_MEMORY          (1 << 1)
#define PCI_COMMAND_IO              (1 << 0)

/* PCIM default value */

#define CFG_PCIM_CSR_VAL    (PCI_COMMAND_SERR       | \
                             PCI_COMMAND_PARITY     | \
                             PCI_COMMAND_INVALIDATE | \
                             PCI_COMMAND_MASTER     | \
                             PCI_COMMAND_MEMORY     | \
                             PCI_COMMAND_INTX_DISABLE)

/* Outbound window size */

#define OB_SIZE_8M    3
#define OB_SIZE_4M    2
#define OB_SIZE_2M    1
#define OB_SIZE_1M    0
#define OB_WIN_CNT    32   /* 32 OutBound windows */

#define TI816X_PCI_VENDOR_ID            0x104c
#define TI816X_PCI_DEVICE_ID            0x8888

#define MAX_LINK_UP_CHECK_COUNT         100000

typedef struct ti81xxPcieDrvCtrl
    {
    VXB_DEVICE_ID         pInst;
    void *                regBase;
    void *                handle;
    int                   maxBusSet;

    void *                mem32Addr;
    UINT32                mem32Size;
    void *                memIo32Addr;
    UINT32                memIo32Size;
    void *                io32Addr;
    UINT32                io32Size;
    void *                io16Addr;
    UINT32                io16Size;

    UINT32                autoConfig;
    UINT32                speedDetect;
    BOOL                  initDone;

    struct vxbPciConfig * pPciConfig;
    struct vxbPciInt *    pIntInfo;
    struct hcfDevice *    pHcf;
    } TI81XX_PCIE_DRV_CTRL;

#define CSR_SETBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) | (val))

#define CSR_CLRBIT_1(pDev, offset, val)          \
        CSR_WRITE_1(pDev, offset, CSR_READ_1(pDev, offset) & ~(val))

#define CSR_SETBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) | (val))

#define CSR_CLRBIT_2(pDev, offset, val)          \
        CSR_WRITE_2(pDev, offset, CSR_READ_2(pDev, offset) & ~(val))

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbTiDm81xxPciExh */

